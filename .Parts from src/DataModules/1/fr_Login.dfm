object frame_Login: Tframe_Login
  Left = 542
  Top = 450
  Width = 550
  Height = 286
  Caption = 'frame_Login'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object gb_ConnectToSQL: TGroupBox
    Left = 0
    Top = 0
    Width = 542
    Height = 133
    Align = alTop
    Caption = #1055#1086#1076#1082#1083#1102#1095#1077#1085#1080#1077' '#1082' DB RPLS'
    TabOrder = 0
    object dxInspector2: TdxInspector
      Left = 2
      Top = 15
      Width = 538
      Height = 114
      Align = alTop
      TabOrder = 0
      DividerPos = 234
      PaintStyle = ipsExtended
      OnChangeNode = dxInspector2ChangeNode
      Data = {
        970000000600000008000000000000000A000000726F775F5365727665720800
        00000000000009000000726F775F6C6F67696E08000000000000000C00000072
        6F775F50617373776F726408000000000000000C000000726F775F53514C5F61
        7574680800000000000000100000006478496E73706563746F7232526F773708
        000000000000000C000000726F775F446174616261736500000000}
      object row_login: TdxInspectorTextRow
        Caption = 'Login'
        OnChange = row_ServerChange
      end
      object row_Password: TdxInspectorTextRow
        Caption = 'Password'
        OnChange = row_ServerChange
      end
      object row_SQL_auth: TdxInspectorTextPickRow
        Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1087#1086#1076#1083#1080#1085#1085#1086#1089#1090#1080' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1103
        OnChange = row_ServerChange
        DropDownListStyle = True
        Items.Strings = (
          #1091#1095#1077#1090#1085#1072#1103' '#1079#1072#1087#1080#1089#1100' Windows'
          #1091#1095#1077#1090#1085#1072#1103' '#1079#1072#1087#1080#1089#1100' SQL Server')
        Text = #1091#1095#1077#1090#1085#1072#1103' '#1079#1072#1087#1080#1089#1100' Windows'
      end
      object row_Server: TdxInspectorTextButtonRow
        Caption = 'Server'
        OnChange = row_ServerChange
        Buttons = <
          item
            Default = True
          end>
        OnButtonClick = row_ServerButtonClick
      end
      object row_Database: TdxInspectorTextPickRow
        Caption = 'Database'
        OnValidate = row_DatabaseValidate
        OnEditButtonClick = row_DatabaseEditButtonClick
        DropDownRows = 20
        OnCloseUp = row_DatabaseCloseUp
      end
      object dxInspector2Row7: TdxInspectorTextRow
        IsCategory = True
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 133
    Width = 542
    Height = 32
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      542
      32)
    object Button1: TButton
      Left = 406
      Top = 4
      Width = 132
      Height = 21
      Action = act_Check
      Anchors = [akTop, akRight]
      TabOrder = 0
    end
  end
  object ActionList1: TActionList
    Left = 132
    Top = 180
    object act_Check: TAction
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1089#1086#1077#1076#1080#1085#1077#1085#1080#1103
      OnExecute = act_CheckExecute
    end
    object act_Database_Refresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1041#1044
      OnExecute = act_CheckExecute
    end
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Initi' +
      'al Catalog=master;Data Source=SERVER;Use Procedure for Prepare=1' +
      ';Auto Translate=True;Packet Size=4096;Workstation ID=ALEX;Use En' +
      'cryption for Data=False;Tag with column collation when possible=' +
      'False'
    DefaultDatabase = 'master'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 56
    Top = 180
  end
end
