unit _dm_Main_Init;

interface
{$I ver.inc}

uses Forms, Classes, SysUtils,  Dialogs, IniFiles,

 // dm_Idle,
//  f_Idle,

u_App_config,

  u_func,


  i_Options_,
  i_WMS,


  dm_DB_check,

  u_types,

  u_func_msg,

  u_reg,
  u_log,
  u_files,

  dm_User_Security,

  dm_Act_Repeater,

  dm_MapEngine_store,

  dm_Act_Link_SDB,
  dm_Act_Project,
 // dm_Object_Manager,

  dm_Main,
  dm_Main_res,

  dm_Act_Antenna,
  dm_Act_Link_Repeater_Antenna,

  dm_Act_ColorSchema ,
  dm_act_LOS,
  dm_Act_Folder,
  dm_Act_Company,
  dm_Act_Terminal,
  dm_Act_Trx,

  dm_act_AntType,

  //dm_Act_Mast,

  dm_Act_Template_Site,
  //dm_Act_Template_Site_mast,
  dm_Act_Template_Site_linkend,
  dm_Act_Template_Site_linkend_antenna,

  dm_Act_Template_linkend,
  dm_Act_Template_linkend_antenna,

  dm_Act_Template_PMP_Site,
  dm_act_GeoRegion,
  dm_Act_MSC,
  dm_Act_BSC,
  dm_Act_Cell_Layer,
  dm_act_CalcModel,
  dm_act_Clutter,
  dm_Act_Combiner,
 // dm_Act_Filter,

  dm_Act_MapFile,

  dm_Act_Profile,
  dm_Act_Explorer,

  dm_Act_Map,


//  dm_Act_Template_Linkend_Antenna,


  dm_Act_Status,

  dm_act_Rel_Engine,
  dm_Act_RelFile,
  dm_Act_Report,

  dm_act_Link,
  dm_Act_LinkEnd,
  dm_act_LinkEndType,
  dm_act_LinkFreqPlan, AppEvnts,
  dm_Act_LinkFreqPlan_Link,
  dm_act_LinkLine,
  dm_act_LinkLine_Link,
  dm_act_LinkNet,
  dm_act_LinkNet_Link_Ref,
  dm_act_LinkNet_LinkLine_Ref,
  dm_act_LinkType,

  dm_Act_Pmp_Sector,
  dm_Act_Pmp_Site,
  dm_Act_PmpTerminal,
  dm_Act_PMP_CalcRegion,
  dm_act_PMP_CalcRegion_Site,

  dm_Act_Property,
//  I_Property,


//  x_Options,


  f_Main_MDI
  ;


type
  TdmMain_Init_link = class(TDataModule)
    ApplicationEvents1: TApplicationEvents;

    procedure ApplicationEvents1Deactivate(Sender: TObject);
    procedure ApplicationEvents1Exception(Sender: TObject; E: Exception);
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private

 //   procedure TE_update_reg;

//    function CheckFiles: Boolean;
  public
    function AfterOpenDatabase: Boolean;
//    procedure FreeDataModules;
    procedure InitDataModules;
  //  procedure TestFree;
  end;

var
  dmMain_Init_link: TdmMain_Init_link;


implementation

//uses f_Idle;
 {$R *.dfm}


 {
// ---------------------------------------------------------------
function TdmMain_Init_link.CheckFiles: Boolean;
// ---------------------------------------------------------------
var
  i: Integer;
  oFiles: TStringList;

  oIni: TIniFile;
  s: string;
begin
  oFiles:=TStringList.Create;

  oIni:=TIniFile.Create ( ChangeFileExt(Application.ExeName, '.ini'));
  oIni.ReadSection('files',oFiles);

  i:=oFiles.Count;

  for I := 0 to oFiles.Count - 1 do
  begin
    s:= IncludeTrailingBackslash(  ExtractFileDir(Application.ExeName));
    s:=s + oFiles[i];

    if not FileExists(s) then
    begin
      ShowMessage('not found: '+s);
    //  Halt ;
    end;
  end;


  FreeAndNil(oIni);

  FreeAndNil(oFiles);


end;

}

// ---------------------------------------------------------------
function TdmMain_Init_link.AfterOpenDatabase: Boolean;
// ---------------------------------------------------------------
begin


  // -------------------------
  TdmDB_check1.Init;
  Result:=dmDB_check1.Exec;
  FreeAndNil(dmDB_check1);


  TdmUser_Security.Init;
  dmUser_Security.Open;


//  Result:=

//  if not b then
  //  Exit;

end;

procedure TdmMain_Init_link.ApplicationEvents1Deactivate(Sender: TObject);
begin

end;


// -------------------------------------------------------------------
procedure TdmMain_Init_link.DataModuleCreate(Sender: TObject);
// -------------------------------------------------------------------
var
  b: Boolean;
begin

  g_App_config.LoadFromFile ( ChangeFileExt(Application.ExeName, '.ini' ));


  Options_Init__1;
//  WMS_Init;



//  CheckFiles();


  Assert(Assigned(Application));

  g_Log.SetFileName(
    IncludeTrailingBackslash(GetTempFileDir())+'_rpls_log.txt');


//  g_Log:=TLog.Create(
 //   IncludeTrailingBackslash(GetTempFileDir())+'_rpls_log.txt');

  g_EventManager:=TEventManager.Create;
 // g_ShellEvents:=TShellEventManager.Create;

  g_Obj:=TObjectEngine.Create;



  TdmMain.Init;


  if not dmMain.OpenDB_dlg then

//  if not dmMain.OpenDB_ then
//  if not dmMain.OpenDB_reg then
    Exit;


 if not AfterOpenDatabase() then
   Exit;

   {
  TdmUser_Security.Init;
  dmUser_Security.Open;


  // -------------------------
  TdmDB_check1.Init;
  b:=dmDB_check1.Exec;
  FreeAndNil(dmDB_check1);

  if not b then
    Exit;
 }

  // -------------------------


  InitDataModules();


//  Application.CreateForm(Tfrm_Idle, frm_Idle);

//  frm_Idle.Show;


  Application.CreateForm(Tfrm_Main_MDI, frm_Main_MDI);
  Application.ProcessMessages;


  dmAct_Project.LoadLastProject;


end;




procedure TdmMain_Init_link.InitDataModules;
begin

//  TdmIdle.Init;

  TdmMain_res.Init;

  TdmAct_Link_SDB.Init;

  TdmUser_Security.Init;


//  TdmObject_Manager.Init;
  TdmAct_Profile.Init;

  TdmAct_Antenna.Init;
  TdmAct_RelFile.Init;
  TdmAct_MSC.Init;
  TdmAct_BSC.Init;

  Tdmact_LOS.Init;

  TdmAct_Company.Init;
  TdmAct_Terminal.Init;
  TdmAct_AntType.Init;
  TdmAct_ColorSchema.Init;

  
//  TdmAct_Company.Init;
  TdmAct_Trx.Init;
  //TdmAct_TransferTech.Init;
  TdmAct_CalcModel.Init;

  TdmAct_Terminal.Init;
  TdmAct_Cell_Layer.Init;
  TdmAct_ClutterModel.Init;

  TdmAct_CalcModel.Init;
  TdmAct_Combiner.Init;
  TdmAct_Status.Init;
  TdmAct_Report.Init;


  //shell
  TdmAct_Explorer.Init;
 //// TdmAct_Filter.Init;
  TdmAct_Folder.Init;

  TdmAct_LinkEndType.Init;

  TdmAct_Link.Init;
  TdmAct_LinkEnd.Init;

  TdmAct_LinkFreqPlan.Init;
  TdmAct_LinkFreqPlan_Link.Init;
  TdmAct_LinkLine.Init;
  TdmAct_LinkLine_Link.Init;
  TdmAct_LinkNet.Init;
  TdmAct_LinkNet_Link_Ref.Init;
  TdmAct_LinkNet_LinkLine_Ref.Init;
  TdmAct_LinkType.Init;

  TdmAct_PMP_Sector.Init;
  TdmAct_PMP_site.Init;
  TdmAct_PmpTerminal.Init;
  TdmAct_Pmp_CalcRegion.Init;
  TdmAct_PMP_CalcRegion_Site.Init;


  TdmAct_Rel_Engine.Init;

  TdmAct_Property.Init;

  TdmAct_Link_Repeater.Init;
  TdmAct_Link_Repeater_Antenna.Init;

//  TdmAct_Mast.Init;


 // TdmAct_LinkNet.Init;
  TdmAct_GeoRegion.Init;
  TdmAct_MapFile.Init;




  TdmAct_Template_Site.Init;
//  TdmAct_Template_Site_Mast.Init;
  TdmAct_Template_Site_Linkend.Init;
  TdmAct_Template_Site_Linkend_Antenna.Init;
 // TdmAct_Template_Site.Init;


  TdmAct_Template_Linkend_Antenna.Init;
  TdmAct_Template_Linkend.Init;
  TdmAct_Template_PMP_Site.Init;

  TdmAct_Project.Init;


  TdmMapEngine_store.Init;

 // Exit;

///////////////////
  TdmAct_Map.Init;

end;


procedure TdmMain_Init_link.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(g_EventManager);
  FreeAndNil(g_Obj);
end;



procedure TdmMain_Init_link.ApplicationEvents1Exception(Sender: TObject; E:
    Exception);
begin

  ShowMessage(E.Message);

  g_Log.AddError('', E.Message);

end;




end.


{
procedure TdmMain_Init_link.TE_update_reg;
const
  REGISTRY_PATH = 'Software\Onega\Link';
begin
  reg_WriteString (REGISTRY_PATH, 'AppPath', Application.ExeName);

end;
 }
