unit dm_Main;

interface

uses
  ActiveX, DB, SysUtils, Classes, Windows,  ADODB, Forms, Dialogs,

//  i_SNMP,

  u_Log,

  u_com,
  Login_TLB,


  dm_Onega_DB_data,

  u_const,

  u_vars,

  u_reg,

  u_func,

  u_db_manager,

  I_db_login
  , ADOInt;


type


  TdmMainDirsRec = record
    Dir:  string;
    
    ProjectCalcDir:  string;
    ProjectMapDir:   string;
  end;


  TdmMain = class(TDataModule)
    ADOConnection1: TADOConnection;
    procedure ADOConnection1Disconnect(Connection: TADOConnection; var EventStatus:
        TEventStatus);
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

  private
    FConnectionObject_old: _Connection;

    FProjectID : integer;
    FProjectName: string;

  //  FUserID: Integer;
//    FUserName  : string;

    FDirs: TdmMainDirsRec;

    function GetProjectID: Integer;
    function GetProjectName: string;
    procedure SetProjectID(aValue: Integer);
    procedure SetProjectName(aValue: string);


// TODO: InitDirectories
//  procedure InitDirectories;

  protected
//    function GetUserID: Integer;
//    procedure SetUserID(Value: Integer);

  private
    function GetLocalDir(aProjectID: integer): string;
    function GetADOConnection: TADOConnection;
    function OpenDB_dlg_new: boolean;
  public
    LoginRec  : TdbLoginRec;

//    ProjectIsReadOnly : Boolean;
 //   Project_EnableEditing : Boolean;
           

    function GetDirs_(aProjectID: Integer=-1): TdmMainDirsRec;

    function SetLocalDir (aProjectID: integer; aFileDir: string): string;

    function GetProjectRegPath(aProjectID: integer=0): string;

    property ADOConnection: TADOConnection read GetADOConnection;

  //  property DEBUG: Boolean read GetDEBUG;
    property ProjectID: Integer read FProjectID write SetProjectID;
    property ProjectName: string read GetProjectName write SetProjectName;

    property Dirs: TdmMainDirsRec read FDirs; // GetDirs_;
// TODO: aaaaaaaaaaaaaaa
//  procedure aaaaaaaaaaaaaaa;
    function GetCalcDir(aProjectID: integer): string;

    function GetConnectionString: string;

   // property UserID: Integer read GetUserID write SetUserID;

    class procedure Init;


    function Open_ADOConnection(aConnectionStr: string): Boolean;

    function OpenDB_(aLoginOptions: TDBLoginOptions = []): boolean;
    function OpenDB_dlg: boolean;
    function OpenDB_rec(aLoginRec : TdbLoginRec): boolean;
    function OpenDB_reg: boolean;

    procedure SetADOConnectionObject(aConnectionObject: _Connection);
    procedure RestoreADOConnectionObject;

//    function OpenDB_(aIsShowDlg: boolean=False): boolean;
    procedure Test_Open;
  end;

procedure TdmMain_InitDataManagers(aADOConnection: TADOConnection);

procedure TdmMain_FreeDataManagers;


const
  REGISTRY_LOGIN_ = 'Software\Onega\RPLS_DB_LINK\Login_new1\';


var
  dmMain: TdmMain;

  gl_DB: TDBManager;

  g_IsDebug: Boolean;


//===============================================================
//implementation
//===============================================================
implementation   
{$R *.dfm}


class procedure TdmMain.Init;
begin
  Assert(not Assigned(dmMain));

 // if Assigned(dmMain) then  raise Exception.Create('procedure TTdmMain_Init;');

 //Application.Initialize

  CoInitialize(nil);


  Application.CreateForm(TdmMain, dmMain);

//  dmMain:=TdmMain.Create(Application);

//  Application.CreateForm(TdmMain, dmMain_);
//  dmMain:=TdmMain.Create(Application);

//  Application.CreateForm(TdmMain1, dmMain1);

end;



procedure TdmMain_InitDataManagers(aADOConnection: TADOConnection);
begin
 // Assert(not Assigned(dmOnega_DB_data), 'Value not assigned');
  Assert(not Assigned(gl_DB), 'Value not assigned');

  TdmOnega_DB_data.Init;
  dmOnega_DB_data.ADOConnection :=aADOConnection;

  //:=TOnega_DB_data.Create(aADOConnection);
  gl_DB:=TDBManager.Create(aADOConnection);

end;


procedure TdmMain_FreeDataManagers;
begin
  FreeAndNil(dmOnega_DB_data);
  FreeAndNil(gl_DB);
end;


procedure TdmMain.ADOConnection1Disconnect(Connection: TADOConnection; var EventStatus: TEventStatus);
begin
//  g_Log.AddError('TdmMain.ADOConnection', 'Disconnect');

//  Init_ISnmp.Send('dmMain.ADOConnection Disconnect');

end;

//--------------------------------------------------------------------
procedure TdmMain.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  Assert(Assigned(Application));



  if ADOConnection1.Connected then
    ShowMessage('procedure TdmMain.DataModuleCreate(Sender: TObject); -  ADOConn');

//    g_Log.Error('procedure TdmMain.DataModuleCreate(Sender: TObject); -  ADOConn');

  TdmMain_InitDataManagers (ADOConnection1);

  //TdmOnega_DB_data.Init;
 // dmOnega_DB_data.ADOConnection:=ADOConnection1;
                      

//  dmOnega_DB_data:=TOnega_DB_data.Create(ADOConnection1);
// // dmOnega_DB_data.ADOConnection :=ADOConnection1;
//
//
//  gl_DB:=TDBManager.Create(ADOConnection);
////  gl_DB.ADOConnection:=ADOConnection;

(*
  TdmDBManager.Init;
  dmDBManager.SetADOConnection(ADOConnection);
*)

//  ProjectIsReadOnly := True;

  LoginRec.Server       := '(local)';
  LoginRec.DataBase     := 'onega_link';
  LoginRec.IsUseWinAuth := True;
  LoginRec.Login        := 'sa';

end;


procedure TdmMain.DataModuleDestroy(Sender: TObject);
begin
 // ShowMessage('procedure TdmMain.DataModuleDestroy(Sender: TObject);');


 ///  Assert(not Assigned(FConnectionObject_old), 'FConnectionObject_old assigned');

(*
  if Assigned(FConnectionObject_old) then
  begin
    ADOConnection1.ConnectionObject := FConnectionObject_old;

    FConnectionObject_old:=nil;
  end;
*)


//  dmMain := nil;

 // dmOnega_DB_data.ADOConnection :=nil;
 // FreeAndNil(dmOnega_DB_data);

 // gl_DB.ADOConnection:=nil;

  FreeAndNil(gl_DB);


 /// ShowMessage('procedure TdmMain.DataModuleDestroy(Sender: TObject);');
 // g_Log.Add('procedure TdmMainX.DataModuleDestroy(Sender: TObject);');
  inherited;
end;


//--------------------------------------------------------------------
function TdmMain.OpenDB_(aLoginOptions: TDBLoginOptions = []): boolean;
//--------------------------------------------------------------------
var
  vIDBLogin: IDBLoginX;
begin
  vIDBLogin:=I_db_login.Get_IDBLoginX;
//  vIDBLogin.

  result :=vIDBLogin.Open(REGISTRY_LOGIN_,
       // dmMain.
        LoginRec,
        aLoginOptions,

       // dmMain.
        ADOConnection.ConnectionObject,
        Application.Handle
        );
end;

// ---------------------------------------------------------------
function TdmMain.GetConnectionString: string;
// ---------------------------------------------------------------
begin
  result:=ADOConnection.ConnectionString;

  if LoginRec.Password<>'' then
    result:=result + Format(';Password=%s', [LoginRec.Password]);

//  ADOConnection.Pa

end;


//--------------------------------------------------------------------
function TdmMain.OpenDB_rec(aLoginRec : TdbLoginRec): boolean;
//--------------------------------------------------------------------
var vIDBLogin: IDBLoginX;
begin
//  b:=dmConnection1.OpenDatabase(rec, dmMain.ADOConnection);


  vIDBLogin:=I_db_login.Get_IDBLoginX;
//  vIDBLogin.

  result :=vIDBLogin.Open(REGISTRY_LOGIN_,
        aLoginRec,
        [], //aLoginOptions,

       // dmMain.
        ADOConnection.ConnectionObject,
        Application.Handle
        );
end;


//--------------------------------------------------------------------
function TdmMain.OpenDB_reg: boolean;
//--------------------------------------------------------------------
begin
  Result := OpenDB_([loFromReg]);
end;

//--------------------------------------------------------------------
function TdmMain.OpenDB_dlg: boolean;
//--------------------------------------------------------------------
begin
  Result := OpenDB_([loShowDlg]);
end;



//--------------------------------------------------------------------
function TdmMain.OpenDB_dlg_new: boolean;
//--------------------------------------------------------------------
var
  b: Boolean;
  iDll: THandle;
  v: ILoginX;

  rec: Login_TLB.TLoginRec;

begin
  Result := OpenDB_([loShowDlg]);


  iDll:= GetComObject( ExtractFilePath(Application.ExeName) + 'login.dll',
            CLASS_LoginX, ILoginX, v);


  Result:=v.Exec_ex(REGISTRY_LOGIN_, ADOConnection1.ConnectionObject, rec)=S_OK;
  if Result then
  begin
    LoginRec.Server:=rec.Server_Name;
    LoginRec.Login:=rec.User_Name;


    ShowMessage ( ADOConnection1.ConnectionString);
  end;

//

  v:=nil;
//
  FreeLibrary(iDll);



end;




//-------------------------------------------------------------------
function TdmMain.GetCalcDir(aProjectID: integer): string;
//-------------------------------------------------------------------
begin
  Result:= GetLocalDir  (aProjectID);

{  case GetCalcStoreType(aProjectID) of
    cst_local:  Result:= GetLocalDir  (aProjectID);
    cst_shared: Result:= GetSharedDir (aProjectID);
  end;
}

end;

//-------------------------------------------------------------------
function TdmMain.GetLocalDir(aProjectID: integer): string;
//-------------------------------------------------------------------
begin
 // Result:= Reg_ReadString (REGISTRY_PROJECTS1+IntToStr(aProjectID), 'local_dir', TEMP_DIR);
  Result:= Reg_ReadString (GetProjectRegPath(aProjectID), 'local_dir', g_ApplicationDataDir);
  if Result<>'' then
    Result:=IncludeTrailingBackslash(Result)
  else
    Result:=g_ApplicationDataDir;


 //  Result:='p:\_���� 1\';

end;

//-------------------------------------------------------------------
function TdmMain.SetLocalDir (aProjectID: integer; aFileDir: string): string;
//-------------------------------------------------------------------
begin
 // Reg_WriteString (REGISTRY_PROJECTS1+IntToStr(aProjectID), 'local_dir', aFileDir);
  Reg_WriteString (GetProjectRegPath(aProjectID), 'local_dir', aFileDir);
///  UpdateCalcMaps(aProjectID, aFileDir);

end;


function TdmMain.GetADOConnection: TADOConnection;
begin
  Result:=ADOConnection1;
end;


function TdmMain.GetProjectID: Integer;
begin
  Result := FProjectID;
end;

function TdmMain.GetProjectName: string;
begin
  Result := FProjectName;
end;


function TdmMain.GetProjectRegPath(aProjectID: integer=0): string;
begin
  if aProjectID=0 then
    aProjectID:=ProjectID;

  Assert(aProjectID>0,'TdmMain.GetProjectRegPath: string; - ProjectID>0');

  Result:= Format (REGISTRY_PROJECTS1 + 'project_%d\', [aProjectID]);
end;



procedure TdmMain.SetProjectID(aValue: Integer);
begin
  FProjectID:=aValue;

 // FillChar(FDirs, SizeOf(FDirs), 0);

  if FProjectID>0 then
    FDirs := GetDirs_ (FProjectID);
//    InitDirectories();

end;

procedure TdmMain.SetProjectName(aValue: string);
begin
  FProjectName:=aValue;
end;


procedure TdmMain.Test_Open;
begin
  ADOConnection1.Open;
end;


// ---------------------------------------------------------------
function TdmMain.GetDirs_(aProjectID: Integer=-1): TdmMainDirsRec;
// ---------------------------------------------------------------
var
  sDir: string;
  sPath: string;
begin
  if aProjectID=-1 then
    aProjectID := ProjectID;


  sPath := Format('%s.%s.%d\', [LoginRec.Server, LoginRec.Database, aProjectID]);


  sDir := GetLocalDir(ProjectID)+ sPath;

 //  sDir := 'p:\_����_1\';


//  if sProjectCalcDir = '' then
  // sDir:= GetCalcDir (aProjectID) + sPath ;
//  else
//    sRootPath:= sProjectCalcDir  + sPath;

 // sProjectCalcDir:= sRootPath + 'shared\';

  Result.Dir:=sDir ;

  Result.ProjectCalcDir:=sDir + 'Predictions\';
  Result.ProjectMapDir :=sDir + 'Maps_\';

  //; sRootPath + 'MAPS_link_v2008_11\';

 // Result := ;
end;


// ---------------------------------------------------------------
function TdmMain.Open_ADOConnection(aConnectionStr: string): Boolean;
// ---------------------------------------------------------------
var
  oSList: TStringList;
begin


  ADOConnection1.Close;
  ADOConnection1.ConnectionString:=aConnectionStr;

  oSList:=TStringList.Create;

  //#13
  oSList.Text:= StringReplace(aConnectionStr, ';', #13, [rfReplaceAll]);;

  LoginRec.Server  :=oSList.Values ['Data Source'];
  LoginRec.DataBase:=oSList.Values['Initial Catalog'];

//  LoginRec.DataBase:=oSList.Values['User ID'];

  //ConnectionString=Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;
  //Initial Catalog=onega_link_mts;Data Source=SERVER1;
  //Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;
  //Workstation ID=ALEX;Use Encryption for Data=False;Tag with column collation when possible=False;;Password=sa


  FreeAndNil(oSList);


  try
    ADOConnection1.Open;

    Result:=True;
  except
    Result:=False;
  end;
end;

// ---------------------------------------------------------------
procedure TdmMain.SetADOConnectionObject(aConnectionObject: _Connection);
// ---------------------------------------------------------------
begin
  Assert(not Assigned(FConnectionObject_old), 'Value not assigned');

  FConnectionObject_old := ADOConnection1.ConnectionObject;
  ADOConnection1.ConnectionObject := aConnectionObject;
end;


// ---------------------------------------------------------------
procedure TdmMain.RestoreADOConnectionObject;
// ---------------------------------------------------------------
begin
  Assert(Assigned(FConnectionObject_old), 'Value not assigned');

//  if Assigned(FConnectionObject_old) then
 // begin
  ADOConnection1.ConnectionObject := FConnectionObject_old;

/////////////  FConnectionObject_old:=nil;
 // end;

end;


var
  sDir: string;
begin
  sDir:=IncludeTrailingBackslash(ExtractFileDir(Application.ExeName));

  g_IsDebug:= ini_ReadBool (sDir+'_config.ini', 'debug', 'enable', false);

 // g_IsDebug := True;

//  ini_WriteBool (sDir+'_config.ini', 'debug', 'enable', True);

end.


