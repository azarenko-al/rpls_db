unit dm_App;

interface

uses
  SysUtils, Classes, Dialogs, Windows,

  u_files,

  u_vars,

  u_log,

  u_ini_LinkCalcParams,

  dm_Link_Calc,
  

  dm_Main

  ;

type
  TdmApp_link_calc = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);

  end;

var
  dmApp_link_calc: TdmApp_link_calc;

//==============================================================
implementation {$R *.DFM}



//--------------------------------------------------------------
procedure TdmApp_link_calc.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const
  DEF_SOFTWARE_NAME= 'RPLS_DB_LINK';
  DEF_TEMP_FILENAME = 'link_calc.ini';

var
  bHasResult: Boolean;
  sIniFileName: string;
//  oParams: TIni_Link_Calc_Params;

begin
    //g_ApplicationDataDir


  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
//    sIniFileName:=TEMP_INI_FILE;

//    sIniFileName:=GetCommonApplicationDataDir_Local_AppData(DEF_SOFTWARE_NAME)+
    sIniFileName:=g_ApplicationDataDir +
                  DEF_TEMP_FILENAME;


  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;

 // g_Log:=TLog.Create();
//    IncludeTrailingBackslash(GetTempFileDir())+'_rpls_log.txt');


//  oParams:=TIni_Link_Calc_Params.Create;
//  oParams.LoadFromFile(sIniFileName);


//  if not oParams.Validate then
//    Exit;

(*
  if (iLinkID=0) or
     (iProjectID=0)
  then begin
    ShowMessage('(iLinkID=0) or (iProjectID=0) ');
    Exit;
  end;*)


//  ConnectionString

  TdmMain.Init;
 // if not dmMain.OpenDB_dlg then
  if not dmMain.OpenDB_reg then
    Exit;


  {$DEFINE use_dll111}

  {$IFDEF use_dll}
  {
  if Load_ILink_calc() then
  begin
    ILink_calc.InitADO(dmMain.ADOConnection1.ConnectionObject);
 //   ILink_graph.InitAppHandle(Application.Handle);
    ILink_calc.Execute(sIniFileName);

    UnLoad_ILink_calc();
  end;

  Exit;
   }
  {$ENDIF}


////////////
 // TdmRel_Engine.Init;

//  dmMain.ProjectID:=oParams.ProjectID;

  TdmLink_Calc.Init;
//////  dmLink_Calc.Params1.ADOConnection:=dmMain.ADOConnection;

 // db_SetComponentADOConnection (dmLink_Calc, dmMain.ADOConnection);



  dmLink_Calc.Params.LoadFromFile(sIniFileName);

//  dmLink_Calc.Params.I



  dmMain.ProjectID:=dmLink_Calc.Params.ProjectID;

  if dmLink_Calc.Params.LinkID>0 then
    dmLink_Calc.Params.IDList.AddID(dmLink_Calc.Params.LinkID);


   assert(dmLink_Calc.Params.IDList.Count>0, 'dmLink_Calc.Params.IDList.Count>0');


  if dmLink_Calc.Params.Validate then
 //   dmLink_Calc.Execute (dmLink_Calc.Params.LinkID, nil);
//    dmLink_Calc.ExecuteList1 (dmLink_Calc.Params.LinkID, nil);
    dmLink_Calc.ExecuteList1 (dmLink_Calc.Params.IDList);

(*
  dmLink_Calc.Params.IsUsePassiveElements:=oParams.UsePassiveElements;
  dmLink_Calc.Params.IsCalcWithAdditionalRain:=oParams.IsCalcWithAdditionalRain;
  dmLink_Calc.Params.IsSaveReportToDB:=oParams.IsSaveReportToDB;
*)


(*ShowMessage('107');
  dmLink_Calc.Execute (107);
*)
  bHasResult:=dmLink_Calc.Params.Results.Enabled;


  FreeAndNil(dmLink_Calc);


  if bHasResult then
    ExitProcess(1)
  else
    ExitProcess(0);


//  FreeAndNil(oParams);

end;





end.

{


{
  if Tdlg_Property_And_Link_Import.ExecDlg (iProjectID) then
    ExitProcess(1)
  else
    ExitProcess(0);
}
