unit dm_Link_calc;

interface
{$DEFINE test}

uses
  SysUtils, Classes, DB, ADODB, Forms,  Variants, Math, Dialogs,  StrUtils,
  XMLIntf,

//  u_Opti_classes,




  u_opti_class_new,



  d_progress,

  f_Main_opti,

//  d_Message,


  u_classes_Adaptive,

  u_dlg,

 // u_Profiler,

  u_LinkCalcResult_new,

  u_link_model_layout,

  u_Ini_rpls_rrl_params,

  u_str,
  u_xml_document,

  d_AdaptiveModulation,

  u_link_calc_classes_main,

  u_link_calc_classes_export,

  u_db_manager,

  dm_Onega_DB_data,
  u_vars,

  u_rrl_param_rec_new, //shared

  u_rel_Profile,

  u_Link_const,

  u_const_db,
  u_const,

  u_classes,
  u_db,
  u_files,
  u_func,
  u_Geo,
  u_run,

  u_Link_CalcRains,
  
  dm_Main,

  dm_Link_Calc_SESR,
  dm_Rel_Engine,

  u_ini_LinkCalcParams, dxmdaset;

  {
  ����� ����, ������-�� � rrl.xml ������ ������ ����������� � ���������� �������! � ���������, ������ � ���������� ��� � 42 ����� ���:
  < param id =" 42 " value =" 0 " comment =" �������� ����������� " /> � ������ ���� ��������� ���:
  < param id =" 42 " value =" 0 " comment =" �������� ����������� ����� ���.������ (0- min max (H1,H2); 1- min H1+H2) " />

  }

type

  TdmLink_calc = class(TDataModule)
    qry_LinkEnd1: TADOQuery;
    qry_Antennas1: TADOQuery;
    qry_LinkEnd2: TADOQuery;
    qry_Antennas2: TADOQuery;
    ADOStoredProc_Link: TADOStoredProc;
    qry_Modes: TADOQuery;
    q_LInk_repeater: TADOQuery;
    ds_LInk_repeater: TDataSource;
    q_LInk_repeater_ant: TADOQuery;
    ds_LInk_repeater_ant: TDataSource;
    ds_LinkEnd1: TDataSource;
    ds_LinkEnd2: TDataSource;
    ds_Antennas1: TDataSource;
    ds_Antennas2: TDataSource;
    dxMemData_Groups: TdxMemData;
    dxMemData_Items: TdxMemData;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FDBManager: TDBManager;

    FDBLink: TDBLink;

    FLog: TStringList;

    FAdaptiveModeArr: array of TAdaptiveModeRec;


    procedure CalcRains(aDBLink: TDBLink; aLinkEndID1: integer; aLinkCalcResults:
        TLinkCalcResult);

    function Execute(aDBLink: TDBLink): Boolean;

    function Execute_AdaptiveModulation(aDBLink: TDBLink): boolean;
    function Execute_Optimize_Equioment(aDBLink: TDBLink): boolean;

    procedure Execute_Optimize_Equioment_progress;

    function Execute_Repeater(aDBLink: TDBLink): boolean;
    procedure Profile_Open(aDBLink: TDBLink);
    procedure Profile_Open_repeater(aDBLink: TDBLink);

    function Run_rpls_rrl(aFile1, aFile2, aFile_layout: string): Boolean;

    procedure SaveReflectionPointsToDB(aDBLink: TDBLink; aLinkCalcResults:
        TLinkCalcResult; aLINK_INDEX: Integer = 0);
  public
    LinkList: TDBLinkList;

  public
//    Link: TDBLink;

    Params: TIni_Link_Calc_Params;

    LinkCalcResults: TLinkCalcResult;
    LinkCalcResults_back: TLinkCalcResult;

  //  LinkCalcResult_loader: TLinkCalcResult_loader;

    procedure SaveCalcResultsToDB(aDBLink: TDBLink; aLinkCalcResults,
        aLinkCalcResults_back: TLinkCalcResult);

    procedure ExecuteList1(aIDList: TIDList);


    class procedure Init;
    
    procedure OpenData_new(aID: Integer; aLink: TDBLink);

//    procedure SaveToClass11(aCalcParam: TrrlCalcParam);

  end;


var
  dmLink_calc: TdmLink_calc;


//==================================================================
implementation
 {$R *.dfm}


class procedure TdmLink_calc.Init;
begin
  if not Assigned(dmLink_calc) then
    dmLink_calc := TdmLink_calc.Create(Application);

end;


//--------------------------------------------------------------------
procedure TdmLink_calc.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  LinkCalcResults      := TLinkCalcResult.Create();
  LinkCalcResults_back := TLinkCalcResult.Create();

  db_SetComponentADOConnection(Self, dmMain.AdoConnection);

  Params := TIni_Link_Calc_Params.Create();

  FDBManager:=TDBManager.Create (dmmain.ADOConnection);

  LinkList:=TDBLinkList.Create;

  FLog:= TStringList.Create;

end;

// ---------------------------------------------------------------
procedure TdmLink_calc.DataModuleDestroy(Sender: TObject);
// ---------------------------------------------------------------
begin
  FreeAndNil(LinkList);

  FreeAndNil(LinkCalcResults);
  FreeAndNil(LinkCalcResults_back);


  FreeAndNil(FDBManager);

  FreeAndNil(Params);

  inherited;
end;


//--------------------------------------------------------------------
procedure TdmLink_calc.OpenData_new(aID: Integer; aLink: TDBLink);
//--------------------------------------------------------------------
const
  SQL_SELECT_LINKEND_ANT1 = //band,
     'SELECT * FROM '+ TBL_LINKEND_ANTENNA +
     ' WHERE linkend_id=:linkend_id '+
     ' ORDER BY IsNull(is_master,0) desc, height desc';
   //  ' ORDER BY is_master desc, height desc';

//


 // SQL_SELECT_LINKEND=
  //   'SELECT * FROM ' + TBL_LinkEnd + ' WHERE id=:id ';

  SQL_SELECT_LINKEND=
    'SELECT L.*, LinkEndType.band AS LinkEndType_band '+
    ' FROM  LinkEnd L LEFT OUTER JOIN LinkEndType ON L.linkendtype_id = dbo.LinkEndType.id ' +
    ' WHERE L.id=:id ';



  {
  view_PMP_sector_for_Calc = 'view_PMP_sector_for_Calc';


  SQL_SELECT_PMP_SECTOR=
      'SELECT * FROM ' + view_PMP_sector_for_Calc + ' WHERE id=:id ';
//      'SELECT * FROM ' + TBL_PMP_Sector + ' WHERE id=:id ';


           //   view_PmpTerminal_for_Calc

  view_Pmp_Terminal_for_Calc = 'view_Pmp_Terminal_for_Calc';


  SQL_SELECT_PMP_TERMINAL=
     'SELECT *  FROM ' + view_Pmp_Terminal_for_Calc + ' WHERE id=:id ';
//     'SELECT *  FROM ' + TBL_PmpTerminal + ' WHERE id=:id ';
 }

   view_Linkend_Antenna_for_calc = 'view_Linkend_Antenna_for_calc';


  SQL_SELECT_LINKEND_ANT = //band,
     'SELECT * FROM '+ view_Linkend_Antenna_for_calc +
     ' WHERE linkend_id=:linkend_id '+
     ' ORDER BY IsNull(is_master,0) desc, height desc';

{
  SQL_SELECT_PMP_SECTOR_ANT =
     'SELECT * FROM '+ view_Linkend_Antenna_for_calc +
     ' WHERE PMP_SECTOR_id=:PMP_SECTOR_id '+
     ' ORDER BY height desc';

  SQL_SELECT_PMP_TERMINAL_ANT =
     'SELECT * FROM '+ view_Linkend_Antenna_for_calc +
//     'SELECT * FROM '+ view_PMP_TERMINAL_ANTENNAS +
     ' WHERE PMP_TERMINAL_id=:PMP_TERMINAL_id ORDER BY height desc';

}

  // for AM
  SQL_SELECT_LINKENDType_MODES =
     'SELECT * FROM ' + TBL_LinkEndType_Mode +
     ' WHERE LINKENDType_id=:id '+
     ' and bitrate_Mbps >= :bitrate_Mbps ' +
     ' ORDER by bitrate_Mbps';


var
  I: Integer;
//  iPmpSectorID,iPmpTerminalID,
  iLinkEndID1,iLinkEndID2: integer;
  dFreq,f1,f2: double;
  iLink_repeater_ID: Integer;
  
begin
  //Assert(aID>0);

//  g_Profiler.Start('function TdmLink_calc.OpenData');


 // FLinkID:=aID;


  Assert(Assigned(dmOnega_DB_data), 'Value not assigned');

  dmOnega_DB_data.Link_Select(ADOStoredProc_Link, aID);
  Assert(ADOStoredProc_Link.recordCount=1, 'ADOStoredProc_Link.recordCount=1');

//}  db_View( ADOStoredProc_Link);


//  if not dmLink.GetInfoFromDataset (ADOStoredProc_Link, FLinkParamRec) then
//  if not dmLink.GetInfo (aID, FLinkParamRec) then
 //   exit;                                               
//
  // Added by Aleksey 21.07.2009 18:38:48

   aLink.LoadFromDataset(ADOStoredProc_Link);


   iLinkEndID1:=aLink.LinkEnd1_ID;
   iLinkEndID2:=aLink.LinkEnd2_ID;


   assert (iLinkEndID1>0);

   db_OpenQuery_ (qry_LinkEnd1, SQL_SELECT_LINKEND, [FLD_ID, iLinkEndID1]);
   db_OpenQuery_ (qry_LinkEnd2, SQL_SELECT_LINKEND, [FLD_ID, iLinkEndID2]);


   db_OpenQuery_ (qry_Antennas1, SQL_SELECT_LINKEND_ANT,
                [FLD_LINKEND_ID, iLinkEndID1]);


 //  db_View(qry_Antennas1);

//    tools_DatasetToClass(qry_Antennas1, 'd:\ant.pas');

   db_OpenQuery_ (qry_Antennas2, SQL_SELECT_LINKEND_ANT,
                [FLD_LINKEND_ID, iLinkEndID2]);


 // db_View(qry_Antennas2);


   if qry_LinkEnd1.IsEmpty or (qry_LinkEnd2.IsEmpty) then
     raise Exception.Create ('qry_LinkEnd1.IsEmpty or qry_LinkEnd2.IsEmpty');



//  g_Profiler.Stop('function TdmLink_calc.OpenData');



 ///// db_View(qry_LinkEnd2);


//  db_View(qry_LinkEnd1);
//  db_View(qry_LinkEnd2);


  aLink.LinkEnd1.LoadFromDataset(qry_LinkEnd1);
  aLink.LinkEnd2.LoadFromDataset(qry_LinkEnd2);




  aLink.LinkEnd1.Antennas.LoadFromDataset(qry_Antennas1);
  aLink.LinkEnd2.Antennas.LoadFromDataset(qry_Antennas2);

//  Result:= aLink.Validate_Critical(nil);
 // Result:=Result and aLink.Validate_Bands(nil);

//  if not Result then
 //   Exit;


  //LinkRepeater
  if aLink.LinkRepeater.ID> 0 then
  begin

    dmOnega_DB_data.OpenQuery(q_LInk_repeater,
      Format('SELECT * FROM %s WHERE link_id=%d',
           [view_LINK_REPEATER, aLink.ID ]));

    Assert(q_LInk_repeater.RecordCount>0);

    aLink.LinkRepeater_LoadFromDataset(q_LInk_repeater);
//    Link.LinkRepeater_Restore;

   //  db_View(q_LInk_repeater);

 //  iID:=q_LInk_repeater.FieldByName(FLD_id).AsInteger;

    dmOnega_DB_data.OpenQuery(q_LInk_repeater_ant,
      Format('SELECT * FROM %s WHERE Link_repeater_id=%d',
           [VIEW_LINK_REPEATER_ANTENNA, aLink.LinkRepeater.ID]));


    aLink.LinkRepeater.Antennas.LoadFromDataset(q_LInk_repeater_ant);

  end;


 ///////// ShowMessage('test - Params.IsUseAdaptiveModulation');

//  Params.IsUseAdaptiveModulation := True;



  //-------------------------------------------------------------------
  // IsUseAdaptiveModulation
  //-------------------------------------------------------------------
  if Params.Mode = mtAdaptiveModulation then
  begin

  //min_
{
    db_OpenQuery (qry_Modes, SQL_SELECT_LINKENDType_MODES,
         [
           db_Par(FLD_ID, aLink.LinkEnd1.LinkEndType_ID),
         ]);
}
    db_OpenQuery_ (qry_Modes, SQL_SELECT_LINKENDType_MODES,
         [
           FLD_ID,           aLink.LinkEnd1.LinkEndType_ID,
           FLD_BitRate_Mbps, Params.Bitrate_min
         ]);


    Setlength(FAdaptiveModeArr, qry_Modes.RecordCount);
  //  Setlength(FAdaptiveModeArr, 2);

    for I := 0 to qry_Modes.RecordCount - 1 do
  //  for I := 0 to 1 do
    begin
      FAdaptiveModeArr[i].LinkEndType_ID  :=qry_Modes.FieldByName(FLD_LinkEndType_ID).AsInteger;


      FAdaptiveModeArr[i].id   :=qry_Modes.FieldByName(FLD_id).AsInteger;
      FAdaptiveModeArr[i].Mode :=qry_Modes.FieldByName(FLD_Mode).AsInteger;

      FAdaptiveModeArr[i].THRESHOLD_BER_3 :=qry_Modes.FieldByName(FLD_THRESHOLD_BER_3).AsFloat;
      FAdaptiveModeArr[i].THRESHOLD_BER_6 :=qry_Modes.FieldByName(FLD_THRESHOLD_BER_6).AsFloat;

      FAdaptiveModeArr[i].Power_dBm       :=qry_Modes.FieldByName(FLD_Power_max).AsFloat;
      FAdaptiveModeArr[i].BitRate_Mbps    :=qry_Modes.FieldByName(FLD_BitRate_Mbps).AsFloat;

      FAdaptiveModeArr[i].Modulation_type :=qry_Modes.FieldByName(FLD_Modulation_type).AsString;

      FAdaptiveModeArr[i].bandwidth :=qry_Modes.FieldByName(FLD_bandwidth).AsFloat;

      qry_Modes.Next;
    end;

  end;


//  db_View(qry_Modes);

  if aLink.LinkRepeater.ID =0  then
    Profile_Open(aLink)
  else
    Profile_Open_repeater(aLink);

end;




//-------------------------------------------------------------------
procedure TdmLink_calc.Profile_Open(aDBLink: TDBLink);
//-------------------------------------------------------------------
var
  d: double;
  iProfileStep_m: Integer;
  sProfile_XML: string;
  sTempFile: string;

  blVector: TBLVector;
  eLen_km: Double;
  iRes: Integer;
begin
//  dmRel_Engine.AssignClutterModel (aDBLink.ClutterModel_ID, FrelProfile.Clutters);
  dmRel_Engine.AssignClutterModel (aDBLink.ClutterModel_ID, aDBLink.relProfile.Clutters);


  sProfile_XML:=aDBLink.Profile_XML;


  aDBLink.IsProfileExists:=True;

 ////// sProfile_XML:='';

  iProfileStep_m:=aDBLink.Profile_Step_m;

  eLen_km:= geo_Distance_KM(aDBLink.BLVector);


//      blVector:= Link.BLVector;


//function TrelProfile.LoadFromXml(aValue: string; aLength_km: double): Boolean;

  if (sProfile_XML='') or
     (not aDBLink.relProfile.LoadFromXml(sProfile_XML, eLen_km)) then
  begin

    blVector:= aDBLink.BLVector;

    dmRel_Engine.Open;// OpenByVector (blVector);

    aDBLink.IsProfileExists:=
      dmRel_Engine.BuildProfile1 (aDBLink.relProfile, blVector, iProfileStep_m);//, FClutterModelID);


  //  FIsProfileExists:=True;
//
//    if FIsProfileExists then
//    begin
//    end;

  end else
  begin
//    FrelProfile.isDirectionReversed := Link.is_profile_reversed;

  end;

  aDBLink.relProfile.isDirectionReversed := aDBLink.is_profile_reversed;

  //---------------------------------------------------------
  aDBLink.relProfile.SetRefraction(aDBLink.Refraction);
  aDBLink.relProfile.Data.Distance_KM := aDBLink.GetLength_km;

end;


//-------------------------------------------------------------------
procedure TdmLink_calc.Profile_Open_repeater(aDBLink: TDBLink);
//-------------------------------------------------------------------
var
  d: double;
  iProfileStep_m: Integer;
 // sProfile_XML: string;
  sTempFile: string;

  blVector: TBLVector;
  eLen_km: Double;
  iRes: Integer;
  sProfile1_XML: string;
  sProfile2_XML: string;
begin
  dmRel_Engine.AssignClutterModel (aDBLink.ClutterModel_ID, aDBLink.relProfile.Clutters);


  sProfile1_XML:=aDBLink.LinkRepeater.Part1.Profile_XML;
  sProfile2_XML:=aDBLink.LinkRepeater.Part2.Profile_XML;

  aDBLink.IsProfileExists1:=False;
  aDBLink.IsProfileExists2:=False;

  iProfileStep_m:=aDBLink.Profile_Step_m;


//  dmRel_Engine.OpenByVector (aDBLink.BLVector);
  dmRel_Engine.Open;

  eLen_km:= geo_Distance_KM(aDBLink.LinkRepeater.Part1.BLVector);

  // -------------------------
  if (sProfile1_XML='') or (not aDBLink.relProfile1.LoadFromXml(sProfile1_XML, eLen_km)) then
  begin

    blVector:= aDBLink.LinkRepeater.Part1.BLVector;

  //  dmRel_Engine.OpenByVector (blVector);

    aDBLink.IsProfileExists1:= dmRel_Engine.BuildProfile1 (aDBLink.relProfile1, blVector, iProfileStep_m);//, FClutterModelID);


  end;

  //---------------------------------------------------------
  aDBLink.relProfile1.SetRefraction(aDBLink.Refraction);
  aDBLink.relProfile1.Data.Distance_KM := aDBLink.LinkRepeater.Part1.Length_KM;

  eLen_km:= geo_Distance_KM(aDBLink.LinkRepeater.Part2.BLVector);


  // -------------------------
  if (sProfile2_XML='') or (not aDBLink.relProfile2.LoadFromXml(sProfile2_XML, eLen_km)) then
  begin

    blVector:= aDBLink.LinkRepeater.Part2.BLVector;

//    dmRel_Engine.OpenByVector (blVector);

    aDBLink.IsProfileExists2:= dmRel_Engine.BuildProfile1 (aDBLink.relProfile2, blVector, iProfileStep_m);//, FClutterModelID);


  end;

  //---------------------------------------------------------
  aDBLink.relProfile2.SetRefraction(aDBLink.Refraction);
  aDBLink.relProfile2.Data.Distance_KM := aDBLink.LinkRepeater.Part2.Length_KM;


//  aDBLink.relProfile1.isDirectionReversed := aDBLink.is_profile_reversed;
//  aDBLink.relProfile2.isDirectionReversed := aDBLink.is_profile_reversed;

end;

//-------------------------------------------------------------------
procedure TdmLink_calc.SaveReflectionPointsToDB(aDBLink: TDBLink;
    aLinkCalcResults: TLinkCalcResult; aLINK_INDEX: Integer = 0);
//-------------------------------------------------------------------
var
  i: integer;
  oReflectionPoint: TReflectionPoint;

begin
  if not aDBLink.LinkRepeater.ID=0 then
    Assert (aLINK_INDEX = 0);

  if aDBLink.LinkRepeater.ID>0 then
    Assert (aLINK_INDEX > 0);


    i:=aLinkCalcResults.ReflectionPoints.Count;


    for I := 0 to aLinkCalcResults.ReflectionPoints.Count - 1 do
//        for I := 0 to Link.ReflectionPoints.Count - 1 do
    begin
      oReflectionPoint:=aLinkCalcResults.ReflectionPoints[i];

      FDBManager.AddRecord (TBL_Link_reflection_points,
           [
            db_Par (FLD_LINK_ID,   aDBLink.ID),
//            db_Par (FLD_LINK_ID,   aLink_ID),
            //                          //TruncFloat(


            db_Par (FLD_DISTANCE_to_center_km,    oReflectionPoint.DISTANCE_to_center_km),
            db_Par (FLD_ABS_PROSVET, oReflectionPoint.ABS_PROSVET_m),  //A��������� ������� ��� �������� 1-�� ������� �����. [m]
            //                            //����������� ������� ��� �������� 1-�� �������
            //                            �����������.������� ��� ��������
            db_Par (FLD_LENGTH_KM,      oReflectionPoint.Length_km) , //������������� 1-�� �������
            db_Par (FLD_RADIUS_KM,      oReflectionPoint.RADIUS_km ),   //P����� �������� 1-�� �������

/////////
            db_Par (FLD_Signal_Depression_dB, oReflectionPoint.Signal_Depression_dB ),   //���������� �� 1-�� ������� ��������� [dB]

            db_Par (FLD_LINK_INDEX, IIF(aLINK_INDEX>0, aLINK_INDEX, NULL))
           ]);
    end;
end;




//--------------------------------------------------------------------
procedure TdmLink_calc.SaveCalcResultsToDB(aDBLink: TDBLink; aLinkCalcResults,
    aLinkCalcResults_back: TLinkCalcResult);
//--------------------------------------------------------------------

  // ---------------------------------------------------------------

var
  iStatus,iValue,iID,i,j,id_parent: integer;
  d: Double;
  e: Double;
  d1: Double;
  d2: Double;
  d3: Double;
  d4: Double;
  dLos: Double;

  dThresholdBer6,dThresholdBer3: double;

  dValue: double;
  iLinkEndID1, iLinkEndID2: integer;
//  dKng_LinkEnd1, dKng_LinkEnd2: double;

  iGST_Type: integer;

  rec: TCalcEsrBberParamRec;

  FCalcResults_Rec: TdmLink_calc_CalcResults_rec1;
  iRes: Integer;

  s: string;
  sCalcResultsXML: string;
  vStatus_back: Variant;


begin
  Assert(aDBLink.ID>0);

//  LinkCalcResults.Load

 // Assert(Assigned(gl_DB), 'Value not assigned');

  dmOnega_DB_data.Link_ClearCalc(aDBLink.ID);


 // LoadCalcResultsFromXML_ToRec(aFileName);

//  DoSaveReflectionPointsToDB();
//////  SaveReflectionPointsToDB(aDBLink, aLinkCalcResults);


//  if Params.IsSaveReportToDB then
////////////////////  DoSaveRecToDB();


  FillChar(FCalcResults_Rec,SizeOf(FCalcResults_Rec),-1);


 //��������� ������� ������� �� ����� ��������� (���)
//  FCalcResults_Rec.Rx_Level_dBm       := aLinkCalcResults.Data.Rx_Level_dBm;
  FCalcResults_Rec.Rx_Level_dBm       := RoundFloat (aLinkCalcResults.Data.Rx_Level_dBm, 2);


  FCalcResults_Rec.SESR               := aLinkCalcResults.Data.SESR;
  FCalcResults_Rec.Kng                := aLinkCalcResults.Data.Kng;
  FCalcResults_Rec.Sesr_Req           := aLinkCalcResults.Data.Sesr_Req;
  FCalcResults_Rec.KNG_Req            := aLinkCalcResults.Data.KNG_Req;

  FCalcResults_Rec.MARGIN_HEIGHT_m    := aLinkCalcResults.Data.MARGIN_HEIGHT_m;
  FCalcResults_Rec.MARGIN_DISTANCE_km := aLinkCalcResults.Data.MARGIN_DISTANCE_km;

  FCalcResults_Rec.SESR_SUBREFRACTION1:= aLinkCalcResults.Data.SESR_SUBREFRACTION;
  FCalcResults_Rec.SESR_RAIN          := aLinkCalcResults.Data.SESR_RAIN;
  FCalcResults_Rec.SESR_INTERFERENCE  := aLinkCalcResults.Data.SESR_INTERFERENCE;

  FCalcResults_Rec.KNG_SUBREFRACTION  := aLinkCalcResults.Data.KNG_SUBREFRACTION;
  FCalcResults_Rec.KNG_RAIN           := aLinkCalcResults.Data.KNG_RAIN;
  FCalcResults_Rec.KNG_INTERFERENCE   := aLinkCalcResults.Data.KNG_INTERFERENCE;


  FCalcResults_Rec.Status             := aLinkCalcResults.Data.Status;
  FCalcResults_Rec.Los_Status         := aLinkCalcResults.Data.Los_Status;

//  FCalc_Length_KM :=aDBLink.Length_km;

 //----------------------------------------------------------------------
  //    ����� �� ���������


  dThresholdBer6:= aDBLink.Linkend2.THRESHOLD_BER_6;
  dThresholdBer3:= aDBLink.Linkend2.THRESHOLD_BER_3;


{
  case aDBLink.LinkType of

    ltLink_:
    begin
      dThresholdBer6:= aDBLink.Linkend2.THRESHOLD_BER_6;
      dThresholdBer3:= aDBLink.Linkend2.THRESHOLD_BER_3;
    end;

    ltPmpLink_:
    begin
      dThresholdBer6:= aDBLink.Linkend2.THRESHOLD_BER_6;
      dThresholdBer3:= aDBLink.Linkend2.THRESHOLD_BER_3;

    end;
  end;
 }

  //����� �� ��������� [dB]
  case aDBLink.BER_REQUIRED_Index of
    0: FCalcResults_Rec.fade_margin_dB:=FCalcResults_Rec.Rx_Level_dBm - dThresholdBer3;
    1: FCalcResults_Rec.fade_margin_dB:=FCalcResults_Rec.Rx_Level_dBm - dThresholdBer6;
  end;
  //----------------------------------------------------------------------
 // zz    FieldByName('').

  iGST_Type:=aDBLink.Trb.GST_TYPE;

  if (aDBLink.GetLength_km > 0) then
//  if (FCalc_Length_KM > 0) then
  begin
    FillChar(rec,SizeOf(rec),0);

    rec.ID       :=aDBLink.ID;
    rec.ObjName  :='LINK';
//    rec.Length_KM:=FCalc_Length_KM;
    rec.Length_KM:=aDBLink.GetLength_km;

    rec.GST_Type  :=iGST_Type;
    rec.Bitrate_Mbps:=aDBLink.LinkEnd1.Bitrate_Mbps;

//    rec.Bitrate_Mbps:=Link.Bitrate_Mbps;

    dmLink_Calc_SESR.CalcEsrBber11 (rec);

  e:=aDBLink.ESR_norm;
  e:=aDBLink.ESR_required;
  e:=aDBLink.BBER_norm;
  e:=aDBLink.BBER_required;


   FCalcResults_Rec.ESR_norm     :=rec.Output.ESR_norm;
   FCalcResults_Rec.ESR_required :=rec.Output.ESR_Required;
   FCalcResults_Rec.BBER_norm    :=rec.Output.BBER_norm;
   FCalcResults_Rec.BBER_required:=rec.Output.BBER_Required;

  end;

      //    LinkCalcResults


//  db_UpdateRecord(ADOStoredProc_Link, [db_Par(FLD_profile_XML, sProfile_XML)]);

  sCalcResultsXML:=aLinkCalcResults.SaveToXMLString;



  if Assigned(aLinkCalcResults_back) then
    vStatus_back:=IIF(Params.IsRunBothSides, aLinkCalcResults_back.Data.STATUS, null)
  else
    vStatus_back:=null;


//  e:=Link.NIIR1998.water_area_percent;

///aLinkCalcResults_back
//Params.IsRunBothSides

  iRes:=dmOnega_DB_data.ExecStoredProc_(sp_Link_Update,
          [
//           db_Par(FLD_ID,                  aLink_ID),
           FLD_ID,                  aDBLink.ID,

           FLD_Rx_Level_dBm,        FCalcResults_Rec.Rx_Level_dBm,

           FLD_SESR,                FCalcResults_Rec.SESR,
           FLD_KNG,                 FCalcResults_Rec.Kng,

           FLD_SESR_NORM,           FCalcResults_Rec.Sesr_Req,
           FLD_KNG_NORM,            FCalcResults_Rec.KNG_Req,

           FLD_ESR_NORM,            FCalcResults_Rec.ESR_norm,
           FLD_ESR_REQUIRED,        FCalcResults_Rec.ESR_required,
           FLD_BBER_NORM,           FCalcResults_Rec.BBER_norm,
           FLD_BBER_REQUIRED,       FCalcResults_Rec.BBER_required,

           //����� �� ��������� [dB]
           FLD_FADE_MARGIN_DB,      FCalcResults_Rec.Fade_margin_dB,

           FLD_MARGIN_HEIGHT,       FCalcResults_Rec.MARGIN_HEIGHT_m,   //  GetCalcParamID(32),//,  '����������� ������������� ������� [m]');
           FLD_MARGIN_DISTANCE,     FCalcResults_Rec.MARGIN_DISTANCE_km,//  GetCalcParamID(29),//,  '��������� ����� �������� [�m]');

           FLD_SESR_SUBREFRACTION,  FCalcResults_Rec.SESR_SUBREFRACTION1,//  GetCalcParamID(146),//,, 'SESR �������������� ������������ [%]');
           FLD_SESR_RAIN,           FCalcResults_Rec.SESR_RAIN,         //  GetCalcParamID(147),//, �������� ������������ [%]');
           FLD_SESR_INTERFERENCE,   FCalcResults_Rec.SESR_INTERFERENCE, //  GetCalcParamID(148), //, 'SESR ����������������� ������������ [%]');

           FLD_KNG_SUBREFRACTION,   FCalcResults_Rec.KNG_SUBREFRACTION,//  GetCalcParamID(146),//,, 'SESR �������������� ������������ [%]');
           FLD_KNG_RAIN,            FCalcResults_Rec.KNG_RAIN,         //  GetCalcParamID(147),//, �������� ������������ [%]');
           FLD_KNG_INTERFERENCE,    FCalcResults_Rec.KNG_INTERFERENCE, //  GetCalcParamID(148), //, 'SESR ����������������� ������������ [%]');


           FLD_STATUS,              FCalcResults_Rec.STATUS,
           FLD_STATUS_back,         vStatus_back, // IIF(Params.IsRunBothSides, aLinkCalcResults_back.Data.STATUS, null),

           FLD_LOS_STATUS,          FCalcResults_Rec.LOS_STATUS,

           FLD_NIIR1998_water_area_percent,  aDBLink.NIIR1998.water_area_percent,

           FLD_CALC_RESULTS_XML,    sCalcResultsXML

          ]);

   Assert (iRes>0, 'mOnega_DB_data.ExecStoredProc_(sp_Link_Update -  iRes>0');

  // �������������� ���� �������
//  FLink
//     Link.is

//  if Params.IsCalcWithAdditionalRain then
  if aDBLink.Is_CalcWithAdditionalRain then
//    CalcRains(aLink_ID,iLinkEndID1,  aLinkCalcResults);
    CalcRains(aDBLink, iLinkEndID1,  aLinkCalcResults);

end;


//--------------------------------------------------------------------
function TdmLink_calc.Execute(aDBLink: TDBLink): Boolean;
//--------------------------------------------------------------------
const
  TEMP_IN_FILENAME1  = 'rrl.xml';
  TEMP_IN_FILENAME2  = 'rrl2.xml';
  TEMP_IN_FILENAME3  = 'rrl3.xml';
  TEMP_IN_layout     = 'rrl_layout.txt';

//  TEMP_IN_FILENAME1_  = 'rrl_.xml';

  TEMP_OUT_FILENAME1 = 'rrl_result_1.xml';
  TEMP_OUT_FILENAME2 = 'rrl_result_2.xml';

  TEMP_OUT_FILENAME_all = 'rrl_result_all.xml';


var
  e: Double;
  k: Integer;
//  sFile1,sFile1_,sFile2: string;
  s: string;
  sErrStr: string;
//  sFile_2: string;
//  sRunPath: string;

  oCalcParams: TrrlCalcParam;
  sFile_in1: string;
  sFile_in2: string;
  sFile_layout: string;
  sFile_out1: string;
  sFile_out2: string;

  sPrefix: string;

  rec: TGet_Equivalent_Length_KM_rec;
  sFile_result_all: string;

begin
  Result:=False;
 // FLinkID:=aID;

//  if not OpenData_new (FLinkID, aDBLink) then
 //   exit;

 

   LinkCalcResults.Clear;

//  if not OpenData (FLinkID) then
//    Exit;

  if aDBLink.LinkRepeater.ID > 0 then
  begin
    Result := Execute_Repeater (aDBLink);
    Exit;
  end;


  if Params.Mode = mtAdaptiveModulation then
  begin
    Result := Execute_AdaptiveModulation (aDBLink);
    Exit;
  end;


  if Params.Mode = mtOptimize_equip then
  begin
    Result := Execute_Optimize_Equioment (aDBLink);
    Exit;
  end;




  CursorHourGlass;

  sPrefix:= Format('%d_', [aDBLink.ID]);


 //// sFile1:=GetTempFileNameWithExt('xml');

  sFile_in1 :=g_ApplicationDataDir + sPrefix + TEMP_IN_FILENAME1;
  sFile_in2 :=g_ApplicationDataDir + sPrefix + TEMP_IN_FILENAME2;

  sFile_layout :=g_ApplicationDataDir + TEMP_IN_layout;

  sFile_out1 :=g_ApplicationDataDir + sPrefix +  TEMP_OUT_FILENAME1;
  sFile_out2 :=g_ApplicationDataDir + sPrefix +  TEMP_OUT_FILENAME2;


  sFile_result_all:=g_ApplicationDataDir + TEMP_OUT_FILENAME_all;


 // SysUtils.DeleteFile(sFile2);

 // LInk.Runtime.IsUsePassiveElements := Params.IsUsePassiveElements;

//  if LInk.GST_equivalent_length_km=0 then
//    LInk.GST_equivalent_length_km  :=
//      dmLink_Calc_SESR.Get_Equivalent_Length_KM(ADOStoredProc_Link);

{
  aDBLink.Trb.GST_length_km1  :=
      dmLink_Calc_SESR.Get_Equivalent_Length_KM(ADOStoredProc_Link);
}

  FillChar(rec, SizeOf(rec), 0);
//  rec.Length_Km:=ADOStoredProc_Link

{
  rec.Length_Km     :=ADOStoredProc_Link.FieldByName(FLD_LENGTH).AsFloat/1000;
  rec.GST_length_km :=ADOStoredProc_Link.FieldByName(FLD_GST_LENGTH).AsFloat;
  rec.GST_Type      :=ADOStoredProc_Link.FieldByName(FLD_GST_TYPE).AsInteger;
}

  rec.Length_Km     :=aDBLink.GetLength_km(); //  ADOStoredProc_Link.FieldByName(FLD_LENGTH).AsFloat/1000;
  rec.GST_length_km :=aDBLink.Trb.GST_LENGTH_km_;  // �.. GetLength_km();//ADOStoredProc_Link.FieldByName(FLD_GST_LENGTH).AsFloat;
  rec.GST_Type      :=aDBLink.Trb.GST_Type;       //GetLength_km();//ADOStoredProc_Link.FieldByName(FLD_GST_TYPE).AsInteger;



  aDBLink.Trb.GST_LENGTH_km_  :=  dmLink_Calc_SESR.Get_Equivalent_Length_KM_rec(rec);


  // -----------------------------
  oCalcParams:=TrrlCalcParam.Create;


//  TDBLink_export.SaveToClass_(aDBLink, aDBLink.LinkEnd1, aDBLink.LinkEnd2, oCalcParams);


  TDBLink_export.SaveToClass(aDBLink, aDBLink.LinkEnd1, aDBLink.LinkEnd2,
      oCalcParams, aDBLink.relProfile, aDBLink.IsProfileExists);

//  SaveToClass(oCalcParams);

  if not oCalcParams.ValidateDlg then
    Exit;

  oCalcParams.SaveToXML (sFile_in1, True);


//  C:\ONEGA\RPLS_DB\bin\

  CalcMethod_LoadFromXMLFile(
//      GetApplicationDir + 'link_calc_model_setup\RRL_ParamSys.xml',
      GetApplicationDir + 'link_calc_model_setup\RRL_ParamUser.xml',
      oCalcParams.Calc_method,
      sFile_layout
      );


  //else
   // Exit;

  Run_rpls_rrl(sFile_in1, sFile_out1, sFile_layout);


  LinkCalcResults.Groups.Clear;


  if FileExists(sFile_out1) then
  begin
//    LinkCalcResults.Items.Clear;

//    LinkCalcResult_loader
    LinkCalcResults.LoadFromXmlFile_RRL_Result(sFile_out1);
  ////  LinkCalcResults.LoadFromXMLFile_RRL_XML(sFile_in1);


//    LinkCalcResults.Items.Clear;
//    LinkCalcResult_loader.LoadFromXMLFile_RRL_XML(sFile1);
//    LinkCalcResult_loader.LoadFromXmlFile_RRL_Result(sFile2);

  end;

  // ---------------------------------------------------------------
  // ---------------------------------------------------------------
  // ---------------------------------------------------------------
  // ---------------------------------------------------------------



 //  ShellExec_Notepad1(sFile_in1);


/////  Params.IsRunBothSides := False; //!!!!!!!

  Params.IsRunBothSides := True;

  if Params.IsRunBothSides then
  begin
//      TDBLink_export.SaveToClass_new(aDBLink, aDBLink.LinkEnd2, aDBLink.LinkEnd1,
//        oCalcParams);

//LinkCalcResults_back

      TDBLink_export.SaveToClass(aDBLink, aDBLink.LinkEnd2, aDBLink.LinkEnd1,
        oCalcParams, aDBLink.relProfile, aDBLink.IsProfileExists);

    //  SaveToClass(oCalcParams);

      if oCalcParams.ValidateDlg then
        oCalcParams.SaveToXML (sFile_in2, True)
      else
        Exit;

//      ShellExec_Notepad(sFile_in2);


      Run_rpls_rrl (sFile_in2, sFile_out2, sFile_layout);


      if FileExists(sFile_out2) then
      begin
      //  LinkCalcResults.LoadFromXMLFile_RRL_XML(sFile_in2, True);
        LinkCalcResults.LoadFromXmlFile_RRL_Result(sFile_out2, True);

        LinkCalcResults_back.LoadFromXmlFile_RRL_Result(sFile_out2);

        k:=LinkCalcResults_back.Data.STATUS;
        e:=LinkCalcResults_back.Data.Rx_Level_dBm;

      end;

  end;


  // ---------------------------------------------------------------
  // ---------------------------------------------------------------
  // ---------------------------------------------------------------
  // ---------------------------------------------------------------
  // ---------------------------------------------------------------


//  LinkCalcResults.SaveToDataset_DBTree(dxMemData1);
//  db_View(dxMemData1);



  SaveCalcResultsToDB (aDBLink, LinkCalcResults, LinkCalcResults_back);

  SaveReflectionPointsToDB(aDBLink, LinkCalcResults);

  LinkCalcResults.saveToXMLFile (sFile_result_all); //'c:\1.xml');


  CursorDefault;
  Result:=True;

  FreeAndNil(oCalcParams);


end;



//--------------------------------------------------------------------
function TdmLink_calc.Execute_Repeater(aDBLink: TDBLink): boolean;
//--------------------------------------------------------------------
var
  b: Boolean;
  dRxLevel2: Double;
  e: Double;
  eRx_Level_dBm1: Double;
  eRx_Level_delta: Double;
  iRes: Integer;
  k: Integer;
  sFile_in_1,sFile1_,sFile_in_2: string;
  s: string;
  sErrStr: string;
  sFile_2: string;


  oCalcParams1: TrrlCalcParam;
  oCalcParams2: TrrlCalcParam;


  oLinkCalcResults1: TLinkCalcResult;
  oLinkCalcResults2: TLinkCalcResult;
  oLinkCalcResults3: TLinkCalcResult;
  s1: string;
  s2: string;
  s3: string;

  sFile_in_3: string;
  sFile_layout: string;
  sFile_out_1: string;
  sFile_out_2: string;
  sFile_out_3: string;


  part1, part2: record

    RelProfile: TrelProfile;
    IsProfileExists: boolean;
    
    Antenna: TDBAntenna;

    GST_length_km : Double;
  end;


  oAntenna: TDBAntenna;

  rec: TGet_Equivalent_Length_KM_rec;

Label
  Exit_proc;

begin
  Params.IsRunBothSides := False; //!!!!!!!


  Result:=False;
//  FLinkID:=aID;


//  Assert(Assigned(Params1.ADOConnection), 'Value not assigned');

//  db_SetComponentADOConnection (Self, Params1.ADOConnection);



 // SetLength (FArr, 0);


  dmOnega_DB_data.Link_ClearCalc(aDBLink.ID);



  CursorHourGlass;



  sFile_in_1:=GetTempFileNameWithExt('xml');

  sFile_in_1 :=g_ApplicationDataDir + 'rrl_passive_1.xml';
  sFile_in_2 :=g_ApplicationDataDir + 'rrl_passive_2.xml';
  sFile_in_3 :=g_ApplicationDataDir + 'rrl_passive_3.xml';

  sFile_layout :=g_ApplicationDataDir + 'rrl_passive_layout.ini';



  sFile_out_1 :=g_ApplicationDataDir + 'rrl_passive_res1.xml';
  sFile_out_2 :=g_ApplicationDataDir + 'rrl_passive_res2.xml';
  sFile_out_3 :=g_ApplicationDataDir + 'rrl_passive_res3.xml';


  FillChar(rec, SizeOf(rec), 0);
//  rec.Length_Km:=ADOStoredProc_Link
 /// rec.Length_Km     := aDBLink.Length_km;//  ADOStoredProc_Link.FieldByName(FLD_LENGTH).AsFloat/1000;
  rec.GST_length_km := aDBLink.Trb.GST_LENGTH_km_;// Length_km;//    ADOStoredProc_Link.FieldByName(FLD_GST_LENGTH).AsFloat;
  rec.GST_Type      := aDBLink.Trb.GST_TYPE;//  ADOStoredProc_Link.FieldByName(FLD_GST_TYPE).AsInteger;


  rec.Length_Km := aDBLink.LinkRepeater.Part1.Length_KM;
  aDBLink.LinkRepeater.Part1.GST_length_km :=  dmLink_Calc_SESR.Get_Equivalent_Length_KM_rec(rec);

  rec.Length_Km := aDBLink.LinkRepeater.Part2.Length_KM;
  aDBLink.LinkRepeater.Part2.GST_length_km :=  dmLink_Calc_SESR.Get_Equivalent_Length_KM_rec(rec);



  //aDBLink.is_profile_reversed;



 ///// if LInk.Trb.GST_length_km1=0 then
 //   aDBLink.Trb.GST_length_km1  :=
//      dmLink_Calc_SESR.Get_Equivalent_Length_KM(ADOStoredProc_Link);


  oCalcParams1:=TrrlCalcParam.Create;
  oCalcParams2:=TrrlCalcParam.Create;
//  oCalcParams3:=TrrlCalcParam.Create;

  oLinkCalcResults1:=TLinkCalcResult.Create;
  oLinkCalcResults2:=TLinkCalcResult.Create;
  oLinkCalcResults3:=TLinkCalcResult.Create;



  // -------------------------
  // Site 1 -> Repeater
  // -------------------------
 // k:=aDBLink.relProfile1.Count;


//  aDBLink.Trb.GST_length_km1  := aDBLink.LinkRepeater.Part1.GST_length_km ;


//  TDBLink_export.SaveToClass(aDBLink, aDBLink.LinkEnd1, aDBLink.LinkEnd2,
//    oCalcParams1, aDBLink.relProfile1, aDBLink.IsProfileExists1);

  aDBLink.relProfile1.IsDirectionReversed:=aDBLink.is_profile_reversed;
  aDBLink.relProfile2.IsDirectionReversed:=aDBLink.is_profile_reversed;

// -----------------------------
//  if not aDBLink.is_profile_reversed then
//  begin
    part1.GST_length_km:=aDBLink.LinkRepeater.Part1.GST_length_km ;
    part2.GST_length_km:=aDBLink.LinkRepeater.Part2.GST_length_km ;

    part1.RelProfile:=aDBLink.relProfile1;
    part2.RelProfile:=aDBLink.relProfile2;

    part1.IsProfileExists:=aDBLink.IsProfileExists1;
    part2.IsProfileExists:=aDBLink.IsProfileExists2;

    part1.Antenna:=aDBLink.LinkRepeater.Antennas[0];
    part2.Antenna:=aDBLink.LinkRepeater.Antennas[1];

//  end;



  aDBLink.Trb.GST_LENGTH_km_  := part1.GST_length_km ;

  TDBLink_export.SaveToClass(aDBLink, aDBLink.LinkEnd1, aDBLink.LinkEnd2,
    oCalcParams1, part1.relProfile, part1.IsProfileExists);

  oAntenna:=part1.Antenna;





  oCalcParams1.TTX.Site2_DIAMETER   := oAntenna.Diameter;
  oCalcParams1.TTX.Site2_GAIN       := oAntenna.Gain;
  oCalcParams1.TTX.Site2_HEIGHT     := oAntenna.Height;
  oCalcParams1.TTX.Site2_VERT_WIDTH := oAntenna.Vert_width;
  oCalcParams1.TTX.Site2_LOSS       := oAntenna.Loss;



  // -------------------------
  // Repeater  -> Site 2
  // -------------------------
//  k:=aDBLink.FrelProfile2.Count;


  aDBLink.Trb.GST_LENGTH_km_ := Part2.GST_length_km ;

  TDBLink_export.SaveToClass(aDBLink, aDBLink.LinkEnd1, aDBLink.LinkEnd2,
    oCalcParams2, Part2.relProfile, Part2.IsProfileExists);

  oAntenna:=Part2.Antenna;


  oCalcParams2.TTX.Site1_DIAMETER   := oAntenna.Diameter;
  oCalcParams2.TTX.Site1_GAIN       := oAntenna.Gain;
  oCalcParams2.TTX.Site1_HEIGHT     := oAntenna.Height;
  oCalcParams2.TTX.Site1_VERT_WIDTH := oAntenna.Vert_width;
  oCalcParams1.TTX.Site1_LOSS       := oAntenna.Loss;



//  SaveToClass(oCalcParams);
  if not oCalcParams1.ValidateDlg then
    goto Exit_proc;

  if not oCalcParams2.ValidateDlg then
    goto Exit_proc;


  // ---------------------------------------------------------------
  // 1. 1 -> REP
  // ---------------------------------------------------------------
  FLog.Add('1 -> REP -------------------');


 // aDBLink.Trb.GST_length_km1  :=  Part1.GST_length_km;


  oCalcParams1.SaveToXML (sFile_in_1, True);
/////  LinkCalcResults.Items.Clear;
  // ---------------------------------------------------------------

  Run_rpls_rrl(sFile_in_1,sFile_out_1, sFile_layout);

  Assert(FileExists(sFile_out_1));

//  if FileExists(sFile_out_1) then

//  Assert(oLinkCalcResults1.Data.Rx_Level_dBm<>0);

//  eRx_Level_dBm1 := oLinkCalcResults1.Data.Rx_Level_dBm;
 // begin
    oLinkCalcResults1.LoadFromXmlFile_RRL_Result(sFile_out_1);
 // end;

  eRx_Level_dBm1 := oLinkCalcResults1.Data.Rx_Level_dBm;

  oCalcParams2.TTX.Site1_Power_dBm := oLinkCalcResults1.Data.Rx_Level_dBm;


  FLog.Add( Format('Site1_Power_dBm : %1.5f', [oLinkCalcResults1.Data.Rx_Level_dBm]) );

  FLog.Add( Format('Status : %d', [oLinkCalcResults1.Data.Status]) );

                               

  // ---------------------------------------------------------------
  // 2. REP -> Site 2
  // ---------------------------------------------------------------
  FLog.Add(' REP -> Site 2 ---------------------');

  oCalcParams2.SaveToXML (sFile_in_2, True);
//  LinkCalcResults.Items.Clear;

  Run_rpls_rrl(sFile_in_2,sFile_out_2,sFile_layout);

  Assert(FileExists(sFile_out_2), 'Run_rpls_rrl(sFile_in_2,sFile_out_2,sFile_layout)');

 // if FileExists(sFile_in_2) then
//  begin
    oLinkCalcResults2.LoadFromXmlFile_RRL_Result(sFile_out_2);
 // end;

  Assert(oLinkCalcResults2.Data.Rx_Level_dBm<>0);


  dRxLevel2 := oLinkCalcResults2.Data.Rx_Level_dBm;

  //� ���������� ������ ��������� ���
  eRx_Level_delta := eRx_Level_dBm1 - dRxLevel2;

  FLog.Add( Format('dRxLevel2 : %1.5f', [oLinkCalcResults2.Data.Rx_Level_dBm]) );

  FLog.Add( Format('eRx_Level_delta : %1.5f', [eRx_Level_dBm1 - dRxLevel2]) );
  FLog.Add( Format('Status : %d', [oLinkCalcResults2.Data.Status]) );


  // ---------------------------------------------------------------
  // 3. Site 1 -> REP  new data
  // ---------------------------------------------------------------
  FLog.Add(' Site 1 -> REP ----------------------------');

//  oCalcParams1.TTX.Site1_Power_dBm :=
  oCalcParams1.TTX.Site2_Threshold_dBM :=
    oCalcParams1.TTX.Site2_Threshold_dBM + eRx_Level_delta;
//    oCalcParams1.TTX.Site1_Power_dBm - eRx_Level_delta;


//  FLog.Add( Format('Site1_Power_dBm : %1.5f', [oCalcParams1.TTX.Site1_Power_dBm]) );
  FLog.Add( Format('Site1_Power_dBm : %1.5f', [oCalcParams1.TTX.Site2_Threshold_dBM]) );


  oCalcParams1.SaveToXML (sFile_in_3, True);
  //LinkCalcResults.Items.Clear;

  Run_rpls_rrl(sFile_in_3,sFile_out_3,sFile_layout);

  if FileExists(sFile_in_3) then
  begin
    oLinkCalcResults3.LoadFromXmlFile_RRL_Result(sFile_out_3);

    e := oLinkCalcResults3.Data.Rx_Level_dBm;

    FLog.Add( Format('Status : %d', [oLinkCalcResults3.Data.Status]) );


  end;


////  oLinkCalcResults3.SaveToDataset_DBTree1(dxMemData1111);

//  db_View(dxMemData1111);



 ////////// oLinkCalcResults3.ReflectionPoints.Clear;

  //1 �������� "������� [dBm]" ���������� �� ������������ ��������� � ����������� ������� ������� �������������.

//  oLinkCalcResults


  oLinkCalcResults2.Data.SESR:=oLinkCalcResults2.Data.SESR + oLinkCalcResults3.Data.SESR;
  oLinkCalcResults2.Data.KNG :=oLinkCalcResults2.Data.KNG  + oLinkCalcResults3.Data.KNG;

  b:= (oLinkCalcResults2.Data.Sesr_Req < oLinkCalcResults2.Data.SESR);
  b:= (oLinkCalcResults2.Data.KNG_Req  < oLinkCalcResults2.Data.KNG);



  if (oLinkCalcResults2.Data.Sesr_Req < oLinkCalcResults2.Data.SESR) or
     (oLinkCalcResults2.Data.KNG_Req  < oLinkCalcResults2.Data.KNG)
  then
    oLinkCalcResults2.Data.STATUS:=2;


  e:=oLinkCalcResults2.Data.STATUS;
  e:=oLinkCalcResults2.Data.Sesr_Req;
  e:=oLinkCalcResults2.Data.KNG_Req;

  e:=oLinkCalcResults3.Data.STATUS;
  e:=oLinkCalcResults3.Data.Sesr_Req;
  e:=oLinkCalcResults3.Data.KNG_Req;



//  SaveCalcResultsToDB (aDBLink, oLinkCalcResults3);
//  SaveCalcResultsToDB (aDBLink, oLinkCalcResults2);

  SaveCalcResultsToDB (aDBLink, oLinkCalcResults2, nil);   // 222222222222


  SaveReflectionPointsToDB(aDBLink, oLinkCalcResults1, 1);
  SaveReflectionPointsToDB(aDBLink, oLinkCalcResults2, 2);


  FLog.SaveToFile( g_ApplicationDataDir + 'rrl_calc.log' );


  s1:=oLinkCalcResults1.SaveToXMLString();
  s2:=oLinkCalcResults2.SaveToXMLString();
  s3:=oLinkCalcResults3.SaveToXMLString();



  iRes:=dmOnega_DB_data.ExecStoredProc(SP_Link_Repeater_Update,
          [
//           db_Par(FLD_ID,                  aLink_ID),
           db_Par(FLD_ID,   aDBLink.LinkRepeater.ID),

           db_Par(FLD_CALC_RESULTS_XML_1, s1),
           db_Par(FLD_CALC_RESULTS_XML_2, s2),
           db_Par(FLD_CALC_RESULTS_XML_3, s3)

          ]);


  Assert(iRes>0);

 // ShowMessage( FLog.Text );


  CursorDefault;
  Result:=True;


Exit_proc:

  FreeAndNil(oCalcParams1);
  FreeAndNil(oCalcParams2);

  FreeAndNil(oLinkCalcResults1);
  FreeAndNil(oLinkCalcResults2);
  FreeAndNil(oLinkCalcResults3);


end;



//--------------------------------------------------------------------
function TdmLink_calc.Execute_AdaptiveModulation(aDBLink: TDBLink): boolean;
//--------------------------------------------------------------------
var
  bTerminated: Boolean;
  dThreshold_dBm: Double;
  I: Integer;

  eRx_Level_dBm1: Double;
  sFile1,sFile2: string;
  s: string;

  oCalcParams: TrrlCalcParam;
  sFile_layout: string;


  rec: TGet_Equivalent_Length_KM_rec;


label Exit_proc;

begin
  Params.IsRunBothSides := False; //!!!!!!!

  Result:=False;

  CursorHourGlass;
                     

//  sFile1:=GetTempFileNameWithExt('xml');


  sFile1 := g_ApplicationDataDir + 'rrl_am.xml';
  sFile2 := g_ApplicationDataDir + 'rrl_result_am.xml';

  sFile_layout :=g_ApplicationDataDir + 'rrl_am_layout.ini';


  FillChar(rec, SizeOf(rec), 0);
//  rec.Length_Km:=ADOStoredProc_Link

 rec.Length_Km     := aDBLink.GetLength_km;
 rec.GST_length_km := aDBLink.Trb.GST_LENGTH_km_;
 rec.GST_Type      := aDBLink.Trb.GST_TYPE;




  rec.Length_Km     :=ADOStoredProc_Link.FieldByName(FLD_LENGTH).AsFloat/1000;
  rec.GST_length_km :=ADOStoredProc_Link.FieldByName(FLD_GST_LENGTH).AsFloat;
  rec.GST_Type      :=ADOStoredProc_Link.FieldByName(FLD_GST_TYPE).AsInteger;


  aDBLink.Trb.GST_LENGTH_km_  :=  dmLink_Calc_SESR.Get_Equivalent_Length_KM_rec(rec);


//    aDBLink.Trb.GST_length_km1  :=
//      dmLink_Calc_SESR.Get_Equivalent_Length_KM(ADOStoredProc_Link);


  oCalcParams:=TrrlCalcParam.Create;


  TDBLink_export.SaveToClass(aDBLink, aDBLink.LinkEnd1, aDBLink.LinkEnd2,
    oCalcParams, aDBLink.relProfile, aDBLink.IsProfileExists);


  for I := 0 to High(FAdaptiveModeArr) do
  begin
    Progress_SetProgress1(i, High(FAdaptiveModeArr), bTerminated);


    oCalcParams.TTX.BitRate_Mbps    := FAdaptiveModeArr[i].bitRate_Mbps;
    oCalcParams.TTX.Site1_Power_dBm := FAdaptiveModeArr[i].Power_dBm;
   // oCalcParams.TTX.Site2_Threshold_dBM:= FAdaptiveModeArr[i].THRESHOLD_BER_3;

    //���������������� dBm
    case aDBLink.BER_REQUIRED_Index of
      0: dThreshold_dBm:=FAdaptiveModeArr[i].THRESHOLD_BER_3;
      1: dThreshold_dBm:=FAdaptiveModeArr[i].THRESHOLD_BER_6;
    else
      raise Exception.Create('');
    end;

//    oCalcParams.TTX.MODULATION_Level_COUNT
//    Modulation_type

    oCalcParams.TTX.Site2_Threshold_dBM:=dThreshold_dBm;


    // SaveToClass(oCalcParams);
    if not oCalcParams.ValidateDlg then
      goto Exit_proc;


    oCalcParams.SaveToXML (sFile1, True);

//ShellExec_Notepad(sFile1);

    Run_rpls_rrl(sFile1,sFile2, sFile_layout);


  //  LinkCalcResults.Items.Clear;

    if FileExists(sFile2) then
    begin
      LinkCalcResults.Groups.Clear;

  //    LinkCalcResult_loader
///////
      LinkCalcResults.LoadFromXmlFile_RRL_Result(sFile2);

    //  LinkCalcResults.LoadFromXMLFile_RRL_XML(sFile1);

      assert(LinkCalcResults.Data.Rx_Level_dBm<>0);


      FAdaptiveModeArr[i].Result.Rx_Level_dBm   := LinkCalcResults.Data.Rx_Level_dBm;
      FAdaptiveModeArr[i].Result.KNG            := LinkCalcResults.Data.Kng;
      FAdaptiveModeArr[i].Result.SESR           := LinkCalcResults.Data.SESR;

      FAdaptiveModeArr[i].Result.fade_margin_dB := LinkCalcResults.Data.fade_margin_dB;
      FAdaptiveModeArr[i].Result.KNG_req        := LinkCalcResults.Data.KNG_Req;
      FAdaptiveModeArr[i].Result.SESR_req       := LinkCalcResults.Data.Sesr_Req;

      FAdaptiveModeArr[i].Result.IsWorking      := LinkCalcResults.Data.STATUS = 1;

    end;

  end;


  Progress_Free;

 //// SaveCalcResultsToDB (FLinkID, LinkCalcResults);

  Tdlg_AdaptiveModulation.ExecDlg (FAdaptiveModeArr, aDBLink.id);


  CursorDefault;
  Result:=True;


Exit_proc:

  FreeAndNil(oCalcParams);


end;

// ---------------------------------------------------------------
procedure TdmLink_calc.Execute_Optimize_Equioment_progress;
// ---------------------------------------------------------------
var
  dThreshold_dBm: Double;
  I: Integer;

  eRx_Level_dBm1: Double;
  sFile1,sFile2: string;
  s: string;

  oCalcParams: TrrlCalcParam;
  sFile_layout: string;


  rec: TGet_Equivalent_Length_KM_rec;

  oTaskItem: TTaskItem;


label Exit_proc;

begin
 // ShowMessage('TdmLink_calc.Execute_Optimize_Equioment_progress;');

  //function Progress_ExecDlg_proc(aOnStartEvent: TProcedureEvent; aCaption: string = ''): Boolean;


  Params.IsRunBothSides := False; //!!!!!!!

 // Result:=False;

//  CursorHourGlass;
                     

//  sFile1:=GetTempFileNameWithExt('xml');


  sFile1 := g_ApplicationDataDir + 'rrl_am.xml';
  sFile2 := g_ApplicationDataDir + 'rrl_result_am.xml';

  sFile_layout :=g_ApplicationDataDir + 'rrl_am_layout.ini';


  FillChar(rec, SizeOf(rec), 0);
//  rec.Length_Km:=ADOStoredProc_Link

 rec.Length_Km     := FDBLink.GetLength_km;
 rec.GST_length_km := FDBLink.Trb.GST_LENGTH_km_;
 rec.GST_Type      := FDBLink.Trb.GST_TYPE;



{
  rec.Length_Km     :=ADOStoredProc_Link.FieldByName(FLD_LENGTH).AsFloat/1000;
  rec.GST_length_km :=ADOStoredProc_Link.FieldByName(FLD_GST_LENGTH).AsFloat;
  rec.GST_Type      :=ADOStoredProc_Link.FieldByName(FLD_GST_TYPE).AsInteger;
}


  FDBLink.Trb.GST_LENGTH_km_  :=  dmLink_Calc_SESR.Get_Equivalent_Length_KM_rec(rec);


//    aDBLink.Trb.GST_length_km1  :=
//      dmLink_Calc_SESR.Get_Equivalent_Length_KM(ADOStoredProc_Link);


  oCalcParams:=TrrlCalcParam.Create;


  TDBLink_export.SaveToClass(FDBLink, FDBLink.LinkEnd1, FDBLink.LinkEnd2,
    oCalcParams, FDBLink.relProfile, FDBLink.IsProfileExists);



//     g_OptiTaskItemList

  for I := 0 to g_OptiTaskItemList.Count-1  do

//  for I := 0 to High(FAdaptiveModeArr) do
  begin
 //   if i>3  then
  //    break;


    oTaskItem:=g_OptiTaskItemList[i];

    oCalcParams.TTX.Site1_GAIN      :=oTaskItem.Antenna.Gain;
    oCalcParams.TTX.Site1_DIAMETER  :=oTaskItem.Antenna.Diameter;
    oCalcParams.TTX.Site1_VERT_WIDTH:=oTaskItem.Antenna.Vert_width;

    oCalcParams.TTX.Site2_GAIN      :=oTaskItem.Antenna.Gain;
    oCalcParams.TTX.Site2_DIAMETER  :=oTaskItem.Antenna.Diameter;
    oCalcParams.TTX.Site2_VERT_WIDTH:=oTaskItem.Antenna.Vert_width;



    oCalcParams.TTX.BitRate_Mbps    := oTaskItem.bitRate_Mbps;
    oCalcParams.TTX.Site1_Power_dBm := oTaskItem.Power_dBm;
   // oCalcParams.TTX.Site2_Threshold_dBM:= FAdaptiveModeArr[i].THRESHOLD_BER_3;

    //���������������� dBm
    case FDBLink.BER_REQUIRED_Index of
      0: dThreshold_dBm:=oTaskItem.THRESHOLD_BER_3;
      1: dThreshold_dBm:=oTaskItem.THRESHOLD_BER_6;
    else
      raise Exception.Create('');
    end;

//    oCalcParams.TTX.MODULATION_Level_COUNT
//    Modulation_type

    oCalcParams.TTX.Site2_Threshold_dBM:=dThreshold_dBm;


    // SaveToClass(oCalcParams);
    if not oCalcParams.ValidateDlg then
      goto Exit_proc;


    oCalcParams.SaveToXML (sFile1, True);

//ShellExec_Notepad(sFile1);

    Run_rpls_rrl(sFile1,sFile2, sFile_layout);


  //  LinkCalcResults.Items.Clear;

    if FileExists(sFile2) then
    begin
      LinkCalcResults.Groups.Clear;

  //    LinkCalcResult_loader
///////
      LinkCalcResults.LoadFromXmlFile_RRL_Result(sFile2);

    //  LinkCalcResults.LoadFromXMLFile_RRL_XML(sFile1);

      assert(LinkCalcResults.Data.Rx_Level_dBm<>0);


      oTaskItem.Result.Rx_Level_dBm   := LinkCalcResults.Data.Rx_Level_dBm;
      oTaskItem.Result.KNG            := LinkCalcResults.Data.Kng;
      oTaskItem.Result.SESR           := LinkCalcResults.Data.SESR;

      oTaskItem.Result.fade_margin_dB := LinkCalcResults.Data.fade_margin_dB;
      oTaskItem.Result.KNG_req        := LinkCalcResults.Data.KNG_Req;
      oTaskItem.Result.SESR_req       := LinkCalcResults.Data.Sesr_Req;

      oTaskItem.Result.IsWorking      := (LinkCalcResults.Data.STATUS = 1);

    end;

  end;


 //// SaveCalcResultsToDB (FLinkID, LinkCalcResults);

 // Tdlg_AdaptiveModulation.ExecDlg (FAdaptiveModeArr);


  CursorDefault;
 // Result:=True;


Exit_proc:

  FreeAndNil(oCalcParams);



end;


//--------------------------------------------------------------------
function TdmLink_calc.Execute_Optimize_Equioment(aDBLink: TDBLink): boolean;
//--------------------------------------------------------------------

begin
  FDBLink:=aDBLink;

  result := Tfrm_Main_opti.ExecDlg(aDBLink.id, Execute_Optimize_Equioment_progress) ;


end;




//--------------------------------------------------------------------
procedure TdmLink_calc.CalcRains(aDBLink: TDBLink; aLinkEndID1: integer;
    aLinkCalcResults: TLinkCalcResult);
//--------------------------------------------------------------------
// �������������� ���� �������
// -------------------------------------------------------------------

var
  iValue: Integer;
  rec_in: TLinkCalcRainDop;
  rains_rec_res: TLinkCalcRainDopResults;

  bIsCalcLength: boolean;
  iStatus: integer;
  s: string;

begin
  FillChar(rec_in, SizeOf(rec_in), 0);

  rec_in.rain_INTENSITY:= aDBLink.Rains.rain_INTENSITY;

//  if (rec_in.rain_INTENSITY = 0) then
//    rec_in.rain_INTENSITY:= Link.RRV.RAIN_intensity;


(*  rec_in.rain_INTENSITY:= ADOStoredProc_Link.FieldByName(FLD_rain_INTENSITY).AsFloat;
  if (rec_in.Rain_INTENSITY = 0) then
    rec_in.rain_intense:= ADOStoredProc_Link.FieldByName(FLD_RAIN_RATE).AsFloat;
*)

  Assert(qry_Antennas1.Active);

  case aDBLink.LinkEnd1.Antennas[0].PolarizationType of
    ptV__: rec_in.polarization :=ptV;
    ptH__: rec_in.polarization :=ptH;
//  else
 //   rec_in.polarization :='';
  end;

(*
  if s='V' then
    rec_in.polarization :=ptV;
  if s='H' then
    rec_in.polarization :=ptH;


  s :=Link.LinkEnd1.Antennas[0].Polarization_str;
  if s='V' then
    rec_in.polarization :=ptV;
  if s='H' then
    rec_in.polarization :=ptH;

*)
    //iValue:=FDBLinkEnd1.DBAntennas[0].Polarization;
  //  iValue:=qry_Antennas1.FieldByName(FLD_Polarization).AsInteger;

//      iValue:=gl_DB.GetIntFieldValueByID (VIEW_LINKEND_ANTENNAS, FLD_POLARIZATION, aLinkEndID1);
 //   rec_in.polarization      :=IIF (iValue=0, ptH, ptV);


  rec_in.IsCalcRainLength         := aDBLink.Rains.ISCALCLENGTH;
  rec_in.rain_signal_depression_dB:= aLinkCalcResults.Data.Rains.max_weakness1;//  GetCalcParamID(DEF_MAX_WEAKNESS);
  rec_in.direct_line_tilt         := aLinkCalcResults.Data.Rains.direct_line_tilt;//  GetCalcParamID(DEF_DIRECT_LINE_TILT);
  rec_in.work_freq_GHz            := aDBLink.LinkEnd1.Tx_Freq_Ghz;
//  rec_in.interval_length_KM       := FCalc_Length_KM;
  rec_in.interval_length_KM       := aDBLink.GetLength_km;


  FillChar(rains_rec_res,SizeOf(rains_rec_res),0);


  // -----------------------------------------------
  u_Link_CalcRains.Link_CalcRains1 (rec_in, False, rains_rec_res);
  // -----------------------------------------------

  { TODO : !!!!!!!! }
  // if IsCalcWithAdditionalRain then
  if not rains_rec_res.Rain_Enabled then //rain_fitness_str = DEF_LINK_CALCRAIN_NOT_NORM then
  begin
    iStatus:=2;   // �� �����

    FDBManager.UpdateRecord(TBL_Link, aDBLink.ID, [db_Par(FLD_STATUS, iStatus)]);

//    FDBManager.UpdateRecord(TBL_Link, aID, [db_Par(FLD_STATUS, iStatus)]);

    // dmLink.Update(aID, [db_Par(FLD_STATUS, iStatus)]);

  end;
  // ------------------------------------------------------------

  {
  iRes:=dmOnega_DB_data.ExecStoredProc_(sp_Link_Update,
     [
       FLD_ID,                          aDBLink.ID,

       FLD_RAIN_SIGNAL_QUALITY,         rains_rec_res.rain_fitness_str,
       FLD_RAIN_LENGTH_km,              rains_rec_res.rain_length_km,
       FLD_RAIN_WEAKENING_VERT,         rains_rec_res.weakness_on_vert_polarization,
       FLD_RAIN_WEAKENING_HORZ,         rains_rec_res.weakness_on_horz_polarization,
       FLD_RAIN_ALLOWABLE_INTENSE_VERT, rains_rec_res.allowable_intens_on_vert_polarization,
       FLD_RAIN_ALLOWABLE_INTENSE_HORZ, rains_rec_res.allowable_intens_on_horz_polarization,

       FLD_rain_signal_depression_dB,   rec_in.rain_signal_depression_dB

           //..  db_Par(FLD_TILT,                        rec_in.direct_line_tilt)
      ]);

    }


  FDBManager.UpdateRecord(TBL_Link,

   // with rains_rec_res do
 // dmLink.Update(
      aDBLink.ID,

//    aID,
      [
       db_Par(FLD_RAIN_SIGNAL_QUALITY,         rains_rec_res.rain_fitness_str),
       db_Par(FLD_RAIN_LENGTH_km,              rains_rec_res.rain_length_km),

       db_Par(FLD_RAIN_WEAKENING_VERT,         rains_rec_res.weakness_on_vert_polarization),
       db_Par(FLD_RAIN_WEAKENING_HORZ,         rains_rec_res.weakness_on_horz_polarization),

       db_Par(FLD_RAIN_ALLOWABLE_INTENSE_VERT, rains_rec_res.allowable_intens_on_vert_polarization),
       db_Par(FLD_RAIN_ALLOWABLE_INTENSE_HORZ, rains_rec_res.allowable_intens_on_horz_polarization),

       db_Par(FLD_rain_signal_depression_dB,   rec_in.rain_signal_depression_dB)
     //..  db_Par(FLD_TILT,                        rec_in.direct_line_tilt)
      ]);



{

  iRes:=dmOnega_DB_data.ExecStoredProc_(sp_Link_Update,
          [
//           db_Par(FLD_ID,                  aLink_ID),
           FLD_ID,                  aDBLink.ID,

           FLD_Rx_Level_dBm,        FCalcResults_Rec.Rx_Level_dBm,

           FLD_SESR,                FCalcResults_Rec.SESR,
           FLD_KNG,                 FCalcResults_Rec.Kng,

}




end;

// ---------------------------------------------------------------
function TdmLink_calc.Run_rpls_rrl(aFile1, aFile2, aFile_layout: string):
    Boolean;
// ---------------------------------------------------------------
var
  sRunPath: string;

  oIni_rpls_rrl_params: TIni_rpls_rrl_params;
  sFile: string;

begin
  Assert(aFile1<>'');
  Assert(aFile2<>'');
  Assert(aFile_layout<>'');

  oIni_rpls_rrl_params:=TIni_rpls_rrl_params.Create;


  sRunPath := GetApplicationDir() + EXE_RPLS_RRL;

  Result := True;

  {$DEFINE use_dll111}

  {$IFDEF use_dll}
   {
    if Load_Irpls_rrl() then
    begin
    //  Irpls_rrl.InitADO(dmMain.ADOConnection1.ConnectionObject);
   //   ILink_graph.InitAppHandle(Application.Handle);
      Irpls_rrl.Execute(sFile);

      UnLoad_Irpls_rrl();
    end;
    }
  //  Exit;

  {$ELSE}

    oIni_rpls_rrl_params.In_FileName := aFile1;
    oIni_rpls_rrl_params.out_FileName := aFile2;
    oIni_rpls_rrl_params.OUTREZ_FileName := aFile_layout;


    sFile :=g_ApplicationDataDir + 'rrl.ini';

    oIni_rpls_rrl_params.SaveToFile(sFile);

    Result := RunAppEx (sRunPath,  [sFile]);

 //   Result := RunAppEx (sRunPath,  [aFile1, aFile2, '', aFile_layout]);

//                      DoubleQuotedStr(aFile1) +' '+
  //                    DoubleQuotedStr(aFile2)) ;

//    Result := RunApp (sRunPath,
//                      DoubleQuotedStr(aFile1) +' '+
//                      DoubleQuotedStr(aFile2)) ;


  //  if RunApp (GetApplicationDir() + EXE_RPLS_RRL, sFile1 +' '+ aFile2) then


  {$ENDIF}

  FreeAndNil(oIni_rpls_rrl_params);

end;


//--------------------------------------------------------------------
procedure TdmLink_Calc.ExecuteList1(aIDList: TIDList);
//--------------------------------------------------------------------
var
  bCritical: Boolean;
  i: integer;
  k: Integer;
  oLink: TDBLink;

  oList_bands: TStringList;
  oList_critical: TStringList;

  bTerminated: boolean;

label
  go_exit;

begin
  oList_critical:=TStringList.Create;
  oList_bands:=TStringList.Create;


   Progress_Show('������...');


  for i:=0 to aIDList.Count-1 do
  begin
    oLink:=LinkList.AddItem;

    OpenData_new(aIDList[i].ID, oLink);

    oLink.Validate_Critical(oList_critical);
    oLink.Validate_Bands_(oList_bands);


{    bCritical:=oList_critical.Count>0;

    if oList_critical.Count>0 then
    begin
      ShowMessage(oList_critical.Text);

      goto go_exit;
    end;
}

    Execute (oLink);

    LinkList.Clear;

    Progress_SetProgress1(i, aIDList.Count, bTerminated);

//    Execute (aIDList[i].ID , oLink);

  end;

  Progress_Free;
//-------------------------------------
 {
  bCritical:=oList_critical.Count>0;

  if oList_critical.Count>0 then
  begin
    ShowMessage(oList_critical.Text);

    goto go_exit;
  end;
 }
//  bCritical:=oList_critical.Count>0;

{
  if oList_bands.Count>0 then
  begin
    if not Tdlg_Message1.ExecDlg ('���������� ?', oList_bands.Text) then
      goto go_exit;

  end;
}
{

  Progress_Show('������...');


  for i:=0 to aIDList.Count-1 do
  begin
  //  oLink:=LinkList.AddItem;

    //OpenData_new(aIDList[i].ID, oLink);

//    oLink.Validate_Critical(oList_critical);
//    oLink.Validate_Bands(oList_bands);

    Execute (LinkList[i]);

    Progress_SetProgress1(i, aIDList.Count, bTerminated);

  end;

  Progress_Free;
 }

//  ShowMessage(oList_bands.Text);

 {
  if not Result then
  begin
    k :=  MyMessageDlg(oStrList.Text, mtWarning, [mbYes, mbNo],
        ['Ok','������'], '���������� ?' );
    Result := k = 6;

  end;
  }

go_exit:

  FreeAndNil(oList_bands);
  FreeAndNil(oList_critical);



end;




end.


