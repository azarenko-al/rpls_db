object frm_Main_opti: Tfrm_Main_opti
  Left = 473
  Top = 338
  Caption = 'frm_Main_opti'
  ClientHeight = 745
  ClientWidth = 1364
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 41
    Width = 1364
    Height = 411
    ActivePage = TabSheet2
    Align = alTop
    MultiLine = True
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = '1. '#1055#1072#1088#1072#1084#1077#1090#1088#1099
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxVerticalGrid1: TcxVerticalGrid
        Left = 0
        Top = 0
        Width = 497
        Height = 383
        Align = alLeft
        LookAndFeel.Kind = lfFlat
        OptionsView.PaintStyle = psDelphi
        OptionsView.RowHeaderWidth = 213
        TabOrder = 0
        OnEditValueChanged = cxVerticalGrid1EditValueChanged
        Version = 1
        object cxVerticalGrid1CategoryRow2: TcxCategoryRow
          Properties.Caption = #1058#1088#1077#1073#1086#1074#1072#1085#1080#1103' '#1087#1086' '#1082#1072#1095#1077#1089#1090#1074#1091
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object row_Use: TcxEditorRow
          Properties.Caption = #1048#1089#1087#1086#1083#1100#1079#1086#1074#1072#1090#1100
          Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
          Properties.EditProperties.Columns = 2
          Properties.EditProperties.Items = <
            item
              Caption = #1090#1080#1087#1086#1074#1099#1077' '#1079#1085#1072#1095#1077#1085#1080#1103
              Value = '0'
            end
            item
              Caption = #1085#1077#1090#1080#1087#1086#1074#1099#1077' '#1079#1085#1072#1095#1077#1085#1080#1103
              Value = '1'
            end>
          Properties.DataBinding.ValueType = 'Integer'
          Properties.Value = 0
          Visible = False
          ID = 1
          ParentID = 0
          Index = 0
          Version = 1
        end
        object row_NetworkType: TcxEditorRow
          Properties.Caption = #1058#1080#1087' '#1089#1077#1090#1080
          Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
          Properties.EditProperties.DropDownListStyle = lsFixedList
          Properties.EditProperties.ImmediatePost = True
          Properties.EditProperties.Items.Strings = (
            '1'
            '2'
            '3')
          Properties.EditProperties.OnCloseUp = row_NetworkTypeEditPropertiesCloseUp
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
          ID = 2
          ParentID = 0
          Index = 1
          Version = 1
        end
        object row_SESR: TcxEditorRow
          Properties.Caption = 'SESR'
          Properties.DataBinding.ValueType = 'Float'
          Properties.Value = Null
          ID = 3
          ParentID = 0
          Index = 2
          Version = 1
        end
        object row_KNG: TcxEditorRow
          Properties.Caption = 'KNG'
          Properties.DataBinding.ValueType = 'Float'
          Properties.Value = Null
          ID = 4
          ParentID = 0
          Index = 3
          Version = 1
        end
        object cxVerticalGrid1CategoryRow1: TcxCategoryRow
          Properties.Caption = #1043#1072#1088#1072#1085#1090#1080#1088#1086#1074#1072#1085#1085#1072#1103' '#1089#1082#1086#1088#1086#1089#1090#1100' '#1087#1077#1088#1077#1076#1072#1095#1080' '#1076#1072#1085#1085#1099#1093':'
          ID = 5
          ParentID = -1
          Index = 1
          Version = 1
        end
        object row_Bitrate: TcxEditorRow
          Properties.Caption = #1057#1082#1086#1088#1086#1089#1090#1100' '#1087#1077#1088#1077#1076#1072#1095#1080' Mbit/s'
          Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
          Properties.DataBinding.ValueType = 'Integer'
          Properties.Value = 50
          ID = 6
          ParentID = 5
          Index = 0
          Version = 1
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = '2. '#1054#1073#1086#1088#1091#1076#1086#1074#1072#1085#1080#1077
      ImageIndex = 1
      object PageControl2: TPageControl
        Left = 0
        Top = 41
        Width = 1356
        Height = 323
        ActivePage = TabSheet_Ant
        Align = alTop
        Style = tsFlatButtons
        TabOrder = 0
        object TabSheet_RRS: TTabSheet
          Caption = #1056#1056#1057
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object cxGrid_LinkEndType: TcxGrid
            Left = 0
            Top = 0
            Width = 1348
            Height = 188
            Align = alTop
            TabOrder = 0
            LookAndFeel.Kind = lfFlat
            object cxGridDBTableView1_LinkEndType: TcxGridDBTableView
              PopupMenu = PopupMenu_LinkEndType
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = ds_LinkEndType
              DataController.KeyFieldNames = 'id'
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsView.GroupByBox = False
              OptionsView.Indicator = True
              object col_Checked1: TcxGridDBColumn
                DataBinding.FieldName = 'checked'
                Width = 30
              end
              object cxGridDBTableView1_LinkEndTypefolder_name: TcxGridDBColumn
                DataBinding.FieldName = 'folder_name'
                Options.Editing = False
                Width = 258
              end
              object cxGridDBTableView1_LinkEndTypename: TcxGridDBColumn
                DataBinding.FieldName = 'name'
                Options.Editing = False
                Width = 397
              end
              object cxGridDBTableView1_LinkEndTypeband: TcxGridDBColumn
                DataBinding.FieldName = 'band'
                Options.Editing = False
              end
              object cxGridDBTableView1_LinkEndTypevendor_id: TcxGridDBColumn
                DataBinding.FieldName = 'vendor_id'
                Visible = False
                Options.Editing = False
              end
              object cxGridDBTableView1_LinkEndTypeColumn1: TcxGridDBColumn
                DataBinding.FieldName = 'id'
                Visible = False
              end
            end
            object cxGrid_LinkEndTypeDBTableView_mode: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = ds_LinkEndType_modes
              DataController.DetailKeyFieldNames = 'LinkEndType_id'
              DataController.KeyFieldNames = 'id'
              DataController.MasterKeyFieldNames = 'id'
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Kind = skCount
                  Column = cxGrid_LinkEndTypeDBTableView_modemode
                end>
              DataController.Summary.SummaryGroups = <>
              OptionsView.GroupByBox = False
              OptionsView.Indicator = True
              object cxGrid_LinkEndTypeDBTableView_modeid: TcxGridDBColumn
                DataBinding.FieldName = 'id'
                Visible = False
              end
              object cxGrid_LinkEndTypeDBTableView_modeLinkEndType_id: TcxGridDBColumn
                DataBinding.FieldName = 'LinkEndType_id'
                Visible = False
              end
              object cxGrid_LinkEndTypeDBTableView_modemode: TcxGridDBColumn
                DataBinding.FieldName = 'mode'
              end
              object cxGrid_LinkEndTypeDBTableView_modebitrate_Mbps: TcxGridDBColumn
                DataBinding.FieldName = 'bitrate_Mbps'
              end
              object cxGrid_LinkEndTypeDBTableView_modemodulation_type: TcxGridDBColumn
                DataBinding.FieldName = 'modulation_type'
              end
              object cxGrid_LinkEndTypeDBTableView_modethreshold_BER_3: TcxGridDBColumn
                DataBinding.FieldName = 'threshold_BER_3'
              end
              object cxGrid_LinkEndTypeDBTableView_modethreshold_BER_6: TcxGridDBColumn
                DataBinding.FieldName = 'threshold_BER_6'
              end
            end
            object cxGridLevel1: TcxGridLevel
              GridView = cxGridDBTableView1_LinkEndType
              object cxGrid_LinkEndTypeLevel1: TcxGridLevel
                GridView = cxGrid_LinkEndTypeDBTableView_mode
              end
            end
          end
        end
        object TabSheet_Ant: TTabSheet
          Caption = #1040#1085#1090#1077#1085#1085#1099
          ImageIndex = 1
          object cxGrid_ant: TcxGrid
            Left = 0
            Top = 0
            Width = 1348
            Height = 169
            Align = alTop
            TabOrder = 0
            LookAndFeel.Kind = lfFlat
            object cxGrid_antDBTableView_Antenna: TcxGridDBTableView
              PopupMenu = PopupMenu_Antenna
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = ds_Ant
              DataController.KeyFieldNames = 'id'
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsView.GroupByBox = False
              OptionsView.Indicator = True
              object col_checked2: TcxGridDBColumn
                DataBinding.FieldName = 'checked'
                Width = 48
              end
              object cxGrid_antDBTableView_Antennafolder_name: TcxGridDBColumn
                DataBinding.FieldName = 'folder_name'
                Options.Editing = False
                Width = 157
              end
              object cxGrid_antDBTableView_Antennaname: TcxGridDBColumn
                DataBinding.FieldName = 'name'
                Options.Editing = False
                Width = 327
              end
              object cxGrid_antDBTableView_Antennaband: TcxGridDBColumn
                DataBinding.FieldName = 'band'
                Options.Editing = False
                Width = 66
              end
              object cxGrid_antDBTableView_Antennagain: TcxGridDBColumn
                DataBinding.FieldName = 'gain'
                Options.Editing = False
              end
              object cxGrid_antDBTableView_Antennafreq: TcxGridDBColumn
                DataBinding.FieldName = 'freq'
                Options.Editing = False
              end
              object cxGrid_antDBTableView_Antennadiameter: TcxGridDBColumn
                DataBinding.FieldName = 'diameter'
                Options.Editing = False
              end
              object cxGrid_antDBTableView_Antennavert_width: TcxGridDBColumn
                DataBinding.FieldName = 'vert_width'
                Options.Editing = False
              end
              object cxGrid_antDBTableView_Antennahorz_width: TcxGridDBColumn
                DataBinding.FieldName = 'horz_width'
                Options.Editing = False
              end
              object cxGrid_antDBTableView_Antennapolarization_str: TcxGridDBColumn
                DataBinding.FieldName = 'polarization_str'
                Options.Editing = False
                Width = 86
              end
              object cxGrid_antDBTableView_AntennaColumn1_id: TcxGridDBColumn
                DataBinding.FieldName = 'id'
                Visible = False
              end
            end
            object cxGrid_antLevel1: TcxGridLevel
              GridView = cxGrid_antDBTableView_Antenna
            end
          end
        end
      end
      object ToolBar5: TToolBar
        Left = 0
        Top = 0
        Width = 1356
        Height = 41
        BorderWidth = 2
        ButtonHeight = 25
        Caption = 'ToolBar1'
        TabOrder = 1
        object Button8: TButton
          Left = 0
          Top = 0
          Width = 100
          Height = 25
          Action = act_Open
          TabOrder = 0
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = '3. '#1056#1072#1089#1095#1077#1090
      ImageIndex = 4
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxGrid_Res: TcxGrid
        Left = 0
        Top = 41
        Width = 1356
        Height = 294
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGrid_ResDBBandedTableView_Res: TcxGridDBBandedTableView
          PopupMenu = PopupMenu1_result
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_dxMemData_result
          DataController.Filter.MaxValueListCount = 1000
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Kind = skCount
              Column = col_LinkEndType_name
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.BandHiding = True
          OptionsCustomize.BandsQuickCustomization = True
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellSelect = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          OptionsView.GroupFooters = gfVisibleWhenExpanded
          OptionsView.HeaderAutoHeight = True
          OptionsView.Indicator = True
          OptionsView.FixedBandSeparatorWidth = 1
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          Bands = <
            item
              Caption = #1056#1056#1057
            end
            item
              Caption = #1040#1085#1090#1077#1085#1085#1072
              Width = 425
            end
            item
              Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090
            end
            item
              Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090' '#1087#1086#1083#1085#1099#1081
            end>
          object col_ID: TcxGridDBBandedColumn
            DataBinding.FieldName = 'id'
            Visible = False
            Options.Filtering = False
            VisibleForCustomization = False
            Width = 41
            Position.BandIndex = -1
            Position.ColIndex = -1
            Position.RowIndex = -1
          end
          object col__Mode: TcxGridDBBandedColumn
            Caption = #1056#1077#1078#1080#1084
            DataBinding.FieldName = 'Mode'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = False
            Options.Filtering = False
            Width = 41
            Position.BandIndex = 0
            Position.ColIndex = 3
            Position.RowIndex = 0
          end
          object col_Bitrate_Mbps: TcxGridDBBandedColumn
            DataBinding.FieldName = 'Bitrate_Mbps'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = False
            Options.Filtering = False
            Width = 68
            Position.BandIndex = 0
            Position.ColIndex = 5
            Position.RowIndex = 0
          end
          object col__Power_max: TcxGridDBBandedColumn
            DataBinding.FieldName = 'Power_dBm'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = False
            Options.Filtering = False
            Width = 63
            Position.BandIndex = 0
            Position.ColIndex = 2
            Position.RowIndex = 0
          end
          object col__threshold_BER_3: TcxGridDBBandedColumn
            DataBinding.FieldName = 'threshold_BER_3'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = False
            Options.Filtering = False
            Width = 89
            Position.BandIndex = 0
            Position.ColIndex = 6
            Position.RowIndex = 0
          end
          object col__threshold_BER_6: TcxGridDBBandedColumn
            DataBinding.FieldName = 'threshold_BER_6'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = False
            Options.Filtering = False
            Width = 89
            Position.BandIndex = 0
            Position.ColIndex = 7
            Position.RowIndex = 0
          end
          object col_sesr: TcxGridDBBandedColumn
            Caption = 'SESR'
            DataBinding.FieldName = 'sesr'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.DisplayFormat = ',0.00000'
            Position.BandIndex = 3
            Position.ColIndex = 2
            Position.RowIndex = 0
          end
          object col_kng: TcxGridDBBandedColumn
            Caption = 'KNG'
            DataBinding.FieldName = 'kng'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.DisplayFormat = ',0.00000'
            Position.BandIndex = 3
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object col_rx_level: TcxGridDBBandedColumn
            DataBinding.FieldName = 'Rx_Level_dBm'
            Width = 67
            Position.BandIndex = 3
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object col_Modulation_type: TcxGridDBBandedColumn
            DataBinding.FieldName = 'Modulation_type'
            Width = 84
            Position.BandIndex = 0
            Position.ColIndex = 4
            Position.RowIndex = 0
          end
          object col_fade_margin_dB: TcxGridDBBandedColumn
            DataBinding.FieldName = 'fade_margin_dB'
            Position.BandIndex = 3
            Position.ColIndex = 5
            Position.RowIndex = 0
          end
          object col_kng_req: TcxGridDBBandedColumn
            Caption = 'kng_req'
            DataBinding.FieldName = 'kng_REQUIRED'
            Position.BandIndex = 3
            Position.ColIndex = 4
            Position.RowIndex = 0
          end
          object col_sesr_req: TcxGridDBBandedColumn
            DataBinding.FieldName = 'sesr_REQUIRED'
            Position.BandIndex = 3
            Position.ColIndex = 6
            Position.RowIndex = 0
          end
          object col_gain: TcxGridDBBandedColumn
            DataBinding.FieldName = 'gain'
            Width = 45
            Position.BandIndex = 1
            Position.ColIndex = 4
            Position.RowIndex = 0
          end
          object col_antennaType_name: TcxGridDBBandedColumn
            DataBinding.FieldName = 'antennaType_name'
            Width = 152
            Position.BandIndex = 1
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object col_antennaType_ID: TcxGridDBBandedColumn
            DataBinding.FieldName = 'antennaType_ID'
            Visible = False
            Width = 79
            Position.BandIndex = 1
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object col_LinkEndType_name: TcxGridDBBandedColumn
            DataBinding.FieldName = 'LinkEndType_name'
            MinWidth = 1
            Width = 101
            Position.BandIndex = 0
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object col_LinkEndType_id1: TcxGridDBBandedColumn
            DataBinding.FieldName = 'LinkEndType_id'
            Visible = False
            Width = 96
            Position.BandIndex = 0
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object col_Is_Working1: TcxGridDBBandedColumn
            DataBinding.FieldName = 'Is_Working'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Position.BandIndex = 3
            Position.ColIndex = 3
            Position.RowIndex = 0
          end
          object col_Diameter: TcxGridDBBandedColumn
            DataBinding.FieldName = 'Diameter'
            Width = 79
            Position.BandIndex = 1
            Position.ColIndex = 3
            Position.RowIndex = 0
          end
          object col_Antenna_Polarization: TcxGridDBBandedColumn
            DataBinding.FieldName = 'Polarization'
            Width = 70
            Position.BandIndex = 1
            Position.ColIndex = 5
            Position.RowIndex = 0
          end
          object col_Vert_width: TcxGridDBBandedColumn
            DataBinding.FieldName = 'Vert_width'
            Position.BandIndex = 1
            Position.ColIndex = 2
            Position.RowIndex = 0
          end
          object col_Rx_Level_draft: TcxGridDBBandedColumn
            DataBinding.FieldName = 'Rx_Level_draft'
            Position.BandIndex = 2
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object col_Zapas_draft: TcxGridDBBandedColumn
            DataBinding.FieldName = 'Zapas_draft'
            Position.BandIndex = 2
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
        end
        object cxGrid_ResLevel1: TcxGridLevel
          GridView = cxGrid_ResDBBandedTableView_Res
        end
      end
      object ToolBar3: TToolBar
        Left = 0
        Top = 0
        Width = 1356
        Height = 41
        BorderWidth = 3
        ButtonHeight = 25
        ButtonWidth = 123
        Caption = 'ToolBar1'
        TabOrder = 1
        object Button10: TButton
          Left = 0
          Top = 0
          Width = 161
          Height = 25
          Action = act_Run_short
          TabOrder = 1
        end
        object Button4: TButton
          Left = 161
          Top = 0
          Width = 130
          Height = 25
          Action = act_Run_full
          TabOrder = 2
        end
        object ToolButton1: TToolButton
          Left = 291
          Top = 0
          Width = 8
          Caption = 'ToolButton1'
          Style = tbsSeparator
        end
        object Button3: TButton
          Left = 299
          Top = 0
          Width = 130
          Height = 25
          Action = act_Save
          TabOrder = 3
        end
        object ToolButton3: TToolButton
          Left = 429
          Top = 0
          Width = 8
          Caption = 'ToolButton3'
          ImageIndex = 0
          Style = tbsSeparator
        end
        object Button2: TButton
          Left = 437
          Top = 0
          Width = 130
          Height = 25
          Action = act_Save_Ini
          ModalResult = 1
          TabOrder = 0
          Visible = False
        end
        object ToolButton2: TToolButton
          Left = 567
          Top = 0
          Width = 8
          Caption = 'ToolButton2'
          ImageIndex = 1
          Style = tbsSeparator
          Visible = False
        end
      end
    end
  end
  object ToolBar4: TToolBar
    Left = 0
    Top = 0
    Width = 1364
    Height = 41
    ButtonHeight = 25
    Caption = 'ToolBar1'
    TabOrder = 1
    Visible = False
    object Button1: TButton
      Left = 0
      Top = 0
      Width = 75
      Height = 25
      Caption = 'Open'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button8_Save: TButton
      Left = 75
      Top = 0
      Width = 75
      Height = 25
      Caption = 'Save'
      TabOrder = 1
      OnClick = Button8_SaveClick
    end
    object Button9_Load: TButton
      Left = 150
      Top = 0
      Width = 75
      Height = 25
      Caption = 'Load'
      TabOrder = 2
      OnClick = Button9_LoadClick
    end
  end
  object qry_Ant: TADOQuery
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    Parameters = <>
    SQL.Strings = (
      'SELECT   top 10  *, '
      ''
      'cast(0 as bit) as checked  ,'
      ''
      ' dbo.fn_Folder_FullPath (folder_id) as folder_name'
      ''
      ''
      'FROM antennaType')
    Left = 426
    Top = 479
  end
  object qry_LinkEndType: TADOQuery
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    Parameters = <>
    SQL.Strings = (
      'SELECT  '
      ''
      ''
      'top 10 *,  '
      ''
      ' dbo.fn_Folder_FullPath (folder_id) as folder_name ,'
      ''
      'cast(0 as bit) as checked'
      ''
      ''
      'FROM linkEndType')
    Left = 538
    Top = 479
  end
  object ds_Ant: TDataSource
    DataSet = qry_Ant
    Left = 432
    Top = 552
  end
  object ds_LinkEndType: TDataSource
    DataSet = qry_LinkEndType
    Left = 536
    Top = 552
  end
  object dxMemData_Ant: TdxMemData
    Active = True
    Indexes = <>
    SortOptions = []
    Left = 72
    Top = 456
  end
  object dxMemData_linkendtype: TdxMemData
    Active = True
    Indexes = <>
    SortOptions = []
    Left = 72
    Top = 512
  end
  object qry_LinkEndType_modes: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * '
      ''
      'FROM linkEndType_mode')
    Left = 674
    Top = 479
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 248
    Top = 656
    object act_LinkEndType_check: TAction
      Caption = 'act_LinkEndType_check'
      OnExecute = act_LinkEndType_checkExecute
    end
    object act_LinkEndType_uncheck: TAction
      Caption = 'act_LinkEndType_uncheck'
      OnExecute = act_LinkEndType_checkExecute
    end
    object act_AntennaType_check: TAction
      Caption = 'act_AntennaType_check'
      OnExecute = act_LinkEndType_checkExecute
    end
    object act_AntennaType_uncheck: TAction
      Caption = 'act_AntennaType_uncheck'
      OnExecute = act_LinkEndType_checkExecute
    end
    object act_Run_short: TAction
      Caption = 'act_Run'
      OnExecute = act_LinkEndType_checkExecute
    end
    object act_Run_full: TAction
      Caption = 'act_Run_fuill'
      OnExecute = act_LinkEndType_checkExecute
    end
    object act_Open: TAction
      Caption = 'act_Open'
      OnExecute = act_LinkEndType_checkExecute
    end
    object act_Save: TAction
      Caption = 'act_Save'
      OnExecute = act_LinkEndType_checkExecute
    end
    object act_Save_Ini: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1072#1081#1083
      OnExecute = act_LinkEndType_checkExecute
    end
  end
  object ds_LinkEndType_modes: TDataSource
    DataSet = qry_LinkEndType_modes
    Left = 672
    Top = 552
  end
  object dxMemData_result: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 256
    Top = 488
    object dxMemData_resultLinkEndType_name: TStringField
      FieldName = 'LinkEndType_name'
    end
    object dxMemData_resultAntEnnaType_name: TStringField
      FieldName = 'AntEnnaType_name'
    end
    object dxMemData_resultLinkEndType_id: TIntegerField
      FieldName = 'LinkEndType_id'
    end
    object dxMemData_resultAntEnnaType_id: TIntegerField
      FieldName = 'AntEnnaType_id'
    end
    object dxMemData_resultLinkEndType_mode_id: TIntegerField
      FieldName = 'LinkEndType_mode_id'
    end
    object dxMemData_resultBitRate_Mbps: TFloatField
      FieldName = 'BitRate_Mbps'
    end
    object dxMemData_resultPower_dBm: TFloatField
      FieldName = 'Power_dBm'
    end
    object dxMemData_resultTHRESHOLD: TFloatField
      FieldName = 'THRESHOLD_BER_3'
    end
    object dxMemData_resultRxLevel: TFloatField
      FieldName = 'RxLevel_dBm'
    end
  end
  object ds_dxMemData_result: TDataSource
    DataSet = dxMemData_result
    Left = 256
    Top = 544
  end
  object ADOStoredProc_ant: TADOStoredProc
    CursorType = ctStatic
    ProcedureName = 'sp_Task_Link_Optimize_equipment'
    Parameters = <>
    Left = 808
    Top = 480
  end
  object ADOStoredProc_linkend: TADOStoredProc
    CursorType = ctStatic
    ProcedureName = 'sp_Task_Link_Optimize_equipment;1'
    Parameters = <>
    Left = 928
    Top = 480
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc_ant
    Left = 800
    Top = 544
  end
  object DataSource2: TDataSource
    DataSet = ADOStoredProc_linkend
    Left = 920
    Top = 544
  end
  object q_Link: TADOQuery
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'SELECT   top 10  *, '
      ''
      'cast(0 as bit) as checked  ,'
      ''
      ' dbo.fn_Folder_FullPath (folder_id) as folder_name'
      ''
      ''
      'FROM antennaType')
    Left = 1042
    Top = 479
  end
  object ds_Link: TDataSource
    DataSet = q_Link
    Left = 1040
    Top = 544
  end
  object PopupMenu_Antenna: TPopupMenu
    Left = 56
    Top = 584
    object actAntennaTypecheck1: TMenuItem
      Action = act_AntennaType_check
    end
    object actAntennaTypeuncheck1: TMenuItem
      Action = act_AntennaType_uncheck
    end
  end
  object PopupMenu_LinkEndType: TPopupMenu
    Left = 56
    Top = 656
    object MenuItem1: TMenuItem
      Action = act_LinkEndType_check
    end
    object MenuItem2: TMenuItem
      Action = act_LinkEndType_uncheck
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\opt_equip'
    StoredValues = <>
    Left = 424
    Top = 656
  end
  object PopupMenu1_result: TPopupMenu
    Left = 248
    Top = 600
    object MenuItem3: TMenuItem
      Action = act_Save
    end
  end
  object qry_LinkEnd1: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT   top 10  *, '
      ''
      'cast(0 as bit) as checked  ,'
      ''
      ' dbo.fn_Folder_FullPath (folder_id) as folder_name'
      ''
      ''
      'FROM antennaType')
    Left = 1130
    Top = 479
  end
  object ds_LinkEnd: TDataSource
    DataSet = qry_LinkEnd1
    Left = 1128
    Top = 544
  end
end
