unit dm_Link_calc_data;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms,  Variants, Math, Dialogs,  StrUtils,
  XMLIntf,

  d_Message,

  u_dlg,

  u_Profiler,   

//  u_LinkCalcResult,
  u_LinkCalcResult_new,

  u_link_model_layout,

  u_Ini_rpls_rrl_params,

  u_str,
  u_xml_document,

  d_AdaptiveModulation,

  u_link_calc_classes_main,

  u_link_calc_classes_export,

  //u_LinkCalcResult_loader,

  u_db_manager,

  dm_Onega_DB_data,
  u_vars,     

  u_rrl_param_rec_new, //shared

  u_rel_Profile,

//  u_types,

  u_Link_const,

  u_const_db,
  u_const,

  u_classes,
  u_db,
  u_files,
  u_func,
  u_Geo,
  u_run,

  u_Link_CalcRains,
  dm_Main,

  dm_Link_Calc_SESR,
  dm_Rel_Engine,

  u_ini_LinkCalcParams, dxmdaset;

  

type
  TdmLink_calc_data = class(TDataModule)
    qry_LinkEnd1: TADOQuery;
    qry_Antennas1: TADOQuery;
    qry_LinkEnd2: TADOQuery;
    qry_Antennas2: TADOQuery;
    ADOStoredProc_Link: TADOStoredProc;
    qry_Modes: TADOQuery;
    q_LInk_repeater: TADOQuery;
    ds_LInk_repeater: TDataSource;
    q_LInk_repeater_ant: TADOQuery;
    ds_LInk_repeater_ant: TDataSource;
    ds_LinkEnd1: TDataSource;
    ds_LinkEnd2: TDataSource;
    ds_Antennas1: TDataSource;
    ds_Antennas2: TDataSource;
    dxMemData1: TdxMemData;
    dxMemData_Groups: TdxMemData;
    dxMemData_Items: TdxMemData;
  private
    procedure Profile_Open(aDBLink: TDBLink);
    procedure Profile_Open_repeater(aDBLink: TDBLink);
    { Private declarations }
  public
    class procedure Init;
    
    procedure OpenData_new(aID: Integer; aLink: TDBLink);
    { Public declarations }
  end;

var
  dmLink_calc_data: TdmLink_calc_data;

implementation

{$R *.dfm}

class procedure TdmLink_calc_data.Init;
begin
  if not Assigned(dmLink_calc_data) then
    dmLink_calc_data := TdmLink_calc_data.Create(Application);

end;

//--------------------------------------------------------------------
procedure TdmLink_calc_data.OpenData_new(aID: Integer; aLink: TDBLink);
//--------------------------------------------------------------------
const
  SQL_SELECT_LINKEND_ANT = //band,
     'SELECT * FROM '+ TBL_LINKEND_ANTENNA +

     ' WHERE linkend_id=:linkend_id '+
     ' ORDER BY is_master desc, height desc';


 // SQL_SELECT_LINKEND=
  //   'SELECT * FROM ' + TBL_LinkEnd + ' WHERE id=:id ';

  SQL_SELECT_LINKEND=
    'SELECT L.*, LinkEndType.band AS LinkEndType_band '+
    ' FROM  LinkEnd L LEFT OUTER JOIN LinkEndType ON L.linkendtype_id = dbo.LinkEndType.id ' +
    ' WHERE L.id=:id ';



//  SQL_SELECT_PMP_SECTOR=
//      'SELECT * FROM ' + TBL_PMP_Sector + ' WHERE id=:id ';

//  SQL_SELECT_PMP_TERMINAL=
//     'SELECT *  FROM ' + TBL_PmpTerminal + ' WHERE id=:id ';


     {
  SQL_SELECT_PMP_SECTOR_ANT =
     'SELECT * FROM '+ TBL_LINKEND_ANTENNA +
     ' WHERE PMP_SECTOR_id=:PMP_SECTOR_id '+
     ' ORDER BY height desc';


  SQL_SELECT_PMP_TERMINAL_ANT =
     'SELECT * FROM '+ TBL_LINKEND_ANTENNA +
//     'SELECT * FROM '+ view_PMP_TERMINAL_ANTENNAS +
     ' WHERE PMP_TERMINAL_id=:PMP_TERMINAL_id ORDER BY height desc';
   }

  // for AM
  SQL_SELECT_LINKENDType_MODES =
     'SELECT * FROM ' + TBL_LinkEndType_Mode +
     ' WHERE LINKENDType_id=:id '+
     ' ORDER by bitrate_Mbps';


var
  I: Integer;
  iPmpSectorID,iPmpTerminalID,
  iLinkEndID1,iLinkEndID2: integer;
  dFreq,f1,f2: double;
  iLink_repeater_ID: Integer;
begin
  //Assert(aID>0);

  g_Profiler.Start('function TdmLink_calc.OpenData');


 // FLinkID:=aID;


  Assert(Assigned(dmOnega_DB_data), 'Value not assigned');

  dmOnega_DB_data.Link_Select(ADOStoredProc_Link, aID);
  Assert(ADOStoredProc_Link.recordCount=1, 'ADOStoredProc_Link.recordCount=1');

//  db_View( ADOStoredProc_Link);


//  if not dmLink.GetInfoFromDataset (ADOStoredProc_Link, FLinkParamRec) then
//  if not dmLink.GetInfo (aID, FLinkParamRec) then
 //   exit;                                               
//
  // Added by Aleksey 21.07.2009 18:38:48

   aLink.LoadFromDataset(ADOStoredProc_Link);



   iLinkEndID1:=aLink.LinkEnd1_ID;
   iLinkEndID2:=aLink.LinkEnd2_ID;


   db_OpenQuery (qry_LinkEnd1, SQL_SELECT_LINKEND, [db_Par(FLD_ID, iLinkEndID1)]);
   db_OpenQuery (qry_LinkEnd2, SQL_SELECT_LINKEND, [db_Par(FLD_ID, iLinkEndID2)]);


   db_OpenQuery (qry_Antennas1, SQL_SELECT_LINKEND_ANT,     [db_Par(FLD_LINKEND_ID, iLinkEndID1)]);


 //  db_ViewDataSet(qry_Antennas1);

//    tools_DatasetToClass(qry_Antennas1, 'd:\ant.pas');

   db_OpenQuery (qry_Antennas2, SQL_SELECT_LINKEND_ANT,    [db_Par(FLD_LINKEND_ID, iLinkEndID2)]);


 // db_ViewDataSet(qry_Antennas2);


   if qry_LinkEnd1.IsEmpty or (qry_LinkEnd2.IsEmpty) then
     raise Exception.Create ('qry_LinkEnd1.IsEmpty or qry_LinkEnd2.IsEmpty');

     {

  case aLink.LinkType of    //
//  case FLinkParamRec.LinkType of    //
    // -------------------------------------------------------------------
    ltLink_:  begin
    // -------------------------------------------------------------------


       iLinkEndID1:=aLink.LinkEnd1_ID;
       iLinkEndID2:=aLink.LinkEnd2_ID;


       db_OpenQuery (qry_LinkEnd1, SQL_SELECT_LINKEND, [db_Par(FLD_ID, iLinkEndID1)]);
       db_OpenQuery (qry_LinkEnd2, SQL_SELECT_LINKEND, [db_Par(FLD_ID, iLinkEndID2)]);


       db_OpenQuery (qry_Antennas1, SQL_SELECT_LINKEND_ANT,     [db_Par(FLD_LINKEND_ID, iLinkEndID1)]);


     //  db_ViewDataSet(qry_Antennas1);

   //    tools_DatasetToClass(qry_Antennas1, 'd:\ant.pas');

       db_OpenQuery (qry_Antennas2, SQL_SELECT_LINKEND_ANT,    [db_Par(FLD_LINKEND_ID, iLinkEndID2)]);


     // db_ViewDataSet(qry_Antennas2);


       if qry_LinkEnd1.IsEmpty or (qry_LinkEnd2.IsEmpty) then
         raise Exception.Create ('qry_LinkEnd1.IsEmpty or qry_LinkEnd2.IsEmpty');

     end;

    // -------------------------------------------------------------------
    ltPmpLink_: begin
    // -------------------------------------------------------------------

//       iPmpSectorID  :=ADOStoredProc_Link.FieldByName(FLD_PMP_SECTOR_ID).AsInteger;
//       iPmpTerminalID:=ADOStoredProc_Link.FieldByName(FLD_PMP_TERMINAL_ID).AsInteger;

       db_OpenQuery (qry_LinkEnd1, SQL_SELECT_PMP_SECTOR,   [db_Par(FLD_ID, iPmpSectorID)]);
       db_OpenQuery (qry_LinkEnd2, SQL_SELECT_PMP_TERMINAL, [db_Par(FLD_ID, iPmpTerminalID)]);

       Assert((not qry_LinkEnd1.IsEmpty) and (not qry_LinkEnd2.IsEmpty));


     ///  db_View(qry_LinkEnd1);
      /// db_View(qry_LinkEnd2);



       db_OpenQuery (qry_Antennas1, SQL_SELECT_PMP_SECTOR_ANT,
                    [db_Par(FLD_PMP_SECTOR_ID, iPMPSECTORID)]);

       db_OpenQuery (qry_Antennas2, SQL_SELECT_PMP_TERMINAL_ANT,
                    [db_Par(FLD_PMP_TERMINAL_ID, iPmpTerminalID )]);

 //      if (qry_LinkEnd1.IsEmpty) or (qry_LinkEnd2.IsEmpty) then
   //      raise Exception.Create ('qry_LinkEnd1.IsEmpty or qry_LinkEnd2.IsEmpty');

    end;
  end;
  }

  g_Profiler.Stop('function TdmLink_calc.OpenData');



 ///// db_View(qry_LinkEnd2);


  aLink.LinkEnd1.LoadFromDataset(qry_LinkEnd1);
  aLink.LinkEnd2.LoadFromDataset(qry_LinkEnd2);



  aLink.LinkEnd1.Antennas.LoadFromDataset(qry_Antennas1);
  aLink.LinkEnd2.Antennas.LoadFromDataset(qry_Antennas2);

//  Result:= aLink.Validate_Critical(nil);
 // Result:=Result and aLink.Validate_Bands(nil);

//  if not Result then
 //   Exit;


  if aLink.LinkRepeater.ID > 0 then
  begin

    dmOnega_DB_data.OpenQuery(q_LInk_repeater,
      Format('SELECT * FROM %s WHERE id=%d',
           [view_LINK_REPEATER, aLink.LinkRepeater.ID ]));

    Assert(q_LInk_repeater.RecordCount>0);

    aLink.LinkRepeater_LoadFromDataset(q_LInk_repeater);
//    Link.LinkRepeater_Restore;

   //  db_View(q_LInk_repeater);

    dmOnega_DB_data.OpenQuery(q_LInk_repeater_ant,
      Format('SELECT * FROM %s WHERE Link_repeater_id=%d',
           [VIEW_LINK_REPEATER_ANTENNA, aLink.LinkRepeater.ID]));


    aLink.LinkRepeater.Antennas.LoadFromDataset(q_LInk_repeater_ant);

  end;


 ///////// ShowMessage('test - Params.IsUseAdaptiveModulation');

//  Params.IsUseAdaptiveModulation := True;


  {
  //-------------------------------------------------------------------
  // IsUseAdaptiveModulation
  //-------------------------------------------------------------------
  if Params.IsUseAdaptiveModulation then
  begin
    db_OpenQuery (qry_Modes, SQL_SELECT_LINKENDType_MODES,
         [db_Par(FLD_ID, aLink.LinkEnd1.LinkEndType_ID)]);

    Setlength(FAdaptiveModeArr, qry_Modes.RecordCount);
  //  Setlength(FAdaptiveModeArr, 2);

    for I := 0 to qry_Modes.RecordCount - 1 do
  //  for I := 0 to 1 do
    begin
      FAdaptiveModeArr[i].id   :=qry_Modes.FieldByName(FLD_id).AsInteger;
      FAdaptiveModeArr[i].Mode :=qry_Modes.FieldByName(FLD_Mode).AsInteger;

      FAdaptiveModeArr[i].THRESHOLD_BER_3 :=qry_Modes.FieldByName(FLD_THRESHOLD_BER_3).AsFloat;
      FAdaptiveModeArr[i].THRESHOLD_BER_6 :=qry_Modes.FieldByName(FLD_THRESHOLD_BER_6).AsFloat;

      FAdaptiveModeArr[i].Power_dBm       :=qry_Modes.FieldByName(FLD_Power_max).AsFloat;
      FAdaptiveModeArr[i].BitRate_Mbps    :=qry_Modes.FieldByName(FLD_BitRate_Mbps).AsFloat;

      FAdaptiveModeArr[i].Modulation_type :=qry_Modes.FieldByName(FLD_Modulation_type).AsString;

      qry_Modes.Next;
    end;

  end;


//  db_View(qry_Modes);

  if aLink.LinkRepeater.ID =0  then
    Profile_Open(aLink)
  else
    Profile_Open_repeater(aLink);
  }
  
end;

//-------------------------------------------------------------------
procedure TdmLink_calc_data.Profile_Open(aDBLink: TDBLink);
//-------------------------------------------------------------------
var
  d: double;
  iProfileStep_m: Integer;
  sProfile_XML: string;
  sTempFile: string;

  blVector: TBLVector;
  eLen_km: Double;
  iRes: Integer;
begin
//  dmRel_Engine.AssignClutterModel (aDBLink.ClutterModel_ID, FrelProfile.Clutters);
  dmRel_Engine.AssignClutterModel (aDBLink.ClutterModel_ID, aDBLink.relProfile.Clutters);


  sProfile_XML:=aDBLink.Profile_XML;


  aDBLink.IsProfileExists:=True;

 ////// sProfile_XML:='';

  iProfileStep_m:=aDBLink.Profile_Step_m;

  eLen_km:= geo_Distance_KM(aDBLink.BLVector);


//      blVector:= Link.BLVector;


//function TrelProfile.LoadFromXml(aValue: string; aLength_km: double): Boolean;

  if (sProfile_XML='') or
     (not aDBLink.relProfile.LoadFromXml(sProfile_XML, eLen_km)) then
  begin

    blVector:= aDBLink.BLVector;

    dmRel_Engine.Open; //ByVector (blVector);

    aDBLink.IsProfileExists:=
      dmRel_Engine.BuildProfile1 (aDBLink.relProfile, blVector, iProfileStep_m);//, FClutterModelID);


  //  FIsProfileExists:=True;
//
//    if FIsProfileExists then
//    begin
//    end;

  end else
  begin
//    FrelProfile.isDirectionReversed := Link.is_profile_reversed;

  end;

   aDBLink.relProfile.isDirectionReversed := aDBLink.is_profile_reversed;

  //---------------------------------------------------------
  aDBLink.relProfile.SetRefraction(aDBLink.Refraction);
  aDBLink.relProfile.Data.Distance_KM := aDBLink.GetLength_km;

end;

//-------------------------------------------------------------------
procedure TdmLink_calc_data.Profile_Open_repeater(aDBLink: TDBLink);
//-------------------------------------------------------------------
var
  d: double;
  iProfileStep_m: Integer;
 // sProfile_XML: string;
  sTempFile: string;

  blVector: TBLVector;
  eLen_km: Double;
  iRes: Integer;
  sProfile1_XML: string;
  sProfile2_XML: string;
begin
  dmRel_Engine.AssignClutterModel (aDBLink.ClutterModel_ID, aDBLink.relProfile.Clutters);


  sProfile1_XML:=aDBLink.LinkRepeater.Part1.Profile_XML;
  sProfile2_XML:=aDBLink.LinkRepeater.Part2.Profile_XML;

  aDBLink.IsProfileExists1:=False;
  aDBLink.IsProfileExists2:=False;

  iProfileStep_m:=aDBLink.Profile_Step_m;


  dmRel_Engine.Open; //ByVector (aDBLink.BLVector);

  eLen_km:= geo_Distance_KM(aDBLink.LinkRepeater.Part1.BLVector);

  // -------------------------
  if (sProfile1_XML='') or (not aDBLink.relProfile1.LoadFromXml(sProfile1_XML, eLen_km)) then
  begin

    blVector:= aDBLink.LinkRepeater.Part1.BLVector;

  //  dmRel_Engine.OpenByVector (blVector);

    aDBLink.IsProfileExists1:=
      dmRel_Engine.BuildProfile1 (aDBLink.relProfile1, blVector, iProfileStep_m);//, FClutterModelID);


  end;

  //---------------------------------------------------------
  aDBLink.relProfile1.SetRefraction(aDBLink.Refraction);
  aDBLink.relProfile1.Data.Distance_KM := aDBLink.LinkRepeater.Part1.Length_KM;

  eLen_km:= geo_Distance_KM(aDBLink.LinkRepeater.Part2.BLVector);


  // -------------------------
  if (sProfile2_XML='') or (not aDBLink.relProfile2.LoadFromXml(sProfile2_XML,eLen_km)) then
  begin

    blVector:= aDBLink.LinkRepeater.Part2.BLVector;

//    dmRel_Engine.OpenByVector (blVector);

    aDBLink.IsProfileExists2:=
      dmRel_Engine.BuildProfile1 (aDBLink.relProfile2, blVector, iProfileStep_m);//, FClutterModelID);


  end;

  //---------------------------------------------------------
  aDBLink.relProfile2.SetRefraction(aDBLink.Refraction);
  aDBLink.relProfile2.Data.Distance_KM := aDBLink.LinkRepeater.Part2.Length_KM;


end;




end.
