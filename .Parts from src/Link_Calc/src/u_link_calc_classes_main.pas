unit u_link_calc_classes_main;

interface
//{$DEFINE new}


uses
  DB, Classes, SysUtils, dialogs,

  u_reflection_points,

  u_rel_Profile,

  u_db,

  u_func,
  u_dlg,

  u_Link_const,
  u_const_db,

  u_Geo;


type
  TDBLinkReflectionPointList = class;
  TDBAntennaList             = class;
  TDBLinkEnd                 = class;
  TDBLinkRepeater            = class;

  //status
  TDBLinkWorkType = (lwtNotDefined,lwtWork,lwtNoWork);

  TDBLinkType = (ltLink_,ltPmpLink_);


  TDBLink = class(TCollectionItem)
  private
    ReflectionPoints: TDBLinkReflectionPointList;
  public

    RelProfile: TrelProfile;
    IsProfileExists: boolean;

    // ---------------------------------------------------------------
    RelProfile1: TrelProfile;
    RelProfile2: TrelProfile;

    IsProfileExists1: boolean;
    IsProfileExists2: boolean;



  public
     Name_  : string;

     id : Integer;

//    Runtime: record
//                IsUsePassiveElements : Boolean;
//              end;


    Is_UsePassiveElements     : Boolean;
    Is_CalcWithAdditionalRain : Boolean;


//    LinkType: TDBLinkType;

    Project_id : Integer;

{
    LinkEnd1,
    LinkEnd2: record
      ID           : Integer;
      Name         : string;
    end;
}

    LinkEnd1_ID: Integer;
    LinkEnd2_ID: Integer;

//    PmpSector_ID: Integer;
//    PmpTerminal_ID: Integer;


    Property1,
    Property2: record
      ID           : Integer;
      Pos          : TBLPoint;
      Ground_Height: Variant;
      Name         : string;
    end;


    PropertyID1: Integer;
    PropertyID2: Integer;

    Property1_Pos: TBLPoint;
    Property2_Pos: TBLPoint;

    Property1_Ground_Height: Variant;
    Property2_Ground_Height: Variant;


    BLVector   :  TBLVector;

    Calc_method : Integer; // 0 "����, ITU-R � 16 ����� ��" (�� ���������)
				                   // 1 "���� � 53363 - 2009"
                           // 0 "���� 1998

    ClutterModel_ID: Integer;

    Is_profile_reversed : Boolean;

    Profile_Step_m: Integer;


    // ����� -----------------------------
    Rains: record
              IsCALCLENGTH : Boolean;
              rain_INTENSITY : Double;
           end;


    BER_REQUIRED_Index : Integer; //IIF((iBer_Required=0), STR_BER3 , STR_BER6);

    NFrenel: Double;


    //TRB: array[1..4] of double;  //���������� � ����������� �������� ���
     {1=0.012      ;����������� �������� SESR ��� ����, %
      2=0.05       ;����������� �������� ���  ��� ����, %
      3=600        ;����� ����, ��
      4=0.200      ;���������� ����� ���, ������������� �������������}


//    GST_equivalent_length_km : Double;

    Trb: record
      GST_TYPE : Integer;

      GST_LENGTH_km_ : Double;
     // GST_equivalent_length_km: Double;

      GST_SESR      : Double; // '����������� �������� SESR ��� ����, %');
      GST_Kng       : Double; // '����������� �������� ���  ��� ����, %');

      KNG_dop       : Double; // '���������� ����� ���, ������������� �������������}');
      SPACE_LIMIT   : double; // '���������� ������������� ������� p(a%)');
      SPACE_LIMIT_PROBABILITY: Double; //'���������� ����������� a% ������������� ������������ �������� p(a%) [%]');


//      Fade_margin_required : Double; //��������� ����� �� ��������� [dB] (0-������� ������� �� ��������� ����������)"
    end;


//    GST_length_km :double;
 //   GST_Type      :double;




    //�������� ������ ������� V����(g)=V���.�, ��
    Precision_of_condition_V_diffration_is_equal_to_V_min_dB : Double;

    REFRACTION:     Double; // '�����. ��������� (��� ��.���������=0, ����� �.�.=0)');

    //--------------------------
    // ITU-R
    //--------------------------
    RRV_ITU: record
    // -------------------------
      gradient_diel           : Double;  //1 '������� �������� ��������� ����.�������������,10^-8 1/�' );
      gradient_deviation      : Double;  //2 '����������� ���������� ���������,10^-8 1/�' );

      Terrain_type            : Integer;//3 '��� ���������(0...2) 1-���������� 2-���������� (��������) 3-���������� (������)' );
      underlying_terrain_type : Double; //4 '��� ������������ �������.(1...3,0->����.�����=����.���.)' );

      Steam_wet               : Double; // 5 '���������� ��������� �������� ����, �/���.�');

      Air_temperature         : double;   //6. ����������� ������� [��.�]" (�� ��������� 15)

      Atmosphere_pressure     : Double;//7 ����������� �������� [����]" (�� ��������� 1013)

      Q_factor                : Double; // 8 'Q-������ ������ �����������');
      climate_factor          : double; // 9 '������������� ������ K��, 10^-6');
      factor_B                : Double; // 10 '�������� ������������ b ��� ������� ������');
      factor_C                : Double; // 11 '�������� ������������ c ��� ������� ������');
      FACTOR_D                : Double; // 12 '�������� ������������ d ��� ������� ������');

//      RAIN_RATE             :      Double; // 13 '������������� ����� � ������� 0.01% �������, ��/���');
      RAIN_intensity          : Double; // 13 '������������� ����� � ������� 0.01% �������, ��/���');

        //mm_per_hour
    end;

    // -------------------------
    GOST: record
    // -------------------------
      gradient_diel           : Double;  //1 '������� �������� ��������� ����.�������������,10^-8 1/�' );
      gradient_deviation      : Double;  //2 '����������� ���������� ���������,10^-8 1/�' );

      Terrain_type            : Integer;//3 '��� ���������(0...2) 1-���������� 2-���������� (��������) 3-���������� (������)' );
      underlying_terrain_type : Double; //4 '��� ������������ �������.(1...3,0->����.�����=����.���.)' );
      Steam_wet               : Double; // 5 '���������� ��������� �������� ����, �/���.�');
      Air_temperature         : double;   //6. ����������� ������� [��.�]" (�� ��������� 15)
      Atmosphere_pressure     : Double;//7 ����������� �������� [����]" (�� ��������� 1013)
      climate_factor          : double; // 9 '������������� ������ K��, 10^-6');
      RAIN_intensity          : Double; // 9.����������� ������������� �����
//      Link_Center1             : TBLPoint; //���������� ������� ��������� � ��������
    end;

    // -------------------------
    NIIR1998 : record
    // -------------------------
      gradient_diel       : Double; //1 '������� �������� ��������� ����.�������������,10^-8 1/�' );
      gradient_deviation  : Double; //2 '����������� ���������� ���������,10^-8 1/�' );

      Terrain_type        : Integer;//3 '��� ���������(0...2) 1-���������� 2-���������� (��������) 3-���������� (������)' );
      Steam_wet           : Double; // 5 '���������� ��������� �������� ����, �/���.�');
      Air_temperature     : double; //6. ����������� ������� [��.�]" (�� ��������� 15)
      Atmosphere_pressure : Double; //7 ����������� �������� [����]" (�� ��������� 1013)

      rain_region_number  : Integer;//10 ����� ������ �� ����� ���� (���.8.11) ��� ������������� ������
      Qd_region_number    : Integer;//11 ����� ������ �� ����� ���� (���.8.15) ��� ��������� Q�
      water_area_percent  : Double; //K�� (%) � ���� ������ ����������� �� ���������

    end;

   // -------------------------
    RRV_E_band_ITU : record  //3.�-�������� (��.ITU-R)
    // -------------------------
      Param_1 : Double;  //	1.������� �������� ��������� ����. �������������, 10^-8, 1/�	1	�� �����. (-9)
      Param_2 : Double;  //	2.����������� ���������� ���������,10^-8, 1/�	2	�� �����. 7
      Param_3 : Double;  //	3.���������� ��������� �������� ����, �/���.�	4	�� �����. 7.5
      Param_4 : double;  //	4.����������� �������, ��.�	12	�� �����. 15
      Param_5 : Double;  //	5.����������� ��������, ����	13	�� �����. 1013
      Param_6 : double;  //	6.������������� ����� � ������� 0.01% �������, ��/���	10	�� �����.20

    end;

  (*  // -------------------------
    RRV_E_band_GOST : record //4.�-�������� (��.����)
    // -------------------------
      Param_1 : Double; //	1. ������� �������� ��������� ����. �������������,10^-8, 1/�	1	�� �����. (-9)
      Param_2 : Double; //	2.����������� ���������� ���������,10^-8, 1/�	2	�� �����. 7
      Param_3 : Double; //	3.���������� ��������� �������� ����, �/���.�	4	�� �����. 7.5
      Param_4 : double; //	4.����������� �������, ��.�	12	�� �����. 15
      Param_5 : double; //	5.����������� ��������, ����	13	�� �����. 1013
      Param_6 : double; //	6.����������� ������������� �����	19	�� �����. 1
  //    Param_7 : double; //*	7.���������� ������� ��������� (������), ��	15	�� �����.60
   //   Param_8 : Double; //*	8.���������� ������� ��������� (�������), ��	16	�� �����.30
    end;

    // -------------------------
    RRV_E_band_NIIR : record //5.�-�������� (��.����)
    // -------------------------
      Param_1 : Double; // 1.������� �������� ��������� ����. �������������,10^-8, 1/�	1	�� �����. (-9)
      Param_2 : Double; // 2.����������� ���������� ���������,10^-8, 1/�	2	�� �����. 7
      Param_3 : Double; // 3.���������� ��������� �������� ����, �/���.�	4	�� �����. 7.5
      Param_4 : double; // 4.����������� �������, ��.�	12	�� �����. 20
      Param_5 : double; // 5.����������� ��������, ����	13	�� �����. 1013
      Param_6 : double; // 6.����� ������ �� ����� ������������� ������ (1...22)	17	�� �����.1
      Param_7 : double; // 7.����� ������ (������) �� ����� Qd (1�22)	18	�� �����.1
    end;
 *)
    // -------------------------
    RRV_UKV : record  //    6.���-�������� (16 �����)
    // -------------------------
      Param_1 : Double; // 1.������� �������� ��������� ����. �������������, 10^-8, 1/�	1	�� �����. (-8)
      Param_2 : Integer;// 2.��� ������������ ����������� (1-������������.(����.), 2-�������������., 3-����.)	3	� xml ���������� ��-��: 1,2,3 (�� �����.2)
      Param_3 : Double; // 3.��������.����������� c (0-�����.����.�� ���)	8	�� �����.0
      Param_4 : double; // 4.��� ������ ������� (��� �>0) [dB] (0-���������)	12	�� �����.0
    end;

//    end;



    ESR_norm      : double;
    ESR_required  : Double;
    BBER_norm     : Double;
    BBER_required : Double;


//    Has_Repeater : boolean;

    Profile_XML: string;

 //   Link_Repeater_id : Integer;

//    property Length_km: double read GetLength_km_;
  public
    LinkEnd1: TDBLinkEnd;
    LinkEnd2: TDBLinkEnd;

    LinkRepeater: TDBLinkRepeater;

    ReflectionPointsArray : TRelReflectionPointRecArray;


    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
   // Length_m : Double;

    function GetLength_km: double;

    procedure LoadFromDataset(aDataset: TDataset);

    procedure LinkRepeater_LoadFromDataset(aDataset: TDataset);
    procedure LoadFromDataset_ReflectionPoints(aDataset: TDataset);

    function Validate_Critical(aList: TStrings): Boolean;
    function Validate_Bands_(aList: TStrings): Boolean;
  end;

  // ---------------------------------------------------------------
  TDBLinkList = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TDBLink;
  public
    constructor Create;
    function AddItem: TDBLink;

    property Items[Index: Integer]: TDBLink read GetItems; default;
  end;



  // ---------------------------------------------------------------
  TDBLinkReflectionPoint = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
    DISTANCE_to_center_km : Double; //���������� �� ������� 1-�� ������� ��������� [km]
    ABS_PROSVET_m : Double; //A��������� ������� ��� �������� 1-�� ������� �����. [m]
                            //����������� ������� ��� �������� 1-�� �������
                            //�����������.������� ��� ��������
    Length_km : Double;     //������������� 1-�� �������
    RADIUS_km : Double;     //P����� �������� 1-�� �������

    Signal_Depression_dB  : Double;     //���������� �� 1-�� ������� ��������� [dB]
  end;

  // ---------------------------------------------------------------
  TDBLinkReflectionPointList = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TDBLinkReflectionPoint;
  public
    constructor Create;
    function AddItem: TDBLinkReflectionPoint;

    property Items[Index: Integer]: TDBLinkReflectionPoint read GetItems; default;
  end;

  // ---------------------------------------------------------------
  TDBLinkEnd = class
  // ---------------------------------------------------------------
  private
    function GetTx_Freq_GHz: double;
  public
    property Tx_Freq_GHz: double read GetTx_Freq_GHz;
  public
    Name_ : string;

    Antenna_offset_horizontal_m : double;


    Band : string;

    LinkEndType_ID : Integer;

    THRESHOLD_BER_3    : double;
    THRESHOLD_BER_6    : double;
    Power_dBm          : double;

    TX_FREQ_MHz        : Double;

    BitRate_Mbps       : double;

    Passive_element_LOSS_dBm  : Double;
    LOSS_dBm           : Double;
    KNG                : Double;

    KRATNOST_BY_SPACE  : Integer;

    KRATNOST_BY_FREQ   : Integer;
    FREQ_CHANNEL_COUNT : Integer;

    Freq_Spacing_MHz   : Double;

    SIGNATURE_WIDTH : double;// '������ ���������, ���');
    SIGNATURE_HEIGHT: double;// '������ ���������, ��');
    MODULATION_COUNT: double;// '����� ������� ���������');
//    EQUALISER_PROFIT1: double;// '�������, �������� ������������, ��');


    ATPC_profit_dB  : double;// <param id="45" value="10" comment="������� �� ATPC, ��"/>
    Use_ATPC        : Boolean;
    Threshold_degradation_dB: Double; //Threshold_degradation_dB,  '���������� ����������������, ��');

    GOST_53363_modulation : string;
    GOST_53363_RX_count_on_combined_diversity : Integer;


    XPIC_42_use_2_stvol: Integer;
    XPIC_49_coef: double; //,  '49.����.�����.���.��� ��.�����.[��]');
    XPIC_50: double; //,  '50�����.�� �������.����� [��]');

{
  DoAdd (vGroup, 49, TTX.
  DoAdd (vGroup, 50, TTX.XPIC_50, '');
}

  public
    Antennas: TDBAntennaList;

    constructor Create;
    destructor Destroy; override;

    procedure LoadFromDataset(aDataset: TDataset);
  end;


  TDBAntennaPolarizationType = (ptV__,ptH__,ptX__);

  // ---------------------------------------------------------------
  TDBAntenna = class(TCollectionItem)
  // ---------------------------------------------------------------
  private
    Polarization_str    : string;

    AntennaType_ID  : integer;
  public
    Name_ : string;

    Height              : Double;
    Diameter            : Double;
    Gain                : Double;
    repeater_gain       : Double;
    Vert_width          : Double;
    Loss                : Double;

    Freq_Mhz            : Double;
    Band                : string;


    function GetPolarization1: Integer;
    function GetPolarizationType: TDBAntennaPolarizationType;

    procedure LoadFromDataset(aDataset: TDataset);

    property PolarizationType: TDBAntennaPolarizationType read GetPolarizationType;
  end;

  // ---------------------------------------------------------------
  TDBAntennaList = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TDBAntenna;
  public
    constructor Create;
    function AddItem: TDBAntenna;

    procedure LoadFromDataset(aDataset: TDataset);

    property Items[Index: Integer]: TDBAntenna read GetItems; default;
  end;


{
  // ---------------------------------------------------------------
  TDBAntenna = class(TCollectionItem)
  // ---------------------------------------------------------------

  public


  end;

}

  // ---------------------------------------------------------------
  TDBLinkRepeater = class
  // ---------------------------------------------------------------
  private
  protected
  public
     ID : Integer;

     Property_Ground_Height : Variant;

     Property_ID : Integer;
     Property_Pos: TBLPoint;

 //    BLVector1: TBLVector;
  //   BLVector2: TBLVector;



     Part1,Part2: record
        BLVector: TBLVector;

       Profile_XML : string;
       Length_KM : double;
       GST_Length_KM : double;

     end;


     Antenna1,Antenna2:  record
       Height : double;
       Gain : double;
       Loss : double;
     end ;

  //      _Height : double;
   //  Antenna2_Height : Double;



     calc_results_xml_1 : string;
     calc_results_xml_2 : string;
     calc_results_xml_3 : string;


  public
    Antennas: TDBAntennaList;
  public
    constructor Create;
    destructor Destroy; override;

    procedure LoadFromDataset(aDataset: TDataset);
  end;


implementation

//const
{
  DEF_Calc_Method_ITU_R       = 0;
  DEF_Calc_Method_GOST        = 1;
  DEF_Calc_Method_NIIR_1998   = 2;

  DEF_Calc_Method_E_BAND_ITU  = 3;
  DEF_Calc_Method_E_BAND_GOST = 4;
  DEF_Calc_Method_E_BAND_NIIR = 5;
  DEF_Calc_Method_UKV         = 6;
 }


 // FLD_Property1_dot_Name = 'Property1.Name';
//  FLD_Property2_dot_Name = 'Property2.Name';



//---------------------------------------------------------------------------
constructor TDBLink.Create(Collection: TCollection);
//---------------------------------------------------------------------------
begin
  inherited Create(Collection);

  ReflectionPoints := TDBLinkReflectionPointList.Create();
  LinkEnd1 := TDBLinkEnd.Create();
  LinkEnd2 := TDBLinkEnd.Create();
  LinkRepeater := TDBLinkRepeater.Create();


  relProfile:=TrelProfile.Create;

  relProfile1:=TrelProfile.Create;
  relProfile2:=TrelProfile.Create;



end;

//---------------------------------------------------------------------------
destructor TDBLink.Destroy;
//---------------------------------------------------------------------------
begin

  FreeAndNil(relProfile);
  FreeAndNil(relProfile1);
  FreeAndNil(relProfile2);


  FreeAndNil(LinkRepeater);
  FreeAndNil(LinkEnd2);
  FreeAndNil(LinkEnd1);
  FreeAndNil(ReflectionPoints);

  inherited Destroy;
end;

//---------------------------------------------------------------------------
procedure TDBLink.LinkRepeater_LoadFromDataset(aDataset: TDataset);
//---------------------------------------------------------------------------
begin
  LinkRepeater.LoadFromDataset(aDataset);



  LinkRepeater.Part1.BLVector := MakeBLVector(Property1_Pos,
                                                 LinkRepeater.Property_Pos);

  LinkRepeater.Part2.BLVector := MakeBLVector(LinkRepeater.Property_Pos,
                                                 Property2_Pos);


  LinkRepeater.Part1.Length_KM  := geo_Distance_km(LinkRepeater.Part1.BLVector);
  LinkRepeater.Part2.Length_KM  := geo_Distance_km(LinkRepeater.Part2.BLVector);


end;

// ---------------------------------------------------------------
procedure TDBLink.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
{const
  FLD_Precision_of_condition_V_diffration_is_equal_to_V_min_dB = 'Precision_of_condition_V_diffration_is_equal_to_V_min_dB';
}

(*
const
  DEF_Calc_Method_ITU_R       = 0;
  DEF_Calc_Method_GOST        = 1;
  DEF_Calc_Method_NIIR_1998   = 2;

  DEF_Calc_Method_E_BAND_ITU  = 3;
//  DEF_Calc_Method_E_BAND_GOST = 4;
 // DEF_Calc_Method_E_BAND_NIIR = 5;
//  DEF_Calc_Method_UKV         = 6;
  DEF_Calc_Method_UKV         = 4;
*)

var
  e: Double;
  s: string;
begin
  Assert(Assigned(aDataset), 'Value not assigned');
  Assert(aDataset.RecordCount=1, 'Value not assigned');


   // ��� - ������� ��������������� ���������

 //  db_View(aDataset);



  with aDataset do
  begin
     Name_:=FieldByName(FLD_Name).AsString;

//    Band :=  FieldByName(FLD_Band).AsStr;


    id :=  FieldByName(FLD_id).AsInteger;


    Project_id :=  FieldByName(FLD_Project_id).AsInteger;

    Assert(Assigned(LinkRepeater));


//    Has_Repeater  :=FieldByName(FLD_has_Repeater).AsBoolean;

    LinkRepeater.ID :=  FieldByName(FLD_Link_Repeater_id).AsInteger;

//    Project_id :=  FieldByName(FLD_Project_id).AsInteger;
//    Project_id :=  FieldByName(FLD_Project_id).AsInteger;


    Is_UsePassiveElements     := FieldByName(FLD_Is_UsePassiveElements).AsBoolean;
    Is_CalcWithAdditionalRain := FieldByName(FLD_Is_CalcWithAdditionalRain).AsBoolean;



    Precision_of_condition_V_diffration_is_equal_to_V_min_dB:=
       FieldByName(FLD_Precision_of_condition_V_diffration_is_equal_to_V_min_dB).AsFloat;

  //  if Precision_of_condition_V_diffration_is_equal_to_V_min_dB=0 then
   //   Precision_of_condition_V_diffration_is_equal_to_V_min_dB:=1;



    Profile_XML :=FieldByName(FLD_Profile_XML).AsString ;

//    EsrNorm  :=FieldByName(FLD_ESR_NORM).AsFloat;
//    BberNorm :=FieldByName(FLD_BBER_NORM).AsFloat;
//    GSTLength:=FieldByName(FLD_GST_LENGTH).AsFloat;

{
    s := FieldByName(FLD_OBJNAME).AsString;
    if Eq (s, 'pmp_link')
      then LinkType:=ltPmpLink_
      else LinkType:=ltLink_;


     case LinkType of
        ltPmpLink_: begin
//          PmpSector_ID  :=FieldByName(FLD_PMP_SECTOR_ID).AsInteger;
  //        PmpTerminal_ID:=FieldByName(FLD_PMP_TERMINAL_ID).AsInteger;
        end;

        ltLink_: begin
          LinkEnd1_ID :=FieldByName(FLD_LinkEnd1_ID).AsInteger;
          LinkEnd2_ID :=FieldByName(FLD_LinkEnd2_ID).AsInteger;
        end;
      end;
    }

    LinkEnd1_ID :=FieldByName(FLD_LinkEnd1_ID).AsInteger;
    LinkEnd2_ID :=FieldByName(FLD_LinkEnd2_ID).AsInteger;


    PropertyID1  :=FieldByName(FLD_Property1_ID).AsInteger;
    PropertyID2  :=FieldByName(FLD_Property2_ID).AsInteger;


    //////////////////////
    Property1.ID  :=FieldByName(FLD_Property1_ID).AsInteger;
    Property2.ID  :=FieldByName(FLD_Property2_ID).AsInteger;

    Property1.Name  :=FieldByName(FLD_Property1_Name).AsString;
    Property2.Name  :=FieldByName(FLD_Property2_Name).AsString;


//    Property1.Name  :=FieldByName(FLD_Property1_dot_Name).AsString;
 //   Property2.Name  :=FieldByName(FLD_Property2_dot_Name).AsString;

    Property1.Ground_Height := FieldByName(FLD_Property1_Ground_Height).AsVariant;
    Property2.Ground_Height := FieldByName(FLD_Property2_Ground_Height).AsVariant;
                             
    //////////////////////


    // -------------------------
    //Variant
    // -------------------------
    Property1_Ground_Height := FieldByName(FLD_Property1_Ground_Height).AsVariant;
    Property2_Ground_Height := FieldByName(FLD_Property2_Ground_Height).AsVariant;

    BLVector :=db_ExtractBLVector (aDataset);


    e := geo_Distance_km(BLVector);

    Property1_Pos:=BLVector.Point1;
    Property2_Pos:=BLVector.Point2;


    is_profile_reversed:=FieldByName(FLD_is_profile_reversed).AsBoolean;

    //(LinkType=ltLink_) and 

    if (is_profile_reversed) then
    begin
      ExchangeInt(LinkEnd1_ID, LinkEnd2_ID);
      ExchangeInt(PropertyID1, PropertyID2);

      BLVector := geo_RotateBLVector_(BLVector);
    end;


    Calc_Method:=FieldByName(FLD_Calc_Method).AsInteger;

    Profile_Step_m:=FieldByName(FLD_PROFILE_STEP).AsInteger;

    ClutterModel_ID:=FieldByName(FLD_CLUTTER_MODEL_ID).AsInteger;

    Rains.ISCALCLENGTH  := FieldByName(FLD_RAIN_ISCALCLENGTH).AsBoolean;

  //  {$IFDEF new}
    Rains.rain_INTENSITY := FieldByName(FLD_rain_INTENSITY_extra).AsFloat;
 //z   {$ELSE}
 //z   Rains.rain_INTENSITY := FieldByName(FLD_rain_INTENSITY).AsFloat;
  //zzz  {$ENDIF}



  //  Rains.RAIN_RATE:=      FieldByName(FLD_RAIN_RATE).AsFloat;   //  '������������� ����� � ������� 0.01% �������, ��/���');

    BER_REQUIRED_Index := FieldByName(FLD_BER_REQUIRED).AsInteger;

//    length_m    :=FieldByName(FLD_length).AsFloat;
    NFrenel     :=FieldByName(FLD_NFrenel).AsFloat;


      REFRACTION:= FieldByName(FLD_REFRACTION).AsFloat;  // '�����. ��������� (��� ��.���������=0, ����� �.�.=0)');


      // ��� - ������� ��������������� ���������
    //RRV: array[1..11] of double; //�������������� ����� �� �������� ��������������� ���������
     {1=-9.000     ;������� �������� ��������� ����.�������������,10^-8 1/�
      2= 7.000     ;����������� ���������� ���������,10^-8 1/�
      3= 1.000     ;��� ������������ �������.(1...3,0->����.�����=����.���.)
      4= 7.500     ;���������� ��������� �������� ����, �/���.�
      5= 1.000     ;Q-������ ������ �����������
      6=41.000     ;������������� ������ K��, 10^-6
      7= 1.500     ;�������� ������������ b ��� ������� ������
      8= 0.500     ;�������� ������������ c ��� ������� ������
      9= 2.000     ;�������� ������������ d ��� ������� ������
      10=20.000    ;������������� ����� � ������� 0.01% �������, ��/���
      11= 1.335    ;�����. ��������� (��� ��.���������=0, ����� �.�.=0)}



      case Calc_Method of

        0:

      //  with RRV_ITU do
        begin
          //'������� �������� ��������� ����.�������������,10^-8 1/�' );
          RRV_ITU.gradient_diel     :=FieldByName(FLD_gradient_diel).AsFloat;

          //'����������� ���������� ���������,10^-8 1/�' );
          RRV_ITU.gradient_deviation:=FieldByName(FLD_gradient_deviation).AsFloat;

          RRV_ITU.terrain_type:=   FieldByName(FLD_terrain_type).AsInteger;//+1 + d;// '��� ������������ �������.(1...3,0->����.�����=����.���.)' );

          RRV_ITU.underlying_terrain_type:= FieldByName(FLD_underlying_terrain_type).AsInteger;//+1 + d;// '��� ������������ �������.(1...3,0->����.�����=����.���.)' );

          RRV_ITU.steam_wet:=      FieldByName(FLD_steam_wet).AsFloat;   //  '���������� ��������� �������� ����, �/���.�');
          RRV_ITU.Q_factor:=       FieldByName(FLD_Q_factor).AsFloat;    //  'Q-������ ������ �����������');
          RRV_ITU.climate_factor:= FieldByName(FLD_climate_factor).AsFloat;//'������������� ������ K��, 10^-6');
          RRV_ITU.factor_B:=       FieldByName(FLD_factor_B).AsFloat;    //  '�������� ������������ b ��� ������� ������');
          RRV_ITU.factor_C:=       FieldByName(FLD_factor_C).AsFloat;    //  '�������� ������������ c ��� ������� ������');
          RRV_ITU.FACTOR_D:=       FieldByName(FLD_FACTOR_D).AsFloat;    //  '�������� ������������ d ��� ������� ������');

          RRV_ITU.RAIN_intensity:= FieldByName(FLD_rain_intensity).AsFloat;   //  '������������� ����� � ������� 0.01% �������, ��/���');

          //����������� ������� [��.�]" (�� ��������� 15)
          RRV_ITU.Air_temperature	    := FieldByName(FLD_Air_temperature).AsFloat;

          //����������� �������� [����]" (�� ��������� 1013)
          RRV_ITU.Atmosphere_pressure := FieldByName(FLD_Atmosphere_pressure).AsFloat;

        end;


{      if FIsProfileExists then
        d:=0
      else
        d:=0.00001;
}
//      d:=0;



      1:
      // -------------------------
     // with GOST do
      // -------------------------
      begin
        //'������� �������� ��������� ����.�������������,10^-8 1/�' );
        GOST.gradient_diel     :=FieldByName(FLD_GOST_gradient_diel).AsFloat;

        //'����������� ���������� ���������,10^-8 1/�' );
        GOST.gradient_deviation:=FieldByName(FLD_GOST_gradient_deviation).AsFloat;

        GOST.terrain_type:=   FieldByName(FLD_GOST_terrain_type).AsInteger;//+1 + d;// '��� ������������ �������.(1...3,0->����.�����=����.���.)' );

        GOST.underlying_terrain_type:= FieldByName(FLD_GOST_underlying_terrain_type).AsInteger;//+1 + d;// '��� ������������ �������.(1...3,0->����.�����=����.���.)' );

        //����������� ������� [��.�]" (�� ��������� 15)
        GOST.Air_temperature	    := FieldByName(FLD_GOST_Air_temperature).AsFloat;

        GOST.climate_factor:= FieldByName(FLD_GOST_climate_factor).AsFloat;//'������������� ������ K��, 10^-6');

        GOST.steam_wet:= FieldByName(FLD_GOST_steam_wet).AsFloat;//'������������� ������ K��, 10^-6');


      //  aObj.RRV.steam_wet_4               :=aDBLink.GOST.steam_wet;

        //����������� �������� [����]" (�� ��������� 1013)
        GOST.Atmosphere_pressure := FieldByName(FLD_GOST_Atmosphere_pressure).AsFloat;

         //9.����������� ������������� �����
        GOST.rain_INTENSITY := FieldByName(FLD_GOST_rain_INTENSITY).AsFloat;

//        Link_Center1.B:=FieldByName(FLD_GOST_Link_Center_lat).AsFloat;
//        Link_Center1.L:=FieldByName(FLD_GOST_Link_Center_lon).AsFloat;

      end;



      2:
      // -------------------------
//      with NIIR1998 do
      // -------------------------
      begin
        //1 '������� �������� ��������� ����.�������������,10^-8 1/�' );
        NIIR1998.gradient_diel :=FieldByName(FLD_NIIR1998_gradient_diel).AsFloat;

        //2 '����������� ���������� ���������,10^-8 1/�' );
        NIIR1998.gradient_deviation :=FieldByName(FLD_NIIR1998_gradient_deviation).AsFloat;

        //3 '��� ���������(0...2) 1-���������� 2-���������� (��������) 3-���������� (������)' );
        NIIR1998.Terrain_type:=FieldByName(FLD_NIIR1998_Terrain_type).AsInteger;

        // 5 '���������� ��������� �������� ����, �/���.�');
        NIIR1998.Steam_wet :=FieldByName(FLD_NIIR1998_Steam_wet).AsFloat;

        //����������� ������� [��.�]" (�� ��������� 15)
        NIIR1998.Air_temperature	    := FieldByName(FLD_NIIR1998_Air_temperature).AsFloat;

        //����������� �������� [����]" (�� ��������� 1013)
        NIIR1998.Atmosphere_pressure := FieldByName(FLD_NIIR1998_Atmosphere_pressure).AsFloat;


        //10 ����� ������ �� ����� ���� (���.8.11) ��� ������������� ������
        NIIR1998.rain_region_number :=FieldByName(FLD_NIIR1998_rain_region_number).AsInteger;

        //11 ����� ������ �� ����� ���� (���.8.15) ��� ��������� Q�
        NIIR1998.Qd_region_number   :=FieldByName(FLD_NIIR1998_Qd_region_number).AsInteger;

        //9. ���� ������ ����������� �� �������-��, %
        NIIR1998.water_area_percent   :=FieldByName(FLD_NIIR1998_water_area_percent).AsFloat;

        //12 ������� �������� ��������� ��������������� ��������-����� �� ����� ���� (���.2.3 � ����.2.1)
    //    NIIR1998_gradient :=FieldByName(FLD_NIIR1998_gradient).AsFloat;

        //���������� ��������� �� ����� ���� (���.2.12 � ����.2.1)
    //    NIIR1998_numidity :=FieldByName(FLD_NIIR1998_numidity).AsFloat;

      end;

      3:
      //DEF_RRV_E_band_ITU    
      // -------------------------
      with RRV_E_band_ITU do begin  //3.�-�������� (��.ITU-R)
      // -------------------------
        s:='RRV_E_band_ITU_';

        Param_1 := FieldByName(s+ 'Param_1').AsFloat; // 1.������� �������� ��������� ����. �������������, 10^-8, 1/�	1	�� �����. (-9)
        Param_2 := FieldByName(s+ 'Param_2').AsFloat; // 2.����������� ���������� ���������,10^-8, 1/�	2	�� �����. 7
        Param_3 := FieldByName(s+ 'Param_3').AsFloat; // 3.���������� ��������� �������� ����, �/���.�	4	�� �����. 7.5
        Param_4 := FieldByName(s+ 'Param_4').AsFloat; // 4.����������� �������, ��.�	12	�� �����. 15
        Param_5 := FieldByName(s+ 'Param_5').AsFloat; // 5.����������� ��������, ����	13	�� �����. 1013
        Param_6 := FieldByName(s+ 'Param_6').AsFloat; // 6.������������� ����� � ������� 0.01% �������, ��/���	10	�� �����.20

      end;

//      6:
      4:
      //DEF_RRV_UKV:
      // -------------------------
      with RRV_UKV do begin  //    6.���-�������� (16 �����)
      // -------------------------
        s:='RRV_UKV_';

        Param_1 := FieldByName(s+ 'Param_1').AsFloat;   // 1.������� �������� ��������� ����. �������������, 10^-8, 1/�	1	�� �����. (-8)
        Param_2 := FieldByName(s+ 'Param_2').AsInteger; // 2.��� ������������ ����������� (1-������������.(����.), 2-�������������., 3-����.)	3	� xml ���������� ��-��: 1,2,3 (�� �����.2)
        Param_3 := FieldByName(s+ 'Param_3').AsFloat;   // 3.��������.����������� c (0-�����.����.�� ���)	8	�� �����.0
        Param_4 := FieldByName(s+ 'Param_4').AsFloat;   // 4.��� ������ ������� (��� �>0) [dB] (0-���������)	12	�� �����.0
      end;

     end;
   //zzz   {$ENDIF}

      Trb.GST_TYPE :=FieldByName(FLD_GST_TYPE).AsInteger;

      Trb.GST_LENGTH_km_  :=FieldByName(FLD_GST_LENGTH).AsFloat;

      Trb.GST_SESR    :=FieldByName(FLD_GST_SESR).AsFloat; //   '����������� �������� SESR ��� ����, %');
      Trb.GST_Kng     :=FieldByName(FLD_GST_Kng).AsFloat; //    '����������� �������� ���  ��� ����, %');
//    b.Length      :=dmLink_Calc_SESR.GetRoundedLengthByID(FLinkID);  //               , '����� ����, ��');
      Trb.KNG_dop     :=FieldByName(FLD_KNG_dop).AsFloat; //    '���������� ����� ���, ������������� �������������}');
      Trb.SPACE_LIMIT :=FieldByName(FLD_SPACE_LIMIT).AsFloat; // '���������� ������������� ������� p(a%)');
      Trb.SPACE_LIMIT_PROBABILITY:=FieldByName(FLD_SPACE_LIMIT_PROBABILITY).AsFloat; //'���������� ����������� a% ������������� ������������ �������� p(a%) [%]');


  //    Trb.Fade_margin_required :=FieldByName(FLD_Fade_margin_required_dB).AsFloat; //��������� ����� �� ��������� [dB] (0-������� ������� �� ��������� ����������)"


//      GST_length_km := FieldByName(FLD_GST_length).AsFloat;
//      GST_Type      := FieldByName(FLD_GST_Type).AsFloat;


      ESR_norm      := FieldByName(FLD_ESR_NORM).AsFloat;
      ESR_required  := FieldByName(FLD_ESR_REQUIRED).AsFloat;
      BBER_norm     := FieldByName(FLD_BBER_NORM).AsFloat;
      BBER_required := FieldByName(FLD_BBER_REQUIRED).AsFloat;



  end;
end;


function TDBLink.GetLength_km: double;
begin

//  Result := Length_m / 1000;

  Result := geo_Distance_km(BLVector);
end;

// ---------------------------------------------------------------
procedure TDBLink.LoadFromDataset_ReflectionPoints(aDataset: TDataset);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  SetLength (ReflectionPointsArray, aDataset.RecordCount);

 // if Length(Result)=0 then
//    Exit;

  with aDataset do
    for I := 0 to RecordCount-1 do
    begin
//      ReflectionPointsArray[i].Distance_KM    :=FieldByName(FLD_DISTANCE).AsFloat;
      ReflectionPointsArray[i].DISTANCE_to_center_km  :=FieldByName(FLD_DISTANCE).AsFloat;
      ReflectionPointsArray[i].Length_km      :=FieldByName(FLD_LENGTH).AsFloat;
      ReflectionPointsArray[i].Radius_KM      :=FieldByName(FLD_RADIUS).AsFloat;
      ReflectionPointsArray[i].abs_prosvet_m  :=FieldByName(FLD_ABS_PROSVET).AsFloat;

      Next;
    end;

end;


// ---------------------------------------------------------------
function TDBLink.Validate_Critical(aList: TStrings): Boolean;
// ---------------------------------------------------------------
var
  oStrList: TStringList;
  s1: string;
  s2: string;

begin
 // Result:=True;

  oStrList:=TStringList.Create();

  if (LinkEnd1.Antennas.Count=0) or (LinkEnd2.Antennas.Count=0) then
 // begin
    oStrList.Add('���-�� ������ = 0');
   // ErrorDlg ('���-�� ������ = 0');
  //  Result:=False;
 // end;



  if (LinkEnd1.Antennas.Count>0) and (LinkEnd2.Antennas.Count>0) then
    if (LinkEnd1.Antennas[0].Polarization_str <>
        LinkEnd2.Antennas[0].Polarization_str) then
  //  begin
      oStrList.Add('����������� ������ ������ ���� ����������.');

   //////// ErrorDlg ('����������� ������ ������ ���� ����������.');
 //   Result:=False;
 // end;

(*
  s1:=qry_Antennas1.FieldByName(FLD_Polarization_str).AsString;
  s2:=qry_Antennas2.FieldByName(FLD_Polarization_str).AsString;

  if (qry_Antennas1.FieldByName(FLD_Polarization_str).AsString <>
      qry_Antennas2.FieldByName(FLD_Polarization_str).AsString) then
  begin
    oStrList.Add('����������� ������ ������ ���� ����������.');
  //  ErrorDlg ('����������� ������ ������ ���� ����������.');
 //   Result:=False;
  end;
*)

(*
  if Freq_GHz=0 then // begin
    oStrList.Add('������� GHz = 0');
  //  ErrorDlg ('������� GHz = 0');
 //   Result:=False;
//  end;*)

//  if LinkEnd1.Power_dBm=0 then  //  begin
//    oStrList.Add('Power_dBm = 0');
//  //  ErrorDlg ('������� GHz = 0');
// //   Result:=False;
// // end;


  if LinkEnd2.THRESHOLD_BER_3=0 then // begin
    oStrList.Add('���2 - �� ������ ���������������� ��� BER(-3) = 0. ����� ��������� ������ � �����������.');


  if LinkEnd2.THRESHOLD_BER_6=0 then // begin
    oStrList.Add('���2 - �� ������ ���������������� ��� BER(-6) = 0. ����� ��������� ������ � �����������.');
  //  ErrorDlg ('������� GHz = 0');
 //   Result:=False;
//  end;

  Result:=oStrList.Count=0;

//  if not Result then
//    ShowMessage(oStrList.Text);

  aList.AddStrings(oStrList);


  FreeAndNil(oStrList);

{
  if qry_LinkEnd1.FieldByName(FLD_POWER).AsFloat=0 then begin
    ErrorDlg ('�������� ����������� = 0');   Result:=False;
  end;

}
end;


// ---------------------------------------------------------------
function TDBLink.Validate_Bands_(aList: TStrings): Boolean;
// ---------------------------------------------------------------
var
  I: Integer;
  k: Integer;
  oList_main: TStringList;

  oList: TStringList;
  s: string;


  s1: string;
  s2: string;
  sName: string;


begin
 // Result:=True;

  oList_main:=TStringList.Create();

  oList:=TStringList.Create();


  oList_main.Add(Name_);

//  PropertyID1


  // -------------------------
  // PPC1
  // -------------------------
  sName:= Format('��������1: %s ���1: %s. ', [Property1.Name, LinkEnd1.Name_]);
  oList.Clear;
  oList.Add(sName);

  if (LinkEnd1.Band='') then
    oList_main.Add('-�������� ������');

  for I := 0 to LinkEnd1.Antennas.Count - 1 do
  begin
    s:=LinkEnd1.Antennas[i].Name_;

    if LinkEnd1.Antennas[i].Band='' then
      oList_main.Add( Format('-%s: �������� ������', [s]));

    if LinkEnd1.Antennas[i].Band<>LinkEnd1.Band then
      oList_main.Add( Format('-%s: �������� ����������', [s]));

  end;

  if oList.Count>1 then
   oList_main.AddStrings(oList);

  // -------------------------
  // PPC1
  // -------------------------
  sName:= Format('��������2: %s ���2: %s. ', [Property2.Name, LinkEnd2.Name_]);
  oList.Clear;
  oList.Add(sName);


  if (LinkEnd2.Band='') then
    oList_main.Add('-�������� ������');

  for I := 0 to LinkEnd2.Antennas.Count - 1 do
  begin
    s:=LinkEnd2.Antennas[i].Name_;


    if LinkEnd2.Antennas[i].Band='' then
      oList_main.Add( Format('-%s: �������� ������', [s]));

    if LinkEnd2.Antennas[i].Band<>LinkEnd2.Band then
      oList_main.Add( Format('-%s: �������� ����������', [s]));

  end;

  // -------------------------
  // PPC1 - PPC2
  // -------------------------
  if (LinkEnd1.Band <> LinkEnd2.Band) then
    oList_main.Add('���1 - ���2: ��������� ����������');


  Result:=oList_main.Count=1;

  if not Result then
    if  assigned(aList) then
      aList.AddStrings(oList_main);


//  if not Result then
  //  Result := ConfirmDlg(oList_main.Text);

{
  if not Result then
  begin
    k :=  MyMessageDlg(oList_main.Text, mtWarning, [mbYes, mbNo],
        ['Ok','������'], '���������� ?' );
    Result := k = 6;

  end;
}


//  ModalResults: array[TMsgDlgBtn] of Integer = (
//    mrYes, mrNo, mrOk, mrCancel, mrAbort, mrRetry, mrIgnore, mrAll, mrNoToAll,
//    mrYesToAll, 0);




  FreeAndNil(oList_main);
  FreeAndNil(oList);

end;


constructor TDBLinkReflectionPointList.Create;
begin
  inherited Create(TDBLinkReflectionPoint);
end;

function TDBLinkReflectionPointList.AddItem: TDBLinkReflectionPoint;
begin
  Result := TDBLinkReflectionPoint (inherited Add);
end;

function TDBLinkReflectionPointList.GetItems(Index: Integer):
    TDBLinkReflectionPoint;
begin
  Result := TDBLinkReflectionPoint(inherited Items[Index]);
end;

constructor TDBLinkEnd.Create;
begin
  inherited Create;
  Antennas := TDBAntennaList.Create();
end;

destructor TDBLinkEnd.Destroy;
begin
  FreeAndNil(Antennas);
  inherited Destroy;
end;

function TDBLinkEnd.GetTx_Freq_GHz: double;
begin
  Result := TX_FREQ_MHz / 1000;
end;

// ---------------------------------------------------------------
procedure TDBLinkEnd.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
var
  sBand: string;
 // v: Variant;
begin
  with aDataset do
  begin
    Name_ :=  FieldByName(FLD_Name).AsString;



    LinkEndType_ID:=FieldByName(FLD_LinkEndType_ID).AsInteger;
    // -----------------------------

    if LinkEndType_ID>0 then
      Band :=  FieldByName(FLD_LINKENDTYPE_Band).AsString
    else
      Band :=  FieldByName(FLD_Band).AsString;



    THRESHOLD_BER_3   :=FieldByName(FLD_THRESHOLD_BER_3).asFloat;
    THRESHOLD_BER_6   :=FieldByName(FLD_THRESHOLD_BER_6).asFloat;

    if (THRESHOLD_BER_3<>0) and (THRESHOLD_BER_6=0) then
      THRESHOLD_BER_6:=THRESHOLD_BER_3;
      


  //  BAND              :=FieldByName(FLD_BAND).AsString;
    TX_FREQ_MHz       :=FieldByName(FLD_TX_FREQ_MHz).AsFloat;

    if TX_FREQ_MHz=0 then
      TX_FREQ_MHz:=FieldByName(FLD_RX_FREQ_MHz).AsFloat;


    Power_dBm         :=FieldByName(FLD_POWER_dBm).AsFloat;

    Kng               :=FieldByName(FLD_Kng).AsFloat;
    LOSS_dBm          :=FieldByName(FLD_LOSS).AsFloat;

    Passive_element_LOSS_dBm:=FieldByName(FLD_Passive_element_LOSS).AsFloat;

    FREQ_CHANNEL_COUNT:=FieldByName(FLD_FREQ_CHANNEL_COUNT).AsInteger;

    Freq_Spacing_MHz  :=FieldByName(FLD_Freq_Spacing).AsFloat;


//    assert(Freq_Spacing_MHz < 700, 'Freq_Spacing_MHz < 700');


    KRATNOST_BY_SPACE:=FieldByName(FLD_KRATNOST_BY_SPACE).AsInteger;
    KRATNOST_BY_FREQ :=FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger;

    SIGNATURE_WIDTH := FieldByName(FLD_SIGNATURE_WIDTH).AsFloat;//,  'double; //  '������ ���������, ���');
    SIGNATURE_HEIGHT:= FieldByName(FLD_SIGNATURE_HEIGHT).AsFloat;//, 'double; // '������ ���������, ��');
    MODULATION_COUNT:= FieldByName(FLD_MODULATION_COUNT).AsFloat;//,double;// '����� ������� ���������');
  //  EQUALISER_PROFIT1:= FieldByName('EQUALISER_PROFIT').AsFloat;//, 'double; // '�������, �������� ������������, ��');

//    BitRate_Mbps  := FieldByName(FLD_BitRate_Mbps).AsInteger;
    BitRate_Mbps  := FieldByName(FLD_BitRate_Mbps).AsFloat;

//    if Assigned(FindField(FLD_ATPC_profit_dB)) then
 //   begin
    ATPC_profit_dB:= FieldByName(FLD_ATPC_profit_dB).AsFloat;//, 'double; // '�������, �������� ������������, ��');
    Use_ATPC      := FieldByName(FLD_Use_ATPC).AsBoolean;//, 'double; // '�������, �������� ������������, ��');
    Threshold_degradation_dB:= FieldByName(FLD_Threshold_degradation_dB).AsFloat;//, 'double; // '�������, �������� ������������, ��');

    GOST_53363_modulation                      := FieldByName(FLD_GOST_53363_modulation).AsString;
    GOST_53363_RX_count_on_combined_diversity  := FieldByName(FLD_GOST_53363_RX_count_on_combined_diversity).AsInteger;

  //  v:=FieldByName(FLD_Antenna_offset_horizontal_m).asFloat;

    Antenna_offset_horizontal_m :=FieldByName(FLD_Antenna_offset_horizontal_m).asFloat;


    XPIC_42_use_2_stvol :=  FieldByName('XPIC_use_2_stvola_diff_pol').AsInteger; //,  '49.����.�����.���.��� ��.�����.[��]');

    XPIC_49_coef:=  FieldByName('NIIR1998_compensator_of_noises_Ixpic_dB').asFloat; //,  '49.����.�����.���.��� ��.�����.[��]');
    XPIC_50     :=  FieldByName('NIIR1998_Coefficient_cross_polarization_protection_dB').asFloat; //,  '50�����.�� �������.����� [��]');



//  end;

   //   : double;// <param id="45" value="10" comment="������� �� ATPC, ��"/>
  //  : Double; //Threshold_degradation_dB,  '���������� ����������������, ��');


  end;
end;


function TDBAntenna.GetPolarizationType: TDBAntennaPolarizationType;
begin
  if Eq(Polarization_str,'h') then
    Result:=ptH__ else
  if Eq(Polarization_str,'v') then
    Result:=ptV__
  else
    Result:=ptX__;


(*  case Polarization_str of
    0: Result:=ptH;
    1: Result:=ptV;
    2: Result:=ptX;
  else
     Result:=ptH;
  end;
*)
end;

// ---------------------------------------------------------------
function TDBAntenna.GetPolarization1: Integer;
// ---------------------------------------------------------------
var
  s: string;
begin
 //TTX.POLARIZATION, '��� �����������, 0-��������������,1-������������,2-�����');
  s := LowerCase(Polarization_str);

  if s='h' then  Result := 0 else
  if s='v' then  Result := 1 else
                 Result := 2;

end;

// ---------------------------------------------------------------
procedure TDBAntenna.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
begin

  with aDataset do
  begin
//    Id                  :=FieldByName(FLD_Id).AsInteger;

//    BLPoint.B           :=FieldByName(FLD_Lat).AsFloat;
//    BLPoint.L           :=FieldByName(FLD_Lon).AsFloat;

    Name_     :=FieldByName(FLD_Name).AsString;

    Polarization_str    :=LowerCase(FieldByName(FLD_Polarization_str).AsString);
    Diameter            :=FieldByName(FLD_Diameter).AsFloat;
    Height              :=FieldByName(FLD_Height).AsFloat;
    Gain                :=FieldByName(FLD_Gain).AsFloat;
    Vert_width          :=FieldByName(FLD_Vert_width).AsFloat;

  //  if Assigned(FileField(FLD_Loss_dB)) then
   //    Loss :=FieldByName(FLD_Loss_dB).AsFloat
   // else
    Loss :=FieldByName(FLD_Loss).AsFloat;


    Freq_Mhz:=FieldByName(FLD_Freq_Mhz).AsFloat;
  //  Band    :=FieldByName(FLD_Band).AsString;


    AntennaType_ID:=FieldByName(FLD_AntennaType_ID).AsInteger;

    if AntennaType_ID>0 then
      Band :=  FieldByName(FLD_AntennaType_Band).AsString
    else
      Band :=  FieldByName(FLD_Band).AsString;


    if Assigned(FindField(FLD_repeater_gain)) then
    begin
      repeater_gain:=FieldByName(FLD_repeater_gain).AsFloat;
      Gain:=Gain + repeater_gain;
    end;

//    FronttoBackratio    :=FieldByName(FLD_Fronttobackratio).AsFloat;

/////////    Band := FieldByName(FLD_BAND).AsString;
  end;
end;

constructor TDBAntennaList.Create;
begin
  inherited Create(TDBAntenna);
end;

function TDBAntennaList.AddItem: TDBAntenna;
begin
  Result := TDBAntenna (inherited Add);
end;

function TDBAntennaList.GetItems(Index: Integer): TDBAntenna;
begin
  Result := TDBAntenna(inherited Items[Index]);
end;

procedure TDBAntennaList.LoadFromDataset(aDataset: TDataset);
begin
  Clear;

  aDataset.First;
  while not aDataset.EOF do
  begin
    AddItem.LoadFromDataset(aDataset);
    aDataset.Next;
  end;
end;

constructor TDBLinkRepeater.Create;
begin
  inherited Create;
  Antennas := TDBAntennaList.Create();
end;

destructor TDBLinkRepeater.Destroy;
begin
  FreeAndNil(Antennas);
  inherited Destroy;
end;

// ---------------------------------------------------------------
procedure TDBLinkRepeater.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
begin
  Assert(aDataset.RecordCount=1);

  with aDataset do
  begin
    ID:=FieldByName(FLD_ID).AsInteger;


    Part1.Profile_XML:=FieldByName(FLD_Profile_XML_1).AsString;
    Part2.Profile_XML:=FieldByName(FLD_Profile_XML_2).AsString;

    Antenna1.Height:=FieldByName(FLD_Antenna1_Height).AsFloat;
    Antenna2.Height:=FieldByName(FLD_Antenna2_Height).AsFloat;

    Antenna1.Loss:=FieldByName(FLD_Antenna1_Loss).AsFloat;
    Antenna2.Loss:=FieldByName(FLD_Antenna2_Loss).AsFloat;

    Antenna1.Gain:=FieldByName(FLD_Antenna1_Gain).AsFloat;
    Antenna2.Gain:=FieldByName(FLD_Antenna2_Gain).AsFloat;



    Property_Ground_Height:=FieldByName(FLD_Ground_Height).AsVariant;
    Property_Pos  :=  db_ExtractBLPoint (aDataset);

    Property_ID:=FieldByName(FLD_Property_ID).AsInteger;


    calc_results_xml_1:=FieldByName(FLD_calc_results_xml_1).AsString;
    calc_results_xml_2:=FieldByName(FLD_calc_results_xml_2).AsString;
    calc_results_xml_3:=FieldByName(FLD_calc_results_xml_3).AsString;


    Assert(Property_Pos.B<>0, 'Value <=0');

  end;
end;

constructor TDBLinkList.Create;
begin
  inherited Create(TDBLink);
end;

function TDBLinkList.AddItem: TDBLink;
begin
  Result := TDBLink (Add);

  Assert(Assigned(Result.LinkEnd1));
  Assert(Assigned(Result.LinkEnd2));

end;

function TDBLinkList.GetItems(Index: Integer): TDBLink;
begin
  Result := TDBLink(inherited Items[Index]);
end;



end.


     {


   for I := 0 to qry_Modes.RecordCount - 1 do
  //  for I := 0 to 1 do
    begin
      FAdaptiveModeArr[i].id   :=qry_Modes.FieldByName(FLD_id).AsInteger;
      FAdaptiveModeArr[i].Mode :=qry_Modes.FieldByName(FLD_Mode).AsInteger;

      FAdaptiveModeArr[i].THRESHOLD_BER_3 :=qry_Modes.FieldByName(FLD_THRESHOLD_BER_3).AsFloat;
      FAdaptiveModeArr[i].THRESHOLD_BER_6 :=qry_Modes.FieldByName(FLD_THRESHOLD_BER_6).AsFloat;

      FAdaptiveModeArr[i].Power_dBm       :=qry_Modes.FieldByName(FLD_Power_max).AsFloat;
      FAdaptiveModeArr[i].BitRate_Mbps    :=qry_Modes.FieldByName(FLD_BitRate_Mbps).AsFloat;

      FAdaptiveModeArr[i].Modulation_type :=qry_Modes.FieldByName(FLD_Modulation_type).AsString;

      qry_Modes.Next;
    end;
