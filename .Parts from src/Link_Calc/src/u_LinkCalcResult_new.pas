unit u_LinkCalcResult_new;


interface

uses Classes, SysUtils, xmldoc, XMLIntf,

   u_Xml_Document,

   u_link_const,

   u_const_db,

   u_db,

   u_func
  , DB;

type

  TReflectionPointList = class;
  TReflectionPoint     = class;

  // ---------------------------------------------------------------
  TLinkCalcResultItem = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
    Name:    string;
    Caption: string;

    ID:    Integer;
    Value: Double;
    Value2: Double;
  end;


  // ---------------------------------------------------------------
  TLinkCalcResultItemList = class(TCollection)
  // ---------------------------------------------------------------
  private
//    function FindByCaption11111111(aCaption: string): TLinkCalcResultItem;
    function FindByName(aName: string): TLinkCalcResultItem;

    function GetCalcParamValueByID(aID: integer): Double;
    function GetItems(Index: Integer): TLinkCalcResultItem;
  public
    constructor Create;

    function AddItem: TLinkCalcResultItem;
    function FindByID(aID: Integer): TLinkCalcResultItem;

    property Items[Index: Integer]: TLinkCalcResultItem read GetItems; default;
  end;


  // ---------------------------------------------------------------
  TLinkCalcResultGroup = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
  //  Name:    string;
    Caption: string;

    SubItems: TLinkCalcResultItemList;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
  end;


// ---------------------------------------------------------------
  TLinkCalcResultGroupList = class(TCollection)
  // ---------------------------------------------------------------
  private
//    function GetCalcParamValueByID_new(aName: string; aID: integer): Double;
    function GetItems(Index: Integer): TLinkCalcResultGroup;

 //   function FindByName(aValue: string): TLinkCalcResultGroup;

  public
    constructor Create;

    function AddItem: TLinkCalcResultGroup;
    function FindByCaption(aCaption: string): TLinkCalcResultGroup;


    function GetCalcParamValueByID(aID: integer): Double;
    function GetCalcParamValueByID_AsInteger(aID: integer): integer;

    function ParamExists(aID: integer): Boolean;



    property Items[Index: Integer]: TLinkCalcResultGroup read GetItems; default;
  end;



  // ---------------------------------------------------------------
  TdmLink_calc_CalcResults_rec1 = record
  // ---------------------------------------------------------------

    Rx_Level_dBm: Double;

    SESR           : Double;
    Kng            : Double;
    Sesr_Req       : Double;
    KNG_Req        : Double;
    ESR_norm       : Double;
    BBER_norm      : Double;
    ESR_required   : Double;
    BBER_required  : Double;

    fade_margin_dB : Double; // ����� �� ��������� [dB]

    MARGIN_HEIGHT_m    : Double;  //'����������� ������������� ������� [m]'
    MARGIN_DISTANCE_km : Double;  //'��������� ����� �������� [�m]'

    SESR_SUBREFRACTION1 : Double;  //'SESR �������������� ������������ [%]'
    SESR_RAIN          : Double;  //'�������� ������������ [%]'
    SESR_INTERFERENCE  : Double;  //'SESR ����������������� ������������ [%]'

    KNG_SUBREFRACTION : Double;  //'SESR �������������� ������������ [%]'
    KNG_RAIN          : Double;  //'�������� ������������ [%]'
    KNG_INTERFERENCE  : Double;  //'SESR ����������������� ������������ [%]'

//    rain_signal_depression_dB : double;

    Status      : Integer;
    LOS_STATUS  : Integer;
  end;


  // ---------------------------------------------------------------
  TLinkCalcResult = class
  // ---------------------------------------------------------------
  private
    procedure LoadFromXmlFile_RRL_XML(aFileName: string; aB_to_A_calc: boolean =
        False);
// TODO: GetGroupByCaption
//  function GetGroupByCaption(aValue: string): TLinkCalcResultItem;

    procedure Load_ReflectionPoints;
//    procedure Parse11;
    procedure Parse2;

//    procedure Parse2_;
//    procedure Parse2_old111;
  public
    ReflectionPoints: TReflectionPointList;
    Groups          : TLinkCalcResultGroupList;

//    GroupList: TLinkCalcResultGroupList;


    Data : record
      Rx_Level_dBm  : Double;
      STATUS        : Integer;  //(1)-�����; (2) - �� �����
      IsAvailable : Boolean;

      LOS_IsAvailable : Boolean; //���� ����� ������ ��������� ��� ���
      LOS_STATUS      : Integer;


      SESR          : Double;
      Kng           : Double;

      Sesr_Req      : Double;
//      FadeReserv    : Double;
      KNG_Req       : Double;
      ESR_norm      : Double;
      BBER_norm     : Double;

      ESR_required  : Double;
      BBER_required : double;

      fade_margin_dB : Double; // ����� �� ��������� [dB]
      Threshold : Double; //����������������

      MARGIN_HEIGHT_m    : Double; //'����������� ������������� ������� [m]'
      MARGIN_DISTANCE_km : Double; //'��������� ����� �������� [�m]'

      SESR_SUBREFRACTION : Double; //'SESR �������������� ������������ [%]'
      SESR_RAIN          : Double; //'�������� ������������ [%]'
      SESR_INTERFERENCE  : Double; //'SESR ����������������� ������������ [%]'

      KNG_SUBREFRACTION : Double; //'SESR �������������� ������������ [%]'
      KNG_RAIN          : Double; //'�������� ������������ [%]'
      KNG_INTERFERENCE  : Double; //'SESR ����������������� ������������ [%]'


      //    rain_signal_depression_dB : double;

//      GetCalcParamID(DEF_MAX_WEAKNESS);

      Rains: record
                max_weakness1:        double;   //���������� ���������� (��)
                direct_line_tilt:   double;   //������ ����� ������ ���������(����)
             end;

      Optimize: record
                  TX_ANTENNA_H : double;
                  RX_ANTENNA_H : double;
                  RX_ANTENNA_DOP_H : double;
                  TX_ANTENNA_DOP_H : double;
                end;

    end;


    constructor Create;
    destructor Destroy; override;

    procedure Clear;
    
    procedure LoadFromXmlFile(aFileName: string);
    procedure LoadFromXmlFile_RRL_Result(aFileName: string; aB_to_A_calc: boolean =
        False);

    procedure LoadFromXmlString(aText: string);
    procedure SaveToDataset_2_Datasets(aDataset_Group, aDataset_Items: TDataSet;
        aLink_ID: Integer; aCaption: string = '');

    procedure SaveToXMLDoc(aXmlDocument: TXmlDocumentEx);

    procedure SaveToXMLFile(aFileName: string);
    function  SaveToXMLString: string;


   procedure SaveToDataset_DBTree1(aDataset: TDataSet; aParent_label: string = '');


//    procedure SaveToXMLFile(aFileName: string);
    //1 �������� ������ �� ����� ����������

  end;

  // ---------------------------------------------------------------
  TReflectionPoint = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
    DISTANCE_to_center_km : Double;//���������� �� ������� 1-�� ������� ��������� [km]
    ABS_PROSVET_m : Double; //A��������� ������� ��� �������� 1-�� ������� �����. [m]
                            //����������� ������� ��� �������� 1-�� �������
                            //�����������.������� ��� ��������
    Length_km : Double;     //������������� 1-�� �������
    RADIUS_km : Double;     //P����� �������� 1-�� �������

    Signal_Depression_dB : double;
  end;

  // ---------------------------------------------------------------
  TReflectionPointList = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TReflectionPoint;
  public
    constructor Create;
    procedure SaveToDataset(aDataset: TDataSet);

    function AddItem: TReflectionPoint;

    property Items[Index: Integer]: TReflectionPoint read GetItems; default;
  end;




//=================================================================
implementation
                 
//
//
const
  FLD_LINK_ID = 'LINK_ID';

//  DEF_GROUP_REFLECTION_PARAMS = '��������� �������� ���������';
//

  (*
//  DEF_LENGTH_KM    = 1;
  DEF_STATUS       = 7;  //����������� ���������

  DEF_SESR         = 17;
  DEF_KNG          = 19;

  DEF_KNG_REQ      = 18;
  DEF_SESR_REQ     = 16;
  DEF_LOS          = 31;
  DEF_Rx_LEVEL_dBm = 101; //��������� ������� ������� �� ����� ��������� (���)

  DEF_MAX_WEAKNESS     = 14;  //���������� ���������� (��)
  DEF_DIRECT_LINE_TILT = 95;  //������ ����� ������ ���������(����)

*)

constructor TLinkCalcResultItemList.Create;
begin
  inherited Create(TLinkCalcResultItem);
end;

function TLinkCalcResultItemList.AddItem: TLinkCalcResultItem;
begin
  Result := TLinkCalcResultItem (inherited Add);
end;

//--------------------------------------------------------------------
function TLinkCalcResultItemList.GetCalcParamValueByID(aID: integer): Double;
//--------------------------------------------------------------------
var i,j: integer;
  oItem: TLinkCalcResultItem;
begin
  Result:=0;

  oItem := FindByID(aID);
  if Assigned(oItem) then
    Result := oItem.Value;


end;


//-------------------------------------------------------------------
function TLinkCalcResultItemList.FindByName(aName: string): TLinkCalcResultItem;
//-------------------------------------------------------------------
var i: integer;
begin
  Result := nil;

  for i:=0 to Count-1 do
    if Eq(Items[i].Name, aName) then
    begin
      Result:=Items[i];
      Break;
    end;
end;


//-------------------------------------------------------------------
function TLinkCalcResultItemList.FindByID(aID: Integer): TLinkCalcResultItem;
//-------------------------------------------------------------------
var i: integer;
begin
  Result := nil;

  for i:=0 to Count-1 do
    if Items[i].ID = aID then
    begin
      Result:=Items[i];
      Break;
    end;
end;


function TLinkCalcResultItemList.GetItems(Index: Integer): TLinkCalcResultItem;
begin
  if Index>=Count then
    Result := nil
  else
    Result := TLinkCalcResultItem(inherited Items[Index]);
end;



constructor TLinkCalcResult.Create;
begin
  inherited Create;
  Groups := TLinkCalcResultGroupList.Create();

  ReflectionPoints := TReflectionPointList.Create();

 // GroupList:=TLinkCalcResultGroupList.Create();


//  FillChar(Data,SizeOf(Data),-1);
end;

// ---------------------------------------------------------------
destructor TLinkCalcResult.Destroy;
// ---------------------------------------------------------------
begin
  FreeAndNil(ReflectionPoints);
  FreeAndNil(Groups);
 // FreeAndNil(GroupList);

  inherited Destroy;
end;

// ---------------------------------------------------------------
procedure TLinkCalcResult.Clear;
// ---------------------------------------------------------------
begin
  Groups.Clear;
  ReflectionPoints.Clear;

  FillChar(Data,SizeOf(Data),0);
end;


// ---------------------------------------------------------------
procedure TLinkCalcResult.LoadFromXmlFile_RRL_Result(aFileName: string;
    aB_to_A_calc: boolean = False);
// ---------------------------------------------------------------
var
  oXMLDoc: TXMLDoc;
  vGroup,vReport,vDocument,vNode: IXMLNode;

  eValue: Double;
  I: integer;
  iLen: integer;
  iID: Integer;
  iPos: Integer;
  iValue,j,id_parent: integer;
  k: Integer;

 // sGroupName,
  sValue,//sName,
  sGroupCaption: string;

  oGroup: TLinkCalcResultGroup;
//  oCalcResultItem: TLinkCalcResultItem;
  oSubItem: TLinkCalcResultItem;
  s: string;
 // sCaption: string;

begin
//  Items.Clear;

  Assert(FileExists(aFileName), aFileName);

  oXMLDoc:=TXMLDoc.Create;
  oXMLDoc.LoadFromFile(aFileName);


  vDocument := oXMLDoc.DocumentElement;
  vReport   := xml_FindNode(vDocument, 'REPORT');

  for i:=0 to vReport.ChildNodes.Count-1 do
  begin
    vGroup:=vReport.ChildNodes[i];
  //  sGroupName:=vGroup.LocalName;


    sGroupCaption :=Trim(xml_GetStrAttr(vGroup, 'Caption'));
 //   sGroupName    :=Trim(xml_GetStrAttr(vGroup, 'Name'));

    Assert(sGroupCaption<>'');
  //  Assert(sGroupName<>'');

(*
    iPos := Pos('.',sGroupCaption);
    if iPos>0 then
    begin
//      iNumber := StrToInt(Copy(sCaption,1,iPos-1));
      sGroupName  := Copy(sGroupCaption,1,iPos-1);
    end else
      sGroupName   :=sGroupCaption;
*)


    oGroup:=Groups.FindByCaption(sGroupCaption);
  //  oGroup:=Groups.FindByName(sGroupName);

    if not Assigned(oGroup) then
    begin
      oGroup:=Groups.AddItem;

    //  oGroup.Name    := sGroupName;
      oGroup.Caption := sGroupCaption;

    end else
      k:=0;

(*

    if iPos>0 then
    begin
//      iNumber := StrToInt(Copy(sCaption,1,iPos-1));
      oCalcResultItem.GroupName  := Copy(sCaption,1,iPos-1);
    end else
      oCalcResultItem.GroupName   :=sCaption;

*)

 //   ��������� �������� ���������

//     else
  //    oCalcResultItem.GroupName   :=Format('group_%d',[i]);


 //   oCalcResultItem.IsGroup := True;

//    oCalcResultItem.GroupName   :=Format('group_%d',[i]);
  //  oCalcResultItem.GroupCaption:=sCaption;
  
     // -------------------------
     for j:=0 to vGroup.ChildNodes.Count-1 do
     // -------------------------
     begin
       vNode:=vGroup.ChildNodes[j];

       iID :=xml_GetIntAttr(vNode, 'ID');

  //     oSubItem :=nil;

       if aB_to_A_calc then
       begin
           // ������ �� ����� ���������
         if not ((iID>=102) and (iID<=110)) then
         begin
           oSubItem := oGroup.SubItems.FindByID(iID);

           if Assigned(oSubItem) then
             oSubItem.Value2:=xml_GetFloatAttr(vNode, 'Value') ;
         end
       end

       else
//       if not Assigned(oSubItem) then
       begin
         oSubItem :=oGroup.SubItems.AddItem;

         oSubItem.ID:=iID;

//         oSubItem.ID      :=xml_GetIntAttr(vNode, 'ID');
         oSubItem.Caption :=xml_GetStrAttr(vNode, 'Caption');
     //    oSubItem.ID      :=iID;
         oSubItem.Value   :=xml_GetFloatAttr(vNode, 'Value');
      // end;

       end;

     end;
  end;

  FreeAndNil(oXMLDoc);



  if ReflectionPoints.Count=0 then
    Load_ReflectionPoints();

  Parse2();
end;


// ---------------------------------------------------------------
procedure TLinkCalcResult.SaveToXMLFile(aFileName: string);
// ---------------------------------------------------------------
var
  oXml_Document: TXmlDocumentEx;
begin
  oXml_Document:=TXmlDocumentEx.Create;

  SaveToXMLDoc(oXml_Document);

  oXml_Document.SaveToFile(aFileName);

  FreeAndNil(oXml_Document);
end;


// ---------------------------------------------------------------
function TLinkCalcResult.SaveToXMLString: string;
// ---------------------------------------------------------------
var
  oXml_Document: TXmlDocumentEx;
begin
  oXml_Document:=TXmlDocumentEx.Create;

  SaveToXMLDoc(oXml_Document);

  Result := oXml_Document.XmlDocument.XML.Text;

  FreeAndNil(oXml_Document);
end;


// ---------------------------------------------------------------
procedure TLinkCalcResult.SaveToXMLDoc(aXmlDocument: TXmlDocumentEx);
// ---------------------------------------------------------------
var
  I: Integer;
  j: Integer;
  vGroupNode: IXMLNode;
  vNode: IXMLNode;
  oGroup: TLinkCalcResultGroup;
  oSubItem: TLinkCalcResultItem;

  oReflectionPoint: TReflectionPoint;

begin
  Assert(Assigned(aXmlDocument.DocumentElement));


  for I := 0 to Groups.Count - 1 do
  begin
    oGroup := Groups[i];

    vGroupNode  := aXmlDocument.DocumentElement.AddChild ('GROUP');
//    vGroupNode.Attributes['name']    := oGroup.Name;
    vGroupNode.Attributes['caption'] := oGroup.Caption;


    for j := 0 to oGroup.SubItems.Count - 1 do
    begin
      oSubItem := oGroup.SubItems[j];

      vNode := vGroupNode.AddChild ('ITEM');
      vNode.Attributes['id']      := oSubItem.ID;
      vNode.Attributes['caption'] := oSubItem.Caption;
      vNode.Attributes['value']   := oSubItem.Value;
      vNode.Attributes['value2']  := oSubItem.Value2;
    end;
  end;


  vGroupNode  := aXmlDocument.DocumentElement.AddChild ('DATA');

    vGroupNode.AddChild('Rx_Level_dBm').Attributes['value'] := Data.Rx_Level_dBm;
    vGroupNode.AddChild('Status').Attributes['value'] := Data.Status;

    vGroupNode.AddChild('Kng').Attributes['value'] := Data.Kng;
    vGroupNode.AddChild('Sesr_Req').Attributes['value'] := Data.Sesr_Req;
    vGroupNode.AddChild('KNG_Req').Attributes['value'] := Data.KNG_Req;
    vGroupNode.AddChild('ESR_norm').Attributes['value'] := Data.ESR_norm;
    vGroupNode.AddChild('fade_margin_dB').Attributes['value'] := Data.fade_margin_dB;

    vGroupNode.AddChild('MARGIN_HEIGHT_m').Attributes['value'] := Data.MARGIN_HEIGHT_m;
    vGroupNode.AddChild('MARGIN_DISTANCE_km').Attributes['value'] := Data.MARGIN_DISTANCE_km;

    vGroupNode.AddChild('SESR_SUBREFRACTION').Attributes['value']:= Data.SESR_SUBREFRACTION;
    vGroupNode.AddChild('SESR_RAIN').Attributes['value']           := Data.SESR_RAIN;
    vGroupNode.AddChild('SESR_INTERFERENCE').Attributes['value'] := Data.SESR_INTERFERENCE;


    vGroupNode.AddChild('KNG_SUBREFRACTION').Attributes['value']:= Data.KNG_SUBREFRACTION;
    vGroupNode.AddChild('KNG_RAIN').Attributes['value']         := Data.KNG_RAIN;
    vGroupNode.AddChild('KNG_INTERFERENCE').Attributes['value'] := Data.KNG_INTERFERENCE;


  // ---------------------------------------------------------------
  if ReflectionPoints.Count>0 then
  // ---------------------------------------------------------------
  begin
    vGroupNode  := aXmlDocument.DocumentElement.AddChild ('ReflectionPoints');
//    vGroupNode.Attributes['name']    := 'ReflectionPoints';
    vGroupNode.Attributes['caption'] := '����� ���������';


    for i := 0 to ReflectionPoints.Count - 1 do
    begin
      oReflectionPoint := ReflectionPoints[i];

   //   vNode := vGroupNode.AddChild ('ITEM');

//      oSubItem := oCalcResultItem.SubItems[j];

      vNode := vGroupNode.AddChild ('ITEM');

      //���������� �� ������� 1-�� ������� ��������� [km]
      vNode.Attributes['DISTANCE_to_center_km'] := oReflectionPoint.DISTANCE_to_center_km;
      //A��������� ������� ��� �������� 1-�� ������� �����. [m]
      vNode.Attributes['ABS_PROSVET_m'] := oReflectionPoint.ABS_PROSVET_m;
      vNode.Attributes['Length_km']     := oReflectionPoint.Length_km;
      vNode.Attributes['RADIUS_km']     := oReflectionPoint.RADIUS_km;
      vNode.Attributes['Signal_Depression_dB']  := oReflectionPoint.Signal_Depression_dB;
    end;

  end;

end;


//--------------------------------------------------------------------
procedure TLinkCalcResult.LoadFromXmlFile_RRL_XML(aFileName: string;
    aB_to_A_calc: boolean = False);
//--------------------------------------------------------------------
//1 �������� ������ �� ����� ����������
//--------------------------------------------------------------------
var
  oXMLDoc: TXMLDoc;

  iIDRoot, iStatus,iValue,iID,i,j,id_parent: integer;

  vReport,vNode,vGroup,vRoot,vDocument: IXMLNode;
  s,sGroupName,sValue,sName,sCaption: string;

  iLen: Integer;
  iGroupID: integer;

  oGroup: TLinkCalcResultGroup;
//  oCalcResultGroup: TLinkCalcResultItem;

  oItem: TLinkCalcResultItem;
  sGroupCaption: string;


begin
  Assert(FileExists(aFileName), aFileName);


 // Items.Clear;

  oXMLDoc:=TXMLDoc.Create;
  oXMLDoc.LoadFromFile (aFileName);

  vDocument:=oXMLDoc.DocumentElement;

//  vDocument := oXMLDoc.DocumentElement;
  vReport   := xml_FindNode(vDocument, 'RRL_PARAMS');


  //and (vReport.ChildNodes.Count>0)

  if Assigned(vReport) then
//  begin
    for i:=0 to vReport.ChildNodes.Count-1 do
    begin
      vGroup:=vReport.ChildNodes[i];
      sGroupName:=vGroup.LocalName;

      Assert (sGroupName<>'');


      if Eq(sGroupName,'rrv') then  sGroupCaption:='_1 �������� ������ - ������� ���' else
      if Eq(sGroupName,'dlt') then  sGroupCaption:='_2 �������� ������ - �������������� ���������' else
      if Eq(sGroupName,'trb') then  sGroupCaption:='_3 �������� ������ - �����' else
      if Eq(sGroupName,'ttx') then  sGroupCaption:='_4 �������� ������ - ��������� ���������' else
      if Eq(sGroupName,'relief') then
        Continue;


      oGroup:=Groups.FindByCaption(sGroupCaption);
    //  oCalcResultItem:=Items.FindGroupByCaption(sGroupCaption);

      if not Assigned(oGroup) then
      begin
        oGroup:=Groups.AddItem;

//        oGroup.Name:=sGroupName;
        oGroup.Caption:=sGroupCaption;

      end;



      for j:=0 to vGroup.ChildNodes.Count-1 do
      begin
        vNode:=vGroup.ChildNodes[j];

        iID:=xml_GetIntAttr(vNode, 'id');

        oItem:=oGroup.SubItems.FindByID(iID);
        if Assigned(oItem) then
          oItem.Value2  :=xml_GetFloatAttr (vNode, 'Value') else

        if not Assigned(oItem) then
        begin
          oItem :=oGroup.SubItems.AddItem;

          oItem.ID     :=iID; //xml_GetIntAttr(vNode, 'id');
          oItem.Caption:=xml_GetStrAttr(vNode, 'comment');
          oItem.Value  :=xml_GetFloatAttr (vNode, 'Value');
        end;

      end;
    end;
//  end;


  FreeAndNil(oXMLDoc);
end;


//-------------------------------------------------------------------
procedure TLinkCalcResult.Load_ReflectionPoints;
//-------------------------------------------------------------------
var
  b: Boolean;
  bHasErrors: Boolean;
  I: Integer;
  j,iCount,iInd: integer;
  i0: Integer;
  i1: Integer;
  i111: Integer;
  i4: Integer;
  i5: Integer;
  iHalf_Count: Integer;
  k: Integer;
  m: Integer;

  oGroup,oGroup1: TLinkCalcResultGroup;

  oReflectionPoint: TReflectionPoint;
const
  MAX_COUNT =8;

//  DEF_reflection_points = 'reflection_points';


begin
  ReflectionPoints.Clear;

  k:=Groups.Count;

  Assert(Groups.Count>1);

  oGroup1 := Groups.FindByCaption('��������� ��������������� �����������');


  oGroup := Groups.FindByCaption('��������� �������� ���������');

  if not Assigned(oGroup) then
    Exit;


{
//  oItem := Items.FindGroupByCaption('��������� �������� ���������');
  oGroup := Groups.FindByName(DEF_reflection_points);

 // oItem:=GetGroupByCaption('��������� �������� ���������');

}

(*        <GROUP name="7" caption=" 7.���������� ������� ���������� �� ������� ��� �����.����.">
      <ITEM id="44" value="1" caption=" ��� ��������� (1-��������,2-������������,3-��������)"/>
      <ITEM id="45" value="1" caption=" K��������� �������� ���������"/>
      <ITEM id="46" value="0" caption=" K��������� ��������������� �����������"/>
*)

{
      <ITEM id="45" value="4" caption=" K��������� �������� ���������"/>


    <GROUP name="reflection_points" caption="��������� ��������������� �����������">
      <ITEM id="89" value="9.45679" caption="���������� �� ������� 1-�� ��������.������-� ����� [km]"/>
      <ITEM id="90" value="0.8452" caption="���������� �� ������� 1-�� ��������.������-� ������[km]"/>
      <ITEM id="91" value="1.00914" caption="A��������� ������� ��� 1-� ��������.������������ [m]"/>
      <ITEM id="92" value="2.44297" caption="����������� ������� ��� 1-� ��������.������������ [m]"/>
      <ITEM id="93" value="0.41307" caption="������������� ������� ��� 1-� ��������.������������"/>
      <ITEM id="94" value="0.03042" caption="������������� 1-�� ���������������� ����������� [km]"/>
      <ITEM id="95" value="8.59313" caption="������ ������� 1-�� ���������������� ����������� [m]"/>
      <ITEM id="96" value="0.01346" caption="P����� ����� ����������������  1-� ��������. ����-� [km]"/>
      <ITEM id="97" value="55.7963" caption="�������� X 1-�� ���������������� �����������  (*1000)"/>
      <ITEM id="98" value="0.19277" caption="�������� Y 1-�� ���������������� �����������"/>
      <ITEM id="100" value="-3.53684" caption="���������� �� 1-�� ��������.����������� [dB]"/>
    </GROUP>

    <GROUP name="reflection_points" caption="��������� �������� ���������">
      <ITEM id="102" value="2.19965" caption="���������� �� ������� 1-�� ������� ��������� [km]"/>
      <ITEM id="103" value="36.44186" caption="A��������� ������� ��� �������� 1-�� ������� �����. [m]"/>
      <ITEM id="104" value="1.76025" caption="����������� ������� ��� �������� 1-�� ������� �����. [m]"/>
      <ITEM id="105" value="20.70257" caption="�����������.������� ��� �������� 1-�� ������� �����."/>
      <ITEM id="106" value="0.23516" caption="������������� 1-�� ������� ��������� [km]"/>
      <ITEM id="107" value="8930.24036" caption="P����� �������� 1-�� ������� ��������� [km]"/>
      <ITEM id="108" value="0.99143" caption="K���������� ������������ 1-�� ������� ���������"/>
      <ITEM id="109" value="0.00001" caption="M����� ������������ ��������� �� 1-�� ������� (��������)"/>
      <ITEM id="110" value="0.00007" caption="���������� �� 1-�� ������� ��������� [dB]"/>
    </GROUP>



}
  //�������� �� ������ ���-��
  iCount:=Groups.GetCalcParamValueByID_AsInteger(45);

  //K��������� ��������������� �����������
  iHalf_Count:=Groups.GetCalcParamValueByID_AsInteger(46);


//  iCount:=Round(Groups.GetCalcParamValueByID_new('7',45));

  //      <ITEM id="45" value="4" caption=" K��������� �������� ���������"/>


//        <ITEM id="46" value="1" caption=" K��������� ��������������� �����������"/>

//  iCount:=Round(oGroup.SubItems.GetCalcParamValueByID (45)); //K��������� �������� ���������"

//          Length(FArr[i].Items) div MAX_COUNT;
//  k:=oGroup.SubItems.Count;

  b:= Groups.ParamExists(102);// then

  b:= Groups.ParamExists(98);// then



  //K��������� �������� ���������"
  if not Groups.ParamExists(102) then
    exit;


  for j:=0 to iCount-1 do
  begin
    bHasErrors:=False;

{
    for m := 0 to 4 do
      if oGroup.SubItems[j+iCount*m] = nil then
      begin
        break;
      end;
}

    if bHasErrors then
      break;



    i0:=oGroup.SubItems[j+iCount*0].ID;
    i1:=oGroup.SubItems[j+iCount*1].ID;

    i4:=oGroup.SubItems[j+iCount*4].ID;

    i111:=j+iCount*5;

    i5:=oGroup.SubItems[j+iCount*5].ID;


    if (oGroup.SubItems[j+iCount*0].ID=102) and
       (oGroup.SubItems[j+iCount*1].ID=103) and
     //  (FArr[i].Items[j+iCount*2].Param=104) and  //
     //  (FArr[i].Items[j+iCount*3].Param=105) and  //
       (oGroup.SubItems[j+iCount*4].ID=106) and
       (oGroup.SubItems[j+iCount*5].ID=107) //and
     //  (FArr[i].Items[j+iCount*6].Param=108) and  //
     //  (FArr[i].Items[j+iCount*7].Param=109) and  //
     //  (FArr[i].Items[j+iCount*8].Param=110)     //
    then
      begin
     {
      if (FArr[i].Items[j+iCount*0].Value=0) or //���������� �� ������� 1-�� ������� ��������� [km]
           (FArr[i].Items[j+iCount*4].Value=0)    //������������� 1-�� �������
        then
           Continue;
      }

        oReflectionPoint:=ReflectionPoints.AddItem;

        with oReflectionPoint do
        begin
          DISTANCE_to_center_km := oGroup.SubItems[j+iCount*0].Value;   //���������� �� ������� 1-�� ������� ��������� [km]
          ABS_PROSVET_m         := oGroup.SubItems[j+iCount*1].Value;  //A��������� ������� ��� �������� 1-�� ������� �����. [m]
                                                      // ����������� ������� ��� �������� 1-�� �������
                                                      // �����������.������� ��� ��������
          LENGTH_km             := oGroup.SubItems[j+iCount*4].Value; //������������� 1-�� �������
          RADIUS_km             := oGroup.SubItems[j+iCount*5].Value;   //P����� �������� 1-�� �������

          Signal_Depression_dB  := oGroup.SubItems[j+iCount*8].Value; //���������� �� 1-�� ������� ��������� [dB]

        end;


     end;
  end;
//   end;


{

    <GROUP name="reflection_points" caption="��������� ��������������� �����������">
      <ITEM id="89" value="18.7893" caption="���������� �� ������� 1-�� ��������.������-� ����� [km]"/>
      <ITEM id="89" value="46.36829" caption="���������� �� ������� 2-�� ��������.������-� ����� [km]"/>
      <ITEM id="90" value="31.3657" caption="���������� �� ������� 1-�� ��������.������-� ������[km]"/>
      <ITEM id="90" value="3.7867" caption="���������� �� ������� 2-�� ��������.������-� ������[km]"/>
      <ITEM id="91" value="12.6055" caption="A��������� ������� ��� 1-� ��������.������������ [m]"/>
      <ITEM id="91" value="5.36418" caption="A��������� ������� ��� 2-� ��������.������������ [m]"/>




<GROUP caption="��������� �������� ���������">
  <ITEM id="102" value="0.55877" caption="���������� �� ������� 1-�� ������� ��������� [km]"/>
  <ITEM id="102" value="2.7137" caption="���������� �� ������� 2-�� ������� ��������� [km]"/>

  <ITEM id="103" value="12.69782" caption="A��������� ������� ��� �������� 1-�� ������� �����. [m]"/>
  <ITEM id="103" value="5.04774" caption="A��������� ������� ��� �������� 2-�� ������� �����. [m]"/>

  <ITEM id="104" value="1.94589" caption="����������� ������� ��� �������� 1-�� ������� �����. [m]"/>
  <ITEM id="104" value="2.96715" caption="����������� ������� ��� �������� 2-�� ������� �����. [m]"/>

  <ITEM id="105" value="6.52544" caption="�����������.������� ��� �������� 1-�� ������� �����."/>
  <ITEM id="105" value="1.70121" caption="�����������.������� ��� �������� 2-�� ������� �����."/>

  <ITEM id="106" value="0.30761" caption="������������� 1-�� ������� ��������� [km]"/>
  <ITEM id="106" value="1.23444" caption="������������� 2-�� ������� ��������� [km]"/>

  <ITEM id="107" value="3.4646" caption="P����� �������� 1-�� ������� ��������� [km]"/>
  <ITEM id="107" value="71.9471" caption="P����� �������� 2-�� ������� ��������� [km]"/>

  <ITEM id="108" value="0.20837" caption="K���������� ������������ 1-�� ������� ���������"/>
  <ITEM id="108" value="0.25459" caption="K���������� ������������ 2-�� ������� ���������"/>

  <ITEM id="109" value="0.2" caption="M����� ������������ ��������� �� 1-�� ������� (��������)"/>
  <ITEM id="109" value="0.2" caption="M����� ������������ ��������� �� 2-�� ������� (��������)"/>

  <ITEM id="110" value="-0.29946" caption="���������� �� 1-�� ������� ��������� [dB]"/>
  <ITEM id="110" value="0.42892" caption="���������� �� 2-�� ������� ��������� [dB]"/>

</GROUP>
}

end;


// ---------------------------------------------------------------
procedure TLinkCalcResult.Parse2;
// ---------------------------------------------------------------
const

//  DEF_LENGTH_KM    = 1;
  DEF_STATUS       = 7;  //����������� ���������

  DEF_SESR         = 17;
  DEF_KNG          = 19;

  DEF_KNG_REQ      = 18;
  DEF_SESR_REQ     = 16;
  DEF_LOS          = 31;
  
  DEF_Threshold = 34; //��������� �������� ������� �� ����� ��������� [dBm] - ����������������

  DEF_Rx_LEVEL_dBm = 101; //��������� ������� ������� �� ����� ��������� (���)

  DEF_MAX_WEAKNESS_14  = 14;  //���������� ���������� (��)
  DEF_DIRECT_LINE_TILT = 95;  //������ ����� ������ ���������(����)


var
  dLos: Double;
  iValue: integer;
  iStatus: integer;

  oItem: TLinkCalcResultItem;
 // oGroup: TLinkCalcResultGroup;

begin
 // with FLinkCalcResult_ref do
  begin
   //��������� ������� ������� �� ����� ��������� (���)
//  <GROUP name="13" caption="13. ���������� ������� ������ ������� �� ����� ���������">

//    Groups.Get

//    oGroup := Groups.FindByName('13');//: TLinkCalcResultItem;

 //   Assert(Assigned(oGroup), '13. ���������� ������� ������ ������� �� ����� ���������');


    Data.Rx_Level_dBm :=Groups.GetCalcParamValueByID(DEF_Rx_LEVEL_dBm);

//..    Data.Rx_Level_dBm :=Groups.GetCalcParamValueByID_new('13',DEF_Rx_LEVEL_dBm);


 //  <GROUP name="2" caption="2. ���������� ������� ����������� ����������� ���������">
    iValue :=Groups.GetCalcParamValueByID_AsInteger(DEF_STATUS);
//    iValue :=AsInteger (Groups.GetCalcParamValueByID_new('2',DEF_STATUS));
    iStatus:=IIF(iValue=1, 1, 2);  //(1)-�����; (2) - �� �����

    Data.STATUS     := iStatus;

    Data.IsAvailable := (iStatus=1);

//  <GROUP name="2" caption="2. ���������� ������� ����������� ����������� ���������">

{    Data.SESR       :=Groups.GetCalcParamValueByID_new('2',DEF_SESR);
    Data.Kng        :=Groups.GetCalcParamValueByID_new('2',DEF_KNG);
    Data.Sesr_Req   :=Groups.GetCalcParamValueByID_new('2',DEF_SESR_REQ);
    Data.KNG_Req    :=Groups.GetCalcParamValueByID_new('2',DEF_KNG_REQ);

    Data.Threshold  :=Groups.GetCalcParamValueByID_new('5',DEF_Threshold);

}

    Data.SESR       :=Groups.GetCalcParamValueByID(DEF_SESR);
    Data.Kng        :=Groups.GetCalcParamValueByID(DEF_KNG);
    Data.Sesr_Req   :=Groups.GetCalcParamValueByID(DEF_SESR_REQ);
    Data.KNG_Req    :=Groups.GetCalcParamValueByID(DEF_KNG_REQ);

    Data.Threshold  :=Groups.GetCalcParamValueByID(DEF_Threshold);

    // ����� �� ��������� [dB]
    Data.fade_margin_dB := RoundFloat (Data.Rx_Level_dBm - Data.Threshold, 2);

    //  <GROUP name="4" caption="4. ���������� ������� �������� ��� ������� ���������">

    dLos        :=Groups.GetCalcParamValueByID(DEF_LOS); //AsFloat (Lookup(FLD_PARAM, DEF_LOS,      FLD_VALUE_));
//    dLos        :=Groups.GetCalcParamValueByID_new('4',DEF_LOS); //AsFloat (Lookup(FLD_PARAM, DEF_LOS,      FLD_VALUE_));
    if dLos>=0  then dLos:=1
                else dLos:=2;

    Data.LOS_STATUS := Round(dLos);

    Data.LOS_IsAvailable:= (Round(dLos)=1);


  //  Data.Length_KM_   :=Items.GetCalcParamValueByID(DEF_LENGTH_KM);

  (*  Data.Rx_Level_dBm:=TruncFloat(Data.Rx_Level_dBm);
    Data.SESR :=TruncFloat(Data.SESR, 6);
    Data.Kng  :=TruncFloat(Data.Kng,  6);
  *)

//  <GROUP name="21" caption="21. ���������� ������� ������������ �������������� ��� SESR">

//  <GROUP name="4" caption="4. ���������� ������� �������� ��� ������� ���������">

    Data.MARGIN_HEIGHT_m:=   Groups.GetCalcParamValueByID(32);//,  '����������� ������������� ������� [m]');
    Data.MARGIN_DISTANCE_km:=Groups.GetCalcParamValueByID(29);//,  '��������� ����� �������� [�m]');

{    Data.MARGIN_HEIGHT_m:=   Groups.GetCalcParamValueByID_new('4',32);//,  '����������� ������������� ������� [m]');
    Data.MARGIN_DISTANCE_km:=Groups.GetCalcParamValueByID_new('4',29);//,  '��������� ����� �������� [�m]');
}

//  <GROUP name="21" caption="21. ���������� ������� ������������ �������������� ��� SESR">


    Data.SESR_SUBREFRACTION:=Groups.GetCalcParamValueByID(146);//,, 'SESR �������������� ������������ [%]');
    Data.SESR_RAIN:=         Groups.GetCalcParamValueByID(147);//, �������� ������������ [%]');
    Data.SESR_INTERFERENCE:= Groups.GetCalcParamValueByID(148);//, 'SESR ����������������� ������������ [%]');

//  <GROUP name="22" caption="22. ���������� ������� ������������ �������������� ��� ���">

    Data.KNG_SUBREFRACTION:=Groups.GetCalcParamValueByID(149);//,, 'SESR �������������� ������������ [%]');
    Data.KNG_RAIN:=         Groups.GetCalcParamValueByID(150);//, �������� ������������ [%]');
    Data.KNG_INTERFERENCE:= Groups.GetCalcParamValueByID(151);//, 'SESR ����������������� ������������ [%]');



{    Data.SESR_SUBREFRACTION:=Groups.GetCalcParamValueByID_new('21',146);//,, 'SESR �������������� ������������ [%]');
    Data.SESR_RAIN:=         Groups.GetCalcParamValueByID_new('21',147);//, �������� ������������ [%]');
    Data.SESR_INTERFERENCE:= Groups.GetCalcParamValueByID_new('21',148);//, 'SESR ����������������� ������������ [%]');

//  <GROUP name="22" caption="22. ���������� ������� ������������ �������������� ��� ���">

    Data.KNG_SUBREFRACTION:=Groups.GetCalcParamValueByID_new('22',149);//,, 'SESR �������������� ������������ [%]');
    Data.KNG_RAIN:=         Groups.GetCalcParamValueByID_new('22',150);//, �������� ������������ [%]');
    Data.KNG_INTERFERENCE:= Groups.GetCalcParamValueByID_new('22',151);//, 'SESR ����������������� ������������ [%]');
}


(*
  <GROUP caption="22. ���������� ������� ������������ �������������� ��� ���">
      <ITEM id="202" value="0.33333" caption="�����.��������� ���.�(�) �� ����������� ������ � ��������� ����"/>
      <ITEM id="224" value="0.02403" caption="���� ���.��������� � ����.����� 10�"/>
      <ITEM id="199" value="0.99961" caption="���� ���.��������� � ����.����� 10� (���)"/>
      <ITEM id="149" value="0" caption="���.� ���������������� ������������ [%]"/>
      <ITEM id="151" value="0.05334" caption="���.� ����������������� ������������ [%]"/>
      <ITEM id="150" value="0.01014" caption="���.� �������� ������������ [%]"/>
      <ITEM id="170" value="0" caption="��� ���-1 (�����) [%]"/>
      <ITEM id="171" value="0" caption="��� ���-2 (������)[%]"/>
      <ITEM id="19" value="0.06348" caption="�������� ���������� ������������ K�� [%]"/>
      <ITEM id="152" value="0.05" caption="����������� �������� ��� ��� ���� [%]"/>
      <ITEM id="42" value="28.40439" caption="������������� ����� ���� [km]"/>
      <ITEM id="18" value="0.01249" caption="����������� �������� K�� �� ��������� [%]"/>
    </GROUP>
*)



//  DEF_MAX_WEAKNESS     = 14;  //���������� ���������� (��)
//  DEF_DIRECT_LINE_TILT = 95;  //������ ����� ������ ���������(����)


//  <GROUP name="2" caption="2. ���������� ������� ����������� ����������� ���������">
  Data.Rains.max_weakness1    := Groups.GetCalcParamValueByID(DEF_MAX_WEAKNESS_14);
//  Data.Rains.max_weakness1    := Groups.GetCalcParamValueByID_new('2',DEF_MAX_WEAKNESS_14);

  //  <GROUP name="12" caption="12. ���������� ������� ���������� ��-�� ������� ������">
//  Data.Rains.direct_line_tilt:= Groups.GetCalcParamValueByID_new('12',DEF_DIRECT_LINE_TILT);
  Data.Rains.direct_line_tilt:= Groups.GetCalcParamValueByID(DEF_DIRECT_LINE_TILT);

//  <GROUP name="1" caption="1. ���������� �������� ������ �� ���������">
  Data.Optimize.TX_ANTENNA_H := Groups.GetCalcParamValueByID(3);
  Data.Optimize.RX_ANTENNA_H := Groups.GetCalcParamValueByID(4);
  Data.Optimize.RX_ANTENNA_DOP_H := Groups.GetCalcParamValueByID(6);
  Data.Optimize.TX_ANTENNA_DOP_H := -99; //Groups.GetCalcParamValueByID(7);


{  Data.Optimize.TX_ANTENNA_H := Groups.GetCalcParamValueByID_new('1',3);
  Data.Optimize.RX_ANTENNA_H := Groups.GetCalcParamValueByID_new('1',4);
  Data.Optimize.RX_ANTENNA_DOP_H := Groups.GetCalcParamValueByID_new('1',6);
}

  end;    // with

(*
                  TX_ANTENNA_H : double;
                  RX_ANTENNA_H : double;
                  RX_ANTENNA_DOP_H : double;
                end;
*)



(*
    if AsBoolean(GetGridValue(TAG_ROW_TX_ANTENNA, col_Check.Index)) then
      SetGridValue(TAG_ROW_TX_ANTENNA, col_H_new.Index, dmLink_calc.GetCalcParamValue(3));

    if AsBoolean(GetGridValue(TAG_ROW_RX_ANTENNA, col_Check.Index)) then
      SetGridValue(TAG_ROW_RX_ANTENNA, col_H_new.Index, dmLink_calc.GetCalcParamValue(4));

    if AsBoolean(GetGridValue(TAG_ROW_RX_ANTENNA_DOP, col_Check.Index)) then
      SetGridValue(TAG_ROW_RX_ANTENNA_DOP, col_H_new.Index, dmLink_calc.GetCalcParamValue(6));


*)

end;





constructor TReflectionPointList.Create;
begin
  inherited Create(TReflectionPoint);
end;


function TReflectionPointList.AddItem: TReflectionPoint;
begin
  Result := TReflectionPoint (inherited Add);
end;

function TReflectionPointList.GetItems(Index: Integer):
    TReflectionPoint;
begin
  Result := TReflectionPoint(inherited Items[Index]);
end;


// ---------------------------------------------------------------
procedure TReflectionPointList.SaveToDataset(aDataset: TDataSet);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  aDataset.Close;
  aDataset.Open;

  db_CreateField(aDataset,
      [
       db_Field(FLD_DISTANCE_to_center_km, ftFloat),
       db_Field(FLD_ABS_PROSVET_m,         ftFloat),
       db_Field(FLD_LENGTH_KM,             ftFloat),
       db_Field(FLD_RADIUS_km,             ftFloat),
       db_Field(FLD_Signal_Depression_dB,     ftFloat)
      ]);


  for I := 0 to Count - 1 do
  begin
    aDataset.Append;
    aDataset[FLD_DISTANCE_to_center_km] := Items[i].DISTANCE_to_center_km;
    aDataset[FLD_ABS_PROSVET_m]         := Items[i].ABS_PROSVET_m;
    aDataset[FLD_Length_km]             := Items[i].Length_km;
    aDataset[FLD_RADIUS_km]             := Items[i].RADIUS_km;
    aDataset[FLD_Signal_Depression_dB]     := Items[i].Signal_Depression_dB;
    aDataset.Post;

  end;

(*
    DISTANCE_to_center_km : Double;
    ABS_PROSVET_m : Double; //A��������� ������� ��� �������� 1-�� ������� �����. [m]
                            //����������� ������� ��� �������� 1-�� �������
                            //�����������.������� ��� ��������
    Length_km : Double;     //������������� 1-�� �������
    RADIUS_km : Double;     //P����� �������� 1-�� �������

    Signal_Depression_dB : double;
*)
end;




// ---------------------------------------------------------------
procedure TLinkCalcResult.LoadFromXmlFile(aFileName: string);
// ---------------------------------------------------------------
var
  sXML: string;
begin
  Assert(FileExists(aFileName), aFileName);

  TextFileToStr(aFileName, sXML);

  LoadFromXmlString(sXML);

end;


// ---------------------------------------------------------------
procedure TLinkCalcResult.LoadFromXmlString(aText: string);
// ---------------------------------------------------------------
var
  oXMLDoc: TXMLDoc;
  vItem,vGroup,vDocument,vNode: IXMLNode;

  eValue: Double;
  I: integer;
  iLen: integer;
  iID: Integer;
  iPos: Integer;
  iValue,j,id_parent: integer;

  sGroupName,sValue,
  //sName,
  sGroupCaption: string;

  oGroup: TLinkCalcResultGroup;
  oItem: TLinkCalcResultItem;
 // oCalcResultItem_new: TLinkCalcResultItem1;

  s: string;
  sCaption: string;
  sItemName: string;

  oReflectionPoint: TReflectionPoint;

//  oGroupItem: TLinkCalcResultGroup;

begin
  Groups.Clear;
//  GroupList.Clear;

  ReflectionPoints.Clear;

  if aText='' then
    exit;



  oXMLDoc:=TXMLDoc.Create;
//  oXMLDoc.LoadFromFile(aFileName);
  oXMLDoc.LoadFromText(aText);


  vDocument := oXMLDoc.DocumentElement;

  // -------------------------
  //
  // -------------------------

  if Assigned(vDocument) then
    for i:=0 to vDocument.ChildNodes.Count-1 do
    begin
      vGroup:=vDocument.ChildNodes[i];
      sItemName:=vGroup.LocalName;

      // ---------------------------------------------------------------
      if Eq(sItemName,'GROUP') then
      // ---------------------------------------------------------------
      begin
 //       sName    :=Trim(xml_GetStrAttr(vGroup, 'Name'));
        sCaption :=Trim(xml_GetStrAttr(vGroup, 'Caption'));

     //   Assert (sName<>'');


        oGroup:=Groups.AddItem;
      //  oGroup.Name    := sName;
        oGroup.Caption := sCaption;


        for j:=0 to vGroup.ChildNodes.Count-1 do
        begin
          vItem:=vGroup.ChildNodes[j];

          oItem:=oGroup.SubItems.AddItem;

          oItem.ID      :=xml_GetIntAttr(vItem, 'id');
          oItem.Caption :=xml_GetStrAttr(vItem, 'Caption');
          oItem.Value   :=xml_GetFloatAttr(vItem, 'Value');
          oItem.Value2  :=xml_GetFloatAttr(vItem, 'Value2');

        end;   
      end;

      // ---------------------------------------------------------------
      if sItemName='ReflectionPoints' then
      // ---------------------------------------------------------------
      begin
        for j:=0 to vGroup.ChildNodes.Count-1 do
        begin
          vItem:=vGroup.ChildNodes[j];

          oReflectionPoint:=ReflectionPoints.AddItem;

      //  oCalcResultGroup.ID      :=xml_GetIntAttr(vItem, 'id');

          with oReflectionPoint do
          begin
            DISTANCE_to_center_km :=xml_GetFloatAttr(vItem, 'DISTANCE_to_center_km');
            ABS_PROSVET_m         :=xml_GetFloatAttr(vItem, 'ABS_PROSVET_m');
            LENGTH_km             :=xml_GetFloatAttr(vItem, 'LENGTH_km');
            RADIUS_km             :=xml_GetFloatAttr(vItem, 'RADIUS_km');
            Signal_Depression_dB  :=xml_GetFloatAttr(vItem, 'Signal_Depression_dB');

          end;

        end;

      end;
    end;

  FreeAndNil(oXMLDoc);


end;

// ---------------------------------------------------------------
procedure TLinkCalcResult.SaveToDataset_DBTree1(aDataset: TDataSet;
    aParent_label: string = '');
// ---------------------------------------------------------------
var
  I: Integer;
  iGroupID: Integer;
  iID: Integer;
  iPARENT_ID: Integer;
 // iRecID: Integer;
  j: Integer;

  oGroup: TLinkCalcResultGroup;
  oItem: TLinkCalcResultItem;

begin
  aDataset.DisableControls;

//  i:=aDataset.FieldCount;

  Assert ( Assigned ( aDataset.FindField( FLD_recID ) ) );


  if aDataset.FieldCount<=1 then
  begin
    aDataset.Close;

    db_CreateField(aDataset,
        [
     //    db_Field(FLD_DXTREE_ID,        ftAutoInc),
//         db_Field(FLD_DXTREE_ID,        ftInteger),
         db_Field(FLD_PARENT_ID, ftInteger),
//         db_Field(FLD_DXTREE_PARENT_ID, ftInteger),

         db_Field(FLD_ID,               ftInteger),
         db_Field(FLD_NAME,             ftString),
         db_Field(FLD_CAPTION,          ftString),
         db_Field(FLD_VALUE,            ftFloat),
         db_Field(FLD_VALUE2,           ftFloat)
        ]);



  end;

  aDataset.Open;


  if aParent_label<>'' then
  begin
    db_AddRecord_(aDataset, [FLD_CAPTION, aParent_label ]);
    iPARENT_ID:=aDataset[FLD_recID];

  end else
    iPARENT_ID:=0;


 // iRecID:=0;

  for I := 0 to Groups.Count - 1 do
  begin

    oGroup := Groups[i];

    db_AddRecord_(aDataset,
      [
     //   FLD_DXTREE_ID,  iRecID,
     //   FLD_NAme,       oGroup.Name,
        FLD_CAPTION,    oGroup.Caption,

        FLD_PARENT_ID,   IIF_NULL(iPARENT_ID)
      ]);

    iGroupID:=aDataset['recID'];

  //  iID:=aDataset[FLD_DXTREE_ID];


    for j := 0 to oGroup.SubItems.Count - 1 do
    begin
    //  Inc(iRecID);
      oItem := oGroup.SubItems[j];

      db_AddRecord_(aDataset,
        [
          FLD_PARENT_ID,   iGroupID,

          FLD_ID,                 oItem.ID,
          FLD_CAPTION,            oItem.Caption,
          FLD_value,              oItem.Value,
          FLD_value2,             oItem.value2
        ]);
    end;
  end;

  aDataset.First;
  aDataset.EnableControls;

end;


// ---------------------------------------------------------------
procedure TLinkCalcResult.SaveToDataset_2_Datasets(aDataset_Group,
    aDataset_Items: TDataSet; aLink_ID: Integer; aCaption: string = '');
// ---------------------------------------------------------------
var
  I: Integer;


  iRecGroupID: Integer;

  j: Integer;

  oGroup: TLinkCalcResultGroup;
  oItem: TLinkCalcResultItem;

begin
//  db_View(aDataset_Group);
//  db_View(aDataset_Items);

   Assert ( Assigned ( aDataset_Group.FindField('ID') ) );
   Assert ( Assigned ( aDataset_Items.FindField('ID') ) );


   if Groups.Count=0 then
     exit;


 //   Assert (Groups.Count>0);


  if aCaption<>'' then
  begin
    db_AddRecord_(aDataset_Group,  [
          FLD_LINK_ID, aLink_ID,
          FLD_CAPTION, aCaption
        ]);

     db_AddRecord_(aDataset_Items,
            [
              FLD_PARENT_ID, aDataset_Group.FieldByName(FLD_ID).AsInteger
            ]);


  end;



  for I := 0 to Groups.Count - 1 do
  begin


    oGroup := Groups[i];

    db_AddRecord_(aDataset_Group,
      [
        FLD_LINK_ID, aLink_ID,

//        FLD_NAme,    oGroup.Name,
        FLD_CAPTION, oGroup.Caption
      ]);

    iRecGroupID:=aDataset_Group.FieldByName(FLD_ID).AsInteger;


    for j := 0 to oGroup.SubItems.Count - 1 do
    begin

      oItem := oGroup.SubItems[j];

      db_AddRecord_(aDataset_Items,
        [
          FLD_PARENT_ID, iRecGroupID,

          FLD_CODE,     oItem.ID,
          FLD_CAPTION,  oItem.Caption,
          FLD_value,    oItem.Value,
          FLD_value2,   oItem.value2
        ]);
    end;
  end;
                        
end;



constructor TLinkCalcResultGroup.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  SubItems := TLinkCalcResultItemList.Create();

end;


destructor TLinkCalcResultGroup.Destroy;
begin
  FreeAndNil(SubItems);
  inherited;
end;


constructor TLinkCalcResultGroupList.Create;
begin
  inherited Create(TLinkCalcResultGroup);
end;

// ---------------------------------------------------------------
function TLinkCalcResultGroupList.AddItem: TLinkCalcResultGroup;
// ---------------------------------------------------------------
begin
  Result := TLinkCalcResultGroup (inherited Add);

end;

{
// ---------------------------------------------------------------
function TLinkCalcResultGroupList.FindByName(aValue: string):
    TLinkCalcResultGroup;
// ---------------------------------------------------------------
var i: integer;
begin
  Result := nil;

  for i:=0 to Count-1 do
    if Eq(Items[i].Name, aValue) then
    begin
      Result:=Items[i];
      Break;
    end;
end;
}

// ---------------------------------------------------------------
function TLinkCalcResultGroupList.FindByCaption(aCaption: string):
    TLinkCalcResultGroup;
// ---------------------------------------------------------------
var i: integer;
begin
  Assert(aCaption<>'');

  Result := nil;

  for i:=0 to Count-1 do
    if Eq(Items[i].Caption, aCaption) then
    begin
      Result:=Items[i];
      Break;
    end;
end;


// ---------------------------------------------------------------
function TLinkCalcResultGroupList.GetCalcParamValueByID(aID: integer): Double;
// ---------------------------------------------------------------
var
  i: integer;
  oItem: TLinkCalcResultItem;

begin
  Result:=0;

  for I := 0 to Count - 1 do    // Iterate
  begin
    oItem := Items[i].SubItems.FindByID(aID);
    if Assigned(oItem) then
    begin
      Result:= oItem.Value;
      exit;
    //  Result:=True;
    end;
  end;

end;

function TLinkCalcResultGroupList.GetCalcParamValueByID_AsInteger(aID:
    integer): integer;
begin
  Result := Round(GetCalcParamValueByID(aID));
end;


// ---------------------------------------------------------------
function TLinkCalcResultGroupList.ParamExists(aID: integer): Boolean;
// ---------------------------------------------------------------
var
  i: integer;
  oItem: TLinkCalcResultItem;

begin
  Result:=False;

  for I := 0 to Count - 1 do    // Iterate
  begin
    oItem := Items[i].SubItems.FindByID(aID);
    if Assigned(oItem) then
    begin
      Result:= True;
      exit;
    //  Result:=True;
    end;
  end;

end;


{
// ---------------------------------------------------------------
function TLinkCalcResultGroupList.GetCalcParamValueByID_new(aName: string; aID:
    integer): Double;
// ---------------------------------------------------------------
var
  oGroup: TLinkCalcResultGroup;
begin

  Result:=0;

  oGroup:=FindByName(aName);

  if Assigned(oGroup) then
    Result := oGroup.SubItems.GetCalcParamValueByID(aID);
end;
}


function TLinkCalcResultGroupList.GetItems(Index: Integer):
    TLinkCalcResultGroup;
begin
  Result := TLinkCalcResultGroup(inherited Items[Index]);
end;



end.






{


// ---------------------------------------------------------------
procedure TLinkCalcResult.Parse2_;
// ---------------------------------------------------------------
const

//  DEF_LENGTH_KM    = 1;
  DEF_STATUS       = 7;  //����������� ���������

  DEF_SESR         = 17;
  DEF_KNG          = 19;

  DEF_KNG_REQ      = 18;
  DEF_SESR_REQ     = 16;
  DEF_LOS          = 31;
  
  DEF_Threshold = 34; //��������� �������� ������� �� ����� ��������� [dBm] - ����������������

  DEF_Rx_LEVEL_dBm = 101; //��������� ������� ������� �� ����� ��������� (���)

  DEF_MAX_WEAKNESS_14  = 14;  //���������� ���������� (��)
  DEF_DIRECT_LINE_TILT = 95;  //������ ����� ������ ���������(����)


var
  dLos: Double;
  iValue: integer;
  iStatus: integer;

  oItem: TLinkCalcResultItem;
  oGroup: TLinkCalcResultGroup;

begin
 // with FLinkCalcResult_ref do
  begin
   //��������� ������� ������� �� ����� ��������� (���)
//  <GROUP name="13" caption="13. ���������� ������� ������ ������� �� ����� ���������">

//    Groups.Get

    oGroup := Groups.FindByName('13');//: TLinkCalcResultItem;

    Assert(Assigned(oGroup), '13. ���������� ������� ������ ������� �� ����� ���������');


    Data.Rx_Level_dBm :=Groups.GetCalcParamValueByID(DEF_Rx_LEVEL_dBm);
//    Data.Rx_Level_dBm :=Groups.GetCalcParamValueByID_new('13',DEF_Rx_LEVEL_dBm);

 //  <GROUP name="2" caption="2. ���������� ������� ����������� ����������� ���������">
    iValue :=Groups.GetCalcParamValueByID_AsInteger(DEF_STATUS);
//    iValue :=AsInteger (Groups.GetCalcParamValueByID_new('2',DEF_STATUS));
    iStatus:=IIF(iValue=1, 1, 2);  //(1)-�����; (2) - �� �����

    Data.STATUS     := iStatus;

    Data.IsAvailable := (iStatus=1);

//  <GROUP name="2" caption="2. ���������� ������� ����������� ����������� ���������">
    Data.SESR       :=Groups.GetCalcParamValueByID(DEF_SESR);
    Data.Kng        :=Groups.GetCalcParamValueByID(DEF_KNG);
    Data.Sesr_Req   :=Groups.GetCalcParamValueByID(DEF_SESR_REQ);
    Data.KNG_Req    :=Groups.GetCalcParamValueByID(DEF_KNG_REQ);

    Data.Threshold  :=Groups.GetCalcParamValueByID(DEF_Threshold);
    Data.fade_margin_dB := Data.Rx_Level_dBm - Data.Threshold;
    //  <GROUP name="4" caption="4. ���������� ������� �������� ��� ������� ���������">

    dLos        :=Groups.GetCalcParamValueByID(DEF_LOS); //AsFloat (Lookup(FLD_PARAM, DEF_LOS,      FLD_VALUE_));
    if dLos>=0  then dLos:=1
                else dLos:=2;

    Data.LOS_STATUS := Round(dLos);

    Data.LOS_IsAvailable:= (Round(dLos)=1);


  //  Data.Length_KM_   :=Items.GetCalcParamValueByID(DEF_LENGTH_KM);

  (*  Data.Rx_Level_dBm:=TruncFloat(Data.Rx_Level_dBm);
    Data.SESR :=TruncFloat(Data.SESR, 6);
    Data.Kng  :=TruncFloat(Data.Kng,  6);
  *)

//  <GROUP name="21" caption="21. ���������� ������� ������������ �������������� ��� SESR">

//  <GROUP name="4" caption="4. ���������� ������� �������� ��� ������� ���������">

    Data.MARGIN_HEIGHT_m:=   Groups.GetCalcParamValueByID(32);//,  '����������� ������������� ������� [m]');
    Data.MARGIN_DISTANCE_km:=Groups.GetCalcParamValueByID(29);//,  '��������� ����� �������� [�m]');

//  <GROUP name="21" caption="21. ���������� ������� ������������ �������������� ��� SESR">

    Data.SESR_SUBREFRACTION:=Groups.GetCalcParamValueByID(146);//,, 'SESR �������������� ������������ [%]');
    Data.SESR_RAIN:=         Groups.GetCalcParamValueByID(147);//, �������� ������������ [%]');
    Data.SESR_INTERFERENCE:= Groups.GetCalcParamValueByID(148);//, 'SESR ����������������� ������������ [%]');

//  <GROUP name="22" caption="22. ���������� ������� ������������ �������������� ��� ���">

    Data.KNG_SUBREFRACTION:=Groups.GetCalcParamValueByID(149);//,, 'SESR �������������� ������������ [%]');
    Data.KNG_RAIN:=         Groups.GetCalcParamValueByID(150);//, �������� ������������ [%]');
    Data.KNG_INTERFERENCE:= Groups.GetCalcParamValueByID(151);//, 'SESR ����������������� ������������ [%]');



(*
  <GROUP caption="22. ���������� ������� ������������ �������������� ��� ���">
      <ITEM id="202" value="0.33333" caption="�����.��������� ���.�(�) �� ����������� ������ � ��������� ����"/>
      <ITEM id="224" value="0.02403" caption="���� ���.��������� � ����.����� 10�"/>
      <ITEM id="199" value="0.99961" caption="���� ���.��������� � ����.����� 10� (���)"/>
      <ITEM id="149" value="0" caption="���.� ���������������� ������������ [%]"/>
      <ITEM id="151" value="0.05334" caption="���.� ����������������� ������������ [%]"/>
      <ITEM id="150" value="0.01014" caption="���.� �������� ������������ [%]"/>
      <ITEM id="170" value="0" caption="��� ���-1 (�����) [%]"/>
      <ITEM id="171" value="0" caption="��� ���-2 (������)[%]"/>
      <ITEM id="19" value="0.06348" caption="�������� ���������� ������������ K�� [%]"/>
      <ITEM id="152" value="0.05" caption="����������� �������� ��� ��� ���� [%]"/>
      <ITEM id="42" value="28.40439" caption="������������� ����� ���� [km]"/>
      <ITEM id="18" value="0.01249" caption="����������� �������� K�� �� ��������� [%]"/>
    </GROUP>
*)



//  DEF_MAX_WEAKNESS     = 14;  //���������� ���������� (��)
//  DEF_DIRECT_LINE_TILT = 95;  //������ ����� ������ ���������(����)


//  <GROUP name="2" caption="2. ���������� ������� ����������� ����������� ���������">
  Data.Rains.max_weakness1    := Groups.GetCalcParamValueByID(DEF_MAX_WEAKNESS_14);

  //  <GROUP name="12" caption="12. ���������� ������� ���������� ��-�� ������� ������">
  Data.Rains.direct_line_tilt:= Groups.GetCalcParamValueByID(DEF_DIRECT_LINE_TILT);

//  <GROUP name="1" caption="1. ���������� �������� ������ �� ���������">
  Data.Optimize.TX_ANTENNA_H := Groups.GetCalcParamValueByID(3);
  Data.Optimize.RX_ANTENNA_H := Groups.GetCalcParamValueByID(4);
  Data.Optimize.RX_ANTENNA_DOP_H := Groups.GetCalcParamValueByID(6);


  end;    // with

(*
                  TX_ANTENNA_H : double;
                  RX_ANTENNA_H : double;
                  RX_ANTENNA_DOP_H : double;
                end;
*)



(*
    if AsBoolean(GetGridValue(TAG_ROW_TX_ANTENNA, col_Check.Index)) then
      SetGridValue(TAG_ROW_TX_ANTENNA, col_H_new.Index, dmLink_calc.GetCalcParamValue(3));

    if AsBoolean(GetGridValue(TAG_ROW_RX_ANTENNA, col_Check.Index)) then
      SetGridValue(TAG_ROW_RX_ANTENNA, col_H_new.Index, dmLink_calc.GetCalcParamValue(4));

    if AsBoolean(GetGridValue(TAG_ROW_RX_ANTENNA_DOP, col_Check.Index)) then
      SetGridValue(TAG_ROW_RX_ANTENNA_DOP, col_H_new.Index, dmLink_calc.GetCalcParamValue(6));


*)

end;






}

{
constructor TLinkCalcResultItem.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  SubItems := TLinkCalcResultItemList.Create();
end;


destructor TLinkCalcResultItem.Destroy;
begin
  FreeAndNil(SubItems);
  inherited;
end;

}



{
//--------------------------------------------------------------------
function TLinkCalcResultItemList.GetCalcParamValueByID_new(aGroupName : string;
    aID: integer): Double;
//--------------------------------------------------------------------
var i,j: integer;
  oItem: TLinkCalcResultItem;
begin
  Result:=0;

  for I := 0 to Count-1  do
    if Eq(Items[i].Name, aGroupName) then
    begin
      for j:= 0 to Items[i].SubItems.Count-1 do
        if Items[i].SubItems[j].ID=aID then
        begin
          Result := Items[i].SubItems[j].Value;
          Exit;
        end;

      Break;
    end;
end;
  }

  


//-------------------------------------------------------------------
function TLinkCalcResultItemList.FindByCaption11111111(aCaption: string):
    TLinkCalcResultItem;
//-------------------------------------------------------------------
var i: integer;
begin
  Result := nil;

  for i:=0 to Count-1 do
    if Eq(Items[i].Caption, aCaption) then
    begin
      Result:=Items[i];
      Break;
    end;
end;

