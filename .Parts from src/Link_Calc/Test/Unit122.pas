unit Unit122;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, Grids, DBGrids, StdCtrls,

  XMLIntf,   

  u_files,

  u_str,
  u_xml_document,  

  u_db, cxVGrid, cxControls, cxInplaceContainer
  ;

type
  TForm1 = class(TForm)
    ADOConnection1: TADOConnection;
    t_Calc_METHOD: TADOTable;
    t_group: TADOTable;
    t_group_param: TADOTable;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    Button1: TButton;
    t_Calc_METHOD_items_XREF1: TADOTable;
    DataSource4: TDataSource;
    cxVerticalGrid1: TcxVerticalGrid;
    cxVerticalGrid1EditorRow1: TcxEditorRow;
    cxVerticalGrid1EditorRow2: TcxEditorRow;
    cxVerticalGrid1EditorRow3: TcxEditorRow;
    cxVerticalGrid1EditorRow4: TcxEditorRow;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    procedure CalcMethod_LoadFromXMLFile;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  CalcMethod_LoadFromXMLFile();
end;


procedure TForm1.Button2Click(Sender: TObject);
begin
(*
  t_Calc_METHOD.First;

  with t_Calc_METHOD do
    while not EOF do
    begin


      Next;
    end;
*)

end;




//-----------------------------------------------------------------
procedure TForm1.CalcMethod_LoadFromXMLFile;
//-----------------------------------------------------------------

var
  b: boolean;
  b2: Boolean;
  b3: Boolean;
  bActive: Boolean;
  bIsFull: Boolean;
  iID: Integer;

  i: integer;
  iCode: Integer;
  iGroupID: Integer;
  vRoot,vGroup,vParam,vNode,vNode1,vNode2,vNode3 : IXMLNode;
  vMETHOD: IXMLNode;
  iLen: Integer;
  iMethod_id: Integer;
  iNumber: Integer;
  j: Integer;
  k: Integer;
  oXMLDoc: TXMLDoc;
  s: string;
  s1: string;
  s2: string;
  s3: string;


  oSList: TStringList;
  sFileName: string;
  sName: string;
  sUnit: string;

begin
//C:\ONEGA\RPLS_DB\bin\link_calc_model_setup


 // if sFileName='' then
  sFileName:='C:\ONEGA\RPLS_DB\bin\link_calc_model_setup\' + 'RRL_ParamSys.xml';


  AdoConnection1.Execute('delete from [calc_method]');
  AdoConnection1.Execute('delete from [group]');

  t_Calc_METHOD.Close;
  t_Calc_METHOD.Open;

  t_Group.Close;
  t_Group.Open;

  t_group_param.Close;
  t_group_param.Open;



//  aFileName:='C:\_test_rrl\RRL_ParamUser.xml';

  assert(FileExists(sFileName));


  if not FileExists(sFileName) then
    Exit;

  oSList:=TStringList.Create;

  // FreeAndNil(oSList);

//  if not assigned(FXMLDoc) then
  oXMLDoc:=TXMLDoc.Create;

//  oXMLDoc1:=TXMLDoc.Create;

  oXMLDoc.LoadFromFile(sFileName);

  vRoot := oXMLDoc.DocumentElement;

 // b:=FileExists(aFileName);

 // if vRoot=NULL then
 //   Exit;


  if VarIsNull(vRoot) then
    Exit;


  k:=vRoot.ChildNodes.Count;

  //for MIF use rasters
  vNode := xml_FindNode (vRoot, 'METHODS');
  if VarIsNull(vRoot) then
    Exit;

  for i := 0 to vNode.ChildNodes.Count - 1 do
//  for i := 0 to 0 do
  begin
    vMETHOD:=vNode.ChildNodes[i];

    iNumber := xml_GetIntAttr(vMETHOD,'numb');
    bIsFull := xml_GetIntAttr(vMETHOD,'full')=1;
    sName   := xml_GetStrAttr(vMETHOD,'name');


    if not t_Calc_METHOD.Locate('id', iNumber, []) then
    begin

      t_Calc_METHOD.Append;
      t_Calc_METHOD['id']:=iNumber;
      t_Calc_METHOD['name']:=sName;
      t_Calc_METHOD.Post;
    end;

//    iMethod_id:= iNumber;
   end;



//  for i := 0 to vNode.ChildNodes.Count - 1 do
  for i := 0 to 0 do
  begin
    vMETHOD:=vNode.ChildNodes[i];

    iNumber := xml_GetIntAttr(vMETHOD,'numb');
    bIsFull := xml_GetIntAttr(vMETHOD,'full')=1;
    sName   := xml_GetStrAttr(vMETHOD,'name');


    if not t_Calc_METHOD.Locate('id', iNumber, []) then
    begin

      t_Calc_METHOD.Append;
      t_Calc_METHOD['id']:=iNumber;
      t_Calc_METHOD['name']:=sName;
      t_Calc_METHOD.Post;
    end;

    iMethod_id:= iNumber;


   // if aCalcMethod<>iNumber then
    //  Continue;



    oSList.Clear;

    s:= Format('%d %s', [iNumber,sName]);
    oSList.AddObject(s, Pointer(iNumber));


  //  for j := 0 to 0 do
    for j := 0 to vMETHOD.ChildNodes.Count - 1 do
    begin
      vGroup:=vMETHOD.ChildNodes[j];

      bActive := xml_GetIntAttr(vGroup,'Active')=1;
      iCode:=xml_GetIntAttr(vGroup,'code');
      sName:=xml_GetStrAttr(vGroup,'name');

  //  if not t_group.Locate('code', iCode, []) then
    begin
      t_group.Append;
     // t_group['group_id']:=imethod_id;
      t_group['code']:=iCode;
      t_group['name']:=sName;
      t_group.Post;

    end;             


    iGroupID:=t_group['id'];


      if bActive then
      begin   

        s:= Format('-%d %d.%s', [iCode,iCode,sName]);
        oSList.AddObject(s, Pointer(iNumber));


        for k := 0 to vGroup.ChildNodes.Count - 1 do
        begin
          vParam:=vGroup.ChildNodes[k];

          s := xml_GetAttr(vParam,'rep');

          ParseStringToString3(s, s1,s2,s3, ';');

          b2:=s2='1';
          b3:=s3='1';

          iCode:=xml_GetIntAttr(vParam,'code');
          sName:=xml_GetAttr(vParam,'name');
          sUnit:=xml_GetAttr(vParam,'dim');


 // if not t_group.Locate('code', iCode, []) then
  begin
    t_group_param.Append;
    t_group_param['group_id']:=iGroupID;
    t_group_param['code']:=iCode;
    t_group_param['Name']:=sName;
    t_group_param['unit']:=sUnit;

//    t_group_param['is_report_full'] :=b2;
//    t_group_param['is_report_short']:=b3;
    t_group_param.Post;
  end;



          if (bIsFull and b2) or (not bIsFull and (b3)) then
          begin
            s:= Format('%d %s', [iCode,sName]);

            oSList.AddObject(s, Pointer(iNumber));

          end;
        end;
      end;
    end;


  //  s:= Format('C:\_test_rrl\%d.txt', [iNumber]);

   // ForceDirByFileName(aOutFileName);
   // oSList.SaveToFile(aOutFileName);



  end;


  FreeAndNil(oXMLDoc);


//////////  ShellExec_Notepad_temp(oSList.Text);


end;

end.



end.
