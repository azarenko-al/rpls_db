
unit u_rrl_param_rec;

interface
uses Variants, SysUtils, Classes, Dialogs,

//  u_xml_document,
                 
  u_geo,
  u_func,
  u_XML;

const
  DEF_Calc_Method_ITU_R     = 0;
  DEF_Calc_Method_GOST53363 = 1;
//        Calc_Method : Integer; // 0 "����, ITU-R � 16 ����� ��" (�� ���������)
 //				                     // 1 "���� � 53363 - 2009"



type
  // ---------------------------------------------------------------
  TrrlCalcParam_ = class
  // ---------------------------------------------------------------
//  TrrlCalcParamRec = packed record

    //-------------------------------------------------------------------
    TTX: record
    //-------------------------------------------------------------------
      Site1_Power_dBm     : double;  //'site1-�������� dBm'
      Site1_DIAMETER      : double;  //'site1-�������');
      Site1_GAIN          : double;  //'site1-��������');
      Site1_VERT_WIDTH    : Double;  //'site1-������ ���');
      Site1_LOSS          : Double;  // + dLinkEndLoss, 'site1-������ � ���');
      Site1_HEIGHT        : Double;  //'site1-������');

      Site2_Threshold_dBM : Double;  //������  dBM
      Site2_DIAMETER      : double;  // 'site1-�������');
      Site2_GAIN          : double;  // 'site1-��������');
      Site2_VERT_WIDTH    : Double;  // 'site1-������ ���');
      Site2_LOSS          : Double;  //  + dLinkEndLoss, 'site1-������ � ���');
      Site2_HEIGHT        : Double;  // 'site1-������');


      Dop_Site1, Dop_Site2:
      record
        DIAMETER  : double;  // 'site1-�������');
        GAIN      : double; //  'site1-��������');
        VERT_WIDTH: Double; //  'site1-������ ���');
        LOSS      : Double; //   + dLinkEndLoss, 'site1-������ � ���');
        HEIGHT    : Double; //  'site1-������');
      end;

      BitRate_Mbps        : double;  // '�������� ��������, ��/�');

      SIGNATURE_WIDTH_Mhz: double;  //  '������ ���������, ���');
      SIGNATURE_HEIGHT : double;  // '������ ���������, ��');
      MODULATION_COUNT : double;  // '����� ������� ���������');
      EQUALISER_PROFIT : double;  // '�������, �������� ������������, ��');

      KRATNOST_BY_SPACE: integer; //'��������� ���������� �� ������������, 1...2'
      KRATNOST_BY_FREQ : integer; //'��������� ���������� �� �������, 1..n+1 //1...8'

      Freq_Spacing_MHz  : Double;  //'��������� ������, ���');

(*      Site1_FrontToBackRatio: Double;  //'ant1 - ����.���.�������� (������/�����) ���-�������, ��')
      Site2_FrontToBackRatio: Double; //'ant2 - ����.���.�������� (������/�����) ���-�������, ��'

      Dop_Site1_FrontToBackRatio: Double; //'ant1 - ����.���.�������� (������/�����) ���-�������, ��'
      Dop_Site2_FrontToBackRatio: Double;  //'ant2 - ����.���.�������� (������/�����) ���-�������, ��')
*)

      Site1_KNG1: Double; //'��� ������������ (%)1'
      Site2_KNG1: Double;  //'��� ������������ (%)2'


      Freq_GHz: Double; //  'P������ �������, ���');
      POLARIZATION: integer;

      Dop_Antenna: record //����� ���������
        POLARIZATION: Integer;//46-'��� �����������, 0-��������������,1-������������,2-�����');
//        UseIt : Boolean;
      end;


      AntennaOffsetHor: Double; //�������������� ����� //���������� ����� ��������� �� ������

      GOST_53363: record
        Modulation_Type  : Integer; // ��� ���������, 0-PSK (QPSK), 1-QAM (TCM)

        RX_count_on_combined_diversity	: Integer;   //44 ���������� ��� ��� ����.����������
      end;


      ATPC_profit_dB : double;// <param id="45" value="10" comment="������� �� ATPC, ��"/>
      Threshold_degradation_dB : Double; //Threshold_degradation_dB,  '���������� ����������������, ��');
    end;


    //-------------------------------------------------------------------
    RRV: record //������� ��������������� ��������� (���)
    //-------------------------------------------------------------------

      gradient_diel_1 : double;   // '������� �������� ��������� ����.�������������,10^-8 1/�' );
      gradient_2      : double;   // '����������� ���������� ���������,10^-8 1/�' );

      underlying_terrain_type_3: Double; //'��� ������������ �������.(1...3,0->����.�����=����.���.)' );

      steam_wet_4     : double;   // '���������� ��������� �������� ����, �/���.�');
      Q_factor_5      : double;   // 'Q-������ ������ �����������');
      climate_factor_6: double;   //'������������� ������ K��, 10^-6');
      factor_B_7      : double;   // '�������� ������������ b ��� ������� ������');
      factor_C_8      : double;   // '�������� ������������ c ��� ������� ������');
      FACTOR_D_9      : double;   // '�������� ������������ d ��� ������� ������');
      RAIN_RATE_10    : double;   // '������������� ����� � ������� 0.01% �������, ��/���');
      REFRACTION_11   : double;       //'�����. ��������� (��� ��.���������=0, ����� �.�.=0)');

      terrain_type_14 : Double;   //'��� ���������.(1...3,0->����.�����=����.���.)' );

      //-------------------------------
      // ����
      //-------------------------------

      GOST_53363: record
        Air_temperature	    : double;   //����������� ������� [��.�]" (�� ��������� 15)
        Atmosphere_pressure : double;   //����������� �������� [����]" (�� ��������� 1013)
        BLCenter            : TBLPoint; //���������� ������� ��������� � ��������
      end;
    end;

    //-------------------------------------------------------------------
    DLT: record
    //-------------------------------------------------------------------
      Calc_Method : Integer; // 0 "����, ITU-R � 16 ����� ��" (�� ���������)
				                     // 1 "���� � 53363 - 2009"

      Profile_Step_KM_1: Double; //  '��� ��������� �������, �� ( 0 - ��������� ����)');

      // default values
      precision_V_diffraction_2: Double;//   '�������� ������ ������� V����(g)=V���.�, ��');
      precision_g_V_diffraction_3: Double;// '�������� ������ ������� g(V����)=g(V���.�), 10^-8');
      max_gradient_for_subrefr_4: Double;//  '������������ �������� ��� ������� ������������, 10^-8');

      NFunc1: Double;
      NFunc2: Double;
      ArgGroup: Double;
      NArg: Double;
      Arg_min: Double;
      Arg_max: Double;
      Arg_step: double; //08.09.2008 ��������� ��� ���������� ��������
    end;

    //-------------------------------------------------------------------
    TRB: record
    //-------------------------------------------------------------------
    //TRB: array[1..4] of double;  //���������� � ����������� �������� ���
     {1=0.012      ;����������� �������� SESR ��� ����, %
      2=0.05       ;����������� �������� ���  ��� ����, %
      3=600        ;����� ����, ��
      4=0.200      ;���������� ����� ���, ������������� �������������
      }

      GST_SESR_1               : double; // '����������� �������� SESR ��� ����, %');
      GST_Kng_2                : double; // '����������� �������� ���  ��� ����, %');
      GST_Length_KM_3          : Double; // '����� ����, ��');
      KNG_dop_4                : double; // '���������� ����� ���, ������������� �������������}');
      SPACE_LIMIT_5            : double; // '���������� ������������� ������� p(a%)');
      SPACE_LIMIT_PROBABILITY_6: double; // '���������� ����������� a% ������������� ������������ �������� p(a%) [%]');

    end;

 {   ReliefCount: Integer;

    ReliefArr: packed array[0..10000] of record
       dist       : Double;
       rel_h      : Double;
       local_h    : Double;
       local_code : smallint;

       earth_h    : Double;//test
    end;
}


    Optimization: record
      OptimizeEnabled: Boolean;
      OptimizeKind: (otMinMax_H1_H2, otMin_H1_plus_H2);

      Criteria_opti_dop_antenna:  (otMin_SESR, otMin_KNG);//  '�������� ����������� ������ ���.���.: 0-min SESR, 1-min KNG');

    end;

    //not used
    ReliefRecordCount : Integer;

    Relief: array of packed record
       Distance_KM: Double;
       rel_h      : Double;
       local_h    : Double;
       local_code : smallint;

       earth_h    : Double;//test
    end;

  private
// TODO: LoadFromBinary
//  procedure LoadFromBinary(aFileName: string);
// TODO: SaveToBinary
//  procedure SaveToBinary(aFileName: string);
//    function LoadFromXML(aFileName: string): boolean;
  public
    ErrorMsg : string;

    function Validate1: boolean;

    function SaveToXML(aFileName: string; aIsSaveProfile: boolean = true): Boolean;
    function ValidateDlg: Boolean;
  end;


implementation

// TODO: LoadFromBinary
////-------------------------------------------------------------------
//procedure TrrlCalcParam_.LoadFromBinary(aFileName: string);
////-------------------------------------------------------------------
//var
//oStream: TFileStream;
//I: Integer;
//begin
/////  Ex
//if not FileExists(aFileName) then
//  Exit;
//
//
//oStream:=TFileStream.Create (aFileName, fmOpenRead);
//oStream.ReadBuffer(TTX, SizeOf(TTX));
//oStream.ReadBuffer(DLT, SizeOf(DLT));
//oStream.ReadBuffer(TRB, SizeOf(TRB));
//oStream.ReadBuffer(RRV, SizeOf(RRV));
//oStream.ReadBuffer(Optimization, SizeOf(Optimization));
//
//oStream.ReadBuffer(ReliefRecordCount, SizeOf(ReliefRecordCount));
//
//SetLength(Relief, ReliefRecordCount);
//
//for I := 0 to ReliefRecordCount-1 do
//  oStream.ReadBuffer(Relief[i], SizeOf(Relief[i]));
//
//oStream.Free;
//end;



//--------------------------------------------------------------------
function TrrlCalcParam_.SaveToXML(aFileName: string; aIsSaveProfile: boolean =
    true): Boolean;
//--------------------------------------------------------------------

  //--------------------------------------------------------------------
  procedure DoAdd (aNode: IXMLNode; aParamID: integer; aValue: Variant; aComment: string='');
  //--------------------------------------------------------------------
  begin
    xml_AddNode (aNode, TAG_PARAM, [xml_Att(ATT_id, aParamID),
                                    xml_Att(ATT_value, aValue),
                                    xml_Att(ATT_COMMENT, aComment ) ]);
  end;

  //--------------------------------------------------------------------
  function DoAddLinkType(aRoot: Variant): Boolean;
  //--------------------------------------------------------------------
  var vGroup: IXMLNode;
      iStep: integer;
      d,dRefraction,dLength: double;
  begin
    Result := False;


//    begin
      // ��� - ������� ��������������� ���������
    //RRV: array[1..11] of double; //�������������� ����� �� �������� ��������������� ���������
     {1=-9.000     ;������� �������� ��������� ����.�������������,10^-8 1/�
      2= 7.000     ;����������� ���������� ���������,10^-8 1/�
      3= 1.000     ;��� ������������ �������.(1...3,0->����.�����=����.���.)
      4= 7.500     ;���������� ��������� �������� ����, �/���.�
      5= 1.000     ;Q-������ ������ �����������
      6=41.000     ;������������� ������ K��, 10^-6
      7= 1.500     ;�������� ������������ b ��� ������� ������
      8= 0.500     ;�������� ������������ c ��� ������� ������
      9= 2.000     ;�������� ������������ d ��� ������� ������
      10=20.000    ;������������� ����� � ������� 0.01% �������, ��/���
      11= 1.335    ;�����. ��������� (��� ��.���������=0, ����� �.�.=0)}


      // -------------------------
      vGroup:=xml_AddNode (aRoot, 'RRV', [xml_Par('comment','������� ��������������� ���������')]);
      // -------------------------
      DoAdd (vGroup, 1,  RRV.gradient_diel_1, '������� �������� ��������� ����.�������������,10^-8 1/�' );
      DoAdd (vGroup, 2,  RRV.gradient_2,      '����������� ���������� ���������,10^-8 1/�' );

      DoAdd (vGroup, 3,  RRV.underlying_terrain_type_3,
                '��� ������������ �������.(1...3,0->����.�����=����.���.) '+
                '1-����������������� 2-������������ 3-������' );

      DoAdd (vGroup, 4,  RRV.steam_wet_4,    '���������� ��������� �������� ����, �/���.�');
      DoAdd (vGroup, 5,  RRV.Q_factor_5,     'Q-������ ������ �����������');
      DoAdd (vGroup, 6,  RRV.climate_factor_6,'������������� ������ K��, 10^-6');
      DoAdd (vGroup, 7,  RRV.factor_B_7,     '�������� ������������ b ��� ������� ������');
      DoAdd (vGroup, 8,  RRV.factor_C_8,     '�������� ������������ c ��� ������� ������');
      DoAdd (vGroup, 9,  RRV.FACTOR_D_9,     '�������� ������������ d ��� ������� ������');
      DoAdd (vGroup, 10, RRV.RAIN_RATE_10,    '������������� ����� � ������� 0.01% �������, ��/���');
      DoAdd (vGroup, 11, RRV.REFRACTION_11,    '�����. ��������� (��� ��.���������=0, ����� �.�.=0)');

      if Dlt.Calc_Method = DEF_Calc_Method_GOST53363 then
      begin
        DoAdd (vGroup, 12, RRV.GOST_53363.air_temperature,     '����������� ������� [��.�] (�� ��������� 15)');
        DoAdd (vGroup, 13, RRV.GOST_53363.atmosphere_pressure, '����������� �������� [����] (�� ��������� 1013)');
      end;

      DoAdd (vGroup, 14, RRV.terrain_type_14,
                '��� ���������(0...2) 0-���������� 1-���������� (��������) 2-���������� (������)' );

      if Dlt.Calc_Method = DEF_Calc_Method_GOST53363 then
      begin
        DoAdd (vGroup, 15, TruncFloat(RRV.GOST_53363.BLCenter.B, 6), '���������� ������� ��������� � �������� (������)');
        DoAdd (vGroup, 16, TruncFloat(RRV.GOST_53363.BLCenter.L, 6), '���������� ������� ��������� � �������� (�������)');
      end;

{      if RRV.gradient=0 then
      begin aErrMsg:='����������� ���������� ���������,10^-8 1/�=0';
            Exit;

      end;
}


//      DoAdd (vGroup, 11, 1.335 {FieldValues[FLD_refraction_gradient_0]}, '�����. ��������� (��� ��.���������=0, ����� �.�.=0)');

    //DLT: array[1..4] of double;  //���������� � �������� ��������
     {1= 0.100     ;��� ��������� �������, �� ( 0 - ��������� ����)
      2= 1.000     ;�������� ������ ������� V����(g)=V���.�, ��
      3= 1.000     ;�������� ������ ������� g(V����)=g(V���.�), 10^-8
      4=80.000     ;������������ �������� ��� ������� ������������, 10^-8}

      // -------------------------
      vGroup:=xml_AddNode (aRoot, 'DLT', [xml_Par('comment','��������� �����')]);
      // -------------------------
//      iStep:=FieldByName(FLD_profile_step).AsInteger;

      DoAdd (vGroup,  1, DLT.Profile_Step_KM_1, '��� ��������� �������, �� ( 0 - ��������� ����)');

      // default values
      DoAdd (vGroup,  2, DLT.precision_V_diffraction_2,   '�������� ������ ������� V����(g)=V���.�, ��');
      DoAdd (vGroup,  3, DLT.precision_g_V_diffraction_3, '�������� ������ ������� g(V����)=g(V���.�), 10^-8');
      DoAdd (vGroup,  4, DLT.max_gradient_for_subrefr_4,  '������������ �������� ��� ������� ������������, 10^-8');


      if DLT.NFunc1>0 then
      begin
        DoAdd (vGroup,  5, DLT.NFunc1,   '����� ������ ������� 1..179');
        DoAdd (vGroup,  6, DLT.NFunc2,   '����� ������ ������� 1..179');
        DoAdd (vGroup,  7, DLT.ArgGroup, '����� ������ ���������� 1-TTX, 2-PPB');
        DoAdd (vGroup,  8, DLT.NArg,     '����� ��������� 1..42 ��� ���, 1..11 ��� ���');
        DoAdd (vGroup,  9, DLT.Arg_min,  '��� �������� ���������');
        DoAdd (vGroup,  10,DLT.Arg_max,  '���� �������� ���������');
        DoAdd (vGroup,  11,DLT.Arg_step, '��� �������� �������� ���������');
      end;

      DoAdd (vGroup,  12, DLT.Calc_Method, '0-����, ITU-R � 16 ����� �� (�� ���������), 1-���� � 53363 - 2009');

{
   Calc_Method : Integer; // 0 "����, ITU-R � 16 ����� ��" (�� ���������)
				                      // 1 "���� � 53363 - 2009"

}


     //08.09.2008 ��������� ��� ���������� ��������



    //TRB: array[1..4] of double;  //���������� � ����������� �������� ���
     {1=0.012      ;����������� �������� SESR ��� ����, %
      2=0.05       ;����������� �������� ���  ��� ����, %
      3=600        ;����� ����, ��
      4=0.200      ;���������� ����� ���, ������������� �������������}
      // -------------------------
      vGroup:=xml_AddNode (aRoot, 'TRB', [xml_Par('comment','����������')]);
      // -------------------------

      DoAdd (vGroup,  1, Trb.GST_SESR_1,      '����������� �������� SESR ��� ����, %');
      DoAdd (vGroup,  2, Trb.GST_Kng_2,       '����������� �������� ���  ��� ����, %');
      DoAdd (vGroup,  3, Trb.GST_Length_km_3, '������������� ����� ����, ��');
      DoAdd (vGroup,  4, Trb.KNG_dop_4,       '���������� ����� ���, ������������� �������������}');
      DoAdd (vGroup,  5, Trb.SPACE_LIMIT_5,   '���������� ������������� ������� p(a%)');
      DoAdd (vGroup,  6, Trb.SPACE_LIMIT_PROBABILITY_6,  '���������� ����������� a% ������������� ������������ �������� p(a%) [%]');

  //  end;

    result := True;
  end;


  //--------------------------------------------------------------------
  procedure DoSaveProfileToXMLNode (vRoot: IXMLNode);
  //--------------------------------------------------------------------
  var i: integer;
    vGroup: IXMLNode;
    eDist: double;
  begin
    vGroup:=xml_AddNode (vRoot, 'relief',
                         [xml_Att('count',      Length(relief)),
                          xml_Att('Length',     TRB.GST_Length_KM_3),
                          xml_Att('Length_km',  TRB.GST_Length_KM_3)
                         ]);

    for i:=0 to High(relief) do
    begin
      if relief[i].Distance_KM<0 then
        relief[i].Distance_KM:=0;

//      Assert(relief[i].dist >=0 );

      eDist:=TruncFloat(relief[i].Distance_KM, 4);

      xml_AddNode (vGroup, 'item',
         [xml_ATT ('dist',       eDist),
          xml_ATT ('rel_h',      relief[i].rel_h),
          xml_ATT ('local_h',    relief[i].local_h),
          xml_ATT ('local_code', relief[i].local_code)//,
     //     xml_ATT ('earth_h',    relief[i].earth_h)
         ]);
    end;
  end;

  // -------------------------------------------------------------------


var
  vRoot,vGroup: IXMLNode;
  oXMLDoc: TXMLDoc;

 // oXMLDocument: XMLDoc.TXMLDocument;
//  .FileName :=  aFileName;//  'd:\doc.kml';
 // XMLDocument1.Active := True;


begin
  result := False;


  oXMLDoc := TXMLDoc.Create(
 // gl_XMLDoc.Clear;

  vRoot:=xml_AddNodeTag (oXMLDoc.DocumentElement, 'RRL_PARAMS');

  if not DoAddLinkType (vRoot) then
  begin
    oXMLDoc.Free;
    exit;
  end;

  vGroup:=xml_AddNode (vRoot, 'TTX', [xml_Att('comment', '����������� ��������������')]);

  DoAdd (vGroup,  1, TTX.Site1_Power_dBm,      'site1-�������� dBm');
  DoAdd (vGroup,  2, TTX.Site1_DIAMETER,       'site1-�������');
  DoAdd (vGroup,  3, TTX.Site1_GAIN,           'site1-�������� dB');
  DoAdd (vGroup,  4, TTX.Site1_VERT_WIDTH,     'site1-������ ���');
  DoAdd (vGroup,  5, TTX.Site1_LOSS,           'site1-������ � ���');
  DoAdd (vGroup,  6, TTX.Site1_HEIGHT,         'site1-������');

  DoAdd (vGroup,  7,  TTX.Site2_Threshold_dBM, 'site2-������ dBM');
  DoAdd (vGroup,  8,  TTX.Site2_DIAMETER,      'site2-�������');
  DoAdd (vGroup,  9,  TTX.Site2_GAIN,          'site2-�������� dB');
  DoAdd (vGroup,  10, TTX.Site2_VERT_WIDTH,    'site2-������ ���');
  DoAdd (vGroup,  11, TTX.Site2_LOSS,          'site2-������ � ���');
  DoAdd (vGroup,  12, TTX.Site2_HEIGHT,        'site2-������');



  // -------------------------------------------------------------------
  //  POLARIZATION
  // -------------------------------------------------------------------
  DoAdd (vGroup, 13, TTX.POLARIZATION, '��� �����������, 0-��������������,1-������������,2-�����');
  DoAdd (vGroup, 14, TTX.Freq_GHz,     'P������ �������, ���');

 // if TTX.Freq_GHz=0 then begin aErrMsg:='������� GHz =0'; Exit; end;

  // -------------------------------------------------------------------
  //  LEFT  - ����������
  // -------------------------------------------------------------------

///////////!!!!!!!
//  DoAdd (vGroup, 15, FieldValues[FLD_Speed],            '�������� ��������, ��/�');
  DoAdd (vGroup, 15, TTX.BitRate_Mbps,     '�������� ��������, ��/�');
  DoAdd (vGroup, 16, TTX.SIGNATURE_WIDTH_Mhz,  '������ ���������, ���');
  DoAdd (vGroup, 17, TTX.SIGNATURE_HEIGHT, '������ ���������, ��');

//  TTX.Modulation_Type :=1;
  DoAdd (vGroup, 18, TTX.GOST_53363.Modulation_Type,  '��� ���������, 0-PSK (QPSK), 1-QAM (TCM)');

////////!!!!!!!    DoAdType).AsInteger, '��� ������� ���������, 1-���, 2-��, 3-���');
  DoAdd (vGroup, 19, TTX.MODULATION_COUNT,  '����� ������� ���������');
  DoAdd (vGroup, 20, TTX.EQUALISER_PROFIT,  '�������, �������� ������������, ��');


    //////////////
//    iValue:=FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger + 1;
  //  DoAdd (vGroup, 21, iValue, '��������� ���������� �� �������, 1...8');  //�� ���


  // -------------------------------------------------------------------
  //  RIGHT  - ��������
  // -------------------------------------------------------------------


  DoAdd (vGroup, 21, ttx.KRATNOST_BY_FREQ,  '��������� ���������� �� �������, 1...8');  //�� ���
  DoAdd (vGroup, 22, ttx.KRATNOST_BY_SPACE, '��������� ���������� �� ������������, 1...2'); //�� ���
  DoAdd (vGroup, 23, ttx.Freq_Spacing_MHz,   '��������� ������, ���');


{ 24-28 - ��� ��� 1
  29-33 - ��� ��� 2
  }


  if TTX.Dop_Site1.GAIN>0 then
  begin
    DoAdd (vGroup,  24, TTX.Dop_Site1.DIAMETER,      'dop-site1-�������');
    DoAdd (vGroup,  25, TTX.Dop_Site1.GAIN,          'dop-site1-��������  dB');
    DoAdd (vGroup,  26, TTX.Dop_Site1.VERT_WIDTH  ,  'dop-site1-������ ���');
    DoAdd (vGroup,  27, TTX.Dop_Site1.LOSS,          'dop-site1-������ � ���');
    DoAdd (vGroup,  28, TTX.Dop_Site1.HEIGHT,        'dop-site1-������');

  end;

  if TTX.Dop_Site2.GAIN>0 then
  begin
    DoAdd (vGroup,  29, TTX.Dop_Site2.DIAMETER,     'dop-site2-�������');
    DoAdd (vGroup,  30, TTX.Dop_Site2.GAIN,         'dop-site2-�������� dB');
    DoAdd (vGroup,  31, TTX.Dop_Site2.VERT_WIDTH ,  'dop-site2-������ ���');
    DoAdd (vGroup,  32, TTX.Dop_Site2.LOSS,         'dop-site2-������ � ���');
    DoAdd (vGroup,  33, TTX.Dop_Site2.HEIGHT,       'dop-site2-������');
  end;


(*
  // remove !!!
  DoAdd (vGroup, 34, TTX.Site1_FrontToBackRatio, 'ant1 - ����.���.�������� (������/�����) ���-�������, ��');
  DoAdd (vGroup, 35, TTX.Site2_FrontToBackRatio, 'ant2 - ����.���.�������� (������/�����) ���-�������, ��');

   //dop
  DoAdd (vGroup, 36, TTX.Dop_Site1_FrontToBackRatio, 'ant1 - ����.���.�������� (������/�����) ���-�������, ��');
  DoAdd (vGroup, 37, TTX.Dop_Site2_FrontToBackRatio, 'ant2 - ����.���.�������� (������/�����) ���-�������, ��');
*)

  DoAdd (vGroup, 38, TTX.Site1_KNG1,  '��� ������������ (%)1');
  DoAdd (vGroup, 39, TTX.Site2_KNG1,  '��� ������������ (%)2');

  DoAdd (vGroup, 40, TTX.AntennaOffsetHor,  '�������������� �����');

   //04.02.2011
  DoAdd (vGroup, 41, TTX.Threshold_degradation_dB,  '���������� ����������������, ��');


  //------------------------------------------------------
  // !! ����������� ���������� ��� ������������� ������� �������
  //------------------------------------------------------
  if  Optimization.OptimizeEnabled then
  begin
//  (otNone, otMinMax_H1_H2, otMin_H1_plus_H2);
// then
    DoAdd (vGroup, 42, IIF(Optimization.OptimizeKind=otMinMax_H1_H2,0,1),  '�������� �����������');

    {
    36-37 - ��� ��� 1,2
    }

    DoAdd (vGroup, 43, IIF(Optimization.Criteria_opti_dop_antenna=otMin_SESR,0,1),  '�������� ����������� ������ ���.���.: 0-min SESR, 1-min KNG');
  end;

  //----------------------
  // GOST_53363
  //----------------------

  if Dlt.Calc_Method = DEF_Calc_Method_GOST53363 then
  begin
//    if TTX.GOST_53363.RX_count_on_combined_diversity then
      

    DoAdd (vGroup, 44, TTX.GOST_53363.RX_count_on_combined_diversity,  '���������� ���������� ��� ����.����������');
  end;

   //04.02.2011
  DoAdd (vGroup, 45, TTX.ATPC_profit_dB,  '������� �� ATPC, ��');


  DoAdd (vGroup, 46, TTX.Dop_Antenna.POLARIZATION, '��� �����������, 0-��������������,1-������������,2-�����');




 // gl_XMLDoc.SaveToFile(aFileNameNoProfile);
  if aIsSaveProfile then
    DoSaveProfileToXMLNode (vRoot);

  oXMLDoc.SaveToFile(aFileName);
 // gl_XMLDoc.SaveToFile(TEMP_IN_FILENAME);

  result := True;

  oXMLDoc.Free;


//  FreeAndNil(oXMLDocument);
end;



function TrrlCalcParam_.ValidateDlg: boolean;
begin
  Result :=Validate1;
  if not Result then
    ShowMessage(ErrorMsg);
end;


// ---------------------------------------------------------------
function TrrlCalcParam_.Validate1: boolean;
// ---------------------------------------------------------------
var
  oStrList: TStringList;
begin
  oStrList:=TStringList.Create;

  if TTX.Freq_GHz=0 then
    oStrList.Add('������� GHz = 0');

  if RRV.gradient_2=0 then
    oStrList.Add('����������� ���������� ���������, 10^-8 1/�=0');

  // ������ �� ����� ��������
  if (DLT.Calc_Method=1) then
  begin
    if TTX.BitRate_Mbps=0 then
      oStrList.Add('BitRate Mbps = 0');

    if (RRV.GOST_53363.Air_temperature=0) then
      oStrList.Add('GOST_53363.Air_temperature=0');

    if RRV.GOST_53363.BLCenter.B=0 then
      oStrList.Add('GOST_53363.���������� ������� ��������� � �������� (������)=0');

    if RRV.GOST_53363.BLCenter.L=0 then
      oStrList.Add('GOST_53363.���������� ������� ��������� � �������� (�������)=0');

  end;

  ErrorMsg:=oStrList.Text;

  Result := oStrList.Count=0;

  oStrList.Free;
end;


end.

//=========================================================
//const
//{
//  TRB_PARAMS1: array[1..4] of string = (
//    '����������� �������� SESR ��� ���� [%]',
//    '����������� �������� ���  ��� ���� [%]',
//    '����� ���� [��]',
//    '���������� ����� ���, ������������� �������������');
//
//  DLT_PARAMS: array[1..4] of string = (
//    '��� ��������� ������� (0 - ��������� ����) [��]',
//    '�������� ������ ������� V����(g)=V���.� [��]',
//    '�������� ������ ������� g(V����)=g(V���.�), 10^-8',
//    '������������ �������� ��� ������� ������������, 10^-8');
//
//  RRV_PARAMS: array[1..11] of string = (
//    '������� �������� ��������� ����.�������������, 10^-8 [1/�]',
//    '����������� ���������� ���������, 10^-8 [1/�]',
//    '��� ������������ �������.(1...3,0->����.�����=����.���.)',
//    '���������� ��������� �������� ���� [�/���.�]',
//    'Q-������ ������ �����������',
//    '������������� ������ K�� [10^-6]',
//    '�������� ������������ b ��� ������� ������',
//    '�������� ������������ c ��� ������� ������',
//    '�������� ������������ d ��� ������� ������',
//    '������������� ����� � ������� 0.01% ������� [��/���]',
//    '�����. ��������� (��� ��.���������=0, ����� �.�.=0)');
//
//  TTX_PARAMS: array[1..37] of string = (
//    '�������� ����������� [���]',
//    '������� ���-������� [�]',
//    'K���������� �������� ���������� ������� [��]',
//    '������ ��������� �������������� ���-������� [����]',
//    '������ � A��-��� [��]',
//    'B����� ���������� ������� [�]',
//    '��������� ���������������� ��������� ��� BER=10^-3 [���]',
//    '������� ���-������� [�]',
//    'K���������� �������� �������� ������� [��]',
//    '������ ��������� �������������� ���-������� [����]',
//    '������ � A��-��� [��]',
//    'B����� ���-������� [�]',
//
//    '��� �����������, 0-��������������,1-������������',
//    'P������ ������� [���]',
//    '�������� �������� [��/�]',
//    '������ ��������� [���]',
//    '������ ��������� [��]',
//    '��� ������� ���������, 0-���, 1-��, 2-���',
//    '����� ������� ���������',
//    '�������, �������� ������������ [��]',
//    '��������� ���������� �� ������� [1...8]',
//    '��������� ���������� �� ������������ [1...2]',
//    '��������� ������ [���]',
//
//    '������� ���-������� (���) [�]',
//    'K���������� �������� ���������� ������� (���) [��]',
//    '������ ��������� �������������� ���-������� (���) [����]',
//    '������ � A��-��� (���) [��]',
//    'B����� ���������� ������� (���) [�]',
//
//    '������� ���-������� (���) [�]',
//    'K���������� �������� �������� ������� (���) [��]',
//    '������ ��������� �������������� ���-������� (���) [����].',
//    '������ � A��-��� (���) [��]',
//    'B����� ���-������� (���) [�]',
//    '����.���.�������� (������/�����) ���-������� [��]',
//    '����.���.�������� (������/�����) ���-������� [��]',
//    '����.���.�������� (������/�����) ���-������� (���) [��]',
//    '����.���.�������� (������/�����) ���-������� (���) [��]');
//
//}
//
//--------------------------------------------------------------------
//function TrrlCalcParam_.LoadFromXML(aFileName: string): boolean;
//--------------------------------------------------------------------
//var
//  vNode,vRoot,vGroup: IXMLNode;
//  i,j: integer;
//  ind: integer;  fValue: double;
//  oXMLDoc: TXMLDoc;
//begin
//
//{
//  Result:=False;
//
//  oXMLDoc:=TXMLDoc.Create;
//  oXMLDoc.LoadFromFile (aFileName);
//
//  vRoot:=xml_GetNodeByPath (oXMLDoc.DocumentElement, ['RRL_PARAMS']); //RRL_PARAMS
//  if VarIsNull(vRoot) then
//  begin
//    raise Exception.Create('');
//    Exit;
//  end;
//
//
//  for j:=1 to 4 do
//  begin
//    case j of
//      1: vGroup:=xml_GetNodeByPath (vRoot, ['TTX']);
//      2: vGroup:=xml_GetNodeByPath (vRoot, ['RRV']);
//      3: vGroup:=xml_GetNodeByPath (vRoot, ['DLT']);
//      4: vGroup:=xml_GetNodeByPath (vRoot, ['TRB']);
//    end;
//
//    for i:=0 to vGroup.ChildNodes.Length-1 do
//    begin
//      vNode:=vGroup.ChildNodes.Item(i);
//      ind   :=AsInteger(xml_GetAttr (vNode, 'id'));
//      fValue:=AsFloat  (xml_GetAttr (vNode, 'value'));
//
//      case j of
//        1: if (ind>=1) and (ind<=42) then TTX.Items[ind]:=fValue;  //26.10.2005 37->39; 26.02.2008 39->41; 18.06.2008 41->42
//        2: if (ind>=1) and (ind<=11) then RRV.Items[ind]:=fValue;
//        3: if (ind>=1) and (ind<=11) then DLT.Items[ind]:=fValue;  //09.09.2008 4->11
//        4: if (ind>=1) and (ind<=6)  then TRB.Items[ind]:=fValue;  //18.05.2006 4->6
//      end;
//    end;
//  end;
//
//
//  Result:=True;
//  oXMLDoc.Free;}
//end;


