program test_Rel_Engine;

uses
  Forms,
  Unit8 in 'Unit8.pas' {Form8},
  dm_act_Rel_Engine in '..\dm_act_Rel_Engine.pas' {dmAct_Rel_Engine: TDataModule},
  dm_Rel_Engine in '..\dm_Rel_Engine.pas' {dmRel_Engine: TDataModule};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TForm8, Form8);
  Application.CreateForm(TdmAct_Rel_Engine, dmAct_Rel_Engine);
  Application.CreateForm(TdmRel_Engine, dmRel_Engine);
  Application.Run;
end.
