unit dm_act_Rel_Engine;

interface

uses
  Classes, Forms, Windows, Messages,

  u_func_msg,
  u_const_msg,

  dm_Rel_Engine, AppEvnts
  ;


type
  TdmAct_Rel_Engine = class(TDataModule, IMessageHandler)
    ApplicationEvents1: TApplicationEvents;
    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
   // procedure DataModuleCreate(Sender: TObject);
  protected
    
    procedure GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean);
  public
    class procedure Init;
  end;


var
  dmAct_Rel_Engine: TdmAct_Rel_Engine;

//==================================================================
// implementation
//==================================================================
implementation

uses dm_Onega_db_data;    {$R *.DFM}



//--------------------------------------------------------------------
class procedure TdmAct_Rel_Engine.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_Rel_Engine) then
    dmAct_Rel_Engine:=TdmAct_Rel_Engine.Create (Application);
end;



//--------------------------------------------------------------------
procedure TdmAct_Rel_Engine.GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean);
//--------------------------------------------------------------------
begin
  case aMsg of
    WE_PROJECT_CHANGED_: dmRel_Engine.Close1; //ChangeProject();
  end;
end;


procedure TdmAct_Rel_Engine.ApplicationEvents1Message(var Msg: tagMSG; var
    Handled: Boolean);
var
  b: Boolean;
  k: Integer;
begin
  case Msg.Message of
    WE_PROJECT_CHANGED_: dmRel_Engine.Close1; //ChangeProject();

    WM_USER + Integer(WM__REL_MATRIX_ENABLED):

        begin
           b:=Boolean(Msg.lParam);

           k:=dmOnega_DB_data.Relief_XREF_Update1 (Msg.wParam,  Boolean(Msg.lParam));

        end;


   // PostUserMessage(, oPIDL.ID, Integer(b)));


  end;

end;


begin

end.

{


   PostUserMessage(WM__REL_MATRIX_ENABLED, oPIDL.ID, Integer(b)));

        k:=dmOnega_DB_data.Relief_XREF_Update1 (oPIDL.ID, b);
 //       k:=dmOnega_DB_data.Relief_XREF_Update (oPIDL.ref_ID, b);

        s:=oNode.Values[col_Name_.ItemIndex];
        if not b then
          dmRel_Engine.CloseFile(s);



