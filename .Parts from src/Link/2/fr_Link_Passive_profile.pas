unit fr_Link_Passive_profile;

interface
{$I ver.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, cxSplitter, StdCtrls, ExtCtrls,
  Dialogs,  DB, rxPlacemnt, ActnList, cxControls, TB2Item, TB2Dock, TB2Toolbar, ToolWin, ComCtrls,
  IniFiles,

  u_vars,

  f_Custom,

//  I_Link,
  dm_Main,

//  u_link_passive,

  u_rrl_param_rec_new,
  //dm_Link_passive_calc,


  dm_LinkEnd,
  dm_LinkLine,
//  dm_Link_profile,
  dm_Link,
//  dm_Link_Reflection_Points,
  dm_Link_Summary,

  dm_Rel_Engine,

  u_Rel_Profile,

  u_run,
  u_Geo,
  u_func,
  u_dlg,
  u_files,
  u_db,
  u_reg,

  u_const,
  u_const_db,

  u_link_const,

  u_types,

  fr_Link_profile_ant,
  fr_Link_profile_summary,

//  fr_profile_Rel_Sites,

  cxPropertiesStore, cxPC

  , fr_Profile_Rel;

type


  Tframe_Link_passive_profile = class(Tfrm_Custom)
    pn_Summary: TPanel;  
    ActionList1: TActionList;
    act_Calc: TAction;
    act_Report: TAction;
    act_Save: TAction;
    act_Is_CalcAfterChange: TAction;
    TBDock1: TTBDock;
    TBItem1: TTBItem;
    TBToolbar1: TTBToolbar;
    cxSplitter2: TcxSplitter;
    act_Refl_points: TAction;
    pn_Main: TPanel;
    pn_Profile: TPanel;
    pn_Bottom: TPanel;
    cxSplitter1: TcxSplitter;
    act_Fren_Top: TAction;
    act_Fren_Left: TAction;
    act_Fren_Right: TAction;
    act_Fren_Bot: TAction;
    act_Reload_Profile: TAction;
    act_Optimize_Freq: TAction;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    pn_Site1_active: TPanel;
    pn_Site1_passive: TPanel;
    pn_Site2_passive: TPanel;
    pn_Site2_active: TPanel;

    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);

    procedure _Actions(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction;  var Handled: Boolean);
    procedure cb_Reflection_pointsClick(Sender: TObject);
    procedure cb_Additional_Rain_AccountingClick(Sender: TObject);
    procedure act_Fren_TopExecute(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    FrrlCalcParam: TrrlCalcParam;

    FOnUpdateAfterCalc: TNotifyEvent;

    FLink1ParamRec: TdmLinkInfoRec;
    FLink2ParamRec: TdmLinkInfoRec;

    FLinkLinePassiveRec: TdmLinkLinePassiveInfoRec;


    FrelProfileCommon,
    FrelProfile1,
    FrelProfile2: TrelProfile;

    FLinkParamRec: TdmLinkPassiveInfoRec;

    FLinkLineID: integer;
    FIsBeginUpdate: boolean;

    Fframe_Profile_Rel: Tframe_profile_Rel;
    Fframe_Profile_Summary: Tframe_Link_profile_Summary;

    Fframe_Site1_Active: Tframe_Link_profile_ant;
    Fframe_Site1_Passive: Tframe_Link_profile_ant;
    Fframe_Site2_Active: Tframe_Link_profile_ant;
    Fframe_Site2_Passive: Tframe_Link_profile_ant;


    Fframe_Link_profile_summary: Tframe_Link_profile_summary;

    procedure DoOnChangedEdits (Sender: TObject);
    procedure ShowSites;

    procedure DoOnDataChanged(Sender: TObject);

    procedure DoOnProfileChanged(Sender: TObject);

    procedure Reload_Profile(aIndex: Integer);

//    procedure Calc_;

  public
    procedure View (aLinkLineID: integer);

    property OnUpdateAfterCalc: TNotifyEvent read FOnUpdateAfterCalc write FOnUpdateAfterCalc;

    procedure Calc;
//    procedure Calc_old;

    procedure DoOnCalc(Sender: TObject);
    procedure DoOnChangeAntennaHeight(Sender: TObject);
  end;


//====================================================================
//implementation
//====================================================================
implementation {$R *.dfm}


//--------------------------------------------------------------------
procedure Tframe_Link_passive_profile.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
  pn_Profile.Align:=alClient;

  pn_Main.Align:=alClient;

//  pn_ant_right.Align:=alClient;

  FrelProfileCommon:=TrelProfile.Create;
  FrelProfile1:=TrelProfile.Create;
  FrelProfile2:=TrelProfile.Create;


  CreateChildForm(Tframe_profile_Rel, Fframe_Profile_Rel, pn_Profile);

//                  SetLength(Fframe_Profile_Rel.Params.SiteGroups, 2);

  Fframe_profile_Rel.SetSiteGroupCount(2);

(*//  Fframe_Profile_Rel.Params.IsShowSites :=True;
  Fframe_Profile_Rel.Params.IsShowFrenelZoneTop:=True;
  Fframe_Profile_Rel.Params.IsShowFrenelZoneBottomLeft:=True;
  Fframe_Profile_Rel.Params.IsShowFrenelZoneBottomRight:=True;
  Fframe_Profile_Rel.Params.IsShowFrenelZoneBottom:=True;
*)

//  Fframe_Profile_Rel.OnProfileChanged:=DoOnProfileChanged;

//  Fframe_Profile_Rel.Params.IsShowReflectionPoints:=True;
///  Fframe_Profile_Rel.Params.IsPassiveRelay:=True;

  Fframe_Profile_Rel.ShowStatusBars (False);


  Fframe_Profile_Rel.OnProfileChanged:=DoOnProfileChanged;


  CreateChildForm(Tframe_Link_profile_summary, Fframe_Link_profile_summary, pn_summary);
  CreateChildForm(Tframe_Link_profile_ant, Fframe_Site1_Active, pn_Site1_active);
  CreateChildForm(Tframe_Link_profile_ant, Fframe_Site1_Passive, pn_Site1_Passive);
  CreateChildForm(Tframe_Link_profile_ant, Fframe_Site2_Active, pn_Site2_active);
  CreateChildForm(Tframe_Link_profile_ant, Fframe_Site2_Passive, pn_Site2_Passive);


  Fframe_Link_profile_summary.OnDataChanged:=DoOnDataChanged;
  Fframe_Link_profile_summary.OnCalcButton:=DoOnCalc;

  Fframe_Link_profile_summary.HideHeader;

//  Fframe_Link_profile_summary.IsCalcWithAdditionalRain:=cb_Additional_Rain_Accounting.Checked;

  Fframe_Site1_Active.OnChange:=DoOnChangedEdits;
  Fframe_Site1_Passive.OnChange:=DoOnChangedEdits;
  Fframe_Site2_Active.OnChange:=DoOnChangedEdits;
  Fframe_Site2_Passive.OnChange:=DoOnChangedEdits;


{  Fframe_Ant1.OnChange:=DoOnChangedEdits;
  Fframe_Ant2.OnChange:=DoOnChangedEdits;
}

{  Fframe_Ant1.OnChangeHeight:=DoOnChangeAntennaHeight;
  Fframe_Ant2.OnChangeHeight:=DoOnChangeAntennaHeight;
}

  Fframe_Site1_Active.OnChangeHeight:=DoOnChangeAntennaHeight;
  Fframe_Site1_Passive.OnChangeHeight:=DoOnChangeAntennaHeight;
  Fframe_Site2_Active.OnChangeHeight:=DoOnChangeAntennaHeight;
  Fframe_Site2_Passive.OnChangeHeight:=DoOnChangeAntennaHeight;


  AddComponentProp(pn_Summary, PROP_WIDTH);
  AddComponentProp(pn_Bottom,  PROP_HEIGHT);
  cxPropertiesStore.RestoreFrom;

  cxPageControl1.Align:=alClient;


  pn_Site1_passive.Align:=alClient;
  pn_Site2_active.Align:=alClient;


 // Assert(ILink<>nil);
  FrrlCalcParam := TrrlCalcParam.Create();
end;



procedure Tframe_Link_passive_profile.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FrrlCalcParam);
  
  FreeAndNil(FrelProfileCommon);
  FreeAndNil(FrelProfile1);
  FreeAndNil(FrelProfile2);

  inherited;
end;



//--------------------------------------------------------------------
procedure Tframe_Link_passive_profile.View(aLinkLineID: integer);
//--------------------------------------------------------------------
var
  sXML: string;
begin
  FLinkLineID:=aLinkLineID;

 // cb_Use_Passive_Elements.Checked:= true;

  if aLinkLineID=0 then
    Exit;

  if not dmLinkLine.GetPassiveInfo(FLinkLineID, FLinkLinePassiveRec) then
    raise Exception.Create('procedure Tframe_Link_passive_profile.View(aLinkLineID: integer);');
//    Exit;

  if not dmLink.GetInfo1 (FLinkLinePassiveRec.Link1.ID, FLink1ParamRec) then
    raise Exception.Create('procedure Tframe_Link_passive_profile.View(aLinkLineID: integer);'); //Exit;

  if not dmLink.GetInfo1 (FLinkLinePassiveRec.Link2.ID, FLink2ParamRec) then
    raise Exception.Create('procedure Tframe_Link_passive_profile.View(aLinkLineID: integer);'); //Exit;


  Fframe_Site1_Active.View1  (FLinkLinePassiveRec.Link1.ID, FLinkLinePassiveRec.Link1.active_linkend_id, OBJ_LINKEND);
  Fframe_Site1_Passive.View1 (FLinkLinePassiveRec.Link1.ID, FLinkLinePassiveRec.Link1.passive_linkend_id, OBJ_LINKEND);

  Fframe_Site1_Active.NextLinkEndID :=FLinkLinePassiveRec.Link1.passive_linkend_id;
  Fframe_Site1_Passive.NextLinkEndID:=FLinkLinePassiveRec.Link1.active_linkend_id;

  //---------------------------------------------------------

  Fframe_Site2_Active.View1  (FLinkLinePassiveRec.Link2.ID, FLinkLinePassiveRec.Link2.active_linkend_id, OBJ_LINKEND);
  Fframe_Site2_Passive.View1 (FLinkLinePassiveRec.Link2.ID, FLinkLinePassiveRec.Link2.passive_linkend_id, OBJ_LINKEND);

  Fframe_Site2_Active.NextLinkEndID :=FLinkLinePassiveRec.Link2.passive_linkend_id;
  Fframe_Site2_Passive.NextLinkEndID:=FLinkLinePassiveRec.Link2.active_linkend_id;

  //---------------------------------------------------------

  Fframe_Profile_Rel.Params.NFrenel:=FLink1ParamRec.NFrenel;
 // Fframe_Profile_Rel.Refraction    :=FLink1ParamRec.Refraction;

  Fframe_Profile_Rel.Params.Freq_MHz   :=FLink1ParamRec.Tx_Freq_MHz;

//  Fframe_Profile_Rel.ClutterModelID:=FLinkParamRec.ClutterMOdelID;
 // Fframe_Profile_Rel.BLVector      :=FLink1ParamRec.BLVector;
//  Fframe_Profile_Rel.Step          :=FLink1ParamRec.Step;


//  Fframe_Profile_Rel.Params.IsPassiveRelay:=True;

//  if cb_Reflection_points.Checked then
//  Fframe_Profile_Rel.Params.

//  FLink1ParamRec.

   Fframe_Profile_Rel.Params.SiteGroups[0].Distance_KM:=geo_Distance_km(FLink1ParamRec.BLVector);
   Fframe_Profile_Rel.Params.SiteGroups[0].ReflectionPointArr:=
        dmLink.GetReflectionPointArray(FLink1ParamRec.ID);

   Fframe_Profile_Rel.Params.SiteGroups[1].Distance_KM:=geo_Distance_km(FLink2ParamRec.BLVector);
   Fframe_Profile_Rel.Params.SiteGroups[1].ReflectionPointArr:=
        dmLink.GetReflectionPointArray(FLink2ParamRec.ID);

 ////  Fframe_Profile_Rel.Params.SiteGroups[1].DistanceOffset:=geo_Distance(FLink1ParamRec.BLVector)/1000;


//  else
//    Fframe_Profile_Rel.ReflectionPointArr:=nil;

//  Fframe_Profile_Rel.Params.IsShowReflectionPoints:=cb_Reflection_points.Checked;


  dmRel_Engine.AssignClutterModel (FLink1ParamRec.ClutterMOdel_ID, FrelProfile1.Clutters);
  dmRel_Engine.AssignClutterModel (FLink2ParamRec.ClutterMOdel_ID, FrelProfile2.Clutters);

//  dmRel_Engine.OpenAll;
{
  Reload_Profile(1);
  Reload_Profile(2);
}

  sXML:=FLink1ParamRec.Profile_XML;
  if (sXML='') or (not FrelProfile1.LoadFromXml(sXML)) then
    Reload_Profile(1);

  sXML:=FLink2ParamRec.Profile_XML;
  if (sXML='') or (not FrelProfile2.LoadFromXml(sXML)) then
    Reload_Profile(2);


  FrelProfile1.SetRefraction(FLink1ParamRec.Refraction);
  FrelProfile2.SetRefraction(FLink2ParamRec.Refraction);

  FrelProfile1.Validate;
  FrelProfile2.Validate;

  // ��������� ������� ---------------
                

  if FLinkLinePassiveRec.Link1.active_linkend_id =  FLink1ParamRec.LinkEnd1_ID then
    FrelProfile1.IsDirectionReversed := False
  else
    FrelProfile1.IsDirectionReversed := True;


  if FLinkLinePassiveRec.Link2.passive_linkend_id = FLink1ParamRec.LinkEnd1_ID then
    FrelProfile2.IsDirectionReversed := False
  else
    FrelProfile2.IsDirectionReversed := True;


  Fframe_Profile_Rel.RelProfile_Ref:=FrelProfileCommon;

  Fframe_Profile_Rel.RelProfile_Ref.Assign(FrelProfile1);
  Fframe_Profile_Rel.RelProfile_Ref.Join(FrelProfile2);
  //  geo_Distance(FLink1ParamRec.BLVector)/1000);

  Fframe_Profile_Rel.RelProfile_Ref.Validate;
            
 // Fframe_Profile_Rel.RelProfile2.Assign(FrelProfile2);


  ////////////////////////////////////////////////////////
 // Fframe_Profile_Rel.Params.SiteGroups[0].DistanceKM:=FrelProfile1.GeoDistance1();

  Fframe_Profile_Rel.Params.SiteGroups[0].Site1.Rel_H:=FrelProfile1.FirstItem().Rel_H;
  Fframe_Profile_Rel.Params.SiteGroups[0].Site2.Rel_H:=FrelProfile1.LastItem().Rel_H;

  ////////////////////////////////////////////////////////

//  Fframe_Profile_Rel.Params.SiteGroups[1].Site1.Distance:=0;
//  Fframe_Profile_Rel.Params.SiteGroups[1].Site2.DistanceKM:=FrelProfile2.GeoDistance1();

  Fframe_Profile_Rel.Params.SiteGroups[1].Site1.Rel_H:=FrelProfile2.FirstItem().Rel_H;
  Fframe_Profile_Rel.Params.SiteGroups[1].Site2.Rel_H:=FrelProfile2.LastItem().Rel_H;
  ////////////////////////////////////////////////////////

//  Fframe_Profile_Rel.ShowProfile;

//  Fframe_Profile_Rel.Profile_Rel_Graph.ShowGraph;

  ShowSites();

  Fframe_Link_profile_summary.View (FLinkLinePassiveRec.Link2.ID);


  Fframe_Profile_Rel.RelProfile_Ref.Validate;

end;



//--------------------------------------------------------------------
procedure Tframe_Link_passive_profile.ShowSites;
//--------------------------------------------------------------------
begin
  with Fframe_Profile_Rel.Params do
  begin
    SiteGroups[0].Site1.AntennaHeights:=Fframe_Site1_Active.GetAntennaHeightArr;
    SiteGroups[0].Site2.AntennaHeights:=Fframe_Site1_Passive.GetAntennaHeightArr;

    SiteGroups[1].Site1.AntennaHeights:=Fframe_Site2_Passive.GetAntennaHeightArr;
    SiteGroups[1].Site2.AntennaHeights:=Fframe_Site2_Active.GetAntennaHeightArr;
  end;

  Fframe_Profile_Rel.ShowProfile;
end;


//-------------------------------------------------------------------
procedure Tframe_Link_passive_profile._Actions(Sender: TObject);
//-------------------------------------------------------------------
var
  sXMLFileName,sImgFileName: string;
begin
  //-------------------------------------------------------------------
  if Sender=act_Calc then begin
  //-------------------------------------------------------------------
    Calc();
//    Calc_old();
  //  TdmLink_passive_calc.Init;

    //dmLink_passive_calc.Calc;

  end;


end;


procedure Tframe_Link_passive_profile.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
begin
 { act_Save.Enabled:=Fframe_Ant1.IsModified or
                    Fframe_Ant2.IsModified;
}
end;


procedure Tframe_Link_passive_profile.cb_Reflection_pointsClick(Sender: TObject);
begin
//  Fframe_Profile_Rel.Params.ReflectionPointArr:=dmLink_Reflection_Points.GetReflectionPointArray(FLinkLineID);
 // Fframe_Profile_Rel.Params.IsShowReflectionPoints:=cb_Reflection_points.Checked;
  //Fframe_Profile_Rel.ShowProfile;
end;


procedure Tframe_Link_passive_profile.cb_Additional_Rain_AccountingClick(Sender: TObject);
begin
//  Fframe_Link_profile_summary.IsCalcWithAdditionalRain:=cb_Additional_Rain_Accounting.Checked;

//  dmLink_Summary.IsCalcWithAdditionalRain:=cb_Additional_Rain_Accounting.Checked;
end;


//--------------------------------------------------------------------
procedure Tframe_Link_passive_profile.act_Fren_TopExecute(Sender: TObject);
//--------------------------------------------------------------------
begin


end;


//--------------------------------------------------------------------
procedure Tframe_Link_passive_profile.DoOnChangedEdits (Sender: TObject);
//--------------------------------------------------------------------
var
  rLinkParamRec: TdmLinkInfoRec;
begin

  if Sender=Fframe_Site1_Active  then Fframe_Site1_Passive.RefreshData;
  if Sender=Fframe_Site1_Passive then Fframe_Site1_Active.RefreshData;

  if Sender=Fframe_Site2_Active  then Fframe_Site2_Passive.RefreshData;
  if Sender=Fframe_Site2_Passive then Fframe_Site2_Active.RefreshData;


end;


procedure Tframe_Link_passive_profile.DoOnChangeAntennaHeight(Sender: TObject);
begin
  ShowSites();
end;

procedure Tframe_Link_passive_profile.DoOnDataChanged(Sender: TObject);
begin
  //View(FLinkLineID);
end;

procedure Tframe_Link_passive_profile.DoOnCalc(Sender: TObject);
begin
//  Calc();
end;

//-------------------------------------------------------------------
procedure Tframe_Link_passive_profile.Reload_Profile(aIndex: Integer);
//-------------------------------------------------------------------
var
  sXML: string;
begin
  case aIndex of    //
    1: begin
//        dmRel_Engine.OpenAll;


//

        dmRel_Engine.OpenByVector (FLink1ParamRec.BLVector);
//        dmRel_Engine.OpenByVector (FLinkLinePassiveRec.Link1.BLVector);

      //  dmRel_Engine.AssignClutterModel (FLinkParamRec.ClutterMOdelID, FrelProfile.Clutters);
       // b :=
//        if dmRel_Engine.BuildProfile1 (FrelProfile1, FLinkLinePassiveRec.Link1.BLVector, FLink1ParamRec.PROFILE_Step_M)
        if dmRel_Engine.BuildProfile1 (FrelProfile1, FLink1ParamRec.BLVector, FLink1ParamRec.PROFILE_Step_M)
        then begin
         // FrelProfile1.property1_id := FLink1ParamRec.

         //, FClutterModelID);
       //   FrelProfile1.SaveToXmlFile('d:\1.xml');

        end;


       end;

    2: begin
        dmRel_Engine.OpenByVector (FLink2ParamRec.BLVector);

     //   dmRel_Engine.OpenByVector (FLinkLinePassiveRec.Link2.BLVector);
      //  dmRel_Engine.AssignClutterModel (FLinkParamRec.ClutterMOdelID, FrelProfile.Clutters);
       // b :=
//        if dmRel_Engine.BuildProfile1 (FrelProfile2, FLinkLinePassiveRec.Link2.BLVector, FLink2ParamRec.PROFILE_Step_M)
        if dmRel_Engine.BuildProfile1 (FrelProfile2, FLink2ParamRec.BLVector, FLink2ParamRec.PROFILE_Step_M)
        then begin
         // FrelProfile1.property1_id := FLink1ParamRec.

        end;


        //, FClutterModelID);

        //  FrelProfile2.SaveToXmlFile('d:\2.xml');


       end;
  end;    // case

end;

//-------------------------------------------------------------------
procedure Tframe_Link_passive_profile.DoOnProfileChanged(Sender: TObject);
//-------------------------------------------------------------------
begin

end;


procedure Tframe_Link_passive_profile.FormResize(Sender: TObject);
begin

  pn_Site1_active.Width:=pn_Main.Width div 2;
  pn_Site2_passive.Width:=pn_Main.Width div 2;

end;


//--------------------------------------------------------------------
procedure Tframe_Link_passive_profile.Calc;
//--------------------------------------------------------------------
const
  TEMP_INI_FILE = 'link_passive_calc.ini';
//  EXE_LINK_PASSIVE_CALC = 'LINK_PASSIVE_CALC.exe';

var
 // k: Integer;
  oInifile: TIniFile;
  sFile: string;
begin
 // k:= IIF(aUsePassiveElements, 1, 0);

  sFile := g_ApplicationDataDir + TEMP_INI_FILE;


  oInifile:=TIniFile.Create (sFile);
  oInifile.WriteInteger ('main', 'ProjectID',  dmMain.ProjectID);
  oInifile.WriteInteger ('main', 'LinkLineID', FLinkLineID);
  oInifile.Free;

  RunApp (GetApplicationDir() + EXE_LINK_PASSIVE_CALC,  DoubleQuotedStr(sFile) );

  Fframe_Link_profile_summary.View (FLinkLinePassiveRec.Link2.ID);

end;

end.


