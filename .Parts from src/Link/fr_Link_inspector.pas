unit fr_Link_inspector;

interface
{$I ver.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, ActnList, rxPlacemnt, StdCtrls, ExtCtrls, ToolWin, ComCtrls,

  I_Inspector,

  fr_DBInspector_Container,


  dm_MapEngine_store,

  dm_User_Security,

//  {$DEFINE test}
  d_Link_Get_Network_Type_new,
  d_Link_Get_Network_Type,

  dm_Onega_DB_data,

//  fr_Object_inspector,

  dm_Main,


  u_const_msg,

  dm_Link,
  I_Link_climate,

  dm_Link_Climate,

  dm_Rel_Engine,

  u_Link_const,
  u_LinkLine_const,

 // dm_MapEngine,

//  u_Link_climate_file,
  d_Link_climate,

  u_GEO,
  u_func,
  u_db,
  u_dlg,

  u_cx,

  u_const_db,
  u_types
  ;


type
  TClutterModelChangeEvent = procedure(Sender: TObject; aID: Integer) of object;


  Tframe_Link_inspector = class(Tframe_DBInspector_Container)
    qry_Temp: TADOQuery;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    procedure FormCreate(Sender: TObject);
//    procedure FormCreate(Sender: TObject);
  //  procedure FormCreate(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
//    FIsClearCalcResults : Boolean;
    FOnClutterModelChange: TClutterModelChangeEvent;

  private
//    FUpdateOnMap : Boolean;
    FIsStatusUpdated: Boolean;
    FStatusIntValue: Integer;
    FStatusStrValue : String;

    procedure Select_Link_climate;

    procedure Select_GST_TYPE ();
    procedure Set_calc_method (aNewValue: integer);

//    procedure UpdateMinProfileStep;

  public
    procedure View (aID: integer); override;

// TODO: DoOnPost
    procedure DoOnPost(Sender: TObject); override;

    procedure DoOnButtonFieldClick (Sender: TObject; aFieldName: string; aNewValue: Variant); override;
    procedure DoOnFieldChanged     (Sender: TObject; aFieldName: string; aNewValue: Variant); override;

  public
    property OnClutterModelChange: TClutterModelChangeEvent read FOnClutterModelChange write FOnClutterModelChange;

    class function ExecDlg (aID: integer): boolean;

  end;

//==================================================================
// implementation
//==================================================================

implementation
{$R *.dfm}

const
  ALIAS_location = 'location';
  ALIAS_MISC = 'MISC';

procedure Tframe_Link_inspector.FormCreate(Sender: TObject);
begin
  inherited;
end;

  {
procedure Tframe_Link_inspector.FormCreate(Sender: TObject);
begin
  inherited;
end;
}

//--------------------------------------------------------------------
class function Tframe_Link_inspector.ExecDlg (aID: integer): boolean;
//--------------------------------------------------------------------
begin
  with Tframe_Link_inspector.Create(Application) do
  try
    View (aID);
   // pn_Buttons.Visible:=True;

    Result:=(ShowModal=mrOK);

  finally
    Free;
  end;

end;


// ---------------------------------------------------------------
procedure Tframe_Link_inspector.View(aID: integer);
// ---------------------------------------------------------------
var
  b: Boolean;
  iCalc_method: Integer;
  iGeoRegion1_ID: Integer;
  iGeoRegion2_ID: Integer;
begin
//  PrepareViewForObject (OBJ_LINK);

  PrepareViewForObject (OBJ_LINK);

  {
  case dmLink.GetType(aID) of
    ltLink:     PrepareViewForObject (OBJ_LINK);
    ltPmpLink:  PrepareViewForObject (OBJ_LINK, OBJ_PMP_LINK);
  end;
  }

  inherited View(aID);

  iCalc_method := GetIntFieldValue(FLD_calc_method);

//  if Eq(aFieldName, FLD_calc_method) then
 // begin
  Set_calc_method(iCalc_method);

  FIsStatusUpdated := False;


//  ExpandRowByAlias(ALIAS_location, False);
  ExpandRowByAlias(ALIAS_MISC, False);

//
//
//  iGeoRegion1_ID := GetIntFieldValue(FLD_GeoRegion1_ID);
//  iGeoRegion2_ID := GetIntFieldValue(FLD_GeoRegion2_ID);
//
//  b:=dmUser_Security.GeoRegion_is_ReadOnly(iGeoRegion1_ID) and
//     dmUser_Security.GeoRegion_is_ReadOnly(iGeoRegion2_ID);
//
//  SetReadOnly(b, 'GeoRegion:'+IntToStr(iGeoRegion1_ID));
//

//  FIsClearCalcResults := False;

end;

// ---------------------------------------------------------------
procedure Tframe_Link_inspector.Set_calc_method (aNewValue: integer);
// ---------------------------------------------------------------
const
  DEF_RRV_E_band_ITU ='RRV_E_band_ITU';
//  DEF_RRV_E_band_GOST='RRV_E_band_GOST';
//  DEF_RRV_E_band_NIIR='RRV_E_band_NIIR';
  DEF_RRV_UKV        ='RRV_UKV';


var
  bl: TBLPoint;
  blVector: TblVector;
  bUKV: Boolean;

//  s: string;
begin


 ////// db_View(GetDataSet());


  case AsInteger(aNewValue) of
    // ��������� ��� (���� � 53363)
    1: begin

//                SetFieldValue (FLD_GOST_link_center_lat, bl.B); //9
//            SetFieldValue (FLD_GOST_link_center_lon, bl.L); //9


        //if  then
         bl.B := GetFloatFieldValue (FLD_GOST_link_center_lat);
         bl.L := GetFloatFieldValue (FLD_GOST_link_center_lon);

         if (bl.B=0) or (bl.l=0) then
         begin
           blVector := db_ExtractBLVector(GetDataset());
           bl := geo_GetVectorCenter (blVector);

           SetFieldValue (FLD_GOST_link_center_lat, bl.B);
           SetFieldValue (FLD_GOST_link_center_lon, bl.L);

         end;

          ;

       //  FieldValue(FLD_calc_method);

//           SetFieldValue (FLD_NIIR1998_NAME, '���������');
       end;

  end;

//  {$IFDEF test}
 // if Eq(aFieldName, FLD_calc_method) then
 // begin
(*    case AsInteger(aNewValue) of
      0: s:=FLD_rrv_name; // = 'rrv_name'; // ������� ��������������� ��������� (���)
      1: s:=FLD_GOST; // = 'RRI_GOST'; // ��������� ��� (���� � 53363)
      2: s:=FLD_NIIR1998; // = 'RRI_NIIR'; // ��������� ��� (���� 1998)

    end;

 bUKV:= k=DEF_Calc_Method_UKV;

  row_SESR_norm.Visible     :=not bUKV;
  row_sesr.Visible          :=not bUKV;
  row__BER_required.Visible :=not bUKV;


*)


    SetRowVisibleByFieldName(FLD_rrv_name,      aNewValue=0 );
    SetRowVisibleByFieldName(FLD_GOST_NAME,     aNewValue=1);
    SetRowVisibleByFieldName(FLD_NIIR1998_NAME, aNewValue=2);

  //  SetRowVisibleByFieldName(FLD_NIIR1998_NAME, aNewValue=2);

    SetRowVisibleByAlias(DEF_RRV_E_band_ITU,  aNewValue=3);
//    SetRowVisibleByAlias(DEF_RRV_E_band_GOST, aNewValue=4);
//    SetRowVisibleByAlias(DEF_RRV_E_band_NIIR, aNewValue=5);
//    SetRowVisibleByAlias(DEF_RRV_UKV,         aNewValue=6);
   SetRowVisibleByAlias(DEF_RRV_UKV,         aNewValue=4);


   bUKV:= aNewValue=DEF_Calc_Method_UKV;

   SetRowVisibleByFieldName(FLD_SESR_norm,     not bUKV );
   SetRowVisibleByFieldName(FLD_SESR,          not bUKV );
   SetRowVisibleByFieldName(FLD_ESR_required,  not bUKV );
   SetRowVisibleByFieldName(FLD_BBER_required, not bUKV );


   //"�������� ������ ������� V����(g)=V���.�, ��"
   SetRowVisibleByFieldName(FLD_precision_of_condition_V_diffration_is_equal_to_V_min_dB, not bUKV );

   //2.3. ������ ������� "���������� ����� ���, ������������� �������������"  --KNG_dop
   SetRowVisibleByFieldName(FLD_KNG_dop, not bUKV );

   //2.4. ������ ������� "���������� ����������� �% ���.�������� < p_dop [%]"
   SetRowVisibleByFieldName(FLD_space_limit_probability, not bUKV );


   //��������� ����� �� ��������� [dB] (0-������� ������� �� ��������� ����������)"
   SetRowVisibleByFieldName(FLD_fade_margin_required_dB, not bUKV );

   //    fade_margin_dB : Double; // ����� �� ��������� [dB]




{
������ ������� "�������� ������ ������� V����(g)=V���.�, ��"
2.3. ������ ������� "���������� ����� ���, ������������� �������������"  --KNG_dop
2.4. ������ ������� "���������� ����������� �% ���.�������� < p_dop [%]"

2.5. �������� � ������ "�������������� ����������" ����� ��������:
"��������� ����� �� ��������� [dB] (0-������� ������� �� ��������� ����������)"

}


//    SetRowVisibleByAlias(DEF_RRV_UKV,         aNewValue=4);


  //  SetRowVisibleByFieldName(s, True);

 // end;
 //   ShowMessage('FLD_calc_method');
//  {$ENDIF}

end;



//-------------------------------------------------------------------
procedure Tframe_Link_inspector.DoOnFieldChanged (Sender: TObject; aFieldName: string;
                                                aNewValue: Variant);
//-------------------------------------------------------------------
var i, ind: integer;
  dLengthKM: Double;

  rNetwork_Type_rec: Tdlg_Link_Get_Network_Type_rec;
  s: string;

begin
  Assert(ID>0);


  {$IFDEF test}
  if Eq(aFieldName, FLD_calc_method) then
  begin
    Set_calc_method(AsInteger(aNewValue));

(*    case AsInteger(aNewValue) of
      0: s:=FLD_rrv_name; // = 'rrv_name'; // ������� ��������������� ��������� (���)
      1: s:=FLD_RRI_GOST; // = 'RRI_GOST'; // ��������� ��� (���� � 53363)
      2: s:=FLD_RRI_NIIR; // = 'RRI_NIIR'; // ��������� ��� (���� 1998)

    end;

    SetRowVisibleByFieldName(FLD_rrv_name, False);
    SetRowVisibleByFieldName(FLD_RRI_GOST, False);
    SetRowVisibleByFieldName(FLD_RRI_NIIR, False);

    SetRowVisibleByFieldName(s, True);
*)
  end;
 //   ShowMessage('FLD_calc_method');
  {$ENDIF}


  //-------------------------------------------------------------------
  if Eq(aFieldName, FLD_CLUTTER_MODEL_ID) then begin
  //-------------------------------------------------------------------
    if Assigned(FOnClutterModelChange) then
      FOnClutterModelChange(Self, AsInteger(aNewValue));
  end;


  //-------------------------------------------------------------------
  for i:=0 to High(LINK_CONDITIONS_FIELDS_ITU_ARR) do
    if Eq(LINK_CONDITIONS_FIELDS_ITU_ARR[i], aFieldName) then
    begin
      SetFieldValue (FLD_RRV_NAME, '���������');
      Exit;
    end;

  //-------------------------------------------------------------------
  for i:=0 to High(LINK_CONDITIONS_FIELDS_GOST_ARR) do
    if Eq(LINK_CONDITIONS_FIELDS_GOST_ARR[i], aFieldName) then
    begin
      SetFieldValue (FLD_GOST_NAME, '���������');
      Exit;
    end;

  //-------------------------------------------------------------------
  for i:=0 to High(LINK_CONDITIONS_FIELDS_NIIR1998_ARR) do
    if Eq(LINK_CONDITIONS_FIELDS_NIIR1998_ARR[i], aFieldName) then
    begin
      SetFieldValue (FLD_NIIR1998_NAME, '���������');
      Exit;
    end;


   // -------------------------------------------------------------------
    if Eq(aFieldName, FLD_GST_TYPE) then
   // -------------------------------------------------------------------
    begin
      ind:=aNewValue;

      if ind = 0 then
        Exit;

      dLengthKM:=GetFloatFieldValue (FLD_Length)/1000;


      if Tdlg_Link_Get_Network_Type.ExecDlg(ID, dLengthKM, ind, rNetwork_Type_rec) then
      begin

        SetFieldValue(FLD_GST_TYPE,      rNetwork_Type_rec.Index );

        SetFieldValue(FLD_GST_LENGTH,    rNetwork_Type_rec.Length_km );
        SetFieldValue(FLD_GST_SESR,      rNetwork_Type_rec.SESR );
        SetFieldValue(FLD_GST_KNG,       rNetwork_Type_rec.KNG );

        SetFieldValue(FLD_ESR_NORM,      rNetwork_Type_rec.Esr_Norm);
        SetFieldValue(FLD_ESR_REQUIRED,  rNetwork_Type_rec.Esr_Req);

        SetFieldValue(FLD_BBER_NORM,     rNetwork_Type_rec.Bber_Norm);
        SetFieldValue(FLD_BBER_REQUIRED, rNetwork_Type_rec.Bber_Req);
      end;
   end;

  // ---------------------------------------------------------------
  if Eq(aFieldName, FLD_GST_LENGTH) or
     Eq(aFieldName, FLD_GST_SESR) or
     Eq(aFieldName, FLD_GST_KNG) or
     Eq(aFieldName, FLD_ESR_NORM) or
     Eq(aFieldName, FLD_BBER_NORM)
  then
    SetFieldValue (FLD_GST_TYPE, 0);
                 

  if Eq(aFieldName, FLD_STATUS_ID) then
  begin
    FIsStatusUpdated := True;
    FStatusIntValue := aNewValue;
    FStatusStrValue := GetStrFieldValue (FLD_STATUS_ID + '_str');
  end;



(*  //clear calc results
  if Eq(aFieldName, FLD_CALC_DIRECTION) then
    FIsClearCalcResults := True;
*)
end;

//-------------------------------------------------------------------
procedure Tframe_Link_inspector.DoOnButtonFieldClick (
          Sender: TObject; aFieldName: string; aNewValue: Variant);
//-------------------------------------------------------------------


var
  rec: TLinkClimateDataRec;
//  blPoint: TBLPoint;
  dLengthKM: Double;
//  iCalc_Method_ID: Integer;
//  iLinkTypeID: integer;
  ind: integer;

  rNetwork_Type_rec: Tdlg_Link_Get_Network_Type_rec;

begin
  inherited;


  {$IFDEF test}
  if Eq(aFieldName, FLD_calc_method) then
    ShowMessage('FLD_calc_method');
  {$ENDIF}


  //-------------------------------------------------------------------
  if Eq(aFieldName, FLD_GST_TYPE) then
  //-------------------------------------------------------------------
  begin
    ind:=aNewValue;

    if ind = 0 then
      Exit;

    dLengthKM:=GetFloatFieldValue (FLD_Length)/1000;


    if Tdlg_Link_Get_Network_Type.ExecDlg(ID, dLengthKM, ind, rNetwork_Type_rec) then
    begin
      SetFieldValue(FLD_GST_TYPE,      rNetwork_Type_rec.Index );

      SetFieldValue(FLD_GST_LENGTH,    rNetwork_Type_rec.Length_km );
      SetFieldValue(FLD_GST_SESR,      rNetwork_Type_rec.SESR );
      SetFieldValue(FLD_GST_KNG,       rNetwork_Type_rec.KNG );

      SetFieldValue(FLD_ESR_NORM,      rNetwork_Type_rec.Esr_Norm);
      SetFieldValue(FLD_BBER_NORM,     rNetwork_Type_rec.Bber_Norm);
      SetFieldValue(FLD_ESR_REQUIRED,  rNetwork_Type_rec.Esr_Req);
      SetFieldValue(FLD_BBER_REQUIRED, rNetwork_Type_rec.Bber_Req);

    end;
  end else

  //-------------------------------------------------------------------
  if    Eq(aFieldName, FLD_RRV_NAME)
     or Eq(aFieldName, FLD_GOST_NAME)
     or Eq(aFieldName, FLD_NIIR1998_NAME)
  then
  //-------------------------------------------------------------------
  begin
    Select_Link_climate();
  end;

end;

// ---------------------------------------------------------------
procedure Tframe_Link_inspector.DoOnPost(Sender: TObject);
// ---------------------------------------------------------------
(*var
  oMsg: TMessageInfo;*)
begin
  inherited;

//  oMsg:=TMessageInfo.Create;
//  oMsg.IntValue := FStatusIntValue;
////  oMsg.StrValue := FStatusStrValue;
  //oMsg.StrValue := ObjectName;

//  PostMessage (Application.Handle, WE_MAP_UPDATE_LINK_STATUS, ID, Integer(oMsg));
//  PostMessage (Application.Handle, WE_MAP_UPDATE_DATA, ID, Integer(oMsg));

  dmMapEngine_store.Update_Data(OBJ_LINK);



 // ShowMessage('procedure Tframe_Link_inspector.DoOnPost(Sender: TObject);');
end;

// ---------------------------------------------------------------
procedure Tframe_Link_inspector.Select_Link_climate;
// ---------------------------------------------------------------
var
  aSource_from_DB: Boolean;
  b: Boolean;
  rec: TLinkClimateDataRec;
  blCenterPoint: TBLPoint;
  blVector: TBLVector;
  dLengthKM: Double;
  iCalc_Method_ID: Integer;
  iLinkTypeID: integer;
  ind: integer;

  rNetwork_Type_rec: Tdlg_Link_Get_Network_Type_rec;

//  vIDBInspector: IDBInspector;

begin
    blVector := db_ExtractBLVector(GetDataset());

//    geo_

    blCenterPoint := geo_GetVectorCenter (blVector);


//    blPoint := db_ExtractBLPoint(GetDataset());

 //   blPoint := db_ExtractBLPoint(GetDataset());



   //////// blPoint:=dmLink.GetBLCenter (ID);

    iLinkTypeID:=GetIntFieldValue(FLD_LinkType_ID);

    iCalc_Method_ID:=GetIntFieldValue (FLD_calc_method);


(*       bl:=geo_GetVectorCenter (Link.BLVector);

      aObj.RRV.GOST_53363.Link_Center_lat:=bl.B;
      aObj.RRV.GOST_53363.Link_Center_lon:=bl.L;
*)

(*
   b:=GetInterface(IDBInspector, vIDBInspector);
   Assert(b);
*)

//    if Tdlg_Link_climate.ExecDlg (blCenterPoint, rec, iLinkTypeID, iCalc_Method_ID, vIDBInspector) then
    if Tdlg_Link_climate.ExecDlg (blCenterPoint, rec, iLinkTypeID, iCalc_Method_ID, aSource_from_DB) then
    begin
      if rec.air_temperature=0     then rec.air_temperature:=15;
      if rec.atmosphere_pressure=0 then rec.atmosphere_pressure:=1013;
      if rec.GOST.rain_intensity=0 then rec.GOST.rain_intensity:=1;


   //   SetFieldValue (FLD_RRV_NAME,      rec.Comment);
      SetFieldValue (FLD_LinkType_ID, rec.LinkType_ID);

      //common

    //  SetFieldValue(FLD_gradient_diel,      rec.ITU.gradient_diel);//1
    //  SetFieldValue(FLD_gradient_deviation, rec.ITU.gradient_deviation); //2


     SetFieldValue (FLD_RRV_NAME,      rec.Comment);
     SetFieldValue (FLD_GOST_NAME,      rec.Comment);
     SetFieldValue (FLD_NIIR1998_NAME,   rec.Comment);


      case iCalc_Method_ID of
       // ---------------------------------
        0: begin
       // ---------------------------------
            SetFieldValue (FLD_RRV_NAME,      rec.Comment);


            SetFieldValue(FLD_gradient_diel,      rec.ITU.gradient_diel);//1
            SetFieldValue(FLD_gradient_deviation, rec.ITU.gradient_deviation); //2

            //!!!!!
            SetFieldValue (FLD_air_temperature,      rec.air_temperature); //5


            SetFieldValue (FLD_TERRAIN_TYPE,    rec.ITU.terrain_type);

            SetFieldValue (FLD_underlying_terrain_type,  rec.ITU.underlying_terrain_type);

            //Q_factor_rec
            SetFieldValue (FLD_Q_FACTOR,        rec.ITU.Q_factor);

      //
            //Climate_rec.
            SetFieldValue (FLD_Factor_C,        rec.ITU.Factor_C);

            //ClimateFactor_rec
            SetFieldValue (FLD_Climate_factor,  rec.ITU.Climate_factor);
            SetFieldValue (FLD_Factor_B,        rec.ITU.Factor_B);
            SetFieldValue (FLD_Factor_D,        rec.ITU.Factor_D);

            //RadioKlimRegion_rec.

            SetFieldValue (FLD_rain_intensity,  rec.ITU.rain_intensity1);
            SetFieldValue (FLD_steam_wet,       rec.ITU.steam_wet);

           end;

       // ---------------------------------
       1: begin
       // ---------------------------------
         if aSource_from_DB then
         begin
             SetFieldValue(FLD_GOST_gradient_diel,      Rec.GOST_gradient_diel);
             SetFieldValue(FLD_GOST_gradient_deviation, Rec.GOST_gradient_deviation);
             SetFieldValue(FLD_GOST_terrain_type,       Rec.GOST_terrain_type);
             SetFieldValue(FLD_GOST_underlying_terrain_type,      Rec.GOST_underlying_terrain_type);
             SetFieldValue(FLD_GOST_steam_wet,           Rec.GOST_steam_wet);
             SetFieldValue(FLD_GOST_air_temperature,     Rec.GOST_air_temperature);
             SetFieldValue(FLD_GOST_atmosphere_pressure, Rec.GOST_atmosphere_pressure);
             SetFieldValue(FLD_GOST_climate_factor,      Rec.GOST_climate_factor);
             SetFieldValue(FLD_GOST_rain_intensity,      Rec.GOST_rain_intensity);

             exit;

         end;

       {
             SetFieldValue(FLD_GOST_gradient_diel,      aRec.GOST_gradient_diel;
             SetFieldValue(FLD_GOST_gradient_deviation, aRec.GOST_gradient_deviation;
             SetFieldValue(FLD_GOST_terrain_type,       aRec.GOST_terrain_type;
             SetFieldValue(FLD_GOST_underlying_terrain_type,      aRec.GOST_underlying_terrain_type;
             SetFieldValue(FLD_GOST_steam_wet,           aRec.GOST_steam_wet;
             SetFieldValue(FLD_GOST_air_temperature,     aRec.GOST_air_temperature;
             SetFieldValue(FLD_GOST_atmosphere_pressure, aRec.GOST_atmosphere_pressure;
             SetFieldValue(FLD_GOST_climate_factor,      aRec.GOST_climate_factor;
             SetFieldValue(FLD_GOST_rain_intensity,      aRec.GOST_rain_intensity;

        }



           SetFieldValue (FLD_GOST_NAME,      rec.Comment);


           SetFieldValue(FLD_GOST_gradient_diel,      rec.ITU.gradient_diel);//1
           SetFieldValue(FLD_GOST_gradient_deviation, rec.ITU.gradient_deviation); //2



(*            if rec.air_temperature=0 then rec.air_temperature:=15;
            if rec.atmosphere_pressure=0 then rec.atmosphere_pressure:=1013;
            if rec.GOST.rain_intensity=0 then rec.GOST.rain_intensity:=1;
*)


            SetFieldValue (FLD_GOST_TERRAIN_TYPE,             rec.ITU.terrain_type); //3
            SetFieldValue (FLD_GOST_underlying_terrain_type,  rec.ITU.underlying_terrain_type);//4
            SetFieldValue (FLD_GOST_steam_wet,                rec.ITU.steam_wet); //5

          //  if rec.air_temperature>0 then
            SetFieldValue (FLD_GOST_air_temperature, rec.air_temperature); //6

           // if rec.atmosphere_pressure>0 then
            SetFieldValue (FLD_GOST_atmosphere_pressure, rec.atmosphere_pressure); //7

            SetFieldValue (FLD_GOST_Climate_factor,  rec.ITU.Climate_factor); //8

//           if rec.GOST.rain_intensity>0 then
            SetFieldValue (FLD_GOST_rain_intensity,  rec.GOST.rain_intensity); //9


            SetFieldValue (FLD_GOST_link_center_lat, blCenterPoint.B); //9
            SetFieldValue (FLD_GOST_link_center_lon, blCenterPoint.L); //9


         //   blCenterPoint


//zz      bl:=geo_GetVectorCenter (Link.BLVector);

//      aObj.RRV.GOST.Link_Center_lat:=bl.B;
//      aObj.RRV.GOST.Link_Center_lon:=bl.L;


      //      bl:=geo_GetVectorCenter (Link.BLVector);

//      aObj.RRV.GOST.Link_Center_lat:=bl.B;
//      aObj.RRV.GOST.Link_Center_lon:=bl.L;



          end;

       // -------------------------
       // NIIR1998
       // -------------------------
       2: begin
           SetFieldValue (FLD_NIIR1998_NAME,      rec.Comment);

           SetFieldValue(FLD_NIIR1998_gradient_diel,      rec.ITU.gradient_diel);//1
           SetFieldValue(FLD_NIIR1998_gradient_deviation, rec.ITU.gradient_deviation); //2



      // ..     if rec.air_temperature=0 then rec.air_temperature:=15;
      //      if rec.atmosphere_pressure=0 then rec.atmosphere_pressure:=1013;

            SetFieldValue (FLD_NIIR1998_TERRAIN_TYPE,         rec.ITU.terrain_type); //3
            SetFieldValue (FLD_NIIR1998_steam_wet,            rec.ITU.steam_wet); //4
            SetFieldValue (FLD_NIIR1998_air_temperature,      rec.air_temperature); //5
            SetFieldValue (FLD_NIIR1998_atmosphere_pressure,  rec.atmosphere_pressure); //6

            SetFieldValue (FLD_NIIR1998_rain_region_number,   rec.NiiR_1998.rain_8_11_intensity_region_number); //7
            SetFieldValue (FLD_NIIR1998_Qd_region_number,     rec.NiiR_1998.rain_8_15_Qd_region_number);//8

            if rec.NiiR_1998.Water_area_percent>0 then
              SetFieldValue (FLD_NIIR1998_Water_area_percent,  rec.NiiR_1998.Water_area_percent);//9

         //   SetFieldValue (FLD_NIIR1998_gradient,            rec.GOST.radioklimat.gradient);
         //   SetFieldValue (FLD_NIIR1998_numidity,            rec.GOST.humidity.absolute_humidity);
          end;

      end;
    end;

end;


procedure Tframe_Link_inspector.Select_GST_TYPE ();
var
  dLengthKM: Double;
  rNetwork_Type_rec: Tdlg_Link_Get_Network_Type_rec;

begin
   // -------------------------------------------------------------------
   // if Eq(aFieldName, FLD_GST_TYPE) then
   // -------------------------------------------------------------------
  //  begin

  (*    ind:=aNewValue;

      if ind = 0 then
        Exit;

      dLengthKM:=GetFloatFieldValue (FLD_Length)/1000;


      if Tdlg_Link_Get_Network_Type.ExecDlg(ID, dLengthKM, ind, rNetwork_Type_rec) then
      begin

        SetFieldValue(FLD_GST_TYPE,      rNetwork_Type_rec.Index );

        SetFieldValue(FLD_GST_LENGTH,    rNetwork_Type_rec.Length_km );
        SetFieldValue(FLD_GST_SESR,      rNetwork_Type_rec.SESR );
        SetFieldValue(FLD_GST_KNG,       rNetwork_Type_rec.KNG );

        SetFieldValue(FLD_ESR_NORM,      rNetwork_Type_rec.Esr_Norm);
        SetFieldValue(FLD_BBER_NORM,     rNetwork_Type_rec.Bber_Norm);
        SetFieldValue(FLD_ESR_REQUIRED,  rNetwork_Type_rec.Esr_Req);
        SetFieldValue(FLD_BBER_REQUIRED, rNetwork_Type_rec.Bber_Req);
      end;

*)  // end;
end;


procedure Tframe_Link_inspector.ToolButton1Click(Sender: TObject);
var
  r: Tdlg_Link_Get_Network_Type_rec_;

begin
  r.Length_km := GetFloatFieldValue(FLD_GST_length);
  r.SESR      := GetFloatFieldValue(FLD_GST_SESR);
  r.KNG       := GetFloatFieldValue(FLD_GST_KNG);

  if Tdlg_Link_Get_Network_Type_new.ExecDlg(r) then
  begin

  //  SetFieldValue(FLD_GST_TYPE,      r.Index );

    SetFieldValue(FLD_GST_LENGTH,    r.Length_km );
    SetFieldValue(FLD_GST_SESR,      r.SESR );
    SetFieldValue(FLD_GST_KNG,       r.KNG );


  end;
  ;

(*
  Tdlg_Link_Get_Network_Type_rec_ = record
  // -------------------------
    Index : byte;
    Length_km : Double;

    SESR      : double;
    Kng       : double;
*)


 //


end;



end.

   {

//-------------------------------------------------------------------
procedure Tframe_Link_inspector.UpdateMinProfileStep;
//-------------------------------------------------------------------
var
  iStep: integer;
  blVector: TBLVector;
begin
{
  if GetIntFieldValue(FLD_profile_step) = 0 then
  begin
    blVector := db_ExtractBLVector(GetDataset());


  //  if dmLink.GetBLVector(ID, blVector) then
  //  begin
//    iStep:=dmRelMatrixFile.GetMinStepForVector (blVector);
//      iStep:=dmRel_Engine.GetMinStepForVector (blVector);

  //    SetFieldValue(FLD_profile_step, iStep);
  //  end;
  end;
 }


  //ShowMessage('');
end;

