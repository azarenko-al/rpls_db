inherited dlg_Link_Climate: Tdlg_Link_Climate
  Left = 606
  Top = 335
  Width = 551
  Height = 517
  BorderWidth = 5
  Caption = #1042#1099#1073#1086#1088' '#1080#1089#1090#1086#1095#1085#1080#1082#1072' '#1076#1072#1085#1085#1099#1093
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 444
    Width = 533
    inherited Bevel1: TBevel
      Width = 533
    end
    inherited Panel3: TPanel
      Left = 352
      Width = 181
      inherited btn_Cancel: TButton
        Left = 90
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 533
    inherited Bevel2: TBevel
      Width = 533
    end
    inherited pn_Header: TPanel
      Width = 533
      Visible = False
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 60
    Width = 533
    Height = 126
    Align = alTop
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 2
    DesignSize = (
      533
      126)
    object rb_BD: TRadioButton
      Left = 5
      Top = 28
      Width = 305
      Height = 17
      Caption = #1041#1044' ('#1084#1086#1076#1077#1083#1100' '#1088#1072#1089#1087#1088#1086#1089#1090#1088#1072#1085#1077#1085#1080#1103' '#1080#1079' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1072')'
      TabOrder = 0
    end
    object rb_Map: TRadioButton
      Left = 5
      Top = 8
      Width = 57
      Height = 17
      Caption = #1050#1072#1088#1090#1072
      Checked = True
      TabOrder = 1
      TabStop = True
    end
    object ComboEdit1: TComboEdit
      Left = 16
      Top = 49
      Width = 505
      Height = 21
      DirectInput = False
      GlyphKind = gkEllipsis
      ButtonWidth = 16
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 2
      OnButtonClick = ComboEdit1ButtonClick
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 120
    object act_RowButtonClick: TAction
      Category = 'Main'
      Caption = 'act_RowButtonClick'
    end
  end
  inherited FormStorage1: TFormStorage
    Left = 148
  end
end
