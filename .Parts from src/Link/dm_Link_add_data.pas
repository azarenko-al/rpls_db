unit dm_Link_add_data;

interface

uses
  SysUtils, Classes, DB, ADODB,  Forms,

  dm_Main,

  u_db,

  u_types,

  u_const_db,
  u_const,

  dm_Onega_DB_data
  ;

type
  TdmLink_add_data = class(TDataModule)
    qry_Prop: TADOQuery;
    qry_LinkEnds: TADOQuery;
    ds_LinkEnds: TDataSource;
    procedure DataModuleCreate(Sender: TObject);
  private
  

  public
    class procedure Init;

    procedure Open;
    procedure Update_PROPERTY(aItems: TStrings);

  end;

var
  dmLink_add_data: TdmLink_add_data;

implementation


{$R *.dfm}


class procedure TdmLink_add_data.Init;
begin
  if not Assigned(dmLink_add_data) then
    Application.CreateForm(TdmLink_add_data, dmLink_add_data);
end;


procedure TdmLink_add_data.DataModuleCreate(Sender: TObject);
begin

end;

//------------------------------------------------------------------------------
procedure TdmLink_add_data.Open;
//------------------------------------------------------------------------------
const
//  view_LinkEnd_for_Link_add = 'view_LinkEnd_for_Link_add';

  SQL_SELECT =
    'SELECT * FROM '+ tbl_PROPERTY + ' WHERE project_id=%d';

begin
  dmOnega_DB_data.OpenQuery(qry_Prop, Format(SQL_SELECT, [dmMain.ProjectID]));

end;


//------------------------------------------------------------------------------
procedure TdmLink_add_data.Update_PROPERTY(aItems: TStrings);
//------------------------------------------------------------------------------

var
  iID: Integer;
  sName: string;
begin
  aItems.BeginUpdate;

  qry_Prop.First;


  with qry_Prop do
    while not EOF do
  begin
    iID  := FieldByName(FLD_ID).AsInteger;
    sName:= FieldByName(FLD_NAME).AsString;

    aItems.AddObject(sName, Pointer(iID));

    Next;
  end;

  aItems.EndUpdate;

end;




end.


     {



        // ---------------------------------------------------------------
procedure Tframe_Link_add.Update_PROPERTY;
// ---------------------------------------------------------------
const
//  view_LinkEnd_for_Link_add = 'view_LinkEnd_for_Link_add';

  SQL_SELECT =
    'SELECT * FROM '+ tbl_PROPERTY + ' WHERE project_id=%d';

var
  iID: Integer;
  sName: string;
begin
  db_OpenQuery (qry_Prop, Format(SQL_SELECT, [dmMain.ProjectID]));


 // comboBox1.Beg

  LockWindowUpdate(comboBox1.Handle);

  with qry_Prop do
    while not EOF do
  begin
    iID  := FieldByName(FLD_ID).AsInteger;
    sName:= FieldByName(FLD_NAME).AsString;

    FComboBoxAutoComplete.Items.AddObject(sName, Pointer(iID));

    Next;
  end;


  LockWindowUpdate(0);


  FComboBoxAutoComplete.StoredItems.Assign(FComboBoxAutoComplete.Items);


end;
