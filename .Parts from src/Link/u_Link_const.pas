unit u_Link_const;

interface

const
  CAPTION_Rx_Level_dBm = '������� ������� [dBm]';
  CAPTION_Is_Working   = '�����������';


  FLD_DISTANCE_to_center_km='DISTANCE_to_center_km';
  FLD_ABS_PROSVET_m        ='ABS_PROSVET_m';
  FLD_LENGTH_KM            ='LENGTH_KM';
  FLD_RADIUS_km            ='RADIUS_km';

//  FLD_Signal_Depression_dB ='Signal_Depression_dB';

//  FLD_Signal_Depression ='Signal_Depression';


   //"�������� ������ ������� V����(g)=V���.�, ��"
   FLD_precision_of_condition_V_diffration_is_equal_to_V_min_dB = 'precision_of_condition_V_diffration_is_equal_to_V_min_dB';


  //��������� ����� �� ��������� [dB] (0-������� ������� �� ��������� ����������)"   
  FLD_fade_margin_required_dB = 'fade_margin_required_dB';


  FLD_GOST_Link_Center_lat = 'GOST_Link_Center_lat';
  FLD_GOST_Link_Center_lon = 'GOST_Link_Center_lon';

  FLD_GOST_rain_intensity = 'GOST_rain_intensity';

  FLD_NIIR1998_Water_area_percent = 'NIIR1998_Water_area_percent';

  FLD_LINKID = 'LINKID';

  FLD_FRENEL_MAX_M ='FRENEL_MAX_M';
//  FLD_FRENEL_MAX_IN_METERS ='FRENEL_MAX_IN_METERS';

 // FLD_IsEqualizer ='IsEqualiser';
  FLD_factor_Q    = 'factor_Q';

  FLD_LINKLINE_NAME = 'LINKLINE_NAME';

  FLD_SPACE_SPREAD_M    = 'SPACE_SPREAD_M';
//  FLD_SPACE_SPREAD          = 'SPACE_SPREAD';


  FLD_SESR_required = 'SESR_REQUIRED';
  FLD_KNG_required  = 'KNG_REQUIRED';


  FLD_KRATNOST_BY_FREQ  = 'KRATNOSTBYFREQ';
  FLD_KRATNOST_BY_SPACE = 'KRATNOSTBYSPACE';

  FLD_Protection_Type   = 'Protection_Type';

  //----------------------------------------------
  // link
  //----------------------------------------------
  FLD_CALC_METHOD   = 'calc_method';
  FLD_SESR          = 'SESR';
  FLD_KNG           = 'KNG';
  FLD_KNG_YEAR      = 'KNG_YEAR';

(*      SArr(FLD_sesr_calc,     'SESR ���� [%]'),
      SArr(FLD_sesr_required, 'SESR ���� [%]'),
      SArr(FLD_KNG_calc,      '��� ���� [%]'),

*)

  FLD_Distance_M = 'Distance_M';

  FLD_ESR_REQUIRED  = 'ESR_REQUIRED';
  FLD_BBER_REQUIRED = 'BBER_REQUIRED';
  FLD_ESR_NORM      = 'ESR_NORM';
  FLD_BBER_NORM     = 'BBER_NORM';

  FLD_GST_TYPE       = 'GST_TYPE';
  FLD_GST_LENGTH     = 'GST_LENGTH';
  FLD_GST_SESR       = 'GST_SESR';
  FLD_GST_KNG        = 'GST_KNG';

  FLD_DATA_SOURCE = 'DATA_SOURCE';

//  FLD_reliability11  = 'reliability'; // ����������� ����������

  FLD_refraction   = 'refraction';  // ����������� ���������
  FLD_refraction_2 = 'refraction_2';  // ����������� ���������

 // FLD_EmpLevel     = 'EmpLevel';    // ��������� ������� ������� �� ����� ��������� (���)
  FLD_NFrenel      = 'NFrenel';     // ������ ���� ������� [nF]

  FLD_STATUS       = 'STATUS';   // WorkingType : (wtNone,wtWorking,wtNotWorking); //����������� ���������
    VAL_LINK_WORKING = 1;
    VAL_LINK_NOT_WORKING = 2;

  FLD_STATUS_STR = 'STATUS_STR';

  //TRB: array[1..4] of double;  //���������� � ����������� �������� ���

//  FLD_NORM_SESR  = 'norm_SESR';  //1=0.012 ;����������� �������� SESR ��� ����, %
//  FLD_NORM_Kng   = 'norm_Kng';   //2=0.05  ;����������� �������� ���  ��� ����, %
//  FLD_GST_length = 'GST_length'; //3=600   ;����� ����, ��
  FLD_KNG_dop    = 'KNG_dop';    //4=0.200 ;���������� ����� ���, ������������� �������������

  //DLT: array[1..4] of double;  //���������� � �������� ��������
  // 1= 0.100     ;��� ��������� �������, �� ( 0 - ��������� ����)
  // 2= 1.000     ;�������� ������ ������� V����(g)=V���.�, ��
  // 3= 1.000     ;�������� ������ ������� g(V����)=g(V���.�), 10^-8
  // 4=80.000     ;������������ �������� ��� ������� ������������, 10^-8
  FLD_profile_step              = 'profile_step';
  FLD_precision_V_diffraction   = 'precision_V_diffraction';
  FLD_precision_g_V_diffraction = 'precision_g_V_diffraction';
  FLD_max_gradient_for_subrefr  = 'max_gradient_for_subrefraction';

 // FLD_TerrainType   = 'TerrainType';
//  FLD_RX_LEVEL      = 'RX_LEVEL';

 //report
 //FLD_max_gradient_for_subrefraction = 'max_gradient_for_subrefraction';
// FLD_PolarizationType = 'PolarizationType';


  //---RRV: //�������������� ����� �� �������� ��������������� ���������
  FLD_RRV_NAME = 'RRV_NAME';

//  FLD_gradient          = 'gradient';     //2= 7.000 ;����������� ���������� ���������,10^-8 1/�

  FLD_Region_name       = 'Region_name';  //         ;��� ������

  FLD_gradient_diel     = 'gradient_diel';//1=-9.000 ;������� �������� ��������� ����.�������������,10^-8 1/�
  FLD_gradient_deviation= 'gradient_deviation';
  FLD_terrain_type      = 'terrain_type'; //3= 1.000 ;��� ������������ �������.(1...3,0->����.�����=����.���.)
  FLD_underlying_terrain_type ='underlying_terrain_type';
  FLD_steam_wet         = 'steam_wet';    //4= 7.500 ;���������� ��������� �������� ����, �/���.�
  FLD_Q_factor          = 'Q_factor';     //5= 1.000 ;Q-������ ������ �����������
  FLD_climate_factor    = 'climate_factor'; //6=41.000     ;������������� ������ K��, 10^-6
  FLD_factor_B          = 'factor_B';     //7= 1.500 ;�������� ������������ b ��� ������� ������
  FLD_factor_C          = 'factor_C';     //8= 0.500 ;�������� ������������ c ��� ������� ������
  FLD_factor_D          = 'factor_D';     //9= 2.000 ;�������� ������������ d ��� ������� ������
  FLD_rain_intensity   = 'rain_intensity';    //10=20.000;������������� ����� � ������� 0.01% �������, ��/���
  FLD_Air_temperature         ='Air_temperature';
  FLD_Atmosphere_pressure     ='Atmosphere_pressure';


  FLD_GOST_gradient_diel       = 'GOST_gradient_diel';//1=-9.000 ;������� �������� ��������� ����.�������������,10^-8 1/�
  FLD_GOST_gradient_deviation  = 'GOST_gradient_deviation';
  FLD_GOST_terrain_type        = 'GOST_terrain_type'; //3= 1.000 ;��� ������������ �������.(1...3,0->����.�����=����.���.)
  FLD_GOST_underlying_terrain_type ='GOST_underlying_terrain_type';
  FLD_GOST_steam_wet           = 'GOST_steam_wet';    //4= 7.500 ;���������� ��������� �������� ����, �/���.�
 // FLD_GOST_rain_intensity      = 'GOST_rain_intensity';    //10=20.000;������������� ����� � ������� 0.01% �������, ��/���
  FLD_GOST_Air_temperature     ='GOST_Air_temperature';
  FLD_GOST_Atmosphere_pressure ='GOST_Atmosphere_pressure';
  FLD_GOST_climate_factor    = 'GOST_climate_factor'; //6=41.000     ;������������� ������ K��, 10^-6


  FLD_NIIR1998_gradient_diel       = 'NIIR1998_gradient_diel';//1=-9.000 ;������� �������� ��������� ����.�������������,10^-8 1/�
  FLD_NIIR1998_gradient_deviation  = 'NIIR1998_gradient_deviation';
  FLD_NIIR1998_terrain_type        = 'NIIR1998_terrain_type'; //3= 1.000 ;��� ������������ �������.(1...3,0->����.�����=����.���.)
  FLD_NIIR1998_underlying_terrain_type ='NIIR1998_underlying_terrain_type';
  FLD_NIIR1998_steam_wet           = 'NIIR1998_steam_wet';    //4= 7.500 ;���������� ��������� �������� ����, �/���.�
  FLD_NIIR1998_rain_intensity      = 'NIIR1998_rain_intensity';    //10=20.000;������������� ����� � ������� 0.01% �������, ��/���
  FLD_NIIR1998_Air_temperature     = 'NIIR1998_Air_temperature';
  FLD_NIIR1998_Atmosphere_pressure = 'NIIR1998_Atmosphere_pressure';



  FLD_rain_INTENSITY_extra = 'rain_INTENSITY_extra';

//  FLD_refraction_gradient_0 = 'refraction_gradient_0';   //11= 1.335;�����. ��������� (��� ��.���������=0, ����� �.�.=0)


 // FLD_water_area_percent = 'water_area_percent';


  LINK_CONDITIONS_FIELDS_ARR11: array [0..9] of string =
     (
      FLD_Q_factor     ,
      FLD_terrain_type ,
      FLD_gradient_diel,
      FLD_gradient_deviation  ,

      FLD_steam_wet    ,

      FLD_climate_factor,
      FLD_factor_B     ,
      FLD_factor_C     ,
      FLD_factor_D     ,
      FLD_rain_intensity
     );

     
  FLD_MARGIN_HEIGHT             = 'MARGIN_HEIGHT';
  FLD_MARGIN_DISTANCE           = 'MARGIN_DISTANCE';

  FLD_SESR_SUBREFRACTION        = 'SESR_SUBREFRACTION';
  FLD_SESR_RAIN                 = 'SESR_RAIN';
  FLD_SESR_INTERFERENCE         = 'SESR_INTERFERENCE';


  FLD_KNG_SUBREFRACTION        = 'KNG_SUBREFRACTION';
  FLD_KNG_RAIN                 = 'KNG_RAIN';
  FLD_KNG_INTERFERENCE         = 'KNG_INTERFERENCE';


 // FLD_MANUFACTURER              = 'manufacturer';

//  FLD_CONTAINER                 = 'container';

 // FLD_DEVICE_NAME               = 'DEVICE_NAME';

  FLD_RAIN_IsCalcLength         = 'RAIN_IsCalcLength';
//  FLD_RAIN_RATE                 = 'rain_rate' ;
 // FLD_RAIN_INTENSITY                 = 'rain_intensity' ;
  FLD_RAIN_SIGNAL_QUALITY            = 'RAIN_signal_quality' ;
  FLD_RAIN_WEAKENING_VERT            = 'RAIN_weakening_vert' ;
  FLD_RAIN_WEAKENING_HORZ            = 'RAIN_weakening_horz' ;
  FLD_RAIN_ALLOWABLE_INTENSE_VERT    = 'RAIN_allowable_intense_vert' ;
  FLD_RAIN_ALLOWABLE_INTENSE_HORZ    = 'RAIN_allowable_intense_horz' ;

  FLD_CHANNEL_WIDTH        = 'CHANNEL_WIDTH';
  FLD_TXRX_SHIFT           = 'TXRX_SHIFT';
  FLD_CHANNEL_COUNT        = 'CHANNEL_COUNT';
  FLD_CHANNEL_NUMBER       = 'CHANNEL_NUMBER';
//  FLD_CHANNEL_NUMBER_TYPE  = 'CHANNEL_NUMBER_TYPE';

//  FLD_KNG2                 = 'KNG2';

  FLD_THRESHOLD_BER_3   = 'THRESHOLD_BER_3';
  FLD_THRESHOLD_BER_6   = 'THRESHOLD_BER_6';
  FLD_BER_REQUIRED      = 'BER_REQUIRED';
  FLD_BER_REQUIRED_STR  = 'BER_REQUIRED_STR';

 // FLD_SPEED_ROW         = 'SPEED_ROW';

  FLD_OTN_PROSVET       = 'OTN_PROSVET';
 // FLD_WEAKNESS          = 'WEAKNESS';

  FLD_rain_signal_depression_dB = 'rain_signal_depression_dB';

//  ���������� ���������� (��)

  FLD_RASHODIMOST       = 'RASHODIMOST';
  FLD_REFLECT_FACTOR    = 'REFLECT_FACTOR';

  FLD_LENGTH_norm       = 'LENGTH_NORM';
  FLD_SESR_norm         = 'SESR_NORM';
  FLD_KNG_norm          = 'KNG_NORM';


  FLD_MODULATION_TYPE   = 'MODULATION_TYPE';
  FLD_SIGNATURE_HEIGHT  = 'SIGNATURE_HEIGHT';
  FLD_SIGNATURE_WIDTH   = 'SIGNATURE_WIDTH';
//  FLD_KRATNOST_BY_FREQ  = 'KRATNOSTBYFREQ';
//  FLD_KRATNOST_BY_SPACE = 'KRATNOSTBYSPACE';

  FLD_FREQ_SPACING      = 'FREQ_SPACING';
  FLD_MODE              = 'MODE';
  FLD_SPACE_LIMIT             = 'SPACE_LIMIT';
  FLD_SPACE_LIMIT_PROBABILITY = 'SPACE_LIMIT_PROBABILITY';

//  FLD_MODULATION_TYPE = 'MODULATION_TYPE';
  FLD_BANDWIDTH = 'BANDWIDTH';

  FLD_BANDWIDTH_TX_3  = 'BANDWIDTH_TX_3';
  FLD_BANDWIDTH_TX_30 = 'BANDWIDTH_TX_30';
  FLD_BANDWIDTH_TX_A  = 'BANDWIDTH_TX_A';

  FLD_BANDWIDTH_RX_3  = 'BANDWIDTH_RX_3';
  FLD_BANDWIDTH_RX_30 = 'BANDWIDTH_RX_30';
  FLD_BANDWIDTH_RX_A  = 'BANDWIDTH_RX_A';

  FLD_HIGH  = 'HIGH';
  FLD_LOW   = 'LOW';
(*
const
  FLD_rrv_name = 'rrv_name'; // ������� ��������������� ��������� (���)
//  FLD_RRI_GOST = 'GOST'; // ��������� ��� (���� � 53363)
//  FLD_RRI_NIIR = 'NIIR'; // ��������� ��� (���� 1998)

*)



const
  FLD_NIIR1998_NAME  = 'NIIR1998_NAME';
  FLD_GOST_NAME = 'GOST_NAME';


  FLD_NIIR1998_rain_region_number = 'NIIR1998_rain_region_number';
  FLD_NIIR1998_Qd_region_number   = 'NIIR1998_Qd_region_number';
//  FLD_NIIR1998_gradient           = 'NIIR1998_gradient';
//  FLD_NIIR1998_numidity           = 'NIIR1998_numidity';


  LINK_CONDITIONS_FIELDS_ITU_ARR: array [0..10] of string =
     (
      FLD_underlying_terrain_type,
      FLD_Q_factor     ,
      FLD_terrain_type ,
      FLD_gradient_diel,
      FLD_gradient_deviation  ,

      FLD_steam_wet,

      FLD_climate_factor,
      FLD_factor_B,
      FLD_factor_C,
      FLD_factor_D,
      FLD_rain_intensity

     );

  LINK_CONDITIONS_FIELDS_GOST_ARR: array [0..8] of string =
     (
      FLD_GOST_TERRAIN_TYPE,
      FLD_GOST_underlying_terrain_type     ,
      FLD_GOST_steam_wet ,

      FLD_GOST_air_temperature,
      FLD_GOST_atmosphere_pressure  ,

      FLD_GOST_Climate_factor,

      FLD_GOST_rain_intensity,
      FLD_GOST_link_center_lat,
      FLD_GOST_link_center_lon

     );

  LINK_CONDITIONS_FIELDS_NIIR1998_ARR: array [0..6] of string =
     (
      FLD_NIIR1998_TERRAIN_TYPE,
      FLD_NIIR1998_steam_wet,
      FLD_NIIR1998_air_temperature,
      FLD_NIIR1998_atmosphere_pressure,

      FLD_NIIR1998_rain_region_number,
      FLD_NIIR1998_Qd_region_number,
      FLD_NIIR1998_Water_area_percent
     );


const
  DEF_Calc_Method_ITU_R       = 0;
  DEF_Calc_Method_GOST        = 1;
  DEF_Calc_Method_NIIR_1998   = 2;

  DEF_Calc_Method_E_BAND_ITU  = 3;
//  DEF_Calc_Method_E_BAND_GOST = 4;
 // DEF_Calc_Method_E_BAND_NIIR = 5;
//  DEF_Calc_Method_UKV         = 6;
  DEF_Calc_Method_UKV         = 4;


   LINK_CALC_METHODS_ARR : array[0..4] of
      record
        ID   : Integer;
        Name : string;
      end  =

  (
  (id : DEF_Calc_Method_ITU_R;      Name: '���������� (ITU, ����, 16�����)'),
  (id : DEF_Calc_Method_GOST;       Name: '���� � 53363-2009�.'),
  (id : DEF_Calc_Method_NIIR_1998;  Name: '����-98�.'),
  (id : DEF_Calc_Method_E_BAND_ITU; Name: 'E-�������� (ITU)'),
  (id : DEF_Calc_Method_UKV;        Name: '��� (16�����)')
  );
      


implementation



end.


