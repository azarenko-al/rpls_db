unit dm_Link;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms,  Dialogs, Variants,

  dm_Onega_DB_data,

  dm_Main,

  u_assert,

  dm_Link_climate,

  dm_Object_base,


  u_classes,
  u_db,
  u_dlg,
  u_func,
  u_Geo,

  u_Log,

  u_const_db,

  u_radio,

  u_reflection_points,

  u_types,

  u_Link_const,
  u_LinkLine_const,

//  u_const_db,

  dm_Library,
  dm_LinkLine,
  dm_Link_calc_SESR,

  dm_Property,
  dm_LinkEnd
  ;


type
  TdmLinkType = (ltLink,ltPmpLink);



  TdmLinkCalcReserve_Rec= record
          LinkEndID1, LinkEndID2: integer;
        //  aBLVector:
   // TBLVector; var
          //
          RxLevel_dBm: Double;
          Fade_margin_dB: Double;
       // ): Double;

        end;


  //---------------------------------------
  TdmLinkAddRec = record
  //---------------------------------------
//    Project_ID: Integer;

    Template_Site_ID: Integer;


    NewName: string;
    FolderID: integer;
    ClutterModelID: integer;

    Comments : string;

    Property1_ID: Integer;
    Property2_ID: Integer;


    // linkends ---------------------------

    LinkEnd1_ID: Integer;
    LinkEnd2_ID: Integer;

    // PMP --------------------------------

//    PmpSectorID: Integer;
//    PmpTerminalID: integer;

    Link_Repeater_ID : Integer;

    // new


    IsUse_fixed_values : Boolean;

    SESR : double;
    KNG : double;
    Space_limit1 : Double;

    calc_method : Integer;

    LinkType_ID : Integer;

(*        row_Use_fixed_values.Properties.Value := ReadBool('', row_Use_fixed_values.Name, False);


    row_SESR.Properties.Value        := ReadString('', row_SESR.Name, FloatToStr(0.0125));
    row_KNG.Properties.Value         := ReadString('', row_KNG.Name,  FloatToStr(0.003));
    row_Space_limit.Properties.Value := ReadInteger('', row_Space_limit.Name, 10);

//    row_calc_method.Properties.Value := ReadString('', row_calc_method.Name, LINK_CALC_METHODS_ARR[0].Name);

    k := ReadInteger('', row_calc_method.Name, 0);

    cx_SetRowItemIndex(row_calc_method, k);


    row_RRV_type.Properties.Value := ReadString('', row_RRV_type.Name, 0);

    row_RRV_model.Properties.Value:=ReadString('', row_RRV_model.Name, '');
*)


  end;
 

type
  //---------------------------------------
  TdmLinkInfoRec = record
  //---------------------------------------
    ID: Integer;
    ClutterModel_ID: integer;

    is_profile_reversed : Boolean;  //����������� ���������� �������

    Length_km : double;
    BLVector: TBLVector;     //updated for CalcDirection

    Left,Right : record
                   LinkEndID : Integer;
                   PropertyID: Integer;
                   BLPoint   : TBLPoint;
                end;

    PropertyID1: Integer;
    PropertyID2: Integer;

    // linkends ---------------------------
    LinkEnd1_ID: Integer;
    LinkEnd2_ID: integer;


    // PMP --------------------------------

    PmpSectorID: Integer;
    PmpTerminalID: integer;

    LinkType: TdmLinkType;

    Tx_Freq_MHz,
    Refraction,
    NFrenel: double;

    PROFILE_Step_M: integer; //��� ��������� �������

    Profile_XML: string;
    Status: Integer;

    SESR : Double;

    Link_repeater: record
                     ID1 : Integer;
                     //
                   end;
  end;


  //-------------------------------------------------------------------
  TdmLink = class(TdmObject_base)
    qry_Links: TADOQuery;
    qry_LinkEndType: TADOQuery;
    qry_LinkEnd: TADOQuery;
    qry_Link_profile_xml: TADOQuery;
    qry_Temp: TADOQuery;
    ADOStoredProc_Link: TADOStoredProc;
    procedure DataModuleCreate(Sender: TObject);


  private
  public

    function CalcReserve(aRec: TdmLinkCalcReserve_Rec; aLinkEndID1, aLinkEndID2:
        integer; aBLVector: TBLVector; var aRxLevel_dBm, aFade_margin_dB: double):
        Double;

    function Del (aID: integer): boolean; override;


    function Add(aRec: TdmLinkAddRec; aIDList: TIDList = nil): integer;

    function GetBLCenter (aID: integer): TBLPoint;
    function GetBLVector(aID: integer; var aBLVector: TBLVector): Boolean;
    function GetClutterModelID(aID: integer): integer;


    function GetInfo1(aID: integer; var aRec: TdmLinkInfoRec): Boolean;

// TODO: GetInfoFromDataset11
   function GetInfoFromDataset(aDataset: TDataset; var aRec: TdmLinkInfoRec):
       boolean;

    function GetLinkEndID (aID: integer; var aLinkEndID1,aLinkEndID2: integer): boolean; overload;


    function GetReflectionPointArray(aID: integer; aLink_index: integer = 0):
        TrelReflectionPointRecArray;

    function GetType(aID: integer): TdmLinkType;
    function HasLinkRepeater(aID: integer): Boolean;

    function MakeLinkName1(aPropName1, aPropName2: string): string;
    // 1-�����, 2-������

    procedure UpdateName_Property1_to_Property2_(aID: integer; aIDList: TIDList =
        nil);

    procedure Update_profile_XML(aID: integer; aValue: string);

  end;   

function dmLink: TdmLink;

//===================================================================
implementation {$R *.dfm}
//===================================================================

var
 FdmLink: TdmLink;


// ------------------------------------------------------------------
function dmLink: TdmLink;
// ------------------------------------------------------------------
begin
  if not Assigned(FdmLink) then
    FdmLink := TdmLink.Create(Application);

  Result := FdmLink;
end;


//-------------------------------------------------------------------
procedure TdmLink.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  TableName:=TBL_LINK;
  ObjectName:=OBJ_LINK;
end;


//-------------------------------------------------------------------
function TdmLink.GetInfo1(aID: integer; var aRec: TdmLinkInfoRec): boolean;
//-------------------------------------------------------------------
var
  k: Integer;
  rec1,rec2: TdmLinkInfoRec;
  sViewName: string;

begin
  result := False;
  FillChar(aRec, SizeOf(aRec), 0);

  Assert(aID>0, ' TdmLink.GetInfo1(aID: integer;  aID>0');

  k:=dmOnega_DB_data.Link_Select(ADOStoredProc_Link, aID);


  Result:=not ADOStoredProc_Link.IsEmpty;
  if not Result then
    Exit;

  with aRec, ADOStoredProc_Link do
  begin
  //  ViewName:=sViewName;
    ID:=aID;

    Length_km :=  FieldByName(FLD_Length_km).AsFloat;


 //     eDistance_KM:=FLinkParamRec.Length_km; //geo_Distance_km(FLinkParamRec.BLVector);

 //   Assert (Length_km>0.01, 'Length_km>0.01: ' + FloatToStr(Length_km));



    if Eq (FieldByName(FLD_OBJNAME).AsString, 'pmp_link')
      then LinkType:=ltPmpLink
      else LinkType:=ltLink;


     case aRec.LinkType of    //
        ltPmpLink: begin
//          PmpSectorID  :=FieldByName(FLD_PMP_SECTOR_ID).AsInteger;
 //         PmpTerminalID:=FieldByName(FLD_PMP_TERMINAL_ID).AsInteger;

        end;

        ltLink: begin
          LinkEnd1_ID   :=FieldByName(FLD_LINKEND1_ID).AsInteger;
          LinkEnd2_ID   :=FieldByName(FLD_LINKEND2_ID).AsInteger;
        end;
      end;    // case

      LinkEnd1_ID   :=FieldByName(FLD_LINKEND1_ID).AsInteger;
      LinkEnd2_ID   :=FieldByName(FLD_LINKEND2_ID).AsInteger;


      PropertyID1  :=FieldByName(FLD_Property1_ID).AsInteger;
      PropertyID2  :=FieldByName(FLD_Property2_ID).AsInteger;


//      CalcDirection:=FieldByName(FLD_CALC_DIRECTION).AsInteger;
      BLVector     :=db_ExtractBLVector (ADOStoredProc_Link);

      Assert(geo_Distance_m(BLVector)>0, 'geo_Distance_m(BLVector)>0');


      is_profile_reversed:=FieldByName(FLD_is_profile_reversed).AsBoolean;


      case LinkType of
        ltLink: begin
                  if not is_profile_reversed then
                  begin
                    Left.LinkEndID  :=LinkEnd1_ID;
                    Left.PropertyID :=PropertyID1;

                    Right.LinkEndID  :=LinkEnd2_ID;
                    Right.PropertyID :=PropertyID2;

                    Left.BLPoint  :=BLVector.Point1;
                    Right.BLPoint :=BLVector.Point2;

                  end else
                  begin
                    Left.LinkEndID  :=LinkEnd2_ID;
                    Left.PropertyID :=PropertyID2;

                    Right.LinkEndID  :=LinkEnd1_ID;
                    Right.PropertyID :=PropertyID1;

                    Left.BLPoint  :=BLVector.Point2;
                    Right.BLPoint :=BLVector.Point1;

                  end;

                end;
        else
        ;//  BLVector_new :=BLVector;
      end;


      Status :=FieldByName(FLD_Status).AsInteger;

      NFrenel       :=FieldByName(FLD_NFRENEL).AsFloat;
      Refraction    :=FieldByName(FLD_REFRACTION).AsFloat;

      ClutterModel_ID:=FieldByName(FLD_CLUTTER_MODEL_ID).AsInteger;

      PROFILE_Step_M:=FieldByName(FLD_PROFILE_STEP).AsInteger;

      Tx_Freq_MHz     :=FieldByName(FLD_Tx_Freq_MHz).AsFloat;
      SESR     :=FieldByName(FLD_SESR).AsFloat;

      Profile_XML   :=FieldByName(FLD_Profile_XML).AsString ;


  end;
end;




//-------------------------------------------------------------------
function TdmLink.GetInfoFromDataset(aDataset: TDataset; var aRec:
  TdmLinkInfoRec): boolean;
//-------------------------------------------------------------------
// 1-�����, 2-������
var
  iCalcDirection: Integer;

  rec1,rec2: TdmLinkInfoRec;
   // sViewName: string;

begin
  result := False;
  FillChar(aRec, SizeOf(aRec), 0);

   // Assert(aID>0, ' TdmLink.GetInfo1(aID: integer;  aID>0');



  Result:=not aDataset.IsEmpty;
  if not Result then
    Exit;

  with aRec, aDataset do
  begin
  //  ViewName:=sViewName;
    ID:=FieldByName(FLD_ID).AsInteger;;


    if Eq (FieldByName(FLD_OBJNAME).AsString, 'pmp_link')
      then LinkType:=ltPmpLink
      else LinkType:=ltLink;


     case aRec.LinkType of    //
        ltPmpLink: begin
      //    PmpSiteID    :=FieldByName(FLD_PMP_SITE_ID).AsInteger;
          PmpSectorID  :=FieldByName(FLD_PMP_SECTOR_ID).AsInteger;
          PmpTerminalID:=FieldByName(FLD_PMP_TERMINAL_ID).AsInteger;

        end;

        ltLink: begin
          LinkEnd1_ID   :=FieldByName(FLD_LINKEND1_ID).AsInteger;
          LinkEnd2_ID   :=FieldByName(FLD_LINKEND2_ID).AsInteger;
        end;
      end;    // case

      PropertyID1  :=FieldByName(FLD_Property1_ID).AsInteger;
      PropertyID2  :=FieldByName(FLD_Property2_ID).AsInteger;


      is_profile_reversed:=FieldByName(FLD_is_profile_reversed).AsBoolean;

   //     CalcDirection:=FieldByName(FLD_CALC_DIRECTION).AsInteger;
      BLVector     :=db_ExtractBLVector (aDataset); //  qry_Temp


  (*
      //pmp ????
      case LinkType of
        ltLink:    geo_UpdateVector (BLVector1, LinkEndID1, LinkEndID2, CalcDirection);
      //  ltPmpLink: geo_UpdateVector (BLVector, LinkEndID1, LinkEndID2, CalcDirection);

      end;*)


      case LinkType of
        ltLink: begin
                  if not is_profile_reversed then
                  begin
                    Left.LinkEndID  :=LinkEnd1_ID;
                    Left.PropertyID :=PropertyID1;

                    Right.LinkEndID  :=LinkEnd2_ID;
                    Right.PropertyID :=PropertyID2;

                    Left.BLPoint  :=BLVector.Point1;
                    Right.BLPoint :=BLVector.Point2;

                  end else
                  begin
                    Left.LinkEndID  :=LinkEnd2_ID;
                    Left.PropertyID :=PropertyID2;

                    Right.LinkEndID  :=LinkEnd1_ID;
                    Right.PropertyID :=PropertyID1;

                    Left.BLPoint  :=BLVector.Point2;
                    Right.BLPoint :=BLVector.Point1;

                  end;

                end;
        else
        ;//  BLVector_new :=BLVector;

      end;


    Status :=FieldByName(FLD_Status).AsInteger;

    NFrenel       :=FieldByName(FLD_NFRENEL).AsFloat;
    Refraction    :=FieldByName(FLD_REFRACTION).AsFloat;

    ClutterModel_ID:=FieldByName(FLD_CLUTTER_MODEL_ID).AsInteger;

    PROFILE_Step_M:=FieldByName(FLD_PROFILE_STEP).AsInteger;
    Tx_Freq_MHz      :=FieldByName(FLD_Tx_Freq_MHz).AsFloat;

    Profile_XML   :=FieldByName(FLD_Profile_XML).AsString ;
  end;

end;


//-------------------------------------------------------------------
function TdmLink.GetLinkEndID (aID: integer;
                               var aLinkEndID1,aLinkEndID2: integer): boolean;
//-------------------------------------------------------------------
var rec: TdmLinkInfoRec;
begin
 if GetInfo1 (aID,  rec) then
  begin
    aLinkEndID1:=rec.LinkEnd1_ID;
    aLinkEndID2:=rec.LinkEnd2_ID;

    Result := True;
  end else
    Result := False;
end;



function TdmLink.GetBLCenter (aID: integer): TBLPoint;
var
  blVector: TBLVector;
begin
  if GetBLVector(aID, blVector) then
    Result:=geo_GetVectorCenter (blVector);
end;

//------------------------------------------------------------
function TdmLink.GetBLVector(aID: integer; var aBLVector: TBLVector): Boolean;
//------------------------------------------------------------
var
  iResult: Integer;
begin
  Assert(aID>0);

  iResult:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc1,
                'sp_Link_select_line_points',  [FLD_ID, aID]);

  Result := iResult>0;

  if Result then
    aBLVector :=db_ExtractBLVector (ADOStoredProc1);

end;



function TdmLink.GetClutterModelID(aID: integer): integer;
begin
  Result:=GetIntFieldValue (aID, FLD_CLUTTER_MODEL_ID);
end;


function TdmLink.GetType(aID: integer): TdmLinkType;
begin
  if Eq (GetStringFieldValue (aID, FLD_OBJNAME) , 'pmp_link')
    then Result:=ltPmpLink
    else Result:=ltLink;
end;

//--------------------------------------------------------------------
function TdmLink.Add(aRec: TdmLinkAddRec; aIDList: TIDList = nil): integer;
//--------------------------------------------------------------------
//
//--------------------------------------------------------------------
var
  ind,id1,id2: integer;

  blVector: TBLVector;
  dEsrNorm, dEsrReq, dBberNorm, dBberReq: double;
  dLength_m: Double;

  sType: string;

  rec: TCalcEsrBberParamRec;

begin
  Result:=0;

 // Assert(aRec.Project_ID>0, 'Project_ID>0 Value <=0');


  Result:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc1, SP_LINK_ADD,
          [
//           db_Par(FLD_Project_ID,       IIF_NULL(aRec.Project_ID)),
           FLD_Project_ID,       dmMain.ProjectID,

           FLD_OBJNAME,     'link',

           FLD_Template_Site_ID, IIF_NULL(aRec.Template_Site_ID),



//    FLD_Property1_ID_, IIF_NULL(aRec.Property1_ID),
//    FLD_Property2_ID_, IIF_NULL(aRec.Property1_ID),
//    Property2_ID: Integer;



           FLD_NAME,             aRec.NewName,

           FLD_FOLDER_ID,        IIF_NULL(aRec.FolderID),
           FLD_CLUTTER_MODEL_ID, IIF_NULL(aRec.ClutterModelID),

           FLD_LINKEND1_ID,      IIF_NULL(aRec.LinkEnd1_ID),
           FLD_LINKEND2_ID,      IIF_NULL(aRec.LinkEnd2_ID),

//           FLD_PMP_SECTOR_ID,    IIF_NULL(aRec.PmpSectorID),
 //          FLD_PMP_TERMINAL_ID,  IIF_NULL(aRec.PmpTerminalID),

           FLD_Comments,         IIF_NULL(aRec.Comments) ,

       //    db_Par(FLD_LINK_REPEATER_ID, aRec.LINK_REPEATER_ID),


       //    db_Par(FLD_SESR,         IIF_NULL(aRec.SESR)),
        //   db_Par(FLD_KNG,          IIF_NULL(aRec.KNG)),
    //       FLD_Space_limit,  IIF_NULL(aRec.Space_limit1),

//           db_Par(FLD_calc_method,  IIF_NULL(aRec.calc_method))
           FLD_calc_method,  aRec.calc_method

//IsUse_fixed_values, SESR, KNG, Space_limit, calc_method, LinkType_ID;

          ]);

  if Result<=0 then
    Exit;


//  db_View (ADOStoredProc1);


  if not ADOStoredProc1.IsEmpty then
  begin
    blVector := db_ExtractBLVector(ADOStoredProc1);

    dLength_m:= geo_Distance_m (blVector);

  end;


  if Assigned(aIDList) then
    aIDList.AddObjName(Result, OBJ_LINK);


  ind:=5;  //������������� ���� (�� 50)

  { TODO : !!!!!!!!!!! }

  rec.ID        :=Result;
  rec.ObjName   :=OBJ_LINK;
  rec.Length_KM :=dLength_m / 1000;
  rec.GST_Type   :=ind;

  dmLink_calc_SESR.CalcEsrBber11(rec
  //Result, OBJ_LINK, dLength_m, ind,
    //                            dEsrNorm, dEsrReq, dBberNorm, dBberReq
                                );

  Update_ (Result,
         [
          db_Par(FLD_GST_TYPE,      ind ),
          db_Par(FLD_GST_LENGTH,    LINKLINE_NORM_ARR1[ind].Max_Length_KM ),
          db_Par(FLD_GST_SESR,      LINKLINE_NORM_ARR1[ind].SESR ),
          db_Par(FLD_GST_KNG,       LINKLINE_NORM_ARR1[ind].KNG ),

          db_Par(FLD_ESR_NORM,      rec.Output.Esr_Norm),
          db_Par(FLD_BBER_NORM,     rec.Output.Bber_Norm),

          db_Par(FLD_ESR_REQUIRED,  rec.Output.Esr_required),
          db_Par(FLD_BBER_REQUIRED, rec.Output.Bber_Required)
         ]);


  if aRec.IsUse_fixed_values then
  begin

    Update_ (Result,
       [
       db_Par(FLD_GST_TYPE,      0),

       db_Par(FLD_GST_SESR,     IIF_NULL(aRec.SESR)),
       db_Par(FLD_GST_KNG,      IIF_NULL(aRec.KNG)),
       db_Par(FLD_Space_limit,  IIF_NULL(aRec.Space_limit1))
       ] );

  end;



  if aRec.LinkType_ID>0 then
  begin
    dmLink_climate.OpenFiles;
    dmLink_climate.UpdateLinkClimateFromDict (Result, aRec.LinkType_ID);
    dmLink_climate.CloseFiles;

  end else
  begin
    dmLink_climate.OpenFiles;
    dmLink_climate.UpdateByMap (Result);
    dmLink_climate.CloseFiles;

  end;



//IsUse_fixed_values, SESR, KNG, Space_limit, calc_method, LinkType_ID;



end;




//--------------------------------------------------------------------
function TdmLink.GetReflectionPointArray(aID: integer; aLink_index: integer =
    0): TrelReflectionPointRecArray;
//--------------------------------------------------------------------
const
  SQL_SELECT_reflection_POINTS =
    'SELECT * FROM '+ TBL_Link_reflection_points +
      ' WHERE (link_id=:link_id)  ';
    //  ' ORDER BY  distance';

  SQL_SELECT_reflection_POINTS_with_INDEX =
    'SELECT * FROM '+ TBL_Link_reflection_points +
      ' WHERE (link_id=:link_id) and (link_index=:index) ';

//      ' ORDER BY distance';


var
  I: Integer;
  s: string;
begin
  Assert(aID>0);

  if aLink_index>0 then
    s:= SQL_SELECT_reflection_POINTS_with_INDEX
  else
    s:= SQL_SELECT_reflection_POINTS;


  db_OpenQuery(qry_Temp, s,
     [db_Par(FLD_LINK_ID, aID),
      db_Par(FLD_INDEX, aLink_index)
     ]);

 // OpenDB_ReflectionPoints (aID, mem_Points);

  SetLength (Result, qry_Temp.RecordCount);

  if Length(Result)=0 then
    Exit;

  with qry_Temp do
    for I := 0 to RecordCount-1 do
    begin
//      Result[i].Distance_KM    :=FieldByName(FLD_DISTANCE).AsFloat;
      Result[i].DISTANCE_to_center_km :=FieldByName(FLD_DISTANCE_to_Center_km).AsFloat;
      Result[i].Length_km      :=FieldByName(FLD_LENGTH_km).AsFloat;
      Result[i].Radius_KM      :=FieldByName(FLD_RADIUS_km).AsFloat;
      Result[i].abs_prosvet_m:=FieldByName(FLD_ABS_PROSVET).AsFloat;

{      Result[i].DISTANCE_to_center_km :=FieldByName(FLD_DISTANCE).AsFloat;
      Result[i].Length_km      :=FieldByName(FLD_LENGTH).AsFloat;
      Result[i].Radius_KM      :=FieldByName(FLD_RADIUS).AsFloat;
      Result[i].abs_prosvet_m:=FieldByName(FLD_ABS_PROSVET).AsFloat;
}


      Result[i].Link_index:=FieldByName(FLD_Link_index).AsInteger;

      Next;
    end;

end;


//--------------------------------------------------------------------
function TdmLink.HasLinkRepeater(aID: integer): Boolean;
//--------------------------------------------------------------------
const
  SQL_SELECT =
    'SELECT * FROM '+ TBL_Link_REPEATER +
      ' WHERE (link_id=:link_id)  ';
    //  ' ORDER BY  distance';

var
  I: Integer;

begin
  Assert(aID>0);

  db_OpenQuery(qry_Temp, SQL_SELECT,
     [db_Par(FLD_LINK_ID, aID)
     ]);

 // OpenDB_ReflectionPoints (aID, mem_Points);

   result := qry_Temp.RecordCount>0;


end;




function TdmLink.Del (aID: integer): boolean;
var
  iRes: Integer;
begin
  iRes:=dmOnega_DB_data.Link_Del(aID);
  Result:= iRes>0;
end;


//---------------------------------------------------------------
function TdmLink.MakeLinkName1(aPropName1, aPropName2: string): string;
//---------------------------------------------------------------
var
  sName: string;
//  rec: TObject_GetNewName_Params;
begin
  Result := '';

  if (aPropName1='') and (aPropName2='') then
    exit;

  sName:=aPropName1+' <-> '+aPropName2;

  {
  FillChar(rec,SizeOf(rec),0);
  rec.ObjName    := ObjectName;
  rec.Name       := sName;
  rec.Project_ID := dmMain.ProjectID;
//  rec.Property_ID := aPropertyID;
  }

  Result := dmOnega_DB_data.Object_GetNewName_select (//rec,
   ObjectName, dmMain.ProjectID, sName);

  Assert(Result<>'');


end;


// ---------------------------------------------------------------
procedure TdmLink.UpdateName_Property1_to_Property2_(aID: integer; aIDList:
    TIDList = nil);
// ---------------------------------------------------------------
var
  iRes: Integer;
begin
  iRes:=dmOnega_DB_data.Link_Rename1(aID, ADOStoredProc1);

  if Assigned(aIDList) then
    with ADOStoredProc1 do
      while not EOF do
      begin
        aIDList.Add_NameAndGUID(FieldValues[FLD_NAME], FieldValues[FLD_GUID]);
        Next;
      end;

//  db_ViewDataSet(qry_Temp);

(*
  if GetLinkEndID (aID, iLinkEndID1,iLinkEndID2) then
  begin
    sProp1:=dmLinkEnd.GetPropertyName(iLinkEndID1);
    sProp2:=dmLinkEnd.GetPropertyName(iLinkEndID2);

    sLinkName  :=sProp1 + ' <-> '+ sProp2;
    sLinkEndID1:=sProp1 + ' -> '+ sProp2;
    sLinkEndID2:=sProp2 + ' -> '+ sProp1;

    RenameNew1 (aID, sLinkName);

    dmLinkEnd.RenameNew1 (iLinkEndID1, sLinkEndID1);
    dmLinkEnd.RenameNew1 (iLinkEndID2, sLinkEndID2);


    sQUID1:=dmLinkEnd.GetGUIDByID (iLinkEndID1);
    sQUID2:=dmLinkEnd.GetGUIDByID (iLinkEndID2);
    sQUID3:=dmLink.GetGUIDByID (aID);

    if Assigned(aIDList) then
    begin
      aIDList.Add_NameAndGUID(sLinkEndID1, sQUID1);
      aIDList.Add_NameAndGUID(sLinkEndID2, sQUID2);
      aIDList.Add_NameAndGUID(sLinkName,   sQUID3);
    end;
  end;
*)

{
    PostEvent (WE_EXPLORER_RENAME_ITEM,
       [app_Par(PAR_GUID, sQUID1),
        app_Par(PAR_NAME, sLinkEndID1) ]);

    PostEvent (WE_EXPLORER_RENAME_ITEM,
       [app_Par(PAR_GUID, sQUID2),
        app_Par(PAR_NAME, sLinkEndID2) ]);

    PostEvent (WE_EXPLORER_RENAME_ITEM,
       [app_Par(PAR_GUID, sQUID3),
        app_Par(PAR_NAME, sLinkName) ]);
}



end;


//--------------------------------------------------------------------
function TdmLink.CalcReserve(aRec: TdmLinkCalcReserve_Rec; aLinkEndID1,
    aLinkEndID2: integer; aBLVector: TBLVector; var aRxLevel_dBm,
    aFade_margin_dB: double): Double;
//--------------------------------------------------------------------
//aRxLevel_dBm - '�������� �� ����� �������� (dBm): '+ AsString(TruncFloat(ePout));
//result- '����� �� ��������� (dB): '+ AsString(TruncFloat(Result));
(*const
  SQL_ANTENNA =
     'SELECT gain FROM ' +
     view_LInkEnd_Antennas +
     ' WHERE linkend_id = :linkend_id';

  SQL1 =
    'SELECT  POWER_dBm, tx_freq_MHz, rx_freq_MHz, '+
    '  threshold_ber_6,threshold_ber_3 FROM ' +
    view_LInkEnd+
    ' WHERE id = :id';
*)
var
  eDistance_km: Double;
  i: Integer;
  iRes: Integer;
  L, dFreq_Mhz, dGain1, dGain2, dTHRESHOLD_dBm,
 // dSpeed,
  dPower_dBm, Pout: double;

//  iLinkEnd1, iLinkEnd2: integer;
  oVector: TBLVector;
  sError: string;

 // rec: TCalcReserveRec;
begin
  Result := 0;


 // iLinkEnd1:= Fframe_Link_add1.GetLinkEndID();
 // iLinkEnd2:= Fframe_Link_add2.GetLinkEndID();

  if (aLinkEndID1 = 0) and (aLinkEndID2 = 0) then
    exit;

  // -------------------------------------------------------------------

 // qry_Temp.Connection:=dmMain.ADOConnection;

(*
  db_OpenQuery(qry_Temp, SQL_ANTENNA, [db_Par(FLD_LINKEND_ID, aLinkEndID1)]);
  dGain1:=qry_Temp.FieldByName(FLD_GAIN).AsFloat;

  db_OpenQuery(qry_Temp, SQL_ANTENNA, [db_Par(FLD_LINKEND_ID, aLinkEndID2)]);
  dGain2:=qry_Temp.FieldByName(FLD_GAIN).AsFloat;


  db_OpenQuery(qry_Temp, SQL1, [db_Par(FLD_ID, aLinkEndID1)]);
  dFreq_Mhz:= qry_Temp.FieldByName(FLD_TX_FREQ_MHz).AsFloat;
  if dFreq_Mhz = 0 then
    dFreq_Mhz:= qry_Temp.FieldByName(FLD_RX_FREQ_MHz).AsFloat;

  if (dFreq_Mhz = 0) then
    exit;

  dPower_dBm:=qry_Temp.FieldByName(FLD_POWER_dBm).AsFloat;

  db_OpenQuery(qry_Temp, SQL1, [db_Par(FLD_ID, aLinkEndID2)]);

  dTHRESHOLD_dBm:=qry_Temp.FieldByName(FLD_THRESHOLD_BER_6).AsFloat;
  if dTHRESHOLD_dBm = 0 then
    dTHRESHOLD_dBm:=qry_Temp.FieldByName(FLD_THRESHOLD_BER_3).AsFloat;

//  oVector:=MakeBLVector(Fframe_Link_add1.GetPropertyPos, Fframe_Link_add2.GetPropertyPos);

  rec.Freq_GHz  := dFreq_Mhz/1000;
  rec.Power_dBm := dPower_dBm;
  rec.Gain1     := dGain1;
  rec.Gain2     := dGain2;
  rec.Distance_km := geo_Distance_km (aBLVector);

  Result:= radio_CalcReserve(rec,
               dFreq_Mhz/1000,
               dPower_dBm, dGain1, dGain2, dTHRESHOLD_dBm,
               geo_Distance_km (aBLVector),
               aRxLevel_dBm, aBLVector );
*)

  eDistance_km := geo_Distance_km (aBLVector);

  try
    iRes:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc1, 'sp_Link_Calc_Reserve',
            [FLD_LINKEND1_ID, aLinkEndID1,
             FLD_LINKEND2_ID, aLinkEndID2,
             FLD_LENGTH_KM,   eDistance_km
             ]);

    if iRes<0 then
    begin
      sError:=ADOStoredProc1.Fields[0].Value;

      g_log.AddError ('', sError);

   //   ShowMessage(sError);

    //  Result := 0;
      Exit;
    end else

 //  db_View(ADOStoredProc1);

    if iRes>0 then
    begin
      aRxLevel_dBm    := ADOStoredProc1.FieldByName(FLD_rx_level_dBm).AsFloat;
      aFade_margin_dB := ADOStoredProc1.FieldByName(FLD_fade_margin_dB).AsFloat;

      Result       := ADOStoredProc1.FieldByName(FLD_fade_margin_dB).AsFloat;
    end;
  except
  //  on E: Exception do: ;
  end;

//  i:=1;

end;


procedure TdmLink.Update_profile_XML(aID: integer; aValue: string);
begin
//  dmOnega_DB_data.Link_Update_profile_XML

  Update (aID, FLD_profile_XML, aValue);
end;


begin

end.






