unit u_ComboBox_AutoComplete;


interface
uses
  Windows, SysUtils, Classes, Messages,  Forms, Controls, StdCtrls;


type
  TOnSelectedEvent = procedure (Sender: TObject; aID: integer) of object;

  TComboBoxAutoComplete = class(TComboBox)
  private
    FOnSelected: TOnSelectedEvent;

    FStoredItems: TStringList;
//    procedure FilterItems_old;
//    procedure StoredItemsChange(Sender: TObject);
    procedure SetStoredItems(const Value: TStringList);
    procedure CNCommand(var AMessage: TWMCommand); message CN_COMMAND;
    procedure DoOnCloseUp(Sender: TObject);
    procedure DoOnKeyPress(Sender: TObject; var Key: Char);
    procedure FilterItems;
    procedure Select;

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure AssignComboBox(aComboBox: TComboBox);
    function GetID: Integer;
    procedure SetTextByID(aID: Integer);

    property StoredItems: TStringList read FStoredItems write SetStoredItems;

    property OnSelected: TOnSelectedEvent read FOnSelected write FOnSelected;

  end;


implementation


//------------------------------------------------------------------------
constructor TComboBoxAutoComplete.Create(AOwner: TComponent);
//------------------------------------------------------------------------
begin
  inherited;
  AutoComplete := False;
  FStoredItems := TStringList.Create;
 // FStoredItems.OnChange := StoredItemsChange;

  OnKeyPress:=DoOnKeyPress;
  OnCloseUp:=DoOnCloseUp;

  Sorted:=True;


  AutoDropDown:=False;

  DropDownCount := 24;

  Left := 10;
  Top := 10;

//  Text := 'Br';

  {
 

  // here's how to fill the StoredItems
  StoredItems.BeginUpdate;
  try
    StoredItems.Add('Mr John Brown');
    StoredItems.Add('Mrs Amanda Brown');
    StoredItems.Add('Mr Brian Jones');
    StoredItems.Add('Mrs Samantha Smith');
  finally
    StoredItems.EndUpdate;
  end;
  }

end;


destructor TComboBoxAutoComplete.Destroy;
begin
  FStoredItems.Free;
  inherited;
end;

//------------------------------------------------------------------------
procedure TComboBoxAutoComplete.DoOnCloseUp(Sender: TObject);
//------------------------------------------------------------------------
var
  iID: Integer;
begin
  if ItemIndex<0 then
    exit;
    

  iID:= Integer( Items.Objects [ItemIndex] );

  if Assigned(FOnSelected) then
      FOnSelected(self, iID);

//  ShowMessage( IntToStr( ItemIndex )  );

//  Select();

//    if Assigned(FOnSelected) then
//      FOnSelected(self)

 //  ShowMessage('ComboBox1CloseUp  ');
end;

//------------------------------------------------------------------------
procedure TComboBoxAutoComplete.Select;
//------------------------------------------------------------------------
begin
//   Select();

  if Assigned(FOnSelected) then
    FOnSelected(self, GetID());

 //  ShowMessage('ComboBox1CloseUp  ');
end;



procedure TComboBoxAutoComplete.DoOnKeyPress(Sender: TObject; var Key: Char);
begin
  if Key=Chr(VK_RETURN)  then
    Select();
end;


procedure TComboBoxAutoComplete.CNCommand(var AMessage: TWMCommand);
begin
  // we have to process everything from our ancestor
  inherited;
  // if we received the CBN_EDITUPDATE notification
  if AMessage.NotifyCode = CBN_EDITUPDATE then
    // fill the items with the matches
    FilterItems;
end;


//------------------------------------------------------------------------
procedure TComboBoxAutoComplete.FilterItems;
//------------------------------------------------------------------------
var
  I: Integer;

  Selection: record
    StartPos, EndPos: Integer;
  end;

  sText: string;
begin

  Screen.Cursor := crNone;
  Screen.Cursor := crDefault;


  // store the current combo edit selection
/////////////
 SendMessage(Handle, CB_GETEDITSEL, WPARAM(@Selection.StartPos), LPARAM(@Selection.EndPos));

 sText:= '';

//Exit;

 // LockWindowUpdate(Self.Handle);

  // begin with the items update
  Items.BeginUpdate;
  try
    // if the combo edit is not empty, then clear the items
    // and search through the FStoredItems
    if Text <> '' then begin
      // clear all items
      Items.Clear;
      // iterate through all of them
      for I := 0 to FStoredItems.Count - 1 do begin
        // check if the current one contains the text in edit
//      if ContainsText(FStoredItems[I], Text) then

//        if Pos(   Text, FStoredItems[I])>0 then begin
        if Pos( AnsiLowerCase(Text), AnsiLowerCase(FStoredItems[I]))>0 then
        begin
          // and if so, then add it to the items
          Items.Add(FStoredItems[I]);
        end;
      end;
    end else begin
      // else the combo edit is empty
      // so then we'll use all what we have in the FStoredItems
      Items.Assign(FStoredItems)
    end;
  finally
    // finish the items update
    Items.EndUpdate;
  end;

 // Exit;


  // and restore the last combo edit selection
  sText := Text;
  SendMessage(Handle, CB_SHOWDROPDOWN, Integer(True), 0);


  if (Items<>nil) and (Items.Count>0) then begin
    ItemIndex := 0;
  end else begin
    ItemIndex := -1;
  end;

  Text := sText;

// LockWindowUpdate(0);

////////////

  SendMessage(Handle, CB_SETEDITSEL, 0, MakeLParam(Selection.StartPos, Selection.EndPos));

end;

// ---------------------------------------------------------------
procedure TComboBoxAutoComplete.AssignComboBox(aComboBox: TComboBox);
// ---------------------------------------------------------------
begin
  Left:= aComboBox.Left;
  Top:= aComboBox.Top;
  Width:= aComboBox.Width;

  Anchors:= aComboBox.Anchors;

  DropDownCount:= aComboBox.DropDownCount;
end;

//------------------------------------------------------------------------
function TComboBoxAutoComplete.GetID: Integer;
//------------------------------------------------------------------------
var
  i: Integer;
  s: string;
begin
  i:=FStoredItems.IndexOf(Text);

  if i>=0 then
    Result:= Integer( FStoredItems.Objects[i] )
  else
    Result := -1;
end;

//------------------------------------------------------------------------
procedure TComboBoxAutoComplete.SetTextByID(aID: Integer);
//------------------------------------------------------------------------
var
  i: Integer;
begin
  i:=FStoredItems.IndexOfObject(Pointer(aID));

  if i>=0 then
    Text:=FStoredItems[i]
  else
    Text := '';
end;



procedure TComboBoxAutoComplete.SetStoredItems(const Value: TStringList);
begin
  if Assigned(FStoredItems) then
    FStoredItems.Assign(Value)
  else
    FStoredItems := Value;
end;

end.

