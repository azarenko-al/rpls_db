object frame_Link_items: Tframe_Link_items
  Left = 1146
  Top = 578
  Width = 707
  Height = 328
  Caption = 'frame_Link_items'
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 120
  TextHeight = 16
  object TBDock1: TTBDock
    Left = 0
    Top = 0
    Width = 699
    Height = 29
    BoundLines = [blTop, blBottom, blLeft, blRight]
    object TBToolbar1: TTBToolbar
      Left = 0
      Top = 0
      Caption = 'TBToolbar1'
      TabOrder = 0
      object TBItem1: TTBItem
        Action = act_Edit
      end
      object TBItem2: TTBItem
        Action = act_Restore_default
      end
    end
  end
  object cxDBTreeList11: TcxDBTreeList
    Left = 0
    Top = 29
    Width = 699
    Height = 152
    Align = alTop
    Bands = <
      item
        Caption.AlignHorz = taCenter
        Caption.Text = #1054#1073#1098#1077#1082#1090#1099
        FixedKind = tlbfLeft
        Width = 170
      end
      item
        Caption.AlignHorz = taCenter
        Caption.Text = 'PPC'
      end
      item
        Caption.AlignHorz = taCenter
        Caption.Text = #1055#1083#1086#1097#1072#1076#1082#1072
        Width = 215
      end
      item
        Caption.AlignHorz = taCenter
        Caption.Text = #1040#1085#1090#1077#1085#1085#1072
        Width = 335
      end>
    DataController.DataSource = DataSource1
    DataController.ParentField = 'parent_guid'
    DataController.KeyField = 'guid'
    LookAndFeel.Kind = lfFlat
    OptionsBehavior.CellHints = True
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsBehavior.AutoDragCopy = True
    OptionsBehavior.DragCollapse = False
    OptionsBehavior.ExpandOnDblClick = False
    OptionsBehavior.ExpandOnIncSearch = True
    OptionsBehavior.MultiSort = False
    OptionsBehavior.ShowHourGlass = False
    OptionsBehavior.Sorting = False
    OptionsCustomizing.BandCustomizing = False
    OptionsCustomizing.DynamicSizing = True
    OptionsCustomizing.RowSizing = True
    OptionsData.Deleting = False
    OptionsSelection.HideSelection = True
    OptionsSelection.InvertSelect = False
    OptionsView.CellTextMaxLineCount = -1
    OptionsView.ShowEditButtons = ecsbFocused
    OptionsView.Bands = True
    OptionsView.Indicator = True
    OptionsView.SimpleCustomizeBox = True
    ParentColor = False
    Preview.AutoHeight = False
    Preview.MaxLineCount = 2
    Preview.Visible = True
    RootValue = -1
    TabOrder = 1
    TabStop = False
    OnEditing = cxDBTreeList11Editing
    OnExit = cxDBTreeList11Exit
    object col__cxDBTreeListColumn1: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      Options.Editing = False
      Width = 52
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_name: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'name'
      Options.Editing = False
      Options.Sorting = False
      Width = 170
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_linkendtype_name: TcxDBTreeListColumn
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.ReadOnly = True
      Properties.OnButtonClick = col__linkendtype_namePropertiesButtonClick
      DataBinding.FieldName = 'linkendtype_name'
      Width = 97
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_power_dBm: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Caption.AlignHorz = taCenter
      DataBinding.FieldName = 'power_dBm'
      Options.Editing = False
      Width = 47
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_THRESHOLD_BER_3: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Caption.AlignHorz = taCenter
      DataBinding.FieldName = 'THRESHOLD_BER_3'
      Options.Editing = False
      Width = 46
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_THRESHOLD_BER_6: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Caption.AlignHorz = taCenter
      DataBinding.FieldName = 'THRESHOLD_BER_6'
      Options.Editing = False
      Width = 42
      Position.ColIndex = 4
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_rx_freq: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Caption.AlignHorz = taCenter
      Caption.Text = 'rx_freq'
      DataBinding.FieldName = 'rx_freq_MHz'
      Options.Editing = False
      Width = 77
      Position.ColIndex = 5
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_tx_freq: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Caption.AlignHorz = taCenter
      Caption.Text = 'tx_freq'
      DataBinding.FieldName = 'tx_freq_MHz'
      Options.Editing = False
      Width = 56
      Position.ColIndex = 6
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__id: TcxDBTreeListColumn
      Tag = -1
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'id'
      Options.Editing = False
      Width = 39
      Position.ColIndex = -1
      Position.RowIndex = -1
      Position.BandIndex = -1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_address: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'address'
      Options.Editing = False
      Width = 99
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Pos: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'Pos'
      Options.Editing = False
      Width = 116
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_LinkEnd_loss: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'LinkEnd_loss'
      Options.Editing = False
      Width = 69
      Position.ColIndex = 7
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_polarization: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Caption.Text = #1055#1086#1083#1103#1088#1080#1079#1072#1094#1080#1103
      DataBinding.FieldName = 'POLARIZATION_str'
      Options.Editing = False
      Width = 24
      Position.ColIndex = 8
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Antenna_Loss: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = False
      DataBinding.FieldName = 'Antenna_Loss_dB'
      Options.Editing = False
      Width = 69
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_gain: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = False
      DataBinding.FieldName = 'gain'
      Options.Editing = False
      Width = 29
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_height: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = False
      Caption.AlignHorz = taCenter
      DataBinding.FieldName = 'height'
      Options.Editing = False
      Width = 32
      Position.ColIndex = 4
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_azimuth: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Caption.AlignHorz = taCenter
      DataBinding.FieldName = 'azimuth'
      Options.Editing = False
      Width = 47
      Position.ColIndex = 5
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_antennatype_name: TcxDBTreeListColumn
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.ReadOnly = True
      Caption.Text = 'ant_Type'
      DataBinding.FieldName = 'antennatype_name'
      Options.Editing = False
      Width = 34
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_diameter: TcxDBTreeListColumn
      PropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.AssignedValues.MaxValue = True
      Properties.AssignedValues.MinValue = True
      Properties.DisplayFormat = '0.0; ; '
      Properties.Nullable = False
      Properties.ReadOnly = False
      Caption.AlignHorz = taCenter
      DataBinding.FieldName = 'diameter'
      Options.Editing = False
      Width = 40
      Position.ColIndex = 6
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__tree_id: TcxDBTreeListColumn
      Tag = -1
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      DataBinding.FieldName = 'tree_id'
      Options.Editing = False
      Width = 44
      Position.ColIndex = -1
      Position.RowIndex = -1
      Position.BandIndex = -1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__tree_parent_id: TcxDBTreeListColumn
      Tag = -1
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      DataBinding.FieldName = 'tree_parent_id'
      Options.Editing = False
      Width = 74
      Position.ColIndex = -1
      Position.RowIndex = -1
      Position.BandIndex = -1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_code: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      Caption.Text = #1050#1086#1076' '#1086#1073#1098#1077#1082#1090#1072
      DataBinding.FieldName = 'code'
      Options.Sorting = False
      Width = 70
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_type: TcxDBTreeListColumn
      Tag = -1
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'type'
      Options.Editing = False
      Width = 50
      Position.ColIndex = -1
      Position.RowIndex = -1
      Position.BandIndex = -1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Antenna_Location_type: TcxDBTreeListColumn
      PropertiesClassName = 'TcxCheckBoxProperties'
      Visible = False
      Caption.Text = 'Antenna_Location_type'
      DataBinding.FieldName = 'Location_type'
      Width = 76
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__IsMaster: TcxDBTreeListColumn
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.NullStyle = nssUnchecked
      Visible = False
      DataBinding.FieldName = 'Is_Master'
      Options.Editing = False
      Width = 24
      Position.ColIndex = 7
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Band: TcxDBTreeListColumn
      DataBinding.FieldName = 'Band'
      Options.Editing = False
      Width = 55
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_BitRate_MBps: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.ReadOnly = True
      DataBinding.FieldName = 'BitRate_MBps'
      Options.Editing = False
      Width = 60
      Position.ColIndex = 8
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 240
    Top = 204
    object act_Edit: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      OnExecute = act_EditExecute
    end
    object act_Antenna_Move: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1099' '#1072#1085#1090#1077#1085#1085#1099
      Visible = False
      OnExecute = act_EditExecute
    end
    object act_Antenna_New: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1072#1085#1090#1077#1085#1085#1091
      OnExecute = act_EditExecute
    end
    object act_Select_LinkendType_Mode: TAction
      Caption = 'act_Select_LinkendType_Mode'
      OnExecute = act_EditExecute
    end
    object act_Select_LinkendType: TAction
      Caption = 'act_Select_LinkendType'
      OnExecute = act_EditExecute
    end
    object act_Grid_Setup: TAction
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
      OnExecute = act_EditExecute
    end
    object act_Restore_default: TAction
      Caption = 'Restore_default'
      OnExecute = act_EditExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 172
    Top = 204
    object N1: TMenuItem
      Action = act_Edit
    end
    object N2: TMenuItem
      Action = act_Antenna_Move
    end
    object N3: TMenuItem
      Action = act_Antenna_New
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N5: TMenuItem
      Action = act_Grid_Setup
    end
    object Restoredefault1: TMenuItem
      Action = act_Restore_default
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object actSelectLinkendType1: TMenuItem
      Action = act_Select_LinkendType
    end
    object actSelectLinkendTypeMode1: TMenuItem
      Action = act_Select_LinkendType_Mode
    end
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc1
    Left = 72
    Top = 208
  end
  object ADOStoredProc1: TADOStoredProc
    LockType = ltBatchOptimistic
    AfterPost = ADOQuery1AfterPost
    Parameters = <>
    Left = 40
    Top = 208
  end
end
