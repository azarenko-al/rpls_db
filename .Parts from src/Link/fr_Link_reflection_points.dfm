object frame_Link_reflection_points: Tframe_Link_reflection_points
  Left = 800
  Top = 295
  Width = 745
  Height = 395
  Caption = 'frame_Link_reflection_points'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar2: TToolBar
    Left = 0
    Top = 0
    Width = 737
    Height = 105
    Caption = 'ToolBar2'
    Color = clAppWorkSpace
    EdgeBorders = []
    ParentColor = False
    TabOrder = 0
    Visible = False
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 105
    Width = 737
    Height = 136
    Align = alTop
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView2: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = DataSource1
      DataController.Filter.MaxValueListCount = 1000
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Editing = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object cxGrid1DBTableView2distance: TcxGridDBColumn
        Caption = #1056#1072#1089#1089#1090#1086#1103#1085#1080#1077' ('#1082#1084')'
        DataBinding.FieldName = 'distance_to_center_km'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Editing = False
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soDescending
        Width = 116
      end
      object cxGrid1DBTableView2length: TcxGridDBColumn
        Caption = #1055#1088#1086#1090#1103#1078#1077#1085#1085#1086#1089#1090#1100' ('#1082#1084')'
        DataBinding.FieldName = 'length_km'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 116
      end
      object cxGrid1DBTableView2radius: TcxGridDBColumn
        Caption = #1056#1072#1076#1080#1091#1089' '#1082#1088#1080#1074#1080#1079#1085#1099' ('#1082#1084')'
        DataBinding.FieldName = 'radius_km'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 130
      end
      object cxGrid1DBTableView2DBColumn1: TcxGridDBColumn
        Caption = #1040#1073#1089' '#1087#1088#1086#1089#1074#1077#1090' [m]'
        DataBinding.FieldName = 'ABS_PROSVET'
        Width = 113
      end
      object col_signal_depression: TcxGridDBColumn
        Caption = #1054#1089#1083#1072#1073#1083#1077#1085#1080#1077' [dB]'
        DataBinding.FieldName = 'signal_depression_db'
        Width = 96
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView2
    end
  end
  object DataSource1: TDataSource
    DataSet = qry_Temp
    Left = 48
    Top = 8
  end
  object qry_Temp: TADOQuery
    LockType = ltReadOnly
    Parameters = <>
    Left = 48
    Top = 52
  end
end
