unit u_Link_add;

interface
uses SysUtils,

  dm_Onega_DB_data,


  u_const_db,

  u_func,
  u_db,

  u_classes;


type

 //---------------------------------------
  TLinkAdd = class
  //---------------------------------------
    NewName: string;
    Project_ID: integer;

    Folder_ID: integer;  //???
    ClutterModelID: integer;

    Status_ID : Integer;

    Comments : string;

 //   CalcDirection: integer;  //����������� ���������� �������

    // linkends ---------------------------
    LinkEnd1_ID: Integer;
    LinkEnd2_ID: integer;

    // PMP --------------------------------
    PmpSectorID  : Integer;
    PmpTerminalID: Integer;
                           
    NAME_SDB : string;
    NAME_TN  : string;

  public
    function Add(aIDList: TIDList = nil): integer;
  end;


implementation

// ---------------------------------------------------------------
function TLinkAdd.Add(aIDList: TIDList = nil): integer;
// ---------------------------------------------------------------
var
  sError: string;
  v: Variant;
begin
  Assert(NewName<>'');
  Assert(Project_ID>0, 'ProjectID <=0');


 // Assert(Length_M>0,   'Length_M <=0');

  if (LinkEnd1_ID>0) then
    Assert(LinkEnd2_ID>0, 'LinkEnd1_ID>0 and LinkEnd2_ID=0');


//   and (LinkEnd2_ID>0) then


  if (LinkEnd1_ID>0) and (LinkEnd2_ID>0) then
    Assert(LinkEnd1_ID<>LinkEnd2_ID, ' LinkEnd1_ID=LinkEnd2_ID');

  Result:=dmOnega_DB_data.ExecStoredProc_ ( SP_Link_ADD,
  
//  Result:=dmOnega_DB_data.OpenStoredProc( dmOnega_DB_data.ADOStoredProc1,    SP_Link_ADD1,

//  Result:=db_ExecStoredProc(aADOConnection, SP_Link_ADD,
           [

           FLD_PROJECT_ID,       Project_ID,
           FLD_NAME,             NewName,

           FLD_Folder_ID,        IIF_NULL(Folder_ID),

        //   db_Par(FLD_FOLDER_ID,        IIF_NULL(FolderID)),
           FLD_CLUTTER_MODEL_ID, IIF_NULL(ClutterModelID),

           FLD_LINKEND1_ID,      IIF_NULL(LinkEnd1_ID),
           FLD_LINKEND2_ID,      IIF_NULL(LinkEnd2_ID),

        //   db_Par(FLD_PMP_SITE_ID,      IIF_NULL(PmpSiteID)),
           FLD_PMP_SECTOR_ID,    IIF_NULL(PmpSectorID),
           FLD_PMP_TERMINAL_ID,  IIF_NULL(PmpTerminalID),

           //new
       //    db_Par(FLD_LENGTH_M,         Trunc(Length_M)),

           FLD_Status_ID,       IIF_NULL(Status_ID),


      //     db_Par(FLD_calc_direction,   aRec.CalcDirection),

           FLD_Comments, IIF_NULL(Comments),

           FLD_NAME_SDB, IIF_NULL(NAME_SDB),
           FLD_NAME_TN,  IIF_NULL(NAME_TN)
           ]);


  sError := Format('Result: %d', [Result]) + CRLF +
            //Result
            Format('Name: %s', [NewName]) + CRLF +
            Format('Project_ID:  %d', [Project_ID])  +CRLF +

            Format('CLUTTER_MODEL_ID:  %d', [ClutterModelID])  +CRLF +


            Format('LinkEnd1_ID: %d', [LinkEnd1_ID]) +CRLF +
            Format('LinkEnd2_ID: %d', [LinkEnd2_ID]) +CRLF +

            Format('Folder_ID:   %d', [Folder_ID])   +CRLF ;


//  Assert(Result>0, 'function TLinkAdd.Add(aIDList: TIDList = nil): integer;   '+ CRLF +
//                   ' SP_Link_ADD.Result <=0 ' + CRLF + sError);


 //////// v:=dmOnega_DB_data.GetFieldValue(TBL_LINK, Result, FLD_CALC_METHOD);




  if Assigned(aIDList) then
    aIDList.AddObjName(Result, 'LINK');


end;

end.