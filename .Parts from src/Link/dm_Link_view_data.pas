unit dm_Link_view_data;

interface

uses
  Forms, Classes, SysUtils, DB, ADODB, Dialogs, Variants,

  u_link_Classes1,

  u_func,

  u_assert,

  u_db,

  u_geo,
  u_const_db,

  u_Log,

  dm_Onega_DB_data, dxmdaset
  ;

type
  TdmLink_view_data = class(TDataModule)
    ADOStoredProc_Link: TADOStoredProc;
    ds_LInk: TDataSource;
    ADOStoredProc_Left: TADOStoredProc;
    ADOStoredProc_Right: TADOStoredProc;
    q_LInk_repeater: TADOQuery;
    ds_LInk_repeater: TDataSource;
    q_LInk_repeater_ant: TADOQuery;
    ds_LInk_repeater_ant: TDataSource;
    ds_reflection_POINTS: TDataSource;
    qry_reflection_POINTS: TADOQuery;
    qry_LinkEnd1: TADOQuery;
    qry_LinkEnd2: TADOQuery;
    ds_LinkEnd2: TDataSource;
    ds_LinkEnd1: TDataSource;
    qry_Antennas1: TADOQuery;
    qry_Antennas2: TADOQuery;
    ds_TN_Links: TDataSource;
    mem_TN_links: TdxMemData;
    ds_Results: TDataSource;
    qry_Results: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure ds_LInkDataChange(Sender: TObject; Field: TField);
  private
  //  FID: Integer;
    FFieldNamesModified_Link: TStringList;
  public
    DBLink: TDBLink_;

    function Get_ID: Integer;

    class procedure Init;

    procedure LoadClassFromData;
    procedure Load_Link_repeater_FromClass;

    procedure OpenData(aID: Integer);
    procedure OpenData_Link_repeater1(aID: Integer);

    function Set_Link_repeater(aID: Integer): Boolean;

    procedure Test;
  end;


var
  dmLink_view_data: TdmLink_view_data;

implementation
{$R *.dfm}




class procedure TdmLink_view_data.Init;
begin
  if not Assigned(dmLink_view_data) then
    dmLink_view_data := TdmLink_view_data.Create(Application);

end;

// ---------------------------------------------------------------
procedure TdmLink_view_data.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  FFieldNamesModified_Link := TStringList.Create();
  FFieldNamesModified_Link.Duplicates := dupIgnore;
  FFieldNamesModified_Link.Sorted := True; ///!!!!!


  DBLink := TDBLink_.Create();



end;


procedure TdmLink_view_data.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(DBLink);
  FreeAndNil(FFieldNamesModified_Link);
end;


procedure TdmLink_view_data.ds_LInkDataChange(Sender: TObject; Field: TField);
begin
  if not Assigned(Field) then
    Exit;

  FFieldNamesModified_Link.Add(Field.FieldName);

  g_Log.SysMsg('FFieldNamesModified_Link--'+ Field.FieldName);
end;


function TdmLink_view_data.Get_ID: Integer;
begin
  Result := DBLink.ID;;
end;



// ---------------------------------------------------------------
procedure TdmLink_view_data.OpenData(aID: Integer);
// ---------------------------------------------------------------
const
  SQL_SELECT_reflection_POINTS =
    'SELECT * FROM '+ TBL_Link_reflection_points + ' WHERE (link_id=:link_id)';  // ORDER BY distance_km';

var
 // bHas_Repeater: Boolean;
  i: Integer;
  iID: Integer;
  iLink_repeater_ID: Integer;
  k: Integer;
  ///iLink_repeater_ID: Integer;                                                        )
begin
//  Get_ID := aID;



  i:=dmOnega_DB_data.Link_Select(ADOStoredProc_Link, aID);

  if ADOStoredProc_Link.IsEmpty then
  begin
    g_Log.Error('procedure TdmLink_view_data.OpenData(aID: Integer); - ADOStoredProc_Link.IsEmpty');

    exit;
  end;


  Assert (not ADOStoredProc_Link.IsEmpty, 'not ADOStoredProc_Link.IsEmpty - ' + IntToStr(aID));

  i:=ADOStoredProc_Link.RecordCount;

//  DBLink.LoadFromDataset(ADOStoredProc_Link);

  dmOnega_DB_data.OpenQuery_(qry_reflection_POINTS,
        SQL_SELECT_reflection_POINTS, [FLD_LINK_ID, aID]);


  iLink_repeater_ID :=ADOStoredProc_Link.FieldByName(FLD_Link_repeater_ID).AsInteger;

//  bHas_Repeater  :=ADOStoredProc_Link.FieldByName(FLD_has_Repeater).AsBoolean;




  if iLink_repeater_ID > 0 then
  begin

    dmOnega_DB_data.OpenQuery(q_LInk_repeater,
      Format('SELECT * FROM %s WHERE link_id=%d',  [view_LINK_REPEATER, aID]));

    k:=q_LInk_repeater.RecordCount;

    Assert(q_LInk_repeater.RecordCount=1, 'q_LInk_repeater.RecordCount<>1');

    iID  := q_LInk_repeater.FieldByName(FLD_ID).AsInteger;

    dmOnega_DB_data.OpenQuery(q_LInk_repeater_ant,
       Format('SELECT * FROM %s WHERE Link_repeater_id=%d',  [VIEW_LINK_REPEATER_ANTENNA, iID]) );

//    q_LInk_repeater_ant.Open;

  // iID  := q_LInk_repeater.FieldByName(FLD_ID).AsInteger;

   //  db_View(q_LInk_repeater);

  //  dmOnega_DB_data.OpenQuery(q_LInk_repeater_ant,
   //   Format('SELECT * FROM %s WHERE Link_repeater_id=%d',  [VIEW_LINK_REPEATER_ANTENNA, iID]));


    Assert(q_LInk_repeater_ant.RecordCount = 2, 'q_LInk_repeater_ant.RecordCount = 2');


  end;

  LoadClassFromData();
end;


// ---------------------------------------------------------------
procedure TdmLink_view_data.LoadClassFromData;
// ---------------------------------------------------------------

//var
//  i: Integer;

begin
//  FID := aID;

  DBLink.LoadFromDataset(ADOStoredProc_Link);

  if DBLink.LinkRepeater.ID>0 then
    Load_Link_repeater_FromClass;


end;

// ---------------------------------------------------------------
procedure TdmLink_view_data.Load_Link_repeater_FromClass;
// ---------------------------------------------------------------
begin
  if DBLink.LinkRepeater.ID>0 then
  begin
    DBLink.LinkRepeater.LoadFromDataset(q_LInk_repeater);

    Assert(DBLink.LinkRepeater.Property_id <> DBLink.Property1_id, 'DBLink.LinkRepeater.Property_id <> DBLink.Property1_id');
    Assert(DBLink.LinkRepeater.Property_id <> DBLink.Property2_id, 'DBLink.LinkRepeater.Property_id <> DBLink.Property2_id');



    DBLink.LinkRepeater.Part1.BLVector := MakeBLVector(DBLink.LinkRepeater.Part1.Property_Pos, DBLink.LinkRepeater.Property_Pos);  
    DBLink.LinkRepeater.Part2.BLVector := MakeBLVector(DBLink.LinkRepeater.Property_Pos, DBLink.LinkRepeater.Part2.Property_Pos);


    DBLink.LinkRepeater.Part1.Distance_KM  := geo_Distance_km(DBLink.LinkRepeater.Part1.BLVector);
    DBLink.LinkRepeater.Part2.Distance_KM  := geo_Distance_km(DBLink.LinkRepeater.Part2.BLVector);

    Assert(DBLink.LinkRepeater.Part1.Distance_KM > 0);
    Assert(DBLink.LinkRepeater.Part2.Distance_KM > 0);


    Assert(q_LInk_repeater_ant.RecordCount = 2, 'q_LInk_repeater_ant.RecordCount = 2');

    q_LInk_repeater_ant.First;
    DBLink.LinkRepeater.Part1.Antenna_Height  := q_LInk_repeater_ant.FieldByName(FLD_Height).AsFloat;

    q_LInk_repeater_ant.Next;
    DBLink.LinkRepeater.Part2.Antenna_Height  := q_LInk_repeater_ant.FieldByName(FLD_Height).AsFloat;

  end;

end;



// ---------------------------------------------------------------
function TdmLink_view_data.Set_Link_repeater(aID: Integer): Boolean;
// ---------------------------------------------------------------
//    Assert(DBLink.LinkRepeater.Property_id <> DBLink.Property1_id);
//    Assert(DBLink.LinkRepeater.Property_id <> DBLink.Property2_id);

var
  k: Integer;
begin
//  FID := aID;
  k:=dmOnega_DB_data.ExecStoredProc_('sp_Link_Set_link_repeater',
             [FLD_ID, DBLink.ID ,
             FLD_link_repeater_ID, aID
             ]);

 // Assert(k<>0);
  g_Log.Add( Format('sp_Link_Set_link_repeater:  %d', [k]));

  if k>=0 then
  begin
    DBLink.LinkRepeater.ID := aID;

    OpenData_Link_repeater1(DBLink.LinkRepeater.ID);
    Load_Link_repeater_FromClass(); //(aID: Integer);


//    OpenData_Link_repeater(DBLink.LinkRepeater.Get_ID);
//          Load_Link_repeater_FromClass(); //(aID: Integer);

  end else
    DBLink.LinkRepeater.ID := 0;




  result := k>=0;

        {

  DBLink.LinkRepeater.Get_ID := aID;

  if aID>0 then
  begin
    k:=dmOnega_DB_data.ExecStoredProc('sp_Link_Set_link_repeater',
             [db_Par(FLD_ID, DBLink.Get_ID ),
             db_Par(FLD_link_repeater_ID, aID ),
             ]);

    db_UpdateRecord(ADOStoredProc_Link, FLD_Link_repeater_id, aID);

    dmOnega_DB_data.ExecCommand(
      Format('UPDATE %s set %s=%d WHERE Get_ID=%d',
        [TBL_LINK, FLD_Link_repeater_id, aID, DBLink.Get_ID]));

  end else begin
    db_UpdateRecord(ADOStoredProc_Link, FLD_Link_repeater_id, NULL);

    dmOnega_DB_data.ExecCommand(
      Format('UPDATE %s set %s=NULL WHERE Get_ID=%d',
        [TBL_LINK, FLD_Link_repeater_id, DBLink.Get_ID]));
  end;

 // OpenData_Link_repeater(aID);


  OpenData_Link_repeater(DBLink.LinkRepeater.Get_ID);

  Load_Link_repeater_FromClass(); //(aID: Integer);
   }

end;


// ---------------------------------------------------------------
procedure TdmLink_view_data.OpenData_Link_repeater1(aID: Integer);
// ---------------------------------------------------------------
begin
  if aID = 0 then
  begin
    q_LInk_repeater.Close;
    q_LInk_repeater_ant.Close;

  end else
  begin
    dmOnega_DB_data.OpenQuery(q_LInk_repeater,
      Format('SELECT * FROM %s WHERE ID=%d',   [VIEW_LINK_REPEATER, aID]));
         //  [TBL_LINK_REPEATER, aID]));

  //  Assert(q_LInk_repeater.RecordCount>0);

    dmOnega_DB_data.OpenQuery(q_LInk_repeater_ant,
      Format('SELECT * FROM %s WHERE Link_repeater_id=%d',   [VIEW_LINK_REPEATER_ANTENNA, aID]));

  end;

end;


procedure TdmLink_view_data.Test;
begin
  ShowMessage(FFieldNamesModified_Link.Text);
end;


end.

