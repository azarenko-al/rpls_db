unit fr_dlg_Link_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ToolWin,
  StdCtrls, ExtCtrls,  ComCtrls, Menus, rxToolEdit,  Db, RxMemDS, Grids, DBGrids,
   Mask, ActnList, ADODB, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid , cxGraphics, cxVGrid, cxInplaceContainer, cxButtonEdit,

  u_ComboBox_AutoComplete,

  d_LinkEnd_add,


  dm_Link_add_data,


 // I_Act_LinkEnd,  //dm_Act_LinkEnd,
 // I_Act_Explorer, //dm_Act_Explorer,
                 
  I_Shell,
  dm_Main,

  u_types,

  u_const_db,
  u_const,

  u_dlg,
  u_db,
  u_cx_VGrid,
  u_func,
  u_Geo,

  dm_LinkEnd,
  dm_Property,
  dm_Link,


   AppEvnts, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData

  ;

type
  TOnLinkEndChangedEvent = procedure(Sender: TObject; aLinkend_ID: Integer) of object;

  TLinkEnd_info_rec = record
    LinkEnd_ID : Integer;

    POWER_dBm : double;
    tx_freq_MHz : double;
    rx_freq_MHz : double;
    threshold_ber_6 : double;
    threshold_ber_3 : Double;

    Gain : double;

    LINKENDType_ID : Integer;

    Property_ID : Integer;
    Property_BLPoint : TBLPoint;

  end;




  Tframe_Link_add = class(TForm)
    ApplicationEvents1: TApplicationEvents;
    ds_LinkEnds: TDataSource;
    ToolBar1: TToolBar;

    ActionList1: TActionList;
    act_Add_LinkEnd: TAction;
    PopupMenu1: TPopupMenu;
    actAddLinkEnd1: TMenuItem;
    qry_LinkEnds: TADOQuery;
    GroupBox1: TGroupBox;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    col_Name: TcxGridDBColumn;
    col_ID: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    ToolBar2: TToolBar;
    ToolButton1: TToolButton;
    col_link_count: TcxGridDBColumn;
    col_link_ID: TcxGridDBColumn;
    col_LinkEndType_name: TcxGridDBColumn;
    col_LinkEndType_mode: TcxGridDBColumn;
    GroupBox2: TGroupBox;
    ComboBox1: TComboBox;
    qry_Prop: TADOQuery;
    b_Refresh: TButton;
    act_Reload: TAction;
    b_Select: TButton;
    act_Select_Property: TAction;
    act_Open_Property: TAction;
    StatusBar1: TStatusBar;
    procedure ApplicationEvents1ActionUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
     procedure act_Add_LinkEndExecute(Sender: TObject);
    procedure b_RefreshClick(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
    procedure cxGrid1DBTableView1CustomDrawCell(Sender: TcxCustomGridTableView;
        ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone:
        Boolean);
    procedure cxGrid1DBTableView1FocusedRecordChanged(Sender: TcxCustomGridTableView;
        APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
        ANewItemRecordFocusingChanged: Boolean);
//    procedure qry_LinkEndsAfterScroll(DataSet: TDataSet);
//    procedure row_PropEditPropertiesButtonClick(Sender: TObject;
 //     AButtonIndex: Integer);
  private
    FOnChange: TNotifyEvent;

    FComboBoxAutoComplete : TComboBoxAutoComplete;

    FLinkEndID: integer;
    FOnLinkEndChanged: TOnLinkEndChangedEvent;

//    procedure ComboBox1KeyPress(Sender: TObject; var Key: Char);
    procedure Open_PropertyLinkEnds (aPropertyID: integer);// aDataset: TDataset);

    procedure SelectProperty ();
    procedure Dlg_Select_Property;
    procedure Update_PROPERTY;
  public
    PropertyID: integer;

    Params: record
      Property_BLPos: TBLPoint;
    end;


    Index : Integer;

    IsAllowAppend: boolean;

    function GetLinkEnd_info_rec(var aRec: TLinkEnd_info_rec): Boolean;

    function GetLinkEndID: integer;
    function GetPropertyPos: TBLPoint;
    function GetPropertyName: string;

    procedure Init;

    procedure DoOnSelected(Sender: TObject; aID: integer);


    property OnLinkEndChanged: TOnLinkEndChangedEvent read FOnLinkEndChanged write
        FOnLinkEndChanged;

    property  OnChange: TNotifyEvent read FOnChange write FOnChange;

  end;


//===================================================================
implementation
{$R *.dfm}

uses dm_Act_Explorer,
     dm_Act_LinkEnd;

const
  FLD_LINK_COUNT = 'LINK_COUNT';


//-------------------------------------------------------------------
procedure Tframe_Link_add.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
//  Assert(Assigned(ILinkEnd));
  //Assert(Assigned(IShell));

  inherited;

  cxGrid1.Align:=alClient;

  GroupBox1.Align:=alClient;

  db_SetComponentADOConnection(Self, dmMain.ADOConnection);

//  cx_InitVerticalGrid (cxVerticalGrid1);


  FComboBoxAutoComplete := TComboBoxAutoComplete.Create(GroupBox2);

  FComboBoxAutoComplete.Parent:=GroupBox2;

  FComboBoxAutoComplete.AssignComboBox (ComboBox1);

  FComboBoxAutoComplete.OnSelected:=DoOnSelected;
//  ComboBox1KeyPress;

//
//
//procedure TForm5.ComboBox1KeyPress(Sender: TObject; var Key: Char);
//begin
//  if Key= Chr(VK_RETURN)  then
//    ShowMessage('');
//
//end;



(*  FComboBoxAutoComplete.Left:= ComboBox1.Left;
  FComboBoxAutoComplete.Top:= ComboBox1.Top;
  FComboBoxAutoComplete.Width:= ComboBox1.Width;

  FComboBoxAutoComplete.Anchors:= ComboBox1.Anchors;

*)

  ComboBox1.Visible := False;

 // FComboBoxAuto.StoredItems.Assign(ListBox1.Items);


 Update_PROPERTY;


end;


//procedure Tframe_Link_add.DoOnSelectedEvent(Sender: TObject; aID: integer);
//begin
//  //
//end;



procedure Tframe_Link_add.DoOnSelected(Sender: TObject; aID: integer);
begin
  PropertyID:=aID;

//  Update_PROPERTY();

  Open_PropertyLinkEnds (PropertyID);//, mem_LinkEnds);

  StatusBar1.SimpleText:= 'prop: '+ IntToStr(PropertyID);



  //ShowMessage('DoOnSelected : '+ IntToStr(aID));
end;



//
//procedure Tframe_Link_add.ComboBox1KeyPress(Sender: TObject; var Key: Char);
//begin
//  if Key= Chr(VK_RETURN)  then
//    Update_PROPERTY();
//
//end;
//


//--------------------------------------------------------------------
procedure Tframe_Link_add.Init;
//--------------------------------------------------------------------
var
  blPos: TBLPoint;
  sName : string;

begin
//  blPos:=Params.Property_BLPos;

  PropertyID:=dmProperty.GetNearestIDandPos (Params.Property_BLPos, blPos, sName);
  if (PropertyID>0) then
  begin
//    ed_Property_Name.Text:=dmProperty.GetNameByID (PropertyID);
//    row_Prop.Properties.Value:=sName; //dmProperty.GetNameByID (PropertyID);         PropertyID

    FComboBoxAutoComplete.Text:=sName;


    //ed_Property_Name.Text;

//    row_Pos.Properties.Value:=geo_FormatBLPoint (blPos);
            //geo_FormatBLPoint (Params.Property_BLPos);;

  //  ed_Pos.Text:=geo_FormatBLPoint (Params.Property_BLPos);

   // dmLinkEnd_View.
    Open_PropertyLinkEnds (PropertyID);  //, mem_LinkEnds);
  end;

end;


//--------------------------------------------------------------------
procedure Tframe_Link_add.Open_PropertyLinkEnds (aPropertyID: integer); // aDataset: TDataset);
//--------------------------------------------------------------------
const
  view_LinkEnd_for_Link_add = 'view_LinkEnd_for_Link_add';

  SQL_SELECT =
    'SELECT * FROM '+ view_LinkEnd_for_Link_add + ' WHERE property_id=%d';

begin
  db_OpenQuery (qry_LinkEnds, Format(SQL_SELECT, [PropertyID]));

end;



//--------------------------------------------------------------------
procedure Tframe_Link_add.ApplicationEvents1ActionUpdate(Action: TBasicAction; var Handled: Boolean);
//--------------------------------------------------------------------
begin

  if (not qry_LinkEnds.IsEmpty) and
    // (mem_LinkEnds[FLD_TYPE]=OBJ_LINKEND) and
//     (qry_LinkEnds.FieldByName(FLD_IS_LINK_USED).AsBoolean = False)
     (qry_LinkEnds.FieldByName(FLD_LINK_ID).AsInteger = 0)
//     (qry_LinkEnds.FieldByName(FLD_LINK_COUNT).AsInteger = 0)

  then FLinkEndID:=qry_LinkEnds.FieldByName(FLD_ID).AsInteger
  else FLinkEndID:=0;

  IsAllowAppend:=(FLinkEndID<>0);

//  act_Add_LinkEnd.Enabled:=row_Prop.Properties.Value <> '';


  act_Add_LinkEnd.Enabled:= PropertyID > 0;

end;


//-------------------------------------------------------------------
function Tframe_Link_add.GetLinkEndID: integer;
//-------------------------------------------------------------------
begin
//  Result.Property_ID:=PropertyID;
 // Result.Property_BLPos:=dmProperty.GetPos (PropertyID);
//  Result:=FLinkEndID;

  if (not qry_LinkEnds.IsEmpty) and
    // (mem_LinkEnds[FLD_TYPE]=OBJ_LINKEND) and
//     (qry_LinkEnds.FieldByName(FLD_IS_LINK_USED).AsBoolean = False)
     (qry_LinkEnds.FieldByName(FLD_LINK_ID).AsInteger = 0)

  then Result:=qry_LinkEnds.FieldByName(FLD_ID).AsInteger
  else Result:=0;

end;

//-------------------------------------------------------------------
function Tframe_Link_add.GetPropertyPos: TBLPoint;
//-------------------------------------------------------------------
begin
  Result:=dmProperty.GetPos(PropertyID);
end;

//-------------------------------------------------------------------
function Tframe_Link_add.GetPropertyName: string;
//-------------------------------------------------------------------
begin
  Result:=dmProperty.GetNameByID (PropertyID);
end;

//-------------------------------------------------------------------
procedure Tframe_Link_add.SelectProperty ();
//-------------------------------------------------------------------
var sName: Widestring;
  iID: integer;
begin
  if dmAct_Explorer.Dlg_Select_Object(otProperty, PropertyID, sName) then
  begin
    FComboBoxAutoComplete.Text:= sName;
//    row_Prop.Properties.Value := sName;

  //  Params.Property_BLPos:=dmProperty.GetPos(PropertyID);
//    ed_Pos.Text:=geo_FormatBLPoint (Params.Property_BLPos);
  end;

  //dmLinkEnd_View.
  Open_PropertyLinkEnds (PropertyID);//, mem_LinkEnds);

  if assigned(FOnChange) then
    FOnChange(Self);
end;

//-------------------------------------------------------------------
procedure Tframe_Link_add.act_Add_LinkEndExecute(Sender: TObject);
//-------------------------------------------------------------------


    //--------------------------------------------------------------------
    function DoLinkEndAdd: Integer;
    //--------------------------------------------------------------------
    var
      rec: Tdlg_LinkEnd_add_rec;
    begin
      FillChar(rec, SizeOf(rec), 0);

     //  rec.FolderID  :=aFolderID;
     //  rec.BLPoint   :=FBLPoint;
      rec.PropertyID:=PropertyID;

      rec.ChannelType:= IIF (Index=1, 'low', 'high');


      Result:=Tdlg_LinkEnd_add.ExecDlg_LinkEnd(rec);//aFolderID, FBLPoint, FProperty);

      if Result>0 then
        Open_PropertyLinkEnds (PropertyID);

    end;


begin
  if (Sender=act_Add_LinkEnd) and (PropertyID>0) then
  begin
    DoLinkEndAdd;
   // dmLinkEnd_View.
   // Open_PropertyLinkEnds (PropertyID);//, mem_LinkEnds);
  end;


  if (Sender=act_Select_Property) then
  begin
    Dlg_Select_Property();
   // dmAct_LinkEnd.Add (0, PropertyID);

   // dmLinkEnd_View.
  //  Open_PropertyLinkEnds (PropertyID);//, mem_LinkEnds);
  end;
//    dmAct_LinkEnd.Add (0, PropertyID);
   // dmLinkEnd_View.
  //  Open_PropertyLinkEnds (PropertyID);//, mem_LinkEnds);
//  end;


//  act_Add_LinkEndExecute


end;

// ---------------------------------------------------------------
procedure Tframe_Link_add.Update_PROPERTY;
// ---------------------------------------------------------------
const
//  view_LinkEnd_for_Link_add = 'view_LinkEnd_for_Link_add';

  SQL_SELECT =
    'SELECT * FROM '+ tbl_PROPERTY + ' WHERE project_id=%d';

var
  iID: Integer;
  sName: string;
begin
 //   TdmLink_add_data.Init;
  //dmLink_add_data.Open();


  dmLink_add_data.Update_PROPERTY(FComboBoxAutoComplete.Items);
  
{

  db_OpenQuery (qry_Prop, Format(SQL_SELECT, [dmMain.ProjectID]));


 // comboBox1.Beg

  LockWindowUpdate(comboBox1.Handle);

  with qry_Prop do
    while not EOF do
  begin
    iID  := FieldByName(FLD_ID).AsInteger;
    sName:= FieldByName(FLD_NAME).AsString;

    FComboBoxAutoComplete.Items.AddObject(sName, Pointer(iID));

    Next;
  end;


  LockWindowUpdate(0);

}
  FComboBoxAutoComplete.StoredItems.Assign(FComboBoxAutoComplete.Items);


end;

//------------------------------------------------------------------------------
procedure Tframe_Link_add.b_RefreshClick(Sender: TObject);
//------------------------------------------------------------------------------
var
 // k: Integer;
 // k1: Integer;
  iID: Integer;
begin
   iID:=FComboBoxAutoComplete.GetID();

//   k1:= FComboBoxAutoComplete.Items.Count;
//   k2:= FComboBoxAutoComplete.ItemIndex;


   if FComboBoxAutoComplete.ItemIndex>=0 then
   begin
     PropertyID:=iID;
//      Integer( FComboBoxAutoComplete.Items.Objects[FComboBoxAutoComplete.ItemIndex]);

     Assert(PropertyID>0);   

     Open_PropertyLinkEnds (PropertyID);

   end;



//  Update_PROPERTY

end;

//
//procedure Tframe_Link_add.Button2Click(Sender: TObject);
//begin
////?/Select_Property
//end;


procedure Tframe_Link_add.cxGrid1DBTableView1CustomDrawCell(Sender:
    TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo:
    TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
//  if AViewInfo.GridRecord.Values[col_link_count.Index]>0 then
  if AViewInfo.GridRecord.Values[col_link_id.Index]>0 then
    ACanvas.Font.Style :=ACanvas.Font.Style + [fsStrikeout];

end;

procedure Tframe_Link_add.cxGrid1DBTableView1FocusedRecordChanged(Sender:
    TcxCustomGridTableView; APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
    ANewItemRecordFocusingChanged: Boolean);
begin
  if Assigned(FOnLinkEndChanged) then
    FOnLinkEndChanged (Self, qry_LinkEnds.FieldByName(FLD_ID).AsInteger);


//  ShowMessage('cxGrid1DBTableView1FocusedRecordChanged');
end;


// ---------------------------------------------------------------
function Tframe_Link_add.GetLinkEnd_info_rec(var aRec: TLinkEnd_info_rec):
    Boolean;
// ---------------------------------------------------------------
begin
  Result := qry_LinkEnds.Active and (qry_LinkEnds.RecordCount>0);

  if Result then
    with qry_LinkEnds do
    begin
      aRec.Property_ID      :=FieldByName(FLD_Property_ID).AsInteger;
      aRec.Property_BLPoint :=db_ExtractBLPoint(qry_LinkEnds);

      aRec.LinkEnd_ID      :=FieldByName(FLD_ID).AsInteger;
      aRec.POWER_dBm       :=FieldByName(FLD_POWER_dBm).AsFloat;
      aRec.tx_freq_MHz     :=FieldByName(FLD_tx_freq_MHz).AsFloat;
      aRec.rx_freq_MHz     :=FieldByName(FLD_rx_freq_MHz).AsFloat;
      aRec.threshold_ber_6 :=FieldByName(FLD_threshold_ber_6).AsFloat;
      aRec.threshold_ber_3 :=FieldByName(FLD_threshold_ber_3).AsFloat;
      aRec.Gain            :=FieldByName(FLD_Gain).AsFloat;

      aRec.LinkEndType_ID  :=FieldByName(FLD_LinkEndType_ID).AsInteger;
    end;

end;



// ---------------------------------------------------------------
procedure Tframe_Link_add.Dlg_Select_Property;
// ---------------------------------------------------------------
var
  sNAme: Widestring;
begin

//    if dmAct_Explorer.Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otProperty, PropertyID) then
    if dmAct_Explorer.Dlg_Select_Object (otProperty, PropertyID, sNAme) then
    begin
   //   Params.Property_BLPos:=dmProperty.GetPos (PropertyID);

    //  row_Pos.Properties.Value:=geo_FormatBLPoint (Params.Property_BLPos);;
  //  ed_Pos.Text:=geo_FormatBLPoint (Params.Property_BLPos);

   // dmLinkEnd_View.
      Open_PropertyLinkEnds (PropertyID);//, mem_LinkEnds);
    end;


   // xx  row_Prop.Properties.Value:=ed_Property_Name.Text;
    //  row_Pos.Properties.Value:=geo_FormatBLPoint (Params.Property_BLPos);;

  //  end;
end;


end.

     {



//
//procedure Tframe_Link_add.qry_LinkEndsAfterScroll(DataSet: TDataSet);
//begin
// // ShowMessage('qry_LinkEndsAfterScroll');
//end;



procedure Tframe_Link_add.row_PropEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);

begin
//
//  //-------------------------------------------------------------------
//  if cxVerticalGrid1.FocusedRow=row_Prop then
//  //-------------------------------------------------------------------
//    if dmAct_Explorer.Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otProperty, PropertyID) then
//    begin
//   //   Params.Property_BLPos:=dmProperty.GetPos (PropertyID);
//
//    //  row_Pos.Properties.Value:=geo_FormatBLPoint (Params.Property_BLPos);;
//  //  ed_Pos.Text:=geo_FormatBLPoint (Params.Property_BLPos);
//
//   // dmLinkEnd_View.
//      Open_PropertyLinkEnds (PropertyID);//, mem_LinkEnds);
//    end;


   // xx  row_Prop.Properties.Value:=ed_Property_Name.Text;
    //  row_Pos.Properties.Value:=geo_FormatBLPoint (Params.Property_BLPos);;

  //  end;
end;

