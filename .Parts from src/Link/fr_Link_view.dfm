inherited frame_Link_View: Tframe_Link_View
  Left = 882
  Top = 182
  Width = 718
  Height = 435
  Caption = 'frame_Link_View'
  KeyPreview = True
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Caption: TPanel [0]
    Width = 710
    TabOrder = 0
  end
  inherited Panel1: TPanel [1]
    Width = 710
    TabOrder = 1
  end
  inherited TBDock1: TTBDock
    Width = 710
  end
  inherited pn_Main: TPanel
    Width = 710
    Height = 254
    object PageControl1: TPageControl
      Left = 1
      Top = 4
      Width = 708
      Height = 157
      ActivePage = TabSheet_Inspector
      Align = alTop
      Style = tsFlatButtons
      TabOrder = 0
      OnChange = PageControl1Change
      object TabSheet_Inspector: TTabSheet
        Caption = #1057#1074#1086#1081#1089#1090#1074#1072
      end
      object TabSheet_Profile: TTabSheet
        Caption = #1055#1088#1086#1092#1080#1083#1100
        ImageIndex = 1
      end
      object TabSheet_CalcRes: TTabSheet
        Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090#1099' '#1088#1072#1089#1095#1077#1090#1072
        ImageIndex = 2
      end
      object TabSheet_Refl_Points: TTabSheet
        Caption = #1058#1086#1095#1082#1080' '#1086#1090#1088#1072#1078#1077#1085#1080#1103
        ImageIndex = 3
      end
      object TabSheet_Reports: TTabSheet
        Caption = #1054#1090#1095#1077#1090#1099
        ImageIndex = 4
      end
      object TabSheet_Repeater: TTabSheet
        Caption = 'Repeater'
        ImageIndex = 7
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 708
      Height = 3
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
    end
  end
  inherited MainActionList: TActionList
    Left = 24
    Top = 0
  end
  inherited ImageList1: TImageList
    Left = 52
    Top = 0
  end
  inherited PopupMenu1: TPopupMenu
    Left = 80
    Top = 0
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 232
    Top = 0
  end
  object FormPlacement1: TFormPlacement
    IniFileName = 'Software\Onega\'
    Left = 264
  end
  object ApplicationEvents1: TApplicationEvents
    OnMessage = ApplicationEvents1Message
    Left = 440
    Top = 8
  end
end
