unit dm_Link_report;

interface

uses
  Classes, Forms,

  u_db,

  u_files,
  

  u_classes,

  dm_Onega_DB_data, ADODB, DB

  ;

type
  TdmLink_report = class(TDataModule)
    ADOStoredProc1: TADOStoredProc;
  private
    { Private declarations }
  public
     class procedure Init;

     procedure Report(aIDList: TIDList);


  end;

var
  dmLink_report: TdmLink_report;

implementation

{$R *.dfm}

class procedure TdmLink_report.Init;
begin
  if not Assigned(dmLink_report) then
   dmLink_report := TdmLink_report.Create(Application);


end;


// ---------------------------------------------------------------
procedure TdmLink_report.Report(aIDList: TIDList);
// ---------------------------------------------------------------
var
  k: Integer;
  s: string;
begin
  Exit;


  s:=aIDList.ValuesToString(',');

  ShellExec_Notepad_temp(s);

  k:=dmOnega_DB_data.OpenStoredProc (ADOStoredProc1, 'sp_Link_report',
               [db_Par(FLD_ID_STR, s)] );

  db_View(ADOStoredProc1);
end;

end.