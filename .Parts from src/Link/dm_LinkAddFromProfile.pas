unit dm_LinkAddFromProfile;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,

  dm_LinkEnd,
  dm_Property,
  dm_Main,

  u_Types,
  u_db,
  u_func,

  u_const_db

  ;

type
  TdmLinkAddFromProfile = class(TDataModule)
  private
 //   function IncStr(aStr: string): string;
  public
    function GetName11(aObjectType: string; aIndex: integer): string;
  end;


function dmLinkAddFromProfile: TdmLinkAddFromProfile;

//=====================================================================
implementation  {$R *.DFM}
//====================================================================

var
  FdmLinkAddFromProfile: TdmLinkAddFromProfile;

  
// ---------------------------------------------------------------
function dmLinkAddFromProfile: TdmLinkAddFromProfile;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLinkAddFromProfile) then
      FdmLinkAddFromProfile := TdmLinkAddFromProfile.Create(Application);

  Result := FdmLinkAddFromProfile;
end;


//---------------------------------------------------------------------
function TdmLinkAddFromProfile.GetName11(aObjectType: string; aIndex: integer):
    string;
//---------------------------------------------------------------------
var
  sName: string;
  i: integer;
begin
  if aObjectType = OBJ_PROPERTY then
    sName:=dmProperty.GetNewName
  else
    sName:=dmLinkEnd.GetNewName(0);

  Result:=sName + Format(' %d', [aIndex]);


  for i := 1 to 300 do
  begin
    if aObjectType = OBJ_PROPERTY then
      if (gl_DB.GetIntFieldValue(TBL_PROPERTY,FLD_ID,
                      [db_Par(FLD_NAME,       Result),
                       db_Par(FLD_PROJECT_ID, dmMain.ProjectID)
                      ]) = 0)
      then exit;


    if aObjectType = OBJ_LINKEND then
      if (gl_DB.GetIntFieldValue(TBL_LINKEND, FLD_ID,
                      [db_Par(FLD_NAME,       Result),
                       db_Par(FLD_PROJECT_ID, dmMain.ProjectID)
                      ]) = 0)
      then exit;

     Result:=sName + Format(' %d %d', [i,aIndex]);
  end;
end;

begin
end.



