unit fr_dlg_Link_add_from_Profile;

interface

uses
  SysUtils, Classes, Controls, Forms, 
  
  Registry,
   Variants,  cxButtonEdit,  cxDropDownEdit,
  cxCheckBox, cxVGrid, 

  d_Geo_Pos,

  u_Property_Add,


  dm_Onega_DB_data,

//  f_Custom,

  u_Storage,

  dm_MapEngine_store,

 // I_Act_Explorer, //dm_Act_Explorer;

//uses
//  dm_Act_Explorer;



  dm_Library,

  
  dm_Act_LinkEndType,

  
  dm_Main,

  //I_MapEngine,
 // dm_MapEngine,

  dm_LinkEndType,

  u_types,

  
  u_cx_VGrid,

  u_dlg,
  
  
  u_func,
  u_Geo,


  u_const_db,
  
  

  u_link_const_str,
  u_link_const,


  dm_Property,
  
  

  dm_LinkEnd,
  

  dm_AntType,


  AppEvnts, cxStyles, cxControls, cxInplaceContainer;

type
  Tframe_Link_add_from_Profile = class(TForm)
    ApplicationEvents1: TApplicationEvents;
    cxVerticalGrid1: TcxVerticalGrid;
    row_Property: TcxEditorRow;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;
    row_Antenna1_Type: TcxEditorRow;
    row_Antenna_Height: TcxEditorRow;
    cxVerticalGrid1CategoryRow3: TcxCategoryRow;
    row_PassiveElement_type: TcxEditorRow;
    row_LinkEnd: TcxCategoryRow;
    row_LinkEnd_Name: TcxEditorRow;
    row_Equipment_Typed: TcxEditorRow;
    row_not_Typed_equipment: TcxCategoryRow;
    row_Freq_GHz: TcxEditorRow;
    row_Power_dBm: TcxEditorRow;
    row__Loss: TcxEditorRow;
    row__Raznos: TcxEditorRow;
    row__Signature_Width: TcxEditorRow;
    row__Signature_Height: TcxEditorRow;
    row_THRESHOLD_BER_3: TcxEditorRow;
    row_THRESHOLD_BER_6: TcxEditorRow;
    row_Typed_equipment: TcxCategoryRow;
    row_LinkEndType: TcxEditorRow;
    row_mode: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    row_Band: TcxEditorRow;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    row_Property_pos: TcxEditorRow;
    row_Property_address: TcxEditorRow;

    procedure ApplicationEvents1ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
//    procedure DoOnButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure FormDestroy(Sender: TObject);
     procedure row_LinkEndType1EditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure row_Equip_TypeEditPropertiesEditValueChanged(
      Sender: TObject);
    procedure row_PropEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxVerticalGrid1StylesGetContentStyle(Sender: TObject;
      AEditProp: TcxCustomEditorRowProperties; AFocused: Boolean;
      ARecordIndex: Integer; var AStyle: TcxStyle);
    procedure row_Property_posEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
//    procedure row_PropEditPropertiesButtonClick(Sender: TObject;
  //    AButtonIndex: Integer);
  private
    FEquipment_Type: integer;
    FPropertyID: integer;

    FPropNumber: string;

    FRegPath: string;
    FLocalRegPath: string;

//    FIsEquipTypped: boBoolean;
//    FIsMakeNewLinkEnd: boolean;
{
    FPower,
    FFreq,
//    FSpeed,
    FRaznos,
    FLoss,

//    FKng2,
//    FModulation_type,
    FSignature_height,
    FSignature_width,
    FSens,
    FSens_6: double;
}
//    FKratnostBySpace,
//    FKratnostByFreq: string;

    Height,
    Power,
    Freq,
    Speed,
    Sens: double;


    FOnEquipmentChanged: TNotifyEvent;

    procedure DoEquipmentChanged;
    procedure DoOnButtonClickNoAction(Sender: TObject; AbsoluteIndex: Integer);
    function GetPropertyNewName: string;
    procedure SetEquipment_Type(Value: integer);
    procedure ShowTypedEquipment(aValue: boolean);
    procedure UpdateAntennaMaxHeight;


  public
    Params: record
      IsPropertyExists: boolean;
      Property_BLPos: TBLPoint;

      Height: double;
      Freq_GHz: double;

    end;

    AntennaTypeID,
    LinkEndTypeID,
    PassiveElementID,
    LinkEndID: Integer;
    Mode: integer;
    LinkEndType_Mode_ID: integer;

//    FolderID,
///////    IsPropExist,
 ////////   IsLinkEndExist: boolean;

    IsAllowAppend1: boolean;

    function GetPropertyName: string;

    function Init(aPropNumber: integer): boolean;
    procedure InitParamsFromRegistry(aPropNumber: string);

    function VerifyInputData(): string;
    property Equipment_Type: integer read FEquipment_Type write SetEquipment_Type;


    function AddLinkEndToDB(aPropertyID: integer): integer;
    function AddProperty1: integer;
    function Get_IsAllowAppend: Boolean;

    property OnEquipmentChanged: TNotifyEvent read FOnEquipmentChanged write FOnEquipmentChanged;

  end;



//===================================================================
//implementation
//===================================================================
implementation {$R *.dfm}


uses
  dm_Act_Explorer;



const
  DEF_EQUIPMENT_TYPED     = 0;
  DEF_EQUIPMENT_NOT_TYPED = 1;



//-------------------------------------------------------------------
procedure Tframe_Link_add_from_Profile.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

 // Assert(Assigned(ILinkEndType));
 // Assert(Assigned(IShell));
 // Assert(Assigned(IMapEngine));


  FRegPath := g_Storage.GetPathByClass(ClassName);


  cxVerticalGrid1.Align:=alClient;

 // pn_Main.Align:=alClient;
//  dx_Typed_Equip.Align:=alClient;

//  row_LinkendType.Properties.on  OnButtonClick        := DoOnButtonClick;
 // row_mode.EditProperties.OnButtonClick               := DoOnButtonClick;
 // row_Antenna1_Type.OnButtonClick      := DoOnButtonClick;
 // row_PassiveElement_type.OnButtonClick:= DoOnButtonClick;

 // dx_Not_Typed_Equip.Visible:= true;
 // dx_Typed_Equip.Visible:=     false;

  row_LinkEndType.Properties.Caption := '���';
  row_Mode.Properties.Caption        := '�����';

 // STR_NET_CODE

{
  if Assigned(row_Code) then
    row_Code.Caption           := STR_NET_CODE;}

  row_Freq_GHz.Properties.Caption              := '1. '+ STR_NOT_TYPED_1_FREQ_GHz;
  row_Power_dBm.Properties.Caption         := '2. '+ STR_NOT_TYPED_2_POWER_dBm;
  row__Loss.Properties.Caption             := '3. '+ STR_NOT_TYPED_3_LOSS_dB;
  row__Raznos.Properties.Caption           := '4. '+ STR_NOT_TYPED_4_RAZNOS_MHz;
  row__signature_width.Properties.Caption  := '5. '+ STR_NOT_TYPED_5_SIGNATURE_WIDTH_MHz;
  row__signature_height.Properties.Caption := '6. '+ STR_NOT_TYPED_6_SIGNATURE_HEIGHT_dB;
  row_THRESHOLD_BER_3.Properties.Caption   := '7. '+ STR_NOT_TYPED_7_SENS_3_dBm;
  row_THRESHOLD_BER_6.Properties.Caption   := '8. '+ STR_NOT_TYPED_8_SENS_6_dBm;

  row_Property_pos.Properties.Caption    :='����������';
  row_Property_address.Properties.Caption:='�����';


//  qry_Temp.Connection:=dmMain.ADOConnection;

  cx_InitVerticalGrid (cxVerticalGrid1);


  TcxComboBoxProperties(
     row_Band.Properties.EditProperties).Items.Assign (dmLIbrary.Bands);

   Assert(Assigned(OnDestroy));


 // Params.IsPropertyExists:=false;
end;


//--------------------------------------------------------------------
procedure Tframe_Link_add_from_Profile.UpdateAntennaMaxHeight;
//--------------------------------------------------------------------
var
  eH: Double;
begin
  eH:=dmOnega_DB_data.Property_GetMaxHeight(FPropertyID);

  if eH>0 then
    row_Antenna_Height.Properties.Value:=eH;
end;


//-------------------------------------------------------------------
procedure Tframe_Link_add_from_Profile.InitParamsFromRegistry(aPropNumber: string);
//-------------------------------------------------------------------
begin
  FPropNumber:= aPropNumber;

  FLocalRegPath:=FRegPath + FPropNumber + '\';


 // cx_VerticalGrid_LoadFromReg(cxVerticalGrid1, FLocalRegPath);


  with TRegIniFile.Create(FLocalRegPath) do
  begin
    //------------------------------------------------------
    LinkEndTypeID      := ReadInteger ('', FLD_LINKENDTYPE_ID, 0);
    LinkEndType_Mode_ID:= ReadInteger ('', FLD_LinkEndType_Mode_ID, 0);
    Mode               := ReadInteger ('', FLD_MODE, 0);

    AntennaTypeID      := ReadInteger ('', FLD_ANTENNATYPE_ID,  0);
    PassiveElementID   := ReadInteger ('', FLD_PASSIVE_ELEMENT_ID,  0);

    //------------------------------------------------------

    row_Band.Properties.Value             := ReadString ('', row_Band.Name, '');
    row_Power_dBm.Properties.Value        := AsFloat(ReadString ('', row_Power_dBm.Name,        '25'));
    row_THRESHOLD_BER_3.Properties.Value  := AsFloat(ReadString ('', row_THRESHOLD_BER_3.Name,  '-80'));
    row_THRESHOLD_BER_6.Properties.Value  := AsFloat(ReadString ('', row_THRESHOLD_BER_6.Name,  '-78'));
    row_Freq_GHz.Properties.Value             := AsFloat(ReadString ('', row_Freq_GHz.Name,            '13'));
    row__Loss.Properties.Value            := AsFloat(ReadString ('', row__Loss.Name,            '0'));
    row__Raznos.Properties.Value          := AsFloat(ReadString ('', row__Raznos.Name,          '0'));
    row__signature_height.Properties.Value:= AsFloat(ReadString ('', row__signature_height.Name,'14'));
    row__signature_width.Properties.Value := AsFloat(ReadString ('', row__signature_width.Name, '24'));

    row_Equipment_Typed.Properties.Value:=  ReadBool ('', row_Equipment_Typed.Name,  false);

    Free;
  end;


 ShowTypedEquipment(row_Equipment_Typed.Properties.Value);

 Equipment_Type:=IIF(row_Equipment_Typed.Properties.Value =True,
                     DEF_EQUIPMENT_TYPED,
                     DEF_EQUIPMENT_NOT_TYPED);

end;


//-------------------------------------------------------------------
procedure Tframe_Link_add_from_Profile.FormDestroy(Sender: TObject);
//-------------------------------------------------------------------
begin
 // cx_VerticalGrid_SaveToReg(cxVerticalGrid1, FLocalRegPath);

//  exit;


  with TRegIniFile.Create(FLocalRegPath) do
  begin
    //------------------------------------------------------
    WriteInteger ('', FLD_LINKENDTYPE_ID,       LinkEndTypeID);
    WriteInteger ('', FLD_LinkEndType_Mode_ID,  LinkEndType_Mode_ID);
    WriteInteger ('', FLD_MODE,               Mode);

 //   := ReadInteger ('', FLD_LinkEndType_Mode_ID, 0);

    WriteInteger ('', FLD_ANTENNATYPE_ID,     AntennaTypeID);
    WriteInteger ('', FLD_PASSIVE_ELEMENT_ID, PassiveElementID);

    //------------------------------------------------------
    WriteString  ('', row_Band.Name,              row_Band.Properties.Value);
    WriteString  ('', row_Power_dBm.Name,         row_Power_dBm.Properties.Value);
    WriteString  ('', row_THRESHOLD_BER_3.Name,   row_THRESHOLD_BER_3.Properties.Value);
    WriteString  ('', row_THRESHOLD_BER_6.Name,   row_THRESHOLD_BER_6.Properties.Value);
    WriteString  ('', row_Freq_GHz.Name,          row_Freq_GHz.Properties.Value);
    WriteString  ('', row__Loss.Name,             row__Loss.Properties.Value);
    WriteString  ('', row__Raznos.Name,           row__Raznos.Properties.Value);
    WriteString  ('', row__signature_height.Name, row__signature_height.Properties.Value);
    WriteString  ('', row__signature_width.Name,  row__signature_width.Properties.Value);

    WriteBool   ('', row_Equipment_Typed.Name,     row_Equipment_Typed.Properties.Value);


//    WriteString  ('', row_Equip_Type.Name,  row_Equip_Type.Text);

    Free;
  end;


  inherited;
end;

//---------------------------------------------------------------------------
function Tframe_Link_add_from_Profile.GetPropertyNewName: string;
//---------------------------------------------------------------------------
var
  k: Integer;
begin
  k:=Random(1000);

  Result:=dmProperty.GetNewName  + Format(' - %d', [k]);

end;


//--------------------------------------------------------------------
function Tframe_Link_add_from_Profile.Init(aPropNumber: integer): boolean;
//--------------------------------------------------------------------
//
//  //------------------------------------
//  procedure DoInitIsPropertyExists();
//  //------------------------------------
//  begin
////    row_Pos.Properties.Value:= geo_FormatBLPoint(Params.Property_BLPos);
//
//  //  if aObjectType = OBJ_PROPERTY then
//    sName:=dmProperty.GetNewName  + Format(' %d', [aIndex]);
//
//    row_Property.Properties.Value:=sName;
//
//    row_Property_pos.Properties.Value:= geo_FormatBLPoint(Params.Property_BLPos);
//
// //   row_LinkEnd_Name.Properties.Value:=
//  //       dmLinkAddFromProfile.GetName1(OBJ_LINKEND, aPropNumber);
//

//  end;




{const
  SQL_SELECT_LINKENDS =
    'SELECT * FROM '+ TBL_LINKEND +
    ' WHERE property_id=:property_id ORDER BY name';
}
var
  s: string;
 // blPos, bl: TBLPoint;
 // oIntArray: TIntArray;
 // iAntTypeID: integer;

  sName: string;

begin
  row_Property_pos.Visible    :=not Params.IsPropertyExists;
  row_Property_address.Visible:=not Params.IsPropertyExists;

  if Params.IsPropertyExists then
    row_Property.Properties.EditPropertiesClass :=TcxButtonEditProperties
  else
    row_Property.Properties.EditPropertiesClass := nil;




  Result:=true;
//  blPos:=Params.Property_BLPos;

 // FPropertyID:=0;
////////////  IsPropExist:=false;
////////////  IsLinkEndExist:=false;

  row_Antenna_Height.Properties.Value:=Params.Height;

  if LinkEndTypeID>0 then
    row_LinkendType.Properties.Value:=dmLinkEndType.GetNameByID(LinkEndTypeID); //qry_Temp[FLD_LINKEND_TYPE_ID]);

  row_Mode.Properties.Value := Mode;


  if PassiveElementID>0 then
    row_PassiveElement_type.Properties.Value:= gl_db.GetNameByID(TBL_PASSIVE_COMPONENT, PassiveElementID);

  if AntennaTypeID>0 then
    row_Antenna1_Type.Properties.Value:= dmAntType.GetNameByID(AntennaTypeID);


//  s:=dmLinkAdd.GetName1(OBJ_LINKEND, aPropNumber);


  row_LinkEnd_Name.Properties.Value:='���';

//  row_LinkEnd_Name.Properties.Value:=dmLinkAddFromProfile.GetName1(OBJ_LINKEND, aPropNumber);


  //--------------------------------------
  if not Params.IsPropertyExists then
  //--------------------------------------
  begin
    sName:=GetPropertyNewName();
//    dmProperty.GetNewName  + Format(' - %d', [aPropNumber]);

    row_Property.Properties.Value:=sName;

    row_Property_pos.Properties.Value:= geo_FormatBLPoint(Params.Property_BLPos)  + ' CK-42';

 //   DoInitIsPropertyExists();

  //  exit;   

  end else
  begin
    FPropertyID:=dmProperty.GetNearestIDandPos (Params.Property_BLPos, Params.Property_BLPos, sName);
    row_Property.Properties.Value:=sName;
  end;
  
{
    if (FPropertyID=0) then
    begin
      LinkEndID:=0;
      Result:=false;
      exit;
    end;
}


 //   row_Property.Properties.Value:=sName;//  dmProperty.GetNameByID (FPropertyID);

  //  dmLinkEnd_View.Open_PropertyLinkEnds (FPropertyID, mem_LinkEnds);

  //zzzzzzzzzzzzzzz  IsPropExist:=true;

//    if (not mem_LinkEnds.IsEmpty) and
//       (mem_LinkEnds.Locate(FLD_IS_LINK_USED, false, [])) then
//{    and
//      (mem_LinkEnds[FLD_TYPE]=OBJ_LINKEND) and
//      (mem_LinkEnds.FieldByName(FLD_IS_LINK_USED).AsBoolean = False)
//    then
// }
//    begin
//      dmOnega_DB_data.OpenQuery(qry_Temp,
//        Format('SELECT * FROM %s WHERE %s=%d',
//               [TBL_LINKEND, FLD_PROPERTY_ID, FPropertyID]));

//          ' '+ TBL_LINKEND );

     // db_OpenQuery(qry_Temp, TBL_LINKEND, FLD_PROPERTY_ID, FPropertyID);


 //     qry_Temp.Locate(FLD_NAME, mem_LinkEnds[FLD_NAME], []);


  //    LinkEndID:=mem_LinkEnds[FLD_ID];
 //     row_LinkEnd_Name.Text:=qry_Temp[FLD_NAME];
  //    row_LinkEnd_Name.Properties.Value:=qry_Temp[FLD_NAME];

//      case qry_Temp.FieldBYName(FLD_EQUIPMENT_TYPE).AsInteger of
//        0: begin
//             Equipment_Type:= DEF_EQUIPMENT_TYPED;
//  //           row_Equip_Type.Text := 'True';
//             row_Equipment_Typed.Properties.Value := true;
//
//             ShowTypedEquipment(True);
//
//
//         //   row_Typed_equipment.Visible:=    True;
//          //  row_not_Typed_equipment.Visible:=False;
//
//            // dx_Not_Typed_Equip.Visible := false;
//            // cx_Typed_Equip.Visible     := true;
//           end;
//        1: begin
//              Equipment_Type:= DEF_EQUIPMENT_NOT_TYPED;
////              row_Equip_Type.Text := 'False';
//              row_Equipment_Typed.Properties.Value := false;
//
//              ShowTypedEquipment(False);
//
//
//           //   row_Typed_equipment.Visible:=    False;
//            //  row_not_Typed_equipment.Visible:=True;
//
////             dx_Not_Typed_Equip.Visible := true;
//  //           cx_Typed_Equip.Visible     := false;
//           end;
//      else
//        raise Exception.Create('');
//      end;


////////////////      IsLinkEndExist  :=true;
//      LinkEndTypeID      :=qry_Temp.FieldBYName(FLD_LINKENDTYPE_ID).AsInteger;
//      LinkEndType_Mode_ID:=qry_Temp.FieldBYName(FLD_LinkEndType_Mode_ID).AsInteger;

       //   := qry_Temp.FieldByName(FLD_MODE).AsInteger;

//      row_LinkendType.Properties.Value:=dmLinkEndType.GetNameByID(LinkEndTypeID); //qry_Temp[FLD_LINKEND_TYPE_ID]);

//      row_mode.Properties.Value       :=qry_Temp.FieldByName(FLD_MODE).AsString;


{      PassiveElementID:=qry_Temp.FieldByName(FLD_PASSIVE_ELEMENT_ID).AsInteger;
      if PassiveElementID = 0 then
        row_PassiveElement_type.Properties.Value:= ''
      else
}
  //      row_PassiveElement_type.Properties.Value:= gl_db.GetNameByID(TBL_PASSIVE_COMPONENT, PassiveElementID);

{
      if (qry_Temp[FLD_EQUIPMENT_TYPE] = DEF_EQUIPMENT_NOT_TYPED) then
      begin
        row_Power_dBm.Properties.Value        := qry_Temp.FieldBYName(FLD_POWER_MAX).AsString;
        row_THRESHOLD_BER_3.Properties.Value  := qry_Temp.FieldBYName(FLD_THRESHOLD_BER_3).AsString;
        row_THRESHOLD_BER_6.Properties.Value  := qry_Temp.FieldBYName(FLD_THRESHOLD_BER_6).AsString;
        row_Freq.Properties.Value             := qry_Temp[FLD_TX_FREQ_MHz]/1000;

        row__Loss.Properties.Value            := qry_Temp[FLD_LOSS];
        row__Raznos.Properties.Value          := qry_Temp[FLD_FREQ_SPACING];
        row__signature_height.Properties.Value:= qry_Temp[FLD_SIGNATURE_HEIGHT];
        row__signature_width.Properties.Value := qry_Temp[FLD_SIGNATURE_WIDTH];
      end;

}

{
      dmOnega_DB_data.OpenQuery(qry_Temp,
         Format('SELECT * FROM %s WHERE %s=%d',
             [TBL_LINKEND_ANTENNA, FLD_LINKEND_ID, LinkEndID]));
}

     // db_OpenQuery (qry_Temp, TBL_LINKEND_ANTENNA, FLD_LINKEND_ID, LinkEndID);


{      if VarIsNull(qry_Temp[FLD_ID]) then
        AntennaTypeID:= 0
      else
        AntennaTypeID:= dmAntenna.GetAntTypeID(qry_Temp[FLD_ID]);
}


{      if not gl_DB.RecordExists(TBL_ANTENNATYPE, [db_Par(FLD_ID, AntennaTypeID)  ]) then
        row_Antenna1_Type.Properties.Value:= ''
      else
}
//        row_Antenna1_Type.Properties.Value:= dmAntType.GetNameByID(AntennaTypeID);

 //   end else
  //  begin
 //     row_LinkEnd_Name.Text:= dmLinkAddFromProfile.GetName(OBJ_LINKEND, aPropNumber);

//      row_LinkEnd_Name.Properties.Value:=
//          dmLinkAddFromProfile.GetName1(OBJ_LINKEND, aPropNumber);
//
//
//      LinkEndID:=0;
//      Result:=false;
//
//
//      row_Mode.Properties.Value             := Mode;
//
//      row_LinkEndType.Properties.Value        := gl_db.GetNameByID (TBL_LINKENDTYPE, LinkEndTypeID);
//      row_Antenna1_Type.Properties.Value      := gl_db.GetNameByID (TBL_ANTENNATYPE, AntennaTypeID);
//      row_PassiveElement_type.Properties.Value:= gl_db.GetNameByID (TBL_PASSIVE_COMPONENT, PassiveElementID);
//
//    end;
{
  end else
  begin
  /////////////////
 //zzzzzzzz   dx_SetReadOnly ([row_IsMakeNewLinkEnd]);

 //   row_IsMakeNewLinkEnd.Text := 'True';
  //  row_IsMakeNewLinkEnd.Properties.Value := true;
  /////////  row_IsMakeNewLinkEnd.OnToggleClick:= nil;

    row_Property.Properties.Value         :=
       dmLinkAddFromProfile.GetName1(OBJ_PROPERTY, aPropNumber);

//    row_LinkEnd_Name.Text := dmLinkAddFromProfile.GetName(OBJ_LINKEND, aPropNumber);
    row_LinkEnd_Name.Properties.Value := dmLinkAddFromProfile.GetName1(OBJ_LINKEND, aPropNumber);

    row_Mode.Properties.Value   := Mode;

    row_LinkEndType.Properties.Value            := gl_db.GetNameByID (TBL_LINKENDTYPE, LinkEndTypeID);
    row_Antenna1_Type.Properties.Value          := gl_db.GetNameByID (TBL_ANTENNATYPE, AntennaTypeID);
    row_PassiveElement_type.Properties.Value    := gl_db.GetNameByID (TBL_PASSIVE_COMPONENT, PassiveElementID);
  end;
}

end;


//--------------------------------------------------------------------
function Tframe_Link_add_from_Profile.AddLinkEndToDB(aPropertyID: integer):
    integer;
//--------------------------------------------------------------------
var
  rec: TdmLinkEndAddRec;
  dHeight: double;
begin


  FillChar(rec, SizeOf(rec), 0);

//  rec.Name         :=row_LinkEnd_Name.Text;
  rec.NewName         :=row_LinkEnd_Name.Properties.Value;
  rec.Property_ID   :=aPropertyID;
//  rec.FolderID     :=0;
  rec.EquipmentType:=Equipment_Type;

  //------------------------------
  if Equipment_Type = DEF_EQUIPMENT_TYPED then
  //------------------------------
  begin
    rec.LinkEndType_ID := LinkEndTypeID;
//    rec.Mode          := Mode;
//    rec.LinkEndType_Mode_ID := LinkEndTypeID;
    rec.LinkEndType_Mode_ID := LinkEndType_Mode_ID;

  end  else
  begin
//    dPower              := row_Power_W.Properties.Value;

    rec.TxFreq_MHz      := AsFloat( row_Freq_GHz.Properties.Value) * 1000;
    rec.BAND            :=   row_Band.Properties.Value;

    rec.Power_dBm       := AsFloat( row_Power_dBm.Properties.Value);
  //  rec.Power_Max_dBm   := dPower;
    rec.Threshold_BER_3 := AsFloat( row_THRESHOLD_BER_3.Properties.Value);
    rec.Threshold_BER_6 := AsFloat( row_THRESHOLD_BER_6.Properties.Value);
    rec.Loss_dB         := AsFloat( row__Loss.Properties.Value);
  //  rec.equaliser_profit:= 0;
    rec.Freq_Spacing_MHz:= row__Raznos.Properties.Value;

    rec.Signature_width_MHz := AsFloat(row__signature_width.Properties.Value);
    rec.Signature_height_dB := AsFloat(row__signature_height.Properties.Value);

  end;


  rec.Antenna1_.AntennaTypeID:=AntennaTypeID;
  rec.PassiveElement_ID:= PassiveElementID;


  dHeight:= AsFloat( row_Antenna_Height.Properties.Value);
  if dHeight <> 0 then
    rec.Antenna1_.Height :=dHeight
  else
    rec.Antenna1_.Height :=30;


  Result:=dmLinkEnd.Add1 (rec, OBJ_LINKEND);
  if Result>0 then
    dmMapEngine_store.Feature_Add(OBJ_LINKEND, Result);

//    dmMapEngine.CreateObject (otLinkEnd, Result);
end;


//--------------------------------------------------------------------
function Tframe_Link_add_from_Profile.AddProperty1: integer;
//--------------------------------------------------------------------
var
  oPropertyAdd: TPropertyAddToDB;
 // rec: TdmPropertyAddRec;
begin
  //FillChar(rec, SizeOf(rec), 0);


  if FPropertyID=0 then
  begin
    oPropertyAdd := TPropertyAddToDB.Create();

    oPropertyAdd.Project_ID:=dmMain.ProjectID;

    oPropertyAdd.NewName  :=row_Property.Properties.Value;

    oPropertyAdd.Address:=AsString(row_Property_address.Properties.Value);

    oPropertyAdd.BLPoint_Pulkovo :=Params.Property_BLPos;

  //  oPropertyAdd.Height    := AsFloat(row_Height.Properties.Value);
   // oPropertyAdd.Azimuth   := row_Azimuth.Properties.Value;
  //  FAntennaAdd.PropertyID:= FPropertyID;

  //  oPropertyAdd.AntennaType_ID :=FRec.AntennaTypeID;

    Result:=oPropertyAdd.Add; //(dmMain.ADOConnection);

    FreeAndNil(oPropertyAdd);

//
//
//
//    rec.Name   :=row_Property.Properties.Value;
//    rec.BLPoint_Pulkovo:=Params.Property_BLPos;
//    rec.Address:=row_Property_address.Properties.Value;
//
//    Result:=dmProperty.Add(rec);

    //---------------------------------------------------

    if Result>0 then
      dmMapEngine_store.Feature_Add (OBJ_Property, Result);

  end else
    Result:=FPropertyID;
                        

{
  FillChar(rec, SizeOf(rec), 0);

  rec.Name    :=row_Prop.Properties.Value;
  rec.BLPoint :=Params.Property_BLPos;
//  rec.FolderID:=0;

  if (not IsPropExist) then
  begin
 //   ShowMessage('property:'+ rec.Name+'   property:'+ IntTostr(rec.FolderID) );

    if (dmProperty.FindByNameAndFolder(rec.Name, rec.FolderID) > 0) then
    begin
      ErrDlg('�������� '+rec.Name+' ��� ���������� �� ��. ���������� ����������.');
      exit;
    end;

    Result:=dmProperty.Add(rec);

    dmMapEngine1.CreateObject (otProperty, Result);
  end
  else
    Result:=dmProperty.FindByName(rec.Name);
}

 // Result:=FPropertyID;

end;



//--------------------------------------------------------------------
procedure Tframe_Link_add_from_Profile.ApplicationEvents1ActionUpdate(
                  Action: TBasicAction; var Handled: Boolean);
//--------------------------------------------------------------------
begin
 ////////!!!!!!!!!!!!
//  row_Height.Properties.Value:=null;

  IsAllowAppend1:=((AntennaTypeID>0) and
             //     (LinkEndTypeID>0) and
                  (row_Antenna_Height.Properties.Value > 0)) ;

  if Equipment_Type = DEF_EQUIPMENT_TYPED then
    IsAllowAppend1:=IsAllowAppend1 and //()((AntennaTypeID>0) and
                    (LinkEndTypeID>0)

                    // and
                    //(row_Height.Properties.Value > 0))
  else
    IsAllowAppend1:=IsAllowAppend1 and
                    (
                    // (AntennaTypeID>0)   and
                    // (row_Height.Properties.Value > 0) and

                     (row_Power_dBm.Properties.Value  <>0) and
                     (row_Freq_GHz.Properties.Value   <>0) and
                     (row_THRESHOLD_BER_3.Properties.Value <>0) AND
                     (row_THRESHOLD_BER_6.Properties.Value <>0) and
                     (row__signature_height.Properties.Value <>0) and
                     (row__signature_width.Properties.Value  <>0));

 /////// if IsLinkEndExist then
///////////    IsAllowAppend1:= true;
end;



//--------------------------------------------------------------------
function Tframe_Link_add_from_Profile.Get_IsAllowAppend: Boolean;
//--------------------------------------------------------------------
begin
 ////////!!!!!!!!!!!!
//  row_Height.Properties.Value:=null;

  result:=((AntennaTypeID>0) and
             //     (LinkEndTypeID>0) and
                  (row_Antenna_Height.Properties.Value > 0)) ;

  if Equipment_Type = DEF_EQUIPMENT_TYPED then
    result:=result and //()((AntennaTypeID>0) and
                    (LinkEndTypeID>0)

                    // and
                    //(row_Height.Properties.Value > 0))
  else
    result:=result and
                    (
                    // (AntennaTypeID>0)   and
                    // (row_Height.Properties.Value > 0) and

                     (row_Power_dBm.Properties.Value  <>0) and
                     (row_Freq_GHz.Properties.Value   <>0) and
                     (row_THRESHOLD_BER_3.Properties.Value <>0) AND
                     (row_THRESHOLD_BER_6.Properties.Value <>0) and
                     (row__signature_height.Properties.Value <>0) and
                     (row__signature_width.Properties.Value  <>0));

 /////// if IsLinkEndExist then
///////////    IsAllowAppend1:= true;
end;



//-------------------------------------------------------------------
function Tframe_Link_add_from_Profile.GetPropertyName: string;
//-------------------------------------------------------------------
begin
//  Result:= AsString( row_Property.Properties.Value);
  Result:= row_Property.Properties.Value;

end;


//-------------------------------------------------------------------
procedure Tframe_Link_add_from_Profile.DoEquipmentChanged;
//-------------------------------------------------------------------
begin
  if Assigned(FOnEquipmentChanged) then
    FOnEquipmentChanged(Self);
end;

//-------------------------------------------------------------------
procedure Tframe_Link_add_from_Profile.DoOnButtonClickNoAction(Sender: TObject; AbsoluteIndex: Integer);
//-------------------------------------------------------------------
begin
  //-------------------------------
  if (Sender=row_LinkendType) or (Sender=row_Antenna1_Type) then
  //-------------------------------
  begin
    ErrorDlg ('��� ������ ����� ������ -����� ���-.');
  end;
end;


//------------------------------------------------------------------
function Tframe_Link_add_from_Profile.VerifyInputData(): string;
//------------------------------------------------------------------
begin
  if (row_Equipment_Typed.Properties.Value= true) then
//  if Eq(row_Equip_Type.Text, 'true') then
    exit;

  if row_Power_dBm.Properties.Value=0         then Result:='M������� = 0';
  if row_Freq_GHz.Properties.Value=0          then Result:='������� = 0';
  if row_THRESHOLD_BER_3.Properties.Value=0   then Result:='���������������� ��� BER^-3 = 0';
  if row_THRESHOLD_BER_6.Properties.Value=0   then Result:='���������������� ��� BER^-6 = 0';
  if row__signature_height.Properties.Value=0 then Result:='������ �������� = 0';
  if row__signature_width.Properties.Value =0 then Result:='������ �������� = 0';

end;



procedure Tframe_Link_add_from_Profile.SetEquipment_Type(Value: integer);
begin
  FEquipment_Type := Value;

//  DEF_EQUIPMENT_TYPED     = 0;
 // DEF_EQUIPMENT_NOT_TYPED = 1;

end;


procedure Tframe_Link_add_from_Profile.row_LinkEndType1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  iLinkEndType_Mode_ID: integer;

  rec: TdmLinkEndTypeModeInfoRec;
begin
  //-------------------------------
  if cxVerticalGrid1.FocusedRow=row_PassiveElement_type then
  //-------------------------------
  begin
    case AButtonIndex of    //
      0: begin
           dmAct_Explorer.Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otCombiner, PassiveElementID);
         end;
      1: begin
           row_PassiveElement_type.Properties.Value:='';
           PassiveElementID:=0;
         end;
    end;    // case

    DoEquipmentChanged;
  end else

  //-------------------------------
  if cxVerticalGrid1.FocusedRow=row_Antenna1_Type then
  //-------------------------------
  begin
    dmAct_Explorer.Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otAntennaType, AntennaTypeID);

//    DoEquipmentChanged;
  end;

  //-------------------------------
  if cxVerticalGrid1.FocusedRow=row_mode then
  //-------------------------------
  begin
    if LinkEndTypeID > 0 then
      if dmAct_LinkEndType.Dlg_GetMode (LinkEndTypeID, LinkEndType_Mode_ID, rec) then //Mode, iLinkEndType_Mode_ID,
      begin
        LinkEndType_Mode_ID := rec.ID;
        Mode                := rec.Mode;

        row_mode.Properties.Value:= Mode;
      end;

{
      if  Mode < 0  then
        Exit;


//    if Mode < 0 then
 //     Mode:= 0;

    if Mode > 0 then}

   // DoEquipmentChanged;
  end else

  //-------------------------------
  if cxVerticalGrid1.FocusedRow=row_LinkendType then
  //-------------------------------
  begin
    dmAct_Explorer.Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otLinkEndType, LinkEndTypeID);

    if LinkEndTypeID > 0 then
      if dmAct_LinkEndType.Dlg_GetMode (LinkEndTypeID, LinkEndType_Mode_ID, rec) then //Mode, iLinkEndType_Mode_ID,
      begin
        LinkEndType_Mode_ID := rec.ID;
        Mode                := rec.Mode;

        row_mode.Properties.Value:= Mode;
      end;


(*    if LinkEndTypeID <> 0 then
    begin
      if gl_DB.GetID(TBL_LINKENDTYPE_MODE,
                    [db_par(FLD_MODE,            1),
                     db_par(FLD_LINKENDType_ID, LinkEndTypeID)]) > 0 then
        Mode:= 1
      else
        Mode:= 0;

      row_mode.Properties.Value:= Mode;
    end;
*)

  end else;

  DoEquipmentChanged;

  cxVerticalGrid1.HideEdit;
end;



//-------------------------------------------------------------------
procedure Tframe_Link_add_from_Profile.ShowTypedEquipment(aValue: boolean);
//-------------------------------------------------------------------
begin
  row_Typed_equipment.Visible:=aValue;
  row_not_Typed_equipment.Visible:=not aValue;

  if not aValue then
    Equipment_Type:= DEF_EQUIPMENT_NOT_TYPED
  else
    Equipment_Type:= DEF_EQUIPMENT_TYPED;


 { ShowTypedEquipment(false);

                    //  dx_Not_Typed_Equip.Visible:= true;
                    //  cx_Typed_Equip.Visible:=     false;
                      Equipment_Type:= DEF_EQUIPMENT_NOT_TYPED;
                    end;

      cbsChecked: begin
                    ShowTypedEquipment(true);

                  //  dx_Not_Typed_Equip.Visible:= false;
                  //  cx_Typed_Equip.Visible:=     true;
                    Equipment_Type:= DEF_EQUIPMENT_TYPED;
}
end;


//---------------------------------------------------------------------------
procedure Tframe_Link_add_from_Profile.row_Equip_TypeEditPropertiesEditValueChanged(
  Sender: TObject);
//---------------------------------------------------------------------------
begin

 //  ShowMessage(Sender.ClassName);
 //  ShowMessage(BoolToStr(TcxCheckBox(Sender).Checked));



  // -------------------------------------------------------------------
  if cxVerticalGrid1.FocusedRow = row_Equipment_Typed then
  // -------------------------------------------------------------------
  begin
    ShowTypedEquipment(TcxCheckBox(Sender).Checked);

   { case State of
      cbsUnchecked: begin
                      ShowTypedEquipment(false);

                    //  dx_Not_Typed_Equip.Visible:= true;
                    //  cx_Typed_Equip.Visible:=     false;
                      Equipment_Type:= DEF_EQUIPMENT_NOT_TYPED;
                    end;

      cbsChecked: begin
                    ShowTypedEquipment(true);

                  //  dx_Not_Typed_Equip.Visible:= false;
                  //  cx_Typed_Equip.Visible:=     true;
                    Equipment_Type:= DEF_EQUIPMENT_TYPED;
                  end;
    end;}

    DoEquipmentChanged;
  end   else

end;


procedure Tframe_Link_add_from_Profile.row_PropEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  if cxVerticalGrid1.FocusedRow=row_Property then
    if dmAct_Explorer.Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otProperty, FPropertyID) then
      UpdateAntennaMaxHeight();

end;


procedure Tframe_Link_add_from_Profile.cxVerticalGrid1StylesGetContentStyle(
  Sender: TObject; AEditProp: TcxCustomEditorRowProperties;
  AFocused: Boolean; ARecordIndex: Integer; var AStyle: TcxStyle);
begin
  if not (AEditProp.Row is TcxEditorRow) then
    Exit;

  if (AEditProp.Row=row_Antenna1_Type) or
     (AEditProp.Row=row_Antenna_Height) or
     (AEditProp.Row=row_Property) or
     (AEditProp.Row=row_LinkEnd_Name)

    // (AEditProp.Row=row_Cell_Layer) or
    // (AEditProp.Row=row_PmpSite)
  then begin
    AStyle := cxStyle1;
  //  Exit;
  end;

end;

procedure Tframe_Link_add_from_Profile.row_Property_posEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
 // rec.BLPoint_Pulkovo:=Params.Property_BLPos;
                                                     

  if Tdlg_Geo_Pos.ExecDlg(Params.Property_BLPos) then
    row_Property_pos.Properties.Value:= geo_FormatBLPoint(Params.Property_BLPos) + ' CK-42';

  // row_Property_pos.Properties.Value:= geo_FormatBLPoint(Params.Property_BLPos);


end;

end.
