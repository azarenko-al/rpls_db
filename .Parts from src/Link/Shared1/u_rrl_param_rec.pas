unit u_rrl_param_rec;

interface
uses
  u_func,
  u_XML;


type
  TrrlParamRec = record

    //-------------------------------------------------------------------
    TTX: record
    //-------------------------------------------------------------------
      Site1_Power     : double;  //'site1-��������'
      Site1_DIAMETER  : double;  // 'site1-�������');
      Site1_GAIN      : double;  //  'site1-��������');
      Site1_VERT_WIDTH: Double;  //  'site1-������ ���');
      Site1_LOSS      : Double;  //   + dLinkEndLoss, 'site1-������ � ���');
      Site1_HEIGHT    : Double;  //  'site1-������');

      Site2_Threshold : Double;  //������
      Site2_DIAMETER  : double;  // 'site1-�������');
      Site2_GAIN      : double;  //  'site1-��������');
      Site2_VERT_WIDTH: Double;  //  'site1-������ ���');
      Site2_LOSS      : Double;  //   + dLinkEndLoss, 'site1-������ � ���');
      Site2_HEIGHT    : Double;  //  'site1-������');


      Dop_Site1_DIAMETER  : double;  // 'site1-�������');
      Dop_Site1_GAIN      : double; //  'site1-��������');
      Dop_Site1_VERT_WIDTH: Double; //  'site1-������ ���');
      Dop_Site1_LOSS      : Double; //   + dLinkEndLoss, 'site1-������ � ���');
      Dop_Site1_HEIGHT    : Double; //  'site1-������');

      Dop_Site2_DIAMETER  : double;  // 'site1-�������');
      Dop_Site2_GAIN      : double; //  'site1-��������');
      Dop_Site2_VERT_WIDTH: Double; //  'site1-������ ���');
      Dop_Site2_LOSS      : Double; //   + dLinkEndLoss, 'site1-������ � ���');
      Dop_Site2_HEIGHT    : Double; //  'site1-������');


      SIGNATURE_WIDTH  : double;  //  '������ ���������, ���');
      SIGNATURE_HEIGHT : double;  // '������ ���������, ��');
  ////ieldByName(FLD_Modulation_Type).AsInteger, '��� ������� ���������, 1-���, 2-��, 3-���');
      MODULATION_COUNT : double;  // '����� ������� ���������');
      EQUALISER_PROFIT : double;  // '�������, �������� ������������, ��');

      KRATNOST_BY_SPACE: integer; //'��������� ���������� �� ������������, 1...2'
      KRATNOST_BY_FREQ : integer; //'��������� ���������� �� �������, 1..n+1 //1...8'

      FreqSpacing      : Double;  //'��������� ������, ���');

      Site1_FrontToBackRatio: Double;  //'ant1 - ����.���.�������� (������/�����) ���-�������, ��')
      Site2_FrontToBackRatio: Double; //'ant2 - ����.���.�������� (������/�����) ���-�������, ��'

      Dop_Site1_FrontToBackRatio: Double; //'ant1 - ����.���.�������� (������/�����) ���-�������, ��'
      Dop_Site2_FrontToBackRatio: Double;  //'ant2 - ����.���.�������� (������/�����) ���-�������, ��')


      Site1_KNG: Double; //'��� ������������ (%)1'
      Site2_KNG: Double;  //'��� ������������ (%)2'


      Freq: Double; //  'P������ �������, ���');
      POLARIZATION: integer;

      AntennaOffsetHor: Double; //�������������� ����� //���������� ����� ��������� �� ������

    end;


    //-------------------------------------------------------------------
    RRV: record
    //-------------------------------------------------------------------

      gradient_diel : double;   // '������� �������� ��������� ����.�������������,10^-8 1/�' );
      gradient      : double;   // '����������� ���������� ���������,10^-8 1/�' );

      terrain_type  : Double;   //'��� ������������ �������.(1...3,0->����.�����=����.���.)' );
      steam_wet     : double;   // '���������� ��������� �������� ����, �/���.�');
      Q_factor      : double;   // 'Q-������ ������ �����������');
      climate_factor: double;   //'������������� ������ K��, 10^-6');
      factor_B      : double;   // '�������� ������������ b ��� ������� ������');
      factor_C      : double;   // '�������� ������������ c ��� ������� ������');
      FACTOR_D      : double;   // '�������� ������������ d ��� ������� ������');
      RAIN_RATE     : double;   // '������������� ����� � ������� 0.01% �������, ��/���');


      REFRACTION: double;       //'�����. ��������� (��� ��.���������=0, ����� �.�.=0)');

    end;

    //-------------------------------------------------------------------
    DLT: record
    //-------------------------------------------------------------------
      Step: Double; //  '��� ��������� �������, �� ( 0 - ��������� ����)');

      // default values
      precision_V_diffraction: Double;//   '�������� ������ ������� V����(g)=V���.�, ��');
      precision_g_V_diffraction: Double;// '�������� ������ ������� g(V����)=g(V���.�), 10^-8');
      max_gradient_for_subrefr: Double;//  '������������ �������� ��� ������� ������������, 10^-8');

    end;

    //-------------------------------------------------------------------
    TRB: record
    //-------------------------------------------------------------------
    //TRB: array[1..4] of double;  //���������� � ����������� �������� ���
     {1=0.012      ;����������� �������� SESR ��� ����, %
      2=0.05       ;����������� �������� ���  ��� ����, %
      3=600        ;����� ����, ��
      4=0.200      ;���������� ����� ���, ������������� �������������}

      GST_SESR: double; //   '����������� �������� SESR ��� ����, %');
      GST_Kng:  double; //    '����������� �������� ���  ��� ����, %');
      Length :  Double;  //               , '����� ����, ��');
      KNG_dop:  double; //    '���������� ����� ���, ������������� �������������}');
      SPACE_LIMIT: double; // '���������� ������������� ������� p(a%)');
      SPACE_LIMIT_PROBABILITY: double; //'���������� ����������� a% ������������� ������������ �������� p(a%) [%]');

    end;

    Relief: array of record
       dist       : Double;
       rel_h      : Double;
       local_h    : Double;
       local_code : smallint;


       earth_h    : Double;//test

    end;


    OptimizeEnabled: Boolean;
    OptimizeKind: (otMinMax_H1_H2, otMin_H1_plus_H2);

  end;


  procedure TrrlParamRec_SaveToXML(aRec: TrrlParamRec; aFileName: string; aIsSaveProfile: boolean = true);


implementation



//--------------------------------------------------------------------
procedure TrrlParamRec_SaveToXML(aRec: TrrlParamRec; aFileName: string; aIsSaveProfile: boolean = true);
//--------------------------------------------------------------------

  //--------------------------------------------------------------------
  procedure DoAdd (aNode: IXMLNode; aParamID: integer; aValue: Variant; aComment: string='');
  //--------------------------------------------------------------------
  begin
    xml_AddNode (aNode, TAG_PARAM, [xml_Att(ATT_id, aParamID),
                                    xml_Att(ATT_value, aValue),
                                    xml_Att(ATT_COMMENT, aComment ) ]);
  end;

  //--------------------------------------------------------------------
  procedure DoAddLinkType (aRoot: Variant);
  //--------------------------------------------------------------------
  var vGroup: IXMLNode;
      iStep: integer;
      d,dRefraction,dLength: double;
  begin


    begin
      // ��� - ������� ��������������� ���������
    //RRV: array[1..11] of double; //�������������� ����� �� �������� ��������������� ���������
     {1=-9.000     ;������� �������� ��������� ����.�������������,10^-8 1/�
      2= 7.000     ;����������� ���������� ���������,10^-8 1/�
      3= 1.000     ;��� ������������ �������.(1...3,0->����.�����=����.���.)
      4= 7.500     ;���������� ��������� �������� ����, �/���.�
      5= 1.000     ;Q-������ ������ �����������
      6=41.000     ;������������� ������ K��, 10^-6
      7= 1.500     ;�������� ������������ b ��� ������� ������
      8= 0.500     ;�������� ������������ c ��� ������� ������
      9= 2.000     ;�������� ������������ d ��� ������� ������
      10=20.000    ;������������� ����� � ������� 0.01% �������, ��/���
      11= 1.335    ;�����. ��������� (��� ��.���������=0, ����� �.�.=0)}

     // -------------------------------------------------------------------

      vGroup:=xml_AddNodeTag (aRoot, 'RRV');
      DoAdd (vGroup, 1,  aRec.RRV.gradient_diel, '������� �������� ��������� ����.�������������,10^-8 1/�' );
      DoAdd (vGroup, 2,  aRec.RRV.gradient,      '����������� ���������� ���������,10^-8 1/�' );

      DoAdd (vGroup, 3,  aRec.RRV.terrain_type, '��� ������������ �������.(1...3,0->����.�����=����.���.)' );
      DoAdd (vGroup, 4,  aRec.RRV.steam_wet,    '���������� ��������� �������� ����, �/���.�');
      DoAdd (vGroup, 5,  aRec.RRV.Q_factor,     'Q-������ ������ �����������');
      DoAdd (vGroup, 6,  aRec.RRV.climate_factor,'������������� ������ K��, 10^-6');
      DoAdd (vGroup, 7,  aRec.RRV.factor_B,     '�������� ������������ b ��� ������� ������');
      DoAdd (vGroup, 8,  aRec.RRV.factor_C,     '�������� ������������ c ��� ������� ������');
      DoAdd (vGroup, 9,  aRec.RRV.FACTOR_D,     '�������� ������������ d ��� ������� ������');
      DoAdd (vGroup, 10, aRec.RRV.RAIN_RATE,    '������������� ����� � ������� 0.01% �������, ��/���');
      DoAdd (vGroup, 11, aRec.RRV.REFRACTION,    '�����. ��������� (��� ��.���������=0, ����� �.�.=0)');


//      DoAdd (vGroup, 11, 1.335 {FieldValues[FLD_refraction_gradient_0]}, '�����. ��������� (��� ��.���������=0, ����� �.�.=0)');

    //DLT: array[1..4] of double;  //���������� � �������� ��������
     {1= 0.100     ;��� ��������� �������, �� ( 0 - ��������� ����)
      2= 1.000     ;�������� ������ ������� V����(g)=V���.�, ��
      3= 1.000     ;�������� ������ ������� g(V����)=g(V���.�), 10^-8
      4=80.000     ;������������ �������� ��� ������� ������������, 10^-8}


      vGroup:=xml_AddNodeTag (aRoot, 'DLT');
//      iStep:=FieldByName(FLD_profile_step).AsInteger;

      DoAdd (vGroup,  1, aRec.DLT.Step,  '��� ��������� �������, �� ( 0 - ��������� ����)');


      // default values
      DoAdd (vGroup,  2, aRec.DLT.precision_V_diffraction,   '�������� ������ ������� V����(g)=V���.�, ��');
      DoAdd (vGroup,  3, aRec.DLT.precision_g_V_diffraction, '�������� ������ ������� g(V����)=g(V���.�), 10^-8');
      DoAdd (vGroup,  4, aRec.DLT.max_gradient_for_subrefr,  '������������ �������� ��� ������� ������������, 10^-8');


    //TRB: array[1..4] of double;  //���������� � ����������� �������� ���
     {1=0.012      ;����������� �������� SESR ��� ����, %
      2=0.05       ;����������� �������� ���  ��� ����, %
      3=600        ;����� ����, ��
      4=0.200      ;���������� ����� ���, ������������� �������������}
      vGroup:=xml_AddNodeTag (aRoot, 'TRB');

      DoAdd (vGroup,  1, aRec.Trb.GST_SESR,      '����������� �������� SESR ��� ����, %');
      DoAdd (vGroup,  2, aRec.Trb.GST_Kng,       '����������� �������� ���  ��� ����, %');
      DoAdd (vGroup,  3, aRec.Trb.Length,        '������������� ����� ����, ��');
      DoAdd (vGroup,  4, aRec.Trb.KNG_dop,       '���������� ����� ���, ������������� �������������}');
      DoAdd (vGroup,  5, aRec.Trb.SPACE_LIMIT,   '���������� ������������� ������� p(a%)');
      DoAdd (vGroup,  6, aRec.Trb.SPACE_LIMIT_PROBABILITY,  '���������� ����������� a% ������������� ������������ �������� p(a%) [%]');


    end;
  end;
  //--------------------------------------------------------------------


  //--------------------------------------------------------------------
  procedure DoSaveProfileToXMLNode (vRoot: IXMLNode);
  //--------------------------------------------------------------------
  var i: integer;
    vGroup: IXMLNode;
  begin
    vGroup:=xml_AddNode (vRoot, 'relief',
                         [xml_Att('count', length(aRec.relief)) ]);

    for i:=0 to High(aRec.relief) do
    begin
      Assert(aRec.relief[i].dist >=0 );
    

      xml_AddNode (vGroup, 'item',
         [xml_ATT ('dist',       aRec.relief[i].dist),
          xml_ATT ('rel_h',      aRec.relief[i].rel_h),
          xml_ATT ('local_h',    aRec.relief[i].local_h),
          xml_ATT ('local_code', aRec.relief[i].local_code),

          xml_ATT ('earth_h',    aRec.relief[i].earth_h)
         ]);
    end;


  end;

  // -------------------------------------------------------------------


var vRoot,vGroup: IXMLNode;

begin
  gl_XMLDoc.Clear;

  vRoot:=xml_AddNodeTag (gl_XMLDoc.DocumentElement, 'RRL_PARAMS');

  DoAddLinkType (vRoot);

  // ��� �����������, 0-��������������,1-������������
  // P������ �������, ���
  // �������� ��������, ��/�
  // ������ ���������, ���
  // ������ ���������, ��
  // ��� ������� ���������, 1-���, 2-��, 3-���
  // ����� ������� ���������
  // �������, �������� ������������, ��
  // ��������� ���������� �� �������, 1...8
  // ��������� ���������� �� ������������, 1...2
  // ��������� ������, ���
  vGroup:=xml_AddNodeTag (vRoot, 'TTX');

  DoAdd (vGroup,  1, aRec.TTX.Site1_Power,      'site1-��������');
  DoAdd (vGroup,  2, aRec.TTX.Site1_DIAMETER,   'site1-�������');
  DoAdd (vGroup,  3, aRec.TTX.Site1_GAIN,       'site1-��������');
  DoAdd (vGroup,  4, aRec.TTX.Site1_VERT_WIDTH, 'site1-������ ���');
  DoAdd (vGroup,  5, aRec.TTX.Site1_LOSS,       'site1-������ � ���');

  DoAdd (vGroup,  6, aRec.TTX.Site1_HEIGHT,     'site1-������');

  DoAdd (vGroup,  7,  aRec.TTX.Site2_Threshold, 'site2-������');
  DoAdd (vGroup,  8,  aRec.TTX.Site2_DIAMETER,  'site2-�������');
  DoAdd (vGroup,  9,  aRec.TTX.Site2_GAIN,      'site2-��������');
  DoAdd (vGroup,  10, aRec.TTX.Site2_VERT_WIDTH,'site2-������ ���');
  DoAdd (vGroup,  11, aRec.TTX.Site2_LOSS,      'site2-������ � ���');
  DoAdd (vGroup,  12, aRec.TTX.Site2_HEIGHT,    'site2-������');



  // -------------------------------------------------------------------
  //  POLARIZATION
  // -------------------------------------------------------------------
  DoAdd (vGroup, 13, aRec.TTX.POLARIZATION, '��� �����������, 0-��������������,1-������������,2-�����');
  DoAdd (vGroup, 14, aRec.TTX.Freq,         'P������ �������, ���');

  // -------------------------------------------------------------------
  //  LEFT  - ����������
  // -------------------------------------------------------------------

///////////!!!!!!!    DoAdd (vGroup, 15, FieldValues[FLD_Speed],            '�������� ��������, ��/�');
  DoAdd (vGroup, 16,  aRec.TTX.SIGNATURE_WIDTH,  '������ ���������, ���');
  DoAdd (vGroup, 17,  aRec.TTX.SIGNATURE_HEIGHT, '������ ���������, ��');
////////!!!!!!!    DoAdType).AsInteger, '��� ������� ���������, 1-���, 2-��, 3-���');
  DoAdd (vGroup, 19, aRec.TTX.MODULATION_COUNT,  '����� ������� ���������');
  DoAdd (vGroup, 20, aRec.TTX.EQUALISER_PROFIT,  '�������, �������� ������������, ��');



    //////////////
//    iValue:=FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger + 1;
  //  DoAdd (vGroup, 21, iValue, '��������� ���������� �� �������, 1...8');  //�� ���


  // -------------------------------------------------------------------
  //  RIGHT  - ��������
  // -------------------------------------------------------------------

  DoAdd (vGroup, 22, aRec.ttx.KRATNOST_BY_SPACE, '��������� ���������� �� ������������, 1...2'); //�� ���

  DoAdd (vGroup, 21, aRec.ttx.KRATNOST_BY_FREQ,  '��������� ���������� �� �������, 1...8');  //�� ���
  DoAdd (vGroup, 23, aRec.ttx.FreqSpacing,       '��������� ������, ���');


{ 24-28 - ��� ��� 1
  29-33 - ��� ��� 2
  }


  if aRec.TTX.Dop_Site1_GAIN>0 then
  begin
    DoAdd (vGroup,  24, aRec.TTX.Dop_Site1_DIAMETER,      'dop-site1-�������');
    DoAdd (vGroup,  25, aRec.TTX.Dop_Site1_GAIN,          'dop-site1-��������');
    DoAdd (vGroup,  26, aRec.TTX.Dop_Site1_VERT_WIDTH  ,  'dop-site1-������ ���');
    DoAdd (vGroup,  27, aRec.TTX.Dop_Site1_LOSS,          'dop-site1-������ � ���');
    DoAdd (vGroup,  28, aRec.TTX.Dop_Site1_HEIGHT,        'dop-site1-������');

  end;

  if aRec.TTX.Dop_Site2_GAIN>0 then
  begin
    DoAdd (vGroup,  29, aRec.TTX.Dop_Site2_DIAMETER,     'dop-site2-�������');
    DoAdd (vGroup,  30, aRec.TTX.Dop_Site2_GAIN,         'dop-site2-��������');
    DoAdd (vGroup,  31, aRec.TTX.Dop_Site2_VERT_WIDTH ,  'dop-site2-������ ���');
    DoAdd (vGroup,  32, aRec.TTX.Dop_Site2_LOSS,         'dop-site2-������ � ���');
    DoAdd (vGroup,  33, aRec.TTX.Dop_Site2_HEIGHT,       'dop-site2-������');
  end;

  DoAdd (vGroup, 34, aRec.TTX.Site1_FrontToBackRatio, 'ant1 - ����.���.�������� (������/�����) ���-�������, ��');
  DoAdd (vGroup, 35, aRec.TTX.Site2_FrontToBackRatio, 'ant2 - ����.���.�������� (������/�����) ���-�������, ��');

   //dop
  DoAdd (vGroup, 36, aRec.TTX.Dop_Site1_FrontToBackRatio, 'ant1 - ����.���.�������� (������/�����) ���-�������, ��');
  DoAdd (vGroup, 37, aRec.TTX.Dop_Site2_FrontToBackRatio, 'ant2 - ����.���.�������� (������/�����) ���-�������, ��');

  DoAdd (vGroup, 38, aRec.TTX.Site1_KNG,  '��� ������������ (%)1');
  DoAdd (vGroup, 39, aRec.TTX.Site2_KNG,  '��� ������������ (%)2');

  DoAdd (vGroup, 40, aRec.TTX.AntennaOffsetHor,  '�������������� �����');



  if  aRec.OptimizeEnabled then
//  (otNone, otMinMax_H1_H2, otMin_H1_plus_H2);
// then
    DoAdd (vGroup, 42, IIF(aRec.OptimizeKind=otMinMax_H1_H2,0,1),  '�������� �����������');



    {
    36-37 - ��� ��� 1,2
    }

 // gl_XMLDoc.SaveToFile(aFileNameNoProfile);
  if aIsSaveProfile then
    DoSaveProfileToXMLNode (vRoot);

  gl_XMLDoc.SaveToFile(aFileName);
 // gl_XMLDoc.SaveToFile(TEMP_IN_FILENAME);

//  oXMLDoc.Free;
end;


end.
