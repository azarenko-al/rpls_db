unit Unit4;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs,
  IniFiles,

  dm_Link_calc,

  d_Link_Optimize_freq_diversity,
  d_Link_antennas_optimize,


  u_db,
  u_const_db,

  dm_Main,
  dm_Main

  ;


type
  TForm4 = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
    procedure Open;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form4: TForm4;

implementation

{$R *.dfm}

procedure TForm4.FormCreate(Sender: TObject);
var
  iLink: Integer;
begin

 { TdmMain.Init;

  if not dmMain.OpenFromReg then
    Exit;


   iLink:=gl_DB.GetMaxID1(TBL_LINK );
}


{
   dmMain.ProjectID:=502;

}

  Open;


 //zzzzzzzzzzzzzzz  Tdlg_Link_Optimize_freq_diversity.ExecDlg(iLink);

 //
//  Tdlg_Link_antennas_optimize.ExecDlg(iLink);

 //  Tdlg_Link_antennas_opti.Create(Application).ShowModal;

//  dmLink_calc.OpenData(46401);


  {

  dmMain.ProjectID:=iProjectID;
  dmLink_Calc.IsUsePassiveElements:=bUsePassiveElements;
  dmLink_Calc.IsCalcWithAdditionalRain:=bIsCalcWithAdditionalRain;

  dmLink_Calc.Execute (iLinkID);



  with Tframe_Link_View.Create(Self) do
  begin
    View(54284,'');
//    View(53912,'');
    ShowModal;
  end;

  }


end;


//--------------------------------------------------------------
procedure TForm4.Open;
//--------------------------------------------------------------
const
  TEMP_INI_FILE = 'c:\temp\rpls\link_calc.ini';

var
  iProjectID, iLinkID: integer;
  sIniFileName: string;
  bUsePassiveElements: boolean;
  bIsCalcWithAdditionalRain: Boolean;
  bSaveReportToDB: boolean;

begin
  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
    sIniFileName:=TEMP_INI_FILE;


  with TIniFile.Create(sIniFileName) do
  begin
    iProjectID                :=ReadInteger ('main', 'ProjectID',  0);
    iLinkID                   :=ReadInteger ('main', 'LinkID',  0);
   // bSaveReportToDB           :=ReadBool    ('main', 'IsSaveReportToDB',  True);
  //  bUsePassiveElements       :=ReadBool    ('main', 'UsePassiveElements',  false);
  //  bIsCalcWithAdditionalRain :=ReadBool    ('main', 'IsCalcWithAdditionalRain',  false);

    Free;
  end;
  
{
  iProjectID :=1;
  iLinkID    :=1;
}
  if (iLinkID=0) or
     (iProjectID=0)
  then begin
    ShowMessage('(iLinkID=0) or (iProjectID=0) ');
    Exit;
  end;


  TdmMain.Init;

  if not dmMain.OpenFromReg then
    Exit;

  dmMain.ProjectID:=iProjectID;


  Tdlg_Link_antennas_optimize.ExecDlg(iLinkID);

//  Tfrm_Main_link_graph.ExecDlg (iLinkID);

 // Application.CreateForm(Tfrm_Main, frm_Main);


{  dmMain.ProjectID:=iProjectID;
  dmLink_Calc.IsUsePassiveElements:=bUsePassiveElements;
  dmLink_Calc.IsCalcWithAdditionalRain:=bIsCalcWithAdditionalRain;
  dmLink_Calc.Params.IsSaveReportToDB:=bSaveReportToDB;

  dmLink_Calc.Execute (iLinkID);
}

end;


end.
