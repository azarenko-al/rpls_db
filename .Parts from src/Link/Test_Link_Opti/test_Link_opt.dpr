program test_Link_opt;

uses
  Forms,
  u_rrl_param_rec in '..\..\..\..\ONEGA DLL\RPLS_RRL\src_shared\u_rrl_param_rec.pas',
  Unit4 in 'Unit4.pas' {Form4},
  d_Link_Optimize_freq_diversity in '..\d_Link_Optimize_freq_diversity.pas' {dlg_Link_Optimize_freq_diversity},
  d_Link_antennas_optimize in '..\d_Link_antennas_optimize.pas' {dlg_Link_antennas_optimize},
  dm_Link_calc in '..\Link_Calc\src\dm_Link_calc.pas' {dmLink_calc: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm4, Form4);
  Application.Run;
end.
