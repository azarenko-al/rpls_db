unit dm_Link_Climate;

interface
{$I ver.inc}

uses
  Classes, Forms, Variants, SysUtils, DB, ADODB,

//  u_Link_climate_file,


  I_Link_climate,

  dm_Main,

  u_db,
  u_const_db,
  u_link_const,
  u_Geo
  ;

type


  // -------------------------------------------------------------------
  TdmLinkClimateParamRec = packed record
  // -------------------------------------------------------------------
//    DataSource: (dtFromMap, dtFromDict);   // 0 - ��, 1 - �����

    // From dict
    LinkType_ID: integer;
    LinkType_Name: string;

    // from Map
    BLPoint: TBLPoint;

    Calc_Method_Code : Integer; //0,1,2
  end;


  //--------------------------------------------------------------------
  TdmLink_Climate = class(TDataModule)
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);

  private
// TODO: Get1___
//  procedure Get1___(aParams: TdmLinkClimateParamRec; var aRec:
//      TLinkClimateDataRec);
    procedure UpdateRecord(aID: integer; aRec: TLinkClimateDataRec);


  public
    procedure CloseFiles;
    function OpenFiles: Boolean;

    procedure UpdateByMap(aID: integer);

    procedure GetFromDict(aParams: TdmLinkClimateParamRec; var aRec:
        TLinkClimateDataRec);

    procedure UpdateLinkClimateFromDict(aLinkID, aLinkType_ID: integer);

    function GetFromMaps(aParams: TdmLinkClimateParamRec; var aRec:
        TLinkClimateDataRec): Boolean;
  end;


function dmLink_Climate: TdmLink_Climate;


//====================================================================
implementation {$R *.dfm}
//====================================================================
uses
  dm_Link, dm_Onega_db_data;



var
  FdmLink_Climate: TdmLink_Climate;


// ---------------------------------------------------------------
function dmLink_Climate: TdmLink_Climate;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLink_Climate) then
      FdmLink_Climate := TdmLink_Climate.Create(Application);

    Result := FdmLink_Climate;
end;


procedure TdmLink_Climate.DataModuleCreate(Sender: TObject);
begin
  inherited;

  db_SetComponentADOConnection(Self, dmMain.AdoConnection);
end;


function TdmLink_Climate.OpenFiles: Boolean;
begin
  Result := I_Link_climate.Load_ILink_climate_file;
end;


procedure TdmLink_Climate.CloseFiles;
begin
  I_Link_climate.UnLoad_ILink_climate_file;
end;


// -------------------------------------------------------------------
procedure TdmLink_Climate.UpdateRecord(aID: integer; aRec: TLinkClimateDataRec);
// -------------------------------------------------------------------
begin
  Assert(aRec.air_temperature>0, 'Value <=0');
  Assert(aRec.atmosphere_pressure>0, 'Value <=0');


  dmOnega_DB_data.ExecStoredProc('sp_Link_climate_UPD',

 // with aRec do //.ITU
//    dmLink.Update_ (aID,
       [
        db_Par(FLD_ID, aID ),


        // ITU
        db_Par(FLD_RRV_NAME      , aRec.comment ),

        //Q_factor_rec
        db_Par(FLD_terrain_type,            aRec.ITU.terrain_type ),
        db_Par(FLD_underlying_terrain_type, aRec.ITU.underlying_terrain_type),

        db_Par(FLD_Q_factor    ,  aRec.ITU.Q_factor ),

        db_Par(FLD_gradient_diel ,      aRec.ITU.gradient_diel),
        db_Par(FLD_gradient_deviation , aRec.ITU.gradient_deviation ),

        db_Par(FLD_climate_factor,      aRec.ITU.climate_factor),

        db_Par(FLD_factor_B,   aRec.ITU.factor_B),
        db_Par(FLD_factor_C,   aRec.ITU.factor_C),
        db_Par(FLD_factor_D,   aRec.ITU.factor_D),

        db_Par(FLD_rain_intensity,   aRec.ITU.rain_intensity1 ),
        db_Par(FLD_steam_wet,        aRec.ITU.steam_wet  ),

        // ---------------------------------------------------------------
        //GOST
        // ---------------------------------------------------------------

         db_Par (FLD_GOST_NAME,      aRec.Comment),

(*            if rec.air_temperature=0 then rec.air_temperature:=15;
          if rec.atmosphere_pressure=0 then rec.atmosphere_pressure:=1013;
          if rec.GOST.rain_intensity=0 then rec.GOST.rain_intensity:=1;
*)
          db_Par(FLD_GOST_gradient_diel ,      aRec.ITU.gradient_diel),
          db_Par(FLD_GOST_gradient_deviation , aRec.ITU.gradient_deviation ),

          db_Par (FLD_GOST_TERRAIN_TYPE,             aRec.ITU.terrain_type), //3
          db_Par (FLD_GOST_underlying_terrain_type,  aRec.ITU.underlying_terrain_type),//4
          db_Par (FLD_GOST_steam_wet,                aRec.ITU.steam_wet), //5

        //  if rec.air_temperature>0 then
          db_Par (FLD_GOST_air_temperature, aRec.air_temperature), //6

         // if rec.atmosphere_pressure>0 then
          db_Par (FLD_GOST_atmosphere_pressure, aRec.atmosphere_pressure), //7

          db_Par (FLD_GOST_Climate_factor,  aRec.ITU.Climate_factor), //8

//           if rec.GOST.rain_intensity>0 then
          db_Par (FLD_GOST_rain_intensity,  aRec.GOST.rain_intensity), //9


    // -------------------------
       // NIIR1998
       // -------------------------
   //  2: begin
          db_Par (FLD_NIIR1998_NAME,      aRec.Comment),

    // ..     if rec.air_temperature=0 then rec.air_temperature:=15;
    //      if rec.atmosphere_pressure=0 then rec.atmosphere_pressure:=1013;
          db_Par(FLD_NIIR1998_gradient_diel ,      aRec.ITU.gradient_diel),
          db_Par(FLD_NIIR1998_gradient_deviation , aRec.ITU.gradient_deviation ),


          db_Par (FLD_NIIR1998_TERRAIN_TYPE,         aRec.ITU.terrain_type), //3
          db_Par (FLD_NIIR1998_steam_wet,            aRec.ITU.steam_wet), //4

          db_Par (FLD_NIIR1998_air_temperature,      aRec.air_temperature), //5
          db_Par (FLD_NIIR1998_atmosphere_pressure,  aRec.atmosphere_pressure), //6

          db_Par (FLD_NIIR1998_rain_region_number,   aRec.NiiR_1998.rain_8_11_intensity_region_number), //7
          db_Par (FLD_NIIR1998_Qd_region_number,     aRec.NiiR_1998.rain_8_15_Qd_region_number)//8


       ]);

end;


// -------------------------------------------------------------------
procedure TdmLink_Climate.UpdateByMap(aID: integer);
// -------------------------------------------------------------------
var
  blPoint: TBLPoint;
  rec: TLinkClimateDataRec;
begin
  if Assigned(ILink_climate_file) then
  begin
    blPoint:=dmLink.GetBLCenter (aID);

    if ILink_climate_file.GetByBLPoint (blPoint, rec) then
    begin
      rec.Comment:='�� �����';

      UpdateRecord (aID, rec);

    end;
  end;
end;


// -------------------------------------------------------------------
function TdmLink_Climate.GetFromMaps(aParams: TdmLinkClimateParamRec; var aRec:
    TLinkClimateDataRec): Boolean;
// -------------------------------------------------------------------
var
  blPoint: TBLPoint;
{
var
  oFile: TLink_climate_file;
}

begin
  Result := False;

{
  oFile:=TLink_climate_file.Create;
  oFile.OpenFiles;
  oFile.GetByBLPoint (aParams.BLPoint, aRec);

  oFile.CloseFiles;
}
//  u_Link_climate_file;



  if Assigned(ILink_climate_file) then
  begin
//    if aParams.Calc_Method_Code=0 then
    Result := ILink_climate_file.GetByBLPoint (aParams.BLPoint, aRec);
  //  else

//    if aParams.Calc_Method_Code=2 then     //(43,960936645, 39,256628123)
  //    Result := ILink_climate_file.GetByBLPoint_GOST (aParams.BLPoint, aRec);

  end;

//          FLink_climate_file.GetByBLPoint (aParams.BLPoint, aRec);
  aRec.Comment:='�� �����';

end;


// -------------------------------------------------------------------
procedure TdmLink_Climate.GetFromDict(aParams: TdmLinkClimateParamRec; var
    aRec: TLinkClimateDataRec);
// -------------------------------------------------------------------
begin

  db_OpenTableByID (qry_Temp, TBL_LinkType,  aParams.LinkType_ID);

  aRec.Comment:='�� �����������: '+ aParams.LinkType_Name;

  with qry_Temp do
    if not IsEmpty then
    begin
      aRec.LinkType_ID  := aParams.LinkType_ID;

      aRec.air_temperature     :=FieldByName(FLD_air_temperature).AsFloat;


      aRec.ITU.underlying_terrain_type :=FieldByName(FLD_underlying_terrain_type).AsInteger;
      aRec.ITU.terrain_type    :=FieldByName(FLD_TERRAIN_TYPE).AsInteger;
      aRec.ITU.Q_factor        :=FieldByName(FLD_Q_FACTOR).AsFloat;
      aRec.ITU.Factor_C        :=FieldByName(FLD_Factor_C).AsFloat;
      aRec.ITU.Climate_factor  :=FieldByName(FLD_Climate_factor).AsFloat;
      aRec.ITU.Factor_B        :=FieldByName(FLD_Factor_B).AsFloat;
      aRec.ITU.Factor_D        :=FieldByName(FLD_Factor_D).AsFloat;

      aRec.ITU.gradient_diel      :=FieldByName(FLD_gradient_diel).AsFloat;
      aRec.ITU.gradient_deviation :=FieldByName(FLD_gradient_deviation).AsFloat;
      aRec.ITU.rain_intensity1     :=FieldByName(FLD_rain_intensity).AsFloat;
      aRec.ITU.steam_wet          :=FieldByName(FLD_steam_wet).AsFloat;





      aRec.GOST.rain_intensity   :=FieldByName(FLD_GOST_rain_intensity).AsFloat;

      aRec.NIIR_1998.rain_8_11_intensity_region_number:=FieldByName(FLD_NIIR1998_rain_region_number).AsInteger;
      aRec.NIIR_1998.rain_8_15_Qd_region_number       :=FieldByName(FLD_NIIR1998_Qd_region_number).AsInteger;



   aRec.GOST_gradient_diel           :=FieldByName(FLD_GOST_gradient_diel).AsFloat;
   aRec.GOST_gradient_deviation      :=FieldByName(FLD_GOST_gradient_deviation).AsFloat;
   aRec.GOST_terrain_type            :=FieldByName(FLD_GOST_terrain_type).AsFloat;
   aRec.GOST_underlying_terrain_type :=FieldByName(FLD_GOST_underlying_terrain_type).AsFloat;
   aRec.GOST_steam_wet               :=FieldByName(FLD_GOST_steam_wet).AsFloat;
   aRec.GOST_air_temperature         :=FieldByName(FLD_GOST_air_temperature).AsFloat;
   aRec.GOST_atmosphere_pressure     :=FieldByName(FLD_GOST_atmosphere_pressure).AsFloat;
   aRec.GOST_climate_factor          :=FieldByName(FLD_GOST_climate_factor).AsFloat;
   aRec.GOST_rain_intensity          :=FieldByName(FLD_GOST_rain_intensity).AsFloat;

   aRec.NIIR1998_gradient_diel       :=FieldByName(FLD_NIIR1998_gradient_diel).AsFloat;
   aRec.NIIR1998_gradient_deviation  :=FieldByName(FLD_NIIR1998_gradient_deviation).AsFloat;
   aRec.NIIR1998_terrain_type        :=FieldByName(FLD_NIIR1998_terrain_type).AsFloat;
   aRec.NIIR1998_steam_wet           :=FieldByName(FLD_NIIR1998_steam_wet).AsFloat;
   aRec.NIIR1998_air_temperature     :=FieldByName(FLD_NIIR1998_air_temperature).AsFloat;
   aRec.NIIR1998_atmosphere_pressure :=FieldByName(FLD_NIIR1998_atmosphere_pressure).AsFloat;
   aRec.NIIR1998_rain_region_number  :=FieldByName(FLD_NIIR1998_rain_region_number).AsInteger;
   aRec.NIIR1998_Qd_region_number    :=FieldByName(FLD_NIIR1998_Qd_region_number).AsInteger;
   aRec.NIIR1998_water_area_percent  :=FieldByName(FLD_NIIR1998_water_area_percent).AsFloat;



    end;

end;


// -------------------------------------------------------------------
procedure TdmLink_Climate.UpdateLinkClimateFromDict(aLinkID, aLinkType_ID:
    integer);
// -------------------------------------------------------------------

const
  DEF_FIELDS : array[0..30] of string =
  (

  FLD_gradient_diel,
  FLD_gradient_deviation,
  FLD_terrain_type,
  FLD_underlying_terrain_type,
  FLD_steam_wet,
  FLD_Q_factor,
  FLD_climate_factor,
  FLD_factor_B,
  FLD_factor_C,
  FLD_factor_D,
  FLD_air_temperature,
  FLD_rain_intensity,
  FLD_atmosphere_pressure,

  FLD_GOST_gradient_diel,
  FLD_GOST_gradient_deviation,
  FLD_GOST_terrain_type,
  FLD_GOST_underlying_terrain_type,
  FLD_GOST_steam_wet,
  FLD_GOST_air_temperature,
  FLD_GOST_atmosphere_pressure,
  FLD_GOST_climate_factor,
  FLD_GOST_rain_intensity,

  FLD_NIIR1998_gradient_diel,
  FLD_NIIR1998_gradient_deviation,
  FLD_NIIR1998_terrain_type,
  FLD_NIIR1998_steam_wet,
  FLD_NIIR1998_air_temperature,
  FLD_NIIR1998_atmosphere_pressure,
  FLD_NIIR1998_rain_region_number,
  FLD_NIIR1998_Qd_region_number,
  FLD_NIIR1998_water_area_percent

  );

var
  I: Integer;
  k: Integer;
  oParams: TdbParamList;
  sName: string;

begin

  db_OpenTableByID (qry_Temp, TBL_LinkType,  aLinkType_ID);

  Assert(not qry_Temp.IsEmpty, '  db_OpenTableByID (qry_Temp, TBL_LinkType,  aLinkType_ID);  - IsEmpty');

//  aRec.Comment:='�� �����������: '+ aParams.LinkType_Name;


  oParams := TdbParamList.Create();

  oParams.AddItem(FLD_LinkType_ID, aLinkType_ID);


  sName:=qry_Temp[FLD_NAME];

    sName:='�� �����������: '+ sName;

    k:=Length(sName);


    oParams.AddItem(FLD_RRV_NAME, sName);
    oParams.AddItem(FLD_GOST_NAME, sName);
    oParams.AddItem(FLD_NIIR1998_NAME, sName);




  for I := 0 to High(DEF_FIELDS) do
  begin
    oParams.AddItem(DEF_FIELDS[i], qry_Temp[DEF_FIELDS[i]]);

  end;


  dmOnega_DB_data.UpdateRecordByID_list(TBL_LINK, aLinkID, oParams);


 //   dmLink.Update_ (aID,


  FreeAndNil(oParams);



end;




begin

end.

