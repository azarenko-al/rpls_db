object frame_Link_profile_summary: Tframe_Link_profile_summary
  Left = 1103
  Top = 468
  Width = 697
  Height = 781
  Caption = 'frame_Link_profile_summary'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object TBDock1: TTBDock
    Left = 0
    Top = 0
    Width = 689
    Height = 53
    BoundLines = [blTop, blBottom]
    object TBToolbar1: TTBToolbar
      Left = 0
      Top = 0
      Caption = 'TBToolbar1'
      DockableTo = []
      DockMode = dmCannotFloatOrChangeDocks
      DockPos = 8
      ShrinkMode = tbsmWrap
      TabOrder = 0
      object TBItem1: TTBItem
        Action = act_Calc
      end
      object TBSeparatorItem1: TTBSeparatorItem
      end
      object TBItem4: TTBItem
        Action = act_Calc_Adaptive
      end
      object TBSeparatorItem2: TTBSeparatorItem
      end
      object TBItem3: TTBItem
        Action = act_Opti
      end
      object TBSeparatorItem3: TTBSeparatorItem
      end
      object TBItem2: TTBItem
        Action = act_calc_params
      end
      object TBItem5: TTBItem
        Action = act_opt_equipment
      end
      object TBSeparatorItem4: TTBSeparatorItem
      end
      object TBControlItem4: TTBControlItem
        Control = cxDBLookupComboBox2
      end
      object TBControlItem1: TTBControlItem
        Control = DBCheckBox_Is_calc_with_passive
      end
      object DBCheckBox_Is_calc_with_passive: TDBCheckBox
        Left = 217
        Top = 25
        Width = 168
        Height = 22
        Caption = #1059#1095#1105#1090' '#1087#1072#1089#1089#1080#1074#1085#1099#1093' '#1101#1083#1077#1084#1077#1085#1090#1086#1074
        DataField = 'is_UsePassiveElements'
        DataSource = ds_Link
        TabOrder = 0
        ValueChecked = 'True'
        ValueUnchecked = 'False'
        OnMouseUp = DBCheckBox_Is_calc_rainsMouseUp
      end
      object cxDBLookupComboBox2: TcxDBLookupComboBox
        Left = 0
        Top = 25
        DataBinding.DataField = 'calc_method'
        DataBinding.DataSource = ds_Link
        Properties.DropDownListStyle = lsFixedList
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'id'
        Properties.ListColumns = <
          item
            FieldName = 'name'
          end>
        Properties.ListOptions.ShowHeader = False
        Properties.ListSource = ds_dxMemData_Calc_method
        Properties.OnCloseUp = cxDBLookupComboBox2PropertiesCloseUp
        Style.BorderStyle = ebs3D
        Style.PopupBorderStyle = epbsSingle
        TabOrder = 1
        Width = 217
      end
    end
  end
  object cxDBVerticalGrid1: TcxDBVerticalGrid
    Left = 0
    Top = 360
    Width = 689
    Height = 353
    BorderStyle = cxcbsNone
    Align = alBottom
    OptionsView.CellTextMaxLineCount = 3
    OptionsView.ShowEditButtons = ecsbAlways
    OptionsView.AutoScaleBands = False
    OptionsView.PaintStyle = psDelphi
    OptionsView.GridLineColor = clBtnShadow
    OptionsView.RowHeaderMinWidth = 30
    OptionsView.RowHeaderWidth = 284
    OptionsView.ValueWidth = 60
    OptionsView.ValueMinWidth = 60
    OptionsBehavior.GoToNextCellOnEnter = True
    OptionsBehavior.BandSizing = False
    Styles.OnGetContentStyle = cxDBVerticalGrid1StylesGetContentStyle
    TabOrder = 1
    OnEdited = cxDBVerticalGrid1Edited
    DataController.DataSource = DataSource1
    Version = 1
    object cxDBVerticalGrid1CategoryRow1: TcxCategoryRow
      Properties.Caption = #1050#1072#1095#1077#1089#1090#1074#1086' '#1089#1074#1103#1079#1080
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object row__Rx_Level_dBm: TcxDBEditorRow
      Expanded = False
      Properties.Caption = #1059#1088#1086#1074#1077#1085#1100' [dBm]'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Alignment.Vert = taVCenter
      Properties.EditProperties.AutoSelect = False
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'Rx_Level_dBm'
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object row_SESR_norm: TcxDBEditorRow
      Expanded = False
      Properties.Caption = 'SESR '#1090#1088#1077#1073' [%]'
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.DisplayFormat = ',0.00000'
      Properties.DataBinding.FieldName = 'SESR_norm'
      ID = 2
      ParentID = 0
      Index = 1
      Version = 1
    end
    object row_sesr: TcxDBEditorRow
      Expanded = False
      Properties.Caption = 'SESR '#1088#1072#1089#1095' [%]'
      Properties.DataBinding.FieldName = 'sesr'
      ID = 3
      ParentID = 0
      Index = 2
      Version = 1
    end
    object row_kng_norm: TcxDBEditorRow
      Expanded = False
      Properties.Caption = #1050#1085#1075' '#1090#1088#1077#1073' [%]'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Alignment.Vert = taVCenter
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'kng_norm'
      ID = 4
      ParentID = 0
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid1kng: TcxDBEditorRow
      Expanded = False
      Properties.Caption = #1050#1085#1075' '#1088#1072#1089#1095' [%]'
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.DisplayFormat = ',0.00000'
      Properties.DataBinding.FieldName = 'kng'
      ID = 5
      ParentID = 0
      Index = 4
      Version = 1
    end
    object cxDBVerticalGrid1kng_year: TcxDBEditorRow
      Expanded = False
      Properties.Caption = #1050#1085#1075' '#1088#1072#1089#1095' ['#1084#1080#1085#1091#1090'/'#1075#1086#1076']'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Alignment.Vert = taVCenter
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = False
      Properties.DataBinding.FieldName = 'kng_year'
      ID = 6
      ParentID = 0
      Index = 5
      Version = 1
    end
    object row_Reliability: TcxDBEditorRow
      Properties.Caption = #1053#1072#1076#1077#1078#1085#1086#1089#1090#1100' '#1089#1074#1103#1079#1080' %'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.DataBinding.FieldName = 'Reliability'
      ID = 7
      ParentID = 0
      Index = 6
      Version = 1
    end
    object row__BER_required: TcxDBEditorRow
      Expanded = False
      Properties.Caption = 'BER '#1090#1088#1077#1073' '
      Properties.DataBinding.FieldName = 'BER_required_str'
      ID = 8
      ParentID = 0
      Index = 7
      Version = 1
    end
    object row__fade_margin_dB: TcxDBEditorRow
      Expanded = False
      Properties.Caption = #1047#1072#1087#1072#1089' '#1085#1072' '#1079#1072#1084#1080#1088#1072#1085#1080#1103' [dB]'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Alignment.Vert = taVCenter
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'fade_margin_dB'
      ID = 9
      ParentID = 0
      Index = 8
      Version = 1
    end
    object row_Status_back: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Status_back'
      Visible = False
      ID = 10
      ParentID = 0
      Index = 9
      Version = 1
    end
    object row_K_gotovnost: TcxDBEditorRow
      Properties.Caption = 'K '#1075#1086#1090#1086#1074#1085#1086#1089#1090#1080' %'
      Properties.DataBinding.FieldName = 'K_gotovnost'
      ID = 12
      ParentID = 0
      Index = 10
      Version = 1
    end
    object row__Status: TcxDBEditorRow
      Properties.Caption = #1055#1088#1080#1075#1086#1076#1085#1086#1089#1090#1100
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Alignment.Vert = taVCenter
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'status_name'
      ID = 11
      ParentID = 0
      Index = 11
      Version = 1
    end
    object cxDBVerticalGrid1CategoryRow2: TcxCategoryRow
      Properties.Caption = #1054#1073#1086#1088#1091#1076#1086#1074#1072#1085#1080#1077
      ID = 13
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1DBMultiEditorRow1: TcxDBMultiEditorRow
      Properties.Editors = <
        item
          Caption = #1063#1072#1089#1090#1086#1090#1072' Min'
          Width = 60
          DataBinding.FieldName = 'freq_min'
          Options.Editing = False
        end
        item
          Caption = 'Max [MHz]'
          DataBinding.FieldName = 'freq_max'
          Options.Editing = False
        end>
      Visible = False
      ID = 14
      ParentID = 13
      Index = 0
      Version = 1
    end
    object row_Band: TcxDBEditorRow
      Properties.Caption = #1044#1080#1072#1087#1072#1079#1086#1085
      Properties.DataBinding.FieldName = 'Band'
      Properties.Options.Editing = False
      ID = 15
      ParentID = 13
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1freq_min: TcxDBEditorRow
      Expanded = False
      Properties.Caption = 'Min '#1095#1072#1089#1090#1086#1090#1072' [MHz]'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Alignment.Vert = taVCenter
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'freq_min'
      Visible = False
      ID = 16
      ParentID = 13
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1freq_max: TcxDBEditorRow
      Expanded = False
      Properties.Caption = 'Max '#1095#1072#1089#1090#1086#1090#1072' [MHz]'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Alignment.Vert = taVCenter
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'freq_max'
      Visible = False
      ID = 17
      ParentID = 13
      Index = 3
      Version = 1
    end
    object row_freq_MHz: TcxDBEditorRow
      Expanded = False
      Properties.Caption = #1056#1072#1089#1095#1077#1090'. '#1095#1072#1089#1090#1086#1090#1072' [MHz]'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Alignment.Vert = taVCenter
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'tx_freq_MHz'
      ID = 18
      ParentID = 13
      Index = 4
      Version = 1
    end
    object cxDBVerticalGrid1_bitrate_Mbps: TcxDBEditorRow
      Expanded = False
      Properties.Caption = #1057#1082#1086#1088#1086#1089#1090#1100' '#1087#1077#1088#1077#1076#1072#1095#1080' (Mb/s)'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Alignment.Vert = taVCenter
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'bitrate_Mbps'
      ID = 19
      ParentID = 13
      Index = 5
      Version = 1
    end
    object cxDBVerticalGrid1E1: TcxDBEditorRow
      Expanded = False
      Properties.Caption = #1057#1082#1086#1088#1086#1089#1090#1100' '#1087#1077#1088#1077#1076#1072#1095#1080' (E1)'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Alignment.Vert = taVCenter
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'E1'
      Visible = False
      ID = 20
      ParentID = 13
      Index = 6
      Version = 1
    end
    object CategoryRow_Rains: TcxCategoryRow
      Properties.Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1081' '#1091#1095#1077#1090' '#1086#1089#1072#1076#1082#1086#1074
      ID = 21
      ParentID = -1
      Index = 2
      Version = 1
    end
    object row_Is_CalcWithAdditionalRain: TcxDBEditorRow
      Properties.Caption = #1059#1095#1077#1089#1090#1100' '#1074' '#1086#1073#1097#1077#1081' '#1087#1088#1080#1075#1086#1076#1085#1086#1089#1090#1080
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.ImmediatePost = True
      Properties.EditProperties.NullStyle = nssUnchecked
      Properties.EditProperties.UseAlignmentWhenInplace = True
      Properties.EditProperties.OnEditValueChanged = row_Is_CalcWithAdditionalRainEditPropertiesEditValueChanged
      Properties.DataBinding.FieldName = 'Is_CalcWithAdditionalRain'
      ID = 22
      ParentID = 21
      Index = 0
      Version = 1
    end
    object row__Rain_Intensity: TcxDBEditorRow
      Expanded = False
      Properties.Caption = #1048#1085#1090#1077#1085#1089#1080#1074#1085#1086#1089#1090#1100' [mm/h] (0..1000)'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Alignment.Vert = taVCenter
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = False
      Properties.EditProperties.OnValidate = row__IntensityEditPropertiesValidate
      Properties.DataBinding.FieldName = 'rain_Intensity'
      ID = 23
      ParentID = 21
      Index = 1
      Version = 1
    end
    object row__IsCalcLength: TcxDBEditorRow
      Expanded = False
      Properties.Caption = #1056#1072#1089#1095#1105#1090' '#1076#1083#1080#1085#1099' '#1076#1086#1078#1076'. '#1091#1095#1072#1089#1090#1082#1072
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.Alignment = taLeftJustify
      Properties.EditProperties.ImmediatePost = True
      Properties.EditProperties.NullStyle = nssUnchecked
      Properties.EditProperties.ReadOnly = False
      Properties.EditProperties.UseAlignmentWhenInplace = True
      Properties.EditProperties.ValueChecked = 'True'
      Properties.EditProperties.ValueGrayed = ''
      Properties.EditProperties.ValueUnchecked = 'False'
      Properties.DataBinding.FieldName = 'rain_IsCalcLength'
      ID = 24
      ParentID = 21
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1polarization: TcxDBEditorRow
      Expanded = False
      Properties.Caption = #1055#1086#1083#1103#1088#1080#1079#1072#1094#1080#1103
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Alignment.Vert = taVCenter
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'polarization_str'
      ID = 25
      ParentID = 21
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid1Signal_Quality: TcxDBEditorRow
      Expanded = False
      Properties.Caption = #1055#1088#1080#1075#1086#1076#1085#1086#1089#1090#1100' '#1074' '#1091#1089#1083'. '#1086#1089#1072#1076#1082#1086#1074
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Alignment.Vert = taVCenter
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'rain_Signal_Quality'
      ID = 26
      ParentID = 21
      Index = 4
      Version = 1
    end
    object cxDBVerticalGrid1Rain_Length: TcxDBEditorRow
      Expanded = False
      Properties.Caption = #1044#1083#1080#1085#1072' '#1076#1086#1078#1076'. '#1091#1095#1072#1089#1090#1082#1072' (km)'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Alignment.Vert = taVCenter
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'Rain_Length_km'
      ID = 27
      ParentID = 21
      Index = 5
      Version = 1
    end
    object cxDBVerticalGrid1Weakening_vert: TcxDBEditorRow
      Expanded = False
      Properties.Caption = #1054#1089#1083#1072#1073#1083#1077#1085#1080#1077' (V) [dB]'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Alignment.Vert = taVCenter
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'rain_Weakening_vert'
      ID = 28
      ParentID = 21
      Index = 6
      Version = 1
    end
    object cxDBVerticalGrid1Weakening_horz: TcxDBEditorRow
      Expanded = False
      Properties.Caption = #1054#1089#1083#1072#1073#1083#1077#1085#1080#1077' (H) [dB]'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Alignment.Vert = taVCenter
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'rain_Weakening_horz'
      ID = 29
      ParentID = 21
      Index = 7
      Version = 1
    end
    object cxDBVerticalGrid1Allowable_Intense_vert: TcxDBEditorRow
      Expanded = False
      Properties.Caption = #1044#1086#1087#1091#1089#1090'. '#1080#1085#1090#1077#1085#1089'. (V) [mm/h]'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Alignment.Vert = taVCenter
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'rain_Allowable_Intense_vert'
      ID = 30
      ParentID = 21
      Index = 8
      Version = 1
    end
    object cxDBVerticalGrid1Allowable_Intense_horz: TcxDBEditorRow
      Expanded = False
      Properties.Caption = #1044#1086#1087#1091#1089#1090'. '#1080#1085#1090#1077#1085#1089'. (H) [mm/h]'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Alignment.Vert = taVCenter
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'rain_Allowable_Intense_horz'
      ID = 31
      ParentID = 21
      Index = 9
      Version = 1
    end
    object cxDBVerticalGrid1CategoryRow4: TcxCategoryRow
      ID = 32
      ParentID = -1
      Index = 3
      Version = 1
    end
    object row_Space_spread: TcxDBEditorRow
      Expanded = False
      Properties.Caption = #1055#1088#1086#1089#1090#1088' '#1088#1072#1079#1085#1077#1089#1077#1085#1080#1077' [m]'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = False
      Properties.DataBinding.FieldName = 'SPACE_SPREAD_m'
      Properties.Options.Editing = False
      ID = 33
      ParentID = -1
      Index = 4
      Version = 1
    end
    object row_Frenel_max: TcxDBEditorRow
      Expanded = False
      Properties.Caption = #1064#1080#1088#1080#1085#1072' 1-'#1081' '#1079#1086#1085#1099' '#1060#1088#1077#1085#1077#1083#1103' [m]'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.MaxLength = 1
      Properties.EditProperties.ReadOnly = False
      Properties.DataBinding.FieldName = 'FRENEL_MAX_M'
      Properties.Options.Editing = False
      ID = 34
      ParentID = -1
      Index = 5
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.AssignedValues.DisplayFormat = True
      Properties.EditProperties.MaxValue = 1000.000000000000000000
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'rain_Intensity'
      Visible = False
      ID = 35
      ParentID = -1
      Index = 6
      Version = 1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 713
    Width = 689
    Height = 40
    Align = alBottom
    Caption = 'Panel1'
    TabOrder = 2
    Visible = False
    object Button1: TButton
      Left = 16
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 0
    end
    object Button2: TButton
      Left = 112
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Button2'
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object ActionList1: TActionList
    Left = 15
    Top = 137
    object act_Calc: TAction
      Caption = #1056#1072#1089#1095#1077#1090
      OnExecute = act_CalcExecute
    end
    object act_Opti: TAction
      Caption = #1054#1087#1090#1080#1084#1080#1079#1080#1088#1086#1074#1072#1090#1100' '#1074#1099#1089#1086#1090#1099' '#1072#1085#1090#1077#1085#1085
      OnExecute = act_CalcExecute
    end
    object act_calc_params: TAction
      Caption = #1043#1088#1072#1092#1080#1082#1080
      OnExecute = act_CalcExecute
    end
    object act_Opti_new: TAction
      Caption = 'act_Opti_new'
      OnExecute = act_CalcExecute
    end
    object act_Calc_Adaptive: TAction
      Caption = #1056#1072#1089#1095#1077#1090' '#1089' '#1091#1095#1077#1090#1086#1084' '#1040#1052
      Hint = #1056#1072#1089#1095#1077#1090' '#1089' '#1091#1095#1077#1090#1086#1084' '#1040#1052
      OnExecute = act_CalcExecute
    end
    object act_opt_equipment: TAction
      Caption = 'Opt'
      OnExecute = act_CalcExecute
    end
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc1
    Left = 98
    Top = 72
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    ProcedureName = 'sp_Link_Summary'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 1
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 91957
      end>
    Prepared = True
    Left = 96
    Top = 128
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 288
    Top = 192
    PixelsPerInch = 96
    object cxStyle_Red: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clRed
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
    end
    object cxStyle_ReadOnly: TcxStyle
      AssignedValues = [svColor]
      Color = clBtnFace
    end
  end
  object ds_Link: TDataSource
    DataSet = qry_Link1
    Left = 16
    Top = 192
  end
  object qry_Link1: TADOQuery
    Parameters = <>
    Left = 16
    Top = 248
  end
  object dxMemData_Calc_method: TdxMemData
    Active = True
    Indexes = <>
    SortOptions = []
    Left = 152
    Top = 248
    object dxMemData_Calc_methodname: TStringField
      FieldName = 'name'
      Size = 50
    end
    object dxMemData_Calc_methodid: TIntegerField
      FieldName = 'id'
    end
  end
  object ds_dxMemData_Calc_method: TDataSource
    DataSet = dxMemData_Calc_method
    Left = 152
    Top = 192
  end
  object cxPropertiesStore: TcxPropertiesStore
    Active = False
    Components = <>
    StorageName = 'cxPropertiesStore'
    StorageType = stRegistry
    Left = 284
    Top = 254
  end
  object ApplicationEvents1: TApplicationEvents
    OnMessage = ApplicationEvents1Message
    Left = 392
    Top = 192
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = ''
    NotDocking = [dsNone, dsTop, dsRight, dsBottom]
    PopupMenuLinks = <>
    Style = bmsXP
    SunkenBorder = True
    UseSystemFont = True
    Left = 424
    Top = 256
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar2: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 2'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 1504
      FloatTop = 133
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton4'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton5'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarManager1Bar1: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 26
      DockingStyle = dsTop
      FloatLeft = 1211
      FloatTop = 192
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 192
          Visible = True
          ItemName = 'dxBarLookupCombo1'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 158
          Visible = True
          ItemName = 'cxBarEditItem4'
        end
        item
          BeginGroup = True
          UserDefine = [udWidth]
          UserWidth = 38
          Visible = True
          ItemName = 'cxBarEditItem1'
        end
        item
          Visible = True
          ItemName = 'dxBarControlContainerItem1'
        end>
      OneOnRow = True
      Row = 1
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarButton1: TdxBarButton
      Action = act_Calc_Adaptive
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = act_Calc
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = act_Opti
      Category = 0
    end
    object cxBarEditItem2: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      BarStyleDropDownButton = False
      Properties.DropDownAutoSize = True
      Properties.DropDownListStyle = lsFixedList
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'id'
      Properties.ListColumns = <
        item
          FieldName = 'name'
        end>
      Properties.ListOptions.ShowHeader = False
      Properties.ListSource = ds_dxMemData_Calc_method
    end
    object dxBarButton4: TdxBarButton
      Action = act_calc_params
      Category = 0
    end
    object dxBarButton5: TdxBarButton
      Action = act_opt_equipment
      Category = 0
    end
    object cxBarEditItem1: TcxBarEditItem
      Align = iaRight
      Caption = #1059#1095#1105#1090' '#1087#1072#1089#1089#1080#1074#1085#1099#1093' '#1101#1083#1077#1084#1077#1085#1090#1086#1074
      Category = 0
      Hint = #1059#1095#1105#1090' '#1087#1072#1089#1089#1080#1074#1085#1099#1093' '#1101#1083#1077#1084#1077#1085#1090#1086#1074
      Visible = ivAlways
      ShowCaption = True
      Width = 100
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.Alignment = taCenter
      Properties.NullStyle = nssUnchecked
    end
    object cxBarEditItem3: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.ListColumns = <>
    end
    object dxBarControlContainerItem1: TdxBarControlContainerItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
    end
    object dxBarLookupCombo1: TdxBarLookupCombo
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDD000000000000000D0FFFF0FFFFFFFF0D0F77F0F777777F0D0CCCC0CCCCCC
        CC0D0C77C0C777777C0D0CCCC0CCCCCCCC0D0F77F0F777777F0D0FFFF0FFFFFF
        FF0D0F77F0F777777F0D0FFFF0FFFFFFFF0D000000000000000D0FFFCCCCFFF0
        DDDD0F777777FFF0DDDD0FFFCCCCFFF0DDDD000000000000DDDD}
      Width = 100
      ShowEditor = False
      ImmediateDropDown = True
      KeyField = 'id'
      ListField = 'name'
      ListSource = ds_dxMemData_Calc_method
      RowCount = 7
    end
    object cxBarEditItem4: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.DropDownAutoSize = True
      Properties.DropDownListStyle = lsFixedList
      Properties.KeyFieldNames = 'id'
      Properties.ListColumns = <
        item
          FieldName = 'name'
        end>
      Properties.ListSource = ds_dxMemData_Calc_method
    end
  end
  object cxLookAndFeelController1: TcxLookAndFeelController
    Kind = lfFlat
    NativeStyle = True
    SkinName = 'Office2013LightGray'
    Left = 248
    Top = 120
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Password=sa;Persist Security Info=True;User I' +
      'D=sa;Data Source=onega_link'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 488
    Top = 88
  end
end
