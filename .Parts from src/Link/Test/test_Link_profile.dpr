
program test_Link_profile;

uses
  Forms,
  a_Unit in 'a_Unit.pas' {Form6},
  d_expl_Object_Select in '..\..\Explorer\d_expl_Object_Select.pas' {dlg_expl_Select_Object},
  d_Link_add in '..\d_Link_add.pas' {dlg_Link_add},
  d_LinkEnd_add in '..\..\LinkEnd\d_LinkEnd_add.pas' {dlg_LinkEnd_add},
  d_Map_Desktops in '..\..\MapDesktop\d_Map_Desktops.pas' {dlg_Map_Desktops},
  d_Report_Select in '..\..\Report\d_Report_Select.pas' {dlg_Report_Select},
  dm_Act_Link in '..\dm_act_Link.pas' {dmAct_Link: TDataModule},
  dm_Act_LinkEnd in '..\..\LinkEnd\dm_Act_LinkEnd.pas' {dmAct_LinkEnd: TDataModule},
  dm_Act_Map_Engine in '..\..\MapEngine\dm_Act_Map_Engine.pas' {dmAct_Map_Engine: TDataModule},
  dm_act_Rel_Engine in '..\..\RelEngine\dm_act_Rel_Engine.pas' {dmAct_Rel_Engine: TDataModule},
  dm_Act_Report in '..\..\Report\dm_Act_Report.pas' {dmAct_Report1: TDataModule},
  dm_Explorer in '..\..\Explorer\dm_Explorer.pas' {dmExplorer: TDataModule},
  dm_Link in '..\dm_Link.pas' {dmLink: TDataModule},
  dm_Link_Items_View in '..\dm_Link_Items_View.pas' {dmLink_Items_View: TDataModule},
  dm_Link_Summary in '..\dm_Link_Summary.pas' {dmLink_Summary: TDataModule},
  dm_LinkEnd_View in '..\..\LinkEnd\dm_LinkEnd_View.pas' {dmLinkEnd_View: TDataModule},
  dm_LinkLineType in '..\..\LinkLineType\dm_LinkLineType.pas' {dmLinkLineType: TDataModule},
  dm_Main in '..\..\DataModules\dm_Main.pas' {dmMain: TDataModule},
  dm_Object_base in '..\..\Common\dm_Object_base.pas' {dmObject_base: TDataModule},
  dm_Report in '..\..\Report\dm_Report.pas' {dmReport: TDataModule},
  f_Log in '..\..\Forms\f_Log.pas' {frm_Log},
  f_Map in '..\..\Map\f_Map.pas' {frm_Map},
  fr_Browser_List in '..\..\Explorer\fr_Browser_List.pas' {frame_Browser_List},
  fr_dlg_Link_add in '..\fr_dlg_Link_add.pas' {frame_Link_add},
  fr_Explorer_base in '..\..\Explorer\fr_Explorer_base.pas' {frame_Explorer_base},
  fr_Link_calc_results in '..\fr_Link_calc_results.pas' {frame_Link_calc_results},
  fr_Link_items in '..\fr_Link_items.pas' {frame_Link_items},
  fr_Link_map in '..\fr_Link_map.pas' {frame_Link_Map},
  fr_Link_params in '..\fr_Link_params.pas' {frame_Link_Params},
  fr_Link_profile_ant in '..\fr_Link_profile_ant.pas' {frame_Link_profile_ant},
  fr_Link_view in '..\fr_Link_view.pas' {frame_Link_View},
  fr_Map in '..\..\Map\fr_Map.pas' {frame_Map},
  fr_Profile_Rel in '..\..\Profile\fr_profile_Rel.pas' {frame_profile_Rel},
  fr_profile_Rel_Sites in '..\..\Profile\fr_profile_Rel_Sites.pas' {frame_profile_Rel_Sites},
  fr_View_base in '..\..\Explorer\fr_View_base.pas' {frame_View_Base},
  test_f_Export_data in 'test_f_Export_data.pas' {frm_Link_Calc},
  u_const_msg in '..\..\u_const_msg.pas',
  u_Link_const in '..\u_Link_const.pas',
  d_Report_Add in '..\..\Report\d_Report_Add.pas' {dlg_Report_Add},
  fr_Link_profile_summary in '..\fr_Link_profile_summary.pas' {frame_Link_profile_summary},
  dm_Antenna in '..\..\Antenna\dm_Antenna.pas' {dmAntenna: TDataModule},
  fr_Profile_Rel_base in '..\..\Profile\fr_Profile_Rel_base.pas' {frame_Profile_Rel_base},
  dm_Link_calc in '..\dm_Link_calc.pas' {dmLink_calc: TDataModule},
  fr_Link_reflection_points in '..\fr_Link_reflection_points.pas' {frame_Link_reflection_points},
  dm_AntType_Import in '..\..\AntennaType\dm_AntType_Import.pas' {dmAntType_Import: TDataModule},
  d_Property_Move in '..\..\Property\d_Property_Move.pas' {dlg_Property_Move},
  dm_act_Antenna in '..\..\Antenna\dm_act_Antenna.pas' {dmAct_Antenna: TDataModule},
  dm_act_LinkFreqPlan_Link in '..\..\LinkFreqPlan\dm_act_LinkFreqPlan_Link.pas' {dmAct_LinkFreqPlan_Link: TDataModule},
  dm_Custom in '..\..\Common\dm_Custom.pas' {dmCustom: TDataModule},
  dm_Rel_Engine in '..\..\RelEngine\dm_Rel_Engine.pas' {dmRel_Engine: TDataModule},
  dm_LinkLine_link in '..\..\LinkLine\dm_LinkLine_link.pas' {dmLinkLine_link: TDataModule},
  dm_Region in '..\..\Region\dm_Region.pas' {dmRegion: TDataModule},
  dm_act_LinkLineType in '..\..\LinkLineType\dm_act_LinkLineType.pas' {dmAct_LinkLineType: TDataModule},
  dm_act_Region in '..\..\Region\dm_act_Region.pas' {dmAct_Region: TDataModule},
  dm_Filter in '..\..\Filter\dm_Filter.pas' {dmFilter: TDataModule},
  fr_Explorer in '..\..\Explorer\fr_Explorer.pas' {frame_Explorer},
  dm_Link_Climate in '..\dm_Link_Climate.pas' {dmLink_Climate: TDataModule},
  dm_RelMatrixFile in '..\..\RelMatrixFile\dm_RelMatrixFile.pas' {dmRelMatrixFile: TDataModule},
  fr_Link_profile in '..\fr_Link_profile.pas' {frame_Link_profile},
  u_Geo in '..\..\..\..\common\u_GEO.pas',
  fr_DBInspector in '..\..\Explorer\fr_DBInspector.pas' {frame_DBInspector},
  d_Link_climate in '..\d_Link_climate.pas' {dlg_Link_Climate},
  dm_Link_report in '..\dm_Link_report.pas' {dmLink_report: TDataModule},
  d_Property_add in '..\..\Property\d_Property_add.pas' {dlg_Property_add},
  dm_act_Property in '..\..\Property\dm_act_Property.pas' {dmAct_Property: TDataModule},
  fr_LinkEnd_view in '..\..\LinkEnd\fr_LinkEnd_view.pas' {frame_LinkEnd_View},
  fr_Object_inspector in '..\..\Common\fr_Object_inspector.pas' {frame_Object_inspector},
  dm_LinkEndType in '..\..\LinkEndType\dm_LinkEndType.pas' {dmLinkEndType: TDataModule},
  dm_LinkEnd in '..\..\LinkEnd\dm_LinkEnd.pas' {dmLinkEnd: TDataModule},
  fr_Link_inspector in '..\fr_Link_inspector.pas' {frame_Link_inspector},
  fr_PMP_Sector_inspector in '..\..\PMP\fr_PMP_Sector_inspector.pas' {frame_PMP_Sector_inspector},
  u_LinkLine_const in '..\..\LinkLine\u_linkLine_const.pas',
  fr_Antenna_inspector in '..\..\Antenna\fr_Antenna_inspector.pas' {frame_Antenna_inspector},
  dm_View_Engine in '..\..\Explorer\dm_View_Engine.pas' {dmViewEngine: TDataModule};

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(TForm6, Form6);
  Application.CreateForm(TdmViewEngine, dmViewEngine);
  Application.Run;
end.
