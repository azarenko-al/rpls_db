
program test_Link;

uses
  a_Unit in 'a_Unit.pas' {Form6},
  d_Antenna_add in '..\..\Antenna\d_Antenna_add.pas' {dlg_Antenna_add},
  d_Antenna_Move in '..\..\Antenna\d_Antenna_Move.pas' {dlg_Antenna_Move},
  d_Link_add in '..\d_Link_add.pas' {dlg_Link_add},
  d_Link_add_from_Profile in '..\d_Link_add_from_Profile.pas' {dlg_Link_add_from_Profile},
  d_Link_climate in '..\d_Link_climate.pas' {dlg_Link_Climate},
  d_Link_Optimize_freq_diversity in '..\d_Link_Optimize_freq_diversity.pas' {dlg_Link_Optimize_freq_diversity},
  dm_act_Antenna in '..\..\Antenna\dm_Act_Antenna.pas' {dmAct_Antenna: TDataModule},
  dm_Act_Link in '..\dm_act_Link.pas' {dmAct_Link: TDataModule},
  dm_Act_LinkEnd in '..\..\LinkEnd\dm_Act_LinkEnd.pas' {dmAct_LinkEnd: TDataModule},
  dm_act_LinkFreqPlan_Link in '..\..\LinkFreqPlan\dm_Act_LinkFreqPlan_Link.pas' {dmAct_LinkFreqPlan_Link: TDataModule},
  dm_act_Property in '..\..\Property\dm_act_Property.pas' {dmAct_Property: TDataModule},
  dm_act_Rel_Engine in '..\..\RelEngine\dm_act_Rel_Engine.pas' {dmAct_Rel_Engine: TDataModule},
  dm_ClutterModel in '..\..\Clutters\dm_ClutterModel.pas' {dmClutterModel: TDataModule},
  dm_Custom in '..\..\.common\dm_Custom.pas' {dmCustom: TDataModule},
  dm_Link in '..\dm_Link.pas' {dmLink: TDataModule},
  dm_Link_calc_SESR in '..\dm_Link_Calc_SESR.pas' {dmLink_calc_SESR: TDataModule},
  dm_Link_Climate in '..\dm_Link_Climate.pas' {dmLink_Climate: TDataModule},
  dm_Link_Items_View in '..\dm_Link_Items_View.pas' {dmLink_Items_View: TDataModule},
  dm_Link_profile in '..\dm_Link_profile.pas' {dmLink_profile: TDataModule},
  dm_Link_Reflection_Points in '..\dm_Link_Reflection_Points.pas' {dmLink_Reflection_Points: TDataModule},
  dm_Link_Summary in '..\dm_Link_Summary.pas' {dmLink_Summary: TDataModule},
  dm_Link_tools in '..\dm_Link_tools.pas' {dmLink_tools: TDataModule},
  dm_LinkEnd in '..\..\LinkEnd\dm_LinkEnd.pas' {dmLinkEnd: TDataModule},
  dm_LinkEnd_tools in '..\..\LinkEnd\dm_LinkEnd_tools.pas' {dmLinkEnd_tools: TDataModule},
  dm_LinkEnd_View in '..\..\LinkEnd\dm_LinkEnd_View.pas' {dmLinkEnd_View: TDataModule},
  dm_LinkEndType in '..\..\LinkEndType\dm_LinkEndType.pas' {dmLinkEndType: TDataModule},
  dm_LinkLine_link in '..\..\LinkLine\dm_LinkLine_link.pas' {dmLinkLine_link: TDataModule},
  dm_Main in '..\..\DataModules\dm_Main.pas' {dmMain: TDataModule},
  dm_Object_base in '..\..\Common\dm_Object_base.pas' {dmObject_base: TDataModule},
  Forms,
  fr_dlg_Link_add in '..\fr_dlg_Link_add.pas' {frame_Link_add},
  fr_dlg_Link_add_from_Profile in '..\fr_dlg_Link_add_from_Profile.pas' {frame_Link_add_from_Profile},
  fr_Link_calc_results in '..\fr_Link_calc_results.pas' {frame_Link_calc_results},
  fr_Link_inspector in '..\fr_Link_inspector.pas' {frame_Link_inspector},
  fr_Link_params in '..\fr_Link_params.pas' {frame_Link_Params},
  fr_Link_Passive_profile in '..\fr_Link_Passive_profile.pas' {frame_Link_passive_profile},
  fr_Link_profile in '..\fr_Link_profile.pas' {frame_Link_profile},
  fr_Link_profile_ant in '..\fr_Link_profile_ant.pas' {frame_Link_profile_ant},
  fr_Link_profile_summary in '..\fr_Link_profile_summary.pas' {frame_Link_profile_summary},
  fr_Link_reflection_points in '..\fr_Link_reflection_points.pas' {frame_Link_reflection_points},
  fr_Link_view in '..\fr_Link_view.pas' {frame_Link_View},
  fr_LinkEnd_inspector in '..\..\LinkEnd\fr_LinkEnd_inspector.pas' {frame_LinkEnd_inspector},
  fr_LinkEnd_view in '..\..\LinkEnd\fr_LinkEnd_view.pas' {frame_LinkEnd_View},
  fr_LinkLine_inspector in '..\..\LinkLine\fr_LinkLine_inspector.pas' {frame_LinkLine_inspector},
  fr_View_base in '..\..\Explorer\fr_View_base.pas' {frame_View_Base},
  I_Folder in '..\..\Folder\I_Folder.pas',
  u_Link_const in '..\u_Link_const.pas',
  u_Link_Report_Lib in '..\u_Link_report_lib.pas',
  u_LinkLine_const in '..\..\LinkLine\u_linkLine_const.pas',
  d_LinkEnd_add in '..\..\LinkEnd\d_LinkEnd_add.pas' {dlg_LinkEnd_add};

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(TForm6, Form6);
  Application.Run;
end.
