unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ExtCtrls, ADODB, StdCtrls,

  dm_Main,

  d_Link_Get_network_type_new
  ;

type
  TForm1 = class(TForm)
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
var
  rec: Tdlg_Link_Get_network_type_rec_ ;
begin

  TdmMain.Init;
  dmMain.OpenDB_reg;

  FillChar(rec,SizeOf(rec),0);

  rec.Length_km:=30;
  rec.SESR :=0.33;
  rec.Kng :=0.3443;

  Tdlg_Link_Get_network_type_new.ExecDlg(rec);
  
end;

end.
