object frm_Link_Calc: Tfrm_Link_Calc
  Left = 331
  Top = 95
  Width = 696
  Height = 574
  Caption = 'frm_Link_Calc'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 604
    Top = 504
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 688
    Height = 193
    ActivePage = TabSheet1
    Align = alTop
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 297
        Height = 165
        Align = alLeft
        DataSource = dmLink_calc.ds_Link
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object DBGrid2: TDBGrid
        Left = 323
        Top = 0
        Width = 357
        Height = 165
        Align = alRight
        DataSource = dmLink_calc.ds_LinkType
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
  end
  object PageControl2: TPageControl
    Left = 0
    Top = 193
    Width = 688
    Height = 148
    ActivePage = TabSheet2
    Align = alTop
    TabOrder = 2
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      object DBGrid3: TDBGrid
        Left = 0
        Top = 0
        Width = 297
        Height = 120
        Align = alLeft
        DataSource = dmLink_calc.ds_LinkEnd1
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object DBGrid4: TDBGrid
        Left = 324
        Top = 0
        Width = 356
        Height = 120
        Align = alRight
        DataSource = dmLink_calc.ds_LinkEnd2
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
  end
  object PageControl3: TPageControl
    Left = 0
    Top = 341
    Width = 688
    Height = 142
    ActivePage = TabSheet3
    Align = alTop
    TabOrder = 3
    object TabSheet3: TTabSheet
      Caption = 'TabSheet3'
      object DBGrid5: TDBGrid
        Left = 0
        Top = 0
        Width = 297
        Height = 114
        Align = alLeft
        DataSource = dmLink_calc.ds_Antennas1
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object DBGrid6: TDBGrid
        Left = 320
        Top = 0
        Width = 360
        Height = 114
        Align = alRight
        DataSource = dmLink_calc.ds_Antennas2
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
  end
  object FormStorage1: TFormStorage
    StoredValues = <>
    Left = 172
    Top = 32
  end
end
