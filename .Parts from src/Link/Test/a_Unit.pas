unit a_Unit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, rxPlacemnt, ExtCtrls, ComCtrls, Menus,



  dm_Link,


  dm_Main,
  I_Options,

  dm_LinkLine,

  fr_Link_passive_profile,

  fr_Explorer_Container,
  fr_Browser,

  d_Link_add_from_Profile,

  fr_Link_View,
  fr_LinkEnd_View,


  dm_Act_LinkEndType,

  dm_Act_Map_Engine,

  dm_Act_Explorer,
  dm_Act_Project,

  f_Log,
  u_db,
  u_func_msg,

  u_func,

  u_const,
  u_const_msg,
  u_const_db,

  u_types,



  Db //, dxmdaset, PickDlg

  ;

type
  TForm6 = class(TForm)
    PageControl1: TPageControl;
    p_Main: TTabSheet;
    Panel1: TPanel;
    Splitter1: TSplitter;
    pn_Browser: TPanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    btnPick: TButton;
    FormStorage1: TFormStorage;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure btnPickClick(Sender: TObject);
  private
    Fframe_Links: Tframe_Explorer_Container;

    Fframe_Browser: Tframe_Browser; 


//    Fframe_Links : Tframe_Explorer_Container;
 //   Fframe_Browser : Tframe_Browser;


  //  Fframe_Link_View: Tframe_Link_View;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form6: TForm6;


implementation
  {$R *.dfm}

uses
 u_Log,

//  dm_act_Site,
  I_Folder,

  dm_Act_Link,
  dm_Act_LinkEnd,
  dm_act_LinkLine,

  dm_act_Antenna,
  dm_Act_Report,
  dm_Act_MapFile,
  dm_act_Filter,

  dm_act_LinkFreqPlan_Link,
  dm_act_Property,

  dm_Act_MapDesktop,
  dm_act_LinkNet,

  u_reg,

  dm_act_Rel_Engine,

  dm_Object_base;



procedure TForm6.FormDestroy(Sender: TObject);
var
//  i: integer;
  s1,sClassName: string;
begin


 // btnPickClick(nil);


 // Fframe_Link_View.Free;

{
  Fframe_Links.OnNodeFocused:=nil;

  Fframe_Browser.Free;
  Fframe_Links.Free;}

  inherited;

end;

// -------------------------------------------------------------------
procedure TForm6.FormCreate(Sender: TObject);
// -------------------------------------------------------------------
var
  iLink: Integer;
  iProjectID: Integer;
  // aRec: TdmLinkPassiveInfoRec;
begin
  Tfrm_Log.CreateForm();



  TdmMain.Init;
  if not dmMain.OpenFromReg then
    Exit;



//  dmLinkLine.GetPassiveInfo(303, aRec);



  TdmAct_Property.Init;
  TdmAct_LinkLine.Init;

  TdmAct_Link.Init;
  //TdmAct_Site.Init;
  TdmAct_LinkEnd.Init;


  TdmAct_Report.Init;
  TdmAct_Rel_Engine.Init;

  TdmAct_LinkFreqPlan_Link.Init;
  TdmAct_LinkNet.Init;
  TdmAct_Antenna.Init;
  TdmAct_Filter.Init;


//  TdmAct_MapDesktop.Init;

  IOptions_Init;


  TdmAct_LinkEndType.Init;


//  dmAct_Map_Engine.Enabled:=False;

// IShell));






 // gl_Reg:=TRegStore_Create;

 // gl_Reg:=TRegStore_Create(REGISTRY_ROOT);

//  g_Log:=TLog.Create('c:\temp\rpls\_error_log.txt');

{

  TdmAct_MapFile.Init;

  TdmAct_Folder.Init;
}

{



}

{


}


//  dm_Custom.init_UpdateADOConnections;
  //dmAct_Link;

  pn_Browser.Align:=alClient;

 // g_RegistryRootPath:=REGISTRY_ROOT;
 // gl_Reg.InitRegistry(REGISTRY_ROOT);



  TdmAct_Explorer.Init;

{

  // -------------------------------------------------------------------
  TdmMain.Init;
  IShellFactory_Init (dmMain);
  // -------------------------------------------------------------------

  dmMain.OpenDlg;

  dmAct_Map_Engine.Enabled:=False;


  dmAct_Project.LoadLastProject;

}



 { Fframe_Link_View:=Tframe_Link_View.Create(Application);

  Fframe_Link_View.View(47681);
  Fframe_Link_View.Sho
  }


{
  CreateChildForm(Tframe_Explorer_Container, Fframe_Links,  p_Main);


 // Fframe_Links := Tframe_Explorer_Container.CreateChildForm( ts_Link);

  Fframe_Links.SetViewObjects([otReport,otPmpSite,otProperty,
                                 otLink,otLinkLine,otLinkEnd]);
  Fframe_Links.RegPath:='Link';


  CreateChildForm(Tframe_Browser, Fframe_Browser,  pn_Browser);

//  Fframe_Browser := Tframe_Browser.CreateChildForm( pn_Browser);

  Fframe_Links.OnNodeFocused:=Fframe_Browser.ViewObjectByPIDL;

}

  TdmAct_Project.Init;

//  dmAct_Project.LoadLastProject;



  iLink:=dmMain.ProjectID;

  {
  with Tframe_Link_passive_profile.Create(Self) do
  begin
    View(305);
    Show;
  end;
  }


{

 with Tframe_LinkEnd_View.Create(Self) do
  begin
    View(103176,'');
    ShowModal;
  end;

}


 // dmAct_Link.Add(0);

////////
 ////// Tdlg_Link_add_from_Profile_Test ;


  iLink:=gl_DB.GetMaxID(TBL_LINK1 );

  iProjectID:=dmLink.GetIntFieldValue(iLink, FLD_PROJECT_ID);

  dmAct_Project.Dlg_Open(iProjectID);


  with Tframe_Link_View.Create(Self) do
  begin
  //..  View(54329,'');
    View(iLink,'');
  //  View(53912,'');
    ShowModal;
  end;



end;


procedure TForm6.Button1Click(Sender: TObject);
begin
//  dmAct_Project.Browse;
end;



procedure TForm6.Button3Click(Sender: TObject);
begin
//LinkReport_Test;

//  dmLink_print.Link_SaveToImage (31613, 't:\wwwwwwwww.bmp');

end;



procedure TForm6.btnPickClick(Sender: TObject);
var
  s1,s2: string;
  i: integer;
begin

//  PickDialog1.PickItems.Clear;


  for i:=Application.ComponentCount-1  downto 0 do
  begin
    s1:=Application.Components[i].Name;
    s2:=Application.Components[i].ClassName;

  //  PickDialog1.PickItems.Add(s1+'  -  '+s2);
//    Application.Components[i].Free;

//    Application.Components[i].RemoveComponent();

  end;

  //PickDialog1.Execute;

end;


var
 i,j: integer;
 d1,d2: double;
begin

  d1:=10;
  d2:=0.2;
  i:=125;


  j:=Trunc(d1/d2);
  j:=8;


end.


var
  s: string;
  i: Integer;
begin
{


