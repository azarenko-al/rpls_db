unit fr_Link_reflection_points;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
    ToolWin, ComCtrls,
   ADODB, dxmdaset, DB, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,

  u_storage,

  dm_Main,

  u_const,
  u_db,
  u_func,

  u_const_db,


  cxGridCustomView, cxGrid

  , cxGraphics;

type
  Tframe_Link_reflection_points = class(TForm)
    ToolBar2: TToolBar;
    DataSource1: TDataSource;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView2: TcxGridDBTableView;
    cxGrid1DBTableView2distance: TcxGridDBColumn;
    cxGrid1DBTableView2length: TcxGridDBColumn;
    cxGrid1DBTableView2radius: TcxGridDBColumn;
    qry_Temp: TADOQuery;
    cxGrid1DBTableView2DBColumn1: TcxGridDBColumn;
    col_signal_depression: TcxGridDBColumn;
//    procedure cxGrid1DBTableView1CustomDrawCell(Sender: TcxCustomGridTableView;
//        ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone:
//        Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
 //   FRegPath: string;
  public
    procedure View (aID: integer);

  end;


implementation

uses dm_Onega_db_data;

{$R *.DFM}


//--------------------------------------------------------------------
procedure Tframe_Link_reflection_points.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  cxGrid1.Align:=alClient;

//  dmLink_Reflection_Points.InitDB_ReflectionPoints (mem_Data);


  //FRegPath:= REGISTRY_FORMS + ClassName +'\';

  //cxGrid1DBTableView1.RestoreFromRegistry (FRegPath + cxGrid1DBTableView1.Name);

  db_SetComponentADOConnection(Self, dmMain.ADOConnection);

  g_Storage.RestoreFromRegistry(cxGrid1DBTableView2, className);

//  col_link_index1.Visible := False;


 // dx_CheckColumnSizes_DBTreeList (dxDBGrid1);
end;


procedure Tframe_Link_reflection_points.FormDestroy(Sender: TObject);
begin
//  cxGrid1DBTableView1.StoreToRegistry (FRegPath + cxGrid1DBTableView1.Name);
  g_Storage.StoreToRegistry(cxGrid1DBTableView2, className);

  inherited;
end;


// ---------------------------------------------------------------
procedure Tframe_Link_reflection_points.View(aID: integer);
// ---------------------------------------------------------------
const
  SQL_SELECT_POINTS =
//    'SELECT * FROM '+ TBL_Link_reflection_points + ' WHERE (link_id=:link_id) ORDER BY distance';
    'SELECT * FROM '+ TBL_Link_reflection_points + ' WHERE (link_id=:link_id)';// ORDER BY link_index';//, distance_km';

begin
 // dmLink_Reflection_Points.OpenDB_ReflectionPoints (aID, mem_Data);

////////////  db_OpenQuery(qry_Temp, SQL_SELECT_POINTS, [db_Par(FLD_LINK_ID, aID)]);


   dmOnega_DB_data.OpenQuery(qry_Temp, SQL_SELECT_POINTS, [db_Par(FLD_LINK_ID, aID)]);



(*

 db_SetFieldCaptions(qry_Temp,
     [
     // SArr(FLD_NAME, '��������'),
   //   SArr('checked',          '���'),
      SArr('LinkEnd.checked',  '���'),
      SArr('LinkEnd.Name',     '��������'),*)

end;


end.
