  unit d_Link_add_from_Profile;

interface

uses
  Classes, Graphics, Controls, Forms, Dialogs,
  ActnList,    StdCtrls, ExtCtrls,  ComCtrls, cxVGrid,  cxButtonEdit,

  d_Wizard_add_with_Inspector,


//  d_Wizard_Add_with_params,

  dm_User_Security,

  dm_MapEngine_store,



  dm_Library,


  dm_Main,

  I_Shell,

   I_Act_Profile,
  //I_Act_Explorer, //
//  dm_Act_Explorer,


  u_Geo,
  u_func,
  u_db,
  u_radio,
  u_dlg,
  u_reg,

  u_cx_vgrid,

  u_types,
  u_const_str,
  u_const_db,
  u_link_const,


  dm_ClutterModel,

  dm_Link,
  
  
  
  
  dm_LinkEndType,

  fr_dlg_Link_add_from_Profile, cxControls, cxInplaceContainer,
  cxPropertiesStore, rxPlacemnt, ImgList;

type
  TDlgLinkAddRec= record
                    BLVector: TBLVector;
                    Height1, Height2: double;
                    Freq_GHz: double;

                   // ProfileType: string;
                    ProfileType: TProfileBuildType;

                  //  FolderID: integer;

                   // RepeaterPos: TBLPoint;
                  end;



  Tdlg_Link_add_from_Profile = class(Tdlg_Wizard_add_with_Inspector)
    Button1: TButton;
    act_MakeLinkName: TAction;
    StatusBar1: TStatusBar;
    StatusBar2: TStatusBar;
    cxVerticalGrid1: TcxVerticalGrid;
    row_ClutterModel: TcxEditorRow;
    cxVerticalGrid1EditorRow2: TcxEditorRow;
    cxVerticalGrid1EditorRow3: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    row_Repeater11: TcxEditorRow;
    row_Repeater_property: TcxEditorRow;
    row_Antenna1_H: TcxEditorRow;
    row_Antenna2_H: TcxEditorRow;
    TabSheet_LinkEnds: TTabSheet;
    pn_Left: TPanel;
    Splitter1: TSplitter;
    pn_Right: TPanel;
    row_Template_site: TcxEditorRow;
    ImageList1: TImageList;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure FormResize(Sender: TObject);
    procedure act_MakeLinkNameExecute(Sender: TObject);
    procedure row_ClutterModel1EditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure row_Template_site_linkendEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);

  private
    Fframe_Link_add1: Tframe_Link_add_from_Profile;
    Fframe_Link_add2: Tframe_Link_add_from_Profile;

    FID,FFolderID: integer;
    FClutterMOdelID: integer;

    FTemplate_Site_ID: integer;



 
    procedure DoOnEquipmentChanged(Sender: TObject);

  private
    FVector: TBLVector;
//    function AddRepeater(aIndex: Integer): Integer;
    procedure UpdateEquipment(aDest_LinkEnd: integer);
    function CalcReserve: double;

    procedure Append;
 //   procedure Dlg_Apply_template;
  public
    function  MakeLinkName: string;
    procedure MakeLinkEndNames;

    class function ExecDlg (aRec: TDlgLinkAddRec): integer;

{                            aVector: TBLVector;
                            aHeight1, aHeight2: double;
                            aFreq: double;
                            aProfileType: string=''
                            ): integer;
}
  end;


  procedure Tdlg_Link_add_from_Profile_Test;


implementation
{$R *.dfm}

uses
  dm_Act_Explorer;


procedure Tdlg_Link_add_from_Profile_Test;    //   45.799, 39.061
var
  Rec: TDlgLinkAddRec;
begin
{
  rec.Vector:=MakeBLVector(45.034, 39.153, 45.713, 39.302);
  rec.Height1:=30;
  rec.Height2:=30;
  Rec.RepeaterPos:=MakeBLPoint(45.799, 39.061);
  rec.Freq:=7;
  rec.ProfileType:='PROFILE_BETWEEN_BS';


  Tdlg_Link_add_from_Profile.ExecDlg (rec);}

end;


//--------------------------------------------------------------------
class function Tdlg_Link_add_from_Profile.ExecDlg (aRec: TDlgLinkAddRec): integer;
//--------------------------------------------------------------------
var
  blPos: TBLPoint;

begin
{
  if dmUser_Security.ProjectIsReadOnly then
  begin
    ShowMessage('���������� �������� ������. ������ ������ ��� ������.');
    Result:=0;
    Exit;
  end;
 }

  with Tdlg_Link_add_from_Profile.Create(Application) do
  begin
    FVector:=aRec.BLVector;

    Fframe_Link_add1.Params.IsPropertyExists:=
        aRec.ProfileType in [DEF_PROFILE_BETWEEN_BS, DEF_PROFILE_BETWEEN_BS_AND_POINT];

    Fframe_Link_add2.Params.IsPropertyExists:=
        aRec.ProfileType in [DEF_PROFILE_BETWEEN_BS ];

   {
    case aRec.ProfileType of    //
      DEF_PROFILE_BETWEEN_BS:
      begin
        Fframe_Link_add1.Params.IsAllowAppend1:=true;
        Fframe_Link_add2.Params.IsAllowAppend1:=true;
      end;

      DEF_PROFILE_BETWEEN_BS_AND_POINT:
      begin
        Fframe_Link_add1.Params.IsAllowAppend1:=true;
        Fframe_Link_add2.Params.IsAllowAppend1:=false;
      end;
      else
        begin
          Fframe_Link_add1.Params.IsAllowAppend1:=false;
          Fframe_Link_add2.Params.IsAllowAppend1:=false;
        end;

     // 2: ;
     // 3: ;
    end;    // case
    }


{

    if (aRec.ProfileType = DEF_PROFILE_BETWEEN_BS) then
    begin
      Fframe_Link_add1.Params.IsFindBS:=true;
      Fframe_Link_add2.Params.IsFindBS:=true;
    end else

    if (aRec.ProfileType = DEF_PROFILE_BETWEEN_BS_AND_POINT) then
    begin
      Fframe_Link_add1.Params.IsFindBS:=true;
      Fframe_Link_add2.Params.IsFindBS:=false;
    end else

    begin
      Fframe_Link_add1.Params.IsFindBS:=false;
      Fframe_Link_add2.Params.IsFindBS:=false;
    end;}


    Fframe_Link_add1.Params.Property_BLPos:=aRec.BLVector.Point1;
    Fframe_Link_add2.Params.Property_BLPos:=aRec.BLVector.Point2;

    Fframe_Link_add1.Params.Height:=aRec.Height1;
    Fframe_Link_add2.Params.Height:=aRec.Height2;

    Fframe_Link_add1.Params.Freq_GHz:=aRec.Freq_GHz;
    Fframe_Link_add2.Params.Freq_GHz:=aRec.Freq_GHz;

    Fframe_Link_add1.Init(1);
    Fframe_Link_add2.Init(2);

   //---------------------------------------------------------

    ed_Name_.Text:=MakeLinkName();

    MakeLinkEndNames;

    CalcReserve;

    if (ShowModal=mrOk) then
    begin
      Result:=FID;
    end else
      Result:=0;

    Free;
  end;
end;


//--------------------------------------------------------------------
procedure Tdlg_Link_add_from_Profile.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
  SetActionName (STR_DLG_ADD_LINK);

  cxVerticalGrid1.Align:=alClient;

  cx_InitVerticalGrid(cxVerticalGrid1);


  pn_Right.Align:=alClient;
  lb_Name.Caption:=STR_NAME;

  StatusBar1.Color:=clYellow;


  row_Template_site.Properties.Caption:='������';

  
  CreateChildForm(Tframe_Link_add_from_Profile,  Fframe_Link_add1, pn_Left);
  CreateChildForm(Tframe_Link_add_from_Profile,  Fframe_Link_add2, pn_Right);

 // Fframe_Link_add1:=Tframe_Link_add_from_Profile.CreateChildForm( pn_Left);
//  Fframe_Link_add2:=Tframe_Link_add_from_Profile.CreateChildForm( pn_Right);

  Fframe_Link_add1.OnEquipmentChanged:=DoOnEquipmentChanged;
  Fframe_Link_add2.OnEquipmentChanged:=DoOnEquipmentChanged;

//  DoOnLinkEndChangedEvent


  Fframe_Link_add1.InitParamsFromRegistry('1');
  Fframe_Link_add2.InitParamsFromRegistry('2');


  AddComponentProp(PageControl1, 'ActivePage');

///  AddComponentProp(row_Repeater, DEF_PropertiesValue);
  AddComponentProp(row_Antenna1_H, DEF_PropertiesValue);
  AddComponentProp(row_Antenna2_H, DEF_PropertiesValue);
  RestoreFrom();


//  GsPages1.ActivePageIndex:=1;

//  with gl_Reg.RegIni do
 //z begin
  FClutterMOdelID:=reg_ReadInteger(FRegPath, FLD_CLUTTER_MODEL_ID, 0);

  if FClutterMOdelID=0 then
     FClutterMOdelID:=dmClutterModel.GetDefaultID();
//  end;

  row_ClutterModel.Properties.Value:=dmClutterModel.GetNameByID(FClutterModelID);//  gl_DB.GetNameByID(TBL_CLUTTER_MODEL, FClutterModelID);

  FormResize(nil);

  Height:= 580;

end;

//--------------------------------------------------------------------
procedure Tdlg_Link_add_from_Profile.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
//  with gl_Reg.RegIni do
 // begin
  reg_WriteInteger(FRegPath, FLD_CLUTTER_MODEL_ID, FClutterModelID);

/////    WriteInteger (Name, row_NamesGenerateMethod.Name,
     /////             row_NamesGenerateMethod.Items.IndexOf(row_NamesGenerateMethod.Text));
//  end;

//  Fframe_Link_add1.Free;
//  Fframe_Link_add2.Free;
  inherited;
end;


procedure Tdlg_Link_add_from_Profile.act_OkExecute(Sender: TObject);
begin
  inherited;

  Append;
end;


//--------------------------------------------------------------------
procedure Tdlg_Link_add_from_Profile.Append;
//--------------------------------------------------------------------

    //--------------------------------------------------------------
    function DoAddlink(aName: string; aLinkEnd1_ID, aLinkEnd2_ID: integer;
        aIsMainLInk: Boolean=False): Integer;
    //--------------------------------------------------------------
    var
      rec: TdmLinkAddRec;
      sFolderGUID: string;

    begin
        FillChar (rec, SizeOf(rec), 0);

        rec.NewName       :=aName;
     //   rec.FolderID      :=Fframe_Link_add2.FolderID;
        rec.ClutterModelID:=FClutterMOdelID;
        rec.LinkEnd1_ID    :=aLinkEnd1_ID;
        rec.LinkEnd2_ID    :=aLinkEnd2_ID;


        rec.Template_Site_ID:=FTemplate_Site_ID;


        result:=dmLink.Add (rec);

        //-------------------
        if result>0 then
        begin
          sFolderGUID:=g_Obj.ItemByName[OBJ_LINK].RootFolderGUID;

{          if rec.FolderID=0 then
            sFolderGUID:=g_Obj.ItemByName[OBJ_LINK].RootFolderGUID
          else
            sFolderGUID:=dmFolder.GetGUIDByID(rec.FolderID);
}

          g_ShellEvents.Shell_UpdateNodeChildren_ByGUID (sFolderGUID);
        end;
        //-------------------

    end;

var
  iProperty1_ID, iProperty2_ID, iLinkEnd1, iLinkEnd2: integer;
  sError, sName: string;
  sName1: string;
  sName2: string;
begin
  sError:=Fframe_Link_add1.VerifyInputData();
  if sError <> '' then
  begin
    ErrDlg(sError);
    exit;
  end;

  sError:=Fframe_Link_add2.VerifyInputData();
  if sError <> '' then
  begin
    ErrDlg(sError);
    exit;
  end;

  sName:= ed_Name_.Text;

  if dmLink.FindByName(sName)<>0 then
  begin
    ShowMessage('�������� '+sName+' ��� ����������, ������� ������ ���');
    exit;
  end;

  iProperty1_ID:=Fframe_Link_add1.AddProperty1();
  iProperty2_ID:=Fframe_Link_add2.AddProperty1();

  if (iProperty1_ID=0) or (iProperty2_ID=0) then
  begin
    ErrorDlg('����������� ������ ��������.');
    Exit;
  end;


  Assert((iProperty1_ID>0) and (iProperty1_ID>0), '(iProperty1_ID>0) and (iProperty1_ID>0)');

  iLinkEnd1:=Fframe_Link_add1.AddLinkEndToDB (iProperty1_ID);
  iLinkEnd2:=Fframe_Link_add2.AddLinkEndToDB (iProperty2_ID);

  Assert((iLinkEnd1>0));
  Assert((iLinkEnd2>0));


 ///// dmLinkEnd.Update (iLinkEnd1, FLD_CHANNEL_TYPE, 'low');
 /////// dmLinkEnd.Update (iLinkEnd2, FLD_CHANNEL_TYPE, 'high');


  FID:=DoAddLink(ed_Name_.Text, iLinkEnd1, iLinkEnd2, True);

  if FID>0 then
  begin
    dmMapEngine_store.Feature_Add(OBJ_LINK, FID);

   // {$ENDIF}
  //
  //  dmMapEngine.CreateObject (otLink, FID);

    //  if Assigned(IMapAct) then
 //   dmAct_Map.MapRefresh;
  end;

end;

//------------------------------------------------------------------
procedure Tdlg_Link_add_from_Profile.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//------------------------------------------------------------------
begin
  act_Ok.Enabled:= Fframe_Link_add1.IsAllowAppend1 and
                   Fframe_Link_add2.IsAllowAppend1 and
                   (row_ClutterModel.Properties.Value<>'');
end;


//---------------------------------------------------------------
procedure Tdlg_Link_add_from_Profile.FormResize(Sender: TObject);
//---------------------------------------------------------------
begin
  pn_Left.Width:=TabSheet_LinkEnds.Width div 2;

  StatusBar1.Panels[0].Width:= AsInteger(Width/2);
  StatusBar2.Panels[0].Width:= AsInteger(Width/2);
end;


//---------------------------------------------------------------
procedure Tdlg_Link_add_from_Profile.act_MakeLinkNameExecute(Sender: TObject);
//---------------------------------------------------------------
begin
  ed_Name_.Text:=MakeLinkName;
  MakeLinkEndNames;
end;


//---------------------------------------------------------------
procedure Tdlg_Link_add_from_Profile.MakeLinkEndNames;
//---------------------------------------------------------------
var
 // iProperty1_ID: Integer;
  //iProperty2_ID: Integer;
  sProp1, sProp2, sLinkEnd1, sLinkEnd2: string;
begin
  sProp1:=Fframe_Link_add1.GetPropertyName();
  sProp2:=Fframe_Link_add2.GetPropertyName();

//  iProperty1_ID:=Fframe_Link_add1.AddProperty1();
 // iProperty2_ID:=Fframe_Link_add2.AddProperty1();


  if (sProp1='') and (sProp2='') then
    exit;

  sLinkEnd1:=sProp1+' -> '+sProp2;
  sLinkEnd2:=sProp2+' -> '+sProp1;

  {
  if dmLinkEnd.FindByNameAndFolder(sLinkEnd1, 0)>0 then
  begin
    MsgDlg('��� '+sLinkEnd1+' ��� ����������');
 //   if not Fframe_Link_add1.IsLinkEndExist then
      Fframe_Link_add1.row_LinkEnd_Name.Properties.Value:=
        dmLinkAddFromProfile.GetName1(OBJ_LINKEND, 1);

//      Fframe_Link_add1.row_LinkEnd_Name.Text:=dmLinkAddFromProfile.GetName(OBJ_LINKEND, 1);
  end;

  if dmLinkEnd.FindByNameAndFolder(sLinkEnd2, 0)>0 then
  begin
    MsgDlg('��� '+sLinkEnd2+' ��� ����������');
//    if not Fframe_Link_add2.IsLinkEndExist then
    //  Fframe_Link_add2.row_LinkEnd_Name.Text:=dmLinkAddFromProfile.GetName(OBJ_LINKEND, 2);
      Fframe_Link_add2.row_LinkEnd_Name.Properties.Value:=
            dmLinkAddFromProfile.GetName1(OBJ_LINKEND, 2);
  end;
   }
//  if not Fframe_Link_add1.IsLinkEndExist then
    Fframe_Link_add1.row_LinkEnd_Name.Properties.Value:=sLinkEnd1;

//  if not Fframe_Link_add2.IsLinkEndExist then
    Fframe_Link_add2.row_LinkEnd_Name.Properties.Value:=sLinkEnd2;

end;


//---------------------------------------------------------------
function Tdlg_Link_add_from_Profile.MakeLinkName: string;
//---------------------------------------------------------------
var
  sProp1, sProp2: string;
  sName: string;
//  sLink: string;
//  i: integer;
begin
  sProp1:=Fframe_Link_add1.GetPropertyName();
  sProp2:=Fframe_Link_add2.GetPropertyName();

/////  sName:=sProp1+' <-> '+sProp2;

 // Result := dmLink.MakeLinkName1(sProp1, sProp2);


  Result := dmLink.MakeLinkName1(sProp1, sProp2);

  Assert(Result<>'');

//  Result := dmOnega_DB_data.Object_GetNewName_select (OBJ_LINK, dmMain.ProjectID, sName);


end;


//---------------------------------------------------------------
procedure Tdlg_Link_add_from_Profile.DoOnEquipmentChanged(Sender: TObject);
//---------------------------------------------------------------
begin
  if Sender=Fframe_Link_add1 then UpdateEquipment(2);
  if Sender=Fframe_Link_add2 then UpdateEquipment(1);

  CalcReserve();
end;

//--------------------------------------------------------------------
procedure Tdlg_Link_add_from_Profile.UpdateEquipment(aDest_LinkEnd: integer);
//--------------------------------------------------------------------
{var
      Fframe_Link_add1: Tframe_Link_add_from_Profile;
    Fframe_Link_add2: Tframe_Link_add_from_Profile;
}
begin
  case aDest_LinkEnd of
    1: with Fframe_Link_add1 do
       begin
       //  Equipment_Type:= DEF_EQUIPMENT_TYPED;
         LinkEndTypeID      := Fframe_Link_add2.LinkEndTypeID;

         LinkEndType_Mode_ID:= Fframe_Link_add2.LinkEndType_Mode_ID;
         Mode               := Fframe_Link_add2.Mode;

         row_mode.Properties.Value:= Mode;
         row_LinkEndType.Properties.Value:= dmLinkEndType.GetNameByID(LinkEndTypeID);
       end;

    2: with Fframe_Link_add2 do
       begin
      //   Fframe_Link_add2.Equipment_Type:= DEF_EQUIPMENT_TYPED;
         Fframe_Link_add2.LinkEndTypeID := Fframe_Link_add1.LinkEndTypeID;
         Fframe_Link_add2.Mode          := Fframe_Link_add1.Mode;
         Fframe_Link_add2.LinkEndType_Mode_ID:= Fframe_Link_add1.LinkEndType_Mode_ID;

         row_mode.Properties.Value:= Mode;
         row_LinkEndType.Properties.Value:= dmLinkEndType.GetNameByID(LinkEndTypeID);
       end;
  end;
end;

//--------------------------------------------------------------------
function Tdlg_Link_add_from_Profile.CalcReserve: double;
//--------------------------------------------------------------------
var
  b: Boolean;
  L, dFreq_MHZ, dGain1, dGain2, dSense, dSpeed, dPower_dBm, Pout: double;
  sBand: string;

  rec: TCalcReserveRec;

begin

//  Result:=dmLink.CalcReserve(rec, iLinkEnd1,iLinkEnd2, oVector, ePout, eFade_margin_dB);


  dGain1:=gl_DB.GetFieldValueByID(TBL_ANTENNATYPE, FLD_GAIN, Fframe_Link_add1.AntennaTypeID, '');
  dGain2:=gl_DB.GetFieldValueByID(TBL_ANTENNATYPE, FLD_GAIN, Fframe_Link_add2.AntennaTypeID, '');

  //-------------------------------------------------------------------
  if Fframe_Link_add1.Equipment_Type = 0 then
  begin
   // raise Exception.Create('');
//    ShowMessage('function Tdlg_Link_add_from_Profile.CalcReserve: double;');

//    dFreq_MHZ:= gl_DB.GetFieldValueByID(TBL_LINKENDTYPE, FLD_RANGE,     Fframe_Link_add1.LinkEndTypeID, '');// * 1000;
    sBand:= gl_DB.GetFieldValueByID(TBL_LINKENDTYPE, FLD_BAND,  Fframe_Link_add1.LinkEndTypeID, '');// * 1000;

    dFreq_MHZ :=dmLibrary.GetBandAveFreq_MHZ (sBAnd);
    dPower_dBm:=gl_DB.GetFieldValueByID(TBL_LINKENDTYPE, FLD_POWER_MAX, Fframe_Link_add1.LinkEndTypeID, '');
  end else
  begin

    dFreq_MHZ:= AsFloat(Fframe_Link_add1.row_Freq_GHz.Properties.Value) * 1000;
    dPower_dBm:=AsFloat(Fframe_Link_add1.row_Power_dBm.Properties.Value);

{    dFreq_MHZ:= AsFloat(Fframe_Link_add1.row_Freq.Text);
    dPower_dBm:=AsFloat(Fframe_Link_add1.row_Power.Text);
}  end;

  //-------------------------------------------------------------------
  if Fframe_Link_add2.Equipment_Type = 0 then
    dSense:= gl_DB.GetDoubleFieldValue(TBL_LINKENDTYPE_MODE, FLD_THRESHOLD_BER_6,
                    [db_Par(FLD_LINKENDTYPE_ID, Fframe_Link_add2.LinkEndTypeID)])
  else
    dSense:=Fframe_Link_add2.row_THRESHOLD_BER_6.Properties.Value;

  rec.Freq_GHz := dFreq_MHZ /1000;
  rec.Power_dBm := dPower_dBm;
  rec.Gain1 :=  dGain1;
  rec.Gain1 :=  dGain2;
  rec.THRESHOLD :=  dSense;
  rec.Distance_km  :=  geo_Distance_km(FVector);
//  rec.Vector  := FVector;


//  Result:= radio_CalcReserve(rec,
//                  dFreq_MHZ, dPower_dBm, dGain1, dGain2,
//                  dSense, geo_Distance_km(FVector),  POut, FVector);

  b:= radio_CalcReserve1(rec);



//  if Result<0 then
  if rec.Result.zapas <0 then
    StatusBar2.Color:=clRed
  else
    StatusBar2.Color:=clLime;

  StatusBar1.Panels[0].Text:=
    '������ ��� ��������� ���������:';
  StatusBar1.Panels[1].Text:=
    '����� ��������� (km): '+AsString(rec.Distance_km); ///1000

  StatusBar2.Panels[0].Text:=
    '�������� �� ����� �������� (dBm): '+AsString(TruncFloat( rec.Result.RxLevel_dBm));
  StatusBar2.Panels[1].Text:=
    '����� �� ��������� (dB): '+AsString(TruncFloat(rec.Result.Zapas));

end;    //


//-------------------------------------------------------------------
procedure Tdlg_Link_add_from_Profile.row_ClutterModel1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
//-------------------------------------------------------------------
begin
  if cxVerticalGrid1.FocusedRow=row_ClutterModel then
    dmAct_Explorer.Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit,
                                  otClutterModel, FClutterModelID);


 { if cxVerticalGrid1.FocusedRow=row_Repeater_property then
    dmAct_Explorer.Dlg_SelectObject_cxInspectorRow (row_Repeater_property,
                                  otProperty, FRepeater.PropertyID);
}
end;



procedure Tdlg_Link_add_from_Profile.row_Template_site_linkendEditPropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
begin

  if AButtonIndex=0 then
    if cxVerticalGrid1.FocusedRow=row_Template_site then
      dmAct_Explorer.Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otTemplate_Site, FTemplate_Site_ID);

//      Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otTemplate_Site, FTemplate_Site_ID);

//    Dlg_Apply_template;

  if AButtonIndex=1 then
  begin
    FTemplate_Site_ID:=0;
    row_Template_site.Properties.Value:='';
  end;

{
  if AButtonIndex=0 then
    Dlg_Apply_template;

  if AButtonIndex=1 then
    FTemplate_Site_ID:=0;
}

//  ShowMessage('row_Template_site_linkendEditPropertiesButtonClick');

end;




end.