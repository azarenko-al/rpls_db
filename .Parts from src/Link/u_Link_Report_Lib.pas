unit u_Link_Report_Lib;

interface
uses


  u_ini_Link_report_params,

dm_Main,

  u_vars,

  u_const,

  u_func,
  u_classes;


  function LinkReport_Execute_Link(aLinkID: integer): boolean;

(*
  function LinkReport_Execute_LinkNet(aReport: TIni_Link_Report_Param;
      aLinkLineIDList, aLinkIDList: TIDList): boolean;

*)


  function LinkReport_Execute_LinkLine(aLinkLineID: integer): Boolean;

  function LinkReport_Execute_LinkList(aIDList: TIDList): boolean;


// ==================================================================
implementation

uses SysUtils;
// ==================================================================

const
  TEMP_INI_FILE = 'link_report_.ini';


// ---------------------------------------------------------------
function LinkReport_Execute_Link(aLinkID: integer): boolean;
// ---------------------------------------------------------------
var
  sDocFileName: string;
  oIDList: TIDList;
begin
 // Assert(aObj.Scale>0);

  oIDList:=TIDList.Create;
  oIDList.AddID(aLinkID);

  Result := LinkReport_Execute_LinkList (oIDList);

  oIDList.Free;

{
  sDocFileName:=ini_ReadString (params.IniFileName, 'result', 'doc', sDocFileName);

  ShellExec (sDocFileName,'');
}

//    IniFileName
//    RunApp ('open', sDocFileName);

//    s:=WinExecutableName(sDocFileName);


  //  WinShellExecute('', '"'+sDocFileName+'"');


   // ShellExecute(0,'explorer', nil, pChar(string(sDocFileName)), nil, SW_SHOWDEFAULT);//SW_NORMAL);
//    ShellExecute(0,'explorer', pChar(string(sDocFileName)), nil, nil, SW_SHOWDEFAULT);//SW_NORMAL);

//    ShellExec (sDocFileName,'');

end;

// ---------------------------------------------------------------
function LinkReport_Execute_LinkList(aIDList: TIDList): boolean;
// ---------------------------------------------------------------

//  EXE_LINK_REPORT = 'dll_link_report.exe';

var
  sDocFileName: string;
//  oInifile: TIniFile;
  sIniFile: string;

  oReportParam: TIni_Link_Report_Param;

//  oIni: TReportIniObj;
 // ini_rec: TReportIniRec;
begin
//  Assert(aReport.Scale>0);
  sIniFile := g_ApplicationDataDir + TEMP_INI_FILE;



  oReportParam:=TIni_Link_Report_Param.Create;

  oReportParam.ProjectID := dmMain.ProjectID;
  oReportParam.Category  := 'Link';


  oReportParam.IDList.Assign(aIDList);

  oReportParam.SaveToINI(sIniFile);


 // sIniFile:=TEMP_INI_FILE;


 {
  DeleteFile(sIniFile);



  aReport.ProjectID        := dmMain.ProjectID;
  aReport.Category        := 'Link';
 // aReport.Action          := 'LinkList_report';
  aReport.SaveToINI (sIniFile);


  aIDList.SaveIDsToIniFile (sIniFile, 'items');

 // raise Exception.Create('');

 }
 
 FreeAndNil(oReportParam);

 // ShellExec (GetApplicationDir() + EXE_LINK_REPORT,  sIniFile);



//  RunApp (GetApplicationDir() + EXE_LINK_REPORT,  sIniFile );

  ShellExec (GetApplicationDir() + EXE_LINK_REPORT,  DoubleQuotedStr(sIniFile) );


{
  sDocFileName:=ini_ReadString (sIniFile, 'result', 'doc', sDocFileName);
  ShellExec (sDocFileName,'');
}

//  ShellExec(aFileName,'');
end;


// ---------------------------------------------------------------
function LinkReport_Execute_LinkLine(aLinkLineID: integer): Boolean;
// ---------------------------------------------------------------
var
//  oInifile: TIniFile;
  sDocFileName,sIniFile: string;
  oIDList: TIDList;
  oReportParam: TIni_Link_Report_Param;
 // oIni: TReportIniObj;
 // ini_rec: TReportIniRec;
begin
  sIniFile := g_ApplicationDataDir + TEMP_INI_FILE;


  oReportParam:=TIni_Link_Report_Param.Create;

  oReportParam.ProjectID := dmMain.ProjectID;
  oReportParam.Category  := 'LinkLine';

  oReportParam.IDList.AddID(aLinkLineID);

  oReportParam.SaveToINI (sIniFile);

(*

 // Assert(aReport.Scale>0);

//  sIniFile:=TEMP_INI_FILE;

  DeleteFile(sIniFile);


  aReport.ProjectID        := dmMain.ProjectID;
  aReport.Category      := 'LinkLine';
//  aReport.Action        := 'LinkLine_report';
  aReport.SaveToINI (sIniFile);
*)


{

  oInifile:=TIniFile.Create (sIniFile);
  oInifile.WriteString  ('main', 'FileDir', aReport.FileDir);
  oInifile.WriteInteger  ('main', 'ReportID', aReport.ReportID);

  oInifile.WriteString  ('main', 'ReportFileName', aReport.ReportFileName);
  oInifile.WriteInteger  ('main', 'ImageHeight', aReport.ImageHeight);
  oInifile.WriteInteger  ('main', 'Scale',       aReport.Scale);



  oInifile.WriteInteger ('main', 'MapDesktopID', aReport.MapDesktopID);

  oInifile.WriteString  ('main', 'Action',      'LinkLine_report');
  oInifile.WriteInteger ('main', 'ProjectID',  dmMain.ProjectID);

  oInifile.WriteInteger  ('main', 'ImageWidth',  aReport.ImageWidth);
  oInifile.WriteInteger ('main', 'LinkLineID',  aLinkLineID);}
 // oInifile.WriteInteger ('main', 'Scale',        aReport.Scale);

 // oInifile.Free;

(*
  oIDList:=TIDList.Create;
  oIDList.AddID(aLinkLineID);

  oIDList.SaveIDsToIniFile (sIniFile, 'items');
  oIDList.Free;
*)


  FreeAndNil(oReportParam);


  ShellExec(GetApplicationDir() + EXE_LINK_REPORT,  DoubleQuotedStr(sIniFile) );

{  RunApp (GetApplicationDir() + EXE_LINK_REPORT,  sIniFile );

  sDocFileName:=ini_ReadString (sIniFile, 'result', 'doc', sDocFileName);
  ShellExec (sDocFileName,'');
}

end;


end.



(*

      {


// ---------------------------------------------------------------
function LinkReport_Execute_LinkNet(aReport: TIni_Link_Report_Param;
    aLinkLineIDList, aLinkIDList: TIDList): boolean;
// ---------------------------------------------------------------
var
//  oInifile: TIniFile;
  sIniFile: string;
 // ini_rec: TReportIniRec;

//  oIni: TReportIniObj;
begin
 // sIniFile:=TEMP_INI_FILE;
  sIniFile := g_ApplicationDataDir + TEMP_INI_FILE;


  aReport.ProjectID        := dmMain.ProjectID;
  aReport.Action        := 'LinkNet_report';
  aReport.SaveToINI (sIniFile);


{
  oInifile:=TIniFile.Create (sIniFile);
  oInifile.WriteString  ('main', 'FileDir',         aReport.FileDir);
  oInifile.WriteInteger ('main', 'ReportID',        aReport.ReportID);
  oInifile.WriteString  ('main', 'ReportFileName',  aReport.ReportFileName);
  oInifile.WriteString  ('main', 'Action',      'LinkNet_report');
  oInifile.WriteInteger ('main', 'ProjectID',       dmMain.ProjectID);

  oInifile.WriteInteger ('main', 'MapDesktopID', aReport.MapDesktopID);

  oInifile.WriteInteger  ('main', 'ImageWidth',  aReport.ImageWidth);
  oInifile.WriteInteger  ('main', 'ImageHeight', aReport.ImageHeight);
  oInifile.WriteInteger  ('main', 'Scale',       aReport.Scale);


 // oInifile.WriteInteger('main', 'Scale',           aReport.Scale);


  oInifile.Free;
}
  aLinkIDList.SaveIDsToIniFile (sIniFile, 'links');
  aLinkLineIDList.SaveIDsToIniFile (sIniFile, 'LinkLines');

//  RunApp (GetApplicationDir() + EXE_LINK_REPORT,  sIniFile );

  ShellExec (GetApplicationDir() + EXE_LINK_REPORT,  DoubleQuotedStr(sIniFile) );
end;



