unit u_link_const_str;

interface


const

  STR_NOT_TYPED_1_FREQ_GHz         = '1 ������� (��������) [GHz]';
  STR_NOT_TYPED_2_POWER_dBm        = '2 �������� �������� ��� [dBm]';
  STR_NOT_TYPED_3_LOSS_dB          = '3 ������ ��� (�� ������) [dB]';
  STR_NOT_TYPED_4_RAZNOS_MHz       = '4 Min ������ ������� [MHz]';
  STR_NOT_TYPED_5_SIGNATURE_WIDTH_MHz = '5 ������ ��������� [MHz]';
  STR_NOT_TYPED_6_SIGNATURE_HEIGHT_dB = '6 ������ ��������� [dB]';
  STR_NOT_TYPED_7_SENS_3_dBm       = '7 ���������������� ��� BER 10^-3 [dBm]';
  STR_NOT_TYPED_8_SENS_6_dBm      = '8 ���������������� ��� BER 10^-6 [dBm]';
  STR_NOT_TYPED_9_KNG             = '9 ���  ������������ [%]';

  STR_NOT_TYPED_KRATNOSTBYSPACE  = '��������� ���������� �� ������������';
  STR_NOT_TYPED_KRATNOSTBYFREQ   = '��������� ���������� �� �������';
 // STR_NOT_TYPED_EQUALISER_PROFIT_dB = '7 �������, �������� ������������ [dB]';


implementation

end.
