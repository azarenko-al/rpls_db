unit fr_Link_profile;

interface
{$I ver.Inc}

uses
//  Gexperts,

  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, cxSplitter, StdCtrls, ExtCtrls,
  Dialogs,  rxPlacemnt, ActnList, cxControls, TB2Item, TB2Dock, TB2Toolbar, ToolWin, ComCtrls,
  IniFiles, cxPropertiesStore, ActnMan, ActnCtrls, XPStyleActnCtrls, ActnMenus, dxBar, Variants,
  Grids, DBGrids, dxmdaset, Mask, cxContainer, cxEdit, cxCheckBox,
  cxPC, WSDLBind, DBCtrls,   DB, ADODB,
  cxDBEdit, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,

  i_SNMP,

  u_link_const,

  u_assert,

//  u_Profiler,

//  dm_Localization,

  fr_Link_profile_ant,
  fr_Link_profile_summary,

  dm_Onega_DB_data,

  fr_Profile_Rel,

  f_Custom,

  I_rel_Matrix1,

  u_link_Classes1,

  dm_Link_view_data,

 // I_Act_Link, //dm_Act_Link;

  u_Storage,

  u_Link_run,

  u_types,

  u_ini_Link_optimize_params,

  u_reflection_points,

  u_vars,

  dm_Rel_Engine,
  u_Rel_Profile,

  dm_Main,

  u_db,
  u_dlg,
  u_files,
  u_func,
  u_Geo,
  u_Log,
  u_reg,

  //u_types,

  u_const_db,
  u_const,

  dm_Link,

  dxSkinsDefaultPainters, cxClasses,

  MapXLib_TLB,

  cxBarEditItem, ImgList,
  dxBarExtItems;

  {
const
  WM_REFRESH_DATA111111 = WM_USER + 600;

  }

type


  Tframe_Link_profile = class(Tfrm_Custom)
    act_Calc: TAction;
    act_Fren_Bot: TAction;
    act_Fren_Left: TAction;
    act_Fren_Right: TAction;
    act_Fren_Top: TAction;
    act_Is_CalcAfterChange: TAction;
    act_IsSaveResultsToDB: TAction;
    act_Optimize_Freq: TAction;
    act_profile_direction_Change_new: TAction;
    act_Refl_points: TAction;
    act_Reload_Profile: TAction;
    act_Report: TAction;
    act_Save: TAction;
    act_Refraction_2: TAction;
    ActionList1: TActionList;
    cb_Fren_Bot: TCheckBox;
    cb_Fren_Left: TCheckBox;
    cb_Fren_Right: TCheckBox;
    cb_Fren_Top: TCheckBox;
    cb_Reversed: TCheckBox;
    cxSplitter1: TcxSplitter;
    cxSplitter2: TcxSplitter;
    Image1: TImage;
    Image4: TImage;
    Image5: TImage;
    Image6: TImage;
    Label2: TLabel;
    Panel2: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    pn_Main: TPanel;
    pn_Profile: TPanel;
    pn_Summary: TPanel;
    TBControlItem1: TTBControlItem;
    TBControlItem10: TTBControlItem;
    TBControlItem11: TTBControlItem;
    TBControlItem12: TTBControlItem;
    TBControlItem13: TTBControlItem;
    TBControlItem2: TTBControlItem;
    TBControlItem3: TTBControlItem;
    TBControlItem4: TTBControlItem;
    TBControlItem5: TTBControlItem;
    TBControlItem6: TTBControlItem;
    TBControlItem7: TTBControlItem;
    TBControlItem8: TTBControlItem;
    TBControlItem9: TTBControlItem;
    TBDock1: TTBDock;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    TBItem3: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    TBToolbar1: TTBToolbar;
    TBToolbar3: TTBToolbar;
    TBToolbar4: TTBToolbar;
    pn_cxPageControl1: TPanel;
    pn_ant_left: TPanel;
    pn_Split: TPanel;
    pn_Ant_right: TPanel;
    DBGrid1: TDBGrid;
    TBToolbar5: TTBToolbar;
    TBItem7: TTBItem;
    TBItem8: TTBItem;
    TBItem9: TTBItem;
    TBControlItem14: TTBControlItem;
    cb_Reflection_points: TCheckBox;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarManager1Bar2: TdxBar;
    dxBarButton4: TdxBarButton;
    dxBarManager1Bar3: TdxBar;
    cxBarEditItem1_Fren_Top: TcxBarEditItem;
    cxBarEditItem2: TcxBarEditItem;
    dxBarButton6: TdxBarButton;
    dxBarButton5: TdxBarButton;
    dxBarButton7: TdxBarButton;
    dxBarButton8: TdxBarButton;
    cxBarEditItem_Fren_Left: TcxBarEditItem;
    cxBarEditItem_Fren_Right: TcxBarEditItem;
    cxBarEditItem_Fren_Bot: TcxBarEditItem;
    dxBarManager1Bar4: TdxBar;
    cxBarEditItem_refraction_2_checked: TcxBarEditItem;
    TBToolbar2: TTBToolbar;
    TBControlItem15: TTBControlItem;
    cb_Refraction_2: TCheckBox;
    procedure FormDestroy(Sender: TObject);
    procedure Action2Execute(Sender: TObject);
    procedure FormCreate(Sender: TObject);

    procedure _Actions(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction;  var Handled: Boolean);
    procedure cb_Reflection_pointsClick(Sender: TObject);
    procedure act_Fren_TopExecute(Sender: TObject);
    procedure act_Refraction_2Execute(Sender: TObject);
    procedure cb_Refraction_2Click(Sender: TObject);
    procedure dxBarSpinEdit1_RefractionCurChange(Sender: TObject);
//    procedure DBCheckBox1Click(Sender: TObject);
    procedure pn_cxPageControl1Resize(Sender: TObject);
//    procedure cxDBCheckBox2PropertiesChange(Sender: TObject);
  private
    FOnUpdateAfterCalc: TNotifyEvent;

    FLinkParamRec: TdmLinkInfoRec;

    FRepeater: record
                 LinkRec1,
                 LinkRec2:  TdmLinkInfoRec;
               end;


    FDBLink_ref : TDBLink_;

    FReadOnly : Boolean;


    FRelProfile: TrelProfile;
    FRelProfile1: TrelProfile;
    FRelProfile2: TrelProfile;

//    FLinkParamRec: TdmLinkViewParamsRec;

    FID: integer;
//    FObjName: string;

    FIsBeginUpdate: boolean;

    Fframe_Profile_Rel: Tframe_profile_Rel;
    Fframe_Profile_Summary: Tframe_Link_profile_Summary;

    Fframe_Ant1: Tframe_Link_profile_ant;
    Fframe_Ant2: Tframe_Link_profile_ant;

//    Fframe_Passive1: Tframe_Link_profile_ant;
//    Fframe_Passive2: Tframe_Link_profile_ant;


    Fframe_Link_profile_summary: Tframe_Link_profile_summary;

    procedure DoOnChangedEdits (Sender: TObject);
    procedure ShowSites;

    procedure DoOnDataChanged(Sender: TObject);

//    procedure DoOnGetUsePassiveElement(var aValue : boolean);
    procedure DoOnProfileChanged(Sender: TObject);
    procedure Profile_Reload;

    function Reload_Profile: Boolean;
    function Reload_Profile_for_repeater(aIndex: Integer): Boolean;

    function Run_Link_Optimize_freq_diversity(aID: Integer): Boolean;

    procedure ShowProfileForRepeater;
//    procedure UpdateLinkData;
  protected
  //  procedure WM_REFRESH_DATA_proc(var Message: TMessage); message WM_REFRESH_DATA;

  public
    procedure View (aID: integer);

    procedure SetReadOnly(aValue: Boolean);

    property OnUpdateAfterCalc: TNotifyEvent read FOnUpdateAfterCalc write FOnUpdateAfterCalc;

    procedure Calc;

    procedure DoOnCalc(Sender: TObject);
    procedure DoOnChangeAntennaHeight(Sender: TObject);
  //  procedure SetReadOnly(aValue: Boolean);
  end;



//====================================================================
//implementation
//====================================================================
implementation

uses dm_Act_Link;

{$R *.dfm}


procedure Tframe_Link_profile.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FrelProfile);
  
  FreeAndNil(FrelProfile1);
  FreeAndNil(FrelProfile2);

  inherited;
end;

procedure Tframe_Link_profile.Action2Execute(Sender: TObject);
begin
  inherited;
end;


// ---------------------------------------------------------------
procedure Tframe_Link_profile.SetReadOnly(aValue: Boolean);
// ---------------------------------------------------------------
begin
  Fframe_Link_profile_summary.SetReadOnly(aValue);

  Fframe_Ant1.SetReadOnly(aValue);
  Fframe_Ant2.SetReadOnly(aValue);

  SetFormActionsEnabled (Self,
//  [
//      act_Calc,
//      //act_Fren_Bot, act_Fren_Left, act_Fren_Right, act_Fren_Top,
//      act_Is_CalcAfterChange,
//      act_IsSaveResultsToDB,
//      act_Optimize_Freq,
//      act_profile_direction_Change_new,
//      act_Refl_points,
//      act_Reload_Profile,
//      act_Report,
//      act_Save
     // ],
      not aValue);


//  cxGrid1DBTableView1.OptionsData.Editing := not Value;
//  FReadOnly := Value;

 //ADOStoredProc1.LockType := IIF(Value, ltReadOnly, ltBatchOptimistic);
 // SetFormActionsEnabled(Self, not Value);

//  ADOStoredProc1.LockType := IIF(dmUser_Security.ProjectIsReadOnly, ltReadOnly, ltBatchOptimistic);
//  SetFormActionsEnabled(Self, not dmUser_Security.ProjectIsReadOnly);

end;


//--------------------------------------------------------------------
procedure Tframe_Link_profile.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
var
  I: Integer;
begin
  inherited;

(*     LINK_CALC_METHODS_ARR : array[0..4] of
      record
        ID   : Integer;
        Name : string;
      end  =

  (
  (id : DEF_Calc_Method_ITU_R;      Name: '���������� (ITU, ����, 16�����)'),
  (id : DEF_Calc_Method_GOST;       Name: '���� � 53363-2009�.'),
  (id : DEF_Calc_Method_NIIR_1998;  Name: '����-98�.'),
  (id : DEF_Calc_Method_E_BAND_ITU; Name: 'E-�������� (ITU)'),
  (id : DEF_Calc_Method_UKV;        Name: '��� (16�����)')
  );

*)

(*
  for I := 0 to High(LINK_CALC_METHODS_ARR) do
    db_AddRecord(dxMemData_Calc_method,
       [
         db_Par(FLD_NAME, LINK_CALC_METHODS_ARR[i].Name),
         db_Par(FLD_ID, LINK_CALC_METHODS_ARR[i].ID)
       ]);
*)


//
//  if g_Is_Debug_mode then
//  begin
//    g_log.Msg('Tframe_Link_profile.FormResize');
//
//    Color:=clRed;
//  end;



 ////

  pn_Profile.Align:=alClient;

  pn_Main.Align:=alClient;

  pn_ant_right.Align:=alClient;
//  pn_Passive2.Align:=alClient;

  //cxPageControl1.HideTabs:=True;

  FrelProfile:=TrelProfile.Create;


  FrelProfile1:=TrelProfile.Create;
  FrelProfile2:=TrelProfile.Create;


//  DBCheckBox_Is_calc_rains.DataField:= FLD_Is_CalcWithAdditionalRain;
//  DBCheckBox_Is_calc_with_passive.DataField:=FLD_Is_UsePassiveElements;

  TdmLink_view_data.Init;

  Assert(Assigned(dmLink_view_data), 'Value not assigned');

 // DBCheckBox_Is_calc_rains.DataSource:=dmLink_view_data.ds_LInk;
 // DBCheckBox_Is_calc_with_passive.DataSource:=dmLink_view_data.ds_LInk;


  CreateChildForm(Tframe_profile_Rel, Fframe_Profile_Rel, pn_Profile);

 // Fframe_Profile_Rel.Params.IsShowSites :=True;
  Fframe_Profile_Rel.Params.IsShowReflectionPoints:=True;

  Fframe_Profile_Rel.Params.IsShowFrenelZoneTop:=cb_Fren_Top.Checked;
  Fframe_Profile_Rel.Params.IsShowFrenelZoneBottomLeft:=cb_Fren_Left.Checked;
  Fframe_Profile_Rel.Params.IsShowFrenelZoneBottomRight:=cb_Fren_Right.Checked;
  Fframe_Profile_Rel.Params.IsShowFrenelZoneBottom:=cb_Fren_Bot.Checked;
  Fframe_Profile_Rel.OnProfileChanged:=DoOnProfileChanged;


  CreateChildForm(Tframe_Link_profile_summary, Fframe_Link_profile_summary, pn_summary);
 // Fframe_Link_profile_summary.OngetUsePassiveElement := DoOnGetUsePassiveElement;

  CreateChildForm(Tframe_Link_profile_ant, Fframe_Ant1, pn_ant_left);
  CreateChildForm(Tframe_Link_profile_ant, Fframe_Ant2, pn_Ant_right);

  Fframe_Link_profile_summary.OnDataChanged:=DoOnDataChanged;
  Fframe_Link_profile_summary.OnCalcButton:=DoOnCalc;

////  Fframe_Link_profile_summary.IsCalcWithAdditionalRain:=cb_Additional_Rain_Accounting.Checked;


  Fframe_Ant1.OnChange:=DoOnChangedEdits;
  Fframe_Ant2.OnChange:=DoOnChangedEdits;

  Fframe_Ant1.OnChangeHeight:=DoOnChangeAntennaHeight;
  Fframe_Ant2.OnChangeHeight:=DoOnChangeAntennaHeight;


//  g_Storage

  AddComponentProp(pn_Summary, PROP_WIDTH);
  AddComponentProp(pn_cxPageControl1,  PROP_HEIGHT);

 // AddComponentProp(cb_SaveToDB, 'Checked');
//  AddComponentProp(cb_Use_Passive_Elements, 'Checked');
 // AddComponentProp(cb_CalcOnUpdate, 'Checked');
  AddComponentProp(cb_Reflection_points, 'Checked');
//  AddComponentProp(cb_Additional_Rain_Accounting, 'Checked');

  cxPropertiesStore.RestoreFrom;


  Fframe_Profile_Rel.Params.IsShowReflectionPoints:=cb_Reflection_points.Checked;

  pn_Summary.Realign;
  pn_Main.Realign;
  
  cxSplitter1.Realign;
  cxSplitter2.Realign;


  Fframe_Profile_Rel.RelProfile_Ref:=FrelProfile;


//  dmLocalization.Load_Form (Self);


 // Assert(ILink<>nil);
end;


//--------------------------------------------------------------------
procedure Tframe_Link_profile.View(aID: integer);
//--------------------------------------------------------------------
var
  sXML: string;
  s: string;
  eDistance_KM: double;
  eRel1: Double;
  eRel2: Double;
  iID1: Integer;
  iID2: Integer;

  oReflectionPoints: TrelReflectionPointRecArray;
begin
  if FID <> aID then
    dmLink_view_data.DBLink.Refraction_2:=0;

  FID:=aID;


 // g_Profiler.Start('Tframe_Link_profile.View');



  Assert(Assigned(dmLink_view_data), 'Value not assigned: dmLink_view_data');


  FDBLink_ref:=dmLink_view_data.DBLink;

 ///// cb_Use_Passive_Elements.Checked:= true;

  if aID=0 then
    Exit;


  dmLink_view_data.OpenData(aID);

  if not dmLink.GetInfo1(aID, FLinkParamRec) then
    Exit;


   s:=FDBLink_ref.Profile_XML;
   s:=FLinkParamRec.Profile_XML;

   iID1:= FDBLink_ref.LinkEnd1_ID;
   iID2:= FDBLink_ref.LinkEnd2_ID;



  Fframe_Ant1.View1 (FID, iID1 );// aID,
  Fframe_Ant2.View1 (FID, iID2 );// aID,

  Fframe_Ant1.NextLinkEndID:=iID2;
  Fframe_Ant2.NextLinkEndID:=iID1;



  // -------------------------
  if FDBLink_ref.LinkRepeater.ID>0 then
  begin

    ShowProfileForRepeater();


    Exit;

//    Fframe_profile_Rel.SetSiteGroupCount(2);

  end
  // -------------------------
  else begin
    Fframe_profile_Rel.SetSiteGroupCount(1);

    oReflectionPoints:=dmLink.GetReflectionPointArray(aID);

    Fframe_Profile_Rel.Params.SiteGroups[0].ReflectionPointArr:=  oReflectionPoints;

  end;

//            dmLink.GetReflectionPointArray(aID);


      dmRel_Engine.AssignClutterModel (FLinkParamRec.ClutterMOdel_ID, FrelProfile.Clutters);


      sXML:=FLinkParamRec.Profile_XML;


      if (sXML='') or (not FrelProfile.LoadFromXml(sXML, FLinkParamRec.Length_km)) then
      begin
//        ShowMessage('������ ������� �� ������ �����...');

        g_Log.Add('������ ������� �� ������ �����...');
        Reload_Profile();
      end;

      FrelProfile.ApplyDefaultClutterHeights;

      FrelProfile.Data.Distance_KM := FLinkParamRec.Length_km;

      FrelProfile.SetRefraction(FLinkParamRec.Refraction);
      FrelProfile.IsDirectionReversed := FLinkParamRec.is_profile_reversed;

      Fframe_Profile_Rel.RelProfile_Ref:=FrelProfile;
      Fframe_Profile_Rel.IsDirectionReversed := FLinkParamRec.is_profile_reversed;

      eDistance_KM:=FLinkParamRec.Length_km; //geo_Distance_km(FLinkParamRec.BLVector);

      Assert (eDistance_KM>0.01, 'eDistance_KM>0.01: ' + FloatToStr(eDistance_KM));


      if eDistance_KM>0 then
     // if FLinkParamRec.Length_km>0 then
      begin
        Fframe_Profile_Rel.Params.SiteGroups[0].Distance_KM:=eDistance_KM; //geo_Distance_km(FLinkParamRec.BLVector);

        Assert(Fframe_Profile_Rel.Params.SiteGroups[0].Distance_KM>0,'Fframe_Profile_Rel.Params.SiteGroups[0].DistanceKM');

        eRel1 :=FrelProfile.FirstItem().Rel_H;
        eRel2 :=FrelProfile.LastItem().Rel_H;


        // -------------------------
        Fframe_Profile_Rel.Params.SiteGroups[0].Site1.Rel_H:=eRel1;
        Fframe_Profile_Rel.Params.SiteGroups[0].Site2.Rel_H:=eRel2;


      end;

//  end;


  Fframe_Profile_Rel.Params.NFrenel :=FLinkParamRec.NFrenel;
  Fframe_Profile_Rel.Params.Freq_MHz:=FLinkParamRec.Tx_Freq_MHz;



  Fframe_Profile_Rel.BLVector :=FLinkParamRec.BLVector;

  cb_Reversed.Checked:=FLinkParamRec.is_profile_reversed;

  ShowSites();

  Fframe_Link_profile_summary.View (aID);


end;

//--------------------------------------------------------------------
procedure Tframe_Link_profile.ShowProfileForRepeater;
//--------------------------------------------------------------------
var
  e: Double;
  eRel1: Double;
  eRel2: Double;
  k: Integer;
  sXML1: string;

  oDBLink: TDBLink_;

  oReflectionPoints1: TrelReflectionPointRecArray;
  oReflectionPoints2: TrelReflectionPointRecArray;
  s: string;
  sXML2: string;

  rec: TrelProfilePointRec;

begin
  dmRel_Engine.AssignClutterModel (FLinkParamRec.ClutterMOdel_ID, FrelProfile1.Clutters);
  dmRel_Engine.AssignClutterModel (FLinkParamRec.ClutterMOdel_ID, FrelProfile2.Clutters);

  Fframe_profile_Rel.SetSiteGroupCount(2);

 

  oDBLink := dmLink_view_data.DBLink;


  FrelProfile1.Clear;
  FrelProfile2.Clear;




  sXML1:=oDBLink.LinkRepeater.Part1.Profile_XML1;
//  if (sXML1='') or (not FrelProfile1.LoadFromXml(sXML1, oDBLink.Length_km)) then
  if (sXML1='') or (not FrelProfile1.LoadFromXml(sXML1, oDBLink.LinkRepeater.Part1.Distance_KM)) then
    Reload_Profile_for_repeater(1);

  sXML2:=oDBLink.LinkRepeater.Part2.Profile_XML1;
  if (sXML2='') or (not FrelProfile2.LoadFromXml(sXML2, oDBLink.LinkRepeater.Part2.Distance_KM)) then
//  if (sXML1='') or (not FrelProfile2.LoadFromXml(sXML1, oDBLink.Length_km)) then
    Reload_Profile_for_repeater(2);


//  ShellExec_Notepad_temp( sXML1 + sXML2 );



  FrelProfile1.Validate;
  FrelProfile2.Validate;

 // Assert (FrelProfile1.Data.Count<>FrelProfile2.Data.Count);  //!!!!!!!!!!!!!!!!!!!!!!!!


   k:=FrelProfile1.Data.Count;
   k:=FrelProfile2.Data.Count;



  FrelProfile1.SetRefraction(FLinkParamRec.Refraction);
  FrelProfile2.SetRefraction(FLinkParamRec.Refraction);




  FrelProfile.Clear;

  FrelProfile.Assign(FrelProfile1);
  FRelProfile.Join  (FrelProfile2);


  FrelProfile.IsDirectionReversed := FLinkParamRec.is_profile_reversed;


    e:=FrelProfile1.Data.Distance_KM;
    e:=FrelProfile2.Data.Distance_KM;





  Assert(Assigned(Fframe_Profile_Rel.RelProfile_Ref));

  Fframe_Profile_Rel.RelProfile_Ref.Validate;


  assert(oDBLink.LinkRepeater.ID>0);

  assert(oDBLink.LinkRepeater.Part1.Distance_KM>0);
  assert(oDBLink.LinkRepeater.Part2.Distance_KM>0);

  assert(oDBLink.LinkRepeater.Part1.Distance_KM<50);
  assert(oDBLink.LinkRepeater.Part2.Distance_KM<50);




  eRel1 :=FrelProfile1.FirstItem().Rel_H;


  eRel2 :=FrelProfile1.LastItem().Rel_H;

  // -------------------------
  Fframe_Profile_Rel.Params.SiteGroups[0].Site1.Rel_H:=eRel1;
  Fframe_Profile_Rel.Params.SiteGroups[0].Site2.Rel_H:=eRel2;



  // -------------------------
//  if not VarIsNull(oDBLink.LinkRepeater.Property_Ground_Height) then
//    eRel1 :=oDBLink.LinkRepeater.Property_Ground_Height
//  else

    eRel1 :=FrelProfile2.FirstItem().Rel_H;

  // -------------------------

//  if not VarIsNull(oDBLink.Property2_Ground_Height) then
//    eRel2 :=oDBLink.Property2_Ground_Height
//  else


    eRel2 :=FrelProfile2.LastItem().Rel_H;

  // -------------------------
  Fframe_Profile_Rel.Params.SiteGroups[1].Site1.Rel_H:=eRel1;
  Fframe_Profile_Rel.Params.SiteGroups[1].Site2.Rel_H:=eRel2;


(*
  Fframe_Profile_Rel.Params.SiteGroups[1].Site1.Rel_H:=FrelProfile2.FirstItem().Rel_H;
  Fframe_Profile_Rel.Params.SiteGroups[1].Site2.Rel_H:=FrelProfile2.LastItem().Rel_H;

*)
  ////////////////////////////////////////////////////////

  Fframe_Profile_Rel.Params.NFrenel :=oDBLink.NFrenel;
  Fframe_Profile_Rel.Params.Freq_MHz:=oDBLink.Tx_Freq_MHz;


  Fframe_Profile_Rel.BLVector :=FLinkParamRec.BLVector;


//  oDBLink.LinkRepeater.ID

   oReflectionPoints1:=dmLink.GetReflectionPointArray(oDBLink.ID, 1);
   Fframe_Profile_Rel.Params.SiteGroups[0].ReflectionPointArr:=oReflectionPoints1;

   oReflectionPoints2:=dmLink.GetReflectionPointArray(oDBLink.ID, 2);
   Fframe_Profile_Rel.Params.SiteGroups[1].ReflectionPointArr:=oReflectionPoints2;


//  Fframe_Profile_Rel.ShowProfile;

//  Fframe_Profile_Rel.Profile_Rel_Graph.ShowGraph;

  ShowSites();

  Fframe_Link_profile_summary.View (oDBLink.ID);


//  Fframe_Profile_Rel.RelProfile_Ref.Validate;


end;



//-------------------------------------------------------------------
function Tframe_Link_profile.Reload_Profile_for_repeater(aIndex: Integer):
    Boolean;
//-------------------------------------------------------------------
var
  sXML: string;
  oDBLink: TDBLink_;

  oRelProfile: TrelProfile;
  sField: string;

  blVector :  TBLVector;

begin
  Assert(aIndex in [1,2]);

  oDBLink := dmLink_view_data.DBLink;

  dmRel_Engine.Open ();


  case aIndex of    //
    1: begin
         oRelProfile:=FrelProfile1;
         sField:=FLD_Profile_XML_1;
         blVector:=oDBLink.LinkRepeater.Part1.BLVector;
       end;

    2: begin
         oRelProfile:=FrelProfile2;
         sField:=FLD_Profile_XML_2;
         blVector:=oDBLink.LinkRepeater.Part2.BLVector;
       end;
  end;


  dmRel_Engine.AssignClutterModel (oDBLink.ClutterMOdel_ID, oRelProfile.Clutters);


  if dmRel_Engine.BuildProfile1 (oRelProfile, blVector, oDBLink.PROFILE_Step_M)
  then
    sXML:=oRelProfile.GetXML()
  else
    sXML:='';


  Result:=(sXML<>'');

  dmOnega_DB_data.UpdateRecordByID(TBL_LINK_Repeater,
     oDBLink.LinkRepeater.ID,  [db_Par(sField, sXML)]);

end;



//--------------------------------------------------------------------
procedure Tframe_Link_profile.ShowSites;
//--------------------------------------------------------------------
begin

  if FDBLink_ref.LinkRepeater.ID>0 then
  begin

 //   FDBLink_ref.Link_repeater.Antenna1_Height


//      MakeDoubleArray([6]);
      //  Fframe_Site1_Passive.GetAntennaHeightArr;

   //   SiteGroups[0].Site1.AntennaHeights:=Fframe_Site2_Passive.GetAntennaHeightArr;
  //    SiteGroups[1].Site2.AntennaHeights:=Fframe_Site2_Active.GetAntennaHeightArr;

      Fframe_Profile_Rel.Params.SiteGroups[0].Site1.AntennaHeights:=Fframe_Ant1.GetAntennaHeightArr;
      Fframe_Profile_Rel.Params.SiteGroups[1].Site2.AntennaHeights:=Fframe_Ant2.GetAntennaHeightArr;


      if not FDBLink_ref.Is_profile_reversed then
      begin
        Fframe_Profile_Rel.Params.SiteGroups[0].Distance_KM:= FDBLink_ref.LinkRepeater.Part1.Distance_KM;
        Fframe_Profile_Rel.Params.SiteGroups[1].Distance_KM:= FDBLink_ref.LinkRepeater.Part2.Distance_KM;

        Fframe_Profile_Rel.Params.SiteGroups[0].Site2.AntennaHeights:= MakeDoubleArray([FDBLink_ref.LinkRepeater.Part1.Antenna_Height]);
        Fframe_Profile_Rel.Params.SiteGroups[1].Site1.AntennaHeights:= MakeDoubleArray([FDBLink_ref.LinkRepeater.Part2.Antenna_Height]);
      end else
      begin
        Fframe_Profile_Rel.Params.SiteGroups[0].Distance_KM:= FDBLink_ref.LinkRepeater.Part2.Distance_KM;
        Fframe_Profile_Rel.Params.SiteGroups[1].Distance_KM:= FDBLink_ref.LinkRepeater.Part1.Distance_KM;

        Fframe_Profile_Rel.Params.SiteGroups[0].Site2.AntennaHeights:= MakeDoubleArray([FDBLink_ref.LinkRepeater.Part2.Antenna_Height]);
        Fframe_Profile_Rel.Params.SiteGroups[1].Site1.AntennaHeights:= MakeDoubleArray([FDBLink_ref.LinkRepeater.Part1.Antenna_Height]);
      end;


//    with Fframe_Profile_Rel.Params do
 //   begin


      Fframe_Profile_Rel.Repeater.Enabled := True;
      Fframe_Profile_Rel.Repeater.BLPoint := FDBLink_ref.LinkRepeater.Property_Pos;


  end else begin
    Fframe_Profile_Rel.Params.SiteGroups[0].Site1.AntennaHeights:=Fframe_Ant1.GetAntennaHeightArr;
    Fframe_Profile_Rel.Params.SiteGroups[0].Site2.AntennaHeights:=Fframe_Ant2.GetAntennaHeightArr;

    Fframe_Profile_Rel.Repeater.Enabled := False;

  end;

  Fframe_Profile_Rel.ShowProfile;
end;

//-------------------------------------------------------------------
procedure Tframe_Link_profile.Profile_Reload;
//-------------------------------------------------------------------
var
  oDBLink: TDBLink_;

begin

  oDBLink := dmLink_view_data.DBLink;

//  FLinkParamRec.Link_repeater

//  if FLinkParamRec.Link_repeater.ID>0 then
  if oDBLink.LinkRepeater.ID>0 then
  begin
    oDBLink.Linkrepeater.Part1.Profile_XML1:='';
    oDBLink.Linkrepeater.Part2.Profile_XML1:='';


//    Reload_Profile_for_repeater (1);
//    Reload_Profile_for_repeater (2);

    ShowProfileForRepeater ();

  //  FrelProfile.ShowXmlFile;

  end else
  begin
    Reload_Profile();

    Fframe_Profile_Rel.ShowProfile;

  end;

end;



//-------------------------------------------------------------------
procedure Tframe_Link_profile._Actions(Sender: TObject);
//-------------------------------------------------------------------
var
  sXMLFileName,sImgFileName: string;

  oDBLink: TDBLink_;
begin
  if FReadOnly then
    Exit;

  //-------------------------------------------------------------------
  if Sender=act_Reload_Profile then begin
  //-------------------------------------------------------------------
    Profile_Reload();

   end else

  //-------------------------------------------------------------------
  if Sender=act_Save then begin
  //-------------------------------------------------------------------
//    raise Exception.Create('');

    Fframe_Ant1.SaveAntennaHeightsToDB;
    Fframe_Ant2.SaveAntennaHeightsToDB;
  end else

  //-------------------------------------------------------------------
  if Sender=act_Calc then begin
  //-------------------------------------------------------------------
    Calc();
  end else

  //-------------------------------------------------------------------
  if Sender=act_Optimize_Freq then begin
  //------------------------------------------------------
    Run_Link_Optimize_freq_diversity(FID);
  end else

  //-------------------------------------------------------------------
  if Sender=act_Report then begin
  //-------------------------------------------------------------------
    dmAct_Link.Dlg_Report (FID); //, FObjName
  end else


  //------------------------------------------------------
  if Sender=act_Refraction_2 then
  //------------------------------------------------------
  begin             //  is_profile_reversed
    act_Refraction_2.Checked:=not act_Refraction_2.Checked;

    Fframe_Profile_Rel.Params_Refraction_2:=
       IIF(act_Refraction_2.Checked, dmLink_view_data.DBLink.Refraction_2, 0);


    //         oDBLink := dmLink_view_data.DBLink;



    //  e:=dxBarSpinEdit1_Refraction.Value;


    Fframe_Profile_Rel.ShowProfile;



//    dmLink.Update_(FID, [db_Par(FLD_is_profile_reversed, cb_Reversed.Checked)]);

//    dmLink_view_data.OpenData (FID);

 //   View(FID);
  end;


  //------------------------------------------------------
  if Sender=act_profile_direction_Change_new then
  //------------------------------------------------------
  begin             //  is_profile_reversed
    dmLink.Update_(FID, [db_Par(FLD_is_profile_reversed, cb_Reversed.Checked)]);

    dmLink_view_data.OpenData (FID);

    View(FID);
  end;

end;


// ---------------------------------------------------------------
procedure Tframe_Link_profile.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
// ---------------------------------------------------------------
begin
  act_Save.Enabled:=Fframe_Ant1.IsModified or
                    Fframe_Ant2.IsModified;

  //

  cb_Reversed.Enabled:= FDBLink_ref.LinkRepeater.id=0;

end;


procedure Tframe_Link_profile.cb_Reflection_pointsClick(Sender: TObject);
begin

  Fframe_Profile_Rel.Params.IsShowReflectionPoints:=cb_Reflection_points.Checked;
  Fframe_Profile_Rel.ShowProfile;
end;


//--------------------------------------------------------------------
procedure Tframe_Link_profile.act_Fren_TopExecute(Sender: TObject);
//--------------------------------------------------------------------
var
  e: Double;
begin
  if (Sender = act_Fren_Top)   then
    Fframe_Profile_Rel.Params.IsShowFrenelZoneTop:=cb_Fren_Top.Checked;

  if (Sender = act_Fren_Left)  then
    Fframe_Profile_Rel.Params.IsShowFrenelZoneBottomLeft:=cb_Fren_Left.Checked;

  if (Sender = act_Fren_Right) then
     Fframe_Profile_Rel.Params.IsShowFrenelZoneBottomRight:=cb_Fren_Right.Checked;

  if (Sender = act_Fren_Bot) then
    Fframe_Profile_Rel.Params.IsShowFrenelZoneBottom:=cb_Fren_Bot.Checked;

  if (sender = cxBarEditItem_refraction_2_checked)
  then
    Fframe_Profile_Rel.Params_Refraction_2:=
       IIF(cxBarEditItem_refraction_2_checked.EditValue=True, dmLink_view_data.DBLink.Refraction_2, 0);


//         oDBLink := dmLink_view_data.DBLink;



//  e:=dxBarSpinEdit1_Refraction.Value;


  Fframe_Profile_Rel.ShowProfile;
end;

procedure Tframe_Link_profile.act_Refraction_2Execute(Sender: TObject);
begin
  inherited;
end;



//-------------------------------------------------------------------
procedure Tframe_Link_profile.Calc;
//-------------------------------------------------------------------
begin
  act_Save.Execute;

  dmAct_Link.Calc (FID);

  Fframe_Link_profile_summary.View (FID);

  if cb_Reflection_points.Checked then
    Fframe_Profile_Rel.Params.SiteGroups[0].ReflectionPointArr:=dmLink.GetReflectionPointArray(FID)
  else
    Fframe_Profile_Rel.Params.SiteGroups[0].ReflectionPointArr:=nil;


  Fframe_Profile_Rel.ShowProfile;

  if Assigned(FOnUpdateAfterCalc) then
    FOnUpdateAfterCalc(Self);

end;


procedure Tframe_Link_profile.cb_Refraction_2Click(Sender: TObject);
begin
   Fframe_Profile_Rel.Params_Refraction_2:=   IIF(cb_Refraction_2.Checked, dmLink_view_data.DBLink.Refraction_2, 0);

  Fframe_Profile_Rel.ShowProfile;
end;


//--------------------------------------------------------------------
procedure Tframe_Link_profile.DoOnChangedEdits (Sender: TObject);
//--------------------------------------------------------------------
var
  rLinkParamRec: TdmLinkInfoRec;
begin
  if dmLink.GetInfo1(FID, rLinkParamRec) then
    if Fframe_Profile_Rel.Params.Freq_MHz <> rLinkParamRec.Tx_Freq_MHz then
    begin
      Fframe_Profile_Rel.Params.Freq_MHz := rLinkParamRec.Tx_Freq_MHz;
      Fframe_Profile_Rel.ShowGraph;
    end;

  if Sender=Fframe_Ant1 then  Fframe_Ant2.RefreshData;
  if Sender=Fframe_Ant2 then  Fframe_Ant1.RefreshData;

end;

procedure Tframe_Link_profile.DoOnChangeAntennaHeight(Sender: TObject);
begin
   ShowSites();
end;

procedure Tframe_Link_profile.DoOnDataChanged(Sender: TObject);
begin
  View(FID);
end;

procedure Tframe_Link_profile.DoOnCalc(Sender: TObject);
begin
  Calc();
end;


//-------------------------------------------------------------------
function Tframe_Link_profile.Reload_Profile: Boolean;
//-------------------------------------------------------------------
var
  e: Double;
  s: string;
  sXML: string;
begin
  Assert( FLinkParamRec.BLVector.Point1.b<>0,
    'Tframe_Link_profile.Reload_Profile: Boolean: FLinkParamRec.BLVector.Point1.b<>0');

  Assert( FLinkParamRec.BLVector.Point1.L<>0,
    'Tframe_Link_profile.Reload_Profile: Boolean: FLinkParamRec.BLVector.Point1.L>0');

    
  s:=geo_FormatBLPoint(FLinkParamRec.BLVector.Point1) + ' - ' +
     geo_FormatBLPoint(FLinkParamRec.BLVector.Point2);



  g_Log.Msg('function Tframe_Link_profile.Reload_Profile: '+ s );



  dmRel_Engine.Open ();

  e:=geo_Distance_km(FLinkParamRec.BLVector);

  g_Log.Msg('geo_Distance_km: '+ FloatToStr(e)) ;


//  dmRel_Engine.AssignClutterModel (FLinkParamRec.ClutterMOdelID, FrelProfile.Clutters);
 // b :=
 // FrelProfile.isDirectionReversed :=False;

  Result := dmRel_Engine.BuildProfile1 (FrelProfile,
       FLinkParamRec.BLVector, FLinkParamRec.PROFILE_Step_M);//, FClutterModelID);

  dmRel_Engine.Close1;


  if Result then
  begin
    FrelProfile.isDirectionReversed := FLinkParamRec.is_profile_reversed;
//    FrelProfile.SetDirection(FLinkParamRec.CalcDirection);

  //  FrelProfile.Property1_id:=FLinkParamRec.PropertyID1;
//    FrelProfile.Property2_id:=FLinkParamRec.PropertyID2;

    g_Log.Msg('FrelProfile.Count: ' + IntToStr(FrelProfile.Count)) ;


    sXML:=FrelProfile.GetXML();


////////    ShellExec_Notepad_temp (sXML  );

    dmLink.Update_profile_XML(FID, sXML);

    g_Log.Msg(sXML) ;


    g_Log.Msg('������� �������� � ��.') ;

//    Fframe_Profile_Rel.RelProfile_Ref:=FrelProfile;

//    Fframe_Profile_Rel.IsDirectionReversed := FLinkParamRec.is_profile_reversed;


  end else
    g_Log.Error('�� ������� ��������� �������!') ;

//    ShowMessage('�� ������� ��������� �������!') ;

end;


//-------------------------------------------------------------------
procedure Tframe_Link_profile.DoOnProfileChanged(Sender: TObject);
//-------------------------------------------------------------------
var
  sXML: string;
begin
//  sXML:=Fframe_Profile_Rel.RelProfile_Ref.GetXML();

 // StringToFile(sXML, 's:\_1.xml');
 // ShowMessage(sXML);
                                

  FRelProfile.Assign(Fframe_Profile_Rel.RelProfile_Ref);

  sXML:=FrelProfile.GetXML();

 // StringToFile(sXML, 's:\_2.xml');

  dmLink.Update_profile_XML(FID,sXML);


end;

procedure Tframe_Link_profile.dxBarSpinEdit1_RefractionCurChange(Sender:  TObject);
begin
//  showMessage ('Tframe_Link_profile.dxBarSpinEdit1_RefractionCurChange');
end;

procedure Tframe_Link_profile.pn_cxPageControl1Resize(Sender: TObject);
begin
  pn_ant_left.Width:=pn_cxPageControl1.Width div 2;
end;


//-------------------------------------------------------------------
function Tframe_Link_profile.Run_Link_Optimize_freq_diversity(aID: Integer):
    Boolean;
//-------------------------------------------------------------------
begin
  TLink_run.Run_Link_Optimize_freq_diversity (aID);

end;

end.

{




procedure Tframe_Link_profile.cxDBCheckBox2PropertiesChange(
  Sender: TObject);
begin
  ShowMessage('cxDBCheckBox2PropertiesChange');

end;


procedure Tframe_Link_profile.DBCheckBox1Click(Sender: TObject);
begin
 // ShowMessage('DBCheckBox1Click');
end;

           ��������� ���������� �� �������
