unit dm_Link_calc_SESR;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms, 

  dm_Main,
  dm_Onega_DB_data,

  u_db,
  u_func,

//  u_types,

  u_const_db,

  u_Link_const,
  u_LinkLine_const;


type

  TGet_Equivalent_Length_KM_rec = record
     Length_Km   : double;
     GST_length_km   : double;
     GST_Type  : integer;

  end;




(*

  TdmLink_calc_SESR_rec = record
    ID: integer;
    ObjName: string;
    LengthKM: double;
    GSTType: integer;

    LinkLineSpeed: double; //=0

    //result
    EsrNorm, EsrReq, BberNorm, BberReq: double;

  end;
*)

  TCalcRequiredEsr = record
                       GST_Type: integer;
                       GST_Length_KM: double;

                       EsrNorm: Double;
                       BberNorm: double;

                     //  var
                       Result: record
                         aEsrReq: Double;
                         aBberReq: double;
                       end;  
                     end;



  TCalcEsrBberParamRec = record
                      ID: integer;
                      ObjName: string;
                      Length_kM: double;
                      GST_Type: integer;

                      Bitrate_Mbps : double;

                      //var
                      Output: record
                                Esr_Norm: Double;
                                Bber_Norm: Double;

                                Esr_required: Double;
                                Bber_required: double;
                              end;
                      GSTLength_km: Double;

                      LinkLineSpeed_Mbps: Double;

                    end;



  TdmLink_calc_SESR = class(TDataModule)
    qry_Temp: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
  public
    function CalcEsrBber11(var aRec: TCalcEsrBberParamRec): boolean;

    procedure CalcRequiredEsr (
                              // var aRec: TCalcRequiredEsr;

                               aGSTType: Integer;
                               aLength_KM: double;

                               aEsrNorm, aBberNorm: double;

                               var aEsrReq, aBberReq: double);

    function Get_Equivalent_Length_KM_(aLength_KM, aGST_length_km: double;
        aGST_Type: integer): double;

        //; aDBLink: TDBLink
    function Get_Equivalent_Length_KM1(aDataset: TDataset): Double;

    function Get_Equivalent_Length_KM_rec(aRec: TGet_Equivalent_Length_KM_rec):
        Double;




  end;


function dmLink_calc_SESR: TdmLink_calc_SESR;


//===================================================================
implementation {$R *.dfm}
//===================================================================


var
  FdmLink_calc_SESR: TdmLink_calc_SESR = nil;


// ---------------------------------------------------------------
function dmLink_calc_SESR: TdmLink_calc_SESR;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLink_calc_SESR) then
      FdmLink_calc_SESR := TdmLink_calc_SESR.Create(Application);

  Result := FdmLink_calc_SESR;
end;



procedure TdmLink_calc_SESR.DataModuleDestroy(Sender: TObject);
begin
  FdmLink_calc_SESR := nil;
//  ShowMessage('procedure TdmLink_calc_SESR.DataModuleDestroy(Sender: TObject);');
end;

procedure TdmLink_calc_SESR.DataModuleCreate(Sender: TObject);
begin
  db_SetComponentADOConnection(Self, dmMain.AdoConnection);
end;

//-------------------------------------------------------------------
function TdmLink_calc_SESR.Get_Equivalent_Length_KM1(aDataset: TDataset):
    Double;
//; aDBLink: TDBLink
//-------------------------------------------------------------------
var
  eLength_Km: double;
  dGST_length_km: double;
  e: double;
  iGST_Type: integer;
 // dRoundedLength_km: Double;
begin
(*  Assert(Assigned(aDBLink), 'Value not assigned');

  eLength_Km  :=aDBLink.Length_km;
  dGST_length_km      :=aDBLink.Trb.GST_LENGTH;
  iGST_Type         :=aDBLink.Trb.GST_TYPE;
*)

  eLength_Km     :=aDataset.FieldByName(FLD_LENGTH).AsFloat/1000;
  dGST_length_km :=aDataset.FieldByName(FLD_GST_LENGTH).AsFloat;
  iGST_Type      :=aDataset.FieldByName(FLD_GST_TYPE).AsInteger;

  e:=Get_Equivalent_Length_KM_(eLength_Km, dGST_length_km, iGST_Type);

  if e>0 then
    Result:=eLength_Km * dGST_length_km / e
  else
    Result:=0;


end;


// --------------------------------------------------
function TdmLink_calc_SESR.Get_Equivalent_Length_KM_(aLength_KM,
    aGST_length_km: double; aGST_Type: integer): double;
// --------------------------------------------------
//  aLength_KM - link length
// --------------------------------------------------

    //----------------------------------------------------------------
    function RoundToValue(aValue: integer; aToValue: integer): double;
    //----------------------------------------------------------------
    var
        d1,d2: double;
    begin
      { TODO : aValue:=0; }
      if aValue<=0 then
        aValue:=1;

      d1:= aValue mod aToValue;
      d2:=(aValue div aToValue);
      Result:=IIF(d1 = 0,
                  aToValue*d2,
                  aToValue*(d2 + 1)
                 );
    end;

begin
    if aGST_Type = 0 then
      Result := aLength_KM
    else

    case LINKLINE_NORM_ARR1[aGST_Type].NamePr92 of     //
    // 1 - 2500 km
    1: begin
         if aLength_KM>=2500 then
           Result:=aLength_KM
         else

         if ((aLength_KM>1000) and (aLength_KM<2500)) then
           Result:=RoundToValue(Round(aLength_KM), 500)
         else
           Result:=RoundToValue(Round(aLength_KM), 250);

         if Result>aGST_length_km then
           Result := aGST_length_km;
       end;
    2: begin
       //  aLength_KM:=1;

         if aLength_KM>200 then
           Result:=RoundToValue(Round(aLength_KM), 100)
         else
           Result:=RoundToValue(Round(aLength_KM), 50);

         if Result>aGST_length_km then
           Result := aGST_length_km;
       end;
    3: begin
         Result:=100;
       end;
    4: begin
         Result:=aGST_length_km;
       end;
  end;
end;

//-------------------------------------------------------------------
function TdmLink_calc_SESR.CalcEsrBber11(var aRec: TCalcEsrBberParamRec):
    boolean;
//-------------------------------------------------------------------


type

  TLinkCSTTypeRec = record
    Bitrate_Mbps : double;   //speed
    Param_A_ESR  : double;
    Param_A_BBER : double;
  end;


const

  LINK_CST_TYPE_ARRAY: array [0..4] of TLinkCSTTypeRec =
    (
     (Bitrate_Mbps: 0.064;  Param_A_ESR:  0.08;  Param_A_BBER: 0   ),
     (Bitrate_Mbps: 2.048;  Param_A_ESR:  0.04;  Param_A_BBER: 0.0003  ),
     (Bitrate_Mbps: 8.448;  Param_A_ESR:  0.05;  Param_A_BBER: 0.0002  ),
     (Bitrate_Mbps: 34.368; Param_A_ESR:  0.075;  Param_A_BBER: 0.0002 ),
     (Bitrate_Mbps: 139.264; Param_A_ESR:  0.16;  Param_A_BBER: 0.0002  )
    );

var
  dESR_norm, dBBER_norm, dESR_required, dBBER_required,
  dLength1, dBitrate_Mbps: double;
  d_A_ESR, d_A_BBER: double;
  iGSTType: integer;
  i: Integer;
  iLinkEndId1: integer;
  dGSTLength_km: double;
  sTable: string;
begin
(*  Assert(aRec.ID=aID);
  Assert(aRec.ObjName=aObjName);
  Assert(aRec.LengthKM=aLengthKM);
*)
 // aID, aObjType, aLengthKM, aGSTType;
   dGSTLength_km:=0;


  if aRec.ObjName = 'LINK' then
  begin
//    iLinkEndId1:=dmLINK.GetLinkEnd1ID (aID);

  /////////  dBitrate_Mbps:=dmLINK.GetSpeed (aID);

    dBitrate_Mbps:=aRec.Bitrate_Mbps;

   // dmLINKEND.GetDoubleFieldValue (iLinkEndID1, FLD_SPEED);
  end  else

  if aRec.ObjName = 'LINKLINE' then
    dBitrate_Mbps:=aRec.LinkLineSpeed_Mbps
  else
    raise Exception.Create('aObjType := ?');



  if (dBitrate_Mbps = 0) then
    dBitrate_Mbps:=2 else

  if (dBitrate_Mbps > 139) then
    dBitrate_Mbps:=139;

(*
  for I := 4 downto 0 do
    if dBitrate_Mbps <= Trunc(LINK_CST_TYPE_ARRAY[i].Bitrate_Mbps) then
    begin
      d_A_ESR:=LINK_CST_TYPE_ARRAY[i].Param_A_ESR;
      d_A_BBER:=LINK_CST_TYPE_ARRAY[i].Param_A_BBER;
    end;
*)

  if aRec.GST_Type > 0 then
  begin
    for I := 4 downto 0 do
      if dBitrate_Mbps <= Trunc(LINK_CST_TYPE_ARRAY[i].Bitrate_Mbps) then
      begin
        d_A_ESR :=LINK_CST_TYPE_ARRAY[i].Param_A_ESR;
        d_A_BBER:=LINK_CST_TYPE_ARRAY[i].Param_A_BBER;
      end;


    aRec.Output.Esr_Norm :=100*(d_A_ESR* LINKLINE_NORM_ARR1[aRec.GST_Type].Param_C);
    TruncFloatVar(aRec.Output.Esr_Norm, 6);

    aRec.Output.Bber_Norm:=100*(d_A_BBER*LINKLINE_NORM_ARR1[aRec.GST_Type].Param_C);
    TruncFloatVar(aRec.Output.Bber_Norm, 6);

    CalcRequiredEsr(//aRec,
                    aRec.GST_Type, aRec.Length_KM,

                    aRec.Output.Esr_Norm,
                    aRec.Output.Bber_Norm,

                    aRec.Output.Esr_Required,
                    aRec.Output.Bber_Required);
  end

    else

  begin
    if Eq(aRec.ObjName, 'LINK') then
      sTable:=TBL_LINK else

    if Eq(aRec.ObjName, 'LINKLINE') then
      sTable:=TBL_LINKLINE

    else
      raise Exception.Create('aObjType = ?');


      dmOnega_DB_data.OpenQuery(qry_Temp,
        Format('select ESR_NORM, BBER_NORM, GST_LENGTH FROM %s where id=%d',
            [sTable, aRec.ID]));

   {

    // ---------------------------------------------------------------
    if Eq(aRec.ObjName, 'LINK') then
    // ---------------------------------------------------------------
    begin

      dmOnega_DB_data.OpenQuery(qry_Temp,
        Format('select ESR_NORM, BBER_NORM, GST_LENGTH FROM %s where id=%d',
            [TBL_LINK, aRec.ID]));


    //   'select ESR_NORM, BBER_NORM, GST_LENGTH where id='+ IntToStr(aRec.ID));

      aRec.Output.Esr_Norm  := qry_Temp.FieldByName(FLD_BBER_NORM).AsFloat;
      aRec.Output.Bber_Norm := qry_Temp.FieldByName(FLD_BBER_NORM).AsFloat;
      aRec.GSTLength_km     := qry_Temp.FieldByName(FLD_GST_LENGTH).AsFloat;


      dGSTLength_km :=aRec.GSTLength_km;

      aRec.Output.Esr_Required:=aRec.Output.Esr_Norm * aRec.Length_KM / dGSTLength_km;
      TruncFloatVar(aRec.Output.Esr_Required, 6);

      aRec.Output.Bber_Required:=aRec.Output.Bber_Norm * aRec.Length_KM / dGSTLength_km;
      TruncFloatVar(aRec.Output.Bber_Required, 6);
    end    else

    // ---------------------------------------------------------------
    if  Eq(aRec.ObjName, 'LINKLINE') then
    // ---------------------------------------------------------------
    begin
      dmOnega_DB_data.OpenQuery(qry_Temp,
        Format('select ESR_NORM, BBER_NORM, GST_LENGTH FROM %s where id=%d',
            [TBL_LINKLINE, aRec.ID]));

      }
//      ''+ IntToStr(aRec.ID));

      aRec.Output.Esr_Norm  := qry_Temp.FieldByName(FLD_BBER_NORM).AsFloat;
      aRec.Output.Bber_Norm := qry_Temp.FieldByName(FLD_BBER_NORM).AsFloat;
      aRec.GSTLength_km     := qry_Temp.FieldByName(FLD_GST_LENGTH).AsFloat;


      aRec.Output.Esr_Required:=aRec.Output.Esr_Norm*aRec.Length_KM / aRec.GSTLength_km; // dGSTLength_km;
      TruncFloatVar(aRec.Output.Esr_Required, 6);

      aRec.Output.Bber_Required:=aRec.Output.Bber_Norm*aRec.Length_KM / aRec.GSTLength_km; //   dGSTLength_km;
      TruncFloatVar(aRec.Output.Bber_Required, 6);
  //  end
   // else
    //  raise Exception.Create('aObjType = ?');

  end;


  aRec.output.Esr_Norm      :=  TruncFloat(aRec.output.Esr_Norm,  6);
  aRec.output.Bber_Norm     :=  TruncFloat(aRec.output.Bber_Norm, 6);
  aRec.output.Esr_Required  :=  TruncFloat(aRec.output.Esr_Required,   6);
  aRec.output.Bber_Required :=  TruncFloat(aRec.output.Bber_Required,  6);

end;

//-------------------------------------------------------------------------
procedure TdmLink_calc_SESR.CalcRequiredESR(
      //   var aRec: TCalcRequiredEsr;

         aGSTType: integer;
         aLength_KM: double;

         aEsrNorm, aBberNorm: double;

         var aEsrReq, aBberReq: double);

//-------------------------------------------------------------------------

var
  dLength_km: double;
  eMax_Length_KM: Double;

  eEsrReq, eBberReq: double;

begin
  dLength_km:=Get_Equivalent_Length_KM_(aLength_KM, LINKLINE_NORM_ARR1[aGSTType].Max_Length_KM, aGSTType);

  eMax_Length_KM:=LINKLINE_NORM_ARR1[aGSTType].Max_Length_KM;

  case LINKLINE_NORM_ARR1[aGSTType].NamePr92 of     //
    1: begin
         eEsrReq:= aEsrNorm  * dLength_km / LINKLINE_NORM_ARR1[aGSTType].Max_Length_KM;
         eBberReq:=aBberNorm * dLength_km / LINKLINE_NORM_ARR1[aGSTType].Max_Length_KM;
       end;
    2: begin
         eEsrReq :=aEsrNorm*dLength_km  / LINKLINE_NORM_ARR1[aGSTType].Max_Length_KM;
         eBberReq:=aBberNorm*dLength_km / LINKLINE_NORM_ARR1[aGSTType].Max_Length_KM;
       end;
    3: begin
         eEsrReq:=aEsrNorm;
         eBberReq:=aBberNorm;
       end;
    4: begin
         eEsrReq:=aEsrNorm;
         eBberReq:=aBberNorm;
       end;
  end;    // case

  TruncFloatVar(eEsrReq, 6);
  TruncFloatVar(eBberReq, 6);


//  aRec.Result.aEsrReq :=eEsrReq;
//  aRec.Result.aBberReq:=eBberReq;

  aEsrReq :=eEsrReq;
  aBberReq:=eBberReq;


end;

// ---------------------------------------------------------------
function TdmLink_calc_SESR.Get_Equivalent_Length_KM_rec(aRec:     
    TGet_Equivalent_Length_KM_rec): Double;
// ---------------------------------------------------------------

var
  e: double;
begin

  e:=Get_Equivalent_Length_KM_(aRec.Length_Km, aRec.GST_length_km, aRec.GST_Type);

  if e>0 then
    Result:=aRec.Length_Km * aRec.GST_length_km / e
  else
    Result:=0;

end;


end.


{


var
  eLength_Km: double;
  dGST_length_km: double;
  e: double;
  iGST_Type: integer;
 // dRoundedLength_km: Double;
begin
(*  Assert(Assigned(aDBLink), 'Value not assigned');

  eLength_Km  :=aDBLink.Length_km;
  dGST_length_km      :=aDBLink.Trb.GST_LENGTH;
  iGST_Type         :=aDBLink.Trb.GST_TYPE;
*)

  eLength_Km     :=aDataset.FieldByName(FLD_LENGTH).AsFloat/1000;
  dGST_length_km :=aDataset.FieldByName(FLD_GST_LENGTH).AsFloat;
  iGST_Type      :=aDataset.FieldByName(FLD_GST_TYPE).AsInteger;

  e:=Get_Equivalent_Length_KM_(eLength_Km, dGST_length_km, iGST_Type);

  if e>0 then
    Result:=eLength_Km * dGST_length_km / e
  else
    Result:=0;


end;
