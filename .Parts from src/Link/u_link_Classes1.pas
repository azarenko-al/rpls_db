unit u_link_classes1;

interface
uses
  DB, Classes, SysUtils, dialogs,

  u_func,

  u_db,

  u_Link_const,
  u_const_db,

  u_Geo;


type
  TDBLinkReflectionPointList = class;
  TDBAntennaList = class;
  TDBLinkEnd     = class;
  TDBLinkRepeater= class;

  //status
  TDBLinkWorkType = (lwtNotDefined,lwtWork,lwtNoWork);

  TDBLinkType = (ltLink_,ltPmpLink_);


  TDBLink_ = class
  private



    function GetLength_km: double;
  public
    ID  : Integer;

    Is_UsePassiveElements     : Boolean;
    Is_CalcWithAdditionalRain : Boolean;


    Property1_Pos: TBLPoint;
    Property2_Pos: TBLPoint;


    GeoRegion1_ID : Integer;
    GeoRegion2_ID : Integer;


    Property1_ID: Integer;
    Property2_ID: Integer;


    Property1_Ground_Height: Variant;
    Property2_Ground_Height: Variant;


//    LinkType: TDBLinkType;

    Project_id : Integer;

    LinkEnd1_ID: Integer;
    LinkEnd2_ID: Integer;

//    PmpSector_ID: Integer;
//    PmpTerminal_ID: Integer;


    BLVector   :  TBLVector;

    Calc_method : Integer; // 0 "����, ITU-R � 16 ����� ��" (�� ���������)
				                   // 1 "���� � 53363 - 2009"
                           // 0 "���� 1998

    ClutterModel_ID: Integer;

    Is_profile_reversed : Boolean;

    Profile_Step_m: Integer;

    Refraction : Double;
    Refraction_2 : Double;

    Tx_Freq_MHz : Double;

    // ����� -----------------------------
    Rains: record
              IsCALCLENGTH : Boolean;
              rain_INTENSITY_extra : Double;
           end;


  //  BER_REQUIRED_Index : Integer; //IIF((iBer_Required=0), STR_BER3 , STR_BER6);

    Length_m : Double;
    NFrenel: Double;

    Profile_XML: string;


    CALC_RESULTS_XML: String;

   // Repeater_ID : Integer;

//    Has_Repeater : boolean;


    property Length_km: double read GetLength_km;
  public
    ReflectionPoints: TDBLinkReflectionPointList;

    LinkEnd1: TDBLinkEnd;
    LinkEnd2: TDBLinkEnd;

    LinkRepeater: TDBLinkRepeater;

    constructor Create;
    destructor Destroy; override;

    procedure LoadFromDataset(aDataset: TDataset);

    function Validate: Boolean;
  end;


  // ---------------------------------------------------------------
  TDBLinkReflectionPoint = class(TCollectionItem)
  // ---------------------------------------------------------------
  private
//    Signal_Depression_dB: Double;
  public
    DISTANCE_to_center_km : Double; //���������� �� ������� 1-�� ������� ��������� [km]
    ABS_PROSVET_m : Double; //A��������� ������� ��� �������� 1-�� ������� �����. [m]
                            //����������� ������� ��� �������� 1-�� �������
                            //�����������.������� ��� ��������
    Length_km : Double;     //������������� 1-�� �������
    RADIUS_km : Double;     //P����� �������� 1-�� �������

  end;

  // ---------------------------------------------------------------
  TDBLinkReflectionPointList = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TDBLinkReflectionPoint;
  public
    constructor Create;
    function AddItem: TDBLinkReflectionPoint;

    property Items[Index: Integer]: TDBLinkReflectionPoint read GetItems; default;
  end;

  // ---------------------------------------------------------------
  TDBLinkEnd = class
  // ---------------------------------------------------------------
  private
    function GetTx_Freq_GHz: double;
  public
    property Tx_Freq_GHz: double read GetTx_Freq_GHz;
  public
    LinkEndType_ID : Integer;

    THRESHOLD_BER_3    : double;
    THRESHOLD_BER_6    : double;
    Power_dBm          : double;

    TX_FREQ_MHz        : Double;

    BitRate_Mbps       : double;

    Passive_element_LOSS_dBm  : Double;
    LOSS_dBm           : Double;
    KNG                : Double;

    KRATNOST_BY_SPACE  : Integer;

    KRATNOST_BY_FREQ   : Integer;
    FREQ_CHANNEL_COUNT : Integer;

    Freq_Spacing_MHz   : Double;

    SIGNATURE_WIDTH : double;// '������ ���������, ���');
    SIGNATURE_HEIGHT: double;// '������ ���������, ��');
    MODULATION_COUNT: double;// '����� ������� ���������');
//    EQUALISER_PROFIT1: double;// '�������, �������� ������������, ��');


    ATPC_profit_dB  : double;// <param id="45" value="10" comment="������� �� ATPC, ��"/>
    Use_ATPC        : Boolean;
    Threshold_degradation_dB: Double; //Threshold_degradation_dB,  '���������� ����������������, ��');

    GOST_53363_modulation : string;
    GOST_53363_RX_count_on_combined_diversity : Integer;

  public
    Antennas: TDBAntennaList;

    constructor Create;
    destructor Destroy; override;

    procedure LoadFromDataset(aDataset: TDataset);
  end;


  TDBAntennaPolarizationType = (ptV__,ptH__,ptX__);

  // ---------------------------------------------------------------
  TDBAntenna = class(TCollectionItem)
  // ---------------------------------------------------------------
  private
    Polarization_str    : string;

  public
    Height              : Double;
    Diameter            : Double;
    Gain                : Double;
    Vert_width          : Double;
    Loss            : Double;

    function GetPolarization1: Integer;
    function GetPolarizationType: TDBAntennaPolarizationType;

    procedure LoadFromDataset(aDataset: TDataset);

    property PolarizationType: TDBAntennaPolarizationType read GetPolarizationType;
  end;

  // ---------------------------------------------------------------
  TDBAntennaList = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TDBAntenna;
  public
    constructor Create;
    function AddItem: TDBAntenna;

    function GetAntennaHeightArr: TDoubleArray;

    procedure LoadFromDataset(aDataset: TDataset);
    property Items[Index: Integer]: TDBAntenna read GetItems; default;
  end;


  // ---------------------------------------------------------------
  TDBLinkRepeater = class
  // ---------------------------------------------------------------
  private
    Antennas: TDBAntennaList;
  public
     id : Integer;


     Property_Ground_Height : Variant;

     Property_Pos: TBLPoint;
     Property_id : Integer;

//     BLVector2: TBLVector;
//     BLVector1: TBLVector;

     Part1, Part2:
     record
       BLVector: TBLVector;

       Profile_XML1 : string;
       Distance_KM : double;
       Antenna_Height : double;

      Property_Pos: TBLPoint;

     end ;

    {
     Profile1_XML : string;
     Profile2_XML : string;

     Distance1_KM : double;
     Distance2_KM : double;

     Antenna1_Height : double;
     Antenna2_Height : Double;
    }
    
     CALC_RESULTS_XML_1: String ;
     CALC_RESULTS_XML_2: String ;
     CALC_RESULTS_XML_3: String ;


  public
    constructor Create;
    destructor Destroy; override;

    procedure LoadFromDataset(aDataset: TDataset);
  end;


implementation


constructor TDBLink_.Create;
begin
  inherited Create;

  ReflectionPoints := TDBLinkReflectionPointList.Create();
  LinkEnd1 := TDBLinkEnd.Create();
  LinkEnd2 := TDBLinkEnd.Create();
  LinkRepeater := TDBLinkRepeater.Create();
end;


destructor TDBLink_.Destroy;
begin
  FreeAndNil(LinkRepeater);
  FreeAndNil(LinkEnd2);
  FreeAndNil(LinkEnd1);
  FreeAndNil(ReflectionPoints);
  
  inherited Destroy;
end;

// ---------------------------------------------------------------
procedure TDBLink_.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------


var
  e: Double;
  s: string;
begin
//  Assert(Assigned(aDataset), 'Value not assigned');
//  Assert(aDataset.RecordCount>0, 'Value not assigned');

//  db_View1(aDataset);



   // ��� - ������� ��������������� ���������

  with aDataset do
  begin
    ID := FieldByName(FLD_ID).AsInteger;

    Tx_Freq_MHz     :=FieldByName(FLD_Tx_Freq_MHz).AsFloat;




    GeoRegion1_ID := FieldByName(FLD_GeoRegion1_ID).AsInteger;
    GeoRegion2_ID := FieldByName(FLD_GeoRegion2_ID).AsInteger;


    // -------------------------
    //Variant
    // -------------------------
    Property1_Ground_Height := FieldByName(FLD_Property1_Ground_Height).AsVariant;
    Property2_Ground_Height := FieldByName(FLD_Property2_Ground_Height).AsVariant;


  //  Project_id :=  FieldByName(FLD_Project_id).AsInteger;

//    Project_id :=  FieldByName(FLD_Project_id).AsInteger;
//    Project_id :=  FieldByName(FLD_Project_id).AsInteger;

    Is_UsePassiveElements     := FieldByName(FLD_Is_UsePassiveElements).AsBoolean;
    Is_CalcWithAdditionalRain := FieldByName(FLD_Is_CalcWithAdditionalRain).AsBoolean;

    Profile_XML :=FieldByName(FLD_Profile_XML).AsString ;

    CALC_RESULTS_XML:= FieldByName(FLD_CALC_RESULTS_XML).AsString ;

{
    s := FieldByName(FLD_OBJNAME).AsString;
    if Eq (s, 'pmp_link')
      then LinkType:=ltPmpLink_
      else LinkType:=ltLink_;
}

    {
     case LinkType of
        ltPmpLink_: begin
//          PmpSector_ID  :=FieldByName(FLD_PMP_SECTOR_ID).AsInteger;
 //         PmpTerminal_ID:=FieldByName(FLD_PMP_TERMINAL_ID).AsInteger;
        end;

        ltLink_: begin
          LinkEnd1_ID :=FieldByName(FLD_LinkEnd1_ID).AsInteger;
          LinkEnd2_ID :=FieldByName(FLD_LinkEnd2_ID).AsInteger;
        end;
      end;
    }


    LinkEnd1_ID :=FieldByName(FLD_LinkEnd1_ID).AsInteger;
    LinkEnd2_ID :=FieldByName(FLD_LinkEnd2_ID).AsInteger;

    Assert (LinkEnd1_ID  > 0);


    Property1_ID  :=FieldByName(FLD_Property1_ID).AsInteger;
    Property2_ID  :=FieldByName(FLD_Property2_ID).AsInteger;

  //  Property_ground_height_1  :=FieldByName(FLD_Property1_ID).AsInteger;
  //  Property_ground_height_1  :=FieldByName(FLD_Property2_ID).AsInteger;


    BLVector :=db_ExtractBLVector (aDataset);

    e:=geo_Distance_m(BLVector);

    Assert(geo_Distance_m(BLVector)>0, 'geo_Distance_m(BLVector)>0');


    LinkRepeater.Part1.Property_Pos:= BLVector.Point1;
    LinkRepeater.Part2.Property_Pos:= BLVector.Point2;



    Refraction   :=FieldByName(FLD_Refraction).AsFloat;

    if Assigned(Fields.FindField(FLD_Refraction_2)) then
      Refraction_2 :=FieldByName(FLD_Refraction_2).AsFloat;

    // -----------------------------
    // profile_reversed
    // -----------------------------
    is_profile_reversed:=FieldByName(FLD_is_profile_reversed).AsBoolean;

    if // (LinkType=ltLink_) and
       (is_profile_reversed) then
    begin
      ExchangeInt(LinkEnd1_ID, LinkEnd2_ID);
      ExchangeInt(Property1_ID, Property2_ID);

      BLVector := geo_RotateBLVector_(BLVector);
    end;



    Property1_Pos:=BLVector.Point1;
    Property2_Pos:=BLVector.Point2;

    // -----------------------------

    Calc_Method:=FieldByName(FLD_Calc_Method).AsInteger;

    Profile_Step_m:=FieldByName(FLD_PROFILE_STEP).AsInteger;

    ClutterModel_ID:=FieldByName(FLD_CLUTTER_MODEL_ID).AsInteger;

    Rains.ISCALCLENGTH  := FieldByName(FLD_RAIN_ISCALCLENGTH).AsBoolean;
    Rains.rain_INTENSITY_extra := FieldByName(FLD_rain_INTENSITY_extra).AsFloat;

  //  Rains.RAIN_RATE:=      FieldByName(FLD_RAIN_RATE).AsFloat;   //  '������������� ����� � ������� 0.01% �������, ��/���');


  //  BER_REQUIRED_Index := FieldByName(FLD_BER_REQUIRED).AsInteger;


    length_m    :=FieldByName(FLD_length).AsFloat;
    NFrenel     :=FieldByName(FLD_NFrenel).AsFloat;


//    Link_repeater.ID :=FieldByName(FLD_Link_repeater_ID).AsInteger;
 //   LinkRepeater.ID :=FieldByName(FLD_Link_repeater_ID).AsInteger;

  //  Has_Repeater  :=FieldByName(FLD_has_Repeater).AsBoolean;


     LinkRepeater.ID    := FieldByName(FLD_LINK_REPEATER_ID).AsInteger;

(*    if Assigned(Fields.FindField(FLD_Repeater_lat)) then
    begin
      Repeater_Pos.B := FieldByName(FLD_Repeater_lat).AsFloat;
      Repeater_Pos.L := FieldByName(FLD_Repeater_lon).AsFloat;
    end;
*)
  end;
end;


function TDBLink_.GetLength_km: double;
begin
  Result := Length_m / 1000;
end;


// ---------------------------------------------------------------
function TDBLink_.Validate: Boolean;
// ---------------------------------------------------------------
var
  oStrList: TStringList;
  s1: string;
  s2: string;

begin
 // Result:=True;

  oStrList:=TStringList.Create();

  if (LinkEnd1.Antennas.Count=0) or (LinkEnd2.Antennas.Count=0) then
 // begin
    oStrList.Add('���-�� ������ = 0');
   // ErrorDlg ('���-�� ������ = 0');
  //  Result:=False;
 // end;



  if (LinkEnd1.Antennas[0].Polarization_str <>
      LinkEnd2.Antennas[0].Polarization_str) then
//  begin
    oStrList.Add('����������� ������ ������ ���� ����������.');

   //////// ErrorDlg ('����������� ������ ������ ���� ����������.');
 //   Result:=False;
 // end;

(*
  s1:=qry_Antennas1.FieldByName(FLD_Polarization_str).AsString;
  s2:=qry_Antennas2.FieldByName(FLD_Polarization_str).AsString;

  if (qry_Antennas1.FieldByName(FLD_Polarization_str).AsString <>
      qry_Antennas2.FieldByName(FLD_Polarization_str).AsString) then
  begin
    oStrList.Add('����������� ������ ������ ���� ����������.');
  //  ErrorDlg ('����������� ������ ������ ���� ����������.');
 //   Result:=False;
  end;
*)

(*
  if Freq_GHz=0 then // begin
    oStrList.Add('������� GHz = 0');
  //  ErrorDlg ('������� GHz = 0');
 //   Result:=False;
//  end;*)

  if LinkEnd1.Power_dBm=0 then  //  begin
    oStrList.Add('Power_dBm = 0');
  //  ErrorDlg ('������� GHz = 0');
 //   Result:=False;
 // end;

  if LinkEnd2.THRESHOLD_BER_6=0 then // begin
    oStrList.Add('THRESHOLD_BER_6 = 0');
  //  ErrorDlg ('������� GHz = 0');
 //   Result:=False;
//  end;

  Result:=oStrList.Count=0;

  if not Result then
    ShowMessage(oStrList.Text);

  FreeAndNil(oStrList);

{
  if qry_LinkEnd1.FieldByName(FLD_POWER).AsFloat=0 then begin
    ErrorDlg ('�������� ����������� = 0');   Result:=False;
  end;

}
end;

constructor TDBLinkReflectionPointList.Create;
begin
  inherited Create(TDBLinkReflectionPoint);
end;

function TDBLinkReflectionPointList.AddItem: TDBLinkReflectionPoint;
begin
  Result := TDBLinkReflectionPoint (inherited Add);
end;

function TDBLinkReflectionPointList.GetItems(Index: Integer):
    TDBLinkReflectionPoint;
begin
  Result := TDBLinkReflectionPoint(inherited Items[Index]);
end;

constructor TDBLinkEnd.Create;
begin
  inherited Create;
  Antennas := TDBAntennaList.Create();
end;

destructor TDBLinkEnd.Destroy;
begin
  FreeAndNil(Antennas);
  inherited Destroy;
end;

function TDBLinkEnd.GetTx_Freq_GHz: double;
begin
  Result := TX_FREQ_MHz / 1000;
end;

procedure TDBLinkEnd.LoadFromDataset(aDataset: TDataset);
begin
  with aDataset do
  begin
    LinkEndType_ID:=FieldByName(FLD_LinkEndType_ID).AsInteger;


    THRESHOLD_BER_3   :=FieldByName(FLD_THRESHOLD_BER_3).asFloat;
    THRESHOLD_BER_6   :=FieldByName(FLD_THRESHOLD_BER_6).asFloat;

    if (THRESHOLD_BER_3<>0) and (THRESHOLD_BER_6=0) then
      THRESHOLD_BER_6:=THRESHOLD_BER_3;
      


  //  BAND              :=FieldByName(FLD_BAND).AsString;
    TX_FREQ_MHz       :=FieldByName(FLD_TX_FREQ_MHz).AsFloat;

    if TX_FREQ_MHz=0 then
      TX_FREQ_MHz:=FieldByName(FLD_RX_FREQ_MHz).AsFloat;


    Power_dBm         :=FieldByName(FLD_POWER_dBm).AsFloat;

    Kng               :=FieldByName(FLD_Kng).AsFloat;
    LOSS_dBm          :=FieldByName(FLD_LOSS).AsFloat;

    Passive_element_LOSS_dBm:=FieldByName(FLD_Passive_element_LOSS).AsFloat;

    FREQ_CHANNEL_COUNT:=FieldByName(FLD_FREQ_CHANNEL_COUNT).AsInteger;

    Freq_Spacing_MHz  :=FieldByName(FLD_Freq_Spacing).AsFloat;

    KRATNOST_BY_SPACE:=FieldByName(FLD_KRATNOST_BY_SPACE).AsInteger;
    KRATNOST_BY_FREQ :=FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger;

    SIGNATURE_WIDTH := FieldByName(FLD_SIGNATURE_WIDTH).AsFloat;//,  'double; //  '������ ���������, ���');
    SIGNATURE_HEIGHT:= FieldByName(FLD_SIGNATURE_HEIGHT).AsFloat;//, 'double; // '������ ���������, ��');
    MODULATION_COUNT:= FieldByName(FLD_MODULATION_COUNT).AsFloat;//,double;// '����� ������� ���������');
  //  EQUALISER_PROFIT1:= FieldByName('EQUALISER_PROFIT').AsFloat;//, 'double; // '�������, �������� ������������, ��');

    BitRate_Mbps  := FieldByName(FLD_BitRate_Mbps).AsInteger;

//    if Assigned(FindField(FLD_ATPC_profit_dB)) then
 //   begin
    ATPC_profit_dB:= FieldByName(FLD_ATPC_profit_dB).AsFloat;//, 'double; // '�������, �������� ������������, ��');
    Use_ATPC      := FieldByName(FLD_Use_ATPC).AsBoolean;//, 'double; // '�������, �������� ������������, ��');
    Threshold_degradation_dB:= FieldByName(FLD_Threshold_degradation_dB).AsFloat;//, 'double; // '�������, �������� ������������, ��');

    GOST_53363_modulation                      := FieldByName(FLD_GOST_53363_modulation).AsString;
    GOST_53363_RX_count_on_combined_diversity  := FieldByName(FLD_GOST_53363_RX_count_on_combined_diversity).AsInteger;

//  end;

   //   : double;// <param id="45" value="10" comment="������� �� ATPC, ��"/>
  //  : Double; //Threshold_degradation_dB,  '���������� ����������������, ��');


  end;
end;


function TDBAntenna.GetPolarizationType: TDBAntennaPolarizationType;
begin
  if Eq(Polarization_str,'h') then
    Result:=ptH__ else
  if Eq(Polarization_str,'v') then
    Result:=ptV__
  else
    Result:=ptX__;


(*  case Polarization_str of
    0: Result:=ptH;
    1: Result:=ptV;
    2: Result:=ptX;
  else
     Result:=ptH;
  end;
*)
end;


function TDBAntenna.GetPolarization1: Integer;
var
  s: string;
begin
 //TTX.POLARIZATION, '��� �����������, 0-��������������,1-������������,2-�����');
  s := LowerCase(Polarization_str);

  if s='h' then  Result := 0 else
  if s='v' then  Result := 1 else
                 Result := 2;

end;

// ---------------------------------------------------------------
procedure TDBAntenna.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
begin

  with aDataset do
  begin
//    Id                  :=FieldByName(FLD_Id).AsInteger;

//    BLPoint.B           :=FieldByName(FLD_Lat).AsFloat;
//    BLPoint.L           :=FieldByName(FLD_Lon).AsFloat;

    Polarization_str    :=LowerCase(FieldByName(FLD_Polarization_str).AsString);
    Diameter            :=FieldByName(FLD_Diameter).AsFloat;
    Height              :=FieldByName(FLD_Height).AsFloat;
    Gain                :=FieldByName(FLD_Gain).AsFloat;
    Vert_width          :=FieldByName(FLD_Vert_width).AsFloat;
    Loss             :=FieldByName(FLD_Loss).AsFloat;

//    FronttoBackratio    :=FieldByName(FLD_Fronttobackratio).AsFloat;

/////////    Band := FieldByName(FLD_BAND).AsString;
  end;
end;

constructor TDBAntennaList.Create;
begin
  inherited Create(TDBAntenna);
end;

function TDBAntennaList.AddItem: TDBAntenna;
begin
  Result := TDBAntenna (inherited Add);
end;

//-------------------------------------------------------------------
function TDBAntennaList.GetAntennaHeightArr: TDoubleArray;
//-------------------------------------------------------------------
var
  i: integer;
begin
  SetLength(Result, Count);

  for I := 0 to Count - 1 do
    Result[i]:=Items[i].Height;

end;

function TDBAntennaList.GetItems(Index: Integer): TDBAntenna;
begin
  Result := TDBAntenna(inherited Items[Index]);
end;

procedure TDBAntennaList.LoadFromDataset(aDataset: TDataset);
begin
  Clear;

  aDataset.First;
  while not aDataset.EOF do
  begin
    AddItem.LoadFromDataset(aDataset);
    aDataset.Next;
  end;
end;

constructor TDBLinkRepeater.Create;
begin
  inherited Create;
  Antennas := TDBAntennaList.Create();
end;

destructor TDBLinkRepeater.Destroy;
begin
  FreeAndNil(Antennas);
  inherited Destroy;
end;

// ---------------------------------------------------------------
procedure TDBLinkRepeater.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
begin
  with aDataset do
    if RecordCount > 0 then
  begin
    Part1.Profile_XML1:=aDataset.FieldByName(FLD_Profile_XML_1).AsString;
    Part2.Profile_XML1:=aDataset.FieldByName(FLD_Profile_XML_2).AsString;


    Property_Ground_Height:=aDataset.FieldByName(FLD_Ground_Height).AsVariant;
    Property_Pos  :=  db_ExtractBLPoint (aDataset);

    Property_id   := aDataset.FieldByName(FLD_Property_id).AsInteger;

    Assert(Property_Pos.B<>0, 'Value <=0');
    Assert(Property_id>0, 'Value <=0');


    CALC_RESULTS_XML_1:=aDataset.FieldByName(FLD_CALC_RESULTS_XML_1).AsString;
    CALC_RESULTS_XML_2:=aDataset.FieldByName(FLD_CALC_RESULTS_XML_2).AsString;
    CALC_RESULTS_XML_3:=aDataset.FieldByName(FLD_CALC_RESULTS_XML_3).AsString;
              

//
//    dmOnega_DB_data.OpenQuery(q_LInk_repeater_ant,
//      Format('SELECT * FROM %s WHERE Link_repeater_id=%d',
//           [VIEW_LINK_REPEATER_ANTENNA, DBLink.Link_repeater.ID]));
//

(*    DBLink.Link_repeater.Antenna1_Height  := q_LInk_repeater_ant.FieldByName(FLD_Height).AsFloat;

    q_LInk_repeater_ant.Next;
    if not q_LInk_repeater_ant.EOF then
      DBLink.Link_repeater.Antenna2_Height  := q_LInk_repeater_ant.FieldByName(FLD_Height).AsFloat;
*)


  end;
end;



end.


