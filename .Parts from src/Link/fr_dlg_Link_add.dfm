object frame_Link_add: Tframe_Link_add
  Left = 1472
  Top = 427
  Width = 456
  Height = 491
  Caption = 'frame_Link_add'
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -10
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 448
    Height = 121
    Caption = 'ToolBar1'
    Color = clAppWorkSpace
    ParentColor = False
    TabOrder = 0
    Visible = False
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 169
    Width = 448
    Height = 152
    Align = alTop
    Caption = #1056#1056#1057
    TabOrder = 1
    object cxGrid1: TcxGrid
      Left = 2
      Top = 15
      Width = 444
      Height = 135
      Align = alClient
      TabOrder = 0
      LookAndFeel.Kind = lfFlat
      object cxGrid1DBTableView1: TcxGridDBTableView
        PopupMenu = PopupMenu1
        NavigatorButtons.ConfirmDelete = False
        OnCustomDrawCell = cxGrid1DBTableView1CustomDrawCell
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = ds_LinkEnds
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.ImmediateEditor = False
        OptionsCustomize.ColumnFiltering = False
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Appending = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsSelection.CellSelect = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        OptionsView.Header = False
        OptionsView.Indicator = True
        object col_Name: TcxGridDBColumn
          DataBinding.FieldName = 'name'
          Width = 160
          IsCaptionAssigned = True
        end
        object col_ID: TcxGridDBColumn
          DataBinding.FieldName = 'id'
          Visible = False
        end
        object col_link_count: TcxGridDBColumn
          DataBinding.FieldName = 'link_count'
          Visible = False
        end
        object col_link_ID: TcxGridDBColumn
          DataBinding.FieldName = 'link_ID'
          Visible = False
          Width = 67
        end
        object col_LinkEndType_name: TcxGridDBColumn
          Caption = 'LinkEndType'
          DataBinding.FieldName = 'LinkEndType_name'
          Visible = False
          Width = 122
        end
        object col_LinkEndType_mode: TcxGridDBColumn
          Caption = 'mode'
          DataBinding.FieldName = 'LinkEndType_mode'
          Visible = False
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object ToolBar2: TToolBar
    Left = 0
    Top = 435
    Width = 448
    Height = 29
    Align = alBottom
    ButtonHeight = 21
    ButtonWidth = 81
    Caption = 'ToolBar2'
    ShowCaptions = True
    TabOrder = 2
    object ToolButton1: TToolButton
      Left = 0
      Top = 2
      Action = act_Add_LinkEnd
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 121
    Width = 448
    Height = 48
    Align = alTop
    Caption = #1055#1083#1086#1097#1072#1076#1082#1072
    TabOrder = 3
    DesignSize = (
      448
      48)
    object ComboBox1: TComboBox
      Left = 5
      Top = 16
      Width = 305
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      ItemHeight = 13
      TabOrder = 0
    end
    object b_Refresh: TButton
      Left = 320
      Top = 13
      Width = 68
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      TabOrder = 1
      OnClick = b_RefreshClick
    end
    object b_Select: TButton
      Left = 393
      Top = 14
      Width = 49
      Height = 24
      Action = act_Select_Property
      Anchors = [akTop, akRight]
      TabOrder = 2
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 416
    Width = 448
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object ApplicationEvents1: TApplicationEvents
    OnActionUpdate = ApplicationEvents1ActionUpdate
    Left = 36
    Top = 6
  end
  object ds_LinkEnds: TDataSource
    DataSet = qry_LinkEnds
    Left = 216
    Top = 5
  end
  object ActionList1: TActionList
    OnUpdate = ApplicationEvents1ActionUpdate
    Left = 68
    Top = 6
    object act_Add_LinkEnd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1056#1056#1057
      OnExecute = act_Add_LinkEndExecute
    end
    object act_Reload: TAction
      Caption = 'act_Reload'
      OnExecute = act_Add_LinkEndExecute
    end
    object act_Select_Property: TAction
      Caption = #1042#1099#1073#1086#1088
      OnExecute = act_Add_LinkEndExecute
    end
    object act_Open_Property: TAction
      Caption = 'act_Open_Property'
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 116
    Top = 6
    object actAddLinkEnd1: TMenuItem
      Action = act_Add_LinkEnd
    end
  end
  object qry_LinkEnds: TADOQuery
    LockType = ltReadOnly
    Parameters = <>
    Left = 216
    Top = 56
  end
  object qry_Prop: TADOQuery
    LockType = ltReadOnly
    Parameters = <>
    Left = 312
    Top = 16
  end
end
