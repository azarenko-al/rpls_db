unit u_Link_CalcRains;

interface

uses
  Math,

  u_func
  ;


type
  TLinkCalcRainDop = //packed
  record
    IsCalcRainLength:   boolean;  //������� ������� ����� ��������� �������(1-��, 0-���)

    Rain_INTENSITY:      double;   //������������� ������� (��/���)
    Polarization:     (ptH,ptV,ptX);  //����������� (0-�����., 1- ����.)
//    Polarization:   ()integer;  //����������� (0-�����., 1- ����.)
//    weakness:           double;   //���������� ���������� (��)
    rain_signal_depression_dB:   double;   //���������� ���������� (��);

    interval_length_KM: double;   //����� ��������� (��)
    Work_freq_GHz:      double;   //������� ������� (���)
    direct_line_tilt:   double;   //������ ����� ������ ���������(����)
  end;


  TLinkCalcRainDopResults = //packed
  record
  //availability
    Rain_Enabled: Boolean;     //������� ����������� � ������
    //Availability_in_rains : Boolean;

    Rain_Work_Status : (wsNotWork,wsWork);

    rain_fitness_str: string;                      //������� ����������� � ������
    weakness_on_vert_polarization: Double;         //���������� ��� ������������   ����������� (��)
    weakness_on_horz_polarization: Double;         //���������� ��� �������������� ����������� (��)
    rain_length_km: Double;                         //����� ��������� ������� �� ���������
    allowable_intens_on_vert_polarization: Double;         //���������� ������������� ��� ������������   ����������� (��)
    allowable_intens_on_horz_polarization: double; //���������� ������������� ��� �������������� ����������� (��)
  end;


  TLinkRainsCalc = class(TObject)
  public
    Input: record
      IsCalcLength:       boolean;  //������� ������� ����� ��������� �������(1-��, 0-���)

      Rain_INTENSITY:      double;   //������������� ������� (��/���)
      Polarization:     (ptH_,ptV_,ptX_);  //����������� (0-�����., 1- ����.)
  //    Polarization:   ()integer;  //����������� (0-�����., 1- ����.)
      rain_signal_depression_dB:   double;   //���������� ���������� (��)
      interval_length_KM: double;   //����� ��������� (��)
      Work_freq_GHz:      double;   //������� ������� (���)
      direct_line_tilt:   double;   //������ ����� ������ ���������(����)
    end;

    Results : record
      Rain_Enabled: Boolean;

      Rain_Work_Status : (wsNotWork1,wsWork1);
      rain_fitness_str: string;                      //������� ����������� � ������

      weakness_on_vert_polarization: double;         //���������� ��� ������������   ����������� (��)
      weakness_on_horz_polarization: double;         //���������� ��� �������������� ����������� (��)

      rain_length_km: double;                        //����� ��������� ������� �� ���������

      allowable_intens_on_vert_polarization: Double; //���������� ������������� ��� ������������   ����������� (��)
      allowable_intens_on_horz_polarization: double; //���������� ������������� ��� �������������� ����������� (��)
    end;

    procedure Execute(aIsCalcWithAdditionalRain: boolean);

  end;



  procedure Link_CalcRains1 (aRainDop: TLinkCalcRainDop;
                            aIsCalcWithAdditionalRain: boolean;
                            var aRainDopResults: TLinkCalcRainDopResults);

//=====================================================================
implementation
//=====================================================================

const
  STR_LINK_CALCRAIN_NOT_NORM = '�� ��������';//'�� � �����';
  STR_LINK_CALCRAIN_NORM     = '��������'; //'� �����';
                                

// ---------------------------------------------------------------
procedure Link_CalcRains1 (aRainDop: TLinkCalcRainDop;
                          aIsCalcWithAdditionalRain: boolean;
                          var aRainDopResults: TLinkCalcRainDopResults);
// ---------------------------------------------------------------

   //------------------------------------------------------
   function DoLosRain(Ir, Pr, R, F, aTilt, aLdop: double;
                      aIsReffD: boolean; var aIdop,
                      aRd: double): double;
   //------------------------------------------------------
   const
  
     FrM: array[0..25] of double=(1,
                                  2,        4,       6,       7,       8,
                                  10,       12,      15,      20,      25,
                                  30,       35,      40,      45,      50,
                                  60,       70,      80,      90,      100,
                                  120,      150,     200,     300,     400);
    
     BhM: array[0..25] of double=(0.0000387,
                                  0.000154, 0.00065, 0.00175, 0.00301, 0.00454,
                                  0.0101,   0.0188,  0.0367,  0.0751,  0.124,
                                  0.187,    0.263,   0.35,    0.442,   0.536,
                                  0.707,    0.851,   0.975,   1.06,    1.12,
                                  1.18,     1.31,    1.45,    1.36,    1.32);
    
     BvM: array[0..25] of double=(0.0000352,
                                  0.000138, 0.000591,0.00155, 0.00265, 0.00395,
                                  0.00887,  0.0168,  0.0335,  0.0691,  0.113,
                                  0.167,    0.233,   0.31,    0.393,   0.479,
                                  0.642,    0.784,   0.906,   0.999,   1.06,
                                  1.13,     1.27,    1.42,    1.35,    1.31);

     AhM: array[0..25] of double=(0.912,
                                  0.963,    1.121,   1.308,   1.332,   1.327,
                                  1.276,    1.217,   1.154,   1.099,   1.061,
                                  1.021,    0.979,   0.939,   0.903,   0.873,
                                  0.826,    0.793,   0.769,   0.753,   0.743,
                                  0.731,    0.71,    0.689,   0.688,   0.683);

     AvM: array[0..25] of double=(0.88,
                                  0.923,    1.075,   1.265,   1.312,   1.31,
                                  1.264,    1.2,     1.128,   1.065,   1.03,
                                  1.0,      0.963,   0.929,   0.897,   0.868,
                                  0.824,    0.793,   0.769,   0.754,   0.744,
                                  0.732,    0.711,   0.69,    0.689,   0.684);

   var
      i: integer;
      Koef_a,Koef_b,GammaD,ReffD,Koef_k,Koef_k100,
      Imin, Imax, Ivar, Eps,
      Wmin, Wmax, Wvar, Wdop,
      Upolar,
      Koef_Av,Koef_Ah,Koef_Bv,Koef_Bh: double;
   begin
    Result:=0; aIdop:=0; aRd:=0;

    if Ir<0 then exit;
    if R<=0 then exit;

    if aLdop>=0 then exit;    //aRainDop.weakness;  //���������� ���������� (��)
    if (F<1) or (F>400) then exit;

    Koef_Av:=AvM[0];
    Koef_Ah:=AhM[0];
    Koef_Bv:=BvM[0];
    Koef_Bh:=BhM[0];

    for i:=1 to 25 do
     if (F>FrM[i-1]) and (F<=FrM[i]) then
     begin
       Koef_Av:=AvM[i-1]+(F-FrM[i-1])*(AvM[i]-AvM[i-1])/(FrM[i]-FrM[i-1]);
       Koef_Ah:=AhM[i-1]+(F-FrM[i-1])*(AhM[i]-AhM[i-1])/(FrM[i]-FrM[i-1]);
       Koef_Bv:=BvM[i-1]+(F-FrM[i-1])*(BvM[i]-BvM[i-1])/(FrM[i]-FrM[i-1]);
       Koef_Bh:=BhM[i-1]+(F-FrM[i-1])*(BhM[i]-BhM[i-1])/(FrM[i]-FrM[i-1]);
       break;
     end;

    if Pr=0 then Upolar:=0        //�������������� �����������
      else
    if Pr=1 then Upolar:=90  //������������ �����������
            else Upolar:=45;        //��������� �����������

    Koef_b:=0.5*(Koef_Bh+Koef_Bv+(Koef_Bh-Koef_Bv)*sqr(cos(aTilt*Pi/180))*cos(2*Upolar*Pi/180));
    Koef_a:=0.5*(Koef_Bh*Koef_Ah+Koef_Bv*Koef_Av+(Koef_Bh*Koef_Ah-Koef_Bv*Koef_Av)*sqr(cos(aTilt*Pi/180))*cos(2*Upolar*Pi/180))/Koef_b;

    GammaD:=Koef_b*Power(Ir,Koef_a);
    if Ir<100 then ReffD:=35*exp(-0.015*Ir)
              else ReffD:=35*exp(-1.5);

    //������� ������� ����� ��������� �������(1-��, 0-���)
    if not aIsReffD then Koef_k:=1
                    else Koef_k:=1/(1+R/ReffD);
  
    aRd:=Koef_k*R;     //����� ��������� ������� �� ���������

    Result:=-GammaD*Koef_k*R;
  
    if not aIsReffD then Koef_k100:=1
  //  if IsReffD=0 then Koef_k100:=1
                 else Koef_k100:=1/(1+R/(35*exp(-1.5)));
  
    aIdop:=exp(ln(-aLdop/(Koef_b*Koef_k100*R))/Koef_a);

  //  if (aIdop>=100) or (IsReffD=0) then exit;
    if (aIdop>=100) or (not aIsReffD) then
      exit;

  //������ ������� ��� Ir<100 � ��������� �� Ir ����� ��������� ������� (��� IsReffD<>0)

    Eps:=0.01;  // �������� ����� ���������� � dB
    Imin:=0.;
    Wmin:=0.;
    Imax:=aIdop;
    Wmax:=Koef_b*Power(Imax,Koef_a)*R/(1+R/(35*exp(-0.015*Imax))); //����� ������������ �������� ����� ����������
    Ivar:=Imax;
    Wvar:=Wmax;
    Wdop:=-aLdop;

    while abs(Wdop-Wvar)>Eps do
    begin
      if (Wdop<Wvar) then Imax:=Ivar
                     else Imin:=Ivar;
      Ivar:=0.5*(Imin+Imax);
      Wvar:=Koef_b*Power(Ivar,Koef_a)*R/(1+R/(35*exp(-0.015*Ivar)));
    end;

    aIdop:=Ivar;
   end;
    //-------------------------------------------


var
   iPr : Integer;  //polarization
   Ir,L,R,F,tilt,Lv,Lg,Iv,Ig,Rd: double; //Pr,
   bIsReffD: boolean;
   Ks: string;

begin
  // ������ ����� �������� ������

   Ir:= aRainDop.rain_INTENSITY;     //������������� ������� (��/���)

   iPr:=IIF(aRainDop.polarization=ptH, 0,1);

//   Pr:=      aRainDop.polarization;     //����������� (0-�����., 1- ����.)

   L :=      aRainDop.rain_signal_depression_dB; //���������� ���������� (��)
   R :=      aRainDop.interval_length_KM;  //����� ��������� (��)
   F :=      aRainDop.work_freq_GHz;        //������� ������� (���)
   tilt :=   aRainDop.direct_line_tilt;   //������ ����� ������ ���������(����)
   bIsReffD:=aRainDop.IsCalcRainLength;     //������� ������� ����� ��������� �������(1-��, 0-���)

  // ����� ����� �������� ������

//   if tilt<0 then tilt:=-tilt; //������ ������������� ���� �������
   tilt:=Abs(tilt); //������ ������������� ���� �������

   Lv:=0; //���������� ��� ������������   ����������� (��)
   Lg:=0; //���������� ��� �������������� ����������� (��)
   Iv:=0; //���������� ������������� ��� ������������   ����������� (��)
   Ig:=0; //���������� ������������� ��� �������������� ����������� (��)

   Lg:=DoLosRain(Ir,0,R,F,tilt,L,bIsReffD,Ig,Rd);
   Lv:=DoLosRain(Ir,1,R,F,tilt,L,bIsReffD,Iv,Rd);

//  STR_LINK_CALCRAIN_NOT_NORM = '�� �����';//'�� � �����';
//  STR_LINK_CALCRAIN_NORM     = '�����'; //'� �����';
  
   Ks:=STR_LINK_CALCRAIN_NOT_NORM; //�������� ������� ����������� ��������� � �������� �������

   aRainDopResults.Rain_Work_Status :=wsNotWork;

   //L : ���������� ���������� (��)
   //iPr : Integer;  //polarization

   if (iPr=0) and (Lg>L) then Ks:=STR_LINK_CALCRAIN_NORM;
   if (iPr=1) and (Lv>L) then Ks:=STR_LINK_CALCRAIN_NORM;

   if ((iPr=0) and (Lg>L)) or
      ((iPr=1) and (Lv>L))
   then
     aRainDopResults.Rain_Work_Status :=wsWork;

   //������� ����������� � ������
   aRainDopResults.Rain_Enabled:=((iPr=0) and (Lg>L)) or
                                 ((iPr=1) and (Lv>L));

  //������ ����� ����������� �������

   //������� ����������� � ������
   aRainDopResults.rain_fitness_str:=Ks;


//   Lv:=0; //���������� ��� ������������   ����������� (��)
//   Lg:=0; //���������� ��� �������������� ����������� (��)


   //���������� ��� ������������   ����������� (��)
   aRainDopResults.weakness_on_vert_polarization:=TruncFloat(Lv,2);

   //���������� ��� �������������� ����������� (��)
   aRainDopResults.weakness_on_horz_polarization:=TruncFloat(Lg,2);

   //����� ��������� ������� �� ���������
   aRainDopResults.rain_length_km:=TruncFloat(Rd, 3);

   //���������� ������������� ��� ������������   ����������� (��)
   aRainDopResults.allowable_intens_on_vert_polarization:=TruncFloat(Iv,2);

   //���������� ������������� ��� �������������� ����������� (��)
   aRainDopResults.allowable_intens_on_horz_polarization:=TruncFloat(Ig,2);

  //����� ����� ����������� �������
end;

procedure TLinkRainsCalc.Execute(aIsCalcWithAdditionalRain: boolean);


   //------------------------------------------------------
   function DoLosRain(Ir, Pr, R, F, aTilt, aLdop: double;
                      aIsReffD: boolean; var aIdop,
                      aRd: double): double;
   //------------------------------------------------------
   const
  
     FrM: array[0..25] of double=(1,
                                  2,        4,       6,       7,       8,
                                  10,       12,      15,      20,      25,
                                  30,       35,      40,      45,      50,
                                  60,       70,      80,      90,      100,
                                  120,      150,     200,     300,     400);
    
     BhM: array[0..25] of double=(0.0000387,
                                  0.000154, 0.00065, 0.00175, 0.00301, 0.00454,
                                  0.0101,   0.0188,  0.0367,  0.0751,  0.124,
                                  0.187,    0.263,   0.35,    0.442,   0.536,
                                  0.707,    0.851,   0.975,   1.06,    1.12,
                                  1.18,     1.31,    1.45,    1.36,    1.32);
    
     BvM: array[0..25] of double=(0.0000352,
                                  0.000138, 0.000591,0.00155, 0.00265, 0.00395,
                                  0.00887,  0.0168,  0.0335,  0.0691,  0.113,
                                  0.167,    0.233,   0.31,    0.393,   0.479,
                                  0.642,    0.784,   0.906,   0.999,   1.06,
                                  1.13,     1.27,    1.42,    1.35,    1.31);

     AhM: array[0..25] of double=(0.912,
                                  0.963,    1.121,   1.308,   1.332,   1.327,
                                  1.276,    1.217,   1.154,   1.099,   1.061,
                                  1.021,    0.979,   0.939,   0.903,   0.873,
                                  0.826,    0.793,   0.769,   0.753,   0.743,
                                  0.731,    0.71,    0.689,   0.688,   0.683);

     AvM: array[0..25] of double=(0.88,
                                  0.923,    1.075,   1.265,   1.312,   1.31,
                                  1.264,    1.2,     1.128,   1.065,   1.03,
                                  1.0,      0.963,   0.929,   0.897,   0.868,
                                  0.824,    0.793,   0.769,   0.754,   0.744,
                                  0.732,    0.711,   0.69,    0.689,   0.684);

   var
      i: integer;
      Koef_a,Koef_b,GammaD,ReffD,Koef_k,Koef_k100,
      Imin, Imax, Ivar, Eps,
      Wmin, Wmax, Wvar, Wdop,
      Upolar,
      Koef_Av,Koef_Ah,Koef_Bv,Koef_Bh: double;
   begin
    Result:=0; aIdop:=0; aRd:=0;

    if Ir<0 then exit;
    if R<=0 then exit;
    if aLdop>=0 then exit;    //aRainDop.weakness;  //���������� ���������� (��)
    if (F<1) or (F>400) then exit;

    Koef_Av:=AvM[0];
    Koef_Ah:=AhM[0];
    Koef_Bv:=BvM[0];
    Koef_Bh:=BhM[0];

    for i:=1 to 25 do
     if (F>FrM[i-1]) and (F<=FrM[i]) then
     begin
       Koef_Av:=AvM[i-1]+(F-FrM[i-1])*(AvM[i]-AvM[i-1])/(FrM[i]-FrM[i-1]);
       Koef_Ah:=AhM[i-1]+(F-FrM[i-1])*(AhM[i]-AhM[i-1])/(FrM[i]-FrM[i-1]);
       Koef_Bv:=BvM[i-1]+(F-FrM[i-1])*(BvM[i]-BvM[i-1])/(FrM[i]-FrM[i-1]);
       Koef_Bh:=BhM[i-1]+(F-FrM[i-1])*(BhM[i]-BhM[i-1])/(FrM[i]-FrM[i-1]);
       break;
     end;

    if Pr=0 then Upolar:=0        //�������������� �����������
      else
    if Pr=1 then Upolar:=90  //������������ �����������
            else Upolar:=45;        //��������� �����������

    Koef_b:=0.5*(Koef_Bh+Koef_Bv+(Koef_Bh-Koef_Bv)*sqr(cos(aTilt*Pi/180))*cos(2*Upolar*Pi/180));
    Koef_a:=0.5*(Koef_Bh*Koef_Ah+Koef_Bv*Koef_Av+(Koef_Bh*Koef_Ah-Koef_Bv*Koef_Av)*sqr(cos(aTilt*Pi/180))*cos(2*Upolar*Pi/180))/Koef_b;

    GammaD:=Koef_b*Power(Ir,Koef_a);
    if Ir<100 then ReffD:=35*exp(-0.015*Ir)
              else ReffD:=35*exp(-1.5);

    //������� ������� ����� ��������� �������(1-��, 0-���)
    if not aIsReffD then Koef_k:=1
                    else Koef_k:=1/(1+R/ReffD);

    aRd:=Koef_k*R;     //����� ��������� ������� �� ���������

    Result:=-GammaD*Koef_k*R;
  
    if not aIsReffD then Koef_k100:=1
  //  if IsReffD=0 then Koef_k100:=1
                 else Koef_k100:=1/(1+R/(35*exp(-1.5)));
  
    aIdop:=exp(ln(-aLdop/(Koef_b*Koef_k100*R))/Koef_a);

  //  if (aIdop>=100) or (IsReffD=0) then exit;
    if (aIdop>=100) or (not aIsReffD) then
      exit;

  //������ ������� ��� Ir<100 � ��������� �� Ir ����� ��������� ������� (��� IsReffD<>0)

    Eps:=0.01;  // �������� ����� ���������� � dB
    Imin:=0.;
    Wmin:=0.;
    Imax:=aIdop;
    Wmax:=Koef_b*Power(Imax,Koef_a)*R/(1+R/(35*exp(-0.015*Imax))); //����� ������������ �������� ����� ����������
    Ivar:=Imax;
    Wvar:=Wmax;
    Wdop:=-aLdop;

    while abs(Wdop-Wvar)>Eps do
    begin
      if (Wdop<Wvar) then Imax:=Ivar
                     else Imin:=Ivar;
      Ivar:=0.5*(Imin+Imax);
      Wvar:=Koef_b*Power(Ivar,Koef_a)*R/(1+R/(35*exp(-0.015*Ivar)));
    end;

    aIdop:=Ivar;
   end;
    //-------------------------------------------


var
   iPr : Integer;
   Ir,L,R,F,tilt,Lv,Lg,Iv,Ig,Rd: double;
   bIsReffD: boolean;
   Ks: string;

begin
  // ������ ����� �������� ������

   Ir:= Input.Rain_INTENSITY;     //������������� ������� (��/���)

   iPr:=IIF(Input.polarization=ptH_, 0,1);

//   Pr:=      aRainDop.polarization;     //����������� (0-�����., 1- ����.)
   L :=      Input.rain_signal_depression_dB;  //���������� ���������� (��)
   R :=      Input.interval_length_KM;  //����� ��������� (��)
   F :=      Input.work_freq_GHz;        //������� ������� (���)
   tilt :=   Input.direct_line_tilt;   //������ ����� ������ ���������(����)
   bIsReffD:=Input.IsCalcLength;     //������� ������� ����� ��������� �������(1-��, 0-���)

  // ����� ����� �������� ������

//   if tilt<0 then tilt:=-tilt; //������ ������������� ���� �������
   tilt:=Abs(tilt); //������ ������������� ���� �������

   Lv:=0; //���������� ��� ������������   ����������� (��)
   Lg:=0; //���������� ��� �������������� ����������� (��)
   Iv:=0; //���������� ������������� ��� ������������   ����������� (��)
   Ig:=0; //���������� ������������� ��� �������������� ����������� (��)

   Lg:=DoLosRain(Ir,0,R,F,tilt,L,bIsReffD,Ig,Rd);
   Lv:=DoLosRain(Ir,1,R,F,tilt,L,bIsReffD,Iv,Rd);

   Ks:=STR_LINK_CALCRAIN_NOT_NORM; //�������� ������� ����������� ��������� � �������� �������

   //L : ���������� ���������� (��)
   //iPr : Integer;  //polarization

   if (iPr=0) and (Lg>L) then Ks:=STR_LINK_CALCRAIN_NORM;
   if (iPr=1) and (Lv>L) then Ks:=STR_LINK_CALCRAIN_NORM;

   //������� ����������� � ������
   Results.Rain_Enabled:=((iPr=0) and (Lg>L)) or
                         ((iPr=1) and (Lv>L));

  //������ ����� ����������� �������

   //������� ����������� � ������
   Results.rain_fitness_str:=Ks;

   //���������� ��� ������������   ����������� (��)
   Results.weakness_on_vert_polarization:=TruncFloat(Lv,2);
  
   //���������� ��� �������������� ����������� (��)
   Results.weakness_on_horz_polarization:=TruncFloat(Lg,2);

   //����� ��������� ������� �� ���������
   Results.rain_length_km:=TruncFloat(Rd, 3);

   //���������� ������������� ��� ������������   ����������� (��)
   Results.allowable_intens_on_vert_polarization:=TruncFloat(Iv,2);

   //���������� ������������� ��� �������������� ����������� (��)
   Results.allowable_intens_on_horz_polarization:=TruncFloat(Ig,2);

  //����� ����� ����������� �������
end;


begin 
end.


