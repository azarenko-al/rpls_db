inherited dmAct_Link: TdmAct_Link
  OldCreateOrder = True
  Left = 2098
  Top = 756
  Height = 226
  Width = 420
  inherited ActionList1: TActionList
    Left = 32
    Top = 16
    object act_Add_LinkLine: TAction
      Category = 'Link'
      Caption = 'act_Add_LinkLine'
    end
    object act_Change_LinkEndType: TAction
      Category = 'Link'
      Caption = 'act_Change_LinkEndType'
    end
    object act_Del_with_LinkEnds: TAction
      Caption = 'act_Del_with_LinkEnds'
    end
    object act_UpdateAntennaAzimuth: TAction
      Category = 'Link'
      Caption = 'Set Antenna Azimuth'
    end
    object act_Report: TAction
      Category = 'Report'
      Caption = 'Report'
    end
    object act_Calc: TAction
      Category = 'Link'
      Caption = 'act_Calc'
    end
    object act_Update_Climate: TAction
      Category = 'Link'
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1087#1072#1088#1072#1084#1077#1090#1088#1099' '#1082#1083#1080#1084#1072#1090#1072
    end
    object act_Del_with_Property: TAction
      Caption = 'act_Del_with_Property'
    end
    object act_Create_Net: TAction
      Caption = 'act_Create_Net'
    end
    object Action2: TAction
      Caption = 'Action2'
    end
    object act_Update_Name_by_linkends: TAction
      Category = 'Link'
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1085#1072#1079#1074#1072#1085#1080#1077' '#1087#1086' '#1087#1083#1086#1097#1072#1076#1082#1072#1084
    end
    object act_Object_dependence: TAction
      Category = 'Link'
      Caption = 'act_Object_dependence'
    end
    object act_Export_RPLS_XML: TAction
      Category = 'Link_export'
      Caption = 'act_Export_RPLS_XML'
    end
    object act_Export_Google: TAction
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' GOOGLE'
    end
    object act_Export_RPLS_XML_folder: TAction
      Category = 'Link'
      Caption = 'act_Export_RPLS_XML_folder'
    end
    object act_Eprasys: TAction
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Eprasys'
    end
    object Action1: TAction
      Category = 'Link'
      Caption = 'Action1'
    end
    object act_Export_ACAD: TAction
      Category = 'Link_export'
      Caption = 'act_Export_ACAD'
    end
    object act_Link_profile_Save_XML: TAction
      Category = 'Link_export'
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1087#1088#1086#1092#1080#1083#1100' '#1074' XML '#1092#1072#1081#1083
    end
    object act_Link_profile_Load_XML: TAction
      Category = 'Link_export'
      Caption = 'act_Link_profile_Load_XML'
    end
    object act_Calc_Adaptive: TAction
      Category = 'Link'
      Caption = 'act_Calc_Adaptive'
    end
    object act_Link_profile_Save_to_CSV: TAction
      Category = 'Link_export'
      Caption = 'act_Link_profile_Save_to_CSV'
    end
    object act_Link_profile_Load_from_CSV: TAction
      Category = 'Link_export'
      Caption = 'act_Link_profile_Load_from_CSV'
    end
    object act_AntennaType_change: TAction
      Category = 'Link'
      Caption = 'act_AntennaType_change'
    end
    object act_Update_profile: TAction
      Caption = 'act_Update_profile'
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = 'XML (*.xml)|*.xml'
    Left = 136
    Top = 16
  end
  object SaveDialog1: TSaveDialog
    Filter = 'XML (*.xml)|*.xml'
    Left = 136
    Top = 80
  end
  object ActionList2: TActionList
    Left = 256
    Top = 16
    object act_Export_MDB: TAction
      Caption = 'act_Export_MDB'
    end
    object act_Copy: TAction
      Caption = 'Copy'
    end
    object act_Add_Repeater: TAction
      Caption = 'act_Add_Repeater'
    end
    object act_Move_By_GeoRegions: TAction
      Caption = 'act_Move_By_GeoRegions'
    end
    object act_Report_FreqPlan: TAction
      Caption = 'act_Report_FreqPlan'
    end
    object act_Repeater: TAction
      Caption = 'act_Repeater'
    end
    object act_Audit: TAction
      Caption = 'act_Audit'
    end
  end
end
