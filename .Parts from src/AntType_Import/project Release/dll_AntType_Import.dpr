program dll_AntType_Import;

uses
  Forms,
  Windows,
  d_AntType_import in '..\src\d_AntType_import.pas' {dlg_AntennaType_import},
  dm_AntType_Import in '..\src\dm_AntType_Import.pas' {dmAntType_Import: TDataModule},
  dm_Main_app in '..\src\dm_Main_app.pas' {dmMain_app: TDataModule},
  u_ant_ADF in '..\src\u_ant_ADF.pas',
  u_ant_MSI in '..\src\u_ant_MSI.pas',
  u_antenna_custom in '..\src\u_antenna_custom.pas',
  u_ant_DAT in '..\src\u_ant_DAT.pas',
  dm_MDB in '..\src\dm_MDB.pas' {dmMDB: TDataModule},
  f_AntType_Preview in '..\src\f_AntType_Preview.pas' {frm_AntType_Preview},
  u_const_db in '..\..\u_const_db.pas',
  dm_Main_Data in '..\..\dm_Main_Data.pas' {dmMain_Data: TDataModule},
  dm_AntType_add in 'W:\RPLS_DB XE\src\AntennaType\dm_AntType_add.pas' {dmAntType_add: TDataModule};

{$R *.RES}

begin
  SetThreadLocale(1049);


  Application.Initialize;
  Application.CreateForm(TdmMain_app, dmMain_app);
  Application.Run;
end.
