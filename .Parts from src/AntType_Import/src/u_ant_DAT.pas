unit u_ant_DAT;

interface
uses Classes,SysUtils,

  u_antenna_custom,

    u_Func_arrays,
  u_func;

type
  //--------------------------------------------------------------------
  TPathLossFile = class(TCustomAntennaFile)
  //--------------------------------------------------------------------
  public
    H: TAntennaDiagram;
    V: TAntennaDiagram;

    constructor Create;
    destructor Destroy; override;

    function LoadFromFile(aFileName: string): boolean; override;
  end;


//==========================================================
implementation

constructor TPathLossFile.Create;
begin
  inherited Create;
  H := TAntennaDiagram.Create();
  V := TAntennaDiagram.Create();
end;


destructor TPathLossFile.Destroy;
begin
  FreeAndNil(V);
  FreeAndNil(H);
  inherited Destroy;
end;

//---------------------------------------------------------
// Load from ADF file
//---------------------------------------------------------
function TPathLossFile.LoadFromFile(aFileName: string): boolean;
//---------------------------------------------------------
var strList: TStringList;
    strArr: TStrArray;
    str,s1,s2,str3, sFileName: string;
    dAngle,eLoss: double;
    iAngle,i,total: integer;
    k,m,n,p: integer;

    state: (stHEADER,HH,VH,HV,VV);

    oAntennaMask: TAntennaMask;

    iLen: integer;
    s: string;
  eAngle: Double;
begin
  H.Clear;
  V.Clear;


  Result:=false;

  if not FileExists(aFileName) then Exit;

  strList:=TStringList.Create;
  Result:=false;

  try
    strList.LoadFromFile (AFileName);
  except
  end;

  state:=stHEADER;

//  k:=0; m:=0; n:=0; p:=0;

  if strList.Count<10 then
    Exit;


  i := 0;
  while i<strList.Count do
  begin
    s:= strList[i];
    I := I+1;

    strArr:=StringToStrArray (s,' ');

    iLen:=High(strArr)+1;
    s1:='';
    s2:='';

    if iLen>=1 then s1:=strArr[0];
    if iLen>=2 then s2:=strArr[1];


    case i of
      0: ; //RFS
      1: ModelName :=s1; //SB4-127A
      2: ; //4 ; Compactline Single Polarisation
      3: ; //NONE
      4: ; //NONE
      5: ; //20020701
      6: ; // FRT 020705
      7: ; // 12700 - 13250 MHZ
      8: ; // 41.5 dBi
      9: ; // 1.4 Deg

(*    else
      begin
        if Eq(s1, 'HH') then

      end;


*)
    end;


    if Eq(s1,'VH') then state:=VH  else
    if Eq(s1,'VV') then state:=VV  else
    if Eq(s1,'HH') then state:=HH  else
    if Eq(s1,'HV') then state:=HV
    else

    if state in [HH,HV,VH,VV] then
    begin
      eAngle:=AsFloat(s1);
      eLoss :=AsFloat(s2);

      case state of
        VH: V.Horz.AddItem(eAngle, eLoss);
        VV: V.Vert.AddItem(eAngle, eLoss);
        HH: H.Horz.AddItem(eAngle, eLoss);
        HV: H.Vert.AddItem(eAngle, eLoss);
      end;
    end;
    
  end;

{HH 107
-180.0  -67.00
 -95.0  -67.00
 -80.0  -60.00
 -66.5  -48.25
 -47.5  -42.90
 -30.0  -42.00
 -20.5  -41.00
}


  strList.Free;
end;


end.

