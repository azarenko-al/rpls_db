unit f_AntType_Preview;

interface

uses
  Classes, Controls, Forms, cxGridLevel,  cxGridDBTableView, cxGrid, rxPlacemnt, StdCtrls,
  cxGridCustomTableView, cxGridTableView, cxClasses, cxControls,

  u_cx,
  u_const_db,
  u_func,

  dm_MDB,

  cxGridCustomView, ExtCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  Data.DB, cxDBData
  ;

type
  Tfrm_AntType_Preview = class(TForm)
    cxGrid_AntennaTypes: TcxGrid;
    cxGridDBTableView_AntennaTypes: TcxGridDBTableView;
    cxGridDBTableView_AntennaTypesID: TcxGridDBColumn;
    cxGridDBTableView_AntennaTypesname: TcxGridDBColumn;
    cxGridDBTableView_AntennaTypesgain_dB: TcxGridDBColumn;
    cxGridDBTableView_AntennaTypesfreq_MHz: TcxGridDBColumn;
    cxGridDBTableView_AntennaTypesband: TcxGridDBColumn;
    cxGridDBTableView_AntennaTypesvert_mask: TcxGridDBColumn;
    cxGridDBTableView_AntennaTypeshorz_mask: TcxGridDBColumn;
    cxGridLevel6: TcxGridLevel;
    FormStorage1: TFormStorage;
    cxGridDBTableView_AntennaTypesDBColumn1_checked: TcxGridDBColumn;
    cxGridDBTableView_AntennaTypesDBColumn1_FileName: TcxGridDBColumn;
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;
    col_Diameter: TcxGridDBColumn;
    col_vert_width: TcxGridDBColumn;
    col_horz_width: TcxGridDBColumn;
    col_Polarization: TcxGridDBColumn;
    col_TiltType: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
  private

  public
    class function ExecDlg: boolean;

  end;

implementation
{$R *.dfm}

// ---------------------------------------------------------------
class function Tfrm_AntType_Preview.ExecDlg: boolean;
// ---------------------------------------------------------------
begin
  with Tfrm_AntType_Preview.Create(Application) do
  begin
    Result := ShowModal=mrOk;

    Free;
  end;
end;


// ---------------------------------------------------------------
procedure Tfrm_AntType_Preview.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  Caption :='������';

//  db_ViewDataSet(dmMDB_ant.tbl_AntennaType);

 cxGrid_AntennaTypes.Align:=alClient;


  cxGridDBTableView_AntennaTypes.dataController.DataSource :=  dmMDB_ant.ds_AntennaType;



  cx_SetColumnCaptions1(cxGridDBTableView_AntennaTypes,
     [
      FLD_NAME, '��������',

      FLD_Freq_MHz,   'Freq_MHz',
      FLD_Gain_dB,    'Gain_dB',

      FLD_horz_mask,  'horz_mask',
      FLD_vert_mask,  'vert_mask',

      FLD_vert_width, 'vert_width',
      FLD_horz_width, 'horz_width'

    ]);

end;

end.


(*

//----------------------------------------------------------------------------
procedure db_SetFieldCaptions(aDataset: TDataset; aFieldNames : array of TStrArray);
//----------------------------------------------------------------------------
var I: Integer;
begin
  Assert(Assigned(aDataset), 'aDataset not assigned');
//  Assert(Length(aFieldNames) = Length(aCaptions));

  for I := 0 to High(aFieldNames) do
  begin
    Assert(Length(aFieldNames[i])=2);

    db_SetFieldCaption (aDataset, aFieldNames[i][0], aFieldNames[i][1]);
  end;
end;

*)
