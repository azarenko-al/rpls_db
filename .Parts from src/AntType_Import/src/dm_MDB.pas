unit dm_MDB;

interface

{$DEFINE CompactDatabase}
  

uses
  Classes, DB, ADODB,  Forms,  Dialogs, SysUtils,

  u_func,

  u_const_db,

  u_db_mdb,
  u_db
  ;

type
  TdmMDB_ant = class(TDataModule)
    ADOConnection: TADOConnection;
    tbl_AntennaType: TADOTable;
    ds_AntennaType: TDataSource;
    procedure DataModuleCreate(Sender: TObject);
  private
  public
    class procedure Init;
    procedure Open;

  end;

var
  dmMDB_ant: TdmMDB_ant;

implementation
{$R *.dfm}

const
   DEF_AntennaType = 'AntennaType';


// ---------------------------------------------------------------
class procedure TdmMDB_ant.Init;
// ---------------------------------------------------------------
begin
  if not Assigned(dmMDB_ant) then
    dmMDB_ant := TdmMDB_ant.Create(Application);
end;

// ---------------------------------------------------------------
procedure TdmMDB_ant.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
var
  sMdbFileName: string;

begin
 if ADOConnection.Connected then
    ShowMessage('TdmMDB.ADOConnection.Connected');

  sMdbFileName := ChangeFileExt(Application.ExeName, '.mdb');

  {$IFDEF CompactDatabase}
  mdb_CompactDatabase(sMdbFileName);
  {$ENDIF}


  if not mdb_OpenConnection (ADOConnection, sMdbFileName) then
    Halt;

end;


// ---------------------------------------------------------------
procedure TdmMDB_ant.Open;
// ---------------------------------------------------------------
begin

  ADOConnection.Execute('DELETE FROM ' + DEF_AntennaType);

//  db_TableDeleteRecords(ADOTable1);
  db_TableOpen1(tbl_AntennaType);

end;



end.