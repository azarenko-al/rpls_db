object frm_AntType_Preview: Tfrm_AntType_Preview
  Left = 973
  Top = 166
  BorderWidth = 5
  Caption = 'frm_AntType_Preview'
  ClientHeight = 379
  ClientWidth = 870
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid_AntennaTypes: TcxGrid
    Left = 0
    Top = 0
    Width = 870
    Height = 169
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    ExplicitWidth = 915
    object cxGridDBTableView_AntennaTypes: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.CellHints = True
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsView.FooterAutoHeight = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGridDBTableView_AntennaTypesDBColumn1_checked: TcxGridDBColumn
        DataBinding.FieldName = 'checked'
        Visible = False
        Width = 59
      end
      object cxGridDBTableView_AntennaTypesID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Visible = False
        Width = 56
      end
      object cxGridDBTableView_AntennaTypesname: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        SortIndex = 0
        SortOrder = soAscending
        Width = 143
      end
      object cxGridDBTableView_AntennaTypesgain_dB: TcxGridDBColumn
        DataBinding.FieldName = 'gain_dB'
        Width = 76
      end
      object cxGridDBTableView_AntennaTypesfreq_MHz: TcxGridDBColumn
        DataBinding.FieldName = 'freq_MHz'
        Width = 73
      end
      object cxGridDBTableView_AntennaTypesband: TcxGridDBColumn
        DataBinding.FieldName = 'band'
        Width = 61
      end
      object col_Diameter: TcxGridDBColumn
        DataBinding.FieldName = 'Diameter'
      end
      object cxGridDBTableView_AntennaTypesDBColumn1_FileName: TcxGridDBColumn
        DataBinding.FieldName = 'FileName'
        Width = 97
      end
      object col_vert_width: TcxGridDBColumn
        DataBinding.FieldName = 'vert_width'
      end
      object col_horz_width: TcxGridDBColumn
        DataBinding.FieldName = 'horz_width'
      end
      object col_Polarization: TcxGridDBColumn
        DataBinding.FieldName = 'Polarization_str'
      end
      object cxGridDBTableView_AntennaTypesvert_mask: TcxGridDBColumn
        DataBinding.FieldName = 'vert_mask'
        Width = 94
      end
      object cxGridDBTableView_AntennaTypeshorz_mask: TcxGridDBColumn
        DataBinding.FieldName = 'horz_mask'
        Width = 71
      end
      object col_TiltType: TcxGridDBColumn
        DataBinding.FieldName = 'Tilt_Type'
        Visible = False
      end
    end
    object cxGridLevel6: TcxGridLevel
      Caption = 'Site cells'
      GridView = cxGridDBTableView_AntennaTypes
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 351
    Width = 870
    Height = 28
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 915
    object Panel2: TPanel
      Left = 651
      Top = 0
      Width = 219
      Height = 28
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 696
      object Button1: TButton
        Left = 111
        Top = 4
        Width = 97
        Height = 23
        Cancel = True
        Caption = #1047#1072#1082#1088#1099#1090#1100
        ModalResult = 2
        TabOrder = 0
      end
      object Button2: TButton
        Left = 7
        Top = 4
        Width = 97
        Height = 23
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' DB'
        Default = True
        ModalResult = 1
        TabOrder = 1
        Visible = False
      end
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    Options = []
    StoredValues = <>
    Left = 36
    Top = 39
  end
end
