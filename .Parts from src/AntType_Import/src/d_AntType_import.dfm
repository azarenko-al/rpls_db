inherited dlg_AntennaType_import: Tdlg_AntennaType_import
  Left = 1583
  Top = 766
  Caption = 'dlg_AntennaType_import'
  ClientHeight = 413
  ClientWidth = 586
  OldCreateOrder = True
  Position = poDefault
  OnDestroy = FormDestroy
  ExplicitLeft = 1583
  ExplicitTop = 766
  ExplicitWidth = 594
  ExplicitHeight = 441
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 378
    Width = 586
    ExplicitTop = 379
    ExplicitWidth = 586
    inherited Bevel1: TBevel
      Width = 586
      ExplicitWidth = 586
    end
    inherited Panel3: TPanel
      Left = 397
      Width = 189
      ExplicitLeft = 397
      ExplicitWidth = 189
      inherited btn_Ok: TButton
        Left = 11
        Default = False
        ModalResult = 0
        ExplicitLeft = 11
      end
      inherited btn_Cancel: TButton
        Left = 95
        ExplicitLeft = 95
      end
    end
    object Button2: TButton
      Left = 12
      Top = 8
      Width = 145
      Height = 22
      Caption = 'sp_Dictionary_Clear'
      TabOrder = 1
      Visible = False
      OnClick = Button2Click
    end
    object Button1: TButton
      Left = 184
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 2
      Visible = False
      OnClick = Button1Click
    end
  end
  inherited pn_Top_: TPanel
    Width = 586
    ExplicitWidth = 586
    inherited Bevel2: TBevel
      Width = 586
      ExplicitWidth = 586
    end
    inherited pn_Header: TPanel
      Width = 586
      Visible = False
      ExplicitWidth = 586
    end
  end
  object Panel4: TPanel [2]
    Left = 0
    Top = 60
    Width = 586
    Height = 61
    Align = alTop
    BevelOuter = bvNone
    BorderWidth = 2
    TabOrder = 2
    object lb_Name: TLabel
      Left = 4
      Top = 8
      Width = 32
      Height = 13
      Caption = #1055#1072#1087#1082#1072
    end
    object ed_Folder: TEdit
      Left = 5
      Top = 24
      Width = 420
      Height = 21
      ParentColor = True
      ReadOnly = True
      TabOrder = 0
    end
  end
  object StatusBar1: TStatusBar [3]
    Left = 0
    Top = 359
    Width = 586
    Height = 19
    Panels = <>
    SimplePanel = True
    OnDblClick = StatusBar1DblClick
  end
  object PageControl1: TPageControl [4]
    Left = 0
    Top = 121
    Width = 586
    Height = 113
    ActivePage = TabSheet1
    Align = alTop
    Style = tsButtons
    TabOrder = 4
    object TabSheet1: TTabSheet
      Caption = #1060#1072#1081#1083#1099' '#1076#1083#1103' '#1080#1084#1087#1086#1088#1090#1072
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 96
    inherited act_Ok: TAction
      Caption = #1048#1084#1087#1086#1088#1090
    end
  end
  inherited FormStorage1: TFormStorage
    Left = 124
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 151
  end
end
