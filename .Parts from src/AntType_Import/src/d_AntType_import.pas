unit d_AntType_import;

interface

uses
  Classes, Controls, Forms, ComCtrls, cxPropertiesStore, rxPlacemnt,
  Dialogs, ActnList, StdCtrls, ExtCtrls, Registry,

  f_AntType_Preview,

  d_Wizard,

//  dm_Onega_DB_data,

  dm_Main_data,    

  u_common_const,

  fr_FileList,

  u_func,
  u_dlg,

  u_const_str,

  dm_AntType_Import, cxClasses, System.Actions

  ;

type
  Tdlg_AntennaType_import = class(Tdlg_Wizard)
    Panel4: TPanel;
    lb_Name: TLabel;
    StatusBar1: TStatusBar;
    ed_Folder: TEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Button2: TButton;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
 //   procedure act_OpenExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure act_OkExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure StatusBar1DblClick(Sender: TObject);

  private
    Fframe_FileList: Tframe_FileList;
    FFolderID: integer;


  public
    class function ExecDlg (aFolderID: integer): boolean;
  end;


//====================================================================
implementation  {$R *.dfm}
//====================================================================

//--------------------------------------------------------------------
class function Tdlg_AntennaType_import.ExecDlg(aFolderID: integer): boolean;
//--------------------------------------------------------------------
begin
  with Tdlg_AntennaType_import.Create(Application) do
  try
    FFolderID:=aFolderID;

    ed_Folder.Text:=dmOnega_DB_data.Folder_GetFullPath(FFolderID);

  //  ed_Folder.Text:=dmFolder.GetPath (FFolderID);

    Result:=(ShowModal=mrOk);

  finally
    Free;
  end;

end;

//--------------------------------------------------------------------
procedure Tdlg_AntennaType_import.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
const
  FILTER_MSI_ADF ='DNA ����� (*.msi;*.adf;*.txt)|*.msi;*.adf;*.txt';

begin
  inherited;

  lb_Name.Caption:=STR_FOLDER;
 // Caption:='������ ������';

  Caption :='������ ������ | '+ GetAppVersionStr();


  // common
//  FormStorage1.IniFileName:=g_RegistryRootPath;
  //FormStorage1.Active:=True;


//  pn_Files.Align:=alClient;
  PageControl1.Align:=alClient;
//  ts_Params.Caption:='����� ��� �������';

  CreateChildForm(Tframe_FileList, Fframe_FileList, TabSheet1);
  Fframe_FileList.SetRegistryPath(FRegPath);

 // Fframe_FileList:=Tframe_FileList.CreateChildForm( ts_Params);
  Fframe_FileList.SetFilter(FILTER_MSI_ADF);

//  Fframe_FileList.Se


//  Fframe_FileList.Lo


  with TRegIniFile.Create(FRegPath) do
 // with gl_Reg do
  begin
//    CurrentRegPath := FRegPath;
    Fframe_FileList.OpenDialog1.InitialDir:= ReadString('', 'OpenDir', '');
    Free;
  end;


  StatusBar1.SimpleText:=dmMain.LoginRec.ConnectionStatusStr;

end;

//--------------------------------------------------------------------
procedure Tdlg_AntennaType_import.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  if Fframe_FileList.OpenDialog1.Files.Count > 0 then
    with TRegIniFile.Create(FRegPath) do
    begin
//      CurrentRegPath := FRegPath;
      WriteString ('', 'OpenDir', Fframe_FileList.OpenDialog1.Files[0]);
      Free;
    end;

//  Fframe_FileList.Free;

  inherited;
end;


//--------------------------------------------------------------------
procedure Tdlg_AntennaType_import.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//--------------------------------------------------------------------
begin
  act_Ok.Enabled:=Fframe_FileList.Files.Items.Count>0;
end;

//--------------------------------------------------------------------
procedure Tdlg_AntennaType_import.act_OkExecute(Sender: TObject);
//--------------------------------------------------------------------
begin
  dmAntType_Import.Params.FolderID:=FFolderID;
  dmAntType_Import.Params.FileNames.Assign (Fframe_FileList.Files.Items);

  dmAntType_Import.ExecuteDlg;

  Tfrm_AntType_Preview.ExecDlg;


//  MsgDlg('������ ��������!');


end;

procedure Tdlg_AntennaType_import.Button1Click(Sender: TObject);
begin
  Tfrm_AntType_Preview.ExecDlg;
end;


procedure Tdlg_AntennaType_import.Button2Click(Sender: TObject);
begin
 // g_Log.Msg ('sp_Dictionary_Clear');
  ////////dmMain.ADOConnection.Execute('sp_Dictionary_Clear');
end;


procedure Tdlg_AntennaType_import.StatusBar1DblClick(Sender: TObject);
begin
  if dmMain.OpenDB_dlg then
    StatusBar1.SimpleText:=dmMain.LoginRec.ConnectionStatusStr;

end;


end.
       {

//--------------------------------------------------------------------
procedure Tdlg_AntennaType_import.act_OpenExecute(Sender: TObject);
//--------------------------------------------------------------------
begin
//  ;
end;
