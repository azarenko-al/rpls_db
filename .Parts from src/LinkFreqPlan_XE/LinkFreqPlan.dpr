program LinkFreqPlan;

uses
  Vcl.Forms,
  d_LinkFreqPlan_Main in 'src\d_LinkFreqPlan_Main.pas' {dlg_LinkFreqPlan_Main},
  u_ini_LinkFreqPlan in 'src_shared\u_ini_LinkFreqPlan.pas',
  dm_App in 'src\dm_App.pas' {dmApp_link_calc: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmApp_link_calc, dmApp_link_calc);
  Application.Run;
end.
