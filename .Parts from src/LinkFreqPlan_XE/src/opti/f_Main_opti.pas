unit f_Main_opti;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,  Dialogs, ComCtrls,
  cxDropDownEdit, cxVGrid, DB, cxGridLevel, cxGridDBTableView, cxGrid, ADODB,
  dxmdaset, StdCtrls, ActnList,   cxGridDBBandedTableView,
  Menus, cxGridBandedTableView, ToolWin, cxGridCustomTableView,
  cxGridTableView, cxClasses, cxControls, cxGridCustomView,   cxInplaceContainer, rxPlacemnt,

  u_cx_VGrid_export,

////////////  dm_Optimize_Equipment,


  d_Progress,

  u_LinkEndType_const,
  u_Link_const,

  u_const_db,


  u_opti_class_new,

  u_const_str,

  u_types,

  u_cx,
  u_cx_vgrid,

  dm_Onega_DB_data,

//  dm_On, ActnList, dxmdaset, Grids, DBGrids, StdCtrls

  u_radio,

  u_log,

  u_files,


  //u_const_db,

  u_LinkLine_const,

  u_Opti_classes,

  u_func,

  u_db,

  Grids, DBGrids, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxEdit, cxRadioGroup, cxSpinEdit, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, cxDBData, cxTextEdit, cxCurrencyEdit, cxCheckBox,
  System.Actions
  ;

type
  Tfrm_Main_opti = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    cxVerticalGrid1: TcxVerticalGrid;
    row_SESR: TcxEditorRow;
    row_NetworkType: TcxEditorRow;
    row_Use: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    row_Bitrate: TcxEditorRow;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;
    TabSheet5: TTabSheet;
    qry_Ant: TADOQuery;
    qry_LinkEndType: TADOQuery;
    ds_Ant: TDataSource;
    ds_LinkEndType: TDataSource;
    dxMemData_Ant: TdxMemData;
    dxMemData_linkendtype: TdxMemData;
    qry_LinkEndType_modes: TADOQuery;
    row_KNG: TcxEditorRow;
    ActionList1: TActionList;
    act_LinkEndType_check: TAction;
    act_LinkEndType_uncheck: TAction;
    act_AntennaType_check: TAction;
    act_AntennaType_uncheck: TAction;
    ds_LinkEndType_modes: TDataSource;
    dxMemData_result: TdxMemData;
    dxMemData_resultLinkEndType_name: TStringField;
    ds_dxMemData_result: TDataSource;
    dxMemData_resultAntEnnaType_name: TStringField;
    dxMemData_resultLinkEndType_id: TIntegerField;
    dxMemData_resultAntEnnaType_id: TIntegerField;
    dxMemData_resultRxLevel: TFloatField;
    dxMemData_resultLinkEndType_mode_id: TIntegerField;
    dxMemData_resultBitRate_Mbps: TFloatField;
    dxMemData_resultPower_dBm: TFloatField;
    dxMemData_resultTHRESHOLD: TFloatField;
    ADOStoredProc_ant: TADOStoredProc;
    ADOStoredProc_linkend: TADOStoredProc;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    cxGrid_Res: TcxGrid;
    cxGrid_ResDBBandedTableView_Res: TcxGridDBBandedTableView;
    col_ID: TcxGridDBBandedColumn;
    col__Mode: TcxGridDBBandedColumn;
    col_Bitrate_Mbps: TcxGridDBBandedColumn;
    col__Power_max: TcxGridDBBandedColumn;
    col__threshold_BER_3: TcxGridDBBandedColumn;
    col__threshold_BER_6: TcxGridDBBandedColumn;
    col_sesr: TcxGridDBBandedColumn;
    col_kng: TcxGridDBBandedColumn;
    col_rx_level: TcxGridDBBandedColumn;
    col_Modulation_type: TcxGridDBBandedColumn;
    col_fade_margin_dB: TcxGridDBBandedColumn;
    col_kng_req: TcxGridDBBandedColumn;
    col_sesr_req: TcxGridDBBandedColumn;
    cxGrid_ResLevel1: TcxGridLevel;
    col_gain: TcxGridDBBandedColumn;
    col_antennaType_name: TcxGridDBBandedColumn;
    col_antennaType_ID: TcxGridDBBandedColumn;
    col_LinkEndType_name: TcxGridDBBandedColumn;
    col_LinkEndType_id1: TcxGridDBBandedColumn;
    col_Is_Working1: TcxGridDBBandedColumn;
    ToolBar3: TToolBar;
    Button2: TButton;
    act_Run_short: TAction;
    col_Diameter: TcxGridDBBandedColumn;
    q_Link: TADOQuery;
    ds_Link: TDataSource;
    col_Antenna_Polarization: TcxGridDBBandedColumn;
    act_Run_full: TAction;
    Button10: TButton;
    ToolBar4: TToolBar;
    Button1: TButton;
    col_Vert_width: TcxGridDBBandedColumn;
    col_Rx_Level_draft: TcxGridDBBandedColumn;
    Button8_Save: TButton;
    Button9_Load: TButton;
    col_Zapas_draft: TcxGridDBBandedColumn;
    PopupMenu_Antenna: TPopupMenu;
    actAntennaTypecheck1: TMenuItem;
    actAntennaTypeuncheck1: TMenuItem;
    PageControl2: TPageControl;
    TabSheet_RRS: TTabSheet;
    TabSheet_Ant: TTabSheet;
    cxGrid_ant: TcxGrid;
    cxGrid_antDBTableView_Antenna: TcxGridDBTableView;
    col_checked2: TcxGridDBColumn;
    cxGrid_antDBTableView_Antennafolder_name: TcxGridDBColumn;
    cxGrid_antDBTableView_Antennaname: TcxGridDBColumn;
    cxGrid_antDBTableView_Antennaband: TcxGridDBColumn;
    cxGrid_antDBTableView_Antennagain: TcxGridDBColumn;
    cxGrid_antDBTableView_Antennafreq: TcxGridDBColumn;
    cxGrid_antDBTableView_Antennadiameter: TcxGridDBColumn;
    cxGrid_antDBTableView_Antennavert_width: TcxGridDBColumn;
    cxGrid_antDBTableView_Antennahorz_width: TcxGridDBColumn;
    cxGrid_antDBTableView_Antennapolarization_str: TcxGridDBColumn;
    cxGrid_antDBTableView_AntennaColumn1_id: TcxGridDBColumn;
    cxGrid_antLevel1: TcxGridLevel;
    cxGrid_LinkEndType: TcxGrid;
    cxGridDBTableView1_LinkEndType: TcxGridDBTableView;
    col_Checked1: TcxGridDBColumn;
    cxGridDBTableView1_LinkEndTypename: TcxGridDBColumn;
    cxGridDBTableView1_LinkEndTypefolder_name: TcxGridDBColumn;
    cxGridDBTableView1_LinkEndTypeband: TcxGridDBColumn;
    cxGridDBTableView1_LinkEndTypevendor_id: TcxGridDBColumn;
    cxGridDBTableView1_LinkEndTypeColumn1: TcxGridDBColumn;
    cxGrid_LinkEndTypeDBTableView_mode: TcxGridDBTableView;
    cxGrid_LinkEndTypeDBTableView_modeid: TcxGridDBColumn;
    cxGrid_LinkEndTypeDBTableView_modeLinkEndType_id: TcxGridDBColumn;
    cxGrid_LinkEndTypeDBTableView_modemode: TcxGridDBColumn;
    cxGrid_LinkEndTypeDBTableView_modebitrate_Mbps: TcxGridDBColumn;
    cxGrid_LinkEndTypeDBTableView_modemodulation_type: TcxGridDBColumn;
    cxGrid_LinkEndTypeDBTableView_modethreshold_BER_3: TcxGridDBColumn;
    cxGrid_LinkEndTypeDBTableView_modethreshold_BER_6: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    cxGrid_LinkEndTypeLevel1: TcxGridLevel;
    ToolBar5: TToolBar;
    Button8: TButton;
    PopupMenu_LinkEndType: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    act_Open: TAction;
    FormStorage1: TFormStorage;
    ToolButton1: TToolButton;
    Button4: TButton;
    act_Save: TAction;
    PopupMenu1_result: TPopupMenu;
    MenuItem3: TMenuItem;
    qry_LinkEnd1: TADOQuery;
    ds_LinkEnd: TDataSource;
    ToolButton3: TToolButton;
    act_Save_Ini: TAction;
    Button3: TButton;
    ToolButton2: TToolButton;
    procedure FormDestroy(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_LinkEndType_checkExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button8_SaveClick(Sender: TObject);
    procedure Button9_LoadClick(Sender: TObject);
    procedure cxVerticalGrid1EditValueChanged(Sender: TObject; ARowProperties:
        TcxCustomEditorRowProperties);
    procedure FormCreate(Sender: TObject);
    procedure row_NetworkTypeEditPropertiesCloseUp(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
  private
    FParams: record
       SESR: double;
       KNG: double;

       BitRate_Mbps: Integer;
    end;
     FLink_ID : Integer;


     FIniFileName : string;


     FLen_km: Double;

     FEvent: TProcedureEvent;

    FDataUpdated : boolean;


    procedure ResultsToDataset;
    procedure LoadFromVerticalGrid;
    procedure Load_checked;

    procedure OpenData;

    procedure Run_full;
    procedure Save;
    procedure SaveChecked;
    procedure Save_Results;

  public
    class function ExecDlg(aLink_ID: Integer; aEvent: TProcedureEvent): Boolean;

    //OnStartEvent: TProcedureEvent;

    procedure Init;
    procedure Run;

  end;

var
  frm_Main_opti: Tfrm_Main_opti;

implementation

uses dm_Link_calc;

{$R *.dfm}


const
  FLD_Rx_Level_draft = 'Rx_Level_draft';
  FLD_fade_margin_DRAFT    = 'FLD_fade_margin_DRAFT';


  CAPTION_Rx_Level_DRAFT = '������� ������� [dBm]';
  CAPTION_fade_margin_DRAFT = '����� [dBm]';




procedure Tfrm_Main_opti.FormDestroy(Sender: TObject);
begin
  cx_VerticalGrid_SaveToIni (cxVerticalGrid1, FIniFileName);

end;

procedure Tfrm_Main_opti.ActionList1Update(Action: TBasicAction; var Handled:
    Boolean);
begin
  act_Save.Enabled:=not dxMemData_result.IsEmpty;

  act_Run_short.Enabled:=(not dxMemData_Ant.IsEmpty) and (not dxMemData_linkendtype.IsEmpty);
  act_Run_full.Enabled :=act_Run_short.Enabled;

end;

// ---------------------------------------------------------------
procedure Tfrm_Main_opti.Save;
// ---------------------------------------------------------------
var
  iAntEnnaType_id: Integer;
  iLINKENDTYPE_ID: Integer;
  iLinkEndType_mode_id: Integer;
  k1: Integer;
  k2: Integer;
begin
  iLINKENDTYPE_ID     :=dxMemData_result.FieldByName(FLD_LINKENDTYPE_ID).AsInteger;
  iLinkEndType_mode_id:=dxMemData_result.FieldByName(FLD_LinkEndType_mode_id).AsInteger;


  iAntEnnaType_id:=dxMemData_result.FieldByName(FLD_AntEnnaType_id).AsInteger;

  k1:=dmOnega_DB_data.ExecStoredProc_( 'sp_Link_Update_AntennaType',
     [
       FLD_Link_ID, FLink_ID,

       FLD_AntennaType_ID,  iAntEnnaType_id
     ]);


  k2:=dmOnega_DB_data.ExecStoredProc_( 'sp_Link_Update_LinkEndType',
     [
//       FLD_ID, FLink_ID,
       FLD_Link_ID, FLink_ID,

       FLD_LinkEndType_ID,      iLINKENDTYPE_ID,
       FLD_LinkEndType_Mode_ID, iLinkEndType_mode_id
     ]);

   ShowMessage('���������.');

  FDataUpdated := true;

   //
end;


// ---------------------------------------------------------------
procedure Tfrm_Main_opti.act_LinkEndType_checkExecute(Sender: TObject);
// ---------------------------------------------------------------
begin
  if Sender = act_Save_Ini then
  begin
    Save_Results
  end else


  if Sender = act_Save then
  begin
    Save();
  end else


  if Sender = act_Open then
  begin
    OpenData();
  end else

  if Sender = act_Run_short then
  begin
    SaveChecked();

    Run();
  end else

  if Sender = act_Run_full then
    Run_full() else


  if Sender = act_LinkEndType_check then
    db_UpdateAllRecords_(dxMemData_linkendtype, FLD_CHECKED, True);
  ;
  if Sender = act_LinkEndType_uncheck then
    db_UpdateAllRecords_(dxMemData_linkendtype, FLD_CHECKED, False);
  ;

  if Sender = act_AntennaType_check then
    db_UpdateAllRecords_(dxMemData_Ant, FLD_CHECKED, True);
  ;
  if Sender = act_AntennaType_uncheck then
    db_UpdateAllRecords_(dxMemData_Ant, FLD_CHECKED, False);
  ;


end;

procedure Tfrm_Main_opti.Button1Click(Sender: TObject);
begin
  OpenData;
end;


procedure Tfrm_Main_opti.Button3Click(Sender: TObject);
begin
//
end;



procedure Tfrm_Main_opti.SaveChecked;
begin
  cx_SaveCheckedItemsToIni(cxGridDBTableView1_LinkEndType, FIniFileName, 'LinkEndType');
  cx_SaveCheckedItemsToIni(cxGrid_antDBTableView_Antenna,  FIniFileName, 'Antenna');
end;


procedure Tfrm_Main_opti.Load_checked;
begin
  cx_LoadCheckedItemsFromIni(cxGridDBTableView1_LinkEndType, FIniFileName, 'LinkEndType');
  cx_LoadCheckedItemsFromIni(cxGrid_antDBTableView_Antenna,  FIniFileName, 'Antenna');
end;


procedure Tfrm_Main_opti.Button8_SaveClick(Sender: TObject);
begin
  SaveChecked
end;


procedure Tfrm_Main_opti.Button9_LoadClick(Sender: TObject);
begin
  Load_checked
end;


procedure Tfrm_Main_opti.cxVerticalGrid1EditValueChanged(Sender: TObject;
    ARowProperties: TcxCustomEditorRowProperties);
begin
 // ShowMessage('cxVerticalGrid1EditValueChanged');

  if (cxVerticalGrid1.FocusedRow = row_SESR)  or
     (cxVerticalGrid1.FocusedRow = row_KNG)
  then

    cx_SetRowItemIndex(row_NetworkType, 0);

end;

//  cx



// ---------------------------------------------------------------
class function Tfrm_Main_opti.ExecDlg(aLink_ID: Integer; aEvent:
    TProcedureEvent): Boolean;
// ---------------------------------------------------------------

begin
  with Tfrm_Main_opti.Create(Application) do
  begin
    FEvent:=aEvent;

    FLink_ID:=aLink_ID;

//    OpenData();

    ShowModal;

    result := FDataUpdated;

    Free;
  end;
end;


// ---------------------------------------------------------------
procedure Tfrm_Main_opti.OpenData;
// ---------------------------------------------------------------

const
  DEF_Task_Link_Optimize_equipment = 'sp_Task_Link_Optimize_equipment';

var
  k: Integer;
  s: string;
  sBand: string;
begin
  LoadFromVerticalGrid();


//  dmOnega_DB_data.OpenQuery_(q_Link, 'SELECT * FROM '+ TBL_Link +' WHERE id=:id', [FLD_ID, FLink_ID]);
  dmOnega_DB_data.OpenQuery_(q_Link, 'SELECT * FROM '+ view_Link +' WHERE id=:id', [FLD_ID, FLink_ID]);

{
  s:=Format('SELECT * FROM '+ view_LinkEnd +' WHERE id in (%d,%d)', [ q_Link.FieldByNAme(FLD_LinkEnd1_ID).AsInteger, q_Link.FieldByNAme(FLD_LinkEnd2_ID).AsInteger ]);

  dmOnega_DB_data.OpenQuery(qry_LinkEnd, s, []);

}



  Assert (not q_Link.IsEmpty);


  if not q_Link.IsEmpty then
  begin
    FLen_km:=q_Link[FLD_LENGTH] / 1000;
    sBand:=q_Link.fieldBYName(FLD_BAND).AsString;
  end;

  Assert (FLen_km>0);




  k:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc_ant, DEF_Task_Link_Optimize_equipment,
     [
       FLD_OBJNAME, OBJ_ANTENNA_TYPE,
       FLD_BAND, sBand
     ]);


  k:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc_linkend, DEF_Task_Link_Optimize_equipment,
     [
       FLD_OBJNAME, OBJ_linkend_TYPE,
       FLD_BAND, sBand,
       FLD_BitRate_Mbps,  FParams.BitRate_Mbps
     ]);


  k:=ADOStoredProc_ant.RecordCount;
  k:=ADOStoredProc_linkend.RecordCount;


  TabSheet_RRS.Caption:= Format('��� (%d)',     [ADOStoredProc_linkend.RecordCount]);
  TabSheet_Ant.Caption:= Format('������� (%d)', [ADOStoredProc_ant.RecordCount]);


{
  k:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc_linkend, DEF_Task_Link_Optimize_equipment,
     [
       db_Par(FLD_OBJNAME, OBJ_linkend_TYPE),
       db_Par(FLD_BAND, sBand)
     ]);
}


//  dmOnega_DB_data.OpenQuery(qry_LinkEndType_modes, 'SELECT * FROM '+  TBL_LinkEndType_mode  );
  dmOnega_DB_data.OpenQuery(qry_LinkEndType_modes, 'SELECT * FROM '+  TBL_LinkEndType_mode + ' WHERE bitrate_mbps>=' + IntToStr(FParams.BitRate_Mbps) );


  dxMemData_Ant.Close;
  dxMemData_Ant.Open;

  dxMemData_linkendtype.Close;
  dxMemData_linkendtype.Open;


  dxMemData_Ant.LoadFromDataSet(ADOStoredProc_ant);
  dxMemData_linkendtype.LoadFromDataSet(ADOStoredProc_linkend);

  ds_LinkEndType.DataSet:=dxMemData_linkendtype;
  ds_Ant.DataSet:=dxMemData_Ant;

  Load_checked();


end;



// ---------------------------------------------------------------
procedure Tfrm_Main_opti.FormCreate(Sender: TObject);
// ---------------------------------------------------------------

var

  I: Integer;
  oComboBoxProperties: TcxComboBoxProperties;
  s: string;

begin
//  Assert (not ADOConnection111111.Connected);


 ////// TdmOptimize_Equipment.Init;


  Caption:='����������� ������������';


  s:=ChangeFileExt( ExtractFileName (Application.ExeName), '.ini');
  FIniFileName:= GetCommonApplicationDataDir_Local_AppData ( 'onega') + s;


//  FIniFileName:= ChangeFileExt(Application.ExeName, '.ini');



//  FIniFileName:='d:\111\111.ini';


  cxGrid_ant.Align:=alClient;
  cxGrid_LinkEndType.Align:=alClient;
  cxGrid_Ant.Align:=alClient;
  cxGrid_res.Align:=alClient;

  PageControl1.Align:=alClient;
  PageControl2.Align:=alClient;


  PageControl1.ActivePageIndex:=0;
  PageControl2.ActivePageIndex:=0;


{
  dxMemData_Ant.LoadFromDataSet(qry_Ant);
  dxMemData_linkendtype.LoadFromDataSet(qry_LinkEndType);
}

//  qry_Ant.Close;
//  qry_LinkEndType.Close;


  // row_NetworkType.Properties.EditPropertiesClass:=TcxComboBoxProperties;

   oComboBoxProperties:=TcxComboBoxProperties(row_NetworkType.Properties.EditProperties);

   oComboBoxProperties.Items.Clear;

   for I := 0 to High(LINKLINE_NORM_ARR1) do    // Iterate
   begin
     oComboBoxProperties.Items.AddObject (LINKLINE_NORM_ARR1[i].Name_,
                                          Pointer(i) );

   end;    // for


//   row_NetworkType.Properties.Value:=LINKLINE_NORM_ARR1[0].Name_;


  cx_SetRowItemIndex(row_NetworkType, 6);

  row_NetworkTypeEditPropertiesCloseUp(nil);


//  cxVerticalGrid1.Refresh;



  act_AntennaType_check.Caption:='��� ���';
  act_AntennaType_uncheck.Caption:='���� ���';

  act_LinkEndType_check.Caption:='��� ���';
  act_LinkEndType_uncheck.Caption:='���� ���';

  act_Open.Caption:='���������';

  act_Run_short.Caption:='��������������� ������';//��������� 1 ����';
  act_Run_Full.Caption :='������ ������'; //��������� 2 ����';


  act_Save.Caption :='���������';


  col_Rx_Level_draft.DataBinding.FieldName:=FLD_Rx_Level_draft;
  col_Zapas_draft.DataBinding.FieldName   :=FLD_fade_margin_DRAFT;



//   /
//    LINKLINE_NORM_ARR1

{
  cxGrid_ResDBBandedTableView_Res.DataController.DataSource:=ds_dxMemData_result;

  cxGridDBTableView1_LinkEndType.DataController.DataSource:=ds_LinkEndType;
  cxGrid_LinkEndTypeDBTableView_mode.DataController.DataSource:=ds_LinkEndType_modes;

  cxGrid_antDBTableView_Antenna.DataController.DataSource:=ds_LinkEndType_modes;
 }

  Init;


  cx_VerticalGrid_LoadFromIni (cxVerticalGrid1, FIniFileName);


  if dmLink_Calc.Params.Bitrate_min > 0 then
    row_Bitrate.Properties.Value:=dmLink_Calc.Params.Bitrate_min;


end;

// ---------------------------------------------------------------
procedure Tfrm_Main_opti.Init;
// ---------------------------------------------------------------
begin
  db_SetFieldCaptions(ADOStoredProc_ant,
    [
      FLD_Name,          STR_NAME,
      FLD_Diameter,      STR_DIAMETER,
      FLD_TILT,          STR_TILT, // '�� [dB]',
      FLD_Gain,          STR_GAIN, // '�� [dB]',
      FLD_vert_width,    '������ ��� (V)',
      FLD_horz_width,    '������ ��� (H)',
    //  SArr(FLD_antenna_loss,  STR_LOSS),
      FLD_Polarization_STR,  STR_Polarization,
      FLD_BAND,              STR_BAND
    ]);


  cx_SetColumnCaptions(cxGrid_antDBTableView_Antenna,
    [
      FLD_FOLDER_Name,   STR_FOLDER,

      FLD_CHECKED,   STR_CHECKED,


      FLD_Name,          STR_NAME,
      FLD_Diameter,      STR_DIAMETER,
      FLD_Gain,          STR_GAIN, // '�� [dB]',

      FLD_FREQ,          STR_FREQ_MHz, // '�� [dB]',


      FLD_vert_width,    '������ ��� (V)',
      FLD_horz_width,    '������ ��� (H)',
//..      FLD_loss,        STR_LOSS,
    //  SArr(FLD_antenna_loss,  STR_LOSS),
      FLD_Polarization_STR,  STR_Polarization,
      FLD_BAND,              STR_BAND
    ]);



 cx_SetColumnCaptions(cxGridDBTableView1_LinkEndType,
    [
      FLD_FOLDER_Name,   STR_FOLDER,

      FLD_CHECKED,   STR_CHECKED,


      FLD_Name,         STR_NAME,
      FLD_BAND,         STR_BAND
    ]);


  cx_SetColumnCaptions(cxGrid_LinkEndTypeDBTableView_mode,
     [
      FLD_Mode,             '�����',

      FLD_BITRATE_MBPS,     CAPTION_BITRATE_Mbps,
      FLD_Modulation_type,  CAPTION_MODULATION_TYPE,
      FLD_threshold_BER_3,  CAPTION_THRESHOLD_BER_3,
      FLD_threshold_BER_6,  CAPTION_THRESHOLD_BER_6

    ]);



    db_CreateField(dxMemData_result,
           [
              db_Field(FLD_LinkEndType_id,    ftInteger),
              db_Field(FLD_LinkEndType_name,  ftString),

              db_Field(FLD_LinkEndType_mode_id, ftInteger),
              db_Field(FLD_BitRate_Mbps,    ftFloat),

              db_Field(FLD_MODE, ftInteger),
              db_Field(FLD_Modulation_type,    ftString),


              db_Field(FLD_Power_dBm,    ftFloat),

              db_Field(FLD_THRESHOLD_BER_3, ftFloat),
              db_Field(FLD_THRESHOLD_BER_6, ftFloat),

              // -----------------------------
              db_Field(FLD_AntEnnaType_id,   ftInteger),
              db_Field(FLD_AntEnnaType_name,  ftString),

           //   antennaType_ID

              db_Field(FLD_Polarization,  ftString),
              db_Field(FLD_Gain, ftFloat),
              db_Field(FLD_Diameter, ftFloat),
              db_Field(FLD_Vert_width, ftFloat),
              db_Field(FLD_Freq_Mhz, ftFloat),


              // -----------------------------

              db_Field(FLD_Rx_Level_draft,  ftFloat),
              db_Field(FLD_fade_margin_DRAFT,     ftFloat),

              db_Field(FLD_Rx_Level_dBm,  ftFloat),
              db_Field(FLD_fade_margin_dB,  ftFloat),

              db_Field(FLD_KNG,  ftFloat),
              db_Field(FLD_SESR,  ftFloat),

              db_Field(FLD_KNG_required,  ftFloat),
              db_Field(FLD_SESR_required,  ftFloat),

              db_Field(FLD_Is_Working,  ftBoolean)


              ]);





  cx_SetColumnCaptions(cxGrid_ResDBBandedTableView_Res,
     [
       FLD_LinkEndType_name, CAPTION_name,

       FLD_AntEnnaType_name, CAPTION_name,



      FLD_Name,          STR_NAME,
      FLD_Diameter,      STR_DIAMETER,
      FLD_Gain,          STR_GAIN, // '�� [dB]',

      FLD_FREQ,          STR_FREQ_MHz, // '�� [dB]',


      FLD_vert_width,    '������ ��� (V)',
      FLD_horz_width,    '������ ��� (H)',
//..      FLD_loss,        STR_LOSS,
    //  SArr(FLD_antenna_loss,  STR_LOSS),
       FLD_Polarization,  STR_Polarization,

       FLD_Mode,             '�����',

       FLD_BITRATE_MBPS,     CAPTION_BITRATE_Mbps,
       FLD_threshold_BER_3,  CAPTION_THRESHOLD_BER_3,
       FLD_threshold_BER_6,  CAPTION_THRESHOLD_BER_6,

       FLD_Modulation_type,  CAPTION_MODULATION_TYPE,

       FLD_MODE,             CAPTION_MODE,
       FLD_Power_dBm,        CAPTION_Power_max_dBm,


       FLD_Modulation_type,  CAPTION_Modulation_type,


       FLD_Rx_Level_DRAFT,     CAPTION_Rx_Level_DRAFT,
       FLD_fade_margin_DRAFT,  CAPTION_fade_margin_DRAFT,


       FLD_Rx_Level_dBm,     CAPTION_Rx_Level_dBm,
       FLD_fade_margin_dB,   CAPTION_fade_margin_dB,

       FLD_KNG_required,   'KNG ����[%]',
       FLD_SESR_required,  'SESR ����[%]',

       FLD_KNG,   'KNG',
       FLD_SESR,  'SESR',

       FLD_Is_Working,   '�����������'

      ]);

end;

// ---------------------------------------------------------------
procedure Tfrm_Main_opti.LoadFromVerticalGrid;
// ---------------------------------------------------------------
var
  iValue: Integer;
  s: string;
begin
  cxVerticalGrid1.HideEdit;


  FParams.SESR:= AsFloat(row_SESR.Properties.Value);
  FParams.KNG := AsFloat(row_KNG.Properties.Value);   
  FParams.BitRate_Mbps := AsInteger(row_Bitrate.Properties.Value);

end;



procedure Tfrm_Main_opti.row_NetworkTypeEditPropertiesCloseUp(Sender: TObject);
var
  k: Integer;
  s: string;
begin
  s:=row_NetworkType.Properties.Value;

  k:=cx_GetRowItemIndex (row_NetworkType);


  row_SESR.Properties.Value:= LINKLINE_NORM_ARR1[k].SESR;
  row_KNG.Properties.Value := LINKLINE_NORM_ARR1[k].KNG;


end;


// ---------------------------------------------------------------
procedure Tfrm_Main_opti.Run_full;
// ---------------------------------------------------------------
begin
 if not Assigned (FEvent) then
   ShowMessage(' if not Assigned (FEvent) then');


  if Assigned (FEvent) then
  begin
    FEvent();

    ResultsToDataset;
  end;

end;


// ---------------------------------------------------------------
procedure Tfrm_Main_opti.Run;
// ---------------------------------------------------------------
var
  I1: Integer;
  I2: Integer;
  i3: Integer;

  rec: TCalcReserveRec;

  iTotal : Integer;     

  oTaskItem: TTaskItem;

  oAntennaType: TOptiAntennaType;
  oMode: TOptiLinkEndType_mode;

  oLinkEndType: TOptiLinkEndType;

label
  proc_exit;

begin
  LoadFromVerticalGrid();


  dxMemData_Ant.DisableControls;
  dxMemData_linkendtype.DisableControls;
  qry_LinkEndType_modes.DisableControls;


  g_OptiData.LinkEndTypes.LoadFromDataset(dxMemData_linkendtype);

  if g_OptiData.LinkEndTypes.Count=0 then
  begin
    ShowMessage('�������� ������������.');
    goto proc_exit;
  end;


 // Assert(g_OptiData.LinkEndTypes.Count>0, 'g_Data.LinkEndTypes.Count>0');

  g_OptiData.LinkEndTypes.Mode_LoadFromDataset(qry_LinkEndType_modes, 0 );



  g_OptiData.Antennas.LoadFromDataset(dxMemData_Ant);
//  Assert(g_OptiData.Antennas.Count>0);


  if g_OptiData.Antennas.Count=0 then
  begin
    ShowMessage('�������� ������ ������.');
    goto proc_exit;
  end;


//  dxMemData_result.Open;



  Assert (FLen_km>0);

  rec.Distance_km:=FLen_km;

  iTotal :=0;





  g_OptiTaskItemList.Clear;



   for I1 := 0 to g_OptiData.LinkEndTypes.Count - 1 do
     for I2 := 0 to g_OptiData.LinkEndTypes[i1].Modes.Count - 1 do
       for i3 := 0 to g_OptiData.Antennas.Count - 1 do
       begin
          Inc(iTotal);

         oAntennaType :=g_OptiData.Antennas[i3];
         oMode        :=g_OptiData.LinkEndTypes[i1].Modes[i2];
         oLinkEndType :=g_OptiData.LinkEndTypes[i1];


//         FillChar(rec, SizeOf(rec),0);


         Assert (Rec.Distance_km > 0);

         rec.Power_dBm:=oMode.Power_dBm;
         rec.THRESHOLD:=oMode.THRESHOLD_BER_3;

         rec.Freq_GHz:=oAntennaType.Freq_Mhz/1000;
         rec.Gain1   :=oAntennaType.Gain;
         rec.Gain2   :=oAntennaType.Gain;



         if radio_CalcReserve1(rec) then
           if rec.Result.RxLevel_dBm >= rec.THRESHOLD then

           begin
             oTaskItem:=g_OptiTaskItemList.AddItem();

             Assert (oAntennaType.id>0);

             oTaskItem.Antenna.ID        :=oAntennaType.id;
             oTaskItem.Antenna.Name      :=oAntennaType.Name_;
             oTaskItem.Antenna.Diameter  :=oAntennaType.Diameter;
             oTaskItem.Antenna.Vert_width:=oAntennaType.Vert_width;
             oTaskItem.Antenna.Freq_Mhz  :=oAntennaType.Freq_Mhz;
             oTaskItem.Antenna.Gain      :=oAntennaType.Gain;
             oTaskItem.Antenna.Polarization  :=oAntennaType.Polarization_str;


             oTaskItem.LinkEndType.ID     :=oLinkEndType.ID;
             oTaskItem.LinkEndType.name   :=oLinkEndType.Name_;

             oTaskItem.ID             :=oMode.ID;
             oTaskItem.Mode           :=oMode.Mode;

             oTaskItem.THRESHOLD_BER_3:=oMode.THRESHOLD_BER_3;
             oTaskItem.THRESHOLD_BER_6:=oMode.THRESHOLD_BER_6;
             oTaskItem.BitRate_Mbps   :=oMode.BitRate_Mbps;
             oTaskItem.Power_dBm      :=oMode.Power_dBm;

             oTaskItem.Modulation_type:=oMode.Modulation_type;


             oTaskItem.Result.Rx_Level_draft:=  rec.Result.RxLevel_dBm;
             oTaskItem.Result.Zapas_draft   :=  rec.Result.Zapas;

           end;


       end;

     ResultsToDataset();


   //  ShowMessage  ( Format('%d - %d', [iTotal, g_OptiTaskItemList.Count]) );

proc_exit:

  dxMemData_Ant.EnableControls;
  dxMemData_linkendtype.EnableControls;
  qry_LinkEndType_modes.EnableControls;




  // TODO -cMM: Tz.Run default body inserted
end;


// ---------------------------------------------------------------
procedure Tfrm_Main_opti.ResultsToDataset;
// ---------------------------------------------------------------
var
  I: Integer;

  oTaskItem: TTaskItem;

begin


  dxMemData_result.DisableControls;



   dxMemData_result.Close;
   dxMemData_result.Open;



   for I := 0 to g_OptiTaskItemList.Count - 1 do
   begin
     oTaskItem:=g_OptiTaskItemList[i];

     Assert (oTaskItem.AntEnna.gain<>0);

//     oTaskItem.Mode

//     oTaskItem.Result RxLevel_dBm

     db_AddRecord_(dxMemData_result, [
            // -----------------------------
            FLD_LinkEndType_id,    oTaskItem.LinkEndType.id,
            FLD_LinkEndType_name,  oTaskItem.LinkEndType.Name,


            FLD_MODE,              oTaskItem.Mode,
            FLD_Modulation_type,   oTaskItem.Modulation_type,


            FLD_LinkEndType_mode_id,  oTaskItem.ID,
            FLD_mode,  oTaskItem.mode,
            FLD_BitRate_Mbps,         oTaskItem.BitRate_Mbps,

            FLD_Power_dBm,       oTaskItem.Power_dBm,

            FLD_THRESHOLD_BER_3, oTaskItem.THRESHOLD_BER_3,
            FLD_THRESHOLD_BER_6, oTaskItem.THRESHOLD_BER_6,

            // -----------------------------
            FLD_AntEnnaType_id,    oTaskItem.AntEnna.id,
            FLD_AntEnnaType_name,  oTaskItem.AntEnna.name,
            FLD_gain,              oTaskItem.AntEnna.gain,
            FLD_Diameter,          oTaskItem.AntEnna.Diameter,
            FLD_Vert_width,        oTaskItem.AntEnna.Vert_width,
            FLD_Freq_Mhz,          oTaskItem.AntEnna.Freq_Mhz,

            FLD_Polarization,      oTaskItem.AntEnna.Polarization,

            // -----------------------------

            FLD_Rx_Level_draft,    IIF_NOT_0(Round(oTaskItem.Result.Rx_Level_draft)),
            FLD_fade_margin_DRAFT, IIF_NOT_0(Round(oTaskItem.Result.Zapas_draft)),

            FLD_Rx_Level_dBm,    IIF_NOT_0(oTaskItem.Result.Rx_Level_dBm),


            FLD_Is_Working,      oTaskItem.Result.IsWorking,

            FLD_fade_margin_dB,  IIF_NOT_0( oTaskItem.Result.fade_margin_dB ),

            FLD_SESR,            IIF_NOT_0( oTaskItem.Result.SESR),
            FLD_KNG,             IIF_NOT_0( oTaskItem.Result.KNG),

            FLD_SESR_required,   IIF_NOT_0( oTaskItem.Result.SESR_req),
            FLD_KNG_required,    IIF_NOT_0( oTaskItem.Result.KNG_req)

          ]);

   end;

  dxMemData_result.EnableControls;


end;



procedure Tfrm_Main_opti.ToolButton2Click(Sender: TObject);
begin
end;


// ---------------------------------------------------------------
procedure Tfrm_Main_opti.Save_Results;
// ---------------------------------------------------------------
var
  FDataset: TDataset;

begin
  FDataset:=dxMemData_result;

  dmLink_Calc.Params.Results.Enabled := True;

  dmLink_Calc.Params.Results.MODE           := FDataset.FieldByName(FLD_MODE).AsInteger;
  dmLink_Calc.Params.Results.THRESHOLD_BER_3:= FDataset.FieldByName(FLD_THRESHOLD_BER_3).AsFloat;
  dmLink_Calc.Params.Results.THRESHOLD_BER_6:= FDataset.FieldByName(FLD_THRESHOLD_BER_6).AsFloat;
  dmLink_Calc.Params.Results.Power_dBm      := FDataset.FieldByName(FLD_Power_dBm).AsFloat;

  dmLink_Calc.Params.Results.BitRate_Mbps   := FDataset.FieldByName(FLD_BitRate_Mbps).AsFloat;


  dmLink_Calc.Params.Results.Modulation_type   := FDataset.FieldByName(FLD_Modulation_type).AsString;


  dmLink_Calc.Params.Results.KNG            := FDataset.FieldByName(FLD_KNG).AsFloat;
  dmLink_Calc.Params.Results.SESR           := FDataset.FieldByName(FLD_SESR).AsFloat;
  dmLink_Calc.Params.Results.Rx_Level_dBm   := FDataset.FieldByName(FLD_Rx_Level_dBm).AsFloat;

  dmLink_Calc.Params.Results.FADE_MARGIN_DB := FDataset.FieldByName(FLD_FADE_MARGIN_DB).AsFloat;
  dmLink_Calc.Params.Results.Is_Working     := FDataset.FieldByName(FLD_Is_Working).AsBoolean;


  dmLink_Calc.Params.Results.LinkEndType_name     := FDataset.FieldByName(FLD_LinkEndType_name).AsString;
  dmLink_Calc.Params.Results.antennaType_name     := FDataset.FieldByName(FLD_antennaType_name).AsString;

  dmLink_Calc.Params.Results.LinkEndType_id     := FDataset.FieldByName(FLD_LinkEndType_id).AsInteger;
  dmLink_Calc.Params.Results.LinkEndType_mode_id     := FDataset.FieldByName(FLD_LinkEndType_mode_id).AsInteger;
  dmLink_Calc.Params.Results.antennaType_id     := FDataset.FieldByName(FLD_LinkEndType_id).AsInteger;


  dmLink_Calc.Params.Results.diameter     := FDataset.FieldByName(FLD_diameter).AsFloat;


  dmLink_Calc.Params.SaveToFile('');
end;



end.


