object dlg_LinkFreqPlan_Add_from_map: Tdlg_LinkFreqPlan_Add_from_map
  Left = 477
  Top = 472
  Width = 735
  Height = 479
  BorderWidth = 5
  Caption = 'dlg_LinkFreqPlan_Add_from_map'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid2: TcxGrid
    Left = 0
    Top = 0
    Width = 709
    Height = 161
    Align = alTop
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = ''
    object cxGridDBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGridDBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Width = 46
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 401
    Width = 709
    Height = 29
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Panel2: TPanel
      Left = 509
      Top = 0
      Width = 200
      Height = 29
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object Button1: TButton
        Left = 16
        Top = 0
        Width = 75
        Height = 25
        Cancel = True
        Caption = #1047#1072#1082#1088#1099#1090#1100
        ModalResult = 2
        TabOrder = 0
      end
      object Button2: TButton
        Left = 104
        Top = 0
        Width = 75
        Height = 25
        Cancel = True
        Caption = #1047#1072#1082#1088#1099#1090#1100
        ModalResult = 2
        TabOrder = 1
      end
    end
  end
  object FormPlacement1: TFormPlacement
    IniFileName = 'Software\Onega\'
    Left = 392
    Top = 248
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=server1'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 48
    Top = 264
  end
  object DataSource1: TDataSource
    Left = 184
    Top = 309
  end
  object sp_Filter_items_sel: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ui_explorer.sp_Filter_items_SEL'
    Parameters = <>
    Prepared = True
    Left = 184
    Top = 248
  end
end
