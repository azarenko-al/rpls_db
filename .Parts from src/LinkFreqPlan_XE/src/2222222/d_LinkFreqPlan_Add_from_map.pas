unit d_LinkFreqPlan_Add_from_map;

interface

uses
  dm_Onega_DB_data,
  u_geo,


  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rxPlacemnt, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxCheckBox, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, ADODB, StdCtrls,
  ExtCtrls;

type
  Tdlg_LinkFreqPlan_Add_from_map = class(TForm)
    FormPlacement1: TFormPlacement;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    ADOConnection1: TADOConnection;
    DataSource1: TDataSource;
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    cxGridDBTableView1id: TcxGridDBColumn;
    sp_Filter_items_sel: TADOStoredProc;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class procedure ExecDlg;

    class procedure AddByPoly(var aBLPoints: TBLPointArrayF);
    class procedure AddByRect(aBLRect: TBLRect);
    class procedure AddByRing(aCenter: TBLPoint; aRadius_m: double);


    { Public declarations }
  end;


implementation

{$R *.dfm}

class procedure Tdlg_LinkFreqPlan_Add_from_map.AddByPoly(var aBLPoints:
    TBLPointArrayF);
begin
  ShowMessage ('AddByPoly');
end;

class procedure Tdlg_LinkFreqPlan_Add_from_map.AddByRect(aBLRect: TBLRect);
begin
   ShowMessage ('AddByRect');
end;

class procedure Tdlg_LinkFreqPlan_Add_from_map.AddByRing(aCenter: TBLPoint;
    aRadius_m: double);
begin
    ShowMessage ('AddByRing');
end;

//------------------------------------------------------------------------------
class procedure Tdlg_LinkFreqPlan_Add_from_map.ExecDlg;
//------------------------------------------------------------------------------
begin

  with Tdlg_LinkFreqPlan_Add_from_map.Create(Application) do
  begin
//    FFolderGUID:=aFolderGUID;

  //  OpenData;

    ShowModal;
 //   if Result then
 //     Save;

    Free;
  end;

end;


procedure Tdlg_LinkFreqPlan_Add_from_map.FormCreate(Sender: TObject);
begin
  Caption:='WMS';

  if ADOConnection1.Connected then
    ShowMessage ('procedure Tdlg_LinkFreqPlan_Add_from_map.FormCreate(Sender: TObject);');

end;

end.

{
// ---------------------------------------------------------------
function TrelMatrixBase.ToGeometry: string;
// ---------------------------------------------------------------
var
  I: Integer;
  xy_arr: TXYPointArray;
  bl_arr: TBLPointArray;

  bl_WGS_arr: TBLPointArray;

  bl: TBLPoint;
begin
  SetLength (bl_WGS_arr, 5);

  case MatrixType of
    mtXY_: begin
              xy_arr := geo_XYRectToXYPoints_(XYBounds);
//              Assert(XYBounds.TopLeft.B<>0);

              for I := 0 to High(bl_WGS_arr)-1 do
//              for I := 0 to High(bl_WGS_arr)-1 do
                bl_WGS_arr[i] := geo_Pulkovo42_XY_to_WGS84_BL(xy_arr[i], ZoneNum);

           end;

    mtLonLat_: begin
              assert(BLBounds.TopLeft.B<>0);

              bl_arr := geo_BLRectToBLPoints_(BLBounds);

             // Result := bl_arr;

              for I := 0 to High(bl_WGS_arr)-1 do
                bl_WGS_arr[i] := geo_Pulkovo42_to_WGS84(bl_arr[i]);

           end;

  end;
  // ---------------------------------------------------------------

  Result:='POLYGON((';

  bl_WGS_arr[4]:=bl_WGS_arr[0];

  for I := 0 to High(bl_WGS_arr) do
    Result:=Result+ ReplaceStr( Format('%1.6f %1.6f',[bl_WGS_arr[i].L, bl_WGS_arr[i].B]), ',','.') +
                    IIF(i<High(bl_WGS_arr), ', ','');

  Result:=Result + '))';


//  Result:=ReplaceStr(Result, ',','.');

end;

