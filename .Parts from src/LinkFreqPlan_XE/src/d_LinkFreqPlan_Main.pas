﻿unit d_LinkFreqPlan_Main;

interface

uses
  u_db,
  u_cx,
  u_ini_LinkFreqPlan,
  u_func,

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, Data.DB, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  Data.Win.ADODB, RxPlacemnt, Vcl.StdCtrls, Vcl.ExtCtrls, cxCheckBox;

type
  Tdlg_LinkFreqPlan_Main = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;
    FormPlacement1: TFormPlacement;
    ADOConnection1: TADOConnection;
    ds_Links: TDataSource;
    sp_Links: TADOStoredProc;
    cxGrid_Link: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    ds_Bands: TDataSource;
    sp_Bands: TADOStoredProc;
    cxGridDBTableView1id: TcxGridDBColumn;
    cxGridDBTableView1name: TcxGridDBColumn;
    cxGridDBTableView1band: TcxGridDBColumn;
    cxGrid_Bands: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    cxGridDBTableView2checked: TcxGridDBColumn;
    cxGridDBTableView2band: TcxGridDBColumn;
    DataSource1: TDataSource;
    ADOStoredProc1: TADOStoredProc;
    Panel3: TPanel;
    ed_Name: TLabeledEdit;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormPlacement1RestorePlacement(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FParams: TIni_LinkFreqPlan_Params;
    procedure Save;

  protected
    procedure CreateParams(var Params: TCreateParams); override;
  public
//    Params: record

    class function ExecDlg(aParams: TIni_LinkFreqPlan_Params): Boolean;
  end;

implementation

{$R *.dfm}


procedure Tdlg_LinkFreqPlan_Main.Button1Click(Sender: TObject);
begin
  SAve;
end;

procedure Tdlg_LinkFreqPlan_Main.FormCreate(Sender: TObject);
begin
  if ADOConnection1.Connected then
    ShowMessage ('Tdlg_Main.FormCreate  - ADOConnection1.Connected  ');

  Caption:='Создание частотного плана' + GetAppVersionStr;// +' '+ FormatDateTime ('yyyy-mm-dd hh:mm', GetFileDate (Application.Exename));



  ed_Name.EditLabel.Caption:='Название сети';

  cxGrid_Bands.Align:=alBottom;
  cxGrid_Link.Align:=alClient;


//  WindowState:=wsMaximized;
//  BorderStyle:=bsDialog;

  FormStyle := fsStayOnTop;
end;

procedure Tdlg_LinkFreqPlan_Main.CreateParams(var Params: TCreateParams);
begin
  inherited;
  Params.WndParent := 0;
end;

//------------------------------------------------------------------------------
class function Tdlg_LinkFreqPlan_Main.ExecDlg(aParams: TIni_LinkFreqPlan_Params): Boolean;
//------------------------------------------------------------------------------
begin

  with Tdlg_LinkFreqPlan_Main.Create(Application) do
  begin
    FParams:=aParams;

    try
      db_OpenADOConnectionString(ADOConnection1, aParams.ConnectionString);

      db_StoredProc_Open (ADOStoredProc1, 'sp_Object_GetNewName_select', // [db_Par(FLD_ID, aID)]);
              [ FLD_ObjName,     'linknet',
                FLD_Project_ID,  aParams.Project_ID
              ]);
      Assert(ADOStoredProc1.Fields.Count>0);

      ed_Name.Text:= ADOStoredProc1.Fields[0].AsString;



      db_StoredProc_Open(sp_Links, 'LinkFreqPlan.sp_links_inside_poly_SEL', [
        'PROJECT_ID',   aParams.Project_ID,
        'LAT',          aParams.Lat,
        'LON',          aParams.Lon,
        'RADIUS_m',     aParams.Radius_m,

        'GEOM_STR',     aParams.Geometry_Str,

         'ACTION',      'link'
      ]);

      db_StoredProc_Open(sp_Bands, 'LinkFreqPlan.sp_links_inside_poly_SEL', [
        'PROJECT_ID',   aParams.Project_ID,
        'LAT',          aParams.Lat,
        'LON',          aParams.Lon,
        'RADIUS_m',     aParams.Radius_m,

        'GEOM_STR',     aParams.Geometry_Str,

        'ACTION',       'band'

      ]);


    except on E: Exception do
      ShowMessage (E.Message);
    end;


//    FFolderGUID:=aFolderGUID;

  //  OpenData;

    Result:=ShowModal=mrOk;
 //   if Result then
 //     Save;

    Free;
  end;

end;

procedure Tdlg_LinkFreqPlan_Main.FormPlacement1RestorePlacement(Sender:
    TObject);
begin

end;

//----------------------------------------------------------------
procedure Tdlg_LinkFreqPlan_Main.FormShow(Sender: TObject);
//----------------------------------------------------------------
begin

end;

//----------------------------------------------------------------
procedure Tdlg_LinkFreqPlan_Main.Save;
//----------------------------------------------------------------
var
  iLinkFreqPlan_ID: Integer;
  iLinkNet_ID: Integer;
  k: Integer;
  oSList: TStringList;
  s: string;
  sBand: string;
//  aDataset: TDataset;
begin
  oSList:=TStringList.Create;


  sp_Links.First;
  sp_Links.DisableControls;
//  sValues:='';

  with sp_Links do
   while not EOF do
   begin
     sBand:=FieldByName('Band').AsString;

      if sp_Bands.Locate('checked;band',VarArrayOf([true,sBand]),[]) then
      begin
        oSList.Add(FieldByName('id').AsString);
      end;

      Next;

   end;

  oSList.Delimiter:=',';
  s:=oSList.DelimitedText;


  iLinkNet_ID:=db_StoredProc_exec (ADOStoredProc1, 'dbo.sp_LinkNet_Add', // [db_Par(FLD_ID, aID)]);
            [
              'Project_ID',  FParams.Project_ID,
              'NAME',        ed_Name.Text,
              'LINK_ID_STR', s
            ]);

  FParams.Result.LinkNet_ID:=iLinkNet_ID;

  iLinkFreqPlan_ID:=db_StoredProc_exec (ADOStoredProc1, 'dbo.sp_LinkFreqPlan_add', // [db_Par(FLD_ID, aID)]);
            [
              'LINKNET_ID',         iLinkNet_ID,
              'NAME',               'ЧТП',
              'MaxCalcDistance_km', 100,
              'MIN_CI_dB',          30

            ]);

  // TODO -cMM: Tdlg_Main.Save default body inserted
end;


end.


