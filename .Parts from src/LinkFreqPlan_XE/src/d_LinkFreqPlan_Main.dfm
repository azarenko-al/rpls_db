object dlg_LinkFreqPlan_Main: Tdlg_LinkFreqPlan_Main
  Left = 1019
  Top = 319
  Caption = 'dlg_LinkFreqPlan_Main'
  ClientHeight = 579
  ClientWidth = 545
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poDefault
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 550
    Width = 545
    Height = 29
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Panel2: TPanel
      Left = 345
      Top = 0
      Width = 200
      Height = 29
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object Button1: TButton
        Left = 23
        Top = 0
        Width = 75
        Height = 25
        Cancel = True
        Caption = 'Ok'
        ModalResult = 1
        TabOrder = 0
        OnClick = Button1Click
      end
      object Button2: TButton
        Left = 104
        Top = 0
        Width = 75
        Height = 25
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        ModalResult = 2
        TabOrder = 1
      end
    end
  end
  object cxGrid_Link: TcxGrid
    Left = 0
    Top = 57
    Width = 545
    Height = 145
    Align = alTop
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = ''
    object cxGridDBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ds_Links
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGridDBTableView1name: TcxGridDBColumn
        Caption = #1056#1056#1048
        DataBinding.FieldName = 'name'
        Width = 372
      end
      object cxGridDBTableView1band: TcxGridDBColumn
        Caption = #1044#1080#1072#1087#1072#1079#1086#1085
        DataBinding.FieldName = 'band'
        Width = 73
      end
      object cxGridDBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object cxGrid_Bands: TcxGrid
    Left = 0
    Top = 202
    Width = 545
    Height = 135
    Align = alTop
    TabOrder = 2
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = ''
    object cxGridDBTableView2: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ds_Bands
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGridDBTableView2checked: TcxGridDBColumn
        Caption = ' '
        DataBinding.FieldName = 'checked'
        PropertiesClassName = 'TcxCheckBoxProperties'
      end
      object cxGridDBTableView2band: TcxGridDBColumn
        Caption = #1044#1080#1072#1087#1072#1079#1086#1085
        DataBinding.FieldName = 'band'
        Width = 73
      end
    end
    object cxGridLevel2: TcxGridLevel
      GridView = cxGridDBTableView2
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 545
    Height = 57
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object ed_Name: TLabeledEdit
      Left = 16
      Top = 19
      Width = 393
      Height = 21
      EditLabel.Width = 45
      EditLabel.Height = 13
      EditLabel.Caption = 'ed_Name'
      TabOrder = 0
    end
  end
  object FormPlacement1: TFormPlacement
    IniFileName = 'Software\Onega\'
    Options = []
    UseRegistry = True
    OnRestorePlacement = FormPlacement1RestorePlacement
    Left = 80
    Top = 352
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=server1'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 80
    Top = 416
  end
  object ds_Links: TDataSource
    DataSet = sp_Links
    Left = 216
    Top = 477
  end
  object sp_Links: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'LinkFreqPlan.sp_links_inside_poly_SEL'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@PROJECT_ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@LAT'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@LON'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@RADIUS_m'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@GEOM_STR'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@ACTION'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = 'link'
      end>
    Prepared = True
    Left = 216
    Top = 416
  end
  object ds_Bands: TDataSource
    DataSet = sp_Bands
    Left = 320
    Top = 477
  end
  object sp_Bands: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'LinkFreqPlan.sp_links_inside_poly_SEL'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@PROJECT_ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@LAT'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@LON'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@RADIUS_m'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@GEOM_STR'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@ACTION'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = 'band'
      end>
    Prepared = True
    Left = 320
    Top = 416
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc1
    Left = 400
    Top = 477
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'LinkFreqPlan.sp_links_inside_poly_SEL'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@PROJECT_ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@LAT'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@LON'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@RADIUS_m'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@GEOM_STR'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@ACTION'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = 'band'
      end>
    Prepared = True
    Left = 400
    Top = 416
  end
end
