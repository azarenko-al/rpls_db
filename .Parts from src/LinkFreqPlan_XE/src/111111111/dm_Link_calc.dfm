object dmLink_calc: TdmLink_calc
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 1198
  Top = 360
  Height = 539
  Width = 645
  object qry_LinkEnd1: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT    *'
      'FROM      Property'
      'ORDER BY name')
    Left = 44
    Top = 172
  end
  object qry_Antennas1: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT     dbo.LinkEndAntenna.*, dbo.AntennaType.*'
      'FROM         dbo.AntennaType RIGHT OUTER JOIN'
      
        '                      dbo.LinkEndAntenna ON dbo.AntennaType.id =' +
        ' dbo.LinkEndAntenna.antennaType_id')
    Left = 252
    Top = 176
  end
  object qry_LinkEnd2: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT    *'
      'FROM      Property'
      'ORDER BY name')
    Left = 132
    Top = 172
  end
  object qry_Antennas2: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT     dbo.LinkEndAntenna.*, dbo.AntennaType.*'
      'FROM         dbo.AntennaType RIGHT OUTER JOIN'
      
        '                      dbo.LinkEndAntenna ON dbo.AntennaType.id =' +
        ' dbo.LinkEndAntenna.antennaType_id')
    Left = 344
    Top = 176
  end
  object ADOStoredProc_Link: TADOStoredProc
    Parameters = <>
    Left = 72
    Top = 16
  end
  object qry_Modes: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT    *'
      'FROM      Property'
      'ORDER BY name')
    Left = 244
    Top = 92
  end
  object q_LInk_repeater: TADOQuery
    Parameters = <>
    Left = 368
    Top = 24
  end
  object ds_LInk_repeater: TDataSource
    DataSet = q_LInk_repeater
    Left = 368
    Top = 88
  end
  object q_LInk_repeater_ant: TADOQuery
    Parameters = <>
    Left = 480
    Top = 24
  end
  object ds_LInk_repeater_ant: TDataSource
    DataSet = q_LInk_repeater_ant
    Left = 480
    Top = 88
  end
  object ds_LinkEnd1: TDataSource
    DataSet = qry_LinkEnd1
    Left = 40
    Top = 240
  end
  object ds_LinkEnd2: TDataSource
    DataSet = qry_LinkEnd2
    Left = 136
    Top = 240
  end
  object ds_Antennas1: TDataSource
    DataSet = qry_Antennas1
    Left = 256
    Top = 240
  end
  object ds_Antennas2: TDataSource
    DataSet = qry_Antennas2
    Left = 344
    Top = 240
  end
  object dxMemData_Groups: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 48
    Top = 352
  end
  object dxMemData_Items: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 48
    Top = 408
  end
end
