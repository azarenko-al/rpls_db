unit u_link_calc_classes_export;

interface

uses
  Math,

  u_db,

  u_func,

  u_GEO,

  u_link_calc_classes_main,
  u_rrl_param_rec_new,
  
  I_rel_Matrix1,

  u_rel_Profile;

type
  TDBLink_export = class(TObject)
  private
  public
//    class procedure SaveToClass_new(aDBLink: TDBLink; aDBLinkEnd1, aDBLinkEnd2:
//        TDBLinkEnd; aObj: TrrlCalcParam);
//    class procedure ExportToClass(aDBLink: TDBLink; aObj: TrrlCalcParam);


    class procedure SaveToClass(aDBLink: TDBLink; aDBLinkEnd1, aDBLinkEnd2:
        TDBLinkEnd; aObj: TrrlCalcParam; aRelProfile: TrelProfile;
        aIsProfileExists: Boolean);

        // aIsProfileExists: Boolean;
//        aIsUsePassiveElements: Boolean);
  end;

implementation

(*
class procedure TDBLink_export.ExportToClass(aDBLink: TDBLink; aObj:
    TrrlCalcParam);
begin

end;
*)


  {
class procedure TDBLink_export.SaveToClass_new(aDBLink: TDBLink; aDBLinkEnd1,
    aDBLinkEnd2: TDBLinkEnd; aObj: TrrlCalcParam);
begin
  TDBLink_export.SaveToClass(aDBLink, aDBLinkEnd1, aDBLinkEnd2,
       aObj,  aDBLink.RelProfile,  aDBLink.IsProfileExists);


end;
   }


//--------------------------------------------------------------------
class procedure TDBLink_export.SaveToClass(aDBLink: TDBLink; aDBLinkEnd1,
    aDBLinkEnd2: TDBLinkEnd; aObj: TrrlCalcParam; aRelProfile: TrelProfile;
    aIsProfileExists: Boolean);
//--------------------------------------------------------------------

  //--------------------------------------------------------------------
  procedure DoAddLinkType;
  //--------------------------------------------------------------------
  var
    iStep: integer;
    d,dRefraction: double; //dLength

    bl: TBLPoint;

  begin
    Assert(Assigned(aObj), 'Value not assigned');


      // -------------------------
      // ��� - ������� ��������������� ���������
      // -------------------------

(*
      aObj.RRV.gradient_diel_1      :=aDBLink.RRV_ITU.gradient_diel;  //'������� �������� ��������� ����.�������������,10^-8 1/�' );
      aObj.RRV.gradient_deviation_2 :=aDBLink.RRV_ITU.gradient_deviation;       //'����������� ���������� ���������,10^-8 1/�' );

      if aIsProfileExists then
        d:=0
      else
        d:=0.00001;

     //'��� ������������ �������.(1...3,0->����.�����=����.���.) '+
     //'1-����������������� 2-������������ 3-������' );
      aObj.RRV.underlying_terrain_type_3:=
            aDBLink.RRV_ITU.underlying_terrain_type + 1 + d;

      // '��� ���������(1...3)' );
      // '��� ���������(0...2)' );
      aObj.RRV.terrain_type_14  :=aDBLink.RRV_ITU.terrain_type;  //'��� ���������(0...2) 1-���������� 2-���������� (��������) 3-���������� (������)' );

//      underlying_terrain_type

      aObj.RRV.steam_wet_4:=      aDBLink.RRV_ITU.steam_wet;   //  '���������� ��������� �������� ����, �/���.�');
      aObj.RRV.Q_factor_5:=       aDBLink.RRV_ITU.Q_factor;    //  'Q-������ ������ �����������');
      aObj.RRV.climate_factor_6:= aDBLink.RRV_ITU.climate_factor;//'������������� ������ K��, 10^-6');
      aObj.RRV.factor_B_7:=       aDBLink.RRV_ITU.factor_B;    //  '�������� ������������ b ��� ������� ������');
      aObj.RRV.factor_C_8:=       aDBLink.RRV_ITU.factor_C;    //  '�������� ������������ c ��� ������� ������');
      aObj.RRV.FACTOR_D_9:=       aDBLink.RRV_ITU.FACTOR_D;    //  '�������� ������������ d ��� ������� ������');
      aObj.RRV.RAIN_intensity_10:=aDBLink.RRV_ITU.RAIN_intensity;   //  '������������� ����� � ������� 0.01% �������, ��/���');

*)


      aObj.RRV.REFRACTION_11:=    aDBLink.REFRACTION;  // '�����. ��������� (��� ��.���������=0, ����� �.�.=0)');


   //------------------------------------------------------------------
   // DLT
   //------------------------------------------------------------------
   // DLT: array[1..4] of double;  //���������� � �������� ��������
     {1= 0.100     ;��� ��������� �������, �� ( 0 - ��������� ����)
      2= 1.000     ;�������� ������ ������� V����(g)=V���.�, ��
      3= 1.000     ;�������� ������ ������� g(V����)=g(V���.�), 10^-8
      4=80.000     ;������������ �������� ��� ������� ������������, 10^-8}



      aObj.Calc_Method:=aDBLink.calc_method;
      aObj.DLT.Profile_Step_KM_1:=TruncFloat(aDBLink.Profile_Step_m/1000, 3);
//      aObj.DLT.Profile_Step_KM_1:=TruncFloat(FProfileStep_m/1000, 3);

{
      vGroup:=xml_AddNodeTag (aRoot, 'DLT');
//      iStep:=FieldByName(FLD_profile_step).AsInteger;

      DoAdd (vGroup,  1, TruncFloat(FProfileStep_m/1000, 3),  '��� ��������� �������, �� ( 0 - ��������� ����)');
}


//      rec.DLT.precision_V_diffraction

      aObj.DLT.precision_V_diffraction_2  := aDBLink.Precision_of_condition_V_diffration_is_equal_to_V_min_dB;

//      aObj.DLT.precision_V_diffraction_2  :=1;//   '�������� ������ ������� V����(g)=V���.�, ��');

      aObj.DLT.precision_g_V_diffraction_3:=1;// '�������� ������ ������� g(V����)=g(V���.�), 10^-8');
      aObj.DLT.max_gradient_for_subrefr_4 :=80;//  '������������ �������� ��� ������� ������������, 10^-8');

    // default values
 //     DoAdd (vGroup,  2, 1 {FieldValues[FLD_precision_V_diffraction]},   '�������� ������ ������� V����(g)=V���.�, ��');
 //     DoAdd (vGroup,  3, 1 {FieldValues[FLD_precision_g_V_diffraction]}, '�������� ������ ������� g(V����)=g(V���.�), 10^-8');
 //     DoAdd (vGroup,  4, 80 {FieldValues[FLD_max_gradient_for_subrefr]},  '������������ �������� ��� ������� ������������, 10^-8');


    //TRB: array[1..4] of double;  //���������� � ����������� �������� ���
     {1=0.012      ;����������� �������� SESR ��� ����, %
      2=0.05       ;����������� �������� ���  ��� ����, %
      3=600        ;����� ����, ��
      4=0.200      ;���������� ����� ���, ������������� �������������}

      aObj.Trb.GST_SESR_1    :=aDBLink.Trb.GST_SESR; //   '����������� �������� SESR ��� ����, %');
      aObj.Trb.GST_Kng_2     :=aDBLink.Trb.GST_Kng; //    '����������� �������� ���  ��� ����, %');
      aObj.Trb.KNG_dop_4     :=aDBLink.Trb.KNG_dop; //    '���������� ����� ���, ������������� �������������}');
      aObj.Trb.SPACE_LIMIT_5 :=aDBLink.Trb.SPACE_LIMIT; // '���������� ������������� ������� p(a%)');
      aObj.Trb.SPACE_LIMIT_PROBABILITY_6:=aDBLink.Trb.SPACE_LIMIT_PROBABILITY; //'���������� ����������� a% ������������� ������������ �������� p(a%) [%]');

   //   Assert(aDBLink.GST_equivalent_length_km>0, 'aDBLink.GST_equivalent_length_km>0');

//      aObj.Trb.GST_equivalent_length_km_3:=aDBLink.GST_equivalent_length_km;
      aObj.Trb.GST_length_km_3_:=aDBLink.Trb.GST_LENGTH_km_;


//       aObj.Trb.GST_equivalent_length_km_3:=
//         dmLink_Calc_SESR.Get_Equivalent_Length_KM(ADOStoredProc_Link); //, Link
  //       dmLink_Calc_SESR.Get_Equivalent_Length_KM(aDataset); //, Link

      case aDBLink.Calc_method of
        // -------------------------
        0: //ITU
        // -------------------------
        begin

          aObj.RRV.gradient_diel_1     :=aDBLink.RRV_ITU.gradient_diel;  //'������� �������� ��������� ����.�������������,10^-8 1/�' );
          aObj.RRV.gradient_deviation_2:=aDBLink.RRV_ITU.gradient_deviation; //'����������� ���������� ���������,10^-8 1/�' );


    //      IIF(aIsProfileExists, 0, 0.00001);
(*
          if aIsProfileExists then
            d:=0
          else
            d:=0.00001;
*)
         //'��� ������������ �������.(1...3,0->����.�����=����.���.) '+
         //'1-����������������� 2-������������ 3-������' );
//          aObj.RRV.underlying_terrain_type_3:=
  //              aDBLink.RRV_ITU.underlying_terrain_type + 1 + d;


         //'��� ������������ �������.(1...3,0->����.�����=����.���.) '+
         //'1-����������������� 2-������������ 3-������' );
          aObj.RRV.underlying_terrain_type_3:=
              aDBLink.RRV_ITU.underlying_terrain_type + 1 +
                   IIF(aIsProfileExists, 0, 0.00001);

//          aObj.RRV.terrain_type_14  :=aDBLink.RRV_ITU.terrain_type1+1;  //'��� ���������(0...2) 1-���������� 2-���������� (��������) 3-���������� (������)' );

          aObj.RRV.steam_wet_4:=      aDBLink.RRV_ITU.steam_wet;   //  '���������� ��������� �������� ����, �/���.�');
          aObj.RRV.Q_factor_5:=       aDBLink.RRV_ITU.Q_factor;    //  'Q-������ ������ �����������');
          aObj.RRV.climate_factor_6:= aDBLink.RRV_ITU.climate_factor;//'������������� ������ K��, 10^-6');
          aObj.RRV.factor_B_7:=       aDBLink.RRV_ITU.factor_B;    //  '�������� ������������ b ��� ������� ������');
          aObj.RRV.factor_C_8:=       aDBLink.RRV_ITU.factor_C;    //  '�������� ������������ c ��� ������� ������');
          aObj.RRV.FACTOR_D_9:=       aDBLink.RRV_ITU.FACTOR_D;    //  '�������� ������������ d ��� ������� ������');
          aObj.RRV.RAIN_intensity_10:=aDBLink.RRV_ITU.RAIN_intensity;   //  '������������� ����� � ������� 0.01% �������, ��/���');

          aObj.RRV.Air_temperature_12    :=aDBLink.RRV_ITU.Air_temperature;
          aObj.RRV.Atmosphere_pressure_13:=aDBLink.RRV_ITU.Atmosphere_pressure;

          aObj.RRV.terrain_type_14  :=aDBLink.RRV_ITU.terrain_type;  //'��� ���������(0...2) 1-���������� 2-���������� (��������) 3-���������� (������)' );


        end;

        // -------------------------
        1: //GOST
        // -------------------------
        begin
          aObj.RRV.gradient_diel_1           :=aDBLink.GOST.gradient_diel;  //'������� �������� ��������� ����.�������������,10^-8 1/�' );
          aObj.RRV.gradient_deviation_2      :=aDBLink.GOST.gradient_deviation; //'����������� ���������� ���������,10^-8 1/�' );

          aObj.RRV.terrain_type_14          :=aDBLink.GOST.terrain_type;  //'��� ���������(0...2) 1-���������� 2-���������� (��������) 3-���������� (������)' );


         //'��� ������������ �������.(1...3,0->����.�����=����.���.) '+
         //'1-����������������� 2-������������ 3-������' );
//          aObj.RRV.underlying_terrain_type_3:=
//              aDBLink.RRV_ITU.underlying_terrain_type + 1 +
//                   IIF(aIsProfileExists, 0, 0.00001);

          aObj.RRV.underlying_terrain_type_3 :=
             aDBLink.GOST.underlying_terrain_type +
                 1 + IIF(aIsProfileExists, 0, 0.00001);

          aObj.RRV.steam_wet_4               :=aDBLink.GOST.steam_wet;   //  '���������� ��������� �������� ����, �/���.�');

          aObj.RRV.Air_temperature_12        :=aDBLink.GOST.Air_temperature;
          aObj.RRV.Atmosphere_pressure_13    :=aDBLink.GOST.Atmosphere_pressure;

          //////////////
          Assert(aDBLink.GOST.climate_factor>0, 'aDBLink.GOST.climate_factor=0');

          aObj.RRV.climate_factor_6          :=aDBLink.GOST.climate_factor;//'������������� ������ K��, 10^-6');

          aObj.RRV.RRV_GOST.RAIN_intensity_19 :=aDBLink.GOST.RAIN_intensity;

          //////////////
          //Assert(aDBLink.GOST.Link_Center.b <> aDBLink.GOST.Link_Center.L);

//          aObj.RRV.GOST.Link_Center_lat_15   :=aDBLink.GOST.Link_Center1.B;
 //         aObj.RRV.GOST.Link_Center_lon_16   :=aDBLink.GOST.Link_Center1.L;

        end;

        // -------------------------
        2: //NIIR1998
        // -------------------------
        begin
          aObj.RRV.gradient_diel_1     :=aDBLink.NIIR1998.gradient_diel;  //'������� �������� ��������� ����.�������������,10^-8 1/�' );
          aObj.RRV.gradient_deviation_2:=aDBLink.NIIR1998.gradient_deviation; //'����������� ���������� ���������,10^-8 1/�' );

//          aObj.RRV.terrain_type_14 :=aDBLink.NIIR1998.terrain_type+1;  //'��� ���������(0...2) 1-���������� 2-���������� (��������) 3-���������� (������)' );
          aObj.RRV.terrain_type_14 :=aDBLink.NIIR1998.terrain_type;  //'��� ���������(0...2) 1-���������� 2-���������� (��������) 3-���������� (������)' );
          aObj.RRV.steam_wet_4      :=aDBLink.NIIR1998.steam_wet;   //  '���������� ��������� �������� ����, �/���.�');

          aObj.RRV.Air_temperature_12    :=aDBLink.NIIR1998.Air_temperature;
          aObj.RRV.Atmosphere_pressure_13:=aDBLink.NIIR1998.Atmosphere_pressure;

          //10 ����� ������ �� ����� ���� (���.8.11) ��� ������������� ������
          aObj.RRV.RRV_NIIR1998.Rain_region_number_17:=aDBLink.NIIR1998.rain_region_number;

          //11 ����� ������ �� ����� ���� (���.8.15) ��� ��������� Q�
          aObj.RRV.RRV_NIIR1998.Qd_region_number_18  :=aDBLink.NIIR1998.Qd_region_number;

          // 9. ���� ������ ����������� �� �������-��, %
          aObj.RRV.RRV_NIIR1998.Water_percent_20     :=aDBLink.NIIR1998.water_area_percent;

        end;

     // -------------------------
      3: begin //  RRV_E_band_ITU : record  //3.�-�������� (��.ITU-R)
      // -------------------------
        aObj.RRV.RRV_E_band_ITU.Param_1 :=aDBLink.RRV_E_band_ITU.Param_1;
        aObj.RRV.RRV_E_band_ITU.Param_2 :=aDBLink.RRV_E_band_ITU.Param_2;
        aObj.RRV.RRV_E_band_ITU.Param_3 :=aDBLink.RRV_E_band_ITU.Param_3;
        aObj.RRV.RRV_E_band_ITU.Param_4 :=aDBLink.RRV_E_band_ITU.Param_4;
        aObj.RRV.RRV_E_band_ITU.Param_5 :=aDBLink.RRV_E_band_ITU.Param_5;
        aObj.RRV.RRV_E_band_ITU.Param_6 :=aDBLink.RRV_E_band_ITU.Param_6;

//        Param_1 : Double;  //	1.������� �������� ��������� ����. �������������, 10^-8, 1/�	1	�� �����. (-9)
//        Param_2 : Double;  //	2.����������� ���������� ���������,10^-8, 1/�	2	�� �����. 7
//        Param_3 : Double;  //	3.���������� ��������� �������� ����, �/���.�	4	�� �����. 7.5
//        Param_4 : double;  //	4.����������� �������, ��.�	12	�� �����. 15
//        Param_5 : Double;  //	5.����������� ��������, ����	13	�� �����. 1013
//        Param_6 : double;  //	6.������������� ����� � ������� 0.01% �������, ��/���	10	�� �����.20

      end;

  (*    // -------------------------
      4: begin //RRV_E_band_GOST : record //4.�-�������� (��.����)
      // -------------------------
        aObj.RRV.RRV_E_band_GOST.Param_1 :=aDBLink.RRV_E_band_GOST.Param_1;
        aObj.RRV.RRV_E_band_GOST.Param_2 :=aDBLink.RRV_E_band_GOST.Param_2;
        aObj.RRV.RRV_E_band_GOST.Param_3 :=aDBLink.RRV_E_band_GOST.Param_3;
        aObj.RRV.RRV_E_band_GOST.Param_4 :=aDBLink.RRV_E_band_GOST.Param_4;
        aObj.RRV.RRV_E_band_GOST.Param_5 :=aDBLink.RRV_E_band_GOST.Param_5;
        aObj.RRV.RRV_E_band_GOST.Param_6 :=aDBLink.RRV_E_band_GOST.Param_6;
*)
//
//        Param_1 : Double; //	1. ������� �������� ��������� ����. �������������,10^-8, 1/�	1	�� �����. (-9)
//        Param_2 : Double; //	2.����������� ���������� ���������,10^-8, 1/�	2	�� �����. 7
//        Param_3 : Double; //	3.���������� ��������� �������� ����, �/���.�	4	�� �����. 7.5
//        Param_4 : double; //	4.����������� �������, ��.�	12	�� �����. 15
//        Param_5 : double; //	5.����������� ��������, ����	13	�� �����. 1013
//        Param_6 : double; //	6.����������� ������������� �����	19	�� �����. 1
    //    Param_7 : double; //*	7.���������� ������� ��������� (������), ��	15	�� �����.60
     //   Param_8 : Double; //*	8.���������� ������� ��������� (�������), ��	16	�� �����.30
   //   end;

   {
      // -------------------------
      5: begin // RRV_E_band_NIIR : record //5.�-�������� (��.����)
      // -------------------------
        aObj.RRV.RRV_E_band_NIIR.Param_1 :=aDBLink.RRV_E_band_NIIR.Param_1;
        aObj.RRV.RRV_E_band_NIIR.Param_2 :=aDBLink.RRV_E_band_NIIR.Param_2;
        aObj.RRV.RRV_E_band_NIIR.Param_3 :=aDBLink.RRV_E_band_NIIR.Param_3;
        aObj.RRV.RRV_E_band_NIIR.Param_4 :=aDBLink.RRV_E_band_NIIR.Param_4;
        aObj.RRV.RRV_E_band_NIIR.Param_5 :=aDBLink.RRV_E_band_NIIR.Param_5;
        aObj.RRV.RRV_E_band_NIIR.Param_6 :=aDBLink.RRV_E_band_NIIR.Param_6;
        aObj.RRV.RRV_E_band_NIIR.Param_7 :=aDBLink.RRV_E_band_NIIR.Param_7;

//        Param_1 : Double; // 1.������� �������� ��������� ����. �������������,10^-8, 1/�	1	�� �����. (-9)
//        Param_2 : Double; // 2.����������� ���������� ���������,10^-8, 1/�	2	�� �����. 7
//        Param_3 : Double; // 3.���������� ��������� �������� ����, �/���.�	4	�� �����. 7.5
//        Param_4 : double; // 4.����������� �������, ��.�	12	�� �����. 20
//        Param_5 : double; // 5.����������� ��������, ����	13	�� �����. 1013
//        Param_6 : double; // 6.����� ������ �� ����� ������������� ������ (1...22)	17	�� �����.1
//        Param_7 : double; // 7.����� ������ (������) �� ����� Qd (1�22)	18	�� �����.1

      end;
   }

      // -------------------------
//      6: begin  //RRV_UKV : record  //    6.���-�������� (16 �����)
      4: begin  //RRV_UKV : record  //    6.���-�������� (16 �����)
      // -------------------------
        aObj.RRV.RRV_UKV.Param_1 :=aDBLink.RRV_UKV.Param_1;
        aObj.RRV.RRV_UKV.Param_2 :=aDBLink.RRV_UKV.Param_2;
        aObj.RRV.RRV_UKV.Param_3 :=aDBLink.RRV_UKV.Param_3;
        aObj.RRV.RRV_UKV.Param_4 :=aDBLink.RRV_UKV.Param_4;


    //    Param_1 : Double; // 1.������� �������� ��������� ����. �������������, 10^-8, 1/�	1	�� �����. (-8)
     //   Param_2 : Integer;// 2.��� ������������ ����������� (1-������������.(����.), 2-�������������., 3-����.)	3	� xml ���������� ��-��: 1,2,3 (�� �����.2)
      //  Param_3 : Double; // 3.��������.����������� c (0-�����.����.�� ���)	8	�� �����.0
       // Param_4 : double; // 4.��� ������ ������� (��� �>0) [dB] (0-���������)	12	�� �����.0
      end;



      end;

(*

      //-------------------------------------------
      // ����
      //-------------------------------------------
      aObj.RRV.Air_temperature_12    :=aDBLink.RRV_ITU.Air_temperature;
      aObj.RRV.Atmosphere_pressure_13:=aDBLink.RRV_ITU.Atmosphere_pressure;

      aObj.RRV.GOST.RAIN_intensity_19:=aDBLink.GOST.RAIN_intensity;

      bl:=geo_GetVectorCenter (aDBLink.BLVector);

      aObj.RRV.GOST.Link_Center_lat_15:=bl.B;
      aObj.RRV.GOST.Link_Center_lon_16:=bl.L;
*)


(*      aObj.RRV.GOST.Link_Center_lat_15:=aDBLink.RRV.GOST.Link_Center.B;
      aObj.RRV.GOST.Link_Center_lon_16:=aDBLink.RRV.GOST.Link_Center.L;
*)

   (*   //-------------------------------------------
      // NIIR1998
      //-------------------------------------------
      aObj.RRV.NIIR1998.Rain_region_number_17:=aDBLink.NIIR1998.rain_region_number;
      aObj.RRV.NIIR1998.Qd_region_number_18  :=aDBLink.NIIR1998.Qd_region_number;

///////////      if aDBLink.RRV.NIIR1998.NIIR1998_water_area_percent=0 then
      aDBLink.NIIR1998.water_area_percent := aRelProfile.GetWaterAreaPercent();

      aObj.RRV.NIIR1998.Water_percent_20:=aDBLink.NIIR1998.water_area_percent;
*)


(*      aObj.RRV.GOST_53363.Air_temperature    :=aDBLink.RRV.GOST_53363.Air_temperature;
      aObj.RRV.GOST_53363.Atmosphere_pressure:=aDBLink.RRV.GOST_53363.Atmosphere_pressure;
*)
//      aObj.RRV.GOST_53363. Atmosphere_pressure:=Link.RRV.GOST_53363.Atmosphere_pressure;


    //  bl:=geo_GetVectorCenter (aDBLink.BLVector);

    //  aObj.RRV.GOST.Link_Center_lat_15:=aDBLink.RRV.GOST.Link_Center.B;
     // aObj.RRV.GOST.Link_Center_lon_16:=aDBLink.RRV.GOST.Link_Center.L;

  end;


  //--------------------------------------------------------------------
  procedure DoSaveProfileToXMLNode (aStep: integer); //vRoot: IXMLNode;
  //--------------------------------------------------------------------
  var i: integer;
    b: Boolean;
  //  b: Boolean;
    e: double;

    rec: TrelProfilePointRec;
  //  eDistance_KM: Double;
  ///  eDistKM: double;
   // k: Integer;
  begin
    if aRelProfile.Count=0 then
      Exit;


     Assert (aRelProfile.Data.Distance_KM>0); 

  ///////  aObj.Relief.RecordCount := aRelProfile.Count;
//    aObj.Relief.Length_KM   := aDBLink.Length_km;
    aObj.Relief.Length_KM   := aRelProfile.Data.Distance_KM;

  // e:=aRelProfile.Data.Distance_KM; ////////////

//   aRelProfile.Data.Distance_KM := aDBLink.Length_km;


    SetLength (aObj.relief.Items, aRelProfile.Count);

    b:=aDBLink.LinkEnd1 = aDBLinkEnd1;

    if not b then
      aRelProfile.IsDirectionReversed:=not aRelProfile.IsDirectionReversed;


  //  aRelProfile.ShowXmlFile;

  //  aRelProfile.IsDirectionReversed := not b;

    with aRelProfile do
      for i:=0 to Count-1 do
      begin
         // k:=i;
        //  eDistance_KM := Items[i].Distance_KM;

      (*    e:=aRelProfile.Data.Distance_KM;

     //     k:=Count-1-i;
       //   eDistance_KM := Data.Distance_KM - Items[i].Distance_KM;


        if b then begin
          k:=i;
          eDistance_KM := Items[i].Distance_KM;
        end else begin
          k:=Count-1-i;

          eDistance_KM := Data.Distance_KM - Items[k].Distance_KM;
          if eDistance_KM<0 then
            eDistance_KM := 0;

        end;
        *)

       // Assert(eDistance_KM>=0, 'Value <=0');

        try
          rec:= Items[i];


          aObj.relief.Items[i].Distance_KM := rec.Distance_KM;

          aObj.relief.Items[i].rel_h        :=rec.Rel_H;
          aObj.relief.Items[i].clutter_h    :=rec.Clutter_H;
          aObj.relief.Items[i].clutter_code :=rec.Clutter_Code;

          aObj.relief.Items[i].earth_h    :=TruncFloat(rec.earth_h, 2);
        except
          
        end;


(*        aObj.relief.Items[i].Distance_KM:=Items[i].Distance_KM;

        aObj.relief.Items[i].rel_h      :=Items[i].Rel_H;
        aObj.relief.Items[i].local_h    :=Items[i].Clutter_H;
        aObj.relief.Items[i].local_code :=Items[i].Clutter_Code;

        aObj.relief.Items[i].earth_h    :=TruncFloat(Items[i].earth_h, 2);
*)

      end;

  end;

 // -------------------------------------------------------------------

var
  iValue: integer;

  dThreshold_dBm: Double;
  dLinkEnd1_Loss_dBm: double;
  dLinkEnd2_Loss_dBm: double;

  oDBAntenna: TDBAntenna;
  s: string;

begin
//  assert(aLinkID<>0);

  aObj.Link_Center_Point:=  geo_GetVectorCenterV( aDBLink.BLVector);

  DoAddLinkType();// (vRoot);

  // ��� �����������, 0-��������������,1-������������
  // P������ �������, ���
  // �������� ��������, ��/�
  // ������ ���������, ���
  // ������ ���������, ��
  // ��� ������� ���������, 1-���, 2-��, 3-���
  // ����� ������� ���������
  // �������, �������� ������������, ��
  // ��������� ���������� �� �������, 1...8
  // ��������� ���������� �� ������������, 1...2
  // ��������� ������, ���

//  if not aDBLink.Runtime.IsUsePassiveElements then
  if not aDBLink.Is_UsePassiveElements then
    dLinkEnd1_Loss_dBm:= 0
  else
    dLinkEnd1_Loss_dBm:= aDBLinkEnd1.LOSS_dBm +
                        aDBLinkEnd1.Passive_element_LOSS_dBm;

(*
  dLinkEnd_Loss_dBm:= FDBLinkEnd1.LOSS_dBm;
 //// dLinkEnd_Loss_dBm:= qry_LinkEnd1.FieldByName(FLD_LOSS).AsFloat;

  if not Params.IsUsePassiveElements then
    dLinkEnd_Loss_dBm:= 0;
*)

 // qry_Antennas1.First;

  if aDBLinkEnd1.Antennas.Count>=1 then
  begin
    oDBAntenna:=aDBLinkEnd1.Antennas[0];

//    FblVectorForOffset.Point1:=oDBAntenna.BLPoint;

    aObj.TTX.Site1_Power_dBm :=   aDBLinkEnd1.Power_dBm;
    aObj.TTX.Site1_DIAMETER  :=   oDBAntenna.DIAMETER;
    aObj.TTX.Site1_GAIN      :=   oDBAntenna.Gain;
    aObj.TTX.Site1_VERT_WIDTH:=   oDBAntenna.VERT_WIDTH;
    aObj.TTX.Site1_LOSS      :=   oDBAntenna.Loss + dLinkEnd1_Loss_dBm;
    aObj.TTX.Site1_HEIGHT    :=   oDBAntenna.HEIGHT;


   //TTX.POLARIZATION, '��� �����������, 0-��������������,1-������������,2-�����');

   case oDBAntenna.PolarizationType of
     ptH__: iValue:=0;
     ptV__: iValue:=1;
   else
     iValue:=2;
   end;

//
//   s:=oDBAntenna.Polarization_str;
//
//   if Eq(s,'h') then iValue:=0 else
//   if Eq(s,'v') then iValue:=1
//                else iValue:=2;

    aObj.TTX.POLARIZATION := iValue;// oDBAntenna.GetPOLARIZATION;
  end;

  //���������������� dBm
  case aDBLink.BER_REQUIRED_Index of
    0: dThreshold_dBm:=aDBLinkEnd2.THRESHOLD_BER_3;
    1: dThreshold_dBm:=aDBLinkEnd2.THRESHOLD_BER_6;
  else
    dThreshold_dBm:=0;
  end;

//  FLD_PASSIVE_ELEMENT_LOSS

//  if not aDBLink.Runtime.IsUsePassiveElements then
  if not aDBLink.Is_UsePassiveElements then
    dLinkEnd2_Loss_dBm:= 0
  else
    dLinkEnd2_Loss_dBm:= aDBLinkEnd2.LOSS_dBm +
                        aDBLinkEnd2.Passive_element_LOSS_dBm;


  if aDBLinkEnd2.Antennas.Count>=1 then
  begin
    oDBAntenna:=aDBLinkEnd2.Antennas[0];

    aObj.TTX.Site2_Threshold_dBM := dThreshold_dBm;
    aObj.TTX.Site2_DIAMETER      := oDBAntenna.DIAMETER;
    aObj.TTX.Site2_GAIN          := oDBAntenna.Gain;
    aObj.TTX.Site2_VERT_WIDTH    := oDBAntenna.VERT_WIDTH;
    aObj.TTX.Site2_LOSS          := oDBAntenna.Loss + dLinkEnd2_Loss_dBm;
    aObj.TTX.Site2_HEIGHT        := oDBAntenna.HEIGHT;
  end;

  // -------------------------------------------------------------------
  //  POLARIZATION
  // -------------------------------------------------------------------

  aObj.TTX.Freq_GHz:=aDBLinkEnd1.Tx_Freq_Ghz;
  //FFreq_GHz;

  // -------------------------------------------------------------------
  //  LEFT  - ����������
  // -------------------------------------------------------------------

  aObj.TTX.SIGNATURE_WIDTH_Mhz    := aDBLinkEnd1.SIGNATURE_WIDTH;//,  'double; //  '������ ���������, ���');
  aObj.TTX.SIGNATURE_HEIGHT_dB    := aDBLinkEnd1.SIGNATURE_HEIGHT;//, 'double; // '������ ���������, ��');
  aObj.TTX.MODULATION_level_COUNT := aDBLinkEnd1.MODULATION_COUNT;//,double;// '����� ������� ���������');
 // aObj.TTX.EQUALISER_PROFIT:=0;// Link.LinkEnd1.EQUALISER_PROFIT1;//, 'double; // '�������, �������� ������������, ��');
  aObj.TTX.BitRate_Mbps           := aDBLinkEnd1.BitRate_Mbps;


  aObj.TTX.XPIC_42_use_2_stvol:= aDBLinkEnd1.XPIC_42_use_2_stvol;
  aObj.TTX.XPIC_49_coef       := aDBLinkEnd1.XPIC_49_coef;
  aObj.TTX.XPIC_50            := aDBLinkEnd1.XPIC_50;



  s:=aDBLinkEnd1.GOST_53363_modulation;
  if Eq(s,'PSK(QPSK)') then
    iValue:=0 else
  if Eq(s,'QAM(TCM)') then
    iValue:=1
  else
    iValue:=1;
//    iValue:=-1;

  aObj.TTX.GOST_53363.Modulation_Type := iValue;
  aObj.TTX.GOST_53363.RX_count_on_combined_diversity := aDBLinkEnd1.GOST_53363_RX_count_on_combined_diversity;



  // -------------------------------------------------------------------
  //  RIGHT  - ��������
  // -------------------------------------------------------------------

  iValue:=aDBLinkEnd2.KRATNOST_BY_SPACE;
  if (iValue=1) and (aDBLinkEnd2.Antennas.Count<=1)
    then iValue:=0;


  aObj.ttx.KRATNOST_BY_SPACE:=iValue+ 1;

   // 21 ��������� ���������� �� �������, 1...8"/>

   if ((aDBLinkEnd1.KRATNOST_BY_FREQ = 2) and
       (aDBLinkEnd2.KRATNOST_BY_FREQ = 2))
   then begin
     iValue:= 1;
   end else

   if ((aDBLinkEnd1.KRATNOST_BY_FREQ = 1) and
       (aDBLinkEnd2.KRATNOST_BY_FREQ = 1))
   then begin
     iValue:= Min(aDBLinkEnd1.FREQ_CHANNEL_COUNT,
                  aDBLinkEnd2.FREQ_CHANNEL_COUNT) + 1;
   end else
     iValue:= 1;

//         <param id="22" value="1" comment="��������� ���������� �� ������������, 1...2"/>
//      <param id="23" value="49" comment="��������� ������, ���"/>


  //'��������� ���������� �� �������, 1...8');  //�� ���
  aObj.TTX.KRATNOST_BY_FREQ:=iValue;

  //'��������� ������, ���');
  aObj.TTX.Freq_Spacing_MHz :=aDBLinkEnd1.Freq_Spacing_MHz;


  aObj.TTX.ATPC_profit_dB :=aDBLinkEnd1.ATPC_profit_dB;
  if not aDBLinkEnd1.Use_ATPC then
    aObj.TTX.ATPC_profit_dB :=0;

  aObj.TTX.Threshold_degradation_dB :=aDBLinkEnd1.Threshold_degradation_dB;

{ 24-28 - ��� ��� 1
  29-33 - ��� ��� 2
  }

  if aDBLinkEnd1.Antennas.Count>=2 then
  begin
    oDBAntenna:=aDBLinkEnd1.Antennas[1];

    aObj.TTX.Dop_Site1.DIAMETER  := oDBAntenna.DIAMETER;
    aObj.TTX.Dop_Site1.GAIN      := oDBAntenna.GAIN;
    aObj.TTX.Dop_Site1.VERT_WIDTH:= oDBAntenna.VERT_WIDTH;
//    aObj.TTX.Dop_Site1.LOSS1      := oDBAntenna.Loss  + dLinkEnd_Loss_dBm;;
    aObj.TTX.Dop_Site1.LOSS1      := oDBAntenna.Loss  + dLinkEnd1_Loss_dBm;
    aObj.TTX.Dop_Site1.HEIGHT    := oDBAntenna.HEIGHT;
  end;

  if aDBLinkEnd2.Antennas.Count>=2 then
  begin
    oDBAntenna:=aDBLinkEnd2.Antennas[1];

//    FblVectorForOffset.Point2:=oDBAntenna.BLPoint;

    aObj.TTX.Dop_Site2.DIAMETER  :=   oDBAntenna.DIAMETER;
    aObj.TTX.Dop_Site2.GAIN      :=   oDBAntenna.GAIN;
    aObj.TTX.Dop_Site2.VERT_WIDTH:=   oDBAntenna.VERT_WIDTH;
//    aObj.TTX.Dop_Site2.LOSS1      :=   oDBAntenna.Loss;
    aObj.TTX.Dop_Site2.LOSS1      :=   oDBAntenna.Loss  + dLinkEnd2_Loss_dBm;
    aObj.TTX.Dop_Site2.HEIGHT    :=   oDBAntenna.HEIGHT;

  end;

  aObj.TTX.Site1_KNG:=aDBLinkEnd1.KNG;  //'��� ������������ (%)1');
  aObj.TTX.Site2_KNG:=aDBLinkEnd1.KNG;  //'��� ������������ (%)1');


//  aObj.TTX.AntennaOffsetHor_m := aDBLinkEnd1.Antenna_offset_horizontal_m;
  aObj.TTX.AntennaOffsetHor_m := aDBLinkEnd2.Antenna_offset_horizontal_m;

//  Antenna_offset_horizontal_m

 // FblVectorForOffset.Point2:=oDBAntenna.BLPoint;


//  gl_XMLDoc.SaveToFile(aFileNameNoProfile);

//  TrrlParamRec_SaveToXML (rec, aFileNameNoProfile);//'e:\2.xml');

  DoSaveProfileToXMLNode (aDBLink.Profile_Step_m);     //vRoot,

 // gl_XMLDoc.SaveToFile(aFileName);
 // gl_XMLDoc.SaveToFile(TEMP_IN_FILENAME);

//    gl_XMLDoc.SaveToFile('e:\1.xml');

end;

end.
