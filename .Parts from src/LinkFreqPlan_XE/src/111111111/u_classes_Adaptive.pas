unit u_classes_Adaptive;

interface

uses
  classes;

type
  TAdaptiveModeRec1 = record
      ID              : Integer;
      Mode            : Integer;

      THRESHOLD_BER_3 : double;
      THRESHOLD_BER_6 : double;

      Power_dBm       : Double;
      BitRate_Mbps    : double;

      Modulation_type : string;

      KNG             : Double;
      SESR            : Double;
      Rx_Level_dBm    : Double;

      fade_margin_dB  : Double;  // ����� �� ��������� [dB]
      KNG_req         : Double;  // ����
      SESR_req        : Double;  // ����

      IsWorking : Boolean;  //�����, �� �����
    end;


  // ---------------------------------------------------------------
  TAdaptiveModeRecItem111111111111 = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
     ID              : Integer;
      Mode            : Integer;

      THRESHOLD_BER_3 : double;
      THRESHOLD_BER_6 : double;

      Power_dBm       : Double;
      BitRate_Mbps    : double;

      Modulation_type : string;

      KNG             : Double;
      SESR            : Double;
      Rx_Level_dBm    : Double;

      fade_margin_dB  : Double;  // ����� �� ��������� [dB]
      KNG_req         : Double;  // ����
      SESR_req        : Double;  // ����

      IsWorking : Boolean;  //�����, �� �����
  end;



implementation

end.
