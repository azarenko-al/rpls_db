unit x_link_calc;

interface


uses  Sysutils, 

  u_dll_with_dmMain,

  dm_Main,
  //dm_Onega_DB_data,

  dm_Link_Calc,

  i_Link_calc
  ;

type

  TLink_calcX = class(TDllCustomX, ILink_calcX)
  public
     procedure Execute(aFileName: WideString); stdcall;
  end;

  function DllGetInterface(const IID: TGUID; var Obj): HResult; stdcall;

exports
  DllGetInterface;


implementation




// ---------------------------------------------------------------
procedure TLink_calcX.Execute(aFileName: WideString);
// ---------------------------------------------------------------

var
  iID: integer;
//  iCount: integer;
  I: integer;
//  iMode: integer;
//  iMapDesktopID: integer;
  b: boolean;


begin
  Assert(Assigned(dmMain), 'Assigned(dmMain) not assigned');

  TdmLink_Calc.Init;
//////  dmLink_Calc.Params1.ADOConnection:=dmMain.ADOConnection;

 // db_SetComponentADOConnection (dmLink_Calc, dmMain.ADOConnection);



  dmLink_Calc.Params.LoadFromFile(aFileName);


  dmMain.ProjectID:=dmLink_Calc.Params.ProjectID;

  if dmLink_Calc.Params.Validate then
    dmLink_Calc.Execute (dmLink_Calc.Params.LinkID);

(*
  dmLink_Calc.Params.IsUsePassiveElements:=oParams.UsePassiveElements;
  dmLink_Calc.Params.IsCalcWithAdditionalRain:=oParams.IsCalcWithAdditionalRain;
  dmLink_Calc.Params.IsSaveReportToDB:=oParams.IsSaveReportToDB;
*)


(*ShowMessage('107');
  dmLink_Calc.Execute (107);
*)

  FreeAndNil(dmLink_Calc);


end;




// ---------------------------------------------------------------
function DllGetInterface(const IID: TGUID; var Obj): HResult;
// ---------------------------------------------------------------
begin
  Result:=S_FALSE; Integer(Obj):=0;

  if IsEqualGUID(IID, ILink_calcX) then
  begin
    with TLink_calcX.Create() do
      if GetInterface(IID,OBJ) then
        Result:=S_OK
  end else
    raise Exception.Create('if IsEqualGUID(IID, ILink_calcX)');


end;


begin
 // RegisterClass(TdmMain_bpl);

end.


