unit u_ini_LinkFreqPlan;

interface
uses
   IniFiles, SysUtils, Dialogs,

   u_files,
   u_func
   ;

type


  TIni_LinkFreqPlan_Params = class(TObject)
  public
    Project_ID: Integer;

    ConnectionString: string;
    Geometry_Str: string;

    //-----circle---------
    Lat,Lon : double;
    Radius_m  : Integer;

    Result: record
      LinkNet_ID : integer;
    end;

//    Lat_min : double;
//    Lon_min : double;
//    Lat_max: double;
//    Lon_max : double;


    procedure LoadFromFile(aFileName: string);
    procedure SaveToFile(aFileName: string);

  end;




implementation


// ---------------------------------------------------------------
procedure TIni_LinkFreqPlan_Params.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
const
  DEF_main = 'main';
var
  oIniFile: TIniFile;                                                                                            
begin

  Assert(FileExists(aFileName), aFileName);

  oIniFile := TIniFile.Create(aFileName);

  with oIniFile do
  begin
    Project_ID        :=ReadInteger (DEF_main, 'Project_ID', 0);

    ConnectionString := ReadString (DEF_main, 'ConnectionString', '' );
    Geometry_Str     := ReadString (DEF_main, 'Geometry_Str', '' );

    Lat  :=ReadFloat (DEF_main, 'Lat', 0);
    Lon  :=ReadFloat (DEF_main, 'Lon', 0);
    Radius_m  :=ReadInteger (DEF_main, 'Radius_m', 0);


    Result.LinkNet_ID:=ReadInteger (DEF_main, 'LinkNet_ID', 0);

    Assert (Project_ID>0);
    Assert (Project_ID>0);


  end;

  FreeAndNil(oIniFile);
end;

// ---------------------------------------------------------------
procedure TIni_LinkFreqPlan_Params.SaveToFile(aFileName: string);
// ---------------------------------------------------------------
const
  DEF_main = 'main';
var
  oIniFile: TIniFile;
  sFileName: string;
begin
//  FIniFileName

  sFileName:= ChangeFileExt(aFileName, '.ini');

  ForceDirByFileName(sFileName);

  DeleteFile(sFileName);


  oIniFile := TIniFile.Create(sFileName);

  with oIniFile do
  begin
    WriteInteger (DEF_main, 'Project_ID', Project_ID);
    WriteString (DEF_main, 'ConnectionString', ConnectionString );

    WriteString   (DEF_main, 'Geometry_Str', Geometry_Str);

    WriteFloat   (DEF_main, 'Lat', Lat);
    WriteFloat   (DEF_main, 'Lon', Lon);
    WriteInteger (DEF_main, 'Radius_m', Radius_m);



//    WriteInteger (DEF_main, 'LinkNet_ID', Result.LinkNet_ID);


//
//    WriteFloat (DEF_main, 'Lat_min', Lat_min );
//    WriteFloat (DEF_main, 'Lon_min', Lon_min );
//    WriteFloat (DEF_main, 'Lat_max', Lat_max );
//    WriteFloat (DEF_main, 'Lon_max', Lon_max );

  end;

  FreeAndNil(oIniFile);
end;



end.





