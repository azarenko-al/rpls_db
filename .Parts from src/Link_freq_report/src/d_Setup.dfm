object dlg_Setup: Tdlg_Setup
  Left = 1216
  Top = 138
  BorderStyle = bsDialog
  Caption = 'dlg_Setup'
  ClientHeight = 522
  ClientWidth = 558
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    558
    522)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 470
    Width = 543
    Height = 5
    Anchors = [akLeft, akRight]
    Shape = bsTopLine
    ExplicitTop = 400
    ExplicitWidth = 537
  end
  object Label1: TLabel
    Left = 5
    Top = 8
    Width = 32
    Height = 13
    Caption = #1060#1072#1081#1083':'
  end
  object FilenameEdit1: TFilenameEdit
    Left = 5
    Top = 24
    Width = 542
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    NumGlyphs = 1
    TabOrder = 0
    Text = 'c:\report.doc'
    ExplicitWidth = 536
  end
  object Button4: TButton
    Left = 8
    Top = 492
    Width = 129
    Height = 25
    Action = act_DesignReport
    Anchors = [akLeft, akBottom]
    TabOrder = 1
  end
  object cxVerticalGrid1: TcxVerticalGrid
    Left = 5
    Top = 64
    Width = 542
    Height = 113
    Anchors = [akLeft, akTop, akRight]
    LookAndFeel.Kind = lfUltraFlat
    OptionsView.ShowEditButtons = ecsbAlways
    OptionsView.PaintStyle = psDelphi
    OptionsView.RowHeaderWidth = 198
    OptionsBehavior.ImmediateEditor = False
    TabOrder = 2
    Version = 1
    object row_Template_ID: TcxEditorRow
      Properties.Caption = #1064#1072#1073#1083#1086#1085
      Properties.EditPropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.EditProperties.DropDownListStyle = lsFixedList
      Properties.EditProperties.DropDownRows = 30
      Properties.EditProperties.ImmediatePost = True
      Properties.EditProperties.KeyFieldNames = 'id'
      Properties.EditProperties.ListColumns = <
        item
          FieldName = 'name'
        end>
      Properties.EditProperties.ListOptions.AnsiSort = True
      Properties.EditProperties.ListOptions.ShowHeader = False
      Properties.EditProperties.ListSource = dmLink_freq_plan.ds_Reports
      Properties.DataBinding.ValueType = 'Integer'
      Properties.Value = Null
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxVerticalGrid1CategoryRow4: TcxCategoryRow
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object row_IsOpenReportAfterDone: TcxEditorRow
      Properties.Caption = #1054#1090#1082#1088#1099#1090#1100' '#1087#1086#1089#1083#1077' '#1092#1086#1088#1084#1080#1088#1086#1074#1072#1085#1080#1103
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.Alignment = taLeftJustify
      Properties.EditProperties.UseAlignmentWhenInplace = True
      Properties.DataBinding.ValueType = 'Boolean'
      Properties.Value = True
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxVerticalGrid1CategoryRow1: TcxCategoryRow
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object row_FileType: TcxEditorRow
      Properties.Caption = #1060#1086#1088#1084#1072#1090' '#1092#1072#1081#1083#1072
      Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
      Properties.EditProperties.Columns = 3
      Properties.EditProperties.ImmediatePost = True
      Properties.EditProperties.Items = <
        item
          Caption = 'Word'
          Value = 'word'
        end
        item
          Caption = 'PDF'
          Value = 'pdf'
        end
        item
          Caption = 'Excel'
          Value = 'excel'
        end>
      Properties.EditProperties.OnEditValueChanged = row_FileTypeEditPropertiesEditValueChanged
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = 'word'
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
  end
  object Panel1: TPanel
    Left = 380
    Top = 492
    Width = 177
    Height = 29
    Anchors = [akRight, akBottom]
    BevelOuter = bvNone
    TabOrder = 3
    object btn_Ok: TButton
      Left = 6
      Top = 0
      Width = 75
      Height = 25
      Caption = 'Ok'
      ModalResult = 1
      TabOrder = 0
    end
    object btn_Cancel: TButton
      Left = 86
      Top = 0
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
  object Button5: TButton
    Left = 144
    Top = 492
    Width = 161
    Height = 25
    Action = act_Preview
    Anchors = [akLeft, akBottom]
    TabOrder = 4
  end
  object Button1: TButton
    Left = 8
    Top = 438
    Width = 169
    Height = 25
    Action = act_Test_Add
    Anchors = [akLeft, akBottom]
    TabOrder = 5
  end
  object Button2: TButton
    Left = 232
    Top = 368
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 6
    Visible = False
    OnClick = Button2Click
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\Forms\'
    Options = []
    UseRegistry = True
    StoredProps.Strings = (
      'FilenameEdit1.FileName')
    StoredValues = <>
    Left = 104
    Top = 216
  end
  object ActionList1: TActionList
    Left = 32
    Top = 216
    object act_DesignReport: TAction
      Caption = 'DesignReport'
      OnExecute = act_DesignReportExecute
    end
    object act_Copy: TAction
      Caption = 'act_Copy'
      OnExecute = act_DesignReportExecute
    end
    object act_Preview: TAction
      Caption = 'act_Preview'
      OnExecute = act_DesignReportExecute
    end
    object act_Test_Add: TAction
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1086#1090#1095#1077#1090#1099' '#1074' '#1073#1072#1079#1077
      ShortCut = 16468
      OnExecute = act_DesignReportExecute
    end
    object Action1: TAction
      Caption = 'Action1'
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 32
    Top = 280
  end
end
