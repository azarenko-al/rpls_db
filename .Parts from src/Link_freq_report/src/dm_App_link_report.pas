unit dm_App_link_report;

interface

uses
  Classes, Dialogs,  SysUtils,

  //u_Link_report_vars,

  I_Options,

  u_ini_Link_report_params,

  dm_Report,
  dm_Link_report,
  dm_LinkLine_report,
  dm_Link_report_xml,
  dm_Link_report_data,
  dm_Link_map,

  u_vars,

  u_func,

  dm_Main

  ;
  //dm_LinkLine_Complex_Report11,

type
  TdmMain_app__report = class(TDataModule)
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  public
  //  procedure UpdateReports;
  end;

var
  dmMain_app__report: TdmMain_app__report;


implementation
 {$R *.DFM}

//--------------------------------------------------------------
procedure TdmMain_app__report.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const
  DEF_TEMP_FILENAME = 'link_report.ini';

  REGISTRY_ROOT = 'Software\Onega\RPLS_DB\';

var
  sIniFileName: string;
  sReportFileName: string;

begin
  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
    sIniFileName:=g_ApplicationDataDir+ DEF_TEMP_FILENAME;

  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;

  // -------------------------------------------------------------------

  TdmMain.Init;
  if not dmMain.OpenDB_reg then    // True
    Exit;



  {$DEFINE use_dll111}

  {$IFDEF use_dll}

  if Load_ILink_report() then
  begin
    ILink_report.InitADO(dmMain.ADOConnection1.ConnectionObject);
 //   ILink_graph.InitAppHandle(Application.Handle);
    ILink_report.Execute(sFile);

    UnLoad_ILink_report();
  end;

  Exit;

  {$ENDIF}




  g_Link_Report_Param:=TIni_Link_Report_Param.Create;

  g_Link_Report_Param.LoadFromINI(sIniFileName);

 //zzzzzzzz LoadFromINI(sIniFileName, rec);

  TdmLink_report_data.Init;
//  TdmRel_Engine.Init;
  TdmLink_report.Init;
  TdmLinkLine_report.Init;
  TdmLink_map.Init;
  TdmLink_report_xml.Init;


(*  gParams.IsSaveDocsToDB  :=rec.IsSaveDocsToDB;
  gParams.IsShowReportDoc :=rec.IsShowReportDoc;

*)

  if (g_Link_Report_Param.FileDir='') or
     (g_Link_Report_Param.ProjectID=0) or
     (g_Link_Report_Param.ReportID=0)
  then begin
    ShowMessage('(FileDir='''') or (ProjectID=0) or (ReportID=0) ');
    Exit;
  end;

  // -------------------------------------------------------------------

 ////// ShowMessage(IntToStr(rec.ProjectID));


  dmMain.ProjectID:=g_Link_Report_Param.ProjectID;

  IOptionsX_Load;

 // IOptions:=Get_IOptionsX();


 /////////// dmLink_Map.Params.MapDesktopID:=g_ReportParams.MapDesktopID;

 // rec.Scale:=0;
(*

  if g_ReportParams.Scale=0 then
  begin
    sScale:='1 000 000';
    if InputQuery('����� ��������', '�������:', sScale) then
      g_ReportParams.Scale:= AsInteger( ReplaceStr (sScale, ' ','') )
    else
      Exit;

  end;*)


(*  dmLink_Map.Params.Scale       :=g_ReportParams.Scale;


  dmLink_Map.Params.ImageWidth  :=g_ReportParams.ImageWidth;
  dmLink_Map.Params.ImageHeight :=g_ReportParams.ImageHeight;
*)
 // dmLink_report.Params.IniFileName:=sIniFileName;
//  dmLink_report.Params.IsShowReflectionPoints:=rec.IsShowReflectionPoints;
 // dmLink_report.Params.Profile_IsShowFrenelZoneBottomLeftRight:=g_ReportParams.Profile_IsShowFrenelZoneBottomLeftRight;

  dmLinkLine_report.Params.IniFileName:=sIniFileName;



  dmLink_report.LinkIDList.LoadIDsFromIniFile (sIniFileName, 'links');
  dmLink_report.LinkLineIDList.LoadIDsFromIniFile (sIniFileName, 'LinkLines');

//   UpdateReports;


//  if iScale>0 then
 //   dmLink_map.Scale:=iScale;


  sReportFileName:=g_Link_Report_Param.FileDir + 'temp\'+ g_Link_Report_Param.ReportFileName;
  dmReport.SaveToFile (g_Link_Report_Param.ReportID, sReportFileName);

  dmLink_report_data.FileDir:=g_Link_Report_Param.FileDir;

  // ---------------------------------------------------------------

  CursorHourGlass;


  //-------------------------------------------------------------------
  if Eq(g_Link_Report_Param.Action,'LinkList_report') then
  //-------------------------------------------------------------------
  begin
   // dmLink_report.Params.ObjName_:=g_ReportParams.ObjName;
    dmLink_report.ReportTemplateFileName:=sReportFileName;
 //   dmLink_report.ExecuteProc;
    dmLink_report.ExecuteDlg();

//    dmLink_report.Execute_LinkList (); //sReportFileName)

  end  else

  //-------------------------------------------------------------------
  if Eq(g_Link_Report_Param.Action,'LinkLine_report') then
  //-------------------------------------------------------------------
  begin
    dmLinkLine_report.ReportTemplateFileName:=sReportFileName;

    if g_Link_Report_Param.LinkLineID>0 then
      dmLinkLine_report.Execute_LinkLine (g_Link_Report_Param.LinkLineID);         //,  sReportFileName

    if dmLink_report.LinkLineIDList.Count>0 then
      dmLinkLine_report.Execute_LinkLine (dmLink_report.LinkLineIDList[0].ID);         //,  sReportFileName
  end;


  //-------------------------------------------------------------------
  if Eq(g_Link_Report_Param.Action,'LinkNet_report') then
  //-------------------------------------------------------------------
   // dmLink_report.Execute_LinkNet (sReportFileName)
  else
    ;


  CursorDefault;


 // Tfrm_Documents.ExecDlg;

 // dmLink_report.ExecuteTest (iID);

end;


procedure TdmMain_app__report.DataModuleDestroy(Sender: TObject);
var
  I: integer;
  s: string;
  s4: string;
  s3: string;
  s2: string;
  s1: string;

begin
  IOptionsX_UnLoad();

  FreeAndNil(g_Link_Report_Param);


(*
  I := Application.ComponentCount;


  for I := Application.ComponentCount - 1  downto 0 do
  //  if Application.Components[i] is TDataModule then
    begin
    //  ShowMessage(Application.Components[i].ClassName +'  '+ Application.Components[i].Name);

      s1:=Application.Components[i].Name;
      s2:=Application.Components[i].ClassName;

      s3:=Application.Components[i].Owner.Name;
      s4:=Application.Components[i].Owner.className;

   //   (Application.Components[i]).Free;

    //     ShowMessage(Application.Components[i].ClassName +'  '+ Application.Components[i].Name);

    end;
    *)



(*
  FreeAndNil(dmLink_report_data);
  FreeAndNil(dmLink_report);
  FreeAndNil(dmLinkLine_report);
  FreeAndNil(dmLink_map);
  FreeAndNil(dmLink_report_xml);
  FreeAndNil(dmRel_Engine);

  FreeAndNil(dmMain);

  inherited;*)

end;


end.


{



procedure TdmMain_app__report.UpdateReports;
begin
{  dmReport.LoadFromFile (23, 't:\reports\�� ��������- �������� ������ ��� ������� ������������.doc');
  dmReport.LoadFromFile (11, 't:\reports\�� ��������- ������ �����.doc');
  dmReport.LoadFromFile (10, 't:\reports\�� ��������- ����������� �����.doc');

  dmReport.LoadFromFile (12, 't:\reports\�� �����- ����������� �����.xsl');
}
end;


 // Tfrm_Documents.ExecDlg;


{
  dmLink_report_data.Test;
  Exit;
}

{
  dmMain.ProjectID:=478;

  dmLink_map.Property_SaveToImage_all ();
  Exit;
}

