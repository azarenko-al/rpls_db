object dmMain_Data1111: TdmMain_Data1111
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 1158
  Top = 807
  Height = 446
  Width = 670
  object qry_Projects: TADOQuery
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'SELECT id,name FROM PROJECT ORDER BY name')
    Left = 50
    Top = 15
  end
  object ds_Projects: TDataSource
    DataSet = qry_Projects
    Left = 52
    Top = 87
  end
  object qry_Prop_max_h: TADOQuery
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'SELECT id,name FROM PROJECT ORDER BY name')
    Left = 170
    Top = 15
  end
  object qry_Prop: TADOQuery
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'SELECT id,name FROM PROJECT ORDER BY name')
    Left = 274
    Top = 15
  end
  object ADOConnection_SQL: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=C:\ON' +
      'EGA\RPLS_DB\bin\excel_to_DB.mdb;Mode=Share Deny None;Persist Sec' +
      'urity Info=False;Jet OLEDB:System database="";Jet OLEDB:Registry' +
      ' Path="";Jet OLEDB:Database Password="";Jet OLEDB:Engine Type=5;' +
      'Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Global Partial Bulk ' +
      'Ops=2;Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:New Databas' +
      'e Password="";Jet OLEDB:Create System Database=False;Jet OLEDB:E' +
      'ncrypt Database=False;Jet OLEDB:Don'#39't Copy Locale on Compact=Fal' +
      'se;Jet OLEDB:Compact Without Replica Repair=False;Jet OLEDB:SFP=' +
      'False;'
    ConnectionTimeout = 5
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 229
    Top = 219
  end
  object t_Projects: TADOTable
    Connection = ADOConnection_SQL
    CursorType = ctStatic
    Left = 344
    Top = 216
  end
  object DataSource1: TDataSource
    DataSet = t_Projects
    Left = 344
    Top = 264
  end
end
