unit dm_Property_map;

interface
{$I ver.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Dialogs, Variants,
  Controls, Forms, Db, ADODB, MapXLib_TLB,

  dm_Custom,

  I_Options,

  d_MIF_export,

  u_Mapinfo_WOR,

  u_classes,
  u_func,
  u_db,
  u_img,
  u_files,
  u_GEO,
  u_MapX,
  u_mapx_func,
  u_mitab,

  u_types, //used for I_Options

  u_const,
  u_const_db,

//  dm_LinkEnd,
 // dm_LinkLine,
  //dm_Link,
  dm_Map_Desktop
  ;

type
  TdmProperty_map = class(TdmCustom)
    qry_Properties: TADOQuery;
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FMap: TMap;
//    FMIMap: IMapX;

    FStyle: TmiStyle;

    procedure AssignLineStyle (aLineStyle: TOptionsObjectStyleRec);
    procedure AssignFontStyle (aFontStyle: TOptionsObjectStyleRec);

  public
    Params: record
      MapDesktopID: integer;
      Scale: integer;

      ImageWidth: integer;
      ImageHeight: integer;

    end;

//    ProperList: TStringList;


    class procedure Init;

    procedure InsertMaps;
    procedure InitMap;
    procedure SaveToImageFile(aID: integer; aFileName: string);

  end;


//function dmLink_map: TdmLink_map;

var
  dmProperty_map: TdmProperty_map;



//====================================================================
implementation {$R *.dfm}
//===================================================================

{const
  IMAGE_WIDTH =19; // in sm
  IMAGE_HEIGHT=12;

}

class procedure TdmProperty_map.Init;
begin
  if not Assigned(dmProperty_map) then
    dmProperty_map := TdmProperty_map.Create(Application);
end;

(*

// ---------------------------------------------------------------
function dmLink_map: TdmLink_map;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLink_map) then
    FdmLink_map := TdmLink_map.Create(Application);

  Result := FdmLink_map;
end;

*)
//------------------------------------------------------
procedure TdmProperty_map.InsertMaps;
//------------------------------------------------------
var
  i: integer;
  sPath: string;
//  iCurDesktop: integer;
  map_rec: TMapRegRec;
  map_view_rec: TMapViewAddRec;
  oCheckedFileList: TStringList;
  vLayer: CMapXLayer;
begin
  oCheckedFileList:= TStringList.Create;


 // iCurDesktop := dmMap_Desktop.GetDefaultDesktop;
  sPath := dmMap_Desktop.GetMapDesktopRegistryRoot(Params.MapDesktopID);

     //GetCurrentDesktopByType('link', aID);

  dmMap_Desktop.GetMapsCheckedList (Params.MapDesktopID, oCheckedFileList);

  for i := 0 to oCheckedFileList.Count-1 do
    if FileExists(oCheckedFileList[i]) then
    try
    // z  vLayer:=FMap.Layers.Add (oCheckedFileList[i], EmptyParam);

 //      mapx_LabelProperties_LoadFromReg (vLayer.LabelProperties, aRec.LabelProperties_RegPath);


 {   if aRec.ObjectName<>'' then
    aRec.LabelProperties_RegPath:=REGISTRY_MapObjectsLabels +  aRec.ObjectName
  else
    aRec.LabelProperties_RegPath:=REGISTRY_MapObjectsLabels + ExtractFileName(aRec.FileName);

  u_MapX_func.map_MapAdd (Map1, aRec);
}


      dmMap_Desktop.Desktop_GetMapParams (sPath,
                                          Integer(oCheckedFileList.Objects[i]),
                                          oCheckedFileList[i],
                                          map_rec);
      if not map_rec.Checked then
        Continue;


      FillChar(map_view_rec, SizeOf(map_view_rec), 0);

      map_view_rec.FileName:=oCheckedFileList[i];
      map_view_rec.AutoLabel:=map_rec.AutoLabel;
      map_view_rec.LabelProperties_RegPath:=REGISTRY_MapObjectsLabels + ExtractFileName(map_view_rec.FileName);


      u_mapx_func.map_MapAdd(FMap, map_view_rec);

      // dmMap_Desktop.

      // vLayer.AutoLabel:= (Pos('city', oCheckedFileList[i]) > 0);
    //   vLayer.LabelProperties.Style.TextFontHalo:= true;
    except
    end;

  // -------------------------------------------------------------------
  oCheckedFileList.Free;

end;


procedure TdmProperty_map.DataModuleCreate(Sender: TObject);
begin
  inherited;
  TdmMap_Desktop.Init;
end;


procedure TdmProperty_map.InitMap;
begin
  if FMap=nil then
    FMap:=TMap.Create(nil);
end;


procedure TdmProperty_map.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FMap);

  inherited;
end;






//--------------------------------------------------------------------
procedure TdmProperty_map.SaveToImageFile(aID: integer; aFileName: string);
//--------------------------------------------------------------------
var
  sPath: string;
  sGeoFileName: string;
  i, iCurDesktop: Integer;
  oPropIDList: TIDList;
  blVector: TBLVector;
  iLinkEnd1, iLinkEnd2, iProp1, iProp2: integer;

  oCheckedFileList: TStringList;

  sTabFileName, sFile: string;

  vLayer: CMapXLayer;

  oMap: TmitabMap;


  rec: TExportMapRec;

  map_rec: TMapRegRec;

  map_view_rec: TMapViewAddRec;

begin
  InitMap();

try

  CursorHourGlass;

  oPropIDList:=TIDList.Create;
  oCheckedFileList:= TStringList.Create;

  sTabFileName:=ChangeFileExt(aFileName, '.tab');

  oMap:=TmitabMap.Create;

  oMap.CreateFile (sTabFileName,
                   [mapX_Field (FLD_ID,   miTypeInt),
                    mapX_Field (FLD_NAME, miTypeString)
                   ]);

  oPropIDList.AddID(iProp1);
  oPropIDList.AddID(iProp2);


  for i := 0 to oPropIDList.Count-1 do
  begin
    db_OpenTableByID(qry_Temp, TBL_PROPERTY, oPropIDList[i].ID);

    AssignFontStyle (IOptions.GetDefaultStyle(OBJ_Property));


    if i = 0 then
      oMap.WriteSymbol (blVector.Point1, FStyle,
                   [mapx_Par(FLD_ID,   qry_Temp[FLD_ID]),
                    mapx_Par(FLD_NAME, qry_Temp[FLD_NAME]) ])
    else
      oMap.WriteSymbol (blVector.Point2, FStyle,
                   [mapx_Par(FLD_ID,   qry_Temp[FLD_ID]),
                    mapx_Par(FLD_NAME, qry_Temp[FLD_NAME]) ]);

  end;

  oMap.Free;


  FMap.Layers.RemoveAll;

//  if FileExists(sTabFileName) then
//  begin
  vLayer:= FMap.Layers.Add (sTabFileName, EmptyParam);

  dmMap_Desktop.ApplyLabelProperties(vLayer, 'property.tab');
  vLayer.LabelProperties.Overlap:=  true;
//  vLayer.LabelProperties.Position:= 2;
  vLayer.AutoLabel:= True;


  InsertMaps();

{  iCurDesktop := dmMap_Desktop.GetDefaultDesktop;
  sPath       := dmMap_Desktop.GetMapDesktopRegistryRoot (iCurDesktop);

     //GetCurrentDesktopByType('link', aID);

  dmMap_Desktop.GetMapsCheckedList (iCurDesktop, oCheckedFileList);

  for i := 0 to oCheckedFileList.Count-1 do
    if FileExists(oCheckedFileList[i]) then
    try
       vLayer:=FMap.Layers.Add (oCheckedFileList[i], EmptyParam);
}
 //      mapx_LabelProperties_LoadFromReg (vLayer.LabelProperties, aRec.LabelProperties_RegPath);


 {   if aRec.ObjectName<>'' then
    aRec.LabelProperties_RegPath:=REGISTRY_MapObjectsLabels +  aRec.ObjectName
  else
    aRec.LabelProperties_RegPath:=REGISTRY_MapObjectsLabels + ExtractFileName(aRec.FileName);

  u_MapX_func.map_MapAdd (Map1, aRec);
}


 {     dmMap_Desktop.Desktop_GetMapParams  (sPath,
                                          Integer(oCheckedFileList.Objects[i]),
                                          oCheckedFileList[i],
                                          map_rec);

      FillChar(map_view_rec, SizeOf(map_view_rec), 0);

      map_view_rec.FileName:=oCheckedFileList[i];
      map_view_rec.AutoLabel:=map_rec.AutoLabel;
      map_view_rec.LabelProperties_RegPath:=REGISTRY_MapObjectsLabels + ExtractFileName(map_view_rec.FileName);


      u_mapx_func.map_MapAdd(FMap, map_view_rec);

      // dmMap_Desktop.

      // vLayer.AutoLabel:= (Pos('city', oCheckedFileList[i]) > 0);
    //   vLayer.LabelProperties.Style.TextFontHalo:= true;
    except
    end;
}
  // -------------------------------------------------------------------

  sGeoFileName := GetTempFileName_('gst');
  FMap.SaveMapAsGeoset('',sGeoFileName);

  u_Mapinfo_WOR.mapx_SaveToWOR(FMap, ChangeFileExt(aFileName, '.wor'));

//  FMap.SaveMapAsGeoset('',sGeoFileName);


  FMap.Layers.RemoveAll;


  rec.Scale:=Params.Scale;
  rec.GeosetFileName:=sGeoFileName;
  rec.ImgFileName:=aFileName;
  rec.BLVector:=blVector;

  rec.Width:=Params.ImageWidth;
  rec.Height:=Params.ImageHeight;

{  rec.Width:=IMAGE_WIDTH;
  rec.Height:=IMAGE_HEIGHT;
}
  rec.Mode:=mtVEctor;


//  , aFileName, blVector, IMAGE_WIDTH, IMAGE_HEIGHT

  MIF_ExportMap (rec);

  DeleteFile(sGeoFileName);

  // -------------------------------------------------------------------

  img_InsertScaleRuleIntoBMP (aFileName, aFileName, Params.Scale);


  FreeAndNil(oCheckedFileList);
  FreeAndNil(oPropIDList);


  finally
    CursorDefault;
  end;

end;



procedure TdmProperty_map.AssignLineStyle(aLineStyle: TOptionsObjectStyleRec);
begin
  FStyle:=aLineStyle.MIStyle;
end;


procedure TdmProperty_map.AssignFontStyle(aFontStyle: TOptionsObjectStyleRec);
begin
  FStyle:=aFontStyle.MIStyle;
end;



begin
//  TdmLink_map.Init;

end.


