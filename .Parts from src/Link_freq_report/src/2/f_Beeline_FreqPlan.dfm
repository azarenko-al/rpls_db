object frm_Beeline_FreqPlan: Tfrm_Beeline_FreqPlan
  Left = 454
  Top = 192
  Width = 931
  Height = 655
  Caption = 'frm_Beeline_FreqPlan'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid2: TDBGrid
    Left = 8
    Top = 8
    Width = 217
    Height = 313
    DataSource = ds_LInks
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 534
    Width = 907
    Height = 165
    ActivePage = TabSheet2
    Align = alBottom
    TabOrder = 1
    object TabSheet3: TTabSheet
      Caption = 'TabSheet3'
      ImageIndex = 2
      object DBGrid4: TDBGrid
        Left = 0
        Top = 0
        Width = 899
        Height = 194
        Align = alTop
        DataSource = ds_LInks
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 994
        Height = 97
        Align = alTop
        DataSource = ds_property1
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
      object DBGrid3: TDBGrid
        Left = 0
        Top = 97
        Width = 994
        Height = 96
        Align = alTop
        DataSource = ds_property2
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      object DBGrid5: TDBGrid
        Left = 0
        Top = 0
        Width = 899
        Height = 97
        Align = alTop
        DataSource = ds_Linkend1
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
      object DBGrid6: TDBGrid
        Left = 0
        Top = 97
        Width = 899
        Height = 96
        Align = alTop
        DataSource = ds_Linkend2
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
  end
  object Button1: TButton
    Left = 248
    Top = 129
    Width = 113
    Height = 25
    Caption = 'ShowReport'
    TabOrder = 2
    OnClick = Button1Click
  end
  object cxDBTreeList1: TcxDBTreeList
    Left = 560
    Top = 448
    Width = 250
    Height = 86
    Bands = <
      item
      end>
    RootValue = -1
    TabOrder = 3
    object cxDBTreeList1cxDBTreeListColumn1: TcxDBTreeListColumn
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1cxDBTreeListColumn2: TcxDBTreeListColumn
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1cxDBTreeListColumn3: TcxDBTreeListColumn
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1cxDBTreeListColumn4: TcxDBTreeListColumn
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1cxDBTreeListColumn5: TcxDBTreeListColumn
      Position.ColIndex = 4
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=SQL_ONEGA_111111'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 264
    Top = 16
  end
  object qry_property1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = ds_LInks
    Parameters = <
      item
        Name = 'property1_id'
        DataType = ftInteger
        Precision = 10
        Value = 1075627
      end>
    SQL.Strings = (
      'select * from property'
      ''
      'where (id=:property1_id)'
      '')
    Left = 472
    Top = 16
  end
  object ds_property1: TDataSource
    DataSet = qry_property1
    Left = 472
    Top = 80
  end
  object ds_LInks: TDataSource
    DataSet = qry_LInks_new
    Left = 368
    Top = 72
  end
  object frxReport: TfrxReport
    Version = '4.15.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.ShowCaptions = True
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41121.613084722200000000
    ReportOptions.LastChange = 41892.473604780090000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    Left = 336
    Top = 184
    Datasets = <
      item
        DataSet = frxDBDataset_link
        DataSetName = 'link'
      end
      item
        DataSet = frxDBDataset_q_Linkend1
        DataSetName = 'Linkend1'
      end
      item
        DataSet = frxDBDataset_q_Linkend2
        DataSetName = 'Linkend2'
      end
      item
        DataSet = frxDBDataset_property1
        DataSetName = 'property1'
      end
      item
        DataSet = frxDBDataset_property2
        DataSetName = 'property2'
      end>
    Variables = <>
    Style = <
      item
        Name = 'Title'
        Color = clGray
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Header'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Group header'
        Color = 16053492
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Data'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
      end
      item
        Name = 'Group footer'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Header line'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 2.000000000000000000
      end>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 600.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 256
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      PrintIfEmpty = False
      object ReportTitle1: TfrxReportTitle
        Height = 45.354360000000000000
        Top = 16.000000000000000000
        Width = 2192.127400000000000000
        StartNewPage = True
        object Memo8: TfrxMemoView
          Align = baWidth
          Width = 2192.127400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#1119#1057#1026#1056#1109#1056#181#1056#1108#1057#8218' '#1057#8225#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#1029#1056#1109'-'#1057#8218#1056#181#1057#1026#1057#1026#1056#1105#1057#8218#1056#1109#1057#1026#1056#1105#1056#176#1056#187#1057#1034#1056#1029#1056#1109#1056#1110#1056#1109' '#1056 +
              #1111#1056#187#1056#176#1056#1029#1056#176'  '#1056#1029#1056#176' '#1057#8218#1056#181#1057#1026#1057#1026#1056#1105#1057#8218#1056#1109#1057#1026#1056#1105#1056#1105)
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        Height = 120.944923390000000000
        Top = 224.000000000000000000
        Width = 2192.127400000000000000
        DataSet = frxDBDataset_link
        DataSetName = 'link'
        RowCount = 0
        object property1name: TfrxMemoView
          Align = baLeft
          Left = 75.590600000000000000
          Width = 162.519790000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'name'
          DataSet = frxDBDataset_property1
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property1."name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2name: TfrxMemoView
          Align = baLeft
          Left = 75.590600000000000000
          Top = 60.472479999999960000
          Width = 162.519790000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'name'
          DataSet = frxDBDataset_property2
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property2."name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line: TfrxMemoView
          Width = 30.236240000000000000
          Height = 120.944923390000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Line]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property1address: TfrxMemoView
          Align = baLeft
          Left = 238.110390000000000000
          Width = 226.771653543307100000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'address'
          DataSet = frxDBDataset_property1
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property1."address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2address: TfrxMemoView
          Align = baLeft
          Left = 238.110390000000000000
          Top = 60.472479999999960000
          Width = 226.771653543307100000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'address'
          DataSet = frxDBDataset_property2
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property2."address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1LinkEndTypename: TfrxMemoView
          Align = baLeft
          Left = 691.653697086614000000
          Width = 132.283464570000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'LinkEndType.name'
          DataSet = frxDBDataset_q_Linkend1
          DataSetName = 'Linkend1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."LinkEndType.name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          Left = 691.653697086614000000
          Top = 60.472440940000010000
          Width = 132.344510160000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'LinkEndType.name'
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."LinkEndType.name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1antenna_height: TfrxMemoView
          Align = baLeft
          Left = 1065.826969376614000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'antenna_height'
          DataSet = frxDBDataset_q_Linkend1
          DataSetName = 'Linkend1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."antenna_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          Left = 1065.888014966614000000
          Top = 60.472479999999960000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend1
          DataSetName = 'Linkend1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."antenna_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2rx_freq_MHz: TfrxMemoView
          Align = baLeft
          Left = 823.998207246614000000
          Top = 60.472479999999960000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'rx_freq_MHz'
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          Left = 823.937161656614000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_polarization: TfrxMemoView
          Align = baLeft
          Left = 1544.376250333700000000
          Width = 53.291338582677170000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."antenna_polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Align = baLeft
          Left = 1544.376250333700000000
          Top = 60.472479999999960000
          Width = 53.291338582677170000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'antenna_polarization'
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."antenna_polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_diameter: TfrxMemoView
          Align = baLeft
          Left = 1491.084911751023000000
          Width = 53.291338582677170000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."antenna_diameter"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Align = baLeft
          Left = 1491.084911751023000000
          Top = 60.472479999999960000
          Width = 53.291338582677170000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'antenna_diameter'
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."antenna_diameter"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          Left = 899.527712836614000000
          Width = 49.133858270000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."antenna_horz_width"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Align = baLeft
          Left = 899.588758426614000000
          Top = 60.472479999999960000
          Width = 49.133858270000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."antenna_horz_width"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          Left = 948.661571106614000000
          Width = 49.133858270000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."antenna_vert_width"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Align = baLeft
          Left = 948.722616696614000000
          Top = 60.472479999999960000
          Width = 49.133858270000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."antenna_vert_width"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          Left = 997.795429376614000000
          Width = 68.031540000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend1
          DataSetName = 'Linkend1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."antenna_gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          Left = 997.856474966614100000
          Top = 60.472479999999960000
          Width = 68.031540000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend1
          DataSetName = 'Linkend1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."antenna_gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object linklength_km: TfrxMemoView
          Align = baLeft
          Left = 1251.084865366614000000
          Width = 52.913385830000000000
          Height = 120.944960000000000000
          ShowHint = False
          DataField = 'length_km'
          DataSet = frxDBDataset_link
          DataSetName = 'link'
          DisplayFormat.FormatStr = '%2.1f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."length_km"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1power_dBm: TfrxMemoView
          Align = baLeft
          Left = 1303.998251196614000000
          Width = 52.913385830000000000
          Height = 120.944960000000000000
          ShowHint = False
          DataField = 'power_dBm'
          DataSet = frxDBDataset_q_Linkend1
          DataSetName = 'Linkend1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Linkend1."power_dBm"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2radiation_class: TfrxMemoView
          Align = baLeft
          Left = 1356.911637026614000000
          Top = 60.472479999999960000
          Width = 71.811070000000000000
          Height = 60.472480000000000000
          ShowHint = False
          DataField = 'radiation_class'
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Align = baLeft
          Left = 1356.911637026614000000
          Width = 71.811070000000000000
          Height = 60.472480000000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2bitrate_Mbps: TfrxMemoView
          Align = baLeft
          Left = 1428.722707026614000000
          Top = 60.472479999999960000
          Width = 62.362204724409450000
          Height = 60.472480000000000000
          ShowHint = False
          DataField = 'bitrate_Mbps'
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."bitrate_Mbps"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Align = baLeft
          Left = 1428.722707026614000000
          Width = 62.362204724409450000
          Height = 60.472480000000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."bitrate_Mbps"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Align = baLeft
          Left = 1597.667588916377000000
          Width = 102.047310000000000000
          Height = 120.944911180000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Align = baLeft
          Left = 1699.714898916377000000
          Width = 49.133890000000000000
          Height = 120.944911180000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
          VAlign = vaCenter
        end
        object property2lat_str: TfrxMemoView
          Align = baLeft
          Left = 464.882043543307100000
          Top = 60.472479999999960000
          Width = 113.385826771653500000
          Height = 60.472480000000000000
          ShowHint = False
          DataField = 'lat_str'
          DataSet = frxDBDataset_property2
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[property2."lat_str"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Align = baLeft
          Left = 464.882043543307100000
          Width = 113.385826771653500000
          Height = 60.472480000000000000
          ShowHint = False
          DataSet = frxDBDataset_property2
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[property1."lat_str"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Align = baLeft
          Left = 578.267870314960500000
          Top = 60.472479999999960000
          Width = 113.385826771653500000
          Height = 60.472480000000000000
          ShowHint = False
          DataSet = frxDBDataset_property2
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[property2."lon_str"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Align = baLeft
          Left = 578.267870314960500000
          Width = 113.385826771653500000
          Height = 60.472480000000000000
          ShowHint = False
          DataSet = frxDBDataset_property2
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[property1."lon_str"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Align = baLeft
          Left = 30.236240000000000000
          Width = 45.354360000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_property1
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property1."code"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Align = baLeft
          Left = 30.236240000000000000
          Top = 60.472480000000080000
          Width = 45.354360000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_property1
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property2."code"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_ground_height: TfrxMemoView
          Align = baLeft
          Left = 1141.417520556614000000
          Width = 56.692913390000000000
          Height = 60.472480000000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."antenna_ground_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Align = baLeft
          Left = 1141.478566146614000000
          Top = 60.472479999999960000
          Width = 56.692913390000000000
          Height = 60.472480000000000000
          ShowHint = False
          DataField = 'antenna_ground_height'
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."antenna_ground_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Align = baLeft
          Left = 1198.110433946614000000
          Width = 52.913385830000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."antenna_azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Align = baLeft
          Left = 1198.171479536614000000
          Top = 60.472479999999960000
          Width = 52.913385830000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."antenna_azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        Height = 75.590551180000000000
        Top = 124.000000000000000000
        Width = 2192.127400000000000000
        object Memo3: TfrxMemoView
          Align = baLeft
          Left = 238.110390000000000000
          Width = 226.771653543307100000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1106#1056#1169#1057#1026#1056#181#1057#1027)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Align = baLeft
          Left = 948.661571103779500000
          Width = 49.133858270000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8221#1056#1116#1056#1106
            #1056#1030#1056#181#1057#1026#1057#8218'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Align = baLeft
          Left = 899.527712836063000000
          Width = 49.133858267716500000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8221#1056#1116#1056#1106
            #1056#1110#1056#1109#1057#1026'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          Width = 30.236240000000000000
          Height = 75.590551181102400000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1074#8222#8211
            #1056#1111'/'#1056#1111)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          Left = 464.882043543307100000
          Width = 113.385826771653500000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1025#1056#1105#1057#1026#1056#1109#1057#8218#1056#176','
            #1057#1027'.'#1057#8364'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          Left = 691.653697084960500000
          Width = 132.283464570000000000
          Height = 75.590551181102400000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '
            #1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          Left = 823.937161654960500000
          Width = 75.590551181102400000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#152#1056#1029#1056#1169#1056#181#1056#1108#1057#1027
            '('#1056#1029#1056#1109#1056#1112#1056#1105#1056#1029#1056#176#1056#187#1057#8249' '#1057#8225#1056#176#1057#1027#1057#8218#1056#1109#1057#8218'), '#1056#1114#1056#8220#1057#8224)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Align = baLeft
          Left = 578.267870314960500000
          Width = 113.385826770000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8221#1056#1109#1056#187#1056#1110#1056#1109#1057#8218#1056#176','
            #1056#1030'.'#1056#1169'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          Left = 30.236240000000000000
          Width = 45.354360000000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1074#8222#8211
            #1056#177'/'#1057#1027)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          Left = 997.795429373779500000
          Width = 68.031496062992100000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1113#1057#1107','
            #1056#1169#1056#8216)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          Left = 1065.826925436772000000
          Width = 75.590551180000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '
            #1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176' '
            #1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249','
            ' '#1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          Left = 1542.819096828819000000
          Width = 53.291338582677170000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          Left = 1489.527758246141000000
          Width = 53.291338582677170000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112'.'
            #1056#176#1056#1029#1057#8218'., '#1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          Left = 1251.023775832598000000
          Width = 52.913385826771700000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#160#1056#176#1057#1027#1057#1027#1057#8218'., '#1056#1108#1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          Left = 1303.937161659370000000
          Width = 52.913385826771700000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240'-'
            #1056#1029#1056#1109#1057#1027#1057#8218#1057#1034', '
            #1056#1169#1056#8216#1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          Left = 1356.850547486142000000
          Width = 70.315006040000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1113#1056#187#1056#176#1057#1027#1057#1027
            #1056#1105#1056#183#1056#187#1057#1107#1057#8225#1056#181#1056#1029#1056#1105#1057#1039)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Align = baLeft
          Left = 1427.165553526142000000
          Width = 62.362204720000000000
          Height = 75.590551180000010000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#1109#1057#8218#1056#1109#1056#1108#1056#176', M'#1056#8216#1056#1105#1057#8218'/'#1057#1027)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Align = baLeft
          Left = 1596.110435411496000000
          Width = 104.173295620000000000
          Height = 75.590551181102400000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#164#1056#152#1056#1115' '
            #1057#1027#1056#1169#1056#181#1056#187#1056#176#1056#1030#1057#8364#1056#181#1056#1110#1056#1109' '
            #1056#183#1056#176#1056#1111#1056#1105#1057#1027#1057#1034)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Align = baLeft
          Left = 1700.283731031496000000
          Width = 49.133890000000000000
          Height = 75.590551181102400000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#1112'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Align = baLeft
          Left = 75.590600000000000000
          Width = 162.519790000000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1116#1056#176#1056#183#1056#1030#1056#176#1056#1029#1056#1105#1056#181' ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Align = baLeft
          Left = 1141.417476616771000000
          Width = 56.692913385826800000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'H '#1056#176#1056#1029#1057#8218'.'
            #1056#1109#1057#8218' '#1057#1107#1057#1026'. '#1056#1112#1056#1109#1057#1026#1057#1039', '
            #1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Align = baLeft
          Left = 1198.110390002598000000
          Width = 52.913385830000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218', '#1056#1110#1057#1026#1056#176#1056#1169'.')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDBDataset_link: TfrxDBDataset
    UserName = 'link'
    CloseDataSource = False
    DataSource = ds_LInks
    BCDToCurrency = False
    Left = 840
    Top = 176
  end
  object frxRTFExport1: TfrxRTFExport
    FileName = 'D:\222222222222333233333444222222.rtf'
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    CreationTime = 41141.726405983800000000
    DataOnly = False
    PictureType = gpPNG
    OpenAfterExport = True
    Wysiwyg = True
    Creator = 'FastReport'
    SuppressPageHeadersFooters = False
    HeaderFooterMode = hfText
    AutoSize = False
    Left = 288
    Top = 328
  end
  object qry_property2: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = ds_LInks
    Parameters = <
      item
        Name = 'property2_id'
        DataType = ftInteger
        Precision = 10
        Value = 1075508
      end>
    SQL.Strings = (
      'select * from property'
      ''
      'where (id=:property2_id)'
      '')
    Left = 552
    Top = 16
  end
  object ds_property2: TDataSource
    DataSet = qry_property2
    Left = 552
    Top = 80
  end
  object frxDBDataset_property1: TfrxDBDataset
    UserName = 'property1'
    CloseDataSource = False
    DataSet = qry_property1
    BCDToCurrency = False
    Left = 720
    Top = 176
  end
  object frxDBDataset_property2: TfrxDBDataset
    UserName = 'property2'
    CloseDataSource = False
    DataSet = qry_property2
    BCDToCurrency = False
    Left = 720
    Top = 232
  end
  object q_Linkend1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = ds_LInks
    Parameters = <
      item
        Name = 'Linkend1_id'
        DataType = ftInteger
        Precision = 10
        Value = 223905
      end>
    SQL.Strings = (
      'select * from view_Linkend_report'
      ''
      'where (id=:Linkend1_id)'
      '')
    Left = 656
    Top = 24
  end
  object ds_Linkend1: TDataSource
    DataSet = q_Linkend1
    Left = 656
    Top = 88
  end
  object q_Linkend2: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = ds_LInks
    Parameters = <
      item
        Name = 'Linkend2_id'
        DataType = ftInteger
        Precision = 10
        Value = 223906
      end>
    SQL.Strings = (
      'select * from  view_Linkend_report   where (id=:Linkend2_id)'
      '')
    Left = 728
    Top = 24
  end
  object ds_Linkend2: TDataSource
    DataSet = q_Linkend2
    Left = 728
    Top = 88
  end
  object frxDBDataset_q_Linkend1: TfrxDBDataset
    UserName = 'Linkend1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'id=id'
      'name=name'
      'project_id=project_id'
      'property_id=property_id'
      'linkendtype_id=linkendtype_id'
      'folder_id=folder_id'
      'objname=objname'
      '_=_'
      'LinkEndType_mode_id=LinkEndType_mode_id'
      'LinkEndType_band_id=LinkEndType_band_id'
      '_____________=_____________'
      'band=band'
      'subband=subband'
      'rx_freq_MHz=rx_freq_MHz'
      'tx_freq_MHz=tx_freq_MHz'
      'channel_number=channel_number'
      'channel_type=channel_type'
      'loss=loss'
      'redundancy=redundancy'
      'redundancy_type=redundancy_type'
      'equipment_type=equipment_type'
      'Protection=Protection'
      '__=__'
      'passive_element_id=passive_element_id'
      'passive_element_loss=passive_element_loss'
      'power_max=power_max'
      'power_loss=power_loss'
      'power_dBm=power_dBm'
      'power_W=power_W'
      '_____=_____'
      'mode=mode'
      'modulation_type=modulation_type'
      'modulation_count=modulation_count'
      'signature_width=signature_width'
      'signature_height=signature_height'
      'equaliser_profit=equaliser_profit'
      'freq_spacing=freq_spacing'
      'channel_spacing=channel_spacing'
      'freq_channel_count=freq_channel_count'
      'kratnostByFreq=kratnostByFreq'
      'kratnostBySpace=kratnostBySpace'
      'failure_period=failure_period'
      'restore_period=restore_period'
      'kng=kng'
      'threshold_BER_3=threshold_BER_3'
      'threshold_BER_6=threshold_BER_6'
      'channel_width_MHz=channel_width_MHz'
      '_______________=_______________'
      'comments=comments'
      '________=________'
      'guid=guid'
      'date_created=date_created'
      'date_modify=date_modify'
      'user_created=user_created'
      'user_modify=user_modify'
      '____Repeater____=____Repeater____'
      'space_diversity=space_diversity'
      '______________=______________'
      'LinkID=LinkID'
      'GOST_53363_modulation=GOST_53363_modulation'
      
        'GOST_53363_RX_count_on_combined_diversity=GOST_53363_RX_count_on' +
        '_combined_diversity'
      'rx_level_dBm=rx_level_dBm'
      'threshold_degradation_dB=threshold_degradation_dB'
      'ATPC_profit_dB=ATPC_profit_dB'
      'use_ATPC=use_ATPC'
      'bitrate_Mbps=bitrate_Mbps'
      '___________=___________'
      'pmp_site_id=pmp_site_id'
      'pmp_sector_id=pmp_sector_id'
      '_____PMP____=_____PMP____'
      'cell_layer_id=cell_layer_id'
      'calc_radius_km=calc_radius_km'
      '_______=_______'
      'calc_model_id=calc_model_id'
      'k0_open=k0_open'
      'k0_closed=k0_closed'
      'k0=k0'
      
        'NIIR1998_compensator_of_noises_Ixpic_dB=NIIR1998_compensator_of_' +
        'noises_Ixpic_dB'
      
        'NIIR1998_Coefficient_cross_polarization_protection_dB=NIIR1998_C' +
        'oefficient_cross_polarization_protection_dB'
      'azimuth=azimuth'
      'LinkEndType.name=LinkEndType.name'
      'radiation_class=radiation_class'
      'antenna_height=antenna_height'
      'antenna_diameter=antenna_diameter'
      'antenna_polarization=antenna_polarization'
      'antenna_gain=antenna_gain'
      'antenna_vert_width=antenna_vert_width'
      'antenna_horz_width=antenna_horz_width'
      'antenna_loss=antenna_loss'
      'antenna_tilt=antenna_tilt'
      'redundancy_str=redundancy_str'
      'antenna_ground_height=antenna_ground_height'
      'tx_freq_GHz=tx_freq_GHz'
      'antenna_Azimuth=antenna_Azimuth')
    DataSet = q_Linkend1
    BCDToCurrency = False
    Left = 576
    Top = 176
  end
  object frxDBDataset_q_Linkend2: TfrxDBDataset
    UserName = 'Linkend2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'id=id'
      'name=name'
      'project_id=project_id'
      'property_id=property_id'
      'linkendtype_id=linkendtype_id'
      'folder_id=folder_id'
      'objname=objname'
      '_=_'
      'LinkEndType_mode_id=LinkEndType_mode_id'
      'LinkEndType_band_id=LinkEndType_band_id'
      '_____________=_____________'
      'band=band'
      'subband=subband'
      'rx_freq_MHz=rx_freq_MHz'
      'tx_freq_MHz=tx_freq_MHz'
      'channel_number=channel_number'
      'channel_type=channel_type'
      'loss=loss'
      'redundancy=redundancy'
      'redundancy_type=redundancy_type'
      'equipment_type=equipment_type'
      'Protection=Protection'
      '__=__'
      'passive_element_id=passive_element_id'
      'passive_element_loss=passive_element_loss'
      'power_max=power_max'
      'power_loss=power_loss'
      'power_dBm=power_dBm'
      'power_W=power_W'
      '_____=_____'
      'mode=mode'
      'modulation_type=modulation_type'
      'modulation_count=modulation_count'
      'signature_width=signature_width'
      'signature_height=signature_height'
      'equaliser_profit=equaliser_profit'
      'freq_spacing=freq_spacing'
      'channel_spacing=channel_spacing'
      'freq_channel_count=freq_channel_count'
      'kratnostByFreq=kratnostByFreq'
      'kratnostBySpace=kratnostBySpace'
      'failure_period=failure_period'
      'restore_period=restore_period'
      'kng=kng'
      'threshold_BER_3=threshold_BER_3'
      'threshold_BER_6=threshold_BER_6'
      'channel_width_MHz=channel_width_MHz'
      '_______________=_______________'
      'comments=comments'
      '________=________'
      'guid=guid'
      'date_created=date_created'
      'date_modify=date_modify'
      'user_created=user_created'
      'user_modify=user_modify'
      '____Repeater____=____Repeater____'
      'space_diversity=space_diversity'
      '______________=______________'
      'LinkID=LinkID'
      'GOST_53363_modulation=GOST_53363_modulation'
      
        'GOST_53363_RX_count_on_combined_diversity=GOST_53363_RX_count_on' +
        '_combined_diversity'
      'rx_level_dBm=rx_level_dBm'
      'threshold_degradation_dB=threshold_degradation_dB'
      'ATPC_profit_dB=ATPC_profit_dB'
      'use_ATPC=use_ATPC'
      'bitrate_Mbps=bitrate_Mbps'
      '___________=___________'
      'pmp_site_id=pmp_site_id'
      'pmp_sector_id=pmp_sector_id'
      '_____PMP____=_____PMP____'
      'cell_layer_id=cell_layer_id'
      'calc_radius_km=calc_radius_km'
      '_______=_______'
      'calc_model_id=calc_model_id'
      'k0_open=k0_open'
      'k0_closed=k0_closed'
      'k0=k0'
      
        'NIIR1998_compensator_of_noises_Ixpic_dB=NIIR1998_compensator_of_' +
        'noises_Ixpic_dB'
      
        'NIIR1998_Coefficient_cross_polarization_protection_dB=NIIR1998_C' +
        'oefficient_cross_polarization_protection_dB'
      'azimuth=azimuth'
      'LinkEndType.name=LinkEndType.name'
      'radiation_class=radiation_class'
      'antenna_height=antenna_height'
      'antenna_diameter=antenna_diameter'
      'antenna_polarization=antenna_polarization'
      'antenna_gain=antenna_gain'
      'antenna_vert_width=antenna_vert_width'
      'antenna_horz_width=antenna_horz_width'
      'antenna_loss=antenna_loss'
      'antenna_tilt=antenna_tilt'
      'redundancy_str=redundancy_str'
      'antenna_ground_height=antenna_ground_height'
      'tx_freq_GHz=tx_freq_GHz'
      'antenna_Azimuth=antenna_Azimuth')
    DataSet = q_Linkend2
    BCDToCurrency = False
    Left = 576
    Top = 232
  end
  object qry_LInks_new: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'exec sp_Link_report '#39'109902,109926,'#39
      '')
    Left = 368
    Top = 16
  end
  object frxXLSExport1: TfrxXLSExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    ExportEMF = True
    AsText = False
    Background = True
    FastExport = True
    PageBreaks = True
    EmptyLines = True
    SuppressPageHeadersFooters = False
    Left = 288
    Top = 272
  end
  object frxReport_tele2: TfrxReport
    Version = '4.15.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.ShowCaptions = True
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41121.613084722200000000
    ReportOptions.LastChange = 41801.831026469910000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    Left = 432
    Top = 184
    Datasets = <
      item
        DataSet = frxDBDataset_link
        DataSetName = 'link'
      end
      item
        DataSet = frxDBDataset_q_Linkend1
        DataSetName = 'Linkend1'
      end
      item
        DataSet = frxDBDataset_q_Linkend2
        DataSetName = 'Linkend2'
      end
      item
        DataSet = frxDBDataset_property1
        DataSetName = 'property1'
      end
      item
        DataSet = frxDBDataset_property2
        DataSetName = 'property2'
      end>
    Variables = <>
    Style = <
      item
        Name = 'Title'
        Color = clGray
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Header'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Group header'
        Color = 16053492
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Data'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
      end
      item
        Name = 'Group footer'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Header line'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 2.000000000000000000
      end>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 600.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 256
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      PrintIfEmpty = False
      object ReportTitle1: TfrxReportTitle
        Top = 16.000000000000000000
        Visible = False
        Width = 2192.127400000000000000
        StartNewPage = True
      end
      object MasterData1: TfrxMasterData
        Height = 120.944923390000000000
        Top = 232.000000000000000000
        Width = 2192.127400000000000000
        DataSet = frxDBDataset_link
        DataSetName = 'link'
        RowCount = 0
        object property1name: TfrxMemoView
          Align = baLeft
          Left = 45.354360000000000000
          Width = 113.385826771654000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'name'
          DataSet = frxDBDataset_property1
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property1."name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2name: TfrxMemoView
          Align = baLeft
          Left = 45.354360000000000000
          Top = 60.472480000000010000
          Width = 113.385826771654000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'name'
          DataSet = frxDBDataset_property2
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property2."name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property1address: TfrxMemoView
          Align = baLeft
          Left = 272.126013543308000000
          Width = 226.771653543307000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'address'
          DataSet = frxDBDataset_property1
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property1."address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2address: TfrxMemoView
          Align = baLeft
          Left = 272.126013543308000000
          Top = 60.472480000000420000
          Width = 226.771653543307000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'address'
          DataSet = frxDBDataset_property2
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property2."address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1LinkEndTypename: TfrxMemoView
          Align = baLeft
          Left = 158.740186771654000000
          Width = 113.385826771654000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'LinkEndType.name'
          DataSet = frxDBDataset_q_Linkend1
          DataSetName = 'Linkend1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."LinkEndType.name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          Left = 158.740186771654000000
          Top = 60.472440940000010000
          Width = 113.385826771654000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'LinkEndType.name'
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."LinkEndType.name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1antenna_height: TfrxMemoView
          Align = baLeft
          Left = 740.787430866143100000
          Width = 75.590551181102360000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'antenna_height'
          DataSet = frxDBDataset_q_Linkend1
          DataSetName = 'Linkend1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."antenna_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          Left = 740.787430866143100000
          Top = 60.472480000000080000
          Width = 75.590551181102360000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend1
          DataSetName = 'Linkend1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."antenna_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          Left = 967.559084409450100000
          Width = 75.590551181102360000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."antenna_loss"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_polarization: TfrxMemoView
          Align = baLeft
          Left = 1421.102391490552000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."antenna_polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Align = baLeft
          Left = 1421.102391494962000000
          Top = 60.472480000000420000
          Width = 75.590551181102360000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'antenna_polarization'
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."antenna_polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          Left = 1269.921289130552000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."antenna_tilt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          Left = 1269.921289133860000000
          Top = 60.472480000000010000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."antenna_tilt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          Left = 891.968533228347800000
          Width = 75.590551181102360000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend1
          DataSetName = 'Linkend1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."antenna_gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          Left = 891.968533228347800000
          Top = 60.472480000000010000
          Width = 75.590551181102360000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend1
          DataSetName = 'Linkend1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."antenna_gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2radiation_class: TfrxMemoView
          Align = baLeft
          Left = 1345.511840313860000000
          Top = 60.472480000000420000
          Width = 75.590551181102360000
          Height = 60.472480000000000000
          ShowHint = False
          DataField = 'radiation_class'
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Align = baLeft
          Left = 1345.511840310552000000
          Width = 75.590551180000000000
          Height = 60.472480000000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2lat_str: TfrxMemoView
          Align = baLeft
          Left = 498.897667086615000000
          Top = 60.472480000000010000
          Width = 120.944881889764000000
          Height = 60.472480000000000000
          ShowHint = False
          DataField = 'lat_str'
          DataSet = frxDBDataset_property2
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[property2."lat_str"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Align = baLeft
          Left = 498.897667086615000000
          Width = 120.944881889764000000
          Height = 60.472480000000000000
          ShowHint = False
          DataSet = frxDBDataset_property2
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[property1."lat_str"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Align = baLeft
          Left = 619.842548976379000000
          Top = 60.472480000000010000
          Width = 120.944881889764000000
          Height = 60.472480000000000000
          ShowHint = False
          DataSet = frxDBDataset_property2
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[property2."lon_str"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Align = baLeft
          Left = 619.842548976379000000
          Width = 120.944881889764000000
          Height = 60.472480000000000000
          ShowHint = False
          DataSet = frxDBDataset_property2
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[property1."lon_str"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Align = baLeft
          Width = 45.354360000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_property1
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Align = baLeft
          Top = 60.472480000000010000
          Width = 45.354360000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_property1
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Align = baLeft
          Left = 816.377982047245400000
          Width = 75.590551181102360000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."antenna_azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Align = baLeft
          Left = 816.377982047245400000
          Top = 60.472480000000010000
          Width = 75.590551181102360000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."antenna_azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          Left = 967.559084409450100000
          Top = 60.472480000000010000
          Width = 75.590551181102360000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."antenna_loss"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1power_dBm: TfrxMemoView
          Align = baLeft
          Left = 1043.149635590552000000
          Width = 75.590551180000000000
          Height = 60.472480000000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend1
          DataSetName = 'Linkend1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Linkend1."power_Wt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Align = baLeft
          Left = 1043.149635590552000000
          Top = 60.472480000000010000
          Width = 75.590551181102360000
          Height = 60.472480000000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend1
          DataSetName = 'Linkend1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Linkend2."power_Wt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_ground_height: TfrxMemoView
          Align = baLeft
          Left = 1194.330737952757000000
          Top = 60.472480000000010000
          Width = 75.590551181102360000
          Height = 60.472480000000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."antenna_ground_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Align = baLeft
          Left = 1194.330737950552000000
          Width = 75.590551180000000000
          Height = 60.472480000000000000
          ShowHint = False
          DataField = 'antenna_ground_height'
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."antenna_ground_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Align = baLeft
          Left = 1118.740186770552000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Align = baLeft
          Left = 1118.740186771655000000
          Top = 60.472480000000010000
          Width = 75.590551181102360000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        Height = 128.503946770000000000
        Top = 80.000000000000000000
        Width = 2192.127400000000000000
        object Memo3: TfrxMemoView
          Align = baLeft
          Left = 272.126013541654000000
          Width = 226.771653540000000000
          Height = 120.944881890000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1114#1056#181#1057#1027#1057#8218#1056#1109' '#1057#1026#1056#176#1056#183#1056#1112#1056#181#1057#8240#1056#181#1056#1029#1056#1105#1057#1039' '#1056#160#1056#160#1056#1038' ('#1056#176#1056#1169#1057#1026#1056#181#1057#1027')')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo7: TfrxMemoView
          Align = baLeft
          Left = 1269.921289129371000000
          Width = 75.590551181102360000
          Height = 120.944881889763800000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176' '#1056#1110#1056#187#1056#176#1056#1030#1056#1029#1056#1109#1056#1110#1056#1109' '#1056#187#1056#181#1056#1111#1056#181#1057#1027#1057#8218#1056#1108#1056#176' '#1056#1105#1056#183#1056#187#1057#1107#1057#8225 +
              #1056#181#1056#1029#1056#1105#1057#1039', '#1056#1110#1057#1026#1056#176#1056#1169)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          Left = 498.897667081654000000
          Top = 68.031540000000040000
          Width = 120.944881890000000000
          Height = 52.913371180000000000
          GroupIndex = 1
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1057#1027'.'#1057#8364'. ('#1056#1110#1057#1026#1056#176#1056#1169', '#1056#1112#1056#1105#1056#1029', '#1057#1027')')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          Left = 158.740186771654000000
          Width = 113.385826770000000000
          Height = 120.944881889764000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#160#1056#160#1056#1038)
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          Left = 1118.740186767166000000
          Width = 75.590551181102360000
          Height = 120.944881889763800000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1057#8249' '#1056#1119#1056#160#1056#8221'/'#1056#1119#1056#160#1056#1114', '#1056#1114#1056#8220#1057#8224)
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo13: TfrxMemoView
          Align = baLeft
          Left = 619.842548971654000000
          Top = 68.031540000000040000
          Width = 120.944881890000000000
          Height = 52.913371180000000000
          GroupIndex = 1
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1030'.'#1056#1169'. ('#1056#1110#1057#1026#1056#176#1056#1169', '#1056#1112#1056#1105#1056#1029', '#1057#1027')')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          Width = 45.354360000000000000
          Height = 120.944881889764000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1074#8222#8211' '
            #1056#1111'/'#1056#1111'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          Left = 891.968533223858700000
          Width = 75.590551181102360000
          Height = 120.944886770000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1057#1036#1057#8222#1057#8222#1056#1105#1057#8224#1056#1105#1056#181#1056#1029#1057#8218' '#1057#1107#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1057#1039' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', dBi')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          Left = 740.787430861654000000
          Width = 75.590551181102360000
          Height = 120.944881890000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029' '#1056#160#1056#160#1056#1038' '#1056#1109#1057#8218' '#1056#1111#1056#1109#1056#1030#1056#181#1057#1026#1057#8230#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105' '#1056#8212#1056 +
              #181#1056#1112#1056#187#1056#1105', '#1056#1112)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          Left = 1421.102391491576000000
          Width = 75.590551181102360000
          Height = 120.944911180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' '#1056#1119#1056#160#1056#8221'/'#1056#1119#1056#160#1056#1114)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          Left = 1043.149635586063000000
          Width = 75.590551181102360000
          Height = 120.944911180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', '#1056#8217#1057#8218)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          Left = 1345.511840310473000000
          Width = 75.590551181102360000
          Height = 120.944911180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#187#1056#176#1057#1027#1057#1027
            #1056#1105#1056#183#1056#187#1057#1107#1057#8225#1056#181#1056#1029#1056#1105#1057#1039)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Align = baLeft
          Left = 45.354360000000000000
          Width = 113.385826771654000000
          Height = 120.944881889764000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1074#8222#8211' '#1056#1038#1057#8218#1056#176#1056#1029#1057#8224#1056#1105#1056#1105' ('#1056#1109#1056#177#1056#1109#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' '#1056#1030' '#1057#1027#1056#181#1057#8218#1056#1105')')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Align = baLeft
          Left = 1194.330737948268000000
          Width = 75.590551181102360000
          Height = 120.944881889763800000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249' '#1056#1029#1056#176#1056#1169' '#1057#1107#1057#1026#1056#1109#1056#1030#1056#1029#1056#181#1056#1112 +
              ' '#1056#1112#1056#1109#1057#1026#1057#1039', '#1056#1112)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Align = baLeft
          Left = 816.377982042756400000
          Width = 75.590551181102360000
          Height = 120.944881890000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218' '#1056#1110#1056#187#1056#176#1056#1030#1056#1029#1056#1109#1056#1110#1056#1109' '#1056#187#1056#181#1056#1111#1056#181#1057#1027#1057#8218#1056#1108#1056#176' '#1056#1105#1056#183#1056#187#1057#1107#1057#8225#1056#181#1056#1029#1056#1105#1057 +
              #1039' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', '#1056#1110#1057#1026#1056#176#1056#1169)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Align = baLeft
          Left = 498.897667081654000000
          Width = 241.889846770000000000
          Height = 68.031491180000000000
          GroupIndex = 1
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8220#1056#181#1056#1109#1056#1110#1057#1026#1056#176#1057#8222#1056#1105#1057#8225#1056#181#1057#1027#1056#1108#1056#1105#1056#181' '#1056#1108#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' ('#1056#1038#1056#1113'-42)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Align = baLeft
          Left = 967.559084404961000000
          Width = 75.590551181102360000
          Height = 120.944911180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#8218#1056#181#1057#1026#1056#1105' '#1056#1030' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1056#1109'-'#1057#8222#1056#1105#1056#1169#1056#181#1057#1026#1056#1029#1056#1109#1056#1112' '#1057#8218#1057#1026#1056#176#1056#1108#1057#8218#1056#181', dB')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
      end
      object Line: TfrxMemoView
        Left = 19.338590000000000000
        Top = 378.393940000000000000
        Width = 79.370130000000000000
        Height = 18.897650000000000000
        ShowHint = False
        Memo.UTF8 = (
          '[Line]')
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
    end
  end
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    CheckboxAsShape = False
    Left = 288
    Top = 376
  end
  object frxDesigner1: TfrxDesigner
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    TemplatesExt = 'fr3'
    Restrictions = []
    RTLLanguage = False
    MemoParentFont = False
    Left = 416
    Top = 328
  end
end
