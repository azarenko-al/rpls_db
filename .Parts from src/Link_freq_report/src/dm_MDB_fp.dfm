object dmMDB111111111: TdmMDB111111111
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  Left = 1551
  Top = 410
  Height = 538
  Width = 859
  object ADOConnection_MDB: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Users\Alexey\App' +
      'Data\Local\RPLS_DB_LINK\report_freq_plan.mdb;Persist Security In' +
      'fo=False;'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 60
    Top = 12
  end
  object t_Property1: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'id'
    MasterFields = 'Property1_id'
    MasterSource = ds_LInks
    TableName = 'property1'
    Left = 184
    Top = 104
  end
  object t_LinkEnd1: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'id'
    MasterFields = 'linkend1_id'
    MasterSource = ds_LInks
    TableName = 'LinkEnd1'
    Left = 384
    Top = 104
  end
  object t_Property2: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'id'
    MasterFields = 'Property2_id'
    MasterSource = ds_LInks
    TableName = 'property2'
    Left = 264
    Top = 104
  end
  object t_LinkEnd2: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'id'
    MasterFields = 'linkend2_id'
    MasterSource = ds_LInks
    TableName = 'LinkEnd2'
    Left = 464
    Top = 104
  end
  object t_Antennas1111: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'link_id'
    MasterFields = 'id'
    MasterSource = ds_LInks
    TableName = 'Antennas1'
    Left = 584
    Top = 104
  end
  object t_Antennas2222: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'link_id'
    MasterFields = 'id'
    MasterSource = ds_LInks
    TableName = 'Antennas2'
    Left = 664
    Top = 104
  end
  object t_Link: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    TableName = 'Link'
    Left = 80
    Top = 104
  end
  object ds_property1: TDataSource
    DataSet = t_Property1
    Left = 184
    Top = 200
  end
  object ds_LInks: TDataSource
    DataSet = t_Link
    Left = 80
    Top = 200
  end
  object ds_property2: TDataSource
    DataSet = t_Property2
    Left = 272
    Top = 200
  end
  object ds_Linkend1: TDataSource
    DataSet = t_LinkEnd1
    Left = 392
    Top = 200
  end
  object ds_Linkend2: TDataSource
    DataSet = t_LinkEnd2
    Left = 464
    Top = 200
  end
  object frxReport: TfrxReport
    Version = '4.15.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.ShowCaptions = True
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41121.613084722200000000
    ReportOptions.LastChange = 41940.802632372700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '    '
      '    '
      ''
      ''
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%s%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d'#176' %.2d'#39#39' %1.2f'#39#39#39#39#39';'
      'var    '
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      '                           '
      ' // angle.Sec:=TruncFloat(angle.Sec,2);'
      ' '
      '    Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      '  if aValue<0 then'
      '    Result:='#39'-'#39'+Result;'
      'end;'
      '  '
      ''
      ' //------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eSec: Double;'
      ''
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      ''
      ''
      '   //    aValue:=99;                            '
      '          '
      ''
      '          '
      '  aDeg:=Trunc(aValue);'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      ''
      '//  e:= (aValue - Trunc(aValue));'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      ''
      ''
      '  aMin:=Trunc(e);'
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  while aSec>(60-0.1) do'
      '  begin'
      '    aMin:=aMin+1;'
      '    aSec:=aSec-60;'
      '  end;    '
      ''
      '  while aMin>=60 do'
      '  begin'
      '      aMin:=aMin-60;'
      '      aDeg:=aDeg+1;'
      '  end;  '
      ''
      '  if aSec<0 then'
      '     aSec:=0;'
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      ''
      ''
      ''
      'end;'
      '  '
      ''
      '                                 '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_1111(aValue: Double; var aDeg,aMin:in' +
        'teger; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      ''
      '      '
      ''
      ''
      'end;'
      '        '
      ''
      '       '
      'begin'
      ''
      'end.')
    Left = 96
    Top = 392
    Datasets = <
      item
        DataSet = frxDBDataset_link
        DataSetName = 'link'
      end
      item
        DataSet = frxDBDataset_q_Linkend1
        DataSetName = 'Linkend1'
      end
      item
        DataSet = frxDBDataset_q_Linkend2
        DataSetName = 'Linkend2'
      end
      item
        DataSet = frxDBDataset_property1
        DataSetName = 'property1'
      end
      item
        DataSet = frxDBDataset_property2
        DataSetName = 'property2'
      end>
    Variables = <>
    Style = <
      item
        Name = 'Title'
        Color = clGray
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Header'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Group header'
        Color = 16053492
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Data'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
      end
      item
        Name = 'Group footer'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Header line'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 2.000000000000000000
      end>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 600.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 256
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      PrintIfEmpty = False
      object ReportTitle1: TfrxReportTitle
        Height = 45.354360000000000000
        Top = 16.000000000000000000
        Width = 2192.127400000000000000
        StartNewPage = True
        object Memo8: TfrxMemoView
          Align = baWidth
          Width = 2192.127400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#1119#1057#1026#1056#1109#1056#181#1056#1108#1057#8218' '#1057#8225#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#1029#1056#1109'-'#1057#8218#1056#181#1057#1026#1057#1026#1056#1105#1057#8218#1056#1109#1057#1026#1056#1105#1056#176#1056#187#1057#1034#1056#1029#1056#1109#1056#1110#1056#1109' '#1056 +
              #1111#1056#187#1056#176#1056#1029#1056#176'  '#1056#1029#1056#176' '#1057#8218#1056#181#1057#1026#1057#1026#1056#1105#1057#8218#1056#1109#1057#1026#1056#1105#1056#1105)
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        Height = 120.944960000000000000
        Top = 216.000000000000000000
        Width = 2192.127400000000000000
        RowCount = 0
        object property1name: TfrxMemoView
          Align = baLeft
          Left = 75.590600000000000000
          Width = 162.519790000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property1."name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2name: TfrxMemoView
          Align = baLeft
          Left = 75.590600000000000000
          Top = 60.472480000000000000
          Width = 162.519790000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property2."name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line: TfrxMemoView
          Width = 30.236240000000000000
          Height = 120.944923390000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Line]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property1address: TfrxMemoView
          Align = baLeft
          Left = 238.110390000000000000
          Width = 226.771653540000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'address'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property1."address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2address: TfrxMemoView
          Align = baLeft
          Left = 238.110390000000000000
          Top = 60.472480000000000000
          Width = 226.771653540000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'address'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property2."address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1LinkEndTypename: TfrxMemoView
          Align = baLeft
          Left = 691.653697080000000000
          Width = 132.283464570000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."LinkEndType_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          Left = 691.653697080000000000
          Top = 60.472440940000000000
          Width = 132.344510160000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."LinkEndType_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1antenna_height: TfrxMemoView
          Align = baLeft
          Left = 1065.826969370000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          Left = 1065.888014960000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2rx_freq_MHz: TfrxMemoView
          Align = baLeft
          Left = 823.998207240000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'rx_freq_MHz'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          Left = 823.937161650000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_polarization: TfrxMemoView
          Align = baLeft
          Left = 1544.376250320000000000
          Width = 53.291338580000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Align = baLeft
          Left = 1544.376250320000000000
          Top = 60.472480000000000000
          Width = 53.291338580000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_diameter: TfrxMemoView
          Align = baLeft
          Left = 1491.084911740000000000
          Width = 53.291338580000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Align = baLeft
          Left = 1491.084911740000000000
          Top = 60.472480000000000000
          Width = 53.291338580000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."diameter"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          Left = 899.527712830000000000
          Width = 49.133858270000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."horz_width"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Align = baLeft
          Left = 899.588758420000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."horz_width"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          Left = 948.661571100000000000
          Width = 49.133858270000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."vert_width"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Align = baLeft
          Left = 948.722616690000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."vert_width"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          Left = 997.795429370000000000
          Width = 68.031540000000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          Left = 997.856474960000000000
          Top = 60.472480000000000000
          Width = 68.031540000000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object linklength_km: TfrxMemoView
          Align = baLeft
          Left = 1251.084865360000000000
          Width = 52.913385830000000000
          Height = 120.944960000000000000
          ShowHint = False
          DataField = 'length_km'
          DisplayFormat.FormatStr = '%2.1f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."length_km"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1power_dBm: TfrxMemoView
          Align = baLeft
          Left = 1303.998251190000000000
          Width = 52.913385830000000000
          Height = 120.944960000000000000
          ShowHint = False
          DataField = 'power_dBm'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Linkend1."power_dBm"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2radiation_class: TfrxMemoView
          Align = baLeft
          Left = 1356.911637020000000000
          Top = 60.472480000000000000
          Width = 71.811070000000000000
          Height = 60.472480000000000000
          ShowHint = False
          DataField = 'radiation_class'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Align = baLeft
          Left = 1356.911637020000000000
          Width = 71.811070000000000000
          Height = 60.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2bitrate_Mbps: TfrxMemoView
          Align = baLeft
          Left = 1428.722707020000000000
          Top = 60.472480000000000000
          Width = 62.362204720000000000
          Height = 60.472480000000000000
          ShowHint = False
          DataField = 'bitrate_Mbps'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."bitrate_Mbps"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Align = baLeft
          Left = 1428.722707020000000000
          Width = 62.362204720000000000
          Height = 60.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."bitrate_Mbps"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Align = baLeft
          Left = 1597.667588900000000000
          Width = 102.047310000000000000
          Height = 120.944911180000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Align = baLeft
          Left = 1699.714898900000000000
          Width = 49.133890000000000000
          Height = 120.944911180000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
          VAlign = vaCenter
        end
        object property2lat_str: TfrxMemoView
          Align = baLeft
          Left = 464.882043540000000000
          Top = 60.472480000000000000
          Width = 113.385826770000000000
          Height = 60.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<property2."lat_gck_2011">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Align = baLeft
          Left = 464.882043540000000000
          Width = 113.385826770000000000
          Height = 60.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<property1."lat_gck_2011">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Align = baLeft
          Left = 578.267870310000000000
          Top = 60.472480000000000000
          Width = 113.385826770000000000
          Height = 60.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<property2."lon_gck_2011">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Align = baLeft
          Left = 578.267870310000000000
          Width = 113.385826770000000000
          Height = 60.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<property1."lon_gck_2011">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Align = baLeft
          Left = 30.236240000000000000
          Width = 45.354360000000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property1."code"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Align = baLeft
          Left = 30.236240000000000000
          Top = 60.472480000000100000
          Width = 45.354360000000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property2."code"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_ground_height: TfrxMemoView
          Align = baLeft
          Left = 1141.417520550000000000
          Width = 56.692913390000000000
          Height = 60.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height_with_ground"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Align = baLeft
          Left = 1141.478566140000000000
          Top = 60.472480000000000000
          Width = 56.692913390000000000
          Height = 60.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height_with_ground"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Align = baLeft
          Left = 1198.110433940000000000
          Width = 52.913385830000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Align = baLeft
          Left = 1198.171479530000000000
          Top = 60.472480000000000000
          Width = 52.913385830000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        Height = 75.590551180000000000
        Top = 120.000000000000000000
        Width = 2192.127400000000000000
        object Memo3: TfrxMemoView
          Align = baLeft
          Left = 238.110390000000000000
          Width = 226.771653540000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1106#1056#1169#1057#1026#1056#181#1057#1027)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Align = baLeft
          Left = 948.661571100000000000
          Width = 49.133858270000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8221#1056#1116#1056#1106
            #1056#1030#1056#181#1057#1026#1057#8218'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Align = baLeft
          Left = 899.527712830000000000
          Width = 49.133858270000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8221#1056#1116#1056#1106
            #1056#1110#1056#1109#1057#1026'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          Width = 30.236240000000000000
          Height = 75.590551181102400000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1074#8222#8211
            #1056#1111'/'#1056#1111)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          Left = 464.882043540000000000
          Width = 113.385826770000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1025#1056#1105#1057#1026#1056#1109#1057#8218#1056#176','
            #1057#1027'.'#1057#8364'.  ('#1056#8220'CK-2011)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          Left = 691.653697080000000000
          Width = 132.283464570000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '
            #1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          Left = 823.937161650000000000
          Width = 75.590551180000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#152#1056#1029#1056#1169#1056#181#1056#1108#1057#1027
            '('#1056#1029#1056#1109#1056#1112#1056#1105#1056#1029#1056#176#1056#187#1057#8249' '#1057#8225#1056#176#1057#1027#1057#8218#1056#1109#1057#8218'), '#1056#1114#1056#8220#1057#8224)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Align = baLeft
          Left = 578.267870310000000000
          Width = 113.385826770000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8221#1056#1109#1056#187#1056#1110#1056#1109#1057#8218#1056#176','
            #1056#1030'.'#1056#1169'.   ('#1056#8220'CK-2011)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          Left = 30.236240000000000000
          Width = 45.354360000000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1074#8222#8211
            #1056#177'/'#1057#1027)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          Left = 997.795429370000000000
          Width = 68.031496060000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1113#1057#1107','
            #1056#1169#1056#8216)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          Left = 1065.826925430000000000
          Width = 75.590551180000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '
            #1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176' '
            #1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249','
            ' '#1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          Left = 1542.819096830000000000
          Width = 53.291338580000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          Left = 1489.527758250000000000
          Width = 53.291338580000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112'.'
            #1056#176#1056#1029#1057#8218'., '#1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          Left = 1251.023775830000000000
          Width = 52.913385830000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#160#1056#176#1057#1027#1057#1027#1057#8218'., '#1056#1108#1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          Left = 1303.937161660000000000
          Width = 52.913385830000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240'-'
            #1056#1029#1056#1109#1057#1027#1057#8218#1057#1034', '
            #1056#1169#1056#8216#1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          Left = 1356.850547490000000000
          Width = 70.315006040000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1113#1056#187#1056#176#1057#1027#1057#1027
            #1056#1105#1056#183#1056#187#1057#1107#1057#8225#1056#181#1056#1029#1056#1105#1057#1039)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Align = baLeft
          Left = 1427.165553530000000000
          Width = 62.362204720000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#1109#1057#8218#1056#1109#1056#1108#1056#176', M'#1056#8216#1056#1105#1057#8218'/'#1057#1027)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Align = baLeft
          Left = 1596.110435410000000000
          Width = 104.173295620000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#164#1056#152#1056#1115' '
            #1057#1027#1056#1169#1056#181#1056#187#1056#176#1056#1030#1057#8364#1056#181#1056#1110#1056#1109' '
            #1056#183#1056#176#1056#1111#1056#1105#1057#1027#1057#1034)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Align = baLeft
          Left = 1700.283731030000000000
          Width = 49.133890000000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#1112'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Align = baLeft
          Left = 75.590600000000000000
          Width = 162.519790000000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1116#1056#176#1056#183#1056#1030#1056#176#1056#1029#1056#1105#1056#181' ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Align = baLeft
          Left = 1141.417476610000000000
          Width = 56.692913390000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'H '#1056#176#1056#1029#1057#8218'.'
            #1056#1109#1057#8218' '#1057#1107#1057#1026'. '#1056#1112#1056#1109#1057#1026#1057#1039', '
            #1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Align = baLeft
          Left = 1198.110390000000000000
          Width = 52.913385830000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218', '#1056#1110#1057#1026#1056#176#1056#1169'.')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxReport_tele2: TfrxReport
    Version = '4.15.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.ShowCaptions = True
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41121.613084722200000000
    ReportOptions.LastChange = 42060.747135995370000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '  '
      ''
      '  '
      '  '
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%s%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d'#176' %.2d'#39#39' %1.2f'#39#39#39#39#39';'
      'var    '
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      '                           '
      ' // angle.Sec:=TruncFloat(angle.Sec,2);'
      ' '
      '    Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      '  if aValue<0 then'
      '    Result:='#39'-'#39'+Result;'
      'end;'
      ''
      ' //------------------------------------------------------------'
      'function ChangeName (aValue: String): String;'
      '//------------------------------------------------------------'
      'begin   '
      '  Result:=Copy(aValue, 1, 2) + Copy(aValue, 5, 4)'
      'end;  '
      '                     '
      ''
      '  var'
      '  LLine : integer;                                         '
      ''
      '  '
      '//------------------------------------------------------------'
      'function GetLine (): integer;'
      '//------------------------------------------------------------'
      'begin'
      '  Inc(LLine);                '
      '  Result:=LLine;  '
      'end;  '
      '                     '
      '  '
      '  '
      ''
      '    '
      ''
      ' //------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eSec: Double;'
      ''
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      ''
      ''
      '   //    aValue:=99;                            '
      '          '
      ''
      '          '
      '  aDeg:=Trunc(aValue);'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      ''
      '//  e:= (aValue - Trunc(aValue));'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      ''
      ''
      '  aMin:=Trunc(e);'
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  while aSec>(60-0.1) do'
      '  begin'
      '    aMin:=aMin+1;'
      '    aSec:=aSec-60;'
      '  end;    '
      ''
      '  while aMin>=60 do'
      '  begin'
      '      aMin:=aMin-60;'
      '      aDeg:=aDeg+1;'
      '  end;  '
      ''
      '  if aSec<0 then'
      '     aSec:=0;    '
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      ''
      ''
      ''
      'end;'
      '                                   '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_1111(aValue: Double; var aDeg,aMin:in' +
        'teger; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      '                   '
      ''
      ''
      'end;'
      '        '
      ''
      '       '
      'begin'
      ''
      'end.')
    Left = 200
    Top = 392
    Datasets = <
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end>
    Variables = <>
    Style = <
      item
        Name = 'Title'
        Color = clGray
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Header'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Group header'
        Color = 16053492
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Data'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
      end
      item
        Name = 'Group footer'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Header line'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 2.000000000000000000
      end>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 429.291750000000000000
      PaperHeight = 209.999791670000000000
      PaperSize = 256
      LeftMargin = 10.001250000000000000
      RightMargin = 10.001250000000000000
      TopMargin = 10.001250000000000000
      BottomMargin = 10.001250000000000000
      PrintIfEmpty = False
      object ReportTitle1: TfrxReportTitle
        Top = 16.000000000000000000
        Visible = False
        Width = 1546.920999052500000000
        StartNewPage = True
      end
      object MasterData1: TfrxMasterData
        Height = 120.944923390000000000
        Top = 216.000000000000000000
        Width = 1546.920999052500000000
        RowCount = 0
        object property1name: TfrxMemoView
          Align = baLeft
          Left = 45.354360000000000000
          Width = 113.385826770000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property1."name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2name: TfrxMemoView
          Align = baLeft
          Left = 45.354360000000000000
          Top = 60.472480000000010000
          Width = 113.385826771654000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property2."name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property1address: TfrxMemoView
          Align = baLeft
          Left = 272.126013540000000000
          Width = 200.000000000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'address'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property1."address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2address: TfrxMemoView
          Align = baLeft
          Left = 272.126013541654000000
          Top = 60.472480000000010000
          Width = 200.000000000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'address'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property2."address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1LinkEndTypename: TfrxMemoView
          Align = baLeft
          Left = 158.740186770000000000
          Width = 113.385826770000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."LinkEndType_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          Left = 158.740186771654000000
          Top = 60.472440940000000000
          Width = 113.385826770000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."LinkEndType_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1antenna_height: TfrxMemoView
          Align = baLeft
          Left = 714.015777320000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna1."height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          Left = 714.015777321654000000
          Top = 60.472480000000010000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          Left = 940.787430860000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."loss"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_polarization: TfrxMemoView
          Align = baLeft
          Left = 1394.330737940000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Align = baLeft
          Left = 1394.740186762756000000
          Top = 60.472480000000010000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          Left = 1243.149635580000000000
          Width = 75.590551180000000000
          Height = 60.470000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."tilt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          Left = 1243.149635582756000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 60.470000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."tilt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          Left = 865.196879680000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          Left = 865.196879681654000000
          Top = 60.472480000000010000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1radiation_class: TfrxMemoView
          Align = baLeft
          Left = 1318.740186760000000000
          Width = 75.590551180000000000
          Height = 60.470000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2lat_str: TfrxMemoView
          Align = baLeft
          Left = 472.126013541654000000
          Top = 60.472480000000010000
          Width = 120.944881890000000000
          Height = 60.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<property2."lat">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Align = baLeft
          Left = 472.126013540000000000
          Width = 120.944881890000000000
          Height = 60.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<property1."lat">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Align = baLeft
          Left = 593.070895431654000000
          Top = 60.472480000000010000
          Width = 120.944881890000000000
          Height = 60.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<property2."lon">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Align = baLeft
          Left = 593.070895430000000000
          Width = 120.944881890000000000
          Height = 60.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<property1."lon">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Align = baLeft
          Width = 45.354360000000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[GetLine()]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Align = baLeft
          Top = 60.472480000000000000
          Width = 45.354360000000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[GetLine()]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Align = baLeft
          Left = 789.606328500000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Align = baLeft
          Left = 789.606328501654000000
          Top = 60.472480000000010000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          Left = 940.787430861654000000
          Top = 60.472480000000010000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."loss"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1power_dBm: TfrxMemoView
          Align = baLeft
          Left = 1016.377982040000000000
          Width = 75.590551180000000000
          Height = 60.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Linkend1."power_W"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Align = baLeft
          Left = 1016.377982041654000000
          Top = 60.472480000000010000
          Width = 75.590551180000000000
          Height = 60.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Linkend2."power_W"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_ground_height: TfrxMemoView
          Align = baLeft
          Left = 1167.559084402756000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 60.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height_with_ground"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Align = baLeft
          Left = 1167.559084400000000000
          Width = 75.590551180000000000
          Height = 60.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height_with_ground"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Align = baLeft
          Left = 1091.968533220000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Align = baLeft
          Left = 1091.968533221654000000
          Top = 60.472480000000010000
          Width = 75.590551181102400000
          Height = 60.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          Left = 1469.921289120000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."tilt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Align = baLeft
          Left = 1470.330737942756000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."tilt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          Left = 1318.740186762756000000
          Top = 60.470000000000030000
          Width = 76.000000000000000000
          Height = 60.470000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        Height = 120.944911180000000000
        Top = 76.000000000000000000
        Width = 1546.920999052500000000
        object Memo3: TfrxMemoView
          Align = baLeft
          Left = 272.126013541654000000
          Width = 200.000000000000000000
          Height = 120.944881890000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1114#1056#181#1057#1027#1057#8218#1056#1109' '#1057#1026#1056#176#1056#183#1056#1112#1056#181#1057#8240#1056#181#1056#1029#1056#1105#1057#1039' '#1056#160#1056#160#1056#1038' ('#1056#176#1056#1169#1057#1026#1056#181#1057#1027')')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo7: TfrxMemoView
          Align = baLeft
          Left = 1243.149635587166000000
          Width = 75.590551181102400000
          Height = 120.944881889764000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176' '#1056#1110#1056#187#1056#176#1056#1030#1056#1029#1056#1109#1056#1110#1056#1109' '#1056#187#1056#181#1056#1111#1056#181#1057#1027#1057#8218#1056#1108#1056#176' '#1056#1105#1056#183#1056#187#1057#1107#1057#8225 +
              #1056#181#1056#1029#1056#1105#1057#1039', '#1056#1110#1057#1026#1056#176#1056#1169)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          Left = 472.126013541654000000
          Top = 68.031540000000030000
          Width = 120.944881890000000000
          Height = 52.913371180000000000
          GroupIndex = 1
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1057#1027'.'#1057#8364'. ('#1056#1110#1057#1026#1056#176#1056#1169', '#1056#1112#1056#1105#1056#1029', '#1057#1027')')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          Left = 158.740186771654000000
          Width = 113.385826770000000000
          Height = 120.944881889764000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#160#1056#160#1056#1038)
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          Left = 1091.968533226064000000
          Width = 75.590551180000000000
          Height = 120.944881890000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1057#8249' '#1056#1119#1056#160#1056#8221'/'#1056#1119#1056#160#1056#1114', '#1056#1114#1056#8220#1057#8224)
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo13: TfrxMemoView
          Align = baLeft
          Left = 593.070895431654000000
          Top = 68.031540000000030000
          Width = 120.944881890000000000
          Height = 52.913371180000000000
          GroupIndex = 1
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1030'.'#1056#1169'. ('#1056#1110#1057#1026#1056#176#1056#1169', '#1056#1112#1056#1105#1056#1029', '#1057#1027')')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          Width = 45.354360000000000000
          Height = 120.944881889764000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1074#8222#8211' '
            #1056#1111'/'#1056#1111'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          Left = 865.196879683858800000
          Width = 75.590551181102400000
          Height = 120.944886770000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1057#1036#1057#8222#1057#8222#1056#1105#1057#8224#1056#1105#1056#181#1056#1029#1057#8218' '#1057#1107#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1057#1039' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', dBi')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          Left = 714.015777321654000000
          Width = 75.590551181102400000
          Height = 120.944881890000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029' '#1056#160#1056#160#1056#1038' '#1056#1109#1057#8218' '#1056#1111#1056#1109#1056#1030#1056#181#1057#1026#1057#8230#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105' '#1056#8212#1056 +
              #181#1056#1112#1056#187#1056#1105', '#1056#1112)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          Left = 1394.330737949371000000
          Width = 75.590551181102400000
          Height = 120.944911180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' '#1056#1119#1056#160#1056#8221'/'#1056#1119#1056#160#1056#1114)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          Left = 1016.377982046064000000
          Width = 75.590551180000000000
          Height = 120.944911180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', '#1056#8217#1057#8218)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          Left = 1318.740186768268000000
          Width = 75.590551181102400000
          Height = 120.944911180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#187#1056#176#1057#1027#1057#1027
            #1056#1105#1056#183#1056#187#1057#1107#1057#8225#1056#181#1056#1029#1056#1105#1057#1039)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Align = baLeft
          Left = 45.354360000000000000
          Width = 113.385826771654000000
          Height = 120.944881889764000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1074#8222#8211' '#1056#1038#1057#8218#1056#176#1056#1029#1057#8224#1056#1105#1056#1105' ('#1056#1109#1056#177#1056#1109#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' '#1056#1030' '#1057#1027#1056#181#1057#8218#1056#1105')')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Align = baLeft
          Left = 1167.559084406064000000
          Width = 75.590551181102400000
          Height = 120.944881889764000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249' '#1056#1029#1056#176#1056#1169' '#1057#1107#1057#1026#1056#1109#1056#1030#1056#1029#1056#181#1056#1112 +
              ' '#1056#1112#1056#1109#1057#1026#1057#1039', '#1056#1112)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Align = baLeft
          Left = 789.606328502756400000
          Width = 75.590551181102400000
          Height = 120.944881890000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218' '#1056#1110#1056#187#1056#176#1056#1030#1056#1029#1056#1109#1056#1110#1056#1109' '#1056#187#1056#181#1056#1111#1056#181#1057#1027#1057#8218#1056#1108#1056#176' '#1056#1105#1056#183#1056#187#1057#1107#1057#8225#1056#181#1056#1029#1056#1105#1057 +
              #1039' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', '#1056#1110#1057#1026#1056#176#1056#1169)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Align = baLeft
          Left = 472.126013541654000000
          Width = 241.889846770000000000
          Height = 68.031491180000000000
          GroupIndex = 1
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8220#1056#181#1056#1109#1056#1110#1057#1026#1056#176#1057#8222#1056#1105#1057#8225#1056#181#1057#1027#1056#1108#1056#1105#1056#181' '#1056#1108#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' ('#1056#1038#1056#1113'-42)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Align = baLeft
          Left = 940.787430864961200000
          Width = 75.590551181102400000
          Height = 120.944911180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#8218#1056#181#1057#1026#1056#1105' '#1056#1030' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1056#1109'-'#1057#8222#1056#1105#1056#1169#1056#181#1057#1026#1056#1029#1056#1109#1056#1112' '#1057#8218#1057#1026#1056#176#1056#1108#1057#8218#1056#181', dB')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Align = baLeft
          Left = 1469.921289130473000000
          Width = 75.590551180000000000
          Height = 120.944911180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
      end
    end
  end
  object frxReport_Tele2_CK42: TfrxReport
    Version = '4.15.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.AllowEdit = False
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbTools, pbNavigator, pbExportQuick, pbNoFullScreen]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41121.613084722200000000
    ReportOptions.LastChange = 42090.555932268500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '//------------------------------------------------------------'
      '//function dBm_to_Wt_ (aValue: variant): double;'
      '//------------------------------------------------------------'
      '//begin'
      '//  if aValue = null then'
      '//    Result:=0'
      '//  else'
      '//    Result:=dBm_to_Wt (aValue);           '
      '  '
      '  '
      '//end;'
      '  '
      'var'
      '  LLine : integer;                                         '
      ''
      '  '
      '//------------------------------------------------------------'
      'function GetLine (): integer;'
      '//------------------------------------------------------------'
      'begin'
      '  Inc(LLine);                '
      '  Result:=LLine;  '
      'end;  '
      '                     '
      '  '
      '  '
      ''
      '//------------------------------------------------------------'
      'function ChangeName (aValue: String): String;'
      '//------------------------------------------------------------'
      'begin   '
      '  Result:=Copy(aValue, 1, 2) + Copy(aValue, 5, 4)'
      'end;  '
      '                     '
      '  '
      '  '
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d %.2d %1.0f'#39';'
      'var'
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      '                           '
      ' // angle.Sec:=TruncFloat(angle.Sec,2);'
      ' '
      '    Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      '  if aValue<0 then'
      '    Result:='#39'-'#39'+Result;'
      'end;'
      ''
      ''
      ' //------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eSec: Double;'
      ''
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      ''
      ''
      '   //    aValue:=99;                            '
      '          '
      ''
      '          '
      '  aDeg:=Trunc(aValue);'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      ''
      '//  e:= (aValue - Trunc(aValue));'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      ''
      ''
      '  aMin:=Trunc(e);'
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '   while aSec>(60-0.1) do'
      '  begin'
      '    aMin:=aMin+1;'
      '    aSec:=aSec-60;'
      '  end;    '
      ''
      '  while aMin>=60 do'
      '  begin'
      '      aMin:=aMin-60;'
      '      aDeg:=aDeg+1;'
      '  end;  '
      ''
      '  if aSec<0 then'
      '     aSec:=0;    '
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      ''
      ''
      ''
      'end;'
      '  '
      '                              '
      '           '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_111111(aValue: Double; var aDeg,aMin:' +
        'integer; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      '                   '
      ''
      ''
      'end;'
      '        '
      ''
      '       '
      'begin'
      ''
      'end.')
    Left = 326
    Top = 388
    Datasets = <
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end>
    Variables = <>
    Style = <
      item
        Name = 'Title'
        Color = clGray
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Header'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Group header'
        Color = 16053492
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Data'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
      end
      item
        Name = 'Group footer'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Header line'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 2.000000000000000000
      end>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 429.291750000000000000
      PaperHeight = 839.999166670000000000
      PaperSize = 256
      LeftMargin = 10.001250000000000000
      RightMargin = 10.001250000000000000
      TopMargin = 10.001250000000000000
      BottomMargin = 10.001250000000000000
      PrintIfEmpty = False
      object ReportTitle1: TfrxReportTitle
        Top = 16.000000000000000000
        Visible = False
        Width = 1546.920999052500000000
        StartNewPage = True
      end
      object MasterData1: TfrxMasterData
        Height = 40.944960000000000000
        Top = 224.000000000000000000
        Width = 1546.920999052500000000
        RowCount = 0
        object property1name: TfrxMemoView
          Align = baLeft
          Left = 80.000000000000000000
          Width = 61.385826770000000000
          Height = 20.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[property1."name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2name: TfrxMemoView
          Align = baLeft
          Left = 80.000000000000000000
          Top = 20.472480000000000000
          Width = 61.385826770000000000
          Height = 20.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[property2."name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property1address: TfrxMemoView
          Align = baLeft
          Left = 254.771653540000000000
          Width = 636.000000000000000000
          Height = 20.472440940000000000
          ShowHint = False
          DataField = 'address'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property1."address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2address: TfrxMemoView
          Align = baLeft
          Left = 254.771653540000000000
          Top = 20.472480000000000000
          Width = 636.000000000000000000
          Height = 20.472440940000000000
          ShowHint = False
          DataField = 'address'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property2."address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1LinkEndTypename: TfrxMemoView
          Align = baLeft
          Left = 141.385826770000000000
          Width = 113.385826770000000000
          Height = 20.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Linkend1."LinkEndType_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          Left = 141.385826770000000000
          Top = 20.472440940000000000
          Width = 113.385826770000000000
          Height = 20.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Linkend2."LinkEndType_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1antenna_height: TfrxMemoView
          Align = baLeft
          Left = 1060.661417320000000000
          Width = 51.590551180000000000
          Height = 20.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Antenna1."height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          Left = 1060.661417320000000000
          Top = 20.472480000000000000
          Width = 51.590551180000000000
          Height = 20.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          Left = 1215.433070860000000000
          Width = 51.590551180000000000
          Height = 20.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna1."loss"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_polarization: TfrxMemoView
          Align = baLeft
          Left = 1524.976377940000000000
          Width = 35.590551180000000000
          Height = 20.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna1."polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Align = baLeft
          Left = 1524.976377940000000000
          Top = 20.472480000000000000
          Width = 35.590551180000000000
          Height = 20.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna2."polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          Left = 1401.795275580000000000
          Width = 51.590551180000000000
          Height = 20.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna1."tilt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          Left = 1401.795275580000000000
          Top = 20.472480000000000000
          Width = 51.590551180000000000
          Height = 20.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna2."tilt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          Left = 1163.842519680000000000
          Width = 51.590551180000000000
          Height = 20.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna1."gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          Left = 1163.842519680000000000
          Top = 20.472480000000000000
          Width = 51.590551180000000000
          Height = 20.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna2."gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2radiation_class: TfrxMemoView
          Align = baLeft
          Left = 1453.385826760000000000
          Top = 20.472480000000000000
          Width = 71.590551180000000000
          Height = 20.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Linkend2."radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Align = baLeft
          Left = 1453.385826760000000000
          Width = 71.590551180000000000
          Height = 20.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Linkend1."radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2lat_str: TfrxMemoView
          Align = baLeft
          Left = 890.771653540000000000
          Top = 20.472480000000000000
          Width = 84.944881890000000000
          Height = 20.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<property2."lat">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Align = baLeft
          Left = 890.771653540000000000
          Width = 84.944881890000000000
          Height = 20.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<property1."lat">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Align = baLeft
          Left = 975.716535430000000000
          Top = 20.472480000000000000
          Width = 84.944881890000000000
          Height = 20.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<property2."lon">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Align = baLeft
          Left = 975.716535430000000000
          Width = 84.944881890000000000
          Height = 20.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<property1."lon">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Align = baLeft
          Width = 80.000000000000000000
          Height = 20.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[GetLine()]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Align = baLeft
          Top = 20.472480000000000000
          Width = 80.000000000000000000
          Height = 20.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[GetLine()]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Align = baLeft
          Left = 1112.251968500000000000
          Width = 51.590551180000000000
          Height = 20.472440940000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.0f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna1."azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Align = baLeft
          Left = 1112.251968500000000000
          Top = 20.472480000000000000
          Width = 51.590551180000000000
          Height = 20.472440940000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.0f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna2."azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          Left = 1215.433070860000000000
          Top = 20.472480000000000000
          Width = 51.590551180000000000
          Height = 20.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna2."loss"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1power_dBm: TfrxMemoView
          Align = baLeft
          Left = 1267.023622040000000000
          Width = 39.590551180000000000
          Height = 20.472480000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[<Linkend2."power_W">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Align = baLeft
          Left = 1267.023622040000000000
          Top = 20.472480000000000000
          Width = 39.590551180000000000
          Height = 20.472480000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[<Linkend2."power_W">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_ground_height: TfrxMemoView
          Align = baLeft
          Left = 1350.204724400000000000
          Top = 20.472480000000000000
          Width = 51.590551180000000000
          Height = 20.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna2."height_with_ground"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Align = baLeft
          Left = 1350.204724400000000000
          Width = 51.590551180000000000
          Height = 20.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna1."height_with_ground"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Align = baLeft
          Left = 1306.614173220000000000
          Width = 43.590551180000000000
          Height = 20.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Linkend1."rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Align = baLeft
          Left = 1306.614173220000000000
          Top = 20.472480000000000000
          Width = 43.590551180000000000
          Height = 20.472440940000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Linkend2."rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        Height = 128.944911180000000000
        Top = 76.000000000000000000
        Width = 1546.920999052500000000
        object Memo3: TfrxMemoView
          Align = baLeft
          Left = 254.771653540000000000
          Width = 636.000000000000000000
          Height = 128.944881890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1114#1056#181#1057#1027#1057#8218#1056#1109' '#1057#1026#1056#176#1056#183#1056#1112#1056#181#1057#8240#1056#181#1056#1029#1056#1105#1057#1039' '#1056#160#1056#160#1056#1038' ('#1056#176#1056#1169#1057#1026#1056#181#1057#1027')')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Align = baLeft
          Left = 1401.795300260000000000
          Width = 51.590551180000000000
          Height = 128.944881890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176' '#1056#1110#1056#187#1056#176#1056#1030#1056#1029#1056#1109#1056#1110#1056#1109' '#1056#187#1056#181#1056#1111#1056#181#1057#1027#1057#8218#1056#1108#1056#176' '#1056#1105#1056#183#1056#187#1057#1107#1057#8225 +
              #1056#181#1056#1029#1056#1105#1057#1039', '#1056#1110#1057#1026#1056#176#1056#1169)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          Left = 890.771653540000000000
          Top = 72.531541820000000000
          Width = 84.944894230000000000
          Height = 56.413369360000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1057#1027'.'#1057#8364'. ('#1056#1110#1057#1026#1056#176#1056#1169', '#1056#1112#1056#1105#1056#1029', '#1057#1027')')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          Left = 141.385826770000000000
          Width = 113.385826770000000000
          Height = 128.944881890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#160#1056#160#1056#1038)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          Left = 1306.614197900000000000
          Width = 43.590551180000000000
          Height = 128.944881890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1057#8249' '#1056#1119#1056#160#1056#8221'/'#1056#1119#1056#160#1056#1114', '#1056#1114#1056#8220#1057#8224)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Align = baLeft
          Left = 975.716547770000000000
          Top = 72.531541820000000000
          Width = 84.944894230000000000
          Height = 56.413369360000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1030'.'#1056#1169'. ('#1056#1110#1057#1026#1056#176#1056#1169', '#1056#1112#1056#1105#1056#1029', '#1057#1027')')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          Width = 80.000000000000000000
          Height = 128.944881890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1074#8222#8211' '
            #1056#1111'/'#1056#1111'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          Left = 1163.842544360000000000
          Width = 51.590551180000000000
          Height = 128.944886770000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1057#1036#1057#8222#1057#8222#1056#1105#1057#8224#1056#1105#1056#181#1056#1029#1057#8218' '#1057#1107#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1057#1039' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', dBi')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          Left = 1060.661442000000000000
          Width = 51.590551180000000000
          Height = 128.944881890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029' '#1056#160#1056#160#1056#1038' '#1056#1109#1057#8218' '#1056#1111#1056#1109#1056#1030#1056#181#1057#1026#1057#8230#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105' '#1056#8212#1056 +
              #181#1056#1112#1056#187#1056#1105', '#1056#1112)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          Left = 1524.976402620000000000
          Width = 35.590551180000000000
          Height = 128.944911180000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' '#1056#1119#1056#160#1056#8221'/'#1056#1119#1056#160#1056#1114)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          Left = 1267.023646720000000000
          Width = 39.590551180000000000
          Height = 128.944911180000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', '#1056#8217#1057#8218)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          Left = 1453.385851440000000000
          Width = 71.590551180000000000
          Height = 128.944911180000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#187#1056#176#1057#1027#1057#1027
            #1056#1105#1056#183#1056#187#1057#1107#1057#8225#1056#181#1056#1029#1056#1105#1057#1039)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Align = baLeft
          Left = 80.000000000000000000
          Width = 61.385826770000000000
          Height = 128.944881890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1074#8222#8211' '#1056#1038#1057#8218#1056#176#1056#1029#1057#8224#1056#1105#1056#1105' ('#1056#1109#1056#177#1056#1109#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' '#1056#1030' '#1057#1027#1056#181#1057#8218#1056#1105')')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Align = baLeft
          Left = 1350.204749080000000000
          Width = 51.590551180000000000
          Height = 128.944881890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249' '#1056#1029#1056#176#1056#1169' '#1057#1107#1057#1026#1056#1109#1056#1030#1056#1029#1056#181#1056#1112 +
              ' '#1056#1112#1056#1109#1057#1026#1057#1039', '#1056#1112)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Align = baLeft
          Left = 1112.251993180000000000
          Width = 51.590551180000000000
          Height = 128.944881890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218' '#1056#1110#1056#187#1056#176#1056#1030#1056#1029#1056#1109#1056#1110#1056#1109' '#1056#187#1056#181#1056#1111#1056#181#1057#1027#1057#8218#1056#1108#1056#176' '#1056#1105#1056#183#1056#187#1057#1107#1057#8225#1056#181#1056#1029#1056#1105#1057 +
              #1039' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', '#1056#1110#1057#1026#1056#176#1056#1169)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Align = baLeft
          Left = 890.771653540000000000
          Width = 169.889846770000000000
          Height = 72.531489760000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8220#1056#181#1056#1109#1056#1110#1057#1026#1056#176#1057#8222#1056#1105#1057#8225#1056#181#1057#1027#1056#1108#1056#1105#1056#181' '#1056#1108#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' ('#1056#1038#1056#1113'-42)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Align = baLeft
          Left = 1215.433095540000000000
          Width = 51.590551180000000000
          Height = 128.944911180000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#8218#1056#181#1057#1026#1056#1105' '#1056#1030' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1056#1109'-'#1057#8222#1056#1105#1056#1169#1056#181#1057#1026#1056#1029#1056#1109#1056#1112' '#1057#8218#1057#1026#1056#176#1056#1108#1057#8218#1056#181', dB')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
      end
    end
  end
  object frxReport_new: TfrxReport
    Version = '4.15.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.AllowEdit = False
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbTools, pbNavigator, pbExportQuick, pbNoFullScreen]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41121.613084722200000000
    ReportOptions.LastChange = 42090.555932268500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '//------------------------------------------------------------'
      '//function dBm_to_Wt_ (aValue: variant): double;'
      '//------------------------------------------------------------'
      '//begin'
      '//  if aValue = null then'
      '//    Result:=0'
      '//  else'
      '//    Result:=dBm_to_Wt (aValue);           '
      '  '
      '  '
      '//end;'
      '  '
      ''
      '  '
      '  '
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d %.2d %1.0f'#39';'
      'var'
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      '                           '
      ' // angle.Sec:=TruncFloat(angle.Sec,2);'
      ' '
      '    Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      '  if aValue<0 then'
      '    Result:='#39'-'#39'+Result;'
      'end;'
      '  '
      ''
      ''
      ' //------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eSec: Double;'
      ''
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      ''
      ''
      '   //    aValue:=99;                            '
      '          '
      ''
      '          '
      '  aDeg:=Trunc(aValue);'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      ''
      '//  e:= (aValue - Trunc(aValue));'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      ''
      ''
      '  aMin:=Trunc(e);'
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      
        '  //ShowMessage( FloatToStr(aSec));                             ' +
        '                                 '
      '                         '
      '  while aSec>(60-0.1) do'
      '  begin'
      '    aMin:=aMin+1;'
      '    aSec:=aSec-60;'
      '  end;    '
      ''
      '  while aMin>=60 do'
      '  begin'
      '      aMin:=aMin-60;'
      '      aDeg:=aDeg+1;'
      '  end;  '
      ''
      '  if aSec<0 then'
      '     aSec:=0;'
      '  '
      '  '
      '   if (aSec>=60) then'
      
        '     showMessage('#39'(aSec>=60)'#39');                                 ' +
        '                                '
      ''
      '             '
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      ''
      ''
      ''
      'end;'
      '  '
      '                       '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_11111111(aValue: Double; var aDeg,aMi' +
        'n:integer; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      '                   '
      ''
      ''
      'end;'
      '        '
      ''
      '       '
      'begin'
      ''
      'end.')
    Left = 470
    Top = 388
    Datasets = <
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end>
    Variables = <>
    Style = <
      item
        Name = 'Title'
        Color = clGray
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Header'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Group header'
        Color = 16053492
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Data'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
      end
      item
        Name = 'Group footer'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Header line'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 2.000000000000000000
      end>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 429.291750000000000000
      PaperHeight = 839.999166670000000000
      PaperSize = 256
      LeftMargin = 10.001250000000000000
      RightMargin = 10.001250000000000000
      TopMargin = 10.001250000000000000
      BottomMargin = 10.001250000000000000
      PrintIfEmpty = False
      object ReportTitle1: TfrxReportTitle
        Height = 86.929190000000000000
        Top = 16.000000000000000000
        Visible = False
        Width = 1546.920999052500000000
        StartNewPage = True
        object Memo1: TfrxMemoView
          Left = 8.098992860000000000
          Top = 5.219350950000000000
          Width = 1305.557648570000000000
          Height = 73.970801430000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#1119#1056#160#1056#1115#1056#8226#1056#1113#1056#1118' '#1056#167#1056#1106#1056#1038#1056#1118#1056#1115#1056#1118#1056#1116#1056#1115'-'#1056#1118#1056#8226#1056#160#1056#160#1056#152#1056#1118#1056#1115#1056#160#1056#152#1056#1106#1056#8250#1056#172#1056#1116#1056#1115#1056#8220#1056#1115' '#1056 +
              #1119#1056#8250#1056#1106#1056#1116#1056#1106' '#1056#160#1056#173#1056#1038
            
              #1056#160#1056#1106#1056#8221#1056#152#1056#1115#1056#160#1056#8226#1056#8250#1056#8226#1056#8482#1056#1116#1056#171#1056#1168' '#1056#1038#1056#1118#1056#1106#1056#1116#1056#166#1056#152#1056#8482' '#1056#152' '#1056#1038#1056#8226#1056#1118#1056#8226#1056#8482' '#1056#8216#1056#8226#1056#1038#1056#1119 +
              #1056#160#1056#1115#1056#8217#1056#1115#1056#8221#1056#1116#1056#1115#1056#8220#1056#1115' '#1056#160#1056#1106#1056#8221#1056#152#1056#1115#1056#8221#1056#1115#1056#1038#1056#1118#1056#1032#1056#1119#1056#1106
            
              #1056#1119#1056#1115' '#1056#1038#1056#1168#1056#8226#1056#1114#1056#8226' '#1042#171#1056#1118#1056#1115#1056#167#1056#1113#1056#1106' - '#1056#1118#1056#1115#1056#167#1056#1113#1056#1106#1042#187', '#1056#1038#1056#1118#1056#1106#1056#166#1056#152#1056#1115#1056#1116#1056#1106#1056#160#1056 +
              #1116#1056#171#1056#1168' '#1056#152' '#1056#1119#1056#8226#1056#160#1056#8226#1056#8221#1056#8217#1056#152#1056#8211#1056#1116#1056#171#1056#1168' '#1056#160#1056#8226#1056#1119#1056#1115#1056#160#1056#1118#1056#1106#1056#8211#1056#1116#1056#171#1056#1168' '#1056#1118#1056#8226#1056#8250#1056#8226#1056 +
              #8217#1056#152#1056#8212#1056#152#1056#1115#1056#1116#1056#1116#1056#171#1056#1168' '#1056#1038#1056#1118#1056#1106#1056#1116#1056#166#1056#152#1056#8482)
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Align = baRight
          Left = 1395.739799052500000000
          Top = 3.779530000000000000
          Width = 151.181200000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            #1056#1118#1056#176#1056#177#1056#187#1056#1105#1057#8224#1056#176' '#1056#164#1056#1038'-1')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 22.677165350000000000
        Top = 512.000000000000000000
        Width = 1546.920999052500000000
        Child = frxReport_new.Child3
        RowCount = 0
        object property1name: TfrxMemoView
          Align = baLeft
          Left = 49.133858267716500000
          Width = 130.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[property1."name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property1address: TfrxMemoView
          Align = baLeft
          Left = 309.133858267716500000
          Width = 400.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          DataField = 'address'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property1."address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1LinkEndTypename: TfrxMemoView
          Align = baLeft
          Left = 179.133858267716500000
          Width = 130.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Linkend1."LinkEndType_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1antenna_height: TfrxMemoView
          Align = baLeft
          Left = 983.133858267716500000
          Width = 50.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Antenna1."height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          Left = 1133.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna1."loss"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_polarization: TfrxMemoView
          Align = baLeft
          Left = 1433.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna1."polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          Left = 1333.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna1."tilt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          Left = 1083.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna1."gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Align = baLeft
          Left = 1383.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Linkend1."radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Align = baLeft
          Width = 49.133858267716500000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Align = baLeft
          Left = 1033.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.0f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna1."azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1power_dBm: TfrxMemoView
          Align = baLeft
          Left = 1183.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[<Linkend2."power_W">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Align = baLeft
          Left = 1283.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna1."height_with_ground"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Align = baLeft
          Left = 1233.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Linkend1."rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          Left = 709.133858267716500000
          Width = 100.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Align = baLeft
          Left = 809.133858267716500000
          Width = 174.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            
              '[DegreeToStr(<property1."lat_gck_2011">)] '#1057#1027#1057#8364'   [DegreeToStr(<p' +
              'roperty1."lon_gck_2011">)] '#1056#1030#1056#1169)
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        Height = 200.000000000000000000
        Top = 164.000000000000000000
        Width = 1546.920999052500000000
        Child = frxReport_new.Child1
        object Memo3: TfrxMemoView
          Align = baLeft
          Left = 309.133858270000000000
          Width = 500.000000000000000000
          Height = 30.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1114#1056#181#1057#1027#1057#8218#1056#1109' '#1057#1107#1057#1027#1057#8218#1056#176#1056#1029#1056#1109#1056#1030#1056#1108#1056#1105' '#1056#160#1056#173#1056#1038)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Align = baLeft
          Left = 1333.133858270000000000
          Width = 50.000000000000000000
          Height = 200.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176' '#1056#1110#1056#187#1056#176#1056#1030#1056#1029#1056#1109#1056#1110#1056#1109' '#1056#187#1056#181#1056#1111#1056#181#1057#1027#1057#8218#1056#1108#1056#176' '#1056#1105#1056#183#1056#187#1057#1107#1057#8225 +
              #1056#181#1056#1029#1056#1105#1057#1039', '#1056#1110#1057#1026#1056#176#1056#1169)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          Left = 179.133858270000000000
          Width = 130.000000000000000000
          Height = 200.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1116#1056#176#1056#1105#1056#1112#1056#181#1056#1029#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1056#181' '#1056#160#1056#173#1056#1038)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          Left = 1233.133858270000000000
          Width = 50.000000000000000000
          Height = 200.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1057#8249' '#1056#1119#1056#160#1056#8221'/'#1056#1119#1056#160#1056#1114', '#1056#1114#1056#8220#1057#8224)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          Width = 49.133858270000000000
          Height = 200.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1074#8222#8211' '
            #1056#1111'/'#1056#1111'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          Left = 1083.133858270000000000
          Width = 50.000000000000000000
          Height = 200.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1057#1036#1057#8222#1057#8222#1056#1105#1057#8224#1056#1105#1056#181#1056#1029#1057#8218' '#1057#1107#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1057#1039' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', dBi')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          Left = 983.133858270000000000
          Width = 50.000000000000000000
          Height = 200.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029' '#1056#160#1056#160#1056#1038' '#1056#1109#1057#8218' '#1056#1111#1056#1109#1056#1030#1056#181#1057#1026#1057#8230#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105' '#1056#8212#1056 +
              #181#1056#1112#1056#187#1056#1105', '#1056#1112)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          Left = 1433.133858270000000000
          Width = 50.000000000000000000
          Height = 200.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' '#1056#1119#1056#160#1056#8221'/'#1056#1119#1056#160#1056#1114)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          Left = 1183.133858270000000000
          Width = 50.000000000000000000
          Height = 200.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', '#1056#8217#1057#8218)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          Left = 1383.133858270000000000
          Width = 50.000000000000000000
          Height = 200.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#187#1056#176#1057#1027#1057#1027
            #1056#1105#1056#183#1056#187#1057#1107#1057#8225#1056#181#1056#1029#1056#1105#1057#1039)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Align = baLeft
          Left = 49.133858270000000000
          Width = 130.000000000000000000
          Height = 200.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1074#8222#8211' '#1056#1038#1057#8218#1056#176#1056#1029#1057#8224#1056#1105#1056#1105' ('#1056#1109#1056#177#1056#1109#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' '#1056#1030' '#1057#1027#1056#181#1057#8218#1056#1105')')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Align = baLeft
          Left = 1283.133858270000000000
          Width = 50.000000000000000000
          Height = 200.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249' '#1056#1029#1056#176#1056#1169' '#1057#1107#1057#1026#1056#1109#1056#1030#1056#1029#1056#181#1056#1112 +
              ' '#1056#1112#1056#1109#1057#1026#1057#1039', '#1056#1112)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Align = baLeft
          Left = 1033.133858270000000000
          Width = 50.000000000000000000
          Height = 200.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218' '#1056#1110#1056#187#1056#176#1056#1030#1056#1029#1056#1109#1056#1110#1056#1109' '#1056#187#1056#181#1056#1111#1056#181#1057#1027#1057#8218#1056#1108#1056#176' '#1056#1105#1056#183#1056#187#1057#1107#1057#8225#1056#181#1056#1029#1056#1105#1057 +
              #1039' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', '#1056#1110#1057#1026#1056#176#1056#1169)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Align = baLeft
          Left = 1133.133858270000000000
          Width = 50.000000000000000000
          Height = 200.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#8218#1056#181#1057#1026#1056#1105' '#1056#1030' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1056#1109'-'#1057#8222#1056#1105#1056#1169#1056#181#1057#1026#1056#1029#1056#1109#1056#1112' '#1057#8218#1057#1026#1056#176#1056#1108#1057#8218#1056#181', dB')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Align = baLeft
          Left = 309.133858270000000000
          Top = 30.236240000000000000
          Width = 400.000000000000000000
          Height = 170.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1106#1056#1169#1057#1026#1056#181#1057#1027' '#1057#1107#1057#1027#1057#8218#1056#176#1056#1029#1056#1109#1056#1030#1056#1108#1056#1105)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          Left = 709.133858270000000000
          Top = 30.236240000000000000
          Width = 100.000000000000000000
          Height = 170.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            
              #1056#8221#1056#1109#1056#1111#1056#1109#1056#187#1056#1029#1056#1105#1057#8218#1056#181#1056#187#1057#1034#1056#1029#1056#176#1057#1039' '#1056#1105#1056#1029#1057#8222#1056#1109#1057#1026#1056#1112#1056#176#1057#8224#1056#1105#1057#1039' '#1056#1109' '#1056#1112#1056#181#1057#1027#1057#8218#1056#181' ' +
              #1057#1107#1057#1027#1057#8218#1056#176#1056#1029#1056#1109#1056#1030#1056#1108#1056#1105' '#1056#160#1056#173#1056#1038)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          Align = baLeft
          Left = 809.133858270000000000
          Width = 174.000000000000000000
          Height = 200.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8220#1056#181#1056#1109#1056#1110#1057#1026#1056#176#1057#8222#1056#1105#1057#8225#1056#181#1057#1027#1056#1108#1056#1105#1056#181' '#1056#1108#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' ('#1056#8220#1056#1038#1056#1113'-2011)')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
      end
      object Child1: TfrxChild
        Height = 42.015691890000000000
        Top = 384.000000000000000000
        Width = 1546.920999052500000000
        Child = frxReport_new.Child2
        object Memo24: TfrxMemoView
          Align = baLeft
          Width = 49.133858267716500000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Align = baLeft
          Left = 49.133858267716500000
          Width = 130.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Align = baLeft
          Left = 179.133858267716500000
          Width = 130.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Align = baLeft
          Left = 309.133858267716500000
          Width = 400.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Align = baLeft
          Left = 709.133858267716500000
          Width = 100.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Align = baLeft
          Left = 983.133858267716500000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          Align = baLeft
          Left = 1033.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1110#1057#1026#1056#176#1056#1169'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Align = baLeft
          Left = 1083.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1169#1056#8216#1056#1105)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Align = baLeft
          Left = 1133.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1169#1056#8216)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Align = baLeft
          Left = 1183.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8217#1057#8218)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Align = baLeft
          Left = 1233.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1114#1056#8220#1057#8224)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Align = baLeft
          Left = 1283.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Align = baLeft
          Left = 1333.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1110#1057#1026#1056#176#1056#1169'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Align = baLeft
          Left = 1383.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Align = baLeft
          Left = 1433.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          Align = baLeft
          Left = 809.133858267716500000
          Width = 174.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1110#1057#1026#1056#176#1056#1169'., '#1056#1112#1056#1105#1056#1029'., '#1057#1027#1056#181#1056#1108'.')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Child2: TfrxChild
        Height = 42.015691890000000000
        Top = 448.000000000000000000
        Width = 1546.920999052500000000
        object Memo39: TfrxMemoView
          Align = baLeft
          Width = 49.133858267716500000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '1')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Align = baLeft
          Left = 49.133858267716500000
          Width = 130.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '2')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Align = baLeft
          Left = 179.133858267716500000
          Width = 130.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '3')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Align = baLeft
          Left = 309.133858267716500000
          Width = 400.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '4')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Align = baLeft
          Left = 709.133858267716500000
          Width = 100.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '4')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Align = baLeft
          Left = 983.133858267716500000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '6')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Align = baLeft
          Left = 1033.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '7')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Align = baLeft
          Left = 1083.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '8')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Align = baLeft
          Left = 1133.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '9')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Align = baLeft
          Left = 1183.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '10')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Align = baLeft
          Left = 1233.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '11')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Align = baLeft
          Left = 1283.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '12')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Align = baLeft
          Left = 1333.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '13')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Align = baLeft
          Left = 1383.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '14')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          Align = baLeft
          Left = 1433.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '15')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          Align = baLeft
          Left = 809.133858267716500000
          Width = 174.000000000000000000
          Height = 42.015691890000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '5')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Child3: TfrxChild
        Height = 22.677165350000000000
        Top = 556.000000000000000000
        Width = 1546.920999052500000000
        object property2name: TfrxMemoView
          Align = baLeft
          Left = 49.133858267716500000
          Width = 130.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[property2."name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2address: TfrxMemoView
          Align = baLeft
          Left = 309.133858267716500000
          Width = 400.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          DataField = 'address'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property2."address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          Left = 179.133858267716500000
          Width = 130.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Linkend2."LinkEndType_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          Left = 983.133858267716500000
          Width = 50.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Align = baLeft
          Left = 1433.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna2."polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          Left = 1333.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna2."tilt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          Left = 1083.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna2."gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2radiation_class: TfrxMemoView
          Align = baLeft
          Left = 1383.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Linkend2."radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Align = baLeft
          Width = 49.133858267716500000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Align = baLeft
          Left = 1033.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.0f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna2."azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          Left = 1133.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna2."loss"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Align = baLeft
          Left = 1183.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[<Linkend2."power_W">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_ground_height: TfrxMemoView
          Align = baLeft
          Left = 1283.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[antenna2."height_with_ground"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Align = baLeft
          Left = 1233.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Linkend2."rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Align = baLeft
          Left = 709.133858267716500000
          Width = 100.000000000000000000
          Height = 22.677165354330700000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Align = baLeft
          Left = 809.133858267716500000
          Width = 174.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            
              '[DegreeToStr(<property2."lat_gck_2011">)] '#1057#1027#1057#8364'   [DegreeToStr(<p' +
              'roperty2."lon_gck_2011">)] '#1056#1030#1056#1169)
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxReport_MTS_new: TfrxReport
    Version = '4.15.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.AllowEdit = False
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbTools, pbNavigator, pbExportQuick, pbNoFullScreen]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41121.613084722200000000
    ReportOptions.LastChange = 42821.434012303200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '//------------------------------------------------------------'
      '//function dBm_to_Wt_ (aValue: variant): double;'
      '//------------------------------------------------------------'
      '//begin'
      '//  if aValue = null then'
      '//    Result:=0'
      '//  else'
      '//    Result:=dBm_to_Wt (aValue);           '
      '  '
      '  '
      '//end;'
      '  '
      '   '
      ''
      '            '
      '  '
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d %.2d %1.0f'#39';'
      'var'
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      '                           '
      ' // angle.Sec:=TruncFloat(angle.Sec,2);'
      ' '
      '    Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      '  if aValue<0 then'
      '    Result:='#39'-'#39'+Result;'
      'end;'
      '  '
      ''
      '//------------------------------------------------------------'
      'function ChangeName (aValue: String): String;'
      '//------------------------------------------------------------'
      'begin   '
      '  Result:=Copy(aValue, 1, 2) + Copy(aValue, 5, 4)'
      'end;  '
      ''
      '                    '
      ''
      ' //------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eSec: Double;'
      ''
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      ''
      ''
      '   //    aValue:=99;                            '
      '          '
      ''
      '          '
      '  aDeg:=Trunc(aValue);'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      ''
      '//  e:= (aValue - Trunc(aValue));'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      ''
      ''
      '  aMin:=Trunc(e);'
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      
        '  //ShowMessage( FloatToStr(aSec));                             ' +
        '                                 '
      '                         '
      '  while aSec>(60-0.1) do'
      '  begin'
      '    aMin:=aMin+1;'
      '    aSec:=aSec-60;'
      '  end;    '
      ''
      '  while aMin>=60 do'
      '  begin'
      '      aMin:=aMin-60;'
      '      aDeg:=aDeg+1;'
      '  end;  '
      ''
      '  if aSec<0 then'
      '     aSec:=0;'
      '  '
      '  '
      '   if (aSec>=60) then'
      
        '     showMessage('#39'(aSec>=60)'#39');                                 ' +
        '                                '
      ''
      '             '
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      ''
      ''
      ''
      'end;'
      '  '
      '                       '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_11111111(aValue: Double; var aDeg,aMi' +
        'n:integer; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      '                   '
      ''
      ''
      'end;'
      '        '
      ''
      '       '
      'begin'
      ''
      'end.')
    Left = 608
    Top = 388
    Datasets = <
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end>
    Variables = <>
    Style = <
      item
        Name = 'Title'
        Color = clGray
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Header'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Group header'
        Color = 16053492
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Data'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
      end
      item
        Name = 'Group footer'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Header line'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 2.000000000000000000
      end>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 693.875083330000000000
      PaperHeight = 839.999166670000000000
      PaperSize = 256
      LeftMargin = 10.001250000000000000
      RightMargin = 10.001250000000000000
      TopMargin = 10.001250000000000000
      BottomMargin = 10.001250000000000000
      PrintIfEmpty = False
      object ReportTitle1: TfrxReportTitle
        Height = 55.000000000000000000
        Top = 16.000000000000000000
        Visible = False
        Width = 2546.921644873235000000
        StartNewPage = True
        Stretched = True
      end
      object MasterData1: TfrxMasterData
        Height = 22.677165350000000000
        Top = 252.000000000000000000
        Width = 2546.921644873235000000
        Child = frxReport_MTS_new.Child3
        RowCount = 0
        Stretched = True
        object property1name: TfrxMemoView
          Align = baLeft
          Width = 250.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1LinkEndTypename: TfrxMemoView
          Align = baLeft
          Left = 250.000000000000000000
          Width = 130.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property1."name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1antenna_height: TfrxMemoView
          Align = baLeft
          Left = 950.000000000000000000
          Width = 90.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna1."height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          Left = 1160.000000000000000000
          Width = 60.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          Left = 1680.000000000000000000
          Width = 70.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[<Linkend1."Bandwidth">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          Left = 1100.000000000000000000
          Width = 60.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."tilt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Align = baLeft
          Left = 1040.000000000000000000
          Width = 60.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.0f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1power_dBm: TfrxMemoView
          Align = baLeft
          Left = 1220.000000000000000000
          Width = 120.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2f'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[<Linkend1."linkendtype_name">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Align = baLeft
          Left = 1570.000000000000000000
          Width = 110.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[<Linkend1."redundancy_str">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Align = baLeft
          Left = 1410.000000000000000000
          Width = 70.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Align = baLeft
          Left = 510.000000000000000000
          Width = 100.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr(<property1."lat_wgs">) +  '#39#1057#1027#1057#8364#39']')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          Left = 380.000000000000000000
          Width = 130.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property1."code"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Align = baLeft
          Left = 610.000000000000000000
          Width = 100.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr(<property1."lon_wgs">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          Left = 710.000000000000000000
          Width = 120.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[<property1."lat_wgs">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Align = baLeft
          Left = 830.000000000000000000
          Width = 120.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[<property1."lon_wgs">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Align = baLeft
          Left = 1340.000000000000000000
          Width = 70.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[<Linkend1."band">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Align = baLeft
          Left = 1480.000000000000000000
          Width = 90.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Align = baLeft
          Left = 1750.000000000000000000
          Width = 70.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[<Linkend1."LinkID">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Align = baLeft
          Left = 1820.000000000000000000
          Width = 80.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[<Linkend1."modulation_type">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Align = baLeft
          Left = 1900.000000000000000000
          Width = 80.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[<Property1."ground_height">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Align = baLeft
          Left = 1980.000000000000000000
          Width = 80.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[<Link."status">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Align = baLeft
          Left = 2060.000000000000000000
          Width = 80.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        Height = 100.000000000000000000
        Top = 132.000000000000000000
        Width = 2546.921644873235000000
        object Memo11: TfrxMemoView
          Align = baLeft
          Left = 250.000000000000000000
          Width = 130.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1111#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          Left = 1410.000000000000000000
          Width = 70.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1057#8249' '#1056#1119#1056#160#1056#8221', '#1056#1114#1056#8220#1057#8224)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          Left = 1100.000000000000000000
          Width = 60.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1057#1107#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          Left = 950.000000000000000000
          Width = 90.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1030#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176',m')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          Left = 1220.000000000000000000
          Width = 120.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1057#8218#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Align = baLeft
          Width = 250.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1116#1056#176#1056#183#1056#1030#1056#176#1056#1029#1056#1105#1056#181' '#1056#187#1056#1105#1056#1029#1056#1108#1056#176)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Align = baLeft
          Left = 1040.000000000000000000
          Width = 60.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Align = baLeft
          Left = 1160.000000000000000000
          Width = 60.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1169#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1057#8249)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          Align = baLeft
          Left = 510.000000000000000000
          Width = 100.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1108#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1056#176' WGS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Align = baLeft
          Left = 380.000000000000000000
          Width = 130.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1074#8222#8211' '#1056#1111#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#1105' ('#1056#1113#1056#1109#1056#1169')')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          Left = 610.000000000000000000
          Width = 100.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1108#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1056#176' WGS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Align = baLeft
          Left = 710.000000000000000000
          Width = 120.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1057#8364#1056#1105#1057#1026#1056#1109#1057#8218#1056#176)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Align = baLeft
          Left = 830.000000000000000000
          Width = 120.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1169#1056#1109#1056#187#1056#1110#1056#1109#1057#8218#1056#176)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          Left = 1570.000000000000000000
          Width = 110.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1057#1026#1056#181#1056#183#1056#181#1057#1026#1056#1030#1056#1105#1057#1026#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1056#181)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          Left = 1680.000000000000000000
          Width = 70.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1025#1056#1105#1057#1026#1056#1105#1056#1029#1056#176' '#1056#1111#1056#1109#1056#187#1056#1109#1057#1027#1057#8249' MHz')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Align = baLeft
          Left = 1340.000000000000000000
          Width = 70.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1169#1056#1105#1056#176#1056#1111#1056#176#1056#183#1056#1109#1056#1029' GHz')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Align = baLeft
          Left = 1480.000000000000000000
          Width = 90.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1111#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Align = baLeft
          Left = 1750.000000000000000000
          Width = 70.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'Link ID')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Align = baLeft
          Left = 1820.000000000000000000
          Width = 80.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1114#1056#1109#1056#1169#1057#1107#1056#187#1057#1039#1057#8224#1056#1105#1057#1039)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Align = baLeft
          Left = 1900.000000000000000000
          Width = 80.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1030#1057#8249#1057#1027#1056#176#1057#8218#1056#176' '#1056#1029#1056#176#1056#1169' '#1057#1107#1057#1026#1056#1109#1056#1030#1056#1029#1056#181#1056#1112' '#1056#1112#1056#1109#1057#1026#1057#1039)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          Align = baLeft
          Left = 1980.000000000000000000
          Width = 80.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1038#1057#8218#1056#176#1057#8218#1057#1107#1057#1027)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Align = baLeft
          Left = 2060.000000000000000000
          Width = 80.000000000000000000
          Height = 100.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1032#1057#8225#1056#181#1057#8218' '#1056#1111#1056#176#1057#1027#1057#1027#1056#1105#1056#1030#1056#1029#1057#8249#1057#8230' '#1057#1036#1056#187#1056#181#1056#1112#1056#181#1056#1029#1057#8218#1056#1109#1056#1030)
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Child3: TfrxChild
        Height = 46.677165350000000000
        Top = 296.000000000000000000
        Width = 2546.921644873235000000
        Stretched = True
        object property2name: TfrxMemoView
          Align = baLeft
          Width = 250.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          Left = 250.000000000000000000
          Width = 130.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property2."name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          Left = 950.000000000000000000
          Width = 90.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          Left = 1680.000000000000000000
          Width = 70.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[<Linkend2."Bandwidth">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          Left = 1100.000000000000000000
          Width = 60.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."diameter"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Align = baLeft
          Left = 1040.000000000000000000
          Width = 60.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.0f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          Left = 1160.000000000000000000
          Width = 60.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."loss"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Align = baLeft
          Left = 1220.000000000000000000
          Width = 120.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2f'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[<Linkend2."linkendtype_name">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_ground_height: TfrxMemoView
          Align = baLeft
          Left = 1570.000000000000000000
          Width = 110.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[<Linkend2."redundancy_str">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Align = baLeft
          Left = 1410.000000000000000000
          Width = 70.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Align = baLeft
          Left = 510.000000000000000000
          Width = 100.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr(<property2."lat_wgs">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          Left = 380.000000000000000000
          Width = 130.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[property2."code"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Align = baLeft
          Left = 610.000000000000000000
          Width = 100.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr(<property2."lon_wgs">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Align = baLeft
          Left = 710.000000000000000000
          Width = 120.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[<property2."lat_wgs">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Align = baLeft
          Left = 830.000000000000000000
          Width = 120.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[<property2."lon_wgs">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Align = baLeft
          Left = 1340.000000000000000000
          Width = 70.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[<Linkend2."band">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Align = baLeft
          Left = 1480.000000000000000000
          Width = 90.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Align = baLeft
          Left = 1750.000000000000000000
          Width = 70.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[<Linkend2."LinkID">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Align = baLeft
          Left = 1820.000000000000000000
          Width = 80.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[<Linkend2."modulation_type">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Align = baLeft
          Left = 1900.000000000000000000
          Width = 80.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[<Property2."ground_height">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Align = baLeft
          Left = 1980.000000000000000000
          Width = 80.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[<Link."status">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Align = baLeft
          Left = 2060.000000000000000000
          Width = 80.000000000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDBDataset_link: TfrxDBDataset
    UserName = 'link'
    CloseDataSource = False
    DataSet = t_Link
    BCDToCurrency = False
    Left = 104
    Top = 312
  end
  object frxDBDataset_property1: TfrxDBDataset
    UserName = 'property1'
    CloseDataSource = False
    DataSet = t_Property1
    BCDToCurrency = False
    Left = 216
    Top = 312
  end
  object frxDBDataset_property2: TfrxDBDataset
    UserName = 'property2'
    CloseDataSource = False
    DataSet = t_Property2
    BCDToCurrency = False
    Left = 304
    Top = 312
  end
  object frxDBDataset_q_Linkend1: TfrxDBDataset
    UserName = 'Linkend1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'id=id'
      'name=name'
      'property_id=property_id'
      'linkendtype_id=linkendtype_id'
      'folder_id=folder_id'
      'objname=objname'
      'LinkEndType_mode_id=LinkEndType_mode_id'
      'LinkEndType_band_id=LinkEndType_band_id'
      'band=band'
      'subband=subband'
      'rx_freq_MHz=rx_freq_MHz'
      'tx_freq_MHz=tx_freq_MHz'
      'channel_number=channel_number'
      'channel_type=channel_type'
      'loss=loss'
      'redundancy=redundancy'
      'redundancy_type=redundancy_type'
      'equipment_type=equipment_type'
      'Protection=Protection'
      'passive_element_id=passive_element_id'
      'passive_element_loss=passive_element_loss'
      'power_max=power_max'
      'power_loss=power_loss'
      'power_dBm=power_dBm'
      'mode=mode'
      'modulation_type=modulation_type'
      'modulation_count=modulation_count'
      'signature_width=signature_width'
      'signature_height=signature_height'
      'equaliser_profit=equaliser_profit'
      'freq_spacing=freq_spacing'
      'channel_spacing=channel_spacing'
      'freq_channel_count=freq_channel_count'
      'kratnostByFreq=kratnostByFreq'
      'kratnostBySpace=kratnostBySpace'
      'failure_period=failure_period'
      'restore_period=restore_period'
      'kng=kng'
      'threshold_BER_3=threshold_BER_3'
      'threshold_BER_6=threshold_BER_6'
      'channel_width_MHz=channel_width_MHz'
      'comments=comments'
      'guid=guid'
      'date_created=date_created'
      'date_modify=date_modify'
      'user_created=user_created'
      'user_modify=user_modify'
      '____Repeater____=____Repeater____'
      'space_diversity=space_diversity'
      'LinkID=LinkID'
      'GOST_53363_modulation=GOST_53363_modulation'
      
        'GOST_53363_RX_count_on_combined_diversity=GOST_53363_RX_count_on' +
        '_combined_diversity'
      'rx_level_dBm=rx_level_dBm'
      'threshold_degradation_dB=threshold_degradation_dB'
      'ATPC_profit_dB=ATPC_profit_dB'
      'use_ATPC=use_ATPC'
      'bitrate_Mbps=bitrate_Mbps'
      'pmp_site_id=pmp_site_id'
      'pmp_sector_id=pmp_sector_id'
      '_____PMP____=_____PMP____'
      'cell_layer_id=cell_layer_id'
      'calc_radius_km=calc_radius_km'
      'calc_model_id=calc_model_id'
      'k0_open=k0_open'
      'k0_closed=k0_closed'
      'k0=k0'
      
        'NIIR1998_compensator_of_noises_Ixpic_dB=NIIR1998_compensator_of_' +
        'noises_Ixpic_dB'
      
        'NIIR1998_Coefficient_cross_polarization_protection_dB=NIIR1998_C' +
        'oefficient_cross_polarization_protection_dB'
      'azimuth=azimuth'
      'antenna_offset_horizontal_m=antenna_offset_horizontal_m'
      'channel_type_2=channel_type_2'
      'channel_number_2=channel_number_2'
      'tx_freq_MHz_2=tx_freq_MHz_2'
      'rx_freq_MHz_2=rx_freq_MHz_2'
      'subband_2=subband_2'
      'custom_reserve_radio=custom_reserve_radio'
      'LinkEndType_name=LinkEndType_name'
      'radiation_class=radiation_class'
      'bandwidth=bandwidth'
      'antenna_height=antenna_height'
      'antenna_diameter=antenna_diameter'
      'antenna_polarization=antenna_polarization'
      'antenna_gain=antenna_gain'
      'antenna_vert_width=antenna_vert_width'
      'antenna_horz_width=antenna_horz_width'
      'antenna_loss=antenna_loss'
      'antenna_tilt=antenna_tilt'
      'redundancy_str=redundancy_str'
      'antenna_ground_height=antenna_ground_height'
      'power_Wt=power_Wt'
      'tx_freq_GHz=tx_freq_GHz'
      'antenna_Azimuth=antenna_Azimuth'
      'power_max1=power_max1'
      'power_w=power_w')
    DataSet = t_LinkEnd1
    BCDToCurrency = False
    Left = 448
    Top = 312
  end
  object frxDBDataset_q_Linkend2: TfrxDBDataset
    UserName = 'Linkend2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'id=id'
      'name=name'
      'property_id=property_id'
      'linkendtype_id=linkendtype_id'
      'folder_id=folder_id'
      'objname=objname'
      'LinkEndType_mode_id=LinkEndType_mode_id'
      'LinkEndType_band_id=LinkEndType_band_id'
      'band=band'
      'subband=subband'
      'rx_freq_MHz=rx_freq_MHz'
      'tx_freq_MHz=tx_freq_MHz'
      'channel_number=channel_number'
      'channel_type=channel_type'
      'loss=loss'
      'redundancy=redundancy'
      'redundancy_type=redundancy_type'
      'equipment_type=equipment_type'
      'Protection=Protection'
      'passive_element_id=passive_element_id'
      'passive_element_loss=passive_element_loss'
      'power_max=power_max'
      'power_loss=power_loss'
      'power_dBm=power_dBm'
      'mode=mode'
      'modulation_type=modulation_type'
      'modulation_count=modulation_count'
      'signature_width=signature_width'
      'signature_height=signature_height'
      'equaliser_profit=equaliser_profit'
      'freq_spacing=freq_spacing'
      'channel_spacing=channel_spacing'
      'freq_channel_count=freq_channel_count'
      'kratnostByFreq=kratnostByFreq'
      'kratnostBySpace=kratnostBySpace'
      'failure_period=failure_period'
      'restore_period=restore_period'
      'kng=kng'
      'threshold_BER_3=threshold_BER_3'
      'threshold_BER_6=threshold_BER_6'
      'channel_width_MHz=channel_width_MHz'
      'comments=comments'
      'guid=guid'
      'date_created=date_created'
      'date_modify=date_modify'
      'user_created=user_created'
      'user_modify=user_modify'
      '____Repeater____=____Repeater____'
      'space_diversity=space_diversity'
      'LinkID=LinkID'
      'GOST_53363_modulation=GOST_53363_modulation'
      
        'GOST_53363_RX_count_on_combined_diversity=GOST_53363_RX_count_on' +
        '_combined_diversity'
      'rx_level_dBm=rx_level_dBm'
      'threshold_degradation_dB=threshold_degradation_dB'
      'ATPC_profit_dB=ATPC_profit_dB'
      'use_ATPC=use_ATPC'
      'bitrate_Mbps=bitrate_Mbps'
      'pmp_site_id=pmp_site_id'
      'pmp_sector_id=pmp_sector_id'
      '_____PMP____=_____PMP____'
      'cell_layer_id=cell_layer_id'
      'calc_radius_km=calc_radius_km'
      'calc_model_id=calc_model_id'
      'k0_open=k0_open'
      'k0_closed=k0_closed'
      'k0=k0'
      
        'NIIR1998_compensator_of_noises_Ixpic_dB=NIIR1998_compensator_of_' +
        'noises_Ixpic_dB'
      
        'NIIR1998_Coefficient_cross_polarization_protection_dB=NIIR1998_C' +
        'oefficient_cross_polarization_protection_dB'
      'azimuth=azimuth'
      'antenna_offset_horizontal_m=antenna_offset_horizontal_m'
      'channel_type_2=channel_type_2'
      'channel_number_2=channel_number_2'
      'tx_freq_MHz_2=tx_freq_MHz_2'
      'rx_freq_MHz_2=rx_freq_MHz_2'
      'subband_2=subband_2'
      'custom_reserve_radio=custom_reserve_radio'
      'LinkEndType_name=LinkEndType_name'
      'radiation_class=radiation_class'
      'bandwidth=bandwidth'
      'antenna_height=antenna_height'
      'antenna_diameter=antenna_diameter'
      'antenna_polarization=antenna_polarization'
      'antenna_gain=antenna_gain'
      'antenna_vert_width=antenna_vert_width'
      'antenna_horz_width=antenna_horz_width'
      'antenna_loss=antenna_loss'
      'antenna_tilt=antenna_tilt'
      'redundancy_str=redundancy_str'
      'antenna_ground_height=antenna_ground_height'
      'power_Wt=power_Wt'
      'tx_freq_GHz=tx_freq_GHz'
      'antenna_Azimuth=antenna_Azimuth'
      'power_max1=power_max1'
      'power_w=power_w')
    DataSet = t_LinkEnd2
    BCDToCurrency = False
    Left = 608
    Top = 312
  end
  object t_Property: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'id'
    MasterFields = 'Property1_id'
    MasterSource = ds_LInks
    TableName = 'property1'
    Left = 208
    Top = 24
  end
  object t_linkend: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'id'
    MasterFields = 'linkend1_id'
    MasterSource = ds_LInks
    TableName = 'LinkEnd1'
    Left = 312
    Top = 24
  end
end
