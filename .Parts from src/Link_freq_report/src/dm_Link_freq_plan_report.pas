unit dm_Link_freq_plan_report;

interface

uses
  SysUtils, Classes, Forms, frxDBSet, DB, ADODB, StdCtrls, Dialogs,
  frxDesgn, frxExportPDF, frxExportXLS, frxClass, frxExportRTF,

  u_doc,

  dm_Report,
  dm_Main_Data,

//  dm_MDB_fp,

  dm_Link_freq_plan,

  u_radio,

  u_FastReport,

 // dm_report,

  u_dll_geo_convert,

  u_geo_convert_new,

  u_geo,

  u_Link_freq_report_classes,

  u_func,
         

  u_const_db,

  u_files,

  u_db,
  u_run,


   dxmdaset, Controls, frxExportBaseDialog
  ;

type
  TdmLink_freq_plan_report = class(TDataModule)
    Button1: TButton;
    ds_LInks: TDataSource;
    frxReport: TfrxReport;
    frxDBDataset_link: TfrxDBDataset;
    frxRTFExport1: TfrxRTFExport;
    qry_LInks: TADOQuery;
    frxXLSExport1: TfrxXLSExport;
    frxReport_tele2: TfrxReport;
    frxPDFExport1: TfrxPDFExport;
    frxDesigner1: TfrxDesigner;
    frxReport_Tele2_CK42: TfrxReport;
    frxReport_new: TfrxReport;
    frxReport_MTS_new: TfrxReport;
    qry_LInksname: TStringField;
    qry_LInksproject_id: TIntegerField;
    qry_LInksLinkEnd1_Property_name: TStringField;
    qry_LInksLinkEnd1_Property_lat: TFloatField;
    qry_LInksLinkEnd1_Property_lon: TFloatField;
    qry_LInksLinkEnd1_LinkEndType_name: TStringField;
    qry_LInksLinkEnd1_power_Wt: TFloatField;
    qry_LInksLinkEnd1_rx_freq_MHz: TFloatField;
    qry_LInksLinkEnd1_tx_freq_MHz: TFloatField;
    qry_LInksLinkEnd1_antenna_gain: TStringField;
    qry_LInksLinkEnd1_antenna_height: TStringField;
    qry_LInksLinkEnd1_antenna_diameter: TStringField;
    qry_LInksLinkEnd1_antenna_polarization: TStringField;
    qry_LInksLinkEnd2_antenna_diameter: TStringField;
    qry_LInksLinkEnd2_antenna_polarization: TStringField;
    qry_LInksLinkEnd2_antenna_height: TStringField;
    qry_LInksLinkEnd2_antenna_gain: TStringField;
    qry_LInksLinkEnd2_antenna_tilt: TStringField;
    qry_LInksLinkEnd2_rx_freq_MHz: TFloatField;
    qry_LInksLinkEnd2_tx_freq_MHz: TFloatField;
    qry_LInksLinkEnd2_Property_name: TStringField;
    qry_LInksLinkEnd2_Property_lat: TFloatField;
    qry_LInksLinkEnd2_Property_lon: TFloatField;
    qry_LInksLinkEnd2_power_Wt: TFloatField;
    qry_LInksLinkEnd2_Property_code: TStringField;
    qry_LInksLinkEnd2_Property_address: TStringField;
    qry_LInksLinkEnd1_Property_code: TStringField;
    qry_LInksLinkEnd1_Property_address: TStringField;
    qry_LInksLinkEnd1_antenna_Azimuth: TFloatField;
    qry_LInksLinkEnd2_antenna_Azimuth: TFloatField;
    qry_LInksLinkEnd2_LinkEndType_name: TStringField;
    qry_LInksLinkEnd2_radiation_class: TStringField;
    qry_LInksLinkEnd1_radiation_class: TStringField;
    qry_LInksLinkEnd1_antenna_loss: TStringField;
    qry_LInksLinkEnd2_antenna_loss: TStringField;
    qry_LInksLinkEnd2_power_W: TFloatField;
    qry_LInksLinkEnd1_power_W: TFloatField;
    qry_LInksLinkEnd1_antenna_tilt: TStringField;
    qry_LInksLinkEnd2_antenna_ground_height: TStringField;
    qry_LInksLinkEnd1_antenna_ground_height: TStringField;
    qry_LInksLinkEnd1_Property_ground_height: TFloatField;
    qry_LInksLinkEnd2_Property_ground_height: TFloatField;
    qry_LInksLinkEnd2_bitrate_Mbps: TFloatField;
    qry_LInksLinkEnd1_bitrate_Mbps: TFloatField;
    qry_LInkslength_km: TFloatField;
    qry_LInksLinkEnd1_power_dBm: TFloatField;
    qry_LInksLinkEnd2_power_dBm: TFloatField;
    qry_LInksLinkEnd2_antenna_vert_width: TStringField;
    qry_LInksLinkEnd2_antenna_horz_width: TStringField;
    qry_LInksLinkEnd1_antenna_vert_width: TStringField;
    qry_LInksLinkEnd1_antenna_horz_width: TStringField;
    qry_LInksid: TIntegerField;
    qry_LInksLinkEnd1_name: TStringField;
    qry_LInksLinkEnd2_name: TStringField;
    qry_LInksLinkEnd1_bandwidth: TFloatField;
    qry_LInksLinkEnd2_bandwidth: TFloatField;
    qry_LInksLinkend1_property_lat_wgs: TFloatField;
    qry_LInksLinkend1_property_lon_wgs: TFloatField;
    qry_LInksLinkend2_property_lat_wgs: TFloatField;
    qry_LInksLinkend2_property_lon_wgs: TFloatField;
    qry_LInksLinkEnd1_band: TWideStringField;
    qry_LInksLinkEnd2_band: TWideStringField;
    qry_LInksLinkEnd1_LinkID: TStringField;
    qry_LInksLinkEnd2_LinkID: TStringField;
    qry_LInksLinkEnd1_modulation_type: TStringField;
    qry_LInksLinkEnd2_modulation_type: TStringField;
    qry_LInksLinkEnd1_property_lon_gck_2011: TFloatField;
    qry_LInksLinkEnd1_property_lat_gck_2011: TFloatField;
    qry_LInksLinkEnd2_property_lon_gck_2011: TFloatField;
    qry_LInksLinkEnd2_property_lat_gck_2011: TFloatField;
    qry_LInksLinkEnd1_slot: TIntegerField;
    qry_LInksLinkEnd1_IP_address: TStringField;
    qry_LInksLinkEnd2_slot: TIntegerField;
    qry_LInksLinkEnd2_IP_address: TStringField;
    qry_LInksrx_level_dBm: TFloatField;
    qry_LInksLinkEnd1_NE: TStringField;
    qry_LInksLinkEnd2_NE: TStringField;
//'    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
//    procedure qry_Linkend2CalcFields(DataSet: TDataSet);
 //   procedure qry_LInksAfterScroll(DataSet: TDataSet);
//    procedure qry_property1CalcFields(DataSet: TDataSet);

//    procedure qry_LinkEnd1CalcFields(DataSet: TDataSet);
    procedure qry_LInksCalcFields(DataSet: TDataSet);

  private
    FAllowUpdated : Boolean;
    FDataPrepared : Boolean;

//    FAntennaList1: TAntennaList;
//    FAntennaList2: TAntennaList;

//    FDataSet_antenna1: TDataSet;
//    FDataSet_antenna2: TDataSet;

    FfrxReport: TfrxReport;


    FAllowCalcFields: boolean;

    procedure DoExecExportByFilter(aExportFilter: TfrxCustomExportFilter; aExt:
        String);
//    procedure UpdateAntennaInfo;
   // procedure Open_Designer;
    procedure PrintReport;
    procedure ReportExportByFilter(aExportFilter: TfrxCustomExportFilter; aExt,
        aFileName: String);
   // procedure SetConnectionObject(aConnectionObject: _Connection);
    function ShowDesignReport: Boolean;
    { Private declarations }

    procedure Prepeare_Data;


  public
    Params: record
        //FileType : string;
        FileType : (ftExcel,ftWord,ftPDF);
        FileName : string;

        IsPreview : Boolean;
        IsOpenReportAfterDone : Boolean;

        ID_str : string;

        Report_ID : Integer;
      end;


    function DesignReport: boolean;
//    procedure DesignReport;

//    procedure Execute(aID_str: string);
    procedure Execute;

    class procedure Init;
    procedure Open1;

//    procedure SetADOConnection(aADOConnection: TADOConnection);
    procedure TestAdd;
  end;

var
  dmLink_freq_plan_report: TdmLink_freq_plan_report;

implementation



{$R *.dfm}


const
//  FLD_height_with_ground = 'height_with_ground';

  View_Link_freq_order  = 'reports.View_Link_freq_order';



class procedure TdmLink_freq_plan_report.Init;
begin
  if not Assigned(dmLink_freq_plan_report) then
    dmLink_freq_plan_report := TdmLink_freq_plan_report.Create(Application);

end;


// ---------------------------------------------------------------
procedure TdmLink_freq_plan_report.TestAdd;
// ---------------------------------------------------------------
const
  DEF_link_freq_plan = 'link_freq_plan';

  DEF_NAME_CK42    = 'FR - ����� ��� (CK42)';
  DEF_NAME_Tele2   = 'FR - ����� ��� (Tele2)';
  DEF_NAME_Beeline = 'FR - ����� ��� (Beeline)';

//  DEF_NAME_MTS_TURK = '����� ��� (MTS ������������)';

 // DEF_NAME_TELE2_rx_level = '����2 ��� � ��������� ';


  DEF_OLD = 'FR - ����� ��� (Beeline) ��� 2011';

  DEF_NAME_FC1 = '������� ��-1';


  DEF_ARR: array[0..3] of string =(
      DEF_NAME_CK42,
      DEF_NAME_Tele2,
      DEF_NAME_Beeline,
      DEF_NAME_FC1
      );

var
  I: Integer;
  k: Integer;
  
  rec: TdmReportAddRec;
  
  //dm_Main_Data
  
  s: string;
  sTempDir: string;
  
begin
  s:= Format('DELETE from %s where category=''%s''',
             [TBL_REPORT, DEF_link_freq_plan]); // 'select * from property where (id=:property2_id)');

  dmMain_Data.ADOConnection1.Execute(s);
 // dm_Main_Data
  

  sTempDir:=GetTempFileDir;


  FillChar(rec,SizeOf(rec),0);
  rec.Category:=DEF_link_freq_plan;



  rec.Name:=DEF_NAME_Tele2;
  rec.FileName:= sTempDir +  rec.Name + '.fr3';

  frxReport_tele2.SaveToFile(rec.FileName);

  k:=dmReport.Add(rec);

  DeleteFile(rec.FileName);

  // ---------------------------------------------------------------

  rec.Name:=DEF_NAME_Beeline;
  rec.FileName:= sTempDir +  rec.Name + '.fr3';

  frxReport.SaveToFile(rec.FileName);

  k:=dmReport.Add(rec);


  // ---------------------------------------------------------------

  rec.Name:=DEF_NAME_CK42;
  rec.FileName:= sTempDir +  rec.Name + '.fr3';

  frxReport_Tele2_CK42.SaveToFile(rec.FileName);

  k:=dmReport.Add(rec);


  // ---------------------------------------------------------------

  rec.Name:='������� ��-1';
  rec.FileName:= sTempDir +  rec.Name + '.fr3';

  frxReport_new.SaveToFile(rec.FileName);

  k:=dmReport.Add(rec);


//  // ---------------------------------------------------------------
//
//  rec.Name:=DEF_NAME_MTS_TURK;
//  rec.FileName:= sTempDir +  rec.Name + '.fr3';
//
//  frxReport_MTS_new.SaveToFile(rec.FileName);
//
//  k:=dmReport.Add(rec);

  // ---------------------------------------------------------------
{
  rec.Name:='����2 ��� � ���������';
  rec.FileName:= sTempDir +  rec.Name + '.fr3';

  frxReport1_Tele2_list.SaveToFile(rec.FileName);

  k:=dmReport.Add(rec);

}



//  ������� ��-1



  // ---------------------------------------------------------------

  DeleteFile(rec.FileName);

  ShowMessage('������� ������� ���������.');

end;




// ---------------------------------------------------------------
procedure TdmLink_freq_plan_report.ReportExportByFilter(aExportFilter:
    TfrxCustomExportFilter; aExt, aFileName: String);
// ---------------------------------------------------------------
var
  b: Boolean;
  sDocFile: string;
begin
  assert(aFileName<>'');

  aFileName:= ChangeFileExt(aFileName, aExt);

  b:= DeleteFile (aFileName);

  if FileExists(aFileName) then
     aFileName:= ChangeFileExt(aFileName, Format('_%d',[Random (100000)]) + aExt);



  aExportFilter.FileName := aFileName;
  aExportFilter.ShowDialog:= False;

  FfrxReport.Export(aExportFilter);


  if Eq(aExt, '.rtf') then
  begin
    sDocFile:=ChangeFileExt(aFileName, '.doc');

    b:= DeleteFile (sDocFile);

    if FileExists(sDocFile) then
       sDocFile:= ChangeFileExt(sDocFile, Format('_%d',[Random (100000)]) + '.doc');


    RTF_to_doc (aFileName,  sDocFile);

    DeleteFile (aFileName);

   aFileName:= ChangeFileExt(aFileName, '.doc');

  end;




 if Params.IsOpenReportAfterDone then
    ShellExec (aFileName,'');

//      aExportFilter.ShowDialog:= True;
end;



// ---------------------------------------------------------------
procedure TdmLink_freq_plan_report.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
//  if ADOConnection1.Connected then
//    ShowMessage('procedure TdmLink_freq_plan_report.FormCreate(Sender: TObject); - ADOConnection1.Connected ');

//  aQuery.Connection.Connected


  db_SetComponentADOConnection(Self, dmMain_Data.ADOConnection1 );

  Params.IsPreview := True;
  Params.FileType := ftExcel;

 // Open;

end;

// ---------------------------------------------------------------
procedure TdmLink_freq_plan_report.Open1;
// ---------------------------------------------------------------
begin


  //---------------------------
  // cala fields
  //---------------------------
  qry_LInks.Fields.Clear;

  db_OpenQuery(qry_LInks, 'select * from '+ View_Link_freq_order +'  where id=0');

//  db_View(qry_LInks);

  db_CreateFieldsForDataset(qry_LInks);


  db_CreateCalculatedField(qry_LInks, 'Linkend1_property_' + FLD_lat_wgs, ftFloat);
  db_CreateCalculatedField(qry_LInks, 'Linkend1_property_' + FLD_lon_wgs, ftFloat);

  db_CreateCalculatedField(qry_LInks, 'Linkend1_property_' + FLD_LAT_CK95, ftFloat);
  db_CreateCalculatedField(qry_LInks, 'Linkend1_property_' + FLD_lon_CK95, ftFloat);

  db_CreateCalculatedField(qry_LInks, 'Linkend1_property_' + FLD_LAT_GCK_2011, ftFloat);
  db_CreateCalculatedField(qry_LInks, 'Linkend1_property_' + FLD_LON_GCK_2011, ftFloat);


  db_CreateCalculatedField(qry_LInks, 'Linkend2_property_' + FLD_lat_wgs, ftFloat);
  db_CreateCalculatedField(qry_LInks, 'Linkend2_property_' + FLD_lon_wgs, ftFloat);

  db_CreateCalculatedField(qry_LInks, 'Linkend2_property_' + FLD_LAT_CK95, ftFloat);
  db_CreateCalculatedField(qry_LInks, 'Linkend2_property_' + FLD_lon_CK95, ftFloat);

  db_CreateCalculatedField(qry_LInks, 'Linkend2_property_' + FLD_LAT_GCK_2011, ftFloat);
  db_CreateCalculatedField(qry_LInks, 'Linkend2_property_' + FLD_LON_GCK_2011, ftFloat);

  // ---------------------------------------------------------------
  db_CreateCalculatedField(qry_LInks, 'property1_' + FLD_lat_wgs, ftFloat);
  db_CreateCalculatedField(qry_LInks, 'property1_' + FLD_lon_wgs, ftFloat);

  db_CreateCalculatedField(qry_LInks, 'property1_' + FLD_LAT_CK95, ftFloat);
  db_CreateCalculatedField(qry_LInks, 'property1_' + FLD_lon_CK95, ftFloat);

  db_CreateCalculatedField(qry_LInks, 'property1_' + FLD_LAT_GCK_2011, ftFloat);
  db_CreateCalculatedField(qry_LInks, 'property1_' + FLD_LON_GCK_2011, ftFloat);


  db_CreateCalculatedField(qry_LInks, 'property2_' + FLD_lat_wgs, ftFloat);
  db_CreateCalculatedField(qry_LInks, 'property2_' + FLD_lon_wgs, ftFloat);

  db_CreateCalculatedField(qry_LInks, 'property2_' + FLD_LAT_CK95, ftFloat);
  db_CreateCalculatedField(qry_LInks, 'property2_' + FLD_lon_CK95, ftFloat);

  db_CreateCalculatedField(qry_LInks, 'property2_' + FLD_LAT_GCK_2011, ftFloat);
  db_CreateCalculatedField(qry_LInks, 'property2_' + FLD_LON_GCK_2011, ftFloat);


  FAllowCalcFields:=True;

//  db_View(qry_LInks);



//  db_CreateFields_FromSrcToDest(qry_Property1, qry_Property2);

  {
  //---------------------------
  // cala fields
  //---------------------------
  db_OpenQuery(qry_LinkEnd1, 'SELECT * FROM '+ view_Linkend_report  +' where id=0');

  db_CreateFieldsForDataset(qry_LinkEnd1);
//  db_CreateCalculatedField(qry_LinkEnd1, FLD_POWER_Wt, ftFloat);
  db_CreateCalculatedField(qry_LinkEnd1, FLD_POWER_W, ftFloat);

  db_CreateFields_FromSrcToDest(qry_LinkEnd1, qry_LinkEnd2);

  }


//  db_View(FDataSet_link);


   {

  qry_Property1.SQL.Text:= 'select * from '+TBL_property+' where (id=:property1_id)';
  qry_Property2.SQL.Text:= 'select * from '+TBL_property+' where (id=:property2_id)';

  qry_Linkend1.SQL.Text:= 'select * from '+ view_Linkend_report +' where (id=:Linkend1_id)';
  qry_Linkend2.SQL.Text:= 'select * from '+ view_Linkend_report +' where (id=:Linkend2_id)';

  }

//  qry_Antennas1.SQL.Text:= 'select * from '+ TBL_LINKEND_ANTENNA +' where (Linkend_id=:id)';
//  qry_Antennas2.SQL.Text:= qry_Antennas1.SQL.Text;


//  FAntennaList1 := TAntennaList.Create();
//  FAntennaList2 := TAntennaList.Create();

  FfrxReport:=frxReport;



end;


// ---------------------------------------------------------------
procedure TdmLink_freq_plan_report.PrintReport;
// ---------------------------------------------------------------
var
  sTempFileName: string;
begin
  frxReport.PrepareReport;

  sTempFileName := Params.FileName;


  case Params.FileType of
    ftExcel : ReportExportByFilter(frxXLSExport1, '.xls', sTempFileName);
    ftWord  : ReportExportByFilter(frxRTFExport1, '.doc', sTempFileName);
    ftPDF   : ReportExportByFilter(frxPDFExport1, '.pdf', sTempFileName);
  end;

end;


// ---------------------------------------------------------------
procedure TdmLink_freq_plan_report.DoExecExportByFilter(aExportFilter:
    TfrxCustomExportFilter; aExt: String);
// ---------------------------------------------------------------
begin
  assert(params.FileName<>'');

  aExportFilter.FileName:= ChangeFileExt(params.FileName, aExt);
  aExportFilter.ShowDialog:= False;
  frxReport.Export(aExportFilter);
//      aExportFilter.ShowDialog:= True;
end;


// ---------------------------------------------------------------
function TdmLink_freq_plan_report.ShowDesignReport: Boolean;
// ---------------------------------------------------------------
var
  b: Boolean;
  oStream1: TMemoryStream;
  oStream2: TMemoryStream;
begin
  oStream1:=TMemoryStream.Create;
  oStream2:=TMemoryStream.Create;

  frxReport.SaveToStream(oStream1);


  frxReport.DesignReport(True);
//  frxReport.m
  b:=frxReport.Modified;

  frxReport.SaveToStream(oStream2);


  b:=not u_func.CompareMemoryStreams(oStream1,oStream2);

  Result := b;

  FreeAndNil(oStream1);
  FreeAndNil(oStream2);

end;

// ---------------------------------------------------------------
function TdmLink_freq_plan_report.DesignReport: boolean;
// ---------------------------------------------------------------
var
  s: string;
  sTemplateFileName: string;
begin
  s:=Format('SELECT * FROM %s where id=0', [View_Link_freq_order]);

 // /'/  SELECT * FROM	view_Link  where id IN (@ID_STR)  order by name

  db_OpenQuery(qry_LInks, s);

  frxDBDataset_UpdateAll(Self);


//  sTemplateFileName := g_Link_Report_Param.FileDir + Format('report_%d.rep', [g_Link_Report_Param.Report_ID]);
  sTemplateFileName := Params.FileName + '_tmp_rep.fr3';

 // Assert(Params.Report_ID>0);

  dmReport.SaveToFile(Params.Report_ID, sTemplateFileName);
                     
  frxReport.LoadFromFile(sTemplateFileName);

  if ShowDesignReport then
  begin
    frxReport.SaveToFile(sTemplateFileName);

    dmReport.LoadFromFile(Params.Report_ID, sTemplateFileName);

  end;

end;



// ---------------------------------------------------------------
procedure TdmLink_freq_plan_report.Execute;
// ---------------------------------------------------------------
var
  b: Boolean;
  sTempFileName: string;
  s: string;
begin
  Prepeare_Data();


    {
    db_View(qry_LInks_new);
    db_View(mem_Antenna1);
    db_View(mem_Antenna2);
   }

 ///// tfrm_test.ExecDlg;


  Assert(Params.Report_ID>0);


//  sTempFileName :=  'd:\1111111111111111.fr3';
  sTempFileName := GetTempFileNameWithExt('.fr3');

  b:=dmReport.SaveToFile(Params.Report_ID, sTempFileName);

  Assert(b, ' b:=dmReport.SaveToFile(Params.Report_ID, sTempFileName);');

 // sHOWModal;


//  dmMain.ADOConnection1.Execute(s);

  frxReport.LoadFromFile(sTempFileName);

  DeleteFile(sTempFileName);



//
//  Open_Designer;
//
//  Exit;
//

//  frxReport.Lo


  if Params.IsPreview then
    FfrxReport.ShowReport

  else
    PrintReport();


//  exec sp_Link_report '109902,109926,109903,109933,109904,'

end;


// ---------------------------------------------------------------
procedure TdmLink_freq_plan_report.Prepeare_Data;
// ---------------------------------------------------------------
var
  b: Boolean;
  sTempFileName: string;
  s: string;
begin
  if FDataPrepared then
    exit;


//  Open;

//  s:=Format('exec %s ''%s''', [sp_Link_report, aID_str]);
  Assert(Params.ID_str<>'');


//    db_OpenQuery(qry_LInks, 'select * from '+ DEF_View_Link_freq_order +'  where id=0');

  Assert (Params.ID_str <> '');


  s:=Format('SELECT * FROM %s where id IN (%s) order by name', [View_Link_freq_order, Params.ID_str]);

 // /'/  SELECT * FROM	view_Link  where id IN (@ID_STR)  order by name

  db_OpenQuery(qry_LInks, s);

  Assert (qry_LInks.RecordCount > 0, 'qry_LInks.RecordCount = 0');





  frxDBDataset_UpdateAll(Self);

  // FDataPrepared:=True;

//  exec sp_Link_report '109902,109926,109903,109933,109904,'

end;




// ---------------------------------------------------------------
procedure TdmLink_freq_plan_report.qry_LInksCalcFields(DataSet: TDataSet);
// ---------------------------------------------------------------

    procedure DoExec (aPrefix: string);
    var
      bl: TBLPoint;
      bl_WGS: TBLPoint;
      bl_CK95: TBLPoint;
      bl_GCK: TBLPoint;

    begin
      bl.B:=DataSet.FieldByName(aPrefix+ FLD_lat).AsFloat;
      bl.L:=DataSet.FieldByName(aPrefix + FLD_lon).AsFloat;

      bl_WGS :=geo_Pulkovo42_to_WGS84(bl);
      bl_CK95:=geo_Pulkovo42_to_CK_95(bl);

       CK42_to_GSK_2011(bl.B, bl.L, bl_GCK.B, bl_GCK.L);
    //      external DEF_LIB_geo_convert;


      DataSet[aPrefix + FLD_lat_wgs]:=bl_WGS.B;
      DataSet[aPrefix + FLD_lon_wgs]:=bl_WGS.L;

      DataSet[aPrefix + FLD_lat_CK95]:=bl_CK95.B;
      DataSet[aPrefix + FLD_lon_CK95]:=bl_CK95.L;


      DataSet[aPrefix + FLD_LAT_GCK_2011]:=bl_GCK.B;
      DataSet[aPrefix + FLD_LON_GCK_2011]:=bl_GCK.L;

    end;




begin
  if not FAllowCalcFields then
    exit;

  DoExec ('Linkend1_property_');
  DoExec ('Linkend2_property_');

  DoExec ('property1_');
  DoExec ('property2_');


end;


end.


