unit d_Setup;

interface

uses
  rxPlacemnt, ActnList, cxVGrid, AppEvnts, 
  SysUtils, Variants, Classes, Controls, Forms, StdCtrls, rxToolEdit, ExtCtrls,

  dm_Link_freq_plan_report,

 // u_cx_VGrid_export,

  u_cx_VGrid,

  u_db,

 // dm_Main,

  dm_Link_freq_plan,

  u_func, cxControls, cxInplaceContainer, Mask, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxEdit, cxDBLookupComboBox, cxCheckBox,
  cxRadioGroup, System.Actions;

type
  Tdlg_Setup = class(TForm)
    FilenameEdit1: TFilenameEdit;
    Bevel1: TBevel;
    FormStorage1: TFormStorage;
    Label1: TLabel;
    ActionList1: TActionList;
    act_DesignReport: TAction;
    Button4: TButton;
    act_Copy: TAction;
    cxVerticalGrid1: TcxVerticalGrid;
    row_Template_ID: TcxEditorRow;
    cxVerticalGrid1CategoryRow4: TcxCategoryRow;
    row_IsOpenReportAfterDone: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    row_FileType: TcxEditorRow;
    Panel1: TPanel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    Button5: TButton;
    act_Preview: TAction;
    act_Test_Add: TAction;
    Button1: TButton;
    ApplicationEvents1: TApplicationEvents;
    Action1: TAction;
    Button2: TButton;
    procedure FormDestroy(Sender: TObject);
    procedure act_DesignReportExecute(Sender: TObject);
//    procedure act_Test_AddExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);

    procedure row_FileTypeEditPropertiesEditValueChanged(Sender: TObject);

  private
    FID_str: string;

    FRegPath: string;
     
    procedure ChangeFileType;
 
    procedure Exec;

    procedure SaveToClass;
    procedure Test_Add;
  public
    class function ExecDlg(aID_str: string): boolean;

  end;


implementation

{$R *.dfm}


// ---------------------------------------------------------------
procedure Tdlg_Setup.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
var
  s: string;
begin
  Caption:='����� ���:' +  GetAppVersionStr();

 // RadioGroup1.Caption:='���';

  act_Preview.Caption:='�������� ����� �������';
//  cb_Open_after_done.Caption:='������� ����� ������������';

(*
  ShowMessage(FormStorage1.StoredProps.Text);


  StrToFile(FormStorage1.StoredProps.Text, 'd:\111.txt');


  FormStorage1.StoredProps.Add(cb_Open_after_done.Name + '.Checked');
  FormStorage1.StoredProps.Add(cb_Preview.Name + '.Checked');

  ShowMessage(FormStorage1.StoredProps.Text);


  FormStorage1.Active := True;

  s := '';
*)
//  FormStorage1.RestoreFormPlacement

  act_DesignReport.Caption:='�������� ������';


//  cx_V

  //cx_VerticalGrid_SaveToReg(aVerticalGrid: TcxVerticalGrid; aRegPath: string);
  FRegPath:= 'Software\Onega\Forms\link_report_setup1';



  row_IsOpenReportAfterDone.Properties.Caption:='������� ����� ������������';


//  dmLink_freq_plan.Open;


  if dmLink_freq_plan.qry_Reports.RecordCount=0 then
    Test_Add();


  cx_VerticalGrid_LoadFromReg(cxVerticalGrid1, FRegPath);

  cx_InitVerticalGrid(cxVerticalGrid1);



//  Dialog_SetDefaultSize(Self);

end;


procedure Tdlg_Setup.FormDestroy(Sender: TObject);
begin
  cx_VerticalGrid_SaveToReg(cxVerticalGrid1, FRegPath);
end;



// ---------------------------------------------------------------
procedure Tdlg_Setup.Test_Add;
// ---------------------------------------------------------------
var
  iID: Integer;
begin
  dmLink_freq_plan_report.TestAdd;
  dmLink_freq_plan.Open;

  iID:=dmLink_freq_plan.qry_Reports.FieldByName(FLD_ID).AsInteger;

  row_Template_ID.Properties.Value:=iID;

end;


// ---------------------------------------------------------------
procedure Tdlg_Setup.act_DesignReportExecute(Sender: TObject);
// ---------------------------------------------------------------
begin
  // -------------------------
  if Sender=act_Test_Add then
  // -------------------------
  begin
   Test_Add;

  end else

  // -------------------------
  if Sender=act_DesignReport then
  // -------------------------
  begin
    SaveToClass;
    //dmLink_freq_plan_report.Params.IsPreview := True;

   // dmLink_freq_plan_report.Open;
    dmLink_freq_plan_report.DesignReport();
  end else


//
//  // -------------------------
//  if Sender=act_Copy then
//  begin
//  //  Copy() ;
//
//  end;

  // -------------------------
  if Sender=act_Preview then
  // -------------------------
  begin
    SaveToClass();

//    IsPreview := True;
    dmLink_freq_plan_report.Params.IsPreview := True;
    dmLink_freq_plan_report.Execute;

//    SaveToClass(g_Link_Report_Param);
//
//    g_Link_Report_Param.IsPreview := True;
//
//    Run();
  end ;

end;


procedure Tdlg_Setup.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
var
  b: Boolean;
begin


 // b:=dmLink_freq_plan.Check_Report_ID (AsInteger( row_Template_ID.Properties.Value));


  btn_Ok.Enabled:= //b;
      AsInteger( row_Template_ID.Properties.Value) > 0;


////   not VarIsNull( row_Template_ID.Properties.Value);
   //and
 //   (Trim(AsString(row_folder.Properties.Value))<>'')

  act_Preview.Enabled:=btn_Ok.Enabled;

end;

procedure Tdlg_Setup.Button2Click(Sender: TObject);
begin
//  dmMain.ADOConnection.Execute('DELETE FROM report');
 // dmMain.ADOConnection.Execute('DELETE FROM Map_Desktops');

end;


//--------------------------------------------------------------------
class function Tdlg_Setup.ExecDlg(aID_str: string): boolean;
//--------------------------------------------------------------------
begin
  with Tdlg_Setup.Create(Application) do
  try
    FID_str :=aID_str;

    Result:=(ShowModal=mrOk);

    if result then
      Exec;
      
  finally
    Free;
  end;

//  if Result then
    

end;


// ---------------------------------------------------------------
procedure Tdlg_Setup.Exec;
// ---------------------------------------------------------------
begin
  SaveToClass;
  //dmLink_freq_plan_report.Params.IsPreview := True;
  dmLink_freq_plan_report.Params.IsPreview := False;
  dmLink_freq_plan_report.Execute;;
end;

// ---------------------------------------------------------------
procedure Tdlg_Setup.SaveToClass;
// ---------------------------------------------------------------
var
  sValue: string;
begin
  sValue := row_FileType.Properties.Value;

  if Eq(sValue,'word')  then dmLink_freq_plan_report.Params.FileType:=ftWord else
  if Eq(sValue,'excel') then dmLink_freq_plan_report.Params.FileType:=ftExcel else
  if Eq(sValue,'pdf')   then dmLink_freq_plan_report.Params.FileType:=ftPDF
      else raise Exception.Create('');

(*

  with dmLink_freq_plan_report.Params do
    case row_FileType.Properties.Value of
      0: FileType:=ftExcel;
      1: FileType:=ftword;
      2: FileType:=ftpdf;
    end;
*)
//  FileType:=s;
//  frm_Beeline_FreqPlan.Params.FileType:=s;

 Assert(FID_str<>'');
  dmLink_freq_plan_report.Params.ID_str       :=FID_str;

  dmLink_freq_plan_report.Params.FileName      :=FilenameEdit1.FileName;
  dmLink_freq_plan_report.Params.IsOpenReportAfterDone:= row_IsOpenReportAfterDone.Properties.Value;

  dmLink_freq_plan_report.Params.Report_ID:=AsInteger (row_Template_ID.Properties.Value);

//  cb_Preview.Checked;
//  dmLink_freq_plan_report.Params.IsOpenReportAfterDone:=cb_Open_after_done.Checked;

//  dmLink_freq_plan_report.Params.Report_ID:= AsInteger (DBLookupComboBox1.KeyValue);

  Assert(dmLink_freq_plan_report.Params.Report_ID>0);

end;


// ---------------------------------------------------------------
procedure Tdlg_Setup.ChangeFileType;
// ---------------------------------------------------------------
var
  sExt: string;
  sValue: string;
begin
  sValue := row_FileType.Properties.Value;

  if Eq(sValue,'word') then sExt:='.doc' else
  if Eq(sValue,'excel') then sExt:='.xls' else
  if Eq(sValue,'pdf') then sExt:='.pdf';


  FilenameEdit1.FileName:= ChangeFileExt(FilenameEdit1.FileName, sExt);

end;



procedure Tdlg_Setup.row_FileTypeEditPropertiesEditValueChanged(
  Sender: TObject);
begin
  ChangeFileType();
end;


end.


