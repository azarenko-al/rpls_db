unit dm_MDB_fp;

interface

uses
  ADODB, Db,  Forms, Classes, SysUtils, Dialogs,

  u_const_db,

  u_db,

  

  u_db_mdb , frxClass, frxDBSet

  ;

type
  TdmMDB111111111 = class(TDataModule)
    ADOConnection_MDB: TADOConnection;
    t_Property1: TADOTable;
    t_LinkEnd1: TADOTable;
    t_Property2: TADOTable;
    t_LinkEnd2: TADOTable;
    t_Antennas1111: TADOTable;
    t_Antennas2222: TADOTable;
    t_Link: TADOTable;
    ds_property1: TDataSource;
    ds_LInks: TDataSource;
    ds_property2: TDataSource;
    ds_Linkend1: TDataSource;
    ds_Linkend2: TDataSource;
    frxReport: TfrxReport;
    frxReport_tele2: TfrxReport;
    frxReport_Tele2_CK42: TfrxReport;
    frxReport_new: TfrxReport;
    frxReport_MTS_new: TfrxReport;
    frxDBDataset_link: TfrxDBDataset;
    frxDBDataset_property1: TfrxDBDataset;
    frxDBDataset_property2: TfrxDBDataset;
    frxDBDataset_q_Linkend1: TfrxDBDataset;
    frxDBDataset_q_Linkend2: TfrxDBDataset;
    t_Property: TADOTable;
    t_linkend: TADOTable;
//    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FtempFileName : string;


    function OpenMDB(aMDBFileName: string): Boolean;

    function CloseMDB: Integer;

//    procedure Open_data;

  public


// TODO: BeginTransaction
//  procedure BeginTransaction;

    function CreateMDB(aFileName: string): Boolean;

    procedure CreateTableFromDataset(aTblName: string; aDataSet_src: TDataSet;
        aTable: TADOTable);
//    TableNameList: TStringList;

    class procedure Init;



  end;

var
  dmMDB111111111: TdmMDB111111111;




implementation
  {$R *.DFM}


function TdmMDB111111111.CloseMDB: Integer;
begin
  ADOConnection_MDB.Close;

end;




//------------------------------------------------------------------------------
class procedure TdmMDB111111111.Init;
//------------------------------------------------------------------------------
begin
  if not Assigned(dmMDB) then
    Application.CreateForm(TdmMDB, dmMDB);
end;


procedure TdmMDB111111111.DataModuleCreate(Sender: TObject);
begin
  if ADOConnection_MDB.Connected then
    ShowMessage('ADOConnection_MDB.Connected');

  ADOConnection_MDB.Close;

//  Assert(not ADOConnection_MDB.Connected);

end;



//------------------------------------------------------------------------------
function TdmMDB111111111.CreateMDB(aFileName: string): Boolean;
//------------------------------------------------------------------------------

var
  s: string;

 //  arr: array of TdbFieldRec;

begin
  Assert(aFileName<>'', 'Value <=0');

  aFileName:=ChangeFileExt(aFileName, '.mdb');

  DeleteFile (aFileName);

  Result := mdb_CreateFileAndOpen(aFileName, ADOConnection_MDB);

{
  arr:=  Mak( [
      db_Field(FLD_link_id,  ftInteger),

      db_Field(FLD_height,   ftString),
      db_Field(FLD_polarization, ftString, 4),
      db_Field(FLD_diameter, ftString),


      db_Field(FLD_height_with_ground    , ftString),

      db_Field(FLD_VERT_WIDTH    , ftString),
      db_Field(FLD_VERT_WIDTH    , ftString),

      db_Field(FLD_AZIMUTH    , ftString),
      db_Field(FLD_LOSS    , ftString),
      db_Field(FLD_gain    , ftString),

      db_Field(FLD_tilt    , ftString),


    ]);

}
  {
 mdb_CreateTableFromArr(ADOConnection_MDB, 'Antennas1',
    [
      db_Field(FLD_link_id,  ftInteger),

      db_Field(FLD_height,   ftString),
      db_Field(FLD_polarization, ftString, 4),
      db_Field(FLD_diameter, ftString),


      db_Field(FLD_height_with_ground    , ftString),

      db_Field(FLD_VERT_WIDTH    , ftString),
      db_Field(FLD_HORZ_WIDTH    , ftString),

      db_Field(FLD_AZIMUTH    , ftString),
      db_Field(FLD_LOSS    , ftString),
      db_Field(FLD_gain    , ftString),

      db_Field(FLD_tilt    , ftString)

    ]);

   s:= 'CREATE UNIQUE INDEX Antennas1_index ON Antennas1(link_id) WITH DISALLOW NULL';
   ADOConnection_MDB.Execute(s);


  mdb_CreateTableFromArr(ADOConnection_MDB, 'Antennas2',
    [
      db_Field(FLD_link_id,  ftInteger),

      db_Field(FLD_height,   ftString),
      db_Field(FLD_polarization, ftString, 4),
      db_Field(FLD_diameter, ftString),


      db_Field(FLD_height_with_ground  , ftString),

      db_Field(FLD_VERT_WIDTH, ftString),
      db_Field(FLD_HORZ_WIDTH, ftString),

      db_Field(FLD_AZIMUTH , ftString),
      db_Field(FLD_LOSS    , ftString),
      db_Field(FLD_gain    , ftString),

      db_Field(FLD_tilt    , ftString)


    ]);

   s:= 'CREATE UNIQUE INDEX Antennas1_index ON Antennas2(link_id) WITH DISALLOW NULL';
   ADOConnection_MDB.Execute(s);

   }


 //  db_TableOpen1(t_Antennas1);
//   db_TableOpen1(t_Antennas2);

{

 db_AddRecord_(aDataSet,
              [
                FLD_LINK_ID, iLink_ID,

                FLD_HEIGHT,       aAntennaList.GetValue(FLD_HEIGHT),
                FLD_POLARIZATION, aAntennaList.GetValue(FLD_POLARIZATION),
                FLD_Diameter,     aAntennaList.GetValue(FLD_Diameter),

                FLD_height_with_ground,  aAntennaList.GetValue(FLD_height_with_ground),

                FLD_VERT_WIDTH,  aAntennaList.GetValue(FLD_VERT_WIDTH),
                FLD_HORZ_WIDTH,  aAntennaList.GetValue(FLD_HORZ_WIDTH),

                FLD_AZIMUTH,  aAntennaList.GetValue(FLD_AZIMUTH),
                FLD_LOSS,     aAntennaList.GetValue(FLD_LOSS),
                FLD_GAIN,     aAntennaList.GetValue(FLD_GAIN),

//                FLD_TILT,     aAntennaList.GetValue(FLD_TILT)
                FLD_TILT,     aTilt

              ]);


  "CREATE UNIQUE INDEX CustID " _
        & "ON Customers (CustomerID) " _
        & "WITH DISALLOW NULL;"
}

{
  mdb_CreateTableFromArr(ADOConnection_MDB, 'Profile_items',
    [
      db_Field(FLD_link_id, ftInteger),

      db_Field(FLD_INDEX, ftInteger),

      db_Field(FLD_distance_km, ftFloat),

      db_Field(FLD_ground_h, ftFloat),
      db_Field(FLD_clutter_h, ftInteger),
      db_Field(FLD_clutter_code, ftInteger),

      db_Field(FLD_clutter_name, ftString, 200),
      db_Field(FLD_clutter_color, ftInteger),

      db_Field(FLD_earth_h, ftFloat)

    ]);

   s:= 'CREATE UNIQUE INDEX Profile_items_index ON Profile_items(link_id,distance_km) WITH DISALLOW NULL';
   ADOConnection_MDB.Execute(s);
}


 // s:= Format('"CREATE UNIQUE INDEX CustID ON Profile_items(FLD_link_id, ) ', []);

{

 mdb_CreateTableFromArr(ADOConnection_MDB, 'link_img',
    [
      db_Field(FLD_link_id, ftInteger),


      db_Field(FLD_distance_km, ftFloat),

      db_Field(FLD_img1_filename, ftString, 200),
      db_Field(FLD_img2_filename, ftString, 200),
      db_Field(FLD_img3_filename, ftString, 200),
      db_Field(FLD_map_filename, ftString, 200),
      db_Field(FLD_profile_img_filename, ftString, 200)

    ]);

   s:= 'CREATE UNIQUE INDEX link_img_index ON link_img(link_id) WITH DISALLOW NULL';
   ADOConnection_MDB.Execute(s);}

{
      "CREATE UNIQUE INDEX CustID " _
        & "ON Customers (CustomerID) " _
        & "WITH DISALLOW NULL;"

}


{
  mdb_CreateTableFromArr(ADOConnection_MDB, 'Antennas1',
    [
      db_Field(FLD_link_id,  ftInteger),

      db_Field(FLD_polarization, ftString, 4),
      db_Field(FLD_height,   ftString),
      db_Field(FLD_diameter, ftString),
      db_Field(FLD_tilt    , ftString),
      db_Field(FLD_gain    , ftString)

    ]);

   s:= 'CREATE UNIQUE INDEX Antennas1_index ON Antennas1(link_id) WITH DISALLOW NULL';
   ADOConnection_MDB.Execute(s);


  mdb_CreateTableFromArr(ADOConnection_MDB, 'Antennas2',
    [
      db_Field(FLD_link_id,  ftInteger),

      db_Field(FLD_polarization,  ftString, 4),
      db_Field(FLD_height,   ftString),
      db_Field(FLD_diameter, ftString),
      db_Field(FLD_tilt    , ftString),
      db_Field(FLD_gain    , ftString)

    ]);

   s:= 'CREATE UNIQUE INDEX Antennas1_index ON Antennas2(link_id) WITH DISALLOW NULL';
   ADOConnection_MDB.Execute(s);

}

 // Open_data;

end;

// ---------------------------------------------------------------
procedure TdmMDB111111111.CreateTableFromDataset(aTblName: string; aDataSet_src:
    TDataSet; aTable: TADOTable);
// ---------------------------------------------------------------
var
  s: string;
begin

  s:=mdb_MakeCreateSql_From_Dataset(aDataSet_src, aTblName);


  try
    ADOConnection_MDB.Execute(s);
  except

   // ShowMessage(s);
  end;          

  aTable.TableName:=aTblName;
  aTable.Open;

//  Result := ;
end;


//------------------------------------------------------------------------------
function TdmMDB111111111.OpenMDB(aMDBFileName: string): Boolean;
//------------------------------------------------------------------------------
var
  I: Integer;

  s: string;
begin
//  TableNameList.Clear;

  FtempFileName := ChangeFileExt(aMDBFileName, '.mdb');

  Result := mdb_OpenConnection(ADOConnection_MDB, FtempFileName);

{
  if not Result then
    Exit;

    ADOTable1.Close;
  end;
}

//  FreeAndNil(oTableFieldNameList);


end;



procedure test;
begin
 // TdmMDB.Init;
//  dmMDB.CreateMDB('g:\d.mdb');

end;    //



begin
//  test;


end.




{


// ---------------------------------------------------------------
procedure TdmMDB.Open_data;
// ---------------------------------------------------------------
begin

{  t_Link.Open;


  t_Property1.Open;
  t_Property2.Open;

  t_LinkEnd1.Open;
  t_LinkEnd2.Open;
}
//  t_link_img.Open;


{
  t_Antennas1.Open;
  t_Antennas2.Open;

}
//  t_link_img.Open;



{  db_CopyRecords (qry_Property1, dmMDB.t_Property1 );
  db_CopyRecords (qry_Property2, dmMDB.t_Property2 );

  db_CopyRecords (qry_LinkEnd1, dmMDB.t_LinkEnd1 );
  db_CopyRecords (qry_LinkEnd2, dmMDB.t_LinkEnd2 );

  db_CopyRecords (qry_Antennas1, dmMDB.t_Antennas1 );
  db_CopyRecords (qry_Antennas2, dmMDB.t_Antennas2 );
}

  // TODO -cMM: TdmMDB.Open_data default body inserted
end;



}


ALTER TABLE Orders
ADD FOREIGN KEY (PersonID) REFERENCES Persons(PersonID);
