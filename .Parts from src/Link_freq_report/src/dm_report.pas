unit dm_report;

interface

uses
  SysUtils, Classes, frxExportXLS, frxClass, frxExportRTF, frxDBSet, DB, ADODB;

type
  TdmLink_report = class(TDataModule)
    ADOConnection1: TADOConnection;
    qry_property1: TADOQuery;
    qry_LInks: TADOQuery;
    ds_property1: TDataSource;
    ds_LInks: TDataSource;
    frxReport1: TfrxReport;
    frxDBDataset_link: TfrxDBDataset;
    frxRTFExport1: TfrxRTFExport;
    frxXLSExport1: TfrxXLSExport;
    qry_property2: TADOQuery;
    ds_property2: TDataSource;
    frxDBDataset_property1: TfrxDBDataset;
    frxDBDataset_property2: TfrxDBDataset;
    q_Linkend1: TADOQuery;
    ds_Linkend1: TDataSource;
    q_Linkend2: TADOQuery;
    ds_Linkend2: TDataSource;
    frxDBDataset_q_Linkend1: TfrxDBDataset;
    frxDBDataset_q_Linkend2: TfrxDBDataset;
    ADOQuery1: TADOQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmLink_report: TdmLink_report;

implementation

{$R *.dfm}

end.
