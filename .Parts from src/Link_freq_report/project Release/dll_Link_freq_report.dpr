program dll_Link_freq_report;



uses
  d_Setup in '..\src\d_Setup.pas' {dlg_Setup},
  dm_App_link_freq_report in '..\src\dm_App_link_freq_report.pas' {dmMain_app__freq_report: TDataModule},
  dm_Link_freq_plan in '..\src\dm_Link_freq_plan.pas' {dmLink_freq_plan: TDataModule},
  dm_Link_freq_plan_report in '..\src\dm_Link_freq_plan_report.pas' {dmLink_freq_plan_report: TDataModule},
  Forms,
  u_ini_Link_freq_report_params in '..\src_shared\u_ini_Link_freq_report_params.pas',
  u_Link_freq_report_classes in '..\src\u_Link_freq_report_classes.pas',
  u_dll_geo_convert in 'W:\DLL\Geo_Convert\shared\u_dll_geo_convert.pas',
  u_geo in 'W:\common7\Package GEO\src\u_geo.pas',
  u_db_mdb in 'W:\common7\Package_Common\u_db_mdb.pas',
  u_cx in 'W:\common7\Package_Common\u_cx.pas',
  dm_Main_Data in 'W:\RPLS_DB XE\src\dm_Main_Data.pas' {dmMain_Data: TDataModule},
  u_const_db in 'W:\RPLS_DB XE\src\u_const_db.pas',
  dm_Report in 'W:\RPLS_DB XE\src\Report\dm_Report.pas' {dmReport: TDataModule},
  u_vars in 'W:\RPLS_DB XE\src\u_vars.pas';

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(TdmMain_app__freq_report, dmMain_app__freq_report);
  Application.CreateForm(TdmMain_Data, dmMain_Data);
  Application.Run;
end.
