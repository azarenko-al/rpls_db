unit I_Link_freq_report;

interface

uses ADOInt,
  u_dll;

type

  ILink_freq_report_X111111111111 = interface(IInterface)
  ['{CD6E2566-0A21-4BC5-96B9-18778A1E05FD}']

    procedure InitADO(aConnection: _Connection); stdcall;
  //  procedure InitAppHandle(aHandle: integer); stdcall;

    procedure Execute (aFileName: WideString);  stdcall;
  end;

var
  ILink_freq_report: ILink_freq_report_X111111111111;

  function Load_ILink_freq_report: Boolean;
  procedure UnLoad_ILink_freq_report;

implementation

const
//  DEF_BPL_FILENAME = 'bpl_LOS.bpl';
  DEF_FILENAME = 'dll_Link_freq_report.dll';

var
  LHandle : Integer;


function Load_ILink_freq_report: Boolean;
begin
  Result := GetInterface_DLL(DEF_FILENAME, LHandle,  ILink_freq_report_X111111111111, ILink_freq_report)=0;
end;


procedure UnLoad_ILink_freq_report;
begin
  ILink_freq_report := nil;
  UnloadPackage_dll(LHandle);
end;


end.



