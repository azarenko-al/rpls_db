unit u_ini_Link_freq_report_params;

interface
uses
  SysUtils, IniFiles, 

  
  u_classes
  ;

type
  // ---------------------------------------------------------------
  TIni_Link_freq_Report_Param = class
  // ---------------------------------------------------------------
  private
  public
    ConnectionString: string;
    IsTest:  Boolean;

    IDList: TIDList;

    constructor Create;
    destructor Destroy; override;

    procedure LoadFromINI(aFileName: string);
    procedure SaveToINI(aFileName: string);

// TODO: Validate
//  function Validate: boolean;

  end;


var
  g_Ini_Link_freq_Report_Param_: TIni_Link_freq_Report_Param;


implementation


constructor TIni_Link_freq_Report_Param.Create;
begin
  inherited Create;

  IDList := TIDList.Create();
end;


destructor TIni_Link_freq_Report_Param.Destroy;
begin
  FreeAndNil(IDList);


  inherited Destroy;
end;


//--------------------------------------------------------------
procedure TIni_Link_freq_Report_Param.LoadFromINI(aFileName: string);
//--------------------------------------------------------------
var
  oIni: TIniFile;
begin
  IDList.LoadIDsFromIniFile(aFileName, 'main');

  oIni:=TIniFile.Create (aFileName);
  IsTest:=oIni.ReadBool ('main', 'IsTest', False);

  ConnectionString:=oIni.ReadString ('main', 'ConnectionString', '');


//  ShowMessage(ConnectionString);


  oIni.Free;

end;

// ---------------------------------------------------------------
procedure TIni_Link_freq_Report_Param.SaveToINI(aFileName: string);
// ---------------------------------------------------------------
var
  oIni: TIniFile;
begin
  ForceDirectories( ExtractFileDir(aFileName) );

  IDList.SaveIDsToIniFile(aFileName, 'main');


  oIni:=TIniFile.Create (aFileName);
  oIni.WriteBool ('main', 'IsTest', IsTest);
  oIni.WriteString ('main', 'ConnectionString', ConnectionString);

  oIni.Free;


end;



initialization
//  g_Ini_Link_freq_Report_Param:=TIni_Link_freq_Report_Param.Create;

finalization
//  FreeAndNil(g_Ini_Link_freq_Report_Param);



end.



