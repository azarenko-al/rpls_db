unit d_Filter;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxCheckBox,
  Data.Win.ADODB, Vcl.StdCtrls, Vcl.ExtCtrls, dxBar, cxClasses, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridCustomView,
  cxGrid;

type
  Tdlg_Filter = class(TForm)
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1id: TcxGridDBColumn;
    cxGridDBTableView1objname: TcxGridDBColumn;
    cxGridDBTableView1name: TcxGridDBColumn;
    cxGridDBTableView1user_created: TcxGridDBColumn;
    cxGridDBTableView1is_shared: TcxGridDBColumn;
    cxGridDBTableView1is_can_update: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarButton1: TdxBarButton;
    ADOStoredProc_SEL: TADOStoredProc;
    ds_ADOStoredProc_SEL: TDataSource;
    ADOConnection1: TADOConnection;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_ReadOnly: TcxStyle;
    Bevel1: TBevel;
    b_Save: TButton;
    b_Close: TButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class procedure ExecDlg;
    
  end;
//
//var
//  dlg_Filter: Tdlg_Filter;

implementation

{$R *.dfm}

class procedure Tdlg_Filter.ExecDlg;
begin
  with Tdlg_Filter.Create (Application) do
  begin   
    ShowModal;     

    Free;
  end;

end;


procedure Tdlg_Filter.FormCreate(Sender: TObject);
begin
  cxGrid2.Align:=alClient;
end;


end.
