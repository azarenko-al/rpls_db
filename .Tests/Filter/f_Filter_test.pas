unit f_Filter_test;

interface

uses
  u_db,
  u_files,


  IOUtils,

  

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxFilterControl,
  cxDBFilterControl, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, Data.Win.ADODB, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, dxBar, cxCheckBox;

type
  TForm35 = class(TForm)
    ADOConnection1: TADOConnection;
    ds_tables_for_export: TDataSource;
    ADOTable1: TADOTable;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxGrid1DBTableView1folder_id: TcxGridDBColumn;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    cxGrid1DBTableView1gain: TcxGridDBColumn;
    cxGrid1DBTableView1gain_type: TcxGridDBColumn;
    cxGrid1DBTableView1tilt_type: TcxGridDBColumn;
    cxGrid1DBTableView1freq: TcxGridDBColumn;
    cxGrid1DBTableView1diameter: TcxGridDBColumn;
    cxGrid1DBTableView1omni: TcxGridDBColumn;
    cxGrid1DBTableView1vert_width: TcxGridDBColumn;
    cxGrid1DBTableView1horz_width: TcxGridDBColumn;
    cxGrid1DBTableView1fronttobackratio: TcxGridDBColumn;
    cxGrid1DBTableView1polarization_ratio: TcxGridDBColumn;
    cxGrid1DBTableView1polarization_str: TcxGridDBColumn;
    cxGrid1DBTableView1weight: TcxGridDBColumn;
    cxGrid1DBTableView1band: TcxGridDBColumn;
    cxGrid1DBTableView1vert_mask: TcxGridDBColumn;
    cxGrid1DBTableView1horz_mask: TcxGridDBColumn;
    cxGrid1DBTableView1electrical_tilt: TcxGridDBColumn;
    cxGrid1DBTableView1comment: TcxGridDBColumn;
    cxGrid1DBTableView1file_content: TcxGridDBColumn;
    cxGrid1DBTableView1guid: TcxGridDBColumn;
    cxGrid1DBTableView1date_created: TcxGridDBColumn;
    cxGrid1DBTableView1date_modify: TcxGridDBColumn;
    cxGrid1DBTableView1user_created: TcxGridDBColumn;
    cxGrid1DBTableView1user_modify: TcxGridDBColumn;
    cxGrid1DBTableView1vendor_id: TcxGridDBColumn;
    cxGrid1DBTableView1band_id: TcxGridDBColumn;
    Button1: TButton;
    ADOStoredProc_SEL: TADOStoredProc;
    ds_ADOStoredProc_SEL: TDataSource;
    DBGrid1: TDBGrid;
    b_Save: TButton;
    Button3: TButton;
    ADOStoredProc1: TADOStoredProc;
    dxBarManager1: TdxBarManager;
    b_Load: TButton;
    dxBarManager1Bar1: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarPopupMenu1: TdxBarPopupMenu;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1id: TcxGridDBColumn;
    cxGridDBTableView1objname: TcxGridDBColumn;
    cxGridDBTableView1name: TcxGridDBColumn;
    cxGridDBTableView1user_created: TcxGridDBColumn;
    cxGridDBTableView1is_shared: TcxGridDBColumn;
    cxGridDBTableView1is_can_update: TcxGridDBColumn;
    procedure Button1Click(Sender: TObject);
    procedure b_SaveClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure b_LoadClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form35: TForm35;

implementation

{$R *.dfm}

procedure TForm35.Button1Click(Sender: TObject);
begin
  cxGrid1DBTableView1.StoreToIniFile('d:\aaa.ini',true,[gsoUseFilter],'1111');
  
end;



procedure TForm35.b_SaveClick(Sender: TObject);
var
  k: Integer;
  s: string;
  sFile: string;
  sName: string;
begin
  k:=cxGrid1DBTableView1.DataController.Filter.Root.Count;

  if k=0 then 
    Exit;
  
  
  sName:='������';

  if InputQuery('��������� �������','��������', sName) then 
  begin
  
    sFile:=GetTempFileDir() + 'filter.ini';
    sFile:='d:\aaa.ini';

    cxGrid1DBTableView1.StoreToIniFile(sFile,true,[gsoUseFilter],'1111');

    s:=TFile.ReadAllText(sFile);
    

    k:=db_StoredProc_Exec(ADOStoredProc1, 'ui.sp_Filter_INS_UPD',
        [
        'objname', 'AntennaType',
        'name', sName,
        'BODY', s
        ]);

    ADOStoredProc_SEL.Requery([]);
  end;
 
end;



procedure TForm35.Button3Click(Sender: TObject);
begin
  db_StoredProc_Open(ADOStoredProc_SEL, 'ui.sp_Filter_SEL', ['objname', 'AntennaType']);


end;


procedure TForm35.b_LoadClick(Sender: TObject);
var
  s: string;
  sFile: string;
begin
  s:=ADOStoredProc_SEL.FieldByName('body').AsString;

  sFile:=GetTempFileDir() + 'filter.ini';
  sFile:='d:\aaa.ini';


   TFile.WriteAllText(sFile, s);
                    
  cxGrid1DBTableView1.RestoreFromIniFile(sFile, true, false, [gsoUseFilter], '1111');
  
  
//

//procedure TcxCustomGridView.RestoreFromIniFile(const AStorageName: string;
//  AChildrenCreating: Boolean = True; AChildrenDeleting: Boolean = False;
//  AOptions: TcxGridStorageOptions = [gsoUseFilter, gsoUseSummary];
//  const ARestoreViewName: string = ''; const AOwnerName: string = '');
//begin
//  RestoreFrom(AStorageName, nil, TcxIniFileReader, AChildrenCreating,
//    AChildrenDeleting, AOptions, ARestoreViewName, AOwnerName);
//end;
//
//

//  
//  if InputQuery('��������� �������','��������', sName) then 
//  begin
//  
//
//    cxGrid1DBTableView1.StoreToIniFile(sFile,true,[gsoUseFilter],'1111');
//
//    
//
//    k:=db_StoredProc_Exec(ADOStoredProc1, 'ui.sp_Filter_INS_UPD',
//        [
//        'objname', 'AntennaType',
//        'name', sName,
//        'BODY', s
//        ]);
//
//    ADOStoredProc_SEL.Requery([]);
//  end;
// 

end;


procedure TForm35.FormCreate(Sender: TObject);
begin
//  cxGrid1DBTableView1.FilterBox.

end;

end.
