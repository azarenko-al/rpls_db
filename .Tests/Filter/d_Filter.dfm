object dlg_Filter: Tdlg_Filter
  Left = 1147
  Top = 291
  Caption = 'dlg_Filter'
  ClientHeight = 401
  ClientWidth = 806
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    806
    401)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 0
    Top = 357
    Width = 806
    Height = 44
    Align = alBottom
    Shape = bsTopLine
    ExplicitTop = 333
    ExplicitWidth = 827
  end
  object cxGrid2: TcxGrid
    Left = 0
    Top = 28
    Width = 457
    Height = 329
    Align = alLeft
    TabOrder = 0
    ExplicitHeight = 232
    object cxGridDBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ds_ADOStoredProc_SEL
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsView.GroupByBox = False
      object cxGridDBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Width = 34
      end
      object cxGridDBTableView1objname: TcxGridDBColumn
        DataBinding.FieldName = 'objname'
        Visible = False
        Width = 88
      end
      object cxGridDBTableView1name: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'name'
        Width = 161
      end
      object cxGridDBTableView1user_created: TcxGridDBColumn
        Caption = #1048#1079#1084#1077#1085#1080#1083
        DataBinding.FieldName = 'user_created'
        Width = 82
      end
      object cxGridDBTableView1is_shared: TcxGridDBColumn
        DataBinding.FieldName = 'is_shared'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Width = 71
      end
      object cxGridDBTableView1is_can_update: TcxGridDBColumn
        DataBinding.FieldName = 'is_readonly'
        Width = 77
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object b_Save: TButton
    Left = 635
    Top = 368
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    Default = True
    ModalResult = 1
    TabOrder = 5
    ExplicitLeft = 656
    ExplicitTop = 344
  end
  object b_Close: TButton
    Left = 723
    Top = 368
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = #1047#1072#1082#1088#1099#1090#1100
    ModalResult = 8
    TabOrder = 6
    ExplicitLeft = 744
    ExplicitTop = 344
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 529
    Top = 64
    DockControlHeights = (
      0
      0
      28
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 728
      FloatTop = 431
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
    end
  end
  object ADOStoredProc_SEL: TADOStoredProc
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    ProcedureName = 'ui.sp_Filter_SEL;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@OBJNAME'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end>
    Left = 680
    Top = 128
  end
  object ds_ADOStoredProc_SEL: TDataSource
    DataSet = ADOStoredProc_SEL
    Left = 681
    Top = 184
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=SERVER1;Use Procedu' +
      're for Prepare=1;Auto Translate=True;Packet Size=4096;Workstatio' +
      'n ID=ALEX;Use Encryption for Data=False;Tag with column collatio' +
      'n when possible=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 536
    Top = 136
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 688
    Top = 56
    PixelsPerInch = 96
    object cxStyle_ReadOnly: TcxStyle
      AssignedValues = [svColor]
      Color = clSilver
    end
  end
end
