program test_Filter;

uses
  Vcl.Forms,
  f_Filter_test in 'f_Filter_test.pas' {Form35},
  d_Filter in '..\..\.Parts\Filter\src\d_Filter.pas' {dlg_Filter};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm35, Form35);
  Application.CreateForm(Tdlg_Filter, dlg_Filter);
  Application.Run;
end.
