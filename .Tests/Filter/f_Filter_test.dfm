object Form35: TForm35
  Left = 196
  Top = 278
  Caption = 'Form35'
  ClientHeight = 546
  ClientWidth = 1482
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 24
    Top = 192
    Width = 521
    Height = 257
    PopupMenu = dxBarPopupMenu1
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ds_tables_for_export
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object cxGrid1DBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
      end
      object cxGrid1DBTableView1folder_id: TcxGridDBColumn
        DataBinding.FieldName = 'folder_id'
      end
      object cxGrid1DBTableView1name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
      end
      object cxGrid1DBTableView1gain: TcxGridDBColumn
        DataBinding.FieldName = 'gain'
      end
      object cxGrid1DBTableView1gain_type: TcxGridDBColumn
        DataBinding.FieldName = 'gain_type'
      end
      object cxGrid1DBTableView1tilt_type: TcxGridDBColumn
        DataBinding.FieldName = 'tilt_type'
      end
      object cxGrid1DBTableView1freq: TcxGridDBColumn
        DataBinding.FieldName = 'freq'
      end
      object cxGrid1DBTableView1diameter: TcxGridDBColumn
        DataBinding.FieldName = 'diameter'
      end
      object cxGrid1DBTableView1omni: TcxGridDBColumn
        DataBinding.FieldName = 'omni'
      end
      object cxGrid1DBTableView1vert_width: TcxGridDBColumn
        DataBinding.FieldName = 'vert_width'
      end
      object cxGrid1DBTableView1horz_width: TcxGridDBColumn
        DataBinding.FieldName = 'horz_width'
      end
      object cxGrid1DBTableView1fronttobackratio: TcxGridDBColumn
        DataBinding.FieldName = 'fronttobackratio'
      end
      object cxGrid1DBTableView1polarization_ratio: TcxGridDBColumn
        DataBinding.FieldName = 'polarization_ratio'
      end
      object cxGrid1DBTableView1polarization_str: TcxGridDBColumn
        DataBinding.FieldName = 'polarization_str'
      end
      object cxGrid1DBTableView1weight: TcxGridDBColumn
        DataBinding.FieldName = 'weight'
      end
      object cxGrid1DBTableView1band: TcxGridDBColumn
        DataBinding.FieldName = 'band'
      end
      object cxGrid1DBTableView1vert_mask: TcxGridDBColumn
        DataBinding.FieldName = 'vert_mask'
      end
      object cxGrid1DBTableView1horz_mask: TcxGridDBColumn
        DataBinding.FieldName = 'horz_mask'
      end
      object cxGrid1DBTableView1electrical_tilt: TcxGridDBColumn
        DataBinding.FieldName = 'electrical_tilt'
      end
      object cxGrid1DBTableView1comment: TcxGridDBColumn
        DataBinding.FieldName = 'comment'
      end
      object cxGrid1DBTableView1file_content: TcxGridDBColumn
        DataBinding.FieldName = 'file_content'
      end
      object cxGrid1DBTableView1guid: TcxGridDBColumn
        DataBinding.FieldName = 'guid'
      end
      object cxGrid1DBTableView1date_created: TcxGridDBColumn
        DataBinding.FieldName = 'date_created'
      end
      object cxGrid1DBTableView1date_modify: TcxGridDBColumn
        DataBinding.FieldName = 'date_modify'
      end
      object cxGrid1DBTableView1user_created: TcxGridDBColumn
        DataBinding.FieldName = 'user_created'
      end
      object cxGrid1DBTableView1user_modify: TcxGridDBColumn
        DataBinding.FieldName = 'user_modify'
      end
      object cxGrid1DBTableView1vendor_id: TcxGridDBColumn
        DataBinding.FieldName = 'vendor_id'
      end
      object cxGrid1DBTableView1band_id: TcxGridDBColumn
        DataBinding.FieldName = 'band_id'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object Button1: TButton
    Left = 32
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 1
    OnClick = Button1Click
  end
  object DBGrid1: TDBGrid
    Left = 576
    Top = 192
    Width = 345
    Height = 257
    DataSource = ds_ADOStoredProc_SEL
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object b_Save: TButton
    Left = 32
    Top = 112
    Width = 75
    Height = 25
    Caption = 'save'
    TabOrder = 3
    OnClick = b_SaveClick
  end
  object Button3: TButton
    Left = 120
    Top = 64
    Width = 75
    Height = 25
    Caption = 'refresh'
    TabOrder = 4
    OnClick = Button3Click
  end
  object b_Load: TButton
    Left = 120
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Load'
    TabOrder = 8
    OnClick = b_LoadClick
  end
  object cxGrid2: TcxGrid
    Left = 948
    Top = 192
    Width = 493
    Height = 257
    PopupMenu = dxBarPopupMenu1
    TabOrder = 10
    object cxGridDBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ds_ADOStoredProc_SEL
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.GroupByBox = False
      object cxGridDBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Width = 34
      end
      object cxGridDBTableView1objname: TcxGridDBColumn
        DataBinding.FieldName = 'objname'
        Width = 88
      end
      object cxGridDBTableView1name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Width = 111
      end
      object cxGridDBTableView1user_created: TcxGridDBColumn
        DataBinding.FieldName = 'user_created'
        Width = 82
      end
      object cxGridDBTableView1is_shared: TcxGridDBColumn
        DataBinding.FieldName = 'is_shared'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Width = 71
      end
      object cxGridDBTableView1is_can_update: TcxGridDBColumn
        DataBinding.FieldName = 'is_can_update'
        Width = 56
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=SERVER1;Use Procedu' +
      're for Prepare=1;Auto Translate=True;Packet Size=4096;Workstatio' +
      'n ID=ALEX;Use Encryption for Data=False;Tag with column collatio' +
      'n when possible=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 448
    Top = 64
  end
  object ds_tables_for_export: TDataSource
    DataSet = ADOTable1
    Left = 577
    Top = 120
  end
  object ADOTable1: TADOTable
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 'antennaType'
    Left = 577
    Top = 64
  end
  object ADOStoredProc_SEL: TADOStoredProc
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    ProcedureName = 'ui.sp_Filter_SEL;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@OBJNAME'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end>
    Left = 712
    Top = 64
  end
  object ds_ADOStoredProc_SEL: TDataSource
    DataSet = ADOStoredProc_SEL
    Left = 713
    Top = 120
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    ProcedureName = '__sp_Folder_Select_Tree___;1'
    Parameters = <>
    Left = 888
    Top = 72
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 1048
    Top = 64
    DockControlHeights = (
      0
      0
      28
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 728
      FloatTop = 431
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
    end
  end
  object dxBarPopupMenu1: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
        ItemName = 'dxBarButton1'
      end>
    UseOwnFont = False
    Left = 312
    Top = 80
  end
end
