unit Unit1;

interface

uses
  u_GEO,



  u_Mapx,
  u_Mapx_lib,

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs;

type
  TForm25 = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form25: TForm25;

implementation

{$R *.dfm}

procedure TForm25.FormCreate(Sender: TObject);

var
  oMapFile: TmiMap;

  i,j: Integer;
  i_lat,j_lon: integer;
  buf, d1, d2: double;
  eRain: Double;
  FTxtFile_Rain: TextFile;
  iLat: Integer;
  iLon: Integer;

  blRect: TBLRect;
  
  arr: TBLPointArray;

  oList: TStringList;
  s: string;

begin
  oList:=TStringList.Create;


  AssignFile(FTxtFile_Rain, 'C:\ONEGA\RPLS_DB\DATA\�����������������\ESARAIN_R001.PRN');
  Reset(FTxtFile_Rain);
  

  oMapFile:=TmiMap.Create;
  oMapFile.CreateFile1 ('C:\ONEGA\RPLS_DB\DATA\�����������������\ESARAIN_R001',
                    [                       
                     mapx_Field('Rain', miTypeFloat),                       
                     mapx_Field('row', miTypeInt),                       
                     mapx_Field('col', miTypeInt)                       
                    ]);

//
//i_lat:=round((90-aPoint.B)/1.5);
//j_lon:=round(aPoint.L/1.5);
//


  for iLat:=0 to 120 do
  begin
    s:='';

    for iLon:=0 to 240 do 
    begin
      Read(FTxtFile_Rain, eRain);


      blRect.TopLeft.B    := 90 - (iLat) * 1.5;
      blRect.TopLeft.L    := iLon * 1.5;

      blRect.BottomRight.B:= 90 - (iLat+1) * 1.5;
      blRect.BottomRight.L:= (iLon+1) * 1.5;
      

      arr:=geo_BLRectToBLPoints_polygone(blRect);


       oMapFile.WriteRegion (arr, 
            [
            'Rain', eRain,
            'row',  iLat,  
            'col',  iLon
            ]);

      
      
      s:=s+' '+ Format('%8.4f', [eRain]);
                
    end;      

    oList.Add(s);
  
  end;


  oList.SaveToFile('C:\ONEGA\RPLS_DB\DATA\�����������������\ESARAIN_R001.txt');


                    
//  if (i_lat in [0..120]) and (j_lon in [0..240]) then

//  begin
//    if i_lat>0 then for i:=0 to i_lat-1 do Readln(FTxtFile_Rain, d1);
//    if j_lon>0 then for j:=0 to j_lon-1 do Read(FTxtFile_Rain, d2);



    //Read(var F: File; V1; [ ..., VN]);

                    
                    
  FreeAndNil(oMapFile);


  CloseFile(FTxtFile_Rain);
                    
end;

end.


  (*

// -------------------------------------------------------------------
function TLink_climate_file.DoGetRainIntenseByPoint(aPoint: TBLPoint): Double;
// -------------------------------------------------------------------
var
    i,j: Integer;
    i_lat,j_lon: integer;
    buf, d1, d2: double;
     FTxtFile_Rain: TextFile;
begin
  AssignFile(FTxtFile_Rain, GetFileDir() + DEF_FileName_Rain);
  Reset(FTxtFile_Rain);

  i_lat:=round((90-aPoint.B)/1.5);
  j_lon:=round(aPoint.L/1.5);

  {
  AssignFile(FTxtFile_Rain, GetFileDir() + DEF_FileName_Rain);
  Reset(FTxtFile_Rain);
  }

  if (i_lat in [0..120]) and (j_lon in [0..240]) then
  begin
    if i_lat>0 then for i:=0 to i_lat-1 do Readln(FTxtFile_Rain, d1);
    if j_lon>0 then for j:=0 to j_lon-1 do Read(FTxtFile_Rain, d2);

    buf:= 0;
    Read(FTxtFile_Rain, buf);
    Result:=buf;
  end
    else Result:=0;

  CloseFile(FTxtFile_Rain);

  //      CloseFile(FTxtFile_Rain);
end;
