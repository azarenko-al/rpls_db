unit Unit20;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, IniFiles,  Vcl.ExtCtrls, Vcl.StdCtrls;

type
  TForm20 = class(TForm)
    Timer1: TTimer;
    Edit1: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private

    Fidle_time_minutes: Integer;
    Fidle_msg: string;

    FLastHookTime: TDateTime;
    procedure Init_Idle;

 
  public
    { Public declarations }
  end;

var
  Form20: TForm20;

implementation

{$R *.dfm}

procedure TForm20.FormCreate(Sender: TObject);
begin
  Init_Idle;
end;

procedure TForm20.Timer1Timer(Sender: TObject);

    function DoLastInput: DWord;
    var
      LInput: TLastInputInfo;
    begin
      LInput.cbSize := SizeOf(TLastInputInfo);
      GetLastInputInfo(LInput);
      Result := GetTickCount - LInput.dwTime;
    end;

var
  dt: TDateTime;

begin

  dt := DoLastInput() / 1000 / SecsPerDay;


  Edit1.Text := FormatDateTime('hh:nn:ss', dt);


//
end;


// ---------------------------------------------------------------
procedure TForm20.Init_Idle;
// ---------------------------------------------------------------
var
  oIni: TIniFile;
  s: string;
  sFile: string;

begin
  FLastHookTime:=Now();

 // s:=ChangeFileExt( Application.ExeName, '.ini');

  sFile:=ExtractFilePath(Application.ExeName) + 'rpls_db_link.ini';
  Assert (FileExists(sFile)); 

  //-----------------------------------------------

  oIni := TIniFile.Create (sFile);

  Fidle_time_minutes:= oIni.ReadInteger ('IDLE', 'time_minutes', 1);
  Fidle_msg         := oIni.ReadString  ('IDLE', 'msg', '������ ��������� ��������� ��-�� ������������ %s');

  FreeAndNil(oIni);


//   ShowMessage( IntToStr(Fidle_time_minutes) + ' minute');


end;



end.
