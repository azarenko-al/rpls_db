unit f_Options;

interface

uses
  u_db,


  CodeSiteLogging,
  
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, Data.Win.ADODB,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, cxDBLookupComboBox, cxColorComboBox,
  cxImageComboBox, cxSpinEdit, cxButtonEdit, cxVGrid, cxDBVGrid,
  cxInplaceContainer, Vcl.ImgList, Vcl.StdCtrls, cxDBTL;

type
  Tfrm_Options1 = class(TForm)
    ADOConnection1: TADOConnection;
    ds_Objects: TDataSource;
    t_Objects: TADOTable;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    ds_Object_style: TDataSource;
    t_Object_style: TADOTable;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1id: TcxGridDBColumn;
    cxGridDBTableView1objname: TcxGridDBColumn;
    cxGridDBTableView1caption: TcxGridDBColumn;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxGrid1DBTableView1parent_id: TcxGridDBColumn;
    cxGrid1DBTableView1enabled: TcxGridDBColumn;
    col_style_name: TcxGridDBColumn;
    cxGrid1DBTableView1caption: TcxGridDBColumn;
    cxGrid1DBTableView1field_name: TcxGridDBColumn;
    ds_object_item_values: TDataSource;
    t_object_item_values: TADOTable;
    cxGrid3: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    cxLookAndFeelController1: TcxLookAndFeelController;
    cxGridDBTableView1Column1: TcxGridDBColumn;
    cxGridDBTableView2enabled: TcxGridDBColumn;
    cxGridDBTableView2field_value: TcxGridDBColumn;
    col_line_color: TcxGridDBColumn;
    col_line_style: TcxGridDBColumn;
    cxGridDBTableView2line_width: TcxGridDBColumn;
    col_region_border_style: TcxGridDBColumn;
    cxGridDBTableView2region_border_color: TcxGridDBColumn;
    cxGridDBTableView2region_border_width: TcxGridDBColumn;
    col_region_style: TcxGridDBColumn;
    cxGridDBTableView2region_color: TcxGridDBColumn;
    cxGridDBTableView2region_back_color: TcxGridDBColumn;
    cxGridDBTableView2Symbol_Character: TcxGridDBColumn;
    cxGridDBTableView2Symbol_font_name: TcxGridDBColumn;
    cxGridDBTableView2Symbol_font_size: TcxGridDBColumn;
    cxGridDBTableView2Symbol_font_color: TcxGridDBColumn;
    img_MIF_LineStyle: TImageList;
    img_MIF_RegionStyle: TImageList;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1line_color: TcxDBEditorRow;
    cxDBVerticalGrid1line_style: TcxDBEditorRow;
    cxDBVerticalGrid1line_width: TcxDBEditorRow;
    cxDBVerticalGrid1region_border_style: TcxDBEditorRow;
    cxDBVerticalGrid1region_border_color: TcxDBEditorRow;
    cxDBVerticalGrid1region_border_width: TcxDBEditorRow;
    cxDBVerticalGrid1region_style: TcxDBEditorRow;
    cxDBVerticalGrid1region_color: TcxDBEditorRow;
    cxDBVerticalGrid1region_back_color: TcxDBEditorRow;
    cxDBVerticalGrid1Symbol_Character: TcxDBEditorRow;
    cxDBVerticalGrid1Symbol_font_name: TcxDBEditorRow;
    cxDBVerticalGrid1Symbol_font_size: TcxDBEditorRow;
    cxDBVerticalGrid1Symbol_font_color: TcxDBEditorRow;
    row_Line: TcxCategoryRow;
    row_Symbol: TcxCategoryRow;
    row_Region: TcxCategoryRow;
    DataSource1: TDataSource;
    ADOTable1: TADOTable;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(Sender:
        TcxCustomGridTableView; APrevFocusedRecord, AFocusedRecord:
        TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
  private
    procedure InitTreeListColumn(aColumn: TcxGridDBColumn);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_Options1: Tfrm_Options1;

implementation

{$R *.dfm}

procedure Tfrm_Options1.FormCreate(Sender: TObject);
begin
  InitTreeListColumn (col_Line_Style);
  InitTreeListColumn (col_region_border_style);  
  InitTreeListColumn (col_region_style);  

end;


// ---------------------------------------------------------------
procedure Tfrm_Options1.InitTreeListColumn(aColumn: TcxGridDBColumn);
// ---------------------------------------------------------------
var
  k,I: Integer;

  obj: TcxImageComboBoxProperties;
  
begin  
  obj:=TcxImageComboBoxProperties(aColumn.Properties)  ;
  k:=obj.Items.Count;

  with TcxImageComboBoxProperties(aColumn.Properties) do 
    for I := 1 to Items.Count-1 do
      Items[i].Value:=i-1;
end;


procedure Tfrm_Options1.Button1Click(Sender: TObject);
var
  id: Integer;
  s: string;
  sName: string;
begin

with t_Objects do
    while not Eof do
    begin
      s:=FieldValues['feature_type'];
      id:=FieldValues['id'];

      if s = 'line' then
      begin      
        sName:='line_size';

        if not t_Object_style.Locate('parent_id;name', VarArrayOf([id, sName]), []) then 
          db_AddRecord_(t_Object_style,
             ['name',sName,
              'parent_id' , id ]);
      
        sName:='line_width';

        if not t_Object_style.Locate('parent_id;name', VarArrayOf([id, sName]), []) then 
          db_AddRecord_(t_Object_style,
             ['name',sName,
              'parent_id' , id ]);
                     
        sName:='line_color';

        if not t_Object_style.Locate('parent_id;name', VarArrayOf([id, sName]), []) then 
          db_AddRecord_(t_Object_style,
             ['name',sName,
              'parent_id' , id ]);
                                  
      end else


      if s = 'symbol' then
      begin      
        sName:='Symbol_font_size';

        if not t_Object_style.Locate('parent_id;name', VarArrayOf([id, sName]), []) then 
          db_AddRecord_(t_Object_style,
             ['name',sName,
              'parent_id' , id ]);
      
        sName:='Symbol_font_color';

        if not t_Object_style.Locate('parent_id;name', VarArrayOf([id, sName]), []) then 
          db_AddRecord_(t_Object_style,
             ['name',sName,
              'parent_id' , id ]);
                     
        sName:='Symbol_Character';

        if not t_Object_style.Locate('parent_id;name', VarArrayOf([id, sName]), []) then 
          db_AddRecord_(t_Object_style,
             ['name',sName,
              'parent_id' , id ]);

        sName:='Symbol_font_name';

        if not t_Object_style.Locate('parent_id;name', VarArrayOf([id, sName]), []) then 
          db_AddRecord_(t_Object_style,
             ['name',sName,
              'parent_id' , id ]);


          //cxDBVerticalGrid1Symbol_font_name
                                  
      end;

      
      
//      db_AddRecordFromDataSet(aSrcDataset, aDestDataSet);

      Next;
    end;

end;


procedure Tfrm_Options1.cxGrid1DBTableView1FocusedRecordChanged(Sender:
    TcxCustomGridTableView; APrevFocusedRecord, AFocusedRecord:
    TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
var
  v: Variant;
begin
   CodeSite.Send('RecordChanged');

   v:=AFocusedRecord.Values[col_style_name.Index];

//   if v = 'Symbol_Character' then
   
   
end;

end.

//
//
//  with aSrcDataSet do
//    while not Eof do
//    begin
//      Progress_SetProgress2(aSrcDataset.RecNo, aSrcDataSet.RecordCount, bTerminated);
//      if bTerminated then
//        Exit;
//
//      db_AddRecordFromDataSet(aSrcDataset, aDestDataSet);
//
//      Next;
//    end;
