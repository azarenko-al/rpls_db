unit dm_Main_SQL;

interface

uses
  SysUtils, Classes, DB, ADODB,

  u_db,
  u_db_manager,

  I_DB_login;

type
  TdmMain_SQL = class(TDataModule)
    ADOConnection1: TADOConnection;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure OpenDB(aRec: TdbLoginRec);

  public
    procedure Close;
    function Open(aConnectionString: string): boolean;
  end;

var
  dmMain_SQL: TdmMain_SQL;

  gl_DB: TDBManager;



implementation

{$R *.dfm}


procedure TdmMain_SQL.DataModuleCreate(Sender: TObject);
begin
  gl_DB:=TDBManager.Create;
  gl_DB.ADOConnection:=ADOConnection1;  
end;


procedure TdmMain_SQL.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(gl_DB);
end;


function TdmMain_SQL.Open(aConnectionString: string): boolean;
begin
  Result := db_OpenADOConnectionString(ADOConnection1, aConnectionString);
end;

procedure TdmMain_SQL.OpenDB(aRec: TdbLoginRec);
begin

end;


procedure TdmMain_SQL.Close;
begin
  ADOConnection1.Close;
end;

end.
