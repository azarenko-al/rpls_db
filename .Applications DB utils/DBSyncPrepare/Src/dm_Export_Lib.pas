unit dm_Export_Lib;

interface

uses
  SysUtils, Classes, Forms, ADODB, Dialogs, 

 // dm_ExportStructure_2005,

 // d_Progress,

  dm_Main_SQL,

  dm_MDB,

  u_db_mdb,
  
  u_db, 

  u_local_const, DB;


type
  TdmExport_Lib1111 = class(TDataModule)
    qry_Temp: TADOQuery;
    ADOTable_dest: TADOTable;
    ADOQuery1: TADOQuery;
    ADOTable_SQL: TADOTable;
    ADOConnection1: TADOConnection;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure Create_ObjectTables;
//    procedure CLearMDB;

    procedure Export_Lib_Tables;
    { Private declarations }
  public
     procedure Exec;
    class procedure Init;
    { Public declarations }
  end;

var
  dmExport_Lib1111: TdmExport_Lib1111;

implementation

{$R *.dfm}


procedure TdmExport_Lib1111.DataModuleCreate(Sender: TObject);
begin
  db_SetComponentADOConnection(Self, dmMain_SQL.AdoConnection1);


  if ADOConnection1.Connected then
    ShowMessage('TdmExport_Lib.ADOConnection1.Connected');
    

end;

//------------------------------------------------------------------------------
procedure TdmExport_Lib1111.Create_ObjectTables;
//------------------------------------------------------------------------------

  // ---------------------------------------------------------------
  procedure DoCreate_tables(aTableNameArr: array of String);
  // ---------------------------------------------------------------
  var
    I: Integer;
    j: Integer;
    sTable: string;
    sFields: string;
    sFld: String;
    sSQL: string;
  begin
    for j := 0 to High(aTableNameArr) do
    begin
//      if FMDB.TableExists(aTableNameArr[j]) then
//        exit;

      db_OpenQuery(qry_Temp, 'SELECT TOP 1 * FROM '+ aTableNameArr[j]);

      sSQL:= mdb_MakeCreateSql_From_Dataset (qry_Temp, aTableNameArr[j]);

      dmMDB.ADOConnection1.Execute (sSQL);


    end;
  end;
  // ---------------------------------------------------------------

const
  ARR: array[0..3] of string =
    (
     TBL_OBJECT_FIELDS,
     TBL_OBJECTS,
     TBL_PICKLIST,
     TBL_PICKLIST_VALUES
     );
//     TBL_REPORT);

var
  I: Integer;
  oList: TStringList;
  sSQL: string;
  sTable: string;

begin

  oList:= TStringList.Create;
  dmMDB.ADOConnection1.GetTableNames(oList);

  for I := 0 to High(ARR) do
  begin
    sTable:=ARR[i];

    if oList.IndexOf(sTable)>=0 then
      dmMDB.ADOConnection1.Execute(Format('DROP TABLE %s', [sTable]));

    DoCreate_tables([sTable]);
  end;

  oList.Free;


  sSQL:= Format('ALTER TABLE %s ADD PRIMARY KEY (id) ', [TBL_OBJECTS]);
  dmMDB.ADOConnection1.Execute(sSQL);

  sSQL:= Format('ALTER TABLE %s ADD PRIMARY KEY (id) ', [TBL_OBJECT_FIELDS]);
  dmMDB.ADOConnection1.Execute(sSQL);

 {
  sSQL:= Format('ALTER TABLE %s ADD FOREIGN KEY (object_id) '+
             'REFERENCES %s(id) ON DELETE CASCADE',
             [TBL_OBJECT_FIELDS, TBL_OBJECTS]);

  dmMDB.ADOConnection1.Execute(sSQL);
  }

end;

procedure TdmExport_Lib1111.Exec;
begin
    Create_ObjectTables;
    Export_Lib_Tables();

end;

//----------------------------------------------------------
// ���������� ������� objects
//----------------------------------------------------------

//----------------------------------------------------------
procedure TdmExport_Lib1111.Export_Lib_Tables;
//----------------------------------------------------------

const
  ARR: array[0..3] of record  Name: string; SQL: string;  end =
   (
    (Name: TBL_OBJECTS;
     SQL : 'SELECT * FROM _objects where (name not like ''%[_][_]'')'),

    (Name: TBL_OBJECT_FIELDS;
     SQL : 'SELECT * FROM _object_fields WHERE object_id IN '+
           '  (SELECT id FROM _objects where (name not like ''%[_][_]'') )'),

    (Name: TBL_PICKLIST;
     SQL : 'SELECT * FROM '+ TBL_PICKLIST),

    (Name: TBL_PICKLIST_VALUES;
     SQL : 'SELECT * FROM '+ TBL_PICKLIST_VALUES)

//    (Name: TBL_REPORT;
 //    SQL : 'SELECT * FROM '+ TBL_REPORT + ' WHERE is_default=1')
    );


var
  I: Integer;
  sSQL: string;
  sTableName: string;
begin
 // db_SetComponentsADOConn([t_Objects, t_Object_fields], dmMDB.ADOConnection1);
  db_SetComponentsADOConn([ADOTable_dest], dmMDB.ADOConnection1);
  db_SetComponentsADOConn([ADOQuery1], dmMain_SQL.AdoConnection1);


  for I := 0 to High(ARR) do
  begin
   // sTableName:=ARR[i].Name;

  //  if db_IsTableExists(dmMDB.ADOConnection1, sTableName) then
   //   dmMDB.ADOConnection1.Execute('DROP TABLE '+ sTableName);

    ADOTable_dest.TableName := ARR[i].Name;
    db_OpenQuery(ADOQuery1,   ARR[i].SQL);
    db_CopyRecords(ADOQuery1, ADOTable_dest);

  end;


  sSQL:= Format('ALTER TABLE %s ADD FOREIGN KEY (object_id) '+
             'REFERENCES %s(id) ON DELETE CASCADE',
             [TBL_OBJECT_FIELDS, TBL_OBJECTS]);

  dmMDB.ADOConnection1.Execute(sSQL);


end;


class procedure TdmExport_Lib1111.Init;
begin
//  if not Assigned(dmExport_Lib) then
//    dmExport_Lib:= TdmExport_Lib.Create(Application);

end;





end.


(*



 // db_View(aDataset);

 {$IFDEF old}



    if not t_Sys_Objects_columns.Locate(FLD_TABLENAME+';'+FLD_NAME,
       VarArrayOf([sTableName, sName]), []) then

    //  with t_Sys_Objects_columns do
       // begin
         // Log(sTableName+'-'+ sName);

         try

          b:=db_AddRecord (t_Sys_Objects_columns,
            [
             db_Par(FLD_PROJECT_NAME,    Params.Project_name1),

             db_Par(FLD_TABLENAME,       obj.TABLENAME),
             db_Par(FLD_NAME,            obj.NAME),

             db_Par(FLD_TYPE,            obj.Type_),
//             db_Par(FLD_LENGTH,          IIF(obj.Length=0,null, obj.Length)),
             db_Par(FLD_LENGTH,          obj.Length),
             db_Par(FLD_VARIABLE,        obj.IsVariableLen),
             db_Par(FLD_isnullable,      obj.isnullable),

             db_Par(FLD_IsComputed,      obj.IsComputed),
             db_Par(FLD_Computed_Text,   obj.ComputedText),

             db_Par(FLD_IDENTITY,        obj.IsIdentity),

             db_Par(FLD_DEFAULT_NAME,    obj.DEFAULT_NAME),
             db_Par(FLD_DEFAULT_CONTENT, obj.DEFAULT_CONTENT)
            ]);

         except
           on E: Exception do
             ShowMessage(obj.TableName + ' -- ' + obj.Name);

         end;

       //   if not b then
        //    ShowMessage(sTableName+'-'+ sName);
     //   end;


      Next;

   end;


   
      obj.TYPE_         := s;
      obj.Length        := FieldValues[FLD_Length];
//      obj.Size          :=FieldValues[FLD_Size];
      obj.IsVariableLen := FieldValues[FLD_VARIABLE];
      obj.IsNullable    := FieldValues[FLD_IsNullable];
      obj.IsIdentity    := FieldByName('IsIdentity').AsInteger=1;
      obj.IsComputed    := FieldByName('IsComputed').AsInteger=1;

      // ---------------------------------------------------------------
      if q_Computed_columns.Locate('OBJECT_NAME;NAME',
        VarArrayOf([FieldValues[FLD_TABLENAME], FieldValues[FLD_NAME]] ), [] ) then
      begin
        obj.ComputedText := q_Computed_columns.FieldByName(FLD_TEXT).AsString;
        obj.TYPE_ :='';
        obj.Length:=0;
      end;


      // ---------------------------------------------------------------
      if q_DF.Locate('TABLENAME;COLUMN_NAME',
        VarArrayOf([FieldValues[FLD_TABLENAME],FieldValues[FLD_NAME]] ), [] ) then
      begin
        obj.DEFAULT_NAME    := q_DF.FieldByName(FLD_DEFAULT_NAME).AsString;
        obj.DEFAULT_CONTENT := q_DF.FieldByName(FLD_DEFAULT_CONTENT).AsString;


        if (LeftStr(obj.DEFAULT_CONTENT,2)='((') and
           (RightStr(obj.DEFAULT_CONTENT,2) = '))')
        then
          obj.DEFAULT_CONTENT :=Copy(obj.DEFAULT_CONTENT, 2, Length(obj.DEFAULT_CONTENT)-2);

      end;


      


//----------------------------------------------------------
procedure TdmExport_Lib.ExportTableFields;
//----------------------------------------------------------
const

//  SQL_ALL_NEW =
//    'SELECT *,                                                                           '+
//    '   COLUMNPROPERTY(OBJECT_ID(c.TABLE_NAME),c.COLUMN_NAME,''IsIdentity'') as IsIdentity'+
//    '                                                                                     '+
//    'FROM  INFORMATION_SCHEMA.COLUMNS AS c                                                '+
//    '     JOIN  INFORMATION_SCHEMA.TABLES AS t  ON t.TABLE_NAME = c.TABLE_NAME            '+
//    'WHERE                                                       '+
//    '    (t.TABLE_TYPE = ''Base Table'') AND                     '+
//    '    (t.TABLE_NAME  NOT LIKE ''[_][_]%'') AND                '+
//    '    (c.COLUMN_NAME NOT LIKE ''[_][_]%'')                    '+
//    'order by t.TABLE_name                                       ';
//
//    

  SQL_ALL =
    ' SELECT  TOP 100 PERCENT o.id AS table_id, o.name AS tablename,   '+
    '    c.colid, c.name,  c.length , c.iscomputed, '+ //AS size
    '    t.variable, t.name AS type,          '+ // 0 AS PK, AS Variable
    '    ColumnProperty(o.id, c.name, ''IsIdentity'') AS IsIdentity,   '+
    '    c.isnullable AS isnullable                                    '+
    ' FROM  sysobjects o INNER JOIN                                    '+
    '       syscolumns c ON o.id = c.id INNER JOIN                     '+
    '       systypes t ON c.xusertype = t.xusertype                    '+
    ' WHERE (o.xtype = ''U'')                                       '+
    '     AND  (o.name <> ''dtp%'')                                    '+
    '     AND  (o.name <> ''_tables'')                                 '+
  //  '     AND  (o.name NOT LIKE ''[_][_]%'')                              '+
  //  '      AND  (c.name NOT LIKE ''[_][_]%'')                          '+
    ' ORDER BY o.name, c.colid                                         ';

{
  SQL_SELECT_DF =
    ' SELECT TOP 100 PERCENT                             '+
    '   c.name   AS COLUMN_NAME,                         '+
    '        OBJECT_NAME(o.parent_obj) AS tablename ,    '+
    '   o.name   AS DEFAULT_NAME,                        '+
    '   com.Text AS DEFAULT_CONTENT                      '+
    ' FROM sysobjects o INNER JOIN                       '+
    '      syscolumns c ON o.id = c.cdefault INNER JOIN  '+
    '      syscomments com ON o.id = com.id              '+
    ' WHERE o.xtype = ''D''                              '+
    ' ORDER BY tablename                                 ';


  SQL_SELECT_COMPUTED =
    'SELECT     TOP 100 PERCENT OBJECT_NAME(dbo.syscolumns.id) AS OBJECT_NAME, dbo.syscolumns.name AS name, Comments.text AS text '+
    'FROM         dbo.syscolumns INNER JOIN                                                                                       '+
    '                      dbo.syscomments Comments ON dbo.syscolumns.id = Comments.id AND dbo.syscolumns.colid = Comments.number '+
    'WHERE     (dbo.syscolumns.iscomputed = 1) AND (dbo.syscolumns.xtype = 62)                                                    ';
}

var
  b: Boolean;
  I: Integer;
  sTableName, sName: string;
  v: variant;
  obj: TDBTableField;
  s: string;
  oTable: TDBObject;
  s1: string;

begin
///  Assert(Assigned(FDBStructure_ref));


 // db_OpenQuery(q_DF, SQL_SELECT_DF);

 // db_OpenQuery(q_Computed_columns, SQL_SELECT_COMPUTED);
//  db_OpenQuery(q_Computed_columns, q_Computed_columns.SQL.Text);

 // db_View(q_DF);

//  db_OpenQuery(qry_TableFields, SQL_ALL_NEW);


  if not db_OpenQuery(qry_TableFields, SQL_ALL) then
    Exit;


//  try
    with qry_TableFields do
      while not EOF do
    begin
      Progress2(RecNo, RecordCount);
      if FTerminated then
        Break;


      sName     := FieldValues[FLD_NAME];
      sTABLENAME:=FieldValues[FLD_TABLENAME];

   //   if Length(sName)<=3 then
   //     ShowMessage(sName);
{
      if //(not NameIsCorrectForExport(sName)) or
         (not tableNameIsCorrectForExport(sTABLENAME))
      then
     // s:=LeftStr(sName,1);
     // if (s='-') or (s='_') then           // or (s='_')     '_'
      begin
        Next;
        Continue;
      end;


      if (LeftStr(sName,1)='_') or (LeftStr(sName,1)='-') then
      begin
        Next;
        Continue;
      end;


    sTableName :=FieldValues[FLD_TABLENAME];

    oTable := FDBStructure_ref.Tables.FindByName(sTableName);

    if not Assigned(oTable) then
    begin
      Next;
      Continue;
    end;
  }

  //  if Assigned(oTable) then
    begin
      obj:=oTable.TableFields.AddItem;

      obj.Name          :=FieldValues[FLD_NAME];
      obj.TableName     :=FieldValues[FLD_TABLENAME];



   //   s1:=FieldValues['Usertype'];

      s:=FieldValues[FLD_TYPE];

//      if  then
        

      if s='TComment'  then  s:='varchar' else
      if s='TBoolean'  then  s:='bit'     else
      if s='TName'     then  s:='varchar' else
      if s='TName100'  then  s:='varchar' else
      if s='TName250'  then  s:='varchar' else
      if s='TKey'      then  s:='int' else
      if s='TIdentity' then  s:='int' else ;
    //  if s='TIdentity' then s:='int' else




    end;


 //   s:=


end;

