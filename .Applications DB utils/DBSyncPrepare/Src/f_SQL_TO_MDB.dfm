inherited fSQL_TO_MDB: TfSQL_TO_MDB
  Left = 1027
  Top = 413
  Width = 768
  Height = 848
  Caption = #1055#1088#1086#1075#1088#1072#1084#1084#1072' '#1089#1080#1085#1093#1088#1086#1085#1080#1079#1072#1094#1080#1080' '#1089#1090#1088#1091#1082#1090#1091#1088' '#1073#1072#1079' '#1076#1072#1085#1085#1099#1093'. '#1057#1077#1088#1074#1077#1088#1085#1072#1103' '#1095#1072#1089#1090#1100'. '
  OldCreateOrder = True
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 744
    Width = 760
    Height = 76
    Constraints.MaxHeight = 0
    Constraints.MinHeight = 0
    inherited Bevel1: TBevel
      Width = 760
    end
    inherited Panel3: TPanel
      Left = 581
      Height = 74
      inherited btn_Ok: TButton
        Left = 4
      end
      inherited btn_Cancel: TButton
        Left = 88
        Caption = #1042#1099#1093#1086#1076
      end
    end
    object Button1: TButton
      Left = 184
      Top = 41
      Width = 75
      Height = 25
      Action = act_Versions
      TabOrder = 1
    end
    object Button3: TButton
      Left = 184
      Top = 5
      Width = 113
      Height = 25
      Action = act_Database_tables
      TabOrder = 2
    end
    object Button4: TButton
      Left = 304
      Top = 5
      Width = 105
      Height = 23
      Action = act_Structure_new
      TabOrder = 3
    end
    object Button6: TButton
      Left = 416
      Top = 5
      Width = 105
      Height = 23
      Action = act_Actions
      TabOrder = 4
    end
    object btn_Save_Service_Script: TButton
      Left = 8
      Top = 40
      Width = 161
      Height = 25
      Caption = 'Save_Service_Script'
      TabOrder = 5
      OnClick = btn_Save_Service_ScriptClick
    end
    object Button9: TButton
      Left = 8
      Top = 5
      Width = 81
      Height = 23
      Caption = 'Structure new'
      TabOrder = 6
      OnClick = Button9Click
    end
  end
  inherited pn_Top_: TPanel
    Width = 760
    inherited Bevel2: TBevel
      Width = 760
    end
    inherited pn_Header: TPanel
      Width = 760
      Visible = False
      inherited lb_Action: TLabel
        Width = 300
        Caption = #1069#1082#1089#1087#1086#1088#1090' '#1076#1072#1085#1085#1099#1093' '#1080#1079' SQL-'#1089#1077#1088#1074#1077#1088#1072' Infotel '#1074' '#1092#1072#1081#1083' MS ACCESS'
      end
    end
  end
  object pn_main: TPanel [2]
    Left = 0
    Top = 60
    Width = 760
    Height = 293
    Align = alTop
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 2
    object gb_ConnectSQL: TGroupBox
      Left = 5
      Top = 5
      Width = 750
      Height = 55
      Align = alTop
      Caption = #1055#1086#1076#1082#1083#1102#1095#1077#1085#1080#1077' '#1082' SQL-server'
      TabOrder = 0
    end
    object GroupBox2: TGroupBox
      Left = 5
      Top = 60
      Width = 750
      Height = 69
      Align = alTop
      Caption = 'db_version.ini'
      TabOrder = 1
      object ed_db_version1: TFilenameEdit
        Left = 5
        Top = 17
        Width = 500
        Height = 21
        DefaultExt = '*.ini'
        Filter = 'INI files (*.ini)|*.ini'
        NumGlyphs = 1
        TabOrder = 0
        Text = '"W:\RPLS_DB tools\_bin\db_version.ini"'
      end
      object ed_db_version2: TFilenameEdit
        Left = 5
        Top = 41
        Width = 500
        Height = 21
        DefaultExt = '*.ini'
        Filter = 'INI files (*.ini)|*.ini'
        NumGlyphs = 1
        TabOrder = 1
        Text = 'C:\ONEGA\RPLS_DB\bin\db_version.ini'
      end
    end
    object GroupBox3: TGroupBox
      Left = 5
      Top = 175
      Width = 750
      Height = 46
      Align = alTop
      Caption = 'SQL script'
      TabOrder = 2
      object ed_Script: TFilenameEdit
        Left = 5
        Top = 17
        Width = 500
        Height = 21
        DefaultExt = '*.ini'
        Filter = 'SQL files (*.sql)|*.sql'
        NumGlyphs = 1
        TabOrder = 0
        Text = '"W:\RPLS_DB tools\_bin\onega.sql"'
      end
      object Button5: TButton
        Left = 528
        Top = 16
        Width = 75
        Height = 25
        Caption = 'test'
        TabOrder = 1
        OnClick = Button5Click
      end
    end
    object GroupBox1: TGroupBox
      Left = 5
      Top = 129
      Width = 750
      Height = 46
      Align = alTop
      Caption = 'Publish'
      TabOrder = 3
      object ed_Publish_dir: TDirectoryEdit
        Left = 5
        Top = 18
        Width = 500
        Height = 21
        InitialDir = 'C:\ONEGA\RPLS_DB\bin\'
        NumGlyphs = 1
        TabOrder = 0
        Text = 'C:\ONEGA\RPLS_DB\bin\'
      end
    end
    object GroupBox4: TGroupBox
      Left = 5
      Top = 221
      Width = 750
      Height = 46
      Align = alTop
      Caption = 'Include Library'
      TabOrder = 4
      object ed_Library: TFilenameEdit
        Left = 40
        Top = 17
        Width = 465
        Height = 21
        DefaultExt = '*.ini'
        Filter = 'SQL files (*.sql)|*.sql'
        NumGlyphs = 1
        TabOrder = 0
        Text = 'D:\script.sql'
      end
      object cb_Include_Library: TCheckBox
        Left = 8
        Top = 18
        Width = 25
        Height = 17
        TabOrder = 1
      end
    end
  end
  object GroupBox1_Log: TGroupBox [3]
    Left = 0
    Top = 673
    Width = 760
    Height = 71
    Align = alBottom
    Caption = ' '#1046#1091#1088#1085#1072#1083' '#1089#1086#1073#1099#1090#1080#1081' '
    TabOrder = 3
    object RichEdit1: TRichEdit
      Left = 2
      Top = 15
      Width = 756
      Height = 54
      Align = alClient
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
  object PageControl1: TPageControl [4]
    Left = 0
    Top = 448
    Width = 760
    Height = 225
    ActivePage = TabSheet1
    Align = alBottom
    TabOrder = 4
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      object cxVerticalGrid1: TcxVerticalGrid
        Left = 0
        Top = 0
        Width = 752
        Height = 197
        Align = alClient
        LookAndFeel.Kind = lfFlat
        OptionsView.PaintStyle = psDelphi
        OptionsView.RowHeaderWidth = 190
        TabOrder = 0
        Version = 1
        object row_SQL_script: TcxEditorRow
          Properties.Caption = 'SQL script'
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object row_Include_Library: TcxEditorRow
          Properties.Caption = 'Include Library'
          Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
          Properties.EditProperties.ImmediatePost = True
          Properties.EditProperties.NullStyle = nssUnchecked
          Properties.EditProperties.UseAlignmentWhenInplace = True
          Properties.DataBinding.ValueType = 'Boolean'
          Properties.Value = Null
          ID = 1
          ParentID = 0
          Index = 0
          Version = 1
        end
        object row_Library_filename: TcxEditorRow
          Properties.Caption = 'Library_filename'
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
          ID = 2
          ParentID = 0
          Index = 1
          Version = 1
        end
        object cxVerticalGrid1EditorRow3: TcxEditorRow
          Properties.Caption = 'db_version.ini'
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
          ID = 3
          ParentID = -1
          Index = 1
          Version = 1
        end
        object cxVerticalGrid1EditorRow4: TcxEditorRow
          Properties.Caption = 'Publish'
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
          ID = 4
          ParentID = -1
          Index = 2
          Version = 1
        end
        object row_Is_Export_Objects_and_PickList: TcxEditorRow
          Properties.Caption = 'Export_Objects_and_PickList'
          Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
          Properties.EditProperties.ImmediatePost = True
          Properties.EditProperties.UseAlignmentWhenInplace = True
          Properties.DataBinding.ValueType = 'Boolean'
          Properties.Value = True
          ID = 5
          ParentID = -1
          Index = 3
          Version = 1
        end
        object row_Is_Export_DatabaseInfo_New: TcxEditorRow
          Properties.Caption = 'Export_DatabaseInfo_New'
          Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
          Properties.EditProperties.UseAlignmentWhenInplace = True
          Properties.DataBinding.ValueType = 'Boolean'
          Properties.Value = True
          ID = 6
          ParentID = -1
          Index = 4
          Version = 1
        end
        object row_Is_Save_Service_ScriptToFile: TcxEditorRow
          Properties.Caption = 'Save_Service_Script'
          Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
          Properties.EditProperties.ImmediatePost = True
          Properties.EditProperties.NullStyle = nssInactive
          Properties.EditProperties.UseAlignmentWhenInplace = True
          Properties.DataBinding.ValueType = 'Boolean'
          Properties.Value = False
          ID = 7
          ParentID = -1
          Index = 5
          Version = 1
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      object cxDBVerticalGrid1: TcxDBVerticalGrid
        Left = 0
        Top = 0
        Width = 752
        Height = 197
        Align = alClient
        LayoutStyle = lsMultiRecordView
        LookAndFeel.Kind = lfOffice11
        LookAndFeel.NativeStyle = False
        OptionsView.PaintStyle = psDelphi
        OptionsView.RowHeaderMinWidth = 20
        OptionsView.RowHeaderWidth = 213
        OptionsView.ValueWidth = 312
        TabOrder = 0
        DataController.DataSource = ds_Projects
        Version = 1
        object cxDBVerticalGrid1name: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'name'
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid1CategoryRow1: TcxCategoryRow
          Properties.Caption = 'Connection'
          ID = 1
          ParentID = -1
          Index = 1
          Version = 1
        end
        object row_server: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
          Properties.EditProperties.DropDownRows = 20
          Properties.EditProperties.OnInitPopup = cxDBVerticalGrid1serverEditPropertiesInitPopup
          Properties.DataBinding.FieldName = 'server'
          ID = 2
          ParentID = 1
          Index = 0
          Version = 1
        end
        object row_auth: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
          Properties.EditProperties.DropDownListStyle = lsFixedList
          Properties.EditProperties.Items.Strings = (
            'sql'
            'win')
          Properties.DataBinding.FieldName = 'auth'
          ID = 3
          ParentID = 1
          Index = 1
          Version = 1
        end
        object row_Login: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'login'
          ID = 4
          ParentID = 1
          Index = 2
          Version = 1
        end
        object row_password: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'password'
          ID = 5
          ParentID = 1
          Index = 3
          Version = 1
        end
        object row_database: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
          Properties.EditProperties.DropDownRows = 20
          Properties.EditProperties.OnInitPopup = row_databaseEditPropertiesInitPopup
          Properties.DataBinding.FieldName = 'database'
          ID = 6
          ParentID = 1
          Index = 4
          Version = 1
        end
        object cxDBVerticalGrid1CategoryRow3: TcxCategoryRow
          Properties.Caption = 'library'
          ID = 7
          ParentID = -1
          Index = 2
          Version = 1
        end
        object cxDBVerticalGrid1library_enabled: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
          Properties.EditProperties.UseAlignmentWhenInplace = True
          Properties.DataBinding.FieldName = 'library_enabled'
          ID = 8
          ParentID = 7
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid1library_path: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1library_pathEditPropertiesButtonClick
          Properties.DataBinding.FieldName = 'library_path'
          ID = 9
          ParentID = 7
          Index = 1
          Version = 1
        end
        object cxDBVerticalGrid1CategoryRow2: TcxCategoryRow
          Properties.Caption = 'Export'
          ID = 10
          ParentID = -1
          Index = 3
          Version = 1
        end
        object cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
          Properties.EditProperties.UseAlignmentWhenInplace = True
          Properties.DataBinding.FieldName = 'Is_Export_DatabaseInfo'
          ID = 11
          ParentID = 10
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
          Properties.EditProperties.UseAlignmentWhenInplace = True
          Properties.DataBinding.FieldName = 'Is_Export_Objects_and_PickList'
          ID = 12
          ParentID = 10
          Index = 1
          Version = 1
        end
        object cxDBVerticalGrid1CategoryRow4: TcxCategoryRow
          Properties.Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1086
          ID = 13
          ParentID = -1
          Index = 4
          Version = 1
        end
        object cxDBVerticalGrid1DBEditorRow3: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxMemoProperties'
          Properties.EditProperties.ScrollBars = ssBoth
          Properties.EditProperties.VisibleLineCount = 10
          Properties.DataBinding.FieldName = 'exceptions'
          ID = 14
          ParentID = 13
          Index = 0
          Version = 1
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'TabSheet3'
      ImageIndex = 2
      object DBLookupComboBox1: TDBLookupComboBox
        Left = 8
        Top = 16
        Width = 249
        Height = 21
        KeyField = 'id'
        ListField = 'name'
        ListSource = ds_Projects
        TabOrder = 0
      end
      object Button8: TButton
        Left = 8
        Top = 72
        Width = 75
        Height = 25
        Caption = 'Button8'
        TabOrder = 1
        OnClick = Button8Click
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'TabSheet4'
      ImageIndex = 3
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 752
        Height = 197
        Align = alClient
        DataSource = ds_Projects
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -10
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 352
    Top = 8
    object act_Structure_new: TAction [1]
      Caption = 'act_Structure_new'
      OnExecute = act_StructureExecute
    end
    object act_SetupTables: TAction
      Category = 'Main'
      Caption = 'act_SetupTables'
    end
    object act_Structure: TAction
      Caption = 'Structure'
      OnExecute = act_StructureExecute
    end
    object act_Versions: TAction
      Caption = 'Versions'
      OnExecute = act_StructureExecute
    end
    object act_Database_tables: TAction
      Caption = 'Database_tables'
      OnExecute = act_StructureExecute
    end
    object act_Actions: TAction
      Caption = 'act_Actions'
      OnExecute = act_StructureExecute
    end
  end
  inherited FormStorage1: TFormStorage
    StoredProps.Strings = (
      'ed_db_version1.FileName'
      'ed_Script.Text'
      'ed_Library.FileName'
      'cb_Include_Library.Checked'
      'ed_db_version2.FileName'
      'PageControl1.ActivePage')
    Left = 416
    Top = 8
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 559
  end
  object ActionList2: TActionList
    Left = 216
    Top = 360
    object act_FileOpen1: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
    end
    object FileSaveAsIni: TFileSaveAs
      Category = 'File'
      Caption = 'Save &As...'
      Dialog.DefaultExt = '*.ini'
      Dialog.Filter = '*.ini|*.ini'
      Hint = 'Save As|Saves the active file with a new name'
      ImageIndex = 30
    end
  end
  object ds_Projects: TDataSource
    DataSet = t_Projects
    Left = 496
    Top = 368
  end
  object t_Projects: TADOTable
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 'Projects'
    Left = 440
    Top = 368
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\ONEGA\RPLS_DB\bi' +
      'n\install_db.mdb;Persist Security Info=False;'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 336
    Top = 364
  end
  object cxPropertiesStore1: TcxPropertiesStore
    Components = <
      item
        Component = row_Is_Save_Service_ScriptToFile
        Properties.Strings = (
          'Properties.Value')
      end>
    StorageName = 'cxPropertiesStore1'
    StorageType = stRegistry
    Left = 640
    Top = 376
  end
end
