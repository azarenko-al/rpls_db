unit d_Actions;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, DB,
       

  dm_MDB,

  u_db,

  ADODB, rxPlacemnt;

type
  Tdlg_Actions1111111 = class(TForm)
    ds_Tables: TDataSource;
    ADOTable1: TADOTable;
    MDBConnection: TADOConnection;
    cxGrid11: TcxGrid;
    cxGridDBTableView9: TcxGridDBTableView;
    cxGridLevel12: TcxGridLevel;
    FormStorage1: TFormStorage;
    cxGridDBTableView9id: TcxGridDBColumn;
    cxGridDBTableView9xtype: TcxGridDBColumn;
    cxGridDBTableView9name: TcxGridDBColumn;
    cxGridDBTableView9action: TcxGridDBColumn;
    cxGridDBTableView9column_name: TcxGridDBColumn;
    cxGridDBTableView9category: TcxGridDBColumn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class procedure ShowForm;
    { Public declarations }
  end;


implementation




{$R *.dfm}

//var
/////  dlg_Actions: Tdlg_Actions;



//-------------------------------------------------------------------
class procedure Tdlg_Actions1111111.ShowForm;
//-------------------------------------------------------------------
begin
  if not Assigned(dlg_Actions) then
    Application.CreateForm(Tdlg_Actions, dlg_Actions);

  dlg_Actions.Show;

(*  else
  with Tdlg_Structure.Create(Application) do
  begin
    Open();

    Show;
  end;
*)

end;

// ---------------------------------------------------------------
procedure Tdlg_Actions1111111.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  db_SetComponentADOConnection(Self, dmMDB.ADOConnection1);


  cxGrid11.Align:=alClient;

  if MDBConnection.Connected then
  begin
    ADOTable1.Active:=true;

 //   ShowMessage('MDBConnection.Connected');
 //   MDBConnection.Close;
  end;

//  cxGridDBTableView9.

end;

procedure Tdlg_Actions1111111.FormDestroy(Sender: TObject);
begin
 dlg_Actions  := nil;
end;



procedure Tdlg_Actions1111111.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action :=caFree;
end;


end.
