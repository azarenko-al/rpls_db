object dlg_Actions1111111: Tdlg_Actions1111111
  Left = 930
  Top = 291
  Width = 887
  Height = 511
  Caption = 'dlg_Actions1111111'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid11: TcxGrid
    Left = 0
    Top = 0
    Width = 879
    Height = 241
    Align = alTop
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    object cxGridDBTableView9: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = ds_Tables
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrDisabled
      OptionsCustomize.DataRowSizing = True
      OptionsCustomize.GroupRowSizing = True
      OptionsView.Footer = True
      OptionsView.FooterAutoHeight = True
      OptionsView.FooterMultiSummaries = True
      object cxGridDBTableView9id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Width = 32
      end
      object cxGridDBTableView9action: TcxGridDBColumn
        DataBinding.FieldName = 'action'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Items.Strings = (
          'drop')
        Width = 75
      end
      object cxGridDBTableView9xtype: TcxGridDBColumn
        DataBinding.FieldName = 'xtype'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Items.Strings = (
          'view'
          'column'
          'sp')
        Width = 109
      end
      object cxGridDBTableView9name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Width = 260
      end
      object cxGridDBTableView9column_name: TcxGridDBColumn
        DataBinding.FieldName = 'column_name'
        Width = 146
      end
      object cxGridDBTableView9category: TcxGridDBColumn
        DataBinding.FieldName = 'category'
        PropertiesClassName = 'TcxComboBoxProperties'
        Width = 141
      end
    end
    object cxGridLevel12: TcxGridLevel
      GridView = cxGridDBTableView9
    end
  end
  object ds_Tables: TDataSource
    DataSet = ADOTable1
    Left = 48
    Top = 384
  end
  object ADOTable1: TADOTable
    Active = True
    Connection = MDBConnection
    CursorType = ctStatic
    TableDirect = True
    TableName = '__actions'
    Left = 48
    Top = 332
  end
  object MDBConnection: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=C:\ON' +
      'EGA\RPLS_DB\bin\install_db.mdb;Mode=Share Deny None;Persist Secu' +
      'rity Info=False;Jet OLEDB:System database="";Jet OLEDB:Registry ' +
      'Path="";Jet OLEDB:Database Password="";Jet OLEDB:Engine Type=5;J' +
      'et OLEDB:Database Locking Mode=1;Jet OLEDB:Global Partial Bulk O' +
      'ps=2;Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:New Database' +
      ' Password="";Jet OLEDB:Create System Database=False;Jet OLEDB:En' +
      'crypt Database=False;Jet OLEDB:Don'#39't Copy Locale on Compact=Fals' +
      'e;Jet OLEDB:Compact Without Replica Repair=False;Jet OLEDB:SFP=F' +
      'alse'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 200
    Top = 336
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 312
    Top = 338
  end
end
