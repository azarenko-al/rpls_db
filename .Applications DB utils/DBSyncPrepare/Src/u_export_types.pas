unit u_export_types11111111;

interface

type

  TFieldRec11111111111 = record

    TABLENAME : string;
    NAME   : string;

//    TYPE,        obj.Type_),
    //          D_LENGTH,      IIF(obj.Length=0,null, obj.Length)),
    LENGTH : Integer;
    VARIABLE  : boolean;
    isnullable : boolean;

    IsComputed : boolean;
    Computed_Text : string;

    IDENTITY  : boolean;

    DEFAULT_NAME  : string;
    DEFAULT_CONTENT : string;

   end;


   TObjectRec = record

      PROJECT_NAME : string;

      NAME  : string;
      TABLENAME : string;
      SCHEMA : string;

      CONTENT : string;
      XTYPE : string;


      Index: record
        index_description : string;

        IS_PRIMARY_KEY : boolean;
        IS_UNIQUE_KEY : boolean;
        IS_UNIQUE_INDEX : boolean;

        INDEX_KEYS : string;

        INDEX_TYPE : string;
      end;

      FK : record
        PK_TABLE_NAME : string;
        PK_COLUMN_NAME : string;

        FK_TABLE_NAME : string;
        FK_COLUMN_NAME  : string;

        IsDeleteCascade : boolean;
        IsUpdateCascade : boolean;

      end;

  end;



implementation




end.
