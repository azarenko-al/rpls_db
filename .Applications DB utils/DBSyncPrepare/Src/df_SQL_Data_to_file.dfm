object data_form_SQL_Data_to_file_1111111: Tdata_form_SQL_Data_to_file_1111111
  Left = 663
  Top = 305
  Width = 1216
  Height = 653
  Caption = 'data_form_SQL_Data_to_file_1111111'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 719
    Top = 118
    Width = 75
    Height = 25
    Caption = 'Run'
    TabOrder = 0
  end
  object FilenameEdit1: TFilenameEdit
    Left = 819
    Top = 120
    Width = 359
    Height = 21
    NumGlyphs = 1
    TabOrder = 1
    Text = 'd:\script.sql'
  end
  object Button2: TButton
    Left = 846
    Top = 20
    Width = 75
    Height = 24
    Caption = 'Open'
    TabOrder = 2
  end
  object DBGrid2: TDBGrid
    Left = 24
    Top = 150
    Width = 570
    Height = 91
    DataSource = ds_Project
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBGrid3: TDBGrid
    Left = 24
    Top = 264
    Width = 498
    Height = 217
    DataSource = ds_Tables
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBGrid4: TDBGrid
    Left = 16
    Top = 496
    Width = 273
    Height = 108
    DataSource = ds_tables_with_identity
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object ADOConnection_DB: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_empty;Data Source=SERVER1;Use Proced' +
      'ure for Prepare=1;Auto Translate=True;Packet Size=4096;Workstati' +
      'on ID=ALEX;Use Encryption for Data=False;Tag with column collati' +
      'on when possible=False'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 96
    Top = 40
  end
  object DataSource1: TDataSource
    DataSet = ADOTable1
    Left = 256
    Top = 96
  end
  object ADOTable1: TADOTable
    Connection = ADOConnection_DB
    CursorType = ctStatic
    TableName = 'AntennaType'
    Left = 256
    Top = 40
  end
  object ADOQuery1: TADOQuery
    Connection = ADOConnection_DB
    Parameters = <>
    Left = 488
    Top = 56
  end
  object ADOConnection_MDB: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\ONEGA\RPLS_DB\bi' +
      'n\install_db.mdb;Persist Security Info=False;'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 728
    Top = 24
  end
  object ds_tables_for_export: TDataSource
    DataSet = t_tables_for_export
    Left = 904
    Top = 424
  end
  object t_tables_for_export: TADOTable
    Active = True
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'project_id'
    MasterFields = 'id'
    MasterSource = ds_Project
    TableName = '__tables_for_export'
    Left = 904
    Top = 360
  end
  object t_Project: TADOTable
    Active = True
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    Filtered = True
    TableName = 'Projects'
    Left = 728
    Top = 360
  end
  object ds_Project: TDataSource
    DataSet = t_Project
    Left = 728
    Top = 416
  end
  object q_Tables: TADOQuery
    Active = True
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    DataSource = ds_Project
    Parameters = <
      item
        Name = 'id'
        Attributes = [paNullable]
        DataType = ftInteger
        NumericScale = 255
        Precision = 255
        Value = 1
      end>
    SQL.Strings = (
      'select * from '
      ''
      '__tables_for_export'
      ''
      'where '
      '  project_id = :id'
      'and enable = true'
      ''
      ''
      ''
      'order by name')
    Left = 1072
    Top = 360
  end
  object ds_Tables: TDataSource
    DataSet = q_Tables
    Left = 1072
    Top = 424
  end
  object q_tables_with_identity: TADOQuery
    Connection = ADOConnection_DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      ''
      'select '
      ''
      ''
      '--col.*  , '
      ''
      '--table_name,'
      ''
      #39'[dbo].['#39'+ table_name +'#39']'#39' as table_name,'
      'COLUMN_NAME,'
      ''
      'sc.Is_Identity'
      ''
      ''
      ''
      ' from  INFORMATION_SCHEMA.COLUMNS  col'
      ''
      
        'INNER JOIN sys.columns AS sc ON sc.object_id = object_id(table_s' +
        'chema + '#39'.'#39' + table_name) AND sc.NAME = col.COLUMN_NAME'
      ''
      'inner join sys.objects obj on obj.object_Id=sc.object_Id '
      ''
      
        'left join  sys.computed_columns cc on cc.name = col.COLUMN_NAME ' +
        'and cc.object_id = obj.object_Id'
      ''
      'where obj.[type]='#39'u'#39
      ''
      'and sc.Is_Identity = 1')
    Left = 584
    Top = 456
  end
  object ds_tables_with_identity: TDataSource
    DataSet = q_tables_with_identity
    Left = 584
    Top = 520
  end
end
