object dmSQL_Data_to_file11111111: TdmSQL_Data_to_file11111111
  OldCreateOrder = False
  Left = 936
  Top = 417
  Height = 364
  Width = 647
  object ds_tables_for_export: TDataSource
    DataSet = t_tables_for_export
    Left = 112
    Top = 144
  end
  object t_tables_for_export: TADOTable
    CursorType = ctStatic
    IndexFieldNames = 'project_id'
    MasterFields = 'id'
    TableName = '__tables_for_export'
    Left = 112
    Top = 80
  end
  object q_Tables: TADOQuery
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'id'
        Attributes = [paNullable]
        DataType = ftInteger
        NumericScale = 255
        Precision = 255
        Value = 2
      end>
    SQL.Strings = (
      'select * from '
      ''
      '__tables_for_export'
      ''
      'where '
      '  project_id = :id'
      'and enable = true'
      ''
      ''
      ''
      'order by name')
    Left = 264
    Top = 80
  end
  object ds_Tables: TDataSource
    DataSet = q_Tables
    Left = 264
    Top = 144
  end
end
