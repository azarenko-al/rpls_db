unit d_SelectObjectFields;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  d_Wizard, rxPlacemnt, ActnList, StdCtrls, ExtCtrls,
  dxTL, dxDBCtrl, dxCntner, dxDBTL, Db, dxmdaset, dxDBTLCl,
  dxDBGrid, ADODB, dxExEdtr,

  u_classes,

  dm_Main,

  u_const_db,

  u_DB,

  cxPropertiesStore

  ;

type
  Tdlg_SelectObjectFields = class(Tdlg_Wizard)
    mem_Fields: TdxMemData;
    ds_Fields: TDataSource;
    pn_Main: TPanel;
    spl4: TSplitter;
    dx_Objects: TdxDBGrid;
    col_Object_Name: TdxDBGridColumn;
    col_Object_id: TdxDBGridColumn;
    dx_Fields: TdxDBTreeList;
    col_Caption: TdxDBTreeListColumn;
    col_Name: TdxDBTreeListPickColumn;
    col_enabled: TdxDBTreeListCheckColumn;
    col_id: TdxDBTreeListColumn;
    col_Parent_id: TdxDBTreeListColumn;
    col_Checked: TdxDBTreeListCheckColumn;
    ds_Objects: TDataSource;
    qry_Objects: TADOQuery;
    qry_Temp: TADOQuery;
    procedure act_OkExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dx_ObjectsChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure col_CheckedToggleClick(Sender: TObject; const Text: String;
      State: TdxCheckBoxState);
    procedure FormDestroy(Sender: TObject);
  private
    FIDList: TIDList;
  public
    class procedure ExecDlg (aIDList: TIDList);
  end;

var
  dlg_SelectObjectFields: Tdlg_SelectObjectFields;

const
  SQL_SELECT_FIELDS =
    'SELECT * FROM ' + TBL_OBJECT_FIELDS +
    '  WHERE object_id=:id '+
    '  ORDER BY parent_id, index_';

//=========================================================
implementation {$R *.DFM}
//=========================================================

//------------------------------------------------------------
class procedure Tdlg_SelectObjectFields.ExecDlg(aIDList: TIDList);
//------------------------------------------------------------
begin
  with Tdlg_SelectObjectFields.Create(Application) do
  begin
    if ShowModal=mrOk then
      aIDList.Assign (FIDList);

    Free;
  end;
end;

//------------------------------------------------------------
procedure Tdlg_SelectObjectFields.act_OkExecute(Sender: TObject);
//------------------------------------------------------------
begin
  inherited;
  //
end;

//------------------------------------------------------------
procedure Tdlg_SelectObjectFields.FormCreate(Sender: TObject);
//------------------------------------------------------------
begin
  with Constraints do begin
    MaxHeight:=0;
    MinHeight:=0;
    MaxWidth :=0;
    MinWidth :=0;
  end;

  pn_Main.Align:= alClient;
  dx_Fields.Align:= alClient;

  SetActionName('�������� ���� ��� ����������(���� checked � ������ �������)');

  db_SetComponentsADOConn([qry_Temp, qry_Objects], dmMain.AdoConnection);

  FIDList:= TIDList.Create;

  db_CreateField(mem_Fields, [db_Field(FLD_CHECKED,     ftBoolean),
                              db_Field(FLD_ID,          ftInteger),
                              db_Field(FLD_PARENT_ID,   ftInteger),
                              db_Field(FLD_NAME,        ftString),
                              db_Field(FLD_CAPTION,     ftString),
                              db_Field(FLD_ENABLED,     ftBoolean)  ]);

  mem_Fields.Open;

  qry_Objects.DisableControls;
  db_OpenQuery(qry_Objects, 'SELECT * FROM _objects');
  qry_Objects.EnableControls;

  if Assigned(dx_Objects.FocusedNode) then
    dx_ObjectsChangeNode(nil, nil, dx_Objects.FocusedNode);
end;

//------------------------------------------------------------
procedure Tdlg_SelectObjectFields.dx_ObjectsChangeNode(Sender: TObject;
                                          OldNode, Node: TdxTreeListNode);
//------------------------------------------------------------
var
  iId: Integer;
begin
  iId:= Node.Values[col_Object_id.Index];

  db_OpenQuery(qry_Temp, SQL_SELECT_FIELDS, [db_Par(FLD_ID, iId)] );
  mem_Fields.DisableControls;
  db_CopyRecords(qry_Temp, mem_Fields);
  mem_Fields.First;
  mem_Fields.EnableControls;

  dx_Fields.FullExpand;
end;

//------------------------------------------------------------
procedure Tdlg_SelectObjectFields.col_CheckedToggleClick(Sender: TObject;
  const Text: String; State: TdxCheckBoxState);
//------------------------------------------------------------
var
  iId: Integer;
begin
  iId:= dx_Fields.FocusedNode.Values[col_id.Index];

  if Text = col_Checked.ValueChecked then
    FIDList.AddID(iId)
  else
    FIDList.DeleteByID(iId);
end;

procedure Tdlg_SelectObjectFields.FormDestroy(Sender: TObject);
begin
  FIDList.Free;
  
  inherited;
end;

end.
