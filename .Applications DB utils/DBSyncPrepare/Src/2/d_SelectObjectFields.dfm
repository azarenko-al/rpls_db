inherited dlg_SelectObjectFields: Tdlg_SelectObjectFields
  Left = 617
  Top = 133
  Width = 767
  Height = 538
  Caption = 'dlg_SelectObjectFields'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 469
    Width = 759
    inherited Bevel1: TBevel
      Width = 759
    end
    inherited btn_Ok: TButton
      Left = 596
    end
    inherited btn_Cancel: TButton
      Left = 680
    end
  end
  inherited pn_Top_: TPanel
    Width = 759
    inherited Bevel2: TBevel
      Width = 759
    end
    inherited pn_Header: TPanel
      Width = 759
    end
  end
  object pn_Main: TPanel [2]
    Left = 0
    Top = 60
    Width = 759
    Height = 282
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object spl4: TSplitter
      Left = 153
      Top = 0
      Height = 282
    end
    object dx_Objects: TdxDBGrid
      Left = 0
      Top = 0
      Width = 153
      Height = 282
      Bands = <
        item
          Caption = #1054#1090#1086#1073#1088#1072#1078#1072#1077#1084#1099#1077' '#1086#1073#1098#1077#1082#1090#1099
          Width = 133
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      SummaryGroups = <>
      SummarySeparator = ', '
      Align = alLeft
      TabOrder = 0
      DataSource = ds_Objects
      Filter.Criteria = {00000000}
      LookAndFeel = lfFlat
      OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEditing, edgoEnterShowEditor, edgoTabThrough, edgoVertThrough]
      OptionsDB = [edgoCanAppend, edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoUseBookmarks]
      OptionsView = [edgoBandHeaderWidth, edgoDrawEndEllipsis, edgoIndicator, edgoUseBitmap]
      OnChangeNode = dx_ObjectsChangeNode
      object col_Object_Name: TdxDBGridColumn
        Width = 96
        BandIndex = 0
        RowIndex = 0
        FieldName = 'name'
      end
      object col_Object_id: TdxDBGridColumn
        Width = 20
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Id'
      end
    end
    object dx_Fields: TdxDBTreeList
      Left = 240
      Top = 0
      Width = 519
      Height = 282
      Bands = <
        item
        end>
      DefaultLayout = False
      HeaderPanelRowCount = 1
      KeyField = 'id'
      ParentField = 'parent_id'
      Align = alRight
      DragMode = dmAutomatic
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      DataSource = ds_Fields
      LookAndFeel = lfFlat
      OptionsBehavior = [etoAutoDragDrop, etoAutoDragDropCopy, etoDragExpand, etoDragScroll, etoEditing, etoEnterShowEditor, etoImmediateEditor, etoStoreToRegistry, etoTabThrough]
      OptionsDB = [etoCancelOnExit, etoCanDelete, etoCanNavigation, etoCheckHasChildren, etoConfirmDelete, etoLoadAllRecords]
      OptionsView = [etoBandHeaderWidth, etoHotTrack, etoIndicator, etoUseBitmap, etoUseImageIndexForSelected]
      PaintStyle = psOutlook
      ShowBands = True
      ShowGrid = True
      TreeLineColor = clGrayText
      object col_Caption: TdxDBTreeListColumn
        DisableEditor = True
        Width = 127
        BandIndex = 0
        RowIndex = 0
        FieldName = 'caption'
      end
      object col_Checked: TdxDBTreeListCheckColumn
        Color = clYellow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clYellow
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 53
        BandIndex = 0
        RowIndex = 0
        FieldName = 'checked'
        ValueChecked = 'True'
        ValueUnchecked = 'False'
        OnToggleClick = col_CheckedToggleClick
      end
      object col_Name: TdxDBTreeListPickColumn
        DisableEditor = True
        Width = 112
        BandIndex = 0
        RowIndex = 0
        FieldName = 'name'
      end
      object col_enabled: TdxDBTreeListCheckColumn
        DisableEditor = True
        MinWidth = 20
        Width = 38
        BandIndex = 0
        RowIndex = 0
        FieldName = 'enabled'
        Border3D = True
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object col_id: TdxDBTreeListColumn
        DisableEditor = True
        Width = 20
        BandIndex = 0
        RowIndex = 0
        FieldName = 'id'
      end
      object col_Parent_id: TdxDBTreeListColumn
        DisableEditor = True
        Width = 59
        BandIndex = 0
        RowIndex = 0
        FieldName = 'parent_id'
      end
    end
  end
  object mem_Fields: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 320
    Top = 8
  end
  object ds_Fields: TDataSource
    DataSet = mem_Fields
    Left = 264
    Top = 8
  end
  object ds_Objects: TDataSource
    DataSet = qry_Objects
    Left = 128
    Top = 8
  end
  object qry_Objects: TADOQuery
    Parameters = <>
    Left = 184
    Top = 8
  end
  object qry_Temp: TADOQuery
    Parameters = <>
    Left = 512
    Top = 8
  end
end
