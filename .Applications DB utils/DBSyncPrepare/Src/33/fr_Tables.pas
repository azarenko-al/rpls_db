unit fr_Tables;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Grids,
  dxExEdtr, DBGrids, dxInspRw, dxDBInRw, dxInspct, dxDBInsp, ActnList, ImgList,
  AppEvnts, Menus, dxDBTLCl, dxDBTL, dxTL, dxDBCtrl, dxDBGrid, dxCntner, ExtCtrls,
  ComCtrls, StdActns, rxPlacemnt, StdCtrls, DBCtrls, ToolWin, cxControls, cxSplitter,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxClasses, cxGridCustomView, cxGridLevel, cxGrid,
  DB, dxmdaset, ADODB,

  u_const_db,

  u_func,
//  u_types,
  u_DB,
  u_db_manager,  

  dm_Main_SQL,

  TB2Item, TB2Dock, TB2Toolbar, cxPC
  
  ;

type
  Tfrm_Tables11 = class(TForm)
    pn_Top: TPanel;
    Panel1: TPanel;
    ActionList1: TActionList;
    FormStorage1: TFormStorage;
    mem_tbl: TdxMemData;
    ds_tbl: TDataSource;
    ADOQuery: TADOQuery;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBItem1: TTBItem;
    ADOQuery1: TADOQuery;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    tree_tbl: TdxDBTreeList;
    col_id: TdxDBTreeListColumn;
    col_name: TdxDBTreeListColumn;
    col_tree_id: TdxDBTreeListColumn;
    col_parent_tree_id: TdxDBTreeListColumn;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    tree_view: TdxDBTreeList;
    dxDBTreeListColumn1: TdxDBTreeListColumn;
    dxDBTreeListColumn2: TdxDBTreeListColumn;
    dxDBTreeListColumn3: TdxDBTreeListColumn;
    dxDBTreeListColumn4: TdxDBTreeListColumn;
    tree_sp: TdxDBTreeList;
    dxDBTreeListColumn5: TdxDBTreeListColumn;
    dxDBTreeListColumn6: TdxDBTreeListColumn;
    dxDBTreeListColumn7: TdxDBTreeListColumn;
    dxDBTreeListColumn8: TdxDBTreeListColumn;
    mem_sp: TdxMemData;
    ds_sp: TDataSource;
    mem_view: TdxMemData;
    ds_view: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure Save(Sender: TObject);
  private
//    FTableName: string;

  //  FProcess

  public
    procedure Open;

    class procedure ExecDlg();
  end;

var
  frm_Tables11: Tfrm_Tables11;

//=================================================================
implementation {$R *.dfm}
//=================================================================

const
  FLD_TYPE_ = 'TYPE_';

  FLD_TREE_ID         ='TREE_ID';
  FLD_TREE_PARENT_ID  ='TREE_PARENT_ID';



//--------------------------------------------------------------------
class procedure Tfrm_Tables11.ExecDlg;
//--------------------------------------------------------------------
begin
(*
  with Tfrm_Tables1.Create(Application) do begin
    Open;

    ShowModal;

    Free;
  end;*)

end;



//--------------------------------------------------------------------
procedure Tfrm_Tables11.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  pn_Top.Align:=alClient;
//  tree.Align:=alClient;

  db_CreateField(mem_tbl, [db_Field(FLD_ID                  , ftInteger),
                           db_Field(FLD_TREE_ID             , ftInteger),
                           db_Field(FLD_TREE_PARENT_ID      , ftInteger),
                           db_Field(FLD_NAME                , ftString, 250) ]);

  db_CreateField(mem_View,[db_Field(FLD_ID                  , ftInteger),
                           db_Field(FLD_TREE_ID             , ftInteger),
                           db_Field(FLD_TREE_PARENT_ID      , ftInteger),
                           db_Field(FLD_NAME                , ftString, 250) ]);

  db_CreateField(mem_sp , [db_Field(FLD_ID                  , ftInteger),
                           db_Field(FLD_TREE_ID             , ftInteger),
                           db_Field(FLD_TREE_PARENT_ID      , ftInteger),
                           db_Field(FLD_NAME                , ftString, 250) ]);

//  FTableName:= '_tables';


  db_SetComponentADOConnection(Self, dmMain_SQL.ADOConnection1);

  //ADOQuery.Connection:= dmMain.AdoConnection;
//  ADOQuery1.Connection:= dmMain.AdoConnection;

  tree_tbl.Align:= alClient;
  tree_view.Align:= alClient;
  tree_sp.Align:= alClient;
end;


//--------------------------------------------------------------------
procedure Tfrm_Tables11.Open;
//--------------------------------------------------------------------
const

  SQL_SELECT_TBL =
    'SELECT                                        '+
    '   o.id,                                      '+
    '   o.name,                                    '+
    '   t.type_                                    '+
    ' FROM sysobjects o LEFT OUTER JOIN            '+
    '      (SELECT * FROM _tables WHERE obj_type=1)'+
    '      t on o.id=t.id                          '+
    ' WHERE (xtype = ''U'') AND                    '+
    '       (o.name <> ''dtproperties'') AND       '+
    '       (o.name not like ''[_][_]%'')          '+
    ' ORDER BY o.name                              ';

  SQL_SEL_VIEW =
    'SELECT  TOP 100 PERCENT o.id, o.name, t.type_                  '+
    'FROM   sysobjects o LEFT OUTER JOIN                            '+
    '       (SELECT * FROM _tables WHERE obj_type=2) t on o.id=t.id '+
    'WHERE  (o.xtype = ''V'') AND                                   '+
    '       (o.name NOT LIKE ''[_][_]%'') AND                       '+
    '       (o.name <> ''sysconstraints'')   AND                    '+
    '       (o.name <> ''syssegments''	)                           '+
    'ORDER BY o.name                                                ';

  SQL_SEL_SP =
    'SELECT  TOP 100 PERCENT o.id, o.name, t.type_                  '+
    'FROM    sysobjects o LEFT OUTER JOIN                           '+
    '       (SELECT * FROM _tables WHERE obj_type=3) t on o.id=t.id '+
    'WHERE  (o.xtype in (''P'', ''TF'', ''IF'', ''FN'')) AND        '+
    '       (o.name NOT LIKE N''dt_%'') AND                         '+
    '       (o.name NOT LIKE ''[_][_]%'')                           '+
    'ORDER BY o.name                                                ';


  procedure DoAddRecords(aMem: TDataset; aSql: string);
  begin
    db_Clear(aMem);

    db_AddRecord(aMem, [db_Par(FLD_ID            , -1    ),
                        db_Par(FLD_TREE_ID       , 0     ),
                        db_Par(FLD_TREE_PARENT_ID, -1    ),
                        db_Par(FLD_NAME          , '��� �� ��������')
                       ]);
    db_AddRecord(aMem, [db_Par(FLD_ID            , -1    ),
                        db_Par(FLD_TREE_ID       , 1     ),
                        db_Par(FLD_TREE_PARENT_ID, -1    ),
                        db_Par(FLD_NAME          , 'Link')
                       ]);
    db_AddRecord(aMem, [db_Par(FLD_ID            , -1    ),
                        db_Par(FLD_TREE_ID       , 2     ),
                        db_Par(FLD_TREE_PARENT_ID, -1    ),
                        db_Par(FLD_NAME          , 'RFP 2G')
                       ]);
    db_AddRecord(aMem, [db_Par(FLD_ID            , -1    ),
                        db_Par(FLD_TREE_ID       , 3     ),
                        db_Par(FLD_TREE_PARENT_ID, -1    ),
                        db_Par(FLD_NAME          , 'RFP 3G')
                       ]);

    db_OpenQuery(ADOQuery, aSql);


    with ADOQuery do begin
      First;
      while not EOF do
      begin
        db_AddRecord(aMem,
          [db_Par(FLD_ID            , FieldByName(FLD_ID).AsInteger       ),
           db_Par(FLD_TREE_ID       , FieldByName(FLD_ID).AsInteger       ),
           db_Par(FLD_TREE_PARENT_ID, FieldByName(FLD_TYPE_).AsInteger    ),
           db_Par(FLD_NAME          , FieldByName(FLD_NAME).AsString      )
          ]);
        Next;
      end;
    end;

  end;

begin
 // F

  mem_tbl.DisableControls;
  mem_view.DisableControls;
  mem_sp.DisableControls;

  DoAddRecords(mem_tbl, SQL_SELECT_TBL);
  DoAddRecords(mem_view, SQL_SEL_VIEW);
  DoAddRecords(mem_sp, SQL_SEL_SP);


  mem_tbl.EnableControls;
  mem_view.EnableControls;
  mem_sp.EnableControls;

  tree_tbl.FullExpand;
  tree_view.FullExpand;
  tree_sp.FullExpand;

end;

//--------------------------------------------------------------------
procedure Tfrm_Tables11.Save(Sender: TObject);
//--------------------------------------------------------------------

  procedure DoSave(aObjType: Integer; aMem: TDataSet);
  const
    SQL_ADD =
      'INSERT INTO _tables (id, name, type_, obj_type) VALUES (%d, ''%s'', %d, %d)';
  var
    iType, iID: Integer;
    sSql: string;
  begin
    with aMem do begin
      DisableControls;
      First;
      while not EOF do
      begin
        iType:= FieldByName(FLD_TREE_PARENT_ID).AsInteger;
        iID:= FieldByName(FLD_ID).AsInteger;

        if Eq('VIEW_LinkEndType', FieldByName(FLD_NAME).AsString) then
          iID:= iID;

        if iID>0 then begin
          sSql:= Format(SQL_ADD, [iID, FieldByName(FLD_NAME).AsString, iType, aObjType]);

          gl_DB.ExecSQLBuf(sSql);
        end;

        Next;
      end;
      First;
      EnableControls;
    end;
  end;


var
  iRecNo1, iRecNo2, iRecNo3: Integer;
begin
  iRecNo1:= tree_tbl.TopIndex;
  iRecNo2:= tree_view.TopIndex;
  iRecNo3:= tree_sp.TopIndex;

  gl_DB.ExecCommandSimple('DELETE FROM _tables');

  DoSave(1, mem_Tbl);
  DoSave(2, mem_view);
  DoSave(3, mem_sp);

  gl_DB.ExecSQLBuf;

  tree_tbl.TopIndex:= iRecNo1;
  tree_view.TopIndex:= iRecNo2;
  tree_sp.TopIndex:= iRecNo3;
end;



end.
