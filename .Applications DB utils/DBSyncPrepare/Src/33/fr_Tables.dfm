object frm_Tables11: Tfrm_Tables11
  Left = 692
  Top = 207
  Width = 458
  Height = 512
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pn_Top: TPanel
    Left = 0
    Top = 65
    Width = 450
    Height = 320
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object TBDock1: TTBDock
      Left = 0
      Top = 0
      Width = 450
      Height = 25
      BoundLines = [blTop, blBottom, blLeft, blRight]
      FixAlign = True
      object TBToolbar1: TTBToolbar
        Left = 0
        Top = 0
        Caption = 'TBToolbar1'
        DockMode = dmCannotFloat
        DockPos = -30
        Options = [tboShowHint]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        object TBItem1: TTBItem
          Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
          OnClick = Save
        end
      end
    end
    object cxPageControl1: TcxPageControl
      Left = 0
      Top = 25
      Width = 450
      Height = 295
      ActivePage = cxTabSheet1
      Align = alClient
      TabOrder = 1
      ClientRectBottom = 295
      ClientRectRight = 450
      ClientRectTop = 23
      object cxTabSheet1: TcxTabSheet
        Caption = #1058#1072#1073#1083#1080#1094#1099
        ImageIndex = 0
        object tree_tbl: TdxDBTreeList
          Left = 0
          Top = 0
          Width = 353
          Height = 272
          Bands = <
            item
            end>
          DefaultLayout = False
          HeaderPanelRowCount = 1
          KeyField = 'tree_id'
          ParentField = 'tree_parent_id'
          Align = alLeft
          DragMode = dmAutomatic
          TabOrder = 0
          DataSource = ds_tbl
          LookAndFeel = lfFlat
          OptionsBehavior = [etoAutoDragDrop, etoAutoDragDropCopy, etoAutoSort, etoDragExpand, etoDragScroll, etoEditing, etoEnterShowEditor, etoImmediateEditor, etoMultiSelect, etoTabThrough]
          OptionsView = [etoAutoWidth, etoBandHeaderWidth, etoRowSelect, etoUseBitmap, etoUseImageIndexForSelected]
          ShowHeader = False
          TreeLineColor = clGrayText
          object col_id: TdxDBTreeListColumn
            Caption = 'ID '#1090#1072#1073#1083#1080#1094#1099
            Visible = False
            Width = 87
            BandIndex = 0
            RowIndex = 0
            FieldName = 'id'
          end
          object col_name: TdxDBTreeListColumn
            Caption = #1054#1073#1098#1077#1082#1090#1099
            DisableEditor = True
            Width = 250
            BandIndex = 0
            RowIndex = 0
            FieldName = 'name'
          end
          object col_tree_id: TdxDBTreeListColumn
            Visible = False
            BandIndex = 0
            RowIndex = 0
            FieldName = 'tree_id'
          end
          object col_parent_tree_id: TdxDBTreeListColumn
            Visible = False
            BandIndex = 0
            RowIndex = 0
            FieldName = 'TREE_PARENT_ID'
          end
        end
      end
      object cxTabSheet2: TcxTabSheet
        Caption = #1047#1072#1087#1088#1086#1089#1099
        ImageIndex = 1
        object tree_view: TdxDBTreeList
          Left = 0
          Top = 0
          Width = 353
          Height = 272
          Bands = <
            item
            end>
          DefaultLayout = False
          HeaderPanelRowCount = 1
          KeyField = 'tree_id'
          ParentField = 'tree_parent_id'
          Align = alLeft
          DragMode = dmAutomatic
          TabOrder = 0
          DataSource = ds_view
          LookAndFeel = lfFlat
          OptionsBehavior = [etoAutoDragDrop, etoAutoDragDropCopy, etoAutoSort, etoDragExpand, etoDragScroll, etoEditing, etoEnterShowEditor, etoImmediateEditor, etoMultiSelect, etoTabThrough]
          OptionsView = [etoAutoWidth, etoBandHeaderWidth, etoRowSelect, etoUseBitmap, etoUseImageIndexForSelected]
          ShowHeader = False
          TreeLineColor = clGrayText
          object dxDBTreeListColumn1: TdxDBTreeListColumn
            Caption = 'ID '#1090#1072#1073#1083#1080#1094#1099
            Visible = False
            Width = 87
            BandIndex = 0
            RowIndex = 0
            FieldName = 'id'
          end
          object dxDBTreeListColumn2: TdxDBTreeListColumn
            Caption = #1054#1073#1098#1077#1082#1090#1099
            DisableEditor = True
            Width = 250
            BandIndex = 0
            RowIndex = 0
            FieldName = 'name'
          end
          object dxDBTreeListColumn3: TdxDBTreeListColumn
            Visible = False
            BandIndex = 0
            RowIndex = 0
            FieldName = 'tree_id'
          end
          object dxDBTreeListColumn4: TdxDBTreeListColumn
            Visible = False
            BandIndex = 0
            RowIndex = 0
            FieldName = 'TREE_PARENT_ID'
          end
        end
      end
      object cxTabSheet3: TcxTabSheet
        Caption = #1061#1088#1072#1085#1080#1084#1099#1077' '#1087#1088#1086#1094#1077#1076#1091#1088#1099' '#1080' '#1092#1091#1085#1082#1094#1080#1080
        ImageIndex = 2
        object tree_sp: TdxDBTreeList
          Left = 0
          Top = 0
          Width = 353
          Height = 272
          Bands = <
            item
            end>
          DefaultLayout = False
          HeaderPanelRowCount = 1
          KeyField = 'tree_id'
          ParentField = 'tree_parent_id'
          Align = alLeft
          DragMode = dmAutomatic
          TabOrder = 0
          DataSource = ds_sp
          LookAndFeel = lfFlat
          OptionsBehavior = [etoAutoDragDrop, etoAutoDragDropCopy, etoAutoSort, etoDragExpand, etoDragScroll, etoEditing, etoEnterShowEditor, etoImmediateEditor, etoMultiSelect, etoTabThrough]
          OptionsView = [etoAutoWidth, etoBandHeaderWidth, etoRowSelect, etoUseBitmap, etoUseImageIndexForSelected]
          ShowHeader = False
          TreeLineColor = clGrayText
          object dxDBTreeListColumn5: TdxDBTreeListColumn
            Caption = 'ID '#1090#1072#1073#1083#1080#1094#1099
            Visible = False
            Width = 87
            BandIndex = 0
            RowIndex = 0
            FieldName = 'id'
          end
          object dxDBTreeListColumn6: TdxDBTreeListColumn
            Caption = #1054#1073#1098#1077#1082#1090#1099
            DisableEditor = True
            Width = 250
            BandIndex = 0
            RowIndex = 0
            FieldName = 'name'
          end
          object dxDBTreeListColumn7: TdxDBTreeListColumn
            Visible = False
            BandIndex = 0
            RowIndex = 0
            FieldName = 'tree_id'
          end
          object dxDBTreeListColumn8: TdxDBTreeListColumn
            Visible = False
            BandIndex = 0
            RowIndex = 0
            FieldName = 'TREE_PARENT_ID'
          end
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 450
    Height = 65
    Align = alTop
    BevelOuter = bvLowered
    Color = clAppWorkSpace
    TabOrder = 1
    Visible = False
  end
  object ActionList1: TActionList
    Left = 80
    Top = 8
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 124
    Top = 8
  end
  object mem_tbl: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 64
    Top = 416
  end
  object ds_tbl: TDataSource
    DataSet = mem_tbl
    Left = 24
    Top = 416
  end
  object ADOQuery: TADOQuery
    Parameters = <>
    Left = 208
    Top = 8
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    Left = 272
    Top = 16
  end
  object mem_sp: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 320
    Top = 416
  end
  object ds_sp: TDataSource
    DataSet = mem_sp
    Left = 280
    Top = 416
  end
  object mem_view: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 200
    Top = 416
  end
  object ds_view: TDataSource
    DataSet = mem_view
    Left = 152
    Top = 416
  end
end
