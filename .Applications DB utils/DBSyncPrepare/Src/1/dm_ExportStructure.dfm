object dmExportStructure_old: TdmExportStructure_old
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 639
  Top = 296
  Height = 487
  Width = 583
  object qry_Temp: TADOQuery
    Parameters = <>
    Left = 48
    Top = 72
  end
  object proc_Temp: TADOStoredProc
    Parameters = <>
    Left = 128
    Top = 88
  end
  object dset_Temp: TADODataSet
    Parameters = <>
    Left = 216
    Top = 64
  end
  object qry_Temp2: TADOQuery
    Parameters = <>
    Left = 40
    Top = 136
  end
  object qry_Temp3: TADOQuery
    Parameters = <>
    Left = 136
    Top = 160
  end
  object qry_Tables: TADOQuery
    Parameters = <>
    Left = 104
    Top = 24
  end
  object qry_PKeys: TADOQuery
    Parameters = <>
    Left = 240
    Top = 136
  end
  object qry_Views: TADOQuery
    Parameters = <>
    Left = 60
    Top = 208
  end
  object qry_TableFields: TADOQuery
    Parameters = <>
    Left = 60
    Top = 264
  end
  object qry_TableDefaults: TADOQuery
    Parameters = <>
    Left = 60
    Top = 320
  end
  object mem_Temp: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 240
    Top = 224
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    Left = 424
    Top = 136
  end
  object qry_SQLSERVER_TABLES: TADOQuery
    Parameters = <>
    SQL.Strings = (
      'SELECT                                  '
      '       id AS table_id,                        '
      '       name AS tablename            '
      '         '
      '     FROM sysobjects                          '
      '     WHERE (xtype = '#39#39'U'#39#39') AND              '
      '           (name <> '#39#39'dtproperties'#39#39') AND    '
      '           (name <> '#39#39'_tables'#39#39') AND       '
      '           (name not like '#39#39'[_][_]%'#39#39')     '
      '     ORDER BY name                       ')
    Left = 412
    Top = 256
  end
  object qry_SQLSERVER_PK: TADOQuery
    Parameters = <>
    SQL.Strings = (
      'SELECT                                  '
      '       id AS table_id,                        '
      '       name AS tablename            '
      '         '
      '     FROM sysobjects                          '
      '     WHERE (xtype = '#39#39'U'#39#39') AND              '
      '           (name <> '#39#39'dtproperties'#39#39') AND    '
      '           (name <> '#39#39'_tables'#39#39') AND       '
      '           (name not like '#39#39'[_][_]%'#39#39')     '
      '     ORDER BY name                       ')
    Left = 412
    Top = 328
  end
end
