unit dm_ExportStructure;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, AdoDb, dxmdaset, variants,

  dm_Main_SQL,

//  dm_Main,


  u_func,
  u_db,

  u_local_const,
  u_const_db
  ;

type
  TOnProgressEvent = procedure(aProgress,aMax: integer; var aTerminated: boolean) of object;


type
  TdmExportStructure_old = class(TDataModule)
    qry_Temp: TADOQuery;
    proc_Temp: TADOStoredProc;
    dset_Temp: TADODataSet;
    qry_Temp2: TADOQuery;
    qry_Temp3: TADOQuery;
    qry_Tables: TADOQuery;
    qry_PKeys: TADOQuery;
    qry_Views: TADOQuery;
    qry_TableFields: TADOQuery;
    qry_TableDefaults: TADOQuery;
    mem_Temp: TdxMemData;
    ADOQuery1: TADOQuery;
    qry_SQLSERVER_TABLES: TADOQuery;
    qry_SQLSERVER_PK: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    FOnProgress: TOnProgressEvent;

    procedure DoProgress(aPosition,aTotal: integer);

  public
    procedure ExportNeededTables(aResultDataset: TDataSet; aNameFieldName, aTableIDFieldName:
        string; aUpdateRRL, aUpdate2G, aUpdate3G, aUpdateOthers: boolean);


    procedure ExportTables(aResultDataset: TDataSet; aNameFieldName, aTableIDFieldName:
        string);

    procedure ExportTableFields(aResultDataset: TDataSet);
    //; aConnection: TADoConnection = nil


    procedure ExportCKToDataSet(aResultDataset: TDataSet);

    procedure ExportStoredProcsToDataSet(aResultDataset: TDataSet; aNameFieldName,
        aSqlFieldName: string);

    procedure ExportViewsToDataSet(aResultDataset: TDataSet; aNameFieldName, aSqlFieldName:
        string);

    procedure ExportTriggersToDataSet(aResultDataset: TDataSet; aTableIDFieldName,
        aNameFieldName, aSqlFieldName: string);

    procedure ExportIndexesToDataSet(aResultDataset: TDataSet; aTableIDFieldName,
        aNameFieldName, aTableNameFieldName, aSqlFieldName: string);

    procedure ExportFKToDataSet(aResultDataset: TDataSet; aNameFieldName,
        aTableNameFieldName, aSqlFieldName: string);

    property OnProgress:  TOnProgressEvent read FOnProgress write FOnProgress;

  end;

var
  FdmExportStructure: TdmExportStructure_old;


function dmExportStructure_old: TdmExportStructure_old;

//--------------------------------------------------------------------
implementation {$R *.DFM}
//--------------------------------------------------------------------

const
  SQL_PREPARE_PKEYS =
    'SELECT                                                                          '+
    '   TABLENAME = convert(sysname,o.name),                                         '+
    '   COLUMN_NAME = convert(sysname,c.name),                                       '+
    '   PK_NAME = convert(sysname,i.name),                                           '+
    '   KEY_SEQ =                                                                    '+
    '     CASE                                                                       '+
    '        when c.name = index_col(o.name, i.indid,  1) then convert (smallint,1)  '+
    '        when c.name = index_col(o.name, i.indid,  2) then convert (smallint,2)  '+
    '        when c.name = index_col(o.name, i.indid,  3) then convert (smallint,3)  '+
    '        when c.name = index_col(o.name, i.indid,  4) then convert (smallint,4)  '+
    '        when c.name = index_col(o.name, i.indid,  5) then convert (smallint,5)  '+
    '        when c.name = index_col(o.name, i.indid,  6) then convert (smallint,6)  '+
    '        when c.name = index_col(o.name, i.indid,  7) then convert (smallint,7)  '+
    '        when c.name = index_col(o.name, i.indid,  8) then convert (smallint,8)  '+
    '        when c.name = index_col(o.name, i.indid,  9) then convert (smallint,9)  '+
    '        when c.name = index_col(o.name, i.indid, 10) then convert (smallint,10) '+
    '        when c.name = index_col(o.name, i.indid, 11) then convert (smallint,11) '+
    '        when c.name = index_col(o.name, i.indid, 12) then convert (smallint,12) '+
    '        when c.name = index_col(o.name, i.indid, 13) then convert (smallint,13) '+
    '        when c.name = index_col(o.name, i.indid, 14) then convert (smallint,14) '+
    '        when c.name = index_col(o.name, i.indid, 15) then convert (smallint,15) '+
    '        when c.name = index_col(o.name, i.indid, 16) then convert (smallint,16) '+
    '     END                                                                        '+
    ' :into                                                                          '+
    ' FROM                                                                           '+
    '   sysindexes i, syscolumns c, sysobjects o                                     '+
    ' WHERE                                                                          '+
    '   (o.id = c.id)                      and                                       '+
    '   (o.id = i.id)                      and                                       '+
    '   (o.type = ''U'')                   and                                       '+
    '   (o.name not like ''[_][_]%'')      and                                       '+
    '   (o.name <> ''dtproperties'')       and                                       '+
    '   (i.status & 0x800) = 0x800         and                                       '+
    '      (c.name = index_col (o.name, i.indid,  1) or                              '+
    '       c.name = index_col (o.name, i.indid,  2) or                              '+
    '       c.name = index_col (o.name, i.indid,  3) or                              '+
    '       c.name = index_col (o.name, i.indid,  4) or                              '+
    '       c.name = index_col (o.name, i.indid,  5) or                              '+
    '       c.name = index_col (o.name, i.indid,  6) or                              '+
    '       c.name = index_col (o.name, i.indid,  7) or                              '+
    '       c.name = index_col (o.name, i.indid,  8) or                              '+
    '       c.name = index_col (o.name, i.indid,  9) or                              '+
    '       c.name = index_col (o.name, i.indid, 10) or                              '+
    '       c.name = index_col (o.name, i.indid, 11) or                              '+
    '       c.name = index_col (o.name, i.indid, 12) or                              '+
    '       c.name = index_col (o.name, i.indid, 13) or                              '+
    '       c.name = index_col (o.name, i.indid, 14) or                              '+
    '       c.name = index_col (o.name, i.indid, 15) or                              '+
    '       c.name = index_col (o.name, i.indid, 16)     )                           '+
    ' ORDER BY 1, 3                                                                  ';



function dmExportStructure_old: TdmExportStructure_old;
begin
  if not Assigned(FdmExportStructure) then
    FdmExportStructure:= TdmExportStructure_old.Create(Application);

  Result:= FdmExportStructure;
end;

procedure TdmExportStructure_old.DataModuleCreate(Sender: TObject);
begin
  Assert(Assigned(dmMain_SQL), 'Value not assigned');

  db_SetComponentADOConnection(Self, dmMain_SQL.ADOConnection1);
end;

procedure TdmExportStructure_old.DoProgress(aPosition,aTotal: integer);
begin

end;

//----------------------------------------------------------
procedure TdmExportStructure_old.ExportTables(aResultDataset: TDataSet; aNameFieldName,
    aTableIDFieldName: string);
//----------------------------------------------------------
const
  SQL_SELECT_SQLSERVER_TABLES =
    'SELECT                                     '+
    '   id AS table_id,                         '+
    '   name AS tablename                       '+
    ' FROM sysobjects                           '+
    ' WHERE (xtype = ''U'') AND                 '+
    '       (name <> ''dtproperties'') AND      '+
    '       (name <> ''_tables'') AND           '+
    '       (name not like ''[_][_]%'')         '+
    ' ORDER BY name                             ';
begin
  db_OpenQuery(qry_Temp,  SQL_SELECT_SQLSERVER_TABLES);

  with qry_Temp do
    while not EOF do
  begin
//    FLD_NAME, FLD_TABLE_ID);

    db_AddRecord (aResultDataset,
      [db_Par(aNameFieldName,      FieldValues[FLD_TABLENAME]),
       db_Par(aTableIDFieldName,   FieldValues[FLD_TABLE_ID])    ]);

    Next;
  end;
end;

//----------------------------------------------------------
procedure TdmExportStructure_old.ExportNeededTables(aResultDataset: TDataSet; aNameFieldName,
    aTableIDFieldName: string; aUpdateRRL, aUpdate2G, aUpdate3G, aUpdateOthers: boolean);
//----------------------------------------------------------

const
  SQL_SEL = 'SELECT * FROM _tables WHERE (type_ in (%s)) ';

var
  oIntArray: TIntArray;
  sSql: string;
begin

  SetLength(oIntArray, 0);

  if aUpdateOthers then
    IntArrayAddValue(oIntArray, 0);
  if aUpdateRRL then
    IntArrayAddValue(oIntArray, 1);
  if aUpdate2G then
    IntArrayAddValue(oIntArray, 2);
  if aUpdate3G then
    IntArrayAddValue(oIntArray, 3);

  sSql:= Format(SQL_SEL, [IntArrayToStringSimple(oIntArray, ',')]);
  if aUpdateOthers then
    sSql:= sSql + 'or (type_ is null)';

  db_OpenQuery(qry_Temp, sSql);

  with qry_Temp do
    while not EOF do
  begin
    db_AddRecord (aResultDataset,
      [db_Par(aNameFieldName   ,  FieldValues[FLD_NAME]),
       db_Par(aTableIDFieldName,  FieldValues[FLD_ID])  ,
       db_Par('type_'          ,  FieldValues['obj_type'])
      ]);

    Next;
  end;

end;

//----------------------------------------------------------
procedure TdmExportStructure_old.ExportTableFields(aResultDataset: TDataSet);
//----------------------------------------------------------
const
  SQL_ALL =
    ' SELECT  TOP 100 PERCENT o.id AS table_id, o.name AS tablename,                  '+
    '    c.colid, c.name, t.name AS type, c.length AS size,                           '+
    '    t.variable AS Variable, t.usertype AS Usertype, 0 AS PK,                     '+
    '    0 AS default_is_bound,                                                       '+
    '    CAST(''_'' AS nvarchar(4000)) AS DEFAULT_CONTENT,                            '+
    '    CAST(''_'' AS nvarchar(500))  AS DEFAULT_NAME,                               '+
    '    ColumnProperty(o.id, c.name, ''IsIdentity'') AS IsIdentity,                  '+
    '    ColumnProperty(o.id, c.name, ''IsRowGuidCol'') AS IsRowGuidCol,              '+
    '    IDENT_INCR (o.name) AS ident_incr,                                           '+
    '    IDENT_SEED (o.name) AS ident_seed,                                           '+
    '    c.isnullable AS NULLS                                                        '+
    ' INTO ##TEMP                                                                     '+
    ' FROM  sysobjects o INNER JOIN                                                   '+
    '       syscolumns c ON o.id = c.id INNER JOIN                                    '+
    '       systypes t ON c.xusertype = t.xusertype                                   '+
    ' WHERE (o.xtype = ''U'') AND                                                     '+
    '       (o.name <> ''dtproperties'') AND                                          '+
    '       (o.name <> ''_tables'') AND                                               '+
    '       (o.name NOT LIKE ''[_][_]%'') AND                                         '+
    '       (c.name NOT LIKE ''[_][_]%'')                                             '+
    ' ORDER BY o.name, c.colid                                                        '+
      #13#10 +

    ' SELECT                                                                          '+
    '   TABLENAME = convert(sysname,o.name),                                          '+
    '   COLUMN_NAME = convert(sysname,c.name),                                        '+
    '   PK_NAME = convert(sysname,i.name),                                            '+
    '   KEY_SEQ =                                                                     '+
    '     CASE                                                                        '+
    '        when c.name = index_col(o.name, i.indid,  1) then convert (smallint,1)   '+
    '        when c.name = index_col(o.name, i.indid,  2) then convert (smallint,2)   '+
    '        when c.name = index_col(o.name, i.indid,  3) then convert (smallint,3)   '+
    '        when c.name = index_col(o.name, i.indid,  4) then convert (smallint,4)   '+
    '        when c.name = index_col(o.name, i.indid,  5) then convert (smallint,5)   '+
    '        when c.name = index_col(o.name, i.indid,  6) then convert (smallint,6)   '+
    '        when c.name = index_col(o.name, i.indid,  7) then convert (smallint,7)   '+
    '        when c.name = index_col(o.name, i.indid,  8) then convert (smallint,8)   '+
    '        when c.name = index_col(o.name, i.indid,  9) then convert (smallint,9)   '+
    '        when c.name = index_col(o.name, i.indid, 10) then convert (smallint,10)  '+
    '        when c.name = index_col(o.name, i.indid, 11) then convert (smallint,11)  '+
    '        when c.name = index_col(o.name, i.indid, 12) then convert (smallint,12)  '+
    '        when c.name = index_col(o.name, i.indid, 13) then convert (smallint,13)  '+
    '        when c.name = index_col(o.name, i.indid, 14) then convert (smallint,14)  '+
    '        when c.name = index_col(o.name, i.indid, 15) then convert (smallint,15)  '+
    '        when c.name = index_col(o.name, i.indid, 16) then convert (smallint,16)  '+
    '     END                                                                         '+
    ' INTO ##Temp_PK                                                                  '+
    ' FROM                                                                            '+
    '   sysindexes i, syscolumns c, sysobjects o                                      '+
    ' WHERE                                                                           '+
    '   (o.id = c.id)                      and                                        '+
    '   (o.id = i.id)                      and                                        '+
    '   (o.type = ''U'')                   and                                        '+
    '   (o.name not like ''[_][_]%'')      and                                        '+
    '   (o.name <> ''dtproperties'')       and                                        '+
    '   (i.status & 0x800) = 0x800         and                                        '+
    '      (c.name = index_col (o.name, i.indid,  1) or                               '+
    '       c.name = index_col (o.name, i.indid,  2) or                               '+
    '       c.name = index_col (o.name, i.indid,  3) or                               '+
    '       c.name = index_col (o.name, i.indid,  4) or                               '+
    '       c.name = index_col (o.name, i.indid,  5) or                               '+
    '       c.name = index_col (o.name, i.indid,  6) or                               '+
    '       c.name = index_col (o.name, i.indid,  7) or                               '+
    '       c.name = index_col (o.name, i.indid,  8) or                               '+
    '       c.name = index_col (o.name, i.indid,  9) or                               '+
    '       c.name = index_col (o.name, i.indid, 10) or                               '+
    '       c.name = index_col (o.name, i.indid, 11) or                               '+
    '       c.name = index_col (o.name, i.indid, 12) or                               '+
    '       c.name = index_col (o.name, i.indid, 13) or                               '+
    '       c.name = index_col (o.name, i.indid, 14) or                               '+
    '       c.name = index_col (o.name, i.indid, 15) or                               '+
    '       c.name = index_col (o.name, i.indid, 16)     )                            '+
    ' ORDER BY 1, 3                                                                   '+
      #13#10 +

    ' SELECT TOP 100 PERCENT                                                          '+
    '   bound = CASE o.parent_obj WHEN 0 THEN 1 ELSE 0 END,                           '+
    '   c.name AS COLUMN_NAME,                                                        '+
    '   c.id AS TABLE_ID,                                                             '+
    '   o.name AS DEFAULT_NAME,                                                       '+
    '   com.text AS DEFAULT_CONTENT                                                   '+
    ' INTO ##Temp_DF                                                                  '+
    ' FROM sysobjects o INNER JOIN                                                    '+
    '      syscolumns c ON o.id = c.cdefault INNER JOIN                               '+
    '      syscomments com ON o.id = com.id                                           '+
    ' WHERE o.xtype = ''D''                                                           '+
    ' ORDER BY table_id                                                               '+
      #13#10 +

    ' update ##temp SET PK=1                                                          '+
    ' WHERE EXISTS(SELECT * FROM ##Temp_PK                                            '+
    '              WHERE tablename=##temp.tablename and                               '+
    '                    column_name=##temp.name)                                     '+
      #13#10 +

    'UPDATE ##temp SET                                                                '+
    '  DEFAULT_NAME = '''',                                                           '+
    '  DEFAULT_CONTENT = ''''                                                         '+
      #13#10 +

    ' UPDATE ##temp SET                                                               '+
    '   DEFAULT_NAME = ISNULL(t.DEFAULT_NAME, ''''),                                  '+
    '   DEFAULT_CONTENT = ISNULL(t.DEFAULT_CONTENT, ''''),                            '+
    '   default_is_bound = t.bound                                                    '+
    ' FROM ##Temp_DF t                                                                '+
    ' WHERE t.table_id=##temp.table_id AND t.column_name=##temp.name                  '+
      #13#10 +

    ' UPDATE ##temp                                                                   '+
    ' SET Variable=1 WHERE type in (''char'', ''nchar'', ''binary'')                  '+
      #13#10 +

    'UPDATE ##temp                                                                    '+
    ' SET Variable=0 WHERE type=''sysname''                                           '+
      #13#10 +

    ' UPDATE ##temp SET size=size/2                                                   '+
    ' WHERE type in (''nchar'', ''nvarchar'', ''ntext'')                              '+
      #13#10 ;


  SQL_DROP_TEMP =
    ' If Object_Id(''tempdb..##TEMP'') is Not Null Drop table ##TEMP                  '+
      #13#10 +
    ' If Object_Id(''tempdb..##Temp_PK'') is Not Null Drop table ##Temp_PK            '+
      #13#10 +
    ' If Object_Id(''tempdb..##Temp_DF'') is Not Null Drop table ##Temp_DF            '+
      #13#10 ;


var
  I: Integer;
  bFieldIsPrimaryKey: boolean;
  sTableName, sName, sSql: string;
  iIsBound, iPK, iIsIdentity: Integer;
  v: variant;
  label exit_label;
begin
//  gl_Db.ErrorMsg:= '';

  gl_Db.ExecCommandSimpleCRLF(SQL_DROP_TEMP, false);
  gl_Db.ExecCommandSimpleCRLF(SQL_ALL);

  if not db_OpenQuery(qry_TableFields, 'SELECT * FROM ##TEMP') then
    goto exit_label;

  gl_Db.ExecCommandSimpleCRLF(SQL_DROP_TEMP);

 // if Assigned (aConnection) then
//    aConnection.BeginTrans;

  try
    with qry_TableFields do
      while not EOF do
    begin
      iPK:=             FieldByName('PK').AsInteger;
      iIsIdentity:=     FieldByName('IsIdentity').AsInteger;
      iIsBound:=        FieldByName('default_is_bound').AsInteger;

      db_AddRecord (aResultDataset, //qry_TableFields,
       // [FLD_TABLE_ID, FLD_TABLENAME, FLD_NAME, FLD_TYPE, FLD_SIZE,
        // FLD_VARIABLE, FLD_USERTYPE, FLD_NULLS, FLD_IsRowGuidCol],

        [
         db_Par(FLD_TABLE_ID,       qry_TableFields[FLD_TABLE_ID]),
         db_Par(FLD_TABLENAME,      qry_TableFields[FLD_TABLENAME]),
         db_Par(FLD_NAME,           qry_TableFields[FLD_NAME]),
         db_Par(FLD_TYPE,           qry_TableFields[FLD_TYPE]),
         db_Par(FLD_SIZE,           qry_TableFields[FLD_SIZE]),
         db_Par(FLD_VARIABLE,       qry_TableFields[FLD_VARIABLE]),
         db_Par(FLD_USERTYPE,       qry_TableFields[FLD_USERTYPE]),
         db_Par(FLD_NULLS,          qry_TableFields[FLD_NULLS]),
         db_Par(FLD_IsRowGuidCol,   qry_TableFields[FLD_IsRowGuidCol]),


         db_Par(FLD_PK,               IIF(iPK=1, true, false)),
         db_Par(FLD_DEFAULT_NAME,     IIF_NULL(FieldByName(FLD_DEFAULT_NAME).AsString)),
         db_Par(FLD_DEFAULT_CONTENT,  IIF_NULL(Trim(FieldByName(FLD_DEFAULT_CONTENT).AsString))),
         db_Par(FLD_default_is_bound, IIF(iIsBound=1, true, false)),
         db_Par(FLD_IDENTITY,         IIF(iIsIdentity=1, true, false)),
         db_Par(FLD_IDENT_SEED,       IIF(iIsIdentity=1, FieldByName(FLD_IDENT_SEED).AsInteger, NULL)),
         db_Par(FLD_IDENT_INCR,       IIF(iIsIdentity=1, FieldByName(FLD_IDENT_INCR).AsInteger, NULL))
        ]);

      Next;
    end;

  except

(*    if Assigned (aConnection) then
    begin
      aConnection.RollbackTrans;
      exit;
    end;
*)
  end;

(*
  if Assigned (aConnection) then
    aConnection.CommitTrans;
*)

exit_label:

(*  if gl_Db.ErrorMsg<>'' then
    ShowMessage(gl_Db.ErrorMsg);
*)
end;


//--------------------------------------------------------------------
procedure TdmExportStructure_old.ExportCKToDataSet(aResultDataset: TDataSet);
//--------------------------------------------------------------------
const
  SQL_SELECT_CK =
    'SELECT TOP 100 PERCENT o.name,                              '+
    '        o.parent_obj AS TABLE_ID,                           '+
    '        Object_Name(o.parent_obj) AS tablename,             '+
    '        c.text AS CONTENT                                   '+
    'FROM    sysobjects o INNER JOIN                             '+
    '        syscomments c ON o.id = c.id                        '+
    'WHERE   (o.xtype = ''C'')                                   '+
    'ORDER BY o.name                                             ';

  SQL_CONTENT = 'ALTER TABLE [%s] ADD CONSTRAINT [%s] CHECK %s';

begin
  db_OpenQuery (qry_Temp, SQL_SELECT_CK);

  with qry_Temp do
    while not EOF do
    begin
      db_AddRecord (aResultDataset, //qry_Temp,
        //  [FLD_NAME, FLD_TABLE_ID, FLD_TABLENAME],

          [
          db_Par(FLD_NAME,      qry_Temp[FLD_NAME]),
          db_Par(FLD_TABLE_ID,  qry_Temp[FLD_TABLE_ID]),
          db_Par(FLD_TABLENAME, qry_Temp[FLD_TABLENAME]),

          db_Par(FLD_CONTENT, Format(SQL_CONTENT, [qry_Temp[FLD_TABLENAME], qry_Temp[FLD_NAME], qry_Temp[FLD_CONTENT]]) ) ]);

      Next;
    end;
end;

//--------------------------------------------------------------------
procedure TdmExportStructure_old.ExportStoredProcsToDataSet(aResultDataset: TDataSet;
    aNameFieldName, aSqlFieldName: string);
//--------------------------------------------------------------------
const
  SQL_SELECT_PROCEDURES =
    ' SELECT  TOP 100 PERCENT o.name, c.text, colid              '+
    ' FROM    sysobjects o INNER JOIN                            '+
    '         syscomments c ON o.id = c.id                       '+
    ' WHERE  (o.xtype in (''P'', ''TF'', ''IF'', ''FN'')) AND    '+
    '        (o.name NOT LIKE N''dt_%'') AND                     '+
    '        (o.name NOT LIKE ''[_][_]%'')                       ';
//    ' ORDER BY o.name DESC, colid                                ';
var
  sSql, sName: string;
begin
  db_OpenQuery (qry_Temp, SQL_SELECT_PROCEDURES);

  qry_Temp.Sort := 'name, colid';

  with qry_Temp do begin
   // First;
    while not EOF do begin
      sName:= FieldValues[FLD_NAME];

      if aResultDataset.Locate(FLD_NAME, FieldValues[FLD_NAME], [LoCaseInsensitive]) then
      begin
        sSql:= aResultDataset[aSqlFieldName]+FieldValues[FLD_TEXT];
        db_UpdateRecord (aResultDataset, [db_Par(aSqlFieldName, sSql)]);
      end
      else
        db_AddRecord (aResultDataset,
                    [db_Par(aNameFieldName, FieldValues[FLD_NAME]),
                     db_Par(aSqlFieldName,  FieldValues[FLD_TEXT] )    ]);
      Next;
    end;
  end;
end;


//--------------------------------------------------------------------
procedure TdmExportStructure_old.ExportViewsToDataSet(aResultDataset: TDataSet;
    aNameFieldName, aSqlFieldName: string);
//--------------------------------------------------------------------
const
  SQL_SELECT_REAL_VIEWS =
    ' SELECT  TOP 100 PERCENT o.name, c.text, colid              '+
    ' FROM   sysobjects o INNER JOIN                             '+
    '        syscomments c ON o.id = c.id                        '+
    ' WHERE  (o.xtype = ''V'') AND                               '+
    '        (o.name NOT LIKE ''[_][_]%'') AND                   '+
    '        (o.name <> ''sysconstraints'')   AND                '+
    '        (o.name <> ''syssegments''	)                 '+
    ' ORDER BY o.name, colid                                     ';
var
  sSql, sName: string;
begin
  db_OpenQuery (qry_Temp, SQL_SELECT_REAL_VIEWS);

  with qry_Temp do
    while not EOF do
    begin
      sName:= FieldValues[FLD_NAME];

      if aResultDataset.Locate(FLD_NAME, FieldValues[FLD_NAME], [LoCaseInsensitive]) then
      begin
        sSql:= aResultDataset[aSqlFieldName]+FieldValues[FLD_TEXT];
        db_UpdateRecord (aResultDataset, [db_Par(aSqlFieldName, sSql)]);
      end
      else
        db_AddRecord (aResultDataset,
                    [db_Par(aNameFieldName, FieldValues[FLD_NAME]),
                     db_Par(aSqlFieldName,  FieldValues[FLD_TEXT])    ]);
      Next;
    end;

end;

//--------------------------------------------------------------------
procedure TdmExportStructure_old.ExportTriggersToDataSet(aResultDataset: TDataSet;
    aTableIDFieldName, aNameFieldName, aSqlFieldName: string);
//--------------------------------------------------------------------
const
  SQL_SELECT_TRIGGERS =
    ' SELECT TOP 100 PERCENT * FROM                                           '+
    ' (SELECT TOP 100 PERCENT o.name, c.text, colid,                          '+
    '                         o.parent_obj AS table_id,                       '+
    '        (SELECT name FROM sysobjects WHERE id=o.parent_obj) AS tablename '+
    '  FROM   sysobjects o INNER JOIN                                         '+
    '        syscomments c ON o.id = c.id                                     '+
    '  WHERE  (o.xtype = ''TR'') AND (o.name not like ''[_][_]%'')            '+
    ' ) v1                                                                    '+
    ' ORDER BY name, colid                                                    ';
var
  sSql, sName: string;
begin
  db_OpenQuery (qry_Temp, SQL_SELECT_TRIGGERS);

  with qry_Temp do
//  begin
  //  First;
    while not EOF do
    begin
      sName:= FieldValues[FLD_NAME];
      sSql := FieldValues[FLD_TEXT];

      if aResultDataset.Locate(aNameFieldName, FieldValues[FLD_NAME], [LoCaseInsensitive]) then
      begin
        sSql:= aResultDataset[aSqlFieldName] + FieldValues[FLD_TEXT];
        db_UpdateRecord (aResultDataset, [db_Par(aSqlFieldName, sSql)]);
      end
      else
        db_AddRecord (aResultDataset,
                    [db_Par(aTableIDFieldName, FieldValues[FLD_TABLE_ID]),
                     db_Par(FLD_TABLENAME,     FieldValues[FLD_TABLENAME]),
                     db_Par(aNameFieldName,    FieldValues[FLD_NAME]),
                     db_Par(aSqlFieldName,     sSql )    ]);
      Next;
    end;
 // end;
end;





//--------------------------------------------------------------------
procedure TdmExportStructure_old.ExportIndexesToDataSet(aResultDataset: TDataSet;
    aTableIDFieldName, aNameFieldName, aTableNameFieldName, aSqlFieldName: string);
//--------------------------------------------------------------------
const
  SQL_SEL_OBJECTS_WITH_INDEXES =
    ' SELECT TOP 100 PERCENT so.name AS tablename, so.id AS table_id,                       '+
    '         si.name AS index_name, si.indid,                                              '+
    '   INDEXPROPERTY(OBJECT_ID(so.name), si.name, ''IndexFillFactor'') AS IndexFillFactor, '+
    '   INDEXPROPERTY(OBJECT_ID(so.name), si.name, ''IsPadIndex'') AS IsPadIndex            '+
    ' FROM  sysindexes si INNER JOIN sysobjects so ON si.id = so.id                         '+
    ' WHERE   (si.indid > 0) AND (si.indid < 255) AND ((si.status & 64)=0) AND              '+
    '         (so.type = ''U'') AND (so.name NOT LIKE ''[_][_]%'') AND                      '+
    '         (so.name NOT LIKE N''dt_%'')                                                  '+
    ' ORDER BY so.name                                                                      ';

  SQL_ALTER_PK =
    ' ALTER TABLE [%s] ADD PRIMARY KEY %s (%s) %s ON [PRIMARY] ';

  SQL_ALTER_UK =
    ' ALTER TABLE [%s] ADD UNIQUE %s (%s) %s ON [PRIMARY] ';

var
  sFillFactor, sIndexName, sTableName, sIndexKeys, sTemp, sValue, sSql: string;
  bZapyat, bClustered, bIndexIsPK, bIndexIsUniqueConstr: boolean;
  oStrArr: TStrArray;
  iFillFactor: Integer;

  function DoMakeIndexFields(aIndexKeys: string): string;
  var
    I: Integer;
    StrArr: TStrArray;
  begin
    StrArr:= StringToStrArray(aIndexKeys, ',');
    for I := 0 to High(StrArr) do
      StrArr[i]:= Format('[%s]', [Trim(StrArr[i])]);
    Result:= StrArrayToString(StrArr, ',');
  end;

begin
  db_OpenQuery (qry_Tables, SQL_SEL_OBJECTS_WITH_INDEXES);
  sSql:= ReplaceStr(SQL_PREPARE_PKEYS, ':into', ' ');
  db_OpenQuery (qry_PKeys,  sSql);

  sTableName:= '';

  qry_Tables.First;
  with qry_Tables do
    while not EOF do
  begin
    if sTableName <> qry_Tables.FieldByName(FLD_TABLENAME).AsString then
      if not db_OpenQuery(qry_Temp3, Format('exec sp_helpindex @objname=''%s''',
                        [FieldByName(FLD_TABLENAME).AsString])) then
      begin Next; continue; end;

    sTableName:= FieldByName(FLD_TABLENAME).AsString;

    if not qry_Temp3.Locate('index_name', FieldByName('index_name').AsString, []) then
      Raise Exception.Create('');

    sIndexName := qry_Temp3.FieldByName('index_name').AsString;
    sIndexKeys := qry_Temp3.FieldByName('index_keys').AsString;
    sIndexKeys := ReplaceStr(sIndexKeys, '(-)', ' DESC');
    sIndexKeys := DoMakeIndexFields(sIndexKeys);
    sTemp      := qry_Temp3.FieldByName('index_description').AsString;

    bIndexIsPK := qry_PKeys.Locate(FLD_TABLENAME+';'+'pk_name',
                  VarArrayOf([sTableName, sIndexName]),
                  [LoCaseInsensitive]);

    bIndexIsUniqueConstr:= (Pos('unique key', sTemp)>0);

    sValue:='CREATE ';

    //�������� ������� UNIQUE
    if Pos('unique,', sTemp)<>0 then
      sValue:= sValue+'UNIQUE ';

    if Pos('nonclustered', sTemp)<>0 then
      bClustered:= false
    else
    if Pos('clustered', sTemp)<>0 then
      bClustered:= true;

    sValue:= sValue + IIF(bClustered, ' CLUSTERED ', ' NONCLUSTERED ');

    //������� ���� �������, ������� � ���������� ����
    sValue:= sValue+Format('INDEX [%s] ON [%s](%s)', [sIndexName,sTableName,sIndexKeys]);


    SetLength(oStrArr, 0);

    //�������� ������� IGNORE_DUP_KEY
    if Pos('ignore duplicate keys',sTemp)<>0 then
      StrArrayAddValue(oStrArr, ' IGNORE_DUP_KEY ');

    //�������� ������� IndexFillFactor
    iFillFactor:= 0;
    sFillFactor:= '';
    if qry_Tables.FieldByName('IndexFillFactor').AsInteger<>0 then begin
      iFillFactor:= FieldByName('IndexFillFactor').AsInteger;
      sFillFactor:= ' FILLFACTOR = '+ AsString(iFillFactor);
      StrArrayAddValue(oStrArr, sFillFactor);
    end;
    
    //�������� ��������� ISPADINDEX
    if qry_Tables.FieldByName('IsPadIndex').AsInteger=1 then
      StrArrayAddValue(oStrArr, ' PAD_INDEX ');

    //�������� ������� STATISTICS_NORECOMPUTE
    if Pos('stats no recompute',sTemp)<>0 then
      StrArrayAddValue(oStrArr, ' STATISTICS_NORECOMPUTE ');

    if High(oStrArr)>=0 then
      sValue:= sValue + ' WITH ' + StrArrayToString(oStrArr, ',');

    //�������� ������� ON [PRIMARY]
    if Pos('on PRIMARY',sTemp)<>0 then
      sValue:= sValue + ' ON [PRIMARY]';

    if bIndexIsPK then
      sValue:= Format(SQL_ALTER_PK, [qry_Tables[FLD_TABLENAME],  // sIndexName,
                IIF(bClustered, 'CLUSTERED', 'NONCLUSTERED'), sIndexKeys,
                IIF(iFillFactor=0, '', ' WITH '+sFillFactor)]);

    if bIndexIsUniqueConstr then
      sValue:= Format(SQL_ALTER_UK, [qry_Tables[FLD_TABLENAME],
                IIF(bClustered, 'CLUSTERED', 'NONCLUSTERED'), sIndexKeys,
                IIF(iFillFactor=0, '', ' WITH '+sFillFactor)]);

    db_AddRecord (aResultDataset,
                    [db_Par(FLD_TABLE_ID,       qry_Tables[FLD_TABLE_ID]),
                     db_Par(FLD_NAME,           sIndexName),
                     db_Par(FLD_TABLENAME,      qry_Tables[FLD_TABLENAME]),
                     db_Par(FLD_IS_CLUSTERED,   bClustered),
                     db_Par(FLD_IS_PRIMARY_KEY, bIndexIsPK),
                     db_Par(FLD_IS_UNIQUE_KEY,  bIndexIsUniqueConstr),
                     db_Par(FLD_INDEX_KEYS,     sIndexKeys),
                     db_Par(FLD_FILL_FACTOR,    IIF_NULL(iFillFactor)),
                     db_Par(FLD_CONTENT,        sValue)       ]);
    Next;
  end;

end;



//--------------------------------------------------------------------
procedure TdmExportStructure_old.ExportFKToDataSet(aResultDataset: TDataSet; aNameFieldName,
    aTableNameFieldName, aSqlFieldName: string);
//--------------------------------------------------------------------
const
   SQL_SEL_TABLES =
    'SELECT TOP 100 PERCENT                                                '+
    '   KEYNO as KEYNO11,                                                  '+
    '   OBJECT_NAME(sf.rkeyid) AS PKTABLE_NAME,                            '+
    '   col_name(sf.rkeyid, rkey) AS pkcolumn_name,                        '+
    '   OBJECT_NAME(sf.fkeyid) AS fktable_name,                            '+
    '   col_name(sf.fkeyid, fkey) AS fkcolumn_name,                        '+
    '   ObjectProperty(constid, ''CnstIsUpdateCascade'') as Update_Rule,   '+
    '   ObjectProperty(constid, ''CnstIsDeleteCascade'') as Delete_Rule,   '+
    '   OBJECT_NAME(constid) AS fk_name,                                   '+
    '   ObjectProperty(constid, ''CnstIsNotRepl'') as Not_For_Replication  '+
    'FROM  sysforeignkeys sf INNER JOIN                                    '+
    '      sysobjects so ON sf.fkeyid = so.id                              '+
    'WHERE (so.xtype = ''U'') AND (so.name not like ''[_][_]%'')           '+
    'ORDER BY so.name                                                      ';

var
  sTableName, sSQLCode, sSql, pktable_name, pkcolumn_name,
          fktable_name, fkcolumn_name, fk_name:string;
  iKeyNo, iUpdate_Rule, iDelete_Rule, iNotForReplication: Integer;
begin
  db_OpenQuery (qry_Tables, SQL_SEL_TABLES);
  db_ClearFields(mem_Temp);

  db_CreateField(mem_Temp, [db_Field('KEYNO11', ftInteger),
                            db_Field('PKTABLE_NAME', ftString),
                            db_Field('pkcolumn_name', ftString),
                            db_Field('fktable_name', ftString),
                            db_Field('fkcolumn_name', ftString),
                            db_Field('Update_Rule', ftInteger),
                            db_Field('Delete_Rule', ftInteger),
                            db_Field('fk_name', ftString),
                            db_Field('Not_For_Replication', ftInteger)
                           ]);
  db_CopyRecords(qry_Tables, mem_Temp);

  with qry_Tables do
  begin
    First;

    while not EOF do
    begin
      iKeyNo:= FieldByName(FLD_KEYNO).AsInteger;
      if iKeyNo>1 then begin
        if mem_Temp.Locate(FLD_KEYNO+';'+FLD_FK_NAME,
                VarArrayOf([1, FieldByName(FLD_FK_NAME).AsString]), [loCaseInsensitive])
        then
          db_UpdateRecord(mem_Temp,
                [db_Par(FLD_pkcolumn_name, mem_Temp.FieldByName(FLD_pkcolumn_name).AsString+'],['+FieldByName(FLD_pkcolumn_name).AsString),
                 db_Par(FLD_fkcolumn_name, mem_Temp.FieldByName(FLD_fkcolumn_name).AsString+'],['+FieldByName(FLD_fkcolumn_name).AsString)
                ]);

        if mem_Temp.Locate(FLD_KEYNO+';'+FLD_FK_NAME,
                VarArrayOf([iKeyNo, FieldByName(FLD_FK_NAME).AsString]), [loCaseInsensitive])
        then
           mem_Temp.Delete;

      end;

      Next;
    end;
  end;



  mem_Temp.first;
  with mem_Temp do
    while not EOF do
  begin
    sTableName:= FieldByName('fktable_name').AsString;

    pktable_name  := FieldByName('PKTABLE_NAME').AsString;
    pkcolumn_name := FieldByName('pkcolumn_name').AsString;
    fktable_name  := sTableName;
    fkcolumn_name := FieldByName('fkcolumn_name').AsString;
    fk_name       := FieldByName('fk_name').AsString;
    iUpdate_Rule  := FieldByName('Update_Rule').AsInteger;
    iDelete_Rule  := FieldByName('Delete_Rule').AsInteger;
    iNotForReplication := FieldByName('Not_For_Replication').AsInteger;

    sSQLCode:= '';
    sSQLCode:= Format('ALTER TABLE [%s] ADD CONSTRAINT [%s] FOREIGN KEY ([%s]) REFERENCES [%s] ([%s])',
                      [fktable_name, fk_name, fkcolumn_name, pktable_name, pkcolumn_name]);
    if iUpdate_Rule=1 then sSQLCode:= sSQLCode+' ON UPDATE CASCADE';
    if iDelete_Rule=1 then sSQLCode:= sSQLCode+' ON DELETE CASCADE';

    // by default
    if iNotForReplication=1 then
      sSQLCode:= sSQLCode+' NOT FOR REPLICATION';

    sSql:= Format('select * from %s WHERE %s not in (select %s from %s)',
                [fktable_name, fkcolumn_name, pkcolumn_name, pktable_name]);

    db_AddRecord (aResultDataset,
                  [db_Par(aNameFieldName,       fk_name),
                   db_Par(aTableNameFieldName,  sTableName),
                   db_Par(FLD_PK_TABLENAME,     pktable_name),
                   db_Par(FLD_integrity_SQL,    sSql),
                   db_Par(aSqlFieldName,        sSQLCode)]);

    Next;
  end;
end;




end.
