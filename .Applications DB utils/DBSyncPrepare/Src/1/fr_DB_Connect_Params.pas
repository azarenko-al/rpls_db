unit fr_DB_Connect_Params;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  dxExEdtr, dxEdLib, dxEditor, dxCntner, StdCtrls, ExtCtrls, DB, AdoDB,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, Buttons,

  dm_Main,

  d_ConnectAdditionalParams,

  u_func_dlg,
  u_func

  ;

type                 
  Tframe_DB_Connect_Params = class(TForm)
    lbUserName: TLabel;
    lbPass: TLabel;
    Label9: TLabel;
    lbDatabase: TLabel;
    lbAuth: TLabel;
    lbServer: TLabel;
    Bevel1: TBevel;
    btn_Additional: TButton;
    ed_Server: TdxButtonEdit;
    ed_User: TdxEdit;
    ed_Password: TdxEdit;
    pn_Connect: TPanel;
    Image3: TImage;
    Image4: TImage;
    AppIcon: TImage;
    KeyImage: TImage;
    Label5: TLabel;
    Label6: TLabel;
    Button1: TButton;
    ed_DataBase: TcxComboBox;
    ed_Auth: TcxComboBox;
    btn_Connect: TBitBtn;
    procedure ed_ServerButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure FormCreate(Sender: TObject);
    procedure ed_Database1Enter(Sender: TObject);
    procedure btn_Setup_and_ConnectClick(Sender: TObject);
    procedure btn_AdditionalClick(Sender: TObject);
    procedure ed_Auth1Change(Sender: TObject);
    procedure btn_ConnectClick(Sender: TObject);

  private
    FNetworkLibrary,
    FNetworkAddress: string;

    function  GetServer(): string;
    function  GetDefaultDB(): string;
    function  GetUser(): string;
    function  GetPassword(): string;

//    function  GetNetworkLibrary(): string;
//    function  GetNetworkAddress(): string;

    procedure SetIsShowBorder(const Value: boolean);
    procedure SetIsShowDB(const Value: boolean);
  public
    CurWidth,
    CurHeight: Integer;

    property  Server: string    read GetServer;
    property  DefaultDB: string read GetDefaultDB;
    property  User: string      read GetUser;
    property  Password: string  read GetPassword;

    property  NetworkLibrary: string  read FNetworkLibrary;
    property  NetworkAddress: string  read FNetworkAddress;

    property  IsShowDB    : boolean  write SetIsShowDB;
    property  IsShowBorder: boolean  write SetIsShowBorder;

    function  RefreshDBList (aServer, aUser, aPassword: string; aEdit: TcxComboBox): boolean;

    function  UseWinAuth: boolean;

    class function CreateChildForm (aOwner: TComponent; aDest: TWinControl): Tframe_DB_Connect_Params;
  end;

var
  frame_DB_Connect_Params: Tframe_DB_Connect_Params;

//==========================================================
implementation  {$R *.DFM}
//==========================================================


//--------------------------------------------------------------------
class function Tframe_DB_Connect_Params.CreateChildForm (aOwner: TComponent;
        aDest: TWinControl): Tframe_DB_Connect_Params;
//--------------------------------------------------------------------
begin
  Result:= Tframe_DB_Connect_Params.Create (aOwner);
  CopyControls (Result, aDest);

  Result.ed_User.Width:= aDest.Width-155;
  Result.ed_Server.Width:= aDest.Width-155;
  Result.ed_Password.Width:= aDest.Width-155;
  Result.ed_DataBase.Width:= aDest.Width-155-35;
  Result.ed_Auth.Width:= aDest.Width-155;

  Result.Bevel1.Width:= aDest.Width-70;

  Result.btn_Additional.Left:= aDest.Width - Result.btn_Additional.width - 7;
  Result.btn_Connect.Left:= aDest.Width - Result.btn_Connect.width - 7;
end;

//----------------------------------------------------------------------
procedure Tframe_DB_Connect_Params.FormCreate(Sender: TObject);
//----------------------------------------------------------------------
var
  oConnectRec: TLoginRec;
begin
  pn_Connect.Caption := '';

  CurWidth := 408;
  CurHeight:= 216;

  dmMain.LoadConnectRecFromReg (oConnectRec);
  dmMain.LoadAdditionalConnectParamsFromReg(oConnectRec.Server, FNetworkLibrary, FNetworkAddress);

//  ed_Auth.Items.Clear;
//  ed_Auth.Items.Add('������������ ������� ������ SQL Server');
//  ed_Auth.Items.Add('������������ ������� ������ Windows   ');

  lbUserName.Caption:='������������';
  lbPass.Caption    :='������';
  LbServer.Caption  :='������';
  lbDatabase.Caption:='���� ������';
  lbAuth.Caption    :='�����������:';

  with oConnectRec do
  begin
    ed_Server.Text:=   Server;
    ed_User.Text:=     User;
    ed_Password.Text:= Password;
    ed_Database.Text:= DefaultDB;
    ed_Auth.ItemIndex:= IIF(IsUseWinAuth, 1, 0);
  end;

  ed_Auth1Change(nil);
end;

(*
//----------------------------------------------------------------------
function Tframe_DB_Connect_Params.GetHeight(): Integer;
//----------------------------------------------------------------------
begin
  Result:= Height;
end;
*)

//----------------------------------------------------------------------
function Tframe_DB_Connect_Params.GetServer(): string;
//----------------------------------------------------------------------
begin
  Result:= ed_Server.Text;
end;

//----------------------------------------------------------------------
function Tframe_DB_Connect_Params.GetDefaultDB(): string;
//----------------------------------------------------------------------
begin
  Result:= ed_DataBase.Text;
end;

//----------------------------------------------------------------------
function Tframe_DB_Connect_Params.GetUser(): string;
//----------------------------------------------------------------------
begin
  Result:= ed_User.Text;
end;

//----------------------------------------------------------------------
function Tframe_DB_Connect_Params.GetPassword(): string;
//----------------------------------------------------------------------
begin
  Result:= ed_Password.Text;
end;

(*
//----------------------------------------------------------------------
function Tframe_DB_Connect_Params.GetNetworkLibrary(): string;
//----------------------------------------------------------------------
begin
  Result:= FNetworkLibrary;
end;

//----------------------------------------------------------------------
function Tframe_DB_Connect_Params.GetNetworkAddress(): string;
//----------------------------------------------------------------------
begin
  Result:= FNetworkAddress;
end;
*)

//----------------------------------------------------------------------
procedure Tframe_DB_Connect_Params.ed_ServerButtonClick(Sender: TObject; AbsoluteIndex: Integer);
//----------------------------------------------------------------------
var  sStr : string;
     oDialog: TOpenDialog;
     sServerName: string;
begin
  sStr := ShowServerDialog(Self.Handle);
  if sStr='' then
    Exit;

(*
  sServerName:= GetLocalCompName();

  if Eq(sStr, 'local')     or Eq(sStr, '(local)') or
     Eq(sStr, '127.0.0.1') or Eq(sStr, sServerName)
  then
    sStr := '(local)';
*)

  ed_Server.Text := sStr;

//  RefreshDBList (ed_Server.Text, ed_User.Text, ed_Password.Text, ed_DataBase);
end;

//----------------------------------------------------------------------
function Tframe_DB_Connect_Params.UseWinAuth: boolean;
//----------------------------------------------------------------------
begin
  Result:= IIF(ed_Auth.ItemIndex=0, false, true);
end;

//----------------------------------------------------------------------
function Tframe_DB_Connect_Params.RefreshDBList;
//----------------------------------------------------------------------
var
  oQuery: TADOQuery;
  s, sError: string;
begin
  if not dmMain.TestConnect(aServer, aUser, aPassword, FNetworkLibrary, FNetworkAddress, UseWinAuth) then
  begin
    Result:= false;
    aEdit.Properties.Items.Clear;
    aEdit.Text:= '';
    ErrorDlg(dmMain.ErrorMsg);
    exit;
  end;

  dmMain.OpenByParams (aServer, aUser, aPassword, 'master',
                       FNetworkLibrary, FNetworkAddress, UseWinAuth);

  oQuery:= TADOQuery.Create(nil);
  oQuery.Connection:= dmMain.ADOConnection;
  Result:= true;

  oQuery.sql.Text:= 'exec sp_databases';

  try
    oQuery.Open;
    aEdit.Properties.Items.Clear;
    aEdit.Text:= '';
  except
    Result:= false;
    aEdit.Properties.Items.Clear;
    aEdit.Text:= '';
    oQuery.Free;
    exit;
  end;


  with oQuery do
    if not IsEmpty then
    begin
      while not Eof do
      begin
        aEdit.Properties.Items.Add(FieldByName('DATABASE_NAME').AsString);

        Next;
      end;
    end;

  if aEdit.Properties.Items.IndexOf(aEdit.Text)=-1 then
    aEdit.Text:= '';

  oQuery.Close;
  oQuery.Free;
end;


//----------------------------------------------------------------------
procedure Tframe_DB_Connect_Params.btn_ConnectClick(Sender: TObject);
//----------------------------------------------------------------------
begin
  if not RefreshDBList (ed_Server.Text, ed_User.Text, ed_Password.Text, ed_DataBase) then
    ShowMessage('�� ������� ������������ � �������. ��������� ��������� �����������');
end;

//----------------------------------------------------------------------
procedure Tframe_DB_Connect_Params.ed_Database1Enter(Sender: TObject);
//----------------------------------------------------------------------
begin
//  if not RefreshDBList (ed_Server.Text, ed_User.Text, ed_Password.Text, ed_DataBase) then
//    ShowMessage('�� ������� ������������ � �������. ��������� ��������� �����������');
end;

//----------------------------------------------------------------------
procedure Tframe_DB_Connect_Params.btn_Setup_and_ConnectClick(Sender: TObject);
//----------------------------------------------------------------------
var
  oConnectRec: TLoginRec;
begin
  with oConnectRec do
  begin
    Server:=    ed_Server.Text;
    User:=      ed_User.Text;
    Password:=  ed_Password.Text;
    DefaultDB:= ed_Database.Text;
    IsUseWinAuth:= UseWinAuth;

    if dmMain.TestConnect(Server, User, Password, FNetworkLibrary, FNetworkAddress, UseWinAuth) then
    begin
      dmMain.SaveConnectRecToReg(oConnectRec);
      if dmMain.Open then
      begin
        ShowMessage('��������� ����������� ������� ���������');
//        dmMain.Add_ODBC_Source(Server, DefaultDB, User);
      end;
    end;
  end;
end;

//----------------------------------------------------------------------
procedure Tframe_DB_Connect_Params.btn_AdditionalClick(Sender: TObject);
//----------------------------------------------------------------------
var
  sParamsStr: string;
  iInd: Integer;
begin
  if TDlg_ConnectAdditionalParams.ExecDlg(Server, sParamsStr) then
  begin
    iInd:= Pos(',', sParamsStr);
    FNetworkLibrary:= Copy(sParamsStr, 1, iInd-1);
    FNetworkAddress:= Copy(sParamsStr, iInd+1, Length(sParamsStr)-iInd);
    dmMain.SaveAdditionalConnectParamsToReg (FNetworkLibrary, FNetworkAddress); ///Server,
  end;
end;

//----------------------------------------------------------------------
procedure Tframe_DB_Connect_Params.SetIsShowBorder(const Value: boolean);
//----------------------------------------------------------------------
begin
(*  if Value then begin
    pn_All.BevelInner:= bvSpace;
    pn_All.BevelOuter:= bvLowered;
  end else
  begin
    pn_All.BevelInner:= bvNone;
    pn_All.BevelOuter:= bvNone;
  end;*)
end;

//----------------------------------------------------------------------
procedure Tframe_DB_Connect_Params.SetIsShowDB(const Value: boolean);
//----------------------------------------------------------------------
begin
  ed_Database.Visible:= Value;
  lbDatabase.Visible:= Value;

(*  if Value then begin
    gb_Server.Height:= 77;
    btn_Additional.Top:= 29;
  end else
  begin
    gb_Server.Height:= 49;
    btn_Additional.Top:= 16;
  end;*)
end;

//----------------------------------------------------------------------
procedure Tframe_DB_Connect_Params.ed_Auth1Change(Sender: TObject);
//------------------- ---------------------------------------------------
begin
  ed_User.Enabled:=     not UseWinAuth;
  ed_Password.Enabled:= not UseWinAuth;
end;


end.



//  u_ConnectAdditionalParams,

///  s: widestring;
 ////, sNetworkLibrary, sNetworkAddress
(*  s:= widestring(ed_Server.Text);
  ConfigureClientForSqlServer(Handle, PChar(s));
  dmMain.LoadAdditionalConnectParamsFromReg(ed_Server.Text, FNetworkLibrary, FNetworkAddress);*)

