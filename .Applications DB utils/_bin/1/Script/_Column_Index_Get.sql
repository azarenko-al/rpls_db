
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Column_Index_Get]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Column_Index_Get]
GO




------------------------------------------------------------------
CREATE PROCEDURE dbo._Column_Index_Get
------------------------------------------------------------------
(
  @TABLE_NAME VARCHAR(50),
  @COLUMN_NAME VARCHAR(50),
  
  @sql_create VARCHAR(8000) output, 
  @sql_drop   VARCHAR(8000) output
    
)

AS
BEGIN
  set @sql_create=''
  set @sql_drop  =''
 


/*

  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME ='lib_Radiation_class'
    set @COLUMN_NAME='name'    
  END*/

  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME ='LinkEnd'
    set @COLUMN_NAME='project_id'   
    
    
 --   ALTER TABLE [dbo].[LinkEnd]

  END


/*
SELECT --TABLE_NAME, 
    CONSTRAINT_TYPE, CONSTRAINT_NAME
  
  FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
  WHERE constraint_catalog=DB_NAME() AND TABLE_NAME = @TABLE_NAME 
*/
 
 
----------------------------------------

SELECT 
    CONSTRAINT_TYPE, CONSTRAINT_NAME
    into #CONSTRAINTS
  FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
  WHERE constraint_catalog=DB_NAME() AND TABLE_NAME = @TABLE_NAME 
--------------------------------------



  SELECT  i.name 
    into #temp  
    
   FROM sysobjects o
     JOIN sysindexes i ON i.id = o.id
     JOIN sysindexkeys ik ON ik.id = i.id  AND ik.indid = i.indid
     JOIN syscolumns c ON c.id = ik.id  AND c.colid = ik.colid
          
    WHERE (i.indid BETWEEN 1 AND 254)
        and o.name=@TABLE_NAME
        and c.name=@COLUMN_NAME
        AND indexproperty(o.id, i.name, 'IsStatistics') = 0
        AND indexproperty(o.id, i.name, 'IsHypothetical') = 0
           
     



if not exists(SELECT * FROM	#temp)
  return -1	
   

--SELECT * FROM	#temp
   
   
SELECT --o.name, 
   i.name,
   col1 = MIN (CASE ik.keyno WHEN 1 THEN c.name END),
   col2 = MIN (CASE ik.keyno WHEN 2 THEN c.name END),
   col3 = MIN (CASE ik.keyno WHEN 3 THEN c.name END),
   col4 = MIN (CASE ik.keyno WHEN 4 THEN c.name END),
   col5 = MIN (CASE ik.keyno WHEN 5 THEN c.name END)   
      
   
  INTO #index_column 
   
 FROM sysobjects o
     JOIN sysindexes i ON i.id = o.id
     JOIN sysindexkeys ik ON ik.id = i.id  AND ik.indid = i.indid
     JOIN syscolumns c ON c.id = ik.id AND c.colid = ik.colid

--     left join #CONSTRAINTS c on c.CONSTRAINT_NAME = m.name

   WHERE i.indid BETWEEN 1 AND 254
       and o.name=@TABLE_NAME
       and c.name=@COLUMN_NAME   
       AND indexproperty(o.id, i.name, 'IsStatistics') = 0
       AND indexproperty(o.id, i.name, 'IsHypothetical') = 0
   GROUP BY o.name, i.name
   ORDER BY o.name, i.name
  
 
 
-- SELECT CONSTRAINT_TYPE    FROM #CONSTRAINTS   
      


SELECT  
    name as CONSTRAINT_NAME,
    
    col1 + 
    + CASE WHEN col2 is not Null then ','+col2 else ''  END    
    + CASE WHEN col3 is not Null then ','+col3 else ''  END    
    + CASE WHEN col4 is not Null then ','+col4 else ''  END    
    + CASE WHEN col5 is not Null then ','+col5 else ''  END  as columns,
    
    c.CONSTRAINT_TYPE

INTO #index_column1
FROM #index_column m	
  left outer join #CONSTRAINTS c on c.CONSTRAINT_NAME = m.name

 
-----------------------------------------------
SELECT

   @sql_drop  =@sql_drop +
 
    CASE  
       WHEN CONSTRAINT_TYPE is Null  then 
          'DROP INDEX '+ @TABLE_NAME +'.'+ CONSTRAINT_NAME                  
       
       WHEN CONSTRAINT_TYPE='UNIQUE'  then 
    --      'DROP INDEX '+ @TABLE_NAME +'.'+ CONSTRAINT_NAME                  
          'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT  '+ CONSTRAINT_NAME 
        
       WHEN CONSTRAINT_TYPE='PRIMARY KEY'  then      
         'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT  '+ CONSTRAINT_NAME        
         
       ELSE
         '--index not defined--'  
    END +';'   

  FROM #index_column1
  
-----------------------------------------------
SELECT

 @sql_create=@sql_create +
 
    CASE  
       WHEN CONSTRAINT_TYPE is Null  then     
         'CREATE INDEX '+ CONSTRAINT_NAME +' ON '+ @TABLE_NAME +' ('+ COLUMNS + ') ON [PRIMARY]'

       WHEN CONSTRAINT_TYPE='UNIQUE'  then       
         'CREATE UNIQUE  INDEX '+ CONSTRAINT_NAME +' ON '+ @TABLE_NAME +' ('+ COLUMNS + ') ON [PRIMARY]'
              
       WHEN CONSTRAINT_TYPE='PRIMARY KEY'  then 
         'ALTER TABLE '+ @TABLE_NAME +' ADD CONSTRAINT '+ CONSTRAINT_NAME + ' PRIMARY KEY ('+ COLUMNS +') ON [PRIMARY]'

       ELSE
         '--index not defined--'  
   
    END +';' 
  

  FROM #index_column1
   
      
 drop table #CONSTRAINTS
 drop table #temp
   
 /*
  begin transaction
  
  exec ( @sql_drop )
  exec ( @sql_create )
  
  rollback transaction*/
   
   
end
GO

