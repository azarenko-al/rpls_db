-- SQL Manager 2011 for SQL Server 3.7.0.2
-- ---------------------------------------
-- ����         : ALEX
-- ���� ������  : mts_vologda
-- ������       : Microsoft SQL Server  9.00.1399.06


--
-- Dropping user-defined function _CHECK_CONSTRAINTS_for_column : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_CHECK_CONSTRAINTS_for_column]') AND xtype IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
  DROP FUNCTION [dbo].[_CHECK_CONSTRAINTS_for_column]
GO

--
-- Definition for user-defined function _CHECK_CONSTRAINTS_for_column : 
--
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION dbo._CHECK_CONSTRAINTS_for_column 
(
  @TABLE_NAME  VARCHAR(50), -- = 'linkend',
  @COLUMN_NAME VARCHAR(50)  -- = 'rx_freq'
)

RETURNS TABLE
AS
	RETURN (

     SELECT 
      
     'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT '+ c.CONSTRAINT_NAME +';' as [sql_drop],
     'ALTER TABLE '+ @TABLE_NAME +' ADD CONSTRAINT  '+ c.CONSTRAINT_NAME + ' check ('+ cc.check_clause +');' as [sql_create]
        
       --    @sql_create ='ALTER TABLE '+ @TABLE_NAME +' ADD CONSTRAINT '+ o.name + 
         --     '  DEFAULT(' +  cc.check_clause  +') FOR '+ @COLUMN_NAME,
 
       
       
     FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS c
      left join INFORMATION_SCHEMA.CHECK_CONSTRAINTS cc
        ON cc.CONSTRAINT_NAME = c.CONSTRAINT_NAME
        
     WHERE c.CONSTRAINT_TYPE='CHECK'
          and c.TABLE_NAME  = @TABLE_NAME 
          and cc.check_clause like '%[[]'+ @COLUMN_NAME  +']%'


    )
GO

