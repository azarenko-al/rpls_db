

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Drop_index]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Drop_index]
GO


------------------------------------------------------------------
CREATE PROCEDURE dbo._Drop_index
------------------------------------------------------------------
(
  @TABLE_NAME VARCHAR(50),
  @INDEX_NAME VARCHAR(50))
AS
BEGIN
  if @TABLE_NAME IS NULL
  BEGIN
  
    set @TABLE_NAME ='Map_XREF'
    set @INDEX_NAME ='IX_Map_XREF' 
    
  END 


DECLARE 
  @sql_drop   VARCHAR(8000)
  
DECLARE 
  @temp table(sql_drop  VARCHAR(1000))


/*
***** Object:  Index [IX_Map_XREF]    Script Date: 02/17/2014 20:49:05 *****
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Map_XREF]') AND name = N'IX_Map_XREF')
DROP INDEX [IX_Map_XREF] ON [dbo].[Map_XREF] WITH ( ONLINE = OFF )
GO
*/


--IF EXISTS(



--return;


insert into @temp

  SELECT 
  --  i.name ,
  --  c.CONSTRAINT_TYPE,
  
--  @sql_drop  =@sql_drop +
 
    CASE  
       WHEN CONSTRAINT_TYPE is Null  then 
          'DROP INDEX '+ @TABLE_NAME +'.'+ i.name                  
       
       WHEN CONSTRAINT_TYPE='UNIQUE'  then 
    --      'DROP INDEX '+ @TABLE_NAME +'.'+ CONSTRAINT_NAME                  
          'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT  '+ i.name 
        
       WHEN CONSTRAINT_TYPE='PRIMARY KEY'  then      
         'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT  '+ i.name        
         
       ELSE
         '--index not defined--'  
    END   as [SQL_drop]    
    
--into #temp1



   FROM sysobjects o
     JOIN sysindexes i ON i.id = o.id
     left outer join INFORMATION_SCHEMA.TABLE_CONSTRAINTS c 
         on c.CONSTRAINT_NAME = i.name
     
   --  JOIN sysindexkeys ik ON ik.id = i.id  AND ik.indid = i.indid
  --   JOIN syscolumns c ON c.id = ik.id  AND c.colid = ik.colid
          
    WHERE 
    
   -- (i.indid BETWEEN 1 AND 254)
       -- and 
        o.name=@TABLE_NAME
        and i.name=@INDEX_NAME
        
        and c.constraint_catalog=DB_NAME() 
        AND c.TABLE_NAME = @TABLE_NAME 
        
        
    --    and c.name=@COLUMN_NAME
--        AND indexproperty(o.id, i.name, 'IsStatistics') = 0
 --       AND indexproperty(o.id, i.name, 'IsHypothetical') = 0
           


--select * from #temp


IF EXISTS(SELECT * FROM	@temp) 
BEGIN
  SELECT @sql_drop=sql_drop FROM @temp 
  
  print @sql_drop
  exec ( @sql_drop  )
  
  return 1
END	 


----------------------------------------------------
-- sql 2000 
----------------------------------------------------

insert into @temp

  SELECT --o.name,i.name,xtype,  --  *

     CASE  
        WHEN xtype='U'  then 
    --      'DROP INDEX '+ @TABLE_NAME +'.'+ CONSTRAINT_NAME                  
        --  'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT  '+ i.name 
        
     'DROP INDEX '+  i.name +' ON ' + @TABLE_NAME 
     
     --+'.'+ i.name    
    
     --  WHEN CONSTRAINT_TYPE='PRIMARY KEY'  then      
      --   'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT  '+ i.name        
         
      ELSE
         '--index not defined--'  
    END  as [SQL_drop]    
   
--  into #temp
 -- into @temp

   FROM sysobjects o
     JOIN sysindexes i ON i.id = o.id
          
    WHERE 
        o.name=@TABLE_NAME
        and i.name=@INDEX_NAME
        
        

IF EXISTS(SELECT * FROM	@temp) 
BEGIN
  SELECT @sql_drop=sql_drop FROM @temp 
  
  print @sql_drop
  exec ( @sql_drop  )
  
  return 1
END	 




select 'index not found' 
print 'index not found'

return -1

end
   

GO

