-- SQL Manager 2011 for SQL Server 3.7.0.2
-- ---------------------------------------
-- ����         : ALEX
-- ���� ������  : mts_vologda
-- ������       : Microsoft SQL Server  9.00.1399.06


--
-- Dropping user-defined function _CHECK_CONSTRAINTS_for_column : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_CHECK_CONSTRAINTS_for_column]') AND xtype IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
  DROP FUNCTION [dbo].[_CHECK_CONSTRAINTS_for_column]
GO

--
-- Definition for user-defined function _CHECK_CONSTRAINTS_for_column : 
--
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION dbo._CHECK_CONSTRAINTS_for_column 
(
  @TABLE_NAME  VARCHAR(50), -- = 'linkend',
  @COLUMN_NAME VARCHAR(50)  -- = 'rx_freq'
)

RETURNS TABLE
AS
	RETURN (

     SELECT 
      
     'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT '+ c.CONSTRAINT_NAME +';' as [sql_drop],
     'ALTER TABLE '+ @TABLE_NAME +' ADD CONSTRAINT  '+ c.CONSTRAINT_NAME + ' check ('+ cc.check_clause +');' as [sql_create]
        
       --    @sql_create ='ALTER TABLE '+ @TABLE_NAME +' ADD CONSTRAINT '+ o.name + 
         --     '  DEFAULT(' +  cc.check_clause  +') FOR '+ @COLUMN_NAME,
 
       
       
     FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS c
      left join INFORMATION_SCHEMA.CHECK_CONSTRAINTS cc
        ON cc.CONSTRAINT_NAME = c.CONSTRAINT_NAME
        
     WHERE c.CONSTRAINT_TYPE='CHECK'
          and c.TABLE_NAME  = @TABLE_NAME 
          and cc.check_clause like '%[[]'+ @COLUMN_NAME  +']%'


    )
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnAdd]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnAdd]
GO




------------------------------------------------------------------
CREATE PROCEDURE dbo._ColumnAdd
------------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(100),
  @COLUMN_TYPE VARCHAR(50),
  @DEFAULT VARCHAR(50)=null
  --, 
--  @ISNULL VARCHAR(50)=null 
)
AS
BEGIN
 declare 
   @s   varchar(1000),
  -- @null  varchar(50),
   @def varchar(50)
  
  set @def=''
  if IsNull(@DEFAULT,'') <> ''
    set @def = ' DEFAULT (' + @DEFAULT + ') ' 

/*
  set @null=''
 if ISNULL(@ISNULL,'') = ''
    set @null = ' ' 
  else
    set @null = ' NOT NULL' 
*/

  IF OBJECT_ID(@TABLE_NAME) IS NULL
  begin 
    print 'OBJECT_ID(@TableName) is null):' + @TABLE_NAME
    RETURN -1;
  end



  ------------------------
  if dbo._ColumnExists(@TABLE_NAME, @COLUMN_NAME)=0
  ---------------------------
  begin  
    set @s = 'ALTER TABLE '+ @TABLE_NAME +' ADD   '+ @COLUMN_NAME + ' ' + @COLUMN_TYPE  +' '+ @def  -- + @null
    print @s
    exec (@s)
    

 --   set @s = 'ALTER TABLE '+ @TABLE_NAME +' ALTER COLUMN   '+ @COLUMN_NAME + ' ' + @COLUMN_TYPE  + @null
  --  print @s
   -- exec (@s)

    
    print @@rowcount
  end else 
    print 'column exists: ' + @TABLE_NAME +' - '+ @COLUMN_NAME 
  

end
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnAddCalculated]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnAddCalculated]
GO



------------------------------------------------------------------
CREATE PROCEDURE dbo._ColumnAddCalculated
------------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(100),
  @DEFAULT     VARCHAR(50) 
)
AS
BEGIN
 declare 
   @s   varchar(1000);
   

  exec _ColumnDrop @TABLE_NAME, @COLUMN_NAME


  set @s = 'ALTER TABLE '+ @TABLE_NAME +' ADD   '+ @COLUMN_NAME + ' as ' + @DEFAULT 
  print @s
  exec (@s)
      
  print @@rowcount


end
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnAddDefault]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnAddDefault]
GO



------------------------------------------------------------------
CREATE PROCEDURE dbo._ColumnAddDefault
------------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(50),
--  @CONSTRAINT_NAME VARCHAR(50),
  @DEFAULT VARCHAR(50)=null 
)
AS
BEGIN


if @TABLE_NAME is null 
BEGIN
 set @TABLE_NAME='link'
 set @COLUMN_NAME ='calc_method'
 set @DEFAULT='(3)'
  
END 

  declare 
    @CONSTRAINT_NAME_NEW varchar(100),
    @Column_default varchar(100),
    @sql varchar(100),
    @s   varchar(1000),
    @null varchar(50) 
   -- @def varchar(50)


  if dbo._ColumnExists(@TABLE_NAME, @COLUMN_NAME)=0
  BEGIN
    print 'column not exists'
    RETURN -1
  END 
  


  if @DEFAULT=''  set @DEFAULT=null


  SELECT --@name=name,  
  		@column_default=Column_default
  	FROM INFORMATION_SCHEMA.Columns 
  	WHERE TABLE_NAME = @TABLE_NAME AND COLUMN_NAME = @COLUMN_NAME


  IF @column_default = @DEFAULT
    RETURN 1



  set @CONSTRAINT_NAME_NEW = 'DF_' + @TABLE_NAME + '_' + @COLUMN_NAME    


   set @sql=''

   SELECT 
     @sql = 'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT '+ o.name 
   
   FROM sysobjects o INNER JOIN                       
        syscolumns c ON o.id = c.cdefault INNER JOIN  
        syscomments com ON o.id = com.id              
   WHERE (o.xtype = 'D') and (OBJECT_NAME(o.parent_obj)=@TABLE_NAME) and (c.name=@COLUMN_NAME)
	
   print @sql
   exec (@sql)

    
    if @DEFAULT is not null
    BEGIN
    
      set @s ='ALTER TABLE '+ @TABLE_NAME +' ADD CONSTRAINT '+ @CONSTRAINT_NAME_NEW + 
              '  DEFAULT ' + @DEFAULT  +' FOR '+ @COLUMN_NAME
      print @s

      exec (@s)      
    END
    

end
GO



IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnAlter]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnAlter]
GO





------------------------------------------------------------------
CREATE PROCEDURE dbo._ColumnAlter
------------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(100),
  @COLUMN_TYPE VARCHAR(50),
  @DEFAULT     VARCHAR(100)=null -- ,
--  @ISNULL      VARCHAR(50)=null

)
AS
BEGIN
-- begin transaction


/*

  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME='AntennaType'
    set @COLUMN_NAME='gain'
    set @COLUMN_TYPE='float'
    set @DEFAULT='(0)'
    set @ISNULL=NULL
    
    
  END */

/*
  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME ='lib_Radiation_class'
    set @COLUMN_NAME='name'  
    set @COLUMN_TYPE='varchar(90)'
  END
 */


  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME ='Link'
    set @COLUMN_NAME='NAME'  
    set @COLUMN_TYPE='varchar(200)'
    set @DEFAULT=''
  END
 
 
--  set @ISNULL=ISNULL(@ISNULL,'')

  if @DEFAULT='' set @DEFAULT=NULL

 

  if dbo._ColumnExists(@TABLE_NAME, @COLUMN_NAME)=0
  BEGIN
    print 'COLUMN_NAME not EXISTS'
    RETURN -1    
  END  


  DECLARE
    @FK_sql_create VARCHAR(8000), 
    @FK_sql_drop   VARCHAR(8000),  


    @DF_sql_create VARCHAR(8000), 
    @DF_sql_drop   VARCHAR(8000),  

    @Index_sql_create VARCHAR(8000), 
    @Index_sql_drop   VARCHAR(8000)  



  declare 
    @is_nullable varchar(50),
    @column_default varchar(100),
    @isnull varchar(10),
    @s   varchar(1000) 
 

/*

  ------------------------
 -- if @COLUMN_TYPE='sysname'
  if @DEFAULT is not null   
  ---------------------------
 begin

    set @s ='ALTER TABLE '+@TABLE_NAME+' DISABLE TRIGGER all'
    print @s
    exec (@s)
 
    set @s ='UPDATE  '+ @TABLE_NAME +' set '+ @COLUMN_NAME +'='+ @DEFAULT  +' WHERE ' + @COLUMN_NAME  +' IS NULL '
    print @s
    exec (@s)
    
    set @s ='ALTER TABLE '+@TABLE_NAME+' ENABLE TRIGGER all'
    print @s
    exec (@s)
    
  end

*/


  SELECT --@name=name,  
  		@Column_default = Column_default,
  		@is_nullable    = is_nullable
        
  	FROM 
      INFORMATION_SCHEMA.Columns 
  	WHERE 
       TABLE_NAME = @TABLE_NAME AND COLUMN_NAME = @COLUMN_NAME


  exec _Column_FK_Get  @TABLE_NAME, @COLUMN_NAME, @FK_sql_create output, @FK_sql_drop output
  exec _Column_DF_Get  @TABLE_NAME, @COLUMN_NAME, @DF_sql_create output, @DF_sql_drop output
  exec _Column_Index_Get  @TABLE_NAME, @COLUMN_NAME, @Index_sql_create output, @Index_sql_drop output




/*
print @FK_sql_create
print @FK_sql_drop


print @DF_sql_create
print @DF_sql_drop

print @Index_sql_create
print @Index_sql_drop
*/


   exec (@FK_sql_drop)
   exec (@Index_sql_drop)
   exec (@DF_sql_drop)


  if @is_nullable = 'no' 
     set @isnull=' NOT NULL'    
  else 
    set @isnull=' NULL'



  set @s ='ALTER TABLE '+ @TABLE_NAME +' ALTER COLUMN  '
          + @COLUMN_NAME + ' ' + @COLUMN_TYPE + @isnull 
          -- +' '+  @ISNULL
  print @s
  exec (@s)
 

/*
print @FK_sql_drop
print @DF_sql_drop
print @Index_sql_drop

*/

print @FK_sql_create
print @DF_sql_create
print @Index_sql_create


 exec (@Index_sql_create)
 exec (@FK_sql_create)
 exec (@DF_sql_create)



  --rollback transaction
  
end

go


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnDrop]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnDrop]
GO



------------------------------------------------------------------
CREATE PROCEDURE dbo._ColumnDrop
------------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(50)
)
AS
BEGIN
  DECLARE 
    @sql varchar(4000);

  DECLARE
    @FK_sql_create VARCHAR(8000), 
    @FK_sql_drop   VARCHAR(8000),  


    @DF_sql_create VARCHAR(8000), 
    @DF_sql_drop   VARCHAR(8000),  

    @Index_sql_create VARCHAR(8000), 
    @Index_sql_drop   VARCHAR(8000)  




if @TABLE_NAME is null 
BEGIN
 set @TABLE_NAME='linkend'
 set @COLUMN_NAME ='folder_id'
  
END 


  if dbo._ColumnExists(@TABLE_NAME, @COLUMN_NAME)=0
  BEGIN
    print 'COLUMN_NAME not EXISTS'
    RETURN -1    
  END  



    
  ------------------------
  -- _Drop_Check_Constraints_for_Column
  ------------------------
  set @sql=''

 SELECT 
  
   @sql=@sql + 'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT  '+ c.CONSTRAINT_NAME +';'
    
 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS c
  left join INFORMATION_SCHEMA.CHECK_CONSTRAINTS cc
    ON cc.CONSTRAINT_NAME = c.CONSTRAINT_NAME
    
 WHERE c.CONSTRAINT_TYPE='CHECK'
      and c.TABLE_NAME  = @TABLE_NAME 
      and cc.check_clause like '%[[]'+ @COLUMN_NAME  +']%'


  print @sql  
  exec ( @sql )


-------------------------------------------------


  exec _Column_FK_Get  @TABLE_NAME, @COLUMN_NAME, @FK_sql_create output, @FK_sql_drop output
  exec _Column_DF_Get  @TABLE_NAME, @COLUMN_NAME, @DF_sql_create output, @DF_sql_drop output
  exec _Column_Index_Get  @TABLE_NAME, @COLUMN_NAME, @Index_sql_create output, @Index_sql_drop output



print @FK_sql_drop
print @DF_sql_drop
print @Index_sql_drop

  if @FK_sql_drop   <>'' exec (@FK_sql_drop)
  if @Index_sql_drop<>'' exec (@Index_sql_drop)
  if @DF_sql_drop   <>'' exec (@DF_sql_drop)


   
 set @sql= 'ALTER TABLE '+ @TABLE_NAME +' DROP COLUMN ['+ @COLUMN_NAME + ']'
 print @sql    
   
   
 exec (@sql)
        
end
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnExists]') AND xtype IN (N'FN', N'IF', N'TF'))
  DROP FUNCTION [dbo].[_ColumnExists]
GO



--------------------------------------------------------------------------------
CREATE FUNCTION [dbo]._ColumnExists(@TableName varchar(100), @ColumnName varchar(100))
--------------------------------------------------------------------------------
RETURNS bit AS
BEGIN
 -- DECLARE @Result bit;

  IF not (OBJECT_ID(@TableName)>0)
  begin 
  -- print 'OBJECT_ID(@TableName) is null):' + @TableName
    RETURN -1;
  end

  IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.Columns 
              WHERE TABLE_NAME = @TableName 
                AND COLUMN_NAME = @ColumnName)
    RETURN 1
    --SET @Result = 1
  ELSE  
    RETURN 0
    --SET @Result = 0


  RETURN 111
--@Result;

END

GO










IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnRename]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnRename]
GO


----------------------------------------------------
CREATE PROCEDURE dbo._ColumnRename
---------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(50),
  @COLUMN_NAME_NEW VARCHAR(50)
)
AS
BEGIN
  DECLARE 
    @s VARCHAR(1000);

  DECLARE
    @sql_create VARCHAR(8000), 
    @sql_drop   VARCHAR(8000)

 if @TABLE_NAME is null
 begin
   set @TABLE_NAME = 'linkend';
   set @COLUMN_NAME = 'tx_freq';
   set @COLUMN_NAME_NEW = 'tx_freq1';
   
 end



  IF not (OBJECT_ID(@TABLE_NAME)>0)
  begin 
   print 'OBJECT_ID(@TableName) is null):' + @TABLE_NAME
  --  RETURN -1;
  end


  if not ((dbo._ColumnExists(@TABLE_NAME, @COLUMN_NAME)=1 ) and
         (dbo._ColumnExists(@TABLE_NAME, @COLUMN_NAME_NEW)=0 ))         
    RETURN;
         
--  begin

 set @sql_create=''
 set @sql_drop=''

 select 
	  @sql_drop   = @sql_drop + sql_drop,
	  @sql_create = @sql_create + 
                       replace(sql_create, '['+@COLUMN_NAME+']', '['+@COLUMN_NAME_NEW+']')
  
  FROM _CHECK_CONSTRAINTS_for_column(@TABLE_NAME,@COLUMN_NAME)

  print @sql_drop  
  exec ( @sql_drop )
  


    set @s = 'exec sp_RENAME '''+ @TABLE_NAME +'.'+ @COLUMN_NAME +''', '''+ @COLUMN_NAME_NEW +''', ''COLUMN'''

print @s

      exec('exec sp_RENAME '''+ @TABLE_NAME +'.'+ @COLUMN_NAME +''', '''+ @COLUMN_NAME_NEW +''', ''COLUMN''')


  print @sql_create  
  exec ( @sql_create )
  

--  end

end


go


               
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnSetIdentity]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnSetIdentity]
GO



-------------------------------------------------------------
CREATE PROCEDURE [dbo]._ColumnSetIdentity
--------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(50),
  @FLAG        VARCHAR(50)
)
AS
BEGIN
 -- return -1



  IF @TABLE_NAME is null 
  BEGIN
    set @TABLE_NAME  = '_objects'
    
    set @TABLE_NAME  = 'lib_VENDOR_Equipment'  
      
    set @COLUMN_NAME = 'id'
    set @FLAG='ON' 
    
  END 


  IF OBJECT_ID(@TABLE_NAME) is NULL
    RETURN -1

  DECLARE
  	@column_names VARCHAR(8000),
  
    @sql_insert VARCHAR(8000), 
    
    @FK_sql_create VARCHAR(8000), 
    @FK_sql_drop   VARCHAR(8000), 
 
    @Index_sql_create VARCHAR(8000), 
    @Index_sql_drop   VARCHAR(8000)  
   


  declare 
    @Column_default varchar(100),
    @IS_NULLABLE   varchar(3),
    @data_type     varchar(50),
    
    @TABLE_NAME_temp  VARCHAR(50),
          
    @s varchar(500), 
    @sql varchar(500) 



  set @column_names=''
   

  set @TABLE_NAME_temp = @TABLE_NAME + '___'
 
 
 if object_id (@TABLE_NAME_temp) is not null 
   exec('drop table '+ @TABLE_NAME_temp)
 
 

  SELECT  @column_names=@column_names + ',' + column_name 
    FROM [INFORMATION_SCHEMA].[COLUMNS]
    WHERE (TABLE_NAME=@TABLE_NAME) 
    
  set @column_names = SUBSTRING(@column_names, 2, LEN(@column_names))
  

  
--print @column_names    
    

--begin tran
        
 

  exec _Column_FK_Get     @TABLE_NAME, @COLUMN_NAME, @FK_sql_create output,    @FK_sql_drop output
  exec _Column_Index_Get  @TABLE_NAME, @COLUMN_NAME, @Index_sql_create output, @Index_sql_drop output


print @FK_sql_drop
print @Index_sql_drop

  exec (@FK_sql_drop)
  exec (@Index_sql_drop)


  exec ( 'select * into '+ @TABLE_NAME_temp + ' FROM '+ @TABLE_NAME )
  exec ( 'DELETE FROM '+ @TABLE_NAME )
 

  if dbo._ColumnExists(@TABLE_NAME, @COLUMN_NAME)=1
  BEGIN
    set @sql= 'ALTER TABLE '+ @TABLE_NAME +' drop column  ' +  @COLUMN_NAME 
    print @sql
    exec (@sql)
  END  



  if @FLAG = 'OFF'
    set @sql= 'ALTER TABLE '+ @TABLE_NAME +' add  ' +  @COLUMN_NAME +' int '
  ELSE
    set @sql= 'ALTER TABLE '+ @TABLE_NAME +' add  ' +  @COLUMN_NAME +' int IDENTITY(1,1)'

  print @sql
  exec (@sql)

  set @sql= 'ALTER TABLE '+ @TABLE_NAME +' alter column  ' +  @COLUMN_NAME +' int not null'
 
  print @sql
  exec (@sql)


  set @sql_insert = ''


  
  if @FLAG = 'ON'
  BEGIN
  --  print ( 'SET IDENTITY_INSERT '+ @TABLE_NAME +' ON') 
  --  exec  ( 'SET IDENTITY_INSERT '+ @TABLE_NAME +' ON')
    
    set @sql_insert = @sql_insert + 'SET IDENTITY_INSERT '+ @TABLE_NAME +' ON;'    
  END 

  set @sql_insert =  @sql_insert +
     'INSERt INTO '+ @TABLE_NAME +' ('+  @column_names + ') '+
     '  SELECT '+  @column_names +' FROM ' + @TABLE_NAME_temp + ';'
     
--  print @sql_insert
 -- exec (@sql_insert )


  if @FLAG = 'ON'
--    exec ( 'SET IDENTITY_INSERT '+ @TABLE_NAME +' OFF')  
  BEGIN
  --  print ( 'SET IDENTITY_INSERT '+ @TABLE_NAME +' OFF') 
   -- exec  ( 'SET IDENTITY_INSERT '+ @TABLE_NAME +' OFF')    

    set @sql_insert = @sql_insert + 'SET IDENTITY_INSERT '+ @TABLE_NAME +' OFF;'    

  END   
  
  print @sql_insert
  exec  ( @sql_insert)  
  
  
 -- exec ('DROP TABLE ' + @TABLE_NAME_temp)
  
 
print @Index_sql_create
print @FK_sql_create

  exec (@Index_sql_create)
  exec (@FK_sql_create)
    

--rollback tran


END
GO
 



IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnSetNull]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnSetNull]
GO


------------------------------------------------------------------
CREATE PROCEDURE dbo._ColumnSetNull
------------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(50),
--  @CONSTRAINT_NAME VARCHAR(50),
  
  @ISNULL VARCHAR(50)
)
AS
BEGIN
  declare 
    @Column_default varchar(100),
    @is_nullable   varchar(3),
    @data_type     varchar(50),
    
    @FK_sql_create VARCHAR(8000) , 
    @FK_sql_drop   VARCHAR(8000) ,

    @Index_sql_create VARCHAR(8000) , 
    @Index_sql_drop   VARCHAR(8000) ,
          
    @s varchar(500), 
    @sql varchar(500) 



  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME ='_db_version_'
    set @COLUMN_NAME='product_name'  
    set @ISNULL='not null'
  END


--exec _ColumnSetNull '_db_version_','product_name','NOT NULL'


set @ISNULL = IsNull(@ISNULL,'')


IF IsNull(@ISNULL,'') = 'null'
  set @is_nullable='yes'
else
  set @is_nullable='no'



--set @ISNULL = IsNull(@ISNULL,

-- COLUMN_DEFAULT

set @sql=''
set @data_type=''

  SELECT 
    @column_default = column_default,
  
    @data_type = DATA_TYPE +
         
         case 
           when character_maximum_length>0 then
             '('+ cast(character_maximum_length as VARCHAR(50)) +') '
           else ' ' end ,
  
  
   @sql =  
   'ALTER TABLE '+ TABLE_NAME +' ALTER COLUMN '+ COLUMN_NAME +' '
   

    
 FROM [INFORMATION_SCHEMA].[COLUMNS]
 where TABLE_NAME = @TABLE_NAME 
     and COLUMN_NAME= @COLUMN_NAME
     and IS_NULLABLE <> @is_nullable


if @sql=''
BEGIN
  print 'no data need to be changed'
  return -1  
END  


set @data_type=replace(@data_type,'nvarchar(128)','sysname')

if @data_type='sysname'
BEGIN
  print 'sysname is always not null'
  return -2    
END 


set @sql=@sql + @data_type +  ' ' + @ISNULL





--------------------------------------------------
 -- if @COLUMN_TYPE='sysname'
  if (@ISNULL = 'not null') and (@column_default is not null)   
--------------------------------------------------
 begin

    set @s ='ALTER TABLE '+@TABLE_NAME+' DISABLE TRIGGER all'
    print @s
    exec (@s)
 
    set @s ='UPDATE  '+ @TABLE_NAME +' set '+ @COLUMN_NAME +'='+ @column_default  +' WHERE ' + @COLUMN_NAME  +' IS NULL '
    print @s
    exec (@s)
    
    set @s ='ALTER TABLE '+@TABLE_NAME+' ENABLE TRIGGER all'
    print @s
    exec (@s)
    
  end


 -- if (@ISNULL = 'not null') 
  --  IF EXISTS(SELECT * FROM ) 


 
 
exec _Column_Index_Get  @TABLE_NAME, @COLUMN_NAME,  @Index_sql_create output, @Index_sql_drop output


  print @Index_sql_drop


--begin tran


  if @Index_sql_drop<>'' exec (@Index_sql_drop)
 

  print @sql
  exec (@sql)
  

  if @Index_sql_create<>'' exec (@Index_sql_create)

 
-- rollback
  


end




GO


  
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Column_DF_Get]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Column_DF_Get]
GO



---------------------------------------------------------------
CREATE PROCEDURE dbo._Column_DF_Get
---------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(50),
  
  @sql_create VARCHAR(8000) output, 
  @sql_drop   VARCHAR(8000) output
  
)
as
BEGIN

  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME ='lib_Radiation_class'
    set @COLUMN_NAME='name'  
  END
 

  set @sql_create=''
  set @sql_drop=''
 

   SELECT
   -- @DEFAULT_NAME =o.name 
   
     @sql_create ='ALTER TABLE '+ @TABLE_NAME +' ADD CONSTRAINT '+ o.name + 
              '  DEFAULT(' +  com.text  +') FOR '+ @COLUMN_NAME,
                
      @sql_drop= 'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT '+ o.name
   
   --o.name, com.text   
     FROM sysobjects o INNER JOIN                       
          syscolumns c ON o.id = c.cdefault INNER JOIN  
          syscomments com ON o.id = com.id              
     WHERE (o.xtype = 'D') 
       and (OBJECT_NAME(o.parent_obj)=@TABLE_NAME) 
       and (c.name=@COLUMN_NAME)
    
  
  return @@ROWCOUNT
  

END
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Column_FK_Get]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Column_FK_Get]
GO


-------------------------------------------------------
CREATE PROCEDURE dbo._Column_FK_Get
-------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(50),
  
  @sql_create VARCHAR(8000) output, 
  @sql_drop   VARCHAR(8000) output
  
)
as
BEGIN


if @TABLE_NAME is null 
BEGIN
 set @TABLE_NAME='Passive_Component'
 set @COLUMN_NAME ='channels'

 set @TABLE_NAME='link'
 set @COLUMN_NAME ='id'


--// set @TABLE_NAME='LinkFreqPlan'
--// set @COLUMN_NAME ='folder_id'
  
END 

/*
  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME ='lib_Radiation_class'
    set @COLUMN_NAME='name'  
  END
*/ 
  
  /*
  DECLARE
    @sql  VARCHAR(8000), 
    @sql_drop VARCHAR(8000); 
    
  */

  select 
      object_name(constid) NAME, 
      
      object_name(fkeyid)  	FK_Table_name, 
      c1.name 			  		 	FK_Column_name,
      
      object_name(rkeyid)  	PK_Table_name, 
      c2.name 			   			PK_Column_name,
      
       ObjectProperty(constid, 'CnstIsUpdateCascade') as IsUpdateCascade,   
       ObjectProperty(constid, 'CnstIsDeleteCascade') as IsDeleteCascade  
   
  INTO #temp      
      
   from sysforeignkeys s
     inner join syscolumns c1 on ( s.fkeyid = c1.id and s.fkey = c1.colid )
     inner join syscolumns c2 on ( s.rkeyid = c2.id and s.rkey = c2.colid )
     
  WHERE 
    (object_name(fkeyid)=@TABLE_NAME and c1.name=@COLUMN_NAME)
    or
    (object_name(rkeyid)=@TABLE_NAME and c2.name=@COLUMN_NAME) 
    
     

--select * from   #temp  

    
     
     
  set @sql_create=''
  set @sql_drop=''
     
  SELECT 
     --*
  
    @sql_create=@sql_create + 
          'ALTER TABLE '+ FK_Table_name + ' ADD CONSTRAINT ' +  NAME
           + ' FOREIGN KEY (' + FK_Column_name + ') REFERENCES '+ PK_Table_name
           + ' (' + PK_Column_name + ') ' + 
           
           CASE  
              when IsUpdateCascade=1 then  'ON UPDATE CASCADE '
           END +

           CASE 
              when IsDeleteCascade=1 then  'ON DELETE CASCADE '
           END             
           
           + ';'+ CHAR(10)
  
,
    @sql_drop=@sql_drop + 
          'ALTER TABLE '+ FK_Table_name + ' DROP CONSTRAINT ' +  NAME + ';'+ CHAR(10)

  FROM #temp  


 /*
  
  ALTER TABLE [dbo].[AntennaType]
ADD CONSTRAINT [FK_AntennaType_Folder] FOREIGN KEY ([folder_id]) 
  REFERENCES [dbo].[Folder] ([id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO*/


print @sql_create
print @sql_drop
 
END

go





   

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Column_Index_Get]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Column_Index_Get]
GO




------------------------------------------------------------------
CREATE PROCEDURE dbo._Column_Index_Get
------------------------------------------------------------------
(
  @TABLE_NAME VARCHAR(50),
  @COLUMN_NAME VARCHAR(50),
  
  @sql_create VARCHAR(8000) output, 
  @sql_drop   VARCHAR(8000) output
    
)

AS
BEGIN
  set @sql_create=''
  set @sql_drop  =''
 


/*

  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME ='lib_Radiation_class'
    set @COLUMN_NAME='name'    
  END*/

  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME ='LinkEnd'
    set @COLUMN_NAME='project_id'   
    
    
 --   ALTER TABLE [dbo].[LinkEnd]

  END


/*
SELECT --TABLE_NAME, 
    CONSTRAINT_TYPE, CONSTRAINT_NAME
  
  FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
  WHERE constraint_catalog=DB_NAME() AND TABLE_NAME = @TABLE_NAME 
*/
 
 
----------------------------------------

SELECT 
    CONSTRAINT_TYPE, CONSTRAINT_NAME
    into #CONSTRAINTS
  FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
  WHERE constraint_catalog=DB_NAME() AND TABLE_NAME = @TABLE_NAME 
--------------------------------------



  SELECT  i.name 
    into #temp  
    
   FROM sysobjects o
     JOIN sysindexes i ON i.id = o.id
     JOIN sysindexkeys ik ON ik.id = i.id  AND ik.indid = i.indid
     JOIN syscolumns c ON c.id = ik.id  AND c.colid = ik.colid
          
    WHERE (i.indid BETWEEN 1 AND 254)
        and o.name=@TABLE_NAME
        and c.name=@COLUMN_NAME
        AND indexproperty(o.id, i.name, 'IsStatistics') = 0
        AND indexproperty(o.id, i.name, 'IsHypothetical') = 0
           
     



if not exists(SELECT * FROM	#temp)
  return -1	
   

--SELECT * FROM	#temp
   
   
SELECT --o.name, 
   i.name,
   col1 = MIN (CASE ik.keyno WHEN 1 THEN c.name END),
   col2 = MIN (CASE ik.keyno WHEN 2 THEN c.name END),
   col3 = MIN (CASE ik.keyno WHEN 3 THEN c.name END),
   col4 = MIN (CASE ik.keyno WHEN 4 THEN c.name END),
   col5 = MIN (CASE ik.keyno WHEN 5 THEN c.name END)   
      
   
  INTO #index_column 
   
 FROM sysobjects o
     JOIN sysindexes i ON i.id = o.id
     JOIN sysindexkeys ik ON ik.id = i.id  AND ik.indid = i.indid
     JOIN syscolumns c ON c.id = ik.id AND c.colid = ik.colid

--     left join #CONSTRAINTS c on c.CONSTRAINT_NAME = m.name

   WHERE i.indid BETWEEN 1 AND 254
       and o.name=@TABLE_NAME
       and c.name=@COLUMN_NAME   
       AND indexproperty(o.id, i.name, 'IsStatistics') = 0
       AND indexproperty(o.id, i.name, 'IsHypothetical') = 0
   GROUP BY o.name, i.name
   ORDER BY o.name, i.name
  
 
 
-- SELECT CONSTRAINT_TYPE    FROM #CONSTRAINTS   
      


SELECT  
    name as CONSTRAINT_NAME,
    
    col1 + 
    + CASE WHEN col2 is not Null then ','+col2 else ''  END    
    + CASE WHEN col3 is not Null then ','+col3 else ''  END    
    + CASE WHEN col4 is not Null then ','+col4 else ''  END    
    + CASE WHEN col5 is not Null then ','+col5 else ''  END  as columns,
    
    c.CONSTRAINT_TYPE

INTO #index_column1
FROM #index_column m	
  left outer join #CONSTRAINTS c on c.CONSTRAINT_NAME = m.name

 
-----------------------------------------------
SELECT

   @sql_drop  =@sql_drop +
 
    CASE  
       WHEN CONSTRAINT_TYPE is Null  then 
          'DROP INDEX '+ @TABLE_NAME +'.'+ CONSTRAINT_NAME                  
       
       WHEN CONSTRAINT_TYPE='UNIQUE'  then 
    --      'DROP INDEX '+ @TABLE_NAME +'.'+ CONSTRAINT_NAME                  
          'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT  '+ CONSTRAINT_NAME 
        
       WHEN CONSTRAINT_TYPE='PRIMARY KEY'  then      
         'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT  '+ CONSTRAINT_NAME        
         
       ELSE
         '--index not defined--'  
    END +';'   

  FROM #index_column1
  
-----------------------------------------------
SELECT

 @sql_create=@sql_create +
 
    CASE  
       WHEN CONSTRAINT_TYPE is Null  then     
         'CREATE INDEX '+ CONSTRAINT_NAME +' ON '+ @TABLE_NAME +' ('+ COLUMNS + ') ON [PRIMARY]'

       WHEN CONSTRAINT_TYPE='UNIQUE'  then       
         'CREATE UNIQUE  INDEX '+ CONSTRAINT_NAME +' ON '+ @TABLE_NAME +' ('+ COLUMNS + ') ON [PRIMARY]'
              
       WHEN CONSTRAINT_TYPE='PRIMARY KEY'  then 
         'ALTER TABLE '+ @TABLE_NAME +' ADD CONSTRAINT '+ CONSTRAINT_NAME + ' PRIMARY KEY ('+ COLUMNS +') ON [PRIMARY]'

       ELSE
         '--index not defined--'  
   
    END +';' 
  

  FROM #index_column1
   
      
 drop table #CONSTRAINTS
 drop table #temp
   
 /*
  begin transaction
  
  exec ( @sql_drop )
  exec ( @sql_create )
  
  rollback transaction*/
   
   
end
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Create_FK]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Create_FK]
GO



------------------------------------------------------------------
CREATE PROCEDURE dbo._Create_FK
------------------------------------------------------------------
(
  @NAME  VARCHAR(100),
                  
  @PK_TABLE_NAME   VARCHAR(100),
  @PK_COLUMN_NAME  VARCHAR(100),
   
  @FK_TABLE_NAME   VARCHAR(100),   
  @FK_COLUMN_NAME  VARCHAR(100),  
  
  @ON  VARCHAR(50)  =null
  
)

AS
BEGIN
  print ''
  
  
  DECLARE
     @s VARCHAR(1000);


if @NAME is null
BEGIN
  set @NAME ='FK_LinkEndType_lib_VENDOR_Equipment'
                  
  set @PK_TABLE_NAME  ='lib_VENDOR_Equipment'
  set @pk_column_name  ='id'
   
  set @fk_table_name  ='LinkEndType'
  set @fk_column_name ='vendor_equipment_id'
  
  
END 




  exec _Drop_FK @NAME --@TABLE_NAME, 

 


  set @s = 'ALTER TABLE '+ @fk_table_name +' with nocheck ADD CONSTRAINT ' + @NAME 
      +' FOREIGN KEY ('+ @FK_COLUMN_NAME + ') REFERENCES ' + @pk_table_name + ' (' + @pk_column_name + ') '
      + COALESCE(@ON,'')
           
  print @s    
  exec(@s)  
  

  set @s = 'ALTER TABLE '+ @fk_table_name +' check  CONSTRAINT ' + @NAME 
  print @s    
  exec(@s)  


  return 0
  
  /*
  
  ALTER TABLE [dbo].[AntennaType]
ADD CONSTRAINT [FK_AntennaType_Folder] FOREIGN KEY ([folder_id]) 
  REFERENCES [dbo].[Folder] ([id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO*/
  
END
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Drop]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Drop]
GO




------------------------------------------------------------------
CREATE PROCEDURE dbo._Drop
------------------------------------------------------------------
(
  @OBJECT_NAME VARCHAR(50),
  @NAME  VARCHAR(100)
)
AS
BEGIN
  declare
   @s varchar(1000);


  if @OBJECT_NAME = 'view'
    if object_id(@NAME) is not null  exec ('drop view '+ @NAME) 
  
  if @OBJECT_NAME = 'Procedure'
    if object_id(@NAME) is not null  exec ('drop Procedure '+ @NAME) 

  if @OBJECT_NAME = 'function'
    if object_id(@NAME) is not null  exec ('drop function '+ @NAME) 
  

  ---------------------------------------------
  if @OBJECT_NAME = 'trigger'
  ---------------------------------------------
  begin
    if OBJECTPROPERTY(OBJECT_ID(@NAME), 'IsTrigger')=1
    begin
      set @s='DROP TRIGGER '+ @NAME 
      print @s
      exec(@s)
    end       

  end

 
end
GO




IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Drop_FK]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Drop_FK]
GO



------------------------------------------------------------------
CREATE PROCEDURE dbo._Drop_FK
------------------------------------------------------------------
(
  @NAME  VARCHAR(100)
    
)
as
BEGIN

DECLARE
  @sql_drop VARCHAR(8000) 

/*

if @TABLE_NAME is null 
BEGIN
 set @TABLE_NAME='LinkFreqPlan'
 set @COLUMN_NAME ='folder_id'
  
END */




  select 
      object_name(constid) NAME,       
      object_name(fkeyid)  FK_Table_name 

  INTO #temp      
      
   from sysforeignkeys s
     inner join syscolumns c1 on ( s.fkeyid = c1.id and s.fkey = c1.colid )
     inner join syscolumns c2 on ( s.rkeyid = c2.id and s.rkey = c2.colid )
     
  WHERE 
    object_name(constid)=@NAME         
    
        
 -- SELECT * FROM #temp 
     
     
  set @sql_drop=''
     
  SELECT 
     --*  
   
    @sql_drop=@sql_drop + 
          'ALTER TABLE '+ FK_Table_name + ' DROP CONSTRAINT ' +  NAME + ';'+ CHAR(10)

  FROM #temp  
 
  
  print @sql_drop  
  exec (@sql_drop)

 
 
 -- begin transaction

 -- rollback transaction

END
GO




IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Drop_index]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Drop_index]
GO


------------------------------------------------------------------
CREATE PROCEDURE dbo._Drop_index
------------------------------------------------------------------
(
  @TABLE_NAME VARCHAR(50),
  @INDEX_NAME VARCHAR(50))
AS
BEGIN
  if @TABLE_NAME IS NULL
  BEGIN
  
    set @TABLE_NAME ='Map_XREF'
    set @INDEX_NAME ='IX_Map_XREF' 
    
  END 


DECLARE 
  @sql_drop   VARCHAR(8000)
  
DECLARE 
  @temp table(sql_drop  VARCHAR(1000))


/*
***** Object:  Index [IX_Map_XREF]    Script Date: 02/17/2014 20:49:05 *****
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Map_XREF]') AND name = N'IX_Map_XREF')
DROP INDEX [IX_Map_XREF] ON [dbo].[Map_XREF] WITH ( ONLINE = OFF )
GO
*/


--IF EXISTS(



--return;


insert into @temp

  SELECT 
  --  i.name ,
  --  c.CONSTRAINT_TYPE,
  
--  @sql_drop  =@sql_drop +
 
    CASE  
       WHEN CONSTRAINT_TYPE is Null  then 
          'DROP INDEX '+ @TABLE_NAME +'.'+ i.name                  
       
       WHEN CONSTRAINT_TYPE='UNIQUE'  then 
    --      'DROP INDEX '+ @TABLE_NAME +'.'+ CONSTRAINT_NAME                  
          'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT  '+ i.name 
        
       WHEN CONSTRAINT_TYPE='PRIMARY KEY'  then      
         'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT  '+ i.name        
         
       ELSE
         '--index not defined--'  
    END   as [SQL_drop]    
    
--into #temp1



   FROM sysobjects o
     JOIN sysindexes i ON i.id = o.id
     left outer join INFORMATION_SCHEMA.TABLE_CONSTRAINTS c 
         on c.CONSTRAINT_NAME = i.name
     
   --  JOIN sysindexkeys ik ON ik.id = i.id  AND ik.indid = i.indid
  --   JOIN syscolumns c ON c.id = ik.id  AND c.colid = ik.colid
          
    WHERE 
    
   -- (i.indid BETWEEN 1 AND 254)
       -- and 
        o.name=@TABLE_NAME
        and i.name=@INDEX_NAME
        
        and c.constraint_catalog=DB_NAME() 
        AND c.TABLE_NAME = @TABLE_NAME 
        
        
    --    and c.name=@COLUMN_NAME
--        AND indexproperty(o.id, i.name, 'IsStatistics') = 0
 --       AND indexproperty(o.id, i.name, 'IsHypothetical') = 0
           


--select * from #temp


IF EXISTS(SELECT * FROM	@temp) 
BEGIN
  SELECT @sql_drop=sql_drop FROM @temp 
  
  print @sql_drop
  exec ( @sql_drop  )
  
  return 1
END	 


----------------------------------------------------
-- sql 2000 
----------------------------------------------------

insert into @temp

  SELECT --o.name,i.name,xtype,  --  *

     CASE  
        WHEN xtype='U'  then 
    --      'DROP INDEX '+ @TABLE_NAME +'.'+ CONSTRAINT_NAME                  
        --  'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT  '+ i.name 
        
     'DROP INDEX '+  i.name +' ON ' + @TABLE_NAME 
     
     --+'.'+ i.name    
    
     --  WHEN CONSTRAINT_TYPE='PRIMARY KEY'  then      
      --   'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT  '+ i.name        
         
      ELSE
         '--index not defined--'  
    END  as [SQL_drop]    
   
--  into #temp
 -- into @temp

   FROM sysobjects o
     JOIN sysindexes i ON i.id = o.id
          
    WHERE 
        o.name=@TABLE_NAME
        and i.name=@INDEX_NAME
        
        

IF EXISTS(SELECT * FROM	@temp) 
BEGIN
  SELECT @sql_drop=sql_drop FROM @temp 
  
  print @sql_drop
  exec ( @sql_drop  )
  
  return 1
END	 




select 'index not found' 
print 'index not found'

return -1

end
   

GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Drop_table]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Drop_table]
GO



------------------------------------------------------------------
CREATE PROCEDURE dbo._Drop_table
------------------------------------------------------------------
(
  @TABLE_NAME VARCHAR(50)
)
AS
BEGIN
  declare
   @fk_name varchar(100),
   @fk_table_name varchar(100),
   @sql varchar(4000);



  if OBJECT_ID(@TABLE_NAME) is null 
    return -1

--------------------------------------------
  set @sql =''

    
  SELECT   
/*                                            
     OBJECT_NAME(sf.rkeyid)    AS PK_TABLE_NAME,                           
     col_name(sf.rkeyid, rkey) AS pk_column_name,                      
     OBJECT_NAME(sf.fkeyid)    AS fk_table_name,                          
     col_name(sf.fkeyid, fkey) AS fk_column_name,                      
     OBJECT_NAME(constid) AS Name                                  
     */  
     
      @sql = @sql + 'ALTER TABLE '+ OBJECT_NAME(sf.fkeyid) 
                    +'  DROP CONSTRAINT '+ OBJECT_NAME(constid) +';' + CHAR(10)
              
      
  FROM  sysforeignkeys sf 
     INNER JOIN sysobjects so ON sf.fkeyid = so.id                             
     
  WHERE (so.xtype = 'U') 
      and ( 
           (OBJECT_NAME(sf.rkeyid)= @TABLE_NAME)
           or 
           (OBJECT_NAME(sf.fkeyid)= @TABLE_NAME)
          )

--------------------------------------------

 print @sql   
 exec (@sql)        

--------------------------------------------
   
 set @sql= 'DROP TABLE '+ @TABLE_NAME 
 print @sql   
 exec (@sql)        

end
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Index_Exists]') AND xtype IN (N'FN', N'IF', N'TF'))
  DROP FUNCTION [dbo].[_Index_Exists]
GO



--------------------------------------------------------------------------------
CREATE FUNCTION dbo._Index_Exists(@TableName varchar(100), @IndexName varchar(100))
--------------------------------------------------------------------------------
RETURNS bit AS
BEGIN
 -- IF (OBJECT_ID('sys.indexes') is not null) 
--    IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(@TableName) AND name = @IndexName)
  --    RETURN 1;  


  IF (OBJECT_ID('sysindexes') is not null) 
   IF EXISTS (SELECT * FROM sysindexes WHERE name = @IndexName)  --object_id = OBJECT_ID(@TableName) AND 
    RETURN 1;  

  RETURN 0;
END
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Rename_Index]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Rename_Index]
GO



----------------------------------------------------
CREATE PROCEDURE dbo._Rename_Index
---------------------------------------------------
(
  @TABLE_NAME  VARCHAR(100),
  
  @INDEX_NAME  VARCHAR(100),
  @INDEX_NAME_NEW VARCHAR(100)  
)
AS
BEGIN
  DECLARE 
    @exist_src  bit,
    @exist_dest bit,
    @s VARCHAR(1000);


if @TABLE_NAME is null 
BEGIN
 set @TABLE_NAME='CalcMap'
 
 set @INDEX_NAME ='PK_CalcMaps'
 set @INDEX_NAME_NEW ='PK_CalcMap'
  
END 



  if dbo._Index_Exists (@TABLE_NAME, @INDEX_NAME)=0
    return -1
   
  if dbo._Index_Exists (@TABLE_NAME, @INDEX_NAME_NEW)=1
    return -2


/*
  IF (OBJECT_ID(@TABLE_NAME) is null)  
    return -1

  IF (OBJECT_ID(@INDEX_NAME) is null)
    return -2
*/  
  

  
  
  IF (OBJECT_ID(@INDEX_NAME_NEW) is null)
  begin 
      set @s = 'exec sp_RENAME '''+ @TABLE_NAME +'.'+ @INDEX_NAME +''', '''+ @INDEX_NAME_NEW + ''''
      print @s
      exec(@s)  
  end else

  begin 
     set @s='DROP INDEX '+ @TABLE_NAME +'.'+ @INDEX_NAME 
     print @s
     exec(@s)  
  end 
  

end
GO

--
-- Dropping stored procedure _UpdateAllViews: 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_UpdateAllViews]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_UpdateAllViews]
GO




------------------------------------------------------------------
CREATE PROCEDURE dbo._UpdateAllViews
------------------------------------------------------------------

AS
BEGIN

  
  DECLARE 
    @name varchar(200),
    @sql nvarchar(100);
  
  DECLARE 
    @temp table (name sysname);
     


INSERT INTO @temp
	select name  	  
	from sysobjects WHERE (type = 'V') 



  WHILE exists(SELECT * FROM @temp)
  BEGIN
    SELECT top 1 @name=name FROM @temp
    delete from @temp where name=@name
  
    print @name
    exec sp_refreshview @name

  END
 

END
GO
