
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Drop]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Drop]
GO




------------------------------------------------------------------
CREATE PROCEDURE dbo._Drop
------------------------------------------------------------------
(
  @OBJECT_NAME VARCHAR(50),
  @NAME  VARCHAR(100)
)
AS
BEGIN
  declare
   @s varchar(1000);


  if @OBJECT_NAME = 'view'
    if object_id(@NAME) is not null  exec ('drop view '+ @NAME) 
  
  if @OBJECT_NAME = 'Procedure'
    if object_id(@NAME) is not null  exec ('drop Procedure '+ @NAME) 

  if @OBJECT_NAME = 'function'
    if object_id(@NAME) is not null  exec ('drop function '+ @NAME) 
  

  ---------------------------------------------
  if @OBJECT_NAME = 'trigger'
  ---------------------------------------------
  begin
    if OBJECTPROPERTY(OBJECT_ID(@NAME), 'IsTrigger')=1
    begin
      set @s='DROP TRIGGER '+ @NAME 
      print @s
      exec(@s)
    end       

  end

 
end
GO


