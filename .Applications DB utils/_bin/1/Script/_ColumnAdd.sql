
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnAdd]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnAdd]
GO




------------------------------------------------------------------
CREATE PROCEDURE dbo._ColumnAdd
------------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(100),
  @COLUMN_TYPE VARCHAR(50),
  @DEFAULT VARCHAR(50)=null
  --, 
--  @ISNULL VARCHAR(50)=null 
)
AS
BEGIN
 declare 
   @s   varchar(1000),
  -- @null  varchar(50),
   @def varchar(50)
  
  set @def=''
  if IsNull(@DEFAULT,'') <> ''
    set @def = ' DEFAULT (' + @DEFAULT + ') ' 

/*
  set @null=''
 if ISNULL(@ISNULL,'') = ''
    set @null = ' ' 
  else
    set @null = ' NOT NULL' 
*/

  IF OBJECT_ID(@TABLE_NAME) IS NULL
  begin 
    print 'OBJECT_ID(@TableName) is null):' + @TABLE_NAME
    RETURN -1;
  end



  ------------------------
  if dbo._ColumnExists(@TABLE_NAME, @COLUMN_NAME)=0
  ---------------------------
  begin  
    set @s = 'ALTER TABLE '+ @TABLE_NAME +' ADD   '+ @COLUMN_NAME + ' ' + @COLUMN_TYPE  +' '+ @def  -- + @null
    print @s
    exec (@s)
    

 --   set @s = 'ALTER TABLE '+ @TABLE_NAME +' ALTER COLUMN   '+ @COLUMN_NAME + ' ' + @COLUMN_TYPE  + @null
  --  print @s
   -- exec (@s)

    
    print @@rowcount
  end else 
    print 'column exists: ' + @TABLE_NAME +' - '+ @COLUMN_NAME 
  

end
GO
