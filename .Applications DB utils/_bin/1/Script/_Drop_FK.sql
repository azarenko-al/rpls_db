

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Drop_FK]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Drop_FK]
GO



------------------------------------------------------------------
CREATE PROCEDURE dbo._Drop_FK
------------------------------------------------------------------
(
  @NAME  VARCHAR(100)
    
)
as
BEGIN

DECLARE
  @sql_drop VARCHAR(8000) 

/*

if @TABLE_NAME is null 
BEGIN
 set @TABLE_NAME='LinkFreqPlan'
 set @COLUMN_NAME ='folder_id'
  
END */




  select 
      object_name(constid) NAME,       
      object_name(fkeyid)  FK_Table_name 

  INTO #temp      
      
   from sysforeignkeys s
     inner join syscolumns c1 on ( s.fkeyid = c1.id and s.fkey = c1.colid )
     inner join syscolumns c2 on ( s.rkeyid = c2.id and s.rkey = c2.colid )
     
  WHERE 
    object_name(constid)=@NAME         
    
        
 -- SELECT * FROM #temp 
     
     
  set @sql_drop=''
     
  SELECT 
     --*  
   
    @sql_drop=@sql_drop + 
          'ALTER TABLE '+ FK_Table_name + ' DROP CONSTRAINT ' +  NAME + ';'+ CHAR(10)

  FROM #temp  
 
  
  print @sql_drop  
  exec (@sql_drop)

 
 
 -- begin transaction

 -- rollback transaction

END
GO


