
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnAddCalculated]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnAddCalculated]
GO



------------------------------------------------------------------
CREATE PROCEDURE dbo._ColumnAddCalculated
------------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(100),
  @DEFAULT     VARCHAR(50) 
)
AS
BEGIN
 declare 
   @s   varchar(1000);
   

  exec _ColumnDrop @TABLE_NAME, @COLUMN_NAME


  set @s = 'ALTER TABLE '+ @TABLE_NAME +' ADD   '+ @COLUMN_NAME + ' as ' + @DEFAULT 
  print @s
  exec (@s)
      
  print @@rowcount


end
GO
