
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnAlter]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnAlter]
GO





------------------------------------------------------------------
CREATE PROCEDURE dbo._ColumnAlter
------------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(100),
  @COLUMN_TYPE VARCHAR(50),
  @DEFAULT     VARCHAR(100)=null -- ,
--  @ISNULL      VARCHAR(50)=null

)
AS
BEGIN
-- begin transaction


/*

  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME='AntennaType'
    set @COLUMN_NAME='gain'
    set @COLUMN_TYPE='float'
    set @DEFAULT='(0)'
    set @ISNULL=NULL
    
    
  END */

/*
  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME ='lib_Radiation_class'
    set @COLUMN_NAME='name'  
    set @COLUMN_TYPE='varchar(90)'
  END
 */


  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME ='Link'
    set @COLUMN_NAME='NAME'  
    set @COLUMN_TYPE='varchar(200)'
    set @DEFAULT=''
  END
 
 
--  set @ISNULL=ISNULL(@ISNULL,'')

  if @DEFAULT='' set @DEFAULT=NULL

 

  if dbo._ColumnExists(@TABLE_NAME, @COLUMN_NAME)=0
  BEGIN
    print 'COLUMN_NAME not EXISTS'
    RETURN -1    
  END  


  DECLARE
    @FK_sql_create VARCHAR(8000), 
    @FK_sql_drop   VARCHAR(8000),  


    @DF_sql_create VARCHAR(8000), 
    @DF_sql_drop   VARCHAR(8000),  

    @Index_sql_create VARCHAR(8000), 
    @Index_sql_drop   VARCHAR(8000)  



  declare 
    @is_nullable varchar(50),
    @column_default varchar(100),
    @isnull varchar(10),
    @s   varchar(1000) 
 

/*

  ------------------------
 -- if @COLUMN_TYPE='sysname'
  if @DEFAULT is not null   
  ---------------------------
 begin

    set @s ='ALTER TABLE '+@TABLE_NAME+' DISABLE TRIGGER all'
    print @s
    exec (@s)
 
    set @s ='UPDATE  '+ @TABLE_NAME +' set '+ @COLUMN_NAME +'='+ @DEFAULT  +' WHERE ' + @COLUMN_NAME  +' IS NULL '
    print @s
    exec (@s)
    
    set @s ='ALTER TABLE '+@TABLE_NAME+' ENABLE TRIGGER all'
    print @s
    exec (@s)
    
  end

*/


  SELECT --@name=name,  
  		@Column_default = Column_default,
  		@is_nullable    = is_nullable
        
  	FROM 
      INFORMATION_SCHEMA.Columns 
  	WHERE 
       TABLE_NAME = @TABLE_NAME AND COLUMN_NAME = @COLUMN_NAME


  exec _Column_FK_Get  @TABLE_NAME, @COLUMN_NAME, @FK_sql_create output, @FK_sql_drop output
  exec _Column_DF_Get  @TABLE_NAME, @COLUMN_NAME, @DF_sql_create output, @DF_sql_drop output
  exec _Column_Index_Get  @TABLE_NAME, @COLUMN_NAME, @Index_sql_create output, @Index_sql_drop output




/*
print @FK_sql_create
print @FK_sql_drop


print @DF_sql_create
print @DF_sql_drop

print @Index_sql_create
print @Index_sql_drop
*/


   exec (@FK_sql_drop)
   exec (@Index_sql_drop)
   exec (@DF_sql_drop)


  if @is_nullable = 'no' 
     set @isnull=' NOT NULL'    
  else 
    set @isnull=' NULL'



  set @s ='ALTER TABLE '+ @TABLE_NAME +' ALTER COLUMN  '
          + @COLUMN_NAME + ' ' + @COLUMN_TYPE + @isnull 
          -- +' '+  @ISNULL
  print @s
  exec (@s)
 

/*
print @FK_sql_drop
print @DF_sql_drop
print @Index_sql_drop

*/

print @FK_sql_create
print @DF_sql_create
print @Index_sql_create


 exec (@Index_sql_create)
 exec (@FK_sql_create)
 exec (@DF_sql_create)



  --rollback transaction
  
end

go


