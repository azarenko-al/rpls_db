
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Drop_table]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Drop_table]
GO



------------------------------------------------------------------
CREATE PROCEDURE dbo._Drop_table
------------------------------------------------------------------
(
  @TABLE_NAME VARCHAR(50)
)
AS
BEGIN
  declare
   @fk_name varchar(100),
   @fk_table_name varchar(100),
   @sql varchar(4000);



  if OBJECT_ID(@TABLE_NAME) is null 
    return -1

--------------------------------------------
  set @sql =''

    
  SELECT   
/*                                            
     OBJECT_NAME(sf.rkeyid)    AS PK_TABLE_NAME,                           
     col_name(sf.rkeyid, rkey) AS pk_column_name,                      
     OBJECT_NAME(sf.fkeyid)    AS fk_table_name,                          
     col_name(sf.fkeyid, fkey) AS fk_column_name,                      
     OBJECT_NAME(constid) AS Name                                  
     */  
     
      @sql = @sql + 'ALTER TABLE '+ OBJECT_NAME(sf.fkeyid) 
                    +'  DROP CONSTRAINT '+ OBJECT_NAME(constid) +';' + CHAR(10)
              
      
  FROM  sysforeignkeys sf 
     INNER JOIN sysobjects so ON sf.fkeyid = so.id                             
     
  WHERE (so.xtype = 'U') 
      and ( 
           (OBJECT_NAME(sf.rkeyid)= @TABLE_NAME)
           or 
           (OBJECT_NAME(sf.fkeyid)= @TABLE_NAME)
          )

--------------------------------------------

 print @sql   
 exec (@sql)        

--------------------------------------------
   
 set @sql= 'DROP TABLE '+ @TABLE_NAME 
 print @sql   
 exec (@sql)        

end
GO
