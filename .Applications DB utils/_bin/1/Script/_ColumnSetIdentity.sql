IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnSetIdentity]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnSetIdentity]
GO



-------------------------------------------------------------
CREATE PROCEDURE [dbo]._ColumnSetIdentity
--------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(50),
  @FLAG        VARCHAR(50)
)
AS
BEGIN
 -- return -1



  IF @TABLE_NAME is null 
  BEGIN
    set @TABLE_NAME  = '_objects'
    
    set @TABLE_NAME  = 'lib_VENDOR_Equipment'  
      
    set @COLUMN_NAME = 'id'
    set @FLAG='ON' 
    
  END 


  IF OBJECT_ID(@TABLE_NAME) is NULL
    RETURN -1

  DECLARE
  	@column_names VARCHAR(8000),
  
    @sql_insert VARCHAR(8000), 
    
    @FK_sql_create VARCHAR(8000), 
    @FK_sql_drop   VARCHAR(8000), 
 
    @Index_sql_create VARCHAR(8000), 
    @Index_sql_drop   VARCHAR(8000)  
   


  declare 
    @Column_default varchar(100),
    @IS_NULLABLE   varchar(3),
    @data_type     varchar(50),
    
    @TABLE_NAME_temp  VARCHAR(50),
          
    @s varchar(500), 
    @sql varchar(500) 



  set @column_names=''
   

  set @TABLE_NAME_temp = @TABLE_NAME + '___'
 
 
 if object_id (@TABLE_NAME_temp) is not null 
   exec('drop table '+ @TABLE_NAME_temp)
 
 

  SELECT  @column_names=@column_names + ',' + column_name 
    FROM [INFORMATION_SCHEMA].[COLUMNS]
    WHERE (TABLE_NAME=@TABLE_NAME) 
    
  set @column_names = SUBSTRING(@column_names, 2, LEN(@column_names))
  

  
--print @column_names    
    

--begin tran
        
 

  exec _Column_FK_Get     @TABLE_NAME, @COLUMN_NAME, @FK_sql_create output,    @FK_sql_drop output
  exec _Column_Index_Get  @TABLE_NAME, @COLUMN_NAME, @Index_sql_create output, @Index_sql_drop output


print @FK_sql_drop
print @Index_sql_drop

  exec (@FK_sql_drop)
  exec (@Index_sql_drop)


  exec ( 'select * into '+ @TABLE_NAME_temp + ' FROM '+ @TABLE_NAME )
  exec ( 'DELETE FROM '+ @TABLE_NAME )
 

  if dbo._ColumnExists(@TABLE_NAME, @COLUMN_NAME)=1
  BEGIN
    set @sql= 'ALTER TABLE '+ @TABLE_NAME +' drop column  ' +  @COLUMN_NAME 
    print @sql
    exec (@sql)
  END  



  if @FLAG = 'OFF'
    set @sql= 'ALTER TABLE '+ @TABLE_NAME +' add  ' +  @COLUMN_NAME +' int '
  ELSE
    set @sql= 'ALTER TABLE '+ @TABLE_NAME +' add  ' +  @COLUMN_NAME +' int IDENTITY(1,1)'

  print @sql
  exec (@sql)

  set @sql= 'ALTER TABLE '+ @TABLE_NAME +' alter column  ' +  @COLUMN_NAME +' int not null'
 
  print @sql
  exec (@sql)


  set @sql_insert = ''


  
  if @FLAG = 'ON'
  BEGIN
  --  print ( 'SET IDENTITY_INSERT '+ @TABLE_NAME +' ON') 
  --  exec  ( 'SET IDENTITY_INSERT '+ @TABLE_NAME +' ON')
    
    set @sql_insert = @sql_insert + 'SET IDENTITY_INSERT '+ @TABLE_NAME +' ON;'    
  END 

  set @sql_insert =  @sql_insert +
     'INSERt INTO '+ @TABLE_NAME +' ('+  @column_names + ') '+
     '  SELECT '+  @column_names +' FROM ' + @TABLE_NAME_temp + ';'
     
--  print @sql_insert
 -- exec (@sql_insert )


  if @FLAG = 'ON'
--    exec ( 'SET IDENTITY_INSERT '+ @TABLE_NAME +' OFF')  
  BEGIN
  --  print ( 'SET IDENTITY_INSERT '+ @TABLE_NAME +' OFF') 
   -- exec  ( 'SET IDENTITY_INSERT '+ @TABLE_NAME +' OFF')    

    set @sql_insert = @sql_insert + 'SET IDENTITY_INSERT '+ @TABLE_NAME +' OFF;'    

  END   
  
  print @sql_insert
  exec  ( @sql_insert)  
  
  
 -- exec ('DROP TABLE ' + @TABLE_NAME_temp)
  
 
print @Index_sql_create
print @FK_sql_create

  exec (@Index_sql_create)
  exec (@FK_sql_create)
    

--rollback tran


END
GO
 


