
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Rename_Index]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Rename_Index]
GO



----------------------------------------------------
CREATE PROCEDURE dbo._Rename_Index
---------------------------------------------------
(
  @TABLE_NAME  VARCHAR(100),
  
  @INDEX_NAME  VARCHAR(100),
  @INDEX_NAME_NEW VARCHAR(100)  
)
AS
BEGIN
  DECLARE 
    @exist_src  bit,
    @exist_dest bit,
    @s VARCHAR(1000);


if @TABLE_NAME is null 
BEGIN
 set @TABLE_NAME='CalcMap'
 
 set @INDEX_NAME ='PK_CalcMaps'
 set @INDEX_NAME_NEW ='PK_CalcMap'
  
END 



  if dbo._Index_Exists (@TABLE_NAME, @INDEX_NAME)=0
    return -1
   
  if dbo._Index_Exists (@TABLE_NAME, @INDEX_NAME_NEW)=1
    return -2


/*
  IF (OBJECT_ID(@TABLE_NAME) is null)  
    return -1

  IF (OBJECT_ID(@INDEX_NAME) is null)
    return -2
*/  
  

  
  
  IF (OBJECT_ID(@INDEX_NAME_NEW) is null)
  begin 
      set @s = 'exec sp_RENAME '''+ @TABLE_NAME +'.'+ @INDEX_NAME +''', '''+ @INDEX_NAME_NEW + ''''
      print @s
      exec(@s)  
  end else

  begin 
     set @s='DROP INDEX '+ @TABLE_NAME +'.'+ @INDEX_NAME 
     print @s
     exec(@s)  
  end 
  

end
GO

