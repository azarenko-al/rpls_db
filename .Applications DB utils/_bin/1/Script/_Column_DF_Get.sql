
  
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Column_DF_Get]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Column_DF_Get]
GO



---------------------------------------------------------------
CREATE PROCEDURE dbo._Column_DF_Get
---------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(50),
  
  @sql_create VARCHAR(8000) output, 
  @sql_drop   VARCHAR(8000) output
  
)
as
BEGIN

  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME ='lib_Radiation_class'
    set @COLUMN_NAME='name'  
  END
 

  set @sql_create=''
  set @sql_drop=''
 

   SELECT
   -- @DEFAULT_NAME =o.name 
   
     @sql_create ='ALTER TABLE '+ @TABLE_NAME +' ADD CONSTRAINT '+ o.name + 
              '  DEFAULT(' +  com.text  +') FOR '+ @COLUMN_NAME,
                
      @sql_drop= 'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT '+ o.name
   
   --o.name, com.text   
     FROM sysobjects o INNER JOIN                       
          syscolumns c ON o.id = c.cdefault INNER JOIN  
          syscomments com ON o.id = com.id              
     WHERE (o.xtype = 'D') 
       and (OBJECT_NAME(o.parent_obj)=@TABLE_NAME) 
       and (c.name=@COLUMN_NAME)
    
  
  return @@ROWCOUNT
  

END
GO
