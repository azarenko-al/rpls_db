
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Create_FK]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Create_FK]
GO



------------------------------------------------------------------
CREATE PROCEDURE dbo._Create_FK
------------------------------------------------------------------
(
  @NAME  VARCHAR(100),
                  
  @PK_TABLE_NAME   VARCHAR(100),
  @PK_COLUMN_NAME  VARCHAR(100),
   
  @FK_TABLE_NAME   VARCHAR(100),   
  @FK_COLUMN_NAME  VARCHAR(100),  
  
  @ON  VARCHAR(50)  =null
  
)

AS
BEGIN
  print ''
  
  
  DECLARE
     @s VARCHAR(1000);


if @NAME is null
BEGIN
  set @NAME ='FK_LinkEndType_lib_VENDOR_Equipment'
                  
  set @PK_TABLE_NAME  ='lib_VENDOR_Equipment'
  set @pk_column_name  ='id'
   
  set @fk_table_name  ='LinkEndType'
  set @fk_column_name ='vendor_equipment_id'
  
  
END 




  exec _Drop_FK @NAME --@TABLE_NAME, 

 


  set @s = 'ALTER TABLE '+ @fk_table_name +' with nocheck ADD CONSTRAINT ' + @NAME 
      +' FOREIGN KEY ('+ @FK_COLUMN_NAME + ') REFERENCES ' + @pk_table_name + ' (' + @pk_column_name + ') '
      + COALESCE(@ON,'')
           
  print @s    
  exec(@s)  
  

  set @s = 'ALTER TABLE '+ @fk_table_name +' check  CONSTRAINT ' + @NAME 
  print @s    
  exec(@s)  


  return 0
  
  /*
  
  ALTER TABLE [dbo].[AntennaType]
ADD CONSTRAINT [FK_AntennaType_Folder] FOREIGN KEY ([folder_id]) 
  REFERENCES [dbo].[Folder] ([id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO*/
  
END
GO

