

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnAddDefault]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnAddDefault]
GO



------------------------------------------------------------------
CREATE PROCEDURE dbo._ColumnAddDefault
------------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(50),
--  @CONSTRAINT_NAME VARCHAR(50),
  @DEFAULT VARCHAR(50)=null 
)
AS
BEGIN


if @TABLE_NAME is null 
BEGIN
 set @TABLE_NAME='link'
 set @COLUMN_NAME ='calc_method'
 set @DEFAULT='(3)'
  
END 

  declare 
    @CONSTRAINT_NAME_NEW varchar(100),
    @Column_default varchar(100),
    @sql varchar(100),
    @s   varchar(1000),
    @null varchar(50) 
   -- @def varchar(50)


  if dbo._ColumnExists(@TABLE_NAME, @COLUMN_NAME)=0
  BEGIN
    print 'column not exists'
    RETURN -1
  END 
  


  if @DEFAULT=''  set @DEFAULT=null


  SELECT --@name=name,  
  		@column_default=Column_default
  	FROM INFORMATION_SCHEMA.Columns 
  	WHERE TABLE_NAME = @TABLE_NAME AND COLUMN_NAME = @COLUMN_NAME


  IF @column_default = @DEFAULT
    RETURN 1



  set @CONSTRAINT_NAME_NEW = 'DF_' + @TABLE_NAME + '_' + @COLUMN_NAME    


   set @sql=''

   SELECT 
     @sql = 'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT '+ o.name 
   
   FROM sysobjects o INNER JOIN                       
        syscolumns c ON o.id = c.cdefault INNER JOIN  
        syscomments com ON o.id = com.id              
   WHERE (o.xtype = 'D') and (OBJECT_NAME(o.parent_obj)=@TABLE_NAME) and (c.name=@COLUMN_NAME)
	
   print @sql
   exec (@sql)

    
    if @DEFAULT is not null
    BEGIN
    
      set @s ='ALTER TABLE '+ @TABLE_NAME +' ADD CONSTRAINT '+ @CONSTRAINT_NAME_NEW + 
              '  DEFAULT ' + @DEFAULT  +' FOR '+ @COLUMN_NAME
      print @s

      exec (@s)      
    END
    

end
GO


