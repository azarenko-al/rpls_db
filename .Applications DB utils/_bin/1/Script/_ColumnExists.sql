
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnExists]') AND xtype IN (N'FN', N'IF', N'TF'))
  DROP FUNCTION [dbo].[_ColumnExists]
GO



--------------------------------------------------------------------------------
CREATE FUNCTION [dbo]._ColumnExists(@TableName varchar(100), @ColumnName varchar(100))
--------------------------------------------------------------------------------
RETURNS bit AS
BEGIN
 -- DECLARE @Result bit;

  IF not (OBJECT_ID(@TableName)>0)
  begin 
  -- print 'OBJECT_ID(@TableName) is null):' + @TableName
    RETURN -1;
  end

  IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.Columns 
              WHERE TABLE_NAME = @TableName 
                AND COLUMN_NAME = @ColumnName)
    RETURN 1
    --SET @Result = 1
  ELSE  
    RETURN 0
    --SET @Result = 0


  RETURN 111
--@Result;

END

GO







