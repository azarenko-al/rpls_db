
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnSetNull]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnSetNull]
GO


------------------------------------------------------------------
CREATE PROCEDURE dbo._ColumnSetNull
------------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(50),
--  @CONSTRAINT_NAME VARCHAR(50),
  
  @ISNULL VARCHAR(50)
)
AS
BEGIN
  declare 
    @Column_default varchar(100),
    @is_nullable   varchar(3),
    @data_type     varchar(50),
    
    @FK_sql_create VARCHAR(8000) , 
    @FK_sql_drop   VARCHAR(8000) ,

    @Index_sql_create VARCHAR(8000) , 
    @Index_sql_drop   VARCHAR(8000) ,
          
    @s varchar(500), 
    @sql varchar(500) 



  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME ='_db_version_'
    set @COLUMN_NAME='product_name'  
    set @ISNULL='not null'
  END


--exec _ColumnSetNull '_db_version_','product_name','NOT NULL'


set @ISNULL = IsNull(@ISNULL,'')


IF IsNull(@ISNULL,'') = 'null'
  set @is_nullable='yes'
else
  set @is_nullable='no'



--set @ISNULL = IsNull(@ISNULL,

-- COLUMN_DEFAULT

set @sql=''
set @data_type=''

  SELECT 
    @column_default = column_default,
  
    @data_type = DATA_TYPE +
         
         case 
           when character_maximum_length>0 then
             '('+ cast(character_maximum_length as VARCHAR(50)) +') '
           else ' ' end ,
  
  
   @sql =  
   'ALTER TABLE '+ TABLE_NAME +' ALTER COLUMN '+ COLUMN_NAME +' '
   

    
 FROM [INFORMATION_SCHEMA].[COLUMNS]
 where TABLE_NAME = @TABLE_NAME 
     and COLUMN_NAME= @COLUMN_NAME
     and IS_NULLABLE <> @is_nullable


if @sql=''
BEGIN
  print 'no data need to be changed'
  return -1  
END  


set @data_type=replace(@data_type,'nvarchar(128)','sysname')

if @data_type='sysname'
BEGIN
  print 'sysname is always not null'
  return -2    
END 


set @sql=@sql + @data_type +  ' ' + @ISNULL





--------------------------------------------------
 -- if @COLUMN_TYPE='sysname'
  if (@ISNULL = 'not null') and (@column_default is not null)   
--------------------------------------------------
 begin

    set @s ='ALTER TABLE '+@TABLE_NAME+' DISABLE TRIGGER all'
    print @s
    exec (@s)
 
    set @s ='UPDATE  '+ @TABLE_NAME +' set '+ @COLUMN_NAME +'='+ @column_default  +' WHERE ' + @COLUMN_NAME  +' IS NULL '
    print @s
    exec (@s)
    
    set @s ='ALTER TABLE '+@TABLE_NAME+' ENABLE TRIGGER all'
    print @s
    exec (@s)
    
  end


 -- if (@ISNULL = 'not null') 
  --  IF EXISTS(SELECT * FROM ) 


 
 
exec _Column_Index_Get  @TABLE_NAME, @COLUMN_NAME,  @Index_sql_create output, @Index_sql_drop output


  print @Index_sql_drop


--begin tran


  if @Index_sql_drop<>'' exec (@Index_sql_drop)
 

  print @sql
  exec (@sql)
  

  if @Index_sql_create<>'' exec (@Index_sql_create)

 
-- rollback
  


end




GO

