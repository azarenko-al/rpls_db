-- SQL Manager 2011 for SQL Server 3.7.0.2
-- ---------------------------------------
-- Oino         : seRVER
-- Aaca aaiiuo  : onega_test_rostelecom_1
-- Aa?ney       : Microsoft SQL Server  8.00.194


--
-- Dropping user-defined function _ColumnExists : 
--

 


--
-- Dropping stored procedure _UpdateAllViews: 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_UpdateAllViews]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_UpdateAllViews]
GO




IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Column_DF_Get]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Column_DF_Get]
GO

--
-- Dropping stored procedure _Column_FK_Get : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Column_FK_Get]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Column_FK_Get]
GO

--
-- Dropping stored procedure _Column_Index_Get : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Column_Index_Get]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Column_Index_Get]
GO

--
-- Dropping stored procedure _ColumnAdd : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnAdd]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnAdd]
GO

--
-- Dropping stored procedure _ColumnAddCalculated : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnAddCalculated]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnAddCalculated]
GO

--
-- Dropping stored procedure _ColumnAddDefault : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnAddDefault]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnAddDefault]
GO

--
-- Dropping stored procedure _ColumnAlter : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnAlter]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnAlter]
GO

--
-- Dropping stored procedure _ColumnDrop : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnDrop]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnDrop]
GO

--
-- Dropping stored procedure _ColumnRename : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnRename]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnRename]
GO

--
-- Dropping stored procedure _ColumnSetIdentity : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnSetIdentity]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnSetIdentity]
GO

--
-- Dropping stored procedure _ColumnSetNull : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnSetNull]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnSetNull]
GO

--
-- Dropping stored procedure _Create_FK : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Create_FK]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Create_FK]
GO

--
-- Dropping stored procedure _Drop : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Drop]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Drop]
GO

--
-- Dropping stored procedure _Drop_FK : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Drop_FK]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Drop_FK]
GO

--
-- Dropping stored procedure _Drop_index : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Drop_index]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Drop_index]
GO

--
-- Dropping stored procedure _Drop_table : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Drop_table]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Drop_table]
GO

--
-- Dropping stored procedure _Rename_Index : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Rename_Index]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Rename_Index]
GO

--
-- Definition for user-defined function _ColumnExists : 
--
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


--
-- Definition for stored procedure _Column_DF_Get : 
--
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------------------
CREATE PROCEDURE dbo._Column_DF_Get
---------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(50),
  
  @sql_create VARCHAR(8000) output, 
  @sql_drop   VARCHAR(8000) output
  
)
as
BEGIN

  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME ='lib_Radiation_class'
    set @COLUMN_NAME='name'  
  END
 

  set @sql_create=''
  set @sql_drop=''
 

   SELECT
   -- @DEFAULT_NAME =o.name 
   
     @sql_create ='ALTER TABLE '+ @TABLE_NAME +' ADD CONSTRAINT '+ o.name + 
              '  DEFAULT(' +  com.text  +') FOR '+ @COLUMN_NAME,
                
      @sql_drop= 'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT '+ o.name
   
   --o.name, com.text   
     FROM sysobjects o INNER JOIN                       
          syscolumns c ON o.id = c.cdefault INNER JOIN  
          syscomments com ON o.id = com.id              
     WHERE (o.xtype = 'D') 
       and (OBJECT_NAME(o.parent_obj)=@TABLE_NAME) 
       and (c.name=@COLUMN_NAME)
    
  
  return @@ROWCOUNT
  

END
GO

--
-- Definition for stored procedure _Column_FK_Get : 
--
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
