


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnRename]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnRename]
GO


----------------------------------------------------
CREATE PROCEDURE dbo._ColumnRename
---------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(50),
  @COLUMN_NAME_NEW VARCHAR(50)
)
AS
BEGIN
  DECLARE 
    @s VARCHAR(1000);

  DECLARE
    @sql_create VARCHAR(8000), 
    @sql_drop   VARCHAR(8000)

 if @TABLE_NAME is null
 begin
   set @TABLE_NAME = 'linkend';
   set @COLUMN_NAME = 'tx_freq';
   set @COLUMN_NAME_NEW = 'tx_freq1';
   
 end



  IF not (OBJECT_ID(@TABLE_NAME)>0)
  begin 
   print 'OBJECT_ID(@TableName) is null):' + @TABLE_NAME
  --  RETURN -1;
  end


  if not ((dbo._ColumnExists(@TABLE_NAME, @COLUMN_NAME)=1 ) and
         (dbo._ColumnExists(@TABLE_NAME, @COLUMN_NAME_NEW)=0 ))         
    RETURN;
         
--  begin

 set @sql_create=''
 set @sql_drop=''

 select 
	  @sql_drop   = @sql_drop + sql_drop,
	  @sql_create = @sql_create + 
                       replace(sql_create, '['+@COLUMN_NAME+']', '['+@COLUMN_NAME_NEW+']')
  
  FROM _CHECK_CONSTRAINTS_for_column(@TABLE_NAME,@COLUMN_NAME)

  print @sql_drop  
  exec ( @sql_drop )
  


    set @s = 'exec sp_RENAME '''+ @TABLE_NAME +'.'+ @COLUMN_NAME +''', '''+ @COLUMN_NAME_NEW +''', ''COLUMN'''

print @s

      exec('exec sp_RENAME '''+ @TABLE_NAME +'.'+ @COLUMN_NAME +''', '''+ @COLUMN_NAME_NEW +''', ''COLUMN''')


  print @sql_create  
  exec ( @sql_create )
  

--  end

end


go


               
