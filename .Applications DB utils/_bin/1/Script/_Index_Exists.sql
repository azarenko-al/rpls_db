

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Index_Exists]') AND xtype IN (N'FN', N'IF', N'TF'))
  DROP FUNCTION [dbo].[_Index_Exists]
GO



--------------------------------------------------------------------------------
CREATE FUNCTION dbo._Index_Exists(@TableName varchar(100), @IndexName varchar(100))
--------------------------------------------------------------------------------
RETURNS bit AS
BEGIN
 -- IF (OBJECT_ID('sys.indexes') is not null) 
--    IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(@TableName) AND name = @IndexName)
  --    RETURN 1;  


  IF (OBJECT_ID('sysindexes') is not null) 
   IF EXISTS (SELECT * FROM sysindexes WHERE name = @IndexName)  --object_id = OBJECT_ID(@TableName) AND 
    RETURN 1;  

  RETURN 0;
END
GO

