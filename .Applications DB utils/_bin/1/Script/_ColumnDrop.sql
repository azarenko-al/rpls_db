IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_ColumnDrop]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_ColumnDrop]
GO



------------------------------------------------------------------
CREATE PROCEDURE dbo._ColumnDrop
------------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(50)
)
AS
BEGIN
  DECLARE 
    @sql varchar(4000);

  DECLARE
    @FK_sql_create VARCHAR(8000), 
    @FK_sql_drop   VARCHAR(8000),  


    @DF_sql_create VARCHAR(8000), 
    @DF_sql_drop   VARCHAR(8000),  

    @Index_sql_create VARCHAR(8000), 
    @Index_sql_drop   VARCHAR(8000)  




if @TABLE_NAME is null 
BEGIN
 set @TABLE_NAME='linkend'
 set @COLUMN_NAME ='folder_id'
  
END 


  if dbo._ColumnExists(@TABLE_NAME, @COLUMN_NAME)=0
  BEGIN
    print 'COLUMN_NAME not EXISTS'
    RETURN -1    
  END  



    
  ------------------------
  -- _Drop_Check_Constraints_for_Column
  ------------------------
  set @sql=''

 SELECT 
  
   @sql=@sql + 'ALTER TABLE '+ @TABLE_NAME +' DROP CONSTRAINT  '+ c.CONSTRAINT_NAME +';'
    
 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS c
  left join INFORMATION_SCHEMA.CHECK_CONSTRAINTS cc
    ON cc.CONSTRAINT_NAME = c.CONSTRAINT_NAME
    
 WHERE c.CONSTRAINT_TYPE='CHECK'
      and c.TABLE_NAME  = @TABLE_NAME 
      and cc.check_clause like '%[[]'+ @COLUMN_NAME  +']%'


  print @sql  
  exec ( @sql )


-------------------------------------------------


  exec _Column_FK_Get  @TABLE_NAME, @COLUMN_NAME, @FK_sql_create output, @FK_sql_drop output
  exec _Column_DF_Get  @TABLE_NAME, @COLUMN_NAME, @DF_sql_create output, @DF_sql_drop output
  exec _Column_Index_Get  @TABLE_NAME, @COLUMN_NAME, @Index_sql_create output, @Index_sql_drop output



print @FK_sql_drop
print @DF_sql_drop
print @Index_sql_drop

  if @FK_sql_drop   <>'' exec (@FK_sql_drop)
  if @Index_sql_drop<>'' exec (@Index_sql_drop)
  if @DF_sql_drop   <>'' exec (@DF_sql_drop)


   
 set @sql= 'ALTER TABLE '+ @TABLE_NAME +' DROP COLUMN ['+ @COLUMN_NAME + ']'
 print @sql    
   
   
 exec (@sql)
        
end
GO

