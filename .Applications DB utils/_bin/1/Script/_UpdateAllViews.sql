--
-- Dropping stored procedure _UpdateAllViews: 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_UpdateAllViews]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_UpdateAllViews]
GO




------------------------------------------------------------------
CREATE PROCEDURE dbo._UpdateAllViews
------------------------------------------------------------------

AS
BEGIN

  
  DECLARE 
    @name varchar(200),
    @sql nvarchar(100);
  
  DECLARE 
    @temp table (name sysname);
     


INSERT INTO @temp
	select name  	  
	from sysobjects WHERE (type = 'V') 



  WHILE exists(SELECT * FROM @temp)
  BEGIN
    SELECT top 1 @name=name FROM @temp
    delete from @temp where name=@name
  
    print @name
    exec sp_refreshview @name

  END
 

END
GO
