
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[_Column_FK_Get]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[_Column_FK_Get]
GO


-------------------------------------------------------
CREATE PROCEDURE dbo._Column_FK_Get
-------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(50),
  
  @sql_create VARCHAR(8000) output, 
  @sql_drop   VARCHAR(8000) output
  
)
as
BEGIN


if @TABLE_NAME is null 
BEGIN
 set @TABLE_NAME='Passive_Component'
 set @COLUMN_NAME ='channels'

 set @TABLE_NAME='link'
 set @COLUMN_NAME ='id'


--// set @TABLE_NAME='LinkFreqPlan'
--// set @COLUMN_NAME ='folder_id'
  
END 

/*
  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME ='lib_Radiation_class'
    set @COLUMN_NAME='name'  
  END
*/ 
  
  /*
  DECLARE
    @sql  VARCHAR(8000), 
    @sql_drop VARCHAR(8000); 
    
  */

  select 
      object_name(constid) NAME, 
      
      object_name(fkeyid)  	FK_Table_name, 
      c1.name 			  		 	FK_Column_name,
      
      object_name(rkeyid)  	PK_Table_name, 
      c2.name 			   			PK_Column_name,
      
       ObjectProperty(constid, 'CnstIsUpdateCascade') as IsUpdateCascade,   
       ObjectProperty(constid, 'CnstIsDeleteCascade') as IsDeleteCascade  
   
  INTO #temp      
      
   from sysforeignkeys s
     inner join syscolumns c1 on ( s.fkeyid = c1.id and s.fkey = c1.colid )
     inner join syscolumns c2 on ( s.rkeyid = c2.id and s.rkey = c2.colid )
     
  WHERE 
    (object_name(fkeyid)=@TABLE_NAME and c1.name=@COLUMN_NAME)
    or
    (object_name(rkeyid)=@TABLE_NAME and c2.name=@COLUMN_NAME) 
    
     

--select * from   #temp  

    
     
     
  set @sql_create=''
  set @sql_drop=''
     
  SELECT 
     --*
  
    @sql_create=@sql_create + 
          'ALTER TABLE '+ FK_Table_name + ' ADD CONSTRAINT ' +  NAME
           + ' FOREIGN KEY (' + FK_Column_name + ') REFERENCES '+ PK_Table_name
           + ' (' + PK_Column_name + ') ' + 
           
           CASE  
              when IsUpdateCascade=1 then  'ON UPDATE CASCADE '
           END +

           CASE 
              when IsDeleteCascade=1 then  'ON DELETE CASCADE '
           END             
           
           + ';'+ CHAR(10)
  
,
    @sql_drop=@sql_drop + 
          'ALTER TABLE '+ FK_Table_name + ' DROP CONSTRAINT ' +  NAME + ';'+ CHAR(10)

  FROM #temp  


 /*
  
  ALTER TABLE [dbo].[AntennaType]
ADD CONSTRAINT [FK_AntennaType_Folder] FOREIGN KEY ([folder_id]) 
  REFERENCES [dbo].[Folder] ([id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO*/


print @sql_create
print @sql_drop
 
END

go





   
