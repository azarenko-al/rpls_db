select top 9999 * 

from 
(
    select 
     schema_name(schema_id) as schema_name,
     *
    
     from sys.tables
     
    where
     -- (table_type='base table') 
          (name not like '[_][_]%')         
      and (name not like '%[_]history')  

      and (name <> 'sysdiagrams')  
      AND (name <> 'dtproperties') 
      and name <> 'msfavorites'

      and (name not like '%[_][_]')      
          
) M
   
where schema_name <> 'service'         
order by schema_name,  name
