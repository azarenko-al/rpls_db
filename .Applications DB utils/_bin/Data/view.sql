  select 
  
 o.object_id,

  o.NAME,
  o.create_date,
  o.modify_date,
  
  schema_name (o.schema_id) as schema_name,
  
  definition
  
  
from sys.objects     o
  join sys.sql_modules m on m.object_id = o.object_id


where
  
   o.type      = 'V'
   
   
   and o.NAME not LIKE '%[_][_]'
   and o.NAME not LIKE '[_][_]%'

   and o.NAME not like '%11'

   and schema_name (o.schema_id) not LIKE 'sync%'

   and schema_name (o.schema_id) not LIKE 'service'


--   and schema_name (o.schema_id) <> 'Security'
