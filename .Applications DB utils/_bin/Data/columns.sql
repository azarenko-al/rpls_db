
select 


col.*  , 

sc.Is_Identity,
sc.Is_computed,


cc.definition as computed_definition,



case
  when sc.Is_computed=1 then '[' + column_name + '] as ' + cc.definition
  else '[' + column_name + '] ' + data_type + 
       IIF( IsNull( col.character_maximum_length ,0) between 1 and 10000, '('+  cast( col.character_maximum_length as varchar(10)) +')' , '')
       + IIF (col.IS_NULLABLE = 'NO', ' not null', '')
       
       + IIF (col.column_default is not null, ' DEFAULT '+ col.column_default, '')
       
       + IIF (sc.Is_Identity = 1, ' IDENTITY(1,1) ', '')
       
end  
as sql_create  
   


 from  INFORMATION_SCHEMA.COLUMNS  col

INNER JOIN sys.columns AS sc ON sc.object_id = object_id(table_schema + '.' + table_name) AND sc.NAME = col.COLUMN_NAME

inner join sys.objects obj on obj.object_Id=sc.object_Id 

left join  sys.computed_columns cc on cc.name = col.COLUMN_NAME and cc.object_id = obj.object_Id

where obj.[type]='u'

  and col.COLUMN_NAME not like'[_]%'
  and col.COLUMN_NAME not like'%[_][_]'
  

order by table_name, ordinal_position  
