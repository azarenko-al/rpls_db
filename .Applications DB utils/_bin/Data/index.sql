
select *,


replace (replace (replace (
case 
  when is_primary_key=1 	  then 'ALTER TABLE :TABLE ADD CONSTRAINT :name UNIQUE NONCLUSTERED (:columns) ON [PRIMARY] '  
  when is_unique=1 			  then 'CREATE UNIQUE INDEX :name ON :TABLE (:columns) ON [PRIMARY]  '  
  when is_unique_constraint=1 then 'ALTER TABLE :TABLE ADD CONSTRAINT :name UNIQUE NONCLUSTERED (:columns) ON [PRIMARY] ' 
                              else 'CREATE INDEX :name ON :TABLE (:columns) ON [PRIMARY] '
end ,
 ':TABLE', schema_name+'.'+Table_Name),
 ':name', Index_Name),
 ':columns', IndexColumnsNames)

as sql_create,

replace (replace (replace (
case 
  when is_primary_key=1 	  then ' ALTER TABLE :TABLE DROP CONSTRAINT :name'  
  when is_unique=1 			  then ' ALTER TABLE :TABLE DROP CONSTRAINT :name' 
  when is_unique_constraint=1 then ' ALTER TABLE :TABLE DROP CONSTRAINT :name' 
                              else ' DROP INDEX  :name ON :TABLE '
end ,
 ':TABLE', schema_name+'.'+Table_Name),
 ':name', Index_Name),
 ':columns', IndexColumnsNames)

as sql_drop
  

from 

(


  SELECT 
  
      INX.object_id,

      INX.[name] AS [Index_Name],
      TBL.[name] AS [Table_Name],
      schema_name(TBL.schema_id) as schema_name,

      INX.is_primary_key,
      INX.is_unique,
      INX.is_unique_constraint,
      
      DS1.[IndexColumnsNames]
   --   DS2.[IncludedColumnsNames]
      
      
      
FROM [sys].[indexes] INX
INNER JOIN [sys].[tables] TBL
    ON INX.[object_id] = TBL.[object_id]
    
    
CROSS APPLY 
(
    SELECT STUFF
    (
        (
            SELECT ', [' + CLS.[name] + ']'
            FROM [sys].[index_columns] INXCLS
            INNER JOIN [sys].[columns] CLS 
                ON INXCLS.[object_id] = CLS.[object_id] 
                AND INXCLS.[column_id] = CLS.[column_id]
            WHERE INX.[object_id] = INXCLS.[object_id] 
                AND INX.[index_id] = INXCLS.[index_id]
                AND INXCLS.[is_included_column] = 0
            FOR XML PATH('')
        )
        ,1
        ,1
        ,''
    ) 
) DS1 ([IndexColumnsNames])


/*CROSS APPLY 
(
    SELECT STUFF
    (
        (
            SELECT ', [' + CLS.[name] + ']'
            FROM [sys].[index_columns] INXCLS
            INNER JOIN [sys].[columns] CLS 
                ON INXCLS.[object_id] = CLS.[object_id] 
                AND INXCLS.[column_id] = CLS.[column_id]
            WHERE INX.[object_id] = INXCLS.[object_id] 
                AND INX.[index_id] = INXCLS.[index_id]
                AND INXCLS.[is_included_column] = 1
            FOR XML PATH('')
        )
        ,1
        ,1
        ,''
    ) 
) DS2 ([IncludedColumnsNames])

*/
    where INX.[name] is not null
    
) M
