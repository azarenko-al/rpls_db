
select 
object_id,
    name, 
    schema_name,

   'CREATE TYPE '+ schema_name +'.'+ name +' AS TABLE ( '+  [sql_columns] + ')'   [sql_create] ,
   'DROP TYPE '  + schema_name +'.'+ name  [sql_drop] 

from 

(
  select 
  type_table_object_id as object_id,
 --object_id,
  
    name, 
    schema_name(schema_id) as schema_name,
      
     SUBSTRING(
          (
              SELECT ','+sql_create  AS [text()]
            --  FROM sync.view_UDT_columns 
                            
               FROM               
                  (
                   SELECT 
                      tt.name as table_name, 
                      schema_name(tt.schema_id) as schema_name,         
                      c.name + ' '+

                  case
                    when st.name='varchar' then st.name +'('+ cast(c.max_length as varchar(10)) +  ')'
                    else st.name
                  end 

                  as sql_create
                          
                    FROM sys.columns c
                      JOIN sys.table_types tt ON c.[object_id] = tt.type_table_object_id  
                      JOIN sys.types st ON st.system_type_id  = c.system_type_id
                           
                   ) S   WHERE S.table_name = M.name
                
          FOR XML PATH ('')
      ), 2, 1000) [sql_columns] 
        
 from sys.table_types M
 ) A
