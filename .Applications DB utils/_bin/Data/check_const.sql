
select * ,

 
REPLACE(replace(replace(
'ALTER TABLE :table WITH NOCHECK ADD CONSTRAINT [:name] CHECK :definition',
':table', schema_name + '.'+ table_name ),   
':definition', [definition] ),
':name', [constraint_name] )

as sql_create,   
-------------------------------


REPLACE(replace(
'ALTER TABLE :table DROP CONSTRAINT [:name]',
':table', schema_name + '.'+  table_name ),   
':name', [constraint_name] )

as sql_drop   

-----------------------------

from

(

  select 
    con.object_id,
    
    con.[name] as constraint_name,
    schema_name(t.schema_id) as schema_name, 
    
   

    
/*      SQL_ALTER_TABLE_ADD_CHECK_CONSTRAINT_no_proc =
   'ALTER TABLE [%s] WITH NOCHECK ADD CONSTRAINT [%s] CHECK %s';  //+ DEF_GO_CRLF;
*/
    
    
   -- + '.' + 
    t.[name]  as [table_name],
    
    t.create_date,
    t.modify_date,
    
    col.[name] as column_name,
    con.[definition],
    case when con.is_disabled = 0 
        then 'Active' 
        else 'Disabled' 
        end as [status]
from sys.check_constraints con
    left outer join sys.objects t
        on con.parent_object_id = t.object_id
    left outer join sys.all_columns col
        on con.parent_column_id = col.column_id
        and con.parent_object_id = col.object_id
        
) M
