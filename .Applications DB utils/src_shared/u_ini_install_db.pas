unit u_ini_install_db;


interface
uses
  SysUtils, IniFiles, Dialogs;

 // u_classes;


const
   DEF_INSTALL_DB_EXE = 'install_db.exe';

type

  // ---------------------------------------------------------------
  TIni_Install_db = class(TObject)
  // ---------------------------------------------------------------
  private

  public
    Server         : String;
    DataBase       : String;
    Login          : String;
    Password       : String;
    IsUseWinAuth   : boolean;  //win or SQL server

    IniFileName : string;

    Project_Name : string;

 //   TableName : string;
   // ObjectName : string;
  //  SelectedIDList: TIDList;

    IsAuto:   Boolean;

    procedure SaveToIni(aFileName: String);
    function LoadFromIni(aFileName: String): Boolean;
  end;


(*
function StrToExportModeType(aValue: string): TExportModeType;
function ExportModeTypeToStr(aValue: TExportModeType): string;
*)

(*const
  TEMP_INI_FILE = 'Install_db.ini';
*)

implementation

const
  DEF_MAIN='Main';


// ---------------------------------------------------------------
function TIni_Install_db.LoadFromIni(aFileName: String): Boolean;
// ---------------------------------------------------------------
var
  oIni: TMemIniFile;
  s: string;
begin
  Result:=False;

  if (aFileName='') or (not FileExists(aFileName)) then
    Exit;

  oIni:=TMemIniFile.Create (aFileName);

  with oIni do
  begin
    Server        := ReadString (DEF_MAIN, 'Server',     Server);
    DataBase      := ReadString (DEF_MAIN, 'DataBase',   DataBase);
    Login         := ReadString (DEF_MAIN, 'Login',      Login);
    Password      := ReadString (DEF_MAIN, 'Password',   Password);
    IsUseWinAuth  := ReadBool   (DEF_MAIN, 'UseWinAuth', False);

    IsAuto :=ReadBool   (DEF_MAIN, 'IsAuto', False);

    IniFileName   := ReadString (DEF_MAIN, 'IniFileName',  IniFileName);

    Project_Name   := ReadString (DEF_MAIN, 'Project_Name',  '');

  end;


  Assert(Project_Name<>'');


  // -------------------------
 // ProjectID:=oIni.ReadInteger('main', 'Project_ID',  0);

 // TableName := oIni.ReadString ('main', 'ObjectName', '');
 // TableName  := oIni.ReadString ('main', 'TableName', '');

 // MdbFileName := oIni.ReadString('main', 'MdbFileName', '');

 // s :=oIni.ReadString ('main', 'mode', '');
 // Mode :=StrToExportModeType(s);


  oIni.Free;
  // -------------------------

  Result:=True;



  //if TableName<>'' then
   // SelectedIDList.LoadIDsFromIniFile (aFileName, 'list');

end;


// ---------------------------------------------------------------
procedure TIni_Install_db.SaveToIni(aFileName: String);
// ---------------------------------------------------------------
var
  oIni: TIniFile;
  s: string;
begin
  ForceDirectories(ExtractFileDir (aFileName));

  oIni:=TIniFile.Create (aFileName);

  with oIni do
  begin
    WriteString (DEF_MAIN, 'Server',     Server);
    WriteString (DEF_MAIN, 'DataBase',   DataBase);
    WriteString (DEF_MAIN, 'Login',      Login);
    WriteString (DEF_MAIN, 'Password',   Password);
    WriteBool   (DEF_MAIN, 'UseWinAuth', IsUseWinAuth);

    WriteBool   (DEF_MAIN, 'IsAuto', IsAuto);

    WriteString (DEF_MAIN, 'IniFileName',  IniFileName);

    WriteString (DEF_MAIN, 'Project_Name',  Project_Name);


  end;

  oIni.Free;

end;


end.


