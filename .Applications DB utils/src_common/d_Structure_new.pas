unit d_Structure_new;

interface

uses
  StdCtrls, ExtCtrls,  Classes, Controls, Forms, SysUtils,
  DB, ADODB,  cxGridDBTableView, cxGridLevel, Dialogs,
  cxGrid, ComCtrls, rxPlacemnt,
  DBCtrls, Mask, cxGridCustomTableView, cxGridTableView, cxClasses,


  dm_MDB,

  u_db,

  u_func,

  cxControls, cxGridCustomView, cxSplitter, Grids, DBGrids;


type

  Tdlg_Structure_new = class(TForm)
    PageControl1: TPageControl;

    ds_CK: TDataSource;
    ds_FK: TDataSource;
    ds_Indexes: TDataSource;
    ds_Tables: TDataSource;
    ds_Triggers: TDataSource;
    ds_Views: TDataSource;
    cxGrid1DBTableView_Triggername: TcxGridDBColumn;
    cxGrid1DBTableView_Triggercontent: TcxGridDBColumn;
    cxGrid1DBTableView_indexesid: TcxGridDBColumn;
    cxGrid1DBTableView_indexesname: TcxGridDBColumn;
    cxGrid1DBTableView_indexes_is_primary_key: TcxGridDBColumn;
    cxGrid1DBTableView_indexes_is_unique_key: TcxGridDBColumn;
    cxGrid1DBTableView_indexesindex_keys: TcxGridDBColumn;
    cxGrid1DBTableView_indexescontent: TcxGridDBColumn;
    TabSheet3: TTabSheet;
    cxGrid3: TcxGrid;
    cxGridDBTableView_FK: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    cxGridDBTableView_FKid: TcxGridDBColumn;
    cxGridDBTableView_FKname: TcxGridDBColumn;
    cxGridDBTableView_FKtablename: TcxGridDBColumn;
    cxGridDBTableView_FKpk_tablename: TcxGridDBColumn;
    TabSheet4: TTabSheet;
    cxGrid_tr: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridLevel3: TcxGridLevel;
    cxGridDBTableView3tablename: TcxGridDBColumn;
    cxGridDBTableView3name: TcxGridDBColumn;
    cxGridDBTableView3content: TcxGridDBColumn;
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    btn_Cancel: TButton;
    ds_Owner: TDataSource;
    cxGrid1DBTableView1DBColumn1_Owner: TcxGridDBColumn;
    cxGridDBTableView1DBColumn1_Status: TcxGridDBColumn;
    TabSheet6: TTabSheet;
    cxGrid_SP: TcxGrid;
    cxGridDBTableView_SP: TcxGridDBTableView;
    cxGridLevel4: TcxGridLevel;
    ds_SP: TDataSource;
    cxGridDBTableView_SPname: TcxGridDBColumn;
    cxGridDBTableView_SPcontent: TcxGridDBColumn;
    cxGridDBTableView_SPstatus_id: TcxGridDBColumn;
    t_Owner: TADOTable;
    cxGrid1DBTableView1DBColumn1_xtype: TcxGridDBColumn;
    ds_TableFields: TDataSource;
    MDBConnection: TADOConnection;
    cxGrid1Level3: TcxGridLevel;
    cxGrid1DBTableView2_TableFields: TcxGridDBTableView;
    cxGrid1DBTableView2_TableFieldsDBColumn1: TcxGridDBColumn;
    cxGrid1DBTableView2_TableFieldsDBColumn3: TcxGridDBColumn;
    cxGrid1DBTableView2_TableFieldsDBColumn2: TcxGridDBColumn;
    cxGrid1DBTableView2_TableFieldsDBColumn4: TcxGridDBColumn;
    cxGridDBTableView1DBColumn1_checked: TcxGridDBColumn;
    cxGridDBTableView_SPDBColumn1_checked: TcxGridDBColumn;
    TabSheet_CK: TTabSheet;
    cxGrid6: TcxGrid;
    cxGridDBTableView5: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridLevel5: TcxGridLevel;
    cxGridDBTableView1DBColumn1_text: TcxGridDBColumn;
    TabSheet7: TTabSheet;
    cxGrid_OBJECTS: TcxGrid;
    cxGridDBTableView6: TcxGridDBTableView;
    cxGridLevel6: TcxGridLevel;
    ds_OBJECTS: TDataSource;
    t_OBJECTS: TADOTable;
    cxGridDBTableView6id: TcxGridDBColumn;
    cxGridDBTableView6name: TcxGridDBColumn;
    cxGridDBTableView6display_name: TcxGridDBColumn;
    cxGridDBTableView6table_name: TcxGridDBColumn;
    cxGridDBTableView6folder_name: TcxGridDBColumn;
    cxGridDBTableView6view_name: TcxGridDBColumn;
    cxGridDBTableView6checked: TcxGridDBColumn;
    cxGridDBTableView_FKDBColumn1: TcxGridDBColumn;
    cxGridDBTableView_FKDBColumn2: TcxGridDBColumn;
    cxGrid1DBTableView2_TableFieldsDBColumn5: TcxGridDBColumn;
    cxGrid1DBTableView2_TableFieldsDBColumn6: TcxGridDBColumn;
    cxGridDBTableView_FKDBColumn3: TcxGridDBColumn;
    cxGridDBTableView3DBColumn1: TcxGridDBColumn;
    TabSheet_Owner: TTabSheet;
    cxGrid7: TcxGrid;
    cxGridDBTableView7: TcxGridDBTableView;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridLevel7: TcxGridLevel;
    TabSheet5: TTabSheet;
    cxGrid_func: TcxGrid;
    cxGridDBTableView_func: TcxGridDBTableView;
    cxGridDBColumn7: TcxGridDBColumn;
    cxGridDBColumn8: TcxGridDBColumn;
    cxGridDBColumn9: TcxGridDBColumn;
    cxGridDBColumn10: TcxGridDBColumn;
    cxGridLevel8: TcxGridLevel;
    ds_Func: TDataSource;
    cxGridDBTableView_funcDBColumn1: TcxGridDBColumn;
    cxGridDBTableView_SPDBColumn1: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn1_is_exists: TcxGridDBColumn;
    cxGridDBTableView1DBColumn1: TcxGridDBColumn;
    cxGridDBTableView_FKDBColumn4: TcxGridDBColumn;
    cxGridDBTableView3DBColumn2: TcxGridDBColumn;
    cxGridDBTableView5DBColumn1: TcxGridDBColumn;
    Splitter1: TSplitter;
    Splitter4: TSplitter;
    FormStorage2: TFormStorage;
    COL_IDENTITY: TcxGridDBColumn;
    COL_IsNullable: TcxGridDBColumn;
    col_xtype1: TcxGridDBColumn;
    cxGridDBTableView_FKDBColumn5_xtype: TcxGridDBColumn;
    cxGridDBTableView3DBColumn3_xtype: TcxGridDBColumn;
    cxGridDBTableView_SPDBColumn2_xtype: TcxGridDBColumn;
    cxGridDBTableView5DBColumn2_xtype: TcxGridDBColumn;
    cxGridDBTableView_funcDBColumn2_xtype: TcxGridDBColumn;
    cxGrid1DBTableView_indexes_is_unique_index: TcxGridDBColumn;
    TabSheet9: TTabSheet;
    ds_DatabaseInfo: TDataSource;
    t_Database_Info: TADOTable;
    cxGrid9: TcxGrid;
    cxGridDBTableView4: TcxGridDBTableView;
    cxGridLevel9: TcxGridLevel;
    TabSheet10: TTabSheet;
    cxGrid_Index: TcxGrid;
    cxGridDBTableView8: TcxGridDBTableView;
    cxGridLevel10: TcxGridLevel;
    cxGridDBTableView8id: TcxGridDBColumn;
    cxGridDBTableView8tablename: TcxGridDBColumn;
    cxGridDBTableView8name: TcxGridDBColumn;
    cxGridDBTableView8index_description: TcxGridDBColumn;
    cxGridDBTableView8xtype: TcxGridDBColumn;
    cxGridDBTableView8is_exists: TcxGridDBColumn;
    cxGridDBTableView8index_type: TcxGridDBColumn;
    cxGridDBTableView1DBColumn2_priority: TcxGridDBColumn;
    cxGridDBTableView_funcDBColumn2_priority: TcxGridDBColumn;
    cxGridDBTableView_SPDBColumn2_priority: TcxGridDBColumn;
    cxGridDBTableView4version: TcxGridDBColumn;
    col_product_name: TcxGridDBColumn;
    TabSheet_OBJECTS_main: TTabSheet;
    cxGrid_OBJECTS_main: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel11: TcxGridLevel;
    ds_OBJECTS_main: TDataSource;
    t_OBJECTS_main: TADOTable;
    cxGridDBTableView2id: TcxGridDBColumn;
    cxGridDBTableView2name: TcxGridDBColumn;
    cxGridDBTableView2enabled: TcxGridDBColumn;
    cxGridDBTableView2Owner: TcxGridDBColumn;
    cxGrid1DBTableView_TriggerDBColumn1_Owner: TcxGridDBColumn;
    TabSheet8: TTabSheet;
    cxGrid11: TcxGrid;
    cxGridDBTableView9: TcxGridDBTableView;
    cxGridLevel12: TcxGridLevel;
    ds_Projects: TDataSource;
    t_sys_Projects: TADOTable;
    cxGridDBTableView9name: TcxGridDBColumn;
    qry_Tables: TADOQuery;
    qry_CK: TADOQuery;
    qry_Triggers: TADOQuery;
    qry_indexes: TADOQuery;
    qry_Views: TADOQuery;
    qry_FK: TADOQuery;
    qry_Func: TADOQuery;
    qry_SP: TADOQuery;
    qry_table_fields: TADOQuery;
    Panel1: TPanel;
    DBEdit1: TDBEdit;
    Label1: TLabel;
    col_IsComputed: TcxGridDBColumn;
    col_computed_text: TcxGridDBColumn;
    DBLookupComboBox1: TDBLookupComboBox;
    TabSheet11_udp: TTabSheet;
    ds_UDT: TDataSource;
    qry_UDT: TADOQuery;
    cxGrid_UDP: TcxGrid;
    cxGridDBTableView10_udp: TcxGridDBTableView;
    cxGridDBColumn11: TcxGridDBColumn;
    cxGridDBColumn12: TcxGridDBColumn;
    cxGridDBColumn13: TcxGridDBColumn;
    cxGridDBColumn14: TcxGridDBColumn;
    cxGridDBColumn15: TcxGridDBColumn;
    cxGridDBColumn16: TcxGridDBColumn;
    cxGridLevel13: TcxGridLevel;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGridDBTableView1Column1: TcxGridDBColumn;
    cxGridDBTableView1Column2: TcxGridDBColumn;
    cxGridDBTableView3Column1: TcxGridDBColumn;
    cxGridDBTableView3Column2: TcxGridDBColumn;
    cxGridDBTableView_SPColumn1: TcxGridDBColumn;
    cxGridDBTableView_SPColumn2: TcxGridDBColumn;
    cxGridDBTableView_funcColumn1: TcxGridDBColumn;
    cxGridDBTableView_funcColumn2: TcxGridDBColumn;
    PageControl_SP: TPageControl;
    TabSheet12: TTabSheet;
    TabSheet13: TTabSheet;
    DBMemo1: TDBMemo;
    Splitter2: TSplitter;
    DBMemo_SP: TDBMemo;
    PageControl_func: TPageControl;
    TabSheet14: TTabSheet;
    DBMemo2: TDBMemo;
    TabSheet15: TTabSheet;
    DBMemo3: TDBMemo;
    PageControl_view: TPageControl;
    TabSheet16: TTabSheet;
    DBMemo4: TDBMemo;
    TabSheet17: TTabSheet;
    DBMemo5: TDBMemo;
    cxGridDBTableView8Column1: TcxGridDBColumn;
    cxGrid_view: TcxGrid;
    Splitter5: TSplitter;
    PageControl_tr: TPageControl;
    TabSheet11: TTabSheet;
    DBMemo6: TDBMemo;
    TabSheet18: TTabSheet;
    DBMemo7: TDBMemo;
    PageControl_index: TPageControl;
    TabSheet19: TTabSheet;
    DBMemo8: TDBMemo;
    TabSheet20: TTabSheet;
    DBMemo9: TDBMemo;
    Splitter3: TSplitter;
    TabSheet21: TTabSheet;
    DBMemo10: TDBMemo;
    TabSheet22: TTabSheet;
    DBMemo11: TDBMemo;
    TabSheet23: TTabSheet;
    DBMemo12: TDBMemo;
//    procedure FormDestroy(Sender: TObject);
    procedure btn_CancelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    procedure Open;

  public
    class procedure ExecDlg(aPrefix: string = '');
  end;



implementation

{$R *.dfm}




//-------------------------------------------------------------------
class procedure Tdlg_Structure_new.ExecDlg(aPrefix: string = '');
//-------------------------------------------------------------------
var
  dlg_Structure_new: Tdlg_Structure_new;

begin
  dlg_Structure_new:=Tdlg_Structure_new (FindForm(Tdlg_Structure_new.ClassName));


  if not Assigned(dlg_Structure_new) then
    Application.CreateForm(Tdlg_Structure_new, dlg_Structure_new);

  dlg_Structure_new.Show;


end;


// ---------------------------------------------------------------
procedure Tdlg_Structure_new.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin

  Caption :='Structure';
  PageControl1.Align:=alClient;

  if MDBConnection.Connected then
  begin
    ShowMessage('MDBConnection.Connected');
    MDBConnection.Close;
  end;


  t_OBJECTS.TableName       := '_OBJECTS';
  t_Owner.TableName         := 'Owner';

  t_OBJECTS_main.TableName  := '_OBJECTS_main';

  t_Database_Info.TableName  := 'Database_Info';
//  t_.TableName  := 'DatabaseInfo';


  cxGrid_OBJECTS.Align :=alClient;

  cxGrid_UDP.Align :=alClient;
  cxGrid_SP.Align :=alClient;
  cxGrid_Func.Align :=alClient;
  cxGrid_View.Align :=alClient;
  cxGrid_Index.Align :=alClient;
  cxGrid_tr.Align :=alClient;

  Open;

 // TabSheet_sys_objects.Visible := False;

 cxGrid_OBJECTS_main.Align :=alClient;


  cxGrid7.Align:=alClient;
//  cxGrid11.Align:=alClient;

end;


// ---------------------------------------------------------------
procedure Tdlg_Structure_new.Open;
// ---------------------------------------------------------------
const
  DEF_SQL        =  'select * from sys_objects where xtype=''%s'' '; // and project_id = '':name''
//  DEF_SQL_FIELDS =  'select * from sys_objects_table_fields where project_name = '':name''';
  DEF_SQL_FIELDS =  'select * from sys_objects_table_fields ';

  //order by priority desc, name
begin
  db_SetComponentADOConnection(Self, dmMDB.ADOConnection1);

 // db_TableOpen1()

  t_sys_Projects.Open;

  db_OpenQuery (qry_Tables,   Format(DEF_SQL, ['u']));
  db_OpenQuery (qry_Views,    Format(DEF_SQL, ['v']));
  db_OpenQuery (qry_Triggers, Format(DEF_SQL, ['tr']));
  db_OpenQuery (qry_indexes,  Format(DEF_SQL, ['index']));
  db_OpenQuery (qry_FK,       Format(DEF_SQL, ['fk']));
  db_OpenQuery (qry_Func,     Format(DEF_SQL, ['fn']));
  db_OpenQuery (qry_SP,       Format(DEF_SQL, ['sp']));
  db_OpenQuery (qry_CK,       Format(DEF_SQL, ['ck']));
  db_OpenQuery (qry_UDT,      Format(DEF_SQL, ['UDT']));

  db_OpenQuery (qry_table_fields,     DEF_SQL_FIELDS);



  t_OBJECTS.Open;

  t_Owner.Open;
  t_Database_Info.Open;

  t_OBJECTS_main.Open;


end;


procedure Tdlg_Structure_new.btn_CancelClick(Sender: TObject);
begin
  Close;
end;


procedure Tdlg_Structure_new.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action :=caFree;
end;

end.


