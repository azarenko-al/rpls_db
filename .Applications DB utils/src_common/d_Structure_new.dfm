object dlg_Structure_new: Tdlg_Structure_new
  Left = 930
  Top = 218
  Width = 1087
  Height = 911
  BorderWidth = 3
  Caption = 'dlg_Structure_new'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 30
    Width = 1073
    Height = 499
    ActivePage = TabSheet5
    Align = alTop
    TabOrder = 0
    object TabSheet8: TTabSheet
      Caption = 'Projects'
      ImageIndex = 12
      object cxGrid11: TcxGrid
        Left = 0
        Top = 0
        Width = 473
        Height = 471
        Align = alLeft
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView9: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_Projects
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.GroupByBox = False
          object cxGridDBTableView9name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 301
          end
        end
        object cxGridLevel12: TcxGridLevel
          GridView = cxGridDBTableView9
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Tables'
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 1065
        Height = 471
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGrid1DBTableView1: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_Tables
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.ImmediateEditor = False
          OptionsView.Indicator = True
          object cxGrid1DBTableView1name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            PropertiesClassName = 'TcxTextEditProperties'
            Options.Editing = False
            Width = 250
          end
          object cxGrid1DBTableView1checked: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
            Width = 78
          end
          object cxGrid1DBTableView1DBColumn1_xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
          object cxGrid1DBTableView1DBColumn1_Owner: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListOptions.ShowHeader = False
            Properties.ListSource = ds_Owner
            Visible = False
            Width = 93
          end
          object cxGrid1DBTableView1DBColumn1_is_exists: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
            Width = 79
          end
          object cxGrid1DBTableView1Column2: TcxGridDBColumn
            DataBinding.FieldName = 'create_date'
            Width = 128
          end
          object cxGrid1DBTableView1Column1: TcxGridDBColumn
            DataBinding.FieldName = 'modify_date'
            Width = 140
          end
        end
        object cxGrid1DBTableView_CK: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_CK
          DataController.DetailKeyFieldNames = 'TableName'
          DataController.KeyFieldNames = 'id'
          DataController.MasterKeyFieldNames = 'name'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.ImmediateEditor = False
          OptionsData.Editing = False
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object cxGrid1DBTableView_CKid: TcxGridDBColumn
            DataBinding.FieldName = 'id'
          end
          object cxGrid1DBTableView_CKname: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 100
          end
          object cxGrid1DBTableView_CKcontent: TcxGridDBColumn
            DataBinding.FieldName = 'content'
            Width = 1534
          end
        end
        object cxGrid1DBTableView_indexes: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_Indexes
          DataController.DetailKeyFieldNames = 'TableName'
          DataController.KeyFieldNames = 'id'
          DataController.MasterKeyFieldNames = 'name'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.ImmediateEditor = False
          OptionsCustomize.ColumnFiltering = False
          OptionsData.Editing = False
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object cxGrid1DBTableView_indexesid: TcxGridDBColumn
            DataBinding.FieldName = 'id'
          end
          object cxGrid1DBTableView_indexesname: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 258
          end
          object cxGrid1DBTableView_indexes_is_primary_key: TcxGridDBColumn
            DataBinding.FieldName = 'is_primary_key'
            Width = 34
          end
          object cxGrid1DBTableView_indexes_is_unique_key: TcxGridDBColumn
            DataBinding.FieldName = 'is_unique_key'
            Width = 34
          end
          object cxGrid1DBTableView_indexes_is_unique_index: TcxGridDBColumn
            DataBinding.FieldName = 'is_unique_index'
            Width = 34
          end
          object cxGrid1DBTableView_indexesindex_keys: TcxGridDBColumn
            DataBinding.FieldName = 'index_keys'
            Width = 222
          end
          object cxGrid1DBTableView_indexescontent: TcxGridDBColumn
            DataBinding.FieldName = 'content'
          end
        end
        object cxGrid1DBTableView_Trigger: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_Triggers
          DataController.DetailKeyFieldNames = 'TableName'
          DataController.KeyFieldNames = 'id'
          DataController.MasterKeyFieldNames = 'name'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.ImmediateEditor = False
          OptionsData.Editing = False
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object cxGrid1DBTableView_Triggername: TcxGridDBColumn
            DataBinding.FieldName = 'name'
          end
          object cxGrid1DBTableView_Triggercontent: TcxGridDBColumn
            DataBinding.FieldName = 'content'
          end
          object cxGrid1DBTableView_TriggerDBColumn1_Owner: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListSource = ds_Owner
          end
        end
        object cxGrid1DBTableView2_TableFields: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_TableFields
          DataController.DetailKeyFieldNames = 'TableName'
          DataController.KeyFieldNames = 'id'
          DataController.MasterKeyFieldNames = 'name'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.ImmediateEditor = False
          OptionsCustomize.ColumnFiltering = False
          OptionsData.Editing = False
          OptionsView.GroupByBox = False
          object cxGrid1DBTableView2_TableFieldsDBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 83
          end
          object cxGrid1DBTableView2_TableFieldsDBColumn3: TcxGridDBColumn
            DataBinding.FieldName = 'type'
            Width = 68
          end
          object cxGrid1DBTableView2_TableFieldsDBColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'size'
            Width = 52
          end
          object cxGrid1DBTableView2_TableFieldsDBColumn4: TcxGridDBColumn
            DataBinding.FieldName = 'PK'
            Width = 36
          end
          object cxGrid1DBTableView2_TableFieldsDBColumn5: TcxGridDBColumn
            DataBinding.FieldName = 'default_name'
            Width = 112
          end
          object cxGrid1DBTableView2_TableFieldsDBColumn6: TcxGridDBColumn
            DataBinding.FieldName = 'default_content'
            Width = 105
          end
          object COL_IDENTITY: TcxGridDBColumn
            DataBinding.FieldName = 'IDENTITY'
            Width = 34
          end
          object COL_IsNullable: TcxGridDBColumn
            DataBinding.FieldName = 'IsNullable'
            Width = 34
          end
          object col_IsComputed: TcxGridDBColumn
            DataBinding.FieldName = 'IsComputed'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Width = 34
          end
          object col_computed_text: TcxGridDBColumn
            DataBinding.FieldName = 'computed_text'
            Width = 50
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
          Options.DetailTabsPosition = dtpTop
          object cxGrid1Level2: TcxGridLevel
            Caption = 'Table Fields'
            GridView = cxGrid1DBTableView2_TableFields
          end
          object cxGrid1Level4: TcxGridLevel
            Caption = 'Indexes'
            GridView = cxGrid1DBTableView_indexes
          end
          object cxGrid1Level5: TcxGridLevel
            Caption = 'Triggers'
            GridView = cxGrid1DBTableView_Trigger
          end
          object cxGrid1Level3: TcxGridLevel
            Caption = 'CK'
            GridView = cxGrid1DBTableView_CK
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Views'
      ImageIndex = 1
      object Splitter4: TSplitter
        Left = 0
        Top = 293
        Width = 1065
        Height = 3
        Cursor = crVSplit
        Align = alBottom
      end
      object cxGrid_view: TcxGrid
        Left = 0
        Top = 0
        Width = 1065
        Height = 249
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView1: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_Views
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.ImmediateEditor = False
          OptionsView.Indicator = True
          object cxGridDBTableView1id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
            Options.Editing = False
            Options.Filtering = False
          end
          object cxGridDBTableView1name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            PropertiesClassName = 'TcxTextEditProperties'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 194
          end
          object cxGridDBTableView1content: TcxGridDBColumn
            DataBinding.FieldName = 'Definition'
            PropertiesClassName = 'TcxMemoProperties'
            Options.Editing = False
            Width = 116
          end
          object cxGridDBTableView1SQLCode: TcxGridDBColumn
            DataBinding.FieldName = 'SQLCode'
            Visible = False
            Options.Editing = False
            Width = 105
          end
          object cxGridDBTableView1DBColumn1_checked: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
          end
          object cxGridDBTableView1DBColumn1_text: TcxGridDBColumn
            DataBinding.FieldName = 'text'
            PropertiesClassName = 'TcxMemoProperties'
            Properties.ScrollBars = ssBoth
            Options.Filtering = False
          end
          object cxGridDBTableView1DBColumn1_Status: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.DropDownListStyle = lsFixedList
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListOptions.ShowHeader = False
            Properties.ListSource = ds_Owner
            Visible = False
            Width = 90
          end
          object cxGridDBTableView1DBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
          object col_xtype1: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
          object cxGridDBTableView1DBColumn2_priority: TcxGridDBColumn
            DataBinding.FieldName = 'priority'
          end
          object cxGridDBTableView1Column2: TcxGridDBColumn
            DataBinding.FieldName = 'create_date'
          end
          object cxGridDBTableView1Column1: TcxGridDBColumn
            DataBinding.FieldName = 'modify_date'
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
      object PageControl_view: TPageControl
        Left = 0
        Top = 296
        Width = 1065
        Height = 175
        ActivePage = TabSheet22
        Align = alBottom
        TabOrder = 1
        object TabSheet22: TTabSheet
          Caption = 'sql_create'
          ImageIndex = 2
          object DBMemo11: TDBMemo
            Left = 0
            Top = 0
            Width = 761
            Height = 147
            Align = alLeft
            DataField = 'sql_create'
            DataSource = ds_Views
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
        object TabSheet16: TTabSheet
          Caption = 'sql_drop'
          object DBMemo4: TDBMemo
            Left = 0
            Top = 0
            Width = 761
            Height = 147
            Align = alLeft
            DataField = 'sql_drop'
            DataSource = ds_Views
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
        object TabSheet17: TTabSheet
          Caption = 'Definition'
          ImageIndex = 1
          object DBMemo5: TDBMemo
            Left = 0
            Top = 0
            Width = 825
            Height = 147
            Align = alLeft
            DataField = 'Definition'
            DataSource = ds_Views
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'FK'
      ImageIndex = 2
      object cxGrid3: TcxGrid
        Left = 0
        Top = 0
        Width = 1065
        Height = 471
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView_FK: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_FK
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.ImmediateEditor = False
          OptionsView.Indicator = True
          object cxGridDBTableView_FKid: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
            Options.Editing = False
          end
          object cxGridDBTableView_FKname: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 226
          end
          object cxGridDBTableView_FKtablename: TcxGridDBColumn
            DataBinding.FieldName = 'pk_table_name'
            Options.Editing = False
            Width = 121
          end
          object cxGridDBTableView_FKDBColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'pk_column_name'
            Width = 81
          end
          object cxGridDBTableView_FKpk_tablename: TcxGridDBColumn
            DataBinding.FieldName = 'fk_table_name'
            Options.Editing = False
            Width = 114
          end
          object cxGridDBTableView_FKDBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'fk_column_name'
            Width = 138
          end
          object cxGridDBTableView_FKDBColumn3: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListSource = ds_Owner
            Visible = False
            Width = 79
          end
          object cxGridDBTableView_FKDBColumn4: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
          object cxGridDBTableView_FKDBColumn5_xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
        end
        object cxGridLevel2: TcxGridLevel
          GridView = cxGridDBTableView_FK
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Triggers'
      ImageIndex = 3
      object Splitter5: TSplitter
        Left = 0
        Top = 293
        Width = 1065
        Height = 3
        Cursor = crVSplit
        Align = alBottom
      end
      object cxGrid_tr: TcxGrid
        Left = 0
        Top = 0
        Width = 1065
        Height = 241
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView3: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_Triggers
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Indicator = True
          object cxGridDBTableView3tablename: TcxGridDBColumn
            DataBinding.FieldName = 'tablename'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 124
          end
          object cxGridDBTableView3name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            SortIndex = 1
            SortOrder = soAscending
            Width = 147
          end
          object cxGridDBTableView3content: TcxGridDBColumn
            DataBinding.FieldName = 'Definition'
            PropertiesClassName = 'TcxMemoProperties'
            Options.Editing = False
            Width = 119
          end
          object cxGridDBTableView3DBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListSource = ds_Owner
            Visible = False
            Width = 93
          end
          object cxGridDBTableView3DBColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
          object cxGridDBTableView3DBColumn3_xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
            Visible = False
          end
          object cxGridDBTableView3Column2: TcxGridDBColumn
            DataBinding.FieldName = 'create_date'
          end
          object cxGridDBTableView3Column1: TcxGridDBColumn
            DataBinding.FieldName = 'modify_date'
          end
        end
        object cxGridLevel3: TcxGridLevel
          GridView = cxGridDBTableView3
        end
      end
      object PageControl_tr: TPageControl
        Left = 0
        Top = 296
        Width = 1065
        Height = 175
        ActivePage = TabSheet21
        Align = alBottom
        TabOrder = 1
        object TabSheet21: TTabSheet
          Caption = 'sql_create'
          ImageIndex = 2
          object DBMemo10: TDBMemo
            Left = 0
            Top = 0
            Width = 761
            Height = 147
            Align = alLeft
            DataField = 'sql_create'
            DataSource = ds_Triggers
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
        object TabSheet11: TTabSheet
          Caption = 'sql_drop'
          object DBMemo6: TDBMemo
            Left = 0
            Top = 0
            Width = 761
            Height = 147
            Align = alLeft
            DataField = 'sql_drop'
            DataSource = ds_Triggers
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
        object TabSheet18: TTabSheet
          Caption = 'Definition'
          ImageIndex = 1
          object DBMemo7: TDBMemo
            Left = 0
            Top = 0
            Width = 825
            Height = 147
            Align = alLeft
            DataField = 'Definition'
            DataSource = ds_Triggers
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'SP'
      ImageIndex = 5
      object Splitter2: TSplitter
        Left = 0
        Top = 293
        Width = 1065
        Height = 3
        Cursor = crVSplit
        Align = alBottom
      end
      object cxGrid_SP: TcxGrid
        Left = 0
        Top = 0
        Width = 1065
        Height = 249
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView_SP: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_SP
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Indicator = True
          object cxGridDBTableView_SPname: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 287
          end
          object cxGridDBTableView_SPcontent: TcxGridDBColumn
            DataBinding.FieldName = 'Definition'
            PropertiesClassName = 'TcxMemoProperties'
            Options.Editing = False
          end
          object cxGridDBTableView_SPDBColumn1_checked: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
            Width = 101
          end
          object cxGridDBTableView_SPstatus_id: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.DropDownListStyle = lsFixedList
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListOptions.ShowHeader = False
            Properties.ListSource = ds_Owner
            Visible = False
            Width = 86
          end
          object cxGridDBTableView_SPDBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
            Width = 87
          end
          object cxGridDBTableView_SPDBColumn2_xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
          object cxGridDBTableView_SPDBColumn2_priority: TcxGridDBColumn
            DataBinding.FieldName = 'priority'
          end
          object cxGridDBTableView_SPColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'create_date'
          end
          object cxGridDBTableView_SPColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'modify_date'
          end
        end
        object cxGridLevel4: TcxGridLevel
          GridView = cxGridDBTableView_SP
        end
      end
      object PageControl_SP: TPageControl
        Left = 0
        Top = 296
        Width = 1065
        Height = 175
        ActivePage = TabSheet12
        Align = alBottom
        TabOrder = 1
        object TabSheet12: TTabSheet
          Caption = 'sql_drop'
          object DBMemo1: TDBMemo
            Left = 0
            Top = 0
            Width = 761
            Height = 147
            Align = alLeft
            DataField = 'sql_drop'
            DataSource = ds_SP
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
        object TabSheet13: TTabSheet
          Caption = 'Definition'
          ImageIndex = 1
          object DBMemo_SP: TDBMemo
            Left = 0
            Top = 0
            Width = 825
            Height = 147
            Align = alLeft
            DataField = 'Definition'
            DataSource = ds_SP
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'Func'
      ImageIndex = 8
      object Splitter1: TSplitter
        Left = 0
        Top = 293
        Width = 1065
        Height = 3
        Cursor = crVSplit
        Align = alBottom
      end
      object cxGrid_func: TcxGrid
        Left = 0
        Top = 0
        Width = 1065
        Height = 225
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView_func: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_Func
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Indicator = True
          object cxGridDBColumn7: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 270
          end
          object cxGridDBColumn8: TcxGridDBColumn
            DataBinding.FieldName = 'Definition'
            PropertiesClassName = 'TcxMemoProperties'
          end
          object cxGridDBColumn9: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
            Width = 101
          end
          object cxGridDBColumn10: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.DropDownListStyle = lsFixedList
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListOptions.ShowHeader = False
            Properties.ListSource = ds_Owner
            Width = 86
          end
          object cxGridDBTableView_funcDBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
            Width = 86
          end
          object cxGridDBTableView_funcDBColumn2_xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
          object cxGridDBTableView_funcDBColumn2_priority: TcxGridDBColumn
            DataBinding.FieldName = 'priority'
          end
          object cxGridDBTableView_funcColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'modify_date'
          end
          object cxGridDBTableView_funcColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'create_date'
          end
        end
        object cxGridLevel8: TcxGridLevel
          GridView = cxGridDBTableView_func
        end
      end
      object PageControl_func: TPageControl
        Left = 0
        Top = 296
        Width = 1065
        Height = 175
        ActivePage = TabSheet23
        Align = alBottom
        TabOrder = 1
        object TabSheet23: TTabSheet
          Caption = 'sql_create'
          ImageIndex = 2
          object DBMemo12: TDBMemo
            Left = 0
            Top = 0
            Width = 761
            Height = 147
            Align = alLeft
            DataField = 'sql_create'
            DataSource = ds_Func
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
        object TabSheet14: TTabSheet
          Caption = 'sql_drop'
          object DBMemo2: TDBMemo
            Left = 0
            Top = 0
            Width = 761
            Height = 147
            Align = alLeft
            DataField = 'sql_drop'
            DataSource = ds_Func
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
        object TabSheet15: TTabSheet
          Caption = 'Definition'
          ImageIndex = 1
          object DBMemo3: TDBMemo
            Left = 0
            Top = 0
            Width = 825
            Height = 147
            Align = alLeft
            DataField = 'Definition'
            DataSource = ds_Func
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
      end
    end
    object TabSheet_CK: TTabSheet
      Caption = 'CK'
      ImageIndex = 6
      object cxGrid6: TcxGrid
        Left = 0
        Top = 0
        Width = 1065
        Height = 471
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView5: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_CK
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          object cxGridDBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
            Options.Editing = False
          end
          object cxGridDBColumn3: TcxGridDBColumn
            DataBinding.FieldName = 'tablename'
            Options.Editing = False
            Width = 121
          end
          object cxGridDBColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 226
          end
          object cxGridDBColumn5: TcxGridDBColumn
            DataBinding.FieldName = 'Definition'
            Options.Editing = False
            Width = 355
          end
          object cxGridDBTableView5DBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
          object cxGridDBTableView5DBColumn2_xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
        end
        object cxGridLevel5: TcxGridLevel
          GridView = cxGridDBTableView5
        end
      end
    end
    object TabSheet10: TTabSheet
      Caption = 'indexes'
      ImageIndex = 10
      object Splitter3: TSplitter
        Left = 0
        Top = 293
        Width = 1065
        Height = 3
        Cursor = crVSplit
        Align = alBottom
      end
      object cxGrid_Index: TcxGrid
        Left = 0
        Top = 0
        Width = 1065
        Height = 257
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView8: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_Indexes
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.ImmediateEditor = False
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.Indicator = True
          object cxGridDBTableView8id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
          end
          object cxGridDBTableView8tablename: TcxGridDBColumn
            DataBinding.FieldName = 'tablename'
            Width = 179
          end
          object cxGridDBTableView8name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 201
          end
          object cxGridDBTableView8index_description: TcxGridDBColumn
            DataBinding.FieldName = 'sql_create'
            Width = 292
          end
          object cxGridDBTableView8xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
          object cxGridDBTableView8is_exists: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
          object cxGridDBTableView8index_type: TcxGridDBColumn
            DataBinding.FieldName = 'index_type'
            Visible = False
            Width = 257
          end
          object cxGridDBTableView8Column1: TcxGridDBColumn
            DataBinding.FieldName = 'Definition'
            Width = 258
          end
        end
        object cxGridLevel10: TcxGridLevel
          GridView = cxGridDBTableView8
        end
      end
      object PageControl_index: TPageControl
        Left = 0
        Top = 296
        Width = 1065
        Height = 175
        ActivePage = TabSheet20
        Align = alBottom
        TabOrder = 1
        object TabSheet19: TTabSheet
          Caption = 'sql_drop'
          object DBMemo8: TDBMemo
            Left = 0
            Top = 0
            Width = 761
            Height = 147
            Align = alLeft
            DataField = 'sql_drop'
            DataSource = ds_Indexes
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
        object TabSheet20: TTabSheet
          Caption = 'Definition'
          ImageIndex = 1
          object DBMemo9: TDBMemo
            Left = 0
            Top = 0
            Width = 825
            Height = 147
            Align = alLeft
            DataField = 'Definition'
            DataSource = ds_Indexes
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
      end
    end
    object TabSheet7: TTabSheet
      Caption = 'OBJECTS'
      ImageIndex = 7
      object cxGrid_OBJECTS: TcxGrid
        Left = 0
        Top = 0
        Width = 1065
        Height = 161
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView6: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_OBJECTS
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.GroupByBox = False
          object cxGridDBTableView6id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            SortIndex = 0
            SortOrder = soAscending
          end
          object cxGridDBTableView6checked: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssUnchecked
            Width = 80
          end
          object cxGridDBTableView6name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            Width = 189
          end
          object cxGridDBTableView6display_name: TcxGridDBColumn
            DataBinding.FieldName = 'display_name'
            Visible = False
          end
          object cxGridDBTableView6table_name: TcxGridDBColumn
            DataBinding.FieldName = 'table_name'
            Visible = False
          end
          object cxGridDBTableView6folder_name: TcxGridDBColumn
            DataBinding.FieldName = 'folder_name'
            Visible = False
          end
          object cxGridDBTableView6view_name: TcxGridDBColumn
            DataBinding.FieldName = 'view_name'
            Visible = False
          end
        end
        object cxGridLevel6: TcxGridLevel
          GridView = cxGridDBTableView6
        end
      end
    end
    object TabSheet_Owner: TTabSheet
      Caption = 'Owner'
      ImageIndex = 8
      object cxGrid7: TcxGrid
        Left = 0
        Top = 0
        Width = 1065
        Height = 455
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView7: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_Owner
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.GroupByBox = False
          object cxGridDBColumn4: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            Width = 101
          end
          object cxGridDBColumn6: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssUnchecked
            Options.Editing = False
            Width = 80
          end
        end
        object cxGridLevel7: TcxGridLevel
          GridView = cxGridDBTableView7
        end
      end
    end
    object TabSheet9: TTabSheet
      Caption = 'DatabaseInfo'
      ImageIndex = 9
      object cxGrid9: TcxGrid
        Left = 0
        Top = 0
        Width = 987
        Height = 471
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView4: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_DatabaseInfo
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          object col_product_name: TcxGridDBColumn
            DataBinding.FieldName = 'product_name'
            Width = 141
          end
          object cxGridDBTableView4version: TcxGridDBColumn
            DataBinding.FieldName = 'version'
            Width = 114
          end
        end
        object cxGridLevel9: TcxGridLevel
          GridView = cxGridDBTableView4
        end
      end
    end
    object TabSheet_OBJECTS_main: TTabSheet
      Caption = 'OBJECTS_main'
      ImageIndex = 11
      object cxGrid_OBJECTS_main: TcxGrid
        Left = 0
        Top = 0
        Width = 1065
        Height = 181
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView2: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_OBJECTS_main
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          object cxGridDBTableView2id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
          end
          object cxGridDBTableView2name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 161
          end
          object cxGridDBTableView2enabled: TcxGridDBColumn
            DataBinding.FieldName = 'enabled'
            Width = 84
          end
          object cxGridDBTableView2Owner: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListSource = ds_Owner
            Width = 136
          end
        end
        object cxGridLevel11: TcxGridLevel
          GridView = cxGridDBTableView2
        end
      end
    end
    object TabSheet11_udp: TTabSheet
      Caption = 'UDP'
      ImageIndex = 13
      object cxGrid_UDP: TcxGrid
        Left = 0
        Top = 0
        Width = 1065
        Height = 225
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView10_udp: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_UDT
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Indicator = True
          object cxGridDBColumn11: TcxGridDBColumn
            DataBinding.FieldName = 'tablename'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 124
          end
          object cxGridDBColumn12: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            SortIndex = 1
            SortOrder = soAscending
            Width = 147
          end
          object cxGridDBColumn13: TcxGridDBColumn
            DataBinding.FieldName = 'Definition'
            PropertiesClassName = 'TcxMemoProperties'
            Options.Editing = False
            Width = 222
          end
          object cxGridDBColumn14: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListSource = ds_Owner
            Width = 93
          end
          object cxGridDBColumn15: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
          object cxGridDBColumn16: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
        end
        object cxGridLevel13: TcxGridLevel
          GridView = cxGridDBTableView10_udp
        end
      end
    end
  end
  object pn_Buttons: TPanel
    Left = 0
    Top = 842
    Width = 1073
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 1
    DesignSize = (
      1073
      35)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 1073
      Height = 2
      Align = alTop
      Shape = bsTopLine
      Visible = False
    end
    object btn_Cancel: TButton
      Left = 994
      Top = 8
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ModalResult = 2
      TabOrder = 0
      OnClick = btn_CancelClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1073
    Height = 30
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 6
      Top = 6
      Width = 33
      Height = 13
      Caption = 'Project'
    end
    object DBEdit1: TDBEdit
      Left = 49
      Top = 2
      Width = 168
      Height = 21
      DataField = 'name'
      DataSource = ds_Projects
      ParentColor = True
      ReadOnly = True
      TabOrder = 0
    end
    object DBLookupComboBox1: TDBLookupComboBox
      Left = 272
      Top = 2
      Width = 249
      Height = 21
      KeyField = 'id'
      ListField = 'name'
      ListSource = ds_Projects
      TabOrder = 1
    end
  end
  object ds_Tables: TDataSource
    DataSet = qry_Tables
    Left = 24
    Top = 616
  end
  object ds_Views: TDataSource
    DataSet = qry_Views
    Left = 480
    Top = 616
  end
  object ds_CK: TDataSource
    DataSet = qry_CK
    Left = 200
    Top = 616
  end
  object ds_FK: TDataSource
    DataSet = qry_FK
    Left = 536
    Top = 616
  end
  object ds_Triggers: TDataSource
    DataSet = qry_Triggers
    Left = 344
    Top = 616
  end
  object ds_Indexes: TDataSource
    DataSet = qry_indexes
    Left = 408
    Top = 616
  end
  object ds_Owner: TDataSource
    DataSet = t_Owner
    Left = 776
    Top = 616
  end
  object ds_SP: TDataSource
    DataSet = qry_SP
    Left = 680
    Top = 616
  end
  object t_Owner: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableName = 'Owner'
    Left = 776
    Top = 576
  end
  object ds_TableFields: TDataSource
    DataSet = qry_table_fields
    Left = 120
    Top = 616
  end
  object MDBConnection: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Password="";Data Source=C:\ONEG' +
      'A\RPLS_DB\bin\install_db.mdb;Persist Security Info=True'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 296
    Top = 716
  end
  object ds_OBJECTS: TDataSource
    DataSet = t_OBJECTS
    Left = 120
    Top = 728
  end
  object t_OBJECTS: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableName = '_OBJECTS'
    Left = 120
    Top = 680
  end
  object ds_Func: TDataSource
    DataSet = qry_Func
    Left = 616
    Top = 616
  end
  object FormStorage2: TFormStorage
    IniFileName = 'Software\Onega\Install_DB\'
    StoredProps.Strings = (
      'PageControl1.ActivePage')
    StoredValues = <>
    Left = 728
    Top = 680
  end
  object ds_DatabaseInfo: TDataSource
    DataSet = t_Database_Info
    Left = 488
    Top = 736
  end
  object t_Database_Info: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableDirect = True
    TableName = 'Database_Info'
    Left = 488
    Top = 680
  end
  object ds_OBJECTS_main: TDataSource
    DataSet = t_OBJECTS_main
    Left = 872
    Top = 616
  end
  object t_OBJECTS_main: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableName = '_OBJECTS_main'
    Left = 872
    Top = 576
  end
  object ds_Projects: TDataSource
    DataSet = t_sys_Projects
    Left = 600
    Top = 736
  end
  object t_sys_Projects: TADOTable
    Active = True
    Connection = MDBConnection
    CursorType = ctStatic
    TableName = 'Projects'
    Left = 600
    Top = 680
  end
  object qry_Tables: TADOQuery
    Active = True
    Connection = MDBConnection
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'name'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = 'link'
      end>
    SQL.Strings = (
      'select * from sys_objects'
      'where xtype='#39'u'#39
      ' ')
    Left = 24
    Top = 568
  end
  object qry_CK: TADOQuery
    Active = True
    Connection = MDBConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from sys_objects'
      'where xtype='#39'ck'#39
      '')
    Left = 200
    Top = 568
  end
  object qry_Triggers: TADOQuery
    Active = True
    Connection = MDBConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from sys_objects'
      'where xtype='#39'tr'#39' ')
    Left = 344
    Top = 568
  end
  object qry_indexes: TADOQuery
    Active = True
    Connection = MDBConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from sys_objects'
      'where xtype='#39'index'#39' ')
    Left = 416
    Top = 568
  end
  object qry_Views: TADOQuery
    Active = True
    Connection = MDBConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from sys_objects'
      'where xtype='#39'v'#39)
    Left = 480
    Top = 568
  end
  object qry_FK: TADOQuery
    Active = True
    Connection = MDBConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from sys_objects'
      'where xtype='#39'fk'#39
      '')
    Left = 544
    Top = 568
  end
  object qry_Func: TADOQuery
    Active = True
    Connection = MDBConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from sys_objects'
      'where xtype='#39'fn'#39)
    Left = 616
    Top = 568
  end
  object qry_SP: TADOQuery
    Active = True
    Connection = MDBConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from sys_objects'
      'where xtype='#39'sp'#39' ')
    Left = 680
    Top = 568
  end
  object qry_table_fields: TADOQuery
    Connection = MDBConnection
    CursorType = ctStatic
    DataSource = ds_Tables
    Parameters = <
      item
        Name = 'object_id'
        Attributes = [paNullable]
        DataType = ftInteger
        NumericScale = 255
        Precision = 255
        Value = Null
      end>
    SQL.Strings = (
      'select * from  sys_objects_table_fields'
      'where object_id = :object_id')
    Left = 120
    Top = 568
  end
  object ds_UDT: TDataSource
    DataSet = qry_UDT
    Left = 272
    Top = 616
  end
  object qry_UDT: TADOQuery
    Active = True
    Connection = MDBConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from sys_objects'
      'where xtype='#39'ck'#39' ')
    Left = 272
    Top = 568
  end
end
