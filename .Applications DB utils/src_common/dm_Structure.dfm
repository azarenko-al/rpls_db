object dmStructure: TdmStructure
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 209
  Top = 278
  Height = 502
  Width = 1265
  object ds_Tables: TDataSource
    DataSet = tbl_Tables
    Left = 56
    Top = 104
  end
  object ds_Views: TDataSource
    DataSet = tbl_Views
    Left = 392
    Top = 104
  end
  object ds_CK: TDataSource
    DataSet = tbl_CK
    Left = 192
    Top = 104
  end
  object ds_FK: TDataSource
    DataSet = tbl_FK
    Left = 448
    Top = 104
  end
  object ds_Triggers: TDataSource
    DataSet = tbl_Triggers
    Left = 256
    Top = 104
  end
  object ds_Indexes: TDataSource
    DataSet = tbl_indexes
    Left = 320
    Top = 104
  end
  object ds_Owner: TDataSource
    DataSet = t_Owner
    Left = 720
    Top = 104
  end
  object ds_SP: TDataSource
    DataSet = tbl_SP
    Left = 664
    Top = 104
  end
  object t_Owner: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableName = 'Owner'
    Left = 720
    Top = 48
  end
  object tbl_Triggers: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableDirect = True
    TableName = 'Triggers'
    Left = 248
    Top = 48
  end
  object tbl_SP: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableName = 'StoredProcs'
    Left = 664
    Top = 48
  end
  object tbl_FK: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableDirect = True
    TableName = 'FK'
    Left = 456
    Top = 48
  end
  object tbl_indexes: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableDirect = True
    TableName = 'Indexes'
    Left = 320
    Top = 48
  end
  object tbl_CK: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableDirect = True
    TableName = 'CK'
    Left = 184
    Top = 48
  end
  object tbl_Views: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableDirect = True
    TableName = 'Views'
    Left = 392
    Top = 44
  end
  object tbl_Tables: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableDirect = True
    TableName = 'Tables'
    Left = 56
    Top = 44
  end
  object tbl_TableFields: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableDirect = True
    TableName = 'Table_Fields'
    Left = 128
    Top = 48
  end
  object ds_TableFields: TDataSource
    DataSet = tbl_TableFields
    Left = 128
    Top = 104
  end
  object MDBConnection: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=W:\RPLS_DB tools\_b' +
      'in\install_db.mdb;Persist Security Info=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 376
    Top = 212
  end
  object ds_OBJECTS: TDataSource
    DataSet = t_OBJECTS
    Left = 64
    Top = 256
  end
  object t_OBJECTS: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableName = '_OBJECTS'
    Left = 64
    Top = 208
  end
  object ds_Func: TDataSource
    DataSet = t_Func
    Left = 600
    Top = 104
  end
  object t_Func: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableName = 'Func'
    Left = 600
    Top = 48
  end
  object ds_DatabaseInfo: TDataSource
    DataSet = t_Database_Info
    Left = 976
    Top = 104
  end
  object t_Database_Info: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableDirect = True
    TableName = 'Database_Info'
    Left = 976
    Top = 56
  end
  object ds_OBJECTS_main: TDataSource
    DataSet = t_OBJECTS_main
    Left = 816
    Top = 104
  end
  object t_OBJECTS_main: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableName = '_OBJECTS_main'
    Left = 816
    Top = 48
  end
  object t_sys_Projects: TADOTable
    CursorType = ctStatic
    TableName = 'sys_Projects'
    Left = 792
    Top = 200
  end
  object ds_Projects: TDataSource
    DataSet = t_sys_Projects
    Left = 792
    Top = 248
  end
end
