unit dm_MDB;

interface

uses
  SysUtils, Classes, Forms, Dialogs, DB, ADODB,

  u_files,
  u_db_mdb

  ;

type
  TdmMDB = class(TDataModule)
    ADOConnection1: TADOConnection;
    procedure DataModuleCreate(Sender: TObject);
  private
    function OpenDB1: Boolean;
// TODO: UpdateIniFile1
//  procedure UpdateIniFile1(aChecksum: Integer; aIniFileName: string);

  public
    MDBFileName: string;

    procedure CompactDatabase1;
    procedure EmptyTable(aTableName: string);

    procedure PublishToDir1(aDir: string);

    class procedure Init1;
  end;

var
  dmMDB: TdmMDB;

implementation
{$R *.dfm}
(*
const
  DEF_install_db_mdb = 'install_db.mdb';
*)


class procedure TdmMDB.Init1;
begin
  if not Assigned(dmMDB) then
    dmMDB := TdmMDB.Create(Application);
end;


procedure TdmMDB.DataModuleCreate(Sender: TObject);
begin
  if ADOConnection1.Connected then
  begin
    ShowMessage('procedure TdmMDB.DataModuleCreate(Sender: TObject); - MDBConnection.Connected');

    ADOConnection1.Close;
  end;


  MDBFileName := GetApplicationDir()+ 'install_db.mdb';

  OpenDB1;

end;


// ---------------------------------------------------------------
procedure TdmMDB.CompactDatabase1;
// ---------------------------------------------------------------
begin
//  Exit;

  {$DEFINE CompactDatabase1}
  {$IFDEF CompactDatabase1}

  ADOConnection1.Close;

 // mdb_CompactDatabase(MDBFileName);
  {$ENDIF}
end;


//------------------------------------------------------------------------------
function TdmMDB.OpenDB1: Boolean;
//------------------------------------------------------------------------------

begin
  Result := True;

  if ADOConnection1.Connected then
    Exit;

 // MDBFileName := GetApplicationDir()+ 'onega_tables.mdb';
  //ir :=  GetApplicationDir()

//  MDBFileName := GetApplicationDir()+ 'install_db.mdb';

//  MDBFileName := GetApplicationDir()+ DEF_install_db_mdb;


//  {$DEFINE CompactDatabase1}
//  {$IFDEF CompactDatabase1}
//  mdb_CompactDatabase(MDBFileName);
//  {$ENDIF}


  if not ADOConnection1.Connected then
    if not mdb_OpenConnection(ADOConnection1, MDBFileName) then
    begin
      ShowMessage('������ �������� ����� MS Access: '+ MDBFileName);
      Result := False;
    end;


 // Filecopy (MDBFileName, 'd:\aaaa.mdb');

end;



procedure TdmMDB.EmptyTable(aTableName: string);
begin
  ADOConnection1.Execute(Format('DELETE FROM %s', [aTableName]));
end;

// ---------------------------------------------------------------
procedure TdmMDB.PublishToDir1(aDir: string);
// ---------------------------------------------------------------
begin

  ADOConnection1.Close;

  aDir := IncludeTrailingBackslash(aDir);

  FileCopy_Src_Dest (GetApplicationDir()+ 'install_db.mdb',  aDir + 'install_db.mdb' );
  FileCopy_Src_Dest (GetApplicationDir()+ 'install_db.exe',  aDir + 'install_db.exe' );

  Assert (FileExists(aDir + 'install_db.mdb'));
  Assert (FileExists(aDir + 'install_db.exe'));


end;


end.




