unit u_sync_SQL_maker;

interface

uses SysUtils,

  u_Func_arrays,
  u_func,

  u_sync_classes;

type
  TSQL_maker = class(TObject)
  private
    procedure CompareTables(aDBTable_MDB, aDBTable_SQL: TDBObject);

    function CreateField(aField: TDBTableField): boolean;
    function MakeCreateSQL_Index(aIndex: TDBObject): string;
  public
    DBStructure_SQL: TDBStructure;
    DBStructure_MDB: TDBStructure;

    procedure Compare;

    procedure CompareTableFields(aField_sql, aField_mdb: TDBTableField);

    function PrepareTableCreateSQL(aDBTable: TDBObject): boolean;
    function GetDropSQL(aDBObject: TDBObject): string;
  end;


implementation

const
  SQL_ALTER_TABLE_DROP_CONSTRAINT   = 'ALTER TABLE [%s] DROP CONSTRAINT [%s]';

  SQL_DROP_TABLE        = 'DROP TABLE [%s]';
  SQL_DROP_INDEX        = 'DROP INDEX [%s].[%s]';
  SQL_DROP_TRIGGER      = 'DROP TRIGGER [%s]';
  SQL_DROP_VIEW         = 'DROP VIEW [%s]';


// TODO: CompareIndexes
////---------------------------------------------------------
//function TSQL_maker.CompareIndexes(aObject1, aObject2: TDBObject): Boolean;
////---------------------------------------------------------
//begin
//
//Result:= (aObject1.Index.IS_CLUSTERED = aObject2.Index.IS_CLUSTERED) AND
//         (aObject1.Index.INDEX_KEYS   = aObject2.Index.INDEX_KEYS) AND
//         (aObject1.Index.FILL_FACTOR  = aObject1.Index.FILL_FACTOR);
//
//end;

(*
function TSQL_maker.CompareFields(aField1, aField2: TDBTableField): Boolean;
begin
  Result :=
       (aField1.TYPE_             = aField2.TYPE_) AND
       (aField1.SIZE              = aField2.SIZE) AND
       (aField1.DEFAULT_CONTENT   = aField2.DEFAULT_CONTENT) AND
     //  (aField1.IsRowGuidCol      = aField2.IsRowGuidCol) AND

       (aField1.IsIDENTITY          = aField2.IsIDENTITY) AND
//       (aField1.IDENT_SEED        = aField2.IDENT_SEED) AND
 //      (aField1.IDENT_INCR        = aField2.IDENT_INCR) AND

       (aField1.AllowNULLS          = aField2.AllowNULLS)

end;

*)

//---------------------------------------------------------
procedure TSQL_maker.CompareTableFields(aField_sql, aField_mdb: TDBTableField);
//---------------------------------------------------------
// ��������� �������� IDENTITY �� ��������������, �.�.
// ��� ������� ��������� � TSQL, ������� �� ������ ��� �������� ��� ���������� ���
// ��� ��������� ����� �������� ����������:
// - ������� ��������� ��������, �� ��������� IDENTITY ����� ��� ����
// - ����������� ���� ������ �� ���������
// - ��������� ��� �����������, ��������, ����� ��������� ��������
// - ������� ��������� ��������
// - ������������� ��������� - ��������� �� ��� ���������
// - ������������ ��� �����������, ��������, �����
//---------------------------------------------------------
var
 // sMdbDef: string;

//  bDefaultMdbIsBound, bDefaultSqlIsBound, 

  bNeedAlterColumn, bNeedAlterIdentity,
 // bNeedRecreateRowGuid,
  bNeedAlterDefault, bIsColumnTypeVariable: boolean;
  FSql: string;
  label label_exit;
begin
//  Result:= false;
//  gl_DB.ErrorMsg:='';
 // FModuleName:= 'TdmCompareAndModify.AlterField';

 (* gl_DB.BeginTrans;

  FMdbDataSet:= dmMdb.tbl_table_fields;
  FSqlDataSet:= t_TableFields;
//  FSqlDataSet:= dmSQLServ.t_TableFields;

  if not (Eq(FSqlDataSet[FLD_NAME],      FMdbDataSet[FLD_NAME])     and
          Eq(FSqlDataSet[FLD_TABLENAME], FMdbDataSet[FLD_TABLENAME]))
  then
    Raise Exception.Create('datasets not sincronized');

*)

  //-----------------------------------------
  // RowGuidColumns
//  if aField_sql.IsRowGuidCol <> aField_mdb.IsRowGuidCol then
////  if aField_sql.IsRowGuidCol FSqlDataSet[FLD_IsRowGuidCol]<>FMdbDataSet[FLD_IsRowGuidCol] then
//  begin
//    if aField_sql.IsRowGuidCol then
//      FSql:= Format('ALTER TABLE [%s] ALTER COLUMN [%s] DROP ROWGUIDCOL',
//                [aField_mdb.TABLENAME, aField_mdb.NAME])
//    else
//      FSql:= Format('ALTER TABLE [%s] ALTER COLUMN [%s] ADD ROWGUIDCOL',
//                [aField_mdb.TABLENAME, aField_mdb.NAME]);
//
//
//(*    AddSript(FSql);
//
//    if not gl_DB.ExecCommandSimple(FSql) then
//    begin
//      g_Log.AddError(FModuleName, gl_Db.ErrorMsg + FSql);
//      goto label_exit;
//    end;
//*)
//  end;

  //-----------------------------------------

  bNeedAlterColumn:=
       (aField_sql.TYPE_<>aField_mdb.TYPE_)     or
       (aField_sql.SIZE <>aField_mdb.SIZE)      or
       (aField_sql.IsNullable<>aField_mdb.IsNullable);

(*  bDefaultMdbIsBound:= aField_mdb.DEFAULT_IS_BOUND;
  bDefaultSqlIsBound:= aField_sql.DEFAULT_IS_BOUND;
*)
  bNeedAlterDefault:=
        (aField_sql.DEFAULT_CONTENT <> aField_mdb.DEFAULT_CONTENT);

  if bNeedAlterColumn then
    bNeedAlterDefault:= true;

//  if bNeedAlterColumn then
 //   bNeedRecreateRowGuid:= aField_sql.IsRowGuidCol;


  if bNeedAlterColumn or bNeedAlterDefault then
    if aField_sql.DEFAULT_NAME<>'' then
    begin
(*
      if bDefaultSqlIsBound then
        FSql:= Format('EXEC sp_unbindefault [%s.%s]',
                        [aField_mdb.TABLENAME, aField_mdb.NAME])
      else
*)
        FSql:= Format(SQL_ALTER_TABLE_DROP_CONSTRAINT,
                   [aField_mdb.TABLENAME,
                    aField_sql.DEFAULT_NAME
                   ]);

  (*    AddSript(FSql);

      if not gl_DB.ExecCommandSimple(FSql) then
      begin
        g_Log.AddError(FModuleName, gl_DB.ErrorMsg + FSql);
        goto label_exit;
      end;

*)

    end;


  if bNeedAlterColumn then
  begin
//    if bNeedRecreateRowGuid then
//    begin
//      FSql:= Format('ALTER TABLE [%s] ALTER COLUMN [%s] DROP ROWGUIDCOL',
//                [aField_mdb.TABLENAME, aField_mdb.NAME]);
//
//  (*    AddSript(FSql);
//
//      if not gl_DB.ExecCommandSimple(FSql) then
//      begin
//        g_Log.AddError(FModuleName, gl_Db.ErrorMsg + FSql);
//        goto label_exit;
//      end;
//*)
//
//    end;

    bIsColumnTypeVariable:= (aField_sql.IsVariableLen);
    // AND
     //                       (aField_sql.USERTYPE < 250);

    FSql:= Format('ALTER TABLE [%s] ALTER COLUMN [%s] %s%s %s',
                [aField_mdb.TableName,
                 aField_mdb.NAME,
                 aField_mdb.TYPE_,
                 IIF (bIsColumnTypeVariable, '('+ IntToStr(aField_mdb.Size)+')', ' '),
                 IIF (aField_mdb.IsNullable, 'NULL', 'NOT NULL')
                 ]);

 (*   AddSript(FSql);

    if not gl_DB.ExecCommandSimple(FSql) then
    begin
      g_Log.AddError(FModuleName, gl_Db.ErrorMsg + FSql);
      goto label_exit;
    end;
*)

//    if bNeedRecreateRowGuid then
//    begin
//      FSql:= Format('ALTER TABLE [%s] ALTER COLUMN [%s] ADD ROWGUIDCOL',
//                [aField_mdb.TABLENAME, aField_mdb.NAME]);
//
//
//  (*    AddSript(FSql);
//
//      if not gl_DB.ExecCommandSimple(FSql) then
//      begin
//        g_Log.AddError(FModuleName, gl_Db.ErrorMsg+ FSql);
//        goto label_exit;
//      end;
//*)
//
//    end;

  end;


  if bNeedAlterDefault then
  begin
  //  sMdbDef:=aField_mdb.DEFAULT_CONTENT;

    if aField_mdb.DEFAULT_CONTENT<>'' then
    begin
//      if bDefaultMdbIsBound then
//        FSql:= Format('EXEC sp_bindefault [%s], [%s.%s]',
//                 [aField_mdb.DEFAULT_NAME,
//                  aField_mdb.TABLENAME,
//                  aField_mdb.NAME
//                  ])
//      else

        FSql:= Format('ALTER TABLE [%s] ADD DEFAULT %s FOR [%s]',  // CONSTRAINT [%s]
                 [aField_mdb.TABLENAME, // FMdbDataSet[FLD_DEFAULT_NAME],
                  aField_mdb.DEFAULT_CONTENT,
                  aField_mdb.NAME ]);

(*
      AddSript(FSql);

      if not gl_DB.ExecCommandSimple(FSql) then
      begin
        g_Log.AddError(FModuleName, gl_Db.ErrorMsg + FSql);
        goto label_exit;
      end;
*)
    end;
  end;

(*
  Result:= (gl_DB.ErrorMsg='');

label_exit:

  if Result then
    gl_DB.CommitTrans
  else
    gl_DB.RollbackTrans;
*)

  bNeedAlterIdentity:=
       (aField_sql.IsIDENTITY <> aField_mdb.IsIDENTITY);

//             or
 //      (aField_sql.IDENT_SEED <>aField_mdb.IDENT_SEED)      or
  //     (aField_sql.IDENT_INCR <>aField_mdb.IDENT_INCR);

(*
  if bNeedAlterIdentity then
  begin
    db_UpdateRecord(t_Data_Compare, FLD_ERROR,
        '���������� �������� IDENTITY �� ��������������. ���������� � �������������');
    Result:= false;
  end;
*)
end;


//---------------------------------------------------------
function TSQL_maker.MakeCreateSQL_Index(aIndex: TDBObject): string;
//---------------------------------------------------------
var
  I: Integer;
 // iFillFactor: Integer;
  sDefault, sPK_Definition: string;
 // oFieldArray: TStrArray;

 // FSql : string;

 // bClustered,
  bVariable, bIdentity: boolean;
  s: string;

//  oField: TDBTableField;
 // oPRIMARY_KEY: TDBObject;
begin
 // Result:= false;

 // FModuleName:= 'TSQL_maker.PrepareTableCreateSQL';


 // oPRIMARY_KEY :=aDBTable.Indexes.FindIndexByPRIMARY_KEY();

  //------------------------------
  //  Primary Key
//  with dmMDB do
//  begin
//    FType:= TYPE_TABLE;
 //   FDataSet:= tbl_table_fields;

(*    bClustered:= false;
    iFillFactor:= 0;
    sPK_Definition:= '';
*)

  with aIndex do
  begin
 //   bClustered := Index.IS_CLUSTERED;
   // iFillFactor:= Index.FILL_FACTOR;

    Result:=
      Format('ALTER TABLE [%s] ADD                               '+
             'CONSTRAINT [%s] PRIMARY KEY   (%s) ON [PRIMARY]', //%s
              [TableName,
               Name,
//               IIF(bClustered,    'CLUSTERED', 'NONCLUSTERED'),
               Index.INDEX_KEYS
            //   IIF(iFillFactor>0, ' WITH FILLFACTOR = '+AsString(iFillFactor), '')
              ]);


  end;
end;

//---------------------------------------------------------
function TSQL_maker.PrepareTableCreateSQL(aDBTable: TDBObject): boolean;
//---------------------------------------------------------
var
  I: Integer;
 // iFillFactor: Integer;
  sDefault, sPK_Definition: string;
  oFieldArray: TStrArray;

  FSql : string;

 // bClustered,
//  bVariable,
   bIdentity: boolean;
  s: string;

  oField: TDBTableField;
  oPRIMARY_KEY: TDBObject;
begin
  Result:= false;

 // FModuleName:= 'TSQL_maker.PrepareTableCreateSQL';

  if aDBTable.PrimaryKeys.Count=0 then
    Exit;



  oPRIMARY_KEY :=aDBTable.PrimaryKeys[0];

  //------------------------------
  //  Primary Key
//  with dmMDB do
//  begin
//    FType:= TYPE_TABLE;
 //   FDataSet:= tbl_table_fields;

//    bClustered:= false;
   // iFillFactor:= 0;
  sPK_Definition:= '';

  if Assigned(oPRIMARY_KEY) then
  begin

//      bClustered := oPRIMARY_KEY.Index.IS_CLUSTERED;
 //   iFillFactor:= oPRIMARY_KEY.Index.FILL_FACTOR;

    sPK_Definition:=
      Format('ALTER TABLE [%s] ADD '+
             'CONSTRAINT [%s] PRIMARY KEY  (%s)  ON [PRIMARY]', //%s
              [oPRIMARY_KEY.TableName,
               oPRIMARY_KEY.Name,
             //  IIF(bClustered,    'CLUSTERED', 'NONCLUSTERED'),
               oPRIMARY_KEY.Index.INDEX_KEYS
            //   IIF(iFillFactor>0, ' WITH FILLFACTOR = '+AsString(iFillFactor), '')
              ]);


  end;


(*

    if tbl_indexes.Locate(FLD_TABLENAME+';'+FLD_IS_PRIMARY_KEY,
                  VarArrayOf([FObjName, true]), [loCaseInsensitive]) then
    begin
      bClustered := tbl_indexes.FieldByName(FLD_IS_CLUSTERED).AsBoolean;
      iFillFactor:= tbl_indexes.FieldByName(FLD_FILL_FACTOR).AsInteger;

      sPK_Definition:=
        Format('ALTER TABLE [%s] ADD                               '+
               'CONSTRAINT [%s] PRIMARY KEY %s (%s) %s ON [PRIMARY]',
                [FObjName,
                 tbl_indexes.FieldByName(FLD_NAME).AsString,
                 IIF(bClustered,    'CLUSTERED', 'NONCLUSTERED'),
                 tbl_indexes.FieldByName(FLD_INDEX_KEYS).AsString,
                 IIF(iFillFactor>0, ' WITH FILLFACTOR = '+AsString(iFillFactor), '')
                ]);
    end;
  end;  *)
  //------------------------------

  SetLength(oFieldArray, 0);

  for I := 0 to aDBTable.TableFields.Count - 1 do
  begin
    oField := aDBTable.TableFields[i];

   // bVariable:= oField.IsVariableLen; //  AND (oField.USERTYPE < 250);
    bIdentity:=  oField.IsIdentity;

    if oField.DEFAULT_CONTENT='' then
      sDefault:= ''
    else
      sDefault:= Format('DEFAULT %s', [oField.DEFAULT_CONTENT]);


    FSql:= Format('[%s] %s %s %s %s %s ',
             [oField.Name,
              oField.TYPE_,
              IIF (oField.IsVariableLen, Format('(%s)', [oField.SIZE]), ' '),
              sDefault,
              IIF (bIdentity, 'IDENTITY (1, 1)',' '),
               //             [oField.IDENT_SEED, oField.IDENT_INCR]),
               //    ' '),
             // IIF (oField.IsRowGuidCol, 'ROWGUIDCOL', ''),
              IIF (oField.IsNullable, 'NULL', 'NOT NULL')
             ]);
    StrArrayAddValue(oFieldArray, FSql);

   //   Next;
 //   end;
  end;

(*
  s := StrArrayToString(oFieldArray, ',');
  FSql := Format('CREATE TABLE [dbo].[%s] ( %s ) ON [PRIMARY]',
          [aDBTable.Name, s]);

*)
//  Result:= gl_DB.ExecCommandSimple(FSql);
//  AddSript(FSql);

(*
  if not Result then
  begin
    g_Log.AddError(FModuleName, gl_DB.ErrorMsg + FSql);
    exit;
  end;

  if Result then
    if sPK_Definition<>'' then
      if not gl_DB.ExecCommandSimple (sPK_Definition) then
        g_Log.AddError(FModuleName, gl_DB.ErrorMsg + sPK_Definition);
        
*)

end;

// ---------------------------------------------------------------
function TSQL_maker.GetDropSQL(aDBObject: TDBObject): string;
// ---------------------------------------------------------------
var
  sTblName, sObjName: string;
begin
  with aDBObject do
    case ObjectType of

      otTABLE:    Result:= Format(SQL_DROP_TABLE, [Name]);
      otVIEW:     Result:= Format(SQL_DROP_VIEW, [Name]);
      otTRIGGER:  Result:= Format(SQL_DROP_TRIGGER, [Name]) ;
      otStoredPROC:
                begin
                  if Pos(UpperCase('procedure'), UpperCase(Text))>0 then
                    Result:= Format('DROP PROCEDURE %s', [Name]);

                  if Pos(UpperCase('function'), UpperCase(Text))>0 then
                    Result:= Format('DROP FUNCTION %s', [Name]);
                end;

      otIndex:
        begin
        //  sTblName:= aDataSet[FLD_TABLENAME];

          if Index.IS_PRIMARY_KEY or
             Index.IS_UNIQUE_KEY
          then
            Result:=Format(SQL_ALTER_TABLE_DROP_CONSTRAINT, [TableName, Name])
          else
            Result:=Format(SQL_DROP_INDEX, [TableName, Name]);
        end;


      otFK, otCK:
       begin
        //  sTblName:= aDataSet[FLD_TABLENAME];
          Result:=Format(SQL_ALTER_TABLE_DROP_CONSTRAINT, [TableName, Name]);
        end ;

    end;

  (*
    if aObjNameToDrop<>'' then
      sObjName:= aObjNameToDrop
    else
      sObjName:= aDataSet[FLD_NAME];
  *)


end;

//---------------------------------------------------------
function TSQL_maker.CreateField(aField: TDBTableField): boolean;
//---------------------------------------------------------
var
  sDefault_CONTENT: string;
  bIsColumnTypeVariable: boolean;
  FSql: string;
begin
  Result:= false;
  //gl_DB.ErrorMsg:='';
 // FModuleName:= 'TdmCompareAndModify.CreateField';

  //with dmMDB do
//  begin
    if aField.DEFAULT_CONTENT='' then
      sDefault_CONTENT:= ''
    else
      sDefault_CONTENT:= Format('DEFAULT %s', [aField.DEFAULT_CONTENT]);

    bIsColumnTypeVariable:= (aField.IsVariableLen);
    // AND
     //                       (aField.USERTYPE < 250);

    FSql:= Format('ALTER TABLE %s ADD [%s] %s%s %s %s',
              [aField.TableName,
               aField.NAME,
               aField.TYPE_,
               IIF(bIsColumnTypeVariable, '('+ IntToStr(aField.Size) +')', ' '),
               sDefault_CONTENT,
               IIF(aField.IsIDENTITY, 'IDENTITY (1, 1)',  '')
              // IIF(aField.IsRowGuidCol, 'ROWGUIDCOL', '')
               ] );

(*
    Result:= gl_DB.ExecCommandSimple(FSql);
    AddSript(FSql);

    if not Result then
    begin
      g_Log.AddError(FModuleName, gl_DB.ErrorMsg + FSql);
      exit;
    end;

*)

    if (not aField.IsNullable) then
    begin
      FSql:= Format('ALTER TABLE %s ALTER COLUMN [%s] %s%s NOT NULL',
                [aField.TableName,
                 aField.NAME,
                 aField.TYPE_,
                 IIF (bIsColumnTypeVariable, '('+ IntToStr(aField.Size) +')', '')
                 ]);

   (*   AddSript(FSql);

      if not gl_DB.ExecCommandSimple(FSql) then
        g_Log.AddError(FModuleName, gl_Db.ErrorMsg + FSql);
*)

    end;
//  end;

end;


procedure TSQL_maker.CompareTables(aDBTable_MDB, aDBTable_SQL: TDBObject);
begin

end;


// ---------------------------------------------------------------
procedure TSQL_maker.Compare;
// ---------------------------------------------------------------
var
  I: Integer; 
  oTable_SQL, oTable_MDB: TDBObject;

begin
  Assert(Assigned(DBStructure_SQL), 'Value not assigned');
  Assert(Assigned(DBStructure_MDB), 'Value not assigned');


  for I := 0 to DBStructure_MDB.Tables.Count - 1 do
  begin
    oTable_MDB:=DBStructure_MDB.Tables[i];
    oTable_SQL:=DBStructure_MDB.Tables.FindByName(oTable_MDB.Name);

    if not Assigned(oTable_SQL) then
      PrepareTableCreateSQL(oTable_MDB)
    else
      CompareTables(oTable_MDB, oTable_SQL);

  end;


end;



end.

//
////---------------------------------------------------------
//function TdmCompareAndModify.AlterField(aTblName: string): boolean;
////---------------------------------------------------------
//// ��������� �������� IDENTITY �� ��������������, �.�.
//// ��� ������� ��������� � TSQL, ������� �� ������ ��� �������� ��� ���������� ���
//// ��� ��������� ����� �������� ����������:
//// - ������� ��������� ��������, �� ��������� IDENTITY ����� ��� ����
//// - ����������� ���� ������ �� ���������
//// - ��������� ��� �����������, ��������, ����� ��������� ��������
//// - ������� ��������� ��������
//// - ������������� ��������� - ��������� �� ��� ���������
//// - ������������ ��� �����������, ��������, �����
////---------------------------------------------------------
//var
//  sMdbDef: string;
//  bNeedAlterColumn, bDefaultMdbIsBound, bDefaultSqlIsBound, bNeedAlterIdentity,
//  bNeedRecreateRowGuid, bNeedAlterDefault, bIsColumnTypeVariable: boolean;
//  label label_exit;
//begin
//  Result:= false;
//  gl_DB.ErrorMsg:='';
//  FModuleName:= 'TdmCompareAndModify.AlterField';
//
//  gl_DB.BeginTrans;
//
//  FMdbDataSet:= dmMdb.tbl_table_fields;
//  FSqlDataSet:= t_TableFields;
////  FSqlDataSet:= dmSQLServ.t_TableFields;
//
//  if not (Eq(FSqlDataSet[FLD_NAME],      FMdbDataSet[FLD_NAME])     and
//          Eq(FSqlDataSet[FLD_TABLENAME], FMdbDataSet[FLD_TABLENAME]))
//  then
//    Raise Exception.Create('datasets not sincronized');
//
//
//  //-----------------------------------------
//  // RowGuidColumns
//  if FSqlDataSet[FLD_IsRowGuidCol]<>FMdbDataSet[FLD_IsRowGuidCol] then
//  begin
//    if FSqlDataSet[FLD_IsRowGuidCol] then
//      FSql:= Format('ALTER TABLE [%s] ALTER COLUMN [%s] DROP ROWGUIDCOL',
//                [FMdbDataSet[FLD_TABLENAME], FMdbDataSet[FLD_NAME]])
//    else
//      FSql:= Format('ALTER TABLE [%s] ALTER COLUMN [%s] ADD ROWGUIDCOL',
//                [FMdbDataSet[FLD_TABLENAME], FMdbDataSet[FLD_NAME]]);
//
//    AddSript(FSql);
//
//    if not gl_DB.ExecCommandSimple(FSql) then
//    begin
//      g_Log.AddError(FModuleName, gl_Db.ErrorMsg + FSql);
//      goto label_exit;
//    end;
//
//  end;
//  //-----------------------------------------
//
//  bNeedAlterColumn:=
//       (FSqlDataSet[FLD_TYPE] <>FMdbDataSet[FLD_TYPE])      or
//       (FSqlDataSet[FLD_SIZE] <>FMdbDataSet[FLD_SIZE])      or
//       (FSqlDataSet[FLD_NULLS]<>FMdbDataSet[FLD_NULLS]);
//
//  bDefaultMdbIsBound:= FMdbDataSet[FLD_DEFAULT_IS_BOUND];
//  bDefaultSqlIsBound:= FSqlDataSet[FLD_DEFAULT_IS_BOUND];
//
//  bNeedAlterDefault:=
//        (FSqlDataSet[FLD_DEFAULT_CONTENT] <> FMdbDataSet[FLD_DEFAULT_CONTENT]);
//
//  if bNeedAlterColumn then
//    bNeedAlterDefault:= true;
//
//  if bNeedAlterColumn then
//    bNeedRecreateRowGuid:= FSqlDataSet[FLD_IsRowGuidCol];
//
//
//  if bNeedAlterColumn or bNeedAlterDefault then
//    if FSqlDataSet.FieldByName(FLD_DEFAULT_NAME).AsString<>'' then
//    begin
//      if bDefaultSqlIsBound then
//        FSql:= Format('EXEC sp_unbindefault [%s.%s]',
//                        [FMdbDataSet[FLD_TABLENAME], FMdbDataSet[FLD_NAME]])
//      else
//        FSql:= Format(SQL_ALTER_TABLE_DROP_CONSTRAINT,
//                   [FMdbDataSet.FieldByName(FLD_TABLENAME).AsString,
//                    FSqlDataSet.FieldByName(FLD_DEFAULT_NAME).AsString
//                   ]);
//
//      AddSript(FSql);
//
//      if not gl_DB.ExecCommandSimple(FSql) then
//      begin
//        g_Log.AddError(FModuleName, gl_DB.ErrorMsg + FSql);
//        goto label_exit;
//      end;
//    end;
//
//
//  if bNeedAlterColumn then
//  begin
//    if bNeedRecreateRowGuid then
//    begin
//      FSql:= Format('ALTER TABLE [%s] ALTER COLUMN [%s] DROP ROWGUIDCOL',
//                [FMdbDataSet[FLD_TABLENAME], FMdbDataSet[FLD_NAME]]);
//
//      AddSript(FSql);
//
//      if not gl_DB.ExecCommandSimple(FSql) then
//      begin
//        g_Log.AddError(FModuleName, gl_Db.ErrorMsg + FSql);
//        goto label_exit;
//      end;
//    end;
//
//    bIsColumnTypeVariable:= (FMdbDataSet[FLD_VARIABLE]) AND
//                            (FMdbDataSet[FLD_USERTYPE] < 250);
//
//    FSql:= Format('ALTER TABLE [%s] ALTER COLUMN [%s] %s%s %s',
//                [aTblName,
//                 FMdbDataSet[FLD_NAME],
//                 FMdbDataSet[FLD_TYPE],
//                 IIF (bIsColumnTypeVariable, '('+FMdbDataSet[FLD_SIZE]+')', ' '),
//                 IIF (FMdbDataSet.FieldByName(FLD_NULLS).AsBoolean, 'NULL', 'NOT NULL')
//                 ]);
//
//    AddSript(FSql);
//
//    if not gl_DB.ExecCommandSimple(FSql) then
//    begin
//      g_Log.AddError(FModuleName, gl_Db.ErrorMsg + FSql);
//      goto label_exit;
//    end;
//
//    if bNeedRecreateRowGuid then
//    begin
//      FSql:= Format('ALTER TABLE [%s] ALTER COLUMN [%s] ADD ROWGUIDCOL',
//                [FMdbDataSet[FLD_TABLENAME], FMdbDataSet[FLD_NAME]]);
//
//      AddSript(FSql);
//
//      if not gl_DB.ExecCommandSimple(FSql) then
//      begin
//        g_Log.AddError(FModuleName, gl_Db.ErrorMsg+ FSql);
//        goto label_exit;
//      end;
//    end;
//
//  end;
//
//
//  if bNeedAlterDefault then
//  begin
//    sMdbDef:= FMdbDataSet.FieldByName(FLD_DEFAULT_CONTENT).AsString;
//
//    if sMdbDef<>'' then
//    begin
//      if bDefaultMdbIsBound then
//        FSql:= Format('EXEC sp_bindefault [%s], [%s.%s]',
//                 [FMdbDataSet[FLD_DEFAULT_NAME],
//                  FMdbDataSet[FLD_TABLENAME],
//                  FMdbDataSet[FLD_NAME]])
//      else
//        FSql:= Format('ALTER TABLE [%s] ADD DEFAULT %s FOR [%s]',  // CONSTRAINT [%s]
//                 [FMdbDataSet[FLD_TABLENAME], // FMdbDataSet[FLD_DEFAULT_NAME],
//                  sMdbDef,
//                  FMdbDataSet[FLD_NAME] ]);
//
//      AddSript(FSql);
//
//      if not gl_DB.ExecCommandSimple(FSql) then
//      begin
//        g_Log.AddError(FModuleName, gl_Db.ErrorMsg + FSql);
//        goto label_exit;
//      end;
//    end;
//  end;
//
//  Result:= (gl_DB.ErrorMsg='');
//
//label_exit:
//
//  if Result then
//    gl_DB.CommitTrans
//  else
//    gl_DB.RollbackTrans;
//
//
//  bNeedAlterIdentity:=
//       (FSqlDataSet[FLD_IDENTITY  ] <>FMdbDataSet[FLD_IDENTITY  ])      or
//       (FSqlDataSet[FLD_IDENT_SEED] <>FMdbDataSet[FLD_IDENT_SEED])      or
//       (FSqlDataSet[FLD_IDENT_INCR] <>FMdbDataSet[FLD_IDENT_INCR]);
//
//  if bNeedAlterIdentity then
//  begin
//    db_UpdateRecord(t_Data_Compare, FLD_ERROR,
//        '���������� �������� IDENTITY �� ��������������. ���������� � �������������');
//    Result:= false;
//  end;
//
//end;

