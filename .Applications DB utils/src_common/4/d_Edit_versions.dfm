object dlg_Versions_Edit: Tdlg_Versions_Edit
  Left = 459
  Top = 130
  Width = 667
  Height = 603
  Caption = #1042#1077#1088#1089#1080#1080' ('#1083#1086#1082#1072#1083#1100#1085#1099#1077' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1103' '#1095#1077#1088#1077#1079' SQL)'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 242
    Width = 659
    Height = 11
    Cursor = crVSplit
    Align = alTop
    ResizeStyle = rsUpdate
  end
  object grid: TdxDBGrid
    Left = 0
    Top = 73
    Width = 659
    Height = 128
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderMinRowCount = 2
    HeaderPanelRowCount = 1
    KeyField = 'id'
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alTop
    PopupMenu = PopupMenu1
    TabOrder = 0
    DataSource = DataSource1
    Filter.Criteria = {00000000}
    LookAndFeel = lfFlat
    OptionsBehavior = [edgoDragScroll, edgoEditing, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    OptionsCustomize = [edgoBandMoving, edgoBandSizing, edgoColumnMoving, edgoColumnSizing, edgoRowSizing]
    OptionsDB = [edgoCanAppend, edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
    ShowNewItemRow = True
    object col_caption: TdxDBGridColumn
      Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
      Width = 223
      BandIndex = 0
      RowIndex = 0
      FieldName = 'name'
    end
    object col_enabled: TdxDBGridCheckColumn
      Caption = #1056#1072#1079#1088#1077#1096#1077#1085#1086
      Width = 42
      BandIndex = 0
      RowIndex = 0
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object col_exec_each_time: TdxDBGridCheckColumn
      Caption = #1055#1088#1080#1084#1077#1085#1103#1090#1100' '#1082#1072#1078#1076#1099#1081' '#1088#1072#1079
      Width = 44
      BandIndex = 0
      RowIndex = 0
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object col_date: TdxDBGridDateColumn
      Caption = #1044#1072#1090#1072
      Width = 115
      BandIndex = 0
      RowIndex = 0
    end
    object col_content_sql: TdxDBGridMemoColumn
      Caption = 'SQL'
      Width = 118
      BandIndex = 0
      RowIndex = 0
    end
    object col_content_programmed: TdxDBGridColumn
      Caption = #1055#1088#1086#1075#1088#1072#1084#1084#1085#1086#1077' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1077
      Width = 103
      BandIndex = 0
      RowIndex = 0
    end
  end
  object pn_Buttons: TPanel
    Left = 0
    Top = 534
    Width = 659
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 1
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 659
      Height = 2
      Align = alTop
      Shape = bsTopLine
    end
    object Label1: TLabel
      Left = 8
      Top = 13
      Width = 70
      Height = 13
      Caption = #1058#1077#1089#1090#1086#1074#1072#1103' '#1041#1044':'
    end
    object ed_Test_DB: TEdit
      Left = 91
      Top = 9
      Width = 121
      Height = 21
      TabOrder = 0
      Text = 'onega'
    end
  end
  object DBMemo1: TDBMemo
    Left = 0
    Top = 448
    Width = 659
    Height = 86
    Align = alBottom
    DataSource = ds
    ScrollBars = ssBoth
    TabOrder = 2
  end
  object Panel1: TPanel
    Left = 0
    Top = 201
    Width = 659
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    Visible = False
    object DBNavigator1: TDBNavigator
      Left = 10
      Top = 8
      Width = 116
      Height = 25
      DataSource = ds
      VisibleButtons = [nbInsert, nbDelete, nbPost, nbCancel]
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 659
    Height = 73
    Align = alTop
    Caption = 'Panel2'
    TabOrder = 4
    Visible = False
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 328
    Width = 659
    Height = 120
    Align = alBottom
    TabOrder = 5
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = DataSource1
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      Filtering.MaxDropDownCount = 12
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.Navigator = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.Indicator = True
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object cxGrid1DBTableView1DBColumn1: TcxGridDBColumn
        Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
        DataBinding.FieldName = 'name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 223
      end
      object cxGrid1DBTableView1DBColumn2: TcxGridDBColumn
        Caption = #1056#1072#1079#1088#1077#1096#1077#1085#1086
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ReadOnly = False
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 42
      end
      object cxGrid1DBTableView1DBColumn3: TcxGridDBColumn
        Caption = #1055#1088#1080#1084#1077#1085#1103#1090#1100' '#1082#1072#1078#1076#1099#1081' '#1088#1072#1079
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ReadOnly = False
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 44
      end
      object cxGrid1DBTableView1DBColumn4: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Options.Filtering = False
        Width = 115
      end
      object cxGrid1DBTableView1DBColumn5: TcxGridDBColumn
        Caption = 'SQL'
        PropertiesClassName = 'TcxMemoProperties'
        Properties.Alignment = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 118
      end
      object cxGrid1DBTableView1DBColumn6: TcxGridDBColumn
        Caption = #1055#1088#1086#1075#1088#1072#1084#1084#1085#1086#1077' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1077
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 103
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qry: TADOQuery
    Connection = ADOConnection1
    AfterPost = qryAfterPost
    OnNewRecord = qryNewRecord
    Parameters = <>
    Left = 56
    Top = 264
  end
  object ds: TDataSource
    DataSet = qry
    Left = 96
    Top = 264
  end
  object ADOConnection1: TADOConnection
    LoginPrompt = False
    Left = 176
    Top = 264
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 552
    Top = 10
  end
  object PopupMenu1: TPopupMenu
    Left = 88
    Top = 16
    object N1: TMenuItem
      Action = act_Test_SQL
    end
    object SQL1: TMenuItem
      Action = act_Test_All_SQL
    end
  end
  object ActionList1: TActionList
    Left = 40
    Top = 16
    object act_Test_SQL: TAction
      Caption = #1055#1088#1086#1074#1077#1088#1080#1090#1100' SQL '#1085#1072' '#1090#1077#1089#1090#1086#1074#1086#1081' '#1041#1044
      OnExecute = act_Test_SQLExecute
    end
    object act_Test_All_SQL: TAction
      Caption = #1055#1088#1086#1074#1077#1088#1080#1090#1100' '#1074#1077#1089#1100' SQL '#1085#1072' '#1090#1077#1089#1090#1086#1074#1086#1081' '#1041#1044
      OnExecute = act_Test_SQLExecute
    end
  end
  object cmd: TADOCommand
    Parameters = <>
    Left = 480
    Top = 8
  end
  object ADOTable1: TADOTable
    Active = True
    Connection = dmMDB.ADOConnection1
    CursorType = ctStatic
    TableName = 'version_updates'
    Left = 280
    Top = 264
  end
  object DataSource1: TDataSource
    DataSet = ADOTable1
    Left = 352
    Top = 264
  end
end
