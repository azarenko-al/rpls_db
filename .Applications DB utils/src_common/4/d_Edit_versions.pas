unit d_Edit_versions;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, dxDBGrid, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, ADODB,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  dxCntner, StdCtrls, ExtCtrls, DBCtrls, ActnList, Menus, rxPlacemnt,

  dm_MDB,

  u_func,
  //u_func_db,
  //u_func_dlg,
  u_const_db,
  u_const,

  dm_Main,

  cxControls, cxGridCustomView, cxClasses, cxGridLevel, cxGrid

  ;

type
  Tdlg_Versions_Edit = class(TForm)
    grid: TdxDBGrid;
    qry: TADOQuery;
    col_content_sql: TdxDBGridMemoColumn;
    col_enabled: TdxDBGridCheckColumn;
    col_exec_each_time: TdxDBGridCheckColumn;
    col_date: TdxDBGridDateColumn;
    col_content_programmed: TdxDBGridColumn;
    ds: TDataSource;
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    ed_Test_DB: TEdit;
    Label1: TLabel;
    ADOConnection1: TADOConnection;
    DBMemo1: TDBMemo;
    Panel1: TPanel;
    DBNavigator1: TDBNavigator;
    col_caption: TdxDBGridColumn;
    FormStorage1: TFormStorage;
    PopupMenu1: TPopupMenu;
    ActionList1: TActionList;
    act_Test_SQL: TAction;
    N1: TMenuItem;
    cmd: TADOCommand;
    Splitter1: TSplitter;
    act_Test_All_SQL: TAction;
    SQL1: TMenuItem;
    Panel2: TPanel;
    ADOTable1: TADOTable;
    DataSource1: TDataSource;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1DBColumn1: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn2: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn3: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn4: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn5: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn6: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure act_Test_SQLExecute(Sender: TObject);
    procedure qryAfterPost(DataSet: TDataSet);
    procedure qryNewRecord(DataSet: TDataSet);
  private
    FGridRegPath: string;
   // FConnectRec: TLoginRec;
    FLib, FAddr: string;

    FUpdate: boolean;

    function  OpenDbVersions(): boolean;

  public
    class procedure ExecDlg();
  end;

(*var
  dlg_Edit_versions: Tdlg_Edit_versions;
*)
//==============================================================
implementation    
{$R *.dfm}

const
  FLD_DATETIME = 'datetime';

class procedure Tdlg_Versions_Edit.ExecDlg;
begin
  with Tdlg_Versions_Edit.Create(Application) do
  begin
    if OpenDbVersions() then
      ShowModal;

    Free;
  end;
end;

//--------------------------------------------------------------
procedure Tdlg_Versions_Edit.FormCreate(Sender: TObject);
//--------------------------------------------------------------
begin
//  grid.Align:= alClient;
  dbmemo1.Align:= alClient;

(*
  dmMain.UseCustomConnection:= true;
  dmMain.CustomConnection:= AdoConnection1;

  cmd.Connection:= dmMain.CustomConnection;

  col_content_sql       .FieldName:= 'content_SqlServ_2000';
  col_enabled           .FieldName:= FLD_ENABLED;
  col_exec_each_time    .FieldName:= 'exec_each_time';
  col_date              .FieldName:= FLD_DATETIME;
  col_content_programmed.FieldName:= 'programmed_action_name';
  col_caption           .FieldName:= FLD_CAPTION;

  dbmemo1.DataField:= 'content_SqlServ_2000';

  FGridRegPath:= REGISTRY_FORMS + ClassName + grid.Name + '1';

  grid.LoadFromRegistry(FGridRegPath);
*)

end;

//--------------------------------------------------------------
procedure Tdlg_Versions_Edit.FormClose(Sender: TObject;
  var Action: TCloseAction);
//--------------------------------------------------------------
begin
 // Action:= caFree;
end;

//--------------------------------------------------------------
procedure Tdlg_Versions_Edit.FormDestroy(Sender: TObject);
//--------------------------------------------------------------
begin
  grid.SaveToRegistry(FGridRegPath);

(*  dmMain.UseCustomConnection:= false;
  dmMain.CustomConnection:= nil;
*)
end;

//--------------------------------------------------------------
function  Tdlg_Versions_Edit.OpenDbVersions(): boolean;
//--------------------------------------------------------------
begin

 (* dmMain.LoadConnectRecFromReg(FConnectRec);
  dmMain.LoadAdditionalConnectParamsFromReg(FConnectRec.Server, FLib, FAddr);

  with FConnectRec do begin
    Result:= dmMain.OpenByParams(Server, User, Password,
                'onega_version_updates', FLib, FAddr, IsUseWinAuth);

    if not Result then begin
      ErrorDlg('������ �����������: '+ dmMain.ErrorMsg);
      exit;
    end;
  end;

  Result:= db_OpenTable(qry, 'version_updates');
  if not Result then begin
    ErrorDlg('������: '+ gl_DB.ErrorMsg);
    exit;
  end;*)

end;

//--------------------------------------------------------------
procedure Tdlg_Versions_Edit.act_Test_SQLExecute(Sender: TObject);
//--------------------------------------------------------------
var
  sSql, sMsg: string;
  iRecords: Integer;

    function DoExec(): boolean;
    begin
     (* Result:= true;
      
      sSql:= qry.FieldByName(col_content_sql.FieldName).AsString;
      if sSql='' then exit;

      sSql:= Format('USE %s      '+
                    'BEGIN TRAN  '+
                    '%s          '+
                    'ROLLBACK    ', [ed_Test_DB.Text, sSql] );

      try
        cmd.CommandText := sSql;
        cmd.Execute (iRecords);
        sMsg:= Format('���������� ������� "%s" ������ �� �������. ��������� ������� %d',
                [qry.FieldByName(FLD_CAPTION).AsString, iRecords]);
        Result:= true;
      except
        on e: Exception do begin
          Result:= false;
          sMsg:= Format('������ ��� ����������� ������� "%s". �����: %s',
                   [qry.FieldByName(FLD_CAPTION).AsString, e.Message]);
        end;
      end;*)
    end;

var
  sRes: string;
begin
  sMsg:= '';
  sRes:= '';
(*
  if sender = act_Test_SQL then begin
    DoExec();
    ShowMessage(sMsg);
  end;

  if sender = act_Test_All_SQL then begin
    CursorSql;
    qry.disableControls;
    with qry do begin
      First;
      while not EOF do
      begin
        if not DoExec() then
          sRes:= sRes + sMsg + CRLF;

        Next;
      end;
    end;

    ShowMessage(sRes);
    CursorDefault;
    qry.enableControls;
  end;*)

end;

//--------------------------------------------------------------
procedure Tdlg_Versions_Edit.qryAfterPost(DataSet: TDataSet);
//--------------------------------------------------------------
begin
(*  if FUpdate then
    exit;

  FUpdate:= true;
  db_SaveDatasetPosition([qry]);
  qry.requery;
  db_RestoreDatasetPosition([qry]);
  FUpdate:= false;*)

end;

//--------------------------------------------------------------
procedure Tdlg_Versions_Edit.qryNewRecord(DataSet: TDataSet);
//--------------------------------------------------------------
begin
  DataSet[FLD_DATETIME]:= AsString( Double( Now ) );
end;

end.
