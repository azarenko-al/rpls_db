unit dm_Structure;

interface

uses
  SysUtils, Classes, DB, ADODB;

type
  TdmStructure = class(TDataModule)
    ds_Tables: TDataSource;
    ds_Views: TDataSource;
    ds_CK: TDataSource;
    ds_FK: TDataSource;
    ds_Triggers: TDataSource;
    ds_Indexes: TDataSource;
    ds_Owner: TDataSource;
    ds_SP: TDataSource;
    t_Owner: TADOTable;
    tbl_Triggers: TADOTable;
    tbl_SP: TADOTable;
    tbl_FK: TADOTable;
    tbl_indexes: TADOTable;
    tbl_CK: TADOTable;
    tbl_Views: TADOTable;
    tbl_Tables: TADOTable;
    tbl_TableFields: TADOTable;
    ds_TableFields: TDataSource;
    MDBConnection: TADOConnection;
    ds_OBJECTS: TDataSource;
    t_OBJECTS: TADOTable;
    ds_Func: TDataSource;
    t_Func: TADOTable;
    ds_DatabaseInfo: TDataSource;
    t_Database_Info: TADOTable;
    ds_OBJECTS_main: TDataSource;
    t_OBJECTS_main: TADOTable;
    t_sys_Projects: TADOTable;
    ds_Projects: TDataSource;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure Open;
    { Private declarations }
  public
    class procedure Init1;
    { Public declarations }
  end;

var
  dmStructure: TdmStructure;

implementation

{$R *.dfm}


class procedure TdmStructure.Init1;
begin
  if not Assigned(dmStructure) then
    dmStructure := TdmStructure.Create(Application);
end;



procedure TdmStructure.DataModuleCreate(Sender: TObject);
begin

  tbl_Tables.TableName      := 'Tables';
  tbl_TableFields.TableName := 'Table_Fields';
  tbl_Views.TableName       := 'Views';
  tbl_Triggers.TableName    := 'Triggers';
  tbl_SP.TableName          := 'StoredProcs';
  t_Func.TableName          := 'Func';
  tbl_indexes.TableName     := 'indexes';
  tbl_FK.TableName          := 'FK';
  tbl_CK.TableName          := 'CK';

  t_OBJECTS.TableName       := '_OBJECTS';
  t_Owner.TableName         := 'Owner';

  t_OBJECTS_main.TableName  := '_OBJECTS_main';

  t_Database_Info.TableName  := 'Database_Info';
end;



// ---------------------------------------------------------------
procedure TdmStructure.Open;
// ---------------------------------------------------------------
begin
  db_SetComponentADOConnection(Self, dmMDB.ADOConnection1);

 // db_TableOpen1()


  tbl_Tables.Open;
  tbl_TableFields.Open;
  tbl_Views.Open;
  tbl_Triggers.Open;
  tbl_SP.Open;
  t_Func.Open;
  tbl_indexes.Open;
  tbl_FK.Open;
  tbl_CK.Open;

  t_OBJECTS.Open;

  t_Owner.Open;
  t_Database_Info.Open;

  t_OBJECTS_main.Open;

 // t__sys_objects.Open;

(*
  tbl_Tables.TableName        := 'Tables';
  tbl_TableFields.TableName   := 'Table_Fields';
  tbl_Views.TableName         := 'Views';
  tbl_Triggers.TableName      := 'Triggers';
  tbl_StoredProcs.TableName   := 'StoredProcs';
  tbl_indexes.TableName       := 'indexes';
  tbl_FK.TableName            := 'FK';
  tbl_CK.TableName            := 'CK';

  t_OBJECTS.TableName         := '_OBJECTS';
  t_Status.TableName          := 'Status';

*)

 // qry_Status.Open;

end;



end.
