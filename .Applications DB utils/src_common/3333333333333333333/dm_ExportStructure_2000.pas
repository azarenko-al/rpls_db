unit dm_ExportStructure_2000;

interface

uses
  SysUtils, Classes, Forms, Db, AdoDb, Variants, StrUtils, Dialogs,

  dm_MDB,

  d_Progress,

  dm_Main_SQL,

  u_local_const,

  u_Log,

  u_func,
  u_db,
  u_const_db,

  u_sync_classes;

type
  TdmExportStructure_2000 = class(TDataModule)
    qry_Temp: TADOQuery;
    qry_Indexes: TADOQuery;
    qry_Tables: TADOQuery;
    qry_PKeys: TADOQuery;
    qry_TableFields: TADOQuery;
    q_DF: TADOQuery;
    q_PK: TADOQuery;
    ADOTable1: TADOTable;
    q_HelpText: TADOQuery;
    qry_SP: TADOQuery;
    q_Triggers: TADOQuery;
    qry_Views: TADOQuery;
    ADOQuery1: TADOQuery;
    q_Computed_columns: TADOQuery;
    sql_SysObjects: TADOQuery;
//    ADOConnection1: TADOConnection;
    procedure DataModuleCreate(Sender: TObject);
// TODO: Test
//  procedure Test;
  private
    FPrefix : string;
//    FADOConnection_ref: TADOConnection;
    FDBStructure_ref: TDBStructure;

    procedure ExportTableFields(aDataset: TDataSet);
    procedure ExportCKToDataSet(aDataset: TDataSet);
    procedure ExportStoredProcsToDataSet(aDataset: TDataSet; aRoutine_Type :
        string);
    procedure ExportViewsToDataSet(aDataset: TDataSet);
    procedure ExportTriggersToDataSet(aDataset: TDataSet);
    procedure ExportIndexesToDataSet(aDataset: TDataSet);
    procedure ExportFKToDataSet(aDataset: TDataSet);


    procedure EmptyTable(aTableName: string);
    procedure ExecDlg_start_proc;
    procedure ExportTable(aDataset: TDataSet);
    function Get_sp_HelpText(aName : string): string;
    procedure Log(aMsg: string);
    function NameIsCorrectForExport(aName : string): Boolean;
//    DatabaseName : string;


    procedure Progress2(aValue,aMax : Integer);
    procedure Run(aPrefix: string);



    procedure UpdateTable_Delete_Is_not_Exists(aTableName: string);
    procedure UpdateTable_Set_Is_Exists_False(aTableName: string);

// TODO: Open_PK
//  procedure Open_PK;
    procedure AfterRun;
    procedure BeforeRun;
    function OpenIndexesForTable(aTableName: string): Boolean;

  public


    procedure ExecDlg(aPrefix: string; aDBStructure: TDBStructure);

    function ExportVersionUpdates(aDataset: TDataSet): boolean;

    //(aDataset: TDataSet;
//        aNameFieldName, aTableIDFieldName: string);          // ; aUpdateRRL, aUpdate2G, aUpdate3G, aUpdateOthers: boolean
                   
    class procedure Init;

    procedure Load_DBStructure(aADOConnection: TADOConnection; aDBStructure:
        TDBStructure);

  end;

var
  dmExportStructure_2000: TdmExportStructure_2000;

//function dmExportStructure: TdmExportStructure;


//--------------------------------------------------------------------
implementation
 {$R *.DFM}

const
  FLD_IS_EXISTS = 'IS_EXISTS';

  FLD_XTYPE = 'XTYPE';


  FLD_PK_TABLE_NAME  ='PK_TABLE_NAME';
  FLD_PK_COLUMN_NAME ='PK_COLUMN_NAME';

  FLD_FK_TABLE_NAME  ='FK_TABLE_NAME';
  FLD_FK_COLUMN_NAME ='FK_COLUMN_NAME';

  FLD_IsDeleteCascade='IsDeleteCascade';
  FLD_IsUpdateCascade='IsUpdateCascade';

  FLD_index_description = 'index_description';


  FLD_CONSTRAINT_NAME = 'CONSTRAINT_NAME';

  FLD_routine_name = 'routine_name';
  FLD_routine_type ='routine_type';

  FLD_SCHEMA = 'SCHEMA';
  FLD_TABLE_SCHEMA = 'TABLE_SCHEMA';
  FLD_ROUTINE_SCHEMA = 'ROUTINE_SCHEMA';


  FLD_IsComputed    = 'IsComputed';
  FLD_Computed_Text = 'Computed_Text';





class procedure TdmExportStructure_2000.Init;
begin
  if not Assigned(dmExportStructure_2000) then
    dmExportStructure_2000:= TdmExportStructure_2000.Create(Application);

end;


procedure TdmExportStructure_2000.DataModuleCreate(Sender: TObject);
begin
 // TdmMain_SQL.Init;

  db_SetComponentADOConnection(Self, dmMain_SQL.ADOConnection1);
end;


procedure TdmExportStructure_2000.UpdateTable_Set_Is_Exists_False(aTableName:
    string);
var
  s: string;
begin
  s := Format('UPDATE %s SET is_exists=False', [aTableName]);
  dmMDB.ADOConnection1.Execute(s);
end;


procedure TdmExportStructure_2000.UpdateTable_Delete_Is_not_Exists(aTableName:
    string);
var
  s: string;
begin
  s := Format('Delete FROM %s WHERE is_exists=False', [aTableName]);
  dmMDB.ADOConnection1.Execute(s);
end;


procedure TdmExportStructure_2000.EmptyTable(aTableName: string);
begin
  dmMDB.ADOConnection1.Execute(Format('DELETE FROM %s', [aTableName]));
end;



//----------------------------------------------------------
procedure TdmExportStructure_2000.ExportTable(aDataset: TDataSet);
//----------------------------------------------------------
const

  SQL_SELECT_SQLSERVER_USER_TABLES_new =
    'select * from INFORMATION_SCHEMA.TABLES          '+
    'where (table_type=''base table'') AND            '+
    '       (table_name <> ''dtproperties'') AND      '+
    '       (table_name not like ''[_][_]%'')         '+
    '      and (table_name not like ''%[_]history'')  '+
    '      and (table_name not like ''%[_][_]'')      '+
    'order by table_name                              ';

var
  id: Integer;
  sTableName: string;
//  sXTYPE: string;
  obj: TDBObject;
 // s: string;

begin
  Assert(Assigned(FDBStructure_ref), 'Value not assigned');


//  s:=ReplaceStr(SQL_SELECT_SQLSERVER_USER_TABLES_new, '[%s]', DatabaseName);

//  s:=Format(SQL_SELECT_SQLSERVER_USER_TABLES_new, [DatabaseName]);
  db_OpenQuery(qry_Temp, SQL_SELECT_SQLSERVER_USER_TABLES_new);


  with qry_Temp do
    while not EOF do
  begin
    Progress2(RecNo, RecordCount);


//    sTableName := FieldValues[FLD_TABLE_SCHEMA];
    sTableName := FieldValues[FLD_TABLE_NAME];


    Log('Table: '+ sTableName);
    Progress2(RecNo, RecordCount);

    if not NameIsCorrectForExport(sTableName) then
    begin
      Next;
      Continue;
    end;

    obj := FDBStructure_ref.Tables.AddItem(otTable);
    obj.Name   := sTableName;
    obj.SCHEMA := FieldValues[FLD_TABLE_SCHEMA];
   // obj.Status := FieldValues[FLD_Status];


    if not aDataset.Locate(FLD_NAME, sTableName, []) then
       db_AddRecord (aDataset,
           [
            db_Par(FLD_NAME,   sTableName),
            db_Par(FLD_SCHEMA, FieldValues[FLD_TABLE_SCHEMA]),
            db_Par(FLD_XTYPE,  'U')
           ])
    else
       db_UpdateRecord (aDataset,
            [
            // db_Par(FLD_CONTENT,   sText),
             db_Par(FLD_IS_EXISTS, True),
             db_Par(FLD_XTYPE,  'U')
            ]);


 //      id:=aDataset[FLD_ID];

 //    UpdateTable_Set_Is_Exists_False (aTableName, aDataset[FLD_ID]);

//    db_UpdateRecord (aDataset, FLD_IS_EXISTS, True);

    Next;
  end;
end;

// ---------------------------------------------------------------
function TdmExportStructure_2000.NameIsCorrectForExport(aName : string): Boolean;
// ---------------------------------------------------------------
var
  s: string;
begin
  aName := LowerCase(aName);

  if IsStrInStrArray(aName, ['_objects','_object_fields','_db_version' ]) then
//  if (aName='_objects') or (aName='_object_fields') then
  begin
    Result := True;
    Exit;
  end;


  s:=LeftStr(aName,1);

  Result:= (s<>'-') and (s<>'_') and (Pos('11', aName)=0);

end;

//----------------------------------------------------------
procedure TdmExportStructure_2000.ExportTableFields(aDataset: TDataSet);
//----------------------------------------------------------
const

//  SQL_ALL_NEW =
//    'SELECT *,                                                                           '+
//    '   COLUMNPROPERTY(OBJECT_ID(c.TABLE_NAME),c.COLUMN_NAME,''IsIdentity'') as IsIdentity'+
//    '                                                                                     '+
//    'FROM  INFORMATION_SCHEMA.COLUMNS AS c                                                '+
//    '     JOIN  INFORMATION_SCHEMA.TABLES AS t  ON t.TABLE_NAME = c.TABLE_NAME            '+
//    'WHERE                                                       '+
//    '    (t.TABLE_TYPE = ''Base Table'') AND                     '+
//    '    (t.TABLE_NAME  NOT LIKE ''[_][_]%'') AND                '+
//    '    (c.COLUMN_NAME NOT LIKE ''[_][_]%'')                    '+
//    'order by t.TABLE_name                                       ';
//
//


  SQL_ALL =
    ' SELECT  TOP 100 PERCENT o.id AS table_id, o.name AS tablename,   '+
    '    c.colid, c.name, t.name AS type, c.length , c.iscomputed, '+ //AS size
    '    t.variable, t.usertype AS Usertype,               '+ // 0 AS PK, AS Variable
    '    ColumnProperty(o.id, c.name, ''IsIdentity'') AS IsIdentity,   '+
    '    c.isnullable AS isnullable                                    '+
    ' FROM  sysobjects o INNER JOIN                                    '+
    '       syscolumns c ON o.id = c.id INNER JOIN                     '+
    '       systypes t ON c.xusertype = t.xusertype                    '+
    ' WHERE (o.xtype = ''U'') AND                                      '+
    '       (o.name <> ''dtproperties'') AND                           '+
    '       (o.name <> ''_tables'') AND                                '+
    '       (o.name NOT LIKE ''[_][_]%'') AND                          '+
    '       (c.name NOT LIKE ''[_][_]%'')                              '+
    ' ORDER BY o.name, c.colid                                         ';


  SQL_SELECT_DF =
    ' SELECT TOP 100 PERCENT                             '+
    '   c.name   AS COLUMN_NAME,                         '+
    '        OBJECT_NAME(o.parent_obj) AS tablename ,    '+
    '   o.name   AS DEFAULT_NAME,                        '+
    '   com.text AS DEFAULT_CONTENT                      '+
    ' FROM sysobjects o INNER JOIN                       '+
    '      syscolumns c ON o.id = c.cdefault INNER JOIN  '+
    '      syscomments com ON o.id = com.id              '+
    ' WHERE o.xtype = ''D''                              '+
    ' ORDER BY tablename                                 ';




//SELECT     TOP 100 PERCENT OBJECT_NAME(dbo.syscolumns.id) AS OBJECT_NAME, dbo.syscolumns.name AS name, Comments.text AS text
//FROM         dbo.syscolumns INNER JOIN
//                      dbo.syscomments Comments ON dbo.syscolumns.id = Comments.id AND dbo.syscolumns.colid = Comments.number
//WHERE     (dbo.syscolumns.iscomputed = 1) AND (dbo.syscolumns.xtype = 62)



var
  b: Boolean;
  I: Integer;
  sTableName, sName: string;
  v: variant;
  obj: TDBTableField;
  s: string;
  oTable: TDBObject;

begin
  Assert(Assigned(FDBStructure_ref));



  db_OpenQuery(q_DF, SQL_SELECT_DF);

  db_OpenQuery(q_Computed_columns, q_Computed_columns.SQL.Text);

 // db_View(q_DF);

//  db_OpenQuery(qry_TableFields, SQL_ALL_NEW);


  if not db_OpenQuery(qry_TableFields, SQL_ALL) then
    Exit;


//  try
    with qry_TableFields do
      while not EOF do
    begin
      Progress2(RecNo, RecordCount);


      sName     := FieldValues[FLD_NAME];
      sTABLENAME:=FieldValues[FLD_TABLENAME];

   //   if Length(sName)<=3 then
   //     ShowMessage(sName);

      if (not NameIsCorrectForExport(sName)) or
         (not NameIsCorrectForExport(sTABLENAME))
      then
     // s:=LeftStr(sName,1);
     // if (s='-') or (s='_') then           // or (s='_')     '_'
      begin
        Next;
        Continue;
      end;


    sTableName :=FieldValues[FLD_TABLENAME];

    oTable := FDBStructure_ref.Tables.FindByName(sTableName);

    if not Assigned(oTable) then
    begin
      Next;
      Continue;
    end;


  //  if Assigned(oTable) then
    begin
      obj:=oTable.TableFields.AddItem;

      obj.Name          :=FieldValues[FLD_NAME];
      obj.TableName     :=FieldValues[FLD_TABLENAME];

      s:=FieldValues[FLD_TYPE];

      if s='TComment'  then  s:='varchar' else
      if s='TBoolean'  then  s:='bit'     else
      if s='TName'     then  s:='varchar' else
      if s='TName100'  then  s:='varchar' else
      if s='TName250'  then  s:='varchar' else
      if s='TKey'      then  s:='int' else
      if s='TIdentity' then  s:='int' else
    //  if s='TIdentity' then s:='int' else
      ;


      obj.TYPE_         :=s;
      obj.Length        :=FieldValues[FLD_Length];
//      obj.Size          :=FieldValues[FLD_Size];
      obj.IsVariableLen :=FieldValues[FLD_VARIABLE];
      obj.IsNullable    :=FieldValues[FLD_IsNullable];
      obj.IsIdentity   := FieldByName('IsIdentity').AsInteger=1;
      obj.IsComputed   := FieldByName('IsComputed').AsInteger=1;

      // ---------------------------------------------------------------
      if q_Computed_columns.Locate('OBJECT_NAME;NAME',
        VarArrayOf([FieldValues[FLD_TABLENAME], FieldValues[FLD_NAME]] ), [] ) then
      begin
        obj.ComputedText := q_Computed_columns.FieldByName(FLD_TEXT).AsString;
        obj.TYPE_ :='';
        obj.Length:=0;
      end;


      // ---------------------------------------------------------------
      if q_DF.Locate('TABLENAME;COLUMN_NAME',
        VarArrayOf([FieldValues[FLD_TABLENAME],FieldValues[FLD_NAME]] ), [] ) then
      begin
        obj.DEFAULT_NAME    := q_DF.FieldByName(FLD_DEFAULT_NAME).AsString;
        obj.DEFAULT_CONTENT := q_DF.FieldByName(FLD_DEFAULT_CONTENT).AsString;


        if (LeftStr(obj.DEFAULT_CONTENT,2)='((') and
           (RightStr(obj.DEFAULT_CONTENT,2) = '))')
        then
          obj.DEFAULT_CONTENT :=Copy(obj.DEFAULT_CONTENT, 2, Length(obj.DEFAULT_CONTENT)-2);

      end;

    end;


 //   s:=

 // db_View(aDataset);

    if not aDataset.Locate(FLD_TABLENAME+';'+FLD_NAME,
       VarArrayOf([sTableName, sName]), []) then

      with aDataset do
        begin
         // Log(sTableName+'-'+ sName);

          b:=db_AddRecord (aDataset,
            [
             db_Par(FLD_TABLENAME,   obj.TABLENAME),
             db_Par(FLD_NAME,        obj.NAME),

             db_Par(FLD_TYPE,        obj.Type_),
             db_Par(FLD_LENGTH,      obj.Length),
             db_Par(FLD_VARIABLE,    obj.IsVariableLen),
             db_Par(FLD_isnullable,  obj.isnullable),

             db_Par(FLD_IsComputed,      obj.IsComputed),
             db_Par(FLD_Computed_Text,   obj.ComputedText),

             db_Par(FLD_IDENTITY,        obj.IsIdentity),

             db_Par(FLD_DEFAULT_NAME,    obj.DEFAULT_NAME),
             db_Par(FLD_DEFAULT_CONTENT, obj.DEFAULT_CONTENT)
            ]);

       //   if not b then
        //    ShowMessage(sTableName+'-'+ sName);
        end;

      Next;

   end;

end;

//--------------------------------------------------------------------
function TdmExportStructure_2000.Get_sp_HelpText(aName : string): string;
//--------------------------------------------------------------------
(*
http://stackoverflow.com/questions/1727414/get-the-results-of-sp-helptext-as-a-single-string


ou can try something like this
DECLARE @Table TABLE(
        Val VARCHAR(MAX)
)

INSERT INTO @Table EXEC sp_helptext 'sp_configure'

DECLARE @Val VARCHAR(MAX)

SELECT  @Val = COALESCE(@Val + ' ' + Val, Val)
FROM    @Table

SELECT @Val

*)

begin
  db_OpenQuery(q_HelpText, Format('exec sp_HelpText %s', [aNAME]) );
  Result:= '';

  with q_HelpText do
    while not EOF do
  begin
    Result := Result+ q_HelpText.Fields[0].AsString;
    Next;
  end;

  Result := Trim(Result);


  ASSERT(Result<>'');
end;


//--------------------------------------------------------------------
procedure TdmExportStructure_2000.ExportCKToDataSet(aDataset: TDataSet);
//--------------------------------------------------------------------
const
  SQL_SELECT_CK_new =
    'select * from INFORMATION_SCHEMA.TABLE_CONSTRAINTS '+
    'where CONSTRAINT_type=''check''                    ';

var
  s: string;
  sTABLENAME: string;

  oTable, obj: TDBObject;
  sNAME: string;
  sTABLE_SCHEMA: string;
  sText: string;

begin
  Assert(Assigned(FDBStructure_ref), 'Value not assigned');

 // s:=ReplaceStr(SQL_SELECT_CK_new, '[%s]', DatabaseName);

  //  s:=Format(SQL_SELECT_CK_new, [DatabaseName]);


  db_OpenQuery (qry_Temp, SQL_SELECT_CK_new);


//  db_View(qry_Temp);

//  db_OpenQuery (qry_Temp, s);

  with qry_Temp do
    while not EOF do
    begin
      sNAME     :=FieldValues[FLD_CONSTRAINT_NAME];
      sTABLENAME:=FieldValues[FLD_TABLE_NAME];

      Log('View: '+ sName);
      Progress2(RecNo, RecordCount);


      sTABLE_SCHEMA := FieldValues[FLD_TABLE_SCHEMA];

      sText := Get_sp_HelpText(sName);

      oTable := FDBStructure_ref.Tables.FindByName(sTableName);

      if Assigned(oTable) then
      begin
        obj:=oTable.CK.AddItem(otCK);

        obj.Name     :=sNAME;
        obj.TABLENAME:=sTABLENAME;
        obj.SCHEMA   :=sTABLE_SCHEMA;

        obj.Text :=sTEXT;

//      if not aDataset.Locate(FLD_NAME, sName, []) then

        if not aDataset.Locate(FLD_NAME, sName, []) then
          db_AddRecord(aDataset,
            [
             db_Par(FLD_NAME,      sNAME),
             db_Par(FLD_TABLENAME, sTABLENAME),
             db_Par(FLD_SCHEMA,    sTABLE_SCHEMA),

             db_Par(FLD_CONTENT,   sText),
             db_Par(FLD_XTYPE,     'ck')
            ])
        else
          db_UpdateRecord (aDataset,
            [
             db_Par(FLD_CONTENT,   sText),
             db_Par(FLD_IS_EXISTS, True),

             db_Par(FLD_XTYPE,     'ck')
            ]);


      // UpdateTable_Set_Is_Exists_False (aTableName, aDataset[FLD_ID]);

    //  db_UpdateRecord (aDataset, FLD_IS_EXISTS, True);

      end;

      Next;
    end;
end;

//--------------------------------------------------------------------
procedure TdmExportStructure_2000.ExportStoredProcsToDataSet(aDataset: TDataSet;
    aRoutine_Type : string);
//--------------------------------------------------------------------
//const
//
//
//SQL_SELECT_ROUTINES_2000 =
//'  select '+
//'        * '+
//'    from '+
//'        INFORMATION_SCHEMA.ROUTINES as ISR '+
//'    where '+
//'        ISR.ROUTINE_TYPE = ''PROCEDURE'' and '+
//'        ObjectProperty (Object_Id (ISR.ROUTINE_NAME), ''IsMSShipped'') = 0 and '+
//'        ( '+
//'            select '+
//'                major_id '+
//'            from '+
//'                sys.extended_properties '+
//'            where '+
//'                major_id = object_id(ISR.ROUTINE_NAME) and '+
//'                minor_id = 0 and '+
//'                class = 1 and '+
//'                name = N''microsoft_database_tools_support'' '+
//'        ) is null '+
//'    order by '+
//'        ISR.ROUTINE_CATALOG, '+
//'        ISR.ROUTINE_SCHEMA, '+
//'        ISR.ROUTINE_NAME ';



const
  
  SQL_SELECT_ROUTINES_2000 =
    'select * from INFORMATION_SCHEMA.ROUTINES  '+
    ' WHERE                                       '+
    '     (routine_name NOT LIKE N''dt_%'') AND   '+
    '     (routine_name NOT LIKE ''[_]%'')        '+
    'order by routine_name                        ';

label
  while_end;

var
  sName: string;
  obj: TDBObject;
  s: string;
  sRoutine_Type: string;
  sText: string;
  sXType: string;

begin
  Assert(Assigned(FDBStructure_ref), 'Value not assigned');


//  Assert(DatabaseName<>'', 'Value <=0');

 // s:=ReplaceStr(SQL_SELECT_ROUTINES_2000, '[%s]', DatabaseName);

 // s:=Format(SQL_SELECT_ROUTINES_2000, [DatabaseName]);
//  db_OpenQuery (qry_SP, qry_SP.SQL.Text);
  db_OpenQuery (qry_SP, SQL_SELECT_ROUTINES_2000);


 // db_View(qry_SP);


  with qry_SP do
    while not EOF do
    begin
      sName:= FieldByName(FLD_routine_name).AsString;

      Log('SP: '+ sName);
      Progress2(RecNo, RecordCount);



      if (not NameIsCorrectForExport(sName)) then
      begin
        goto while_end;
//        Next;
  //      Continue;
      end;


      sText:=Get_sp_HelpText(sName);

      sText := ReplaceStr(sText, '[dbo]', 'dbo');
      sText := ReplaceStr(sText, '['+sNAME+']', sNAME);


      sRoutine_Type :=FieldByName(FLD_routine_type).AsString;

      if not Eq(aRoutine_Type, sRoutine_Type) then
      begin
        goto while_end;

//        Next;
 //       Continue;
      end;



//      if Eq(sRoutine_Type, 'function') then
      if Eq(aRoutine_Type, 'function') then
      begin
        sXType :='F';
        obj:=FDBStructure_ref.StoredFuncs.AddItem(otStoredFunc)
      end else
      begin
        sXType :='SP';
        obj:=FDBStructure_ref.StoredProcs.AddItem(otStoredPROC);

      end;

      obj.Name := sName;
      obj.Text := sText;
      obj.SCHEMA := FieldValues[FLD_ROUTINE_SCHEMA];
 //     obj.Status := FieldValues[FLD_Status];

      if not aDataset.Locate(FLD_NAME, sName, []) then
         db_AddRecord (aDataset,
            [db_Par(FLD_NAME,         sName),
             db_Par(FLD_Routine_Type, sRoutine_Type),
             db_Par(FLD_SCHEMA,       FieldValues[FLD_ROUTINE_SCHEMA]),

             db_Par(FLD_XTYPE,     sXType),

             db_Par(FLD_CONTENT,    sText)
            ])
     else
       db_UpdateRecord (aDataset,
          [
           db_Par(FLD_CONTENT,    sText),
           db_Par(FLD_IS_EXISTS,  True),

           db_Par(FLD_XTYPE,     sXType)
          ]);


   //  UpdateTable_Set_Is_Exists_False (aTableName, aDataset[FLD_ID]);

    //  db_UpdateRecord (aDataset, FLD_IS_EXISTS, True);

  while_end:

      Next;
    end;
end;

//--------------------------------------------------------------------
procedure TdmExportStructure_2000.ExportViewsToDataSet(aDataset: TDataSet);
//--------------------------------------------------------------------
const
  SQL_SELECT_VIEWS_new =
    'SELECT * FROM INFORMATION_SCHEMA.views          '+
    ' WHERE  (table_name NOT LIKE ''[_]%'')     AND  '+
    '        (table_name <> ''sysconstraints'') AND  '+
    '        (table_name <> ''syssegments''	)        '+
    ' ORDER BY table_name                            ';


var
  i: Integer;
  sSql, sName: string;
//  sXTYPE: string;

  obj: TDBObject;
  s: string;
  sText: string;

begin
  Assert(Assigned(FDBStructure_ref), 'Value not assigned');

//  db_OpenQuery (qry_Temp, SQL_SELECT_VIEWS);
 // s:=Format(SQL_SELECT_VIEWS_new, [DatabaseName]);

 // s:=ReplaceStr(SQL_SELECT_VIEWS_new, '[%s]', DatabaseName);


  db_OpenQuery (qry_Temp, SQL_SELECT_VIEWS_new);


  with qry_Temp do
    while not EOF do
    begin
      sName:= FieldValues[FLD_TABLE_NAME];

      Log('View: '+ sName);
      Progress2(RecNo, RecordCount);


      if not NameIsCorrectForExport(sName) then
      begin
        Next;
        Continue;
      end;

      sText:=Get_sp_HelpText(sName);

      obj:=FDBStructure_ref.Views.AddItem(otView);

      obj.Name   := sNAME;
      obj.Text   := sText;
      obj.SCHEMA := FieldValues[FLD_TABLE_SCHEMA];



//  aStr1 := ReplaceStr(aStr1, '[dbo]', 'dbo');
//  aStr2 := ReplaceStr(aStr2, '[dbo]', 'dbo');

//  if aObjectName<>'' then
//  begin
      sText := ReplaceStr(sText, '[dbo]', 'dbo');
      sText := ReplaceStr(sText, '['+sNAME+']', sNAME);

//  end;




   //   obj.Status := FieldValues[FLD_Status];


      if not aDataset.Locate(FLD_NAME, sName, []) then
        db_AddRecord (aDataset,
                [
                 db_Par(FLD_NAME,   sName),
                 db_Par(FLD_SCHEMA, obj.SCHEMA),
                 db_Par(FLD_CONTENT, sText),
                 db_Par(FLD_XTYPE,  'v')
                ])
      else
        db_UpdateRecord (aDataset,
                  [
                   db_Par(FLD_CONTENT,   sText),
                   db_Par(FLD_IS_EXISTS, True),

                   db_Par(FLD_XTYPE,  'v')
                  ]);

      Next;
    end;
end;

//--------------------------------------------------------------------
procedure TdmExportStructure_2000.ExportTriggersToDataSet(aDataset: TDataSet);
//--------------------------------------------------------------------
const
  SQL_SELECT_TRIGGERS =
    ' SELECT DISTINCT name, '+
    '        OBJECT_NAME(parent_obj) AS tablename  ' +
//    '        objectproperty(object_id(name), ''ExecIsTriggerDisabled'') as is_Disabled '+
    '  FROM   sysobjects                            '+
    '  WHERE  (xtype = ''TR'') AND                                '+
    '         (name not like ''[_][_]%'')                         '+
    '        and (objectproperty(object_id(name), ''ExecIsTriggerDisabled'') =0) '+
    ' ORDER BY name                                            ';

var
//  sSql,
  sName: string;
//  sXTYPE: string;
  oTable, obj: TDBObject;
  s: string;
  sTableName: string;
  sText: string;

begin
  Assert(Assigned(FDBStructure_ref), 'Value not assigned');

  db_OpenQuery (qry_Temp, SQL_SELECT_TRIGGERS);

 // db_View(qry_Temp);


  with qry_Temp do
    while not EOF do
    begin
      Progress2(RecNo, RecordCount);

      sName := FieldValues[FLD_NAME];

      if Pos('_audit_', LowerCase(sName))> 0then
      begin
        Next;
        Continue;
      end;


      Log('Trigger: '+ sName);
      Progress2(RecNo, RecordCount);



   //   sXTYPE:= aDataset.FieldByName(FLD_xtype).AsString;

      sTableName :=FieldValues[FLD_TABLENAME];

     // oTable := FDBStructure_ref.Tables.FindByName(sTableName);

      obj := FDBStructure_ref.Triggers.FindByName(sName);

      if not Assigned(obj) then
      begin
//        obj:=oTable.Triggers.AddItem(otTrigger);
        obj:=FDBStructure_ref.Triggers.AddItem(otTrigger);

        obj.Name      := FieldValues[FLD_NAME];
        obj.TableName := FieldValues[FLD_TABLENAME];

        sText  := Get_sp_HelpText(sName);

        sText := ReplaceStr(sText, '[dbo]', 'dbo');
        sText := ReplaceStr(sText, '['+obj.Name+']', obj.Name);

        obj.Text := sText;

      end;


      if not aDataset.Locate(FLD_NAME, FieldValues[FLD_NAME], []) then
        db_AddRecord (aDataset,
            [
             db_Par(FLD_NAME, obj.Name),
             db_Par(FLD_TABLENAME,  obj.TableName),
             db_Par(FLD_CONTENT,    obj.Text),
             db_Par(FLD_XTYPE,      'tr')
            ])
      else
        db_UpdateRecord(aDataset,
            [
             db_Par(FLD_CONTENT,    obj.Text),
             db_Par(FLD_IS_EXISTS,  True),

             db_Par(FLD_XTYPE,      'tr')
            ]);

      Next;
    end;

end;


//--------------------------------------------------------------------
procedure TdmExportStructure_2000.ExportIndexesToDataSet(aDataset: TDataSet);
//--------------------------------------------------------------------

    // ---------------------------------------------------------------
    procedure DoInsertIndexes(aTableName : string);
    // ---------------------------------------------------------------
    var

      sIndexName,  sIndexKeys, sDescription : string;

      bIndexIsPK, bIndexIsUnique: boolean;

      oTable, obj: TDBObject;
      s: string;


    begin
        //  with qry_Indexes do
          while (not qry_Indexes.EOF) do
          begin
            sIndexName := qry_Indexes.FieldByName('index_name').AsString;
            sIndexKeys := qry_Indexes.FieldByName('index_keys').AsString;
    //        sIndexKeys := ReplaceStr(sIndexKeys, '(-)', ' DESC');

            sDescription := qry_Indexes.FieldByName('index_description').AsString;

            bIndexIsPK    := (Pos('primary key', sDescription)>0);
            bIndexIsUnique:= (Pos('unique key',  sDescription)>0);


            oTable := FDBStructure_ref.Tables.FindByName(aTableName);

            if Assigned(oTable) then
            begin

(*              if bIndexIsPK then
                obj:=oTable.PrimaryKeys.AddItem(otPrimaryKey)
              else
*)
              obj:=oTable.Indexes.AddItem(otIndex);

              obj.Name := sIndexName;
              obj.TableName :=aTableName;


             // obj.Index.IS_PRIMARY_KEY := bIndexIsPK;
              obj.Index.IS_UNIQUE_KEY  := bIndexIsUnique;
              obj.SetIndex_INDEX_KEYS(sIndexKeys);



          //  db_View(qry_Indexes);

          //  assert(aDataset.Active);

            if not aDataset.Locate(FLD_TABLENAME+';'+FLD_NAME, VarArrayOf([aTableName, sIndexName]), []) then
              db_AddRecord (aDataset,
                  [
                   db_Par(FLD_TABLENAME,       aTableName),
                   db_Par(FLD_NAME,            sIndexName),

                   db_Par(FLD_index_description, sDescription),
                   db_Par(FLD_IS_PRIMARY_KEY, bIndexIsPK),
                   db_Par(FLD_IS_UNIQUE_KEY,  bIndexIsUnique),
                   db_Par(FLD_INDEX_KEYS,     sIndexKeys)
                   ])
            else
              db_UpdateRecord (aDataset,
                  [
                   db_Par(FLD_index_description, sDescription),
                   db_Par(FLD_IS_PRIMARY_KEY, bIndexIsPK),
                   db_Par(FLD_IS_UNIQUE_KEY,  bIndexIsUnique),
                   db_Par(FLD_INDEX_KEYS,     sIndexKeys),

                   db_Par(FLD_IS_EXISTS,  True)
                  ]);

         //   UpdateTable_Set_Is_Exists_False (aTableName, aDataset[FLD_ID]);

          //  db_UpdateRecord (aDataset, FLD_IS_EXISTS, True);

                   end;
              qry_Indexes.Next;
            end;
    end;    //


const
  SQL_SEL_TABLES_WITH_INDEXES_new =
  ' SELECT distinct so.name AS name                         '+
  ' FROM  sysindexes si                                     '+
  ' INNER JOIN sysobjects so ON si.id = so.id               '+
//  ' WHERE   (si.indid > 0) AND (si.indid < 255) AND ((si.status & 64)=0) AND    '+
  ' WHERE    (si.indid > 0) AND (si.indid < 255) AND ((si.status & 64)=0) AND   '+
  '         (so.type = ''U'') AND (so.name NOT LIKE ''[_][_]%'') AND            '+
  '         (so.name NOT LIKE N''dt_%'')                                        '+
  ' ORDER BY so.name                                                            ';

(*  SQL_SEL_OBJECTS_WITH_INDEXES =
    ' SELECT TOP 100 PERCENT so.name AS tablename, so.id AS table_id,                       '+
    '         si.name AS index_name, si.indid                                              '+
    ' FROM  sysindexes si INNER JOIN sysobjects so ON si.id = so.id                         '+
    ' WHERE   (si.indid > 0) AND (si.indid < 255) AND ((si.status & 64)=0) AND              '+
    '         (so.type = ''U'') AND (so.name NOT LIKE ''[_][_]%'') AND                      '+
    '         (so.name NOT LIKE N''dt_%'')                                                  '+
    ' ORDER BY so.name                                                                      ';

*)
(*
  SQL_SELECT_SQLSERVER_USER_TABLES_new =
    'select * from INFORMATION_SCHEMA.TABLES    '+
    'where (table_type=''base table'') AND      '+
    '       (table_name <> ''dtproperties'') AND      '+
    '       (table_name not like ''[_][_]%'')         '+
    'order by table_name                        ';

*)


var
  sTableName: string;

begin
  Assert(Assigned(FDBStructure_ref), 'Value not assigned');

  db_OpenQuery (qry_Tables, SQL_SEL_TABLES_WITH_INDEXES_new);

//  db_OpenQuery (qry_Tables, SQL_SELECT_SQLSERVER_USER_TABLES_new);


 // db_View(qry_Tables);


 // sTableName:= '';

  with qry_Tables do
    while not EOF do
  begin
    Progress2(RecNo, RecordCount);

    sTableName:=FieldByName(FLD_NAME).AsString;

    Log('Indexes for table: '+ sTableName);
    Progress2(RecNo, RecordCount);


//    sTableName:=FieldByName(FLD_TABLE_NAME).AsString;
 //   sIndexName:=FieldByName('index_name').AsString;

    if not NameIsCorrectForExport(sTableName) then
    begin
      Next;
      Continue;
    end;


    if not OpenIndexesForTable(sTableName) then
    begin
       g_Log.Error('IndexesForTable: '+sTableName);

      Next;
      Continue;
    end;


    DoInsertIndexes (sTableName);

    Next;
  end;
end;


//--------------------------------------------------------------------
procedure TdmExportStructure_2000.ExportFKToDataSet(aDataset: TDataSet);
//--------------------------------------------------------------------
const
   SQL_SEL_FK =
      'SELECT TOP 100 PERCENT                                                '+
      '   OBJECT_NAME(sf.rkeyid)    AS PK_TABLE_NAME,                            '+
      '   col_name(sf.rkeyid, rkey) AS pk_column_name,                        '+
      '   OBJECT_NAME(sf.fkeyid)    AS fk_table_name,                            '+
      '   col_name(sf.fkeyid, fkey) AS fk_column_name,                        '+
      '   ObjectProperty(constid, ''CnstIsUpdateCascade'') as IsUpdateCascade,   '+
      '   ObjectProperty(constid, ''CnstIsDeleteCascade'') as IsDeleteCascade,   '+
      '   OBJECT_NAME(constid) AS Name                                    '+
      'FROM  sysforeignkeys sf INNER JOIN                                    '+
      '      sysobjects so ON sf.fkeyid = so.id                              '+
      'WHERE (so.xtype = ''U'') AND (so.name not like ''[_][_]%'')           '+
      'ORDER BY so.name                                                      ';

var

  pk_table_name, pk_column_name,
  fk_table_name, fk_column_name, sName: string;

  obj: TDBObject;

  oTable1,oTable2: TDBObject;
begin
  db_OpenQuery (qry_Tables, SQL_SEL_FK);

  with qry_Tables do
    while not EOF do
  begin

    sName       := FieldByName(FLD_Name).AsString;

    Log('FK: '+ sName);
    Progress2(RecNo, RecordCount);


    pk_table_name  := FieldByName('PK_TABLE_NAME').AsString;
    pk_column_name := FieldByName('pk_column_name').AsString;

    fk_table_name  := FieldByName('fk_table_name').AsString;
    fk_column_name := FieldByName('fk_column_name').AsString;

    // only key for existed tables
    oTable1 := FDBStructure_ref.Tables.FindByName(pk_table_name);
    oTable2 := FDBStructure_ref.Tables.FindByName(fk_table_name);


    if Assigned(oTable1) or (Assigned(oTable2)) then
    begin
      obj:=FDBStructure_ref.FK.AddItem(otFK);

      obj.Name              := sName;

      obj.FK.PK_TABLE_NAME  := pk_table_name;
      obj.FK.PK_COLUMN_NAME := pk_column_name;

      obj.FK.FK_TABLE_NAME  := fk_table_name;
      obj.FK.FK_COLUMN_NAME := fk_column_name;

      obj.FK.IsDeleteCascade := FieldByName('IsDeleteCascade').AsInteger=1;
      obj.FK.IsUpdateCascade := FieldByName('IsUpdateCascade').AsInteger=1;

      if not aDataset.Locate(FLD_NAME, sName, []) then
        db_AddRecord (aDataset,
          [
           db_Par(FLD_NAME,           sName) ,

           // -------------------------
           db_Par(FLD_PK_TABLE_NAME,   obj.FK.pk_table_name),
           db_Par(FLD_PK_COLUMN_NAME,  obj.FK.pk_column_name),

           db_Par(FLD_FK_TABLE_NAME,   obj.FK.fk_table_name),
           db_Par(FLD_FK_COLUMN_NAME,  obj.FK.fk_column_name),

           db_Par(FLD_IsDeleteCascade,  obj.FK.IsDeleteCascade),
           db_Par(FLD_IsUpdateCascade,  obj.FK.IsUpdateCascade),

           db_Par(FLD_XTYPE,     'FK')

          ])
     else
       db_UpdateRecord (aDataset,
          [
           db_Par(FLD_PK_TABLE_NAME,   obj.FK.pk_table_name),
           db_Par(FLD_PK_COLUMN_NAME,  obj.FK.pk_column_name),

           db_Par(FLD_FK_TABLE_NAME,   obj.FK.fk_table_name),
           db_Par(FLD_FK_COLUMN_NAME,  obj.FK.fk_column_name),

           db_Par(FLD_IsDeleteCascade,  obj.FK.IsDeleteCascade),
           db_Par(FLD_IsUpdateCascade,  obj.FK.IsUpdateCascade),

           db_Par(FLD_IS_EXISTS,  True),

           db_Par(FLD_XTYPE,     'FK')
          ]);

//      UpdateTable_Set_Is_Exists_False (aTableName, aDataset[FLD_ID]);

  //    db_UpdateRecord (aDataset, FLD_IS_EXISTS, True);

    end;

    Next;
  end;
end;


// ---------------------------------------------------------------
procedure TdmExportStructure_2000.Load_DBStructure(aADOConnection: TADOConnection;
    aDBStructure: TDBStructure);
// ---------------------------------------------------------------

  // ---------------------------------------------------------------
    procedure Do_CK(aDataset: TDataSet);
    // ---------------------------------------------------------------
    var obj: TDBObject;
        i: Integer;
       sTableName: string;
       oTable: TDBObject;

    begin
//      i:=aDataset.RecordCount;

      aDataset.First;

      with aDataset do
         while not EOF do
       begin
          sTableName :=FieldValues[FLD_TABLENAME];

          oTable := aDBStructure.Tables.FindByName(sTableName);
          Assert(Assigned(oTable), 'Value not assigned');

          obj:=oTable.CK.AddItem(otCK);
          obj.TableName := sTableName;
          obj.Name := FieldValues[FLD_Name];
          obj.Text := FieldValues[FLD_CONTENT];

      //    obj.Owner := FieldByName(FLD_Owner).AsString;

          Next;
       end;
    end;

    // ---------------------------------------------------------------
    procedure Do_FK(aDataset: TDataSet);
    // ---------------------------------------------------------------
    var obj: TDBObject;
      i: Integer;
    begin
      aDataset.First;

      with aDataset do
         while not EOF do
       begin
          obj:=aDBStructure.FK.AddItem(otFK);
          obj.Name      := FieldValues[FLD_Name];

          obj.FK.PK_TABLE_NAME   := FieldValues[FLD_PK_TABLE_NAME];
          obj.FK.PK_COLUMN_NAME  := FieldValues[FLD_PK_COLUMN_NAME];

          obj.FK.FK_TABLE_NAME   := FieldValues[FLD_FK_TABLE_NAME];
          obj.FK.FK_COLUMN_NAME  := FieldValues[FLD_FK_COLUMN_NAME];

          obj.FK.IsDeleteCascade  := FieldValues[FLD_IsDeleteCascade];
          obj.FK.IsUpdateCascade  := FieldValues[FLD_IsUpdateCascade];

          obj.Owner := FieldByName(FLD_Owner).AsString;

          Next;
       end;
    end;


    // ---------------------------------------------------------------
    procedure Do_Views(aDataset: TDataSet);
    // ---------------------------------------------------------------
    var obj: TDBObject;
    begin
      aDataset.First;

       with aDataset do
         while not EOF do
       begin
         obj := aDBStructure.Views.AddItem(otView);
         obj.Name:=FieldValues[FLD_Name];
         obj.Text:=FieldValues[FLD_CONTENT];

         obj.Owner := FieldByName(FLD_Owner).AsString;

         Next;
       end;
    end;

    // ---------------------------------------------------------------
    procedure Do_StoredProcs(aDataset: TDataSet);
    // ---------------------------------------------------------------
    var obj: TDBObject;
      sRoutine_Type: string;
    begin
      aDataset.First;

      with aDataset do
         while not EOF do
       begin
          sRoutine_Type:=FieldValues[FLD_Routine_Type];

       //   if Eq(sRoutine_Type, 'function') then
         //   obj:=aDBStructure.StoredFuncs.AddItem(otStoredFunc)
        //  else
            obj:=aDBStructure.StoredProcs.AddItem(otStoredPROC);

          obj.Name:=FieldValues[FLD_Name];
          obj.Text:=FieldValues[FLD_CONTENT];

          obj.Owner := FieldByName(FLD_Owner).AsString;

         Next;
       end;
    end;


    // ---------------------------------------------------------------
    procedure Do_Func(aDataset: TDataSet);
    // ---------------------------------------------------------------
    var obj: TDBObject;
      sRoutine_Type: string;
    begin
      aDataset.First;

      with aDataset do
         while not EOF do
       begin
          sRoutine_Type:=FieldValues[FLD_Routine_Type];

//          if Eq(sRoutine_Type, 'function') then
            obj:=aDBStructure.StoredFuncs.AddItem(otStoredFunc) ;
  //        else
    //        obj:=aDBStructure.StoredProcs.AddItem(otStoredPROC);

          obj.Name:=FieldValues[FLD_Name];
          obj.Text:=FieldValues[FLD_CONTENT];

          obj.Owner := FieldByName(FLD_Owner).AsString;

         Next;
       end;
    end;


    // ---------------------------------------------------------------
    procedure Do_Trigger(aDataset: TDataSet);
    // ---------------------------------------------------------------
    var
      obj: TDBObject;
      bPrimaryKey: Boolean;
      sName: string;
      sTableName: string;
     // oTable: TDBObject;
    begin
      aDataset.First;

      with aDataset do
         while not EOF do
       begin
         sName :=FieldValues[FLD_NAME];
//         sTableName :=FieldValues[FLD_TABLENAME];

         obj := aDBStructure.Triggers.FindByName(sName);
         Assert(not Assigned(obj), 'Value not assigned');

         obj:=aDBStructure.Triggers.AddItem(otTRIGGER);

         obj.Name := FieldValues[FLD_NAME];
       //  obj.TableName := FieldValues[FLD_TableName];
         obj.Text := FieldValues[FLD_CONTENT];

        obj.Owner := FieldByName(FLD_Owner).AsString;

         Next;
       end;
    end;



    // ---------------------------------------------------------------
    procedure Do_tables(aDataset: TDataSet);
    // ---------------------------------------------------------------
    var obj: TDBObject;
      i: Integer;
    begin
      i:=aDataset.RecordCount;
      aDataset.First;

      with aDataset do
         while not EOF do
       begin
         obj := aDBStructure.Tables.AddItem(otTable);
         obj.Name   :=FieldValues[FLD_Name];

         obj.Owner := FieldByName(FLD_Owner).AsString;

         Next;
       end;
    end;


    // ---------------------------------------------------------------
    procedure Do_indexes(aDataset: TDataSet);
    // ---------------------------------------------------------------
    var
      obj: TDBObject;
      bPrimaryKey: Boolean;
      sTableName: string;
      oTable: TDBObject;

    begin
      aDataset.First;

      with aDataset do
         while not EOF do
       begin
          sTableName :=FieldValues[FLD_TABLENAME];

          oTable := aDBStructure.Tables.FindByName(sTableName);
          Assert(Assigned(oTable), 'Value not assigned');

         bPrimaryKey:= FieldByName(FLD_IS_PRIMARY_KEY).AsBoolean;


(*         if bPrimaryKey then
           obj:=oTable.PrimaryKeys.AddItem(otPrimaryKey)
         else
*)
         obj:=oTable.Indexes.AddItem(otIndex);


         obj.Name := FieldValues[FLD_NAME];
         obj.TableName :=FieldValues[FLD_TABLENAME];

         obj.Index.IS_PRIMARY_KEY := FieldByName(FLD_IS_PRIMARY_KEY).AsBoolean;
         obj.Index.IS_UNIQUE_KEY  := FieldByName(FLD_IS_UNIQUE_KEY).AsBoolean;


         obj.SetIndex_INDEX_KEYS(FieldByName(FLD_INDEX_KEYS).AsString);

         Next;
       end;
    end;


    // ---------------------------------------------------------------
    procedure Do_TableFields(aDataset: TDataSet);
    // ---------------------------------------------------------------
    var obj: TDBTableField;
       sTableName: string;
       oTable: TDBObject;
    begin
      aDataset.First;

      with aDataset do
         while not EOF do
       begin
          sTableName :=FieldValues[FLD_TABLENAME];

          oTable := aDBStructure.Tables.FindByName(sTableName);

          if Assigned(oTable) then
          begin
            obj:=oTable.TableFields.AddItem;

            obj.Name := FieldByName(FLD_NAME).AsString;

            obj.TableName       := FieldByName(FLD_TABLENAME).AsString;
            obj.TYPE_           := FieldByName(FLD_TYPE).AsString;
            obj.Length          := FieldByName(FLD_Length).AsInteger;
            obj.IsVariableLen   := FieldByName(FLD_VARIABLE).AsBoolean;
            obj.IsNullable      := FieldByName(FLD_IsNullable).AsBoolean;
            obj.IsIdentity      := FieldByName(FLD_IDENTITY).AsBoolean;

            obj.DEFAULT_NAME    := FieldByName(FLD_DEFAULT_NAME).AsString;
            obj.DEFAULT_CONTENT := FieldByName(FLD_DEFAULT_CONTENT).AsString;

            obj.IsComputed      := FieldByName(FLD_IsComputed).AsBoolean;
            obj.ComputedText    := FieldByName(FLD_Computed_Text).AsString;

          end;    

          Next;
       end;
    end;
    // ---------------------------------------------------------------


begin
  aDBStructure.Clear;
  ADOTable1.Connection :=aADOConnection;

  db_TableOpen1(ADOTable1, 'tables');
  Do_tables(ADOTable1);

  db_TableOpen1(ADOTable1, 'table_fields');
  Do_TableFields(ADOTable1);

  db_TableOpen1(ADOTable1, 'views');
  Do_Views(ADOTable1);

  db_TableOpen1(ADOTable1, 'triggers');
  Do_Trigger(ADOTable1);

  db_TableOpen1(ADOTable1, 'StoredProcs');
  Do_StoredProcs(ADOTable1);

  db_TableOpen1(ADOTable1, 'Func');
  Do_Func(ADOTable1);


  db_TableOpen1(ADOTable1, 'indexes');
  Do_indexes(ADOTable1);

  db_TableOpen1(ADOTable1, 'CK');
  Do_CK(ADOTable1);

  db_TableOpen1(ADOTable1, 'FK');
  Do_FK(ADOTable1);

end;

// TODO: Test
//// ---------------------------------------------------------------
//procedure TdmExportStructure.Test;
//// ---------------------------------------------------------------
//var
//I: Integer;
//j: Integer;
//oStrList: TStringList;
//
//oDBObject: TDBObject;
//begin
//oStrList:=TStringList.Create;
//
//for I := 0 to FDBStructure_ref.Tables.Count - 1 do
//begin
//  oDBObject:=FDBStructure_ref.Tables[i];
//
//  oStrList.Add(oDBObject.MakeCreateSQL(oStrList));
//
//  for j := 0 to oDBObject.PrimaryKeys.Count - 1 do
//    oStrList.Add(oDBObject.PrimaryKeys[j].MakeCreateSQL(oStrList));
//
//  for j := 0 to oDBObject.Indexes.Count - 1 do
//    oStrList.Add(oDBObject.Indexes[j].MakeCreateSQL(oStrList));
//
//  for j := 0 to oDBObject.CK.Count - 1 do
//    oStrList.Add(oDBObject.CK[j].MakeCreateSQL(oStrList));
//
//  for j := 0 to oDBObject.Triggers.Count - 1 do
//    oStrList.Add(oDBObject.Triggers[j].MakeCreateSQL(oStrList));
//
//end;
//
//for I := 0 to FDBStructure_ref.Views.Count - 1 do
//  oStrList.Add(FDBStructure_ref.Views[i].MakeCreateSQL(oStrList));
//
//for I := 0 to FDBStructure_ref.FK.Count - 1 do
//  oStrList.Add(FDBStructure_ref.FK[i].MakeCreateSQL(oStrList));
//
//for I := 0 to FDBStructure_ref.StoredProcs.Count - 1 do
//  oStrList.Add(FDBStructure_ref.StoredProcs[i].MakeCreateSQL(oStrList));
//
//
//
// // oStrList.SaveToFile('s:aaa.txt');
//
//
//oStrList.Free;
//
//end;


//--------------------------------------------------------------------
function TdmExportStructure_2000.ExportVersionUpdates(aDataset: TDataSet): boolean;
//--------------------------------------------------------------------
//var
//  oConnection: TAdoConnection;
//  oConnectRec: TLoginRec;
//  sLib, sAddr: string;

begin
//  oConnection:= TAdoConnection.Create(nil);
//  oConnection.LoginPrompt:= false;
//
//  dmMain.UseCustomConnection:= true;
//  dmMain.CustomConnection:= oConnection;
//
//  dmMain.LoadConnectRecFromReg(oConnectRec);
//  dmMain.LoadAdditionalConnectParamsFromReg(oConnectRec.Server, sLib, sAddr);
//
//  oConnectRec.DefaultDB:= 'onega_version_updates';
//
//  with oConnectRec do
//    Result:= dmMain.OpenByParams(Server, User, Password, DefaultDB, sLib, sAddr, IsUseWinAuth);
//
//  if not Result then exit;
//
//  try
//    aDataset.Close;
//    aDataset.Open;
//  except
//    on e: Exception do begin
//      Result:= false;
//      exit;
//    end;
//  end;
//
//(*
//  with aDataset do
//  begin
//    First;
//    while not EOF do
//      aDataset.Delete;
//  end;
//*)
//
//  qry_objects_to_update.Connection:= oConnection;
//
//  db_OpenTable(qry_objects_to_update, 'version_updates');
//  db_CopyRecords(qry_objects_to_update, aDataset);
//
////  db_View(qry_objects_to_update, aDataset);
//
//  oConnection.Free;


end;


procedure TdmExportStructure_2000.BeforeRun;
begin
  UpdateTable_Set_Is_Exists_False('Tables');
  UpdateTable_Set_Is_Exists_False('Views');
  UpdateTable_Set_Is_Exists_False('Indexes');
  UpdateTable_Set_Is_Exists_False('Func');
  UpdateTable_Set_Is_Exists_False('StoredProcs');
  UpdateTable_Set_Is_Exists_False('Triggers');
  UpdateTable_Set_Is_Exists_False('CK');
  UpdateTable_Set_Is_Exists_False('FK');

end;



procedure TdmExportStructure_2000.AfterRun;
begin
  UpdateTable_Delete_Is_not_Exists('Tables');
  UpdateTable_Delete_Is_not_Exists('Views');
  UpdateTable_Delete_Is_not_Exists('Indexes');
  UpdateTable_Delete_Is_not_Exists('Func');
  UpdateTable_Delete_Is_not_Exists('StoredProcs');
  UpdateTable_Delete_Is_not_Exists('Triggers');
  UpdateTable_Delete_Is_not_Exists('CK');
  UpdateTable_Delete_Is_not_Exists('FK');

end;



// ---------------------------------------------------------------
procedure TdmExportStructure_2000.Run(aPrefix: string);
// ---------------------------------------------------------------
var
  bTerminated: Boolean;
begin
 // Assert(Assigned(FADOConnection_ref), 'Value not assigned');


  FDBStructure_ref.Clear;

//  aPrefix :='sql_';
  if aPrefix = '' then
    BeforeRun;


  if aPrefix <> '' then
  begin
    EmptyTable('sql_Tables');
    EmptyTable('sql_Table_Fields');
    EmptyTable('sql_Views');
    EmptyTable('sql_Indexes');
    EmptyTable('sql_Triggers');
    EmptyTable('sql_Func');
    EmptyTable('sql_StoredProcs');
    EmptyTable('sql_FK');
    EmptyTable('sql_CK');
  end;
                    


  EmptyTable(aPrefix+'Table_Fields');


//  FADOConnection_ref.Execute(Format('DELETE FROM %s', ['Table_Fields']));


(*      FMDB.EmptyTable(['Table_Fields']);


    ExecCommand ('DELETE FROM ' + aTableName);//, [EMPTY_PARAM]);

*)


//    dmExportStructure.BeforeRun;

  ADOTable1.Connection := dmMDB.ADOConnection1;


  Progress_SetProgress1(1, 9, bTerminated);
  db_TableOpen1(ADOTable1, aPrefix+ 'Tables');
  ExportTable(ADOTable1);

  Progress_SetProgress1(2, 9, bTerminated);
  db_TableReOpen(ADOTable1, aPrefix+ 'Table_Fields');
  ExportTableFields(ADOTable1);

  Progress_SetProgress1(3, 9, bTerminated);
  db_TableReOpen(ADOTable1, aPrefix+'CK');
  ExportCKToDataSet(ADOTable1);

  Progress_SetProgress1(4, 9, bTerminated);
  db_TableReOpen(ADOTable1, aPrefix+'FK');
  ExportFKToDataSet(ADOTable1);

  Progress_SetProgress1(5, 9, bTerminated);
  db_TableReOpen(ADOTable1, aPrefix+'Indexes');
  ExportIndexesToDataSet(ADOTable1) ;
                                     
  Progress_SetProgress1(6, 9, bTerminated);
  db_TableReOpen(ADOTable1, aPrefix+'Views');
  ExportViewsToDataSet(ADOTable1) ;

  Progress_SetProgress1(7, 9, bTerminated);
  db_TableReOpen(ADOTable1, aPrefix+'StoredProcs');
  ExportStoredProcsToDataSet(ADOTable1, 'procedure');


  Progress_SetProgress1(8, 9, bTerminated);
  db_TableReOpen(ADOTable1, aPrefix+'Func');
  ExportStoredProcsToDataSet(ADOTable1, 'function');

  Progress_SetProgress1(9, 9, bTerminated);
  db_TableReOpen(ADOTable1, aPrefix+'Triggers');
  ExportTriggersToDataSet(ADOTable1);


  if aPrefix = '' then
    AfterRun;


end;


procedure TdmExportStructure_2000.Progress2(aValue,aMax : Integer);
var bTerminated: boolean ;
begin
  Progress_SetProgress2(aValue,aMax, bTerminated);

end;


procedure TdmExportStructure_2000.Log(aMsg: string);
begin
  g_Log.Msg(aMsg);
end;


procedure TdmExportStructure_2000.ExecDlg_start_proc;
begin
  Run(FPrefix);

end;


procedure TdmExportStructure_2000.ExecDlg(aPrefix: string; aDBStructure:
    TDBStructure);
begin
 //FADOConnection_ref  := aADOConnection;
  FPrefix:=aPrefix;

  FDBStructure_ref :=aDBStructure;

//  ExecDlg_start_proc;


  Progress_ExecDlg_proc(ExecDlg_start_proc);

end;


function TdmExportStructure_2000.OpenIndexesForTable(aTableName: string): Boolean;
var
  s: string;
begin
  try
    Result :=db_OpenQuery(qry_Indexes, Format('exec sp_helpindex @objname=''%s''', [aTableName]));
//    Result := True;
  except
   on E: Exception do
   begin
    // qry_Indexes.Close;
     s:=e.Message +':'+ aTableName;
     g_Log.Error(s);

    Result := False;

 //    ShowMessage(s);
  end;
  end;
end;


begin
  //ShowMessage(FormatDateTime('yyyymmddhhmmss', Now()));

end.

//
//function Progress_ExecDlg(aCaption: string; aOnStartEvent: TNotifyEvent): Boolean;
//function Progress_ExecDlg_proc(aOnStartEvent: TProcedureEvent; aCaption: string = ''):
//    Boolean;

(*
procedure Progress_SetProgress1(aProgress,aTotal: integer; var aTerminated: boolean);
procedure Progress_SetProgress2(aProgress,aTotal: integer; var aTerminated: boolean);

procedure Progress_SetProgressMsg1(aMsg: string);
procedure Progress_SetProgressMsg2(aMsg: string);

*)


//
//
//SELECT     TOP 100 PERCENT OBJECT_NAME(dbo.syscolumns.id) AS OBJECT_NAME, dbo.syscolumns.name AS name, Comments.text AS text
//FROM         dbo.syscolumns INNER JOIN
//                      dbo.syscomments Comments ON dbo.syscolumns.id = Comments.id AND dbo.syscolumns.colid = Comments.number
//WHERE     (dbo.syscolumns.iscomputed = 1) AND (dbo.syscolumns.xtype = 62)


(*
var
  oSList: TStringList;
begin
  oSList:=TStringList.Create;
  oSList.Lo
  FreeAndNil(oSList);
*)

