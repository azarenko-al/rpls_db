object dmExportStructure_2000: TdmExportStructure_2000
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 811
  Top = 198
  Height = 542
  Width = 416
  object qry_Temp: TADOQuery
    Parameters = <>
    Left = 32
    Top = 16
  end
  object qry_Indexes: TADOQuery
    Parameters = <>
    Left = 32
    Top = 88
  end
  object qry_Tables: TADOQuery
    Parameters = <>
    Left = 32
    Top = 160
  end
  object qry_PKeys: TADOQuery
    Parameters = <>
    Left = 136
    Top = 80
  end
  object qry_TableFields: TADOQuery
    Parameters = <>
    Left = 28
    Top = 224
  end
  object q_DF: TADOQuery
    Parameters = <>
    SQL.Strings = (
      ' SELECT TOP 100 PERCENT                            '
      '   c.name   AS COLUMN_NAME,                        '
      '        OBJECT_NAME(o.parent_obj) AS tablename ,   '
      '   o.name   AS DEFAULT_NAME,                       '
      '   com.text AS DEFAULT_CONTENT                     '
      ' FROM sysobjects o INNER JOIN                      '
      '      syscolumns c ON o.id = c.cdefault INNER JOIN '
      '      syscomments com ON o.id = com.id             '
      ' WHERE o.xtype = '#39'D'#39
      ' ORDER BY tablename                                ')
    Left = 328
    Top = 224
  end
  object q_PK: TADOQuery
    Parameters = <>
    Left = 204
    Top = 16
  end
  object ADOTable1: TADOTable
    Left = 312
    Top = 24
  end
  object q_HelpText: TADOQuery
    Parameters = <>
    Left = 216
    Top = 240
  end
  object qry_SP: TADOQuery
    Parameters = <>
    SQL.Strings = (
      'select'
      '        *'
      '    from'
      '        INFORMATION_SCHEMA.ROUTINES '
      '    where'
      '        ROUTINE_TYPE = '#39'PROCEDURE'#39' and'
      
        '        ObjectProperty (Object_Id (ROUTINE_NAME), '#39'IsMSShipped'#39')' +
        ' = 0 and'
      '        ('
      '            select '
      '                major_id '
      '            from '
      '                sys.extended_properties '
      '            where '
      '                major_id = object_id(ROUTINE_NAME) and '
      '                minor_id = 0 and '
      '                class = 1 and '
      '                name = N'#39'microsoft_database_tools_support'#39
      '        ) is null'
      '    '
      'and '
      ' (routine_name NOT LIKE N'#39'dt_%'#39')'
      ' AND   '
      ' (routine_name NOT LIKE '#39'[_]%'#39') ')
    Left = 112
    Top = 304
  end
  object q_Triggers: TADOQuery
    Parameters = <>
    SQL.Strings = (
      ' SELECT  cast(o.name as varchar(100)) as name, c.text, colid,  '
      '        OBJECT_NAME(o.parent_obj) AS tablename                 '
      '  FROM   sysobjects o INNER JOIN                               '
      '        syscomments c ON o.id = c.id                           '
      '  WHERE  (o.xtype = '#39#39'TR'#39#39') AND                                '
      '         (o.name not like '#39'[_][_]%'#39')'
      ' ORDER BY name, colid                                          ')
    Left = 200
    Top = 304
  end
  object qry_Views: TADOQuery
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM INFORMATION_SCHEMA.views        '
      ' WHERE  (table_name NOT LIKE '#39'[_]%'#39')     AND'
      '        (table_name <> '#39#39'sysconstraints'#39#39') AND'
      '        (table_name <> '#39#39'syssegments'#39#39#9')      '
      ' ORDER BY table_name                          ')
    Left = 296
    Top = 304
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM INFORMATION_SCHEMA.views        '
      ' WHERE  (table_name NOT LIKE '#39'[_]%'#39')     AND'
      '        (table_name <> '#39#39'sysconstraints'#39#39') AND'
      '        (table_name <> '#39#39'syssegments'#39#39#9')      '
      ' ORDER BY table_name                          ')
    Left = 336
    Top = 136
  end
  object q_Computed_columns: TADOQuery
    Parameters = <>
    SQL.Strings = (
      
        'SELECT     TOP 100 PERCENT OBJECT_NAME(dbo.syscolumns.id) AS OBJ' +
        'ECT_NAME, dbo.syscolumns.name AS name, Comments.text AS text'
      'FROM         dbo.syscolumns INNER JOIN'
      
        '                      dbo.syscomments Comments ON dbo.syscolumns' +
        '.id = Comments.id AND dbo.syscolumns.colid = Comments.number'
      
        'WHERE     (dbo.syscolumns.iscomputed = 1) AND (dbo.syscolumns.xt' +
        'ype = 62)')
    Left = 136
    Top = 168
  end
  object sql_SysObjects: TADOQuery
    Parameters = <>
    Left = 200
    Top = 432
  end
end
