unit dm_Structure_export1111111111111111;

interface

uses
  SysUtils, Classes, Forms, Db, AdoDb, Variants,

//  dm_Main_SQL,     

  u_local_const,
  
  u_db,
  u_const_db,

  u_sync_classes;


type
  TdmStructure_Load_from_MDB111111111111 = class(TDataModule)
    ADOQuery1: TADOQuery;
    MDBConnection: TADOConnection;
    ADOTable1: TADOTable;
    procedure DataModuleCreate(Sender: TObject);
  private
    FDBStructure: TDBStructure;
                              

    procedure Do_CK(aDataset: TDataSet);
    procedure Do_Database_Info(aDataset: TDataSet);
    procedure Do_FK(aDataset: TDataSet);
    procedure Do_Func(aDataset: TDataSet);
    procedure Do_indexes(aDataset: TDataSet);
    procedure Do_StoredProcs(aDataset: TDataSet);
    procedure Do_TableFields(aDataset: TDataSet);
    procedure Do_tables(aDataset: TDataSet);
    procedure Do_Trigger(aDataset: TDataSet);
    procedure Do_Views(aDataset: TDataSet);
    procedure Exec(aADOConnection: TADOConnection; aDBStructure: TDBStructure;
        aProject_Name: string);

  public
    Params: record

      ADOConnection: TADOConnection;
      DBStructure: TDBStructure;
      Project_Name: string;
    end;

    class procedure Init;
    { Public declarations }
  end;

var
  dmStructure_Load_from_MDB111111111111: TdmStructure_Load_from_MDB111111111111;

implementation

{$R *.dfm}



const
  TBL_sys_objects = 'sys_objects';
  TBL_sys_objects_table_fields = 'sys_objects_table_fields';

  TBL__db_version_ = '_db_version_';

  FLD_PROJECT_NAME = 'PROJECT_NAME';

  FLD_Version = 'version';
  FLD_product_name = 'product_name';

  FLD_IS_EXISTS = 'IS_EXISTS';

  FLD_XTYPE = 'XTYPE';

  FLD_max_modify_date = 'max_modify_date';


  FLD_PK_schema = 'PK_schema';
  FLD_FK_schema = 'FK_schema';

  FLD_PK_TABLE_NAME  ='PK_TABLE_NAME';
  FLD_PK_COLUMN_NAME ='PK_COLUMN_NAME';

  FLD_FK_TABLE_NAME  ='FK_TABLE_NAME';
  FLD_FK_COLUMN_NAME ='FK_COLUMN_NAME';

  FLD_IsDeleteCascade='IsDeleteCascade';
  FLD_IsUpdateCascade='IsUpdateCascade';

  FLD_index_description = 'index_description';


  FLD_CONSTRAINT_NAME = 'CONSTRAINT_NAME';

  FLD_routine_name = 'routine_name';
  FLD_routine_type ='routine_type';

  FLD_SCHEMA = 'SCHEMA';
  FLD_TABLE_SCHEMA = 'TABLE_SCHEMA';
  FLD_ROUTINE_SCHEMA = 'ROUTINE_SCHEMA';


  FLD_IsComputed    = 'IsComputed';
  FLD_Computed_Text = 'Computed_Text';






class procedure TdmStructure_Load_from_MDB111111111111.Init;
begin
//  if not Assigned(dmStructure_Load_from_MDB1) then
 //   dmStructure_Load_from_MDB1 := TdmStructure_Load_from_MDB1.Create(Application);
end;



procedure TdmStructure_Load_from_MDB111111111111.DataModuleCreate(Sender: TObject);
begin
  FDBStructure:=TDBStructure.Create();
end;



// ---------------------------------------------------------------
procedure TdmStructure_Load_from_MDB111111111111.Do_CK(aDataset: TDataSet);
// ---------------------------------------------------------------
var obj: TDBObject;
    i: Integer;
    sTableName: string;
    oTable: TDBObject;

begin
//      i:=aDataset.RecordCount;

  aDataset.First;

  with aDataset do
     while not EOF do
   begin
   //   Progress2(RecNo,RecordCount);


      sTableName :=FieldValues[FLD_TABLENAME];

      oTable := FDBStructure.Tables.FindByName(sTableName);
      Assert(Assigned(oTable), 'Value not assigned');

      obj:=oTable.CK.AddItem(otCheckConstraint);
      obj.TableName := sTableName;
      obj.Name := FieldValues[FLD_Name];
      obj.Text := FieldValues[FLD_CONTENT];

    ////////////  obj.Owner := FieldByName(FLD_Owner).AsString;

      Next;
   end;
end;

// ---------------------------------------------------------------
procedure TdmStructure_Load_from_MDB111111111111.Do_FK(aDataset: TDataSet);
// ---------------------------------------------------------------
var obj: TDBObject;
  i: Integer;
  //sOwner: string;
begin
  aDataset.First;

  with aDataset do
     while not EOF do
   begin
  //    Progress2(RecNo,RecordCount);


      obj:=FDBStructure.FK.AddItem(otFK);
      obj.Name      := FieldValues[FLD_Name];

      obj.FK.PK_schema   := FieldValues[FLD_PK_schema];
      obj.FK.FK_schema   := FieldValues[FLD_FK_schema];


      obj.FK.PK_TABLE_NAME   := FieldValues[FLD_PK_TABLE_NAME];
      obj.FK.PK_COLUMN_NAME  := FieldValues[FLD_PK_COLUMN_NAME];

      obj.FK.FK_TABLE_NAME   := FieldValues[FLD_FK_TABLE_NAME];
      obj.FK.FK_COLUMN_NAME  := FieldValues[FLD_FK_COLUMN_NAME];

      obj.FK.IsDeleteCascade  := FieldValues[FLD_IsDeleteCascade];
      obj.FK.IsUpdateCascade  := FieldValues[FLD_IsUpdateCascade];


 //       if oItem_SQL.TableName<>'' then
      obj.Owner:=FDBStructure.Tables.GetOwnerByName(obj.FK.FK_TABLE_NAME);

//    else
//     sOwner:='';

      //obj.Owner := sOwner;

//          obj.Owner := FieldByName(FLD_Owner).AsString;

      Next;
   end;
end;


// ---------------------------------------------------------------
procedure TdmStructure_Load_from_MDB111111111111.Do_Views(aDataset: TDataSet);
// ---------------------------------------------------------------
var obj: TDBObject;
begin
  aDataset.First;

   with aDataset do
     while not EOF do
   begin
//     Progress2(RecNo,RecordCount);


     obj := FDBStructure.Views.AddItem(otView);
     obj.Name:=FieldValues[FLD_Name];
     obj.Text:=FieldValues[FLD_CONTENT];

     obj.Owner := FieldByName(FLD_Owner).AsString;

   //  Assert (obj.Owner <> '');

     obj.Priority := FieldByName(FLD_Priority).AsInteger;

     Next;
   end;
end;

// ---------------------------------------------------------------
procedure TdmStructure_Load_from_MDB111111111111.Do_StoredProcs(aDataset: TDataSet);
// ---------------------------------------------------------------
var obj: TDBObject;
  sRoutine_Type: string;
begin
  aDataset.First;

  with aDataset do
     while not EOF do
   begin
  //    Progress2(RecNo,RecordCount);

      sRoutine_Type:=FieldValues[FLD_Routine_Type];

   //   if Eq(sRoutine_Type, 'function') then
     //   obj:=aDBStructure.StoredFuncs.AddItem(otStoredFunc)
    //  else
      obj:=FDBStructure.StoredProcs.AddItem(otStoredPROC);

      obj.Name:=FieldValues[FLD_Name];
      obj.Text:=FieldValues[FLD_CONTENT];

      obj.Owner := FieldByName(FLD_Owner).AsString;

      obj.Priority := FieldByName(FLD_Priority).AsInteger;

     Next;
   end;
end;


// ---------------------------------------------------------------
procedure TdmStructure_Load_from_MDB111111111111.Do_Func(aDataset: TDataSet);
// ---------------------------------------------------------------
var obj: TDBObject;
  sRoutine_Type: string;
begin
  aDataset.First;

  with aDataset do
     while not EOF do
   begin
  //    Progress2(RecNo,RecordCount);

      sRoutine_Type:=FieldValues[FLD_Routine_Type];

//          if Eq(sRoutine_Type, 'function') then
      obj:=FDBStructure.StoredFuncs.AddItem(otStoredFunc);

//        else
//        obj:=aDBStructure.StoredProcs.AddItem(otStoredPROC);

      obj.Name:=FieldValues[FLD_Name];
      obj.Text:=FieldValues[FLD_CONTENT];

      obj.Owner := FieldByName(FLD_Owner).AsString;

      obj.Priority := FieldByName(FLD_Priority).AsInteger;

     Next;
   end;
end;


// ---------------------------------------------------------------
procedure TdmStructure_Load_from_MDB111111111111.Do_Trigger(aDataset: TDataSet);
// ---------------------------------------------------------------
var
  oTable, obj: TDBObject;
  bPrimaryKey: Boolean;
  sName: string;
  sTableName: string;
begin
  aDataset.First;

  with aDataset do
     while not EOF do
   begin
   //  Progress2(RecNo,RecordCount);


     sName :=FieldValues[FLD_NAME];
     sTableName :=FieldValues[FLD_TABLENAME];


     oTable := FDBStructure.Tables.FindByName(sTableName);
     Assert(Assigned(oTable), 'Value not assigned');

     obj := FDBStructure.Triggers.FindByName(sName);
     Assert(not Assigned(obj), 'Value not assigned');


//     obj:=oTable.Triggers.AddItem(otTRIGGER);


     obj.Name      := FieldValues[FLD_NAME];
     obj.TableName := FieldValues[FLD_TableName];
     obj.Text      := FieldValues[FLD_CONTENT];

 //    obj.Owner     := FieldByName(FLD_Owner).AsString;

     obj.Owner:=FDBStructure.Tables.GetOwnerByName(obj.TableName);


     Next;
   end;
end;

// ---------------------------------------------------------------
procedure TdmStructure_Load_from_MDB111111111111.Do_tables(aDataset: TDataSet);
// ---------------------------------------------------------------
var obj: TDBObject;
  i: Integer;
begin
  i:=aDataset.RecordCount;
  aDataset.First;

  with aDataset do
     while not EOF do
   begin
   //   Progress2(RecNo,RecordCount);


     obj := FDBStructure.Tables.AddItem(otTable);
     obj.Name   :=FieldValues[FLD_Name];

     obj.Owner := FieldByName(FLD_Owner).AsString;

     Next;
   end;
end;


// ---------------------------------------------------------------
procedure TdmStructure_Load_from_MDB111111111111.Do_indexes(aDataset: TDataSet);
// ---------------------------------------------------------------
var
  bIs_PRIMARY_KEY: Boolean;
  bIS_UNIQUE_INDEX: Boolean;
  bIS_UNIQUE_KEY: Boolean;
  obj: TDBObject;
  bPrimaryKey: Boolean;
  sTableName: string;
  oTable: TDBObject;

begin
  aDataset.First;

  with aDataset do
     while not EOF do
   begin
  //   Progress2(RecNo,RecordCount);

     sTableName :=FieldValues[FLD_TABLENAME];

     oTable := FDBStructure.Tables.FindByName(sTableName);
     Assert(Assigned(oTable), 'Value not assigned');

     bPrimaryKey:= FieldByName(FLD_IS_PRIMARY_KEY).AsBoolean;

(*
     if bPrimaryKey then
       obj:=oTable.PrimaryKeys.AddItem(otPrimaryKey)
     else
*)

     obj:=oTable.Indexes.AddItem(otIndex);


     obj.Name      := FieldValues[FLD_NAME];
     obj.TableName :=FieldValues[FLD_TABLENAME];

//         obj.Index.IndexType := itINDEX;


     bIs_PRIMARY_KEY  := FieldByName(FLD_IS_PRIMARY_KEY).AsBoolean;
     bIS_UNIQUE_KEY   := FieldByName(FLD_is_unique_constraint).AsBoolean;
     bIS_UNIQUE_INDEX := FieldByName(FLD_IS_UNIQUE).AsBoolean;

    if bIs_PRIMARY_KEY   then obj.Index.IndexType := itPRIMARY_KEY else
    if bIS_UNIQUE_KEY    then obj.Index.IndexType := itUNIQUE_KEY  else
    if bIS_UNIQUE_INDEX  then obj.Index.IndexType := itUNIQUE_INDEX
                         else obj.Index.IndexType := itINDEX;


//     obj.SetIndexKeys(FieldByName(FLD_INDEX_KEYS).AsString);

     Next;
   end;
end;


// ---------------------------------------------------------------
procedure TdmStructure_Load_from_MDB111111111111.Do_TableFields(aDataset: TDataSet);
// ---------------------------------------------------------------
var obj: TDBTableField;
   sTableName: string;
   oTable: TDBObject;
begin
  aDataset.First;

  with aDataset do
     while not EOF do
   begin
  //    Progress2(RecNo,RecordCount);


      sTableName :=FieldValues[FLD_TABLENAME];

      oTable := FDBStructure.Tables.FindByName(sTableName);

      if Assigned(oTable) then
      begin
        obj:=oTable.TableFields.AddItem;

        obj.Name := FieldByName(FLD_NAME).AsString;

        obj.TableName       := FieldByName(FLD_TABLENAME).AsString;
        obj.TYPE_           := FieldByName(FLD_TYPE).AsString;
        obj.Length          := FieldByName(FLD_Length).AsInteger;
//        obj.IsVariableLen   := FieldByName(FLD_VARIABLE).AsBoolean;
        obj.IsNullable      := FieldByName(FLD_IsNullable).AsBoolean;
        obj.IsIdentity      := FieldByName(FLD_IDENTITY).AsBoolean;

//        obj.DEFAULT_NAME    := FieldByName(FLD_DEFAULT_NAME).AsString;
        obj.DEFAULT_CONTENT := FieldByName(FLD_DEFAULT_CONTENT).AsString;

        obj.IsComputed      := FieldByName(FLD_IsComputed).AsBoolean;
        obj.ComputedText    := FieldByName(FLD_Computed_Text).AsString;

      end;

      Next;
   end;
end;

// ---------------------------------------------------------------
procedure TdmStructure_Load_from_MDB111111111111.Do_Database_Info(aDataset: TDataSet);
// ---------------------------------------------------------------
begin
  with aDataset do
     if not EOF then
   begin
     FDBStructure.DB_Version      := FieldByName(FLD_Version).AsString;
     FDBStructure.DB_Product_name := FieldByName(FLD_Product_name).AsString;
   end;
end;


// ---------------------------------------------------------------
procedure TdmStructure_Load_from_MDB111111111111.Exec(aADOConnection: TADOConnection;
    aDBStructure: TDBStructure; aProject_Name: string);
// ---------------------------------------------------------------
var
  I: Integer;
  oList: TStringList;

begin
 ////////// FDBStructure:=aDBStructure;
  Assert(aProject_Name<>'');


{
  aDBStructure.Clear;


  ADOTable1.Connection :=aADOConnection;
  ADOQuery1.Connection :=aADOConnection;
 }


  db_OpenQuery(ADOQuery1, Format('select * from tables WHERE Project_Name=''%s'' order by name  ', [aProject_Name]));

  Assert(ADOQuery1.RecordCount > 0);

//  db_TableOpen1(ADOTable1, 'tables');
  Do_tables(ADOQuery1);

  db_OpenQuery(ADOQuery1, Format('select * from table_fields  WHERE Project_Name=''%s''   ', [aProject_Name] ) );
  Do_TableFields(ADOQuery1);


  db_OpenQuery(ADOQuery1, Format('select * from views  WHERE Project_Name=''%s'' order by priority desc, name  ', [aProject_Name]));
 // db_TableOpen1(ADOTable1, 'views');
  Do_Views(ADOQuery1);

  db_OpenQuery(ADOQuery1, Format('select * from triggers WHERE Project_Name=''%s''  order by name   ', [aProject_Name]));
 // db_TableOpen1(ADOTable1, 'triggers');
  Do_Trigger(ADOQuery1);

  db_OpenQuery(ADOQuery1, Format('select * from StoredProcs  WHERE Project_Name=''%s'' order by priority desc, name ', [aProject_Name]));
 // db_TableOpen1(ADOTable1, 'StoredProcs');
  Do_StoredProcs(ADOQuery1);

  db_OpenQuery(ADOQuery1, Format('select * from Func  WHERE Project_Name=''%s'' order by priority  desc, name ', [aProject_Name]));
//  db_TableOpen1(ADOTable1, 'Func');
  Do_Func(ADOQuery1);

  db_OpenQuery(ADOQuery1, Format('select * from indexes  WHERE Project_Name=''%s'' order by name ', [aProject_Name]));
 // db_TableOpen1(ADOTable1, 'indexes');
  Do_indexes(ADOQuery1);

//  db_OpenQuery(ADOQuery1, 'select * from views order by name');
  db_OpenQuery(ADOQuery1, Format('select * from CK  WHERE Project_Name=''%s''  order by name  ', [aProject_Name]));
  Do_CK(ADOQuery1);

  db_OpenQuery(ADOQuery1, Format('select * from FK  WHERE Project_Name=''%s''  order by name ', [aProject_Name]));
//  db_TableOpen1(ADOTable1, 'FK');
  Do_FK(ADOQuery1);

  db_TableOpen1(ADOTable1, TBL_database_info1);
  Do_Database_Info(ADOTable1);


  oList:=TStringList.Create();

  FDBStructure.GetSQLList(oList);

//  oList.SaveToFile('d:\111.txt');


  FreeAndNil(oList);


end;



end.
