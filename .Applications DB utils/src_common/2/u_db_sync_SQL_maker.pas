unit u_db_sync_SQL_maker;

interface

uses SysUtils,

  u_func,

  u_sync_classes;

type
  TSQL_maker = class(TObject)
  private
  public
    function CreateTableSQL(aDBObject: TDBObject): boolean;
    function GetDropSQL(aDBObject: TDBObject): string;
  end;

implementation

//---------------------------------------------------------
function TSQL_maker.CreateTableSQL(aDBObject: TDBObject): boolean;
//---------------------------------------------------------
var
  I: Integer;
  iFillFactor: Integer;
  sDefault, sPK_Definition: string;
  oFieldArray: TStrArray;

  FSql : string;

  bClustered, bVariable, bIdentity: boolean;
  s: string;

  oField: TDBTableField;
  oPRIMARY_KEY: TDBObject;
begin
  Result:= false;

 // FModuleName:= 'TSQL_maker.CreateTableSQL';


  oPRIMARY_KEY :=aDBObject.Indexes.FindIndexByPRIMARY_KEY();

  //------------------------------
  //  Primary Key
//  with dmMDB do
//  begin
//    FType:= TYPE_TABLE;
 //   FDataSet:= tbl_table_fields;

    bClustered:= false;
    iFillFactor:= 0;
    sPK_Definition:= '';

    if Assigned(oPRIMARY_KEY) then
    begin

      bClustered := oPRIMARY_KEY.Index.IS_CLUSTERED;
      iFillFactor:= oPRIMARY_KEY.Index.FILL_FACTOR;

      sPK_Definition:=
        Format('ALTER TABLE [%s] ADD                               '+
               'CONSTRAINT [%s] PRIMARY KEY %s (%s) %s ON [PRIMARY]',
                [oPRIMARY_KEY.TableName,
                 oPRIMARY_KEY.Name,
                 IIF(bClustered,    'CLUSTERED', 'NONCLUSTERED'),
                 oPRIMARY_KEY.Index.INDEX_KEYS,
                 IIF(iFillFactor>0, ' WITH FILLFACTOR = '+AsString(iFillFactor), '')
                ]);


    end;
(*

    if tbl_indexes.Locate(FLD_TABLENAME+';'+FLD_IS_PRIMARY_KEY,
                  VarArrayOf([FObjName, true]), [loCaseInsensitive]) then
    begin
      bClustered := tbl_indexes.FieldByName(FLD_IS_CLUSTERED).AsBoolean;
      iFillFactor:= tbl_indexes.FieldByName(FLD_FILL_FACTOR).AsInteger;

      sPK_Definition:=
        Format('ALTER TABLE [%s] ADD                               '+
               'CONSTRAINT [%s] PRIMARY KEY %s (%s) %s ON [PRIMARY]',
                [FObjName,
                 tbl_indexes.FieldByName(FLD_NAME).AsString,
                 IIF(bClustered,    'CLUSTERED', 'NONCLUSTERED'),
                 tbl_indexes.FieldByName(FLD_INDEX_KEYS).AsString,
                 IIF(iFillFactor>0, ' WITH FILLFACTOR = '+AsString(iFillFactor), '')
                ]);
    end;
  end;  *)
  //------------------------------

  SetLength(oFieldArray, 0);

  for I := 0 to aDBObject.TableFields.Count - 1 do
  begin
      oField := aDBObject.TableFields[i];

      bVariable:= oField.VARIABLE  AND (oField.USERTYPE < 250);
      bIdentity:=  oField.IsIdentity;

      if oField.DEFAULT_CONTENT='' then
        sDefault:= ''
      else
        sDefault:= Format('DEFAULT %s', [oField.DEFAULT_CONTENT]);


      FSql:= Format('[%s] %s %s %s %s %s %s ',
               [oField.Name,
                oField.TYPE_,
                IIF (bVariable, Format('(%s)', [oField.SIZE]), ' '),
                sDefault,
                IIF (bIdentity,
                     Format('IDENTITY (%d, %s)',
                              [oField.IDENT_SEED, oField.IDENT_INCR]),
                     ' '),
                IIF (oField.IsRowGuidCol, 'ROWGUIDCOL', ''),
                IIF (oField.NULLS, 'NULL', 'NOT NULL')
               ]);
      StrArrayAddValue(oFieldArray, FSql);

   //   Next;
 //   end;
  end;

(*
  s := StrArrayToString(oFieldArray, ',');
  FSql := Format('CREATE TABLE [dbo].[%s] ( %s ) ON [PRIMARY]',
          [aDBObject.Name, s]);

*)
//  Result:= gl_DB.ExecCommandSimple(FSql);
//  AddSript(FSql);

(*
  if not Result then
  begin
    g_Log.AddError(FModuleName, gl_DB.ErrorMsg + FSql);
    exit;
  end;

  if Result then
    if sPK_Definition<>'' then
      if not gl_DB.ExecCommandSimple (sPK_Definition) then
        g_Log.AddError(FModuleName, gl_DB.ErrorMsg + sPK_Definition);
        
*)

end;

// ---------------------------------------------------------------
function TSQL_maker.GetDropSQL(aDBObject: TDBObject): string;
// ---------------------------------------------------------------
const
  SQL_ALTER_TABLE_DROP_CONSTRAINT   = 'ALTER TABLE [%s] DROP CONSTRAINT [%s]';

  SQL_DROP_TABLE        = 'DROP TABLE [%s]';
  SQL_DROP_INDEX        = 'DROP INDEX [%s].[%s]';
  SQL_DROP_TRIGGER      = 'DROP TRIGGER [%s]';
  SQL_DROP_VIEW         = 'DROP VIEW [%s]';

var
  sTblName, sObjName: string;
begin
  with aDBObject do
    case ObjectType of

      otTABLE:    Result:= Format(SQL_DROP_TABLE, [Name]);
      otVIEW:     Result:= Format(SQL_DROP_VIEW, [Name]);
      otTRIGGER:  Result:= Format(SQL_DROP_TRIGGER, [Name]) ;
      otPROC:
                begin
                  if Pos(UpperCase('procedure'), UpperCase(Text))>0 then
                    Result:= Format('DROP PROCEDURE %s', [Name]);

                  if Pos(UpperCase('function'), UpperCase(Text))>0 then
                    Result:= Format('DROP FUNCTION %s', [Name]);
                end;

      otIndex:
        begin
        //  sTblName:= aDataSet[FLD_TABLENAME];

          if Index.IS_PRIMARY_KEY or
             Index.IS_UNIQUE_KEY
          then
            Result:=Format(SQL_ALTER_TABLE_DROP_CONSTRAINT, [TableName, Name])
          else
            Result:=Format(SQL_DROP_INDEX, [TableName, Name]);
        end;


      otFK, otCK:
       begin
        //  sTblName:= aDataSet[FLD_TABLENAME];
          Result:=Format(SQL_ALTER_TABLE_DROP_CONSTRAINT, [TableName, Name]);
        end ;

    end;

  (*
    if aObjNameToDrop<>'' then
      sObjName:= aObjNameToDrop
    else
      sObjName:= aDataSet[FLD_NAME];
  *)


end;

end.
