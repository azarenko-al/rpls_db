program rpls_db_link;

{%File '..\src\ver.inc'}

uses
  Dialogs,
  Windows,
  dm_act_Antenna in '..\Src\Antenna\dm_Act_Antenna.pas' {dmAct_Antenna: TDataModule},
  dm_act_AntType in '..\Src\AntennaType\dm_Act_AntType.pas' {dmAct_AntType: TDataModule},
  dm_act_Base in '..\Src\Common\dm_act_Base.pas' {dmAct_Base: TDataModule},
  dm_act_BSC in '..\Src\BSC\dm_Act_BSC.pas' {dmAct_BSC: TDataModule},
//  dm_act_CalcRegion_PMP in '..\src\CalcRegion\dm_act_CalcRegion_PMP.pas' {dmAct_CalcRegion_PMP: TDataModule},
//  dm_Act_Cell in '..\src\Cell\dm_Act_CEll.pas' {dmAct_Cell: TDataModule},
  dm_Act_Clutter in '..\Src\Clutters\dm_Act_Clutter.pas' {dmAct_Clutter: TDataModule},
  dm_act_Folder in '..\Src\Folder\dm_act_Folder.pas' {dmAct_Folder: TDataModule},
  dm_act_GeoRegion in '..\src\GeoRegion\dm_act_GeoRegion.pas' {dmAct_GeoRegion: TDataModule},
  dm_Act_Link in '..\Src\Link\dm_act_Link.pas' {dmAct_Link: TDataModule},
  dm_Act_LinkEnd in '..\src\LinkEnd\dm_Act_LinkEnd.pas' {dmAct_LinkEnd: TDataModule},
  dm_act_LinkLine in '..\src\LinkLine\dm_act_LinkLine.pas' {dmAct_LinkLine: TDataModule},
  dm_act_MSC in '..\Src\MSC\dm_Act_MSC.pas' {dmAct_MSC: TDataModule},
  dm_act_PMP_site in '..\src\PMP_site\dm_Act_Pmp_Site.pas' {dmAct_PMP_site: TDataModule},
  dm_Act_PmpTerminal in '..\src\PmpTerminal\dm_Act_PmpTerminal.pas' {dmAct_PmpTerminal: TDataModule},
  dm_Act_Project in '..\src\Project\dm_Act_Project.pas' {dmAct_Project: TDataModule},
  dm_Act_Project1 in '..\src\Project\dm_Act_Project1.pas' {dmAct_Project1: TDataModule},
  dm_act_Property in '..\Src\Property\dm_act_Property.pas' {dmAct_Property: TDataModule},
  dm_act_Rel_Engine in '..\Src\RelEngine\dm_act_Rel_Engine.pas' {dmAct_Rel_Engine: TDataModule},
  dm_Act_RelFile in '..\src\RelMatrixFile\dm_Act_RelFile.pas' {dmAct_RelFile: TDataModule},
  dm_Act_Report in '..\src\Report\dm_Act_Report.pas' {dmAct_Report: TDataModule},
//  dm_act_TransferTech in '..\src\TransferTech\dm_act_TransferTech.pas' {dmAct_TransferTech: TDataModule},
  dm_act_Trx in '..\src\Trx\dm_act_Trx.pas' {dmAct_Trx: TDataModule},
  dm_BSC_View in '..\Src\BSC\dm_BSC_View.pas' {dmBSC_View: TDataModule},
  dm_Main in '..\src\DataModules\dm_Main.pas' {dmMain: TDataModule},
  dm_Property in '..\src\Property\dm_Property.pas' {dmProperty: TDataModule},
  dm_Rel_Engine in '..\Src\RelEngine\dm_Rel_Engine.pas' {dmRel_Engine: TDataModule},
  f_Log in '..\src\Forms\f_Log.pas' {frm_Log},
  f_Main_MDI in '..\Src\Forms\f_Main_MDI.pas' {frm_Main_MDI},
  f_Map in '..\Src\Map\f_Map.pas' {frm_Map},
  Forms,
 // i_Core in '..\src\_Interfaces\i_Core.pas',
//  i_MapX_tools in '..\..\common DLL\MAP\i_MapX_tools.pas',
  SysUtils,
  u_config_classes in '..\src\_Config\u_config_classes.pas',
  u_Map_Classes in '..\src\Map\u_Map_Classes.pas',
  u_profile_classes in '..\src\Profile\u_profile_classes.pas',
  u_reflection_points in '..\src\Profile\u_reflection_points.pas',
//  u_rel_matrix_list in '..\..\common\DLL_REL\src\u_rel_matrix_list.pas',
  u_types in '..\Src\u_types.pas',
  u_ViewEngine_classes in '..\src\Explorer\u_ViewEngine_classes.pas',
//  u_xml_Sites in '..\src\Property\u_xml_Sites.pas',
  d_Antenna_inspector in '..\src\Antenna\d_Antenna_inspector.pas' {dlg_Antenna_inspector},
//  u_Onega_DB_data in '..\src\u_Onega_DB_data.pas',
  u_vars in '..\src\u_vars.pas',
  u_LinkLine_Add_classes in '..\src\LinkLine\u_LinkLine_Add_classes.pas',
  u_AntType_classes in '..\src\AntennaType\u_AntType_classes.pas',
  fr_LinkLine_add in '..\src\LinkLine\fr_LinkLine_add.pas' {frame_LinkLine_add},
  dm_MapEngine_store in '..\src\MapEngine_new\dm_MapEngine_store.pas' {dmMapEngine_store: TDataModule},
  d_Link_Get_Network_Type in '..\src\Link\d_Link_Get_Network_Type.pas' {dlg_Link_Get_Network_Type},
  _dm_Main_Init in '..\src\DataModules\_dm_Main_Init.pas' {dmMain_Init_link: TDataModule};

{$R *.RES}

(*
procedure DoExceptionEvent (Sender: TObject; E: Exception);
begin
  ShowMessage('');
end;

*)
begin
  SetThreadLocale(1049);
                  
  Application.Initialize;

  //Application.OnException :=DoExceptionEvent;

  Application.CreateForm(TdmMain_Init_link, dmMain_Init_link);
  Application.Run;

end.
