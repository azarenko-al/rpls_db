unit i_Google;

interface

//uses ADOInt,
//  u_dll;

type
//11111111111111111111
  IGoogle_X = interface(IInterface)
  ['{129918C1-97A2-4410-97E8-7C7C5D55A9D7}']

    procedure InitADO(aConnection: _Connection); stdcall;
//    procedure InitAppHandle(aHandle: integer); stdcall;

    procedure Execute (aFileName: WideString);  stdcall;
  end;

var
  IGoogle: IGoogle_X;

  function Load_IGoogle: Boolean;
  procedure UnLoad_IGoogle;

implementation

const
//  DEF_BPL_FILENAME = 'bpl_LOS.bpl';
  DEF_FILENAME = 'dll_Google.dll';

var
  LHandle : Integer;


function Load_IGoogle: Boolean;
begin
  Result:=GetInterface_DLL(DEF_FILENAME, LHandle, IGoogle_X, IGoogle)=0;
end;


procedure UnLoad_IGoogle;
begin
  IGoogle := nil;
  UnloadPackage_dll(LHandle);
end;


end.



