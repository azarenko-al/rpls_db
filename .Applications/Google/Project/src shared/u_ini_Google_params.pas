unit u_ini_Google_params;


interface
uses
   IniFiles, SysUtils, Dialogs,

   u_func,
   u_classes
   ;

type
  // ---------------------------------------------------------------
  TIni_Google_Params = class
  // ---------------------------------------------------------------
  public
    ProjectID: Integer;

    Link_IDList: TIDList;
    Property_IDList: TIDList;

    Mode : (mtAll,mtProperty,mtLink);

    IsTest:  Boolean;

    ConnectionString: string;
    
    constructor Create;
    destructor Destroy; override;

    procedure LoadFromFile(aFileName: string);
    procedure SaveToFile(aFileName: string);

    function Validate: boolean;
  end;

var
  g_Ini_Google_Params: TIni_Google_Params;



implementation


constructor TIni_Google_Params.Create;
begin
  inherited Create;
  Link_IDList := TIDList.Create();
  Property_IDList := TIDList.Create();
end;


destructor TIni_Google_Params.Destroy;
begin
  FreeAndNil(Property_IDList);
  FreeAndNil(Link_IDList);
  inherited Destroy;
end;


// ---------------------------------------------------------------
procedure TIni_Google_Params.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
var
  sMode: string;
begin
//  ShellExec_Notepad(aFileName);


///////  Assert(FileExists(aFileName), aFileName);


  if not FileExists(aFileName) then
   Exit;


  with TIniFile.Create(aFileName) do
  begin
    IsTest   :=ReadBool   ('main', 'IsTest', False);

    ProjectID:= ReadInteger ('main', 'ProjectID',  0);
    sMode    := ReadString  ('main', 'Mode', sMode);
       
    ConnectionString := ReadString  ('main', 'ConnectionString', ConnectionString);

Assert (ConnectionString <> '');
  
    
    Free;
  end;


  Link_IDList.LoadIDsFromIniFile(aFileName, 'items');
  Property_IDList.LoadIDsFromIniFile(aFileName, 'Property_items');

  sMode := LowerCase(sMode);

  if Eq(sMode,'All')      then Mode := mtAll else
  if Eq(sMode,'Property') then Mode := mtProperty else
  if Eq(sMode,'Link')     then Mode := mtLink




end;

// ---------------------------------------------------------------
procedure TIni_Google_Params.SaveToFile(aFileName: string);
// ---------------------------------------------------------------
var
  sMode: string;
begin
  Assert(ProjectID>0);

  case Mode of
    mtAll     : sMode:='All';
    mtProperty: sMode:='Property';
    mtLink    : sMode:='Link';

  end;

    //  Mode : (mtAll,mtProperty,mtLink);


  with TIniFile.Create(aFileName) do
  begin
    WriteBool ('main', 'IsTest', IsTest);
  
    WriteInteger ('main', 'ProjectID', ProjectID);
    WriteString  ('main', 'Mode', sMode);

    WriteString  ('main', 'ConnectionString', ConnectionString);
                  
    Free;
  end;


  Link_IDList.SaveIDsToIniFile(aFileName, 'items');
  Property_IDList.SaveIDsToIniFile(aFileName, 'Property_items');

end;




function TIni_Google_Params.Validate: boolean;
begin
  Result :=  (ProjectID>0);

//  if not Result then
 //   ShowMessage('(iLinkID=0) or (iProjectID=0)');

end;


initialization
  g_Ini_Google_Params:=TIni_Google_Params.Create;

finalization  
  FreeAndNil(g_Ini_Google_Params);


end.

