 program google;

uses
  d_Google_export in 'src\d_Google_export.pas' {dlg_Google_export},
  dm_Google_export in 'src\dm_Google_export.pas' {dmGoogle_export},
  dm_App in 'src\dm_App.pas' {dmMain_app: TDataModule},
  Forms,
  u_ini_Google_params in 'src shared\u_ini_Google_params.pas',
  d_Wizard in 'W:\common XE\Wizards\d_Wizard.pas' {dlg_Wizard},
  dm_Main_Data in 'src\dm_Main_Data.pas' {dmMain_Data: TDataModule},
  u_vars in '..\..\u_vars.pas',
  u_const_db in '..\..\EXCEL_to_DB_new\Project\Shared\u_const_db.pas',
  u_Google_classes in 'src\u_Google_classes.pas',
  u_Matrix in 'W:\common XE\matrix\u_Matrix.pas';

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(TdmMain_app, dmMain_app);
  Application.CreateForm(TdmMain_Data, dmMain_Data);
  //  Application.CreateForm(Tdlg_Dataset_Select_records, dlg_Dataset_Select_records);
  Application.Run;
end.
