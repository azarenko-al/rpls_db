object frame_Google_filter: Tframe_Google_filter
  Left = 0
  Top = 0
  Width = 423
  Height = 380
  TabOrder = 0
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 331
    Height = 380
    Align = alLeft
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = DataSource1
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skCount
          Column = cxGrid1DBTableView1name
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      Filtering.MaxDropDownCount = 12
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.Indicator = True
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object cxGrid1DBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 20
      end
      object cxGrid1DBTableView1checked: TcxGridDBColumn
        Caption = #1042#1099#1073#1088#1072#1085#1086
        DataBinding.FieldName = 'checked'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ReadOnly = False
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        Width = 72
      end
      object cxGrid1DBTableView1name: TcxGridDBColumn
        Caption = #1056#1056' '#1080#1085#1090#1077#1088#1074#1072#1083
        DataBinding.FieldName = 'name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Options.Editing = False
        Width = 239
      end
    end
    object cxGrid1Level1: TcxGridLevel
    end
  end
  object qry_Links: TADOQuery
    Parameters = <>
    SQL.Strings = (
      'SELECT id, name FROM Link')
    Left = 361
    Top = 341
  end
  object DataSource1: TDataSource
    Left = 357
    Top = 471
  end
  object dxMemData1: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 359
    Top = 419
  end
end
