unit dm_Google_export;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms, Graphics, XMLDoc, XMLIntf,

  dm_Main_Data,

  u_Ini_Google_Params,

  u_xml_document,

  d_Progress,

  u_const_DB,

  u_db,

  u_google_classes,

  u_classes;


type
  TdmGoogle_export = class(TDataModule)
    qry_Links: TADOQuery;
    qry_Props1: TADOQuery;
    qry_Antenna1: TADOQuery;
    qry_Props2: TADOQuery;
    qry_Antenna2: TADOQuery;
    qry_Projects: TADOQuery;
    ds_Projects: TDataSource;
    ds_Links: TDataSource;
    ds_Props1: TDataSource;
    ds_Props2: TDataSource;
    qry_Linkend1: TADOQuery;
    ds_Linkend1: TDataSource;
    qry_Linkend2: TADOQuery;
    ds_Linkend2: TDataSource;
    qry_Link_Repeater: TADOQuery;
    ds_Link_Repeater: TDataSource;
    qry_Link_Repeater_Ant: TADOQuery;
    ds_Antenna1: TDataSource;
    ds_Antenna2: TDataSource;
    qry_Link_Repeater_prop: TADOQuery;
    qry_Prop: TADOQuery;
    qry_Project: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FDBProject: TDBProject;

    procedure Export_Prop(aParentNode: IXMLNode);
 //   procedure Export_Link(aParentNode: IXMLNode);

  //  procedure Export_linkend(aParentNode: IXMLNode; aProperty1_ID, aLinkEnd_ID:
   //     Integer);
    procedure Export_Link_new(aParentNode: IXMLNode);

//    procedure OpenData;
    procedure OpenData_Links;

    procedure ExecuteProc_Prop;



  public
    Params: record
      ProjectID : Integer;

      XmlFileName: string;
      KmlFileName: string;

      IsExportProperties : Boolean;

      IsShowFrenel: boolean;
      IsLinkLabels: Boolean;

      IsRunGoogle : boolean;

    end;

//    procedure Execute11111111;
    procedure ExecuteProc;

    class procedure Init;
  end;


 // function dmGoogle_export: TdmGoogle_export;

var
  dmGoogle_export: TdmGoogle_export;


implementation
{$R *.dfm}

//const
//  ATT_INDEX = 'INDEX';


//--------------------------------------------------------------------
class procedure TdmGoogle_export.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmGoogle_export) then
    dmGoogle_export:=TdmGoogle_export.Create(Application);

end;


//-------------------------------------------------------------------
procedure TdmGoogle_export.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  db_SetComponentADOConnection(Self, dmMain_Data.ADOConnection_SQL);

  FDBProject:=TDBProject.Create;

//
//  qry_Props1.SQL.Text:='SELECT * FROM '+  TBL_PROPERTY +' WHERE id=:property1_id';
//  qry_Props2.SQL.Text:='SELECT * FROM '+  TBL_PROPERTY +' WHERE id=:property2_id';
//
//  qry_Linkend1.SQL.Text:='SELECT * FROM '+  TBL_Linkend +' WHERE id=:Linkend1_id';
//  qry_Linkend2.SQL.Text:='SELECT * FROM '+  TBL_Linkend +' WHERE id=:Linkend2_id';
//
//
//  qry_Antenna1.SQL.Text:='SELECT * FROM '+  TBL_LINKEND_ANTENNA +' WHERE Linkend_id=:id';
//  qry_Antenna2.SQL.Text:='SELECT * FROM '+  TBL_LINKEND_ANTENNA +' WHERE Linkend_id=:id';
//
//
//  qry_Link_Repeater.SQL.Text    :='SELECT * FROM '+  view_LINK_Repeater +' WHERE id=:LINK_Repeater_id';
//  qry_Link_Repeater_Ant.SQL.Text:='SELECT * FROM '+  TBL_LINK_Repeater_ANTENNA +' WHERE LINK_Repeater_id=:id';
//  qry_Link_Repeater_prop.SQL.Text:='SELECT * FROM '+  TBL_PROPERTY +' WHERE id=:PROPERTY_id';

end;


procedure TdmGoogle_export.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FDBProject);
end;


//
////-------------------------------------------------------------------
//procedure TdmGoogle_export.Execute11111111;
////-------------------------------------------------------------------
//begin
////  Progress_ExecDlg('',ExecuteProc);
//end;



// ---------------------------------------------------------------
procedure TdmGoogle_export.Export_Link_new(aParentNode: IXMLNode);
// ---------------------------------------------------------------

      // ---------------------------------------------------------------
      procedure DoExport_linkend(aParentNode: IXMLNode; aProperty: TProperty; aHEIGHT: double);
      // ---------------------------------------------------------------
   //   var
    //    iHEIGHT: Integer;
      begin
    //     Assert(aLinkEnd.AntennaList.Count>0);
     //    iHEIGHT:=Round(aLinkEnd.AntennaList[0].Height);

          xml_AddNode(aParentNode, 'linkend',
                [
                  xml_Att(FLD_PROPERTY_NAME,  aProperty.Name_),

                  xml_Att(FLD_LAT,     aProperty.lat),
                  xml_Att(FLD_LON,     aProperty.lon),

            //      xml_Att(FLD_LAT_STR, aProperty.lat_str),
             //     xml_Att(FLD_LON_STR, aProperty.lon_str),

                  xml_Att(FLD_HEIGHT,  Round(aHEIGHT))
                ]);
      end;



var
  i: Integer;
  iID: Integer;
 // iIndex: Integer;

  vLink: IXMLNode;
  vLink1: IXMLNode;

  oLink: TLink;
  sName: string;

 // vRoot, vLinks, vSite, vLink: IXMLNode;
begin
 //  if g_Ini_Google_Params.Mode in (mtAll, mtLink) then
   OpenData_Links();



 // vLinks := xml_AddNode(aParentNode, 'links',
    //          [xml_Att('frenel', IIF(Params.IsShowFrenel, 1,0)) ]);

//  xml_AddNode(aParentNode,'ranges', [xml_Att('color', clRed) ]);


//  iRecNo :=0;

 // db_View(qry_Links);

//  iIndex :=0;


  for I := 0 to FDBProject.LinkList.Count - 1 do    // Iterate
  begin
    oLink:=FDBProject.LinkList[i];

    if Params.IsLinkLabels then
      sName:=oLink.Name_
    else
      sName:='';

   // Inc(iIndex);


    if oLink.LinkRepeater.ID=0 then
    begin
      //Inc(iIndex);
      //----------------------------

      vLink := xml_AddNode(aParentNode, 'link',
                [
                //  xml_Att('Index', iIndex),
                  xml_Att('Name',  sName),

                  // Freq in GZ
//                  xml_Att('FREQ',       oLink.LinkEnd1.TX_FREQ_MHZ/1000),
//                  xml_Att(FLD_FREQ_MHZ, oLink.LinkEnd1.TX_FREQ_MHZ/1000)

                  xml_Att('FREQ',       14),
                  xml_Att(FLD_FREQ_MHZ, 14)


                ]);

      if oLink.LinkEnd1.AntennaList.Count=0 then
        Continue;

      if oLink.LinkEnd2.AntennaList.Count=0 then
        Continue;


      Assert(oLink.LinkEnd1.AntennaList.Count>0, 'oLink.LinkEnd1.AntennaList.Count>0');
      Assert(oLink.LinkEnd2.AntennaList.Count>0, 'oLink.LinkEnd2.AntennaList.Count>0');


      DoExport_linkend(vLink, oLink.Property1, oLink.LinkEnd1.AntennaList[0].Height);
      DoExport_linkend(vLink, oLink.Property2, oLink.LinkEnd2.AntennaList[0].Height);

    end else
    begin
      //----------------------------

      if oLink.LinkEnd1.AntennaList.Count=0 then
        Continue;

      if oLink.LinkEnd2.AntennaList.Count=0 then
        Continue;


      Assert(oLink.LinkEnd1.AntennaList.Count>0, 'oLink.LinkEnd1.AntennaList.Count>0');
      Assert(oLink.LinkEnd2.AntennaList.Count>0, 'oLink.LinkEnd2.AntennaList.Count>0');

      //----------------------------
      vLink := xml_AddNode(aParentNode, 'link',
                [
              //    xml_Att('Index', iIndex),
                  xml_Att('Name',  sName),

                  // Freq in GZ
                  xml_Att('FREQ',       oLink.LinkEnd1.TX_FREQ_MHZ/1000),
                  xml_Att(FLD_FREQ_MHZ, oLink.LinkEnd1.TX_FREQ_MHZ/1000)
                ]);


//          oLink.LinkRepeater.

      DoExport_linkend(vLink, oLink.Property1, oLink.LinkEnd1.AntennaList[0].Height);
      DoExport_linkend(vLink, oLink.LinkRepeater.Property_, oLink.LinkRepeater.Antenna1_Height);
      //----------------------------

    //  Inc(iIndex);


      vLink1 := xml_AddNode(aParentNode, 'link',
                [
               //   xml_Att('Index', iIndex),
                  xml_Att('Name',  sName),

                  // Freq in GZ
                  xml_Att('FREQ',       oLink.LinkEnd1.TX_FREQ_MHZ/1000),
                  xml_Att(FLD_FREQ_MHZ, oLink.LinkEnd1.TX_FREQ_MHZ/1000)
                ]);

//      DoExport_linkend(vLink, oLink.Property1, oLink.LinkEnd1.AntennaList[0].Height);
      DoExport_linkend(vLink1, oLink.LinkRepeater.Property_, oLink.LinkRepeater.Antenna2_Height);
      DoExport_linkend(vLink1, oLink.Property2, oLink.LinkEnd2.AntennaList[0].Height);


    end;


  end;


end;



//-------------------------------------------------------------------
procedure TdmGoogle_export.ExecuteProc;
//-------------------------------------------------------------------
var
  oXMLDoc : TXmlDocumentEx;
  sObj: string;
  sProjectName: string;
  vDoc: IXMLNode;
  vLinks,vProperties: IXMLNode;

begin
  Assert(Assigned(g_Ini_Google_Params));


  if g_Ini_Google_Params.Mode in [mtProperty] then
  begin
    ExecuteProc_Prop;

    Exit;

  end;
                         

 // OpenData();

  db_OpenQuery(qry_Project,  Format('SELECT name FROM '+  TBL_PROJECT +' WHERE id=%d ', [g_Ini_Google_Params.ProjectID] ));

  sProjectName:=qry_Project.FieldByName(FLD_NAME).AsString;
   
//  sProjectName:=gl_DB.GetNameByID(TBL_PROJECT, dmMain.ProjectID);

  oXMLDoc := TXmlDocumentEx.Create;

  vDoc:=oXMLDoc.DocumentElement;


  if g_Ini_Google_Params.Mode in [mtProperty] then
    sObj:='site'
  else

//  if g_Ini_Google_Params.Mode in [mtAll,mtLink] then
 //   Export_Link(vLinks);
    sObj:='link';




  xml_AddNode(vDoc, 'exportinfo',
        [xml_Att('kml_filename', Params.KmlFileName),
         xml_Att('project_name', sProjectName),

//         xml_Att('object',       sObj)
         xml_Att('object',       'link')
        ]);



//  if g_Ini_Google_Params.Mode in [mtProperty] then
//    vProperties := vDoc.AddChild('sites')
//  else
//    vProperties := vDoc.AddChild('properties');


  vProperties := vDoc.AddChild('properties');

  vLinks := vDoc.AddChild('links');
  vLinks.Attributes['frenel'] := IIF(Params.IsShowFrenel, 1,0) ;

  xml_AddNode(vLinks,'ranges', [xml_Att('color', clRed) ]);


  if g_Ini_Google_Params.Mode in [mtAll,mtProperty] then
    Export_Prop(vProperties);

  if g_Ini_Google_Params.Mode in [mtAll,mtLink] then
 //   Export_Link(vLinks);
    Export_Link_new(vLinks);

  oXMLDoc.SaveToFile(Params.XmlFileName);

  FreeAndNil(oXMLDoc);

end;

//-------------------------------------------------------------------
procedure TdmGoogle_export.ExecuteProc_Prop;
//-------------------------------------------------------------------
 (*  XML_TEXT =
      '<?xml version="1.0" encoding="windows-1251"?>'+
      '<?xml:stylesheet type="text/xsl" href=""?>   '+
      '<Document>                                   '+
      '  <exportinfo />                             '+
      '  <sites>                                    '+
      '    <cell_layers length="10" color="16711680">                          '+
      '        <cell_layer id="7" name="DCS1800" length="5" color="16711680"/> '+
      '        <cell_layer id="8" name="GSM900" length="10" color="255"/>      '+
      '        <cell_layer id="9" name="EGSM900" length="5" color="65280"/>    '+
      '    </cell_layers>                                                      '+
      '  </sites>                                   '+
      '</Document>                                  ';
  SQL_SEL = 'SELECT id, name, address, lat, lon FROM property where %s ORDER BY name';

<?xml version="1.0" encoding="windows-1251"?>
<?xml:stylesheet type="text/xsl" href=""?>
<Document>
  <exportinfo kml_filename="c:\g.kml" project_name="project" object="site"/>
  <sites three-dimensional="1">
    <cell_layers length="50" color="65280">
      <cell_layer id="2" name="DCS1800" length="20" color="255"/>
      <cell_layer id="1" name="GSM900" length="30" color="16711680"/>
    </cell_layers>
    <site id="183998" name="tm001" address="" lat="37.889421" lon="58.346547">
      <antenna id="766827" cell_layer_id="1" name="???????" azimuth="150" height="91" tilt="-5" horz_width="68" vert_width="16"/>
      <antenna id="766828" cell_layer_id="1" name="???????" azimuth="270" height="93" tilt="-14" horz_width="68" vert_width="16"/>
      <antenna id="766829" cell_layer_id="1" name="???????" azimuth="151" height="93" tilt="-14" horz_width="68" vert_width="16"/>
    </site>
  </sites>
</Document>


*)



var
  oXMLDoc : TXmlDocumentEx;
  sProjectName: string;
  sWhere: string;
  vDoc: IXMLNode;
 // vLinks,
  vSites: IXMLNode;
  vCell_layers: IXMLNode;

begin
  Assert(Assigned(g_Ini_Google_Params));

  sWhere:=g_Ini_Google_Params.Property_IDList.MakeWhereString;

  db_OpenQuery(qry_Props1,   Format('SELECT * FROM '+  TBL_PROPERTY +' WHERE (id in (%s))',   [sWhere] ));
//      'SELECT * FROM '+  TBL_PROPERTY +' WHERE (project_id=:project_id) and (id in ())',
 //     [db_Par(FLD_project_id, Params.ProjectID)] );


//  OpenData();



  db_OpenQuery(qry_Project,  Format('SELECT name FROM '+  TBL_PROJECT +' WHERE id=%d ', [g_Ini_Google_Params.ProjectID] ));


  sProjectName:=qry_Project.FieldByName(FLD_NAME).AsString;
  
//  gl_DB.GetNameByID(TBL_PROJECT, dmMain.ProjectID);

  oXMLDoc := TXmlDocumentEx.Create;

  vDoc:=oXMLDoc.DocumentElement;

  xml_AddNode(vDoc, 'exportinfo',
        [xml_Att('kml_filename', Params.KmlFileName),
         xml_Att('project_name', sProjectName),
         xml_Att('object',       'site')
        ]);


  vSites := vDoc.AddChild('sites');

    // -------------------------
    // ������������� ��� ������ �������
    // -------------------------
    vCell_layers:=vSites.AddChild('cell_layers');
    vCell_layers.Attributes['color']:='16711680';


////////////  Export_Prop (vSites);



  with qry_Props1 do
    while not EOF do
    begin
    //  iID:=FieldByName(FLD_ID).AsInteger;

      // -------------------------
      // CK 42 !!!!!
      // -------------------------
      xml_AddNode(vSites, 'site',
              [
               xml_Att('id',      FieldByName(FLD_ID).AsInteger),
               xml_Att('Name',    FieldByName(FLD_NAME).AsString),
               xml_Att('address', FieldByName(FLD_address).AsString),
               xml_Att('lat',     FieldByName(FLD_LAT).AsFloat),
               xml_Att('lon',     FieldByName(FLD_LON).AsFloat)

            //   xml_Att('lat_str', FieldByName(FLD_LAT_STR).AsString),
             //  xml_Att('lon_str', FieldByName(FLD_LON_STR).AsString)

              ]);
      Next;
    end;



//  vLinks := vDoc.AddChild('links');
 // vLinks.Attributes['frenel'] := IIF(Params.IsShowFrenel, 1,0) ;

 // xml_AddNode(vLinks,'ranges', [xml_Att('color', clRed) ]);


 // if g_Ini_Google_Params.Mode in [mtAll,mtProperty] then
  //  Export_Prop(vProperties);

 // if g_Ini_Google_Params.Mode in [mtAll,mtLink] then
  //  Export_Link(vLinks);

  oXMLDoc.SaveToFile(Params.XmlFileName);

  FreeAndNil(oXMLDoc);

end;

// ---------------------------------------------------------------
procedure TdmGoogle_export.Export_Prop(aParentNode: IXMLNode);
// ---------------------------------------------------------------
var
  iID: Integer;
  sID: string;
  sSQL: string;
 // vProperties,vDoc,vChild: IXMLNode;
begin
  if not Params.IsExportProperties then
    Exit;


   if g_Ini_Google_Params.Mode = mtAll then
   begin
//    Assert(dmMain.ProjectID>0);

    sSQL:=Format('SELECT * FROM '+ TBL_PROPERTY+ ' WHERE project_id =%d', [g_Ini_Google_Params.ProjectID]);
  end else begin
    sID:=g_Ini_Google_Params.Property_IDList.MakeDelimitedText_ID;

    sSQL:=Format('SELECT * FROM '+ TBL_PROPERTY+ ' WHERE id IN (%s)', [sID]);
  end;


  db_OpenQuery(qry_Prop, sSQL);



  with qry_Prop do
    while not EOF do
  begin
   // iID:=FieldByName(FLD_ID).AsInteger;

 //  Params.IsUseFilter and
//    if (g_Ini_Google_Params.Property_IDList.Count>0) and
//       (g_Ini_Google_Params.Property_IDList.IndexByID (iID)<0) then
//    begin
//      Next;
//      Continue;
//    end;

    xml_AddNode(aParentNode, 'property',
            [
             xml_Att('id',      FieldByName(FLD_ID).AsInteger),
             xml_Att('Name',    FieldByName(FLD_NAME).AsString),
             xml_Att('address', FieldByName(FLD_address).AsString),
             xml_Att('lat',     FieldByName(FLD_LAT).AsFloat),
             xml_Att('lon',     FieldByName(FLD_LON).AsFloat)

         //    xml_Att('lat_str', FieldByName(FLD_LAT_STR).AsString),
         //    xml_Att('lon_str', FieldByName(FLD_LON_STR).AsString)

            ]);
    Next;
  end;

end;



// ---------------------------------------------------------------
procedure TdmGoogle_export.OpenData_Links;
// ---------------------------------------------------------------
var
  iLinkRepeater_ID: Integer;
  iProp1: Integer;
  iProp2: Integer;
  sID: string;
  sObjName: string;

   oLink: TLink;
  sSQL: string;
begin

  if g_Ini_Google_Params.Mode = mtAll then
  begin
//    Assert(dmMain.ProjectID>0);

    //VIEW_link_location

    sSQL:=Format('SELECT * FROM '+ VIEW_link_location+ ' WHERE project_id =%d', [g_Ini_Google_Params.ProjectID])
    
  end else begin
    sID:=g_Ini_Google_Params.Link_IDList.MakeDelimitedText_ID;

    sSQL:=Format('SELECT * FROM '+ VIEW_link_location+ ' WHERE id IN (%s)', [sID]);
    
  end;


  db_OpenQuery(qry_Links, sSQL);


//  db_View (qry_Links);


//  if g_Ini_Google_Params.Link_IDList.Count > 0 then
//  begin
//    sID:=g_Ini_Google_Params.Link_IDList.MakeDelimitedText_ID;

//    db_OpenQuery(qry_Links,  Format('SELECT * FROM '+ TBL_LINK+ ' WHERE id IN (%s)', [sID]));


    /////////////////////////

    qry_Props1.SQL.Text:='SELECT * FROM '+  TBL_PROPERTY +' WHERE id=:property_id';
    qry_Props2.SQL.Text:='SELECT * FROM '+  TBL_PROPERTY +' WHERE id=:property_id';


//    qry_Props1.SQL.Text:='SELECT * FROM '+  TBL_PROPERTY +' WHERE id=:property1_id';
//    qry_Props2.SQL.Text:='SELECT * FROM '+  TBL_PROPERTY +' WHERE id=:property2_id';


    qry_Linkend1.SQL.Text:='SELECT * FROM '+  TBL_Linkend +' WHERE id=:Linkend1_id';
    qry_Linkend2.SQL.Text:='SELECT * FROM '+  TBL_Linkend +' WHERE id=:Linkend2_id';


    qry_Antenna1.SQL.Text:='SELECT * FROM '+  TBL_LINKEND_ANTENNA +' WHERE Linkend_id=:id';
    qry_Antenna2.SQL.Text:='SELECT * FROM '+  TBL_LINKEND_ANTENNA +' WHERE Linkend_id=:id';


    qry_Link_Repeater.SQL.Text    :='SELECT * FROM '+  view_LINK_Repeater +' WHERE id=:LINK_Repeater_id';
    qry_Link_Repeater_Ant.SQL.Text:='SELECT * FROM '+  TBL_LINK_Repeater_ANTENNA +' WHERE LINK_Repeater_id=:id';
    qry_Link_Repeater_prop.SQL.Text:='SELECT * FROM '+  TBL_PROPERTY +' WHERE id=:PROPERTY_id';





    qry_Linkend1.Open;
    qry_Linkend2.Open;

    qry_Antenna1.Open;
    qry_Antenna2.Open;

    qry_Props1.Open;
    qry_Props2.Open;



    Assert(not qry_Props1.IsEmpty);
    Assert(not qry_Props2.IsEmpty);

{
    Assert(qry_Props1.RecordCount=1);
    Assert(qry_Props2.RecordCount=1);

    Assert(qry_Linkend1.RecordCount=1);
    Assert(qry_Linkend2.RecordCount=1);}

    /////////////////////////

  //  FDBProject.LinkList.LoadFromDataset(qry_Links);
  //  FDBProject.LinkList.Prepare();


    with qry_Links do
      while not EOF do
      begin
        oLink:=FDBProject.LinkList.AddItem();

        oLink.Name_  :=FieldByName(FLD_Name).AsString;
        oLink.ObjName:=FieldByName(FLD_ObjName).AsString;
        
        oLink.LinkRepeater_ID := FieldByName(FLD_LINK_REPEATER_ID).AsInteger;


        oLink.Property1.LoadFromDataset(qry_Props1);
        oLink.Property2.LoadFromDataset(qry_Props2);

        oLink.LinkEnd1.LoadFromDataset(qry_Linkend1);
        oLink.LinkEnd2.LoadFromDataset(qry_Linkend2);

        oLink.LinkEnd1.AntennaList.LoadFromDataset(qry_Antenna1);
        oLink.LinkEnd2.AntennaList.LoadFromDataset(qry_Antenna2);


        if oLink.LinkRepeater_ID>0 then
        begin
          qry_Link_Repeater.Open;
          qry_Link_Repeater_Ant.Open;
          qry_Link_Repeater_prop.Open;

          Assert(not qry_Link_Repeater_prop.IsEmpty, 'not qry_Link_Repeater_prop.IsEmpty');

          oLink.LinkRepeater.LoadFromDataset(qry_Link_Repeater);

          oLink.LinkRepeater.Property_.LoadFromDataset(qry_Link_Repeater_prop);

        end;


//
//        oLink.PROPERTY1_id:=FieldByName(FLD_PROPERTY1_ID).AsInteger;
//        oLink.PROPERTY2_id:=FieldByName(FLD_PROPERTY2_ID).AsInteger;
//
//        oLink.LinkEnd1_ID:=FieldByName(FLD_LINKEND1_ID).AsInteger;
//        oLink.LinkEnd2_ID:=FieldByName(FLD_LINKEND2_ID).AsInteger;
//


//        oLink.LinkRepeater_ID :=  FieldByName(FLD_Link_Repeater_id).AsInteger;


      //  Next;

//
//
//        iProp1:=qry_Links.FieldByName(FLD_PROPERTY1_ID).AsInteger;
//        iProp2:=qry_Links.FieldByName(FLD_PROPERTY2_ID).AsInteger;
//
//        sObjName:=qry_Links.FieldByName(FLD_ObjName).AsString;
//
//        iLinkRepeater_ID := FieldByName(FLD_Link_Repeater_id).AsInteger;


        Next;
      end;


  //  exit;

 // end;


end;



begin

end.



