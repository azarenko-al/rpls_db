inherited dlg_Google_export: Tdlg_Google_export
  Left = 1297
  Top = 198
  Caption = 'dlg_Google_export'
  ClientHeight = 447
  ClientWidth = 624
  FormStyle = fsStayOnTop
  OldCreateOrder = True
  ExplicitLeft = 1297
  ExplicitTop = 198
  ExplicitWidth = 632
  ExplicitHeight = 475
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 412
    Width = 624
    ExplicitTop = 398
    ExplicitWidth = 655
    inherited Bevel1: TBevel
      Width = 624
      ExplicitWidth = 655
    end
    inherited Panel3: TPanel
      Left = 451
      Width = 173
      ExplicitLeft = 482
      ExplicitWidth = 173
      inherited btn_Ok: TButton
        Left = 2
        ModalResult = 0
        ExplicitLeft = 2
      end
      inherited btn_Cancel: TButton
        Left = 86
        ExplicitLeft = 86
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 624
    ExplicitWidth = 655
    inherited Bevel2: TBevel
      Width = 624
      ExplicitWidth = 655
    end
    inherited pn_Header: TPanel
      Width = 624
      ExplicitWidth = 655
    end
  end
  object pn_File: TPanel [2]
    Left = 0
    Top = 113
    Width = 624
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 655
    DesignSize = (
      624
      48)
    object Label1: TLabel
      Left = 5
      Top = 7
      Width = 146
      Height = 13
      Caption = #1060#1072#1081#1083' KML '#1087#1088#1086#1077#1082#1090#1072' GOOGLE'
    end
    object ed_FileName: TFilenameEdit
      Left = 5
      Top = 22
      Width = 615
      Height = 22
      Filter = #1042#1089#1077' '#1092#1072#1081#1083#1099' (*.kml)|*.kml'
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 0
      Text = 'c:\google.kml'
      ExplicitWidth = 646
    end
  end
  object pn_DB_Project: TPanel [3]
    Left = 0
    Top = 60
    Width = 624
    Height = 53
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 655
    DesignSize = (
      624
      53)
    object Label2: TLabel
      Left = 5
      Top = 5
      Width = 97
      Height = 13
      Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1087#1088#1086#1077#1082#1090#1072':'
    end
    object Bevel3: TBevel
      Left = 0
      Top = 50
      Width = 624
      Height = 3
      Align = alBottom
      Shape = bsBottomLine
      ExplicitWidth = 655
    end
    object DBLookupComboBox_Projects: TDBLookupComboBox
      Left = 3
      Top = 24
      Width = 618
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      DropDownRows = 20
      KeyField = 'id'
      ListField = 'name'
      ListSource = ds_Projects
      TabOrder = 0
      ExplicitWidth = 649
    end
  end
  object StatusBar1: TStatusBar [4]
    Left = 0
    Top = 393
    Width = 624
    Height = 19
    Panels = <>
    SimplePanel = True
    Visible = False
  end
  object Panel1: TPanel [5]
    Left = 0
    Top = 161
    Width = 624
    Height = 136
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 5
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 209
      Height = 136
      Align = alLeft
      TabOrder = 0
      object cb_Frenel: TCheckBox
        Left = 8
        Top = 12
        Width = 185
        Height = 17
        Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1079#1086#1085#1091' '#1060#1088#1077#1085#1077#1083#1103
        TabOrder = 0
      end
      object cb_LinkLabels: TCheckBox
        Left = 8
        Top = 34
        Width = 193
        Height = 17
        Caption = #1055#1086#1076#1087#1080#1089#1080' '#1085#1072' '#1056#1056' '#1080#1085#1090#1077#1088#1074#1072#1083#1072#1093
        TabOrder = 1
      end
      object cb_Prop1: TCheckBox
        Left = 8
        Top = 58
        Width = 193
        Height = 23
        Caption = #1069#1082#1089#1087#1086#1088#1090' '#1087#1083#1086#1097#1072#1076#1086#1082
        Checked = True
        State = cbChecked
        TabOrder = 2
      end
      object cb_Run_Google_Earth: TCheckBox
        Left = 8
        Top = 82
        Width = 193
        Height = 17
        Caption = 'Run Google Earth'
        Checked = True
        State = cbChecked
        TabOrder = 3
      end
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
    inherited act_Cancel: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
    end
  end
  inherited FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\common\Forms\Tdlg_Google_export'
    StoredProps.Strings = (
      'ed_FileName.FileName'
      'cb_Frenel.Checked'
      'cb_LinkLabels.Checked'
      'cb_Prop1.Checked'
      'cb_Run_Google_Earth.Checked')
  end
  object qry_Projects: TADOQuery
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'SELECT id,name FROM PROJECT ORDER BY name')
    Left = 74
    Top = 315
  end
  object ds_Projects: TDataSource
    DataSet = qry_Projects
    Left = 28
    Top = 316
  end
end
