unit Unit43111111111111;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxCheckBox,
  cxRadioGroup, cxVGrid, cxDBVGrid, cxInplaceContainer, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, Vcl.ExtCtrls, cxSplitter, cxDBLookupComboBox,
  dxmdaset, Data.Win.ADODB, Vcl.Menus, Vcl.StdActns, System.Actions,
  Vcl.ActnList, Vcl.Grids, Vcl.DBGrids;

type
  TForm43 = class(TForm)
    cxSplitter1: TcxSplitter;
    Panel1: TPanel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1_project: TcxGridDBTableView;
    cxGrid2DBTableView1_projectenabled: TcxGridDBColumn;
    cxGrid2DBTableView1_projectuser_id: TcxGridDBColumn;
    cxGrid2DBTableView1_projectProject_ID: TcxGridDBColumn;
    cxGrid2DBTableView1_projectname: TcxGridDBColumn;
    cxGrid2DBTableView2_Geo: TcxGridDBTableView;
    cxGrid2DBTableView2_Geoenabled: TcxGridDBColumn;
    cxGrid2DBTableView2_Geouser_id: TcxGridDBColumn;
    cxGrid2DBTableView2_GeoGeoRegion_ID: TcxGridDBColumn;
    cxGrid2DBTableView2_GeoColumn1: TcxGridDBColumn;
    cxGrid2DBTableView2_Geoname: TcxGridDBColumn;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2Level2: TcxGridLevel;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1UID: TcxDBEditorRow;
    cxDBVerticalGrid1Name: TcxDBEditorRow;
    row_Address: TcxDBEditorRow;
    row_email: TcxDBEditorRow;
    row_Phone: TcxDBEditorRow;
    row_Comment: TcxDBEditorRow;
    row_Lib: TcxDBEditorRow;
    row_project: TcxDBEditorRow;
    DBGrid1: TDBGrid;
    DataSource1__users: TDataSource;
    ADOStoredProc1_User_Projects: TADOStoredProc;
    DataSource1_User_Projects: TDataSource;
    cxLookAndFeelController1: TcxLookAndFeelController;
    ADOStoredProc1: TADOStoredProc;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    ActionList1: TActionList;
    acr_Refresh: TAction;
    act_Save: TAction;
    act_user_Add: TAction;
    act_User_del: TAction;
    act_Active_Users: TAction;
    FileExit1: TFileExit;
    MainMenu1: TMainMenu;
    User1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    Exit1: TMenuItem;
    PopupMenu1: TPopupMenu;
    N4: TMenuItem;
    N3: TMenuItem;
    N5: TMenuItem;
    acrRefresh1: TMenuItem;
    ADOTable1_Users: TADOTable;
    ADOStoredProc_GeoRegion_SEL: TADOStoredProc;
    ds_ADOStoredProc_sp_User_GeoRegion_SEL: TDataSource;
    dxMemData_lib: TdxMemData;
    dxMemData_libid: TIntegerField;
    dxMemData_libname: TStringField;
    DataSource_lib: TDataSource;
    dxMemData_project: TdxMemData;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    DataSource1_project: TDataSource;
    ActionList2: TActionList;
    act_Check_all: TAction;
    act_UnCheck_all: TAction;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1_Users: TcxGridDBTableView;
    cxGrid1DBTableView1_UsersUID: TcxGridDBColumn;
    cxGrid1DBTableView1_UsersName: TcxGridDBColumn;
    cxGrid1DBTableView1_Usersis_admin: TcxGridDBColumn;
    col_comments: TcxGridDBColumn;
    cxGrid1DBTableView1_UsersColumn1: TcxGridDBColumn;
    cxGrid1DBTableView1_Usersemail: TcxGridDBColumn;
    cxGrid1DBTableView1_Usersaddress: TcxGridDBColumn;
    cxGrid1DBTableView1_Usersphone: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form43: TForm43;

implementation

{$R *.dfm}

end.
