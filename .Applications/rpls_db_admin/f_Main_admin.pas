unit f_Main_admin;

interface

uses
  System.SysUtils, System.Variants, System.Classes, 
  Vcl.Controls, Vcl.Forms, cxLookAndFeels,
  cxStyles, 
  Data.DB, 
  Data.Win.ADODB, cxDBVGrid, cxSplitter,
  cxGridCustomTableView, cxGridDBTableView, cxGridLevel,
  cxGrid,

  CodeSiteLogging,
  
  

  d_ViewUsersActivity,
  
  d_User_Add,

  u_cx,
  
  u_db,
  u_dlg,

//  u_db,
  

  Vcl.StdCtrls, Vcl.DBGrids, Vcl.ExtCtrls, System.Actions,
  Vcl.ActnList, Vcl.ComCtrls, Vcl.Menus, 
  dxmdaset, Vcl.StdActns, Vcl.DBCtrls, cxGraphics, cxControls,
  cxLookAndFeelPainters, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, cxDBLookupComboBox, cxCheckBox, cxRadioGroup,
  cxClasses, Vcl.Grids, Vcl.ToolWin, cxVGrid, cxInplaceContainer,
  cxGridTableView, cxGridCustomView
  ;

type
  TOnPostModifiedData = procedure (aDataset: TDataset) of object;


  Tfrm_Main_admin = class(TForm)
    DataSource1__users: TDataSource;
    cxSplitter1: TcxSplitter;
    ADOStoredProc1_User_Projects: TADOStoredProc;
    DataSource1_User_Projects: TDataSource;
    cxLookAndFeelController1: TcxLookAndFeelController;
    ADOStoredProc1: TADOStoredProc;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    Panel1: TPanel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1_project: TcxGridDBTableView;
    cxGrid2DBTableView1_projectenabled: TcxGridDBColumn;
    cxGrid2DBTableView1_projectuser_id: TcxGridDBColumn;
    cxGrid2DBTableView1_projectProject_ID: TcxGridDBColumn;
    cxGrid2DBTableView1_projectname: TcxGridDBColumn;
    cxGrid2Level1: TcxGridLevel;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1UID: TcxDBEditorRow;
    cxDBVerticalGrid1Name: TcxDBEditorRow;
    ActionList1: TActionList;
    acr_Refresh: TAction;
    act_user_Add: TAction;
    act_User_del: TAction;
    MainMenu1: TMainMenu;
    User1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    PopupMenu1: TPopupMenu;
    acrRefresh1: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    cxGrid2Level2: TcxGridLevel;
    cxGrid2DBTableView2_Geo: TcxGridDBTableView;
    ADOTable1_Users: TADOTable;
    ADOStoredProc_GeoRegion_SEL: TADOStoredProc;
    ds_ADOStoredProc_sp_User_GeoRegion_SEL: TDataSource;
    cxGrid2DBTableView2_Geoenabled: TcxGridDBColumn;
    cxGrid2DBTableView2_Geouser_id: TcxGridDBColumn;
    cxGrid2DBTableView2_GeoGeoRegion_ID: TcxGridDBColumn;
    cxGrid2DBTableView2_Geoname: TcxGridDBColumn;
    DBGrid1: TDBGrid;
    row_Address: TcxDBEditorRow;
    row_Lib: TcxDBEditorRow;
    row_email: TcxDBEditorRow;
    row_Phone: TcxDBEditorRow;
    row_Comment: TcxDBEditorRow;
    row_project: TcxDBEditorRow;
    act_Active_Users: TAction;
    N6: TMenuItem;
    N7: TMenuItem;
    FileExit1: TFileExit;
    Exit1: TMenuItem;
    N8: TMenuItem;
    cxStyle2: TcxStyle;
    ActionList2: TActionList;
    act_Check_all: TAction;
    act_UnCheck_all: TAction;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1_Users: TcxGridDBTableView;
    cxGrid1DBTableView1_UsersUID: TcxGridDBColumn;
    cxGrid1DBTableView1_UsersName: TcxGridDBColumn;
    cxGrid1DBTableView1_Usersis_admin: TcxGridDBColumn;
    col_comments: TcxGridDBColumn;
    cxGrid1DBTableView1_UsersColumn1: TcxGridDBColumn;
    cxGrid1DBTableView1_Usersemail: TcxGridDBColumn;
    cxGrid1DBTableView1_Usersaddress: TcxGridDBColumn;
    cxGrid1DBTableView1_Usersphone: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    StatusBar1: TStatusBar;
    procedure acr_RefreshExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_Active_UsersExecute(Sender: TObject);
    procedure act_user_AddExecute(Sender: TObject);
    procedure act_User_delExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ADOQuery1_UsersAfterScroll(DataSet: TDataSet);
    procedure ADOStoredProc1_User_ProjectsAfterPost(DataSet: TDataSet);
    procedure ADOStoredProc_GeoRegion_SELAfterPost(DataSet: TDataSet);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure cxGrid1DBTableView1_UsersFocusedRecordChanged(Sender:
        TcxCustomGridTableView; APrevFocusedRecord, AFocusedRecord:
        TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
    procedure Panel1Click(Sender: TObject);
  private
    FEnableDataScroll : boolean;
  
    procedure DoOnPostGeo(aDataset: TDataset);
    procedure DoOnPostProjects(aDataset: TDataset);
//    procedure Post_Project_XREF;
    procedure Refresh_Users;
    procedure Save;

    procedure Refresh_User_projects;
    
    procedure Save_Projects11(aDataset: TCustomADODataSet; aProc:
        TOnPostModifiedData);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_Main_admin: Tfrm_Main_admin;

implementation

{$R *.dfm}

uses dm_App, Winapi.ADOInt;


procedure Tfrm_Main_admin.acr_RefreshExecute(Sender: TObject);
begin
    Refresh_Users;
end;

procedure Tfrm_Main_admin.ActionList1Update(Action: TBasicAction; var Handled:
    Boolean);
begin
 // act_Save.Enabled:=FDataChanged;
end;


procedure Tfrm_Main_admin.act_Active_UsersExecute(Sender: TObject);
begin
  Tdlg_ViewUsersActivity.ExecDlg;
end;

procedure Tfrm_Main_admin.act_user_AddExecute(Sender: TObject);
begin

  if Tdlg_User_Add.ExecDlg then
    ADOTable1_Users.requery;

  
end;

// ---------------------------------------------------------------
procedure Tfrm_Main_admin.act_User_delExecute(Sender: TObject);
// ---------------------------------------------------------------
var
  k: Integer;
begin
  if ConfirmDlg('������� ������������ ?') then
  begin
    k:=db_ExecStoredProc__(ADOStoredProc1, 'Security.sp_User_Del',
    [
//      FLD_login, ADOTable1_Users[FLD_NAME],
      FLD_ID, ADOTable1_Users[FLD_ID]
      
//      FLD_project_id, ADOStoredProc1_User_Projects[FLD_project_id],
//      FLD_ENABLED, ADOStoredProc1_User_Projects[FLD_ENABLED]   
   
    ] );  

    ADOTable1_Users.requery;

  end;



end;

//const
//  DEF_sp_User_Security_XREF_UPD = 'Security.sp_User_Security_XREF_UPD';


// ---------------------------------------------------------------
procedure Tfrm_Main_admin.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  Caption:='��������� ������������� DB Link';

 // cxGrid1Level1.Active


  cxGrid1.Align:=alClient;
  cxGrid2.Align:=alClient;  
  
  
  db_SetComponentADOConnection(Self, dmApp_admin.ADOConnection1);


  db_ExecStoredProc__(ADOStoredProc1, 'Security.sp_User_Init', [] ) ;
                                                               

  cx_SetColumnCaptions1(cxGrid1DBTableView1_Users,

//  db_SetFieldCaptions(ADOQuery1_Users,
  [
    FLD_NAME,            '�����',
    'is_admin',          '�������������',    
    'is_modify_lib',     '�������������� ������������',
    'is_modify_project', '�������������� ��������',
    'comments', '����������',
    'email',    'Email',
    'address',  '�����',    
    'phone',    '���'    

  ]);


  cx_SetColumnCaptions1(cxGrid2DBTableView1_project,
// db_SetFieldCaptions(ADOStoredProc1_User_Projects,
  [
    FLD_NAME, '������',
    'enabled', '��������'    

  ]);
          
  cx_SetColumnCaptions1(cxGrid2DBTableView2_Geo,
// db_SetFieldCaptions(ADOStoredProc1_User_Projects,
  [
    FLD_NAME, '������',
    'enabled', '��������'    

  ]);


  Refresh_Users;

//  FEnableDataScroll :=true;
  
  



//  dmApp_admin.ADOConnection1.
                                                        

end;


procedure Tfrm_Main_admin.ADOQuery1_UsersAfterScroll(DataSet: TDataSet);
begin
  Refresh_User_projects;
end;

procedure Tfrm_Main_admin.ADOStoredProc1_User_ProjectsAfterPost(DataSet: TDataSet);
begin
  DoOnPostProjects (ADOStoredProc1_User_Projects);

 // Post_Project_XREF;
end;

procedure Tfrm_Main_admin.ADOStoredProc_GeoRegion_SELAfterPost(DataSet:
    TDataSet);
begin
   DoOnPostGeo (ADOStoredProc_GeoRegion_SEL);

end;

procedure Tfrm_Main_admin.Button1Click(Sender: TObject);

begin
  Save;

end;

procedure Tfrm_Main_admin.Button2Click(Sender: TObject);
begin
 // dmApp_admin.ADOConnection1.CommitTrans;
end;

procedure Tfrm_Main_admin.Button3Click(Sender: TObject);
begin
 // dmApp_admin.ADOConnection1.BeginTrans;
end;


procedure Tfrm_Main_admin.cxGrid1DBTableView1_UsersFocusedRecordChanged(Sender:
    TcxCustomGridTableView; APrevFocusedRecord, AFocusedRecord:
    TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
 Refresh_User_projects;
end;



procedure Tfrm_Main_admin.Refresh_Users;
begin
  FEnableDataScroll:=False;

  db_TableOpen1(ADOTable1_Users, 'Security.view_Users');   

  FEnableDataScroll:=True;
  
  Refresh_User_projects;
  
end;




// ---------------------------------------------------------------
procedure Tfrm_Main_admin.DoOnPostProjects(aDataset: TDataset);
// ---------------------------------------------------------------
var
  k: Integer;
begin
  k:=db_ExecStoredProc__(ADOStoredProc1, 'Security.sp_User_Security_XREF_UPD',
  [
    FLD_user_id,    aDataset[FLD_user_id],
    FLD_project_id, aDataset[FLD_project_id],
    FLD_ENABLED,    aDataset[FLD_ENABLED]   
   
  ] );

  Assert(k>0);

end;


// ---------------------------------------------------------------
procedure Tfrm_Main_admin.DoOnPostGeo(aDataset: TDataset);
// ---------------------------------------------------------------
var
  k: Integer;
begin
  k:=db_ExecStoredProc__(ADOStoredProc1, 'Security.sp_User_Security_XREF_UPD',
  [
    FLD_user_id,      aDataset[FLD_user_id],
    FLD_GeoRegion_id, aDataset[FLD_GeoRegion_id],
    FLD_ENABLED,      aDataset[FLD_ENABLED]   
   
  ] );

  Assert(k>0);
  
end;

procedure Tfrm_Main_admin.Panel1Click(Sender: TObject);
begin

end;


// ---------------------------------------------------------------
procedure Tfrm_Main_admin.Save;
// ---------------------------------------------------------------
var
  k: Integer;
  s: string;
  v: variant;
begin
  Save_Projects11(ADOStoredProc1_User_Projects, DoOnPostProjects);

  Save_Projects11(ADOStoredProc_GeoRegion_SEL, DoOnPostGeo);


  Refresh_User_projects;   
             
end;


// ---------------------------------------------------------------
procedure Tfrm_Main_admin.Save_Projects11(aDataset: TCustomADODataSet; aProc:
    TOnPostModifiedData);
// ---------------------------------------------------------------
var
  k: Integer;
  s: string;
  v: variant;
begin
// ADOStoredProc1_User_Projects.

    db_PostDataset(aDataset);
     

    v:=aDataset.Recordset.Filter;

    aDataset.Recordset.Filter:=adFilterPendingRecords ;
    
//    exit;
    
    k:=aDataset.RecordCount;
    
    aDataset.First;
    
    with aDataset do
      while not EOF do
      begin
        s:=FieldByName(FLD_NAME).AsString;
      
      aProc (aDataset);
      
//        Post_Project_XREF;
      //v:=Recordset. EditMode;
                 
        next;
      end;

end;



// ---------------------------------------------------------------
procedure Tfrm_Main_admin.Refresh_User_projects;
// ---------------------------------------------------------------
var
  id: Integer;
  k: Integer;
begin
//////////////////////
 // Exit;

  if not FEnableDataScroll then
    Exit;
     

  CodeSite.SendMsg('Refresh_User_projects');
     

//  if (not ADOTable1_Users.Active) or (ADOTable1_Users.RecordCount=0) then
//    Exit;
 
//  ShowMessage('1');  
    
  id:=ADOTable1_Users.FieldByName(FLD_ID).AsInteger;
                

                
                                               
  k:=db_ExecStoredProc__(ADOStoredProc1_User_Projects, 'Security.sp_User_Projects_SEL',
  [
    FLD_user_id, id
   
  ], true ) ;


    k:=db_ExecStoredProc__(ADOStoredProc_GeoRegion_SEL , 'Security.sp_User_GeoRegion_SEL',
  [
    FLD_user_id, id
   
  ], true ) ;


 // ShowMessage('2');  
  

//
end;

end.
