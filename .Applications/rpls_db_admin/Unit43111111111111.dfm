object Form43: TForm43
  Left = 498
  Top = 223
  Caption = 'Form43'
  ClientHeight = 758
  ClientWidth = 1446
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object cxSplitter1: TcxSplitter
    Left = 873
    Top = 0
    Width = 8
    Height = 758
    HotZoneClassName = 'TcxXPTaskBarStyle'
    AlignSplitter = salRight
    Control = Panel1
    ExplicitHeight = 668
  end
  object Panel1: TPanel
    Left = 881
    Top = 0
    Width = 565
    Height = 758
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 668
    object cxGrid2: TcxGrid
      Left = 0
      Top = 454
      Width = 565
      Height = 304
      Align = alBottom
      TabOrder = 0
      LevelTabs.Style = 3
      RootLevelOptions.DetailTabsPosition = dtpTop
      ExplicitTop = 364
      object cxGrid2DBTableView1_project: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = DataSource1_User_Projects
        DataController.KeyFieldNames = 'name'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsView.ColumnAutoWidth = True
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        object cxGrid2DBTableView1_projectenabled: TcxGridDBColumn
          DataBinding.FieldName = 'enabled'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ImmediatePost = True
          Properties.NullStyle = nssUnchecked
          Width = 44
        end
        object cxGrid2DBTableView1_projectuser_id: TcxGridDBColumn
          DataBinding.FieldName = 'user_id'
          Visible = False
          Options.Editing = False
        end
        object cxGrid2DBTableView1_projectProject_ID: TcxGridDBColumn
          DataBinding.FieldName = 'Project_ID'
          Visible = False
          Options.Editing = False
        end
        object cxGrid2DBTableView1_projectname: TcxGridDBColumn
          DataBinding.FieldName = 'name'
          Options.Editing = False
          Options.Sorting = False
          Width = 380
        end
      end
      object cxGrid2DBTableView2_Geo: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = ds_ADOStoredProc_sp_User_GeoRegion_SEL
        DataController.KeyFieldNames = 'name'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsView.ColumnAutoWidth = True
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        object cxGrid2DBTableView2_Geoenabled: TcxGridDBColumn
          DataBinding.FieldName = 'enabled'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.NullStyle = nssUnchecked
          Width = 64
        end
        object cxGrid2DBTableView2_Geouser_id: TcxGridDBColumn
          DataBinding.FieldName = 'user_id'
          Visible = False
        end
        object cxGrid2DBTableView2_GeoGeoRegion_ID: TcxGridDBColumn
          DataBinding.FieldName = 'GeoRegion_ID'
          Visible = False
        end
        object cxGrid2DBTableView2_GeoColumn1: TcxGridDBColumn
          Caption = #1050#1086#1076
          DataBinding.FieldName = 'code'
          Options.Sorting = False
          Width = 65
        end
        object cxGrid2DBTableView2_Geoname: TcxGridDBColumn
          DataBinding.FieldName = 'name'
          Options.Editing = False
          Options.Sorting = False
          Width = 422
        end
      end
      object cxGrid2Level1: TcxGridLevel
        Caption = #1055#1088#1086#1077#1082#1090#1099
        GridView = cxGrid2DBTableView1_project
      end
      object cxGrid2Level2: TcxGridLevel
        Caption = #1056#1077#1075#1080#1086#1085#1099
        GridView = cxGrid2DBTableView2_Geo
      end
    end
    object cxDBVerticalGrid1: TcxDBVerticalGrid
      Left = 0
      Top = 0
      Width = 565
      Height = 233
      Align = alTop
      OptionsView.PaintStyle = psDelphi
      OptionsView.RowHeaderWidth = 189
      OptionsBehavior.RowSizing = True
      OptionsBehavior.AllowChangeRecord = False
      Navigator.Buttons.CustomButtons = <>
      Styles.Content = cxStyle1
      TabOrder = 1
      DataController.DataSource = DataSource1__users
      Version = 1
      object cxDBVerticalGrid1UID: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'ID'
        Visible = False
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1Name: TcxDBEditorRow
        Properties.Caption = #1051#1086#1075#1080#1085
        Properties.DataBinding.FieldName = 'Name'
        Properties.Options.Editing = False
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object row_Address: TcxDBEditorRow
        Properties.Caption = #1040#1076#1088#1077#1089
        Properties.DataBinding.FieldName = 'Address'
        ID = 2
        ParentID = -1
        Index = 2
        Version = 1
      end
      object row_email: TcxDBEditorRow
        Properties.Caption = 'Email'
        Properties.DataBinding.FieldName = 'email'
        ID = 3
        ParentID = -1
        Index = 3
        Version = 1
      end
      object row_Phone: TcxDBEditorRow
        Properties.Caption = #1058#1077#1083#1077#1092#1086#1085
        Properties.DataBinding.FieldName = 'Phone'
        ID = 4
        ParentID = -1
        Index = 4
        Version = 1
      end
      object row_Comment: TcxDBEditorRow
        Properties.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
        Properties.DataBinding.FieldName = 'comments'
        ID = 5
        ParentID = -1
        Index = 5
        Version = 1
      end
      object row_Lib: TcxDBEditorRow
        Height = 47
        Properties.Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1086#1074
        Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
        Properties.EditProperties.Items = <
          item
            Caption = #1056#1072#1079#1088#1077#1096#1077#1085#1086
            Value = 1
          end
          item
            Caption = #1047#1072#1087#1088#1077#1097#1077#1085#1086
            Value = 0
          end>
        Properties.DataBinding.FieldName = 'is_modify_lib'
        ID = 6
        ParentID = -1
        Index = 6
        Version = 1
      end
      object row_project: TcxDBEditorRow
        Height = 63
        Properties.Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1087#1088#1086#1077#1082#1090#1086#1074
        Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
        Properties.EditProperties.Items = <
          item
            Caption = #1056#1072#1079#1088#1077#1096#1077#1085#1086
            Value = 1
          end
          item
            Caption = #1056#1072#1079#1088#1077#1096#1077#1085#1086' '#1090#1086#1083#1100#1082#1086' '#1076#1083#1103' '#1089#1074#1086#1077#1075#1086' '#1088#1077#1075#1080#1086#1085#1072
            Value = 2
          end
          item
            Caption = #1047#1072#1087#1088#1077#1097#1077#1085#1086
            Value = 0
          end>
        Properties.DataBinding.FieldName = 'is_modify_project'
        ID = 7
        ParentID = -1
        Index = 7
        Version = 1
      end
    end
  end
  object DBGrid1: TDBGrid
    Left = 479
    Top = 159
    Width = 177
    Height = 137
    DataSource = DataSource1__users
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Visible = False
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 409
    Height = 758
    Align = alLeft
    TabOrder = 3
    LevelTabs.Style = 5
    RootLevelOptions.DetailTabsPosition = dtpTop
    ExplicitLeft = 8
    ExplicitTop = 8
    ExplicitHeight = 668
    object cxGrid1DBTableView1_Users: TcxGridDBTableView
      PopupMenu = PopupMenu1
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = DataSource1__users
      DataController.KeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGrid1DBTableView1_UsersUID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Visible = False
        Options.Sorting = False
      end
      object cxGrid1DBTableView1_UsersName: TcxGridDBColumn
        DataBinding.FieldName = 'Name'
        Options.Editing = False
        Options.Sorting = False
        Width = 110
      end
      object cxGrid1DBTableView1_Usersis_admin: TcxGridDBColumn
        DataBinding.FieldName = 'is_modify_project'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'id'
        Properties.ListColumns = <
          item
            FieldName = 'name'
          end>
        Properties.ListSource = DataSource1_project
        Options.Sorting = False
        Width = 97
      end
      object col_comments: TcxGridDBColumn
        DataBinding.FieldName = 'comments'
        Visible = False
      end
      object cxGrid1DBTableView1_UsersColumn1: TcxGridDBColumn
        Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
        DataBinding.FieldName = 'is_modify_lib'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'id'
        Properties.ListColumns = <
          item
            FieldName = 'name'
          end>
        Properties.ListSource = DataSource_lib
        Options.Sorting = False
        Width = 104
      end
      object cxGrid1DBTableView1_Usersemail: TcxGridDBColumn
        DataBinding.FieldName = 'email'
        Options.Sorting = False
        Width = 76
      end
      object cxGrid1DBTableView1_Usersaddress: TcxGridDBColumn
        DataBinding.FieldName = 'address'
        Options.Sorting = False
        Width = 48
      end
      object cxGrid1DBTableView1_Usersphone: TcxGridDBColumn
        DataBinding.FieldName = 'phone'
        Options.Sorting = False
        Width = 48
      end
    end
    object cxGrid1Level1: TcxGridLevel
      Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1080
      GridView = cxGrid1DBTableView1_Users
    end
  end
  object DataSource1__users: TDataSource
    DataSet = ADOTable1_Users
    Left = 568
    Top = 408
  end
  object ADOStoredProc1_User_Projects: TADOStoredProc
    Connection = dmApp_admin.ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'Security.sp_User_Projects_SEL'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 10
      end
      item
        Name = '@user_id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 7
      end>
    Prepared = True
    Left = 720
    Top = 352
  end
  object DataSource1_User_Projects: TDataSource
    DataSet = ADOStoredProc1_User_Projects
    Left = 720
    Top = 416
  end
  object cxLookAndFeelController1: TcxLookAndFeelController
    Kind = lfFlat
    Left = 728
    Top = 96
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = dmApp_admin.ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 776
    Top = 256
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 728
    Top = 40
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clWindow
    end
    object cxStyle2: TcxStyle
    end
  end
  object ActionList1: TActionList
    Left = 528
    Top = 40
    object acr_Refresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
    end
    object act_Save: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    end
    object act_user_Add: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    end
    object act_User_del: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
    end
    object act_Active_Users: TAction
      Caption = #1040#1082#1090#1080#1074#1085#1099#1077' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1080'...'
    end
    object FileExit1: TFileExit
      Category = 'File'
      Caption = #1042#1099#1093#1086#1076
      Hint = 'Exit|Quits the application'
      ImageIndex = 43
    end
  end
  object MainMenu1: TMainMenu
    Left = 528
    Top = 88
    object User1: TMenuItem
      Caption = #1054#1087#1077#1088#1072#1094#1080#1080
      object N1: TMenuItem
        Action = act_user_Add
      end
      object N2: TMenuItem
        Action = act_User_del
      end
      object N6: TMenuItem
        Caption = '-'
      end
      object N7: TMenuItem
        Action = act_Active_Users
      end
      object N8: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Action = FileExit1
      end
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 232
    Top = 216
    object N4: TMenuItem
      Action = act_user_Add
    end
    object N3: TMenuItem
      Action = act_User_del
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object acrRefresh1: TMenuItem
      Action = acr_Refresh
    end
  end
  object ADOTable1_Users: TADOTable
    Connection = dmApp_admin.ADOConnection1
    CursorType = ctStatic
    IndexFieldNames = 'name'
    TableName = 'Security.Users'
    Left = 568
    Top = 352
  end
  object ADOStoredProc_GeoRegion_SEL: TADOStoredProc
    Connection = dmApp_admin.ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    DataSource = DataSource1__users
    ProcedureName = 'Security.sp_User_GeoRegion_SEL'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Prepared = True
    Left = 720
    Top = 504
  end
  object ds_ADOStoredProc_sp_User_GeoRegion_SEL: TDataSource
    DataSet = ADOStoredProc_GeoRegion_SEL
    Left = 720
    Top = 568
  end
  object dxMemData_lib: TdxMemData
    Active = True
    Indexes = <>
    Persistent.Data = {
      5665728FC2F5285C8FFE3F020000000400000003000300696400320000000100
      05006E616D650001000000000109000000C7E0EFF0E5F9E5EDEE010100000001
      09000000D0E0E7F0E5F8E5EDEE}
    SortOptions = []
    Left = 552
    Top = 496
    object dxMemData_libid: TIntegerField
      FieldName = 'id'
    end
    object dxMemData_libname: TStringField
      FieldName = 'name'
      Size = 50
    end
  end
  object DataSource_lib: TDataSource
    DataSet = dxMemData_lib
    Left = 552
    Top = 544
  end
  object dxMemData_project: TdxMemData
    Active = True
    Indexes = <>
    Persistent.Data = {
      5665728FC2F5285C8FFE3F020000000400000003000300696400140000000100
      05006E616D650001000000000109000000C7E0EFF0E5F9E5EDEE010100000001
      09000000D0E0E7F0E5F8E5EDEE01020000000113000000D0E0E7F0E5F8E5EDEE
      20E4EBFF20F1E2EEE8F5}
    SortOptions = []
    Left = 448
    Top = 496
    object IntegerField1: TIntegerField
      FieldName = 'id'
    end
    object StringField1: TStringField
      FieldName = 'name'
      Size = 50
    end
  end
  object DataSource1_project: TDataSource
    DataSet = dxMemData_project
    Left = 448
    Top = 544
  end
  object ActionList2: TActionList
    Left = 1072
    Top = 496
    object act_Check_all: TAction
      Caption = #1054#1090#1084#1077#1090#1080#1090#1100' '#1074#1089#1077
    end
    object act_UnCheck_all: TAction
      Caption = #1057#1085#1103#1090#1100' '#1086#1090#1084#1077#1090#1082#1080
    end
  end
end
