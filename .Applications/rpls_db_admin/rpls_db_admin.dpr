program rpls_db_admin;

uses
  Vcl.Forms,
  f_Main_admin in 'f_Main_admin.pas' {frm_Main_admin},
  dm_App in 'dm_App.pas' {dmApp_admin: TDataModule},
  d_User_Add in 'd_User_Add.pas' {dlg_User_Add},
  Login_TLB in 'W:\common dll XE\DB_LOGIN\Login_TLB.pas',
  I_db_login in 'W:\common dll XE\DB_LOGIN\src\I_db_login.pas',
  d_ViewUsersActivity in 'src\d_ViewUsersActivity.pas' {dlg_ViewUsersActivity};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmApp_admin, dmApp_admin);
  Application.CreateForm(Tfrm_Main_admin, frm_Main_admin);
  Application.Run;
end.
