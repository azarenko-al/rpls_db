unit d_User_Add;

interface

uses
  System.SysUtils, System.Variants, System.Classes, 
  Vcl.Controls, Vcl.Forms, 
  cxStyles, 
  Vcl.StdCtrls, Data.Win.ADODB, cxVGrid,
  


  u_db, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxEdit,
  cxClasses, Data.DB, cxInplaceContainer ;

type
  Tdlg_User_Add = class(TForm)
    cxVerticalGrid1: TcxVerticalGrid;
    row_Login: TcxEditorRow;
    row_Pass: TcxEditorRow;
    ADOStoredProc1: TADOStoredProc;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    btn_Ok: TButton;
    Button2: TButton;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    row_Domain: TcxEditorRow;
    procedure btn_OkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    procedure Add;
    { Private declarations }
  public
    class function ExecDlg: boolean;
    { Public declarations }
  end;

//var
//  dlg_User_Add: Tdlg_User_Add;

implementation

{$R *.dfm}



// ---------------------------------------------------------------
class function Tdlg_User_Add.ExecDlg: boolean;
// ---------------------------------------------------------------
begin
  with Tdlg_User_Add.Create(Application) do
  begin

    Result:= ShowModal=mrOk;

    Free;
  end; 
end;

procedure Tdlg_User_Add.FormCreate(Sender: TObject);
begin
  Caption:='�������� ������������';
end;

// ---------------------------------------------------------------
procedure Tdlg_User_Add.Add;
// ---------------------------------------------------------------
var
  k: Integer;
begin
  k:=db_ExecStoredProc__(ADOStoredProc1, 'Security.sp_User_ADD',
      [
        FLD_DOMAIN,   row_Domain.Properties.Value,

        FLD_login,    row_Login.Properties.Value,
        FLD_PASSWORD, row_Pass.Properties.Value
   
      ] );
      
  if k=1 then
    ModalResult:=mrOk;
     
     
end;


procedure Tdlg_User_Add.btn_OkClick(Sender: TObject);
begin
  Add;
end;


end.

{

 db_ExecStoredProc__(ADOStoredProc1, 'Security.sp_User_Security_XREF_UPD',
  [
    FLD_user_id, ADOStoredProc1_User_Projects[FLD_user_id],
    FLD_project_id, ADOStoredProc1_User_Projects[FLD_project_id],
    FLD_ENABLED, ADOStoredProc1_User_Projects[FLD_ENABLED]   
   
  ] );
}
