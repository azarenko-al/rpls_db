unit d_ViewUsersActivity;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, StdCtrls, ExtCtrls,  Db, ADODB, Menus,
//  dxCntner, dxTL, dxDBCtrl, dxDBGrid, cxPropertiesStore,

  dm_App,

  u_func,
  u_db,
//  u_func_dx,
//  u_func_img,
  u_dlg, 

  rxPlacemnt, 
  
  cxGridLevel,
  cxGridDBTableView, 
  cxGrid, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, System.Actions, cxGridCustomTableView, cxGridTableView, cxClasses,
  cxGridCustomView;

  
type
  Tdlg_ViewUsersActivity = class(TForm)
    act_Renew: TAction;
    ds_Users: TDataSource;
    qry_Users: TADOQuery;
    act_Kill_Process: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Timer_refresh: TTimer;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1spid: TcxGridDBColumn;
    cxGrid1DBTableView1dbid: TcxGridDBColumn;
    cxGrid1DBTableView1uid: TcxGridDBColumn;
    cxGrid1DBTableView1cpu: TcxGridDBColumn;
    cxGrid1DBTableView1login_time: TcxGridDBColumn;
    cxGrid1DBTableView1last_batch: TcxGridDBColumn;
    cxGrid1DBTableView1status: TcxGridDBColumn;
    cxGrid1DBTableView1hostname: TcxGridDBColumn;
    cxGrid1DBTableView1program_name: TcxGridDBColumn;
    cxGrid1DBTableView1nt_username: TcxGridDBColumn;
    cxGrid1DBTableView1nt_domain: TcxGridDBColumn;
    cxGrid1DBTableView1net_address: TcxGridDBColumn;
    cxGrid1DBTableView1net_library: TcxGridDBColumn;
    cxGrid1DBTableView1loginame: TcxGridDBColumn;
    cxGrid1DBTableView1db_name: TcxGridDBColumn;
    Panel1: TPanel;
    ed_RefreshInterval: TEdit;
    Label1: TLabel;
    ActionList1: TActionList;
    FormStorage1: TFormStorage;
    procedure _Action(Sender: TObject);
    procedure FormCreate(Sender: TObject);
//    procedure dx_UsersCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
//              ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
//              ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
//              var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
//              var ADone: Boolean);
    procedure Timer_refreshTimer(Sender: TObject);
    procedure ed_RefreshIntervalExit(Sender: TObject);
//    procedure dx_UsersHotTrackNode(Sender: TObject; AHotTrackInfo: TdxTreeListHotTrackInfo;
//              var ACursor: TCursor);
  private
  //  FDefaultDB, FUserLogin: string;
  public
    class procedure ExecDlg;
  end;

//var
//  dlg_ViewUsersActivity: Tdlg_ViewUsersActivity;


//==============================================================================
implementation {$R *.DFM}
//==============================================================================
const
  ERR_GETTING_USERLIST =
    '������ ��������� ������ �������� �������������';
  ERR_KILL_PROCESS =
    '������ ��� ������� ��������� �������. �������� � ������������ ������������ ���� ��� ���������� ������ ��������';

  MSG_KILL_CONFIRM =
    '�� ������������� ������ ��������� ���������� �������? (���� ������� ��������� �����-�� ��������, ��������� ����� ���� ������!)';

//  SQL_SEL_ACTIVE_USERS =
//    'SELECT  p.spid, p.dbid, p.uid, p.cpu, p.login_time, p.last_batch, p.status, '+
//    '        p.hostname, p.program_name, p.nt_username, p.nt_domain,             '+
//    '        p.net_address, p.net_library, p.loginame, d.name AS db_name         '+
//    ' FROM   master..sysprocesses p LEFT OUTER JOIN                              '+
//    '        master..sysdatabases d ON p.dbid = d.dbid                           '+
//    ' WHERE  (p.hostname <> N'''') AND (d.name = N''%s'')                        ';
//


//------------------------------------------------------------------------------
class procedure Tdlg_ViewUsersActivity.ExecDlg;
//------------------------------------------------------------------------------
begin
  with Tdlg_ViewUsersActivity.Create(Application) do
  try
    try
      db_OpenQuery(qry_Users, 'SELECT * from [Security].view_Active_sysprocesses');
      
    except
      on e: Exception do begin
        ErrorDlg(ERR_GETTING_USERLIST+CRLF+'����� ������: '+E.Message);
        Close;
      end;
    end;

    SHowModal;
    
  //  Result:= (SHowModal=mrOk);
  finally
    Free;
  end;
end;


//------------------------------------------------------------------------------
procedure Tdlg_ViewUsersActivity.FormCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  Caption:= '�������� ������������';
  //SetActionName ('�������� ������������ � �� �������������');
//  btn_Cancel__.Caption:= '�������';

  //-------------------------------------

  db_SetComponentADOConnection(Self, dmApp_admin.ADOConnection1);

  //dmMain_RFP.LoadConnectRecFromReg_();

 // FDefaultDB:= dmMain_RFP.LoginRec.DefaultDB;
//  FUserLogin:= dmMain_RFP.LoginRec.User;

  //-------------------------------------

  cxGrid1.Align:= alClient;

//  col_spid.Visible:= dmMain_RFP.DEBUG;
//  btn_Ok__.Visible:= false;

//  col_login.FieldName:= 'loginame';
//  col_login_time.FieldName:= 'login_time';
//  col_last_query_time.FieldName:= 'last_batch';
//  col_DB.FieldName:= 'db_name';
//  col_PC.FieldName:= 'hostname';
//  col_spid.FieldName:= 'spid';
end;


//------------------------------------------------------------------------------
procedure Tdlg_ViewUsersActivity._Action(Sender: TObject);
//------------------------------------------------------------------------------
var
  ispid: Integer;
  sProgName: string;
begin
  //-------------------------------------
  if Sender=act_Renew then
  //-------------------------------------
  begin
//    CursorSql;

//    db_SaveDatasetPosition([qry_Users]);
//    qry_Users.DisableControls;
    qry_Users.Requery;
//    qry_Users.EnableControls;
//    db_RestoreDatasetPosition([qry_Users]);

 //   CursorDefault;
  end else


  //-------------------------------------
  if Sender=act_Kill_Process then
  //-------------------------------------
  begin
    ispid:= qry_Users['spid'];

    sProgName:= qry_Users['Program_Name'];

    if Eq(sProgName, 'RPLS DB ADMIN') then
    begin
      MsgDlg('���������� ��������� ���� �������');
      exit;
    end;

    if ConfirmDlg(MSG_KILL_CONFIRM) then
    begin
      dmApp_admin.ADOConnection1.Execute('KILL '+AsString(ispid));
    
//      if not gl_Db.ExecCommandSimple('KILL '+AsString(ispid)) then
  //       ErrorDlg(ERR_KILL_PROCESS);

      act_Renew.Execute;
    end;
  end;
end;

//
////------------------------------------------------------------------------------
//procedure Tdlg_ViewUsersActivity.dx_UsersCustomDrawCell(Sender: TObject;
//          ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
//          AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
//          var AText: String; var AColor: TColor; AFont: TFont;
//          var AAlignment: TAlignment; var ADone: Boolean);
////------------------------------------------------------------------------------
//begin
//(*if Eq(AsString(aNode.Values[col_login.Index]), FUserLogin) then
//  begin
//    img_DrawColorRect(ACanvas, ARect, clWindow, clYellow, 2);
//    ADone:=True;
//    AFont.Style:= AFont.Style + [fsUnderline];
//  end; *)
//end;


//------------------------------------------------------------------------------
procedure Tdlg_ViewUsersActivity.Timer_refreshTimer(Sender: TObject);
//------------------------------------------------------------------------------
begin
  act_Renew.Execute;
end;


//------------------------------------------------------------------------------
procedure Tdlg_ViewUsersActivity.ed_RefreshIntervalExit(Sender: TObject);
//------------------------------------------------------------------------------
var
  iRefreshInterval: double;
begin
  iRefreshInterval:= AsFloat(ed_RefreshInterval.Text);

//  if iRefreshInterval < 0.1 then
//     ed_RefreshInterval.Text:= '0.1';

  if iRefreshInterval > 120 then
     ed_RefreshInterval.Text:= '120';

  Timer_refresh.Interval:= Trunc(AsFloat(ed_RefreshInterval.Text)*1000);
end;
//
//
////------------------------------------------------------------------------------
//procedure Tdlg_ViewUsersActivity.dx_UsersHotTrackNode(Sender: TObject;
//          AHotTrackInfo: TdxTreeListHotTrackInfo; var ACursor: TCursor);
////------------------------------------------------------------------------------
//begin
////  dx_ShowHintForDbGridColumns(dx_Users, AHotTrackInfo, []);
//end;


end.
