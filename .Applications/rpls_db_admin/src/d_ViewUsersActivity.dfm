object dlg_ViewUsersActivity: Tdlg_ViewUsersActivity
  Left = 858
  Top = 290
  Caption = 'dlg_ViewUsersActivity'
  ClientHeight = 584
  ClientWidth = 1187
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 433
    Height = 543
    Align = alLeft
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      PopupMenu = PopupMenu1
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ds_Users
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1spid: TcxGridDBColumn
        DataBinding.FieldName = 'spid'
      end
      object cxGrid1DBTableView1dbid: TcxGridDBColumn
        DataBinding.FieldName = 'dbid'
      end
      object cxGrid1DBTableView1uid: TcxGridDBColumn
        DataBinding.FieldName = 'uid'
      end
      object cxGrid1DBTableView1cpu: TcxGridDBColumn
        DataBinding.FieldName = 'cpu'
      end
      object cxGrid1DBTableView1login_time: TcxGridDBColumn
        DataBinding.FieldName = 'login_time'
      end
      object cxGrid1DBTableView1last_batch: TcxGridDBColumn
        DataBinding.FieldName = 'last_batch'
      end
      object cxGrid1DBTableView1status: TcxGridDBColumn
        DataBinding.FieldName = 'status'
        Width = 75
      end
      object cxGrid1DBTableView1hostname: TcxGridDBColumn
        DataBinding.FieldName = 'hostname'
        Width = 47
      end
      object cxGrid1DBTableView1program_name: TcxGridDBColumn
        DataBinding.FieldName = 'program_name'
        Width = 205
      end
      object cxGrid1DBTableView1nt_username: TcxGridDBColumn
        DataBinding.FieldName = 'nt_username'
        Width = 20
      end
      object cxGrid1DBTableView1nt_domain: TcxGridDBColumn
        DataBinding.FieldName = 'nt_domain'
        Width = 79
      end
      object cxGrid1DBTableView1net_address: TcxGridDBColumn
        DataBinding.FieldName = 'net_address'
      end
      object cxGrid1DBTableView1net_library: TcxGridDBColumn
        DataBinding.FieldName = 'net_library'
      end
      object cxGrid1DBTableView1loginame: TcxGridDBColumn
        DataBinding.FieldName = 'loginame'
        Width = 78
      end
      object cxGrid1DBTableView1db_name: TcxGridDBColumn
        DataBinding.FieldName = 'db_name'
        Width = 57
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 543
    Width = 1187
    Height = 41
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 1
    object Label1: TLabel
      Left = 19
      Top = 14
      Width = 140
      Height = 13
      Caption = #1048#1085#1090#1077#1088#1074#1072#1083' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1103' ('#1089#1077#1082')'
    end
    object ed_RefreshInterval: TEdit
      Left = 165
      Top = 11
      Width = 51
      Height = 21
      TabOrder = 0
      Text = '30'
      OnExit = ed_RefreshIntervalExit
    end
  end
  object ActionList1: TActionList
    Left = 801
    Top = 204
    object act_Renew: TAction
      Category = 'Main'
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      OnExecute = _Action
    end
    object act_Kill_Process: TAction
      Category = 'Main'
      Caption = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1087#1088#1086#1094#1077#1089#1089
      OnExecute = _Action
    end
  end
  object ds_Users: TDataSource
    DataSet = qry_Users
    Left = 608
    Top = 224
  end
  object qry_Users: TADOQuery
    Connection = dmApp_admin.ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * from [Security].view_Active_sysprocesses')
    Left = 608
    Top = 168
  end
  object PopupMenu1: TPopupMenu
    Left = 832
    Top = 336
    object N1: TMenuItem
      Action = act_Kill_Process
    end
  end
  object Timer_refresh: TTimer
    Interval = 60000
    OnTimer = Timer_refreshTimer
    Left = 880
    Top = 208
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredValues = <>
    Left = 608
    Top = 56
  end
end
