object dlg_User_Add: Tdlg_User_Add
  Left = 1158
  Top = 395
  BorderWidth = 5
  Caption = 'dlg_User_Add'
  ClientHeight = 191
  ClientWidth = 422
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    422
    191)
  PixelsPerInch = 96
  TextHeight = 13
  object cxVerticalGrid1: TcxVerticalGrid
    Left = 0
    Top = 0
    Width = 422
    Height = 133
    BorderStyle = cxcbsNone
    Align = alTop
    OptionsView.PaintStyle = psDelphi
    OptionsView.RowHeaderMinWidth = 34
    OptionsView.RowHeaderWidth = 107
    Styles.Content = cxStyle1
    TabOrder = 0
    ExplicitWidth = 477
    Version = 1
    object row_Domain: TcxEditorRow
      Properties.Caption = #1044#1086#1084#1077#1085
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object row_Login: TcxEditorRow
      Properties.Caption = 'Login'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = ''
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object row_Pass: TcxEditorRow
      Options.CanResized = False
      Properties.Caption = #1055#1072#1088#1086#1083#1100
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = ''
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxVerticalGrid1CategoryRow1: TcxCategoryRow
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
  end
  object btn_Ok: TButton
    Left = 258
    Top = 158
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    Default = True
    TabOrder = 1
    OnClick = btn_OkClick
    ExplicitLeft = 243
    ExplicitTop = 164
  end
  object Button2: TButton
    Left = 339
    Top = 158
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 2
    ExplicitLeft = 324
    ExplicitTop = 164
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = dmApp_admin.ADOConnection1
    Parameters = <>
    Left = 20
    Top = 87
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 124
    Top = 87
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clHighlightText
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
  end
end
