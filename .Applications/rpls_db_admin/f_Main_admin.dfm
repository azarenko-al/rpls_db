object frm_Main_admin: Tfrm_Main_admin
  Left = 1308
  Top = 181
  Caption = 'frm_Main_admin'
  ClientHeight = 666
  ClientWidth = 1225
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxSplitter1: TcxSplitter
    Left = 844
    Top = 0
    Width = 8
    Height = 647
    HotZoneClassName = 'TcxXPTaskBarStyle'
    AlignSplitter = salRight
    Control = Panel1
    ExplicitHeight = 689
  end
  object Panel1: TPanel
    Left = 852
    Top = 0
    Width = 373
    Height = 647
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 1
    OnClick = Panel1Click
    ExplicitHeight = 689
    object cxGrid2: TcxGrid
      Left = 0
      Top = 343
      Width = 373
      Height = 304
      Align = alBottom
      TabOrder = 0
      LevelTabs.Style = 3
      RootLevelOptions.DetailTabsPosition = dtpTop
      ExplicitLeft = 6
      ExplicitTop = 362
      object cxGrid2DBTableView1_project: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = DataSource1_User_Projects
        DataController.KeyFieldNames = 'name'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = #1047#1072#1087#1080#1089#1077#1081': 0'
            Kind = skCount
            Column = cxGrid2DBTableView1_projectname
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsView.ColumnAutoWidth = True
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        object cxGrid2DBTableView1_projectenabled: TcxGridDBColumn
          DataBinding.FieldName = 'enabled'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ImmediatePost = True
          Properties.NullStyle = nssUnchecked
          Width = 46
        end
        object cxGrid2DBTableView1_projectuser_id: TcxGridDBColumn
          DataBinding.FieldName = 'user_id'
          Visible = False
          Options.Editing = False
        end
        object cxGrid2DBTableView1_projectProject_ID: TcxGridDBColumn
          DataBinding.FieldName = 'Project_ID'
          Visible = False
          Options.Editing = False
        end
        object cxGrid2DBTableView1_projectname: TcxGridDBColumn
          DataBinding.FieldName = 'name'
          Options.Editing = False
          Options.Sorting = False
          Width = 313
        end
      end
      object cxGrid2DBTableView2_Geo: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = ds_ADOStoredProc_sp_User_GeoRegion_SEL
        DataController.KeyFieldNames = 'name'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = #1047#1072#1087#1080#1089#1077#1081': 0'
            Kind = skCount
            Column = cxGrid2DBTableView2_Geoname
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsView.ColumnAutoWidth = True
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        object cxGrid2DBTableView2_Geoenabled: TcxGridDBColumn
          DataBinding.FieldName = 'enabled'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.NullStyle = nssUnchecked
          Width = 64
        end
        object cxGrid2DBTableView2_Geouser_id: TcxGridDBColumn
          DataBinding.FieldName = 'user_id'
          Visible = False
        end
        object cxGrid2DBTableView2_GeoGeoRegion_ID: TcxGridDBColumn
          DataBinding.FieldName = 'GeoRegion_ID'
          Visible = False
        end
        object cxGrid2DBTableView2_Geoname: TcxGridDBColumn
          DataBinding.FieldName = 'name'
          Options.Editing = False
          Options.Sorting = False
          Width = 422
        end
      end
      object cxGrid2Level1: TcxGridLevel
        Caption = #1055#1088#1086#1077#1082#1090#1099
        GridView = cxGrid2DBTableView1_project
      end
      object cxGrid2Level2: TcxGridLevel
        Caption = #1056#1077#1075#1080#1086#1085#1099
        GridView = cxGrid2DBTableView2_Geo
      end
    end
    object cxDBVerticalGrid1: TcxDBVerticalGrid
      Left = 0
      Top = 0
      Width = 373
      Height = 233
      Align = alTop
      OptionsView.PaintStyle = psDelphi
      OptionsView.RowHeaderWidth = 189
      OptionsBehavior.RowSizing = True
      OptionsBehavior.AllowChangeRecord = False
      Navigator.Buttons.CustomButtons = <>
      Styles.Content = cxStyle1
      TabOrder = 1
      DataController.DataSource = DataSource1__users
      Version = 1
      object cxDBVerticalGrid1UID: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'ID'
        Visible = False
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1Name: TcxDBEditorRow
        Properties.Caption = #1051#1086#1075#1080#1085
        Properties.DataBinding.FieldName = 'Name'
        Properties.Options.Editing = False
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object row_Address: TcxDBEditorRow
        Properties.Caption = #1040#1076#1088#1077#1089
        Properties.DataBinding.FieldName = 'Address'
        ID = 2
        ParentID = -1
        Index = 2
        Version = 1
      end
      object row_email: TcxDBEditorRow
        Properties.Caption = 'Email'
        Properties.DataBinding.FieldName = 'email'
        ID = 3
        ParentID = -1
        Index = 3
        Version = 1
      end
      object row_Phone: TcxDBEditorRow
        Properties.Caption = #1058#1077#1083#1077#1092#1086#1085
        Properties.DataBinding.FieldName = 'Phone'
        ID = 4
        ParentID = -1
        Index = 4
        Version = 1
      end
      object row_Comment: TcxDBEditorRow
        Properties.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
        Properties.DataBinding.FieldName = 'comments'
        ID = 5
        ParentID = -1
        Index = 5
        Version = 1
      end
      object row_Lib: TcxDBEditorRow
        Height = 47
        Properties.Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1086#1074
        Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
        Properties.EditProperties.Items = <
          item
            Caption = #1056#1072#1079#1088#1077#1096#1077#1085#1086
            Value = 1
          end
          item
            Caption = #1047#1072#1087#1088#1077#1097#1077#1085#1086
            Value = 0
          end>
        Properties.DataBinding.FieldName = 'is_modify_lib'
        ID = 6
        ParentID = -1
        Index = 6
        Version = 1
      end
      object row_project: TcxDBEditorRow
        Height = 63
        Properties.Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1087#1088#1086#1077#1082#1090#1086#1074
        Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
        Properties.EditProperties.Items = <
          item
            Caption = #1056#1072#1079#1088#1077#1096#1077#1085#1086
            Value = 1
          end
          item
            Caption = #1056#1072#1079#1088#1077#1096#1077#1085#1086' '#1090#1086#1083#1100#1082#1086' '#1076#1083#1103' '#1089#1074#1086#1077#1075#1086' '#1088#1077#1075#1080#1086#1085#1072
            Value = 2
          end
          item
            Caption = #1047#1072#1087#1088#1077#1097#1077#1085#1086
            Value = 0
          end>
        Properties.DataBinding.FieldName = 'is_modify_project'
        ID = 7
        ParentID = -1
        Index = 7
        Version = 1
      end
    end
  end
  object DBGrid1: TDBGrid
    Left = 439
    Top = 143
    Width = 177
    Height = 137
    DataSource = DataSource1__users
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Visible = False
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 409
    Height = 647
    Align = alLeft
    TabOrder = 3
    LevelTabs.Style = 5
    RootLevelOptions.DetailTabsPosition = dtpTop
    ExplicitHeight = 689
    object cxGrid1DBTableView1_Users: TcxGridDBTableView
      PopupMenu = PopupMenu1
      Navigator.Buttons.CustomButtons = <>
      OnFocusedRecordChanged = cxGrid1DBTableView1_UsersFocusedRecordChanged
      DataController.DataSource = DataSource1__users
      DataController.KeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGrid1DBTableView1_UsersUID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Visible = False
        Options.Sorting = False
      end
      object cxGrid1DBTableView1_UsersName: TcxGridDBColumn
        DataBinding.FieldName = 'Name'
        Options.Editing = False
        Options.Sorting = False
        Width = 110
      end
      object cxGrid1DBTableView1_Usersis_admin: TcxGridDBColumn
        DataBinding.FieldName = 'is_modify_project'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'id'
        Properties.ListColumns = <
          item
            FieldName = 'name'
          end>
        Options.Sorting = False
        Width = 97
      end
      object col_comments: TcxGridDBColumn
        DataBinding.FieldName = 'comments'
        Visible = False
      end
      object cxGrid1DBTableView1_UsersColumn1: TcxGridDBColumn
        Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
        DataBinding.FieldName = 'is_modify_lib'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'id'
        Properties.ListColumns = <
          item
            FieldName = 'name'
          end>
        Options.Sorting = False
        Width = 104
      end
      object cxGrid1DBTableView1_Usersemail: TcxGridDBColumn
        DataBinding.FieldName = 'email'
        Options.Sorting = False
        Width = 76
      end
      object cxGrid1DBTableView1_Usersaddress: TcxGridDBColumn
        DataBinding.FieldName = 'address'
        Options.Sorting = False
        Width = 48
      end
      object cxGrid1DBTableView1_Usersphone: TcxGridDBColumn
        DataBinding.FieldName = 'phone'
        Options.Sorting = False
        Width = 48
      end
    end
    object cxGrid1Level1: TcxGridLevel
      Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1080
      GridView = cxGrid1DBTableView1_Users
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 647
    Width = 1225
    Height = 19
    Panels = <>
    SimplePanel = True
    Visible = False
    ExplicitLeft = 544
    ExplicitTop = 600
    ExplicitWidth = 0
  end
  object DataSource1__users: TDataSource
    DataSet = ADOTable1_Users
    Left = 496
    Top = 424
  end
  object ADOStoredProc1_User_Projects: TADOStoredProc
    Connection = dmApp_admin.ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    AfterPost = ADOStoredProc1_User_ProjectsAfterPost
    ProcedureName = 'Security.sp_User_Projects_SEL'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 10
      end
      item
        Name = '@user_id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 7
      end>
    Prepared = True
    Left = 680
    Top = 344
  end
  object DataSource1_User_Projects: TDataSource
    DataSet = ADOStoredProc1_User_Projects
    Left = 680
    Top = 392
  end
  object cxLookAndFeelController1: TcxLookAndFeelController
    Kind = lfFlat
    Left = 672
    Top = 80
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = dmApp_admin.ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 680
    Top = 232
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 672
    Top = 32
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clWindow
    end
    object cxStyle2: TcxStyle
    end
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 480
    Top = 8
    object acr_Refresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      OnExecute = acr_RefreshExecute
    end
    object act_user_Add: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      OnExecute = act_user_AddExecute
    end
    object act_User_del: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = act_User_delExecute
    end
    object act_Active_Users: TAction
      Caption = #1040#1082#1090#1080#1074#1085#1099#1077' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1080'...'
      OnExecute = act_Active_UsersExecute
    end
    object FileExit1: TFileExit
      Category = 'File'
      Caption = #1042#1099#1093#1086#1076
      Hint = 'Exit|Quits the application'
      ImageIndex = 43
    end
  end
  object MainMenu1: TMainMenu
    Left = 480
    Top = 56
    object User1: TMenuItem
      Caption = #1054#1087#1077#1088#1072#1094#1080#1080
      object N1: TMenuItem
        Action = act_user_Add
      end
      object N2: TMenuItem
        Action = act_User_del
      end
      object N6: TMenuItem
        Caption = '-'
      end
      object N7: TMenuItem
        Action = act_Active_Users
      end
      object N8: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Action = FileExit1
      end
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 232
    Top = 216
    object N4: TMenuItem
      Action = act_user_Add
    end
    object N3: TMenuItem
      Action = act_User_del
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object acrRefresh1: TMenuItem
      Action = acr_Refresh
    end
  end
  object ADOTable1_Users: TADOTable
    Connection = dmApp_admin.ADOConnection1
    CursorType = ctStatic
    IndexFieldNames = 'name'
    TableName = 'Security.Users'
    Left = 496
    Top = 368
  end
  object ADOStoredProc_GeoRegion_SEL: TADOStoredProc
    Connection = dmApp_admin.ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    AfterPost = ADOStoredProc_GeoRegion_SELAfterPost
    DataSource = DataSource1__users
    ProcedureName = 'Security.sp_User_GeoRegion_SEL'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Prepared = True
    Left = 680
    Top = 472
  end
  object ds_ADOStoredProc_sp_User_GeoRegion_SEL: TDataSource
    DataSet = ADOStoredProc_GeoRegion_SEL
    Left = 680
    Top = 520
  end
  object ActionList2: TActionList
    Left = 496
    Top = 512
    object act_Check_all: TAction
      Caption = #1054#1090#1084#1077#1090#1080#1090#1100' '#1074#1089#1077
    end
    object act_UnCheck_all: TAction
      Caption = #1057#1085#1103#1090#1100' '#1086#1090#1084#1077#1090#1082#1080
    end
  end
end
