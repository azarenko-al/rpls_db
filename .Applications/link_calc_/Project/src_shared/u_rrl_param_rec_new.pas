unit u_rrl_param_rec_new;

interface

  {.$DEFINE open_file}



uses Variants, Classes, Dialogs, XMLIntf, SysUtils,

//  u_hash,

  I_rel_Matrix1,

  u_xml_document,
  u_files,
  u_geo,
  u_func
  ;

const

  EXE_RPLS_RRL = 'rpls_rrl.exe';

const
  DEF_Calc_Method_ITU_R       = 0;
  DEF_Calc_Method_GOST        = 1;
  DEF_Calc_Method_NIIR_1998   = 2;
  DEF_Calc_Method_E_BAND_ITU  = 3;
  DEF_Calc_Method_UKV         = 4;



type
  // ---------------------------------------------------------------
  TrrlCalcParam = class
  // ---------------------------------------------------------------
    IsShowWindow : Boolean;

    Calc_method : Integer; // 0 "����, ITU-R � 16 ���� ��" (�� ���������)
				                   // 1 "���� � 53363 - 2009"
                           // 0 "���� 1998

    Link_Center_Point: TBLPoint;



    //-------------------------------------------------------------------
    TTX: record
    //-------------------------------------------------------------------
      Site1_Power_dBm     : double;  //'site1-�������� dBm'
      Site1_DIAMETER      : double;  //'site1-�������');
      Site1_GAIN          : double;  //'site1-��������');
      Site1_VERT_WIDTH    : Double;  //'site1-������ ���');
      Site1_LOSS          : Double;  // + dLinkEndLoss, 'site1-������ � ���');
      Site1_HEIGHT        : Double;  //'site1-������');

      Site2_Threshold_dBM : Double;  //������  dBM
      Site2_DIAMETER      : double;  // 'site1-�������');
      Site2_GAIN          : double;  // 'site1-��������');
      Site2_VERT_WIDTH    : Double;  // 'site1-������ ���');
      Site2_LOSS          : Double;  //  + dLinkEndLoss, 'site1-������ � ���');
      Site2_HEIGHT        : Double;  // 'site1-������');


      Dop_Site1, Dop_Site2:
      record
        DIAMETER  : double;  // 'site1-�������');
        GAIN      : double; //  'site1-��������');
        VERT_WIDTH: Double; //  'site1-������ ���');
        LOSS1      : Double; //   + dLinkEndLoss, 'site1-������ � ���');
        HEIGHT    : Double; //  'site1-������');
      end;


      SIGNATURE_WIDTH_Mhz: double; //  '������ ���������, ���');
      SIGNATURE_HEIGHT_dB : double;   // '������ ���������, ��');
      MODULATION_Level_COUNT : double;   // '����� ������� ���������');


//      EQUALISER_PROFIT : double; // '�������, �������� ������������, ��');

      KRATNOST_BY_SPACE: integer; //'��������� ���������� �� ������������, 1...2'
      KRATNOST_BY_FREQ : integer; //'��������� ���������� �� �������, 1..n+1 //1...8'

      Freq_Spacing_MHz  : Double;  //'��������� ������, ���');

(*      Site1_FrontToBackRatio: Double;  //'ant1 - ����.���.�������� (������/�����) ���-�������, ��')
      Site2_FrontToBackRatio: Double; //'ant2 - ����.���.�������� (������/�����) ���-�������, ��'

      Dop_Site1_FrontToBackRatio: Double; //'ant1 - ����.���.�������� (������/�����) ���-�������, ��'
      Dop_Site2_FrontToBackRatio: Double;  //'ant2 - ����.���.�������� (������/�����) ���-�������, ��')
*)

      Site1_KNG: Double; //'��� ������������ (%)1'
      Site2_KNG: Double;  //'��� ������������ (%)2'


      BitRate_Mbps        : double;  // '�������� ��������, ��/�');
      Freq_GHz: Double; //  'P������ �������, ���');

      POLARIZATION: integer;//'��� �����������, 0-��������������,1-������������,2-�����');
      POLARIZATION_type: (ptV_,ptH_,ptX_);//'��� �����������, 0-��������������,1-������������,2-�����');

      Dop_Antenna: record //����� ���������
         POLARIZATION: Integer;//46-'��� �����������, 0-��������������,1-������������,2-�����');
//        UseIt : Boolean;


      end;


      AntennaOffsetHor_m: Double; //�������������� ����� //���������� ����� ��������� �� ������

      // -------------------------
      GOST_53363 : record
        Modulation_Type  : Integer; // ��� ���������, 0-PSK (QPSK), 1-QAM (TCM)

        RX_count_on_combined_diversity	: Integer;   //44 ���������� ��� ��� ����.����������
      end;


      ATPC_profit_dB : double;// <param id="45" value="10" comment="������� �� ATPC, ��"/>
      Threshold_degradation_dB : Double; //Threshold_degradation_dB,  '���������� ����������������, ��');

      XPIC_42_use_2_stvol: integer; //'48.������� ���.2 ���.� ���������.����. [0-���,1-��]',
      XPIC_49_coef: double;
      XPIC_50: double;

{
���� � ���, ��� �� � ����� �������� �������� � xml-���� ������ ���� ����������, � ���� ���:
'48.������� ���.2 ���.� ���������.����. [0-���,1-��]',
'49.����.�����.���.��� ��.�����.[��]',
'50.�����.�� �������.����� [��]', );
}

      //    '49.����.�����.���.��� ��.�����.[��]',
      //    '50.�����.�� �������.����� [��]', );


    end;


    //-------------------------------------------------------------------
    RRV: record //������� ��������������� ��������� (���)
    //-------------------------------------------------------------------

      gradient_diel_1      : double;   // '������� �������� ��������� ����.�������������,10^-8 1/�' );
      gradient_deviation_2 : double;   // '����������� ���������� ���������,10^-8 1/�' );

      terrain_type_14 : Double;   //'3. ��� ���������.(1...3,0->����.�����=����.���.)' );
      underlying_terrain_type_3: Double; //4 ��� ������������ �������.(1...3,0->����.�����=����.���.)' );

      steam_wet_4     : double;   // 5 '���������� ��������� �������� ����, �/���.�');
      Q_factor_5      : double;   // 8 'Q-������ ������ �����������');
      climate_factor_6: double;   // '������������� ������ K��, 10^-6');
      factor_B_7      : double;   // '�������� ������������ b ��� ������� ������');
      factor_C_8      : double;   // '�������� ������������ c ��� ������� ������');
      FACTOR_D_9      : double;   // '�������� ������������ d ��� ������� ������');

      //13.������������� ����� � ������� 0.01% �������, ��/���
      RAIN_intensity_10 : double;   // '������������� ����� � ������� 0.01% �������, ��/���');
      REFRACTION_11     : double;   //'�����. ��������� (��� ��.���������=0, ����� �.�.=0)');

      Air_temperature_12	   : double;   //����������� ������� [��.�]" (�� ��������� 15)
      Atmosphere_pressure_13 : double;   //����������� �������� [����]" (�� ��������� 1013)


      //-------------------------------
      // ����
      //-------------------------------
      RRV_GOST: record
      // -------------------------
   //     Link_Center_lat_15 : double; //  10.���������� ������� ��������� (��-����), ��
    //    Link_Center_lon_16 : double; //  11.���������� ������� ��������� (���-����), ��

        RAIN_intensity_19 : double;   // 9.����������� ������������� �����
      end;

      // -------------------------
      RRV_NIIR1998 : record
      // -------------------------
        Rain_region_number_17 : Integer;//10 ����� ������ �� ����� ���� (���.8.11) ��� ������������� ������
        Qd_region_number_18   : Integer;//11 ����� ������ �� ����� ���� (���.8.15) ��� ��������� Q�
        Water_percent_20      : Double; // 9. ���� ������ ����������� �� �������-��, %
      end;

      // -------------------------
      RRV_E_band_ITU : record  //3.�-�������� (��.ITU-R)
      // -------------------------
        Param_1 : Double;  //	1.������� �������� ��������� ����. �������������, 10^-8, 1/�	1	�� �����. (-9)
        Param_2 : Double;  //	2.����������� ���������� ���������,10^-8, 1/�	2	�� �����. 7
        Param_3 : Double;  //	3.���������� ��������� �������� ����, �/���.�	4	�� �����. 7.5
        Param_4 : double;  //	4.����������� �������, ��.�	12	�� �����. 15
        Param_5 : Double;  //	5.����������� ��������, ����	13	�� �����. 1013
        Param_6 : double;  //	6.������������� ����� � ������� 0.01% �������, ��/���	10	�� �����.20
      end;

      // -------------------------
      RRV_E_band_GOST : record //4.�-�������� (��.����)
      // -------------------------
        Param_1 : Double; //	1. ������� �������� ��������� ����. �������������,10^-8, 1/�	1	�� �����. (-9)
        Param_2 : Double; //	2.����������� ���������� ���������,10^-8, 1/�	2	�� �����. 7
        Param_3 : Double; //	3.���������� ��������� �������� ����, �/���.�	4	�� �����. 7.5
        Param_4 : double; //	4.����������� �������, ��.�	12	�� �����. 15
        Param_5 : double; //	5.����������� ��������, ����	13	�� �����. 1013
        Param_6 : double; //	6.����������� ������������� �����	19	�� �����. 1
    //    Param_7 : double; //*	7.���������� ������� ��������� (������), ��	15	�� �����.60
     //   Param_8 : Double; //*	8.���������� ������� ��������� (�������), ��	16	�� �����.30
      end;

      // -------------------------
      RRV_E_band_NIIR : record //5.�-�������� (��.����)
      // -------------------------
        Param_1 : Double; // 1.������� �������� ��������� ����. �������������,10^-8, 1/�	1	�� �����. (-9)
        Param_2 : Double; // 2.����������� ���������� ���������,10^-8, 1/�	2	�� �����. 7
        Param_3 : Double; // 3.���������� ��������� �������� ����, �/���.�	4	�� �����. 7.5
        Param_4 : double; // 4.����������� �������, ��.�	12	�� �����. 20
        Param_5 : double; // 5.����������� ��������, ����	13	�� �����. 1013
        Param_6 : double; // 6.����� ������ �� ����� ������������� ������ (1...22)	17	�� �����.1
        Param_7 : double; // 7.����� ������ (������) �� ����� Qd (1�22)	18	�� �����.1
      end;

      // -------------------------
      RRV_UKV : record  //    6.���-�������� (16 �����)
      // -------------------------
        Param_1 : Double; // 1.������� �������� ��������� ����. �������������, 10^-8, 1/�	1	�� �����. (-8)
        Param_2 : Integer;// 2.��� ������������ ����������� (1-������������.(����.), 2-�������������., 3-����.)	3	� xml ���������� ��-��: 1,2,3 (�� �����.2)
        Param_3 : Double; // 3.��������.����������� c (0-�����.����.�� ���)	8	�� �����.0
        Param_4 : double; // 4.��� ������ ������� (��� �>0) [dB] (0-���������)	12	�� �����.0
      end;

    end;

    //-------------------------------------------------------------------
    DLT: record
    //-------------------------------------------------------------------
      Profile_Step_KM_1 : Double; //  '��� ��������� �������, �� ( 0 - ��������� ����)');

      // default values
      precision_V_diffraction_2  : Double;//   '�������� ������ ������� V����(g)=V���.�, ��');
      precision_g_V_diffraction_3: Double;// '�������� ������ ������� g(V����)=g(V���.�), 10^-8');
      max_gradient_for_subrefr_4 : Double;//  '������������ �������� ��� ������� ������������, 10^-8');

    end;

    // ---------------------------------------------------------------
    Link_Graph: record
    // ---------------------------------------------------------------
      NFunc1: Double;
      NFunc2: Double;
      ArgGroup: Double;
      NArg: Double;
      Arg_min: Double;
      Arg_max: Double;
      Arg_step: double; //08.09.2008 ��������� ��� ���������� ��������

    end;


    //-------------------------------------------------------------------
    TRB: record
    //-------------------------------------------------------------------
    //TRB: array[1..4] of double;  //���������� � ����������� �������� ���
     {1=0.012      ;����������� �������� SESR ��� ����, %
      2=0.05       ;����������� �������� ���  ��� ����, %
      3=600        ;����� ����, ��
      4=0.200      ;���������� ����� ���, ������������� �������������
      }

      GST_SESR_1               : double; // '����������� �������� SESR ��� ����, %');
      GST_Kng_2                : double; // '����������� �������� ���  ��� ����, %');
  //    GST_equivalent_length_km_3: Double; // '����� ����, ��');
      GST_Length_KM_3_          : Double; // '����� ����, ��');
      KNG_dop_4                : double; // '���������� ����� ���, ������������� �������������}');
      SPACE_LIMIT_5            : double; // '���������� ������������� ������� p(a%)');
      SPACE_LIMIT_PROBABILITY_6: double; // '���������� ����������� a% ������������� ������������ �������� p(a%) [%]');

      Fade_margin_required_7: double;//��������� ����� �� ��������� [dB] (0-������� ������� �� ��������� ����������)"


//      ���� �������� ���������� ���������� � ��������� ������ � xml-����� � ������ <TRB comment="����������"> ����� ������������ ����� ���������� ��� ������� 7:
//<param id="7" value="0" comment="��������� ����� �� ��������� [dB] (0-������� ������� �� ��������� ����������)"/>

    end;

    Optimization: record
      OptimizeEnabled: Boolean;
      OptimizeKind: (otMinMax_H1_H2, otMin_H1_plus_H2);

      Criteria_opti_dop_antenna:  (otMin_SESR, otMin_KNG);//  '�������� ����������� ������ ���.���.: 0-min SESR, 1-min KNG');

    end;

    // ---------------------------------------------------------------
    Relief : record
    // ---------------------------------------------------------------

      Length_KM: Double; //����� ���������

//      Count : Integer;
    //  Items1: array[0..20000] of //packed

      Items: array of //packed
        record
           Distance_KM  : Double;
           rel_h        : Double;
           Clutter_h    : Double;
           Clutter_Code : smallint;

           earth_h      : Double;//test
        end;
    end;


  private
    procedure SetDefault;
  public
    ErrorMsg : string;

    constructor Create;
    function Validate1: boolean;

    function SaveToXML(aFileName: string; aIsSaveProfile: boolean = true): Boolean;

    procedure SetPolarization(aValue: string);
    procedure Show;

    function ValidateDlg: Boolean;
  end;


implementation



constructor TrrlCalcParam.Create;
begin
  inherited;

  IsShowWindow := False; //!!!!!!

  FillChar(RRV,SizeOf(RRV),9999);


end;


procedure TrrlCalcParam.Show;
var
  s: string;
  sFile: string;
begin
  sFile:=GetTempFileNameWithExt('.xml');

  SaveToXML(sFile);

  ShellExec_Notepad1(sFile);

end;


//--------------------------------------------------------------------
function TrrlCalcParam.SaveToXML(aFileName: string; aIsSaveProfile: boolean =
    true): Boolean;
//--------------------------------------------------------------------

  //--------------------------------------------------------------------
  procedure DoAdd (aNode: IXMLNode; aParamID: integer; aValue: double; aComment: string='');
  //--------------------------------------------------------------------
 // var
   // vNode: IXMLNode;
  begin


    xml_AddNodeEx(aNode, TAG_PARAM, [
        ATT_id,      aParamID,
        ATT_value,   aValue,
        ATT_COMMENT, aComment
      ]);

(*    vNode:=aNode.AddChild(TAG_PARAM);
      vNode.Attributes[ATT_id]:= aParamID;
      vNode.Attributes[ATT_value]:= aValue;
      vNode.Attributes[ATT_COMMENT]:= aComment;
*)
  end;

  //--------------------------------------------------------------------
  function DoAddLinkType(aRoot: IXMLNode): Boolean;
  //--------------------------------------------------------------------

    // ----------------------
    procedure DoAdd_RRV_new(); //vGroup: IXMLNode);
    // ----------------------
    var
      vGroup: IXMLNode;
    begin



      // -------------------------
      vGroup:=aRoot.AddChild('RRV');
        vGroup.Attributes['comment'] := '������� ��������������� ���������';


      case Calc_Method of

(*
1. ������� �������� ��������� ����. �������������,10^-8, 1/�
2. ����������� ���������� ���������,10^-8, 1/�
3.��� ��������� (0-����������, 1- ���������� (��������), 2- ���������� (������))
4.��� ������������ �������. (1- �����������������, 2-������������, 3-������)
5.���������� ��������� �������� ����, �/���.�
6.����������� �������, ��.�
7.����������� ��������, ����
8.������������� ������ K��, 10^-6 (��� F>20���)
9.����������� ������������� �����
10.���������� ������� ��������� (������), ��
11.���������� ������� ��������� (�������), ��
*)

        // -------------------------
        DEF_Calc_Method_ITU_R:
        // -------------------------
        begin
          DoAdd (vGroup, 1,  RRV.gradient_diel_1,      '������� �������� ��������� ����.�������������,10^-8 1/�' );
          DoAdd (vGroup, 2,  RRV.gradient_deviation_2, '����������� ���������� ���������,10^-8 1/�' );


          Assert(RRV.underlying_terrain_type_3>0, 'Value <=0');

          DoAdd (vGroup, 3,  RRV.underlying_terrain_type_3,
                '��� ������������ �������.(1...3,0->����.�����=����.���.) '+ '1-����������������� 2-������������ 3-������' );

          DoAdd (vGroup, 4,  RRV.steam_wet_4,    '���������� ��������� �������� ����, �/���.�');


          DoAdd (vGroup, 5,  RRV.Q_factor_5,     'Q-������ ������ �����������');
          DoAdd (vGroup, 6,  RRV.climate_factor_6,'������������� ������ K��, 10^-6');
          DoAdd (vGroup, 7,  RRV.factor_B_7,     '�������� ������������ b ��� ������� ������');

          DoAdd (vGroup, 8,  RRV.factor_C_8,     '�������� ������������ c ��� ������� ������');
          DoAdd (vGroup, 9,  RRV.FACTOR_D_9,     '�������� ������������ d ��� ������� ������');
          DoAdd (vGroup, 10, RRV.RAIN_intensity_10,'������������� ����� � ������� 0.01% �������, ��/���');

          DoAdd (vGroup, 12, RRV.Air_temperature_12,     '����������� ������� [��.�] (�� ��������� 15)');
          DoAdd (vGroup, 13, RRV.Atmosphere_pressure_13, '����������� �������� [����] (�� ��������� 1013)');

          DoAdd (vGroup, 14, RRV.terrain_type_14,  '��� ���������(0...2) 0-���������� 1-���������� (��������) 2-���������� (������)' );

        end;



        // -------------------------
        DEF_Calc_Method_GOST:
        // -------------------------
        begin
          DoAdd (vGroup, 1,  RRV.gradient_diel_1,      '������� �������� ��������� ����.�������������,10^-8 1/�' );
          DoAdd (vGroup, 2,  RRV.gradient_deviation_2, '����������� ���������� ���������,10^-8 1/�' );


          Assert(RRV.underlying_terrain_type_3>0, 'Value <=0');

          DoAdd (vGroup, 3,  RRV.underlying_terrain_type_3,
                    '��� ������������ �������.(1...3,0->����.�����=����.���.) '+'1-����������������� 2-������������ 3-������' );

          DoAdd (vGroup, 4,  RRV.steam_wet_4,    '���������� ��������� �������� ����, �/���.�');


          DoAdd (vGroup, 6,  RRV.climate_factor_6,       '������������� ������ K��, 10^-6');

          DoAdd (vGroup, 12, RRV.Air_temperature_12,     '����������� ������� [��.�] (�� ��������� 15)');
          DoAdd (vGroup, 13, RRV.Atmosphere_pressure_13, '����������� �������� [����] (�� ��������� 1013)');

          DoAdd (vGroup, 14, RRV.terrain_type_14,  '��� ���������(0...2) 0-���������� 1-���������� (��������) 2-���������� (������)' );


(*
      Assert(Link_Center_Point.B<16>0);
      Assert(Link_Center_Point.L<>0);

      DoAdd (vGroup, 16,  Link_Center_Point.B, '7.���������� ������� ��������� (������), ��');
      DoAdd (vGroup, 16,  Link_Center_Point.L, '8.���������� ������� ��������� (�������), ��');
*)

          Assert(Link_Center_Point.B<>0);
          Assert(Link_Center_Point.L<>0);


          DoAdd (vGroup, 15, TruncFloat(Link_Center_Point.B, 6), '���������� ������� ��������� � �������� (������)');
          DoAdd (vGroup, 16, TruncFloat(Link_Center_Point.L, 6), '���������� ������� ��������� � �������� (�������)');


          DoAdd (vGroup, 19, RRV.RRV_GOST.RAIN_intensity_19, '����������� ������������� �����');

        end;

        // -------------------------
        DEF_Calc_Method_NIIR_1998:
        // -------------------------
        begin
          DoAdd (vGroup,1,  RRV.gradient_diel_1,                 '1.������� �������� ��������� ����. �������������,10^-8, 1/�');
          DoAdd (vGroup,2,  RRV.gradient_deviation_2,            '2.����������� ���������� ���������,10^-8, 1/�');
          DoAdd (vGroup,4,  RRV.steam_wet_4,                     '4.���������� ��������� �������� ����, �/���.�');
          DoAdd (vGroup,12, RRV.Air_temperature_12,              '5.����������� �������, ��.�');
          DoAdd (vGroup,13, RRV.Atmosphere_pressure_13,          '6.����������� ��������, ����');

          DoAdd (vGroup,14, RRV.terrain_type_14,                 '3.��� ��������� (0-����������, 1- ���������� (��������), 2- ���������� (������)');


          DoAdd (vGroup,17, RRV.RRV_NIIR1998.Rain_region_number_17,  '7.����� ������ �� ����� ������������� ������ (1...22)');
          DoAdd (vGroup,18, RRV.RRV_NIIR1998.Qd_region_number_18,    '8.����� ������ (������) �� ����� Qd (1�22)');
          DoAdd (vGroup,20, RRV.RRV_NIIR1998.Water_percent_20,       '9. ���� ������ ����������� �� ���������, %');
        end;


        // -------------------------
        DEF_Calc_Method_E_BAND_ITU:
        // -------------------------
        begin
          DoAdd (vGroup,  1,  RRV.RRV_E_BAND_ITU.Param_1, '1.������� �������� ��������� ����. �������������, 10^-8, 1/�');
          DoAdd (vGroup,  2,  RRV.RRV_E_BAND_ITU.Param_2, '2.����������� ���������� ���������,10^-8, 1/�');
          DoAdd (vGroup,  4,  RRV.RRV_E_BAND_ITU.Param_3, '3.���������� ��������� �������� ����, �/���.�');
          DoAdd (vGroup, 10,  RRV.RRV_E_BAND_ITU.Param_6, '6.������������� ����� � ������� 0.01% �������, ��/���');


          DoAdd (vGroup, 12,  RRV.RRV_E_BAND_ITU.Param_4, '4.����������� �������, ��.�');
          DoAdd (vGroup, 13,  RRV.RRV_E_BAND_ITU.Param_5, '5.����������� ��������, ����');

        end;

        // -------------------------
        DEF_Calc_Method_UKV:
        // -------------------------
        begin
          DoAdd (vGroup,  1,  RRV.RRV_UKV.Param_1,  '1.������� �������� ��������� ����. �������������, 10^-8, 1/�' );
          DoAdd (vGroup,  3,  RRV.RRV_UKV.Param_2,  '2.��� ������������ ����������� (1-������������.(����.), 2-�������������., 3-����.)' );
          DoAdd (vGroup,  8,  RRV.RRV_UKV.Param_3,  '3.��������.����������� c (0-�����.����.�� ���)' );
          
          DoAdd (vGroup, 12,  RRV.RRV_UKV.Param_4,  '4.��� ������ ������� (��� �>0) [dB] (0-���������)' );
        end;
      end;


    end;


    // ----------------------
    procedure DoAdd_RRV(); //vGroup: IXMLNode);
    // ----------------------
    var
      vGroup: IXMLNode;
    begin
      // -------------------------
      vGroup:=aRoot.AddChild('RRV');
        vGroup.Attributes['comment'] := '������� ��������������� ���������';

      // -------------------------
      Assert(RRV.underlying_terrain_type_3>0, 'Value <=0');

      DoAdd (vGroup, 1,  RRV.gradient_diel_1,      '������� �������� ��������� ����.�������������,10^-8 1/�' );
      DoAdd (vGroup, 2,  RRV.gradient_deviation_2, '����������� ���������� ���������,10^-8 1/�' );

    //  case Calc_Method of
      //  DEF_Calc_Method_GOST

      DoAdd (vGroup, 3,  RRV.underlying_terrain_type_3,
                '��� ������������ �������.(1...3,0->����.�����=����.���.) '+
                '1-����������������� 2-������������ 3-������' );

      DoAdd (vGroup, 4,  RRV.steam_wet_4,    '���������� ��������� �������� ����, �/���.�');
      DoAdd (vGroup, 5,  RRV.Q_factor_5,     'Q-������ ������ �����������');
      DoAdd (vGroup, 6,  RRV.climate_factor_6,'������������� ������ K��, 10^-6');
      DoAdd (vGroup, 7,  RRV.factor_B_7,     '�������� ������������ b ��� ������� ������');
      DoAdd (vGroup, 8,  RRV.factor_C_8,     '�������� ������������ c ��� ������� ������');
      DoAdd (vGroup, 9,  RRV.FACTOR_D_9,     '�������� ������������ d ��� ������� ������');
      DoAdd (vGroup, 10, RRV.RAIN_intensity_10,'������������� ����� � ������� 0.01% �������, ��/���');
      DoAdd (vGroup, 11, RRV.REFRACTION_11,    '�����. ��������� (��� ��.���������=0, ����� �.�.=0)');


      DoAdd (vGroup, 12, RRV.Air_temperature_12,     '����������� ������� [��.�] (�� ��������� 15)');
      DoAdd (vGroup, 13, RRV.Atmosphere_pressure_13, '����������� �������� [����] (�� ��������� 1013)');



      DoAdd (vGroup, 14, RRV.terrain_type_14,  '��� ���������(0...2) 0-���������� 1-���������� (��������) 2-���������� (������)' );


      case Calc_Method of
        DEF_Calc_Method_GOST:
        begin
          DoAdd (vGroup, 15, TruncFloat(Link_Center_Point.B, 6), '���������� ������� ��������� � �������� (������)');
          DoAdd (vGroup, 16, TruncFloat(Link_Center_Point.L, 6), '���������� ������� ��������� � �������� (�������)');

//          DoAdd (vGroup, 15, TruncFloat(RRV.GOST.Link_Center_lat_15, 6), '���������� ������� ��������� � �������� (������)');
//          DoAdd (vGroup, 16, TruncFloat(RRV.GOST.Link_Center_lon_16, 6), '���������� ������� ��������� � �������� (�������)');

        end;


        DEF_Calc_Method_NIIR_1998 :
        begin

  //      <param id="17" value="1" comment="����� ������ �� ����� ���������.������ ����-98 (1...22)"/>
   //     <param id="18" value="1" comment="����� ������ �� ����� Qd ����-98 (1...22)"/>

          DoAdd (vGroup, 17, RRV.RRV_NIIR1998.Rain_region_number_17, '����� ������ �� ����� ���������.������ ����-98 (1...22)');
          DoAdd (vGroup, 18, RRV.RRV_NIIR1998.Qd_region_number_18,   '����� ������ �� ����� Qd ����-98 (1...22)');
        end;
      end;


      case Calc_Method of
        DEF_Calc_Method_GOST:
          DoAdd (vGroup, 19, RRV.RRV_GOST.RAIN_intensity_19, '����������� ������������� �����');

        DEF_Calc_Method_NIIR_1998 :
          DoAdd (vGroup, 20, RRV.RRV_NIIR1998.Water_percent_20, '���� ������ ����������� �� �������-��, %');
      end;

    //  if Calc_Method = DEF_Calc_Method_GOST then




{      if RRV.gradient=0 then
      begin aErrMsg:='����������� ���������� ���������,10^-8 1/�=0';
            Exit;

      end;
}
    end;

    

  var vGroup: IXMLNode;
      iStep: integer;
      d,dRefraction,dLength: double;
  begin
    Result := False;

  //  vGroup:=aRoot.AddChild('RRV');
   //   vGroup.Attributes['comment'] := '������� ��������������� ���������';

    DoAdd_RRV_new();




//      DoAdd (vGroup, 11, 1.335 {FieldValues[FLD_refraction_gradient_0]}, '�����. ��������� (��� ��.���������=0, ����� �.�.=0)');

    //DLT: array[1..4] of double;  //���������� � �������� ��������
     {1= 0.100     ;��� ��������� �������, �� ( 0 - ��������� ����)
      2= 1.000     ;�������� ������ ������� V����(g)=V���.�, ��
      3= 1.000     ;�������� ������ ������� g(V����)=g(V���.�), 10^-8
      4=80.000     ;������������ �������� ��� ������� ������������, 10^-8}

      // -------------------------
      vGroup:=aRoot.AddChild('DLT');
        vGroup.Attributes['comment']:='��������� �����';
      // -------------------------
//      iStep:=FieldByName(FLD_profile_step).AsInteger;

      DoAdd (vGroup,  1, DLT.Profile_Step_KM_1, '��� ��������� �������, �� ( 0 - ��������� ����)');

      // default values
      DoAdd (vGroup,  2, DLT.precision_V_diffraction_2,   '�������� ������ ������� V����(g)=V���.�, ��');
      DoAdd (vGroup,  3, DLT.precision_g_V_diffraction_3, '�������� ������ ������� g(V����)=g(V���.�), 10^-8');
      DoAdd (vGroup,  4, DLT.max_gradient_for_subrefr_4,  '������������ �������� ��� ������� ������������, 10^-8');


      if Link_Graph.NFunc1>0 then
        with Link_Graph do
      begin
        DoAdd (vGroup,  5,  NFunc1,   '����� ������ ������� 1..179');
        DoAdd (vGroup,  6,  NFunc2,   '����� ������ ������� 1..179');
        DoAdd (vGroup,  7,  ArgGroup, '����� ������ ���������� 1-TTX, 2-PPB');
        DoAdd (vGroup,  8,  NArg,     '����� ��������� 1..42 ��� ���, 1..11 ��� ���');
        DoAdd (vGroup,  9,  Arg_min,  '��� �������� ���������');
        DoAdd (vGroup,  10, Arg_max,  '���� �������� ���������');
        DoAdd (vGroup,  11, Arg_step, '��� �������� �������� ���������');
      end;

      DoAdd (vGroup,  12, Calc_Method,
          '0-ITU-R � 16 ���� �� (�� ���������); '+
          '1-���� � 53363 - 2009; '+
          '2-����-1998; '+
          '3-E-�������� (ITU); '+
       //   '4-E_BAND_GOST; '+
         // '5-E_BAND_NIIR; '+
          '4-��� (16�����); ');

               
//        <param id="12" value="2" comment="0-16 ���� �� � ITU-R (�� ������-���), 1-���� � 53363-2009, 2- ����-1998, "/>



     //08.09.2008 ��������� ��� ���������� ��������



    //TRB: array[1..4] of double;  //���������� � ����������� �������� ���
     {1=0.012      ;����������� �������� SESR ��� ����, %
      2=0.05       ;����������� �������� ���  ��� ����, %
      3=600        ;����� ����, ��
      4=0.200      ;���������� ����� ���, ������������� �������������}
      // -------------------------
//      vGroup:=aRoot.AddChild('TRB');
//        vGroup.Attributes['comment'] := '����������';
      // -------------------------

      vGroup:=xml_AddNodeEx(aRoot, 'TRB', [
         'comment', '����������'
        ]);



      DoAdd (vGroup,  1, Trb.GST_SESR_1,      '����������� �������� SESR ��� ����, %');
      DoAdd (vGroup,  2, Trb.GST_Kng_2,       '����������� �������� ���  ��� ����, %');
//      DoAdd (vGroup,  3, Trb.GST_Equivalent_length_km_3, '������������� ����� ����, ��');
      DoAdd (vGroup,  3, Trb.GST_length_km_3_, '������������� ����� ����, ��');
      DoAdd (vGroup,  4, Trb.KNG_dop_4,       '���������� ����� ���, ������������� �������������');

      if (Calc_Method in [DEF_Calc_Method_ITU_R, DEF_Calc_Method_UKV]) and (TTX.Freq_GHz < 2) then
        Trb.SPACE_LIMIT_5:=-15;


      DoAdd (vGroup,  5, Trb.SPACE_LIMIT_5,   '���������� ������������� ������� p(a%)');
      DoAdd (vGroup,  6, Trb.SPACE_LIMIT_PROBABILITY_6,  '���������� ����������� a% ������������� ������������ �������� p(a%) [%]');

    result := True;
  end;


  //--------------------------------------------------------------------
  procedure DoSaveProfileToXMLNode (vRoot: IXMLNode);
  //--------------------------------------------------------------------
  var i: integer;
    vGroup: IXMLNode;
    vNode: IXMLNode;
    eDist: double;

  begin
    Assert(Length(Relief.Items)>0, 'Length(Relief.Items)=0');

 //   Assert(Relief.Length_KM>0, 'Value <=0');


    vGroup:=xml_AddNodeEx(vRoot, 'relief', [
        'count',      Length(Relief.Items),
        'Length',     Relief.Length_KM,
        'Length_km',  Relief.Length_KM
      ]);


(*    vGroup:=vRoot.AddChild ('relief');
      vGroup.Attributes['count']:=      Length(Relief.Items);
      vGroup.Attributes['Length']:=     Relief.Length_KM;
      vGroup.Attributes['Length_km']:=  Relief.Length_KM;
*)

//      vGroup.Attributes['Length']:=     TRB.GST_Length_KM_3;
 //     vGroup.Attributes['Length_km']:=  TRB.GST_Length_KM_3;


    for i:=0 to High(Relief.Items) do
    begin
{
      DoAdd (vGroup,  12, Calc_Method,
          '0-ITU-R � 16 ���� �� (�� ���������); '+
          '1-���� � 53363 - 2009; '+
          '2-����-1998; '+
          '3-E-�������� (ITU); '+
       //   '4-E_BAND_GOST; '+
         // '5-E_BAND_NIIR; '+
          '4-��� (16�����); ');

      <param id="14" value="81.12" comment="P������ �������, ���"/>

  DoAdd (vGroup, 14, TTX.Freq_GHz,     'P������ �������, ���');

������ ��� ������� ���������� (ITU, ���� � 16 �����) � ��� (16 �����)
                ���� ������� ������ 2���, ��:

const
  DEF_Calc_Method_ITU_R       = 0;
  DEF_Calc_Method_GOST        = 1;
  DEF_Calc_Method_NIIR_1998   = 2;
  DEF_Calc_Method_E_BAND_ITU  = 3;
  DEF_Calc_Method_UKV         = 4;

const
  DEF_CLU_OPEN_AREA = 0;  // 1- ��� (1)
  DEF_CLU_FOREST    = 1;  // 1- ��� (1)
  DEF_CLU_WATER     = 2;  // 3- ���
  DEF_CLU_COUNTRY   = 3;  // 3- ���
  DEF_CLU_ROAD      = 4;
  DEF_CLU_RailROAD  = 5;
  DEF_CLU_BOLOTO_NEPROHODIMOE  = 6; // 6 - ������������ ������
  DEF_CLU_CITY      = 7;  // 7- �����
  DEF_CLU_BOLOTO    = 8; // ���������� ������
  DEF_CLU_ONE_BUILD = 73; // 73- ���.����
  DEF_CLU_ONE_HOUSE = 31; // 31- ���


1.	�� ��������� �� ��������� ������ ��������������� ������������� ������� -15.
2.	�� ������� ��� ������� ��������� �������� ������������� ������ ���� 5 ������ �� ���������� 200 ������ �� ���� ���������� ������ ����� � ������, ������� ������ � ��������� ������� (����� ��������� ������).
3.	�� ������� ��� ������� ��������� �������� ������������� ������ ��������� ��������� 10 ������ �� ���������� 100 ������ �� ���� ���������� ������ ����� � ������, ������� ������ � ��������� ������� (����� ��������� ������).

����� - ��� ��������� � ���������� ��������� � �� �������.



}
      if (Calc_Method in [DEF_Calc_Method_ITU_R, DEF_Calc_Method_NIIR_1998, DEF_Calc_Method_UKV]) and (TTX.Freq_GHz < 2) then
//      if (Calc_Method in [DEF_Calc_Method_ITU_R, DEF_Calc_Method_UKV]) and (TTX.Freq_GHz < 2) then
      begin
      // ---------------------------------------------------------------
      // 2.	�� ������� ��� ������� ��������� �������� ������������� ������ ���� 5 ������ �� ���������� 200 ������ �� ���� ���������� ������ ����� � ������, ������� ������ � ��������� ������� (����� ��������� ������).
      // ---------------------------------------------------------------
        if ((Relief.Items[i].Distance_KM < 0.2) or ( Relief.Length_KM - Relief.Items[i].Distance_KM < 0.2))
            and
           (Relief.Items[i].Clutter_Code = DEF_CLU_FOREST)
        then begin
            if (Relief.Items[i].Clutter_h > 5 ) then Relief.Items[i].Clutter_h:=5;
           // if (Relief.Items[i].Clutter_h > 10 ) then Relief.Items[i].Clutter_h:=10;
          end;

      // ---------------------------------------------------------------
      // 3.	�� ������� ��� ������� ��������� �������� ������������� ������ ��������� ��������� 10 ������ �� ���������� 100 ������ �� ���� ���������� ������ ����� � ������, ������� ������ � ��������� ������� (����� ��������� ������).
      // ---------------------------------------------------------------

        if ((Relief.Items[i].Distance_KM < 0.1) or ( Relief.Length_KM -  Relief.Items[i].Distance_KM < 0.1))
            and
           (Relief.Items[i].Clutter_Code in [DEF_CLU_ONE_BUILD, DEF_CLU_CITY])
        then begin
            if (Relief.Items[i].Clutter_h > 10 ) then Relief.Items[i].Clutter_h:=10;
           // if (Relief.Items[i].Clutter_h > 10 ) then Relief.Items[i].Clutter_h:=10;
          end;

      end;



      // ������ ������ = 0 !!!!!
      if (i=0) then
        Relief.Items[i].Distance_KM:=0;

      // ��������� ������ = Relief.Length_KM !!!!!
      if (i=High(Relief.Items)) then
        Relief.Items[i].Distance_KM:=Relief.Length_KM;



      if (i >0) and
         (Relief.Items[i].Distance_KM <= Relief.Items[i-1].Distance_KM)
      then
        continue;


      if Relief.Items[i].Distance_KM > Relief.Length_KM then
        continue;

      if Relief.Items[i].Distance_KM<0 then
        continue;


        //Relief.Items[i].Distance_KM:=0;

//      Assert(relief[i].dist >=0 );

(*
      eDist:=TruncFloat(Relief.Items[i].Distance_KM, 4);
      if eDist > Relief.Length_KM then
        Exit;
       // eDist := Relief.Length_KM;

*)


    eDist:=  RoundFloat(Relief.Items[i].Distance_KM, 9);


    xml_AddNodeEx(vGroup, 'relief', [
//       'dist',       Relief.Items[i].Distance_KM ,
       'dist',       eDist,
       'rel_h',      Relief.Items[i].rel_h,
       'local_h',    Relief.Items[i].Clutter_h,
       'local_code', Relief.Items[i].Clutter_code

      ]);



    end;
  end;

  // -------------------------------------------------------------------


var
  vRoot,vGroup: IXMLNode;

 // oXMLDoc: TXMLDoc;

  oXMLDoc: TXmlDocumentEx;
  sHASH: string;

 // oXMLDocument: XMLDoc.TXMLDocument;
//  .FileName :=  aFileName;//  'd:\doc.kml';
 // XMLDocument1.Active := True;


begin
//  <param id="3" value="0" comment="����� ����, ��"/>
//  IF TRB.GST_Length_KM_3_

  assert(TRB.GST_Length_KM_3_>0);


  SetDefault;


  result := False;

//  Assert(Link_Center_Point.B>0, 'Value <=0');
//  Assert(Link_Center_Point.L>0, 'Value <=0');



//  oXMLDocument:=TXMLDocument.Create(nil);

  oXMLDoc := TXmlDocumentEx.Create();
 // gl_XMLDoc.Clear;

  vRoot:=oXMLDoc.DocumentElement.AddChild('RRL_PARAMS');

//  if IsShowWindow then
    vRoot.Attributes['IsShowWindow']:= IsShowWindow;
    //'False';


//  xml_AddNodeTag (oXMLDoc.DocumentElement, 'RRL_PARAMS');

  if not DoAddLinkType (vRoot) then
  begin
    oXMLDoc.Free;
    exit;
  end;

  vGroup:=xml_AddNodeEx(vRoot, 'TTX', [
     'comment', '����������� ��������������'
    ]);

 // vGroup:=vRoot.AddChild ('TTX');
  // vGroup.Attributes['comment']:= '����������� ��������������';

  DoAdd (vGroup,  1, TTX.Site1_Power_dBm,      'site1-�������� dBm');
  DoAdd (vGroup,  2, TTX.Site1_DIAMETER,       'site1-�������');
  DoAdd (vGroup,  3, TTX.Site1_GAIN,           'site1-�������� dB');
  DoAdd (vGroup,  4, TTX.Site1_VERT_WIDTH,     'site1-������ ���');
  DoAdd (vGroup,  5, TTX.Site1_LOSS,           'site1-������ � ���');
  DoAdd (vGroup,  6, TTX.Site1_HEIGHT,         'site1-������');

  DoAdd (vGroup,  7,  TTX.Site2_Threshold_dBM, 'site2-������ dBM');
  DoAdd (vGroup,  8,  TTX.Site2_DIAMETER,      'site2-�������');
  DoAdd (vGroup,  9,  TTX.Site2_GAIN,          'site2-�������� dB');
  DoAdd (vGroup,  10, TTX.Site2_VERT_WIDTH,    'site2-������ ���');
  DoAdd (vGroup,  11, TTX.Site2_LOSS,          'site2-������ � ���');
  DoAdd (vGroup,  12, TTX.Site2_HEIGHT,        'site2-������');



  // -------------------------------------------------------------------
  //  POLARIZATION
  // -------------------------------------------------------------------
  DoAdd (vGroup, 13, TTX.POLARIZATION, '��� �����������, 0-��������������,1-������������,2-�����');
  DoAdd (vGroup, 14, TTX.Freq_GHz,     'P������ �������, ���');

 // if TTX.Freq_GHz=0 then begin aErrMsg:='������� GHz =0'; Exit; end;

  // -------------------------------------------------------------------
  //  LEFT  - ����������
  // -------------------------------------------------------------------

///////////!!!!!!!
//  DoAdd (vGroup, 15, FieldValues[FLD_Speed],            '�������� ��������, ��/�');
  DoAdd (vGroup, 15, TTX.BitRate_Mbps,     '�������� ��������, ��/�');
  DoAdd (vGroup, 16, TTX.SIGNATURE_WIDTH_Mhz,  '������ ���������, ���');
  DoAdd (vGroup, 17, TTX.SIGNATURE_HEIGHT_dB, '������ ���������, ��');

//  TTX.Modulation_Type :=1;
  DoAdd (vGroup, 18, TTX.GOST_53363.Modulation_Type,  '��� ���������, 0-PSK (QPSK), 1-QAM (TCM)');

////////!!!!!!!    DoAdType).AsInteger, '��� ������� ���������, 1-���, 2-��, 3-���');
  DoAdd (vGroup, 19, TTX.MODULATION_Level_COUNT,  '����� ������� ���������');
//  DoAdd (vGroup, 20, TTX.EQUALISER_PROFIT,  '�������, �������� ������������, ��');
  DoAdd (vGroup, 20, 0,  '�������, �������� ������������, ��');


    //////////////
//    iValue:=FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger + 1;
  //  DoAdd (vGroup, 21, iValue, '��������� ���������� �� �������, 1...8');  //�� ���


  // -------------------------------------------------------------------
  //  RIGHT  - ��������
  // -------------------------------------------------------------------


  DoAdd (vGroup, 21, ttx.KRATNOST_BY_FREQ,  '��������� ���������� �� �������, 1...8');  //�� ���

  // XPIC
  if ttx.KRATNOST_BY_SPACE>2 then
     Inc (ttx.KRATNOST_BY_SPACE,2);
  DoAdd (vGroup, 22, ttx.KRATNOST_BY_SPACE, '��������� ���������� �� ������������, 1...2'); //�� ���


//  assert(ttx.Freq_Spacing_MHz < 700);

  DoAdd (vGroup, 23, ttx.Freq_Spacing_MHz,   '��������� ������, ���');


{ 24-28 - ��� ��� 1
  29-33 - ��� ��� 2
  }


  if TTX.Dop_Site1.GAIN>0 then
  begin
    DoAdd (vGroup,  24, TTX.Dop_Site1.DIAMETER,      'dop-site1-�������');
    DoAdd (vGroup,  25, TTX.Dop_Site1.GAIN,          'dop-site1-��������  dB');
    DoAdd (vGroup,  26, TTX.Dop_Site1.VERT_WIDTH  ,  'dop-site1-������ ���');
    DoAdd (vGroup,  27, TTX.Dop_Site1.LOSS1,         'dop-site1-������ � ���');
    DoAdd (vGroup,  28, TTX.Dop_Site1.HEIGHT,        'dop-site1-������');

  end;

  if TTX.Dop_Site2.GAIN>0 then
  begin
    DoAdd (vGroup,  29, TTX.Dop_Site2.DIAMETER,     'dop-site2-�������');
    DoAdd (vGroup,  30, TTX.Dop_Site2.GAIN,         'dop-site2-�������� dB');
    DoAdd (vGroup,  31, TTX.Dop_Site2.VERT_WIDTH ,  'dop-site2-������ ���');
    DoAdd (vGroup,  32, TTX.Dop_Site2.LOSS1,        'dop-site2-������ � ���');
    DoAdd (vGroup,  33, TTX.Dop_Site2.HEIGHT,       'dop-site2-������');
  end;


(*
  // remove !!!
  DoAdd (vGroup, 34, TTX.Site1_FrontToBackRatio, 'ant1 - ����.���.�������� (������/�����) ���-�������, ��');
  DoAdd (vGroup, 35, TTX.Site2_FrontToBackRatio, 'ant2 - ����.���.�������� (������/�����) ���-�������, ��');

   //dop
  DoAdd (vGroup, 36, TTX.Dop_Site1_FrontToBackRatio, 'ant1 - ����.���.�������� (������/�����) ���-�������, ��');
  DoAdd (vGroup, 37, TTX.Dop_Site2_FrontToBackRatio, 'ant2 - ����.���.�������� (������/�����) ���-�������, ��');
*)

  DoAdd (vGroup, 38, TTX.Site1_KNG,  '��� ������������ (%)1');
  DoAdd (vGroup, 39, TTX.Site2_KNG,  '��� ������������ (%)2');

  DoAdd (vGroup, 40, TTX.AntennaOffsetHor_m,  '�������������� �����');
//  DoAdd (vGroup, 40, 990,  '�������������� �����');
//  DoAdd (vGroup, 40, 0,  '�������������� �����');

   //04.02.2011
  DoAdd (vGroup, 41, TTX.Threshold_degradation_dB,  '���������� ����������������, ��');


  //------------------------------------------------------
  // !! ����������� ���������� ��� ������������� ������� �������
  //------------------------------------------------------
  if  Optimization.OptimizeEnabled then
  begin
//  (otNone, otMinMax_H1_H2, otMin_H1_plus_H2);
// then
    DoAdd (vGroup, 42, IIF(Optimization.OptimizeKind=otMinMax_H1_H2,0,1),  '�������� ����������� ����� ���.������ (0- min max (H1,H2); 1- min H1+H2)');

    {
    36-37 - ��� ��� 1,2
    }

    DoAdd (vGroup, 43, IIF(Optimization.Criteria_opti_dop_antenna=otMin_SESR,0,1),  '�������� ����������� ������ ���.���.: 0-min SESR, 1-min KNG');
  end;

  //----------------------
  // GOST_53363
  //----------------------

  if Calc_Method = DEF_Calc_Method_GOST then
  begin
//    if TTX.GOST_53363.RX_count_on_combined_diversity then


    DoAdd (vGroup, 44, TTX.GOST_53363.RX_count_on_combined_diversity,  '���������� ���������� ��� ����.����������');
  end;

   //04.02.2011
  DoAdd (vGroup, 45, TTX.ATPC_profit_dB,  '������� �� ATPC, ��');
  DoAdd (vGroup, 46, TTX.Dop_Antenna.POLARIZATION, '��� ����������� Dop_Antenna, 0-��������������,1-������������,2-�����');


  DoAdd (vGroup, 48, TTX.XPIC_42_use_2_stvol, '48.������� ���.2 ���.� ���������.����. [0-���,1-��]');
  DoAdd (vGroup, 49, TTX.XPIC_49_coef,        '49.����.�����.���.��� ��.�����.[��]');
  DoAdd (vGroup, 50, TTX.XPIC_50,             '50.�����.�� �������.����� [��]');


{
���� � ���, ��� �� � ����� �������� �������� � xml-���� ������ ���� ����������, � ���� ���:
'48.������� ���.2 ���.� ���������.����. [0-���,1-��]',
'49.����.�����.���.��� ��.�����.[��]',
'50.�����.�� �������.����� [��]', );
}



{
      XPIC_49_coef: double;
      XPIC_50: double;

      //    '',
      //    '', );
}


 // gl_XMLDoc.SaveToFile(aFileNameNoProfile);
  if aIsSaveProfile then
  begin
    Assert(Length(Relief.Items)>0, 'Length(Relief.Items)=0');


    DoSaveProfileToXMLNode (vRoot);
  end;

  oXMLDoc.SaveToFile(aFileName);

//  AssertFil


  {$IFDEF open_file}
 ShellExec_Notepad(aFileName);

  {$ENDIF}


//   sHASH:=u_hash.MD5_from_str(oXMLDoc.SaveToText);

   StrToFile( sHASH, ChangeFileExt(aFileName,'.ini'));

 // gl_XMLDoc.SaveToFile(TEMP_IN_FILENAME);

  result := True;

  FreeAndNil(oXMLDoc);




//  FreeAndNil(oXMLDocument);
end;




function TrrlCalcParam.ValidateDlg: boolean;
begin
  Result :=Validate1;
  if not Result then
    ShowMessage(ErrorMsg);
end;


// ---------------------------------------------------------------
function TrrlCalcParam.Validate1: boolean;
// ---------------------------------------------------------------
var
//  I: Integer;
  oStrList: TStringList;

  procedure DoCondition (aCondition : boolean ; aMsg : string);
  begin
    if aCondition then
      oStrList.Add(aMsg);
  end;


begin
  oStrList:=TStringList.Create;

  DoCondition (TTX.Freq_GHz=0, '������� MHz = 0');

//  if TTX.Freq_GHz=0 then
 //   oStrList.Add('������� MHz = 0');

(*
     DEF_Calc_Method_ITU_R = 0;
  DEF_Calc_Method_GOST  = 1;
  DEF_Calc_Method_NIIR_1998 = 2;

*)


(*  for I := 1 to High(Relief.Items) do
  begin
    Assert(Relief.Items[i].Distance_KM>0, 'Relief.Items[i].Distance_KM>0');


    Assert(Relief.Items[i-1].Distance_KM < Relief.Items[i].Distance_KM, '');

  end;

*)

  case Calc_method of

    DEF_Calc_Method_E_BAND_ITU:
    begin
    //  DoCondition(RRV.gradient_deviation_2=0, '����������� ���������� ���������, 10^-8 1/�=0');

    end;

    // ---------------------------------------------------------------
    DEF_Calc_Method_ITU_R:
    // ---------------------------------------------------------------
    begin
      DoCondition(RRV.gradient_deviation_2=0, '����������� ���������� ���������, 10^-8 1/�=0');

//      if RRV.gradient_deviation_2=0 then
  //       oStrList.Add('����������� ���������� ���������, 10^-8 1/�=0');

    end;

    // ---------------------------------------------------------------
    DEF_Calc_Method_GOST:
    // ---------------------------------------------------------------

(*  end;

//  if Calc_method=0 then

  // ������ �� ����� ��������
  // -------------------------
  if (Calc_Method=1) then
  // -------------------------*)
    begin
      DoCondition(TTX.BitRate_Mbps=0, 'BitRate Mbps = 0');
      DoCondition(RRV.Air_temperature_12=0, 'Air_temperature = 0');



//      if TTX.BitRate_Mbps=0 then
  //      oStrList.Add('BitRate Mbps = 0');

//      if (RRV.Air_temperature_12=0) then
  //      oStrList.Add('Air_temperature=0');

//      if RRV.GOST.Link_Center_lat_15=0 then
  //      oStrList.Add('GOST.���������� ������� ��������� � �������� (������)=0');

    //  if RRV.GOST.Link_Center_lon_16=0 then
      //  oStrList.Add('GOST.���������� ������� ��������� � �������� (�������)=0');

    end;

    // ---------------------------------------------------------------
    DEF_Calc_Method_NIIR_1998:
    // ---------------------------------------------------------------
    begin
      DoCondition(TTX.BitRate_Mbps=0, 'BitRate Mbps = 0');

//      if TTX.BitRate_Mbps=0 then
  //      oStrList.Add('BitRate Mbps = 0');


    end;

  end;


  ErrorMsg:=oStrList.Text;

  Result := oStrList.Count=0;

  oStrList.Free;
end;


procedure TrrlCalcParam.SetDefault;
begin

  case Calc_method of


    // -------------------------
    DEF_Calc_Method_E_BAND_ITU:
    // -------------------------
    if RRV.RRV_E_BAND_ITU.Param_1=0 then
    begin

     RRV.RRV_E_BAND_ITU.Param_1:=-9;   //, '1.������� �������� ��������� ����. �������������, 10^-8, 1/�');
     RRV.RRV_E_BAND_ITU.Param_2:=7;    //, '2.����������� ���������� ���������,10^-8, 1/�');
     RRV.RRV_E_BAND_ITU.Param_3:=7.5;  //, '3.���������� ��������� �������� ����, �/���.�');
     RRV.RRV_E_BAND_ITU.Param_4:=15;   //, '4.����������� �������, ��.�');
     RRV.RRV_E_BAND_ITU.Param_5:=1013; //, '5.����������� ��������, ����');
     RRV.RRV_E_BAND_ITU.Param_6:=20;   //, '6.������������� ����� � ������� 0.01% �������, ��/���');
    end;


    // -------------------------
    DEF_Calc_Method_UKV:
    // -------------------------
    if RRV.RRV_UKV.Param_1=0 then
    begin
      RRV.RRV_UKV.Param_1:=-8; // '1.������� �������� ��������� ����. �������������, 10^-8, 1/�' );
      RRV.RRV_UKV.Param_2:=2;  // '2.��� ������������ ����������� (1-������������.(����.), 2-�������������., 3-����.)' );
      RRV.RRV_UKV.Param_3:=0;  // '3.��������.����������� c (0-�����.����.�� ���)' );
      RRV.RRV_UKV.Param_4:=0;  // '4.��� ������ ������� (��� �>0) [dB] (0-���������)' );
    end;                 

  end;


end;


procedure TrrlCalcParam.SetPolarization(aValue: string);
begin
  if Eq(aValue,'h') then TTX.Polarization := 0 else
  if Eq(aValue,'v') then TTX.Polarization := 1 else
  if Eq(aValue,'x') then TTX.Polarization := 2;

    //      POLARIZATION: integer;//'��� �����������, 0-��������������,1-������������,2-�����');


//case oDBAntenna.PolarizationType of
//     ptH__: iValue:=0;
//     ptV__: iValue:=1;
//   else
//     iValue:=2;
//   end;*)
end;



end.

