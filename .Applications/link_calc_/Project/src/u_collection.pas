unit u_collection;

interface

uses
  classes,

  u_func
  ;

type
  // ---------------------------------------------------------------
  TCollectionItemWithID = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
    ID: Integer;
    Name: string;
  end;


  // ---------------------------------------------------------------
  TCollectionWithID = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TCollectionItemWithID;
  public
    function FindByID(aID: Integer): TCollectionItemWithID;
    function FindByName(aValue: string): TCollectionItemWithID;

    property Items[Index: Integer]: TCollectionItemWithID read GetItems; default;

  end;

implementation

function TCollectionWithID.GetItems(Index: Integer): TCollectionItemWithID;
begin
  Result := TCollectionItemWithID(inherited Items[Index]);
end;

// ---------------------------------------------------------------
function TCollectionWithID.FindByID(aID: Integer): TCollectionItemWithID;
// ---------------------------------------------------------------
var
  i: integer;
begin
  Result := nil;

  for i:=0 to Count-1 do
    if Items[i].ID = aID then
    begin
      Result:=Items[i];
      Break;
    end;
end;

// ---------------------------------------------------------------
function TCollectionWithID.FindByName(aValue: string): TCollectionItemWithID;
// ---------------------------------------------------------------
var i: integer;
begin
  Result := nil;

  for i:=0 to Count-1 do
    if Eq(Items[i].Name, aValue) then
    begin
      Result:=Items[i];
      Break;
    end;
end;

end.
