object dlg_AdaptiveModulation: Tdlg_AdaptiveModulation
  Left = 563
  Top = 622
  Width = 1341
  Height = 556
  Caption = 'dlg_AdaptiveModulation'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object pn_Buttons: TPanel
    Left = 0
    Top = 493
    Width = 1333
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 0
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 1333
      Height = 2
      Align = alTop
      Shape = bsTopLine
    end
    object Panel3: TPanel
      Left = 1240
      Top = 2
      Width = 93
      Height = 33
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btn_Cancel: TButton
        Left = 7
        Top = 5
        Width = 75
        Height = 21
        Cancel = True
        Caption = #1047#1072#1082#1088#1099#1090#1100
        ModalResult = 2
        TabOrder = 0
      end
    end
    object Button2: TButton
      Left = 7
      Top = 7
      Width = 169
      Height = 20
      Action = FileSaveAs1
      TabOrder = 1
    end
    object Button1: TButton
      Left = 432
      Top = 8
      Width = 75
      Height = 25
      Caption = 'test'
      TabOrder = 2
      Visible = False
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 1333
    Height = 249
    Align = alTop
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBBandedTableView1: TcxGridDBBandedTableView
      PopupMenu = PopupMenu1
      NavigatorButtons.ConfirmDelete = False
      OnCustomDrawCell = cxGrid1DBBandedTableView1CustomDrawCell
      DataController.DataSource = DataSource
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = #1089#1088#1077#1076#1085#1103#1103' '#1089#1082#1086#1088#1086#1089#1090#1100'  0 Mbit/s'
          Kind = skMin
          Column = cxGrid1DBBandedTableView1Column4
        end>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.BandHiding = True
      OptionsCustomize.BandsQuickCustomization = True
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.HeaderAutoHeight = True
      OptionsView.Indicator = True
      OptionsView.FixedBandSeparatorWidth = 1
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      Bands = <
        item
          Caption = #1056#1056#1057
        end
        item
          Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090#1099' '#1088#1072#1089#1095#1077#1090#1072
        end
        item
        end>
      object col_ID: TcxGridDBBandedColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.FilteringPopup = False
        Options.GroupFooters = False
        Options.Grouping = False
        VisibleForCustomization = False
        Width = 41
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object col__Mode: TcxGridDBBandedColumn
        Caption = #1056#1077#1078#1080#1084
        DataBinding.FieldName = 'Mode'
        Options.Filtering = False
        Width = 61
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object col_Bitrate_Mbps: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Bitrate_Mbps'
        Options.Filtering = False
        Width = 82
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object col__Power_max: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Power_dBm'
        Options.Filtering = False
        Width = 88
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object col__threshold_BER_3: TcxGridDBBandedColumn
        DataBinding.FieldName = 'threshold_BER_3'
        Options.Filtering = False
        Width = 101
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object col__threshold_BER_6: TcxGridDBBandedColumn
        DataBinding.FieldName = 'threshold_BER_6'
        Options.Filtering = False
        Width = 126
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object col_sesr: TcxGridDBBandedColumn
        DataBinding.FieldName = 'sesr'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0.00000'
        Position.BandIndex = 1
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object col_kng: TcxGridDBBandedColumn
        DataBinding.FieldName = 'kng'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0.00000'
        Position.BandIndex = 1
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object col_rx_level: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Rx_Level_dBm'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0.0'
        Width = 67
        Position.BandIndex = 1
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object col_IsWorking: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Is_Working'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Width = 65
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object col_Modulation_type: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Modulation_type'
        Width = 52
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object col_fade_margin_dB: TcxGridDBBandedColumn
        DataBinding.FieldName = 'fade_margin_dB'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0.0'
        Position.BandIndex = 1
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object col_kng_req: TcxGridDBBandedColumn
        Caption = 'kng_req'
        DataBinding.FieldName = 'kng_REQUIRED'
        Position.BandIndex = 1
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object col_sesr_req: TcxGridDBBandedColumn
        DataBinding.FieldName = 'sesr_REQUIRED'
        Position.BandIndex = 1
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object col_bandwidth: TcxGridDBBandedColumn
        DataBinding.FieldName = 'bandwidth'
        Visible = False
        GroupIndex = 0
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object col_kng_year: TcxGridDBBandedColumn
        DataBinding.FieldName = 'kng_year'
        Position.BandIndex = 1
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1K: TcxGridDBBandedColumn
        DataBinding.FieldName = 'K'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0.00000'
        Position.BandIndex = 1
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Column1: TcxGridDBBandedColumn
        Caption = 'T'#1088#1072#1073'.'#1095'.i'
        DataBinding.FieldName = 'T_work_hours'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0.00000'
        Width = 90
        Position.BandIndex = 1
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Column2: TcxGridDBBandedColumn
        Caption = 'T'#1088#1072#1073'.%i'
        DataBinding.FieldName = 'T_work_percent'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0.00000'
        Width = 90
        Position.BandIndex = 1
        Position.ColIndex = 11
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Column3: TcxGridDBBandedColumn
        Caption = 'T'#1085#1077#1075'.'#1095'.i'
        DataBinding.FieldName = 'T_neg_hours'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0.00000'
        Width = 90
        Position.BandIndex = 1
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Column4: TcxGridDBBandedColumn
        DataBinding.FieldName = 'sum'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0.00'
        Visible = False
        Width = 85
        Position.BandIndex = -1
        Position.ColIndex = -1
        Position.RowIndex = -1
      end
      object col_K_gotov: TcxGridDBBandedColumn
        Caption = 'K '#1075#1086#1090#1086#1074#1085#1086#1089#1090#1080
        Position.BandIndex = 2
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBBandedTableView1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 393
    Width = 1333
    Height = 100
    Align = alBottom
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 2
    object cxVerticalGrid1: TcxVerticalGrid
      Left = 5
      Top = 5
      Width = 404
      Height = 90
      Align = alLeft
      LookAndFeel.Kind = lfUltraFlat
      LookAndFeel.NativeStyle = False
      OptionsView.PaintStyle = psDelphi
      OptionsView.GridLineColor = clMedGray
      OptionsView.RowHeaderWidth = 325
      OptionsBehavior.AlwaysShowEditor = False
      OptionsBehavior.ImmediateEditor = False
      OptionsBehavior.HeaderSizing = False
      OptionsBehavior.RowTracking = False
      OptionsData.Editing = False
      TabOrder = 0
      Version = 1
      object row_BitRate: TcxEditorRow
        Properties.Caption = #1057#1082#1086#1088#1086#1089#1090#1100' '#1087#1088#1080#1086#1088#1080#1090#1077#1090#1085#1086#1075#1086' '#1087#1086#1090#1086#1082#1072', V'#1087#1088', Mbit/s'
        Properties.DataBinding.ValueType = 'Float'
        Properties.Value = 0.000000000000000000
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object row_Sum: TcxEditorRow
        Properties.Caption = #1057#1088#1077#1076#1085#1103#1103' '#1089#1082#1086#1088#1086#1089#1090#1100' '#1085#1077#1087#1088#1080#1086#1088#1080#1090#1077#1090#1085#1086#1075#1086' '#1087#1086#1090#1086#1082#1072', V'#1089#1088', Mbit/s'
        Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.EditProperties.DisplayFormat = ',0.000'
        Properties.DataBinding.ValueType = 'Float'
        Properties.Value = Null
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object row_B: TcxEditorRow
        Properties.Caption = #1055#1086#1083#1086#1089#1072' '#1095#1072#1089#1090#1086#1090' '#1076#1083#1103' '#1088#1077#1078#1080#1084#1072' AMR, B, MHz'
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 2
        ParentID = -1
        Index = 2
        Version = 1
      end
    end
    object b_Calc: TButton
      Left = 432
      Top = 8
      Width = 75
      Height = 25
      Caption = #1056#1072#1089#1095#1077#1090
      TabOrder = 1
      OnClick = b_CalcClick
    end
    object b_Save_SDB: TButton
      Left = 432
      Top = 40
      Width = 75
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      TabOrder = 2
      OnClick = b_Save_SDBClick
    end
  end
  object DataSource: TDataSource
    DataSet = dxMemData1
    Left = 50
    Top = 308
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\Forms\dlg_AdaptiveModulation'
    StoredValues = <>
    Left = 184
    Top = 264
  end
  object ActionList1: TActionList
    Left = 288
    Top = 264
    object FileSaveAs1: TFileSaveAs
      Category = 'File'
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1090#1072#1073#1083#1080#1094#1091' '#1074' Excel... '
      Dialog.DefaultExt = '*.xls'
      Dialog.FileName = 'C:\report.xls'
      Dialog.Filter = 'Excel *.xls|*.xls'
      Hint = 'Save As|Saves the active file with a new name'
      ImageIndex = 30
      OnAccept = FileSaveAs1Accept
    end
    object act_Save: TAction
      Category = 'File'
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1088#1077#1078#1080#1084' '#1074' '#1086#1073#1086#1088#1091#1076#1086#1074#1072#1085#1080#1077
      OnExecute = act_SaveExecute
    end
    object act_Set_Priority_Bitrate_mas_0: TAction
      Category = 'File'
      Caption = #1057#1082#1086#1088#1086#1089#1090#1100' '#1087#1088#1080#1086#1088#1080#1090#1077#1090#1085#1086#1075#1086' '#1087#1086#1090#1086#1082#1072' > 0'
      OnExecute = act_Set_Priority_Bitrate_mas_0Execute
    end
    object act_Set_Priority_Bitrate_eq_0: TAction
      Category = 'File'
      Caption = #1057#1082#1086#1088#1086#1089#1090#1100' '#1087#1088#1080#1086#1088#1080#1090#1077#1090#1085#1086#1075#1086' '#1087#1086#1090#1086#1082#1072' = 0'
      OnExecute = act_Set_Priority_Bitrate_eq_0Execute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 392
    Top = 264
    object N1: TMenuItem
      Action = act_Set_Priority_Bitrate_mas_0
    end
    object N01: TMenuItem
      Action = act_Set_Priority_Bitrate_eq_0
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object SaveAs1: TMenuItem
      Action = act_Save
    end
  end
  object frxReport1: TfrxReport
    Version = '4.15.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Modal = False
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 43355.467020949100000000
    ReportOptions.LastChange = 43494.508705844910000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    Left = 680
    Top = 272
    Datasets = <
      item
        DataSet = frxDBDataset1
        DataSetName = 'frxDBDataset1'
      end>
    Variables = <>
    Style = <
      item
        Name = 'Title'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Header'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Group header'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
      end
      item
        Name = 'Data'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
      end
      item
        Name = 'Group footer'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftTop]
      end
      item
        Name = 'Header line'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 2.000000000000000000
      end>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 323.458416670000000000
      PaperHeight = 209.999791670000000000
      PaperSize = 256
      LeftMargin = 10.001250000000000000
      RightMargin = 10.001250000000000000
      TopMargin = 10.001250000000000000
      BottomMargin = 10.001250000000000000
      object MasterData1: TfrxMasterData
        Height = 18.897650000000000000
        Top = 128.000000000000000000
        Width = 1146.920740731765000000
        DataSet = frxDBDataset1
        DataSetName = 'frxDBDataset1'
        RowCount = 0
        object Memo18: TfrxMemoView
          Align = baLeft
          Left = 30.000000000000000000
          Width = 120.000000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'THRESHOLD_BER_3'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBDataset1."THRESHOLD_BER_3"]')
          ParentFont = False
          Style = 'Data'
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          Left = 150.000000000000000000
          Width = 120.000000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'THRESHOLD_BER_6'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBDataset1."THRESHOLD_BER_6"]')
          ParentFont = False
          Style = 'Data'
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          Left = 270.000000000000000000
          Width = 64.684408730000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Power_dBm'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBDataset1."Power_dBm"]')
          ParentFont = False
          Style = 'Data'
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          Left = 334.684408730000000000
          Width = 74.267284100000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'BitRate_Mbps'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset1."BitRate_Mbps"]')
          ParentFont = False
          Style = 'Data'
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          Left = 408.951692830000000000
          Width = 87.843024198634200000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Modulation_type'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset1."Modulation_type"]')
          ParentFont = False
          Style = 'Data'
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          Left = 553.493396288634200000
          Width = 60.000000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'KNG'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBDataset1."KNG"]')
          ParentFont = False
          Style = 'Data'
        end
        object Memo24: TfrxMemoView
          Align = baLeft
          Left = 678.493396288634200000
          Width = 54.302960410000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'KNG_year'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBDataset1."KNG_year"]')
          ParentFont = False
          Style = 'Data'
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          Left = 732.796356698634200000
          Width = 65.000000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'SESR'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBDataset1."SESR"]')
          ParentFont = False
          Style = 'Data'
        end
        object Memo26: TfrxMemoView
          Align = baLeft
          Left = 862.796356698634200000
          Width = 79.058721780000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Rx_Level_dBm'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBDataset1."Rx_Level_dBm"]')
          ParentFont = False
          Style = 'Data'
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          Left = 941.855078478634200000
          Width = 101.418764300000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'FADE_MARGIN_DB'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBDataset1."FADE_MARGIN_DB"]')
          ParentFont = False
          Style = 'Data'
        end
        object Memo28: TfrxMemoView
          Align = baLeft
          Left = 613.493396288634200000
          Width = 65.000000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'KNG_required'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBDataset1."KNG_required"]')
          ParentFont = False
          Style = 'Data'
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          Left = 797.796356698634200000
          Width = 65.000000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'SESR_required'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBDataset1."SESR_required"]')
          ParentFont = False
          Style = 'Data'
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          Width = 30.000000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'MODE'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset1."MODE"]')
          ParentFont = False
          Style = 'Data'
        end
        object Memo32: TfrxMemoView
          Align = baLeft
          Left = 496.794717028634200000
          Width = 56.698679260000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'bandwidth'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset1."bandwidth"]')
          ParentFont = False
          Style = 'Data'
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          Left = 1043.273842778634000000
          Width = 80.000000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Working_str'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBDataset1."Working_str"]')
          ParentFont = False
          Style = 'Data'
        end
      end
      object ReportTitle1: TfrxReportTitle
        Height = 50.000000000000000000
        Top = 16.000000000000000000
        Width = 1146.920740731765000000
        object Memo3: TfrxMemoView
          Align = baLeft
          Left = 30.000000000000000000
          Width = 120.000000000000000000
          Height = 50.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#167#1057#1107#1056#1030#1057#1027#1057#8218#1056#1030#1056#1105#1057#8218#1056#181#1056#187#1057#1034#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1057#1026#1056#1105' BER 10^-3, dBm')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          Left = 150.000000000000000000
          Width = 120.000000000000000000
          Height = 50.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#167#1057#1107#1056#1030#1057#1027#1057#8218#1056#1030#1056#1105#1057#8218#1056#181#1056#187#1057#1034#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1057#1026#1056#1105' BER 10^-6, dBm')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Align = baLeft
          Left = 270.000000000000000000
          Width = 64.684408730000000000
          Height = 50.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', dBm')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          Left = 334.684408730000000000
          Width = 74.267284100000000000
          Height = 50.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Align = baLeft
          Left = 408.951692830000000000
          Width = 87.843024200000000000
          Height = 50.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1056#1105#1056#1169' '#1056#1112#1056#1109#1056#1169#1057#1107#1056#187#1057#1039#1057#8224#1056#1105#1056#1105)
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Align = baLeft
          Left = 553.493396290000000000
          Width = 65.000000000000000000
          Height = 50.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'KNG '#1057#8218#1057#1026#1056#181#1056#177', %')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          Left = 683.493396290000000000
          Width = 65.000000000000000000
          Height = 50.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'KNG '#1056#1112#1056#1105#1056#1029' '#1056#1030' '#1056#1110#1056#1109#1056#1169)
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          Left = 748.493396290000000000
          Width = 65.000000000000000000
          Height = 50.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'SESR, %')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          Left = 878.493396290000000000
          Width = 79.058721780000000000
          Height = 50.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1026#1056#1109#1056#1030#1056#181#1056#1029#1057#1034' '#1057#1027#1056#1105#1056#1110#1056#1029#1056#176#1056#187#1056#176', dBm')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          Left = 957.552118070000000000
          Width = 101.418764300000000000
          Height = 50.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8212#1056#176#1056#1111#1056#176#1057#1027' '#1056#1029#1056#176' '#1056#183#1056#176#1056#1112#1056#1105#1057#1026#1056#176#1056#1029#1056#1105#1057#1039', dB')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Align = baLeft
          Left = 618.493396290000000000
          Width = 65.000000000000000000
          Height = 50.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'KNG '#1057#8218#1057#1026#1056#181#1056#177', %')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          Left = 813.493396290000000000
          Width = 65.000000000000000000
          Height = 50.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'SESR '#1057#8218#1057#1026#1056#181#1056#177', %')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Align = baLeft
          Width = 30.000000000000000000
          Height = 50.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#160#1056#181#1056#182#1056#1105#1056#1112)
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Align = baLeft
          Left = 1058.970882370000000000
          Width = 80.000000000000000000
          Height = 50.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034)
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          Left = 496.794717030000000000
          Width = 56.698679260000000000
          Height = 50.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1025#1056#1105#1057#1026#1056#1105#1056#1029#1056#176' '#1056#1111#1056#1109#1056#187#1056#1109#1057#1027#1057#8249' , MHz')
          ParentFont = False
        end
      end
    end
  end
  object frxXLSExport1: TfrxXLSExport
    FileName = 'W:\RPLS_DB tools\Install_DB\dasdasdas.xls'
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    CreationTime = 43355.610747361110000000
    DataOnly = False
    ExportEMF = True
    AsText = False
    Background = True
    FastExport = True
    PageBreaks = True
    EmptyLines = True
    SuppressPageHeadersFooters = False
    Left = 808
    Top = 328
  end
  object frxDBDataset1: TfrxDBDataset
    UserName = 'frxDBDataset1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'RecId=RecId'
      'id=id'
      'THRESHOLD_BER_3=THRESHOLD_BER_3'
      'THRESHOLD_BER_6=THRESHOLD_BER_6'
      'Power_dBm=Power_dBm'
      'BitRate_Mbps=BitRate_Mbps'
      'Modulation_type=Modulation_type'
      'KNG=KNG'
      'KNG_year=KNG_year'
      'SESR=SESR'
      'Rx_Level_dBm=Rx_Level_dBm'
      'FADE_MARGIN_DB=FADE_MARGIN_DB'
      'KNG_required=KNG_required'
      'SESR_required=SESR_required'
      'MODE=MODE'
      'Is_Working=Is_Working'
      'bandwidth=bandwidth'
      'K=K'
      'T_neg_hours=T_neg_hours'
      'T_work_hours=T_work_hours'
      'T_work_percent=T_work_percent'
      'sum=sum'
      'Working_str=Working_str'
      'LinkEndType_ID=LinkEndType_ID')
    DataSet = dxMemData1
    BCDToCurrency = False
    Left = 808
    Top = 272
  end
  object dxMemData1: TdxMemData
    Indexes = <>
    Persistent.Data = {
      5665728FC2F5285C8FFE3F120000000400000003000300696400080000000600
      10005448524553484F4C445F4245525F33000800000006001000544852455348
      4F4C445F4245525F36000800000006000A00506F7765725F64426D0008000000
      06000D00426974526174655F4D6270730014000000010010004D6F64756C6174
      696F6E5F747970650008000000060004004B4E470008000000060009004B4E47
      5F7965617200080000000600050053455352000800000006000D0052785F4C65
      76656C5F64426D000800000006000F00464144455F4D415247494E5F44420008
      00000006000D004B4E475F7265717569726564000800000006000E0053455352
      5F72657175697265640004000000030005004D4F4445000200000005000B0049
      735F576F726B696E67000800000006000A0062616E6477696474680008000000
      06000F00545F776F726B5F70657263656E7400080000000600040073756D00}
    SortOptions = []
    OnCalcFields = dxMemData1CalcFields
    Left = 48
    Top = 264
    object dxMemData1id: TIntegerField
      FieldName = 'id'
    end
    object dxMemData1THRESHOLD_BER_3: TFloatField
      DisplayWidth = 10
      FieldName = 'THRESHOLD_BER_3'
    end
    object dxMemData1THRESHOLD_BER_6: TFloatField
      DisplayWidth = 10
      FieldName = 'THRESHOLD_BER_6'
    end
    object dxMemData1Power_dBm: TFloatField
      DisplayWidth = 10
      FieldName = 'Power_dBm'
    end
    object dxMemData1BitRate_Mbps: TFloatField
      DisplayWidth = 10
      FieldName = 'BitRate_Mbps'
    end
    object dxMemData1Modulation_type: TStringField
      DisplayWidth = 20
      FieldName = 'Modulation_type'
    end
    object dxMemData1KNG: TFloatField
      DisplayWidth = 10
      FieldName = 'KNG'
    end
    object dxMemData1KNG_year: TFloatField
      DisplayWidth = 10
      FieldName = 'KNG_year'
    end
    object dxMemData1SESR: TFloatField
      DisplayWidth = 10
      FieldName = 'SESR'
    end
    object dxMemData1Rx_Level_dBm: TFloatField
      DisplayWidth = 10
      FieldName = 'Rx_Level_dBm'
    end
    object dxMemData1FADE_MARGIN_DB: TFloatField
      DisplayWidth = 10
      FieldName = 'FADE_MARGIN_DB'
    end
    object dxMemData1KNG_required: TFloatField
      DisplayWidth = 10
      FieldName = 'KNG_required'
    end
    object dxMemData1SESR_required: TFloatField
      DisplayWidth = 10
      FieldName = 'SESR_required'
    end
    object dxMemData1MODE: TIntegerField
      DisplayWidth = 10
      FieldName = 'MODE'
    end
    object dxMemData1Is_Working: TBooleanField
      DisplayWidth = 5
      FieldName = 'Is_Working'
    end
    object dxMemData1bandwidth: TFloatField
      DisplayWidth = 10
      FieldName = 'bandwidth'
    end
    object dxMemData1K: TFloatField
      DisplayLabel = 'K '#1087#1088#1086#1089#1090#1086#1103
      FieldKind = fkCalculated
      FieldName = 'K'
      DisplayFormat = '0.00000000'
      Calculated = True
    end
    object dxMemData1T_neg_hours: TFloatField
      FieldKind = fkCalculated
      FieldName = 'T_neg_hours'
      Calculated = True
    end
    object dxMemData1T_work_hours: TFloatField
      FieldKind = fkCalculated
      FieldName = 'T_work_hours'
      Calculated = True
    end
    object dxMemData1T_work_percent: TFloatField
      FieldName = 'T_work_percent'
    end
    object dxMemData1sum: TFloatField
      FieldName = 'sum'
    end
    object dxMemData1Working_str: TStringField
      FieldKind = fkCalculated
      FieldName = 'Working_str'
      Calculated = True
    end
    object dxMemData1LinkEndType_ID: TIntegerField
      FieldName = 'LinkEndType_ID'
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 1000
    Top = 272
    PixelsPerInch = 96
    object cxStyle_selected: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clLime
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object cxStyle1: TcxStyle
    end
  end
end
