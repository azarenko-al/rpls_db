unit u_com;

interface
uses
 shellapi, Forms,  StrUtils, Windows, Variants, SysUtils, Dialogs, ActiveX;





function GetComObject(aDllFileName: string; const CLSID, IID: TGUID; var aObj):
    THandle;

//procedure FreeComObject(aHandle: THandle; var aObj);




implementation

//function ExecAndWait(const ExecuteFile, ParamString : string): boolean; forward;



//-------------------------------------------------------------------
function GetComObject(aDllFileName: string; const CLSID, IID: TGUID; var aObj):
    THandle;
//-------------------------------------------------------------------
type
  TGetObject = function(const CLSID, IID: TGUID; var aObj): HResult; stdcall;
var
  GetObject: TGetObject;
  vFactory: IClassFactory;

begin
  Assert(FileExists(aDllFileName), 'not FileExists: ' + aDllFileName);


  Result:=LoadLibrary(PChar(aDllFileName));
  Integer(aObj):=0;

  if Result >= 32 then
  begin
    @GetObject:=GetProcAddress(Result,'DllGetClassObject');

    if Assigned(GetObject) then
      if GetObject(CLSID,IClassFactory,vFactory) = S_OK then
        vFactory.CreateInstance(nil,IID,aObj);

    vFactory:=nil
  end
end;


end.