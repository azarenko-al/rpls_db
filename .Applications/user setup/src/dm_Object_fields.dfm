object dmObject_fields: TdmObject_fields
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 805
  Top = 412
  Height = 291
  Width = 433
  object qry_Objects: TADOQuery
    CursorType = ctStatic
    EnableBCD = False
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM _objects'
      'ORDER BY name')
    Left = 79
    Top = 10
  end
  object dsObjects: TDataSource
    DataSet = mem_Objects
    Left = 20
    Top = 14
  end
  object qry_Fields: TADOQuery
    AutoCalcFields = False
    CursorType = ctStatic
    EnableBCD = False
    Parameters = <
      item
        Name = 'id'
        DataType = ftInteger
        Precision = 10
        Value = 10
      end>
    SQL.Strings = (
      'SELECT *'
      ''
      'FROM _object_fields'
      ''
      'WHERE object_id=:id'
      ''
      'ORDER BY parent_id, index_')
    Left = 77
    Top = 74
  end
  object dsFields: TDataSource
    DataSet = mem_Fields
    Left = 20
    Top = 78
  end
  object qry_Names: TADOQuery
    Parameters = <>
    Left = 320
    Top = 196
  end
  object mem_Objects: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 144
    Top = 8
  end
  object mem_Fields: TdxMemData
    Indexes = <>
    SortOptions = []
    BeforePost = mem_FieldsBeforePost
    OnFilterRecord = mem_FieldsFilterRecord
    Left = 144
    Top = 72
  end
  object mem_List: TdxMemData
    Indexes = <>
    SortOptions = []
    BeforePost = mem_ListBeforePost
    AfterPost = mem_ListAfterPost
    Left = 144
    Top = 136
  end
  object ds_List: TDataSource
    DataSet = mem_List
    Left = 28
    Top = 142
  end
  object qry_List: TADOQuery
    AutoCalcFields = False
    CursorType = ctStatic
    EnableBCD = False
    Parameters = <
      item
        Name = 'id'
        DataType = ftInteger
        Precision = 10
        Value = 10
      end>
    SQL.Strings = (
      'SELECT *'
      ''
      'FROM _object_fields'
      ''
      'WHERE object_id=:id'
      ''
      'ORDER BY parent_id, index_')
    Left = 85
    Top = 138
  end
  object qry_Temp: TADOQuery
    AutoCalcFields = False
    CursorType = ctStatic
    EnableBCD = False
    Parameters = <
      item
        Name = 'id'
        DataType = ftInteger
        Precision = 10
        Value = 10
      end>
    SQL.Strings = (
      'SELECT *'
      ''
      'FROM _object_fields'
      ''
      'WHERE object_id=:id'
      ''
      'ORDER BY parent_id, index_')
    Left = 229
    Top = 130
  end
end
