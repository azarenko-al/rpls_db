unit Login_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 11.04.2018 14:11:48 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\common dll XE\DB_LOGIN\Project20 (1)
// LIBID: {1987EA2D-0721-4438-8390-27743EA2C2FC}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (stdvcl40.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Windows, Classes, Variants, Graphics, OleServer, ActiveX;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  LoginMajorVersion = 1;
  LoginMinorVersion = 0;

  LIBID_Login: TGUID = '{1987EA2D-0721-4438-8390-27743EA2C2FC}';

  IID_ILoginX: TGUID = '{0605141F-C9F3-4DDC-A77C-0C8E0837352C}';
  CLASS_LoginX: TGUID = '{D81654A1-7E7D-4FE0-80BC-E44E70CF92A9}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ILoginX = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  LoginX = ILoginX;


// *********************************************************************//
// Interface: ILoginX
// Flags:     (256) OleAutomation
// GUID:      {0605141F-C9F3-4DDC-A77C-0C8E0837352C}
// *********************************************************************//
  ILoginX = interface(IUnknown)
    ['{0605141F-C9F3-4DDC-A77C-0C8E0837352C}']
    function Exec(const aRegPath: WideString; const aConnectionObject: IDispatch): HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoLoginX provides a Create and CreateRemote method to          
// create instances of the default interface ILoginX exposed by              
// the CoClass LoginX. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoLoginX = class
    class function Create: ILoginX;
    class function CreateRemote(const MachineName: string): ILoginX;
  end;

implementation

uses ComObj;

class function CoLoginX.Create: ILoginX;
begin
  Result := CreateComObject(CLASS_LoginX) as ILoginX;
end;

class function CoLoginX.CreateRemote(const MachineName: string): ILoginX;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_LoginX) as ILoginX;
end;

end.

