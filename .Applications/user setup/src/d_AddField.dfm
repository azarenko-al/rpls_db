inherited dlg_AddField: Tdlg_AddField
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = 'dlg_AddField'
  ClientHeight = 176
  ClientWidth = 438
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 16
    Top = 40
    Width = 77
    Height = 13
    Caption = #1048#1084#1103' '#1087#1086#1083#1103' '#1074' '#1041#1044
  end
  object Label2: TLabel [1]
    Left = 16
    Top = 88
    Width = 19
    Height = 13
    Caption = #1058#1080#1087
  end
  object Label3: TLabel [2]
    Left = 16
    Top = 112
    Width = 129
    Height = 13
    Caption = #1056#1072#1079#1084#1077#1088' ('#1090#1077#1082#1089#1090#1086#1074#1099#1077' '#1087#1086#1083#1103')'
  end
  object Label4: TLabel [3]
    Left = 16
    Top = 64
    Width = 118
    Height = 13
    Caption = #1048#1084#1103' '#1087#1086#1083#1103' '#1074' '#1087#1088#1086#1075#1088#1072#1084#1084#1077
  end
  object lb_Error: TLabel [4]
    Left = 16
    Top = 184
    Width = 3
    Height = 13
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  inherited pn_Buttons: TPanel
    Top = 141
    Width = 438
    inherited Bevel1: TBevel
      Width = 438
    end
    inherited pn_but_aligned: TPanel
      Left = 270
    end
  end
  inherited pn_Top_: TPanel
    Width = 438
    Height = 30
    Constraints.MaxHeight = 30
    Visible = True
    inherited Bevel2: TBevel
      Top = 26
      Width = 438
      Height = 4
    end
    inherited pn_Header: TPanel
      Width = 438
      Height = 26
      inherited lb_Action: TLabel
        Top = 2
      end
    end
  end
  object ed_DB_name: TEdit [7]
    Left = 165
    Top = 36
    Width = 261
    Height = 21
    TabOrder = 2
    OnChange = ed_DB_nameChange
  end
  object ed_size: TEdit [8]
    Left = 165
    Top = 108
    Width = 261
    Height = 21
    TabOrder = 5
    Text = '255'
  end
  object ed_prog_name: TEdit [9]
    Left = 165
    Top = 60
    Width = 261
    Height = 21
    TabOrder = 3
  end
  object cb_type: TComboBox [10]
    Left = 165
    Top = 84
    Width = 261
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 4
    Items.Strings = (
      #1057#1087#1080#1089#1086#1082
      #1055#1088#1086#1089#1090#1086#1081' '#1090#1077#1082#1089#1090
      #1062#1077#1083#1086#1077
      #1057' '#1087#1083#1072#1074#1072#1102#1097#1077#1081' '#1079#1072#1087#1103#1090#1086#1081' ('#1076#1088#1086#1073#1085#1086#1077')')
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 385
  end
  inherited FormStorage1: TFormStorage
    Left = 356
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 327
  end
end
