program rpls_view_setup_user;

uses
  Forms,
  f_Main in 'src\f_Main.pas' {frm_Main},
  dm_Object_fields in 'src\dm_Object_fields.pas' {dmObject_fields: TDataModule},
  d_AddDBField in 'src\d_AddDBField.pas' {dlg_AddDBField},
  fr_Object_fields in 'src\fr_Object_fields.pas' {frm_Object_fields},
  d_Wizard in '..\..\common\d_Wizard.pas' {dlg_Wizard},
  d_AddField in 'src\d_AddField.pas' {dlg_AddField},
  dm_App in 'src\dm_App.pas' {dmApp: TDataModule},
  u_com in 'src\u_COM.pas',
  Login_TLB in 'src\activeX\Login_TLB.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TdmApp, dmApp);
  Application.CreateForm(Tfrm_Main, frm_Main);
  Application.Run;
end.
