//============================================================================//
//  ����� ���������� � �������������� ���� �������������
//
//============================================================================//
unit d_ViewField;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, ADODB, DBCtrls, Mask, dxCntner, dxEditor, dxEdLib,
  dxDBELib, dxExEdtr, dxDBEdtr,
  u_Common,
  u_func_db,
  dm_Views, Grids, DBGrids, ExtCtrls, dxInspRw, dxDBInRw, dxInspct,
  dxDBInsp;


type
  Tdlg_ViewField = class(TForm)
    Label6: TLabel;
    DBLookupComboBox1: TDBLookupComboBox;
    Panel1: TPanel;
    buOK: TButton;
    buCancel: TButton;
    dxDBInspector1: TdxDBInspector;
    dxDBInspector1Row1: TdxInspectorDBRow;
    dxDBInspector1Row2: TdxInspectorDBRow;
    dxDBInspector1Row3: TdxInspectorDBRow;
    dxDBInspector1Row4: TdxInspectorDBMemoRow;
    dxDBInspector1Row5: TdxInspectorDBCheckRow;
    dxDBInspector1Row6: TdxInspectorDBRow;
    procedure buOKClick(Sender: TObject);
  public
    class function AddField():boolean;
    class function EditField():boolean;
  end;


implementation

{$R *.dfm}

//============================================================================//
//  ��������� ���������� ����
//
//============================================================================//
class function Tdlg_ViewField.AddField ():boolean;
var
  iMaxValue: integer;
begin
  with Tdlg_ViewField.Create(Application) do
  try
    result:=(ShowModal=mrOK);
  finally
    Free;
  end;
end;

//============================================================================//
//  ��������� ��������������
//
//============================================================================//
class function Tdlg_ViewField.EditField ():boolean;
begin
  with Tdlg_ViewField.Create(Application) do
  try
    Caption:=STR_CAPTION_FIELD_EDIT;
    result:=(ShowModal=mrOK);
  finally
    Free;
  end;
end;

//============================================================================//
//  �������� ���������
//
//============================================================================//
procedure Tdlg_ViewField.buOKClick(Sender: TObject);
begin
  db_PostDataset (dmViews.qry_ViewField);
  Modalresult:=mrOK;
end;


end.
