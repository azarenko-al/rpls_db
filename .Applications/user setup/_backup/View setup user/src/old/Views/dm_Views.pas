unit dm_Views;

interface

uses
  SysUtils, Classes, DB, ADODB,
  u_Common,
  u_func_DB;

type
  TdmViews = class(TDataModule)
    qry_ViewFields: TADOQuery;
    dsViewFields: TDataSource;
    dsViews: TDataSource;
    qry_Objects: TADOQuery;
    dsObjects: TDataSource;
    qry_Views: TADOQuery;
    qry_Object_Fields: TADOQuery;
    qry_ViewField: TADOQuery;
    ds_Object_Fields: TDataSource;
    ds_ViewField: TDataSource;
    ADOConnection: TADOConnection;
    procedure qry_ViewsAfterScroll(DataSet: TDataSet);
    procedure qry_ViewsNewRecord(DataSet: TDataSet);
    procedure qry_ObjectsAfterScroll(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    procedure ViewFieldAdd;
    procedure ViewFieldEdit (aID: integer);

    procedure ViewAdd;
    procedure ViewDelete;
    procedure ViewFieldDelete;
  end;

var
  dmViews: TdmViews;

implementation
{$R *.dfm}

uses dm_main;

const
  TBL_VIEWS = '_views';
  TBL_VIEW_FIELDS = '_view_fields';

  SQL_SELECT_VIEWS =
    'SELECT * FROM ' + TBL_VIEWS +
    ' WHERE (object_id=:object_id)';

  SQL_SELECT_VIEW_FIELDS =
    'SELECT a.*, '+
    '       b.name, b.caption '+
    ' FROM _view_fields a, _object_fields b '+
    ' WHERE (a.view_id=:id) AND (b.id=a.field_id) '+
    ' ORDER BY b.parent_id, a.index_';

  SQL_SELECT_FIELD_ADD =
    'SELECT id, name FROM _object_fields '+
    ' WHERE (object_id=%d) AND '+
    ' (id NOT IN (SELECT field_id FROM _view_fields '+
    ' WHERE (view_id=%d))) '+
    ' ORDER BY index_';

  SQL_SELECT_FIELD_EDIT =
    'SELECT b.id, b.name FROM _view_fields a, _object_fields b '+
    ' WHERE (a.id=:id) AND (b.id=a.field_id) ';


procedure TdmViews.qry_ViewsNewRecord(DataSet: TDataSet);
begin
// ��� ������� ����� ������ ������������� ������� �������� ��������� �����

  qry_Views.FieldByName('object_id').Value:=
      qry_Objects.FieldByName(FLD_ID).AsInteger;
//  qry_Views.FieldByName('user_id').Clear;
end;


//============================================================================//
// ���������� �������������
//
//============================================================================//
procedure TdmViews.ViewAdd;
begin
//  qry_Views1.Open;
  qry_Views.Insert;

end;

//============================================================================//
// �������� �������������
//
//============================================================================//
procedure TdmViews.ViewDelete;
begin
  qry_Views.Delete;
end;

//============================================================================//
// �������������� ���� �������������
//
//============================================================================//
procedure TdmViews.ViewFieldEdit (aID: integer);
begin
  db_OpenQuery (qry_ViewField, qry_ViewField.SQL.Text,
                [db_Par (FLD_ID, aID)]  );

  db_OpenQuery (qry_Object_Fields, SQL_SELECT_FIELD_EDIT,
                [db_Par (FLD_ID, aID)]  );

end;

//============================================================================//
// ���������� ���� �������������
//
//============================================================================//
procedure TdmViews.ViewFieldAdd;
var
  iObjectID,iMaxValue,iViewID: integer;
begin
  iViewID:=qry_Views.FieldByName(FLD_ID).AsInteger;
  iObjectID:=qry_Views.FieldByName('object_id').AsInteger;

  iMaxValue:=gl_DB.GetMaxFieldValue (
                                  TBL_VIEW_FIELDS,
                                  FLD_INDEX,
                                  [db_Par (FLD_VIEW_ID, iViewID)]
                                  );

  with qry_ViewField do begin
    Open;
    Insert;
//    FieldByName('user_id').Clear;
    FieldByName('view_id').Value:=iViewID;
    FieldByName('index_').Value:=iMaxValue+1;
  end;

  db_OpenQuery(qry_Object_Fields ,
               Format(SQL_SELECT_FIELD_ADD, [iObjectID, iViewID]));
end;
          
//============================================================================//
// �������� ���� �������������
//
//============================================================================//
procedure TdmViews.ViewFieldDelete;
begin
  gl_DB.DeleteRecord (TBL_VIEW_FIELDS,
                      qry_ViewFields.FieldByName(FLD_ID).AsInteger );
  db_RefreshQuery (qry_ViewFields);
end;


procedure TdmViews.qry_ObjectsAfterScroll(DataSet: TDataSet);
begin
  db_OpenQuery (qry_Views, SQL_SELECT_VIEWS,
     [db_Par ('object_id', qry_Objects.FieldByName(FLD_ID).AsInteger) ]);
end;


procedure TdmViews.qry_ViewsAfterScroll(DataSet: TDataSet);
begin
  db_OpenQuery(qry_ViewFields, SQL_SELECT_VIEW_FIELDS,
               [db_Par(FLD_ID, qry_Views.FieldByName(FLD_ID).AsInteger)]);
end;


procedure TdmViews.DataModuleCreate(Sender: TObject);
begin
  db_SetComponentADOConnection(Self, dmMain.ADOConnection);

//  qry_Views.SQL.Text:=SQL_SELECT_VIEWS;
  qry_ViewFields.SQL.Text:=SQL_SELECT_VIEW_FIELDS;

  qry_Objects.Open;
//  qry_Views1.Open;
//  qry_ViewFields.Open;

end;


end.





