unit fr_Views;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxExEdtr, Menus, dxDBTLCl, dxGrClms, ExtCtrls, dxTL, dxDBCtrl,
  dxDBGrid, dxCntner, AppEvnts,
  ActnList, XPStyleActnCtrls, ActnMan, ImgList,
  dxDBTL, Grids, DBGrids,

  d_ViewField,
  dm_Views,
  dm_main,
  u_func_db,
  u_common;


type
  Tframe_Views = class(TForm)
    dbgObjectViews: TdxDBGrid;
    Splitter1: TSplitter;
    pn_Main: TPanel;
    dbgViewFields: TdxDBGrid;
    dbgViewFieldsColumn1: TdxDBGridColumn;
    dbgViewFieldsColumn2: TdxDBGridColumn;
    dbgViewFieldsColumn3: TdxDBGridColumn;
    dbgViews: TdxDBGrid;
    Splitter2: TSplitter;
    pmViews: TPopupMenu;
    N25: TMenuItem;
    N26: TMenuItem;
    pmViewFields: TPopupMenu;
    Add1: TMenuItem;
    Edit1: TMenuItem;
    Delete1: TMenuItem;
    Up1: TMenuItem;
    Down1: TMenuItem;
    Panel1: TPanel;
    ilMain: TImageList;
    acmMain: TActionManager;
    ActionList1: TActionList;
    act_ViewAdd: TAction;
    act_ViewDel: TAction;
    act_FieldAdd: TAction;
    act_FieldEdit: TAction;
    act_FieldDel: TAction;
    act_FieldUp: TAction;
    act_FieldDown: TAction;
    dbgViewsName: TdxDBGridMaskColumn;
    dbgViewsForRecord: TdxDBGridCheckColumn;
    dbgViewsIsDefault: TdxDBGridCheckColumn;
    dbgObjectViewsColumn1: TdxDBGridColumn;
    dbgObjectViewsColumn2: TdxDBGridColumn;
    dbgViewsColumn4: TdxDBGridColumn;
    procedure FormCreate(Sender: TObject);
    procedure act_ViewAddExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frame_Views: Tframe_Views;

implementation
{$R *.dfm}


procedure Tframe_Views.FormCreate(Sender: TObject);
begin
  if not Assigned(dmViews) then
    dmViews:=TdmViews.Create(Application);

  pn_Main.Align:=alCLient;
  dbgViewFields.Align:=alCLient;
end;


procedure Tframe_Views.act_ViewAddExecute(Sender: TObject);
begin
  if Sender=act_ViewAdd then begin
    dmViews.ViewAdd;
    dbgViews.ShowEditor;
  end else
    // else

  if Sender=act_ViewDel then begin
    if MessageDlg(STR_DELETE_VIEW,mtConfirmation,[mbYes,mbNo],0)=mrYes then
      dmViews.ViewDelete;
  end else

  if Sender=act_FieldAdd then
  begin
    dmViews.ViewFieldAdd();
    if Tdlg_ViewField.AddField () then
    begin
      db_RefreshQuery (dmViews.qry_ViewFields);
      dmViews.qry_ViewFields.Last;
    end;

  end else

  if Sender=act_FieldEdit then
  begin
    dmViews.ViewFieldEdit (dmViews.qry_ViewFields.FieldByName(FLD_ID).AsInteger);
    if Tdlg_ViewField.EditField() then
      db_RefreshQuery (dmViews.qry_ViewFields, dmViews.qry_ViewFields.FieldByName(FLD_ID).AsInteger);

    //dmViews.ViewFieldEdit else
  end else

  if Sender=act_FieldDel then
  begin
    if MessageDlg(STR_DELETE_VIEW_FIELD,mtConfirmation,[mbYes,mbNo],0)=mrYes then
      dmViews.ViewFieldDelete;
  end else

  if Sender=act_FieldUp then
    gl_DB.MoveRecord (dmViews.qry_ViewFields, True, TBL_VIEW_FIELDS, FLD_INDEX) else

  if Sender=act_FieldDown then
    gl_DB.MoveRecord (dmViews.qry_ViewFields, False, TBL_VIEW_FIELDS, FLD_INDEX) else
 ;

end;



end.
