object dmViews: TdmViews
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 378
  Top = 206
  Height = 361
  Width = 324
  object qry_ViewFields: TADOQuery
    Connection = ADOConnection
    CursorType = ctStatic
    EnableBCD = False
    Parameters = <
      item
        Name = 'id'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT a.*,'
      '       b.name, b.caption'
      ''
      'FROM _view_fields a, _object_fields b'
      'WHERE (a.view_id=:id) AND (b.id=a.field_id)'
      ''
      'ORDER BY b.parent_id, a.index_')
    Left = 165
    Top = 78
  end
  object dsViewFields: TDataSource
    DataSet = qry_ViewFields
    Left = 165
    Top = 18
  end
  object dsViews: TDataSource
    DataSet = qry_Views
    Left = 95
    Top = 18
  end
  object qry_Objects: TADOQuery
    Connection = ADOConnection
    CursorType = ctStatic
    AfterScroll = qry_ObjectsAfterScroll
    EnableBCD = False
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM _objects'
      'ORDER BY name')
    Left = 19
    Top = 78
  end
  object dsObjects: TDataSource
    DataSet = qry_Objects
    Left = 20
    Top = 18
  end
  object qry_Views: TADOQuery
    Active = True
    Connection = ADOConnection
    CursorType = ctStatic
    AfterScroll = qry_ViewsAfterScroll
    OnNewRecord = qry_ViewsNewRecord
    EnableBCD = False
    Parameters = <
      item
        Name = 'object_id'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM _views'
      'WHERE (object_id=:object_id) ')
    Left = 94
    Top = 78
  end
  object qry_Object_Fields: TADOQuery
    Connection = ADOConnection
    CursorType = ctStatic
    EnableBCD = False
    Parameters = <
      item
        Name = 'obj_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'view_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'SELECT id, name '
      'FROM _object_fields '
      ' WHERE (object_id=:obj_id) AND '
      '                (id NOT IN (SELECT field_id '
      '                                   FROM _view_fields '
      
        '                                    WHERE (view_id=:view_id) AND' +
        ' (user_id=0) )) '
      'ORDER BY index_')
    Left = 72
    Top = 248
  end
  object qry_ViewField: TADOQuery
    Connection = ADOConnection
    CursorType = ctStatic
    EnableBCD = False
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = -1
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM _view_fields'
      ' WHERE (id=:id)')
    Left = 188
    Top = 252
  end
  object ds_Object_Fields: TDataSource
    DataSet = qry_Object_Fields
    Left = 72
    Top = 196
  end
  object ds_ViewField: TDataSource
    DataSet = qry_ViewField
    Left = 188
    Top = 200
  end
  object ADOConnection: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=SQL_ONEGA'
    LoginPrompt = False
    Left = 256
    Top = 20
  end
end
