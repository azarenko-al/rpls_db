unit fr_View_setup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxExEdtr, Grids, DBGrids, dxInspRw, dxDBInRw, dxInspct,
  dxDBInsp, ActnList, ImgList, XPStyleActnCtrls, ActnMan, AppEvnts, Menus,
  dxDBTLCl, dxDBTL, dxTL, dxDBCtrl, dxDBGrid, dxCntner, ExtCtrls,
  ComCtrls, StdActns,
  u_Globals,
  u_func_DB, Placemnt
  ;

type
  TForm3 = class(TForm)
    pn_Top: TPanel;
    spl4: TSplitter;
    dx_Objects: TdxDBGrid;
    dx_ObjectsColumn1: TdxDBGridColumn;
    dx_ObjectsColumn4: TdxDBGridColumn;
    Panel1: TPanel;
    mnu_Objects: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    mnu_Fields: TPopupMenu;
    N5: TMenuItem;
    N3: TMenuItem;
    N9: TMenuItem;
    N4: TMenuItem;
    N8: TMenuItem;
    N10: TMenuItem;
    N7: TMenuItem;
    N6: TMenuItem;
    N20: TMenuItem;
    N19: TMenuItem;
    ActionList1: TActionList;
    act_Field_Reindex: TAction;
    act_Object_add: TAction;
    act_Object_del: TAction;
    act_Field_Add: TAction;
    act_Field_Del: TAction;
    act_Field_Down: TAction;
    act_Field_Up: TAction;
    act_FieldDatabaseDel: TAction;
    act_FieldDatabaseAdd: TAction;
    dx_Fields: TdxDBTreeList;
    col_Caption: TdxDBTreeListColumn;
    col_Name: TdxDBTreeListColumn;
    col_Index: TdxDBTreeListColumn;
    col_Parent_id: TdxDBTreeListColumn;
    col_Field_Type: TdxDBTreeListPickColumn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    dxDBInspector1: TdxDBInspector;
    dxDBInspector1name: TdxInspectorDBMaskRow;
    dxDBInspector1type: TdxInspectorDBMaskRow;
    dxDBInspector1caption: TdxInspectorDBMaskRow;
    dxDBInspector1enabled: TdxInspectorDBCheckRow;
    act_Field_Left: TAction;
    N11: TMenuItem;
    FormStorage1: TFormStorage;
    dxDBInspector1Row6: TdxInspectorDBCheckRow;
    col_IsFolder: TdxDBTreeListCheckColumn;
    test1: TMenuItem;
    procedure act_Field_ReindexExecute(Sender: TObject);
    procedure dx_ObjectsExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dx_FieldsCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
    procedure dx_FieldsDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure test1Click(Sender: TObject);
  private
    procedure DoFieldDatabaseAddExecute();
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

uses dm_View_setup;

{$R *.dfm}

const
  STR_DELETE_OBJECT = '�� ������� � ���, ��� ����� ������� ���� ������?';
  STR_DELETE_FIELD  = '�� ������� � ���, ��� ����� ������� ��� ����?';


procedure TForm3.FormCreate(Sender: TObject);
begin
  pn_Top.Align:=alClient;
  dx_Fields.Align:=alClient;

  col_Field_Type.Items.Text:=
     'text'  + CRLF +
     'list'  + CRLF +
     'float' + CRLF +
     'int'   + CRLF +
     'comments' + CRLF +
     'degree'+ CRLF +
     'date' ;// + CRLF ;

end;


//===========================================================================//
//  ������� ���� � ��
//
//===========================================================================//
procedure TForm3.DoFieldDatabaseAddExecute();

var mTable, mFieldname, mType: string;
    mPrec, mScale: integer;
begin
{
  mTable:=dx_Objects.DataSource.DataSet.FieldByName(FLD_TABLENAME).AsString;
  if Tdlg_AddDBField.AddField(mFieldname,mType,mPrec,mScale) and
    db_AddTableField (dmMain.ADOConnection, mTable,mFieldname,mType,mPrec,mScale) then
      data_Objects.FieldAdd;
      //actFieldAdd.Execute;
      }
end;



//--------------------------------------------------------------------
procedure TForm3.act_Field_ReindexExecute(Sender: TObject);
//--------------------------------------------------------------------
begin
//  MsgDlg (Action.Name);

  //---------------------------
  // Objects
  //---------------------------
  if Sender= act_Object_add then dmView_setup.ObjectAdd else
  if Sender= act_Object_Del then begin
    if ConfirmDlg(STR_DELETE_OBJECT) then dmView_setup.ObjectDelete;
  end else

  //---------------------------
  // Fields
  //---------------------------
  if Sender= act_Field_Reindex then dmView_setup.FieldReindex else

  if Sender= act_Field_Add then dmView_setup.FieldAdd else
  if Sender= act_Field_Del then begin
    if ConfirmDlg(STR_DELETE_FIELD) then dmView_setup.FieldDelete;
  end else

  if Sender= act_Field_Up   then dmView_setup.FieldMoveUp else
  if Sender= act_Field_Down then dmView_setup.FieldMoveDown else
  if Sender= act_Field_Left then dmView_setup.FieldMoveLeft else

  if Sender= act_FieldDatabaseAdd then DoFieldDatabaseAddExecute else
  if Sender= act_FieldDatabaseDel then
  begin
    if ConfirmDlg(STR_DELETE_FIELD) then
       with dmView_setup do
         if gl_DB.DropTableField (
                               qry_Objects.FieldByName(FLD_TABLE_NAME).AsString,
                               qry_Fields.FieldByName(FLD_FIELD_NAME).AsString)
         then FieldDelete;
  end else
  ;
end;

//--------------------------------------------------------------------
procedure TForm3.dx_ObjectsExit(Sender: TObject);
//--------------------------------------------------------------------
begin
  if Sender is TdxDBGrid then
    db_PostDataset((Sender as TdxDBGrid).DataSource.DataSet);

  if Sender is TdxDBTreeList  then
    db_PostDataset((Sender as TdxDBTreeList).DataSource.DataSet);

end;


procedure TForm3.dx_FieldsCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
  ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
  ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
  var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
  var ADone: Boolean);
begin
  if ANode.HasChildren then
    AFont.Style:=AFont.Style + [fsBold];

end;

procedure TForm3.dx_FieldsDragDrop(Sender, Source: TObject; X, Y: Integer);
begin
 //  with dx_Fields.DataSource.DataSet do begin
  //   Close; Open;
  // end;  
end;

procedure TForm3.test1Click(Sender: TObject);
begin
  dx_Fields.FullExpand;

end;

end.
