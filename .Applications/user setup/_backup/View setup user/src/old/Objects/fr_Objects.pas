unit fr_Objects;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxExEdtr, Grids, DBGrids, dxInspRw, dxDBInRw, dxInspct,
  dxDBInsp, ActnList, ImgList, XPStyleActnCtrls, ActnMan, AppEvnts, Menus,
  dxDBTLCl, dxDBTL, dxTL, dxDBCtrl, dxDBGrid, dxCntner, ExtCtrls,
  ComCtrls, StdActns,
  u_Globals,
  u_func_DB,
  u_func_dlg,
  Placemnt
  ;

type
  Tframe_Objects = class(TForm)
    pn_Top: TPanel;
    spl4: TSplitter;
    dx_Objects: TdxDBGrid;
    dx_ObjectsColumn1: TdxDBGridColumn;
    dx_ObjectsColumn4: TdxDBGridColumn;
    Panel1: TPanel;
    mnu_Objects: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    mnu_Fields: TPopupMenu;
    N5: TMenuItem;
    N3: TMenuItem;
    N9: TMenuItem;
    N4: TMenuItem;
    N8: TMenuItem;
    N10: TMenuItem;
    N7: TMenuItem;
    N6: TMenuItem;
    N20: TMenuItem;
    N19: TMenuItem;
    ActionList1: TActionList;
    act_Field_Reindex: TAction;
    act_Object_add: TAction;
    act_Object_del: TAction;
    act_Field_Add: TAction;
    act_Field_Del: TAction;
    act_Field_Down: TAction;
    act_Field_Up: TAction;
    act_FieldDatabaseDel: TAction;
    act_FieldDatabaseAdd: TAction;
    dx_Fields: TdxDBTreeList;
    col_Caption: TdxDBTreeListColumn;
    col_Name: TdxDBTreeListColumn;
    col_Index: TdxDBTreeListColumn;
    col_Parent_id: TdxDBTreeListColumn;
    FileExit1: TFileExit;
    col_Field_Type: TdxDBTreeListPickColumn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    dxDBInspector1: TdxDBInspector;
    dxDBInspector1name: TdxInspectorDBMaskRow;
    dxDBInspector1type: TdxInspectorDBMaskRow;
    dxDBInspector1caption: TdxInspectorDBMaskRow;
    dxDBInspector1items: TdxInspectorDBMemoRow;
    dxDBInspector1enabled: TdxInspectorDBCheckRow;
    act_Field_Left: TAction;
    N11: TMenuItem;
    FormStorage1: TFormStorage;
    dxDBInspector1Row6: TdxInspectorDBCheckRow;
    col_IsFolder: TdxDBTreeListCheckColumn;
    test1: TMenuItem;
    col_XREF_TableName: TdxDBTreeListColumn;
    procedure act_Field_ReindexExecute(Sender: TObject);
    procedure dx_ObjectsExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dx_FieldsCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
    procedure dx_FieldsDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure test1Click(Sender: TObject);
  private
    procedure DoFieldDatabaseAddExecute();
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frame_Objects: Tframe_Objects;

implementation
uses dm_Objects, dm_Views;

{$R *.dfm}

const
  STR_DELETE_OBJECT = '�� ������� � ���, ��� ����� ������� ���� ������?';
  STR_DELETE_FIELD  = '�� ������� � ���, ��� ����� ������� ��� ����?';


procedure Tframe_Objects.FormCreate(Sender: TObject);
begin
  if not Assigned(dmObjects) then
    dmObjects:=TdmObjects.Create(Application);

  pn_Top.Align:=alClient;
  dx_Fields.Align:=alClient;

  col_Field_Type.Items.Text:=
     'text'  + CRLF +
     'list'  + CRLF +
     'FileName' + CRLF +
     'FileDir'  + CRLF +
     'float' + CRLF +
     'int'   + CRLF +
     'xref'  + CRLF +
     'comments' + CRLF +
     'degree'+ CRLF +
     'date' ;// + CRLF ;

end;


//===========================================================================//
//  ������� ���� � ��
//
//===========================================================================//
procedure Tframe_Objects.DoFieldDatabaseAddExecute();

var mTable, mFieldname, mType: string;
    mPrec, mScale: integer;
begin
{
  mTable:=dx_Objects.DataSource.DataSet.FieldByName(FLD_TABLENAME).AsString;
  if Tdlg_AddDBField.AddField(mFieldname,mType,mPrec,mScale) and
    db_AddTableField (dmMain.ADOConnection, mTable,mFieldname,mType,mPrec,mScale) then
      data_Objects.FieldAdd;
      //actFieldAdd.Execute;
      }
end;



//--------------------------------------------------------------------
procedure Tframe_Objects.act_Field_ReindexExecute(Sender: TObject);
//--------------------------------------------------------------------
begin
//  MsgDlg (Action.Name);

  //---------------------------
  // Objects
  //---------------------------
  if Sender= act_Object_add then dmObjects.ObjectAdd else
  if Sender= act_Object_Del then begin
    if ConfirmDlg(STR_DELETE_OBJECT) then dmObjects.ObjectDelete;
  end else

  //---------------------------
  // Fields
  //---------------------------
  if Sender= act_Field_Reindex then dmObjects.FieldReindex else

  if Sender= act_Field_Add then dmObjects.FieldAdd else
  if Sender= act_Field_Del then begin
    if ConfirmDlg(STR_DELETE_FIELD) then dmObjects.FieldDelete;
  end else

  if Sender= act_Field_Up   then dmObjects.FieldMoveUp else
  if Sender= act_Field_Down then dmObjects.FieldMoveDown else
  if Sender= act_Field_Left then dmObjects.FieldMoveLeft else

  if Sender= act_FieldDatabaseAdd then DoFieldDatabaseAddExecute else
  if Sender= act_FieldDatabaseDel then
  begin
    if ConfirmDlg(STR_DELETE_FIELD) then
       with dmObjects do
         if gl_DB.DropTableField (
                               qry_Objects.FieldByName(FLD_TABLE_NAME).AsString,
                               qry_Fields.FieldByName(FLD_FIELD_NAME).AsString)
         then FieldDelete;
  end else
  ;
end;

//--------------------------------------------------------------------
procedure Tframe_Objects.dx_ObjectsExit(Sender: TObject);
//--------------------------------------------------------------------
begin
  if Sender is TdxDBGrid then
    db_PostDataset((Sender as TdxDBGrid).DataSource.DataSet);

  if Sender is TdxDBTreeList  then
    db_PostDataset((Sender as TdxDBTreeList).DataSource.DataSet);

end;


procedure Tframe_Objects.dx_FieldsCustomDrawCell(Sender: TObject; ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
  ASelected, AFocused, ANewItemRow: Boolean; var AText: String;  var AColor: TColor; AFont: TFont; var AAlignment: TAlignment; var ADone: Boolean);
begin
  if ANode.HasChildren then
    AFont.Style:=AFont.Style + [fsBold];
end;


procedure Tframe_Objects.dx_FieldsDragDrop(Sender, Source: TObject; X, Y: Integer);
begin
 //  with dx_Fields.DataSource.DataSet do begin
  //   Close; Open;
  // end;  
end;

procedure Tframe_Objects.test1Click(Sender: TObject);
begin
  dx_Fields.FullExpand;

end;

end.
