object frame_Objects: Tframe_Objects
  Left = 277
  Top = 175
  Width = 637
  Height = 482
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1087#1086#1083#1077#1081
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 101
  TextHeight = 13
  object pn_Top: TPanel
    Left = 0
    Top = 65
    Width = 629
    Height = 152
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object spl4: TSplitter
      Left = 177
      Top = 0
      Height = 152
    end
    object dx_Objects: TdxDBGrid
      Left = 0
      Top = 0
      Width = 177
      Height = 152
      Bands = <
        item
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      SummaryGroups = <>
      SummarySeparator = ', '
      Align = alLeft
      PopupMenu = mnu_Objects
      TabOrder = 0
      OnExit = dx_ObjectsExit
      DataSource = dmObjects.dsObjects
      Filter.Criteria = {00000000}
      LookAndFeel = lfFlat
      OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEditing, edgoEnterShowEditor, edgoTabThrough, edgoVertThrough]
      OptionsDB = [edgoCanAppend, edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoUseBookmarks]
      OptionsView = [edgoBandHeaderWidth, edgoDrawEndEllipsis, edgoIndicator, edgoUseBitmap]
      object dx_ObjectsColumn1: TdxDBGridColumn
        Caption = #1048#1084#1103' '#1086#1073#1098#1077#1082#1090#1072
        Width = 101
        BandIndex = 0
        RowIndex = 0
        FieldName = 'name'
      end
      object dx_ObjectsColumn4: TdxDBGridColumn
        Width = 20
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Id'
      end
    end
    object dx_Fields: TdxDBTreeList
      Left = 204
      Top = 0
      Width = 425
      Height = 152
      Bands = <
        item
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      KeyField = 'id'
      ParentField = 'parent_id'
      Align = alRight
      DragMode = dmAutomatic
      PopupMenu = mnu_Fields
      TabOrder = 1
      OnDragDrop = dx_FieldsDragDrop
      OnExit = dx_ObjectsExit
      DataSource = dmObjects.dsFields
      LookAndFeel = lfFlat
      OptionsBehavior = [etoAutoDragDrop, etoAutoDragDropCopy, etoDragExpand, etoDragScroll, etoEditing, etoEnterShowEditor, etoImmediateEditor, etoStoreToRegistry, etoTabs, etoTabThrough]
      OptionsDB = [etoCancelOnExit, etoCanDelete, etoCanInsert, etoCanNavigation, etoCheckHasChildren, etoConfirmDelete, etoLoadAllRecords]
      OptionsView = [etoBandHeaderWidth, etoHotTrack, etoIndicator, etoUseBitmap, etoUseImageIndexForSelected]
      PaintStyle = psOutlook
      ShowGrid = True
      TreeLineColor = clGrayText
      OnCustomDrawCell = dx_FieldsCustomDrawCell
      object col_Caption: TdxDBTreeListColumn
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'caption'
      end
      object col_Name: TdxDBTreeListColumn
        Width = 107
        BandIndex = 0
        RowIndex = 0
        FieldName = 'name'
      end
      object col_Field_Type: TdxDBTreeListPickColumn
        BandIndex = 0
        RowIndex = 0
        FieldName = 'type'
        Items.Strings = (
          'text'
          'list'
          'int')
        ItemsSorted = True
        DropDownRows = 30
      end
      object col_Index: TdxDBTreeListColumn
        BandIndex = 0
        RowIndex = 0
        FieldName = 'index_'
      end
      object col_Parent_id: TdxDBTreeListColumn
        BandIndex = 0
        RowIndex = 0
        FieldName = 'parent_id'
      end
      object col_IsFolder: TdxDBTreeListCheckColumn
        MinWidth = 20
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'is_folder'
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object col_XREF_TableName: TdxDBTreeListColumn
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'xref_TableName'
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 629
    Height = 65
    Align = alTop
    BevelOuter = bvLowered
    Color = clBtnShadow
    TabOrder = 1
    Visible = False
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 261
    Width = 629
    Height = 193
    ActivePage = TabSheet1
    Align = alBottom
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      object dxDBInspector1: TdxDBInspector
        Left = 0
        Top = 0
        Width = 465
        Height = 165
        Align = alLeft
        DataSource = dmObjects.dsFields
        DefaultFields = False
        TabOrder = 0
        DividerPos = 89
        Data = {
          C30000000600000008000000000000001200000064784442496E73706563746F
          72316E616D6508000000000000001200000064784442496E73706563746F7231
          7479706508000000000000001500000064784442496E73706563746F72316361
          7074696F6E08000000000000001500000064784442496E73706563746F723165
          6E61626C656408000000000000001300000064784442496E73706563746F7231
          6974656D7308000000000000001200000064784442496E73706563746F723152
          6F773600000000}
        object dxDBInspector1name: TdxInspectorDBMaskRow
          FieldName = 'name'
        end
        object dxDBInspector1type: TdxInspectorDBMaskRow
          FieldName = 'type'
        end
        object dxDBInspector1caption: TdxInspectorDBMaskRow
          FieldName = 'caption'
        end
        object dxDBInspector1items: TdxInspectorDBMemoRow
          RowHeight = 60
          ScrollBars = ssBoth
          FieldName = 'items'
        end
        object dxDBInspector1enabled: TdxInspectorDBCheckRow
          ValueChecked = '1'
          ValueUnchecked = '0'
          FieldName = 'enabled'
        end
        object dxDBInspector1Row6: TdxInspectorDBCheckRow
          Caption = 'is folder'
          ValueChecked = 'True'
          ValueUnchecked = 'False'
          FieldName = 'is_folder'
        end
      end
    end
  end
  object mnu_Objects: TPopupMenu
    Left = 16
    Top = 4
    object N1: TMenuItem
      Action = act_Object_add
    end
    object N2: TMenuItem
      Action = act_Object_del
    end
  end
  object mnu_Fields: TPopupMenu
    Left = 44
    Top = 5
    object N5: TMenuItem
      Action = act_Field_Add
    end
    object N3: TMenuItem
      Action = act_Field_Del
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object N4: TMenuItem
      Action = act_Field_Down
    end
    object N8: TMenuItem
      Action = act_Field_Up
    end
    object N11: TMenuItem
      Action = act_Field_Left
    end
    object N10: TMenuItem
      Caption = '-'
    end
    object N7: TMenuItem
      Action = act_FieldDatabaseAdd
    end
    object N6: TMenuItem
      Action = act_FieldDatabaseDel
    end
    object N20: TMenuItem
      Caption = '-'
    end
    object N19: TMenuItem
      Action = act_Field_Reindex
    end
    object test1: TMenuItem
      Caption = #1056#1072#1089#1082#1088#1099#1090#1100' '#1074#1089#1077' '#1087#1091#1085#1082#1090#1099
      OnClick = test1Click
    end
  end
  object ActionList1: TActionList
    Left = 104
    Top = 4
    object act_Field_Reindex: TAction
      Category = 'Fields'
      Caption = #1055#1077#1088#1077#1080#1085#1076#1077#1082#1089#1080#1088#1086#1074#1072#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 0
      OnExecute = act_Field_ReindexExecute
    end
    object act_Object_add: TAction
      Category = 'Object'
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 0
      OnExecute = act_Field_ReindexExecute
    end
    object act_Object_del: TAction
      Category = 'Object'
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = act_Field_ReindexExecute
    end
    object act_Field_Add: TAction
      Category = 'Fields'
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ShortCut = 16452
      SecondaryShortCuts.Strings = (
        'Ctrl+L')
      OnExecute = act_Field_ReindexExecute
    end
    object act_Field_Del: TAction
      Category = 'Fields'
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = act_Field_ReindexExecute
    end
    object act_Field_Down: TAction
      Category = 'Fields'
      Caption = #1057#1076#1074#1080#1085#1091#1090#1100' '#1074#1085#1080#1079
      OnExecute = act_Field_ReindexExecute
    end
    object act_Field_Up: TAction
      Category = 'Fields'
      Caption = #1057#1076#1074#1080#1085#1091#1090#1100' '#1074#1074#1077#1088#1093
      OnExecute = act_Field_ReindexExecute
    end
    object act_FieldDatabaseDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1083#1077' '#1080#1079' '#1041#1044
      OnExecute = act_Field_ReindexExecute
    end
    object act_FieldDatabaseAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1083#1077' '#1074' '#1041#1044
      OnExecute = act_Field_ReindexExecute
    end
    object FileExit1: TFileExit
      Category = 'File'
      Caption = #1042#1099#1093#1086#1076
      Hint = 'Exit|Quits the application'
      ImageIndex = 43
    end
    object act_Field_Left: TAction
      Category = 'Fields'
      Caption = #1057#1076#1074#1080#1085#1091#1090#1100' '#1074#1083#1077#1074#1086
      OnExecute = act_Field_ReindexExecute
    end
  end
  object FormStorage1: TFormStorage
    StoredProps.Strings = (
      'dx_Objects.Width')
    StoredValues = <>
    Left = 220
    Top = 4
  end
end
