unit fr_Object_fields;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Grids,
  dxExEdtr, DBGrids, dxInspRw, dxDBInRw, dxInspct, dxDBInsp, ActnList, ImgList,
  AppEvnts, Menus, dxDBTLCl, dxDBTL, dxTL, dxDBCtrl, dxDBGrid, dxCntner, ExtCtrls,
  ComCtrls, StdActns, rxPlacemnt, StdCtrls, DBCtrls, ToolWin, cxControls, cxSplitter,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxClasses, cxGridCustomView, cxGridLevel, cxGrid,
  variants, TB2Item, TB2Dock, TB2Toolbar, RXSplit,

  dm_Object_fields,

  d_AddDBField,

  u_const_db,

  dm_Main,

  u_func_dlg,
  u_func,
  u_func_dx,
  u_classes,
  u_types,
  u_func_DB

  ;

type
  Tfrm_Object_fields = class(TForm)
    pn_Top: TPanel;
    Panel1: TPanel;
    mnu_Fields: TPopupMenu;
    N19: TMenuItem;
    ActionList2: TActionList;
    act_Field_Reindex: TAction;
    act_Field_Add: TAction;
    act_Field_Del: TAction;
    act_Field_Down: TAction;
    act_Field_Up: TAction;
    FormStorage1: TFormStorage;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    grid_Objects: TcxGridDBBandedTableView;
    col_Object_Name: TcxGridDBBandedColumn;
    col_Object_id_: TcxGridDBBandedColumn;
    col_Object_Caption: TcxGridDBBandedColumn;
    pn_List: TPanel;
    grid_select_list: TdxDBGrid;
    col_status_id: TdxDBGridColumn;
    col_status_name: TdxDBGridColumn;
    ImageList2: TImageList;
    ActionList1: TActionList;
    act_Up: TAction;
    act_Down: TAction;
    act_Add: TAction;
    act_Del: TAction;
    act_Top: TAction;
    act_Bottom: TAction;
    TBToolbar1: TTBToolbar;
    TBSubmenuItem1: TTBSubmenuItem;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    TBItem7: TTBItem;
    TBItem8: TTBItem;
    TBItem9: TTBItem;
    TBItem10: TTBItem;
    RxSplitter1: TRxSplitter;
    RxSplitter2: TRxSplitter;
    pn_flds: TPanel;
    dx_Fields: TdxDBTreeList;
    col_Caption: TdxDBTreeListColumn;
    col_Name: TdxDBTreeListPickColumn;
    col_enabled: TdxDBTreeListCheckColumn;
    col_Field_Type: TdxDBTreeListPickColumn;
    col_is_in_view: TdxDBTreeListCheckColumn;
    col_is_in_list: TdxDBTreeListCheckColumn;
    col_readonly: TdxDBTreeListCheckColumn;
    col_IsGroupAssignmentEnabled: TdxDBTreeListCheckColumn;
    col_IsMapCriteria: TdxDBTreeListCheckColumn;
    col_Index: TdxDBTreeListColumn;
    col_Object_id: TdxDBTreeListColumn;
    col_id: TdxDBTreeListColumn;
    col_Parent_id: TdxDBTreeListColumn;
    col_Status: TdxDBTreeListColumn;
    col_IsSystem: TdxDBTreeListColumn;
    TBToolbar2: TTBToolbar;
    TBItem3: TTBItem;
    TBItem4: TTBItem;
    TBSeparatorItem2: TTBSeparatorItem;
    TBItem5: TTBItem;
    TBItem6: TTBItem;
    col_guid: TdxDBTreeListColumn;
    Add1: TMenuItem;
    Del1: TMenuItem;
    N1: TMenuItem;
    act_AddDelimitier: TAction;
    col_user_type: TdxDBTreeListColumn;
    StatusBar1: TStatusBar;
    col_parent_guid: TdxDBTreeListColumn;
    act_AddFolder: TAction;
    actAddFolder1: TMenuItem;
    actAddDelimitier1: TMenuItem;
    procedure act_Field_ReindexExecute(Sender: TObject);
    procedure dx_ObjectsExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dx_FieldsCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
    procedure dx_FieldsDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure dx_FieldsDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure dx_FieldsHotTrackNode(Sender: TObject;
      AHotTrackInfo: TdxTreeListHotTrackInfo; var ACursor: TCursor);
    procedure ActionList2Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure grid_ObjectsFocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure dx_FieldsEditing(Sender: TObject; Node: TdxTreeListNode;
      var Allow: Boolean);
    procedure dx_FieldsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure dx_FieldsChangeNodeEx(Sender: TObject);
    procedure dx_FieldsMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure dx_FieldsMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure dx_FieldsColumnSorting(Sender: TObject;
      Column: TdxTreeListColumn; var Allow: Boolean);
  private
    FBeginDragAttempt: boolean;
    FBeginDragPoint: TPoint;
    FBeginDragNode: TdxTreeListNode;

    function  GetPrior(aNode: TdxTreelistNode): TdxTreelistNode;
    function  GetNext (aNode: TdxTreelistNode): TdxTreelistNode;
    function  LastNodeLevel1: TdxTreelistNode;

    function  IsSystem(aNode: TdxTreelistNode): boolean;

    procedure MoveFields(aDirectIsUp: boolean);
    procedure FieldMoveUp;
    procedure FieldMoveDown;

    procedure FieldsDelete();

    function  GetIndex(aNode: TdxTreeListNode; aDirectIsUp: boolean): double;

    procedure FieldReindex();
  end;

var
  frm_Object_fields: Tfrm_Object_fields;

//=================================================================
implementation {$R *.dfm}
//=================================================================


const
  STR_DELETE_OBJECT = '�� ������� � ���, ��� ����� ������� ���� ������?';
  STR_DELETE_FIELD  = '�� ������� � ���, ��� ����� ������� ��� ����?';


procedure Tfrm_Object_fields.FormCreate(Sender: TObject);
begin
  pn_Top.Align:=alClient;
  dx_Fields.Align:=alClient;
  grid_select_list.Align:=alClient;
  pn_flds.Align:=alClient;

  col_Field_Type.Items.Text:=
     'text'  + CRLF +
     'list'  + CRLF +
//     'strlist' + CRLF +
     'userlist'  + CRLF +
     TYPE_STATUS  + CRLF +
     'bool'  + CRLF +
     'float' + CRLF +
     'int'   + CRLF +
     'xref'   + CRLF +
     'delimiter' + CRLF +
     'comments' + CRLF +
     'degree_N'+ CRLF +
     'degree_E'+ CRLF +
     'degree'+ CRLF +
     'date'+ CRLF +
     'datetime'+ CRLF +
     'button'
      ;// + CRLF ;

  col_Object_Name.Caption:=     '���';

  col_Caption.Caption:=         '��� � ���������'; //'������������ ���';
  col_Name.Caption:=            '��� � ��';
  col_Index.Caption:=           '������ (���������)';
  col_Field_Type.Caption:=      '���';
  col_user_type.Caption:=       '���';  //'��� ������';
  col_is_in_view.Caption:=      '� ������ View';
  col_is_in_list.Caption:=      '� ������ List';
  col_IsGroupAssignmentEnabled.Caption:= '��������� ������������';
  col_IsMapCriteria.Caption:=   '�����'; //'�������� �����';    hint - '��������� ����������� �� �����'
  col_readonly.Caption:=        '������ ������';
  col_enabled.Caption:=         '���������';

  StatusBar1.SimpleText:= '��� ���������������� ����� - ��������, ���� ������ - �����';

  act_Field_Reindex.Caption:= '��������������';
  act_Field_Add.Caption:=     '�������� ����';
  act_Field_Del.Caption:=     '������� ����';
  act_Field_Down.Caption:=    '���������� ����';
  act_Field_Up.Caption:=      '���������� ����';

  act_AddFolder    .Caption:= '�������� �����';
  act_AddDelimitier.Caption:= '�������� �����������';

  SetActionsExecuteProc([act_Up, act_Down, act_Top,
                         act_Bottom, act_Del, act_Add,
                         act_AddFolder, act_AddDelimitier ],
                         act_Field_ReindexExecute);

  act_Field_Reindex.visible:= false;

  col_id.visible:= dmMain.DEBUG;
  col_parent_id.visible:= dmMain.DEBUG;
  col_index.visible:= dmMain.DEBUG;
  col_Field_Type.visible:= dmMain.DEBUG;
//  col_name.visible:= dmMain.DEBUG;
  col_guid.visible:= dmMain.DEBUG;
  col_parent_guid.visible:= dmMain.DEBUG;

  col_Object_id_.Visible:= dmMain.DEBUG;
  col_Object_Name.Visible:= dmMain.DEBUG;

  dmObject_Fields.OpenData;
end;


//--------------------------------------------------------------------
procedure Tfrm_Object_fields.act_Field_ReindexExecute(Sender: TObject);
//--------------------------------------------------------------------
begin
  with dmObject_fields, dx_Fields do
  begin
    //---------------------------
    // Fields
    //---------------------------
    if Sender= act_Field_Reindex then FieldReindex else

    if Sender= act_Field_Add then begin FieldAdd(GetIndex(LastNodeLevel1, true)); dx_FieldsChangeNodeEx(nil);  end else
    if Sender= act_Field_Del then FieldsDelete else

    if Sender= act_Field_Up   then begin FieldMoveUp;   end else   // FieldReindex;
    if Sender= act_Field_Down then begin FieldMoveDown; end else   // FieldReindex;

    if Sender= act_Up     then begin ListItemUp;    end; // FieldReindex; end else
    if Sender= act_Down   then begin ListItemDown;  end; // FieldReindex; end else
    if Sender= act_Top    then begin ListItemTop;   end; // FieldReindex; end else
    if Sender= act_Bottom then begin ListItemBottom;end; // FieldReindex; end else
    if Sender= act_Del    then begin ListItemDel;   end; // FieldReindex; end else
    if Sender= act_Add    then begin ListItemAdd;   end; // FieldReindex; end else

    if Sender= act_AddFolder     then begin AddFolder    (GetIndex(LastNodeLevel1, true)); end;
    if Sender= act_AddDelimitier then begin AddDelimitier(GetIndex(LastNodeLevel1, true)); end;
  end;
end;

//--------------------------------------------------------------------
function Tfrm_Object_fields.IsSystem(aNode: TdxTreelistNode): boolean;
//--------------------------------------------------------------------
begin
  Result:= AsInteger(aNode.Values[col_IsSystem.index])=1;
end;

//--------------------------------------------------------------------
function Tfrm_Object_fields.GetPrior;
//--------------------------------------------------------------------
begin
  Result:= nil;
  if not Assigned(aNode) then exit;
  Result:= aNode.GetPrevSibling;

(*  Result:= nil;
  if not Assigned(aNode) then exit;

  Result:= aNode.GetPriorNode;
  if not Assigned(Result) then exit;

  while Result.level<>aNode.level do begin
    Result:= aNode.GetPriorNode;
    if not Assigned(Result) then exit;
  end;

  if Result.level<>aNode.level then
    Result:= nil;*)
end;

//--------------------------------------------------------------------
function Tfrm_Object_fields.GetNext;
//--------------------------------------------------------------------
begin
  Result:= nil;
  if not Assigned(aNode) then exit;
  Result:= aNode.GetNextSibling;

(*  Result:= nil;
  if not Assigned(aNode) then exit;

  Result:= aNode.GetNextNode;

  if not Assigned(Result) then exit;

  if Result.level<>aNode.level then
    Result:= nil;*)
end;

//--------------------------------------------------------------------
function Tfrm_Object_fields.LastNodeLevel1;
//--------------------------------------------------------------------
var
  i: Integer;
begin
  if dx_Fields.Count>0 then
    Result:= dx_Fields.Items[dx_Fields.Count-1]
  else
    Result:= nil;
end;

//--------------------------------------------------------------------
procedure Tfrm_Object_fields.MoveFields(aDirectIsUp: boolean);
//--------------------------------------------------------------------
var cur_recno, new_recno, cur_id, new_id: integer;
    cur_order, new_order: double;
    cur_table, new_table: string;
    cur_rec, new_rec: TDbParamRecArray;
    vNextPriorNode, vNextPriorNode2: TdxTreeListNode;
    oUpdateIDList: TIDList;
    i: Integer;
    fInd: float;
    cur, new: record
      id: Integer;
      index: double;
    end;
begin
  oUpdateIDList:= TIDList.Create;

  with dmObject_fields.mem_Fields do
  begin
    with dx_Fields.FocusedNode, cur do begin
      id   := AsInteger(Values[col_id.index]);
      index:= AsFloat(Values[col_index.index]);
    end;

    if aDirectIsUp then begin
//      if dx_Fields.FocusedNode = dx_Fields.TopNode then
//        exit;
      vNextPriorNode:= GetPrior(dx_Fields.FocusedNode); // dx_Fields.FocusedNode.GetPriorNode;
      if vNextPriorNode=nil then exit;
      vNextPriorNode2:= GetPrior(vNextPriorNode); // vNextPriorNode.GetPriorNode;
    end else begin
//      if dx_Fields.FocusedNode = dx_Fields.LastNode then
//        exit;
      vNextPriorNode:= GetNext(dx_Fields.FocusedNode); // dx_Fields.FocusedNode.GetNextNode;
      if vNextPriorNode=nil then exit;
      vNextPriorNode2:= GetNext(vNextPriorNode); // vNextPriorNode.GetNextNode;
    end;

    with vNextPriorNode, new do begin
      id   := AsInteger(Values[col_id.index]);
      index:= AsFloat(Values[col_index.index]);
    end;

    // ���� ������ ���� ����� � ��������� ����� - ���� ���������
    // ��������� - ���������� ������ �� ������ ������� ������ ���� �����
    // (������ ���������� ���� �������� �� ��������, ��� �������� ���
    //  ��������� ���������� ����������� ��������)
    if IsSystem(vNextPriorNode) then begin
      // ���� �� ������� �� ����� ���� (���)
      if not Assigned(vNextPriorNode2) then begin
        if aDirectIsUp then
          oUpdateIDList.AddTag2(cur.id, new.index-0.5)
        else
          oUpdateIDList.AddTag2(cur.id, new.index+0.5);
      end else
      begin
        fInd:= GetIndex(vNextPriorNode2, aDirectIsUp);
        oUpdateIDList.AddTag2(cur.id, fInd);
      end;
    end
    // ����� ������ ������ ������� �������
    else begin
      oUpdateIDList.AddTag2(new.id, cur.index);
      oUpdateIDList.AddTag2(cur.id, new.index);
    end;
  end;

  dmObject_fields.LoadingFlds:= true;
  for i:= 0 to oUpdateIDList.Count - 1 do
    with dmObject_fields, oUpdateIDList[i] do
  begin
    gl_Db.UpdateRecord(TBL_OBJECT_FIELDS_USER, ID, FLD_INDEX_, Tag2);
    if mem_Fields.Locate(FLD_ID, ID, []) then
      db_UpdateRecord(mem_Fields, FLD_INDEX_, Tag2);
  end;
  dmObject_fields.LoadingFlds:= false;

  oUpdateIDList.Free;
end;

//--------------------------------------------------------------------
procedure Tfrm_Object_fields.FieldMoveUp;
//--------------------------------------------------------------------
begin
  MoveFields(true);
end;

//--------------------------------------------------------------------
procedure Tfrm_Object_fields.FieldMoveDown;
//--------------------------------------------------------------------
begin
  MoveFields(false);
end;

//--------------------------------------------------------------------
function Tfrm_Object_fields.GetIndex;
//--------------------------------------------------------------------

  function Diff(a1Val, a2Val: float): float;
  begin
    if aDirectIsUp then
      result:= a1Val + a2Val
    else
      result:= a1Val - a2Val;
  end;

var
  fIndex: double;
begin
  if not Assigned (aNode) then begin
    Result:= 0.5;
    exit;
  end;

  fIndex:= AsFloat(aNode.Values[col_Index.Index]);
  if IsSystem(aNode) then
    Result:= Diff( fIndex, 0.5 )
  else
    Result:= Diff( fIndex, 0.001 );
end;

//--------------------------------------------------------------------
procedure Tfrm_Object_fields.FieldsDelete();
//--------------------------------------------------------------------

    procedure DoDel(aNode: TdxTreeListNode);
    var i: Integer;
    begin
      for i := 0 to aNode.Count - 1 do
        DoDel(aNode[i]);

      aNode.Focused:= true;
      dmObject_fields.FieldDelete;
    end;

begin
  if not ConfirmDlg('�� ������������� ������ ������� ��� ���� �� �� '+
                '(�������������� ������ ����� ����������) ?')
  then
    exit;

  DoDel(dx_Fields.FocusedNode);
end;

//--------------------------------------------------------------------
procedure Tfrm_Object_fields.dx_ObjectsExit(Sender: TObject);
//--------------------------------------------------------------------
begin
  if Sender is TdxDBGrid then
    db_PostDataset((Sender as TdxDBGrid).DataSource.DataSet);

  if Sender is TdxDBTreeList  then
    db_PostDataset((Sender as TdxDBTreeList).DataSource.DataSet);
end;

//--------------------------------------------------------------------
procedure Tfrm_Object_fields.dx_FieldsCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
  ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
  ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
  var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
  var ADone: Boolean);
//--------------------------------------------------------------------
begin
  if ANode.HasChildren then
    AFont.Style:= AFont.Style + [fsBold];

  if not IsSystem(aNode) then begin
    AColor:= clCream;
    AFont.Color:= clBlue;
  end;

  if AColumn = col_user_type then begin
    if AsString(aNode.Values[col_Field_Type.index]) = 'text' then begin
      if AsString(aNode.Values[col_name.index]) = '' then
        AText:= '' //'�����'
      else
        AText:= '���������';
    end else
    if AsString(aNode.Values[col_Field_Type.index]) = 'list' then
      AText:= '������' else
    if AsString(aNode.Values[col_Field_Type.index]) = 'userlist' then
      AText:= '������' else
    if AsString(aNode.Values[col_Field_Type.index]) = TYPE_STATUS then
      AText:= '������' else
    if AsString(aNode.Values[col_Field_Type.index]) = 'bool' then
      AText:= '�������' else
    if AsString(aNode.Values[col_Field_Type.index]) = 'float' then
      AText:= '� ��������� ������' else
    if AsString(aNode.Values[col_Field_Type.index]) = 'int' then
      AText:= '�����' else
    if AsString(aNode.Values[col_Field_Type.index]) = 'xref' then
      AText:= '������' else
//    if AsString(aNode.Values[col_Field_Type.index]) = 'delimiter' then
//      AText:= '������ ������ (�����������)' else
    if AsString(aNode.Values[col_Field_Type.index]) = 'comments' then
      AText:= '�����������' else
    if AsString(aNode.Values[col_Field_Type.index]) = 'button' then
      AText:= '������'
    else
      AText:= '';
  end;
end;

//--------------------------------------------------------------------
procedure Tfrm_Object_fields.dx_FieldsEditing(Sender: TObject;
  Node: TdxTreeListNode; var Allow: Boolean);
//--------------------------------------------------------------------
begin
  Allow:= not IsSystem(Node);

  if Allow then begin
    if dx_Fields.FocusedColumn = col_IsMapCriteria.colindex then
      Allow:= AsString(Node.Values[col_name.index]) <> '';

    if Eq_Any_var( dx_Fields.FocusedColumn, [col_caption.colindex, col_name.colindex]) then
      Allow:= AsString(Node.Values[col_Field_Type.index]) <> 'delimiter';
  end;
end;

//--------------------------------------------------------------------
procedure Tfrm_Object_fields.dx_FieldsDragOver(Sender, Source: TObject;
                        X, Y: Integer; State: TDragState; var Accept: Boolean);
//--------------------------------------------------------------------
begin
  Accept := (Source = dx_Fields) and
            (FBeginDragNode <> dx_Fields.GetNodeAt(X,Y));
end;

//--------------------------------------------------------------------
procedure Tfrm_Object_fields.dx_FieldsMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
//--------------------------------------------------------------------
begin
  if Button = mbLeft then
    if AsInteger(dx_Fields.FocusedNode.Values[col_IsSystem.index]) = 0 then begin
      FBeginDragAttempt:= true;
      FBeginDragNode:= dx_Fields.FocusedNode;
      FBeginDragPoint.X:= X;
      FBeginDragPoint.Y:= Y;
    end;
end;

//--------------------------------------------------------------------
procedure Tfrm_Object_fields.dx_FieldsMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
//--------------------------------------------------------------------
begin
  if FBeginDragAttempt then
    if ( Abs(FBeginDragPoint.X - X) > 10) or ( Abs(FBeginDragPoint.Y - Y) > 10 ) then
      dx_Fields.BeginDrag(true);

  dx_ShowHint_ext(dx_Fields, X,Y, [], [],
       [col_IsMapCriteria
        ],
       ['��������� ����������� �� �����'
        ]);

end;

//--------------------------------------------------------------------
procedure Tfrm_Object_fields.dx_FieldsMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
//--------------------------------------------------------------------
begin
  FBeginDragAttempt:= false;
end;


//--------------------------------------------------------------------
procedure Tfrm_Object_fields.dx_FieldsDragDrop(Sender, Source: TObject; X, Y: Integer);
//--------------------------------------------------------------------
var
  iID: Integer;
  fIndex: float;
  vParentID, vGUID: variant;
  vNode, vNode1: TdxTreeListNode;
begin
  // ��� ������ � ��� ������� ���� ��� ���������� �� ����������

  iID:= AsInteger(FBeginDragNode.Values[col_id.index]);

  vNode1:= FBeginDragNode.parent;       // ���� �����������
//  vNode1:= dx_Fields.getNodeAt(X,Y);  // ���� �����������
  // ���� ���� ���������� ���������� - ������� �������������� ������� � �������� �� ���� ������ ����
  if Assigned(vNode1) then begin
    vParentID   := vNode1.Values[col_id.index];
    vGUID       := vNode1.Values[col_guid.index];
    vNode       := vNode1.GetLastChild;  // ������ ���� ����� ����� ����� ������ � ��������� vNode = FBeginDragNode
    vNode       := GetPrior(vNode);      // ����� ���� ����� ��� � ������� �� �� ������
    if Assigned(vNode) then
      fIndex      := GetIndex(vNode, true)
    else
      fIndex      := 0.5;                // ���� ���� ��� - ����� ������ 0.5
  end else begin
    vParentID   := NULL;
    vGUID       := NULL;
    fIndex      := GetIndex(LastNodeLevel1, true);
  end;

  gl_DB.UpdateRecord(TBL_OBJECT_FIELDS_USER, iID,
                       [db_Par(FLD_PARENT_ID  , vParentID)
                       ,db_Par(FLD_PARENT_GUID, vGUID)
                       ,db_Par(FLD_INDEX_     , fIndex)
                       ]);

  with dmObject_fields do begin
    LoadingFlds:= true;
    if mem_Fields.locate(FLD_ID, iID, []) then
      db_UpdateRecord(mem_Fields, FLD_INDEX_, fIndex);
    LoadingFlds:= false;  
  end;
end;



//--------------------------------------------------------------------
procedure Tfrm_Object_fields.dx_FieldsHotTrackNode(Sender: TObject;
  AHotTrackInfo: TdxTreeListHotTrackInfo; var ACursor: TCursor);
//--------------------------------------------------------------------
begin
  dx_ShowHintForDbTreeColumns(dx_Fields, AHotTrackInfo,
        [col_Caption, col_Name]);
end;

//--------------------------------------------------------------------
procedure Tfrm_Object_fields.ActionList2Update(Action: TBasicAction;
  var Handled: Boolean);
//--------------------------------------------------------------------
var
  bUserFld, b: boolean;
begin
//  b:= dx_Fields.SelectedCount=1;
  bUserFld:= AsInteger(dx_Fields.FocusedNode.Values[col_IsSystem.index]) = 0;

  act_Field_Del         .Enabled:= bUserFld;
  act_Field_Down        .Enabled:= bUserFld;
  act_Field_Up          .Enabled:= bUserFld;
end;

//--------------------------------------------------------------------
procedure Tfrm_Object_fields.grid_ObjectsFocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord;
  ANewItemRecordFocusingChanged: Boolean);
//--------------------------------------------------------------------
begin
  if Assigned(AFocusedRecord) then begin
    dmObject_fields.FilterID:= AsInteger(AFocusedRecord.Values[col_Object_id_.Index]);
    dmObject_fields.mem_Fields.UpdateFilters;
    dx_Fields.FullExpand;
  end;
end;

//--------------------------------------------------------------------
procedure Tfrm_Object_fields.dx_FieldsChangeNodeEx(Sender: TObject);
//--------------------------------------------------------------------
begin
  grid_select_list.enabled:=
        Eq(dmObject_fields.mem_Fields.FieldByName(FLD_TYPE).AsString, TYPE_STATUS);

  act_Up    .Enabled:= grid_select_list.enabled;
  act_Down  .Enabled:= grid_select_list.enabled;
  act_Top   .Enabled:= grid_select_list.enabled;
  act_Bottom.Enabled:= grid_select_list.enabled;
  act_Add   .Enabled:= grid_select_list.enabled;
  act_Del   .Enabled:= grid_select_list.enabled;

  dmObject_fields.OpenList();
end;

//--------------------------------------------------------------------
procedure Tfrm_Object_fields.FieldReindex;
//--------------------------------------------------------------------
var
  object_id, iRecNo: integer;
begin
(*  iRecNo:= qry_Fields.RecNo;

  object_id:=qry_Fields.FieldByName(FLD_OBJECT_ID).AsInteger;
  gl_DB.UpdateOrderIndex (TBL_OBJECT_FIELDS,
                          FLD_INDEX_,
                         [db_Par (FLD_OBJECT_ID, object_id)] );

  qry_Fields.Requery();

  qry_Fields.RecNo:= iRecNo;*)
end;


procedure Tfrm_Object_fields.dx_FieldsColumnSorting(Sender: TObject;
  Column: TdxTreeListColumn; var Allow: Boolean);
begin
  Allow:= false;
end;

end.
