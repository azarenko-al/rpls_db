inherited dlg_AddDBField: Tdlg_AddDBField
  Left = 477
  Top = 396
  Width = 383
  Height = 296
  Caption = 'dlg_AddDBField'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 229
    Width = 375
    inherited Bevel1: TBevel
      Width = 375
    end
    inherited btn_Ok: TButton
      Left = 212
    end
    inherited btn_Cancel: TButton
      Left = 296
    end
  end
  inherited pn_Top_: TPanel
    Width = 375
    inherited Bevel2: TBevel
      Width = 375
    end
    inherited pn_Header: TPanel
      Width = 375
    end
  end
  object dxInspector1: TdxInspector [2]
    Left = 0
    Top = 60
    Width = 375
    Height = 97
    Align = alTop
    TabOrder = 2
    DividerPos = 187
    Data = {
      5900000004000000080000000000000009000000726F775F5461626C65080000
      000000000008000000726F775F4E616D65080000000000000008000000726F77
      5F54797065080000000000000008000000726F775F53697A6500000000}
    object row_Type: TdxInspectorTextPickRow
      Caption = #1058#1080#1087
      Items.Strings = (
        #1062#1077#1083#1086#1077
        #1057' '#1087#1083#1072#1074#1072#1102#1097#1077#1081' '#1090#1086#1095#1082#1086#1081
        #1057#1090#1088#1086#1082#1086#1074#1086#1077)
    end
    object row_Name: TdxInspectorTextRow
      Caption = #1048#1084#1103
    end
    object row_Table: TdxInspectorTextRow
      Caption = #1058#1072#1073#1083#1080#1094#1072
      ReadOnly = True
    end
    object row_Size: TdxInspectorTextRow
      Caption = #1056#1072#1079#1084#1077#1088
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 224
  end
  inherited FormStorage1: TFormStorage
    Left = 284
  end
end
