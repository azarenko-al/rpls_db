unit d_AddDBField;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  d_Wizard, rxPlacemnt, ActnList, StdCtrls, ExtCtrls, dxInspct, dxInspRw, dxCntner,

  u_func_db,
  u_func_reg,

  u_types,

  u_const_db, cxPropertiesStore

  ;

type
  Tdlg_AddDBField = class(Tdlg_Wizard)
    dxInspector1: TdxInspector;
    row_Type: TdxInspectorTextPickRow;
    row_Name: TdxInspectorTextRow;
    row_Table: TdxInspectorTextRow;
    row_Size: TdxInspectorTextRow;
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure act_OkExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FTableName: string;
    FResult: boolean;
  public
    class function Execute (aObjectName: string): boolean;
  end;

var
  dlg_AddDBField: Tdlg_AddDBField;

//=======================================================
implementation {$R *.DFM}
//=======================================================

//-------------------------------------------------------
class function Tdlg_AddDBField.Execute (aObjectName: string): boolean;
//-------------------------------------------------------
begin
  with Tdlg_AddDBField.Create(Application) do
  begin
    if (not gl_Obj.ItemExist[aObjectName]) then begin
      Free;
      exit;
    end;

    FTableName:=  gl_Obj.ItemByName[aObjectName].TableName;

    if gl_DB.TableExists(FTableName) then
    begin
      row_Table.Text:= FTableName;

      if ShowModal = mrOk then
        Result:= FResult
      else
        Result:= false;
    end;

    Free;
  end;
end;

//-------------------------------------------------------
procedure Tdlg_AddDBField.FormCreate(Sender: TObject);
//-------------------------------------------------------
begin
  FResult:= false;

  with gl_Reg.RegIni do
  begin
    row_Name.Text:=                ReadString (Name, row_Name.Name, '');
    row_Type.Text:= row_Type.Items[ReadInteger(Name, row_Type.Name, 0)];
    row_Size.Text:=                ReadString (Name, row_Size.Name, '');
  end;
end;

//-------------------------------------------------------
procedure Tdlg_AddDBField.FormDestroy(Sender: TObject);
//-------------------------------------------------------
begin
  with gl_Reg.RegIni do
  begin
    WriteString (Name, row_Name.Name, row_Name.Text);
    WriteInteger(Name, row_Type.Name, row_Type.Items.IndexOF(row_Type.Text));
    WriteString (Name, row_Size.Name, row_Size.Text);
  end;
end;

//-------------------------------------------------------
procedure Tdlg_AddDBField.act_OkExecute(Sender: TObject);
//-------------------------------------------------------
var
  iIndex: integer;
  sSql, sType: string;
begin
  if gl_DB.FieldExist(FTableName, row_Name.Text) then
  begin
    ShowMessage('���� "'+row_Name.Text+'" ��� ���������� � ������� "'+FTableName+
                '". ���������� �� ��������.');
    ModalResult:=mrNone;
    exit;
  end;

  iIndex := row_Type.Items.IndexOf(row_Type.Text);

  case iIndex of
    0: sType:= 'int';
    1: sType:= 'float';
    2: sType:= 'varchar';
  end;

  if (iIndex = 0) or (iIndex = 1) then
    FResult:= gl_DB.AddTableField(FTableName, row_Name.Text, sType, -1, -1)
  else
    if (iIndex = 2) then
      FResult:= gl_DB.AddTableField(FTableName, row_Name.Text, sType, StrToInt(row_Size.Text), -1);

end;

//-------------------------------------------------------
procedure Tdlg_AddDBField.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//-------------------------------------------------------
begin
  act_Ok.Enabled:= (row_Type.Text <> '') and
                   (row_Name.Text <> '');
end;




end.
