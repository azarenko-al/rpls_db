unit d_AddField;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxPropertiesStore, rxPlacemnt, ActnList, StdCtrls,
  ExtCtrls, dxmdaset,

  d_Wizard,

  u_func,
  u_classes,
  u_const_db,
  u_func_db,
  u_types
  ;

type
  Tdlg_AddField = class(Tdlg_Wizard)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    ed_DB_name: TEdit;
    ed_size: TEdit;
    ed_prog_name: TEdit;
    cb_type: TComboBox;
    lb_Error: TLabel;
    procedure act_OkExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ed_DB_nameChange(Sender: TObject);
  private
    FObjID: Integer;
    FIndex: double;
    FObjName, FTblName: string;
    FFieldNames: TStringList;

    FResult: Integer;
    FMaxID: Integer;

    FMemData: TdxMemData;

  public
    class function ExecDlg(aObjID: Integer; aObjName: string; aIndex: double;
        var aMemData: TdxMemData): Integer;
  end;

//============================================================================//
implementation {$R *.dfm}
//============================================================================//

const
  FieldTypes : array [0..3] of string =
    ('������',
     '������� �����',
     '�����',
     '� ��������� ������ (�������)'
    );

//--------------------------------------------------------------------
class function Tdlg_AddField.ExecDlg;
//--------------------------------------------------------------------
begin
  with Tdlg_AddField.Create(Application) do begin
    FObjName:= aObjName;
    FTblName:= gl_OBj.ItemByName[FObjName].TableName;
    FObjID:= aObjID;
    FIndex:= aIndex;

    FMemData:= aMemData;

    FMaxID:= gl_Db.GetMaxID(TBL_OBJECT_FIELDS_USER) + 1;
    if FMaxID < 500000 then
      FMaxID:= 500000;

    gl_Db.ADOConnection.GetFieldNames(FTblName, FFieldNames);

    if ShowModal=mrOk then
      Result:= FResult
    else
      Result:= -1;

    Free;
  end;
end;

//--------------------------------------------------------------------
procedure Tdlg_AddField.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
var
  i: Integer;
begin
  inherited;

  Caption:= '���������� ����';
  lb_action.caption:= '���������� ����������������� ���� � �� RPLS';

  FFieldNames:= TStringList.Create;

  cb_type.Items.Clear;
  for i := 0 to High(FieldTypes) do
    cb_type.Items.Add(FieldTypes[i]);
  cb_type.ItemIndex:= 0;
end;

//--------------------------------------------------------------------
procedure Tdlg_AddField.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
  FFieldNames.Free;
end;

//--------------------------------------------------------------------
procedure Tdlg_AddField.act_OkExecute(Sender: TObject);
//--------------------------------------------------------------------
var
  iIndex, iIsInList, iIsMapCriteria: integer;
  sSql, sType, sObjFieldsType, sDBName: string;
  vStatusName: variant;
begin
  inherited;

  CursorHourGlass;

  case cb_type.ItemIndex of
    0, 2: sType:= 'int';
    1: sType:= 'varchar';
    3: sType:= 'float';
  end;

  case cb_type.ItemIndex of
    2, 3: sObjFieldsType:= sType;
    1: sObjFieldsType:= 'text';
    0: sObjFieldsType:= 'status';
  end;

  if cb_type.ItemIndex = 0 then begin
    iIsInList:= 0;
    iIsMapCriteria:= 1;
  end else
  begin
    iIsInList:= 1;
    iIsMapCriteria:= 0;
  end;

  gl_DB.BeginTrans1;

  sDBName:= '_user_field_'+ ed_DB_name.Text;

  case cb_type.ItemIndex of
    0, 2, 3: gl_DB.AddTableField(FTblName, sDBName, sType, -1, -1);
    1: gl_DB.AddTableField(FTblName, sDBName, sType, StrToInt(ed_size.Text), -1);
  end;

  if cb_type.ItemIndex=0 then
    vStatusName:= FTblName + '_' + sDBName + '_status'
  else
    vStatusName:= NULL;

  gl_DB.AddRecord(TBL_OBJECT_FIELDS_USER,
               [db_Par (FLD_ID          , FMaxID),
                db_Par (FLD_OBJECT_ID   , FObjID),
                db_Par (FLD_OBJECT_NAME , FObjName),
                db_Par (FLD_INDEX_      , FIndex),
                db_Par (FLD_TYPE        , sObjFieldsType),
                db_Par (FLD_NAME        , sDBName),
                db_Par (FLD_CAPTION     , ed_prog_name.Text),
                db_Par (FLD_IS_FOLDER   , 0),
                db_Par (FLD_ENABLED     , 1),
                db_Par (FLD_IS_IN_VIEW  , 1),
                db_Par (FLD_IS_IN_LIST  , iIsInList),
                db_Par (FLD_ISGroupAssignmentEnabled  , 1),
                db_Par (FLD_ISMapCriteria  , iIsMapCriteria),
                db_Par (FLD_STATUS_NAME , vStatusName)
               ]);

  if iIsInList = 1 then begin
    gl_DB.UpdateViews(nil, false);
    gl_DB.UpdateViews();
  end;

 // db_View (FMemData);

  db_AddRecord(FMemData,
               [db_Par (FLD_ID          , FMaxID),
                db_Par (FLD_OBJECT_ID   , FObjID),
                db_Par (FLD_OBJECT_NAME , FObjName),
                db_Par (FLD_INDEX_      , FIndex),
                db_Par (FLD_TYPE        , sObjFieldsType),
                db_Par (FLD_NAME        , sDBName),
                db_Par (FLD_CAPTION     , ed_prog_name.Text),
                db_Par (FLD_IS_FOLDER   , 0),
                db_Par (FLD_ENABLED     , 1),
                db_Par (FLD_IS_IN_VIEW  , 1),
                db_Par (FLD_IS_IN_LIST  , iIsInList),
                db_Par (FLD_ISGroupAssignmentEnabled  , 1),
                db_Par (FLD_ISMapCriteria  , iIsMapCriteria),
                db_Par (FLD_STATUS_NAME , vStatusName)
               ]);

  FResult:= FMaxID;

  if cb_type.ItemIndex=0 then begin
    gl_DB.AddRecord(TBL_STATUS,
               [db_Par (FLD_OBJECT_NAME , FObjName),
                db_Par (FLD_NAME        , vStatusName),
                db_Par (FLD_ENABLED     , 1)
               ]);
  end;

  gl_DB.CommitTrans;

  CursorDefault;
end;

//--------------------------------------------------------------------
procedure Tdlg_AddField.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
//--------------------------------------------------------------------
begin
  inherited;
  act_Ok.Enabled:=
    (ed_DB_name.Text<>'') and (ed_prog_name.Text<>'') and (lb_Error.Caption='');

  if act_Ok.Enabled then
    if (cb_type.Text = FieldTypes[0]) or (cb_type.Text = FieldTypes[1]) then
      act_Ok.Enabled:= AsInteger(ed_size.Text)<>0;
end;

//--------------------------------------------------------------------
procedure Tdlg_AddField.ed_DB_nameChange(Sender: TObject);
//--------------------------------------------------------------------
const
  ALLOWED_LETTERS : array [0..62] of string =
    ('a','b','c','d','e','f','g','h','i','j','k','l','m',
     'n','o','p','q','r','s','t','u','v','w','x','y','z',

     'A','B','C','D','E','F','G','H','I','J','K','L','M',
     'N','O','P','Q','R','S','T','U','V','W','X','Y','Z',

     '1','2','3','4','5','6','7','8','9',
     '_','-'
    );

  function FoundInArray(aChar: char): boolean;
  var i: Integer;
  begin
    Result:= false;
    for i := 0 to High(ALLOWED_LETTERS) do
    begin
      if aChar = ALLOWED_LETTERS[i] then begin
        Result:= true;
        exit;
      end;
    end;
  end;


var
  i: Integer;
  sName, sDBName: string;
begin
  inherited;

  sDBName:= '_user_field_'+ ed_DB_name.Text;

  lb_Error.Caption:= '';
  if FFieldNames.IndexOf(sDBName)>0 then begin
    lb_Error.Caption:= '���� � ����� ������ ��� ���������� � ��';
    exit;
  end;

  sName:= ed_DB_name.Text;
  for i:= 1 to Length(sName) do
    if not FoundInArray( sName[i] ) then begin
    lb_Error.Caption:= '����������� ������� ��� ����� ���� � ��: '+CRLF+
                       'a - z, A - Z, 1 - 9, '' _ '', '' - ''';
    exit;
  end;

end;

end.
