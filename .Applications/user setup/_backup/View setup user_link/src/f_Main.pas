unit f_Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, rxPlacemnt, ExtCtrls, ComCtrls, cxPC, cxControls,

  dm_App,

  u_func,
  u_func_reg,

  dm_Object_fields,
  fr_Object_fields
  
  ;


type
  Tfrm_Main = class(TForm)
    Panel1: TPanel;
    FormStorage1: TFormStorage;
    pn_Main: TPanel;
    StatusBar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
  private
    Ffrm_Object_fields: Tfrm_Object_fields;
  end;

var
  frm_Main: Tfrm_Main;

implementation {$R *.dfm}

procedure Tfrm_Main.FormCreate(Sender: TObject);
//var
//  oConnectRec: TLoginRec;
begin
  pn_Main.Align:= alClient;

  Caption:= '���������������� ����'; // '��������� ������������ ����� RPLS DB';

//  dmApp.ADOConnection1.Open;


  dmObject_fields;

  Ffrm_Object_fields:= Tfrm_Object_fields.Create(Self);
  CopyControls (Ffrm_Object_fields, pn_Main);

  gl_Reg.InitRegistry1 ('rpls_view_setup');

{
  dmMain_RFP.LoadConnectRecFromReg_ (oConnectRec);
  with oConnectRec do
    StatusBar1.SimpleText:=
      Format('������: "%s"  |  ��: "%s"  |  ������������: "%s"',
               [Server, DefaultDB, User]);
}
end;

end.
