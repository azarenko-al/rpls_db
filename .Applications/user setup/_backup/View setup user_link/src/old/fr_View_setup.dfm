object Form3: TForm3
  Left = 277
  Top = 85
  Width = 637
  Height = 482
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 101
  TextHeight = 13
  object pn_Top: TPanel
    Left = 0
    Top = 65
    Width = 629
    Height = 152
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object spl4: TSplitter
      Left = 177
      Top = 0
      Width = 3
      Height = 152
      Cursor = crHSplit
    end
    object dx_Objects: TdxDBGrid
      Left = 0
      Top = 0
      Width = 177
      Height = 152
      Bands = <
        item
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      SummaryGroups = <>
      SummarySeparator = ', '
      Align = alLeft
      PopupMenu = mnu_Objects
      TabOrder = 0
      OnExit = dx_ObjectsExit
      LookAndFeel = lfFlat
      OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEditing, edgoEnterShowEditor, edgoTabThrough, edgoVertThrough]
      OptionsDB = [edgoCanAppend, edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoUseBookmarks]
      OptionsView = [edgoBandHeaderWidth, edgoDrawEndEllipsis, edgoIndicator, edgoUseBitmap]
      object dx_ObjectsColumn1: TdxDBGridColumn
        Width = 101
        BandIndex = 0
        RowIndex = 0
        FieldName = 'name'
      end
      object dx_ObjectsColumn4: TdxDBGridColumn
        Width = 20
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Id'
      end
    end
    object dx_Fields: TdxDBTreeList
      Left = 204
      Top = 0
      Width = 425
      Height = 152
      Bands = <
        item
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      KeyField = 'id'
      ParentField = 'parent_id'
      Align = alRight
      DragMode = dmAutomatic
      PopupMenu = mnu_Fields
      TabOrder = 1
      OnDragDrop = dx_FieldsDragDrop
      OnExit = dx_ObjectsExit
      LookAndFeel = lfFlat
      OptionsBehavior = [etoAutoDragDrop, etoAutoDragDropCopy, etoDragExpand, etoDragScroll, etoEditing, etoEnterShowEditor, etoImmediateEditor, etoStoreToRegistry, etoTabThrough]
      OptionsDB = [etoCancelOnExit, etoCanDelete, etoCanNavigation, etoCheckHasChildren, etoConfirmDelete, etoLoadAllRecords]
      OptionsView = [etoBandHeaderWidth, etoHotTrack, etoIndicator, etoUseBitmap, etoUseImageIndexForSelected]
      PaintStyle = psOutlook
      ShowGrid = True
      TreeLineColor = clGrayText
      OnCustomDrawCell = dx_FieldsCustomDrawCell
      object col_Caption: TdxDBTreeListColumn
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'caption'
      end
      object col_Name: TdxDBTreeListColumn
        Width = 107
        BandIndex = 0
        RowIndex = 0
        FieldName = 'name'
      end
      object col_Field_Type: TdxDBTreeListPickColumn
        BandIndex = 0
        RowIndex = 0
        FieldName = 'type'
        Items.Strings = (
          'text'
          'list'
          'int')
      end
      object col_Index: TdxDBTreeListColumn
        BandIndex = 0
        RowIndex = 0
        FieldName = 'index_'
      end
      object col_Parent_id: TdxDBTreeListColumn
        BandIndex = 0
        RowIndex = 0
        FieldName = 'parent_id'
      end
      object col_IsFolder: TdxDBTreeListCheckColumn
        MinWidth = 20
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'is_folder'
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 629
    Height = 65
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 1
    Visible = False
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 261
    Width = 629
    Height = 193
    ActivePage = TabSheet1
    Align = alBottom
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      object dxDBInspector1: TdxDBInspector
        Left = 0
        Top = 0
        Width = 465
        Height = 165
        Align = alLeft
        DefaultFields = False
        TabOrder = 0
        DividerPos = 90
        Data = {
          A40000000500000008000000000000001200000064784442496E73706563746F
          7231526F773608000000000000001200000064784442496E73706563746F7231
          6E616D6508000000000000001200000064784442496E73706563746F72317479
          706508000000000000001500000064784442496E73706563746F723163617074
          696F6E08000000000000001500000064784442496E73706563746F7231656E61
          626C656400000000}
        object dxDBInspector1name: TdxInspectorDBMaskRow
          FieldName = 'name'
        end
        object dxDBInspector1type: TdxInspectorDBMaskRow
          FieldName = 'type'
        end
        object dxDBInspector1caption: TdxInspectorDBMaskRow
          FieldName = 'caption'
        end
        object dxDBInspector1enabled: TdxInspectorDBCheckRow
          ValueChecked = '1'
          ValueUnchecked = '0'
          FieldName = 'enabled'
        end
        object dxDBInspector1Row6: TdxInspectorDBCheckRow
          Caption = 'is folder'
          ValueChecked = 'True'
          ValueUnchecked = 'False'
          FieldName = 'is_folder'
        end
      end
    end
  end
  object mnu_Objects: TPopupMenu
    Left = 16
    Top = 4
    object N1: TMenuItem
      Action = act_Object_add
    end
    object N2: TMenuItem
      Action = act_Object_del
    end
  end
  object mnu_Fields: TPopupMenu
    Left = 44
    Top = 5
    object N5: TMenuItem
      Action = act_Field_Add
    end
    object N3: TMenuItem
      Action = act_Field_Del
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object N4: TMenuItem
      Action = act_Field_Down
    end
    object N8: TMenuItem
      Action = act_Field_Up
    end
    object N11: TMenuItem
      Action = act_Field_Left
    end
    object N10: TMenuItem
      Caption = '-'
    end
    object N7: TMenuItem
      Action = act_FieldDatabaseAdd
    end
    object N6: TMenuItem
      Action = act_FieldDatabaseDel
    end
    object N20: TMenuItem
      Caption = '-'
    end
    object N19: TMenuItem
      Action = act_Field_Reindex
    end
    object test1: TMenuItem
      OnClick = test1Click
    end
  end
  object ActionList1: TActionList
    Left = 104
    Top = 4
    object act_Field_Reindex: TAction
      Category = 'Fields'
      Caption = 'act_Field_Reindex'
      ImageIndex = 0
      OnExecute = act_Field_ReindexExecute
    end
    object act_Object_add: TAction
      Category = 'Object'
      Caption = 'act_Object_add'
      ImageIndex = 0
      OnExecute = act_Field_ReindexExecute
    end
    object act_Object_del: TAction
      Category = 'Object'
      Caption = 'act_Object_del'
      OnExecute = act_Field_ReindexExecute
    end
    object act_Field_Add: TAction
      Category = 'Fields'
      Caption = 'act_Field_Add'
      ShortCut = 16452
      OnExecute = act_Field_ReindexExecute
    end
    object act_Field_Del: TAction
      Category = 'Fields'
      Caption = 'act_Field_Del'
      OnExecute = act_Field_ReindexExecute
    end
    object act_Field_Down: TAction
      Category = 'Fields'
      Caption = 'act_Field_Down'
      OnExecute = act_Field_ReindexExecute
    end
    object act_Field_Up: TAction
      Category = 'Fields'
      Caption = 'act_Field_Up'
      OnExecute = act_Field_ReindexExecute
    end
    object act_FieldDatabaseDel: TAction
      Caption = 'act_FieldDatabaseDel'
      OnExecute = act_Field_ReindexExecute
    end
    object act_FieldDatabaseAdd: TAction
      Caption = 'act_FieldDatabaseAdd'
      OnExecute = act_Field_ReindexExecute
    end
    object act_Field_Left: TAction
      Category = 'Fields'
      Caption = 'act_Field_Left'
      OnExecute = act_Field_ReindexExecute
    end
  end
  object FormStorage1: TFormStorage
    StoredProps.Strings = (
      'dx_Objects.Width')
    StoredValues = <>
    Left = 220
    Top = 4
  end
end
