object dmObjects: TdmObjects
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 444
  Top = 258
  Height = 346
  Width = 317
  object qry_Objects: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    AfterScroll = qry_ObjectsAfterScroll
    EnableBCD = False
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM _objects'
      'ORDER BY name')
    Left = 27
    Top = 74
  end
  object dsObjects: TDataSource
    DataSet = qry_Objects
    Left = 24
    Top = 10
  end
  object qry_Fields: TADOQuery
    Active = True
    AutoCalcFields = False
    Connection = ADOConnection1
    CursorType = ctStatic
    OnNewRecord = qry_FieldsNewRecord
    EnableBCD = False
    Parameters = <
      item
        Name = 'id'
        DataType = ftInteger
        Precision = 10
        Value = 14
      end>
    SQL.Strings = (
      'SELECT *'
      ''
      'FROM _object_fields'
      ''
      'WHERE object_id=:id'
      ''
      'ORDER BY parent_id, index_')
    Left = 81
    Top = 74
  end
  object dsFields: TDataSource
    DataSet = qry_Fields
    Left = 76
    Top = 10
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=SQL_ONEGA'
    LoginPrompt = False
    Left = 204
    Top = 24
  end
end
