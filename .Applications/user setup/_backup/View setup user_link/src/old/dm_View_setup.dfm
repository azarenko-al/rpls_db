object dmView_setup: TdmView_setup
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 285
  Top = 161
  Height = 291
  Width = 433
  object qry_Objects: TADOQuery
    Active = True
    Connection = data_Main.ADOConnection1
    CursorType = ctStatic
    AfterScroll = qry_ObjectsAfterScroll
    EnableBCD = False
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM _objects'
      'ORDER BY name')
    Left = 23
    Top = 74
  end
  object dsObjects: TDataSource
    DataSet = qry_Objects
    Left = 24
    Top = 10
  end
  object qry_Fields: TADOQuery
    Active = True
    AutoCalcFields = False
    Connection = data_Main.ADOConnection1
    CursorType = ctStatic
    OnNewRecord = qry_FieldsNewRecord
    DataSource = dsObjects
    EnableBCD = False
    Parameters = <
      item
        Name = 'id'
        DataType = ftInteger
        Precision = 10
        Value = 10
      end>
    SQL.Strings = (
      'SELECT *'
      ''
      'FROM _object_fields'
      ''
      'WHERE object_id=:id'
      ''
      'ORDER BY parent_id, index_')
    Left = 81
    Top = 70
  end
  object dsFields: TDataSource
    DataSet = qry_Fields
    Left = 80
    Top = 14
  end
end
