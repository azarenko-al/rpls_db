unit dm_View_setup;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms,
  u_func_DB;

type
  TdmView_setup = class(TDataModule)
    qry_Objects: TADOQuery;
    dsObjects: TDataSource;
    qry_Fields: TADOQuery;
    dsFields: TDataSource;

    procedure qry_ObjectsAfterScroll(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure qry_FieldsNewRecord(DataSet: TDataSet);

  private
    ADOConnection: TADOConnection;
    { Private declarations }
  public

    procedure ObjectAdd;
    procedure ObjectDelete;

    procedure FieldAdd;
    procedure FieldDelete;
    procedure FieldReindex;

    procedure FieldMoveUp;
    procedure FieldMoveDown;
    procedure FieldMoveLeft;

  end;

var
  dmView_setup: TdmView_setup;

const
   TBL_OBJECTS        = '_objects';
   TBL_OBJECT_FIELDS  = '_object_fields';

   FLD_OBJECT_ID    = 'object_id';
   FLD_FIELD_ID     = 'field_id';

   FLD_FIELD_NAME   = 'FieldName';
   FLD_TABLE_NAME   = 'Tablename';
   FLD_TABLENAME    = 'TableName';

   SQL_SELECT_FIELDS =
      'SELECT * FROM ' + TBL_OBJECT_FIELDS +
      '  WHERE object_id=:id '+
      '  ORDER BY parent_id, index_';


//============================================================================//
implementation

uses dm_Main; {$R *.dfm}
//============================================================================//

//============================================================================//
procedure TdmView_setup.DataModuleCreate(Sender: TObject);
//============================================================================//
begin
  db_SetComponentADOConnection(Self, data_Main.ADOConnection1);

  qry_Objects.Open;

  qry_Fields.DataSource:=dsObjects;
  db_OpenQuery (qry_Fields, SQL_SELECT_FIELDS);

end;

//============================================================================//
//  ���������� �������
//
//============================================================================//
procedure TdmView_setup.ObjectAdd;
begin
  qry_Objects.Append;
end;

//============================================================================//
//  �������� �������
//
//============================================================================//
procedure TdmView_setup.ObjectDelete;
begin
  gl_DB.DeleteRecord (TBL_OBJECTS, qry_Objects.FieldByName(FLD_ID).AsInteger);
  qry_Objects.Requery();
end;

procedure TdmView_setup.FieldMoveUp;
begin
  gl_DB.MoveRecord (qry_Fields, True, TBL_OBJECT_FIELDS, FLD_INDEX);
end;

procedure TdmView_setup.FieldMoveDown;
begin
  gl_DB.MoveRecord (qry_Fields, False, TBL_OBJECT_FIELDS, FLD_INDEX);
end;

procedure TdmView_setup.FieldMoveLeft;
var id: integer;
begin
  id:=qry_Fields.FieldByName(FLD_ID).AsInteger;
  gl_DB.UpdateRecord(TBL_OBJECT_FIELDS, id,
                    [db_Par (FLD_PARENT_ID, NULL) ]);
  db_RefreshQuery (qry_Fields, id);
end;


//============================================================================//
// ���������� ���� �������
//
//============================================================================//
procedure TdmView_setup.FieldAdd;
//============================================================================//
var sql: string;
    id,iMaxValue,id_object: integer;
begin
//  qry_Fields.Append;


  id_object:=qry_Objects.FieldByName(FLD_ID).AsInteger;
  iMaxValue:=gl_DB.GetMaxFieldValue (TBL_OBJECT_FIELDS,
                                     FLD_INDEX,
                                     [db_Par (FLD_OBJECT_ID, id_object)]);

  gl_DB.AddRecord(TBL_OBJECT_FIELDS,
     [db_Par (FLD_OBJECT_ID, id_object),
      db_Par (FLD_INDEX,     iMaxValue+1)]);

  db_RefreshQuery(qry_Fields, gl_DB.GetIdent);

end;

//============================================================================//
// �������� ���� �������
//
//============================================================================//
procedure TdmView_setup.FieldDelete;
var  object_id: integer;
begin
  object_id:=qry_Fields.FieldByName(FLD_OBJECT_ID).AsInteger;
  gl_DB.DeleteRecord (TBL_OBJECT_FIELDS, qry_Fields.FieldByName(FLD_ID).AsInteger);

  FieldReindex ();
end;


procedure TdmView_setup.FieldReindex;
var  object_id: integer;
begin
  object_id:=qry_Fields.FieldByName(FLD_OBJECT_ID).AsInteger;
  gl_DB.UpdateOrderIndex (TBL_OBJECT_FIELDS,
                          FLD_INDEX,
                         [db_Par (FLD_OBJECT_ID, object_id)] );

  qry_Fields.Requery();
end;



procedure TdmView_setup.qry_ObjectsAfterScroll(DataSet: TDataSet);
begin
//  db_OpenQuery (qry_Fields, SQL_SELECT_FIELDS,
 //              [db_Par (FLD_OBJECT_ID, qry_Objects.FieldByName(FLD_ID).AsInteger)]);
end;




procedure TdmView_setup.qry_FieldsNewRecord(DataSet: TDataSet);
var sql: string;
    id,iMaxValue,id_object: integer;
begin
{
  id_object:=qry_Objects.FieldByName(FLD_ID).AsInteger;
  iMaxValue:=gl_DB.GetMaxFieldValue (TBL_OBJECT_FIELDS,
                                     FLD_INDEX,
                                     [db_Par (FLD_OBJECT_ID, id_object)]);

  qry_Fields.FieldByName(FLD_OBJECT_ID).AsInteger:=id_object;
  qry_Fields.FieldByName(FLD_INDEX).AsInteger:=iMaxValue+1;
 }
end;

end.

