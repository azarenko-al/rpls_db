object dlg_ViewField: Tdlg_ViewField
  Left = 330
  Top = 233
  BorderStyle = bsDialog
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1083#1077' '#1087#1088#1077#1076#1089#1090#1072#1074#1083#1077#1085#1080#1103
  ClientHeight = 259
  ClientWidth = 450
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  PixelsPerInch = 101
  TextHeight = 13
  object Label6: TLabel
    Left = 0
    Top = 4
    Width = 71
    Height = 13
    Caption = #1055#1086#1083#1077' '#1086#1073#1098#1077#1082#1090#1072
  end
  object DBLookupComboBox1: TDBLookupComboBox
    Left = 0
    Top = 20
    Width = 145
    Height = 21
    DataField = 'Field_id'
    DataSource = dmViews.ds_dlg_ViewField
    KeyField = 'id'
    ListField = 'name'
    ListSource = dmViews.ds_dlg_Field
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 225
    Width = 450
    Height = 34
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object buOK: TButton
      Left = 300
      Top = 4
      Width = 75
      Height = 25
      Caption = #1043#1086#1090#1086#1074#1086
      TabOrder = 0
      OnClick = buOKClick
    end
    object buCancel: TButton
      Left = 376
      Top = 4
      Width = 75
      Height = 25
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
  object dxDBInspector1: TdxDBInspector
    Left = 0
    Top = 56
    Width = 450
    Height = 169
    Align = alBottom
    DataSource = dmViews.ds_dlg_ViewField
    DefaultFields = False
    TabOrder = 2
    DividerPos = 144
    Data = {
      BC0000000600000008000000000000001200000064784442496E73706563746F
      7231526F773608000000000000001200000064784442496E73706563746F7231
      526F773108000000000000001200000064784442496E73706563746F7231526F
      773208000000000000001200000064784442496E73706563746F7231526F7733
      08000000000000001200000064784442496E73706563746F7231526F77350800
      0000000000001200000064784442496E73706563746F7231526F773400000000}
    object dxDBInspector1Row1: TdxInspectorDBRow
      Caption = #1064#1080#1088#1080#1085#1072
      FieldName = 'Width'
    end
    object dxDBInspector1Row2: TdxInspectorDBRow
      Caption = #1042#1099#1089#1086#1090#1072
      FieldName = 'Height'
    end
    object dxDBInspector1Row3: TdxInspectorDBRow
      Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1082#1072
      FieldName = 'Sorted'
    end
    object dxDBInspector1Row4: TdxInspectorDBMemoRow
      Caption = #1060#1080#1083#1100#1090#1088
      RowHeight = 60
      FieldName = 'Filter'
    end
    object dxDBInspector1Row5: TdxInspectorDBCheckRow
      Caption = #1042#1080#1076#1080#1084#1086#1077
      ValueChecked = 'True'
      ValueUnchecked = 'False'
      FieldName = 'Visible'
    end
    object dxDBInspector1Row6: TdxInspectorDBRow
      Caption = #1055#1086#1083#1077' '#1086#1073#1098#1077#1082#1090#1072
      FieldName = 'Field_id'
    end
  end
end
