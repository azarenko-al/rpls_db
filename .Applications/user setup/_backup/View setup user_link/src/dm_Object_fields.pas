unit dm_Object_fields;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms, variants, dxmdaset, Dialogs,

  u_func,
  u_func_DB,
  u_func_dlg,

  u_types,
  u_const_db,

  d_AddField,

  dm_App
//  dm_Main_RFP
  ;

type
  TdmObject_fields = class(TDataModule)
    qry_Objects: TADOQuery;
    dsObjects: TDataSource;
    qry_Fields: TADOQuery;
    dsFields: TDataSource;
    qry_Names: TADOQuery;
    mem_Objects: TdxMemData;
    mem_Fields: TdxMemData;
    mem_List: TdxMemData;
    ds_List: TDataSource;
    qry_List: TADOQuery;
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure FillNameList(aName : string; aNameList : TStrings);
    procedure mem_FieldsFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure mem_ListBeforePost(DataSet: TDataSet);
    procedure mem_ListAfterPost(DataSet: TDataSet);
    procedure mem_FieldsBeforePost(DataSet: TDataSet);
  private
    FMaxIndexFlds, FMaxIndex, FParentID: Integer;

    FLoadingList: boolean;

    procedure ReOpenList;

  public
    LoadingFlds: boolean;

    FilterID: Integer;

    procedure FieldAdd     (aIndex: double);
    procedure AddFolder    (aIndex: double);
    procedure AddDelimitier(aIndex: double);

    procedure FieldDelete;
    procedure FieldReindex;

    procedure RefreshList;

    procedure OpenData;
    procedure OpenFields();
    procedure OpenList;

    procedure ListItemUp;
    procedure ListItemDown;
    procedure ListItemTop;
    procedure ListItemBottom;
    procedure ListItemDel;
    procedure ListItemAdd;
  end;

var
  FdmObject_fields: TdmObject_fields;

function dmObject_fields: TdmObject_fields;


const
   FLD_OBJECT_ID    = 'object_id';
   FLD_FIELD_ID     = 'field_id';

   FLD_FIELD_NAME   = 'FieldName';
   FLD_TABLE_NAME   = 'Tablename';
   FLD_TABLENAME    = 'TableName';

   FLD_SYSTEM = 'SYSTEM';

   TYPE_STATUS = 'status';

   SQL_SELECT_FIELDS =
      ' SELECT id,  object_id, Object_Name, is_folder, parent_id, '+
      '    index_, name, type, caption,              '+
      '    is_readonly,  hidden, sorted, enabled, hint, items,  '+
      '     xref_ObjectName, xref_TableName, xref_FieldName,     '+
      '    is_in_list, is_in_view, column_caption, column_index,         '+
      '    show_button, isGroupAssignmentEnabled, IsMapCriteria,          '+
      '    status_name                               '+
      '    , 1 as system FROM ' + TBL_OBJECT_FIELDS +
      '  WHERE IsNull(enabled, 0)<>0 ' +

      '    UNION ALL '+
      ' SELECT id,  object_id, Object_Name, is_folder, parent_id, '+
      '    index_, name, type, caption,             '+
      '    is_readonly,  hidden, sorted, enabled, hint, items,  '+
      '     xref_ObjectName, xref_TableName, xref_FieldName,     '+
      '    is_in_list, is_in_view, column_caption, column_index,         '+
      '    show_button, isGroupAssignmentEnabled, IsMapCriteria,          '+
      '    status_name                             '+
      '    , 0 as system FROM ' + TBL_OBJECT_FIELDS_USER +
      '  ORDER BY object_id, parent_id, index_';



(*
   SQL_SELECT_FIELDS =
      ' SELECT id, old_id, object_id, Object_Name, is_folder, parent_id, '+
      '    index_, name, type, caption, size_, DefaultValue,             '+
      '    is_readonly, required, hidden, sorted, enabled, hint, items,  '+
      '    values_, xref_ObjectName, xref_TableName, xref_FieldName,     '+
      '    is_in_list, is_in_view, column_caption, column_index,         '+
      '    show_button, isGroupAssignmentEnabled, IsMapCriteria,         '+
      '    status_name, guid, parent_guid                                '+
      '    , 1 as system FROM ' + TBL_OBJECT_FIELDS +
      '  WHERE IsNull(enabled, 0)<>0 '+
      '    UNION ALL '+
      ' SELECT id, old_id, object_id, Object_Name, is_folder, parent_id, '+
      '    index_, name, type, caption, size_, DefaultValue,             '+
      '    is_readonly, required, hidden, sorted, enabled, hint, items,  '+
      '    values_, xref_ObjectName, xref_TableName, xref_FieldName,     '+
      '    is_in_list, is_in_view, column_caption, column_index,         '+
      '    show_button, isGroupAssignmentEnabled, IsMapCriteria,         '+
      '    status_name, guid, parent_guid                                '+
      '    , 0 as system FROM ' + TBL_OBJECT_FIELDS_USER +
      '  ORDER BY object_id, parent_id, index_';



   SQL_SELECT_FIELDS =
      ' SELECT id, object_id,  is_folder, parent_id, '+
      '    index_, name, type, caption,               '+
      '    is_readonly,  hidden, sorted, enabled, hint, items,  '+
      '     xref_ObjectName, xref_TableName, xref_FieldName,     '+
      '    is_in_list, is_in_view, column_caption, column_index,         '+
      '    show_button, isGroupAssignmentEnabled,          '+
      '    status_name                                 '+
      '    , 1 as system FROM ' + TBL_OBJECT_FIELDS +
      '  WHERE IsNull(enabled, 0)<>0 ';

      '    UNION ALL '+
      ' SELECT id,  object_id,  is_folder, parent_id, '+
      '    index_, name, type, caption,               '+
      '    is_readonly,  hidden, sorted, enabled, hint, items,  '+
      '     xref_ObjectName, xref_TableName, xref_FieldName,     '+
      '    is_in_list, is_in_view, column_caption, column_index,         '+
      '    show_button, isGroupAssignmentEnabled,          '+
      '    status_name                                 '+
      '    , 0 as system FROM ' + TBL_OBJECT_FIELDS_USER +
      '  ORDER BY object_id, parent_id, index_';
*)

  ENABLED_TYPES =
    [ otProperty
//    , otSite
//    , otCell
    , otAntenna
//    , otBsc
//    , otCalcRegion_2G
//    , otCalcRegion_3G
//    , otDriveTest_2G
//    , otDriveTest_3G
    , otLink
    , otLinkEnd
    , otLinkLine
    , otLinkNet
//    , otMsc
//    , otPmpSector
//    , otPmpSite
//    , otPmpTerminal
//    , otTraffic
//    , otTrafficModel_3G
    ];


//============================================================================//
implementation

uses Math; {$R *.dfm}
//============================================================================//

function dmObject_fields: TdmObject_fields;
begin
  if not Assigned(FdmObject_fields) then
    FdmObject_fields:= TdmObject_fields.Create (Application);

  Result:= FdmObject_fields;
end;

//--------------------------------------------------------------------
procedure TdmObject_fields.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  db_CreateField(mem_List, [db_Field(FLD_ID             , ftInteger),
                            db_Field(FLD_INDEX_         , ftInteger),
                            db_Field(FLD_PARENT_ID      , ftInteger),
                            db_Field(FLD_NAME           , ftString, 300) ]);

  db_SetComponentADOConn(Self, dmApp.ADOConnection1);
end;

//--------------------------------------------------------------------
procedure TdmObject_fields.OpenData;
//--------------------------------------------------------------------
var
  sName: string;
  ot: TrpObjectType;
begin
  mem_Objects.DisableControls;
  mem_Fields.DisableControls;

  db_OpenQuery2 (qry_Objects, '*', TBL_OBJECTS, [EMPTY_PARAM], FLD_NAME);
  db_CreateFieldsFromDataSet(qry_Objects, mem_Objects);
  db_CreateField(mem_Objects, FLD_CAPTION, ftString);
  with qry_Objects do begin
    First;
    db_Clear(mem_Objects);
    while not EOF do
    begin
      sName:= FieldByName(FLD_NAME).AsString;
      if gl_Obj.ItemExist[sName] then begin
        ot:= gl_Obj.ItemByName[sName].Type_;

        if ot in ENABLED_TYPES then
          db_AddRecord(mem_Objects,
            [ db_Par(FLD_ID, FieldValues[FLD_ID]),
              db_Par(FLD_NAME, sName),
              db_Par(FLD_CAPTION, gl_Obj.ItemByName[sName].RootFolderName)
            ] );
      end;

      Next;
    end;
  end;

  OpenFields;

  db_OpenQuery2(qry_List, '*', TBL_STATUS, EMPTY_PARAM, FLD_INDEX_);
end;

//--------------------------------------------------------------------
procedure TdmObject_fields.OpenFields();
//--------------------------------------------------------------------
begin
  LoadingFlds:= true;

  db_OpenQuery (qry_Fields, SQL_SELECT_FIELDS);
  db_CreateFieldsFromDataSet(qry_Fields, mem_Fields);
  db_CopyRecords(qry_Fields, mem_Fields);

  mem_Objects.First;

  mem_Objects.EnableControls;
  mem_Fields.EnableControls;

  mem_Fields.Filtered:= true;

  FilterID:= mem_Objects.FieldByName(FLD_ID).AsInteger;
  mem_Fields.UpdateFilters;

  mem_Fields.Last;
  FMaxIndexFlds:= mem_Fields.FieldByName(FLD_INDEX_).AsInteger + 1;
  mem_Fields.First;

  LoadingFlds:= false;
end;

//--------------------------------------------------------------------
procedure TdmObject_fields.ReOpenList;
//--------------------------------------------------------------------
begin
  qry_List.Requery;
end;


////--------------------------------------------------------------------
//procedure TdmObject_fields.GetMaxIndex

//--------------------------------------------------------------------
procedure TdmObject_fields.OpenList;
//--------------------------------------------------------------------
var
  sStatusName: string;
begin
  db_Clear(mem_List);

  sStatusName:= mem_Fields.FieldByName(FLD_STATUS_NAME).AsString;
  if sStatusName='' then
    exit;

  if not qry_List.Active then
    exit;

  if qry_List.Locate(FLD_NAME, sStatusName, [loCaseInsensitive]) then
    FParentID:= qry_List.FieldByName(FLD_ID).AsInteger
  else
    exit;

  FLoadingList:= true;
  mem_List.DisableControls;

  with qry_List do begin
    First;
    while not EOF do
    begin
      if FieldByName(FLD_PARENT_ID).AsInteger = FParentID then begin
        db_AddRecord(mem_List, [db_Par(FLD_ID           , FieldByName(FLD_ID).AsInteger),
                                db_Par(FLD_INDEX_       , FieldByName(FLD_INDEX_).AsInteger),
                                db_Par(FLD_PARENT_ID    , FieldByName(FLD_PARENT_ID).AsInteger),
                                db_Par(FLD_NAME         , FieldByName(FLD_NAME).AsString)
                               ]);
        FMaxIndex:= qry_List.FieldByName(FLD_INDEX_).AsInteger;
      end;

      Next;
    end;
  end;

  mem_List.First;
  mem_List.EnableControls;

  FLoadingList:= false;
end;

//--------------------------------------------------------------------
procedure TdmObject_fields.mem_ListBeforePost(DataSet: TDataSet);
//--------------------------------------------------------------------
var
  sName: string;
  iID, iParentID: Integer;
begin
  if FLoadingList then
    exit;

  iID:= DataSet.FieldByName(FLD_ID).AsInteger;
  sName:= DataSet.FieldByName(FLD_NAME).AsString;
  if iID>0 then begin
    gl_DB.UpdateRecord(TBL_STATUS, iID,
        [db_Par(FLD_NAME     , sName),
         db_Par(FLD_INDEX_   , DataSet.FieldByName(FLD_INDEX_).AsInteger)]);
  end else begin
    Inc(FMaxIndex);

    gl_DB.AddRecord(TBL_STATUS, [db_Par(FLD_NAME     , sName),
                                 db_Par(FLD_INDEX_   , FMaxIndex),
                                 db_Par(FLD_PARENT_ID, FParentID) ]);
  end;
end;

//--------------------------------------------------------------------
procedure TdmObject_fields.RefreshList;
//--------------------------------------------------------------------
var
  iRecNo: Integer;
begin
  iRecNo:= mem_List.RecNo;

  qry_List.Requery;
  OpenList;

  mem_List.RecNo:= iRecNo;
end;

//--------------------------------------------------------------------
procedure TdmObject_fields.mem_ListAfterPost(DataSet: TDataSet);
//--------------------------------------------------------------------
begin
  if FLoadingList then
    exit;

  RefreshList;
end;

//--------------------------------------------------------------------
procedure TdmObject_fields.ListItemAdd;
//--------------------------------------------------------------------
begin
  db_PostDataset(mem_List);
  mem_List.Append;
end;

//--------------------------------------------------------------------
procedure TdmObject_fields.ListItemDel;
//--------------------------------------------------------------------
var
  iID: Integer;
  sObjName, sFldName, sTable, sSql: string;
begin
  iID:= mem_List.FieldByName(FLD_ID).AsInteger;
  if iID>0 then begin
    if ConfirmDlg('�� ������������� ������ ������� ��� �������� �� �� (�������������� ������ ����� ����������) ?') then
    begin
      sObjName:= mem_Fields.FieldByName(FLD_OBJECT_NAME).AsString;
      sFldName:= mem_Fields.FieldByName(FLD_NAME).AsString;
      sTable:= gl_Obj.ItemByName[ sObjName ].TableName;

      sSql:= Format('UPDATE %s SET %s=NULL WHERE %s=%d', [sTable, sFldName, sFldName, iID]);

      Cursorsql;
      if gl_DB.ExecCommandSimple(sSql) then
        mem_List.Delete;

      gl_DB.DeleteRecord(TBL_STATUS, iID);

      RefreshList;
      cursordefault;
    end;
  end;
end;

//--------------------------------------------------------------------
procedure TdmObject_fields.ListItemUp;
begin
  db_MoveRecordUpDown (mem_List, FLD_INDEX_, FLD_ID, true);
end;

procedure TdmObject_fields.ListItemDown;
begin
  db_MoveRecordUpDown (mem_List, FLD_INDEX_, FLD_ID, false);
end;

procedure TdmObject_fields.ListItemTop;
begin
  db_MoveRecordTopBottom (mem_List, FLD_INDEX_, FLD_ID, true);
end;

procedure TdmObject_fields.ListItemBottom;
begin
  db_MoveRecordTopBottom (mem_List, FLD_INDEX_, FLD_ID, false);
end;
//--------------------------------------------------------------------


//--------------------------------------------------------------------
procedure TdmObject_fields.FillNameList (aName : string; aNameList : TStrings);
//--------------------------------------------------------------------
var
  sTable: string;
begin
  if (not gl_Obj.ItemExist[aName]) then begin
    exit;
  end;

  sTable:= gl_Obj.ItemByName[aName].TableName;

  if (gl_DB.TableExists(sTable)) then
  begin
    db_OpenQuery (qry_Names,  'SELECT top 1 * FROM ' + sTable);
    qry_Names.GetFieldNames (aNameList);
  end;
end;



//--------------------------------------------------------------------
procedure TdmObject_fields.FieldReindex;
//--------------------------------------------------------------------
var
  object_id, iRecNo: integer;
begin
  iRecNo:= qry_Fields.RecNo;

  object_id:=qry_Fields.FieldByName(FLD_OBJECT_ID).AsInteger;
  gl_DB.UpdateOrderIndex (TBL_OBJECT_FIELDS,
                          FLD_INDEX_,
                         [db_Par (FLD_OBJECT_ID, object_id)] );

  qry_Fields.Requery();

  qry_Fields.RecNo:= iRecNo;
end;


//============================================================================//
// ���������� ���� �������
//
//============================================================================//
procedure TdmObject_fields.FieldAdd;
//============================================================================//
begin
  LoadingFlds:= true;

//  CursorHourGlass;
  TDlg_AddField.ExecDlg(mem_Objects.FieldByName(FLD_ID).AsInteger,
        mem_Objects.FieldByName(FLD_NAME).AsString, aIndex,
        mem_Fields );

  ReOpenList;

//  CursorDefault;
  LoadingFlds:= false;
end;

//--------------------------------------------------------------------
procedure TdmObject_fields.AddDelimitier(aIndex: double);
//--------------------------------------------------------------------
var
  iID: Integer;
begin
  LoadingFlds:= true;

  iID:= gl_Db.GetMaxID(TBL_OBJECT_FIELDS_USER) + 1;
  if iID < 500000 then
    iID:= 500000;

  gl_DB.AddRecord(TBL_OBJECT_FIELDS_USER,
               [db_Par (FLD_ID          , iID ),
                db_Par (FLD_OBJECT_ID   , mem_Objects.FieldByName(FLD_ID).AsInteger),
                db_Par (FLD_OBJECT_NAME , mem_Objects.FieldByName(FLD_NAME).AsString),
                db_Par (FLD_INDEX_      , aIndex),
                db_Par (FLD_TYPE        , 'delimiter'),
                db_Par (FLD_IS_FOLDER   , 0),
                db_Par (FLD_ENABLED     , 1),
                db_Par (FLD_IS_IN_VIEW  , 1),
                db_Par (FLD_IS_IN_LIST  , 0)
               ]);

  db_AddRecord(mem_Fields,
               [db_Par (FLD_ID          , iID ),
                db_Par (FLD_OBJECT_ID   , mem_Objects.FieldByName(FLD_ID).AsInteger),
                db_Par (FLD_OBJECT_NAME , mem_Objects.FieldByName(FLD_NAME).AsString),
                db_Par (FLD_INDEX_      , aIndex),
                db_Par (FLD_TYPE        , 'delimiter'),
                db_Par (FLD_IS_FOLDER   , 0),
                db_Par (FLD_ENABLED     , 1),
                db_Par (FLD_IS_IN_VIEW  , 1),
                db_Par (FLD_IS_IN_LIST  , 0)
               ]);

  LoadingFlds:= false;
end;

//--------------------------------------------------------------------
procedure TdmObject_fields.AddFolder(aIndex: double);
//--------------------------------------------------------------------
var
  iID: Integer;
  sCaption: string;
begin
  if not InputQuery('�������� �����', '������� �������� �����', sCaption) then
    exit;

  LoadingFlds:= true;

  iID:= gl_Db.GetMaxID(TBL_OBJECT_FIELDS_USER) + 1;
  if iID < 500000 then
    iID:= 500000;

  gl_DB.AddRecord(TBL_OBJECT_FIELDS_USER,
               [db_Par (FLD_ID          , iID ),
                db_Par (FLD_OBJECT_ID   , mem_Objects.FieldByName(FLD_ID).AsInteger),
                db_Par (FLD_OBJECT_NAME , mem_Objects.FieldByName(FLD_NAME).AsString),
                db_Par (FLD_INDEX_      , aIndex),
                db_Par (FLD_TYPE        , 'text'),
                db_Par (FLD_CAPTION     , sCaption),
                db_Par (FLD_IS_FOLDER   , 1),
                db_Par (FLD_ENABLED     , 1),
                db_Par (FLD_IS_IN_VIEW  , 1),
                db_Par (FLD_IS_IN_LIST  , 0)
               ]);


 // db_View (mem_Fields);

  db_AddRecord(mem_Fields,
               [db_Par (FLD_ID          , iID ),
                db_Par (FLD_OBJECT_ID   , mem_Objects.FieldByName(FLD_ID).AsInteger),
                db_Par (FLD_OBJECT_NAME , mem_Objects.FieldByName(FLD_NAME).AsString),
                db_Par (FLD_INDEX_      , aIndex),
                db_Par (FLD_TYPE        , 'text'),
                db_Par (FLD_CAPTION     , sCaption),
                db_Par (FLD_IS_FOLDER   , 1),
                db_Par (FLD_ENABLED     , 1),
                db_Par (FLD_IS_IN_VIEW  , 1),
                db_Par (FLD_IS_IN_LIST  , 0)
               ]);

  LoadingFlds:= false;
end;


//--------------------------------------------------------------------
procedure TdmObject_fields.mem_FieldsBeforePost(DataSet: TDataSet);
//--------------------------------------------------------------------
var
  iID: Integer;
begin
  if LoadingFlds then
    exit;

  if DataSet.FieldByName(FLD_SYSTEM).AsInteger=1 then
    exit;

  iID:= DataSet.FieldByName(FLD_ID).AsInteger;
  if iID>0 then begin
    with DataSet do
      gl_DB.UpdateRecord(TBL_OBJECT_FIELDS_USER, iID,
          [db_Par(FLD_NAME        , FieldValues[FLD_NAME] )
          ,db_Par(FLD_CAPTION     , FieldValues[FLD_CAPTION] )
          ,db_Par(FLD_ENABLED     , FieldValues[FLD_ENABLED] )
          ,db_Par(FLD_TYPE        , FieldValues[FLD_TYPE] )
          ,db_Par(FLD_IS_FOLDER   , FieldValues[FLD_IS_FOLDER] )
          ,db_Par(FLD_IS_READONLY , FieldValues[FLD_IS_READONLY] )

//          ,db_Par(FLD_ISGroupAssignmentEnabled , FieldValues[FLD_ISGroupAssignmentEnabled] )
//          ,db_Par(FLD_ISMapCriteria            , FieldValues[FLD_ISMapCriteria] )
          ]);
  end;
end;

//============================================================================//
// �������� ���� �������
//
//============================================================================//
procedure TdmObject_fields.FieldDelete;
var iObjID, iStatusID: integer;
  sObjName, sTblName, sDBName: string;
  bRes: boolean;
begin
  CursorHourGlass;

  with mem_Fields do
  begin
    sObjName:= mem_Objects.FieldByName(FLD_NAME).AsString;
    sTblName:= gl_Obj.ItemByName[sObjName].TableName;
    sDBName:= FieldByName(FLD_NAME).AsString;

    gl_Db.BeginTrans1;

    bRes:= true;
    if sDBName<>'' then
      bRes:= gl_DB.DropTableField (sTblName, sDBName);

    if Eq(FieldByName(FLD_TYPE).AsString, TYPE_STATUS) then begin
      if bRes then begin
        iStatusID:= gl_Db.GetID(TBL_STATUS, [db_Par(FLD_NAME, FieldByName(FLD_STATUS_NAME).AsString)] );
        gl_Db.DeleteRecords(TBL_STATUS, FLD_PARENT_ID, iStatusID);
        gl_Db.DeleteRecord(TBL_STATUS, iStatusID);
      end;
    end;

    if bRes then begin
      gl_DB.DeleteRecord (TBL_OBJECT_FIELDS_USER, FieldByName(FLD_ID).AsInteger);
      Delete;
    end;

    if bRes then
      gl_Db.CommitTrans
    else
      gl_Db.RollbackTrans;
  end;

  if sDBName<>'' then begin
    gl_DB.UpdateViews(nil, false);
    gl_DB.UpdateViews();
  end;

  CursorDefault;
end;


//--------------------------------------------------------------------
procedure TdmObject_fields.mem_FieldsFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
//--------------------------------------------------------------------
begin
  Accept:= mem_Fields.FieldByName(FLD_OBJECT_ID).AsInteger = FilterID;
end;


end.

