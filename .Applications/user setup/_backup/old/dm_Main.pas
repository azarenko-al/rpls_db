unit dm_Main;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms,
  u_func_DB;

type
  Tdata_Main = class(TDataModule)
    ADOConnection1: TADOConnection;

    procedure qry_ObjectsAfterScroll(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);

  private
    procedure ObjectAdd;
    procedure ObjectDelete;

    procedure FieldAdd;
    procedure FieldDelete;

  //    ADOConnection: TADOConnection;
    { Private declarations }
  public


  end;

var
  data_Main: Tdata_Main;

const
   TBL_OBJECTS        = '_objects';
   TBL_OBJECT_FIELDS  = '_object_fields';

   //_object_querys

   FLD_ID           = 'id';
   FLD_PARENT_ID    = 'Parent_id';
   FLD_OBJECT_ID    = 'object_id';
   FLD_FIELD_ID     = 'field_id';

   FLD_FIELD_NAME   = 'FieldName';
   FLD_TABLE_NAME   = 'Tablename';
   FLD_INDEX        = 'index_';
   FLD_TABLENAME    = 'TableName';

   SQL_SELECT_FIELDS =
      'SELECT * FROM ' + TBL_OBJECT_FIELDS +
      '  WHERE object_id=:object_id '+
      '  ORDER BY parent_id, index_';



//============================================================================//
implementation {$R *.dfm}
//============================================================================//

//============================================================================//
procedure Tdata_Main.DataModuleCreate(Sender: TObject);
//============================================================================//
begin
  gl_DB.ADOConnection:=ADOConnection1;

  {
  qry_Objects.Connection:=ADOConnection;
  qry_Fields.Connection:=ADOConnection;

  qry_Objects.Active:=true;
  qry_Fields.Active:=true;
  }
end;

//============================================================================//
//  ���������� �������
//
//============================================================================//
procedure Tdata_Main.ObjectAdd;
begin
//  qry_Objects.Append;
end;

//============================================================================//
//  �������� �������
//
//============================================================================//
procedure Tdata_Main.ObjectDelete;
begin
{
  gl_db.DeleteRecord (TBL_OBJECTS, qry_Objects.FieldByName(FLD_ID).AsInteger);
  qry_Objects.Requery();
  }
end;

//============================================================================//
// ���������� ���� �������
//
//============================================================================//
procedure Tdata_Main.FieldAdd;
//============================================================================//
var sql: string;
    id,iMaxValue,id_object: integer;
begin
//  db_PostDataset (qry_Fields);
//          SQL_SELECT_ORDER_FIELD = 'SELECT MAX(showorder) FROM _object_fields WHERE (object_id=%d)';
{
    id_object:=qry_Objects.FieldByName(FLD_ID).AsInteger;

    iMaxValue:=gl_db.GetMaxFieldValue (
                                    TBL_OBJECT_FIELDS,
                                    FLD_INDEX,
                                    [db_Par (FLD_OBJECT_ID, id_object)]
                                   );
 }

 // if qry_Fields.State=dsEdit then qry_Fields.Post;
//  iValue:=dmMain.GetNextOrder(mtField,
//      [qry_Fields.FieldByName(FLD_ID).AsInteger]);

//  id_object:=qry_Objects.FieldByName(FLD_ID).AsInteger;

  gl_db.AddRecord(TBL_OBJECT_FIELDS,
     [db_Par (FLD_OBJECT_ID,  id_object),
      db_Par (FLD_INDEX, iMaxValue)
     ]);

     {
  sql:=ReplaceStr(SQL_INSERT_FIELD,':object_id', qry_Fields.FieldByName(FLD_ID).AsString);
  sql:=ReplaceStr(sql,':showorder',IntToStr(dmMain.GetNextOrder(mtField,[qry_Fields.FieldByName(FLD_ID).AsInteger])));
  dmMain.ADOConnection.Execute(sql);
}

//////  db_RefreshQuery (qry_Fields, gl_db.GetIdent );
end;

//============================================================================//
// �������� ���� �������
//
//============================================================================//
procedure Tdata_Main.FieldDelete;
var  object_id: integer;
 //    sql: string;
begin
{
  object_id:=qry_Fields.FieldByName(FLD_OBJECT_ID).AsInteger;
  gl_db.DeleteRecord (TBL_OBJECT_FIELDS, qry_Fields.FieldByName(FLD_ID).AsInteger);

  gl_db.UpdateOrderIndex (TBL_OBJECT_FIELDS,
                       FLD_INDEX,
                       [db_Par (FLD_OBJECT_ID, object_id)] );

  qry_Fields.Requery();
}
  {
  dmMain.adc.BeginTrans;
  try
    ord:=qry_Fields.FieldByName('showorder').AsInteger;
    dmMain.adc.Execute(Format(SQL_DELETE_FIELD,[qry_Fields.FieldByName(FLD_ID).AsInteger]));

    sql:=ReplaceStr(SQL_UPDATE_FIELD_ORDER,':object_id',qry_Fields.FieldByName(FLD_ID).AsString);
    sql:=ReplaceStr(sql,':showorder',IntToStr(ord));
    dmMain.adc.Execute(sql);
    dmMain.adc.CommitTrans;
  except
    dmMain.adc.RollbackTrans;
  end;
  db_RefreshQuery(qry_Fields);
  }
end;


procedure Tdata_Main.qry_ObjectsAfterScroll(DataSet: TDataSet);
begin
{
  db_OpenQuery (qry_Fields, SQL_SELECT_FIELDS,
               [db_Par (FLD_OBJECT_ID, qry_Objects.FieldByName(FLD_ID).AsInteger)]);
               }
end;


end.
