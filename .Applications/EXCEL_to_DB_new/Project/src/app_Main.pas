unit app_Main;

interface

uses
  Windows, Classes, 

  u_reg,
  f_Main,

  u_vars,

  u_const_msg,

  dm_Main

  ;

type
  TdmMain_app__xmldb = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);

  end;

var
  dmMain_app__xmldb: TdmMain_app__xmldb;

//==============================================================
implementation {$R *.DFM}
//==============================================================


procedure PostMsg;
var
  h: hwnd;
begin
    h:=FindWindow (PChar('Tfrm_Main_MDI'), nil);
    if h>0 then
      SendMessage (h, WM_EXTERNAL_PROJECT_IMPORT_DONE,0,0);

end;



//--------------------------------------------------------------
procedure TdmMain_app__xmldb.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const
//  TEMP_INI_FILE = 'c:\temp\rpls\Project_Import.ini';
  DEF_SOFTWARE_NAME = 'RPLS_DB_LINK';
  DEF_TEMP_FILENAME = 'Project_Import.ini';


var
  b: boolean;
  iProjectID: integer;
  sIniFileName: string;

//  oInifile: TIniFile;
begin
  //Exit;

 /// gl_Reg.
//  gl_Reg:=TRegStore_Create ('XML-DB');


  sIniFileName:=ParamStr(1);

  if sIniFileName='' then
//    sIniFileName:=TEMP_INI_FILE;
    sIniFileName:=g_ApplicationDataDir+ DEF_TEMP_FILENAME;


(*
  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;
*)

  TdmMain.Init;
  if not dmMain.OpenDB_reg then
    Exit;


 // if not dmMain.OpenDB then
   // Exit;

  iProjectID:=ini_ReadInteger(sIniFileName, 'main', 'Project_ID',  0);

  if iProjectID>0 then
    dmMain.ProjectID:=iProjectID;


  // ShowMessage('dmMain.ProjectID:=735;');
 /////////
 // dmMain.ProjectID:=495;


 //zzzzzz dmMain.ProjectID:=433;

 //IsPositiveResult
               //

 // TdmDB_MDB.Init;

 // Tdmact_Explorer.Init;
//z  TdmAct_Clutter.Init;
 //z TdmAct_CalcModel.Init;


  b:=Tfrm_Main.ExecDlg;

  if b then
  begin
    PostMsg ();

    ini_WriteBool(sIniFileName, 'main', 'result', b);
    ini_WriteBool(sIniFileName, 'result', 'status', True);

  end;


//  if Tfrm_Main.Create(Application).ShowModal = mrOk then
//    ExitProcess(1)
 // else
  //  ExitProcess(0);

 // ExitProcess(1);

{
 if Tdlg_Property_And_Link_Import.ExecDlg (iProjectID) then
    ExitProcess(1)
  else
    ExitProcess(0);
}

end;



end.
