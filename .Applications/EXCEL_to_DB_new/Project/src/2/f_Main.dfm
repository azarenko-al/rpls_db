inherited frm_Main: Tfrm_Main
  Left = 531
  Top = 210
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = 'frm_Main'
  ClientHeight = 504
  ClientWidth = 648
  OldCreateOrder = True
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 469
    Width = 648
    Caption = 'test'
    Visible = False
    inherited Bevel1: TBevel
      Width = 648
    end
    inherited Panel3: TPanel
      Left = 388
      Width = 260
      inherited btn_Ok: TButton
        Left = 13
      end
      inherited btn_Cancel: TButton
        Left = 97
      end
    end
    object Button4: TButton
      Left = 40
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Button4'
      TabOrder = 1
      OnClick = Button4Click
    end
    object Button1: TButton
      Left = 160
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Import'
      TabOrder = 2
      OnClick = Button1Click
    end
  end
  inherited pn_Top_: TPanel
    Width = 648
    inherited Bevel2: TBevel
      Width = 648
    end
    inherited pn_Header: TPanel
      Width = 648
    end
  end
  object GSPages1: TGSPages [2]
    Left = 0
    Top = 60
    Width = 648
    Height = 313
    Align = alTop
    ActivePage = GSPage1
    TabHidden = True
    object GSPage1: TGSPage
      Left = 0
      Top = 23
      Width = 648
      Height = 290
      HorzScrollBar.Smooth = True
      HorzScrollBar.Tracking = True
      VertScrollBar.Smooth = True
      VertScrollBar.Tracking = True
      Align = alClient
      TabOrder = 0
      Caption = 'GSPage1'
      object Panel1: TPanel
        Left = 0
        Top = 255
        Width = 648
        Height = 35
        Align = alBottom
        BevelOuter = bvNone
        Constraints.MaxHeight = 35
        Constraints.MinHeight = 35
        TabOrder = 0
        OnClick = Panel1Click
        DesignSize = (
          648
          35)
        object Bevel3: TBevel
          Left = 0
          Top = 0
          Width = 648
          Height = 2
          Align = alTop
          Shape = bsTopLine
        end
        object btn_Close: TButton
          Left = 569
          Top = 5
          Width = 75
          Height = 23
          Action = act_Cancel
          Anchors = [akTop, akRight]
          Cancel = True
          ModalResult = 2
          TabOrder = 0
        end
        object Button3: TButton
          Left = 476
          Top = 5
          Width = 87
          Height = 23
          Action = act_ConvProject
          Anchors = [akTop, akRight]
          Default = True
          TabOrder = 1
        end
        object Button2: TButton
          Left = 11
          Top = 5
          Width = 105
          Height = 23
          Action = act_Project_Clear
          TabOrder = 2
          Visible = False
        end
      end
      object StatusBar1: TStatusBar
        Left = 0
        Top = 236
        Width = 648
        Height = 19
        Panels = <>
        SimplePanel = True
        OnDblClick = StatusBar1DblClick
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 648
        Height = 49
        Align = alTop
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1087#1088#1086#1077#1082#1090#1072' '#1074' DB:'
        TabOrder = 2
        DesignSize = (
          648
          49)
        object DBLookupComboBox_Projects: TDBLookupComboBox
          Left = 8
          Top = 18
          Width = 627
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          DropDownRows = 20
          KeyField = 'id'
          ListField = 'name'
          ListSource = ds_Projects
          TabOrder = 0
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 49
        Width = 648
        Height = 48
        Align = alTop
        Caption = #1060#1072#1081#1083' XML '#1087#1088#1086#1077#1082#1090#1072
        TabOrder = 3
        DesignSize = (
          648
          48)
        object ed_FileName: TFilenameEdit
          Left = 8
          Top = 18
          Width = 625
          Height = 22
          Anchors = [akLeft, akTop, akRight]
          NumGlyphs = 1
          TabOrder = 0
        end
      end
    end
    object GSPage2: TGSPage
      Left = 0
      Top = 23
      Width = 648
      Height = 290
      HorzScrollBar.Smooth = True
      HorzScrollBar.Tracking = True
      VertScrollBar.Smooth = True
      VertScrollBar.Tracking = True
      Align = alClient
      TabOrder = 1
      Visible = False
      TabVisible = False
      Caption = 'GSPage2'
    end
  end
  object ActionList2: TActionList
    OnUpdate = ActionList1Update
    Left = 257
    Top = 4
    object act_ConvProject: TAction
      Caption = #1050#1086#1085#1074#1077#1088#1090#1072#1094#1080#1103
      OnExecute = _Action
    end
    object act_ConvDicts: TAction
      Caption = #1050#1086#1085#1074#1077#1088#1090#1072#1094#1080#1103' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1086#1074
      OnExecute = _Action
    end
    object act_Project_Clear: TAction
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1087#1088#1086#1077#1082#1090
      OnExecute = _Action
    end
  end
  object qry_Projects: TADOQuery
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'SELECT id,name FROM PROJECT ORDER BY name')
    Left = 18
    Top = 379
  end
  object ds_Projects: TDataSource
    DataSet = qry_Projects
    Left = 60
    Top = 380
  end
end
