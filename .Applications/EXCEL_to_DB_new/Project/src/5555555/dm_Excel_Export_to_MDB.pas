unit dm_Excel_Export_to_MDB;

interface

uses
  SysUtils, Classes,  Forms, dxmdaset, DB, RxMemDS, ADODB,



  u_Config_File,

  u_func,

  u_files,

  u_import_classes,

  u_geo_convert_new,

 // dm_MDB,

  u_db_mdb,

  u_geo,

  u_db
  ;

type
  TdmExcel_Export_to_MDB = class(TDataModule)
    ADOConnection_out: TADOConnection;
    ADOTable_out: TADOTable;
  private
    procedure CreateMDB(aFileName: string);
    { Private declarations }
  public
    class procedure Init;

    procedure SaveToExportMDB(aFileName: string; aPropertyList: TPropertyList);

  end;

var
  dmExcel_Export_to_MDB: TdmExcel_Export_to_MDB;

implementation

{$R *.dfm}

class procedure TdmExcel_Export_to_MDB.Init;
begin
  if not Assigned(dmExcel_Export_to_MDB) then
    Application.CreateForm(TdmExcel_Export_to_MDB, dmExcel_Export_to_MDB);
end;


// ---------------------------------------------------------------
procedure TdmExcel_Export_to_MDB.SaveToExportMDB(aFileName: string; aPropertyList:
    TPropertyList);
// ---------------------------------------------------------------
var
  I: Integer;   
  oProperty: TProperty;
begin

  CreateMDB(aFileName);


  for i:=0 to aPropertyList.Count-1 Do
  begin
    oProperty := aPropertyList[i];


    db_AddRecord_(ADOTable_out,
     [
       FLD_NAME,    oProperty.Name,
       FLD_ADDRESS, oProperty.Address,
       FLD_TOWER_HEIGHT, oProperty.TOWER_HEIGHT,

      // FLD_TOWER_HEIGHT,

       FLD_LAT,     oProperty.BLPoint.B,
       FLD_LON,     oProperty.BLPoint.L

//       FLD_LAT_STR, geo_FormatDegree(oProperty.BLPoint.B),
//       FLD_LON_STR, geo_FormatDegree(oProperty.BLPoint.L)


      ] );
  end;

  ADOConnection_out.Close;

end;
              

// ---------------------------------------------------------------
procedure TdmExcel_Export_to_MDB.CreateMDB(aFileName: string);
// ---------------------------------------------------------------

    procedure DoAdd();
    var
      sSQL: string;
    begin
      sSQL :=mdb_MakeCreateSQL ('property',
         [
          db_Field(FLD_ID,      ftAutoInc),
          db_Field(FLD_NAME,    ftString, 200),

          db_Field(FLD_ADDRESS, ftString, 200),
//          db_Field(FLD_B, ftString, 200),

          db_Field(FLD_TOWER_HEIGHT, ftFloat),

          db_Field(FLD_LAT,     ftFloat),
          db_Field(FLD_LON,     ftFloat)

   //       db_Field(FLD_LAT_STR, ftString),
    //      db_Field(FLD_LON_STR, ftString)

         ]);
             
      ADOConnection_out.Execute(sSQL);
    end;


 // end;
  // ---------------------------------------------------------------


begin
  mdb_CreateFile (aFileName);

  mdb_OpenConnection(ADOConnection_out, aFileName);
  DoAdd();

  db_TableOpen1(ADOTable_out, 'property');


end;



end.
