unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  ValEdit, DBGrids, Mask, rxToolEdit, RxMemDS, ToolWin, ActnList,
  rxPlacemnt, ExtCtrls, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  Dialogs, DB, ADODB, Grids, ComCtrls, StdCtrls, DBCtrls,

  dm_Excel_to_DB,

 // u_import_classes,

  u_func,

  u_dlg,

  dm_MDB,

  u_geo,

  u_db,

  dm_Excel,

  cxGrid
  ;

type
  TForm1 = class(TForm)
    PageControl1: TPageControl;
    TabSheet1_Excel_Data: TTabSheet;
    TabSheet_Columns: TTabSheet;
    StringGrid1: TStringGrid;
    DBGrid_Columns: TDBGrid;
    ToolBar1: TToolBar;
    TabSheet_MDB: TTabSheet;
    DBGrid1: TDBGrid;
    ToolBar2: TToolBar;
    Button3: TButton;
    ActionList1: TActionList;
    act_LoadDataFromExcel: TAction;
    ToolBar3: TToolBar;
    Button5: TButton;
    act_LoadColumns: TAction;
    ToolBar4: TToolBar;
    Label1: TLabel;
    ToolButton1: TToolButton;
    act_SaveDataToDB: TAction;
    Button6: TButton;
    FormStorage1: TFormStorage;
    Panel1: TPanel;
    act_SaveToExportMDB: TAction;
    Button1: TButton;
    ToolButton2: TToolButton;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    cxGrid1DBTableView1address: TcxGridDBColumn;
    cxGrid1DBTableView1lat: TcxGridDBColumn;
    cxGrid1DBTableView1lon: TcxGridDBColumn;
    cxGrid1DBTableView1ground_height: TcxGridDBColumn;
    cxGrid1DBTableView1tower_height: TcxGridDBColumn;
    cxGrid1DBTableView1town: TcxGridDBColumn;
    cxGrid1DBTableView1property_status: TcxGridDBColumn;
    cxGrid1DBTableView1comments: TcxGridDBColumn;
    cxGrid1DBTableView1status: TcxGridDBColumn;
    cxGrid1DBTableView1lat_str: TcxGridDBColumn;
    cxGrid1DBTableView1lon_str: TcxGridDBColumn;
    cxGrid1DBTableView1code: TcxGridDBColumn;
    cxGrid1DBTableView1region: TcxGridDBColumn;
    cxGrid1DBTableView1city: TcxGridDBColumn;
    SaveDialog1: TSaveDialog;
    TabSheet_File: TTabSheet;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    ed_FileName: TFilenameEdit;
    cb_Coord: TComboBox;
    act_Open: TAction;
    Button2: TButton;
    DBEdit1: TDBEdit;
    GroupBox1: TGroupBox;
    ListBox1: TListBox;
    procedure act_LoadDataFromExcelExecute(Sender: TObject);
    procedure ed_FileNameAfterDialog(Sender: TObject; var Name: string; var Action:
        Boolean);
    procedure FormCreate(Sender: TObject);
  private  
  public
  end;

var
  Form1: TForm1;

implementation


{$R *.dfm}



procedure TForm1.act_LoadDataFromExcelExecute(Sender: TObject);
var
  iCoordSys: Integer;
  iFirstLine: Integer;
  sFileName: string;
begin
  // -------------------------
  if Sender=act_Open then
  // -------------------------
  begin
    dmExcel_to_DB.OpenExcelFile (ed_FileName.FileName, ListBox1.Items);

    if ListBox1.Items.Count>0 then
      ListBox1.ItemIndex := 0;

//    dmExcel_to_DB.LO LoadDataFromExcel(iFirstLine, StringGrid1);
  end else

  // -------------------------
  if Sender=act_LoadDataFromExcel then
  // -------------------------
  begin
    iFirstLine := StrToInt( Trim(ed_First_Row.Text));

  //  LoadDataFromExcel();



//    dmExcel_to_DB.OpenExcelFile (ed_FileName.FileName, 1);
    dmExcel_to_DB.LoadDataFromExcel(ed_FileName.FileName, iFirstLine, StringGrid1, ListBox1.ItemIndex);
  end else

  // -------------------------
  if Sender=act_LoadColumns then
  begin
    dmExcel_to_DB.LoadColumns();
  end else

  // -------------------------
  if Sender=act_SaveDataToDB then
  begin
    dmExcel_to_DB.SaveDataToDB();

    cxGrid1DBTableView1.ApplyBestFit();
  end else

  // -------------------------
  if Sender=act_SaveToExportMDB then
  begin
    if SaveDialog1.Execute then
      sFileName :=SaveDialog1.FileName
    else
      Exit;


//    dmExcel_to_DB.SaveToExportMDB('s:\111.mdb');
    case cb_Coord.ItemIndex of
      0: iCoordSys:=EK_CK_42;
      1: iCoordSys:=EK_WGS84;
    else
      iCoordSys:=EK_CK_42;
    end;

    dmExcel_to_DB.SaveToExportMDB(sFileName,iCoordSys);

  end;


end;

procedure TForm1.ed_FileNameAfterDialog(Sender: TObject; var Name: string; var
    Action: Boolean);
begin
  ListBox1.Items.Clear;

//  ShowMessage('procedure TForm1.ed_FileNameAfterDialog(ction: Boolean);');
end;


// ---------------------------------------------------------------
procedure TForm1.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  Caption :='����������� Excel -> MDB '+ GetAppVersionStr();


  PageControl1.Align:=alClient;
  PageControl1.ActivePageIndex :=0;

  ed_FileName.Filter := 'Excel (*.xls)|*.xls';

//  TabSheet1_Excel_Data.Caption :=''


  StringGrid1.Align:=alClient;
  StringGrid1.RowCount := 2;
  StringGrid1.ColCount := 1;

  cxGrid1.Align:=alClient;

//  DBGrid1.Align:=alClient;
  DBGrid_Columns.Align:=alClient;

  TdmExcel_to_DB.Init;


  act_Open.Caption :='�������';
  act_LoadDataFromExcel.Caption :='��������� ������ �� Excel';
  act_LoadColumns.Caption       :='��������� ���������';
  act_SaveDataToDB.Caption      :='���������� ������';
  act_SaveToExportMDB.Caption   :='������� � MDB';

  TabSheet_File.Caption  :='����';
  TabSheet1_Excel_Data.Caption  :='Excel';
  TabSheet_Columns.Caption      :='���������';
  TabSheet_MDB.Caption          :='������';


//  FColumns := TStringList.Create();

 // GetData;

end;



end.
