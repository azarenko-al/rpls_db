unit f_Data_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid, Grids, DBGrids, StdCtrls, ToolWin, ComCtrls, StdActns, ActnList,


  cxDropDownEdit,

 // dm_Excel_Export_to_MDB,
  dm_Config_mdb,

  dm_Excel_to_classes,

  u_const_db,


 // dm_Excel_to_DB,

  dm_Excel_file,
  u_import_classes,

 // u_import_classes,

  u_cx_vgrid,

  u_func,
  u_common_const,

  u_geo,

  u_db,

  u_Config_File,

  rxPlacemnt, Mask, rxToolEdit, cxInplaceContainer, cxTL,
  cxTLData, cxDBTL

  ;

type
  Tfrm_Data_view = class(TForm)
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2TableView_Data: TcxGridTableView;
    cxGrid2Level1: TcxGridLevel;
    pn_Fields: TPanel;
    Splitter1: TSplitter;
    cxGrid1: TcxGrid;
    cxGridDBTableView_Columns: TcxGridDBTableView;
    col_index: TcxGridDBColumn;
    col_header: TcxGridDBColumn;
    col_name: TcxGridDBColumn;
    col_key: TcxGridDBColumn;
    cxGridLevel2: TcxGridLevel;
    ActionList1: TActionList;
    act_LoadDataFromExcel: TAction;
    act_SaveToExportMDB: TAction;
    act_FileOpen1: TFileOpen;
    ToolBar1: TToolBar;
    Button3: TButton;
    FormStorage1: TFormStorage;
    Panel2: TPanel;
    Label7: TLabel;
    ed_Filename: TFilenameEdit;
    Button1: TButton;
    Button2: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGrid1: TDBGrid;
    TabSheet2: TTabSheet;
    cxGridDBTableView_ColumnsColumn1: TcxGridDBColumn;
    Panel1: TPanel;
    cxDBTreeList2: TcxDBTreeList;
    cxDBTreeListColumn1: TcxDBTreeListColumn;
    cxDBTreeListColumn2: TcxDBTreeListColumn;
    cxDBTreeListColumn3: TcxDBTreeListColumn;
    cxDBTreeListColumn4: TcxDBTreeListColumn;
    cxDBTreeListColumn5: TcxDBTreeListColumn;
    cxDBTreeListColumn6: TcxDBTreeListColumn;
    cxDBTreeListColumn7: TcxDBTreeListColumn;
    cxDBTreeListColumn8: TcxDBTreeListColumn;
    cxDBTreeListColumn9: TcxDBTreeListColumn;
    Button4: TButton;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure act_LoadDataFromExcelExecute(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure col_namePropertiesCloseUp(Sender: TObject);
  //  procedure Button3Click(Sender: TObject);
  //
  //  procedure col_namePropertiesEditValueChanged(Sender: TObject);
 //   procedure ToolButton1Click(Sender: TObject);
  private
    FFilledColumnIndexList: TStringList;


    procedure LoadDataFromExcel;
    procedure SaveToExportMDB;

  public
    class function ExecDlg: boolean;


    { Public declarations }
  end;

var
  frm_Data_view: Tfrm_Data_view;

implementation

uses dm_Export_classes_to_MDB;



{$R *.dfm}


// ---------------------------------------------------------------
class function Tfrm_Data_view.ExecDlg: boolean;
// ---------------------------------------------------------------
begin
  with Tfrm_Data_view.Create(Application) do
  begin

    ShowModal;

    Free;
  end;


end;

// ---------------------------------------------------------------
procedure Tfrm_Data_view.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  cxGrid2.Align:=alClient;

  FFilledColumnIndexList:=TStringList.Create;


  LoadDataFromExcel();

end;



procedure Tfrm_Data_view.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FFilledColumnIndexList);
end;


// ---------------------------------------------------------------
procedure Tfrm_Data_view.SaveToExportMDB;
// ---------------------------------------------------------------
var
  sFileName: string;
begin
  sFileName := ed_FileName.FileName;
//    's:\1111.mdb';


//    dmExcel_to_DB.Params.CoordSystem := iCoordSys;
//    dmExcel_to_DB.Params.FirstHeaderLine := 1;

  dmExcel_to_Classes.ExportToClasses_Property();

  dmExport_Classes_to_MDB.SaveToExportMDB(sFileName, dmExcel_to_Classes.Project.PropertyList);

end;


// ---------------------------------------------------------------
procedure Tfrm_Data_view.act_LoadDataFromExcelExecute(Sender: TObject);

begin
 // -------------------------
  if Sender=act_LoadDataFromExcel then
  // -------------------------
  begin
    LoadDataFromExcel
  end else

  // -------------------------
  if Sender=act_SaveToExportMDB then
  // -------------------------
  begin
    SaveToExportMDB


  end;

end;


procedure Tfrm_Data_view.Button2Click(Sender: TObject);
begin
  dmExcel_to_Classes.SaveColumns;

end;

procedure Tfrm_Data_view.Button4Click(Sender: TObject);
var
  oProperties: TcxPopupEditProperties;
  sFileName: string;

begin
  //cxGridDBTableView_ColumnsColumn1.Properties.

  oProperties:=TcxPopupEditProperties(cxGridDBTableView_ColumnsColumn1.Properties);

  GetParentForm(oProperties.PopupControl).Hide;
                          

end;


// ---------------------------------------------------------------
procedure Tfrm_Data_view.LoadDataFromExcel;
// ---------------------------------------------------------------
begin
//  g_ConfigFile.DataSourceType:=

  dmConfig_mdb.OpenFieldsDataset(g_ConfigFile.DataSourceType);

  dmExcel_to_Classes.LoadDataFromExcel(
      g_ConfigFile.FileName,

      cxGrid2TableView_Data );

 // cxGrid2TableView_Data.ApplyBestFit();

end;


procedure Tfrm_Data_view.col_namePropertiesCloseUp(Sender: TObject);
var
  k: Integer;
begin
  k:=cxGridDBTableView_Columns.DataController.FocusedRecordIndex;

  if FFilledColumnIndexList.IndexOfObject(Pointer(k))<0 then
    FFilledColumnIndexList.AddObject( IntToStr(k), Pointer(k) )

  //
end;

end.


