object frm_Data_view: Tfrm_Data_view
  Left = 877
  Top = 291
  Width = 825
  Height = 540
  Caption = 'frm_Data_view'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 277
    Width = 817
    Height = 3
    Cursor = crVSplit
    Align = alBottom
  end
  object cxGrid2: TcxGrid
    Left = 0
    Top = 33
    Width = 817
    Height = 96
    Align = alTop
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    object cxGrid2DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
    end
    object cxGrid2TableView_Data: TcxGridTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnMoving = False
      OptionsView.GroupByBox = False
    end
    object cxGrid2Level1: TcxGridLevel
      GridView = cxGrid2TableView_Data
    end
  end
  object pn_Fields: TPanel
    Left = 0
    Top = 280
    Width = 817
    Height = 168
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object cxGrid1: TcxGrid
      Left = 0
      Top = 0
      Width = 505
      Height = 168
      Align = alLeft
      TabOrder = 0
      LookAndFeel.Kind = lfFlat
      object cxGridDBTableView_Columns: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dmExcel_to_Classes.ds_Columns
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnFiltering = False
        OptionsCustomize.ColumnGrouping = False
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        object col_index: TcxGridDBColumn
          DataBinding.FieldName = 'index'
          PropertiesClassName = 'TcxTextEditProperties'
          Options.Editing = False
          Width = 59
        end
        object col_header: TcxGridDBColumn
          DataBinding.FieldName = 'header'
          Options.Editing = False
          Width = 121
        end
        object col_name: TcxGridDBColumn
          Caption = 'name'
          DataBinding.FieldName = 'field_name'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.DropDownRows = 20
          Properties.ImmediatePost = True
          Properties.KeyFieldNames = 'name'
          Properties.ListColumns = <
            item
              FieldName = 'caption'
            end>
          Properties.ListSource = dmConfig_mdb.ds_Fields
          Properties.OnCloseUp = col_namePropertiesCloseUp
          Width = 126
        end
        object col_key: TcxGridDBColumn
          DataBinding.FieldName = 'key'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.NullStyle = nssUnchecked
          Width = 49
        end
        object cxGridDBTableView_ColumnsColumn1: TcxGridDBColumn
          PropertiesClassName = 'TcxPopupEditProperties'
        end
      end
      object cxGridLevel2: TcxGridLevel
        GridView = cxGridDBTableView_Columns
      end
    end
    object PageControl1: TPageControl
      Left = 528
      Top = 0
      Width = 289
      Height = 168
      ActivePage = TabSheet2
      Align = alRight
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = 'TabSheet1'
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 281
          Height = 140
          Align = alClient
          DataSource = dmConfig_mdb.ds_Fields
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'TabSheet2'
        ImageIndex = 1
        object Panel1: TPanel
          Left = 0
          Top = 3
          Width = 241
          Height = 121
          Caption = 'Panel1'
          TabOrder = 0
          object cxDBTreeList2: TcxDBTreeList
            Left = 1
            Top = 1
            Width = 239
            Height = 72
            Align = alTop
            Bands = <
              item
              end>
            DataController.DataSource = dmConfig_mdb.ds_Fields
            DataController.ParentField = 'parent_id'
            DataController.KeyField = 'id'
            LookAndFeel.Kind = lfFlat
            RootValue = -1
            TabOrder = 0
            object cxDBTreeListColumn1: TcxDBTreeListColumn
              DataBinding.FieldName = 'id'
              Width = 46
              Position.ColIndex = 0
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object cxDBTreeListColumn2: TcxDBTreeListColumn
              DataBinding.FieldName = 'category'
              Width = 100
              Position.ColIndex = 1
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object cxDBTreeListColumn3: TcxDBTreeListColumn
              DataBinding.FieldName = 'name'
              Width = 100
              Position.ColIndex = 2
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object cxDBTreeListColumn4: TcxDBTreeListColumn
              DataBinding.FieldName = 'is_virtual'
              Width = 100
              Position.ColIndex = 3
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object cxDBTreeListColumn5: TcxDBTreeListColumn
              DataBinding.FieldName = 'caption'
              Width = 100
              Position.ColIndex = 4
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object cxDBTreeListColumn6: TcxDBTreeListColumn
              DataBinding.FieldName = 'comment'
              Width = 100
              Position.ColIndex = 5
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object cxDBTreeListColumn7: TcxDBTreeListColumn
              DataBinding.FieldName = 'FieldType'
              Width = 100
              Position.ColIndex = 6
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object cxDBTreeListColumn8: TcxDBTreeListColumn
              DataBinding.FieldName = 'parent_id'
              Width = 100
              Position.ColIndex = 7
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object cxDBTreeListColumn9: TcxDBTreeListColumn
              DataBinding.FieldName = 'is_category'
              Width = 100
              Position.ColIndex = 8
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
          end
          object Button4: TButton
            Left = 8
            Top = 80
            Width = 75
            Height = 25
            Caption = 'Button4'
            TabOrder = 1
            OnClick = Button4Click
          end
        end
      end
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 817
    Height = 33
    BorderWidth = 1
    Caption = 'ToolBar1'
    TabOrder = 2
    object Button3: TButton
      Left = 0
      Top = 2
      Width = 153
      Height = 22
      Action = act_LoadDataFromExcel
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 448
    Width = 817
    Height = 65
    Align = alBottom
    Caption = 'Panel1'
    TabOrder = 3
    DesignSize = (
      817
      65)
    object Label7: TLabel
      Left = 8
      Top = 13
      Width = 57
      Height = 13
      Caption = #1048#1084#1103' '#1092#1072#1081#1083#1072
    end
    object ed_Filename: TFilenameEdit
      Left = 8
      Top = 28
      Width = 425
      Height = 21
      Filter = 'All files (*.*)|*.*|qqq|*.33'
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 0
      Text = 'd:\mts.mdb'
    end
    object Button1: TButton
      Left = 601
      Top = 26
      Width = 152
      Height = 22
      Action = act_SaveToExportMDB
      TabOrder = 1
    end
    object Button2: TButton
      Left = 464
      Top = 24
      Width = 113
      Height = 25
      Caption = 'SaveColumns'
      TabOrder = 2
      OnClick = Button2Click
    end
  end
  object ActionList1: TActionList
    Left = 24
    Top = 160
    object act_LoadDataFromExcel: TAction
      Caption = 'LoadDataFromExcel'
      OnExecute = act_LoadDataFromExcelExecute
    end
    object act_SaveToExportMDB: TAction
      Caption = 'act_SaveToExportMDB'
      OnExecute = act_LoadDataFromExcelExecute
    end
    object act_FileOpen1: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredProps.Strings = (
      'pn_Fields.Height')
    StoredValues = <>
    Left = 104
    Top = 160
  end
end
