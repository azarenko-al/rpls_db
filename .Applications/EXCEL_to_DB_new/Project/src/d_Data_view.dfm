object dlg_Data_view: Tdlg_Data_view
  Left = 862
  Top = 241
  Caption = 'dlg_Data_view'
  ClientHeight = 528
  ClientWidth = 1172
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid_top: TcxGrid
    Left = 0
    Top = 33
    Width = 1172
    Height = 84
    Align = alTop
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    ExplicitWidth = 867
    object cxGrid_topDBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
    end
    object cxGrid_topTableView_Data: TcxGridTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object cxGrid_topTableView_DataColumn1: TcxGridColumn
        Styles.OnGetContentStyle = cxGrid2TableView_DataColumn1StylesGetContentStyle
        Width = 156
      end
      object cxGrid_topTableView_DataColumn2: TcxGridColumn
        Width = 205
      end
    end
    object cxGrid_topLevel1: TcxGridLevel
      GridView = cxGrid_topTableView_Data
    end
  end
  object pn_Fields: TPanel
    Left = 0
    Top = 305
    Width = 1172
    Height = 223
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 867
    object cxGrid_bottom: TcxGrid
      Left = 0
      Top = 0
      Width = 705
      Height = 183
      Align = alLeft
      TabOrder = 0
      LookAndFeel.Kind = lfFlat
      object cxGridDBTableView_Columns: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = ds_Data
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnFiltering = False
        OptionsCustomize.ColumnGrouping = False
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        Styles.OnGetContentStyle = cxGridDBTableView_ColumnsStylesGetContentStyle
        object col_index: TcxGridDBColumn
          DataBinding.FieldName = 'RecId'
          PropertiesClassName = 'TcxTextEditProperties'
          Options.Editing = False
          Styles.Content = cxStyle_Read_only
          Width = 23
        end
        object col_header: TcxGridDBColumn
          DataBinding.FieldName = 'FIELD_caption'
          Options.Editing = False
          Styles.Content = cxStyle_Read_only
          Width = 212
        end
        object col_name: TcxGridDBColumn
          Caption = 'name'
          DataBinding.FieldName = 'COLUMN_INDEX'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.DropDownRows = 30
          Properties.ImmediatePost = True
          Properties.KeyFieldNames = 'INDEX'
          Properties.ListColumns = <
            item
              FieldName = 'caption'
            end>
          Properties.ListOptions.AnsiSort = True
          Properties.ListOptions.ShowHeader = False
          Properties.ListSource = ds_Columns
          Properties.OnCloseUp = col_namePropertiesCloseUp
          Properties.OnValidate = col_namePropertiesValidate
          Styles.OnGetContentStyle = col_nameStylesGetContentStyle
          Width = 316
        end
        object col_is_not_null: TcxGridDBColumn
          DataBinding.FieldName = 'is_not_null'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Visible = False
          Width = 64
        end
        object col_COLUMN_INDEX: TcxGridDBColumn
          DataBinding.FieldName = 'COLUMN_INDEX'
          Visible = False
        end
      end
      object cxGridLevel2: TcxGridLevel
        GridView = cxGridDBTableView_Columns
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 183
      Width = 1172
      Height = 40
      Align = alBottom
      BevelOuter = bvNone
      Constraints.MaxHeight = 40
      Constraints.MinHeight = 40
      TabOrder = 1
      ExplicitWidth = 867
      object Bevel1: TBevel
        Left = 0
        Top = 0
        Width = 1172
        Height = 3
        Align = alTop
        ExplicitWidth = 940
      end
      object Panel3: TPanel
        Left = 0
        Top = 3
        Width = 122
        Height = 37
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Button7: TButton
          Left = 2
          Top = 6
          Width = 83
          Height = 25
          Action = act_Exec
          Default = True
          TabOrder = 0
        end
      end
      object Button4: TButton
        Left = 207
        Top = 9
        Width = 100
        Height = 25
        Caption = 'Button4'
        TabOrder = 1
        Visible = False
        OnClick = Button4Click
      end
    end
    object cxSplitter2: TcxSplitter
      Left = 705
      Top = 0
      Width = 8
      Height = 183
      HotZoneClassName = 'TcxSimpleStyle'
      Control = cxGrid_bottom
    end
    object DBGrid1: TDBGrid
      Left = 852
      Top = 0
      Width = 320
      Height = 183
      Align = alRight
      DataSource = DataSource1
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Visible = False
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1172
    Height = 33
    BorderWidth = 1
    ButtonHeight = 21
    Caption = 'ToolBar1'
    TabOrder = 2
    Visible = False
    ExplicitWidth = 867
    object Button5: TButton
      Left = 0
      Top = 0
      Width = 75
      Height = 21
      Action = act_Exec
      TabOrder = 0
    end
    object ed_Filename1: TFilenameEdit
      Left = 75
      Top = 0
      Width = 353
      Height = 21
      Filter = 'All files (*.*)|*.*|qqq|*.33'
      NumGlyphs = 1
      TabOrder = 1
      Text = 's:\_mts.mdb'
    end
    object Button2: TButton
      Left = 428
      Top = 0
      Width = 81
      Height = 21
      Caption = 'SaveColumns'
      TabOrder = 2
      Visible = False
    end
    object Button1: TButton
      Left = 509
      Top = 0
      Width = 88
      Height = 21
      Action = act_SaveToExportMDB
      TabOrder = 3
    end
    object Button3: TButton
      Left = 597
      Top = 0
      Width = 75
      Height = 21
      Caption = 'Button3'
      TabOrder = 4
      Visible = False
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 0
    Top = 117
    Width = 1172
    Height = 8
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salTop
    Control = cxGrid_top
    ExplicitWidth = 8
  end
  object ActionList1: TActionList
    Left = 72
    Top = 160
    object act_SaveToExportMDB: TAction
      Caption = 'act_SaveToExportMDB'
      OnExecute = act_LoadDataFromExcelExecute
    end
    object act_Exec: TAction
      Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100
      OnExecute = act_LoadDataFromExcelExecute
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    UseRegistry = True
    StoredProps.Strings = (
      'cxGrid_top.Height')
    StoredValues = <>
    Left = 176
    Top = 160
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 312
    Top = 160
    PixelsPerInch = 96
    object cxStyle_used: TcxStyle
      AssignedValues = [svColor]
      Color = clLime
    end
    object cxStyle_header: TcxStyle
      AssignedValues = [svColor]
      Color = clRed
    end
    object cxStyle_Column_selected: TcxStyle
      AssignedValues = [svColor]
      Color = clFuchsia
    end
    object cxStyle_not_null: TcxStyle
      AssignedValues = [svColor]
      Color = clYellow
    end
  end
  object ds_Data: TDataSource
    DataSet = dxMemData_Data
    Left = 624
    Top = 223
  end
  object dxMemData_Data: TdxMemData
    Active = True
    Indexes = <>
    SortOptions = []
    BeforePost = dxMemData_DataBeforePost
    Left = 624
    Top = 168
    object dxMemData_Dataid: TAutoIncField
      FieldName = 'id'
    end
    object dxMemData_DataFIELD_caption: TStringField
      FieldName = 'FIELD_caption'
      Size = 50
    end
    object dxMemData_DataFIELD_NAME: TStringField
      FieldName = 'FIELD_NAME'
      Size = 50
    end
    object dxMemData_DataIS_NOT_NULL: TBooleanField
      FieldName = 'IS_NOT_NULL'
    end
    object dxMemData_DataCOLUMN_INDEX: TIntegerField
      FieldName = 'COLUMN_INDEX'
    end
  end
  object dxMemData_Columns: TdxMemData
    Active = True
    Indexes = <>
    SortOptions = []
    Left = 736
    Top = 168
    object IntegerField1: TIntegerField
      FieldName = 'INDEX'
    end
    object dxMemData_Columnscaption: TStringField
      FieldName = 'caption'
      Size = 100
    end
  end
  object ds_Columns: TDataSource
    DataSet = dxMemData_Columns
    Left = 736
    Top = 223
  end
  object cxStyleRepository2: TcxStyleRepository
    Left = 464
    Top = 208
    PixelsPerInch = 96
    object cxStyle_Read_only: TcxStyle
      AssignedValues = [svColor]
      Color = clBtnFace
    end
  end
  object dxMemData1: TdxMemData
    Active = True
    Indexes = <>
    SortOptions = []
    BeforePost = dxMemData_DataBeforePost
    Left = 216
    Top = 224
    object AutoIncField1: TAutoIncField
      FieldName = 'id'
    end
    object StringField1: TStringField
      FieldName = 'FIELD_caption'
      Size = 50
    end
    object StringField2: TStringField
      FieldName = 'FIELD_NAME'
      Size = 50
    end
    object BooleanField1: TBooleanField
      FieldName = 'IS_NOT_NULL'
    end
    object IntegerField2: TIntegerField
      FieldName = 'COLUMN_INDEX'
    end
  end
  object DataSource1: TDataSource
    DataSet = dxMemData1
    Left = 304
    Top = 223
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 976
    Top = 176
  end
end
