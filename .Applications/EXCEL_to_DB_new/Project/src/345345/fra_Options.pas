unit fra_Options;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxVGrid, cxControls, cxInplaceContainer, Menus,

  cxDropDownEdit,

  u_func,

  u_Config_File
  ;

type
  Tframe_Options1 = class(TFrame)
    cxVerticalGrid1: TcxVerticalGrid;
    row_Sheets: TcxEditorRow;
    cxVerticalGrid1CategoryRow4: TcxCategoryRow;
    row_FirstRowIndex_1: TcxEditorRow;
    row_DataType: TcxEditorRow;
    row_CoordSys1: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    cxVerticalGrid1CategoryRow3: TcxCategoryRow;
    row_Gain_dB2: TcxEditorRow;
    row_Diameter2: TcxEditorRow;
    row_Height2: TcxEditorRow;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;
    row_Power_dBm2: TcxEditorRow;
    row_Bitrate_Mbps2: TcxEditorRow;
    row_Threshold_BER_32: TcxEditorRow;
    row_Threshold_BER_62: TcxEditorRow;
    PopupMenu1: TPopupMenu;
    row_Get_antenna_H_from_site: TcxEditorRow;
    cxVerticalGrid1CategoryRow5: TcxCategoryRow;
    row_Freq_MHz: TcxEditorRow;
    procedure FrameClick(Sender: TObject);
  private
    function Validated: Boolean;
    { Private declarations }
  public
    function GetSheetList: TStrings;
    procedure Init;
    procedure LoadFromClass;
    procedure Restore_Defaults;
    procedure SaveToClass;
    { Public declarations }
  end;

implementation

{$R *.dfm}

        gggggggggggggggggggg
procedure Tframe_Options1.FrameClick(Sender: TObject);
begin

end;

// ---------------------------------------------------------------
procedure Tframe_Options1.Init;
// ---------------------------------------------------------------
begin
  cxVerticalGrid1.Align:=alClient;

  row_DataType.Properties.Caption:='��� ������';
  row_Sheets.Properties.Caption:='����';
  row_Freq_MHz.Properties.Caption:='������� ��������, MHz';


  row_Get_antenna_H_from_site.Properties.Caption:='������ ������� �� ���� ������';


  row_Get_antenna_H_from_site.Visible := False;

end;


 // ---------------------------------------------------------------
procedure Tframe_Options1.Restore_Defaults;
// ---------------------------------------------------------------
begin
  g_ConfigFile.SetDefaults();
  LoadFromClass();
end;


// ---------------------------------------------------------------
function Tframe_Options1.GetSheetList: TStrings;
// ---------------------------------------------------------------
var
  oComboBoxProperties: TcxComboBoxProperties;
begin
  oComboBoxProperties:=TcxComboBoxProperties(row_Sheets.Properties.EditProperties);

// .. dmExcel_file.GetSheets (ed_FileName.FileName, ListBox1.Items);
  Result :=  oComboBoxProperties.Items;
end;



// ---------------------------------------------------------------
function Tframe_Options1.Validated: Boolean;
// ---------------------------------------------------------------
begin
  Result := AsString( row_Sheets.Properties.Value )<>'';

end;

// ---------------------------------------------------------------
procedure Tframe_Options1.SaveToClass;
// ---------------------------------------------------------------
var
  s: string;
begin

 // g_ConfigFile.

  with g_ConfigFile do
  begin
    SheetName       := row_Sheets.Properties.Value;
    FirstRowIndex_1:= AsInteger (row_FirstRowIndex_1.Properties.Value);


    Height_M        :=  AsInteger(row_Height2.Properties.Value);
    Gain         :=  AsFloat(row_Gain_dB2.Properties.Value);
    Diameter        :=  AsFloat(row_Diameter2.Properties.Value);


    Freq_MHz        :=  AsFloat(row_Freq_MHz.Properties.Value);
    Power_dBm       :=  AsFloat(row_Power_dBm2.Properties.Value);
    Threshold_BER_3 :=  AsFloat(row_Threshold_BER_32.Properties.Value);
    Threshold_BER_6 :=  AsFloat(row_Threshold_BER_62.Properties.Value);
    Bitrate_Mbps    :=  AsInteger(row_Bitrate_Mbps2.Properties.Value);


    case row_DataType.Properties.Value of
      0: DataSourceType:=dtProp;
      1: DataSourceType:=dtLink;
    end;

    s:=row_CoordSys1.Properties.Value;

    if Eq(s, 'CK42') then CoordSys := csPulkovo_CK42 else
    if Eq(s, 'CK95') then CoordSys := csPulkovo_CK95 else
    if Eq(s, 'WGS')  then CoordSys := csWGS;

//
//    case row_CoordSys.Properties.Value of
//      0: CoordSys:=csPulkovo_CK42;
//      0: CoordSys:=csPulkovo_CK42;
//      1: CoordSys:=csWGS;
//    end;



    IsGet_antenna_H_from_site := row_Get_antenna_H_from_site.Properties.Value;

  end;



end;


// ---------------------------------------------------------------
procedure Tframe_Options1.LoadFromClass;
// ---------------------------------------------------------------
var
  iValue: Integer;
  s: string;
begin
  cxVerticalGrid1.HideEdit;



 // g_ConfigFile.

  with g_ConfigFile do
  begin
    row_Sheets.Properties.Value := SheetName;

    row_FirstRowIndex_1.Properties.Value :=FirstRowIndex_1;


    row_Height2.Properties.Value          := Height_M;
    row_Gain_dB2.Properties.Value         := Gain;
    row_Diameter2.Properties.Value        := Diameter;

    row_Freq_MHz.Properties.Value       := Freq_MHz;
    row_Power_dBm2.Properties.Value       := Power_dBm;
    row_Threshold_BER_32.Properties.Value := Threshold_BER_3;
    row_Threshold_BER_62.Properties.Value := Threshold_BER_6;
    row_Bitrate_Mbps2.Properties.Value    := Bitrate_Mbps;



    case DataSourceType of
      dtProp: iValue:=0;
      dtLink: iValue:=1;
    end;


    row_DataType.Properties.Value :=iValue;


    row_Get_antenna_H_from_site.Properties.Value := IsGet_antenna_H_from_site;


    case CoordSys of
      csPulkovo_CK42: s:='CK42';
      csPulkovo_CK95: s:='CK95';
      csWGS:          s:='WGS';
    end;

    row_CoordSys1.Properties.Value:=s;

//    if Eq(s, 'CK42') then CoordSys := csPulkovo_CK42 else
//    if Eq(s, 'CK95') then CoordSys := csPulkovo_CK95 else
//    if Eq(s, 'wgs')  then CoordSys := csWGS;



(*
    case row_DataType.Properties.Value of
      0: DataType:=dtProp;
      1: DataType:=dtLink;
    end;

    case row_CoordSys.Properties.Value of
      0: CoordSys:=csPulkovo;
      1: CoordSys:=csWGS;
    end;

*)



  end;



end;


end.

    {

// ---------------------------------------------------------------
procedure Tfrm_Main.Open_sheets;
// ---------------------------------------------------------------
var
  oComboBoxProperties: TcxComboBoxProperties;
  sFileName: string;

begin
  oComboBoxProperties:=TcxComboBoxProperties(row_Sheets.Properties.EditProperties);


// .. dmExcel_file.GetSheets (ed_FileName.FileName, ListBox1.Items);
  dmExcel_file.GetSheets (ed_FileName.FileName, oComboBoxProperties.Items);

