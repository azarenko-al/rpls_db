unit dm_Export_classes_to_MDB;

interface

uses
  SysUtils, Classes,  Forms, dxmdaset, DB, RxMemDS, ADODB,

  u_Config_File,

  u_func,

  u_files,

  u_import_classes,

  u_geo_convert_new,

 // dm_MDB,

  u_db_mdb,

  u_geo,

  u_db
   ;

type
  TdmExport_classes_to_MDB = class(TDataModule)
    ADOConnection_out: TADOConnection;
    ADOTable_out: TADOTable;
  private
    procedure CreateMDB_property(aFileName: string);
    { Private declarations }
  public
    class procedure Init;

    procedure SaveToExportMDB(aFileName: string; aProject: TExcelProject);

  end;

var
  dmExport_classes_to_MDB: TdmExport_classes_to_MDB;

implementation

{$R *.dfm}

const
  FLD_LAT_CK95 = 'LAT_CK95';
  FLD_LON_CK95 = 'LON_CK95';



class procedure TdmExport_classes_to_MDB.Init;
begin
  if not Assigned(dmExport_classes_to_MDB) then
    Application.CreateForm(TdmExport_classes_to_MDB, dmExport_classes_to_MDB);
end;


// ---------------------------------------------------------------
procedure TdmExport_classes_to_MDB.SaveToExportMDB(aFileName: string; aProject:
    TExcelProject);
// ---------------------------------------------------------------
var
  I: Integer;
  oProperty: TPropertyRaw;

  CK_95,wgs: TBLPoint;

begin

  CreateMDB_property(aFileName);


  for i:=0 to aProject.PropertyList.Count-1 Do
  begin
    oProperty := aProject.PropertyList[i];

   {
  if (coords.Lat>0) and (coords.Lon>0)
   then
   begin

      oProperty.BLPoint.B := coords.Lat;
      oProperty.BLPoint.L := coords.Lon;

      case g_ConfigFile.CoordSys of
        csPulkovo_CK42: ;
        csPulkovo_CK95: oProperty.BLPoint:= geo_Pulkovo_CK_95_to_CK_42(oProperty.BLPoint);
        csWGS:          oProperty.BLPoint:= geo_WGS84_to_Pulkovo42(oProperty.BLPoint);
//        csGOST:          oProperty.BLPoint:= geo_WGS84_to_ Pulkovo42(oProperty.BLPoint);
      else
        raise exception.create('')
      end;

   end;
   }

    if (oProperty.Name<>'') and (oProperty.BLPoint.B>0) and (oProperty.BLPoint.L>0)
    then
      wgs  := geo_Pulkovo42_to_WGS84(oProperty.BLPoint);
      CK_95:= geo_Pulkovo42_to_CK_95(oProperty.BLPoint);

//    assert(oProperty.Name<>'');


      db_AddRecord_(ADOTable_out,
       [
         FLD_NAME,    oProperty.Name,

        // FLD_TOWER_HEIGHT,

         FLD_LAT,     oProperty.BLPoint.B,
         FLD_LON,     oProperty.BLPoint.L,


          FLD_LAT_WGS,   wgs.b,
          FLD_LON_WGS,   wgs.l,

          FLD_LAT_CK95,  CK_95.b,
          FLD_LON_CK95,  CK_95.l,



         FLD_ADDRESS, oProperty.Address,
         FLD_TOWER_HEIGHT, oProperty.TOWER_HEIGHT


  //       FLD_LAT_STR, geo_FormatDegree(oProperty.BLPoint.B),
  //       FLD_LON_STR, geo_FormatDegree(oProperty.BLPoint.L)


        ] );
  end;

  ADOConnection_out.Close;

end;


// ---------------------------------------------------------------
procedure TdmExport_classes_to_MDB.CreateMDB_property(aFileName: string);
// ---------------------------------------------------------------

    procedure DoAdd();
    var
      sSQL: string;
    begin
      sSQL :=mdb_MakeCreateSQL ('property',
         [
          db_Field(FLD_ID,      ftAutoInc),
          db_Field(FLD_NAME,    ftString, 200),

          db_Field(FLD_LAT,     ftFloat),
          db_Field(FLD_LON,     ftFloat),

          db_Field(FLD_LAT_WGS,     ftFloat),
          db_Field(FLD_LON_WGS,     ftFloat),

          db_Field(FLD_LAT_CK95,     ftFloat),
          db_Field(FLD_LON_CK95,     ftFloat),


          db_Field(FLD_ADDRESS, ftString, 200),
//          db_Field(FLD_B, ftString, 200),

          db_Field(FLD_TOWER_HEIGHT, ftFloat)


   //       db_Field(FLD_LAT_STR, ftString),
    //      db_Field(FLD_LON_STR, ftString)

         ]);

      ADOConnection_out.Execute(sSQL);
    end;


 // end;
  // ---------------------------------------------------------------


begin
  mdb_CreateFile (aFileName);

  mdb_OpenConnection(ADOConnection_out, aFileName);
  DoAdd();

  db_TableOpen1(ADOTable_out, 'property');


end;



end.


