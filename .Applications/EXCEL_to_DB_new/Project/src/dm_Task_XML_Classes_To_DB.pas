unit dm_Task_XML_Classes_To_DB;

interface

uses
  Classes, DB, ADODB, Forms, Variants, SysUtils,

  u_Link_const,

  u_files,
  u_DB,

  u_const_db,

  dm_MDB,

  d_Progress
  ;

type

  TdmTask_Classes_To_MDB = class(TDataModule)
    ADOTable1: TADOTable;
    t_Property: TADOTable;
    t_Project: TADOTable;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private

  private           
    FTerminated: Boolean;



    function SetProgress2(aProgress,aTotal: integer): Boolean;
  public
   // XProject_ref: TxProject;

    Params: record
            //  Project_ID : Integer;

              Template_MdbFileName : string;
              MdbFileName : string;
            end;

    procedure Execute;
    function ExecuteDlg: Boolean;

    class procedure Init;

//    procedure TestValues;
  end;



var
  dmTask_Classes_To_MDB: TdmTask_Classes_To_MDB;

implementation


{$R *.dfm}

const
  FLD_BUILDING = 'BUILDING';
      

// ---------------------------------------------------------------
class procedure TdmTask_Classes_To_MDB.Init;
// ---------------------------------------------------------------
begin
  if not Assigned(dmTask_Classes_To_MDB) then
    dmTask_Classes_To_MDB := TdmTask_Classes_To_MDB.Create(Application);
end;


// ---------------------------------------------------------------
procedure TdmTask_Classes_To_MDB.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  TdmMDB.Init;

  Assert(Assigned(dmMDB), 'Value not assigned');

  db_SetComponentADOConnection(Self, dmMDB.ADOConnection_MDB1);
end;


procedure TdmTask_Classes_To_MDB.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(dmMDB);
  inherited;
end;


function TdmTask_Classes_To_MDB.SetProgress2(aProgress,aTotal: integer):
    Boolean;
begin
  Progress_SetProgress2(aProgress,aTotal, FTerminated);
  Result := FTerminated;
end;





(*
//------------------------------------------------
procedure TdmTask_Classes_To_MDB.AddPropertyToDB(aXScenario: TxScenario;
    aXProperty: TxProperty);
//------------------------------------------------
var
  sName,sNumber: string;
  i: integer;
  sComment: string;

  oDataSet: TDataSet;

  iID: Integer;
begin
  //oDataSet :=t_Property;
  db_TableReOpen(ADOTable1, 'Property');
  oDataSet :=ADOTable1;


  if not oDataSet.Locate( FLD_Folder_ID +';'+ FLD_Name,
      VarArrayOf([aXScenario.Property_Folder_DB_ID,  aXProperty.Name]), [])
  then begin
    db_AddRecord(oDataSet,
         [
        //   db_Par(FLD_PROJECT_ID, Params.Project_ID),
           db_Par(FLD_Folder_ID, aXScenario.Property_Folder_DB_ID),


           db_Par(FLD_NAME,       aXProperty.Name),
           db_Par(FLD_ADDRESS,    aXProperty.Address),
           db_Par(FLD_BUILDING,   aXProperty.Building),

           db_Par(FLD_LAT,        aXProperty.BLPoint.B),
           db_Par(FLD_LON,        aXProperty.BLPoint.L),

           db_Par(FLD_GROUND_HEIGHT,   aXProperty.GROUND_HEIGHT)

        //   db_Par(FLD_COMMENT,      sComment)
          ]);


    aXProperty.DB_ID:=oDataSet.FieldByName(FLD_ID).AsInteger;

  end;

 // AddSiteCellsToDB(aXSite);

end;
*)


//
////------------------------------------------------
//procedure TdmTask_Classes_To_MDB.AddProjectToDB(aXProject: TxProject);
////------------------------------------------------
//var
//  oDataSet: TDataSet;
//begin
//  db_TableReOpen(ADOTable1, 'Project');
//  oDataSet :=ADOTable1;
//
//  if not oDataSet.Locate(FLD_Name, aXProject.Name, [])
//  then begin
//    db_AddRecord(oDataSet,
//         [
//           db_Par(FLD_NAME,  aXProject.Name)
//          ]);
//
//  //  aXProject.DB_ID:=ADOTable1[FLD_ID];
//  end;
//end;
//
//



// ---------------------------------------------------------------
procedure TdmTask_Classes_To_MDB.Execute;
// ---------------------------------------------------------------
var
  I: Integer;
//  oXProperty: TxProperty;
  //oXScenario: TxScenario;
  //oXSite: TxSite;
  j: integer;
  k: integer;
  iCount: integer;

  sFileName: string;
  m: integer;
begin
  FileCopy(Params.Template_MdbFileName, params.MdbFileName);

//  data_export_template.mdb
  dmMDB.OpenMDB(params.MdbFileName);
//////////  dmMDB1.EmptyTables;
(*

  db_TableReOpen(t_AntennaType, 'AntennaType');
  db_TableReOpen(t_Property, 'Property');
  db_TableReOpen(t_Link, 'Link');
  db_TableReOpen(t_Linkend, 'Linkend');
  db_TableReOpen(t_Linkend_Antenna, 'Linkend_Antenna');
  db_TableReOpen(t_Project, 'Project');
*)
 // db_TableReOpen(t_TrxType, 'TrxType');


//  TdmMDB.Init;

//  dmMDB.OpenMDB(params.MdbFileName);


//  OpenProjectData (Params.Project_ID);


//  AddProjectToDB(XProject_ref);
//
//  AddScenariosToDB;
//
//  AddReliefToDB();
//  AddAntennaTypeToDB();
//
// // if FTerminated then
//  //  Exit;
//
//
//
//
//  for I := 0 to XProject_ref.ScenarioList.Count - 1 do
//  begin
//    oXScenario := XProject_ref.ScenarioList[i];
//
//    ////// test !!!!!!!!
//   //////// for m := 0 to 50 do
//  //  begin
//
//      for j := 0 to oXScenario.PropertyList.Count - 1 do
//      begin
//        Progress_SetProgress2(j, oXScenario.PropertyList.Count, FTerminated);
//        if FTerminated then
//          Exit;
//
//        oXProperty :=oXScenario.PropertyList[j];
//
//        AddPropertyToDB(oXScenario , oXProperty);
//
//      //  for k := 0 to oXProperty.SiteList.Count - 1 do
//       //  AddPropertyToDB(oXProperty.SiteList[k], oXProperty);
//
//      end;
//
//
//      for j := 0 to oXScenario.LinkList.Count - 1 do
//      begin
//        Progress_SetProgress2(j, oXScenario.LinkList.Count, FTerminated);
//
//        Add_Link(oXScenario, oXScenario.LinkList[j]);
//      end;
//
//
//   // end;
//  end;

(*
  for I := 0 to XProject_ref.LinkList.Count - 1 do
  begin
    Progress_SetProgress2(i, XProject_ref.LinkList.Count, FTerminated);

    Add_Link(XProject_ref.LinkList[i]);
  end;*)

 // FADOConnection.CommitTrans;


  dmMDB.CloseMDB;

end;


function TdmTask_Classes_To_MDB.ExecuteDlg: Boolean;
begin
  Result := Progress_ExecDlg_proc(Execute);
end;



end.





