object dmExport_classes_link_to_MDB: TdmExport_classes_link_to_MDB
  OldCreateOrder = False
  Left = 1584
  Top = 497
  Height = 383
  Width = 456
  object ADOConnection_out: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=W:\RP' +
      'LS_DB EXPORT-IMPORT\EXCEL_to_DB_new\bin\Excel_to_db.mdb;Mode=Sha' +
      're Deny None;Extended Properties="";Persist Security Info=False;' +
      'Jet OLEDB:System database="";Jet OLEDB:Registry Path="";Jet OLED' +
      'B:Database Password="";Jet OLEDB:Engine Type=5;Jet OLEDB:Databas' +
      'e Locking Mode=1;Jet OLEDB:Global Partial Bulk Ops=2;Jet OLEDB:G' +
      'lobal Bulk Transactions=1;Jet OLEDB:New Database Password="";Jet' +
      ' OLEDB:Create System Database=False;Jet OLEDB:Encrypt Database=F' +
      'alse;Jet OLEDB:Don'#39't Copy Locale on Compact=False;Jet OLEDB:Comp' +
      'act Without Replica Repair=False;Jet OLEDB:SFP=False'
    ConnectionTimeout = 5
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 45
    Top = 11
  end
  object t_property: TADOTable
    Connection = ADOConnection_out
    TableName = 'property'
    Left = 48
    Top = 80
  end
  object t_linkEnd: TADOTable
    Connection = ADOConnection_out
    TableName = 't_linkEnd'
    Left = 128
    Top = 80
  end
  object t_link: TADOTable
    Connection = ADOConnection_out
    TableName = 't_link'
    Left = 208
    Top = 80
  end
  object t_linkEnd_antenna: TADOTable
    Connection = ADOConnection_out
    TableName = 'linkEnd_antenna'
    Left = 288
    Top = 80
  end
  object t_linkEndType: TADOTable
    Connection = ADOConnection_out
    TableName = 't_linkEndType'
    Left = 128
    Top = 152
  end
  object t_Folder: TADOTable
    Connection = ADOConnection_out
    TableName = 't_Folder'
    Left = 232
    Top = 152
  end
  object t_AntennaType: TADOTable
    Connection = ADOConnection_out
    TableName = 't_AntennaType'
    Left = 128
    Top = 232
  end
end
