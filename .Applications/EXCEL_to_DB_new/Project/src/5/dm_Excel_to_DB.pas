unit dm_Excel_to_DB;

interface

uses
  SysUtils, Classes, DB, ADODB,  Variants,  Forms, Dialogs,

  StrUtils, Math,        cxCustomData,

  dm_Excel_file,


  u_Config_File,

  u_func,

  u_files,

  u_import_classes,

  u_geo_convert_new,

 // dm_MDB,

  u_db_mdb,

  u_geo,

  u_db,

  //dm_Excel

    ComCtrls, cxGridCustomTableView, cxGridTableView, RxMemDS, dxmdaset;

type
  TdmExcel_to_Classes = class(TDataModule)
    RxMemoryData_Columns: TRxMemoryData;
    ds_RxMemoryData_Columns: TDataSource;
    procedure DataModuleDestroy(Sender: TObject);
  //  procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  //  procedure qry_File_columnsNewRecord(DataSet: TDataSet);
  private
    FDataSet_Columns: TDataSet;

    procedure aaaaaa;
    procedure SaveColumns;

//    FFirstLine: Integer; //1,2,..

//    procedure CreateMDB(aFileName: string);
//    procedure MakeColumnsArr;
   // procedure OpenFileQuery(aFileName: string);
//    procedure SaveToExportMDB_(aFileName: string; aCoordSystem: Integer);

  public
    PropertyList: TPropertyList;


    procedure ClearItems;
   // ConfigFile : TConfigFile;
//
//    Params: record
//        //      CoordSystem: Integer;
////              FirstHeaderLine : Integer;
//         //     WorksheetIndex : Integer;
//            end;


 //   procedure LoadColumns;

 //   procedure OpenExcelFile(aFileName: string; aStrings: TStrings);

    class procedure Init;

    procedure LoadDataFromExcel(aFileName: string; acxGridTableView:
        TcxGridTableView);

  //  procedure SaveDataToDB;

    procedure ExportToClasses;

  end;

var
  dmExcel_to_Classes: TdmExcel_to_Classes;


implementation


{$R *.dfm}



const
  FLD_first_row_index = 'first_row_index';


const
  FLD_field2_name   = 'field2_name';
  FLD_column_index  = 'column_index';
  FLD_FILE_ID       = 'FILE_ID';
  FLD_column_header = 'column_header';
  FLD_IS_USED       = 'IS_USED';

  TBL_file          = 'file';
  TBL_file_columns  = 'file_columns';
  TBL__Property     = '_Property';

const
  FLD_lat_lon = 'lat_lon';
  FLD_lon_lat = 'lon_lat';

  FLD_LAT_DEG = 'LAT_DEG';
  FLD_LAT_MIN = 'LAT_MIN';
  FLD_LAT_SEC = 'LAT_SEC';

  FLD_LON_DEG = 'LON_DEG';
  FLD_LON_MIN = 'LON_MIN';
  FLD_LON_SEC = 'LON_SEC';




    procedure geo_Encode2DegreeFromString(aValue: string; var aDegree1, aDegree2:
        Double); forward;

    function geo_EncodeDegreeFromString(aValue: string): Double;  forward;




class procedure TdmExcel_to_Classes.Init;
begin
  if not Assigned(dmExcel_to_Classes) then
    Application.CreateForm(TdmExcel_to_Classes, dmExcel_to_Classes);
end;


// ---------------------------------------------------------------
procedure TdmExcel_to_Classes.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  PropertyList:=TPropertyList.Create;


  FDataSet_Columns := RxMemoryData_Columns;
            
  db_CreateField(RxMemoryData_Columns,
    [
      db_Field(FLD_INDEX,  ftInteger),
      db_Field(FLD_HEADER, ftString),
      db_Field(FLD_FIELD_NAME,   ftString),
      db_Field(FLD_KEY,    ftBoolean)
    ]  );

  RxMemoryData_Columns.Open;

end;


procedure TdmExcel_to_Classes.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(PropertyList);
end;    


procedure TdmExcel_to_Classes.ClearItems;
begin
 // acxGridTableView.ClearItems;

end;



// ---------------------------------------------------------------
procedure TdmExcel_to_Classes.LoadDataFromExcel(aFileName: string;
    acxGridTableView: TcxGridTableView);

var
  rColumn: TColumn;
  sHeader: string;
begin
   Assert(g_ConfigFile.FirstRowIndex_1>0, 'Value <=0');

//  Assert(FFirstLine>0, 'Value <=0');

  dmExcel_file.Open(aFileName);
  dmExcel_file.LoadWorksheetByName(g_ConfigFile.sheetName);
  dmExcel_file.Close;


  FDataSet_Columns.Close;
  FDataSet_Columns.Open;


//  k:=acxGridTableView.DataController.RecordCount;

  acxGridTableView.ClearItems;



  while acxGridTableView.DataController.RecordCount>0 do
    acxGridTableView.DataController.DeleteRecord(0);


  //  k:=acxGridTableView.ColumnCount;

//  db_view();

 // if dmExcel_file.ColCount>0 then
 // begin
    for iCol := 1 to dmExcel_file.ColCount  do
    begin
      sHeader:=VarToStr(dmExcel_file.Cells_1[g_ConfigFile.FirstRowIndex_1, iCol]);


      oColumn:= acxGridTableView.CreateColumn;
      oColumn.Caption :=sHeader;

//     db_AddRecord_(RxMemoryData_Columns,
     db_AddRecord_(FDataSet_Columns,
          [FLD_INDEX,  iCol,
           FLD_HEADER, sHeader
          ]);

    if Length( g_ConfigFile.Columns) > iCol then
    begin
      rColumn:=g_ConfigFile.Columns[i];

 //     if rColumn.FieldName then
      db_UpdateRecord__(FDataSet_Columns,
          [FLD_FIELD_NAME, rColumn.FieldName,
           FLD_KEY,        rColumn.IsKey
          ]);

    end;





(*
      db_Field(FLD_FIELD_NAME,   ftString),
      db_Field(FLD_KEY,    ftBoolean)
*)

          //      oColumn.W :=s;
    end;


  acxGridTableView.DataController.BeginUpdate;

           // g_ConfigFile.FirstRowIndex

//    for iRow := Params.FirstHeaderLine+1 to dmExcel_file.RowCount  do
  for iRow := g_ConfigFile.FirstRowIndex_1 + 1 to dmExcel_file.RowCount  do
  begin
    k:=acxGridTableView.DataController.AppendRecord;

    for iCol := 1 to dmExcel_file.ColCount  do
    begin
      s:=VarToStr(dmExcel_file.Cells_1[iRow,iCol]);

    //////////////////
    acxGridTableView.DataController.Values[k, iCol-1]:=s;
    end;

    acxGridTableView.DataController.Post;
  end;


 // end;

  acxGridTableView.DataController.EndUpdate;


end;


(*

// ---------------------------------------------------------------
procedure TdmExcel_to_DB.MakeColumnsArr;
// ---------------------------------------------------------------
var
  I: Integer;
  FColumnArr: array of record
                FieldName : string;
                IsKey : Boolean;
              end;
begin

  SetLength(FColumnArr, FDataSet_Columns.RecordCount);

  with FDataSet_Columns do
    while not EOF do
    begin
      i:=RecNo;
   //   for I := 0 to RecordCount - 1 do
    //  begin
      FColumnArr[i].FieldName := FieldByName(FLD_FIELD_NAME).AsString;
      FColumnArr[i].IsKey     := FieldByName(FLD_KEY).AsBoolean;
    //  end;
      Next;
    end;    // while

end;

*)



procedure TdmExcel_to_Classes.aaaaaa;
begin
//  FDataSet_Columns.First;

{
  SetLength(arrColumns, FDataSet_Columns.RecordCount);

  with FDataSet_Columns do
    while not EOF do
    begin
      i:=RecNo;

      arrColumns[i-1].FieldName := FieldByName(FLD_FIELD_NAME).AsString;
      arrColumns[i-1].IsKey     := FieldByName(FLD_KEY).AsBoolean;

      Next;
    end;    // while

 }
end;


// ---------------------------------------------------------------
procedure TdmExcel_to_Classes.SaveColumns;
// ---------------------------------------------------------------
var
  i: Integer;
  rColumn: TColumn;
begin
  FDataSet_Columns.First;

  SetLength(g_ConfigFile.Columns, FDataSet_Columns.RecordCount);

  with FDataSet_Columns do
    while not EOF do
    begin
      i:=RecNo;

      rColumn.FieldName := FieldByName(FLD_FIELD_NAME).AsString;
      rColumn.IsKey := FieldByName(FLD_KEY).AsBoolean;

      g_ConfigFile.Columns[i-1] := rColumn;

//      rColumn.

//      g_ConfigFile.Columns[i-1].FieldName := FieldByName(FLD_FIELD_NAME).AsString;
//      g_ConfigFile.Columns[i-1].IsKey     := FieldByName(FLD_KEY).AsBoolean;


      Next;
    end;    // while

end;



// ---------------------------------------------------------------
procedure TdmExcel_to_Classes.ExportToClasses;
// ---------------------------------------------------------------
var
  coords: record
           SaveColumns, ang_lon: TAngle;
           Lat : Double;
           Lon : Double;
         end;
var
  I: Integer;

  oProperty: TProperty;

(*
  arrColumns: array of record
                FieldName : string;
                IsKey : Boolean;
              end;
*)


  iCol: Integer;
  iRow: Integer;
 // s: string;
  sField: string;
  v: Variant;

  rColumn: TColumn;
begin

//  PropertyList:=TPropertyList.Create;

  SaveColumns();



(*  FDataSet_Columns.First;

  SetLength(g_ConfigFile.Columns, FDataSet_Columns.RecordCount);

  with FDataSet_Columns do
    while not EOF do
    begin
      i:=RecNo;

//      rColumn.

      g_ConfigFile.Columns[i-1].FieldName := FieldByName(FLD_FIELD_NAME).AsString;
      g_ConfigFile.Columns[i-1].IsKey     := FieldByName(FLD_KEY).AsBoolean;

      Next;
    end;    // while
*)

  Assert(g_ConfigFile.FirstRowIndex_1>0, 'Value <=0');


  for iRow := g_ConfigFile.FirstRowIndex_1 + 1 to dmExcel_file.RowCount  do
  begin
    oProperty:=PropertyList.AddItem;
    FillChar(coords, SizeOf(coords), 0);


    for iCol := 0 to High(g_ConfigFile.Columns) do
    //  if arrColumns[iCol].IsKey or
     //    (arrColumns[iCol].FieldName<>'') then
    begin
      rColumn := g_ConfigFile.Columns[iCol];

      if rColumn.IsKey or (rColumn.FieldName<>'') then
        Continue;


      v:=dmExcel_file.Cells_1[iRow,iCol+1];


      if rColumn.IsKey then
        oProperty.Name := v
      else

     // if arrColumns[iCol].FieldName<>'' then
      begin
        sField:=rColumn.FieldName;

      //  v:=dmExcel_file.Cells_1[iRow,iCol+1];


        if Eq(sField, FLD_NAME)    then oProperty.Name := v  else
        if Eq(sField, FLD_ADDRESS) then oProperty.Address := v  else
        if Eq(sField, FLD_TOWER_HEIGHT) then oProperty.TOWER_HEIGHT := v  else

     // -------------------------
        if Eq(sField, FLD_LAT_DEG)  then  coords.SaveColumns.Deg:=AsInteger(v) else
        if Eq(sField, FLD_LAT_MIN)  then  coords.SaveColumns.Min:=AsInteger(v) else
        if Eq(sField, FLD_LAT_SEC)  then  coords.SaveColumns.Sec:=AsFloat(v)   else

        // -------------------------
        if Eq(sField, FLD_LON_DEG)  then  coords.ang_lon.Deg:=AsInteger(v) else
        if Eq(sField, FLD_LON_MIN)  then  coords.ang_lon.Min:=AsInteger(v) else
        if Eq(sField, FLD_LON_SEC)  then  coords.ang_lon.Sec:=AsFloat(v)   else

        // -------------------------
        if Eq(sField, FLD_LAT)  then coords.Lat:=geo_EncodeDegreeFromString (v) else
        if Eq(sField, FLD_LON)  then coords.Lon:=geo_EncodeDegreeFromString (v)
        else;

(*
            // -------------------------
        if Eq(aField_name, FLD_TOWER_HEIGHT) or
          (Eq(aField_name, FLD_GROUND_HEIGHT))
        then
*)
      end;
    end;


    if coords.SaveColumns.Deg>0 then
      coords.Lat := geo_EncodeDegreeRec(coords.SaveColumns);

    if coords.ang_lon.Deg>0 then
      coords.Lon := geo_EncodeDegreeRec(coords.ang_lon);


   Assert(coords.Lat>0, 'Value <=0');
   Assert(coords.Lon>0, 'Value <=0');

    oProperty.BLPoint.B := coords.Lat;
    oProperty.BLPoint.L := coords.Lon;

  end;

(*

  CreateMDB(aFileName);


  for i:=0 to PropertyList.Count-1 Do
  begin
    oProperty:=PropertyList[i];


    db_AddRecord_(ADOTable_out,
     [
       FLD_NAME,    oProperty.Name,
       FLD_ADDRESS, oProperty.Address,
       FLD_TOWER_HEIGHT, oProperty.TOWER_HEIGHT,

      // FLD_TOWER_HEIGHT,

       FLD_LAT,     oProperty.BLPoint.B,
       FLD_LON,     oProperty.BLPoint.L,

       FLD_LAT_STR, geo_FormatDegree(oProperty.BLPoint.B),
       FLD_LON_STR, geo_FormatDegree(oProperty.BLPoint.L)


      ] );
  end;

  ADOConnection_out.Close;
*)
end;



//--------------------------------------------------------------------
procedure geo_Encode2DegreeFromString(aValue: string; var
    aDegree1, aDegree2: Double);
//--------------------------------------------------------------------
// 54 44' 10.00''    52 42' 30.00''
//--------------------------------------------------------------------
  procedure DoAdd(var Value: string; aChar: Char);
  begin
    if (Value='') then
      if (aChar='0') or (not (aChar in ['0'..'9'])) then
         Exit;

    Value := Value + aChar;
  end;


var
  ang1: TAngle;
  ang2: TAngle;
  I: Integer;
  sValue, sStr: string;

  sDeg1: string;
  sMin1: string;
  sSec1: string;

  sDeg2: string;
  sMin2: string;
  sSec2: string;

  iSection : Integer;
 // s: string;
  bIsDigit : Boolean;

  ch: Char;
  e: Double;
  eDeg: Double;
  eDeg1: Integer;
  eDeg2: Integer;
  eMin: Double;
  eMin1: Double;
  eMin2: Double;
  eSec1: Double;
  eSec2: Double;

begin
  iSection:=0;

  for I := 1 to length(aValue) do
  begin
 //   s:=aValue[i];
    ch := aValue[i];

    if ch in [#$A] then
      iSection:=3
    else

    if ch in ['0'..'9','.',','] then
    begin
      bIsDigit:=True;

      case iSection of
        0: DoAdd(sDeg1, ch);
        1: DoAdd(sMin1, ch);
        2: DoAdd(sSec1, ch);

        3: DoAdd(sDeg2, ch);
        4: DoAdd(sMin2, ch);
        5: DoAdd(sSec2, ch);
      end;

    end else
    begin
      if bIsDigit then
      begin
        bIsDigit := False;
        Inc(iSection);

       // if  then

      end;
    end;
  end;


  eDeg1 := StrToIntDef(sDeg1, 0);
  eDeg2 := StrToIntDef(sDeg2, 0);

  eMin1 := AsFloat(sMin1);
  eMin2 := AsFloat(sMin2);

  eSec1 := AsFloat(sSec1);
  eSec2 := AsFloat(sSec2);

  e:=Frac(eMin1);
  e:=Frac(eMin2);

  // -------------------------
  if Frac(eMin1)>0 then
    aDegree1 := geo_EncodeDegree_Deg_Min(eDeg1, eMin1)
  else
    aDegree1 := geo_EncodeDegree(eDeg1, eMin1, eSec1);

  // -------------------------
  if Frac(eMin2)>0 then
    aDegree2 := geo_EncodeDegree_Deg_Min(eDeg2, eMin2)
  else
    aDegree2 := geo_EncodeDegree(eDeg2, eMin2, eSec2);

end;

//--------------------------------------------------------------------
function geo_EncodeDegreeFromString(aValue: string): Double;
//--------------------------------------------------------------------
var
  eDegree1 : double;
  eDegree2 : Double;
begin
  geo_Encode2DegreeFromString(aValue, eDegree1, eDegree2);

  Result := eDegree1;
end;


(*
var
  aDegree1, aDegree2: Double;
begin

  geo_Encode2DegreeFromString('53? 25,09? �.�.'#$A'50? 06,55? �.�.',
    aDegree1, aDegree2);
*)


end.




//'53? 25,09? �.�.'#$A'50? 06,55? �.�.'


     {

  // ---------------------------------------------------------------
  procedure DoAdd_antenna;
  // ---------------------------------------------------------------
  begin
    sSQL :=mdb_MakeCreateSQL ('linkend_antenna',
       [
        db_Field(FLD_ID,   ftAutoInc),
        db_Field(FLD_NAME, ftString, 200),

        db_Field(FLD_property_id, ftinteger),
        db_Field(FLD_linkend_id,  ftinteger),

        db_Field(FLD_HEIGHT, ftFloat),

        db_Field(FLD_DIAMETER, ftFloat),
        db_Field(FLD_GAIN,     ftFloat),

        db_Field(FLD_Polarization_str, ftString, 1),

        db_Field(FLD_COMMENT, ftString)

       ]);


    ADOConnection1_out.Execute(sSQL);
  end;
  // ---------------------------------------------------------------



begin
  mdb_CreateFile (aFileName);

  mdb_OpenConnection(ADOConnection1_out, aFileName);

  DoAdd_Folder();



  

// ---------------------------------------------------------------
procedure TdmExcel_to_DB.LoadColumns;
// ---------------------------------------------------------------
var
  c: Integer;
  iFileID: Integer;
  s: string;

  oADOQuery: TADOQuery;
begin
  oADOQuery:=qry_File_columns;

//  db_View(oADOQuery);

  iFileID:=qry_File [FLD_ID];

  s:= Format('UPDATE %s SET is_used=False and file_id=%d',
        [TBL_file_columns, iFileID]);
  oADOQuery.Connection.Execute(s);

  oADOQuery.Close;
  oADOQuery.Open;

//  db_TableReOpen(oADOQuery);


//  oADOQuery.Close;
//  oADOQuery.Open;

 // Assert(Assigned(dmExcel_file), 'Value not assigned');


  if dmExcel_file.RowCount>FFirstLine then
    for c := 1 to dmExcel_file.ColCount  do
    begin
      s:=VarToStr(dmExcel_file.Cells_1[FFirstLine,c]);

    //  if s<>'' then
   //   begin
//        if not oADOQuery.Locate(FLD_column_header +';'+ FLD_column_index,
  //                VarArrayOf([s,c]), []) then
        if not oADOQuery.Locate(FLD_column_index,
                  c, []) then
          db_AddRecord(oADOQuery,
             [db_Par(FLD_column_Index, c),
              db_Par(FLD_column_header, s),
              db_Par(FLD_FILE_ID, iFileID)
              ])
        else
          db_UpdateRecord(oADOQuery,
             [db_Par(FLD_IS_USED, True)]);
    //  end;
    end;

  s:= Format('DELETE FROM %s WHERE (is_used=False) and (file_id=%d)',
       [TBL_file_columns, iFileID]);
  oADOQuery.Connection.Execute(s);


  oADOQuery.Close;
  oADOQuery.Open;

 // db_TableReOpen(oADOQuery);


end;



// ---------------------------------------------------------------
procedure TdmExcel_to_DB.OpenFileQuery(aFileName: string);
// ---------------------------------------------------------------
var
  s: string;
begin
  s:= ExtractFileName( aFileName);


  db_OpenQuery(qry_File, 'SELECT * FROM File WHERE filename=:filename', [db_Par(FLD_FILENAME, s)]);

  if qry_File.RecordCount=0 then
    db_AddRecord(qry_File,  [db_Par(FLD_FILENAME, s)]);


  qry_File_columns.SQL.TEXT := 'SELECT * FROM File_columns WHERE file_id=:id order by column_index';
  qry_File_columns.Open;

end;


// ---------------------------------------------------------------
procedure TdmExcel_to_DB.SaveDataToDB;
// ---------------------------------------------------------------

var
  ang_lat, ang_lon: TAngle;
  eLat : Double;
  eLon : Double;


    // -------------------------
    procedure DoGetField(aADOTable: TADOTable; aField_name, aValue: string);
    // -------------------------
    begin
        // -------------------------
        if Eq(aField_name, FLD_LAT_DEG)  then  ang_lat.Deg:=AsInteger(aValue) else
        if Eq(aField_name, FLD_LAT_MIN)  then  ang_lat.Min:=AsInteger(aValue) else
        if Eq(aField_name, FLD_LAT_SEC)  then  ang_lat.Sec:=AsFloat(aValue) else

        // -------------------------
        if Eq(aField_name, FLD_LON_DEG)  then  ang_lon.Deg:=AsInteger(aValue) else
        if Eq(aField_name, FLD_LON_MIN)  then  ang_lon.Min:=AsInteger(aValue) else
        if Eq(aField_name, FLD_LON_SEC)  then  ang_lon.Sec:=AsFloat(aValue) else


        // -------------------------
        if Eq(aField_name, FLD_LAT)  then
          eLat:=geo_EncodeDegreeFromString (aValue) else

        if Eq(aField_name, FLD_LON)  then
          eLon:=geo_EncodeDegreeFromString (aValue) else

        // -------------------------
        if Eq(aField_name, FLD_lat_lon)  then
          geo_Encode2DegreeFromString (aValue, eLat, eLon) else

        // -------------------------
        if Eq(aField_name, FLD_lon_lat)  then
          geo_Encode2DegreeFromString (aValue, eLon, eLat) else

        // -------------------------
        if Eq(aField_name, FLD_TOWER_HEIGHT) or
          (Eq(aField_name, FLD_GROUND_HEIGHT))
        then
          aADOTable[aField_name] := AsFloat(aValue)

        else
          aADOTable[aField_name] :=aValue;

    end;    //

var
  I: Integer;
  c: Integer;
  d: Double;
  oADOTable: TADOTable;
  oADOQuery: TADOQuery;
  r: Integer;
  s: string;
  sColumn_header: string;

  sField_name: string;

  iColumn_index: Integer;
  sField2_name: string;

  FColumns: TStringList;


begin

  oADOQuery:=qry_File_columns;

  oADOQuery.DisableControls;
  oADOQuery.First;

  FColumns:=TStringList.Create;


  with oADOQuery do
    while not EOF do
  begin
    sField_name   := FieldByName(FLD_field_name).AsString;
  //  sField2_name   := FieldByName(FLD_field2_name).AsString;
    sColumn_header:= FieldByName(FLD_column_header).AsString;
    iColumn_index:= FieldByName(FLD_column_index).AsInteger;


    FColumns.Values[IntToStr(iColumn_index)] := sField_name;
  //  FColumns.Values[IntToStr(iColumn_index)+'_2'] := sField2_name;

    Next;
  end;

  oADOQuery.EnableControls;;


   // ---------------------------------------------------------------
  oADOTable:=t_Property ;
  oADOTable.DisableControls;

  s:= Format('DELETE FROM %s', [TBL__Property]);
  oADOTable.Connection.Execute(s);

  db_TableReOpen(oADOTable);

  for r := FFirstLine+1 to dmExcel_file.RowCount  do
  begin
    eLat:=0;
    eLon:=0;

    FillChar(ang_lat, SizeOf(ang_lat), 0);
    FillChar(ang_lon, SizeOf(ang_lon), 0);

    oADOTable.Append;

    oADOTable[FLD_ID]:= r-FFirstLine;

    for c := 1 to dmExcel_file.ColCount  do
    begin
      s:=VarToStr(dmExcel_file.Cells_1[r,c]);

      sColumn_header:= VarToStr(dmExcel_file.Cells_1[FFirstLine,c]);

      sField_name :=FColumns.Values[IntToStr(c)];
    //  sField2_name:=FColumns.Values[IntToStr(c)+'_2'];

      s:=VarToStr(dmExcel_file.Cells_1[r,c]);

      if sField_name<>'' then
        DoGetField (oADOTable, sField_name, s);

   //   if sField2_name<>'' then
     //   DoGetField (oADOTable, sField2_name, s);
    end;


    if ang_lat.Deg>0 then
      eLat := geo_EncodeDegreeRec(ang_lat);

    if ang_lon.Deg>0 then
      eLon := geo_EncodeDegreeRec(ang_lon);


    oADOTable[FLD_LAT] := eLat;
    oADOTable[FLD_LON] := eLon;

    oADOTable[FLD_LAT_STR] := geo_FormatDegree(eLat);
    oADOTable[FLD_LON_STR] := geo_FormatDegree(eLon);

    if oADOTable.FieldByName(FLD_NAME).AsString='' then
      oADOTable[FLD_NAME] :=  r - FFirstLine;


(*    if oADOTable.FieldByName(FLD_NAME).AsString='' then
      oADOTable.Cancel
    else
*)

      oADOTable.Post;

  end;

  db_TableReOpen(oADOTable);

  oADOTable.EnableControls;;


  FreeAndNil(FColumns);

end;




// ---------------------------------------------------------------
procedure TdmExcel_to_DB.SaveToExportMDB_(aFileName: string; aCoordSystem:
    Integer);
// ---------------------------------------------------------------
const
  ARR_FIELDS:  array[0..3] of string = (
    FLD_NAME,
    FLD_ADDRESS,
    FLD_LAT,
    FLD_LON
  ) ;


var
  blCK42: TBLPoint;
  I: Integer;
  c: Integer;
  d: Double;
  oADOTable_src: TADOTable;
  oADOTable_dest: TADOTable;
  r: Integer;
  s: string;
  sTemplate_FileName: string;

  blWgs: TBlPoint;

begin
  if aFileName='' then
    raise Exception.Create('');


  sTemplate_FileName := ChangeFileExt(Application.ExeName, '.template.mdb');

  FileCopy(sTemplate_FileName, aFileName);


  if not mdb_OpenConnection (ADOConnection_template_MDB, aFileName) then
    Exit;


//  FColumns:=TStringList.Create;


  oADOTable_src:=t_Property ;
  oADOTable_dest:=t_Property_export ;

  oADOTable_src.DisableControls;

  db_TableReOpen(oADOTable_src);
  db_TableReOpen(oADOTable_dest, 'Property');

  with oADOTable_src do
    while not EOF do
  begin
    oADOTable_dest.Append;

    for I := 0 to High(ARR_FIELDS) do
       oADOTable_dest[ARR_FIELDS[i]] :=FieldValues[ARR_FIELDS[i]];

    if aCoordSystem = EK_WGS84 then
    begin
      blWgs := db_ExtractBLPoint(oADOTable_src);
      blCK42:= geo_WGS84_to_Pulkovo42(blWgs);

      oADOTable_dest[FLD_LAT] := blCK42.B;
      oADOTable_dest[FLD_LON] := blCK42.L;

    end;


    oADOTable_dest.Post;

    Next;
  end;


  oADOTable_src.EnableControls;;


//  oADOTable_dest.Close;
  ADOConnection_template_MDB.Close;

end;




{
// ---------------------------------------------------------------
procedure TdmExcel_to_DB.CreateMDB(aFileName: string);
// ---------------------------------------------------------------

    procedure DoAdd();
    var
      sSQL: string;
    begin
      sSQL :=mdb_MakeCreateSQL ('property',
         [
          db_Field(FLD_ID,      ftAutoInc),
          db_Field(FLD_NAME,    ftString, 200),

          db_Field(FLD_ADDRESS, ftString, 200),

          db_Field(FLD_TOWER_HEIGHT, ftFloat),

          db_Field(FLD_LAT,     ftFloat),
          db_Field(FLD_LON,     ftFloat),

          db_Field(FLD_LAT_STR, ftString),
          db_Field(FLD_LON_STR, ftString)

         ]);
             
      ADOConnection_out.Execute(sSQL);
    end;


 // end;
  // ---------------------------------------------------------------


begin
  mdb_CreateFile (aFileName);

  mdb_OpenConnection(ADOConnection_out, aFileName);
  DoAdd();

  db_TableOpen1(ADOTable_out, 'property');


end;
 }






       

(*
// ---------------------------------------------------------------
procedure TdmExcel_to_DB.OpenExcelFile(aFileName: string; aStrings: TStrings);
// ---------------------------------------------------------------
begin
 // TdmExcel_file.Init;

  dmExcel_file.Open(aFileName);

  dmExcel_file.LoadWorksheetNames(aStrings);
  dmExcel_file.Close;

 // OpenFileQuery (aFileName);
end;
*)

