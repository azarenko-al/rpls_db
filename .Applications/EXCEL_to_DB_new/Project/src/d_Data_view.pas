unit d_Data_view;

//  cxGrid_topTableView_Data.LayoutChanged();

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid, Grids, DBGrids, StdCtrls, ToolWin, ComCtrls, StdActns, ActnList,
  rxPlacemnt, Mask, rxToolEdit, cxInplaceContainer, cxTL,

  cxDropDownEdit,

  u_ini_Excel_to_DB_params,

  u_ini_data_export_import_params,
  u_vars,


  dm_Export_classes_to_MDB,

  dm_Export_classes_link_to_MDB,

 // dm_Excel_Export_to_MDB,
  dm_Config_mdb,

  dm_Excel_to_classes,

  u_const_db,


  u_files,

 // dm_Excel_to_DB,

  dm_Excel_file,
  u_import_classes,

 // u_import_classes,

  u_cx_vgrid,

  u_func,
  u_common_const,

  u_run,

  u_geo,

  u_db,

  u_Config_File,


  cxTLData, cxDBTL, cxStyles, cxSplitter, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, cxTextEdit, cxDBLookupComboBox, cxCheckBox,
  cxMaskEdit, cxTLdxBarBuiltInMenu, System.Actions, RxMemDS, dxmdaset

  ;

type
  Tdlg_Data_view = class(TForm)
    cxGrid_top: TcxGrid;
    cxGrid_topDBTableView1: TcxGridDBTableView;
    cxGrid_topTableView_Data: TcxGridTableView;
    cxGrid_topLevel1: TcxGridLevel;
    pn_Fields: TPanel;
    cxGrid_bottom: TcxGrid;
    cxGridDBTableView_Columns: TcxGridDBTableView;
    col_index: TcxGridDBColumn;
    col_header: TcxGridDBColumn;
    col_name: TcxGridDBColumn;
    cxGridLevel2: TcxGridLevel;
    ActionList1: TActionList;
    act_SaveToExportMDB: TAction;
    ToolBar1: TToolBar;
    FormStorage1: TFormStorage;
    col_is_not_null: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    col_COLUMN_INDEX: TcxGridDBColumn;
    cxStyle_used: TcxStyle;
    Button5: TButton;
    cxStyle_header: TcxStyle;
    act_Exec: TAction;
    ed_Filename1: TFilenameEdit;
    Button2: TButton;
    Button1: TButton;
    Button3: TButton;
    cxGrid_topTableView_DataColumn1: TcxGridColumn;
    cxGrid_topTableView_DataColumn2: TcxGridColumn;
    cxSplitter1: TcxSplitter;
    Panel2: TPanel;
    Bevel1: TBevel;
    Panel3: TPanel;
    Button7: TButton;
    cxSplitter2: TcxSplitter;
    cxStyle_Column_selected: TcxStyle;
    ds_Data: TDataSource;
    dxMemData_Data: TdxMemData;
    dxMemData_Dataid: TAutoIncField;
    dxMemData_DataFIELD_caption: TStringField;
    dxMemData_DataFIELD_NAME: TStringField;
    dxMemData_DataIS_NOT_NULL: TBooleanField;
    dxMemData_DataCOLUMN_INDEX: TIntegerField;
    dxMemData_Columns: TdxMemData;
    IntegerField1: TIntegerField;
    ds_Columns: TDataSource;
    dxMemData_Columnscaption: TStringField;
    cxStyle_not_null: TcxStyle;
    cxStyleRepository2: TcxStyleRepository;
    cxStyle_Read_only: TcxStyle;
    dxMemData1: TdxMemData;
    AutoIncField1: TAutoIncField;
    StringField1: TStringField;
    StringField2: TStringField;
    BooleanField1: TBooleanField;
    IntegerField2: TIntegerField;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    Button4: TButton;
    Timer1: TTimer;    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure act_LoadDataFromExcelExecute(Sender: TObject);
    procedure Button4Click(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
//    procedure Button4Click(Sender: TObject);
//    procedure Button5Click(Sender: TObject);
    procedure col_namePropertiesCloseUp(Sender: TObject);

//    procedure DoOnColumnGetContentStyle(Sender: TcxCustomGridTableView; ARecord:
//        TcxCustomGridRecord; AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
        
    procedure cxGrid2TableView_DataColumn1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure cxGridDBTableView_ColumnsStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure col_nameStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure dxMemData_DataBeforePost(DataSet: TDataSet);
    procedure col_namePropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure Timer1Timer(Sender: TObject);

//    procedure cxGrid2TableView_DataStylesGetContentStyle(
 //     Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  //    AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
  //  procedure Button3Click(Sender: TObject);
  //
  //  procedure col_namePropertiesEditValueChanged(Sender: TObject);
 //   procedure ToolButton1Click(Sender: TObject);
  private
    FUsedColumnIndexList: TStringList;

    FFileName_MDB : string;


        
    procedure LoadDataFromExcel;
    procedure SaveToExportMDB;

  public
    function IsAllowRun: Boolean;
    class function ExecDlg: boolean;

    procedure RunConverter;


    { Public declarations }
  end;

var
  dlg_Data_view: Tdlg_Data_view;

implementation

{$R *.dfm}


// ---------------------------------------------------------------
class function Tdlg_Data_view.ExecDlg: boolean;
// ---------------------------------------------------------------
begin
  with Tdlg_Data_view.Create(Application) do
  begin

    ShowModal;

    Free;
  end;


end;

// ---------------------------------------------------------------
procedure Tdlg_Data_view.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  Caption :='������';

  cxGrid_top.Align:=alTop;
  pn_Fields.Align:=alClient;

  FUsedColumnIndexList:=TStringList.Create;



  col_Index.Caption  :='�';
  col_Header.Caption :='�������� � ���� ������';
  col_Name.Caption   :='������� �� ���������';
//  col_Key.Caption    :='�������� ����';


//  col_Key.Visible := False;



  FFileName_MDB:=GetTempFileNameWithExt('mdb');

//  FFileName_MDB:= ChangeFileExt(g_ConfigFile.FileName, '.temp.mdb');
  FFileName_MDB:= ChangeFileExt(g_ConfigFile.FileName, '.mdb');


  {.$DEFINE test}


  {$IFDEF test}
 // ToolBar1.Visible:=True;


 // FFileName_MDB := ed_FileName1.FileName;

  {$ENDIF}

  db_CreateFieldArr(dxMemData_Data,
    [
      db_Field(FLD_INDEX,  ftInteger),
   //   db_Field(FLD_HEADER, ftString),
      db_Field(FLD_COLUMN_INDEX, ftInteger),
      db_Field(FLD_FIELD_caption, ftString),
      db_Field(FLD_FIELD_NAME, ftString),
      db_Field(FLD_IS_NOT_NULL,  ftBoolean)
//      db_Field(FLD_KEY,    ftBoolean)
    ]  );

  dxMemData_Data.Open;  


  LoadDataFromExcel();
  

end;



procedure Tdlg_Data_view.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FUsedColumnIndexList);
end;


// ---------------------------------------------------------------
procedure Tdlg_Data_view.SaveToExportMDB;
// ---------------------------------------------------------------
var
  r: TColumn;
  sIniIniFileName: string;

//  sFileName: string;
begin
 // sFileName := ed_FileName.FileName;
//    's:\1111.mdb';

  g_ConfigFile.Columns.Clear;
  dxMemData_Data.First;

  with dxMemData_Data do 
    while not EOF do
    begin
      if not VarIsNull(FieldValues[FLD_COLUMN_INDEX]) then
      begin
        r.FieldName  :=FieldByName(FLD_Field_Name).AsString;
        r.ColumnIndex:=FieldByName(FLD_COLUMN_INDEX).AsInteger;

        g_ConfigFile.Columns.Add(r);
      end;  
      
    
      Next;
    end;
  
  sIniIniFileName:=g_ConfigFile.GetIniFileName(g_ConfigFile.FileName);
  g_ConfigFile.SaveToIniFile(sIniIniFileName);
  


//    dmExcel_to_DB.Params.CoordSystem := iCoordSys;
//    dmExcel_to_DB.Params.FirstHeaderLine := 1;

  case g_ConfigFile.DataSourceType of
    dtProp: begin
              dmExcel_to_Classes.ExportToClasses_Property();

              TdmExport_Classes_to_MDB.Init;
              dmExport_Classes_to_MDB.SaveToExportMDB(FFileName_MDB,  dmExcel_to_Classes.Project);

            end;

    dtLink: begin
              dmExcel_to_Classes.ExportToClasses_LInk;

              TdmExport_classes_link_to_MDB.Init;
              dmExport_classes_link_to_MDB.SaveToExportMDB(FFileName_MDB, dmExcel_to_Classes.Project);
            end;
    //dmExport_classes_link_to_MDB. ExportToClasses _Property();

//         procedure SaveToExportMDB(aFileName: string; aProject: TExcelProject);

  end;





end;


// ---------------------------------------------------------------
procedure Tdlg_Data_view.act_LoadDataFromExcelExecute(Sender: TObject);
// ---------------------------------------------------------------
var
  s: string;
begin
  if Sender=act_Exec then
  begin
    SaveToExportMDB();

//    Exit;

    
///////////////////////
    runConverter();


    s:=Format ('exec dbo.sp_Project_restore_after_import %d', [g_ConfigFile.Runtime.Project_ID]);
    g_ConfigFile.Runtime.ADOConnection.Execute(s);


  end;

end;

procedure Tdlg_Data_view.Button4Click(Sender: TObject);
begin
IsAllowRun
end;

function Tdlg_Data_view.IsAllowRun: Boolean;
var
  iRecNo: Integer;
begin
  dxMemData_Data.DisableControls;
  iRecNo:=dxMemData_Data.RecNo;

  dxMemData1.Close;

  dxMemData1.LoadFromDataSet(dxMemData_Data);

  dxMemData1.Filter:='is_not_null = ''true''';  //.. and column_index is null
  dxMemData1.Filtered:=True;
  
  while dxMemData1.Locate ('is_not_null', False,[]) do 
    dxMemData1.Delete;


  Result:= not dxMemData1.Locate ('column_index', Null,[]);   
  
  act_Exec.Enabled:=Result;

  dxMemData_Data.First;
  dxMemData_Data.RecNo:=iRecNo;
  dxMemData_Data.EnableControls;

end;


// ---------------------------------------------------------------
procedure Tdlg_Data_view.col_namePropertiesCloseUp(Sender: TObject);
// ---------------------------------------------------------------
var
  iIndex: Integer;
  k: Integer;
begin

  iIndex:=cxGridDBTableView_Columns.DataController.FocusedRecordIndex;


  if not VarIsNull (cxGridDBTableView_Columns.DataController.Values[iIndex, col_COLUMN_INDEX.Index] ) then 
  begin
    k:=cxGridDBTableView_Columns.DataController.Values[iIndex, col_COLUMN_INDEX.Index];

    if FUsedColumnIndexList.IndexOfObject(Pointer(k))<0 then
      FUsedColumnIndexList.AddObject( IntToStr(k), Pointer(k) );
  
    cxGrid_topTableView_Data.LayoutChanged();

  end;
  
  IsAllowRun();
end;



procedure Tdlg_Data_view.col_namePropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
   Timer1.Enabled:=True;

end;

procedure Tdlg_Data_view.col_nameStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
  if not ARecord.Selected then 
    if ARecord.Values[col_is_not_null.Index] = True  then
      if VarIsNull(ARecord.Values[col_COLUMN_INDEX.Index]) then
        AStyle:=cxStyle_not_null
  
  
end;

// ---------------------------------------------------------------
procedure Tdlg_Data_view.LoadDataFromExcel;
// ---------------------------------------------------------------
var
  I: Integer;
  k: Integer;
  oColumn: TcxGridDBColumn;
begin
//  g_ConfigFile.DataSourceType:=

  dmConfig_mdb.OpenFieldsDataset(g_ConfigFile.DataSourceType);

  Assert(dmConfig_mdb.t_Fields.RecordCount>0);
  
  with dmConfig_mdb.t_Fields do 
    while not EOF do
    begin
      db_AddRecord_(   dxMemData_Data, 
      [
         FLD_FIELD_NAME,     FieldValues[FLD_NAME],
         FLD_FIELD_caption,  FieldValues[FLD_CAPTION],
         FLD_IS_NOT_NULL,    FieldValues[FLD_IS_NOT_NULL]
      ]);
    
      Next;
    end;  
  
//  
  dmExcel_to_Classes.LoadDataFromExcelFile(
      g_ConfigFile.FileName,          
      cxGrid_topTableView_Data );
                      

  for I := 0 to dmExcel_to_Classes.HeaderList.Count-1 do
      db_AddRecord_  (dxMemData_Columns, 
      [ 
        'index', i, 
        'caption', Format ('%2d ',[i])+  dmExcel_to_Classes.HeaderList[i]
      ]);
  

  for I := 0 to g_ConfigFile.Columns.Count-1 do
    if dxMemData_Data.Locate(FLD_FIELD_NAME,  g_ConfigFile.Columns[i].FieldName, []) then    
    begin
      k:=g_ConfigFile.Columns[i].ColumnIndex;
      FUsedColumnIndexList.AddObject( IntToStr(k), Pointer(k) );
    
      db_UpdateRecord__  (dxMemData_Data, 
      [ 
        FLD_FIELD_NAME,    g_ConfigFile.Columns[i].FieldName, 
        FLD_COLUMN_INDEX,  g_ConfigFile.Columns[i].ColumnIndex
      ]);
    end;
    

  //dxMemData_Columns

  for I := 0 to cxGrid_topTableView_Data.ColumnCount - 1 do
    cxGrid_topTableView_Data.Columns[i].Styles.OnGetContentStyle:=cxGrid2TableView_DataColumn1StylesGetContentStyle;
                    
  IsAllowRun();
//  cxGrid2TableView_Data.ApplyBestFit();

end;


procedure Tdlg_Data_view.cxGrid2TableView_DataColumn1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin      
  if FUsedColumnIndexList.IndexOfObject(Pointer(AItem.Index)) >=0 then
    AStyle := cxStyle_used;

end;


procedure Tdlg_Data_view.cxGridDBTableView_ColumnsStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
//  if ARecord.Values[col_is_Not_null.Index] = true then 
//    AStyle:=cxStyle_not_null;

end;


//-------------------------------------------------------------------
procedure Tdlg_Data_view.RunConverter;
//-------------------------------------------------------------------
const
  TEMP_FILENAME = 'project_export.ini';

 // PROG_data_export_import = 'data_export_import.exe';

var
  iProjectID: Integer;
  sFile : string;

  oParams: TIni_Data_export_import_params;
//  sMdbFileName: string;

begin
//  assert(g_Excel_to_DB_params.ProjectID1 > 0);

  assert(g_ConfigFile.Runtime.Project_ID > 0);
                           
  ////////////////////////////////


  sFile := g_ApplicationDataDir + TEMP_FILENAME;


//  sMdbFileName:= GetTempFileNameWithExt('mdb');
 // sMdbFileName:= ed_Filename.FileName;

 // iProjectID := AsInteger(DBLookupComboBox_Projects.KeyValue);


//  sFile := g_ApplicationDataDir + TEMP_FILENAME;

  oParams:=TIni_Data_export_import_params.Create;
 // oParams.ProjectID := iProjectID;


//  if g_Excel_to_DB_params.ProjectID>0 then
  oParams.ProjectID:=g_ConfigFile.Runtime.Project_ID;
//--  oParams.ProjectID:=g_Excel_to_DB_params.ProjectID1;


   assert(oParams.ProjectID>0);


  oParams.IsAuto := True;

  oParams.Mode     := mtImport_project;
 // oParams.IsAuto   := True;

  oParams.FileName_MDB := FFileName_MDB;
  oParams.ConnectionString := g_Excel_to_DB_params.ConnectionString;


//      FMdbFileName

  oParams.SaveToIni(sFile);

  FreeAndNil(oParams);

  // -------------------------

  RunApp (GetApplicationDir()+ PROG_data_export_import_exe, DoubleQuotedStr(sFile));
  
//   ShellExec (GetApplicationDir()+ PROG_data_export_import_exe, DoubleQuotedStr(sFile));

end;



procedure Tdlg_Data_view.dxMemData_DataBeforePost(DataSet: TDataSet);
begin
//  DataSet[FLD_]

end;

procedure Tdlg_Data_view.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=False;
  IsAllowRun();
end;


end.




//  cxGrid_topTableView_Data.LayoutChanged();
