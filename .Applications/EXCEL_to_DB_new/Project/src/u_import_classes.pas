unit u_import_classes;

interface

uses Classes,SysUtils,Variants,Forms,  typinfo,

  dm_Excel_file,
   
  System.Generics.Collections, 
  
//  u_crc32,

  u_func,
  u_Geo;

//  u_compare_classes;

type

//class(TList<TOptionsStyleItem>)

 // -------------------------------------------------------------------
  TPropertyRaw = class //(TCollectionItem)
  // -------------------------------------------------------------------
  public
    Runtime: record
               DB_ID : Integer;
             end;


    Name: string;

    Number : Integer;
    Status : string;
    Address : string;
    Building: string;
    TOWER_HEIGHT : double;

    GROUND_HEIGHT: Double;

    BLPoint: TBLPoint;

    // -------------------------
    H1_m,H2_m,H3_m : Integer;
    CellID_1,CellID_2,CellID_3 : Integer;


  end;

 // -------------------------------------------------------------------
  TLinkEndType_Raw = class//(TCollectionItem)
  // -------------------------------------------------------------------
  public
    DB_ID : Integer;

    Name: string;

    Equipment: string;
    Band: Integer;
  end;


  // -------------------------------------------------------------------
  TLinkRaw = class
  // -------------------------------------------------------------------
  public
    Name: string;

    Runtime: record

               LinkEnd: array[1..2] of record
                           DB_ID : Integer;
                         end;

             end;


    Property1_ref: TPropertyRaw;
    Property2_ref: TPropertyRaw;


    Sites: array[1..2] of record

      Name:         string;
      TOWER_HEIGHT: double;
      Address:      string;

      BLPoint: TBLPoint;

      Diameter: double;     
                            
      LinkEnd_name:  string;   

      Runtime: record
               //  LinkEnd_DB_ID : Integer;
               end;
    end;

    tx_mhz : double;
    rx_mhz : double;
    Power  : double;

    LinkEndType_DB_ID : Integer;
//    AntennaType_DB_ID : Integer;

//    Input_Power : double;

    channel_width_MHz: double;

    Modulation: string;
    //E1_number: integer;

    LinkendType_name : string;

    band   : Integer;

    Capacity  : double;


   {
    LinkEnd1,LinkEnd2: record
      Power_dBm: double;
      Freq_MHz: double;

//      Threshold_BER_3: double;
      Threshold_BER_3: double;
      Bitrate_Mbps: double;
    end;
    }

    Transmission_Type : string;
    Transmission_Mode : Integer;

  end;


  // ---------------------------------------------------------------
  TRawPropertyList = class(TList<TPropertyRaw>)  
  // ---------------------------------------------------------------
  private
    function FindIndexByName(aName: string): integer;
 //   function GetItems(Index: Integer): TPropertyRaw;
  public
//    constructor Create;
    function AddItem: TPropertyRaw;

    function FindByName(aName: string): TPropertyRaw;

//    property Items[Index: Integer]: TPropertyRaw read GetItems; default;
  end;

  // ---------------------------------------------------------------
  TRawLinkEndTypeList =  class(TList<TLinkEndType_Raw>)  
  // ---------------------------------------------------------------
  private
    function FindIndexByName(aName: string): integer;
//    function FindIndexByName(aName: string): integer;
//    function GetItems(Index: Integer): TLinkEndType_Raw;
  public
//    constructor Create;
    function AddItem: TLinkEndType_Raw;

    function FindByEquipmentBand(aEquipment: string; aBand: Integer):  TLinkEndType_Raw;
    function FindByName(aName: string): TLinkEndType_Raw;

  //  property Items[Index: Integer]: TLinkEndType_Raw read GetItems; default;
  end;


  // ---------------------------------------------------------------
  TRawLinkList = class(TList<TLinkRaw>)
  // ---------------------------------------------------------------
  public
    function AddItem: TLinkRaw;

  end;


  TExcelProject = class(TObject)
  public
     PropertyList: TRawPropertyList;
     LinkList: TRawLinkList;

     LinkEndTypeList: TRawLinkEndTypeList;

     constructor Create;
     destructor Destroy; override;
  end;


implementation



function TRawPropertyList.AddItem: TPropertyRaw;
begin
  Result := TPropertyRaw.Create;
  Add(Result);
//  (inherited Add);
end;

function TRawPropertyList.FindIndexByName(aName: string): integer;
var I: Integer;
begin
  Result := -1;

  for I := 0 to Count - 1 do
    if Eq(Items[i].Name, aName) then
    begin
      Result := i;
      Break;
    end;
end;

function TRawPropertyList.FindByName(aName: string): TPropertyRaw;
var I: Integer;
begin
  i:=FindIndexByName(aName);
  if i>=0 then
    Result := Items[i]
  else
    Result := nil;
end;


constructor TExcelProject.Create;
begin
  inherited Create;
  PropertyList := TRawPropertyList.Create();
  LinkList := TRawLinkList.Create();

  LinkEndTypeList:=TRawLinkEndTypeList.Create();

end;

destructor TExcelProject.Destroy;
begin
  FreeAndNil(LinkList);
  FreeAndNil(PropertyList);
  FreeAndNil(LinkEndTypeList);

  inherited Destroy;
end;



function TRawLinkList.AddItem: TLinkRaw;
begin
  Result := TLinkRaw.Create();
  Add(Result);
end;

//function TRawLinkList.GetItems(Index: Integer): TLinkRaw;
//begin
//  Result := TLinkRaw(inherited Items[Index]);
//end;
//
//constructor TRawLinkEndTypeList.Create;
//begin
//  inherited Create (TLinkEndType_Raw);
//
//end;

function TRawLinkEndTypeList.AddItem: TLinkEndType_Raw;
begin
  Result := TLinkEndType_Raw.Create;
  Add (Result);
//  (inherited Add);

end;


function TRawLinkEndTypeList.FindByName(aName: string): TLinkEndType_Raw;
var I: Integer;
begin
  i:=FindIndexByName(aName);
  if i>=0 then
    Result := Items[i]
  else
    Result := nil;
end;

// ---------------------------------------------------------------
function TRawLinkEndTypeList.FindByEquipmentBand(aEquipment: string; aBand:
    Integer): TLinkEndType_Raw;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Result:=nil;

  for I :=  0 to Count - 1 do
    if Eq(Items[i].Equipment, aEquipment) and (Items[i].Band=aBand)  then
    begin
      Result := Items[i];
      Break;
    end;

end;


//    Equipment: string;
//


//
//function TRawLinkEndTypeList.GetItems(Index: Integer): TLinkEndType_Raw;
//begin
//  Result := TLinkEndType_Raw(inherited Items[Index]);
//
////  Result := ;
//end;


function TRawLinkEndTypeList.FindIndexByName(aName: string): integer;
var I: Integer;
begin
  Result := -1;

  for I := 0 to Count - 1 do
    if Eq(Items[i].Name, aName) then
    begin
      Result := i;
      Break;
    end;
end;



(*

var
  oList: TLinkEndType_mode_List;

*)
//  o: TRttiContext;

begin

(*
  Application.Initialize;

  oList:=TLinkEndType_mode_List.Create;
  oList.LoadFromExcel('W:\Tasks new\Tasks new\Link import excel\Data\1.xls');

*)
end.


