unit dm_MDB;

interface

uses
  Forms, Classes, ADODB, 

  u_db,
  u_db_mdb, DB;


type
  TdmMDB1 = class(TDataModule)
    ADOConnection_MDB1: TADOConnection;
    view_Fields_for_property: TADOTable;
    ds_view_Fields_for_property: TDataSource;
    procedure DataModuleCreate(Sender: TObject);
    procedure EmptyTables;
  private
    procedure EmptyTable(aTableName: string);

  public
    class procedure Init;

    function OpenMDB(aMDBFileName: string): Boolean;
    procedure CloseMDB;
  end;

var
  dmMDB1: TdmMDB1;


implementation

uses dm_MDB;
{$R *.dfm}


class procedure TdmMDB1.Init;
begin
  if not Assigned(dmMDB) then
    Application.CreateForm(TdmMDB, dmMDB);
end;


// ---------------------------------------------------------------
procedure TdmMDB1.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
var
  sFileName: string;
begin
(*
  if ADOConnection_MDB.Connected then
  begin
    g_Log.Error('TdmMDB.ADOConnection.Connected');
    ShowMessage('TdmMDB.ADOConnection.Connected');
  end;

  sFileName := ChangeFileExt(Application.ExeName, '.mdb');

  if not mdb_OpenConnection (ADOConnection_MDB, sFileName) then
    Halt;
*)
 // db_SetComponentADOConnection(Self, ADOConnection_MDB);

end;

procedure TdmMDB1.EmptyTable(aTableName: string);
begin
  db_DeleteRecords(ADOConnection_MDB1, aTableName);
end;

procedure TdmMDB1.EmptyTables;
begin

//  EmptyTable('AntennaType');
//
//  EmptyTable('Project');
//
//  EmptyTable('Property');
//
//  EmptyTable('Linkend');
//  EmptyTable('Linkend_Antenna');
//  EmptyTable('Link');

//  EmptyTable('TrxType');

end;


procedure TdmMDB1.CloseMDB;
begin
  ADOConnection_MDB1.Close;
end;


//------------------------------------------------------------------------------
function TdmMDB1.OpenMDB(aMDBFileName: string): Boolean;
//------------------------------------------------------------------------------
begin
  Result := mdb_OpenConnection(ADOConnection_MDB1, aMDBFileName);


//  if mdb_OpenConnection(ADOConnection_MDB, aMDBFileName) then
 //   ADOConnection.GetTableNames(FTableNameList);


 (* if TableExists(TBL_PROJECT) then
    db_TableOpen1(t_Project, TBL_PROJECT);
*)

end;




end.

