unit d_Config;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid, Grids, DBGrids, StdCtrls, ToolWin, ComCtrls, StdActns, ActnList,

  dm_Excel_to_classes,

  dm_Export_classes_to_MDB,
  dm_Config_mdb,

  u_const_db,


 // dm_Excel_to_DB,

  dm_Excel_file,
  u_import_classes,

 // u_import_classes,

  u_cx_vgrid,

  u_func,
  u_common_const,

  u_geo,

  u_db,


  u_Config_File, rxPlacemnt, Mask, rxToolEdit

  ;

type
  Tdlg_Config = class(TForm)
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    ActionList1: TActionList;
    act_LoadDataFromExcel: TAction;
    act_SaveToExportMDB: TAction;
    act_FileOpen1: TFileOpen;
    FormStorage1: TFormStorage;
    cxGrid2Level2: TcxGridLevel;
    cxGrid2DBTableView_property: TcxGridDBTableView;
    cxGrid2DBTableView_Link: TcxGridDBTableView;
    cxGrid2DBTableView_Linkid: TcxGridDBColumn;
    cxGrid2DBTableView_Linkcategory: TcxGridDBColumn;
    cxGrid2DBTableView_Linkname: TcxGridDBColumn;
    cxGrid2DBTableView_Linkis_virtual: TcxGridDBColumn;
    cxGrid2DBTableView_Linkcaption: TcxGridDBColumn;
    cxGrid2DBTableView_Linkcomment: TcxGridDBColumn;
    cxGrid2DBTableView_LinkFieldType: TcxGridDBColumn;
    cxGrid2DBTableView_propertyid: TcxGridDBColumn;
    cxGrid2DBTableView_propertycategory: TcxGridDBColumn;
    cxGrid2DBTableView_propertyname: TcxGridDBColumn;
    cxGrid2DBTableView_propertyis_virtual: TcxGridDBColumn;
    cxGrid2DBTableView_propertycaption: TcxGridDBColumn;
    cxGrid2DBTableView_propertycomment: TcxGridDBColumn;
    cxGrid2DBTableView_propertyFieldType: TcxGridDBColumn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
//    procedure act_LoadDataFromExcelExecute(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
  //  procedure Button3Click(Sender: TObject);
  //
  //  procedure col_namePropertiesEditValueChanged(Sender: TObject);
 //   procedure ToolButton1Click(Sender: TObject);
  private
  //  procedure LoadDataFromExcel;
  //  procedure SaveToExportMDB;

  public
    class function ExecDlg: boolean;


    { Public declarations }
  end;


implementation

{$R *.dfm}


var
  dlg_Config: Tdlg_Config;


// ---------------------------------------------------------------
class function Tdlg_Config.ExecDlg: boolean;
// ---------------------------------------------------------------
begin
  if not Assigned(dlg_Config) then
    dlg_Config :=Tdlg_Config.Create(Application);


  dlg_Config.Show;


//
//
//
//  with Tdlg_Config.Create(Application) do
//  begin
//  //  LoadDataFromExcel();
//
//    ShowModal;
//
//    Free;
//  end;
//

end;

procedure Tdlg_Config.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;


procedure Tdlg_Config.FormCreate(Sender: TObject);
begin
  cxGrid2.Align:=alClient;

//  dmConfig_mdb.view_Fields_for_link.Open;
//  dmConfig_mdb.view_Fields_for_property.Open;
//

end;

end.


