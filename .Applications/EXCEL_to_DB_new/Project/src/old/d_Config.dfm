object dlg_Config: Tdlg_Config
  Left = 556
  Top = 288
  Width = 711
  Height = 515
  Caption = 'dlg_Config'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid2: TcxGrid
    Left = 0
    Top = 0
    Width = 703
    Height = 385
    Align = alTop
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    RootLevelOptions.DetailTabsPosition = dtpTop
    object cxGrid2DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
    end
    object cxGrid2DBTableView_property: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmConfig_mdb.ds_view_Fields_for_property
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NewItemRow.Visible = True
      OptionsView.GroupByBox = False
      object cxGrid2DBTableView_propertyid: TcxGridDBColumn
        DataBinding.FieldName = 'id'
      end
      object cxGrid2DBTableView_propertycategory: TcxGridDBColumn
        DataBinding.FieldName = 'category'
      end
      object cxGrid2DBTableView_propertyname: TcxGridDBColumn
        DataBinding.FieldName = 'name'
      end
      object cxGrid2DBTableView_propertyis_virtual: TcxGridDBColumn
        DataBinding.FieldName = 'is_virtual'
      end
      object cxGrid2DBTableView_propertycaption: TcxGridDBColumn
        DataBinding.FieldName = 'caption'
      end
      object cxGrid2DBTableView_propertycomment: TcxGridDBColumn
        DataBinding.FieldName = 'comment'
      end
      object cxGrid2DBTableView_propertyFieldType: TcxGridDBColumn
        DataBinding.FieldName = 'FieldType'
      end
    end
    object cxGrid2DBTableView_Link: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmConfig_mdb.ds_view_Fields_for_link
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NewItemRow.Visible = True
      OptionsView.GroupByBox = False
      object cxGrid2DBTableView_Linkid: TcxGridDBColumn
        DataBinding.FieldName = 'id'
      end
      object cxGrid2DBTableView_Linkcategory: TcxGridDBColumn
        DataBinding.FieldName = 'category'
        Width = 47
      end
      object cxGrid2DBTableView_Linkname: TcxGridDBColumn
        DataBinding.FieldName = 'name'
      end
      object cxGrid2DBTableView_Linkis_virtual: TcxGridDBColumn
        DataBinding.FieldName = 'is_virtual'
      end
      object cxGrid2DBTableView_Linkcaption: TcxGridDBColumn
        DataBinding.FieldName = 'caption'
      end
      object cxGrid2DBTableView_Linkcomment: TcxGridDBColumn
        DataBinding.FieldName = 'comment'
      end
      object cxGrid2DBTableView_LinkFieldType: TcxGridDBColumn
        DataBinding.FieldName = 'FieldType'
      end
    end
    object cxGrid2Level1: TcxGridLevel
      Caption = 'link'
      GridView = cxGrid2DBTableView_Link
    end
    object cxGrid2Level2: TcxGridLevel
      Caption = 'prop'
      GridView = cxGrid2DBTableView_property
    end
  end
  object ActionList1: TActionList
    Left = 24
    Top = 432
    object act_LoadDataFromExcel: TAction
      Caption = 'LoadDataFromExcel'
    end
    object act_SaveToExportMDB: TAction
      Caption = 'act_SaveToExportMDB'
    end
    object act_FileOpen1: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 88
    Top = 432
  end
end
