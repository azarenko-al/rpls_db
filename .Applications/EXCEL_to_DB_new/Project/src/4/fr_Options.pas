unit fr_Options;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxVGrid, cxControls, cxInplaceContainer,

  u_func,

  u_Config_File
  ;

type
  Tframe_Options = class(TForm)
    cxVerticalGrid1: TcxVerticalGrid;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;
    row_Power_dBm2: TcxEditorRow;
    row_Bitrate_Mbps2: TcxEditorRow;
    row_Threshold_BER_32: TcxEditorRow;
    row_Threshold_BER_62: TcxEditorRow;
    cxVerticalGrid1CategoryRow3: TcxCategoryRow;
    row_Gain_dB2: TcxEditorRow;
    row_Diameter2: TcxEditorRow;
    row_Height2: TcxEditorRow;
    row_DataType: TcxEditorRow;
    row_CoordSys: TcxEditorRow;
    cxVerticalGrid1EditorRow3: TcxEditorRow;
    row_FirstRowIndex_1: TcxEditorRow;
    row_Sheets: TcxEditorRow;
    cxVerticalGrid1CategoryRow4: TcxCategoryRow;
    procedure FormCreate(Sender: TObject);
  private
    procedure SaveTClass;

  public
    procedure LoadFromlass;

  end;

(*
var
  frame_Options: Tframe_Options;
  *)


implementation

{$R *.dfm}

procedure Tframe_Options.FormCreate(Sender: TObject);
begin

end;

// ---------------------------------------------------------------
procedure Tframe_Options.SaveTClass;
// ---------------------------------------------------------------
begin

 // g_ConfigFile.

  with g_ConfigFile do
  begin
    SheetName       := row_Sheets.Properties.Value;
    FirstRowIndex_1:= AsInteger (row_FirstRowIndex_1.Properties.Value);



    Height_M        :=  AsInteger(row_Height2.Properties.Value);
    Gain_dB         :=  AsFloat(row_Gain_dB2.Properties.Value);
    Diameter        :=  AsFloat(row_Diameter2.Properties.Value);

    Power_dBm       :=  AsFloat(row_Power_dBm2.Properties.Value);
    Threshold_BER_3 :=  AsFloat(row_Threshold_BER_32.Properties.Value);
    Threshold_BER_6 :=  AsFloat(row_Threshold_BER_62.Properties.Value);
    Bitrate_Mbps    :=  AsInteger(row_Bitrate_Mbps2.Properties.Value);

    case row_DataType.Properties.Value of
      0: DataSourceType:=dtProp;
      1: DataSourceType:=dtLink;
    end;

    case row_CoordSys.Properties.Value of
      0: CoordSys:=csPulkovo;
      1: CoordSys:=csWGS;
    end;



  end;



end;


// ---------------------------------------------------------------
procedure Tframe_Options.LoadFromlass;
// ---------------------------------------------------------------
var
  iValue: Integer;
begin

 // g_ConfigFile.

  with g_ConfigFile do
  begin
    row_Sheets.Properties.Value := SheetName;

    row_Height2.Properties.Value          := Height_M;
    row_Gain_dB2.Properties.Value         := Gain_dB;
    row_Diameter2.Properties.Value        := Diameter;

    row_Power_dBm2.Properties.Value       := Power_dBm;
    row_Threshold_BER_32.Properties.Value := Threshold_BER_3;
    row_Threshold_BER_62.Properties.Value := Threshold_BER_6;
    row_Bitrate_Mbps2.Properties.Value    := Bitrate_Mbps;



    case DataSourceType of
      dtProp: iValue:=0;
      dtLink: iValue:=1;
    end;


    row_DataType.Properties.Value :=iValue;


(*
    case row_DataType.Properties.Value of
      0: DataType:=dtProp;
      1: DataType:=dtLink;
    end;

    case row_CoordSys.Properties.Value of
      0: CoordSys:=csPulkovo;
      1: CoordSys:=csWGS;
    end;

*)



  end;



end;



end.


