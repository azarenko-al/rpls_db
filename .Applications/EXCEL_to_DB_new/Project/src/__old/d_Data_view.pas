unit d_Data_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid, Grids, DBGrids, StdCtrls, ToolWin, ComCtrls, StdActns, ActnList,
  rxPlacemnt, Mask, rxToolEdit, cxInplaceContainer, cxTL,

  cxDropDownEdit,

  u_ini_Excel_to_DB_params,

  u_ini_data_export_import_params,
  u_vars,


  dm_Export_classes_to_MDB,

  dm_Export_classes_link_to_MDB,

 // dm_Excel_Export_to_MDB,
  dm_Config_mdb,

  dm_Excel_to_classes,

  u_const_db,


  u_files,

 // dm_Excel_to_DB,

  dm_Excel_file,
  u_import_classes,

 // u_import_classes,

  u_cx_vgrid,

  u_func,
  u_common_const,

  u_run,

  u_geo,

  u_db,

  u_Config_File,


  cxTLData, cxDBTL, cxStyles, cxSplitter, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, cxTextEdit, cxDBLookupComboBox, cxCheckBox,
  cxMaskEdit, cxTLdxBarBuiltInMenu, System.Actions

  ;

type
  Tdlg_Data_view = class(TForm)
    cxGrid_top: TcxGrid;
    cxGrid_topDBTableView1: TcxGridDBTableView;
    cxGrid_topTableView_Data: TcxGridTableView;
    cxGrid_topLevel1: TcxGridLevel;
    pn_Fields: TPanel;
    cxGrid_bottom: TcxGrid;
    cxGridDBTableView_Columns: TcxGridDBTableView;
    col_index: TcxGridDBColumn;
    col_header: TcxGridDBColumn;
    col_name: TcxGridDBColumn;
    col_key: TcxGridDBColumn;
    cxGridLevel2: TcxGridLevel;
    ActionList1: TActionList;
    act_SaveToExportMDB: TAction;
    ToolBar1: TToolBar;
    FormStorage1: TFormStorage;
    cxGridDBTableView_ColumnsColumn1: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxGridDBTableView_ColumnsColumn2: TcxGridDBColumn;
    cxStyle_used: TcxStyle;
    Button5: TButton;
    cxStyle_header: TcxStyle;
    act_Exec: TAction;
    ed_Filename1: TFilenameEdit;
    Button2: TButton;
    Button1: TButton;
    Button3: TButton;
    cxGrid_topTableView_DataColumn1: TcxGridColumn;
    cxGrid_topTableView_DataColumn2: TcxGridColumn;
    cxSplitter1: TcxSplitter;
    Panel2: TPanel;
    Bevel1: TBevel;
    Panel3: TPanel;
    Button7: TButton;
    cxSplitter2: TcxSplitter;
    cxStyle_Column_selected: TcxStyle;    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure act_LoadDataFromExcelExecute(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure col_namePropertiesCloseUp(Sender: TObject);

//    procedure DoOnColumnGetContentStyle(Sender: TcxCustomGridTableView; ARecord:
//        TcxCustomGridRecord; AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
        
    procedure cxGrid2TableView_DataColumn1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure cxGridDBTableView_ColumnsFocusedItemChanged(Sender:
        TcxCustomGridTableView; APrevFocusedItem, AFocusedItem:
        TcxCustomGridTableItem);

//    procedure cxGrid2TableView_DataStylesGetContentStyle(
 //     Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  //    AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
  //  procedure Button3Click(Sender: TObject);
  //
  //  procedure col_namePropertiesEditValueChanged(Sender: TObject);
 //   procedure ToolButton1Click(Sender: TObject);
  private
    FUsedColumnIndexList: TStringList;

    FFileName_MDB : string;


        
    procedure LoadDataFromExcel;
    procedure SaveToExportMDB;

  public
    class function ExecDlg: boolean;

    procedure RunConverter;


    { Public declarations }
  end;

var
  dlg_Data_view: Tdlg_Data_view;

implementation

{$R *.dfm}


// ---------------------------------------------------------------
class function Tdlg_Data_view.ExecDlg: boolean;
// ---------------------------------------------------------------
begin
  with Tdlg_Data_view.Create(Application) do
  begin

    ShowModal;

    Free;
  end;


end;

// ---------------------------------------------------------------
procedure Tdlg_Data_view.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  Caption :='������';

  cxGrid_top.Align:=alTop;
  pn_Fields.Align:=alClient;

  FUsedColumnIndexList:=TStringList.Create;


  LoadDataFromExcel();

  col_Index.Caption  :='�';
  col_Header.Caption :='��������� (Excel)';
  col_Name.Caption   :='���� � ��';
  col_Key.Caption    :='�������� ����';


  col_Key.Visible := False;



  FFileName_MDB:=GetTempFileNameWithExt('mdb');

//  FFileName_MDB:= ChangeFileExt(g_ConfigFile.FileName, '.temp.mdb');
  FFileName_MDB:= ChangeFileExt(g_ConfigFile.FileName, '.mdb');


  {.$DEFINE test}


  {$IFDEF test}
 // ToolBar1.Visible:=True;


 // FFileName_MDB := ed_FileName1.FileName;

  {$ENDIF}



end;



procedure Tdlg_Data_view.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FUsedColumnIndexList);
end;


// ---------------------------------------------------------------
procedure Tdlg_Data_view.SaveToExportMDB;
// ---------------------------------------------------------------
//var
//  sFileName: string;
begin
 // sFileName := ed_FileName.FileName;
//    's:\1111.mdb';



//    dmExcel_to_DB.Params.CoordSystem := iCoordSys;
//    dmExcel_to_DB.Params.FirstHeaderLine := 1;

  case g_ConfigFile.DataSourceType of
    dtProp: begin
              dmExcel_to_Classes.ExportToClasses_Property();

              TdmExport_Classes_to_MDB.Init;
              dmExport_Classes_to_MDB.SaveToExportMDB(FFileName_MDB,  dmExcel_to_Classes.Project);

            end;

    dtLink: begin
              dmExcel_to_Classes.ExportToClasses_LInk;

              TdmExport_classes_link_to_MDB.Init;
              dmExport_classes_link_to_MDB.SaveToExportMDB(FFileName_MDB, dmExcel_to_Classes.Project);
            end;
    //dmExport_classes_link_to_MDB. ExportToClasses _Property();

//         procedure SaveToExportMDB(aFileName: string; aProject: TExcelProject);

  end;





end;


// ---------------------------------------------------------------
procedure Tdlg_Data_view.act_LoadDataFromExcelExecute(Sender: TObject);
// ---------------------------------------------------------------
var
  s: string;
begin
  if Sender=act_Exec then
  begin
    SaveToExportMDB();
///////////////////////
    runConverter();


    s:=Format ('exec dbo.sp_Project_restore_after_import %d', [g_ConfigFile.Runtime.Project_ID]);
    g_ConfigFile.Runtime.ADOConnection.Execute(s);


  end;

end;


procedure Tdlg_Data_view.Button2Click(Sender: TObject);
begin
  dmExcel_to_Classes.SaveColumns;

end;

procedure Tdlg_Data_view.Button4Click(Sender: TObject);
var
  oProperties: TcxPopupEditProperties;
begin
  oProperties:=TcxPopupEditProperties(cxGridDBTableView_ColumnsColumn1.Properties);

  GetParentForm(oProperties.PopupControl).Hide;
end;


procedure Tdlg_Data_view.Button5Click(Sender: TObject);
begin
  FormStorage1.RestoreFormPlacement;
end;

// ---------------------------------------------------------------
procedure Tdlg_Data_view.col_namePropertiesCloseUp(Sender: TObject);
// ---------------------------------------------------------------
var
  k: Integer;
begin
  k:=cxGridDBTableView_Columns.DataController.FocusedRecordIndex;

  if FUsedColumnIndexList.IndexOfObject(Pointer(k))<0 then
    FUsedColumnIndexList.AddObject( IntToStr(k), Pointer(k) );

  cxGrid_topTableView_Data.LayoutChanged();
end;



// ---------------------------------------------------------------
procedure Tdlg_Data_view.LoadDataFromExcel;
// ---------------------------------------------------------------
var
  I: Integer;
  oColumn: TcxGridDBColumn;
begin
//  g_ConfigFile.DataSourceType:=

  dmConfig_mdb.OpenFieldsDataset(g_ConfigFile.DataSourceType);

  dmExcel_to_Classes.LoadDataFromExcelFile(
      g_ConfigFile.FileName,

      cxGrid_topTableView_Data );


  g_ConfigFile.GetUsedColumns(FUsedColumnIndexList);


  for I := 0 to cxGrid_topTableView_Data.ColumnCount - 1 do
    cxGrid_topTableView_Data.Columns[i].Styles.OnGetContentStyle:=cxGrid2TableView_DataColumn1StylesGetContentStyle;



 // cxGrid2TableView_Data.ApplyBestFit();

end;


procedure Tdlg_Data_view.cxGrid2TableView_DataColumn1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  k: Integer;
begin
  k:=ARecord.Index;

  if FUsedColumnIndexList.IndexOfObject(Pointer(AItem.Index)) >=0 then
    AStyle := cxStyle_used;

 //   k:=AItem.ID;

 //   AItem.Caption
    
//  Sender.
    
//  if cxGridDBTableView_Columns.DataController.RecNo-1 = AItem.ID  then
//    AStyle := cxStyle_Column_selected;
              
end;

procedure Tdlg_Data_view.cxGridDBTableView_ColumnsFocusedItemChanged(Sender:
    TcxCustomGridTableView; APrevFocusedItem, AFocusedItem:
    TcxCustomGridTableItem);
var
  k: Integer;
begin
  k:=  cxGridDBTableView_Columns.DataController.RecNo;

//  cxGrid_top.Height:=cxGrid_top.Height+1;
//  cxGrid_top.Height:=cxGrid_top.Height-1;
  

//  cxGrid_topTableView_Data.Columns[k].re
  
 // cxGrid_topTableView_Data.Invalidate(true);
  
//  PostMessage(cxGrid2.Handle, WM_)
//  cxGrid2.Invalidate(True);
  
end;

//procedure Tdlg_Data_view.DoOnColumnGetContentStyle(Sender:
//    TcxCustomGridTableView; ARecord: TcxCustomGridRecord; AItem:
//    TcxCustomGridTableItem; out AStyle: TcxStyle);
//
//var
//  k: Integer;
//begin
//  k:=ARecord.Index;
//
//  if FUsedColumnIndexList.IndexOfObject(Pointer(AItem.Index)) >=0 then
//    AStyle := cxStyle_used;
//
//end;

//-------------------------------------------------------------------
procedure Tdlg_Data_view.RunConverter;
//-------------------------------------------------------------------
const
  TEMP_FILENAME = 'project_export.ini';

 // PROG_data_export_import = 'data_export_import.exe';

var
  iProjectID: Integer;
  sFile : string;

  oParams: TIni_Data_export_import_params;
//  sMdbFileName: string;

begin
//  assert(g_Excel_to_DB_params.ProjectID1 > 0);

  assert(g_ConfigFile.Runtime.Project_ID > 0);



  ////////////////////////////////


  sFile := g_ApplicationDataDir + TEMP_FILENAME;


//  sMdbFileName:= GetTempFileNameWithExt('mdb');
 // sMdbFileName:= ed_Filename.FileName;

 // iProjectID := AsInteger(DBLookupComboBox_Projects.KeyValue);


//  sFile := g_ApplicationDataDir + TEMP_FILENAME;

  oParams:=TIni_Data_export_import_params.Create;
 // oParams.ProjectID := iProjectID;


//  if g_Excel_to_DB_params.ProjectID>0 then
  oParams.ProjectID:=g_ConfigFile.Runtime.Project_ID;
//--  oParams.ProjectID:=g_Excel_to_DB_params.ProjectID1;


   assert(oParams.ProjectID>0);


  oParams.IsAuto := True;

  oParams.Mode     := mtImport_project;
 // oParams.IsAuto   := True;

  oParams.FileName_MDB := FFileName_MDB;
  oParams.ConnectionString := g_Excel_to_DB_params.ConnectionString;


//      FMdbFileName

  oParams.SaveToIni(sFile);

  FreeAndNil(oParams);

  // -------------------------

  RunApp (GetApplicationDir()+ PROG_data_export_import_exe, DoubleQuotedStr(sFile));
//   ShellExec (GetApplicationDir()+ PROG_data_export_import_exe, DoubleQuotedStr(sFile));

end;




end.


