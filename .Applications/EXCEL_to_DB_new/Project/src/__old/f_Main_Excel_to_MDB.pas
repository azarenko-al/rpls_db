unit f_Main_Excel_to_MDB;

interface

uses
  Classes, Controls, Forms,    DBGrids, rxToolEdit, ActnList,
  rxPlacemnt, ExtCtrls,  cxGridDBTableView, cxGridLevel,
  Variants, SysUtils,
  cxGridCustomView, Grids, ToolWin, Mask, cxVGrid, cxInplaceContainer,
  cxGrid, cxGridCustomTableView, cxGridTableView, cxClasses, cxControls,
  Dialogs, DB, ComCtrls, StdCtrls, DBCtrls,  cxDropDownEdit,Menus, StdActns,

 // dm_Main,

  dm_Main_Data,
  
  u_const_db,

  //d_Config,
  fr_Options,

   u_vars,

  u_ini_Excel_to_DB_params,


 // fr_Options,

  d_Data_view,

  u_Config_File,


  dm_Excel_to_classes,

  dm_Excel_file,
  u_import_classes,

 // u_import_classes,

  u_cx_vgrid,

  u_files,

  u_func,
  u_common_const,

  u_geo,

  u_db,
                 
 //u_compare_classes, 

 ADODB, System.Actions;

type
  Tfrm_Main = class(TForm)
    ActionList1: TActionList;
    FormStorage1: TFormStorage;
    act_Open_sheets: TAction;
    act_Config: TAction;
    PopupMenu1: TPopupMenu;
    Panel1: TPanel;
    pn_Main: TPanel;
    GroupBox2: TGroupBox;
    ed_FileName: TFilenameEdit;
    Button2: TButton;
    act_Exec: TAction;
    Button1: TButton;
    GroupBox_Project: TGroupBox;
    DBLookupComboBox_Projects: TDBLookupComboBox;
    Bevel1: TBevel;
    Panel2: TPanel;
    Button7: TButton;
    qry_Projects: TADOQuery;
    ds_Projects: TDataSource;
    Button4: TButton;
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_LoadDataFromExcelExecute(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
//    procedure Button3Click(Sender: TObject);
    //procedure Button5Click(Sender: TObject);
//    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure ed_FileNameAfterDialog(Sender: TObject; var Name: string; var Action:
        Boolean);
    procedure FormCreate(Sender: TObject);
//    procedure frame_Options11Click(Sender: TObject);

  //  procedure row_FileEditPropertiesButtonClick(Sender: TObject;
   //   AButtonIndex: Integer);
  private
    Fframe_Options: Tframe_Options;

  //  Fframe_Options: Tframe_Options;

    procedure Open_sheets;
//    FConfigFile: TConfigFile;

    procedure Exec;
    procedure InitParams;
    procedure Open_Projects;

    procedure SaveToClass;
    procedure Test;


  public
  end;

var
  frm_Main: Tfrm_Main;

implementation

{$R *.dfm}



procedure Tfrm_Main.ActionList1Update(Action: TBasicAction; var Handled:  Boolean);
begin
  act_Exec.Enabled:= AsString(Fframe_Options.row_Sheets.Properties.Value) <> '';

end;

// ---------------------------------------------------------------
procedure Tfrm_Main.act_LoadDataFromExcelExecute(Sender: TObject);
// ---------------------------------------------------------------
begin
  if Sender=act_Exec then
    Exec

(*
  // -------------------------
  else if Sender=act_Config then
  // -------------------------
    Tdlg_Config.ExecDlg
*)


  // -------------------------
  else if Sender=act_Open_sheets then
  // -------------------------
  begin
    Open_sheets();
  end else

end;

procedure Tfrm_Main.Button4Click(Sender: TObject);
begin
  test;
end;

procedure Tfrm_Main.Button5Click(Sender: TObject);
begin

end;



// ---------------------------------------------------------------
procedure Tfrm_Main.Open_sheets;
// ---------------------------------------------------------------
var
  sFileName: string;
  oList: TStrings;
begin


  oList:=Fframe_Options.GetSheetList();

  dmExcel_file.GetSheets (ed_FileName.FileName, oList);


//  ..if ListBox1.Items.Count>0 then
  //  ListBox1.ItemIndex := 0;

  sFileName:=g_ConfigFile.GetIniFileName(ed_FileName.FileName);

  g_ConfigFile.LoadFromIniFile(sFileName);


//  frame_Options11.cxVerticalGrid1.HideEdit;
  Fframe_Options.LoadFromClass;



  if oList.Count>0 then
    Fframe_Options.row_Sheets.Properties.Value := oList[0];


end;



procedure Tfrm_Main.Button7Click(Sender: TObject);
begin
  Exec;

end;




procedure Tfrm_Main.ed_FileNameAfterDialog(Sender: TObject; var Name: string; var
    Action: Boolean);
begin
  act_Open_sheets.Execute;
end;


// ---------------------------------------------------------------
procedure Tfrm_Main.InitParams;
// ---------------------------------------------------------------
const
//  DEF_SOFTWARE_NAME= 'RPLS_DB_LINK';
//  DEF_TEMP_FILENAME = 'project_export.ini';
  DEF_TEMP_FILENAME = 'project_export.ini';

var
  sIniFileName: string;
begin
  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
    sIniFileName:=g_ApplicationDataDir+ DEF_TEMP_FILENAME;


  g_Excel_to_DB_params.LoadFromIni(sIniFileName);

end;



//-----------------------------------------------------------
procedure Tfrm_Main.Open_Projects;
//-----------------------------------------------------------
begin
 // if not qry_Projects.Active then
//  begin

  Assert(Assigned (dmMain_Data));
  
    qry_Projects.Connection :=dmMain_Data.ADOConnection_SQL;
    db_OpenQuery(qry_Projects, 'SELECT id,name FROM '+TBL_project+' ORDER BY name');

  //  DBLookupComboBox_Projects.KeyValue := 1568;

     DBLookupComboBox_Projects.KeyValue :=g_Excel_to_DB_params.Project_ID;

//    DBLookupComboBox_Projects.KeyValue :=dmMain.ProjectID;
 // end;

end;




//-----------------------------------------------------------
procedure Tfrm_Main.Test;
//-----------------------------------------------------------
begin
 // if not qry_Projects.Active then

  DBLookupComboBox_Projects.KeyValue := 1568;

  ed_FileName.FileName:='P:\1\����� Radiolinks_KE.xlsx';

  Assert (FileExists (ed_FileName.FileName));

//  Open_sheets; ????? Radiolinks_KE.xlsx

  act_Open_sheets.Execute;

end;




// ---------------------------------------------------------------
procedure Tfrm_Main.FormCreate(Sender: TObject);
// ---------------------------------------------------------------

begin
  InitParams();

 // Caption :='����������� Excel -> DB '+ GetAppVersionStr();


  Caption:= '����������� Excel -> DB '+  //GetAppVersionStr() +
        '  ������: ' + GetApplicationFileDateAsStr();
//    + FormatDateTime ('yyyy-mm-dd hh:mm', GetFileDate (Application.Exename));



  CreateChildForm_(Tframe_Options,  Fframe_Options, pn_Main);



//  ListView1.Items.Clear;
//  ListView1.Columns.Clear;
//
 // FConfigFile:=TConfigFile.Create;

 // frame_Options11.Init;



//  CreateChildForm(Tframe_Options,   Fframe_Options, TabSheet_options);

 // frame_Options11.Align:=alClient;


  pn_Main.Align:=alClient;
//  PageControl1.ActivePageIndex :=0;

///  act_FileOpen1.Dialog.Filter := DEF_EXCEL_Filter;//'Excel (*.xls,*.xlsx)|*.xls;*.xlsx';


  ed_FileName.Filter := DEF_EXCEL_Filter;//'Excel (*.xls,*.xlsx)|*.xls;*.xlsx';
 // ed_FileName_mdb.Filter  := DEF_EXCEL_Filter;//'Excel (*.xls,*.xlsx)|*.xls;*.xlsx';

  TdmExcel_to_Classes.Init;

  act_Open_sheets.Caption :='�������';


  Open_Projects;

//  Test;

end;    


// ---------------------------------------------------------------
procedure Tfrm_Main.SaveToClass();
// ---------------------------------------------------------------
var
  sFileName: string;
begin
//  Assert(Assigned(g_ConfigFile), 'Value not assigned');

  g_ConfigFile.Runtime.Project_ID:=DBLookupComboBox_Projects.KeyValue;
  g_ConfigFile.Runtime.ADOConnection:=dmMain_Data.ADOConnection_SQL;

  Assert (g_ConfigFile.Runtime.Project_ID > 0, 'Project_ID > 0');



  g_ConfigFile.FileName := ed_FileName.FileName;

  Fframe_Options.SaveToClass;

  sFileName:=g_ConfigFile.GetIniFileName(ed_FileName.FileName);

  g_ConfigFile.SaveToIniFile(sFileName);

{
     Runtime: record
      Project_ID : Integer;

      ADOConnection: TADOConnection;
    end;
 }


end;



procedure Tfrm_Main.Exec;
begin
  SaveToClass;

  Tdlg_Data_view.ExecDlg;
end;



end.



  {

// ---------------------------------------------------------------
procedure Tfrm_Main.row_FileEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
// ---------------------------------------------------------------
begin
(*  if act_FileOpen1.Execute then
  begin
    cx_SetButtonEditText (Sender, act_FileOpen1.Dialog.FileName);

  end;*)

end;


