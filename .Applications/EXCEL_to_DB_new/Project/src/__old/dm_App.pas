unit dm_App;

interface

uses
  SysUtils, Forms, Classes,

//  dm_Onega_Data,

  u_ini_Excel_to_DB_params,

  dm_Config_mdb,

  dm_Excel_to_Classes,
  dm_Export_Classes_to_MDB,
  dm_Export_classes_link_to_MDB,

  

  u_db,              

                
  u_func,

  u_vars,

  dm_Main_Data, 

  Data.DB, Data.Win.ADODB

  ;

type
  TdmApp = class(TDataModule)
   procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
   class procedure Init;

  end;

var
  dmApp: TdmApp;

implementation

uses f_Main_Excel_to_MDB;

{$R *.dfm}


class procedure TdmApp.Init;
begin
  if not Assigned(dmApp) then
    dmApp := TdmApp.Create(Application);

end;


// ---------------------------------------------------------------
procedure TdmApp.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
const
//  DEF_SOFTWARE_NAME= 'RPLS_DB_LINK';
//  DEF_TEMP_FILENAME = 'project_export.ini';
  DEF_TEMP_FILENAME = 'project_export.ini';
  
  REGISTRY_LOGIN_ = 'Software\Onega\RPLS_DB_LINK\Login_new1\';
    
  
var
  sIniFileName: string;
begin
  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
    sIniFileName:=g_ApplicationDataDir+ DEF_TEMP_FILENAME;


  g_Excel_to_DB_params.LoadFromIni(sIniFileName);

  
// begin
//  TdmMain.Init;
//  if not dmMain.OpenDB_reg then
//    Exit;


//  TdmMDB.Init;

 // TdmOnega_Data.Init;
  TdmMain_Data.Init;
        
  if g_Excel_to_DB_params.ConnectionString = '' then
  begin
    if not dmMain_Data.OpenFromReg (REGISTRY_LOGIN_) then 
      Exit;
                          
  end else begin
    if not dmMain_Data.OpenADOConnectionString (g_Excel_to_DB_params.ConnectionString) then 
      Exit;
      
  end;
  

  TdmExcel_to_Classes.Init;
  TdmExport_Classes_to_MDB.Init;
  TdmExport_classes_link_to_MDB.Init;

(*

  if sMode='' then
  begin
    sMode :='import_project';
  end;*)

  TdmConfig_mdb.Init;


  Application.CreateForm(Tfrm_Main, frm_Main);
//  frm_Main_1.Init (StrToExportModeType(smode), iProjectID, FParams);
//  frm_Main_1.Init (FParams);



end;


end.

{
// ---------------------------------------------------------------
procedure Tfrm_Main.InitParams;
// ---------------------------------------------------------------
const
//  DEF_SOFTWARE_NAME= 'RPLS_DB_LINK';
//  DEF_TEMP_FILENAME = 'project_export.ini';
  DEF_TEMP_FILENAME = 'project_export.ini';

var
  sIniFileName: string;
begin
  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
    sIniFileName:=g_ApplicationDataDir+ DEF_TEMP_FILENAME;


  g_Excel_to_DB_params.LoadFromIni(sIniFileName);

end;

