unit u_config_file;

interface
uses
 // SysUtils, Classes, DB,
  ADODB, Forms,

  SysUtils, IniFiles, Classes

//  u_xml_document

  ;


type
  TCoordSys       = (csPulkovo_CK42, csPulkovo_CK95, csWGS, csGOST);
  TDataSourceType = (dtProp,dtLink);

  TColumn= record
              //  Header : string;
                FieldName : string;
                IsKey : Boolean;

                DegreeFormat : string;
              end;

  // ---------------------------------------------------------------
  TConfigFile = class(TObject)
  // ---------------------------------------------------------------
  private
    FFileName: string;

  public
    Runtime: record
      Project_ID : Integer;

      ADOConnection: TADOConnection;
    end;

//  public
    FileName : string;


    Height  : Double;
    Gain   : Double;
    Diameter  : Double;

    Freq_MHz        : Double;
    Power_dBm       : Double;
    Threshold_BER_3 : Double;
    Threshold_BER_6 : Double;
    Bitrate_Mbps    : Double;

    IsGet_antenna_H_from_site : Boolean; //
 // public
  //  SheetIndex : Integer;
    SheetName : string;

    HeaderRowIndex_1: Integer;
    DataRowIndex_1: Integer;

    
    DataSourceType: TDataSourceType;

    Columns: array of TColumn;

    KeyColumnIndex  : Integer;

    CoordSys: TCoordSys; // (csNone, csPulkovo, csWGS);


    constructor Create;
//    destructor Destroy; override;

    function GetIniFileName(aFileName: string): string;

    function LoadFromIniFile(aFileName: string): boolean;
    procedure GetUsedColumns(aList: TStringList);

    procedure SaveToIniFile(aFileName: string);

    procedure SetDefaults;

  end;


var
  g_ConfigFile: TConfigFile;


implementation

uses
  XMLIntf;

const
  DEF_main = 'main';
  DEF_columns = 'columns';


// ---------------------------------------------------------------
constructor TConfigFile.Create;
// ---------------------------------------------------------------
begin
  inherited Create;

  SetDefaults();
end;


// ---------------------------------------------------------------
procedure TConfigFile.SetDefaults;
// ---------------------------------------------------------------
begin
  HeaderRowIndex_1:=1;
  DataRowIndex_1  :=2;


  Height       :=30;
  Diameter     :=0.6;
  Gain         :=36;

  Power_dBm       :=25;
  Threshold_BER_3 :=-80;
  Threshold_BER_6 :=-80;
  Bitrate_Mbps    :=4;

  Freq_MHz:=18000;

end;



function TConfigFile.GetIniFileName(aFileName: string): string;
begin
  Result := ChangeFileExt(aFileName, '.config.ini');
end;

// ---------------------------------------------------------------
function TConfigFile.LoadFromIniFile(aFileName: string): boolean;
// ---------------------------------------------------------------
var
  I: Integer;
  iCount: Integer;
  oIniFile: TIniFile;
begin
  if not FileExists (aFileName) then
    exit;

  Assert (FileExists (aFileName), aFileName);

  oIniFile := TIniFile.Create(aFileName);

//  FirstRowIndex_1 := oIniFile.ReadInteger (DEF_main, 'FirstRowIndex', FirstRowIndex_1);

  HeaderRowIndex_1:= oIniFile.ReadInteger (DEF_main, 'HeaderRowIndex', HeaderRowIndex_1);  
  DataRowIndex_1  := oIniFile.ReadInteger (DEF_main, 'DataRowIndex', DataRowIndex_1);

  if HeaderRowIndex_1=0 then  HeaderRowIndex_1:=1;
  if DataRowIndex_1=0   then  DataRowIndex_1:=2;
  
//  SheetIndex := oIniFile.ReadInteger (DEF_main, 'SheetIndex', SheetIndex);
  SheetName  := oIniFile.ReadString (DEF_main, 'SheetName', '');


  CoordSys := TCoordSys(oIniFile.ReadInteger (DEF_main, 'CoordSys', 0));
  DataSourceType := TDataSourceType(oIniFile.ReadInteger (DEF_main, 'DataSourceType', 0));


  IsGet_antenna_H_from_site   := oIniFile.ReadBool (DEF_main, 'IsGet_antenna_H_from_site', IsGet_antenna_H_from_site);



  iCount:=oIniFile.ReadInteger(DEF_columns, 'count',0);

  SetLength(Columns, iCount);

  for I := 0 to High(Columns) do
  begin
  //  Columns[i].Header:= oIniFile.ReadString (DEF_columns, Format('%d_Header', [i]), '');
    Columns[i].FieldName := oIniFile.ReadString (DEF_columns, Format('%d_Field_Name', [i]), '');
    Columns[i].IsKey     := oIniFile.ReadBool   (DEF_columns, Format('%d_Is_Key', [i]), False);
    Columns[i].DegreeFormat  := oIniFile.ReadString (DEF_columns, Format('%d_Degree_Format', [i]), '');

  end;


  FreeAndNil(oIniFile);
end;

// ---------------------------------------------------------------
procedure TConfigFile.GetUsedColumns(aList: TStringList);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  aList.Clear;

  for I := 0 to Length(Columns) - 1 do
    if (Columns[i].FieldName<>'') or (Columns[i].IsKey) then
       aList.AddObject( IntToStr(i), Pointer(i) );


  //  if FFilledColumnIndexList.IndexOfObject(Pointer(k))<0 then
//    FFilledColumnIndexList.AddObject( IntToStr(k), Pointer(k) );

  // TODO -cMM: TConfigFile.GetUsedColumns default body inserted
end;


// ---------------------------------------------------------------
procedure TConfigFile.SaveToIniFile(aFileName: string);
// ---------------------------------------------------------------

var
  I: Integer;
  oIniFile: TIniFile;
begin
  DeleteFile(aFileName);


  oIniFile := TIniFile.Create(aFileName);

  oIniFile.WriteString (DEF_main, 'SheetName', SheetName);


  oIniFile.WriteInteger (DEF_main, 'HeaderRowIndex', HeaderRowIndex_1);
  oIniFile.WriteInteger (DEF_main, 'DataRowIndex',   DataRowIndex_1);
  
 // oIniFile.WriteInteger (DEF_main, 'SheetIndex', SheetIndex);    

  oIniFile.WriteInteger (DEF_main, 'CoordSys',       Integer(CoordSys));
  oIniFile.WriteInteger (DEF_main, 'DataSourceType', Integer(DataSourceType));

  oIniFile.WriteBool (DEF_main, 'IsGet_antenna_H_from_site', IsGet_antenna_H_from_site);


  oIniFile.WriteInteger (DEF_columns, 'count', Length(Columns));

  for I := 0 to High(Columns) do
  begin
   // oIniFile.WriteString (DEF_columns, Format('%d_Header_name', [i]), Columns[i].Header );
    oIniFile.WriteString (DEF_columns, Format('%d_Field_name',  [i]), Columns[i].FieldName );
    oIniFile.WriteBool   (DEF_columns, Format('%d_Is_key', [i]), Columns[i].Iskey );
    oIniFile.WriteString (DEF_columns, Format('%d_Degree_Format', [i]), Columns[i].DegreeFormat );


  end;


  FreeAndNil(oIniFile);
end;



initialization
  g_ConfigFile:=TConfigFile.Create;

finalization
  FreeAndNil(g_ConfigFile);


end.


