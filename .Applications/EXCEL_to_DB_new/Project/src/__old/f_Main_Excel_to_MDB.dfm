object frm_Main: Tfrm_Main
  Left = 1277
  Top = 222
  Caption = 'Excel to DB'
  ClientHeight = 592
  ClientWidth = 680
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 557
    Width = 680
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 680
      Height = 3
      Align = alTop
      Shape = bsTopLine
    end
    object Button1: TButton
      Left = 6
      Top = 6
      Width = 83
      Height = 25
      Action = act_Config
      TabOrder = 0
      Visible = False
    end
    object Panel2: TPanel
      Left = 558
      Top = 3
      Width = 122
      Height = 32
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object Button7: TButton
        Left = 30
        Top = 3
        Width = 83
        Height = 25
        Action = act_Exec
        TabOrder = 0
      end
    end
  end
  object pn_Main: TPanel
    Left = 0
    Top = 0
    Width = 680
    Height = 433
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GroupBox2: TGroupBox
      Left = 0
      Top = 46
      Width = 680
      Height = 75
      Align = alTop
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090' Excel'
      TabOrder = 0
      DesignSize = (
        680
        75)
      object ed_FileName: TFilenameEdit
        Left = 8
        Top = 15
        Width = 665
        Height = 24
        OnAfterDialog = ed_FileNameAfterDialog
        Filter = 'All files (*.*)|*.*|qqq|*.33'
        Anchors = [akLeft, akTop, akRight]
        NumGlyphs = 1
        TabOrder = 0
        Text = ''
      end
      object Button2: TButton
        Left = 8
        Top = 47
        Width = 75
        Height = 20
        Action = act_Open_sheets
        TabOrder = 1
      end
      object Button4: TButton
        Left = 104
        Top = 47
        Width = 61
        Height = 20
        Caption = 'test'
        TabOrder = 2
        Visible = False
        OnClick = Button4Click
      end
    end
    object GroupBox_Project: TGroupBox
      Left = 0
      Top = 0
      Width = 680
      Height = 46
      Align = alTop
      Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1087#1088#1086#1077#1082#1090#1072' '#1074' DB:'
      TabOrder = 1
      DesignSize = (
        680
        46)
      object DBLookupComboBox_Projects: TDBLookupComboBox
        Left = 8
        Top = 17
        Width = 665
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        DropDownRows = 20
        KeyField = 'id'
        ListField = 'name'
        ListSource = ds_Projects
        TabOrder = 0
      end
    end
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 48
    Top = 472
    object act_Open_sheets: TAction
      Caption = #1054#1090#1082#1088#1099#1090#1100
      OnExecute = act_LoadDataFromExcelExecute
    end
    object act_Config: TAction
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
      OnExecute = act_LoadDataFromExcelExecute
    end
    object act_Exec: TAction
      Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100
      OnExecute = act_LoadDataFromExcelExecute
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\Excel_to_DB'
    Options = [fpPosition]
    UseRegistry = True
    StoredProps.Strings = (
      'ed_FileName.FileName')
    StoredValues = <>
    Left = 152
    Top = 468
  end
  object PopupMenu1: TPopupMenu
    Left = 336
    Top = 464
  end
  object qry_Projects: TADOQuery
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'SELECT id,name FROM PROJECT ORDER BY name')
    Left = 450
    Top = 463
  end
  object ds_Projects: TDataSource
    DataSet = qry_Projects
    Left = 526
    Top = 463
  end
end
