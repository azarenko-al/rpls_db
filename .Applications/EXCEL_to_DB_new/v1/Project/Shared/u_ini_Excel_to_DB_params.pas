unit u_ini_Excel_to_DB_params;

interface
uses
  SysUtils, IniFiles, Dialogs     
  ;


const
   PROG_Excel_to_DB_exe = 'Excel_to_DB.exe';

type

  // ---------------------------------------------------------------
  TIni_Excel_to_DB_params = class(TObject)
  // ---------------------------------------------------------------
  public
    Project_ID: Integer;
    ConnectionString : string;

    procedure SaveToIni(aFileName: String);
    procedure LoadFromIni(aFileName: String);
  end;


var
  g_Excel_to_DB_params : TIni_Excel_to_DB_params;



implementation

const
  DEF_Project_ID = 'Project_ID';
  DEF_MAIN = 'main';



// ---------------------------------------------------------------
procedure TIni_Excel_to_DB_params.LoadFromIni(aFileName: String);
// ---------------------------------------------------------------
var
  oIni: TMemIniFile;
  s: string;
begin
  if not FileExists(aFileName) then
    Exit;


  oIni:=TMemIniFile.Create (aFileName);

  // -------------------------
  Project_ID      :=oIni.ReadInteger(DEF_MAIN, DEF_Project_ID,  0);
  ConnectionString:=oIni.ReadString (DEF_MAIN, 'ConnectionString',  '');


  oIni.Free;
  // -------------------------

end;


// ---------------------------------------------------------------
procedure TIni_Excel_to_DB_params.SaveToIni(aFileName: String);
// ---------------------------------------------------------------
var
  oIni: TMemIniFile;
  s: string;
begin
  DeleteFile(aFileName);

  ForceDirectories( ExtractFileDir(aFileName) );

  // --------------------------------------

  oIni:=TMemIniFile.Create (aFileName);

 // oIni.EraseSection('main');

  oIni.WriteInteger(DEF_MAIN, DEF_Project_ID, Project_ID);
  oIni.WriteString (DEF_MAIN, 'ConnectionString', ConnectionString);

  oIni.UpdateFile;
  oIni.Free;


end;


initialization
  g_Excel_to_DB_params := TIni_Excel_to_DB_params.Create();

Finalization
  FreeAndNil(g_Excel_to_DB_params);


end.

