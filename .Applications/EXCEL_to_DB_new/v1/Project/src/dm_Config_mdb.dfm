object dmConfig_mdb: TdmConfig_mdb
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 1198
  Top = 987
  Height = 341
  Width = 669
  object ADOConnection_MDB1: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=C:\ON' +
      'EGA\RPLS_DB\bin\excel_to_DB.mdb;Mode=Share Deny None;Persist Sec' +
      'urity Info=False;Jet OLEDB:System database="";Jet OLEDB:Registry' +
      ' Path="";Jet OLEDB:Database Password="";Jet OLEDB:Engine Type=5;' +
      'Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Global Partial Bulk ' +
      'Ops=2;Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:New Databas' +
      'e Password="";Jet OLEDB:Create System Database=False;Jet OLEDB:E' +
      'ncrypt Database=False;Jet OLEDB:Don'#39't Copy Locale on Compact=Fal' +
      'se;Jet OLEDB:Compact Without Replica Repair=False;Jet OLEDB:SFP=' +
      'False;'
    ConnectionTimeout = 5
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 53
    Top = 19
  end
  object t_Fields: TADOTable
    Connection = ADOConnection_MDB1
    CursorType = ctStatic
    TableName = 'view_Fields_for_property'
    Left = 168
    Top = 16
  end
  object ds_Fields: TDataSource
    DataSet = t_Fields
    Left = 168
    Top = 64
  end
  object view_Fields_for_property: TADOTable
    Connection = ADOConnection_MDB1
    CursorType = ctStatic
    TableName = 'view_Fields_for_property'
    Left = 296
    Top = 16
  end
  object ds_view_Fields_for_property: TDataSource
    DataSet = view_Fields_for_property
    Left = 296
    Top = 64
  end
  object view_Fields_for_link: TADOTable
    Connection = ADOConnection_MDB1
    CursorType = ctStatic
    TableName = 'view_Fields_for_link'
    Left = 480
    Top = 16
  end
  object ds_view_Fields_for_link: TDataSource
    DataSet = view_Fields_for_link
    Left = 480
    Top = 64
  end
  object t_Equipment_to_LinkendType: TADOTable
    Connection = ADOConnection_MDB1
    CursorType = ctStatic
    IndexFieldNames = 'name'
    TableName = 'Equipment_to_LinkendType'
    Left = 176
    Top = 152
  end
  object ds_Equipment_to_LinkendType: TDataSource
    DataSet = t_Equipment_to_LinkendType
    Left = 168
    Top = 216
  end
  object t_Equipment_to_AntennaType: TADOTable
    Connection = ADOConnection_MDB1
    CursorType = ctStatic
    IndexFieldNames = 'name'
    TableName = 'Equipment_to_AntennaType'
    Left = 392
    Top = 152
    object t_Equipment_to_AntennaTypeid: TAutoIncField
      FieldName = 'id'
      ReadOnly = True
    end
    object t_Equipment_to_AntennaTypediameter: TFloatField
      FieldName = 'diameter'
      Precision = 2
    end
    object t_Equipment_to_AntennaTypeband: TIntegerField
      FieldName = 'band'
    end
    object t_Equipment_to_AntennaTypeName: TWideStringField
      FieldName = 'Name'
      Size = 255
    end
  end
  object ds_Equipment_to_AntennaType: TDataSource
    DataSet = t_Equipment_to_AntennaType
    Left = 392
    Top = 216
  end
end
