unit u_Task_XML_to_Classes;

interface

uses
  SysUtils, Variants,  Dialogs,

  d_Progress,

  

//  xml_project_template,

  

  u_files,
  u_func, Classes;

type

  TTask_XML_to_Classes = class
  private
    FLog: TStringList;

    procedure AddAntenna(aXCell: TXCell; aIXMlAntennaType: IXMlAntennaType);
    procedure AddCell(aXSite: TXSite; aIXMlSectorType: IXMlSectorType);
    procedure AddCombiners(aIXMLCombinerType: IXMLCOMBINERSType);
    procedure AddReliefMatrixes_new(aIXMLMatrixesType: IXMLMatrixesType);
    procedure AddRRLs(aIXMLRRLsType: IXMLRRLSType);
    procedure AddStations(aIXMlStationsType: IXMlStationsType; aScenario:
        TxScenario);
    procedure AddTrxs(aIXMLTRXSType: IXMLTRXSType);

    procedure LogError(aMsg: string);
    procedure Log(aMsg: string);
  public
    XProject: TxProject;
  public
    Params: record
      RplsProject_XmlFileName: string;
    end;

    constructor Create;
    destructor Destroy; override;

    procedure ExecuteDlg;
//    destructor Destroy; override;
    procedure ExecuteProc();

    class procedure Init;
  end;

var
  Task_XML_to_Classes: TTask_XML_to_Classes;

//==================================================================
//==================================================================
implementation


class procedure TTask_XML_to_Classes.Init;
begin
  if not Assigned(Task_XML_to_Classes) then
    Task_XML_to_Classes:=TTask_XML_to_Classes.Create;
end;

constructor TTask_XML_to_Classes.Create;
begin
  inherited Create;
  XProject := TxProject.Create();
  FLog := TStringList.Create();
end;

destructor TTask_XML_to_Classes.Destroy;
begin
  FreeAndNil(FLog);
  FreeAndNil(XProject);
  inherited Destroy;
end;

//------------------------------------------------
procedure TTask_XML_to_Classes.AddAntenna(aXCell: TXCell; aIXMlAntennaType:
    IXMlAntennaType);
//------------------------------------------------
var
  oXAntennaType: TxAntennaType;
  oXAntenna: TxAntenna;
  sModelName: string;
  sDNA_vert: string;
  sDNA_horz: string;
  s: string;
begin
  s:=aIXMlAntennaType.DNA_FileName;
  s:=ExtractFileNameNoExt(s);

  sModelName:=s;
//  sModelName:=aIXMlAntennaType.Name;
  if sModelName='' then
    Exit;

  sDNA_vert :='';
  sDNA_horz :='';

  oXAntenna:=aXCell.AntennaList.AddItem;


  oXAntenna.Name   := aIXMlAntennaType.Name;
  oXAntenna.AZIMUTH:= AsFloat(aIXMlAntennaType.Azimuth);
  oXAntenna.Height := AsFloat(aIXMlAntennaType.Height);
  oXAntenna.TILT   := AsFloat(aIXMlAntennaType.Slope);

//  oXAntenna.Fre  :=  AsFloat(aIXMlAntennaType.Gain);


//  if sModelName<>'' then
 //   if not tbl_Antenna_Type.Locate (FLD_NAME, sModelName, []) then
//  begin
  //  DoLog('Add Antenna Type: '+ sModelName);

  if Assigned(aIXMlAntennaType.DNA_vert) then
    sDNA_vert :=aIXMlAntennaType.DNA_vert.Value;

  if Assigned(aIXMlAntennaType.DNA_horz) then
    sDNA_horz :=aIXMlAntennaType.DNA_horz.Value;

(*
   with oAntennas[i].Params.AntennaType do
          for j:=0 to High(DiagramHorz) do
            sDiagramHorz:= sDiagramHorz + Format('%g=%g;', [DiagramHorz[j].Angle, DiagramHorz[j].Loss]);

        with oAntennas[i].Params.AntennaType do
          for j:=0 to High(DiagramVert) do
            sDiagramVert:= sDiagramVert + Format('%g=%g;', [DiagramVert[j].Angle, DiagramVert[j].Loss]);

*)

    oXAntennaType:= XProject.AntennaTypeList.FindByName(sModelName);

    if not Assigned(oXAntennaType) then
    begin
      oXAntennaType:=XProject.AntennaTypeList.AddItem;

      oXAntennaType.Name      :=sModelName;
      oXAntennaType.GAIN_dB   := AsFloat(aIXMlAntennaType.Gain);
      oXAntennaType.FREQ_MHz  := AsFloat(aIXMlAntennaType.Freq);

  //    oXAntennaType.TILT_TYPE :=IIF(oAntennas[i].Params.AntennaType.DiagramVert[0].Loss=0, 1, 0);

  //    oXAntennaType.VERT_WIDTH:=oAntennas[i].GetVertWidth;
  //    oXAntennaType.HORZ_WIDTH:=oAntennas[i].GetHorzWidth;

      oXAntennaType.vert_mask := sDNA_vert;
      oXAntennaType.horz_mask := sDNA_horz;

    end;


    oXAntenna.AntennaType_Ref := oXAntennaType;



//  end;
end;

//------------------------------------------------
procedure TTask_XML_to_Classes.AddCell(aXSite: TXSite; aIXMlSectorType: IXMlSectorType);
//------------------------------------------------
var
  sNetStandardName: string;
  i: integer;
//  oSectorList: TSectorList;
  oXTrx: TxTrxType;
  oXCombiner: TxCombiner;

 // oCombiner: TCombiner;
//  oNetStandard: TNetStandard;

  oXCell: TxCell;
  iTrxID: integer;
  iCombinerID: Integer;
begin
//  with dmDB_MDB do
//  begin
   // oSectorList:=aSite.Sectors;


  iTrxID  := AsInteger(aIXMlSectorType.TrxID);

  oxTrx := XProject.TrxTypeList.FindByID(iTrxID);
  if not Assigned(oxTrx) then
  begin
    LogError ('Trx not found');
    Exit;
  end;

  iCombinerID  := AsInteger(aIXMlSectorType.CombinerID);
  oxCombiner := XProject.CombinerList.FindByID(iCombinerID);

(*  if not Assigned(oxCombiner) then
  begin
    LogError ('Trx not found');
    Exit;
  end;
*)


  oXCell := aXSite.CellList.AddItem;


//  TTrx(gl_Rep.Trxs.FindObjectByID (oSectorList[i].TrxID ));

(*
          <sector id="3954" Name="������3" Cell_ID="83" Color="5765284" BSIC_id="-1" BCCH="0" Hysteresis="0" Forbidden_Channels="" Allowed_Channels="" Fixed_Channels="741; 747; 753; 832" FreqRequiredCount="0" CalcRadius="0" DeltaGrad="2" CalcMOdel_id="0" tp_id="-1" CalcMOdel_name="" tp_name="" K0_open="0" K0_closed="0" NetStandard_ID="2" CombinerID="104639" TrxID="104422">
            <ANTENNA id="3955" Name="�������1" Azimuth="305" Height="37" ServDistance="0" Checked="0" FeederLoss="0" b="0" l="0" FixedPosition="0" Model="HBXX6517DSVTM_et-4" Producer="" Gain="19.05" Freq="1785" Slope="0" Rotate="0" TiltElectrical="4" DNA_FileName="C:\Antennas\Andrew_new_antennas092007\HBXX-6517DS-VTM_1785ACO_et4.msi" OMNI="0">
            </ANTENNA>
          </sector>

*)


 //   for i:=0 to oSectorList.Count-1 do
  //  begin
    //  DoProgress2(i, oSectorList.Count);

   //   oTrx        := TTrx(gl_Rep.Trxs.FindObjectByID (oSectorList[i].TrxID ));
    //  oCombiner   := TCombiner(gl_Rep.Combiners.FindObjectByID (oSectorList[i].CombinerID));
     // oNetStandard:= gl_Rep.NetStandards.FindByID(oSectorList[i].NetStandard_ID);

 //     if not assigned(oNetStandard) then
  //      Continue;

     // if assigned(oNetStandard) then
   //   sNetStandardName:=oNetStandard.Name_;
    //..  else
     //   sNetStandardName:='';


     // oXCell:=aXSite.CellList.AddItem;

      oXCell.NAME:= aIXMlSectorType.name;

//////      oXCell.CALC_RADIUS_km:=   aSite.Params.Radius;

     // oXCell.SITE_ID,         aSiteID),
//      oXCell.SITE_NAME,       aSite.Name_),

{      TRX_ID,          IIF(oTrx.DB_ID>0, oTrx.DB_ID, null)  ),
      TRX_NAME,        oTrx.Name_ ),
}

      if Assigned(oxCombiner) then
        oXCell.COMBINER_LOSS_dB := oxCombiner.Loss_dB;

      oXCell.Color:= AsInteger (aIXMlSectorType.Color);

    //  oXCell.CELLID:=  AsInteger (aIXMlSectorType.Cell_ID);//         oSectorList[i].Params.Cell_ID;

      oXCell.POWER_W:=         oxTrx.POWER_W;
      oXCell.Sensitivity_dBm:= oxTrx.Sensitivity_dBm;
      oXCell.FREQ_MHz:=        oxTrx.FREQ_MHz;

    //  oXCell.CALC_MODEL_ID:=   oSectorList[i].Params.CalcModel_ID;

    //  oXCell.Calc_MOdel_name:= oSectorList[i].Params.CalcModel_Name;
    //  oXCell.COLOR:=           oSectorList[i].Params.Color;

(*      oXCell.K0:=              oSectorList[i].Params.Calibrate.k0;
      oXCell.K0_OPEN:=         oSectorList[i].Params.Calibrate.k0_open;
      oXCell.K0_CLOSED:=       oSectorList[i].Params.Calibrate.k0_closed;
*)
    //  oXCell.CELLID:=          oSectorList[i].Params.Cell_ID;
     // oXCell.CELL_LAYER_NAME:= oNetStandard.Name_;

     /////// oXCell.COMBINER_LOSS:=   oCombiner.Params.Losses;

   //   oXCell.BCCH:=            oSectorList[i].Params.BCCH;
    //  oXCell.HYSTERESIS:=      oSectorList[i].Params.Hysteresis;

    //  oXCell.BSIC_ID:=         oSectorList[i].Params.BSIC_id;

(*      oXCell.FREQ_REQ_COUNT:=  oSectorList[i].Params.FreqRequiredCount;
      oXCell.FREQ_FIXED:=      IntArrayToString(oSectorList[i].Params.FreqFixedArr);
      oXCell.FREQ_ALLOW:=      IntArrayToString(oSectorList[i].Params.FreqAllowedArr);
      oXCell.FREQ_FORBIDDEN:=  IntArrayToString(oSectorList[i].Params.FreqForbiddenArr);

*)


   for I := 0 to aIXMlSectorType. Count - 1 do
     AddAntenna (oXCell, aIXMlSectorType.ANTENNA[i]);

end;



//------------------------------------------------------------
procedure TTask_XML_to_Classes.AddReliefMatrixes_new(aIXMLMatrixesType:
    IXMLMatrixesType);
//------------------------------------------------------------
var
  i: Integer;
  oXRelief: u_xml_classes.TxReliefMatrix;
begin
//  G_Progress.Do
//  DoLog('Add Matrixes...');

  for I := 0 to aIXMLMatrixesType.Count - 1 do
  begin
    oXRelief:=XProject.ReliefMatrixList.AddItem;

    oXRelief.FileName:=  aIXMLMatrixesType[i].FileName;
    oXRelief.STEP:=      AsInteger(aIXMLMatrixesType[i].Step);
    oXRelief.ZONENUM:=   AsInteger(aIXMLMatrixesType[i].Zone);
  //  oXRelief.FILESIZE:=  AsInteger(aIXMLMatrixesType[i].FileSize);

  end;
end;



//------------------------------------------------------------
procedure TTask_XML_to_Classes.AddTrxs(aIXMLTRXSType: IXMLTRXSType);
//------------------------------------------------------------
var
  i: Integer;
  oXTrx: u_xml_classes.TxTrxType;
begin
//  DoLog('Add Matrixes...');

  for I := 0 to aIXMLTRXSType.Count - 1 do
  begin
    oXTrx:=XProject.TrxTypeList.AddItem;

    oXTrx.Name:=  aIXMLTRXSType[i].name;

    oXTrx.ID:=  AsInteger(aIXMLTRXSType[i].id);
    oXTrx.POWER_W:= AsFloat(aIXMLTRXSType[i].Power);
//    oXTrx.Sensitivity_dBm :=   AsInteger(aIXMLTRXSType[i].);
   // oXTrx.FILESIZE:=  AsInteger(aIXMLMatrixesType[i].FileSize);

   oXTrx.Sensitivity_dBm :=AsInteger(aIXMLTRXSType[i].Sensitivity);
   oXTrx.FREQ_MHz :=AsInteger(aIXMLTRXSType[i].FreqWork);

  end;

end;


//------------------------------------------------------------
procedure TTask_XML_to_Classes.AddCombiners(aIXMLCombinerType: IXMLCOMBINERSType);
//------------------------------------------------------------
var
  i: Integer;
  obj: u_xml_classes.TxCombiner;
begin
//  DoLog('Add Matrixes...');

  for I := 0 to aIXMLCombinerType.Count - 1 do
  begin
    obj:=XProject.CombinerList.AddItem;

    obj.Name:=  aIXMLCombinerType[i].name;

    obj.ID:=  AsInteger(aIXMLCombinerType[i].id);
    obj.Loss_dB:= AsFloat(aIXMLCombinerType[i].Losses);
  end;

end;


//------------------------------------------------------------
procedure TTask_XML_to_Classes.AddRRLs(aIXMLRRLsType: IXMLRRLSType);
//------------------------------------------------------------
var
//  j: integer;
  i: Integer;
  j: Integer;
 // iParent_id: Integer;
  oAntenna: TxAntenna;

 //
  oXScenario: TxScenario;

//  oXTrx: TxTrx;
  oXLink: TxLink;
  sSite1_Name: string;
  sSite2_Name: string;

  oProperty1: TxProperty;
  oProperty2: TxProperty;
  s: string;

  sPolarization: string;

  vRRL: IXMLRRLType;

  oLinkEnd1: TxLinkend;
  oLinkEnd2: TxLinkend;
  sParent_id: string;

//  oXSite: TxSite;

 // vIXMlSectorType: IXMlSectorType;

begin
//  DoLog('Add stations...');

//  oXScenario := XProject.ScenarioList.AddItem;

  for j := 0 to XProject.ScenarioList.Count-1 do
  for I := 0 to aIXMLRRLsType.Count - 1 do
  begin
  //  aIXMLRRLsType[i]
    vRRL := aIXMLRRLsType[i];

    oXScenario  := XProject.ScenarioList[j];


    sParent_id := aIXMLRRLsType[i].Parent_id;

    if sParent_id<>oXScenario.Id then
      Continue;



    oXScenario  := XProject.ScenarioList.FindByID(sParent_id);

    if not Assigned(oXScenario) then
    begin
      FLog.Add('Parent_id ����� �����������: '+ sParent_id);

      Continue;
    end;


    sSite1_Name := vRRL.Site1_Name;
    sSite2_Name := vRRL.Site2_Name;

    oProperty1 := oXScenario.PropertyList.FindByName(sSite1_Name);
    oProperty2 := oXScenario.PropertyList.FindByName(sSite2_Name);

    if not (Assigned(oProperty1)) then //and Assigned(oProperty2)) then
    begin
      FLog.Add(Format('��������: %s', [oXScenario.Name]));
      FLog.Add(Format('  ��������: %s', [vRRL.Name]));
      FLog.Add(Format('     �������� 1 �� �������: %s. ������� �����.', [sSite1_Name]));

      oProperty1 := oXScenario.PropertyList.AddItem;
      oProperty1.Name      := sSite1_Name;
      oProperty1.BLPoint.B := AsFloat( vRRL.B1);
      oProperty1.BLPoint.L := AsFloat( vRRL.L1);

//      Continue;
    end;

    if not (Assigned(oProperty2)) then //and Assigned(oProperty2)) then
    begin
      FLog.Add(Format('��������: %s', [oXScenario.Name]));
      FLog.Add(Format('  ��������: %s', [vRRL.Name]));
      FLog.Add(Format('     �������� 2 �� �������: %s. ������� �����.', [sSite2_Name]));

      oProperty2 := oXScenario.PropertyList.AddItem;
      oProperty2.Name      := sSite2_Name;
      oProperty2.BLPoint.B := AsFloat( vRRL.B2);
      oProperty2.BLPoint.L := AsFloat( vRRL.L2);


//      Continue;
    end;


  //��  if not (Assigned(oProperty1) and Assigned(oProperty2)) then
   //��   Continue;


    oXLink:=oXScenario.LinkList.AddItem;

//    oXLink.Scenario_Name := oXScenario.Name;

    oXLink.LinkEnd1.Property_Ref :=oProperty1;
    oXLink.LinkEnd2.Property_Ref :=oProperty2;

//    oLinkEnd1 :=

    oXLink.LinkEnd1.Name:=oProperty1.Name + '->' +oProperty2.Name;
    oXLink.LinkEnd2.Name:=oProperty2.Name + '->' +oProperty1.Name;

//    vRRL.


    oXLink.Name:= aIXMLRRLsType[i].Name;
    oXLink.Rx_Level_dBm:= AsInteger(vRRL.EmpLevel);
    oXLink.NFrenel     := AsFloat(vRRL.NFrenel);
    oXLink.Refraction  := AsFloat( vRRL.Refraction);

//    oXLink.Length_m    := AsFloat( vRRL.Le);

  //  vRRL.Le

    if AsInteger(vRRL.PolarizationType)=0 then
      sPolarization :='h'
    else
      sPolarization :='v';


    oXLink.LinkEnd1.Bitrate_Mbps   := AsInteger( vRRL.Speed);
    oXLink.LinkEnd1.KratnostByFreq := AsInteger( vRRL.KratnostByFreq);

    oXLink.LinkEnd1.kratnostBySpace:= AsInteger( vRRL.kratnostBySpace);
    oXLink.LinkEnd1.signature_width := AsFloat( vRRL.SignatureWidth);
    oXLink.LinkEnd1.signature_height:= AsFloat( vRRL.SignatureHeight);
    oXLink.LinkEnd1.Loss := AsFloat( vRRL.Site1_Feeder_Losses);

//    if oXLink.LinkEnd1.kratnostBySpace>0 then
  //    oXLink.LinkEnd1.kratnostBySpace:=oXLink.LinkEnd1.kratnostBySpace-1;


    oXLink.LinkEnd2.Bitrate_Mbps   := oXLink.LinkEnd1.Bitrate_Mbps;
    oXLink.LinkEnd2.KratnostByFreq := oXLink.LinkEnd1.KratnostByFreq ;
    oXLink.LinkEnd2.kratnostBySpace:= oXLink.LinkEnd1.kratnostBySpace;
    oXLink.LinkEnd2.signature_width := AsFloat( vRRL.SignatureWidth);
    oXLink.LinkEnd2.signature_height:= AsFloat( vRRL.SignatureHeight);
    oXLink.LinkEnd2.Loss := AsFloat( vRRL.Site2_Feeder_Losses);

//    if oXLink.LinkEnd2.kratnostBySpace>0 then
  //    oXLink.LinkEnd2.kratnostBySpace:=oXLink.LinkEnd2.kratnostBySpace-1;


    oXLink.LinkEnd1.Power_dBm :=AsFloat(vRRL.Site1_Power);
    oXLink.LinkEnd1.THRESHOLD_dBm:=AsFloat(vRRL.Site2_Sensitivity);

    oXLink.LinkEnd2.Power_dBm :=oXLink.LinkEnd1.Power_dBm;
    oXLink.LinkEnd2.THRESHOLD_dBm :=oXLink.LinkEnd1.THRESHOLD_dBm;


    //---------------------------
    with oXLink.TRB do begin
    //---------------------------
      GST_SESR_1               := AsFloat(vRRL.TRB_1); // '����������� �������� SESR ��� ����, %');
      GST_Kng_2                := AsFloat(vRRL.TRB_2); // '����������� �������� ���  ��� ����, %');
      GST_Length_KM_3          := AsFloat(vRRL.TRB_3); // '����� ����, ��');
      KNG_dop_4                := AsFloat(vRRL.TRB_4); // '���������� ����� ���, ������������� �������������}');
      SPACE_LIMIT_5            := AsFloat(vRRL.TRB_5); // '���������� ������������� ������� p(a%)');
      SPACE_LIMIT_PROBABILITY_6:= AsFloat(vRRL.TRB_6); // '���������� ����������� a% ������������� ������������ �������� p(a%) [%]');
    end;

    //---------------------------
    with oXLink.DLT do begin
    //---------------------------
//      Calc_Method : Integer; // 0 "����, ITU-R � 16 ����� ��" (�� ���������)
	//			                     // 1 "���� � 53363 - 2009"

      Profile_Step_KM_1           := AsFloat(vRRL.DLT_1);//'��� ��������� �������, �� ( 0 - ��������� ����)');
      precision_V_diffraction_2   := AsFloat(vRRL.DLT_2);//'�������� ������ ������� V����(g)=V���.�, ��');
      precision_g_V_diffraction_3 := AsFloat(vRRL.DLT_3);//'�������� ������ ������� g(V����)=g(V���.�), 10^-8');
      max_gradient_for_subrefr_4  := AsFloat(vRRL.DLT_4);//'������������ �������� ��� ������� ������������, 10^-8');
    end;

    //-------------------------------------------------------------------
    with oXLink.RRV do begin  //������� ��������������� ��������� (���)
    //-------------------------------------------------------------------

      gradient_diel_1 := AsFloat(vRRL.RRV_1);   // '������� �������� ��������� ����.�������������,10^-8 1/�' );
      gradient_deviation_2 := AsFloat(vRRL.RRV_2);   // '����������� ���������� ���������,10^-8 1/�' );

      underlying_terrain_type_3:= AsFloat(vRRL.RRV_3); //'��� ������������ �������.(1...3,0->����.�����=����.���.)' );

      steam_wet_4     := AsFloat(vRRL.RRV_4);   // '���������� ��������� �������� ����, �/���.�');
      Q_factor_5      := AsFloat(vRRL.RRV_5);   // 'Q-������ ������ �����������');
      climate_factor_6:= AsFloat(vRRL.RRV_6);   //'������������� ������ K��, 10^-6');
      factor_B_7      := AsFloat(vRRL.RRV_7);   // '�������� ������������ b ��� ������� ������');
      factor_C_8      := AsFloat(vRRL.RRV_8);   // '�������� ������������ c ��� ������� ������');
      FACTOR_D_9      := AsFloat(vRRL.RRV_9);   // '�������� ������������ d ��� ������� ������');
      RAIN_RATE_10    := AsFloat(vRRL.RRV_10);   // '������������� ����� � ������� 0.01% �������, ��/���');
      REFRACTION_11   := AsFloat(vRRL.RRV_11);       //'�����. ��������� (��� ��.���������=0, ����� �.�.=0)');

      terrain_type_14 := AsFloat(vRRL.RRV_12);   //'��� ���������.(1...3,0->����.�����=����.���.)' );
    end;



  //  oXLink.LinkEnd1.Rx_Level_dBm :=oXLink.Rx_Level_dBm;
  //  oXLink.LinkEnd2.Rx_Level_dBm :=oXLink.Rx_Level_dBm;

//aIXMLRRLsType[i].NFrenel




    //-------- TX ---------
    oXLink.LinkEnd1.FREQ_GHz     :=AsFloat(vRRL.FreqWork);
    oXLink.LinkEnd2.FREQ_GHz     :=AsFloat(vRRL.FreqWork);

    oXLink.LinkEnd1.Bitrate_Mbps :=AsInteger(vRRL.Speed);
    oXLink.LinkEnd2.Bitrate_Mbps :=AsInteger(vRRL.Speed);


  //  oXLink.LinkEnd1.Power_dBm :=AsFloat(aIXMLRRLsType[i].Site1_Power);

//    oXLink.LinkEnd2.Power_dBm :=oXLink.;

    // -------------------------
    oAntenna:=oXLink.LinkEnd1.AntennaList.AddItem;
    oAntenna.Name :='�������1';
    oAntenna.Height   := AsFloat(vRRL.Site1_Antenna_Height);
    oAntenna.DIAMETER := AsFloat(vRRL.Site1_Antenna_DIAMETER);
    oAntenna.GAIN_dB  := AsFloat(vRRL.Site1_Antenna_Gain);
    oAntenna.Diagram_Width  := AsFloat(vRRL.Site1_Antenna_Width);
//    oAntenna.vert_width  := AsFloat(vRRL.Site1_Antenna_Width);
    oAntenna.PolarizationStr  := sPolarization;

    oProperty1.GROUND_HEIGHT := AsFloat(vRRL.Site1_rel_height);

(*    vRRL.site2_

    if AsFloat(vRRL.Site1_Antenna_Height)<>AsFloat(vRRL.Site1_Antenna_Height) then
    begin

    end;

*)
//    AsFloat(vRRL.Site2)

  //  oAntenna.AZIMUTH := AsFloat(aIXMLRRLsType[i].Site1_Antenna_Azimuth_Given);

//    oAntenna.AZIMUTH  := AsFloat(aIXMLRRLsType[i].Site1_Antenna_ AZIMUTH);

   // -------------------------
    oAntenna:=oXLink.LinkEnd2.AntennaList.AddItem;
    oAntenna.Name :='�������1';
    oAntenna.Height   := AsFloat(vRRL.Site2_Antenna_Height);
    oAntenna.DIAMETER := AsFloat(vRRL.Site2_Antenna_DIAMETER);
    oAntenna.GAIN_dB  := AsFloat(vRRL.Site2_Antenna_Gain);
    oAntenna.Diagram_Width  := AsFloat(vRRL.Site2_Antenna_Width);
    oAntenna.PolarizationStr := sPolarization;

    oProperty2.GROUND_HEIGHT := AsFloat(vRRL.Site2_rel_height);


 //   oXLink.LinkEnd1.KratnostByFreq := AsInteger( vRRL.KratnostByFreq);
    if oXLink.LinkEnd1.kratnostBySpace<>1 then
      if AsFloat(vRRL.Site2_Antenna_Height)<>AsFloat(vRRL.Site2_Extra_Antenna_Height) then
    begin
      oAntenna:=oXLink.LinkEnd2.AntennaList.AddItem;
      oAntenna.Name :='�������2';
      oAntenna.Height   := AsFloat(vRRL.Site2_Extra_Antenna_Height);
      oAntenna.DIAMETER := AsFloat(vRRL.Site2_Extra_Antenna_DIAMETER);
      oAntenna.GAIN_dB  := AsFloat(vRRL.Site2_Extra_Antenna_Gain);
      oAntenna.Diagram_Width  := AsFloat(vRRL.Site2_Antenna_Width);
      oAntenna.PolarizationStr := sPolarization;

    end;


  end;

end;




//------------------------------------------------------------
procedure TTask_XML_to_Classes.AddStations(aIXMlStationsType: IXMlStationsType;
    aScenario: TxScenario);
//------------------------------------------------------------
var
  j: integer;
  i: Integer;
 // oXScenario: TxScenario;

  oXProperty: TxProperty;
  oXSite: TxSite;

  vIXMlSectorType: IXMlSectorType;
begin
//  DoLog('Add stations...');

//  oXScenario := XProject.ScenarioList.AddItem;

  for I := 0 to aIXMlStationsType.Count - 1 do
  begin
    oXProperty:=aScenario.PropertyList.AddItem;

//    oXProperty.Scenario_Name := aScenario.Name;

    oXProperty.Name:= aIXMlStationsType[i].Name;

    oXProperty.BLPoint.B  := AsFloat (aIXMlStationsType[i].B);
    oXProperty.BLPoint.L  := AsFloat (aIXMlStationsType[i].L);
    oXProperty.Address    := aIXMlStationsType[i].Address;
    oXProperty.Building   := aIXMlStationsType[i].Building;

    oXProperty.Number   := AsInteger(aIXMlStationsType[i].Number);

    oXProperty.Status   := AsInteger(aIXMlStationsType[i].CurrStatus);


//    oXProperty.Comment2   := aIXMlStationsType[i].Comment;


    if aIXMlStationsType[i].Count>0 then
    begin
      oXSite:=oXProperty.SiteList.AddItem;
      oXSite.Name:=oXProperty.Name;

      for j := 0 to aIXMlStationsType[i].Count - 1 do
        AddCell (oXSite, aIXMlStationsType[i].Sector[j]);
    end;

  end;

end;


procedure TTask_XML_to_Classes.ExecuteDlg;
begin
  Progress_ExecDlg_proc(ExecuteProc);

end;

//-----------------------------------------------------
procedure TTask_XML_to_Classes.ExecuteProc();
//-----------------------------------------------------
//var i: integer;
 //   sFileName_MDB : string;
var
  vXMLDocumentType: IXMLDocumentType;
  i: integer;
  sFolderName: string;

  oScenario: TxScenario;
begin
  FLog.Clear;

  XProject.Clear;

  if not FileExists(Params.RplsProject_XmlFileName) then
    Exit;



//  DoProgressMsg2 ('���������� ������...');
 // DoProgress2(0, 1);


//  dmmdb_InitFile(Params.RplsProjectFileName);

 // sFileName_MDB:=dmDB_MDB.GetFileName(Params.RplsProjectFileName);
//  sFileName_MDB:= ChangeFileExt(Params.RplsProjectFileName, DEF_MDB_EXTENSION);

{
  ShowMessage('DeleteFile(sFileName_MDB);');
      DeleteFile(sFileName_MDB);
}

{  if FileExists(sFileName_MDB) then
  begin
    if FileDateTime(sFileName_MDB) > FileDateTime(Params.RplsProjectFileName) then
    begin
      dmDB_MDB.ClearDB_ID (sFileName_MDB);
      Exit;
    end
    else
    begin
      DeleteFile(sFileName_MDB);
      dmDB_MDB.Create_MDB (sFileName_MDB);
    end;
  end
  else
    dmDB_MDB.Create_MDB (sFileName_MDB);

  dmDB_MDB.Clear;
}

//  dmDB_MDB.ADOConnection.BeginTrans;


  try
    vXMLDocumentType:=LoadDocument(Params.RplsProject_XmlFileName);
  except
    Exit;
  end;

  if not Assigned(vXMLDocumentType.Project) then
  begin
    ShowMessage('not Assigned(vXMLDocumentType.Project)');
    Exit;
  end;


  XProject.FileName := Params.RplsProject_XmlFileName;
  XProject.Name     := vXMLDocumentType.Project.Name;

 // i:=vXMLDocumentType.Project.Matrixes.Count;

  if Assigned(vXMLDocumentType.Project.Matrixes) then
    AddReliefMatrixes_new (vXMLDocumentType.Project.Matrixes);

(*

  if Assigned(vXMLDocumentType.Project.TRXS) then
    AddTrxs (vXMLDocumentType.Project.TRXS);

  if Assigned(vXMLDocumentType.Project.COMBINERS) then
    AddCombiners (vXMLDocumentType.Project.Combiners);
*)

  //  vXMLDocumentType.Project.TRXS


  if Assigned(vXMLDocumentType.Project.Scenario) then
    for I := 0 to vXMLDocumentType.Project.Scenario.Count - 1 do
    begin
      oScenario := XProject.ScenarioList.AddItem;
      oScenario.Name := vXMLDocumentType.Project.Scenario[i].Name;
      oScenario.ID   := vXMLDocumentType.Project.Scenario[i].id;


      AddStations (vXMLDocumentType.Project.Scenario[i].Stations, oScenario);


(*        oXTrx: u_xml_classes.TxTrxType;
begin
//  DoLog('Add Matrixes...');

  for I := 0 to aIXMLTRXSType.Count - 1 do
  begin
    oXTrx:=XProject.TrxTypeList.AddItem;
*)

      sFolderName :=vXMLDocumentType.Project.Scenario[i].Name;

  //    AddStations (vXMLDocumentType.Project.Scenario[i].Stations, sFolderName);
    end;


  if Assigned(vXMLDocumentType.Project.Scenario) then
    for I := 0 to vXMLDocumentType.Project.Scenario.Count - 1 do
    begin
   //   sFolderName :=vXMLDocumentType.Project.Scenario[i].Name;

  //    AddStations (vXMLDocumentType.Project.Scenario[i].Stations, sFolderName);
    end;



  if Assigned(vXMLDocumentType.Project.RRLS) then
   // for I := 0 to vXMLDocumentType.Project.RRLS.Count - 1 do
      AddRRLs (vXMLDocumentType.Project.RRLS);


  if FLog.Count>0 then
    ShellExec_Notepad_temp(FLog.Text);

//    ShowMessage(FLog.Text);



 // FrpProject:=TProject.Create;
 // Project_LoadFromXMLFile (gl_Project.Data, Params.RplsProjectFileName);
 // FScenario:=gl_Project.ActiveScenario;

 //// if FScenario=nil then
  //  Exit;

 // tbl_Property.Open;

//  dmDB_MDB.OpenTables();


{  AddTrxType ();
  AddTrafficModelTypes ();// Done !!!
  AddLAC ();
  AddBSIC ();
}

  //only link tables
(*
  AddProperty(); // Done!!!
  AddLinks();
  AddReliefMatrixes(); // Done !!!
*)
//  AddCalcRegion(); // Done best !!!


///  AddCellLayer();

//  dmDB_MDB.ADOConnection.CommitTrans;

//   MessageDlg ('����������� ���������!', mtCustom, [mbOk], 0);
//  FrpProject.Free;
end;



procedure TTask_XML_to_Classes.Log(aMsg: string);
begin

end;

procedure TTask_XML_to_Classes.LogError(aMsg: string);
begin

end;


begin
end.


