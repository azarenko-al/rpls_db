unit f_Main;

interface

uses
  SysUtils, Classes, Controls, Forms,  ActnList, Registry, Dialogs,
  GSTabs, StdCtrls, rxToolEdit, cxPropertiesStore, ComCtrls,

  u_Ini_Data_export_import_params,

  dm_Task_XML_Classes_To_DB,
  dm_MDB,

  //u_Task_XML_to_Classes,

  u_vars, 

  dm_Onega_DB_data,

  d_Wizard,

  dm_Main,
 
  u_func,
  u_dlg,
  u_db,
  u_reg,

  u_const_db,

  ExtCtrls, DB, ADODB, DBCtrls, ToolWin, Mask, rxPlacemnt
  ;


type
  Tfrm_Main = class(Tdlg_Wizard)
    ActionList2: TActionList;
    act_ConvProject: TAction;
    act_ConvDicts: TAction;
    act_Project_Clear: TAction;
    GSPages1: TGSPages;
    GSPage1: TGSPage;
    Panel1: TPanel;
    Bevel3: TBevel;
    btn_Close: TButton;
    Button3: TButton;
    Button2: TButton;
    StatusBar1: TStatusBar;
    qry_Projects: TADOQuery;
    ds_Projects: TDataSource;
    Button4: TButton;
    GroupBox2: TGroupBox;
    DBLookupComboBox_Projects: TDBLookupComboBox;
    GroupBox3: TGroupBox;
    ed_FileName: TFilenameEdit;
    Button1: TButton;
   procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
//    procedure btn_XML_to_memClick(Sender: TObject);
//    procedure btn_mem_to_DBClick(Sender: TObject);
    procedure DoOnButClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure _Action(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure Button1Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Panel1Click(Sender: TObject);
    procedure StatusBar1DblClick(Sender: TObject);
    procedure row_Use_Cache_MDBEditPropertiesChange(Sender: TObject);

  private
    FChanged: boolean;

    FMdbFileName: string;

    FProjectName: string;

    FCalcModelID,
    FProjectID,
    FClutterModelID: integer;

    IsConvCalc: string;

    procedure Convert2;
    procedure ConvProject();
    procedure Run_ImportFromMDB;
    procedure OpenProjects;
//    procedure Run11;
  public

    class function ExecDlg (): boolean;
  end;

var
  frm_Main: Tfrm_Main;


//==================================================================
implementation
 {$R *.dfm}


const
  FLD_CONV_CALC = 'ConvCalc';
  FLD_FILEDIR   = 'FILEDIR';


class function Tfrm_Main.ExecDlg (): boolean;
begin
  with Tfrm_Main.Create(Application) do
  begin
    ShowModal;

    Result := FChanged;

    Free;
  end;


end;



//-------------------------------------------------------------------
procedure Tfrm_Main.FormCreate(Sender: TObject);
//-------------------------------------------------------------------

{const
  TEMP_INI_FILE = 'c:\temp\rpls\Import_Project.ini';

var
  oInifile: TIniFile;
  sIniFileName: string;
 // iProjectID: integer;
 }

begin
  inherited;

  Caption :='����������� ������� RPLS XML -> DB '+ GetAppVersionStr();

  if dmMain.ProjectID>0 then
     DBLookupComboBox_Projects.KeyValue :=dmMain.ProjectID;



  ed_FileName.Filter := 'XML (*.xml)|*.xml';
 
 // Caption:='����������� �������';


//  GSPages1.Align:=alClient;

  GSPages1.ActivePageIndex:=0;
  GSPages1.Align:=alClient;


  SetActionName ('��������� ����������� ������� �� XML � ���� ������.');

//  gl_Reg.InitRegistry ('XML-DB');

  AddComponentProp (ed_FileName,    'Text');
//  AddComponentProp (ed_FileName,    PROP_FILENAME);

  AddComponentProp (GSPages1, PROP_ACTIVE_PAGE);
//  AddComponentProp (cxPageControl1, PROP_ACTIVE_PAGE);
//  AddComponentProp (row_Use_Cache_MDB, 'Value');

  cxPropertiesStore.RestoreFrom;

{  with gl_Reg do
  begin
    BeginGroup (Self, Name);
    AddControl (ed_FileName,   [PROP_FILENAME]);
    AddControl (ed_FileDicts,  [PROP_FILENAME]);
    AddControl (GSPages1,      [PROP_ACTIVE_PAGE_INDEX]);
  end;
}
 // FProjectID:= AsInteger(ParamStr(1));

{
  FProjectID:=417;
          }


 //////// ShowMessage ( IntToStr(FProjectID) );

  FProjectID:=dmMain.ProjectID;


  if gl_DB.GetNameByID (TBL_PROJECT, FProjectID) = '' then
    FProjectID:=0;

{  if (FProjectID <> 0) then
    pn_DB_Project.Visible:= False;
}

  with TRegIniFile.Create(FRegPath) do
 // with gl_Reg.RegIni do         //gl_Reg.
  begin
/////    FProjectID     := ReadInteger (Name, FLD_PROJECT_ID, 0);

    FClutterModelID:= ReadInteger ('', FLD_CLUTTER_MODEL_ID, 0);
    FCalcModelID   := ReadInteger ('', FLD_CALC_MODEL_ID, 0);
    IsConvCalc     := ReadString  ('', FLD_CONV_CALC, '');

//    row_Project_Dir.Text:= ReadString (Name, FLD_FILEDIR, '');
    Free;
  end;

 // ed_Project.Text     := gl_DB.GetNameByID (TBL_PROJECT, FProjectID)  + ' : '+ IntToStr(FProjectID);
 // row__CalcModel.Properties.Value  := gl_DB.GetNameByID (TBL_CALCMODEL, FCalcModelID);
//  row__AddCalc.Properties.Value    := IsConvCalc;

//  row_LinkType.Text   := gl_DB.GetNameByID (TBL_Link_Type, FLinkTypeID);
//  row_LinkEndType.Text:= gl_DB.GetNameByID (TBL_LINKEND_TYPE, FLinkEndTypeID);
 // row_Clu.Text        := gl_DB.GetNameByID (TBL_CLUTTER_MODEL, FClutterModelID);
//  row_AntennaType.Text:= gl_DB.GetNameByID (TBL_ANTENNA_TYPE, FAntennaTypeID);
//  row_Project_Dir.Text:= FNetDir;


(*  if FClutterMOdelID=0 then
    FClutterMOdelID:= dmClutterModel.GetDefaultID();

  row__Clu.Properties.Value := dmClutterModel.GetNameByID (FClutterModelID);

*)

  StatusBar1.SimpleText:=dmMain.LoginRec.ConnectionStatusStr;

  btn_Ok.Hide;
  act_Cancel.Caption:='�������';

 // cx_InitVerticalGrid(cxVerticalGrid1);


  OpenProjects;

end;


//-------------------------------------------------------------------
procedure Tfrm_Main.FormDestroy(Sender: TObject);
//-------------------------------------------------------------------
begin
  with TRegIniFile.Create(FRegPath) do
  begin
    WriteInteger ('', FLD_PROJECT_ID,       FProjectID);
    WriteInteger ('', FLD_CLUTTER_MODEL_ID, FClutterModelID);
    WriteInteger ('', FLD_CALC_MODEL_ID,    FCalcModelID);
  //  WriteString  ('', FLD_CONV_CALC,        row_AddCalc.Text);

    Free;
 //   WriteInteger (Name, FLD_LinkType_ID,      FLinkTypeID);
 //   WriteInteger (Name, FLD_LinkEnd_Type_ID,  FLinkEndTypeID);
//    WriteInteger (Name, FLD_ANTENNATYPE_ID,   FAntennaTypeID);
 //   WriteString  (Name, FLD_FILEDIR,          row_Project_Dir.Text);
  end;

// gl_Reg.

//  gl_Reg.SaveAndClearGroup (Self);

  inherited;
end;



//-------------------------------------------------------------------
procedure Tfrm_Main.ConvProject();
//-------------------------------------------------------------------
var
  sFileName: string;
begin
//  dmmdb_InitFile(aFileName);


  sFileName := ed_FileName.FileName;

  dmMDB.OpenMDB(sFileName);

//  dm_Task_XML_Classes_To_DB

    (*

  dmTask_XML_to_MDB.Params.RplsProjectFileName:= sFileName;  //'t:\test.xml';
  dmTask_XML_to_MDB.ExecuteDlg('����������� ������� �� ����� XML � ���� ������');

  // -------------------------
  TdmTask_XML_Classes_To_MDB.Init;
  dmTask_XML_Classes_To_MDB.XProject := dmTask_XML_to_MDB.XProject;
  dmTask_XML_Classes_To_MDB.SaveToMDB;

  //procedure TdmTask.SaveToMDB;

  Exit;




  if not Tdlg_Compare.ExecDlg(FProjectID, ed_FileName.FileName) then
    Exit;

  dmMain.ProjectID:=FProjectID;



  with dmTask_MDB_to_DB.Params do
  begin
    RplsProjectFileName:= ed_FileName.FileName;
    ProjectID     := FProjectID;
  //  LinkTypeID    := FLinkTypeID;
  //  LinkEndTypeID := FLinkEndTypeID;
    ClutterModelID:= FClutterModelID;
    CalcModelID   := FCalcModelID;

    IsAddCalc     := AsBoolean(row__AddCalc.Properties.Value);

 //   Use_Cache_MDB:=row_Use_Cache_MDB.Value;

/////    NetDir        := dmProject.GetNetDir (FProjectID);//  row_Project_Dir.Text;


  end;

  dmTask_MDB_to_DB.ExecuteDlg('����������� ������� �� ����� XML � ���� ������');


*)
end;

//--------------------------------------------------------------------
procedure Tfrm_Main.DoOnButClick(Sender: TObject; AbsoluteIndex: Integer);
//--------------------------------------------------------------------
var
  sName: string;
begin
(*
  //------------------
  if cxVerticalGrid1.FocusedRow=row__Clu then
  begin
    if IShell.Dlg_Select_Object (otClutterModel, FClutterModelID, sName) then
    begin
  //    row__Clu.Properties.Value:=FtmpName;
//      row__Clu.
      row__Clu.Properties.Value:=sName;
   ////////   (Sender as TcxButtonEdit).PostEditValue;

    //  (Sender as TcxButtonEdit).EditValue:=FtmpName;
    end;
  end else

  //------------------
  if cxVerticalGrid1.FocusedRow=row__CalcModel then
  begin
    if IShell.Dlg_Select_Object (otCalcModel, FCalcModelID, sName) then
    begin
      row__CalcModel.Properties.Value:=sName;
   //   (Sender as TcxButtonEdit).EditValue:=FtmpName;

    end;
  end;
*)

 // cxVerticalGrid1.HideEdit;

end;


//-------------------------------------------------------------------
procedure Tfrm_Main._Action(Sender: TObject);
//-------------------------------------------------------------------
var
  sName: string;
begin
  inherited;

  //------------------
  if Sender=act_ConvProject then
  begin
    Convert2();

    Run_ImportFromMDB();

  //  FChanged := True;
  end else


 (* //------------------
  if Sender=act_ConvDicts then
  begin
    if not FileExists(ed_FileDicts.Text) then
      ErrorDlg('���� '+ed_FileDicts.Text+' �� ������')
    else
    begin
      dmTask_Dicts_to_DB.Params.DictionaryFileName:= ed_FileDicts.Text;
      dmTask_Dicts_to_DB.ExecuteDlg ('����������� �������� � ���� ������');

      FChanged := True;
    end;
  end; // else
*)

  if Sender=act_Project_Clear then
    if ConfirmDlg('�������� ������ ?') then
    begin
      CursorHourGlass;

      dmOnega_DB_data.Project_Clear_(FProjectID);


//      dmProject.Clear (FProjectID);

      CursorDefault;
      MsgDlg ('������ ������.');

      FChanged := True;
    end;

end;



procedure Tfrm_Main.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
begin
  inherited;

/////////////  act_ConvProject.Enabled:=(FProjectID>0) and (FClutterModelID>0);

//  act_Project_Clear.Enabled:= (FProjectID>0);

end;


procedure Tfrm_Main.Button4Click(Sender: TObject);
begin
  Convert2;

end;

// ---------------------------------------------------------------
procedure Tfrm_Main.Convert2;
// ---------------------------------------------------------------

//const
//  DEF_FILE_XML = 'W:\RPLS_DB EXPORT-IMPORT\XML_to_RPLS_DB\Data\xml\��������������.xml';

//var
//  oTask_XML_to_Classes: TTask_XML_to_Classes;
//  s: string;

begin
//  oTask_XML_to_Classes:=TTask_XML_to_Classes.Create;
//  oTask_XML_to_Classes.Params.RplsProject_XmlFileName :=ed_FileName.FileName;
//
///////  oTask_XML_to_Classes.Params.RplsProject_FileName := DEF_FILE_XML;
//
////  oTask_XML_to_Classes.ExecuteProc;
//  oTask_XML_to_Classes.ExecuteDlg;
//
//  // ---------------------------------------------------------------
//
//  TdmTask_XML_Classes_To_MDB.Init;
//
//  dmTask_XML_Classes_To_MDB.XProject_ref := oTask_XML_to_Classes.XProject;
////  dmTask_XML_Classes_To_MDB.Params.Project_ID :=1;
//
//
////  dmTask_XML_Classes_To_MDB.Params.Template_MdbFileName :=
////    GetApplicationDir + 'XML_to_RPLS_DB\xml_data_template.mdb';
//
//  dmTask_XML_Classes_To_MDB.Params.Template_MdbFileName :=
//    GetApplicationDir + 'RPLS_xml_to_DB.template.mdb';
//
//
// //  ShowMessage('Template_MdbFileName: '+   dmTask_XML_Classes_To_MDB.Params.Template_MdbFileName);
//
//  FMdbFileName:=ChangeFileExt(ed_FileName.FileName, '.mdb');
//
//  dmTask_XML_Classes_To_MDB.Params.MdbFileName := FMdbFileName;
//
////     ChangeFileExt(ed_FileName.FileName, '.mdb');
//
//
////  dmTask_XML_Classes_To_MDB.Execute;
//  dmTask_XML_Classes_To_MDB.ExecuteDlg;
//
//  FreeAndNil(dmTask_XML_Classes_To_MDB);
//
//  // ---------------------------------------------------------------
//
////  dmTask_XML_Classes_To_MDB.XProject := dmTask_XML_to_MDB.XProject;
// // dmTask_XML_Classes_To_MDB.SaveToMDB;
//
//
//  FreeAndNil(oTask_XML_to_Classes);
end;


procedure Tfrm_Main.OpenProjects;
begin
  qry_Projects.Connection :=dmMain.ADOConnection;
  db_OpenQuery(qry_Projects, 'SELECT id,name FROM '+TBL_project+' ORDER BY name');

end;


procedure Tfrm_Main.Panel1Click(Sender: TObject);
begin
  inherited;
end;


// -------------------------------------------------------------------
procedure Tfrm_Main.StatusBar1DblClick(Sender: TObject);
begin
 // dmMain.OpenDlg(True);

end;

procedure Tfrm_Main.row_Use_Cache_MDBEditPropertiesChange(
  Sender: TObject);
var
  b: boolean;
begin
 

end;


//-------------------------------------------------------------------
procedure Tfrm_Main.Run_ImportFromMDB;
//-------------------------------------------------------------------
const
  TEMP_FILENAME = 'project_export.ini';

 // PROG_data_export_import = 'data_export_import.exe';

var
  iProjectID: Integer;
  sFile : string;

  oParams: TIni_Data_export_import_params;
  
begin
  sFile := g_ApplicationDataDir + TEMP_FILENAME;


  iProjectID := AsInteger(DBLookupComboBox_Projects.KeyValue);


//  sFile := g_ApplicationDataDir + TEMP_FILENAME;

  oParams:=TIni_Data_export_import_params.Create;
  oParams.ProjectID := iProjectID;
  oParams.Mode     := mtImport_project;
  oParams.IsAuto   := True;

  oParams.MdbFileName := FMdbFileName;


//      FMdbFileName

  oParams.SaveToIni(sFile);

  FreeAndNil(oParams);

  // -------------------------

  ShellExec (GetApplicationDir()+ PROG_data_export_import_exe, DoubleQuotedStr(sFile));

end;




procedure Tfrm_Main.Button1Click(Sender: TObject);
begin
  Run_ImportFromMDB;
end;


end.

