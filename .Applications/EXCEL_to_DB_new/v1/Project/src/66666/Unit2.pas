unit Unit2;

interface

uses
  Forms, Classes, DB, XMLDoc, XMLIntf , SysUtils


  ;

type
  //---------------------------------------------------------
  TProperty = class(TCollectionItem)
  //---------------------------------------------------------
  public
    Name: string;

    Lat: Double;
    Lon: double;

    Lat_WGS84: Double;
    Lon_WGS84: double;

    Comments: string;   
  end;


  //-------------------------------------------------------------------
  TPropertyList = class(TCollection)
  //-------------------------------------------------------------------
  private
    function GetItems(Index: Integer): TProperty;
  public
    constructor Create;
    function AddItem: TProperty;
    property Items[Index: Integer]: TProperty read GetItems; default;

  end;


implementation



constructor TPropertyList.Create;
begin
  inherited Create(TProperty);
end;

function TPropertyList.AddItem: TProperty;
begin
  Result := TProperty (inherited Add);
end;

function TPropertyList.GetItems(Index: Integer): TProperty;
begin
  Result := TProperty(inherited Items[Index]);
end;



end.


