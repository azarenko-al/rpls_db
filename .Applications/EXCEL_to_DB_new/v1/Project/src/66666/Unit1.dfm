object Form1: TForm1
  Left = 381
  Top = 184
  Width = 809
  Height = 515
  Caption = 'Excel to DB'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 8
    Width = 801
    Height = 369
    ActivePage = TabSheet1_Excel_Data
    Align = alTop
    TabOrder = 0
    object TabSheet_File: TTabSheet
      Caption = 'TabSheet_File'
      ImageIndex = 3
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 793
        Height = 113
        Align = alTop
        Caption = #1044#1086#1082#1091#1084#1077#1085#1090' Excel'
        TabOrder = 0
        DesignSize = (
          793
          113)
        object Label2: TLabel
          Left = 5
          Top = 24
          Width = 57
          Height = 13
          Caption = #1048#1084#1103' '#1092#1072#1081#1083#1072
        end
        object Label3: TLabel
          Left = 5
          Top = 68
          Width = 100
          Height = 13
          Caption = #1057#1080#1089#1090#1077#1084#1072' '#1082#1086#1086#1088#1076#1080#1085#1072#1090
        end
        object ed_FileName: TFilenameEdit
          Left = 5
          Top = 39
          Width = 705
          Height = 21
          OnAfterDialog = ed_FileNameAfterDialog
          Anchors = [akLeft, akTop, akRight]
          NumGlyphs = 1
          TabOrder = 0
        end
        object cb_Coord: TComboBox
          Left = 5
          Top = 81
          Width = 193
          Height = 21
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 1
          Text = #1055#1091#1083#1082#1086#1074#1086' CK-42'
          Items.Strings = (
            #1055#1091#1083#1082#1086#1074#1086' CK-42'
            'WGS 84')
        end
      end
      object Button2: TButton
        Left = 6
        Top = 280
        Width = 75
        Height = 25
        Action = act_Open
        TabOrder = 1
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 120
        Width = 217
        Height = 145
        Caption = #1051#1080#1089#1090
        TabOrder = 2
        object ListBox1: TListBox
          Left = 2
          Top = 15
          Width = 213
          Height = 128
          Align = alClient
          ItemHeight = 13
          TabOrder = 0
        end
      end
    end
    object TabSheet1_Excel_Data: TTabSheet
      Caption = 'Excel_Data'
      object StringGrid1: TStringGrid
        Left = 0
        Top = 62
        Width = 793
        Height = 123
        Align = alTop
        DefaultColWidth = 150
        DefaultRowHeight = 18
        FixedCols = 0
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine]
        TabOrder = 0
      end
      object ToolBar1: TToolBar
        Left = 0
        Top = 29
        Width = 793
        Height = 33
        BorderWidth = 1
        Caption = 'ToolBar1'
        TabOrder = 1
        object Button3: TButton
          Left = 0
          Top = 2
          Width = 153
          Height = 22
          Action = act_LoadDataFromExcel
          TabOrder = 0
        end
      end
      object ToolBar4: TToolBar
        Left = 0
        Top = 0
        Width = 793
        Height = 29
        ButtonHeight = 23
        Caption = 'ToolBar2'
        TabOrder = 2
        object Label1: TLabel
          Left = 0
          Top = 2
          Width = 196
          Height = 23
          Caption = #1053#1086#1084#1077#1088' '#1089#1090#1088#1086#1082#1080' '#1089' '#1085#1072#1079#1074#1072#1085#1080#1103#1084#1080' '#1089#1090#1086#1083#1073#1094#1086#1074
        end
        object ToolButton1: TToolButton
          Left = 196
          Top = 2
          Width = 10
          Caption = 'ToolButton1'
          Style = tbsSeparator
        end
        object DBEdit1: TDBEdit
          Left = 206
          Top = 2
          Width = 73
          Height = 23
          DataField = 'first_row_index'
          DataSource = dmExcel_to_DB.ds_File
          TabOrder = 0
        end
      end
    end
    object TabSheet_Columns: TTabSheet
      Caption = 'Columns'
      ImageIndex = 1
      object DBGrid_Columns: TDBGrid
        Left = 0
        Top = 33
        Width = 793
        Height = 168
        Align = alTop
        DataSource = dmExcel_to_DB.ds_File_columns
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'column_index'
            Title.Caption = #1048#1085#1076#1077#1082#1089
            Width = 95
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'column_header'
            Title.Caption = #1047#1072#1075#1086#1083#1086#1074#1086#1082
            Width = 152
            Visible = True
          end
          item
            DropDownRows = 20
            Expanded = False
            FieldName = 'field_caption'
            Title.Caption = #1055#1086#1083#1077' '#1074' '#1041#1044
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'field2_caption'
            Title.Caption = #1055#1086#1083#1077' '#1074' '#1041#1044' (2)'
            Width = 147
            Visible = True
          end
          item
            DropDownRows = 20
            Expanded = False
            FieldName = 'field_name'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'field2_name'
            Visible = False
          end>
      end
      object ToolBar3: TToolBar
        Left = 0
        Top = 0
        Width = 793
        Height = 33
        BorderWidth = 1
        Caption = 'ToolBar1'
        TabOrder = 1
        object Button5: TButton
          Left = 0
          Top = 2
          Width = 153
          Height = 22
          Action = act_LoadColumns
          TabOrder = 0
        end
      end
    end
    object TabSheet_MDB: TTabSheet
      Caption = 'MDB'
      ImageIndex = 2
      object DBGrid1: TDBGrid
        Left = 0
        Top = 29
        Width = 793
        Height = 76
        Align = alTop
        DataSource = dmExcel_to_DB.ds_Property
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Visible = False
      end
      object ToolBar2: TToolBar
        Left = 0
        Top = 0
        Width = 793
        Height = 29
        Caption = 'ToolBar2'
        TabOrder = 1
        object Button1: TButton
          Left = 0
          Top = 2
          Width = 153
          Height = 22
          Action = act_SaveDataToDB
          TabOrder = 0
        end
        object ToolButton2: TToolButton
          Left = 153
          Top = 2
          Width = 8
          Caption = 'ToolButton2'
          Style = tbsSeparator
        end
        object Button6: TButton
          Left = 161
          Top = 2
          Width = 152
          Height = 22
          Action = act_SaveToExportMDB
          TabOrder = 1
        end
      end
      object cxGrid1: TcxGrid
        Left = 0
        Top = 233
        Width = 793
        Height = 108
        Align = alBottom
        TabOrder = 2
        LookAndFeel.Kind = lfFlat
        object cxGrid1DBTableView1: TcxGridDBTableView
          DataController.DataSource = dmExcel_to_DB.ds_Property
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          OptionsView.GroupByBox = False
          object cxGrid1DBTableView1id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
          end
          object cxGrid1DBTableView1name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 140
          end
          object cxGrid1DBTableView1address: TcxGridDBColumn
            DataBinding.FieldName = 'address'
            Width = 328
          end
          object cxGrid1DBTableView1lat: TcxGridDBColumn
            DataBinding.FieldName = 'lat'
            Width = 78
          end
          object cxGrid1DBTableView1lon: TcxGridDBColumn
            DataBinding.FieldName = 'lon'
            Width = 73
          end
          object cxGrid1DBTableView1lat_str: TcxGridDBColumn
            DataBinding.FieldName = 'lat_str'
            Width = 114
          end
          object cxGrid1DBTableView1lon_str: TcxGridDBColumn
            DataBinding.FieldName = 'lon_str'
            Width = 127
          end
          object cxGrid1DBTableView1ground_height: TcxGridDBColumn
            DataBinding.FieldName = 'ground_height'
          end
          object cxGrid1DBTableView1tower_height: TcxGridDBColumn
            DataBinding.FieldName = 'tower_height'
          end
          object cxGrid1DBTableView1town: TcxGridDBColumn
            DataBinding.FieldName = 'town'
            Width = 46
          end
          object cxGrid1DBTableView1property_status: TcxGridDBColumn
            DataBinding.FieldName = 'property_status'
          end
          object cxGrid1DBTableView1comments: TcxGridDBColumn
            DataBinding.FieldName = 'comments'
            Width = 71
          end
          object cxGrid1DBTableView1status: TcxGridDBColumn
            DataBinding.FieldName = 'status'
          end
          object cxGrid1DBTableView1code: TcxGridDBColumn
            DataBinding.FieldName = 'code'
            Width = 175
          end
          object cxGrid1DBTableView1region: TcxGridDBColumn
            DataBinding.FieldName = 'region'
            Width = 52
          end
          object cxGrid1DBTableView1city: TcxGridDBColumn
            DataBinding.FieldName = 'city'
            Width = 39
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 801
    Height = 8
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
  end
  object ActionList1: TActionList
    Left = 184
    Top = 408
    object act_LoadDataFromExcel: TAction
      Caption = 'LoadDataFromExcel'
      OnExecute = act_LoadDataFromExcelExecute
    end
    object act_LoadColumns: TAction
      Caption = 'LoadColumns'
      OnExecute = act_LoadDataFromExcelExecute
    end
    object act_SaveDataToDB: TAction
      Caption = 'act_SaveDataToDB'
      OnExecute = act_LoadDataFromExcelExecute
    end
    object act_SaveToExportMDB: TAction
      Caption = 'act_SaveToExportMDB'
      OnExecute = act_LoadDataFromExcelExecute
    end
    object act_Open: TAction
      Caption = 'act_Open'
      OnExecute = act_LoadDataFromExcelExecute
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\Excel_to_DB'
    Options = [fpPosition]
    StoredProps.Strings = (
      'SaveDialog1.FileName'
      'SaveDialog1.InitialDir'
      'ed_FileName.FileName')
    StoredValues = <>
    Left = 80
    Top = 404
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '.mdb'
    FileName = 'property'
    Filter = 'MDB (.mdb)|*.mdb'
    Left = 280
    Top = 408
  end
end
