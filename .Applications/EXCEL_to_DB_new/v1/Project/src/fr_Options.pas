unit fr_Options;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxVGrid, cxControls, cxInplaceContainer,

  
  cxDropDownEdit,

  u_func,

  u_Config_File,


   cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinsDefaultPainters, cxStyles, cxEdit, cxSpinEdit,
  cxRadioGroup, cxCheckBox, Grids, DBGrids, ComCtrls

  ;

type
  Tframe_Options = class(TForm)
    PopupMenu1: TPopupMenu;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    cxVerticalGrid1: TcxVerticalGrid;
    row_Sheets: TcxEditorRow;
    cxVerticalGrid1CategoryRow4: TcxCategoryRow;
    row_Header_row_index: TcxEditorRow;
    row_DataType: TcxEditorRow;
    row_CoordSys1: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    cxVerticalGrid1CategoryRow3: TcxCategoryRow;
    row_Gain_dB2: TcxEditorRow;
    row_Diameter2: TcxEditorRow;
    row_Height2: TcxEditorRow;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;
    row_Power_dBm2: TcxEditorRow;
    row_Bitrate_Mbps2: TcxEditorRow;
    row_Threshold_BER_32: TcxEditorRow;
    row_Threshold_BER_62: TcxEditorRow;
    row_Freq_MHz: TcxEditorRow;
    row_Get_antenna_H_from_site: TcxEditorRow;
    TabSheet3: TTabSheet;
    DBGrid2: TDBGrid;
    row_Data_row_index: TcxEditorRow;
    cxVerticalGrid1CategoryRow5: TcxCategoryRow;
    procedure FormCreate(Sender: TObject);
  private
    function Validated: Boolean;
    { Private declarations }
  public
    function GetSheetList: TStrings;
    procedure Init;
    procedure LoadFromClass;
    procedure Restore_Defaults;
    procedure SaveToClass;

  end;

//var
//  frame_Options: Tframe_Options;

implementation

uses dm_Config_mdb;

{$R *.dfm}

procedure Tframe_Options.FormCreate(Sender: TObject);
begin

  PageControl1.Align:=alClient;
  PageControl1.ActivePageIndex:=0;// Align:=alClient;

  cxVerticalGrid1.Align:=alClient;
  
  row_DataType.Properties.Caption:='��� ������';
  row_Sheets.Properties.Caption:='����';
  row_Freq_MHz.Properties.Caption:='������� ��������, MHz';


  row_Get_antenna_H_from_site.Properties.Caption:='������ ������� �� ���� ������';


  row_Get_antenna_H_from_site.Visible := False;

end;

// ---------------------------------------------------------------
function Tframe_Options.GetSheetList: TStrings;
// ---------------------------------------------------------------
var
  oComboBoxProperties: TcxComboBoxProperties;
begin
  oComboBoxProperties:=TcxComboBoxProperties(row_Sheets.Properties.EditProperties);

// .. dmExcel_file.GetSheets (ed_FileName.FileName, ListBox1.Items);
  Result :=  oComboBoxProperties.Items;
end;

// ---------------------------------------------------------------
procedure Tframe_Options.Init;
// ---------------------------------------------------------------
begin


end;

// ---------------------------------------------------------------
procedure Tframe_Options.LoadFromClass;
// ---------------------------------------------------------------
var
  iValue: Integer;
  s: string;
begin
  cxVerticalGrid1.HideEdit;



 // g_ConfigFile.

  with g_ConfigFile do
  begin
    row_Sheets.Properties.Value := SheetName;

    row_Header_row_index.Properties.Value:=HeaderRowIndex_1;
    row_Data_row_index.Properties.Value  :=DataRowIndex_1;
                                         
    row_Height2.Properties.Value          := Height;
    row_Gain_dB2.Properties.Value         := Gain;
    row_Diameter2.Properties.Value        := Diameter;

    row_Freq_MHz.Properties.Value         := Freq_MHz;
    row_Power_dBm2.Properties.Value       := Power_dBm;
    row_Threshold_BER_32.Properties.Value := Threshold_BER_3;
    row_Threshold_BER_62.Properties.Value := Threshold_BER_6;
    row_Bitrate_Mbps2.Properties.Value    := Bitrate_Mbps;



    case DataSourceType of
      dtProp: iValue:=0;
      dtLink: iValue:=1;
    end;


    row_DataType.Properties.Value :=iValue;


    row_Get_antenna_H_from_site.Properties.Value := IsGet_antenna_H_from_site;


    case CoordSys of
      csPulkovo_CK42: s:='CK42';
      csPulkovo_CK95: s:='CK95';
      csWGS:          s:='WGS';
    end;

    row_CoordSys1.Properties.Value:=s;

//    if Eq(s, 'CK42') then CoordSys := csPulkovo_CK42 else
//    if Eq(s, 'CK95') then CoordSys := csPulkovo_CK95 else
//    if Eq(s, 'wgs')  then CoordSys := csWGS;



(*
    case row_DataType.Properties.Value of
      0: DataType:=dtProp;
      1: DataType:=dtLink;
    end;

    case row_CoordSys.Properties.Value of
      0: CoordSys:=csPulkovo;
      1: CoordSys:=csWGS;
    end;

*)



  end;



end;

 // ---------------------------------------------------------------
procedure Tframe_Options.Restore_Defaults;
// ---------------------------------------------------------------
begin
  g_ConfigFile.SetDefaults();
  LoadFromClass();
end;

// ---------------------------------------------------------------
procedure Tframe_Options.SaveToClass;
// ---------------------------------------------------------------
var
  s: string;
begin

 // g_ConfigFile.

  with g_ConfigFile do
  begin
    SheetName       := row_Sheets.Properties.Value;

    
    HeaderRowIndex_1:=  (row_Header_row_index.Properties.Value);
    DataRowIndex_1  :=  (row_Data_row_index.Properties.Value);
                    
   Assert(HeaderRowIndex_1>0, 'Tframe_Options.SaveToClass;  HeaderRowIndex_1>0');
   Assert(DataRowIndex_1>0,   'Tframe_Options.SaveToClass;  DataRowIndex_1>0');

                    
    Height         :=  AsInteger(row_Height2.Properties.Value);
    Gain           :=  AsFloat(row_Gain_dB2.Properties.Value);
    Diameter       :=  AsFloat(row_Diameter2.Properties.Value);


    Freq_MHz        :=  AsFloat(row_Freq_MHz.Properties.Value);
    Power_dBm       :=  AsFloat(row_Power_dBm2.Properties.Value);
    Threshold_BER_3 :=  AsFloat(row_Threshold_BER_32.Properties.Value);
    Threshold_BER_6 :=  AsFloat(row_Threshold_BER_62.Properties.Value);
    Bitrate_Mbps    :=  AsInteger(row_Bitrate_Mbps2.Properties.Value);


    case row_DataType.Properties.Value of
      0: DataSourceType:=dtProp;
      1: DataSourceType:=dtLink;
    end;

    s:=row_CoordSys1.Properties.Value;

    if Eq(s, 'CK42') then CoordSys := csPulkovo_CK42 else
    if Eq(s, 'CK95') then CoordSys := csPulkovo_CK95 else
    if Eq(s, 'WGS')  then CoordSys := csWGS;

//
//    case row_CoordSys.Properties.Value of
//      0: CoordSys:=csPulkovo_CK42;
//      0: CoordSys:=csPulkovo_CK42;
//      1: CoordSys:=csWGS;
//    end;



    IsGet_antenna_H_from_site := row_Get_antenna_H_from_site.Properties.Value;

  end;



end;

// ---------------------------------------------------------------
function Tframe_Options.Validated: Boolean;
// ---------------------------------------------------------------
begin
  Result := AsString( row_Sheets.Properties.Value )<>'';

end;

end.
