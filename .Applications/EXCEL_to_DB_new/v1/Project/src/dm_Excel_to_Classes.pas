unit dm_Excel_to_Classes;

interface

{$DEFINE test}


uses
  SysUtils, Classes, DB, ADODB,  Variants,  Forms, Dialogs,
  StrUtils, Math, cxCustomData,

  u_excel_const,

  dm_Excel_file,

  u_Config_File,

  u_func,

  u_files,

  u_import_classes,

  u_geo_convert_new,

 // dm_MDB,

  u_db_mdb,

  u_geo,

  u_db,

  //dm_Excel

    ComCtrls, cxGridCustomTableView, cxGridTableView, RxMemDS, dxmdaset,
    cxGridBandedTableView;

type
  TdmExcel_to_Classes = class(TDataModule)
    RxMemoryData_Columns: TRxMemoryData;
    ds_Columns: TDataSource;
    procedure DataModuleDestroy(Sender: TObject);
  //  procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  //  procedure qry_File_columnsNewRecord(DataSet: TDataSet);
  private
    FDataSet_Columns: TDataSet;
    procedure LoadColumns;
//    procedure aaaaaa;

  public
    Project: TExcelProject;

//    PropertyList: TRawPropertyList;
//    PropertyList: TRawPropertyList;



    class procedure Init;


    procedure LoadDataFromExcelFile(aFileName: string; acxGridTableView:  TcxGridTableView);

//    procedure LoadDataFromExcelFile1(aFileName: string; acxGridView:
//        TcxGridBandedTableView);

    procedure SaveColumns;

    procedure ExportToClasses_LInk;
    procedure ExportToClasses_Property;
  end;

var
  dmExcel_to_Classes: TdmExcel_to_Classes;


implementation


{$R *.dfm}



const
  FLD_first_row_index = 'first_row_index';

  FLD_field2_name   = 'field2_name';
  FLD_column_index  = 'column_index';
  FLD_FILE_ID       = 'FILE_ID';
  FLD_column_header = 'column_header';
  FLD_IS_USED       = 'IS_USED';

  TBL_file          = 'file';
  TBL_file_columns  = 'file_columns';
  TBL__Property     = '_Property';


  FLD_SITE1_LinkEnd_name ='SITE1_LinkEnd_name';
  FLD_SITE2_LinkEnd_name ='SITE2_LinkEnd_name';

  
  FLD_SITE1_LAT = 'SITE1_LAT';
  FLD_SITE1_LON = 'SITE1_LON';

  FLD_SITE2_LAT = 'SITE2_LAT';
  FLD_SITE2_LON = 'SITE2_LON';


  FLD_lat_lon = 'lat_lon';
  FLD_lon_lat = 'lon_lat';

  FLD_LAT_DEG = 'LAT_DEG';
  FLD_LAT_MIN = 'LAT_MIN';
  FLD_LAT_SEC = 'LAT_SEC';

  FLD_LON_DEG = 'LON_DEG';
  FLD_LON_MIN = 'LON_MIN';
  FLD_LON_SEC = 'LON_SEC';


  FLD_SITE1_diameter = 'SITE1_diameter';
  FLD_SITE2_diameter = 'SITE2_diameter';

  FLD_tx_mhz = 'tx_mhz';
  FLD_rx_mhz = 'rx_mhz';

//  FLD_SITE2_tx_mhz = 'SITE2_tx_mhz';
//  FLD_SITE2_rx_mhz = 'SITE2_rx_mhz';

  FLD_power = 'power';
 // FLD_SITE2_power = 'SITE2_power';


  FLD_LinkEndType_name = 'LinkEndType_name';

  

    procedure geo_Encode2DegreeFromString(aValue: string; var aDegree1, aDegree2:
        Double); forward;

    function geo_EncodeDegreeFromString(aValue: string): Double;  forward;




class procedure TdmExcel_to_Classes.Init;
begin
  if not Assigned(dmExcel_to_Classes) then
    Application.CreateForm(TdmExcel_to_Classes, dmExcel_to_Classes);
end;


// ---------------------------------------------------------------
procedure TdmExcel_to_Classes.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  Project:=TExcelProject.Create;

//  PropertyList:=TPropertyList.Create;


  FDataSet_Columns := RxMemoryData_Columns;
            
  db_CreateFieldArr(RxMemoryData_Columns,
    [
      db_Field(FLD_INDEX,  ftInteger),
      db_Field(FLD_HEADER, ftString),
      db_Field(FLD_FIELD_NAME, ftString),
      db_Field(FLD_KEY,    ftBoolean)
    ]  );

  RxMemoryData_Columns.Open;


end;


procedure TdmExcel_to_Classes.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(Project);
//  :=TExcelProject.Create;


//  FreeAndNil(FUsedColumnList);
//  FreeAndNil(PropertyList);
end;



// ---------------------------------------------------------------
procedure TdmExcel_to_Classes.LoadDataFromExcelFile(aFileName: string;
    acxGridTableView: TcxGridTableView);
// ---------------------------------------------------------------
var
  iCol: Integer;
  iRow: Integer;
  k: Integer;
  oColumn: TcxGridColumn;
 // rColumn: TColumn;
  s: string;
  sHeader: string;
begin
  Assert(g_ConfigFile.HeaderRowIndex_1>0, 'HeaderRowIndex_1>0');
  Assert(g_ConfigFile.DataRowIndex_1>0,   'DataRowIndex_1>0');

//  Assert(FFirstLine>0, 'Value <=0');

  dmExcel_file.Open(aFileName);
  dmExcel_file.LoadWorksheetByName(g_ConfigFile.sheetName);
  dmExcel_file.Close;

(*
  FDataSet_Columns.Close;
  FDataSet_Columns.Open;

  Assert (FDataSet_Columns.RecordCount=0);
*)
  LoadColumns();



//  k:=acxGridTableView.DataController.RecordCount;

  acxGridTableView.ClearItems;




  while acxGridTableView.DataController.RecordCount>0 do
    acxGridTableView.DataController.DeleteRecord(0);



  for iCol := 1 to dmExcel_file.ColCount  do
  begin
    sHeader:=VarToStr(dmExcel_file.Cells_1[g_ConfigFile.HeaderRowIndex_1, iCol]);
                                           
    oColumn:= acxGridTableView.CreateColumn;
    oColumn.Caption := format('%d. %s', [iCol, sHeader]);

  end;


  acxGridTableView.DataController.BeginUpdate;


//    for iRow := Params.FirstHeaderLine+1 to dmExcel_file.RowCount  do
  for iRow := g_ConfigFile.DataRowIndex_1  to dmExcel_file.RowCount  do
  begin
    k:=acxGridTableView.DataController.AppendRecord;

    for iCol := 1 to dmExcel_file.ColCount  do
    begin
      s:=VarToStr(dmExcel_file.Cells_1[iRow,iCol]);

    //////////////////
    acxGridTableView.DataController.Values[k, iCol-1]:=s;
    end;

    acxGridTableView.DataController.Post;
  end;


 // end;

  acxGridTableView.DataController.EndUpdate;
         
  acxGridTableView.ApplyBestFit();


end;


// ---------------------------------------------------------------
procedure TdmExcel_to_Classes.SaveColumns;
// ---------------------------------------------------------------
var
  i: Integer;
  rColumn: TColumn;
  sIniIniFileName: string;

  FUsedColumnList: TStringList;

begin
 // db_View(FDataSet_Columns);


  FUsedColumnList := TStringList.Create();

  FDataSet_Columns.First;

  FUsedColumnList.Clear;

  SetLength(g_ConfigFile.Columns, FDataSet_Columns.RecordCount);

  with FDataSet_Columns do
    while not EOF do
    begin
      i:=RecNo;

      rColumn.FieldName  := FieldByName(FLD_FIELD_NAME).AsString;
      rColumn.IsKey      := FieldByName(FLD_KEY).AsBoolean;

      if rColumn.FieldName<>'' then
        FUsedColumnList.Add(rColumn.FieldName);


      g_ConfigFile.Columns[i-1] := rColumn;

      Next;
    end;    // while

  sIniIniFileName:=g_ConfigFile.GetIniFileName(g_ConfigFile.FileName);

  g_ConfigFile.SaveToIniFile(sIniIniFileName);

  {$IFDEF test}
//  ShellExec_Notepad(sIniIniFileName);

  {$ENDIF}


  FreeAndNil(FUsedColumnList);

end;



// ---------------------------------------------------------------
procedure TdmExcel_to_Classes.ExportToClasses_Property;
// ---------------------------------------------------------------
var
  coords: record
           ang_lat,  ang_lon: TAngle;

           Lat : Double;
           Lon : Double;
         end;
var
  I: Integer;

  oProperty: TPropertyRaw;


  iCol: Integer;
  iRow: Integer;
 // s: string;
  sField: string;
  v: Variant;

  rColumn: TColumn;
begin


  SaveColumns();


  Assert(g_ConfigFile.HeaderRowIndex_1>0, 'Value <=0');
  Assert(g_ConfigFile.DataRowIndex_1>0, 'Value <=0');


  for iRow := g_ConfigFile.DataRowIndex_1 to dmExcel_file.RowCount  do
  begin
    oProperty:= Project.PropertyList.AddItem;


    FillChar(coords, SizeOf(coords), 0);


    for iCol := 0 to High(g_ConfigFile.Columns) do
    begin
      rColumn := g_ConfigFile.Columns[iCol];

      if not ((rColumn.FieldName<>'') or rColumn.IsKey) then
        Continue;


      v:=dmExcel_file.Cells_1[iRow,iCol+1];


      if rColumn.IsKey then
        oProperty.Name := v;


//      if rColumn.IsKey then
 //       oProperty.Name := v
   //   else

     // if arrColumns[iCol].FieldName<>'' then
     // begin
        sField:=rColumn.FieldName;

      //  v:=dmExcel_file.Cells_1[iRow,iCol+1];


        if Eq(sField, FLD_NAME) then
        begin
          oProperty.Name := v;

          if oProperty.Name= '' then
            Continue;

        end else



        if Eq(sField, FLD_ADDRESS) then oProperty.Address := v  else
        if Eq(sField, FLD_TOWER_HEIGHT) then oProperty.TOWER_HEIGHT := v  else

     // -------------------------
        if Eq(sField, FLD_LAT_DEG)  then  coords.ang_lat.Deg:=AsInteger(v) else
        if Eq(sField, FLD_LAT_MIN)  then  coords.ang_lat.Min:=AsInteger(v) else
        if Eq(sField, FLD_LAT_SEC)  then  coords.ang_lat.Sec:=AsFloat(v)   else

        // -------------------------
        if Eq(sField, FLD_LON_DEG)  then  coords.ang_lon.Deg:=AsInteger(v) else
        if Eq(sField, FLD_LON_MIN)  then  coords.ang_lon.Min:=AsInteger(v) else
        if Eq(sField, FLD_LON_SEC)  then  coords.ang_lon.Sec:=AsFloat(v)   else

        // -------------------------
        if Eq(sField, FLD_LAT)  then coords.Lat:=geo_EncodeDegreeFromString (v) else
        if Eq(sField, FLD_LON)  then coords.Lon:=geo_EncodeDegreeFromString (v)

        else;

(*
            // -------------------------
        if Eq(aField_name, FLD_TOWER_HEIGHT) or
          (Eq(aField_name, FLD_GROUND_HEIGHT))
        then
*)
    //  end;
    end;


    if coords.ang_lat.Deg>0 then
      coords.Lat := geo_EncodeDegreeRec(coords.ang_lat);

    if coords.ang_lon.Deg>0 then
      coords.Lon := geo_EncodeDegreeRec(coords.ang_lon);


//   Assert(coords.Lat>0, 'Value <=0');
//   Assert(coords.Lon>0, 'Value <=0');

   if (coords.Lat>0) and (coords.Lon>0)
   then
   begin

      oProperty.BLPoint.B := coords.Lat;
      oProperty.BLPoint.L := coords.Lon;

      case g_ConfigFile.CoordSys of
        csPulkovo_CK42: ;
        csPulkovo_CK95: oProperty.BLPoint:= geo_Pulkovo_CK_95_to_CK_42(oProperty.BLPoint);
        csWGS:          oProperty.BLPoint:= geo_WGS84_to_Pulkovo42(oProperty.BLPoint);
//        csGOST:          oProperty.BLPoint:= geo_WGS84_to_ Pulkovo42(oProperty.BLPoint);
      else
        raise exception.create('')
      end;

   end;



//    if g_ConfigFile.CoordSys=csWGS then
  //    oProperty.BLPoint:= geo_WGS84_to_Pulkovo42(oProperty.BLPoint);



  end;

end;


// ---------------------------------------------------------------
procedure TdmExcel_to_Classes.ExportToClasses_LInk;
// ---------------------------------------------------------------

var
  I: Integer;

  oLink: TLinkRaw;


  iCol: Integer;
  iRow: Integer;
 // s: string;
  sField: string;
  v: Variant;

  rColumn: TColumn;

const
  FLD_Modulation = 'Modulation';

  FLD_equipment = 'equipment';
  FLD_E1_number = 'E1_number';
  FLD_band      = 'band';

FLD_channel_width_MHz = 'channel_width_MHz';

  //      if Eq(sField, FLD_band)       then oLink.band := v else


begin


  SaveColumns();


  Assert(g_ConfigFile.DataRowIndex_1>0, 'DataRowIndex_1>0');


  for iRow := g_ConfigFile.DataRowIndex_1  to dmExcel_file.RowCount  do
  begin
    oLink:= Project.LinkList.AddItem;

//    FillChar(coords, SizeOf(coords), 0);


    for iCol := 0 to High(g_ConfigFile.Columns) do
    begin
      rColumn := g_ConfigFile.Columns[iCol];

      if not ((rColumn.FieldName<>'') or rColumn.IsKey) then
        Continue;


      v:=dmExcel_file.Cells_1[iRow,iCol+1];


//      if rColumn.IsKey then
//        oLink.Name := v;


     // if arrColumns[iCol].FieldName<>'' then

        sField:=rColumn.FieldName;

      //  v:=dmExcel_file.Cells_1[iRow,iCol+1];

      try




        if Eq(sField, FLD_NAME)  then oLink.Name := v else


        if Eq(sField, FLD_SITE1_LAT)     then oLink.Sites[1].BLPoint.B := v else
        if Eq(sField, FLD_SITE1_LON)     then oLink.Sites[1].BLPoint.L := v else
        if Eq(sField, FLD_SITE1_NAME)    then oLink.Sites[1].Name := v else
        if Eq(sField, FLD_SITE1_ADDRESS) then oLink.Sites[1].Address := v else
        if Eq(sField, FLD_SITE1_TOWER_HEIGHT) then oLink.Sites[1].TOWER_HEIGHT := v else
        if Eq(sField, FLD_SITE1_diameter)     then oLink.Sites[1].diameter     := AsFloat(v) else

        if Eq(sField, FLD_SITE1_LinkEnd_name) then oLink.Sites[1].LinkEnd_name := v else

      //  if Eq(sField, FLD_SITE1_power)    then oLink.Sites[1].power     := v else

//site2_tx_mhz


        if Eq(sField, FLD_SITE2_LAT)     then oLink.Sites[2].BLPoint.B := v else
        if Eq(sField, FLD_SITE2_LON)     then oLink.Sites[2].BLPoint.L := v else
        if Eq(sField, FLD_SITE2_NAME)    then oLink.Sites[2].Name := v else
        if Eq(sField, FLD_SITE2_ADDRESS) then oLink.Sites[2].Address := v else
        if Eq(sField, FLD_SITE2_TOWER_HEIGHT) then oLink.Sites[2].TOWER_HEIGHT := v else
        if Eq(sField, FLD_SITE2_diameter)     then oLink.Sites[2].diameter     := AsFloat(v) else

        if Eq(sField, FLD_SITE2_LinkEnd_name) then oLink.Sites[2].LinkEnd_name := v else

//

//        if Eq(sField, FLD_SITE2_tx_mhz)   then oLink.Sites[2].tx_mhz     := v else
//        if Eq(sField, FLD_SITE2_rx_mhz)   then oLink.Sites[2].rx_mhz     := v else

     //   if Eq(sField, FLD_SITE2_power)    then oLink.Sites[2].power     := v else


//        if Eq(sField, FLD_SITE1_NAME)  then oLink.Site1.Name := v else
//        if Eq(sField, FLD_SITE2_NAME)  then oLink.Site2.Name := v else


        if Eq(sField, FLD_power)   then  oLink.Power     := AsFloat(v) else

        if Eq(sField, FLD_tx_mhz)   then  oLink.tx_mhz     := AsFloat(v) else

        if Eq(sField, FLD_rx_mhz)   then  oLink.rx_mhz     := AsFloat(v) else



//        if Eq(sField, FLD_Transmission_Type)  then oLink.Transmission_Type := v else
//        if Eq(sField, FLD_Transmission_Mode)  then oLink.Transmission_Mode := v else

//LinkEnd_name

        if Eq(sField, 'modulation_type')  then oLink.Modulation := v else
//        if Eq(sField, FLD_E1_number)   then oLink.E1_number := v else
        if Eq(sField, FLD_channel_width_MHz)   then oLink.channel_width_MHz := AsFloat(v) else
//        if Eq(sField, 'Input_Power')   then  oLink.Input_Power := AsFloat(v) else
        if Eq(sField, FLD_LinkEndType_name)  then oLink.LinkEndType_name := v else
   //     if Eq(sField, 'E1_number')  then oLink.E1_number := v else
        if Eq(sField, FLD_band)     then oLink.band := v else

        if Eq(sField, 'Capacity')     then oLink.Capacity := AsFloat(v) else   


      except

      end;


{
    Modulation: string;
    E1_number: integer;

    equipment : string;

    band   : string;
}


//        if Eq(sField, FLD_LAT)  then coords.Lat:=geo_EncodeDegreeFromString (v) else
 //       if Eq(sField, FLD_LON)  then coords.Lon:=geo_EncodeDegreeFromString (v)


        ;


    end;

    if oLink.Name = '' then
      FreeAndNil(oLink);


    ///////////////////////////////
  ////////////  break;
  end;

end;


// ---------------------------------------------------------------
procedure TdmExcel_to_Classes.LoadColumns;
// ---------------------------------------------------------------
var
  iCol: Integer;
  iRow: Integer;
  k: Integer;
  rColumn: TColumn;
  s: string;
  sHeader: string;
begin
   Assert(g_ConfigFile.HeaderRowIndex_1>0, 'HeaderRowIndex_1>0');

//  Assert(FFirstLine>0, 'Value <=0');


  FDataSet_Columns.Close;
  FDataSet_Columns.Open;

  Assert (FDataSet_Columns.RecordCount=0);


//  k:=acxGridTableView.DataController.RecordCount;



  //  k:=acxGridTableView.ColumnCount;

//  db_view();

 // if dmExcel_file.ColCount>0 then
 // begin
    for iCol := 1 to dmExcel_file.ColCount  do
    begin
      sHeader:=VarToStr(dmExcel_file.Cells_1[g_ConfigFile.HeaderRowIndex_1, iCol]);


//      oColumn:= acxGridTableView.CreateColumn;
//      oColumn.Caption :=sHeader;

      db_AddRecord_(FDataSet_Columns,
          [FLD_INDEX,  iCol,
           FLD_HEADER, sHeader
          ]);

      if Length( g_ConfigFile.Columns) >= iCol then
      begin
        rColumn:=g_ConfigFile.Columns[iCol-1];

   //     if rColumn.FieldName then
        db_UpdateRecord__(FDataSet_Columns,
            [FLD_FIELD_NAME, rColumn.FieldName,
             FLD_KEY,        rColumn.IsKey
            ]);

      end;
    end;


end;



//--------------------------------------------------------------------
procedure geo_Encode2DegreeFromString(aValue: string; var
    aDegree1, aDegree2: Double);
//--------------------------------------------------------------------
// 54 44' 10.00''    52 42' 30.00''
//--------------------------------------------------------------------
  procedure DoAdd(var Value: string; aChar: Char);
  begin
    if (Value='') then
      if (aChar='0') or (not (aChar in ['0'..'9'])) then
         Exit;

    Value := Value + aChar;
  end;


var
  ang1: TAngle;
  ang2: TAngle;
  I: Integer;
  sValue, sStr: string;

  sDeg1: string;
  sMin1: string;
  sSec1: string;

  sDeg2: string;
  sMin2: string;
  sSec2: string;

  iSection : Integer;
 // s: string;
  bIsDigit : Boolean;

  ch: Char;
  e: Double;
  eDeg: Double;
  eDeg1: Integer;
  eDeg2: Integer;
  eMin: Double;
  eMin1: Double;
  eMin2: Double;
  eSec1: Double;
  eSec2: Double;

begin
  iSection:=0;

  for I := 1 to length(aValue) do
  begin
 //   s:=aValue[i];
    ch := aValue[i];

    if ch in [#$A] then
      iSection:=3
    else

    if ch in ['0'..'9','.',','] then
    begin
      bIsDigit:=True;

      case iSection of
        0: DoAdd(sDeg1, ch);
        1: DoAdd(sMin1, ch);
        2: DoAdd(sSec1, ch);

        3: DoAdd(sDeg2, ch);
        4: DoAdd(sMin2, ch);
        5: DoAdd(sSec2, ch);
      end;

    end else
    begin
      if bIsDigit then
      begin
        bIsDigit := False;
        Inc(iSection);

       // if  then

      end;
    end;
  end;


  eDeg1 := StrToIntDef(sDeg1, 0);
  eDeg2 := StrToIntDef(sDeg2, 0);

  eMin1 := AsFloat(sMin1);
  eMin2 := AsFloat(sMin2);

  eSec1 := AsFloat(sSec1);
  eSec2 := AsFloat(sSec2);

  e:=Frac(eMin1);
  e:=Frac(eMin2);

  // -------------------------
  if Frac(eMin1)>0 then
    aDegree1 := geo_EncodeDegree_Deg_Min(eDeg1, eMin1)
  else
    aDegree1 := geo_EncodeDegree(eDeg1, eMin1, eSec1);

  // -------------------------
  if Frac(eMin2)>0 then
    aDegree2 := geo_EncodeDegree_Deg_Min(eDeg2, eMin2)
  else
    aDegree2 := geo_EncodeDegree(eDeg2, eMin2, eSec2);

end;

//--------------------------------------------------------------------
function geo_EncodeDegreeFromString(aValue: string): Double;
//--------------------------------------------------------------------
var
  b: Boolean;
  eDegree1 : double;
  eDegree2 : Double;
begin
//  SysUtils.TFormatSettings.DecimalSeparator := '.';
  
  aValue := ReplaceStr(aValue, ',','.');

  if TryStrToFloat(aValue, Result) then
    Exit;



  geo_Encode2DegreeFromString(aValue, eDegree1, eDegree2);

  Result := eDegree1;
end;


//var
//  e: double;
//begin
 // e:=geo_EncodeDegreeFromString('33,4444');

  


end.




