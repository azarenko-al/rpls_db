
{***********************************************************}
{                                                           }
{                     XML Data Binding                      }
{                                                           }
{         Generated on: 29.03.2011 13:45:43                 }
{       Generated from: K:\Tele2\xml_project_templave.xml   }
{   Settings stored in: K:\Tele2\xml_project_templave.xdb   }
{                                                           }
{***********************************************************}

unit xml_project_template;

interface

uses XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLDocumentType = interface;
  IXMLProjectType = interface;
  IXMLNetstandardsType = interface;
  IXMLNetstandardType = interface;
  IXMLMatrixesType = interface;
  IXMLItemType = interface;
  IXMLCOMBINERSType = interface;
  IXMLCombinerType = interface;
  IXMLTRXSType = interface;
  IXMLTrxType = interface;
  IXMLScenarioType = interface;
  IXMLScenarioTypeList = interface;
  IXMLStationsType = interface;
  IXMLSiteType = interface;
  IXMLSectorType = interface;
  IXMLANTENNAType = interface;
  IXMLDNA_vertType = interface;
  IXMLDNA_horzType = interface;

{ IXMLDocumentType }

  IXMLDocumentType = interface(IXMLNode)
    ['{F79E6F54-3B78-4527-9171-A0A160394774}']
    { Property Accessors }
    function Get_Project: IXMLProjectType;
    { Methods & Properties }
    property Project: IXMLProjectType read Get_Project;
  end;

{ IXMLProjectType }

  IXMLProjectType = interface(IXMLNode)
    ['{87186A10-8742-4E84-AFA2-7A5813A3241C}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_OptimisedScenario_id: WideString;
    function Get_Name: WideString;
    function Get_Netstandards: IXMLNetstandardsType;
    function Get_Matrixes: IXMLMatrixesType;
    function Get_COMBINERS: IXMLCOMBINERSType;
    function Get_TRXS: IXMLTRXSType;
    function Get_Scenario: IXMLScenarioTypeList;
    procedure Set_Id(Value: WideString);
    procedure Set_OptimisedScenario_id(Value: WideString);
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property OptimisedScenario_id: WideString read Get_OptimisedScenario_id write Set_OptimisedScenario_id;
    property Name: WideString read Get_Name write Set_Name;
    property Netstandards: IXMLNetstandardsType read Get_Netstandards;
    property Matrixes: IXMLMatrixesType read Get_Matrixes;
    property COMBINERS: IXMLCOMBINERSType read Get_COMBINERS;
    property TRXS: IXMLTRXSType read Get_TRXS;
    property Scenario: IXMLScenarioTypeList read Get_Scenario;
  end;

{ IXMLNetstandardsType }

  IXMLNetstandardsType = interface(IXMLNodeCollection)
    ['{5AFE6EBB-15C9-4D33-8363-1C4E7B4FC905}']
    { Property Accessors }
    function Get_Netstandard(Index: Integer): IXMLNetstandardType;
    { Methods & Properties }
    function Add: IXMLNetstandardType;
    function Insert(const Index: Integer): IXMLNetstandardType;
    property Netstandard[Index: Integer]: IXMLNetstandardType read Get_Netstandard; default;
  end;

{ IXMLNetstandardType }

  IXMLNetstandardType = interface(IXMLNode)
    ['{3E9D425D-520B-418F-A8E3-3E630A34371F}']
    { Property Accessors }
    function Get_Name: WideString;
    function Get_Channels: WideString;
    function Get_Id: WideString;
    function Get_Freq_Min: WideString;
    function Get_Freq_Max: WideString;
    function Get_Freq_ave: WideString;
    function Get_Bandwidth: WideString;
    function Get_Duplex: WideString;
    function Get_ChannelWidth: WideString;
    function Get_FreqCount: WideString;
    function Get_LogicChannelCount: WideString;
    function Get_TotalChannelCount: WideString;
    function Get_CI_requirement: WideString;
    function Get_StartChannelNum: WideString;
    procedure Set_Name(Value: WideString);
    procedure Set_Channels(Value: WideString);
    procedure Set_Id(Value: WideString);
    procedure Set_Freq_Min(Value: WideString);
    procedure Set_Freq_Max(Value: WideString);
    procedure Set_Freq_ave(Value: WideString);
    procedure Set_Bandwidth(Value: WideString);
    procedure Set_Duplex(Value: WideString);
    procedure Set_ChannelWidth(Value: WideString);
    procedure Set_FreqCount(Value: WideString);
    procedure Set_LogicChannelCount(Value: WideString);
    procedure Set_TotalChannelCount(Value: WideString);
    procedure Set_CI_requirement(Value: WideString);
    procedure Set_StartChannelNum(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
    property Channels: WideString read Get_Channels write Set_Channels;
    property Id: WideString read Get_Id write Set_Id;
    property Freq_Min: WideString read Get_Freq_Min write Set_Freq_Min;
    property Freq_Max: WideString read Get_Freq_Max write Set_Freq_Max;
    property Freq_ave: WideString read Get_Freq_ave write Set_Freq_ave;
    property Bandwidth: WideString read Get_Bandwidth write Set_Bandwidth;
    property Duplex: WideString read Get_Duplex write Set_Duplex;
    property ChannelWidth: WideString read Get_ChannelWidth write Set_ChannelWidth;
    property FreqCount: WideString read Get_FreqCount write Set_FreqCount;
    property LogicChannelCount: WideString read Get_LogicChannelCount write Set_LogicChannelCount;
    property TotalChannelCount: WideString read Get_TotalChannelCount write Set_TotalChannelCount;
    property CI_requirement: WideString read Get_CI_requirement write Set_CI_requirement;
    property StartChannelNum: WideString read Get_StartChannelNum write Set_StartChannelNum;
  end;

{ IXMLMatrixesType }

  IXMLMatrixesType = interface(IXMLNodeCollection)
    ['{791332C7-CBE3-4046-8FF4-357B60302595}']
    { Property Accessors }
    function Get_Item(Index: Integer): IXMLItemType;
    { Methods & Properties }
    function Add: IXMLItemType;
    function Insert(const Index: Integer): IXMLItemType;
    property Item[Index: Integer]: IXMLItemType read Get_Item; default;
  end;

{ IXMLItemType }

  IXMLItemType = interface(IXMLNode)
    ['{D09D09BA-D49B-4DF2-A234-F7C1E24A55B0}']
    { Property Accessors }
    function Get_Filename: WideString;
    function Get_Enabled: WideString;
    function Get_Step: WideString;
    function Get_Zone: WideString;
    function Get_Filesize: WideString;
    procedure Set_Filename(Value: WideString);
    procedure Set_Enabled(Value: WideString);
    procedure Set_Step(Value: WideString);
    procedure Set_Zone(Value: WideString);
    procedure Set_Filesize(Value: WideString);
    { Methods & Properties }
    property Filename: WideString read Get_Filename write Set_Filename;
    property Enabled: WideString read Get_Enabled write Set_Enabled;
    property Step: WideString read Get_Step write Set_Step;
    property Zone: WideString read Get_Zone write Set_Zone;
    property Filesize: WideString read Get_Filesize write Set_Filesize;
  end;

{ IXMLCOMBINERSType }

  IXMLCOMBINERSType = interface(IXMLNodeCollection)
    ['{64B81526-36DC-4AA8-991F-E642F57B89D8}']
    { Property Accessors }
    function Get_Combiner(Index: Integer): IXMLCombinerType;
    { Methods & Properties }
    function Add: IXMLCombinerType;
    function Insert(const Index: Integer): IXMLCombinerType;
    property Combiner[Index: Integer]: IXMLCombinerType read Get_Combiner; default;
  end;

{ IXMLCombinerType }

  IXMLCombinerType = interface(IXMLNode)
    ['{3FF8D91C-6E61-401E-8E5F-17542749DAD9}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Name: WideString;
    function Get_Number: WideString;
    function Get_Losses: WideString;
    function Get_Freq: WideString;
    procedure Set_Id(Value: WideString);
    procedure Set_Name(Value: WideString);
    procedure Set_Number(Value: WideString);
    procedure Set_Losses(Value: WideString);
    procedure Set_Freq(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property Name: WideString read Get_Name write Set_Name;
    property Number: WideString read Get_Number write Set_Number;
    property Losses: WideString read Get_Losses write Set_Losses;
    property Freq: WideString read Get_Freq write Set_Freq;
  end;

{ IXMLTRXSType }

  IXMLTRXSType = interface(IXMLNodeCollection)
    ['{1BF3F000-D874-49C6-B1E9-198A9F0A483A}']
    { Property Accessors }
    function Get_Trx(Index: Integer): IXMLTrxType;
    { Methods & Properties }
    function Add: IXMLTrxType;
    function Insert(const Index: Integer): IXMLTrxType;
    property Trx[Index: Integer]: IXMLTrxType read Get_Trx; default;
  end;

{ IXMLTrxType }

  IXMLTrxType = interface(IXMLNode)
    ['{54197DEE-AD41-4BF7-8A87-A5E1785C9988}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Name: WideString;
    function Get_Power: WideString;
    function Get_Sensitivity: WideString;
    function Get_FreqWork: WideString;
    procedure Set_Id(Value: WideString);
    procedure Set_Name(Value: WideString);
    procedure Set_Power(Value: WideString);
    procedure Set_Sensitivity(Value: WideString);
    procedure Set_FreqWork(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property Name: WideString read Get_Name write Set_Name;
    property Power: WideString read Get_Power write Set_Power;
    property Sensitivity: WideString read Get_Sensitivity write Set_Sensitivity;
    property FreqWork: WideString read Get_FreqWork write Set_FreqWork;
  end;

{ IXMLScenarioType }

  IXMLScenarioType = interface(IXMLNode)
    ['{CCF13513-7C9F-41BD-8140-DF7C79315EEE}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Name: WideString;
    function Get_Comments: WideString;
    function Get_OptimisedRegion_id: WideString;
    function Get_Active: WideString;
    function Get_Stations: IXMLStationsType;
    procedure Set_Id(Value: WideString);
    procedure Set_Name(Value: WideString);
    procedure Set_Comments(Value: WideString);
    procedure Set_OptimisedRegion_id(Value: WideString);
    procedure Set_Active(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property Name: WideString read Get_Name write Set_Name;
    property Comments: WideString read Get_Comments write Set_Comments;
    property OptimisedRegion_id: WideString read Get_OptimisedRegion_id write Set_OptimisedRegion_id;
    property Active: WideString read Get_Active write Set_Active;
    property Stations: IXMLStationsType read Get_Stations;
  end;

{ IXMLScenarioTypeList }

  IXMLScenarioTypeList = interface(IXMLNodeCollection)
    ['{6F7BF037-2709-48FE-BA21-BA553C9D281D}']
    { Methods & Properties }
    function Add: IXMLScenarioType;
    function Insert(const Index: Integer): IXMLScenarioType;
    function Get_Item(Index: Integer): IXMLScenarioType;
    property Items[Index: Integer]: IXMLScenarioType read Get_Item; default;
  end;

{ IXMLStationsType }

  IXMLStationsType = interface(IXMLNodeCollection)
    ['{4F305E24-E54E-458C-957B-9EBF4A9C8149}']
    { Property Accessors }
    function Get_Expanded: WideString;
    function Get_Site(Index: Integer): IXMLSiteType;
    procedure Set_Expanded(Value: WideString);
    { Methods & Properties }
    function Add: IXMLSiteType;
    function Insert(const Index: Integer): IXMLSiteType;
    property Expanded: WideString read Get_Expanded write Set_Expanded;
    property Site[Index: Integer]: IXMLSiteType read Get_Site; default;
  end;

{ IXMLSiteType }

  IXMLSiteType = interface(IXMLNodeCollection)
    ['{013B77BA-993C-48CA-8993-EE1A9DF226A1}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Number: WideString;
    function Get_Name: WideString;
    function Get_CurrStatus: WideString;
    function Get_Address: WideString;
    function Get_Building: WideString;
    function Get_LAC: WideString;
    function Get_Radius: WideString;
    function Get_Radius_CIA: WideString;
    function Get_B: WideString;
    function Get_L: WideString;
    function Get_RelHeight: WideString;
    function Get_LocalName: WideString;
    function Get_LocalHeight: WideString;
    function Get_Comment: WideString;
    function Get_SectorLen: WideString;
    function Get_Sector(Index: Integer): IXMLSectorType;
    procedure Set_Id(Value: WideString);
    procedure Set_Number(Value: WideString);
    procedure Set_Name(Value: WideString);
    procedure Set_CurrStatus(Value: WideString);
    procedure Set_Address(Value: WideString);
    procedure Set_Building(Value: WideString);
    procedure Set_LAC(Value: WideString);
    procedure Set_Radius(Value: WideString);
    procedure Set_Radius_CIA(Value: WideString);
    procedure Set_B(Value: WideString);
    procedure Set_L(Value: WideString);
    procedure Set_RelHeight(Value: WideString);
    procedure Set_LocalName(Value: WideString);
    procedure Set_LocalHeight(Value: WideString);
    procedure Set_Comment(Value: WideString);
    procedure Set_SectorLen(Value: WideString);
    { Methods & Properties }
    function Add: IXMLSectorType;
    function Insert(const Index: Integer): IXMLSectorType;
    property Id: WideString read Get_Id write Set_Id;
    property Number: WideString read Get_Number write Set_Number;
    property Name: WideString read Get_Name write Set_Name;
    property CurrStatus: WideString read Get_CurrStatus write Set_CurrStatus;
    property Address: WideString read Get_Address write Set_Address;
    property Building: WideString read Get_Building write Set_Building;
    property LAC: WideString read Get_LAC write Set_LAC;
    property Radius: WideString read Get_Radius write Set_Radius;
    property Radius_CIA: WideString read Get_Radius_CIA write Set_Radius_CIA;
    property B: WideString read Get_B write Set_B;
    property L: WideString read Get_L write Set_L;
    property RelHeight: WideString read Get_RelHeight write Set_RelHeight;
    property LocalName: WideString read Get_LocalName write Set_LocalName;
    property LocalHeight: WideString read Get_LocalHeight write Set_LocalHeight;
    property Comment: WideString read Get_Comment write Set_Comment;
    property SectorLen: WideString read Get_SectorLen write Set_SectorLen;
    property Sector[Index: Integer]: IXMLSectorType read Get_Sector; default;
  end;

{ IXMLSectorType }

  IXMLSectorType = interface(IXMLNodeCollection)
    ['{5DF77EF9-AC88-4339-8C45-51E27CFCB897}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Name: WideString;
    function Get_Cell_ID: WideString;
    function Get_Color: WideString;
    function Get_BSIC_id: WideString;
    function Get_BCCH: WideString;
    function Get_Hysteresis: WideString;
    function Get_Forbidden_Channels: WideString;
    function Get_Allowed_Channels: WideString;
    function Get_Fixed_Channels: WideString;
    function Get_FreqRequiredCount: WideString;
    function Get_CalcRadius: WideString;
    function Get_DeltaGrad: WideString;
    function Get_CalcMOdel_id: WideString;
    function Get_Tp_id: WideString;
    function Get_CalcMOdel_name: WideString;
    function Get_Tp_name: WideString;
    function Get_K0_open: WideString;
    function Get_K0_closed: WideString;
    function Get_NetStandard_ID: WideString;
    function Get_CombinerID: WideString;
    function Get_TrxID: WideString;
    function Get_ANTENNA(Index: Integer): IXMLANTENNAType;
    procedure Set_Id(Value: WideString);
    procedure Set_Name(Value: WideString);
    procedure Set_Cell_ID(Value: WideString);
    procedure Set_Color(Value: WideString);
    procedure Set_BSIC_id(Value: WideString);
    procedure Set_BCCH(Value: WideString);
    procedure Set_Hysteresis(Value: WideString);
    procedure Set_Forbidden_Channels(Value: WideString);
    procedure Set_Allowed_Channels(Value: WideString);
    procedure Set_Fixed_Channels(Value: WideString);
    procedure Set_FreqRequiredCount(Value: WideString);
    procedure Set_CalcRadius(Value: WideString);
    procedure Set_DeltaGrad(Value: WideString);
    procedure Set_CalcMOdel_id(Value: WideString);
    procedure Set_Tp_id(Value: WideString);
    procedure Set_CalcMOdel_name(Value: WideString);
    procedure Set_Tp_name(Value: WideString);
    procedure Set_K0_open(Value: WideString);
    procedure Set_K0_closed(Value: WideString);
    procedure Set_NetStandard_ID(Value: WideString);
    procedure Set_CombinerID(Value: WideString);
    procedure Set_TrxID(Value: WideString);
    { Methods & Properties }
    function Add: IXMLANTENNAType;
    function Insert(const Index: Integer): IXMLANTENNAType;
    property Id: WideString read Get_Id write Set_Id;
    property Name: WideString read Get_Name write Set_Name;
    property Cell_ID: WideString read Get_Cell_ID write Set_Cell_ID;
    property Color: WideString read Get_Color write Set_Color;
    property BSIC_id: WideString read Get_BSIC_id write Set_BSIC_id;
    property BCCH: WideString read Get_BCCH write Set_BCCH;
    property Hysteresis: WideString read Get_Hysteresis write Set_Hysteresis;
    property Forbidden_Channels: WideString read Get_Forbidden_Channels write Set_Forbidden_Channels;
    property Allowed_Channels: WideString read Get_Allowed_Channels write Set_Allowed_Channels;
    property Fixed_Channels: WideString read Get_Fixed_Channels write Set_Fixed_Channels;
    property FreqRequiredCount: WideString read Get_FreqRequiredCount write Set_FreqRequiredCount;
    property CalcRadius: WideString read Get_CalcRadius write Set_CalcRadius;
    property DeltaGrad: WideString read Get_DeltaGrad write Set_DeltaGrad;
    property CalcMOdel_id: WideString read Get_CalcMOdel_id write Set_CalcMOdel_id;
    property Tp_id: WideString read Get_Tp_id write Set_Tp_id;
    property CalcMOdel_name: WideString read Get_CalcMOdel_name write Set_CalcMOdel_name;
    property Tp_name: WideString read Get_Tp_name write Set_Tp_name;
    property K0_open: WideString read Get_K0_open write Set_K0_open;
    property K0_closed: WideString read Get_K0_closed write Set_K0_closed;
    property NetStandard_ID: WideString read Get_NetStandard_ID write Set_NetStandard_ID;
    property CombinerID: WideString read Get_CombinerID write Set_CombinerID;
    property TrxID: WideString read Get_TrxID write Set_TrxID;
    property ANTENNA[Index: Integer]: IXMLANTENNAType read Get_ANTENNA; default;
  end;

{ IXMLANTENNAType }

  IXMLANTENNAType = interface(IXMLNode)
    ['{7A25B30C-9BCE-43B2-B988-D2F5CF52638D}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Name: WideString;
    function Get_Azimuth: WideString;
    function Get_Height: WideString;
    function Get_ServDistance: WideString;
    function Get_Checked: WideString;
    function Get_FeederLoss: WideString;
    function Get_B: WideString;
    function Get_L: WideString;
    function Get_FixedPosition: WideString;
    function Get_Model: WideString;
    function Get_Producer: WideString;
    function Get_Gain: WideString;
    function Get_Freq: WideString;
    function Get_Slope: WideString;
    function Get_Rotate: WideString;
    function Get_TiltElectrical: WideString;
    function Get_DNA_FileName: WideString;
    function Get_OMNI: WideString;
    function Get_DNA_vert: IXMLDNA_vertType;
    function Get_DNA_horz: IXMLDNA_horzType;
    procedure Set_Id(Value: WideString);
    procedure Set_Name(Value: WideString);
    procedure Set_Azimuth(Value: WideString);
    procedure Set_Height(Value: WideString);
    procedure Set_ServDistance(Value: WideString);
    procedure Set_Checked(Value: WideString);
    procedure Set_FeederLoss(Value: WideString);
    procedure Set_B(Value: WideString);
    procedure Set_L(Value: WideString);
    procedure Set_FixedPosition(Value: WideString);
    procedure Set_Model(Value: WideString);
    procedure Set_Producer(Value: WideString);
    procedure Set_Gain(Value: WideString);
    procedure Set_Freq(Value: WideString);
    procedure Set_Slope(Value: WideString);
    procedure Set_Rotate(Value: WideString);
    procedure Set_TiltElectrical(Value: WideString);
    procedure Set_DNA_FileName(Value: WideString);
    procedure Set_OMNI(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property Name: WideString read Get_Name write Set_Name;
    property Azimuth: WideString read Get_Azimuth write Set_Azimuth;
    property Height: WideString read Get_Height write Set_Height;
    property ServDistance: WideString read Get_ServDistance write Set_ServDistance;
    property Checked: WideString read Get_Checked write Set_Checked;
    property FeederLoss: WideString read Get_FeederLoss write Set_FeederLoss;
    property B: WideString read Get_B write Set_B;
    property L: WideString read Get_L write Set_L;
    property FixedPosition: WideString read Get_FixedPosition write Set_FixedPosition;
    property Model: WideString read Get_Model write Set_Model;
    property Producer: WideString read Get_Producer write Set_Producer;
    property Gain: WideString read Get_Gain write Set_Gain;
    property Freq: WideString read Get_Freq write Set_Freq;
    property Slope: WideString read Get_Slope write Set_Slope;
    property Rotate: WideString read Get_Rotate write Set_Rotate;
    property TiltElectrical: WideString read Get_TiltElectrical write Set_TiltElectrical;
    property DNA_FileName: WideString read Get_DNA_FileName write Set_DNA_FileName;
    property OMNI: WideString read Get_OMNI write Set_OMNI;
    property DNA_vert: IXMLDNA_vertType read Get_DNA_vert;
    property DNA_horz: IXMLDNA_horzType read Get_DNA_horz;
  end;

{ IXMLDNA_vertType }

  IXMLDNA_vertType = interface(IXMLNode)
    ['{7C482EA4-AC36-4CC6-AFAD-E5F536EA03F0}']
    { Property Accessors }
    function Get_Value: WideString;
    procedure Set_Value(Value: WideString);
    { Methods & Properties }
    property Value: WideString read Get_Value write Set_Value;
  end;

{ IXMLDNA_horzType }

  IXMLDNA_horzType = interface(IXMLNode)
    ['{6C6F48A2-D474-4ADB-92FB-5A0DD0AE678F}']
    { Property Accessors }
    function Get_Value: WideString;
    procedure Set_Value(Value: WideString);
    { Methods & Properties }
    property Value: WideString read Get_Value write Set_Value;
  end;

{ Forward Decls }

  TXMLDocumentType = class;
  TXMLProjectType = class;
  TXMLNetstandardsType = class;
  TXMLNetstandardType = class;
  TXMLMatrixesType = class;
  TXMLItemType = class;
  TXMLCOMBINERSType = class;
  TXMLCombinerType = class;
  TXMLTRXSType = class;
  TXMLTrxType = class;
  TXMLScenarioType = class;
  TXMLScenarioTypeList = class;
  TXMLStationsType = class;
  TXMLSiteType = class;
  TXMLSectorType = class;
  TXMLANTENNAType = class;
  TXMLDNA_vertType = class;
  TXMLDNA_horzType = class;

{ TXMLDocumentType }

  TXMLDocumentType = class(TXMLNode, IXMLDocumentType)
  protected
    { IXMLDocumentType }
    function Get_Project: IXMLProjectType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLProjectType }

  TXMLProjectType = class(TXMLNode, IXMLProjectType)
  private
    FScenario: IXMLScenarioTypeList;
  protected
    { IXMLProjectType }
    function Get_Id: WideString;
    function Get_OptimisedScenario_id: WideString;
    function Get_Name: WideString;
    function Get_Netstandards: IXMLNetstandardsType;
    function Get_Matrixes: IXMLMatrixesType;
    function Get_COMBINERS: IXMLCOMBINERSType;
    function Get_TRXS: IXMLTRXSType;
    function Get_Scenario: IXMLScenarioTypeList;
    procedure Set_Id(Value: WideString);
    procedure Set_OptimisedScenario_id(Value: WideString);
    procedure Set_Name(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLNetstandardsType }

  TXMLNetstandardsType = class(TXMLNodeCollection, IXMLNetstandardsType)
  protected
    { IXMLNetstandardsType }
    function Get_Netstandard(Index: Integer): IXMLNetstandardType;
    function Add: IXMLNetstandardType;
    function Insert(const Index: Integer): IXMLNetstandardType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLNetstandardType }

  TXMLNetstandardType = class(TXMLNode, IXMLNetstandardType)
  protected
    { IXMLNetstandardType }
    function Get_Name: WideString;
    function Get_Channels: WideString;
    function Get_Id: WideString;
    function Get_Freq_Min: WideString;
    function Get_Freq_Max: WideString;
    function Get_Freq_ave: WideString;
    function Get_Bandwidth: WideString;
    function Get_Duplex: WideString;
    function Get_ChannelWidth: WideString;
    function Get_FreqCount: WideString;
    function Get_LogicChannelCount: WideString;
    function Get_TotalChannelCount: WideString;
    function Get_CI_requirement: WideString;
    function Get_StartChannelNum: WideString;
    procedure Set_Name(Value: WideString);
    procedure Set_Channels(Value: WideString);
    procedure Set_Id(Value: WideString);
    procedure Set_Freq_Min(Value: WideString);
    procedure Set_Freq_Max(Value: WideString);
    procedure Set_Freq_ave(Value: WideString);
    procedure Set_Bandwidth(Value: WideString);
    procedure Set_Duplex(Value: WideString);
    procedure Set_ChannelWidth(Value: WideString);
    procedure Set_FreqCount(Value: WideString);
    procedure Set_LogicChannelCount(Value: WideString);
    procedure Set_TotalChannelCount(Value: WideString);
    procedure Set_CI_requirement(Value: WideString);
    procedure Set_StartChannelNum(Value: WideString);
  end;

{ TXMLMatrixesType }

  TXMLMatrixesType = class(TXMLNodeCollection, IXMLMatrixesType)
  protected
    { IXMLMatrixesType }
    function Get_Item(Index: Integer): IXMLItemType;
    function Add: IXMLItemType;
    function Insert(const Index: Integer): IXMLItemType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLItemType }

  TXMLItemType = class(TXMLNode, IXMLItemType)
  protected
    { IXMLItemType }
    function Get_Filename: WideString;
    function Get_Enabled: WideString;
    function Get_Step: WideString;
    function Get_Zone: WideString;
    function Get_Filesize: WideString;
    procedure Set_Filename(Value: WideString);
    procedure Set_Enabled(Value: WideString);
    procedure Set_Step(Value: WideString);
    procedure Set_Zone(Value: WideString);
    procedure Set_Filesize(Value: WideString);
  end;

{ TXMLCOMBINERSType }

  TXMLCOMBINERSType = class(TXMLNodeCollection, IXMLCOMBINERSType)
  protected
    { IXMLCOMBINERSType }
    function Get_Combiner(Index: Integer): IXMLCombinerType;
    function Add: IXMLCombinerType;
    function Insert(const Index: Integer): IXMLCombinerType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCombinerType }

  TXMLCombinerType = class(TXMLNode, IXMLCombinerType)
  protected
    { IXMLCombinerType }
    function Get_Id: WideString;
    function Get_Name: WideString;
    function Get_Number: WideString;
    function Get_Losses: WideString;
    function Get_Freq: WideString;
    procedure Set_Id(Value: WideString);
    procedure Set_Name(Value: WideString);
    procedure Set_Number(Value: WideString);
    procedure Set_Losses(Value: WideString);
    procedure Set_Freq(Value: WideString);
  end;

{ TXMLTRXSType }

  TXMLTRXSType = class(TXMLNodeCollection, IXMLTRXSType)
  protected
    { IXMLTRXSType }
    function Get_Trx(Index: Integer): IXMLTrxType;
    function Add: IXMLTrxType;
    function Insert(const Index: Integer): IXMLTrxType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTrxType }

  TXMLTrxType = class(TXMLNode, IXMLTrxType)
  protected
    { IXMLTrxType }
    function Get_Id: WideString;
    function Get_Name: WideString;
    function Get_Power: WideString;
    function Get_Sensitivity: WideString;
    function Get_FreqWork: WideString;
    procedure Set_Id(Value: WideString);
    procedure Set_Name(Value: WideString);
    procedure Set_Power(Value: WideString);
    procedure Set_Sensitivity(Value: WideString);
    procedure Set_FreqWork(Value: WideString);
  end;

{ TXMLScenarioType }

  TXMLScenarioType = class(TXMLNode, IXMLScenarioType)
  protected
    { IXMLScenarioType }
    function Get_Id: WideString;
    function Get_Name: WideString;
    function Get_Comments: WideString;
    function Get_OptimisedRegion_id: WideString;
    function Get_Active: WideString;
    function Get_Stations: IXMLStationsType;
    procedure Set_Id(Value: WideString);
    procedure Set_Name(Value: WideString);
    procedure Set_Comments(Value: WideString);
    procedure Set_OptimisedRegion_id(Value: WideString);
    procedure Set_Active(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLScenarioTypeList }

  TXMLScenarioTypeList = class(TXMLNodeCollection, IXMLScenarioTypeList)
  protected
    { IXMLScenarioTypeList }
    function Add: IXMLScenarioType;
    function Insert(const Index: Integer): IXMLScenarioType;
    function Get_Item(Index: Integer): IXMLScenarioType;
  end;

{ TXMLStationsType }

  TXMLStationsType = class(TXMLNodeCollection, IXMLStationsType)
  protected
    { IXMLStationsType }
    function Get_Expanded: WideString;
    function Get_Site(Index: Integer): IXMLSiteType;
    procedure Set_Expanded(Value: WideString);
    function Add: IXMLSiteType;
    function Insert(const Index: Integer): IXMLSiteType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSiteType }

  TXMLSiteType = class(TXMLNodeCollection, IXMLSiteType)
  protected
    { IXMLSiteType }
    function Get_Id: WideString;
    function Get_Number: WideString;
    function Get_Name: WideString;
    function Get_CurrStatus: WideString;
    function Get_Address: WideString;
    function Get_Building: WideString;
    function Get_LAC: WideString;
    function Get_Radius: WideString;
    function Get_Radius_CIA: WideString;
    function Get_B: WideString;
    function Get_L: WideString;
    function Get_RelHeight: WideString;
    function Get_LocalName: WideString;
    function Get_LocalHeight: WideString;
    function Get_Comment: WideString;
    function Get_SectorLen: WideString;
    function Get_Sector(Index: Integer): IXMLSectorType;
    procedure Set_Id(Value: WideString);
    procedure Set_Number(Value: WideString);
    procedure Set_Name(Value: WideString);
    procedure Set_CurrStatus(Value: WideString);
    procedure Set_Address(Value: WideString);
    procedure Set_Building(Value: WideString);
    procedure Set_LAC(Value: WideString);
    procedure Set_Radius(Value: WideString);
    procedure Set_Radius_CIA(Value: WideString);
    procedure Set_B(Value: WideString);
    procedure Set_L(Value: WideString);
    procedure Set_RelHeight(Value: WideString);
    procedure Set_LocalName(Value: WideString);
    procedure Set_LocalHeight(Value: WideString);
    procedure Set_Comment(Value: WideString);
    procedure Set_SectorLen(Value: WideString);
    function Add: IXMLSectorType;
    function Insert(const Index: Integer): IXMLSectorType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSectorType }

  TXMLSectorType = class(TXMLNodeCollection, IXMLSectorType)
  protected
    { IXMLSectorType }
    function Get_Id: WideString;
    function Get_Name: WideString;
    function Get_Cell_ID: WideString;
    function Get_Color: WideString;
    function Get_BSIC_id: WideString;
    function Get_BCCH: WideString;
    function Get_Hysteresis: WideString;
    function Get_Forbidden_Channels: WideString;
    function Get_Allowed_Channels: WideString;
    function Get_Fixed_Channels: WideString;
    function Get_FreqRequiredCount: WideString;
    function Get_CalcRadius: WideString;
    function Get_DeltaGrad: WideString;
    function Get_CalcMOdel_id: WideString;
    function Get_Tp_id: WideString;
    function Get_CalcMOdel_name: WideString;
    function Get_Tp_name: WideString;
    function Get_K0_open: WideString;
    function Get_K0_closed: WideString;
    function Get_NetStandard_ID: WideString;
    function Get_CombinerID: WideString;
    function Get_TrxID: WideString;
    function Get_ANTENNA(Index: Integer): IXMLANTENNAType;
    procedure Set_Id(Value: WideString);
    procedure Set_Name(Value: WideString);
    procedure Set_Cell_ID(Value: WideString);
    procedure Set_Color(Value: WideString);
    procedure Set_BSIC_id(Value: WideString);
    procedure Set_BCCH(Value: WideString);
    procedure Set_Hysteresis(Value: WideString);
    procedure Set_Forbidden_Channels(Value: WideString);
    procedure Set_Allowed_Channels(Value: WideString);
    procedure Set_Fixed_Channels(Value: WideString);
    procedure Set_FreqRequiredCount(Value: WideString);
    procedure Set_CalcRadius(Value: WideString);
    procedure Set_DeltaGrad(Value: WideString);
    procedure Set_CalcMOdel_id(Value: WideString);
    procedure Set_Tp_id(Value: WideString);
    procedure Set_CalcMOdel_name(Value: WideString);
    procedure Set_Tp_name(Value: WideString);
    procedure Set_K0_open(Value: WideString);
    procedure Set_K0_closed(Value: WideString);
    procedure Set_NetStandard_ID(Value: WideString);
    procedure Set_CombinerID(Value: WideString);
    procedure Set_TrxID(Value: WideString);
    function Add: IXMLANTENNAType;
    function Insert(const Index: Integer): IXMLANTENNAType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLANTENNAType }

  TXMLANTENNAType = class(TXMLNode, IXMLANTENNAType)
  protected
    { IXMLANTENNAType }
    function Get_Id: WideString;
    function Get_Name: WideString;
    function Get_Azimuth: WideString;
    function Get_Height: WideString;
    function Get_ServDistance: WideString;
    function Get_Checked: WideString;
    function Get_FeederLoss: WideString;
    function Get_B: WideString;
    function Get_L: WideString;
    function Get_FixedPosition: WideString;
    function Get_Model: WideString;
    function Get_Producer: WideString;
    function Get_Gain: WideString;
    function Get_Freq: WideString;
    function Get_Slope: WideString;
    function Get_Rotate: WideString;
    function Get_TiltElectrical: WideString;
    function Get_DNA_FileName: WideString;
    function Get_OMNI: WideString;
    function Get_DNA_vert: IXMLDNA_vertType;
    function Get_DNA_horz: IXMLDNA_horzType;
    procedure Set_Id(Value: WideString);
    procedure Set_Name(Value: WideString);
    procedure Set_Azimuth(Value: WideString);
    procedure Set_Height(Value: WideString);
    procedure Set_ServDistance(Value: WideString);
    procedure Set_Checked(Value: WideString);
    procedure Set_FeederLoss(Value: WideString);
    procedure Set_B(Value: WideString);
    procedure Set_L(Value: WideString);
    procedure Set_FixedPosition(Value: WideString);
    procedure Set_Model(Value: WideString);
    procedure Set_Producer(Value: WideString);
    procedure Set_Gain(Value: WideString);
    procedure Set_Freq(Value: WideString);
    procedure Set_Slope(Value: WideString);
    procedure Set_Rotate(Value: WideString);
    procedure Set_TiltElectrical(Value: WideString);
    procedure Set_DNA_FileName(Value: WideString);
    procedure Set_OMNI(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDNA_vertType }

  TXMLDNA_vertType = class(TXMLNode, IXMLDNA_vertType)
  protected
    { IXMLDNA_vertType }
    function Get_Value: WideString;
    procedure Set_Value(Value: WideString);
  end;

{ TXMLDNA_horzType }

  TXMLDNA_horzType = class(TXMLNode, IXMLDNA_horzType)
  protected
    { IXMLDNA_horzType }
    function Get_Value: WideString;
    procedure Set_Value(Value: WideString);
  end;

{ Global Functions }

function GetDocument(Doc: IXMLDocument): IXMLDocumentType;
function LoadDocument(const FileName: WideString): IXMLDocumentType;
function NewDocument: IXMLDocumentType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetDocument(Doc: IXMLDocument): IXMLDocumentType;
begin
  Result := Doc.GetDocBinding('Document', TXMLDocumentType, TargetNamespace) as IXMLDocumentType;
end;

function LoadDocument(const FileName: WideString): IXMLDocumentType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Document', TXMLDocumentType, TargetNamespace) as IXMLDocumentType;
end;

function NewDocument: IXMLDocumentType;
begin
  Result := NewXMLDocument.GetDocBinding('Document', TXMLDocumentType, TargetNamespace) as IXMLDocumentType;
end;

{ TXMLDocumentType }

procedure TXMLDocumentType.AfterConstruction;
begin
  RegisterChildNode('project', TXMLProjectType);
  inherited;
end;

function TXMLDocumentType.Get_Project: IXMLProjectType;
begin
  Result := ChildNodes['project'] as IXMLProjectType;
end;

{ TXMLProjectType }

procedure TXMLProjectType.AfterConstruction;
begin
  RegisterChildNode('netstandards', TXMLNetstandardsType);
  RegisterChildNode('matrixes', TXMLMatrixesType);
  RegisterChildNode('COMBINERS', TXMLCOMBINERSType);
  RegisterChildNode('TRXS', TXMLTRXSType);
  RegisterChildNode('scenario', TXMLScenarioType);
  FScenario := CreateCollection(TXMLScenarioTypeList, IXMLScenarioType, 'scenario') as IXMLScenarioTypeList;
  inherited;
end;

function TXMLProjectType.Get_Id: WideString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLProjectType.Set_Id(Value: WideString);
begin
  SetAttribute('id', Value);
end;

function TXMLProjectType.Get_OptimisedScenario_id: WideString;
begin
  Result := AttributeNodes['OptimisedScenario_id'].Text;
end;

procedure TXMLProjectType.Set_OptimisedScenario_id(Value: WideString);
begin
  SetAttribute('OptimisedScenario_id', Value);
end;

function TXMLProjectType.Get_Name: WideString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLProjectType.Set_Name(Value: WideString);
begin
  SetAttribute('Name', Value);
end;

function TXMLProjectType.Get_Netstandards: IXMLNetstandardsType;
begin
  Result := ChildNodes['netstandards'] as IXMLNetstandardsType;
end;

function TXMLProjectType.Get_Matrixes: IXMLMatrixesType;
begin
  Result := ChildNodes['matrixes'] as IXMLMatrixesType;
end;

function TXMLProjectType.Get_COMBINERS: IXMLCOMBINERSType;
begin
  Result := ChildNodes['COMBINERS'] as IXMLCOMBINERSType;
end;

function TXMLProjectType.Get_TRXS: IXMLTRXSType;
begin
  Result := ChildNodes['TRXS'] as IXMLTRXSType;
end;

function TXMLProjectType.Get_Scenario: IXMLScenarioTypeList;
begin
  Result := FScenario;
end;

{ TXMLNetstandardsType }

procedure TXMLNetstandardsType.AfterConstruction;
begin
  RegisterChildNode('netstandard', TXMLNetstandardType);
  ItemTag := 'netstandard';
  ItemInterface := IXMLNetstandardType;
  inherited;
end;

function TXMLNetstandardsType.Get_Netstandard(Index: Integer): IXMLNetstandardType;
begin
  Result := List[Index] as IXMLNetstandardType;
end;

function TXMLNetstandardsType.Add: IXMLNetstandardType;
begin
  Result := AddItem(-1) as IXMLNetstandardType;
end;

function TXMLNetstandardsType.Insert(const Index: Integer): IXMLNetstandardType;
begin
  Result := AddItem(Index) as IXMLNetstandardType;
end;

{ TXMLNetstandardType }

function TXMLNetstandardType.Get_Name: WideString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLNetstandardType.Set_Name(Value: WideString);
begin
  SetAttribute('Name', Value);
end;

function TXMLNetstandardType.Get_Channels: WideString;
begin
  Result := AttributeNodes['Channels'].Text;
end;

procedure TXMLNetstandardType.Set_Channels(Value: WideString);
begin
  SetAttribute('Channels', Value);
end;

function TXMLNetstandardType.Get_Id: WideString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLNetstandardType.Set_Id(Value: WideString);
begin
  SetAttribute('id', Value);
end;

function TXMLNetstandardType.Get_Freq_Min: WideString;
begin
  Result := AttributeNodes['Freq_Min'].Text;
end;

procedure TXMLNetstandardType.Set_Freq_Min(Value: WideString);
begin
  SetAttribute('Freq_Min', Value);
end;

function TXMLNetstandardType.Get_Freq_Max: WideString;
begin
  Result := AttributeNodes['Freq_Max'].Text;
end;

procedure TXMLNetstandardType.Set_Freq_Max(Value: WideString);
begin
  SetAttribute('Freq_Max', Value);
end;

function TXMLNetstandardType.Get_Freq_ave: WideString;
begin
  Result := AttributeNodes['Freq_ave'].Text;
end;

procedure TXMLNetstandardType.Set_Freq_ave(Value: WideString);
begin
  SetAttribute('Freq_ave', Value);
end;

function TXMLNetstandardType.Get_Bandwidth: WideString;
begin
  Result := AttributeNodes['Bandwidth'].Text;
end;

procedure TXMLNetstandardType.Set_Bandwidth(Value: WideString);
begin
  SetAttribute('Bandwidth', Value);
end;

function TXMLNetstandardType.Get_Duplex: WideString;
begin
  Result := AttributeNodes['Duplex'].Text;
end;

procedure TXMLNetstandardType.Set_Duplex(Value: WideString);
begin
  SetAttribute('Duplex', Value);
end;

function TXMLNetstandardType.Get_ChannelWidth: WideString;
begin
  Result := AttributeNodes['ChannelWidth'].Text;
end;

procedure TXMLNetstandardType.Set_ChannelWidth(Value: WideString);
begin
  SetAttribute('ChannelWidth', Value);
end;

function TXMLNetstandardType.Get_FreqCount: WideString;
begin
  Result := AttributeNodes['FreqCount'].Text;
end;

procedure TXMLNetstandardType.Set_FreqCount(Value: WideString);
begin
  SetAttribute('FreqCount', Value);
end;

function TXMLNetstandardType.Get_LogicChannelCount: WideString;
begin
  Result := AttributeNodes['LogicChannelCount'].Text;
end;

procedure TXMLNetstandardType.Set_LogicChannelCount(Value: WideString);
begin
  SetAttribute('LogicChannelCount', Value);
end;

function TXMLNetstandardType.Get_TotalChannelCount: WideString;
begin
  Result := AttributeNodes['TotalChannelCount'].Text;
end;

procedure TXMLNetstandardType.Set_TotalChannelCount(Value: WideString);
begin
  SetAttribute('TotalChannelCount', Value);
end;

function TXMLNetstandardType.Get_CI_requirement: WideString;
begin
  Result := AttributeNodes['CI_requirement'].Text;
end;

procedure TXMLNetstandardType.Set_CI_requirement(Value: WideString);
begin
  SetAttribute('CI_requirement', Value);
end;

function TXMLNetstandardType.Get_StartChannelNum: WideString;
begin
  Result := AttributeNodes['StartChannelNum'].Text;
end;

procedure TXMLNetstandardType.Set_StartChannelNum(Value: WideString);
begin
  SetAttribute('StartChannelNum', Value);
end;

{ TXMLMatrixesType }

procedure TXMLMatrixesType.AfterConstruction;
begin
  RegisterChildNode('Item', TXMLItemType);
  ItemTag := 'Item';
  ItemInterface := IXMLItemType;
  inherited;
end;

function TXMLMatrixesType.Get_Item(Index: Integer): IXMLItemType;
begin
  Result := List[Index] as IXMLItemType;
end;

function TXMLMatrixesType.Add: IXMLItemType;
begin
  Result := AddItem(-1) as IXMLItemType;
end;

function TXMLMatrixesType.Insert(const Index: Integer): IXMLItemType;
begin
  Result := AddItem(Index) as IXMLItemType;
end;

{ TXMLItemType }

function TXMLItemType.Get_Filename: WideString;
begin
  Result := AttributeNodes['filename'].Text;
end;

procedure TXMLItemType.Set_Filename(Value: WideString);
begin
  SetAttribute('filename', Value);
end;

function TXMLItemType.Get_Enabled: WideString;
begin
  Result := AttributeNodes['enabled'].Text;
end;

procedure TXMLItemType.Set_Enabled(Value: WideString);
begin
  SetAttribute('enabled', Value);
end;

function TXMLItemType.Get_Step: WideString;
begin
  Result := AttributeNodes['step'].Text;
end;

procedure TXMLItemType.Set_Step(Value: WideString);
begin
  SetAttribute('step', Value);
end;

function TXMLItemType.Get_Zone: WideString;
begin
  Result := AttributeNodes['zone'].Text;
end;

procedure TXMLItemType.Set_Zone(Value: WideString);
begin
  SetAttribute('zone', Value);
end;

function TXMLItemType.Get_Filesize: WideString;
begin
  Result := AttributeNodes['filesize'].Text;
end;

procedure TXMLItemType.Set_Filesize(Value: WideString);
begin
  SetAttribute('filesize', Value);
end;

{ TXMLCOMBINERSType }

procedure TXMLCOMBINERSType.AfterConstruction;
begin
  RegisterChildNode('Combiner', TXMLCombinerType);
  ItemTag := 'Combiner';
  ItemInterface := IXMLCombinerType;
  inherited;
end;

function TXMLCOMBINERSType.Get_Combiner(Index: Integer): IXMLCombinerType;
begin
  Result := List[Index] as IXMLCombinerType;
end;

function TXMLCOMBINERSType.Add: IXMLCombinerType;
begin
  Result := AddItem(-1) as IXMLCombinerType;
end;

function TXMLCOMBINERSType.Insert(const Index: Integer): IXMLCombinerType;
begin
  Result := AddItem(Index) as IXMLCombinerType;
end;

{ TXMLCombinerType }

function TXMLCombinerType.Get_Id: WideString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLCombinerType.Set_Id(Value: WideString);
begin
  SetAttribute('id', Value);
end;

function TXMLCombinerType.Get_Name: WideString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLCombinerType.Set_Name(Value: WideString);
begin
  SetAttribute('Name', Value);
end;

function TXMLCombinerType.Get_Number: WideString;
begin
  Result := AttributeNodes['Number'].Text;
end;

procedure TXMLCombinerType.Set_Number(Value: WideString);
begin
  SetAttribute('Number', Value);
end;

function TXMLCombinerType.Get_Losses: WideString;
begin
  Result := AttributeNodes['Losses'].Text;
end;

procedure TXMLCombinerType.Set_Losses(Value: WideString);
begin
  SetAttribute('Losses', Value);
end;

function TXMLCombinerType.Get_Freq: WideString;
begin
  Result := AttributeNodes['Freq'].Text;
end;

procedure TXMLCombinerType.Set_Freq(Value: WideString);
begin
  SetAttribute('Freq', Value);
end;

{ TXMLTRXSType }

procedure TXMLTRXSType.AfterConstruction;
begin
  RegisterChildNode('Trx', TXMLTrxType);
  ItemTag := 'Trx';
  ItemInterface := IXMLTrxType;
  inherited;
end;

function TXMLTRXSType.Get_Trx(Index: Integer): IXMLTrxType;
begin
  Result := List[Index] as IXMLTrxType;
end;

function TXMLTRXSType.Add: IXMLTrxType;
begin
  Result := AddItem(-1) as IXMLTrxType;
end;

function TXMLTRXSType.Insert(const Index: Integer): IXMLTrxType;
begin
  Result := AddItem(Index) as IXMLTrxType;
end;

{ TXMLTrxType }

function TXMLTrxType.Get_Id: WideString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLTrxType.Set_Id(Value: WideString);
begin
  SetAttribute('id', Value);
end;

function TXMLTrxType.Get_Name: WideString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLTrxType.Set_Name(Value: WideString);
begin
  SetAttribute('Name', Value);
end;

function TXMLTrxType.Get_Power: WideString;
begin
  Result := AttributeNodes['Power'].Text;
end;

procedure TXMLTrxType.Set_Power(Value: WideString);
begin
  SetAttribute('Power', Value);
end;

function TXMLTrxType.Get_Sensitivity: WideString;
begin
  Result := AttributeNodes['Sensitivity'].Text;
end;

procedure TXMLTrxType.Set_Sensitivity(Value: WideString);
begin
  SetAttribute('Sensitivity', Value);
end;

function TXMLTrxType.Get_FreqWork: WideString;
begin
  Result := AttributeNodes['FreqWork'].Text;
end;

procedure TXMLTrxType.Set_FreqWork(Value: WideString);
begin
  SetAttribute('FreqWork', Value);
end;

{ TXMLScenarioType }

procedure TXMLScenarioType.AfterConstruction;
begin
  RegisterChildNode('stations', TXMLStationsType);
  inherited;
end;

function TXMLScenarioType.Get_Id: WideString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLScenarioType.Set_Id(Value: WideString);
begin
  SetAttribute('id', Value);
end;

function TXMLScenarioType.Get_Name: WideString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLScenarioType.Set_Name(Value: WideString);
begin
  SetAttribute('Name', Value);
end;

function TXMLScenarioType.Get_Comments: WideString;
begin
  Result := AttributeNodes['Comments'].Text;
end;

procedure TXMLScenarioType.Set_Comments(Value: WideString);
begin
  SetAttribute('Comments', Value);
end;

function TXMLScenarioType.Get_OptimisedRegion_id: WideString;
begin
  Result := AttributeNodes['OptimisedRegion_id'].Text;
end;

procedure TXMLScenarioType.Set_OptimisedRegion_id(Value: WideString);
begin
  SetAttribute('OptimisedRegion_id', Value);
end;

function TXMLScenarioType.Get_Active: WideString;
begin
  Result := AttributeNodes['Active'].Text;
end;

procedure TXMLScenarioType.Set_Active(Value: WideString);
begin
  SetAttribute('Active', Value);
end;

function TXMLScenarioType.Get_Stations: IXMLStationsType;
begin
  Result := ChildNodes['stations'] as IXMLStationsType;
end;

{ TXMLScenarioTypeList }

function TXMLScenarioTypeList.Add: IXMLScenarioType;
begin
  Result := AddItem(-1) as IXMLScenarioType;
end;

function TXMLScenarioTypeList.Insert(const Index: Integer): IXMLScenarioType;
begin
  Result := AddItem(Index) as IXMLScenarioType;
end;
function TXMLScenarioTypeList.Get_Item(Index: Integer): IXMLScenarioType;
begin
  Result := List[Index] as IXMLScenarioType;
end;

{ TXMLStationsType }

procedure TXMLStationsType.AfterConstruction;
begin
  RegisterChildNode('site', TXMLSiteType);
  ItemTag := 'site';
  ItemInterface := IXMLSiteType;
  inherited;
end;

function TXMLStationsType.Get_Expanded: WideString;
begin
  Result := AttributeNodes['Expanded'].Text;
end;

procedure TXMLStationsType.Set_Expanded(Value: WideString);
begin
  SetAttribute('Expanded', Value);
end;

function TXMLStationsType.Get_Site(Index: Integer): IXMLSiteType;
begin
  Result := List[Index] as IXMLSiteType;
end;

function TXMLStationsType.Add: IXMLSiteType;
begin
  Result := AddItem(-1) as IXMLSiteType;
end;

function TXMLStationsType.Insert(const Index: Integer): IXMLSiteType;
begin
  Result := AddItem(Index) as IXMLSiteType;
end;

{ TXMLSiteType }

procedure TXMLSiteType.AfterConstruction;
begin
  RegisterChildNode('sector', TXMLSectorType);
  ItemTag := 'sector';
  ItemInterface := IXMLSectorType;
  inherited;
end;

function TXMLSiteType.Get_Id: WideString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLSiteType.Set_Id(Value: WideString);
begin
  SetAttribute('id', Value);
end;

function TXMLSiteType.Get_Number: WideString;
begin
  Result := AttributeNodes['Number'].Text;
end;

procedure TXMLSiteType.Set_Number(Value: WideString);
begin
  SetAttribute('Number', Value);
end;

function TXMLSiteType.Get_Name: WideString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLSiteType.Set_Name(Value: WideString);
begin
  SetAttribute('Name', Value);
end;

function TXMLSiteType.Get_CurrStatus: WideString;
begin
  Result := AttributeNodes['CurrStatus'].Text;
end;

procedure TXMLSiteType.Set_CurrStatus(Value: WideString);
begin
  SetAttribute('CurrStatus', Value);
end;

function TXMLSiteType.Get_Address: WideString;
begin
  Result := AttributeNodes['Address'].Text;
end;

procedure TXMLSiteType.Set_Address(Value: WideString);
begin
  SetAttribute('Address', Value);
end;

function TXMLSiteType.Get_Building: WideString;
begin
  Result := AttributeNodes['Building'].Text;
end;

procedure TXMLSiteType.Set_Building(Value: WideString);
begin
  SetAttribute('Building', Value);
end;

function TXMLSiteType.Get_LAC: WideString;
begin
  Result := AttributeNodes['LAC'].Text;
end;

procedure TXMLSiteType.Set_LAC(Value: WideString);
begin
  SetAttribute('LAC', Value);
end;

function TXMLSiteType.Get_Radius: WideString;
begin
  Result := AttributeNodes['Radius'].Text;
end;

procedure TXMLSiteType.Set_Radius(Value: WideString);
begin
  SetAttribute('Radius', Value);
end;

function TXMLSiteType.Get_Radius_CIA: WideString;
begin
  Result := AttributeNodes['Radius_CIA'].Text;
end;

procedure TXMLSiteType.Set_Radius_CIA(Value: WideString);
begin
  SetAttribute('Radius_CIA', Value);
end;

function TXMLSiteType.Get_B: WideString;
begin
  Result := AttributeNodes['B'].Text;
end;

procedure TXMLSiteType.Set_B(Value: WideString);
begin
  SetAttribute('B', Value);
end;

function TXMLSiteType.Get_L: WideString;
begin
  Result := AttributeNodes['L'].Text;
end;

procedure TXMLSiteType.Set_L(Value: WideString);
begin
  SetAttribute('L', Value);
end;

function TXMLSiteType.Get_RelHeight: WideString;
begin
  Result := AttributeNodes['RelHeight'].Text;
end;

procedure TXMLSiteType.Set_RelHeight(Value: WideString);
begin
  SetAttribute('RelHeight', Value);
end;

function TXMLSiteType.Get_LocalName: WideString;
begin
  Result := AttributeNodes['LocalName'].Text;
end;

procedure TXMLSiteType.Set_LocalName(Value: WideString);
begin
  SetAttribute('LocalName', Value);
end;

function TXMLSiteType.Get_LocalHeight: WideString;
begin
  Result := AttributeNodes['LocalHeight'].Text;
end;

procedure TXMLSiteType.Set_LocalHeight(Value: WideString);
begin
  SetAttribute('LocalHeight', Value);
end;

function TXMLSiteType.Get_Comment: WideString;
begin
  Result := AttributeNodes['Comment'].Text;
end;

procedure TXMLSiteType.Set_Comment(Value: WideString);
begin
  SetAttribute('Comment', Value);
end;

function TXMLSiteType.Get_SectorLen: WideString;
begin
  Result := AttributeNodes['SectorLen'].Text;
end;

procedure TXMLSiteType.Set_SectorLen(Value: WideString);
begin
  SetAttribute('SectorLen', Value);
end;

function TXMLSiteType.Get_Sector(Index: Integer): IXMLSectorType;
begin
  Result := List[Index] as IXMLSectorType;
end;

function TXMLSiteType.Add: IXMLSectorType;
begin
  Result := AddItem(-1) as IXMLSectorType;
end;

function TXMLSiteType.Insert(const Index: Integer): IXMLSectorType;
begin
  Result := AddItem(Index) as IXMLSectorType;
end;

{ TXMLSectorType }

procedure TXMLSectorType.AfterConstruction;
begin
  RegisterChildNode('ANTENNA', TXMLANTENNAType);
  ItemTag := 'ANTENNA';
  ItemInterface := IXMLANTENNAType;
  inherited;
end;

function TXMLSectorType.Get_Id: WideString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLSectorType.Set_Id(Value: WideString);
begin
  SetAttribute('id', Value);
end;

function TXMLSectorType.Get_Name: WideString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLSectorType.Set_Name(Value: WideString);
begin
  SetAttribute('Name', Value);
end;

function TXMLSectorType.Get_Cell_ID: WideString;
begin
  Result := AttributeNodes['Cell_ID'].Text;
end;

procedure TXMLSectorType.Set_Cell_ID(Value: WideString);
begin
  SetAttribute('Cell_ID', Value);
end;

function TXMLSectorType.Get_Color: WideString;
begin
  Result := AttributeNodes['Color'].Text;
end;

procedure TXMLSectorType.Set_Color(Value: WideString);
begin
  SetAttribute('Color', Value);
end;

function TXMLSectorType.Get_BSIC_id: WideString;
begin
  Result := AttributeNodes['BSIC_id'].Text;
end;

procedure TXMLSectorType.Set_BSIC_id(Value: WideString);
begin
  SetAttribute('BSIC_id', Value);
end;

function TXMLSectorType.Get_BCCH: WideString;
begin
  Result := AttributeNodes['BCCH'].Text;
end;

procedure TXMLSectorType.Set_BCCH(Value: WideString);
begin
  SetAttribute('BCCH', Value);
end;

function TXMLSectorType.Get_Hysteresis: WideString;
begin
  Result := AttributeNodes['Hysteresis'].Text;
end;

procedure TXMLSectorType.Set_Hysteresis(Value: WideString);
begin
  SetAttribute('Hysteresis', Value);
end;

function TXMLSectorType.Get_Forbidden_Channels: WideString;
begin
  Result := AttributeNodes['Forbidden_Channels'].Text;
end;

procedure TXMLSectorType.Set_Forbidden_Channels(Value: WideString);
begin
  SetAttribute('Forbidden_Channels', Value);
end;

function TXMLSectorType.Get_Allowed_Channels: WideString;
begin
  Result := AttributeNodes['Allowed_Channels'].Text;
end;

procedure TXMLSectorType.Set_Allowed_Channels(Value: WideString);
begin
  SetAttribute('Allowed_Channels', Value);
end;

function TXMLSectorType.Get_Fixed_Channels: WideString;
begin
  Result := AttributeNodes['Fixed_Channels'].Text;
end;

procedure TXMLSectorType.Set_Fixed_Channels(Value: WideString);
begin
  SetAttribute('Fixed_Channels', Value);
end;

function TXMLSectorType.Get_FreqRequiredCount: WideString;
begin
  Result := AttributeNodes['FreqRequiredCount'].Text;
end;

procedure TXMLSectorType.Set_FreqRequiredCount(Value: WideString);
begin
  SetAttribute('FreqRequiredCount', Value);
end;

function TXMLSectorType.Get_CalcRadius: WideString;
begin
  Result := AttributeNodes['CalcRadius'].Text;
end;

procedure TXMLSectorType.Set_CalcRadius(Value: WideString);
begin
  SetAttribute('CalcRadius', Value);
end;

function TXMLSectorType.Get_DeltaGrad: WideString;
begin
  Result := AttributeNodes['DeltaGrad'].Text;
end;

procedure TXMLSectorType.Set_DeltaGrad(Value: WideString);
begin
  SetAttribute('DeltaGrad', Value);
end;

function TXMLSectorType.Get_CalcMOdel_id: WideString;
begin
  Result := AttributeNodes['CalcMOdel_id'].Text;
end;

procedure TXMLSectorType.Set_CalcMOdel_id(Value: WideString);
begin
  SetAttribute('CalcMOdel_id', Value);
end;

function TXMLSectorType.Get_Tp_id: WideString;
begin
  Result := AttributeNodes['tp_id'].Text;
end;

procedure TXMLSectorType.Set_Tp_id(Value: WideString);
begin
  SetAttribute('tp_id', Value);
end;

function TXMLSectorType.Get_CalcMOdel_name: WideString;
begin
  Result := AttributeNodes['CalcMOdel_name'].Text;
end;

procedure TXMLSectorType.Set_CalcMOdel_name(Value: WideString);
begin
  SetAttribute('CalcMOdel_name', Value);
end;

function TXMLSectorType.Get_Tp_name: WideString;
begin
  Result := AttributeNodes['tp_name'].Text;
end;

procedure TXMLSectorType.Set_Tp_name(Value: WideString);
begin
  SetAttribute('tp_name', Value);
end;

function TXMLSectorType.Get_K0_open: WideString;
begin
  Result := AttributeNodes['K0_open'].Text;
end;

procedure TXMLSectorType.Set_K0_open(Value: WideString);
begin
  SetAttribute('K0_open', Value);
end;

function TXMLSectorType.Get_K0_closed: WideString;
begin
  Result := AttributeNodes['K0_closed'].Text;
end;

procedure TXMLSectorType.Set_K0_closed(Value: WideString);
begin
  SetAttribute('K0_closed', Value);
end;

function TXMLSectorType.Get_NetStandard_ID: WideString;
begin
  Result := AttributeNodes['NetStandard_ID'].Text;
end;

procedure TXMLSectorType.Set_NetStandard_ID(Value: WideString);
begin
  SetAttribute('NetStandard_ID', Value);
end;

function TXMLSectorType.Get_CombinerID: WideString;
begin
  Result := AttributeNodes['CombinerID'].Text;
end;

procedure TXMLSectorType.Set_CombinerID(Value: WideString);
begin
  SetAttribute('CombinerID', Value);
end;

function TXMLSectorType.Get_TrxID: WideString;
begin
  Result := AttributeNodes['TrxID'].Text;
end;

procedure TXMLSectorType.Set_TrxID(Value: WideString);
begin
  SetAttribute('TrxID', Value);
end;

function TXMLSectorType.Get_ANTENNA(Index: Integer): IXMLANTENNAType;
begin
  Result := List[Index] as IXMLANTENNAType;
end;

function TXMLSectorType.Add: IXMLANTENNAType;
begin
  Result := AddItem(-1) as IXMLANTENNAType;
end;

function TXMLSectorType.Insert(const Index: Integer): IXMLANTENNAType;
begin
  Result := AddItem(Index) as IXMLANTENNAType;
end;

{ TXMLANTENNAType }

procedure TXMLANTENNAType.AfterConstruction;
begin
  RegisterChildNode('DNA_vert', TXMLDNA_vertType);
  RegisterChildNode('DNA_horz', TXMLDNA_horzType);
  inherited;
end;

function TXMLANTENNAType.Get_Id: WideString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLANTENNAType.Set_Id(Value: WideString);
begin
  SetAttribute('id', Value);
end;

function TXMLANTENNAType.Get_Name: WideString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLANTENNAType.Set_Name(Value: WideString);
begin
  SetAttribute('Name', Value);
end;

function TXMLANTENNAType.Get_Azimuth: WideString;
begin
  Result := AttributeNodes['Azimuth'].Text;
end;

procedure TXMLANTENNAType.Set_Azimuth(Value: WideString);
begin
  SetAttribute('Azimuth', Value);
end;

function TXMLANTENNAType.Get_Height: WideString;
begin
  Result := AttributeNodes['Height'].Text;
end;

procedure TXMLANTENNAType.Set_Height(Value: WideString);
begin
  SetAttribute('Height', Value);
end;

function TXMLANTENNAType.Get_ServDistance: WideString;
begin
  Result := AttributeNodes['ServDistance'].Text;
end;

procedure TXMLANTENNAType.Set_ServDistance(Value: WideString);
begin
  SetAttribute('ServDistance', Value);
end;

function TXMLANTENNAType.Get_Checked: WideString;
begin
  Result := AttributeNodes['Checked'].Text;
end;

procedure TXMLANTENNAType.Set_Checked(Value: WideString);
begin
  SetAttribute('Checked', Value);
end;

function TXMLANTENNAType.Get_FeederLoss: WideString;
begin
  Result := AttributeNodes['FeederLoss'].Text;
end;

procedure TXMLANTENNAType.Set_FeederLoss(Value: WideString);
begin
  SetAttribute('FeederLoss', Value);
end;

function TXMLANTENNAType.Get_B: WideString;
begin
  Result := AttributeNodes['b'].Text;
end;

procedure TXMLANTENNAType.Set_B(Value: WideString);
begin
  SetAttribute('b', Value);
end;

function TXMLANTENNAType.Get_L: WideString;
begin
  Result := AttributeNodes['l'].Text;
end;

procedure TXMLANTENNAType.Set_L(Value: WideString);
begin
  SetAttribute('l', Value);
end;

function TXMLANTENNAType.Get_FixedPosition: WideString;
begin
  Result := AttributeNodes['FixedPosition'].Text;
end;

procedure TXMLANTENNAType.Set_FixedPosition(Value: WideString);
begin
  SetAttribute('FixedPosition', Value);
end;

function TXMLANTENNAType.Get_Model: WideString;
begin
  Result := AttributeNodes['Model'].Text;
end;

procedure TXMLANTENNAType.Set_Model(Value: WideString);
begin
  SetAttribute('Model', Value);
end;

function TXMLANTENNAType.Get_Producer: WideString;
begin
  Result := AttributeNodes['Producer'].Text;
end;

procedure TXMLANTENNAType.Set_Producer(Value: WideString);
begin
  SetAttribute('Producer', Value);
end;

function TXMLANTENNAType.Get_Gain: WideString;
begin
  Result := AttributeNodes['Gain'].Text;
end;

procedure TXMLANTENNAType.Set_Gain(Value: WideString);
begin
  SetAttribute('Gain', Value);
end;

function TXMLANTENNAType.Get_Freq: WideString;
begin
  Result := AttributeNodes['Freq'].Text;
end;

procedure TXMLANTENNAType.Set_Freq(Value: WideString);
begin
  SetAttribute('Freq', Value);
end;

function TXMLANTENNAType.Get_Slope: WideString;
begin
  Result := AttributeNodes['Slope'].Text;
end;

procedure TXMLANTENNAType.Set_Slope(Value: WideString);
begin
  SetAttribute('Slope', Value);
end;

function TXMLANTENNAType.Get_Rotate: WideString;
begin
  Result := AttributeNodes['Rotate'].Text;
end;

procedure TXMLANTENNAType.Set_Rotate(Value: WideString);
begin
  SetAttribute('Rotate', Value);
end;

function TXMLANTENNAType.Get_TiltElectrical: WideString;
begin
  Result := AttributeNodes['TiltElectrical'].Text;
end;

procedure TXMLANTENNAType.Set_TiltElectrical(Value: WideString);
begin
  SetAttribute('TiltElectrical', Value);
end;

function TXMLANTENNAType.Get_DNA_FileName: WideString;
begin
  Result := AttributeNodes['DNA_FileName'].Text;
end;

procedure TXMLANTENNAType.Set_DNA_FileName(Value: WideString);
begin
  SetAttribute('DNA_FileName', Value);
end;

function TXMLANTENNAType.Get_OMNI: WideString;
begin
  Result := AttributeNodes['OMNI'].Text;
end;

procedure TXMLANTENNAType.Set_OMNI(Value: WideString);
begin
  SetAttribute('OMNI', Value);
end;

function TXMLANTENNAType.Get_DNA_vert: IXMLDNA_vertType;
begin
  Result := ChildNodes['DNA_vert'] as IXMLDNA_vertType;
end;

function TXMLANTENNAType.Get_DNA_horz: IXMLDNA_horzType;
begin
  Result := ChildNodes['DNA_horz'] as IXMLDNA_horzType;
end;

{ TXMLDNA_vertType }

function TXMLDNA_vertType.Get_Value: WideString;
begin
  Result := AttributeNodes['value'].Text;
end;

procedure TXMLDNA_vertType.Set_Value(Value: WideString);
begin
  SetAttribute('value', Value);
end;

{ TXMLDNA_horzType }

function TXMLDNA_horzType.Get_Value: WideString;
begin
  Result := AttributeNodes['value'].Text;
end;

procedure TXMLDNA_horzType.Set_Value(Value: WideString);
begin
  SetAttribute('value', Value);
end;

end.