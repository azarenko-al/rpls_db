unit dm_Export_classes_link_to_MDB;

interface

uses
  SysUtils, Classes,  Forms, dxmdaset, DB, RxMemDS, ADODB, Variants,

  u_Config_File,

  u_ini_Excel_to_DB_params,

  u_func,

  u_files,

  u_const_db,

  dm_Main_Data,
//  dm_Onega_Data,

  u_import_classes,

  u_geo_convert_new,

 // dm_MDB,

  u_db_mdb,

  u_geo,

  u_db;

//  , u_compare_classes;

type
  TdmExport_classes_link_to_MDB = class(TDataModule)
    ADOConnection_out: TADOConnection;
    t_property: TADOTable;
    t_linkEnd: TADOTable;
    t_link: TADOTable;
    t_linkEnd_antenna: TADOTable;
    t_linkEndType: TADOTable;
    t_Folder: TADOTable;
    t_AntennaType: TADOTable;
  private
    function Add_LinkEnd(aLinkEnd_name: string; aProperty_ID: Integer; aIndex:
        integer; aLink: TLinkRaw): Integer;

//        aTower_Height: Double;


    procedure CreateMDB(aFileName: string);

    procedure Prepare_Property(aProject: TExcelProject);

  public
    class procedure Init;
    procedure SaveToExportMDB(aFileName: string; aProject: TExcelProject);

  end;

var
  dmExport_classes_link_to_MDB: TdmExport_classes_link_to_MDB;

implementation

uses dm_Config_mdb;

{$R *.dfm}


const
  FLD_E1_number = 'E1_number'; 


class procedure TdmExport_classes_link_to_MDB.Init;
begin
  if not Assigned(dmExport_classes_link_to_MDB) then
    Application.CreateForm(TdmExport_classes_link_to_MDB, dmExport_classes_link_to_MDB);
end;


// ---------------------------------------------------------------
procedure TdmExport_classes_link_to_MDB.SaveToExportMDB(aFileName: string;
    aProject: TExcelProject);
// ---------------------------------------------------------------
var
  I: Integer;
  oProperty: TPropertyRaw;
  oProperty1: TPropertyRaw;
  oProperty2: TPropertyRaw;
  oLink: TLinkRaw;
  sLinkEnd1_name: string;
  sLinkEnd2_name: string;
  sLinkEnd_name: string;

   wgs,CK_95, bl: TBLPoint;


  eLen_m: Double;
  iInd: Integer;

  oLinkEndType: TLinkEndType_Raw;
  sName: string;

begin
  CursorHourGlass();


  Assert(g_ConfigFile.Runtime.Project_ID>0);

//  dmOnega_Data.OpenProjectData (g_Excel_to_DB_params.ProjectID1);
//  dmMain_Data.OpenProjectData1 (g_ConfigFile.Runtime.Project_ID);

  //g_ConfigFile.

  //  GetPropInfo(aName: string; var aPos: TBLPoint): Boolean;



  CreateMDB(aFileName);


  Prepare_Property (aProject);


  with dmConfig_mdb.t_Equipment_to_LinkendType do
    while not EOF do
    begin
      oLinkEndType:=aProject.LinkEndTypeList.AddItem;

      oLinkEndType.Name:= FieldByName(FLD_NAME).AsString;

      oLinkEndType.Equipment:= FieldByName('Equipment').AsString;
      oLinkEndType.Band:= FieldByName('Band').AsInteger;

      Next;
    end;



  //aProject.LinkEndTypeList



  // ---------------------------------------------------------------
  db_AddRecord_(t_Folder,
   [
     FLD_NAME, 'Import',
     FLD_TYPE, 'LinkEndType'
    ] );

  db_AddRecord_(t_Folder,
   [
     FLD_NAME, 'Import',
     FLD_TYPE, 'ANTENNA_TYPE'
    ] );


  for i:=0 to aProject.LinkEndTypeList.Count-1 Do
  begin
    oLinkEndType := aProject.LinkEndTypeList[i];

    db_AddRecord_(t_LinkEndType,
     [
       FLD_NAME, oLinkEndType.Name,
       FLD_BAND, oLinkEndType.Band,
       FLD_FOLDER_ID, 1
      ] );


    oLinkEndType.DB_ID := t_LinkEndType[FLD_ID];
  end;

  //////////////////////////////////////////

  dmConfig_mdb.t_Equipment_to_ANTENNAType.First;
  with dmConfig_mdb.t_Equipment_to_ANTENNAType do
    while not EOF do
  begin
     db_AddRecord_(t_ANTENNAType,
         [
           FLD_NAME,     FieldValues[FLD_NAME],
           FLD_BAND,     FieldValues[FLD_BAND],
//           FLD_DIAMETER, TruncFloat(FieldBYName(FLD_DIAMETER).AsFloat, 1),
           FLD_DIAMETER, FieldBYName(FLD_DIAMETER).AsFloat,
           FLD_FOLDER_ID, 2
          ] );

    Next;
  end;

  //////////////////////////////////////////

  for i:=0 to aProject.LinkEndTypeList.Count-1 Do
  begin
    oLinkEndType := aProject.LinkEndTypeList[i];

    db_AddRecord_(t_LinkEndType,
     [
       FLD_NAME, oLinkEndType.Name,
       FLD_BAND, oLinkEndType.Band,
       FLD_FOLDER_ID, 1
      ] );
                
    oLinkEndType.DB_ID := t_LinkEndType[FLD_ID];
  end;


  // ---------------------------------------------------------------

  for i:=0 to aProject.PropertyList.Count-1 Do
  begin
    oProperty := aProject.PropertyList[i];

    Assert(oProperty.Name<>'');

{
    if dmOnega_Data.GetPropInfo(oProperty.Name, bl) then
      oProperty.BLPoint:=bl
    else
      Continue;
}

    wgs  := geo_Pulkovo42_to_WGS84(oProperty.BLPoint);
    CK_95:= geo_Pulkovo42_to_CK_95(oProperty.BLPoint);



    db_AddRecord_(t_property,
     [
      // FLD_ID,      i+1, // oProperty.Id,
       FLD_NAME,         oProperty.Name,
 //      FLD_NAME,    oProperty.Name,

       FLD_ADDRESS,      oProperty.Address,
       FLD_TOWER_HEIGHT, oProperty.TOWER_HEIGHT,

       FLD_LAT,     oProperty.BLPoint.B,
       FLD_LON,     oProperty.BLPoint.L,


        FLD_LAT_WGS,   wgs.b,
        FLD_LON_WGS,   wgs.l,

        FLD_LAT_CK95,  CK_95.b,
        FLD_LON_CK95,  CK_95.l


//
//       FLD_ADDRESS, oProperty.Address,
//       FLD_TOWER_HEIGHT, oProperty.TOWER_HEIGHT


//       FLD_LAT_STR, geo_FormatDegree(oProperty.BLPoint.B),
//       FLD_LON_STR, geo_FormatDegree(oProperty.BLPoint.L)


      ] );

    oProperty.Runtime.DB_ID := t_property[FLD_ID];
  end;



  for i:=0 to aProject.LinkList.Count-1 Do
  begin
    oLink := aProject.LinkList[i];

    if (oLink.Property1_ref.Runtime.DB_ID=0) or
       (oLink.Property2_ref.Runtime.DB_ID=0)
    then
      continue;


    if oLink.Name='' then
      oLink.Name:=oLink.Sites[1].Name + '<->'+ oLink.Sites[2].Name;


//    sLinkEnd1_name:=oLink.Sites[1].LinkEnd_Name; // + '->'+ oLink.Sites[2].Name;
//    sLinkEnd2_name:=oLink.Sites[2].LinkEnd_Name; // Name + '->'+ oLink.Sites[1].Name;

    sLinkEnd1_name:=oLink.Sites[1].Name + '->'+ oLink.Sites[2].Name;
    sLinkEnd2_name:=oLink.Sites[2].Name + '->'+ oLink.Sites[1].Name;

    
    oLinkEndType:=aProject.LinkEndTypeList.FindByEquipmentBand (oLink.LinkendType_name, oLink.Band);

    if Assigned (oLinkEndType) then
    begin
      oLink.LinkEndType_DB_ID:=oLinkEndType.DB_ID;
//      oLink.AntennaType_DB_ID:=oLinkEndType.DB_ID;
    end;
            


//    v:=


//      t_ANTENNAType

//    sLinkEnd1_name:=oLink.Sites[1].LIn Name + '->'+ oLink.Sites[2].Name;
//    sLinkEnd2_name:=oLink.Sites[2].Name + '->'+ oLink.Sites[1].Name;





    oLink.Runtime.LinkEnd[1].DB_ID:=Add_LinkEnd (sLinkEnd1_name, oLink.Property1_ref.Runtime.DB_ID, 1, oLink);
    oLink.Runtime.LinkEnd[2].DB_ID:=Add_LinkEnd (sLinkEnd2_name, oLink.Property2_ref.Runtime.DB_ID, 2, oLink);


    eLen_m:= geo_Distance_m(oLink.Property1_ref.BLPoint, oLink.Property2_ref.BLPoint);

    Assert(oLink.Name<>'');


    if not VarIsNull (t_Link.Lookup(FLD_NAME, oLink.Name, FLD_ID)) then
    begin
      iInd:=1;

      repeat
        sName:= Format('%s (%d)', [oLink.Name, iInd]);
        Inc(iInd);

      until VarIsNull (t_Link.Lookup(FLD_NAME, sName, FLD_ID));

      oLink.Name:=sName;

    end;



    db_AddRecord_(t_Link,
     [
      // FLD_ID,      i+1, // oProperty.Id,
       FLD_NAME,    oLink.Name  ,

       FLD_LinkEnd1_ID,  oLink.Runtime.LinkEnd[1].DB_ID,
       FLD_LinkEnd2_ID,  oLink.Runtime.LinkEnd[2].DB_ID,

       FLD_LENGTH,  eLen_m

      ] );

  end;




  ADOConnection_out.Close;

  CursorDefault();

end;


// ---------------------------------------------------------------
procedure TdmExport_classes_link_to_MDB.CreateMDB(aFileName: string);
// ---------------------------------------------------------------

    // ---------------------------------------------------------------
    procedure DoAdd_Folder;
    // ---------------------------------------------------------------
    var
      sSQL: string;
    begin
      sSQL :=mdb_MakeCreateSQL (TBL_Folder,
         [
          db_Field(FLD_ID,      ftAutoInc),
          db_Field(FLD_NAME,    ftString, 100),


          db_Field(FLD_project_ID, ftInteger),
          db_Field(FLD_parent_ID, ftInteger),
          db_Field(FLD_type,      ftString)
         ]);

      ADOConnection_out.Execute(sSQL);
    end;

    // ---------------------------------------------------------------
    procedure DoAdd_LinkendType;
    // ---------------------------------------------------------------
    var
      sSQL: string;
    begin
      sSQL :=mdb_MakeCreateSQL (TBL_LinkendType,
         [
          db_Field(FLD_ID,      ftAutoInc),
          db_Field(FLD_NAME,    ftString, 100),

          db_Field(FLD_band,      ftString),
          db_Field(FLD_FOLDER_ID, ftInteger)

         ]);

      ADOConnection_out.Execute(sSQL);
    end;

    // ---------------------------------------------------------------
    procedure DoAdd_AntennaType;
    // ---------------------------------------------------------------
    var
      sSQL: string;
    begin
      sSQL :=mdb_MakeCreateSQL (TBL_AntennaType,
         [
          db_Field(FLD_ID,      ftAutoInc),
          db_Field(FLD_NAME,    ftString, 100),

          db_Field(FLD_diameter,  ftFloat),

          db_Field(FLD_band,      ftInteger),
          db_Field(FLD_FOLDER_ID, ftInteger)

         ]);

      ADOConnection_out.Execute(sSQL);
    end;


    // ---------------------------------------------------------------
    procedure DoAdd_property;
    // ---------------------------------------------------------------
    var
      sSQL: string;
    begin
      sSQL :=mdb_MakeCreateSQL (TBL_property,
         [
          db_Field(FLD_ID,      ftAutoInc),
          db_Field(FLD_NAME,    ftString, 200) ,

          db_Field(FLD_ADDRESS,      ftString, 200),
          db_Field(FLD_TOWER_HEIGHT, ftFloat),


          db_Field(FLD_LAT,     ftFloat),
          db_Field(FLD_LON,     ftFloat),

          db_Field(FLD_LAT_WGS,     ftFloat),
          db_Field(FLD_LON_WGS,     ftFloat),

          db_Field(FLD_LAT_CK95,     ftFloat),
          db_Field(FLD_LON_CK95,     ftFloat)


//          db_Field(FLD_LAT_WGS,     ftFloat),
 //         db_Field(FLD_LON_WGS,     ftFloat)

         ]);

      ADOConnection_out.Execute(sSQL);
      ADOConnection_out.Execute('CREATE UNIQUE INDEX property_name ON property (name) WITH DISALLOW NULL');

{
 dbs.Execute "CREATE UNIQUE INDEX CustID " _
        & "ON Customers (CustomerID) " _
        & "WITH DISALLOW NULL;"
}

    end;

    // ---------------------------------------------------------------
    procedure DoAdd_link;
    // ---------------------------------------------------------------
    var
      sSQL: string;
    begin
      sSQL :=mdb_MakeCreateSQL (TBL_link,
         [
          db_Field(FLD_ID,      ftAutoInc),
          db_Field(FLD_NAME,    ftString, 200),

          db_Field(FLD_LinkEnd1_ID,  ftInteger),
          db_Field(FLD_LinkEnd2_ID,  ftInteger),

          db_Field(FLD_LENGTH,  ftFloat)

         ]);

      ADOConnection_out.Execute(sSQL);

      ADOConnection_out.Execute('CREATE UNIQUE INDEX link_name ON link (name) WITH DISALLOW NULL');


{
 dbs.Execute "CREATE UNIQUE INDEX CustID ON Customers (CustomerID) WITH DISALLOW NULL;"
}
    end;


    // ---------------------------------------------------------------
    procedure DoAdd_linkEnd;
    // ---------------------------------------------------------------
    var
      sSQL: string;
    begin
      sSQL :=mdb_MakeCreateSQL (TBL_linkEnd,
         [
          db_Field(FLD_ID,      ftAutoInc),
          db_Field(FLD_NAME,    ftString, 200),

          db_Field(FLD_property_ID,     ftInteger),

      //    db_Field(FLD_LINKENDTYPE_NAME,    ftString),

          db_Field(FLD_Power_dBm,       ftFloat),

          db_Field(FLD_Tx_Freq_MHz,     ftFloat),
          db_Field(FLD_Rx_Freq_MHz,     ftFloat),

          db_Field(FLD_Threshold_BER_3, ftFloat),
          db_Field(FLD_Threshold_BER_6, ftFloat),

      //    db_Field(FLD_Bitrate_Mbps,    ftFloat),

          db_Field(FLD_Modulation_TYPE, ftString),
          db_Field(FLD_band,          ftString),

          db_Field(FLD_LinkendType_name, ftString),
          db_Field(FLD_LinkendType_ID,   ftInteger),

          db_Field(FLD_E1_number,     ftInteger),

          db_Field(FLD_BitRate_Mbps,     ftFloat) ,

          db_Field(FLD_channel_width_MHz,   ftFloat) ,

          db_Field(FLD_equipment_type,   ftInteger)





   {
     if Eq(sField, FLD_Modulation)  then oLink.Modulation := v else
        if Eq(sField, FLD_E1_number)  then oLink.E1_number := v else


//        if Eq(sField, FLD_E1_number)  then oLink.E1_number := v else
        if Eq(sField, FLD_equipment)  then oLink.equipment := v else
        if Eq(sField, FLD_band)       then oLink.band := v else
     }

         ]);

      ADOConnection_out.Execute(sSQL);
      ADOConnection_out.Execute('CREATE UNIQUE INDEX linkend_name ON linkend (name) WITH DISALLOW NULL');

    end;

  // ---------------------------------------------------------------
  procedure DoAdd_linkEnd_antenna;
  // ---------------------------------------------------------------
    var
      sSQL: string;
    begin
      sSQL :=mdb_MakeCreateSQL (TBL_linkEnd_antenna,
         [
          db_Field(FLD_ID,      ftAutoInc),
          db_Field(FLD_NAME,    ftString, 200),

          db_Field(FLD_linkEnd_ID,  ftInteger),
          db_Field(FLD_AntennaType_ID,  ftInteger),
                        
          db_Field(FLD_HEIGHT,     ftFloat),

          db_Field(FLD_GAIN,       ftFloat),
          db_Field(FLD_Diameter,   ftFloat)

         ]);

      ADOConnection_out.Execute(sSQL);
    end;


  // ---------------------------------------------------------------


begin
  mdb_CreateFile (aFileName);

  mdb_OpenConnection(ADOConnection_out, aFileName);

  DoAdd_Folder();
  DoAdd_property();
  DoAdd_Link();
  DoAdd_linkEnd();
  DoAdd_linkEnd_antenna();
  DoAdd_LinkendType();
  DoAdd_AntennaType();


  db_TableOpen1(t_Folder, TBL_Folder);

  db_TableOpen1(t_property, TBL_property);
  db_TableOpen1(t_link, TBL_link);
  db_TableOpen1(t_linkEnd, TBL_linkEnd);
  db_TableOpen1(t_linkEnd_antenna, TBL_linkEnd_antenna);

  db_TableOpen1(t_linkEndType, TBL_linkEndType);
  db_TableOpen1(t_AntennaType, TBL_AntennaType);


end;

// ---------------------------------------------------------------
function TdmExport_classes_link_to_MDB.Add_LinkEnd(aLinkEnd_name: string;
    aProperty_ID: Integer; aIndex: integer; aLink: TLinkRaw): Integer;
// ---------------------------------------------------------------
var
  iAntennaType_DB_ID: Integer;
  iInd: Integer;
  sName: string;

begin
  Assert(aLinkEnd_name<>'');

  Assert(g_ConfigFile.Freq_MHz>0);
  Assert(g_ConfigFile.Power_dBm>0);


  if t_ANTENNAType.Locate( FLD_BAND +';'+FLD_DIAMETER, VarArrayOf([aLink.band,  aLink.Sites[aIndex].Diameter]), []) then 
    iAntennaType_DB_ID:=t_ANTENNAType[FLD_ID]
  else
    iAntennaType_DB_ID:=0;

  // ---------------------------------------------------------------
  if not VarIsNull (t_linkEnd.Lookup(FLD_NAME, aLinkEnd_name, FLD_ID)) then
  begin
    iInd:=1;

    repeat
      sName:= Format('%s (%d)', [aLinkEnd_name, iInd]);
      Inc(iInd);

    until VarIsNull (t_linkEnd.Lookup(FLD_NAME, sName, FLD_ID));

    aLinkEnd_name:=sName;

  end;



  db_AddRecord_(t_linkEnd,
     [
        FLD_NAME,    aLinkEnd_name,

        FLD_property_ID,     aProperty_ID,

        FLD_Power_dBm,       aLink.Power, //  g_ConfigFile.Power_dBm,


        FLD_Modulation_TYPE,       aLink.Modulation,

        FLD_LINKENDTYPE_NAME,      aLink.LinkendType_name,

        FLD_LINKENDTYPE_ID,      aLink.LinkEndType_DB_ID,

        FLD_equipment_type, IIF (aLink.LinkEndType_DB_ID > 0, 0, 1),


//        FLD_equipment_type

       // oLink.Sites[2].LinkEndType_DB_ID


        FLD_Tx_Freq_MHz, IIF(aIndex=1, aLink.tx_mhz, aLink.rx_mhz),
        FLD_Rx_Freq_MHz, IIF(aIndex=1, aLink.rx_mhz, aLink.tx_mhz),

       // FLD_Channel_Spacing, aLink.Channel_Spacing,

        FLD_channel_width_MHz, aLink.channel_width_MHz,


        FLD_BAND, aLink.Band,

        FLD_Threshold_BER_3, g_ConfigFile.Threshold_BER_3,
        FLD_Threshold_BER_6, g_ConfigFile.Threshold_BER_3,

        FLD_LinkendType_name, aLink.LinkendType_name,
        FLD_LinkendType_ID, IIF_NULL(aLink.LinkendType_DB_ID),

//        FLD_E1_number, aLink.E1_number,

        FLD_BitRate_Mbps,   aLink.CAPACITY




//        FLD_LinkEndType_name, aLink.

//        )   then oLink.Sites[2].tx_mhz     := v else
//        if Eq(sField, FLD_SITE2_rx_mhz)   then oLink.Sites[2].rx_mhz     := v else



//        site2_tx_mhz


//        FLD_Bitrate_Mbps,    g_ConfigFile.Bitrate_Mbps

      ] );

    Result:=t_linkEnd[FLD_ID];


//    if aTower_Height=0 then
//      aTower_Height:=g_ConfigFile.Height;



    db_AddRecord_(t_linkEnd_antenna,
     [
        FLD_NAME,    'Antenna',

        FLD_LinkEnd_ID,    result,

        FLD_HEIGHT,    aLink.Sites[aIndex].TOWER_HEIGHT, // ]  aTower_Height,
        FLD_Diameter,  aLink.Sites[aIndex].Diameter,  // g_ConfigFile.Diameter

        FLD_AntennaType_ID, IIF_NULL(iAntennaType_DB_ID),


        FLD_GAIN,      g_ConfigFile.Gain

      ] );



  // TODO -cMM: TdmExport_classes_link_to_MDB.Add_LinkEnd default body inserted
end;


// ---------------------------------------------------------------
procedure TdmExport_classes_link_to_MDB.Prepare_Property(aProject:
    TExcelProject);
// ---------------------------------------------------------------
var
  I: Integer;
  oProperty: TPropertyRaw;
  oProperty1: TPropertyRaw;
  oProperty2: TPropertyRaw;
  oLink: TLinkRaw;
  sLinkEnd1_name: string;
  sLinkEnd2_name: string;
  sLinkEnd_name: string;

  bl: TBLPoint;
  eLen_m: Double;
  iInd: Integer;

  oLinkEndType: TLinkEndType_Raw;
  sName: string;


begin
  // TODO -cMM: TdmExport_classes_link_to_MDB.Prepare_Property default body inserted
  for i:=0 to aProject.LinkList.Count-1 Do
  begin
    oLink := aProject.LinkList[i];

Assert(oLink.Sites[1].Name<>'', 'oLink.Sites[1].Name empty');
Assert(oLink.Sites[2].Name<>'', 'oLink.Sites[2].Name empty');


    oProperty1:=aProject.PropertyList.FindByName(oLink.Sites[1].Name);
    if not Assigned(oProperty1) then
    begin
      oProperty1:=aProject.PropertyList.AddItem;
      oProperty1.Name         :=oLink.Sites[1].Name;
      Assert(oProperty1.Name<>'');
                       
      oProperty1.Address      :=oLink.Sites[1].address;
      oProperty1.TOWER_HEIGHT :=oLink.Sites[1].TOWER_HEIGHT;

      oProperty1.BLPoint.B :=oLink.Sites[1].BLPoint.B;
      oProperty1.BLPoint.L :=oLink.Sites[1].BLPoint.L;

    end;


    oProperty2:=aProject.PropertyList.FindByName(oLink.Sites[2].Name);
    if not Assigned(oProperty2) then
    begin
      oProperty2:=aProject.PropertyList.AddItem;
      oProperty2.Name         :=oLink.Sites[2].Name;

      Assert(oProperty2.Name<>'');

      oProperty2.Address      :=oLink.Sites[2].address;
      oProperty2.TOWER_HEIGHT :=oLink.Sites[2].TOWER_HEIGHT;

      oProperty2.BLPoint.B :=oLink.Sites[2].BLPoint.B;
      oProperty2.BLPoint.L :=oLink.Sites[2].BLPoint.L;

    end;

    oLink.Property1_ref:=oProperty1;
    oLink.Property2_ref:=oProperty2;

  end;



  for i:=0 to aProject.PropertyList.Count-1 Do
  begin
    oProperty := aProject.PropertyList[i];


    case g_ConfigFile.CoordSys of
        csPulkovo_CK42: ;
        csPulkovo_CK95: oProperty.BLPoint:= geo_Pulkovo_CK_95_to_CK_42(oProperty.BLPoint);
        csWGS:          oProperty.BLPoint:= geo_WGS84_to_Pulkovo42(oProperty.BLPoint);
//        csGOST:          oProperty.BLPoint:= geo_WGS84_to_ Pulkovo42(oProperty.BLPoint);
      else
        raise exception.create('')
      end;

  end;

end;



end.


