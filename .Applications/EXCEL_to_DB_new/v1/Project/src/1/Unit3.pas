unit u_xml_classes;

interface

uses Classes,SysUtils,

  u_crc32,

  u_func,
  u_Geo;

type
  TxAntennaType = class;
  TxAntennaList = class;
  TxAntennaTypeList = class;
  TxCell = class;
  TxCellList = class;
  TxTrxList = class;
  TxProject= class;
  TxProperty= class;
  TxPropertyList= class;
  TxReliefMatrix = class;
  TxReliefMatrixList = class;
  TxSiteList = class;
  TxScenarioList =class;

  TxCombiner = class;
  TxCombinerList = class;


  TxProject = class
  public
    Name     : string;
    FileName : string; //xml file
  public
    ScenarioList: TxScenarioList;
    ReliefMatrixList: TxReliefMatrixList;
    AntennaTypeList: TxAntennaTypeList;

    TrxList: TxTrxList;
    CombinerList: TxCombinerList;


    constructor Create;
    destructor Destroy; override;

    procedure Clear;

  end;


  // -------------------------------------------------------------------
  TxScenario = class(TCollectionItem)
  // -------------------------------------------------------------------
  public
    Name : string;
  public
    PropertyList: TxPropertyList;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
  end;


 // -------------------------------------------------------------------
  TxAntennaType = class(TCollectionItem)
  // -------------------------------------------------------------------
  public
    DB_ID : Integer;

    Name: string;

    GAIN_dB  : Double;
    FREQ_MHz : Double;

    TILT_TYPE : Integer; //elect, mech

    VERT_WIDTH : Double;
    HORZ_WIDTH : Double;

    Vert_mask : string;
    Horz_mask : string;

    function Validate(var aError: string): Boolean;
    function GetCRC32: LongWord;

  end;

  // -------------------------------------------------------------------
  TxAntenna = class(TCollectionItem)
  // -------------------------------------------------------------------
  public
    db_id : Integer;

    Name: string;
    Height: Double;
    TILT : Double;

    Loss_dB : Double;
//    FeederLoss_dBm : Double;

//    GAIN_dB : Double;

    Width : Double;
    DIAMETER : Double;

    AZIMUTH : Double;

    BLPoint: TBLPoint;
    LOCATION_TYPE : Integer;

  public
    AntennaType_Ref : TxAntennaType;

    function GetCRC32: LongWord;
  end;

  // -------------------------------------------------------------------
  TxSite = class(TCollectionItem)
  // -------------------------------------------------------------------
  public
    DB_ID : Integer;
    DB_Name : string;
          
    Name: string;
    NUMBER : Integer;
    COMMENT1 : string;

  //  CALC_RADIUS_KM : Double;
  //  LAC : Integer;
  public
    CellList: TxCellList;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;

    function Validate(var aError: string): Boolean;
  end;

  // -------------------------------------------------------------------
  TxTrx = class(TCollectionItem)
  // -------------------------------------------------------------------
  public
    ID : Integer;
    Name: string;

    POWER_W          : Double;// ,           oTrx.Params.Power),
    Sensitivity_dBm  : integer;// ,           oTrx.Params.Sensitivity),
    FREQ_MHz         : integer; //,            oTrx.Params.FreqWork),
  end;

  // -------------------------------------------------------------------
  TxCombiner = class(TCollectionItem)
  // -------------------------------------------------------------------
  public
    ID : Integer;
    Name: string;

    Loss_dB : Double; // ,oTrx.Params.Sensitivity),
  end;



  // -------------------------------------------------------------------
  TxCell = class(TCollectionItem)
  // -------------------------------------------------------------------
  public
    DB_ID : Integer;
    DB_Name : string;

    Color : Integer;

    NAME   : string;     //        Trim(oSectorList[i].Name_)),
    CELLID : Integer; //CELL ID /CID

//    SITE_ID,         aSiteID),
  //  SITE_NAME    : string;       aSite.Name_),

   // TRX_ID,          IIF(oTrx.DB_ID>0, oTrx.DB_ID, null)  ),
  //  TRX_NAME,        oTrx.Name_ ),

    POWER_W          : Double;// ,           oTrx.Params.Power),
    Sensitivity_dBm  : Double;// ,           oTrx.Params.Sensitivity),
    FREQ_MHz         : Double;//,            oTrx.Params.FreqWork),

    COMBINER_LOSS_dB : Double;// ,   oCombiner.Params.Losses),


(*    Color            : Integer;//,           oSectorList[i].Params.Color),
    CALC_RADIUS_KM   : Double;//,     aSite.Params.Radius),
//    CALC_MODEL_ID,   oSectorList[i].Params.CalcModel_ID),
  //  Calc_MOdel_name, oSectorList[i].Params.CalcModel_Name),
    K0               : Double;  //             oSectorList[i].Params.Calibrate.k0),
    K0_OPEN          : Double;// ,         oSectorList[i].Params.Calibrate.k0_open),
    K0_CLOSED        : Double;// ,       oSectorList[i].Params.Calibrate.k0_closed),
*)
 //   CELLID,          oSectorList[i].Params.Cell_ID),
   // CELL_LAYER_NAME, oNetStandard.Name_),


(*    COMBINER_LOSS : Double;// ,   oCombiner.Params.Losses),
 //   BCCH,            oSectorList[i].Params.BCCH),
  //  HYSTERESIS,      oSectorList[i].Params.Hysteresis),
   // BSIC_ID,         oSectorList[i].Params.BSIC_id),

    FREQ_REQ_COUNT : Integer;//,  oSectorList[i].Params.FreqRequiredCount),
    FREQ_FIXED     : string;//,      IntArrayToString(oSectorList[i].Params.FreqFixedArr)),
    FREQ_ALLOW     : string;//,      IntArrayToString(oSectorList[i].Params.FreqAllowedArr)),
    FREQ_FORBIDDEN : string;//,  IntArrayToString(oSectorList[i].Params.FreqForbiddenArr))
*)


  {  Name: string;

    NUMBER : Integer;

    COMMENT : string;

    CALC_RADIUS : Double;
    LAC : Integer;}

    //----------------------------------------
//    CellList: TxCellList;

  public
    AntennaList: TxAntennaList;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;

    function GetCRC32: LongWord;
    function Validate(var aError: string): Boolean;
  end;

   // -------------------------------------------------------------------
  TxProperty = class(TCollectionItem)
  // -------------------------------------------------------------------
  public
    DB_ID : Integer;
    // -------------------------

    Name: string;
    Address : string;
    Building: string;

    Comment2 : string;

    GROUND_HEIGHT : Integer;
    BLPoint: TBLPoint;

  public
  //  LinkEndList: TxLinkEndList;
    SiteList: TxSiteList;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;

    function GetCRC32: LongWord;
  end;


  // -------------------------------------------------------------------
  TxReliefMatrix = class(TCollectionItem)
  // -------------------------------------------------------------------
  public
    DB_ID : Integer;

    Name: string;

    FileName : string;
    STEP : Integer;
    ZONENUM : Integer;
    
 //   FILESIZE : Integer;


  {  Height: Double;
    TILT : Integer;

    ANTENNATYPE_ID : Integer;
    SERVICE_DISTANCE : Double;
    Loss : Double;
    HEEL : Double;}
  end;


  TxPropertyList = class(TCollection)
  private
    function FindIndexByName(aName: string): integer;
    function GetItems(Index: Integer): TxProperty;
  public
    constructor Create;
    function AddItem: TxProperty;
    function FindByName(aName: string): TxProperty;

    property Items[Index: Integer]: TxProperty read GetItems; default;
  end;


  TxAntennaTypeList = class(TCollection)
  private
    function GetItems(Index: Integer): TxAntennaType;
  public
    constructor Create;
    function AddItem: TxAntennaType;
    function FindByName(aName: string): TxAntennaType;

    property Items[Index: Integer]: TxAntennaType read GetItems; default;
  end;

  TxSiteList = class(TCollection)
  private
    function GetItems(Index: Integer): TxSite;
  public
    constructor Create;
    function AddItem: TxSite;
  //  function FindByName(aName: string): integer;

    property Items[Index: Integer]: TxSite read GetItems; default;
  end;


  TxCellList = class(TCollection)
  private
    function GetItems(Index: Integer): TxCell;
  public
    constructor Create;
    function AddItem: TxCell;
  //  function FindByName(aName: string): integer;

    property Items[Index: Integer]: TxCell read GetItems; default;
  end;

  TxReliefMatrixList = class(TCollection)
  private
    function GetItems(Index: Integer): TxReliefMatrix;
  public
    constructor Create;
    function AddItem: TxReliefMatrix;
    property Items[Index: Integer]: TxReliefMatrix read GetItems; default;
  end;


  TxScenarioList = class(TCollection)
  private
    function GetItems(Index: Integer): TxScenario;
  public
    constructor Create;
    function AddItem: TxScenario;
    property Items[Index: Integer]: TxScenario read GetItems; default;
  end;

  TxAntennaList = class(TCollection)
  private
//    function FindByName(aName: string): integer;
    function GetItems(Index: Integer): TxAntenna;
  public
    constructor Create;
    function AddItem: TxAntenna;
    property Items[Index: Integer]: TxAntenna read GetItems; default;
  end;


  TxTrxList = class(TCollection)
  private
    function GetItems(Index: Integer): TxTRX;
  public
    constructor Create;
    function AddItem: TxTRX;
    function FindByID(aID: integer): TxTRX;
    property Items[Index: Integer]: TxTRX read GetItems; default;
  end;


  TxCombinerList = class(TCollection)
  private
    function GetItems(Index: Integer): TxCombiner;
  public
    constructor Create;
    function AddItem: TxCombiner;
    function FindByID(aID: integer): TxCombiner;
    property Items[Index: Integer]: TxCombiner read GetItems; default;
  end;


implementation



procedure TxProject.Clear;
begin
  ScenarioList.Clear;
  ReliefMatrixList.Clear;
  AntennaTypeList.Clear;
  TrxList.Clear;
  CombinerList.Clear;
end;                                               


constructor TxAntennaList.Create;
begin
  inherited Create (TxAntenna);
end;

function TxAntennaList.AddItem: TxAntenna;
begin
  Result := TxAntenna(inherited Add);
end;

function TxAntennaList.GetItems(Index: Integer): TxAntenna;
begin
  Result := TxAntenna(inherited Items[Index]);
end;


constructor TxProperty.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  SiteList := TxSiteList.Create();
end;

destructor TxProperty.Destroy;
begin
  FreeAndNil(SiteList);
  inherited Destroy;
end;

constructor TxPropertyList.Create;
begin
  inherited Create (TxProperty);
end;

function TxPropertyList.AddItem: TxProperty;
begin
  Result := TxProperty(inherited Add);
end;


function TxPropertyList.FindIndexByName(aName: string): integer;
var I: Integer;
begin
  Result := -1;

  for I := 0 to Count - 1 do
    if Eq(Items[i].Name, aName) then
    begin
      Result := i;
      Break;
    end;
end;


function TxPropertyList.FindByName(aName: string): TxProperty;
var I: Integer;
begin
  i:=FindIndexByName(aName);
  if i>=0 then
    Result := Items[i]
  else
    Result := nil;
end;



function TxPropertyList.GetItems(Index: Integer): TxProperty;
begin
  Result := TxProperty(inherited Items[Index]);
end;

constructor TxAntennaTypeList.Create;
begin
  inherited Create (TxAntennaType);
end;

function TxAntennaTypeList.AddItem: TxAntennaType;
begin
  Result := TxAntennaType(inherited Add);
end;

function TxAntennaTypeList.FindByName(aName: string): TxAntennaType;
var I: Integer;
begin
 Result := nil;

  for I := 0 to Count - 1 do
    if Eq(Items[i].Name, aName) then
    begin
      Result := Items[i];
      Break;
    end;
end;

function TxAntennaTypeList.GetItems(Index: Integer): TxAntennaType;
begin
  Result := TxAntennaType(inherited Items[Index]);
end;
                 

constructor TxSiteList.Create;
begin
  inherited Create (TxSite);
end;

function TxSiteList.AddItem: TxSite;
begin
  Result := TxSite(inherited Add);
end;

function TxSiteList.GetItems(Index: Integer): TxSite;
begin
  Result := TxSite(inherited Items[Index]);
end;



constructor TxCellList.Create;
begin
  inherited Create (TxCell);
end;

function TxCellList.AddItem: TxCell;
begin
  Result := TxCell(inherited Add);
end;

function TxCellList.GetItems(Index: Integer): TxCell;
begin
  Result := TxCell(inherited Items[Index]);
end;

constructor TxSite.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  CellList := TxCellList.Create();
end;

destructor TxSite.Destroy;
begin
  FreeAndNil(CellList);
  inherited Destroy;
end;


constructor TxCell.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  AntennaList := TxAntennaList.Create();
end;

destructor TxCell.Destroy;
begin
  FreeAndNil(AntennaList);
  inherited Destroy;
end;


constructor TxProject.Create;
begin
  inherited Create;

//  PropertyList := TxPropertyList.Create();
//  LinkList := TxLinkList.Create();
  AntennaTypeList := TxAntennaTypeList.Create();
  ReliefMatrixList := TxReliefMatrixList.Create();

  ScenarioList := TxScenarioList.Create();
 // LinkList := TxLinkList.Create();
  TrxList := TxTrxList.Create();
  CombinerList := TxCombinerList.Create();
end;

destructor TxProject.Destroy;
begin
  FreeAndNil(CombinerList);
  FreeAndNil(TrxList);
//  FreeAndNil(LinkList);
  FreeAndNil(ScenarioList);
  FreeAndNil(ReliefMatrixList);
  FreeAndNil(AntennaTypeList);
//  FreeAndNil(LinkList);
 // FreeAndNil(PropertyList);

  inherited Destroy;
end;

constructor TxReliefMatrixList.Create;
begin
  inherited Create (TxReliefMatrix);
end;

function TxReliefMatrixList.AddItem: TxReliefMatrix;
begin
  Result := TxReliefMatrix(inherited Add);
end;

function TxReliefMatrixList.GetItems(Index: Integer): TxReliefMatrix;
begin
  Result := TxReliefMatrix(inherited Items[Index]);
end;


constructor TxScenarioList.Create;
begin
  inherited Create (TxScenario);
end;

function TxScenarioList.AddItem: TxScenario;
begin
  Result := TxScenario(inherited Add);
end;

function TxScenarioList.GetItems(Index: Integer): TxScenario;
begin
  Result := TxScenario(inherited Items[Index]);
end;

constructor TxScenario.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  PropertyList := TxPropertyList.Create();
end;

destructor TxScenario.Destroy;
begin
  FreeAndNil(PropertyList);
  inherited Destroy;
end;

constructor TxTrxList.Create;
begin
  inherited Create (TxTRX);
end;

function TxTrxList.AddItem: TxTRX;
begin
  Result := TxTRX(inherited Add);
end;

function TxTrxList.GetItems(Index: Integer): TxTRX;
begin
  Result := TxTRX(inherited Items[Index]);
end;

function TxTrxList.FindByID(aID: integer): TxTRX;
var I: Integer;
begin
 Result := nil;

  for I := 0 to Count - 1 do
    if Items[i].ID = aID then
    begin
      Result := Items[i];
      Break;
    end;
end;


function TxAntenna.GetCRC32: LongWord;
var s : string;
begin
  s := Format('%1.6f', [Height]) +
       Format('%1.6f', [TILT]) +
       Format('%1.6f', [AZIMUTH])+
       Format('%1.6f', [LOSS_dB])
       ;
  Result := GetHashCRC32(s);
end;


function TxCell.GetCRC32: LongWord;
var s : string;
begin
  s := Format('%1.6f', [POWER_W]) +
       Format('%1.6f', [Sensitivity_dBm]) +
       Format('%1.6f', [Combiner_LOSS_dB])
       ;
  Result := GetHashCRC32(s);
end;


function TxProperty.GetCRC32: LongWord;
var
  s : string;
begin
  s := Format('%1.6f', [BLPoint.B]) +
       Format('%1.6f', [BLPoint.L]) ;

  Result := GetHashCRC32(s);
end;

function TxAntennaType.GetCRC32: LongWord;
var
  s : string;
begin
  s := Format('%1.6f', [GAIN_dB]) +
       Format('%1.6f', [VERT_WIDTH]) +
       Format('%1.6f', [HORZ_WIDTH]) +
       vert_mask +
       horz_mask;

  Result := GetHashCRC32(s);
end;

// ---------------------------------------------------------------
function TxAntennaType.Validate(var aError: string): Boolean;
// ---------------------------------------------------------------
begin
  aError := '';

  if GAIN_dB=0 then
    aError := aError+ 'Cell GAIN_dB=0. ';

 // if (CellList.Count>0) and (CellList[0].CELLID=0) then
  //  aError := aError+ Format('First cell CID=0', [Name]);

  if Length(aError)> 0 then
    aError := Format('AntennaType: %s. ', [Name]) + aError;

  Result:=Length(aError)=0;
end;

// ---------------------------------------------------------------
function TxCell.Validate(var aError: string): Boolean;
// ---------------------------------------------------------------
begin
  aError := '';

  if name='' then
    aError := aError+ 'Cell name is blank. ';

  if CELLID=0 then
    aError := aError+ 'Cell CELLID=0. ';


 // if (CellList.Count>0) and (CellList[0].CELLID=0) then
  //  aError := aError+ Format('First cell CID=0', [Name]);

  if Length(aError)> 0 then
    aError := Format('Cell: %s.', [Name]) + aError;

  Result:=Length(aError)=0;
end;

// ---------------------------------------------------------------
function TxSite.Validate(var aError: string): Boolean;
// ---------------------------------------------------------------
begin
  aError := '';

  if Pos('.', Name)>0 then
    aError := aError+ 'Name has ''.''. ';

  if CellList.Count=0 then
    aError := aError+ 'Cell count=0. ';

  if (CellList.Count>0) and (CellList[0].CELLID=0) then
    aError := aError+ 'First cell CID=0. ';

  if Length(aError)> 0 then
    aError := Format('Site %s. ', [Name]) + aError;

  Result:=Length(aError)=0;

end;

constructor TxCombinerList.Create;
begin
  inherited Create (TxCombiner);
end;

function TxCombinerList.AddItem: TxCombiner;
begin
  Result := TxCombiner(inherited Add);
end;

function TxCombinerList.GetItems(Index: Integer): TxCombiner;
begin
  Result := TxCombiner(inherited Items[Index]);
end;

function TxCombinerList.FindByID(aID: integer): TxCombiner;
var I: Integer;
begin
 Result := nil;

  for I := 0 to Count - 1 do
    if Items[i].ID = aID then
    begin
      Result := Items[i];
      Break;
    end;
end;




end.


(*

// ---------------------------------------------------------------
function TdmTask_XML_classes_to_MDB.Validate_Cell(aXSite: TXSite; aXCell: TxCell):
    boolean;
// ---------------------------------------------------------------
begin
  Result:=True;

  if aXCell.name='' then
  begin
    LogError(Format('ERROR: site %s: cell name is blank', [aXSite.Name]));

//    LogError('Cell name is blank');
    Result:=False;
  end;

  if aXCell.CELLID=0 then
  begin
    LogError(Format('ERROR: site %s: cell CELLID=0', [aXSite.Name, aXCell.Name]));

//    LogError(Format('Cell %s: CELLID=0', [aXCell.Name]));
    Result:=False;
  end;
end;*)