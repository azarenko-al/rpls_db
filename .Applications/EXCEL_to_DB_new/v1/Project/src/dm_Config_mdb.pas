unit dm_Config_mdb;

interface

uses
  SysUtils, Classes, DB, Forms, ADODB,  Dialogs,

  u_func,

  u_config_file,

  u_db,

  u_db_mdb
  
  ;

type
  TdmConfig_mdb = class(TDataModule)
    ADOConnection_MDB1: TADOConnection;
    t_Fields: TADOTable;
    ds_Fields: TDataSource;
    view_Fields_for_property: TADOTable;
    ds_view_Fields_for_property: TDataSource;
    view_Fields_for_link: TADOTable;
    ds_view_Fields_for_link: TDataSource;
    t_Equipment_to_LinkendType: TADOTable;
    ds_Equipment_to_LinkendType: TDataSource;
    t_Equipment_to_AntennaType: TADOTable;
    ds_Equipment_to_AntennaType: TDataSource;
    t_Equipment_to_AntennaTypeid: TAutoIncField;
    t_Equipment_to_AntennaTypediameter: TFloatField;
    t_Equipment_to_AntennaTypeband: TIntegerField;
    t_Equipment_to_AntennaTypeName: TWideStringField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class procedure Init;

    procedure OpenFieldsDataset(aDataSource: TDataSourceType);

  end;


const

  DEF_view_Fields_for_property = 'view_Fields_for_property';
  DEF_view_Fields_for_link     = 'view_Fields_for_link';




var
  dmConfig_mdb: TdmConfig_mdb;

implementation

{$R *.dfm}



class procedure TdmConfig_mdb.Init;
begin
  if not Assigned(dmConfig_mdb) then
    Application.CreateForm(TdmConfig_mdb, dmConfig_mdb);
end;


// ---------------------------------------------------------------
procedure TdmConfig_mdb.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
var
  sFileName: string;
begin
  if ADOConnection_MDB1.Connected then
    ShowMessage('TdmMDB.ADOConnection.Connected');


  sFileName := ChangeFileExt(Application.ExeName, '.mdb');

  if not mdb_OpenConnection (ADOConnection_MDB1, sFileName) then
  begin
    raise Exception.Create(sFileName);

    Halt;
  end;


  view_Fields_for_link.Open;
  view_Fields_for_property.Open;

  t_Equipment_to_LinkendType.Open;
  t_Equipment_to_AntennaType.Open;


end;



// ---------------------------------------------------------------
procedure TdmConfig_mdb.OpenFieldsDataset(aDataSource: TDataSourceType);
// ---------------------------------------------------------------
var
//  sFileName: string;
  sTableName: string;
begin
  t_Fields.Close;

  case aDataSource of
    dtProp: sTableName:=DEF_view_Fields_for_property;
    dtLink: sTableName:=DEF_view_Fields_for_link;
  end;


  t_Fields.TableName:=sTableName;
  t_Fields.Open;


end;



end.

