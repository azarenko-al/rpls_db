object frame_Options: Tframe_Options
  Left = 817
  Top = 421
  Width = 624
  Height = 436
  Caption = 'frame_Options'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxVerticalGrid1: TcxVerticalGrid
    Left = 0
    Top = 0
    Width = 616
    Height = 313
    BorderStyle = cxcbsNone
    Align = alTop
    LookAndFeel.Kind = lfFlat
    OptionsView.CellTextMaxLineCount = 3
    OptionsView.ShowEditButtons = ecsbFocused
    OptionsView.AutoScaleBands = False
    OptionsView.PaintStyle = psDelphi
    OptionsView.GridLineColor = clBtnShadow
    OptionsView.RowHeaderMinWidth = 30
    OptionsView.RowHeaderWidth = 305
    OptionsView.ValueWidth = 40
    TabOrder = 0
    Version = 1
    object row_Sheets: TcxEditorRow
      Properties.Caption = 'row_Sheets'
      Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
      Properties.EditProperties.DropDownListStyle = lsFixedList
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxVerticalGrid1CategoryRow4: TcxCategoryRow
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object row_FirstRowIndex_1: TcxEditorRow
      Properties.Caption = #1053#1086#1084#1077#1088' '#1089#1090#1088#1086#1082#1080' '#1089' '#1085#1072#1079#1074#1072#1085#1080#1103#1084#1080' '#1089#1090#1086#1083#1073#1094#1086#1074
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.EditProperties.MaxValue = 100.000000000000000000
      Properties.EditProperties.MinValue = 1.000000000000000000
      Properties.EditProperties.UseCtrlIncrement = True
      Properties.DataBinding.ValueType = 'Integer'
      Properties.Value = 1
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object row_DataType: TcxEditorRow
      Properties.Caption = 'DataType'
      Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
      Properties.EditProperties.Columns = 2
      Properties.EditProperties.Items = <
        item
          Caption = #1055#1083#1086#1097#1072#1076#1082#1080
          Value = '0'
        end
        item
          Caption = #1048#1085#1090#1077#1088#1074#1072#1083#1099
          Value = '1'
        end>
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = '0'
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object row_CoordSys: TcxEditorRow
      Properties.Caption = #1057#1080#1089#1090#1077#1084#1072' '#1082#1086#1086#1088#1076#1080#1085#1072#1090
      Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
      Properties.EditProperties.Columns = 2
      Properties.EditProperties.Items = <
        item
          Caption = #1055#1091#1083#1082#1086#1074#1086' CK-42'
          Value = '0'
        end
        item
          Caption = 'WGS 84'
          Value = '1'
        end>
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = '0'
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object cxVerticalGrid1CategoryRow1: TcxCategoryRow
      Expanded = False
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object cxVerticalGrid1CategoryRow3: TcxCategoryRow
      Properties.Caption = #1040#1085#1090#1077#1085#1085#1072
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object row_Gain_dB2: TcxEditorRow
      Expanded = False
      Properties.Caption = #1059#1089#1080#1083#1077#1085#1080#1077' [dB]'
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.EditProperties.ValueType = vtFloat
      Properties.DataBinding.ValueType = 'Float'
      Properties.Value = Null
      ID = 7
      ParentID = 6
      Index = 0
      Version = 1
    end
    object row_Diameter2: TcxEditorRow
      Expanded = False
      Properties.Caption = #1044#1080#1072#1084#1077#1090#1077#1088' [m]'
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.EditProperties.ValueType = vtFloat
      Properties.DataBinding.ValueType = 'Float'
      Properties.Value = Null
      ID = 8
      ParentID = 6
      Index = 1
      Version = 1
    end
    object row_Height2: TcxEditorRow
      Expanded = False
      Properties.Caption = #1042#1099#1089#1086#1090#1072' '#1087#1086#1076#1074#1077#1089#1072' (m)'
      Properties.Hint = #1059#1089#1090#1072#1085#1072#1074#1083#1080#1074#1072#1077#1090#1089#1103', '#1077#1089#1083#1080' '#1074#1099#1089#1086#1090#1072' '#1072#1085#1090#1077#1085#1085#1099' '#1085#1072' '#1079#1072#1076#1072#1085#1072
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.EditProperties.ValueType = vtFloat
      Properties.DataBinding.ValueType = 'Float'
      Properties.Value = Null
      ID = 9
      ParentID = 6
      Index = 2
      Version = 1
    end
    object cxVerticalGrid1EditorRow3: TcxEditorRow
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 10
      ParentID = -1
      Index = 7
      Version = 1
    end
    object cxVerticalGrid1CategoryRow2: TcxCategoryRow
      Properties.Caption = #1056#1056#1057':'
      ID = 11
      ParentID = -1
      Index = 8
      Version = 1
    end
    object row_Power_dBm2: TcxEditorRow
      Expanded = False
      Properties.Caption = #1052#1086#1097#1085#1086#1089#1090#1100' (dBm)'
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.EditProperties.ValueType = vtFloat
      Properties.DataBinding.ValueType = 'Float'
      Properties.Value = Null
      ID = 12
      ParentID = 11
      Index = 0
      Version = 1
    end
    object row_Bitrate_Mbps2: TcxEditorRow
      Expanded = False
      Properties.Caption = #1057#1082#1086#1088#1086#1089#1090#1100' '#1087#1077#1088#1077#1076#1072#1095#1080' (MBit/s)'
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.EditProperties.ValueType = vtFloat
      Properties.DataBinding.ValueType = 'Float'
      Properties.Value = Null
      ID = 13
      ParentID = 11
      Index = 1
      Version = 1
    end
    object row_Threshold_BER_32: TcxEditorRow
      Expanded = False
      Properties.Caption = #1063#1091#1074#1089#1090#1074#1080#1090#1077#1083#1100#1085#1086#1089#1090#1100' '#1087#1088#1080' BER 10^-3 [dBm] '
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.EditProperties.ValueType = vtFloat
      Properties.DataBinding.ValueType = 'Float'
      Properties.Value = Null
      ID = 14
      ParentID = 11
      Index = 2
      Version = 1
    end
    object row_Threshold_BER_62: TcxEditorRow
      Expanded = False
      Properties.Caption = #1063#1091#1074#1089#1090#1074#1080#1090#1077#1083#1100#1085#1086#1089#1090#1100' '#1087#1088#1080' BER 10^-6 [dBm] '
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.EditProperties.ValueType = vtFloat
      Properties.DataBinding.ValueType = 'Float'
      Properties.Value = Null
      ID = 15
      ParentID = 11
      Index = 3
      Version = 1
    end
  end
end
