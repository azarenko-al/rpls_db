program EXCEL_to_DB;

uses
  dm_Config_mdb in 'src\dm_Config_mdb.pas' {dmConfig_mdb: TDataModule},
  dm_Excel_to_Classes in 'src\dm_Excel_to_Classes.pas' {dmExcel_to_Classes: TDataModule},
  dm_Export_classes_link_to_MDB in 'src\dm_Export_classes_link_to_MDB.pas' {dmExport_classes_link_to_MDB: TDataModule},
  dm_Export_classes_to_MDB in 'src\dm_Export_classes_to_MDB.pas' {dmExport_classes_to_MDB: TDataModule},
  d_Data_view in 'src\d_Data_view.pas' {dlg_Data_view},
  f_Main_Excel_to_MDB in 'src\f_Main_Excel_to_MDB.pas' {frm_Main},
  Forms,
  u_config_file in 'src\u_config_file.pas',
  u_import_classes in 'src\u_import_classes.pas',
  u_excel_const in 'src\u_excel_const.pas',
  dm_App in 'src\dm_App.pas' {dmApp: TDataModule},
  dm_Main_Data in 'src\dm_Main_Data.pas' {dmMain_Data: TDataModule},
  fr_Options in 'src\fr_Options.pas' {frame_Options},
  dm_Excel_file in 'W:\common XE\office_new\dm_Excel_file.pas' {dmExcel_file: TDataModule},
  u_ini_Excel_to_DB_params in 'Shared\u_ini_Excel_to_DB_params.pas',
  u_const_db in 'Shared\u_const_db.pas',
  u_ini_data_export_import_params in '..\..\DataExportImport\Project\src shared\u_ini_data_export_import_params.pas',
  u_vars in '..\..\u_vars.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TdmApp, dmApp);
  Application.Run;
end.
