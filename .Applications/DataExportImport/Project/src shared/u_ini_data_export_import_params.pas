unit u_ini_data_export_import_params;

interface
uses
  SysUtils, IniFiles,



  u_classes;


const
   PROG_data_export_import_exe = 'data_export_import.exe';

type
  TExportModeType = (
      mtNone,

      mtExport_selected,

      mtExport_GUIDES,
      mtExport_PROJECT,

      mtImport_GUIDES,
      mtImport_PROJECT
  );

  // ---------------------------------------------------------------
  TIni_Data_export_import_params = class(TObject)
  // ---------------------------------------------------------------
  private

  public
    ConnectionString : string;


    IsFolder1: boolean;
    Folder_ID: integer;

    ProjectID: Integer;

    FileName_MDB: string;

    TableName : string;
   // ObjectName : string;
    SelectedIDList: TIDList;

    IsAuto:   Boolean;

    IsTest:  Boolean;


    Mode: TExportModeType;


    constructor Create;
    destructor Destroy; override;

    procedure SaveToIni(aFileName: String);
    procedure LoadFromIni(aFileName: String);
  end;

var
  g_Ini_Data_export_import_params: TIni_Data_export_import_params;


implementation


function StrToExportModeType(aValue: string): TExportModeType;forward;
function ExportModeTypeToStr(aValue: TExportModeType): string; forward;




constructor TIni_Data_export_import_params.Create;
begin
  inherited Create;
  SelectedIDList := TIDList.Create();
end;

destructor TIni_Data_export_import_params.Destroy;
begin
  FreeAndNil(SelectedIDList);
  inherited Destroy;
end;

// ---------------------------------------------------------------
procedure TIni_Data_export_import_params.LoadFromIni(aFileName: String);
// ---------------------------------------------------------------
var
  oIni: TMemIniFile;
  s: string;
begin
  aFileName:= ChangeFileExt(aFileName, '.ini');

  if not FileExists(aFileName) then
    exit;



  oIni:=TMemIniFile.Create (aFileName);


  ConnectionString := oIni.ReadString('main', 'ConnectionString', '');


  // -------------------------
  ProjectID:=oIni.ReadInteger('main', 'Project_ID',  0);
  IsAuto   :=oIni.ReadBool   ('main', 'IsAuto', False);

  IsTest   :=oIni.ReadBool   ('main', 'IsTest', False);

 // TableName := oIni.ReadString ('main', 'ObjectName', '');
  TableName  := oIni.ReadString ('main', 'TableName', '');

  FileName_MDB := oIni.ReadString('main', 'FileName_MDB', '');

 // assert(FileName_MDB <> '');


  s :=oIni.ReadString ('main', 'mode', '');

//  assert(s<>'');

  Folder_ID:=oIni.ReadInteger('main', 'Folder_ID',  0);
  IsFolder1 :=oIni.ReadBool   ('main', 'IsFolder',  False);


  Mode :=StrToExportModeType(s);


  oIni.Free;
  // -------------------------

  if TableName<>'' then
    SelectedIDList.LoadIDsFromIniFile (aFileName, 'list');

end;


// ---------------------------------------------------------------
procedure TIni_Data_export_import_params.SaveToIni(aFileName: String);
// ---------------------------------------------------------------
var
  oIni: TIniFile;
  s: string;
begin
  aFileName:= ChangeFileExt(aFileName, '.ini');

  DeleteFile(aFileName);

  ForceDirectories( ExtractFileDir(aFileName) );


  oIni:=TIniFile.Create (aFileName);


  oIni.WriteString('main', 'ConnectionString', ConnectionString);


//  oIni.EraseSection('main');

//  assert(FileName_MDB <> '');

  oIni.WriteString('main', 'FileName_MDB', FileName_MDB);

//  assert(ProjectID>0);

  oIni.WriteInteger('main', 'Project_ID', ProjectID);

  oIni.WriteBool   ('main', 'IsAuto', IsAuto);

  oIni.WriteBool   ('main', 'IsTest', IsTest);

  oIni.WriteInteger('main', 'Folder_ID',  Folder_ID);
  oIni.WriteBool   ('main', 'IsFolder',   IsFolder1);



  s:=ExportModeTypeToStr(Mode);
  oIni.WriteString ('main', 'mode', s);

  if TableName<>'' then
    oIni.WriteString ('main', 'TableName', TableName);

//    Mode :=StrToExportModeType(Mode);

 // oIni.UpdateFile;
  oIni.Free;

  if TableName<>'' then
    SelectedIDList.SaveIDsToIniFile(aFileName, 'list');

end;



// ---------------------------------------------------------------
function StrToExportModeType(aValue: string): TExportModeType;
// ---------------------------------------------------------------
begin
  aValue :=LowerCase(aValue);

  if aValue = 'export_selected' then result :=mtExport_selected else
  if aValue = 'export_guides'   then result :=mtexport_guides else
  if aValue = 'export_project'  then result :=mtexport_project else
  if aValue = 'import_guides'   then result :=mtimport_guides else
  if aValue = 'import_project'  then result :=mtimport_project
  else
    raise Exception.Create(aValue);

   // ShowMessage('function StrToExportModeType(aValue: string): TExportModeType; - '+aValue);
//    Result := mtNone;


end;

// ---------------------------------------------------------------
function ExportModeTypeToStr(aValue: TExportModeType): string;
// ---------------------------------------------------------------
begin
  case aValue of
    mtExport_selected: result := 'export_selected';

    mtexport_guides : result := 'export_guides';
    mtexport_project: result := 'export_project';
    
    mtimport_guides : result := 'import_guides';
    mtimport_project: result := 'import_project';
  end;
end;



initialization
  g_Ini_Data_export_import_params:=TIni_Data_export_import_params.Create;

finalization
  FreeAndNil(g_Ini_Data_export_import_params);


end.



   {

initialization
  g_Excel_to_DB_params := TIni_Excel_to_DB_params.Create();

Finalization
  FreeAndNil(g_Excel_to_DB_params);

  ShellExec (GetApplicationDir()+ PROG_data_export_import_exe,
    DoubleQuotedStr(sFile));

