unit I_data_export_import;

interface

uses ADOInt,
  u_dll;

type   
       qqqqqqqqqqqq
  IData_export_import_X = interface(IInterface)
  ['{26F59ED7-B64C-4940-AA74-01F297583F51}']

    procedure InitADO(aConnection: _Connection); stdcall;
    procedure InitAppHandle(aHandle: integer); stdcall;

    procedure Execute (aFileName: WideString);  stdcall;
  end;

var
  IData_export_import: IData_export_import_X;

  function Load_IData_export_import: Boolean;
  procedure UnLoad_IData_export_import;

implementation

const
//  DEF_BPL_FILENAME = 'bpl_LOS.bpl';
  DEF_FILENAME = 'dll_Data_export_import.dll';

var
  LHandle : Integer;


function Load_IData_export_import: Boolean;
begin
  Result := GetInterface_DLL(DEF_FILENAME, LHandle,
                IData_export_import_X, IData_export_import)=0;
end;


procedure UnLoad_IData_export_import;
begin
  IData_export_import := nil;
  UnloadPackage_dll(LHandle);
end;


end.



