program Data_Import_Lib;



uses
  dm_App_import_lib in 'Src\dm_App_import_lib.pas' {dmApp_import: TDataModule},
  dm_Config in 'Src\dm_Config.pas' {dmConfig: TDataModule},
  dm_MDB in 'Src\dm_MDB.pas' {dmMDB: TDataModule},
  dm_MDB_to_SQL in 'Src\dm_MDB_to_SQL.pas' {dmMDB_to_SQL: TDataModule},
  dm_MDB_validate in 'Src\dm_MDB_validate.pas' {dmMDB_validate: TDataModule},
  f_Main_import_lib in 'Src\f_Main_import_lib.pas' {frm_Main_Import_Lib},
  Forms,
  u_classes_tree_new in 'Src\u_classes_tree_new.pas',
  u_Config_classes in 'Src\u_Config_classes.pas',
  u_export_classes in 'Src\u_export_classes.pas',
  u_FK in 'Src\u_FK.pas',
  u_ini_data_export_import_params in 'src shared\u_ini_data_export_import_params.pas',
  u_db in '..\..\..\common7\Package_Common\u_db.pas',
  u_lists in 'Src\u_lists.pas',
  u_const_db in '..\..\EXCEL_to_DB_new\Project\Shared\u_const_db.pas',
  dm_Main_Data in 'Src\dm_Main_Data.pas' {dmMain_Data: TDataModule},
  fra_DB_Login in '..\..\..\.Parts\Login\src\fra_DB_Login.pas' {frame_DB_Login};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TdmApp_import, dmApp_import);
  Application.Run;
end.
