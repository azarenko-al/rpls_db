unit u_Config_classes;

interface

uses
  Classes, SysUtils,    
    
  System.Generics.Collections, 

  u_Func,
  u_Func_arrays,

  DB;

type
  TTableInfoList = class;
  TTableInfo =class;
  TExceptedFieldList=class;
  TFieldReplaceList =class;


  // -------------------------------------------------------------------
  TRefTable = class
  // -------------------------------------------------------------------

  public
    TableName: string;

    TableInfoRef: TTableInfo;
    MasterTableInfoRef: TTableInfo;

    Master_FieldName: string;

    IsRequired: Boolean; //allow NULL

  end;

  // ---------------------------------------------------------------
  TRefTableList = class(TList)
  //class(TList<TRefTable>)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TRefTable;
  public
    function AddItem: TRefTable;

   // procedure LoadFromDataset(aDataset: TDataSet; aTableInfoList: TTableInfoList);


    function FindByFieldName(aFieldName: string): TRefTable;
    property Items[Index: Integer]: TRefTable read GetItems; default;
  end;


  // -------------------------------------------------------------------
  TFieldReplace = class
  // -------------------------------------------------------------------
  private
  public
    TableName: string;
    Name_old : string;
    Name_new : string;
    DataType : string;

    SQL: TStringList;

    constructor Create;
    destructor Destroy; override;

  end;

  // ---------------------------------------------------------------
  TFieldReplaceList = class(TList)
  //class(TList<TRefTable>)
  
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TFieldReplace;
  public
    function AddItem: TFieldReplace;
    function SelectByTableName(aTableName: string): TFieldReplaceList;

    procedure LoadFromDataset(aDataset: TDataset);


    property Items[Index: Integer]: TFieldReplace read GetItems; default;
  end;
         
  // -------------------------------------------------------------------
  TTableInfo = class
  // -------------------------------------------------------------------
  public
    TableName    : string;
    SQL          : string;

//    TableName_old: string;

    DestTableName: string; //new

    KeyFieldName : string;

    UseProjectID : Boolean;
    UseFolderID  : Boolean;

    Folder_Objname : string;

    Index_fields     : string;

    LocateFieldsStr: string;
    LocateFieldsArr: TStrArray;

    RefTables: TRefTableList;

    constructor Create;
    destructor Destroy; override;

    procedure LoadFromDataset(aDataset: TDataset);
    procedure SetLocateFieldsStr(aValue: string);
  end;

  // ---------------------------------------------------------------
  TTableInfoList = class(TList)
  //class(TList<TRefTable>)

  // ---------------------------------------------------------------
  private
// TODO: FindIndexByTableName
//  function FindIndexByTableName(aTableName: string): integer;
    function GetItems(Index: Integer): TTableInfo;
  public
    function AddItem: TTableInfo;

    procedure LoadFromDataset(aDataset: TDataset);

    procedure LoadFromDataset_Ref(aDataset: TDataSet);


    function FindByTableName(aTableName: string): TTableInfo;
    function GetKeyFieldName(aTableName: string): string;

    property Items[Index: Integer]: TTableInfo read GetItems; default;
  end;

  // ---------------------------------------------------------------
  TExceptedFieldList = class(TStringList)
  // ---------------------------------------------------------------
  public
    procedure LoadFromDataset(aDataset: TDataset);


    function FindItem(aTableName: string; aName: string): Boolean;
    procedure AddItem(aTableName: string; aName: string);
  end;


  // ---------------------------------------------------------------
  TConfig = class
  // ---------------------------------------------------------------
  public
    FieldReplaceList: TFieldReplaceList;
    ExceptedFields: TExceptedFieldList;
    TableInfoList: TTableInfoList;
    TablesChecked: TStringList;

    Tables_Relation_arr: array of
    record
      TableName_Main: String;
      TableName_Child: String;
    end;


    constructor Create;
    destructor Destroy; override;
  end;



implementation

constructor TConfig.Create;
begin
  inherited Create;
  ExceptedFields := TExceptedFieldList.Create();
  TableInfoList := TTableInfoList.Create();
  TablesChecked := TStringList.Create();

  FieldReplaceList := TFieldReplaceList.Create();
end;

destructor TConfig.Destroy;
begin
  FreeAndNil(FieldReplaceList);

  FreeAndNil(TablesChecked);
  FreeAndNil(ExceptedFields);
  FreeAndNil(TableInfoList);
  inherited Destroy;
end;


function TRefTableList.AddItem: TRefTable;
begin
  Result := TRefTable.Create;
  Add(Result);
end;

function TRefTableList.GetItems(Index: Integer): TRefTable;
begin
  Result := TRefTable(inherited Items[Index]);
end;

// ---------------------------------------------------------------
function TRefTableList.FindByFieldName(aFieldName: string): TRefTable;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Result := nil;

  for I := 0 to Count - 1 do
    if //Items[i].Src_ADOQuery.Active and
       Eq(Items[i].Master_FieldName, aFieldName) then
    begin
      Result := Items[i];
      Break;
    end;
end;


// ---------------------------------------------------------------
procedure TTableInfoList.LoadFromDataset_Ref(aDataset: TDataSet);
// ---------------------------------------------------------------
var
  oTableInfo: TTableInfo;
  oMasterTableInfo: TTableInfo;
  oRefTable: TRefTable;
  s: string;

begin

  with aDataset do
    while not EOF do
    begin
      s:=FieldByName('MasterTableName').AsString;
      oMasterTableInfo := FindByTableName(s);

   //   if s='Template_Linkend_Antenna' then
     //   s:=s;              


      s:=FieldByName('RefTableName').AsString;
      oTableInfo := FindByTableName(s);

      if not Assigned(oTableInfo) then
      begin
        s:=s;
      end;


      Assert(Assigned(oMasterTableInfo), 'procedure TdmConfig.LoadConfig1(aConfig: TConfig); '+s);
      Assert(Assigned(oTableInfo), 'procedure TdmConfig.LoadConfig1(aConfig: TConfig); '+ s);


      if (Assigned(oMasterTableInfo)) and (Assigned(oTableInfo))
      then begin
        oRefTable:=oMasterTableInfo.RefTables.AddItem;

        oRefTable.MasterTableInfoRef := oMasterTableInfo;
        oRefTable.TableInfoRef     :=oTableInfo;
        oRefTable.TableName        :=FieldByName('RefTableName').AsString;
        oRefTable.Master_FieldName :=FieldByName('MasterKey').AsString;

        oRefTable.IsRequired       :=FieldByName('Required').AsBoolean;
      end;

    //  end;
      Next;
    end;

end;



constructor TTableInfo.Create;
begin
  inherited Create;
  RefTables := TRefTableList.Create();
end;

destructor TTableInfo.Destroy;
begin
  FreeAndNil(RefTables);
  inherited Destroy;
end;

procedure TTableInfo.SetLocateFieldsStr(aValue: string);
begin
  LocateFieldsStr:=aValue;
  LocateFieldsArr:=StringToStrArray(aValue,';');
end;

//-------------------------------------------------------------------
procedure TTableInfo.LoadFromDataset(aDataset: TDataset);
//-------------------------------------------------------------------
begin
  with aDataset do
    if not IsEmpty then
  begin
    TableName    :=FieldByName('Name').AsString;
    Assert(TableName<>'');

    SQL :=FieldByName('SQL').AsString;


   // TableName_old:=FieldByName('Name_old').AsString;

    DestTableName :=TableName;

    KeyFieldName :=FieldByName('key').AsString;

    Assert(KeyFieldName<>'', 'KeyFieldName empty');

    UseProjectID :=FieldByName('Use_Project_ID').AsBoolean;
    UseFolderID  :=FieldByName('Use_Folder_ID').AsBoolean;

    Folder_Objname:=FieldByName('Folder_Objname').AsString;

    Index_fields :=FieldByName('index_fields').AsString;


    SetLocateFieldsStr(Index_fields);

  end;
end;

function TTableInfoList.AddItem: TTableInfo;
begin
  Result := TTableInfo.Create;
  Add(Result);
end;

// TODO: FindIndexByTableName
//function TTableInfoList.FindIndexByTableName(aTableName: string): integer;
//var I: Integer;
//begin
//Result := -1;
//
//for I := 0 to Count - 1 do
//  if Eq(Items[i].TableName, aTableName) then
//  begin
//    Result := i;
//    Break;
//  end;
//end;

// ---------------------------------------------------------------
function TTableInfoList.FindByTableName(aTableName: string): TTableInfo;
// ---------------------------------------------------------------
var I: Integer;
begin
  Result := nil;

  for I := 0 to Count - 1 do
    if Eq(Items[i].TableName, aTableName) then
    begin
      Result := Items[i];
      Break;
    end;
end;

// ---------------------------------------------------------------
function TTableInfoList.GetKeyFieldName(aTableName: string): string;
// ---------------------------------------------------------------
var
  oTableInfo_: TTableInfo;
begin
  oTableInfo_ := FindByTableName(atableName);
  if Assigned(oTableInfo_)
    then Result := oTableInfo_.KeyFieldName
    else Result := '';
end;

function TTableInfoList.GetItems(Index: Integer): TTableInfo;
begin
  Result := TTableInfo(inherited Items[Index]);
end;

// ---------------------------------------------------------------
procedure TTableInfoList.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
var
  oTableInfo: TTableInfo;
begin
  aDataset.First;

  with aDataset do
    while not EOF do
    begin
      oTableInfo := AddItem();

      oTableInfo.LoadFromDataset(aDataset);

      Next;
    end;

end;


procedure TExceptedFieldList.AddItem(aTableName: string; aName: string);
begin
  Add(aName);
end;

function TExceptedFieldList.FindItem(aTableName: string; aName: string):
    Boolean;
begin
  Result := IndexOf(aName)>=0;
end;

procedure TExceptedFieldList.LoadFromDataset(aDataset: TDataset);
begin

// db_TableReOpen(adoTable1, 'Fields_Exception');
  aDataset.First;

  with aDataset do
    while not EOF do
    begin
      Add(FieldBYName('name').AsString);
      Next;
    end;
end;


function TFieldReplaceList.AddItem: TFieldReplace;
begin
  Result := TFieldReplace.Create;
  Add(Result);
end;

function TFieldReplaceList.GetItems(Index: Integer): TFieldReplace;
begin
  Result := TFieldReplace(inherited Items[Index]);
end;

// ---------------------------------------------------------------
procedure TFieldReplaceList.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
var
  oFieldReplace: TFieldReplace;
begin
 with aDataset do
    while not EOF do
    begin
      oFieldReplace := AddItem;

      oFieldReplace.TableName := FieldByName('TableName').AsString;
      oFieldReplace.Name_old  := FieldByName('Name_old').AsString;
      oFieldReplace.Name_new  := FieldByName('Name_new').AsString;
      oFieldReplace.DataType  := FieldByName('DataType').AsString;
      oFieldReplace.SQL.Text  := FieldByName('SQL').AsString;

      Next;
    end;

end;


function TFieldReplaceList.SelectByTableName(aTableName: string): TFieldReplaceList;
var
  I: Integer;
begin
  Result := TFieldReplaceList.Create;

  for I := 0 to Count - 1 do
   if Eq(Items[i].TableName, aTableName) then
     Result.Add(Items[i]);
end;


constructor TFieldReplace.Create;
begin
  inherited Create;
  SQL := TStringList.Create();
end;

destructor TFieldReplace.Destroy;
begin
  FreeAndNil(SQL);
  inherited Destroy;
end;

end.
