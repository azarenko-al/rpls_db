unit u_classes_tree_new;

interface

uses
  Classes, SysUtils, Variants,

  u_func,
  u_files
  ;

type
  TDBItemList = class;

  // ---------------------------------------------------------------
  TDBItem = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
    SubItems: TDBItemList;
  public

    TableName : string;
    Key: Variant;

    Expanded : Boolean;

    IsStoredToDB : Boolean;


  //  Level : Integer;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;

  end;


  TDBItemRefList = class;

  // ---------------------------------------------------------------
  TDBItemList = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TDBItem;
    procedure InsertSubItems(aSList: TStringList; aLevel: Integer);
  public
    constructor Create;

    function AddItem(aTableName: string; aKey: Variant): TDBItem;
    procedure ShowTest;

    function FindNotExpanded(var aDBItem: TDBItem): Boolean;

    procedure SelectByTableName(aTableName: string; aList: TDBItemRefList);

    property Items[Index: Integer]: TDBItem read GetItems; default;
  end;

  // ---------------------------------------------------------------
  TDBItemRefList = class(TList)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TDBItem;
  public
    function GetID: string;

    property Items[Index: Integer]: TDBItem read GetItems; default;
  end;




implementation

constructor TDBItemList.Create;
begin
  inherited Create(TDBItem);
end;


function TDBItemList.AddItem(aTableName: string; aKey: Variant): TDBItem;
begin
 // Assert(aKey>0, 'Value <=0');

  Result := TDBItem (inherited Add);

  Result.TableName := aTableName;
  Result.Key := aKey;

end;

// ---------------------------------------------------------------
function TDBItemList.FindNotExpanded(var aDBItem: TDBItem): Boolean;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Result := False;

  i:=Count;

  for I := 0 to Count - 1 do
    if not Items[i].Expanded then
    begin
      Result := True;
      aDBItem := Items[i];
      Exit;
    end;

  for I := 0 to Count - 1 do
    begin
      Result := Items[i].SubItems.FindNotExpanded(aDBItem);
      if Result then
        Exit;

    //  aTableName := Items[i].TableName;
     // Exit;
    end;


end;


function TDBItemList.GetItems(Index: Integer):
    TDBItem;
begin
  Result := TDBItem(inherited Items[Index]);
end;



// ---------------------------------------------------------------
procedure TDBItemList.ShowTest;
// ---------------------------------------------------------------
var
  I: Integer;
  oSList: TStringList;
begin
  oSList:=TStringList.Create;

  InsertSubItems (oSList, 0);

  ShellExec_Notepad_temp(oSList.Text);

  FreeAndNil(oSList);

end;

// ---------------------------------------------------------------
procedure TDBItemList.InsertSubItems(aSList: TStringList; aLevel: Integer);
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
  sLevel: string;
begin

  for I := 0 to Count - 1 do
  begin
    sLevel := StringOfChar(' ',aLevel);

    s:=sLevel+ Format('%s: %s', [Items[i].TableName, VarToStr(Items[i].Key)]);
    if Items[i].Expanded then
      s:=s + '  Expanded';

    aSList.Add( s);


    if Items[i].SubItems.Count>0 then
      Items[i].SubItems.InsertSubItems(aSList, aLevel+1);
  end;

end;


procedure TDBItemList.SelectByTableName(aTableName: string; aList:
    TDBItemRefList);
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
  begin
    if Eq(Items[i].TableName, aTableName) then
       aList.Add(Items[i]);

    Items[i].SubItems.SelectByTableName(aTableName, aList);
  end;
end;



constructor TDBItem.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  SubItems := TDBItemList.Create();
end;

destructor TDBItem.Destroy;
begin
  FreeAndNil(SubItems);
  inherited Destroy;
end;

function TDBItemRefList.GetID: string;
var
  I: Integer;
begin
  Result := '';

  for I := 0 to Count - 1 do
    Result := Result + Format('%s,', [VarToStr(Items[i].Key)]);

  Result := Copy(Result, 1, Length(Result)-1);
end;

function TDBItemRefList.GetItems(Index: Integer): TDBItem;
begin
  Result := TDBItem(inherited Items[Index]);
end;

end.
