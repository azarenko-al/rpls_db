unit u_lists;

interface

uses      
  System.Generics.Collections, 
  classes;

type

 // ---------------------------------------------------------------
  TRefItem = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
    Parent_TableName: string;
    Slave_TableName: string;
    ParentFieldName: string;

    IsChild: Boolean;    
  end;


  // ---------------------------------------------------------------
  TRefItemList = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TRefItem;
  public
    constructor Create;

    function AddChild(aParent_TableName,aSlave_TableName,aParentFieldName: string):
        TRefItem;
    function AddRoot(aTableName: string): TRefItem;

    property Items[Index: Integer]: TRefItem read GetItems; default;
  end;


var
  g_ItemList: TRefItemList;


implementation


constructor TRefItemList.Create;
begin
  inherited Create(TRefItem);;
end;

// ---------------------------------------------------------------
function TRefItemList.AddChild(aParent_TableName,aSlave_TableName,
    aParentFieldName: string): TRefItem;
// ---------------------------------------------------------------
begin
  Result := TRefItem (Inherited Add);

  Result.Parent_TableName:=aParent_TableName;
  Result.Slave_TableName :=aSlave_TableName;
  Result.ParentFieldName :=aParentFieldName;
  Result.IsChild:=True;

end;


// ---------------------------------------------------------------
function TRefItemList.AddRoot(aTableName: string): TRefItem;
// ---------------------------------------------------------------
begin
  Result := TRefItem (Inherited Add);

  Result.Parent_TableName:=aTableName;
end;



function TRefItemList.GetItems(Index: Integer): TRefItem;
begin
  Result := TRefItem (Inherited Items[Index]);
end;



begin
  g_ItemList:=TRefItemList.Create;




end.


{
           constructor TResultItemList.Create;
begin
  inherited Create(TResultItem);
end;

function TResultItemList.AddItem: TResultItem;
begin
  Result := TResultItem (Inherited Add);
end;

function TResultItemList.GetItems(Index: Integer): TResultItem;
begin
  Result := TResultItem (Inherited Items[Index]);
end;

