unit dm_SQL_to_MDB;

interface

uses
  SysUtils, Classes, Forms, Variants, ADODB, Db,  Dialogs,

  DbugIntf,


  u_classes_tree_new,

  dm_Config,
  u_Config_classes,

  dm_MDB,

  d_Progress,

  dm_Main, 

  u_FK,

  u_log,
  u_func,
  u_files,

  u_db,

  u_const_db,

  u_export_classes;


type
  TStrNotifyEvent = procedure (aValue: string) of object;

  TLogMsgType = (ltError,ltSQL);


  TdmSQL_to_MDB1 = class(TDataModule)
    qry_Source: TADOQuery;
    tb_Dest: TADOTable;
    ADOConnection_MDB1: TADOConnection;
    q_Ref_keys: TADOQuery;
    q_PK: TADOQuery;

    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FDBTree : TDBItemList;

//    procedure AppendRecordFromDataset;
    procedure CopyDataRecord(aTableInfo: TTableInfo);

    procedure CopyDatasets(aTableInfo: TTableInfo; aIsRefTable: boolean = false);

    function GetTableCreateSQL(aDataset: TDataset; aTableName, aKeyFieldName:
        String): string;

    procedure Log(aMsg: string);

  private
 //   FFKItemList: TFKItemList;
  //  FPrimaryKeyList: TPrimaryKeyList;

    FLogFileName: string;

    FSrcDataSet: TDataSet;
    FDestDataSet: TDataSet;

    FConfig_ref: TConfig;
    
//    FOnLog: TStrNotifyEvent;

    FTable_ID_DB_ID_List: TResultStringList;

    FTableNameList: TStringList;
    FUsedFolderList: TStringList;

//    procedure LoadRefKey;
//    function CreateMDB1(aFileName: string): Boolean;

    procedure Create_table_from_dataset(aDataset: TDataSet; aTableName: String;
        aKeyFieldName : string);

    procedure CopyTable(aTableInfo: TTableInfo; aIsRefTable: boolean = false);
    procedure AddFolders;
    procedure CopyQuery(aSQL: string; aTableName: string; aDBItemList: TDBItemList);
    procedure LogDebug(aMsg: string);
    function TableNameRecordsExist(aTableName : string): Boolean;

    procedure CopyTable_selected(aTableInfo: TTableInfo);
//    procedure Log(aMsg: string);

//    procedure OpenRefKey;
  public
    FilterRecords_List: TStringList;

//    FTableNameList_Lib: TStringList;
 //   FTableNameList_Prop: TStringList;
  //  FTableNameList_Link: TStringList;

  //  FTableNameList_Allowed: TStringList;

//    FilterRecords_List: TStringList;

    Params: record
              Project_ID: integer;
              MDBFileName : string;

              TableName : string;


//              Src_ADOConnection: TADOConnection;
             // Dest_ADOConnection: TADOConnection;

            end;


    procedure ExecuteDlg;
    procedure ExecuteMain;

//    property OnLog: TStrNotifyEvent read FOnLog write FOnLog;

    class procedure Init;

  end;

var
  dmSQL_to_MDB1: TdmSQL_to_MDB1;


implementation
{$R *.DFM}

const
  TBL_LinkendType      = 'LinkendType';
  TBL_LinkendType_band = 'LinkendType_band';
  TBL_LinkendType_mode = 'LinkendType_mode';

  FLD_LinkendType_ID   = 'LinkendType_ID';



{
procedure TdmSQL_to_MDB.OpenRefKey;
const
  DEF_REF_KEY =
    'select '+
    '    object_name(fkeyid)  FK_Table,  '+
    '    c1.name 			        FK_Column, '+
    '    object_name(rkeyid)  PK_Table,  '+
    '    c2.name 			        PK_Column  '+
    ' from sysforeignkeys s              '+
    '   inner join syscolumns c1 on ( s.fkeyid = c1.id and s.fkey = c1.colid ) '+
    '   inner join syscolumns c2 on ( s.rkeyid = c2.id and s.rkey = c2.colid ) ';

begin

  db_OpenQuery(q_Ref_keys, DEF_REF_KEY);

end;
}

class procedure TdmSQL_to_MDB1.Init;
begin
  if not Assigned(dmSQL_TO_MDB1) then
    dmSQL_TO_MDB1 := TdmSQL_TO_MDB1.Create(Application);
end;


//------------------------------------------------------------------------------
procedure TdmSQL_to_MDB1.DataModuleCreate(Sender: TObject);
//------------------------------------------------------------------------------

var
  k: Integer;
begin
  inherited;

  db_SetComponentsADOConn([qry_Source, q_Ref_keys, q_PK], dmMain.ADOConnection);

  db_SetComponentsADOConn([tb_Dest], dmMDB.ADOConnection);

  FSrcDataSet := qry_Source;
  FDestDataSet:= tb_Dest;

  FUsedFolderList := TStringList.Create();
  FTableNameList := TStringList.Create();

  FTable_ID_DB_ID_List := TResultStringList.Create();

  FLogFileName := ChangeFileExt(Application.ExeName, '.log');


  FilterRecords_List := TStringList.Create();


  FDBTree := TDBItemList.Create;


//  FFKItemList:=TFKItemList.Create();
 // FPrimaryKeyList:= TPrimaryKeyList.Create();

  // -------------------------
//  FTableNameList_Lib:=TStringList.Create;

 // FTableNameList_Lib.Add(TBL_FOLDER);

//  FTableNameList_Lib.Add(TBL_AntennaType);
//  FTableNameList_Lib.Add(TBL_LinkendType);
//  FTableNameList_Lib.Add(LowerCase(TBL_LinkendType_band));
//  FTableNameList_Lib.Add(LowerCase(TBL_LinkendType_mode));

 // k:=FTableNameList_Lib.IndexOf(TBL_LinkendType_band);

{
  // -------------------------
  FTableNameList_Prop:= TStringList.Create;
  FTableNameList_Prop.Add(TBL_PROPERTY);
  FTableNameList_Prop.Add(TBL_LINKEND);

  // -------------------------
  FTableNameList_Link:= TStringList.Create;
  FTableNameList_Link.Add(TBL_LINK);


  FTableNameList_Allowed:= TStringList.Create;

  FTableNameList_Allowed.AddStrings(FTableNameList_Lib);}

end;


// ---------------------------------------------------------------
procedure TdmSQL_to_MDB1.DataModuleDestroy(Sender: TObject);
// ---------------------------------------------------------------
begin
  FreeAndNil(FDBTree);

  FreeAndNil(FilterRecords_List);

  FreeAndNil(FTableNameList);
  FreeAndNil(FUsedFolderList);
  FreeAndNil(FTable_ID_DB_ID_List);


//  FreeAndNil(FFKItemList);
//  FreeAndNil(FPrimaryKeyList);

//  FreeAndNil(FTableNameList_Lib);

end;



// --------------------------------------------------
procedure TdmSQL_to_MDB1.AddFolders;
// --------------------------------------------------

  // --------------------------------------------------
  procedure DoAddFolder(aFolderID: integer);
  // --------------------------------------------------
  var v: Variant;
  begin
    if FSrcDataSet.Locate(FLD_ID, aFolderID, []) then
    begin
      if not FDestDataSet.Locate(FLD_ID, aFolderID, []) then
        db_AddRecordFromDataSet(FSrcDataSet, FDestDataSet);


      //  AppendRecordFromDataset();

      v:=FSrcDataSet[FLD_PARENT_ID];

      if FSrcDataSet[FLD_PARENT_ID] <> Null then
        DoAddFolder(FSrcDataSet[FLD_PARENT_ID]);
    end;
  end;
  // --------------------------------------------------

var
  I: Integer;
begin
  if FUsedFolderList.Count=0 then
    Exit;

  db_OpenQuery(qry_Source, 'SELECT * FROM '+TBL_FOLDER);

  Create_table_from_dataset(qry_Source, TBL_FOLDER, FLD_ID);

  db_TableOpen1(tb_Dest, TBL_FOLDER);

  for I := 0 to FUsedFolderList.Count - 1 do
//    DoAddFolder (Integer(FUsedFolderList.Objects[i]));
    DoAddFolder (StrToInt(FUsedFolderList[i]));
end;

{
// -------------------------------------------------------------------
procedure TdmSQL_to_MDB.AppendRecordFromDataset;
// -------------------------------------------------------------------
begin
  db_AddRecordFromDataSet(FSrcDataSet, FDestDataSet);

//    db_AddRecordFromDataSet(aSrcDataset, aDestDataSet);

end;

}

// -------------------------------------------------------------------
procedure TdmSQL_to_MDB1.CopyTable(aTableInfo: TTableInfo; aIsRefTable: boolean
    = false);
// -------------------------------------------------------------------
var
  I: Integer;
  s: string;
  sTableName: string;
begin
  s := Format('SELECT * FROM %s', [aTableInfo.TableName]);

  if aTableInfo.UseProjectID then
  begin
    Assert(Params.Project_ID>0, 'Value <=0');

    s :=s+ Format(' WHERE (project_id=%d)', [Params.Project_ID]);
  end;

  db_OpenQuery(qry_Source, s);

  if qry_Source.RecordCount=0 then
    Exit;

//  if FTableNameList.IndexOf(aTableInfo.TableName)<0 then
    Create_table_from_dataset(qry_Source, aTableInfo.TableName, aTableInfo.KeyFieldName);

  db_TableOpen1(tb_Dest, aTableInfo.TableName);


  CopyDatasets (aTableInfo, aIsRefTable);


  qry_Source.Close;
  tb_Dest.Close;

  Log(FTable_ID_DB_ID_List.Text);

  FTable_ID_DB_ID_List.SaveToFile(FLogFileName);

  // ---------------------------------------------------------------

  // add ref records
  for I := 0 to aTableInfo.RefTables.Count - 1 do
  begin
    sTableName:=aTableInfo.RefTables[i].TableInfoRef.TableName;

    if TableNameRecordsExist(sTableName) then
      CopyTable(aTableInfo.RefTables[i].TableInfoRef, True)
    else
      LogDebug('No records: '+ sTableName);
  end;
end;



// -------------------------------------------------------------------
procedure TdmSQL_to_MDB1.CopyTable_selected(aTableInfo: TTableInfo);
// -------------------------------------------------------------------
var
  I: Integer;
  iID: Integer;
  iKeyID: Integer;
  j: Integer;
  k: Integer;
  sSQL: string;
  sTableName: string;

  oDBItem: TDBItem;
  sRefFieldName: string;
  sRefTableName: string;

  oTableInfo: TTableInfo;
  sKey: string;
  v: Variant;

  oRefList: TDBItemRefList;
  sID: string;
  sMasterTable: string;
  sSlaveTable: string;

const
  DEF_REF_ARR: array[0..4] of record
                        Master,Slave,RefFieldName : string;
                      end =
  (
   (Master:TBL_ClutterModel; Slave:TBL_ClutterModelItems;  RefFieldName: FLD_Clutter_Model_id ),
   (Master:TBL_LINKEND;      Slave:TBL_LINKEND_ANTENNA;    RefFieldName: FLD_linkend_id),
   (Master:TBL_LINK;         Slave:TBL_LINKCALCRESULTS;    RefFieldName: FLD_link_id),
   (Master:TBL_LINKENDTYPE;  Slave:TBL_LINKENDTYPE_MODE;   RefFieldName: FLD_LINKENDTYPE_id),
   (Master:TBL_LINKENDTYPE;  Slave:TBL_LINKENDTYPE_BAND;   RefFieldName: FLD_LINKENDTYPE_id)
  );

//    FDBTree.SelectByTableName(TBL_LINKEND, oRefList);

begin

  FDBTree.Clear;

//  FConfig_ref.

  for I := 0 to FilterRecords_List.Count - 1 do
    FDBTree.AddItem(aTableInfo.TableName, FilterRecords_List[i]);


//    g_

  //  SelectedIDList

 // FDBTree.ShowTest;

  // ---------------------------------------------------------------
  while FDBTree.FindNotExpanded(oDBItem) do
  // ---------------------------------------------------------------
  begin
    sKey := FConfig_ref.TableInfoList.FindByTableName(oDBItem.TableName).KeyFieldName;

    sSQL := Format('SELECT * FROM %s WHERE %s=''%s''', [oDBItem.TableName, sKey, VarToStr(oDBItem.Key)]);

    oDBItem.Expanded := True;

    CopyQuery(sSQL, oDBItem.TableName, oDBItem.SubItems);
  end;


//  FDBTree.ShowTest;

  // ---------------------------------------------------------------

  oRefList:=TDBItemRefList.Create;

  for j := 0 to High(DEF_REF_ARR) do
  begin
    oRefList.Clear;

    sMasterTable :=DEF_REF_ARR[j].Master;
    sSlaveTable  :=DEF_REF_ARR[j].Slave;
    sRefFieldName:=DEF_REF_ARR[j].RefFieldName;

    FDBTree.SelectByTableName(sMasterTable, oRefList);

    if oRefList.Count>0 then
    begin
      sID := oRefList.GetID();

      sSQL := Format('SELECT * FROM %s WHERE %s in (%s) ', [sSlaveTable, sRefFieldName, sID ]);

      for I := 0 to oRefList.Count - 1 do
        oRefList[i].Expanded := True;

      CopyQuery(sSQL, sSlaveTable, FDBTree);
    end;
  end;

  
  FreeAndNil(oRefList);

 // FDBTree.ShowTest;


 // ---------------------------------------------------------------
  while FDBTree.FindNotExpanded(oDBItem) do
  // ---------------------------------------------------------------
  begin
    oDBItem.Expanded := True;

    sKey := FConfig_ref.TableInfoList.FindByTableName(oDBItem.TableName).KeyFieldName;

    sSQL := Format('SELECT * FROM %s WHERE %s=''%s''', [oDBItem.TableName, sKey, VarToStr(oDBItem.Key)]);

    CopyQuery(sSQL, oDBItem.TableName, oDBItem.SubItems);

  end;

 // FDBTree.ShowTest;
                      
  AddFolders;

end;




// -------------------------------------------------------------------
procedure TdmSQL_to_MDB1.CopyQuery(aSQL: string; aTableName: string;
    aDBItemList: TDBItemList);
// -------------------------------------------------------------------
var
  iID: Integer;
  j: Integer;
  k: Integer;
  sKey: string;
  v: Variant;

  oTableInfo: TTableInfo;
  sRefFieldName: string;
  sRefTableName: string;
begin
// db_OpenQuery(qry_Source, 'select * from '+ aTableName +' where 1=0');
 // db_CreateFieldsForDataset(qry_Source);

 // db_View(qry_Source);


  db_OpenQuery(qry_Source, aSQL);

  if qry_Source.RecordCount=0 then
    Exit;

  oTableInfo :=FConfig_ref.TableInfoList.FindByTableName(aTableName);

//    sSQL := Format('SELECT * FROM %s WHERE id=%d', [aTableInfo.TableName, iID]);


  sKey := oTableInfo.KeyFieldName;// dmConfig.ConfigObj.TableInfoList.GetKeyFieldName(aTableName);

//  if FTableNameList.IndexOf(aTableName)<0 then
  Create_table_from_dataset(qry_Source, aTableName, sKey);

//  Create_table_from_dataset(qry_Source, aTableName, '');

  db_TableOpen1(tb_Dest, aTableName);


  while not qry_Source.Eof do
  begin
    if Assigned(qry_Source.FindField(FLD_FOLDER_ID)) then
    begin
      v:=qry_Source[FLD_FOLDER_ID];

      if not VarIsNull(v) then
        FUsedFolderList.Add(VarToStr(v));

//            FUsedFolderList.AddObject(IntToStr(iFolderID), Pointer(iFolderID));

    end;

  //  iID :=qry_Source[FLD_ID];

    v:=tb_Dest.Lookup (sKey, qry_Source[sKey], sKey);

    if VarIsNull(v) then
    begin
   //   k:=qry_Source[sKey];
//      Log( Format('copy record: table:%s; id:%s', [aTableName, VarToStr(v)]));
      db_AddRecordFromDataSet(qry_Source, tb_Dest);


      for j := 0 to oTableInfo.RefTables.Count - 1 do
      begin
        sRefTableName:=oTableInfo.RefTables[j].TableInfoRef.TableName;
        sRefFieldName:=oTableInfo.RefTables[j].Master_FieldName;

        v:=qry_Source[sRefFieldName];
        if not VarIsNull(v) then
          aDBItemList.AddItem(sRefTableName, v);
      end;

    end;

    qry_Source.Next;
  end;

  qry_Source.Close;
  tb_Dest.Close;
end;



// -------------------------------------------------------------------
function TdmSQL_to_MDB1.GetTableCreateSQL(aDataset: TDataset; aTableName,
    aKeyFieldName: String): string;
// -------------------------------------------------------------------
var
  I: integer;
  oSList: TStringList;
  s1: string;
  oField: TField;

begin
  oSList:=TStringList.Create;

  for I := 0 to aDataset.FieldCount-1 do
  begin
    oField :=aDataset.Fields.Fields[I];

    s1:='['+oField.FieldName+']'+' '+
        db_TypeFieldToString11 (oField.DataType, oField.DataSize);

    if Eq(aKeyFieldName, oField.FieldName) then
      s1:=s1+ '  PRIMARY KEY';

    oSList.Add(s1);
  end;


(*  // ��� ������� ����������� ���� db_id
  if Assigned(aDataset.Fields.FindField('id')) and
     (not Assigned(aDataset.Fields.FindField('db_id')))
   then
    oSList.Add('[db_id] int');
*)

 // oSList.Add('checksum int');

(*
  if aMasterTableName<>'' then
  begin
    sKey:=dmMDB.GetTableKeyField1(aMasterTableName);

    s1:= Format('CONSTRAINT [FK_%s_%s] FOREIGN KEY' +
                '  ( [%s] ) REFERENCES [%s] ( [%s] ) ON DELETE CASCADE ON UPDATE CASCADE ',

      [aTableName, aMasterTableName, aMasterFieldName, aMasterTableName, sKey]);

  end;

*)

//              '  CONSTRAINT [FK_site_property] FOREIGN KEY '+
 //             '  ( [property_id] ) REFERENCES [property] ( [id] ) ON DELETE CASCADE ON UPDATE CASCADE '+



  oSList.Delimiter :=',';
  oSList.QuoteChar:=' ';
  s1:=oSList.DelimitedText;

  s1:='CREATE TABLE '+ aTableName +' (' + oSList.DelimitedText + ')';

  Result :=s1;

(*
 // DBManager.ExecCommand
  ('CREATE TABLE '+ 'Site' +
              ' ( id           identity PRIMARY KEY, '+
              '   db_id        int, '+


              '  CONSTRAINT [FK_site_property] FOREIGN KEY '+
              '  ( [property_id] ) REFERENCES [property] ( [id] ) ON DELETE CASCADE ON UPDATE CASCADE '+
              ')' ) ;

*)

  FreeAndNil(oSList);
end;


// -------------------------------------------------------------------
procedure TdmSQL_to_MDB1.Create_table_from_dataset(aDataset: TDataSet;
    aTableName: String; aKeyFieldName : string);
// -------------------------------------------------------------------
var
  S: string;
begin
  if FTableNameList.IndexOf(aTableName)>=0 then
    Exit;

  s:=GetTableCreateSQL (aDataset, aTableName, aKeyFieldName);

  g_Log.Add(s);

  dmMDB.ADOConnection.Execute(S);

//  ADOConnection_MDB.Execute(S);

  FTableNameList.Add(aTableName);
end;


procedure TdmSQL_to_MDB1.Log(aMsg: string);
begin
//  DbugIntf
//  DbugIntf.SendDebug(aMsg);

  g_Log.Msg(aMsg);
end;


procedure TdmSQL_to_MDB1.LogDebug(aMsg: string);
begin
  g_Log.Msg(aMsg);
end;


function TdmSQL_to_MDB1.TableNameRecordsExist(aTableName : string): Boolean;
begin
  Result := FTable_ID_DB_ID_List.SelectNotAppendedByTableName(aTableName) > 0;
end;



// -------------------------------------------------------------------
procedure TdmSQL_to_MDB1.CopyDatasets(aTableInfo: TTableInfo; aIsRefTable:
    boolean = false);
// -------------------------------------------------------------------
var
  bTerminated: Boolean;
  i: integer;
  v: Variant;
  bFound: Boolean;
begin
  Progress_SetProgressMsg2(aTableInfo.TableName);

  // -------------------------
  if aIsRefTable then
  // -------------------------
  begin
    i:=FTable_ID_DB_ID_List.SelectNotAppendedByTableName(aTableInfo.TableName);

    // ------------------------------------
    // ���������� ��������� �������
    // ------------------------------------
    if FTable_ID_DB_ID_List.Selection.Count>0 then
      for I := 0 to FTable_ID_DB_ID_List.Selection.Count - 1 do
      begin
        Progress_SetProgress2(i, FTable_ID_DB_ID_List.Selection.Count, bTerminated);

        v:=FTable_ID_DB_ID_List.Selection[i];

        if FSrcDataSet.Locate(aTableInfo.KeyFieldName, v, []) then
          CopyDataRecord(aTableInfo);
      end

  end else


  // ------------------------------------
  // ���������� ���� �������
  // ------------------------------------

  with FSrcDataSet do
    while not Eof do
    begin
      Progress_SetProgress2(FSrcDataSet.RecNo, FSrcDataSet.RecordCount, bTerminated);
      if bTerminated then  Exit;

      //get key field value
      v := FSrcDataSet[aTableInfo.KeyFieldName];

      if not FTable_ID_DB_ID_List.Exists(aTableInfo.TableName, v) then
        CopyDataRecord(aTableInfo);

      Next;
    end;
end;


// -------------------------------------------------------------------
procedure TdmSQL_to_MDB1.CopyDataRecord(aTableInfo: TTableInfo);
// -------------------------------------------------------------------
var
  v: Variant;
  oRefTable: TRefTable;
  I: Integer;
  sFieldName: string;
  iFolderID: integer;
  s: string;

begin
  db_AddRecordFromDataSet(FSrcDataSet, FDestDataSet);
//
 // AppendRecordFromDataset();

  v:=FSrcDataSet[aTableInfo.KeyFieldName];
  FTable_ID_DB_ID_List.SetValue(aTableInfo.TableName, v, v);

  // -------------------------
  if aTableInfo.UseFolderID then
  begin
    iFolderID := FSrcDataSet.FieldByName(FLD_FOLDER_ID).AsInteger;
//    FUsedFolderList.AddObject(IntToStr(iFolderID), Pointer(iFolderID));
    FUsedFolderList.Add(IntToStr(iFolderID));
  end;

  // -------------------------
  for I := 0 to aTableInfo.RefTables.Count - 1 do
  begin
    oRefTable :=aTableInfo.RefTables[i];

    sFieldName :=oRefTable.Master_FieldName;

    if Assigned(FSrcDataSet.Fields.FindField(sFieldName)) then
    begin
      v:=FSrcDataSet[sFieldName];
//      sTableName:=oRefTable.TableName;

      if not VarIsNull(v) then
        if not FTable_ID_DB_ID_List.Exists(oRefTable.TableName, v) then
        begin
          s:= Format('master: %s; slave: %s; value: %s',
             [aTableInfo.TableName, oRefTable.TableName, VarToStr(v)]);

          LogDebug (s);

          FTable_ID_DB_ID_List.SetValue(oRefTable.TableName, v, NULL);
        end;
    end;
  end;
end;


// ---------------------------------------------------------------
procedure TdmSQL_to_MDB1.ExecuteMain;
// ---------------------------------------------------------------
const
  bTerminated: Boolean=False;

var
  i: integer;
  oTableInfo: TTableInfo;
  sSQL: string;
  sTableName: string;
begin
  bTerminated :=False;

  FTableNameList.Clear;
  FUsedFolderList.Clear;
  FTable_ID_DB_ID_List.clear;


//  FFKItemList.LoadFromADO(q_Ref_keys);
 // FPrimaryKeyList.LoadFromADO(q_PK);


  if not dmMDB.CreateMDB(params.MDBFileName) then
    Exit;

 // TdmConfig.Init;
  dmConfig.LoadConfig;

 // Assert(Assigned(dmConfig), 'Value not assigned');


   //11(FConfig_ref);
  FConfig_ref :=dmConfig.ConfigObj;

  if Params.TableName<>'' then
    FConfig_ref.TablesChecked.Clear;


  if Params.TableName<>'' then
  begin
    oTableInfo :=FConfig_ref.TableInfoList.FindByTableName(Params.TableName);

    Assert(Assigned(oTableInfo), 'Value not assigned');

/////////////
  CopyTable_selected(oTableInfo);



  //  Exit;
  end;


//    else

  for I := 0 to FConfig_ref.TablesChecked.Count - 1 do
    if not bTerminated then
  begin
    sTableName := FConfig_ref.TablesChecked[i];

    Progress_SetProgressMsg1(sTableName);
    Progress_SetProgress1(i, FConfig_ref.TablesChecked.Count, bTerminated);

    oTableInfo :=FConfig_ref.TableInfoList.FindByTableName(sTableName);

    Assert(Assigned(oTableInfo), 'Value not assigned');

    CopyTable(oTableInfo);
  end;

  AddFolders;

  dmMDB.ADOConnection.Close;
end;


// ---------------------------------------------------------------
procedure TdmSQL_to_MDB1.ExecuteDlg;
// ---------------------------------------------------------------
var
  sFileName: string;
begin
  sFileName  := Params.MDBFileName;
(*
  if FileExists(sFileName) then
    if ConfirmDlg('���� ����������, ������������?') then
    begin
      if not DeleteFile (sFileName) then
      begin
        ShowMessage('���� �� ����� ���� �����������, ��� �������');
        Exit;
      end;
    end else
      Exit;

*)
  if FileExists(sFileName) then
    if not DeleteFile (sFileName) then
    begin
      ShowMessage('���� �� ����� ���� �����������, ��� �������');
      Exit;
    end;


  Progress_ExecDlg_proc(ExecuteMain);
end;



end.







//
//  for I := 0 to  FilterRecords_List.Count - 1 -100 do
//  begin
//    iID := StrToInt(FilterRecords_List[i]);
//
//    oDBItem := FDBTree.AddItem(aTableInfo.TableName, iID);
//
//    oDBItem.Expanded := True;
//
//
//    sSQL := Format('SELECT * FROM %s WHERE id=%d', [aTableInfo.TableName, iID]);
//
//    db_OpenQuery(qry_Source, sSQL);
//
//    if qry_Source.RecordCount=0 then
//      Continue;
//
//
////       if FTableNameList.IndexOf(aTableName)>=0 then
////    Exit;
//
//    k:=FTableNameList.IndexOf(aTableInfo.TableName);
//
//    if FTableNameList.IndexOf(aTableInfo.TableName)<0 then
//      Create_table_from_dataset(qry_Source, aTableInfo.TableName, aTableInfo.KeyFieldName);
//
//    db_TableOpen1(tb_Dest, aTableInfo.TableName);
//
//    CopyDatasets (aTableInfo, aIsRefTable);
//
//
//    qry_Source.First;
//
//      with qry_Source do
//        while not EOF do
//      begin
//
//        for j := 0 to aTableInfo.RefTables.Count - 1 do
//        begin
//          sRefTableName:=aTableInfo.RefTables[j].TableInfoRef.TableName;
//          sRefFieldName:=aTableInfo.RefTables[j].Master_FieldName;
//
//          v:=qry_Source[sRefFieldName];
//          if not VarIsNull(v) then
//            oDBItem.SubItems.AddItem(sRefTableName, v);
//
//
//        (*    if TableNameRecordsExist(sTableName) then
//           CopyTable(aTableInfo.RefTables[i].TableInfoRef, True)
//         else
//           LogDebug('No records: '+ sTableName);*)
//        end;
//
//        Next;
//      end;
//
//
//    qry_Source.Close;
//    tb_Dest.Close;
//
//
//
//    // ---------------------------------------------------------------
//    if eq(Params.TableName, TBL_Link+'1111') then
//    // ---------------------------------------------------------------
//    begin
//
//
//(*      oDBItem.SubItems.AddItem(TBL_LinkEnd, qry_Source[FLD_LinkEnd1_ID]);
//      oDBItem.SubItems.AddItem(TBL_LinkEnd, qry_Source[FLD_LinkEnd2_ID]);
//
//      oDBItem.SubItems.AddItem(TBL_PROPERTY, qry_Source[FLD_PROPERTY1_ID]);
//      oDBItem.SubItems.AddItem(TBL_PROPERTY, qry_Source[FLD_PROPERTY2_ID]);
//
//*)
//
//      sSQL := Format('SELECT * FROM LINKEND_ANTENNA WHERE linkend_id IN '+
//                     '(select linkend1_id FROM link WHERE id=%d)',
//        [iID]); //TBL_LINKEND_ANTENNA,
//
//      CopyQuery(sSQL, TBL_LINKEND_ANTENNA);
//
//      //TBL_AntennaType ------------------
//      sSQL := Format('select * from AntennaType '+
//                     '  WHERE id IN  ( SELECT id FROM LINKEND_ANTENNA WHERE id IN '+
//                     '(select linkend1_id FROM link WHERE id=%d) )',
//        [ iID]);
//
//      CopyQuery(sSQL, TBL_AntennaType);
//
//
//      //TBL_LINKENDType ------------------
//      sSQL := Format('select * from LinkendType '+
//                     '  WHERE id IN  ( SELECT distinct LinkendType_id FROM LINKEND WHERE id IN '+
//                     '(select linkend1_id FROM link WHERE id=%d) )',
//        [ iID]);
//
//      CopyQuery(sSQL, TBL_LINKENDType);
//
//
//
//
////      ShellExec_Notepad_temp(FTable_ID_DB_ID_List.Text);
//
//      sSQL := Format('SELECT * FROM LINKEND_ANTENNA WHERE linkend_id IN  '+
//                     '(select linkend2_id FROM link WHERE id=%d)',
//        [ iID]);
//
//      CopyQuery(sSQL, TBL_LINKEND_ANTENNA);
//
//
//      //TBL_AntennaType ------------------
//      sSQL := Format('select * from AntennaType '+
//                     '  WHERE id IN  ( SELECT id FROM LINKEND_ANTENNA WHERE id IN '+
//                     '(select linkend2_id FROM link WHERE id=%d) )',
//        [ iID]);
//
//      CopyQuery(sSQL, TBL_AntennaType);
//
//
//      //TBL_LINKENDType ------------------
//      sSQL := Format('select  * from LinkendType '+
//                     '  WHERE id IN  ( SELECT distinct LinkendType_id FROM LINKEND WHERE id IN '+
//                     '(select linkend2_id FROM link WHERE id=%d) )',
//        [ iID]);
//
//      CopyQuery(sSQL, TBL_LINKENDType);
//
////      ShellExec_Notepad_temp(FTable_ID_DB_ID_List.Text);
//
//    end else
//
//    // ---------------------------------------------------------------
//    if eq(Params.TableName, TBL_LinkendType) then
//    begin
//
//      sSQL := Format('SELECT * FROM %s WHERE %s=%d', [TBL_LinkendType_mode, FLD_LinkendType_ID, iID]);
//      CopyQuery(sSQL, TBL_LinkendType_mode);
//
//      sSQL := Format('SELECT * FROM %s WHERE %s=%d', [TBL_LinkendType_band, FLD_LinkendType_ID, iID]);
//      CopyQuery(sSQL, TBL_LinkendType_band);
//
//    end;
//
//  end;



{


const
   SQL_SEL_FK =
      'SELECT TOP 100 PERCENT                                                '+
      '   OBJECT_NAME(sf.rkeyid)    AS PK_TABLE_NAME,                            '+
      '   col_name(sf.rkeyid, rkey) AS pk_column_name,                        '+
      '   OBJECT_NAME(sf.fkeyid)    AS fk_table_name,                            '+
      '   col_name(sf.fkeyid, fkey) AS fk_column_name,                        '+
      '   ObjectProperty(constid, ''CnstIsUpdateCascade'') as IsUpdateCascade,   '+
      '   ObjectProperty(constid, ''CnstIsDeleteCascade'') as IsDeleteCascade,   '+
      '   OBJECT_NAME(constid) AS Name                                    '+
      'FROM  sysforeignkeys sf INNER JOIN                                    '+
      '      sysobjects so ON sf.fkeyid = so.id                              '+
      'WHERE (so.xtype = ''U'')                      '+
      ' AND (so.name not like ''[_][_]%'')           '+
      ' AND (so.name not like ''%[_][_]'')           '+
      'ORDER BY so.name                                                      ';


 SELECT TOP 100 PERCENT                                                
         OBJECT_NAME(sf.rkeyid)    AS PK_TABLE_NAME,                           
         col_name(sf.rkeyid, rkey) AS pk_column_name,                       
         OBJECT_NAME(sf.fkeyid)    AS fk_table_name,                           
         col_name(sf.fkeyid, fkey) AS fk_column_name,                        
         ObjectProperty(constid, 'CnstIsUpdateCascade') as IsUpdateCascade,  
         ObjectProperty(constid, 'CnstIsDeleteCascade') as IsDeleteCascade, 
         OBJECT_NAME(constid) AS Name                                   
      FROM  sysforeignkeys sf INNER JOIN                                    
            sysobjects so ON sf.fkeyid = so.id                              
      WHERE (so.xtype = 'U')                      
       AND (so.name not like '[_][_]%')           
       AND (so.name not like '%[_][_]')           

      ORDER BY fk_table_name
