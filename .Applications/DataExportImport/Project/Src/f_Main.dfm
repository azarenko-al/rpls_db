inherited frm_Main_1: Tfrm_Main_1
  Left = 1074
  Top = 271
  BorderStyle = bsDialog
  Caption = 'frm_Main_1'
  ClientHeight = 646
  ClientWidth = 585
  ParentFont = True
  OldCreateOrder = True
  Position = poDesktopCenter
  ExplicitLeft = 1074
  ExplicitTop = 271
  ExplicitWidth = 593
  ExplicitHeight = 674
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 393
    Width = 585
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  inherited pn_Buttons: TPanel
    Top = 611
    Width = 585
    PopupMenu = PopupMenu1
    ExplicitTop = 611
    ExplicitWidth = 585
    inherited Bevel1: TBevel
      Width = 585
      ExplicitWidth = 585
    end
    inherited Panel3: TPanel
      Left = 387
      Width = 198
      ExplicitLeft = 387
      ExplicitWidth = 198
      inherited btn_Ok: TButton
        Left = 32
        Top = 6
        ExplicitLeft = 32
        ExplicitTop = 6
      end
      inherited btn_Cancel: TButton
        Left = 113
        ExplicitLeft = 113
      end
    end
    object Button1: TButton
      Left = 0
      Top = 8
      Width = 75
      Height = 25
      Caption = 'test'
      TabOrder = 1
      Visible = False
      OnClick = Button1Click
    end
  end
  inherited pn_Top_: TPanel
    Width = 585
    ExplicitWidth = 585
    inherited Bevel2: TBevel
      Width = 585
      ExplicitWidth = 585
    end
    inherited pn_Header: TPanel
      Width = 585
      ExplicitWidth = 585
    end
  end
  object GroupBox1_Log: TGroupBox [3]
    Left = 0
    Top = 505
    Width = 585
    Height = 87
    Align = alBottom
    Caption = ' '#1046#1091#1088#1085#1072#1083' '#1089#1086#1073#1099#1090#1080#1081' '
    TabOrder = 2
    object RichEdit1: TRichEdit
      Left = 2
      Top = 15
      Width = 581
      Height = 70
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
  object pn_Main_full: TPanel [4]
    Left = 0
    Top = 60
    Width = 585
    Height = 333
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GroupBox_Project: TGroupBox
      Left = 0
      Top = 0
      Width = 585
      Height = 46
      Align = alTop
      Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1087#1088#1086#1077#1082#1090#1072' '#1074' DB:'
      TabOrder = 0
      DesignSize = (
        585
        46)
      object DBLookupComboBox1_Project: TDBLookupComboBox
        Left = 8
        Top = 17
        Width = 570
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        DropDownRows = 20
        KeyField = 'id'
        ListField = 'name'
        ListSource = ds_Projects
        TabOrder = 0
      end
    end
    object GroupBox_file: TGroupBox
      Left = 0
      Top = 153
      Width = 585
      Height = 40
      Align = alTop
      Caption = #1060#1072#1081#1083' '#1089' '#1087#1088#1086#1077#1082#1090#1086#1084' ('#1092#1086#1088#1084#1072#1090' MDB)'
      TabOrder = 1
      object FilenameEdit1: TFilenameEdit
        Left = 2
        Top = 15
        Width = 573
        Height = 23
        Align = alLeft
        Filter = 'All files (*.mdb)|*.mdb'
        Anchors = [akLeft, akTop, akRight, akBottom]
        NumGlyphs = 1
        TabOrder = 0
        Text = ''
        ExplicitHeight = 21
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 193
      Width = 449
      Height = 140
      ActivePage = TabSheet_Test
      Align = alLeft
      Style = tsFlatButtons
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
        object cxGrid1: TcxGrid
          Left = 0
          Top = 0
          Width = 420
          Height = 109
          Align = alLeft
          TabOrder = 0
          LookAndFeel.Kind = lfFlat
          object cxGrid1DBTableView2: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataModeController.GridMode = True
            DataController.DataModeController.SyncMode = False
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnFiltering = False
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Inserting = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.GroupByBox = False
            object col_ID: TcxGridDBColumn
              DataBinding.FieldName = 'id'
              Visible = False
            end
            object col_checked: TcxGridDBColumn
              Caption = ' '
              DataBinding.FieldName = 'checked'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.NullStyle = nssUnchecked
              Options.Sorting = False
              Width = 33
            end
            object col_Caption: TcxGridDBColumn
              Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103
              DataBinding.FieldName = 'caption'
              Options.Editing = False
              Options.Sorting = False
              Width = 333
            end
            object cxGrid1DBTableView2index: TcxGridDBColumn
              DataBinding.FieldName = 'index'
              Visible = False
              Width = 45
            end
            object cxGrid1DBTableView2DBColumn1: TcxGridDBColumn
              Caption = 'Filter'
              PropertiesClassName = 'TcxButtonEditProperties'
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.ReadOnly = True
              Visible = False
            end
            object col_TableName: TcxGridDBColumn
              DataBinding.FieldName = 'TableName'
              Visible = False
              Options.Editing = False
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView2
          end
        end
      end
      object TabSheet_Selected: TTabSheet
        Caption = #1042#1099#1073#1088#1072#1085#1086
        ImageIndex = 1
        object Memo1: TMemo
          Left = 0
          Top = 0
          Width = 513
          Height = 109
          Align = alLeft
          TabOrder = 0
        end
      end
      object TabSheet_Test: TTabSheet
        Caption = 'Test'
        ImageIndex = 2
        object Button2: TButton
          Left = 24
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Test'
          TabOrder = 0
          Visible = False
          OnClick = Button2Click
        end
      end
    end
    object GroupBox1_multi11: TGroupBox
      Left = 0
      Top = 46
      Width = 585
      Height = 107
      Align = alTop
      Caption = #1060#1072#1081#1083' '#1089' '#1087#1088#1086#1077#1082#1090#1086#1084' ('#1092#1086#1088#1084#1072#1090' MDB)'
      TabOrder = 3
      Visible = False
      object ListBox2: TListBox
        Left = 2
        Top = 15
        Width = 479
        Height = 90
        Align = alLeft
        ItemHeight = 13
        TabOrder = 0
      end
      object Button4: TButton
        Left = 496
        Top = 13
        Width = 75
        Height = 25
        Action = FileOpen1
        TabOrder = 1
      end
    end
  end
  object StatusBar1: TStatusBar [5]
    Left = 0
    Top = 592
    Width = 585
    Height = 19
    Panels = <>
    SimplePanel = True
    OnDblClick = StatusBar1DblClick
  end
  inherited ActionList1: TActionList
    Left = 432
  end
  inherited FormStorage1: TFormStorage
    StoredProps.Strings = (
      'GroupBox1_Log.Height'
      'FilenameEdit1.FileName')
    Left = 296
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 376
  end
  object qry_Projects: TADOQuery
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'SELECT id,name FROM PROJECT ORDER BY name')
    Left = 314
    Top = 423
  end
  object ds_Projects: TDataSource
    DataSet = qry_Projects
    Left = 372
    Top = 424
  end
  object PopupMenu1: TPopupMenu
    Left = 64
    Top = 424
    object Action11: TMenuItem
      Action = act_Import_Project_
    end
    object Action31: TMenuItem
      Action = act_Export_Project_
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Action21: TMenuItem
      Action = act_Import_Guides_
    end
    object Action41: TMenuItem
      Action = act_Export_Guides_
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object spDictionaryClear1: TMenuItem
      Caption = 'sp_Dictionary_Clear'
    end
    object sfdf1: TMenuItem
      Caption = 'sp_Project_Clear'
    end
  end
  object ActionList2: TActionList
    Left = 120
    Top = 425
    object act_Import_Project_: TAction
      Caption = 'Import_Project'
    end
    object act_Import_Guides_: TAction
      Caption = 'Import_Guides'
    end
    object act_Export_Project_: TAction
      Caption = 'Export_Project'
    end
    object act_Export_Guides_: TAction
      Caption = 'Export_Guides'
    end
  end
  object ActionList3: TActionList
    Left = 512
    Top = 154
    object FileOpen1: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Dialog.Filter = '*.mdb'
      Dialog.Options = [ofHideReadOnly, ofAllowMultiSelect, ofPathMustExist, ofFileMustExist, ofEnableSizing]
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
    end
  end
end
