unit f_Main_import_lib;

interface

uses
  DB, SysUtils, Classes, Controls, Forms,
  ComCtrls, Dialogs, StdCtrls, ActnList, ExtCtrls,  rxToolEdit,

  IOUtils,
  
  fra_DB_Login,

  u_files,

//  u_cx_Grid,

  u_log,

  u_SQL,

//  I_DB_login,
//  u_db,

  dm_Main_Data,


//  d_Project_Browse1,

//  dm_MDB,

//  dm_SQL_to_MDB_new,

 // dm_SQL_to_MDB,
  dm_MDB_to_SQL,

  d_Wizard,

  u_func,
  
  u_dlg,
  

  u_ini_data_export_import_params,


  StdActns, Mask, cxPropertiesStore, rxPlacemnt, CheckLst, cxClasses,
  System.Actions;

type
  Tfrm_Main_Import_Lib = class(Tdlg_Wizard)
    pn_Main_full: TPanel;
    GroupBox_file: TGroupBox;
    DirectoryEdit1: TDirectoryEdit;
    Button1: TButton;
    gb_ConnectToSQL: TGroupBox;
    CheckListBox1: TCheckListBox;
    procedure act_OkExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure DirectoryEdit1AfterDialog(Sender: TObject; var Name: string; var
        Action: Boolean);
//    procedure FormDestroy(Sender: TObject);
//    procedure act_Import_Project_Execute(Sender: TObject);
//    procedure act_OkExecute(Sender: TObject);
//    procedure Button1Click(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
//    procedure FileOpen1Accept(Sender: TObject);
//    procedure FileOpen1BeforeExecute(Sender: TObject);
  //  procedure Button2Click(Sender: TObject);

    procedure FormCreate(Sender: TObject);
//    procedure sfdf1Click(Sender: TObject);
//    procedure spDictionaryClear1Click(Sender: TObject);
//    procedure StatusBar1DblClick(Sender: TObject);
//    procedure cxGrid1DBTableView2DBColumn1PropertiesButtonClick(
//      Sender: TObject; AButtonIndex: Integer);
//    procedure cxGrid1Exit(Sender: TObject);
  //  procedure FormClose(Sender: TObject; var Action: TCloseAction);
//    procedure FormActivate(Sender: TObject);
  private

  //  FDataSet: TDataset;


 //   FMode: TExportModeType;

    FParams_ref: TIni_Data_export_import_params;

    Fframe_DB_Login: Tframe_DB_Login;

    procedure ImportData;
    procedure Refresh_files;

//    procedure ExportData1;
//    procedure ExportData_new;

//    function GetProjectID: Integer;
 //   procedure OpenProjects;

  //  procedure Log(aValue: string);


  public
    //procedure SetDisplayMode(aValue: TExportModeType);

    //aMode: TExportModeType; aProjectID : Integer;
//    procedure Init(aParams :TIni_Data_export_import_params);
//    procedure Init;
  end;

var
  frm_Main_Import_Lib: Tfrm_Main_Import_Lib;

implementation

//uses dm_Test_Export_Import;

{$R *.DFM}

procedure Tfrm_Main_Import_Lib.act_OkExecute(Sender: TObject);
begin
  ImportData();
//  ImportData();

  
end;

procedure Tfrm_Main_Import_Lib.Button1Click(Sender: TObject);
begin
  Refresh_files
end;

procedure Tfrm_Main_Import_Lib.DirectoryEdit1AfterDialog(Sender: TObject; var
    Name: string; var Action: Boolean);
begin
  Refresh_files

end;

// ---------------------------------------------------------------
procedure Tfrm_Main_Import_Lib.Refresh_files;
// ---------------------------------------------------------------
var
  i: Integer;
begin
//  ScanDir1( DirectoryEdit1.Text, '*.mdb', ListBox1.Items);

  if not TDirectory.Exists(DirectoryEdit1.Text) then
    Exit;

  ScanDir( DirectoryEdit1.Text, '*.mdb', CheckListBox1.Items);

  for i:=CheckListBox1.Items.count-1 downto 0 do
   if Pos('.temp.', CheckListBox1.Items[i])>0 then
     CheckListBox1.Items.Delete(i);
                                    

  
  for i:=0 to CheckListBox1.Items.count-1 do
    CheckListBox1.Checked[i]:=true;

end;



//-----------------------------------------------------------
procedure Tfrm_Main_Import_Lib.FormCreate(Sender: TObject);
//-----------------------------------------------------------
var
  s: string;
begin
  inherited;


  CreateChildForm_(Tframe_DB_Login, Fframe_DB_Login, gb_ConnectToSQL);

  Fframe_DB_Login.IniFileName := ChangeFileExt(Application.ExeName, '.ini');
  Fframe_DB_Login.LoadFromIni();



//  cx_Grid_Init (Self);


 // Caption :='������-������� ������ '+ GetAppVersionStr();


  Caption:= '������ ������������ '+  GetAppVersionStr() + '  ������: '
    + FormatDateTime ('yyyy-mm-dd hh:mm', GetFileDate (Application.Exename));


  SetActionName ('������ ������������ � SQL-������');

//  PageControl1.Align:=alClient;

  CheckListBox1.Align:=alClient;
  pn_Main_full.Align:=alClient;

 // GroupBox1_Log.Align:=alClient;

  act_Ok.Caption:='���������';
  act_Cancel.Caption:='�������';

//  cxGrid1.Align:=alClient;

// .. AddComponentProp(ed_FileName, 'FileName');

 // StatusBar1.SimpleText:=dmMain.LoginRec.ConnectionStatusStr;

 // SetDefaultWidth;

(*  if FParams_ref.MdbFileName<>'' then
    ed_FileName.FileName := FParams_ref.MdbFileName;
*)

 // TdmConfig.Init;
//  cxGrid1DBTableView2.DataController.DataSource :=dmConfig.ds_Display_Groups;

//  TabSheet_Test.TabVisible:=False;


//  FDataSet:=dmConfig.ds_Display_Groups.DataSet;


//  dmConfig.SetDisplayMode(mtPROJECT);

//  dmMDB.SetDisplayMode (mtPROJECT);

 // SetDisplayMode(mtImport_PROJECT);
//  SetDisplayMode (mtGUIDES);

 // DBLookupComboBox1.KeyValue :=qry_Projects['id'];

//  g_Log.RichEdit := RichEdit1;


  //  C:\111111111111111111111111111111111111111\�����������_80GHz\E-Band\EBand_Ant_80GHz.temp.mdb

//  GroupBox1_multi.Visible:= g_Ini_Data_export_import_params.Mode in [mtImport_GUIDES,mtImport_PROJECT];
//  GroupBox_file.Visible  := not GroupBox1_multi.Visible;


//s:=ExtractFilePath(Application.ExeName);
//s:=ExtractFilePath(ExtractFileDir(Application.ExeName));



   if DirectoryEdit1.Text = '' then
//     DirectoryEdit1.Text := GetParentFileDir(Application.ExeName) + 'equipment';
     DirectoryEdit1.Text := ExtractFilePath(ExtractFileDir(Application.ExeName) ) + 'equipment';


   Refresh_files;

end;



//------------------------------------------------------------------------------
procedure Tfrm_Main_Import_Lib.ImportData;
//------------------------------------------------------------------------------
var
  i: Integer;
  iProjectID: Integer;
  r: TdbLoginRec;
begin
  Fframe_DB_Login.SaveToIni();
                              
  r:= Fframe_DB_Login.GetLoginRec();

  dmMain_Data.OpenADOConnectionString (db_ADO_MakeConnectionString (r));
  
//  dmMain_Data.ADOConnection_SQL.ConnectionString:=
//  oADOConnection.DefaultDatabase:=FLoginRec.DataBase;

//  sConnectionString :=ADOConnection1.ConnectionString;

  try
  //  dmMain_Data.ADOConnection.Open;
  except
  end;

//  if not dmMain.OpenDB_rec(r) then
//    Exit;



//      aADOConnection.Close;
//  aADOConnection.ConnectionString:=db_ADO_MakeConnectionString (aLoginRec);


//  assert(FilenameEdit1.FileName <> '', 'ed_FileName.FileName = _');


{
function TdmMain_SQL.OpenDB(aRec: TdbLoginRec): Boolean;
begin
  LoginRec:=aRec;

  //TdmConnection.Init;
  Result := dmConnection1.OpenDatabase (aRec, ADOConnection1);

//  ADOConnection1.Da

//  ADOConnection1.DefaultDatabase :=aRec.DataBase;

end;


 FilenameEdit1.FileName:=ChangeFileExt(FilenameEdit1.FileName, '.mdb');


  if not FileExists(FilenameEdit1.FileName) then
  begin
    ShowMessage('���� �� ����������...' + FilenameEdit1.FileName);

    exit;
  end;
}


(*  if not dmMDB.OpenMDB (ed_FileName.FileName) then
    Exit;
*)

//  iProjectID := AsInteger(DBLookupComboBox1_Project.KeyValue);


 // dmMDB_to_SQL.OnLog :=Log;
  TdmMDB_to_SQL.Init;
  dmMDB_to_SQL.Params.Dest_ADOConnection:= dmMain_Data.ADOConnection_SQL;

//  dmMDB_to_SQL.OnLog := Log;

//  dmMDB_to_SQL.Params.Dest_ProjectID:=iProjectID;
 //

//   for i:=0 to CheckListBox1.Items.count-1 do
//    CheckListBox1.Checked[i]:=true;


  for i:=0 to CheckListBox1.Items.Count-1 do
    if CheckListBox1.Checked[i] then
    begin
      dmMDB_to_SQL.Params.MDB_FileName :=CheckListBox1.Items[i];

      dmMDB_to_SQL.ExecuteDlg;
    end;


///////////////////////////////////////////////////////////
  for i:=0 to CheckListBox1.Items.Count-1 do
    if CheckListBox1.Checked[i] then
    begin
      dmMDB_to_SQL.Params.MDB_FileName :=CheckListBox1.Items[i];

      dmMDB_to_SQL.ExecuteDlg;
    end;




 // dmMDB_to_SQL.Params.IsRefreshRecords:= cb_UpdateExistingObjects.Checked;//  AsBoolean(row_UpdateExistent.Text)

//  dmMDB_to_SQL.ExecuteProc;

  FreeAndNil(dmMDB_to_SQL);

//  dmMDB.ADOConnection.Close;

  MsgDlg('���������');
end;





end.




{



//-----------------------------------------------------------
procedure Tfrm_Main_Import_Lib.act_OkExecute(Sender: TObject);
//-----------------------------------------------------------
begin
  case FParams_ref.Mode of
    mtImport_PROJECT,
    mtImport_Guides:  ImportData;

    mtExport_selected,
    mtExport_PROJECT,
    mtExport_Guides: ExportData_new;

      //ExportData;// (DBLookupComboBox1.KeyValue);
  else

    raise Exception.Create('FMode=none');
//    ShowMessage('FMode=none');
  end;
end;



// ---------------------------------------------------------------
procedure Tfrm_Main_Import_Lib.SetDisplayMode(aValue: TExportModeType);
// ---------------------------------------------------------------
var
  b: Boolean;
begin
//  FMode :=aValue;

  dmConfig.SetDisplayMode (aValue);

  b:=aValue in [mtExport_PROJECT, mtImport_PROJECT];

  GroupBox_Project.visible  := b;
  if b then
    OpenProjects();

  b:=aValue in [mtImport_PROJECT, mtImport_GUIDES];

  if aValue = mtExport_Selected then
    PageControl1.ActivePageIndex :=1
  else
    PageControl1.ActivePageIndex :=0;


//  PageControl1

 // pn_Main.Visible :=not b;



 
//-----------------------------------------------------------
procedure Tfrm_Main_Import_Lib.ExportData_new;
//-----------------------------------------------------------
var
  I: Integer;
  iProjectID: Integer;
begin
  FilenameEdit1.FileName:=ChangeFileExt(FilenameEdit1.FileName, '.mdb');


 // db_PostDataset(cxGrid1DBTableView2.DataController.DataSource.DataSet);
//  iProject_ID:=AsInteger(DBLookupComboBox1.KeyValue);
 // dmSQL_to_MDB.Params.Src_ADOConnection := dmMain.ADOConnection;
 // dmSQL_to_MDB.Params.Dest_ADOConnection:= dmMDB.ADOConnection;
 // dmSQL_to_MDB.OnLog :=Log;

  iProjectID := AsInteger(DBLookupComboBox1_Project.KeyValue);

  db_PostDataset(dmConfig.ds_Display_Groups.DataSet);


  TdmSQL_to_MDB_new.Init;

  if FParams_ref.TableName<>'' then
  begin
    dmSQL_to_MDB_new.params.TableName:=FParams_ref.TableName;

   // dmSQL_to_MDB_new.params.FilterRecords_List.Clear;

    dmSQL_to_MDB_new.Params.IDS:=FParams_ref.SelectedIDList.MakeDelimitedText_ID;


//    for I := 0 to FParams_ref.SelectedIDList.Count - 1 do
//      dmSQL_to_MDB_new.FilterRecords_List.Add(IntToStr(FParams_ref.SelectedIDList[i].id));


  end;


  dmSQL_to_MDB_new.params.MDBFileName:=FilenameEdit1.FileName;
  dmSQL_to_MDB_new.params.Project_ID:=iProjectID;
  dmSQL_to_MDB_new.ExecuteDlg;

  FreeAndNil(dmSQL_to_MDB_new);

//  MsgDlg('���������');



  MsgDlg('���������');



end;



procedure Tfrm_Main_Import_Lib.Button1Click(Sender: TObject);
begin
//  ExportData_new;
end;

procedure Tfrm_Main_Import_Lib.Button2Click(Sender: TObject);
begin
 // TdmTest_Export_Import.Init;
  //dmTest_Export_Import.Exec;
end;





procedure Tfrm_Main_Import_Lib.FileOpen1Accept(Sender: TObject);
var
  i: Integer;
begin

{
  ListBox1.Items.Clear;


  for i:=0 to FileOpen1.Dialog.Files.Count-1 do
    if RightStr(FileOpen1.Dialog.Files[i], Length('.temp.mdb')) <> '.temp.mdb'
      ListBox1.Items.Add(FileOpen1.Dialog.Files[i]);
}

//  ShowMessage ('');

end;

 

}


{

procedure Tfrm_Main_Import_Lib.Log(aValue: string);
begin
  RichEdit1.Lines.Add(aValue);
end;

  if not dmMain_SQL.OpenDB(Fframe_DB_Login.GetLoginRec()) then
    Exit;


    

// ---------------------------------------------------------------
procedure Tfrm_Main_Import_Lib.Init;
// ---------------------------------------------------------------
//var
//  I: Integer;
begin
 // FParams_ref := g_Ini_Data_export_import_params;

//  SetDisplayMode(FParams_ref.Mode);

 // DBLookupComboBox1_Project.KeyValue :=FParams_ref.ProjectID;



{
  if FParams_ref.TableName<>'' then
  begin
    Memo1.Lines.Add( FParams_ref.TableName);

    for I := 0 to FParams_ref.SelectedIDList.Count - 1 do
      Memo1.Lines.Add( IntToStr(FParams_ref.SelectedIDList[i].ID));
  end;

}

{
  if FParams_ref.FileName_MDB<>'' then
    begin
   //   ShowMessage(FParams_ref.MdbFileName);
//      FilenameEdit1.FileName := FParams_ref.FileName_MDB;


      if FParams_ref.IsAuto then
      begin
        act_Ok.Execute;
        Close;
      end;

    end;

}

end;



}
