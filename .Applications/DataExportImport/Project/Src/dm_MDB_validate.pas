unit dm_MDB_validate;

interface

uses
  SysUtils, Forms, Classes, DB, ADODB,  Dialogs,

  u_db,
  u_log,

  u_func,
  u_func_arrays,

  dm_Config,
  dm_MDB,

  u_Config_classes;

type
  TdmMDB_validate = class(TDataModule)
    ADOQuery1: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FConfig_ref: TConfig;

    FUpdateSQL : TStringList;

    FFieldList: TStringList;

    procedure CheckAntennas;
    function SrcTableExists(aTableName: string): Boolean;

    procedure UpdateSrcTable;
    procedure UpdateSrcTables;

  public
    procedure Execute;

    class procedure Init;

  end;

var
  dmMDB_validate: TdmMDB_validate;

implementation
{$R *.dfm}

{const
  TBL_ANTENNATYPE = 'ANTENNATYPE';

}


class procedure TdmMDB_validate.Init;
begin
  if not Assigned(dmMDB_validate) then
    dmMDB_validate:=TdmMDB_validate.Create(Application);
end;

// ---------------------------------------------------------------
procedure TdmMDB_validate.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  db_SetComponentADOConnection(Self, dmMDB.ADOConnection);

  FUpdateSQL := TStringList.Create();
  FFieldList := TStringList.Create();

end;


procedure TdmMDB_validate.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FFieldList);

end;


function TdmMDB_validate.SrcTableExists(aTableName: string): Boolean;
begin
  Result := dmMDB.TableExists(aTableName);
end;

// ---------------------------------------------------------------
procedure TdmMDB_validate.UpdateSrcTable;
// ---------------------------------------------------------------
// modify field names
// ---------------------------------------------------------------
var
  I: Integer;
  j: Integer;
  k: Integer;
  sDataType: string;
  sName_new: string;
  sName_old: string;
  sSQL: string;
  sTableName: string;
  oFieldReplace: TFieldReplace;

  oStrArr: TStrArray;
  s: string;



begin
  FUpdateSQL.Clear;


  for I := 0 to dmMDB.TableNameList.Count - 1 do
  begin
    sTableName:=dmMDB.TableNameList[i];

    ADOQuery1.Close;
    ADOQuery1.SQL.Text := Format('SELECT top 1 * FROM [%s]', [sTableName]);
    ADOQuery1.Open;

    ADOQuery1.GetFieldNames(FFieldList);

    {
    if Eq (sTableName, TBL_ANTENNATYPE) then
    begin
      if (FFieldList.IndexOf('polarization_str')>=0) then
      begin
        s:= Format('ALTER TABLE [%s] ALTER COLUMN %s varchar(4) ', [sTableName, 'polarization_str']);
        FUpdateSQL.Add(s);
      end;

    end;
      
   }


{    if (FFieldList.IndexOf('project_id')>=0) then
      FUpdateSQL.Add(Format('alter table [%s] drop column [project_id] ', [sTableName]));
}


    if (FFieldList.IndexOf('id')>=0) and
       (FFieldList.IndexOf('db_id')<0)
    then begin
 //     sSQL :=Format('alter table [%s] add column [db_id] int', [sTableName]);

//        sSQL :=Format('alter table %s drop column %s;', [sTableName, sName_new]);

   //   dmMDB.ADOConnection.Execute(sSQL);

      FUpdateSQL.Add(Format('alter table [%s] add column [db_id] int', [sTableName]));
    end;

  end;




  // --------------------------------------
  //  polarization
  // --------------------------------------
//  FConfig_ref.FieldReplaceList.SelectByTableName()



  for I := 0 to FConfig_ref.FieldReplaceList.Count-1 do
  begin
    oFieldReplace:=FConfig_ref.FieldReplaceList[i];

    sTableName :=oFieldReplace.TableName;

    if not SrcTableExists(sTableName) then
      Continue;

    ADOQuery1.Close;
    ADOQuery1.SQL.Text := Format('SELECT top 1 * FROM [%s]', [sTableName]);
    ADOQuery1.Open;

    ADOQuery1.GetFieldNames(FFieldList);


//    db_View(ADOQuery1);
    

(*
    ADOTable1.Close;
    ADOTable1.TableName := sTableName;


    ADOTable1.GetFieldNames(oFieldList);
*)

  //  k:=oFieldList.Count;

  //  if not Eq(sTableName, aTableInfo.TableName) then
  //    continue;


    sName_old :=oFieldReplace.Name_old;
    sName_new :=oFieldReplace.Name_new;
    sDataType :=oFieldReplace.DataType;

    if (FFieldList.IndexOf(sName_old)<0) then
      Continue;
                   

    if FFieldList.IndexOf(sName_new)>=0 then
    begin
      sSQL :=Format('alter table [%s] drop column [%s]', [sTableName, sName_new]);

//        sSQL :=Format('alter table %s drop column %s;', [sTableName, sName_new]);

   //   dmMDB.ADOConnection.Execute(sSQL);

      FUpdateSQL.Add(sSQL);
    end;


    sSQL :=Format('alter table  [%s] add column [%s] %s', [sTableName, sName_new,sDataType]);
    FUpdateSQL.Add(sSQL);

  //  dmMDB.ADOConnection.Execute(sSQL);


    sSQL :=Format('UPDATE [%s] SET [%s] = [%s]', [sTableName, sName_new, sName_old]);
    FUpdateSQL.Add(sSQL);


    if oFieldReplace.SQL.Text<>'' then
    begin
      ostrArr :=StringToStrArray(oFieldReplace.SQL.Text, ';');

      for j := 0 to High(ostrArr) do
      begin
    //        sSQL :=Format('alter table [%s] drop column [%s]', [sTableName, sName_old]);
         FUpdateSQL.Add(ostrArr[j]);
      end;
    end;
//      oStrArr

  //  dmMDB.ADOConnection.Execute(sSQL);

    sSQL :=Format('alter table [%s] drop column [%s]', [sTableName, sName_old]);
    FUpdateSQL.Add(sSQL);


    //  dmMDB.ADOConnection.Execute(sSQL);
  end;


//  ShowMessage(FUpdateSQL.Text);


///////
//  ShellExec_Notepad_temp1(FUpdateSQL.Text);



  for I := 0 to FUpdateSQL.count - 1 do
  begin
//    g_Log.Add (FUpdateSQL[i]);

    try
      dmMDB.ADOConnection.Execute(FUpdateSQL[i]);
    except
      ShowMessage(FUpdateSQL[i]);
    end;
  end;




end;

// ---------------------------------------------------------------
procedure TdmMDB_validate.CheckAntennas;
// ---------------------------------------------------------------
const
  SQL_UPDATE_AntennaXREF =
     'SELECT AntennaXREF.linkend_id, Antenna.* into Linkend_Antenna '+
     'FROM Antenna RIGHT JOIN AntennaXREF ON Antenna.id = AntennaXREF.antenna_id ';

var
  I: Integer;
  k: Integer;
  sDataType: string;
  sTableName_new: string;
  sTableName_old: string;
  sSQL: string;
  s: string;

begin

  // ---------------------------------------------------------------
  if SrcTableExists('antenna') and
     SrcTableExists('antennaXREF') then
  begin
    if SrcTableExists('linkend_antenna') then
       FUpdateSQL.Add('drop table linkend_antenna');

    FUpdateSQL.Add(SQL_UPDATE_AntennaXREF);

    FUpdateSQL.Add('drop table antenna');
    FUpdateSQL.Add('drop table antennaXREF');
  end;

  // ---------------------------------------------------------------
  if SrcTableExists('antenna') and
    (not SrcTableExists('antennaXREF')) then
  begin
//    s :=Format('alter table %s RENAME TO %s', ['antenna','linkend_antenna']);
//    FUpdateSQL.Add(s);

    s :=Format('select * into [%s] from [%s]', ['linkend_antenna','antenna']);
    FUpdateSQL.Add(s);

    s :=Format('drop table [%s] ', ['antenna']);
    FUpdateSQL.Add(s);

  end;

end;




// ---------------------------------------------------------------
procedure TdmMDB_validate.UpdateSrcTables;
// ---------------------------------------------------------------

{const
  SQL_UPDATE_AntennaXREF =
     'SELECT AntennaXREF.linkend_id, Antenna.* into Linkend_Antenna '+
     'FROM Antenna RIGHT JOIN AntennaXREF ON Antenna.id = AntennaXREF.antenna_id ';
}

var
  I: Integer;

{
  k: Integer;
  sDataType: string;
  sTableName_new: string;
  sTableName_old: string;
  sSQL: string;
}
  s: string;

begin
  CheckAntennas;

  {
  // ---------------------------------------------------------------
  if SrcTableExists('antenna') and
     SrcTableExists('antennaXREF') then
  begin
    if SrcTableExists('linkend_antenna') then
       FUpdateSQL.Add('drop table linkend_antenna');

    FUpdateSQL.Add(SQL_UPDATE_AntennaXREF);

    FUpdateSQL.Add('drop table antenna');
    FUpdateSQL.Add('drop table antennaXREF');
  end;

  // ---------------------------------------------------------------
  if SrcTableExists('antenna') and
    (not SrcTableExists('antennaXREF')) then
  begin
//    s :=Format('alter table %s RENAME TO %s', ['antenna','linkend_antenna']);
//    FUpdateSQL.Add(s);

    s :=Format('select * into [%s] from [%s]', ['linkend_antenna','antenna']);
    FUpdateSQL.Add(s);

    s :=Format('drop table [%s] ', ['antenna']);
    FUpdateSQL.Add(s);

  end;
  }
  

  for I := 0 to FUpdateSQL.count - 1 do
  begin
    s:=FUpdateSQL[i];

   // g_Log.Add ('MDB: '+ FUpdateSQL[i]);
    try
      dmMDB.ADOConnection.Execute(FUpdateSQL[i]);

    except
      ShowMessage(s);
    end;

  end;

end;



procedure TdmMDB_validate.Execute;
begin
  FConfig_ref:=dmConfig.ConfigObj;

  FUpdateSQL.Clear;

//  CheckAntennas;

  UpdateSrcTables;

  dmMDB.RefreshTableNames;

  UpdateSrcTable;

end;

end.
