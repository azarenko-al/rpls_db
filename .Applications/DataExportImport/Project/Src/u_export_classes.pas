unit u_export_classes;

interface

uses
  Classes, SysUtils, DB, Variants, 
                                 
  System.Generics.Collections, 
  
  u_files,                                   

  u_func;

 

type
  TFolderList  =class;

  TDataRec = record
    TableName: string;
    Key: variant;
  end;

  TStringArr = array of string;


  TResultItem = class // (TCollectionItem)
  public
    TableName: string;
    SrcKey: Variant;
    DestKey: Variant;
  end;



  // ---------------------------------------------------------------
  TResultItemList = //class(TCollection)
                    class(TList<TResultItem>)

  // ---------------------------------------------------------------
  private
 //   function GetItems(Index: Integer): TResultItem;

  public
//    constructor Create;

    function AddItem: TResultItem;

   // property Items[Index: Integer]: TResultItem read GetItems; default;
  end;



  // ---------------------------------------------------------------
  TResultStringList = class(TStringList)
  // ---------------------------------------------------------------
  private
    function GetValueEx(Index: integer): TDataRec;
  public
    Selection: TStringList;

    constructor Create;
    destructor Destroy; override;

    procedure AddKey(aTableName: string; aKey: variant);
    procedure Check_Integrity;

   // procedure Show;

    procedure SetValue(aTableName: string; aSrcKey, aDestKey: variant);
    function GetValue(aTableName: string; aKey: variant): Variant;

    function Exists(aTableName: string; aKey: variant): Boolean;

    function SelectNotAppendedByTableName(aTableName: string): Integer;
    procedure Show;

    function Summary: TStringList;

    function SummaryForTable_Arr(aTableName : string): TStringArr;
    function SummaryForTable_str(aTableName : string): String;

    property ItemsEx[Index: Integer]: TDataRec read GetValueEx;

  end;

  // ---------------------------------------------------------------
  TFolder = class(TObject)
  // ---------------------------------------------------------------
  public
    ID        : Integer;
    Parent_ID : Integer;
    Name      : string;
    ObjName   : string;
//    GUID      : string;
    Project_ID: Integer;

    DB_ID     : Integer;

    SubItems: TFolderList;

    constructor Create;
    destructor Destroy; override;
  end;

  // ---------------------------------------------------------------
  TFolderList = //class(TList)
               class(TList<TFolder>)  
  // ---------------------------------------------------------------
  private
    function AddItem: TFolder;
    function FindByID(aID: Integer): TFolder;
//    function GetItems(Index: Integer): TFolder;
  public
    Selection: TList;

    constructor Create;
    destructor Destroy; override;

    function ChildrenExist(aParentID : integer): Boolean;

    procedure LoadFromDataset(aDataset: TDataset);

    procedure PrepareSubItems;

    procedure Select(aObjName: string);

//    property Items[Index: Integer]: TFolder read GetItems; default;
  end;




implementation



constructor TResultStringList.Create;
begin
  inherited Create;

 // Sorted := True;
  Duplicates := dupIgnore;

  Selection := TStringList.Create();
end;

destructor TResultStringList.Destroy;
begin
  FreeAndNil(Selection);
  inherited Destroy;
end;


function TResultStringList.Exists(aTableName: string; aKey: variant): Boolean;
var
  s: string;
begin
  s:=Values[Format('%s:%s',[aTableName, VarToStr(aKey)])];
  Result :=s<>'';
end;


function TResultStringList.GetValue(aTableName: string; aKey: variant): Variant;
var
  s: string;
begin
  s:=Values[Format('%s:%s',[aTableName, VarToStr(aKey)])];

  if s<>'' then
    Result := s
  else
    Result := null;
end;

// ---------------------------------------------------------------
procedure TResultStringList.SetValue(aTableName: string; aSrcKey, aDestKey:
    variant);
// ---------------------------------------------------------------
var
  sSrc: string;
  sDest,S: string;
begin
  s :=VarToStr(aSrcKey);
  sDest :=VarToStr(aDestKey);

  if sDest='' then
    sDest :='-';

//    raise Exception.Create('');

  Values[Format('%s:%s',[aTableName, s])] := sDest;
end;

// ---------------------------------------------------------------
procedure TResultStringList.AddKey(aTableName: string; aKey: variant);
// ---------------------------------------------------------------
var
  sSrc: string;
  sDest,S: string;
begin
  s :=VarToStr(aKey);
 // sDest :=VarToStr(aDestKey);

//  if sDest='' then
 //   sDest :='-';

//    raise Exception.Create('');

  Values[Format('%s:%s',[aTableName, s])] := '-';

end;


function TResultStringList.GetValueEx(Index: integer): TDataRec;
var
  iPos: Integer;
  s: string;
  s2: string;
  sTable: string;
begin
  s:=Names[Index];

  iPos:=Pos(':',s);

  sTable:= LowerCase(Copy(s, 1, iPos-1));
  s2:=Copy(s, iPos+1, 1000);
 // s2:=''''+ s2 +'''';


  Result.TableName := sTable;
  Result.Key := s2;

end;


// ---------------------------------------------------------------
procedure TResultStringList.Check_Integrity;
// ---------------------------------------------------------------
var
  I: Integer;
 // rec: TDataRec;
  s: string;

  oList: TStringList;

begin
  oList := TStringList.Create;

  for I := 0 to Count - 1 do
  begin
    s:=ValueFromIndex[i];
    if s='-' then
      oList.add(names[i] + '= -' );

//      raise Exception.Create(names[i]);

  end;

  {
  if oList.Count>0 then
    ShowMessage(oList.Text);
  }

  oList.Free;
end;



// ---------------------------------------------------------------
function TResultStringList.Summary: TStringList;
// ---------------------------------------------------------------
var
  I: Integer;
//  iPos: Integer;
//  sTable: string;
  s2: string;
//  sSrc: string;
//  sDest,S: string;

  rec: TDataRec;
begin
  Result := TStringList.Create;

  for I := 0 to Count - 1 do
  begin
    rec:=ItemsEx[i];

{
    s:=Names[i];

    iPos:=Pos(':',s);

    sTable:= LowerCase(Copy(s, 1, iPos-1));
    s2:=Copy(s, iPos+1, 1000);
    s2:=''''+ s2 +'''';
}

    s2:=VarToStr(rec.Key);


    if Result.Values[rec.TableName]='' then
      Result.Values[rec.TableName]:=s2
    else
      Result.Values[rec.TableName]:=Result.Values[rec.TableName] + ','+ s2;
  end;


  Result.Sort;
  
//  s :=VarToStr(aKey);
 // sDest :=VarToStr(aDestKey);

//  if sDest='' then
 //   sDest :='-';

//    raise Exception.Create('');

 // Values[Format('%s:%s',[aTableName, s])] := '-';

end;


// ---------------------------------------------------------------
function TResultStringList.SummaryForTable_str(aTableName : string): String;
// ---------------------------------------------------------------
var
  I: Integer;
  iPos: Integer;
  k: Integer;
  sTable: string;
  s2: string;
  sSrc: string;
  sDest,S: string;

  rec: TDataRec;
  oList : TStringList;
  
begin
  oList := TStringList.Create;
  oList.Delimiter:=',';

  for I := 0 to Count - 1 do
  begin
    rec:=ItemsEx[i];

    if Eq(aTableName, rec.TableName) then
      oList.Add( VarToStr(rec.Key));


//    s:=Names[i];
//
//    iPos:=Pos(':',s);
//
//    sTable:= LowerCase(Copy(s, 1, iPos-1));
//    s2:=Copy(s, iPos+1, 1000);
//    s2:=''''+ s2 +'''';



//    if Result.Values[sTable]='' then
//      Result.Values[sTable]:=s2
//    else
//      Result.Values[sTable]:=Result.Values[sTable] + ','+ s2;

  end;


  oList.Sorted:=true;
  k:=oList.Count; 

  result:=oList.DelimitedText;


  oList.Free;


//  s :=VarToStr(aKey);
 // sDest :=VarToStr(aDestKey);

//  if sDest='' then
 //   sDest :='-';

//    raise Exception.Create('');

 // Values[Format('%s:%s',[aTableName, s])] := '-';

end;




// ---------------------------------------------------------------
function TResultStringList.SummaryForTable_Arr(aTableName : string): TStringArr;
// ---------------------------------------------------------------
const
  DEF_MAX_ITEMS = 20;



var
  I: Integer;
  iArrCount: Integer;
  iPos: Integer;
  j: Integer;
  k: Integer;
  sTable: string;
  s2: string;
  sSrc: string;
  sDest,S: string;

  rec: TDataRec;
  oList : TStringList;

  oList1 : TStringList;

begin
  oList := TStringList.Create;
  oList.Delimiter:=',';

  oList1 := TStringList.Create;
  oList1.Delimiter:=',';


  for I := 0 to Count - 1 do
  begin
    rec:=ItemsEx[i];

    if Eq(aTableName, rec.TableName) then
      oList.Add( VarToStr(rec.Key));


//    s:=Names[i];
//
//    iPos:=Pos(':',s);
//
//    sTable:= LowerCase(Copy(s, 1, iPos-1));
//    s2:=Copy(s, iPos+1, 1000);
//    s2:=''''+ s2 +'''';



//    if Result.Values[sTable]='' then
//      Result.Values[sTable]:=s2
//    else
//      Result.Values[sTable]:=Result.Values[sTable] + ','+ s2;

  end;


//  oList.Sorted:=true;

  iArrCount:=oList.Count div DEF_MAX_ITEMS;

  //------------------------------------------------------
  if oList.Count=0 then
    SetLength(Result, 0)

  else
  //------------------------------------------------------
  begin
    SetLength(Result, iArrCount+1);

    for I := 0 to High(Result) do
    begin
      oList1.Clear;

      for j := oList.Count-1 downto 0 do
      begin
        oList1.Add(oList[j]);
        oList.Delete(j);

        if oList1.COunt>DEF_MAX_ITEMS then
          Break;
      end;


      Result[i]:=oList1.DelimitedText;
    end;
  end;





  oList.Free;
  oList1.Free;


//  s :=VarToStr(aKey);
 // sDest :=VarToStr(aDestKey);

//  if sDest='' then
 //   sDest :='-';

//    raise Exception.Create('');

 // Values[Format('%s:%s',[aTableName, s])] := '-';

end;





// ---------------------------------------------------------------
function TResultStringList.SelectNotAppendedByTableName(aTableName: string):
    Integer;
// ---------------------------------------------------------------
var
  I: Integer;
  iPos: Integer;
  s1: string;
  s2: string;

  rec: TDataRec;

begin
  Selection.Clear;

  for I := 0 to Count - 1 do
  begin
    rec:=ItemsEx[i];


    iPos := Pos(':',Names[i]);
    s1:=Copy(Names[i], 1, iPos-1);
    s2:=Copy(Names[i], iPos+1, 100);

    if Eq(s1,aTableName) then
      if ValueFromIndex[i]='-' then
        Selection.Add(s2);
  end;

  Result := Selection.Count;
end;

procedure TResultStringList.Show;
begin
  ShellExec_Notepad_temp(Text);

end;

 

constructor TFolderList.Create;
begin
  inherited Create;
  Selection := TList.Create();
end;

destructor TFolderList.Destroy;
begin
  FreeAndNil(Selection);
  inherited Destroy;
end;

function TFolderList.AddItem: TFolder;
begin
  Result := TFolder.Create;
  Add(Result);
end;

//function TFolderList.GetItems(Index: Integer): TFolder;
//begin
//  Result := TFolder(inherited Items[Index]);
//end;

procedure TFolderList.PrepareSubItems;
var
  I: Integer;
  oFolder: TFolder;
begin
  exit;

  for I := 0 to Count - 1 do
    if Items[i].Parent_ID>0 then
  begin
    oFolder :=FindByID(Items[i].Parent_ID);
    if Assigned(oFolder) then
      oFolder.SubItems.Add(Items[i]);
  end;
end;

// ---------------------------------------------------------------
function TFolderList.FindByID(aID: Integer): TFolder;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Result := nil;

  for I := 0 to Count - 1 do
      if Items[i].ID= aID then
    begin
      Result := Items[i];
      Break;
    end;
end;

// ---------------------------------------------------------------
procedure TFolderList.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
var
  oFolder: TFolder;
begin
  Clear;

//  db_View(aDataset);


  with aDataset do
    while not EOF do
    begin
      oFolder := AddItem;

      oFolder.ID        :=FieldByName('id').AsInteger;
      oFolder.Parent_ID :=FieldByName('Parent_ID').AsInteger;
      oFolder.Name      :=FieldByName('Name').AsString;
      oFolder.ObjName   :=FieldByName('type').AsString;

      oFolder.Project_ID:=FieldByName('Project_ID').AsInteger;


//      if Assigned(FindField('GUID')) then
 //       oFolder.GUID    :=FieldByName('GUID').AsString;

      Next;
    end;

  PrepareSubItems;
end;


// ---------------------------------------------------------------
procedure TFolderList.Select(aObjName: string);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Selection.Clear;

  for I := 0 to Count - 1 do
    if Eq(Items[i].ObjName, aObjName) then
//    if Eq(Items[i].ObjName, aObjName) and (Items[i].Parent_ID=0) then
  begin
      Selection.Add(Items[i]);
  end;
end;

// ---------------------------------------------------------------
function TFolderList.ChildrenExist(aParentID : integer): Boolean;
// ---------------------------------------------------------------
var
  I: Integer;
begin
//  Selection.Clear;
  Result:=False;

  //Eq(Items[i].ObjName, aObjName) and

  for I := 0 to Count - 1 do
    if (Items[i].Parent_ID=aParentID) then
    begin
      Result:=True;
      exit;
    end;

end;



constructor TFolder.Create;
begin
  inherited Create;
  SubItems := TFolderList.Create();
end;

destructor TFolder.Destroy;
begin
  FreeAndNil(SubItems);
  inherited Destroy;
end;

//constructor TResultItemList.Create;
//begin
//  inherited Create(TResultItem);
//end;

function TResultItemList.AddItem: TResultItem;
begin
  Result := TResultItem.create;
  Add(Result); // (Inherited Add);
end;

//function TResultItemList.GetItems(Index: Integer): TResultItem;
//begin
//  Result := TResultItem (Inherited Items[Index]);
//end;


end.



{


// ---------------------------------------------------------------
function TResultStringList.Summary: TStringList;
// ---------------------------------------------------------------
var
  I: Integer;
  iPos: Integer;
  sTable: string;
  s2: string;
  sSrc: string;
  sDest,S: string;
begin
  Result := TStringList.Create;

  for I := 0 to Count - 1 do
  begin
    s:=Names[i];

    iPos:=Pos(':',s);

    sTable:= LowerCase(Copy(s, 1, iPos-1));
    s2:=Copy(s, iPos+1, 1000);
    s2:=''''+ s2 +'''';

    if Result.Values[sTable]='' then
      Result.Values[sTable]:=s2
    else
      Result.Values[sTable]:=Result.Values[sTable] + ','+ s2;
  end;}
