unit dm_SQL_to_MDB_new;

interface

uses
  SysUtils, Classes, Forms, Variants, ADODB, Db,  Dialogs,

  u_lists,

  u_ini_data_export_import_params,

  dm_Config,

  dm_MDB,

  d_Progress,

  dm_Main_data,

  u_FK,

  u_log,
  u_func,
      
  u_db,

  u_const_db,

  u_export_classes;


type
  TStrNotifyEvent = procedure (aValue: string) of object;

  TLogMsgType = (ltError,ltSQL);


  TdmSQL_to_MDB_new = class(TDataModule)
    qry_Source: TADOQuery;
    tb_Dest: TADOTable;

    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    function GetTableCreateSQL(aDataset: TDataSet; aTableName, aKeyFieldName:
        String): string;

    procedure Log(aMsg: string);

  private
    FDBStructure: TDBStructure;

    FLogFileName: string;

    FTable_ID_DB_ID_List: TResultStringList;
 //   oLocal_Table_ID_DB_ID_List: TResultStringList;


    FTableNameList: TStringList;

    FRootTableNameList: TStringList;


    procedure Create_table_from_dataset(aDataset: TDataSet; aTableName: String;
        aKeyFieldName : string);

    procedure CopyDatasets_new(aTableName: string; aSrcDataset, aDestDataset:
        TDataset);

//    procedure CopyDatasets_new_text(aTableName: string; aSrcDataset: TDataset);

    procedure CopyTable_new(aTableName, aIDS: string);

//    function TableNameRecordsExist(aTableName : string): Boolean;

    procedure CopyTable_selected_new(aTableName, aIDS: string; aUseProjectID:
        Boolean = False);

//    function DoAllow(aPK_Table, aFK_Table: string): boolean;

    procedure ProcessKeyFields(aTableName, aWhereKey, aKeyValue: string;
        aUseProjectID: Boolean = false);
    procedure Init_list(aTableName: string);


  private

  public


    Params: record
              IsTestMode : boolean; 

              Project_ID: integer;
              MDBFileName : string;

              TableName : string;

              IDS: string;   ////!!!!
            end;


    procedure ExecuteDlg;

    procedure ExecuteMain;

//    property OnLog: TStrNotifyEvent read FOnLog write FOnLog;

    class procedure Init;

  end;

var
  dmSQL_to_MDB_new: TdmSQL_to_MDB_new;


implementation
{$R *.DFM}



class procedure TdmSQL_to_MDB_new.Init;
begin
  if not Assigned(dmSQL_to_MDB_new) then
    dmSQL_TO_MDB_new := TdmSQL_TO_MDB_new.Create(Application);
end;


//------------------------------------------------------------------------------
procedure TdmSQL_to_MDB_new.DataModuleCreate(Sender: TObject);
//------------------------------------------------------------------------------

var
  k: Integer;
begin
  inherited;

  FRootTableNameList:=TStringList.Create();

  FDBStructure:=TDBStructure.Create;


  db_SetComponentsADOConn([qry_Source], dmMain_data.ADOConnection_SQL);

  db_SetComponentsADOConn([tb_Dest], dmMDB.ADOConnection);

//  FSrcDataSet := qry_Source;
//  FDestDataSet:= tb_Dest;

//  FUsedFolderList := TStringList.Create();
  FTableNameList := TStringList.Create();

  FTable_ID_DB_ID_List := TResultStringList.Create();

//  oLocal_Table_ID_DB_ID_List:=TResultStringList.Create;


  FLogFileName := ChangeFileExt(Application.ExeName, '.log');


end;


// ---------------------------------------------------------------
procedure TdmSQL_to_MDB_new.DataModuleDestroy(Sender: TObject);
// ---------------------------------------------------------------
begin
  FreeAndNil(FRootTableNameList);

  FreeAndNil(FDBStructure);        

  FreeAndNil(FTableNameList);
  FreeAndNil(FTable_ID_DB_ID_List);


end;


// -------------------------------------------------------------------
procedure TdmSQL_to_MDB_new.CopyTable_new(aTableName, aIDS: string);
// -------------------------------------------------------------------
var
  I: Integer;
  s: string;
  sKey: string;
  sTableName: string;
begin
   if Eq(aTableName, TBL_LINK) then
     s:='';;
     

//  sKey:= FDBStructure.PrimaryKeyList.Values[aTableName];
  sKey:= FDBStructure.GetPrimaryKeyForTable(aTableName);

  s := Format('SELECT * FROM %s WHERE (%s in (%s)) ', [ aTableName, sKey, aIDS]);


  Log ('CopyTable_new - ' + s);

//  if aTableInfo.UseProjectID then
//  begin
//    Assert(Params.Project_ID>0, 'Value <=0');
//
//    s :=s+ Format(' WHERE (project_id=%d)', [Params.Project_ID]);
//  end;

  db_OpenQuery(qry_Source, s);

  if qry_Source.RecordCount=0 then
    Exit;

//  if FTableNameList.IndexOf(aTableInfo.TableName)<0 then
  Create_table_from_dataset(qry_Source, aTableName, sKey);

  db_TableOpen1(tb_Dest, aTableName);

//  CopyDatasets_new_text (aTableName, qry_Source);


  CopyDatasets_new (aTableName, qry_Source, tb_Dest);



  qry_Source.Close;
  tb_Dest.Close;

 // Log(FTable_ID_DB_ID_List.Text);

 // FTable_ID_DB_ID_List.SaveToFile(FLogFileName);

  // ---------------------------------------------------------------

end;




// ---------------------------------------------------------------
procedure TdmSQL_to_MDB_new.ProcessKeyFields(aTableName, aWhereKey, aKeyValue:
    string; aUseProjectID: Boolean = false);
// ---------------------------------------------------------------

var
  I: Integer;
  oQuery: TADOQuery;
  sFK_ColumnName: string;
  sFK_TableName: string;
  sPK_ColumnName: string;
  sPK_TableName: string;
  sPrimaryKey: string;
  sSQL: string;
  v: Variant;

  oSelection_PK: TFKItemRefList;
  oSelection_FK: TFKItemRefList;
  s: string;
//  sID: string;

  arrStr: TStringArr;
  b: Boolean;
  bIsFolder: Boolean;
 // bIsLinkend: Boolean;
  j: Integer;
  k: Integer;


  oSList: TStringList;

label
  label_Exit;

begin
  Assert(aWhereKey<>'');




//  sPrimaryKey:=FDBStructure.PrimaryKeyList.Values[aTableName];
  sPrimaryKey:=FDBStructure.GetPrimaryKeyForTable (aTableName);

  oSelection_FK:=FDBStructure.FKItemList.SelectByTableName_FK(aTableName);
  oSelection_PK:=FDBStructure.FKItemList.SelectByTableName_PK(aTableName);

  k:=oSelection_FK.Count;
  k:=oSelection_PK.Count;


  sSQL := FDBStructure.FKItemList.MakeSQL(aTableName, sPrimaryKey, aWhereKey, aKeyValue);

  if aUseProjectID then
  begin
    if aKeyValue='' then
      sSQL:=sSQL +  Format(' WHERE project_id=%d', [Params.Project_ID])
    else
      sSQL:=sSQL +  Format(' AND (project_id=%d)', [Params.Project_ID]);

  end;



  Log(sSQL);


  // -------------------------
  oQuery:=TADOQuery.Create(nil);
  oQuery.LockType:=ltReadOnly;
  oQuery.Connection:=dmMain_Data.ADOConnection_SQL;


  db_OpenQuery(oQuery, sSQL);

  if oQuery.RecordCount=0 then
    goto label_Exit;


 //  db_View(oQuery);


  Assert( Assigned( oQuery.FindField(sPrimaryKey)) , sSQL + '---'+ sPrimaryKey);


  with oQuery do
     while not EOF do
    begin
      v:=oQuery[sPrimaryKey];


//      bIsLinkend:=  StringIsInArr (aTableName, [TBL_LINKEND]);
      bIsFolder:=  StringIsInArr (aTableName, [TBL_FOLDER]);

//      if not FIsStatus then
//     begin
//      if not bIsLinkend then
{

      if not bIsFolder then
        if FTable_ID_DB_ID_List.Exists (aTableName, v) then
        begin
          Next;
          Continue;
        end;
}


  //      if not FTable_ID_DB_ID_List.Exists (aTableName, v) then
  //     begin
        FTable_ID_DB_ID_List.AddKey (aTableName, v);

      Log('AddKey: '+ aTableName  + ':'+  VarToStr( v));
   //   end;


      for I := 0 to oSelection_FK.Count - 1 do
      begin
        sPK_TableName :=oSelection_FK[i].PK_TableName;
        sPK_ColumnName:=oSelection_FK[i].PK_ColumnName;

        sFK_ColumnName:=oSelection_FK[i].FK_ColumnName;

        v:=oQuery[sFK_ColumnName];

        if not VarIsNull(v) then
          if not FTable_ID_DB_ID_List.Exists(sPK_TableName, sFK_ColumnName) then
//             if not oLocal_Table_ID_DB_ID_List.Exists(sPK_TableName, sFK_ColumnName) then
          begin
        //    Log('AddKey: '+ sPK_TableName  + ':'+  VarToStr( v));

            //
            FTable_ID_DB_ID_List.AddKey (sPK_TableName, v);

          //  oLocal_Table_ID_DB_ID_List.AddKey (sPK_TableName, v);

//            s:=''''+ VarToStr(v) + '''';
            s:=VarToStr(v);


            // FOLDER parent id
             if Eq(aTableName, sPK_TableName) then
             begin
               ProcessKeyFields (aTableName,  FLD_ID, s);
             end;


          end;

      end;


      Next;
    end;




label_Exit:

  FreeAndNil(oQuery);

  FreeAndNil(oSelection_PK);
  FreeAndNil(oSelection_FK);



end;


// -------------------------------------------------------------------
procedure TdmSQL_to_MDB_new.CopyTable_selected_new(aTableName, aIDS: string;
    aUseProjectID: Boolean = False);
// -------------------------------------------------------------------
var
  sPrimaryKey: string;
begin
  sPrimaryKey:=FDBStructure.GetPrimaryKeyForTable(aTableName);
//  sPrimaryKey:=FDBStructure.PrimaryKeyList.Values[aTableName];

  ProcessKeyFields (aTableName,  sPrimaryKey, aIDS, aUseProjectID);

end;


// -------------------------------------------------------------------
function TdmSQL_to_MDB_new.GetTableCreateSQL(aDataset: TDataSet; aTableName,
    aKeyFieldName: String): string;
// -------------------------------------------------------------------
var
  I: integer;
  oSList: TStringList;
  s,s1: string;
  oField: TFieldDef;

begin
  oSList:=TStringList.Create;

  for I := 0 to aDataset.FieldDefs.Count-1 do
  begin
    oField :=aDataset.FieldDefs[I];


    case oField.DataType of

        ftWideString :   s:='varchar('+AsString(oField.Size)+')';
      else
//        raise Exception.Create('');
    //    Result:='';
      end;




    //!!!!!!!!!!!!!!!!!!!!!!!!!!!
{
    if Eq(oField.FieldName, 'status_id') or
       Eq(oField.FieldName, 'state_id') or
       Eq(oField.FieldName, 'placement_id')
    then
      Continue;
}

    s1:='['+oField.Name+']'+' '+
        db_TypeFieldToString11 (oField.DataType, oField.Size);

    if Eq(aKeyFieldName, oField.Name) then
      s1:=s1+ '  PRIMARY KEY';

    oSList.Add(s1);
  end;
         

(*  // ��� ������� ����������� ���� db_id
  if Assigned(aDataset.Fields.FindField('id')) and
     (not Assigned(aDataset.Fields.FindField('db_id')))
   then
    oSList.Add('[db_id] int');
*)


(*
  if aMasterTableName<>'' then
  begin
    sKey:=dmMDB.GetTableKeyField1(aMasterTableName);

    s1:= Format('CONSTRAINT [FK_%s_%s] FOREIGN KEY' +
                '  ( [%s] ) REFERENCES [%s] ( [%s] ) ON DELETE CASCADE ON UPDATE CASCADE ',

      [aTableName, aMasterTableName, aMasterFieldName, aMasterTableName, sKey]);

  end;

*)

//              '  CONSTRAINT [FK_site_property] FOREIGN KEY '+
 //             '  ( [property_id] ) REFERENCES [property] ( [id] ) ON DELETE CASCADE ON UPDATE CASCADE '+



  oSList.Delimiter :=',';
  oSList.QuoteChar:=' ';
  s1:=oSList.DelimitedText;

  s1:='CREATE TABLE '+ aTableName +' (' + oSList.DelimitedText + ')';

  Result :=s1;

(*
 // DBManager.ExecCommand
  ('CREATE TABLE '+ 'Site' +
              ' ( id           identity PRIMARY KEY, '+
              '   db_id        int, '+


              '  CONSTRAINT [FK_site_property] FOREIGN KEY '+
              '  ( [property_id] ) REFERENCES [property] ( [id] ) ON DELETE CASCADE ON UPDATE CASCADE '+
              ')' ) ;

*)

  FreeAndNil(oSList);
end;


// -------------------------------------------------------------------
procedure TdmSQL_to_MDB_new.Create_table_from_dataset(aDataset: TDataSet;
    aTableName: String; aKeyFieldName : string);
// -------------------------------------------------------------------
var
  S: string;
begin
  if FTableNameList.IndexOf(aTableName)>=0 then
    Exit;

  s:=GetTableCreateSQL (aDataset, aTableName, aKeyFieldName);

  g_Log.Add(s);

  dmMDB.ADOConnection.Execute(S);

  FTableNameList.Add(aTableName);
end;


procedure TdmSQL_to_MDB_new.Log(aMsg: string);
begin
  g_Log.Msg(aMsg);
end;


// -------------------------------------------------------------------
procedure TdmSQL_to_MDB_new.CopyDatasets_new(aTableName: string; aSrcDataset,
    aDestDataset: TDataset);
// -------------------------------------------------------------------
var
  bTerminated: Boolean;
begin
  Progress_SetProgressMsg2(aTableName);

  Assert(aSrcDataSet.RecordCount>0);


  // ------------------------------------
  // ���������� ���� �������
  // ------------------------------------
  aSrcDataSet.First;


  with aSrcDataSet do
    while not Eof do
    begin
      Progress_SetProgress2(aSrcDataset.RecNo, aSrcDataSet.RecordCount, bTerminated);
      if bTerminated then
        Exit;

      db_AddRecordFromDataSet(aSrcDataset, aDestDataSet);

      Next;
    end;
end;


// ---------------------------------------------------------------
procedure TdmSQL_to_MDB_new.Init_list(aTableName: string);
// ---------------------------------------------------------------
begin
    // ---------------------------------------------------------------
    if Eq(aTableName, TBL_ClutterModel) then
    // ---------------------------------------------------------------
    begin
      g_ItemList.AddChild (TBL_ClutterModel, TBL_ClutterModelItems, FLD_CLUTTER_MODEL_ID);
      g_ItemList.AddRoot (TBL_ClutterModelType);

    end else



    // ---------------------------------------------------------------
    if Eq(aTableName, TBL_ColorSchema) then
    // ---------------------------------------------------------------
    begin
      g_ItemList.AddChild (TBL_ColorSchema, TBL_ColorSchemaRanges, FLD_COLOR_SCHEMA_ID);

    end else


    // ---------------------------------------------------------------
    if Eq(aTableName, TBL_PMP_CalcRegion) then
    // ---------------------------------------------------------------
    begin
      g_ItemList.AddChild (TBL_PMP_CalcRegion, TBL_PMP_CalcRegionCells, FLD_PMP_CALC_REGION_ID);
      g_ItemList.AddChild (TBL_PMP_CalcRegion, TBL_PMP_CalcRegion_Site, FLD_PMP_CALC_REGION_ID);
      g_ItemList.AddChild (TBL_PMP_CalcRegion, TBL_PMP_CalcRegion_Polygons, FLD_PMP_CALC_REGION_ID);

    end else


    // ---------------------------------------------------------------
    if Eq(aTableName, TBL_LINKENDTYPE) then
    // ---------------------------------------------------------------
    begin
      g_ItemList.AddChild (TBL_LINKENDTYPE, TBL_LINKENDTYPE_MODE, FLD_LINKENDTYPE_ID);
      g_ItemList.AddChild (TBL_LINKENDTYPE, TBL_LINKENDTYPE_BAND, FLD_LINKENDTYPE_ID);
    end else


    // ---------------------------------------------------------------
    if Eq(aTableName, TBL_LinkFreqPlan) then
    // ---------------------------------------------------------------
    begin
      g_ItemList.AddChild (TBL_LinkFreqPlan, TBL_LinkFreqPlan_LINK, FLD_LinkFreqPlan_ID);
      g_ItemList.AddChild (TBL_LinkFreqPlan, TBL_LinkFreqPlan_LINKEND, FLD_LinkFreqPlan_ID);
      g_ItemList.AddChild (TBL_LinkFreqPlan, TBL_LinkFreqPlan_NEIGHBORS, FLD_LinkFreqPlan_ID);
      g_ItemList.AddChild (TBL_LinkFreqPlan, TBL_LinkFreqPlan_RESOURCE, FLD_LinkFreqPlan_ID);

    end else

    // ---------------------------------------------------------------
    if Eq(aTableName, TBL_LINKLINE) then
    // ---------------------------------------------------------------
    begin
      g_ItemList.AddChild (TBL_LINKLINE, TBL_LinkLineLinkXREF, FLD_LINKLINE_ID);

    end else


  // ---------------------------------------------------------------
    if Eq(aTableName, TBL_LINKNET) then
    // ---------------------------------------------------------------
    begin
      g_ItemList.AddChild (TBL_LINKNET, TBL_LINKNET_ITEMS_XREF, FLD_LINKNET_ID);

      g_ItemList.AddChild (TBL_LINKNET, TBL_LinkFreqPlan, FLD_LINKNET_ID);


      Init_list (TBL_LinkFreqPlan);

    end else


   // ---------------------------------------------------------------
    if Eq(aTableName, TBL_LINKEND) then
    // ---------------------------------------------------------------
    begin

    end else   




   // ---------------------------------------------------------------
    if Eq(aTableName, TBL_PROPERTY) then
    // ---------------------------------------------------------------
    begin
    //  CopyTable_selected_new(Params.TableName, Params.IDS, False);

      g_ItemList.AddChild (TBL_PROPERTY, TBL_MSC,         FLD_PROPERTY_ID);
//      g_ItemList.AddChild (TBL_PROPERTY, TBL_PmpTerminal, FLD_PROPERTY_ID);
      g_ItemList.AddChild (TBL_PROPERTY, TBL_PMP_SITE,    FLD_PROPERTY_ID);

//      g_ItemList.AddChild (TBL_PMP_SITE, TBL_PMP_SECTOR,  FLD_PMP_SITE_ID);

      g_ItemList.AddChild (TBL_PROPERTY,      TBL_LINK_Repeater, FLD_PROPERTY_ID);
      g_ItemList.AddChild (TBL_LINK_Repeater, TBL_Link_repeater_Antenna, FLD_LINK_REPEATER_ID);


      g_ItemList.AddChild (TBL_PROPERTY, TBL_LINKEND,         FLD_PROPERTY_ID);
      g_ItemList.AddChild (TBL_LINKEND,  TBL_LINKEND_ANTENNA, FLD_LINKEND_ID);


      g_ItemList.Addroot (TBL_AntennaType);
      g_ItemList.Addroot (TBL_LINKENDType);
      g_ItemList.Addroot (TBL_GEOREGION);


      g_ItemList.AddChild (TBL_LINKENDTYPE, TBL_LINKENDTYPE_MODE, FLD_LINKENDTYPE_ID);
      g_ItemList.AddChild (TBL_LINKENDTYPE, TBL_LINKENDTYPE_BAND, FLD_LINKENDTYPE_ID);
           
//      ProcessKeyFields(aTableName, aWhereKey, aKeyValue: string; aUseProjectID: Boolean = false);

    end else   

  // ---------------------------------------------------------------
  if Eq(aTableName, TBL_LINK) then
  // ---------------------------------------------------------------
  begin
//      sID:=FTable_ID_DB_ID_List.SummaryForTable2(TBL_LINKEND);
                 
//    CopyTable_selected_new(Params.TableName, Params.IDS, False);


//    g_ItemList.AddChild(TBL_Link, TBL_LinkCalcResults, FLD_LINK_ID);
//    g_ItemList.AddChild(TBL_Link, TBL_LINKEND, FLD_LINKEND1_ID);
//    g_ItemList.AddChild(TBL_Link, TBL_LINKEND, FLD_LINKEND2_ID);

 //   g_ItemList.AddChild (TBL_PMP_SITE, TBL_PMP_SECTOR,  FLD_PMP_SITE_ID);


    g_ItemList.Addroot (TBL_LINKEND);

    g_ItemList.AddChild(TBL_LINKEND, TBL_LINKEND_ANTENNA, FLD_LINKEND_ID);


    g_ItemList.Addroot (TBL_LinkType);
//     g_ItemList.Addroot (TBL_STATUS);


    g_ItemList.Addroot (TBL_PROPERTY);

    Init_list (TBL_PROPERTY);

  end;


  g_ItemList.Addroot (TBL_FOLDER);

end;


// ---------------------------------------------------------------
procedure TdmSQL_to_MDB_new.ExecuteDlg;
// ---------------------------------------------------------------
var
  sFileName: string;
begin
  sFileName  := Params.MDBFileName;
(*
  if FileExists(sFileName) then
    if ConfirmDlg('���� ����������, ������������?') then
    begin
      if not DeleteFile (sFileName) then
      begin
        ShowMessage('���� �� ����� ���� �����������, ��� �������');
        Exit;
      end;
    end else
      Exit;

*)
  if FileExists(sFileName) then
    if not DeleteFile (sFileName) then
    begin
      ShowMessage('���� �� ����� ���� �����������, ��� �������');
      Exit;
    end;


  Progress_ExecDlg_proc(ExecuteMain);
end;  



// ---------------------------------------------------------------
procedure TdmSQL_to_MDB_new.ExecuteMain;
// ---------------------------------------------------------------
const
  bTerminated: Boolean=False;

var
  b: Boolean;
  i: integer;
  k: Integer;
  s: string;

  sSQL: string;
  sTableName: string;

  oSList: TStringList;
  sID: string;

  arrStr: TStringArr;
  j: Integer;
  sPrimaryKey: string;

  oRefItem: TRefItem;

//  oSList: TStringList;

begin
  bTerminated :=False;

  FTableNameList.Clear;

  FTable_ID_DB_ID_List.clear;


  FDBStructure.LoadFromADOConn(dmMain_Data.ADOConnection_SQL);


  if not dmMDB.CreateMDB(params.MDBFileName) then
    Exit;



// FRootTableNameList
  if g_Ini_Data_export_import_params.Mode in [mtExport_PROJECT, mtExport_GUIDES] then
    //---------------------------------------------------
  //  if Records.Count=0 then
    //---------------------------------------------------
    begin
      if Params.IsTestMode then
        dmConfig.GetCheckedTableList_all_test(FRootTableNameList)
      else
        dmConfig.GetCheckedTableList(FRootTableNameList);

      //get empty folders
      FRootTableNameList.Add(TBL_FOLDER);

      for I := 0 to FRootTableNameList.Count - 1 do
      begin
        s:=FRootTableNameList[i];

//        SetAllowedForTable (s);

        b:=g_Ini_Data_export_import_params.Mode = mtExport_PROJECT;

  //      if g_Ini_Data_export_import_params.Mode = mtExport_PROJECT then


        k:=Params.Project_ID;


        Init_list (FRootTableNameList[i]);

        CopyTable_selected_new(FRootTableNameList[i], '', b);

{

oSList:=FTable_ID_DB_ID_List.Summary();
ShellExec_Notepad_temp( oSList.Text );
}


      end;

  end ;


  //---------------------------------------------------
  if g_Ini_Data_export_import_params.Mode in [mtExport_selected] then
  //  if Records.Count>0 then
  //---------------------------------------------------
  begin
  //  Records.Delimiter:=',';
  //  s:=Records.DelimitedText;

   // s:=Params.IDS;

//    SetAllowedForTable (Params.TableName);


    Init_list (Params.TableName);

    CopyTable_selected_new(Params.TableName, Params.IDS, False);

//    FTable_ID_DB_ID_List.



  //  Exit;
  end;

  {
   oSList:=FTable_ID_DB_ID_List.Summary();
   ShellExec_Notepad_temp1( oSList.Text );
   }



  // -----------------------------
  //
  // -----------------------------
  for i:=0 to g_ItemList.Count-1 do
  begin
    oRefItem:=g_ItemList[i];


    if  Eq( oRefItem.Parent_TableName, TBL_PROPERTY) then
    begin
      if g_Ini_Data_export_import_params.Mode in [mtExport_selected] then
        Continue;
    end;



    if  Eq( oRefItem.Slave_TableName, TBL_LINKEND) then
      k:=0;

    s:= FTable_ID_DB_ID_List.SummaryForTable_str(oRefItem.Parent_TableName);

    if s='' then
      Continue;

    if oRefItem.IsChild then
      ProcessKeyFields (oRefItem.Slave_TableName, oRefItem.ParentFieldName, s)
    else
      CopyTable_selected_new(oRefItem.Parent_TableName, s, False);

  end;



  oSList:=FTable_ID_DB_ID_List.Summary();
  
 // ShellExec_Notepad_temp1( oSList.Text );


///////////
 ///// ShellExec_Notepad_temp( oSList.Text );
///////////

  for I := 0 to oSList.Count - 1 do
    CopyTable_new(oSList.Names[i], oSList.ValueFromIndex[i] );


  FreeAndNil(oSList);

 // AddFolders;

  dmMDB.ADOConnection.Close;


 ///// ShellExec(params.MDBFileName);


end;

end.



