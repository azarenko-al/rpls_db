unit d_Select;

interface

uses
  Classes, Controls, Forms,
  cxGridLevel, 
  cxGridDBTableView, cxGrid, ADODB,
  DB, dxmdaset, DBGrids, rxPlacemnt, Grids, cxGridCustomTableView,
  cxGridTableView, cxClasses, cxControls, cxGridCustomView, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, cxCheckBox
  ;

type
  Tdlg_Select = class(TForm)
    dxMemData1: TdxMemData;
    DataSource1: TDataSource;
    ADOQuery1: TADOQuery;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    ADOConnection1: TADOConnection;
    DBGrid1: TDBGrid;
    DataSource2: TDataSource;
    cxGrid1DBTableView1RecId: TcxGridDBColumn;
    cxGrid1DBTableView1checked: TcxGridDBColumn;
    cxGrid1DBTableView1folder_name: TcxGridDBColumn;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    FormPlacement1: TFormPlacement;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    dxMemData1folder_name: TStringField;
    dxMemData1id: TAutoIncField;
    dxMemData1folder_id: TIntegerField;
    dxMemData1name: TWideStringField;
    dxMemData1gain: TFloatField;
    dxMemData1gain_type: TIntegerField;
    dxMemData1tilt_type: TIntegerField;
    dxMemData1freq: TFloatField;
    dxMemData1diameter: TFloatField;
    dxMemData1omni: TBooleanField;
    dxMemData1vert_width: TFloatField;
    dxMemData1horz_width: TFloatField;
    dxMemData1fronttobackratio: TFloatField;
    dxMemData1polarization_ratio: TFloatField;
    dxMemData1polarization_str: TWideStringField;
    dxMemData1weight: TFloatField;
    dxMemData1band: TStringField;
    dxMemData1vert_mask: TMemoField;
    dxMemData1horz_mask: TMemoField;
    dxMemData1electrical_tilt: TFloatField;
    dxMemData1_____: TBooleanField;
    dxMemData1comment: TWideStringField;
    dxMemData1file_content: TMemoField;
    dxMemData1______: TBooleanField;
    dxMemData1guid: TGuidField;
    dxMemData1date_created: TDateTimeField;
    dxMemData1date_modify: TDateTimeField;
    dxMemData1user_created: TWideStringField;
    dxMemData1user_modify: TWideStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlg_Select: Tdlg_Select;

implementation

{$R *.dfm}

end.
