unit dm_MDB_to_SQL;

 {$DEFINE test11111}
 {$DEFINE test}        

interface
uses
  SysUtils, Classes, Forms, Dialogs, Variants, ADODB, Db,

  dm_MDB,

  u_Log,

  dm_Config,

  u_Config_classes,

  dm_MDB_validate,

  d_Progress,
  dm_Progress,

//  dmMain

  u_FK,

  u_db,
  u_func,

  u_export_classes;


type
  TStrNotifyEvent = procedure (aValue: string) of object;

  TLogMsgType = (ltError,ltSQL);


  TdmMDB_to_SQL = class(TdmProgress)
    qry_DestTable: TADOQuery;
    qry_SrcTable: TADOQuery;
    ADOQuery1: TADOQuery;
    qry_Dest: TADOQuery;
    qry_check_db_id: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FConfig_ref: TConfig;
    FFolderList: TFolderList;
    FTable_ID_DB_ID_List: TResultStringList;

    FSrcDataSet: TDataSet;
    FDestDataSet: TDataSet;


    FOnLog: TStrNotifyEvent;
    FOnLogError: TStrNotifyEvent;
    FDest_TableNameList: TStringList;

    FTest_UseDB_ID: Boolean;

    FLogFileName : string;

//    function AddFolder(aTableInfo: TTableInfo): Integer;

    procedure AddFolder_new;
    function CheckLocated1(aSrcDataSet: TDataSet; aTableInfo: TTableInfo; var
        aKeyValue: variant; var aIsLocated: Boolean): boolean;

//    function CopyRecord1(aTableInfo: TTableInfo; var aKeyValue: variant): Boolean;

    function CopyRecord_new(aTableInfo: TTableInfo; var aKeyValue: variant):
        Boolean;

    procedure CopyTable(aTableInfo: TTableInfo);

    function DestTableExists(aTableName: string): Boolean;

    function GetPropertyByLinkEnd(aID: Integer): Integer;
//    function GetPropertyID_ByPmpSector(aID: Integer): Integer;
//    function GetPropertyID_ByPmpTerminal(aID: Integer): Integer;

//    function GetPropertyID_ByLinkRepeater(aID: Integer): Integer;


    procedure ImportTable(aTableInfo: TTableInfo);

    procedure Log(aMsg: string);
    procedure LogError(aMsg: string);
    procedure LogDebug(aMsg: string);

//    procedure OpenDestTableName(aTableName: string);
    function OpenDestTable(aTableInfo: TTableInfo): boolean;
    function OpenSrcAndRefTable(aTableInfo: TTableInfo): boolean;

    function SrcTableExists(aTableName: string): Boolean;

    procedure LoadFolders;

  private
    FDBStructure_dest: TDBStructure;
           
  public
    Params: record
//      IsRefreshRecords: boolean;
 //     IsRefreshExistentObjects: boolean;
  //    IsConvertCalcResults: boolean;

    //  Src_ADOConnection: TADOConnection;
      Dest_ADOConnection: TADOConnection;

//      Src_ProjectID : Integer;
      Dest_ProjectID : Integer;

      MDB_FileName : string;

//      CoordSystem : integer;
    end;

    procedure ExecuteProc; override;
    procedure ExecuteDlg1;

    property OnLog: TStrNotifyEvent read FOnLog write FOnLog;

    class procedure Init;
  end;


var
  dmMDB_to_SQL: TdmMDB_to_SQL;


implementation
{$R *.DFM}

const
  FLD_Property1_id = 'Property1_id';
  FLD_Property2_id = 'Property2_id';

  FLD_LinkEnd2_id  = 'LinkEnd2_id';
  FLD_LinkEnd1_id  = 'LinkEnd1_id';
  FLD_PMP_SITE_ID  = 'PMP_SITE_ID';


  TBL_LINKEND      = 'LINKEND';
//  TBL_PMP_SECTOR   = 'PMP_SECTOR';
  TBL_Property     = 'Property';
//  TBL_PMPTERMINAL  = 'PMPTERMINAL';
  TBL_PMP_SITE     = 'PMP_SITE';

//  TBL_Link_Repeater = 'Link_Repeater';

  FLD_FOLDER_ID     = 'FOLDER_ID';
  TBL_FOLDER        = 'FOLDER';




  FLD_property_id   = 'property_id';


// ---------------------------------------------------------------
class procedure TdmMDB_to_SQL.Init;
// ---------------------------------------------------------------
begin
  if not Assigned(dmMDB_to_SQL) then
    dmMDB_to_SQL := TdmMDB_to_SQL.Create(Application);
end;


//------------------------------------------------------------------------------
procedure TdmMDB_to_SQL.DataModuleCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;

  FDBStructure_dest:=TDBStructure.Create();



  FDest_TableNameList := TStringList.Create();

//  FConfig

  FTable_ID_DB_ID_List := TResultStringList.Create();
  FFolderList := TFolderList.Create();

  FSrcDataSet  := qry_SrcTable;
  FDestDataSet := qry_DestTable;

  FTest_UseDB_ID := True;

  FLogFileName := ChangeFileExt(Application.ExeName, '.log');


  db_SetComponentsADOConn([qry_SrcTable,  qry_check_db_id, AdOQuery1], dmMDB.ADOConnection);

end;




//-----------------------------------------------------------------------------
procedure TdmMDB_to_SQL.AddFolder_new;
//-----------------------------------------------------------------------------

    // ---------------------------------------------------------------
    procedure DoAddFolder_(aParent_DB_ID: integer; aParent_ID : Integer);
    // ---------------------------------------------------------------
    var
      b: Boolean;
      i: Integer;
      iDB_ID: Integer;
      iID: Integer;
      oFolder: TFolder;
      sSQL: string;
      sSQL_Insert: string;

      oParamList: TdbParamList;
      s: string;
//      sSQL_Insert_test: string;
      sWHERE: string;

{      s_Insert: string;
      s_Select_all: string;
      s_Select_id: string;
}    begin
     // if aList.Count=0 then
      //  exit;


      oParamList:=TdbParamList.Create;


      for I := 0 to FFolderList.Count - 1 do
      begin
        oFolder :=TFolder(FFolderList[i]);

        if oFolder.Parent_ID<>aParent_ID then
          Continue;



        sWHERE:='([type]=:type) and (name=:name)';
        if oFolder.Project_ID>0 then
          sWHERE:=sWHERE+ ' and (PROJECT_ID=:PROJECT_ID)'
        else
          sWHERE:=sWHERE+ ' and (PROJECT_ID IS NULL)';


        if aParent_DB_ID>0 then
          sWHERE:=sWHERE+ ' and (PARENT_ID=:PARENT_ID)'
        else
          sWHERE:=sWHERE+ ' and (PARENT_ID IS NULL)';

{
        sSQL:=
          Format('IF EXISTS(SELECT * FROM Folder WHERE %s )', [sWhere]) +
          Format('  SELECT id FROM Folder WHERE %s ', [sWhere]) +
          ' ELSE begin '+
          '   INSERT INTO Folder (name,[type],PARENT_ID,PROJECT_ID) VALUES (:name,:type,:PARENT_ID,:PROJECT_ID)'+
          '   select @@IDENTITY'+
          ' END ';


}


      //  if aTableInfo.UseProjectID then
       //   oParamList.FieldValues[FLD_PROJECT_ID] := params.Dest_ProjectID;

//        if oFolder.Project_ID>0 then
//           oParamList.FieldValues[FLD_PROJECT_ID]:=params.Dest_ProjectID
//        else
//           oParamList.SetNullByName(FLD_PROJECT_ID);

//           oParamList.FieldValues[FLD_PROJECT_ID]:=null;

        oParamList.FieldValues[FLD_PROJECT_ID]  :=
           IIF(oFolder.Project_ID>0, params.Dest_ProjectID, null);


        oParamList.FieldValues[FLD_PARENT_ID]  := IIF(aParent_DB_ID>0, aParent_DB_ID, null);
        oParamList.FieldValues[FLD_NAME] := oFolder.Name;
        oParamList.FieldValues[FLD_TYPE] := oFolder.ObjName;


     //   sSQL:= Format('SELECT id FROM Folder WHERE %s ', [sWhere]);
       // db_OpenQuery_list(qry_Dest, sSQL, oParamList);


        //-----------------------------------------------------------


        sSQL:= Format('SELECT id FROM Folder WHERE %s ', [sWhere]);
        db_OpenQuery_ParamList(qry_Dest, sSQL, oParamList);

        if qry_Dest.RecordCount=0 then
        begin
          s:=oParamList.MakeInsertSQL('Folder');


          sSQL:=
          '   INSERT INTO Folder (name,[type],PARENT_ID,PROJECT_ID) VALUES (:name,:type,:PARENT_ID,:PROJECT_ID)'+
          '   select @@IDENTITY';


         // sSQL:= Format('SELECT id FROM Folder WHERE %s ', [sWhere]);
          db_OpenQuery_ParamList(qry_Dest, sSQL, oParamList);

        //  iDB_ID:= qry_Dest.Fields[0].AsInteger;

        end;


        iDB_ID:= qry_Dest.Fields[0].AsInteger;


      s:= Format('UPDATE folder SET db_id=%d  WHERE id=%d', [ iDB_ID, oFolder.ID ]);
      dmMDB.ADOConnection.Execute(s);



     // iDB_ID :=qry_DestTable[FLD_ID];
      oFolder.DB_ID :=iDB_ID;

      FTable_ID_DB_ID_List.SetValue(TBL_Folder, oFolder.ID, iDB_ID);

//      if oFolder.SubItems.Count>0 then
 //       DoAddFolder(oFolder.SubItems, iDB_ID);

      if FFolderList.ChildrenExist (oFolder.ID) then
        DoAddFolder_( iDB_ID, oFolder.ID);


      end;

      FreeAndNil(oParamList);


    end;
    // ---------------------------------------------------------------

begin
  if FFolderList.Count=0 then
    exit;


  DoAddFolder_(0,0);

end;



procedure TdmMDB_to_SQL.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FDBStructure_dest);

  FreeAndNil(FFolderList);
  FreeAndNil(FTable_ID_DB_ID_List);
  FreeAndNil(FDest_TableNameList);

  inherited;
end;


procedure TdmMDB_to_SQL.Log(aMsg: string);
begin
  if Assigned(FOnLog) then FOnLog(aMsg);
end;

procedure TdmMDB_to_SQL.LogDebug(aMsg: string);
begin
  Log (aMsg);
end;

procedure TdmMDB_to_SQL.LogError(aMsg: string);
begin
  Log ('Error  !!!!!!!!!!!!!!! :'+ aMsg);
end;



//---------------------------------------------------
function TdmMDB_to_SQL.CheckLocated1(aSrcDataSet: TDataSet; aTableInfo:
    TTableInfo; var aKeyValue: variant; var aIsLocated: Boolean): boolean;
//---------------------------------------------------
//  True - located
//---------------------------------------------------

var
  oParamList: TdbParamList;
  sSQL: string;

  I: integer;
  iCount: Integer;
  sFieldName: string;
  vSrcValue,vDestValue: variant;

  oRefTable: TRefTable;
  s: string;
  sKey: string;
 // v: Variant;
  vArrParams: array of Variant;


 // oParamList: TdbParamList;

begin
  Result := False;

  aKeyValue:=null;


  if Length(aTableInfo.LocateFieldsArr)= 0 then
  begin
    raise Exception.Create(aTableInfo.TableName);
   // Exit;
  end;

(*

  if Eq(aTableInfo.TableName, 'linkCalcResults') then
    Log ('linkCalcResults');


  if Eq(aTableInfo.TableName, 'lib_Bands') then
    Log ('lib_Bands');
*)

  //Result := true;

 // db_ViewDataSet(FDestDataSet);

  SetLength (vArrParams, Length(aTableInfo.LocateFieldsArr));


  for I := 0 to High(aTableInfo.LocateFieldsArr) do
    if aTableInfo.LocateFieldsArr[i]<>'' then
  begin
    sFieldName:=aTableInfo.LocateFieldsArr[i];

//    if not Assigned( aSrcDataSet.FindField(sFieldName)) then
 //     Continue;



    vSrcValue:=aSrcDataSet.FieldValues[sFieldName];
//    vDestValue:=null;
    vDestValue := vSrcValue;

    if not VarIsNull (vSrcValue) then
    begin
      if Eq(sFieldName, FLD_FOLDER_ID) then
      begin
        vDestValue:=FTable_ID_DB_ID_List.GetValue(TBL_FOLDER, vSrcValue);

        s:= VarToStr(vDestValue);
        Assert(s<>'-');

      end
      else
      begin
        //����� ���� � ����������� ��������
        // ����� ������� ��� ����
        oRefTable:=aTableInfo.RefTables.FindByFieldName(sFieldName);

        if Assigned(oRefTable) then
        begin
          vDestValue:=FTable_ID_DB_ID_List.GetValue(oRefTable.TableName, vSrcValue);

          s:= VarToStr(vDestValue);
          if s='-' then
          begin
            FTable_ID_DB_ID_List.Show;

            s:=s;
          end;


          Assert(s<>'-');

          if vDestValue = null then
          begin
            s:= Format('%s - ref table: %s - field: %s',[aTableInfo.TableName, oRefTable.TableName, sFieldName]);
            //ShowMessage(s);
           // g_Log.Add('---error: no data: '+ s);

            Exit;
          end;


        end;
      end;

    end;// else
      //vDestValue := vSrcValue; //null


    vArrParams[i]:=vDestValue;
  end;


  //-----------------------------------
  // Locate �� ������ !!!
  //-----------------------------------
  // ����� � DEST �������
  // -------------------------


  oParamList:=TdbParamList.Create;

  for I := 0 to High(aTableInfo.LocateFieldsArr) do
    if aTableInfo.LocateFieldsArr[i]<>'' then
//    oParamList.AddItem(aTableInfo.LocateFieldsArr[i], vArrParams[i]);
      oParamList.FieldValues[aTableInfo.LocateFieldsArr[i]] := vArrParams[i];

  if aTableInfo.UseProjectID then
//    oParamList.AddItem('project_id', Params.Dest_ProjectID);
    oParamList.FieldValues['project_id']:= Params.Dest_ProjectID;

    

//  sSQL := oParamList.MakeSelectCountSQL();
//  sKey:= FDBStructure_dest.PrimaryKeyList.Values[aTableInfo.TableName];
  sKey:= FDBStructure_dest.GetPrimaryKeyForTable(aTableInfo.TableName);


 // FDBStructure_dest.Tables.FindByName(aTableInfo.TableName)



  sSQL := oParamList.MakeSelectSQL(aTableInfo.TableName, aTableInfo.KeyFieldName );

  qry_Dest.Connection := Params.Dest_ADOConnection;

  db_OpenQuery_ParamList(qry_Dest, sSQL, oParamList);

  aKeyValue:=qry_Dest.Fields[0].AsVariant;

  FreeAndNil(oParamList);


  aIsLocated:=not VarIsNull(aKeyValue);

  Result := True;

end;


function TdmMDB_to_SQL.SrcTableExists(aTableName: string): Boolean;
begin
  Result := dmMDB.TableExists(aTableName);
end;


function TdmMDB_to_SQL.DestTableExists(aTableName: string): Boolean;
begin
  Result := FDest_TableNameList.IndexOf(aTableName) >= 0;
end;


//---------------------------------------------------
function TdmMDB_to_SQL.CopyRecord_new(aTableInfo: TTableInfo; var aKeyValue:
    variant): Boolean;
//---------------------------------------------------
const
   FLD_LinkEnd_id     ='LinkEnd_id';
//   FLD_Pmp_Terminal_id='Pmp_Terminal_id';
 //  FLD_Pmp_Sector_id  ='Pmp_Sector_id';


//   FLD_link_repeater_id = 'link_repeater_id';


var
  oParamList: TdbParamList;


    // ---------------------------------------------------------------
    function DoAfterInsert: Boolean;
    // ---------------------------------------------------------------
    var
      iDB_ID: Integer;
      iDB_ID1: Integer;
      iDB_ID2: Integer;
      iID: Integer;
      iID1: Integer;
      iID2: Integer;
      iLinkEnd_id: Integer;
//      iPmpSector_id: Integer;
  //    iPmpTerminal_id: Integer;
      iProperty1 : Integer;
      iProperty2 : Integer;
      iProperty_DB_ID: Integer;
      v: Variant;
      vLinkEnd_id: Variant;
  //    vPmpTerminal_id: Variant;

      iLink_repeater_id : Integer;

    begin
      Result := True;


      // -------------------------
      if Eq(aTableInfo.DestTableName, 'link') then
      // -------------------------
      begin
 //       iID1:=oParamList.FieldValues[FLD_LinkEnd1_id];
  //      iID2:=oParamList.FieldValues[FLD_LinkEnd2_id];

        iID1:= FSrcDataSet.FieldByName(FLD_LinkEnd1_id).AsInteger;
        iID2:= FSrcDataSet.FieldByName(FLD_LinkEnd2_id).AsInteger;



        v := FTable_ID_DB_ID_List.GetValue('LinkEnd', iID1);
        if VarIsNull(v) then
        begin
        //  FTable_ID_DB_ID_List.Show;
          Exit;
        end;

        v := FTable_ID_DB_ID_List.GetValue('LinkEnd', iID2);
        if VarIsNull(v) then
        begin
        //  FTable_ID_DB_ID_List.Show;
          Exit;
        end;


        iDB_ID1 := FTable_ID_DB_ID_List.GetValue('LinkEnd', iID1);
        iDB_ID2 := FTable_ID_DB_ID_List.GetValue('LinkEnd', iID2);


      //  iProperty1 :=  GetPropertyByLinkEnd(iDB_ID1);
      //  iProperty2 :=  GetPropertyByLinkEnd(iDB_ID2);

{
        iProperty1 :=  GetPropertyByLinkEnd(iID1);
        iProperty2 :=  GetPropertyByLinkEnd(iID2);


        Assert(iProperty1>0, 'Value <=0');
        Assert(iProperty2>0, 'Value <=0');


//        FDestDataSet.FieldValues[FLD_Property1_id]:= iProperty1;
//        FDestDataSet.FieldValues[FLD_Property2_id]:= iProperty2;

        oParamList.FieldValues[FLD_Property1_id]:= iProperty1;
        oParamList.FieldValues[FLD_Property2_id]:= iProperty2;
 }

  //      Exit;

 (*       if (iProperty1=0) or (iProperty2=0) then
          Result := False
        else begin
          oParamList.FieldValues[FLD_Property1_id]:= iProperty1;
          oParamList.FieldValues[FLD_Property2_id]:= iProperty2;
*)

(*          FDestDataSet[FLD_Property1_id]  :=  iProperty1;
          FDestDataSet[FLD_Property2_id]  :=  iProperty2;
*)
      //  end;
      end else
{
      // -------------------------
      if Eq(aTableInfo.DestTableName, TBL_PMP_SECTOR) then
      // -------------------------
      begin
         iPmpSector_id:= FSrcDataSet.FieldByName(FLD_id).AsInteger;

         iProperty_DB_ID:=GetPropertyID_ByPmpSector(iPmpSector_id);

         Assert(iProperty_DB_ID>0);

          Result :=iProperty_DB_ID>0;

         if Result then
           oParamList.FieldValues[FLD_Property_id]:= iProperty_DB_ID;

      end else

}

      // -------------------------
      if Eq(aTableInfo.DestTableName, 'linkend_antenna') then
      // -------------------------
      begin
     //   try
       //   db_View(FSrcDataSet);

  //        iDB_ID:= FDestDataSet['LinkEnd_id'];
//         iPmpSector_id  := 0;
         iLinkEnd_id    := 0;
//         iPmpTerminal_id:= 0;


//         if Assigned(FSrcDataSet.FindField(FLD_Pmp_Sector_id)) then
//           iPmpSector_id  := FSrcDataSet.FieldByName(FLD_Pmp_Sector_id).AsInteger;

         if Assigned(FSrcDataSet.FindField(FLD_LinkEnd_id)) then
           iLinkEnd_id    := FSrcDataSet.FieldByName(FLD_LinkEnd_id).AsInteger;

//         if Assigned(FSrcDataSet.FindField(FLD_Pmp_Terminal_id)) then
//           iPmpTerminal_id:= FSrcDataSet.FieldByName(FLD_Pmp_Terminal_id).AsInteger;

{         if iPmpSector_id>0 then
         begin
           iProperty_DB_ID:=GetPropertyID_ByPmpSector(iPmpSector_id);

       //    Assert(iProperty_DB_ID>0);
         //  Assert(iProperty_DB_ID>0);

         end else

         if iPmpTerminal_id>0 then
         begin
           iProperty_DB_ID:=GetPropertyID_ByPmpTerminal(iPmpTerminal_id);

         //  Assert(iProperty_DB_ID>0);

         end else

}

         if iLinkEnd_id>0 then
         begin
           iProperty_DB_ID:=GetPropertyByLinkEnd(iLinkEnd_id);

        //   Assert(iProperty_DB_ID>0);
          // Result := False;

          // exit;
         end else
           raise Exception.Create('iProperty_DB_ID:=0;');
          // iProperty_DB_ID:=0;


         Assert(iProperty_DB_ID>0);


         //---------------------------------
         Result :=iProperty_DB_ID>0;

         if Result then
           oParamList.FieldValues[FLD_Property_id]:= iProperty_DB_ID;



//          FDestDataSet['Property_id'] := iProperty1;
      end;
    end;


var
  b: Boolean;
  bNoErrors: Boolean;
  sFieldName: string;
  oDestField: TField;
  oField: TField;
  vValue: OleVariant;
  vValue1: OleVariant;
  I: integer;
  iDB_ID: Integer;
  iID: Integer;
  iSrc_ID: Integer;
  oRefTable: TRefTable;
  oSrcField: TField;
  v: Variant;
  s: string;
  sName: string;


  sSQL: string;

///  sSQL: string;

begin
  oParamList:=TdbParamList.Create;



  Result := False;

//  qry_DestTable.Append;

  if Assigned(FSrcDataSet.FindField(FLD_NAME)) then
  begin
    sName := FSrcDataSet.FieldByName(FLD_NAME).AsString;

//    g_Log.Add(sName);
    DoLog2(sName);
  end;

 //   if Eq(aTableInfo.TableName,'linknet') then
    //  s :='';

  //-----------------------------------
  // ��� ������� ������ ���� � ����� �������
  //-----------------------------------

  if aTableInfo.UseProjectID then
  begin
    Assert(Params.Dest_ProjectID>0, 'Params.Dest_ProjectID <=0');

  //  FDestDataSet[FLD_PROJECT_ID]:=Params.Dest_ProjectID;

//    FDestDataSet.FieldValues[FLD_PROJECT_ID]:= Params.Dest_ProjectID;


    oParamList.FieldValues[FLD_PROJECT_ID]:= Params.Dest_ProjectID;
  end;

  //-------------------------------------------------------------------

 // i:=aSrcDataSet.FieldCount;

//  db_View(FSrcDataSet);

  for I := 0 to FSrcDataSet.FieldCount-1 do
  begin
    oSrcField :=FSrcDataSet.Fields[i];
    sFieldName:=oSrcField.FieldName;

//    Assert(oSrcField.Visible);


    if (FConfig_ref.ExceptedFields.IndexOf(sFieldName)>=0) or
       Eq(sFieldName, 'project_id') or
       (Copy(sFieldName, 1, 1)= '_')
    then
      oSrcField.Visible:=False;
//      FSrcDataSet.Fields.Remove(oSrcField);

  end;

 // db_View(FSrcDataSet);

 {

  for I := 0 to  FSrcDataSet.FieldCount-1  do
  begin
    oSrcField :=FSrcDataSet.Fields[i];

    sFieldName:=oSrcField.FieldName;

    if (FConfig_ref.ExceptedFields.IndexOf(sFieldName)>=0) or
       Eq(sFieldName, 'project_id') or
       (Copy(sFieldName, 1, 1)= '_')
    then begin
      oSrcField.Tag:=1;

 //     oField.Free;

  //    FSrcDataSet.Fields.Remove(oField);

    //  Log('ExceptedFields Remove - '+ sFieldName);
    //  Continue;
    end;
  end;
  }


  for I := 0 to FSrcDataSet.FieldCount - 1 do
  begin
    oSrcField :=FSrcDataSet.Fields[i];

    if not oSrcField.Visible then
       Continue;


//    if oSrcField.Tag = 1 then
  //     Continue;


    sFieldName:=FSrcDataSet.Fields[i].FieldName;


    oDestField:=FDestDataSet.FindField(sFieldName);

   {
    //----------------------------------------------------
    if (oSrcField.DataType = ftBlob) then
      if (oDestField.DataType = ftBlob) then
    begin


   //   db_CopyBlobField (oSrcField, oDestField);
     end;
    //----------------------------------------------------
    }


//    if Assigned(oDestField) then
//    begin
////      oDestField.Required
//    end;


    if not Assigned(oDestField) or (oDestField.ReadOnly) then
    begin
    //  Log('ReadOnly: '+ sFieldName);
      Continue;
    end;

    vValue:=FSrcDataSet.FieldValues[sFieldName];



//    if VarIsNull (vValue) then
 //     Continue;


    if Eq(sFieldName, FLD_FOLDER_ID)
    then begin
      if not VarIsNull(vValue) then
        vValue:=FTable_ID_DB_ID_List.GetValue(TBL_FOLDER, vValue);

    end else begin
      //��������� ����
      oRefTable:=aTableInfo.RefTables.FindByFieldName(sFieldName);

      if Assigned(oRefTable) then
      begin
        if not VarIsNull(vValue) then
        begin
          vValue1:=vValue;

          vValue:=FTable_ID_DB_ID_List.GetValue(oRefTable.TableName, vValue);

          s:=VarToStr(vValue);
          Assert(s<>'-');

        end;

        if VarIsNull(vValue) then
          if oRefTable.IsRequired  then
        begin
       //   FTable_ID_DB_ID_List.Show;

      //    raise Exception.Create(Format('��������� �������� ��� ����: ref table: %s, field: %s', [oRefTable.TableName, sFieldName]));

//          LogError('��������� �������� ��� ����: '+ sFieldName);
          g_Log.Add(Format('��������� �������� ��� ����: ref table: %s, field: %s', [oRefTable.TableName, sFieldName]));
          Exit;
        end;
      end;
    end;

    try
     ///////// FDestDataSet[sFieldName]:=vValue;


      if vValue<>null then
      begin
        oParamList.FieldValues[sFieldName]:= vValue;

//        FDestDataSet.FieldValues[sFieldName] :=  vValue;

      end;

    except

      on e: Exception do
      begin
        s:= Format('Exception: TableName:%s, FieldName:%s, Val: %s',
            [aTableInfo.TableName, sFieldName, VarToString(vValue)]);

        ShowMessage(s);

//        sFieldName + ' - '+ VarToString(vValue));
      end;
    end;
  end;

 // try
    bNoErrors:=DoAfterInsert();
 // except

 // end;


  for I := 0 to FSrcDataSet.FieldCount - 1 do
  begin
    oSrcField :=FSrcDataSet.Fields[i];

    if oSrcField.Tag = 1 then
       Continue;

    sFieldName:=oSrcField.FieldName;

    oDestField:=FDestDataSet.FindField(sFieldName);

    if Assigned(oDestField) and oDestField.Required then
        if varIsEmpty (FDestDataSet.FieldValues[sFieldName]) then
          ShowMessage('if Assigned(oDestField) and oDestField.Required');
 end;


  Result := True;

//  Exit;


   sSQL:=oParamList.MakeInsertSQL(aTableInfo.TableName);


 //  db_OpenQuery_list(qry_Dest, sSQL, oParamList);



  try
    if bNoErrors then
    begin


    //  SET ANSI_WARNINGS OFF --String or binary data would be truncated.

       Params.Dest_ADOConnection.Execute('SET ANSI_WARNINGS OFF');

       b:= db_ExecCommand_list(Params.Dest_ADOConnection, sSQL, oParamList);

    end;
  //  sSQL := oParamList.MakeInsertSQL(aTableInfo.TableName);
  except
   //  Result := True;


     on e: Exception do
      begin
        ShowMessage(sSQL);

        s:= Format('Exception: TableName:%s', [aTableInfo.TableName]);
        ShowMessage(s);

       {$IFDEF test}
         db_View(qry_DestTable);
       {$ENDIF}

      end;

  end;

 // db_MakeInsertQuery_list(aTableInfo.TableName, oParamList):

  try
  //  FDestDataSet.Post;

 //   if Eq(aTableInfo.TableName, 'link') then
  //    Log ('link');


  /////////////////////
   except
  //  on E: Exception do
   //   ShowMessage(sSQL);
  end;



  if Eq(aTableInfo.KeyFieldName, 'id')  then
  begin
    s:= Format('select IDENT_CURRENT (''%s'')', [ aTableInfo.TableName ]);

    db_OpenQuery(qry_Dest, s);

    //  RETURN IDENT_CURRENT ('AntennaType')


//    db_OpenQuery(qry_Dest, 'select @@IDENTITY');


//    iID:=qry_Dest.Fields[0].Value;

    aKeyValue :=qry_Dest.Fields[0].AsInteger;

    g_Log.Add('@@IDENTITY-'+ IntToStr(aKeyValue) );


   if FTest_UseDB_ID then
    begin
      iSrc_ID:=FSrcDataSet.fieldByName(FLD_ID).AsInteger;

      iDB_ID :=qry_Dest.Fields[0].AsInteger;

   //   db_UpdateRecord(FSrcDataSet, FLD_DB_ID, iDB_ID);

      s:= Format('UPDATE %s SET db_id=%d  WHERE id=%d', [aTableInfo.TableName, iDB_ID, iSrc_ID ]);
      dmMDB.ADOConnection.Execute(s);

    end;


  end else begin
    Assert(aTableInfo.KeyFieldName <> '');
    Assert(Assigned( FSrcDataSet.FindField(aTableInfo.KeyFieldName) ));

    aKeyValue:=FSrcDataSet.FieldValues[aTableInfo.KeyFieldName];

//    aKeyValue :=FSrcDataSet.FieldValues[aTableInfo.KeyFieldName].Value;
  end;


  FreeAndNil(oParamList);

end;



//---------------------------------------------------
procedure TdmMDB_to_SQL.CopyTable(aTableInfo: TTableInfo);
//---------------------------------------------------

   procedure DoUpdateProp(aID: Integer);
   var
     oParamList: TdbParamList;

     sSQL: string;

   begin
     oParamList:=TdbParamList.Create;

     oParamList.FieldValues[FLD_LAT]:=FSrcDataSet.FieldByName(FLD_LAT).AsFloat;
     oParamList.FieldValues[FLD_LON]:=FSrcDataSet.FieldByName(FLD_LON).AsFloat;

     oParamList.FieldValues[FLD_ADDRESS]:=FSrcDataSet.FieldByName(FLD_ADDRESS).AsString;


     sSQL:=oParamList.MakeUpdateString(TBL_Property,  aID);


     db_ExecCommand(Params.Dest_ADOConnection, sSQL, oParamList.MakeArr);

  //   db_OpenQuery_ParamList (qry_Dest, sSQL, oParamList);

     FreeAndNil (oParamList);


//     dmMDB.ADOConnection.Execute(s);

   end;


{

  sSQL := oParamList.MakeSelectSQL(aTableInfo.TableName, aTableInfo.KeyFieldName );

  qry_Dest.Connection := Params.Dest_ADOConnection;

  db_OpenQuery_list(qry_Dest, sSQL, oParamList);

  aKeyValue:=qry_Dest.Fields[0].AsVariant;

  FreeAndNil(oParamList);
}


var
  bLocated: boolean;
  iSrc_ID: Integer;
  arrVParams: array of Variant;
  s: string;
  vDestKey: Variant;
  vKeyValue: Variant;
  vSrcKey: Variant;
 //  aKeyValue: Variant;
  bIsLocated: Boolean;
  iDB_ID: Integer;
  k: Integer;
  kRecordCount: Integer;
  sKey: string;
  
label
   label_Exit;

begin           
  sKey:= FDBStructure_dest.GetPrimaryKeyForTable (aTableInfo.TableName);

  if sKey='' then
    ShowMessage(aTableInfo.TableName);


  Assert(sKey<>'');



  kRecordCount:=FSrcDataSet.RecordCount;


  with FSrcDataSet do
    while (not EOF) and (not Terminated) do
  begin
    DoProgress2(RecNo, RecordCount);




    //bLocated:=
    if not CheckLocated1 (FSrcDataSet, aTableInfo, vKeyValue, bLocated) then
    begin
     // raise Exception.Create('if not CheckLocated1 (FSrcDataSet, aTableInfo, vKeyValue, bLocated) then:  ' + aTableInfo.TableName);

      Next;
      Continue;
    end;



//    sKey:= FDBStructure_dest.PrimaryKeyList.Values[aTableInfo.TableName];
 //   sKey:= FDBStructure_dest.GetPrimaryKeyForTable (aTableInfo.TableName);
  //  Assert(sKey<>'');

    if bLocated then
    begin
      if Eq(aTableInfo.TableName, TBL_Property) then
        DoUpdateProp (vKeyValue);


      vSrcKey := FSrcDataSet.FieldByName(aTableInfo.KeyFieldName).AsVariant;



      //vKeyValue

      s:=aTableInfo.TableName;


(*       if Eq(aTableInfo.TableName,'property') then
       begin
          s:='';
       end;
*)

      FTable_ID_DB_ID_List.SetValue(aTableInfo.TableName, vSrcKey, vKeyValue);

      if (Assigned(FSrcDataSet.FindField(FLD_ID))) then
      begin
        iSrc_ID:=FSrcDataSet.FieldByName(FLD_ID).AsInteger;

        // -----------------------------
        if FTest_UseDB_ID then
        // -----------------------------
        begin
          iDB_ID:=vKeyValue;

          Assert(iDB_ID>0);


          db_OpenQuery(qry_check_db_id, Format('SELECT count(*) FROM %s  WHERE db_id=%d', [aTableInfo.TableName, iDB_ID ]));

          k:=qry_check_db_id.Fields[0].Value;

          if k>0 then
            k:=0;

         // Assert(k=0);

          s:= Format('UPDATE %s SET db_id=%d  WHERE id=%d', [aTableInfo.TableName, iDB_ID, iSrc_ID ]);
          dmMDB.ADOConnection.Execute(s);

    //      db_UpdateRecord(FSrcDataSet, FLD_DB_ID, vKeyValue);

        end;
       //   db_UpdateRecord(FSrcDataSet, FLD_DB_ID, vKeyValue);


      end;
    end

    //------------------------------------------
    // � ������ ���� ������ ������ - ����������
    //------------------------------------------

    //------------------------------
    //����� ������ �� �������� �����
    //------------------------------
    else
   // if (not bLocated) then
    begin

     // qry_DestTable.Append;

      if CopyRecord_new (aTableInfo, vKeyValue) then
      try
      //  qry_DestTable.Post;
        sKey:= FDBStructure_dest.GetPrimaryKeyForTable(aTableInfo.TableName);
//        sKey:= FDBStructure_dest.PrimaryKeyList.Values[aTableInfo.TableName];


        vSrcKey  := FSrcDataSet.FieldByName (aTableInfo.KeyFieldName).AsVariant;

      //  vDestKey := aKeyValue;
        //FDestDataSet.FieldByName(aTableInfo.KeyFieldName).AsVariant;

        FTable_ID_DB_ID_List.SetValue(aTableInfo.TableName, vSrcKey, vKeyValue);
//        FTable_ID_DB_ID_List.SetValue(aTableInfo.TableName, vSrcKey, vDestKey);

      except
        on e: Exception do
        begin
          s:= Format('Exception: TableName:%s', [aTableInfo.TableName]);
          ShowMessage(s);

         {$IFDEF test}
        //   db_View(qry_DestTable);
         {$ENDIF}

        end;
 //       begin
  //         LogError (e.Message);//   ShowMessage(e.Message);

   //     end;
      end
      else
      ;//  qry_DestTable.Cancel;
    end;

    Next;
  end;

/////////////////

//    db_View(qry_DestTable);

//  if Eq(aTableInfo.TableName,'property') then
 //   db_View(FSrcDataSet);

end;


procedure TdmMDB_to_SQL.ExecuteDlg1;
begin
  Progress_ExecDlg_proc(ExecuteProc);
end;


//-----------------------------------------------------------------------------
procedure TdmMDB_to_SQL.ImportTable(aTableInfo: TTableInfo);
//-----------------------------------------------------------------------------
begin
  g_Log.Add ('');
  g_Log.Add (Format('������� %s',[aTableInfo.TableName]));


  if not OpenSrcAndRefTable(aTableInfo) then
    Exit;


  if not OpenDestTable(aTableInfo) then
    Exit;

  CopyTable(aTableInfo);


  qry_SrcTable.Close;
  qry_DestTable.Close;


end;

// ---------------------------------------------------------------
procedure TdmMDB_to_SQL.LoadFolders;
// ---------------------------------------------------------------
begin
  if not SrcTableExists('Folder') then
    Exit;

  db_OpenQuery(qry_SrcTable, 'SELECT * FROM '+ 'Folder');

  FFolderList.LoadFromDataset (qry_SrcTable);

end;


//-------------------------------------------------------------------
function TdmMDB_to_SQL.OpenDestTable(aTableInfo: TTableInfo): boolean;
//-------------------------------------------------------------------
var
  sSQL: string;
begin

  Assert(aTableInfo.DestTableName<>'');

//  if aTableInfo.TableName='property' then
//   ;

  if not DestTableExists(aTableInfo.DestTableName) then
  begin
    g_Log.Add (Format('������� %s �� ���������� - destination data',[aTableInfo.TableName]));
    Result:=False;
    Exit;
  end;

//  OpenDestTableName(aTableInfo.DestTableName);

  if aTableInfo.SQL <> '' then
    sSQL:=ReplaceStr(aTableInfo.SQL, ':project_id', IntToStr (Params.Dest_ProjectID))
  else
  begin
    sSQL:='SELECT  * FROM ' + aTableInfo.DestTableName;  

    if aTableInfo.UseProjectID then
      sSQL:=sSQL + Format(' WHERE project_id=%d', [Params.Dest_ProjectID]);

  end;

    //UseProjectID

  g_Log.Add ('dest: '+ sSql);
  db_OpenQuery (qry_DestTable, sSQL);



//  if (aTableInfo.SQL <> '') or aTableInfo.UseProjectID  then
//    db_View (qry_DestTable);


  Result:=True;

end;


//-------------------------------------------------------------------
function TdmMDB_to_SQL.OpenSrcAndRefTable(aTableInfo: TTableInfo): boolean;
//-------------------------------------------------------------------
var
  sSQL: string;
  sTableName: string;
  I: integer;
  vValue: variant;
  j: integer;
  oRefTable: TRefTable;
  sFieldName: string;
  sRefFieldName: string;
begin
  Result := False;

(*  if (not SrcTableExists(aTableInfo.TableName))
       and (SrcTableExists(aTableInfo.TableName_old))
  then
    aTableInfo.TableName :=aTableInfo.TableName_old;
*)

  if (not SrcTableExists(aTableInfo.TableName)) then
  begin
    g_Log.Add (Format('������� %s �� ���������� - source data',[aTableInfo.TableName]));
    Exit;
  end;

//  FUpdateSQL.Clear;

  g_Log.Add('�������� ������� ' + aTableInfo.TableName);

  DoProgressMsg('�������� ������� ' + aTableInfo.TableName);

 // sTempSQL:='SELECT top 1 * FROM ' + aTableInfo.TableName;


  sSQL:='SELECT * FROM ' + aTableInfo.TableName;

  g_Log.Add ('source: '+ sSql);
  db_OpenQuery (qry_SrcTable, sSQL);

  if qry_SrcTable.RecordCount=0 then
  begin
    g_Log.Add( Format('� ������� %s 0 �������',[aTableInfo.TableName ]));
    Exit;
  end;

 // UpdateSrcTable(aTableInfo);


 // db_ViewDataSet(qry_SrcTable);
  //----------------------------------
  // ��������� Locate ����. �� ��� ����� �������� �����
  //----------------------------------
  for I := 0 to High(aTableInfo.LocateFieldsArr) do
  begin
    sFieldName:=aTableInfo.LocateFieldsArr[i];

    if not Assigned(qry_SrcTable.FindField(sFieldName)) then
    begin
      LogError(Format('LocateFieldsArr: ���� �� ���������� - �������: %s - %s', [aTableInfo.TableName, sFieldName]));

      aTableInfo.LocateFieldsArr[i]:='';;

   //   Exit;
     // b:=False;
    end;

  end;

  //----------------------------------
  // ��������� RefTables ����.
  //----------------------------------
  for I := 0 to aTableInfo.RefTables.Count - 1 do
  begin
    oRefTable := aTableInfo.RefTables[i];

    sTableName   :=oRefTable.TableName;
    sRefFieldName:=oRefTable.Master_FieldName;


    if not SrcTableExists (sTableName) then
    begin
      LogError (Format('������� %s �� ����������',[sTableName]));

      if oRefTable.IsRequired then
        Exit;

      Continue;
    end;


    if not Assigned(qry_SrcTable.FindField(sRefFieldName)) then
    begin
      LogError (Format('Ref ���� %s �� ����������',[sRefFieldName]));
      Continue;
    end;

  end;

  Result := True;
end;


//-----------------------------------------------------------------------------
procedure TdmMDB_to_SQL.ExecuteProc;
//-----------------------------------------------------------------------------
var
  I: Integer;
  oTableInfo: TTableInfo;
  s: string;
begin
  if not dmMDB.OpenMDB (Params.MDB_FileName) then
    Exit;

 // TdmConfig.Init;
  dmConfig.LoadConfig;
   //11(FConfig_ref);
  FConfig_ref :=dmConfig.ConfigObj;


  FDBStructure_dest.LoadFromADOConn(Params.Dest_ADOConnection);


  TdmMDB_validate.Init;
  dmMDB_validate.Execute;

 //// Exit;

//---------------------------------------------------------


//  db_SetComponentsADOConn([qry_SrcTable, AdOQuery1], dmMDB.ADOConnection);
  db_SetComponentsADOConn([qry_DestTable, qry_Dest ], Params.Dest_ADOConnection);


//  qry_Dest.Connection := Params.Dest_ADOConnection;

  FTable_ID_DB_ID_List.Clear;

  LoadFolders();

  Params.Dest_ADOConnection.GetTableNames (FDest_TableNameList);


  {$IFDEF test}

(*  for I := 0 to FConfig_ref.TableInfoList.Count - 1 do
  begin
    oTableInfo := FConfig_ref.TableInfoList[i];

    if not SrcTableExists(oTableInfo.TableName) then
      Continue;


    Log(oTableInfo.TableName);
  end;*)

//  Exit;

  {$ENDIF}


 //////
  AddFolder_new();


  for I := 0 to FConfig_ref.TableInfoList.Count - 1 do
  begin
    oTableInfo := FConfig_ref.TableInfoList[i];


   // ShowMessage (Format ('%d - %s',[i, oTableInfo.TableName]) );


    if not SrcTableExists(oTableInfo.TableName) then
      Continue;

  //  if oTableInfo.UseFolderID then
   //   AddFolder(oTableInfo);

//    if Eq(oTableInfo.TableName, 'PMP_SECTOR') then
 //     s:='';

 {
    if Eq(oTableInfo.TableName, 'LINK_repeater_ANTENNA') then
      s:='';
  }

{
    if Eq(oTableInfo.TableName, 'LINK_reflection_points') then
      s:='';


    if Eq(oTableInfo.TableName, 'LINKEND_ANTENNA') then
      s:='';

    if Eq(oTableInfo.TableName, 'PmpTerminal') then
      s:='';

    if Eq(oTableInfo.TableName, 'Pmp_SECTOR') then
      s:='';



    if Eq(oTableInfo.TableName, 'LinkLineLinkXREF') then
      s:='';

    if Eq(oTableInfo.TableName, 'LinkFREQPLAN_LINK') then
    begin
      s:='';

      FTable_ID_DB_ID_List.Show;
    end;

    ShowMessage(oTableInfo.TableName);
}
{

    if Eq(oTableInfo.TableName, 'LinkFREQPLAN_LINK') then
      s:='';

    if Eq(oTableInfo.TableName, 'LinkFREQPLAN') then
      s:='';
 }



    ImportTable(oTableInfo);



  ///////  FTable_ID_DB_ID_List.Show;


    FTable_ID_DB_ID_List.Check_Integrity;


  //  FTable_ID_DB_ID_List.Show;

  //  if Eq(oTableInfo.TableName, 'Property') then
   //   FTable_ID_DB_ID_List.Show;


  end;

  FTable_ID_DB_ID_List.SaveToFile(FLOgFileName);

  dmMDB.CloseMDB;


//  s:=Format ('exec test.sp_Project_restore_after_import (%d)', [Params.Dest_ProjectID]);

//  Params.Dest_ADOConnection.Execute(s);


  //test.sp_Project_restore_after_import



end;


// ---------------------------------------------------------------
function TdmMDB_to_SQL.GetPropertyByLinkEnd(aID: Integer): Integer;
// ---------------------------------------------------------------
var
  iPropertyID: Integer;
begin
  Result:=0;

  if dmMDB.TableExists(TBL_LINKEND) then
  begin
    db_OpenQuery(ADOQuery1,
          'SELECT Property_ID FROM ' + TBL_LINKEND +' WHERE id=' + IntToStr(aID));

    iPropertyID:=ADOQuery1.fieldByNAme(FLD_Property_ID).AsInteger;

    result := FTable_ID_DB_ID_List.GetValue(TBL_Property, iPropertyID);

  end;


 {
  if not qry_Linkend.Active then
    db_OpenQuery(qry_Linkend,
        'SELECT id,property_id FROM ' + TBL_LINKEND +
        ' WHERE project_id=' + IntToStr(Params.Dest_ProjectID));

  Result := AsInteger(qry_Linkend.Lookup(FLD_id, aID, FLD_property_id));
  }
end;


{
// ---------------------------------------------------------------
function TdmMDB_to_SQL.GetPropertyID_ByPmpSector(aID: Integer): Integer;
// ---------------------------------------------------------------
var
  iPMP_SITE_ID: Integer;
  iPropertyID: Integer;
begin
  Result:=0;
}
{
  if dmMDB.TableExists(TBL_PMP_SECTOR) then
  begin
    db_OpenQuery(ADOQuery1,
          'SELECT * FROM ' + TBL_PMP_SECTOR +' WHERE id=' + IntToStr(aID));

    Assert(ADOQuery1.RecordCount=1);

    iPMP_SITE_ID:=ADOQuery1.fieldByNAme(FLD_PMP_SITE_ID).AsInteger;
    Assert(iPMP_SITE_ID>0);

    db_OpenQuery(ADOQuery1,
          'SELECT * FROM ' + TBL_PMP_SITE +' WHERE id=' + IntToStr(iPMP_SITE_ID));

    iPropertyID:=ADOQuery1.fieldByNAme(FLD_Property_ID).AsInteger;

    result := FTable_ID_DB_ID_List.GetValue(TBL_Property, iPropertyID);

  end;
}

{  Assert(result>0);

 // Result := AsInteger(qry_Linkend.Lookup(FLD_id, aID, FLD_property_id));

end;

}
{
// ---------------------------------------------------------------
function TdmMDB_to_SQL.GetPropertyID_ByPmpTerminal(aID: Integer): Integer;
// ---------------------------------------------------------------
var
  iPropertyID: Integer;
begin
  Result:=0;

  if dmMDB.TableExists(TBL_PMPTERMINAL) then
  begin
    db_OpenQuery(ADOQuery1,
          'SELECT Property_ID FROM ' + TBL_PMPTERMINAL +' WHERE id=' + IntToStr(aID));

    iPropertyID:=ADOQuery1.fieldByNAme(FLD_Property_ID).AsInteger;

    result := FTable_ID_DB_ID_List.GetValue(TBL_Property, iPropertyID);

  end;


  Assert(Result>0);

 // Result := AsInteger(qry_Linkend.Lookup(FLD_id, aID, FLD_property_id));

end;

}

end.




{//-------------------------------------------------------------------
procedure TdmMDB_to_SQL.OpenDestTableName(aTableName: string);
//-------------------------------------------------------------------
var
  sSQL: string;
begin
  sSQL:='SELECT  * FROM ' + aTableName + ' WHERE (1<>1)';

  g_Log.Add ('dest: '+ sSql);
  db_OpenQuery (qry_DestTable, sSQL);

end;
}
