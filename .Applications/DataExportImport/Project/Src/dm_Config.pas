unit dm_Config;

interface

  {.$DEFINE CompactDatabase}


uses
  Classes, ADODB, DB, Forms, SysUtils,


  u_const_db,

  u_func,

  u_db,
  u_db_mdb,

  u_ini_data_export_import_params,

  u_Config_classes
  ;

type
  TdmConfig = class(TDataModule)
    ADOConnection: TADOConnection;
    ADOTable1: TADOTable;
    ds_Display_Groups: TDataSource;
    t_Display_Groups: TADOTable;
    view_Tables_new: TADOTable;
    view_Tables_Ref: TADOTable;
    view_Fields_replace_active: TADOTable;
    t_Fields_Exception: TADOTable;
    t_Tables_Relation: TADOTable;
    ds_view_Tables_new: TDataSource;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    ExportType: TExportModeType;
  public
    ConfigObj: TConfig;

    procedure GetCheckedTableList(aList: TStrings);
    procedure GetCheckedTableList_all_test(aList: TStrings);
   // class procedure Init;

    procedure LoadConfig;
    procedure LoadConfig1(aConfig: TConfig);

    procedure SetDisplayMode(aValue: TExportModeType);
  end;

 function dmConfig: TdmConfig;


//====================================================================
implementation
{$R *.dfm}

var
  FdmConfig: TdmConfig;


const
  DEF_FILENAME = 'data_export_import.mdb';

const
  view_Display_Groups_guides  = 'view_Display_Groups_guides';
  view_Display_Groups_project = 'view_Display_Groups_project';

  view_Tables_guide_checked   = 'view_Tables_guide_checked';
  view_Tables_project_checked = 'view_Tables_project_checked';



// ---------------------------------------------------------------
 function dmConfig: TdmConfig;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmConfig) then
    FdmConfig := TdmConfig.Create(Application);

  Result := FdmConfig;
end;



(*// ---------------------------------------------------------------
class procedure TdmConfig.Init;
// ---------------------------------------------------------------
begin
  if not Assigned(dmConfig) then
    dmConfig := TdmConfig.Create(Application);
end;
*)

procedure TdmConfig.DataModuleCreate(Sender: TObject);
var
  sMdbFileName: string;
begin
  sMdbFileName := GetApplicationDir + DEF_FILENAME;


  {$IFDEF CompactDatabase}
  mdb_CompactDatabase(sMdbFileName);
  {$ENDIF}


  if not mdb_OpenConnection(ADOConnection, sMdbFileName) then
    Halt;

  ConfigObj := TConfig.Create();

//  LoadConfig;
end;

procedure TdmConfig.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(ConfigObj);


  db_PostDataset(t_Display_Groups);

end;


procedure TdmConfig.LoadConfig;
begin
  LoadConfig1(ConfigObj);
end;


// ---------------------------------------------------------------
procedure TdmConfig.LoadConfig1(aConfig: TConfig);
// ---------------------------------------------------------------
var
  oTableInfo: TTableInfo;
  oMasterTableInfo: TTableInfo;
  oRefTable: TRefTable;
  s: string;
  s1: string;
  s2: string;
  i: Integer;

  oFieldReplace: TFieldReplace;

begin
  aConfig.ExceptedFields.Clear;
  aConfig.TableInfoList.Clear;
  aConfig.TablesChecked.Clear;


  // -------------------------
  case ExportType of
    mtExport_GUIDES:  s:=view_Tables_guide_checked;
    mtExport_PROJECT: s:=view_Tables_project_checked;
  else
    s:='';
  end;

  if s<>'' then
  begin
    db_TableReOpen(adoTable1, s);

    with adoTable1 do
      while not EOF do
      begin
        aConfig.TablesChecked.Add(FieldBYName('TableName').AsString);
        Next;
      end;
  end;

  // -------------------------

  db_TableReOpen(t_Fields_Exception, 'Fields_Exception');

  aConfig.ExceptedFields.LoadFromDataset(t_Fields_Exception);

(*  with adoTable1 do
    while not EOF do
    begin
      aConfig.ExceptedFields.Add(FieldBYName('name').AsString);
      Next;
    end;
*)

  // -------------------------
 // db_OpenQuery(ADOQuery1, 'SELECT * FROM '+ 'view_Tables_new');

  db_TableReOpen(view_Tables_new, 'view_Tables_new');

 // db_View(view_Tables_new);


  aConfig.TableInfoList.LoadFromDataset(view_Tables_new);

  {
  s:=aConfig.TableInfoList.GetKeyFieldName('CalcModelParams');  //CalcModelParams
  Assert(s<>'');
  }

(*
  with adoTable1 do
    while not EOF do
    begin
      oMasterTableInfo := aConfig.TableInfoList.AddItem();

      oMasterTableInfo.LoadFromDataset(adoTable1);

      Next;
    end;
*)

  // -------------------------
 // db_OpenQuery(ADOQuery1, 'SELECT * FROM '+ 'view_Tables_Ref');
  db_TableReOpen(view_Tables_Ref, 'view_Tables_Ref');
  aConfig.TableInfoList.LoadFromDataset_Ref(view_Tables_Ref);

  {
  with adoTable1 do
    while not EOF do
    begin
      s:=FieldByName('MasterTableName').AsString;
      oMasterTableInfo := aConfig.TableInfoList.FindByTableName(s);
                                  
   //   if s='Template_Linkend_Antenna' then
     //   s:=s;              


      s:=FieldByName('RefTableName').AsString;
      oTableInfo := aConfig.TableInfoList.FindByTableName(s);

      if not Assigned(oTableInfo) then
      begin
        s:=s;
      end;



      Assert(Assigned(oMasterTableInfo), 'procedure TdmConfig.LoadConfig1(aConfig: TConfig); '+s);
      Assert(Assigned(oTableInfo), 'procedure TdmConfig.LoadConfig1(aConfig: TConfig); '+ s);


      if (Assigned(oMasterTableInfo)) and (Assigned(oTableInfo))
      then begin
        oRefTable:=oMasterTableInfo.RefTables.AddItem;

        oRefTable.MasterTableInfoRef := oMasterTableInfo;
        oRefTable.TableInfoRef     :=oTableInfo;
        oRefTable.TableName        :=FieldByName('RefTableName').AsString;
        oRefTable.Master_FieldName :=FieldByName('MasterKey').AsString;

        oRefTable.IsRequired       :=FieldByName('Required').AsBoolean;
      end;

    //  end;
      Next;
    end;

    }

  // -------------------------
 // db_OpenQuery(ADOQuery1, 'SELECT * FROM '+ 'Fields_replace');

  db_TableReOpen(view_Fields_replace_active, 'view_Fields_replace_active');

  aConfig.FieldReplaceList.LoadFromDataset(view_Fields_replace_active);

//  SetLength(aConfig.FieldReplaceArr, adoTable1.RecordCount);
(*
  with adoTable1 do
    while not EOF do
    begin
      oFieldReplace := aConfig.FieldReplaceList.AddItem;

      oFieldReplace.TableName := FieldByName('TableName').AsString;
      oFieldReplace.Name_old  := FieldByName('Name_old').AsString;
      oFieldReplace.Name_new  := FieldByName('Name_new').AsString;
      oFieldReplace.DataType  := FieldByName('DataType').AsString;
      oFieldReplace.SQL.Text  := FieldByName('SQL').AsString;

       Next;
    end;
*)


  // -------------------------------------------------
  // Tables_Relation
  // -------------------------------------------------
  db_TableReOpen(t_Tables_Relation, 'Tables_Relation');

  SetLength(aConfig.Tables_Relation_arr, t_Tables_Relation.RecordCount);


  with t_Tables_Relation do
    while not EOF do
    begin
      i:=RecNo-1;
      aConfig.Tables_Relation_arr[i].TableName_Main  := FieldBYName('TableName_Main').AsString;
      aConfig.Tables_Relation_arr[i].TableName_Child := FieldBYName('TableName_Child').AsString;

      Next;
    end;        

end;

// ---------------------------------------------------------------
procedure TdmConfig.SetDisplayMode(aValue: TExportModeType);
// ---------------------------------------------------------------
begin
  ExportType := aValue;

  case aValue of
    mtExport_GUIDES:  db_TableOpen1(t_Display_Groups, view_Display_Groups_guides);
    mtExport_PROJECT: db_TableOpen1(t_Display_Groups, view_Display_Groups_project);
  end;

//  db_View (t_Display_Groups);

 // db_OpenQuery(ADOQuery1, 'select * from '+ view_Display_Groups_project);

//  db_View (ADOQuery1, 'procedure TdmConfig.SetDisplayMode(aValue: TExportModeType);');

 // db_View (t_Display_Groups, 'procedure TdmConfig.SetDisplayMode(aValue: TExportModeType);');

end;



// ---------------------------------------------------------------
procedure TdmConfig.GetCheckedTableList_all_test(aList: TStrings);
// ---------------------------------------------------------------
begin
  aList.Clear;

  aList.Add(TBL_PROPERTY);
  aList.Add(TBL_LINK);
//  aList.Add(TBL_LinkCalcResults) ;
  aList.Add(TBL_LINKFREQPLAN) ;
  aList.Add(TBL_LINKNET) ;
  aList.Add(TBL_LINKLINE) ;
  aList.Add(TBL_PMP_CALCREGION) ;



end;



// ---------------------------------------------------------------
procedure TdmConfig.GetCheckedTableList(aList: TStrings);
// ---------------------------------------------------------------
var
  sTable: string;
begin
  t_Display_Groups.First;

  aList.Clear;

  with t_Display_Groups do
    while not EOF do
    begin
      sTable:=FieldByName(FLD_TABLENAME).AsString;
      Assert(sTable <> '', 'procedure TdmConfig.GetCheckedTableList(aList: TStrings);');

      if FieldByName(FLD_CHECKED).AsBoolean then
         aList.Add(sTable);

      Next;
    end;


   Assert(aList.Count>0);



 // db_OpenQuery(ADOQuery1, 'select * from '+ view_Display_Groups_project);

//  db_View (ADOQuery1, 'procedure TdmConfig.SetDisplayMode(aValue: TExportModeType);');

 // db_View (t_Display_Groups, 'procedure TdmConfig.SetDisplayMode(aValue: TExportModeType);');


 

end;



end.







