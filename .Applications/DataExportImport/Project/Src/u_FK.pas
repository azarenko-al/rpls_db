unit u_FK;

interface


uses
  Classes, SysUtils, Variants,  db, adodb, Dialogs,
                                 
  System.Generics.Collections, 

  u_db,

  u_func
  ;


type
  TFKItemRefList = class;



  // -------------------------------------------------------------------
  TTableItem = class // (TCollectionItem)
  // -------------------------------------------------------------------
  public
    TableName: string;

    PrimaryKey_FieldName: string;

    Has_Project_ID : boolean;
    Has_Folder_ID : boolean;

//    IsRequired: Boolean; //allow NULL

  end;


 // ---------------------------------------------------------------
  TFKItem = class //(TCollectionItem)
  // ---------------------------------------------------------------

  public
    Name : string;

    PK_TableName : string;
    FK_TableName : string;

    PK_ColumnName : string;
    FK_ColumnName : string;

//    IsRequired: Boolean;

  end;


  // ---------------------------------------------------------------
  TFKItemList = //class(TCollection)
                class(TList<TFKItem>)
  
  // ---------------------------------------------------------------
  private
//    function GetItems(Index: Integer): TFKItem;
    procedure LoadFromADO(aADOQuery: TADOQuery);
    procedure LoadFromADOConn(aADOConnection: TADOConnection);
    procedure LoadFromDataset(aDataset: TDataset);
  public
  public
//    constructor Create;
//    destructor Destroy; override;

    function AddItem(): TFKItem;
//    function GetAsString: string;

    function MakeSQL(aTableName, aPrimaryKey, aWhereKey, aCondition: string):
        string;

    function SelectByTableName_FK(aTableName: string): TFKItemRefList;

    function SelectByTableName_PK(aTableName: string): TFKItemRefList;
    function IsTableHasParent(aTableName: string): Boolean;


//    property Items[Index: Integer]: TFKItem read GetItems; default;
  end;



  // ---------------------------------------------------------------
  TTableItemList = //class(TCollection)
                   class(TList<TTableItem>)
  
  // ---------------------------------------------------------------
  private
   // function GetItems(Index: Integer): TTableItem;
  public
//    constructor Create;
    function AddItem: TTableItem;
    function FindByName(aName: string): TTableItem;
    procedure LoadFromADO(aADOConnection: TADOConnection);

//    property Items[Index: Integer]: TTableItem read GetItems; default;
  end;



 // ---------------------------------------------------------------
  TFKItemRefList = //class(TList)
                   class(TList<TFKItem>)

  // ---------------------------------------------------------------
  private
//    function GetItems(Index: Integer): TFKItem;
  public
    procedure Show;
//    property Items[Index: Integer]: TFKItem read GetItems; default;
  end;

 // ---------------------------------------------------------------
  TPrimaryKeyList = class(TStringList)
  // ---------------------------------------------------------------
  private
    procedure LoadFromADO(aADOQuery: TADOQuery);
  public
    procedure LoadFromADOConn(aADOConnection: TADOConnection);

//    property Table[Index: string]: TFKItem read GetItems;

  end;


 // -------------------------------------------------------------------
  TDBTableField = class
  // -------------------------------------------------------------------
  private
//    function MakeSQL: string;
 ///   function GetFieldTypeStr: string;
  public
    TableName       : string;

    Name            : string;
    IsNullable      : Boolean;



{    IsVariableLen   : Boolean;

    IsComputed : Boolean;
    ComputedText : string;

    Type_           : string;
    Length          : Integer;

    IsIDENTITY      : Boolean;

    DEFAULT_NAME    : string;
    DEFAULT_CONTENT : string;
}

  end;

 // ---------------------------------------------------------------
  TDBTableFieldList =// class(TList)
                      class(TList<TDBTableField>)
  
  // ---------------------------------------------------------------
  private
//    function GetItems(Index: Integer): TDBTableField;
  public
    function AddItem: TDBTableField;
    function Find(aTable,aField: string): TDBTableField;

    function FindByName(aName: string): TDBTableField;
    procedure LoadFromADOConn(aADOConnection: TADOConnection);

 //   property Items[Index: Integer]: TDBTableField read GetItems; default;
  end;



// ---------------------------------------------------------------
  TDBStructure = class
  // ---------------------------------------------------------------
  private
    PrimaryKeyList: TPrimaryKeyList;

    TableFieldList: TDBTableFieldList;
  public
    FKItemList: TFKItemList;

    Tables: TTableItemList;


    constructor Create;
    destructor Destroy; override;

    procedure LoadFromADOConn(aADOConnection: TADOConnection);

    function GetPrimaryKeyForTable(aTableName: string): string;


//    property Table[Index: string]: TFKItem read GetItems;

  end;





implementation




// ---------------------------------------------------------------
procedure TFKItemList.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
var
  oFolder: TFKItem;
begin
  Clear;

  with aDataset do
    while not EOF do
    begin
      oFolder := AddItem;

      oFolder.PK_TableName   :=FieldByName('PK_Table_Name').AsString;
      oFolder.FK_TableName  :=FieldByName('FK_Table_Name').AsString;
      oFolder.PK_ColumnName  :=FieldByName('PK_Column_Name').AsString;
      oFolder.FK_ColumnName  :=FieldByName('FK_Column_Name').AsString;

      Next;
    end;

end;



// ---------------------------------------------------------------
function TFKItemList.SelectByTableName_PK(aTableName: string): TFKItemRefList;
// ---------------------------------------------------------------
var
  I: Integer;
  //iPos: Integer;
 // s1: string;
 // s2: string;
begin
  Result := TFKItemRefList.create;

  for I := 0 to Count - 1 do
    if Eq( Items[i].PK_TableName , aTableName) then
        Result.Add(Items[i]);

end;


// ---------------------------------------------------------------
function TFKItemList.SelectByTableName_FK(aTableName: string): TFKItemRefList;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Result := TFKItemRefList.create;

  for I := 0 to Count - 1 do
    if Eq( Items[i].FK_TableName , aTableName) then
        Result.Add(Items[i]);

end;

// ---------------------------------------------------------------
function TFKItemList.IsTableHasParent(aTableName: string): Boolean;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    if Eq(Items[i].FK_TableName, aTableName) and
       Eq(Items[i].PK_TableName, aTableName)
    then begin
      Result := True;
      Exit;
    end;

  Result := False;  
end;




//
//
//constructor TFKItemList.Create;
//begin
//  inherited Create(TFKItem);
//
//
//  //Selection_PK:= TFKItemRefList.create;
//  //Selection_FK:= TFKItemRefList.Create;
//
//end;
//
//
//function TFKItemList.GetItems(Index: Integer): TFKItem;
//begin
//  Result := TFKItem(inherited Items[Index]);
//
//end;
//
//
//
//function TFKItemRefList.GetItems(Index: Integer): TFKItem;
//begin
//  Result := TFKItem(inherited Items[Index]);
//
//end;

procedure TFKItemRefList.Show;
var
  I: Integer;
  s: string;
begin
  for I := 0 to Count - 1 do    // Iterate
  begin
    s:=s+             
      Format('PK_TableName: %s' +
             'PK_ColumnName: %s'+
             'FK_TableName: %s'+
             'FK_ColumnName: %s' + CRLF,

             [
              Items[i].PK_TableName,
              Items[i].PK_ColumnName,
              Items[i].FK_TableName,
              Items[i].FK_ColumnName
             ]);

  end;

  ShowMessage(s);
end;

//
//destructor TFKItemList.Destroy;
//begin
// // FreeAndNil(Selection_PK);
// // FreeAndNil(Selection_FK);
//
// 
//  inherited;
//  // TODO -cMM: TFKItemList.Destroy default body inserted
//end



function TFKItemList.AddItem(): TFKItem;
begin
  Result := TFKItem.Create; 
  Add(Result);
end;



//---------------------------------------------------------------------------
procedure TFKItemList.LoadFromADOConn(aADOConnection: TADOConnection);
//---------------------------------------------------------------------------
var
  oQuery: TADOQuery;
begin
  oQuery:=TADOQuery.Create(nil);
  oQuery.LockType:=ltReadOnly;
  oQuery.Connection:=aadoConnection;

  LoadFromADO (oQuery);

  oQuery.Free;

end;


// ---------------------------------------------------------------
procedure TFKItemList.LoadFromADO(aADOQuery: TADOQuery);
// ---------------------------------------------------------------
const
  DEF_SQL_2000 =
    ' Select TOP 100 PERCENT     '+
    '         OBJECT_NAME(sf.rkeyid)    AS PK_TABLE_NAME,'+
    '         col_name(sf.rkeyid, rkey) AS pk_column_name,'+
    '         OBJECT_NAME(sf.fkeyid)    AS fk_table_name,'+
    '         col_name(sf.fkeyid, fkey) AS fk_column_name,'+
    '         OBJECT_NAME(constid) AS Name'+
    '      FROM  sysforeignkeys sf INNER JOIN'+
    '            sysobjects so ON sf.fkeyid = so.id'+
    '      WHERE (so.xtype = ''U'')'+
    '       AND (so.name not like ''[_][_]%'')'+
    '       AND (so.name not like ''%[_][_]'')'+

    '      ORDER BY fk_table_name';


var
  oItem: TFKItem;
  sPK_ColumnName: string;
begin
  db_OpenQuery (aADOQuery, DEF_SQL_2000 );


  //db_View(aADOQuery);



  Clear;

  with aADOQuery do
    while not EOF do
    begin
      sPK_ColumnName  :=FieldByName('PK_Column_Name').AsString;

      if not Eq(sPK_ColumnName,'name') then
      begin

        oItem := AddItem;

        oItem.Name   :=FieldByName('Name').AsString;

        oItem.PK_TableName   :=FieldByName('PK_Table_Name').AsString;
        oItem.FK_TableName   :=FieldByName('FK_Table_Name').AsString;

        oItem.PK_ColumnName  :=FieldByName('PK_Column_Name').AsString;
        oItem.FK_ColumnName  :=FieldByName('FK_Column_Name').AsString;

      end;

      Next;
    end;

end;


// ---------------------------------------------------------------
function TFKItemList.MakeSQL(aTableName, aPrimaryKey, aWhereKey, aCondition:
    string): string;
// ---------------------------------------------------------------
var
  I: Integer;
  oSelection_FK: TFKItemRefList;
  s: string;
begin
  oSelection_FK:= SelectByTableName_FK(aTableName);

//  SelectByTableName(aTableName);

  s:=aPrimaryKey;

  for I := 0 to oSelection_FK.Count - 1 do
    s:=s + ','+ oSelection_FK[i].FK_ColumnName;

//  s:=s +

  if aCondition='' then
    Result :=  Format('SELECT %s FROM %s',  [s,aTableName])
  else
    Result :=  Format('SELECT %s FROM %s WHERE (%s in (%s))',
       [s,aTableName,aWhereKey, aCondition]);


  FreeAndNil(oSelection_FK);

end;

// ---------------------------------------------------------------
procedure TPrimaryKeyList.LoadFromADO(aADOQuery: TADOQuery);
// ---------------------------------------------------------------
const
  {
  DEF_SQL_ =
    'SELECT table_name  ,column_name '+
    'FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE   '+
    'WHERE OBJECTPROPERTY(OBJECT_ID(constraint_name), ''IsPrimaryKey'') = 1   '+
    'order by table_name,  column_name ';
  }
  
  DEF_SQL =
    'SELECT * FROM                         '+
    '  INFORMATION_SCHEMA.KEY_COLUMN_USAGE '+
    'WHERE                                 '+
    '  CONSTRAINT_NAME LIKE ''PK%''  or    '+
    '  CONSTRAINT_NAME LIKE ''%PK''        '+
    ' order by table_name,  column_name    ';


var
  s: string;
  sColumnName: string;
  sTableName: string;
begin

  db_OpenQuery (aADOQuery, DEF_SQL );

  Assert (aADOQuery.RecordCount>0);


//  db_View(aADOQuery);


  Clear;

  with aADOQuery do
    while not EOF do
    begin
      sTableName  :=FieldByName('table_name').AsString;
      sColumnName :=FieldByName('column_name').AsString;

      Assert(sColumnName<>'');


      Values[sTableName]:=sColumnName;

      Next;
    end;



//
//  k:=PrimaryKeyList.IndexOfName(aTableName);
//  Assert(k>=0, aTableName);



end;

//---------------------------------------------------------------------------
procedure TPrimaryKeyList.LoadFromADOConn(aADOConnection: TADOConnection);
//---------------------------------------------------------------------------
var
  oQuery: TADOQuery;
begin
  oQuery:=TADOQuery.Create(nil);
  oQuery.LockType:=ltReadOnly;
  oQuery.Connection:=aadoConnection;

  LoadFromADO (oQuery);

  oQuery.Free;

end;




function TDBTableFieldList.AddItem: TDBTableField;
begin
  Result := TDBTableField.Create;
  Add(Result);
end;


function TDBTableFieldList.FindByName(aName: string): TDBTableField;
var I: Integer;
begin
 Result := nil;

  for I := 0 to Count - 1 do
    if Eq(Items[i].Name, aName) then
    begin
      Result := Items[i];
      Break;
    end;
end;

//function TDBTableFieldList.GetItems(Index: Integer): TDBTableField;
//begin
//  Result := TDBTableField(inherited Items[Index]);
//end;

//---------------------------------------------------------------------------
procedure TDBTableFieldList.LoadFromADOConn(aADOConnection: TADOConnection);
//---------------------------------------------------------------------------
const
   SQL_ALL =
    ' SELECT  TOP 100 PERCENT  o.name AS tablename,   '+
    '   c.name,  c.iscomputed, '+ //AS size
 //   '    t.variable, t.name AS type,          '+ // 0 AS PK, AS Variable
 //   '    ColumnProperty(o.id, c.name, ''IsIdentity'') AS IsIdentity,   '+
    '    c.isnullable AS isnullable                                    '+
    ' FROM  sysobjects o INNER JOIN                                    '+
    '       syscolumns c ON o.id = c.id INNER JOIN                     '+
    '       systypes t ON c.xusertype = t.xusertype                    '+
    ' WHERE (o.xtype = ''U'')                                       ';
//    '     AND  (o.name <> ''dtp%'')                                    '+
//    '     AND  (o.name <> ''_tables'')                                 '+
  //  '     AND  (o.name NOT LIKE ''[_][_]%'')                              '+
  //  '      AND  (c.name NOT LIKE ''[_][_]%'')                          '+
//    ' ORDER BY o.name, c.colid                                         ';

  FLD_isnullable = 'isnullable';

var
  oQuery: TADOQuery;

  oField: TDBTableField;
begin
  oQuery:=TADOQuery.Create(nil);
  oQuery.LockType:=ltReadOnly;
  oQuery.Connection:=aadoConnection;

//  LoadFromADO (oQuery);
  db_OpenQuery(oQuery, SQL_ALL);


  with oQuery do
    while not EOF do
  begin
  //  sName     :=FieldValues[FLD_NAME];
  //  sTABLENAME:=FieldValues[FLD_TABLENAME];

    oField:=AddItem;
    oField.TableName:=FieldValues[FLD_TABLENAME];
    oField.Name:=FieldValues[FLD_NAME];
    oField.IsNullable:=FieldValues[FLD_isnullable];

    Next;
  end;


  oQuery.Free;

end;


// ---------------------------------------------------------------
function TDBTableFieldList.Find(aTable,aField: string): TDBTableField;
// ---------------------------------------------------------------
var I: Integer;
begin
 Result := nil;

  for I := 0 to Count - 1 do
    if Eq(Items[i].TableName , aTable) and
       Eq(Items[i].Name, aField)
    then begin
      Result := Items[i];
      Break;
    end;

end;


//---------------------------------------------------------------------------
constructor TDBStructure.Create;
//---------------------------------------------------------------------------
begin
  inherited;

  PrimaryKeyList:= TPrimaryKeyList.Create;
  FKItemList:= TFKItemList.Create;


  Tables:=TTableItemList.Create();


  TableFieldList := TDBTableFieldList.Create();
end;

//---------------------------------------------------------------------------
destructor TDBStructure.Destroy;
//---------------------------------------------------------------------------
begin
  FreeAndNil(TableFieldList);

  FreeAndNil(PrimaryKeyList);
  FreeAndNil(FKItemList);

  FreeAndNil(Tables);

  inherited;

end;


function TDBStructure.GetPrimaryKeyForTable(aTableName: string): string;
var
  k: Integer;
begin
  Assert(aTableName<>'');


//CalcModelParams
 // k:=PrimaryKeyList.IndexOfName(aTableName);
 // Assert(k>=0, aTableName);

  Result:= PrimaryKeyList.Values[aTableName];

  Assert(Result<>'', aTableName);

end;


procedure TDBStructure.LoadFromADOConn(aADOConnection: TADOConnection);
var
  i: Integer;

  oTableItem: TTableItem;
  s: string;
begin
  FKItemList.LoadFromADOConn(aADOConnection);
  PrimaryKeyList.LoadFromADOConn(aADOConnection);

  TableFieldList.LoadFromADOConn(aADOConnection);

  Tables.LoadFromADO(aADOConnection);

  Assert(Tables.Count>0);


  for i := 0 to TableFieldList.Count-1  do
  begin
    oTableItem:=Tables.FindByName(TableFieldList[i].TableName);

    if Assigned(oTableItem) then
    begin
      if Eq(TableFieldList[i].Name, 'project_id') then
        oTableItem.Has_Project_ID := True;

      if Eq(TableFieldList[i].Name, 'folder_id') then
        oTableItem.Has_folder_ID := True;

    end;

//    TableFieldList[i].TableName

  end;


  for i := 0 to Tables.Count-1  do
  begin
    Tables[i].PrimaryKey_FieldName:=PrimaryKeyList.Values[Tables[i].TableName];

  end;


//  ShellExec_Notepad_temp1(PrimaryKeyList.Text);


 // s:=GetPrimaryKeyForTable('CalcModelParams');
 // Assert(s<>'');


end;
//
//constructor TTableItemList.Create;
//begin
//  inherited Create(TTableItem);
//end;
//

function TTableItemList.AddItem: TTableItem;
begin
  Result := TTableItem.Create;
  Add(Result);
end;

//function TTableItemList.GetItems(Index: Integer): TTableItem;
//begin
//  Result := TTableItem(inherited Items[Index]);
//end;


function TTableItemList.FindByName(aName: string): TTableItem;
var I: Integer;
begin
 Result := nil;

  for I := 0 to Count - 1 do
    if Eq(Items[i].TableName, aName) then
    begin
      Result := Items[i];
      Break;
    end;
end;


// ---------------------------------------------------------------
procedure TTableItemList.LoadFromADO(aADOConnection: TADOConnection);
// ---------------------------------------------------------------
const
  SQL_SELECT_SQLSERVER_USER_TABLES_new =
    'select * from INFORMATION_SCHEMA.TABLES  '+
    'where (table_type=''base table'')        ';   //!!!!!!!!!

var
  oItem: TTableItem;
  oQuery: TADOQuery;
begin                   
  oQuery:=TADOQuery.Create(nil);
  oQuery.LockType:=ltReadOnly;
  oQuery.Connection:=aadoConnection;


  db_OpenQuery (oQuery, SQL_SELECT_SQLSERVER_USER_TABLES_new );

 // db_View(aADOQuery);


  Clear;

  with oQuery do
    while not EOF do
    begin
        oItem := AddItem;

        oItem.TableName :=FieldByName(FLD_TABLE_NAME).AsString;


      Next;
    end;


  oQuery.Free;

end;




end.


{


// ---------------------------------------------------------------
function TFKItemList.GetAsString: string;
// ---------------------------------------------------------------
var
  I: Integer;
  iPos: Integer;
  s1: string;
  s2: string;
begin
(*
  for I := 0 to Count - 1 do
  begin
    Result :=
      Format('PK_TableName: %',
         [Items[i].PK_TableName,
         ]);

    if Eq( Items[i].PK_TableName , aTableName) then
        Selection_PK.Add(Items[i]);

    if Eq( Items[i].FK_TableName , aTableName) then
        Selection_FK.Add(Items[i]);

  end;
*)
end;

