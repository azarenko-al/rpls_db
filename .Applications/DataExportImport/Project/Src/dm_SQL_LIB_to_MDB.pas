unit dm_SQL_LIB_to_MDB;

interface

uses
  SysUtils, Classes, Forms, Variants, ADODB, Db,  Dialogs,

  dm_MDB,

  d_Progress,

  dm_Main_Data,

  u_log,
  u_func,

  u_db,

  u_const_db,

  u_export_classes;


type
  TStrNotifyEvent = procedure (aValue: string) of object;

  TLogMsgType = (ltError,ltSQL);


  TdmSQL_LIB_to_MDB = class(TDataModule)
    qry_Source: TADOQuery;
    ADOStoredProc1: TADOStoredProc;
    tb_Dest: TADOTable;

//    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    function GetTableCreateSQL(aDataset: TDataSet; aTableName: String): string;

    procedure Log(aMsg: string);

  private
    FLogFileName: string;


    procedure Create_table_from_dataset(aDataset: TDataSet; aTableName: String);

    procedure CopyDatasets_new(aTableName: string; aSrcDataset, aDestDataset:
        TDataset);

    procedure CopyTable_new(aTableName, aIDS: string);


  private

  public


    Params: record
              Folder_id: integer;

              ObjName : string;

              MDBFileName : string;

            end;


    procedure ExecuteDlg;

    procedure ExecuteMain;

    class procedure Init;

  end;

var
  dmSQL_LIB_to_MDB: TdmSQL_LIB_to_MDB;


implementation
{$R *.DFM}



class procedure TdmSQL_LIB_to_MDB.Init;
begin
  if not Assigned(dmSQL_LIB_to_MDB) then
    dmSQL_LIB_TO_MDB := TdmSQL_LIB_TO_MDB.Create(Application);
end;


//------------------------------------------------------------------------------
procedure TdmSQL_LIB_to_MDB.DataModuleCreate(Sender: TObject);
//------------------------------------------------------------------------------

var
  k: Integer;
  s: string;
begin
  inherited;

  db_SetComponentsADOConn([tb_Dest], dmMDB.ADOConnection);

  FLogFileName := ChangeFileExt(Application.ExeName, '.log');

end;


// -------------------------------------------------------------------
procedure TdmSQL_LIB_to_MDB.CopyTable_new(aTableName, aIDS: string);
// -------------------------------------------------------------------
var
  I: Integer;
  s: string;

  sTableName: string;
begin
   if Eq(aTableName, TBL_LINK) then
     s:='';;

  s := Format('SELECT * FROM %s WHERE id in (%s) ', [ aTableName,  aIDS]);


  Log ('CopyTable_new - ' + s);


  db_OpenQuery(qry_Source, s);

  if qry_Source.RecordCount=0 then
    Exit;

//  if FTableNameList.IndexOf(aTableInfo.TableName)<0 then
  Create_table_from_dataset(qry_Source, aTableName);

  db_TableOpen1(tb_Dest, aTableName);


  CopyDatasets_new (aTableName, qry_Source, tb_Dest);



  qry_Source.Close;
  tb_Dest.Close;

end;



// -------------------------------------------------------------------
function TdmSQL_LIB_to_MDB.GetTableCreateSQL(aDataset: TDataSet;
    aTableName: String): string;
// -------------------------------------------------------------------
var
  I: integer;
  oSList: TStringList;
  s,s1: string;
  oField: TFieldDef;

begin
  oSList:=TStringList.Create;

  for I := 0 to aDataset.FieldDefs.Count-1 do
  begin
    oField :=aDataset.FieldDefs[I];


    case oField.DataType of

        ftWideString :   s:='varchar('+AsString(oField.Size)+')';
      else
//        raise Exception.Create('');
    //    Result:='';
      end;



    s1:='['+oField.Name+']'+' '+
        db_TypeFieldToString11 (oField.DataType, oField.Size);

 //   if Eq(aKeyFieldName, oField.Name) then
   //   s1:=s1+ '  PRIMARY KEY';

    oSList.Add(s1);
  end;
         


  oSList.Delimiter :=',';
  oSList.QuoteChar:=' ';
  s1:=oSList.DelimitedText;

  s1:='CREATE TABLE '+ aTableName +' (' + oSList.DelimitedText + ')';

  Result :=s1;

  FreeAndNil(oSList);
end;


// -------------------------------------------------------------------
procedure TdmSQL_LIB_to_MDB.Create_table_from_dataset(aDataset: TDataSet;
    aTableName: String);
// -------------------------------------------------------------------
var
  S: string;
begin
//  if FTableNameList.IndexOf(aTableName)>=0 then
//    Exit;

  s:=GetTableCreateSQL (aDataset, aTableName);

  g_Log.Add(s);

  dmMDB.ADOConnection.Execute(S);

//  FTableNameList.Add(aTableName);


end;


procedure TdmSQL_LIB_to_MDB.Log(aMsg: string);
begin
  g_Log.Msg(aMsg);
end;


// -------------------------------------------------------------------
procedure TdmSQL_LIB_to_MDB.CopyDatasets_new(aTableName: string; aSrcDataset,
    aDestDataset: TDataset);
// -------------------------------------------------------------------
var
  bTerminated: Boolean;
begin
  Progress_SetProgressMsg2(aTableName);

  Assert(aSrcDataSet.RecordCount>0);


  // ------------------------------------
  // ���������� ���� �������
  // ------------------------------------
  aSrcDataSet.First;


  with aSrcDataSet do
    while not Eof do
    begin
      Progress_SetProgress2(aSrcDataset.RecNo, aSrcDataSet.RecordCount, bTerminated);
      if bTerminated then
        Exit;

      db_AddRecordFromDataSet(aSrcDataset, aDestDataSet);

      Next;
    end;
end;




// ---------------------------------------------------------------
procedure TdmSQL_LIB_to_MDB.ExecuteDlg;
// ---------------------------------------------------------------
var
  sFileName: string;
begin
  sFileName  := Params.MDBFileName;
(*
  if FileExists(sFileName) then
    if ConfirmDlg('���� ����������, ������������?') then
    begin
      if not DeleteFile (sFileName) then
      begin
        ShowMessage('���� �� ����� ���� �����������, ��� �������');
        Exit;
      end;
    end else
      Exit;

*)
  if FileExists(sFileName) then
    if not DeleteFile (sFileName) then
    begin
      ShowMessage('���� �� ����� ���� �����������, ��� �������');
      Exit;
    end;


  Progress_ExecDlg_proc(ExecuteMain);
end;  



// ---------------------------------------------------------------
procedure TdmSQL_LIB_to_MDB.ExecuteMain;
// ---------------------------------------------------------------
const
  bTerminated: Boolean=False;

var
  b: Boolean;
  i: integer;
  k: Integer;
  s: string;

  sSQL: string;
  sTableName: string;

  //oSList: TStringList;
  sID: string;

  arrStr: TStringArr;
  j: Integer;
 // sPrimaryKey: string;

  //oRefItem: TRefItem;

//  oSList: TStringList;

begin

  db_SetComponentsADOConn([qry_Source, ADOStoredProc1], dmMain_Data.ADOConnection_SQL);
  s:=dmMain_Data.ADOConnection_SQL.ConnectionString;
  
  db_SetComponentsADOConn([tb_Dest], dmMDB.ADOConnection);

//  db_SetComponentsADOConn([tb_Dest], dmMDB.ADOConnection);

  bTerminated :=False;

//  FTableNameList.Clear;

//  FTable_ID_DB_ID_List.clear;


  Assert(Params.ObjName<>'');

//  FDBStructure.LoadFromADOConn(dmMain.ADOConnection);


  if not dmMDB.CreateMDB(params.MDBFileName) then
    Exit;



   k:=db_ExecStoredProc__new(ADOStoredProc1, 'export.sp_Lib_folder_export_SEL',
        [FLD_FOLDER_id, Params.Folder_id,
         FLD_OBJNAME,   Params.ObjName

        ],

         [flOpenQuery]
      );



  // db_SetComponentsADOConn([tb_Dest], dmMDB.ADOConnection);
        

   with ADOStoredProc1 do
     while not EOF do
     begin
       CopyTable_new(FieldValues[FLD_Table_Name], FieldValues[FLD_IDS]);


       Next;
     end;


  dmMDB.ADOConnection.Close;


 ///// ShellExec(params.MDBFileName);


end;

end.


