unit d_Filter;

interface

uses
  Classes, Controls, Forms,
  cxGridLevel, 
  cxGridDBTableView, cxGrid, DB,
  ADODB, RxMemDS,

  dm_Config,
  u_Config_classes,
  u_export_classes,


  cxGridCustomTableView, cxGridTableView, cxClasses,
  cxControls, cxGridCustomView
  ;

type
  Tdlg_Filter = class(TForm)
    cxGrid1: TcxGrid;
    cxGrid1DBTableView2: TcxGridDBTableView;
    col_ID: TcxGridDBColumn;
    col_checked: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView2DBColumn1: TcxGridDBColumn;
    ADOQuery1: TADOQuery;
    RxMemoryData1: TRxMemoryData;
    DataSource1: TDataSource;
    procedure FormCreate(Sender: TObject);
  private
   
  public
    class function ExecDlg(aTableName: string; aStrList: TStrings; aProject_ID:
        Integer): boolean;

  end;

var
  dlg_Filter: Tdlg_Filter;

implementation

{$R *.dfm}



class function Tdlg_Filter.ExecDlg(aTableName: string; aStrList: TStrings;
    aProject_ID: Integer): boolean;
var
  s: string;
  oTableInfo: TTableInfo;

begin
  with Tdlg_Filter.Create(Application) do
  begin
    oTableInfo:=dmConfig.ConfigObj.TableInfoList.FindByTableName(aTableName);
    Assert(Assigned(oTableInfo), 'Value not assigned');
(*
    s:='SELECT * FROM ' + aTableName;
    if oTableInfo.UseProjectID then
      s:=s+ Format(' WHERE project_id=', [aProjectID]);

    db_OpenQuery()

    qry_Source

*)

    ShowModal;

    Free;
  end;

end;


procedure Tdlg_Filter.FormCreate(Sender: TObject);
begin
//  db_SetComponentsADOConn([qry_Source], dmMain.ADOConnection);
end;


end.
