unit u_SQL_classes;

interface

uses
  Classes, DB, u_sync_classes;

type
  //-------------------------------------------------------------------
  TDBObject_FK = class(TCollectionItem)
  //-------------------------------------------------------------------
  public
    pk_table_name  : string;
    pk_column_name : string;

    fk_table_name  : string;
    fk_column_name : string;
  public
  end;


  //-------------------------------------------------------------------
  TDBObject_FK_List = class(TCollection)
  //-------------------------------------------------------------------
  private
    function GetItems(Index: Integer): TDBObject_FK;
  public
    constructor Create;
    function AddItem: TDBObject_FK;
    function FindByName(aName: string): TDBObject;
    property Items[Index: Integer]: TDBObject_FK read GetItems; default;
  end;

implementation

constructor TDBObject_FK_List.Create;
begin
  inherited Create(TDBObject_FK);
end;

function TDBObject_FK_List.AddItem: TDBObject_FK;
begin
  Result := TDBObject_FK (inherited Add);
end;

function TDBObject_FK_List.FindByName(aName: string): TDBObject;
var I: Integer;
begin
 Result := nil;

  for I := 0 to Count - 1 do
    if Eq(Items[i].Name, aName) then
    begin
      Result := Items[i];
      Break;
    end;
end;

function TDBObject_FK_List.GetItems(Index: Integer): TDBObject_FK;
begin
  Result := TDBObject_FK(inherited Items[Index]);
end;

end.
