unit dm_SQL;

interface

uses
  SysUtils, Classes, DB, ADODB;

type
  TdmSQL = class(TDataModule)
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    FK_ARR: array of record
              pk_table_name  : string;
              pk_column_name : string;

              fk_table_name  : string;
              fk_column_name : string;

            end;



    { Private declarations }
  public
    procedure ExportFKToDataSet;
    class procedure Init;

  end;

var
  dmSQL: TdmSQL;

implementation

{$R *.dfm}

procedure TdmSQL.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
procedure TdmSQL.ExportFKToDataSet;
//--------------------------------------------------------------------
const
   SQL_SEL_TABLES =
    'SELECT TOP 100 PERCENT                                                '+
    '   OBJECT_NAME(sf.rkeyid) AS PK_TABLE_NAME,                            '+
    '   col_name(sf.rkeyid, rkey) AS pk_column_name,                        '+
    '   OBJECT_NAME(sf.fkeyid) AS fk_table_name,                            '+
    '   col_name(sf.fkeyid, fkey) AS fk_column_name,                        '+
    '   ObjectProperty(constid, ''CnstIsUpdateCascade'') as IsUpdateCascade,   '+
    '   ObjectProperty(constid, ''CnstIsDeleteCascade'') as IsDeleteCascade,   '+
    '   OBJECT_NAME(constid) AS fk_name                                    '+
    'FROM  sysforeignkeys sf INNER JOIN                                    '+
    '      sysobjects so ON sf.fkeyid = so.id                              '+
    'WHERE (so.xtype = ''U'') AND (so.name not like ''[_][_]%'')           '+
    'ORDER BY so.name                                                      ';

var
  i: Integer;
 
begin
  db_OpenQuery (qry_Temp, SQL_SEL_TABLES);

  SetLength(FK_ARR, qry_Temp.RecordCount);


  with qry_Temp do
    while not EOF do
  begin
    i:=qry_Temp.RecNo;

    FK_ARR[i].pk_table_name  := FieldByName('PK_TABLE_NAME').AsString;
    FK_ARR[i].pk_column_name := FieldByName('pk_column_name').AsString;

    FK_ARR[i].fk_table_name  := FieldByName('fk_table_name').AsString;
    FK_ARR[i].fk_column_name := FieldByName('fk_column_name').AsString;

    Next;
  end;
end;

class procedure TdmSQL.Init;
begin
  if not Assigned(dmSQL) then
    dmSQL := TdmSQL.Create(Application);

end;

end.
