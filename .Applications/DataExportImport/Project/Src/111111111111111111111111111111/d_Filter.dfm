object dlg_Filter: Tdlg_Filter
  Left = 684
  Top = 212
  Width = 438
  Height = 518
  Caption = 'dlg_Filter'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 430
    Height = 297
    Align = alTop
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView2: TcxGridDBTableView
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      object col_ID: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
      end
      object col_checked: TcxGridDBColumn
        Caption = ' '
        DataBinding.FieldName = 'checked'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Options.Sorting = False
        Width = 33
      end
      object cxGrid1DBTableView2DBColumn1: TcxGridDBColumn
        Width = 383
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView2
    end
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    Left = 192
    Top = 328
  end
  object RxMemoryData1: TRxMemoryData
    FieldDefs = <>
    Left = 72
    Top = 328
  end
  object DataSource1: TDataSource
    DataSet = RxMemoryData1
    Left = 72
    Top = 384
  end
end
