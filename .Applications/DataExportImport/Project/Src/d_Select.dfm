object dlg_Select: Tdlg_Select
  Left = -4
  Top = -4
  BorderWidth = 5
  Caption = 'dlg_Select'
  ClientHeight = 1006
  ClientWidth = 2550
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = False
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 497
    Height = 1006
    Align = alLeft
    TabOrder = 0
    ExplicitHeight = 509
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      object cxGrid1DBTableView1RecId: TcxGridDBColumn
        DataBinding.FieldName = 'RecId'
        Visible = False
      end
      object cxGrid1DBTableView1checked: TcxGridDBColumn
        DataBinding.FieldName = 'checked'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.ImmediatePost = True
        Properties.NullStyle = nssUnchecked
        Width = 55
      end
      object cxGrid1DBTableView1folder_name: TcxGridDBColumn
        DataBinding.FieldName = 'folder_name'
        Options.Editing = False
        SortIndex = 0
        SortOrder = soAscending
        Width = 208
      end
      object cxGrid1DBTableView1name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Options.Editing = False
        Width = 130
      end
      object cxGrid1DBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Options.Editing = False
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object DBGrid1: TDBGrid
    Left = 560
    Top = 8
    Width = 481
    Height = 193
    DataSource = DataSource2
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object dxMemData1: TdxMemData
    Active = True
    Indexes = <>
    Persistent.Option = poNone
    SortOptions = []
    Left = 568
    Top = 248
    object dxMemData1folder_name: TStringField
      DisplayWidth = 500
      FieldName = 'folder_name'
      Size = 500
    end
    object dxMemData1id: TAutoIncField
      DisplayWidth = 10
      FieldName = 'id'
    end
    object dxMemData1folder_id: TIntegerField
      DisplayWidth = 10
      FieldName = 'folder_id'
    end
    object dxMemData1name: TWideStringField
      DisplayWidth = 100
      FieldName = 'name'
      Size = 100
    end
    object dxMemData1gain: TFloatField
      DisplayWidth = 10
      FieldName = 'gain'
    end
    object dxMemData1gain_type: TIntegerField
      DisplayWidth = 10
      FieldName = 'gain_type'
    end
    object dxMemData1tilt_type: TIntegerField
      DisplayWidth = 10
      FieldName = 'tilt_type'
    end
    object dxMemData1freq: TFloatField
      DisplayWidth = 10
      FieldName = 'freq'
    end
    object dxMemData1diameter: TFloatField
      DisplayWidth = 10
      FieldName = 'diameter'
    end
    object dxMemData1omni: TBooleanField
      DisplayWidth = 5
      FieldName = 'omni'
    end
    object dxMemData1vert_width: TFloatField
      DisplayWidth = 10
      FieldName = 'vert_width'
    end
    object dxMemData1horz_width: TFloatField
      DisplayWidth = 10
      FieldName = 'horz_width'
    end
    object dxMemData1fronttobackratio: TFloatField
      DisplayWidth = 10
      FieldName = 'fronttobackratio'
    end
    object dxMemData1polarization_ratio: TFloatField
      DisplayWidth = 10
      FieldName = 'polarization_ratio'
    end
    object dxMemData1polarization_str: TWideStringField
      DisplayWidth = 50
      FieldName = 'polarization_str'
      Size = 50
    end
    object dxMemData1weight: TFloatField
      DisplayWidth = 10
      FieldName = 'weight'
    end
    object dxMemData1band: TStringField
      DisplayWidth = 50
      FieldName = 'band'
      Size = 50
    end
    object dxMemData1vert_mask: TMemoField
      DisplayWidth = 10
      FieldName = 'vert_mask'
      BlobType = ftMemo
    end
    object dxMemData1horz_mask: TMemoField
      DisplayWidth = 10
      FieldName = 'horz_mask'
      BlobType = ftMemo
    end
    object dxMemData1electrical_tilt: TFloatField
      DisplayWidth = 10
      FieldName = 'electrical_tilt'
    end
    object dxMemData1_____: TBooleanField
      DisplayWidth = 5
      FieldName = '_____'
    end
    object dxMemData1comment: TWideStringField
      DisplayWidth = 250
      FieldName = 'comment'
      Size = 250
    end
    object dxMemData1file_content: TMemoField
      DisplayWidth = 10
      FieldName = 'file_content'
      BlobType = ftMemo
    end
    object dxMemData1______: TBooleanField
      DisplayWidth = 5
      FieldName = '______'
    end
    object dxMemData1guid: TGuidField
      DisplayWidth = 38
      FieldName = 'guid'
      Size = 38
    end
    object dxMemData1date_created: TDateTimeField
      DisplayWidth = 18
      FieldName = 'date_created'
    end
    object dxMemData1date_modify: TDateTimeField
      DisplayWidth = 18
      FieldName = 'date_modify'
    end
    object dxMemData1user_created: TWideStringField
      DisplayWidth = 128
      FieldName = 'user_created'
      Size = 128
    end
    object dxMemData1user_modify: TWideStringField
      DisplayWidth = 128
      FieldName = 'user_modify'
      Size = 128
    end
  end
  object DataSource1: TDataSource
    DataSet = dxMemData1
    Left = 568
    Top = 296
  end
  object ADOQuery1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select '
      ''
      'dbo.fn_Folder_FullPath (folder_id)   as folder_name,'
      ''
      '*'
      ''
      ''
      ' from   antennaType')
    Left = 664
    Top = 248
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Password=sa;Persist Security Info=True;User I' +
      'D=sa;Data Source=onega_link'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 664
    Top = 368
  end
  object DataSource2: TDataSource
    DataSet = ADOQuery1
    Left = 664
    Top = 296
  end
  object FormPlacement1: TFormPlacement
    IniFileName = 'Software\Onega\'
    Left = 808
    Top = 248
  end
end
