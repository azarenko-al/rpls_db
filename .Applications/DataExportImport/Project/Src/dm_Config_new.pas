unit dm_Config_new;

interface

uses
  SysUtils, Classes, DB,  forms,   ADODB;

type
  TdmConfig_new111111 = class(TDataModule)
    ADOConnection: TADOConnection;
    t_Project: TADOTable;
    ds_Project: TDataSource;
    view_Table_new_lib: TADOTable;
    ADOQuery1: TADOQuery;
     procedure DataModuleCreate(Sender: TObject);
  private
    LibList: TStringList;
    { Private declarations }
    procedure LoadLibTables;

    
    class procedure Init;

  public


    { Public declarations }
  end;


implementation

{$R *.dfm}

var
  dmConfig_new111111: TdmConfig_new111111;



class procedure TdmConfig_new111111.Init;
begin
  if not Assigned(dmConfig_new111111) then
    dmConfig_new111111:=TdmConfig_new111111.Create(Application);
end;


procedure TdmConfig_new111111.DataModuleCreate(Sender: TObject);
begin
  LibList := TStringList.Create();
end;

//------------------------------------------------------
procedure TdmConfig_new111111.LoadLibTables;
//------------------------------------------------------
var
  sTableName: string;
begin

  with view_Table_new_lib do
    while not EOF do
    begin
      sTableName:= FieldByName ('TableName').AsString;

      LibList.Add(sTableName);


      Next;
    end;



  // TODO -cMM: TdmConfig_new.LoadLibTables default body inserted
end;

end.
