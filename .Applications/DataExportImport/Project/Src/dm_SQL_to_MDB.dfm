object dmSQL_to_MDB1: TdmSQL_to_MDB1
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 944
  Top = 496
  Height = 327
  Width = 359
  object qry_Source: TADOQuery
    Parameters = <>
    Left = 40
    Top = 180
  end
  object tb_Dest: TADOTable
    Left = 120
    Top = 176
  end
  object ADOConnection_MDB1: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=C:\ON' +
      'EGA\RPLS_DB\bin\GuidesExport.mdb;Mode=Share Deny None;Extended P' +
      'roperties="";Persist Security Info=False;Jet OLEDB:System databa' +
      'se="";Jet OLEDB:Registry Path="";Jet OLEDB:Database Password="";' +
      'Jet OLEDB:Engine Type=5;Jet OLEDB:Database Locking Mode=1;Jet OL' +
      'EDB:Global Partial Bulk Ops=2;Jet OLEDB:Global Bulk Transactions' +
      '=1;Jet OLEDB:New Database Password="";Jet OLEDB:Create System Da' +
      'tabase=False;Jet OLEDB:Encrypt Database=False;Jet OLEDB:Don'#39't Co' +
      'py Locale on Compact=False;Jet OLEDB:Compact Without Replica Rep' +
      'air=False;Jet OLEDB:SFP=False'
    ConnectionTimeout = 5
    LoginPrompt = False
    Mode = cmShareExclusive
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 53
    Top = 19
  end
  object q_Ref_keys: TADOQuery
    Parameters = <>
    SQL.Strings = (
      ''
      'select '
      ''
      '    object_name(fkeyid)  FK_Table, '
      '    c1.name '#9#9#9' FK_Column,'
      '    '
      '    object_name(rkeyid)  PK_Table, '
      '    c2.name '#9#9#9' PK_Column'
      '    '
      '    '
      ' from sysforeignkeys s'
      
        '   inner join syscolumns c1 on ( s.fkeyid = c1.id and s.fkey = c' +
        '1.colid )'
      
        '   inner join syscolumns c2 on ( s.rkeyid = c2.id and s.rkey = c' +
        '2.colid )')
    Left = 216
    Top = 12
  end
  object q_PK: TADOQuery
    Parameters = <>
    SQL.Strings = (
      ''
      'select '
      ''
      '    object_name(fkeyid)  FK_Table, '
      '    c1.name '#9#9#9' FK_Column,'
      '    '
      '    object_name(rkeyid)  PK_Table, '
      '    c2.name '#9#9#9' PK_Column'
      '    '
      '    '
      ' from sysforeignkeys s'
      
        '   inner join syscolumns c1 on ( s.fkeyid = c1.id and s.fkey = c' +
        '1.colid )'
      
        '   inner join syscolumns c2 on ( s.rkeyid = c2.id and s.rkey = c' +
        '2.colid )')
    Left = 216
    Top = 68
  end
end
