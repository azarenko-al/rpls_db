inherited frm_Main_Import_Lib: Tfrm_Main_Import_Lib
  Left = 1122
  Top = 281
  BorderStyle = bsSizeToolWin
  Caption = 'frm_Main_Import_Lib'
  ClientHeight = 544
  ClientWidth = 537
  ParentFont = True
  OldCreateOrder = True
  Position = poDesktopCenter
  ExplicitLeft = 1122
  ExplicitTop = 281
  ExplicitWidth = 545
  ExplicitHeight = 572
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 509
    Width = 537
    ExplicitTop = 524
    ExplicitWidth = 554
    inherited Bevel1: TBevel
      Width = 537
      ExplicitWidth = 554
    end
    inherited Panel3: TPanel
      Left = 339
      Width = 198
      ExplicitLeft = 356
      ExplicitWidth = 198
      inherited btn_Ok: TButton
        Left = 28
        ExplicitLeft = 28
      end
      inherited btn_Cancel: TButton
        Left = 113
        ExplicitLeft = 113
      end
    end
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100
      TabOrder = 1
      OnClick = Button1Click
    end
  end
  inherited pn_Top_: TPanel
    Width = 537
    ExplicitWidth = 554
    inherited Bevel2: TBevel
      Width = 537
      ExplicitWidth = 554
    end
    inherited pn_Header: TPanel
      Width = 537
      ExplicitWidth = 554
    end
  end
  object pn_Main_full: TPanel [2]
    Left = 0
    Top = 225
    Width = 537
    Height = 160
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 554
    object GroupBox_file: TGroupBox
      Left = 0
      Top = 0
      Width = 537
      Height = 40
      Align = alTop
      Caption = #1055#1072#1087#1082#1072' '#1089#1086' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1072#1084#1080' (MDB)'
      TabOrder = 0
      ExplicitWidth = 554
      object DirectoryEdit1: TDirectoryEdit
        Left = 2
        Top = 15
        Width = 533
        Height = 23
        OnAfterDialog = DirectoryEdit1AfterDialog
        Align = alClient
        DialogKind = dkWin32
        NumGlyphs = 1
        TabOrder = 0
        Text = ''
        ExplicitWidth = 550
      end
    end
    object CheckListBox1: TCheckListBox
      Left = 0
      Top = 40
      Width = 537
      Height = 88
      Align = alTop
      Columns = 1
      ItemHeight = 13
      TabOrder = 1
    end
  end
  object gb_ConnectToSQL: TGroupBox [3]
    Left = 0
    Top = 60
    Width = 537
    Height = 165
    Align = alTop
    Caption = #1055#1086#1076#1082#1083#1102#1095#1077#1085#1080#1077' '#1082' '#1041#1044
    Constraints.MinHeight = 165
    TabOrder = 3
  end
  inherited ActionList1: TActionList
    Left = 464
  end
  inherited FormStorage1: TFormStorage
    Left = 296
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 376
  end
end
