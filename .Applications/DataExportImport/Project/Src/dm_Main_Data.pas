unit dm_Main_Data;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms,

  u_db,

  u_const_db,

//  dm_Main,

  u_geo,

  u_func,
  u_common_const

  ;

type
  TdmMain_Data = class(TDataModule)
    qry_Projects: TADOQuery;
    ds_Projects: TDataSource;
    qry_Prop_max_h: TADOQuery;
    qry_Prop: TADOQuery;
    ADOConnection_SQL: TADOConnection;
    t_Projects: TADOTable;
    DataSource1: TDataSource;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure OpenProjects;
    { Private declarations }
  public
    function GetPropInfo(aName: string; var aPos: TBLPoint): Boolean;
    class procedure Init;
    
    function OpenADOConnectionString(aConnectionString: string): Boolean;

    procedure OpenProjectData1(aProject_ID: Integer);

  end;

var
  dmMain_Data: TdmMain_Data;

implementation

{$R *.dfm}

procedure TdmMain_Data.DataModuleCreate(Sender: TObject);
begin
//  db_SetComponentADOConnection(Self, dmMain.ADOConnection);

end;

class procedure TdmMain_Data.Init;
begin
  if not Assigned(dmMain_Data) then
    dmMain_Data := TdmMain_Data.Create(Application);
  
end;


function TdmMain_Data.OpenADOConnectionString(aConnectionString: string):
    Boolean;
begin
  Result:= db_OpenADOConnectionString (ADOConnection_SQL, aConnectionString);
  
end;

//-----------------------------------------------------------
procedure TdmMain_Data.OpenProjects;
//-----------------------------------------------------------
begin
 // qry_Projects.Connection :=dmMain.ADOConnection;
  db_OpenQuery(qry_Projects, 'SELECT id,name FROM '+TBL_project+' ORDER BY name');

end;


//-----------------------------------------------------------
procedure TdmMain_Data.OpenProjectData1(aProject_ID: Integer);
//-----------------------------------------------------------

(*const
   view_Property_Max_height = 'view_Property_Max_height';
*)
begin
  Assert(aProject_ID>0);


(*
//  qry_Projects.Connection :=dmMain.ADOConnection;
  db_OpenQuery(qry_Prop_max_h,
      'SELECT * FROM '+view_Property_Max_height+
      ' WHERE project_id=' + IntToStr(aProject_ID) );
*)


  db_OpenQuery(qry_Prop,
      'SELECT * FROM '+ TBL_Project +
      ' WHERE id=' + IntToStr(aProject_ID) );

  Assert(qry_Prop.RecordCount =1);


  db_OpenQuery(qry_Prop,
      'SELECT * FROM '+ TBL_Property +
      ' WHERE project_id=' + IntToStr(aProject_ID) );


end;

//-----------------------------------------------------------
function TdmMain_Data.GetPropInfo(aName: string; var aPos: TBLPoint): Boolean;
//-----------------------------------------------------------
begin
  Result := qry_Prop.Locate(FLD_Name, aName, []) ;

  if Result then
  begin
    aPos := db_ExtractBLPoint(qry_Prop);
  end;


end;            




end.
