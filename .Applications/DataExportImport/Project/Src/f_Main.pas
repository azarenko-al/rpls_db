unit f_Main;

interface

uses
  DB, ADODB, DBCtrls, SysUtils, Classes, Controls, Forms,
  ComCtrls, cxGridLevel, Dialogs, StdCtrls, ActnList, ExtCtrls,  rxToolEdit,
  Menus,  cxGridDBTableView, cxGrid,
 
  
  cxDBData, 


  u_files,

  u_classes,

  u_cx_Grid,

//  dm_Onega_DB_data,

  u_log,

  dm_Config,
  dm_Main_Data,
                      

//  d_Project_Browse1,

  dm_MDB,

  dm_SQL_lib_to_MDB,

  dm_SQL_to_MDB_new,

 // dm_SQL_to_MDB,
  dm_MDB_to_SQL,

  d_Wizard,           


  u_func,
  u_DB,
  u_dlg,

  u_const_db,

  u_ini_data_export_import_params,


  StdActns, cxGridCustomTableView, cxGridTableView, cxClasses, cxControls,
  cxGridCustomView, Mask, cxPropertiesStore, rxPlacemnt, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxCheckBox, cxButtonEdit,
  System.Actions;

type
  Tfrm_Main_1 = class(Tdlg_Wizard)
    qry_Projects: TADOQuery;
    ds_Projects: TDataSource;
    GroupBox1_Log: TGroupBox;
    RichEdit1: TRichEdit;
    PopupMenu1: TPopupMenu;
    ActionList2: TActionList;
    act_Import_Project_: TAction;
    act_Import_Guides_: TAction;
    act_Export_Project_: TAction;
    act_Export_Guides_: TAction;
    Action11: TMenuItem;
    Action21: TMenuItem;
    Action31: TMenuItem;
    Action41: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    spDictionaryClear1: TMenuItem;
    sfdf1: TMenuItem;
    pn_Main_full: TPanel;
    GroupBox_Project: TGroupBox;
    DBLookupComboBox1_Project: TDBLookupComboBox;
    GroupBox_file: TGroupBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet_Selected: TTabSheet;
    Memo1: TMemo;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView2: TcxGridDBTableView;
    col_ID: TcxGridDBColumn;
    col_checked: TcxGridDBColumn;
    col_Caption: TcxGridDBColumn;
    cxGrid1DBTableView2index: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn1: TcxGridDBColumn;
    col_TableName: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    Splitter1: TSplitter;
    Button1: TButton;
    StatusBar1: TStatusBar;
    TabSheet_Test: TTabSheet;
    Button2: TButton;
    ActionList3: TActionList;
    FileOpen1: TFileOpen;
    GroupBox1_multi11: TGroupBox;
    ListBox2: TListBox;
    Button4: TButton;
    FilenameEdit1: TFilenameEdit;
//    procedure FormDestroy(Sender: TObject);
//    procedure act_Import_Project_Execute(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
//    procedure FileOpen1Accept(Sender: TObject);
//    procedure FileOpen1BeforeExecute(Sender: TObject);
  //  procedure Button2Click(Sender: TObject);

    procedure FormCreate(Sender: TObject);
//    procedure sfdf1Click(Sender: TObject);
//    procedure spDictionaryClear1Click(Sender: TObject);
    procedure StatusBar1DblClick(Sender: TObject);
//    procedure cxGrid1DBTableView2DBColumn1PropertiesButtonClick(
//      Sender: TObject; AButtonIndex: Integer);
//    procedure cxGrid1Exit(Sender: TObject);
  //  procedure FormClose(Sender: TObject; var Action: TCloseAction);
//    procedure FormActivate(Sender: TObject);
  private

  //  FDataSet: TDataset;


 //   FMode: TExportModeType;

    FParams_ref: TIni_Data_export_import_params;

    procedure ImportData;
//    procedure ExportData1;
    procedure ExportData_new;

    function GetProjectID: Integer;
    procedure OpenProjects;

    procedure Log(aValue: string);


  public
    procedure SetDisplayMode(aValue: TExportModeType);

    //aMode: TExportModeType; aProjectID : Integer;
//    procedure Init(aParams :TIni_Data_export_import_params);
    procedure Init;
  end;

var
  frm_Main_1: Tfrm_Main_1;

implementation

//uses dm_Test_Export_Import;

{$R *.DFM}

//-----------------------------------------------------------
procedure Tfrm_Main_1.FormCreate(Sender: TObject);
//-----------------------------------------------------------
begin
  inherited;

  
  cx_Grid_Init (Self);


 // Caption :='������-������� ������ '+ GetAppVersionStr();


  Caption:= '������-������� ������ '+  GetAppVersionStr() + '  ������: '
    + FormatDateTime ('yyyy-mm-dd hh:mm', GetFileDate (Application.Exename));


  SetActionName ('������-������� ������ �� SQL-������� � ���� MS ACCESS');

  PageControl1.Align:=alClient;

 // pn_Main_full.Align:=alClient;

  GroupBox1_Log.Align:=alClient;

  act_Ok.Caption:='���������';
  act_Cancel.Caption:='�������';

  cxGrid1.Align:=alClient;

// .. AddComponentProp(ed_FileName, 'FileName');

//  StatusBar1.SimpleText:=dmMain.LoginRec.ConnectionStatusStr;

  SetDefaultWidth;

(*  if FParams_ref.MdbFileName<>'' then
    ed_FileName.FileName := FParams_ref.MdbFileName;
*)

 // TdmConfig.Init;
  cxGrid1DBTableView2.DataController.DataSource :=dmConfig.ds_Display_Groups;

  TabSheet_Test.TabVisible:=False;


//  FDataSet:=dmConfig.ds_Display_Groups.DataSet;


//  dmConfig.SetDisplayMode(mtPROJECT);

//  dmMDB.SetDisplayMode (mtPROJECT);

 // SetDisplayMode(mtImport_PROJECT);
//  SetDisplayMode (mtGUIDES);

 // DBLookupComboBox1.KeyValue :=qry_Projects['id'];

  g_Log.RichEdit := RichEdit1;

  StatusBar1.SimpleText:= g_Ini_Data_export_import_params.ConnectionString;
  

  //  C:\111111111111111111111111111111111111111\�����������_80GHz\E-Band\EBand_Ant_80GHz.temp.mdb

//  GroupBox1_multi.Visible:= g_Ini_Data_export_import_params.Mode in [mtImport_GUIDES,mtImport_PROJECT];
//  GroupBox_file.Visible  := not GroupBox1_multi.Visible;

end;


//-----------------------------------------------------------
procedure Tfrm_Main_1.OpenProjects;
//-----------------------------------------------------------
begin
  if not qry_Projects.Active then
  begin
    qry_Projects.Connection :=dmMain_Data.ADOConnection_SQL;
    db_OpenQuery(qry_Projects, 'SELECT id,name FROM '+TBL_project+' ORDER BY name');

    DBLookupComboBox1_Project.KeyValue :=g_Ini_Data_export_import_params.ProjectID;
  end;

end;



//-----------------------------------------------------------
procedure Tfrm_Main_1.act_OkExecute(Sender: TObject);
//-----------------------------------------------------------
begin
  case FParams_ref.Mode of
    mtImport_PROJECT,
    mtImport_Guides:  ImportData;

    mtExport_selected,

    mtExport_PROJECT,
    mtExport_Guides: ExportData_new;

      //ExportData;// (DBLookupComboBox1.KeyValue);
  else

    raise Exception.Create('FMode=none');
//    ShowMessage('FMode=none');
  end;
end;




procedure Tfrm_Main_1.StatusBar1DblClick(Sender: TObject);
begin
//  if dmMain.OpenDB_Dlg then
//  begin
//    StatusBar1.SimpleText:=dmMain.LoginRec.ConnectionStatusStr;
//    OpenProjects;
//  end;
end;


procedure Tfrm_Main_1.Button1Click(Sender: TObject);
begin
  ExportData_new;
end;

procedure Tfrm_Main_1.Button2Click(Sender: TObject);
begin
  inherited;
end;



//-----------------------------------------------------------
procedure Tfrm_Main_1.ExportData_new;
//-----------------------------------------------------------
var
  I: Integer;
  iProjectID: Integer;
begin
  FilenameEdit1.FileName:=ChangeFileExt(FilenameEdit1.FileName, '.mdb');


 // db_PostDataset(cxGrid1DBTableView2.DataController.DataSource.DataSet);
//  iProject_ID:=AsInteger(DBLookupComboBox1.KeyValue);
 // dmSQL_to_MDB.Params.Src_ADOConnection := dmMain.ADOConnection;
 // dmSQL_to_MDB.Params.Dest_ADOConnection:= dmMDB.ADOConnection;
 // dmSQL_to_MDB.OnLog :=Log;

  iProjectID := AsInteger(DBLookupComboBox1_Project.KeyValue);

  db_PostDataset(dmConfig.ds_Display_Groups.DataSet);


  if FParams_ref.IsFolder1 then
  begin


    TdmSQL_lib_to_MDB.Init;

//    if FParams_ref.TableName<>'' then
//    begin
    dmSQL_lib_to_MDB.params.Folder_id:=FParams_ref.Folder_ID;

     // dmSQL_to_MDB_new.params.FilterRecords_List.Clear;

 //     dmSQL_to_MDB_new.Params.IDS:=FParams_ref.SelectedIDList.MakeDelimitedText_ID;
//

  //    for I := 0 to FParams_ref.SelectedIDList.Count - 1 do
  //      dmSQL_to_MDB_new.FilterRecords_List.Add(IntToStr(FParams_ref.SelectedIDList[i].id));


  //  end;


    dmSQL_lib_to_MDB.params.MDBFileName:=FilenameEdit1.FileName;
    dmSQL_lib_to_MDB.params.ObjName    :=FParams_ref.TableName;

  //  dmSQL_to_MDB_new.params.Project_ID:=iProjectID;
    dmSQL_lib_to_MDB.ExecuteDlg;

    FreeAndNil(dmSQL_lib_to_MDB);


  end else
  begin
    TdmSQL_to_MDB_new.Init;

    if FParams_ref.TableName<>'' then
    begin
      dmSQL_to_MDB_new.params.TableName:=FParams_ref.TableName;

     // dmSQL_to_MDB_new.params.FilterRecords_List.Clear;

      dmSQL_to_MDB_new.Params.IDS:=FParams_ref.SelectedIDList.MakeDelimitedText_ID;


  //    for I := 0 to FParams_ref.SelectedIDList.Count - 1 do
  //      dmSQL_to_MDB_new.FilterRecords_List.Add(IntToStr(FParams_ref.SelectedIDList[i].id));


    end;


    dmSQL_to_MDB_new.params.MDBFileName:=FilenameEdit1.FileName;
    dmSQL_to_MDB_new.params.Project_ID:=iProjectID;
    dmSQL_to_MDB_new.ExecuteDlg;

    FreeAndNil(dmSQL_to_MDB_new);

  end;

  //  MsgDlg('���������');



  MsgDlg('���������');



end;




//------------------------------------------------------------------------------
procedure Tfrm_Main_1.ImportData;
//------------------------------------------------------------------------------
var
  iProjectID: Integer;
begin
  assert(FilenameEdit1.FileName <> '', 'ed_FileName.FileName = _');

  FilenameEdit1.FileName:=ChangeFileExt(FilenameEdit1.FileName, '.mdb');


  if not FileExists(FilenameEdit1.FileName) then
  begin
    ShowMessage('���� �� ����������...' + FilenameEdit1.FileName);

    exit;
  end;


(*  if not dmMDB.OpenMDB (ed_FileName.FileName) then
    Exit;
*)

  iProjectID := AsInteger(DBLookupComboBox1_Project.KeyValue);


 // dmMDB_to_SQL.OnLog :=Log;
  TdmMDB_to_SQL.Init;

  dmMDB_to_SQL.OnLog := Log;

  dmMDB_to_SQL.Params.Dest_ProjectID:=iProjectID;
  dmMDB_to_SQL.Params.MDB_FileName :=FilenameEdit1.FileName;
  dmMDB_to_SQL.Params.Dest_ADOConnection:= dmMain_Data.ADOConnection_SQL;

 // dmMDB_to_SQL.Params.IsRefreshRecords:= cb_UpdateExistingObjects.Checked;//  AsBoolean(row_UpdateExistent.Text)

//  dmMDB_to_SQL.ExecuteProc;
  dmMDB_to_SQL.ExecuteDlg;

  FreeAndNil(dmMDB_to_SQL);

  dmMDB.ADOConnection.Close;

  MsgDlg('���������');
end;

// ---------------------------------------------------------------
procedure Tfrm_Main_1.SetDisplayMode(aValue: TExportModeType);
// ---------------------------------------------------------------
var
  b: Boolean;
begin
//  FMode :=aValue;

  dmConfig.SetDisplayMode (aValue);

  b:=aValue in [mtExport_PROJECT, mtImport_PROJECT];

  GroupBox_Project.visible  := b;
  if b then
    OpenProjects();

  b:=aValue in [mtImport_PROJECT, mtImport_GUIDES];

  if aValue = mtExport_Selected then
    PageControl1.ActivePageIndex :=1
  else
    PageControl1.ActivePageIndex :=0;


//  PageControl1

 // pn_Main.Visible :=not b;

end;

// ---------------------------------------------------------------
procedure Tfrm_Main_1.Init;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  FParams_ref := g_Ini_Data_export_import_params;

  SetDisplayMode(FParams_ref.Mode);

  DBLookupComboBox1_Project.KeyValue :=FParams_ref.ProjectID;




  if FParams_ref.TableName<>'' then
  begin
    Memo1.Lines.Add( FParams_ref.TableName);

    for I := 0 to FParams_ref.SelectedIDList.Count - 1 do
      Memo1.Lines.Add( IntToStr(FParams_ref.SelectedIDList[i].ID));
  end;



  if FParams_ref.FileName_MDB<>'' then
    begin
   //   ShowMessage(FParams_ref.MdbFileName);
      FilenameEdit1.FileName := FParams_ref.FileName_MDB;


      if FParams_ref.IsAuto then
      begin
        act_Ok.Execute;
        Close;
      end;

    end;


  
end;


function Tfrm_Main_1.GetProjectID: Integer;
begin
  Result := DBLookupComboBox1_Project.KeyValue;
end;




procedure Tfrm_Main_1.Log(aValue: string);
begin
  RichEdit1.Lines.Add(aValue);
end;

end.


{

procedure Tfrm_Main_1.Button2Click(Sender: TObject);
begin
//  TdmTest_Export_Import.Init;
//  dmTest_Export_Import.Exec;
end;



procedure Tfrm_Main_1.FileOpen1Accept(Sender: TObject);
var
  i: Integer;
begin

{
  ListBox1.Items.Clear;


  for i:=0 to FileOpen1.Dialog.Files.Count-1 do
    if RightStr(FileOpen1.Dialog.Files[i], Length('.temp.mdb')) <> '.temp.mdb'
      ListBox1.Items.Add(FileOpen1.Dialog.Files[i]);
}

//  ShowMessage ('');

end;


