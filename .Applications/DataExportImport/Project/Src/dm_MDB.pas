unit dm_MDB;

interface

{.  $DEFINE test}


uses
  ADODB, Db,  Forms, Classes, SysUtils,

  
  
  u_files,

  

  u_db_mdb

  ;

type
  TdmMDB = class(TDataModule)
    ADOConnection: TADOConnection;
    ADOTable1: TADOTable;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FtempFileName : string;

// TODO: BeginTransaction
//  procedure BeginTransaction;
  public
    TableNameList: TStringList;

    function CreateMDB(aFileName: string): Boolean;

    function OpenMDB(aMDBFileName: string): Boolean;

    function CloseMDB: Integer;
    function TableExists(aTableName: string): Boolean;

    class procedure Init;

    procedure RefreshTableNames;
  end;

var
  dmMDB: TdmMDB;


implementation

uses
  System.IOUtils;
  {$R *.DFM}

const
  TBL_PROJECT     = 'project';


function TdmMDB.CloseMDB: Integer;
begin
  ADOConnection.Close;

  DeleteFile(FtempFileName);
  
  {$IFNDEF test}
//   DeleteFile(FtempFileName);
  {$ENDIF}


end;




//------------------------------------------------------------------------------
class procedure TdmMDB.Init;
//------------------------------------------------------------------------------
begin
  if not Assigned(dmMDB) then
    Application.CreateForm(TdmMDB, dmMDB);
end;


procedure TdmMDB.DataModuleCreate(Sender: TObject);
begin
  Assert(not ADOConnection.Connected);

  TableNameList := TStringList.Create();
end;


procedure TdmMDB.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(TableNameList);
end;


//------------------------------------------------------------------------------
function TdmMDB.CreateMDB(aFileName: string): Boolean;
//------------------------------------------------------------------------------
begin
  Assert(aFileName<>'', 'Value <=0');

  aFileName:=ChangeFileExt(aFileName, '.mdb');

  DeleteFile (aFileName);
  Result := mdb_CreateFile(aFileName);

  if Result then
    Result := mdb_OpenConnection(ADOConnection, aFileName);
end;


//------------------------------------------------------------------------------
function TdmMDB.OpenMDB(aMDBFileName: string): Boolean;
//------------------------------------------------------------------------------
var
  I: Integer;
  oTableFieldNameList: TStringList;
  s: string;
begin
  TableNameList.Clear;

  FtempFileName := ChangeFileExt(aMDBFileName, '.temp.mdb');

  if FileExists(FtempFileName) then
    TFile.Delete(FtempFileName);
  
  TFile.Copy (aMDBFileName, FtempFileName);

  Result := mdb_OpenConnection(ADOConnection, FtempFileName);

  if not Result then
    Exit;


  oTableFieldNameList:=TStringList.Create;


  ADOConnection.GetTableNames(TableNameList);

  for I := 0 to TableNameList.Count - 1 do
  begin
    ADOTable1.TableName := TableNameList[i];
    ADOTable1.Open;

    ADOTable1.GetFieldNames(oTableFieldNameList);

    if (oTableFieldNameList.IndexOf('id')>=0) and
       (oTableFieldNameList.IndexOf('db_id')<0)
    then
    begin
      s:= Format('ALTER TABLE [%s] ADD COLUMN db_id int ', [ADOTable1.TableName]);
      ADOConnection.Execute(s);
    end;

{
    if Eq (ADOTable1.TableName, TBL_ANTENNATYPE) then
    begin
      if (oTableFieldNameList.IndexOf('polarization_str')>=0) then
      begin
        s:= Format('ALTER TABLE [%s] ALTER COLUMN %s varchar(4) ', [ADOTable1.TableName, 'polarization_str']);
        ADOConnection.Execute(s);
      end;

    end;
      
}

{
    if (oTableFieldNameList.IndexOf('project_id')>=0)  then
    begin
      s:= Format('ALTER TABLE [%s] drop COLUMN project_id ', [ADOTable1.TableName]);
      ADOConnection.Execute(s);
    end;

    if (oTableFieldNameList.IndexOf('guid')>=0)  then
    begin
      s:= Format('ALTER TABLE [%s] drop COLUMN [guid] ', [ADOTable1.TableName]);
      ADOConnection.Execute(s);
    end;

}


(*
    if (FConfig_ref.ExceptedFields.IndexOf(sFieldName)>=0) or
     //  Eq(sFieldName, 'project_id') or
       (Copy(sFieldName, 1, 1)= '_')
    then begin
      oSrcField.Tag:=1;

*)

    ADOTable1.Close;
  end;


  FreeAndNil(oTableFieldNameList);

//    TableNameList


 // if TableExists(TBL_PROJECT) then
  //  db_TableOpen1(t_Project1, TBL_PROJECT);

end;


//------------------------------------------------------------------------------
procedure TdmMDB.RefreshTableNames;
//------------------------------------------------------------------------------
begin
  ADOConnection.GetTableNames(TableNameList);
end;


function TdmMDB.TableExists(aTableName: string): Boolean;
begin
  Result := TableNameList.IndexOf(aTableName)>=0;
end;

end.


