object dmConfig: TdmConfig
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 1136
  Top = 327
  Height = 550
  Width = 624
  object ADOConnection: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Onega.mdb;Persis' +
      't Security Info=False'
    LoginPrompt = False
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 44
    Top = 20
  end
  object ADOTable1: TADOTable
    Connection = ADOConnection
    Left = 176
    Top = 24
  end
  object ds_Display_Groups: TDataSource
    DataSet = t_Display_Groups
    Left = 312
    Top = 80
  end
  object t_Display_Groups: TADOTable
    Connection = ADOConnection
    TableName = 'view_Display_Groups_guides'
    Left = 312
    Top = 24
  end
  object view_Tables_new: TADOTable
    Connection = ADOConnection
    TableName = 'view_Tables_new'
    Left = 48
    Top = 168
  end
  object view_Tables_Ref: TADOTable
    Connection = ADOConnection
    TableName = 'view_Tables_Ref'
    Left = 152
    Top = 168
  end
  object view_Fields_replace_active: TADOTable
    Connection = ADOConnection
    TableName = 'view_Fields_replace_active'
    Left = 272
    Top = 168
  end
  object t_Fields_Exception: TADOTable
    Connection = ADOConnection
    TableName = 'Fields_Exception'
    Left = 400
    Top = 168
  end
  object t_Tables_Relation: TADOTable
    Connection = ADOConnection
    TableName = 'Tables_Relation'
    Left = 296
    Top = 352
  end
  object ds_view_Tables_new: TDataSource
    DataSet = view_Tables_new
    Left = 48
    Top = 216
  end
end
