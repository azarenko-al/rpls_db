unit d_Lib_export;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxInplaceContainer, cxTL, cxDBTL, DB, ADODB,

  cxControls, cxTLData, Mask, rxToolEdit,

  dm_Structure_export,


  u_cx,

  //dm_Config_new,

  u_db,


  IniFiles, Grids, DBGrids, Provider, DBClient, ImgList, rxPlacemnt
  ;

type
  Tdlg_Lib_export = class(TForm)
    cxDBTreeList1: TcxDBTreeList;
    ADOConnection: TADOConnection;
    ds_Project: TDataSource;
    ADOStoredProc1: TADOStoredProc;
    cxDBTreeList1ID: TcxDBTreeListColumn;
    cxDBTreeList1is_folder: TcxDBTreeListColumn;
    cxDBTreeList1name: TcxDBTreeListColumn;
    cxDBTreeList1checked: TcxDBTreeListColumn;
    b_Load: TButton;
    b_Save: TButton;
    FilenameEdit1: TFilenameEdit;
    Button1: TButton;
    cxDBTreeList1cxDBTreeListColumn1: TcxDBTreeListColumn;
    Button2: TButton;
    ImageList2: TImageList;
    cxDBTreeList1cxDBTreeListColumn2: TcxDBTreeListColumn;
    ed_FileName_MDB: TFilenameEdit;
    Button3: TButton;
    cxDBTreeList1cxDBTreeListColumn3: TcxDBTreeListColumn;
    b_Select_all: TButton;
    FormStorage1: TFormStorage;
    procedure Button1Click(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure b_LoadClick(Sender: TObject);
    procedure b_SaveClick(Sender: TObject);
    procedure b_Select_allClick(Sender: TObject);
  private
    FDataSet: TDataSet;

    { Private declarations }
  public
    class procedure ExecDlg;
    { Public declarations }
  end;

//
//var
//  dlg_Lib_export: Tdlg_Lib_export;


implementation

uses dm_SQL_to_MDB_new;

{$R *.dfm}


//const
//  def_FILE = 'd:\aaaa.ini';




class procedure Tdlg_Lib_export.ExecDlg;
begin
  with Tdlg_Lib_export.Create(Application) do
  begin
  //  if OpenDbVersions() then
    ShowModal;

    Free;
  end;
end;




procedure Tdlg_Lib_export.Button1Click(Sender: TObject);
var
  iID: Integer;
  sID: string;
//  sObj: string;
  sTable: string;
begin
  TdmSQL_to_MDB_new.Init;

  dmSQL_to_MDB_new.Params.MDBFileName:= ed_FileName_MDB.FileName;


  FDataSet.First;
  FDataSet.DisableControls;


  with FDataSet do
    while not EOF do
    begin

      if FieldByName ('checked').AsBoolean then
        if not FieldByName ('is_folder').AsBoolean then
      begin
       // s:= FieldByName ('uid').AsString;
        iID:= FieldByName ('id').AsInteger;
        sID:= FieldByName ('id').AsString;
     //   sObj:= FieldByName ('objname').AsString;
        sTable:= FieldByName ('Table_Name').AsString;

     //   ini.WriteString('main', s, 'on');

       // ListBox1.Items.Add( sTable + ':' + sID );

      //  dmSQL_to_MDB_new.Records.AddObject(sTable, Pointer(iID));

      end;


      Next;
    end;


  FDataSet.First;
  FDataSet.EnableControls;


 // ListBox1.Items.SaveToFile('d:\2222222.txt');


  dmSQL_to_MDB_new.ExecuteMain;

end;

procedure Tdlg_Lib_export.Button3Click(Sender: TObject);
begin
  TdmStructure_Load_from_MDB.Init;

  dmStructure_Load_from_MDB.Exec(nil, nil, 'link');

end;


procedure Tdlg_Lib_export.FormCreate(Sender: TObject);
begin
//  TdmConfig_new.init;

  FDataSet:=ADOStoredProc1;
end;


procedure Tdlg_Lib_export.b_LoadClick(Sender: TObject);
var
  I: Integer;
  ini: TIniFile;
  oSList: TStringList;
  s: string;
  sFileName: string;
begin
  FDataSet.DisableControls;
  FDataSet.First;

  oSList:=TStringList.Create;

  sFileName:=FilenameEdit1.FileName;   

  ini:=TIniFile.Create(sFileName);

  ini.ReadSection ('main', oSList);


  for I := 0 to oSList.Count - 1 do    // Iterate
  begin
    s:=oSList[i];

    Assert(s<>'');

    if FDataSet.Locate('uid', s, []) then
      db_UpdateRecord(FDataSet, 'checked', True);


  end;    // for



  FreeAndNil(ini);

  FreeAndNil(oSList);

  FDataSet.First;
  FDataSet.EnableControls;

end;


procedure Tdlg_Lib_export.b_SaveClick(Sender: TObject);
var
  ini: TIniFile;
  s: string;
  sFileName: string;
  sID: string;
  sObj: string;
begin
  FDataSet.First;
  FDataSet.DisableControls;


  sFileName:=FilenameEdit1.FileName;


  ini:=TIniFile.Create(sFileName);

  ini.EraseSection('main');


  with FDataSet do
    while not EOF do
    begin

      if FieldByName ('checked').AsBoolean then
     //   if not FieldByName ('is_folder').AsBoolean then
      begin
        s:= FieldByName ('uid').AsString;
         Assert(s<>'');


        sID:= FieldByName ('id').AsString;
        sObj:= FieldByName ('objname').AsString;




        ini.WriteString('main', s, 'on');

      //  ListBox1.Items.Add( sObj + ':' + sID );

      end;


      Next;
    end;


  FreeAndNil(ini);

  FDataSet.First;
  FDataSet.EnableControls;;


end;




procedure Tdlg_Lib_export.b_Select_allClick(Sender: TObject);
begin
  db_UpdateAllRecords_(FDataSet, 'checked', True);


end;



end.
