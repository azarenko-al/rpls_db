program Data_Export_Import;



uses
  dm_Config in 'Src\dm_Config.pas' {dmConfig: TDataModule},
  dm_MDB in 'Src\dm_MDB.pas' {dmMDB: TDataModule},
  dm_MDB_to_SQL in 'Src\dm_MDB_to_SQL.pas' {dmMDB_to_SQL: TDataModule},
  dm_MDB_validate in 'Src\dm_MDB_validate.pas' {dmMDB_validate: TDataModule},
  dm_SQL_LIB_to_MDB in 'Src\dm_SQL_LIB_to_MDB.pas' {dmSQL_LIB_to_MDB: TDataModule},
  Forms,
  u_classes_tree_new in 'Src\u_classes_tree_new.pas',
  u_Config_classes in 'Src\u_Config_classes.pas',
  u_export_classes in 'Src\u_export_classes.pas',
  u_FK in 'Src\u_FK.pas',
  u_ini_data_export_import_params in 'src shared\u_ini_data_export_import_params.pas',
  u_db in '..\..\..\common7\Package_Common\u_db.pas',
  u_lists in 'Src\u_lists.pas',
  d_Select in 'Src\d_Select.pas' {dlg_Select},
  f_Main in 'Src\f_Main.pas' {frm_Main_1},
  dm_App in 'Src\dm_App.pas' {dmApp: TDataModule},
  dm_SQL_to_MDB_new in 'Src\dm_SQL_to_MDB_new.pas' {dmSQL_to_MDB_new: TDataModule},
  u_const_db in '..\..\EXCEL_to_DB_new\Project\Shared\u_const_db.pas',
  dm_Main_Data in '..\..\EXCEL_to_DB_new\Project\src\dm_Main_Data.pas' {dmMain_Data: TDataModule},
  u_vars in '..\..\u_vars.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TdmApp, dmApp);
  Application.Run;
end.
