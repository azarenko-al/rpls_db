unit dm_Test_Export_Import;

interface

uses
  SysUtils, Classes, Forms, ADODB, DB,

  dm_SQL_to_MDB_new,

  dm_Main,

  u_ini_data_export_import_params,


  u_db
  ;

type
  TdmTest_Export_Import = class(TDataModule)
    qry_Source: TADOQuery;
    tb_Projects: TADOTable;
    procedure DataModuleCreate(Sender: TObject);
  private
   
    { Private declarations }
  public
    procedure Exec;
    class procedure Init;
    { Public declarations }
  end;

var
  dmTest_Export_Import: TdmTest_Export_Import;

implementation
{$R *.dfm}



class procedure TdmTest_Export_Import.Init;
begin
  if not Assigned(dmTest_Export_Import) then
    dmTest_Export_Import := TdmTest_Export_Import.Create(Application);

end;


procedure TdmTest_Export_Import.DataModuleCreate(Sender: TObject);
begin
  db_SetComponentsADOConn([tb_Projects], dmMain.ADOConnection);

  db_TableOpen1(tb_Projects, 'Projects');

  TdmSQL_to_MDB_new.Init;

end;

//-----------------------------------------------------------
procedure TdmTest_Export_Import.Exec;
//-----------------------------------------------------------
var
  I: Integer;
  iProjectID: Integer;

begin
 // db_PostDataset(cxGrid1DBTableView2.DataController.DataSource.DataSet);
//  iProject_ID:=AsInteger(DBLookupComboBox1.KeyValue);
 // dmSQL_to_MDB.Params.Src_ADOConnection := dmMain.ADOConnection;
 // dmSQL_to_MDB.Params.Dest_ADOConnection:= dmMDB.ADOConnection;
 // dmSQL_to_MDB.OnLog :=Log;



  g_Ini_Data_export_import_params.Mode:=mtExport_GUIDES;

  dmSQL_to_MDB_new.params.IsTestMode := True;

  dmSQL_to_MDB_new.params.MDBFileName:= 'd:\Tests\guides.mdb';
 // dmSQL_to_MDB_new.params.Project_ID:=iProjectID;
  dmSQL_to_MDB_new.ExecuteDlg;


  



//  db_SetComponentsADOConn([tb_Dest], dmMDB.ADOConnection);

  with tb_Projects do
    while not EOF do
    begin
      iProjectID := FieldByName(FLD_Project_id).AsInteger;
                


//  iProjectID := AsInteger(DBLookupComboBox1_Project.KeyValue);

 // db_PostDataset(dmConfig.ds_Display_Groups.DataSet);

//
//
//  if FParams_ref.TableName<>'' then
//  begin
//    dmSQL_to_MDB_new.params.TableName:=FParams_ref.TableName;
//
//   // dmSQL_to_MDB_new.params.FilterRecords_List.Clear;
//
//    dmSQL_to_MDB_new.Params.IDS:=FParams_ref.SelectedIDList.MakeDelimitedText_ID;
//
//
////    for I := 0 to FParams_ref.SelectedIDList.Count - 1 do
////      dmSQL_to_MDB_new.FilterRecords_List.Add(IntToStr(FParams_ref.SelectedIDList[i].id));
//                  S
//
//  end;


   
    g_Ini_Data_export_import_params.Mode:=mtExport_PROJECT;

    dmSQL_to_MDB_new.params.IsTestMode := True;

    dmSQL_to_MDB_new.params.MDBFileName:= Format('d:\Tests\%d.mdb', [iProjectID]);
    dmSQL_to_MDB_new.params.Project_ID:=iProjectID;
    dmSQL_to_MDB_new.ExecuteDlg;

  //  MsgDlg('Выполнено');


//    Break;

    Next;
  end;

  FreeAndNil(dmSQL_to_MDB_new);

end;

end.
