unit Unit11;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,

   u_classes_tree_new,

  dm_Config,
  u_Config_classes,

  dm_MDB,

  d_Progress,

  dm_Main,

  u_log,
  u_func,
  u_files,

  u_db,

  u_const_db,

  Dialogs, DB, ADODB;

type
  TForm11 = class(TForm)
    qry_Source: TADOQuery;
    tb_Dest: TADOTable;
    ADOConnection_MDB1: TADOConnection;
    q_Ref_keys: TADOQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form11: TForm11;

implementation

{$R *.dfm}

end.



// -------------------------------------------------------------------
procedure TdmSQL_to_MDB.CopyTable(aTableInfo: TTableInfo; aIsRefTable: boolean
    = false);
// -------------------------------------------------------------------
var
  I: Integer;
  s: string;
  sTableName: string;
begin
  s := Format('SELECT * FROM %s', [aTableInfo.TableName]);

  if aTableInfo.UseProjectID then
  begin
    Assert(Params.Project_ID>0, 'Value <=0');

    s :=s+ Format(' WHERE (project_id=%d)', [Params.Project_ID]);
  end;

  db_OpenQuery(qry_Source, s);

  if qry_Source.RecordCount=0 then
    Exit;

//  if FTableNameList.IndexOf(aTableInfo.TableName)<0 then
    Create_table_from_dataset(qry_Source, aTableInfo.TableName, aTableInfo.KeyFieldName);

  db_TableOpen1(tb_Dest, aTableInfo.TableName);


  CopyDatasets (aTableInfo, aIsRefTable);


  qry_Source.Close;
  tb_Dest.Close;

  Log(FTable_ID_DB_ID_List.Text);

  FTable_ID_DB_ID_List.SaveToFile(FLogFileName);

  // ---------------------------------------------------------------

  // add ref records
  for I := 0 to aTableInfo.RefTables.Count - 1 do
  begin
    sTableName:=aTableInfo.RefTables[i].TableInfoRef.TableName;

    if TableNameRecordsExist(sTableName) then
      CopyTable(aTableInfo.RefTables[i].TableInfoRef, True)
    else
      LogDebug('No records: '+ sTableName);
  end;
end;
