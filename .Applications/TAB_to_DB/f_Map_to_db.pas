unit f_Map_to_db;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.Mask,
  RxToolEdit,

  IOUtils,
  

  System.Generics.Collections,

  u_db,
  

  u_MapX,
  u_MapX_lib, 
  
  MapXLib_TLB,

  u_assert,
  

  u_func,
  u_files,
  u_Geo, 

  Data.DB, Data.Win.ADODB, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Comp.Client, Xml.xmldom,
  Xml.XMLIntf, Xml.Win.msxmldom, Xml.XMLDoc, RxPlacemnt
  

  ;

type
 

  TForm42 = class(TForm)
    FilenameEdit1: TFilenameEdit;
    ProgressBar1: TProgressBar;
    Button1: TButton;
    ADOConnection1: TADOConnection;
    ADOQuery1: TADOQuery;
    ListBox1: TListBox;
    DirectoryEdit1: TDirectoryEdit;
    Button2: TButton;
    Button4: TButton;
    FormStorage1: TFormStorage;
//    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    procedure LoadFromFile(aFileName: string);
//    FDictionary: TDictionary<String, TRecordData>;

  public
    { Public declarations }
  end;

var
  Form42: TForm42;

implementation

{$R *.dfm}



procedure TForm42.Button1Click(Sender: TObject);
begin
  LoadFromFile  (FilenameEdit1.FileName);
end;


procedure TForm42.LoadFromFile(aFileName: string);
var
  bl: TBLPoint;
  blRect: TblRect;
  I: Integer;
  iCode: Integer;
 //
  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vSelection: CMapXSelection;
  vRowValues: CMapXRowValues;
  vPt: CMapXPoint;
  iCount: integer;
  iLen: Integer;
   j: Integer;
  k: Integer;

  rObjectRec: TmiObjectRec;
  sName: string;
  sType: string;
  v: Variant;

   FmiFile: TmiMap;
  s: string;
  sCode: string;
  sCountry: string;
  sSQL: string;
  sTableName: string;
  s_back: string;

  oSList_names : TStringLIst;
  oSList_values: TStringLIst;
  
begin
  oSList_names :=TStringLIst.Create;
  oSList_values:=TStringLIst.Create;


  FmiFile:=TmiMap.Create;
  

//   sSQL:='delete from  Security.GeoRegion ';
//   ADOConnection1.Execute(sSQL);
  
 

  if FmiFile.OpenFile (aFileName) then
  begin
    sTableName:= ExtractFileName(aFileName);
    sTableName:= 'geo.'+ChangeFileExt(sTableName,'');

  
    vFeatures := FmiFile.VLayer.AllFeatures;

    
   // FmiFile.VLayer.Search()

    
    ProgressBar1.Max:=vFeatures.Count;
    
    s:='';
    for I := 0 to High(FmiFile.FieldArr) do
    begin
      if LowerCase(FmiFile.FieldArr[i].Name)='id' then
        Continue;
    
      oSList_names.Add (FmiFile.FieldArr[i].Name);
      oSList_values.Add (':'+ FmiFile.FieldArr[i].Name);
    
      s:=IIF(s<>'',s + ',','') +  FmiFile.FieldArr[i].Name + ' ' ;
    
      case FmiFile.FieldArr[i].Type_ of

          miTypeString : s:=s + 'varchar(200)';
     
          miTypeNumeric,
          miTypeFloat : s:=s + 'float';
                                  
          miTypeInt ,
          miTypeSmallInt : s:=s + 'int';
       else
         raise Exception.Create('Error Message');
        
      end;
    end;   

    
    sSQL:='if object_id('''+ sTableName +''') is not null drop table '+ sTableName;
    ADOConnection1.Execute(sSQL);

    sSQL:='create table '+ sTableName +' (id int identity, '+s +', [geom_xml] xml, [geom] geography )';
    
//    sSQL:='create table ['+ sTableName +'] (id int identity, '+s +', [geom_xml] xml, [geom] geography)';
//    sSQL:='create table ['+ sTableName +'] (id int identity, '+s +', [geo] geometry)';
    ADOConnection1.Execute(sSQL);
    
//    --set geom = geo.fn_XML_to_geography (geom_xml)

    s:=oSList_names.DelimitedText;


    ADOQuery1.sql.Text:= Format('insert into %s (%s, [geom_xml]) values (%s, :xml)',
//     ADOQuery1.sql.Text:= Format('insert into %s (%s, [geom]) values (%s, geo.fn_XML_to_geography (:xml))',
          [sTableName, oSList_names.DelimitedText, oSList_values.DelimitedText]);

//    ADOQuery1.Prepared:=true;
    
    ADOQuery1.Parameters.Count;
    
    
//    FmiFile.

//////////////////////////////////////////////////

//    for I := 1 to 5  do
    for I := 1 to vFeatures.Count  do
    begin
      ProgressBar1.Position:=i;
    
      vFeature := vFeatures[i];


      assert (vFeature.type_ = miFeatureTypeRegion);


       vRowValues:=FmiFile.VDataset.RowValues[vFeature];
       
       FmiFile.ExtractObjectRec(vFeature, rObjectRec);


       s:=mapx_Feature_Points_to_XML (vFeature);
 
       ADOQuery1.Parameters.FindParam('xml').Value:=s;

 
       for j := 0 to High(FmiFile.FieldArr) do
        begin
          s:=FmiFile.FieldArr[j].Name;

          if LowerCase(s)='id' then
            Continue;   
          
          Assert (Assigned (ADOQuery1.Parameters.FindParam(s)));
              
          ADOQuery1.Parameters.FindParam(s).Value:=
                     mapx_RowValues_GetItemValue(vRowValues, s);
     
        end;   

       ADOQuery1.ExecSQL;
  
      
      
     end; 
    

    sSQL:='update '+ sTableName +' set geom = geo.fn_XML_to_geography (geom_xml) ';
//    sSQL:='update ['+ sTableName +'] set geom = geo.fn_XML_to_geometry (geom_xml) ';
    ADOConnection1.Execute(sSQL); 

  end;  
    
 FmiFile.Free;    


end;

procedure TForm42.Button2Click(Sender: TObject);
begin
  ListBox1.Items.Clear;
  
  ScanDir(  DirectoryEdit1.Text, '*.tab' , ListBox1.Items);

end;

procedure TForm42.Button4Click(Sender: TObject);
var
  i: Integer;
begin
//  for i:=0 to ListBox1.Items.Count-1 do

  for i:=0 to 0 do
    LoadFromFile  (ListBox1.Items[i]);

end;

end.


