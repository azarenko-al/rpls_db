unit u_BinToMap;

interface

  {.$DEFINE test}

uses SysUtils,graphics, Classes, Forms, Dialogs,   ShellApi, Windows,
   System.Generics.Collections,

//  u_dll_gdal,
  x_GDAL,

  u_run,

  u_KMZ_simple,

  u_BMP_file,

  u_geo_convert_new1,

  u_ini_Params_Bin_To_Map_classes,

  u_ini_Params_bin_to_map,


 // u_Bmp_to_TIFF,

  u_files,
  u_func,

  u_GEO,
       
 // u_dll_img,

  u_img_PNG,

  u_log,
  u_progress,

  u_Matrix,
  u_Matrix_base,

  u_MatrixDecomposer
  ;

type
  //------------------------------------------------------
  // ������������ ����� DM �� 2D ��������� �������
  //------------------------------------------------------
  TBinToMap = class (TProgress)
  //------------------------------------------------------
  private
    FCashedLastValue: double;
    FCashedLastColor: integer;
    FCashedLastRangeIndex: integer;

    FUseCashedColor: boolean;

    FColorsDict :  TDictionary <integer, integer>;

  protected
    FMapType: (mtKgr, mtKup, mtOther);

    FMatrixDecomposer : TMatrixDecomposer; // ������� � �������
    FColorMatrix      : TIntMatrix;

//    FRangeMatrix_test  : TIntMatrix;

    FValuesMatrix     : TOrdinalMatrix;

  private
    FMapFileName   : string;


    procedure DeleteColorMatrix();
    function  PrepareColorMatrix (aMatrix: TOrdinalMatrix): boolean;

    function ExecuteBinMatrix(aMatrixFile: TBinMatrixFile): Boolean;
    function GetColorByValue(aValue: Integer; var aRangeIndex: integer): Integer;

    procedure Test_GDAL;

//    procedure Test_GDAL;
  protected
    FMatrixFileName: string;

    FColorSchemaID : integer;
    FCaption       : string;

    procedure SaveColorMatrixToVectorFile (aMatrixFileName,aMapFileName: string);  virtual; abstract;
    function  SaveColorMatrixToRasterFile (aMapFileName: string): boolean;  virtual; abstract;

    function SaveMatrixToBitmapFile(aFileName: string): boolean;

    function  SaveMatrixToRasterFile_MIF (aFileName: string): boolean; virtual; abstract;


    procedure DoOnMapNewBLShape (Sender: TObject;
//                                 var aBLPoints: TBLPointArrayF;
                                 aBLPoints: TBLPointArray;
                                 aFillValue,
                                 aRow,aCol: integer); virtual; abstract;

  protected
    procedure ExecuteProc (); override;
  public

    constructor Create;
    destructor Destroy; override;

    function SaveMatrixToBitmapFile_values(aFileName: string): boolean;

//    function SaveMatrixToBitmapFile_old(aFileName: string): boolean;
  end;


const
 //  DEF_RASTER_EXT = '.gif';
   //DEF_RASTER_EXT = '.jpg';
//   DEF_RASTER_EXT = '.tif';
//   DEF_RASTER_EXT = '.png';
   DEF_RASTER_EXT = '.bmp';
//   DEF_RASTER_EXT = '.png';

//======================================================
//======================================================
implementation

const
//  BLANK_COLOR = clBlack;
  BLANK_COLOR_ = clWhite;


// ---------------------------------------------------------------
constructor TBinToMap.Create;
// ---------------------------------------------------------------
begin
  inherited;


  FColorsDict:=TDictionary <integer, integer>.Create;

  FColorMatrix:=TIntMatrix.Create;

  FMatrixDecomposer:=TMatrixDecomposer.Create;
  FMatrixDecomposer.OnNewBLShape:=DoOnMapNewBLShape;
 // FMatrixDecomposer.OnNewXYShape:=DoOnMapNewXYShape;

  FMatrixDecomposer.Progress2Step:=50;
end;

// ---------------------------------------------------------------
destructor TBinToMap.Destroy;
// ---------------------------------------------------------------
begin
  FreeAndNil(FColorMatrix);
  FreeAndNil(FMatrixDecomposer);

  FreeAndNil(FColorsDict);

//  FreeAndNil(FRangeMatrix_test);

  inherited;
end;


// ---------------------------------------------------------------
function GetTransparentColor: TColor;
// ---------------------------------------------------------------
type
  TColorEx = record
               case b: boolean of    //
                true:  (color: integer);
                false: (RGB32 : packed record
                            B, G, R, A: Byte;
                          end;)
              end;    // case

var
  rec: TColorEx;
begin
  FillChar(rec, SizeOf(rec), 0);
  rec.RGB32.A:=1;

  result:=rec.color;

end;


//------------------------------------------------------
function TBinToMap.SaveMatrixToBitmapFile(aFileName: string): boolean;
//------------------------------------------------------
var
  r,c: integer;
  iColor,iRange: Integer;
  iValue : Integer;
 // iBitCount: byte;
  s,sName : string;
  btValue: Byte;
//  iValue: Integer;

  oBmp: TBmpFile;

  iBLANK_COLOR: TColor;

 // oFileStream: TFileStream;
//  oMemStream: TMemoryStream;

  bt: byte;
  w: word;
begin

  DoLog ('SaveMatrixToBitmapFile');

  Result:= False;



  iBLANK_COLOR:=GetTransparentColor();

 // iBitCount:=IIF(aUseValues, 8, 24);


//ShowMessage ('function TBinToMap.SaveMatrixToBitmapFile(aFileName: string; aCorrCoeff:  '+ aFileName);

//////////////////
//  FValuesMatrix.SaveToFile_Envi(aFileName);

////  FValuesMatrix.SaveToFile_Envi(ChangeFileExt(aFileName, '.envi'));


  FValuesMatrix.SaveToFile(ChangeFileExt(aFileName, '.envi'));


  oBmp:=TBmpFile.Create_Width_Height(aFileName,
                      FValuesMatrix.ColCount, FValuesMatrix.RowCount );


//ShowMessage ('1');

  Progress2Step:=100;


    with FValuesMatrix do
      for r:=0 to RowCount-1 do
      begin
        for c:=0 to ColCount-1 do
  //        if (not IsNull(r,c)) then
        begin

  (*        if (IsNull(r,c)) then
          begin
            oBmp.Write(0);
            Continue;
          end;
  *)

          if Terminated then
          begin
            Result:= false;
            exit;
          end;

  //        if RecordType = dtFloat then
  //          iValue:= ItemsAsFloat[r,c]
  //        else
       //     iValue:= ;

          iValue:=Items[r,c]; // + aCorrCoeff;

         {
          if aUseValues and (RecordType = dtByte) then
          begin
            iValue:=ABS(iValue);

            Assert(iValue>=0,   FloatToStr(iValue));
            Assert(iValue<=255, FloatToStr(iValue));

            btValue:= Round(iValue);
            oBmp.Write_Byte(btValue);

          end else  }
        //  begin
            //----------------
    //        iColor:= g_Params_bin_to_map.GetColor (iValue, iRange);
            iColor:= GetColorByValue (iValue, iRange);


            s:=FMatrixFileName;

//            FValuesMatrix.

      //      assert (iRange <> 3);


            if iColor < 0  then
              iValue:= iBLANK_COLOR;

            if iValue=0 then
              iColor:= iBLANK_COLOR;

            oBmp.Write(iColor);
            //----------------
        //  end;



        end;


        oBmp.Writeln();

        DoProgress2(r, RowCount);
      end;


    FreeAndNil(oBmp);



    Result:=true;



end;


//------------------------------------------------------
function TBinToMap.SaveMatrixToBitmapFile_values(aFileName: string): boolean;
//------------------------------------------------------
var
  r,c: integer;
  iColor,iRange: Integer;
  dValue : Double;
  iBitCount: byte;
  s,sName : string;
  btValue: Byte;
  iValue: Integer;
  k: Integer;

  oBmp: Graphics.TBitmap;
//  oFileStream: TFileStream;

  Row : pByteArray;
begin

 // DoLog ('SaveMatrixToBitmapFile');

  Result:= False;


//  oFileStream:=TFileStream.create(ChangeFileExt(aFileName, '.envi'), fmCreate);
//  oMemStream:=TMemoryStream.create;



 // iBitCount:=IIF(aUseValues, 8, 24);


  oBmp:=Graphics.TBitmap.Create;
  oBmp.PixelFormat  :=pf8bit;
//  oBmp.Monochrome := True;
  oBmp.Width := FValuesMatrix.ColCount;
  oBmp.Height:= FValuesMatrix.RowCount;

  //  e_Width_Height_(aFileName,
   //                   FValuesMatrix.ColCount, FValuesMatrix.RowCount, iBitCount);


  Progress2Step:=100;


    with FValuesMatrix do
      for r:=0 to RowCount-1 do begin
      for c:=0 to ColCount-1 do
//        if (not IsNull(r,c)) then
      begin

(*        if (IsNull(r,c)) then
        begin
          oBmp.Write(0);
          Continue;
        end;
*)

        if Terminated then
        begin
          Result:= false;
          exit;
        end;

       // if RecordType = dtFloat then
       //   dValue:= ItemsAsFloat[r,c]
      //  else

          dValue:= Items[r,c];

     //     dValue:= 55;


        if (RecordType = dtByte) then
        begin
          dValue:=ABS(dValue);

          Assert(dValue>=0,   FloatToStr(dValue));
          Assert(dValue<=255, FloatToStr(dValue));

          btValue:= Round(dValue) mod 255;
//          btValue:= Round(dValue + aCorrCoeff) mod 255;

       //   btValue:=btValue + aCorrCoeff;


        //  oBmp.Canvas.Pixels[r,c]:=btValue;


//          oBmp.Pa


          Row := oBmp.Scanline[r];
      //    k:=Row[c];

          Row[c]:=btValue;

//          Assert (oBmp.Canvas.Pixels[r,c] = btValue);

         // oBmp.Write_Byte(btValue);

        end


      end;

     // oBmp.Writeln();

      DoProgress2(r, RowCount);
    end;



    oBmp.SaveToFile(aFileName);



   FreeAndNil(oBmp);

//   FreeAndNil(oFileStream);


    Result:=true;

  //img_SaveBitmapToFile_file (aFileName, aFileName);

 //////////



    if not (g_Params_bin_to_Map.MapExportType in [mtGeoTiff, mtGeoTiff_values]) then
    begin
      img_BMP_to_PNG(aFileName, ChangeFileExt(aFileName, '.png'));

    end;

  //: String; aIsTransparent:
  //    boolean = false; aTransparentColor: TColor = 0);



end;



//------------------------------------------------------
procedure TBinToMap.DeleteColorMatrix();
//------------------------------------------------------
var
  sName1,  sName: string;
begin
  exit;

{$IFNDEF debug}


  sName := ChangeFileExt(FMapFileName,'_color_matrix');
 // sName2:= sName+'_mirror';
  sName1:= sName+'.bin';
 // sName3:= sName2+'.bin';

  SysUtils.DeleteFile(sName);
  SysUtils.DeleteFile(sName1);
  //SysUtils.DeleteFile(sName2);
 // SysUtils.DeleteFile(sName3);

{$ENDIF}

end;


//---------------------------------------------------------
function TBinToMap.GetColorByValue(aValue: Integer; var aRangeIndex: integer):
    Integer;
//---------------------------------------------------------
var i: integer;
begin
  Result:=-1;
  aRangeIndex:=-1;

  if FUseCashedColor then
    if aValue=FCashedLastValue then
    begin
      result      := FCashedLastColor;
      aRangeIndex := FCashedLastRangeIndex;
      exit;
    end;


  case g_Params_bin_to_Map.ColorSourceType of
    csCells:
       Result:=g_Params_bin_to_Map.Cells.GetColorByID(Round( aValue), aRangeIndex);

    csRanges:
      begin
        Assert(Assigned(g_Params_bin_to_Map.ColorSchema_Selected), 'Value not assigned');

        Result:=g_Params_bin_to_Map.ColorSchema_Selected.GetColor(aValue, aRangeIndex);
      end;   
  end;


  FUseCashedColor:=True;
  FCashedLastValue:=aValue;
  FCashedLastColor:=Result;
  FCashedLastRangeIndex :=aRangeIndex;

end;


//------------------------------------------------------
function TBinToMap.PrepareColorMatrix (aMatrix: TOrdinalMatrix): boolean;
//------------------------------------------------------
var r,c: integer;
  iRange: integer;
  i: Integer;
    iValue : integer;
    sFileName : string;

  iColor: integer;

  rec: TMatrixParamsRec;
  s: string;
begin
 // ShowMessage ('PrepareColorMatrix');

  Assert(aMatrix.ZoneNum>0);

  DoLog ('Preparing ColorMatrix ...');

  Result:= false;

  //��������� ������� ������
  sFileName := ChangeFileExt(FMapFileName,'_color_matrix.tmp');

{  CreateIntMatrix(sFileName,
                  aMatrix.TopLeft.X, aMatrix.TopLeft.Y,
                  aMatrix.RowCount, aMatrix.ColCount,
                  aMatrix.Step, aMatrix.ZoneNum);
}

  FillChar(rec,SizeOf(rec),0);

  rec.StartX     :=aMatrix.TopLeft.X;
  rec.StartY     :=aMatrix.TopLeft.Y;
  rec.RowCount   :=aMatrix.RowCount;
  rec.ColCount   :=aMatrix.ColCount;
  rec.Step       :=aMatrix.Step;
  rec.Zone6      :=aMatrix.ZoneNum;



  FColorMatrix.FileName:=sFileName;
  FColorMatrix.SetParams (rec,
                          aMatrix.TopLeft.X, aMatrix.TopLeft.Y,
                          aMatrix.RowCount, aMatrix.ColCount,
                          aMatrix.Step, aMatrix.ZoneNum);


{  FRangeMatrix_test.SetParams (rec,
                          aMatrix.TopLeft.X, aMatrix.TopLeft.Y,
                          aMatrix.RowCount, aMatrix.ColCount,
                          aMatrix.Step, aMatrix.ZoneNum);

}

  //FRangeMatrix.SetMatrixSize(aMatrix.RowCount, aMatrix.ColCount);

//  FColorMatrix.LoadFromFile (sFileName, fmOpenReadWrite);
//  FColorMatrix.LoadFromFile (sFileName, fmOpenRead);

  FColorMatrix.FillBlank;
//  FRangeMatrix_test.FillBlank;

  Progress2Step:=100;

  with aMatrix do
    for r:=0 to RowCount-1 do
    begin
      for c:=0 to ColCount-1 do
        if (not IsNull(r,c)) then
      begin
        if Terminated then begin
          Result:= false;
          exit;
        end;

//        if aMatrix.RecordType = dtFloat then
//          iValue:= aMatrix.ItemsAsFloat[r,c]
//        else
          iValue:= Items[r,c];

//        iColor:= g_Params_bin_to_Map.GetColor (iValue,iRange);
        iColor:= GetColorByValue (iValue,iRange);

        s:=FMatrixFileName;

      //  assert (iRange<3);

        FColorMatrix[r,c]:= iColor;

//        FRangeMatrix_test[r,c]:= iRange;

     //   FRangeMatrix[r,c]:= iRange;

        i:=FColorMatrix[r,c];
      end;
//      if c=0 then
      DoProgress2(r, RowCount);
    end;


 // ShowMessage ('PrepareColorMatrix 1');

  FColorMatrix.SaveToFile (sFileName);

  FColorMatrix.SaveToTxtFile (ChangeFileExt(sFileName, '_colors.txt'));

// ShowMessage ('PrepareColorMatrix 2');

//  FRangeMatrix_test.SaveToTxtFile ( sFileName + '_ranges.txt');

 // ShowMessage ('PrepareColorMatrix 3');

//  FColorMatrix.SaveToTxtFile (sFileName);

  Result:= true;
end;


//------------------------------------------------------
procedure TBinToMap.Test_GDAL;
//------------------------------------------------------
var
//  sParam: string;
  rec: TGDAL_XYRect;
  sBitmapFile: Shortstring;
  xyRect_42: TXYRect;
begin

  xyRect_42:= FValuesMatrix.GetXYBounds;

  rec.Lat_max:=xyRect_42.TopLeft.X;
  rec.Lat_min:=xyRect_42.BottomRight.X;

  rec.Lon_min:=xyRect_42.TopLeft.y;
  rec.Lon_max:=xyRect_42.BottomRight.y;

  rec.Zone:=FValuesMatrix.ZoneNum;

  sBitmapFile:=ChangeFileExt(FMapFileName, '.bmp');

  rec.BmpFileName:=sBitmapFile;
  rec.TabFileName:=ChangeFileExt(FMapFileName, '.tab');

  
  Assert (rec.Zone<100);

  Assert (FileExists (sBitmapFile));

  TGDAL_XYRect_SaveToFile (sBitmapFile+ '.ini', rec);

  gdal_BMP_to_TAB (sBitmapFile, rec);

end;


//------------------------------------------------------
function TBinToMap.ExecuteBinMatrix(aMatrixFile: TBinMatrixFile): Boolean;
//------------------------------------------------------
var // oMatrix : TOrdinalMatrix;
  sImgFileName: string;
  blBounds: TBLRect;
//  bUseValues: Boolean;
  iCode: Integer;
  oKMZ: TKMZ_simple;
  s: string;
  sBatFile: string;
  sBitmapFile: string;
  sExt: string;
  sFile: string;
  sParam: string;
  sPath: string;
  sPath_bin: string;
  sPath_epsg_csv: string;
  sPath_EXE: string;
//  sTempFile_Bmp: string;
//  sTempFile_tif: string;
  sTiff: string;

  xyRect_42: TXYRect;
  xyRect_UTM: TXYRect;
   // bool: boolean;

begin
{  FMatrixFileName:='f:\_los\los.int';
  FMapFileName:='f:\_los\los.tab';
}
  //oMatrix:=matrix_CreateMatrixByFileName (FMatrixFileName);

//ShowMessage ('1.1  ' + FMatrixFileName);

  FValuesMatrix:=Matrix_CreateMatrixByFileName (FMatrixFileName);



//  FValuesMatrix.SaveToTxtFile(FMatrixFileName);


//ShowMessage ('2');


  if not Assigned(FValuesMatrix) then
  begin
//    raise Exception.Create('�� ������� �������' + FMatrixFileName );
  //  g_Log.AddError('TdmMain.DoProcessException', E.Message);
    g_Log.AddError('TBinToMap.ExecuteBinMatrix', '�� ������� �������' + FMatrixFileName);

    result := False;
    exit;
//    ExitProcess(1);
  end;

    //Exit;

  DoLog ('');
  DoLog ('������ ����� :' + FMatrixFileName);

  if not FValuesMatrix.LoadFromFile (FMatrixFileName, fmOpenReadWrite) then
  begin
    FreeAndNil(FValuesMatrix);
    DoLog ('���� �� ����������: ' + FMatrixFileName);
    Exit;
  end;


//  FValuesMatrix.SaveToFile_Envi( FMatrixFileName + '.envi' );


//  FValuesMatrix.SaveToTxtFile(FMatrixFileName);



//ShowMessage ('3');

  //!!!!!!!!!!!!!!!!!!!!!!!!
 // g_Params_bin_to_Map.MapType := mtVector;


 {$IFDEF Test_GDAL}
 ///  FValuesMatrix.SaveToTxtFile (FMatrixFileName);

 {$ENDIF}




//  else begin
    DoLog (Format('�������: %s;  ����� : %s',
           [FMatrixFileName, FMapFileName]));

    sExt:=LowerCase(ExtractFileExt (FMatrixFileName));

    if Pos('kgr', FMatrixFileName)>0 then FMapType:= mtKgr else
    if Pos('kup', FMatrixFileName)>0 then FMapType:= mtKup
                                     else FMapType:= mtOther;



    if (sExt='.kgr')  then //and (not dmParams_bin_to_MIF.FIsStatistic)
    begin
      Assert( g_Params_bin_to_Map.Cells.Count>0);

      g_Params_bin_to_Map.ColorSourceType:=csCells;

   //   dmParams.PrepareCellsColorRanges();
    //  dmParams.SaveCellsSchemaXmlFile(FMatrixFileName, FMapFileName);
    end
    else begin
      g_Params_bin_to_Map.ColorSourceType:=csRanges;
      g_Params_bin_to_Map.SetColorSchema (FColorSchemaID);


//      dmParams_bin_to_MIF.SaveColorSchemaXmlFile (FColorSchemaID, FMatrixFileName, FMapFileName);
    //  dmParams.SaveColorSchemaToIni (ChangeFileExt(FMapFileName, '.ini'), FMatrixFileName);

    end;


//    if aMatrixFile.ColorSchema_FileName<>'' then
//      g_Params_bin_to_Map.ColorSchema_Selected := aMatrixFile.ColorSchema;


    // ---------------------------------------------------------------
    if g_Params_bin_to_Map.MapExportType = mtRaster then
    // ---------------------------------------------------------------
    begin
      SaveMatrixToRasterFile_MIF (FMapFileName);
      test_GDAL;

    end else
    begin

//    FMatrix:= oMatrix;

       if g_Params_bin_to_Map.MapExportType<>mtGeoTiff_values then
         PrepareColorMatrix (FValuesMatrix);




      case g_Params_bin_to_Map.MapExportType of
        // -----------------------------
        mtKMZ:
        // -----------------------------
          begin
            sBitmapFile:=ChangeFileExt(FMapFileName, '.bmp');

            SaveMatrixToBitmapFile (sBitmapFile);


//            SaveColorMatrixToRasterFile (sBitmapFile);

            oKMZ:=TKMZ_simple.Create;

//                FColorMatrix      : TIntMatrix;
  //  FValuesMatrix     : TOrdinalMatrix;
          xyRect_42:= FValuesMatrix.GetXYBounds;


           oKMZ.CreateFileByXYRect('Image',
                    ChangeFileExt(FMapFileName, '.kmz'),
                    sBitmapFile, //ChangeFileExt(FMapFileName, '.bmp'),
                    255 - g_Params_bin_to_Map.Transparence,
//                    255 - g_Params_bin_to_Map.Transparence_0_255,

                    xyRect_42,
                    FValuesMatrix.ZoneNum
                    );

            FreeAndNil(oKMZ);

          end;
            //   ;//SaveColorMatrixToRasterFile (FMapFileName); // raster map

        mtRaster: begin
                //    ShowMEssage ('SaveColorMatrixToRasterFile....');

                    SaveColorMatrixToRasterFile (FMapFileName); // raster map
                //    ShowMEssage ('SaveColorMatrixToRasterFile');

             //       test_GDAL;
                  end;

        mtKML,
        mtMIF,
        mtVector: begin
                 //   sImgFileName:= ChangeFileExt(FMapFileName, '_raster'+DEF_RASTER_EXT);
                  //Test_GDAL !!!!!!!!!!
                 //   SaveColorMatrixToRasterFile (sImgFileName); // raster map

                 //   ShowMEssage ('SaveColorMatrixToVectorFile......');
                    SaveColorMatrixToVectorFile (FMatrixFileName, FMapFileName); // vector map
                 //   ShowMEssage ('SaveColorMatrixToVectorFile');

                  end;

        mtGeoTiff,
        mtGeoTiff_values:
           begin
             //ShellExecute (application.handle, 'open', 'cmd', PChar(sCmd), nil, SW_MAXIMIZE)

                    sBitmapFile:=ChangeFileExt(FMapFileName, '.bmp');
                    sTiff      :=ChangeFileExt(FMapFileName, '.tif');

//                    sTempFile_Bmp:= IncludeTrailingBackslash(ExtractFileDir(sBitmapFile)) + 'temp.bmp';
//                    sTempFile_tif:= IncludeTrailingBackslash(ExtractFileDir(sBitmapFile)) + 'temp.tif';

                  //  bUseValues:=g_Params_bin_to_Map.MapExportType = mtGeoTiff_values;


                  //aMatrixFile: TBinMatrixFile

                  //aMatrixFile.CorrCoeff

                    if g_Params_bin_to_Map.MapExportType = mtGeoTiff_values then
                      SaveMatrixToBitmapFile_values (sBitmapFile)
                    else
                      SaveMatrixToBitmapFile (sBitmapFile); //, aMatrixFile.CorrCoeff); //, False);

//                   FileCopy(sBitmapFile, sTempFile_Bmp);


                 /////   FileCopy (sBitmapFile,'D:\55555555555555555555555555555\1.bmp');

                 //   img_BMP_to_TIFF(sBitmapFile, sTiff);

                    xyRect_42:= FValuesMatrix.GetXYBounds;

                    Assert (xyRect_42.TopLeft.X<>0);


                    xyRect_UTM.TopLeft    := geo_GK_to_UTM(xyRect_42.TopLeft,     FValuesMatrix.ZoneNum);
                    xyRect_UTM.BottomRight:= geo_GK_to_UTM(xyRect_42.BottomRight, FValuesMatrix.ZoneNum);

                    //[-a_nodata value]

//  s:= sPath_bin+ Format('gdal_translate.exe -a_srs EPSG:%d  "%s.envi" "%s.tif"',[28400+ FRelMatrix.ZoneNum,sFile,sFile]);


//                    sParam:= Format('gdal_translate  -a_srs "+proj=utm  +zone=%d +datum=WGS84" -of GTiff  -co "INTERLEAVE=PIXEL" -a_ullr %1.12f  %1.12f %1.12f %1.12f  %s %s ',
//                    sParam:= Format('gdal_translate  -a_srs "+proj=utm  +zone=%d +datum=WGS84" -of GTiff  -co "INTERLEAVE=PIXEL" -a_ullr %1.12f  %1.12f %1.12f %1.12f  %s %s ',
                    sParam:= Format(' -a_srs "+proj=utm  +zone=%d +datum=WGS84" -of GTiff  -co "COMPRESS=LZW" -a_ullr %1.12f  %1.12f %1.12f %1.12f  %s %s ',
                             [FValuesMatrix.ZoneNum + 30,


//  s:= sPath_bin+ Format('gdal_translate.exe -a_srs EPSG:%d  "%s.envi" "%s.tif"',[28400+ FRelMatrix.ZoneNum,sFile,sFile]);
//  oBAT.Add(s);


                              xyRect_UTM.TopLeft.Y,
                              xyRect_UTM.TopLeft.X,

                              xyRect_UTM.BottomRight.Y,
                              xyRect_UTM.BottomRight.X,

//                              DoubleQuotedStr(sBitmapFile),

{                              DoubleQuotedStr(sTempFile_Bmp),
                              DoubleQuotedStr(sTempFile_tif)
}

                              DoubleQuotedStr(sBitmapFile),
                              DoubleQuotedStr(sTiff)
//                              DoubleQuotedStr(sTiff)

                             ]);



//set GDAL_DATA=C:\ONEGA\RPLS_DB\bin\OSGeo4W\share\epsg_csv
//set GDAL_FILENAME_IS_UTF8=NO



    sPath_EXE:=SearchFileInPath ('gdal_translate.exe');
    if not FileExists(sPath_EXE) then
      sPath_EXE:= IncludeTrailingBackslash ( ExtractFileDir( Application.exeName) ) + 'GDAL\gdal_translate.exe';


   // sPath_bin:=ExtractFilePath(s);

//    sPath_epsg_csv:=GetParentFileDir (ExtractFilePath(sPath_bin)) + 'share\epsg_csv';



                      //http://www.gdal.org/frmt_gtiff.html
                      // gdal_translate rgba.tif withmask.tif -b 1 -b 2 -b 3 -mask 4 -co COMPRESS=JPEG -co PHOTOMETRIC=YCBCR --config GDAL_TIFF_INTERNAL_MASK YES

                //    sPath_EXE:= IncludeTrailingBackslash ( ExtractFileDir( Application.exeName) ) + 'GDAL\gdal_translate.exe';

                    //D:\OSGeo4W\bin

                    if not FileExists(sPath_EXE) then
                    begin
                   //   sPath_EXE:= 'D:\OSGeo4W\bin\gdal_translate.exe';

                      ShowMessage('not FileExists: ' + 'gdal_translate.exe');

                      Assert(FileExists(sPath_EXE));
                      exit;

                    end;
                  //    ShellExec(sPath, sParam);


                    sBatFile :=ChangeFileExt(FMapFileName, '.bat');

//                    s:=sPath_EXE + ' ' + sParam +

                    StrToTextFile (sBatFile, sPath_EXE + ' ' + sParam);

               //     ShellExecute (application.handle, 'open', 'cmd', PChar(sBatFile), nil, SW_MAXIMIZE);


//ShowMessage ('1111111111111111111111');

s:=ExtractFileDir(sBatFile);

                    SetCurrentDir(ExtractFileDir(sBatFile));


                    RunApp(sBatFile, ''); //, iCode);


                    {
                    ShellExecute(
                        application.Handle,
                        'open',
                        PChar(sBatFile),
                         nil, //PChar(params),
                         nil, //PChar(workingdirectory),
//                        SW_SHOW
                        SW_HIDE
                    );
                    }


                  end;

        else
          raise EClassNotFound.Create('');
      end;

    end;



//ShowMessage (' if g_Params_bin_to_Map.MapExportType in [mtRaster] then');

    // ---------------------------------------------------------------

//    if g_Params_bin_to_Map.MapExportType in [mtRaster] then
      case g_Params_bin_to_Map.ColorSourceType of
        csCells:  begin
                    sFile := ChangeFileExt(FMapFileName, '.colors.xml');

                    g_Params_bin_to_Map.Cells.SaveToXMLFile(sFile);
                    Assert(FileExists(sFile), sFile);
                  end;

        csRanges: begin
                    sFile := ChangeFileExt(FMapFileName, '.colors.xml');

                    g_Params_bin_to_Map.ColorSchema_Selected.SaveToXML(sFile);

                  //  ShowMessage(sFile);

                    Assert(FileExists(sFile),  sFile);
                  end;
      end;


//ShowMessage ('8');


  FreeAndNil(FValuesMatrix);


  FColorMatrix.ClearMemory;

 // FMatrixDecomposer.ClearMemory;

  DeleteColorMatrix();
  result := True;



//  ShowMessage ('function TBinToMap.ExecuteBinMatrix');

end;

//------------------------------------------------------
procedure TBinToMap.ExecuteProc;
//------------------------------------------------------
var
  I: Integer;
  oMatrixFileItem: TBinMatrixFile;
  iCount: integer;

  oKMZ: TKMZ_simple;
begin
  FMatrixDecomposer.OnProgress2:=OnProgress2;


  //------------------------------------------------------
  // Bin Matrix
  //------------------------------------------------------

  for I := 0 to g_Params_bin_to_Map.BinMatrixArrayList.Count-1 do
  begin

    DoProgress (i, iCount);

    oMatrixFileItem := g_Params_bin_to_Map.BinMatrixArrayList[i];

//ShowMessage (oMatrixFileItem.MatrixFILENAME);


    FMatrixFileName:= oMatrixFileItem.MatrixFILENAME;
    FMapFileName   := oMatrixFileItem.MapFileName;
    FColorSchemaID := oMatrixFileItem.ColorSchema_ID;
//      FMapKind       := FieldByName(FLD_MAP_KIND).AsInteger;
    FCaption       := oMatrixFileItem.CAPTION;


//    if oMatrixFileItem.IsMegafon then 
//      FColorSchemaID:=-1;
    
    if not ExecuteBinMatrix (oMatrixFileItem) then
      exit;


  //  Next;
  end;

//   ShowMessage ('ExecuteProc');

end;


end.



