unit u_map_classes;

interface

uses

  System.Generics.Collections, 
  

  SysUtils, Classes,
  u_geo

  ;

type
  TMapRegionItem = class //(TCollectionItem)
  public
    BLPointArray: TBLPointArray;
  end;

  // ---------------------------------------------------------------
  TMapRegion = //class//(TCollection)
               class(TList<TMapRegionItem>)
  // ---------------------------------------------------------------
  private
 //   function GetItems(Index: Integer): TMapRegionItem;

  public
    Value : Variant;
    Color : Integer;

    ID     : Integer;
    CELLID : string;
    LAC    : string;

    // --------------
    Properties: TStringList;


  public
    constructor Create;
    destructor Destroy; override;

    function AddItem: TMapRegionItem;

//    property Items[Index: Integer]: TMapRegionItem read GetItems; default;
  end;


  // ---------------------------------------------------------------
  TMapRegionList = //class(TList)
                   class(TList<TMapRegion>)
  // ---------------------------------------------------------------
  private
//    function GetItems(Index: Integer): TMapRegion;
  public
//    constructor Create;

    function FindByValue(aValue: Variant): TMapRegion;

    function AddItem: TMapRegion;

    function AddItemWithValue(aColor: Integer; aValue: Variant; aBLPointArray:
        TBLPointArray): TMapRegion;

//    procedure Clear; override;

//    property Items[Index: Integer]: TMapRegion read GetItems; default;
  end;



implementation

constructor TMapRegion.Create;
begin
  inherited;// Create(TMapRegionItem);
  Properties := TStringList.Create();
end;

destructor TMapRegion.Destroy;
begin
  FreeAndNil(Properties);
  inherited;
end;

function TMapRegion.AddItem: TMapRegionItem;
begin
  result := TMapRegionItem.Create;
  Add(Result);
end;

//function TMapRegion.GetItems(Index: Integer): TMapRegionItem;
//begin
//  Result := TMapRegionItem(inherited Items[Index]);
//end;

//constructor TMapRegionList.Create;
//begin
//  inherited Create();
//end;


function TMapRegionList.AddItem: TMapRegion;
begin
  Result :=TMapRegion.Create;
  Add(Result);

//  result := TMapRegion(Add);
end;

// ---------------------------------------------------------------
function TMapRegionList.AddItemWithValue(aColor: Integer; aValue: Variant;
    aBLPointArray: TBLPointArray): TMapRegion;
// ---------------------------------------------------------------
begin
  Result := FindByValue(aValue);
  if not Assigned(Result) then
  begin
    Result:=AddItem();
    Result.Value:=aValue;
    Result.Color:=aColor;
  end;

  Result.AddItem().BLPointArray:=aBLPointArray;
//    geo_BLPointArrayF_to_BLPointArray(aBLPointArrayF);

end;
//
//// ---------------------------------------------------------------
//procedure TMapRegionList.Clear;
//// ---------------------------------------------------------------
//var
//  I: Integer;
//begin
//  for I := 0 to Count - 1 do
//    Items[i].Free;
//
//  inherited;
//end;

// ---------------------------------------------------------------
function TMapRegionList.FindByValue(aValue: Variant): TMapRegion;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Result := nil;

  for I := 0 to Count - 1 do
    if Items[i].Value = aValue then
    begin
      Result := Items[i];
      Exit;
    end;
end;


//function TMapRegionList.GetItems(Index: Integer): TMapRegion;
//begin
//  Result := TMapRegion(inherited Items[Index]);
//end;


//var

begin
//  TMapRegionList:=


end.
