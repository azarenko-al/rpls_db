unit dm_Main_MIF;

interface

uses
  Classes, Forms, SysUtils, Windows, //Tmultip,

  u_func_reg,
//  Dialogs,
  u_func,
  u_files,
  u_log,

  dm_Params,

  u_BinToMap_mif
  ;

type
  TdmMain_bin_to_mif = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
  private

    FBinToMap: TBinToMap_MIF;
    FTerminated: boolean;

    procedure DoProcessException(Sender: TObject; E: Exception);
  end;

var
  dmMain_bin_to_mif: TdmMain_bin_to_mif;

//========================================================
implementation {$R *.DFM}
//=================================================================
const
//  TEMP_FILENAME='u:\data\in.xml';
//  TEMP_FILENAME='c:\temp\_bin_to_map.xml';
//  TEMP_FILENAME='c:\temp\_bin_to_map_interf.xml';

//  TEMP_FILENAME = 'c:\temp\_bin_to_map.xml';
//  TEMP_FILENAME='c:\temp\_bin_to_mif.xml';


///////  TEMP_FILENAME='C:\WINDOWS\TEMP\bin_to_mif_cia.xml';

  TEMP_FILENAME='e:\test\in.xml';
//  TEMP_FILENAME='c:\temp\rpls\bin_to_mif.xml';
/////////  TEMP_FILENAME='C:\TEMP\rpls\calc_statistic.xml';

//-----------------------------------------------------------------
procedure TdmMain_bin_to_mif.DataModuleCreate(Sender: TObject);
//-----------------------------------------------------------------
var
  sIniFileName: string;
 // MultiImage1: TPMultiImage;

begin
  sIniFileName:=ParamStr(1);

  Application.OnException:= DoProcessException;

//  ShowMessage(sIniFileName);
//  ShowMessage(AsString(FileExists(sIniFileName)));


  if not FileExists(sIniFileName) then
    sIniFileName:=TEMP_FILENAME;


  g_Log.Init (GetApplicationDir+'ErrorLog\bin_to_mif.log');
  g_Log.Add('========================================');
  g_Log.Add('rpls_bin_to_mif.exe');

{
  custom_error_log:=
    TErrorLog.Create(GetApplicationDir+'ErrorLog\rpls_calc_error_log.log', false);
  custom_error_log.AddText('========================================');
  custom_error_log.AddText('rpls_bin_to_mif.exe');
}

  dmParams.LoadFromXML (sIniFileName);

  FBinToMap:= TBinToMap_MIF.Create;
  FTerminated:= not FBinToMap.ExecuteDlg ('���������� ����...');
  FBinToMap.Free;

  ini_WriteBool(sIniFileName, 'main', 'result', not FTerminated);

/////  ExitProcess(IIF(FTerminated, 1, 0));

end;

//--------------------------------------------------------------
procedure TdmMain_bin_to_mif.DoProcessException(Sender: TObject; E: Exception);
//--------------------------------------------------------------
begin
  FTerminated:= true;
 //////////// custom_error_log.AddError('TdmMain.DoProcessException', E.Message);
end;



end.

