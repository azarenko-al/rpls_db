unit u_BinToMap_classes;

interface

uses
  Classes, SysUtils,

  u_img
  ;

type
  TColorSchemaList = class;
  TColorSchemaRangeList = class;

  TCell = class(TCollectionItem)
  public
   ID:         Integer;
   COLOR:      Integer;
   VALUE:      Double;

  end;


  TMatrixFile = class(TCollectionItem)
  public
    MatrixFileName:  String;

    CAPTION       :  String;
    MapFileName   :  String;

    ColorSchemaID :  Integer;
  end;


  TColorSchemaRange = class(TCollectionItem)
  public
    MinValue : double; // �������� � �������
    MaxValue : double; // �������� � �������
    Color    : integer; // ���� �� �����
    MaxColor : integer;
    MinColor : integer;

    Type_    : (rtFixed,rtGradient);
 //  Caption  : string;
  //IsTransparent: boolean;
  end;

  TColorSchema = class(TCollectionItem)
  public
    ID : Integer;
    Index : Integer;

    Ranges: TColorSchemaRangeList;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;

  end;


  TColorSchemaRangeList = class(TCollection)
  private
    function GetItems(Index: Integer): TColorSchemaRange;
  public
    constructor Create;
    function AddItem: TColorSchemaRange;
    function GetColor(aValue: Double): integer;
    function GetRangeIndex(aValue: integer): integer;
    property Items[Index: Integer]: TColorSchemaRange read GetItems; default;
  end;


  TColorSchemaList = class(TCollection)
  private
    function GetItems(Index: Integer): TColorSchema;
  public
    constructor Create;
    function AddItem: TColorSchema;
    property Items[Index: Integer]: TColorSchema read GetItems; default;
  end;


  TMatrixFileList = class(TCollection)
  private
    function GetItems(Index: Integer): TMatrixFile;
  public
    constructor Create;
    function AddItem: TMatrixFile;
    property Items[Index: Integer]: TMatrixFile read GetItems; default;
  end;


  TCellList = class(TCollection)
  private
    function GetItems(Index: Integer): TCell;
  public
    constructor Create;
    function AddItem: TCell;
    property Items[Index: Integer]: TCell read GetItems; default;
  end;

implementation

constructor TColorSchemaRangeList.Create;
begin
  inherited Create(TColorSchemaRange);
end;

function TColorSchemaRangeList.AddItem: TColorSchemaRange;
begin
  result := TColorSchemaRange(Add);
end;

//---------------------------------------------------------
function TColorSchemaRangeList.GetColor(aValue: Double): integer;
//---------------------------------------------------------
// var aRangeIndex: integer):
var i: integer;
begin
  Result:=-1;

  for i:=0 to Count-1 do
//          for i:=0 to High(Ranges) do

   if (Items[i].MinValue<=aValue) and
      (aValue<Items[i].MaxValue)
   then
     with Items[i] do
     begin
         case Type_ of
           rtFixed:    Result:=Color;
           rtGradient: Result:=img_MakeGradientColor (aValue,
                                         MinValue, MaxValue,
                                         MinColor, MaxColor);
         end;

//          aRangeIndex:=i;

         break;
       end;
end;



//---------------------------------------------------------
function TColorSchemaRangeList.GetRangeIndex(aValue: integer): integer;
//---------------------------------------------------------
var i: integer;
begin
  Result:=-1;

  for i:=0 to Count-1 do
    if (Items[i].MinValue<=aValue) and
       (aValue<Items[i].MaxValue) then
    begin
      Result:=i;  Break;
    end;
end;



function TColorSchemaRangeList.GetItems(Index: Integer): TColorSchemaRange;
begin
  Result := TColorSchemaRange(inherited Items[Index]);
end;

constructor TColorSchemaList.Create;
begin
  inherited Create(TColorSchema);
end;

function TColorSchemaList.AddItem: TColorSchema;
begin
  result := TColorSchema(Add);
end;

function TColorSchemaList.GetItems(Index: Integer): TColorSchema;
begin
  Result := TColorSchema(inherited Items[Index]);
end;

constructor TColorSchema.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  Ranges := TColorSchemarangeList.Create();
end;

destructor TColorSchema.Destroy;
begin
  FreeAndNil(Ranges);
  inherited Destroy;
end;

constructor TMatrixFileList.Create;
begin
  inherited Create(TMatrixFile);
end;

function TMatrixFileList.AddItem: TMatrixFile;
begin
  result := TMatrixFile(Add);
end;

function TMatrixFileList.GetItems(Index: Integer): TMatrixFile;
begin
  Result := TMatrixFile(inherited Items[Index]);
end;

constructor TCellList.Create;
begin
  inherited Create(TCell);
end;

function TCellList.AddItem: TCell;
begin
  result := TCell(Add);
end;

function TCellList.GetItems(Index: Integer): TCell;
begin
  Result := TCell(inherited Items[Index]);
end;

end.
