object frm_Main: Tfrm_Main
  Left = 498
  Top = 374
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Converter: Prediction -> Raster'
  ClientHeight = 148
  ClientWidth = 592
  Color = clBtnFace
  Constraints.MaxHeight = 182
  Constraints.MaxWidth = 600
  Constraints.MinHeight = 175
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  DesignSize = (
    592
    148)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 5
    Top = 8
    Width = 128
    Height = 13
    Caption = 'Source prediction file name'
  end
  object Label2: TLabel
    Left = 5
    Top = 56
    Width = 67
    Height = 13
    Caption = 'Destination dir'
  end
  object FilenameEdit1: TFilenameEdit
    Left = 5
    Top = 24
    Width = 580
    Height = 21
    Filter = 'All files (*.kup)|*.kup'
    Anchors = [akLeft, akTop, akRight]
    NumGlyphs = 1
    TabOrder = 0
  end
  object DirectoryEdit1: TDirectoryEdit
    Left = 5
    Top = 72
    Width = 580
    Height = 21
    NumGlyphs = 1
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
  end
  object Button1: TButton
    Left = 5
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Execute'
    Default = True
    TabOrder = 2
    OnClick = Button1Click
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    Options = [fpPosition]
    StoredProps.Strings = (
      'FilenameEdit1.FileName'
      'DirectoryEdit1.Text')
    StoredValues = <>
    Left = 232
    Top = 88
  end
end
