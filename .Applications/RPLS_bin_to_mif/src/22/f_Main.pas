unit f_Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, rxToolEdit, Mask, rxPlacemnt,

  u_BinToMap_classes,

  dm_Params_bin_to_mif,

//  dm_Params_bin_to_mif,
  u_BinToMap_mif,

  u_func

  ;

type
  Tfrm_Main = class(TForm)
    FormStorage1: TFormStorage;
    FilenameEdit1: TFilenameEdit;
    DirectoryEdit1: TDirectoryEdit;
    Label1: TLabel;
    Label2: TLabel;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private

  public

  end;
(*
var
  frm_Main: Tfrm_Main;*)

implementation

{$R *.dfm}

procedure Tfrm_Main.Button1Click(Sender: TObject);
var
  oBinToMap: TBinToMap_MIF;
  oMatrixFile: TMatrixFile;
begin
  if not Assigned(dmParams_bin_to_MIF) then
    Application.CreateForm(TdmParams_bin_to_MIF, dmParams_bin_to_MIF);

  dmParams_bin_to_MIF.LoadFromXML (GetApplicationDir + 'bin_to_mif.xml');

  dmParams_bin_to_MIF.MatrixArrayList.Clear;

  oMatrixFile  :=  dmParams_bin_to_MIF.MatrixArrayList.AddItem;
  oMatrixFile.MatrixFileName  := FilenameEdit1.FileName;
  oMatrixFile.ColorSchemaID  := 1;

  oMatrixFile.MapFileName  := IncludeTrailingBackslash(DirectoryEdit1.Text) +
     ExtractFileName ( ChangeFileExt (FilenameEdit1.FileName, '.tab') );


//  dmParams_bin_to_MIF.LoadFromXML ('');

  oBinToMap:= TBinToMap_MIF.Create;
  oBinToMap.ExecuteDlg ('���������� ����...');
  oBinToMap.Free;

end;

end.
