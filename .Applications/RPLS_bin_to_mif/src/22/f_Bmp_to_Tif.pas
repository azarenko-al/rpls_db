unit f_Bmp_to_Tif;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TMultiP;

type
  Tfrm_Bmp_to_Tif = class(TForm)
    MultiImage1: TPMultiImage;
    procedure FormCreate(Sender: TObject);
  private
  
  public
    class procedure ExecProc ();
  end;

//var
//  frm_Bmp_to_Tif: Tfrm_Bmp_to_Tif;

//=================================================
implementation {$R *.DFM}
//=================================================

class procedure Tfrm_Bmp_to_Tif.ExecProc;
begin
  with Tfrm_Bmp_to_Tif.Create(Application) do
  begin
//    MultiImage1.Picture.Bitmap:= aBitmap;
    MultiImage1.imagename:='C:\Temp.bmp';

    DeleteFile('C:\Temp.tif');
    MultiImage1.SaveAsTIF('C:\Temp.tif');

    Free;
  end;
end;

procedure Tfrm_Bmp_to_Tif.FormCreate(Sender: TObject);
begin
(*  MultiImage1.Picture.Bitmap:=

  MultiImage1.imagename:='C:\Temp.bmp';*)

  MultiImage1.SaveAsTIF('C:\Temp.tif');
end;

end.
