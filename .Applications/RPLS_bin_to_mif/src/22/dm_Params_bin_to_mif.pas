unit dm_Params_bin_to_mif;

interface

uses
  SysUtils, Classes, DB, dxmdaset, Variants, Forms, XMLDoc, XMLIntf,

  u_func,

  u_xml_document,

 // u_XML,
  u_img,

  u_BinToMap_classes
  ;

type
  TdmParams_bin_to_MIF = class(TDataModule)
    procedure DataModuleDestroy(Sender: TObject);
//    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

  private
//    FXMLDoc: TXMLDoc;

{    FvColorSchemaNode: IXMLNode;

}
    FCashedLastValue: double;
    FCashedLastColor: integer;
    FCashedLastRangeIndex: integer;

    FUseCashedColor: boolean;

//    FvCellsNode: IXMLNode;


    FCells: TCellList;


(*
    FCells: array of record
     ID:         Integer;
     COLOR:      Integer;
     VALUE:      Double;
  //   NAME:       String;
  //   SITE_NAME:  String;
   //  CELL_NAME:  String;
    end;
*)

    FColorSchemaIndex: Integer;

    FColorSchemaList: TColorSchemaList;
(*
    FColorSchemas_old: array of record
                     ID : Integer;
                     Index : Integer;
                 //    XMLNode: IXMLNode;

                     Ranges : array of record
                                 MinValue : double; // �������� � �������
                                 MaxValue : double; // �������� � �������
                                 Color    : integer; // ���� �� �����
                                 MaxColor : integer;
                                 MinColor : integer;

                                 Type_    : (rtFixed,rtGradient);
                               //  Caption  : string;
                                //IsTransparent: boolean;
                               end;
                   end;
*)
(*
    FPointGroups: array of record
                    ID:     Integer;
                    MAP:    string;
                    step:   Integer;
                    FRAME:  Boolean;
                    ColorSchemaID: Integer;

                    Points: array of record
                      PARENT_ID: Integer;
                      VALUE: Integer;
                      LAT:   double;
                      LON:   double;
                    end;
                  end;*)


    function SaveCellsSchemaXmlFile(aMatrixFileName, aMapFileName: string): Integer;

  public
    MapType: (mtVector,mtRaster);
 //   FIsStatistic : boolean;

    ColorSourceType: (csCells,csRanges);

    MatrixArrayList: TMatrixFileList;

//    procedure PrepareCellsColorRanges;

//    procedure PrepareStatisticColorRanges;
//    function GetPointListBounds (aID: integer): TBLRect;

    function SetColorSchema (aID: integer): boolean;
    function GetColor(aValue: Double; var aRangeIndex: integer): integer;
    function GetRangeIndex(aValue: integer): integer;


    procedure LoadFromXML (aFileName: string);

// TODO: SaveColorSchemaXmlFile
//  function SaveColorSchemaXmlFile(aID: integer; aMatrixFileName, aMapFileName:
//      string): Integer;
  //  procedure SaveRangeToIniFile(aFileName: string);

    class procedure Init;

  end;

var
  dmParams_bin_to_MIF: TdmParams_bin_to_MIF;

const
  FLD_SCHEMA_ID = 'SchemaID';
  FLD_FRAME     = 'FRAME';
  FLD_MAP_KIND  = 'MapKind';

{  _KGR = '.kgr';
  _FRGR = '.frgr';
}
  FLD_MAP  = 'map';

//==================================================================
//==================================================================
implementation {$R *.DFM}

const
//  BLANK_COLOR = clBlack;
//  BLANK_COLOR = clWhite;


  DEF_FIXED_COLOR    = 0;
  DEF_GRADIENT_COLOR = 1;

  FLD_step = 'step';

 // FLD_Min_Value = 'Min_Value';
//  FLD_SITE_NAME = 'SITE_NAME';
//  FLD_CELL_NAME = 'CELL_NAME';

  FLD_Min_Value='Min_Value';
  FLD_Max_Value='Max_Value';
  FLD_Min_Color='Min_Color';
  FLD_Max_Color='Max_Color';



procedure TdmParams_bin_to_MIF.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FCells);
  FreeAndNil(MatrixArrayList);
  FreeAndNil(FColorSchemaList);

end;


//-----------------------------------------------------------------
procedure TdmParams_bin_to_MIF.DataModuleCreate(Sender: TObject);
//-----------------------------------------------------------------
begin
  MatrixArrayList := TMatrixFileList.Create();

  FColorSchemaList := TColorSchemaList.Create();
  FCells := TCellList.Create();
end;


//---------------------------------------------------------
function TdmParams_bin_to_MIF.GetColor (aValue: Double; var aRangeIndex: integer): integer;
//---------------------------------------------------------
var i: integer;
begin
  Result:=-1;
  aRangeIndex:=-1;


  if FUseCashedColor then
    if aValue=FCashedLastValue then
    begin
      result := FCashedLastColor;
      aRangeIndex :=FCashedLastRangeIndex;
      exit;
    end;


  case ColorSourceType of
    csCells:
        for i:=0 to FCells.Count-1 do
//        for i:=0 to High(FCells) do
          if FCells[i].ID=aValue then
        begin
          Result:=FCells[i].COLOR;
          aRangeIndex:=i;
          Break;
        end;


    csRanges:
      begin
        Assert(FColorSchemaIndex>=0, 'FColorSchemaIndex <0');

        with FColorSchemaList[FColorSchemaIndex] do
          for i:=0 to Ranges.Count-1 do
//          for i:=0 to High(Ranges) do

           if (Ranges[i].MinValue<=aValue) and (aValue<Ranges[i].MaxValue) then
             with Ranges[i] do
             begin
                 case Type_ of
                   rtFixed:    Result:=Color;
                   rtGradient: Result:=img_MakeGradientColor (aValue,
                                                 MinValue, MaxValue,
                                                 MinColor, MaxColor);
                 end;

                 aRangeIndex:=i;
                 break;
               end;
      end;

  end;


  FUseCashedColor:=True;
  FCashedLastValue:=aValue;
  FCashedLastColor:=Result;
  FCashedLastRangeIndex :=aRangeIndex;
end;

//---------------------------------------------------------
function TdmParams_bin_to_MIF.GetRangeIndex (aValue: integer): integer;
//---------------------------------------------------------
var i: integer;
begin
  Result:=-1;

  case ColorSourceType of
    csCells:
      for i:=0 to FCells.Count-1 do
//      for i:=0 to High(FCells) do
        if FCells[i].ID=aValue then
        begin
          Result:=i;
          Break;
        end;

    csRanges:
      begin
        Assert(FColorSchemaIndex>=0, 'Value <0');

        Result:=FColorSchemaList[FColorSchemaIndex].Ranges.GetRangeIndex(aValue);

(*

        with FColorSchemaList[FColorSchemaIndex] do
          for i:=0 to Ranges.Count-1 do
//          for i:=0 to High(Ranges) do
            if (Ranges[i].MinValue<=aValue) and (aValue<Ranges[i].MaxValue) then
            begin
              Result:=i;  Break;
            end;
            *)

     end;
  end;

end;


class procedure TdmParams_bin_to_MIF.Init;
begin
  if not Assigned(dmParams_bin_to_MIF) then
    dmParams_bin_to_MIF := TdmParams_bin_to_MIF.Create(Application);
end;


//-----------------------------------------------------------------
procedure TdmParams_bin_to_MIF.LoadFromXML (aFileName: string);
//-----------------------------------------------------------------

   //-------------------------------------------------------------------
   procedure DoLoadParams(aParamsNode: IXMLNode);
   //-------------------------------------------------------------------
   var i: integer;
       vNode,vNode2 : IXMLNode;
       sValue: string;
   begin
   //  FIsStatistic := AsBoolean(xml_GetAttr(aParamsNode, 'IsStatistic'));
//     vNode:=xml_FindNode (aParamsNode, 'MapType');

     sValue:=xml_FindNodeAttr (aParamsNode, 'MapType', 'value');

//     sValue:=xml_GetAttrByTagName (aParamsNode, 'MapType', 'value');

    // if Eq(sValue,'')or
    //    Eq(sValue,'Vector') then MapType:=mtVector else

     if Eq(sValue,'Raster') then MapType:=mtRaster
                            else MapType:=mtVector;

   //  MapType:=mtRaster;
   //  MapType:=mtVector;

    {$IFDEF RASTER}
    MapType:=mtRaster;
    {$ENDIF}

   end;


   //-------------------------------------------------------------------
   procedure DoAddRange(aNode: IXMLNode; aColorSchema: TColorSchema);
   //-------------------------------------------------------------------
   var i: integer;
      vNode: IXMLNode;
     iLen: Integer;

     oRange: TColorSchemaRange;
   begin
    // iLen := aNode.ChildNodes.Length;

//     SetLength(FColorSchemaList[aIndex].Ranges, iLen);

   //  with aColorSchema do
       for i:=0 to aNode.ChildNodes.Count - 1 do
       begin
         vNode:=aNode.ChildNodes[i];

         oRange:=aColorSchema.Ranges.AddItem;

         oRange.MinValue:= xml_GetFloatAttr(vNode, FLD_Min_Value);
         oRange.MaxValue:= xml_GetFloatAttr(vNode, FLD_Max_Value);
         oRange.COLOR:=    xml_GetIntAttr  (vNode, ATT_COLOR);
         oRange.MinColor:= xml_GetIntAttr  (vNode, FLD_Min_Color);
         oRange.MaxColor:= xml_GetIntAttr  (vNode, FLD_Max_Color);

         if xml_GetIntAttr(vNode, ATT_TYPE)=0 then
         begin
           oRange.TYPE_:=rtFixed;
         end
         else
           oRange.TYPE_:=rtGradient;

{
         db_AddRecord(mem_Range,
                   [db_Par(FLD_PARENT_ID, aID),
                    db_Par(FLD_Min_Value, xml_GetFloatAttr(vNode, FLD_Min_Value)),
                    db_Par(FLD_Max_Value, xml_GetFloatAttr(vNode, FLD_Max_Value)),
                    db_Par(FLD_COLOR,     xml_GetIntAttr  (vNode, ATT_COLOR)),
                    db_Par(FLD_Min_Color, xml_GetIntAttr  (vNode, FLD_Min_Color)),
                    db_Par(FLD_Max_Color, xml_GetIntAttr  (vNode, FLD_Max_Color)),
                    db_Par(FLD_TYPE,      xml_GetIntAttr  (vNode, ATT_TYPE))
                    ]);}

       end;
   end;

   //-------------------------------------------------------------------
   procedure DoLoadCells(aNode: IXMLNode);
   //-------------------------------------------------------------------
   var i: integer;
       dValue  : double;
       vNode,vNode2 : IXMLNode;
     //iLen: Integer;

     oCell: TCell;
   begin
   //   vNode := xml_GetNodeByPath(aNode,['CELLS']);

      vNode:=xml_FindNode (aNode, 'CELLS');


      if Assigned(vNode) then
      begin
    //    FvCellsNode:=vNode.CloneNode(True);

     //   iLen := vNode.ChildNodes.Length;
//        SetLength (FCells, iLen);


        for i := 0 to vNode.ChildNodes.Count - 1 do
        begin
          vNode2:=vNode.ChildNodes[i];

          oCell:=FCells.AddItem;

          dValue := AsFloat(xml_GetAttr (vNode2, ATT_VALUE));

          oCell.ID    := xml_GetIntAttr (vNode2, ATT_ID);
          oCell.COLOR := xml_GetIntAttr (vNode2, ATT_COLOR);
          oCell.VALUE := xml_GetFloatAttr (vNode2, ATT_VALUE);

{          FCells[i].NAME  :=    xml_GetStrAttr (vNode2, ATT_NAME));
          FCells[i].CELL_NAME:= xml_GetStrAttr (vNode2, FLD_CELL_NAME));
          FCells[i].SITE_NAME:= xml_GetStrAttr (vNode2, FLD_SITE_NAME));
}

{
          db_AddRecord(mem_Cells,
                       [db_Par(FLD_ID,        xml_GetAttr (vNode2, ATT_ID)),
                        db_Par(FLD_COLOR,     xml_GetAttr (vNode2, ATT_COLOR))
                     //   db_Par(FLD_VALUE,     dValue),
                      //  db_Par(FLD_NAME,      xml_GetAttr (vNode2, ATT_NAME))
                     //   db_Par(FLD_CELL_NAME, xml_GetAttr (vNode2, FLD_CELL_NAME)),
                      //  db_Par(FLD_SITE_NAME, xml_GetAttr (vNode2, FLD_SITE_NAME))
                        ]);}
        end;
      end;
   end;

   //-------------------------------------------------------------------
   procedure DoLoadMatrixList(aNode: IXMLNode);
   //-------------------------------------------------------------------
   var i: integer;
     //  sMatrixFileName: string;
     //  sMapFileName: string;
       s: string;
       vNode1,vNode2 : IXMLNode;
     iLen: Integer;
     oMatrixFile: TMatrixFile;
   begin
    // s:=xml_TagName(aNode  );

   //   vNode1 := xml_GetNodeByPath(aNode,  ['MATRIXES']);
      vNode1 := xml_FindNode(aNode, 'MATRIXES');


      if VarExists(vNode1) then
      begin
        iLen := AsInteger(vNode1.ChildNodes.Count);
//        iLen := vNode1.ChildNodes.Length;
//        SetLength(MatrixArrayList, iLen);


         for i := 0 to vNode1.ChildNodes.Count - 1 do
         begin
           vNode2:=vNode1.ChildNodes[i];

           oMatrixFile:=MatrixArrayList.AddItem;


         {  sMatrixFileName:=
           sMapFileName   :=;}

           oMatrixFile.MatrixFILENAME:=xml_GetAttr (vNode2, 'FileName');
           oMatrixFile.CAPTION       :=xml_GetAttr (vNode2, 'Caption');
           oMatrixFile.MapFileName   :=xml_GetAttr (vNode2, 'Map');
           oMatrixFile.ColorSchemaID :=xml_GetIntAttr(vNode2, 'SCHEMA');

//           MatrixArrayList[i].MAP_KIND      :=xml_GetIntAttr    (vNode2, FLD_MAP_KIND);

{
           db_AddRecord(mem_Matrix,
                       [db_Par(FLD_FILENAME,  sMatrixFileName),
                        db_Par(FLD_CAPTION,   xml_GetAttr (vNode2, 'Caption')),
                        db_Par(FLD_Map,       sMapFileName),
                        db_Par(FLD_SCHEMA_ID, xml_GetIntAttr (vNode2, 'SCHEMA')),
                        db_Par(FLD_MAP_KIND,  xml_GetIntAttr (vNode2, FLD_MAP_KIND))
                        ]);
}
         end;
      end;
   end;

var
  b: boolean;
  iID: Integer;
//  v: variant;
  i: integer;
  vRoot,vNode,vNode1,vNode2,vNode3 : IXMLNode;
  vColorSchemasNode: IXMLNode;
  iLen: integer;
//  oXMLDoc1: TXMLDoc;
  oXMLDoc: TXMLDoc;

  oColorSchema: TColorSchema;

 // vNode6: IXMLNode;
begin
  if not FileExists(aFileName) then
    Exit;


//  if not assigned(FXMLDoc) then
  oXMLDoc:=TXMLDoc.Create;

//  oXMLDoc1:=TXMLDoc.Create;

  oXMLDoc.LoadFromFile(aFileName);

  vRoot := oXMLDoc.DocumentElement;

 // b:=FileExists(aFileName);

 // if vRoot=NULL then
 //   Exit;


  if VarIsNull(vRoot) then
    Exit;


  //for MIF use rasters
  vNode := xml_FindNode(vRoot, 'PARAMS');
  if VarExists(vNode) then
    DoLoadParams (vNode);


  vColorSchemasNode := xml_FindNode(vRoot, 'COLOR_SCHEMAS');


  if VarExists(vColorSchemasNode) then
  begin
    iLen:=vColorSchemasNode.ChildNodes.Count;
  //  SetLength(FColorSchemaList, iLen);

    for i := 0 to vColorSchemasNode.ChildNodes.Count - 1 do
    begin
      vNode2:=vColorSchemasNode.ChildNodes[i];

      iID:=xml_GetIntAttr (vNode2, ATT_ID);

    //////  db_AddRecord(mem_ColorSchema1, [db_Par(FLD_ID, iID)]);

      oColorSchema:=FColorSchemaList.AddItem;

      oColorSchema.ID:=iID;
      oColorSchema.Index:=i;

   //   FColorSchemaList[i].ID:=iID;
   //   FColorSchemaList[i].Index:=i;
  ////////    FColorSchemaList[i].XMLNode:=vNode2.CloneNode(1); // 1-deep

      DoAddRange (vNode2, oColorSchema);
    end;
  end;

  DoLoadMatrixList(oXMLDoc.DocumentElement);
  DoLoadCells(oXMLDoc.DocumentElement);
 // DoLoadPointLists(oXMLDoc.DocumentElement);

  oXMLDoc.Free;
end;

//-----------------------------------------------------------------
function TdmParams_bin_to_MIF.SaveCellsSchemaXmlFile(aMatrixFileName, aMapFileName:
    string): Integer;
//-----------------------------------------------------------------
var
  I: Integer;
  oXMLDoc: TXMLDoc;
  vNode,vNodeSrc : IXMLNode;
begin
  oXMLDoc:=TXMLDoc.Create;

  xml_AddNode(oXMLDoc.DocumentElement, 'MATRIX',
                  [xml_Par('MatrixFileName', aMatrixFileName)]);

{  vNode :=xml_AddNodeTag(oXMLDoc.DocumentElement, 'COLOR_SCHEMAS');

  vNodeSrc:=FvColorSchemasNode.ChildNodes.Item(FColorSchemaList[i].Index);
}

/////////////  oXMLDoc.Document.ChildNodes. appendChild (FvCellsNode);

////////  oXMLDoc.SaveToFile( 'd:\_cells.xml');

  oXMLDoc.SaveToFile( ChangeFileExt(aMapFileName, '.xml'));
  oXMLDoc.Free;

  //dmParams.SaveColorSchemaXmlFile (FColorSchemaID, FMatrixFileName, FMapFileName);
end;

//-----------------------------------------------------------------
function TdmParams_bin_to_MIF.SetColorSchema(aID: integer): boolean;
//-----------------------------------------------------------------
var i,iValue: integer;
//  I: Integer;
begin
  FColorSchemaIndex:=-1;

  for I := 0 to FColorSchemaList.Count-1 do
//  for I := 0 to High(FColorSchemaList) do
    if FColorSchemaList[i].ID=aID then
    begin
      FColorSchemaIndex:=i;
      Exit;
    end;

  {
  mem_Range.First;
  i := 0;

//////  if mem_ColorSchema1.Locate(FLD_ID, aID, []) then


  with mem_Range do
    while not EOF do
    begin
      if FieldValues[FLD_PARENT_ID] = aID then
        i:=i+1;
      Next;
    end;

  SetLength(Ranges,i);
  i := 0;

  mem_Range.SortedField:=FLD_Min_Value;
  mem_Range.First;

  with mem_Range do
    while not EOF do
    begin
      if FieldValues[FLD_PARENT_ID] = aID then
      begin
          Ranges[i].Color := FieldValues[FLD_COLOR];
          Ranges[i].MinValue := FieldValues[FLD_Min_Value];
          Ranges[i].MaxValue := FieldValues[FLD_Max_Value];
          Ranges[i].MinColor := FieldValues[FLD_Min_Color];
          Ranges[i].MaxColor := FieldValues[FLD_Max_Color];

          iValue:=FieldValues[FLD_TYPE];
          case iValue of
            DEF_FIXED_COLOR    : Ranges[i].Type_:=rtFixed;
            DEF_GRADIENT_COLOR : Ranges[i].Type_:=rtGradient;
          end;

          i:=i+1;
      end;
      Next;
    end;


  Result:=(Length(Ranges)>0);
  }

end;

// TODO: SaveColorSchemaXmlFile
////-----------------------------------------------------------------
//function TdmParams_bin_to_MIF.SaveColorSchemaXmlFile(aID: integer; aMatrixFileName,
//  aMapFileName: string): Integer;
////-----------------------------------------------------------------
//var
//I: Integer;
//oXMLDoc: TXMLDoc;
//vNode,vNodeSrc : IXMLNode;
//begin
// { for I := 0 to High(FColorSchemaList) do
//  if FColorSchemaList[i].ID=aID then
//begin
//  oXMLDoc:=TXMLDoc.Create;
//
//  xml_AddNode(oXMLDoc.DocumentElement, 'MATRIX',
//                  [xml_Par('MatrixFileName', aMatrixFileName)]);
//
//  vNode :=xml_AddNodeTag(oXMLDoc.DocumentElement, 'COLOR_SCHEMAS');
//
//  vNodeSrc:=FvColorSchemasNode.ChildNodes.Item(FColorSchemaList[i].Index);
//
//  vNode.appendChild (vNodeSrc);
//
//  oXMLDoc.SaveToFile( 'd:\1.xml');
//
//  oXMLDoc.SaveToFile( ChangeFileExt(aMapFileName, '.xml'));
//  oXMLDoc.Free;
//
//  Break;
//end;}
//
////dmParams.SaveColorSchemaXmlFile (FColorSchemaID, FMatrixFileName, FMapFileName);
//end;

begin

end.


(*




//------------------------------------------------------
//function TdmParams_bin_to_MIF.GetPointListBounds (aID: integer): TBLRect;
//------------------------------------------------------
//var blBounds: TBLRect;
//    blPoint: TBLPoint;
//begin
//  Result:=MakeBLRect (0,0,0,0);
//
//  with mem_Points do begin
//    First;
//    while not EOF do
//    begin
//       if FieldValues[FLD_PARENT_ID]=aID then
//       begin
//         blPoint:=db_ExtractBLPoint (mem_Points);
//         geo_UpdateRectForPoint (blPoint, Result);
//       end;
//       Next;
//    end;
//  end;
//end;



{
  db_CreateField (mem_Matrix,
             [db_Field (FLD_FILENAME,  ftString),
              db_Field (FLD_CAPTION,   ftString),
              db_Field (FLD_MAP,       ftString),
              db_Field (FLD_SCHEMA_ID, ftInteger),
              db_Field (FLD_MAP_KIND,  ftInteger)
               ]);}

 { db_CreateField (mem_Cells,
             [db_Field (FLD_ID,        ftInteger),
              db_Field (FLD_COLOR,     ftInteger),
              db_Field (FLD_VALUE,     ftFloat),
              db_Field (FLD_NAME,      ftString)
           //   db_Field (FLD_SITE_NAME,  ftString),
            //  db_Field (FLD_CELL_NAME,  ftString)
                                             ]);
}
{  db_CreateField (mem_ColorSchema1,
             [db_Field (FLD_ID,    ftInteger) ]);
}
{
  db_CreateField (mem_Range,
             [db_Field (FLD_PARENT_ID,  ftInteger),
              db_Field (FLD_COLOR,      ftInteger),
              db_Field (FLD_Min_Value,    ftFloat),
              db_Field (FLD_Max_Value,    ftFloat),
              db_Field (FLD_Min_Color,    ftInteger),
              db_Field (FLD_Max_Color,    ftInteger),
              db_Field (FLD_TYPE,       ftInteger) ]);
}
(*
  db_CreateField (mem_Point_Groups,
             [db_Field (FLD_ID,     ftInteger),
              db_Field (FLD_MAP,    ftString),
              db_Field (FLD_step,   ftInteger),
              db_Field (FLD_FRAME,  ftBoolean),
              db_Field (FLD_SCHEMA_ID, ftInteger)
             ]);

  db_CreateField (mem_Points,
             [
              db_Field (FLD_PARENT_ID, ftInteger),
              db_Field (FLD_VALUE, ftInteger),
              db_Field (FLD_LAT,   ftFloat),
              db_Field (FLD_LON,   ftFloat)
             ]);
*)

 // mem_Cells.Open;
//  mem_Matrix.Open;
 // mem_ColorSchema1.Open;
 // mem_Range.Open;

(*  mem_Point_Groups.Open;
  mem_Points.Open;
*)



 (*  //-------------------------------------------------------------------
   procedure DoLoadPoints (aParentNode: IXMLNode; aParentID: integer);
   //-------------------------------------------------------------------
   var i: integer;  vNode: IXMLNode;
   begin
      for i := 0 to aParentNode.ChildNodes.Length - 1 do
      begin
        vNode:=aParentNode.ChildNodes.Item(i);
        db_AddRecord(mem_Points,
                     [db_Par(FLD_PARENT_ID,  aParentID),
                      db_Par(FLD_LAT,        xml_GetFloatAttr(vNode, FLD_LAT)),
                      db_Par(FLD_LON,        xml_GetFloatAttr (vNode, FLD_LON)),
                      db_Par(FLD_VALUE,      xml_GetIntAttr (vNode, FLD_VALUE))
                      ]);
      end;
   end;
*)

 (*  //-------------------------------------------------------------------
   procedure DoLoadPointLists(aNode: IXMLNode);
   //-------------------------------------------------------------------
   var i,iID: integer;   vNode1,vNode: IXMLNode;
   begin
      vNode1:= xml_GetNodeByPath(aNode, //gl_XMLDoc.DocumentElement,
                                ['POINT_GROUPS']);

      if VarExists(vNode1) then
        for i := 0 to vNode1.ChildNodes.Length - 1 do
      begin
        vNode:=vNode1.ChildNodes.Item(i);
        iID  :=xml_GetIntAttr (vNode, FLD_ID);

        db_AddRecord (mem_Point_Groups,
                      [db_Par(FLD_ID,        iID),
                       db_Par(FLD_map,       xml_GetAttr (vNode, 'map') ),
                       db_Par(FLD_step,      xml_GetIntAttr (vNode, 'step') ),
                       db_Par(FLD_FRAME,     AsBoolean(xml_GetAttr (vNode, 'frame')) ),
                       db_Par(FLD_SCHEMA_ID, xml_GetAttr (vNode, 'Schema'))
                       ]);
        DoLoadPoints (vNode, iID);
      end;
   end;
*)




//-----------------------------------------------------------------
procedure TdmParams_bin_to_MIF.PrepareCellsColorRanges;
//-----------------------------------------------------------------
var i: integer;
  sCaption: string;
begin
{
  SetLength (Ranges, mem_Cells.RecordCount);
  i := 0;

  mem_Cells.First;

  with mem_Cells do
    while not EOF do
    begin
      Ranges[i].Color    := FieldValues[FLD_COLOR];
      Ranges[i].MinValue := FieldValues[FLD_ID];
      Ranges[i].MaxValue := FieldValues[FLD_ID];
      Ranges[i].Type_    := rtFixed;// DEF_FIXED_COLOR;

      if Ranges[i].MaxValue<>Ranges[i].MinValue then
        sCaption:=Format('(%d) - (%d)', [Ranges[i].MinValue, Ranges[i].MaxValue])
      else
        sCaption:=Format('%d', [Ranges[i].MinValue]);

    //  Ranges[i].Caption := sCaption;

      i:=i+1;
      Next;
    end;}

end;

//-----------------------------------------------------------------
//procedure TdmParams_bin_to_MIF.PrepareStatisticColorRanges;
//-----------------------------------------------------------------
//var i, iColor: integer;
//      dValue : double;
//      S: string;
//begin
//{
//  mem_Cells.First;
//
//  with mem_Cells do
//    while not EOF do
//    begin
//      dValue := mem_Cells[FLD_VALUE];
//      iColor := GetColor(dValue, i);
//
//      Edit;
//      mem_Cells[FLD_COLOR] := iColor;
//      Post;
//
//      Next;
//    end;
//
//  SetLength (Ranges, mem_Cells.RecordCount);
//  i := 0;
//
//  mem_Cells.First;
//
//  with mem_Cells do
//    while not EOF do
//    begin
//      Ranges[i].Color    := mem_Cells[FLD_COLOR];
//      Ranges[i].MinValue := mem_Cells[FLD_ID];
//      Ranges[i].MaxValue := mem_Cells[FLD_ID];
//      Ranges[i].Type_    := rtFixed;// DEF_FIXED_COLOR;
//      i:=i+1;
//      Next;
//    end;}
//
//end;





  {
//-------------------------------------------------------------------
procedure TdmParams.SaveColorSchemaToIni(aIniFileName, aMatrixFileName: string);
//-------------------------------------------------------------------
var
  sSection: string;
  i: integer;
  ini: TmemIniFile;

begin
  ini:=TmemIniFile.Create (ChangeFileExt(aIniFileName, '.ini'));

  Ini.WriteString ('main','matrix',aMatrixFileName );
  Ini.WriteInteger('main','count', Length (Ranges) );

  for i := 0 to Length (Ranges) - 1 do
  begin
    sSection:='Range_'+IntToStr (i+1);

    Ini.WriteInteger(sSection,'MinValue',Ranges[i].MinValue );
    Ini.WriteInteger(sSection,'MaxValue',Ranges[i].MaxValue );
    Ini.WriteInteger(sSection,'Type',    Integer(Ranges[i].Type_) );

    if Ranges[i].Type_=rtFixed then
      Ini.WriteInteger(sSection,'Color',  Ranges[i].Color )
    else begin
      Ini.WriteInteger(sSection,'MaxColor',Ranges[i].MaxColor );
      Ini.WriteInteger(sSection,'MinColor',Ranges[i].MinColor );

    //  Ini.WriteInteger(sSection,'Color',  Ranges[i].Color );
   //   Ini.WriteString (sSection,'Caption',Ranges[i].Caption );
    end;

  end;

  Ini.UpdateFile;
  Ini.Free;

end;
    }

