unit dm_Main__bin_to_MIF;

interface

uses
  Classes, Forms, SysUtils, Dialogs,

  //u_BinToMap_controller,

 // u_vars,

  u_reg,
  u_log,

  u_ini_Params_Bin_to_Map,     
//  dm_Params_bin_to_mif,
  u_BinToMap_mif
  ;

type
  TdmMain_bin_to_mif = class(TDataModule)
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private

    FBinToMap: TBinToMap_MIF;
    FTerminated: boolean;

    procedure DoProcessException(Sender: TObject; E: Exception);
  end;

var
  dmMain_bin_to_mif: TdmMain_bin_to_mif;

//========================================================
implementation {$R *.DFM}
//=================================================================
//const

// TEMP_FILENAME='e:\test\bin_to_mif.xml';

//  TEMP_FILENAME='c:\temp\rpls\bin_to_mif.xml';
//  TEMP_FILENAME='bin_to_map.xml';

//  TEMP_FILENAME='C:\Tele2\Temp\bin_to_mif.xml';

//  TEmp_FILENAME = 'C:\Tele2\CalcResults\bin_to_mif.xml';

//  TEmp_FILENAME = 'C:\Users\Alexey\AppData\Roaming\RPLS_DB_LINK\bin_to_mif.xml';
//  TEmp_FILENAME = 'C:\Users\Alexey\AppData\Roaming\RPLS_DB_LINK\bin_to_map.xml';

//  TEmp_FILENAME = 'D:\_panorama_data\_kup\bin_to_mif.temp.xml';

//  TEMP_FILENAME = 'W:\GIS\KUP_to_GeoTiff\exe\bin_to_mif.temp.xml';


//  TEMP_FILENAME='C:\Documents and Settings\alex.MAXSELECT\Local Settings\Application Data\RPLS_DB_LINK\bin_to_mif.xml';


//   TEMP_FILENAME='e:\test\in1.xml';


//  DEF_SOFTWARE_NAME= 'RPLS_DB_LINK';


(*const
  REGISTRY_COMMON_FORMS = 'Software\Onega\Common\Forms_05\';
*)

(*
function vars_Form_GetRegPath(aClassName: string): string;
begin
  Result := REGISTRY_COMMON_FORMS + aClassName +'\'
end;
*)




//-----------------------------------------------------------------
procedure TdmMain_bin_to_mif.DataModuleCreate(Sender: TObject);
//-----------------------------------------------------------------

const
  TEMP_FILENAME1 = 'bin_to_mif.xml';
//  TEMP_FILENAME = 'bin_to_mif.xml';


//  TEMP_FILENAME ='C:\Users\m.garbar\AppData\Local\RPLS_DB_LINK\(local).onega_link_2019.1\Predictions\region.1\bin_to_mif.xml';
//  TEMP_FILENAME ='C:\Users\Alexey\AppData\Local\RPLS_DB_LINK\bin_to_mif.xml';

//  TEMP_FILENAME ='D:\megaf_kup\bin_to_mif_1.xml';

  TEMP_FILENAME ='C:\Users\Alexey\AppData\Local\RPLS_DB_LINK\bin_to_map.xml';
  
  



var
  sXmlFileName: string;
  i: integer;
 // oBinToMap_controller: TBinToMap_controller;
  sApplicationDataDir: string;
 // MultiImage1: TPMultiImage;

begin
 // sXmlFileName:=ParamStr(1);

  Application.OnException:= DoProcessException;


  g_Params_bin_to_Map:=TParams_Bin_to_Map1.Create;



  sXmlFileName:=ParamStr(1);
  if sXmlFileName='' then
    sXmlFileName := g_ApplicationDataDir + TEMP_FILENAME;

  //  sXmlFileName:=TEmp_FILENAME;
//    sXmlFileName:=g_ApplicationDataDir+ DEF_TEMP_FILENAME;





  if not FileExists(sXmlFileName) then
  begin
    sXmlFileName:=TEMP_FILENAME;


//    sApplicationDataDir := GetCommonApplicationDataDir_Local_AppData(DEF_SOFTWARE_NAME);

  //  sXmlFileName:=sApplicationDataDir + TEMP_FILENAME;
  end;

////////  ShellExec_Notepad (sXmlFileName);


//  ShowMessage(sXmlFileName);


 // ShellExec_Notepad_temp1(sXmlFileName);


  if not g_Params_bin_to_Map.LoadFromXMLFile (sXmlFileName) then
  begin
    ShowMessage('Error file load: ' + sXmlFileName);
    exit;
  end;

//  if not FileExists(sXmlFileName) then
  //  exit;



 // Application.CreateForm(TdmParams_bin_to_MIF, dmParams_bin_to_MIF);

  assert(g_Params_bin_to_Map.BinMatrixArrayList.Count>0);

  if g_Params_bin_to_Map.BinMatrixArrayList.Count=0 then
    Exit;

(*
  oBinToMap_controller:=TBinToMap_controller.Create;
  oBinToMap_controller.ExecuteProc;
  FreeAndNil(oBinToMap_controller);
*)


  FBinToMap:= TBinToMap_MIF.Create;
  FTerminated:= not FBinToMap.ExecuteDlg ('���������� ����...');
  FreeAndNil(FBinToMap);



//  FBinToMap.Free;

  ini_WriteBool(ChangeFileExt(sXmlFileName,'.ini'), 'main', 'result', not FTerminated);

/////  ExitProcess(IIF(FTerminated, 1, 0));

end;


procedure TdmMain_bin_to_mif.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(g_Params_bin_to_Map);
end;



//--------------------------------------------------------------
procedure TdmMain_bin_to_mif.DoProcessException(Sender: TObject; E: Exception);
//--------------------------------------------------------------
begin
  FTerminated:= true;
  g_log.AddError('TdmMain.DoProcessException', E.Message);

end;



end.

   {
'C:\Documents and Settings\alex.MAXSELECT\Local Settings\Application Data\RPLS_DB_LINK\bin_to_map.xml'
