unit u_BinToMap_controller;

interface 

uses SysUtils,Classes,

//  u_BinToMap_classes,

  u_Params_Bin_To_Map_classes,

  u_Params_bin_to_map,
//  dm_Params_bin_to_mif,

  u_GEO,
  
  u_log,
  

  u_Matrix,
  u_Matrix_base,

  u_MatrixDecomposer
  ;


type
  TBinToMap_controller = class(TObject)
  private
    FMatrixDecomposer : TMatrixDecomposer; // ������� � �������
    FColorMatrix      : TIntMatrix;
    FValuesMatrix     : TOrdinalMatrix;

    FMapType: (mtKgr, mtKup, mtOther);

    FMatrixFileName: string;
    FMapFileName   : string;

    FColorSchemaID : integer;
    FCaption       : string;

  private
    procedure DeleteColorMatrix;
    function ExecuteBinMatrix: Boolean;

    procedure DoLog(aMsg: string);
    function PrepareColorMatrix(aMatrix: TOrdinalMatrix): boolean;
  public
    procedure ExecuteProc;
  end;

implementation



//------------------------------------------------------
procedure TBinToMap_controller.DeleteColorMatrix;
//------------------------------------------------------
var
  sName1,  sName: string;
begin

{$IFNDEF debug}

  sName := ChangeFileExt(FMapFileName,'_color_matrix');
 // sName2:= sName+'_mirror';
  sName1:= sName+'.bin';
 // sName3:= sName2+'.bin';

  SysUtils.DeleteFile(sName);
  SysUtils.DeleteFile(sName1);
  //SysUtils.DeleteFile(sName2);
 // SysUtils.DeleteFile(sName3);

{$ENDIF}

end;

procedure TBinToMap_controller.DoLog(aMsg: string);
begin

end;

//------------------------------------------------------
function TBinToMap_controller.ExecuteBinMatrix: Boolean;
//------------------------------------------------------
var // oMatrix : TOrdinalMatrix;
  sImgFileName: string;
  blBounds: TBLRect;
  sExt: string;
  sFile: string;

   // bool: boolean;

begin
{  FMatrixFileName:='f:\_los\los.int';
  FMapFileName:='f:\_los\los.tab';
}
  //oMatrix:=matrix_CreateMatrixByFileName (FMatrixFileName);

  FValuesMatrix:=Matrix_CreateMatrixByFileName (FMatrixFileName);

  if not Assigned(FValuesMatrix) then
  begin
//    raise Exception.Create('�� ������� �������' + FMatrixFileName );
  //  g_Log.AddError('TdmMain.DoProcessException', E.Message);
    g_Log.AddError('TBinToMap_controller.ExecuteBinMatrix', '�� ������� �������' + FMatrixFileName);

    result := False;
    exit;
//    ExitProcess(1);
  end;

    //Exit;

  DoLog ('');
  DoLog ('������ ����� :' + FMatrixFileName);

  if not FValuesMatrix.LoadFromFile (FMatrixFileName, fmOpenReadWrite) then
  begin
    DoLog ('���� �� ����������: ' + FMatrixFileName);
  end
  else begin
    DoLog (Format('�������: %s;  ����� : %s',
           [FMatrixFileName, FMapFileName]));

    sExt:=LowerCase(ExtractFileExt (FMatrixFileName));

    if Pos('kgr', FMatrixFileName)>0 then FMapType:= mtKgr else
    if Pos('kup', FMatrixFileName)>0 then FMapType:= mtKup
                                     else FMapType:= mtOther;


    // ���� �������, ��� ��� ���� ����� !!!!!
    if (sExt='.kgr')  then //and (not dmParams_bin_to_MIF.FIsStatistic)
    begin
      g_Params_bin_to_Map.ColorSourceType1:=csCells;

   //   dmParams.PrepareCellsColorRanges();
    //  dmParams.SaveCellsSchemaXmlFile(FMatrixFileName, FMapFileName);
    end
    else begin
      g_Params_bin_to_Map.ColorSourceType1:=csRanges;
      g_Params_bin_to_Map.SetColorSchema (FColorSchemaID);

//      dmParams_bin_to_MIF.SaveColorSchemaXmlFile (FColorSchemaID, FMatrixFileName, FMapFileName);
    //  dmParams.SaveColorSchemaToIni (ChangeFileExt(FMapFileName, '.ini'), FMatrixFileName);

    end;

 //   if (dmParams_bin_to_MIF.FIsStatistic) then
  //     dmParams_bin_to_MIF.PrepareStatisticColorRanges();

    if g_Params_bin_to_Map.MapType = mtRaster then
    begin
   ////////////   SaveMatrixToRasterFile_MIF (FMapFileName);
    end else

//    FMatrix:= oMatrix;

    if PrepareColorMatrix (FValuesMatrix) then// begin
///////////////////////
  //  if sExt='.kgr' then PrepareRangeMatrix (FValuesMatrix);
/////////    PrepareRangeMatrix (oMatrix); //if sExt='.kgr' then


      case g_Params_bin_to_Map.MapType of

        mtRaster:
        begin
         // SaveColorMatrixToRasterFile (FMapFileName); // raster map

        end;


        mtKML,
        mtVector: begin
                 //   sImgFileName:= ChangeFileExt(FMapFileName, '_raster'+DEF_RASTER_EXT);
                  //  SaveColorMatrixToRasterFile (sImgFileName); // raster map

                ///////    SaveColorMatrixToVectorFile (FMatrixFileName,FMapFileName); // vector map


                  end;
      end;


    // ---------------------------------------------------------------
    sFile := ChangeFileExt(FMapFileName, '.colors.xml');

    case g_Params_bin_to_Map.ColorSourceType1 of
      csCells:  ;
      csRanges: g_Params_bin_to_Map.ColorSchema_Selected.SaveToXML(sFile);
    end;

  //  end;
  end;

  FValuesMatrix.Free;
  FColorMatrix.ClearMemory;

 // FMatrixDecomposer.ClearMemory;

  DeleteColorMatrix();
  result := True;
end;

//------------------------------------------------------
procedure TBinToMap_controller.ExecuteProc;
//------------------------------------------------------
var
  I: Integer;
  oMatrixFile: TBinMatrixFile;
  iCount: integer;
begin
 /////// FMatrixDecomposer.OnProgress2:=OnProgress2;

 // dmParams.mem_Matrix.First;
  //dmParams.mem_Point_Groups.First;

  //------------------------------------------------------
  // Bin Matrix
  //------------------------------------------------------
  iCount:=g_Params_bin_to_Map.MatrixArrayList.Count;

  for I := 0 to iCount-1 do
  begin

//  end;

//  with dmParams.mem_Matrix do
//   while (not EOF) and (not Terminated) do
 // begin
//////////    DoProgress (i, iCount);

    oMatrixFile := g_Params_bin_to_Map.MatrixArrayList[i];

    FMatrixFileName:= oMatrixFile.MatrixFILENAME;
    FMapFileName   := oMatrixFile.MapFileName;
    FColorSchemaID := oMatrixFile.ColorSchema_ID;
//      FMapKind       := FieldByName(FLD_MAP_KIND).AsInteger;
    FCaption       := oMatrixFile.CAPTION;

    if not ExecuteBinMatrix () then
      exit;

  //  Next;
  end;

end;

//------------------------------------------------------
function TBinToMap_controller.PrepareColorMatrix(aMatrix: TOrdinalMatrix):
    boolean;
//------------------------------------------------------
var r,c: integer;
  iRange: integer;
  i: Integer;
    dValue : Double;
    sFileName : string;

  iColor: integer;
begin
  DoLog ('Preparing ColorMatrix ...');

  Result:= false;

  //��������� ������� ������
  sFileName := ChangeFileExt(FMapFileName,'_color_matrix');

{  CreateIntMatrix(sFileName,
                  aMatrix.TopLeft.X, aMatrix.TopLeft.Y,
                  aMatrix.RowCount, aMatrix.ColCount,
                  aMatrix.Step, aMatrix.ZoneNum);
}

  FColorMatrix.FileName:=sFileName;
  FColorMatrix.SetParams (aMatrix.TopLeft.X, aMatrix.TopLeft.Y,
                          aMatrix.RowCount, aMatrix.ColCount,
                          aMatrix.Step, aMatrix.ZoneNum);

  //FRangeMatrix.SetMatrixSize(aMatrix.RowCount, aMatrix.ColCount);

//  FColorMatrix.LoadFromFile (sFileName, fmOpenReadWrite);
//  FColorMatrix.LoadFromFile (sFileName, fmOpenRead);

  FColorMatrix.FillBlank;
//  FRangeMatrix.FillBlank;

 //////// Progress2Step:=100;

  with aMatrix do
    for r:=0 to RowCount-1 do
    begin
      for c:=0 to ColCount-1 do
        if (not IsNull(r,c)) then
      begin

(*        if Terminated then begin
          Result:= false;
          exit;
        end;
*)


        if aMatrix.RecordType = dtFloat then
          dValue:= aMatrix.ItemsAsFloat[r,c]
        else
          dValue:= Items[r,c];

        iColor:= g_Params_bin_to_Map.GetColor (dValue,iRange);

        FColorMatrix[r,c]:= iColor;

     //   FRangeMatrix[r,c]:= iRange;

        i:=FColorMatrix[r,c];
      end;
//      if c=0 then
    ////////  DoProgress2(r, RowCount);
    end;

  FColorMatrix.SaveToFile (sFileName);

//  FColorMatrix.SaveToTxtFile (sFileName);

  Result:= true;
end;


end.
