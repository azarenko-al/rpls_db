unit u_MapMaker_base;

interface
uses SysUtils,Windows,graphics,

     dm_Params,
     u_GEO,
     u_geo_classes,
//     u_geo_mif,

     u_func,
     u_func_img,
     u_files,
     u_func_db,


     u_Matrix,
     u_progress,
  //   d_Progress_adv,

     u_MatrixDecomposer
     ;

type
  //------------------------------------------------------
  // ������������ ����� DM �� 2D ��������� �������
  //------------------------------------------------------
  TBinToMap = class (TProgress)
  //------------------------------------------------------
  protected
    FMatrixDecomposer : TMatrixDecomposer; // ������� � �������
    FColorMatrix      : TIntMatrix;
    FRangeMatrix      : TIntMatrix; //������� �������� ���� �������

//    FMapExtension: string; //'.tab; .dm'

  private

    procedure PrepareColorMatrix (aMatrix: TOrdinalMatrix);
    procedure PrepareRangeMatrix (aMatrix: TOrdinalMatrix);

    procedure ExecuteMatrix ();

    procedure ExecutePointGroup (aID: integer;
                                 aMapFileName: string;
                                 aStep: integer;
                                 aIsFramed: boolean);
  protected
    FMatrixFileName: string;
    FColorSchemaID : integer;

    FMapFileName   : string;
//    FMapType       : (mtVectorMap,mtRaster);

    procedure SaveRangeMatrixToFile (aFileName: string); virtual; abstract;
    procedure SaveColorMatrixToVectorFile (aFileName: string);  virtual; abstract;
    procedure SaveColorMatrixToRasterFile (aFileName: string);  virtual;

//    function  SaveColorMatrixToRasterFile (aFileName: string): string; virtual;
    procedure SaveColorMatrixToBitmap (aBitmap: TBitmap);

    function  OpenMap (aMapFileName: string; aBLBounds: TBLRect): boolean; virtual; abstract;
    procedure CloseMap (); virtual; abstract;

    procedure DoOnMapNewBLShape (Sender: TObject;
                                 aBLPoints: TBLPointList;
                                 aFillValue: integer); virtual; abstract;

    procedure DrawSquare (aBLPoint: TblPoint;
                          aColor: integer;
                          aStep: integer;
                          aIsFramed: boolean); virtual; abstract;

    procedure ExecuteProc (); override;
  public

    constructor Create;
    destructor Destroy; override;
  end;


const
   DEF_RASTER_EXT = '.gif';


//======================================================
implementation
//======================================================

//------------------------------------------------------
constructor TBinToMap.Create;
//------------------------------------------------------
begin
  inherited;

  FColorMatrix:=TIntMatrix.Create;
  FRangeMatrix:=TIntMatrix.Create;

  FMatrixDecomposer:=TMatrixDecomposer.Create;
  FMatrixDecomposer.OnNewBLShape:=DoOnMapNewBLShape;

  FMatrixDecomposer.Progress2Step:=50;
end;


destructor TBinToMap.Destroy;
begin
  FColorMatrix.Free;
  FRangeMatrix.Free;
  FMatrixDecomposer.Free;
  inherited;
end;

{
procedure TBinToMap.DoOnProgress2 (aPosition, aTotal: integer; var aTerminated: boolean);
begin
  DoProgress2 (aPosition, aTotal, aTerminated);
end;
}
(*
//-----------------------------------------------------
procedure TBinToMap.DoOnMapNewBLShape (aBLPointArr: TBLPointArray; aFillValue: integer);
//-----------------------------------------------------
// ���������� ��� ������������ ������� �������
begin
{
  if aFillValue>=0 then
    FRangeRegions[aFillValue].AddPolygon (blPoints);
}
end;
*)

//------------------------------------------------------
procedure TBinToMap.SaveColorMatrixToRasterFile (aFileName: string);
//------------------------------------------------------
var oPicture : TPicture;
  sBmpFileName: string;
begin
//  sBmpFileName:=ChangeFileExt(aFileName, DEF_RASTER_EXT);

  oPicture := TPicture.Create;
  SaveColorMatrixToBitmap (oPicture.Bitmap);
  img_SaveBitmapToFile (oPicture.Bitmap, aFileName); //sBmpFileName);
  oPicture.Free;
end;


//------------------------------------------------------
procedure TBinToMap.SaveColorMatrixToBitmap (aBitmap: TBitmap);
//------------------------------------------------------
var iValue,r,c : integer;
begin
  aBitmap.Transparent:=True;
  aBitmap.TransparentMode:=tmFixed;
  aBitmap.TransparentColor:=clWhite;

  aBitmap.PixelFormat := pf32bit;
  aBitmap.Width  := FColorMatrix.ColCount;
  aBitmap.Height := FColorMatrix.RowCount;

  with FColorMatrix do
    for r:=0 to RowCount-1 do
    for c:=0 to ColCount-1 do
    begin
      iValue:=FColorMatrix[r,c];
      if iValue=FColorMatrix.BlankValue then
        iValue:=clBlack;

      aBitmap.Canvas.Pixels[c,r]:=iValue;
    end;
end;

//------------------------------------------------------
procedure TBinToMap.PrepareColorMatrix (aMatrix: TOrdinalMatrix);
//------------------------------------------------------
var r,c,iValue: integer;
begin
  //��������� ������� ������
  FColorMatrix.AssignHeader (aMatrix);
  FColorMatrix.FillBlank;
  
  with aMatrix do
  for r:=0 to RowCount-1 do
  for c:=0 to ColCount-1 do if not IsNull(r,c) then
  begin
    iValue:=Items[r,c];

    FColorMatrix[r,c]:=dmParams.GetColor (iValue);

    if (r mod 100) = 0 then
      DoProgress2(r, RowCount);
  end;
end;

//------------------------------------------------------
procedure TBinToMap.PrepareRangeMatrix (aMatrix: TOrdinalMatrix);
//------------------------------------------------------
var r,c,iValue: integer;
begin
  //��������� ������� ������
  FRangeMatrix.AssignHeader (aMatrix);
  FRangeMatrix.FillBlank;

//  Progress2Step:=100;

  with aMatrix do
  for r:=0 to RowCount-1 do
  for c:=0 to ColCount-1 do if not IsNull(r,c) then
  begin
    iValue:=Items[r,c];

    FRangeMatrix[r,c]:=dmParams.GetRangeIndex (iValue);

    if (r mod 100) = 0 then
      DoProgress2(r, RowCount);
  end;
end;


//------------------------------------------------------
procedure TBinToMap.ExecutePointGroup (aID: integer;
                                  aMapFileName: string;
                                  aStep: integer;
                                  aIsFramed: boolean);
//------------------------------------------------------
var blBounds: TBLRect;
    blPoint: TBLPoint;
    iCount,iColor,iValue: integer;
begin
  blBounds:=dmParams.GetPointListBounds (aID);
  iCount:=0;

  OpenMap (aMapFileName, blBounds);

  with dmParams.mem_Points do begin
    First;
    while not EOF do
    begin
       //only items for group
       if FieldValues[FLD_PARENT_ID]=aID then
       begin
         blPoint:=db_ExtractBLPoint (dmParams.mem_Points);
         iValue:=FieldValues[FLD_VALUE];
         iColor:=dmParams.GetColor (iValue);

         DrawSquare (blPoint,iColor,aStep,aIsFramed);

         geo_UpdateRectForPoint (blPoint, blBounds);

//         DoOnProgress2 (iCount, RecordCount, Terminated);
  //       Inc(iCount);
       end;

       Next;
    end;
  end;

  CloseMap ();

end;

//------------------------------------------------------
procedure TBinToMap.ExecuteMatrix ();
//------------------------------------------------------
var  oMatrix : TOrdinalMatrix;
    blBounds: TBLRect;
    sExt: string;
    bool: boolean;

begin
  oMatrix:=matrix_CreateMatrixByFileName (FMatrixFileName);
  if not Assigned(oMatrix) then
    raise Exception.Create('');
    //Exit;

  DoLog ('������ ����� :' + FMatrixFileName);
  if not oMatrix.LoadFromFile (FMatrixFileName) then begin
    DoLog ('���� �� ����������: ' + FMatrixFileName);
  end
  else begin
    DoLog (Format('�������: %s;  ����� : %s',
           [FMatrixFileName, FMapFileName]));

    sExt:=LowerCase(ExtractFileExt (FMatrixFileName));

    if sExt='.kgr' then
      bool:=dmParams.PrepareCellsColorRanges()
    else
      bool:=dmParams.SetColorSchema (FColorSchemaID);

    PrepareColorMatrix (oMatrix);
    PrepareRangeMatrix (oMatrix); //if sExt='.kgr' then

    case dmParams.MapType of
      mtRaster: begin // for raster
                //  PrepareColorMatrix (oMatrix);
                  SaveColorMatrixToRasterFile (FMapFileName);
                end;
      mtVector: begin // for vector map
//                  PrepareRangeMatrix (oMatrix);

                  SaveColorMatrixToVectorFile (FMapFileName);

                  {
                  if sExt='.kgr' then
                    SaveRangeMatrixToFile (FMapFileName)
                  else
                    SaveColorMatrixToFile (FMapFileName);
                   }
                   
                  //   PrepareRangeMatrix()

                 // PrepareColorMatrix (oMatrix);

                end;
    end;

//    if sExt=FMapExtension then //'.tab' then
//    SaveColorMatrixToMIF (Params.MapFileName);
  end;

  oMatrix.Free;
end;


//------------------------------------------------------
procedure TBinToMap.ExecuteProc;
//------------------------------------------------------
begin
  FMatrixDecomposer.OnProgress2:=OnProgress2;

  dmParams.mem_Matrix.First;
  dmParams.mem_Point_Groups.First;

  //------------------------------------------------------
  // Bin Matrix
  //------------------------------------------------------
  with dmParams.mem_Matrix do
    while (not EOF) and (not Terminated) do
    begin
      DoProgress (RecNo, RecordCount);

      FMatrixFileName:=FieldValues['FileName'];
      FMapFileName   :=FieldValues['Map'];
      FColorSchemaID :=FieldByName(FLD_SCHEMA_ID).AsInteger;

      ExecuteMatrix ();

      Next;
    end;

  //------------------------------------------------------
  // Point_Groups
  //------------------------------------------------------
  with dmParams.mem_Point_Groups do
    while (not EOF) and (not Terminated) do
    begin
      DoProgress (RecNo, RecordCount);

      dmParams.SetColorSchema (FieldByName(FLD_SCHEMA_ID).AsInteger);
      ExecutePointGroup (FieldValues[FLD_ID],
                         FieldValues['Map'],
                         FieldByName('step').AsInteger,
                         FieldByName(FLD_FRAME).AsBoolean
                         );
      Next;
    end;

end;



end.



