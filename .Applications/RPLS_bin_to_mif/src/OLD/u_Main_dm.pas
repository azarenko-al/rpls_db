unit u_Main_dm;

interface
uses SysUtils,Windows,graphics,

     dm_Params,
     u_GEO,
     u_func,
     u_func_img,
     u_func_db,

     u_files,
     u_func_raster,

     u_Matrix,
     u_classes,
     d_Progress_adv,

     u_MatrixDecomposer,
     u_Neva_Func
     ;

type
  //------------------------------------------------------
  // ������������ ����� DM �� 2D ��������� �������
  //------------------------------------------------------
  TMapMaker = class (TProgress)
  //------------------------------------------------------
  private
    FMatrixDecomposer : TMatrixDecomposer; // ������� � �������
    FColorMatrix      : TIntMatrix;

    procedure DoOnProgress2 (aPosition, aTotal: integer; var aTerminated: boolean);
    procedure DoOnMapNewShape (Sender: TObject;
                               aShapeLine: TMatrixPolyline; aFillValue: integer);

    procedure ProcessMatrix (aMatrix:TOrdinalMatrix; aMapFileName: string);
    procedure PrepareColorMatrix (aMatrix: TOrdinalMatrix);

    procedure SaveColorMatrixToBitmap (aBitmap: TBitmap);

    procedure SaveColorMatrixToMIF (aFileName: string);
    procedure ProcessPointGroup (aID: integer;
                                 aMapFileName: string;
                                 aStep: integer;
                                 aIsFramed: boolean);

  public
    constructor Create;
    destructor Destroy; override;

    function Execute: boolean;
    procedure Execute_Points();

  end;


//======================================================
implementation
//======================================================

const
   MCODE_REGION    = 20000010;  //��� ������ ������������
   MCODE_MEAS_AREA = 20000012;  //��� �������� ���������


//-------------------------------------------------------------------
constructor TMapMaker.Create;
//-------------------------------------------------------------------
begin
  inherited;

  FColorMatrix:=TIntMatrix.Create;

  FMatrixDecomposer:=TMatrixDecomposer.Create;
  FMatrixDecomposer.OnNewBLShape:=DoOnMapNewShape;
  FMatrixDecomposer.OnProgress2:=DoOnProgress2;

  FMatrixDecomposer.Progress2Step:=50;
end;


destructor TMapMaker.Destroy;
begin
  FColorMatrix.Free;
  FMatrixDecomposer.Free;
  inherited;
end;


procedure TMapMaker.DoOnProgress2 (aPosition, aTotal: integer; var aTerminated: boolean);
begin
  DoProgress2 (aPosition, aTotal, aTerminated);
end;

//------------------------------------------------------
procedure TMapMaker.DoOnMapNewShape (Sender: TObject;
                                 aShapeLine: TMatrixPolyline; aFillValue:integer);
//------------------------------------------------------
var neva_cl,iBrush,cl,i: integer;
    blPoints: TBLPointArray;
    // in the matrix - to draw the map; min and max values
begin
  SetLength (blPoints, aShapeLine.Count);

  for i:=0 to AShapeLine.Count-1 do begin
    blPoints[i].B := aShapeLine[i].B; //X;
    blPoints[i].L := aShapeLine[i].L; //Y;
  end;

  iBrush:=NEVA_PATTERN_SOLID;

  cl:=aFillValue;

  {
  if FOptions.IsGradientFill then begin
    cl:=aFillValue;
  end else begin
    if aFillValue <> -1 then begin
                           cl:=FOptions.ColorValues[aFillValue].Color;
                           if FOptions.ColorValues[aFillValue].IsTransparent then
                             iBrush:=NEVA_PATTERN_DOTS;
                        end
                        else cl:=0;
  end;
}
  // don't show black color
  if cl<>0 then begin
    neva_Insert_BLPolygone (MCODE_REGION, blPoints, cl, iBrush, false);
  end;
end;

//------------------------------------------------------
procedure TMapMaker.SaveColorMatrixToBitmap (aBitmap: TBitmap);
//------------------------------------------------------
var iValue,r,c : integer;
begin
  aBitmap.PixelFormat := pf32bit;
  aBitmap.Width  := FColorMatrix.ColCount;
  aBitmap.Height := FColorMatrix.RowCount;

  with FColorMatrix do
  for r:=0 to RowCount-1 do
    for c:=0 to ColCount-1 do
    begin
      iValue:=FColorMatrix[r,c];
      if iValue=FColorMatrix.BlankValue then
        iValue:=clBlack;

      aBitmap.Canvas.Pixels[c,r] := iValue;
    end;
end;


//------------------------------------------------------
procedure TMapMaker.SaveColorMatrixToMIF (aFileName: string);
//------------------------------------------------------
var oPicture : TPicture;
  sBmpFileName: string;
begin
  sBmpFileName:=ChangeFileExt(aFileName, '.bmp');

  oPicture := TPicture.Create;

  SaveColorMatrixToBitmap (oPicture.Bitmap);

  oPicture.SaveToFile (sBmpFileName);
  oPicture.Destroy;

  SaveStringToFile(geo_MakeXYHeaderForMIF (sBmpFileName,
                  FColorMatrix.TopLeft,
                  FColorMatrix.RowCount, FColorMatrix.ColCount,
                  FColorMatrix.Step,     FColorMatrix.Step),
                  ChangeFileExt (aFileName, '.tab'));
end;

//------------------------------------------------------
procedure TMapMaker.ProcessMatrix (aMatrix: TOrdinalMatrix; aMapFileName: string);
//------------------------------------------------------
var blPoints: TBLPointArray;
begin
  if not neva_Open_Map(aMapFileName, False) then begin
    DoLog('������ ��� �������� �����: '+aMapFileName); Exit;
  end;

//  Log('�������� ������� ��� ��������...');
//  CutPolygon();
  DoLog ('������ ������������ �����: '+ TimeToStr(Now));

  neva_Goto_Down();

  FMatrixDecomposer.Decompose(aMatrix);
  DoLog ('����� ������������ �����: ' + TimeToStr(Now));
  DoLog (Format('����� / c����������� ����������: %d / %d', [aMatrix.RowCount*aMatrix.ColCount, FMatrixDecomposer.ShapesTotal]));

  neva_Close_Map;
end;

//------------------------------------------------------
procedure TMapMaker.PrepareColorMatrix (aMatrix: TOrdinalMatrix);
//------------------------------------------------------
var r,c,iValue: integer;
begin
  //��������� ������� ������
  FColorMatrix.AssignHeader (aMatrix);
  FColorMatrix.FillBlank;

  with aMatrix do
  for r:=0 to RowCount-1 do
  for c:=0 to ColCount-1 do if not IsNull(r,c) then
  begin
    iValue:=Items[r,c];

    FColorMatrix[r,c]:=dmParams.GetColor (iValue);

    if (r mod 100) = 0 then
      DoProgress2(r,RowCount, Terminated);
  end;
end;


//------------------------------------------------------
procedure TMapMaker.ProcessPointGroup (aID: integer;
                                  aMapFileName: string;
                                  aStep: integer;
                                  aIsFramed: boolean);
//------------------------------------------------------
var blBounds: TBLRect;
    blPoint: TBLPoint;
    iCount,iColor,iValue: integer;
begin
  blBounds:=dmParams.GetPointListBounds (aID);
  iCount:=0;

  neva_Create_Map (aMapFileName, '100', blBounds);
  neva_Open_Map (aMapFileName);

  with dmParams.mem_Points do begin
    First;
    while not EOF do
    begin
       if FieldValues[FLD_PARENT_ID]=aID then
       begin
         blPoint:=db_ExtractBLPoint (dmParams.mem_Points);
         iValue:=FieldValues[FLD_VALUE];
         iColor:=dmParams.GetColor (iValue);

         neva_Insert_Square (MCODE_MEAS_AREA, blPoint, aStep, iColor, false);

         if aIsFramed then
           neva_Insert_Frame  (MCODE_MEAS_AREA, blPoint, aStep);

         geo_UpdateRectForPoint (blPoint, blBounds);

         DoOnProgress2 (iCount, RecordCount, Terminated);
         Inc(iCount);
       end;

       Next;
    end;
  end;

   neva_Close_Map;    
end;


//------------------------------------------------------
function TMapMaker.Execute(): boolean;
//------------------------------------------------------

    //------------------------------------------------------
    procedure DoExecMatrix (aExt, aMatrixFileName, aMapFileName: string);
    //------------------------------------------------------
    var  oMatrix : TOrdinalMatrix;
        blBounds: TBLRect;
        sExt: string;
    begin
      oMatrix:=matrix_CreateMatrixByFileName (aMatrixFileName);
      if not Assigned(oMatrix) then
        raise Exception.Create('');
        //Exit;

      DoLog ('������ ����� :' + aMatrixFileName);
      if not oMatrix.LoadFromFile (aMatrixFileName) then
        DoLog ('���� �� ����������: ' + aMatrixFileName)

      else begin
        DoLog (Format('�������: %s;  ����� DM: %s', [aMatrixFileName,aMapFileName]));

        PrepareColorMatrix (oMatrix);

        sExt:=LowerCase(ExtractFileExt (aMapFileName));

        if sExt='.tab' then
          SaveColorMatrixToMIF (aMapFileName) else

        if sExt='.dm' then
        begin
          blBounds:=oMatrix.GetBLBounds();
          neva_Create_Map (aMapFileName, '100', blBounds);
          ProcessMatrix (FColorMatrix, aMapFileName);
        end;

      end;

      oMatrix.Free;
    end;
    //------------------------------------------------------


var
  sExt,sMatrixFileName,sMapFileName: string;
  bool: boolean;
  
begin
  with dmParams.mem_Matrix do
  begin
    First;

    while not EOF do
    begin
      DoProgress (RecNo, RecordCount, Terminated);

      sMatrixFileName:=FieldValues['FileName'];
      sMapFileName   :=FieldValues['Map'];

      sExt:=LowerCase(ExtractFileExt (sMatrixFileName));

      if sExt='.kgr' then
        bool:=dmParams.PrepareCellsColorRanges()
      else
        bool:=dmParams.SetColorSchema (FieldValues['Schema']);

      if bool then
        DoExecMatrix (sExt, sMatrixFileName, sMapFileName);

      Next;
    end;
  end;

  Result:=True;
end;

//------------------------------------------------------
procedure TMapMaker.Execute_Points();
//------------------------------------------------------
begin
  with dmParams.mem_Point_Groups do
  begin
    First;

    while not EOF do
    begin
      DoProgress (RecNo, RecordCount, Terminated);

      dmParams.SetColorSchema (FieldByName('Schema').AsInteger);
      ProcessPointGroup (FieldValues[FLD_ID],
                         FieldValues['Map'],
                         FieldByName('step').AsInteger,
                         FieldByName(FLD_FRAME).AsBoolean
                         );
      Next;
    end;
  end;
end;



end.



