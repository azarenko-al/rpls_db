unit u_Main_mif;

interface
uses SysUtils,Windows,graphics,

     dm_Params,
     u_GEO,
     u_func,
     u_func_img,

     u_files,
     u_func_raster,

     u_Matrix,
     u_classes,
     d_Progress_adv,

     u_MatrixDecomposer
     ;

type
  //------------------------------------------------------
  // ������������ ����� DM �� 2D ��������� �������
  //------------------------------------------------------
  TMapMaker = class (TProgress)
  //------------------------------------------------------
  private
    FMatrixDecomposer : TMatrixDecomposer; // ������� � �������
    FColorMatrix      : TIntMatrix;
    FRangeMatrix      : TIntMatrix; //������� �������� ���� �������

    procedure DoOnProgress2 (aPosition, aTotal: integer; var aTerminated: boolean);
    procedure PrepareColorMatrix (aMatrix: TOrdinalMatrix);
    procedure SaveColorMatrixToBitmap (aBitmap: TBitmap);
    procedure SaveColorMatrixToMIF (aFileName: string);

  public
    constructor Create;
    destructor Destroy; override;

    function Execute: boolean;
  end;


//======================================================
implementation
//======================================================


//------------------------------------------------------
constructor TMapMaker.Create;
//------------------------------------------------------
begin
  inherited;

  FColorMatrix:=TIntMatrix.Create;
  FRangeMatrix:=TIntMatrix.Create;

  FMatrixDecomposer:=TMatrixDecomposer.Create;
  FMatrixDecomposer.OnProgress2:=DoOnProgress2;

  FMatrixDecomposer.Progress2Step:=50;
end;


destructor TMapMaker.Destroy;
begin
  FColorMatrix.Free;
  FRangeMatrix.Free;
  FMatrixDecomposer.Free;
  inherited;
end;


procedure TMapMaker.DoOnProgress2 (aPosition, aTotal: integer; var aTerminated: boolean);
begin
  DoProgress2 (aPosition, aTotal, aTerminated);
end;

//-----------------------------------------------------
procedure TMapMaker.DoOnMapNewBLShape (aPolyline: TMatrixPolyline; aFillValue: integer);
//-----------------------------------------------------
// ���������� ��� ������������ ������� �������
var i:integer;  blPoints: TBLPointArray;
begin
  SetLength (blPoints, aPolyline.Count);
  for i:=0 to aPolyline.Count-1 do
  begin

    blPoints[i].b:=aPolyline[i].b;
    blPoints[i].l:=aPolyline[i].l;
  end;

  if aFillValue>=0 then
    FRangeRegions[aFillValue].AddPolygon (blPoints);
end;


//------------------------------------------------------
procedure TMapMaker.PrepareRangeMatrix (aMatrix: TOrdinalMatrix;
                                    aColorType: TColorRangeType);
// ����������� ������� �������� ������ ������� ������
// � ������ ������
//------------------------------------------------------
var r,c,i,iValue: integer;
begin
{
  with aMatrix do
    FRangeMatrix.SetParams (TopLeft.X,TopLeft.Y, RowCount,ColCount,Step);

  for r:=0 to aMatrix.RowCount-1 do
  for c:=0 to aMatrix.ColCount-1 do
  begin
    iValue:=aMatrix[r,c];
    FRangeMatrix[r,c]:=-1;

    if iValue=aMatrix.BlankValue then Continue;

    case aColorType of

      rtKUP,
      rtCI,
      rtCA,
      rtCIA: for i:=0 to High(Options.ColorRanges [aColorType].Items) do
               with Options.ColorRanges[aColorType].Items[i] do
                 if (MinValue <= iValue) and (iValue < MaxValue)
                 then begin
                   FRangeMatrix[r,c]:=i; Break;
                 end;

      rtKGR: for i:=0 to High(FSectors) do
                 if FSectors[i].ID = iValue then begin
                   FRangeMatrix[r,c]:=i; Break;
                 end;
    end;
  end;
}
end;


//------------------------------------------------------
procedure TMapMaker.SaveColorMatrixToBitmap (aBitmap: TBitmap);
//------------------------------------------------------
var iValue,r,c : integer;
begin
  aBitmap.Transparent:=True;
  aBitmap.TransparentMode:=tmFixed;
  aBitmap.TransparentColor:=clWhite;

  aBitmap.PixelFormat := pf32bit;
  aBitmap.Width  := FColorMatrix.ColCount;
  aBitmap.Height := FColorMatrix.RowCount;

  with FColorMatrix do
  for r:=0 to RowCount-1 do
    for c:=0 to ColCount-1 do
    begin
      iValue:=FColorMatrix[r,c];
      if iValue=FColorMatrix.BlankValue then
        iValue:=clWhite;

      aBitmap.Canvas.Pixels[c,r] := iValue;
    end;
end;


//------------------------------------------------------
procedure TMapMaker.SaveColorMatrixToMIF (aFileName: string);
//------------------------------------------------------
var oPicture : TPicture;
  sBmpFileName: string;
begin
  sBmpFileName:=ChangeFileExt(aFileName, '.gif');

  oPicture := TPicture.Create;

  SaveColorMatrixToBitmap (oPicture.Bitmap);

  oPicture.SaveToFile (sBmpFileName);
  oPicture.Destroy;

  SaveStringToFile(geo_MakeXYHeaderForMIF (sBmpFileName,
                  FColorMatrix.TopLeft,
                  FColorMatrix.RowCount, FColorMatrix.ColCount,
                  FColorMatrix.Step,     FColorMatrix.Step),
                  ChangeFileExt (aFileName, '.tab'));
end;

//------------------------------------------------------
procedure TMapMaker.PrepareColorMatrix (aMatrix: TOrdinalMatrix);
//------------------------------------------------------
var r,c,iValue: integer;
begin
  //��������� ������� ������
  FColorMatrix.AssignHeader (aMatrix);
  FColorMatrix.FillBlank;

  with aMatrix do
  for r:=0 to RowCount-1 do
  for c:=0 to ColCount-1 do if not IsNull(r,c) then
  begin
    iValue:=Items[r,c];

    FColorMatrix[r,c]:=dmParams.GetColor (iValue);

    if (r mod 100) = 0 then
      DoProgress2(r,RowCount, Terminated);
  end;
end;

//------------------------------------------------------
function TMapMaker.Execute(): boolean;
//------------------------------------------------------

    //------------------------------------------------------
    procedure DoExecMatrix (aExt, aMatrixFileName, aMapFileName: string);
    //------------------------------------------------------
    var  oMatrix : TOrdinalMatrix;
        blBounds: TBLRect;
        sExt: string;
    begin
      oMatrix:=matrix_CreateMatrixByFileName (aMatrixFileName);
      if not Assigned(oMatrix) then
        raise Exception.Create('');
        //Exit;

      DoLog ('������ ����� :' + aMatrixFileName);
      if not oMatrix.LoadFromFile (aMatrixFileName) then
        DoLog ('���� �� ����������: ' + aMatrixFileName)

      else begin
        DoLog (Format('�������: %s;  ����� DM: %s', [aMatrixFileName,aMapFileName]));

        PrepareColorMatrix (oMatrix);

        sExt:=LowerCase(ExtractFileExt (aMapFileName));

        if sExt='.tab' then
          SaveColorMatrixToMIF (aMapFileName) else

      end;

      oMatrix.Free;
    end;
    //------------------------------------------------------


var
  sExt,sMatrixFileName,sMapFileName: string;
  bool: boolean;
  
begin
  with dmParams.mem_Matrix do
  begin
    First;

    while not EOF do
    begin
      DoProgress (RecNo, RecordCount, Terminated);

      sMatrixFileName:=FieldValues['FileName'];
      sMapFileName   :=FieldValues['Map'];

      sExt:=LowerCase(ExtractFileExt (sMatrixFileName));

      if sExt='.kgr' then
        bool:=dmParams.PrepareCellsColorRanges()
      else
        bool:=dmParams.SetColorSchema (FieldValues['Schema']);

      if bool then
        DoExecMatrix (sExt, sMatrixFileName, sMapFileName);

      Next;
    end;
  end;

  Result:=True;
end;


end.



