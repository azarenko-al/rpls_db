unit u_BinToMap_export;

interface


uses  SysUtils, Variants, 

   MapXLib_TLB,

   u_MIF_MID_File,

   u_img,

   u_ini_Params_Bin_To_Map_classes,

   u_KML,

   u_ini_Params_bin_to_map,

//   TFeatureCollection

 //  u_GeoJSON,

   u_Geo,

  // u_mitab,

   u_map_classes,

   u_MapX_lib,
   u_MapX
   ;

type
  TExportAsVector = class(TObject)
  private
  public
    MapRegionList_ref: TMapRegionList;

    MatrixFileType: (mftNone,mftKgr,mftKup);


  //  procedure SaveToJSON(aFileName: string);
    procedure SaveToMif(aFileName: string; aIsConvertToTab: boolean = false);
    procedure SaveToTab(aFileName: string);

    procedure SaveToKml(aFileName: string);

  end;

implementation

const
//  FLD_RXLEVDBM= 'RxLevDbm';
  FLD_RecNum= 'RecNum';
  FLD_VALUE = 'VALUE';

//  FLD_ID     ='ID';
  FLD_CELLID ='CELLID';
  FLD_LAC    ='LAC'; 



//------------------------------------------------------------------------------
procedure TExportAsVector.SaveToKml(aFileName: string);
//------------------------------------------------------------------------------
var
  FKML: TKML;


    //------------------------------------------------------------------------------
    procedure DoMakeGoogleMap_KML;
    //------------------------------------------------------------------------------
    var
      i,k,iInd, iTransparence,
      iColor: integer;
      oColorSchema: TColorSchema;

    begin
     // DoProgressMsg2('���������� ��������� ����� Google');
     // DoProgress2(0, gl_MatrixInfo.RowCount);

      FKML:=TKML.Create();
      FKML.AddHeader;


      iTransparence:=255; // - g_Params_bin_to_map.Transparence_0_255;
//      iTransparence:=255 - g_Params_bin_to_map.Transparence_0_255;

      //--------------------------------------

    //    oColorSchema := g_Params_bin_to_map.ColorSchemaList.FindByID(FColorSchemaID);
      oColorSchema := g_Params_bin_to_map.ColorSchema_Selected;

      Assert(Assigned(oColorSchema), 'Assigned(oColorSchema) not assigned');

    end;
    //------------------------------------------------------------------------------


var
  BLPointArray: TBLPointArray;
  I: Integer;
  iID_style: Integer;
  j: Integer;

begin
  Assert(Assigned(MapRegionList_ref), 'Value not assigned');

  
  DoMakeGoogleMap_KML();


//  for I := 0 to 0 do
//    for j := 0 to 0 do

  for I := 0 to MapRegionList_ref.Count - 1 do
    for j := 0 to MapRegionList_ref[i].Count - 1 do
    begin
      BLPointArray:=MapRegionList_ref[i].Items[j].BLPointArray;

     //   FMapRegionList[i].

      iID_style := MapRegionList_ref[i].Value;
      iID_style := MapRegionList_ref[i].Color;

      FKML.AddStyle1 (iID_style, 255);

      FKML.AddPlacemark_Polygon_(BLPointArray, iID_style);

{
      	<StyleMap id="m_ylw-pushpin">
		<Pair>
			<key>normal</key>
			<styleUrl>#m_ylw-pushpin</styleUrl>
		</Pair>
	</StyleMap>

	<Style id="m_ylw-pushpin">
		<PolyStyle>
			<color>ff0000ff</color>
		</PolyStyle>
	</Style>
 }

//      FKML.AddPlacemark_Polygon_(BLPointArray, iID_style);

    end;


  FKML.AddFooter;
//  FKML.SaveToFile(ChangeFileExt(FMapFileName, '.kml'));
  FKML.SaveToFile(ChangeFileExt(aFileName, '.kml'));

  FreeAndNil(FKML);

end;



//------------------------------------------------------------------------------
procedure TExportAsVector.SaveToTab(aFileName: string);
//------------------------------------------------------------------------------
var
  FmiMap1: TmiMap;
  I: Integer;

  BLPointArray: TBLPointArray;

  rStyleRec : TmiStyleRec;


  iIndex: Integer;
  j: Integer;
  sValue: string;
  vCMapXFeature: CMapXFeature;

begin
  Assert(Assigned(MapRegionList_ref), 'Value not assigned');


  aFileName := ChangeFileExt(aFileName, '.tab');


  FmiMap1:=TmiMap.Create;


//  case FMapType of
  case MatrixFileType of
    mftKgr:  FmiMap1.CreateFile1( aFileName,
                            [
                             {$IFDEF test}
                               mapX_Field (FLD_ID, miTypeInt),
                             {$ENDIF}

                             mapX_Field (FLD_CELLID, miTypeInt),
                             mapX_Field (FLD_LAC, miTypeInt)
                            ]);
    else
      FmiMap1.CreateFile1( aFileName, [mapX_Field (FLD_VALUE, miTypeString)]);

  end;



    for I := 0 to MapRegionList_ref.Count - 1 do
    begin

      FillChar(rStyleRec, SizeOf(rStyleRec), 0);

      rStyleRec.RegionBorderStyle      := 0;// MI_BRUSH_PATTERN_BLANK;
      rStyleRec.RegionPattern          := MI_BRUSH_PATTERN_SOLID;
//      rStyleRec.RegionForegroundColor  := FMapRegionList[i].Value;//  aFillValue;
      rStyleRec.RegionBackColor  := MapRegionList_ref[i].Color;//  aFillValue;
//      rStyleRec. RegionIsTransparent    := False;

//      rStyleRec.RegionForegroundColor  := MapRegionList_ref[i].Color;//  aFillValue;
//      rStyleRec.RegionIsTransparent    := False;

      
      FmiMap1.PrepareStyle(miFeatureTypeRegion, rStyleRec);

      BLPointArray:=MapRegionList_ref[i].Items[0].BLPointArray;



      case MatrixFileType of
         mftKgr:
         begin
           vCMapXFeature:= FmiMap1.WriteRegion(BLPointArray,
                          [
                          {$IFDEF test}
                             FLD_ID,     MapRegionList_ref[i].ID,
                          {$ENDIF}
                           FLD_CELLID, MapRegionList_ref[i].CELLID ,
                           FLD_LAC,    MapRegionList_ref[i].LAC
                           ]);
         end;

         else begin
           sValue:= VarToStr ( MapRegionList_ref[i].Value);

           vCMapXFeature:= FmiMap1.WriteRegion(BLPointArray,
                                [FLD_VALUE, sValue]);

          end;
      end;


      for j := 1 to MapRegionList_ref[i].Count - 1 do
      begin
        BLPointArray:=MapRegionList_ref[i].Items[j].BLPointArray;

        FmiMap1.WritePart(vCMapXFeature, BLPointArray);  
      end;

      vCMapXFeature.Update (EmptyParam, EmptyParam);
    end;


  FmiMap1.CloseFile;

  FreeAndNil(FmiMap1);

end;


//------------------------------------------------------------------------------
procedure TExportAsVector.SaveToMif(aFileName: string; aIsConvertToTab: boolean =
    false);
//------------------------------------------------------------------------------
var
  I: Integer;

  BLPointArray: TBLPointArray;
  j: Integer;
  oFile: TMIF_MID_File;
  sValue: string;

  arrBLPoints: array of TBLPointArray;

  rStyleRec : TmiStyleRec;

begin
  Assert(Assigned(MapRegionList_ref), 'Value not assigned');


  aFileName := ChangeFileExt(aFileName, '.mif');


  oFile:=TMIF_MID_File.Create;

  case MatrixFileType of
    mftKgr:  oFile.CreateFile_Pulkovo( aFileName,
                            [
                            {$IFDEF test}
                             mapX_Field (FLD_ID, miTypeInt),

                            {$ENDIF}

                             mapX_Field (FLD_CELLID, miTypeInt),
                             mapX_Field (FLD_LAC, miTypeInt)
                            ]);
    else
      oFile.CreateFile_Pulkovo( aFileName, [mapX_Field (FLD_VALUE, miTypeString)]);

  end;


    for I := 0 to MapRegionList_ref.Count - 1 do
    begin
//          Assert(FMapRegionList[i].Items.Count>0, 'Value <=0');

      FillChar(rStyleRec, SizeOf(rStyleRec), 0);

      rStyleRec.RegionBorderStyle := 0;// MI_BRUSH_PATTERN_BLANK;
      rStyleRec.RegionPattern     := MI_BRUSH_PATTERN_SOLID;
      rStyleRec.RegionBackColor   := MapRegionList_ref[i].Color;//  aFillValue;
      rStyleRec.RegionColor       := MapRegionList_ref[i].Color;//  aFillValue;

//      rStyleRec.RegionBackgroundColor  := MapRegionList_ref[i].Color;//  aFillValue;
//      rStyleRec.RegionForegroundColor  := MapRegionList_ref[i].Color;//  aFillValue;
      
//      rStyleRec.RegionIsTransparent    := False;

      rStyleRec.RegionBorderColor      := MapRegionList_ref[i].Color;

     // FmiMap1.PrepareStyle(miFeatureTypeRegion, rStyleRec);

      BLPointArray:=MapRegionList_ref[i].Items[0].BLPointArray;

      sValue:= VarToStr ( MapRegionList_ref[i].Value);


      SetLength(arrBLPoints, MapRegionList_ref[i].Count);
      for j := 0 to MapRegionList_ref[i].Count - 1 do
        arrBLPoints[j]:=MapRegionList_ref[i].Items[j].BLPointArray;


      case MatrixFileType of
         mftKgr:
           begin
             oFile.WriteRegion(arrBLPoints, rStyleRec,
                            [
                            {$IFDEF test}
                               mapx_Par(FLD_ID,   MapRegionList_ref[i].ID),
                            {$ENDIF}

                             FLD_LAC,    MapRegionList_ref[i].LAC,
                             FLD_CELLID, MapRegionList_ref[i].CellID
                             ]);

           end;

         else begin
           sValue:= VarToStr ( MapRegionList_ref[i].Value);
           oFile.WriteRegion(arrBLPoints, rStyleRec, [FLD_VALUE, sValue]);
         end;
      end;


    end;


   oFile.Close;

   if aIsConvertToTab then
     oFile.ExportToMapinfoTab();


   FreeAndNil(oFile);

end;



end.

