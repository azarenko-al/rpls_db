unit u_BinToMap_mif;

interface

{.$DEFINE test}


uses  SysUtils, Variants, Dialogs,

   u_BinToMap_export,

   u_ini_Params_Bin_To_Map_classes,


//   u_run,

u_func,

   u_BinToMap,
   u_MatrixDecomposer,

   u_ini_Params_bin_to_map,                                                         

   u_Geo,
   u_Geo_mif,

  // u_mitab,
   u_progress,

   u_map_classes
   ;


type
  //------------------------------------------------------
  // ������������ ����� MIF �� 2D ��������� �������
  //------------------------------------------------------
  TBinToMap_MIF = class(TBinToMap)
  private
    FMapRegionList: TMapRegionList;



    FAddMapCount: integer;

  protected
    procedure DoOnMapNewBLShape (Sender: TObject;

                                 aBLPoints: TBLPointArray;
                                 aFillValue,
                                 aRow,aCol: integer); override;

    procedure SaveColorMatrixToVectorFile (aMatrixFileName,aMapFileName: string); override;

    function SaveMatrixToRasterFile_MIF(aFileName: string): boolean; override;

  public
    constructor Create;  
    destructor Destroy; override;

  end;


//======================================================
implementation
//======================================================

uses
  IniFiles,

  u_Matrix_base;

(*

const
//  FLD_RXLEVDBM= 'RxLevDbm';
  FLD_RecNum= 'RecNum';
  FLD_VALUE = 'VALUE';

//  FLD_ID     ='ID';
  FLD_CELLID ='CELLID';
  FLD_LAC    ='LAC'; 
*)


constructor TBinToMap_MIF.Create;
begin
  inherited;

  FMapRegionList:=TMapRegionList.Create;

end;

destructor TBinToMap_MIF.Destroy;
begin
  FreeAndNil(FMapRegionList);

  inherited;
end;

//------------------------------------------------------
function TBinToMap_MIF.SaveMatrixToRasterFile_MIF(aFileName: string): boolean;
//------------------------------------------------------
var
  sImgFileName: string;

  rec: TMifHeaderInfoRec;

  iMax_Y: Integer;
  iMin_X: Integer;
  oIni: TMemIniFile;

//  arrXYPointArrayF_GK: TXYPointArrayF;
  bl: TBLPoint;

//  blRect_CK42: TBLRect;
 // blRect_WGS: TBLRect;
  i: Integer;
  s: string;
//  oMifRaster: TMapinfoRasterTab;

  sFile: string;
  xy: TXYPoint;


//  vIRasterToolsX: IRasterToolsX;

begin
  sImgFileName:= ChangeFileExt(aFileName, DEF_RASTER_EXT);

//  Assert(FileExists(sImgFileName));

  //   img_BMP_to_PNG(FBmpFileName, sPNGFileName, true, clBlack);


//  Result:= SaveMatrixToBitmapFile (sImgFileName);
  Result:= SaveMatrixToBitmapFile (sImgFileName);
                                               


  ///////////////////////////////////////

//  Result:= SaveMatrixToBitmapFile_old (sImgFileName);

   Assert(FileExists(sImgFileName));


   {
  Assert(FValuesMatrix.ZoneNum>0, 'FValuesMatrix.ZoneNum <=0');

  if Result then
  begin
//    oMifRaster:=TMapinfoRasterTab.Create;

    FillChar (rec, SizeOf(rec), 0);

    rec.FileName:=  ChangeFileExt(aFileName, '_.tab'); 
    rec.BitmapFileName:=sImgFileName;


    rec.RowCount:= FValuesMatrix.RowCount;
    rec.ColCount:= FValuesMatrix.ColCount;

    rec.xy.ZoneNum := FValuesMatrix.ZoneNum;

    rec.XYRect     := FValuesMatrix.GetXYBounds;

//    rec.RowCount   := FValuesMatrix.RowCount;
//    rec.ColCount   := FValuesMatrix.ColCount;

    rec.XY.StepX   := FValuesMatrix.Step;
    rec.XY.StepY   := FValuesMatrix.Step;
    rec.XY.TopLeft := FValuesMatrix.TopLeft;

    rec.Transparence_0_255 := g_Params_bin_to_map.Transparence;
 //   rec.Transparence_0_255 := g_Params_bin_to_map.Transparence_0_255;

    geo_SaveXYHeaderForMIF (rec);


  end;
 }

 
  // -------------------------
  // ini
  // -------------------------

  sFile:=ChangeFileExt(sImgFileName, '.ini');
//  DeleteFile(sFile);

//  oIni:=TIniFile.Create( ChangeFileExt(sImgFileName, '.ini') );
  oIni:=TMemIniFile.Create( sFile);

//    rec.ImgRowCount:= FColorMatrix.RowCount;
 //   rec.ImgColCount:= FColorMatrix.ColCount;

  oIni.WriteString('main', 'projection', 'gk');
  oIni.WriteString('main', 'coordinate', 'pulkovo42');
  oIni.WriteString('main', 'FileName', ExtractFileName( sImgFileName));

  oIni.WriteInteger('main', 'RowCount', FValuesMatrix.RowCount);
  oIni.WriteInteger('main', 'ColCount', FValuesMatrix.ColCount);

  s:=ExtractFileName(FValuesMatrix.FileName);

  oIni.WriteString('main', 'bin_FileName',  ExtractFileName(FValuesMatrix.FileName));

//      rec.FileName:=aFileName;



  iMin_X:=Round(FValuesMatrix.TopLeft.X - FValuesMatrix.Step * FValuesMatrix.RowCount);
  iMax_Y:=Round(FValuesMatrix.TopLeft.Y + FValuesMatrix.Step * FValuesMatrix.ColCount);

  Assert(Round(FValuesMatrix.TopLeft.X) > iMin_X, 'Round(FValuesMatrix.TopLeft.X) > iMin_X');


  oIni.WriteInteger('main', 'x_min', iMin_X);
  oIni.WriteInteger('main', 'x_max', Round(FValuesMatrix.TopLeft.X));

  oIni.WriteInteger('main', 'y_min', Round(FValuesMatrix.TopLeft.Y));
  oIni.WriteInteger('main', 'y_max', iMax_Y);

  oIni.WriteInteger('main', 'cell_size', FValuesMatrix.Step);
  oIni.WriteInteger('main', 'zone',      FValuesMatrix.ZoneNum);

  oIni.WriteInteger('main', 'x_width', FValuesMatrix.Step * FValuesMatrix.ColCount);
  oIni.WriteInteger('main', 'y_width', FValuesMatrix.Step * FValuesMatrix.RowCount);


  oIni.UpdateFile;

  FreeAndNil(oIni);


end;


//------------------------------------------------------
procedure TBinToMap_MIF.SaveColorMatrixToVectorFile (aMatrixFileName,aMapFileName: string);
//------------------------------------------------------
var
  oExport: TExportAsVector;
  s: string;

begin
//ShowMessage ('procedure TBinToMap_MIF.SaveColorMatrixToVectorFile (aMatrixFileName,aMapFileName: string); ' + aMatrixFileName);

//


  FMatrixDecomposer.OnProgress2:=OnProgress2;
//  FShapeCount:=0;
//  FMapFileName:=aMapFileName;

  FMapRegionList.Clear;

  DoLog ('������ ������������ �����: '+ TimeToStr(Now));


//  ShowMessage ('procedure TBinToMap_MIF.SaveColorMatrixToVectorFile');



  s:=FColorMatrix.FileName;


  FMatrixDecomposer.Decompose(FColorMatrix);

 // ShowMessage ('1 done');


  DoLog ('����� ������������ �����: ' + TimeToStr(Now));
  DoLog (Format('����� / c����������� ����������: %d / %d',
          [FColorMatrix.RowCount * FColorMatrix.ColCount,
           FMatrixDecomposer.ShapesTotal]));

  DoProgress2 (0,0);

//  SaveToMif(FMapFileName, true);


//ShowMessage ('2');


  oExport:=TExportAsVector.Create;
  oExport.MapRegionList_ref := FMapRegionList;

  case FMapType of
    mtKgr: oExport.MatrixFileType := mftKgr;
    mtKup: oExport.MatrixFileType := mftKup;
  else
           oExport.MatrixFileType := mftNone;
  end;


//ShowMessage ('3');



 // ---------------------------------------------------------------
  case g_Params_bin_to_map.MapExportType of
  // ---------------------------------------------------------------
//    mtJSON:     oExport.SaveToJSON(aMapFileName);

    mtKML:     oExport.SaveToKml(aMapFileName);
    mtVector:  oExport.SaveToTab(aMapFileName);
    mtMIF:     oExport.SaveToMif(aMapFileName);
    mtMIF_TAB: oExport.SaveToMif(aMapFileName, True);
  else
    raise Exception.Create('');
  end;


  FreeAndNil(oExport);


//ShowMessage ('end');



end;



//-----------------------------------------------------
// ���������� ��� ������������ ������� �������
//-----------------------------------------------------
procedure TBinToMap_MIF.DoOnMapNewBLShape (Sender: TObject;
                                          aBLPoints: TBLPointArray;
                                          aFillValue,
                                          aRow,aCol: integer);
//-----------------------------------------------------
var
 // iID: Integer;
  iValue: Integer;
  oMapRegion: TMapRegion;
  oCell: TCell;
begin
  iValue:=FValuesMatrix[aRow,aCol];

  case FMapType of
     mtKgr: if iValue < 0 then
              Exit;
  end;


  oMapRegion := FMapRegionList.AddItemWithValue(aFillValue, iValue,  aBLPoints);


  case FMapType of
     mtKgr:
       begin
       //  iID:= aFillValue;
         Assert(g_Params_bin_to_Map.Cells.Count>0,'g_Params_bin_to_Map.Cells.Count>0');

         oCell:=g_Params_bin_to_Map.Cells.FindByID(iValue);

        // oCell:=g_Params_bin_to_Map.Cells.FindByID(iValue);

         Assert(Assigned(oCell), 'Assigned(oCell): Value='+IntToStr(iValue));

(*
         if not Assigned(oCell) then
         begin

           ShowMessage(IntToStr(iValue));

         end;


         Assert(Assigned(oCell), 'Assigned(oCell)');
 *)
 
//         assert( g_Params_bin_to_Map.Cells.Count < iIndex);

         oMapRegion.ID    :=oCell.ID;
         oMapRegion.CellID:=oCell.CellID;
         oMapRegion.LAC   :=oCell.LAC;

         oMapRegion.Properties.Values['id']     := IntToStr(oCell.ID);
         oMapRegion.Properties.Values['CellID'] := oCell.CellID;
         oMapRegion.Properties.Values['LAC']    := oCell.LAC;

       end;

  end;

end;


end.



