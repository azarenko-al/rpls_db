unit u_BinToMap_to_raster;

interface

uses

   IniFiles, SysUtils,graphics, Variants,

   u_geo_convert_new,

   u_img,

   u_Params_Bin_To_Map_classes,

   u_BinToMap,
   u_MatrixDecomposer,

   u_KML,

   u_Params_bin_to_map,

   u_Geo,
   u_Geo_mif,

 //  u_mitab,
   u_progress,
   u_MapX
   ;


type
  TBinToMap_to_raster = class(TObject)
  protected
    function SaveMatrixToRasterFile_MIF(aFileName: string): boolean; //override;
  end;

implementation

//------------------------------------------------------
function TBinToMap_to_raster.SaveMatrixToRasterFile_MIF(aFileName: string):
    boolean;
//------------------------------------------------------
var
  sImgFileName: string;
  rec: TMifHeaderInfoRec;

  iMax_Y: Integer;
  iMin_X: Integer;
  oIni: TMemIniFile;

//  blBorderBLPoints_CK42: TXYPointArray;
  blBorderBLPoints_CK42: TBLPointArrayF;
  blBorderBLPoints_WGS: TBLPointArrayF;

   arrXYPointArrayF_GK: TXYPointArrayF;
  bl: TBLPoint;
  blRect_CK42: TBLRect;
  blRect_WGS: TBLRect;
  i: Integer;
  oRaster: TMapinfoRasterTab;

  sFile: string;
  xy: TXYPoint;

begin
  sImgFileName:= ChangeFileExt(aFileName, DEF_RASTER_EXT);

//  Result:= SaveMatrixToBitmapFile (sImgFileName);
 // Result:= SaveMatrixToBitmapFile (sImgFileName);
//  Result:= SaveMatrixToBitmapFile_old (sImgFileName);


Assert(FValuesMatrix.ZoneNum>0, 'Value <=0');

  if Result then
  begin
    oRaster:=TMapinfoRasterTab.Create;

    FillChar (rec, SizeOf(rec), 0);

    rec.FileName:=aFileName;
    rec.BitmapFileName:=sImgFileName;


    rec.ImgRowCount:= FValuesMatrix.RowCount;
    rec.ImgColCount:= FValuesMatrix.ColCount;

    rec.xy.ZoneNum    := FValuesMatrix.ZoneNum;

    rec.XYRect  := FValuesMatrix.GetXYBounds;

    rec.RowCount:= FValuesMatrix.RowCount;
    rec.ColCount:= FValuesMatrix.ColCount;
    rec.XY.StepX   := FValuesMatrix.Step;
    rec.XY.StepY   := FValuesMatrix.Step;
    rec.XY.TopLeft := FValuesMatrix.TopLeft;

    geo_SaveXYHeaderForMIF (rec);


    oRaster.Units := utMeter;
    oRaster.BitmapFileName:=sImgFileName;

    oRaster.ImgRowCount:= FValuesMatrix.RowCount;
    oRaster.ImgColCount:= FValuesMatrix.ColCount;
    oRaster.ZoneNum    := FValuesMatrix.ZoneNum;

    oRaster.SetXYRect(FValuesMatrix.GetXYBounds);


    oRaster.SaveToFile(aFileName+'__');

    FreeAndNil(oRaster);
  end;

  // -------------------------
  // ini
  // -------------------------

  sFile:=ChangeFileExt(sImgFileName, '.ini');
//  DeleteFile(sFile);

//  oIni:=TIniFile.Create( ChangeFileExt(sImgFileName, '.ini') );
  oIni:=TMemIniFile.Create( sFile);

//    rec.ImgRowCount:= FColorMatrix.RowCount;
 //   rec.ImgColCount:= FColorMatrix.ColCount;

  oIni.WriteString('main', 'projection', 'gk');
  oIni.WriteString('main', 'coordinate', 'pulkovo42');
  oIni.WriteString('main', 'FileName', ExtractFileName( sImgFileName));

  oIni.WriteInteger('main', 'RowCount', FValuesMatrix.RowCount);
  oIni.WriteInteger('main', 'ColCount', FValuesMatrix.ColCount);

  iMin_X:=Round(FValuesMatrix.TopLeft.X - FValuesMatrix.Step * FValuesMatrix.RowCount);
  iMax_Y:=Round(FValuesMatrix.TopLeft.Y + FValuesMatrix.Step * FValuesMatrix.ColCount);

  Assert(Round(FValuesMatrix.TopLeft.X) > iMin_X);


  oIni.WriteInteger('main', 'x_min', iMin_X);
  oIni.WriteInteger('main', 'x_max', Round(FValuesMatrix.TopLeft.X));

  oIni.WriteInteger('main', 'y_min', Round(FValuesMatrix.TopLeft.Y));
  oIni.WriteInteger('main', 'y_max', iMax_Y);

  oIni.WriteInteger('main', 'cell_size', FValuesMatrix.Step);
  oIni.WriteInteger('main', 'zone',      FValuesMatrix.ZoneNum);

  oIni.WriteInteger('main', 'x_width', FValuesMatrix.Step * FValuesMatrix.ColCount);
  oIni.WriteInteger('main', 'y_width', FValuesMatrix.Step * FValuesMatrix.RowCount);


  geo_XYRectToXYPoints(FValuesMatrix.GetXYBounds, arrXYPointArrayF_GK);
  blBorderBLPoints_CK42.Count:=4;
  blBorderBLPoints_WGS.Count :=4;

  for i:=0 to 3 do
  begin
    xy:=arrXYPointArrayF_GK.Items[i];

    bl := geo_GK_XY_to_Pulkovo42_BL(xy, FValuesMatrix.ZoneNum);

    blBorderBLPoints_CK42.Items[i]  := bl;
    blBorderBLPoints_WGS.Items[i] := geo_Pulkovo42_to_WGS84(bl);
  end;


  blRect_CK42 := geo_RoundBLPointsToBLRect(blBorderBLPoints_CK42);
  blRect_WGS  := geo_RoundBLPointsToBLRect(blBorderBLPoints_WGS);

  oIni.WriteFloat('bounds_CK42', 'lat_max', blRect_CK42.TopLeft.B);
  oIni.WriteFloat('bounds_CK42', 'lat_min', blRect_CK42.BottomRight.B);
  oIni.WriteFloat('bounds_CK42', 'lon_min', blRect_CK42.TopLeft.L);
  oIni.WriteFloat('bounds_CK42', 'lon_max', blRect_CK42.BottomRight.L);

  oIni.WriteFloat('bounds_WGS', 'lat_max', blRect_WGS.TopLeft.B);
  oIni.WriteFloat('bounds_WGS', 'lat_min', blRect_WGS.BottomRight.B);
  oIni.WriteFloat('bounds_WGS', 'lon_min', blRect_WGS.TopLeft.L);
  oIni.WriteFloat('bounds_WGS', 'lon_max', blRect_WGS.BottomRight.L);

  oIni.UpdateFile;

  FreeAndNil(oIni);


(*

  SetLength(arrXYRectArray_UTM, 1);

  arrXYRectArray_UTM[0] := FDEM.XYBounds;

  rXYRect_UTM:=geo_GetRoundXYRect_(arrXYRectArray_UTM);


  geo_XYRectToXYPoints(rXYRect_UTM, arrXYPointArrayF_new);


  // -------------------------
  // border
  // -------------------------
  SetLength(xyBorderXYPoints, Length(Params.BorderBLPoints));

  for i:=0 to Length(Params.BorderBLPoints)-1 do
  begin
    bl:=Params.BorderBLPoints[i];

    xy := geo_Pulkovo42_to_XY(bl, FGK_zone );

    xyBorderXYPoints[i] := xy;
  end;

*)
end;

end.
