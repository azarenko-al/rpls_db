unit u_Task_BinToMap;

interface

uses SysUtils,graphics, Classes,

  u_BMP_file,

//  u_BinToMap_classes,

  u_Params_Bin_To_Map_classes,

  u_Params_bin_to_map,
//  dm_Params_bin_to_mif,

  u_GEO,
  
  u_log,
  u_progress,

  u_Matrix,
  u_Matrix_base,

  u_MatrixDecomposer
  ;


type
  //------------------------------------------------------
  // ������������ ����� DM �� 2D ��������� �������
  //------------------------------------------------------
  TTask_BinToMap = class(TProgress)
  //------------------------------------------------------
  protected
    FMatrixDecomposer : TMatrixDecomposer; // ������� � �������
    FColorMatrix      : TIntMatrix;
    FValuesMatrix     : TOrdinalMatrix;
    FMapType: (mtKgr, mtKup, mtOther);

  private
    procedure DeleteColorMatrix();
    function  PrepareColorMatrix (aMatrix: TOrdinalMatrix): boolean;

    function ExecuteBinMatrix: Boolean;

  protected
    FMatrixFileName: string;
    FMapFileName   : string;

    FColorSchemaID : integer;
    FCaption       : string;

    procedure SaveColorMatrixToVectorFile (aMatrixFileName,aMapFileName: string);  virtual; abstract;
    function  SaveColorMatrixToRasterFile (aMapFileName: string): boolean;  virtual; abstract;

// TODO: SaveColorMatrixToBitmapFile
//  function  SaveColorMatrixToBitmapFile (aFileName: string): boolean;

// TODO: SaveColorMatrixToBitmap
//  function SaveColorMatrixToBitmap(aFileName: string): boolean;

    function SaveMatrixToBitmapFile(aFileName: string): boolean;
    function  SaveMatrixToRasterFile_MIF (aFileName: string): boolean; virtual; abstract;

//    function  OpenMap (aMapFileName: string; aBLBounds: TBLRect): boolean; virtual; abstract;
    function  OpenMap (aMapFileName: string): boolean; virtual; abstract;

    procedure CloseMap (); virtual; abstract;


    procedure DoOnMapNewXYShape (Sender: TObject;
                                 var aXYPoints: TXYPointArrayF;
                                 aFillValue,
                                 aRow,aCol: integer); virtual; abstract;

    procedure DoOnMapNewBLShape (Sender: TObject;
                                // var aBLPoints: TBLPointArrayF;
                                 aBLPoints: TBLPointArray;
                                 aFillValue,
                                 aRow,aCol: integer); virtual; abstract;

  protected
    procedure ExecuteProc (); override;
  public

    constructor Create;
    destructor Destroy; override;

//    function SaveMatrixToBitmapFile_old(aFileName: string): boolean;
  end;


//======================================================
//======================================================
implementation

const
 //  DEF_RASTER_EXT = '.gif';
   //DEF_RASTER_EXT = '.jpg';
   DEF_RASTER_EXT = '.tif';
//   DEF_RASTER_EXT = '.bmp';

const
//  BLANK_COLOR = clBlack;
  BLANK_COLOR = clWhite;


// ---------------------------------------------------------------
constructor TTask_BinToMap.Create;
// ---------------------------------------------------------------
begin
  inherited;

  FColorMatrix:=TIntMatrix.Create;
  
  FMatrixDecomposer:=TMatrixDecomposer.Create;
  FMatrixDecomposer.OnNewBLShape:=DoOnMapNewBLShape;
  FMatrixDecomposer.OnNewXYShape:=DoOnMapNewXYShape;

  FMatrixDecomposer.Progress2Step:=50;
end;

// ---------------------------------------------------------------
destructor TTask_BinToMap.Destroy;
// ---------------------------------------------------------------
begin
  FreeAndNil(FColorMatrix);
  FreeAndNil(FMatrixDecomposer);

  inherited;
end;



//------------------------------------------------------
function TTask_BinToMap.SaveMatrixToBitmapFile(aFileName: string): boolean;
//------------------------------------------------------
//const
 // MSG_ERROR         = '���������� ������� ��������� ����������� �������� %g ��. ���������� ��������� ������ �������� ����.';
 // MSG_ERROR_CONVERT = '���������� ��������� bmp � tiff. ���������� ��������� ������ �������� ����.';

var r,c: integer;
    iColor,iRange: Integer;
    dValue : Double;
    s,sName : string;
    iValue: Integer;
//    oBitmap: TBitmap;

  oBmp: TBmpFile;
begin

  DoLog ('SaveMatrixToBitmapFile');

  Result:= false;


  oBmp:=TBmpFile.Create_Width_Height(aFileName, FValuesMatrix.ColCount, FValuesMatrix.RowCount);



    Progress2Step:=100;


    with FValuesMatrix do
      for r:=0 to RowCount-1 do begin
      for c:=0 to ColCount-1 do
//        if (not IsNull(r,c)) then
      begin

(*        if (IsNull(r,c)) then
        begin
          oBmp.Write(0);
          Continue;
        end;
*)

        if Terminated then
        begin
          Result:= false;
          exit;
        end;

        if RecordType = dtFloat then
          dValue:= ItemsAsFloat[r,c]
        else
          dValue:= Items[r,c];

        //----------------
//        iColor:= g_Params_bin_to_map.GetColor (dValue, iRange);
        iColor:= g_Params_bin_to_map.GetColor (dValue, iRange);
        if iColor < 0  then
          iValue:= BLANK_COLOR;

        if iValue=0 then
          iColor:= BLANK_COLOR;


    //    oBitmap.Canvas.Pixels[c,r]:= iColor; //!!!!!!!

//        if iColor=0 then
          oBmp.Write(iColor);
  //      else
   //       oBmp.Write(iColor)


          //----------------
      end;

      oBmp.Writeln();

      DoProgress2(r, RowCount);
    end;


    FreeAndNil(oBmp);

    Result:=true;

//
//    try
////      oBmp.Free;
//
//      //Result:=
//    //  Result:=
//   //   img_SaveBitmapToFile (oBitmap, aFileName); //!!!!!!
//
//    except
//      DoLog (MSG_ERROR_CONVERT);
//      g_log.AddError('TTask_BinToMap.SaveMatrixToBitmapFile', MSG_ERROR_CONVERT);
//
//    end;

//  end;


(*
  oBmp.CloseFile;
  oBmp.Free;
*)

 // oBitmap.Free;

end;



//------------------------------------------------------
procedure TTask_BinToMap.DeleteColorMatrix();
//------------------------------------------------------
var
  sName1,  sName: string;
begin

{$IFNDEF debug}

  sName := ChangeFileExt(FMapFileName,'_color_matrix');
 // sName2:= sName+'_mirror';
  sName1:= sName+'.bin';
 // sName3:= sName2+'.bin';

  SysUtils.DeleteFile(sName);
  SysUtils.DeleteFile(sName1);
  //SysUtils.DeleteFile(sName2);
 // SysUtils.DeleteFile(sName3);

{$ENDIF}

end;

//------------------------------------------------------
function TTask_BinToMap.PrepareColorMatrix (aMatrix: TOrdinalMatrix): boolean;
//------------------------------------------------------
var r,c: integer;
  iRange: integer;
  i: Integer;
    dValue : Double;
    sFileName : string;

  iColor: integer;
begin
  DoLog ('Preparing ColorMatrix ...');

  Result:= false;

  //��������� ������� ������
  sFileName := ChangeFileExt(FMapFileName,'_color_matrix');

{  CreateIntMatrix(sFileName,
                  aMatrix.TopLeft.X, aMatrix.TopLeft.Y,
                  aMatrix.RowCount, aMatrix.ColCount,
                  aMatrix.Step, aMatrix.ZoneNum);
}

  FColorMatrix.FileName:=sFileName;
  FColorMatrix.SetParams (aMatrix.TopLeft.X, aMatrix.TopLeft.Y,
                          aMatrix.RowCount, aMatrix.ColCount,
                          aMatrix.Step, aMatrix.ZoneNum);

  //FRangeMatrix.SetMatrixSize(aMatrix.RowCount, aMatrix.ColCount);

//  FColorMatrix.LoadFromFile (sFileName, fmOpenReadWrite);
//  FColorMatrix.LoadFromFile (sFileName, fmOpenRead);

  FColorMatrix.FillBlank;
//  FRangeMatrix.FillBlank;

  Progress2Step:=100;

  with aMatrix do
    for r:=0 to RowCount-1 do
    begin
      for c:=0 to ColCount-1 do
        if (not IsNull(r,c)) then
      begin
        if Terminated then begin
          Result:= false;
          exit;
        end;

        if aMatrix.RecordType = dtFloat then
          dValue:= aMatrix.ItemsAsFloat[r,c]
        else
          dValue:= Items[r,c];

        iColor:= g_Params_bin_to_Map.GetColor (dValue,iRange);

        FColorMatrix[r,c]:= iColor;

     //   FRangeMatrix[r,c]:= iRange;

        i:=FColorMatrix[r,c];
      end;
//      if c=0 then
      DoProgress2(r, RowCount);
    end;

  FColorMatrix.SaveToFile (sFileName);

//  FColorMatrix.SaveToTxtFile (sFileName);

  Result:= true;
end;


//------------------------------------------------------
function TTask_BinToMap.ExecuteBinMatrix: Boolean;
//------------------------------------------------------
var // oMatrix : TOrdinalMatrix;
  sImgFileName: string;
  blBounds: TBLRect;
  sExt: string;

   // bool: boolean;

begin
{  FMatrixFileName:='f:\_los\los.int';
  FMapFileName:='f:\_los\los.tab';
}
  //oMatrix:=matrix_CreateMatrixByFileName (FMatrixFileName);

  FValuesMatrix:=Matrix_CreateMatrixByFileName (FMatrixFileName);

  if not Assigned(FValuesMatrix) then
  begin
//    raise Exception.Create('�� ������� �������' + FMatrixFileName );
  //  g_Log.AddError('TdmMain.DoProcessException', E.Message);
    g_Log.AddError('TTask_BinToMap.ExecuteBinMatrix', '�� ������� �������' + FMatrixFileName);

    result := False;
    exit;
//    ExitProcess(1);
  end;

    //Exit;

  DoLog ('');
  DoLog ('������ ����� :' + FMatrixFileName);

  if not FValuesMatrix.LoadFromFile (FMatrixFileName, fmOpenReadWrite) then
  begin
    DoLog ('���� �� ����������: ' + FMatrixFileName);
  end
  else begin
    DoLog (Format('�������: %s;  ����� : %s',
           [FMatrixFileName, FMapFileName]));

    sExt:=LowerCase(ExtractFileExt (FMatrixFileName));

    if Pos('kgr', FMatrixFileName)>0 then FMapType:= mtKgr else
    if Pos('kup', FMatrixFileName)>0 then FMapType:= mtKup
                                     else FMapType:= mtOther;


    // ���� �������, ��� ��� ���� ����� !!!!!
    if (sExt='.kgr')  then //and (not dmParams_bin_to_MIF.FIsStatistic)
    begin
      g_Params_bin_to_Map.ColorSourceType1:=csCells;

   //   dmParams.PrepareCellsColorRanges();
    //  dmParams.SaveCellsSchemaXmlFile(FMatrixFileName, FMapFileName);
    end
    else begin
      g_Params_bin_to_Map.ColorSourceType1:=csRanges;
      g_Params_bin_to_Map.SetColorSchema (FColorSchemaID);

//      dmParams_bin_to_MIF.SaveColorSchemaXmlFile (FColorSchemaID, FMatrixFileName, FMapFileName);
    //  dmParams.SaveColorSchemaToIni (ChangeFileExt(FMapFileName, '.ini'), FMatrixFileName);

    end;

 //   if (dmParams_bin_to_MIF.FIsStatistic) then
  //     dmParams_bin_to_MIF.PrepareStatisticColorRanges();

    if g_Params_bin_to_Map.MapType = mtRaster then
    begin
      SaveMatrixToRasterFile_MIF (FMapFileName);
    end else

//    FMatrix:= oMatrix;

    if PrepareColorMatrix (FValuesMatrix) then// begin
///////////////////////
  //  if sExt='.kgr' then PrepareRangeMatrix (FValuesMatrix);
/////////    PrepareRangeMatrix (oMatrix); //if sExt='.kgr' then


      case g_Params_bin_to_Map.MapType of

        mtRaster: SaveColorMatrixToRasterFile (FMapFileName); // raster map

        mtVector: begin
                 //   sImgFileName:= ChangeFileExt(FMapFileName, '_raster'+DEF_RASTER_EXT);
                  //  SaveColorMatrixToRasterFile (sImgFileName); // raster map

                    SaveColorMatrixToVectorFile (FMatrixFileName,FMapFileName); // vector map
                  end;
      end;
  //  end;
  end;

  FValuesMatrix.Free;
  FColorMatrix.ClearMemory;

 // FMatrixDecomposer.ClearMemory;

  DeleteColorMatrix();
  result := True;
end;

//------------------------------------------------------
procedure TTask_BinToMap.ExecuteProc;
//------------------------------------------------------
var
  I: Integer;
  oMatrixFile: TBinMatrixFile;
  iCount: integer;
begin
  FMatrixDecomposer.OnProgress2:=OnProgress2;

 // dmParams.mem_Matrix.First;
  //dmParams.mem_Point_Groups.First;

  //------------------------------------------------------
  // Bin Matrix
  //------------------------------------------------------
  iCount:=g_Params_bin_to_Map.MatrixArrayList.Count;

  for I := 0 to iCount-1 do
  begin

//  end;

//  with dmParams.mem_Matrix do
 //   while (not EOF) and (not Terminated) do
   // begin
      DoProgress (i, iCount);

      oMatrixFile := g_Params_bin_to_Map.MatrixArrayList[i];

      FMatrixFileName:= oMatrixFile.MatrixFILENAME;
      FMapFileName   := oMatrixFile.MapFileName;
      FColorSchemaID := oMatrixFile.ColorSchema_ID;
//      FMapKind       := FieldByName(FLD_MAP_KIND).AsInteger;
      FCaption       := oMatrixFile.CAPTION;

      if not ExecuteBinMatrix () then
        exit;

    //  Next;
    end;

end;


end.

