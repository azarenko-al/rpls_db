unit Unit4;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TMultiP;

type
  Tfrm_Bmp_to_Tif = class(TForm)
    MultiImage1: TPMultiImage;
    procedure FormCreate(Sender: TObject);
  private
  
  public
    class procedure ExecProc (aBitmap: TBitmap);
  end;

//var
//  frm_Bmp_to_Tif: Tfrm_Bmp_to_Tif;

//=================================================
implementation {$R *.DFM}
//=================================================

class procedure Tfrm_Bmp_to_Tif.ExecProc (aBitmap: TBitmap);
begin
  with Tfrm_Bmp_to_Tif.Create(Application) do
  begin
    MultiImage1.Picture.Bitmap:= aBitmap;

    DeleteFile('C:\Temp.tif');
    MultiImage1.SaveAsTIF('C:\Temp.tif');

    Free;
  end;
end;

procedure Tfrm_Bmp_to_Tif.FormCreate(Sender: TObject);
begin
(*  MultiImage1.Picture.Bitmap:=

  MultiImage1.imagename:='C:\Temp.bmp';

  MultiImage1.SaveAsTIF('C:\Temp.tif');*)
end;

end.
