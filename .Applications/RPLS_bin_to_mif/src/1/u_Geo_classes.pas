unit u_Geo_classes;

//===================================================================
interface
//===================================================================
uses Sysutils, Math, Messages, Windows, Classes,
     u_GEO,
     u_func;




//===================================================================
implementation
//===================================================================

   {
type
  TXYPointItem = class
  public
    X,Y: double;
  end;

  TBLPointItem = class
  public
    Value: TBLPoint;
  end;

  TBLPointList = class(TList)
  private
    function  GetItem (Index: integer): TBLPointItem;
    function  GetPoint (Index: integer): TBLPoint;
    procedure SetPoint (Index: integer; aValue: TBLPoint);

  public
    procedure AddItem (aValue: TBLPoint);
    procedure Clear; override;

    procedure MakeBLPointArray(var aBLPoints: TBLPointArray);

    property  Items [Index: integer]: TBLPointItem read GetItem; default;
    property  Points [Index: integer]: TBLPoint read GetPoint write SetPoint;
  end;
     }

{ TBLCalcPointItem }

{ TBLPointList }
 {
procedure TBLPointList.AddItem(aValue: TBLPoint);
var obj: TBLPointItem;
begin
  obj:=TBLPointItem.Create;
  obj.Value :=aValue;
  Add(obj);
end;

procedure TBLPointList.Clear;
var i: integer;
begin
  for i:=0 to Count-1 do Items[i].Free;
  inherited;
end;



function TBLPointList.GetPoint(Index: integer): TBLPoint;
begin
  Result:=TBLPointItem(inherited Items[Index]).Value;
end;

procedure TBLPointList.SetPoint(Index: integer; aValue: TBLPoint);
begin
  TBLPointItem(inherited Items[Index]).Value:=aValue;
end;

function TBLPointList.GetItem(Index: integer): TBLPointItem;
begin
  Result:=TBLPointItem(inherited Items[Index]);
end;


procedure TBLPointList.MakeBLPointArray(var aBLPoints: TBLPointArray);
var i: integer;
begin
  aBLPoints.Count:=Count;

//  SetLength (Result, Count);
  for i:=0 to Count-1 do
    aBLPoints.Items[i]:=Points[i];


end;
   }

end.






{
type
  TXYPointList = class(TList)
  private
    function GetItem (Index: integer): TXYPointItem;
  public
    procedure Clear; override;
    procedure AddItem (aValue: TXYPoint); overload;
    procedure Press;
    function GetBLPointArray: TBLPointArray;
    property  Items [Index: integer]: TXYPointItem read GetItem; default;
  end;
}

{

function TXYPointList.GetItem (Index: integer): TXYPointItem;
begin
  Result:=TXYPointItem(inherited Items[Index]);
end;


procedure TXYPointList.AddItem (aValue: TXYPoint);
var obj: TXYPointItem;
begin
  obj:=TXYPointItem.Create;
  obj.X:=aValue.X;
  obj.Y:=aValue.Y;
  Add(obj);
end;


procedure TXYPointList.Press;
var i: integer;
begin
  for i:=Count-1 downto 1 do
    if (Items[i].X=Items[i-1].X) and
       (Items[i].Y=Items[i-1].Y)
    then begin
      Items[i].Free;  Delete (i);
    end;
end;


function TXYPointList.GetBLPointArray: TBLPointArray;
var i: integer;
    xyPoints: TXYPointArray;
begin
  SetLength (xyPoints, Count);
  for i:=0 to Count-1 do begin
    xyPoints[i].X:=Items[i].X;
    xyPoints[i].Y:=Items[i].Y;
  end;

  Result:=geo_XYPoints_to_BLPoints (xyPoints);
end;

procedure TXYPointList.Clear;
var i: integer;
begin
  for i:=0 to Count-1 do Items[i].Free;
  inherited;
end;

}


