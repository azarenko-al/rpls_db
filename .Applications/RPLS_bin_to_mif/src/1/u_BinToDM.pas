unit u_BinToDM;

interface
uses SysUtils,

     u_BinToMap,

     u_GEO,
     u_GEO_classes,

     u_MatrixDecomposer,
     u_Neva_Func
     ;

type
  //------------------------------------------------------
  // ������������ ����� DM �� 2D ��������� �������
  //------------------------------------------------------
  TBinToDM = class(TBinToMap)
  private
    procedure SaveColorMatrixToVectorFile(aFileName: string); override;
  protected
    procedure DoOnMapNewBLShape (Sender: TObject; aBLPoints: TBLPointList; aFillValue: integer); override;
    procedure DrawSquare (aBLPoint: TblPoint; aColor: integer; aStep: integer; aIsFramed: boolean); override;

    procedure CloseMap; override;
    function  OpenMap (aMapFileName: string; aBLBounds: TBLRect): Boolean;  override;
  end;


//======================================================
implementation
//======================================================

const
   MCODE_REGION    = 20000010;  //��� ������ ������������
   MCODE_MEAS_AREA = 20000012;  //��� �������� ���������

   OBJ_NAME= '100';



procedure TBinToDM.CloseMap;
begin
  neva_Close_Map;
end;


procedure TBinToDM.DoOnMapNewBLShape(Sender: TObject;
        aBLPoints: TBLPointList; aFillValue: integer);
var
  neva_cl, iBrush, cl, i: Integer;
  blPoints: TBLPointArray;

begin
  iBrush:=NEVA_PATTERN_SOLID;

  cl:=aFillValue;

    {
    if FOptions.IsGradientFill then begin
      cl:=aFillValue;
    end else begin
      if aFillValue <> -1 then begin
                             cl:=FOptions.ColorValues[aFillValue].Color;
                             if FOptions.ColorValues[aFillValue].IsTransparent then
                               iBrush:=NEVA_PATTERN_DOTS;
                          end
                          else cl:=0;
    end;
  }
    // don't show black color
  if cl<>0 then begin
    blPoints:=aBLPoints.MakeBLPointArray;
    neva_Insert_BLPolygone (MCODE_REGION, blPoints, cl, iBrush, false);
  end;
end;

//------------------------------------------------------
procedure TBinToDM.DrawSquare(aBLPoint: TblPoint;
           aColor: integer;
           aStep: integer;
           aIsFramed: boolean);
//------------------------------------------------------

begin
  neva_Insert_Square (MCODE_MEAS_AREA, aBLPoint, aStep, aColor, false);
  if aIsFramed then
    neva_Insert_Frame (MCODE_MEAS_AREA, aBLPoint, aStep);
end;

//------------------------------------------------------
function TBinToDM.OpenMap(aMapFileName: string; aBLBounds: TBLRect): Boolean;
//------------------------------------------------------
begin
  aMapFileName:=ChangeFileExt(aMapFileName, '.dm');
  neva_Create_Map (aMapFileName, '100', aBLBounds);
  neva_Open_Map (aMapFileName, fmOpenWrite);

  Result:=True;
  {
  if not neva_Open_Map(aMapFileName, False) then begin
    DoLog('������ ��� �������� �����: '+aMapFileName); Exit;
  end;
  }
end;

//------------------------------------------------------
procedure TBinToDM.SaveColorMatrixToVectorFile(aFileName: string);
//------------------------------------------------------
var blPoints: TBLPointArray;
begin
  FMatrixDecomposer.OnProgress2:=OnProgress2;

  DoLog ('������ ������������ �����: '+ TimeToStr(Now));

  //  FRangeMatrix.SaveToTxtFile ('t:\aaa.txt');

  OpenMap (aFileName, FColorMatrix.GetBLBounds);

  //  Log('�������� ������� ��� ��������...');
  //  CutPolygon();

  neva_Goto_Down();

  FMatrixDecomposer.Decompose(FColorMatrix);

  DoLog ('����� ������������ �����: ' + TimeToStr(Now));
  DoLog (Format('����� / c����������� ����������: %d / %d',
          [FColorMatrix.RowCount * FColorMatrix.ColCount,
           FMatrixDecomposer.ShapesTotal]));

  CloseMap();

end;


{
//------------------------------------------------------
procedure TBinToDM.SaveRangeMatrixToFile (aFileName: string);
//------------------------------------------------------
var blRect: TBLRect;
   i,j,iTotal,iCount: integer;
begin
  iTotal:=FRangeRegions.PolygonCount;
  iCount:=0;

  OpenMap (aFileName, FRangeRegions.GetBounds);

  for i:=0 to FRangeRegions.Count-1 do
  for j:=0 to FRangeRegions[i].PolygonCount-1 do
  begin
    Inc(iCount);
    DoProgress (iCount, iTotal, Terminated);

    neva_Insert_BLPolygone (
          FRangeRegions[i].Code,
          FRangeRegions[i].Polygons[j].Points,
          0, NEVA_PATTERN_DEFAULT, False);
  end;

  CloseMap();
end;
}


end.



