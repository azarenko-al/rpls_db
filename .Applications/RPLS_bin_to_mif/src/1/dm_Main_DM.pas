unit dm_Main_DM;

interface

uses
  Classes, Forms, SysUtils,

  dm_Params,

  u_BinToMap_DM
  ;

type
  TdmMain = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
  private
    FInFileName: string;
    FMapMaker: TBinToMap_DM;
  end;

var
  dmMain: TdmMain;

//========================================================
implementation {$R *.DFM}
//=================================================================
const
 /////////////////////
//  TEMP_FILENAME='u:\data\in.xml';
//  TEMP_FILENAME='c:\temp\_bin_to_map.xml';
//  TEMP_FILENAME='c:\temp\_bin_to_map_interf.xml';

  TEMP_FILENAME = 'c:\temp\rpls\bin_to_mif.xml';
//  TEMP_FILENAME='t:\_bin_to_map.xml';


//-----------------------------------------------------------------
procedure TdmMain.DataModuleCreate(Sender: TObject);
//-----------------------------------------------------------------
begin

  FInFileName:=ParamStr(1);

 // MsgDlg (Params.InFileName + '   ,  ' + Params.OutFileName);
 // Params.InFileName:='t:\in.xml';
 // Params.OutFileName:='t:\out.xml';

  if not FileExists(FInFileName) then
  begin
    FInFileName:=TEMP_FILENAME;
  end;

//  Exit;

  dmParams.LoadFromXML (FInFileName);
  dmParams.MapType:=mtVector;

  FMapMaker:=TBinToMap_DM.Create;
  FMapMaker.ExecuteDlg ('���������� ����...');
  FMapMaker.Free;

end;


end.

