unit u_ini_Params_Bin_To_Map_classes;

interface

uses
  Classes, SysUtils, XMLIntf, IniFiles, Variants, Graphics,

  System.Generics.Collections, 
 

  
  u_xml_document,

  u_img;

type
  TColorSchemaList = class;
  TColorSchemaRangeList = class;
  TColorSchema= class;


  TBinMatrixFileType = (ftNone_,ftKUP_,ftKGR_);

  // ---------------------------------------------------------------
  TBinMatrixFile = class
  // ---------------------------------------------------------------
  public
    ColorSchema: TColorSchema;

  //  CorrCoeff: integer;

    //DataType

    MatrixFileName:  string;
    CAPTION       :  String;
    MapFileName   :  String;
             
    IsMegafon : Boolean;
    
    
    ColorSchema_ID :  Integer;

    ColorSchema_FileName :  String;

  
    constructor Create; //(Collection: TCollection); override;
    destructor Destroy; override;

    function GetFileType: TBinMatrixFileType;
  end;


  // ---------------------------------------------------------------
  TColorSchemaRange = class 
  // ---------------------------------------------------------------
  public
    MinValue : integer; // �������� � �������
    MaxValue : integer; // �������� � �������
                           
    Color    : integer; // ���� �� �����
    MaxColor : integer;
    MinColor : integer;

    comment : string;

    Type_    : (rtFixed,rtGradient);

 //  Caption  : string;
   // IsTransparent: boolean;
  end;

  // ---------------------------------------------------------------
  TColorSchemaRangeList = class(TList<TColorSchemaRange>)
  // ---------------------------------------------------------------         
  private
  //  function GetRangeIndex(aValue: integer): integer;
  public
    function AddItem: TColorSchemaRange;

    function GetColor(aValue: Double): integer;


 //   property Items[Index: Integer]: TColorSchemaRange read GetItems; default;
  end;
  

  // ---------------------------------------------------------------
  TColorSchema =  class
  // ---------------------------------------------------------------
  private
   // Index : Integer;
    Name111: string;
  public
    ID : Integer;
    Ranges: TColorSchemaRangeList;

    constructor Create; //(Collection: TCollection); override;
    destructor Destroy; override;

    procedure Init_Megafon_Range_30_130;

    function GetColor(aValue: Double; var aRangeIndex: integer): integer;

    procedure LoadFromXMLFile(aFileName: string);
    procedure LoadFromXmlNode(aParentNode: IXMLNode);

    procedure SaveToIniFile(aIniFileName: string);

    procedure SaveToXML(aFileName: string);

    procedure SaveToXMLNode(aParentNode: IXMLNode);

  end;


  TColorSchemaList = class(TList<TColorSchema>)
  private
  public
    function AddItem: TColorSchema;
    function AddItem_MEgafon: TColorSchema;
    function FindByID(aID: integer): TColorSchema;

  end;


  TBinMatrixFileList = class(TList<TBinMatrixFile>) 
  public
    function AddItem: TBinMatrixFile;
  end;


//  TCell = class(TCollectionItem)
// ---------------------------------------------------------------
  TCell = class
  // ---------------------------------------------------------------
  public
    ID:      Integer;
    Color:   Integer;
//   CellID : Integer;
    CellID : string;
    LAC     : string;
  end;


//  TCellList = class(TCollection)
// ---------------------------------------------------------------
  TCellList = class(TList)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TCell;
  public
//    constructor Create;
    function AddItem: TCell;

    procedure Clear; override;

    function GetColorByID(aValue: Integer; var aRangeIndex: integer): Integer;

    function FindByID(aID: integer): TCell;

    procedure SaveToXMLFile(aFileName: string);
    procedure SaveToXMLNode(aParentNode: IXMLNode);
    procedure SortByCellID;

    property Items[Index: Integer]: TCell read GetItems; default;
  end;


implementation

const
//  BLANK_COLOR = clBlack;
//  BLANK_COLOR = clWhite;

  ATT_CELLID = 'CELLID';
  ATT_LAC = 'LAC';


  DEF_FIXED_COLOR    = 0;
  DEF_GRADIENT_COLOR = 1;

  FLD_step = 'step';

 // FLD_Min_Value = 'Min_Value';
//  FLD_SITE_NAME = 'SITE_NAME';
//  FLD_CELL_NAME = 'CELL_NAME';

  FLD_Min_Value='Min_Value';
  FLD_Max_Value='Max_Value';
  FLD_Min_Color='Min_Color';
  FLD_Max_Color='Max_Color';



//constructor TColorSchemaRangeList.Create;
//begin
//  inherited Create(TColorSchemaRange);
//end;

function TColorSchemaRangeList.AddItem: TColorSchemaRange;
begin
  result := TColorSchemaRange.Create;
  Add(Result);
end;

//---------------------------------------------------------
function TColorSchemaRangeList.GetColor(aValue: Double): integer;
//---------------------------------------------------------
// var aRangeIndex: integer):
var i: integer;
begin
  Result:=-1;

  for i:=0 to Count-1 do
//          for i:=0 to High(Ranges) do

   if (Items[i].MinValue<=aValue) and
      (aValue<Items[i].MaxValue)
   then
     with Items[i] do
     begin
         case Type_ of
           rtFixed:    Result:=Color;
           rtGradient: Result:=img_MakeGradientColor (aValue,
                                         MinValue, MaxValue,
                                         MinColor, MaxColor);
         end;

//          aRangeIndex:=i;

         break;
       end;
end;


procedure TColorSchema.Init_Megafon_Range_30_130;
var
  I: Integer;
  oItem: TColorSchemaRange;
begin
  ID:=-1;
  
  Ranges.Clear;

  for I := -130 to -30 do
  begin
    oItem:=Ranges.AddItem;
    
    oItem.MinValue:=i;
    oItem.MaxValue:=i;
    oItem.Color:=i;

  end;


end;



       

function TColorSchemaList.AddItem: TColorSchema;
begin
  result := TColorSchema.Create; 
  Add(Result);
end;

function TColorSchemaList.AddItem_MEgafon: TColorSchema;
begin
  result := TColorSchema.Create;   
  result.Init_Megafon_Range_30_130;
  Add(Result);
end;


//-----------------------------------------------------------------
function TColorSchemaList.FindByID(aID: integer): TColorSchema;
//-----------------------------------------------------------------
var
  i: integer;
begin
  Result := nil;

  for I := 0 to Count-1 do
    if Items[i].ID=aID then
    begin
      Result :=Items[i];
      Exit;
    end;
end;

constructor TColorSchema.Create;
begin
  inherited; 
  Ranges := TColorSchemarangeList.Create();
end;

destructor TColorSchema.Destroy;
begin
  FreeAndNil(Ranges);
  inherited; // Destroy;
end;

//---------------------------------------------------------
function TColorSchema.GetColor(aValue: Double; var aRangeIndex: integer):
    integer;
//---------------------------------------------------------
var i: integer;
begin
  Result:=-1;
  aRangeIndex:=-1;

  Assert (Ranges.Count>0);


//   with FColorSchema do //List[FColorSchemaIndex]
  for i:=0 to Ranges.Count-1 do
//          for i:=0 to High(Ranges) do

   if (Ranges[i].MinValue<=aValue) and (aValue<=Ranges[i].MaxValue) then
     with Ranges[i] do
     begin
         case Type_ of
           rtFixed:    Result:=Color;
           rtGradient: Result:=img_MakeGradientColor (aValue,
                                         MinValue, MaxValue,
                                         MinColor, MaxColor);
         end;

         aRangeIndex:=i;
         break;
       end;


end;

//--------------------------------------------------------------
procedure TColorSchema.SaveToXML(aFileName: string);
//--------------------------------------------------------------
var
  vRoot,vGroup: IXMLNode;
  oXMLDoc: TXMLDoc;

begin
  oXMLDoc := TXMLDoc.Create;

  vRoot:=oXMLDoc.DocumentElement;

  // -------------------------
 // vGroup := vRoot.AddChild('COLOR_SCHEMAS');
  SaveToXMLNode (vRoot);

  oXMLDoc.SaveToFile (aFileName);

  FreeAndNil(oXMLDoc);

end;


//-------------------------------------------------------------------
procedure TColorSchema.LoadFromXmlNode(aParentNode: IXMLNode);
//-------------------------------------------------------------------
var i: integer;
  vNode: IXMLNode;
 iLen: Integer;

 oRange: TColorSchemaRange;
begin
// iLen := aNode.ChildNodes.Length;

//     SetLength(ColorSchemaList[aIndex].Ranges, iLen);

//  with aColorSchema do
   for i:=0 to aParentNode.ChildNodes.Count - 1 do
   begin
     vNode:=aParentNode.ChildNodes[i];

     oRange:=Ranges.AddItem;

   //  oRange.MinValue:= xml_GetFloatAttr(vNode, FLD_Min_Value);
   //  oRange.MaxValue:= xml_GetFloatAttr(vNode, FLD_Max_Value);

     oRange.MinValue:= xml_GetIntAttr(vNode, FLD_Min_Value);
     oRange.MaxValue:= xml_GetIntAttr(vNode, FLD_Max_Value);

     oRange.COLOR:=    xml_GetIntAttr  (vNode, ATT_COLOR);
     oRange.MinColor:= xml_GetIntAttr  (vNode, FLD_Min_Color);
     oRange.MaxColor:= xml_GetIntAttr  (vNode, FLD_Max_Color);

     oRange.comment:= xml_GetStrAttr   (vNode, 'comment');

     if xml_GetIntAttr(vNode, ATT_TYPE)=0 then
     begin
       oRange.TYPE_:=rtFixed;
     end
     else
       oRange.TYPE_:=rtGradient;   
   end;
end;



//-------------------------------------------------------------------
procedure TColorSchema.SaveToXMLNode(aParentNode: IXMLNode);
//-------------------------------------------------------------------
var
  I: Integer;
  j: Integer;
  vGroup: IXMLNode;

//  oColorSchema: TColorSchema;
  oRange: TColorSchemaRange;

begin
  vGroup := xml_AddNode (aParentNode, 'item',
                      [//xml_Par(ATT_NAME, Name),
                       ATT_ID, ID
                       ]);

  Assert (Ranges.Count>0);


//  for j := 0 to oColorSchema.Ranges.Count - 1 do
  for j := 0 to Ranges.Count - 1 do
  begin
    oRange := Ranges[j];

    xml_AddNode (vGroup, 'range',
             [ATT_COLOR,   oRange.Color,
              'min_value', oRange.minvalue,
              'max_value', oRange.maxvalue,
              'min_color', oRange.mincolor,
              'max_color', oRange.maxcolor,
              'comment',   oRange.comment,
              ATT_TYPE,    oRange.TYPE_
              ]);
  end;

end;


//-------------------------------------------------------------------
procedure TColorSchema.SaveToIniFile(aIniFileName: string);
//-------------------------------------------------------------------
var
  I: Integer;
  j: Integer;
  vGroup: IXMLNode;

//  oColorSchema: TColorSchema;
  oRange: TColorSchemaRange;

  oIni: TmemIniFile;
  sSection: string;
begin
  oIni:=TmemIniFile.Create(aIniFileName);

  oIni.WriteInteger('main','count',Ranges.Count);


  for j := 0 to Ranges.Count - 1 do
  begin
    oRange := Ranges[j];

    sSection:=IntToStr(j);

    oIni.WriteInteger(sSection,'color', oRange.Color);

//    oIni.WriteInteger(sSection,'label', oRange.minvalue);

    oIni.WriteInteger(sSection,'min_value', oRange.minvalue);
    oIni.WriteInteger(sSection,'max_value', oRange.maxvalue);
    oIni.WriteInteger(sSection,'min_color', oRange.mincolor);
    oIni.WriteInteger(sSection,'max_color', oRange.maxcolor);


(*
    xml_AddNode (vGroup, 'range',
             [xml_Par(ATT_COLOR,   oRange.Color),
              xml_Par('min_value', oRange.minvalue),
              xml_Par('max_value', oRange.maxvalue),
              xml_Par('min_color', oRange.mincolor),
              xml_Par('max_color', oRange.maxcolor),
              xml_Par(ATT_TYPE,    oRange.TYPE_)
              ]);*)
  end;


  oIni.UpdateFile;

  FreeAndNil(oIni);

(*  vGroup := xml_AddNode (aIniFileName, 'item',
                      [//xml_Par(ATT_NAME, Name),
                       xml_Par(ATT_ID,   ID)
                       ]);
*)
//  for j := 0 to oColorSchema.Ranges.Count - 1 do

end;





//constructor TBinMatrixFileList.Create;
//begin
//  inherited Create(TBinMatrixFile);
//end;

function TBinMatrixFileList.AddItem: TBinMatrixFile;
begin
  result := TBinMatrixFile.Create;
  Add(Result);
end;



function TCellList.AddItem: TCell;
begin
  result := TCell.create;
  Add(Result);

  //result := TCell(Add);
end;

// ---------------------------------------------------------------
function TCellList.FindByID(aID: integer): TCell;
// ---------------------------------------------------------------
var
  i: integer;
begin
  Result:=nil;

  for i:=0 to Count-1 do
    if Items[i].ID=aID then
    begin
      Result:=Items[i];
      Break;
    end;

end;

//---------------------------------------------------------
function TCellList.GetColorByID(aValue: Integer; var aRangeIndex: integer):
    Integer;
//---------------------------------------------------------
var i: integer;
begin
  Result:=-1;
  aRangeIndex:=-1;

  for i:=0 to Count-1 do
//        for i:=0 to High(FCells) do
    if Items[i].ID=aValue then
  begin
    Result:=Items[i].COLOR;
    aRangeIndex:=i;
    Break;
  end;

end;

// ---------------------------------------------------------------
function CompareNames(Item1, Item2: Pointer): Integer;
// ---------------------------------------------------------------
begin
//  Assert(Item1 <> Item2);

//  if TCell(Item1).ID = TCell(Item2).ID then
//    raise Exception.Create('');

  if TCell(Item1).CeLLID = TCell(Item2).CeLLID then
     Result := 0 else

//  if TCell(Item1).CeLLID = TCell(Item2).CeLLID then
  //  raise Exception.Create('');


  if TCell(Item1).CeLLID > TCell(Item2).CeLLID then
    Result := 1
  else
    Result := -1;

//  Result := (Item1 as TCell).CeLL, (Item2 as TCell).Name);
end;

procedure TCellList.Clear;
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    Items[i].Free;

  inherited;
end;

procedure TCellList.SortByCellID;
begin
  Sort(@CompareNames);

end;



function TCellList.GetItems(Index: Integer): TCell;
begin
  Result := TCell(inherited Items[Index]);
end;

//--------------------------------------------------------------
procedure TCellList.SaveToXMLFile(aFileName: string);
//--------------------------------------------------------------
var
  i: Integer;
  vRoot,vGroup: IXMLNode;
  oXMLDoc: TXMLDoc;

begin
  oXMLDoc := TXMLDoc.Create;

  vRoot:=oXMLDoc.DocumentElement;

  SaveToXMLNode (vroot);
//
//  vGroup := vRoot.AddChild('CELLS');
//  vGroup.Attributes['count'] := Count;
//
//  for i := 0 to Count - 1 do
//    xml_AddNode (vGroup, 'item',
//             [
//              xml_Par(ATT_CELLID,  Items[i].CellID),
//              xml_Par(ATT_COLOR,   Items[i].Color),
//              xml_Par(ATT_ID,      Items[i].ID)
//              ]);

  oXMLDoc.SaveToFile (aFileName);

  FreeAndNil(oXMLDoc);

end;

//--------------------------------------------------------------
procedure TCellList.SaveToXMLNode(aParentNode: IXMLNode);
//--------------------------------------------------------------
var
  i: Integer;
  vRoot,vGroup: IXMLNode;

begin
 ///////////////////
  SortByCellID();

  vGroup := aParentNode.AddChild('CELLS');
  vGroup.Attributes['count'] := Count;

  for i := 0 to Count - 1 do
    xml_AddNode (vGroup, 'item',
             [
              ATT_ID,      Items[i].ID,
              ATT_CELLID,  Items[i].CellID,
              ATT_LAC,     Items[i].LAC,
              ATT_COLOR,   Items[i].Color
              ]);

end;



//--------------------------------------------------------------
procedure TColorSchema.LoadFromXMLFile(aFileName: string);
//--------------------------------------------------------------
var
  vNode,vRoot: IXMLNode;
  oXMLDoc: TXMLDoc;

begin
  oXMLDoc := TXMLDoc.Create;

  oXMLDoc.LoadFromFile(aFileName);

  vRoot:=oXMLDoc.DocumentElement;

  //for MIF use rasters
  vNode := xml_FindNode(vRoot, 'item');
  if not VarIsEmpty(vNode) then
    LoadFromXmlNode (vNode);


  // -------------------------
 // vGroup := vRoot.AddChild('COLOR_SCHEMAS');
//  SaveToXMLNode (vRoot);

//  oXMLDoc.SaveToFile (aFileName);

  FreeAndNil(oXMLDoc);

end;

constructor TBinMatrixFile.Create; //(Collection: TCollection);
begin
  inherited; // Create(Collection);
  ColorSchema := TColorSchema.Create;//(nil);
end;

destructor TBinMatrixFile.Destroy;
begin
  FreeAndNil(ColorSchema);
  inherited;// Destroy;
end;


function TBinMatrixFile.GetFileType: TBinMatrixFileType;
begin

  if Pos('.kgr', MatrixFileName)>0 then result:= ftKgr_ else
  if Pos('.kup', MatrixFileName)>0 then result:= ftKup_
                                   else result:= ftNone_;


   // FileTypeStr1 : string; //kup,kgr,los,
 //   FileType1 : (ftNone,ftKUP,ftKGR1);//,ftLOS1); //kup,kgr,los,

//        sExt:=LowerCase(ExtractFileExt (FMatrixFileName));
//
//    if Pos('kgr', FMatrixFileName)>0 then FMapType:= mtKgr else
//    if Pos('kup', FMatrixFileName)>0 then FMapType:= mtKup
//                                     else FMapType:= mtOther;



//    FMapType: (mtKgr, mtKup, mtOther);

  // TODO -cMM: TBinMatrixFile.GetFileType default body inserted
end;


end.




{
//---------------------------------------------------------
function TColorSchemaRangeList.GetRangeIndex(aValue: integer): integer;
//---------------------------------------------------------
var i: integer;
begin
  Result:=-1;

  for i:=0 to Count-1 do
    if (Items[i].MinValue<=aValue) and
       (aValue<Items[i].MaxValue) then
    begin
      Result:=i;  Break;
    end;
end;
 }
 
