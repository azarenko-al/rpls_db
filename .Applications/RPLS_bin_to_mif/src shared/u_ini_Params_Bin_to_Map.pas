unit u_ini_Params_Bin_to_Map;

interface

uses
  SysUtils, Classes, Variants, XMLIntf,

  u_func,

  u_xml_document,

  u_ini_Params_Bin_To_Map_classes
  ;

type
  TParams_Bin_to_Map1 = class
  private

    function SaveCellsSchemaXmlFile1(aMatrixFileName, aMapFileName: string):
        Integer;

  public
  public
    Cells: TCellList;

    ColorSchema_Selected:  TColorSchema;

 // public
    ColorSchemaList: TColorSchemaList;
    BinMatrixArrayList: TBinMatrixFileList;

    MapExportType:   (mtRaster,
                      mtVector,
                      mtKML,
                      mtKMZ,
                      mtMIF,
                      mtMIF_TAB,
                      mtJSON,

                      mtGeoTiff,
                      mtGeoTiff_values
                      );

//    Transparence_0_255 : Integer; //0..255
    Transparence : Integer; //0..255

    ColorSourceType: (csCells,csRanges);


    constructor Create;
    destructor Destroy; override;

    function SetColorSchema(aID: integer): Boolean;

    function LoadFromXMLFile(aFileName: string): Boolean;
    procedure SaveToXMLFile(aFileName: string);

    procedure Clear;


  end;

var
  g_Params_bin_to_Map: TParams_Bin_to_Map1;



//==================================================================
//==================================================================
implementation

const
  TAG_COLOR_SCHEMAS = 'COLOR_SCHEMAS';

const
  FLD_SCHEMA_ID = 'SchemaID';
  FLD_FRAME     = 'FRAME';
  FLD_MAP_KIND  = 'MapKind';

  ATT_Transparence = 'Transparence';
  ATT_CellID = 'CellID';
  ATT_LAC = 'LAC';

  FLD_MAP  = 'map';

//  BLANK_COLOR = clBlack;
//  BLANK_COLOR = clWhite;


  DEF_FIXED_COLOR    = 0;
  DEF_GRADIENT_COLOR = 1;


// ---------------------------------------------------------------
constructor TParams_Bin_to_Map1.Create;
// ---------------------------------------------------------------
begin
  inherited;

  BinMatrixArrayList := TBinMatrixFileList.Create();

  ColorSchemaList := TColorSchemaList.Create();
  Cells := TCellList.Create();

//  Transparence_0_255 :=100;
  Transparence :=100;

end;


destructor TParams_Bin_to_Map1.Destroy;
begin
  FreeAndNil(Cells);
  FreeAndNil(BinMatrixArrayList);
  FreeAndNil(ColorSchemaList);

  inherited;

end;            

procedure TParams_Bin_to_Map1.Clear;
begin
  Cells.Clear;

// public
  ColorSchemaList.Clear;
  BinMatrixArrayList.Clear;

end;


//--------------------------------------------------------------
procedure TParams_Bin_to_Map1.SaveToXMLFile(aFileName: string);
//--------------------------------------------------------------

    //-------------------------------------------------------------------
    procedure DoExportColorSchemasToXMLNode(aParentNode: IXMLNode);
    //-------------------------------------------------------------------
    var
      I: Integer;
      oColorSchema: TColorSchema;

    begin
      for I := 0 to ColorSchemaList.Count - 1 do
      begin
        oColorSchema := ColorSchemaList[i];

        oColorSchema.SaveToXMLNode(aParentNode);

      end;

    end;

var
  I: Integer;
  vRoot,vGroup: IXMLNode;
  oXMLDoc: TXMLDoc;

  oMatrix : TBinMatrixFile;

  s: string;
  sMapType: string;
begin

  oXMLDoc := TXMLDoc.Create;


  vRoot:=oXMLDoc.DocumentElement;

  case MapExportType of
    mtRaster:  sMapType:='Raster';
    mtVector:  sMapType:='Vector';
    mtKML:     sMapType:='KML';
    mtKMZ:     sMapType:='KMZ';
    mtMIF:     sMapType:='MIF';
    mtMIF_TAB: sMapType:='MIF_TAB';
    mtJSON:    sMapType:='JSON';

    mtGeoTiff: sMapType:='GeoTiff';
    mtGeoTiff_values: sMapType:='GeoTiff_values';
  else
    raise Exception.Create('');
  end;

  vGroup:=vRoot.AddChild ('PARAMS');

  vGroup.Attributes['MapType']:=sMapType;
  vGroup.Attributes[ATT_Transparence]:=Transparence;

//  xml_SetAttribs (vGroup, [xml_Par('MapType', sMapType),
//                           xml_Par(ATT_Transparence, Transparence_0_255)
//                           xml_Par(ATT_Transparence, Transparence)
//                          ]);
//  xml_AddNode(vGroup, 'MapType', [ATT_VALUE, sMapType]);

  // -------------------------
  vGroup := vRoot.AddChild('COLOR_SCHEMAS');
  DoExportColorSchemasToXMLNode (vGroup);


  Cells.SaveToXMLNode(vRoot);

  // -------------------------

//  vGroup := vRoot.AddChild(TAG_COLOR_SCHEMAS);
 // dmColorSchema_export.ExportItemToXMLNode (Params.ColorSchemaID_LOS, vGroup);

  vGroup := vRoot.AddChild('MATRIXES');

  for I := 0 to BinMatrixArrayList.Count - 1 do
  begin
    oMatrix:=BinMatrixArrayList[i];

    xml_AddNode (vGroup, 'item',
                ['filename', oMatrix.MatrixFileName,
                 'map',      oMatrix.MapFileName,
                 'schema',   oMatrix.ColorSchema_ID,
                 'schema_FileName', oMatrix.ColorSchema_FileName

                // xml_par('transparent', oMatrix.Tr)
                ]);   
  end;                   


 // s:=vGroup.XML;

  oXMLDoc.SaveToFile (aFileName);

  FreeAndNil(oXMLDoc);

end;
        


//-----------------------------------------------------------------
function TParams_Bin_to_Map1.LoadFromXMLFile(aFileName: string): Boolean;
//-----------------------------------------------------------------

   //-------------------------------------------------------------------
   procedure DoLoadParams(aParamsNode: IXMLNode);
   //-------------------------------------------------------------------
   var i: integer;
       vNode,vNode2 : IXMLNode;
       sValue: string;
   begin
     sValue:=aParamsNode.Attributes['MapType'];

//     sValue:=xml_FindNodeAttr (aParamsNode, 'MapType', 'value');

     if Eq(sValue,'Raster')  then MapExportType:=mtRaster  else
     if Eq(sValue,'Vector')  then MapExportType:=mtVector  else
     if Eq(sValue,'KMZ')     then MapExportType:=mtKMZ     else
     if Eq(sValue,'MIF')     then MapExportType:=mtMIF     else
     if Eq(sValue,'MIF_TAB') then MapExportType:=mtMIF_TAB else
     if Eq(sValue,'JSON')    then MapExportType:=mtJSON    else
     if Eq(sValue,'KML')     then MapExportType:=mtKML     else

     if Eq(sValue,'GeoTiff')        then MapExportType:=mtGeoTiff else
     if Eq(sValue,'GeoTiff_values') then MapExportType:=mtGeoTiff_values

     else
       raise Exception.Create('');

//       MapType:=mtNone;

    {$IFDEF RASTER}
  //////  MapType:=mtRaster;
    {$ENDIF}

(*
    MapType:=mtKMZ;
    MapType:=mtRaster;
*)

//    Transparence_0_255 := xml_GetIntAttr (aParamsNode, 'Transparence', Transparence_0_255);
    Transparence := xml_GetIntAttr (aParamsNode, 'Transparence', Transparence);

//    Assert(Transparence_0_255>=0,   'Transparence_0_255>=0');
 //   Assert(Transparence_0_255<=255, 'Transparence_0_255<=255');


   //// Transparence_0_255:=255 - Transparence_0_255;

  //  Transparence:=100;

   end;


   //-------------------------------------------------------------------
   procedure DoAddRange(aNode: IXMLNode; aColorSchema: TColorSchema);
   //-------------------------------------------------------------------
   begin
       aColorSchema.LoadFromXmlNode(aNode);
   end;

   //-------------------------------------------------------------------
   procedure DoLoadCells(aNode: IXMLNode);
   //-------------------------------------------------------------------
   var i: Integer;
       vNode,vNode2 : IXMLNode;
       oCell: TCell;
   begin
      vNode:=xml_FindNode (aNode, 'CELLS');

      if Assigned(vNode) then
      begin

        for i := 0 to vNode.ChildNodes.Count - 1 do
        begin
          vNode2:=vNode.ChildNodes[i];

          oCell:=Cells.AddItem;

          oCell.ID    := xml_GetIntAttr (vNode2, ATT_ID);
          oCell.COLOR := xml_GetIntAttr (vNode2, ATT_COLOR);
//          oCell.CellID := xml_GetIntAttr (vNode2, ATT_CellID);
          oCell.CellID := xml_GetStrAttr (vNode2, ATT_CellID);
          oCell.LAC := xml_GetStrAttr (vNode2, ATT_LAC);

        end;
      end;


       Cells.SortByCellID();


   end;

   //-------------------------------------------------------------------
   procedure DoLoadMatrixList(aNode: IXMLNode);
   //-------------------------------------------------------------------
   var i: integer;

       s: string;
       vNode1,vNode2 : IXMLNode;
     iLen: Integer;
     oMatrixFile: TBinMatrixFile;
   begin
    // s:=xml_TagName(aNode  );

   //   vNode1 := xml_GetNodeByPath(aNode,  ['MATRIXES']);
      vNode1 := xml_FindNode(aNode, 'MATRIXES');


      if VarExists(vNode1) then
      begin
        iLen := AsInteger(vNode1.ChildNodes.Count);

         for i := 0 to vNode1.ChildNodes.Count - 1 do
         begin
           vNode2:=vNode1.ChildNodes[i];

           oMatrixFile:=BinMatrixArrayList.AddItem();


           oMatrixFile.MatrixFILENAME :=xml_GetAttr (vNode2, 'FileName');
           oMatrixFile.CAPTION        :=xml_GetAttr (vNode2, 'Caption');
           oMatrixFile.MapFileName    :=xml_GetAttr (vNode2, 'Map');
           oMatrixFile.ColorSchema_ID :=xml_GetIntAttr(vNode2, 'SCHEMA');

           oMatrixFile.ColorSchema_FileName :=xml_GetStrAttr(vNode2, 'SCHEMA_FileName');

           oMatrixFile.IsMegafon:=xml_GetStrAttr(vNode2, 'Megafon') = 'yes';

           if oMatrixFile.IsMegafon then
             oMatrixFile.ColorSchema_ID:=-1;
           
           
//           oMatrixFile.CorrCoeff :=xml_GetIntAttr(vNode2, 'CorrCoeff');

{
  <COLOR_SCHEMAS/>
  <PARAMS MapType="GeoTiff_values">
    <MapType value="GeoTiff_values"/>
  </PARAMS>
  <MATRIXES>
    <item filename="C:\temp\rpls\ANDREY\onega_LTE\KUP\35FD84A202F363291075D6B7023816F3C40BB58BE03654688BCCA796A0425B8B.kup" map="C:\Documents and Settings\user\Desktop\a_temp\GeoTIF\AA5311_1.tif" CorrCoeff="-10"/>
}
//           CorrCoeff="-10"

           s:=oMatrixFile.MatrixFILENAME;

////////           Assert(FileExists(oMatrixFile.MatrixFILENAME));


           if oMatrixFile.ColorSchema_FileName<>'' then
             oMatrixFile.ColorSchema.LoadFromXMLFile(oMatrixFile.ColorSchema_FileName);


//           BinMatrixArrayList[i].MAP_KIND      :=xml_GetIntAttr    (vNode2, FLD_MAP_KIND);

         end;
      end;
   end;

var
  b: boolean;
  iID: Integer;
//  v: variant;
  i: integer;
  vRoot,vNode,vNode1,vNode2,vNode3 : IXMLNode;
  vColorSchemasNode: IXMLNode;
  iLen: Integer;
  oXMLDoc: TXMLDoc;

  oColorSchema: TColorSchema;

begin
  result := False;

  if not FileExists(aFileName) then
    Exit;


//  if not assigned(FXMLDoc) then
  oXMLDoc:=TXMLDoc.Create;

//  oXMLDoc1:=TXMLDoc.Create;

  oXMLDoc.LoadFromFile(aFileName);

  vRoot := oXMLDoc.DocumentElement;


  if VarIsNull(vRoot) then
    Exit;


  //for MIF use rasters
  vNode := xml_FindNode(vRoot, 'PARAMS');
  if VarExists(vNode) then
    DoLoadParams (vNode);


  vColorSchemasNode := xml_FindNode(vRoot, 'COLOR_SCHEMAS');


  if vColorSchemasNode <> nil then
  begin
    iLen:=vColorSchemasNode.ChildNodes.Count;

    for i := 0 to vColorSchemasNode.ChildNodes.Count - 1 do
    begin
      vNode2:=vColorSchemasNode.ChildNodes[i];

      iID:=xml_GetIntAttr (vNode2, ATT_ID);

      oColorSchema:=ColorSchemaList.AddItem;

      oColorSchema.ID:=iID;

      DoAddRange (vNode2, oColorSchema);
    end;
  end;

  DoLoadMatrixList(oXMLDoc.DocumentElement);
  DoLoadCells(oXMLDoc.DocumentElement);
 // DoLoadPointLists(oXMLDoc.DocumentElement);

  FreeAndNil(oXMLDoc);

  //Megafon !!!
  ColorSchemaList.AddItem_MEgafon;
  

  result := True;

end;

//-----------------------------------------------------------------
function TParams_Bin_to_Map1.SaveCellsSchemaXmlFile1(aMatrixFileName,
    aMapFileName: string): Integer;
//-----------------------------------------------------------------
var
  I: Integer;
  oXMLDoc: TXMLDoc;
  vNode,vNodeSrc : IXMLNode;
begin
  oXMLDoc:=TXMLDoc.Create;

  xml_AddNode(oXMLDoc.DocumentElement, 'MATRIX',
                  ['MatrixFileName', aMatrixFileName]);

{  vNode :=xml_AddNodeTag(oXMLDoc.DocumentElement, 'COLOR_SCHEMAS');

  vNodeSrc:=FvColorSchemasNode.ChildNodes.Item(ColorSchemaList[i].Index);
}

/////////////  oXMLDoc.Document.ChildNodes. appendChild (FvCellsNode);

////////  oXMLDoc.SaveToFile( 'd:\_cells.xml');

  oXMLDoc.SaveToFile( ChangeFileExt(aMapFileName, '.xml'));
  oXMLDoc.Free;

  //dmParams.SaveColorSchemaXmlFile (FColorSchemaID, FMatrixFileName, FMapFileName);
end;

//-----------------------------------------------------------------
function TParams_Bin_to_Map1.SetColorSchema(aID: integer): Boolean;
//-----------------------------------------------------------------
begin
  ColorSchema_Selected:=ColorSchemaList.FindByID(aID);

end;

begin

end.




