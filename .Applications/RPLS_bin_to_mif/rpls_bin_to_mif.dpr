program rpls_bin_to_mif;



uses
  dm_Main__bin_to_MIF in 'src\dm_Main__bin_to_MIF.pas' {dmMain_bin_to_mif: TDataModule},
  Forms,
  u_BinToMap in 'src\u_BinToMap.pas',
  u_BinToMap_mif in 'src\u_BinToMap_mif.pas',
  u_MatrixDecomposer in 'src\u_MatrixDecomposer.pas',
  u_map_classes in 'src\u_map_classes.pas',
  u_BinToMap_export in 'src\u_BinToMap_export.pas',
  u_ini_Params_Bin_to_Map in 'src shared\u_ini_Params_Bin_to_Map.pas',
  u_ini_Params_Bin_To_Map_classes in 'src shared\u_ini_Params_Bin_To_Map_classes.pas',
  u_KML in 'W:\common XE\KML\u_KML.pas',
  u_kmz_simple in 'W:\common XE\KML\u_kmz_simple.pas',
  u_matrix_base in 'W:\common XE\matrix\u_matrix_base.pas',
  u_Matrix in 'W:\common XE\matrix\u_Matrix.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TdmMain_bin_to_mif, dmMain_bin_to_mif);
  Application.Run;
end.
