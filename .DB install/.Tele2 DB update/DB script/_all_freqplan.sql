
---------------------------------------


IF OBJECT_ID(N'[LinkFreqPlan].[view_LinkFreqPlan_LinkEnd]') IS NOT NULL

DROP VIEW [LinkFreqPlan].[view_LinkFreqPlan_LinkEnd]
GO
/****** Object:  View [LinkFreqPlan].[view_LinkFreqPlan_LinkEnd]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [LinkFreqPlan].[view_LinkFreqPlan_LinkEnd]
AS

SELECT  top 1000000   

--  dbo.fn_LinkEnd_Get_Link_ID (dbo.LinkEnd.id) AS link_id, 
 -- dbo.fn_LinkEnd_Get_Next_LinkEnd_ID (dbo.LinkEnd.id) AS next_linkend_id, 


M.*,

link.link_id, 
link.link_name, 
link.next_linkend_id, 
link.clutter_model_id,
link.length,
link.length_km,



dbo.LinkEnd.name AS LinkEnd_name,
dbo.LinkEnd.property_id, -- AS property_id, 

dbo.LinkEnd.subband, -- AS subband, 

dbo.LinkEnd.LinkEndType_mode_id, -- AS LinkEndType_mode_id,

                      
                      dbo.Property.name AS Property_name, 
                      
--                      dbo.Property.lat AS Property_lat, 
--                      dbo.Property.lon AS Property_lon, 
                     
--
---                     A1.lat AS Property_lat,
   --                   A1.lon AS Property_lon,
 
                      A1.lat,
                      A1.lon,
                      A1.location_type,                     
                      
                      
                      dbo.Property.ground_height AS ground_height,
                      
                      
/*                      dbo.Link.name AS Link_name, 
                      dbo.Link.length AS length, 
                      dbo.Link.length_km AS length_km,                       
                      dbo.Link.clutter_model_id AS clutter_model_id,
*/
                      
                      dbo.LinkEndType_Band.channel_width AS channel_width, 
                      dbo.LinkEndType_Band.txrx_shift, -- AS txrx_shift,
                      dbo.LinkEndType_Band.channel_count, -- AS channel_count, 
                      
                      dbo.LinkEndType_Band.freq_max_high, 
                      dbo.LinkEndType_Band.freq_min_low, 
                      dbo.LinkEndType_Band.name AS LinkEndType_Band_name  ,                    
                      
                      
                      dbo.fn_Linkend_GetMaxAntennaHeight(dbo.LinkEnd.id) AS    MaxAntennaHeight, 
                      
                      dbo.LinkFreqPlan.threshold_degradation AS  LinkFreqPlan_threshold_degradation,
                      dbo.LinkFreqPlan.min_CI_dB AS LinkFreqPlan_min_CI_dB, 
                      

                      dbo.LinkEnd.channel_number, -- AS channel_number, 
                      dbo.LinkEnd.tx_freq_MHz, -- AS tx_freq_MHz, 
                      dbo.LinkEnd.rx_freq_MHz, -- AS rx_freq_MHz, 

                      dbo.LinkEnd.power_dBm, -- AS power_dBm, 
                      dbo.LinkEnd.kng AS kng, 
                      dbo.LinkEnd.loss AS loss, 
                      dbo.LinkEnd.freq_channel_count, -- AS freq_channel_count, 
                      dbo.LinkEnd.channel_type,
                      dbo.LinkEnd.freq_spacing , -- AS freq_spacing,
                      dbo.LinkEnd.kratnostByFreq, -- AS kratnostByFreq,
                      dbo.LinkEnd.kratnostBySpace, -- AS kratnostBySpace, 
                      dbo.LinkEnd.signature_width, -- AS signature_width,
                      dbo.LinkEnd.signature_height, -- AS signature_height, 
                      dbo.LinkEnd.modulation_count, -- AS modulation_count,
                      dbo.LinkEnd.threshold_BER_3, -- AS threshold_BER_3, 
                      dbo.LinkEnd.threshold_BER_6, -- AS threshold_BER_6,
                      
                      dbo.LinkEnd.band, -- AS band,                                         
                      dbo.LinkEnd.linkendtype_id, -- AS linkendtype_id, 
                      dbo.LinkEnd.LinkEndType_band_id, -- AS LinkEndType_band_id, 

                      dbo.LinkEndType_Mode.ch_spacing,    
                      dbo.LinkEndType_Mode.bandwidth AS bandwidth, 
                      dbo.LinkEndType_Mode.bandwidth_tx_3, -- AS bandwidth_tx_3,
                      dbo.LinkEndType_Mode.bandwidth_tx_30, -- AS bandwidth_tx_30, 
                      dbo.LinkEndType_Mode.bandwidth_tx_a, -- AS bandwidth_tx_a,
                      dbo.LinkEndType_Mode.bandwidth_rx_3, -- AS bandwidth_rx_3, 
                      dbo.LinkEndType_Mode.bandwidth_rx_30, -- AS bandwidth_rx_30,
                      dbo.LinkEndType_Mode.bandwidth_rx_a, -- AS bandwidth_rx_a, 

--                 dbo.LinkEndType.FREQ_MAX_HIGH,
 --                     dbo.LinkEndType.FREQ_MIN_LOW,

                      dbo.LinkEndType.level_tx_a,
                      dbo.LinkEndType.level_rx_a,
                      dbo.LinkEndType.name AS LinkEndType_name,
                      dbo.LinkEndType.coeff_noise,
                      
                      
--//                      a.id as ssss,

/*
                      a.POLARIZATION_STR   as antenna_POLARIZATION_STR,
                      a.Polarization_ratio as antenna_Polarization_ratio,
                      a.Gain 			 as antenna_Gain,
                      a.Diameter 		 as antenna_Diameter,
                      a.Loss 			 as antenna_Loss,
                      a.Vert_width 		 as antenna_Vert_width,
                      a.AZIMUTH 		 as antenna_AZIMUTH,
                      a.Height 		 	 as antenna_Height,
                 --     a.Polarization_ratio as antenna_Polarization_ratio
                      
*/

(select count(*) from Linkend_Antenna where Linkend_id = dbo.LinkEnd.id) as Antenna_count
                   
                      
                      
FROM         dbo.LinkEndType_Band RIGHT OUTER JOIN
                      dbo.LinkEndType_Mode RIGHT OUTER JOIN
                      dbo.LinkEnd LEFT OUTER JOIN
                      dbo.LinkEndType ON dbo.LinkEnd.linkendtype_id = dbo.LinkEndType.id ON dbo.LinkEndType_Mode.id = dbo.LinkEnd.LinkEndType_mode_id ON 
                      dbo.LinkEndType_Band.id = dbo.LinkEnd.LinkEndType_band_id RIGHT OUTER JOIN
                   --   dbo.Link RIGHT OUTER JOIN
                      dbo.LinkFreqPlan_LinkEnd M LEFT OUTER JOIN
                      dbo.LinkFreqPlan ON M.LinkFreqPlan_id = dbo.LinkFreqPlan.id 
--                      ON dbo.Link.id = dbo.LinkFreqPlan_LinkEnd.link_id 
                      ON  dbo.LinkEnd.id = M.linkend_id LEFT OUTER JOIN
                      dbo.Property ON dbo.LinkEnd.property_id = dbo.Property.id
                      
                      OUTER apply fn_Linkend_Link_info(dbo.LinkEnd.id) link
                      
                       cross apply [dbo].fn_Linkend_Antenna_pos (LinkEnd.ID) A1
                       
          /*             
                       left join (
                          select Linkend_Antenna.*, AntennaType.Polarization_ratio 
                             from Linkend_Antenna
                               left join AntennaType  on AntennaType.id = Linkend_Antenna.antennaType_id
                       
                       ) A on a.linkend_id = LinkEnd.id
     
*/
                      
                      
     order by  M.id

GO
/****** Object:  View [LinkFreqPlan].[view_LinkFreqPlan_LinkEnd_antennas]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[LinkFreqPlan].[view_LinkFreqPlan_LinkEnd_antennas]') IS NOT NULL

DROP VIEW [LinkFreqPlan].[view_LinkFreqPlan_LinkEnd_antennas]
GO
/****** Object:  View [LinkFreqPlan].[view_LinkFreqPlan_LinkEnd_antennas]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [LinkFreqPlan].[view_LinkFreqPlan_LinkEnd_antennas]
AS
SELECT   

--LinkEnd.id as aaaaaaaaaaaaaa,


  dbo.Linkend_Antenna.*, dbo.Property.ground_height AS ground_height, dbo.LinkFreqPlan_LinkEnd.LinkFreqPlan_id AS LinkFreqPlan_id, 
                      dbo.AntennaType.polarization_ratio AS polarization_ratio, dbo.AntennaType.vert_mask, dbo.AntennaType.horz_mask, 
                      
                      
                      
                        
       A1.lat AS map_lat,
       A1.lon AS map_lon,
                      
                      
                      dbo.Property.lat AS Property_lat, 
                      dbo.Property.lon AS Property_lon
                      
                      
                      
FROM         dbo.Property RIGHT OUTER JOIN
                      dbo.LinkEnd LEFT OUTER JOIN
                      dbo.AntennaType RIGHT OUTER JOIN
                      dbo.Linkend_Antenna ON dbo.AntennaType.id = dbo.Linkend_Antenna.antennaType_id ON dbo.LinkEnd.id = dbo.Linkend_Antenna.linkend_id ON 
                      dbo.Property.id = dbo.Linkend_Antenna.property_id RIGHT OUTER JOIN
                      dbo.LinkFreqPlan_LinkEnd ON dbo.LinkEnd.id = dbo.LinkFreqPlan_LinkEnd.linkend_id
                      

                      

     cross apply [dbo].fn_Linkend_Antenna_pos (Linkend_Antenna.linkend_id) A1
     
--where LinkFreqPlan_id=1016

GO
/****** Object:  UserDefinedFunction [geo].[fn_Length_m]    Script Date: 16.05.2020 10:54:58 ******/


-------------------------------------------------------------------------------

IF OBJECT_ID(N'[dbo].[view_LinkFreqPlan_LinkEnd]') IS NOT NULL
  DROP VIEW [dbo].[view_LinkFreqPlan_LinkEnd]
GO


CREATE VIEW dbo.view_LinkFreqPlan_LinkEnd
AS

SELECT  top 1000000   

--  dbo.fn_LinkEnd_Get_Link_ID (dbo.LinkEnd.id) AS link_id, 
 -- dbo.fn_LinkEnd_Get_Next_LinkEnd_ID (dbo.LinkEnd.id) AS next_linkend_id, 


M.*,

link.link_id, 
link.link_name, 
link.next_linkend_id, 
link.clutter_model_id,
link.length,
link.length_km,



dbo.LinkEnd.name AS LinkEnd_name,
dbo.LinkEnd.property_id, -- AS property_id, 

dbo.LinkEnd.subband, -- AS subband, 

dbo.LinkEnd.LinkEndType_mode_id, -- AS LinkEndType_mode_id,

                      
                      dbo.Property.name AS Property_name, 
                      
                      dbo.Property.lat AS Property_lat, 
                      dbo.Property.lon AS Property_lon, 
                     
--
---                     A1.lat AS Property_lat,
   --                   A1.lon AS Property_lon,
 
                      A1.lat,
                      A1.lon,
                      A1.location_type,                     
                      
                      
                      dbo.Property.ground_height AS ground_height,
                      
                      
/*                      dbo.Link.name AS Link_name, 
                      dbo.Link.length AS length, 
                      dbo.Link.length_km AS length_km,                       
                      dbo.Link.clutter_model_id AS clutter_model_id,
*/
                      
                      dbo.LinkEndType_Band.channel_width AS channel_width, 
                      dbo.LinkEndType_Band.txrx_shift, -- AS txrx_shift,
                      dbo.LinkEndType_Band.channel_count, -- AS channel_count, 
                      
                      dbo.LinkEndType_Band.freq_max_high, 
                      dbo.LinkEndType_Band.freq_min_low, 
                      dbo.LinkEndType_Band.name AS LinkEndType_Band_name  ,                    
                      
                      
                      dbo.fn_Linkend_GetMaxAntennaHeight(dbo.LinkEnd.id) AS    MaxAntennaHeight, 
                      
                      dbo.LinkFreqPlan.threshold_degradation AS  LinkFreqPlan_threshold_degradation,
                      dbo.LinkFreqPlan.min_CI_dB AS LinkFreqPlan_min_CI_dB, 
                      

                      dbo.LinkEnd.channel_number, -- AS channel_number, 
                      dbo.LinkEnd.tx_freq_MHz, -- AS tx_freq_MHz, 
                      dbo.LinkEnd.rx_freq_MHz, -- AS rx_freq_MHz, 

                      dbo.LinkEnd.power_dBm, -- AS power_dBm, 
                      dbo.LinkEnd.kng AS kng, 
                      dbo.LinkEnd.loss AS loss, 
                      dbo.LinkEnd.freq_channel_count, -- AS freq_channel_count, 
                      dbo.LinkEnd.channel_type,
                      dbo.LinkEnd.freq_spacing , -- AS freq_spacing,
                      dbo.LinkEnd.kratnostByFreq, -- AS kratnostByFreq,
                      dbo.LinkEnd.kratnostBySpace, -- AS kratnostBySpace, 
                      dbo.LinkEnd.signature_width, -- AS signature_width,
                      dbo.LinkEnd.signature_height, -- AS signature_height, 
                      dbo.LinkEnd.modulation_count, -- AS modulation_count,
                      dbo.LinkEnd.threshold_BER_3, -- AS threshold_BER_3, 
                      dbo.LinkEnd.threshold_BER_6, -- AS threshold_BER_6,
                      
                      dbo.LinkEnd.band, -- AS band,                                         
                      dbo.LinkEnd.linkendtype_id, -- AS linkendtype_id, 
                      dbo.LinkEnd.LinkEndType_band_id, -- AS LinkEndType_band_id, 

                      dbo.LinkEndType_Mode.ch_spacing,    
                      dbo.LinkEndType_Mode.bandwidth AS bandwidth, 
                      dbo.LinkEndType_Mode.bandwidth_tx_3, -- AS bandwidth_tx_3,
                      dbo.LinkEndType_Mode.bandwidth_tx_30, -- AS bandwidth_tx_30, 
                      dbo.LinkEndType_Mode.bandwidth_tx_a, -- AS bandwidth_tx_a,

                      dbo.LinkEndType_Mode.bandwidth_rx_3, -- AS bandwidth_rx_3, 
                      dbo.LinkEndType_Mode.bandwidth_rx_30, -- AS bandwidth_rx_30,
                      dbo.LinkEndType_Mode.bandwidth_rx_a, -- AS bandwidth_rx_a, 

--                 dbo.LinkEndType.FREQ_MAX_HIGH,
 --                     dbo.LinkEndType.FREQ_MIN_LOW,

                      dbo.LinkEndType.level_tx_a,
                      dbo.LinkEndType.level_rx_a,
                      dbo.LinkEndType.name AS LinkEndType_name,
                      dbo.LinkEndType.coeff_noise

                   
                      
                      
FROM         dbo.LinkEndType_Band RIGHT OUTER JOIN
                      dbo.LinkEndType_Mode RIGHT OUTER JOIN
                      dbo.LinkEnd LEFT OUTER JOIN
                      dbo.LinkEndType ON dbo.LinkEnd.linkendtype_id = dbo.LinkEndType.id ON dbo.LinkEndType_Mode.id = dbo.LinkEnd.LinkEndType_mode_id ON 
                      dbo.LinkEndType_Band.id = dbo.LinkEnd.LinkEndType_band_id RIGHT OUTER JOIN
                   --   dbo.Link RIGHT OUTER JOIN
                      dbo.LinkFreqPlan_LinkEnd M LEFT OUTER JOIN
                      dbo.LinkFreqPlan ON M.LinkFreqPlan_id = dbo.LinkFreqPlan.id 
--                      ON dbo.Link.id = dbo.LinkFreqPlan_LinkEnd.link_id 
                      ON  dbo.LinkEnd.id = M.linkend_id LEFT OUTER JOIN
                      dbo.Property ON dbo.LinkEnd.property_id = dbo.Property.id
                      
                      OUTER apply fn_Linkend_Link_info(dbo.LinkEnd.id) link
                      
                       cross apply [dbo].fn_Linkend_Antenna_pos (LinkEnd.ID) A1
     
                      
                      
                      order by  M.id

go


IF OBJECT_ID(N'[dbo].[view_LinkFreqPlan_Neighbors_root]') IS NOT NULL
  DROP VIEW [dbo].[view_LinkFreqPlan_Neighbors_root]
GO


CREATE VIEW dbo.view_LinkFreqPlan_Neighbors_root
AS
--
--
--

SELECT dbo.LinkEnd.name AS LinkEnd_name,
       dbo.LinkEnd.band,
       
       
       next_LinkEnd.name AS rx_LinkEnd_name,
       
       dbo.Property.name AS Property_name,
       
       M.LinkFreqPlan_id,
       M.id,
       M.linkend_id,
       M.CIA,
       M.CI,
       M.CA,
       M.threshold_degradation,
       M.threshold_degradation_CIA,
       
       m.lat,
       m.lon,
       m.location_type,                     
       
       
       
--       next_linkend_id
       
      round (length,0) as length,
     --  length_KM,
       
--       dbo.Link.length,
--       dbo.Link.length_km,
       
       (
         SELECT top 1 [rx_level_dBm]
         FROM LINKFREQPLAN_NEIGHBORS
         WHERE (LinkFreqPlan_id = m.LinkFreqPlan_id) AND
               (rx_linkEND_id = m.linkend_id) AND
--               (linkEND2_id = m.linkend_id) AND
               
               (IsLink = 1)
       ) AS rx_level_dBm,
       
       (
         SELECT COUNT(*)
         FROM LINKFREQPLAN_NEIGHBORS
         WHERE (LinkFreqPlan_id = m.LinkFreqPlan_id) AND
--               (LinkEND2_id = M.linkend_id) AND
               (RX_LinkEND_id = M.linkend_id) AND
               (IsNeigh = 1) -- and (IsLink <> 1)
       ) AS NEIGHBOuR_count,
       
    --   0 as Azimuth,
       
         dbo.fn_LinkEnd_Azimuth (m.linkend_id, NULL) as Azimuth
       
FROM dbo.Property
     RIGHT OUTER JOIN dbo.LinkEnd ON dbo.Property.id = dbo.LinkEnd.property_id
--     RIGHT OUTER JOIN dbo.LinkFreqPlan_LinkEnd M
     RIGHT OUTER JOIN view_LinkFreqPlan_LinkEnd M
     
     ON dbo.LinkEnd.id = M.linkend_id
     
     left JOIN LinkEnd next_LinkEnd on next_LinkEnd.id=m.next_linkend_id
     
go     



IF OBJECT_ID(N'[dbo].[view_LinkFreqPlan_CIA]') IS NOT NULL
  DROP VIEW [dbo].[view_LinkFreqPlan_CIA]
GO


------------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID(N'[dbo].[view_LinkFreqPlan_Report]') IS NOT NULL
  DROP VIEW [dbo].[view_LinkFreqPlan_Report]
GO


CREATE VIEW dbo.view_LinkFreqPlan_Report
AS
SELECT
  m.*,
  
/*
 dbo.LinkEnd.name AS LinkEnd_name,
       dbo.LinkEnd.band,
       dbo.LinkEnd.channel_number,
       dbo.LinkEnd.rx_freq_MHz,
       dbo.LinkEnd.tx_freq_MHz,
       dbo.LinkEnd.loss,
       dbo.LinkEnd.channel_type,
       dbo.LinkEnd.subband,
       dbo.Property.name AS Property_name,
       dbo.Property.lat,
       dbo.Property.lon,
       dbo.Link.name AS Link_name,
       dbo.LinkEndType.name AS LinkEndType_name,
       dbo.LinkFreqPlan_LinkEnd.id,
       dbo.LinkFreqPlan_LinkEnd.LinkFreqPlan_id,
       dbo.LinkFreqPlan_LinkEnd.linkend_id,
       
*/       
       
       dbo.Linkend_Antenna.antennaType_id,
       dbo.Linkend_Antenna.height,
       dbo.Linkend_Antenna.azimuth,
       dbo.Linkend_Antenna.diameter,
       
       dbo.AntennaType.name AS [AntennaType_name],
       
       
       dbo.Property.lat_str,
       dbo.Property.lon_str
       
FROM view_LinkFreqPlan_LinkEnd m
     INNER JOIN dbo.Linkend_Antenna ON M.linkend_id =  dbo.Linkend_Antenna.linkend_id
     LEFT OUTER JOIN dbo.AntennaType ON dbo.Linkend_Antenna.antennaType_id =  dbo.AntennaType.id
--     LEFT OUTER JOIN dbo.Link ON dbo.LinkFreqPlan_LinkEnd.link_id = dbo.Link.id
 --    LEFT OUTER JOIN dbo.LinkEnd ON dbo.LinkFreqPlan_LinkEnd.linkend_id =   dbo.LinkEnd.id
     LEFT OUTER JOIN dbo.Property ON M.property_id = dbo.Property.id
   --  LEFT OUTER JOIN dbo.LinkEndType ON dbo.LinkEnd.linkendtype_id =   dbo.LinkEndType.id
WHERE (dbo.Linkend_Antenna.linkend_id > 0)

go



CREATE VIEW dbo.[view_LinkFreqPlan_CIA]
AS
SELECT -- 0 as aaaaaa


 dbo.LinkEnd.name AS LinkEnd_name,
       dbo.LinkFreqPlan_LinkEnd.LinkFreqPlan_id,
       dbo.LinkFreqPlan_LinkEnd.id,
       dbo.LinkFreqPlan_LinkEnd.linkend_id,
       dbo.LinkFreqPlan_LinkEnd.CIA,
       dbo.LinkFreqPlan_LinkEnd.CI,
       dbo.LinkFreqPlan_LinkEnd.CA,
       dbo.LinkFreqPlan_LinkEnd.freq_distributed_tx,
       dbo.LinkFreqPlan_LinkEnd.freq_distributed_rx,
       dbo.LinkFreqPlan_LinkEnd.freq_fixed_tx,
       dbo.LinkFreqPlan_LinkEnd.freq_fixed_rx,
       dbo.LinkFreqPlan_LinkEnd.threshold_degradation,
       dbo.LinkFreqPlan_LinkEnd.threshold_degradation_CIA,
       
  Link.Link_name,
  Link.length AS length
--  Link.length_km AS length_km
--  dbo.Link.clutter_model_id AS clutter_model_id,
       
     /*  
       length,
       length_km
       */
       
--FROM view_LinkFreqPlan_LinkEndEx
FROM LinkFreqPlan_LinkEnd
   --  LEFT OUTER JOIN dbo.Link ON dbo.LinkFreqPlan_LinkEnd.link_id = dbo.Link.id
     LEFT OUTER JOIN dbo.LinkEnd ON dbo.LinkFreqPlan_LinkEnd.linkend_id =  dbo.LinkEnd.id
     
     
      cross apply fn_Linkend_Link_info(dbo.LinkFreqPlan_LinkEnd.linkend_id) link
     
go     




-------------------------------------------------------

IF OBJECT_ID(N'[dbo].[view_LinkFreqPlan]') IS NOT NULL
  DROP VIEW dbo.view_LinkFreqPlan 
GO

CREATE VIEW dbo.view_LinkFreqPlan 
AS
  SELECT LinkFreqPlan.*,
    dbo.LinkNet.project_id
  FROM
    LinkFreqPlan
    INNER JOIN dbo.LinkNet ON (LinkFreqPlan.linknet_id = dbo.LinkNet.id)
go



-------------------------------------------------------------
IF OBJECT_ID(N'[dbo].[sp_LinkFreqPlan_Neighbors_add]') IS NOT NULL
  DROP  PROCEDURE dbo.sp_LinkFreqPlan_Neighbors_add
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_LinkFreqPlan_Neighbors_add
(
  @LINKFREQPLAN_ID INT,

  @LINKEND1_ID INT,  -- ���, ��� �����
  @LINKEND2_ID INT,  -- ���, ���� �����   

--  @TX_LINKEND_ID INT,  --
--  @RX_LINKEND_ID INT,  --������������� ��������  (linkend)
    
  @RX_LEVEL_DBM FLOAt=null,   
  

  @IsPropertyOne BIT=null,   
  @IsLink  BIT=null,   

  @Distance_M FLOAT=null,   
  @diagram_level FLOAT=null
   
)    

AS
BEGIN
  DECLARE
    @TX_LINKEND_ID INT = @LINKEND1_ID,  --
    @RX_LINKEND_ID INT = @LINKEND2_ID --������������� ��������  (linkend)
    

/*

declare   
    @LINKEND1_name varchar(50),  --
    @LINKEND2_name varchar(50)  --������������� ��������  (linkend)
 --   @rx_for_LINKEND1_name varchar(50)  --������������� ��������  (linkend)
*/
 
/*

 SELECT @LINKEND1_name=name
    FROM linkend 
    WHERE ID=@LINKEND1_ID*/

/*
  SELECT @LINKEND1_name=name
    FROM linkend 
    WHERE ID=@LINKEND1_ID


  SELECT @LINKEND2_name=name
    FROM linkend 
    WHERE ID=@LINKEND2_ID

    */
    
  declare 
--  	 @rx_for_linkend1_id  int = dbo.fn_LinkEnd_Get_Next_LinkEnd_ID (@TX_LINKEND_ID)
  	 @rx_for_linkend1_id  int = dbo.fn_LinkEnd_Get_Next_LinkEnd_ID (@LINKEND1_ID)
     
     
     
     
/*  SELECT @rx_for_LINKEND1_name=name
    FROM linkend 
    WHERE ID=@rx_for_linkend1_id
*/
     
    
--  set @linkend_id =  dbo.fn_LinkEnd_Get_Next_LinkEnd_ID (@RX_LINKEND_ID)
  
  /*  
  SELECT @LINKEND1_name=name
    FROM linkend 
    WHERE LINKEND1_ID=@RX_LINKEND_ID
    
  IF @linkend_id IS NULL   
  BEGIN 
    SELECT @linkend_id=LINKEND1_ID 
      FROM link 
      WHERE LINKEND2_ID=@RX_LINKEND_ID
  END


  -- belong to the same link --
  IF @TX_LINKEND_ID = @linkend_id
    SET @linkend_id = @RX_LINKEND_ID

*/

--  IF @Distance_M=0  
--    SET @Distance_M=NULL


 -- if  (@IsLink=1) and (dbo.fn_LinkEnd_Has_same_link (@TX_LINKEND_ID, @RX_LINKEND_ID )=0)   
  --   RAISERROR ('if dbo.fn_LinkEnd_Has_same_link (@TX_LINKEND_ID, @RX_LINKEND_ID ) and  (@IsLink=1)', 16, 1);
/*
    SELECT                  
       @lat1=A1.lat,
       @lon1=A1.lon
    FROM dbo.LinkEnd 
         cross apply [dbo].fn_Linkend_Antenna_pos (id) A1
    where id=@TX_LINKEND_ID
*/


/*    
    SELECT                  
       @lat1=A1.lat,
       @lon1=A1.lon
    FROM dbo.LinkEnd 
         cross apply [dbo].fn_Linkend_Antenna_pos (id) A1
    where id=@TX_LINKEND_ID

----------------------

    SELECT                  
       @lat2=A1.lat,
       @lon2=A1.lon
    FROM dbo.LinkEnd 
         cross apply [dbo].fn_Linkend_Antenna_pos (id) A1
    where id=@RX_LINKEND_ID
  
*/
--//if @IsLink<>1
--  set  @RX_LEVEL_DBM = null
----------------------  
  

  INSERT INTO LinkFreqPlan_Neighbors
     (LINKFREQPLAN_ID, 

      LINKEND1_ID,      
      LINKEND2_ID,

  --    tx_LINKEND1_name,
  --    tx_LINKEND2_name,
      
      rx_for_linkend1_id,
--      rx_for_linkend1_name,

      TX_LINKEND_ID,      
      RX_LINKEND_ID,  --������������� ��������  (linkend)
   
    --  TX_LINK_ID,  --����������, �������� �������������� ���������  (linkend)
      
     RX_LEVEL_DBM,   
   --   SESR,
   --   Kng,

      IsPropertyOne,
      IsLink,

 --     azimuth_,
     Distance_M,
      
      diagram_level 
      )
  VALUES
     (@LINKFREQPLAN_ID, 
      
      @LINKEND1_ID,      
      @LINKEND2_ID,

--      @LINKEND1_name,
  --    @LINKEND2_name,
      
      @rx_for_linkend1_id,
--      @rx_for_linkend1_name,
      


      @TX_LINKEND_ID,      
      @RX_LINKEND_ID,
      
   --   @linkend_id,
      
      round(@RX_LEVEL_DBM,1),
   --   @SESR,
   --   @Kng,

      @IsPropertyOne,
      @IsLink,

--dbo.fn_AZIMUTH (@lat1, @lon1, @lat2, @lon2, null),
--dbo.fn_AZIMUTH (@lat1, @lon1, @lat2, @lon2, 'length'),
     @Distance_M,
      @diagram_level
      ) 
      
      
  RETURN IDENT_CURRENT ('LinkFreqPlan_Neighbors')         
      
--  RETURN @@IDENTITY;
      

END

go

-------------------------------------------------------------
IF OBJECT_ID(N'[dbo].[sp_LinkFreqPlan_Neighbors]') IS NOT NULL
  DROP  PROCEDURE dbo.sp_LinkFreqPlan_Neighbors
GO



/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_LinkFreqPlan_Neighbors
(
	@ID INT 
)    

AS
BEGIN
  SET ANSI_WARNINGS OFF --String or binary data would be truncated.

  DECLARE
--    @max_id int,
  	@i int,
    @max_tree_id int;
  		  

 -- if @ID is null
 --   set @ID = 70365
  
  if @ID is null
  BEGIN
   SELECT top 1  @id =id FROM LinkFreqPlan order by id desc
  
--    set @ID = 70365
 --   set @LINKEND_ID =124645
  END

    	
  print @ID
  

  create table [#temp]
      (           
       tree_id 		    Int,
       tree_parent_id   Int,
                   
       BAND        	   VARCHAR(10),
    --   NUMBER          Int,
    
       id 		       Int,    

       TX_LINKEND_ID      Int,
       RX_LINKEND_ID      Int,

    
       TX_LinkEnd_name VARCHAR(50),
       RX_LinkEnd_name VARCHAR(50),
    
       TX_Property_name VARCHAR(50),
       RX_Property_name VARCHAR(50),

       TX_Property_lat float,
       TX_Property_lon float,

       RX_Property_lat float,
       RX_Property_lon float,

       Next_for_RX_Property_lat float,
       Next_for_RX_Property_lon float,



       
--       LINKEND_ID      Int,
       
       DISTANCE_m      Float,
       DISTANCE_km     Float,

       CI              Float,
       Rx_level_dBm    Float,
       INTERFERENCE    Float,
       NEIGHBOUR_COUNT Int,    
       
       
       power_noise Float, 
       power_cia   Float,
       
    --   [TYPE]           Int,
       
       CIA             Float,
       CI_             Float,
       CA              Float,
       
       EXTRA_LOSS      				Float,
       threshold_degradation 		Float,
       threshold_degradation_cia 	Float,
       
       IsByCI bit,
       IsByProperty bit,
       IsByLink bit,
       
       IsNeigh bit
                              
      ) ;      
      

--------------------------------------------------
-- LinkEnd
--------------------------------------------------     
  insert into [#temp]
    (
      tree_id,
     
   --   LINKEND_ID, 
  --   TX_LINKEND_ID, RX_LINKEND_ID,
      
      TX_LinkEnd_name, TX_Property_name,
      BAND, 

	  Rx_level_dBm, 
      DISTANCE_m,
      --DISTANCE_km,
      
      CIA, CI_, CA, threshold_degradation, threshold_degradation_cia,      
      NEIGHBOUR_COUNT    
     )
   select 
     LINKEND_ID, --tree_id

 --    LINKEND_ID,	
 --    TX_LINKEND_ID, RX_LINKEND_ID,
     
     
     LinkEnd_name, Property_name,
     BAND, 

  	 Rx_level_dBm, 
     LENGTH, 
     --LENGTH_km,
      
     CIA, CI, CA, threshold_degradation, threshold_degradation_cia,      
     
     case 
       when NEIGHBOUR_COUNT=0 then null
       else NEIGHBOUR_COUNT
     end  
     
   from 
   		view_LinkFreqPlan_Neighbors_root
   where 
   	 LinkFreqPlan_id = @ID;
     

 -- select @max_id=Max(tree_id)  from  #temp  
     
--  print @max_id   
    
     

--  set @i=0

  --UPDATE [#temp]  
  --	set @i=number = @i+1


  ------------------------------
  -- ByProperty 
  ------------------------------
  insert into [#temp]
    ( 
      tree_parent_id,
      
      tx_linkend_id, rx_linkend_id,
      
      
   --  [TYPE],
      ID, 
      
      RX_LinkEnd_name, rx_Property_name,	
      
/*      DISTANCE_m, 
      CI, INTERFERENCE,
      
      CIA,
      CI_,
      CA,
      EXTRA_LOSS,
      threshold_degradation,
      THRESHOLD_DEGRADATION_CIA,

      IsNeigh,
      power_noise, power_cia ,
*/      
      IsByProperty
       
     )
   select 
      
     	rx_linkend_id,  
      
      tx_linkend_id, rx_linkend_id,      
      
    --  case when IsPropertyOne=1 THEN 2 ELSE 3 END, 

      ID, 
      
      rx_LinkEnd_name, rx_Property_name,
      
      
/*      DISTANCE_m, 
      CI, 
      Rx_level_dBm - DIAGRAM_LEVEL,
      
      CIA,
      CI_,
      CA,
      EXTRA_LOSS,
      threshold_degradation,
      THRESHOLD_DEGRADATION_CIA,  
      IsNeigh,

      power_noise, power_cia ,
*/      
      
      1
      
  
   FROM  view_LinkFreqPlan_Neighbors_slave
   where (LinkFreqPlan_id = @ID) 
    and (IsNull(IsLink,0)<>1)
    
    --  and IsPropertyOne=1
  -- ORDER BY IsPropertyOne DESC

  ------------------------------
  -- ONLY IsLink
  ------------------------------
  insert into [#temp]
    ( tree_parent_id,
      ID, 
      
       tx_linkend_id, rx_linkend_id,
      
      RX_LinkEnd_name, rx_Property_name,
      DISTANCE_m,
      
      IsByLink
      
     --  [TYPE]
     )
   select 
     	tx_linkend_id, 
      ID, 
      
       tx_linkend_id, rx_linkend_id,
      
      rx_LinkEnd_name, rx_Property_name,
      DISTANCE_m,
      
      1
      
    --  1
  
  FROM  view_LinkFreqPlan_Neighbors_slave
  where (LinkFreqPlan_id = @ID) 
       and (IsLink=1);


   
/*const
  TYPE_NEIGHBORS_BY_CI   = 1;  //ninaa ii yia?aaoeea
  TYPE_NEIGHBORS_BY_LINK = 2;  //ninaa ii Link
  TYPE_NEIGHBORS_BY_PROP = 3;  //ninaa ii Property
*/   

  --------------------------
  -- set tree_id
  --------------------------

  SELECT @max_tree_id=Max(tree_id)  
    FROM #temp                                
    WHERE tree_id IS NOT NULL 
  		 
  set @i=@max_tree_id

  UPDATE [#temp]  
  	set @i=tree_id = @i+1
  	WHERE tree_id IS NULL 


  SELECT * FROM #temp                                
 
 END

go



-------------------------------------------------------------
IF OBJECT_ID(N'[dbo].[view_LinkFreqPlan]') IS NOT NULL
  DROP VIEW dbo.view_LinkFreqPlan 
GO



CREATE VIEW dbo.view_LinkFreqPlan 
AS
  SELECT LinkFreqPlan.*,
    dbo.LinkNet.project_id
  FROM
    LinkFreqPlan
    INNER JOIN dbo.LinkNet ON (LinkFreqPlan.linknet_id = dbo.LinkNet.id)

go


------------------------------------------------------------

IF OBJECT_ID(N'[dbo].[sp_LinkFreqPlan_add]') IS NOT NULL
  DROP PROCEDURE dbo.sp_LinkFreqPlan_add
GO



/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_LinkFreqPlan_add
(
  @LINKNET_ID int,

  @NAME VARCHAR(100), 
  
  @MaxCalcDistance_km float, 
  @MIN_CI_dB float,
    
  @IS_GET_INFO bit = 0

)
AS
BEGIN
  DECLARE
  	@new_id int	
   -- @project_id int;   


  if not exists(SELECT * from LINKNET WHERE id=@LINKNET_ID) 
     RAISERROR ('@LINKNET_ID is null', 16, 1);



-- IF @LinkFreqPlan_ID is null  RAISERROR ('@ID is null', 16, 1);
 /*
  if @LinkFreqPlan_ID is null
  BEGIN
   SELECT top 1  @LinkFreqPlan_ID =id 
   	FROM LinkFreqPlan
  
--    set @ID = 70365
 --   set @LINKEND_ID =124645
  END
*/



/*  SELECT @project_id=project_id
    FROM LinkNet
    WHERE id=@LINKNET_ID;
    */
    
  ---------------------------------------------

  INSERT INTO LINKFREQPLAN
     (
   --   project_id,
      LINKNET_ID,
      
      NAME,
      MaxCalcDistance_km,
      min_CI_dB
      ) 
   VALUES     
     (
   --   @project_id,
      @LINKNET_ID,

      @NAME,
      @MaxCalcDistance_km, 
      @MIN_CI_dB      
     )   

  ---------------------------------------------
      
 -- RETURN @@IDENTITY  
  
--  set @new_id=@@IDENTITY
  
  set @new_id=IDENT_CURRENT ('LinkFreqPlan')   
  
  
  print @new_id
  
    
  exec sp_LinkFreqPlan_init @new_id

  
  
  if @IS_GET_INFO=1
    SELECT id,guid,name  FROM LINKFREQPLAN WHERE ID=@new_id
  
  RETURN @new_id;      
        
END

go
------------------------------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-------------------------------------------------------------
IF OBJECT_ID(N'[dbo].[sp_LinkFreqPlan_Init]') IS NOT NULL
  DROP  PROCEDURE dbo.sp_LinkFreqPlan_Init
GO



/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_LinkFreqPlan_Init
(
	@LinkFreqPlan_ID  INT=11
)
AS
BEGIN
--  IF @LinkFreqPlan_ID is null  RAISERROR ('@ID is null', 16, 1);
 /*
  if @LinkFreqPlan_ID is null
  BEGIN
   SELECT top 1  @LinkFreqPlan_ID =id 
   	FROM LinkFreqPlan
  
--    set @ID = 70365
 --   set @LINKEND_ID =124645
  END
*/


declare 
  @linkNet_id INT;

  --begin TRANSACTION
    
  
  SELECT @linkNet_id = LinkNet_id 
    FROM  LinkFreqPlan
    WHERE (id = @LinkFreqPlan_ID)






-- ������ LINKS from LinkNet
create TABLE #link     (id int NOT NULL) 
create TABLE #linkline (id int NOT NULL) 
create TABLE #linkend  (id int NOT NULL) --, link_id int NOT NULL); 
--create TABLE #Property (id int NOT NULL) 
--create TABLE #linkend_NB (id int NOT NULL) 
create TABLE #bands (band nVARCHAR(50) COLLATE Cyrillic_General_CI_AS NOT NULL) 

-------------------------------------------------------------------
-- ������� ��� LINKS �� ����
-------------------------------------------------------------------

--select * INTO ##link from Antenna


 INSERT INTO #link (id)
   SELECT link_id FROM LinkNet_Items_XREF 
     WHERE (linknet_id=@linkNet_id) and (link_id>0) 

--select * From #link 



 INSERT INTO #linkLine (id)
   SELECT linkLine_id FROM LinkNet_Items_XREF 
     WHERE (linknet_id=@linkNet_id) and (linkLine_id>0) 


 INSERT INTO #link (id)
   SELECT link_id FROM LinkLineLinkXREF 
   	  WHERE LinkLine_id IN (SELECT id FROM #linkline)   
      
      -- IN            
        --   (SELECT LinkLine_id FROM LinkNet_Items_XREF WHERE linknet_id=@linkNet_id)   


--select * From #link 




--SELECT Link_id FROM  LinkFreqPlan_Link  where LinkFreqPlan_id = @LinkFreqPlan_ID

 

-------------------------------------------------------------------
-- �������� ����������� LINKS � ��������� ���� - table LinkFreqPlan_Link
-------------------------------------------------------------------
INSERT INTO LinkFreqPlan_Link (LinkFreqPlan_id, Link_id)
 SELECT @LinkFreqPlan_ID, id FROM #link
   WHERE id NOT IN  
      (SELECT Link_id FROM  LinkFreqPlan_Link  where LinkFreqPlan_id = @LinkFreqPlan_ID)




      
-------------------------------------------------------------------      
-- ������� ������ LINKS 
-------------------------------------------------------------------





DELETE FROM LinkFreqPlan_Link 
   WHERE Link_id NOT IN  
      (SELECT id FROM  #link)

      
INSERT INTO #linkend (id) --, link_id
    SELECT LinkEnd1_id FROM Link   --, id
       WHERE id IN  (SELECT id FROM  #link)
  UNION     
    SELECT LinkEnd2_id FROM Link   --, id
       WHERE id IN  (SELECT id FROM  #link)



    SELECT * from Link 
       where id  in (Select id FROM  #link)




-------------------------------------------------------------------
-- ��� ��������, ��� ��������� LinkEnds
-------------------------------------------------------------------
/*    SELECT Property_id FROM LinkEnd 
       WHERE id IN  (SELECT id FROM  #linkend)
*/


/*
 INSERT INTO #Property (id)
    SELECT Property_id FROM LinkEnd 
       WHERE id IN  (SELECT id FROM  #linkend)
 
*/

------------------------------------------------------------------
-- ��� �������� LinkEnds �� ���������, ��� (channel_number > 0) 
-------------------------------------------------------------------

/* SELECT ID FROM LinkEnd
   WHERE (Property_id IN (SELECT id FROM  #Property)) and 
         (id not in  (SELECT id FROM  #linkend)) and 
         (channel_number > 0) 
*/

/*
-- �������� ���������

INSERT INTO #linkend_NB (id)
 SELECT ID FROM LinkEnd
   WHERE (Property_id IN (SELECT id FROM  #Property)) and 
         (id not in  (SELECT id FROM  #linkend)) and 
         (channel_number > 0) 

*/

/*INSERT INTO #linkend_NB (id)
 SELECT ID FROM LinkEnd
   WHERE (Property_id IN (SELECT id FROM  #Property)) and 
         (id not in  (SELECT id FROM  #linkend)) and 
         (channel_number > 0) 
*/

-------------------------------------------------------------------
-- ������� ������ linkend� 
-------------------------------------------------------------------

/*SELECT * FROM LinkFreqPlan_LinkEnd 
   WHERE (LinkEnd_id NOT IN (SELECT id FROM  #linkEnd)) and 
         (LinkEnd_id NOT IN (SELECT id FROM  #linkEnd_NB)) 

*/
DELETE FROM LinkFreqPlan_LinkEnd 
   WHERE (LinkEnd_id NOT IN (SELECT id FROM  #linkEnd))
       --  and 
       --  (LinkEnd_id NOT IN (SELECT id FROM  #linkEnd_NB)) 
         
/*
    
     SELECT @LinkFreqPlan_ID, 		
     		ID,
            CHANNEL_NUMBER,
            TX_FREQ,
            RX_FREQ,    
            CHANNEL_NUMBER_TYPE,
            1     
      FROM linkend     
      WHERE (id not in (SELECT LinkEnd_id FROM  LinkFreqPlan_LinkEnd WHERE LinkFreqPlan_id = @LinkFreqPlan_ID)) and
            ( (id in (SELECT id FROM  #linkEnd)) or  
              (id in (SELECT id FROM  #linkEnd_NB)) )
*/


  INSERT INTO LinkFreqPlan_LinkEnd         
            (LINKFREQPLAN_ID, 
            LINKEND_ID,
        --    LINK_ID,   
         --   NEXT_LINKEND_ID,
                      
            FREQ_FIXED, 
            FREQ_FIXED_TX, 
            FREQ_FIXED_RX, 
          --  CHANNEL_TYPE_FIXED,
            FREQ_REQ_COUNT )
        
     SELECT @LinkFreqPlan_ID, 		
     	     	ID,
       --     dbo.fn_LinkEnd_Get_Link_ID(id),
       --     dbo.fn_LinkEnd_Get_Next_LinkEnd_ID(id),
            
            CHANNEL_NUMBER,
            TX_FREQ_MHz,
            RX_FREQ_MHz,    
          --  CHANNEL_TYPE,
            1     
      FROM linkend     
      WHERE (id not in 
                (SELECT LinkEnd_id 
                 FROM  LinkFreqPlan_LinkEnd 
                 WHERE LinkFreqPlan_id = @LinkFreqPlan_ID)) and
                 
           -- (
             (id in (SELECT id FROM  #linkEnd))
            -- or  
             -- (id in (SELECT id FROM  #linkEnd_NB)) 
             -- )


-------------------------------------------------------------------
-- used Bands
-------------------------------------------------------------------

print 'INSERT into LinkFreqPlan_Resource'

 INSERT INTO #bands (band)
   SELECT DISTINCT LinkEnd.band 
      FROM LinkFreqPlan_LinkEnd M LEFT OUTER JOIN
              LinkEnd ON M.linkend_id = LinkEnd.id
      WHERE M.LinkFreqPlan_id=@LinkFreqPlan_ID
/*
SELECT * FROM #bands

SELECT band FROM LinkFreqPlan_Resource where LinkFreqPlan_ID=@LinkFreqPlan_ID
*/

  INSERT into LinkFreqPlan_Resource (LinkFreqPlan_ID, band)
    SELECT @LinkFreqPlan_ID, band 
      FROM #bands
      WHERE band NOT IN 
         ( SELECT band FROM LinkFreqPlan_Resource where LinkFreqPlan_ID=@LinkFreqPlan_ID)      


  DELETE FROM LinkFreqPlan_Resource  
      WHERE band NOT IN 
         ( SELECT band FROM #bands)      



-- COMMIT TRANSACTION
 
 DROP TABLE #link  
 DROP TABLE #linkline   
 DROP TABLE #linkend  
 --DROP TABLE #Property 
 --DROP TABLE #linkend_NB  
 DROP TABLE #bands
       

END;
go



-----------------------------------------------------------
-------------------------------------------------------------
IF OBJECT_ID(N'dbo.sp_Linkend_Antenna_Update') IS NOT NULL
  DROP  PROCEDURE dbo.sp_Linkend_Antenna_Update
GO

/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE procedure dbo.sp_Linkend_Antenna_Update
(    
  @ID int, 

  @NAME   	varchar(50)=null,

  @HEIGHT  	float=null,
  @AZIMUTH	float=null,
  @TILT	    float=null,
  @LOSS	float=null
)
AS
begin

  UPDATE Linkend_Antenna
  set    
   --   ANTENNATYPE_ID=@ANTENNATYPE_ID,  
     
     
      HEIGHT    = COALESCE(@HEIGHT,  HEIGHT), 
      AZIMUTH   = COALESCE(@AZIMUTH, AZIMUTH),
      TILT      = COALESCE(@TILT,    TILT),  
      LOSS	    = COALESCE(@LOSS, LOSS)

/*      GAIN		=@GAIN, 
      DIAMETER	=@DIAMETER, 
      VERT_WIDTH=@VERT_WIDTH, 
      HORZ_WIDTH=@HORZ_WIDTH, 
      
      POLARIZATION_STR=@POLARIZATION_STR,
      AZIMUTH    =@AZIMUTH,
      TILT       =@TILT, 
      
      LAT           =@LAT,  
      LON           =@LON, 
      LOCATION_TYPE =@LOCATION_TYPE
*/     
  WHERE id=@ID


  RETURN @@ROWCOUNT;


        
end

go

------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-------------------------------------------------------------
IF OBJECT_ID(N'[dbo].[view_LinkFreqPlan_Neighbors]') IS NOT NULL
  DROP  view dbo.view_LinkFreqPlan_Neighbors
GO


/* ------------------------------------------------------------------------------
created: Alex  
used in: 
------------------------------------------------------------------------------*/
CREATE VIEW dbo.view_LinkFreqPlan_Neighbors
AS
SELECT     TOP 100 PERCENT dbo.LinkFreqPlan_Neighbors.tx_linkend_id, dbo.LinkFreqPlan_Neighbors.rx_linkend_id, 
                      dbo.LinkFreqPlan_Neighbors.LinkFreqPlan_id, dbo.LinkFreqPlan_Neighbors.cia, dbo.LinkFreqPlan_Neighbors.ci_, dbo.LinkFreqPlan_Neighbors.ca, 
                      dbo.LinkFreqPlan_Neighbors.IsLink, dbo.LinkFreqPlan_Neighbors.IsPropertyOne, 
                      
    /*                  
   
       A1.lat AS lat1,
       A1.lon AS lon1,
       A2.lat AS lat2,
       A2.lon AS lon2, 
                        */
                      
                     Property_1.lat AS lat1, 
                      Property_1.lon AS lon1, 
                      
                      Property_2.lat AS lat2, 
                      
                      Property_2.lon AS lon2, 
  
  
                     
                      dbo.LinkFreqPlan_Neighbors.threshold_degradation, dbo.LinkFreqPlan_Neighbors.threshold_degradation_cia, 
                      dbo.LinkFreqPlan_Neighbors.IsNeigh, dbo.LinkFreqPlan_Neighbors.power_noise, dbo.LinkFreqPlan_Neighbors.power_cia, 
                      dbo.LinkFreqPlan_Neighbors.extra_loss
FROM         dbo.LinkFreqPlan_Neighbors LEFT OUTER JOIN
                      dbo.LinkEnd LinkEnd_2 ON dbo.LinkFreqPlan_Neighbors.tx_linkend_id = LinkEnd_2.id LEFT OUTER JOIN
                      dbo.Property Property_2 ON LinkEnd_2.property_id = Property_2.id LEFT OUTER JOIN
                      dbo.Property Property_1 RIGHT OUTER JOIN
                      dbo.LinkEnd LinkEnd_1 ON Property_1.id = LinkEnd_1.property_id ON dbo.LinkFreqPlan_Neighbors.rx_linkend_id = LinkEnd_1.id
/*                      
    cross apply [dbo].fn_Linkend_Antenna_pos (LinkEnd_1.id) A1
     cross apply [dbo].fn_Linkend_Antenna_pos (LinkEnd_2.id) A2
*/
                      
WHERE     (dbo.LinkFreqPlan_Neighbors.cia IS NOT NULL)

go

----------------------------------------------------------------

IF OBJECT_ID(N'[LinkFreqPlan].[view_LinkFreqPlan_LinkEnd]') IS NOT NULL
  DROP  view [LinkFreqPlan].[view_LinkFreqPlan_LinkEnd]
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [LinkFreqPlan].[view_LinkFreqPlan_LinkEnd]
AS

SELECT  top 1000000   

--  dbo.fn_LinkEnd_Get_Link_ID (dbo.LinkEnd.id) AS link_id, 
 -- dbo.fn_LinkEnd_Get_Next_LinkEnd_ID (dbo.LinkEnd.id) AS next_linkend_id, 


M.*,

link.link_id, 
link.link_name, 
link.next_linkend_id, 
link.clutter_model_id,
link.length,
--link.length_km,



dbo.LinkEnd.name AS LinkEnd_name,
dbo.LinkEnd.property_id, -- AS property_id, 

dbo.LinkEnd.subband, -- AS subband, 

dbo.LinkEnd.LinkEndType_mode_id, -- AS LinkEndType_mode_id,

                      
                      dbo.Property.name AS Property_name, 
                      
--                      dbo.Property.lat AS Property_lat, 
--                      dbo.Property.lon AS Property_lon, 
                     
--
---                     A1.lat AS Property_lat,
   --                   A1.lon AS Property_lon,
 

                      A1.lat,
                      A1.lon,
                      A1.location_type,                     
                   
                      
                      dbo.Property.ground_height AS ground_height,
                      
                      
/*                      dbo.Link.name AS Link_name, 
                      dbo.Link.length AS length, 
                      dbo.Link.length_km AS length_km,                       
                      dbo.Link.clutter_model_id AS clutter_model_id,
*/
                      
                      dbo.LinkEndType_Band.channel_width AS channel_width, 
                      dbo.LinkEndType_Band.txrx_shift, -- AS txrx_shift,
                      dbo.LinkEndType_Band.channel_count, -- AS channel_count, 
                      
                      dbo.LinkEndType_Band.freq_max_high, 
                      dbo.LinkEndType_Band.freq_min_low, 
                      dbo.LinkEndType_Band.name AS LinkEndType_Band_name  ,                    
                      
                      
                      dbo.fn_Linkend_GetMaxAntennaHeight(dbo.LinkEnd.id) AS    MaxAntennaHeight, 
                      
                      dbo.LinkFreqPlan.threshold_degradation AS  LinkFreqPlan_threshold_degradation,
                      dbo.LinkFreqPlan.min_CI_dB AS LinkFreqPlan_min_CI_dB, 
                      

                      dbo.LinkEnd.channel_number, -- AS channel_number, 
                      dbo.LinkEnd.tx_freq_MHz, -- AS tx_freq_MHz, 
                      dbo.LinkEnd.rx_freq_MHz, -- AS rx_freq_MHz, 

                      dbo.LinkEnd.power_dBm, -- AS power_dBm, 
                      dbo.LinkEnd.kng AS kng, 
                      dbo.LinkEnd.loss AS loss, 
                      dbo.LinkEnd.freq_channel_count, -- AS freq_channel_count, 
                      dbo.LinkEnd.channel_type,
                      dbo.LinkEnd.freq_spacing , -- AS freq_spacing,
                      dbo.LinkEnd.kratnostByFreq, -- AS kratnostByFreq,
                      dbo.LinkEnd.kratnostBySpace, -- AS kratnostBySpace, 
                      dbo.LinkEnd.signature_width, -- AS signature_width,
                      dbo.LinkEnd.signature_height, -- AS signature_height, 
                      dbo.LinkEnd.modulation_count, -- AS modulation_count,
                      dbo.LinkEnd.threshold_BER_3, -- AS threshold_BER_3, 
                      dbo.LinkEnd.threshold_BER_6, -- AS threshold_BER_6,
                      
                      dbo.LinkEnd.band, -- AS band,                                         
                      dbo.LinkEnd.linkendtype_id, -- AS linkendtype_id, 
                      dbo.LinkEnd.LinkEndType_band_id, -- AS LinkEndType_band_id, 

                      dbo.LinkEndType_Mode.ch_spacing,    
                      dbo.LinkEndType_Mode.bandwidth AS bandwidth, 
                      dbo.LinkEndType_Mode.bandwidth_tx_3, -- AS bandwidth_tx_3,
                      dbo.LinkEndType_Mode.bandwidth_tx_30, -- AS bandwidth_tx_30, 
                      dbo.LinkEndType_Mode.bandwidth_tx_a, -- AS bandwidth_tx_a,
                      dbo.LinkEndType_Mode.bandwidth_rx_3, -- AS bandwidth_rx_3, 
                      dbo.LinkEndType_Mode.bandwidth_rx_30, -- AS bandwidth_rx_30,
                      dbo.LinkEndType_Mode.bandwidth_rx_a, -- AS bandwidth_rx_a, 

--                 dbo.LinkEndType.FREQ_MAX_HIGH,
 --                     dbo.LinkEndType.FREQ_MIN_LOW,

                      dbo.LinkEndType.level_tx_a,
                      dbo.LinkEndType.level_rx_a,
                      dbo.LinkEndType.name AS LinkEndType_name,
                      dbo.LinkEndType.coeff_noise,
                      
                      
--//                      a.id as ssss,

/*
                      a.POLARIZATION_STR   as antenna_POLARIZATION_STR,
                      a.Polarization_ratio as antenna_Polarization_ratio,
                      a.Gain 			 as antenna_Gain,
                      a.Diameter 		 as antenna_Diameter,
                      a.Loss 			 as antenna_Loss,
                      a.Vert_width 		 as antenna_Vert_width,
                      a.AZIMUTH 		 as antenna_AZIMUTH,
                      a.Height 		 	 as antenna_Height,
                 --     a.Polarization_ratio as antenna_Polarization_ratio
                      
*/

(select count(*) from Linkend_Antenna where Linkend_id = dbo.LinkEnd.id) as Antenna_count
                   
                      
                      
FROM         dbo.LinkEndType_Band RIGHT OUTER JOIN
                      dbo.LinkEndType_Mode RIGHT OUTER JOIN
                      dbo.LinkEnd LEFT OUTER JOIN
                      dbo.LinkEndType ON dbo.LinkEnd.linkendtype_id = dbo.LinkEndType.id ON dbo.LinkEndType_Mode.id = dbo.LinkEnd.LinkEndType_mode_id ON 
                      dbo.LinkEndType_Band.id = dbo.LinkEnd.LinkEndType_band_id RIGHT OUTER JOIN
                   --   dbo.Link RIGHT OUTER JOIN
                      dbo.LinkFreqPlan_LinkEnd M LEFT OUTER JOIN
                      dbo.LinkFreqPlan ON M.LinkFreqPlan_id = dbo.LinkFreqPlan.id 
--                      ON dbo.Link.id = dbo.LinkFreqPlan_LinkEnd.link_id 
                      ON  dbo.LinkEnd.id = M.linkend_id LEFT OUTER JOIN
                      dbo.Property ON dbo.LinkEnd.property_id = dbo.Property.id
                      
                      OUTER apply fn_Linkend_Link_info(dbo.LinkEnd.id) link
                      
                      cross apply [dbo].fn_Linkend_Antenna_pos (LinkEnd.ID) A1
                       
          /*             
                       left join (
                          select Linkend_Antenna.*, AntennaType.Polarization_ratio 
                             from Linkend_Antenna
                               left join AntennaType  on AntennaType.id = Linkend_Antenna.antennaType_id
                       
                       ) A on a.linkend_id = LinkEnd.id
     
*/
                      
                      
     order by  M.id

go

-----------------------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID(N'LinkFreqPlan.view_LinkFreqPlan_LinkEnd_antennas') IS NOT NULL
  DROP  view LinkFreqPlan.view_LinkFreqPlan_LinkEnd_antennas
GO



CREATE VIEW LinkFreqPlan.view_LinkFreqPlan_LinkEnd_antennas
AS
SELECT   

--LinkEnd.id as aaaaaaaaaaaaaa,


  dbo.Linkend_Antenna.*, dbo.Property.ground_height AS ground_height, dbo.LinkFreqPlan_LinkEnd.LinkFreqPlan_id AS LinkFreqPlan_id, 
                      dbo.AntennaType.polarization_ratio AS polarization_ratio, dbo.AntennaType.vert_mask, dbo.AntennaType.horz_mask, 
                      
LinkEnd.name as LinkEnd_name,
                      
                        
       A1.lat AS map_lat,
       A1.lon AS map_lon,
                      
                      
                      dbo.Property.lat AS Property_lat, 
                      dbo.Property.lon AS Property_lon
                      
                      
                      
FROM         dbo.Property RIGHT OUTER JOIN
                      dbo.LinkEnd LEFT OUTER JOIN
                      dbo.AntennaType RIGHT OUTER JOIN
                      dbo.Linkend_Antenna ON dbo.AntennaType.id = dbo.Linkend_Antenna.antennaType_id ON dbo.LinkEnd.id = dbo.Linkend_Antenna.linkend_id ON 
                      dbo.Property.id = dbo.Linkend_Antenna.property_id RIGHT OUTER JOIN
                      dbo.LinkFreqPlan_LinkEnd ON dbo.LinkEnd.id = dbo.LinkFreqPlan_LinkEnd.linkend_id
                      

                      

     cross apply [dbo].fn_Linkend_Antenna_pos (Linkend_Antenna.linkend_id) A1
     
--where LinkFreqPlan_id=1016

go
------------------------------------------------------------------

-------------------------------------------------------------
IF OBJECT_ID(N'[dbo].[view_LinkFreqPlan_Calc_Neighbors]') IS NOT NULL
  DROP  view dbo.view_LinkFreqPlan_Calc_Neighbors
GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW dbo.view_LinkFreqPlan_Calc_Neighbors
AS
SELECT dbo.LinkFreqPlan_Neighbors.*,
       
       dbo.LinkEndType.coeff_noise as coeff_noise, 

       dbo.LinkEndType_Mode.bandwidth as bandwidth,
      
       LinkEnd.LinkEndType_mode_id,
       
       LinkEnd.name as TX_LinkEnd_name,
       LinkEnd2.name as RX_LinkEnd_name
       
       
FROM dbo.LinkEnd

     LEFT OUTER JOIN dbo.LinkEndType_Mode ON dbo.LinkEnd.LinkEndType_mode_id =   dbo.LinkEndType_Mode.id

     LEFT OUTER JOIN dbo.LinkEndType ON dbo.LinkEnd.linkendtype_id =    dbo.LinkEndType.id
     RIGHT OUTER JOIN dbo.LinkFreqPlan_Neighbors ON dbo.LinkEnd.id =   dbo.LinkFreqPlan_Neighbors.rx_linkend_id
     
     LEFT JOIN dbo.LinkEnd LinkEnd2 ON LinkEnd2.id =   dbo.LinkFreqPlan_Neighbors.tx_linkend_id

go

----------------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-------------------------------------------------------------
IF OBJECT_ID(N'[dbo].[sp_LinkFreqPlan_Linkend]') IS NOT NULL
  DROP  PROCEDURE dbo.sp_LinkFreqPlan_Linkend 
GO



/* ---------------------------------------------------------------
created: Alex  
used in:  TdmLinkFreqPlan_CIA_view.Load_LinkFreqPlan
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_LinkFreqPlan_Linkend 
(
@LinkFreqPlan_id  INT
)

AS
BEGIN
--  DECLARE @project_id int

/*
    SELECT @project_id=project_id 
    	FROM LINKFREQPLAN
      WHERE id=@LinkFreqPlan_id
*/


 --SELECT  0  as ddd 



    SELECT 
      LinkEnd_name ,  
     length,
    
    
      LinkEnd.[name] as rx_LinkEnd_name,
    
    
 --    (SELECT name FROM LinkEnd 
    --   WHERE id=LINKFREQPLAN_LINKEND.linkend_id)  as [linkend_name],

     (SELECT [rx_level_dBm] FROM LINKFREQPLAN_NEIGHBORS 
          WHERE  (LinkFreqPlan_id=@LinkFreqPlan_id) AND 
                 (IsLink = 1) AND
                 (rx_linkend_id = M.linkend_id))   AS [rx_level_dBm],
                 
     
--     (SELECT length FROM Link 
  --     WHERE (project_id=@project_id) and
    --         ((linkend1_id=LINKFREQPLAN_LINKEND.linkend_id) OR 
      --        (linkend2_id=LINKFREQPLAN_LINKEND.linkend_id)))
        --     AS [length],

     (SELECT Count(*) FROM LINKFREQPLAN_NEIGHBORS 
         WHERE (IsLink<>1) and 
       
             (LinkFreqPlan_id=@LinkFreqPlan_id) and
             (RX_Linkend_id= M.linkend_id) )
             as [count], 
             
     (SELECT Count(*) FROM LINKFREQPLAN_NEIGHBORS 
         WHERE (IsNeigh=1) and (IsLink<>1) and 
       
             (LinkFreqPlan_id=@LinkFreqPlan_id) and
             (RX_Linkend_id=M.linkend_id))
             as [count1], 
             
     m.id,     
     cia, ci, ca, linkend_id, threshold_degradation 
     
    FROM  view_LINKFREQPLAN_LINKEND M    
        LEFT OUTER JOIN LinkEnd  
          ON M.next_linkend_id = LinkEnd.id

--link.next_linkend_id, 

  --      LEFT OUTER JOIN Link 
    --      ON LinkFreqPlan_LinkEnd.link_id = Link.id

      
     WHERE  (LinkFreqPlan_id=@LinkFreqPlan_id)
   
  
     

END

go

