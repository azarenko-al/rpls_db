
delete from project_tables
  where name in ('LINKFREQPLAN','pmp_site','PMP_Sector','PmpTERMINAL','link_repeater','LINKEND_ANTENNA','LINKEND')
go


IF OBJECT_ID(N'[dbo].[sp_Project_summary]') IS NOT NULL
  DROP PROCEDURE dbo.sp_Project_summary 
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.sp_Project_summary
    @USER_NAME sysname = null
AS
BEGIN
  if @USER_NAME is null  
    set @USER_NAME = USER_NAME()

 
 
 
 SELECT  
   CAST(0 as bit) as checked,


                    (SELECT     COUNT(*)     FROM          property
                     WHERE      project_id = m.id) AS property_count,
                       
                    (SELECT     COUNT(*) 	FROM  PMP_site 
                    	LEFT JOIN dbo.Property P ON dbo.PMP_site.property_id = PMP_site.id                                              
                      WHERE      project_id = m.id) AS pmp_site_count,
                                                                        
                      
                      --view_PMP_site_with_project_id

                    (SELECT     COUNT(*)  FROM   LinkEnd                
     					  LEFT OUTER JOIN dbo.Property ON dbo.LinkEnd.property_id = dbo.Property.id                    
                        WHERE LinkEnd.ObjName = 'PMP_terminal' and project_id = m.id) AS pmp_terminal_count,
                            
                    (SELECT     COUNT(*)        FROM          pmp_calcregion
                      WHERE      project_id = m.id) AS pmp_calcregion_count,
                            
                    (SELECT     COUNT(*)       FROM          link
                      WHERE      project_id = m.id) AS link_count,

                            

                    (SELECT     COUNT(*)    FROM   Link_Repeater 
                       LEFT OUTER JOIN dbo.Property ON dbo.Link_Repeater.property_id = dbo.Property.id                                        
                      WHERE      (project_id = m.id)   ) AS link_Repeater_count,

                           
                    (SELECT     COUNT(*)     FROM    linkline
                      WHERE      project_id = m.id) AS linkline_count,
                      
                    (SELECT     COUNT(*)      FROM    linknet
                      WHERE      project_id = m.id) AS linknet_count,
                       

                    (SELECT     COUNT(*)    FROM    LinkFreqPlan
                        LEFT OUTER JOIN dbo.LinkNet ON dbo.LinkNet.id = dbo.LinkFreqPlan.LinkNet_id
                      WHERE project_id = m.id )  AS linkfreqplan_count,
                            

                    (SELECT     COUNT(*)    FROM  LinkEnd --_with_project_id
                         LEFT OUTER JOIN dbo.Property ON dbo.LinkEnd.property_id = dbo.Property.id   
                      WHERE      project_id = m.id) AS linkend_count,
                            
                    (SELECT     COUNT(*)   FROM  Linkend_Antenna --_with_project_id
                        LEFT OUTER JOIN dbo.Property ON dbo.Linkend_Antenna.property_id = dbo.Property.id   
                      WHERE      project_id = m.id) AS linkend_antenna_count,


                     (SELECT     COUNT(*)        FROM          link_SDB
                        WHERE      project_id = m.id) AS link_SDB_count,
 
                                                        
                      (SELECT     MAX(datetime)    FROM          activity_log
                        WHERE      (project_id = m.id)) AS last_datetime, 
                            
                            
                        M.*
--                            m.user_created
                            
                            
FROM         dbo.Project M


where 
    @USER_NAME = 'dbo' 
  or user_created =  @USER_NAME 
  or id in (select project_id from Security.ft_Projects_allowed (@USER_NAME))
 



END

go

-------------------------------------------------------------------------------------------

IF OBJECT_ID(N'[dbo].[sp_Project_Clear_]') IS NOT NULL
  DROP PROCEDURE dbo.sp_Project_Clear_
GO


GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Project_Clear_ 
(
  @ID int=null
)
AS
BEGIN
  ------------------------------------------------------
 -- IF @ID is null  RAISERROR ('@ID is null', 16, 1);
  ------------------------------------------------------


declare @prop TABLE (id int)



insert into @prop
  select ID from property WHERE project_id = @ID;  


--if @ID is null 



-- SELECT * INTO #Project  FROM Project  WHERE id = @ID



--	set nocount on

--	begin TRANSACTION
	
	
--	ROLLBACK TRANSACTION
	
	
	
	-- CalcRegion --------------------------
--	delete from pmp_CalcRegionCells
--	  where pmp_calc_region_id in
 --      (SELECT id  from pmp_CALCREGION  where Project_ID = @ID);
	-----------------------------------------------------
    -- PMP CALCREGION
	-----------------------------------------------------
   
--	delete from PMP_CalcRegionCells where Project_ID = @ID;
  delete from pmp_CALCREGION      where Project_ID = @ID;
		
  
  delete from Linkend_Antenna    where Property_ID in (SELECT id FROM @prop);    

--	delete from view_Linkend_Antenna_with_project_id where Project_ID = @ID;
  
  

-- ref to PROPERTY
--	delete from Link_Repeater where Project_ID in (SELECT id FROM #Project);


		
	--GSM ----------------------------------
--	delete from CELL          where Project_ID= @ID;
--	delete from SITE          where Project_ID = @ID;
	--delete from LAC           where Project_ID = @ID;
	--delete from BSIC          where Project_ID = @ID;

--	delete from DriveTest     where Project_ID = @ID;

--	delete from FREQPLAN      where Project_ID = @ID;
--	delete from TrafficModel  where Project_ID = @ID;
	
	/*
	delete from CalcRegionCells
	  where calc_region_id in (SELECT id  from CALCREGION  where Project_ID = @ID);
	
	delete from CALCREGION      where Project_ID = @ID;
	*/
	

	--LINKLINE, LINKNET ----------------------------------
	delete from LINKLINE      where Project_ID = @ID;
	delete from LINKNET       where Project_ID = @ID;


	--LINK ----------------------------------
  delete from Link_Repeater 
       where Property_ID in (SELECT id FROM @prop);  
        
--	delete from LINKFREQPLAN  where Project_ID = @ID;
	delete from LINK_sdb       where Project_ID = @ID;

	delete from LINK          where Project_ID = @ID;
	
	--delete from Link_Repeater  where Project_ID = @ID;


	--LINKEND ---------------------------------- 
	delete from LINKEND   where Property_ID in (SELECT id FROM @prop);  
	
	
	--PMP ----------------------------------
--	delete from PMPTERMINAL  -- where Project_ID = @ID;
--       where Property_ID in (SELECT id FROM Property WHERE project_id = @ID);  


--	delete from PMP_SECTOR   -- where Project_ID = @ID;
--       where Property_ID in (SELECT id FROM Property WHERE project_id = @ID);  

	delete from PMP_SITE      where Property_ID in (SELECT id FROM @prop);  
    
    
--	delete from PMP_SITE      where Project_ID = @ID;
	

	--common ----------------------------------
	delete from MSC           where Project_ID = @ID;
	delete from BSC           where Project_ID = @ID;
  
  
--	delete from Link_Repeater 
  --   where property_ID in (SELECT ID from PROPERTY  where Project_ID = @ID);
  

--	SELECT *  from Link_Repeater 
  --   where property_ID in (SELECT ID from PROPERTY  where Project_ID = @ID);
  
  
--  return
  
  
  
	delete from PROPERTY      where Project_ID = @ID;


	delete from RELIEF        where Project_ID = @ID;

	
	delete from CalcMAP       where Project_ID = @ID;
	delete from [MAP]         where Project_ID = @ID;
    

  DELETE FROM Map_XREF
       WHERE map_desktop_id in  (SELECT id FROM Map_Desktops  where Project_ID = @ID)

	delete from Map_Desktops  where Project_ID = @ID; 
	
	delete from FOLDER        where Project_ID = @ID;
		

--	delete from project      where ID in (SELECT id FROM #Project);
	

--	commit TRANSACTION
	
	return 1;

--	set nocount off

END

go

------------------------------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID(N'[dbo].[sp_Project_restore_after_import]') IS NOT NULL
  DROP PROCEDURE dbo.sp_Project_restore_after_import
GO



CREATE PROCEDURE dbo.sp_Project_restore_after_import
(
	@Project_ID int = 1568
)
AS
BEGIN

    

SELECT     dbo.Linkend_Antenna.id, dbo.Linkend_Antenna.antennaType_id
  into #temp

FROM   dbo.Property RIGHT OUTER JOIN
                      dbo.Linkend_Antenna ON dbo.Property.id = dbo.Linkend_Antenna.property_id
where dbo.Property.project_id = @Project_ID
and antennaType_id is not null



declare 
	@id int,
    @antennaType_id int;

  
while exists (select * from #temp)
begin
  select top 1 @id=id, @antennaType_id=antennaType_id from #temp

	exec  sp_Linkend_Antenna_Update_AntennaType @id=@id, @antennaType_id=@antennaType_id


  delete top (1) from #temp
end

    
drop table #temp


END

go

