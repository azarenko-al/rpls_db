IF OBJECT_ID(N'dbo.fn_Folder_FullPath_GUID') IS NOT NULL
  DROP FUNCTION dbo.fn_Folder_FullPath_GUID 

GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE FUNCTION dbo.fn_Folder_FullPath_GUID 
(
  @ID INT
)
RETURNS varchar(2000)
AS
BEGIN
  DECLARE  
   @parent_id   int,
   @GUID        VARCHAR(2000);
 
  SELECT
   -- @id   = id,
         @parent_id  = parent_id,        
         @GUID = CAST(GUID as varchar(50))
    FROM Folder 
    WHERE id=@ID

 
  IF @parent_id IS NOT NULL
    set @GUID = dbo.fn_Folder_FullPath_GUID(@parent_id) + '   '+ @GUID  

  RETURN @GUID
END

go

--------------------------------------------------

IF OBJECT_ID(N'dbo.sp_Explorer_Search') IS NOT NULL
  DROP PROCEDURE dbo.sp_Explorer_Search


GO


CREATE PROCEDURE dbo.sp_Explorer_Search
(
  @SEARCH varchar(100)  =null,
  @WHOLE_WORD bit=null,
  @PROJECT_ID int =null
)
AS
BEGIN
 

 -- SELECT * FROM 

  declare @t TABLE (  
       objname  varchar(50) , 
       id int, 
       name varchar(200), 
       
       PROJECT_ID int,
       folder_path  varchar(1000)  
       )


  declare
     @s varchar(50);

  if IsNull(@WHOLE_WORD,0)=1 
    set @s=@SEARCH
  ELSE  
    set @s='%'+ @SEARCH +'%'

--//CREATE FUNCTION dbo.fn_Folder_FullPath 

  


INSERT INTO @t (objname, id, name,  PROJECT_ID, folder_path )  
  SELECT 'property' as objname, id, name,  PROJECT_ID , dbo.fn_Folder_FullPath_GUID (folder_id)  
    FROM property 
    WHERE  ((name like (@s)) or  
            (NRI like (@s)) --or 
           -- (bee_beenet like (@s)) 
           )
          and  (PROJECT_ID =@PROJECT_ID)



INSERT INTO @t (objname, id, name,  PROJECT_ID , folder_path )  
  SELECT 'link' as objname, id, name,  PROJECT_ID , dbo.fn_Folder_FullPath_GUID (folder_id)
    FROM link 
    WHERE (name like (@s))    and  (PROJECT_ID =@PROJECT_ID)

 
SELECT * FROM @t

/*
  
  SELECT 'property' as objname, id, name,  PROJECT_ID   
    FROM property 
    WHERE (name like ('%'+ @SEARCH +'%'))  and  (PROJECT_ID =@PROJECT_ID)

union


  SELECT 'link' as objname, id, name,  PROJECT_ID
    FROM link 
    WHERE (name like ('%'+ @SEARCH +'%'))    and  (PROJECT_ID =@PROJECT_ID)
*/

 -- order by Display_Order



  /* Procedure body */
END

go

--------------------------------------------------