if schema_id('_db_update') is null  execute('create schema _db_update')
GO 
if object_id('_db_update.view_FK') is not null    drop view _db_update.view_FK 
GO 
CREATE VIEW _db_update.view_FK 
AS
-- schema_name (o.schema_id) as schema_name,

  SELECT 
   f.name,--   AS 'Name of Foreign Key',
  
/*   delete_referential_action,
   update_referential_action,

   delete_referential_action_desc,
   update_referential_action_desc,
*/   
   
   schema_name (f.schema_id) as schema_name,   
   
   schema_name (f.schema_id) +'.'+ OBJECT_NAME(f.parent_object_id) AS Table_name,


  -- OBJECT_NAME(f.parent_object_id) AS Table_name,

   
   COL_NAME(fc.parent_object_id, fc.parent_column_id) AS 'column_name',
   
--   schema_name (t.schema_id) as References_schema_name,   
   schema_name (t.schema_id) +'.'+ OBJECT_NAME(t.object_id) as 'References_Table_name',

--   schema_name (t.schema_id) +'.'+ OBJECT_NAME(t.object_id) as References_schema_name_full,   


   
   COL_NAME(t.object_id, fc.referenced_column_id) AS 'References_column_name',

--------------------------------------

 'if object_id('''+f.name+''') is not null  '+  CHAR(10) +

replace (replace (
   'ALTER TABLE :table  DROP CONSTRAINT :name',
':table', schema_name (t.schema_id) +'.'+ OBJECT_NAME(f.parent_object_id) ),
':name', f.name ) 

 + ';'+ CHAR(10)
 
as sql_Drop,




--------------------------------------
 --  'ALTER TABLE [' + OBJECT_NAME(f.parent_object_id) + ']  DROP CONSTRAINT [' + f.name + ']' AS 'Delete foreign key',

--------------------------------------
--replace (replace (
--   'ALTER TABLE [:table]  WITH NOCHECK ADD CONSTRAINT [:name] FOREIGN KEY([:foreign_key]) REFERENCES [ + OBJECT_NAME(t.object_id) + '] ([' +
--        COL_NAME(t.object_id,fc.referenced_column_id) + '])' AS 'Create foreign key',


--WITH NOCHECK 

   'ALTER TABLE ' + schema_name (f.schema_id) +'.'+ OBJECT_NAME(f.parent_object_id) + '  ADD CONSTRAINT ' + 
        f.name + ' FOREIGN KEY([' + COL_NAME(fc.parent_object_id,fc.parent_column_id) + ']) REFERENCES ' + 
         schema_name (t.schema_id) +'.'+ OBJECT_NAME(t.object_id) + ' ([' +
        COL_NAME(t.object_id,fc.referenced_column_id) + '])' 
        + IIF(delete_referential_action_desc = 'cascade', ' on delete cascade', '')
        + IIF(update_referential_action_desc = 'cascade', ' on update cascade', '')
        
 + ';'+ CHAR(10)        
        
        AS sql_create

/*
      if FK.IsUpdateCascade1 then sON:= sON+' ON UPDATE CASCADE';
      if FK.IsDeleteCascade  then sON:= sON+' ON DELETE CASCADE';
*/
        
        
    -- , delete_referential_action_desc AS 'UsesCascadeDelete'
FROM sys.foreign_keys AS f,
     sys.foreign_key_columns AS fc,
     sys.tables t 
     
WHERE
  f.OBJECT_ID = fc.constraint_object_id
AND t.OBJECT_ID = fc.referenced_object_id


and schema_name (f.schema_id)  not in ('security','service')
--and schema_name (f.schema_id)<>'service'



--AND OBJECT_NAME(t.object_id) = 'Employees'      --  Just show the FKs which reference a particular table
GO 

if object_id('_db_update.view_Index') is not null    drop view _db_update.view_Index 
GO 
CREATE VIEW _db_update.view_Index  
AS


select *,


schema_name+'.'+Table_Name as Table_Name_full,



replace (replace (replace (replace (

'IF IndexProperty(Object_Id('':TABLE''), '':name'', ''IndexId'') Is Null '+  char(10) +


case 

--ADD PRIMARY KEY CLUSTERED

  when type_desc='SPATIAL'	  then 'CREATE SPATIAL INDEX [:name] ON :TABLE (:columns) USING GEOMETRY_AUTO_GRID ' +
                                    ' WITH ( BOUNDING_BOX = ( XMIN = 0, YMIN = 0, XMAX = 180, YMAX = 90 ) )'
  
  when is_primary_key=1 	  then 'ALTER TABLE :TABLE ADD PRIMARY KEY :CLUSTERED (:columns)  ON [PRIMARY] '  
  when is_unique_constraint=1 then 'ALTER TABLE :TABLE ADD CONSTRAINT :name UNIQUE :CLUSTERED (:columns)  ON [PRIMARY] ' 
  when is_unique=1 			  then 'CREATE UNIQUE INDEX :name ON :TABLE (:columns) ON [PRIMARY]  '  
  
                              else 'CREATE INDEX :name ON :TABLE (:columns)  ON [PRIMARY] '
end ,
 ':TABLE', schema_name+'.'+Table_Name),
 ':name', Index_Name),
 ':columns', IndexColumnsNames),
 ':CLUSTERED', type_desc)  

 + char(10) 
 
as [sql_create],

/*
CREATE INDEX IX_IndexName
ON dbo.TableName
WITH (DROP_EXISTING = ON);

*/
-------------------------



replace (replace (replace (

'IF IndexProperty(Object_Id('':TABLE''), '':name'', ''IndexId'') Is not Null '+  char(10) +

case 
  when is_primary_key=1 or is_unique_constraint=1
                           	  then ' ALTER TABLE :TABLE DROP CONSTRAINT :name' 
   
--  when is_unique=1 			  then ' ALTER TABLE :TABLE DROP CONSTRAINT :name' 
--  when is_unique_constraint=1 then ' ALTER TABLE :TABLE DROP CONSTRAINT :name' 
--  when is_unique=1 			  then 'DROP INDEX :name ON :TABLE' 
                              else ' DROP INDEX  :name ON :TABLE '
end ,
 ':TABLE', schema_name+'.'+Table_Name),
 ':name', Index_Name),
 ':columns', IndexColumnsNames)  
 
  + char(10) as [sql_drop],
  
  
-----------------------------------------------------------------------

schema_name+'.'+Table_Name +','+ -- char(10)  +

IIF(is_primary_key=1, 'is_primary_key,','') +
IIF(is_unique=1, 'is_unique,', '') +
IIF(is_unique_constraint=1, 'is_unique_constraint,','')  + --char(10) +
 
IndexColumnsNames as [Definition]
  
 

from 

(


  SELECT 
    create_date,
    modify_date,

case
  when INX.type_desc='CLUSTERED' then 'CLUSTERED'
  when INX.type_desc='NONCLUSTERED' then 'NONCLUSTERED'
  when INX.type_desc='SPATIAL' then 'SPATIAL'

end as  type_desc,
  
      INX.object_id,
      INDEX_id,

      INX.[name] AS [Index_Name],
      TBL.[name] AS [Table_Name],
      schema_name(TBL.schema_id) as schema_name,

      INX.is_primary_key,
      INX.is_unique,
      INX.is_unique_constraint,
      
      DS1.[IndexColumnsNames]
   --   DS2.[IncludedColumnsNames]
      
      
      
FROM [sys].[indexes] INX
INNER JOIN [sys].[tables] TBL
    ON INX.[object_id] = TBL.[object_id]
    
    
CROSS APPLY 
(
    SELECT STUFF
    (
        (
            SELECT ',[' + CLS.[name] + ']'
            FROM [sys].[index_columns] INXCLS
            INNER JOIN [sys].[columns] CLS 
                ON INXCLS.[object_id] = CLS.[object_id] 
                AND INXCLS.[column_id] = CLS.[column_id]
            WHERE INX.[object_id] = INXCLS.[object_id] 
                AND INX.[index_id] = INXCLS.[index_id]
                AND INXCLS.[is_included_column] = 0
            FOR XML PATH('')
        )
        ,1
        ,1
        ,''
    ) 
) DS1 ([IndexColumnsNames])

    where INX.[name] is not null
    
) M

where 
  Table_Name not like '%[___]'  and

  Table_Name <> 'msfavorites'  and
  Table_Name not like '%111'  
  
  and schema_name not in ('test','service')



--and Table_Name ='Climate'
GO 

if object_id('_db_update.view_check_constraint') is not null    drop view _db_update.view_check_constraint 
GO 
CREATE VIEW _db_update.view_check_constraint 
AS

select * ,

 /*
REPLACE(replace(replace(
'ALTER TABLE :table WITH NOCHECK ADD CONSTRAINT [:name] CHECK :definition',
':table', schema_name + '.'+ table_name ),   
':definition', [definition] ),
':name', [constraint_name] )

select * from sys.default_constraints 

as sql_create,   
-------------------------------
*/

schema_name + '.'+  table_name as table_name_full,

REPLACE(replace(
'ALTER TABLE :table DROP CONSTRAINT [:name]',
':table', schema_name + '.'+  table_name ),   
':name', [constraint_name] )

as sql_drop   

-----------------------------

from

(

  select con.[name] as constraint_name,
    schema_name(t.schema_id) as schema_name, 
    
   

    
/*      SQL_ALTER_TABLE_ADD_CHECK_CONSTRAINT_no_proc =
   'ALTER TABLE [%s] WITH NOCHECK ADD CONSTRAINT [%s] CHECK %s';  //+ DEF_GO_CRLF;
*/
    
    
   -- + '.' + 
    t.[name]  as [table_name],
    
--    t.create_date,
--    t.modify_date,
    
    col.[name] as column_name,
    con.[definition]
    
/*    ,
    case when con.is_disabled = 0 
        then 'Active' 
        else 'Disabled' 
        end as [status]
*/
        
from sys.check_constraints con
    left outer join sys.objects t
        on con.parent_object_id = t.object_id
    left outer join sys.all_columns col
        on con.parent_column_id = col.column_id
        and con.parent_object_id = col.object_id
        
) M

/*
SELECT 
  *, 
   object_definition(OBJECT_ID(CONSTRAINT_NAME)) as definition

 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
 where CONSTRAINT_TYPE = 'check'
 
 */
GO 

if object_id('_db_update.view_View') is not null    drop view _db_update.view_View 
GO 
CREATE VIEW _db_update.view_View
AS
  select 
  
  o.NAME,
  
  schema_name (o.schema_id) + '.' + o.NAME as name_full, 
  
 /* o.create_date,
  o.modify_date,
  */
 -- schema_name (o.schema_id) as schema_name,
  
  definition,
  
 replace(
        'if object_id('':name'') is not null    drop view :name ', 
                                  
        ':name', schema_name (o.schema_id) + '.' + o.NAME) 
        
        + char(13)+char(10)+ 'GO ' + char(13)+char(10)+
        definition +  
        'GO ' + char(13)+char(10)
        
        as sql_create       
  
  
from sys.objects     o
  join sys.sql_modules m on m.object_id = o.object_id


where  
     o.type = 'V'
   and 
     schema_name(o.schema_id) = '_db_update'
     
      and o.NAME not like '%[111]'
GO 

if object_id('_db_update.view_Proc') is not null    drop view _db_update.view_Proc 
GO 
CREATE VIEW _db_update.view_Proc
AS
  select 

 -- o.type,  
  
  o.NAME,


  schema_name (o.schema_id) + '.' + o.NAME as name_full,   
  
/*  o.create_date,
  o.modify_date,
*/
    
 -- schema_name (o.schema_id) as schema_name,
  
--  definition,
  
 replace(
        IIF(o.type in ('p'), 'if object_id('':name'') is not null    drop procedure :name ',  
                             'if object_id('':name'') is not null    drop function  :name '), 
                                  
        ':name', schema_name (o.schema_id) + '.' + o.NAME) 
        + char(13)+char(10)+
        'GO ' + + char(13)+char(10)+ 
        definition +  
        'GO ' + char(13)+char(10) 
        
        as sql_create     
  
  
from sys.objects     o
join sys.sql_modules m on m.object_id = o.object_id

where
   o.type  in ('fn','p','tf','IF')
  and 
     schema_name(o.schema_id) = '_db_update'
     
   and o.NAME not like '%[111]'
 --  and o.NAME <> 'sp_Main'
GO 

if object_id('_db_update.view_Column_DF') is not null    drop view _db_update.view_Column_DF 
GO 
CREATE VIEW _db_update.view_Column_DF
AS
  


    select	SCHEMA_NAME(t.schema_id) +'.'+ t.name [table_name_full], 
           c.name [column_name], 
           d.name [constraint],  
           d.definition,

--SCHEMA_NAME(t.schema_id) as table_SCHEMA,

sql_drop   = 'ALTER TABLE '+ SCHEMA_NAME(t.schema_id) +'.'+t.name +' DROP CONSTRAINT '+ d.name ,

--sql_create = 'ALTER TABLE '+ SCHEMA_NAME(t.schema_id) +'.'+t.name +' ADD DEFAULT '' (suser_name()) ''  FOR '+ c.name 

sql_create ='ALTER TABLE '+  SCHEMA_NAME(t.schema_id) +'.'+t.name +' ADD DEFAULT (' +  d.definition  +') FOR '+ c.name 



from	sys.default_constraints d
join	sys.tables t  
  on	t.object_id = d.parent_object_id
join	sys.columns c
  on	c.object_id = t.object_id
  and	c.column_id = d.parent_column_id
--  and d.definition = '(user_name())'


/*
select	
  t.name [table_name], 
  c.name [column_name], 
  d.name [constraint],

  SCHEMA_NAME(t.schema_id) +'.'+ t.name as table_name_full,

  SCHEMA_NAME(t.schema_id) as table_SCHEMA,


  sql_drop = 'ALTER TABLE '+ SCHEMA_NAME(t.schema_id) +'.'+t.name +' DROP CONSTRAINT '+ d.name 


   
  @sql_create ='ALTER TABLE '+  @TABLE_NAME +' ADD CONSTRAINT '+ o.name + '  DEFAULT(' +  com.text  +') FOR '+ @COLUMN_NAME,
                
     @sql_drop= 'ALTER TABLE '+  @TABLE_NAME +' DROP CONSTRAINT '+ o.name




from	sys.default_constraints d
join	sys.tables t  
  on	t.object_id = d.parent_object_id
join	sys.columns c
  on	c.object_id = t.object_id
  and	c.column_id = d.parent_column_id
  
  */
GO 

if object_id('_db_update.Drop_FK') is not null    drop procedure _db_update.Drop_FK 
GO 
CREATE PROCEDURE _db_update.Drop_FK
------------------------------------------------------------------
(
@NAME  VARCHAR(200)

)
as
BEGIN


declare 
  @sql_drop varchar(max)


  select  
       @sql_drop  =  sql_drop
    
    from _db_update.view_FK
    where  NAME = @NAME
           
--           IndexColumnsNames like '%[[]'+ @COLUMN_NAME  +']%'


  print @sql_drop
  exec ( @sql_drop  )

END
GO 

if object_id('_db_update.Drop_table') is not null    drop procedure _db_update.Drop_table 
GO 
CREATE PROCEDURE _db_update.Drop_table
------------------------------------------------------------------
(
  @TABLE_NAME VARCHAR(50)
)
AS
BEGIN
  declare
   @sql varchar(4000);



  if OBJECT_ID(@TABLE_NAME) is null 
    return -1

--------------------------------------------



  set @sql=''
     
  SELECT 
    @sql=@sql + sql_drop

  from _db_update.view_FK  
  where 
    ( Table_name = @TABLE_NAME ) or
    ( References_Table_name = @TABLE_NAME  )


--------------------------------------------

 print @sql   
 exec (@sql)        

--------------------------------------------
   
 set @sql= 'DROP TABLE '+ @TABLE_NAME 
 print @sql   
 exec (@sql)        

end
GO 

if object_id('_db_update.Column_Exists') is not null    drop function  _db_update.Column_Exists 
GO 
CREATE FUNCTION _db_update.Column_Exists
(
  -----  @Table_SCHEMA varchar(100)='dbo', 
	@TableName varchar(100), 
    @ColumnName varchar(100) 
)
--------------------------------------------------------------------------------
RETURNS bit AS
BEGIN
 -- DECLARE @Result bit;
/*
  IF not (OBJECT_ID(@TableName)>0)
  begin 
  -- print 'OBJECT_ID(@TableName) is null):' + @TableName
    RETURN -1;
  end*/
/*
if  COL_LENGTH('dbo.LinkEnd','project_id')  is not null
  ALTER TABLE dbo.LinkEnd
   drop  COLUMN project_id
GO
*/


if  COL_LENGTH(@TableName, @ColumnName)  is not null
  RETURN 1
  
 RETURN 0  

/*
  IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.Columns 
              WHERE 
                    Table_SCHEMA +'.'+ TABLE_NAME = @TableName 
--              	and TABLE_NAME = @TableName 
                AND COLUMN_NAME = @ColumnName)
    RETURN 1
    --SET @Result = 1
    
    RETURN 0
    --SET @Result = 0

*/

--@Result;

END
GO 

if object_id('_db_update.Column_DF_Get') is not null    drop procedure _db_update.Column_DF_Get 
GO 
CREATE PROCEDURE _db_update.Column_DF_Get
---------------------------------------------------------------
(
 -- @Table_SCHEMA varchar(100)='dbo', 


  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(50),
  
  @sql_create VARCHAR(max) output, 
  @sql_drop   VARCHAR(max) output
  
)
as
BEGIN

  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME ='lib_Radiation_class'
    set @COLUMN_NAME='name'  
  END
 

  set @sql_create=''
  set @sql_drop=''
  



   SELECT
   -- @DEFAULT_NAME =o.name 
   
     @sql_create = sql_create, -- 'ALTER TABLE '+  @TABLE_NAME +' ADD CONSTRAINT '+ o.name + '  DEFAULT(' +  com.text  +') FOR '+ @COLUMN_NAME,
                
     @sql_drop = sql_drop --'ALTER TABLE '+  @TABLE_NAME +' DROP CONSTRAINT '+ o.name
   
   --o.name, com.text   
     FROM _db_update.view_Column_DF
     
     WHERE 
       TABLE_NAME_full = @TABLE_NAME 
       and  
       COLUMN_NAME = @COLUMN_NAME


 
/*
   SELECT
   -- @DEFAULT_NAME =o.name 
   
     @sql_create ='ALTER TABLE '+  @TABLE_NAME +' ADD CONSTRAINT '+ o.name + '  DEFAULT(' +  com.text  +') FOR '+ @COLUMN_NAME,
                
     @sql_drop= 'ALTER TABLE '+  @TABLE_NAME +' DROP CONSTRAINT '+ o.name
   
   --o.name, com.text   
     FROM sysobjects o INNER JOIN                       
          syscolumns c ON o.id = c.cdefault INNER JOIN  
          syscomments com ON o.id = com.id              
     WHERE (o.xtype = 'D') 
       and (OBJECT_NAME(o.parent_obj)=@TABLE_NAME) 
       and (c.name=@COLUMN_NAME)
    
*/
  
  return @@ROWCOUNT
  

END
GO 

if object_id('_db_update.Column_FK_Get') is not null    drop procedure _db_update.Column_FK_Get 
GO 
CREATE PROCEDURE _db_update.Column_FK_Get
-------------------------------------------------------
(
 -- @Table_SCHEMA varchar(100)='dbo', 

  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(50),
  
  @sql_create VARCHAR(MAX) output, 
  @sql_drop   VARCHAR(MAX) output
  
)
as
BEGIN


    if @TABLE_NAME is null 
    BEGIN
     set @TABLE_NAME='dbo.Passive_Component'
     set @COLUMN_NAME ='channels'

     set @TABLE_NAME='dbo.link'
     set @COLUMN_NAME ='id'


    --// set @TABLE_NAME='LinkFreqPlan'
    --// set @COLUMN_NAME ='folder_id'
      
    END 



/*
  select  
       @sql_drop   = @sql_drop + sql_drop,
       @sql_create = @sql_create + sql_create  
  
  
    from service.view_FK
    where  schema_name = @Table_SCHEMA and 
           TABLE_NAME = @TABLE_NAME and
           
           IndexColumnsNames like '%[[]'+ @COLUMN_NAME  +']%'
*/

/*

  select  
       @sql_drop   = @sql_drop + sql_drop,
       @sql_create = @sql_create + sql_create  
  
  
    from service.view_Index  
    where  schema_name = @Table_SCHEMA and 
           TABLE_NAME = @TABLE_NAME and
           
           IndexColumnsNames like '%['+ @COLUMN_NAME  +']%'

print @sql_drop
*/


/*
  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME ='lib_Radiation_class'
    set @COLUMN_NAME='name'  
  END
*/ 
  
  /*
  DECLARE
    @sql  VARCHAR(8000), 
    @sql_drop VARCHAR(8000); 
    
  */
--service.view_FK

/*
  select 
      object_name(constid) NAME, 
      
      object_name(fkeyid)  	FK_Table_name, 
      c1.name 			  		 	FK_Column_name,
      
      object_name(rkeyid)  	PK_Table_name, 
      c2.name 			   			PK_Column_name,
      
       ObjectProperty(constid, 'CnstIsUpdateCascade') as IsUpdateCascade,   
       ObjectProperty(constid, 'CnstIsDeleteCascade') as IsDeleteCascade  
   
  INTO #temp      
      
   from sysforeignkeys s
     inner join syscolumns c1 on ( s.fkeyid = c1.id and s.fkey = c1.colid )
     inner join syscolumns c2 on ( s.rkeyid = c2.id and s.rkey = c2.colid )
     
  WHERE 
    (object_name(fkeyid)=@TABLE_NAME and c1.name=@COLUMN_NAME)
    or
    (object_name(rkeyid)=@TABLE_NAME and c2.name=@COLUMN_NAME) 
    */
     

--select * from  service.view_FK
    
     
/*
   schema_name (f.schema_id) +'.'+ OBJECT_NAME(f.parent_object_id) AS Table_name,


  -- OBJECT_NAME(f.parent_object_id) AS Table_name,

   
   COL_NAME(fc.parent_object_id, fc.parent_column_id) AS 'column_name',
   
--   schema_name (t.schema_id) as References_schema_name,   
   schema_name (t.schema_id) +'.'+ OBJECT_NAME(t.object_id) as 'References_Table_name',

*/     



  set @sql_create=''
  set @sql_drop=''
     
  SELECT 
     --*
  
    @sql_create=@sql_create + sql_create,
    
/*    
          'ALTER TABLE '+    FK_Table_name + ' ADD CONSTRAINT ' +  NAME
           + ' FOREIGN KEY (' + FK_Column_name + ') REFERENCES '+ PK_Table_name
           + ' (' + PK_Column_name + ') ' + 
           
           CASE  
              when IsUpdateCascade=1 then  'ON UPDATE CASCADE '
           END +

           CASE 
              when IsDeleteCascade=1 then  'ON DELETE CASCADE '
           END             
           
           + ';'+ CHAR(10)
*/  

    @sql_drop=@sql_drop + sql_drop
    
--          'ALTER TABLE '+   FK_Table_name + ' DROP CONSTRAINT ' +  NAME + ';'+ CHAR(10)

  from _db_update.view_FK
  where 
   ( Table_name = @TABLE_NAME and column_name = @COLUMN_NAME )
    or
   ( References_Table_name = @TABLE_NAME and References_column_name = @COLUMN_NAME )

 -- FROM #temp  


 /*
  
  ALTER TABLE [dbo].[AntennaType]
ADD CONSTRAINT [FK_AntennaType_Folder] FOREIGN KEY ([folder_id]) 
  REFERENCES [dbo].[Folder] ([id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO*/


print @sql_create
print @sql_drop
 
END
GO 

if object_id('_db_update.Column_Index_Get') is not null    drop procedure _db_update.Column_Index_Get 
GO 
CREATE PROCEDURE _db_update.Column_Index_Get
------------------------------------------------------------------
(
--  @Table_SCHEMA varchar(100)='dbo', 

  @TABLE_NAME VARCHAR(100),
  @COLUMN_NAME VARCHAR(100),
  
  @sql_create VARCHAR(MAX) output, 
  @sql_drop   VARCHAR(MAX) output
    
)

AS
BEGIN
  

  set @sql_create=''
  set @sql_drop  =''
 

--  @sql_drop  =@sql_drop 


  select  
       @sql_drop   = @sql_drop + sql_drop,
       @sql_create = @sql_create + sql_create  
  
  
    from _db_update.view_Index  
    where  
--      schema_name = @Table_SCHEMA and 
      TABLE_NAME_full = @TABLE_NAME and
                 
      IndexColumnsNames like '%[[]'+ @COLUMN_NAME  +']%'

   
   
end
GO 

if object_id('_db_update.Column_Drop') is not null    drop procedure _db_update.Column_Drop 
GO 
CREATE PROCEDURE _db_update.Column_Drop
------------------------------------------------------------------
(
--  @Table_SCHEMA varchar(100)='dbo', 
 
  @TABLE_NAME  VARCHAR(100),
  @COLUMN_NAME VARCHAR(100)
)
AS
BEGIN
  DECLARE 
    @sql varchar(max);

  DECLARE
    @FK_sql_create VARCHAR(max), 
    @FK_sql_drop   VARCHAR(max),  


    @DF_sql_create VARCHAR(max), 
    @DF_sql_drop   VARCHAR(max),  

    @Index_sql_create VARCHAR(max), 
    @Index_sql_drop   VARCHAR(max)  



--exec service._ColumnDrop  'dbo.Link', 'pmp_sector_id' 

--exec service._ColumnDrop  'dbo.Link', 'pmp_sector_id' 
--exec service._ColumnDrop  'dbo.Link', 'pmp_terminal_id' 


if @TABLE_NAME is null 
BEGIN

 set @TABLE_NAME='dbo.link'
 set @COLUMN_NAME ='property2_id'
  
END 


  if _db_update.Column_Exists( @TABLE_NAME, @COLUMN_NAME)=0
  BEGIN
    print 'COLUMN_NAME not EXISTS'
    RETURN -1    
  END  


--service._Column_DF_Get


--table_name_full
    
  ------------------------
  -- _Drop_Check_Constraints_for_Column
  ------------------------
  set @sql=''
 
  select  
      @sql=sql_drop
    from _db_update.view_check_constraint
    where
        table_name_full = @TABLE_NAME
        and 
        definition like '%[[]'+ @COLUMN_NAME  +']%'

/*
 SELECT 
  
   @sql=@sql + 'ALTER TABLE '+  @TABLE_NAME +' DROP CONSTRAINT  '+ c.CONSTRAINT_NAME +';'
    
 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS c
  left join INFORMATION_SCHEMA.CHECK_CONSTRAINTS cc
    ON cc.CONSTRAINT_NAME = c.CONSTRAINT_NAME
    
 WHERE c.CONSTRAINT_TYPE='CHECK'
      and c.TABLE_NAME  = @TABLE_NAME 
      and cc.check_clause like '%[[]'+ @COLUMN_NAME  +']%'
*/
--service.view_check_constraint

  print @sql    
  
--  return
  
  exec ( @sql )


-------------------------------------------------


  exec _db_update.Column_FK_Get     @TABLE_NAME, @COLUMN_NAME, @FK_sql_create output, @FK_sql_drop output
  
  exec _db_update.Column_DF_Get     @TABLE_NAME, @COLUMN_NAME, @DF_sql_create output, @DF_sql_drop output
  exec _db_update.Column_Index_Get  @TABLE_NAME, @COLUMN_NAME, @Index_sql_create output, @Index_sql_drop output


--  return
  

print '@FK_sql_drop - ' + @FK_sql_drop
print '@DF_sql_drop - ' + @DF_sql_drop
print '@Index_sql_drop - ' + @Index_sql_drop

 
  if @FK_sql_drop   <>'' exec (@FK_sql_drop)
  if @Index_sql_drop<>'' exec (@Index_sql_drop)
  if @DF_sql_drop   <>'' exec (@DF_sql_drop)


   
 set @sql= 'ALTER TABLE '+  @TABLE_NAME +' DROP COLUMN ['+ @COLUMN_NAME + ']'
 print @sql    
   
   
 exec (@sql)
        
end
GO 

if object_id('_db_update.Column_AddDefault') is not null    drop procedure _db_update.Column_AddDefault 
GO 
CREATE PROCEDURE _db_update.Column_AddDefault
------------------------------------------------------------------
(
 --@Table_SCHEMA varchar(100)='dbo', 

  @TABLE_NAME  VARCHAR(100),
  @COLUMN_NAME VARCHAR(100),
--  @CONSTRAINT_NAME VARCHAR(50),
  @DEFAULT VARCHAR(100)=null 
)
AS
BEGIN


if @TABLE_NAME is null 
BEGIN
 set @TABLE_NAME='link'
 set @COLUMN_NAME ='calc_method'
 set @DEFAULT='(3)'
  
END 

  declare 
    @CONSTRAINT_NAME_NEW varchar(100),
    @Column_default varchar(100),
    @sql varchar(100),
    @s   varchar(1000),
    @null varchar(50) 
   -- @def varchar(50)


  if _db_update.Column_Exists( @TABLE_NAME, @COLUMN_NAME)=0
  BEGIN
    print 'column not exists'
    RETURN -1
  END 
  


  if @DEFAULT=''
     set @DEFAULT=null



  SELECT --@name=name,  
  		@column_default=Column_default
  	FROM 
      _db_update.view_Columns
  	WHERE 
      TABLE_NAME_full = @TABLE_NAME 
      AND 
      COLUMN_NAME = @COLUMN_NAME


  IF @column_default = @DEFAULT
    RETURN 1




--select * from sys.default_constraints 

--  set @CONSTRAINT_NAME_NEW = 'DF_' + @TABLE_NAME + '_' + @COLUMN_NAME    

  set @CONSTRAINT_NAME_NEW = 'DF_' + replace (@TABLE_NAME,'.','_') + '_' + @COLUMN_NAME    



select 
   @sql = sql_drop
   
from
  _db_update.view_Column_DF

WHERE --Table_SCHEMA  = @Table_SCHEMA and
      TABLE_NAME_full = @TABLE_NAME AND 
      COLUMN_NAME = @COLUMN_NAME


/*
IF OBJECT_ID(@CONSTRAINT_NAME_NEW, 'D') IS NOT NULL 
  exec ('ALTER TABLE '+ @Table_SCHEMA +'.'+ @TABLE_NAME +' DROP CONSTRAINT '+ @CONSTRAINT_NAME_NEW)




   set @sql=''

   SELECT 
     @sql = 'ALTER TABLE '+ @Table_SCHEMA +'.'+@TABLE_NAME +' DROP CONSTRAINT '+ o.name 
   
   FROM sysobjects o INNER JOIN                       
        syscolumns c ON o.id = c.cdefault INNER JOIN  
        syscomments com ON o.id = com.id              
   WHERE (o.xtype = 'D') and (OBJECT_NAME(o.parent_obj)=@TABLE_NAME) and (c.name=@COLUMN_NAME)
	
   */
   
   print @sql
   exec (@sql)

    
    if @DEFAULT is not null
    BEGIN
    
      set @s ='ALTER TABLE '+ @TABLE_NAME +' ADD CONSTRAINT '+ @CONSTRAINT_NAME_NEW + 
              '  DEFAULT ' + @DEFAULT  +' FOR '+ @COLUMN_NAME
      print @s

      exec (@s)      
    END
    

end
GO 

if object_id('_db_update._UpdateAllViews') is not null    drop procedure _db_update._UpdateAllViews 
GO 
CREATE PROCEDURE _db_update._UpdateAllViews
------------------------------------------------------------------

AS
BEGIN

  
  DECLARE 
    @name varchar(200),
    @sql nvarchar(100);
  
  DECLARE 
    @temp table (name sysname);
     
    /*
  SELECT     
    TABLE_SCHEMA,
    TABLE_name,
  --  View_Definition,
    *
    
  FROM INFORMATION_SCHEMA.views    */



--TABLE_name
--TABLE_SCHEMA    
--View_Definition
 -- return



INSERT INTO @temp
	select TABLE_SCHEMA+'.'+ TABLE_name 	  
     FROM INFORMATION_SCHEMA.views    
     where TABLE_name not like '%11'
     
   --  where table_schema='security'
     
--	from sysobjects WHERE (type = 'V') 
/*
	select *
     FROM INFORMATION_SCHEMA.views    
     where table_schema='security'
     

*/




/*
INSERT INTO @temp
	select name  	  
	from sysobjects WHERE (type = 'V') 

*/

  WHILE exists(SELECT * FROM @temp)
  BEGIN
    SELECT top 1 @name=name FROM @temp
    delete from @temp where name=@name
  
    print @name


    exec sp_refreshview @name


--return

  END
 

END
GO 

if object_id('_db_update.sp_Script') is not null    drop procedure _db_update.sp_Script 
GO 
CREATE PROCEDURE _db_update.sp_Script
AS
BEGIN

declare
	@s varchar(max) = ''
    

set @s =  'if schema_id(''_db_update'') is null  execute(''create schema _db_update'')'  + char(13)+char(10)+  
         'GO ' + char(13)+char(10)  


select @s=@s +  sql_create + char(13)+char(10) --  +   'GO ' + char(13)+char(10)  
 from _db_update.view_View    


select @s=@s +  sql_create  +char(13)+char(10)  -- +   'GO ' +char(13)+char(10)  
 from _db_update.view_Proc


--        definition like '%[[]'+ @COLUMN_NAME  +']%'


set @s = @s + 'exec _db_update.sp_Main'  + char(13)+char(10)  
    --     'GO ' + char(13)+char(10)  


--set @s=@s +  'EXEC [_service].[sp_Main] '  +char(13)+char(10)+   'GO ' +char(13)+char(10)  
 


select @s


  /* Procedure body */
END
GO 

if object_id('_db_update.sp_Main') is not null    drop procedure _db_update.sp_Main 
GO 
CREATE PROCEDURE _db_update.sp_Main
AS
BEGIN
  delete from _object_fields
    where object_id not in (select id from _objects)  


IF OBJECT_ID(N'dbo.PMP_site_tri_Constraint') IS NOT NULL
  DROP TRIGGER  dbo.PMP_site_tri_Constraint


IF OBJECT_ID(N'dbo.Link_Repeater_tri_Project_ID') IS NOT NULL
  DROP TRIGGER  dbo.Link_Repeater_tri_Project_ID
 
IF OBJECT_ID(N'dbo.Link_tri_Constraint') IS NOT NULL
  DROP TRIGGER  dbo.Link_tri_Constraint

IF OBJECT_ID(N'dbo.LinkEnd_tri_Constraint_Property_Project_ID') IS NOT NULL
  DROP TRIGGER  dbo.LinkEnd_tri_Constraint_Property_Project_ID






IF OBJECT_ID(N'[dbo].[view_PMP_CalcRegion_PMP_Sites]') IS NOT NULL
  DROP VIEW [dbo].[view_PMP_CalcRegion_PMP_Sites]
  

IF OBJECT_ID(N'[dbo].[view_Projects_summary]') IS NOT NULL
  DROP VIEW [dbo].[view_Projects_summary]

IF OBJECT_ID(N'[dbo].[view_PMP_site]') IS NOT NULL
  DROP VIEW dbo.view_PMP_site


--Modulation_type
--POWER_LOSS
--LinkFreqPlan_Neighbors
------------------------------------------------------------
--- LinkCalcResults
------------------------------------------------------------
--if  COL_LENGTH('dbo.LinkCalcResults','link_id')  is not null
--  exec _db_update.Column_Drop 'dbo.LinkCalcResults','link_id'

--  ALTER TABLE dbo.LinkCalcResults  ADD link_id int

exec _db_update.Drop_table  'dbo.LinkCalcResults'

------------------------------------------------------------
--- PMP_CalcRegionCells
------------------------------------------------------------


if  COL_LENGTH('dbo.link_repeater_antenna','repeater_gain')  is null
  ALTER TABLE dbo.link_repeater_antenna  ADD repeater_gain float



if  COL_LENGTH('dbo.PMP_CalcRegionCells','pmp_sector_id')  is not null
  exec _db_update.Column_Drop 'dbo.PMP_CalcRegionCells','pmp_sector_id'

  ALTER TABLE dbo.PMP_CalcRegionCells  ADD pmp_sector_id int


if  COL_LENGTH('dbo.Pmp_CalcRegion','terminal_sensitivity')  is null
  ALTER TABLE dbo.Pmp_CalcRegion  ADD terminal_sensitivity float


if  COL_LENGTH('dbo.Pmp_CalcRegion','terminal_gain')  is null
  ALTER TABLE dbo.Pmp_CalcRegion  ADD terminal_gain float

if  COL_LENGTH('dbo.Pmp_CalcRegion','terminal_loss')  is null
  ALTER TABLE dbo.Pmp_CalcRegion  ADD terminal_loss float

/*
update _object_fields set name='terminal_gain' where name='terminal_gain_db' and Object_Name is null
update _object_fields set name='terminal_loss' where name='terminal_loss_db'  and Object_Name is null
update _object_fields set name='terminal_sensitivity' where name='terminal_sensitivity_dbm'  and Object_Name is null
*/




--if  COL_LENGTH('dbo.Pmp_CalcRegion','terminal_loss')  is not null
--  exec _db_update.Column_Drop 'dbo.Pmp_CalcRegion','terminal_loss'


-------------------------------------
if  COL_LENGTH('dbo.LinkFreqPlan_Neighbors','tx_link_id')  is not null
  exec _db_update.Column_Drop 'dbo.LinkFreqPlan_Neighbors','tx_link_id'


if  COL_LENGTH('dbo.Template_LinkEnd','POWER_LOSS')  is null
  ALTER TABLE dbo.Template_LinkEnd  ADD POWER_LOSS float

--_UpdateAllViews

if  COL_LENGTH('dbo.Link_reflection_points','radius')  is not null
  exec _db_update.Column_Drop 'dbo.Link_reflection_points','radius'

if  COL_LENGTH('dbo.Link_reflection_points','distance')  is not null
  exec _db_update.Column_Drop 'dbo.Link_reflection_points','distance'

if  COL_LENGTH('dbo.Link_reflection_points','length')  is not null
  exec _db_update.Column_Drop 'dbo.Link_reflection_points','length'



if  COL_LENGTH('dbo.Template_LinkEnd','Modulation_type')  is not null
  exec _db_update.Column_Drop 'dbo.Template_LinkEnd','Modulation_type'



if  COL_LENGTH('dbo.Template_LinkEnd','Modulation_type')  is null
  ALTER TABLE dbo.Template_LinkEnd  ADD Modulation_type varchar(30)

--_UpdateAllViews


if  COL_LENGTH('dbo.link','AM_data_bin')  is null
  ALTER TABLE dbo.link  ADD AM_data_bin image

if  COL_LENGTH('dbo.link','AM_data_xml')  is null
  ALTER TABLE dbo.link  ADD AM_data_xml XML


if  COL_LENGTH('dbo.relief','geom')  is null
  ALTER TABLE dbo.relief  ADD geom geometry NULL

if  COL_LENGTH('dbo.LinkFreqPlan_Neighbors','LINKEND1_ID')  is null
  ALTER TABLE dbo.LinkFreqPlan_Neighbors  ADD LINKEND1_ID int

if  COL_LENGTH('dbo.LinkFreqPlan_Neighbors','LINKEND2_ID')  is null
  ALTER TABLE dbo.LinkFreqPlan_Neighbors  ADD LINKEND2_ID int

if  COL_LENGTH('dbo.LinkFreqPlan_Neighbors','rx_for_linkend1_id')  is null
  ALTER TABLE dbo.LinkFreqPlan_Neighbors  ADD rx_for_linkend1_id int

if  COL_LENGTH('dbo.Linkend_antenna ','mast_index')  is null
  ALTER TABLE dbo.Linkend_antenna  ADD mast_index int


if  COL_LENGTH('dbo.LinkFreqPlan_Neighbors','tx_linkend1_name')  is null
  ALTER TABLE dbo.LinkFreqPlan_Neighbors  ADD tx_linkend1_name varchar(50) 

if  COL_LENGTH('dbo.LinkFreqPlan_Neighbors','tx_linkend2_name')  is null
  ALTER TABLE dbo.LinkFreqPlan_Neighbors  ADD tx_linkend2_name varchar(50) 

if  COL_LENGTH('dbo.LinkFreqPlan_Neighbors','tx_link_id')  is not null
  exec _db_update.Column_Drop 'dbo.LinkFreqPlan_Neighbors','tx_link_id'






IF OBJECT_ID(N'dbo.view_PMP_Terminal_Antennas') IS NOT NULL
   DROP view dbo.view_PMP_Terminal_Antennas


exec _db_update.Drop_FK 'FK_Map_XREF_Map'
exec _db_update.Drop_FK 'FK_Link_reflection_points_Link_id'


IF OBJECT_ID(N'dbo.sp_MapMaker_Data') IS not  NULL
  DROP PROCEDURE dbo.sp_MapMaker_Data

IF OBJECT_ID(N'dbo.sp_MapMaker') IS not  NULL
  DROP PROCEDURE dbo.sp_MapMaker



if  COL_LENGTH('dbo.Template_Linkend_Antenna','loss')  is null
  ALTER TABLE dbo.Template_Linkend_Antenna  ADD loss float

if  COL_LENGTH('dbo.Linkend','color')  is null
  ALTER TABLE dbo.Linkend  ADD  color int DEFAULT round(rand()*(1000000),(0)) NULL
 



--vMsg 547, Level 16, State 0, Line 12166
--The ALTER TABLE statement conflicted with the FOREIGN KEY constraint "FK_Link_reflection_points_Link_id". The conflict occurred in database "1_tele2_nn_MSK", table "dbo.Link", column 'id'.

-------------------------------------------------
-- view
-------------------------------------------------
IF OBJECT_ID(N'dbo.view_PMP_link_summary') IS NOT NULL
   DROP view dbo.view_PMP_link_summary



IF OBJECT_ID(N'dbo.view_PMP_TERMINAL_full') IS NOT NULL
   DROP view dbo.view_PMP_TERMINAL_full

IF OBJECT_ID(N'dbo.view_SH_PMPCALCREGION_SITE') IS NOT NULL
   DROP view dbo.view_SH_PMPCALCREGION_SITE


IF OBJECT_ID(N'dbo.view_Project_summary_') IS NOT NULL
   DROP view dbo.view_Project_summary_

IF OBJECT_ID(N'dbo.view_SH_Pmp_Site') IS NOT NULL
   DROP view dbo.view_SH_Pmp_Site

IF OBJECT_ID(N'dbo.vExportToMDB_Physical_param') IS NOT NULL
   DROP view dbo.vExportToMDB_Physical_param


IF OBJECT_ID(N'dbo.view_LinkFreqPlan_LinkEnd_for_calc_NB') IS NOT NULL
   DROP view dbo.view_LinkFreqPlan_LinkEnd_for_calc_NB


IF OBJECT_ID(N'dbo.view_LinkFreqPlan_LinkEnd_for_calc') IS NOT NULL
   DROP view dbo.view_LinkFreqPlan_LinkEnd_for_calc


IF OBJECT_ID(N'dbo.view_Link_Repeater_Used_In') IS NOT NULL
   DROP view dbo.view_Link_Repeater_Used_In


IF OBJECT_ID(N'dbo.view_PMP_sectors') IS NOT NULL
   DROP view dbo.view_PMP_sectors


--IF OBJECT_ID(N'dbo.view_PMP_sector') IS NOT NULL
--   DROP view dbo.view_PMP_sector


IF OBJECT_ID(N'dbo.view_PMP_Sector_Antennas') IS NOT NULL
   DROP view dbo.view_PMP_Sector_Antennas


IF OBJECT_ID(N'dbo.view_PMP_Sector_base') IS NOT NULL
   DROP view dbo.view_PMP_Sector_base


IF OBJECT_ID(N'dbo.view_PMP_sector_for_Calc') IS NOT NULL
   DROP view dbo.view_PMP_sector_for_Calc



IF OBJECT_ID(N'dbo.view_PMP_terminal_base') IS NOT NULL
   DROP view dbo.view_PMP_terminal_base



IF OBJECT_ID(N'dbo.view_PMP_terminal_for_Calc') IS NOT NULL
   DROP view dbo.view_PMP_terminal_for_Calc

 


IF OBJECT_ID(N'dbo.view_PMP_sector_for_Calc') IS NOT NULL
   DROP view dbo.view_PMP_sector_for_Calc



IF OBJECT_ID(N'dbo.view_PMP_terminal_for_Calc') IS NOT NULL
   DROP view dbo.view_PMP_terminal_for_Calc


IF OBJECT_ID(N'dbo.view_PMP_sector_with_project_id') IS NOT NULL
   DROP view dbo.view_PMP_sector_with_project_id


IF OBJECT_ID(N'dbo.view_PMP_sectors_new') IS NOT NULL
   DROP view dbo.view_PMP_sectors_new


IF OBJECT_ID(N'dbo.view_PMP_TERMINAL_with_project_id') IS NOT NULL
   DROP view dbo.view_PMP_TERMINAL_with_project_id

  
-------------------------------------------------
-- view
-------------------------------------------------

----------------------------------------------------------------------

if  COL_LENGTH('dbo.LinkFreqPlan','PolarizationLevel')  is null
  ALTER TABLE dbo.LinkFreqPlan  ADD PolarizationLevel float DEFAULT -30 NULL



----------------------------------------------------------------------

--if  COL_LENGTH('dbo.Link','AM_data_xml')  is null
--  ALTER TABLE dbo.Link
--    ADD AM_data_xml xml NULL
--GO

if  COL_LENGTH('dbo.Linkend_antenna','pmp_terminal_id')  is not null
  exec _db_update.Column_Drop 'dbo.Linkend_antenna','pmp_terminal_id'

if  COL_LENGTH('dbo.Linkend_antenna','pmp_sector_id')  is not null
  exec _db_update.Column_Drop 'dbo.Linkend_antenna','pmp_sector_id'


if  COL_LENGTH('dbo.Link','pmp_terminal_id')  is not null
  exec _db_update.Column_Drop 'dbo.Link','pmp_terminal_id'

if  COL_LENGTH('dbo.Link','pmp_sector_id')  is not null
  exec _db_update.Column_Drop 'dbo.Link','pmp_sector_id'

if  COL_LENGTH('dbo.Link_repeater_antenna','Display_order')  is not null
  exec _db_update.Column_Drop 'dbo.Link_repeater_antenna','Display_order'


if  COL_LENGTH('dbo.Link_Repeater_Antenna','project_id')  is not null
  exec _db_update.Column_Drop 'dbo.Link_Repeater_Antenna','project_id'


if  COL_LENGTH('dbo.LinkEnd','pmp_sector_id')  is not null
  exec _db_update.Column_Drop 'dbo.LinkEnd','pmp_sector_id'


if  COL_LENGTH('dbo.LinkEnd','project_id')  is not null
  exec _db_update.Column_Drop 'dbo.LinkEnd','project_id'

--  ALTER TABLE dbo.LinkEnd
--   drop  COLUMN project_id
--GO



if  COL_LENGTH('dbo.LinkEnd','Connected_PMP_Sector_ID')  is null
  ALTER TABLE dbo.Linkend
    ADD Connected_PMP_Sector_ID int


if  COL_LENGTH('dbo.LinkEnd','Connected_PMP_Site_ID')  is null
  ALTER TABLE dbo.Linkend
    ADD Connected_PMP_Site_ID int



if  COL_LENGTH('dbo.Link','AM_data_bin')  is null
  ALTER TABLE dbo.Link
    ADD AM_data_bin image NULL



if  COL_LENGTH('dbo.Link','refraction_2')  is null
  ALTER TABLE dbo.Link
    ADD refraction_2 float NULL


if  COL_LENGTH('dbo.Link','status_back')  is null
  ALTER TABLE dbo.Link  ADD status_back int


if  COL_LENGTH('dbo.Link','Link_Repeater_id')  is not null
  exec _db_update.Column_Drop 'dbo.Link','Link_Repeater_id'

--  ALTER TABLE dbo.Link
--   drop  COLUMN Link_Repeater_id

---------------------------------------------------
--PMP_site
---------------------------------------------------
if  COL_LENGTH('dbo.PMP_site','project_id')  is not null
 exec _db_update.Column_Drop 'dbo.PMP_site','project_id'




---------------------------------------------------
--LinkEnd_antenna
---------------------------------------------------
if  COL_LENGTH('dbo.LinkEnd_antenna','loss')  is null
  ALTER TABLE dbo.LinkEnd_antenna  ADD loss float


if  COL_LENGTH('dbo.LinkEnd_antenna','project_id')  is not null
 exec _db_update.Column_Drop 'dbo.LinkEnd_antenna','project_id'
--  ALTER TABLE dbo.LinkEnd_antenna
--   drop  COLUMN project_id
--GO

if  COL_LENGTH('dbo.Template_Linkend_Antenna','loss')  is null
  ALTER TABLE dbo.Template_Linkend_Antenna 
    ADD loss float 



---------------------------------------------------
--Template_Linkend
---------------------------------------------------

if  COL_LENGTH('dbo.Template_Linkend','bitrate_Mbps')  is null
  ALTER TABLE dbo.Template_Linkend
    ADD bitrate_Mbps float NULL


if  COL_LENGTH('dbo.Template_Linkend','SIGNATURE_HEIGHT')  is null
  ALTER TABLE dbo.Template_Linkend
    ADD SIGNATURE_HEIGHT float NULL


if  COL_LENGTH('dbo.Template_Linkend','SIGNATURE_WIDTH')  is null
  ALTER TABLE dbo.Template_Linkend
    ADD SIGNATURE_WIDTH float NULL


if  COL_LENGTH('dbo.Template_Linkend','POWER_MAX')  is null
  ALTER TABLE dbo.Template_Linkend
    ADD POWER_MAX float NULL


if  COL_LENGTH('dbo.Template_Linkend','channel_width_MHz')  is null
  ALTER TABLE dbo.Template_Linkend
    ADD channel_width_MHz float NULL


if  COL_LENGTH('dbo.Template_Linkend','MODULATION_TYPE')  is null
  ALTER TABLE dbo.Template_Linkend
    ADD MODULATION_TYPE float NULL


if  COL_LENGTH('dbo.LinkFreqPlan','project_id')  is not null
  exec _db_update.Column_Drop 'dbo.LinkFreqPlan','project_id'


if  COL_LENGTH('dbo.LinkFreqPlan_LinkEnd','Link_id')  is not null
  exec _db_update.Column_Drop 'dbo.LinkFreqPlan_LinkEnd','Link_id'
 
--  ALTER TABLE dbo.LinkFreqPlan_LinkEnd
--   drop  COLUMN Link_id  



if  COL_LENGTH('dbo.LinkFreqPlan_LinkEnd','next_linkend_id')  is not null
 exec _db_update.Column_Drop 'dbo.LinkFreqPlan_LinkEnd','next_linkend_id'
--  ALTER TABLE dbo.LinkFreqPlan_LinkEnd
--   drop  COLUMN next_linkend_id  
--GO



----------------------------------------------------------------------


if  COL_LENGTH('dbo._objects','table_name_initial')  is null
  ALTER TABLE dbo._objects
   ADD table_name_initial varchar(50) 


if  COL_LENGTH('dbo._object_fields','is_use_on_map')  is null
  ALTER TABLE dbo._object_fields
    ADD is_use_on_map bit NULL


if  COL_LENGTH('dbo._object_fields','is_user_column')  is null
  ALTER TABLE dbo._object_fields
    ADD is_user_column bit NULL

----------------------------------------------------------------------
-- Link_Repeater
----------------------------------------------------------------------
if  COL_LENGTH('dbo.Link_Repeater_antenna','slot')  is  null
 ALTER TABLE dbo.Link_Repeater_antenna ADD slot int NULL

if  COL_LENGTH('dbo.Link_Repeater','project_id')  is not null
 exec _db_update.Column_Drop 'dbo.Link_Repeater','project_id'

if  COL_LENGTH('dbo.Link_Repeater','is_active')  is null
  ALTER TABLE dbo.Link_Repeater ADD is_active int NULL

if  COL_LENGTH('dbo.Link_Repeater','link_ID')  is null
  ALTER TABLE dbo.Link_Repeater ADD link_ID int NULL

if  COL_LENGTH('dbo.Link_Repeater','calc_results_xml_1')  is null
  ALTER TABLE dbo.Link_Repeater ADD calc_results_xml_1 text

if  COL_LENGTH('dbo.Link_Repeater','calc_results_xml_2')  is null
  ALTER TABLE dbo.Link_Repeater ADD calc_results_xml_2 text

if  COL_LENGTH('dbo.Link_Repeater','calc_results_xml_3')  is null
  ALTER TABLE dbo.Link_Repeater ADD calc_results_xml_3 text


----------------------------------------------------------------------
-- Template_LinkEnd
----------------------------------------------------------------------
if  COL_LENGTH('dbo.Template_LinkEnd','mode')  is null
  ALTER TABLE dbo.Template_LinkEnd ADD mode int NULL



if  COL_LENGTH('dbo.Template_LinkEnd','channel_type')  is null
  ALTER TABLE dbo.Template_LinkEnd ADD channel_type int NULL


if  COL_LENGTH('dbo.Template_LinkEnd','subband')  is null
  ALTER TABLE dbo.Template_LinkEnd ADD subband varchar(50) COLLATE Cyrillic_General_CI_AS NULL


if  COL_LENGTH('dbo.Template_LinkEnd','channel_number')  is null
  ALTER TABLE dbo.Template_LinkEnd ADD channel_number int NULL


if  COL_LENGTH('dbo.Template_LinkEnd','rx_freq_MHz')  is null
  ALTER TABLE dbo.Template_LinkEnd ADD rx_freq_MHz int NULL

--yle xml NULL


if  COL_LENGTH('dbo.Map_XREF','style')  is null
  ALTER TABLE dbo.Map_XREF  ADD style xml NULL


----------------------------------------
if  COL_LENGTH('dbo.Template_LinkEnd','tx_freq_MHz')  is null
  ALTER TABLE dbo.Template_LinkEnd ADD tx_freq_MHz int NULL


----------------------------------------------------------------------
if  COL_LENGTH('dbo.Map_Objects','style')  is null
  ALTER TABLE dbo.Map_Objects  ADD style xml NULL

if  COL_LENGTH('dbo.Map_XREF','style')  is null
  ALTER TABLE dbo.Map_XREF  ADD style xml NULL


if  COL_LENGTH('dbo.Property','name_rus')  is null
  ALTER TABLE dbo.Property
    ADD name_rus varchar(200) COLLATE Cyrillic_General_CI_AS NULL


if  COL_LENGTH('dbo.Property','NRI')  is null
  ALTER TABLE dbo.Property
   ADD NRI varchar(30) COLLATE Cyrillic_General_CI_AS NULL


if  COL_LENGTH('dbo.Property','mast_count')  is null
  ALTER TABLE dbo.Property
    ADD mast_count int



-------------------------------------------------
-- SCHEMA
-------------------------------------------------

IF schema_id ('reports') IS NULL  exec ('CREATE SCHEMA [reports]')
IF schema_id ('link') IS NULL  exec ('CREATE SCHEMA [link]')
IF schema_id ('pmp') IS NULL  exec ('CREATE SCHEMA [pmp]')
IF schema_id ('link_calc') IS NULL  exec ('CREATE SCHEMA [link_calc]')
IF schema_id ('link_climate') IS NULL  exec ('CREATE SCHEMA [link_climate]')
IF schema_id ('LinkFreqPlan') IS NULL  exec ('CREATE SCHEMA [LinkFreqPlan]')
IF schema_id ('geo') IS NULL  exec ('CREATE SCHEMA [geo]')
IF schema_id ('history') IS NULL  exec ('CREATE SCHEMA [history]')
IF schema_id ('ui') IS NULL  exec ('CREATE SCHEMA [ui]')
IF schema_id ('lib') IS NULL  exec ('CREATE SCHEMA [lib]')
IF schema_id ('map') IS NULL  exec ('CREATE SCHEMA [map]')
IF schema_id ('Security') IS NULL  exec ('CREATE SCHEMA [Security]')
IF schema_id ('gis') IS NULL  exec ('CREATE SCHEMA [gis]')


IF OBJECT_ID(N'dbo.link_SDB') IS  NULL

  CREATE TABLE dbo.link_SDB (
    id int IDENTITY(1, 1) NOT NULL,
    name varchar(100) COLLATE Cyrillic_General_CI_AS NULL,
    project_id int NULL,
    guid uniqueidentifier DEFAULT newid() NULL,
    link1_id int NULL,
    link2_id int NULL,
    folder_id int NULL,
    date_created datetime DEFAULT getdate() NULL,
    date_modify datetime NULL,
    user_created varchar(50) COLLATE Cyrillic_General_CI_AS DEFAULT suser_name() NULL,
    user_modify varchar(50) COLLATE Cyrillic_General_CI_AS NULL,
    _ bit NULL,
    kng_required float NULL,
    SESR_required float NULL,
    kng float NULL,
    kng_year AS ((((0.01)*[Kng])*(365))*(24))*(60),
    sesr float NULL,
    status int NULL,
    CONSTRAINT link_SDB_pk PRIMARY KEY CLUSTERED (id)
      WITH (
        PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF,
        ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
    CONSTRAINT link_SDB_uq UNIQUE (project_id, name)
      WITH (
        PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF,
        ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
    CONSTRAINT link_SDB_fk FOREIGN KEY (project_id) 
    REFERENCES dbo.Project (id) 
    ON UPDATE NO ACTION
    ON DELETE CASCADE,
    CONSTRAINT link_SDB_fk2 FOREIGN KEY (link1_id) 
    REFERENCES dbo.Link (id) 
    ON UPDATE NO ACTION
    ON DELETE NO ACTION,
    CONSTRAINT link_SDB_fk3 FOREIGN KEY (link2_id) 
    REFERENCES dbo.Link (id) 
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
  )
  ON [PRIMARY]



/*
if OBJECT_ID('Security.GeoRegion') is not null
begin
--  delete from Security.GeoRegion

 -- if OBJECT_ID('Security.GeoRegion_ck') is not null
  --  ALTER TABLE [Security].[GeoRegion] DROP CONSTRAINT [GeoRegion_ck]

end
*/

 
END
GO 

exec _db_update.sp_Main

