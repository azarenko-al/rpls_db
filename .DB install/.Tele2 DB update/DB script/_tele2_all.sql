if schema_id('_db_update') is null  execute('create schema _db_update')
GO 
if object_id('_db_update.view_FK') is not null    drop view _db_update.view_FK 
GO 
CREATE VIEW _db_update.view_FK 
AS
-- schema_name (o.schema_id) as schema_name,

  SELECT 
   f.name,--   AS 'Name of Foreign Key',
  
/*   delete_referential_action,
   update_referential_action,

   delete_referential_action_desc,
   update_referential_action_desc,
*/   
   
   schema_name (f.schema_id) as schema_name,   
   
   schema_name (f.schema_id) +'.'+ OBJECT_NAME(f.parent_object_id) AS Table_name,


  -- OBJECT_NAME(f.parent_object_id) AS Table_name,

   
   COL_NAME(fc.parent_object_id, fc.parent_column_id) AS 'column_name',
   
--   schema_name (t.schema_id) as References_schema_name,   
   schema_name (t.schema_id) +'.'+ OBJECT_NAME(t.object_id) as 'References_Table_name',

--   schema_name (t.schema_id) +'.'+ OBJECT_NAME(t.object_id) as References_schema_name_full,   


   
   COL_NAME(t.object_id, fc.referenced_column_id) AS 'References_column_name',

--------------------------------------

 'if object_id('''+f.name+''') is not null  '+  CHAR(10) +

replace (replace (
   'ALTER TABLE :table  DROP CONSTRAINT :name',
':table', schema_name (t.schema_id) +'.'+ OBJECT_NAME(f.parent_object_id) ),
':name', f.name ) 

 + ';'+ CHAR(10)
 
as sql_Drop,




--------------------------------------
 --  'ALTER TABLE [' + OBJECT_NAME(f.parent_object_id) + ']  DROP CONSTRAINT [' + f.name + ']' AS 'Delete foreign key',

--------------------------------------
--replace (replace (
--   'ALTER TABLE [:table]  WITH NOCHECK ADD CONSTRAINT [:name] FOREIGN KEY([:foreign_key]) REFERENCES [ + OBJECT_NAME(t.object_id) + '] ([' +
--        COL_NAME(t.object_id,fc.referenced_column_id) + '])' AS 'Create foreign key',


--WITH NOCHECK 

   'ALTER TABLE ' + schema_name (f.schema_id) +'.'+ OBJECT_NAME(f.parent_object_id) + '  ADD CONSTRAINT ' + 
        f.name + ' FOREIGN KEY([' + COL_NAME(fc.parent_object_id,fc.parent_column_id) + ']) REFERENCES ' + 
         schema_name (t.schema_id) +'.'+ OBJECT_NAME(t.object_id) + ' ([' +
        COL_NAME(t.object_id,fc.referenced_column_id) + '])' 
        + IIF(delete_referential_action_desc = 'cascade', ' on delete cascade', '')
        + IIF(update_referential_action_desc = 'cascade', ' on update cascade', '')
        
 + ';'+ CHAR(10)        
        
        AS sql_create

/*
      if FK.IsUpdateCascade1 then sON:= sON+' ON UPDATE CASCADE';
      if FK.IsDeleteCascade  then sON:= sON+' ON DELETE CASCADE';
*/
        
        
    -- , delete_referential_action_desc AS 'UsesCascadeDelete'
FROM sys.foreign_keys AS f,
     sys.foreign_key_columns AS fc,
     sys.tables t 
     
WHERE
  f.OBJECT_ID = fc.constraint_object_id
AND t.OBJECT_ID = fc.referenced_object_id


and schema_name (f.schema_id)  not in ('security','service')
--and schema_name (f.schema_id)<>'service'



--AND OBJECT_NAME(t.object_id) = 'Employees'      --  Just show the FKs which reference a particular table
GO 

if object_id('_db_update.view_Index') is not null    drop view _db_update.view_Index 
GO 
CREATE VIEW _db_update.view_Index  
AS


select *,


schema_name+'.'+Table_Name as Table_Name_full,



replace (replace (replace (replace (

'IF IndexProperty(Object_Id('':TABLE''), '':name'', ''IndexId'') Is Null '+  char(10) +


case 

--ADD PRIMARY KEY CLUSTERED

  when type_desc='SPATIAL'	  then 'CREATE SPATIAL INDEX [:name] ON :TABLE (:columns) USING GEOMETRY_AUTO_GRID ' +
                                    ' WITH ( BOUNDING_BOX = ( XMIN = 0, YMIN = 0, XMAX = 180, YMAX = 90 ) )'
  
  when is_primary_key=1 	  then 'ALTER TABLE :TABLE ADD PRIMARY KEY :CLUSTERED (:columns)  ON [PRIMARY] '  
  when is_unique_constraint=1 then 'ALTER TABLE :TABLE ADD CONSTRAINT :name UNIQUE :CLUSTERED (:columns)  ON [PRIMARY] ' 
  when is_unique=1 			  then 'CREATE UNIQUE INDEX :name ON :TABLE (:columns) ON [PRIMARY]  '  
  
                              else 'CREATE INDEX :name ON :TABLE (:columns)  ON [PRIMARY] '
end ,
 ':TABLE', schema_name+'.'+Table_Name),
 ':name', Index_Name),
 ':columns', IndexColumnsNames),
 ':CLUSTERED', type_desc)  

 + char(10) 
 
as [sql_create],

/*
CREATE INDEX IX_IndexName
ON dbo.TableName
WITH (DROP_EXISTING = ON);

*/
-------------------------



replace (replace (replace (

'IF IndexProperty(Object_Id('':TABLE''), '':name'', ''IndexId'') Is not Null '+  char(10) +

case 
  when is_primary_key=1 or is_unique_constraint=1
                           	  then ' ALTER TABLE :TABLE DROP CONSTRAINT :name' 
   
--  when is_unique=1 			  then ' ALTER TABLE :TABLE DROP CONSTRAINT :name' 
--  when is_unique_constraint=1 then ' ALTER TABLE :TABLE DROP CONSTRAINT :name' 
--  when is_unique=1 			  then 'DROP INDEX :name ON :TABLE' 
                              else ' DROP INDEX  :name ON :TABLE '
end ,
 ':TABLE', schema_name+'.'+Table_Name),
 ':name', Index_Name),
 ':columns', IndexColumnsNames)  
 
  + char(10) as [sql_drop],
  
  
-----------------------------------------------------------------------

schema_name+'.'+Table_Name +','+ -- char(10)  +

IIF(is_primary_key=1, 'is_primary_key,','') +
IIF(is_unique=1, 'is_unique,', '') +
IIF(is_unique_constraint=1, 'is_unique_constraint,','')  + --char(10) +
 
IndexColumnsNames as [Definition]
  
 

from 

(


  SELECT 
    create_date,
    modify_date,

case
  when INX.type_desc='CLUSTERED' then 'CLUSTERED'
  when INX.type_desc='NONCLUSTERED' then 'NONCLUSTERED'
  when INX.type_desc='SPATIAL' then 'SPATIAL'

end as  type_desc,
  
      INX.object_id,
      INDEX_id,

      INX.[name] AS [Index_Name],
      TBL.[name] AS [Table_Name],
      schema_name(TBL.schema_id) as schema_name,

      INX.is_primary_key,
      INX.is_unique,
      INX.is_unique_constraint,
      
      DS1.[IndexColumnsNames]
   --   DS2.[IncludedColumnsNames]
      
      
      
FROM [sys].[indexes] INX
INNER JOIN [sys].[tables] TBL
    ON INX.[object_id] = TBL.[object_id]
    
    
CROSS APPLY 
(
    SELECT STUFF
    (
        (
            SELECT ',[' + CLS.[name] + ']'
            FROM [sys].[index_columns] INXCLS
            INNER JOIN [sys].[columns] CLS 
                ON INXCLS.[object_id] = CLS.[object_id] 
                AND INXCLS.[column_id] = CLS.[column_id]
            WHERE INX.[object_id] = INXCLS.[object_id] 
                AND INX.[index_id] = INXCLS.[index_id]
                AND INXCLS.[is_included_column] = 0
            FOR XML PATH('')
        )
        ,1
        ,1
        ,''
    ) 
) DS1 ([IndexColumnsNames])

    where INX.[name] is not null
    
) M

where 
  Table_Name not like '%[___]'  and

  Table_Name <> 'msfavorites'  and
  Table_Name not like '%111'  
  
  and schema_name not in ('test','service')



--and Table_Name ='Climate'
GO 

if object_id('_db_update.view_check_constraint') is not null    drop view _db_update.view_check_constraint 
GO 
CREATE VIEW _db_update.view_check_constraint 
AS

select * ,

 /*
REPLACE(replace(replace(
'ALTER TABLE :table WITH NOCHECK ADD CONSTRAINT [:name] CHECK :definition',
':table', schema_name + '.'+ table_name ),   
':definition', [definition] ),
':name', [constraint_name] )

select * from sys.default_constraints 

as sql_create,   
-------------------------------
*/

schema_name + '.'+  table_name as table_name_full,

REPLACE(replace(
'ALTER TABLE :table DROP CONSTRAINT [:name]',
':table', schema_name + '.'+  table_name ),   
':name', [constraint_name] )

as sql_drop   

-----------------------------

from

(

  select con.[name] as constraint_name,
    schema_name(t.schema_id) as schema_name, 
    
   

    
/*      SQL_ALTER_TABLE_ADD_CHECK_CONSTRAINT_no_proc =
   'ALTER TABLE [%s] WITH NOCHECK ADD CONSTRAINT [%s] CHECK %s';  //+ DEF_GO_CRLF;
*/
    
    
   -- + '.' + 
    t.[name]  as [table_name],
    
--    t.create_date,
--    t.modify_date,
    
    col.[name] as column_name,
    con.[definition]
    
/*    ,
    case when con.is_disabled = 0 
        then 'Active' 
        else 'Disabled' 
        end as [status]
*/
        
from sys.check_constraints con
    left outer join sys.objects t
        on con.parent_object_id = t.object_id
    left outer join sys.all_columns col
        on con.parent_column_id = col.column_id
        and con.parent_object_id = col.object_id
        
) M

/*
SELECT 
  *, 
   object_definition(OBJECT_ID(CONSTRAINT_NAME)) as definition

 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
 where CONSTRAINT_TYPE = 'check'
 
 */
GO 

if object_id('_db_update.view_View') is not null    drop view _db_update.view_View 
GO 
CREATE VIEW _db_update.view_View
AS
  select 
  
  o.NAME,
  
  schema_name (o.schema_id) + '.' + o.NAME as name_full, 
  
 /* o.create_date,
  o.modify_date,
  */
 -- schema_name (o.schema_id) as schema_name,
  
  definition,
  
 replace(
        'if object_id('':name'') is not null    drop view :name ', 
                                  
        ':name', schema_name (o.schema_id) + '.' + o.NAME) 
        
        + char(13)+char(10)+ 'GO ' + char(13)+char(10)+
        definition +  
        'GO ' + char(13)+char(10)
        
        as sql_create       
  
  
from sys.objects     o
  join sys.sql_modules m on m.object_id = o.object_id


where  
     o.type = 'V'
   and 
     schema_name(o.schema_id) = '_db_update'
     
      and o.NAME not like '%[111]'
GO 

if object_id('_db_update.view_Proc') is not null    drop view _db_update.view_Proc 
GO 
CREATE VIEW _db_update.view_Proc
AS
  select 

 -- o.type,  
  
  o.NAME,


  schema_name (o.schema_id) + '.' + o.NAME as name_full,   
  
/*  o.create_date,
  o.modify_date,
*/
    
 -- schema_name (o.schema_id) as schema_name,
  
--  definition,
  
 replace(
        IIF(o.type in ('p'), 'if object_id('':name'') is not null    drop procedure :name ',  
                             'if object_id('':name'') is not null    drop function  :name '), 
                                  
        ':name', schema_name (o.schema_id) + '.' + o.NAME) 
        + char(13)+char(10)+
        'GO ' + + char(13)+char(10)+ 
        definition +  
        'GO ' + char(13)+char(10) 
        
        as sql_create     
  
  
from sys.objects     o
join sys.sql_modules m on m.object_id = o.object_id

where
   o.type  in ('fn','p','tf','IF')
  and 
     schema_name(o.schema_id) = '_db_update'
     
   and o.NAME not like '%[111]'
 --  and o.NAME <> 'sp_Main'
GO 

if object_id('_db_update.view_Column_DF') is not null    drop view _db_update.view_Column_DF 
GO 
CREATE VIEW _db_update.view_Column_DF
AS
  


    select	SCHEMA_NAME(t.schema_id) +'.'+ t.name [table_name_full], 
           c.name [column_name], 
           d.name [constraint],  
           d.definition,

--SCHEMA_NAME(t.schema_id) as table_SCHEMA,

sql_drop   = 'ALTER TABLE '+ SCHEMA_NAME(t.schema_id) +'.'+t.name +' DROP CONSTRAINT '+ d.name ,

--sql_create = 'ALTER TABLE '+ SCHEMA_NAME(t.schema_id) +'.'+t.name +' ADD DEFAULT '' (suser_name()) ''  FOR '+ c.name 

sql_create ='ALTER TABLE '+  SCHEMA_NAME(t.schema_id) +'.'+t.name +' ADD DEFAULT (' +  d.definition  +') FOR '+ c.name 



from	sys.default_constraints d
join	sys.tables t  
  on	t.object_id = d.parent_object_id
join	sys.columns c
  on	c.object_id = t.object_id
  and	c.column_id = d.parent_column_id
--  and d.definition = '(user_name())'


/*
select	
  t.name [table_name], 
  c.name [column_name], 
  d.name [constraint],

  SCHEMA_NAME(t.schema_id) +'.'+ t.name as table_name_full,

  SCHEMA_NAME(t.schema_id) as table_SCHEMA,


  sql_drop = 'ALTER TABLE '+ SCHEMA_NAME(t.schema_id) +'.'+t.name +' DROP CONSTRAINT '+ d.name 


   
  @sql_create ='ALTER TABLE '+  @TABLE_NAME +' ADD CONSTRAINT '+ o.name + '  DEFAULT(' +  com.text  +') FOR '+ @COLUMN_NAME,
                
     @sql_drop= 'ALTER TABLE '+  @TABLE_NAME +' DROP CONSTRAINT '+ o.name




from	sys.default_constraints d
join	sys.tables t  
  on	t.object_id = d.parent_object_id
join	sys.columns c
  on	c.object_id = t.object_id
  and	c.column_id = d.parent_column_id
  
  */
GO 

if object_id('_db_update.Drop_FK') is not null    drop procedure _db_update.Drop_FK 
GO 
CREATE PROCEDURE _db_update.Drop_FK
------------------------------------------------------------------
(
@NAME  VARCHAR(200)

)
as
BEGIN


declare 
  @sql_drop varchar(max)


  select  
       @sql_drop  =  sql_drop
    
    from _db_update.view_FK
    where  NAME = @NAME
           
--           IndexColumnsNames like '%[[]'+ @COLUMN_NAME  +']%'


  print @sql_drop
  exec ( @sql_drop  )

END
GO 

if object_id('_db_update.Drop_table') is not null    drop procedure _db_update.Drop_table 
GO 
CREATE PROCEDURE _db_update.Drop_table
------------------------------------------------------------------
(
  @TABLE_NAME VARCHAR(50)
)
AS
BEGIN
  declare
   @sql varchar(4000);



  if OBJECT_ID(@TABLE_NAME) is null 
    return -1

--------------------------------------------



  set @sql=''
     
  SELECT 
    @sql=@sql + sql_drop

  from _db_update.view_FK  
  where 
    ( Table_name = @TABLE_NAME ) or
    ( References_Table_name = @TABLE_NAME  )


--------------------------------------------

 print @sql   
 exec (@sql)        

--------------------------------------------
   
 set @sql= 'DROP TABLE '+ @TABLE_NAME 
 print @sql   
 exec (@sql)        

end
GO 

if object_id('_db_update.Column_Exists') is not null    drop function  _db_update.Column_Exists 
GO 
CREATE FUNCTION _db_update.Column_Exists
(
  -----  @Table_SCHEMA varchar(100)='dbo', 
	@TableName varchar(100), 
    @ColumnName varchar(100) 
)
--------------------------------------------------------------------------------
RETURNS bit AS
BEGIN
 -- DECLARE @Result bit;
/*
  IF not (OBJECT_ID(@TableName)>0)
  begin 
  -- print 'OBJECT_ID(@TableName) is null):' + @TableName
    RETURN -1;
  end*/
/*
if  COL_LENGTH('dbo.LinkEnd','project_id')  is not null
  ALTER TABLE dbo.LinkEnd
   drop  COLUMN project_id
GO
*/


if  COL_LENGTH(@TableName, @ColumnName)  is not null
  RETURN 1
  
 RETURN 0  

/*
  IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.Columns 
              WHERE 
                    Table_SCHEMA +'.'+ TABLE_NAME = @TableName 
--              	and TABLE_NAME = @TableName 
                AND COLUMN_NAME = @ColumnName)
    RETURN 1
    --SET @Result = 1
    
    RETURN 0
    --SET @Result = 0

*/

--@Result;

END
GO 

if object_id('_db_update.Column_DF_Get') is not null    drop procedure _db_update.Column_DF_Get 
GO 
CREATE PROCEDURE _db_update.Column_DF_Get
---------------------------------------------------------------
(
 -- @Table_SCHEMA varchar(100)='dbo', 


  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(50),
  
  @sql_create VARCHAR(max) output, 
  @sql_drop   VARCHAR(max) output
  
)
as
BEGIN

  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME ='lib_Radiation_class'
    set @COLUMN_NAME='name'  
  END
 

  set @sql_create=''
  set @sql_drop=''
  



   SELECT
   -- @DEFAULT_NAME =o.name 
   
     @sql_create = sql_create, -- 'ALTER TABLE '+  @TABLE_NAME +' ADD CONSTRAINT '+ o.name + '  DEFAULT(' +  com.text  +') FOR '+ @COLUMN_NAME,
                
     @sql_drop = sql_drop --'ALTER TABLE '+  @TABLE_NAME +' DROP CONSTRAINT '+ o.name
   
   --o.name, com.text   
     FROM _db_update.view_Column_DF
     
     WHERE 
       TABLE_NAME_full = @TABLE_NAME 
       and  
       COLUMN_NAME = @COLUMN_NAME


 
/*
   SELECT
   -- @DEFAULT_NAME =o.name 
   
     @sql_create ='ALTER TABLE '+  @TABLE_NAME +' ADD CONSTRAINT '+ o.name + '  DEFAULT(' +  com.text  +') FOR '+ @COLUMN_NAME,
                
     @sql_drop= 'ALTER TABLE '+  @TABLE_NAME +' DROP CONSTRAINT '+ o.name
   
   --o.name, com.text   
     FROM sysobjects o INNER JOIN                       
          syscolumns c ON o.id = c.cdefault INNER JOIN  
          syscomments com ON o.id = com.id              
     WHERE (o.xtype = 'D') 
       and (OBJECT_NAME(o.parent_obj)=@TABLE_NAME) 
       and (c.name=@COLUMN_NAME)
    
*/
  
  return @@ROWCOUNT
  

END
GO 

if object_id('_db_update.Column_FK_Get') is not null    drop procedure _db_update.Column_FK_Get 
GO 
CREATE PROCEDURE _db_update.Column_FK_Get
-------------------------------------------------------
(
 -- @Table_SCHEMA varchar(100)='dbo', 

  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(50),
  
  @sql_create VARCHAR(MAX) output, 
  @sql_drop   VARCHAR(MAX) output
  
)
as
BEGIN


    if @TABLE_NAME is null 
    BEGIN
     set @TABLE_NAME='dbo.Passive_Component'
     set @COLUMN_NAME ='channels'

     set @TABLE_NAME='dbo.link'
     set @COLUMN_NAME ='id'


    --// set @TABLE_NAME='LinkFreqPlan'
    --// set @COLUMN_NAME ='folder_id'
      
    END 



/*
  select  
       @sql_drop   = @sql_drop + sql_drop,
       @sql_create = @sql_create + sql_create  
  
  
    from service.view_FK
    where  schema_name = @Table_SCHEMA and 
           TABLE_NAME = @TABLE_NAME and
           
           IndexColumnsNames like '%[[]'+ @COLUMN_NAME  +']%'
*/

/*

  select  
       @sql_drop   = @sql_drop + sql_drop,
       @sql_create = @sql_create + sql_create  
  
  
    from service.view_Index  
    where  schema_name = @Table_SCHEMA and 
           TABLE_NAME = @TABLE_NAME and
           
           IndexColumnsNames like '%['+ @COLUMN_NAME  +']%'

print @sql_drop
*/


/*
  if @TABLE_NAME IS NULL
  BEGIN
    set @TABLE_NAME ='lib_Radiation_class'
    set @COLUMN_NAME='name'  
  END
*/ 
  
  /*
  DECLARE
    @sql  VARCHAR(8000), 
    @sql_drop VARCHAR(8000); 
    
  */
--service.view_FK

/*
  select 
      object_name(constid) NAME, 
      
      object_name(fkeyid)  	FK_Table_name, 
      c1.name 			  		 	FK_Column_name,
      
      object_name(rkeyid)  	PK_Table_name, 
      c2.name 			   			PK_Column_name,
      
       ObjectProperty(constid, 'CnstIsUpdateCascade') as IsUpdateCascade,   
       ObjectProperty(constid, 'CnstIsDeleteCascade') as IsDeleteCascade  
   
  INTO #temp      
      
   from sysforeignkeys s
     inner join syscolumns c1 on ( s.fkeyid = c1.id and s.fkey = c1.colid )
     inner join syscolumns c2 on ( s.rkeyid = c2.id and s.rkey = c2.colid )
     
  WHERE 
    (object_name(fkeyid)=@TABLE_NAME and c1.name=@COLUMN_NAME)
    or
    (object_name(rkeyid)=@TABLE_NAME and c2.name=@COLUMN_NAME) 
    */
     

--select * from  service.view_FK
    
     
/*
   schema_name (f.schema_id) +'.'+ OBJECT_NAME(f.parent_object_id) AS Table_name,


  -- OBJECT_NAME(f.parent_object_id) AS Table_name,

   
   COL_NAME(fc.parent_object_id, fc.parent_column_id) AS 'column_name',
   
--   schema_name (t.schema_id) as References_schema_name,   
   schema_name (t.schema_id) +'.'+ OBJECT_NAME(t.object_id) as 'References_Table_name',

*/     



  set @sql_create=''
  set @sql_drop=''
     
  SELECT 
     --*
  
    @sql_create=@sql_create + sql_create,
    
/*    
          'ALTER TABLE '+    FK_Table_name + ' ADD CONSTRAINT ' +  NAME
           + ' FOREIGN KEY (' + FK_Column_name + ') REFERENCES '+ PK_Table_name
           + ' (' + PK_Column_name + ') ' + 
           
           CASE  
              when IsUpdateCascade=1 then  'ON UPDATE CASCADE '
           END +

           CASE 
              when IsDeleteCascade=1 then  'ON DELETE CASCADE '
           END             
           
           + ';'+ CHAR(10)
*/  

    @sql_drop=@sql_drop + sql_drop
    
--          'ALTER TABLE '+   FK_Table_name + ' DROP CONSTRAINT ' +  NAME + ';'+ CHAR(10)

  from _db_update.view_FK
  where 
   ( Table_name = @TABLE_NAME and column_name = @COLUMN_NAME )
    or
   ( References_Table_name = @TABLE_NAME and References_column_name = @COLUMN_NAME )

 -- FROM #temp  


 /*
  
  ALTER TABLE [dbo].[AntennaType]
ADD CONSTRAINT [FK_AntennaType_Folder] FOREIGN KEY ([folder_id]) 
  REFERENCES [dbo].[Folder] ([id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO*/


print @sql_create
print @sql_drop
 
END
GO 

if object_id('_db_update.Column_Index_Get') is not null    drop procedure _db_update.Column_Index_Get 
GO 
CREATE PROCEDURE _db_update.Column_Index_Get
------------------------------------------------------------------
(
--  @Table_SCHEMA varchar(100)='dbo', 

  @TABLE_NAME VARCHAR(100),
  @COLUMN_NAME VARCHAR(100),
  
  @sql_create VARCHAR(MAX) output, 
  @sql_drop   VARCHAR(MAX) output
    
)

AS
BEGIN
  

  set @sql_create=''
  set @sql_drop  =''
 

--  @sql_drop  =@sql_drop 


  select  
       @sql_drop   = @sql_drop + sql_drop,
       @sql_create = @sql_create + sql_create  
  
  
    from _db_update.view_Index  
    where  
--      schema_name = @Table_SCHEMA and 
      TABLE_NAME_full = @TABLE_NAME and
                 
      IndexColumnsNames like '%[[]'+ @COLUMN_NAME  +']%'

   
   
end
GO 

if object_id('_db_update.Column_Drop') is not null    drop procedure _db_update.Column_Drop 
GO 
CREATE PROCEDURE _db_update.Column_Drop
------------------------------------------------------------------
(
--  @Table_SCHEMA varchar(100)='dbo', 
 
  @TABLE_NAME  VARCHAR(100),
  @COLUMN_NAME VARCHAR(100)
)
AS
BEGIN
  DECLARE 
    @sql varchar(max);

  DECLARE
    @FK_sql_create VARCHAR(max), 
    @FK_sql_drop   VARCHAR(max),  


    @DF_sql_create VARCHAR(max), 
    @DF_sql_drop   VARCHAR(max),  

    @Index_sql_create VARCHAR(max), 
    @Index_sql_drop   VARCHAR(max)  



--exec service._ColumnDrop  'dbo.Link', 'pmp_sector_id' 

--exec service._ColumnDrop  'dbo.Link', 'pmp_sector_id' 
--exec service._ColumnDrop  'dbo.Link', 'pmp_terminal_id' 


if @TABLE_NAME is null 
BEGIN

 set @TABLE_NAME='dbo.link'
 set @COLUMN_NAME ='property2_id'
  
END 


  if _db_update.Column_Exists( @TABLE_NAME, @COLUMN_NAME)=0
  BEGIN
    print 'COLUMN_NAME not EXISTS'
    RETURN -1    
  END  


--service._Column_DF_Get


--table_name_full
    
  ------------------------
  -- _Drop_Check_Constraints_for_Column
  ------------------------
  set @sql=''
 
  select  
      @sql=sql_drop
    from _db_update.view_check_constraint
    where
        table_name_full = @TABLE_NAME
        and 
        definition like '%[[]'+ @COLUMN_NAME  +']%'

/*
 SELECT 
  
   @sql=@sql + 'ALTER TABLE '+  @TABLE_NAME +' DROP CONSTRAINT  '+ c.CONSTRAINT_NAME +';'
    
 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS c
  left join INFORMATION_SCHEMA.CHECK_CONSTRAINTS cc
    ON cc.CONSTRAINT_NAME = c.CONSTRAINT_NAME
    
 WHERE c.CONSTRAINT_TYPE='CHECK'
      and c.TABLE_NAME  = @TABLE_NAME 
      and cc.check_clause like '%[[]'+ @COLUMN_NAME  +']%'
*/
--service.view_check_constraint

  print @sql    
  
--  return
  
  exec ( @sql )


-------------------------------------------------


  exec _db_update.Column_FK_Get     @TABLE_NAME, @COLUMN_NAME, @FK_sql_create output, @FK_sql_drop output
  
  exec _db_update.Column_DF_Get     @TABLE_NAME, @COLUMN_NAME, @DF_sql_create output, @DF_sql_drop output
  exec _db_update.Column_Index_Get  @TABLE_NAME, @COLUMN_NAME, @Index_sql_create output, @Index_sql_drop output


--  return
  

print '@FK_sql_drop - ' + @FK_sql_drop
print '@DF_sql_drop - ' + @DF_sql_drop
print '@Index_sql_drop - ' + @Index_sql_drop

 
  if @FK_sql_drop   <>'' exec (@FK_sql_drop)
  if @Index_sql_drop<>'' exec (@Index_sql_drop)
  if @DF_sql_drop   <>'' exec (@DF_sql_drop)


   
 set @sql= 'ALTER TABLE '+  @TABLE_NAME +' DROP COLUMN ['+ @COLUMN_NAME + ']'
 print @sql    
   
   
 exec (@sql)
        
end
GO 

if object_id('_db_update.Column_AddDefault') is not null    drop procedure _db_update.Column_AddDefault 
GO 
CREATE PROCEDURE _db_update.Column_AddDefault
------------------------------------------------------------------
(
 --@Table_SCHEMA varchar(100)='dbo', 

  @TABLE_NAME  VARCHAR(100),
  @COLUMN_NAME VARCHAR(100),
--  @CONSTRAINT_NAME VARCHAR(50),
  @DEFAULT VARCHAR(100)=null 
)
AS
BEGIN


if @TABLE_NAME is null 
BEGIN
 set @TABLE_NAME='link'
 set @COLUMN_NAME ='calc_method'
 set @DEFAULT='(3)'
  
END 

  declare 
    @CONSTRAINT_NAME_NEW varchar(100),
    @Column_default varchar(100),
    @sql varchar(100),
    @s   varchar(1000),
    @null varchar(50) 
   -- @def varchar(50)


  if _db_update.Column_Exists( @TABLE_NAME, @COLUMN_NAME)=0
  BEGIN
    print 'column not exists'
    RETURN -1
  END 
  


  if @DEFAULT=''
     set @DEFAULT=null



  SELECT --@name=name,  
  		@column_default=Column_default
  	FROM 
      _db_update.view_Columns
  	WHERE 
      TABLE_NAME_full = @TABLE_NAME 
      AND 
      COLUMN_NAME = @COLUMN_NAME


  IF @column_default = @DEFAULT
    RETURN 1




--select * from sys.default_constraints 

--  set @CONSTRAINT_NAME_NEW = 'DF_' + @TABLE_NAME + '_' + @COLUMN_NAME    

  set @CONSTRAINT_NAME_NEW = 'DF_' + replace (@TABLE_NAME,'.','_') + '_' + @COLUMN_NAME    



select 
   @sql = sql_drop
   
from
  _db_update.view_Column_DF

WHERE --Table_SCHEMA  = @Table_SCHEMA and
      TABLE_NAME_full = @TABLE_NAME AND 
      COLUMN_NAME = @COLUMN_NAME


/*
IF OBJECT_ID(@CONSTRAINT_NAME_NEW, 'D') IS NOT NULL 
  exec ('ALTER TABLE '+ @Table_SCHEMA +'.'+ @TABLE_NAME +' DROP CONSTRAINT '+ @CONSTRAINT_NAME_NEW)




   set @sql=''

   SELECT 
     @sql = 'ALTER TABLE '+ @Table_SCHEMA +'.'+@TABLE_NAME +' DROP CONSTRAINT '+ o.name 
   
   FROM sysobjects o INNER JOIN                       
        syscolumns c ON o.id = c.cdefault INNER JOIN  
        syscomments com ON o.id = com.id              
   WHERE (o.xtype = 'D') and (OBJECT_NAME(o.parent_obj)=@TABLE_NAME) and (c.name=@COLUMN_NAME)
	
   */
   
   print @sql
   exec (@sql)

    
    if @DEFAULT is not null
    BEGIN
    
      set @s ='ALTER TABLE '+ @TABLE_NAME +' ADD CONSTRAINT '+ @CONSTRAINT_NAME_NEW + 
              '  DEFAULT ' + @DEFAULT  +' FOR '+ @COLUMN_NAME
      print @s

      exec (@s)      
    END
    

end
GO 

if object_id('_db_update._UpdateAllViews') is not null    drop procedure _db_update._UpdateAllViews 
GO 
CREATE PROCEDURE _db_update._UpdateAllViews
------------------------------------------------------------------

AS
BEGIN

  
  DECLARE 
    @name varchar(200),
    @sql nvarchar(100);
  
  DECLARE 
    @temp table (name sysname);
     
    /*
  SELECT     
    TABLE_SCHEMA,
    TABLE_name,
  --  View_Definition,
    *
    
  FROM INFORMATION_SCHEMA.views    */



--TABLE_name
--TABLE_SCHEMA    
--View_Definition
 -- return



INSERT INTO @temp
	select TABLE_SCHEMA+'.'+ TABLE_name 	  
     FROM INFORMATION_SCHEMA.views    
     where TABLE_name not like '%11'
     
   --  where table_schema='security'
     
--	from sysobjects WHERE (type = 'V') 
/*
	select *
     FROM INFORMATION_SCHEMA.views    
     where table_schema='security'
     

*/




/*
INSERT INTO @temp
	select name  	  
	from sysobjects WHERE (type = 'V') 

*/

  WHILE exists(SELECT * FROM @temp)
  BEGIN
    SELECT top 1 @name=name FROM @temp
    delete from @temp where name=@name
  
    print @name


    exec sp_refreshview @name


--return

  END
 

END
GO 

if object_id('_db_update.sp_Script') is not null    drop procedure _db_update.sp_Script 
GO 
CREATE PROCEDURE _db_update.sp_Script
AS
BEGIN

declare
	@s varchar(max) = ''
    

set @s =  'if schema_id(''_db_update'') is null  execute(''create schema _db_update'')'  + char(13)+char(10)+  
         'GO ' + char(13)+char(10)  


select @s=@s +  sql_create + char(13)+char(10) --  +   'GO ' + char(13)+char(10)  
 from _db_update.view_View    


select @s=@s +  sql_create  +char(13)+char(10)  -- +   'GO ' +char(13)+char(10)  
 from _db_update.view_Proc


--        definition like '%[[]'+ @COLUMN_NAME  +']%'


set @s = @s + 'exec _db_update.sp_Main'  + char(13)+char(10)  
    --     'GO ' + char(13)+char(10)  


--set @s=@s +  'EXEC [_service].[sp_Main] '  +char(13)+char(10)+   'GO ' +char(13)+char(10)  
 


select @s


  /* Procedure body */
END
GO 

if object_id('_db_update.sp_Main') is not null    drop procedure _db_update.sp_Main 
GO 
CREATE PROCEDURE _db_update.sp_Main
AS
BEGIN
  delete from _object_fields
    where object_id not in (select id from _objects)  


IF OBJECT_ID(N'dbo.PMP_site_tri_Constraint') IS NOT NULL
  DROP TRIGGER  dbo.PMP_site_tri_Constraint


IF OBJECT_ID(N'dbo.Link_Repeater_tri_Project_ID') IS NOT NULL
  DROP TRIGGER  dbo.Link_Repeater_tri_Project_ID
 
IF OBJECT_ID(N'dbo.Link_tri_Constraint') IS NOT NULL
  DROP TRIGGER  dbo.Link_tri_Constraint

IF OBJECT_ID(N'dbo.LinkEnd_tri_Constraint_Property_Project_ID') IS NOT NULL
  DROP TRIGGER  dbo.LinkEnd_tri_Constraint_Property_Project_ID






IF OBJECT_ID(N'[dbo].[view_PMP_CalcRegion_PMP_Sites]') IS NOT NULL
  DROP VIEW [dbo].[view_PMP_CalcRegion_PMP_Sites]
  

IF OBJECT_ID(N'[dbo].[view_Projects_summary]') IS NOT NULL
  DROP VIEW [dbo].[view_Projects_summary]

IF OBJECT_ID(N'[dbo].[view_PMP_site]') IS NOT NULL
  DROP VIEW dbo.view_PMP_site


--Modulation_type
--POWER_LOSS
--LinkFreqPlan_Neighbors
------------------------------------------------------------
--- LinkCalcResults
------------------------------------------------------------
if  COL_LENGTH('dbo.LinkCalcResults','link_id')  is not null
  exec _db_update.Column_Drop 'dbo.LinkCalcResults','link_id'

  ALTER TABLE dbo.LinkCalcResults  ADD link_id int

------------------------------------------------------------
--- PMP_CalcRegionCells
------------------------------------------------------------


if  COL_LENGTH('dbo.link_repeater_antenna','repeater_gain')  is null
  ALTER TABLE dbo.link_repeater_antenna  ADD repeater_gain float



if  COL_LENGTH('dbo.PMP_CalcRegionCells','pmp_sector_id')  is not null
  exec _db_update.Column_Drop 'dbo.PMP_CalcRegionCells','pmp_sector_id'

  ALTER TABLE dbo.PMP_CalcRegionCells  ADD pmp_sector_id int


if  COL_LENGTH('dbo.Pmp_CalcRegion','terminal_sensitivity')  is null
  ALTER TABLE dbo.Pmp_CalcRegion  ADD terminal_sensitivity float


if  COL_LENGTH('dbo.Pmp_CalcRegion','terminal_gain')  is null
  ALTER TABLE dbo.Pmp_CalcRegion  ADD terminal_gain float

if  COL_LENGTH('dbo.Pmp_CalcRegion','terminal_loss')  is null
  ALTER TABLE dbo.Pmp_CalcRegion  ADD terminal_loss float

/*
update _object_fields set name='terminal_gain' where name='terminal_gain_db' and Object_Name is null
update _object_fields set name='terminal_loss' where name='terminal_loss_db'  and Object_Name is null
update _object_fields set name='terminal_sensitivity' where name='terminal_sensitivity_dbm'  and Object_Name is null
*/




--if  COL_LENGTH('dbo.Pmp_CalcRegion','terminal_loss')  is not null
--  exec _db_update.Column_Drop 'dbo.Pmp_CalcRegion','terminal_loss'


-------------------------------------
if  COL_LENGTH('dbo.LinkFreqPlan_Neighbors','tx_link_id')  is not null
  exec _db_update.Column_Drop 'dbo.LinkFreqPlan_Neighbors','tx_link_id'


if  COL_LENGTH('dbo.Template_LinkEnd','POWER_LOSS')  is null
  ALTER TABLE dbo.Template_LinkEnd  ADD POWER_LOSS float

--_UpdateAllViews

if  COL_LENGTH('dbo.Link_reflection_points','radius')  is not null
  exec _db_update.Column_Drop 'dbo.Link_reflection_points','radius'

if  COL_LENGTH('dbo.Link_reflection_points','distance')  is not null
  exec _db_update.Column_Drop 'dbo.Link_reflection_points','distance'

if  COL_LENGTH('dbo.Link_reflection_points','length')  is not null
  exec _db_update.Column_Drop 'dbo.Link_reflection_points','length'



if  COL_LENGTH('dbo.Template_LinkEnd','Modulation_type')  is not null
  exec _db_update.Column_Drop 'dbo.Template_LinkEnd','Modulation_type'



if  COL_LENGTH('dbo.Template_LinkEnd','Modulation_type')  is null
  ALTER TABLE dbo.Template_LinkEnd  ADD Modulation_type varchar(30)

--_UpdateAllViews


if  COL_LENGTH('dbo.link','AM_data_bin')  is null
  ALTER TABLE dbo.link  ADD AM_data_bin image

if  COL_LENGTH('dbo.link','AM_data_xml')  is null
  ALTER TABLE dbo.link  ADD AM_data_xml XML


if  COL_LENGTH('dbo.relief','geom')  is null
  ALTER TABLE dbo.relief  ADD geom geometry NULL

if  COL_LENGTH('dbo.LinkFreqPlan_Neighbors','LINKEND1_ID')  is null
  ALTER TABLE dbo.LinkFreqPlan_Neighbors  ADD LINKEND1_ID int

if  COL_LENGTH('dbo.LinkFreqPlan_Neighbors','LINKEND2_ID')  is null
  ALTER TABLE dbo.LinkFreqPlan_Neighbors  ADD LINKEND2_ID int

if  COL_LENGTH('dbo.LinkFreqPlan_Neighbors','rx_for_linkend1_id')  is null
  ALTER TABLE dbo.LinkFreqPlan_Neighbors  ADD rx_for_linkend1_id int

if  COL_LENGTH('dbo.Linkend_antenna ','mast_index')  is null
  ALTER TABLE dbo.Linkend_antenna  ADD mast_index int


if  COL_LENGTH('dbo.LinkFreqPlan_Neighbors','tx_linkend1_name')  is null
  ALTER TABLE dbo.LinkFreqPlan_Neighbors  ADD tx_linkend1_name varchar(50) 

if  COL_LENGTH('dbo.LinkFreqPlan_Neighbors','tx_linkend2_name')  is null
  ALTER TABLE dbo.LinkFreqPlan_Neighbors  ADD tx_linkend2_name varchar(50) 

if  COL_LENGTH('dbo.LinkFreqPlan_Neighbors','tx_link_id')  is not null
  exec _db_update.Column_Drop 'dbo.LinkFreqPlan_Neighbors','tx_link_id'




if  COL_LENGTH('dbo.link','length_km')  is null
  ALTER TABLE dbo.link  ADD  length_km AS [length]/(1000)




IF OBJECT_ID(N'dbo.view_PMP_Terminal_Antennas') IS NOT NULL
   DROP view dbo.view_PMP_Terminal_Antennas


exec _db_update.Drop_FK 'FK_Map_XREF_Map'
exec _db_update.Drop_FK 'FK_Link_reflection_points_Link_id'


IF OBJECT_ID(N'dbo.sp_MapMaker_Data') IS not  NULL
  DROP PROCEDURE dbo.sp_MapMaker_Data

IF OBJECT_ID(N'dbo.sp_MapMaker') IS not  NULL
  DROP PROCEDURE dbo.sp_MapMaker



if  COL_LENGTH('dbo.Template_Linkend_Antenna','loss')  is null
  ALTER TABLE dbo.Template_Linkend_Antenna  ADD loss float

if  COL_LENGTH('dbo.Linkend','color')  is null
  ALTER TABLE dbo.Linkend  ADD  color int DEFAULT round(rand()*(1000000),(0)) NULL
 



--vMsg 547, Level 16, State 0, Line 12166
--The ALTER TABLE statement conflicted with the FOREIGN KEY constraint "FK_Link_reflection_points_Link_id". The conflict occurred in database "1_tele2_nn_MSK", table "dbo.Link", column 'id'.

-------------------------------------------------
-- view
-------------------------------------------------
IF OBJECT_ID(N'dbo.view_PMP_link_summary') IS NOT NULL
   DROP view dbo.view_PMP_link_summary



IF OBJECT_ID(N'dbo.view_PMP_TERMINAL_full') IS NOT NULL
   DROP view dbo.view_PMP_TERMINAL_full

IF OBJECT_ID(N'dbo.view_SH_PMPCALCREGION_SITE') IS NOT NULL
   DROP view dbo.view_SH_PMPCALCREGION_SITE


IF OBJECT_ID(N'dbo.view_Project_summary_') IS NOT NULL
   DROP view dbo.view_Project_summary_

IF OBJECT_ID(N'dbo.view_SH_Pmp_Site') IS NOT NULL
   DROP view dbo.view_SH_Pmp_Site

IF OBJECT_ID(N'dbo.vExportToMDB_Physical_param') IS NOT NULL
   DROP view dbo.vExportToMDB_Physical_param


IF OBJECT_ID(N'dbo.view_LinkFreqPlan_LinkEnd_for_calc_NB') IS NOT NULL
   DROP view dbo.view_LinkFreqPlan_LinkEnd_for_calc_NB


IF OBJECT_ID(N'dbo.view_LinkFreqPlan_LinkEnd_for_calc') IS NOT NULL
   DROP view dbo.view_LinkFreqPlan_LinkEnd_for_calc


IF OBJECT_ID(N'dbo.view_Link_Repeater_Used_In') IS NOT NULL
   DROP view dbo.view_Link_Repeater_Used_In


IF OBJECT_ID(N'dbo.view_PMP_sectors') IS NOT NULL
   DROP view dbo.view_PMP_sectors


--IF OBJECT_ID(N'dbo.view_PMP_sector') IS NOT NULL
--   DROP view dbo.view_PMP_sector


IF OBJECT_ID(N'dbo.view_PMP_Sector_Antennas') IS NOT NULL
   DROP view dbo.view_PMP_Sector_Antennas


IF OBJECT_ID(N'dbo.view_PMP_Sector_base') IS NOT NULL
   DROP view dbo.view_PMP_Sector_base


IF OBJECT_ID(N'dbo.view_PMP_sector_for_Calc') IS NOT NULL
   DROP view dbo.view_PMP_sector_for_Calc



IF OBJECT_ID(N'dbo.view_PMP_terminal_base') IS NOT NULL
   DROP view dbo.view_PMP_terminal_base



IF OBJECT_ID(N'dbo.view_PMP_terminal_for_Calc') IS NOT NULL
   DROP view dbo.view_PMP_terminal_for_Calc

 


IF OBJECT_ID(N'dbo.view_PMP_sector_for_Calc') IS NOT NULL
   DROP view dbo.view_PMP_sector_for_Calc



IF OBJECT_ID(N'dbo.view_PMP_terminal_for_Calc') IS NOT NULL
   DROP view dbo.view_PMP_terminal_for_Calc


IF OBJECT_ID(N'dbo.view_PMP_sector_with_project_id') IS NOT NULL
   DROP view dbo.view_PMP_sector_with_project_id


IF OBJECT_ID(N'dbo.view_PMP_sectors_new') IS NOT NULL
   DROP view dbo.view_PMP_sectors_new


IF OBJECT_ID(N'dbo.view_PMP_TERMINAL_with_project_id') IS NOT NULL
   DROP view dbo.view_PMP_TERMINAL_with_project_id

  
-------------------------------------------------
-- view
-------------------------------------------------

----------------------------------------------------------------------

if  COL_LENGTH('dbo.LinkFreqPlan','PolarizationLevel')  is null
  ALTER TABLE dbo.LinkFreqPlan  ADD PolarizationLevel float DEFAULT -30 NULL



----------------------------------------------------------------------

--if  COL_LENGTH('dbo.Link','AM_data_xml')  is null
--  ALTER TABLE dbo.Link
--    ADD AM_data_xml xml NULL
--GO

if  COL_LENGTH('dbo.Linkend_antenna','pmp_terminal_id')  is not null
  exec _db_update.Column_Drop 'dbo.Linkend_antenna','pmp_terminal_id'

if  COL_LENGTH('dbo.Linkend_antenna','pmp_sector_id')  is not null
  exec _db_update.Column_Drop 'dbo.Linkend_antenna','pmp_sector_id'


if  COL_LENGTH('dbo.Link','pmp_terminal_id')  is not null
  exec _db_update.Column_Drop 'dbo.Link','pmp_terminal_id'

if  COL_LENGTH('dbo.Link','pmp_sector_id')  is not null
  exec _db_update.Column_Drop 'dbo.Link','pmp_sector_id'

if  COL_LENGTH('dbo.Link_repeater_antenna','Display_order')  is not null
  exec _db_update.Column_Drop 'dbo.Link_repeater_antenna','Display_order'


if  COL_LENGTH('dbo.Link_Repeater_Antenna','project_id')  is not null
  exec _db_update.Column_Drop 'dbo.Link_Repeater_Antenna','project_id'


if  COL_LENGTH('dbo.LinkEnd','pmp_sector_id')  is not null
  exec _db_update.Column_Drop 'dbo.LinkEnd','pmp_sector_id'


if  COL_LENGTH('dbo.LinkEnd','project_id')  is not null
  exec _db_update.Column_Drop 'dbo.LinkEnd','project_id'

--  ALTER TABLE dbo.LinkEnd
--   drop  COLUMN project_id
--GO



if  COL_LENGTH('dbo.LinkEnd','Connected_PMP_Sector_ID')  is null
  ALTER TABLE dbo.Linkend
    ADD Connected_PMP_Sector_ID int


if  COL_LENGTH('dbo.LinkEnd','Connected_PMP_Site_ID')  is null
  ALTER TABLE dbo.Linkend
    ADD Connected_PMP_Site_ID int



if  COL_LENGTH('dbo.Link','AM_data_bin')  is null
  ALTER TABLE dbo.Link
    ADD AM_data_bin image NULL



if  COL_LENGTH('dbo.Link','refraction_2')  is null
  ALTER TABLE dbo.Link
    ADD refraction_2 float NULL


if  COL_LENGTH('dbo.Link','status_back')  is null
  ALTER TABLE dbo.Link  ADD status_back int


if  COL_LENGTH('dbo.Link','Link_Repeater_id')  is not null
  exec _db_update.Column_Drop 'dbo.Link','Link_Repeater_id'

--  ALTER TABLE dbo.Link
--   drop  COLUMN Link_Repeater_id

---------------------------------------------------
--PMP_site
---------------------------------------------------
if  COL_LENGTH('dbo.PMP_site','project_id')  is not null
 exec _db_update.Column_Drop 'dbo.PMP_site','project_id'




---------------------------------------------------
--LinkEnd_antenna
---------------------------------------------------
if  COL_LENGTH('dbo.LinkEnd_antenna','loss')  is null
  ALTER TABLE dbo.LinkEnd_antenna  ADD loss float


if  COL_LENGTH('dbo.LinkEnd_antenna','project_id')  is not null
 exec _db_update.Column_Drop 'dbo.LinkEnd_antenna','project_id'
--  ALTER TABLE dbo.LinkEnd_antenna
--   drop  COLUMN project_id
--GO

if  COL_LENGTH('dbo.Template_Linkend_Antenna','loss')  is null
  ALTER TABLE dbo.Template_Linkend_Antenna 
    ADD loss float 



---------------------------------------------------
--Template_Linkend
---------------------------------------------------

if  COL_LENGTH('dbo.Template_Linkend','bitrate_Mbps')  is null
  ALTER TABLE dbo.Template_Linkend
    ADD bitrate_Mbps float NULL


if  COL_LENGTH('dbo.Template_Linkend','SIGNATURE_HEIGHT')  is null
  ALTER TABLE dbo.Template_Linkend
    ADD SIGNATURE_HEIGHT float NULL


if  COL_LENGTH('dbo.Template_Linkend','SIGNATURE_WIDTH')  is null
  ALTER TABLE dbo.Template_Linkend
    ADD SIGNATURE_WIDTH float NULL


if  COL_LENGTH('dbo.Template_Linkend','POWER_MAX')  is null
  ALTER TABLE dbo.Template_Linkend
    ADD POWER_MAX float NULL


if  COL_LENGTH('dbo.Template_Linkend','channel_width_MHz')  is null
  ALTER TABLE dbo.Template_Linkend
    ADD channel_width_MHz float NULL


if  COL_LENGTH('dbo.Template_Linkend','MODULATION_TYPE')  is null
  ALTER TABLE dbo.Template_Linkend
    ADD MODULATION_TYPE float NULL


if  COL_LENGTH('dbo.LinkFreqPlan','project_id')  is not null
  exec _db_update.Column_Drop 'dbo.LinkFreqPlan','project_id'


if  COL_LENGTH('dbo.LinkFreqPlan_LinkEnd','Link_id')  is not null
  exec _db_update.Column_Drop 'dbo.LinkFreqPlan_LinkEnd','Link_id'
 
--  ALTER TABLE dbo.LinkFreqPlan_LinkEnd
--   drop  COLUMN Link_id  



if  COL_LENGTH('dbo.LinkFreqPlan_LinkEnd','next_linkend_id')  is not null
 exec _db_update.Column_Drop 'dbo.LinkFreqPlan_LinkEnd','next_linkend_id'
--  ALTER TABLE dbo.LinkFreqPlan_LinkEnd
--   drop  COLUMN next_linkend_id  
--GO



----------------------------------------------------------------------


if  COL_LENGTH('dbo._objects','table_name_initial')  is null
  ALTER TABLE dbo._objects
   ADD table_name_initial varchar(50) 


if  COL_LENGTH('dbo._object_fields','is_use_on_map')  is null
  ALTER TABLE dbo._object_fields
    ADD is_use_on_map bit NULL


if  COL_LENGTH('dbo._object_fields','is_user_column')  is null
  ALTER TABLE dbo._object_fields
    ADD is_user_column bit NULL

----------------------------------------------------------------------
-- Link_Repeater
----------------------------------------------------------------------
if  COL_LENGTH('dbo.Link_Repeater_antenna','slot')  is  null
 ALTER TABLE dbo.Link_Repeater_antenna ADD slot int NULL

if  COL_LENGTH('dbo.Link_Repeater','project_id')  is not null
 exec _db_update.Column_Drop 'dbo.Link_Repeater','project_id'

if  COL_LENGTH('dbo.Link_Repeater','is_active')  is null
  ALTER TABLE dbo.Link_Repeater ADD is_active int NULL

if  COL_LENGTH('dbo.Link_Repeater','link_ID')  is null
  ALTER TABLE dbo.Link_Repeater ADD link_ID int NULL

if  COL_LENGTH('dbo.Link_Repeater','calc_results_xml_1')  is null
  ALTER TABLE dbo.Link_Repeater ADD calc_results_xml_1 text

if  COL_LENGTH('dbo.Link_Repeater','calc_results_xml_2')  is null
  ALTER TABLE dbo.Link_Repeater ADD calc_results_xml_2 text

if  COL_LENGTH('dbo.Link_Repeater','calc_results_xml_3')  is null
  ALTER TABLE dbo.Link_Repeater ADD calc_results_xml_3 text


----------------------------------------------------------------------
-- Template_LinkEnd
----------------------------------------------------------------------
if  COL_LENGTH('dbo.Template_LinkEnd','mode')  is null
  ALTER TABLE dbo.Template_LinkEnd ADD mode int NULL



if  COL_LENGTH('dbo.Template_LinkEnd','channel_type')  is null
  ALTER TABLE dbo.Template_LinkEnd ADD channel_type int NULL


if  COL_LENGTH('dbo.Template_LinkEnd','subband')  is null
  ALTER TABLE dbo.Template_LinkEnd ADD subband varchar(50) COLLATE Cyrillic_General_CI_AS NULL


if  COL_LENGTH('dbo.Template_LinkEnd','channel_number')  is null
  ALTER TABLE dbo.Template_LinkEnd ADD channel_number int NULL


if  COL_LENGTH('dbo.Template_LinkEnd','rx_freq_MHz')  is null
  ALTER TABLE dbo.Template_LinkEnd ADD rx_freq_MHz int NULL

--yle xml NULL


if  COL_LENGTH('dbo.Map_XREF','style')  is null
  ALTER TABLE dbo.Map_XREF  ADD style xml NULL


----------------------------------------
if  COL_LENGTH('dbo.Template_LinkEnd','tx_freq_MHz')  is null
  ALTER TABLE dbo.Template_LinkEnd ADD tx_freq_MHz int NULL


----------------------------------------------------------------------
if  COL_LENGTH('dbo.Map_Objects','style')  is null
  ALTER TABLE dbo.Map_Objects  ADD style xml NULL

if  COL_LENGTH('dbo.Map_XREF','style')  is null
  ALTER TABLE dbo.Map_XREF  ADD style xml NULL


if  COL_LENGTH('dbo.Property','name_rus')  is null
  ALTER TABLE dbo.Property
    ADD name_rus varchar(200) COLLATE Cyrillic_General_CI_AS NULL


if  COL_LENGTH('dbo.Property','NRI')  is null
  ALTER TABLE dbo.Property
   ADD NRI varchar(30) COLLATE Cyrillic_General_CI_AS NULL


if  COL_LENGTH('dbo.Property','mast_count')  is null
  ALTER TABLE dbo.Property
    ADD mast_count int



-------------------------------------------------
-- SCHEMA
-------------------------------------------------

IF schema_id ('reports') IS NULL  exec ('CREATE SCHEMA [reports]')
IF schema_id ('link') IS NULL  exec ('CREATE SCHEMA [link]')
IF schema_id ('pmp') IS NULL  exec ('CREATE SCHEMA [pmp]')
IF schema_id ('link_calc') IS NULL  exec ('CREATE SCHEMA [link_calc]')
IF schema_id ('link_climate') IS NULL  exec ('CREATE SCHEMA [link_climate]')
IF schema_id ('LinkFreqPlan') IS NULL  exec ('CREATE SCHEMA [LinkFreqPlan]')
IF schema_id ('geo') IS NULL  exec ('CREATE SCHEMA [geo]')
IF schema_id ('history') IS NULL  exec ('CREATE SCHEMA [history]')
IF schema_id ('ui') IS NULL  exec ('CREATE SCHEMA [ui]')
IF schema_id ('lib') IS NULL  exec ('CREATE SCHEMA [lib]')
IF schema_id ('map') IS NULL  exec ('CREATE SCHEMA [map]')
IF schema_id ('Security') IS NULL  exec ('CREATE SCHEMA [Security]')
IF schema_id ('gis') IS NULL  exec ('CREATE SCHEMA [gis]')


IF OBJECT_ID(N'dbo.link_SDB') IS  NULL

  CREATE TABLE dbo.link_SDB (
    id int IDENTITY(1, 1) NOT NULL,
    name varchar(100) COLLATE Cyrillic_General_CI_AS NULL,
    project_id int NULL,
    guid uniqueidentifier DEFAULT newid() NULL,
    link1_id int NULL,
    link2_id int NULL,
    folder_id int NULL,
    date_created datetime DEFAULT getdate() NULL,
    date_modify datetime NULL,
    user_created varchar(50) COLLATE Cyrillic_General_CI_AS DEFAULT suser_name() NULL,
    user_modify varchar(50) COLLATE Cyrillic_General_CI_AS NULL,
    _ bit NULL,
    kng_required float NULL,
    SESR_required float NULL,
    kng float NULL,
    kng_year AS ((((0.01)*[Kng])*(365))*(24))*(60),
    sesr float NULL,
    status int NULL,
    CONSTRAINT link_SDB_pk PRIMARY KEY CLUSTERED (id)
      WITH (
        PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF,
        ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
    CONSTRAINT link_SDB_uq UNIQUE (project_id, name)
      WITH (
        PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF,
        ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
    CONSTRAINT link_SDB_fk FOREIGN KEY (project_id) 
    REFERENCES dbo.Project (id) 
    ON UPDATE NO ACTION
    ON DELETE CASCADE,
    CONSTRAINT link_SDB_fk2 FOREIGN KEY (link1_id) 
    REFERENCES dbo.Link (id) 
    ON UPDATE NO ACTION
    ON DELETE NO ACTION,
    CONSTRAINT link_SDB_fk3 FOREIGN KEY (link2_id) 
    REFERENCES dbo.Link (id) 
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
  )
  ON [PRIMARY]



/*
if OBJECT_ID('Security.GeoRegion') is not null
begin
--  delete from Security.GeoRegion

 -- if OBJECT_ID('Security.GeoRegion_ck') is not null
  --  ALTER TABLE [Security].[GeoRegion] DROP CONSTRAINT [GeoRegion_ck]

end
*/

 
END
GO 

exec _db_update.sp_Main




/****** Object:  UserDefinedFunction [dbo].[fn_Linkend_Antenna_pos]    Script Date: 16.05.2020 10:54:58 ******/
IF OBJECT_ID(N'[dbo].[fn_Linkend_Antenna_pos]') IS  not  NULL

DROP FUNCTION [dbo].[fn_Linkend_Antenna_pos]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Linkend_Antenna_pos]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE FUNCTION [dbo].[fn_Linkend_Antenna_pos] 
(
  @LINKEND_ID int   
 
)

RETURNS @ret TABLE 
(
    lat 	float,
    lon 	float,
    
    location_type integer
)

as

BEGIN

if exists 
(
  select *
  	from Linkend_Antenna 
       left join Property  on Property.id = Linkend_Antenna.Property_id
    where Linkend_Antenna.Linkend_id = @LINKEND_ID

)


  insert into @ret (lat,lon, location_type)
    select top 1 
      IIF (IsNull(location_type,0) = 0, Property.lat,  Linkend_Antenna.lat) as lat,
      IIF (IsNull(location_type,0) = 0, Property.lon,  Linkend_Antenna.lon) as lon,
      location_type 

      from Linkend_Antenna 
         left join Property  on Property.id = Linkend_Antenna.Property_id
      where Linkend_Antenna.Linkend_id = @LINKEND_ID

else

  insert into @ret (lat,lon)

    select 
        P.lat,
        P.lon      

      from  Linkend L  
        left join Property P  on P.id = L.Property_id
      where L.id = @LINKEND_ID
 
  

	RETURN
  
END

GO




IF OBJECT_ID(N'[dbo].[fn_Linkend_Link_info]') IS NOT NULL
  DROP FUNCTION [dbo].[fn_Linkend_Link_info]
GO


CREATE FUNCTION [dbo].fn_Linkend_Link_info
(	
  @Linkend_id int
  
)
RETURNS TABLE 
--(id int)
AS
RETURN 
(
        
  SELECT 
    L.id as Link_id, 
    L.name as Link_name,
    clutter_model_id,
    cluttermodel.name as clutter_model_name,
      
    length,
    length_km,    
      
    case
      when (LinkEnd1_id = @Linkend_id) then LinkEnd2_id
      when (LinkEnd2_id = @Linkend_id) then LinkEnd1_id
      else  null
    end as next_LinkEnd_id,
    
    @Linkend_id as LinkEnd_id,
    
    calc_method
    
  
    FROM Link L
      left outer join cluttermodel  on cluttermodel.id=L.clutter_model_id
      
    WHERE 
        ((LinkEnd1_id = @Linkend_id) or (LinkEnd2_id=@Linkend_id))
        -- and (1=0)


)


go



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- delete --

/****** Object:  StoredProcedure [link_climate].[sp_Link_climate_UPD]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[link_climate].[sp_Link_climate_UPD]') IS NOT NULL
  DROP PROCEDURE [link_climate].[sp_Link_climate_UPD]
GO


CREATE PROCEDURE [link_climate].[sp_Link_climate_UPD]
(
  @ID   int,  
  
  
------------------------
-- ITU
------------------------
  

@RRV_NAME 				 varchar(100)=NULL,

@terrain_type 			 int = null,          
@underlying_terrain_type int = null,

@Q_factor 			float = null,
@gradient_diel 		float = null,  
@gradient_deviation float = null,
@climate_factor 	float = null,

@factor_B 			float = null,
@factor_C 			float = null,
@factor_D 			float = null,

@rain_intensity 	float = null,
@steam_wet 			float = null,
@air_temperature    float = null,




/*
  // ITU
        db_Par(FLD_RRV_NAME      , aRec.comment ),

        //Q_factor_rec
        db_Par(FLD_terrain_type,            aRec.ITU.terrain_type ),
        db_Par(FLD_underlying_terrain_type, aRec.ITU.underlying_terrain_type),

        db_Par(FLD_Q_factor    ,  aRec.ITU.Q_factor ),

        db_Par(FLD_gradient_diel ,      aRec.ITU.gradient_diel),
        db_Par(FLD_gradient_deviation , aRec.ITU.gradient_deviation ),

        db_Par(FLD_climate_factor,      aRec.ITU.climate_factor),

        db_Par(FLD_factor_B,   aRec.ITU.factor_B),
        db_Par(FLD_factor_C,   aRec.ITU.factor_C),
        db_Par(FLD_factor_D,   aRec.ITU.factor_D),

        db_Par(FLD_rain_intensity,   aRec.ITU.rain_intensity1 ),
        db_Par(FLD_steam_wet,        aRec.ITU.steam_wet  ),

 */ 
------------------------
-- GOST
------------------------

@GOST_NAME 					  varchar(100)=NULL,

@GOST_gradient_diel 		  float = null,
@GOST_gradient_deviation 	  float = null,

@GOST_TERRAIN_TYPE 			  int = null,
@GOST_underlying_terrain_type int = null,
@GOST_steam_wet 		  	  float = null,

@GOST_air_temperature 		  float = null,
@GOST_atmosphere_pressure 	  float = null,

@GOST_Climate_factor 		  float = null,
@GOST_rain_intensity 		  float = null,



/*

   // ---------------------------------------------------------------
        //GOST
        // ---------------------------------------------------------------

         db_Par (FLD_GOST_NAME,      aRec.Comment),

(*            if rec.air_temperature=0 then rec.air_temperature:=15;
          if rec.atmosphere_pressure=0 then rec.atmosphere_pressure:=1013;
          if rec.GOST.rain_intensity=0 then rec.GOST.rain_intensity:=1;
*)
          db_Par(FLD_GOST_gradient_diel ,      aRec.ITU.gradient_diel),
          db_Par(FLD_GOST_gradient_deviation , aRec.ITU.gradient_deviation ),

          db_Par (FLD_GOST_TERRAIN_TYPE,             aRec.ITU.terrain_type), //3
          db_Par (FLD_GOST_underlying_terrain_type,  aRec.ITU.underlying_terrain_type),//4
          db_Par (FLD_GOST_steam_wet,                aRec.ITU.steam_wet), //5

        //  if rec.air_temperature>0 then
          db_Par (FLD_GOST_air_temperature, aRec.air_temperature), //6

         // if rec.atmosphere_pressure>0 then
          db_Par (FLD_GOST_atmosphere_pressure, aRec.atmosphere_pressure), //7

          db_Par (FLD_GOST_Climate_factor,  aRec.ITU.Climate_factor), //8

//           if rec.GOST.rain_intensity>0 then
          db_Par (FLD_GOST_rain_intensity,  aRec.GOST.rain_intensity), //9
*/

------------------------
-- NIIR1998
------------------------

  @NIIR1998_NAME 				varchar(100)=NULL,

  @NIIR1998_gradient_diel 		float = null,
  @NIIR1998_gradient_deviation  float = null,


  @NIIR1998_TERRAIN_TYPE 		int = null,       
  @NIIR1998_steam_wet 			float = null,          

  @NIIR1998_air_temperature 	float = null,
  @NIIR1998_atmosphere_pressure float = null,

  @NIIR1998_rain_region_number 	int = null,
  @NIIR1998_Qd_region_number 	int = null
    
)

AS
BEGIN

if not exists (select * from link where id = @ID)
  RAISERROR('RAISERROR: id not exists ',16,1)

 


--Alter table  property Disable trigger all

  


 
  UPDATE Link 
  SET 
    
------------------------
-- ITU
------------------------      
     
    RRV_NAME 			 	  = COALESCE(@RRV_NAME, RRV_NAME),

    terrain_type 		 	  = COALESCE(@terrain_type, terrain_type),	 
    underlying_terrain_type  = COALESCE(@underlying_terrain_type, underlying_terrain_type),

    Q_factor 		 	 	= COALESCE(@Q_factor, Q_factor),	
    gradient_diel 	 	 	= COALESCE(@gradient_diel, gradient_diel),	  
    gradient_deviation  	= COALESCE(@gradient_deviation, gradient_deviation),
    climate_factor  	 	= COALESCE(@climate_factor	, climate_factor),
    
    air_temperature        = COALESCE(@air_temperature	, air_temperature),

    factor_B 				= COALESCE(@factor_B, factor_B),	
    factor_C 	 			= COALESCE(@factor_C, factor_C),		
    factor_D 	 			= COALESCE(@factor_D, factor_D),		

    rain_intensity 		= COALESCE(@rain_intensity, rain_intensity),
    steam_wet 		 		= COALESCE(@steam_wet, steam_wet), 		   
      
------------------------
-- GOST
------------------------

    GOST_NAME 					 = COALESCE(@GOST_NAME,	GOST_NAME),

    GOST_gradient_diel 		 = COALESCE(@GOST_gradient_diel, GOST_gradient_diel	),	
    GOST_gradient_deviation 	 = COALESCE(@GOST_gradient_deviation, GOST_gradient_deviation),

    GOST_TERRAIN_TYPE 			 = COALESCE(@GOST_TERRAIN_TYPE,	GOST_TERRAIN_TYPE),
    GOST_underlying_terrain_type= COALESCE(@GOST_underlying_terrain_type, GOST_underlying_terrain_type),
    GOST_steam_wet 		  	 = COALESCE(@GOST_steam_wet, GOST_steam_wet),

    GOST_air_temperature 		 = COALESCE(@GOST_air_temperature, GOST_air_temperature),
    GOST_atmosphere_pressure 	 = COALESCE(@GOST_atmosphere_pressure, GOST_atmosphere_pressure),

    GOST_Climate_factor 		 = COALESCE(@GOST_Climate_factor, GOST_Climate_factor	),
    GOST_rain_intensity 		 = COALESCE(@GOST_rain_intensity, GOST_rain_intensity),
      
---------------------------------------------------------------
--GOST
---------------------------------------------------------------
    
   NIIR1998_NAME 				= COALESCE(@NIIR1998_NAME, 				  NIIR1998_NAME	),				

   NIIR1998_gradient_diel 		= COALESCE(@NIIR1998_gradient_diel, 	  NIIR1998_gradient_diel),
   NIIR1998_gradient_deviation = COALESCE(@NIIR1998_gradient_deviation,  NIIR1998_gradient_deviation),


   NIIR1998_TERRAIN_TYPE 		= COALESCE(@NIIR1998_TERRAIN_TYPE,		  NIIR1998_TERRAIN_TYPE),
   NIIR1998_steam_wet 			= COALESCE(@NIIR1998_steam_wet,			  NIIR1998_steam_wet),

   NIIR1998_air_temperature 	= COALESCE(@NIIR1998_air_temperature, 	  NIIR1998_air_temperature),
   NIIR1998_atmosphere_pressure= COALESCE(@NIIR1998_atmosphere_pressure, NIIR1998_atmosphere_pressure),
     
   NIIR1998_rain_region_number = COALESCE(@NIIR1998_rain_region_number,  NIIR1998_rain_region_number),
   NIIR1998_Qd_region_number 	= COALESCE(@NIIR1998_Qd_region_number,	  NIIR1998_Qd_region_number)
   
            
  WHERE 
    id = @ID

/*

select id, air_temperature,
    COALESCE(@air_temperature	, air_temperature)
 from link where id=@id 
*/  
--  Alter table  property ENABLE trigger all



--return



  RETURN @@ROWCOUNT


END

GO
/****** Object:  View [LinkFreqPlan].[view_LinkFreqPlan_LinkEnd]    Script Date: 16.05.2020 10:54:58 ******/


IF OBJECT_ID(N'[geo].[fn_Length_m]') IS NOT NULL

DROP FUNCTION [geo].[fn_Length_m]
GO
/****** Object:  UserDefinedFunction [geo].[fn_Length_m]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [geo].[fn_Length_m] 

(
  @LAT1 float=64,
  @Lon1 float=37,

  @LAT2 float=64.1,
  @LON2 float=37.1 
 


)
RETURNS float
AS
BEGIN
  DECLARE 
--    @s varchar(max) = 'LINESTRING('+ cast(@lon1 as varchar(10)) + cast(@lon1 as varchar(10)) +','+  cast(@lon2 as varchar(10)) + cast(@lon2 as varchar(10)) +' )',
    @g geography;  


    
--  SET @g = geography::STGeomFromText('LINESTRING('+ cast(@lat1 as varchar(10)) +' '+ cast(@lon1 as varchar(10)) +','+  cast(@lon2 as varchar(10)) +' '+ cast(@lon2 as varchar(10)) +' )', 4326);  
  SET @g = geography::STGeomFromText('LINESTRING('+ cast(@lon1 as varchar(10)) +' '+ cast(@lat1 as varchar(10)) +','+  cast(@lon2 as varchar(10)) +' '+ cast(@lat2 as varchar(10)) +' )', 4326);  
  return round(@g.STLength(), 0);  

END


/*

DECLARE @g geography;  
SET @g = geography::STGeomFromText('LINESTRING(-122.360 47.656, -122.343 47.656)', 4326);  
SELECT @g.STLength();  

*/

GO
/****** Object:  UserDefinedFunction [geo].[fn_Length_m_Between_Property]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[geo].[fn_Length_m_Between_Property]') IS NOT NULL


DROP FUNCTION [geo].[fn_Length_m_Between_Property]
GO
/****** Object:  UserDefinedFunction [geo].[fn_Length_m_Between_Property]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [geo].[fn_Length_m_Between_Property]
(

  @ID1   int,
  @ID2   int

)
RETURNS float
AS
BEGIN

 DECLARE  
   
    @lat1 float,
    @lon1 float,
    
    @lat2 float,
    @lon2 float


  SELECT       
       @LAT1 = lat,
       @LON1 = lon              
  FROM Property
     WHERE (id = @ID1)  


  SELECT       
       @LAT2 = lat,
       @LON2 = lon              
  FROM Property
     WHERE (id = @ID2)   




  return  geo.fn_Length_m(@LAT1, @Lon1, @LAT2, @Lon2)
 

  /* Function body */
END

GO

IF OBJECT_ID(N'[lib].[link_status]') IS  NULL

begin

CREATE TABLE [lib].[link_status](
	[id] [int] NOT NULL,
	[name] [varchar](50) NULL,
 CONSTRAINT [link_status_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END

go
IF OBJECT_ID(N'[lib].[Template_Link]') IS  NULL

begin

CREATE TABLE [lib].[Template_Link](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[folder_id] [int] NULL,
	[linkendtype_id] [int] NULL,
	[mode] [int] NULL,
	[linkendtype_mode_id] [int] NULL,
	[AntennaType_id] [int] NULL,
	[height] [int] NULL,
	[guid] [uniqueidentifier] NULL DEFAULT (newid()),
	[date_created] [datetime] NULL DEFAULT (getdate()),
	[date_modify] [datetime] NULL,
	[user_modify] [varchar](50) NULL,
	[user_created] [varchar](50) NULL DEFAULT (suser_name()),
 CONSTRAINT [PK_Template_Link] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [Template_Link_uq] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
 
ALTER TABLE [lib].[Template_Link]  WITH CHECK ADD  CONSTRAINT [Template_Link_fk] FOREIGN KEY([linkendtype_id])
REFERENCES [dbo].[LinkEndType] ([id])

ALTER TABLE [lib].[Template_Link] CHECK CONSTRAINT [Template_Link_fk]

ALTER TABLE [lib].[Template_Link]  WITH CHECK ADD  CONSTRAINT [Template_Link_fk2] FOREIGN KEY([AntennaType_id])
REFERENCES [dbo].[AntennaType] ([id])

ALTER TABLE [lib].[Template_Link] CHECK CONSTRAINT [Template_Link_fk2]

ALTER TABLE [lib].[Template_Link]  WITH CHECK ADD  CONSTRAINT [Template_Link_fk3] FOREIGN KEY([linkendtype_mode_id])
REFERENCES [dbo].[LinkEndType_Mode] ([id])

ALTER TABLE [lib].[Template_Link] CHECK CONSTRAINT [Template_Link_fk3]

END

go


IF OBJECT_ID(N'[lib].[PickList_Values]') IS  NULL

begin

CREATE TABLE [lib].[PickList_Values](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[PickList_id] [int] NOT NULL,
	[code] [int] NULL,
	[value] [varchar](100) NOT NULL,
	[Display_Index] [int] NULL,
	[comment] [text] NULL,
 CONSTRAINT [PK_PickList_Values_PickList_Values] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_PickList_Values_name_PickList_Values] UNIQUE NONCLUSTERED 
(
	[value] ASC,
	[PickList_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


end

GO
/****** Object:  Table [lib].[PickList]    Script Date: 16.05.2020 10:54:58 ******/


IF OBJECT_ID(N'[lib].[PickList]') IS  NULL

begin

CREATE TABLE [lib].[PickList](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[table_name] [varchar](50) NOT NULL,
	[field_name] [varchar](50) NOT NULL,
	[is_system] [bit] NULL,
 CONSTRAINT [PickList_pk_PickList] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

end
 
GO

/****** Object:  StoredProcedure [lib].[sp_Template_Link_apply_to_link]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[_UpdateAllViews]') IS NOT NULL
DROP procedure [dbo].[_UpdateAllViews]
GO

------------------------------------------------------------------
CREATE PROCEDURE dbo._UpdateAllViews
------------------------------------------------------------------

AS
BEGIN

  
  DECLARE 
    @name varchar(200),
    @sql nvarchar(100);
  
  DECLARE 
    @temp table (name sysname);
     
    /*
  SELECT     
    TABLE_SCHEMA,
    TABLE_name,
  --  View_Definition,
    *
    
  FROM INFORMATION_SCHEMA.views    */



--TABLE_name
--TABLE_SCHEMA    
--View_Definition
 -- return



INSERT INTO @temp
	select TABLE_SCHEMA+'.'+ TABLE_name 	  
     FROM INFORMATION_SCHEMA.views    
       where TABLE_name not like '%11'
     
--   and o.NAME not LIKE '%11'
     
     
     
--	from sysobjects WHERE (type = 'V') 
/*
	select *
     FROM INFORMATION_SCHEMA.views    
     where table_schema='security'
     

*/




/*
INSERT INTO @temp
	select name  	  
	from sysobjects WHERE (type = 'V') 

*/

  WHILE exists(SELECT * FROM @temp)
  BEGIN
    SELECT top 1 @name=name FROM @temp
    delete from @temp where name=@name
  
    print @name


    exec sp_refreshview @name

-- exec ('select top 1 * from '+ @name)

--return

  END
 

END

go





IF OBJECT_ID(N'[lib].[sp_Template_Link_apply_to_link]') IS NOT NULL
DROP PROCEDURE [lib].[sp_Template_Link_apply_to_link]
GO
/****** Object:  StoredProcedure [lib].[sp_Template_Link_apply_to_link]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE [lib].[sp_Template_Link_apply_to_link]
(
  @property_id int,
  @Template_Link_id int
)
AS
BEGIN
  declare
    @AntennaType_id INT,
    @LinkEndType_id INT,
    @LinkEndType_mode_id INT


 IF @property_id is null 
 begin
 
/*
    update lib.Template_Link
    set 
      AntennaType_id = ( select top 1 id from AntennaType),
      linkendtype_id = ( select top 1 id from linkendtype)
*/

   set @property_id      = (select top 1 id from [property] )
   set @Template_Link_id = (select top 1 id from lib.Template_Link )
 
 end



select 
       @LinkEndType_id = LinkEndType_id,
       @LinkEndType_mode_id = LinkEndType_mode_id,
       @AntennaType_id = AntennaType_id

  from lib.Template_Link
  where id = @Template_Link_id
   

/*select *
  from Template_Site
*/


 IF @property_id is null 
  --     EXISTS(SELECT * FROM Link WHERE (LINKEND1_ID=@LINKEND2_ID) and (LINKEND2_ID=@LINKEND1_ID))
      RAISERROR ('@property_id', 16, 1);



  declare 
  --  @Template_Antenna_id int ,
--    @Template_LinkEnd_id int ,

   
    @Antenna_id int,
    @LinkEnd_id int 
   
/*--------------------------------------------------
select @Template_LinkEnd_id = id
  from Template_Site_LinkEnd
  where Template_Site_id = @Template_Site_id
*/
/*select *
  from Template_Site_LinkEnd
  where Template_Site_id = @Template_Site_id

*/

/*
IF @Template_LinkEnd_id is null    RAISERROR ('@Template_LinkEnd_id', 16, 1);
---------------------------------------------------
  
select @Template_Antenna_id = id
  from Template_Site_LinkEnd_Antenna
  where Template_Site_LinkEnd_id = @Template_LinkEnd_id
*/

/*select *
  from Template_Site_LinkEnd_Antenna
  where Template_Site_LinkEnd_id = @Template_LinkEnd_id
  
*/
  
--IF @Template_Antenna_id is null    RAISERROR ('@Template_Antenna_id', 16, 1);
  

--------------------------------------------------


  INSERT INTO linkend  (property_id, NAME) VALUES (@property_id,'���');     
  set @LinkEnd_id = IDENT_CURRENT ('linkend')   

  INSERT INTO Linkend_Antenna  (Linkend_id, property_id, NAME) VALUES (@Linkend_id, @property_id, 'Antenna');
  set @Antenna_id = IDENT_CURRENT ('Linkend_Antenna')   


--------------------------------------------------

  if @LinkEndType_mode_id is null 
    select top 1 @LinkEndType_mode_id = id 
      from LinkEndType_mode 
      WHERE LinkendType_id=@LinkendType_id
      order by bitrate_Mbps desc   



  UPDATE Linkend  
  SET 
      LinkendType_id      = t.LinkendType_ID,
      LinkEndType_mode_id = m.id,
--      bitrate_Mbps        = m.bitrate_Mbps
        mode                = m.mode,
--      bitrate_Mbps        = m.bitrate_Mbps
        
        bitrate_Mbps    = m.bitrate_Mbps,
                 
        MODULATION_TYPE =m.MODULATION_TYPE,
        SIGNATURE_HEIGHT=m.SIGNATURE_HEIGHT, 
        SIGNATURE_WIDTH =m.SIGNATURE_WIDTH,  
        THRESHOLD_BER_3 =m.THRESHOLD_BER_3,  
        THRESHOLD_BER_6 =m.THRESHOLD_BER_6,  
            
        channel_width_MHz = m.bandwidth, -- //_MHz,  
        
        POWER_dBm       = m.POWER_MAX,
        POWER_max       = m.POWER_MAX,
        
        tx_freq_mhz =  tp.freq_min,
        rx_freq_mhz =  tp.freq_min
        
    --   MODULATION_COUNT      = m.MODULATION_level_COUNT,
     --  GOST_53363_modulation = m.GOST_53363_modulation        
      
      
  --    LinkendType_mode_id = m.LinkendType_mode_ID 
    
   from 
      (select * from LinkEndType  WHERE id=@LinkEndType_id) tp,
      (select * from LinkEndType_mode  WHERE id=@LinkEndType_mode_id) m,
      (select * from lib.Template_Link WHERE id=@Template_Link_id) t     
   WHERE 
      Linkend.id=@LinkEnd_id    


 --RETURN @@IDENTITY;    
 

--------------------------------------------------

  UPDATE Linkend_Antenna  
  SET 
      antennaType_id  =m.AntennaType_ID,  

   --   band            =m.band,

      Gain            = a.Gain,
      Height          = m.Height,

      band            =a.band,
  
      Freq_MHz        =a.Freq,
--          Freq_MHz        =m.Freq_MHz,
      Diameter        =a.Diameter,
      Polarization_str=A.Polarization_str,
      Vert_Width      =A.Vert_Width,
      Horz_Width      =A.Horz_Width 
      
      
      
      
   --   Freq_MHz        =m.Freq,
     --     Freq_MHz        =m.Freq_MHz,
/*      Diameter        =m.Diameter,
      Polarization_str=m.Polarization_str,
     
      Vert_Width      =m.Vert_Width,
      Horz_Width      =m.Horz_Width 
*/      
   from 
      (select * from AntennaType       WHERE id=@AntennaType_id) a,     
      (select * from lib.Template_Link WHERE id=@Template_Link_id) m     
   WHERE 
      Linkend_Antenna.id=@Antenna_id       

--  INSERT INTO Template_Site_LinkEnd_Antenna  (template_site_linkend_id, NAME) VALUES (@LinkEnd_id,'�������');

--template_site_LINKEND

  RETURN @LinkEnd_id
 
  
END


GO
/****** Object:  UserDefinedFunction [lib].[ft_PickList]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[lib].[ft_PickList]') IS NOT NULL

DROP FUNCTION [lib].[ft_PickList]
GO
/****** Object:  UserDefinedFunction [lib].[ft_PickList]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [lib].[ft_PickList] 
(
  @TABLE_NAME VARCHAR(50) = 'link',
  @FIELD_NAME VARCHAR(100) = 'status'
 
)

--346447
--RETURNS varchar(50)

RETURNS @ret TABLE 
(
    id INT primary key,
    value varchar(100)
    
)

as

BEGIN

  insert into @ret
   SELECT code as id , value
    
  --    FROM view_PickList_Values
      FROM  --view_PickList_Values
      (
      SELECT dbo.PickList.table_name, dbo.PickList.field_name, dbo.PickList_Values.*
        FROM dbo.PickList_Values LEFT OUTER JOIN
              dbo.PickList ON dbo.PickList_Values.PickList_id = dbo.PickList.id
      ) M        
      
      WHERE (TABLE_NAME = @TABLE_NAME) and
            (FIELD_NAME = @FIELD_NAME) and
            
          code is not null      



	RETURN
  
END

GO
/****** Object:  View [link_calc].[view_Link_for_calc]    Script Date: 16.05.2020 10:54:58 ******/


IF OBJECT_ID(N'[link_calc].[view_Link_for_calc]') IS not NULL


DROP VIEW [link_calc].[view_Link_for_calc]
GO
/****** Object:  View [link_calc].[view_Link_for_calc]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [link_calc].[view_Link_for_calc]
AS
SELECT 
    dbo.Link.*, 

 

a1.lat AS lat1, 
a1.lon AS lon1, 
a2.lat AS lat2, 
a2.lon AS lon2, 



Property1.ground_height AS Property1_ground_height, 
Property2.ground_height AS Property2_ground_height,


Property1.name AS Property1_name, 
Property2.name AS Property2_name, 
  

--Property1.id AS Property1_id, 
--Property2.id AS Property2_id, 
  
  Link_Repeater.id as Link_Repeater_id
   
    
  
        
FROM         dbo.LinkEnd LinkEnd2 

              RIGHT OUTER JOIN dbo.LinkEnd LinkEnd1 
              
 --     RIGHT OUTER JOIN dbo.LinkEndType ON LinkEndType.id = LinkEnd1.LinkEndType_id  
      
      RIGHT OUTER JOIN dbo.Link ON LinkEnd1.id = dbo.Link.linkend1_id ON LinkEnd2.id = dbo.Link.linkend2_id 
     
     
      RIGHT OUTER JOIN dbo.Property Property2  ON LinkEnd2.Property_id = Property2.id 
    
      RIGHT OUTER JOIN dbo.Property Property1  ON LinkEnd1.Property_id = Property1.id 

--      LEFT OUTER JOIN dbo.LinkType ON dbo.Link.linkType_id = dbo.LinkType.id LEFT OUTER JOIN dbo.ClutterModel ON dbo.Link.clutter_model_id = dbo.ClutterModel.id
                 
       left OUTER JOIN dbo.Link_Repeater ON dbo.Link.id =  dbo.Link_Repeater.Link_ID
              
                 
              
              
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND1_ID) A1
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND2_ID) A2
              
     


/*
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd1.kratnostBySpace) AS LinkEnd1_kratnostBySpace, 
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd2.kratnostBySpace) AS LinkEnd2_kratnostBySpace, 
*/
  
--where Link.id=5591
             
               
--WHERE     dbo.Link.id =  240806

GO
/****** Object:  StoredProcedure [link_calc].[sp_Link_select_for_calc]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[link_calc].[sp_Link_select_for_calc]') IS  not NULL

DROP PROCEDURE [link_calc].[sp_Link_select_for_calc]
GO
/****** Object:  StoredProcedure [link_calc].[sp_Link_select_for_calc]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:  
 -  ���������� ��� ��������� � ������� ������ VIEW
--------------------------------------------------------------- */

CREATE PROCEDURE [link_calc].[sp_Link_select_for_calc]
(
	@ID int = 250295
)

AS
BEGIN
 -- declare 
  --  @objname varchar(10);
    
  ---------------------------------------------
  
--  SELECT @objname = objname 
 --   FROM  Link
  --  WHERE  (id = @ID)
    
    
--  if ( @objname = 'link'  ) or ( @objname is null )
   
 SELECT * FROM view_link_for_calc WHERE id=@ID
-- SELECT * FROM view_link WHERE id=@ID
    
    
--  else 
--    SELECT * FROM view_PMP_LINK WHERE id=@ID
  
  return @@rowcount
         
END

GO
/****** Object:  StoredProcedure [link_calc].[sp_Link_calc_results_SEL]    Script Date: 16.05.2020 10:54:58 ******/
IF OBJECT_ID(N'[link_calc].[sp_Link_calc_results_SEL]') IS  not  NULL


DROP PROCEDURE [link_calc].[sp_Link_calc_results_SEL]
GO
/****** Object:  StoredProcedure [link_calc].[sp_Link_calc_results_SEL]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [link_calc].[sp_Link_calc_results_SEL]
(
  @ID int =null

)

AS
BEGIN

if @id is null
--  set @id = (select top 1 id from   dbo.Link where calc_results_xml is not null)

  set @id = (select top 1 link_id from Link_Repeater where calc_results_xml_1 is not null )




declare 
  @calc_results_xml   varchar(max),
  @calc_results_xml_1 varchar(max),
  @calc_results_xml_2 varchar(max),
  @calc_results_xml_3 varchar(max),
  @Link_Repeater_id   int


SELECT  
  @calc_results_xml   = dbo.Link.calc_results_xml,     
  
  @calc_results_xml_1 = Link_Repeater.calc_results_xml_1,
  @calc_results_xml_2 = Link_Repeater.calc_results_xml_2,
  @calc_results_xml_3 = Link_Repeater.calc_results_xml_3,
  
  @Link_Repeater_id = Link_Repeater.id  
  
FROM         
    dbo.Link 
     left OUTER JOIN dbo.Link_Repeater ON dbo.Link.id =  dbo.Link_Repeater.Link_ID
              
where Link.id = @id    


-------------------------------------------------------------


  declare 
    @s varchar(max) 



/*  declare 
    @s varchar(max) = 

'  
<?xml version="1.0" encoding="windows-1251"?>
<?xml-stylesheet type="text/xsl" href=""?>

<Document>
  <GROUP caption="1.���������� �������� ������ �� ���������">
    <ITEM id="1" caption=" ����� ��������� [km]" value="24.173" value2="24.173"/>
    <ITEM id="2" caption=" ������� ������� ������� [GHz]" value="13" value2="13"/>
    <ITEM id="184" caption=" ��� ����������� (1-������������., 2-�������.)" value="2" value2="2"/>
    <ITEM id="185" caption=" ��� ��������� (0-��������., 1-��������.(�����),2-��������.(��))" value="0" value2="0"/>
    <ITEM id="181" caption=" �������� �������, 0-�����.; 1-���� �-53363; 2-����; 3-E-����.; 4-���" value="0" value2="0"/>
  </GROUP>
   <GROUP caption="6.���������� ������� ���������� � ���.������������ V���">
    <ITEM id="152" caption=" ����������� �������� ��� ��� ���� [%]" value="0.0125" value2="0.0125"/>
    <ITEM id="42" caption=" ������������� ����� ���� [km]" value="24.173" value2="24.173"/>
       <ITEM id="159" caption=" ���������� ����� �������� � ���.������������ [km]" value="11.3649" value2="12.8081"/>
    <ITEM id="160" caption=" ������� Ho ��� ���� ���������� ������������ � ���.������������ [m]" value="6.80594" value2="6.80594"/>
    <ITEM id="161" caption=" ���������� ������� H(g) � ���.������������ [m]" value="10.75687" value2="10.75687"/>
    <ITEM id="162" caption=" ������������� ������� p(g) � ���.������������" value="1.58051" value2="1.58051"/>
    <ITEM id="15" caption=" ��������� ���������� � �������� ������������ V��� [dB]" value="0" value2="0"/>
  </GROUP> 
</Document>
 
 '
*/


-- select @xml 
   
 
declare 
  @report TABLE (
      tree_id int IDENTITY (1,1),
      tree_parent_id int,
          
      id int,
      caption varchar(100),
          
      value float,
      value2 float        
  )
  
 select * into #report  from @report 
 
  
declare
  @id_new int


  ----------------------------------------------------------
  --= @calc_results_xml 
  ----------------------------------------------------------
if @Link_Repeater_id is not null
begin

   insert into #report (caption) values ('1 -> ������������')
   set @id_new = @@IDEntity

   exec link_calc.sp_Link_calc_results_add_xml_SEL @TREE_PARENT_ID =@id_new,   @XML_TEXT  = @calc_results_xml_1
   
   -------------------------
   insert into #report (caption) values ('������������ -> 2')
   set @id_new = @@IDEntity
   
   exec link_calc.sp_Link_calc_results_add_xml_SEL @TREE_PARENT_ID =@id_new,  @XML_TEXT  = @calc_results_xml_2

   -------------------------
   insert into #report (caption) values ('1 -> ������������')
   set @id_new = @@IDEntity

   exec link_calc.sp_Link_calc_results_add_xml_SEL @TREE_PARENT_ID =@id_new,  @XML_TEXT  = @calc_results_xml_3
      

end else 
   exec link_calc.sp_Link_calc_results_add_xml_SEL   @XML_TEXT  = @calc_results_xml
   
      

  ----------------------------------------------------------
  
 
select * from #report

  return @@ROWCOUNT

   
END

GO
/****** Object:  StoredProcedure [link_calc].[sp_Link_calc_results_add_xml_SEL]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[link_calc].[sp_Link_calc_results_add_xml_SEL]') IS  not  NULL

DROP PROCEDURE [link_calc].[sp_Link_calc_results_add_xml_SEL]
GO
/****** Object:  StoredProcedure [link_calc].[sp_Link_calc_results_add_xml_SEL]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [link_calc].[sp_Link_calc_results_add_xml_SEL]
(
  @TREE_PARENT_ID int=null,
  @XML_TEXT       varchar(max) -- varchar(max) 
)

AS
BEGIN
  
declare
  @s varchar(max),
  @id_new int,
  @xml xml, 
  @node XML     



declare 
  @temp_tbl TABLE (node XML,   node_caption varchar(100)) 

  ----------------------------------------------------------
  --= @calc_results_xml 
  ----------------------------------------------------------
 set @s = @XML_TEXT 
  
 set @s = replace( @s, '<?xml version="1.0" encoding="windows-1251"?>','') 
-- set @s = replace( @s, '<?xml-stylesheet type="text/xsl" href=""?>','') 
 
  
set @xml = @s


--delete from  @temp_tbl

insert into  @temp_tbl
  select 
       r.n.query('.') , 
       r.n.value('@caption', 'varchar(100)') 
    FROM @xml.nodes('(//GROUP)') AS r(n)
        
        
  
  ----------------------------------------------------------
  
  while exists(select * from @temp_tbl)
  begin
    select top 1 @node=node from @temp_tbl
    delete top (1) from @temp_tbl
    
    
    
    insert into #report (caption, tree_parent_id) 
       select r.n.value('@caption', 'varchar(100)'), @TREE_PARENT_ID
          FROM @node.nodes('(//GROUP)') AS r(n)
        
    set @id_new = @@IDEntity
      
    insert into #report (tree_parent_id, caption,id, value, value2)          
       select          
            @id_new, 
             r.n.value('@caption', 'varchar(100)'),
             r.n.value('@id',  'int'),
             r.n.value('@value',  'float'),
             r.n.value('@value2', 'float')        
          FROM @node.nodes('(//ITEM)') AS r(n)
  end  

/*
insert into t_res (is_folder, caption )
  select distinct true, GROUP_caption
    from t_data_xml;

insert into t_res (id, value, caption, tree_parent_id )
  select m.id, m.value, m.caption, t_res.tree_id
    from t_data_xml m
      left join t_res on m.GROUP_caption = t_res.caption;

    */
  ----------------------------------------------------------
  
--select * from @report

  return @@ROWCOUNT

   
END

GO
/****** Object:  UserDefinedFunction [link_calc].[ft_Linkend_dop_antenna]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[link_calc].[ft_Linkend_dop_antenna]') IS not NULL

DROP FUNCTION  [link_calc].[ft_Linkend_dop_antenna]
GO
/****** Object:  UserDefinedFunction [link_calc].[ft_Linkend_dop_antenna]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE FUNCTION [link_calc].[ft_Linkend_dop_antenna] 
(
  @LINKEND_ID int    
)

--346447
--RETURNS varchar(50)

RETURNS @ret TABLE 
(
   id int,
   height  float , 
   gain float
 
)

as

BEGIN
  if @LINKEND_ID is null
    set @LINKEND_ID = 560386




  IF 2 = (select count(*)  from Linkend_Antenna where LINKEND_ID = @LINKEND_ID ) 
  begin
  --  select top 1 *  from Linkend_Antenna where LINKEND_ID = @LINKEND_ID order by height 
    
    insert into @ret
        select top 1 id, height, gain  
          from Linkend_Antenna where LINKEND_ID = @LINKEND_ID order by height      
  end
  
    
  RETURN
  
END

GO
/****** Object:  UserDefinedFunction [link].[fn_LinkEnd_name]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[link].[fn_LinkEnd_name]') IS  not  NULL


DROP FUNCTION [link].[fn_LinkEnd_name]
GO
/****** Object:  UserDefinedFunction [link].[fn_LinkEnd_name]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [link].[fn_LinkEnd_name]  
(
  @PROPERTY_ID int,
  
  @NAME varchar(200) 
)
RETURNS varchar(200)
AS
BEGIN

declare
  @n int = 0,
  @s varchar(200) 

  
  while @n < 100
  begin
    set @s = @NAME + iiF(@n=0, '', cast(@n as varchar(10)) ) 
        
    if not Exists (select * from dbo.LinkEnd   where property_id = @property_id and name = @s )
      return @s     
    
    
    set @n = @n +1
    
  end    
      
  
  return null

  /* Function body */
END

GO
/****** Object:  UserDefinedFunction [link].[fn_LinkEnd_Antenna_Get_Link_ID]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[link].[fn_LinkEnd_Antenna_Get_Link_ID]') IS NOT NULL

DROP FUNCTION [link].[fn_LinkEnd_Antenna_Get_Link_ID]
GO
/****** Object:  UserDefinedFunction [link].[fn_LinkEnd_Antenna_Get_Link_ID]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
purpose: get LINK id 
used in:   
--------------------------------------------------------------- */
CREATE FUNCTION [link].[fn_LinkEnd_Antenna_Get_Link_ID]
(
  @ID int  
)
RETURNS int 
BEGIN
          
--    583580
declare
  @LINK_ID int


;with A as
(
select linkend_id 
  from Linkend_Antenna 
  where id=@ID
)
  select @LINK_ID = id from link
    where 
      linkend1_id in (select * from A) or
      linkend2_id in (select * from A)

  RETURN @link_id
END

GO
/****** Object:  UserDefinedFunction [link].[ft_Link_calc_results_basic]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[link].[ft_Link_calc_results_basic]') IS NOT NULL

DROP FUNCTION [link].[ft_Link_calc_results_basic]
GO
/****** Object:  UserDefinedFunction [link].[ft_Link_calc_results_basic]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE FUNCTION [link].[ft_Link_calc_results_basic] 
(
  @ID int   
 
)

--346447
--RETURNS varchar(50)


--link_calc.ft_Linkend_dop_antenna 

RETURNS @ret TABLE 
(
    Rx_LEVEL     float, --<ITEM id="190" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_dop float, --<ITEM id="179" value="-200" caption=" ������� �������� ������� �� ����� ��������� (���) [dBm]"/>
    
    Rx_LEVEL_back     float, --<ITEM id="190" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_back_dop float, --<ITEM id="179" value="-200" caption=" ������� �������� ������� �� ����� ��������� (���) [dBm]"/>


    Rx_LEVEL_str varchar(50), --<ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_back_str varchar(50), --<ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    
    SESR float,
    KNG  float,
       
    Param_33 float, -- 33 �������� ����������� [dBm]
    Param_35 float,  -- ����������� �������� ���.������� (���) [dB]
    Param_36 float, 
    Param_37 float,  
    Param_40 float,

    Site2_dop_gain float,

    Rx_Level_draft float,
    Rx_Level_dop_draft float
    
  --  calc_results_xml  XML
    
)

as

BEGIN

---------------------------
if @id is null
  set @id = 253557
--  set @id = (select top 1 id from Link where calc_results_xml is not null )
---------------------------

DECLARE  
  @dop_gain float,
  @is_profile_reversed bit
  
 

 SELECT @is_profile_reversed=IsNull(is_profile_reversed ,0)  FROM dbo.link  where id=@id

 SELECT @dop_gain = LE.gain
   FROM dbo.link L  cross apply link_calc.ft_Linkend_dop_antenna 
            (IIF(@is_profile_reversed=1, Linkend1_id, Linkend2_id)) LE
    where  L.id=@id


/*    cross apply link_calc.ft_Linkend_dop_antenna  (Linkend2_id) LE
   where 
    L.id=@id
*/

/*
 SELECT @dop_gain = LE.gain
   FROM dbo.link L  cross apply link_calc.ft_Linkend_dop_antenna  (Linkend2_id) LE
    where  L.id=@id
    */
  

--link_calc.ft_Linkend_dop_antenna 


/*    <GROUP caption=" 13.���������� ������� ������ ������� �� ����� ���������">
      <ITEM id="33" value="30" caption=" �������� ����������� [dBm]"/>
      <ITEM id="35" value="42.97999" caption=" ����������� �������� ���.������� (���) [dB]"/>
      <ITEM id="36" value="39.89999" caption=" ����������� �������� ���.������� (���) [dB]"/>
      <ITEM id="37" value="7" caption=" ��������� ������ � ��� (���) [dB]"/>
      <ITEM id="39" value="0" caption=" �������������� ������ �� ��������� [dB]"/>
      <ITEM id="40" value="-140.48499" caption=" ���������� ������� � ��������� ������������ [dB]"/>
      <ITEM id="48" value="5.0954" caption=" ���������� �� ������� V��� ��-�� ������������� [dB]"/>
      <ITEM id="49" value="0" caption=" ���������� �� ������� V��� ��-�� ��������� [dB]"/>
      <ITEM id="94" value="-0.36859" caption=" ���������� ��-�� ���������� � ����� V��� [dB]"/>
      <ITEM id="99" value="-0.01261" caption=" ��������� ��-�� ������� ������ V��� [dB]"/>
      <ITEM id="100" value="4.71419" caption=" ������� ���������� V��=V���+V���+V���+V��� [dB]"/>
      <ITEM id="190" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (���) [dBm]"/>
      <ITEM id="179" value="-200" caption=" ������� �������� ������� �� ����� ��������� (���) [dBm]"/>
      <ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    </GROUP>

*/


   
  

--select * from link.ft_Link_Calc_results (@id)


  declare 
  	@result varchar(50);
   
  
  insert into @ret (Rx_LEVEL) values (null)

;WITH M as
(
  select * from link.ft_Link_Calc_results (@id)
)
  update @ret
  set
    Rx_LEVEL     = (select top 1 round([value],1) from m where id=190 ), -- <ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_dop = (select top 1 round([value],1) from m where id=179 ), -- <ITEM id="179" value="-200" caption=" ������� �������� ������� �� ����� ��������� (���) [dBm]"/>

    Rx_LEVEL_back     = (select top 1 round(value2,1) from m where id=190 ), -- <ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_back_dop = (select top 1 round(value2,1) from m where id=179 ), -- <ITEM id="179" value="-200" caption=" ������� �������� ������� �� ����� ��������� (���) [dBm]"/>


    SESR     = (select top 1 round([value],1) from m where id=16 ),
    KNG      = (select top 1 round([value],1) from m where id=17 ),
    

    Param_33   = (select top 1 round([value],1) from m where id=33 ),
    Param_35   = (select top 1 round([value],1) from m where id=35 ),
    Param_36   = (select top 1 round([value],1) from m where id=36 ),

    Param_37   = (select top 1 round([value],1) from m where id=37 ),
    Param_40   = (select top 1 round([value],1) from m where id=40 ),

   -- calc_results_xml = (select calc_results_xml from link where id=@id),

    Site2_dop_gain  = @dop_gain


/*      <ITEM id="25" value="24.38664" caption="dop-site1-��������  dB"/>
      <ITEM id="26" value="-9" caption="dop-site1-������ ���"/>
      <ITEM id="27" value="8929.6979" caption="dop-site1-������ � ���"/>
      <ITEM id="28" value="1.40183" caption="dop-site1-������"/>
      <ITEM id="29" value="0.7747" caption="dop-site2-�������"/>
      <ITEM id="30" value="2.7431" caption="dop-site2-�������� dB"/>
      <ITEM id="31" value="24.42277" caption="dop-site2-������ ���"/>
      <ITEM id="32" value="8.90334" caption="dop-site2-������ � ���"/>
      <ITEM id="33" value="30" caption="dop-site2-������"/>

*/
--group_caption='1.���������� �������� ������ �� ���������'


update @ret
  set 
    Rx_LEVEL_dop      = IIF(Rx_LEVEL_dop=-200, null, Rx_LEVEL_dop),
    Rx_LEVEL_back_dop = IIF(Rx_LEVEL_back_dop=-200, null, Rx_LEVEL_back_dop),
    
    Rx_Level_draft = Param_33 + Param_35 + Param_36 - Param_37 + Param_40 ,

-- <ITEM id="30" value="2.7431" caption="dop-site2-�������� dB"/>
    Rx_Level_dop_draft = Param_33 + Param_35 + @dop_gain - Param_37 + Param_40 
    
    

 

update @ret
  set 
      Rx_LEVEL_str = cast(Rx_LEVEL as varchar(50)) + 
                     IIF(Rx_LEVEL_dop is null, '', ' / '+ cast(Rx_LEVEL_dop as varchar(50))),

      Rx_LEVEL_back_str = cast(Rx_LEVEL_back as varchar(50)) + 
                          IIF(Rx_LEVEL_back_dop is null, '', ' / '+ cast(Rx_LEVEL_back_dop as varchar(50)))



--select * from @ret



  RETURN
  

/*


  (*
//  DEF_LENGTH_KM    = 1;
  DEF_STATUS       = 7;  //i?eaiaiinou eioa?aaea

  DEF_SESR         = 17;
  DEF_KNG          = 19;

  DEF_KNG_REQ      = 18;
  DEF_SESR_REQ     = 16;
  DEF_LOS          = 31;
  DEF_Rx_LEVEL_dBm = 101; //Iaaeaiiue o?iaaiu neaiaea ia aoiaa i?eaiieea (aAi)

  DEF_MAX_WEAKNESS     = 14;  //Aiionoeiia ineaaeaiea (aA)
  DEF_DIRECT_LINE_TILT = 95;
  
   
  */
    
    
	RETURN
  
END
/*
<?xml version="1.0" encoding="windows-1251"?>
<Document>
  <REPORT>
    <GROUP caption="���������� �������� ������ �� ���������">
      <ITEM id="1" value="2.84799" caption="����� ��������� [km]"/>
      <ITEM id="2" value="7.495" caption="������� ������� ������� [GHz]"/>
      <ITEM id="3" value="35" caption="������ ������� ����� ������� (���) [m]"/>
      <ITEM id="4" value="45" caption="������ ������� ������ ������� (���) [m]"/>
      <ITEM id="6" value="21" caption="������ ������� ������ ������� (���) [m]"/>
      <ITEM id="211" value="76" caption="������� ������ ������ (���) H[m]"/>
      <ITEM id="230" value="0" caption="��� ���-�� �� ���.(0-�������.���� 800�,1-����.����� 800� � ����� 1200�,2-����������.����� 1200�)"/>
      <ITEM id="184" value="2" caption="��� ����������� (1-������������., 2-�������.)"/>
      <ITEM id="185" value="0" caption="��� ��������� (0-��������., 1-��������.(�����),2-��������.(��))"/>
      <ITEM id="245" value="0" caption="����.(����) ������ �����������"/>
      <ITEM id="181" value="2" caption="�������� �������, 0-�����.; 1-���� �-53363; 2-����; 3-E-����.; 4-���"/>
    </GROUP>
    <GROUP caption="���������� ������� ����������� ����������� ���������">
      <ITEM id="7" value="1" caption="������� ����������� �� ���� ��������� (1-�������,0-���)"/>
      <ITEM id="8" value="1" caption="������� ����������� �� �������� p(a%) (1-�������,0-���)"/>
      <ITEM id="9" value="1" caption="������� ����������� �� �������� V��� (1-�������,0-���)"/>
      <ITEM id="10" value="1" caption="������� ����������� �� �������� SESR (1-�������,0-���)"/>
      <ITEM id="11" value="1" caption="������� ����������� �� �������� K�� (1-�������,0-���)"/>
      <ITEM id="12" value="1" caption="���������� ������������� ������� p_dop"/>
      <ITEM id="13" value="8.89016" caption="������������ ������������� ������� p(a%)"/>
      <ITEM id="14" value="-36.85576" caption="����������   ���������� � ���.������������ V��� [dB]"/>
      <ITEM id="15" value="0" caption="������������ ���������� � ���.������������ V��� [dB]"/>
      <ITEM id="16" value="0.006" caption="����������   ���������� �������� �� ������� SESR [%]"/>
      <ITEM id="17" value="1E-5" caption="������������ ���������� �������� �� ������� SESR [%]"/>
      <ITEM id="18" value="0.0125" caption="����������   ���������� ������������ K�� [%]"/>
      <ITEM id="19" value="1E-5" caption="������������ ���������� ������������ K�� [%]"/>
    </GROUP>
    <GROUP caption="���������� ������� �������� ��� a% ���������">
      <ITEM id="26" value="-9" caption="������� �������� ��������� ����.�������������, 10^-8 [1/�]"/>
      <ITEM id="153" value="10.32577" caption="����������� ���������� ���������, 10^-8 [1/m]"/>
      <ITEM id="217" value="0.55193" caption="�����.���.��� ��������� y(R)"/>
      <ITEM id="172" value="20" caption="���������� ����������� a% ���.�������� ������ p_dop [%]"/>
      <ITEM id="20" value="-0.00145" caption="����������� �������� g ��� a% ���������, 10^-8 [1/m]"/>
      <ITEM id="21" value="6370.29472" caption="������������� ������ ����� [km] (��� a% ���������)"/>
      <ITEM id="22" value="1.00004" caption="����������� ��������� (��� a% ���������)"/>
      <ITEM id="23" value="0.7747" caption="���������� ����� �������� [km] (��� a% ���������)"/>
      <ITEM id="24" value="2.7431" caption="������� Ho ��� ���� ���������� ������������ [m] (��� a% ���������)"/>
      <ITEM id="25" value="24.38664" caption="���������� ������� H(g) [m] (��� a% ���������)"/>
      <ITEM id="13" value="8.89016" caption="������������� ������� p(g)=p(a%)"/>
    </GROUP>
    <GROUP caption="���������� ������� �������� ��� ������� ���������">
      <ITEM id="26" value="-9" caption="����������� ��������, 10^-8 [1/m]"/>
      <ITEM id="27" value="8929.6979" caption="������������� ������ ����� [km]"/>
      <ITEM id="28" value="1.40183" caption="����������� ���������"/>
      <ITEM id="29" value="0.7747" caption="���������� ����� �������� [km]"/>
      <ITEM id="30" value="2.7431" caption="������� Ho ��� ���� ���������� ������������ [m]"/>
      <ITEM id="31" value="24.42277" caption="���������� ������� H(g) [m]"/>
      <ITEM id="32" value="8.90334" caption="������������� ������� p(g)"/>
    </GROUP>
    <GROUP caption="���������� ������� ���.����������� ���������� V���">
      <ITEM id="33" value="30" caption="�������� ����������� [dBm]"/>
      <ITEM id="34" value="-92.5" caption="��������� �������� ������� �� ����� ��������� [dBm]"/>
      <ITEM id="35" value="36.7" caption="����������� �������� ���.������� (���) [dB]"/>
      <ITEM id="36" value="36.7" caption="����������� �������� ���.������� (���) [dB]"/>
      <ITEM id="37" value="40" caption="��������� ������ � ��� (���) [dB]"/>
      <ITEM id="38" value="155.9" caption="�������������� ��������� ��� [dB]"/>
      <ITEM id="39" value="0" caption="�������������� ������ �� ��������� [dB]"/>
      <ITEM id="40" value="-119.04423" caption="���������� ������� � ��������� ������������ [dB]"/>
      <ITEM id="14" value="-36.85576" caption="���������� ���������� ���������� V��� [dB]"/>
    </GROUP>
    <GROUP caption="���������� ������� ���������� � ���.������������ V���">
      <ITEM id="152" value="0.0125" caption="����������� �������� ��� ��� ���� [%]"/>
      <ITEM id="42" value="2.84799" caption="������������� ����� ���� [km]"/>
      <ITEM id="155" value="0.2" caption="���������� ����� K��, ������������� �������������"/>
      <ITEM id="156" value="0.01" caption="���������� �������� K�� � ���.������������ [%]"/>
      <ITEM id="43" value="29.18731" caption="����������� �������� ��� ������������, 10^-8 [1/m]"/>
      <ITEM id="157" value="3301.17477" caption="������������� ������ ����� � ���.������������[km]"/>
      <ITEM id="158" value="0.51823" caption="����������� ��������� � ���.������������"/>
      <ITEM id="159" value="0.7747" caption="���������� ����� �������� � ���.������������ [km]"/>
      <ITEM id="160" value="2.7431" caption="������� Ho ��� ���� ���������� ������������ � ���.������������ [m]"/>
      <ITEM id="161" value="24.26943" caption="���������� ������� H(g) � ���.������������ [m]"/>
      <ITEM id="162" value="8.84744" caption="������������� ������� p(g) � ���.������������"/>
      <ITEM id="15" value="0" caption="��������� ���������� � �������� ������������ V��� [dB]"/>
    </GROUP>
    <GROUP caption="���������� ������� ���������� �� ������� ��� �����.����.">
      <ITEM id="44" value="1" caption="��� ��������� (1-��������,2-������������,3-��������)"/>
      <ITEM id="45" value="0" caption="K��������� �������� ���������"/>
      <ITEM id="48" value="0" caption="���������� �� ������� V��� ��-�� ������������� [dB]"/>
      <ITEM id="49" value="0" caption="���������� �� ������� V��� ��-�� ��������� [dB]"/>
    </GROUP>
    <GROUP caption="���������� ������� ��������� ���������� � �����">
      <ITEM id="89" value="7.5" caption="���������� ��������� �������� ���� [g/m2]"/>
      <ITEM id="182" value="15" caption="����������� ������� [��.�������]"/>
      <ITEM id="183" value="1013" caption="����������� �������� [����]"/>
      <ITEM id="90" value="0.00755" caption="����������� ���������� ��� ��������� [dB/km]"/>
      <ITEM id="91" value="0.00343" caption="����������� ���������� ��� �������� ���� [dB/km]"/>
      <ITEM id="92" value="-0.02152" caption="���������� ��-�� ���������� � ��������� [dB]"/>
      <ITEM id="93" value="-0.00978" caption="���������� ��-�� ���������� � ������� ���� [dB]"/>
      <ITEM id="94" value="-0.03131" caption="���������� ��-�� ���������� � ����� V��� [dB]"/>
    </GROUP>
    <GROUP caption="���������� ������� ���������� ��-�� ������� ������">
      <ITEM id="95" value="0.92534" caption="���� ������� ������ (U) [*]"/>
      <ITEM id="98" value="0.85015" caption="������� ����������� ���������� �(U)"/>
      <ITEM id="97" value="-0.01421" caption="��������� ����������� ���������� F(f) [dB/km]"/>
      <ITEM id="99" value="-0.03442" caption="��������� ��-�� ������� ������ V��� [dB]"/>
    </GROUP>
    <GROUP caption="���������� ������� ������ ������� �� ����� ���������">
      <ITEM id="33" value="30" caption="�������� ����������� [dBm]"/>
      <ITEM id="35" value="36.7" caption="����������� �������� ���.������� (���) [dB]"/>
      <ITEM id="36" value="36.7" caption="����������� �������� ���.������� (���) [dB]"/>
      <ITEM id="37" value="40" caption="��������� ������ � ��� (���) [dB]"/>
      <ITEM id="39" value="0" caption="�������������� ������ �� ��������� [dB]"/>
      <ITEM id="40" value="-119.04423" caption="���������� ������� � ��������� ������������ [dB]"/>
      <ITEM id="48" value="0" caption="���������� �� ������� V��� ��-�� ������������� [dB]"/>
      <ITEM id="49" value="0" caption="���������� �� ������� V��� ��-�� ��������� [dB]"/>
      <ITEM id="94" value="-0.03131" caption="���������� ��-�� ���������� � ����� V��� [dB]"/>
      <ITEM id="99" value="-0.03442" caption="��������� ��-�� ������� ������ V��� [dB]"/>
      <ITEM id="100" value="-0.06573" caption="������� ���������� V��=V���+V���+V���+V��� [dB]"/>
      <ITEM id="190" value="-55.70996" caption="������� �������� ������� �� ����� ��������� (���) [dBm]"/>
      <ITEM id="179" value="-55.69365" caption="������� �������� ������� �� ����� ��������� (���) [dBm]"/>
      <ITEM id="101" value="-55.69365" caption="������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
      <ITEM id="180" value="1" caption="T�� ���.��������� (0-��� ���.���.,1-����.,2-��������.,3-����.)"/>
    </GROUP>
    <GROUP caption="���������� ������� ���������� �������">
      <ITEM id="105" value="1.15909" caption="dV� ���������� ��-�� ���������.����� [dB]"/>
      <ITEM id="106" value="0" caption="dV� ���������� ��-�� ���. �������� [dB]"/>
      <ITEM id="107" value="1.15909" caption="dV���� ��������� ���������� ��-�� ������� �������� �������� [dB]"/>
    </GROUP>
    <GROUP caption="������� V���.��.�� ����������� ����.��� ������.�����.">
      <ITEM id="118" value="18" caption="������� ������� (�������� ��������) [Mbit/s]"/>
      <ITEM id="119" value="0" caption="��� ���������, 0-PSK (QPSK), 1-QAM (TCM)"/>
      <ITEM id="120" value="4" caption="����� ������� ���������"/>
      <ITEM id="123" value="0" caption="I���  �������, �������� ������������ [dB]"/>
      <ITEM id="124" value="-59.25843" caption="V���.��.�� ���������� ����.��� ������.��������� [dB]"/>
    </GROUP>
    <GROUP caption="������ ���������� ���������� �������� ��������� ������.">
      <ITEM id="14" value="-36.85576" caption="V��� ��� ����� ������� ����� � ������. ��������� [dB]"/>
      <ITEM id="107" value="1.15909" caption="dV���� ��������� ���������� ��-�� ������� �������� �������� [dB]"/>
      <ITEM id="100" value="-0.06573" caption="V�� ������� �������� ��������� ���������� [dB]"/>
      <ITEM id="189" value="0" caption="dVp ������� �� ATPC (���.���.����.���)[dB]"/>
      <ITEM id="124" value="-59.25843" caption="V���.��.�� ���������� ����.��� ������.��������� [dB]"/>
      <ITEM id="125" value="-35.62838" caption="V���.�� ��� ����� ������� �����������. ��������� [dB]"/>
      <ITEM id="126" value="-36.79003" caption="V���.� ��� ����� ������� ����������. ��������� [dB]"/>
      <ITEM id="127" value="-35.64724" caption="V���.� ��� ����� ������� ���������� � ������ [dB]"/>
    </GROUP>
    <GROUP caption="���.������� ���������������� ������������ ��������������">
      <ITEM id="126" value="-36.79003" caption="V���.� ��� ����� ������� ����������. ��������� [dB]"/>
      <ITEM id="128" value="1" caption="�������� ������ ������� V����(g)=V���.� [dB]"/>
      <ITEM id="129" value="80" caption="����������� �������� g ��� ������������, 10^-8 [1/m]"/>
      <ITEM id="168" value="1795.37767" caption="������������� ������ ����� (��� ����.�����.) [km]"/>
      <ITEM id="169" value="0.28184" caption="����������� ��������� (��� ����.�����.)"/>
      <ITEM id="130" value="0" caption="��������� ���������� ��� ������������ V����(g) [dB]"/>
      <ITEM id="131" value="0" caption="To ���������������� ������������ �������������� [%]"/>
    </GROUP>
    <GROUP caption="���.������� ����������������� ������������ ��������-���">
      <ITEM id="125" value="-35.62838" caption="V���.�� ��� ����� ������� �����������. ��������� [dB]"/>
      <ITEM id="133" value="20.5" caption="������������� ������ K [10^-6]"/>
      <ITEM id="132" value="1" caption="Q-������ ������ �����������"/>
      <ITEM id="134" value="1.5" caption="�������� ������������ b (������� ������� F^b)"/>
      <ITEM id="135" value="1" caption="�������� ������������ c (���.����. �*Q*c)"/>
      <ITEM id="136" value="3" caption="�������� ������������ d (������� ����� R^d)"/>
      <ITEM id="234" value="97.61425" caption="����.���������.�������.�����.��� ��� (���)"/>
      <ITEM id="235" value="1" caption="����.���������.�������.�����.��� ��� (���)"/>
      <ITEM id="236" value="0" caption="T��� �����������. ������������ �������������� ��� ��� (���) [%]"/>
      <ITEM id="237" value="0" caption="T��� �����������. ������������ �������������� ��� ��� (���) [%]"/>
      <ITEM id="176" value="3806713.87996" caption="���������� �����.��-�� ������������� �� ���� ��� [���]"/>
      <ITEM id="163" value="0.00971" caption="P��� ����������� �����������.��������� � ������ ������� ������ � ������ [%]"/>
      <ITEM id="137" value="0" caption="T��� �����������. ������������ �������������� [%]"/>
    </GROUP>
    <GROUP caption="���.������� ������������ �������������� ��-�� ������">
      <ITEM id="127" value="-35.64724" caption="V���.� ��� ����� ������� ���������� � ������ [dB]"/>
      <ITEM id="213" value="1" caption="����� ������ �� ����� ������. ������ (����-98)"/>
      <ITEM id="212" value="399.96337" caption="������������� ����� (��� ������� ���������� = V���.�) [mm/h] (����-98)"/>
      <ITEM id="139" value="0" caption="��� ����������� (0-��������������,1-������������,2-�����)"/>
      <ITEM id="140" value="1.33794" caption="����������� a"/>
      <ITEM id="141" value="0.00361" caption="����������� K"/>
      <ITEM id="154" value="10.94715" caption="�������� ��������� � ����� K*I^a[dB/km]"/>
      <ITEM id="142" value="1.64786" caption="����������� ������������� ��������� ����������� [km]"/>
      <ITEM id="143" value="0.5786" caption="����������� ���������������� ����������� �����"/>
      <ITEM id="214" value="1" caption="����� ������ �� ����� Qd"/>
      <ITEM id="216" value="0" caption="T�.� �������� ������������ �������������� �� ������ ����� [%]"/>
      <ITEM id="145" value="0" caption="T� �������� ������������ �������������� [%]"/>
    </GROUP>
    <GROUP caption="���������� ������� ������������ �������������� ��� SESR">
      <ITEM id="193" value="999.95883" caption="�����.����.��������� Cm (���)"/>
      <ITEM id="194" value="999.97684" caption="�����.����.��������� Cm (���)"/>
      <ITEM id="195" value="11.60021" caption="��������� �������� ������������ ���.��������� Tm.� (���)[c]"/>
      <ITEM id="196" value="11.62213" caption="��������� �������� ������������ ���.��������� Tm.� (���)[c]"/>
      <ITEM id="222" value="11.60021" caption="��������� �������� ������������ ���.��������� Tm.� [c]"/>
      <ITEM id="197" value="5.96214" caption="��� ������������ ���.��������� (���)[c]"/>
      <ITEM id="198" value="5.96295" caption="��� ������������ ���.��������� (���)[c]"/>
      <ITEM id="223" value="4.81675" caption="��� ������������ ���.��������� [c]"/>
      <ITEM id="225" value="0.06932" caption="���� ���.��������� � ����.����� 10� (���)"/>
      <ITEM id="226" value="0.06911" caption="���� ���.��������� � ����.����� 10� (���)"/>
      <ITEM id="227" value="0.06921" caption="���� ���.��������� � ����.����� 10� (��)"/>
      <ITEM id="228" value="0.10695" caption="���� ���.��������� � ����.����� 10�"/>
      <ITEM id="146" value="0" caption="SESR.� ���������������� ������������ [%]"/>
      <ITEM id="148" value="0" caption="SESR.� ����������������� ������������ [%]"/>
      <ITEM id="147" value="0" caption="SESR.� �������� ������������ [%]"/>
      <ITEM id="17" value="1E-5" caption="�������� ���������� �������� �� ������� SESR [%]"/>
      <ITEM id="16" value="0.006" caption="����������� �������� SESR �� ��������� [%]"/>
    </GROUP>
    <GROUP caption="���������� ������� ������������ �������������� ��� ���">
      <ITEM id="202" value="0.25" caption="�����.��������� ���.�(�) �� ����������� ������ � ��������� ����"/>
      <ITEM id="224" value="0.89304" caption="���� ���.��������� � ����.����� 10�"/>
      <ITEM id="199" value="0.93067" caption="���� ���.��������� � ����.����� 10� (���)"/>
      <ITEM id="200" value="0.93088" caption="���� ���.��������� � ����.����� 10� (���)"/>
      <ITEM id="201" value="0.93078" caption="���� ���.��������� � ����.����� 10� (��)"/>
      <ITEM id="149" value="0" caption="���.� ���������������� ������������ [%]"/>
      <ITEM id="151" value="0" caption="���.� ����������������� ������������ [%]"/>
      <ITEM id="150" value="0" caption="���.� �������� ������������ [%]"/>
      <ITEM id="170" value="0" caption="��� ���-1 (�����) [%]"/>
      <ITEM id="171" value="0" caption="��� ���-2 (������) [%]"/>
      <ITEM id="19" value="1E-5" caption="�������� ���������� ������������ K�� [%]"/>
      <ITEM id="18" value="0.0125" caption="����������� �������� K�� �� ��������� [%]"/>
    </GROUP>
  </REPORT>
</Document>
*/

GO
/****** Object:  UserDefinedFunction [link].[ft_Link_Calc_results]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[link].[ft_Link_Calc_results]') IS NOT NULL

DROP FUNCTION [link].[ft_Link_Calc_results]
GO
/****** Object:  UserDefinedFunction [link].[ft_Link_Calc_results]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [link].[ft_Link_Calc_results]
(
  @ID int =null
)
      
RETURNS @ret TABLE 
(    
  group_caption nvarchar(100),
  [caption] 	nvarchar(100),
  id 		    int,
  [value] 		float,
  [value2]		float

)

as

BEGIN

---------------------------
if @id is null
  set @id = (select top 1 id from Link where calc_results_xml is not null )
---------------------------


declare   
  @calc_results_xml varchar(max)  =  (SELECT calc_results_xml  FROM dbo.Link where id = @id) , 
  @xml xml -- =  (SELECT cast(calc_results_xml as XML) FROM dbo.Link where id = @id) 


set @xml = replace (@calc_results_xml, '<?xml version="1.0" encoding="windows-1251"?>', '<?xml version="1.0"?>')


insert into @ret
  select
 
       r.n.value('../@caption', 'nvarchar(100)'),
       r.n.value('@caption', 'nvarchar(100)'),
       r.n.value('@id',  'int'),
       r.n.value('@value',  'float'),
       r.n.value('@value2', 'float')        
     FROM @xml.nodes('(//ITEM)') AS r(n)


  RETURN
  
END

GO
/****** Object:  View [link].[view_Link_summary]    Script Date: 16.05.2020 10:54:58 ******/


IF OBJECT_ID(N'[link].[view_Link_summary]') IS NOT NULL

DROP VIEW [link].[view_Link_summary]
GO
/****** Object:  View [link].[view_Link_summary]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [link].[view_Link_summary]
-- added: calc_method

AS
SELECT 

a1.lat AS lat1, 
a1.lon AS lon1, 
a2.lat AS lat2, 
a2.lon AS lon2, 


--Round(100-dbo.Link.Kng,2) as Reliability,
  FORMAT ( 100 - dbo.Link.Kng , 'N5')  as Reliability,  -- ���������� 


dbo.Link.Is_CalcWithAdditionalRain,

  dbo.Link.calc_method,  
  dbo.Link.id, 
  dbo.Link.rx_level_dBm, 
  dbo.Link.SESR_norm,
  dbo.Link.KNG_norm, 
  dbo.Link.fade_margin_dB, 
  
 -- dbo.Link.SESR, 
  --cast(dbo.Link.SESR as varchar(10)) as SESR_str, 
  
  FORMAT(dbo.Link.SESR, 'N5') as SESR, 

---  dbo.Link.Kng, 

  FORMAT(dbo.Link.Kng, 'N5') as Kng, 
  
  
    
  FORMAT ( 100 - dbo.Link.Kng , 'N5')  as K_gotovnost,
  
--  FORMAT (link.fn_K_gotovnost (dbo.Link.Kng, dbo.Link.SESR) , 'N5')  as K_gotovnost,
  
 dbo.Link.objname,

 dbo.Link.linkend1_id,
 dbo.Link.linkend2_id,

 dbo.Link.is_profile_reversed,


Link_Repeater.id as Link_Repeater_id,

LinkEnd1.property_id as property1_id,
LinkEnd2.property_id as property2_id,


  dbo.Link.clutter_model_id, 
  dbo.Link.refraction, 
  dbo.Link.refraction_2, 
  dbo.Link.profile_step, 
  dbo.Link.profile_XML, 

  dbo.Link.length,
  dbo.Link.length_km,

  dbo.Link.calc_results_XML, 

  
  dbo.Link.status, 
  dbo.Link.status_back,
  dbo.Link.kng_year, 
  dbo.Link.NFrenel,
  
  dbo.Link.rain_intensity_extra, 
  dbo.Link.rain_IsCalcLength,  
  dbo.Link.rain_Weakening_vert, 
  

  LinkEnd1.bitrate_Mbps, 
  LinkEnd1.tx_freq_MHz, 
  LinkEnd1.band, 
  
  LinkEnd1.modulation_type, 
  LinkEnd1.channel_width_MHz,
  

  
  dbo.Link.rain_Weakening_horz, 
  dbo.Link.Rain_Length_km,
  dbo.Link.BER_required,
  dbo.Link.rain_Allowable_Intense_vert,
  dbo.Link.rain_signal_quality, 
  dbo.Link.rain_Allowable_Intense_horz,
  
  dbo.fn_LinkEnd_GetPolarizationStr(dbo.Link.linkend1_id) AS  Polarization_Str, 
  
  dbo.fn_Get_Space_spread_m(LinkEnd1.tx_freq_MHz) AS Space_spread_m, 
  
  dbo.fn_Get_Frenel_max_m( LinkEnd1.tx_freq_MHz,  dbo.Link.NFrenel, dbo.Link.length)  AS Frenel_max_m, 
                      
  dbo.fn_BerToStr(dbo.Link.BER_required) AS  BER_required_str, 
                      
  dbo.fn_PickList_GetValueByCode('link', 'status', dbo.Link.status) AS status_str, 
  dbo.fn_PickList_GetValueByCode('link', 'status', dbo.Link.status) AS status_name,
  
  Link_status.name as status_str1,
  Link_status.name as status_name1
  
                      
FROM dbo.Link LEFT JOIN
     dbo.LinkEnd LinkEnd1 ON dbo.Link.linkend1_id = LinkEnd1.id
     LEFT  JOIN
     dbo.LinkEnd LinkEnd2 ON dbo.Link.linkend2_id = LinkEnd2.id
     
       left OUTER JOIN dbo.Link_Repeater ON dbo.Link.id =  dbo.Link_Repeater.Link_ID
       
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND1_ID) A1
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND2_ID) A2

     LEFT  JOIN  lib.Link_status  Link_status ON Link_status.id = Link.status
     

/*where Link.id = 250295
*/

GO
/****** Object:  StoredProcedure [link].[sp_Link_Summary]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[link].[sp_Link_Summary]') IS NOT NULL

DROP PROCEDURE [link].[sp_Link_Summary]
GO
/****** Object:  StoredProcedure [link].[sp_Link_Summary]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE [link].[sp_Link_Summary]
(
	@ID int = 5610
)
AS
BEGIN
 -- set @ID=79793

 -- TEST
 --SELECT top 1 @id= id FROM link 


  --------------------------------------------------------------
  IF @ID IS NULL  RAISERROR ('@ID is NULL', 16, 1);
  --------------------------------------------------------------


SELECT * FROM link.view_link_summary WHERE id=@ID 


/*    
  IF EXISTS(SELECT * FROM LINK WHERE (id=@ID) and (linkend1_id>0))    
    SELECT * FROM view_link_summary WHERE id=@ID 
  ELSE
  
  IF EXISTS(SELECT * FROM LINK WHERE (id=@ID) and (pmp_terminal_id>0))
    SELECT * FROM view_pmp_link_summary WHERE id=@ID 
*/ 

     
  RETURN @@ROWCOUNT
  
  
  
  
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Link_Add]    Script Date: 16.05.2020 10:54:58 ******/

/****** Object:  View [dbo].[view_Link_base]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[view_Link_base]') IS not  NULL

DROP VIEW [dbo].[view_Link_base]
GO
/****** Object:  View [dbo].[view_Link_base]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */

CREATE VIEW [dbo].[view_Link_base]
AS
SELECT 
  dbo.Link.project_id,

  dbo.Link.id,
  dbo.Link.name,

--  dbo.Link.project_id,
  dbo.Link.guid,
  
   dbo.Link.folder_id,
--  dbo.Link.user_created,
  
  linkend1_id,
  linkend2_id,


  Property1.id AS Property1_id,
  Property2.id AS Property2_id
  
/*  Property1.georegion_id AS georegion1_id,
  Property2.georegion_id AS georegion2_id
*/
  
FROM
  dbo.Link

  left JOIN dbo.linkend linkend1 ON dbo.Link.linkend1_id = linkend1.id
  left JOIN dbo.linkend linkend2 ON dbo.Link.linkend2_id = linkend2.id

  left JOIN dbo.Property Property1 ON linkend1.Property_id = Property1.id
  left JOIN dbo.Property Property2 ON linkend2.Property_id = Property2.id

  
WHERE
  Link.objname = 'link' OR 
  Link.objname IS NULL

GO
/****** Object:  View [dbo].[view_Link_report]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[view_Link_report]') IS  not  NULL


DROP VIEW [dbo].[view_Link_report]
GO
/****** Object:  View [dbo].[view_Link_report]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_Link_report]
AS
SELECT     dbo.Link.*, 


Round(100-dbo.Link.Kng,2) as Reliability,


a1.lat AS lat1, 
a1.lon AS lon1, 
a2.lat AS lat2, 
a2.lon AS lon2, 




/*Property1.georegion_id AS georegion1_id, 
Property2.georegion_id AS georegion2_id,
*/


-- Property1
Property1.address AS Property1_address, 
Property2.address AS Property2_address,

Property1.name AS Property1_name, 
Property2.name AS Property2_name, 

--Property1.name AS [Property1_name], 
--Property2.name AS [Property2_name], 

--Property1.id AS Property1_id, 
--Property2.id AS Property2_id, 


Property1.code AS Property1_code, 
Property2.code AS Property2_code, 


Property1.ground_height AS Property1_ground_height, 
Property2.ground_height AS Property2_ground_height,

GeoRegion_1.name AS [Property1_GeoRegion_name], 
GeoRegion_2.name AS [Property2_GeoRegion_name], 


Property1.Name_ERP AS [Property1_name_ERP], 
Property1.name_SDB AS [Property1_name_SDB], 

    

  --LinkEnd1.Band, -- AS LinkEnd1_LinkID,        


  LinkEnd1.LinkID AS LinkEnd1_LinkID,        
  LinkEnd2.LinkID AS LinkEnd2_LinkID,        

  LinkEnd1.band AS band,        
  
  LinkEnd1.bitrate_Mbps AS bitrate_Mbps,  
  LinkEnd1.tx_freq_MHz AS tx_freq_MHz, 
  LinkEnd1.rx_freq_MHz AS rx_freq_MHz, 


/*
-- ��������������
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd1.kratnostBySpace) AS LinkEnd1_kratnostBySpace, 
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd2.kratnostBySpace) AS LinkEnd2_kratnostBySpace, 

*/

ant1.diameter AS antenna1_diameter, 
ant2.diameter AS antenna2_diameter, 

ant1.height AS antenna1_height, 
ant2.height AS antenna2_height, 

ant1.polarization AS antenna1_polarization, 
ant2.polarization AS antenna2_polarization, 

  
        
  dbo.ClutterModel.name AS Clutter_Model_Name, 
        
  dbo.LinkType.name AS LinkType_Name, 
--  dbo.Status.name AS status_name, 
  dbo.Status.name AS State_name, 

--  dbo.Status.name AS status_str, 

      
link_status_tbl.value as status_str, 
 
link_calc_method_tbl.value as calc_method_name, 


--     left OUTER JOIN [lib].ft_PickList ('link', 'calc_method')  as link_calc_method_tbl   on link_calc_method_tbl.id = Link.calc_method

 
  
/*        
  dbo.fn_PickList_GetValueByCode('link', 'status',   dbo.Link.status) AS status_str, 
  dbo.fn_PickList_GetValueByCode('link', 'GST_type',  dbo.Link.calc_method) AS GST_type_name, 
  dbo.fn_PickList_GetValueByCode('link', 'calc_method',  dbo.Link.calc_method) AS calc_method_name, 
  dbo.fn_PickList_GetValueByCode('link', 'LOS_status',  dbo.Link.LOS_status) AS LOS_status_name, 
  dbo.fn_PickList_GetValueByCode('link', 'terrain_type',  dbo.Link.terrain_type) AS terrain_type_name, 
  dbo.fn_PickList_GetValueByCode('link', 'underlying_terrain_type', dbo.Link.underlying_terrain_type) AS underlying_terrain_type_name ,
*/

  
  Link_Repeater.id as Link_Repeater_id,
  
  LinkEndType.name as LinkEndType_name,
  
  Link_calc_res.Rx_LEVEL_str,
  Link_calc_res.Rx_LEVEL_back_str,
  
  
  --    Rx_Level_draft = Param_33 + Param_35 + Param_36 - Param_37 + Param_40 

Site2_dop_gain,

  Link_calc_res.Rx_LEVEL_draft,
  Link_calc_res.Rx_LEVEL_dop_draft,
  

  cast(Link_calc_res.Rx_LEVEL_draft  as varchar(10)) + 
     COALESCE(' / ' + Cast(Link_calc_res.Rx_LEVEL_dop_draft as varchar(10)), '') as Rx_LEVEL_draft_str
  
        
/*  
     Rx_LEVEL     float, --<ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_dop float, --<ITEM id="179" value="-200" caption=" ������� �������� ������� �� ����� ��������� (���) [dBm]"/>
    
    Rx_LEVEL_back     float, --<ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_back_dop float, --<ITEM id="179" value="-200" caption=" ������� �������� ������� �� ����� ��������� (���) [dBm]"/>


    Rx_LEVEL_str varchar(50), --<ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_back_str varchar(50), --<ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    
    SESR float,
    KNG  float
*/
  
  
  
        
FROM         dbo.LinkEnd LinkEnd2 

              RIGHT OUTER JOIN dbo.LinkEnd LinkEnd1 
              
      RIGHT OUTER JOIN dbo.LinkEndType ON LinkEndType.id = LinkEnd1.LinkEndType_id  
      RIGHT OUTER JOIN dbo.Link ON LinkEnd1.id = dbo.Link.linkend1_id ON LinkEnd2.id = dbo.Link.linkend2_id 
      LEFT OUTER JOIN dbo.Status ON dbo.Link.status_id = dbo.Status.id 
      LEFT OUTER JOIN dbo.GeoRegion GeoRegion_2 
      RIGHT OUTER JOIN dbo.Property Property2 ON GeoRegion_2.id = Property2.georegion_id ON LinkEnd2.Property_id = Property2.id 
      LEFT OUTER JOIN dbo.GeoRegion GeoRegion_1 
      RIGHT OUTER JOIN dbo.Property Property1 ON GeoRegion_1.id = Property1.georegion_id ON LinkEnd1.Property_id = Property1.id 
      LEFT OUTER JOIN dbo.LinkType ON dbo.Link.linkType_id = dbo.LinkType.id LEFT OUTER JOIN dbo.ClutterModel ON dbo.Link.clutter_model_id = dbo.ClutterModel.id
                 
       left OUTER JOIN dbo.Link_Repeater ON dbo.Link.id =  dbo.Link_Repeater.Link_ID
              
                 
       OUTER apply fn_Linkend_Antenna_info(LinkEnd1.id) ant1
       OUTER apply fn_Linkend_Antenna_info(LinkEnd2.id) ant2
               
              
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND1_ID) A1
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND2_ID) A2
              
     
     left OUTER JOIN [lib].ft_PickList ('link', 'status')       as link_status_tbl        on link_status_tbl.id = Link.status
     left OUTER JOIN [lib].ft_PickList ('link', 'GST_type')     as link_GST_type_tbl      on link_GST_type_tbl.id = Link.GST_type
     left OUTER JOIN [lib].ft_PickList ('link', 'calc_method')  as link_calc_method_tbl   on link_calc_method_tbl.id = Link.calc_method
     left OUTER JOIN [lib].ft_PickList ('link', 'LOS_status')   as link_LOS_status_tbl    on link_LOS_status_tbl.id = Link.LOS_status

     left OUTER JOIN [lib].ft_PickList ('link', 'terrain_type') as link_terrain_types_tbl on link_terrain_types_tbl.id = Link.terrain_type

     left OUTER JOIN [lib].ft_PickList ('link', 'underlying_terrain_type') as link_underlying_terrain_type_tbl on link_underlying_terrain_type_tbl.id = Link.underlying_terrain_type

     left OUTER JOIN [lib].ft_PickList ('linkend', 'kratnostBySpace') as linkend_kratnostBySpace1_tbl on linkend_kratnostBySpace1_tbl.id = LinkEnd1.kratnostBySpace
     left OUTER JOIN [lib].ft_PickList ('linkend', 'kratnostBySpace') as linkend_kratnostBySpace2_tbl on linkend_kratnostBySpace2_tbl.id = LinkEnd2.kratnostBySpace
     
     
    OUTER apply link.[ft_Link_calc_results_basic] (Link.id) Link_calc_res


/*
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd1.kratnostBySpace) AS LinkEnd1_kratnostBySpace, 
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd2.kratnostBySpace) AS LinkEnd2_kratnostBySpace, 
*/
  
--where Link.id=253557
             
               
--WHERE     dbo.Link.id =  1683

GO
IF OBJECT_ID(N'[ui].[Filters]') IS  NULL

begin
 
CREATE TABLE [ui].[Filters](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[objname] [varchar](30) NULL,
	[name] [varchar](100) NULL,
	[comment] [text] NULL,
	[is_shared] [bit] NULL,
	[host_name] [varchar](30) NULL DEFAULT (host_name()),
	[sql_body] [text] NULL,
	[user_created] [varchar](50) NULL DEFAULT (suser_name()),
	[date_created] [datetime] NULL DEFAULT (getdate()),
 CONSTRAINT [Filters_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

end

GO
/****** Object:  StoredProcedure [ui].[sp_Custom_Column_DEL]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[ui].[sp_Custom_Column_DEL]') IS  not NULL

DROP PROCEDURE [ui].[sp_Custom_Column_DEL]
GO
/****** Object:  StoredProcedure [ui].[sp_Custom_Column_DEL]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------------------------------------------------
CREATE PROCEDURE [ui].[sp_Custom_Column_DEL]
------------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(100)
)
AS
BEGIN


IF OBJECT_ID(@TABLE_NAME) IS NULL
begin
  print 'OBJECT_ID(@TableName) is null):' + @TABLE_NAME
  RETURN -1;
end


declare 
  @s varchar(max) = 'ALTER TABLE '+ @TABLE_NAME +' DROP COLUMN  '+ @COLUMN_NAME 


print @s
exec (@s)

----------------------------------------

declare 
  @LIB_TABLE_NAME varchar(100) = 'lib.'+ + @TABLE_NAME + '_'+  @COLUMN_NAME  


IF OBJECT_ID(@LIB_TABLE_NAME) IS NULL
begin
  set  @s = 'drop TABLE '+ @LIB_TABLE_NAME
  
  print @s
  exec (@s)

end



end

GO
/****** Object:  StoredProcedure [ui].[sp_Custom_Column_INS]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[ui].[sp_Custom_Column_INS]') IS  not  NULL

DROP PROCEDURE [ui].[sp_Custom_Column_INS]
GO
/****** Object:  StoredProcedure [ui].[sp_Custom_Column_INS]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------------------------------------------------
CREATE PROCEDURE [ui].[sp_Custom_Column_INS]
------------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(100),

  @COLUMN_DESC VARCHAR(200),

  @COLUMN_TYPE VARCHAR(50),
  @COLUMN_SIZE int,
  
-- int, float, bit,  varchar(n),  list 

  @DEFAULT VARCHAR(50)=null
 
)
AS
BEGIN

declare
@s   varchar(1000),
-- @null  varchar(50),
@def varchar(50)

set @def=''
if IsNull(@DEFAULT,'') <> ''
set @def = ' DEFAULT (' + @DEFAULT + ') '

/*
set @null=''
if ISNULL(@ISNULL,'') = ''
set @null = ' '
else
set @null = ' NOT NULL'
*/

IF OBJECT_ID(@TABLE_NAME) IS NULL
begin
  print 'OBJECT_ID(@TableName) is null):' + @TABLE_NAME
  RETURN -1;
end

/*------------------------
if dbo._ColumnExists(@TABLE_NAME, @COLUMN_NAME)=0
---------------------------
begin
set @s = 'ALTER TABLE '+ @TABLE_NAME +' ADD   '+ @COLUMN_NAME + ' ' + @COLUMN_TYPE  +' '+ @def  -- + @null
print @s
exec (@s)

--   set @s = 'ALTER TABLE '+ @TABLE_NAME +' ALTER COLUMN   '+ @COLUMN_NAME + ' ' + @COLUMN_TYPE  + @null
--  print @s
-- exec (@s)

print @@rowcount
end else
print 'column exists: ' + @TABLE_NAME +' - '+ @COLUMN_NAME
*/

end

GO
/****** Object:  StoredProcedure [ui].[sp_Filter_INS_UPD]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[ui].[sp_Filter_INS_UPD]') IS  not NULL

DROP PROCEDURE [ui].[sp_Filter_INS_UPD]
GO
/****** Object:  StoredProcedure [ui].[sp_Filter_INS_UPD]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ui].[sp_Filter_INS_UPD]
(
   @ID int = null,
   ---------------------------
   
   @OBJNAME   varchar(100)=null,

   @NAME 	  varchar(100)=null,
   @is_shared bit = null
 --  @BODY 	  varchar(max)=null
   
   ----------------------------
   --@ACTION varchar(10)=null
   
)
AS
BEGIN
 -- if @ACTION = 'select' 
  --  select * from Filters where OBJNAME=@OBJNAME
  
  if @ID is null    
  begin
    insert into ui.Filters  (objname) values (@objname)

    set @id = @@IDENTITY  
  end
  
  

  
--  if @ID is not null    
    UPDATE 
      ui.Filters  
    SET 
      objname = COALESCE( @objname, objname),
      name 	  = COALESCE( @name, name),
   --   body    = COALESCE( @body, body),

      is_shared = COALESCE(@is_shared, is_shared)
    WHERE 
      id = @id;

 
  /* Procedure body */
END

GO
/****** Object:  StoredProcedure [ui].[sp_Filter_SEL]    Script Date: 16.05.2020 10:54:58 ******/
IF OBJECT_ID(N'[ui].[sp_Filter_SEL]') IS  not NULL


DROP PROCEDURE [ui].[sp_Filter_SEL]
GO
/****** Object:  StoredProcedure [ui].[sp_Filter_SEL]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ui].[sp_Filter_SEL]
(  
   @OBJNAME varchar(100)=null
)
AS
BEGIN
if @OBJNAME is null
  set @OBJNAME = 'AntennaType'


    select * ,
       cast( IIF(user_created = user_name(),0,1 ) as bit) as is_readonly
    
    	from ui.Filters 
    	where OBJNAME=@OBJNAME --and 
            --  (is_shared = 1 or user_created = user_name())
        



  /* Procedure body */
END

GO

/****** Object:  Table [history].[tables_for_history]    Script Date: 16.05.2020 10:54:58 ******/

  IF OBJECT_ID(N'[history].[history_log]') IS  NULL
  begin


CREATE TABLE [history].[history_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[action] [varchar](10) NULL,
	[table_name] [varchar](50) NULL,
	[record_id] [int] NULL,
	[project_id1111111] [int] NULL,
	[property_id111111111] [int] NULL,
	[user_created] [varchar](50) NULL DEFAULT (suser_name()),
	[date_created] [datetime] NULL DEFAULT (getdate()),
	[xml_deleted] [xml] NULL,
	[xml_inserted] [xml] NULL,
	[record_parent_id] [int] NULL,
	[record_parent_table_name] [varchar](50) NULL,
 CONSTRAINT [history__pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

/****** Object:  Index [history_log_idx]    Script Date: 16.05.2020 10:54:58 ******/
CREATE NONCLUSTERED INDEX [history_log_idx] ON [history].[history_log]
(
	[table_name] ASC,
	[record_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


end
GO
/****** Object:  Table [history].[tables_for_history]    Script Date: 16.05.2020 10:54:58 ******/

  IF OBJECT_ID(N'[history].[tables_for_history]') IS  NULL

  begin

 
CREATE TABLE [history].[tables_for_history](
	[table_name] [varchar](50) NOT NULL,
	[enabled] [bit] NULL DEFAULT ((1)),
	[parent_column_name] [varchar](50) NULL,
	[table_table_caption] [varchar](100) NULL,
	[use_project_id] [bit] NULL DEFAULT ((0)),
	[parent_id_sql] [varchar](50) NULL,
	[parent_table_name] [varchar](50) NULL,
 CONSTRAINT [table_name_pk] PRIMARY KEY CLUSTERED 
(
	[table_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

end

GO
/* Data for the 'history.tables_for_history' table  (Records 1 - 25) */


if  COL_LENGTH('history.tables_for_history','parent_id_sql')  is null
  ALTER TABLE history.tables_for_history
     ADD parent_id_sql varchar(50) 
GO

if  COL_LENGTH('history.tables_for_history','use_project_id')  is null
  ALTER TABLE history.tables_for_history
    ADD use_project_id bit DEFAULT 0 NULL
GO

---------------------------------------------------

if  COL_LENGTH('history.history_log','xml_deleted')  is null
  ALTER TABLE history.history_log
    ADD xml_deleted xml NULL
GO



if  COL_LENGTH('history.history_log','xml_inserted')  is null
  ALTER TABLE history.history_log
    ADD xml_inserted xml NULL
GO

if  COL_LENGTH('history.history_log','record_parent_id')  is null
  ALTER TABLE history.history_log
    ADD record_parent_id int NULL
GO
                                                              
if  COL_LENGTH('history.history_log','record_parent_table_name')  is null
  ALTER TABLE history.history_log
    ADD record_parent_table_name varchar(50) COLLATE Cyrillic_General_CI_AS NULL
GO



delete from history.tables_for_history 
go


INSERT INTO history.tables_for_history (table_name, enabled, parent_column_name, table_table_caption, use_project_id, parent_id_sql, parent_table_name)
VALUES 
  (N'AntennaType', 1, NULL, N'������ ������', 0, NULL, NULL),
  (N'CalcModel', 1, NULL, N'������ �������', NULL, NULL, NULL),
  (N'CalcModelParams', 1, NULL, N'������ ������� - ���������', NULL, NULL, NULL),
  (N'CellLayer', 1, NULL, NULL, 0, NULL, NULL),
  (N'ColorSchema', 1, NULL, N'�������� �����', NULL, NULL, NULL),
  (N'ColorSchemaRanges', 1, N'color_schema_id', NULL, NULL, NULL, N'ColorSchema'),
  (N'lib.Template_Link', 1, NULL, N'lib.Template_Link', 0, NULL, NULL),
  (N'link', 1, NULL, N'������������� �������� (���)', 1, NULL, NULL),
  (N'Link_Repeater', 1, NULL, N'������������', NULL, NULL, NULL),
  (N'Link_Repeater_Antenna', 1, N'Link_Repeater_id', N'������������ - �������', NULL, NULL, N'Link_Repeater'),
  (N'link_SDB', 1, NULL, N'��� SDB c ������� Super Dual Band', 1, NULL, NULL),
  (N'LinkEnd', 1, N'property_id', N'���', 1, NULL, N'property'),
  (N'Linkend_Antenna', 1, N'', N'�������', 1, N'[link].fn_LinkEnd_Antenna_Get_Link_ID (I.id)', N'dbo.Link'),
  (N'LinkEndType', 1, NULL, N'������������ ���', NULL, NULL, NULL),
  (N'LinkEndType_Band', 1, N'LinkEndType_id', N'������������ - ���������', NULL, NULL, N'LinkEndType'),
  (N'LinkEndType_Mode', 1, N'LinkEndType_id', N'������������ - ������', NULL, NULL, N'LinkEndType'),
  (N'LinkFreqPlan', 1, NULL, N'��������-��������������� ���� (���)', 1, NULL, NULL),
  (N'LinkLine', 1, NULL, N'������������� ����� (���)', 1, NULL, NULL),
  (N'LinkNet', 1, NULL, N'���� ������������� �����', 1, NULL, NULL),
  (N'pmp_site', 1, NULL, N'pmp_site', 0, NULL, NULL),
  (N'project', 1, NULL, N'������', NULL, NULL, NULL),
  (N'property', 1, NULL, N'��������', 1, NULL, NULL),
  (N'Template_Linkend', 1, NULL, N'Template_Linkend', 0, NULL, NULL),
  (N'TEMPLATE_LINKEND_ANTENNA', 1, NULL, N'TEMPLATE_LINKEND_ANTENNA', 0, NULL, NULL),
  (N'Template_pmp_site', 1, NULL, N'Template_pmp_site', 0, NULL, NULL)
GO




/****** Object:  View [history].[view_FK]    Script Date: 16.05.2020 10:54:58 ******/

  IF OBJECT_ID(N'[history].[view_FK]') IS  not  NULL
DROP VIEW [history].[view_FK]
GO
/****** Object:  View [history].[view_FK]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [history].[view_FK]
AS
SELECT 
    ccu.constraint_name AS SourceConstraint

   ,ccu.TABLE_SCHEMA AS Source_TABLE_SCHEMA
     ,ccu.table_name AS Source_Table_name
    ,ccu.column_name AS Source_Column
    
    ,kcu.TABLE_SCHEMA AS Target_Table_SCHEMA
    ,kcu.table_name AS Target_Table_name
    ,kcu.column_name AS Target_Column
    
FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu
    INNER JOIN INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
        ON ccu.CONSTRAINT_NAME = rc.CONSTRAINT_NAME 
    INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu 
        ON kcu.CONSTRAINT_NAME = rc.UNIQUE_CONSTRAINT_NAME

GO
/****** Object:  View [history].[view_columns]    Script Date: 16.05.2020 10:54:58 ******/

  IF OBJECT_ID(N'[history].[view_columns]') IS  not  NULL
DROP VIEW [history].[view_columns]
GO
/****** Object:  View [history].[view_columns]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [history].[view_columns]
AS

 select top 100 percent
 
col.*  , 


case
  when c.Column_name is not null then '(select name from '+
                                         FK.Target_Table_SCHEMA +'.'+ FK.Target_Table_name 
                                         +' m where m.id = '+ col.column_name +') as '+ FK.Target_Table_name + '_name'
  
  when col.data_type='float' then 'cast ('+ col.column_name+' as varchar(20)) as '+ col.column_name
  else col.column_name  
end    
 as xml_column_name,
 
 fk.Target_Table_SCHEMA,
 fk.Target_Table_name,
 fk.Target_Column,
 
 c.Column_name as fk_Column_name
  

 from  INFORMATION_SCHEMA.COLUMNS  col
    INNER JOIN sys.columns AS sc ON sc.object_id = object_id(col.table_schema + '.' + col.table_name) 
                                  AND sc.NAME = col.COLUMN_NAME
    inner join sys.objects obj on obj.object_Id=sc.object_Id 
  --  left join  sys.computed_columns cc on cc.name = col.COLUMN_NAME and cc.object_id = obj.object_Id

  left join history.view_FK FK on FK.Source_TABLE_SCHEMA=col.table_schema and 
                                  FK.Source_Table_name=col.table_name and 
                                  FK.Source_Column=col.column_name
                                  
  left join INFORMATION_SCHEMA.COLUMNS c on c.TABLE_SCHEMA=FK.Target_Table_SCHEMA and 
                                            c.Table_name=FK.Target_Table_name and                                        
                                            c.Column_name='name'
                                  
                                    

/*  ccu.constraint_name AS SourceConstraint
   ,ccu.TABLE_SCHEMA AS Source_TABLE_SCHEMA
   
     ,ccu.table_name AS Source_Table_name
    ,ccu.column_name AS Source_Column
    
    ,kcu.table_name AS Target_Table_name
    ,kcu.table_name AS Target_Table_SCHEMA
    ,kcu.column_name AS Target_Column
*/

where 
  obj.[type]='u'
  
  and col.DATA_TYPE<>'text'
  
--  and data_type='float'

  and col.COLUMN_NAME not like'[_][_]%'
  and col.COLUMN_NAME not like'%[_][_]'
  and col.COLUMN_NAME not like '[_]'
  and col.COLUMN_NAME not like'%[-]'
  and col.COLUMN_NAME not like'%[11]'

  and col.table_NAME not like'[_][_]%'
  and col.table_NAME not like'%[_][_]'
  and col.table_NAME not like'%[11]'
  


  and col.COLUMN_NAME not in
      (
--       'id', 
       'guid', 
       'date_modify',
       'date_created',
       'user_modify',
       'user_created'
--       'project_id'
      )


--  and cc.definition is null
  
  and sc.Is_computed<>1

order by 
  col.table_schema,
  col.table_name , 
  col.ordinal_position
  
  /*
  CREATE VIEW history.view_FK
AS
SELECT 
    ccu.constraint_name AS SourceConstraint
   ,ccu.TABLE_SCHEMA AS Source_TABLE_SCHEMA
   
     ,ccu.table_name AS Source_Table_name
    ,ccu.column_name AS Source_Column
    
    ,kcu.table_name AS Target_Table_name
    ,kcu.table_name AS Target_Table_SCHEMA
    ,kcu.column_name AS Target_Column
    
FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu
    INNER JOIN INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
        ON ccu.CONSTRAINT_NAME = rc.CONSTRAINT_NAME 
    INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu 
        ON kcu.CONSTRAINT_NAME = rc.UNIQUE_CONSTRAINT_NAME
*/

GO
/****** Object:  View [history].[view_COLUMNS_group]    Script Date: 16.05.2020 10:54:58 ******/

  IF OBJECT_ID(N'[history].[view_COLUMNS_group]') IS  not  NULL
DROP VIEW [history].[view_COLUMNS_group]
GO
/****** Object:  View [history].[view_COLUMNS_group]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [history].[view_COLUMNS_group]
AS
    
SELECT 
      
	 T.[TABLE_SCHEMA] +'.'+ T.TABLE_NAME as TABLE_NAME_full,

  SUBSTRING(
          (
              SELECT ','+  [xml_column_name] AS [text()]
                FROM view_columns C  
                WHERE 
                --     DATA_TYPE not in ('text') and 
                
                      C.TABLE_SCHEMA =  T.TABLE_SCHEMA and 
                      C.TABLE_NAME=  T.TABLE_NAME                       
              FOR XML PATH ('')
                ), 2, 10000000000) [sql_columns] 


  FROM  [INFORMATION_SCHEMA].[TABLES] T
  where TABLE_TYPE = 'BASE TABLE' and
  
        TABLE_NAME not in ('msfavorites') and 
        TABLE_NAME not like '%[11]' and
        TABLE_NAME not like '%[__]'

GO
/****** Object:  StoredProcedure [history].[sp_Clear_Log]    Script Date: 16.05.2020 10:54:58 ******/

  IF OBJECT_ID(N'[history].[sp_Clear_Log]') IS  not  NULL
DROP PROCEDURE [history].[sp_Clear_Log]
GO
/****** Object:  StoredProcedure [history].[sp_Clear_Log]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [history].[sp_Clear_Log]

AS
BEGIN

  DELETE FROM  history.history_log 


END

GO
/****** Object:  StoredProcedure [history].[sp_Create_triggers]    Script Date: 16.05.2020 10:54:58 ******/
  IF OBJECT_ID(N'[history].[sp_Create_triggers]') IS  not  NULL

DROP PROCEDURE [history].[sp_Create_triggers]
GO
/****** Object:  StoredProcedure [history].[sp_Create_triggers]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [history].[sp_Create_triggers]
(
  @TABLE_NAME_full VARCHAR(50)=null
  
)
AS
BEGIN


 if @TABLE_NAME_full is null 
--    set   @TABLE_NAME  = 'property'
--    set   @TABLE_NAME_full  = 'AntennaType'
--    set   @TABLE_NAME_full  = 'LinkEnd'
    set   @TABLE_NAME_full  = 'lib.Template_Link'
    
--    set   @TABLE_NAME_full  = 'test.AntennaType'
    
--    set   @TABLE_NAME  = 'AntennaType'

  if charindex('.',@TABLE_NAME_full)=0
    set @TABLE_NAME_full = 'dbo.'+@TABLE_NAME_full
 

  DECLARE 
    @parent_id_sql VARCHAR(max),
    @parent_table_name VARCHAR(max),
    
    @sql_insert_parent VARCHAR(max)='',
    @sql_select_parent VARCHAR(max)='',
    
    
    @sql_columns  VARCHAR(max),
    @sql VARCHAR(max), 
    @TriggerName VARCHAR(50) = @TABLE_NAME_full + '_Audit_tr'


  if OBJECT_ID(@TriggerName) IS NOT NULL  
    exec('DROP TRIGGER '+ @TriggerName)

  ---------------------------------------    
  -- create history table
  ---------------------------------------    
--print @TABLE_NAME_full

  select 
         @sql_columns = sql_columns
    FROM history.view_COLUMNS_group      
    where TABLE_NAME_full = @TABLE_NAME_full


select @parent_id_sql     = IsNull(parent_id_sql,''),
       @parent_table_name = IsNull(parent_table_name,'')
       
  from history.tables_for_history
  where table_name = @TABLE_NAME_full or  
        'dbo.' + table_name = @TABLE_NAME_full


IF @parent_id_sql <> ''
begin
  set @sql_insert_parent = ',record_parent_id, record_parent_table_name'  
  set @sql_select_parent = ', '+ @parent_id_sql + ', '''+ @parent_table_name +'''
  '

end


-------------------------------------------------
-- FOR INSERT
-------------------------------------------------
set @sql=
  'CREATE TRIGGER :TriggerName ON :table_name 
    FOR UPDATE,INSERT,DELETE AS  
    begin
      set nocount on

      SET ANSI_WARNINGS  ON  --| OFF }
  
      declare
        @xml_inserted XML,
        @xml_deleted XML
                       
        declare  @tbl_inserted TABLE (xml_data XML, id int, RecNo int)
        declare  @tbl_deleted  TABLE (xml_data XML, id int, RecNo int)

              set @xml_inserted = ( SELECT :columns
                                       from inserted ----  as Record     
                                       for xml  path (''Record''),  elements    
                                     )

              set @xml_deleted = ( SELECT :columns
                                       from deleted ----  as Record     
                                       for xml  path (''Record''),  elements     
                                     )
                                       
        insert into @tbl_inserted
           select t.c.query (''.'') as xml_data,
                  t.c.value(''(id/text())[1]'',''int'') as ID,  
                  row_number () over (order by (select 100) ) as RowNum
              from @xml_inserted.nodes(''*'') t(c)  
                                       
        insert into @tbl_deleted
           select t.c.query (''.'') as xml_data,
                  t.c.value(''(id/text())[1]'',''int'') as ID,  
                  row_number () over (order by (select 100) ) as RowNum
              from @xml_deleted.nodes(''*'') t(c)  

             
  
----------------------------------------
-- Update
----------------------------------------        
  IF EXISTS (select * from inserted) AND EXISTS (select * from deleted)
  BEGIN
    INSERT history.history_log (action, table_name,  record_id,  xml_inserted, xml_deleted :insert_parent )
      SELECT ''Update'','':table_name'',  I.id, I.xml_data, D.xml_data  :select_parent
       from @tbl_inserted I
          left join @tbl_deleted D on I.id=D.id         

 
    RETURN;
  END

----------------------------------------
--  delete
----------------------------------------
  IF EXISTS (select * from deleted) AND NOT EXISTS (select * from inserted)
  BEGIN
    INSERT history.history_log (action, table_name, record_id, xml_deleted :insert_parent )
      SELECT ''delete'','':table_name'',  id, xml_data    :select_parent 
        from @tbl_deleted I 
                
    RETURN;
  END

----------------------------------------
-- Update inserts
----------------------------------------
  IF EXISTS (select * from inserted) AND not EXISTS (select * from deleted)
  BEGIN
    INSERT history.history_log (action, table_name ,  record_id, xml_inserted  :insert_parent )
      SELECT ''insert'','':table_name'',  id, xml_data  :select_parent
           from @tbl_inserted  I
 

    RETURN;
  END
        
         
end ' 
    
      
   set @sql = Replace(@sql, ':columns',          @sql_columns)
--  set @sql = Replace(@sql, ':record_parent_id', @parent_column_name)
  set @sql = Replace(@sql, ':table_name',  	    @TABLE_NAME_full)
  set @sql = Replace(@sql, ':TriggerName',      @TriggerName)

  set @sql = Replace(@sql, ':insert_parent',    @sql_insert_parent)
  set @sql = Replace(@sql, ':select_parent',    @sql_select_parent)

/*  set @sql_insert_parent = ',record_parent_id, record_parent_table_name'  
  set @sql_select_parent = ', '+ @parent_id_sql + ', '+ @parent_table_name 
*/


--select  @sql


  execute  ( @sql)


END


   
    /*
    IF EXISTS (select * from inserted) AND NOT EXISTS (select * from deleted)
  BEGIN
    INSERT [AUDIT].[LOG_TABLE_CHANGES] ([CHG_TYPE], [SCHEMA_NAME], [OBJECT_NAME], [XML_RECSET])
    SELECT 'INSERT', '[ACTIVE]', '[CARS_BY_COUNTRY]', (SELECT * FROM inserted as Record for xml auto, elements , root('RecordSet'), type)
    RETURN;
  END

  -- Detect deletes
  IF EXISTS (select * from deleted) AND NOT EXISTS (select * from inserted)
  BEGIN
    INSERT [AUDIT].[LOG_TABLE_CHANGES] ([CHG_TYPE], [SCHEMA_NAME], [OBJECT_NAME], [XML_RECSET])
    SELECT 'DELETE', '[ACTIVE]', '[CARS_BY_COUNTRY]', (SELECT * FROM deleted as Record for xml auto, elements , root('RecordSet'), type)
    RETURN;
  END

  -- Update inserts
  IF EXISTS (select * from inserted) AND EXISTS (select * from deleted)
  BEGIN
    INSERT [AUDIT].[LOG_TABLE_CHANGES] ([CHG_TYPE], [SCHEMA_NAME], [OBJECT_NAME], [XML_RECSET])
    SELECT 'UPDATE', '[ACTIVE]', '[CARS_BY_COUNTRY]', (SELECT * FROM deleted as Record for xml auto, elements , root('RecordSet'), type)
    RETURN;
  END
  
----------------------------------------
--  -- Detect deletes
----------------------------------------
--  IF EXISTS (select * from deleted) AND NOT EXISTS (select * from inserted)
--  BEGIN
--    INSERT [AUDIT].[LOG_TABLE_CHANGES] ([CHG_TYPE], [SCHEMA_NAME], [OBJECT_NAME], [XML_RECSET])
--    SELECT  (SELECT * FROM deleted as Record for xml auto, elements , root(''RecordSet''), type)
--    RETURN;
--  END

----------------------------------------
-- Update inserts
----------------------------------------
--  IF EXISTS (select * from inserted) AND EXISTS (select * from deleted)
--  BEGIN
--    INSERT [AUDIT].[LOG_TABLE_CHANGES] ([CHG_TYPE], [SCHEMA_NAME], [OBJECT_NAME], [XML_RECSET])
--    SELECT  (SELECT * FROM deleted as Record for xml auto, elements , root(''RecordSet''), type)
--    RETURN;
--  END


 
      declare
        @xml_inserted XML,
        @xml_deleted XML
             
declare  @tbl_inserted TABLE (xml_data XML, id int, RecNo int)
declare  @tbl_deleted  TABLE (xml_data XML, id int, RecNo int)

      set @xml_inserted = ( SELECT id,folder_id,name 
                               from inserted ----  as Record     
                               for xml  path ('Record'),  elements -- raw --,  elements -- auto, elements , root('RecordSet')     
                             )

      set @xml_deleted = ( SELECT id,folder_id,name 
                               from deleted ----  as Record     
                               for xml  path ('Record'),  elements -- raw --,  elements -- auto, elements , root('RecordSet')     
                             )
                             
insert into @tbl_inserted
   select t.c.query ('.') as xml_data,
          t.c.value('(id/text())[1]','int') as ID,  
          row_number () over (order by (select 100) ) as RowNum
      from @xml_inserted.nodes('*') t(c)  
                             
insert into @tbl_deleted
   select t.c.query ('.') as xml_data,
          t.c.value('(id/text())[1]','int') as ID,  
          row_number () over (order by (select 100) ) as RowNum
      from @xml_deleted.nodes('*') t(c)  

-----------
  
    INSERT history.history_log (action, table_name, record_id, xml_inserted, xml_deleted  )
      SELECT 'Update','test.AntennaType', I.id, I.xml_data, D.xml_data
        from @tbl_inserted I
          left join @tbl_deleted D on I.id=D.id        
  
  
*/

GO
/****** Object:  StoredProcedure [history].[sp_Create_triggers_all]    Script Date: 16.05.2020 10:54:58 ******/

 IF OBJECT_ID(N'[history].[sp_Create_triggers_all]') IS  not  NULL

DROP PROCEDURE [history].[sp_Create_triggers_all]
GO
/****** Object:  StoredProcedure [history].[sp_Create_triggers_all]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [history].[sp_Create_triggers_all] 
AS 
BEGIN

  DECLARE  
    @TableName VARCHAR(100);


exec history.sp_Drop_triggers_all 


SELECT * 
     into #temp 
  FROM history.tables_for_history
  WHERE enabled=1	



  while exists(SELECT * FROM #temp)
  BEGIN
    print  @TableName

    SELECT top 1 @TableName=Table_Name FROM #temp;  
    delete top (1) FROM #temp 
     

    exec history.sp_create_triggers 	@TABLE_NAME_FULL=@TableName

  END	

END

GO
/****** Object:  StoredProcedure [history].[sp_Drop_triggers_all]    Script Date: 16.05.2020 10:54:58 ******/
  IF OBJECT_ID(N'[history].[sp_Drop_triggers_all]') IS  not  NULL

DROP PROCEDURE [history].[sp_Drop_triggers_all]
GO
/****** Object:  StoredProcedure [history].[sp_Drop_triggers_all]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [history].[sp_Drop_triggers_all] 
AS 
BEGIN

  DECLARE  
    @Name VARCHAR(500);


select 
    schema_Name(schema_id) + '.' +name as name
    
 into #temp    
  
from 
  sys.objects
    where 
      type = 'TR' and name like '%_Audit_tr'



-----------------------------------


  while exists(SELECT * FROM #temp)
  BEGIN
 
    SELECT top 1 @name =[name]  FROM #temp;  
    delete top (1) FROM #temp 
     
    exec ('drop trigger '+ @name)

  END	   


END

GO
/****** Object:  UserDefinedFunction [history].[ft_Record_change_diff]    Script Date: 16.05.2020 10:54:58 ******/

 IF OBJECT_ID(N'[history].[ft_Record_change_diff]') IS  not  NULL

DROP FUNCTION [history].[ft_Record_change_diff]
GO
/****** Object:  UserDefinedFunction [history].[ft_Record_change_diff]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [history].[ft_Record_change_diff] 
(
 
  @xml_1 XML, --= '<RecordSet><Record><id>10504</id><folder_id>3785</folder_id><name>xgvxfgbcvbc</name>88<gain_type>99</gain_type><tilt_type>6</tilt_type>661.2<omni>0</omni>1.21.26840<polarization_str>h</polarization_str><band>15</band>0<comment/><band_id>7</band_id></Record></RecordSet>',
  @xml_2 XML --= '<RecordSet><Record><id>10504</id><folder_id>3785</folder_id><name>1.2-HSX4-142(H)������</name>42.6<gain_type>0</gain_type><tilt_type>0</tilt_type>143751.2<omni>0</omni>1.21.26840<polarization_str>h</polarization_str><band>15</band>0<comment/><band_id>7</band_id></Record></RecordSet>';

)

RETURNS @res TABLE (
 
  column_name varchar(100),
  old_value  varchar(max),
  new_value  varchar(max)

)
AS
BEGIN

if @xml_1 is null  and  @xml_2 is null
begin
  select top 1
         @xml_1 = xml_deleted, 
         @xml_2 = xml_inserted
     from history.history_log order by id desc
--     from history.history_log where id=517148




 -- set  @xml_1 = '<Record><id>10504</id><folder_id>3785</folder_id><name>xgvxfgbcvbc</name>88<gain_type>99</gain_type><tilt_type>6</tilt_type>661.2<omni>0</omni>1.21.26840<polarization_str>h</polarization_str><band>15</band>0<comment/><band_id>7</band_id></Record>'
 -- set  @xml_2 = '<Record><id>833</id><name>WiMIC (������)</name><folder_id>5303</folder_id><band>3</band><power_max>24</power_max><freq_min>3400</freq_min><freq_max>3550</freq_max><failure_period>10000</failure_period><equaliser_profit>0</equaliser_profit><freq_channel_count>1</freq_channel_count><kratnostByFreq>0</kratnostByFreq><kratnostBySpace>0</kratnostBySpace><level_tx_a>-50</level_tx_a><level_rx_a>-50</level_rx_a><comment>jjjjjjjjjjjjjj</comment></Record>';
end
 

declare @tbl_src TABLE ( 
    column_name varchar(100),
    column_value  varchar(max) )

declare   @tbl_dest TABLE ( 
    column_name varchar(100),
    column_value  varchar(max) )
      

insert into @tbl_src
select  
    C.value('local-name(.)', 'varchar(50)') as column_name,
    C.value('text()[1]', 'varchar(max)') as column_value
 
FROM @xml_1.nodes('/Record/*') AS T(C)

------------------------------------------------
insert into @tbl_dest
select  
    C.value('local-name(.)', 'varchar(50)') as column_name,
    C.value('text()[1]', 'varchar(max)') as column_value

FROM @xml_2.nodes('/Record/*') AS T(C)

------------------------------------------------
 
 insert into @res
    select  
       D.column_name,
       S.column_value as value_old,
       D.column_value as value_new

    FROM  @tbl_dest D
      left outer join @tbl_src S on S.column_name=D.column_name

    where
      IsNull(S.column_value,'') <> IsNull(D.column_value,'')
   
          

  return
 
END

GO
/****** Object:  StoredProcedure [history].[sp_Record_History_SEL]    Script Date: 16.05.2020 10:54:58 ******/
  IF OBJECT_ID(N'[history].[sp_Record_History_SEL]') IS  not  NULL

DROP PROCEDURE [history].[sp_Record_History_SEL]
GO
/****** Object:  StoredProcedure [history].[sp_Record_History_SEL]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [history].[sp_Record_History_SEL]
(
  @table_name varchar(50) = null,
  @id int = null
)
AS
BEGIN
/*  if @table_name is null
  begin
    set @table_name = 'Property'
    set @id = 6836 
  end
*/

  if @table_name is null
  begin
 /*   set @table_name = 'AntennaType'
--    set @table_name = 'test.AntennaType'
    set @id = 10490 
*/

    set @table_name = 'LinkEndType'
--    set 958 = 'test.AntennaType'
    set @id = 958
        
  end

  if charindex('.',@table_name)=0
    set @table_name = 'dbo.' + @table_name
  


 select
  --  id as tree_id,

    ROW_NUMBER() OVER(order by (select null))  AS Row_Num,
--    ROW_NUMBER() OVER(order by (select 100)) as varchar(10)) AS Row_Num,
 		cast(null as int) as parent_id,
 
        *, 
            
       case 
           when action='UPDATE' then '���������'
           when action='INSERT' then '�������'
           when action='DELETE' then '��������'
        end as action_str,        
        
        cast(null as varchar(100)) column_name,
        cast(null as varchar(max)) old_value,
        cast(null as varchar(max)) new_value
        
        
     into #temp 
     
  from history.history_log
  where 
    (table_name  = @table_name and record_id = @id)
    or
    (record_parent_table_name = @table_name  and record_parent_id=@id)


------------------------------------------------


/*
select * into #temp_  
  from #temp 
 
declare 
  @xml_inserted  XML,
  @xml_deleted  XML

while Exists ( SELECT * FROM #temp_)
begin
  select top 1 @xml_inserted=xml_inserted, @xml_deleted=xml_deleted
    from #temp_*/

/*
print  cast(@xml_deleted  as varchar(max)) 
print '--------------------------'

print  cast(@xml_inserted as varchar(max))
print '--------------------------'



 select  h.new_value, h.old_value
      from 
         history.ft_Record_change_diff (  @xml_deleted, @xml_inserted )  h --  ,
*/
  ------------------------------------------

/*  insert into #temp ( new_value, old_value)
    select  h.new_value, h.old_value
      from 
         history.ft_Record_change_diff (@xml_deleted, @xml_inserted )  h --  ,
*/


/*    
  delete top (1) from #temp_
        
end*/
 

insert into #temp (parent_id, column_name, new_value, old_value, xml_inserted, xml_deleted)
  select l.id, h.column_name, h.new_value, h.old_value,
          l.xml_inserted, l.xml_deleted 
--  select l.* , h.column_name, h.new_value, h.old_value
    from 
      #temp L
--      history.history_log  L
       outer apply    
         history.ft_Record_change_diff (  l.xml_deleted, l.xml_inserted )  h   
               
     where action='UPDATE'
 
  
  select * from #temp
    
  
END

GO
/****** Object:  View [dbo].[view__object_fields_enabled]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[view__object_fields_enabled]') IS  not  NULL

DROP VIEW      [dbo].[view__object_fields_enabled]
GO
/****** Object:  View [dbo].[view__object_fields_enabled]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view__object_fields_enabled]
AS
SELECT TOP 100000 
       dbo._objects.name AS objname,
       dbo._objects.table_name,
       dbo._objects.table_name_initial,
       
       dbo._object_fields.*,   
       
       dbo._object_fields.type AS field_type,
       dbo._object_fields.name AS field_name
FROM dbo._object_fields
     LEFT OUTER JOIN dbo._objects ON dbo._object_fields.object_id =
     dbo._objects.id
     
WHERE (dbo._object_fields.enabled = 1)

ORDER BY dbo._objects.name,
         dbo._object_fields.parent_id,
         dbo._object_fields.index_

GO
/****** Object:  UserDefinedTableType [dbo].[TVarChar_table]    Script Date: 16.05.2020 10:54:58 ******/
/****** Object:  UserDefinedTableType [dbo].[TVarChar_table]    Script Date: 16.05.2020 10:54:58 ******/

if TYPE_ID('TVarChar_table') is null
CREATE TYPE [dbo].[TVarChar_table] AS TABLE(
	[value] [varchar](100) NULL
)
GO
/****** Object:  UserDefinedFunction [dbo].[ft_VARCHAR_String_to_Table]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[ft_VARCHAR_String_to_Table]') IS  not  NULL


DROP FUNCTION [dbo].[ft_VARCHAR_String_to_Table]
GO
/****** Object:  UserDefinedFunction [dbo].[ft_VARCHAR_String_to_Table]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ft_VARCHAR_String_to_Table]
(
  @input     AS Varchar(max) ,
  @DELIMITER as Varchar(1)=','
)

RETURNS
   @Result TABLE(Value varchar(50))

AS

BEGIN
/*
0. �����������������
1. Rec.567 (Okumura-Hata)
2. Cost. 231
3. Dego 
4. Ksj-Bertoni
5. Metod B
6. Indoor
7. SOFDMA

*/


--  SET @input = replace(@input,char(10),'')
  SET @input = replace(@input,char(13),'')


  DECLARE @str VARCHAR(50)

  DECLARE @ind Int

  IF(@input is not null)

  BEGIN

        SET @ind = CharIndex(@DELIMITER, @input)

        WHILE @ind > 0

        BEGIN

              SET @str = SUBSTRING(@input,1,@ind-1)

              SET @input = SUBSTRING(@input,@ind+1,LEN(@input)-@ind)

              INSERT INTO @Result values (RTRIM(LTRIM( @str)))

              SET @ind = CharIndex(@DELIMITER, @input)

        END

        SET @str = @input

if RTRIM(LTRIM(@str))<>''
        INSERT INTO @Result values (RTRIM(LTRIM(@str)))

  END

  RETURN

END

GO


IF OBJECT_ID(N'[map].[user_map_settings]') IS   NULL


CREATE TABLE [map].[user_map_settings](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_name] [varchar](50) NOT NULL DEFAULT (suser_name()),
	[host_name] [varchar](50) NOT NULL CONSTRAINT [DF__user_map___host___4DFF6746]  DEFAULT (host_name()),
	[objname] [varchar](50) NOT NULL,
	[label_props] [xml] NULL,
	[attribute_column_names] [varchar](max) NULL,
	[style_xml] [xml] NULL,
	[caption_sql] [varchar](max) NULL,
 CONSTRAINT [user_map_settings_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [user_map_settings_uq] UNIQUE NONCLUSTERED 
(
	[user_name] ASC,
	[host_name] ASC,
	[objname] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]



GO
/****** Object:  StoredProcedure [map].[sp_Map_engine_layer_attributes_SEL]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[map].[sp_Map_engine_layer_attributes_SEL]') IS  not  NULL

DROP PROCEDURE [map].[sp_Map_engine_layer_attributes_SEL]
GO
/****** Object:  StoredProcedure [map].[sp_Map_engine_layer_attributes_SEL]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [map].[sp_Map_engine_layer_attributes_SEL]
(
  @OBJNAME  varchar(50)=null
)
AS
BEGIN
  if @OBJNAME is null
    set @OBJNAME = 'link'
    
    
--ui.user_map_settings


/*select attribute_column_names 
         from  map.user_map_settings
         where 
            user_name = user_name() and
            host_name = host_name() and
             OBJNAME = @OBJNAME 
*/

declare 
  @names varchar(max) =
     (select attribute_column_names 
         from  map.user_map_settings
         where 
            user_name = suser_name() and
            host_name = host_name() and
             OBJNAME = @OBJNAME 
      )
      
      
--prind      
      
      

declare 
  @tbl TVarChar_table

insert into @tbl
  select * from dbo.ft_VARCHAR_String_to_Table (@names, ',')

--select * from @tbl

-------------------


select 
  --  id, parent_id, name, caption,
  
   
    cast( IIF( name in (select * from  @tbl ), 1,0) as bit)  as checked,
    
    IIF( IsNull(Is_folder,0)=0,  ''''+ caption +':'' + IIF('+ name +' is null, '''', cast('+ name +' as varchar(max)) )'  , null) as [caption_sql],
    IIF( IsNull(Is_folder,0)=0,  'IIF('+ name +' is null, '''', cast('+ name +' as varchar(max)) )'  , null) as [caption_small_sql],
  
  --  cast(0 as bit) as checked,
    
     *

    into #temp

  from  dbo.view__object_fields_enabled  
  where is_in_view=1 and
        OBJNAME=@OBJNAME

------------------
select * from #temp
 
END


/*
 if @ID is null
    set @ID = 1111


declare 
  @names varchar(max) =
     (select top 1 attribute_column_names from  ui.user_map_settings)

declare 
  @tbl TVarChar_table

insert into @tbl
  select * from dbo.ft_VARCHAR_String_to_Table (@names)

select * from @tbl

--ft_VARCHAR_String_to_Table


select * from ui.user_map_settings
*/

GO
/****** Object:  StoredProcedure [map].[sp_Map_engine_layer_attributes_UPD]    Script Date: 16.05.2020 10:54:58 ******/

  IF OBJECT_ID(N'[map].[sp_Map_engine_layer_attributes_UPD]') IS  not  NULL

DROP PROCEDURE [map].[sp_Map_engine_layer_attributes_UPD]
GO
/****** Object:  StoredProcedure [map].[sp_Map_engine_layer_attributes_UPD]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [map].[sp_Map_engine_layer_attributes_UPD]
(
  @OBJNAME varchar(50)  ,
  @NAMES   varchar(max) ,
  
  @CAPTION_SQL  varchar(max) = null
  
)
AS
BEGIN
  if @OBJNAME is null
  begin
    set @OBJNAME = 'link'
    
    set @names = 'name'
    
 end


set @CAPTION_SQL = replace (@CAPTION_SQL , '"', '')
set @CAPTION_SQL = replace (@CAPTION_SQL , ';', '+ Char(10)+')

--set @CAPTION_SQL = 'name'  --replace (@NAMES , ',', '+ Char(10) +')



/*   s:=s.Replace('"','');
   s:=s.Replace(Chr(10),'+'+Chr(10)+'+');

*/



if not exists (select * from  map.user_map_settings
                   where 
                      user_name = suser_name() and  
                      host_name = host_name() and
                     OBJNAME = @OBJNAME  
                )
   insert into map.user_map_settings (OBJNAME, attribute_column_names) 
                             values( @OBJNAME, @names )      

ELSE
  update map.user_map_settings
  set 
    attribute_column_names = @names,
    CAPTION_SQL = @CAPTION_SQL
        
  where
     OBJNAME = @OBJNAME and
     user_name = suser_name() and  
     host_name = host_name()   
 

 
  return @@ROWCOUNT
  

 
END

GO
/****** Object:  StoredProcedure [map].[sp_MapMaker]    Script Date: 16.05.2020 10:54:58 ******/

/****** Object:  UserDefinedFunction [dbo].[fn_Linkend_Antenna_info]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[fn_Linkend_Antenna_info]') IS  not  NULL

DROP FUNCTION [dbo].[fn_Linkend_Antenna_info]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Linkend_Antenna_info]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:   
---------------------------------------------------------------

CREATE FUNCTION [dbo].[fn_Linkend_Antenna_info] 

 */


CREATE FUNCTION [dbo].[fn_Linkend_Antenna_info] 
(
  @LINKEND_ID int   
 
)

--346447
--RETURNS varchar(50)

RETURNS @ret TABLE 
(
    -- Columns returned by the function
    
    azimuth 	varchar(50),
    vert_width 	varchar(50),
    horz_width 	varchar(50),    
    gain 		varchar(50),
    polarization 	varchar(50),
    diameter 		varchar(50), 
    ground_height 	varchar(50),
    height 		varchar(50),
    tilt 		varchar(50),
    loss 		varchar(50),

    loss_full 	float,
    
    height_full float
    
   
)

as

BEGIN

  declare 
  	@result varchar(50);
   


  DECLARE   
    @azimuth_str 		varchar(50),
    @vert_width_str 	varchar(50),
    @horz_width_str 	varchar(50),    
    @gain_str 			varchar(50),
    @polarization_str 	varchar(50),
    @diameter_str 		varchar(50), 
    @ground_height_str 	varchar(50),
    @height_str 		varchar(50),
    @tilt_str 			varchar(50),
    @loss_str 			varchar(50),
    
    @loss_full	float =0, 
    @height_full		float    ;

/*
  set @diameter_str='';
  set @height_str='';
  set @polarization_str='';
  set @gain_str='';
  set @vert_width_str ='';
  set @horz_width_str ='';
  set @ground_height_str='';
  set @azimuth_str='';
  set @loss_str='';
  set @tilt_str='';
  
  */
  
  
  select 


      @azimuth_str 		= IsNull(@azimuth_str       + '/', '')+  CAST(azimuth as varchar(50)) , 
      @vert_width_str 	= IsNull(@vert_width_str  	+ '/', '')+  CAST(vert_width as varchar(50)), 
      @horz_width_str 	= IsNull(@horz_width_str  	+ '/', '')+  CAST(horz_width as varchar(50)), 
  	  @polarization_str = IsNull(@polarization_str  + '/', '')+  polarization_str, 
  	  @diameter_str 	= IsNull(@diameter_str  	+ '/', '')+  CAST(diameter as varchar(50)), 
   	  @gain_str     	= IsNull(@gain_str      	+ '/', '')+  CAST(gain as varchar(50)),

   	  @loss_str     	= IsNull(@loss_str      	+ '/', '')+  CAST(Isnull(loss,0) as varchar(50)),
   	  @tilt_str     	= IsNull(@tilt_str      	+ '/', '')+  CAST(tilt as varchar(50)),

  	  @height_str 		= IsNull(@height_str  		+ '/', '')+  CAST(A.height as varchar(50)),
      @ground_height_str= IsNull(@ground_height_str + '/', '')+  CAST(ground_height + A.height as varchar(50)), 

      @loss_full = @loss_full + isnull(A.loss,0), 
      @height_full = isnull(ground_height,0) + isnull(A.height,0) 
    
  
  	from Linkend_Antenna A  
          LEFT OUTER JOIN Property   ON A.property_id = Property.id
    WHERE Linkend_ID=@LINKEND_ID
    ORDER BY A.height
    
    
    INSERT INTO @ret (
      azimuth,
      vert_width,
      horz_width,
      gain,
      polarization ,
      diameter,
      ground_height,
      height,
      tilt,
      loss,
      
      loss_full,
      height_full
    ) 
    
    VALUES (
      @azimuth_str,
      @vert_width_str,
      @horz_width_str,
      @gain_str,
      @polarization_str,
      @diameter_str,
      @ground_height_str,
      @height_str,
      @tilt_str,
      @loss_str,
      
      @loss_full,
      @height_full
    )
    
    
   
    
    

/*
SELECT     dbo.Linkend_Antenna.*, ground_height 
FROM         dbo.Linkend_Antenna LEFT OUTER JOIN
                      dbo.Property ON dbo.Linkend_Antenna.property_id = dbo.Property.id
                      
          */            
   /*  
	set @result='param not defined: '+ @PARAM
     
  IF @PARAM='loss'
    set @result= SUBSTRING(@loss_str, 2, 100)    

  IF @PARAM='tilt'
    set @result= SUBSTRING(@tilt_str, 2, 100)  

  IF @PARAM='azimuth'
    set @result= SUBSTRING(@azimuth_str, 2, 100)  
            
  IF @PARAM='vert_width'
    set @result= SUBSTRING(@vert_width_str, 2, 100)  

  IF @PARAM='horz_width'
    set @result= SUBSTRING(@horz_width_str, 2, 100)  

  IF @PARAM='gain'
    set @result= SU99999BSTRING(@gain_str, 2, 100)  

  IF @PARAM='height'
    set @result= SUBSTRING(@height_str, 2, 100)  
  
  IF @PARAM='diameter'
    set @result= SUBSTRING(@diameter_str, 2, 100)  
  
  IF @PARAM='ground_height'
    set @result= SUBSTRING(@ground_height_str, 2, 100)  

  IF @PARAM='polarization'
    set @result= SUBSTRING(@polarization_str, 2, 100) 

 -- RETURN 'param not defined -?????????????'
*/
	RETURN
  
END

GO


/****** Object:  StoredProcedure [dbo].[sp_Link_Add]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[sp_Link_Add]') IS  not  NULL


DROP PROCEDURE [dbo].[sp_Link_Add]
GO
/****** Object:  StoredProcedure [dbo].[sp_Link_Add]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Link_Add]
(
    @PROJECT_ID int,
    
    @OBJNAME varchar(50) ,
    
    -------------------------------------
    -- TEMPLATE_SITE_ID
    -------------------------------------
    @TEMPLATE_LINK_ID int = null,    
--    @TEMPLATE_SITE_ID int = null,    
    @PROPERTY1_ID int = null,
    @PROPERTY2_ID int = null,
    
    @NAME varchar(250),
    
    @FOLDER_ID int = null,
    @CLUTTER_MODEL_ID int = null,
    
    @LINKEND1_ID int ,
    @LINKEND2_ID int ,
        
--    @PMP_SECTOR_ID int = null,    
--    @PMP_TERMINAL_ID int = null,
    
    
    @Comments varchar(250) = null,
    @Status_id int = null,
    
  --  @NAME_SDB varchar(20) = null,
--    @NAME_TN varchar(20) = null,
    
    @SESR float = null,
    @KNG float = null,
    @CALC_METHOD int = 0      --  null        --  @State_id   			int =null, -- ������: ��������, �����������...   --  @IS_GET_INFO 			bit = 0
)    
AS
begin

  SET ANSI_WARNINGS OFF --String or binary data would be truncated.



  IF @FOLDER_ID=0 SET @FOLDER_ID=NULL;
  --if @LINK_REPEATER_ID=0 set @LINK_REPEATER_ID=null


  DECLARE 
--    @len_m float, 
  --  @sName VARCHAR(100),
    @lat1 float,
    @lon1 float,
    @lat2 float,
    @lon2 float,

    @AZIMUTH1 float,
    @AZIMUTH2 float,
     
--    @property1_id INT,
--    @property2_id INT,
    
    @id INT,
    @new_id INT;

  --  @project_id  int,  

 --   @project1_id  int,  
  --  @project2_id  int;

  --  @project_id  int,  
 --   @objname  VARCHAR(10);
   
 
 --    @aLat float,
  --   @aLon float
     
  --258929
  
    -- Name     : ����������� � ���� � ��������� before insert
  -- Address  : ����������� � ���� � ��������� before insert
  
   
  
  IF @Name       IS NULL  RAISERROR ('@Name is NULL', 16, 1);
--  IF @LENGTH_M=0  RAISERROR ('@LENGTH_M=0', 16, 1);
  
--   set @sName = [dbo].fn_Link_get_new_name (@PROJECT_ID, @Name)


--  set @LENGTH_M = round(@LENGTH_M,1)



  
 
--------------------------
-- check if name exists
-------------------------- 
  SELECT top 1 @id=id 
  	FROM link 
    WHERE (name=@NAME) and (PROJECT_ID=@PROJECT_ID)
           and ((@Folder_id IS NULL) or (Folder_id=@Folder_id))  

  IF @id is not NULL
  BEGIN
    SELECT 'record already exists' as msg,  @id as ID     
    RETURN -@id;  
  END  







  IF NOT EXISTS(SELECT * FROM Status WHERE (id=@status_id))
  BEGIN 
    set @status_id=NULL
  END
/* 

  IF NOT EXISTS(SELECT * FROM lib_Link_State WHERE (id=@state_id))
  BEGIN 
    set @state_id=NULL
  END
*/


  IF NOT EXISTS (SELECT * FROM ClutterModel WHERE (ID=@CLUTTER_MODEL_ID))
  BEGIN 
    SELECT top 1 @CLUTTER_MODEL_ID=id FROM ClutterModel 
    
    if @CLUTTER_MODEL_ID is NULL  RAISERROR ('@CLUTTER_MODEL_ID is NULL', 16, 1);
    
   -- PRINT 'SELECT * FROM ClutterModel WHERE (ID=@CLUTTER_MODEL_ID)'
     
  END
  
  

  

  -----------------------------------------  
--  IF (@LINKEND1_ID IS NOT NULL ) and (@LINKEND2_ID IS NOT NULL )
  -----------------------------------------
--  BEGIN
 /*   IF NOT EXISTS(SELECT * FROM LinkEND WHERE ID=@LINKEND1_ID)
    BEGIN
      RAISERROR ('@LINKEND1_ID not exists', 16, 1);
      return -1      
    END      

    IF NOT EXISTS(SELECT * FROM LinkEND WHERE ID=@LINKEND2_ID)
    BEGIN
      RAISERROR ('@LINKEND2_ID not exists', 16, 1);
      return -2
    END      
*/
 /*
    IF EXISTS(SELECT * FROM Link WHERE (LINKEND1_ID in (@LINKEND1_ID,@LINKEND2_ID)) or 
                                       (LINKEND2_ID in (@LINKEND1_ID,@LINKEND2_ID)))
    BEGIN
      RAISERROR (' IF EXISTS(SELECT * FROM Link WHERE (LINKEND1_ID in (@LINKEND1_ID,@LINKEND2_ID)) or (LINKEND2_ID in (@LINKEND1_ID,@LINKEND2_ID)))', 16, 1);

      SELECT * FROM Link WHERE (LINKEND1_ID in (@LINKEND1_ID,@LINKEND2_ID)) or 
                               (LINKEND2_ID in (@LINKEND1_ID,@LINKEND2_ID))


      return -3
    END      
 
*/

   -- IF EXISTS(SELECT * FROM Link WHERE (LINKEND1_ID=@LINKEND1_ID) and (LINKEND2_ID=@LINKEND2_ID)) or 
  --     EXISTS(SELECT * FROM Link WHERE (LINKEND1_ID=@LINKEND2_ID) and (LINKEND2_ID=@LINKEND1_ID))
  --    RAISERROR ('@LINKEND1_ID<>@LINKEND2_ID', 16, 1);

--    set @objname='link'



--------------------------------------------------------------------
-- 
--------------------------------------------------------------------
if @TEMPLATE_LINK_ID is not null
begin
  IF @property1_id is null    RAISERROR ('@property1_id', 16, 1);
  IF @property2_id is null    RAISERROR ('@property2_id', 16, 1);


  delete from LINKEND where id in (@LINKEND1_ID,@LINKEND2_ID)

  exec @LINKEND1_ID = [lib].sp_Template_Link_apply_to_link @property_id=@property1_id, @TEMPLATE_LINK_ID=@TEMPLATE_LINK_ID
  exec @LINKEND2_ID = [lib].sp_Template_Link_apply_to_link @property_id=@property2_id, @TEMPLATE_LINK_ID=@TEMPLATE_LINK_ID

end else
begin
  SELECT @property1_id=property_id  FROM LinkEnd WHERE (ID=@LINKEND1_ID);            
  SELECT @property2_id=property_id  FROM LinkEnd WHERE (ID=@LINKEND2_ID);

end
 

--    IF @project1_id  IS NULL  BEGIN RAISERROR ('@project1_id is NULL', 16, 1);  return -4  END 
--    IF @project2_id  IS NULL  BEGIN RAISERROR ('@project2_id is NULL', 16, 1);  return -5  END 
--    IF @project1_id <> @project2_id  BEGIN RAISERROR ('@project1_id <> @project1_id', 16, 1);  return -6 END 
--    IF @property1_id = @property2_id  BEGIN RAISERROR ('@property1_id = @property2_id ', 16, 1);  return -8 END 
 
 
           
--    SELECT @property2_id=property_id 
  --    FROM LinkEnd WHERE (ID=@LINKEND2_ID)

    ----------------------------------
    --  set CHANNEL_TYPE 'low','high'
    ----------------------------------
  --  UPDATE LinkEnd set CHANNEL_TYPE='low'  WHERE (ID=@LINKEND1_ID);
  --  UPDATE LinkEnd set CHANNEL_TYPE='high' WHERE (ID=@LINKEND2_ID);


   
--  END ELSE  
 -- BEGIN
 -- print  ''
  
 --   set @objname='pmp_link'
    
 --   set @property1_id = dbo.fn_PMP_Sector_Get_Property_id(@PMP_SECTOR_ID)
--IF @property1_id  IS NULL  RAISERROR ('@property1_id is NULL   111111111', 16, 1); 

/*
    SELECT 
    	@property1_id=property_id   --, @project2_id=project_id 
     --   @project1_id =project_id 
      FROM view_PMP_sector --_with_project_id
      WHERE (ID=@Pmp_sector_ID)
      


    SELECT 
    	@project1_id=project_id 
   	FROM Property 
    WHERE (ID=@property1_id);


--    SELECT @property1_id = property_id
  --  FROM PMP_Sector WHERE id= @PMP_SECTOR_ID
    --LEFT OUTER JOIN PMP_site  ON PMP_Sector.pmp_site_id = PMP_site.id

    SELECT 
    	@property2_id=property_id  
    --    @project2_id=project_id 
      FROM view_PMP_TERMINAL --_with_project_id 
      WHERE (ID=@PMP_TERMINAL_ID)
     
     */
      
 --   IF @project1_id   IS NULL BEGIN RAISERROR ('@project1_id is NULL', 16, 1);   return -4  END 
--    IF @project2_id   IS NULL BEGIN RAISERROR ('@project2_id is NULL', 16, 1);   return -4  END     
--    IF @property1_id  IS NULL  RAISERROR ('@property1_id is NULL', 16, 1); 
--    IF @property2_id  IS NULL  RAISERROR ('@property2_id is NULL', 16, 1); 
--    IF @project1_id <> @project2_id BEGIN  RAISERROR ('@project1_id <> @project1_id', 16, 1);  return -4  END 
 

 -- END




--  exec sp_Object_GetNewName @OBJNAME='Link', @NAME=@NAME, 
 --       @PROJECT_ID=@project_id, @FOLDER_ID=@FOLDER_ID, @RESULT=@sName out


--  set @len_m = dbo.fn_Property_Get_Length_m (@property1_id, @property2_id);
  --IF @len_m > 100000 
   --  RAISERROR ('@len_m > 100 000 ', 16, 1);    
  


  INSERT INTO Link
     (PROJECT_ID, 
      NAME, 
      FOLDER_ID,  
      objname, 
  
      LINKEND1_ID, 
      LINKEND2_ID, 
    
--      PMP_SECTOR_ID, 
--      PMP_TERMINAL_ID,
      
      property1_id,
      property2_id,
         
      length --temp
      
      )
  VALUES
     (@project_id, 
      @NAME, --@sNAME, 
      @FOLDER_ID,
      @objname, 
      
      @LINKEND1_ID, 
      @LINKEND2_ID,
    
--      @PMP_SECTOR_ID, 
--      @PMP_TERMINAL_ID,
      
      @property1_id,
      @property2_id,
             
     
      1 --length 
      ) 
   
     
--  set @new_id=@@IDENTITY;
        
  set @new_id=IDENT_CURRENT ('Link')
     


Alter table  link  Disable trigger all

  
  
  UPDATE Link
  SET    
    CLUTTER_MODEL_ID = @CLUTTER_MODEL_ID,
     
    status_id=@status_id,
      
  --  NAME_SDB=@NAME_SDB,
  --  NAME_TN =@NAME_TN,
    Comments=@Comments,
    
   -- Link_REPEATER_ID=@Link_REPEATER_ID,
      
    SESR= @SESR,
    KNG = @KNG,
 --   SPACE_LIMIT = @SPACE_LIMIT,
    CALC_METHOD = @CALC_METHOD
    
  WHERE 
      id = @new_ID

  exec sp_Link_Update_Length_and_Azimuth @id = @new_ID
  
  Alter table  link ENABLE trigger all
  
  
  -------------------------------------------
  select @lat1=lat, @lon1=lon  FROM [Property]  WHERE id=@property1_id    
  select @lat2=lat, @lon2=lon  FROM [Property]  WHERE id=@property2_id

--RETURN




  
  --  IF @@IDENTITY=0  RAISERROR ('@@IDENTITY=0', 16, 1);
  
    
 -- if @IS_GET_INFO=1
    SELECT id,guid,name,  
    	--	LENGTH_M,	
    		@lat1 as lat1, @lon1 as lon1,  
    		@lat2 as lat2, @lon2 as lon2
      FROM link 
      WHERE ID=@new_id
  
  RETURN @new_id;     
    
--   RETURN @@IDENTITY;    
        
end

/*


Alter table  property Disable trigger all


--  set @new_id=@@IDENTITY
  
    UPDATE property
    SET     
      TOWER_HEIGHT = @TOWER_HEIGHT,
    
      GROUND_HEIGHT =@GROUND_HEIGHT,
      CLUTTER_HEIGHT=@CLUTTER_HEIGHT,
      clutter_name  =@clutter_name,
      
      LAT_WGS = @LAT_WGS,
      LON_WGS = @LON_WGS,      
      
      LAT_CK95 = @LAT_CK95,
      LON_CK95 = @LON_CK95,
            
      ADDRESS 	=@ADDRESS,
      CITY 		=@CITY,
      GEOREGION_ID =@GEOREGION_ID,


      State_id=@State_id, 
      Placement_id=@Placement_id,
     
      Comments=@Comments
    
 
      
   	WHERE 
    	id = @new_id
  
  exec sp_Property_Update_lat_lon_kras_wgs @ID=@new_id
  

Alter table  property ENABLE trigger all


*/

GO
/****** Object:  StoredProcedure [dbo].[sp_LinkEnd_Add]    Script Date: 16.05.2020 10:54:58 ******/


IF OBJECT_ID(N'[dbo].[sp_LinkEnd_Add]') IS  not  NULL

DROP PROCEDURE [dbo].[sp_LinkEnd_Add]
GO
/****** Object:  StoredProcedure [dbo].[sp_LinkEnd_Add]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE procedure [dbo].[sp_LinkEnd_Add]
( 
  @OBJNAME 			VARCHAR(20)=null,
     
 -- @Template_site_ID int =NULL,
  
 -- @PROJECT_ID INT,

  @NAME 			varchar(250),
  @PROPERTY_ID 	    int,

--  @EQUIPMENT_TYPE int =1,   -- 1- ��������� ������������

  @LINKENDTYPE_ID		int =NULL,
  @LINKENDTYPE_MODE_ID  int =null, 


  @CHANNEL_TYPE VARCHAR(4)=null,


  @Rx_Level_dBm			float = null, --������� ������� �� ����� ���������


  @PASSIVE_ELEMENT_ID   int =null, 
  @PASSIVE_ELEMENT_LOSS float = null,


  -----------------------------
  -- custom equipment
  -----------------------------
  @BAND 					VARCHAR(10) = null,

 -- @POWER_MAX 	float = null,
  @Tx_Freq_MHz      		float = null, 
  @POWER_dBm 				float = 25,    
--  @POWER_LOSS float = null,

  @MODULATION_TYPE  		VARCHAR(50) = '32QAM', 
  @SIGNATURE_HEIGHT_dB  	float = 14,   --������ ���������
  @SIGNATURE_WIDTH_MHz  	float = 24,   --������ ���������
  @THRESHOLD_BER_3  		float = -80, 
  @THRESHOLD_BER_6  		float = -73, 

 -- @EQUALISER_PROFIT  		float = null,
  @Freq_Spacing_MHz  		float = null,       

  @Loss          		float = null,
  @KNG            			float = null,
  @Bitrate_Mbps 			float=NULL, --float


 -----------------------------
  -- PMP Sector
  -----------------------------
--  @PMP_SITE_ID int= null,
 -- @PMP_Sector_ID int= null,
  @PMP_Site_ID  int,

  @Cell_Layer_ID int = null,
  @Calc_Model_ID int = null,
  @Calc_Radius_km float= null,


  -----------------------------
  -- PMP terminal 
  -----------------------------
--  @PMP_SITE_ID int= null,
  @Connected_PMP_Sector_ID int= null,


  @COMMENTS varchar(250)=null

 
--  @IS_GET_INFO bit = 0

)
AS
begin

  SET ANSI_WARNINGS OFF --String or binary data would be truncated.

--return


  ---------------------------------------------
--  IF IsNull(@PROPERTY_ID,0)=0  RAISERROR ('@PROPERTY_ID is NULL', 16, 1);
  IF @OBJNAME = 'pmp_sector' and IsNull(@Pmp_site_ID ,0) = 0
     RAISERROR ('@Pmp_site_ID  NULL', 16, 1);
 -- IF @project_id IS NULL  RAISERROR ('@project_id is NULL', 16, 1);


  IF @Name IS NULL  RAISERROR ('@Name is NULL', 16, 1);

  
  ---------------------------------------------

  DECLARE 
    @EQUIPMENT_TYPE int,
    
  --  @project1_id 	int,
 --   @sName			VARCHAR(200),
    @s 				VARCHAR(100), 
    @id 		    int, 
    @new_ID 		int 
     
   


  ---------------------------------
  -- PMP sector
  ---------------------------------
  if IsNull(@Pmp_site_ID ,0) > 0
     SELECT 
      @PROPERTY_ID=PROPERTY_ID  
    FROM  Pmp_site  
      WHERE id = @Pmp_site_ID 
  
  
   
  
  
  IF not EXISTS(SELECT * FROM Property WHERE (ID=@Property_ID) )
    RAISERROR ('Property not EXISTS', 16, 1);
  
  
--  IF not EXISTS(SELECT * FROM project WHERE (ID=@project_ID) )
 --   RAISERROR ('project not EXISTS', 16, 1);
  
  
  
set @name = link.fn_LinkEnd_name (@PROPERTY_ID, @NAME) 
 
 
/*  SELECT top 1 @id=id FROM LinkEnd  WHERE (PROPERTY_ID=@PROPERTY_ID) and (name=@NAME)
  
  IF @id is not NULL
  BEGIN
    SELECT 'record already exists' as msg,  @id as ID     
    RETURN -1 * @id;  
  END  
*/  
  
  

 -- SELECT @project1_id=project_id FROM PROPERTY WHERE (ID=@PROPERTY_ID);
  
  
--  IF @project1_id IS NULL  RAISERROR ('@PROPERTY_ID - @project1_id is NULL', 16, 1);  
--  IF @project_id <> @project1_id  RAISERROR ('@project_id <> @project1_id', 16, 1);


  

  if @LINKENDTYPE_ID=0       SET @LINKENDTYPE_ID=NULL
  if @LINKENDTYPE_MODE_ID=0  SET @LINKENDTYPE_MODE_ID=NULL
    
  if ISNULL(@SIGNATURE_HEIGHT_dB,0)=0  SET @SIGNATURE_HEIGHT_dB=14 --������ ���������
  if ISNULL(@SIGNATURE_WIDTH_MHz,0)=0  SET @SIGNATURE_WIDTH_MHz=24 --������ ���������
  
  
  
    
  
  
 --print @sName



 --set @PROJECT_ID = dbo.fn_Property_GetProjectID(@PROPERTY_ID);
 
 
 --print @PROJECT_ID
 
 
 INSERT INTO LinkEnd
   (
    NAME, 
    PROPERTY_ID,    
    OBJNAME     
   )
  VALUES
   (
    @NAME, 
    @PROPERTY_ID,
    @OBJNAME 
   ) 
   
 -- set @new_ID = @@IDENTITY 
 set @new_id=IDENT_CURRENT ('LinkEnd')
 

print @new_ID


 
Alter table  LinkEnd Disable trigger all

   
  
  
--    SET @sName=@NAME

 
 
  IF ISNULL(@LINKENDTYPE_ID,0) > 0 
  begin
    set @EQUIPMENT_TYPE =0
    set @Tx_Freq_MHz = dbo.fn_LinkEndType_GetAveFreq_MHz (@LINKENDTYPE_ID)
  END
  ELSE BEGIN
    set @EQUIPMENT_TYPE =1    
    
    if Isnull(@Tx_Freq_MHz,0)=0
      set @Tx_Freq_MHz = dbo.fn_Band_GetAveFreq_MHz (@BAND) 
  END


 

 -- set @new_id=IDENT_CURRENT ('LinkEnd')
  

  
  UPDATE LinkEnd 
  set 
     Rx_Level_dBm       = @Rx_Level_dBm,
     
     PASSIVE_ELEMENT_ID = @PASSIVE_ELEMENT_ID,
     
     CHANNEL_TYPE =  @CHANNEL_TYPE,
     
     equipment_type     = @equipment_type,
        
     COMMENTS = @COMMENTS,
     
 
--  UPDATE PMP_Sector 
--   set 
  --  equipment_type = @equipment_type,
        
 --   LINKENDTYPE_ID     =@LINKENDTYPE_ID,
 --   LINKENDTYPE_MODE_ID=@LINKENDTYPE_MODE_ID,
        
   -- rx_freq_MHz = @TX_FREQ_MHZ,
  --  tx_freq_MHz = @TX_FREQ_MHZ,      	     	       
        
    --PMP sector
    Calc_Model_ID = COALESCE( @Calc_Model_ID,  Calc_Model_ID),
    Cell_Layer_ID = COALESCE( @Cell_Layer_ID,  Cell_Layer_ID),
    Calc_Radius_km= COALESCE( @Calc_Radius_km, Calc_Radius_km),
   
 	PMP_Site_ID = COALESCE( @PMP_Site_ID , PMP_Site_ID),
     
    --PMP terminal
    Connected_PMP_Sector_ID = COALESCE( @Connected_PMP_Sector_ID , Connected_PMP_Sector_ID) 



  WHERE    
    ID = @new_ID   

  

  
   
  if ISNULL(@LINKENDTYPE_ID,0) > 0
  
    exec sp_Object_Update_LinkEndType 
       @OBJNAME='LinkEnd', 
       @ID=@new_ID, 
       @LinkEndType_ID=@LinkEndType_ID, 
       @LinkEndType_mode_ID=@LinkEndType_mode_ID

   
  ELSE
    ------------------------
    -- custom equipment
    ------------------------      
    UPDATE linkend 
    set 
      BAND  = @BAND, 

      tx_freq_MHz = @TX_FREQ_MHZ,
      rx_freq_MHz = @TX_FREQ_MHZ,

      POWER_dBm   = @POWER_dBm,  

      SIGNATURE_HEIGHT =  COALESCE(@SIGNATURE_HEIGHT_dB, SIGNATURE_HEIGHT), 
      SIGNATURE_WIDTH  =  COALESCE(@SIGNATURE_width_MHz, SIGNATURE_width),
     
      MODULATION_TYPE  =  COALESCE(@MODULATION_TYPE, MODULATION_TYPE),
      THRESHOLD_BER_3  =  COALESCE(@THRESHOLD_BER_3, THRESHOLD_BER_3), 
      THRESHOLD_BER_6  =  COALESCE(@THRESHOLD_BER_6, THRESHOLD_BER_6),
      Bitrate_Mbps     =  COALESCE(@Bitrate_Mbps,    Bitrate_Mbps), 
        
      
      
      LOSS=@LOSS,         
      KNG = @KNG,
                 
      MODULATION_COUNT      = m.level_COUNT,
      GOST_53363_modulation = m.GOST_53363_modulation 
     
     from 
        (select top 1 * from Lib.Modulations 
         WHERE name=@MODULATION_TYPE) m     
        
    WHERE    
      linkend.ID = @new_ID
       
      
Alter table  LinkEnd enable trigger all
     

if @Connected_PMP_Sector_ID is not null
  exec dbo.sp_PMP_Terminal_Plug  @ID = @new_ID, @PMP_SECTOR_ID = @Connected_PMP_Sector_ID  

/*   
CREATE PROCEDURE dbo.sp_PMP_Terminal_Plug
( 
  @ID 	    	  int,
  @PMP_SECTOR_ID  int
)
AS*/
       
  
  
--  if @IS_GET_INFO=1
  SELECT id,guid,name,PROPERTY_ID FROM LINKEND WHERE ID=@new_ID
  
  RETURN @new_ID;    

  


end

GO
/****** Object:  StoredProcedure [dbo].[sp_Linkend_Antenna_Add]    Script Date: 16.05.2020 10:54:58 ******/
----------------------------------------------

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
IF OBJECT_ID(N'[dbo].[fn_LinkEnd_Azimuth]') IS  not  NULL

DROP function [dbo].[fn_LinkEnd_Azimuth]
GO



CREATE FUNCTION dbo.fn_LinkEnd_Azimuth 
(
  @ID int,  --LinkEnd ID
  @NEXT_LinkEnd_ID int=null
)
RETURNS float
BEGIN
  DECLARE    
--    @property1_id int, 
--    @property2_id int,
  --  @Next_LinkEnd_ID int,
    
    @lat1 float,
    @lon1 float,
    
    @lat2 float,
    @lon2 float,

    @result float;

  if @NEXT_LinkEnd_ID is null 
     set @Next_LinkEnd_ID = dbo.fn_LinkEnd_Get_Next_LinkEnd_ID (@ID);
   


  IF @Next_LinkEnd_ID IS NULL
    return -1;




--5162


    
    SELECT                  
       @lat1=A1.lat,
       @lon1=A1.lon
    FROM dbo.LinkEnd 
         cross apply [dbo].fn_Linkend_Antenna_pos (id) A1
    where id=@ID
    
    
    

------------------
    SELECT                  
       @lat2=A1.lat,
       @lon2=A1.lon
    FROM dbo.LinkEnd 
         cross apply [dbo].fn_Linkend_Antenna_pos (id) A1
    where id=@Next_LinkEnd_ID
    
    
    
  if @LAT1 is null or 
     @LAT2 is null or
     @LON1 is null or
     @LON2 is null
   return null  

    

------------------


--return @Next_LinkEnd_ID
--set @Next_LinkEnd_ID = 0

/*  SELECT @property1_id = property_id
    FROM Linkend
    WHERE id = @ID


  SELECT @property2_id = property_id
    FROM Linkend
    WHERE id = @Next_LinkEnd_ID
 
*/
  RETURN dbo.fn_AZIMUTH (@lat1, @lon1, @lat2, @lon2, null);

--  RETURN dbo.fn_Property_Get_AZIMUTH (@property1_id, @property2_id);
 
    
END

go


IF OBJECT_ID(N'[dbo].[sp_Linkend_Antenna_Add]') IS  not  NULL

DROP PROCEDURE [dbo].[sp_Linkend_Antenna_Add]
GO
/****** Object:  StoredProcedure [dbo].[sp_Linkend_Antenna_Add]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE procedure [dbo].[sp_Linkend_Antenna_Add]
(    
  @NAME 				varchar(50),          
  @ANTENNATYPE_ID		int =null,     

  @HEIGHT  	float=null, 
  @AZIMUTH 	float=null, 
  @TILT 	float=null,
  @LOSS	float=null,

  -----------------------------------
  -- Custom
  -----------------------------------
  @BAND 		varchar(50)=null, 
  @Freq_MHz 	float=null,
  @GAIN	    	float=null,          --_dBi
  @DIAMETER   	float=null,      
  @VERT_WIDTH 	float=null,
  @HORZ_WIDTH 	float=null,
  @POLARIZATION_STR varchar(4)=null,
  
  -----------------------------------
  @LINKEND_ID 		int 

--  @PMP_SECTOR_ID 	int =null,
  
--  @PMP_TERMINAL_ID 	int =null
--  @REPEATER_ID 	    int =null


--@LOCATION_TYPE int = null


--@LAT 		float=null,
--@LON 		float=null,


)
AS
begin
 if @Name is null
  begin
    set @Name = 'sdfsdfsdf'
    set @LINKEND_ID = 5162
    
    set @AntennaType_ID = 11101
    set @Height = 20
    set @Diameter = 33
  
  end
  
  
IF @LINKEND_ID =0
   RAISERROR ('@LINKEND_ID =0', 16, 1);



IF not EXISTS (SELECT * FROM LINKEND WHERE id = @LINKEND_ID)
   RAISERROR ('not EXISTS (SELECT * FROM LINKEND WHERE id = @LINKEND_ID)', 16, 1);






  DECLARE 
    --@sName VARCHAR(100),
    
    @next_linkend_id int,
    @count int, 
    @new_id int, 
    @pmp_site_id int,
    
    @project_id  int, 
    @property_id int,         
           
    @LAT 		float,
    @LON 		float,
    @LOCATION_TYPE int
         
   
 
 --    @aLat float,
  --   @aLon float
     
  --258929
  
    -- Name     : ����������� � ���� � ��������� before insert
  -- Address  : ����������� � ���� � ��������� before insert
  
    
  
--  IF @PROJECT_ID IS NULL  RAISERROR ('@PROJECT_ID is NULL', 16, 1);
  IF @Name IS NULL  RAISERROR ('@Name is NULL', 16, 1);

/*
  IF (@LINKEND_ID IS NULL) and 
     (@PMP_SECTOR_ID IS NULL) and 
     (@PMP_TERMINAL_ID IS NULL) --and
--     (@REPEATER_ID IS NULL) 
          
   RAISERROR ('@LINKEND_ID,@PMP_SECTOR_ID,@PMP_TERMINAL_ID is NULL', 16, 1);
*/
   

  ----------------------------
--  IF ISNULL(@LINKEND_ID,0)>0 
  ----------------------------
--  BEGIN
    SELECT @count=Count(*)  FROM Linkend_Antenna   WHERE LINKEND_ID = @LINKEND_ID  

    if @count=2   RETURN -2;    
--  END   
   
   


  -----------------------------------  
 -- IF (@LINKEND_ID IS NOT NULL) 
--  BEGIN
 --   IF NOT EXISTS(SELECT * FROM LINKEND WHERE id = @LINKEND_ID)
  --    RAISERROR ('@LINKEND_ID NOT EXISTS: %d', 16, 1, @LINKEND_ID);    

      
  
    -- antenna 2 --
    set @Next_LinkEnd_ID = dbo.fn_LinkEnd_Get_Next_LinkEnd_ID(@LINKEND_ID)  
        
/*  select @LINKEND_ID
    
    
    return
          */
    
    if @Next_LinkEnd_ID is not NULL
       set @AZIMUTH = dbo.fn_LinkEnd_Azimuth(@LINKEND_ID,null)  
   
  
--  return
   
 -- END ELSE

/*  -----------------------------------
  IF (@PMP_SECTOR_ID IS NOT NULL) 
  BEGIN  
    set @property_id = dbo.fn_PMP_Sector_Get_Property_id (@PMP_SECTOR_ID)
    
    
  END ELSE
*/
   
/*  -----------------------------------
  IF (@PMP_TERMINAL_ID IS NOT NULL) 
  BEGIN  
    SELECT @property_id=PROPERTY_ID 
      FROM PmpTerminal  WHERE id = @PMP_TERMINAL_ID
      
  END ELSE
*/  
/*
  -----------------------------------
  IF (@REPEATER_ID IS NOT NULL) 
  BEGIN  
    SELECT @property_id=PROPERTY_ID 
      FROM Link_Repeater WHERE id = @REPEATER_ID
      
  END ELSE
  BEGIN
    RAISERROR ('OWNER is incorrect', 16, 1);
    return -2
  END
 */ 	



 -- IF (@property_id IS NULL)  
  --  RAISERROR ('RAISERROR - @PROPERTY_ID is NULL', 16, 1);

    SELECT @property_id=PROPERTY_ID   
      FROM LINKEND   
      WHERE id = @LINKEND_ID

if @property_id is null
   RAISERROR ('@property_id is null', 16, 1);


  SELECT 
   --   @project_id=PROJECT_ID, 
      @lat=LAT, 
      @lon=LON
    FROM Property  
    WHERE id = @property_id 

--SET @LOCATION_TYPE = 0;



--     (@PMP_SECTOR_ID IS NULL) and 
  --   (@PMP_TERMINAL_ID IS NULL)
  
  -- RAISERROR ('@LINKEND_ID,@PMP_SECTOR_ID,@PMP_TERMINAL_ID is NULL', 16, 1);
    

  if  (@ANTENNATYPE_ID  is not null )
  BEGIN
    SELECT 
      @BAND        = BAND,  

      @Gain        = Gain,  
      @Freq_MHz    = Freq,
--      @Freq_MHz    = Freq_MHz,
      @Diameter    = Diameter,
      @Polarization_STR= Polarization_STR,
      @Vert_Width  = Vert_Width,
      @Horz_Width  = Horz_Width
    FROM AntennaType 
    WHERE id = @ANTENNATYPE_ID      
  END



  INSERT INTO Linkend_Antenna
     (
      NAME, 
     
/*     ANTENNATYPE_ID,  
      BAND,
      FREQ_MHz,
      
      LOSS_dB,  GAIN, DIAMETER, 
      VERT_WIDTH, HORZ_WIDTH, 
      POLARIZATION_STR,
      HEIGHT, AZIMUTH, TILT, 
      
      LAT, LON, LOCATION_TYPE,
*/ 
     
      LINKEND_ID, 
--      PMP_SECTOR_ID,
  --    PMP_TERMINAL_ID,
   --   REPEATER_ID,
      
      PROPERTY_ID
      )
  VALUES
     (
      @NAME, 
     
/*     @ANTENNATYPE_ID,  
      @BAND,
      @Freq_MHz, 
     
      @LOSS_dB, @GAIN, @DIAMETER, 
      @VERT_WIDTH, @HORZ_WIDTH, 
      @POLARIZATION_STR,  
      @HEIGHT, @AZIMUTH, @TILT, 
      
      @LAT, @LON, @LOCATION_TYPE,
*/
      @LINKEND_ID,     
--      @PMP_SECTOR_ID,
--      @PMP_TERMINAL_ID,
--      @REPEATER_ID,
        
      @property_id
      ) 



 --  set @new_id = @@IDENTITY
  
  set @new_id= IDENT_CURRENT ('Linkend_Antenna')   
    

 
Alter table  Linkend_Antenna Disable trigger all

  


  UPDATE Linkend_Antenna
  set    
      ANTENNATYPE_ID=@ANTENNATYPE_ID,  
     
      BAND		=@BAND,
      FREQ_MHz	=@FREQ_MHz,
      
      LOSS	=@LOSS,  
      GAIN		=@GAIN, 
      DIAMETER	=@DIAMETER, 
      VERT_WIDTH=@VERT_WIDTH, 
      HORZ_WIDTH=@HORZ_WIDTH, 
      
      POLARIZATION_STR=@POLARIZATION_STR,
      HEIGHT     =@HEIGHT, 
      AZIMUTH    =@AZIMUTH,
      TILT       =@TILT, 
      
      LAT           =@LAT,  
      LON           =@LON, 
      LOCATION_TYPE =@LOCATION_TYPE
     
  WHERE id=@new_id



Alter table  Linkend_Antenna enable trigger all


  ----------------------------
  IF ISNULL(@LINKEND_ID,0)>1 
  ----------------------------
  BEGIN
    SELECT @count=Count(*) 
      FROM Linkend_Antenna
      WHERE LINKEND_ID = @LINKEND_ID
  
    if @count=2  
      UPDATE LINKEND 
        SET  KRATNOSTBYSPACE=1
        WHERE id = @LINKEND_ID
  END

/*  ----------------------------
  IF ISNULL(@PMP_TERMINAL_ID,0)>1
  ----------------------------
  BEGIN
    SELECT @count=Count(*) 
      FROM Linkend_Antenna
      WHERE PMP_TERMINAL_ID = @PMP_TERMINAL_ID

    if @count=2  
      UPDATE PMPTERMINAL
        SET  KRATNOSTBYSPACE=1
        WHERE id = @PMP_TERMINAL_ID


--    UPDATE PMPTERMINAL
  --    SET  KRATNOSTBYSPACE=1
    --  WHERE id = @PMP_TERMINAL_ID
  END
*/

--  SELECT id,guid,name FROM link WHERE ID=@new_id


  RETURN @new_id;


/*       if iAntennaCount=1 then
         dmLinkEnd.Update (aPmpTerminalID, [db_Par(FLD_KRATNOST_BY_SPACE, 1)]);
*/

        
end

GO
/****** Object:  StoredProcedure [dbo].[sp_PMP_Terminal_Plug]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[sp_PMP_Terminal_Plug]') IS  not  NULL

DROP PROCEDURE [dbo].[sp_PMP_Terminal_Plug]
GO
/****** Object:  StoredProcedure [dbo].[sp_PMP_Terminal_Plug]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* 
������������ PMP_Terminal �� ������ ������
 */
/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */ 
CREATE PROCEDURE [dbo].[sp_PMP_Terminal_Plug]
( 
  @ID 	    	  int,
  @PMP_SECTOR_ID  int
)
AS
BEGIN
  ---------------------------------------------
--  IF @ID IS NULL  RAISERROR ('@ID is NULL', 16, 1);
--  IF @PMP_Sector_ID IS NULL  RAISERROR ('@PMP_Sector_ID is NULL', 16, 1);
  ---------------------------------------------
 
if  @ID IS NULL 
begin
  set @ID = 9406
  set @PMP_SECTOR_ID = 9387
  
end



  DECLARE 
    @result 	int,

    @pmp_site_id 	int,
    @link_id int,

	@sector_project_id 		int,    
	@terminal_project_id 	int,    

    @property_pmp_site_id 	    int,
    @property_terminal_id   int,
    
    @pmp_site_name 			VARCHAR(50),
    @pmp_terminal_name  	VARCHAR(50),
    @new_name  				VARCHAR(100),
    @property_name 			VARCHAR(100)
 
  
  SELECT @pmp_site_id       = pmp_site_id, 
         @sector_project_id = project_id
    FROM view_PMP_sector --_with_project_id       
    WHERE id = @PMP_SECTOR_ID


--IF @sector_project_id  IS NULL  RAISERROR ('@sector_project_id is NULL', 16, 1); 

  -------------------------------------------------------------
  IF @pmp_site_id IS NULL  RAISERROR ('@pmp_site_id is NULL', 16, 1);
  -------------------------------------------------------------





  
  SELECT @pmp_site_name    = name, 	
  		 @property_pmp_site_id = property_id
    FROM PMP_Site     
    WHERE id = @pmp_site_id

  SELECT  @pmp_terminal_name    = name,  
  		  @property_terminal_id = property_id,
          @terminal_project_id  = project_id,
          
          @property_name = property_name
            
    FROM view_PMP_TERMINAL --_with_project_id   
    WHERE id = @ID





  IF @terminal_project_id <> @sector_project_id
     RAISERROR ('@terminal_project_id <> @sector_project_id', 16, 1);



update LinkEnd 
  set 
    connected_pmp_sector_id = @PMP_SECTOR_ID,
    connected_pmp_site_id   = (select pmp_site_id from LinkEnd where ID=  @PMP_SECTOR_ID)
  where 
    id = @ID 



  SELECT @link_id = id
    FROM Link  
    WHERE linkend2_id = @ID


  SET @new_name = @pmp_site_name +' <-> '+ @property_name + ' / ' +@pmp_terminal_name
  
  --SET @new_name = @pmp_site_name +' <-> '+ @pmp_terminal_name
--  SET @new_name = @pmp_terminal_name +' <-> '+ @pmp_site_name

/*

print  @pmp_terminal_name
print  @pmp_site_name
print  @new_name

*/



  --------------------------------
  IF @link_id IS NOT NULL
  --------------------------------
  BEGIN
    UPDATE Link  
      SET name = @new_name,
          linkend1_id   = @PMP_SECTOR_ID,
        --  property1_id  = @property_pmp_site_id,
          
          
          profile_XML = null

--          pmp_sector_id = @PMP_SECTOR_ID,
--          property1_id  = @property_site_id
--          length=1 
    WHERE 
      id = @link_id
    
    
    exec sp_Link_ClearCalc @link_id
    exec sp_Link_Update_Length_and_Azimuth @link_id

      
  END ELSE
  --------------------------------
  BEGIN
    IF @PMP_SECTOR_ID  IS NULL  RAISERROR ('@PMP_SECTOR_ID is NULL', 16, 1); 
    IF @sector_project_id  IS NULL  RAISERROR ('@sector_project_id is NULL', 16, 1);   
  
  
--  return
  


--select   @PMP_SECTOR_ID,@ID 

--return

   
/*
  dbo.ft_AZIMUTH_and_length   
  
  
    length_m float,
    AZIMUTH  float,
    AZIMUTH_back  float
    
   */

/*    
    set @AZIMUTH_rep1 = dbo.fn_AZIMUTH(@LAT_rep, @Lon_rep,  @LAT1, @LON1, null)
    set @AZIMUTH_rep2 = dbo.fn_AZIMUTH(@LAT_rep, @Lon_rep,  @LAT2, @LON2, null)
            

*/
  
     EXEC @link_id = sp_Link_Add   
            @project_id     = @sector_project_id,
            @OBJNAME        = 'pmp_link', 
            @NAME			= @new_name,
            
            @linkend1_id    = @PMP_SECTOR_ID,
            @linkend2_id    = @ID 
            
          --  @length_M				= 1

  --return


  -- set @link_id = @@IDENTITY
   
  -- print @link_id
   
--  if @IS_GET_INFO=1
--    SELECT id,guid,name  FROM LINKEnD WHERE ID=@ID
  
--  RETURN @id;     
         
  END
 

print @link_id

--return  


IF @link_id IS NULL  
  RAISERROR ('dbo.sp_PMP_Terminal_Plug - @link_id is NULL', 16, 1);


--exec sp_Link_Update_Length_and_Azimuth @id = @link_id

 
  SELECT * FROM view_Link
--  SELECT * FROM view_Link_lat_lon   
    WHERE id=@link_id
   
  
  RETURN @link_id;
  
  /* 
  select LAT,LON for calc len
   */
  
END

GO
/****** Object:  StoredProcedure [dbo].[sp_PMP_Terminal_Select_PMP_Sectors_for_Plug]    Script Date: 16.05.2020 10:54:58 ******/


IF OBJECT_ID(N'[dbo].[sp_PMP_Terminal_Select_PMP_Sectors_for_Plug]') IS  not  NULL

DROP PROCEDURE [dbo].[sp_PMP_Terminal_Select_PMP_Sectors_for_Plug]
GO
/****** Object:  StoredProcedure [dbo].[sp_PMP_Terminal_Select_PMP_Sectors_for_Plug]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* 
  ����� PMP_Sectors ��� PMP ��������� 
  
created: Alex  
used in: d_PmpTerminal_Plug  
 */
 /* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE [dbo].[sp_PMP_Terminal_Select_PMP_Sectors_for_Plug]
( 
  @ID int,
  @PROJECT_ID int = NULL
)
AS
BEGIN
  ---------------------------------------------
--  IF @ID IS NULL  RAISERROR ('@ID is NULL', 16, 1);
  ---------------------------------------------
  DECLARE 
  --  @project_id int,
    @property_id  int;


--if IsNull(@ID,0) > 0
    SELECT 
        @property_id = property_id ,        
    	@project_id = project_id
      FROM view_PMP_TERMINAL --_with_project_id       
      WHERE id = @ID



  IF @project_id is null   RAISERROR ('@project_id is null ', 16, 1);


--fn_Property_Get_Length_m


  SELECT  id,name,pmp_site_id, pmp_site_name, Property_name, band, 
        dbo.fn_Property_Get_Length_m (@Property_id, Property_id) as Length_m,
        cast(0 as bit) as checked,
        
        @id as pmp_Terminal_id
  
    FROM  view_PMP_sector --_full 
    WHERE (project_id=@project_id)
         
         and 
          
          -- (@property_id IS null) or
          (Property_id <> IsNull(@Property_id,0) ) 
          
    ORDER BY pmp_site_name, name
  
  
  RETURN @@ROWCOUNT
    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Template_Linkend_Copy_to_PMP_SITE]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[pmp].[sp_Template_Linkend_Copy_to_PMP_SITE]') IS  not  NULL

DROP PROCEDURE [pmp].[sp_Template_Linkend_Copy_to_PMP_SITE]
GO
/****** Object:  StoredProcedure [dbo].[sp_Template_Linkend_Copy_to_PMP_SITE]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE pmp.sp_Template_Linkend_Copy_to_PMP_SITE 
(
  @Template_Linkend_ID INT = 1012, 
  @PMP_SITE_ID int = 167
)    
AS
BEGIN
  SET ANSI_WARNINGS OFF --String or binary data would be truncated.

  DECLARE  
    @id_new int,
    @property_ID int;
 --   @PROJECT_ID int; 
 --   @sName VARCHAR(100);

           
   
  SELECT 
--  		@PROJECT_ID = PROJECT_ID,
        @property_ID = property_ID 
--  	FROM view_PMP_Site 
  	FROM PMP_Site 
    WHERE (ID = @PMP_SITE_ID) 
  


 -- if @ID is null
 --   set @ID = 70365
  
  if @Template_Linkend_ID is null
  BEGIN
   SELECT top 1  @Template_Linkend_id =id FROM Template_Linkend
  
--    set @ID = 70365
 --   set @LINKEND_ID =124645
  END

    	
 -- print @ID
  
  INSERT INTO LinkEnd
--  INSERT INTO PMP_Sector
        
      (
      objname,   
 	  PMP_SITE_ID,
      property_ID,
      
       name )
   select 
      'PMP_Sector',   
      @PMP_SITE_ID,
      @property_ID,
      
      name
   from 
   		Template_Linkend     
   where 
   		id = @Template_Linkend_ID;      
    
   set @id_new= IDENT_CURRENT ('LinkEnd')


-----------------------------------------------------------   

Alter table  LinkEnd Disable trigger all

      
 UPDATE LinkEnd 
 SET  
   		band = m.band,
        LinkEndType_ID 		= m.LinkEndType_ID,
        LinkEndType_Mode_ID = m.LinkEndType_Mode_ID,
        Mode 				= m.mode,
        subband				= m.subband,
        bitrate_Mbps    	= m.bitrate_Mbps,
        channel_type        = m.channel_type,
                 
        
      --  MODULATION_count=m.MODULATION_level_count,

        MODULATION_TYPE =m.MODULATION_TYPE,
        SIGNATURE_HEIGHT=m.SIGNATURE_HEIGHT, 
        SIGNATURE_WIDTH =m.SIGNATURE_WIDTH,  
        THRESHOLD_BER_3 =m.THRESHOLD_BER_3,  
        THRESHOLD_BER_6 =m.THRESHOLD_BER_6,  
        
        POWER_MAX       = m.POWER_MAX,
        POWER_dBm       = m.POWER_MAX,
        
        rx_freq_MHz = m.rx_freq_MHz,
        tx_freq_MHz = m.tx_freq_MHz,
        
        channel_width_MHz =m.channel_width_MHz,        
        
        cell_layer_id = m.cell_layer_id,
        calc_model_id = m.calc_model_id,
        k0 			= m.k0,
        k0_open 	= m.k0_open,
        k0_closed 	= m.k0_closed,
     
 --   trx_id 		= m.trx_id,
                  
        calc_radius_km = m.calc_radius_km

    --    combiner_id    = m.combiner_id,
     --   combiner_loss  = m.combiner_loss
                
        
        
--       MODULATION_COUNT      = m.MODULATION_level_COUNT,
--       GOST_53363_modulation = m.GOST_53363_modulation,
        
   --     Freq_spacing  =  m.BANDWIDTH * CHANNEL_SPACING    -- calculated        
        
          
       from 
          (select * from Template_Linkend WHERE id=@Template_Linkend_id) m     
       WHERE 
         (LinkEnd.id = @id_new)       
  
 print @@ROWCOUNt
 
Alter table  LinkEnd enable trigger all
 
  
-----------------------------------------------------------   

    
 --	set @id_new= @@IDENTITY;
    
  exec [pmp].sp_Template_Linkend_Antenna_Copy_to_PMP_Sector_Antenna 
       @Template_Linkend_id = @Template_Linkend_ID, 
       @Linkend_ID          = @id_new
    
  RETURN @id_new       
 
END


  
/*
CREATE TABLE dbo.Template_LinkEnd (
  id int IDENTITY(1, 1) NOT NULL,
  name varchar(50) COLLATE Cyrillic_General_CI_AS NOT NULL,
  template_pmp_site_id int NOT NULL,
  objname varchar(10) COLLATE Cyrillic_General_CI_AS NULL,
  cell_layer_id int NULL,
  calc_model_id int NULL,
  k0 float NULL,
  k0_open float NULL,
  k0_closed float NULL,
  trx_id int NULL,
  linkendtype_id int NULL,
  power_W float NULL,
  power_dBm float NULL,
  THRESHOLD_BER_3 float NULL,
  THRESHOLD_BER_6 float NULL,
  tx_freq_MHz float NULL,
  calc_radius_km int NULL,
  calc_radius_noise_km int NULL,
  combiner_id int NULL,
  combiner_loss float NULL,
  Comment varchar(250) COLLATE Cyrillic_General_CI_AS NULL,
  guid uniqueidentifier DEFAULT newid() NULL,
  date_created datetime DEFAULT getdate() NULL,
  date_modify datetime NULL,
  user_modify varchar(50) COLLATE Cyrillic_General_CI_AS NULL,
  user_created varchar(50) COLLATE Cyrillic_General_CI_AS DEFAULT suser_name() NULL,
  band varchar(50) COLLATE Cyrillic_General_CI_AS NULL,
  LinkEndType_mode_id int NULL,
  LinkEndType_band_id__ int NULL,
  mode int NULL,
  channel_type int NULL,
  rx_freq_MHz float NULL,
  subband varchar(50) COLLATE Cyrillic_General_CI_AS NULL,
  channel_number int NULL,
  bitrate_Mbps float NULL,
  SIGNATURE_HEIGHT float NULL,
  SIGNATURE_WIDTH float NULL,
  channel_width_MHz float NULL,
  POWER_MAX float NULL,
  POWER_LOSS float NULL,
  Modulation_type varchar(30) COLLATE Cyrillic_General_CI_AS NULL,

 UPDATE LinkEnd SET  
        LinkEndType_Mode_ID = m.ID,
        Mode 			= m.mode,
        bitrate_Mbps    = m.bitrate_Mbps,
                 
      --  MODULATION_count=m.MODULATION_level_count,

        MODULATION_TYPE =m.MODULATION_TYPE,
        SIGNATURE_HEIGHT=m.SIGNATURE_HEIGHT, 
        SIGNATURE_WIDTH =m.SIGNATURE_WIDTH,  
        THRESHOLD_BER_3 =m.THRESHOLD_BER_3,  
        THRESHOLD_BER_6 =m.THRESHOLD_BER_6,  
        
    --    POWER_MAX       = m.POWER_MAX,
        POWER_dBm       = m.POWER_MAX,
        
       MODULATION_COUNT      = m.MODULATION_level_COUNT,
       GOST_53363_modulation = m.GOST_53363_modulation,
        
        Freq_spacing  =  m.BANDWIDTH * CHANNEL_SPACING    -- calculated        
        
          
       from 
          (select * from view_LinkEndType_Mode WHERE id=@LinkEndType_Mode_ID) m     
       WHERE 
         (LinkEnd.id = @LinkEnd_id) 

  UPDATE PMP_Site 
     SET 
  	   calc_radius_km  =m.calc_radius_km       
     from 
        (select * from Template_PMP_Site WHERE id=@ID) m     
     WHERE 
        PMP_Site.id=@PMP_Site_ID      
   
Alter table  Pmp_Site Disable trigger all



 if @TEMPLATE_PMP_SITE_ID>0 
   exec dbo.sp_Template_PMP_Site_Copy_to_PMP_Site 
   			@ID = @TEMPLATE_PMP_SITE_ID, 
            @Pmp_site_id = @new_id


Alter table  Pmp_Site enable trigger all

*/


GO
/****** Object:  View [dbo].[view_Link]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[view_Link]') IS not  NULL

DROP VIEW [dbo].[view_Link]
GO
/****** Object:  View [dbo].[view_Link]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_Link]
AS
SELECT     dbo.Link.*, 


Round(100-dbo.Link.Kng,2) as Reliability,


a1.lat AS lat1, 
a1.lon AS lon1, 
a2.lat AS lat2, 
a2.lon AS lon2, 




Property1.georegion_id AS georegion1_id, 
Property2.georegion_id AS georegion2_id,

-- Property1
Property1.address AS Property1_address, 
Property2.address AS Property2_address,

Property1.name AS Property1_name, 
Property2.name AS Property2_name, 

--Property1.name AS [Property1_name], 
--Property2.name AS [Property2_name], 

--Property1.id AS Property1_id, 
--Property2.id AS Property2_id, 


Property1.NRI AS Property1_NRI, 
Property2.NRI AS Property2_NRI, 

Property1.name_rus AS Property1_name_rus, 
Property2.name_rus AS Property2_name_rus, 



Property1.ground_height AS Property1_ground_height, 
Property2.ground_height AS Property2_ground_height,

GeoRegion_1.name AS [Property1_GeoRegion_name], 
GeoRegion_2.name AS [Property2_GeoRegion_name], 


Property1.Name_ERP AS [Property1_name_ERP], 
Property1.name_SDB AS [Property1_name_SDB], 

    

  --LinkEnd1.Band, -- AS LinkEnd1_LinkID,        


  LinkEnd1.LinkID AS LinkEnd1_LinkID,        
  LinkEnd2.LinkID AS LinkEnd2_LinkID,        

  LinkEnd1.band AS band,        
  
  LinkEnd1.bitrate_Mbps AS bitrate_Mbps,  
  LinkEnd1.tx_freq_MHz AS tx_freq_MHz, 
  LinkEnd1.rx_freq_MHz AS rx_freq_MHz, 


/*
-- ��������������
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd1.kratnostBySpace) AS LinkEnd1_kratnostBySpace, 
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd2.kratnostBySpace) AS LinkEnd2_kratnostBySpace, 

*/

ant1.diameter AS antenna1_diameter, 
ant2.diameter AS antenna2_diameter, 

ant1.height AS antenna1_height, 
ant2.height AS antenna2_height, 

ant1.polarization AS antenna1_polarization, 
ant2.polarization AS antenna2_polarization, 

  
        
  dbo.ClutterModel.name AS Clutter_Model_Name, 
        
  dbo.LinkType.name AS LinkType_Name, 
  dbo.Status.name AS status_name, 
  dbo.Status.name AS State_name, 
  
  
    dbo.fn_PickList_GetValueByCode('link', 'status',   dbo.Link.status) AS status_str, 
  
-- dbo.Status.name AS status_str, 


 -- link_status_tbl.value as status_name, 
--  link_status_tbl.value as status_str, 
      
  
  
/*        
  dbo.fn_PickList_GetValueByCode('link', 'status',   dbo.Link.status) AS status_str, 
  dbo.fn_PickList_GetValueByCode('link', 'GST_type',  dbo.Link.calc_method) AS GST_type_name, 
  dbo.fn_PickList_GetValueByCode('link', 'calc_method',  dbo.Link.calc_method) AS calc_method_name, 
  dbo.fn_PickList_GetValueByCode('link', 'LOS_status',  dbo.Link.LOS_status) AS LOS_status_name, 
  dbo.fn_PickList_GetValueByCode('link', 'terrain_type',  dbo.Link.terrain_type) AS terrain_type_name, 
  dbo.fn_PickList_GetValueByCode('link', 'underlying_terrain_type', dbo.Link.underlying_terrain_type) AS underlying_terrain_type_name ,
*/

  
  Link_Repeater.id as Link_Repeater_id,
  
  LinkEndType.name as LinkEndType_name
        
        
FROM         dbo.LinkEnd LinkEnd2 

              RIGHT OUTER JOIN dbo.LinkEnd LinkEnd1 
              
              RIGHT OUTER JOIN dbo.LinkEndType ON LinkEndType.id = LinkEnd1.LinkEndType_id  
              RIGHT OUTER JOIN dbo.Link ON LinkEnd1.id = dbo.Link.linkend1_id ON LinkEnd2.id = dbo.Link.linkend2_id 
              LEFT OUTER JOIN dbo.Status ON dbo.Link.status_id = dbo.Status.id 
              LEFT OUTER JOIN dbo.GeoRegion GeoRegion_2 
              RIGHT OUTER JOIN dbo.Property Property2 ON GeoRegion_2.id = Property2.georegion_id ON LinkEnd2.Property_id = Property2.id 
              LEFT OUTER JOIN dbo.GeoRegion GeoRegion_1 
              RIGHT OUTER JOIN dbo.Property Property1 ON GeoRegion_1.id = Property1.georegion_id ON LinkEnd1.Property_id = Property1.id 
              LEFT OUTER JOIN dbo.LinkType ON dbo.Link.linkType_id = dbo.LinkType.id LEFT OUTER JOIN dbo.ClutterModel ON dbo.Link.clutter_model_id = dbo.ClutterModel.id
                 
               left OUTER JOIN dbo.Link_Repeater ON dbo.Link.id =  dbo.Link_Repeater.Link_ID
              
                 
               OUTER apply [dbo].fn_Linkend_Antenna_info(LinkEnd1.id) ant1
               OUTER apply [dbo].fn_Linkend_Antenna_info(LinkEnd2.id) ant2
               
              
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND1_ID) A1
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND2_ID) A2

   --  left OUTER JOIN [lib].ft_PickList ('link', 'status')       as link_status_tbl        on link_status_tbl.id = Link.status


--where Link.id=240806

              
   /*  
     left OUTER JOIN [lib].ft_PickList ('link', 'status')       as link_status_tbl        on link_status_tbl.id = Link.status
     left OUTER JOIN [lib].ft_PickList ('link', 'GST_type')     as link_GST_type_tbl      on link_GST_type_tbl.id = Link.GST_type
     left OUTER JOIN [lib].ft_PickList ('link', 'calc_method')  as link_calc_method_tbl   on link_calc_method_tbl.id = Link.calc_method
     left OUTER JOIN [lib].ft_PickList ('link', 'LOS_status')   as link_LOS_status_tbl    on link_LOS_status_tbl.id = Link.LOS_status

     left OUTER JOIN [lib].ft_PickList ('link', 'terrain_type') as link_terrain_types_tbl on link_terrain_types_tbl.id = Link.terrain_type

     left OUTER JOIN [lib].ft_PickList ('link', 'underlying_terrain_type') as link_underlying_terrain_type_tbl on link_underlying_terrain_type_tbl.id = Link.underlying_terrain_type

     left OUTER JOIN [lib].ft_PickList ('linkend', 'kratnostBySpace') as linkend_kratnostBySpace1_tbl on linkend_kratnostBySpace1_tbl.id = LinkEnd1.kratnostBySpace
     left OUTER JOIN [lib].ft_PickList ('linkend', 'kratnostBySpace') as linkend_kratnostBySpace2_tbl on linkend_kratnostBySpace2_tbl.id = LinkEnd2.kratnostBySpace
*/

/*
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd1.kratnostBySpace) AS LinkEnd1_kratnostBySpace, 
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd2.kratnostBySpace) AS LinkEnd2_kratnostBySpace, 
*/
  
--where Link.id=5591
             
               
--WHERE     (dbo.Link.objname = 'link') or  (Link.objname is null)

GO
/****** Object:  View [dbo].[view_Link_report]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[view_Link_report]') IS  not  NULL


DROP VIEW [dbo].[view_Link_report]
GO
/****** Object:  View [dbo].[view_Link_report]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_Link_report]
AS
SELECT     dbo.Link.*, 


Round(100-dbo.Link.Kng,2) as Reliability,


a1.lat AS lat1, 
a1.lon AS lon1, 
a2.lat AS lat2, 
a2.lon AS lon2, 




/*Property1.georegion_id AS georegion1_id, 
Property2.georegion_id AS georegion2_id,
*/


-- Property1
Property1.address AS Property1_address, 
Property2.address AS Property2_address,

Property1.name AS Property1_name, 
Property2.name AS Property2_name, 

--Property1.name AS [Property1_name], 
--Property2.name AS [Property2_name], 

--Property1.id AS Property1_id, 
--Property2.id AS Property2_id, 


Property1.code AS Property1_code, 
Property2.code AS Property2_code, 


Property1.ground_height AS Property1_ground_height, 
Property2.ground_height AS Property2_ground_height,

GeoRegion_1.name AS [Property1_GeoRegion_name], 
GeoRegion_2.name AS [Property2_GeoRegion_name], 


Property1.Name_ERP AS [Property1_name_ERP], 
Property1.name_SDB AS [Property1_name_SDB], 

    

  --LinkEnd1.Band, -- AS LinkEnd1_LinkID,        


  LinkEnd1.LinkID AS LinkEnd1_LinkID,        
  LinkEnd2.LinkID AS LinkEnd2_LinkID,        

  LinkEnd1.band AS band,        
  
  LinkEnd1.bitrate_Mbps AS bitrate_Mbps,  
  LinkEnd1.tx_freq_MHz AS tx_freq_MHz, 
  LinkEnd1.rx_freq_MHz AS rx_freq_MHz, 


/*
-- ��������������
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd1.kratnostBySpace) AS LinkEnd1_kratnostBySpace, 
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd2.kratnostBySpace) AS LinkEnd2_kratnostBySpace, 

*/

ant1.diameter AS antenna1_diameter, 
ant2.diameter AS antenna2_diameter, 

ant1.height AS antenna1_height, 
ant2.height AS antenna2_height, 

ant1.polarization AS antenna1_polarization, 
ant2.polarization AS antenna2_polarization, 

  
        
  dbo.ClutterModel.name AS Clutter_Model_Name, 
        
  dbo.LinkType.name AS LinkType_Name, 
--  dbo.Status.name AS status_name, 
  dbo.Status.name AS State_name, 

--  dbo.Status.name AS status_str, 

      
link_status_tbl.value as status_str, 
 
link_calc_method_tbl.value as calc_method_name, 


--     left OUTER JOIN [lib].ft_PickList ('link', 'calc_method')  as link_calc_method_tbl   on link_calc_method_tbl.id = Link.calc_method

 
  
/*        
  dbo.fn_PickList_GetValueByCode('link', 'status',   dbo.Link.status) AS status_str, 
  dbo.fn_PickList_GetValueByCode('link', 'GST_type',  dbo.Link.calc_method) AS GST_type_name, 
  dbo.fn_PickList_GetValueByCode('link', 'calc_method',  dbo.Link.calc_method) AS calc_method_name, 
  dbo.fn_PickList_GetValueByCode('link', 'LOS_status',  dbo.Link.LOS_status) AS LOS_status_name, 
  dbo.fn_PickList_GetValueByCode('link', 'terrain_type',  dbo.Link.terrain_type) AS terrain_type_name, 
  dbo.fn_PickList_GetValueByCode('link', 'underlying_terrain_type', dbo.Link.underlying_terrain_type) AS underlying_terrain_type_name ,
*/

  
  Link_Repeater.id as Link_Repeater_id,
  
  LinkEndType.name as LinkEndType_name,
  
  Link_calc_res.Rx_LEVEL_str,
  Link_calc_res.Rx_LEVEL_back_str,
  
  
  --    Rx_Level_draft = Param_33 + Param_35 + Param_36 - Param_37 + Param_40 

Site2_dop_gain,

  Link_calc_res.Rx_LEVEL_draft,
  Link_calc_res.Rx_LEVEL_dop_draft,
  

  cast(Link_calc_res.Rx_LEVEL_draft  as varchar(10)) + 
     COALESCE(' / ' + Cast(Link_calc_res.Rx_LEVEL_dop_draft as varchar(10)), '') as Rx_LEVEL_draft_str
  
        
/*  
     Rx_LEVEL     float, --<ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_dop float, --<ITEM id="179" value="-200" caption=" ������� �������� ������� �� ����� ��������� (���) [dBm]"/>
    
    Rx_LEVEL_back     float, --<ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_back_dop float, --<ITEM id="179" value="-200" caption=" ������� �������� ������� �� ����� ��������� (���) [dBm]"/>


    Rx_LEVEL_str varchar(50), --<ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_back_str varchar(50), --<ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    
    SESR float,
    KNG  float
*/
  
  
  
        
FROM         dbo.LinkEnd LinkEnd2 

              RIGHT OUTER JOIN dbo.LinkEnd LinkEnd1 
              
      RIGHT OUTER JOIN dbo.LinkEndType ON LinkEndType.id = LinkEnd1.LinkEndType_id  
      RIGHT OUTER JOIN dbo.Link ON LinkEnd1.id = dbo.Link.linkend1_id ON LinkEnd2.id = dbo.Link.linkend2_id 
      LEFT OUTER JOIN dbo.Status ON dbo.Link.status_id = dbo.Status.id 
      LEFT OUTER JOIN dbo.GeoRegion GeoRegion_2 
      RIGHT OUTER JOIN dbo.Property Property2 ON GeoRegion_2.id = Property2.georegion_id ON LinkEnd2.Property_id = Property2.id 
      LEFT OUTER JOIN dbo.GeoRegion GeoRegion_1 
      RIGHT OUTER JOIN dbo.Property Property1 ON GeoRegion_1.id = Property1.georegion_id ON LinkEnd1.Property_id = Property1.id 
      LEFT OUTER JOIN dbo.LinkType ON dbo.Link.linkType_id = dbo.LinkType.id LEFT OUTER JOIN dbo.ClutterModel ON dbo.Link.clutter_model_id = dbo.ClutterModel.id
                 
       left OUTER JOIN dbo.Link_Repeater ON dbo.Link.id =  dbo.Link_Repeater.Link_ID
              
                 
       OUTER apply fn_Linkend_Antenna_info(LinkEnd1.id) ant1
       OUTER apply fn_Linkend_Antenna_info(LinkEnd2.id) ant2
               
              
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND1_ID) A1
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND2_ID) A2
              
     
     left OUTER JOIN [lib].ft_PickList ('link', 'status')       as link_status_tbl        on link_status_tbl.id = Link.status
     left OUTER JOIN [lib].ft_PickList ('link', 'GST_type')     as link_GST_type_tbl      on link_GST_type_tbl.id = Link.GST_type
     left OUTER JOIN [lib].ft_PickList ('link', 'calc_method')  as link_calc_method_tbl   on link_calc_method_tbl.id = Link.calc_method
     left OUTER JOIN [lib].ft_PickList ('link', 'LOS_status')   as link_LOS_status_tbl    on link_LOS_status_tbl.id = Link.LOS_status

     left OUTER JOIN [lib].ft_PickList ('link', 'terrain_type') as link_terrain_types_tbl on link_terrain_types_tbl.id = Link.terrain_type

     left OUTER JOIN [lib].ft_PickList ('link', 'underlying_terrain_type') as link_underlying_terrain_type_tbl on link_underlying_terrain_type_tbl.id = Link.underlying_terrain_type

     left OUTER JOIN [lib].ft_PickList ('linkend', 'kratnostBySpace') as linkend_kratnostBySpace1_tbl on linkend_kratnostBySpace1_tbl.id = LinkEnd1.kratnostBySpace
     left OUTER JOIN [lib].ft_PickList ('linkend', 'kratnostBySpace') as linkend_kratnostBySpace2_tbl on linkend_kratnostBySpace2_tbl.id = LinkEnd2.kratnostBySpace
     
     
    OUTER apply link.[ft_Link_calc_results_basic] (Link.id) Link_calc_res


/*
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd1.kratnostBySpace) AS LinkEnd1_kratnostBySpace, 
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd2.kratnostBySpace) AS LinkEnd2_kratnostBySpace, 
*/
  
--where Link.id=253557
             
               
--WHERE     dbo.Link.id =  1683

GO
/****** Object:  StoredProcedure [dbo].[sp_Map_XREF_Update]    Script Date: 16.05.2020 10:54:58 ******/


GO
/****** Object:  StoredProcedure [dbo].[sp_MapDesktop_ObjectMap_set_default]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[sp_MapDesktop_ObjectMap_set_default]') IS  not  NULL

DROP PROCEDURE [dbo].[sp_MapDesktop_ObjectMap_set_default]
GO
/****** Object:  StoredProcedure [dbo].[sp_MapDesktop_ObjectMap_set_default]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  

copy checked,label_style   
--------------------------------------------------------------- */

CREATE PROCEDURE [dbo].[sp_MapDesktop_ObjectMap_set_default]
(
  @ID INT 
)
AS
BEGIN    
  UPDATE Map_Objects
    set  
      checked     = m.checked,
      label_style = m.label_style,
      style       = m.style
      
    from 
      (select * from Map_XREF WHERE id=@ID) m     
    WHERE 
      (Map_Objects.name = m.MapObject_name)      
      
    
  RETURN @@ROWCOUNT      
END

GO
/****** Object:  StoredProcedure [dbo].[sp_MapDesktop_Select_ObjectMaps]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[sp_MapDesktop_Select_ObjectMaps]') IS  not  NULL

DROP PROCEDURE [dbo].[sp_MapDesktop_Select_ObjectMaps]
GO
/****** Object:  StoredProcedure [dbo].[sp_MapDesktop_Select_ObjectMaps]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE [dbo].[sp_MapDesktop_Select_ObjectMaps]
(
  @ID  			INT = null,
  @CHECKED 	bit =null  
)
AS
BEGIN

if @ID is null
  set @ID = (select top 1 id from Map_Desktops)


  ---------------------------------------------
  -- insert new maps
  ---------------------------------------------    

 /*  SELECT id 
   	  INTO #new_maps
--    FROM [CalcMap]*/


  SELECT MapObject_name 
   	  INTO #current_maps
   FROM Map_XREF 
   where (Map_Desktop_id=@ID) and (MapObject_name is not null)
  
/*
   SELECT id 
   	  INTO #new_maps
--    FROM [Map]
    FROM [view_MapFile_ByHostName]
    WHERE (project_id=@project_id) and
          (id NOT IN  (SELECT map_ID FROM #current_maps))*/
          
/*          
              SELECT map_ID 
                 FROM Map_XREF 
                 where (Map_Desktop_id=@ID) and (map_id>0) 
             ))
*/ 
       
 -- IF EXISTS(SELECT * FROM #new_maps) 
 -- BEGIN

--fixed
if exists(SELECT * FROM Map_Desktops where id=@ID )

    INSERT into Map_XREF 
        (Map_Desktop_id, 	
         MapObject_name, 
         priority, 
         checked,
         label_style,
         style,
         
         AutoLabel
           
         ) --zoom_min, zoom_max, allow_zoom, 
    SELECT 
         @ID, 						
         name, 
         priority, 
         checked,
         label_style,
         style,
         
         
          case when name='property' then  1 else null end 
         
           --zoom_min, zoom_max, allow_zoom, 
         
     FROM [Map_Objects]
     WHERE 
     	name NOT IN (SELECT MapObject_name FROM #current_maps  )  
    
      
 -- END



  DROP table #current_maps
 -- DROP table #new_maps



  ---------------------------------------------
  -- SELECT
  ---------------------------------------------    

  SELECT * FROM VIEW_MapDesktop_ObjectMaps
    WHERE (Map_Desktop_id = @ID)        
            and    
          ((@CHECKED is NULL) or (IsNull(checked,0)=1))
--          ((@CHECKED is NULL) or (IsNull(checked,1)=1))
    order by priority
  
  RETURN @@ROWCOUNT
               
END

GO
/****** Object:  View [reports].[view_LinkEnd_report]    Script Date: 16.05.2020 10:54:58 ******/




---------------------------------------------------
IF OBJECT_ID(N'[dbo].[view_LinkEnd_report]') IS  not  NULL

DROP VIEW [dbo].[view_LinkEnd_report]
GO

/*---

*/
CREATE VIEW dbo.view_LinkEnd_report
AS
SELECT L.*,
         dbo.LinkEndType.name AS [LinkEndType_name],

         dbo.LinkEndType.name AS [LinkEndType.name],
         
         dbo.LinkEndType_Mode.radiation_class AS radiation_class,
         
         --��ਭ� ������, ���
         dbo.LinkEndType_Mode.bandwidth AS bandwidth,
         
      ant.height  AS antenna_height,
      ant.diameter  AS antenna_diameter,
      ant.polarization  AS antenna_polarization,

      ant.gain  AS antenna_gain,
      
      ant.vert_width  AS antenna_vert_width,
      ant.horz_width  AS antenna_horz_width,

      ant.loss  AS antenna_loss,
      ant.tilt  AS antenna_tilt,

      ant.ground_height  AS antenna_ground_height,
         
/*         
         dbo.fn_Linkend_Antenna_GetValue(L.id, 'height') AS antenna_height,
         dbo.fn_Linkend_Antenna_GetValue(L.id, 'diameter') AS antenna_diameter,
         dbo.fn_Linkend_Antenna_GetValue(L.id, 'polarization') AS   antenna_polarization,
         dbo.fn_Linkend_Antenna_GetValue(L.id, 'gain') AS antenna_gain,
         dbo.fn_Linkend_Antenna_GetValue(L.id, 'vert_width') AS  antenna_vert_width,
         dbo.fn_Linkend_Antenna_GetValue(L.id, 'horz_width') AS   antenna_horz_width,

         dbo.fn_Linkend_Antenna_GetValue(L.id, 'loss') AS   antenna_loss,
         dbo.fn_Linkend_Antenna_GetValue(L.id, 'tilt') AS   antenna_tilt,
         */

         dbo.fn_PickList_GetValueByCode('linkend', 'redundancy', L.redundancy)    AS redundancy_str,

    --     dbo.fn_Linkend_Antenna_GetValue(L.id, 'ground_height') AS  antenna_ground_height,
         
         dbo.fn_dBm_to_Wt(L.power_dBm) AS power_Wt,          
         
         
         
         L.tx_freq_MHz / 1000 AS tx_freq_GHz,
         
--         dbo.fn_LinkEnd_Azimuth(L.id) AS Azimuth1
         dbo.fn_LinkEnd_Azimuth(L.id, NULL) AS antenna_Azimuth,
         
         (SELECT MAX(power_max) FROM LinkEndType_Mode mode WHERE mode.id=L.LinkEndType_id) as power_max1
                  
         
         
  FROM dbo.LinkEnd L
       LEFT OUTER JOIN dbo.LinkEndType_Mode ON L.LinkEndType_mode_id =
       dbo.LinkEndType_Mode.id
       LEFT OUTER JOIN dbo.LinkEndType ON L.linkendtype_id = dbo.LinkEndType.id
       
      OUTER apply fn_Linkend_Antenna_info (L.id) ant 
       
go       
     --  where l.id= 186419
     
  /*   
       ant.diameter  AS antenna_diameter,
      ant.height  AS antenna_height,
      ant.polarization  AS antenna_polarization,

LinkEndType_mode.bandwidth                      


                      
                      
FROM         dbo.LinkEnd L LEFT OUTER JOIN
          dbo.Passive_Component ON L.passive_element_id = dbo.Passive_Component.id LEFT OUTER JOIN
          dbo.LinkEndType_mode ON L.linkendtype_mode_id = dbo.LinkEndType_mode.id LEFT OUTER JOIN
          dbo.LinkEndType ON L.linkendtype_id = dbo.LinkEndType.id LEFT OUTER JOIN
          dbo.Property ON L.property_id = dbo.Property.id
                      
                      
          cross apply fn_Linkend_Link_info(L.id) link
          cross apply fn_Linkend_Antenna_info (L.id) ant
          */
---------------------------------------------------



/*

*/

GO

delete from  lib.link_status 
GO

  
--SET IDENTITY_INSERT  lib.link_status   ON
GO    


INSERT INTO  lib.link_status (id, name)
SELECT 1,N'��������'
UNION ALL SELECT 2,N'�� ��������'

GO

--SET IDENTITY_INSERT  lib.link_status   OFF
GO    


update Linkend_Antenna
  set height=1 
  where IsNull(height,0) <1
go


exec sp_MSforeachtable @command1="print '?'", @command2="ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"
GO
--  where table_name_initial is not null

update _objects
  set table_name_initial = table_name
  where table_name_initial is null

go

--------------------------------------------------------
update [_objects] 
  set view_name='view_property_' where name='property'


update [_objects] 
  set table_name_initial='link' where name='link'

update [_objects] 
  set table_name_initial='linkend' where name='linkend'

update [_objects] 
  set table_name_initial='linkend' -- , table_name='linkend'
  where name in ( 'pmp_sector', 'PMP_Terminal')

update [_objects] 
  set table_name='view_pmp_sector' , view_name='view_pmp_sector'
  where name in ( 'pmp_sector')

update [_objects] 
  set table_name='view_PMP_Terminal'  , view_name='view_PMP_Terminal'  
  where name in ( 'PMP_Terminal')

update [_objects] 
  set IsUseProjectID=1
  where name in ('Linkend', 'PMP_site', 'PMP_Terminal')



------------------------
update [_object_fields]  set XREF_TableName='CellLayer' where  XREF_TableName='Cell_layer'


update [_object_fields]  set Name='Terminal_Sensitivity_dbm' where  Name='Terminal_Sensitivity'
update [_object_fields]  set Name='TERMINAL_GAIN_db' where  Name='TERMINAL_GAIN'
update [_object_fields]  set Name='Terminal_Loss_db' where  Name='Terminal_Loss'




update _object_fields
  set items='1+0
1+1
2+0 XPIC
2+2 XPIC'
  where  name='kratnostBySpace'


update [_objects] 
  set table_name_initial='rel_matrix' where name='relief'

go


SET IDENTITY_INSERT  _objects   ON
GO    


delete from _objects where id in (184,187  )
delete from _object_fields where object_id in (184,187  )


GO
INSERT [dbo].[_objects] ([id], [name], [display_name], [table_name], [IsUseProjectID], [folder_name], [view_name], [table_name_initial]) VALUES (187, N'template_link', NULL, N'lib.template_link', NULL, NULL, NULL, N'lib.template_link')
go

INSERT [dbo].[_objects] ([id], [name], [display_name], [table_name], [IsUseProjectID], [folder_name], [view_name], [table_name_initial]) VALUES (184, N'link_sdb', N'�� SDB', N'link_sdb', 1, NULL, NULL, N'link_sdb')
GO

SET IDENTITY_INSERT  _objects   OFF
GO    



GO


INSERT [_object_fields] ([object_id], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [is_folder]) VALUES (187, NULL, 1, N'name', NULL, NULL, N'��������', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [_object_fields] ([object_id], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [is_folder]) VALUES (187, NULL, 2, N'LinkEndType_ID', NULL, N'xref', N'������������ ���', NULL, NULL, NULL, 1, NULL, NULL, N'LinkEndType', N'LinkEndType', N'LinkEndType_name', 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [_object_fields] ([object_id], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [is_folder]) VALUES (187, NULL, 5, N'height', NULL, NULL, N'������ �������', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [_object_fields] ([object_id], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [is_folder]) VALUES (187, NULL, 4, N'antennaType_id', NULL, N'xref', N'������ �������', NULL, NULL, NULL, 1, NULL, NULL, N'antenna_Type', N'antennaType', N'antennaType_name', 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [_object_fields] ([object_id], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [is_folder]) VALUES (187, NULL, 6, N'LinkEndType_Mode_ID', NULL, NULL, N'LinkEndType_Mode_ID', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL)
GO
INSERT [_object_fields] ([object_id], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [is_folder]) VALUES (187, NULL, 3, N'mode', NULL, N'button', N'�����', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, 0, NULL)
GO






GO
INSERT [dbo].[_object_fields] ( [object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, NULL, NULL, 1, N'name', NULL, NULL, N'��������', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[_object_fields] ([object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, 1, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[_object_fields] ([object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, NULL, NULL, 3, N'SESR_required', NULL, NULL, N'SESR ���� [%]', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[_object_fields] ([object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, NULL, NULL, 1, N'SESR', NULL, NULL, N'SESR ���� [%]', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[_object_fields] ([object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, NULL, NULL, 4, N'Kng_required', NULL, NULL, N'��� ���� [%]', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[_object_fields] ([object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, NULL, NULL, 1, N'Kng', NULL, NULL, N'��� ���� [%]', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[_object_fields] ([object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, NULL, NULL, 2, N'kng_year', NULL, NULL, N'��� ���� [���/���]', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO 


--================================================================================
-------exec history.sp_Create_triggers_all 
go



--EXEC  dbo._UpdateAllViews ;
--go


--update _db_version_
--set version='2020.01.18'
--go


/****** Object:  View [Security].[View_User_Security_XREF]    Script Date: 18.01.2021 14:15:25 ******/
IF OBJECT_ID(N'[dbo].[view_PMP_CalcRegion_Pmp_Sectors]') IS NOT NULL
  DROP VIEW [dbo].[view_PMP_CalcRegion_Pmp_Sectors]


GO


CREATE VIEW dbo.view_PMP_CalcRegion_Pmp_Sectors
AS
SELECT DISTINCT dbo.PMP_CalcRegionCells.pmp_calc_region_id,
       
       dbo.PMP_CalcRegionCells.id AS xref_id,
       
       dbo.PMP_CalcRegionCells.checked,
       
       dbo.PMP_CalcRegionCells.pmp_sector_id,
       
       dbo.PMP_site.guid AS [PMP_site_guid],
       
       dbo.PMP_site.name AS PMP_site_name,
       dbo.CellLayer.name AS CellLayer_name,
       dbo.CalcModel.name AS [CalcModel_name],
       
       dbo.LinkEnd.*,
       
       dbo.Property.lat AS lat,
       dbo.Property.lon AS lon,
       dbo.PMP_site.calc_radius_km AS site_calc_radius_km
       
FROM dbo.Property
     RIGHT OUTER JOIN dbo.PMP_site ON dbo.Property.id = dbo.PMP_site.property_id
     RIGHT OUTER JOIN dbo.CellLayer
     RIGHT OUTER JOIN dbo.CalcModel
     RIGHT OUTER JOIN dbo.LinkEnd ON dbo.CalcModel.id =
     dbo.LinkEnd.calc_model_id ON dbo.CellLayer.id =  dbo.LinkEnd.cell_layer_id 
     ON dbo.PMP_site.id =  dbo.LinkEnd.pmp_site_id
     RIGHT OUTER JOIN dbo.PMP_CalcRegionCells ON dbo.LinkEnd.id =  dbo.PMP_CalcRegionCells.pmp_sector_id
     
WHERE (dbo.PMP_CalcRegionCells.pmp_sector_id > 0)

go

/****** Object:  View [Security].[View_User_Security_XREF]    Script Date: 18.01.2021 14:15:25 ******/
IF OBJECT_ID(N'[dbo].[view_PMP_CalcRegion_Pmp_Antenna]') IS NOT NULL
  DROP VIEW [dbo].[view_PMP_CalcRegion_Pmp_Antenna]


GO


CREATE VIEW dbo.view_PMP_CalcRegion_Pmp_Antenna
AS
SELECT DISTINCT 
       dbo.Linkend_Antenna.*,

    dbo.PMP_CalcRegionCells.id AS xref_id,
       dbo.PMP_CalcRegionCells.checked,
       dbo.PMP_CalcRegionCells.pmp_calc_region_id,
       dbo.PMP_CalcRegionCells.antenna_id,
       
       dbo.Linkend.guid AS [Linkend_guid],
       dbo.AntennaType.name AS [AntennaType_name],
       
       dbo.Property.lat AS Property_lat,
       dbo.Property.lon AS Property_lon
       
FROM dbo.Linkend_Antenna
     LEFT OUTER JOIN dbo.Property ON dbo.Linkend_Antenna.property_id =    dbo.Property.id
     LEFT OUTER JOIN dbo.AntennaType ON dbo.Linkend_Antenna.antennaType_id =    dbo.AntennaType.id
     LEFT OUTER JOIN dbo.Linkend ON dbo.Linkend_Antenna.Linkend_id =    dbo.Linkend.id
     RIGHT OUTER JOIN dbo.PMP_CalcRegionCells ON dbo.Linkend_Antenna.id =    dbo.PMP_CalcRegionCells.antenna_id
     
WHERE (dbo.PMP_CalcRegionCells.antenna_id > 0)
----------------------
go




IF OBJECT_ID(N'[dbo].[view_PMP_CalcRegion_Used_Cell_Layers]') IS NOT NULL
  DROP VIEW dbo.view_PMP_CalcRegion_Used_Cell_Layers
GO


CREATE VIEW dbo.view_PMP_CalcRegion_Used_Cell_Layers
AS
SELECT DISTINCT dbo.PMP_CalcRegionCells.pmp_calc_region_id,
       dbo.CellLayer.id,
       dbo.CellLayer.name,
       CAST (1 AS bit) AS checked
FROM dbo.CellLayer
     RIGHT OUTER JOIN dbo.LinkEnd ON dbo.CellLayer.id =
     dbo.LinkEnd.cell_layer_id
     RIGHT OUTER JOIN dbo.PMP_CalcRegionCells ON dbo.LinkEnd.id =
     dbo.PMP_CalcRegionCells.pmp_sector_id
WHERE (dbo.LinkEnd.cell_layer_id > 0)

go

-----------------------------------------------------



IF OBJECT_ID(N'dbo.sp_PMP_CalcRegion_Select_Item') IS NOT NULL
  DROP procedure dbo.sp_PMP_CalcRegion_Select_Item
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_PMP_CalcRegion_Select_Item 
(
	@ID INT 
)    
AS
BEGIN

  if @ID is null
    set @id= (select top 1 id from Pmp_CalcRegion)


  -------------------------------------------------------------------------
--  IF @ID IS NULL  RAISERROR ('@ID is NULL', 16, 1);
  -------------------------------------------------------------------------



--SELECT top 10 * FROM  view_Pmp_CalcRegion_PMP_Sites  


--  DECLARE--
  --  @objname VARCHAR(50);
    	
    		

 -- SET ANSI_WARNINGS OFF --String or binary data would be truncated.


 -- if @ID is null
 --   set @ID = 70365
  
  if @ID is null
  BEGIN
   SELECT top 1  @id =id --, @objname =objname
     FROM pmp_CalcRegion  
  
  print 'test'
  
--    set @ID = 70365
 --   set @LINKEND_ID =124645
  END


 -- SELECT @objname=objname 
 --   FROM pmp_CalcRegion;


  print @ID

  create table [#temp]
      (
       XREF_ID     int NOT NULL,              
       
       ID          int NOT NULL,              
       guid        uniqueidentifier,
       parent_guid uniqueidentifier,
      
       checked 		bit,
       
       name        nVARCHAR(50) NOT NULL,
       objname     VARCHAR(10) NOT NULL,
               
       BAND        VARCHAR(10),
  
      ---------------
      --property
      ---------------       
      property_ID int, 

      property_lat float, 
      property_lon float, 
                  
      property_lat_str VARCHAR(50), 
      property_lon_str VARCHAR(50), 

    
      ---------------
      --PMP_Sector
      ---------------       
       calc_model_Name VARCHAR(50),
       calc_model_id 	Float,
       k0_open 				Float,
       k0_closed 			Float,
       k0 						Float,

       TX_FREQ_MHz    Float,       
       POWER_dBm      Float,    
       POWER_W        Float,
       
       color 		  int,
     
       CellLayer_id 	int,
       CellLayer_name VARCHAR(50),
  
  	   calc_radius_km  float,
    
       THRESHOLD_BER_3 Float,
       THRESHOLD_BER_6 Float,    
       
      ---------------
      --ANTENNA
      ---------------
      AntennaType_ID      int, 
      ANTENNATYPE_NAME    VARCHAR(50),

      DIAMETER            Float,
      HEIGHT              Float,
      HEIGHT_OLD          Float,      
      AZIMUTH             Float, 

      POLARIZATION_str    VARCHAR(4),
      GAIN                Float,
      TILT                Float,
      vert_width          Float,
      horz_width          Float,
      ANTENNA_loss        Float 
                     
      ) ;      

 -- SELECT * FROM #temp   

-- RETURN @@ROWCOUNT
  	  
 
--------------------------------------------------
-- PMP_Site
--------------------------------------------------     
  insert into [#temp]
    (xref_id,
     id,guid,name,objname,
     checked,
      
     property_ID,    
     
     property_lat, 
     property_lon,
     property_lat_str, 
     property_lon_str
     
     )
   select 
     xref_id,
     id,guid,name,'PMP_Site',
     checked,
     
    property_ID,
   
     lat, 
     lon,
     lat_str, 
     lon_str
     
         
   from 
     view_Pmp_CalcRegion_PMP_Sites
   where 
     pmp_Calc_Region_id = @ID
             
     
 -- SELECT * FROM #temp   
     
 -- RETURN @@ROWCOUNT
  
  
-- RETURN
 
 	     
     
--------------------------------------------------
-- PMP_Sector
--------------------------------------------------     
  insert into [#temp]
    (xref_id,
     id,guid,parent_guid,name,objname, 
    
     checked,    
     color,    
     BAND,
    
     POWER_dBm,
     POWER_W,
     TX_FREQ_MHz,
    
     calc_radius_km, 
     calc_model_Name, 
     calc_model_id, 
     k0_open, k0_closed, k0,
     
     CellLayer_id,
     CellLayer_name,
     
     THRESHOLD_BER_3,
     THRESHOLD_BER_6 
  
   --  property_ID        
               
     )
   select 
     xref_id,
     id,guid,[PMP_Site_guid],name,'PMP_Sector',
     
     checked,
     color,
     BAND,
     

     POWER_dBm,
     POWER_W,
     TX_FREQ_MHz,
     
     calc_radius_km,
     
     [CalcModel_Name], 
     calc_model_id, 
     k0_open, k0_closed, k0,
     
     Cell_Layer_id,
     CellLayer_name,
     
     THRESHOLD_BER_3,
     THRESHOLD_BER_6 
     
  --   property_ID       
     
   from 
     view_Pmp_CalcRegion_Pmp_Sectors
   where 
     pmp_Calc_Region_id = @ID
   
   
--   return
    

--------------------------------------------------
-- antennas
--------------------------------------------------    
  insert into [#temp]
    (xref_id,
     checked,
     id,guid,parent_guid,name,objname,
     BAND,
     HEIGHT,
     DIAMETER,AZIMUTH,GAIN,TILT,

     POLARIZATION_str,
     AntennaType_ID,AntennaType_NAME, 
     vert_width,horz_width,
 
     property_ID,
     property_lat, 
     property_lon,
     property_lat_str, 
     property_lon_str
           
      
   --  ANTENNA_loss,
   --  is_master
    )    
   select 
     xref_id,
     checked,
     id,guid,[Linkend_guid],name,'antenna',
     
     BAND,
     HEIGHT, 
     DIAMETER,AZIMUTH,GAIN,TILT,     
     POLARIZATION_str,
     AntennaType_ID,[AntennaType_NAME], 
     vert_width,horz_width,
   
     property_ID,
     property_lat, 
     property_lon,
     
     dbo.FormatDegree(property_lat),
     dbo.FormatDegree(property_lon)

      
  --   loss,
  --   is_master
     
   from view_Pmp_CalcRegion_Pmp_Antenna
   where pmp_Calc_Region_id = @ID


  SELECT * FROM #temp   

  RETURN @@ROWCOUNT

END

go
--------------------------------------------

IF OBJECT_ID(N'dbo.sp_PMP_CalcRegion_Init') IS NOT NULL
  DROP procedure dbo.sp_PMP_CalcRegion_Init
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_PMP_CalcRegion_Init
(
	@ID INT 
)    
AS
BEGIN
  if @ID is null
    set @id=2029




 -- if @ID is null
 --   set @ID = 70365
  
/*  if @ID is null
  BEGIN
    SELECT top 1  @id =id FROM pmp_CalcRegion  
  
--    set @ID = 70365
 --   set @LINKEND_ID =124645
  END
*/

  --------------------------------------------------------
  IF @ID is null  RAISERROR ('@ID is null', 16, 1);
  --------------------------------------------------------

 

  print @ID
  
 -- BEGIN TRANSACTION

  --------------------------------------------------------

  SELECT DISTINCT pmp_site_id INTO #pmp_site  
    FROM pmp_CalcRegionCells
    WHERE (pmp_Calc_Region_id = @ID) and (pmp_site_id>0)


  SELECT DISTINCT pmp_sector_id INTO #pmp_sector 
    FROM pmp_CalcRegionCells
    WHERE (pmp_Calc_Region_id = @ID) and (pmp_sector_id>0)
    
  SELECT Antenna_id INTO #Antenna 
    FROM pmp_CalcRegionCells
    WHERE (pmp_Calc_Region_id = @ID) and (Antenna_id>0)


  --------------------------------------------------------

  SELECT id INTO #pmp_sector_all 
--    FROM PMP_Sector
    FROM LinkEnd
    WHERE pmp_site_id in
           (SELECT pmp_site_id FROM #pmp_site) 
    
  SELECT id INTO #Antenna_all 
    FROM Linkend_Antenna
    WHERE LinkEnd_id in
           (SELECT id FROM #pmp_sector_all) 



  --------------------------------------------------------
    
	INSERT INTO pmp_CalcRegionCells (pmp_Calc_Region_id, pmp_sector_id)   
    SELECT @ID,id  
      FROM #pmp_sector_all
      WHERE id NOT IN 
         (SELECT pmp_sector_id FROM #pmp_sector) 

--return

    
	INSERT INTO pmp_CalcRegionCells (pmp_Calc_Region_id, Antenna_id)   
    SELECT @ID,id  
      FROM #Antenna_all
      WHERE id NOT IN 
         (SELECT Antenna_id FROM #Antenna) 

  --------------------------------------------------------
 
 
/*  SELECT * FROM  #pmp_sector  
  SELECT * FROM  #pmp_site  
*/

  drop table #pmp_site
  drop table #pmp_sector
  drop table #pmp_sector_all
  drop table #Antenna
  drop table #Antenna_all

-- commit TRANSACTION


  SELECT * FROM pmp_CalcRegionCells 
  	WHERE pmp_Calc_Region_id=@ID

END
go




IF OBJECT_ID(N'[dbo].[fn_Object_Has_children]') IS NOT NULL
  DROP function [dbo].[fn_Object_Has_children]


GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE FUNCTION dbo.fn_Object_Has_children
(
  @NAME varchar(50),
  @ID  int
)
RETURNS bit
AS
BEGIN

 -- SELECT top 1 @ID = ID       
 --   FROM Property
  --  where id in (select top 1 Property_id from LinkEnd)
   
  

 -- set @Name='property';
  
--  set @Name=UPPER(@Name)    
  
 -- set @ID = dbo.fn_GetRandomPropertyID;
  
 -- SELECT top 1 @ID = ID       
 ----  FROM Property
  --  where id in (select top 1 Property_id from LinkEnd)
      
  -----------------------------------------------
  -- common
  -----------------------------------------------

  -----------------------------------------------
  if @NAME = 'property'
  -----------------------------------------------
  BEGIN      
    if EXISTS(SELECT * FROM LinkEnd WHERE property_id = @id)
      RETURN 1;

  --  if EXISTS(SELECT * FROM [Site] WHERE property_id = @id)
   --   RETURN 1;

    if EXISTS(SELECT * FROM MSC WHERE property_id = @id)
      RETURN 1;

    if EXISTS(SELECT * FROM BSC WHERE property_id = @id)
      RETURN 1;

    if EXISTS(SELECT * FROM PMP_site WHERE property_id = @id)
      RETURN 1;

    if EXISTS(SELECT * FROM view_Pmp_Terminal WHERE property_id = @id)
      RETURN 1;    

    if EXISTS(SELECT * FROM Link_Repeater  WHERE property_id = @id)
      RETURN 1;    


  END ELSE    

/*  -----------------------------------------------
  -- 2G
  -----------------------------------------------

  -----------------------------------------------
  if @NAME = 'site' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM Cell WHERE site_id = @id)
      RETURN 1 
  END ELSE    

  -----------------------------------------------
  if @NAME = 'cell' BEGIN
  -----------------------------------------------
    IF OBJECT_ID('view_Cell_antenna')>0
      if EXISTS(SELECT * FROM [view_Cell_antenna] WHERE cell_id = @id)
        RETURN 1 
  END ELSE    
*/

  -----------------------------------------------
  -- Link
  -----------------------------------------------

  -----------------------------------------------
  if @NAME = 'LinkFreqPlan' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM LinkFreqPlan_link  WHERE linkfreqplan_id= @id)
      RETURN 1
  END ELSE    

  -----------------------------------------------
  if @NAME = 'LinkEnd' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM  LINKEND_ANTENNA WHERE linkend_id= @id)
      RETURN 1
  END ELSE    

  -----------------------------------------------
  if @NAME = 'Link_Repeater' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM  LINK_Repeater_ANTENNA WHERE LINK_Repeater_id= @id)
      RETURN 1
  END ELSE    


  -----------------------------------------------
  if @NAME = 'Link' BEGIN
  -----------------------------------------------
      RETURN 1
  END ELSE    

  -----------------------------------------------
  if @NAME = 'Link_sdb' BEGIN
  -----------------------------------------------
 --  if EXISTS(SELECT * FROM Link.Link_sdb WHERE Linknet_id= @id)

      RETURN 1
  END ELSE    


  -----------------------------------------------
  if @NAME = 'LinkNet' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM LinkNet_Items_XREF WHERE Linknet_id= @id)
      RETURN 1
  END ELSE    

  -----------------------------------------------
  if @NAME = 'LinkLine' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM LinkLineLinkXREF WHERE linkline_id = @id)
      RETURN 1
  END ELSE    
  
  -----------------------------------------------
  -- PMP
  -----------------------------------------------

  -----------------------------------------------
  if (@NAME = 'PMP_Calc_Region') BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM pmp_CalcRegionCells  WHERE pmp_Calc_Region_id= @id)
      RETURN 1
  END ELSE    

  -----------------------------------------------
  if @NAME = 'PMP_site' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM view_PMP_Sector WHERE pmp_site_id = @id)
      RETURN 1
  END ELSE    
  

  
  -----------------------------------------------
  if @NAME = 'PMP_Sector' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM LINKEND_ANTENNA WHERE LINKEND_id = @id)
      RETURN 1
  END ELSE    

  -----------------------------------------------
  if @NAME = 'PMP_Terminal' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM LINKEND_ANTENNA WHERE LINKEND_id = @id)
      RETURN 1
  END ELSE    

  -----------------------------------------------
  -- Template
  -----------------------------------------------
  
  -----------------------------------------------
  if @NAME = 'Template_Site' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM Template_Site_Linkend WHERE template_site_id = @id)
      RETURN 1
  END ELSE   


/*

  -----------------------------------------------
  if @NAME = 'mast' BEGIN
  -----------------------------------------------
  RETURN 1
--    if EXISTS(SELECT * FROM Linkend_antenna WHERE mast_id = @id)
  --    RETURN 1
  END ELSE   
*/

/*
  -----------------------------------------------
  if @NAME = 'Template_Site_mast' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM lib.Template_Site_Linkend_antenna WHERE template_site_mast_id = @id)
      RETURN 1
  END ELSE  
   */


  -----------------------------------------------
  if @NAME = 'Template_Site_Linkend' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM Template_Site_LinkEnd_Antenna WHERE template_site_Linkend_id = @id)
      RETURN 1
  END ELSE   


  -----------------------------------------------
  -- Template
  -----------------------------------------------


  -----------------------------------------------
  if @NAME = 'Template_pmp_Site' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM Template_Linkend WHERE template_pmp_site_id = @id)
      RETURN 1
  END ELSE    
  
  -----------------------------------------------
  if @NAME = 'Template_LinkEnd' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM Template_LinkEnd_Antenna WHERE template_LinkEnd_id = @id)
      RETURN 1
   END   

  
-- RAISERROR ('@TableName incorrect:', 16, 1 );
  
 RETURN 0; 
  
END

go



------------------------------------------------------------------

IF OBJECT_ID(N'[dbo].[sp_Explorer_Select_Object]') IS NOT NULL
  DROP procedure [dbo].[sp_Explorer_Select_Object]


GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Explorer_Select_Object
(
  @OBJNAME 			varchar(50),
  @PROJECT_ID  	int = null
)

AS
BEGIN

if @OBJNAME is null 
BEGIN
 -- set @OBJNAME = 'Antenna_Type';
  set @PROJECT_ID=1406
  set @OBJNAME = 'Link_Repeater';
  set @OBJNAME = 'template_link';

  
END	 


  declare 
  	@IsUseProjectID bit;
   -- @max_id int;


  SET ANSI_WARNINGS OFF --String or binary data would be truncated.

 -- if @OBJName IS NULL
   --  set @OBJName='LinkEndType'


DECLARE
  @tables table
                      (
                       objname    	varchar(100),
                       table_Name 	varchar(50)
                      )





DECLARE
  @temp table
                      (id     		int, 
                       name    		nvarchar(100),
                       is_folder	bit DEFAULT 0,
                       ObjName 		varchar(50),
                       
                       GUID    		uniqueidentifier,
                       parent_GUID  uniqueidentifier,
                       
                       property_name  	  varchar(200),
                    
                       folder_image_index int DEFAULT -1  --test
                       );

 

	  SELECT @IsUseProjectID =IsUseProjectID
    -- @table_name = table_name    @table_name VARCHAR(255),
    	FROM _objects
      WHERE (Name=@ObjName) 

 

  -- IF @IsUseProjectID = 0 
   --  set @PROJECT_ID = null 

  PRINT @PROJECT_ID





--SELECT * FROM	Folder	 WHERE (@PROJECT_ID is null)

 
/*
    select 
       id,Name,type, id,parent_id, 1,0
--	  SELECT 
  --  	  id,Name,type, id,parent_id, 1,0
      
  	FROM Folder
    --VIEW_Explorer_Folder
    WHERE (type=@ObjName) 
         and ((@PROJECT_ID is null) or (PROJECT_ID=@PROJECT_ID) )
    ORDER BY name asc;


return 
 */
 
 
 

 
    INSERT INTO @temp 
       (id,name,ObjName, GUID, parent_GUID, is_folder, folder_image_index)
	  SELECT 
    	  m.id,m.Name,m.type, m.GUID, s.GUID, 1,0
      
  	FROM Folder M
         LEFT OUTER JOIN Folder S ON m.parent_id = s.id

    --VIEW_Explorer_Folder
    WHERE (m.type=@ObjName) 
--          and ((@PROJECT_ID is null) or (m.PROJECT_ID=@PROJECT_ID) )
          and 
             ( ( IsNull(@IsUseProjectID,0) = 0) or (m.PROJECT_ID=@PROJECT_ID) )
          
    ORDER BY m.name;-- asc;

/*
  SELECT * FROM @temp
  
  return @@ROWCOUNT
  


 return */

 -- select @max_id=max(id)+1 from @temp; --  	Folder; 
  
  

  
-- 

-- test -----
-- sELECT * FROM #temp
-- return;  
--------  
 
INSERT INTO @tables (objname, table_Name) VALUES ('Antenna_Type', 'AntennaType')
INSERT INTO @tables (objname, table_Name) VALUES ('LinkEndType', 'LinkEndType')
INSERT INTO @tables (objname, table_Name) VALUES ('Calc_Model', 'CalcModel')
INSERT INTO @tables (objname, table_Name) VALUES ('combiner', 'Passive_Component')
INSERT INTO @tables (objname, table_Name) VALUES ('CELL_LAYER', 'CELLLAYER')
INSERT INTO @tables (objname, table_Name) VALUES ('Terminal_Type', 'TerminalType')

INSERT INTO @tables (objname, table_Name) VALUES ('Geo_Region', 'GeoRegion')
INSERT INTO @tables (objname, table_Name) VALUES ('template_pmp_Site', 'template_pmp_Site')

INSERT INTO @tables (objname, table_Name) VALUES ('Link_Type', 'LinkType')

INSERT INTO @tables (objname, table_Name) VALUES ('Link', 'Link')
INSERT INTO @tables (objname, table_Name) VALUES ('LinkEnd', 'LinkEnd')

INSERT INTO @tables (objname, table_Name) VALUES ('TEMPLATE_link', 			'lib.TEMPLATE_LINK')
INSERT INTO @tables (objname, table_Name) VALUES ('TEMPLATE_SITE', 			'TEMPLATE_SITE')

INSERT INTO @tables (objname, table_Name) VALUES ('TEMPLATE_SITE_LINKEND', 	'TEMPLATE_SITE_LINKEND')
INSERT INTO @tables (objname, table_Name) VALUES ('TEMPLATE_SITE_LINKEND_antenna', 'TEMPLATE_SITE_LINKEND_antenna')





  ---------------------------------------------
  IF @OBJName='Antenna_Type'
  ---------------------------------------------
  BEGIN           
  
 -- print ''
       
    INSERT INTO @temp (id,name,ObjName, guid,  parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid,  F.guid
      
        FROM AntennaType M 
             LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
        ORDER BY M.name;      
        
	END ELSE	

  ---------------------------------------------
  IF @OBJName='LinkEndType'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid,  F.guid
      
        FROM LinkEndType M 
           LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
        ORDER BY M.name;
	END ELSE


  ---------------------------------------------
  IF @OBJName='Calc_Model'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid, F.guid
      
        FROM CalcModel M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
        ORDER BY M.name;
	END ELSE	

  ---------------------------------------------
  IF @OBJName='combiner'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid,  parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid, F.guid
      
        FROM Passive_Component M 
           LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
        ORDER BY M.name;
	END ELSE

  ---------------------------------------------
  IF @OBJName='CELL_LAYER'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid,  parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid, F.guid
      
        FROM CELLLAYER M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
        ORDER BY M.name;
	END ELSE


  ---------------------------------------------
  IF @OBJName='color_Schema'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid,  F.guid
      
        FROM ColorSchema M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
        ORDER BY M.name;
  END ELSE

 ---------------------------------------------
  IF @OBJName='Terminal_Type'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid,  parent_GUID)
      SELECT M.id, M.name, @OBJName, M.guid, F.guid
      
        FROM TerminalType M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
         ORDER BY M.name;

	END ELSE

 ---------------------------------------------
  IF @OBJName='template_pmp_Site'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT M.id, M.name, @OBJName, M.guid, F.guid
      
        FROM Template_pmp_Site M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
         ORDER BY M.name;

  END ELSE

  ---------------------------------------------
  IF @OBJName='Clutter_model'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid, F.guid
      
        FROM Cluttermodel M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
        ORDER BY M.name;
  END ELSE

  ---------------------------------------------
  IF @OBJName='Geo_Region'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid,  parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid, F.guid
      
        FROM GeoRegion M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
        ORDER BY M.name;
  END ELSE

  ---------------------------------------------
  IF @OBJName='Link_Type'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid,  F.guid
      
        FROM LinkType M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
        ORDER BY M.name;
  END ELSE



  ---------------------------------------------
  -- WITH PROJECT_ID
  ---------------------------------------------

  ---------------------------------------------
  IF @OBJName='Link'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid,  F.guid
      
        FROM Link M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
         WHERE M.PROJECT_ID=@PROJECT_ID
         ORDER BY M.name;
  END ELSE
  
  ---------------------------------------------
  IF @OBJName='LinkLine'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid, F.guid
      
        FROM LinkLine M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
         WHERE M.PROJECT_ID=@PROJECT_ID
         ORDER BY M.name;
  END ELSE

  ---------------------------------------------
  IF @OBJName='Property'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid,  F.guid
      
        FROM Property M 
           LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
         WHERE M.PROJECT_ID=@PROJECT_ID
         ORDER BY M.name;
  END  ELSE
 
  ---------------------------------------------
  IF @OBJName='msc'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid, F.guid
      
        FROM msc M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
         WHERE M.PROJECT_ID=@PROJECT_ID
         ORDER BY M.name;
  END  ELSE

  ---------------------------------------------
  IF @OBJName='pmp_site'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid, F.guid
      
        FROM view_pmp_site M 
           LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
         WHERE M.PROJECT_ID=@PROJECT_ID
         ORDER BY M.name;
  END ELSE
  
  

  
 ---------------------------------------------
  IF @OBJName='template_link'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT M.id, M.name, @OBJName, guid, null
      
--      M.guid, F.guid
      
        FROM lib.Template_link M 
        
--          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
         ORDER BY M.name;

  END ELSE  
  
  
 ---------------------------------------------
  IF @OBJName='template_Site'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT M.id, M.name, @OBJName, guid, null
      
--      M.guid, F.guid
      
        FROM Template_Site M 
        
--          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
         ORDER BY M.name;

  END ELSE  
  
  

 ---------------------------------------------
  IF @OBJName='template_Site_linkend'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT M.id, M.name, @OBJName, M.guid, null  --F.guid
      
        FROM Template_Site_linkend M 
     --     LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
         ORDER BY M.name;

  END ELSE  
 
 ---------------------------------------------
  IF @OBJName='Link_Repeater'
  ---------------------------------------------
  BEGIN  
    print   'Link_Repeater'
                      
       
    INSERT INTO @temp (id,name,ObjName, guid,  parent_GUID, property_name)

      SELECT  M.id, M.name, @OBJName, M.guid,  null, property.name
        FROM view_Link_Repeater_with_project_id M 
        
             LEFT OUTER JOIN property ON property.id = M.property_id        
                
         WHERE M.PROJECT_ID=@PROJECT_ID
         ORDER BY M.name;
             
         
  END  ELSE        
    RAISERROR ('sp_Explorer_Select_Object - @OBJNAME not found: %s' , 16, 1, @OBJNAME );
 


  SELECT * FROM @temp
  
  return @@ROWCOUNT
  
END

go


-----------------------------------------------------------

IF OBJECT_ID(N'[dbo].[sp_Explorer_Select_Folder_Items]') IS NOT NULL
  DROP procedure [dbo].[sp_Explorer_Select_Folder_Items]


GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Explorer_Select_Folder_Items 
(
  @ObjName 			varchar(30),
  @FOLDER_ID   int,  
  @PROJECT_ID  int 
 -- @ID  			 	int
)

AS
BEGIN

if @ObjName is null
begin
  --set @ObjName = 'property'

--  set @ObjName = 'antenna_type'
--  set @PROJECT_ID =2055 -- (select top 1 id from Project order by id desc)
  
 -- set @FOLDER_ID = (select top 1 id from Folder where [type] = 'property' and  PROJECT_ID = @PROJECT_ID )

  set @FOLDER_ID = 2134
  set @ObjName = 'link'

  if @PROJECT_ID is null
     select @PROJECT_ID =project_id from FOLDER where id= @folder_id

end



  DECLARE
  	@IsUseProjectID bit,
    @IsUseCheck bit,
    @IsUseGeoRegion_ID bit,
    @IsUseGeoRegion1_ID bit,
    @IsUseGeoRegion2_ID bit,
    @s VARCHAR(50), 
    @sql NVARCHAR(500), 
    @table_name VARCHAR(50), 
    @GUID  uniqueidentifier;


/*  create TABLE #temp  (id      int, 
                       name    varchar(100),
                       ObjName varchar(50),
                       GUID    uniqueidentifier 
                      -- parent_GUID  uniqueidentifier
                       );
*/
 -- create TABLE #temp1 (id int);

  if @FOLDER_ID=0
    set @FOLDER_ID=NULL

-- set @ID = 506046; 

--  set @ObjName='property';
 -- set @ID = dbo.fn_GetRandomPropertyID;
  
  
  SELECT 
   --   @IsUseProjectID = IsUseProjectID,
      @table_name     = table_name
    FROM _objects
    WHERE (Name=@ObjName) 


print  @table_name


if @ObjName='link'      set @table_name='view_Link_base'
--if @ObjName='link'      set @table_name='view_Link_for_explorer'




if @table_name='pmp_site'      set @table_name='view_pmp_site_with_project_id' 
if @ObjName='pmp_terminal'     set @table_name='view_pmp_terminal' 
if @ObjName='linkend'          set @table_name='view_linkend_with_project_id' 
if @ObjName='LinkEnd_ANTENNA'  set @table_name='view_LinkEnd_ANTENNA' 

if @table_name='relief'      set @table_name='view_Relief' 



  ---------------------------------------------
  IF OBJECT_ID(@table_name) is null  RAISERROR ('OBJECT_ID is null', 16, 1);
  ---------------------------------------------



print  @table_name




--SELECT @IsUseProjectID, @table_name  

  set @IsUseCheck    = dbo.sys_fn_TableHasColumn(@table_name, 'Checked' )

print @IsUseCheck 

   
  set @IsUseProjectID    = dbo.sys_fn_TableHasColumn(@table_name, 'Project_ID' )
  
 -- select @IsUseProjectID, @table_name
  
--  return
  
  
--  set @IsUseGeoRegion_ID = dbo.sys_fn_TableHasColumn(@table_name, 'GeoRegion_ID' )

/*
  set @IsUseGeoRegion1_ID = dbo.sys_fn_TableHasColumn(@table_name, 'GeoRegion1_ID' )
  set @IsUseGeoRegion2_ID = dbo.sys_fn_TableHasColumn(@table_name, 'GeoRegion2_ID' )
*/

print @IsUseProjectID 


/*declare
  @name_format varchar(200) = (select [value] from SETTINGS where name='explorer_property_format')


print @name_format
*/


--  set @IsUseProjectID = sys_fn_TableHasColumn(@table_name, 'GeoRegion_ID' )
  


  -- select * from [INFORMATION_SCHEMA].[COLUMNS] WHERE (table_name=@table_name) and (column_name='GeoRegion_ID')

  /*
  set @IsUseGeoRegion_ID = 0
  if @ObjName='property'  
    set @IsUseGeoRegion_ID = 1 
    */
    
    
   
--  set @sql='SELECT id,guid,name, user_created,'+
 
  set @sql='SELECT id,guid,%name%, '+
           ' dbo.fn_Object_Has_children ('':ObjName'', id) as Has_children, ' +
           ' '':ObjName'' as ObjName'+
--           '  :GeoRegion_ID '+
--      //     '  :GeoRegion1_ID'+
--       //    '  :GeoRegion2_ID'+
           '  :checked'+
           ' FROM :table_name '+
           ' WHERE (1=1) '

/*
  if @ObjName = 'property'
    set @sql=REPLACE(@sql, '%name%', @name_format + ' as name') 
  else
 */
  
    set @sql=REPLACE(@sql, '%name%', 'name') 

 
  set @sql=REPLACE(@sql, ':ObjName', @ObjName) 
  set @sql=REPLACE(@sql, ':table_name', @table_name) 

  
  SET @s = CASE @IsUseCheck WHEN 1 THEN ', Checked ' ELSE '' END
  set @sql=REPLACE(@sql, ':Checked', @s)  
  
  /*
  SET @s = CASE @IsUseGeoRegion_ID WHEN 1 THEN ', GeoRegion_ID ' ELSE '' END
  set @sql=REPLACE(@sql, ':GeoRegion_ID', @s) 

  SET @s = CASE @IsUseGeoRegion1_ID WHEN 1 THEN ', GeoRegion1_ID ' ELSE '' END
  set @sql=REPLACE(@sql, ':GeoRegion1_ID', @s) 
  
  SET @s = CASE @IsUseGeoRegion2_ID WHEN 1 THEN ', GeoRegion2_ID ' ELSE '' END
  set @sql=REPLACE(@sql, ':GeoRegion2_ID', @s) 
*/
  

/*
  if @IsUseGeoRegion_ID=1 
    set @sql=REPLACE(@sql, ':GeoRegion_ID', ', GeoRegion_ID ') 
  ELSE
    set @sql=REPLACE(@sql, ':GeoRegion_ID', '') 
*/ 

 -- exec sp_executesql @sql

 
  if @IsUseProjectID=1 
     set @sql=@sql+ ' AND (PROJECT_ID=' + cast (@PROJECT_ID as varchar (50)) + ')'

  if @FOLDER_ID is not NULL 
     set @sql=@sql+ ' AND (FOLDER_ID=' + cast (@FOLDER_ID as varchar (50))+ ')'
  ELSE 
     set @sql=@sql+ ' AND (FOLDER_ID IS NULL)'
     
     
   set @sql=@sql+ ' ORDER BY name' 
  
--  SELECT @sql 

print @sql
  
  exec sp_executesql @sql


  return @@ROWCOUNT

 -- SELECT * FROM #temp

 -- drop TABLE #temp;
 -- drop TABLE #temp1;
  
END


go




IF OBJECT_ID(N'[dbo].[view_Link_for_explorer]') IS NOT NULL
  DROP view [dbo].[view_Link_for_explorer]


GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */

CREATE VIEW dbo.view_Link_for_explorer
AS
SELECT 
  dbo.Link.id,
  dbo.Link.name,

  dbo.Link.project_id,  --!!!!!!!!!!!!!!
  dbo.Link.guid,
  
  dbo.Link.folder_id,
  dbo.Link.user_created,
  
  Property1.id AS Property1_id,
  Property2.id AS Property2_id,
  
  Property1.georegion_id AS georegion1_id,
  Property2.georegion_id AS georegion2_id

  
FROM
  dbo.Link

  left JOIN dbo.linkend linkend1 ON dbo.Link.linkend1_id = linkend1.id
  left JOIN dbo.linkend linkend2 ON dbo.Link.linkend2_id = linkend2.id

  left JOIN dbo.Property Property1 ON linkend1.Property_id = Property1.id
  left JOIN dbo.Property Property2 ON linkend2.Property_id = Property2.id

go
  
/*WHERE
  dbo.Link.objname = 'link' OR 
  Link.objname IS NULL
  */



/****** Object:  View [Security].[View_User_Security_XREF]    Script Date: 18.01.2021 14:15:25 ******/
IF OBJECT_ID(N'[dbo].[sp_Explorer_Select_SubItems]') IS NOT NULL
  DROP procedure [dbo].[sp_Explorer_Select_SubItems]


GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE [dbo].[sp_Explorer_Select_SubItems]
(
  @ObjName varchar(50),
  @ID  int  ,
  
  @PROJECT_ID  int = null
    
)

AS
BEGIN
  DECLARE
  	@link_id int,
    @GeoRegion_ID int,
    @parent_guid uniqueidentifier,
    @linkend1_id int,
    @linkend2_id int;



  create TABLE #temp  (id      			int, 
                       name    			nvarchar(100),
                       ObjName 			varchar(50),
                       GUID    			uniqueidentifier,
                       parent_GUID  uniqueidentifier,
                       Has_children BIT,
                       
                       ref_id      	int, 
                       ref_GUID    	uniqueidentifier, 
                       ref_ObjName 	varchar(50),

                       GeoRegion_ID int,
                       GeoRegion1_ID int,
                       GeoRegion2_ID int
                      );

 
 -----------------------------------------------
  if @ObjName = 'Link_SDB'
  -----------------------------------------------
  BEGIN
   INSERT INTO #temp (id,name,guid,ObjName) 
      SELECT id, 'common band: '+ name, guid, 'Link' --, @parent_guid, P.GeoRegion_ID 
      -- + name
        FROM  link 
        where id in (select link1_id from dbo.link_sdb where id=@ID)
 
   INSERT INTO #temp (id,name,guid,ObjName) 
      SELECT id, 'E band:            '+ name, guid, 'Link' --, @parent_guid, P.GeoRegion_ID 
      -- +name
        FROM  link 
        where id in (select link2_id from dbo.link_sdb where id=@ID)
 
  
--    SELECT @parent_guid = GUID FROM LinkEnd where id=@ID
   
           
/* 
   INSERT INTO #temp (id,name,guid,ObjName) --, parent_guid, GeoRegion_ID
      SELECT id, name, guid, 'Link_SDB' --, @parent_guid, P.GeoRegion_ID 
        FROM  link.link_sdb -- LinkEnd_antenna A LEFT OUTER JOIN
                --      Property P ON A.property_id = P.id
          
        WHERE project_id=@PROJECT_ID 
        ORDER BY name; 
*/
  end ELSE  


  -----------------------------------------------
  if @ObjName = 'LinkEnd'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM LinkEnd where id=@ID
   
           
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid, 'LinkEnd_ANTENNA', @parent_guid, P.GeoRegion_ID 
        FROM LinkEnd_antenna A LEFT OUTER JOIN
                      Property P ON A.property_id = P.id
          
        WHERE A.LinkEnd_id=@ID 
        ORDER BY A.name; 

  end ELSE  

  -----------------------------------------------
  if @ObjName = 'property'
  -----------------------------------------------
  BEGIN    
    SELECT @parent_guid = GUID, @GeoRegion_ID = GeoRegion_ID 
      FROM Property where id=@ID
 
		-------------------------
		-- LinkEnd
		------------------------- 
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'LinkEnd',@parent_guid,
              dbo.fn_Object_Has_children ('LinkEnd',id) as Has_children, 
              @GeoRegion_ID
      FROM view_LinkEnd_base  
        WHERE property_id=@ID 
        ORDER BY name;
  

    ---------------

/*    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'Site',@parent_guid, 
              dbo.fn_Object_Has_children ('Site',id) as Has_children,
              @GeoRegion_ID 
        FROM Site WHERE property_id=@ID ORDER BY name;
*/

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'MSC',@parent_guid, 
              dbo.fn_Object_Has_children ('Site',id) as Has_children,
              @GeoRegion_ID 
      FROM MSC WHERE property_id=@ID ORDER BY name;

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'BSC',@parent_guid, 
              dbo.fn_Object_Has_children ('BSC',id) as Has_children,
              @GeoRegion_ID 
      FROM BSC WHERE property_id=@ID ORDER BY name;

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'pmp_site',@parent_guid, 
              dbo.fn_Object_Has_children ('pmp_site',id) as Has_children,
              @GeoRegion_ID 
      FROM pmp_site WHERE property_id=@ID ORDER BY name;

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'pmp_terminal',@parent_guid, 
              dbo.fn_Object_Has_children ('pmp_terminal',id) as Has_children,
              @GeoRegion_ID 
      FROM view_pmp_terminal WHERE property_id=@ID ORDER BY name;

     ----- new -----

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'link_repeater',@parent_guid, 
              dbo.fn_Object_Has_children ('link_repeater',id) as Has_children,
              @GeoRegion_ID 
      FROM link_repeater WHERE property_id=@ID ORDER BY name;


/*
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children )
      SELECT id,name,guid,'mast',@parent_guid, 
              dbo.fn_Object_Has_children ('mast',id) as Has_children
               
      FROM mast WHERE property_id=@ID ORDER BY name;

*/



		-------------------------
		-- link
		-------------------------
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion1_ID, GeoRegion2_ID)
      SELECT id,name,guid,'link',@parent_guid,
             dbo.fn_Object_Has_children ('link',id) as Has_children,
             GeoRegion1_ID, GeoRegion2_ID 

       -- FROM view_link  
        FROM view_Link_for_explorer
        WHERE  (Property1_id=@ID) or (Property2_id=@ID)
        ORDER BY name
    


  end ELSE
   
  -----------------------------------------------
  if @ObjName = 'Link'
  -----------------------------------------------
  BEGIN
    DECLARE
 	  @link_repeater_id int;   
    
  --  SELECT @GUID = GUID FROM Link where id=@ID
    SELECT @parent_guid=GUID, @linkend1_id=linkend1_id, @linkend2_id=linkend2_id 
    --, @link_repeater_id=link_repeater_id
      FROM Link 
      WHERE id=@ID


    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT L.id, L.name, L.guid,'LinkEnd', @parent_guid, 
           dbo.fn_Object_Has_children ('LinkEnd',L.id) as Has_children,
           P.GeoRegion_ID 
      FROM LinkEnd L LEFT OUTER JOIN
                      Property P ON L.property_id = P.id
      WHERE L.id IN (@linkend1_id)        
      ORDER BY L.name; 
      

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT L.id, L.name, L.guid,'LinkEnd', @parent_guid, 
           dbo.fn_Object_Has_children ('LinkEnd',L.id) as Has_children,
           P.GeoRegion_ID 
      FROM LinkEnd L LEFT OUTER JOIN
                      Property P ON L.property_id = P.id
      WHERE L.id IN ( @linkend2_id)        
      ORDER BY L.name; 


      
/*      
                INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid, 'LinkEnd_ANTENNA', @parent_guid, P.GeoRegion_ID 
        FROM LinkEnd_antenna A LEFT OUTER JOIN
                      Property P ON A.property_id = P.id
          
*/
      
      

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Link_Repeater',@parent_guid,
           dbo.fn_Object_Has_children ('Link_Repeater',id) as Has_children 
      FROM Link_Repeater 
--      WHERE id = @link_repeater_id
      WHERE link_id = @id
      
  end ELSE

  -----------------------------------------------
  if @ObjName = 'LinkLine_Link'
  -----------------------------------------------
  BEGIN
  --  SELECT @GUID = GUID FROM Link where id=@ID
    SELECT @link_id = link_id FROM LinkLineLinkXREF where id=@ID;
    
    SELECT @parent_guid=GUID, @linkend1_id=linkend1_id, @linkend2_id=linkend2_id 
      FROM Link WHERE id=@ID


    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'LinkEnd',@parent_guid,
           dbo.fn_Object_Has_children ('LinkEnd',id) as Has_children 
      FROM LinkEnd 
      WHERE id IN (@linkend1_id,@linkend2_id)
      ORDER BY name; 



  end ELSE
 

  -----------------------------------------------
  if @ObjName = 'LinkLine'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM LinkLine where id=@ID


    SELECT X.*, Link.name, Link.GUID as Link_GUID
       INTO #temp1
    FROM Link RIGHT OUTER JOIN
          LinkLineLinkXREF X ON Link.id = X.link_id
    WHERE LinkLine_id=@ID      
    ORDER BY X.index_


 --   SELECT * from ##temp1


    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, 
    		ref_id, ref_ObjName, ref_guid,   Has_children)
      SELECT id,name,guid, 'LinkLine_Link', @parent_guid,
            LINK_id, 'LINK', Link_GUID, 1
      FROM #temp1  
     
  end ELSE   
  
  

  -----------------------------------------------
  if @ObjName = 'Link_Repeater'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM LInk_Repeater where id=@ID
   
           
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid)
      SELECT id,name,guid,'LInk_Repeater_ANTENNA',@parent_guid 
        FROM LInk_Repeater_antenna   
        WHERE  LInk_Repeater_id=@ID ORDER BY name; 

  end ELSE    
  
  -----------------------------------------------
  if @ObjName = 'pmp_sector'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM view_PMP_Sector where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid,'LinkEnd_ANTENNA', @parent_guid , P.GeoRegion_ID
        FROM LinkEnd_ANTENNA A LEFT OUTER JOIN
                      Property P ON A.property_id = P.id
        WHERE A.LinkEnd_id=@ID
      
     
      
      /*
      
                INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid, 'LinkEnd_ANTENNA', @parent_guid, P.GeoRegion_ID 
        FROM LinkEnd_antenna A LEFT OUTER JOIN
                      Property P ON A.property_id = P.id*/
      
      
  end ELSE    
  
  -----------------------------------------------
  if (@ObjName = 'pmp_terminal') --or (@ObjName = 'pmpTerminal')
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM view_Pmp_Terminal where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid,'LinkEnd_ANTENNA', @parent_guid, P.GeoRegion_ID 
      FROM LinkEnd_ANTENNA A  LEFT OUTER JOIN
                      Property P ON A.property_id = P.id
      WHERE A.LinkEnd_id=@ID
     
      
    --  ORDER BY name; 

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid)
      SELECT id,name,guid,'link',@parent_guid
        FROM link  
        WHERE  @ID = linkend2_id
--        WHERE  @ID in (linkend1_id, linkend2_id)
    
  end ELSE  
  

  -----------------------------------------------
  if @ObjName = 'LinkFreqPlan'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM LinkFreqPlan where id=@ID
    

    SELECT m.*, Link.name as name, Link.GUID as Link_GUID
       INTO #temp2       
      FROM LinkFreqPlan_Link M 
         LEFT OUTER JOIN Link ON M.link_id = dbo.Link.id        
      WHERE m.LinkFreqPlan_id=@ID 
      ORDER BY name;
    
 --   SELECT * from #temp2
    
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, 
    		ref_id, ref_guid, ref_ObjName)
      SELECT M.id, Link.name, M.guid,'LinkFreqPlan_link',@parent_guid, 1,  
            M.link_id, Link.GUID,  'link'      
        FROM LinkFreqPlan_Link M 
           LEFT OUTER JOIN Link ON M.link_id = dbo.Link.id
        
        WHERE m.LinkFreqPlan_id=@ID 
        ORDER BY name;

  END ELSE

  -----------------------------------------------
  if @ObjName = 'LinkNet'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM LinkNet where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'LinkFreqPlan',@parent_guid, 
              dbo.fn_Object_Has_children ('LinkFreqPlan',id) as Has_children 
      FROM LinkFreqPlan 
      	WHERE LinkNet_id=@ID ORDER BY name;
      

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, ref_id, ref_ObjName,   Has_children)
      SELECT id,name,guid,'LINKNET_LINKLINE_REF',@parent_guid, 
              LINKLINE_id,  'LINKLINE',
              dbo.fn_Object_Has_children ('LINKLINE',LINKLINE_id) as Has_children 
      FROM VIEW_SH_LINKNET_LINKLINE 
      	WHERE LinkNet_id=@ID ORDER BY name;


    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, ref_id, ref_ObjName, Has_children)
      SELECT id,name,guid,'LINKNET_LINK_REF',@parent_guid, 
              LINK_id,'LINK',      
              dbo.fn_Object_Has_children ('LINK',LINK_id) as Has_children 
      FROM VIEW_SH_LINKNET_LINK 
      	WHERE LinkNet_id=@ID ORDER BY name;
           
  end  ELSE



  
  -----------------------------------------------
  if @ObjName = 'template_site'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM template_site where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_site_LinkEnd',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_site_LinkEnd',id) as Has_children 

      FROM Template_site_LinkEnd  
      WHERE  Template_Site_id=@ID
      
     
 /*   ---------------
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children )
      SELECT id,name,guid,'Template_site_mast',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_site_mast',id) as Has_children
             
     
      FROM Template_site_mast
      WHERE  Template_Site_id=@ID
     
      */
      
  end ELSE   

/*
  -----------------------------------------------
  if @ObjName = 'template_site_mast'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM lib.template_site_mast where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_site_LinkEnd_antenna',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_site_LinkEnd_antenna',id) as Has_children 

      FROM lib.Template_site_LinkEnd_antenna  WHERE  Template_site_mast_id=@ID
      
  end ELSE   
*/

  
  -----------------------------------------------
  if @ObjName = 'template_pmp_site'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM template_pmp_site where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_LinkEnd',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_LinkEnd',id) as Has_children 

      FROM Template_LinkEnd  
      WHERE  Template_pmp_Site_id=@ID
      
  end ELSE   


  -----------------------------------------------
  if @ObjName = 'template_site_linkend'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM template_site_linkend where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_site_LinkEnd_antenna',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_site_LinkEnd_antenna',id) as Has_children 

      FROM Template_site_LinkEnd_antenna  WHERE  Template_site_LinkEnd_id=@ID
      
  end ELSE   

  -----------------------------------------------
  if @ObjName = 'template_linkend'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM template_linkend where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_LinkEnd_antenna',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_LinkEnd_antenna',id) as Has_children 

      FROM Template_LinkEnd_antenna  WHERE  Template_LinkEnd_id=@ID
      
  end ELSE   


/*
  -----------------------------------------------
  if @ObjName = 'template_site_linkend'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM template_site_linkend where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_LinkEnd_antenna',@parent_guid, 
              dbo.fn_Object_Has_children ('template_site_linkend',id) as Has_children 

      FROM template_site_linkend  WHERE  Template_LinkEnd_id=@ID
      
  end ELSE   */


  
  
   -----------------------------------------------
  if @ObjName = 'site' BEGIN
  -----------------------------------------------
    SELECT @parent_guid = GUID FROM [site] where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Cell',@parent_guid, 
              dbo.fn_Object_Has_children ('cell',id) as Has_children 

      FROM cell 
      WHERE Site_id=@ID
      
  END ELSE      

   -----------------------------------------------
  if @ObjName = 'cell' BEGIN
  -----------------------------------------------
    SELECT @parent_guid = GUID FROM [cell] where id=@ID
    
    if OBJECT_ID('view_Cell_Antennas')>0
    BEGIN
      INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
        SELECT id,name,guid,'Cell_antenna',@parent_guid, 
                0 as Has_children 

        FROM view_Cell_Antennas 
        WHERE cell_id=@ID
    END   
      
  END ELSE      


  -----------------------------------------------
  if (@ObjName = 'calc_region_pmp') or (@ObjName = 'pmp_calc_region')
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM pmp_CalcRegion where id=@ID


    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children,
          ref_id, ref_ObjName, ref_guid )
      SELECT m.id,PMP_Site.name,m.guid,'PMP_calc_region_Site',@parent_guid, 
--      SELECT m.id,PMP_Site.name,m.guid,'PMP_Site',@parent_guid, 
              dbo.fn_Object_Has_children ('PMP_Site',PMP_Site.id) as Has_children , 
              
           PMP_Site.id,'PMP_Site',  PMP_Site.GUID  

      FROM pmp_CalcRegionCells M
         LEFT OUTER JOIN PMP_Site ON M.PMP_Site_id = PMP_Site.id
                
      WHERE (m.pmp_Calc_Region_id=@ID) and (PMP_Site_id>0)
      

/*    
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children )
     --     ref_id, ref_ObjName, ref_guid
      SELECT id,name,guid,'PMP_Site',@parent_guid, 
              dbo.fn_Object_Has_children ('PMP_Site',id) as Has_children  
              
--              M.link_id, Link.GUID,  'link' 

      FROM PMP_Site        
      WHERE 
        id IN (SELECT PMP_Site_id 
                 FROM pmp_CalcRegionCells 
                 WHERE pmp_Calc_Region_id=@ID)
      
      
*/      
      
    --  ORDER BY name;     
  ----  SELECT id, name, guid FROM '+ view_LINKEND_ANTENNAS +
    --                 ' WHERE linkend_id
    
/*
    INSERT INTO #temp (id,name,guid,ObjName)
      SELECT id,name,guid,'LinkEnd' FROM LinkEnd 
             WHERE (id IN (SELECT linkend1_id FROM LINK  WHERE id = @id)) OR 
                   (id IN (SELECT LINKEND2_id FROM LINK  WHERE id = @id))    
        ORDER BY name; 
*/
  end ELSE    


  -----------------------------------------------
  if @ObjName = 'pmp_site'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM pmp_site where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children )
      SELECT M.id, M.name, M.guid, 'PMP_Sector', @parent_guid, 
              dbo.fn_Object_Has_children ('pmp_sector',M.id) as Has_children 
--              P.GeoRegion_ID

      FROM view_PMP_Sector M  
         LEFT OUTER JOIN PMP_Site S ON M.pmp_site_id = S.id  
         LEFT OUTER JOIN Property P ON S.property_id = P.id
      
      
      
            --          Property P ON M.property_id = P.id
      WHERE  M.PMP_Site_id=@ID
      
      /*
         INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid, 'LinkEnd_ANTENNA', @parent_guid, P.GeoRegion_ID 
        FROM LinkEnd_antenna A LEFT OUTER JOIN
                      Property P ON A.property_id = P.id*/
      
      
      
    --  ORDER BY name;     
  ----  SELECT id, name, guid FROM '+ view_LINKEND_ANTENNAS +
    --                 ' WHERE linkend_id
    
/*
    INSERT INTO #temp (id,name,guid,ObjName)
      SELECT id,name,guid,'LinkEnd' FROM LinkEnd 
             WHERE (id IN (SELECT linkend1_id FROM LINK  WHERE id = @id)) OR 
                   (id IN (SELECT LINKEND2_id FROM LINK  WHERE id = @id))    
        ORDER BY name; 
*/
  end ELSE   
     RAISERROR ('@TableName incorrect: %s', 16, 1, @ObjName );
  
  end_label:

  SELECT * FROM #temp

 -- drop TABLE #temp;
 -- drop TABLE #temp1;
  
END

go

-------------------------------------------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID(N'[dbo].[sp_Explorer_Select_SubItems]') IS NOT NULL
  DROP PROCEDURE [dbo].[sp_Explorer_Select_SubItems]


GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE [dbo].[sp_Explorer_Select_SubItems]
(
  @ObjName varchar(50),
  @ID  int  ,
  
  @PROJECT_ID  int = null
    
)

AS
BEGIN
  DECLARE
  	@link_id int,
    @GeoRegion_ID int,
    @parent_guid uniqueidentifier,
    @linkend1_id int,
    @linkend2_id int;



  create TABLE #temp  (id      			int, 
                       name    			nvarchar(100),
                       ObjName 			varchar(50),
                       GUID    			uniqueidentifier,
                       parent_GUID  uniqueidentifier,
                       Has_children BIT,
                       
                       ref_id      	int, 
                       ref_GUID    	uniqueidentifier, 
                       ref_ObjName 	varchar(50),

                       GeoRegion_ID int,
                       GeoRegion1_ID int,
                       GeoRegion2_ID int
                      );

 
 -----------------------------------------------
  if @ObjName = 'Link_SDB'
  -----------------------------------------------
  BEGIN
   INSERT INTO #temp (id,name,guid,ObjName) 
      SELECT id, 'common band: '+ name, guid, 'Link' --, @parent_guid, P.GeoRegion_ID 
      -- + name
        FROM  link 
        where id in (select link1_id from link_sdb where id=@ID)
 
   INSERT INTO #temp (id,name,guid,ObjName) 
      SELECT id, 'E band:            '+ name, guid, 'Link' --, @parent_guid, P.GeoRegion_ID 
      -- +name
        FROM  link 
        where id in (select link2_id from link_sdb where id=@ID)
 
  
--    SELECT @parent_guid = GUID FROM LinkEnd where id=@ID
   
           
/* 
   INSERT INTO #temp (id,name,guid,ObjName) --, parent_guid, GeoRegion_ID
      SELECT id, name, guid, 'Link_SDB' --, @parent_guid, P.GeoRegion_ID 
        FROM  link.link_sdb -- LinkEnd_antenna A LEFT OUTER JOIN
                --      Property P ON A.property_id = P.id
          
        WHERE project_id=@PROJECT_ID 
        ORDER BY name; 
*/
  end ELSE  


  -----------------------------------------------
  if @ObjName = 'LinkEnd'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM LinkEnd where id=@ID
   
           
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid, 'LinkEnd_ANTENNA', @parent_guid, P.GeoRegion_ID 
        FROM LinkEnd_antenna A LEFT OUTER JOIN
                      Property P ON A.property_id = P.id
          
        WHERE A.LinkEnd_id=@ID 
        ORDER BY A.name; 

  end ELSE  

  -----------------------------------------------
  if @ObjName = 'property'
  -----------------------------------------------
  BEGIN    
    SELECT @parent_guid = GUID, @GeoRegion_ID = GeoRegion_ID 
      FROM Property where id=@ID
 
		-------------------------
		-- LinkEnd
		------------------------- 
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'LinkEnd',@parent_guid,
              dbo.fn_Object_Has_children ('LinkEnd',id) as Has_children, 
              @GeoRegion_ID
      FROM view_LinkEnd_base  
        WHERE property_id=@ID 
        ORDER BY name;
  

    ---------------

/*    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'Site',@parent_guid, 
              dbo.fn_Object_Has_children ('Site',id) as Has_children,
              @GeoRegion_ID 
        FROM Site WHERE property_id=@ID ORDER BY name;
*/

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'MSC',@parent_guid, 
              dbo.fn_Object_Has_children ('Site',id) as Has_children,
              @GeoRegion_ID 
      FROM MSC WHERE property_id=@ID ORDER BY name;

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'BSC',@parent_guid, 
              dbo.fn_Object_Has_children ('BSC',id) as Has_children,
              @GeoRegion_ID 
      FROM BSC WHERE property_id=@ID ORDER BY name;

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'pmp_site',@parent_guid, 
              dbo.fn_Object_Has_children ('pmp_site',id) as Has_children,
              @GeoRegion_ID 
      FROM pmp_site WHERE property_id=@ID ORDER BY name;

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'pmp_terminal',@parent_guid, 
              dbo.fn_Object_Has_children ('pmp_terminal',id) as Has_children,
              @GeoRegion_ID 
      FROM view_pmp_terminal WHERE property_id=@ID ORDER BY name;

     ----- new -----

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'link_repeater',@parent_guid, 
              dbo.fn_Object_Has_children ('link_repeater',id) as Has_children,
              @GeoRegion_ID 
      FROM link_repeater WHERE property_id=@ID ORDER BY name;


/*
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children )
      SELECT id,name,guid,'mast',@parent_guid, 
              dbo.fn_Object_Has_children ('mast',id) as Has_children
               
      FROM mast WHERE property_id=@ID ORDER BY name;

*/



		-------------------------
		-- link
		-------------------------
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion1_ID, GeoRegion2_ID)
      SELECT id,name,guid,'link',@parent_guid,
             dbo.fn_Object_Has_children ('link',id) as Has_children,
             GeoRegion1_ID, GeoRegion2_ID 

       -- FROM view_link  
        FROM view_Link_for_explorer
        WHERE  (Property1_id=@ID) or (Property2_id=@ID)
        ORDER BY name
    


  end ELSE
   
  -----------------------------------------------
  if @ObjName = 'Link'
  -----------------------------------------------
  BEGIN
    DECLARE
 	  @link_repeater_id int;   
    
  --  SELECT @GUID = GUID FROM Link where id=@ID
    SELECT @parent_guid=GUID, @linkend1_id=linkend1_id, @linkend2_id=linkend2_id 
    --, @link_repeater_id=link_repeater_id
      FROM Link 
      WHERE id=@ID


    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT L.id, L.name, L.guid,'LinkEnd', @parent_guid, 
           dbo.fn_Object_Has_children ('LinkEnd',L.id) as Has_children,
           P.GeoRegion_ID 
      FROM LinkEnd L LEFT OUTER JOIN
                      Property P ON L.property_id = P.id
      WHERE L.id IN (@linkend1_id)        
      ORDER BY L.name; 
      

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT L.id, L.name, L.guid,'LinkEnd', @parent_guid, 
           dbo.fn_Object_Has_children ('LinkEnd',L.id) as Has_children,
           P.GeoRegion_ID 
      FROM LinkEnd L LEFT OUTER JOIN
                      Property P ON L.property_id = P.id
      WHERE L.id IN ( @linkend2_id)        
      ORDER BY L.name; 


      
/*      
                INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid, 'LinkEnd_ANTENNA', @parent_guid, P.GeoRegion_ID 
        FROM LinkEnd_antenna A LEFT OUTER JOIN
                      Property P ON A.property_id = P.id
          
*/
      
      

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Link_Repeater',@parent_guid,
           dbo.fn_Object_Has_children ('Link_Repeater',id) as Has_children 
      FROM Link_Repeater 
--      WHERE id = @link_repeater_id
      WHERE link_id = @id
      
  end ELSE

  -----------------------------------------------
  if @ObjName = 'LinkLine_Link'
  -----------------------------------------------
  BEGIN
  --  SELECT @GUID = GUID FROM Link where id=@ID
    SELECT @link_id = link_id FROM LinkLineLinkXREF where id=@ID;
    
    SELECT @parent_guid=GUID, @linkend1_id=linkend1_id, @linkend2_id=linkend2_id 
      FROM Link WHERE id=@ID


    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'LinkEnd',@parent_guid,
           dbo.fn_Object_Has_children ('LinkEnd',id) as Has_children 
      FROM LinkEnd 
      WHERE id IN (@linkend1_id,@linkend2_id)
      ORDER BY name; 



  end ELSE
 

  -----------------------------------------------
  if @ObjName = 'LinkLine'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM LinkLine where id=@ID


    SELECT X.*, Link.name, Link.GUID as Link_GUID
       INTO #temp1
    FROM Link RIGHT OUTER JOIN
          LinkLineLinkXREF X ON Link.id = X.link_id
    WHERE LinkLine_id=@ID      
    ORDER BY X.index_


 --   SELECT * from ##temp1


    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, 
    		ref_id, ref_ObjName, ref_guid,   Has_children)
      SELECT id,name,guid, 'LinkLine_Link', @parent_guid,
            LINK_id, 'LINK', Link_GUID, 1
      FROM #temp1  
     
  end ELSE   
  
  

  -----------------------------------------------
  if @ObjName = 'Link_Repeater'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM LInk_Repeater where id=@ID
   
           
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid)
      SELECT id,name,guid,'LInk_Repeater_ANTENNA',@parent_guid 
        FROM LInk_Repeater_antenna   
        WHERE  LInk_Repeater_id=@ID ORDER BY name; 

  end ELSE    
  
  -----------------------------------------------
  if @ObjName = 'pmp_sector'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM view_PMP_Sector where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid,'LinkEnd_ANTENNA', @parent_guid , P.GeoRegion_ID
        FROM LinkEnd_ANTENNA A LEFT OUTER JOIN
                      Property P ON A.property_id = P.id
        WHERE A.LinkEnd_id=@ID
      
     
      
      /*
      
                INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid, 'LinkEnd_ANTENNA', @parent_guid, P.GeoRegion_ID 
        FROM LinkEnd_antenna A LEFT OUTER JOIN
                      Property P ON A.property_id = P.id*/
      
      
  end ELSE    
  
  -----------------------------------------------
  if (@ObjName = 'pmp_terminal') --or (@ObjName = 'pmpTerminal')
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM view_Pmp_Terminal where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid,'LinkEnd_ANTENNA', @parent_guid, P.GeoRegion_ID 
      FROM LinkEnd_ANTENNA A  LEFT OUTER JOIN
                      Property P ON A.property_id = P.id
      WHERE A.LinkEnd_id=@ID
     
      
    --  ORDER BY name; 

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid)
      SELECT id,name,guid,'link',@parent_guid
        FROM link  
        WHERE  @ID = linkend2_id
--        WHERE  @ID in (linkend1_id, linkend2_id)
    
  end ELSE  
  

  -----------------------------------------------
  if @ObjName = 'LinkFreqPlan'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM LinkFreqPlan where id=@ID
    

    SELECT m.*, Link.name as name, Link.GUID as Link_GUID
       INTO #temp2       
      FROM LinkFreqPlan_Link M 
         LEFT OUTER JOIN Link ON M.link_id = dbo.Link.id        
      WHERE m.LinkFreqPlan_id=@ID 
      ORDER BY name;
    
 --   SELECT * from #temp2
    
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, 
    		ref_id, ref_guid, ref_ObjName)
      SELECT M.id, Link.name, M.guid,'LinkFreqPlan_link',@parent_guid, 1,  
            M.link_id, Link.GUID,  'link'      
        FROM LinkFreqPlan_Link M 
           LEFT OUTER JOIN Link ON M.link_id = dbo.Link.id
        
        WHERE m.LinkFreqPlan_id=@ID 
        ORDER BY name;

  END ELSE

  -----------------------------------------------
  if @ObjName = 'LinkNet'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM LinkNet where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'LinkFreqPlan',@parent_guid, 
              dbo.fn_Object_Has_children ('LinkFreqPlan',id) as Has_children 
      FROM LinkFreqPlan 
      	WHERE LinkNet_id=@ID ORDER BY name;
      

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, ref_id, ref_ObjName,   Has_children)
      SELECT id,name,guid,'LINKNET_LINKLINE_REF',@parent_guid, 
              LINKLINE_id,  'LINKLINE',
              dbo.fn_Object_Has_children ('LINKLINE',LINKLINE_id) as Has_children 
      FROM VIEW_SH_LINKNET_LINKLINE 
      	WHERE LinkNet_id=@ID ORDER BY name;


    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, ref_id, ref_ObjName, Has_children)
      SELECT id,name,guid,'LINKNET_LINK_REF',@parent_guid, 
              LINK_id,'LINK',      
              dbo.fn_Object_Has_children ('LINK',LINK_id) as Has_children 
      FROM VIEW_SH_LINKNET_LINK 
      	WHERE LinkNet_id=@ID ORDER BY name;
           
  end  ELSE



  
  -----------------------------------------------
  if @ObjName = 'template_site'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM template_site where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_site_LinkEnd',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_site_LinkEnd',id) as Has_children 

      FROM Template_site_LinkEnd  
      WHERE  Template_Site_id=@ID
      
     
 /*   ---------------
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children )
      SELECT id,name,guid,'Template_site_mast',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_site_mast',id) as Has_children
             
     
      FROM Template_site_mast
      WHERE  Template_Site_id=@ID
     
      */
      
  end ELSE   

/*
  -----------------------------------------------
  if @ObjName = 'template_site_mast'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM lib.template_site_mast where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_site_LinkEnd_antenna',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_site_LinkEnd_antenna',id) as Has_children 

      FROM lib.Template_site_LinkEnd_antenna  WHERE  Template_site_mast_id=@ID
      
  end ELSE   
*/

  
  -----------------------------------------------
  if @ObjName = 'template_pmp_site'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM template_pmp_site where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_LinkEnd',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_LinkEnd',id) as Has_children 

      FROM Template_LinkEnd  
      WHERE  Template_pmp_Site_id=@ID
      
  end ELSE   


  -----------------------------------------------
  if @ObjName = 'template_site_linkend'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM template_site_linkend where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_site_LinkEnd_antenna',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_site_LinkEnd_antenna',id) as Has_children 

      FROM Template_site_LinkEnd_antenna  WHERE  Template_site_LinkEnd_id=@ID
      
  end ELSE   

  -----------------------------------------------
  if @ObjName = 'template_linkend'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM template_linkend where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_LinkEnd_antenna',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_LinkEnd_antenna',id) as Has_children 

      FROM Template_LinkEnd_antenna  WHERE  Template_LinkEnd_id=@ID
      
  end ELSE   


/*
  -----------------------------------------------
  if @ObjName = 'template_site_linkend'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM template_site_linkend where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_LinkEnd_antenna',@parent_guid, 
              dbo.fn_Object_Has_children ('template_site_linkend',id) as Has_children 

      FROM template_site_linkend  WHERE  Template_LinkEnd_id=@ID
      
  end ELSE   */


  
  
   -----------------------------------------------
  if @ObjName = 'site' BEGIN
  -----------------------------------------------
    SELECT @parent_guid = GUID FROM [site] where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Cell',@parent_guid, 
              dbo.fn_Object_Has_children ('cell',id) as Has_children 

      FROM cell 
      WHERE Site_id=@ID
      
  END ELSE      

   -----------------------------------------------
  if @ObjName = 'cell' BEGIN
  -----------------------------------------------
    SELECT @parent_guid = GUID FROM [cell] where id=@ID
    
    if OBJECT_ID('view_Cell_Antennas')>0
    BEGIN
      INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
        SELECT id,name,guid,'Cell_antenna',@parent_guid, 
                0 as Has_children 

        FROM view_Cell_Antennas 
        WHERE cell_id=@ID
    END   
      
  END ELSE      


  -----------------------------------------------
  if (@ObjName = 'calc_region_pmp') or (@ObjName = 'pmp_calc_region')
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM pmp_CalcRegion where id=@ID


    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children,
          ref_id, ref_ObjName, ref_guid )
      SELECT m.id,PMP_Site.name,m.guid,'PMP_calc_region_Site',@parent_guid, 
--      SELECT m.id,PMP_Site.name,m.guid,'PMP_Site',@parent_guid, 
              dbo.fn_Object_Has_children ('PMP_Site',PMP_Site.id) as Has_children , 
              
           PMP_Site.id,'PMP_Site',  PMP_Site.GUID  

      FROM pmp_CalcRegionCells M
         LEFT OUTER JOIN PMP_Site ON M.PMP_Site_id = PMP_Site.id
                
      WHERE (m.pmp_Calc_Region_id=@ID) and (PMP_Site_id>0)
      

/*    
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children )
     --     ref_id, ref_ObjName, ref_guid
      SELECT id,name,guid,'PMP_Site',@parent_guid, 
              dbo.fn_Object_Has_children ('PMP_Site',id) as Has_children  
              
--              M.link_id, Link.GUID,  'link' 

      FROM PMP_Site        
      WHERE 
        id IN (SELECT PMP_Site_id 
                 FROM pmp_CalcRegionCells 
                 WHERE pmp_Calc_Region_id=@ID)
      
      
*/      
      
    --  ORDER BY name;     
  ----  SELECT id, name, guid FROM '+ view_LINKEND_ANTENNAS +
    --                 ' WHERE linkend_id
    
/*
    INSERT INTO #temp (id,name,guid,ObjName)
      SELECT id,name,guid,'LinkEnd' FROM LinkEnd 
             WHERE (id IN (SELECT linkend1_id FROM LINK  WHERE id = @id)) OR 
                   (id IN (SELECT LINKEND2_id FROM LINK  WHERE id = @id))    
        ORDER BY name; 
*/
  end ELSE    


  -----------------------------------------------
  if @ObjName = 'pmp_site'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM pmp_site where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children )
      SELECT M.id, M.name, M.guid, 'PMP_Sector', @parent_guid, 
              dbo.fn_Object_Has_children ('pmp_sector',M.id) as Has_children 
--              P.GeoRegion_ID

      FROM view_PMP_Sector M  
         LEFT OUTER JOIN PMP_Site S ON M.pmp_site_id = S.id  
         LEFT OUTER JOIN Property P ON S.property_id = P.id
      
      
      
            --          Property P ON M.property_id = P.id
      WHERE  M.PMP_Site_id=@ID
      
      /*
         INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid, 'LinkEnd_ANTENNA', @parent_guid, P.GeoRegion_ID 
        FROM LinkEnd_antenna A LEFT OUTER JOIN
                      Property P ON A.property_id = P.id*/
      
      
      
    --  ORDER BY name;     
  ----  SELECT id, name, guid FROM '+ view_LINKEND_ANTENNAS +
    --                 ' WHERE linkend_id
    
/*
    INSERT INTO #temp (id,name,guid,ObjName)
      SELECT id,name,guid,'LinkEnd' FROM LinkEnd 
             WHERE (id IN (SELECT linkend1_id FROM LINK  WHERE id = @id)) OR 
                   (id IN (SELECT LINKEND2_id FROM LINK  WHERE id = @id))    
        ORDER BY name; 
*/
  end ELSE   
     RAISERROR ('@TableName incorrect: %s', 16, 1, @ObjName );
  
  end_label:

  SELECT * FROM #temp

 -- drop TABLE #temp;
 -- drop TABLE #temp1;
  
END
go


IF OBJECT_ID(N'dbo.fn_Folder_FullPath_GUID') IS NOT NULL
  DROP FUNCTION dbo.fn_Folder_FullPath_GUID 

GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE FUNCTION dbo.fn_Folder_FullPath_GUID 
(
  @ID INT
)
RETURNS varchar(2000)
AS
BEGIN
  DECLARE  
   @parent_id   int,
   @GUID        VARCHAR(2000);
 
  SELECT
   -- @id   = id,
         @parent_id  = parent_id,        
         @GUID = CAST(GUID as varchar(50))
    FROM Folder 
    WHERE id=@ID

 
  IF @parent_id IS NOT NULL
    set @GUID = dbo.fn_Folder_FullPath_GUID(@parent_id) + '   '+ @GUID  

  RETURN @GUID
END

go

--------------------------------------------------

IF OBJECT_ID(N'dbo.sp_Explorer_Search') IS NOT NULL
  DROP PROCEDURE dbo.sp_Explorer_Search


GO


CREATE PROCEDURE dbo.sp_Explorer_Search
(
  @SEARCH varchar(100)  =null,
  @WHOLE_WORD bit=null,
  @PROJECT_ID int =null
)
AS
BEGIN
 

 -- SELECT * FROM 

  declare @t TABLE (  
       objname  varchar(50) , 
       id int, 
       name varchar(200), 
       
       PROJECT_ID int,
       folder_path  varchar(1000)  
       )


  declare
     @s varchar(50);

  if IsNull(@WHOLE_WORD,0)=1 
    set @s=@SEARCH
  ELSE  
    set @s='%'+ @SEARCH +'%'

--//CREATE FUNCTION dbo.fn_Folder_FullPath 

  


INSERT INTO @t (objname, id, name,  PROJECT_ID, folder_path )  
  SELECT 'property' as objname, id, name,  PROJECT_ID , dbo.fn_Folder_FullPath_GUID (folder_id)  
    FROM property 
    WHERE  ((name like (@s)) or  
            (NRI like (@s)) --or 
           -- (bee_beenet like (@s)) 
           )
          and  (PROJECT_ID =@PROJECT_ID)



INSERT INTO @t (objname, id, name,  PROJECT_ID , folder_path )  
  SELECT 'link' as objname, id, name,  PROJECT_ID , dbo.fn_Folder_FullPath_GUID (folder_id)
    FROM link 
    WHERE (name like (@s))    and  (PROJECT_ID =@PROJECT_ID)

 
SELECT * FROM @t

/*
  
  SELECT 'property' as objname, id, name,  PROJECT_ID   
    FROM property 
    WHERE (name like ('%'+ @SEARCH +'%'))  and  (PROJECT_ID =@PROJECT_ID)

union


  SELECT 'link' as objname, id, name,  PROJECT_ID
    FROM link 
    WHERE (name like ('%'+ @SEARCH +'%'))    and  (PROJECT_ID =@PROJECT_ID)
*/

 -- order by Display_Order



  /* Procedure body */
END

go

--------------------------------------------------IF OBJECT_ID(N'dbo.sp_Folder_MoveObjectToFolder') IS  not  NULL
  DROP PROCEDURE [dbo].[sp_Folder_MoveObjectToFolder]
GO


CREATE procedure dbo.sp_Folder_MoveObjectToFolder
(
  @OBJNAME   varchar(50),
  @ID        int,
  @FOLDER_ID int 
)
AS
begin
  DECLARE
    @sTableName nvarchar(50),
    @sFolder_ID nvarchar(20),
    @sID nvarchar(20),
    @sql NVARCHAR(1000);


  IF @FOLDER_ID=0 set @FOLDER_ID=NULL

  SELECT @sTableName=Table_Name 
  	from _objects
    WHERE name=@OBJNAME

--  SET @sTableName=@OBJNAME
  SET @sID=CONVERT(VARCHAR(255), @ID)

  IF @FOLDER_ID is NULL
   SET @sFolder_ID='null'  
  ELSE  
   SET @sFolder_ID=CONVERT(VARCHAR(255), @FOLDER_ID)


  set @sql = 'UPDATE '+ @sTableName +' SET folder_id= '+ @sFolder_ID +' WHERE id='+ @sID

  exec sp_executesql @sql 
  
  RETURN 0  
end

go

---------------------------------------


IF OBJECT_ID(N'[LinkFreqPlan].[view_LinkFreqPlan_LinkEnd]') IS NOT NULL

DROP VIEW [LinkFreqPlan].[view_LinkFreqPlan_LinkEnd]
GO
/****** Object:  View [LinkFreqPlan].[view_LinkFreqPlan_LinkEnd]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [LinkFreqPlan].[view_LinkFreqPlan_LinkEnd]
AS

SELECT  top 1000000   

--  dbo.fn_LinkEnd_Get_Link_ID (dbo.LinkEnd.id) AS link_id, 
 -- dbo.fn_LinkEnd_Get_Next_LinkEnd_ID (dbo.LinkEnd.id) AS next_linkend_id, 


M.*,

link.link_id, 
link.link_name, 
link.next_linkend_id, 
link.clutter_model_id,
link.length,
link.length_km,



dbo.LinkEnd.name AS LinkEnd_name,
dbo.LinkEnd.property_id, -- AS property_id, 

dbo.LinkEnd.subband, -- AS subband, 

dbo.LinkEnd.LinkEndType_mode_id, -- AS LinkEndType_mode_id,

                      
                      dbo.Property.name AS Property_name, 
                      
--                      dbo.Property.lat AS Property_lat, 
--                      dbo.Property.lon AS Property_lon, 
                     
--
---                     A1.lat AS Property_lat,
   --                   A1.lon AS Property_lon,
 
                      A1.lat,
                      A1.lon,
                      A1.location_type,                     
                      
                      
                      dbo.Property.ground_height AS ground_height,
                      
                      
/*                      dbo.Link.name AS Link_name, 
                      dbo.Link.length AS length, 
                      dbo.Link.length_km AS length_km,                       
                      dbo.Link.clutter_model_id AS clutter_model_id,
*/
                      
                      dbo.LinkEndType_Band.channel_width AS channel_width, 
                      dbo.LinkEndType_Band.txrx_shift, -- AS txrx_shift,
                      dbo.LinkEndType_Band.channel_count, -- AS channel_count, 
                      
                      dbo.LinkEndType_Band.freq_max_high, 
                      dbo.LinkEndType_Band.freq_min_low, 
                      dbo.LinkEndType_Band.name AS LinkEndType_Band_name  ,                    
                      
                      
                      dbo.fn_Linkend_GetMaxAntennaHeight(dbo.LinkEnd.id) AS    MaxAntennaHeight, 
                      
                      dbo.LinkFreqPlan.threshold_degradation AS  LinkFreqPlan_threshold_degradation,
                      dbo.LinkFreqPlan.min_CI_dB AS LinkFreqPlan_min_CI_dB, 
                      

                      dbo.LinkEnd.channel_number, -- AS channel_number, 
                      dbo.LinkEnd.tx_freq_MHz, -- AS tx_freq_MHz, 
                      dbo.LinkEnd.rx_freq_MHz, -- AS rx_freq_MHz, 

                      dbo.LinkEnd.power_dBm, -- AS power_dBm, 
                      dbo.LinkEnd.kng AS kng, 
                      dbo.LinkEnd.loss AS loss, 
                      dbo.LinkEnd.freq_channel_count, -- AS freq_channel_count, 
                      dbo.LinkEnd.channel_type,
                      dbo.LinkEnd.freq_spacing , -- AS freq_spacing,
                      dbo.LinkEnd.kratnostByFreq, -- AS kratnostByFreq,
                      dbo.LinkEnd.kratnostBySpace, -- AS kratnostBySpace, 
                      dbo.LinkEnd.signature_width, -- AS signature_width,
                      dbo.LinkEnd.signature_height, -- AS signature_height, 
                      dbo.LinkEnd.modulation_count, -- AS modulation_count,
                      dbo.LinkEnd.threshold_BER_3, -- AS threshold_BER_3, 
                      dbo.LinkEnd.threshold_BER_6, -- AS threshold_BER_6,
                      
                      dbo.LinkEnd.band, -- AS band,                                         
                      dbo.LinkEnd.linkendtype_id, -- AS linkendtype_id, 
                      dbo.LinkEnd.LinkEndType_band_id, -- AS LinkEndType_band_id, 

                      dbo.LinkEndType_Mode.ch_spacing,    
                      dbo.LinkEndType_Mode.bandwidth AS bandwidth, 
                      dbo.LinkEndType_Mode.bandwidth_tx_3, -- AS bandwidth_tx_3,
                      dbo.LinkEndType_Mode.bandwidth_tx_30, -- AS bandwidth_tx_30, 
                      dbo.LinkEndType_Mode.bandwidth_tx_a, -- AS bandwidth_tx_a,
                      dbo.LinkEndType_Mode.bandwidth_rx_3, -- AS bandwidth_rx_3, 
                      dbo.LinkEndType_Mode.bandwidth_rx_30, -- AS bandwidth_rx_30,
                      dbo.LinkEndType_Mode.bandwidth_rx_a, -- AS bandwidth_rx_a, 

--                 dbo.LinkEndType.FREQ_MAX_HIGH,
 --                     dbo.LinkEndType.FREQ_MIN_LOW,

                      dbo.LinkEndType.level_tx_a,
                      dbo.LinkEndType.level_rx_a,
                      dbo.LinkEndType.name AS LinkEndType_name,
                      dbo.LinkEndType.coeff_noise,
                      
                      
--//                      a.id as ssss,

/*
                      a.POLARIZATION_STR   as antenna_POLARIZATION_STR,
                      a.Polarization_ratio as antenna_Polarization_ratio,
                      a.Gain 			 as antenna_Gain,
                      a.Diameter 		 as antenna_Diameter,
                      a.Loss 			 as antenna_Loss,
                      a.Vert_width 		 as antenna_Vert_width,
                      a.AZIMUTH 		 as antenna_AZIMUTH,
                      a.Height 		 	 as antenna_Height,
                 --     a.Polarization_ratio as antenna_Polarization_ratio
                      
*/

(select count(*) from Linkend_Antenna where Linkend_id = dbo.LinkEnd.id) as Antenna_count
                   
                      
                      
FROM         dbo.LinkEndType_Band RIGHT OUTER JOIN
                      dbo.LinkEndType_Mode RIGHT OUTER JOIN
                      dbo.LinkEnd LEFT OUTER JOIN
                      dbo.LinkEndType ON dbo.LinkEnd.linkendtype_id = dbo.LinkEndType.id ON dbo.LinkEndType_Mode.id = dbo.LinkEnd.LinkEndType_mode_id ON 
                      dbo.LinkEndType_Band.id = dbo.LinkEnd.LinkEndType_band_id RIGHT OUTER JOIN
                   --   dbo.Link RIGHT OUTER JOIN
                      dbo.LinkFreqPlan_LinkEnd M LEFT OUTER JOIN
                      dbo.LinkFreqPlan ON M.LinkFreqPlan_id = dbo.LinkFreqPlan.id 
--                      ON dbo.Link.id = dbo.LinkFreqPlan_LinkEnd.link_id 
                      ON  dbo.LinkEnd.id = M.linkend_id LEFT OUTER JOIN
                      dbo.Property ON dbo.LinkEnd.property_id = dbo.Property.id
                      
                      OUTER apply fn_Linkend_Link_info(dbo.LinkEnd.id) link
                      
                       cross apply [dbo].fn_Linkend_Antenna_pos (LinkEnd.ID) A1
                       
          /*             
                       left join (
                          select Linkend_Antenna.*, AntennaType.Polarization_ratio 
                             from Linkend_Antenna
                               left join AntennaType  on AntennaType.id = Linkend_Antenna.antennaType_id
                       
                       ) A on a.linkend_id = LinkEnd.id
     
*/
                      
                      
     order by  M.id

GO
/****** Object:  View [LinkFreqPlan].[view_LinkFreqPlan_LinkEnd_antennas]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[LinkFreqPlan].[view_LinkFreqPlan_LinkEnd_antennas]') IS NOT NULL

DROP VIEW [LinkFreqPlan].[view_LinkFreqPlan_LinkEnd_antennas]
GO
/****** Object:  View [LinkFreqPlan].[view_LinkFreqPlan_LinkEnd_antennas]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [LinkFreqPlan].[view_LinkFreqPlan_LinkEnd_antennas]
AS
SELECT   

--LinkEnd.id as aaaaaaaaaaaaaa,


  dbo.Linkend_Antenna.*, dbo.Property.ground_height AS ground_height, dbo.LinkFreqPlan_LinkEnd.LinkFreqPlan_id AS LinkFreqPlan_id, 
                      dbo.AntennaType.polarization_ratio AS polarization_ratio, dbo.AntennaType.vert_mask, dbo.AntennaType.horz_mask, 
                      
                      
                      
                        
       A1.lat AS map_lat,
       A1.lon AS map_lon,
                      
                      
                      dbo.Property.lat AS Property_lat, 
                      dbo.Property.lon AS Property_lon
                      
                      
                      
FROM         dbo.Property RIGHT OUTER JOIN
                      dbo.LinkEnd LEFT OUTER JOIN
                      dbo.AntennaType RIGHT OUTER JOIN
                      dbo.Linkend_Antenna ON dbo.AntennaType.id = dbo.Linkend_Antenna.antennaType_id ON dbo.LinkEnd.id = dbo.Linkend_Antenna.linkend_id ON 
                      dbo.Property.id = dbo.Linkend_Antenna.property_id RIGHT OUTER JOIN
                      dbo.LinkFreqPlan_LinkEnd ON dbo.LinkEnd.id = dbo.LinkFreqPlan_LinkEnd.linkend_id
                      

                      

     cross apply [dbo].fn_Linkend_Antenna_pos (Linkend_Antenna.linkend_id) A1
     
--where LinkFreqPlan_id=1016

GO
/****** Object:  UserDefinedFunction [geo].[fn_Length_m]    Script Date: 16.05.2020 10:54:58 ******/


-------------------------------------------------------------------------------

IF OBJECT_ID(N'[dbo].[view_LinkFreqPlan_LinkEnd]') IS NOT NULL
  DROP VIEW [dbo].[view_LinkFreqPlan_LinkEnd]
GO


CREATE VIEW dbo.view_LinkFreqPlan_LinkEnd
AS

SELECT  top 1000000   

--  dbo.fn_LinkEnd_Get_Link_ID (dbo.LinkEnd.id) AS link_id, 
 -- dbo.fn_LinkEnd_Get_Next_LinkEnd_ID (dbo.LinkEnd.id) AS next_linkend_id, 


M.*,

link.link_id, 
link.link_name, 
link.next_linkend_id, 
link.clutter_model_id,
link.length,
link.length_km,



dbo.LinkEnd.name AS LinkEnd_name,
dbo.LinkEnd.property_id, -- AS property_id, 

dbo.LinkEnd.subband, -- AS subband, 

dbo.LinkEnd.LinkEndType_mode_id, -- AS LinkEndType_mode_id,

                      
                      dbo.Property.name AS Property_name, 
                      
                      dbo.Property.lat AS Property_lat, 
                      dbo.Property.lon AS Property_lon, 
                     
--
---                     A1.lat AS Property_lat,
   --                   A1.lon AS Property_lon,
 
                      A1.lat,
                      A1.lon,
                      A1.location_type,                     
                      
                      
                      dbo.Property.ground_height AS ground_height,
                      
                      
/*                      dbo.Link.name AS Link_name, 
                      dbo.Link.length AS length, 
                      dbo.Link.length_km AS length_km,                       
                      dbo.Link.clutter_model_id AS clutter_model_id,
*/
                      
                      dbo.LinkEndType_Band.channel_width AS channel_width, 
                      dbo.LinkEndType_Band.txrx_shift, -- AS txrx_shift,
                      dbo.LinkEndType_Band.channel_count, -- AS channel_count, 
                      
                      dbo.LinkEndType_Band.freq_max_high, 
                      dbo.LinkEndType_Band.freq_min_low, 
                      dbo.LinkEndType_Band.name AS LinkEndType_Band_name  ,                    
                      
                      
                      dbo.fn_Linkend_GetMaxAntennaHeight(dbo.LinkEnd.id) AS    MaxAntennaHeight, 
                      
                      dbo.LinkFreqPlan.threshold_degradation AS  LinkFreqPlan_threshold_degradation,
                      dbo.LinkFreqPlan.min_CI_dB AS LinkFreqPlan_min_CI_dB, 
                      

                      dbo.LinkEnd.channel_number, -- AS channel_number, 
                      dbo.LinkEnd.tx_freq_MHz, -- AS tx_freq_MHz, 
                      dbo.LinkEnd.rx_freq_MHz, -- AS rx_freq_MHz, 

                      dbo.LinkEnd.power_dBm, -- AS power_dBm, 
                      dbo.LinkEnd.kng AS kng, 
                      dbo.LinkEnd.loss AS loss, 
                      dbo.LinkEnd.freq_channel_count, -- AS freq_channel_count, 
                      dbo.LinkEnd.channel_type,
                      dbo.LinkEnd.freq_spacing , -- AS freq_spacing,
                      dbo.LinkEnd.kratnostByFreq, -- AS kratnostByFreq,
                      dbo.LinkEnd.kratnostBySpace, -- AS kratnostBySpace, 
                      dbo.LinkEnd.signature_width, -- AS signature_width,
                      dbo.LinkEnd.signature_height, -- AS signature_height, 
                      dbo.LinkEnd.modulation_count, -- AS modulation_count,
                      dbo.LinkEnd.threshold_BER_3, -- AS threshold_BER_3, 
                      dbo.LinkEnd.threshold_BER_6, -- AS threshold_BER_6,
                      
                      dbo.LinkEnd.band, -- AS band,                                         
                      dbo.LinkEnd.linkendtype_id, -- AS linkendtype_id, 
                      dbo.LinkEnd.LinkEndType_band_id, -- AS LinkEndType_band_id, 

                      dbo.LinkEndType_Mode.ch_spacing,    
                      dbo.LinkEndType_Mode.bandwidth AS bandwidth, 
                      dbo.LinkEndType_Mode.bandwidth_tx_3, -- AS bandwidth_tx_3,
                      dbo.LinkEndType_Mode.bandwidth_tx_30, -- AS bandwidth_tx_30, 
                      dbo.LinkEndType_Mode.bandwidth_tx_a, -- AS bandwidth_tx_a,

                      dbo.LinkEndType_Mode.bandwidth_rx_3, -- AS bandwidth_rx_3, 
                      dbo.LinkEndType_Mode.bandwidth_rx_30, -- AS bandwidth_rx_30,
                      dbo.LinkEndType_Mode.bandwidth_rx_a, -- AS bandwidth_rx_a, 

--                 dbo.LinkEndType.FREQ_MAX_HIGH,
 --                     dbo.LinkEndType.FREQ_MIN_LOW,

                      dbo.LinkEndType.level_tx_a,
                      dbo.LinkEndType.level_rx_a,
                      dbo.LinkEndType.name AS LinkEndType_name,
                      dbo.LinkEndType.coeff_noise

                   
                      
                      
FROM         dbo.LinkEndType_Band RIGHT OUTER JOIN
                      dbo.LinkEndType_Mode RIGHT OUTER JOIN
                      dbo.LinkEnd LEFT OUTER JOIN
                      dbo.LinkEndType ON dbo.LinkEnd.linkendtype_id = dbo.LinkEndType.id ON dbo.LinkEndType_Mode.id = dbo.LinkEnd.LinkEndType_mode_id ON 
                      dbo.LinkEndType_Band.id = dbo.LinkEnd.LinkEndType_band_id RIGHT OUTER JOIN
                   --   dbo.Link RIGHT OUTER JOIN
                      dbo.LinkFreqPlan_LinkEnd M LEFT OUTER JOIN
                      dbo.LinkFreqPlan ON M.LinkFreqPlan_id = dbo.LinkFreqPlan.id 
--                      ON dbo.Link.id = dbo.LinkFreqPlan_LinkEnd.link_id 
                      ON  dbo.LinkEnd.id = M.linkend_id LEFT OUTER JOIN
                      dbo.Property ON dbo.LinkEnd.property_id = dbo.Property.id
                      
                      OUTER apply fn_Linkend_Link_info(dbo.LinkEnd.id) link
                      
                       cross apply [dbo].fn_Linkend_Antenna_pos (LinkEnd.ID) A1
     
                      
                      
                      order by  M.id

go


IF OBJECT_ID(N'[dbo].[view_LinkFreqPlan_Neighbors_root]') IS NOT NULL
  DROP VIEW [dbo].[view_LinkFreqPlan_Neighbors_root]
GO


CREATE VIEW dbo.view_LinkFreqPlan_Neighbors_root
AS
--
--
--

SELECT dbo.LinkEnd.name AS LinkEnd_name,
       dbo.LinkEnd.band,
       
       
       next_LinkEnd.name AS rx_LinkEnd_name,
       
       dbo.Property.name AS Property_name,
       
       M.LinkFreqPlan_id,
       M.id,
       M.linkend_id,
       M.CIA,
       M.CI,
       M.CA,
       M.threshold_degradation,
       M.threshold_degradation_CIA,
       
       m.lat,
       m.lon,
       m.location_type,                     
       
       
       
--       next_linkend_id
       
      round (length,0) as length,
     --  length_KM,
       
--       dbo.Link.length,
--       dbo.Link.length_km,
       
       (
         SELECT top 1 [rx_level_dBm]
         FROM LINKFREQPLAN_NEIGHBORS
         WHERE (LinkFreqPlan_id = m.LinkFreqPlan_id) AND
               (rx_linkEND_id = m.linkend_id) AND
--               (linkEND2_id = m.linkend_id) AND
               
               (IsLink = 1)
       ) AS rx_level_dBm,
       
       (
         SELECT COUNT(*)
         FROM LINKFREQPLAN_NEIGHBORS
         WHERE (LinkFreqPlan_id = m.LinkFreqPlan_id) AND
--               (LinkEND2_id = M.linkend_id) AND
               (RX_LinkEND_id = M.linkend_id) AND
               (IsNeigh = 1) -- and (IsLink <> 1)
       ) AS NEIGHBOuR_count,
       
    --   0 as Azimuth,
       
         dbo.fn_LinkEnd_Azimuth (m.linkend_id, NULL) as Azimuth
       
FROM dbo.Property
     RIGHT OUTER JOIN dbo.LinkEnd ON dbo.Property.id = dbo.LinkEnd.property_id
--     RIGHT OUTER JOIN dbo.LinkFreqPlan_LinkEnd M
     RIGHT OUTER JOIN view_LinkFreqPlan_LinkEnd M
     
     ON dbo.LinkEnd.id = M.linkend_id
     
     left JOIN LinkEnd next_LinkEnd on next_LinkEnd.id=m.next_linkend_id
     
go     



IF OBJECT_ID(N'[dbo].[view_LinkFreqPlan_CIA]') IS NOT NULL
  DROP VIEW [dbo].[view_LinkFreqPlan_CIA]
GO


------------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID(N'[dbo].[view_LinkFreqPlan_Report]') IS NOT NULL
  DROP VIEW [dbo].[view_LinkFreqPlan_Report]
GO


CREATE VIEW dbo.view_LinkFreqPlan_Report
AS
SELECT
  m.*,
  
/*
 dbo.LinkEnd.name AS LinkEnd_name,
       dbo.LinkEnd.band,
       dbo.LinkEnd.channel_number,
       dbo.LinkEnd.rx_freq_MHz,
       dbo.LinkEnd.tx_freq_MHz,
       dbo.LinkEnd.loss,
       dbo.LinkEnd.channel_type,
       dbo.LinkEnd.subband,
       dbo.Property.name AS Property_name,
       dbo.Property.lat,
       dbo.Property.lon,
       dbo.Link.name AS Link_name,
       dbo.LinkEndType.name AS LinkEndType_name,
       dbo.LinkFreqPlan_LinkEnd.id,
       dbo.LinkFreqPlan_LinkEnd.LinkFreqPlan_id,
       dbo.LinkFreqPlan_LinkEnd.linkend_id,
       
*/       
       
       dbo.Linkend_Antenna.antennaType_id,
       dbo.Linkend_Antenna.height,
       dbo.Linkend_Antenna.azimuth,
       dbo.Linkend_Antenna.diameter,
       
       dbo.AntennaType.name AS [AntennaType_name],
       
       
       dbo.Property.lat_str,
       dbo.Property.lon_str
       
FROM view_LinkFreqPlan_LinkEnd m
     INNER JOIN dbo.Linkend_Antenna ON M.linkend_id =  dbo.Linkend_Antenna.linkend_id
     LEFT OUTER JOIN dbo.AntennaType ON dbo.Linkend_Antenna.antennaType_id =  dbo.AntennaType.id
--     LEFT OUTER JOIN dbo.Link ON dbo.LinkFreqPlan_LinkEnd.link_id = dbo.Link.id
 --    LEFT OUTER JOIN dbo.LinkEnd ON dbo.LinkFreqPlan_LinkEnd.linkend_id =   dbo.LinkEnd.id
     LEFT OUTER JOIN dbo.Property ON M.property_id = dbo.Property.id
   --  LEFT OUTER JOIN dbo.LinkEndType ON dbo.LinkEnd.linkendtype_id =   dbo.LinkEndType.id
WHERE (dbo.Linkend_Antenna.linkend_id > 0)

go



CREATE VIEW dbo.[view_LinkFreqPlan_CIA]
AS
SELECT -- 0 as aaaaaa


 dbo.LinkEnd.name AS LinkEnd_name,
       dbo.LinkFreqPlan_LinkEnd.LinkFreqPlan_id,
       dbo.LinkFreqPlan_LinkEnd.id,
       dbo.LinkFreqPlan_LinkEnd.linkend_id,
       dbo.LinkFreqPlan_LinkEnd.CIA,
       dbo.LinkFreqPlan_LinkEnd.CI,
       dbo.LinkFreqPlan_LinkEnd.CA,
       dbo.LinkFreqPlan_LinkEnd.freq_distributed_tx,
       dbo.LinkFreqPlan_LinkEnd.freq_distributed_rx,
       dbo.LinkFreqPlan_LinkEnd.freq_fixed_tx,
       dbo.LinkFreqPlan_LinkEnd.freq_fixed_rx,
       dbo.LinkFreqPlan_LinkEnd.threshold_degradation,
       dbo.LinkFreqPlan_LinkEnd.threshold_degradation_CIA,
       
  Link.Link_name,
  Link.length AS length
--  Link.length_km AS length_km
--  dbo.Link.clutter_model_id AS clutter_model_id,
       
     /*  
       length,
       length_km
       */
       
--FROM view_LinkFreqPlan_LinkEndEx
FROM LinkFreqPlan_LinkEnd
   --  LEFT OUTER JOIN dbo.Link ON dbo.LinkFreqPlan_LinkEnd.link_id = dbo.Link.id
     LEFT OUTER JOIN dbo.LinkEnd ON dbo.LinkFreqPlan_LinkEnd.linkend_id =  dbo.LinkEnd.id
     
     
      cross apply fn_Linkend_Link_info(dbo.LinkFreqPlan_LinkEnd.linkend_id) link
     
go     




-------------------------------------------------------

IF OBJECT_ID(N'[dbo].[view_LinkFreqPlan]') IS NOT NULL
  DROP VIEW dbo.view_LinkFreqPlan 
GO

CREATE VIEW dbo.view_LinkFreqPlan 
AS
  SELECT LinkFreqPlan.*,
    dbo.LinkNet.project_id
  FROM
    LinkFreqPlan
    INNER JOIN dbo.LinkNet ON (LinkFreqPlan.linknet_id = dbo.LinkNet.id)
go



-------------------------------------------------------------
IF OBJECT_ID(N'[dbo].[sp_LinkFreqPlan_Neighbors_add]') IS NOT NULL
  DROP  PROCEDURE dbo.sp_LinkFreqPlan_Neighbors_add
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_LinkFreqPlan_Neighbors_add
(
  @LINKFREQPLAN_ID INT,

  @LINKEND1_ID INT,  -- ���, ��� �����
  @LINKEND2_ID INT,  -- ���, ���� �����   

--  @TX_LINKEND_ID INT,  --
--  @RX_LINKEND_ID INT,  --������������� ��������  (linkend)
    
  @RX_LEVEL_DBM FLOAt=null,   
  

  @IsPropertyOne BIT=null,   
  @IsLink  BIT=null,   

  @Distance_M FLOAT=null,   
  @diagram_level FLOAT=null
   
)    

AS
BEGIN
  DECLARE
    @TX_LINKEND_ID INT = @LINKEND1_ID,  --
    @RX_LINKEND_ID INT = @LINKEND2_ID --������������� ��������  (linkend)
    

/*

declare   
    @LINKEND1_name varchar(50),  --
    @LINKEND2_name varchar(50)  --������������� ��������  (linkend)
 --   @rx_for_LINKEND1_name varchar(50)  --������������� ��������  (linkend)
*/
 
/*

 SELECT @LINKEND1_name=name
    FROM linkend 
    WHERE ID=@LINKEND1_ID*/

/*
  SELECT @LINKEND1_name=name
    FROM linkend 
    WHERE ID=@LINKEND1_ID


  SELECT @LINKEND2_name=name
    FROM linkend 
    WHERE ID=@LINKEND2_ID

    */
    
  declare 
--  	 @rx_for_linkend1_id  int = dbo.fn_LinkEnd_Get_Next_LinkEnd_ID (@TX_LINKEND_ID)
  	 @rx_for_linkend1_id  int = dbo.fn_LinkEnd_Get_Next_LinkEnd_ID (@LINKEND1_ID)
     
     
     
     
/*  SELECT @rx_for_LINKEND1_name=name
    FROM linkend 
    WHERE ID=@rx_for_linkend1_id
*/
     
    
--  set @linkend_id =  dbo.fn_LinkEnd_Get_Next_LinkEnd_ID (@RX_LINKEND_ID)
  
  /*  
  SELECT @LINKEND1_name=name
    FROM linkend 
    WHERE LINKEND1_ID=@RX_LINKEND_ID
    
  IF @linkend_id IS NULL   
  BEGIN 
    SELECT @linkend_id=LINKEND1_ID 
      FROM link 
      WHERE LINKEND2_ID=@RX_LINKEND_ID
  END


  -- belong to the same link --
  IF @TX_LINKEND_ID = @linkend_id
    SET @linkend_id = @RX_LINKEND_ID

*/

--  IF @Distance_M=0  
--    SET @Distance_M=NULL


 -- if  (@IsLink=1) and (dbo.fn_LinkEnd_Has_same_link (@TX_LINKEND_ID, @RX_LINKEND_ID )=0)   
  --   RAISERROR ('if dbo.fn_LinkEnd_Has_same_link (@TX_LINKEND_ID, @RX_LINKEND_ID ) and  (@IsLink=1)', 16, 1);
/*
    SELECT                  
       @lat1=A1.lat,
       @lon1=A1.lon
    FROM dbo.LinkEnd 
         cross apply [dbo].fn_Linkend_Antenna_pos (id) A1
    where id=@TX_LINKEND_ID
*/


/*    
    SELECT                  
       @lat1=A1.lat,
       @lon1=A1.lon
    FROM dbo.LinkEnd 
         cross apply [dbo].fn_Linkend_Antenna_pos (id) A1
    where id=@TX_LINKEND_ID

----------------------

    SELECT                  
       @lat2=A1.lat,
       @lon2=A1.lon
    FROM dbo.LinkEnd 
         cross apply [dbo].fn_Linkend_Antenna_pos (id) A1
    where id=@RX_LINKEND_ID
  
*/
--//if @IsLink<>1
--  set  @RX_LEVEL_DBM = null
----------------------  
  

  INSERT INTO LinkFreqPlan_Neighbors
     (LINKFREQPLAN_ID, 

      LINKEND1_ID,      
      LINKEND2_ID,

  --    tx_LINKEND1_name,
  --    tx_LINKEND2_name,
      
      rx_for_linkend1_id,
--      rx_for_linkend1_name,

      TX_LINKEND_ID,      
      RX_LINKEND_ID,  --������������� ��������  (linkend)
   
    --  TX_LINK_ID,  --����������, �������� �������������� ���������  (linkend)
      
     RX_LEVEL_DBM,   
   --   SESR,
   --   Kng,

      IsPropertyOne,
      IsLink,

 --     azimuth_,
     Distance_M,
      
      diagram_level 
      )
  VALUES
     (@LINKFREQPLAN_ID, 
      
      @LINKEND1_ID,      
      @LINKEND2_ID,

--      @LINKEND1_name,
  --    @LINKEND2_name,
      
      @rx_for_linkend1_id,
--      @rx_for_linkend1_name,
      


      @TX_LINKEND_ID,      
      @RX_LINKEND_ID,
      
   --   @linkend_id,
      
      round(@RX_LEVEL_DBM,1),
   --   @SESR,
   --   @Kng,

      @IsPropertyOne,
      @IsLink,

--dbo.fn_AZIMUTH (@lat1, @lon1, @lat2, @lon2, null),
--dbo.fn_AZIMUTH (@lat1, @lon1, @lat2, @lon2, 'length'),
     @Distance_M,
      @diagram_level
      ) 
      
      
  RETURN IDENT_CURRENT ('LinkFreqPlan_Neighbors')         
      
--  RETURN @@IDENTITY;
      

END

go

-------------------------------------------------------------
IF OBJECT_ID(N'[dbo].[sp_LinkFreqPlan_Neighbors]') IS NOT NULL
  DROP  PROCEDURE dbo.sp_LinkFreqPlan_Neighbors
GO



/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_LinkFreqPlan_Neighbors
(
	@ID INT 
)    

AS
BEGIN
  SET ANSI_WARNINGS OFF --String or binary data would be truncated.

  DECLARE
--    @max_id int,
  	@i int,
    @max_tree_id int;
  		  

 -- if @ID is null
 --   set @ID = 70365
  
  if @ID is null
  BEGIN
   SELECT top 1  @id =id FROM LinkFreqPlan order by id desc
  
--    set @ID = 70365
 --   set @LINKEND_ID =124645
  END

    	
  print @ID
  

  create table [#temp]
      (           
       tree_id 		    Int,
       tree_parent_id   Int,
                   
       BAND        	   VARCHAR(10),
    --   NUMBER          Int,
    
       id 		       Int,    

       TX_LINKEND_ID      Int,
       RX_LINKEND_ID      Int,

    
       TX_LinkEnd_name VARCHAR(50),
       RX_LinkEnd_name VARCHAR(50),
    
       TX_Property_name VARCHAR(50),
       RX_Property_name VARCHAR(50),

       TX_Property_lat float,
       TX_Property_lon float,

       RX_Property_lat float,
       RX_Property_lon float,

       Next_for_RX_Property_lat float,
       Next_for_RX_Property_lon float,



       
--       LINKEND_ID      Int,
       
       DISTANCE_m      Float,
       DISTANCE_km     Float,

       CI              Float,
       Rx_level_dBm    Float,
       INTERFERENCE    Float,
       NEIGHBOUR_COUNT Int,    
       
       
       power_noise Float, 
       power_cia   Float,
       
    --   [TYPE]           Int,
       
       CIA             Float,
       CI_             Float,
       CA              Float,
       
       EXTRA_LOSS      				Float,
       threshold_degradation 		Float,
       threshold_degradation_cia 	Float,
       
       IsByCI bit,
       IsByProperty bit,
       IsByLink bit,
       
       IsNeigh bit
                              
      ) ;      
      

--------------------------------------------------
-- LinkEnd
--------------------------------------------------     
  insert into [#temp]
    (
      tree_id,
     
   --   LINKEND_ID, 
  --   TX_LINKEND_ID, RX_LINKEND_ID,
      
      TX_LinkEnd_name, TX_Property_name,
      BAND, 

	  Rx_level_dBm, 
      DISTANCE_m,
      --DISTANCE_km,
      
      CIA, CI_, CA, threshold_degradation, threshold_degradation_cia,      
      NEIGHBOUR_COUNT    
     )
   select 
     LINKEND_ID, --tree_id

 --    LINKEND_ID,	
 --    TX_LINKEND_ID, RX_LINKEND_ID,
     
     
     LinkEnd_name, Property_name,
     BAND, 

  	 Rx_level_dBm, 
     LENGTH, 
     --LENGTH_km,
      
     CIA, CI, CA, threshold_degradation, threshold_degradation_cia,      
     
     case 
       when NEIGHBOUR_COUNT=0 then null
       else NEIGHBOUR_COUNT
     end  
     
   from 
   		view_LinkFreqPlan_Neighbors_root
   where 
   	 LinkFreqPlan_id = @ID;
     

 -- select @max_id=Max(tree_id)  from  #temp  
     
--  print @max_id   
    
     

--  set @i=0

  --UPDATE [#temp]  
  --	set @i=number = @i+1


  ------------------------------
  -- ByProperty 
  ------------------------------
  insert into [#temp]
    ( 
      tree_parent_id,
      
      tx_linkend_id, rx_linkend_id,
      
      
   --  [TYPE],
      ID, 
      
      RX_LinkEnd_name, rx_Property_name,	
      
/*      DISTANCE_m, 
      CI, INTERFERENCE,
      
      CIA,
      CI_,
      CA,
      EXTRA_LOSS,
      threshold_degradation,
      THRESHOLD_DEGRADATION_CIA,

      IsNeigh,
      power_noise, power_cia ,
*/      
      IsByProperty
       
     )
   select 
      
     	rx_linkend_id,  
      
      tx_linkend_id, rx_linkend_id,      
      
    --  case when IsPropertyOne=1 THEN 2 ELSE 3 END, 

      ID, 
      
      rx_LinkEnd_name, rx_Property_name,
      
      
/*      DISTANCE_m, 
      CI, 
      Rx_level_dBm - DIAGRAM_LEVEL,
      
      CIA,
      CI_,
      CA,
      EXTRA_LOSS,
      threshold_degradation,
      THRESHOLD_DEGRADATION_CIA,  
      IsNeigh,

      power_noise, power_cia ,
*/      
      
      1
      
  
   FROM  view_LinkFreqPlan_Neighbors_slave
   where (LinkFreqPlan_id = @ID) 
    and (IsNull(IsLink,0)<>1)
    
    --  and IsPropertyOne=1
  -- ORDER BY IsPropertyOne DESC

  ------------------------------
  -- ONLY IsLink
  ------------------------------
  insert into [#temp]
    ( tree_parent_id,
      ID, 
      
       tx_linkend_id, rx_linkend_id,
      
      RX_LinkEnd_name, rx_Property_name,
      DISTANCE_m,
      
      IsByLink
      
     --  [TYPE]
     )
   select 
     	tx_linkend_id, 
      ID, 
      
       tx_linkend_id, rx_linkend_id,
      
      rx_LinkEnd_name, rx_Property_name,
      DISTANCE_m,
      
      1
      
    --  1
  
  FROM  view_LinkFreqPlan_Neighbors_slave
  where (LinkFreqPlan_id = @ID) 
       and (IsLink=1);


   
/*const
  TYPE_NEIGHBORS_BY_CI   = 1;  //ninaa ii yia?aaoeea
  TYPE_NEIGHBORS_BY_LINK = 2;  //ninaa ii Link
  TYPE_NEIGHBORS_BY_PROP = 3;  //ninaa ii Property
*/   

  --------------------------
  -- set tree_id
  --------------------------

  SELECT @max_tree_id=Max(tree_id)  
    FROM #temp                                
    WHERE tree_id IS NOT NULL 
  		 
  set @i=@max_tree_id

  UPDATE [#temp]  
  	set @i=tree_id = @i+1
  	WHERE tree_id IS NULL 


  SELECT * FROM #temp                                
 
 END

go



-------------------------------------------------------------
IF OBJECT_ID(N'[dbo].[view_LinkFreqPlan]') IS NOT NULL
  DROP VIEW dbo.view_LinkFreqPlan 
GO



CREATE VIEW dbo.view_LinkFreqPlan 
AS
  SELECT LinkFreqPlan.*,
    dbo.LinkNet.project_id
  FROM
    LinkFreqPlan
    INNER JOIN dbo.LinkNet ON (LinkFreqPlan.linknet_id = dbo.LinkNet.id)

go


------------------------------------------------------------

IF OBJECT_ID(N'[dbo].[sp_LinkFreqPlan_add]') IS NOT NULL
  DROP PROCEDURE dbo.sp_LinkFreqPlan_add
GO



/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_LinkFreqPlan_add
(
  @LINKNET_ID int,

  @NAME VARCHAR(100), 
  
  @MaxCalcDistance_km float, 
  @MIN_CI_dB float,
    
  @IS_GET_INFO bit = 0

)
AS
BEGIN
  DECLARE
  	@new_id int	
   -- @project_id int;   


  if not exists(SELECT * from LINKNET WHERE id=@LINKNET_ID) 
     RAISERROR ('@LINKNET_ID is null', 16, 1);



-- IF @LinkFreqPlan_ID is null  RAISERROR ('@ID is null', 16, 1);
 /*
  if @LinkFreqPlan_ID is null
  BEGIN
   SELECT top 1  @LinkFreqPlan_ID =id 
   	FROM LinkFreqPlan
  
--    set @ID = 70365
 --   set @LINKEND_ID =124645
  END
*/



/*  SELECT @project_id=project_id
    FROM LinkNet
    WHERE id=@LINKNET_ID;
    */
    
  ---------------------------------------------

  INSERT INTO LINKFREQPLAN
     (
   --   project_id,
      LINKNET_ID,
      
      NAME,
      MaxCalcDistance_km,
      min_CI_dB
      ) 
   VALUES     
     (
   --   @project_id,
      @LINKNET_ID,

      @NAME,
      @MaxCalcDistance_km, 
      @MIN_CI_dB      
     )   

  ---------------------------------------------
      
 -- RETURN @@IDENTITY  
  
--  set @new_id=@@IDENTITY
  
  set @new_id=IDENT_CURRENT ('LinkFreqPlan')   
  
  
  print @new_id
  
    
  exec sp_LinkFreqPlan_init @new_id

  
  
  if @IS_GET_INFO=1
    SELECT id,guid,name  FROM LINKFREQPLAN WHERE ID=@new_id
  
  RETURN @new_id;      
        
END

go
------------------------------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-------------------------------------------------------------
IF OBJECT_ID(N'[dbo].[sp_LinkFreqPlan_Init]') IS NOT NULL
  DROP  PROCEDURE dbo.sp_LinkFreqPlan_Init
GO



/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_LinkFreqPlan_Init
(
	@LinkFreqPlan_ID  INT=11
)
AS
BEGIN
--  IF @LinkFreqPlan_ID is null  RAISERROR ('@ID is null', 16, 1);
 /*
  if @LinkFreqPlan_ID is null
  BEGIN
   SELECT top 1  @LinkFreqPlan_ID =id 
   	FROM LinkFreqPlan
  
--    set @ID = 70365
 --   set @LINKEND_ID =124645
  END
*/


declare 
  @linkNet_id INT;

  --begin TRANSACTION
    
  
  SELECT @linkNet_id = LinkNet_id 
    FROM  LinkFreqPlan
    WHERE (id = @LinkFreqPlan_ID)






-- ������ LINKS from LinkNet
create TABLE #link     (id int NOT NULL) 
create TABLE #linkline (id int NOT NULL) 
create TABLE #linkend  (id int NOT NULL) --, link_id int NOT NULL); 
--create TABLE #Property (id int NOT NULL) 
--create TABLE #linkend_NB (id int NOT NULL) 
create TABLE #bands (band nVARCHAR(50) COLLATE Cyrillic_General_CI_AS NOT NULL) 

-------------------------------------------------------------------
-- ������� ��� LINKS �� ����
-------------------------------------------------------------------

--select * INTO ##link from Antenna


 INSERT INTO #link (id)
   SELECT link_id FROM LinkNet_Items_XREF 
     WHERE (linknet_id=@linkNet_id) and (link_id>0) 

--select * From #link 



 INSERT INTO #linkLine (id)
   SELECT linkLine_id FROM LinkNet_Items_XREF 
     WHERE (linknet_id=@linkNet_id) and (linkLine_id>0) 


 INSERT INTO #link (id)
   SELECT link_id FROM LinkLineLinkXREF 
   	  WHERE LinkLine_id IN (SELECT id FROM #linkline)   
      
      -- IN            
        --   (SELECT LinkLine_id FROM LinkNet_Items_XREF WHERE linknet_id=@linkNet_id)   


--select * From #link 




--SELECT Link_id FROM  LinkFreqPlan_Link  where LinkFreqPlan_id = @LinkFreqPlan_ID

 

-------------------------------------------------------------------
-- �������� ����������� LINKS � ��������� ���� - table LinkFreqPlan_Link
-------------------------------------------------------------------
INSERT INTO LinkFreqPlan_Link (LinkFreqPlan_id, Link_id)
 SELECT @LinkFreqPlan_ID, id FROM #link
   WHERE id NOT IN  
      (SELECT Link_id FROM  LinkFreqPlan_Link  where LinkFreqPlan_id = @LinkFreqPlan_ID)




      
-------------------------------------------------------------------      
-- ������� ������ LINKS 
-------------------------------------------------------------------





DELETE FROM LinkFreqPlan_Link 
   WHERE Link_id NOT IN  
      (SELECT id FROM  #link)

      
INSERT INTO #linkend (id) --, link_id
    SELECT LinkEnd1_id FROM Link   --, id
       WHERE id IN  (SELECT id FROM  #link)
  UNION     
    SELECT LinkEnd2_id FROM Link   --, id
       WHERE id IN  (SELECT id FROM  #link)



    SELECT * from Link 
       where id  in (Select id FROM  #link)




-------------------------------------------------------------------
-- ��� ��������, ��� ��������� LinkEnds
-------------------------------------------------------------------
/*    SELECT Property_id FROM LinkEnd 
       WHERE id IN  (SELECT id FROM  #linkend)
*/


/*
 INSERT INTO #Property (id)
    SELECT Property_id FROM LinkEnd 
       WHERE id IN  (SELECT id FROM  #linkend)
 
*/

------------------------------------------------------------------
-- ��� �������� LinkEnds �� ���������, ��� (channel_number > 0) 
-------------------------------------------------------------------

/* SELECT ID FROM LinkEnd
   WHERE (Property_id IN (SELECT id FROM  #Property)) and 
         (id not in  (SELECT id FROM  #linkend)) and 
         (channel_number > 0) 
*/

/*
-- �������� ���������

INSERT INTO #linkend_NB (id)
 SELECT ID FROM LinkEnd
   WHERE (Property_id IN (SELECT id FROM  #Property)) and 
         (id not in  (SELECT id FROM  #linkend)) and 
         (channel_number > 0) 

*/

/*INSERT INTO #linkend_NB (id)
 SELECT ID FROM LinkEnd
   WHERE (Property_id IN (SELECT id FROM  #Property)) and 
         (id not in  (SELECT id FROM  #linkend)) and 
         (channel_number > 0) 
*/

-------------------------------------------------------------------
-- ������� ������ linkend� 
-------------------------------------------------------------------

/*SELECT * FROM LinkFreqPlan_LinkEnd 
   WHERE (LinkEnd_id NOT IN (SELECT id FROM  #linkEnd)) and 
         (LinkEnd_id NOT IN (SELECT id FROM  #linkEnd_NB)) 

*/
DELETE FROM LinkFreqPlan_LinkEnd 
   WHERE (LinkEnd_id NOT IN (SELECT id FROM  #linkEnd))
       --  and 
       --  (LinkEnd_id NOT IN (SELECT id FROM  #linkEnd_NB)) 
         
/*
    
     SELECT @LinkFreqPlan_ID, 		
     		ID,
            CHANNEL_NUMBER,
            TX_FREQ,
            RX_FREQ,    
            CHANNEL_NUMBER_TYPE,
            1     
      FROM linkend     
      WHERE (id not in (SELECT LinkEnd_id FROM  LinkFreqPlan_LinkEnd WHERE LinkFreqPlan_id = @LinkFreqPlan_ID)) and
            ( (id in (SELECT id FROM  #linkEnd)) or  
              (id in (SELECT id FROM  #linkEnd_NB)) )
*/


  INSERT INTO LinkFreqPlan_LinkEnd         
            (LINKFREQPLAN_ID, 
            LINKEND_ID,
        --    LINK_ID,   
         --   NEXT_LINKEND_ID,
                      
            FREQ_FIXED, 
            FREQ_FIXED_TX, 
            FREQ_FIXED_RX, 
          --  CHANNEL_TYPE_FIXED,
            FREQ_REQ_COUNT )
        
     SELECT @LinkFreqPlan_ID, 		
     	     	ID,
       --     dbo.fn_LinkEnd_Get_Link_ID(id),
       --     dbo.fn_LinkEnd_Get_Next_LinkEnd_ID(id),
            
            CHANNEL_NUMBER,
            TX_FREQ_MHz,
            RX_FREQ_MHz,    
          --  CHANNEL_TYPE,
            1     
      FROM linkend     
      WHERE (id not in 
                (SELECT LinkEnd_id 
                 FROM  LinkFreqPlan_LinkEnd 
                 WHERE LinkFreqPlan_id = @LinkFreqPlan_ID)) and
                 
           -- (
             (id in (SELECT id FROM  #linkEnd))
            -- or  
             -- (id in (SELECT id FROM  #linkEnd_NB)) 
             -- )


-------------------------------------------------------------------
-- used Bands
-------------------------------------------------------------------

print 'INSERT into LinkFreqPlan_Resource'

 INSERT INTO #bands (band)
   SELECT DISTINCT LinkEnd.band 
      FROM LinkFreqPlan_LinkEnd M LEFT OUTER JOIN
              LinkEnd ON M.linkend_id = LinkEnd.id
      WHERE M.LinkFreqPlan_id=@LinkFreqPlan_ID
/*
SELECT * FROM #bands

SELECT band FROM LinkFreqPlan_Resource where LinkFreqPlan_ID=@LinkFreqPlan_ID
*/

  INSERT into LinkFreqPlan_Resource (LinkFreqPlan_ID, band)
    SELECT @LinkFreqPlan_ID, band 
      FROM #bands
      WHERE band NOT IN 
         ( SELECT band FROM LinkFreqPlan_Resource where LinkFreqPlan_ID=@LinkFreqPlan_ID)      


  DELETE FROM LinkFreqPlan_Resource  
      WHERE band NOT IN 
         ( SELECT band FROM #bands)      



-- COMMIT TRANSACTION
 
 DROP TABLE #link  
 DROP TABLE #linkline   
 DROP TABLE #linkend  
 --DROP TABLE #Property 
 --DROP TABLE #linkend_NB  
 DROP TABLE #bands
       

END;
go



-----------------------------------------------------------
-------------------------------------------------------------
IF OBJECT_ID(N'dbo.sp_Linkend_Antenna_Update') IS NOT NULL
  DROP  PROCEDURE dbo.sp_Linkend_Antenna_Update
GO

/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE procedure dbo.sp_Linkend_Antenna_Update
(    
  @ID int, 

  @NAME   	varchar(50)=null,

  @HEIGHT  	float=null,
  @AZIMUTH	float=null,
  @TILT	    float=null,
  @LOSS	float=null
)
AS
begin

  UPDATE Linkend_Antenna
  set    
   --   ANTENNATYPE_ID=@ANTENNATYPE_ID,  
     
     
      HEIGHT    = COALESCE(@HEIGHT,  HEIGHT), 
      AZIMUTH   = COALESCE(@AZIMUTH, AZIMUTH),
      TILT      = COALESCE(@TILT,    TILT),  
      LOSS	    = COALESCE(@LOSS, LOSS)

/*      GAIN		=@GAIN, 
      DIAMETER	=@DIAMETER, 
      VERT_WIDTH=@VERT_WIDTH, 
      HORZ_WIDTH=@HORZ_WIDTH, 
      
      POLARIZATION_STR=@POLARIZATION_STR,
      AZIMUTH    =@AZIMUTH,
      TILT       =@TILT, 
      
      LAT           =@LAT,  
      LON           =@LON, 
      LOCATION_TYPE =@LOCATION_TYPE
*/     
  WHERE id=@ID


  RETURN @@ROWCOUNT;


        
end

go

------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-------------------------------------------------------------
IF OBJECT_ID(N'[dbo].[view_LinkFreqPlan_Neighbors]') IS NOT NULL
  DROP  view dbo.view_LinkFreqPlan_Neighbors
GO


/* ------------------------------------------------------------------------------
created: Alex  
used in: 
------------------------------------------------------------------------------*/
CREATE VIEW dbo.view_LinkFreqPlan_Neighbors
AS
SELECT     TOP 100 PERCENT dbo.LinkFreqPlan_Neighbors.tx_linkend_id, dbo.LinkFreqPlan_Neighbors.rx_linkend_id, 
                      dbo.LinkFreqPlan_Neighbors.LinkFreqPlan_id, dbo.LinkFreqPlan_Neighbors.cia, dbo.LinkFreqPlan_Neighbors.ci_, dbo.LinkFreqPlan_Neighbors.ca, 
                      dbo.LinkFreqPlan_Neighbors.IsLink, dbo.LinkFreqPlan_Neighbors.IsPropertyOne, 
                      
    /*                  
   
       A1.lat AS lat1,
       A1.lon AS lon1,
       A2.lat AS lat2,
       A2.lon AS lon2, 
                        */
                      
                     Property_1.lat AS lat1, 
                      Property_1.lon AS lon1, 
                      
                      Property_2.lat AS lat2, 
                      
                      Property_2.lon AS lon2, 
  
  
                     
                      dbo.LinkFreqPlan_Neighbors.threshold_degradation, dbo.LinkFreqPlan_Neighbors.threshold_degradation_cia, 
                      dbo.LinkFreqPlan_Neighbors.IsNeigh, dbo.LinkFreqPlan_Neighbors.power_noise, dbo.LinkFreqPlan_Neighbors.power_cia, 
                      dbo.LinkFreqPlan_Neighbors.extra_loss
FROM         dbo.LinkFreqPlan_Neighbors LEFT OUTER JOIN
                      dbo.LinkEnd LinkEnd_2 ON dbo.LinkFreqPlan_Neighbors.tx_linkend_id = LinkEnd_2.id LEFT OUTER JOIN
                      dbo.Property Property_2 ON LinkEnd_2.property_id = Property_2.id LEFT OUTER JOIN
                      dbo.Property Property_1 RIGHT OUTER JOIN
                      dbo.LinkEnd LinkEnd_1 ON Property_1.id = LinkEnd_1.property_id ON dbo.LinkFreqPlan_Neighbors.rx_linkend_id = LinkEnd_1.id
/*                      
    cross apply [dbo].fn_Linkend_Antenna_pos (LinkEnd_1.id) A1
     cross apply [dbo].fn_Linkend_Antenna_pos (LinkEnd_2.id) A2
*/
                      
WHERE     (dbo.LinkFreqPlan_Neighbors.cia IS NOT NULL)

go

----------------------------------------------------------------

IF OBJECT_ID(N'[LinkFreqPlan].[view_LinkFreqPlan_LinkEnd]') IS NOT NULL
  DROP  view [LinkFreqPlan].[view_LinkFreqPlan_LinkEnd]
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [LinkFreqPlan].[view_LinkFreqPlan_LinkEnd]
AS

SELECT  top 1000000   

--  dbo.fn_LinkEnd_Get_Link_ID (dbo.LinkEnd.id) AS link_id, 
 -- dbo.fn_LinkEnd_Get_Next_LinkEnd_ID (dbo.LinkEnd.id) AS next_linkend_id, 


M.*,

link.link_id, 
link.link_name, 
link.next_linkend_id, 
link.clutter_model_id,
link.length,
--link.length_km,



dbo.LinkEnd.name AS LinkEnd_name,
dbo.LinkEnd.property_id, -- AS property_id, 

dbo.LinkEnd.subband, -- AS subband, 

dbo.LinkEnd.LinkEndType_mode_id, -- AS LinkEndType_mode_id,

                      
                      dbo.Property.name AS Property_name, 
                      
--                      dbo.Property.lat AS Property_lat, 
--                      dbo.Property.lon AS Property_lon, 
                     
--
---                     A1.lat AS Property_lat,
   --                   A1.lon AS Property_lon,
 

                      A1.lat,
                      A1.lon,
                      A1.location_type,                     
                   
                      
                      dbo.Property.ground_height AS ground_height,
                      
                      
/*                      dbo.Link.name AS Link_name, 
                      dbo.Link.length AS length, 
                      dbo.Link.length_km AS length_km,                       
                      dbo.Link.clutter_model_id AS clutter_model_id,
*/
                      
                      dbo.LinkEndType_Band.channel_width AS channel_width, 
                      dbo.LinkEndType_Band.txrx_shift, -- AS txrx_shift,
                      dbo.LinkEndType_Band.channel_count, -- AS channel_count, 
                      
                      dbo.LinkEndType_Band.freq_max_high, 
                      dbo.LinkEndType_Band.freq_min_low, 
                      dbo.LinkEndType_Band.name AS LinkEndType_Band_name  ,                    
                      
                      
                      dbo.fn_Linkend_GetMaxAntennaHeight(dbo.LinkEnd.id) AS    MaxAntennaHeight, 
                      
                      dbo.LinkFreqPlan.threshold_degradation AS  LinkFreqPlan_threshold_degradation,
                      dbo.LinkFreqPlan.min_CI_dB AS LinkFreqPlan_min_CI_dB, 
                      

                      dbo.LinkEnd.channel_number, -- AS channel_number, 
                      dbo.LinkEnd.tx_freq_MHz, -- AS tx_freq_MHz, 
                      dbo.LinkEnd.rx_freq_MHz, -- AS rx_freq_MHz, 

                      dbo.LinkEnd.power_dBm, -- AS power_dBm, 
                      dbo.LinkEnd.kng AS kng, 
                      dbo.LinkEnd.loss AS loss, 
                      dbo.LinkEnd.freq_channel_count, -- AS freq_channel_count, 
                      dbo.LinkEnd.channel_type,
                      dbo.LinkEnd.freq_spacing , -- AS freq_spacing,
                      dbo.LinkEnd.kratnostByFreq, -- AS kratnostByFreq,
                      dbo.LinkEnd.kratnostBySpace, -- AS kratnostBySpace, 
                      dbo.LinkEnd.signature_width, -- AS signature_width,
                      dbo.LinkEnd.signature_height, -- AS signature_height, 
                      dbo.LinkEnd.modulation_count, -- AS modulation_count,
                      dbo.LinkEnd.threshold_BER_3, -- AS threshold_BER_3, 
                      dbo.LinkEnd.threshold_BER_6, -- AS threshold_BER_6,
                      
                      dbo.LinkEnd.band, -- AS band,                                         
                      dbo.LinkEnd.linkendtype_id, -- AS linkendtype_id, 
                      dbo.LinkEnd.LinkEndType_band_id, -- AS LinkEndType_band_id, 

                      dbo.LinkEndType_Mode.ch_spacing,    
                      dbo.LinkEndType_Mode.bandwidth AS bandwidth, 
                      dbo.LinkEndType_Mode.bandwidth_tx_3, -- AS bandwidth_tx_3,
                      dbo.LinkEndType_Mode.bandwidth_tx_30, -- AS bandwidth_tx_30, 
                      dbo.LinkEndType_Mode.bandwidth_tx_a, -- AS bandwidth_tx_a,
                      dbo.LinkEndType_Mode.bandwidth_rx_3, -- AS bandwidth_rx_3, 
                      dbo.LinkEndType_Mode.bandwidth_rx_30, -- AS bandwidth_rx_30,
                      dbo.LinkEndType_Mode.bandwidth_rx_a, -- AS bandwidth_rx_a, 

--                 dbo.LinkEndType.FREQ_MAX_HIGH,
 --                     dbo.LinkEndType.FREQ_MIN_LOW,

                      dbo.LinkEndType.level_tx_a,
                      dbo.LinkEndType.level_rx_a,
                      dbo.LinkEndType.name AS LinkEndType_name,
                      dbo.LinkEndType.coeff_noise,
                      
                      
--//                      a.id as ssss,

/*
                      a.POLARIZATION_STR   as antenna_POLARIZATION_STR,
                      a.Polarization_ratio as antenna_Polarization_ratio,
                      a.Gain 			 as antenna_Gain,
                      a.Diameter 		 as antenna_Diameter,
                      a.Loss 			 as antenna_Loss,
                      a.Vert_width 		 as antenna_Vert_width,
                      a.AZIMUTH 		 as antenna_AZIMUTH,
                      a.Height 		 	 as antenna_Height,
                 --     a.Polarization_ratio as antenna_Polarization_ratio
                      
*/

(select count(*) from Linkend_Antenna where Linkend_id = dbo.LinkEnd.id) as Antenna_count
                   
                      
                      
FROM         dbo.LinkEndType_Band RIGHT OUTER JOIN
                      dbo.LinkEndType_Mode RIGHT OUTER JOIN
                      dbo.LinkEnd LEFT OUTER JOIN
                      dbo.LinkEndType ON dbo.LinkEnd.linkendtype_id = dbo.LinkEndType.id ON dbo.LinkEndType_Mode.id = dbo.LinkEnd.LinkEndType_mode_id ON 
                      dbo.LinkEndType_Band.id = dbo.LinkEnd.LinkEndType_band_id RIGHT OUTER JOIN
                   --   dbo.Link RIGHT OUTER JOIN
                      dbo.LinkFreqPlan_LinkEnd M LEFT OUTER JOIN
                      dbo.LinkFreqPlan ON M.LinkFreqPlan_id = dbo.LinkFreqPlan.id 
--                      ON dbo.Link.id = dbo.LinkFreqPlan_LinkEnd.link_id 
                      ON  dbo.LinkEnd.id = M.linkend_id LEFT OUTER JOIN
                      dbo.Property ON dbo.LinkEnd.property_id = dbo.Property.id
                      
                      OUTER apply fn_Linkend_Link_info(dbo.LinkEnd.id) link
                      
                      cross apply [dbo].fn_Linkend_Antenna_pos (LinkEnd.ID) A1
                       
          /*             
                       left join (
                          select Linkend_Antenna.*, AntennaType.Polarization_ratio 
                             from Linkend_Antenna
                               left join AntennaType  on AntennaType.id = Linkend_Antenna.antennaType_id
                       
                       ) A on a.linkend_id = LinkEnd.id
     
*/
                      
                      
     order by  M.id

go

-----------------------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID(N'LinkFreqPlan.view_LinkFreqPlan_LinkEnd_antennas') IS NOT NULL
  DROP  view LinkFreqPlan.view_LinkFreqPlan_LinkEnd_antennas
GO



CREATE VIEW LinkFreqPlan.view_LinkFreqPlan_LinkEnd_antennas
AS
SELECT   

--LinkEnd.id as aaaaaaaaaaaaaa,


  dbo.Linkend_Antenna.*, dbo.Property.ground_height AS ground_height, dbo.LinkFreqPlan_LinkEnd.LinkFreqPlan_id AS LinkFreqPlan_id, 
                      dbo.AntennaType.polarization_ratio AS polarization_ratio, dbo.AntennaType.vert_mask, dbo.AntennaType.horz_mask, 
                      
LinkEnd.name as LinkEnd_name,
                      
                        
       A1.lat AS map_lat,
       A1.lon AS map_lon,
                      
                      
                      dbo.Property.lat AS Property_lat, 
                      dbo.Property.lon AS Property_lon
                      
                      
                      
FROM         dbo.Property RIGHT OUTER JOIN
                      dbo.LinkEnd LEFT OUTER JOIN
                      dbo.AntennaType RIGHT OUTER JOIN
                      dbo.Linkend_Antenna ON dbo.AntennaType.id = dbo.Linkend_Antenna.antennaType_id ON dbo.LinkEnd.id = dbo.Linkend_Antenna.linkend_id ON 
                      dbo.Property.id = dbo.Linkend_Antenna.property_id RIGHT OUTER JOIN
                      dbo.LinkFreqPlan_LinkEnd ON dbo.LinkEnd.id = dbo.LinkFreqPlan_LinkEnd.linkend_id
                      

                      

     cross apply [dbo].fn_Linkend_Antenna_pos (Linkend_Antenna.linkend_id) A1
     
--where LinkFreqPlan_id=1016

go
------------------------------------------------------------------

-------------------------------------------------------------
IF OBJECT_ID(N'[dbo].[view_LinkFreqPlan_Calc_Neighbors]') IS NOT NULL
  DROP  view dbo.view_LinkFreqPlan_Calc_Neighbors
GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW dbo.view_LinkFreqPlan_Calc_Neighbors
AS
SELECT dbo.LinkFreqPlan_Neighbors.*,
       
       dbo.LinkEndType.coeff_noise as coeff_noise, 

       dbo.LinkEndType_Mode.bandwidth as bandwidth,
      
       LinkEnd.LinkEndType_mode_id,
       
       LinkEnd.name as TX_LinkEnd_name,
       LinkEnd2.name as RX_LinkEnd_name
       
       
FROM dbo.LinkEnd

     LEFT OUTER JOIN dbo.LinkEndType_Mode ON dbo.LinkEnd.LinkEndType_mode_id =   dbo.LinkEndType_Mode.id

     LEFT OUTER JOIN dbo.LinkEndType ON dbo.LinkEnd.linkendtype_id =    dbo.LinkEndType.id
     RIGHT OUTER JOIN dbo.LinkFreqPlan_Neighbors ON dbo.LinkEnd.id =   dbo.LinkFreqPlan_Neighbors.rx_linkend_id
     
     LEFT JOIN dbo.LinkEnd LinkEnd2 ON LinkEnd2.id =   dbo.LinkFreqPlan_Neighbors.tx_linkend_id

go

----------------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-------------------------------------------------------------
IF OBJECT_ID(N'[dbo].[sp_LinkFreqPlan_Linkend]') IS NOT NULL
  DROP  PROCEDURE dbo.sp_LinkFreqPlan_Linkend 
GO



/* ---------------------------------------------------------------
created: Alex  
used in:  TdmLinkFreqPlan_CIA_view.Load_LinkFreqPlan
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_LinkFreqPlan_Linkend 
(
@LinkFreqPlan_id  INT
)

AS
BEGIN
--  DECLARE @project_id int

/*
    SELECT @project_id=project_id 
    	FROM LINKFREQPLAN
      WHERE id=@LinkFreqPlan_id
*/


 --SELECT  0  as ddd 



    SELECT 
      LinkEnd_name ,  
     length,
    
    
      LinkEnd.[name] as rx_LinkEnd_name,
    
    
 --    (SELECT name FROM LinkEnd 
    --   WHERE id=LINKFREQPLAN_LINKEND.linkend_id)  as [linkend_name],

     (SELECT [rx_level_dBm] FROM LINKFREQPLAN_NEIGHBORS 
          WHERE  (LinkFreqPlan_id=@LinkFreqPlan_id) AND 
                 (IsLink = 1) AND
                 (rx_linkend_id = M.linkend_id))   AS [rx_level_dBm],
                 
     
--     (SELECT length FROM Link 
  --     WHERE (project_id=@project_id) and
    --         ((linkend1_id=LINKFREQPLAN_LINKEND.linkend_id) OR 
      --        (linkend2_id=LINKFREQPLAN_LINKEND.linkend_id)))
        --     AS [length],

     (SELECT Count(*) FROM LINKFREQPLAN_NEIGHBORS 
         WHERE (IsLink<>1) and 
       
             (LinkFreqPlan_id=@LinkFreqPlan_id) and
             (RX_Linkend_id= M.linkend_id) )
             as [count], 
             
     (SELECT Count(*) FROM LINKFREQPLAN_NEIGHBORS 
         WHERE (IsNeigh=1) and (IsLink<>1) and 
       
             (LinkFreqPlan_id=@LinkFreqPlan_id) and
             (RX_Linkend_id=M.linkend_id))
             as [count1], 
             
     m.id,     
     cia, ci, ca, linkend_id, threshold_degradation 
     
    FROM  view_LINKFREQPLAN_LINKEND M    
        LEFT OUTER JOIN LinkEnd  
          ON M.next_linkend_id = LinkEnd.id

--link.next_linkend_id, 

  --      LEFT OUTER JOIN Link 
    --      ON LinkFreqPlan_LinkEnd.link_id = Link.id

      
     WHERE  (LinkFreqPlan_id=@LinkFreqPlan_id)
   
  
     

END

go


delete from Map_XREF  where MapObject_name='pmp_sector_ant'
go


delete from Map_objects  where name='pmp_sector_ant'
go

-------------------------------

IF OBJECT_ID(N'gis.sp_Relief_Add') IS not  NULL
  DROP PROCEDURE gis.sp_Relief_Add
GO



CREATE PROCEDURE gis.sp_Relief_Add
(
  @PROJECT_ID  	INT,
  
  @NAME 		varchar(250),  -- file name
  
  @FILESIZE 	BIGINT, --bytes
  @FILEDATE 	DATETIME,
  
  @STEP_X FLOAT=null,
  @STEP_Y FLOAT=null,
  
  @X_MIN FLOAT=null,
  @Y_MAX FLOAT=null,
  @X_MAX FLOAT=null,
  @Y_MIN FLOAT=null,  
    
  @STEP_LAT FLOAT=null,
  @STEP_LON FLOAT=null,

  @LAT_MIN FLOAT=null,
  @LON_MAX FLOAT=null,
  @LAT_MAX FLOAT=null,
  @LON_MIN FLOAT=null,
  
  
  @COMMENT varchar(250)=null,
  
  @GEOM_STR varchar(max)
    
)
AS
BEGIN
  ------------------------------------------------------
  IF @PROJECT_ID is null  RAISERROR ('@ID is null', 16, 1);
  IF @NAME is null  RAISERROR ('@NAME is null', 16, 1);
  ------------------------------------------------------
 
  DECLARE
    @new_id int,
    @id  INT;
    

--set @GEOM_STR = replace(@GEOM_STR,',','.')

    
 -- set @NAME=alltrim(@NAME)  

  SELECT @id=id 
  	FROM Relief 
--  	FROM view_Relief 
    WHERE (PROJECT_ID=@PROJECT_ID) and (NAME=@NAME) and (HostName = HOST_NAME())

  IF @id IS NOT NULL 
    RETURN @id;
            

  INSERT INTO Relief
     (PROJECT_ID, 
      NAME,
      geom
      )
  VALUES
     (@PROJECT_ID, 
      @Name,
      geometry::STPolyFromText(@geom_str, 4326).MakeValid()
      )
   /* 
     geom = case
       		  when @geom_str is not null 
                 then geometry::STPolyFromText(@geom_str, 4326).MakeValid()
              else null   
            end
*/  
    
  set @new_id= IDENT_CURRENT ('Relief')
    
  INSERT into Relief_XREF  (Relief_id) VALUES (@new_id)
  
  --set @new_id=@@IDENTITY;   
  
  UPDATE Relief
  SET 
     FILESIZE = @FILESIZE,
     FILEDATE = @FILEDATE,  
     
     STEP_X  =   @STEP_X,  
     STEP_Y  =   @STEP_Y,  
                  
     X_MIN   =    @X_MIN,  
     Y_MAX   =    @Y_MAX, 
     X_MAX   =    @X_MAX, 
     Y_MIN   =    @Y_MIN, 
                    
     STEP_LAT=    @STEP_LAT, 
     STEP_LON=    @STEP_LON, 
                 
     LAT_MIN =    @LAT_MIN,  
     LON_MAX =    @LON_MAX,  
     LAT_MAX =    @LAT_MAX,  
     LON_MIN =    @LON_MIN,  
          
     COMMENT   = @COMMENT
   
/*
     geom = case
       		  when @geom_str is not null 
                 then geometry::STPolyFromText(@geom_str, 4326).MakeValid()
              else null   
            end
*/
  WHERE 
    id = @new_id
  
  RETURN @new_id;
     


END
     
go


IF OBJECT_ID(N'gis.sp_Relief_by_LatLon_SEL') IS not  NULL
  DROP PROCEDURE gis.sp_Relief_by_LatLon_SEL
GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE gis.sp_Relief_by_LatLon_SEL
(
  @PROJECT_ID  	INT ,  

  @LAT float, 
  @LON float

)
AS
BEGIN
  if @LAT is null 
  begin
    set @LAT = 60.0176
    set @LON = 30.398

  end


  DECLARE 
    @g geometry =  geometry::Point(@LON, @LAT,  4326) --.ShortestLineTo (geometry::Point(@LON2,@LAT2,  4326)) ; 


  SELECT * 
     --    geom.ToString()
  FROM 
    Relief
  where   
    @g.STIntersects(geom) =1 
    and   (hostname=host_name())  
    and project_id = @project_id
    
  return @@ROWCOUNT  

END
go

-------------------------------------------------
IF OBJECT_ID(N'gis.sp_Relief_geom_clear_UPD_ALL') IS not  NULL
  DROP PROCEDURE gis.sp_Relief_geom_clear_UPD_ALL
GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE gis.sp_Relief_geom_clear_UPD_ALL

AS
BEGIN 
  
  UPDATE Relief
  SET      
     geom = null
         

  
  return @@ROWCOUNT  ;
  
  


END
go


-------------------------------------------------
IF OBJECT_ID(N'gis.sp_Relief_Intersects_line_SEL') IS not  NULL
  DROP PROCEDURE gis.sp_Relief_Intersects_line_SEL
GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE gis.sp_Relief_Intersects_line_SEL
(
  @PROJECT_ID  	INT,   

  @LAT1 float, 
  @LON1 float,
  
  @LAT2 float, 
  @LON2 float

)
AS
BEGIN
  if @LAT1 is null 
  begin
    set @LAT1 = 55.793038
    set @LON1 =37.808243

    set @LAT2 = 55.794038
    set @LON2 =37.818243

  end


  DECLARE 
    @g geometry =  geometry::Point(@LON1,@LAT1,  4326).ShortestLineTo (geometry::Point(@LON2,@LAT2,  4326)) ; 


  SELECT *, 
         geom.ToString()
  FROM 
    Relief
  where   
    @g.STIntersects(geom) =1 
    and   (hostname=host_name())  
    and project_id = @project_id
    and id in  (SELECT relief_id  FROM Relief_XREF WHERE checked = 1  ) 

    
  return @@ROWCOUNT  


    --POLYGON ((37.767419 55.815241, 
              --37.768846 55.770046, 
              --37.849021 55.770823, 
              --37.847687 55.816019, 
              --37.767419 55.815241))


  
  /*

SELECT *, 
         geo_WGS.STAsText (),
         geo_WGS.STSrid,
         geo_WGS.EnvelopeCenter().ToString()   
  FROM 
    GIS.Relief  
  
  
  
SELECT
  pt1.ToString(),
  pt2.ToString(),
  pt1.ShortestLineTo(pt2).ToString() AS line -- what goes here
FROM #tmp;


DECLARE @g geometry;  
DECLARE @h geometry;  
SET @g = geometry::STGeomFromText('POLYGON((0 0, 0 2, 2 2, 2 0, 0 0))', 0);  
SET @h = geometry::STGeomFromText('POLYGON((1 1, 3 1, 3 3, 1 3, 1 1))', 0);  
SELECT @g.STIntersection(@h).ToString();  
POINT (37.808243250129351 55.7930387796179)
*/
  /* Procedure body */
END


go


-------------------------------------------------
IF OBJECT_ID(N'gis.sp_Relief_UPD') IS not  NULL
  DROP PROCEDURE gis.sp_Relief_UPD
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE gis.sp_Relief_UPD
(
  @ID  	INT,
  
  @geom_str varchar(max)
    
)
AS
BEGIN 
--set @GEOM_STR = replace(@GEOM_STR,',','.')
  


  UPDATE Relief
  SET      
     geom =  geometry::STPolyFromText(@geom_str, 4326).MakeValid()
         

  WHERE 
    id = @id
  
  return @@ROWCOUNT  ;
  
  


END
go

-------------------------------------------------
IF OBJECT_ID(N'gis.sp_Relief_UPD_ALL') IS not  NULL
  DROP PROCEDURE gis.sp_Relief_UPD_ALL
GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE gis.sp_Relief_UPD_ALL

AS
BEGIN 
  
  UPDATE Relief
  SET      
     geom = geom.MakeValid()
         

  WHERE 
   geom is not null
  
  return @@ROWCOUNT  ;
  
  


END

go

-------------------------------------------------
IF OBJECT_ID(N'gis.sp_Relief_with_null_geom_SEL') IS not  NULL
  DROP PROCEDURE gis.sp_Relief_with_null_geom_SEL
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE gis.sp_Relief_with_null_geom_SEL
(
  @PROJECT_ID  	INT    
)
AS
BEGIN   
  select * from Relief
  where
     geom is null    
     and   (hostname=host_name())  
           
     and project_id = @project_id
  
  return @@ROWCOUNT  ;


END

go



  IF OBJECT_ID(N'[history].[view_columns]') IS  not  NULL
DROP VIEW [history].[view_columns]
GO
/****** Object:  View [history].[view_columns]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [history].[view_columns]
AS

 select top 100 percent
 
col.*  , 


case
  when c.Column_name is not null then '(select name from '+
                                         FK.Target_Table_SCHEMA +'.'+ FK.Target_Table_name 
                                         +' m where m.id = '+ col.column_name +') as '+ FK.Target_Table_name + '_name'
  
  when col.data_type='float' then 'cast ('+ col.column_name+' as varchar(20)) as '+ col.column_name
  else col.column_name  
end    
 as xml_column_name,
 
 fk.Target_Table_SCHEMA,
 fk.Target_Table_name,
 fk.Target_Column,
 
 c.Column_name as fk_Column_name
  

 from  INFORMATION_SCHEMA.COLUMNS  col
    INNER JOIN sys.columns AS sc ON sc.object_id = object_id(col.table_schema + '.' + col.table_name) 
                                  AND sc.NAME = col.COLUMN_NAME
    inner join sys.objects obj on obj.object_Id=sc.object_Id 
  --  left join  sys.computed_columns cc on cc.name = col.COLUMN_NAME and cc.object_id = obj.object_Id

  left join history.view_FK FK on FK.Source_TABLE_SCHEMA=col.table_schema and 
                                  FK.Source_Table_name=col.table_name and 
                                  FK.Source_Column=col.column_name
                                  
  left join INFORMATION_SCHEMA.COLUMNS c on c.TABLE_SCHEMA=FK.Target_Table_SCHEMA and 
                                            c.Table_name=FK.Target_Table_name and                                        
                                            c.Column_name='name'
                                  
                                    

/*  ccu.constraint_name AS SourceConstraint
   ,ccu.TABLE_SCHEMA AS Source_TABLE_SCHEMA
   
     ,ccu.table_name AS Source_Table_name
    ,ccu.column_name AS Source_Column
    
    ,kcu.table_name AS Target_Table_name
    ,kcu.table_name AS Target_Table_SCHEMA
    ,kcu.column_name AS Target_Column
*/

where 
  obj.[type]='u'
  
  and col.DATA_TYPE<>'text'
  
--  and data_type='float'

  and col.COLUMN_NAME not like'[_][_]%'
  and col.COLUMN_NAME not like'%[_][_]'
  and col.COLUMN_NAME not like '[_]'
  and col.COLUMN_NAME not like'%[-]'
  and col.COLUMN_NAME not like'%[11]'

  and col.table_NAME not like'[_][_]%'
  and col.table_NAME not like'%[_][_]'
  and col.table_NAME not like'%[11]'
  


  and col.COLUMN_NAME not in
      (
--       'id', 
       'guid', 
       'date_modify',
       'date_created',
       'user_modify',
       'user_created'
--       'project_id'
      )


--  and cc.definition is null
  
  and sc.Is_computed<>1

order by 
  col.table_schema,
  col.table_name , 
  col.ordinal_position
  
  /*
  CREATE VIEW history.view_FK
AS
SELECT 
    ccu.constraint_name AS SourceConstraint
   ,ccu.TABLE_SCHEMA AS Source_TABLE_SCHEMA
   
     ,ccu.table_name AS Source_Table_name
    ,ccu.column_name AS Source_Column
    
    ,kcu.table_name AS Target_Table_name
    ,kcu.table_name AS Target_Table_SCHEMA
    ,kcu.column_name AS Target_Column
    
FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu
    INNER JOIN INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
        ON ccu.CONSTRAINT_NAME = rc.CONSTRAINT_NAME 
    INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu 
        ON kcu.CONSTRAINT_NAME = rc.UNIQUE_CONSTRAINT_NAME
*/

GO




--------------------------------------------------------------
IF OBJECT_ID(N'[history].[view_COLUMNS_group]') IS NOT NULL
DROP view [history].[view_COLUMNS_group]
GO


CREATE VIEW history.view_COLUMNS_group
AS
    
SELECT 
      
	 T.[TABLE_SCHEMA] +'.'+ T.TABLE_NAME as TABLE_NAME_full,

  SUBSTRING(
          (
              SELECT ','+  [xml_column_name] AS [text()]
                FROM history.view_columns C  
                WHERE 
                --     DATA_TYPE not in ('text') and 
                
                      C.TABLE_SCHEMA =  T.TABLE_SCHEMA and 
                      C.TABLE_NAME=  T.TABLE_NAME                       
              FOR XML PATH ('')
                ), 2, 10000000000) [sql_columns] 


  FROM  [INFORMATION_SCHEMA].[TABLES] T
  where TABLE_TYPE = 'BASE TABLE' and
  
        TABLE_NAME not in ('msfavorites') and 
        TABLE_NAME not like '%[11]' and
        TABLE_NAME not like '%[__]'

go



IF OBJECT_ID(N'[lib].[view_Lookup_Bitrate]') IS NOT NULL
DROP view [lib].[view_Lookup_Bitrate]
GO

CREATE VIEW lib.view_Lookup_Bitrate
AS
  select distinct bitrate_Mbps
  from LinkendType_mode

go




/****** Object:  UserDefinedFunction [lib].[ft_AntennaType_mask_SEL]    Script Date: 16.05.2020 10:54:58 ******/
  IF OBJECT_ID(N'[lib].[ft_AntennaType_mask_SEL]') IS  not  NULL

DROP FUNCTION [lib].[ft_AntennaType_mask_SEL]
GO
/****** Object:  UserDefinedFunction [lib].[ft_AntennaType_mask_SEL]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE FUNCTION [lib].[ft_AntennaType_mask_SEL] 
(
  @ID int  ,
  @MASK_TYPE varchar(10) --horz,vert
 
)

--346447
--RETURNS varchar(50)

RETURNS @ret TABLE 
(  
    angle float,
    loss  float     
)

as

BEGIN

--set @ID=16683

--  select * from AntennaType   where  id  = 16683 


------------------------------------------------
-- test
------------------------------------------------
if @ID is null 
begin
  select top 1 @ID = id   from AntennaType   where horz_mask is not null
   
  set  @MASK_TYPE = 'vert'
end    
------------------------------------------------

 -- declare 
 -- 	@result varchar(50);
   


  DECLARE
    @mask varchar(max), -- ='0=32;1=32;2.5=48;5=51.5;15=54;23=62;33=63;36=66;60=67;75=74;180=74;285=74;300=67;324=66;327=63;337=62;345=54;355=51.5;357.5=48;359=32;',
    @xml XML


select @mask =  
   case
     when @MASK_TYPE= 'horz' then horz_mask
     when @MASK_TYPE= 'vert' then vert_mask
   end  

  from AntennaType 
  where id=@id



--set @mask = '0,0=38,0;4,0=38,0;10,0=52,0;15,0=55,0;22,0=55,0;25,0=57,0;48,0=57,0;60,0=60,0;72,0=60,0;85,0=66,0;180,0=66,0;275,0=66,0;288,0=60,0;300,0=60,0;312,0=57,0;335,0=57,0;338,0=55,0;345,0=55,0;350,0=52,0;356,0=38,0'

set @mask = replace (@mask, ',','.')


  if right(@mask, 1) = ';'
    set @mask = substring(@mask, 1, len(@mask) - 1 )

      
      
  set  @mask = LTrim(RTrim( @mask))  
  set  @mask = replace (@mask, '=', '" loss="')  
  set  @mask = replace (@mask, ';', '"/><item angle="')  

  set  @mask = '<item angle="'+  @mask + '"/>'

 -- print @mask 

  set  @xml = CONVERT (xml, @mask  )

  insert into @ret
   select * from 
     (
      SELECT  
         Tbl.Col.value('@angle', 'float') as angle,  
         Tbl.Col.value('@loss', 'float')  as loss 
            
      FROM   @xml.nodes('//item') Tbl(Col)  
     ) m 
     order by angle

	RETURN
  
END

GO
/****** Object:  StoredProcedure [lib].[ft_AntennaType_restore_from_mask]    Script Date: 16.05.2020 10:54:58 ******/

  IF OBJECT_ID(N'[lib].[ft_AntennaType_restore_from_mask]') IS  not  NULL
DROP PROCEDURE [lib].[ft_AntennaType_restore_from_mask]
GO
/****** Object:  StoredProcedure [lib].[ft_AntennaType_restore_from_mask]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--CREATE PROCEDURE lib.ft_AntennaType_restore_polarization_ratio_from_vert_mask
CREATE PROCEDURE [lib].[ft_AntennaType_restore_from_mask]
(
  @ID int
)
AS
BEGIN

---------------------------------------------
-- test
---------------------------------------------
if @ID is null 
begin
set @id = 18553

--     select top 1 @ID = id  from AntennaType  where vert_mask is not null

--  select top 1 * from AntennaType  where ID = @id 

  
end 
/*
declare 
  @loss_vert float,
  @loss_horz float


select top 1 @loss_vert = loss from lib.ft_AntennaType_mask_SEL (@ID,'vert') where angle=0  
select top 1 @loss_horz = loss from lib.ft_AntennaType_mask_SEL (@ID,'horz') where angle=0  

print @loss_vert
print @loss_horz
*/

/*
select * from lib.ft_AntennaType_mask_SEL (@ID,'vert')
select * from lib.ft_AntennaType_mask_SEL (@ID,'horz') 
*/

  UPDATE AntennaType
    set 
      polarization_ratio =
       
            (select top 1 loss from lib.ft_AntennaType_mask_SEL (@ID,'vert') where angle=0) +
            (select top 1 loss from lib.ft_AntennaType_mask_SEL (@ID,'horz') where angle=0)
            
 
   where ID=@ID
       
END

GO
/****** Object:  StoredProcedure [lib].[sp_Template_Link_Add]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[lib].[sp_Template_Link_Add]') IS NOT NULL

DROP PROCEDURE [lib].[sp_Template_Link_Add]
GO
/****** Object:  StoredProcedure [lib].[sp_Template_Link_Add]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE [lib].[sp_Template_Link_Add]
(
  @NAME VARCHAR(100)      
)
AS
BEGIN
  declare 
    @id int 

  INSERT INTO Template_Link  (NAME) VALUES (@NAME);     
  
  set @id = IDENT_CURRENT ('lib.Template_Link')   

  RETURN @id
 
  
END

GO







IF OBJECT_ID(N'lib.Modulations') IS  NULL

CREATE TABLE lib.Modulations (
  id int IDENTITY(1, 1) NOT NULL,
  NAME varchar(50) COLLATE Cyrillic_General_CI_AS NOT NULL,
  level_count float NULL,
  GOST_53363_modulation varchar(50) COLLATE Cyrillic_General_CI_AS NULL,
  PRIMARY KEY CLUSTERED (id)
    WITH (
      PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF,
      ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
ON [PRIMARY]
GO



IF OBJECT_ID(N'dbo.sp_Template_Linkend_Antenna_Add') IS not  NULL
  DROP PROCEDURE dbo.sp_Template_Linkend_Antenna_Add
GO



/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE procedure dbo.sp_Template_Linkend_Antenna_Add
(    
  @NAME 									varchar(50),          
  @TEMPLATE_LINKEND_ID   	int,
  
  @ANTENNATYPE_ID		int =null,     

  @HEIGHT  	float=null, 
  @AZIMUTH 	float=null, 
  @TILT 		float=null,
  @LOSS 	float=null 


)
AS
begin
  ---------------------------------------------
  IF @Name IS NULL  RAISERROR ('@Name is NULL', 16, 1);
  ---------------------------------------------

  DECLARE
  	@new_id int;
  		

  set @HEIGHT=ROUND(@HEIGHT,1)
  set @AZIMUTH=ROUND(@AZIMUTH,1)
  

  INSERT INTO Template_Linkend_Antenna
     (NAME, 
      TEMPLATE_LINKEND_ID,
      ANTENNATYPE_ID,  
    
      HEIGHT, AZIMUTH, TILT,
      LOSS
    
      )
  VALUES
     (@NAME, 
      @TEMPLATE_LINKEND_ID,
     	@ANTENNATYPE_ID,  
     
      @HEIGHT, @AZIMUTH, @TILT,
      @LOSS     
      ) 
  
  
    
  set @new_id = IDENT_CURRENT ('Template_Linkend_Antenna')
   
  
 -- set @new_id = @@IDENTITY;

/*  IF IsNull(@ANTENNATYPE_ID,0)>0
    exec sp_Object_Update_AntType 
    	@OBJNAME='Template_Linkend_Antenna',
      @ID=@new_id,
      @AntennaType_ID=@ANTENNATYPE_ID;
 */ 

  RETURN @new_id;
     
        
end

go


IF OBJECT_ID(N'lib.sp_CalcModel_Del') IS not  NULL
  DROP PROCEDURE lib.sp_CalcModel_Del
GO

/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE lib.sp_CalcModel_Del
(
  @ID INT  -- CalcMap ID
)
AS
BEGIN
 
  update Pmp_CalcRegion
    set Calc_Model_id = null
    where Calc_Model_id = @id
  
  update Template_LinkEnd
    set Calc_Model_id = null
    where Calc_Model_id = @id

  update LinkEnd
    set Calc_Model_id = null
    where Calc_Model_id = @id


  --DELETE FROM Map_XREF WHERE CalcMap_id =@ID

  DELETE FROM CalcModel WHERE  id=@ID;
  
--  select * from  CalcMap WHERE  id=@ID;
  
  
 RETURN @@ROWCOUNT
    

END
go
------------------------------------------------------------------
IF OBJECT_ID(N'dbo.sp_Object_Update_LinkEndType_Mode') IS not  NULL
  DROP PROCEDURE dbo.sp_Object_Update_LinkEndType_Mode  
GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */

CREATE PROCEDURE dbo.sp_Object_Update_LinkEndType_Mode  
(
  @OBJNAME	varchar(50),
  @ID   	int,    
  @LinkEndType_Mode_ID 	int   
)
AS
BEGIN
  IF @OBJNAME IS NULL  RAISERROR ('@OBJNAME is NULL', 16, 1);
  IF @ID IS NULL  RAISERROR ('@ID is NULL', 16, 1);



---------------------------------------------
  IF @OBJNAME in ('LinkEnd','pmp_sector','pmp_terminal')
---------------------------------------------
  BEGIN
       
    IF EXISTS(SELECT * FROM view_LinkEndType_Mode WHERE id = @LinkEndType_Mode_ID)
    BEGIN
      UPDATE LinkEnd SET  
        LinkEndType_Mode_ID = m.ID,
        Mode 			= m.mode,
        bitrate_Mbps    = m.bitrate_Mbps,
                 
        MODULATION_TYPE =m.MODULATION_TYPE,
        SIGNATURE_HEIGHT=m.SIGNATURE_HEIGHT, 
        SIGNATURE_WIDTH =m.SIGNATURE_WIDTH,  
        THRESHOLD_BER_3 =m.THRESHOLD_BER_3,  
        THRESHOLD_BER_6 =m.THRESHOLD_BER_6,  
            
        channel_width_MHz = m.bandwidth, -- //_MHz,  
        
        POWER_dBm       = m.POWER_MAX,
        
       MODULATION_COUNT      = m.MODULATION_level_COUNT,
       GOST_53363_modulation = m.GOST_53363_modulation,        
        
        Freq_spacing  =  m.bandwidth * IsNull(CHANNEL_SPACING,0) 

        
       from 
          (select * from view_LinkEndType_Mode WHERE id=@LinkEndType_Mode_ID) m     
       WHERE 
         (LinkEnd.id = @ID) 
 
     END 
     
  END ELSE       


  
---------------------------------------------
  IF @OBJNAME='Template_linkend'
---------------------------------------------
  BEGIN
  
    IF EXISTS(SELECT * FROM LinkEndType_Mode WHERE id = @LinkEndType_Mode_ID)
    BEGIN
      UPDATE Template_linkend SET  
        LinkEndType_Mode_ID = m.ID,
        
        
        THRESHOLD_BER_3 =m.THRESHOLD_BER_3,  
        THRESHOLD_BER_6 =m.THRESHOLD_BER_6,
        
        POWER_dBm       = m.POWER_MAX 
        
       from 
          (select * from view_LinkEndType_Mode WHERE id=@LinkEndType_Mode_ID) m     
       WHERE 
         (Template_linkend.id = @ID)     
     END 
     
  END ELSE begin
    RAISERROR ('sp_Object_Update_LinkEndType_Mode  error ', 16, 1);       
    return -1
  END
  
  RETURN @@ROWCOUNT 

END
go
---------------------------------------------------

IF OBJECT_ID(N'dbo.sp_Template_Linkend_Del') IS not  NULL
  DROP PROCEDURE dbo.sp_Template_Linkend_Del
GO




CREATE procedure dbo.sp_Template_Linkend_Del
( 

  @ID	int
)
AS
begin    
  delete from Template_LinkEnd_Antenna    where Template_LinkEnd_ID = @id

  delete from Template_Linkend   where ID = @id
   
  return @@ROWCOUNT
  
end

go






IF OBJECT_ID(N'dbo.sp_Link_Repeater_Antenna_Add') IS not  NULL
  DROP PROCEDURE dbo.sp_Link_Repeater_Antenna_Add
GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE procedure dbo.sp_Link_Repeater_Antenna_Add
(    
  @Link_Repeater_ID int,
  @Name VARCHAR(50), 
 
  -----------------------------------

  @ANTENNATYPE_ID		int =null,     

  @HEIGHT  	float=null, 
  @AZIMUTH 	float=null, 
  @TILT 	float=null,
  @LOSS  	float=null,

  -----------------------------------
  -- Custom
  -----------------------------------
  @BAND 		varchar(50)=null, 
  @Freq_MHz 	float=null,
  @GAIN	    	float=null,          --_dBi
  @DIAMETER   	float=null,      
  @VERT_WIDTH 	float=null,
  @HORZ_WIDTH 	float=null,
  @POLARIZATION_STR varchar(4)=null,
  


 @SLOT int
-- @DISPLAY_ORDER int

--  @REPEATER_ID 	    int =null


--@LOCATION_TYPE int = null


--@LAT 		float=null,
--@LON 		float=null,


)
AS
begin
  DECLARE 
    --@sName VARCHAR(100),
    
    @count int, 
    @new_id int, 
    @pmp_site_id int,
    
  --  @project_id  int, 
    @property_id int,         
           
    @LAT 		float,
    @LON 		float,
    @LOCATION_TYPE int
         
   
 
 --    @aLat float,
  --   @aLon float
     
  --258929
  
    -- Name     : ����������� � ���� � ��������� before insert
  -- Address  : ����������� � ���� � ��������� before insert
  


  SELECT 
      @property_id=PROPERTY_ID
        -- @project_id = project_id  
    FROM Link_Repeater 
    WHERE id = @Link_Repeater_ID

  IF (@property_id IS NULL)  
      RAISERROR ('RAISERROR - @PROPERTY_ID is NULL', 16, 1);




  SELECT 
    --  @project_id=PROJECT_ID, 
      @lat=LAT, 
      @lon=LON
    FROM Property  
    WHERE id = @property_id 

--SET @LOCATION_TYPE = 0;



--     (@PMP_SECTOR_ID IS NULL) and 
  --   (@PMP_TERMINAL_ID IS NULL)
  
  -- RAISERROR ('@LINKEND_ID,@PMP_SECTOR_ID,@PMP_TERMINAL_ID is NULL', 16, 1);
    

  if  (@ANTENNATYPE_ID  is not null )
  BEGIN
    SELECT 
      @BAND        = BAND,  

      @Gain        = Gain,  
      @Freq_MHz    = Freq,
--      @Freq_MHz    = Freq_MHz,
      @Diameter    = Diameter,
      @Polarization_STR= Polarization_STR,
      @Vert_Width  = Vert_Width,
      @Horz_Width  = Horz_Width
    FROM AntennaType 
    WHERE id = @ANTENNATYPE_ID      
  END



  INSERT INTO Link_Repeater_Antenna
     (
      NAME, 

      Link_Repeater_ID,
      
       HEIGHT ,   
      
    --  property_id,      
  --    project_id,
      
    --  DISPLAY_ORDER,
      SLOT
      
    --  PROPERTY_ID
      )
  VALUES
     (
      @NAME,      

      @Link_Repeater_ID,
      @HEIGHT, 
      
    --  @property_id,
    --  @project_id,
      
   --   @DISPLAY_ORDER,
      @SLOT
        
    --  @property_id
      ) 



  --set @new_id = @@IDENTITY
  
  set @new_id= IDENT_CURRENT ('Link_Repeater_Antenna')   
    


  UPDATE Link_Repeater_ANTENNA
  set    
      ANTENNATYPE_ID=@ANTENNATYPE_ID,  
     
--      HEIGHT     =@HEIGHT, 
      GAIN		=@GAIN, 

      BAND		=@BAND,
      FREQ_MHz	=@FREQ_MHz,
      
      LOSS	=@LOSS,  
      DIAMETER	=@DIAMETER, 
      VERT_WIDTH=@VERT_WIDTH, 
      HORZ_WIDTH=@HORZ_WIDTH, 
      
      POLARIZATION_STR=@POLARIZATION_STR,
      
      AZIMUTH    =@AZIMUTH,
      TILT       =@TILT, 
      
      LAT           =@LAT,  
      LON           =@LON, 
      LOCATION_TYPE =@LOCATION_TYPE
     
  WHERE id=@new_id



  RETURN @new_id;

        
end


go



IF OBJECT_ID(N'[dbo].[view_Link_Repeater_with_project_id]') IS NOT NULL
  DROP VIEW dbo.view_Link_Repeater_with_project_id
GO


CREATE VIEW dbo.view_Link_Repeater_with_project_id
AS
SELECT dbo.Link_Repeater.*,
       dbo.Property.project_id
       
FROM dbo.Link_Repeater
     LEFT OUTER JOIN dbo.Property ON dbo.Link_Repeater.property_id =  dbo.Property.id

go
-------------------------------------------------------




--------------------------------------------
IF OBJECT_ID(N'[dbo].[view_Link_repeater]') IS NOT NULL
  DROP VIEW dbo.view_Link_repeater
GO


CREATE VIEW dbo.view_Link_repeater
AS
SELECT 
  dbo.Link_Repeater.*,
  
  dbo.Property.Project_id,


  dbo.Property.lat,
  dbo.Property.lon,
  dbo.Property.ground_height,

A1.height as Antenna1_height,
A2.height as Antenna2_height,

A1.loss as Antenna1_loss,
A2.loss as Antenna2_loss,


A1.gain as Antenna1_gain,
A2.gain as Antenna2_gain,


A1.azimuth as azimuth1,
A2.azimuth as azimuth2,



A1.polarization_str as polarization1,
A2.polarization_str as polarization2,


A1.diameter as diameter1,
A2.diameter as diameter2,


A1.height as height1,
A2.height as height2,

A1.loss as loss1,
A2.loss as loss2,


A1.gain as gain1,
A2.gain as gain2

    

      
FROM

  dbo.Link_Repeater  
  LEFT OUTER JOIN dbo.Property ON (dbo.Link_Repeater.property_id = dbo.Property.id) 
  
  LEFT OUTER JOIN dbo.Link_Repeater_Antenna A1 ON (A1.Link_Repeater_id = dbo.Link_Repeater.id) and  (A1.slot=1)  
  LEFT OUTER JOIN dbo.Link_Repeater_Antenna A2 ON (A2.Link_Repeater_id = dbo.Link_Repeater.id) and  (A2.slot=2)
go


----------------------------------------


IF OBJECT_ID(N'dbo.view_Link_repeater_antenna') IS not  NULL
  DROP VIEW dbo.view_Link_repeater_antenna
GO


CREATE VIEW dbo.view_Link_repeater_antenna
-- 
--     
-- 
AS
SELECT TOP 100 PERCENT 
  dbo.Link_Repeater_Antenna.*,
  dbo.AntennaType.name AS AntennaType_name,
  dbo.AntennaType.band AS AntennaType_band
FROM
  dbo.Link_Repeater_Antenna
  LEFT OUTER JOIN dbo.Link_Repeater ON (dbo.Link_Repeater_Antenna.Link_Repeater_id = dbo.Link_Repeater.id)
--  LEFT OUTER JOIN dbo.Property ON (dbo.Link_Repeater.property_id = dbo.Property.id)
  LEFT OUTER JOIN dbo.AntennaType ON (dbo.Link_Repeater_Antenna.antennaType_id = dbo.AntennaType.id)
  
  ORDER BY
  dbo.Link_Repeater_Antenna.slot


go


-------------------------------------------------------------------------------------------------------------


IF OBJECT_ID(N'dbo.sp_Link_Set_link_repeater') IS not  NULL
  DROP PROCEDURE dbo.sp_Link_Set_link_repeater
GO



/* ---------------------------------------------------------------
 --------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Link_Set_link_repeater

(
  @ID int, 
  @LINK_repeater_ID int 
)
AS
BEGIN

  DECLARE 
    @result int

  DECLARE
    @len float, 
   
    @LINK_ID int,
    @property_rep_ID int,
    @property2_ID int,
    @property1_ID int ; 
  
-- IF @PROPERTY_ID IS NULL  RAISERROR ('@PROPERTY_ID is NULL', 16, 1);

if @LINK_repeater_ID=0 
  set @LINK_repeater_ID=null


  if not Exists (select * from link where id=@id)
    RAISERROR ('@link not exists', 16, 1);


  if not Exists (select * from Link_Repeater where ID = @LINK_repeater_ID)
    RAISERROR ('@Link_Repeater not exists', 16, 1);


--select * from link where id=@id


    UPDATE Link_Repeater  
      SET 
        LINK_ID=NULL,
        
        profile_XML_1=NULL,
        profile_XML_2=NULL,
        
        calc_results_xml_1=NULL,
        calc_results_xml_2=NULL,
        calc_results_xml_3=NULL
    
    
    WHERE  LINK_ID = @ID



----------------------------------
----------------------------------

----------------------------------
  if @LINK_repeater_ID is not null 
----------------------------------  
  begin   

    SELECT       
       @property1_ID  = l1.property_ID,
       @property2_ID  = l1.property_ID
     FROM link
       left join linkend L1 on L1.id=link.linkend1_id
       left join linkend L2 on L2.id=link.linkend2_id
     
     WHERE (link.id = @ID)   
   

   
 
    SELECT 
         @property_rep_ID=property_ID,
         @LINK_ID = LINK_ID
      FROM Link_Repeater
      WHERE (id = @LINK_repeater_ID)      
      
      
      
   set @len =  dbo.fn_Property_Get_Length_m (@property1_ID, @property_rep_ID);
   IF @len<50   RAISERROR (' @len<50 @PROPERTY1_ID is NULL', 16, 1);
     
   set @len =  dbo.fn_Property_Get_Length_m (@property2_ID, @property_rep_ID);
   IF @len<50  RAISERROR (' @len<50 @PROPERTY2_ID is NULL', 16, 1);

     


      print @property_rep_ID  
        
      
      IF @property1_ID IS NULL  RAISERROR ('@property1_ID is NULL', 16, 1);
      IF @property2_ID IS NULL  RAISERROR ('@property2_ID is NULL', 16, 1);

      
      if @property_rep_ID = @property1_ID or 
         @property_rep_ID = @property2_ID 
       return -1     
      
  
    ----------------------------------
 --   UPDATE Link           SET has_repeater=1             WHERE (id = @ID)
    UPDATE Link_Repeater  SET LINK_ID=@ID  WHERE (id = @LINK_repeater_ID)

        
  end
  

 set @result = @@ROWCOUNT


exec sp_Link_Update_Length_and_Azimuth @id = @id


  exec sp_Link_ClearCalc @ID=@ID

  return @result


END
go

--------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID(N'dbo.sp_Link_repeater_Update') IS not  NULL
  DROP PROCEDURE dbo.sp_Link_repeater_Update
GO



/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Link_repeater_Update
(
  @ID   int,  
  
--  @MODE VARCHAR(50) =null,  --'Calc_Results'
  ------------------------- 
  
  @CALC_RESULTS_XML_1 TEXT = null,
  @CALC_RESULTS_XML_2 TEXT = null,
  @CALC_RESULTS_XML_3 TEXT = null
  
    
)

AS
BEGIN


 
  UPDATE Link_repeater 
  SET 
     
    CALC_RESULTS_XML_1 = COALESCE(@CALC_RESULTS_XML_1, CALC_RESULTS_XML_1),
    CALC_RESULTS_XML_2 = COALESCE(@CALC_RESULTS_XML_2, CALC_RESULTS_XML_2),
    CALC_RESULTS_XML_3 = COALESCE(@CALC_RESULTS_XML_3, CALC_RESULTS_XML_3)
             
  WHERE id = @ID

 
    

  RETURN @@ROWCOUNT


END
go
if  COL_LENGTH('dbo.link','SDB_bitrate_priority')  is null
  ALTER TABLE dbo.link
     ADD SDB_bitrate_priority float
GO

if  COL_LENGTH('dbo.link','SDB_bitrate_no_priority')  is null
  ALTER TABLE dbo.link
     ADD SDB_bitrate_no_priority float
GO



IF OBJECT_ID(N'dbo.sp_Link_rename_linkends_UPD') IS  not  NULL
  DROP PROCEDURE [dbo].[sp_Link_rename_linkends_UPD]
GO


IF OBJECT_ID(N'[dbo].[view_LinkEnd]') IS NOT NULL

DROP VIEW [dbo].[view_LinkEnd]
GO


/* task -> rename




*/
CREATE VIEW dbo.view_LinkEnd
AS
SELECT   
  L.*, 
  dbo.LinkEndType.name AS LinkEndType_name, 
  dbo.Property.name AS  property_name, 

  dbo.Property.project_id , 
   

  
  dbo.Property.lat AS lat,      
  dbo.Property.lon AS lon, 
  
        dbo.Property.ground_height AS ground_height,
        dbo.Property.guid AS [Property_guid], 
                      
                      
        dbo.Passive_Component.name AS [Passive_Component_name], 



        dbo.fn_PickList_GetValueByCode('linkend', 'equipment_type', L.equipment_type) AS equipment_type_name, 
        dbo.fn_PickList_GetValueByCode('linkend',    'kratnostBySpace', L.kratnostBySpace) AS     kratnostBySpace_name, 
        dbo.fn_PickList_GetValueByCode('linkend', 'kratnostByFreq'      , L.kratnostByFreq) AS kratnostByFreq_name, 
        dbo.fn_PickList_GetValueByCode('linkend', 'redundancy',      L.redundancy) AS redundancy_name, 
      
  
--           dbo.fn_LinkEnd_Get_Link_Name(L.id) AS Link_name, 

--        dbo.fn_LinkEnd_Get_Link_Calc_method(L.id) AS     Link_calc_method_id,       
--    dbo.fn_LinkEnd_Get_Link_ID(L.id) AS Link_id, 
        
        link.calc_method AS Link_calc_method_id,       
--        link.calc_method_name AS Link_calc_method_name,
        link.Link_name,       
        link.Link_id,       
        
        dbo.Property.georegion_id AS georegion_id,
                      
                      
                      
dbo.fn_dBm_to_Wt(L.power_dBm)      AS power_Wt, 
                      

      ant.diameter  AS antenna_diameter,
      ant.height  AS antenna_height,
      ant.polarization  AS antenna_polarization,

/*
dbo.fn_Linkend_Antenna_GetValue(L.id, 'diameter')      AS antenna_diameter, 
dbo.fn_Linkend_Antenna_GetValue(L.id, 'height')        AS antenna_height, 
dbo.fn_Linkend_Antenna_GetValue(L.id, 'polarization')  AS antenna_polarization,

*/

LinkEndType_mode.bandwidth                      


                      
                      
FROM         dbo.LinkEnd L LEFT OUTER JOIN
          dbo.Passive_Component ON L.passive_element_id = dbo.Passive_Component.id LEFT OUTER JOIN
          dbo.LinkEndType_mode ON L.linkendtype_mode_id = dbo.LinkEndType_mode.id LEFT OUTER JOIN
          dbo.LinkEndType ON L.linkendtype_id = dbo.LinkEndType.id LEFT OUTER JOIN
          dbo.Property ON L.property_id = dbo.Property.id
                      
                      
          OUTER apply fn_Linkend_Link_info(L.id) link
          OUTER apply fn_Linkend_Antenna_info (L.id) ant
          
          
       --   where
       
      /* 
   dbo.lib_vendor.name AS Vendor_name, 
   dbo.lib_VENDOR_Equipment.name AS Vendor_Equipment_name 
        
FROM 
   dbo.Linkend
   LEFT OUTER JOIN  dbo.LinkEndType on Linkend.LinkEndType_id = dbo.LinkEndType.id
   LEFT OUTER JOIN
          dbo.lib_VENDOR_Equipment ON dbo.LinkEndType.vendor_equipment_id = dbo.lib_VENDOR_Equipment.id LEFT OUTER JOIN
          dbo.lib_vendor ON dbo.LinkEndType.vendor_id = dbo.lib_vendor.id
       */
go

--------------------------------------------------------



/* ---------------------------------------------------------------
created: Alex  
used in:  

description
  linkend1_name=@property1_name +'->'+ @property2_name;
  linkend2_name=@property2_name +'->'+ @property1_name;
  link_name=@property1_name +'<->'+ @property2_name;

--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Link_rename_linkends_UPD
(
  @ID   int = 10762
)

AS
BEGIN
  ---------------------------------------------------
  IF @ID IS NULL  RAISERROR ('@ID is NULL', 16, 1);
  ---------------------------------------------------

  DECLARE 
 
    @linkend1_id int,
    @linkend2_id int,


    @LINKEND1_name nVARCHAR(100),
    @LINKEND2_name nVARCHAR(100),
          
    @property1_ID int,
    @property2_ID int,

    @property1_name nVARCHAR(100),
    @property2_name nVARCHAR(100)
     ;
  	

    
  SELECT 
   --    @project_ID  = project_ID,
       @LINKEND1_ID = LINKEND1_ID,
       @LINKEND2_ID = LINKEND2_ID,

       @property1_id = property1_id,
       @property2_id = property2_id,

       @property1_name = [property1_name],
       @property2_name = [property2_name]

    FROM VIEW_Link
    WHERE (id = @ID);
    
   
   

  set @linkend1_name=@property1_name +'->'+ @property2_name;
  set @linkend2_name=@property2_name +'->'+ @property1_name;
 -- set @link_name=@property1_name +'<->'+ @property2_name;



  exec sp_Object_GetNewName_new @OBJNAME='LinkEnd', @NAME=@LINKEND1_name, 
    @property_ID=@property1_id, @RESULT=@LINKEND1_name out


  exec sp_Object_GetNewName_new @OBJNAME='LinkEnd', @NAME=@LINKEND2_name, 
    @property_ID=@property2_id, @RESULT=@LINKEND2_name out


  UPDATE Linkend SET name = @LINKEND1_name WHERE (id = @linkend1_id) 
  UPDATE Linkend SET name = @LINKEND2_name WHERE (id = @linkend2_id) 



  RETURN @@ROWCOUNT 

 
END

go



-----------------------------------

IF OBJECT_ID(N'[dbo].[view_LinkEnd_with_project_id]') IS NOT NULL
  DROP VIEW dbo.view_LinkEnd_with_project_id
GO

CREATE VIEW dbo.view_LinkEnd_with_project_id
AS
SELECT dbo.LinkEnd.*,
       dbo.Property.project_id
FROM dbo.LinkEnd
     LEFT OUTER JOIN dbo.Property ON dbo.LinkEnd.property_id =  dbo.Property.id
     
where 
  LinkEnd.ObjName is null or      
  LinkEnd.ObjName = 'linkend'

GO
------------------------------------------------------


IF OBJECT_ID(N'[dbo].[view_Linkend_Antenna_with_project_id]') IS NOT NULL
  DROP VIEW dbo.view_Linkend_Antenna_with_project_id 
GO

CREATE VIEW dbo.view_Linkend_Antenna_with_project_id 
AS
SELECT dbo.Linkend_Antenna.*,
       dbo.Property.project_id AS project_id
FROM dbo.Linkend_Antenna
     LEFT OUTER JOIN dbo.Property ON dbo.Linkend_Antenna.property_id =
     dbo.Property.id

go

-----------------------------------------------------------------------

IF OBJECT_ID(N'[dbo].[view_LinkEnd_antennas]') IS NOT NULL

DROP VIEW [dbo].[view_LinkEnd_antennas]
GO



CREATE VIEW dbo.view_LinkEnd_antennas
AS
SELECT
       dbo.Linkend_Antenna.*,

       dbo.LinkEnd.guid AS [LinkEnd_guid],

       dbo.LinkEnd.pmp_site_id, -- AS [LinkEnd_guid],

       
       dbo.AntennaType.fronttobackratio,
       dbo.AntennaType.polarization_ratio,
       dbo.AntennaType.name AS antennaType_name,
             
          
       dbo.Property.georegion_id AS georegion_id,
       dbo.Property.project_id,
       
       dbo.Property.lat AS Property_lat,
       dbo.Property.lon AS Property_lon,
       
       
       IIF (IsNull(location_type,0) = 0, Property.lat,  Linkend_Antenna.lat) as map_lat,
       IIF (IsNull(location_type,0) = 0, Property.lon,  Linkend_Antenna.lon) as map_lon
       
       
/*       case IsNull(location_type,0)
         when 0 then Property.lat 
         else Linkend_Antenna.lat
       end AS map_lat,
        
       case IsNull(location_type,0)
         when 0 then Property.lon
         else Linkend_Antenna.lon
       end AS map_lon
*/
       
       
FROM dbo.Linkend_Antenna  
     LEFT OUTER JOIN dbo.Property ON dbo.Linkend_Antenna.property_id =
     dbo.Property.id
     LEFT OUTER JOIN dbo.AntennaType ON dbo.Linkend_Antenna.antennaType_id =
     dbo.AntennaType.id
     LEFT OUTER JOIN dbo.LinkEnd ON dbo.Linkend_Antenna.linkend_id =
     dbo.LinkEnd.id

go




IF OBJECT_ID(N'[dbo].[VIEW_link_location]') IS NOT NULL
  DROP VIEW [dbo].[VIEW_link_location]
GO


CREATE VIEW dbo.VIEW_link_location
AS
SELECT dbo.Link.id,
       dbo.Link.objname,

       dbo.Link.name,


--property1_ID,
--property2_ID,

--       P1.id AS property1_ID,
--       P2.id AS property2_ID,

      
       A1.lat AS lat1,
       A1.lon AS lon1,
       A2.lat AS lat2,
       A2.lon AS lon2, 
  
    
/*       P1.lat_wgs AS lat_wgs1,
       P1.lon_wgs AS lon_wgs1,
       P2.lat_wgs AS lat_wgs2,
       P2.lon_wgs AS lon_wgs2, 
*/

      
       Link_Repeater.id as  Link_Repeater_id,
      
       Property_repeater.ID AS link_repeater_Property_ID,
       
       Property_repeater.lat AS repeater_lat,
       Property_repeater.lon AS repeater_lon,
      

       dbo.Link.project_id,
 
       Link.LINKEND1_ID,
       Link.LINKEND2_ID,
 
--      Link.pmp_terminal_id,  
 

 
      -- dbo.Link.has_repeater,
       
       CHECKSUM(A1.lat + A2.lat, A1.lon + A2.lon) AS CHECKSUM
       
FROM dbo.Link 
     
    
     LEFT OUTER JOIN dbo.Link_Repeater ON dbo.Link.id =   dbo.Link_Repeater.Link_ID 
     LEFT OUTER JOIN dbo.Property Property_repeater   ON Property_repeater.id =  dbo.Link_Repeater.property_id 
     
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND1_ID) A1
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND2_ID) A2
     

    -- where Link.id= 159650
    /*
           
    
    
SELECT        dbo.Link.id, dbo.Link.objname, dbo.Link.name, dbo.Link.Property1_id, dbo.Link.Property2_id, dbo.Link_Repeater.id AS Link_Repeater_id, Property_repeater.id AS link_repeater_Property_ID, 
                         Property_repeater.lat AS repeater_lat, Property_repeater.lon AS repeater_lon, dbo.Link.project_id, dbo.Link.linkend1_id, dbo.Link.linkend2_id, dbo.Link.pmp_terminal_id
FROM            dbo.Link LEFT OUTER JOIN
                         dbo.Link_Repeater ON dbo.Link.id = dbo.Link_Repeater.Link_ID LEFT OUTER JOIN
                         dbo.Property AS Property_repeater ON Property_repeater.id = dbo.Link_Repeater.property_id    
    
    
    
FROM dbo.Property P1
     RIGHT OUTER JOIN dbo.Property P2
     
     RIGHT OUTER JOIN dbo.Property Property_repeater
     RIGHT OUTER JOIN dbo.Link
     LEFT OUTER JOIN dbo.Link_Repeater ON dbo.Link.id =
     dbo.Link_Repeater.Link_ID ON Property_repeater.id =
     dbo.Link_Repeater.property_id ON P2.id = dbo.Link.Property2_id ON P1.id =   dbo.Link.Property1_id
     
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND1_ID) A1
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND2_ID) A2
     */

go

--ALTER TABLE dbo.Link  
--   alter column length float not NULL
--GO



-----------------------------------------

IF OBJECT_ID(N'[dbo].[sp_Linkend_Antenna_GetNewName]') IS not  NULL
  DROP PROCEDURE [dbo].[sp_Linkend_Antenna_GetNewName]
GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Linkend_Antenna_GetNewName
(
  @OBJNAME    VARCHAR(50),  
  @ID INT   
)
AS
BEGIN
  declare 
    @new_name 	VARCHAR(50),
    @i 					int;


  if @OBJNAME = ''   RAISERROR('@OBJNAME = ''''', 16, 1)
 

  SET @i=0
  
  ---------------------------------------------
  IF @OBJNAME='template_Linkend'
  ---------------------------------------------  
    WHILE 1=1
    BEGIN
      SET @i=@i+1
--      SET @new_name = '�������' + CONVERT(VARCHAR(255), @i)
      SET @new_name = 'Antenna' + CONVERT(VARCHAR(255), @i)
       
      IF not EXISTS(SELECT * FROM template_Linkend_Antenna  
                    WHERE (template_LinkEnd_id=@ID) and (name=@new_name))
        BREAK;        
    END 
  ELSE    


  ---------------------------------------------
  IF @OBJNAME in ('Linkend', 'pmp_sector', 'pmp_terminal')
  ---------------------------------------------  
    WHILE 1=1
    BEGIN
      SET @i=@i+1
--      SET @new_name = '�������' + CONVERT(VARCHAR(255), @i)
      SET @new_name = 'Antenna' + CONVERT(VARCHAR(255), @i)
       
      IF not EXISTS(SELECT * FROM Linkend_Antenna  WHERE (LinkEnd_id=@ID) and (name=@new_name))
        BREAK;        
    END 
  ELSE  

/*  ---------------------------------------------
  IF @OBJNAME='pmp_sector'
  ---------------------------------------------  
    WHILE 1=1
    BEGIN
      SET @i=@i+1
--      SET @new_name = '�������' + CONVERT(VARCHAR(255), @i)
      SET @new_name = 'Antenna' + CONVERT(VARCHAR(255), @i)
       
      IF not EXISTS(SELECT * FROM Linkend_Antenna  WHERE (pmp_sector_id=@ID) and (name=@new_name))
        BREAK;        
    END 
  ELSE  */
  
/*
  ---------------------------------------------
  IF @OBJNAME='repeater'
  ---------------------------------------------  
    WHILE 1=1
    BEGIN
      SET @i=@i+1
      SET @new_name = '�������' + CONVERT(VARCHAR(255), @i)
       
      IF not EXISTS(SELECT * FROM Linkend_Antenna  WHERE (repeater_id=@ID) and (name=@new_name))
        BREAK;        
    END 
  ELSE  */
/*
  ---------------------------------------------
  IF @OBJNAME='pmp_terminal'
  ---------------------------------------------  
    WHILE 1=1
    BEGIN
      SET @i=@i+1
      SET @new_name = 'Antenna' + CONVERT(VARCHAR(255), @i)
       
      IF not EXISTS(SELECT * FROM Linkend_Antenna  WHERE (pmp_terminal_id=@ID) and (name=@new_name))
        BREAK;        
    END
  
  ELSE 
  
    */
     
  
     RAISERROR('@OBJNAME = ''''', 16, 1)  
 

  SELECT @new_name 

END

go





-----------------------------------------------------------------------

IF OBJECT_ID(N'[dbo].[VIEW_link_location]') IS not  NULL
  DROP view [dbo].[VIEW_link_location]
GO



CREATE VIEW dbo.VIEW_link_location
AS
SELECT dbo.Link.id,
       dbo.Link.objname,

       dbo.Link.name,


--property1_ID,
--property2_ID,

--       P1.id AS property1_ID,
--       P2.id AS property2_ID,

      
       A1.lat AS lat1,
       A1.lon AS lon1,
       A2.lat AS lat2,
       A2.lon AS lon2, 
  
    
/*       P1.lat_wgs AS lat_wgs1,
       P1.lon_wgs AS lon_wgs1,
       P2.lat_wgs AS lat_wgs2,
       P2.lon_wgs AS lon_wgs2, 
*/

      
       Link_Repeater.id as  Link_Repeater_id,
      
       Property_repeater.ID AS link_repeater_Property_ID,
       
       Property_repeater.lat AS repeater_lat,
       Property_repeater.lon AS repeater_lon,
      

       dbo.Link.project_id,
 
       Link.LINKEND1_ID,
       Link.LINKEND2_ID,
 
--      Link.pmp_terminal_id,  
 

 
      -- dbo.Link.has_repeater,
       
       CHECKSUM(A1.lat + A2.lat, A1.lon + A2.lon) AS CHECKSUM
       
FROM dbo.Link 
     
    
     LEFT OUTER JOIN dbo.Link_Repeater ON dbo.Link.id =   dbo.Link_Repeater.Link_ID 
     LEFT OUTER JOIN dbo.Property Property_repeater   ON Property_repeater.id =  dbo.Link_Repeater.property_id 
     
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND1_ID) A1
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND2_ID) A2
     

go
   

------------------------------------------



IF OBJECT_ID(N'[dbo].[sp_Linkend_Antenna_Copy_for_LINKEND]') IS not  NULL
  DROP procedure [dbo].[sp_Linkend_Antenna_Copy_for_LINKEND]
GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
error codes: 
	-1 - record exists
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Linkend_Antenna_Copy_for_LINKEND
(
  @SOURCE_LINKEND_ID  int,
  @DEST_LINKEND_ID    int 
   
)
AS
BEGIN
  DECLARE
    @new_id int,
    @Linkend_id int;
    
 
  SELECT name
      INTO #temp   
    FROM Linkend_Antenna 
    where 
      Linkend_id=@DEST_LINKEND_ID
     
  
   Insert Into Linkend_Antenna 
     (
        name,                        
     --   PROJECT_ID,  
                    
        Linkend_id,          
        property_id,
                    
        ANTENNATYPE_ID,
                    
        BAND,		          
        FREQ_MHz,	          
                               
        LOSS,	          
        GAIN,		          
        DIAMETER,	          
        VERT_WIDTH,          
        HORZ_WIDTH,
                                      
        POLARIZATION_STR,
        HEIGHT,
        AZIMUTH,
        TILT,
                    
        LAT,
        LON,
        LOCATION_TYPE                        
                   
       ) 
    SELECT

        NAME,         
    --    PROJECT_ID, 
                               
        @DEST_LINKEND_ID,            
        property_id,
                    
        ANTENNATYPE_ID,
        BAND,		          
        FREQ_MHz,	          
                               
        LOSS,	          
        GAIN,		          
        DIAMETER,	          
        VERT_WIDTH,          
        HORZ_WIDTH,
                   
        POLARIZATION_STR,
        HEIGHT,
        AZIMUTH,
        TILT,
                              
        LAT,
        LON,
        LOCATION_TYPE
                           
 FROM Linkend_Antenna                 
   where 
      (Linkend_id=@SOURCE_LINKEND_ID) and 
      (name NOT IN (SELECT name FROM #temp))

    
   
  RETURN IDENT_CURRENT ('Linkend_Antenna')   
       
 -- RETURN @@ROWCOUNT    
    

END

go


---------------------------------------------------------------

IF OBJECT_ID(N'[dbo].[sp_LinkEnd_Copy]') IS not  NULL
  DROP procedure [dbo].[sp_LinkEnd_Copy]
GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE procedure dbo.sp_LinkEnd_Copy
( 
  @ID 	int,
  @NAME varchar(250)
)
AS
begin
  DECLARE    
    @new_ID 	 int,
  --  @project_id  int,
    @PROPERTY_ID int;

/*
 select top 1 @id =id FROM linkend     
set @NAME='gdfgdfgdfgd'
*/


  SELECT @PROPERTY_ID=PROPERTY_ID
        -- @project_id=PROJECT_ID
    FROM Linkend
    WHERE id=@ID;
    


  INSERT INTO LinkEnd
   (--PROJECT_ID, 
    NAME, 
    PROPERTY_ID
   )
  VALUES
   (--@PROJECT_ID, 
    @NAME, 	
    @PROPERTY_ID
   ) 
 
  
  set @new_id=IDENT_CURRENT ('Linkend')       
 -- set @new_ID = @@IDENTITY
 

  exec sp_Linkend_Antenna_Copy_for_LINKEND @SOURCE_LINKEND_ID=@ID,  @DEST_LINKEND_ID=@new_ID   


  exec sys_sp_Copy_table_record  
     @TABLE_NAME = 'LinkEnd', 
     @ID=@ID,
     @NEW_ID =@NEW_ID


  return @new_ID
     
  
end

go

IF OBJECT_ID(N'[dbo].[sp_LinkEnd_Add]') IS not  NULL
  DROP procedure [dbo].[sp_LinkEnd_Add]
GO



/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE procedure dbo.sp_LinkEnd_Add
( 
  @OBJNAME 			VARCHAR(20)=null,
     
 -- @Template_site_ID int =NULL,
  
 -- @PROJECT_ID INT,

  @NAME 			varchar(250),
  @PROPERTY_ID 	    int,

--  @EQUIPMENT_TYPE int =1,   -- 1- ��������� ������������

  @LINKENDTYPE_ID		int =NULL,
  @LINKENDTYPE_MODE_ID  int =null, 


  @CHANNEL_TYPE VARCHAR(4)=null,


  @Rx_Level_dBm			float = null, --������� ������� �� ����� ���������


  @PASSIVE_ELEMENT_ID   int =null, 
  @PASSIVE_ELEMENT_LOSS float = null,


  -----------------------------
  -- custom equipment
  -----------------------------
  @BAND 					VARCHAR(10) = null,

 -- @POWER_MAX 	float = null,
  @Tx_Freq_MHz      		float = null, 
  @POWER_dBm 				float = 25,    
--  @POWER_LOSS float = null,

  @MODULATION_TYPE  		VARCHAR(50) = '32QAM', 
  @SIGNATURE_HEIGHT_dB  	float = 14,   --������ ���������
  @SIGNATURE_WIDTH_MHz  	float = 24,   --������ ���������
  @THRESHOLD_BER_3  		float = -80, 
  @THRESHOLD_BER_6  		float = -73, 

 -- @EQUALISER_PROFIT  		float = null,
  @Freq_Spacing_MHz  		float = null,       

  @Loss          		float = null,
  @KNG            			float = null,
  @Bitrate_Mbps 			float=NULL, --float


 -----------------------------
  -- PMP Sector
  -----------------------------
--  @PMP_SITE_ID int= null,
 -- @PMP_Sector_ID int= null,
  @PMP_Site_ID  int,

  @Cell_Layer_ID int = null,
  @Calc_Model_ID int = null,
  @Calc_Radius_km float= null,


  -----------------------------
  -- PMP terminal 
  -----------------------------
--  @PMP_SITE_ID int= null,
  @Connected_PMP_Sector_ID int= null,


  @COMMENTS varchar(250)=null

 
--  @IS_GET_INFO bit = 0

)
AS
begin

  SET ANSI_WARNINGS OFF --String or binary data would be truncated.

--return


  ---------------------------------------------
--  IF IsNull(@PROPERTY_ID,0)=0  RAISERROR ('@PROPERTY_ID is NULL', 16, 1);
  IF @OBJNAME = 'pmp_sector' and IsNull(@Pmp_site_ID ,0) = 0
     RAISERROR ('@Pmp_site_ID  NULL', 16, 1);
 -- IF @project_id IS NULL  RAISERROR ('@project_id is NULL', 16, 1);


  IF @Name IS NULL  RAISERROR ('@Name is NULL', 16, 1);

  
  ---------------------------------------------

  DECLARE 
    @EQUIPMENT_TYPE int,
    
  --  @project1_id 	int,
 --   @sName			VARCHAR(200),
    @s 				VARCHAR(100), 
    @id 		    int, 
    @new_ID 		int 
     
   


  ---------------------------------
  -- PMP sector
  ---------------------------------
  if IsNull(@Pmp_site_ID ,0) > 0
     SELECT 
      @PROPERTY_ID=PROPERTY_ID  
    FROM  Pmp_site  
      WHERE id = @Pmp_site_ID 
  
  
   
  
  
  IF not EXISTS(SELECT * FROM Property WHERE (ID=@Property_ID) )
    RAISERROR ('Property not EXISTS', 16, 1);
  
  
--  IF not EXISTS(SELECT * FROM project WHERE (ID=@project_ID) )
 --   RAISERROR ('project not EXISTS', 16, 1);
  
  
  
set @name = link.fn_LinkEnd_name (@PROPERTY_ID, @NAME) 
 
 
/*  SELECT top 1 @id=id FROM LinkEnd  WHERE (PROPERTY_ID=@PROPERTY_ID) and (name=@NAME)
  
  IF @id is not NULL
  BEGIN
    SELECT 'record already exists' as msg,  @id as ID     
    RETURN -1 * @id;  
  END  
*/  
  
  

 -- SELECT @project1_id=project_id FROM PROPERTY WHERE (ID=@PROPERTY_ID);
  
  
--  IF @project1_id IS NULL  RAISERROR ('@PROPERTY_ID - @project1_id is NULL', 16, 1);  
--  IF @project_id <> @project1_id  RAISERROR ('@project_id <> @project1_id', 16, 1);


  

  if @LINKENDTYPE_ID=0       SET @LINKENDTYPE_ID=NULL
  if @LINKENDTYPE_MODE_ID=0  SET @LINKENDTYPE_MODE_ID=NULL
    
  if ISNULL(@SIGNATURE_HEIGHT_dB,0)=0  SET @SIGNATURE_HEIGHT_dB=14 --������ ���������
  if ISNULL(@SIGNATURE_WIDTH_MHz,0)=0  SET @SIGNATURE_WIDTH_MHz=24 --������ ���������
  
  
  
    
  
  
 --print @sName



 --set @PROJECT_ID = dbo.fn_Property_GetProjectID(@PROPERTY_ID);
 
 
 --print @PROJECT_ID
 
 
 INSERT INTO LinkEnd
   (
    NAME, 
    PROPERTY_ID,    
    OBJNAME     
   )
  VALUES
   (
    @NAME, 
    @PROPERTY_ID,
    @OBJNAME 
   ) 
   
 -- set @new_ID = @@IDENTITY 
 set @new_id=IDENT_CURRENT ('LinkEnd')
 

print @new_ID


 
Alter table  LinkEnd Disable trigger all

   
  
  
--    SET @sName=@NAME

 
 
  IF ISNULL(@LINKENDTYPE_ID,0) > 0 
  begin
    set @EQUIPMENT_TYPE =0
    set @Tx_Freq_MHz = dbo.fn_LinkEndType_GetAveFreq_MHz (@LINKENDTYPE_ID)
  END
  ELSE BEGIN
    set @EQUIPMENT_TYPE =1    
    
    if Isnull(@Tx_Freq_MHz,0)=0
      set @Tx_Freq_MHz = dbo.fn_Band_GetAveFreq_MHz (@BAND) 
  END


 

 -- set @new_id=IDENT_CURRENT ('LinkEnd')
  

  
  UPDATE LinkEnd 
  set 
     Rx_Level_dBm       = @Rx_Level_dBm,
     
     PASSIVE_ELEMENT_ID = @PASSIVE_ELEMENT_ID,
     
     CHANNEL_TYPE =  @CHANNEL_TYPE,
     
     equipment_type     = @equipment_type,
        
     COMMENTS = @COMMENTS,
     
 
--  UPDATE PMP_Sector 
--   set 
  --  equipment_type = @equipment_type,
        
 --   LINKENDTYPE_ID     =@LINKENDTYPE_ID,
 --   LINKENDTYPE_MODE_ID=@LINKENDTYPE_MODE_ID,
        
   -- rx_freq_MHz = @TX_FREQ_MHZ,
  --  tx_freq_MHz = @TX_FREQ_MHZ,      	     	       
        
    --PMP sector
    Calc_Model_ID = COALESCE( @Calc_Model_ID,  Calc_Model_ID),
    Cell_Layer_ID = COALESCE( @Cell_Layer_ID,  Cell_Layer_ID),
    Calc_Radius_km= COALESCE( @Calc_Radius_km, Calc_Radius_km),
   
 	PMP_Site_ID = COALESCE( @PMP_Site_ID , PMP_Site_ID),
     
    --PMP terminal
    Connected_PMP_Sector_ID = COALESCE( @Connected_PMP_Sector_ID , Connected_PMP_Sector_ID) 



  WHERE    
    ID = @new_ID   

  

  
   
  if ISNULL(@LINKENDTYPE_ID,0) > 0
  
    exec sp_Object_Update_LinkEndType 
       @OBJNAME='LinkEnd', 
       @ID=@new_ID, 
       @LinkEndType_ID=@LinkEndType_ID, 
       @LinkEndType_mode_ID=@LinkEndType_mode_ID

   
  ELSE
    ------------------------
    -- custom equipment
    ------------------------      
    UPDATE linkend 
    set 
      BAND  = @BAND, 

      tx_freq_MHz = @TX_FREQ_MHZ,
      rx_freq_MHz = @TX_FREQ_MHZ,

      POWER_dBm   = @POWER_dBm,  

      SIGNATURE_HEIGHT =  COALESCE(@SIGNATURE_HEIGHT_dB, SIGNATURE_HEIGHT), 
      SIGNATURE_WIDTH  =  COALESCE(@SIGNATURE_width_MHz, SIGNATURE_width),
     
      MODULATION_TYPE  =  COALESCE(@MODULATION_TYPE, MODULATION_TYPE),
      THRESHOLD_BER_3  =  COALESCE(@THRESHOLD_BER_3, THRESHOLD_BER_3), 
      THRESHOLD_BER_6  =  COALESCE(@THRESHOLD_BER_6, THRESHOLD_BER_6),
      Bitrate_Mbps     =  COALESCE(@Bitrate_Mbps,    Bitrate_Mbps), 
        
      
      
      LOSS=@LOSS,         
      KNG = @KNG,
                 
      MODULATION_COUNT      = m.level_COUNT,
      GOST_53363_modulation = m.GOST_53363_modulation 
     
     from 
        (select top 1 * from Lib.Modulations 
         WHERE name=@MODULATION_TYPE) m     
        
    WHERE    
      linkend.ID = @new_ID
       
      
Alter table  LinkEnd enable trigger all
     

if @Connected_PMP_Sector_ID is not null
begin
  declare 
    @link_id int

  exec @link_id= dbo.sp_PMP_Terminal_Plug  @ID = @new_ID, @PMP_SECTOR_ID = @Connected_PMP_Sector_ID  

  IF @link_id is null
    RAISERROR ('procedure dbo.sp_LinkEnd_Add -  exec @link_id= dbo.sp_PMP_Terminal_Plug - @link_id is null', 16, 1);
  

end


/*   
CREATE PROCEDURE dbo.sp_PMP_Terminal_Plug
( 
  @ID 	    	  int,
  @PMP_SECTOR_ID  int
)
AS*/
       
  
  
--  if @IS_GET_INFO=1
  SELECT id,guid,name,PROPERTY_ID FROM LINKEND WHERE ID=@new_ID
  
  RETURN @new_ID;    

  


end

go

--------------------------------------------
IF OBJECT_ID(N'[dbo].[sp_Link_select_line_points]') IS not  NULL
  DROP procedure [dbo].[sp_Link_select_line_points]
GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Link_select_line_points
(
  @ID int
)
AS
BEGIN
 -- TEST
 -- SELECT top 1 @id= id FROM link 

  -------------------------------------------------------
--  IF @ID IS NULL  RAISERROR ('@ID is NULL', 16, 1);
  -------------------------------------------------------

--  IF EXISTS(SELECT * FROM LINK WHERE (id=@ID) and (linkend1_id>0))
    SELECT lat1,lon1,lat2,lon2 
      FROM view_Link
      WHERE (id=@ID)
  
/*  ELSE

  IF EXISTS(SELECT * FROM LINK WHERE (id=@ID) and (pmp_terminal_id>0))
    SELECT lat1,lon1,lat2,lon2 
      FROM view_PMP_Link
      WHERE (id=@ID)

*/
  RETURN @@ROWCOUNT
 

END

go
---------------------------------------------------------------




if  COL_LENGTH('dbo.Link_repeater','is_active')  is null
  ALTER TABLE dbo.Link_repeater
    ADD is_active int

go



if  COL_LENGTH('dbo.LinkEnd','XPIC_use_2_stvola_diff_pol')  is null
  ALTER TABLE dbo.Linkend
    ADD XPIC_use_2_stvola_diff_pol int

go


IF OBJECT_ID(N'[dbo].[view_Linkend_Antenna_for_calc]') IS not  NULL
  DROP view [dbo].[view_Linkend_Antenna_for_calc]
GO


CREATE VIEW dbo.view_Linkend_Antenna_for_calc
AS
SELECT
       dbo.AntennaType.band AS antennaType_band,
       dbo.Linkend_Antenna.*
       
FROM dbo.Linkend_Antenna
     LEFT OUTER JOIN dbo.AntennaType ON dbo.Linkend_Antenna.antennaType_id =  dbo.AntennaType.id

go



IF OBJECT_ID(N'[dbo].[sp_Link_Select_Item]') IS not  NULL
  DROP PROCEDURE [dbo].[sp_Link_Select_Item]
GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Link_Select_Item 
(
  @ID INT,
  @LINKEND_ID  int 
  
--  @PMP_SECTOR_ID  int =null, 
--  @PMP_TERMINAL_ID int =null 
  
)    
AS
BEGIN



  SET ANSI_WARNINGS OFF --String or binary data would be truncated.


  if @ID is null 
  begin
     set @ID = (select top 1 id from link)
     set @LINKEND_ID = 0

  end

  
   IF @LINKEND_ID=0  
     SET @LINKEND_ID=NULL
 
-- return
 


--  if @ID is null
--  BEGIN
--   SELECT top 1  @id =id  FROM Link   
--     WHERE pmp_sector_id>0
--   SELECT top 1  @id =id, @LINKEND_ID=LINKEND1_ID FROM Link   WHERE @LINKEND_ID>0

  
--    set @ID = 70365
 --   set @LINKEND_ID =124645
--  END

  
  declare    
--    @objname VARCHAR(50),
    
  --  @bOneItem BIT,
    
    @property_id  int,
    @property1_id int,
    @property2_id int,
    
    @parent_guid uniqueidentifier,
        
    @linkend1_id_ int,
    @linkend2_id_ int
--    @pmp_sector_id_   int,
--    @pmp_terminal_id_ int    
    ;
  	




  declare 
     @temp table
     
--  create table [#temp]
      (ID          int NOT NULL,              
       objname     VARCHAR(20) NOT NULL,

       guid        uniqueidentifier,
       parent_guid uniqueidentifier,
       
       name        nVARCHAR(100) NOT NULL,
               
       BAND        VARCHAR(50),
            
      EquipmentType_Name VARCHAR(100),
      
      ---------------
      --propery
      ---------------
       ADDRESS VARCHAR(200),
       
       lat   float,  
       lon   float, 
       

      ---------------
      --LinkEnd
      ---------------
       LinkEndType_ID 			int,      
       LinkEndType_Name 		nVARCHAR(100),
       LinkEndType_Mode_ID 	int,       
      
       MODULATION_TYPE VARCHAR(50),
       bitrate_Mbps    float,  
       
       POWER_dBm      Float,    
       THRESHOLD_BER_3 Float,
       THRESHOLD_BER_6 Float,       
       TX_FREQ_MHz    Float,
       RX_FREQ_MHz    Float,             

       LinkEnd_loss   Float,
  
      ---------------
      --ANTENNA
      ---------------
      AntennaType_ID      int, 
      ANTENNATYPE_NAME    nVARCHAR(100),

      DIAMETER            Float,
      HEIGHT              Float,
      HEIGHT_OLD          Float,      
      AZIMUTH             Float, 

      POLARIZATION_str    VARCHAR(4),
      GAIN                Float,
      TILT                Float,
      vert_width          Float,
      horz_width          Float,
      ANTENNA_loss     Float,
      
      location_type int,
      
      is_master 					bit --master antenna
               
      ) ;      

 
 
  SELECT 
 --      @objname = ISNULL(objname,'link'),

       @property1_id = linkend1.property_id,
       @property2_id = linkend2.property_id,
 
/*
       @property1_id = property1_id,
       @property2_id = property2_id,
*/
       @LINKEND1_ID_ = LINKEND1_ID,
       @LINKEND2_ID_ = LINKEND2_ID

--       @pmp_sector_id_  =pmp_sector_id,
--       @pmp_terminal_id_=pmp_terminal_id
              
    FROM link --view_Link_base
      left JOIN dbo.linkend linkend1 ON dbo.Link.linkend1_id = linkend1.id
      left JOIN dbo.linkend linkend2 ON dbo.Link.linkend2_id = linkend2.id
    
--    FROM Link
    WHERE (link.id = @ID)   


    
--------------------------------------------------
-- �������� ���� �������
--------------------------------------------------
  if (IsNull(@LINKEND_ID,0)>0)
--   and        
--     (IsNull(@LINKEND1_ID_,0)>0) and (IsNull(@LINKEND2_ID_,0)>0) 
  BEGIN
    if @LINKEND_ID = @LINKEND1_ID_
      set @property_id = @property1_id  ELSE 
      
    if @LINKEND_ID = @LINKEND2_ID_
      set @property_id = @property2_id
    ELSE 
      RETURN -1;

    set @property1_id = @property_id
    set @property2_id = @property_id
    set @LINKEND1_ID_ = @LINKEND_ID
    set @LINKEND2_ID_ = @LINKEND_ID
  END  



--------------------------------------------------
-- Property
--------------------------------------------------     
--  insert into [#temp]
  insert into @temp
    (id,guid,name,objname,
     ADDRESS, lat, lon
     )
   select 
     id,guid,name,'Property',
     ADDRESS, lat, lon
     
   from Property      
--   where id in (@property_id) 
   where id in (@property_id, @property1_id, @property2_id) 
   
--  SELECT * FROM #temp  
          
      
      
--------------------------------------------------
-- LinkEnd
--------------------------------------------------     

--    insert into [#temp]
    insert into @temp
      (id,guid,parent_guid,name,objname, 
       LinkEndType_ID,LinkEndType_Name,  EquipmentType_Name,
       LinkEndType_Mode_ID,     
       
       BAND,
       THRESHOLD_BER_3, THRESHOLD_BER_6, 
       POWER_dBm, 
       TX_FREQ_MHz, RX_FREQ_MHz,
            
       bitrate_Mbps,
       MODULATION_TYPE,
       
       LinkEnd_Loss     
       )
     select -- top 1
       id,guid,[Property_guid],name,'LinkEnd',
       LinkEndType_ID, LinkEndType_Name,  LinkEndType_Name,
       LinkEndType_Mode_ID,     

       BAND,
       THRESHOLD_BER_3, THRESHOLD_BER_6, 
       POWER_dBm, 
       TX_FREQ_MHz, RX_FREQ_MHz,
       
       bitrate_Mbps,
       MODULATION_TYPE,

       passive_element_loss
       
     from 
       view_LinkEnd      
--       view_LinkEnd_short      
     where 
        id in (@LINKEND_ID,  @LINKEND1_ID_, @LINKEND2_ID_)
--        id in (@LINKEND_ID)
      

--select * from @temp
--RETURN  

/*
select @LINKEND_ID,  @LINKEND1_ID_, @LINKEND2_ID_
--RETURN  

select * from view_LinkEnd  where id in (@LINKEND_ID,  @LINKEND1_ID_, @LINKEND2_ID_)

RETURN
*/ 

    --------------------
    -- PMP_sector
    --------------------
--    insert into [#temp]

    --------------------
    -- PMP TERMINAL
    --------------------
--    insert into [#temp]
 --  if (@pmp_TERMINAL_id is not null) or (@pmp_TERMINAL_id_ is not null)   



    set @LINKEND1_ID_ = IsNull(@LINKEND1_ID_,0)
    set @LINKEND2_ID_ = IsNull(@LINKEND2_ID_,0)

--    set @pmp_sector_id_   = IsNull(@pmp_sector_id_,0)
--    set @pmp_terminal_id_ = IsNull(@pmp_terminal_id_,0)

    set @LINKEND_ID = IsNull(@LINKEND_ID,0)
--    set @pmp_sector_id   = IsNull(@pmp_sector_id,0)
--    set @pmp_terminal_id = IsNull(@pmp_terminal_id,0)
    


--------------------------------------------------
-- view_LinkEnd_antennas
--------------------------------------------------    
--  insert into [#temp]
  insert into @temp
    (id,guid,parent_guid,name,
     objname,
    
     BAND,
     HEIGHT, HEIGHT_OLD, DIAMETER,AZIMUTH,GAIN,TILT,POLARIZATION_str,
     AntennaType_ID,AntennaType_NAME, EquipmentType_Name,
     vert_width,horz_width,
     ANTENNA_loss,
     is_master,
     
     location_type,
     lat, lon
    )    
   select 
     id,guid,[LinkEnd_guid],name,
     'LINKEND_antenna',
     
     BAND,
     HEIGHT, HEIGHT, DIAMETER,AZIMUTH,GAIN,TILT, POLARIZATION_str,     
     AntennaType_ID,AntennaType_NAME, AntennaType_NAME, 
     vert_width,horz_width,
     loss,
     is_master,

     location_type,
     lat, lon
     
   from view_LinkEnd_antennas 
   where
--        (LINKEND_id      in (@LINKEND_id) )
        (LINKEND_id      in (@LINKEND_id, @LINKEND1_id_, @LINKEND2_id_) )
        
        -- or 
        
    --    (pmp_sector_id   in (@pmp_sector_id,  @pmp_sector_id_) )   or 
     --   (pmp_terminal_id in (@pmp_terminal_id, @pmp_terminal_id_) )
        

--------------------------------------------------
-- view_pmp_sector_antennas
--------------------------------------------------    

 

--------------------------------------------------
-- view_pmp_terminal_antennas
--------------------------------------------------    

-- insert into [#temp]


--  SELECT * FROM #temp  
  SELECT * FROM @temp  
  
  
--  IF @@ROWCOUNT =0  RAISERROR ('@@ROWCOUNT =0', 16, 1);

  
  RETURN @@ROWCOUNT  
  
END


   go

/****** Object:  StoredProcedure [link_calc].[sp_Link_Calc_Reserve]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[link_calc].[sp_Link_Calc_Reserve]') IS not  NULL

DROP PROCEDURE [link_calc].[sp_Link_Calc_Reserve]
GO
/****** Object:  StoredProcedure [link_calc].[sp_Link_Calc_Reserve]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:  
errors:
  -1: 
  
������ ��������������� ������
  
--------------------------------------------------------------- */
CREATE PROCEDURE link_calc.sp_Link_Calc_Reserve
(
  @Template_Link_id INT,  

  @Property1_id INT,
  @Property2_id INT,  

  @LINKEND1_ID int,
  @LINKEND2_ID int ,
  
  @LAT1 FLOAT=NULL, 
  @LON1 FLOAT=NULL,
    
  @LAT2 FLOAT=NULL, 
  @LON2 FLOAT=NULL,
  
  @AntennaType_ID int=null,
  @LinkEndType_mode_ID int=null

)
AS
BEGIN
/*
 set  @Template_Link_id=1

 
 set @Property1_id =2077607
 set @Property2_id =2077608  

 set @LINKEND1_ID =null
 set @LINKEND2_ID =549866
*/

  declare
--    @Property1_id INT,
--    @Property2_id INT,  
  
/*    @lat1 float,
    @lon1 float,
    @lat2 float,
    @lon2 float,
*/  
   -- @AZIMUTH float, 
    @LENGTH_M float,   
   
    @gain1 float,
    @gain2 float,
    @power float,
    @THRESHOLD float,
    @THRESHOLD_BER_6 float,
    @THRESHOLD_BER_3 float,
   
    @FREQ_GHz float,
    @TX_FREQ_MHz float,
    @RX_FREQ_MHz float,
    @fade_margin float,
    @rx_level float,
    @L float
    ;
    

  -------------------------------------------------------
 -- IF @LENGTH_KM=0  RAISERROR ('@LENGTH_KM=0', 16, 1);
--  IF @FREQ_GHz=0   RAISERROR ('@FREQ_GHz=0', 16, 1);
  -------------------------------------------------------

  if IsNull(@Template_Link_id,0) =0 and
     IsNull(@LINKEND1_ID,0)>0  and  
     IsNull(@LINKEND2_ID,0)>0
  BEGIN
    select @property1_id=property_id  FROM LINKEND  WHERE id=@LINKEND1_ID
    select @property2_id=property_id  FROM LINKEND  WHERE id=@LINKEND2_ID
      

    SELECT top 1 @gain1=gain FROM Linkend_Antenna WHERE LINKEND_id=@LINKEND1_ID
    SELECT top 1 @gain2=gain FROM Linkend_Antenna WHERE LINKEND_id=@LINKEND2_ID



      SELECT top 1 
        @power 		=ISNULL(power_dBm,0),
        @THRESHOLD_BER_6=ISNULL(THRESHOLD_BER_6,0),
        @THRESHOLD_BER_3=ISNULL(THRESHOLD_BER_3,0),
        @TX_FREQ_MHz	=ISNULL(TX_FREQ_MHz,0),
        @RX_FREQ_MHz	=ISNULL(RX_FREQ_MHz,0)
      FROM Linkend 
      WHERE id=@LINKEND1_ID

  END


 -- select @lat1=lat, @lon1=lon FROM Property WHERE id=@property1_id
--  select @lat2=lat, @lon2=lon FROM Property WHERE id=@property2_id
  if @property1_id is not null
    set @LENGTH_M = geo.fn_Length_m_Between_Property (@property1_id,@property2_id)
  else
  if @lat1 is not null
    set @LENGTH_M = geo.fn_Length_m (@lat1, @lon1, @lat2, @lon2)


/*CREATE FUNCTION geo.fn_Length_m 

(
  @LAT1 float=64,
  @Lon1 float=37,

  @LAT2 float=64.1,
  @LON2 float=37.1 
 
)
*/

 declare
--    @AntennaType_id INT,
    @LinkEndType_id INT
 --   @LinkEndType_mode_id INT

  ---------------------------------------------------------------------------  
  if IsNull(@Template_Link_id,0)>0
  ---------------------------------------------------------------------------
  begin
    select 
           @LinkEndType_id = LinkEndType_id,
           @LinkEndType_mode_id = LinkEndType_mode_id,
           @AntennaType_id = AntennaType_id

      from lib.Template_Link
      where id = @Template_Link_id

    if @LinkEndType_mode_id is null 
      select top 1 @LinkEndType_mode_id = id 
        from LinkEndType_mode 
        WHERE LinkendType_id=@LinkendType_id
        order by bitrate_Mbps desc   


     SELECT top 1 
        @power 	=ISNULL(power_max,0),
        @THRESHOLD_BER_6=ISNULL(THRESHOLD_BER_6,0),
        @THRESHOLD_BER_3=ISNULL(THRESHOLD_BER_3,0)
  --      @TX_FREQ_MHz	=ISNULL(TX_FREQ_MHz,0),
  --      @RX_FREQ_MHz	=ISNULL(RX_FREQ_MHz,0)
      FROM LinkEndType_mode 
      WHERE id=@LinkEndType_mode_id

     SELECT top 1 
  --      @power_dBm 	=ISNULL(power_dBm,0),
  --      @THRESHOLD_BER_6=ISNULL(THRESHOLD_BER_6,0),
  --      @THRESHOLD_BER_3=ISNULL(THRESHOLD_BER_3,0),
        @TX_FREQ_MHz	=ISNULL(freq_min,0),
        @RX_FREQ_MHz	=ISNULL(freq_min,0)
      FROM LinkEndType
      WHERE id=@LinkEndType_id


    SELECT top 1 @gain1=gain, @gain2=gain  FROM AntennaType WHERE id=@AntennaType_id
    
  end

else
--=======================================================================
  if IsNull(@Template_Link_id,0)=0 and
     IsNull(@LinkEndType_mode_id,0)>0 and 
  	 IsNull(@AntennaType_id,0)>0 
  ---------------------------------------------------------------------------     
  begin    

     SELECT top 1 
        @power 	=ISNULL(power_max,0),
        @THRESHOLD_BER_6=ISNULL(THRESHOLD_BER_6,0),
        @THRESHOLD_BER_3=ISNULL(THRESHOLD_BER_3,0)
  --      @TX_FREQ_MHz	=ISNULL(TX_FREQ_MHz,0),
  --      @RX_FREQ_MHz	=ISNULL(RX_FREQ_MHz,0)
      FROM LinkEndType_mode 
      WHERE id=@LinkEndType_mode_id

     SELECT top 1 
  --      @power_dBm 	=ISNULL(power_dBm,0),
  --      @THRESHOLD_BER_6=ISNULL(THRESHOLD_BER_6,0),
  --      @THRESHOLD_BER_3=ISNULL(THRESHOLD_BER_3,0),
        @TX_FREQ_MHz	=ISNULL(freq_min,0),
        @RX_FREQ_MHz	=ISNULL(freq_min,0)
      FROM LinkEndType
      WHERE id=
              (select LinkEndType_id from LinkEndType_mode WHERE id=@LinkEndType_mode_id)


    SELECT top 1 @gain1=gain, @gain2=gain  FROM AntennaType WHERE id=@AntennaType_id
    
  end







  -------------------------------------------------------
  IF @gain1 IS NULL BEGIN    
     SELECT 'PPC1: �������� = 0'
     RETURN -1
  END
  
  IF @gain2 IS NULL BEGIN    
     SELECT 'PPC2: �������� = 0'
     RETURN -1
  END  
  
  -------------------------------------------------------



  set @FREQ_GHz = @TX_FREQ_MHz / 1000;
  if @FREQ_GHz=0  set @FREQ_GHz = @RX_FREQ_MHz / 1000;

  set @THRESHOLD = @THRESHOLD_BER_6;
  if @THRESHOLD=0  set @THRESHOLD = @THRESHOLD_BER_3;


  set @L = 41890 * @LENGTH_M/1000 * @FREQ_GHz;
--  set @L = 41890 * @LENGTH_KM * @FREQ_GHz;

  if @L >0
    set @L = 20 * LOG10(@L)
  ELSE
  BEGIN
    SELECT 'FREQ_GHz = 0'
    print  @FREQ_GHz
  --  print  @FREQ_GHz
  
   RETURN -1

  END;

  --�������� �� ����� ��������� dBm
  set @rx_level = @power + @gain1 + @gain2 - @L

  --����� �� ��������� dB
  set @fade_margin =@rx_level - @THRESHOLD

  SELECT @rx_level      as rx_level_dBm, 
   		 @fade_margin   as fade_margin_dB,
         @LENGTH_M/1000 as LENGTH_KM 
     --   @LENGTH_KM 


  
  RETURN @@ROWCOUNT

  

END


go

IF OBJECT_ID(N'[dbo].[sp_Link_Repeater_Add]') IS not  NULL
  DROP PROCEDURE [dbo].[sp_Link_Repeater_Add]
GO


--------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE procedure dbo.sp_Link_Repeater_Add
(    
  @NAME 			varchar(250),
  @PROPERTY_ID 	    int,


  @IS_ACTIVE BIT = 0,

  @ANTENNA1_AntennaType_ID  int=null,
  @ANTENNA2_AntennaType_ID  int=null,

  @ANTENNA1_HEIGHT 			float=null,
  @ANTENNA2_HEIGHT 			float=null,

  @ANTENNA1_gain 			float=null,
  @ANTENNA2_gain 			float=null
)
AS
begin
  ---------------------------------------------
  IF @PROPERTY_ID IS NULL  RAISERROR ('@PROPERTY_ID is NULL', 16, 1);
  IF @Name IS NULL  RAISERROR ('@Name is NULL', 16, 1);
  ---------------------------------------------

  DECLARE     
    @Antenna1_ID 	int,
    @Antenna2_ID 	int,
   ------ @PROJECT_ID int,
    
  --  @project_id 	int,
    @new_ID 		int 
        

-- set @PROJECT_ID = dbo.fn_Property_GetProjectID(@PROPERTY_ID)

 --IF @project_id IS NULL  RAISERROR ('@project_id is NULL', 16, 1);

   /*
  SELECT @PROJECT_ID = project_id 
    FROM Property 
    where id= @PROPERTY_ID*/
 
 
-- (SELECT project_id FROM	Property where Property.id= Link_Repeater.Property_id) 

  INSERT INTO [Link_Repeater]
   (
    NAME, 
    PROPERTY_ID ,
    IS_ACTIVE
   --- project_id    
   )
  VALUES
   (
    @NAME, 
    @PROPERTY_ID,
    @IS_ACTIVE 
    
  --  (SELECT project_id FROM	Property where Property.id= Link_Repeater.Property_id) 
    
 -----   @PROJECT_ID
   ) 
   
  set @new_id= IDENT_CURRENT ('Link_Repeater')   
     
   
  --set @new_ID = @@IDENTITY



  if @ANTENNA1_AntennaType_ID = 0
    set @ANTENNA1_AntennaType_ID = null


  if @ANTENNA2_AntennaType_ID = 0
    set @ANTENNA2_AntennaType_ID = null


  
--  IF (@ANTENNA1_AntennaType_ID IS NOT NULL) and 
--     (@ANTENNA2_AntennaType_ID IS NOT NULL) 
--  BEGIN
    exec @Antenna1_ID = sp_Link_Repeater_Antenna_Add
      @NAME 		    = 'Antenna1',
      @ANTENNATYPE_ID   = @ANTENNA1_AntennaType_ID,
      @HEIGHT           = @ANTENNA1_HEIGHT,
      @GAIN             = @ANTENNA1_GAIN,
      @Link_Repeater_ID = @new_ID,
    --  @DISPLAY_ORDER    = 1,
      @SLOT    = 1

print @Antenna1_ID
-- IF @Antenna1_ID IS NULL  RAISERROR ('@Antenna1_ID is NULL', 16, 1);


    exec @Antenna2_ID = sp_Link_Repeater_Antenna_Add
      @NAME 		    = 'Antenna2',
      @ANTENNATYPE_ID   = @ANTENNA2_AntennaType_ID,
      @HEIGHT           = @ANTENNA2_HEIGHT,
      @GAIN             = @ANTENNA2_GAIN,
      @Link_Repeater_ID = @new_ID,
    --  @DISPLAY_ORDER    = 2,
      @SLOT    = 2
      
      
      
print @Antenna2_ID
-- IF @Antenna2_ID IS NULL  RAISERROR ('@Antenna2_ID is NULL', 16, 1);
      
      
/*
    UPDATE Link_Repeater
    set    
        ANTENNA1_ID=@Antenna1_ID,  
        ANTENNA2_ID=@Antenna2_ID  
       
    WHERE id=@new_id    
    
    */    
 -- END
  	     
  

  IF @Antenna1_ID IS NULL or @Antenna2_ID IS NULL
  begin
    DELETE FROm  Link_Repeater WHERE id= @new_ID
    return -1
  end

   
  
 -- if @IS_GET_INFO=1
  
  SELECT @new_ID as id,
         @Antenna1_ID as Antenna1_ID,
         @Antenna2_ID as Antenna2_ID  
  
  RETURN @new_ID;      
  
  
end

 go



------------------------------------------------------------------------------------


IF OBJECT_ID(N'[dbo].[sp_Link_Update_Length_and_Azimuth]') IS not  NULL
  DROP procedure [dbo].[sp_Link_Update_Length_and_Azimuth]
GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
error:
  -1 : not exists
  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Link_Update_Length_and_Azimuth
(
  @ID   int = 20972
)

AS
BEGIN
  if @ID is null
    set @ID = 10661


--  DECLARE
  --   @AZIMUTH2 float;

--  set @AZIMUTH1=ROUND(@AZIMUTH1,1)
 -- set @AZIMUTH2=ROUND(@AZIMUTH2,1)
/*
  IF @AZIMUTH1<180
     set @AZIMUTH2= @AZIMUTH1 + 180
  ELSE
     set @AZIMUTH2= @AZIMUTH1 - 180
  
   */

  DECLARE  
    @result INT,
  
 --   @Property1_id INT,
 --   @Property2_id INT,
  --  @property_rep_ID int,
    
    @LINK_repeater_ID  int,
    
    @AZIMUTH_rep1 float,
    @AZIMUTH_rep2 float,    
    
    @objname varchar(10),
    
    @AZIMUTH1 float, 
    @AZIMUTH2 float,
     
    @LENGTH_M float, 
        
    @LENGTH1_M float,
    @LENGTH2_M float,    
        
    @lat_rep float,
    @lon_rep float,
    
 --   @has_repeater bit,

    @lat1 float,
    @lon1 float,
    @lat2 float,
    @lon2 float;

  DECLARE
     @ilinkend1_id int,
     @ilinkend2_id int
     
  --   @pmp_terminal_id int ;



-- IF @PROPERTY_ID IS NULL  RAISERROR ('@PROPERTY_ID is NULL', 16, 1);



  IF NOT EXISTS(SELECT * FROM Link WHERE (id=@ID))
    RAISERROR (' IF NOT EXISTS(SELECT * FROM Link WHERE (id=@ID))', 16, 1);
  --  RETURN -1


  	
  SELECT    
      @objname = objname,  
     
       @iLINKEND1_ID = LINKEND1_ID,
       @iLINKEND2_ID = LINKEND2_ID,
 --      @pmp_terminal_id = pmp_terminal_id,  
  
  
       @LAT1 = lat1,
       @LON1 = lon1,
         
       @LAT2 = lat2,
       @LON2 = lon2,
              
       @LINK_repeater_ID = link_repeater_ID,      
       @LAT_rep = repeater_lat,
       @LON_rep = repeater_lon
               
              
  FROM VIEW_link_location
     WHERE (id = @ID)   


  IF (@LAT1=0) or (@LAT2=0) or (@Lon1=0) or (@Lon2=0) 
    RAISERROR (' PROCEDURE dbo.sp_Link_Update_Length_and_Azimuth - (@LAT1=0) or (@LAT2=0) or (@Lon1=0) or (@Lon2=0)  ', 16, 1);




  -------------------------------------------
  IF @LINK_repeater_ID IS NOT NULL
  -------------------------------------------
  BEGIN
  

    
    set @AZIMUTH_rep1 = dbo.fn_AZIMUTH(@LAT_rep, @Lon_rep,  @LAT1, @LON1, null)
    set @AZIMUTH_rep2 = dbo.fn_AZIMUTH(@LAT_rep, @Lon_rep,  @LAT2, @LON2, null)
              
      
 --   return
    
          
    set @AZIMUTH1 = dbo.fn_AZIMUTH(@LAT1, @Lon1,  @LAT_rep, @Lon_rep, null)
    set @AZIMUTH2 = dbo.fn_AZIMUTH(@LAT2, @Lon2,  @LAT_rep, @Lon_rep, null)
           
           
    set @LENGTH1_M = dbo.fn_AZIMUTH(@LAT1, @Lon1, @LAT_rep, @Lon_rep, 'LENGTH')
    set @LENGTH2_M = dbo.fn_AZIMUTH(@LAT2, @Lon2, @LAT_rep, @Lon_rep, 'LENGTH')
    SET @LENGTH_M = @LENGTH1_M + @LENGTH2_M               
    
    
    UPDATE Link_Repeater_Antenna
      SET AZIMUTH = @AZIMUTH_rep1
      WHERE (Link_Repeater_id = @LINK_repeater_ID) and (slot = 1)
    
    UPDATE Link_Repeater_Antenna
      SET AZIMUTH = @AZIMUTH_rep2
      WHERE (Link_Repeater_id = @LINK_repeater_ID) and (slot = 2)

           
   --   select * from Link_Repeater_Antenna  WHERE (Link_Repeater_id = @LINK_repeater_ID) 
      
      
  
  END 
  -------------------------------------------
  ELSE BEGIN
  -------------------------------------------  
       
    set @AZIMUTH1 = dbo.fn_AZIMUTH(@LAT1, @Lon1,  @LAT2, @LON2, null)
    set @AZIMUTH2 = dbo.fn_AZIMUTH(@LAT2, @Lon2,  @LAT1, @LON1, null)
    set @LENGTH_M = dbo.fn_AZIMUTH(@LAT2, @Lon2,  @LAT1, @LON1, 'LENGTH')
 
  
--  select @AZIMUTH1, @AZIMUTH2, @LENGTH_M
  
/*
DECLARE @g geography = 'LINESTRING(-120 45, -120 0, -90 0)';   
SELECT @g.EnvelopeAngle();  
*/
  
  END


  UPDATE Link
    SET   
      LENGTH   = @LENGTH_M, 
      AZIMUTH1 = @AZIMUTH1, 
      AZIMUTH2 = @AZIMUTH2

    WHERE 
        id = @ID
  
  
  set @result = @@ROWCOUNT
  

--  if @iLINKEND1_ID IS NOT NULL
--  BEGIN
  if IsNull(@objname,'') <> 'pmp_link' 
    UPDATE Linkend_Antenna SET Azimuth = @AZIMUTH1 WHERE (LINKEND_id = @iLINKEND1_ID) 
  
  
--@iLINKEND2_ID 
  UPDATE Linkend_Antenna SET Azimuth = @AZIMUTH2 WHERE (LINKEND_id = @iLINKEND2_ID) 
--  END
  
/*  
  select @objname,  @AZIMUTH1, @AZIMUTH2, @LENGTH_M, @iLINKEND1_ID,  @iLINKEND2_ID


select * from Linkend_Antenna WHERE (LINKEND_id = @iLINKEND1_ID) 
select * from Linkend_Antenna WHERE (LINKEND_id = @iLINKEND2_ID) 


*/
  

  RETURN @result
  
END

go


----------------------------------------------------------
IF OBJECT_ID(N'dbo.sp_Task_Link_Optimize_equipment') IS not  NULL
  DROP PROCEDURE dbo.sp_Task_Link_Optimize_equipment
GO


---------------------------------------------------------
CREATE PROCEDURE dbo.sp_Task_Link_Optimize_equipment
---------------------------------------------------------
(
  @OBJNAME varchar(100),

  @BAND varchar(50), 

  
--  @TABLENAME varchar(100),
  
  @BitRate_Mbps int=null
 -- @BAND varchar(50)=null
)

AS
BEGIN
/*
  set @OBJNAME = 'antenna_type'
  set @TABLENAME = 'AntennaType' 

  set @BITRATE = 250;


  set @BITRATE = COALESCE(@BITRATE, 0);
*/


--set @OBJNAME = 'antenna_type'


/* DECLARE 
    @folders table (id int, 
    							  name varchar(100), 
                    path varchar(1000));
*/

--  INSERT INTO @folders (id, path) 
    SELECT 
           id , dbo.fn_Folder_FullPath(id) as folder_name
       INTO #temp
    
       FROM Folder WHERE [type]=@OBJNAME


--	SELECT * from #temp   
  
  
  
--	SELECT M.* , F.path from AntennaType M left join  @folders F on M.folder_id = F.id 



/*


------------------------------
if @OBJNAME = 'LinkEndType_mode'
------------------------------
  SELECT * FROM  LinkEndType_mode  WHERE  BITRATE_MBPS >= @BITRATE
*/



------------------------------
if @OBJNAME = 'antenna_type' or @OBJNAME = 'antenna' 
------------------------------
   SELECT  -- top 5 
        M.* , F.folder_name, cast(0 as bit) as checked 
     from -- AntennaType
      view_Task_Opti_Antennas

           M left join  #temp F on M.folder_id = F.id 
           
    WHERE diameter>0       
      and   
--        (band = COALESCE(@band , band) )
        (band = @band) 


--  IF @OBJNAME = 'antenna_type' 

------------------------------
ELSE
------------------------------

  SELECT  -- top 5 
  	  M.* , F.folder_name, cast(0 as bit) as checked 
    from LinkEndType M 
    left join  #temp F on M.folder_id = F.id
    
    WHERE
        (band = @band)
        and  (m.id in (SELECT LinkEndType_id FROM  LinkEndType_mode  WHERE  BITRATE_MBPS >= @BITRATE_MBPS))
      
--    where m.id in
 --      (select LinkEndType_id from LinkEndType_Mode   )


  return @@ROWCOUNT

-- where bitrate_mbps >  @BITRATE

  /* Procedure body */
END
go


----------------------------------------------------------
IF OBJECT_ID(N'dbo.view_Task_Opti_Antennas') IS not  NULL
  DROP VIEW dbo.view_Task_Opti_Antennas 
GO


CREATE VIEW dbo.view_Task_Opti_Antennas 
AS
  SELECT 
    AntennaType.id,
    AntennaType.folder_id,
    AntennaType.name,
    AntennaType.gain,
    AntennaType.freq,
    AntennaType.diameter,
    AntennaType.vert_width,
    AntennaType.horz_width,
    AntennaType.band,
    AntennaType.polarization_str
  FROM
    AntennaType
    
    where diameter>0

go

-------------------------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID(N'dbo.sp_Link_Select') IS not  NULL
  DROP PROCEDURE dbo.sp_Link_Select
GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
 -  ���������� ��� ��������� � ������� ������ VIEW
--------------------------------------------------------------- */
CREATE  PROCEDURE dbo.sp_Link_Select 
(
	@ID int = 10737
)

AS
BEGIN
 -- declare 
  --  @objname varchar(10);
    
  ---------------------------------------------
  
--  SELECT @objname = objname 
 --   FROM  Link
  --  WHERE  (id = @ID)
    
    
--  if ( @objname = 'link'  ) or ( @objname is null )
   
 SELECT * FROM view_link_report WHERE id=@ID
-- SELECT * FROM view_link WHERE id=@ID
    
    
--  else 
--    SELECT * FROM view_PMP_LINK WHERE id=@ID
  
  return @@rowcount
         
END
go

----------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID(N'[dbo].[sp_Link_Update_LinkEndType]') IS NOT NULL
  DROP procedure dbo.sp_Link_Update_LinkEndType
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Link_Update_LinkEndType 
(
--  @OBJNAME			varchar(50),
  @LINK_ID   	  	int, -- Template_Cell ID
  
  @LinkEndType_ID 		int,
  @LinkEndType_Mode_ID 	int--,  --= null
  
 -- @LinkEndType_mode_ID int=NULL
)
AS
BEGIN

/*
  set @OBJNAME	= 'pmp_terminal'
  set @ID   	= 498 
  
  set @LinkEndType_ID = 168
  set @LinkEndType_Mode_ID  = 1363 
*/


  ---------------------------------------------
--  IF @OBJNAME IS NULL  RAISERROR ('@OBJNAME is NULL', 16, 1);
  IF @LINK_ID        IS NULL  RAISERROR ('@LINK_ID is NULL', 16, 1);
  IF @LinkEndType_ID IS NULL  RAISERROR ('@LinkEndType_ID is NULL', 16, 1);
  ---------------------------------------------
  
 -- IF @Mode IS NULL  RAISERROR ('@Mode is NULL', 16, 1);


  DECLARE 
     @linkend1_id INT,
     @linkend2_id INT,
  
 	   @mode 			  int,
	   @band 			  varchar(20),
     @freq_MHz 		  float,
     @failure_period  float,
     @equaliser_profit	float;

  --   @iLinkEnd_Type_ID  int ,
    -- @POWER_MAX 		float, 
 
  SELECT 
       @band = Band,
  --     @POWER_MAX 			=  POWER_MAX
       @failure_period 	= failure_period,
       @equaliser_profit = equaliser_profit  
                
    FROM LinkEndType
    WHERE (id = @LinkEndType_ID)   
   
  
  set @freq_MHz = dbo.fn_LinkEndType_GetAveFreq_MHz(@LinkEndType_ID);


  SELECT @linkend1_id = LINKEND1_ID, 
    	   @linkend2_id = LINKEND2_ID
      FROM Link 
      WHERE (id = @LINK_ID)  


--        db_Par(FLD_EQUIPMENT_TYPE,      IIF(rec.LinkEndType_ID>0, DEF_TYPE_EQUIPMENT, DEF_NON_TYPE_EQUIPMENT)),
--  //������������: 0-�������, 1-���������
--  DEF_TYPE_EQUIPMENT     = 0;
--  DEF_NON_TYPE_EQUIPMENT = 1;


---------------------------------------------
 -- IF @OBJNAME='LinkEnd'
---------------------------------------------  
 -- BEGIN
  UPDATE LinkEnd SET
      EQUIPMENT_TYPE = 0,		    
          
      LinkEndType_ID	  = @LinkEndType_ID,
    --  LinkEndType_Mode_ID = @LinkEndType_Mode_ID,
--        Mode = @mode,

      BAND 			= @BAND,
      Tx_freq_MHz     = @freq_MHz,
      Rx_freq_MHz     = @freq_MHz,
      
--   Mode 			  = @Mode,
      

  --  POWER_MAX         = @POWER_MAX,
--    POWER_dBm         = @POWER_MAX,

 --   Tx_freq_MHz   	  = @freq_MHz,
--    Rx_freq_MHz   	  = @freq_MHz
          
    failure_period   = @failure_period,
    equaliser_profit = @equaliser_profit      
     
  WHERE (id in (@linkend1_id, @linkend2_id) )  

 -- END  
  

   exec sp_Object_Update_LinkEndType_Mode 
     @OBJNAME='LinkEnd', 
--     @ID=@LINK_ID, 
     @ID=@linkend1_id, 
   --  @LinkEndType_ID=@LinkEndType_ID, 
     @LinkEndType_mode_ID=@LinkEndType_mode_ID


   exec sp_Object_Update_LinkEndType_Mode 
     @OBJNAME='LinkEnd', 
--     @ID=@LINK_ID, 
     @ID=@linkend2_id, 
   --  @LinkEndType_ID=@LinkEndType_ID, 
     @LinkEndType_mode_ID=@LinkEndType_mode_ID



  RETURN @@ROWCOUNT   
 

  
 -- exec sp_LinkEnd_Update_Mode @ID=@ID, @Mode=@Mode --, @LinkEndType_mode_ID=@LinkEndType_mode_ID
 
END
/*

  ---------------------------------------------
  IF @Linkend_ID is not null
  ---------------------------------------------
  BEGIN
    UPDATE Linkend_Antenna SET 
  		    Gain            =m.Gain,

          Freq_MHz         =m.Freq,
--          Freq_MHz        =m.Freq_MHz,
          Diameter        =m.Diameter,
          Polarization_str=m.Polarization_str,
          Vert_Width      =m.Vert_Width,
          Horz_Width      =m.Horz_Width,
          
          antennaType_id  =@AntennaType_ID  
      
     from 
        (select * from AntennaType WHERE id=@AntennaType_ID) m     
     WHERE 
        Linkend_Antenna.Linkend_ID=@Linkend_ID
  END ELSE

    
  
  */
go
------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID(N'[dbo].[sp_Link_Update_AntennaType]') IS NOT NULL
  DROP PROCEDURE dbo.sp_Link_Update_AntennaType 
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Link_Update_AntennaType 
(
--  @ID   			int,   
  @Link_ID   			int,   
  @AntennaType_ID int   
)
AS
BEGIN
  DECLARE
     @temp  table (id int);

  DECLARE 
    @id1 INT,
    @linkend1_id INT,
    @linkend2_id INT
     
  select 
  		@linkend1_id=linkend1_id,  
      @linkend2_id=linkend2_id 
    FROM Link 
    WHERE id = @Link_ID

   
  
  INSERT INTO @temp (id) 
    select id FROM Linkend_Antenna 
      WHERE linkend_id in (@linkend1_id,@linkend2_id)
    
    
    
  WHILE Exists( select id FROM @temp  )    
  BEGIN
     select top 1 @id1=id  FROM @temp
  
    exec sp_Linkend_Antenna_Update_AntennaType @ID=@id1, @AntennaType_ID=@AntennaType_ID

    DELETE FROM @temp WHERE id=@id1
  END;


  RETURN @@ROWCOUNT   
  
 
END
go
----------------------------------------------------------------------------

IF OBJECT_ID(N'link.sp_Link_Reverse') IS  not  NULL
  DROP PROCEDURE link.sp_Link_Reverse
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
������� �������
--------------------------------------------------------------- */
CREATE  PROCEDURE link.sp_Link_Reverse

(
  @ID int
)  

AS
BEGIN
  if  @ID is null
    set @id = (select top 1 id from link)

--select linkend1_id,linkend2_id from link where  (id = @ID)


  delete from Link_reflection_points where link_id=@ID

  UPDATE link
    SET 
      linkend1_id = linkend2_id,
      linkend2_id = linkend1_id,
      
      Profile_XML = null
      

    WHERE (id = @ID)


--select linkend1_id,linkend2_id from link where  (id = @ID)

  return @@ROWCOUNT

END;

go
----------------------------------------------


IF OBJECT_ID(N'dbo.sp_Link_Add') IS  not  NULL
  DROP PROCEDURE [dbo].[sp_Link_Add]
GO


/****** Object:  StoredProcedure [dbo].[sp_Link_Add]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Link_Add]
(
    @PROJECT_ID int,
    
    @OBJNAME varchar(50) ,
    
    -------------------------------------
    -- TEMPLATE_SITE_ID
    -------------------------------------
    @TEMPLATE_LINK_ID int = null,    
--    @TEMPLATE_SITE_ID int = null,    
    @PROPERTY1_ID int = null,
    @PROPERTY2_ID int = null,
    
    @NAME varchar(250),
    
    @FOLDER_ID int = null,
    @CLUTTER_MODEL_ID int = null,
    
    @LINKEND1_ID int ,
    @LINKEND2_ID int ,
        
--    @PMP_SECTOR_ID int = null,    
--    @PMP_TERMINAL_ID int = null,
    
    
    @Comments varchar(250) = null,
    @Status_id int = null,
    
  --  @NAME_SDB varchar(20) = null,
--    @NAME_TN varchar(20) = null,
    
    @SESR float = null,
    @KNG float = null,
    @CALC_METHOD int = 0      --  null        --  @State_id   			int =null, -- ������: ��������, �����������...   --  @IS_GET_INFO 			bit = 0
)    
AS
begin

  SET ANSI_WARNINGS OFF --String or binary data would be truncated.



  IF @FOLDER_ID=0 SET @FOLDER_ID=NULL;
  --if @LINK_REPEATER_ID=0 set @LINK_REPEATER_ID=null


  DECLARE 
--    @len_m float, 
  --  @sName VARCHAR(100),
    @lat1 float,
    @lon1 float,
    @lat2 float,
    @lon2 float,

    @AZIMUTH1 float,
    @AZIMUTH2 float,
     
--    @property1_id INT,
--    @property2_id INT,
    
    @id INT,
    @new_id INT;

  --  @project_id  int,  

 --   @project1_id  int,  
  --  @project2_id  int;

  --  @project_id  int,  
 --   @objname  VARCHAR(10);
   
 
 --    @aLat float,
  --   @aLon float
     
  --258929
  
    -- Name     : ����������� � ���� � ��������� before insert
  -- Address  : ����������� � ���� � ��������� before insert
  
   
  
  IF @Name       IS NULL  RAISERROR ('@Name is NULL', 16, 1);
--  IF @LENGTH_M=0  RAISERROR ('@LENGTH_M=0', 16, 1);
  
--   set @sName = [dbo].fn_Link_get_new_name (@PROJECT_ID, @Name)


--  set @LENGTH_M = round(@LENGTH_M,1)



  
 
--------------------------
-- check if name exists
-------------------------- 
  SELECT top 1 @id=id 
  	FROM link 
    WHERE (name=@NAME) and (PROJECT_ID=@PROJECT_ID)
           and ((@Folder_id IS NULL) or (Folder_id=@Folder_id))  

  IF @id is not NULL
  BEGIN
    SELECT 'record already exists' as msg,  @id as ID     
    RETURN -@id;  
  END  







  IF NOT EXISTS(SELECT * FROM Status WHERE (id=@status_id))
  BEGIN 
    set @status_id=NULL
  END
/* 

  IF NOT EXISTS(SELECT * FROM lib_Link_State WHERE (id=@state_id))
  BEGIN 
    set @state_id=NULL
  END
*/


  IF NOT EXISTS (SELECT * FROM ClutterModel WHERE (ID=@CLUTTER_MODEL_ID))
  BEGIN 
    SELECT top 1 @CLUTTER_MODEL_ID=id FROM ClutterModel 
    
    if @CLUTTER_MODEL_ID is NULL  RAISERROR ('@CLUTTER_MODEL_ID is NULL', 16, 1);
    
   -- PRINT 'SELECT * FROM ClutterModel WHERE (ID=@CLUTTER_MODEL_ID)'
     
  END
  
  

  

  -----------------------------------------  
--  IF (@LINKEND1_ID IS NOT NULL ) and (@LINKEND2_ID IS NOT NULL )
  -----------------------------------------
--  BEGIN
 /*   IF NOT EXISTS(SELECT * FROM LinkEND WHERE ID=@LINKEND1_ID)
    BEGIN
      RAISERROR ('@LINKEND1_ID not exists', 16, 1);
      return -1      
    END      

    IF NOT EXISTS(SELECT * FROM LinkEND WHERE ID=@LINKEND2_ID)
    BEGIN
      RAISERROR ('@LINKEND2_ID not exists', 16, 1);
      return -2
    END      
*/
 /*
    IF EXISTS(SELECT * FROM Link WHERE (LINKEND1_ID in (@LINKEND1_ID,@LINKEND2_ID)) or 
                                       (LINKEND2_ID in (@LINKEND1_ID,@LINKEND2_ID)))
    BEGIN
      RAISERROR (' IF EXISTS(SELECT * FROM Link WHERE (LINKEND1_ID in (@LINKEND1_ID,@LINKEND2_ID)) or (LINKEND2_ID in (@LINKEND1_ID,@LINKEND2_ID)))', 16, 1);

      SELECT * FROM Link WHERE (LINKEND1_ID in (@LINKEND1_ID,@LINKEND2_ID)) or 
                               (LINKEND2_ID in (@LINKEND1_ID,@LINKEND2_ID))


      return -3
    END      
 
*/

   -- IF EXISTS(SELECT * FROM Link WHERE (LINKEND1_ID=@LINKEND1_ID) and (LINKEND2_ID=@LINKEND2_ID)) or 
  --     EXISTS(SELECT * FROM Link WHERE (LINKEND1_ID=@LINKEND2_ID) and (LINKEND2_ID=@LINKEND1_ID))
  --    RAISERROR ('@LINKEND1_ID<>@LINKEND2_ID', 16, 1);

--    set @objname='link'



--------------------------------------------------------------------
-- 
--------------------------------------------------------------------
if @TEMPLATE_LINK_ID is not null
begin
  IF @property1_id is null    RAISERROR ('@property1_id', 16, 1);
  IF @property2_id is null    RAISERROR ('@property2_id', 16, 1);


  delete from LINKEND where id in (@LINKEND1_ID,@LINKEND2_ID)

  exec @LINKEND1_ID = [lib].sp_Template_Link_apply_to_link @property_id=@property1_id, @TEMPLATE_LINK_ID=@TEMPLATE_LINK_ID
  exec @LINKEND2_ID = [lib].sp_Template_Link_apply_to_link @property_id=@property2_id, @TEMPLATE_LINK_ID=@TEMPLATE_LINK_ID

end else
begin
  SELECT @property1_id=property_id  FROM LinkEnd WHERE (ID=@LINKEND1_ID);            
  SELECT @property2_id=property_id  FROM LinkEnd WHERE (ID=@LINKEND2_ID);

end
 

--    IF @project1_id  IS NULL  BEGIN RAISERROR ('@project1_id is NULL', 16, 1);  return -4  END 
--    IF @project2_id  IS NULL  BEGIN RAISERROR ('@project2_id is NULL', 16, 1);  return -5  END 
--    IF @project1_id <> @project2_id  BEGIN RAISERROR ('@project1_id <> @project1_id', 16, 1);  return -6 END 
--    IF @property1_id = @property2_id  BEGIN RAISERROR ('@property1_id = @property2_id ', 16, 1);  return -8 END 
 
 
           
--    SELECT @property2_id=property_id 
  --    FROM LinkEnd WHERE (ID=@LINKEND2_ID)

    ----------------------------------
    --  set CHANNEL_TYPE 'low','high'
    ----------------------------------
  --  UPDATE LinkEnd set CHANNEL_TYPE='low'  WHERE (ID=@LINKEND1_ID);
  --  UPDATE LinkEnd set CHANNEL_TYPE='high' WHERE (ID=@LINKEND2_ID);


   
--  END ELSE  
 -- BEGIN
 -- print  ''
  
 --   set @objname='pmp_link'
    
 --   set @property1_id = dbo.fn_PMP_Sector_Get_Property_id(@PMP_SECTOR_ID)
--IF @property1_id  IS NULL  RAISERROR ('@property1_id is NULL   111111111', 16, 1); 

/*
    SELECT 
    	@property1_id=property_id   --, @project2_id=project_id 
     --   @project1_id =project_id 
      FROM view_PMP_sector --_with_project_id
      WHERE (ID=@Pmp_sector_ID)
      


    SELECT 
    	@project1_id=project_id 
   	FROM Property 
    WHERE (ID=@property1_id);


--    SELECT @property1_id = property_id
  --  FROM PMP_Sector WHERE id= @PMP_SECTOR_ID
    --LEFT OUTER JOIN PMP_site  ON PMP_Sector.pmp_site_id = PMP_site.id

    SELECT 
    	@property2_id=property_id  
    --    @project2_id=project_id 
      FROM view_PMP_TERMINAL --_with_project_id 
      WHERE (ID=@PMP_TERMINAL_ID)
     
     */
      
 --   IF @project1_id   IS NULL BEGIN RAISERROR ('@project1_id is NULL', 16, 1);   return -4  END 
--    IF @project2_id   IS NULL BEGIN RAISERROR ('@project2_id is NULL', 16, 1);   return -4  END     
--    IF @property1_id  IS NULL  RAISERROR ('@property1_id is NULL', 16, 1); 
--    IF @property2_id  IS NULL  RAISERROR ('@property2_id is NULL', 16, 1); 
--    IF @project1_id <> @project2_id BEGIN  RAISERROR ('@project1_id <> @project1_id', 16, 1);  return -4  END 
 

 -- END




--  exec sp_Object_GetNewName @OBJNAME='Link', @NAME=@NAME, 
 --       @PROJECT_ID=@project_id, @FOLDER_ID=@FOLDER_ID, @RESULT=@sName out


--  set @len_m = dbo.fn_Property_Get_Length_m (@property1_id, @property2_id);
  --IF @len_m > 100000 
   --  RAISERROR ('@len_m > 100 000 ', 16, 1);    
  


  INSERT INTO Link
     (PROJECT_ID, 
      NAME, 
      FOLDER_ID,  
      objname, 
  
      LINKEND1_ID, 
      LINKEND2_ID, 
    
--      PMP_SECTOR_ID, 
--      PMP_TERMINAL_ID,
      
      property1_id,
      property2_id,
         
      length --temp
      
      )
  VALUES
     (@project_id, 
      @NAME, --@sNAME, 
      @FOLDER_ID,
      @objname, 
      
      @LINKEND1_ID, 
      @LINKEND2_ID,
    
--      @PMP_SECTOR_ID, 
--      @PMP_TERMINAL_ID,
      
      @property1_id,
      @property2_id,
             
     
      1 --length 
      ) 
   
     
--  set @new_id=@@IDENTITY;
        
  set @new_id=IDENT_CURRENT ('Link')
     


Alter table  link  Disable trigger all

  
  
  UPDATE Link
  SET    
    CLUTTER_MODEL_ID = @CLUTTER_MODEL_ID,
     
    status_id=@status_id,
      
  --  NAME_SDB=@NAME_SDB,
  --  NAME_TN =@NAME_TN,
    Comments=@Comments,
    
   -- Link_REPEATER_ID=@Link_REPEATER_ID,
      
    SESR= @SESR,
    KNG = @KNG,
 --   SPACE_LIMIT = @SPACE_LIMIT,
    CALC_METHOD = @CALC_METHOD
    
  WHERE 
      id = @new_ID

  exec sp_Link_Update_Length_and_Azimuth @id = @new_ID
  
  Alter table  link ENABLE trigger all
  
  
  -------------------------------------------
  select @lat1=lat, @lon1=lon  FROM [Property]  WHERE id=@property1_id    
  select @lat2=lat, @lon2=lon  FROM [Property]  WHERE id=@property2_id

--RETURN


exec dbo.sp_Link_rename_linkends_UPD @new_ID




  
  --  IF @@IDENTITY=0  RAISERROR ('@@IDENTITY=0', 16, 1);
  
    
 -- if @IS_GET_INFO=1
    SELECT id,guid,name,  
    	--	LENGTH_M,	
    		@lat1 as lat1, @lon1 as lon1,  
    		@lat2 as lat2, @lon2 as lon2
      FROM link 
      WHERE ID=@new_id
  
  RETURN @new_id;     
    
--   RETURN @@IDENTITY;    
        
end

/*


Alter table  property Disable trigger all


--  set @new_id=@@IDENTITY
  
    UPDATE property
    SET     
      TOWER_HEIGHT = @TOWER_HEIGHT,
    
      GROUND_HEIGHT =@GROUND_HEIGHT,
      CLUTTER_HEIGHT=@CLUTTER_HEIGHT,
      clutter_name  =@clutter_name,
      
      LAT_WGS = @LAT_WGS,
      LON_WGS = @LON_WGS,      
      
      LAT_CK95 = @LAT_CK95,
      LON_CK95 = @LON_CK95,
            
      ADDRESS 	=@ADDRESS,
      CITY 		=@CITY,
      GEOREGION_ID =@GEOREGION_ID,


      State_id=@State_id, 
      Placement_id=@Placement_id,
     
      Comments=@Comments
    
 
      
   	WHERE 
    	id = @new_id
  
  exec sp_Property_Update_lat_lon_kras_wgs @ID=@new_id
  

Alter table  property ENABLE trigger all


*/

GO


IF OBJECT_ID(N'[dbo].[sp_LinkEnd_Update]') IS NOT NULL
  DROP PROCEDURE [dbo].[sp_LinkEnd_Update]


GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_LinkEnd_Update
(
  @ID int,
  
  
  @CHANNEL_TYPE VARCHAR(4) = null --low,high
  
)
AS
BEGIN
  -- ERROR codes
  -- -1 - @nextLinkEnd_id IS NULL  


  DECLARE
    @sCHANNEL_TYPE VARCHAR(4),
--    @sSubBand VARCHAR(20),
--    @sSubBand_next VARCHAR(20),
--    @LINKENDType_ID int,
  	@nextLinkEnd_id int;
  	
  		
  set @nextLinkEnd_id = dbo.fn_LinkEnd_Get_Next_LinkEnd_ID (@ID)
  

  
  if @CHANNEL_TYPE='low'
    set @sCHANNEL_TYPE='high'
  ELSE
    set @sCHANNEL_TYPE='low'
    
--   CHANNEL_NUMBER_TYPE  =IIF(m.CHANNEL_NUMBER_TYPE=0, 1, 0),

  ----------------
  ----- MAIN ----- 
  ----------------  
  
  UPDATE LinkEnd 
  SET 
    CHANNEL_TYPE  =  COALESCE( @CHANNEL_TYPE, CHANNEL_TYPE)
 
  WHERE 
  		id = @ID
      
      
  ----------------
  ----- NEXT -----      
  ----------------
  
 UPDATE LinkEnd 
  SET 
    CHANNEL_TYPE  =  COALESCE( @sCHANNEL_TYPE, CHANNEL_TYPE)
 
  WHERE 
  		id = @nextLinkEnd_id  
    


END;
go
-----------------------------------------


IF OBJECT_ID(N'dbo.view_LinkEnd_with_project_id') IS NOT NULL
  DROP view [dbo].[view_LinkEnd_with_project_id]
       
GO


CREATE VIEW dbo.view_LinkEnd_with_project_id
AS
SELECT dbo.LinkEnd.*,
       dbo.Property.project_id
FROM dbo.LinkEnd
     LEFT OUTER JOIN dbo.Property ON dbo.LinkEnd.property_id =  dbo.Property.id
     
where 
  LinkEnd.ObjName is null or      
  LinkEnd.ObjName = 'linkend'

go
-------------------------------------------------------



IF OBJECT_ID(N'dbo.sp_Map_XREF_Update') IS not  NULL
  DROP PROCEDURE dbo.sp_Map_XREF_Update
GO


/* ---------------------------------------------------------------
created: Alex  
used in:  

delete !!!!!!!!
 
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Map_XREF_Update
(
  @ID       INT, -- MAP_XREF id

  @CHECKED BIT,

--  @priority   INT,
  @ZOOM_MIN   INT = NULL,
  @ZOOM_MAX   INT = NULL,

  @ALLOW_ZOOM BIT = NULL,
  
  @AUTOLABEL BIT = NULL,
  @LABEL_STYLE VARCHAR(MAX) = NULL,
      
  @STYLE VARCHAR(MAX) = NULL
)
AS
BEGIN
 ------------------------------------------------------
  IF @ID is null  RAISERROR ('@ID is null', 16, 1);
  ------------------------------------------------------
  
 
  UPDATE MAP_XREF 
    SET 
      CHECKED  = COALESCE(@CHECKED,CHECKED),
      
      ZOOM_MIN =COALESCE(@ZOOM_MIN,  ZOOM_MIN),
      ZOOM_MAX =COALESCE(@ZOOM_MAX,  ZOOM_MAX),

      ALLOW_ZOOM =COALESCE(@ALLOW_ZOOM,ALLOW_ZOOM),
        
      AutoLabel = COALESCE(@AutoLabel,AutoLabel),
      label_style = COALESCE(@label_style,label_style),

      style = COALESCE(@style, style)
 

--priority =@priority
    WHERE id=@ID
  
END

go

------------------------------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID(N'dbo.sp_Object_Get_Pos') IS not  NULL
  DROP PROCEDURE dbo.sp_Object_Get_Pos
GO




CREATE PROCEDURE dbo.[sp_Object_Get_Pos]
(
  @OBJNAME    VARCHAR(50),  
  @ID   INT

 
--  @LAT  float OUTPUT,
--  @Lon  float OUTPUT
  
)
AS
BEGIN

  if @OBJNAME is null
  begin
    set  @OBJNAME = 'linkend'
--    set  @ID = 3378
    set  @ID = 5119
  
  end
  
---------------------------------------------------
if @OBJNAME = 'linkend'
    SELECT                  
       A1.lat,
       A1.lon
                          
/*       @LAT = A1.lat,
       @Lon = A1.lon
*/
    FROM dbo.LinkEnd 
         cross apply [dbo].fn_Linkend_Antenna_pos (id) A1
         
    where id=@ID

 

return @@ROWCOUNT     

 
END
go


IF OBJECT_ID(N'[map].[sp_MapMaker]') IS  not  NULL

DROP PROCEDURE [map].[sp_MapMaker]
GO
/****** Object:  StoredProcedure [map].[sp_MapMaker]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE [map].[sp_MapMaker]
(
  @PROJECT_ID 	INT=null,
  @OBJNAME 		  VARCHAR(50)=null,
  @ID 	  		  INT=null
  
--  @CHECKSUM_ONLY  bit=0   -- get only chcksum, not selection

--  @MODE VARCHAR(50)=null,
 -- @FIELDS VARCHAR(50)=null
  
 -- @ID 	INT=null
  
 -- @TABLENAME 	VARCHAR(50)=null 
)

AS
BEGIN
  DECLARE
    @s VARCHAR(1000),
  	@checksum INT;
    
    /*
if @PROJECT_ID is null    set @PROJECT_ID=1165
if @OBJNAME is null       set @OBJNAME='Link'
   
  */

/*set @OBJNAME='Linkend_antenna' 
set @CHECKSUM_ONLY =1
set @MODE ='data'
*/

/*
set @OBJNAME='link' 
set @CHECKSUM_ONLY =1
*/

/*  SELECT top 1 @PROJECT_ID=ID 
  	FROM Project
*/    
  
-- if @OBJNAME IS NULL   set @OBJNAME='Linkend_antenna' 
-- if @OBJNAME IS NULL   set @OBJNAME='pmp_calc_region' 

/*
    SELECT CHECKSUM_AGG (BINARY_CHECKSUM(lat,lon ) )
  --  SELECT CHECKSUM_AGG (lat,lon  )
        FROM PMP_CalcRegion_Polygons
          
          */ 
          
      
 /* 
CREATE PROCEDURE dbo.sp_MapMaker_Get_Object_visible_columns
(
  @OBJNAME varchar(50),
  
  @COLUMNS VARCHAR(4000) OUTPUT
)*/          
          
          
  
  -------------------------------------------------------------
  IF @OBJNAME='link_repeater'  
  -------------------------------------------------------------
  BEGIN
    IF @ID IS NOT NULL 
    begin
      SELECT id,lat,lon FROM view_link_repeater WHERE (id=@ID)
      RETURN @@ROWCOUNT     
    end


    SELECT id,lat,lon 
      FROM view_Link_Repeater
      WHERE (PROJECT_ID=@PROJECT_ID)
        
--        WHERE ((@PROJECT_ID IS NOT NULL) and (PROJECT_ID=@PROJECT_ID)) or 
--        	  ((@ID IS NOT NULL) and (id=@ID))
   

  END else



  -------------------------------------------------------------
  IF @OBJNAME='Property'  
  -------------------------------------------------------------
  BEGIN
    IF @ID IS NOT NULL 
    begin
      SELECT id,lat,lon, lat_wgs,lon_wgs FROM Property WHERE (id=@ID)
      RETURN @@ROWCOUNT     
    end


    SELECT id,lat,lon , lat_wgs,lon_wgs
      FROM Property 
      WHERE (PROJECT_ID=@PROJECT_ID)
      order by id
        
--        WHERE ((@PROJECT_ID IS NOT NULL) and (PROJECT_ID=@PROJECT_ID)) or 
--        	  ((@ID IS NOT NULL) and (id=@ID))
   

  END else

  -------------------------------------------------------------
  IF @OBJNAME='Link'  
  -------------------------------------------------------------
  BEGIN
    -----------------------------------------------------
    IF @ID IS NOT NULL 
    begin
      SELECT * 
          --   CHECKSUM(lat1+lat2, lon1+lon2)  as CHECKSUM
        from VIEW_link_location
         WHERE (id=@ID)-- and (IsNull(objname,'link')='link')
         order by id
         
    
      RETURN @@ROWCOUNT     
    end
    
    -----------------------------------------------------

    SELECT * 
        --   CHECKSUM(lat1+lat2, lon1+lon2)  as CHECKSUM
      from VIEW_link_location
      WHERE (PROJECT_ID=@PROJECT_ID) --and (IsNull(objname,'link')='link')
      order by id

  END else


  -------------------------------------------------------------
  IF @OBJNAME='pmp_Link'  
  -------------------------------------------------------------
  BEGIN
    -----------------------------------------------------
    IF @ID IS NOT NULL 
    begin
      SELECT * 
          --   CHECKSUM(lat1+lat2, lon1+lon2)  as CHECKSUM
        from VIEW_link_location
         WHERE (id=@ID) and (IsNull(objname,'link')='pmp_link')
    
      RETURN @@ROWCOUNT     
    end
    
    -----------------------------------------------------

    SELECT * 
        --   CHECKSUM(lat1+lat2, lon1+lon2)  as CHECKSUM
      from VIEW_link_location
      WHERE (PROJECT_ID=@PROJECT_ID) and (IsNull(objname,'link')='pmp_link')

  END else


  -------------------------------------------------------------
  IF @OBJNAME='pmp_calc_region' or @OBJNAME='calc_region_pmp'
  -------------------------------------------------------------
  BEGIN
    -----------------------------------------------------
    IF @ID IS NOT NULL 
    begin
      SELECT id
       FROM pmp_calcregion
       WHERE (ID=@ID)
      
      RETURN @@ROWCOUNT     
    end  
    -----------------------------------------------------
    
  
      SELECT id              
        FROM pmp_calcregion 
        WHERE (PROJECT_ID=@PROJECT_ID)

      
      -- -0-M LEFT OUTER JOIN  Property P ON M.property_id = P.id
  --    WHERE M.PROJECT_ID=@PROJECT_ID  
    
--    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  --  RETURN @checksum
  END else 


  -------------------------------------------------------------
  IF @OBJNAME='PMP_site'
  -------------------------------------------------------------
  BEGIN
    -----------------------------------------------------
    IF @ID IS NOT NULL 
    begin
     -- SELECT id,lat,lon FROM Property WHERE (id=@ID)
            
      SELECT M.id, P.lat, P.lon 
       FROM PMP_site M 
       		LEFT OUTER JOIN  Property P ON M.property_id = P.id
       WHERE (M.ID=@ID)
      
      RETURN @@ROWCOUNT     
    end  
    -----------------------------------------------------
  
  
    SELECT M.id, P.lat, P.lon 
     FROM view_PMP_site M 
          LEFT OUTER JOIN  Property P ON M.property_id = P.id
     WHERE (M.PROJECT_ID=@PROJECT_ID)-- and ((@ID IS NULL) or (M.id=@ID))   
    
  END else

  -------------------------------------------------------------
  IF @OBJNAME='PMP_terminal'
  -------------------------------------------------------------
  BEGIN
    -----------------------------------------------------
    IF @ID IS NOT NULL 
    begin
     -- SELECT id,lat,lon FROM Property WHERE (id=@ID)
            
      SELECT M.id, P.lat, P.lon 
       FROM view_PMP_terminal M 
       		LEFT OUTER JOIN  Property P ON M.property_id = P.id
       WHERE (M.ID=@ID)
      
      RETURN @@ROWCOUNT     
    end  
    -----------------------------------------------------
  
  
    SELECT M.id, P.lat, P.lon 
     FROM view_PMP_TERMINAL M 
          LEFT OUTER JOIN  Property P ON M.property_id = P.id
     WHERE (M.PROJECT_ID=@PROJECT_ID)-- and ((@ID IS NULL) or (M.id=@ID))
       
  END else


  -------------------------------------------------------------
  IF @OBJNAME='MSC'  
  -------------------------------------------------------------
  BEGIN
    -----------------------------------------------------
    IF @ID IS NOT NULL 
    begin
     -- SELECT id,lat,lon FROM Property WHERE (id=@ID)
            
      SELECT M.id, P.lat, P.lon 
       FROM MSC M LEFT OUTER JOIN  Property P ON M.property_id = P.id
       WHERE (M.ID=@ID)
      
      RETURN @@ROWCOUNT     
    end  
    -----------------------------------------------------
    
    SELECT M.id, P.lat, P.lon 
       FROM MSC M 
       		LEFT OUTER JOIN  Property P ON M.property_id = P.id
       WHERE (M.PROJECT_ID=@PROJECT_ID)-- and ((@ID IS NULL) or (M.id=@ID))
   

  END else
   



  -------------------------------------------------------------
  IF @OBJNAME='BSC'  
  -------------------------------------------------------------
  BEGIN
    -----------------------------------------------------
    IF @ID IS NOT NULL 
    begin
     -- SELECT id,lat,lon FROM Property WHERE (id=@ID)
            
      SELECT M.id, P.lat, P.lon 
       FROM BSC M 
       		LEFT OUTER JOIN  Property P ON M.property_id = P.id
       WHERE (M.ID=@ID)
      
      RETURN @@ROWCOUNT     
    end
    -----------------------------------------------------
  
  
      SELECT M.id, P.lat, P.lon 
       FROM BSC M 
       		LEFT OUTER JOIN  Property P ON M.property_id = P.id
--       WHERE M.PROJECT_ID=@PROJECT_ID
       WHERE (M.PROJECT_ID=@PROJECT_ID)-- and ((@ID IS NULL) or (M.id=@ID))
      
  END else



  -------------------------------------------------------------
  IF @OBJNAME='Linkend'  
  -------------------------------------------------------------
  BEGIN
    -----------------------------------------------------
    IF @ID IS NOT NULL 
    begin
     -- SELECT id,lat,lon FROM Property WHERE (id=@ID)
            
     SELECT M.id, P.lat, P.lon 
       FROM Linkend M 
  --     		LEFT OUTER JOIN  Property P ON M.property_id = P.id
           cross apply [dbo].fn_Linkend_Antenna_pos (id) P
            
       WHERE (M.ID=@ID)
      
      RETURN @@ROWCOUNT     
    end
    -----------------------------------------------------
     

      
  
      SELECT M.id, p.lat,p.lon --, BINARY_CHECKSUM(lat,lon) as CHECKSUM
        FROM view_Linkend M 
--        FROM view_Linkend_with_project_id M 
--        	LEFT OUTER JOIN Property P ON M.property_id = P.id
           cross apply [dbo].fn_Linkend_Antenna_pos (id) P
            
        WHERE (m.PROJECT_ID=@PROJECT_ID)-- and ((@ID IS NULL) or (M.id=@ID))
      
  END else


/*
  -------------------------------------------------------------
  IF @OBJNAME='PMP_Sector_ant'  
  -------------------------------------------------------------
  BEGIN
    -----------------------------------------------------
    IF @ID IS NOT NULL 
    begin
      SELECT id, map_lat as lat, map_lon as lon, azimuth
       FROM view_pmp_sector_antennas ---M  LEFT OUTER JOIN  Property P ON M.property_id = P.id
       WHERE (ID=@ID)
      
      RETURN @@ROWCOUNT     
    end
    -----------------------------------------------------
    
      SELECT id,map_lat as lat, map_lon as lon, azimuth --, BINARY_CHECKSUM(lat,lon) as CHECKSUM
        FROM view_pmp_sector_antennas
        WHERE (PROJECT_ID=@PROJECT_ID) --and ((@ID IS NULL) or (id=@ID))
          
    
  END else
*/

  -------------------------------------------------------------
  IF @OBJNAME='Linkend_antenna'  
  -------------------------------------------------------------
  BEGIN
    -----------------------------------------------------
    IF @ID IS NOT NULL 
    begin
      SELECT id,map_lat as lat, map_lon as lon, azimuth
       FROM view_LinkEnd_antennas ---M  LEFT OUTER JOIN  Property P ON M.property_id = P.id
       WHERE (ID=@ID)
      
      RETURN @@ROWCOUNT     
    end
    -----------------------------------------------------
      
  
     SELECT id, map_lat as lat, map_lon as lon,  azimuth --, BINARY_CHECKSUM(lat,lon) as CHECKSUM
        FROM view_LinkEnd_antennas
        WHERE (PROJECT_ID=@PROJECT_ID) --and ((@ID IS NULL) or (id=@ID))


  END ELSE BEGIN
    SELECT 'Object not found: ' + @OBJNAME
    RETURN -1
    
  END
  
  
  RETURN @@ROWCOUNT


END

GO
/****** Object:  StoredProcedure [map].[sp_MapMaker_Data]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[map].[sp_MapMaker_Data]') IS  not  NULL

DROP PROCEDURE [map].[sp_MapMaker_Data]
GO
/****** Object:  StoredProcedure [map].[sp_MapMaker_Data]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [map].[sp_MapMaker_Data]
(
  @PROJECT_ID 	  INT, --=null,
  @OBJNAME 		  VARCHAR(50) --=null
)

AS
BEGIN
  DECLARE
    @s VARCHAR(1000),    
    @LABEL_SQL varchar(5000),    
  	@checksum INT;
    
    
if @PROJECT_ID is null
begin
  set @PROJECT_ID= (select top 1  id from Project)
  
  set @OBJNAME = 'property'

end


-------exec sp_MapMaker_get_object_label @ObjName=@ObjName, @IS_INCLUDE_CAPTION=0, @LABEL_SQL=@LABEL_SQL output

--select @LABEL_SQL

--print @LABEL_SQL


/*
CREATE PROCEDURE dbo.____sp_MapMaker_get_object_label
(
  @ObjName 	varchar(50),
  @IS_INCLUDE_CAPTION bit = null,
  
  @LABEL_SQL varchar(5000) output
)

*/


/*
select * from map.user_map_settings 
*/


declare 
  @caption_sql varchar(max) =
         (select top 1 caption_sql 
             from  map.user_map_settings
             where
                OBJNAME  = @OBJNAME and 
                user_name = user_name() and
                host_name = host_name() 

--                user_name = 'dbo' and
--                host_name = 'alex' 


          )  
          
if IsNull(@caption_sql,'') = '' 
  set @caption_sql = 'name'


set @caption_sql = replace (@caption_sql, ';', '+'',''+' )


print @caption_sql

declare 
  @sql varchar(max)


set @caption_sql = 'cast('+ @caption_sql +' as varchar(2000))'
--cast((:caption) as varchar(2000)) 


--cast((:caption) as varchar(2000))



  ----------------------
  IF @OBJNAME='Property'  
   set @sql = ' SELECT id, 
                   name, :caption as caption,  status, state_name,  mast_count,   --name, --+ CHAR(13)+ name 
                   
                   Linkend_Antenna_count
                   
              FROM view_Property_
              WHERE PROJECT_ID=:PROJECT_ID
              order by id
            '


--      SELECT id, (name + CHAR(10)+ 'id:'+ cast(id as VARCHAR(50))) as caption    --name,
      
      
--      SELECT CAST(id as INT), (name + CHAR(10)+ 'id:'+ cast(id as VARCHAR(50))) as caption    --name,
--      FROM Property

      
--      )) or

  --    WHERE ((@ID IS NULL) and (PROJECT_ID=@PROJECT_ID)) or
    --        ((@ID IS NOT NULL) and (id=@ID))
            
  else
  ----------------------
  IF @OBJNAME='Link'  
   set @sql = '    
                SELECT  id,
                        name, status_name, band, LinkEndType_name,  :caption as caption  --name,
                  FROM view_Link
                  WHERE (PROJECT_ID=:PROJECT_ID) -- and (IsNull(objname,''link'')=''link'')
                  order by id
  '
      
      
      -- and ((@ID IS NULL) or (id=@ID))
  else

  ----------------------
  IF @OBJNAME='pmp_Link'  
   set @sql = '    
  
    SELECT  id,
            name, status_name,  :caption as caption  --name,
      FROM view_Link
      WHERE (PROJECT_ID=:PROJECT_ID)  and (IsNull(objname,''link'')=''pmp_link'')
'      
      -- and ((@ID IS NULL) or (id=@ID))
  else


  ----------------------
  IF @OBJNAME='Linkend'  
   set @sql = '    
  
    SELECT id, 
           name, 
           :caption as caption  --name,
      FROM view_Linkend  --_base --_with_project_id
      WHERE (PROJECT_ID=:PROJECT_ID) 
      order by id
'      
      
      --and ((@ID IS NULL) or (id=@ID))
  else

          
  
  -------------------------------------------------------------
  IF @OBJNAME='link_repeater'  
  -------------------------------------------------------------
  BEGIN
   set @sql = '    
  
  SELECT id, 
           name, 
           :caption as caption  --name,
      FROM view_Link_Repeater
      WHERE (PROJECT_ID=:PROJECT_ID) --and ((@ID IS NULL) or (id=@ID))  
'  
        
--        WHERE ((@PROJECT_ID IS NOT NULL) and (PROJECT_ID=@PROJECT_ID)) or 
--        	  ((@ID IS NOT NULL) and (id=@ID))
   

  END else

    
  ----------------------
  IF @OBJNAME='Linkend_antenna'  
   set @sql = '    
  
    SELECT id,
          name, 
         :caption as caption
      FROM view_LinkEnd_antennas
      WHERE (PROJECT_ID=:PROJECT_ID) --and ((@ID IS NULL) or (id=@ID))     
'      
      --name, --cast(azimuth as VARCHAR(50)) 
  else
    
  ----------------------
  IF @OBJNAME='MSC'  
   set @sql = '    

    SELECT id,
       name, :caption as caption    --name,
      FROM MSC
      WHERE (PROJECT_ID=:PROJECT_ID)-- and ((@ID IS NULL) or (id=@ID))
'
  else

  ----------------------
  IF @OBJNAME='PMP_site'  
   set @sql = '    

    SELECT id,
           name, 
           :caption as caption    --name,
      FROM view_PMP_site
      WHERE (PROJECT_ID=:PROJECT_ID)-- and ((@ID IS NULL) or (id=@ID))
'
  else


  ----------------------
  IF @OBJNAME='PMP_terminal'  
   set @sql = '    
    SELECT id,
          name, :caption as caption    --name,
      FROM view_PMP_TERMINAL --_with_project_id
      WHERE (PROJECT_ID=:PROJECT_ID)-- and ((@ID IS NULL) or (id=@ID))
'      
  else
    
  ----------------------
  IF @OBJNAME='BSC'  
   set @sql = '    
  
    SELECT id,
       name, 
       :caption as caption    --name,
      FROM BSC
      WHERE (PROJECT_ID=:PROJECT_ID) --and ((@ID IS NULL) or (id=@ID))
'
  else
    
  ----------------------
  IF @OBJNAME='pmp_calc_region' or  @OBJNAME='calc_region_pmp'
   set @sql = '    
 
   SELECT id, 
           name, 
           :caption as caption    --name,
      FROM pmp_calcregion
      WHERE (PROJECT_ID=:PROJECT_ID) --and ((@ID IS NULL) or (id=@ID))
'

  ----------------------

  ELSE BEGIN
    RAISERROR ('Object not found', 16, 1);

    SELECT 'Object not found: ' + @OBJNAME
    RETURN -1      
  END


  set @sql = Replace (@sql, ':caption',    @caption_sql )
  set @sql = Replace (@sql, ':PROJECT_ID', cast(@PROJECT_ID as varchar(20)) )


print @sql

  exec (@sql)  
  
  
  RETURN @@ROWCOUNT

 

END

GO
/****** Object:  StoredProcedure [map].[sp_options_object_list_names]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[map].[sp_options_object_list_names]') IS  not  NULL

DROP PROCEDURE [map].[sp_options_object_list_names]
GO
/****** Object:  StoredProcedure [map].[sp_options_object_list_names]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [map].[sp_options_object_list_names]
(
  @OBJNAME varchar(50)='link'

)
AS
BEGIN
  
  select * 
    from dbo.view__object_fields_enabled
    where 
      OBJNAME = @OBJNAME 
--      and [type] in ('list', 'status','lib','xref' ) 
      and 
         ([type] in ('list', 'status','lib','xref' ) or IsNull(xref_TableName,'') <> '' ) -- is not null)
--         ([type] in ('list', 'status','lib','xref' ) or lib_Table_Name is not null)
      
   order by 
      caption   
  
  
END

GO
/****** Object:  StoredProcedure [map].[sp_options_object_list_values]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[map].[sp_options_object_list_values]') IS  not NULL

DROP PROCEDURE [map].[sp_options_object_list_values]
GO
/****** Object:  StoredProcedure [map].[sp_options_object_list_values]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [map].[sp_options_object_list_values]
(
  @ID int -- =19400

)
AS
BEGIN
  if @id is null
--    set @id = (select top 1 id from _object_fields where [type]= 'lib')
--    set @id = 58500
    set @id =	60500	


/*�������� ���������	name	id	type	xref_TableName
4. ��� ������������ ����������� (1- �����������������, 2-������������, 3-������)	underlying_terrain_type	60500	list	
*/


declare
  @report_tbl TABLE (id INt, code int, name varchar(100))


declare 
  @new_id int,
  @status_name varchar(50) ,
  @field_name varchar(50) ,
  @field_type varchar(50) ,
  @lib_Table_Name varchar(50),
  @xref_TableName varchar(50),
  @items varchar(max),
  
  @table_name_initial varchar(50)
  

  select @field_name  = name, 
         @field_type  = [type],
         @status_name = status_name,
         @items = items,
--         @lib_Table_Name     = IsNull(lib_Table_Name,'') ,
         @table_name_initial = table_name_initial,
         @xref_TableName = IsNull(xref_TableName,'')
         
    from dbo.view__object_fields_enabled
    where ID = @ID


/*select @field_name

return
*/

--CREATE FUNCTION [dbo].ft_VARCHAR_String_to_Table
/*
 select *
         
    from dbo.view__object_fields_enabled
    where ID = @ID
  */
 
 ---------------------
-- select * from [dbo].ft_VARCHAR_String_to_Table (@items, char(10))
 

  if @field_type = 'list' and 
    not exists (
               select * 
                  from lib.PickList    
                  where 
                     table_name=@table_name_initial and 
                     field_name = @field_name  )
  
  begin
  
    insert into  lib.PickList (table_name, field_name) values (@table_name_initial, @field_name )    
    set @new_id = @@IDENTITY


--select @field_name, @new_id

    insert into  lib.PickList_Values (PickList_id, value)  
       select @new_id, [Value]
         from [dbo].ft_VARCHAR_String_to_Table (@items, char(10))
  
  end

 ---------------------


--return

 

    
  print @field_type  
  print @lib_Table_Name  
  print @status_name  
   
    
   
--   select id,name  
--        from view_Status_Values where status_name = @status_name  


--select @xref_TableName


 
-----------------------------------------
  IF @field_type = 'status' 
-----------------------------------------
  begin
--    print '1--------------------'
  
    insert into @report_tbl  (id  , name ) 
      select id,name  
        from view_Status_Values where status_name = @status_name
  end 
  
  else      
 

-----------------------------------------
  IF @xref_TableName <> '' 
-----------------------------------------
    insert into @report_tbl  ( name  ) 
        exec ('select distinct name  from '+ @xref_TableName +' order by name')

/* 
    insert into @report_tbl  (id  , name  ) 
        exec ('select id, name  from '+ @xref_TableName +' order by name')
*/
      
 -- else     

/*-----------------------------------------
  IF @lib_Table_Name <> '' --is not null
-----------------------------------------
    insert into @report_tbl  (id , name )
      exec ('select id, name  from '+ @lib_Table_Name +' order by name')
*/

-----------------------------------------
  ELSE    
-----------------------------------------
    insert into @report_tbl (id  , name , code)
       select id, value as name , code
          from lib.PickList_Values  
          where 
            PickList_id in
                           (select id 
                              from lib.PickList    
                              where 
                                 table_name=@table_name_initial and 
                                 field_name = @field_name
                              )

          order by 
             Display_Index, [value]


select * from @report_tbl


/*  
    select * 
      from status  
      where parent_id in
         (select id 
            from lib.PickList    
            where name=@status_name and parent_id is null 
            )
      order by index_
*/

--table_name_initial



--   and [type] in ('list', 'status','lib','xref' ) 

  
END

GO
/****** Object:  StoredProcedure [map].[sp_User_Map_Settings_UPD]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[map].[sp_User_Map_Settings_UPD]') IS  not  NULL

DROP PROCEDURE [map].[sp_User_Map_Settings_UPD]
GO
/****** Object:  StoredProcedure [map].[sp_User_Map_Settings_UPD]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [map].[sp_User_Map_Settings_UPD]
(
  @OBJNAME   varchar(50) ,
  
  @names   	 varchar(max) =  null,
  @style_XML XML =  null
)
AS
BEGIN
  if @OBJNAME is null
  begin
    set @OBJNAME = 'link'
    
    set @names = 'name'
    
 end


declare 
  @id int = (select id 
                from  
                  map.user_map_settings
                where 
                    user_name = user_name() and  
                    host_name = host_name() and
                   OBJNAME = @OBJNAME  
                )
  if @id is null                
  begin
    insert into map.user_map_settings (OBJNAME) values( @OBJNAME )      
    set @id = @@IDENTITY
  end                             

---------------------------

  update map.user_map_settings
  set 
    attribute_column_names = COALESCE( @names, attribute_column_names),
    style_XML = COALESCE( @style_XML, style_XML)
  where
    id = @id

 
  return @ID
  
 
END

GO


-------------------------------

IF OBJECT_ID(N'dbo.sp_LinkNet_Del') IS not  NULL
  DROP PROCEDURE dbo.sp_LinkNet_Del
GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE procedure dbo.sp_LinkNet_Del
(
  @ID  int
)
AS
begin   

/*SELECT id INTO #LinkFreqPlan
   from LinkFreqPlan
   where LinkNet_id=@ID*/

   
  delete from CalcMap  
     where LinkFreqPlan_id in 
       (SELECT id from LinkFreqPlan where LinkNet_id=@ID)
     


 /* delete from LinkFreqPlan_Link  where LinkFreqPlan_id=@ID
  delete from LinkFreqPlan_LinkEnd  where LinkFreqPlan_id=@ID
  delete from LinkFreqPlan_Neighbors  where LinkFreqPlan_id=@ID
 */ 
 
 


  delete from LinkFreqPlan_Resource  
      where LinkFreqPlan_id in (select id from LinkFreqPlan  where LinkNet_id=@ID)


delete from LinkFreqPlan  where LinkNet_id=@ID
   

  DELETE FROM LinkNet_Items_XREF WHERE LinkNet_ID=@ID
  DELETE FROM LinkNet WHERE ID=@ID
     
  DECLARE @result int 
  set @result =  @@ROWCOUNT ;
  
  
 IF Exists (select * from LinkNet where id=@ID)
     RAISERROR ('record Exists', 16, 1);
    
  
  return @result    
      
end
go
IF OBJECT_ID(N'[dbo].[view_Template_Linkend_Antenna]') IS NOT NULL

DROP VIEW [dbo].[view_Template_Linkend_Antenna]
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW dbo.view_Template_Linkend_Antenna
AS
SELECT a2.*,
       dbo.AntennaType.name AS [AntennaType_name],
       a1.guid AS [Template_Linkend_guid],
       a1.template_pmp_site_id AS template_pmp_site_id
       
FROM dbo.Template_LinkEnd_Antenna a2
     LEFT OUTER JOIN dbo.Template_LinkEnd a1 ON a2.template_linkend_id = a1.id
     LEFT OUTER JOIN dbo.AntennaType ON a2.antennaType_id = dbo.AntennaType.id

go

---------------------------------------------------

IF OBJECT_ID(N'[dbo].[view_Template_Linkend]') IS NOT NULL

DROP VIEW [dbo].[view_Template_Linkend]
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW dbo.view_Template_Linkend
AS
SELECT M.*,
       dbo.CalcModel.name AS [CalcModel_name],
       dbo.CellLayer.name AS [CellLayer_name],
       dbo.LinkEndType.name AS [LinkEndType_name],
       dbo.Passive_Component.name AS [Passive_Component_name]
FROM dbo.Template_LinkEnd M
     LEFT OUTER JOIN dbo.Passive_Component ON M.combiner_id =
     dbo.Passive_Component.id
     LEFT OUTER JOIN dbo.LinkEndType ON M.linkendtype_id = dbo.LinkEndType.id
     LEFT OUTER JOIN dbo.CellLayer ON M.cell_layer_id = dbo.CellLayer.id
     LEFT OUTER JOIN dbo.CalcModel ON M.calc_model_id = dbo.CalcModel.id
go




-----------------------------------------------------------
IF OBJECT_ID(N'[dbo].[view_PMP_TERMINAL]') IS NOT NULL

DROP VIEW [dbo].[view_PMP_TERMINAL]
GO


CREATE VIEW dbo.view_PMP_TERMINAL
AS
SELECT LinkEnd.*,

       dbo.Property.name AS property_name,
       dbo.Property.lat AS lat,
       dbo.Property.lon AS lon,
       dbo.Property.ground_height AS ground_height,
       
       dbo.LinkEndType.name AS LinkEndType_name,
       
       dbo.PMP_site.name AS PMP_site_name,          
       
       PMP_site.name   AS connected_pmp_site_name, 
       pmp_sector.name AS connected_pmp_sector_name, 
       
       dbo.Property.project_id       
   
  --     dbo.PMP_Sector.name AS PMP_Sector_name
       
FROM LinkEnd 
     LEFT OUTER JOIN dbo.PMP_site ON LinkEnd.pmp_site_id =     dbo.PMP_site.id
     LEFT OUTER JOIN dbo.LinkEnd pmp_sector ON LinkEnd.connected_pmp_sector_id =  pmp_sector.id
     
--     LEFT OUTER JOIN dbo.PMP_Sector ON LinkEnd.pmp_sector_id =     dbo.PMP_Sector.id
     
     LEFT OUTER JOIN dbo.LinkEndType ON LinkEnd.linkendtype_id =     dbo.LinkEndType.id
 --    LEFT OUTER JOIN dbo.Property ON LinkEnd.property_id =     dbo.Property.id
     
     
     LEFT OUTER JOIN dbo.Property ON dbo.LinkEnd.property_id = dbo.Property.id
     

     
     
where 
  LinkEnd.ObjName = 'PMP_terminal'

go



------------------------------------------------------------

IF OBJECT_ID(N'[dbo].[view_PMP_sector]') IS NOT NULL

DROP VIEW [dbo].[view_PMP_sector]
GO


CREATE VIEW dbo.view_PMP_sector
AS     
SELECT 

      dbo.LinkEnd.*,

      dbo.LinkEndType.name AS linkendtype_name,
             
      dbo.Property.guid AS [Property_guid],
      dbo.Property.georegion_id AS georegion_id,

      dbo.Property.project_id AS project_id,

             
      dbo.Property.lon,              
      dbo.Property.lat,
             
      dbo.CalcModel.name AS Calc_Model_Name,
      dbo.CellLayer.name AS cell_layer_name,
             
      dbo.Property.name AS Property_name,
             
      dbo.PMP_site.name AS PMP_site_name,
      dbo.PMP_site.guid AS PMP_site_guid
       
       
FROM dbo.PMP_site
     LEFT OUTER JOIN dbo.Property ON dbo.PMP_site.property_id = dbo.Property.id
     RIGHT OUTER JOIN dbo.CellLayer
     RIGHT OUTER JOIN dbo.LinkEnd ON dbo.CellLayer.id =   dbo.LinkEnd.cell_layer_id
     LEFT OUTER JOIN dbo.CalcModel ON dbo.LinkEnd.calc_model_id =   dbo.CalcModel.id
     LEFT OUTER JOIN dbo.LinkEndType ON dbo.LinkEnd.linkendtype_id =    dbo.LinkEndType.id ON dbo.PMP_site.id = dbo.LinkEnd.pmp_site_id
     



     
where 
  LinkEnd.ObjName = 'PMP_sector'

go

-------------------------------------------------------------------------------


IF OBJECT_ID(N'[dbo].[view_PMP_sector]') IS NOT NULL
  DROP VIEW dbo.view_PMP_sector
GO


CREATE VIEW dbo.view_PMP_sector
AS     
SELECT 

      dbo.LinkEnd.*,

      dbo.LinkEndType.name AS linkendtype_name,
             
      dbo.Property.guid AS [Property_guid],
      dbo.Property.georegion_id AS georegion_id,

      dbo.Property.project_id AS project_id,

             
      dbo.Property.lon,              
      dbo.Property.lat,
             
      dbo.CalcModel.name AS Calc_Model_Name,
      dbo.CellLayer.name AS cell_layer_name,
             
      dbo.Property.name AS Property_name,
             
      dbo.PMP_site.name AS PMP_site_name,
      dbo.PMP_site.guid AS PMP_site_guid
       
       
FROM dbo.PMP_site
     LEFT OUTER JOIN dbo.Property ON dbo.PMP_site.property_id = dbo.Property.id
     RIGHT OUTER JOIN dbo.CellLayer
     RIGHT OUTER JOIN dbo.LinkEnd ON dbo.CellLayer.id =   dbo.LinkEnd.cell_layer_id
     LEFT OUTER JOIN dbo.CalcModel ON dbo.LinkEnd.calc_model_id =   dbo.CalcModel.id
     LEFT OUTER JOIN dbo.LinkEndType ON dbo.LinkEnd.linkendtype_id =    dbo.LinkEndType.id ON dbo.PMP_site.id = dbo.LinkEnd.pmp_site_id
     



     
where 
  LinkEnd.ObjName = 'PMP_sector'
go



-----------------------------------------------------
IF OBJECT_ID(N'[dbo].[view_PMP_Link]') IS NOT NULL
  DROP VIEW dbo.view_PMP_Link
GO


CREATE VIEW dbo.view_PMP_Link
AS
SELECT     dbo.Link.*,

/*    Property1.lat AS lat1, Property1.lon AS lon1, 
    Property2.lat AS lat2, Property2.lon AS lon2, 
*/         
    dbo.LinkEnd.tx_freq_MHz AS tx_freq_MHz, 
                              
    dbo.LinkEnd.Bitrate_Mbps AS Bitrate_Mbps, 
    dbo.LinkEnd.band AS band, 

/*    dbo.fn_PickList_GetValueByCode('link', 'status',  dbo.Link.status) as  status_name,
                      
    dbo.fn_PickList_GetValueByCode('link', 'GST_type', dbo.Link.calc_method) AS GST_type_name, 
    dbo.fn_PickList_GetValueByCode('link', 'calc_method', dbo.Link.GST_type) AS calc_method_name, 
    dbo.fn_PickList_GetValueByCode('link',  'LOS_status', dbo.Link.LOS_status) AS LOS_status_name,                       
    dbo.fn_PickList_GetValueByCode('link', 'terrain_type', dbo.Link.terrain_type)  AS terrain_type_name, 
    dbo.fn_PickList_GetValueByCode('link', 'underlying_terrain_type', dbo.Link.underlying_terrain_type)   AS underlying_terrain_type_name, 
*/
    
  --  dbo.Status.name AS status_name, 
    
    dbo.ClutterModelType.name AS Clutter_Model_Name 
                      
    
/*    Property1.name AS Property1_name, 
    Property2.name AS Property2_name,


    Property1.georegion_id AS georegion1_id, 
    Property2.georegion_id AS georegion2_id,
                      
    Property1.ground_height AS Property1_ground_height, 
    Property2.ground_height AS Property2_ground_height,
*/

 --Link_Repeater.id as Link_Repeater_id
  

 FROM         dbo.LinkEnd 
    RIGHT OUTER JOIN   dbo.Status 
    RIGHT OUTER JOIN      dbo.Link 
    LEFT OUTER JOIN      dbo.ClutterModelType ON dbo.Link.clutter_model_id =   dbo.ClutterModelType.id ON dbo.Status.id = dbo.Link.status  ON dbo.LinkEnd.id = dbo.Link.linkend2_id
--    LEFT OUTER JOIN dbo.Property Property2 ON dbo.Link.Property2_id = Property2.id 
 --   LEFT OUTER JOIN dbo.Property Property1 ON dbo.Link.Property1_id = Property1.id
            
  --  left OUTER JOIN dbo.Link_Repeater ON dbo.Link.id =  dbo.Link_Repeater.Link_ID
            
WHERE   
   (dbo.Link.objname in ( 'pmp_link',  'pmp'))

go

---------------------------------------------------------------------------------
IF OBJECT_ID(N'[dbo].[view_PMP_TERMINAL]') IS NOT NULL
  DROP VIEW dbo.view_PMP_TERMINAL
GO


CREATE VIEW dbo.view_PMP_TERMINAL
AS
SELECT LinkEnd.*,

       dbo.Property.name AS property_name,
       dbo.Property.lat AS lat,
       dbo.Property.lon AS lon,
       dbo.Property.ground_height AS ground_height,
       
       dbo.LinkEndType.name AS LinkEndType_name,
       
       dbo.PMP_site.name AS PMP_site_name,          
       
       PMP_site.name   AS connected_pmp_site_name, 
       pmp_sector.name AS connected_pmp_sector_name, 
       
       dbo.Property.project_id       
   
  --     dbo.PMP_Sector.name AS PMP_Sector_name
       
FROM LinkEnd 
     LEFT OUTER JOIN dbo.PMP_site ON LinkEnd.pmp_site_id =     dbo.PMP_site.id
     LEFT OUTER JOIN dbo.LinkEnd pmp_sector ON LinkEnd.connected_pmp_sector_id =  pmp_sector.id
     
--     LEFT OUTER JOIN dbo.PMP_Sector ON LinkEnd.pmp_sector_id =     dbo.PMP_Sector.id
     
     LEFT OUTER JOIN dbo.LinkEndType ON LinkEnd.linkendtype_id =     dbo.LinkEndType.id
 --    LEFT OUTER JOIN dbo.Property ON LinkEnd.property_id =     dbo.Property.id
     
     
     LEFT OUTER JOIN dbo.Property ON dbo.LinkEnd.property_id = dbo.Property.id
     

     
     
where 
  LinkEnd.ObjName = 'PMP_terminal'
go


---------------------------------------------------------------------

IF OBJECT_ID(N'[dbo].[view_PMP_site_with_project_id]') IS NOT NULL
  DROP VIEW dbo.view_PMP_site_with_project_id
GO

CREATE VIEW dbo.view_PMP_site_with_project_id
AS
SELECT 
       P.project_id,
       dbo.PMP_site.*
FROM dbo.PMP_site
     LEFT OUTER JOIN dbo.Property P ON dbo.PMP_site.property_id = P.id
go











IF OBJECT_ID(N'pmp.sp_Template_Linkend_Antenna_Copy_to_PMP_Sector_Antenna') IS NOT NULL
  DROP procedure pmp.sp_Template_Linkend_Antenna_Copy_to_PMP_Sector_Antenna
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */

CREATE PROCEDURE pmp.sp_Template_Linkend_Antenna_Copy_to_PMP_Sector_Antenna
(
  @TEMPLATE_LINKEND_ID 	int,
--  @PROPERTY_ID 			int,
  @LINKEND_ID 	     	int 
)    
AS
BEGIN
  SET ANSI_WARNINGS OFF --String or binary data would be truncated.
 

  DECLARE  
    @property_ID int
 --   @PROJECT_ID int; 
   
  
  SELECT 
--  		@PROJECT_ID = PROJECT_ID,
        @property_ID = property_ID 
--  	FROM view_PMP_Site 
  	FROM LINKEND 
    WHERE (ID = @LINKEND_ID) 
  

--------------------------------------------------
-- view_LinkEnd_antennas
--------------------------------------------------    
  insert into Linkend_Antenna
    (
   --  PROJECT_ID,
     Linkend_id,
     property_ID,
     
     name, --objname,
     
     BAND,
     
     HEIGHT, 
     DIAMETER,GAIN,TILT,
 
     POLARIZATION_str,
     AntennaType_ID, 
     vert_width,horz_width,
     loss,
     
     azimuth
     
    )    
   select 
   --  @PROJECT_ID,
     @Linkend_id, 
     @property_ID, 
    
     name,
     BAND,
     
     HEIGHT, 
     DIAMETER,GAIN,TILT,
 
     POLARIZATION_str,
     AntennaType_ID,
     vert_width,horz_width,
     loss,
     azimuth
     
   from 
   	Template_Linkend_Antenna 
   WHERE 
   	Template_Linkend_id=@Template_Linkend_id
   
 
 
END

go

--------------------------------------------------------
IF OBJECT_ID(N'pmp.sp_Template_Linkend_Copy_to_PMP_SITE') IS NOT NULL
  DROP procedure pmp.sp_Template_Linkend_Copy_to_PMP_SITE 
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE pmp.sp_Template_Linkend_Copy_to_PMP_SITE 
(
  @Template_Linkend_ID INT = 1012, 
  @PMP_SITE_ID int = 167
)    
AS
BEGIN
  SET ANSI_WARNINGS OFF --String or binary data would be truncated.

  DECLARE  
    @id_new int,
    @property_ID int;
 --   @PROJECT_ID int; 
 --   @sName VARCHAR(100);

           
   
  SELECT 
--  		@PROJECT_ID = PROJECT_ID,
        @property_ID = property_ID 
--  	FROM view_PMP_Site 
  	FROM PMP_Site 
    WHERE (ID = @PMP_SITE_ID) 
  


 -- if @ID is null
 --   set @ID = 70365
  
  if @Template_Linkend_ID is null
  BEGIN
   SELECT top 1  @Template_Linkend_id =id FROM Template_Linkend
  
--    set @ID = 70365
 --   set @LINKEND_ID =124645
  END

    	
 -- print @ID
  
  INSERT INTO LinkEnd
--  INSERT INTO PMP_Sector
        
      (
      objname,   
 	  PMP_SITE_ID,
      property_ID,
      
       name )
   select 
      'PMP_Sector',   
      @PMP_SITE_ID,
      @property_ID,
      
      name
   from 
   		Template_Linkend     
   where 
   		id = @Template_Linkend_ID;      
    
   set @id_new= IDENT_CURRENT ('LinkEnd')


-----------------------------------------------------------   

Alter table  LinkEnd Disable trigger all

      
 UPDATE LinkEnd 
 SET  
   		band = m.band,
        LinkEndType_ID 		= m.LinkEndType_ID,
        LinkEndType_Mode_ID = m.LinkEndType_Mode_ID,
        Mode 				= m.mode,
        subband				= m.subband,
        bitrate_Mbps    	= m.bitrate_Mbps,
        channel_type        = m.channel_type,
                 
        
      --  MODULATION_count=m.MODULATION_level_count,

        MODULATION_TYPE =m.MODULATION_TYPE,
        SIGNATURE_HEIGHT=m.SIGNATURE_HEIGHT, 
        SIGNATURE_WIDTH =m.SIGNATURE_WIDTH,  
        THRESHOLD_BER_3 =m.THRESHOLD_BER_3,  
        THRESHOLD_BER_6 =m.THRESHOLD_BER_6,  
        
        POWER_MAX       = m.POWER_MAX,
        POWER_dBm       = m.POWER_dBm,
        POWER_W         = m.POWER_W,
        
        rx_freq_MHz = m.rx_freq_MHz,
        tx_freq_MHz = m.tx_freq_MHz,
        
        channel_width_MHz =m.channel_width_MHz,        
        
        cell_layer_id = m.cell_layer_id,
        calc_model_id = m.calc_model_id,
        k0 			= m.k0,
        k0_open 	= m.k0_open,
        k0_closed 	= m.k0_closed,
     
 --   trx_id 		= m.trx_id,
                  
        calc_radius_km = m.calc_radius_km

    --    combiner_id    = m.combiner_id,
     --   combiner_loss  = m.combiner_loss
                
        
        
--       MODULATION_COUNT      = m.MODULATION_level_COUNT,
--       GOST_53363_modulation = m.GOST_53363_modulation,
        
   --     Freq_spacing  =  m.BANDWIDTH * CHANNEL_SPACING    -- calculated        
        
          
       from 
          (select * from Template_Linkend WHERE id=@Template_Linkend_id) m     
       WHERE 
         (LinkEnd.id = @id_new)       
  
 print @@ROWCOUNt
 
Alter table  LinkEnd enable trigger all
 
  
-----------------------------------------------------------   

    
 --	set @id_new= @@IDENTITY;
    
  exec [pmp].sp_Template_Linkend_Antenna_Copy_to_PMP_Sector_Antenna 
       @Template_Linkend_id = @Template_Linkend_ID, 
       @Linkend_ID          = @id_new
    
  RETURN @id_new       
 
END

go

-----------------------------------------------------
IF OBJECT_ID(N'pmp.sp_Template_Pmp_Site_Copy_to_PMP_Site') IS NOT NULL
  DROP procedure pmp.sp_Template_Pmp_Site_Copy_to_PMP_Site
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE pmp.sp_Template_Pmp_Site_Copy_to_PMP_Site
(
  @ID INT,
  @PMP_Site_ID int 
)  
AS
BEGIN
 DECLARE
   @id1 int
 --  @Template_LinkEnd_id int; 



  UPDATE PMP_Site 
     SET 
  	   calc_radius_km  =m.calc_radius_km       
     from 
        (select * from Template_PMP_Site WHERE id=@ID) m     
     WHERE 
        PMP_Site.id=@PMP_Site_ID       

  -------------------------------


/* SELECT *  
    
    FROM Template_LinkEnd
    WHERE Template_pmp_site_id = @ID
*/
  SELECT id  
     INTO #temp
    FROM Template_LinkEnd
    WHERE Template_pmp_site_id = @ID


  WHILE EXISTS(SELECT * FROM #temp)
  BEGIN
    SELECT TOP 1 @id1=id FROM #temp
    DELETE FROM #temp WHERE id=@id1  
    
    exec pmp.sp_Template_Linkend_Copy_to_PMP_SITE 
--               @ID          = @Template_LinkEnd_id, 
               @Template_Linkend_ID   = @id1, 
               @PMP_SITE_ID = @PMP_Site_ID    
               
  END

  
END

go


---------------------------------------------------------


IF OBJECT_ID(N'[dbo].[sp_Pmp_Site_Add]') IS NOT NULL
  DROP procedure dbo.sp_Pmp_Site_Add
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE procedure dbo.sp_Pmp_Site_Add
(
  @PROJECT_ID 	    int=null,   
  @PROPERTY_NAME 	varchar(100)=null,
  @LAT 				float=null,
  @LON 				float=null,
  @LAT_WGS			float=null,
  @LON_WGS			float=null,


  @PROPERTY_ID 	    int,   
  @NAME 			 varchar(100),
  
  @CALC_RADIUS_KM   float=null,   
  @TEMPLATE_PMP_SITE_ID int=null,  -- if not null -> apply !
    
  @IS_GET_INFO 			bit = 0    
)
AS
begin
  -------------------------------------------------------------------------
--  IF @PROPERTY_ID IS NULL  RAISERROR ('@PROPERTY_ID is NULL', 16, 1);

--  IF NOT EXISTS(SELECT * FROM PROPERTY WHERE (ID=@PROPERTY_ID) )
--    RAISERROR ('@PROPERTY_ID is not EXISTS', 16, 1);
  -------------------------------------------------------------------------
  

  DECLARE 
   @new_id int
 --  @project_id int;
 

-- set @project_id = dbo.fn_Property_GetProjectID(@PROPERTY_ID)


--print 'pmp PROJECT_ID' + cast(@PROJECT_ID as VARCHAR(50))

/*
  SELECT @PROJECT_ID = PROJECT_ID 
  	FROM PROPERty  
    WHERE (ID = @PROPERTY_ID) 
    
*/
  IF IsNull(@PROPERTY_ID,0) < 1
  begin
  	INSERT INTO property (project_id,name, lat,lon) VALUES ( @project_id,@PROPERTY_NAME, @lat,@lon) 
    
    set @PROPERTY_ID= IDENT_CURRENT ('property')
   
  end




  INSERT INTO Pmp_Site
     (--PROJECT_ID, 
      NAME, 
      PROPERTY_ID,
      CALC_RADIUS_KM
      )
  VALUES
     (--@project_id, 
      @Name,
      @PROPERTY_ID,
      @CALC_RADIUS_KM
     );
  
  
    
  set @new_id= IDENT_CURRENT ('Pmp_Site')
   
--  set @new_id=@@IDENTITY;
----------------------------------------------------  
   
Alter table  Pmp_Site Disable trigger all



 if @TEMPLATE_PMP_SITE_ID>0 
   exec pmp.sp_Template_PMP_Site_Copy_to_PMP_Site 
   			@ID = @TEMPLATE_PMP_SITE_ID, 
            @Pmp_site_id = @new_id


Alter table  Pmp_Site enable trigger all



  if @IS_GET_INFO=1
    SELECT id,guid,name  FROM Pmp_Site WHERE ID=@new_id
  
  RETURN @new_id; 
      
        
end

go

--------------------------------------

IF OBJECT_ID(N'[dbo].[view_Linkend_base]') IS NOT NULL
  DROP VIEW dbo.view_Linkend_base
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW dbo.view_Linkend_base
AS     
SELECT 
   Linkend.* ,
   
   LinkEndType.name as LinkEndType_name,   
   
   dbo.lib_vendor.name AS Vendor_name, 
   dbo.lib_VENDOR_Equipment.name AS Vendor_Equipment_name 
        
FROM 
   dbo.Linkend
   LEFT OUTER JOIN  dbo.LinkEndType on Linkend.LinkEndType_id = dbo.LinkEndType.id
   LEFT OUTER JOIN
          dbo.lib_VENDOR_Equipment ON dbo.LinkEndType.vendor_equipment_id = dbo.lib_VENDOR_Equipment.id LEFT OUTER JOIN
          dbo.lib_vendor ON dbo.LinkEndType.vendor_id = dbo.lib_vendor.id
   
   
   where 
      IsNull(objname, 'linkend') = 'linkend'

go



-------------------------------------------------------------
IF OBJECT_ID(N'[dbo].[view_PMP_site]') IS NOT NULL
  DROP VIEW dbo.view_PMP_site
GO


CREATE VIEW dbo.view_PMP_site
AS
SELECT P.georegion_id,
       P.project_id,
       P.lat,
       P.lon,
       P.address,
       P.name AS Property_name,
       S.name AS status_str,   
       dbo.PMP_site.*
FROM dbo.PMP_site
     LEFT OUTER JOIN dbo.Status S ON dbo.PMP_site.status_id = S.id
     LEFT OUTER JOIN dbo.Property P ON dbo.PMP_site.property_id = P.id

go



/****** Object:  View [Security].[View_User_Security_XREF]    Script Date: 18.01.2021 14:15:25 ******/
IF OBJECT_ID(N'[dbo].[view_PMP_CalcRegion_PMP_Sites]') IS NOT NULL
  DROP VIEW [dbo].[view_PMP_CalcRegion_PMP_Sites]


GO

CREATE VIEW dbo.view_PMP_CalcRegion_PMP_Sites
AS
SELECT DISTINCT TOP 100 PERCENT dbo.PMP_CalcRegionCells.id AS xref_id,
       dbo.PMP_CalcRegionCells.checked,
       dbo.PMP_CalcRegionCells.pmp_calc_region_id,
       dbo.PMP_site.id,
       dbo.PMP_site.name,
       
       dbo.Property.project_id,
       
       dbo.PMP_site.guid,
       dbo.Property.lat,
       dbo.Property.lon,
       dbo.Property.lat_str,
       dbo.Property.lon_str,
       dbo.PMP_site.property_id
       
FROM dbo.PMP_site
     LEFT OUTER JOIN dbo.Property ON dbo.PMP_site.property_id = dbo.Property.id
     RIGHT OUTER JOIN dbo.PMP_CalcRegionCells ON dbo.PMP_site.id =
     dbo.PMP_CalcRegionCells.pmp_site_id
     
WHERE (dbo.PMP_CalcRegionCells.pmp_site_id > 0) AND
      (dbo.PMP_CalcRegionCells.pmp_sector_id IS NULL)
      
ORDER BY dbo.PMP_site.name

go


-------------------------------------------------------------------------

IF OBJECT_ID(N'dbo.sp_Pmp_Site_Select_Item') IS NOT NULL
  DROP procedure dbo.sp_Pmp_Site_Select_Item 
GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Pmp_Site_Select_Item 
(
	@ID INT 
)    
AS
BEGIN
  -------------------------------------------------------------------------
  IF @ID IS NULL  RAISERROR ('@ID is NULL', 16, 1);
  -------------------------------------------------------------------------
    
  -------------------------------------------------------------------------
  IF OBJECT_ID('view_PMP_sector') is null  RAISERROR ('OBJECT_ID is NULL', 16, 1);
  -------------------------------------------------------------------------
     


  
  SET ANSI_WARNINGS OFF --String or binary data would be truncated.


 -- if @ID is null
 --   set @ID = 70365
  
  if @ID is null
  BEGIN
   SELECT top 1  @id =id FROM PMP_site
  
--    set @ID = 70365
 --   set @LINKEND_ID =124645
  END

    	
  print @ID

  declare @temp table 
  
      (ID          int NOT NULL,              
       guid        uniqueidentifier,
       parent_guid uniqueidentifier,
       
       name        NVARCHAR(100) NOT NULL,
       objname     VARCHAR(10) NOT NULL,
               
       BAND        VARCHAR(10),
            
       LinkEnd_ID 	int,


      ---------------
      --PmpSite sector
      ---------------
       cell_layer_ID  INT,
       cell_layer_name NVARCHAR(50),
      
       Color 						int,
       calc_radius_km		float,
       
       LinkEndType_ID 	int,
       LinkEndType_Name NVARCHAR(50),

       LinkEndType_Mode_ID 	int,

       
       calc_model_Name NVARCHAR(50),
       calc_model_id 	Float,
       k0_open 				Float,
       k0_closed 			Float,
       k0 						Float,
       
       POWER_dBm      Float,    
       POWER_W      	Float,

       THRESHOLD_BER_3 Float,
       THRESHOLD_BER_6 Float,       
       TX_FREQ_MHz    Float,
       RX_FREQ_MHz    Float,             

      LinkEnd_loss        Float,

      ---------------
      --ANTENNA
      ---------------
      AntennaType_ID      int, 
      AntennaType_Name    NVARCHAR(50),

      DIAMETER            Float,
      HEIGHT              Float,   
      AZIMUTH             Float, 
      POLARIZATION_str    VARCHAR(4),
      GAIN                Float,
      TILT                Float,
      vert_width          Float,
      horz_width          Float,
      ANTENNA_loss     Float
      
   --   is_master 					bit --master antenna
               
      )  

--------------------------------------------------
-- pmp_sector
--------------------------------------------------     
  insert into @temp
    (id,guid,name,objname, 
    
     color,
    
     cell_layer_id,cell_layer_name,
     
     calc_radius_km,
    
     LinkEndType_ID,LinkEndType_Name,  
     LinkEndType_Mode_ID,
     
     BAND,
     THRESHOLD_BER_3, THRESHOLD_BER_6, 
     POWER_dBm, 
     POWER_W,
     
     TX_FREQ_MHz, RX_FREQ_MHz,
     LinkEnd_Loss,
     
     calc_model_Name, calc_model_id, 
     k0_open, k0_closed, k0,
     
     LinkEnd_ID
     
     )
   select 
     id,guid,name,
     'pmp_sector',

	 	 color,	

     cell_layer_id,cell_layer_name,
     
     calc_radius_km,

     LinkEndType_ID, LinkEndType_Name, 
     LinkEndType_Mode_ID,


     BAND,
     THRESHOLD_BER_3, THRESHOLD_BER_6, 
     POWER_dBm, 
     dbo.fn_dBm_to_Wt(POWER_dBm),
     
     TX_FREQ_MHz, RX_FREQ_MHz,
  
     passive_element_loss,
     
     
     calc_model_Name, calc_model_id, 
     k0_open, k0_closed, k0,
     
     ID
     
--   from view_PMP_sectors_full      
   from view_PMP_sector
   where pmp_site_id = @ID
     
   
 

--------------------------------------------------
-- antennas
--------------------------------------------------    
  insert into @temp
    (id,guid,parent_guid,name,
    objname,
     BAND,
     HEIGHT, 
     DIAMETER,AZIMUTH,GAIN,TILT,
     POLARIZATION_str,
     AntennaType_ID,AntennaType_NAME,  
     vert_width,horz_width,
     ANTENNA_loss
     
    -- is_master
    )    
   select 
     id,guid,[linkend_guid],name,
     'antenna',  --[LinkEnd.guid]
     BAND,
     
     HEIGHT, 
     DIAMETER,AZIMUTH,GAIN,TILT,
     POLARIZATION_str,
     AntennaType_ID,AntennaType_NAME, 
     vert_width,horz_width,
     loss
--     is_master
     
 --  from view_PMP_sector_antennas 
   from view_LInkend_antennas 
   where LInkend_id  in (select LinkEnd_ID from  @temp)  


  SELECT * FROM @temp                   
 
 
  RETURN @@ROWCOUNT
  
END

go

-------------------------------------------------------------------------
IF OBJECT_ID(N'dbo.sp_Pmp_Site_Update_Item') IS NOT NULL
  DROP procedure dbo.sp_Pmp_Site_Update_Item 
GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Pmp_Site_Update_Item 
(
  @ID 	        INT, 
  @OBJNAME 		varchar(50),

  @CELL_LAYER_ID int=null,
  
  @COLOR 	     int=null,
  @POWER_dBm     Float=null,
  @POWER_W 	     Float=null,

  @TX_FREQ_MHz Float=null,


  @THRESHOLD_BER_3  Float=null, 
  @THRESHOLD_BER_6  Float=null,  
  
  @CALC_RADIUS_KM   Float=null, 
  
  @K0_OPEN 			Float=null,      
  @K0_CLOSED 		Float=null,     
  @K0 				Float=null,             
        
  --ANTENNA
  @HEIGHT           Float=null,   
  @AZIMUTH          Float=null, 
  @TILT             Float=null,

  @LOSS         Float=null 
  
 -- @CELL_LAYER_ID int
) 

AS
BEGIN
  ---------------------------------------------
  IF @ID IS NULL  RAISERROR ('@ID is NULL', 16, 1);
  IF @OBJNAME IS NULL  RAISERROR ('@OBJNAME is NULL', 16, 1);
  ---------------------------------------------


---------------------------------------------
 if @OBJNAME = 'PMP_Sector'
---------------------------------------------
   UPDATE Linkend 
     SET      
        THRESHOLD_BER_3 = COALESCE(@THRESHOLD_BER_3, THRESHOLD_BER_3),
        THRESHOLD_BER_6 = COALESCE(@THRESHOLD_BER_6, THRESHOLD_BER_6),
        TX_FREQ_MHZ     = @TX_FREQ_MHZ,
     
  		Color 		= COALESCE(@Color, Color), 
        POWER_dBm   = COALESCE(@POWER_dBm, POWER_dBm),
                
        CALC_RADIUS_KM= COALESCE(@CALC_RADIUS_KM, CALC_RADIUS_KM),
        
        K0_OPEN		=    COALESCE( @K0_OPEN,    K0_OPEN),
        K0_CLOSED	=    COALESCE( @K0_CLOSED,  K0_CLOSED),
        K0			=    COALESCE( @K0,         K0), 
        
        CELL_LAYER_ID = COALESCE(@CELL_LAYER_ID, CELL_LAYER_ID)
        
     WHERE 
        id=@ID    
        
  ELSE
---------------------------------------------
 if @OBJNAME = 'antenna'
---------------------------------------------
   UPDATE Linkend_Antenna 
     SET 
  		HEIGHT  =COALESCE( @HEIGHT, HEIGHT),
        AZIMUTH =COALESCE( @AZIMUTH, AZIMUTH) ,
        TILT    =COALESCE( @TILT, TILT),  
        LOSS =COALESCE( @LOSS, LOSS)  
     WHERE 
        id=@ID    
  ELSE
    RAISERROR ('@ID is null', 16, 1);

        
  RETURN @@ROWCOUNT
          
END
go

---------------------------------------------------


IF OBJECT_ID(N'dbo.sp_Pmp_Sector_Del') IS NOT NULL
  DROP procedure dbo.sp_Pmp_Sector_Del
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Pmp_Sector_Del 
(
  @ID int
)

AS
BEGIN

/*
  DECLARE @iID int

  DECLARE C CURSOR FOR
    SELECT id
       FROM Link
       WHERE (Pmp_Sector_id = @ID)
  open C
  
  fetch C INTO @iID
  while @@FETCH_STATUS = 0
    begin
      exec sp_Link_Del @iID
      
      fetch C INTO @iID
    end
  close C
  deallocate C*/
  
  /*

UPDATE Pmp_Terminal
  SET pmp_sector_id = NULL,
      pmp_site_id   = NULL
  WHERE pmp_sector_id = @ID*/


--delete from Linkend_Antenna  where PMP_Sector_id =@ID
--delete from PMP_Sector where id=@ID


-------------------------------------------------------------------------
-- delete PMP Links 
-------------------------------------------------------------------------


delete FROM Link
  WHERE @ID in (Linkend1_id, Linkend2_id)


UPDATE Linkend
  SET connected_pmp_sector_id = NULL,
      pmp_site_id   = NULL
  WHERE connected_pmp_sector_id =@ID 
   

delete from Linkend_Antenna
  where Linkend_id =@ID 

delete from PMP_CalcRegionCells
  where pmp_sector_id =@ID 


delete from Linkend 
  where id=@ID 
  
/*
BEGIN TRY  
     { sql_statement | statement_block }  
END TRY  
BEGIN CATCH  
     [ { sql_statement | statement_block } ]  
END CATCH

BEGIN TRANSACTION;  
  
BEGIN TRY  
    -- Generate a constraint violation error.  
    DELETE FROM Production.Product  
    WHERE ProductID = 980;  
END TRY  
BEGIN CATCH  
    SELECT   
        ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_SEVERITY() AS ErrorSeverity  
        ,ERROR_STATE() AS ErrorState  
        ,ERROR_PROCEDURE() AS ErrorProcedure  
        ,ERROR_LINE() AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
  
    IF @@TRANCOUNT > 0  
        ROLLBACK TRANSACTION;  
END CATCH;  
  
IF @@TRANCOUNT > 0  
    COMMIT TRANSACTION;  
  */

/*
-------------------------------------------------------------------------
if exists (select * from Linkend  where id=@ID)    
   RAISERROR ('sp_Pmp_Sector_Del - exists (select * from Linkend  where id=@ID)', 16, 1);
-------------------------------------------------------------------------
  
  */
  
  RETURN @@ROWCOUNT
  
END
  
go

------------------------------------------------------------------------------

IF OBJECT_ID(N'dbo.sp_Pmp_Site_GetSubItemList') IS NOT NULL
  DROP procedure dbo.sp_Pmp_Site_GetSubItemList 
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Pmp_Site_GetSubItemList 
(
	@ID INT
)
AS
BEGIN

  create table [#temp]
      (ID       int NOT NULL,  
       guid  		uniqueidentifier,
       name     VARCHAR(100) NOT NULL,
       objname  VARCHAR(20)  NOT NULL
      ); 
          

--  SELECT id,guid,'PMP_site' as objname
  --       FROM pmp_site
    --     WHERE (id = @aID)

   -- UNION   

 
  SELECT id
    into [#PMP_Sector]
  FROM LinkEnd
  WHERE (pmp_site_id = @ID);


            
  insert into [#temp] (id,guid,NAME,objname)   
    SELECT id,guid,name,'PMP_SECTOR'
      FROM LinkEnd
      WHERE (pmp_site_id = @ID);
    
    
  insert into [#temp] (id,guid,NAME,objname)  
    SELECT id,guid,name,'PMP_SECTOR_ant'
      FROM VIEW_LinkEnd_ANTENNAS
      WHERE  (pmp_site_id =@ID);
        
      
  insert into [#temp] (id,guid,NAME,objname)  
    SELECT id,guid,name,'LINK' 
       FROM LINK
       WHERE 
         LinkEnd1_id IN (SELECT id FROM LinkEnd WHERE pmp_site_id=@ID)
         or
         LinkEnd2_id IN (SELECT id FROM LinkEnd WHERE pmp_site_id=@ID)

--       WHERE (pmp_site_id = @ID)
        
       
  SELECT * FROM [#temp] 
                                        
END

go


-------------------------------------------------------------
IF OBJECT_ID(N'dbo.sp_Pmp_Site_Del') IS NOT NULL
  DROP procedure dbo.sp_Pmp_Site_Del 
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */ 
CREATE PROCEDURE dbo.sp_Pmp_Site_Del 
(
  @ID int
)
AS

BEGIN 

declare 
  @linkends TABLE (id int)
  
  insert into @linkends
    SELECT id 
      FROM LinkEnd  WHERE pmp_site_id = @ID


  DELETE FROM link
    where 
      Linkend1_id in  (SELECT id FROM @linkends)
      or 
      Linkend2_id in  (SELECT id FROM @linkends)
     

  DELETE FROM Linkend_Antenna  
    where Linkend_id in  (select id FROM @linkends)  

--  DELETE FROM pmp_sector  
--    where pmp_site_id = @ID

  DELETE FROM pmp_CalcRegionCells 
    WHERE PMP_Sector_ID in  (select id FROM @linkends)  


  DELETE FROM pmp_CalcRegionCells 
    WHERE (PMP_SITE_ID=@ID) 
            

 -- select * from LinkEnd where PMP_Site_id=@ID

  
  update LinkEnd
    set connected_pmp_sector_id = NULL,
        connected_pmp_site_id   = NULL
    where    
       connected_pmp_sector_id in  (SELECT id FROM @linkends)
  

 -- select * from LinkEnd where connected_pmp_sector_id is not null and  PMP_Site_id=@ID


  delete from LinkEnd where PMP_Site_id=@ID

  delete from Pmp_Site where id=@ID
    
--  if @@ERROR


  RETURN @@ROWCOUNT  
  
    
  --//DELETE FROM pmp_site 
  --  where id in (SELECT id FROM DELETED)

   
 /* delete from link
     WHERE (Pmp_Site_id = @ID)
    
    
  UPDATE Pmp_Terminal
    SET pmp_site_id = NULL,
        pmp_sector_id = NULL
    where PMP_Site_id=@ID


  delete from pmp_CalcRegionCells   where PMP_Site_id=@ID    
  delete from PMP_Sector where PMP_Site_id=@ID
  */
  
  
  
END
go


---------------------------------------------------

IF OBJECT_ID(N'dbo.fn_Property_Get_Length_m ') IS NOT NULL
  DROP FUNCTION dbo.fn_Property_Get_Length_m 
GO


CREATE FUNCTION dbo.fn_Property_Get_Length_m 
(

  @ID1   int,
  @ID2   int

)
RETURNS float
AS
BEGIN

 DECLARE  
   
    @lat1 float,
    @lon1 float,
    
    @lat2 float,
    @lon2 float


  SELECT       
       @LAT1 = lat,
       @LON1 = lon              
  FROM Property
     WHERE (id = @ID1)  


  SELECT       
       @LAT2 = lat,
       @LON2 = lon              
  FROM Property
     WHERE (id = @ID2)   




--  return  geo.fn_AZIMUTH_m(@LAT1, @Lon1, @LAT2, @Lon2)
  return  dbo.fn_AZIMUTH(@LAT1, @Lon1, @LAT2, @Lon2, 'LENGTH')


  /* Function body */
END
go




IF OBJECT_ID(N'dbo.sp_PMP_Terminal_Select_PMP_Sectors_for_Plug') IS NOT NULL
  DROP procedure dbo.sp_PMP_Terminal_Select_PMP_Sectors_for_Plug
GO


/* 
  ����� PMP_Sectors ��� PMP ��������� 
  
created: Alex  
used in: d_PmpTerminal_Plug  
 */
 /* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_PMP_Terminal_Select_PMP_Sectors_for_Plug
( 
  @ID int,
  @PROJECT_ID int = NULL
)
AS
BEGIN
  ---------------------------------------------
--  IF @ID IS NULL  RAISERROR ('@ID is NULL', 16, 1);
  ---------------------------------------------
  DECLARE 
  --  @project_id int,
    @property_id  int;


--if IsNull(@ID,0) > 0
    SELECT 
        @property_id = property_id ,        
    	@project_id = project_id
      FROM view_PMP_TERMINAL --_with_project_id       
      WHERE id = @ID



  IF @project_id is null   RAISERROR ('@project_id is null ', 16, 1);


--fn_Property_Get_Length_m


  SELECT  id,name,pmp_site_id, pmp_site_name, Property_name, band, 
        0 as Length_m,
        cast(0 as bit) as checked,
        
        @id as pmp_Terminal_id
  
    FROM  view_PMP_sector --_full 
    WHERE (project_id=@project_id)
         
         and 
          
          -- (@property_id IS null) or
          (Property_id <> IsNull(@Property_id,0) ) 
          
    ORDER BY pmp_site_name, name
  
  
  RETURN @@ROWCOUNT
    
END

go

----------------------------------------------

IF OBJECT_ID(N'dbo.sp_Pmp_Terminal_Del') IS NOT NULL
  DROP procedure dbo.sp_Pmp_Terminal_Del 
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Pmp_Terminal_Del 
(
  @ID int
)
AS
BEGIN

/*
DELETE FROM link
  where pmp_terminal_id in (SELECT id FROM DELETED)
  
  
DELETE FROM Linkend_Antenna
  where Pmp_Terminal_id in (SELECT id FROM DELETED)  
  
  
DELETE FROM PmpTerminal
  where id in (SELECT id FROM DELETED)
*/


--select * from link_SDB where link1_id = 6626 or link2_id = 6626


--  select *  FROM Link  WHERE @id in (Linkend1_id, Linkend2_id)

--return


  DELETE FROM Link  WHERE @id in (Linkend1_id, Linkend2_id)

  DELETE FROM Linkend_Antenna  where Linkend_id = @ID

  DELETE FROM LinkEnd  WHERE id=@ID
  
         
  RETURN @@ROWCOUNT    
       
END
go

------------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID(N'[dbo].[sp_Template_Pmp_Site_Select_Item]') IS NOT NULL
  DROP PROCEDURE dbo.sp_Template_Pmp_Site_Select_Item
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Template_Pmp_Site_Select_Item 
(
	@ID INT 
)    
AS
BEGIN
  SET ANSI_WARNINGS OFF --String or binary data would be truncated.


 -- if @ID is null
 --   set @ID = 70365
  
  if @ID is null
  BEGIN
   SELECT top 1  @id =id FROM Template_pmp_Site
  
--    set @ID = 70365
 --   set @LINKEND_ID =124645
  END

    	
  print @ID

  create table [#temp]
      (ID          int NOT NULL,              
       guid        uniqueidentifier,
       parent_guid uniqueidentifier,
       
       name        NVARCHAR(100) NOT NULL,
       objname     VARCHAR(10) NOT NULL,
               
       BAND        VARCHAR(10),
            
      ---------------
      --PmpSite sector
      ---------------
       cell_layer_ID  INT,
       cell_layer_name VARCHAR(50),
      
     --  Color 						int,
       calc_radius_km		float,
       
       LinkEndType_ID 	int,
       LinkEndType_Name VARCHAR(50),
       
       calc_model_Name VARCHAR(50),
       calc_model_id 	Float,
       k0_open 				Float,
       k0_closed 			Float,
       k0 						Float,
       
       POWER_dBm      Float,    
       POWER_W      	Float,

       THRESHOLD_BER_3 Float,
       THRESHOLD_BER_6 Float,       
       TX_FREQ_MHz    Float,
     --  RX_FREQ_MHz    Float,             

   --   LinkEnd_loss        Float,

       combiner_name			VARCHAR(50),
       combiner_id 				int,
       combiner_loss    	float,


      ---------------
      --ANTENNA
      ---------------
      AntennaType_ID      int, 
      AntennaType_Name    VARCHAR(50),

      DIAMETER            Float,
      HEIGHT              Float,   
      AZIMUTH             Float, 
      POLARIZATION_str    VARCHAR(4), --s
      GAIN                Float,
      TILT                Float,
      vert_width          Float,
      horz_width          Float,
      ANTENNA_loss     Float,
      
   --   is_master 					bit --master antenna
               
      ) ;      



--------------------------------------------------
-- LinkEnd
--------------------------------------------------     
  insert into [#temp]
    (id,guid,name,objname, 
    
  --   color,
    
     cell_layer_id, cell_layer_name,
     
     calc_radius_km,

     BAND,   
     LinkEndType_ID,LinkEndType_Name,  
     THRESHOLD_BER_3, THRESHOLD_BER_6, 
     POWER_dBm, POWER_W,
     TX_FREQ_MHz, 
     --RX_FREQ_MHz,
  --   LinkEnd_Loss,
     
     calc_model_id, calc_model_Name, 
     k0_open, k0_closed, k0,
     
     combiner_name,
     combiner_id,
     combiner_loss
     
     )
   select 
     id,guid,name,'pmp_sector',

	-- 	color,	

     cell_layer_id, [CellLayer_name],
     
     calc_radius_km,

     BAND,
     LinkEndType_ID, [LinkEndType_name], 
   
     THRESHOLD_BER_3, THRESHOLD_BER_6, 
     
     POWER_dBm, dbo.fn_dBm_to_Wt(POWER_dBm),
     TX_FREQ_MHz, 
     --RX_FREQ_MHz,
    --  passive_element_loss,
     
     
     calc_model_id, [CalcModel_name], 
     k0_open, k0_closed, k0,
     
     [Passive_Component_name],
     combiner_id,
     combiner_loss  --_dB
     
     
   from 
     VIEW_Template_Linkend     
   where 
     template_pmp_site_id = @ID
     

--------------------------------------------------
-- view_LinkEnd_antennas
--------------------------------------------------    
  insert into [#temp]
    (id,guid,parent_guid,name,
     objname,
     BAND,
  
     AntennaType_ID, AntennaType_NAME,  

     HEIGHT, 
     DIAMETER,AZIMUTH,GAIN,TILT,
     POLARIZATION_str,
     vert_width,horz_width,
     ANTENNA_loss
     
    -- is_master
    )    
   select 
     id,guid,[Template_Linkend_guid],name,
     'antenna',  --[LinkEnd.guid]
      BAND,
     
     AntennaType_ID, [AntennaType_name], 

     HEIGHT, 
     DIAMETER,AZIMUTH,GAIN,TILT,
     POLARIZATION_str,
     vert_width,horz_width,
     loss
--     is_master
     
   from 
     view_Template_Linkend_Antenna 
   where 
      template_pmp_site_id = @ID


  SELECT * FROM #temp                                
 
  RETURN @@ROWCOUNT
  
 
END
go


delete from project_tables
  where name in ('LINKFREQPLAN','pmp_site','PMP_Sector','PmpTERMINAL','link_repeater','LINKEND_ANTENNA','LINKEND')
go


IF OBJECT_ID(N'[dbo].[sp_Project_summary]') IS NOT NULL
  DROP PROCEDURE dbo.sp_Project_summary 
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.sp_Project_summary
    @USER_NAME sysname = null
AS
BEGIN
  if @USER_NAME is null  
    set @USER_NAME = USER_NAME()

 
 
 
 SELECT  
   CAST(0 as bit) as checked,


                    (SELECT     COUNT(*)     FROM          property
                     WHERE      project_id = m.id) AS property_count,
                       
                    (SELECT     COUNT(*) 	FROM  PMP_site 
                    	LEFT JOIN dbo.Property P ON dbo.PMP_site.property_id = PMP_site.id                                              
                      WHERE      project_id = m.id) AS pmp_site_count,
                                                                        
                      
                      --view_PMP_site_with_project_id

                    (SELECT     COUNT(*)  FROM   LinkEnd                
     					  LEFT OUTER JOIN dbo.Property ON dbo.LinkEnd.property_id = dbo.Property.id                    
                        WHERE LinkEnd.ObjName = 'PMP_terminal' and project_id = m.id) AS pmp_terminal_count,
                            
                    (SELECT     COUNT(*)        FROM          pmp_calcregion
                      WHERE      project_id = m.id) AS pmp_calcregion_count,
                            
                    (SELECT     COUNT(*)       FROM          link
                      WHERE      project_id = m.id) AS link_count,

                            

                    (SELECT     COUNT(*)    FROM   Link_Repeater 
                       LEFT OUTER JOIN dbo.Property ON dbo.Link_Repeater.property_id = dbo.Property.id                                        
                      WHERE      (project_id = m.id)   ) AS link_Repeater_count,

                           
                    (SELECT     COUNT(*)     FROM    linkline
                      WHERE      project_id = m.id) AS linkline_count,
                      
                    (SELECT     COUNT(*)      FROM    linknet
                      WHERE      project_id = m.id) AS linknet_count,
                       

                    (SELECT     COUNT(*)    FROM    LinkFreqPlan
                        LEFT OUTER JOIN dbo.LinkNet ON dbo.LinkNet.id = dbo.LinkFreqPlan.LinkNet_id
                      WHERE project_id = m.id )  AS linkfreqplan_count,
                            

                    (SELECT     COUNT(*)    FROM  LinkEnd --_with_project_id
                         LEFT OUTER JOIN dbo.Property ON dbo.LinkEnd.property_id = dbo.Property.id   
                      WHERE      project_id = m.id) AS linkend_count,
                            
                    (SELECT     COUNT(*)   FROM  Linkend_Antenna --_with_project_id
                        LEFT OUTER JOIN dbo.Property ON dbo.Linkend_Antenna.property_id = dbo.Property.id   
                      WHERE      project_id = m.id) AS linkend_antenna_count,


                     (SELECT     COUNT(*)        FROM          link_SDB
                        WHERE      project_id = m.id) AS link_SDB_count,
 
                                                        
                      (SELECT     MAX(datetime)    FROM          activity_log
                        WHERE      (project_id = m.id)) AS last_datetime, 
                            
                            
                        M.*
--                            m.user_created
                            
                            
FROM         dbo.Project M


where 
    @USER_NAME = 'dbo' 
  or user_created =  @USER_NAME 
  or id in (select project_id from Security.ft_Projects_allowed (@USER_NAME))
 



END

go

-------------------------------------------------------------------------------------------

IF OBJECT_ID(N'[dbo].[sp_Project_Clear_]') IS NOT NULL
  DROP PROCEDURE dbo.sp_Project_Clear_
GO


GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Project_Clear_ 
(
  @ID int=null
)
AS
BEGIN
  ------------------------------------------------------
 -- IF @ID is null  RAISERROR ('@ID is null', 16, 1);
  ------------------------------------------------------


declare @prop TABLE (id int)



insert into @prop
  select ID from property WHERE project_id = @ID;  


--if @ID is null 



-- SELECT * INTO #Project  FROM Project  WHERE id = @ID



--	set nocount on

--	begin TRANSACTION
	
	
--	ROLLBACK TRANSACTION
	
	
	
	-- CalcRegion --------------------------
--	delete from pmp_CalcRegionCells
--	  where pmp_calc_region_id in
 --      (SELECT id  from pmp_CALCREGION  where Project_ID = @ID);
	-----------------------------------------------------
    -- PMP CALCREGION
	-----------------------------------------------------
   
--	delete from PMP_CalcRegionCells where Project_ID = @ID;
  delete from pmp_CALCREGION      where Project_ID = @ID;
		
  
  delete from Linkend_Antenna    where Property_ID in (SELECT id FROM @prop);    

--	delete from view_Linkend_Antenna_with_project_id where Project_ID = @ID;
  
  

-- ref to PROPERTY
--	delete from Link_Repeater where Project_ID in (SELECT id FROM #Project);


		
	--GSM ----------------------------------
--	delete from CELL          where Project_ID= @ID;
--	delete from SITE          where Project_ID = @ID;
	--delete from LAC           where Project_ID = @ID;
	--delete from BSIC          where Project_ID = @ID;

--	delete from DriveTest     where Project_ID = @ID;

--	delete from FREQPLAN      where Project_ID = @ID;
--	delete from TrafficModel  where Project_ID = @ID;
	
	/*
	delete from CalcRegionCells
	  where calc_region_id in (SELECT id  from CALCREGION  where Project_ID = @ID);
	
	delete from CALCREGION      where Project_ID = @ID;
	*/
	

	--LINKLINE, LINKNET ----------------------------------
	delete from LINKLINE      where Project_ID = @ID;
	delete from LINKNET       where Project_ID = @ID;


	--LINK ----------------------------------
  delete from Link_Repeater 
       where Property_ID in (SELECT id FROM @prop);  
        
--	delete from LINKFREQPLAN  where Project_ID = @ID;
	delete from LINK_sdb       where Project_ID = @ID;

	delete from LINK          where Project_ID = @ID;
	
	--delete from Link_Repeater  where Project_ID = @ID;


	--LINKEND ---------------------------------- 
	delete from LINKEND   where Property_ID in (SELECT id FROM @prop);  
	
	
	--PMP ----------------------------------
--	delete from PMPTERMINAL  -- where Project_ID = @ID;
--       where Property_ID in (SELECT id FROM Property WHERE project_id = @ID);  


--	delete from PMP_SECTOR   -- where Project_ID = @ID;
--       where Property_ID in (SELECT id FROM Property WHERE project_id = @ID);  

	delete from PMP_SITE      where Property_ID in (SELECT id FROM @prop);  
    
    
--	delete from PMP_SITE      where Project_ID = @ID;
	

	--common ----------------------------------
	delete from MSC           where Project_ID = @ID;
	delete from BSC           where Project_ID = @ID;
  
  
--	delete from Link_Repeater 
  --   where property_ID in (SELECT ID from PROPERTY  where Project_ID = @ID);
  

--	SELECT *  from Link_Repeater 
  --   where property_ID in (SELECT ID from PROPERTY  where Project_ID = @ID);
  
  
--  return
  
  
  
	delete from PROPERTY      where Project_ID = @ID;


	delete from RELIEF        where Project_ID = @ID;

	
	delete from CalcMAP       where Project_ID = @ID;
	delete from [MAP]         where Project_ID = @ID;
    

  DELETE FROM Map_XREF
       WHERE map_desktop_id in  (SELECT id FROM Map_Desktops  where Project_ID = @ID)

	delete from Map_Desktops  where Project_ID = @ID; 
	
	delete from FOLDER        where Project_ID = @ID;
		

--	delete from project      where ID in (SELECT id FROM #Project);
	

--	commit TRANSACTION
	
	return 1;

--	set nocount off

END

go

------------------------------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID(N'[dbo].[sp_Project_restore_after_import]') IS NOT NULL
  DROP PROCEDURE dbo.sp_Project_restore_after_import
GO



CREATE PROCEDURE dbo.sp_Project_restore_after_import
(
	@Project_ID int = 1568
)
AS
BEGIN

    

SELECT     dbo.Linkend_Antenna.id, dbo.Linkend_Antenna.antennaType_id
  into #temp

FROM   dbo.Property RIGHT OUTER JOIN
                      dbo.Linkend_Antenna ON dbo.Property.id = dbo.Linkend_Antenna.property_id
where dbo.Property.project_id = @Project_ID
and antennaType_id is not null



declare 
	@id int,
    @antennaType_id int;

  
while exists (select * from #temp)
begin
  select top 1 @id=id, @antennaType_id=antennaType_id from #temp

	exec  sp_Linkend_Antenna_Update_AntennaType @id=@id, @antennaType_id=@antennaType_id


  delete top (1) from #temp
end

    
drop table #temp


END

go



IF OBJECT_ID(N'[dbo].[view_Property_]') IS NOT NULL
  DROP view [dbo].[view_Property_]


GO


GO

CREATE VIEW dbo.view_Property_
AS
SELECT dbo.Property.*,
       dbo.GeoRegion.name AS GeoRegion_Name,
       
       Status_Values_placement.name AS placement_name,
       Status_Values_for_state.name AS state_name,
              
       
       dbo.fn_FormatDegree(lat_wgs,'') as lat_wgs_str_,
       
       (select Count(*) from Linkend_Antenna where Property_id=id) as Linkend_Antenna_count
       
       
       
       
FROM dbo.Property
     LEFT OUTER JOIN dbo.GeoRegion ON dbo.Property.georegion_id =  dbo.GeoRegion.id
     LEFT OUTER JOIN dbo.Status Status_Values_placement ON  dbo.Property.placement_id = Status_Values_placement.id
     LEFT OUTER JOIN dbo.Status Status_Values_for_state ON dbo.Property.state_id  = Status_Values_for_state.id

go
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


----------------------------------------------------------------------
IF OBJECT_ID(N'dbo.sp_Property_Select_Item') IS NOT NULL
  DROP PROCEDURE dbo.sp_Property_Select_Item


GO




/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Property_Select_Item 
(
	@ID INT
)    
AS
BEGIN
  if @ID is null
    set @ID = 3917
  
  print @ID

  declare  @temp table
    (
       ID          int NOT NULL,              
       guid        uniqueidentifier,
       parent_guid uniqueidentifier,
       
       name        NVARCHAR(150) NOT NULL,               
       objname     VARCHAR(30),               
            

      ---------------
      --ANTENNA
      ---------------
      AntennaType_ID      int, 
      ANTENNATYPE_NAME    NVARCHAR(150),

      BAND        		  VARCHAR(10),

      DIAMETER            Float,
      HEIGHT              Float,
      AZIMUTH             Float, 
      POLARIZATION_str    VARCHAR(4),
      GAIN                Float,
      TILT                Float,
      vert_width          Float,
      horz_width          Float,
      ANTENNA_loss     Float,
      
      mast_index int,
      
      date_created datetime
                     
      ) ;      
         



--------------------------------------------------
-- LinkEnd
--------------------------------------------------     
  insert into @temp  
     (id,guid,name,objname, date_created,

      AntennaType_NAME 
     
      )  
   select  
      id,guid,name,'LinkEnd', date_created , 
      LinkEndType_name       
      
   from view_LinkEnd_base      
   where Property_id = @ID


      

  insert into @temp
    (id,guid,name,objname, date_created )
   select 
     id,guid,name,'pmp_terminal' , date_created       
   from view_pmp_terminal      
   where Property_id = @ID

 
--------------------------------------------------
-- pmp_site
--------------------------------------------------        

  insert into @temp
    (id,guid,name,objname, date_created )
   select 
     id,guid,name,'pmp_site', date_created        
   from pmp_site     
   where Property_id = @ID


  insert into @temp
    (id,guid,name,objname, parent_guid  )
   select 
     M.id, M.guid, M.name, 'pmp_sector' , p.guid       
   from view_pmp_sector M
       LEFT OUTER JOIN  pmp_site P ON p.id = m.pmp_site_id    
   where P.Property_id = @ID

/*

  SELECT * FROM @temp                                


return

*/



--------------------------------------------------
-- view_LinkEnd_antennas
--------------------------------------------------    
  insert into @temp
    (
     parent_guid,
    
     id,guid,name, 
     BAND,
     HEIGHT, 
     DIAMETER,AZIMUTH,GAIN,TILT,
     POLARIZATION_str,
     
     AntennaType_ID,AntennaType_NAME, 
     
     vert_width,horz_width,
     ANTENNA_loss,
     mast_index 
    )    
   select 
     m.LinkEnd_guid,
   
/*     case
       when m.LinkEnd_id >0 then L.guid
       when m.pmp_sector_id >0 then S.guid
       when m.pmp_terminal_id >0 then t.guid
       else null
     end,   
*/
   
     m.id, m.guid,m.name,
     m.BAND,
     m.HEIGHT,  
     m.DIAMETER,m.AZIMUTH,m.GAIN,m.TILT,
     m.POLARIZATION_str,
     
     m.AntennaType_ID, m.AntennaType_NAME,
     
     m.vert_width,
     m.horz_width,
     m.loss,
     m.mast_index     
     
   from view_LinkEnd_antennas M   
--       LEFT OUTER JOIN  LinkEnd L    ON L.id = m.LinkEnd_id    
--       LEFT OUTER JOIN  pmp_sector S ON S.id = m.pmp_sector_id    
 --      LEFT OUTER JOIN  pmpterminal T ON T.id = m.pmp_terminal_id    
    
   where m.Property_id = @ID 
/*


  SELECT * FROM @temp                                


return
*/




--------------------------------------------------
-- Link_Repeater
--------------------------------------------------

  insert into @temp
    (id,guid,name,objname )
   select 
     id,guid,name,'Link_Repeater'        
   from Link_Repeater 
   where Property_id = @ID
   

--------------------------------------------------
-- view_Link_Repeater
--------------------------------------------------    
  insert into @temp
    (
     parent_guid,
    
     id,guid,name, 
     BAND,
     HEIGHT, 
     DIAMETER,AZIMUTH,GAIN,TILT,
     POLARIZATION_str,
     AntennaType_ID,AntennaType_NAME, 
     vert_width,horz_width,
     ANTENNA_loss    
    )    
   select 
     L.guid     ,
   
     m.id, m.guid,m.name,
     m.BAND,
     m.HEIGHT,  
     m.DIAMETER,m.AZIMUTH,m.GAIN,m.TILT,
     m.POLARIZATION_str,
     m.AntennaType_ID,
     m.AntennaType_NAME,
     m.vert_width,
     m.horz_width,
     m.loss    
     
   from view_Link_Repeater_antenna M   
       LEFT OUTER JOIN  Link_Repeater L    ON L.id = m.Link_Repeater_id   
    
   where l.Property_id = @ID  --and LinkEnd_id>0



  SELECT * FROM @temp                                

 
END
go


---------------------------------------------------------


IF OBJECT_ID(N'dbo.sp_Linkend_Antenna_Select') IS NOT NULL
  DROP procedure dbo.sp_Linkend_Antenna_Select


GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Linkend_Antenna_Select
(
  @OBJNAME    VARCHAR(50),  
  @ID INT   
)
AS
BEGIN 

/* case ObjNameToType(FObjName) of
  //  otCell:        begin sTable:=view_CELL_ANTENNAS;         sField:=FLD_CELL_ID;      end;
    otLinkEnd:     begin sTable:=view_LINKEND_ANTENNAS;      sField:=FLD_LINKEND_ID;      end;
    otPMPTERMINAL: begin sTable:=view_PMP_TERMINAL_ANTENNAS; sField:=FLD_PMP_TERMINAL_ID; end;
    otPMPSector:   begin sTable:=view_PMP_Sector_ANTENNAS;   sField:=FLD_PMP_Sector_ID;   end;

   // otCell_3G:     begin sTable:=VIEW_CELL_ANTENNAS_3G;   sField:=FLD_CELL_3G_ID;   end;
  else
    raise Exception.Create('procedure TdmAntenna_view.AddAntennas-ObjNameToType(aObjName)');
  end;


  s := Format('SELECT * FROM %s WHERE %s=%d', [sTable, sField, FID]);
//  s := 'SELECT * FROM ' +sTable+ ' WHERE '+ sField + '='
*/

  if @OBJNAME = ''   RAISERROR('@OBJNAME = ''''', 16, 1)
 
/*
  ---------------------------------------------
  IF @OBJNAME='Repeater'
  ---------------------------------------------  
    SELECT * FROM view_LinkEnd_antennas WHERE Repeater_id=@ID     
  ELSE
*/
  ---------------------------------------------
  IF @OBJNAME in ('Linkend', 'pmp_sector', 'pmp_terminal')
  ---------------------------------------------  
    SELECT * 
 --     FROM LinkEnd_antenna 
      FROM view_LinkEnd_antennas 
      WHERE linkend_id=@ID     
  ELSE

/*  ---------------------------------------------
  IF @OBJNAME='pmp_sector'
  ---------------------------------------------  
    SELECT * FROM view_Linkend_ANTENNAS WHERE PMP_Sector_ID=@ID     
  ELSE


*/
  ---------------------------------------------
  IF @OBJNAME='template_site_linkend'
  ---------------------------------------------  
    SELECT * 
      FROM view_Template_Site_LinkEnd_antenna 
      WHERE Template_Site_LinkEnd_ID=@ID     
  ELSE




  ---------------------------------------------
--  IF @OBJNAME='pmp_terminal'
  ---------------------------------------------  
--    SELECT * FROM view_PMP_TERMINAL_ANTENNAS WHERE PMP_TERMINAL_ID=@ID     
--  ELSE
    return -1
    
    
  RETURN @@ROWCOUNT
  
END
go
-----------------------------------------------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */

IF OBJECT_ID(N'[dbo].[sp_Property_GetItemsList_]') IS NOT NULL
  DROP PROCEDURE [dbo].[sp_Property_GetItemsList_]


GO


CREATE PROCEDURE dbo.sp_Property_GetItemsList_
(
  @ID INT
)
AS
BEGIN
  DECLARE  @MyTable TABLE       
          (ID          int NOT NULL,            
           guid        uniqueidentifier,
           name        VARCHAR(100) NOT NULL,
           objname     VARCHAR(50) NOT NULL
           
          ) ;                                



-- create table [#MyTable]


   
INSERT INTO @MyTable  (id,guid,NAME,objname)          
  SELECT id,guid,NAME, 'MSC'       FROM MSC WHERE Property_id=@ID
    UNION
  SELECT id,guid,NAME, 'BSC'       FROM BSC WHERE Property_id=@ID
    UNION
  SELECT id,guid,NAME, 'LINKEND'   FROM LINKEND WHERE Property_id=@ID
--    UNION
--  SELECT id,guid,NAME, 'SITE'      FROM SITE WHERE Property_id=@ID
    UNION
  SELECT id,guid,NAME, 'PMP_SITE'  FROM PMP_SITE WHERE Property_id=@ID
    UNION
  SELECT id,guid,NAME, 'PMP_TERMINAL' FROM view_PMP_TERMINAL WHERE Property_id=@ID
    UNION
  SELECT id,guid,NAME, 'link_repeater' FROM link_repeater WHERE Property_id=@ID
--    UNION
--  SELECT id,guid,NAME, 'site'       FROM Site WHERE Property_id=@ID
  --  UNION
    
--  SELECT cell.id, cell.guid, cell.NAME, 'SiteCell' FROM cell
--    left outer join site on site.id=cell.site_id  WHERE site.Property_id=@ID


       
-- LinkLines --
/*
INSERT INTO @MyTable  (id,guid,NAME,objname)          
  SELECT l.id, l.guid, l.NAME, 'link' 
      FROM Link L
         left join LinkEnd  LinkEnd1 on 
      WHERE (LinkEnd1.Property_id=@ID) or (Property2_id=@ID)
*/      
      --- (linkend1_id IN (SELECT id FROM [#MyTable] WHERE objname='LINKEND' ) ) or
        --    (linkend2_id IN (SELECT id FROM [#MyTable] WHERE objname='LINKEND' ) ) 
                       
            
INSERT INTO @MyTable  (id,guid,NAME,objname)          
  -- antennas --
  SELECT id,guid,NAME, 'LINKEND_ANTenna' 
      FROM LinkEnd_antenna
      WHERE (linkend_id>0) and (Property_id=@ID)
      -- IN (SELECT id FROM [#MyTable] WHERE objname='LINKEND' )

/*
--PMP antennas--                
INSERT INTO @MyTable  (id,guid,NAME,objname)     
  SELECT id,guid,NAME, 'PMP_TERMINAL_ant' 
      FROM LinkEnd_antenna
      WHERE (LinkEnd_id >0) and (Property_id=@ID)
      

--PMP antennas--                
INSERT INTO @MyTable  (id,guid,NAME,objname)     
  SELECT id,guid,NAME, 'PMP_sector_ant' 
      FROM LinkEnd_antenna
      WHERE (LinkEnd_id >0) and (Property_id=@ID)
      */
      

/*
--PMP antennas--                
INSERT INTO @MyTable  (id,guid,NAME,objname)     
  SELECT id,guid,NAME, 'link_repeater_antenna' 
      FROM link_repeater_antenna
      WHERE  (Property_id=@ID)*/
            
      

--       IN (SELECT id FROM [#MyTable] WHERE objname='PMP_TERMINAL' )
            
        
-- PMP links--          

/*       
INSERT INTO [#MyTable]  (id,guid,NAME,objname)     
  SELECT id,guid,NAME, 'link' 
      FROM link
      WHERE pmp_terminal_id IN (SELECT id FROM [#MyTable] WHERE objname='PMP_TERMINAL' )
*/
            
      

/*            
-- Cell --  
INSERT INTO [#MyTable]  (id,guid,NAME,objname)          
  SELECT id,guid,NAME, 'CELL' 
      FROM Cell
      WHERE site_id IN (SELECT id FROM [#MyTable] WHERE objname='SITE' )
      
-- Cell antennas --                
INSERT INTO [#MyTable]  (id,guid,NAME,objname)     
  SELECT id,guid,NAME, 'CELL_ANT' 
      FROM view_Cell_antennas
      WHERE cell_id IN (SELECT id FROM [#MyTable] WHERE objname='cell' )
*/

      
                
   --   linkend1_id = @aID) OR
  
  
   SELECT DISTINCT * FROM @MyTable;       
   
 END

go
----------------------------------------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID(N'[dbo].[sp_Property_Del_]') IS NOT NULL
  DROP PROCEDURE [dbo].[sp_Property_Del_]


GO



/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Property_Del_ 
(
  @ID int = 7997
)
AS

  
BEGIN  
 DECLARE
    @id_ int
  
   /*

  SELECT link.id 
      INTO #link
    FROM link 
      left join linkend L1 on L1.id = link.linkend1_id
      left join linkend L2 on L2.id = link.linkend2_id
      
    WHERE
      (L1.property_id = @Property_id) or
      (L2.property_id = @Property_id) 
*/

  delete from Link 
    where 
      linkend1_id in (select id from [linkend] where property_id=@ID) 
      or
      linkend2_id in (select id from [linkend] where property_id=@ID) 


 
--  delete from Link where (property1_ID=@ID) or (property2_ID=@ID)


  delete from Link_Repeater where property_id=@ID


  delete from Linkend_Antenna where property_id=@ID
  delete from Linkend where property_id=@ID

  delete from BSC      where property_id=@ID
  delete from MSC      where property_id=@ID

--  delete from PMPTERMINAL where property_id=@ID
  delete from PMP_SITE where property_id=@ID


--  delete from SITE where property_id=@ID




  delete from property where id=@ID
  


  IF Exists (select * from property where id=@ID)
     RAISERROR ('record Exists', 16, 1);
  
  
  
  RETURN @@ROWCOUNT
    
  
  
END
go


IF OBJECT_ID(N'[reports].[view_LinkEnd_report]') IS  not  NULL

DROP VIEW [reports].[view_LinkEnd_report]
GO
/****** Object:  View [reports].[view_LinkEnd_report]    Script Date: 16.05.2020 10:54:58 ******/


CREATE VIEW [reports].[view_LinkEnd_report]
AS
SELECT  


L.id,
L.name,

L.band,

L.LinkID,


L.rx_freq_MHz,
L.tx_freq_MHz,

--
--Property
--
dbo.Property.name as Property_name,
dbo.Property.lat as Property_lat,
dbo.Property.lon as Property_lon,

dbo.Property.lat_WGS as Property_lat_WGS,
dbo.Property.lon_WGS as Property_lon_WGS,


dbo.Property.code as Property_code,
dbo.Property.address as Property_address,
dbo.Property.ground_height as Property_ground_height,



--
--LinkEndType
--
       dbo.LinkEndType.name AS [LinkEndType_name],

--         dbo.LinkEndType.name AS [LinkEndType.name],
         
       dbo.LinkEndType_Mode.radiation_class AS radiation_class,
       dbo.LinkEndType_Mode.bitrate_Mbps AS bitrate_Mbps,
         
       --������ ������, ���
       dbo.LinkEndType_Mode.bandwidth AS bandwidth,
       dbo.LinkEndType_Mode.modulation_type,


         
      ant.height  AS antenna_height,
      ant.diameter  AS antenna_diameter,
      ant.polarization  AS antenna_polarization,

      ant.gain  AS antenna_gain,
      
      ant.vert_width  AS antenna_vert_width,
      ant.horz_width  AS antenna_horz_width,

      ant.loss  AS antenna_loss,
      ant.tilt  AS antenna_tilt,

      ant.ground_height  AS antenna_ground_height,

     height_full as antenna_height_full,


IsNull(L.passive_element_loss,0) + ant.loss_full  AS loss_full,


      dbo.fn_PickList_GetValueByCode('linkend', 'redundancy', L.redundancy)    AS redundancy_str,

         
--    L.power_W,
    L.power_dBm,
    
     dbo.fn_dBm_to_Wt(L.power_dBm) AS power_W,          
         
         
         
     L.tx_freq_MHz / 1000 AS tx_freq_GHz,
         
--         dbo.fn_LinkEnd_Azimuth(L.id) AS Azimuth1
     dbo.fn_LinkEnd_Azimuth(L.id, null) AS antenna_Azimuth,
         
     (SELECT MAX(power_max) FROM LinkEndType_Mode mode WHERE mode.LinkEndType_id=L.LinkEndType_id) as power_max,
     
     --dbo.fn_dBm_to_Wt 
     
     L.subband,
     L.channel_type,
     
   /*  freq_max_low,
     freq_min_low,
     freq_max_high,
     freq_min_high,*/
     
 
     IIF(L.channel_type='low',
       cast(freq_min_low as varchar(10)) +'-'+cast(freq_max_low as varchar(10)) ,
       cast(freq_min_high as varchar(10)) +'-'+cast(freq_max_high as varchar(10)) 
     ) as channel_min_max_str
     
                  
         
         
  FROM dbo.LinkEnd L
       LEFT OUTER JOIN dbo.LinkEndType_Mode ON L.LinkEndType_mode_id =  dbo.LinkEndType_Mode.id       
       LEFT OUTER JOIN dbo.LinkEndType_Band ON L.LinkEndType_Band_id =  dbo.LinkEndType_Band.id       

       LEFT OUTER JOIN dbo.Property    ON L.property_id    = dbo.Property.id       
       LEFT OUTER JOIN dbo.LinkEndType ON L.linkendtype_id = dbo.LinkEndType.id
       
      OUTER apply dbo.fn_Linkend_Antenna_info (L.id) ant

GO

IF OBJECT_ID(N'[reports].[View_Link_freq_order]') IS  not  NULL

/****** Object:  View [reports].[View_Link_freq_order]    Script Date: 16.05.2020 10:54:58 ******/
DROP VIEW [reports].[View_Link_freq_order]
GO
/****** Object:  View [reports].[View_Link_freq_order]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------
-- ��������� ������




CREATE VIEW [reports].[View_Link_freq_order]
AS
SELECT 

       LinkEnd1.loss_full        AS LinkEnd1_loss_full, 
       LinkEnd2.loss_full        AS LinkEnd2_loss_full,


--LinkEnd1.antenna_height_full  -  LinkEnd2.antenna_height_full as aaaaaaaaaaaa,


       LinkEnd1.antenna_height_full  AS LinkEnd1_antenna_height_full, 
       LinkEnd2.antenna_height_full  AS LinkEnd2_antenna_height_full, 
       
       LinkEnd1_antenna_tilt_calc = DEGREES (ATAN( (LinkEnd1.antenna_height_full - LinkEnd2.antenna_height_full) / (link.length_km * 1000) )) ,      
       LinkEnd2_antenna_tilt_calc = DEGREES (ATAN( (LinkEnd2.antenna_height_full - LinkEnd1.antenna_height_full) / (link.length_km * 1000) )) ,      

   --    dbo.link.length_km, 


       dbo.link.NAME, 
       dbo.link.project_id, 
       dbo.link.length_km, 
       dbo.link.id, 
       

       LinkEnd1.property_name          AS LinkEnd1_Property_name, 
       LinkEnd1.property_lat           AS LinkEnd1_Property_lat, 
       LinkEnd1.property_lon           AS LinkEnd1_Property_lon, 
       LinkEnd1.property_code          AS LinkEnd1_Property_code, 
       LinkEnd1.property_address       AS LinkEnd1_Property_address, 

       LinkEnd2.property_name          AS LinkEnd2_Property_name, 
       LinkEnd2.property_lat           AS LinkEnd2_Property_lat, 
       LinkEnd2.property_lon           AS LinkEnd2_Property_lon, 
       LinkEnd2.property_code          AS LinkEnd2_Property_code, 
       LinkEnd2.property_address       AS LinkEnd2_Property_address, 

----------------------
-- new
----------------------
       LinkEnd1.property_name          AS Property1_name, 
       LinkEnd1.property_lat           AS Property1_lat, 
       LinkEnd1.property_lon           AS Property1_lon, 

       LinkEnd1.property_lat_wgs           AS Property1_lat_wgs, 
       LinkEnd1.property_lon_wgs           AS Property1_lon_wgs, 


       LinkEnd1.property_code          AS Property1_code, 
       LinkEnd1.property_address       AS Property1_address, 

       LinkEnd2.property_name          AS Property2_name, 
       LinkEnd2.property_lat           AS Property2_lat, 
       LinkEnd2.property_lon           AS Property2_lon, 

       LinkEnd2.property_lat_wgs           AS Property2_lat_wgs, 
       LinkEnd2.property_lon_wgs           AS Property2_lon_wgs,  

       LinkEnd2.property_code          AS Property2_code, 
       LinkEnd2.property_address       AS Property2_address, 

       LinkEnd1.property_ground_height AS Property1_ground_height, 
       LinkEnd2.property_ground_height AS Property2_ground_height, 

----------------------
--  LinkEnd1
----------------------
       LinkEnd1.antenna_gain           AS LinkEnd1_antenna_gain, 
       LinkEnd1.antenna_height         AS LinkEnd1_antenna_height, 
       LinkEnd1.antenna_diameter       AS LinkEnd1_antenna_diameter, 
       LinkEnd1.antenna_polarization   AS LinkEnd1_antenna_polarization, 

       LinkEnd1.linkendtype_name       AS LinkEnd1_LinkEndType_name, 
       LinkEnd1.power_w               AS LinkEnd1_power_W, 
       LinkEnd1.rx_freq_mhz            AS LinkEnd1_rx_freq_MHz, 
       LinkEnd1.tx_freq_mhz            AS LinkEnd1_tx_freq_MHz, 


----------------------
--  LinkEnd2
----------------------
       LinkEnd2.antenna_diameter       AS LinkEnd2_antenna_diameter, 
       LinkEnd2.antenna_polarization   AS LinkEnd2_antenna_polarization, 
       LinkEnd2.antenna_height         AS LinkEnd2_antenna_height, 
       LinkEnd2.antenna_gain           AS LinkEnd2_antenna_gain, 
       LinkEnd2.antenna_tilt           AS LinkEnd2_antenna_tilt, 


       LinkEnd1.property_ground_height AS LinkEnd1_Property_ground_height, 
       LinkEnd2.property_ground_height AS LinkEnd2_Property_ground_height, 
       

/*       LinkEnd1.property_ground_height AS LinkEnd1_Property_ground_height, 
       LinkEnd2.property_ground_height AS LinkEnd2_Property_ground_height, 
*/

       LinkEnd2.rx_freq_mhz            AS LinkEnd2_rx_freq_MHz, 
       LinkEnd2.tx_freq_mhz            AS LinkEnd2_tx_freq_MHz, 
       
       
       LinkEnd2.power_w               AS LinkEnd2_power_W, 

       
       LinkEnd1.antenna_azimuth        AS LinkEnd1_antenna_Azimuth, 
       LinkEnd2.antenna_azimuth        AS LinkEnd2_antenna_Azimuth, 
       LinkEnd2.linkendtype_name       AS LinkEnd2_LinkEndType_name, 
       LinkEnd2.radiation_class        AS LinkEnd2_radiation_class, 
       LinkEnd1.radiation_class        AS LinkEnd1_radiation_class, 
       LinkEnd1.antenna_loss           AS LinkEnd1_antenna_loss, 
       LinkEnd2.antenna_loss           AS LinkEnd2_antenna_loss, 
--       LinkEnd2.power_w                AS LinkEnd2_power_W, 
--       LinkEnd1.power_w                AS LinkEnd1_power_W, 

       LinkEnd1.antenna_tilt           AS LinkEnd1_antenna_tilt, 

       LinkEnd2.antenna_ground_height  AS LinkEnd2_antenna_ground_height, 
       LinkEnd1.antenna_ground_height  AS LinkEnd1_antenna_ground_height, 

       
       LinkEnd2.bitrate_mbps           AS LinkEnd2_bitrate_Mbps, 
       LinkEnd1.bitrate_mbps           AS LinkEnd1_bitrate_Mbps, 
       LinkEnd1.power_dbm              AS LinkEnd1_power_dBm, 
       LinkEnd2.power_dbm              AS LinkEnd2_power_dBm, 
       LinkEnd2.antenna_vert_width     AS LinkEnd2_antenna_vert_width, 
       LinkEnd2.antenna_horz_width     AS LinkEnd2_antenna_horz_width, 
       LinkEnd1.antenna_vert_width     AS LinkEnd1_antenna_vert_width, 
       LinkEnd1.antenna_horz_width     AS LinkEnd1_antenna_horz_width, 
       LinkEnd1.NAME                   AS LinkEnd1_name, 
       LinkEnd2.NAME                   AS LinkEnd2_name, 
       LinkEnd1.bandwidth              AS LinkEnd1_bandwidth, 
       LinkEnd2.bandwidth              AS LinkEnd2_bandwidth, 
       LinkEnd1.band                   AS LinkEnd1_band, 
       LinkEnd2.band                   AS LinkEnd2_band, 
       LinkEnd1.linkid                 AS LinkEnd1_LinkID, 
       LinkEnd2.linkid                 AS LinkEnd2_LinkID, 
       LinkEnd1.modulation_type        AS LinkEnd1_modulation_type, 
       LinkEnd2.modulation_type        AS LinkEnd2_modulation_type,
             


       
       LinkEnd1.subband as LinkEnd1_subband,
       LinkEnd2.subband as LinkEnd2_subband,
       
       LinkEnd1.channel_min_max_str as LinkEnd1_channel_min_max_str,
       LinkEnd2.channel_min_max_str as LinkEnd2_channel_min_max_str
       
        
FROM   dbo.link 
       LEFT OUTER JOIN reports.view_linkend_report AS LinkEnd2 
                    ON dbo.link.linkend2_id = LinkEnd2.id 
       LEFT OUTER JOIN reports.view_linkend_report AS LinkEnd1 
                    ON dbo.link.linkend1_id = LinkEnd1.id 

go

--WHERE
--  ( dbo.link.linkend1_id IS NOT NULL ) 

--and 
 -- dbo.link.id = 253552



if  COL_LENGTH('dbo.Link','SDB_bitrate_priority')  is null
  ALTER TABLE dbo.Link  ADD SDB_bitrate_priority float 
go

if  COL_LENGTH('dbo.Link','SDB_bitrate_no_priority')  is null
  ALTER TABLE dbo.Link  ADD SDB_bitrate_no_priority float 
go



------------------------------------------------------------
IF OBJECT_ID(N'[link].[view_Link_SDB]') IS NOT NULL
  DROP view [link].[view_Link_SDB]


GO

GO

CREATE VIEW link.view_Link_SDB

AS
SELECT     dbo.Link.*, 


  LinkEnd.channel_width_MHz,
  LinkEnd.band,
  LinkEnd.bitrate_Mbps,
  
  
  dbo.fn_PickList_GetValueByCode('link', 'status',  dbo.Link.status) AS status_str 
--lib.link_status

        
        
FROM         dbo.LinkEnd  RIGHT OUTER JOIN
              dbo.Link ON LinkEnd.id = dbo.Link.linkend1_id
              
              
go
--where Link.id = 1684


------------------------------------------------------------
IF OBJECT_ID(N'[link].[sp_Link_SDB_items_SEL]') IS NOT NULL
  DROP procedure [link].[sp_Link_SDB_items_SEL]


GO


--------------------------------------------------------------- */
CREATE procedure link.[sp_Link_SDB_items_SEL]
--------------------------------------------------------------- */

(
  @ID  INT

)
AS
begin
if @id is null
  select top 1 @id=id from link_sdb



 declare @tbl TABLE
   (
     name_sdb varchar(100),

     name varchar(100),
     
     SESR float,
     kng  float,

     SESR_required float,
     kng_required  float,
     
     bandwidth float,
     length_m float,
     
     band varchar(50),
     
     SDB_bitrate_priority float,
     SDB_bitrate_no_priority  float,
    
     status int,
     status_str varchar(50)

 
    
   )
  --, sesr_required,kng_required
 INSERT INTO @tbl (name_sdb, name, sesr,kng,  sesr_required,kng_required,  length_m, band, SDB_bitrate_priority,  SDB_bitrate_no_priority, status_str, bandwidth)

 
   select 'Common-Band', name, sesr,kng, sesr_norm,kng_norm,  length, band, SDB_bitrate_priority,  SDB_bitrate_no_priority, status_str, channel_width_MHz
     from link.view_Link_SDB  where id in (select link1_id from link_sdb where id=@ID)
     
    UNION

   select 'E-Band', name, sesr,kng,  sesr_norm,kng_norm,  length, band, SDB_bitrate_priority,  SDB_bitrate_no_priority, status_str, channel_width_MHz
     from link.view_Link_SDB  where id in (select link2_id from link_sdb where id=@ID)
 
/*

 select
        
        POWER( 10, SUM( LOG10(bandwidth ))) 
        
        
     from @tbl
     */
     
declare 
  @sesr1 float,     
  @sesr2 float, 
  @kng1 float,     
  @kng2 float 
     
  
  select @sesr1=sesr, @kng1=kng from @tbl where name_sdb = 'common-band' 
  select @sesr2=sesr, @kng2=kng from @tbl where name_sdb = 'e-band' 
  
--  select @sesr1
  

------------------------------------------------
 INSERT INTO @tbl (name_sdb, 
 		sesr_required, kng_required, 
        
 		sesr, kng, 
        
        SDB_bitrate_priority,  SDB_bitrate_no_priority
    --    status_str
         )
 
   select 'SDB', 
   		Min(sesr_required),
        Min(kng_required),
        
        
        0.01 * @sesr1 * @sesr2, --  POWER( 10.0, SUM( LOG10( sesr))) ,
        0.01 * @kng1  * @kng2, --  POWER( 10.0, SUM( LOG10( sesr))) ,
       
        
        min(SDB_bitrate_priority),  
        min(SDB_bitrate_no_priority)
        
      --  dbo.fn_PickList_GetValueByCode('link', 'status',  dbo.Link.status) AS status_str 
        
        
     from @tbl

----------------------------------------------------------------

update @tbl
  set status = iif (sesr_required > sesr and kng_required > kng,  1, 0)    
where 
  name_sdb = 'SDB' 

update @tbl
  set status_str = dbo.fn_PickList_GetValueByCode('link', 'status',  status)     
where 
  name_sdb = 'SDB' 


---------------------------------------------------------------

--  select * from @tbl where name_sdb = 'SDB' 


update R
  set 
    r.sesr =  m.sesr,
    r.kng  =  m.kng,

    r.sesr_required =  m.sesr_required,
    r.kng_required  =  m.kng_required,

    r.status  =  m.status
 

from link_sdb R, 
  (select * from @tbl where name_sdb = 'SDB' ) M

where id = @ID 


--  select top 1 @id=id from link.link_sdb

---------------------------------------------------

update @tbl
  set name_sdb = 'SDB (Super Dual Band)'
where 
  name_sdb = 'SDB' 
  
---------------------------------------------------

 
 select * from @tbl


/*

 -----------------------------------------------
  if @ObjName = 'Link_SDB'
  -----------------------------------------------
  BEGIN
   INSERT INTO #temp (id,name,guid,ObjName) 
      SELECT id, 'common band: '+ name, guid, 'Link' --, @parent_guid, P.GeoRegion_ID 
      -- + name
        FROM  link 
        where id in (select link1_id from link.link_sdb where id=@ID)
 
   INSERT INTO #temp (id,name,guid,ObjName) 
      SELECT id, 'E band: '+ name, guid, 'Link' --, @parent_guid, P.GeoRegion_ID 
      -- +name
        FROM  link 
        where id in (select link2_id from link.link_sdb where id=@ID)
 
  

delete from link.Link_SDB
  where id=@ID*/


RETURN @@IDENTITY;

end
go


------------------------------------------------------------
IF OBJECT_ID(N'[link].[sp_Link_SDB_Del]') IS NOT NULL
  DROP procedure [link].[sp_Link_SDB_Del]


GO



--------------------------------------------------------------- */
CREATE procedure link.sp_Link_SDB_Del
--------------------------------------------------------------- */

(
  @ID  INT

)
AS
begin

delete from Link_SDB where id=@ID


RETURN @@IDENTITY;

end

go
-------------------------------------------------------------
IF OBJECT_ID(N'[link].[sp_Link_SDB_Add]') IS NOT NULL
  DROP procedure [link].[sp_Link_SDB_Add]


GO



--------------------------------------------------------------- */
CREATE procedure link.sp_Link_SDB_Add
--------------------------------------------------------------- */

(
  @PROJECT_ID  INT,
  @NAME 		 varchar(100),
  
  @link1_id int,
  @link2_id int

)
AS
begin


  INSERT INTO Link_SDB
  (
    PROJECT_ID,
    NAME,
     
    link1_id,
    link2_id 
  )
  VALUES
  (
    @project_id,
    @NAME,

    @link1_id,
    @link2_id 

  )

RETURN IDENT_CURRENT ('Link_sdb')


end

go
----------------------------------------------------------

IF OBJECT_ID(N'[dbo].[sp_Link_Update]') IS NOT NULL
  DROP PROCEDURE [dbo].[sp_Link_Update]


GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Link_Update
(
  @ID   int,  
  
--  @MODE VARCHAR(50) =null,  --'Calc_Results'
  ------------------------- 


  @Rx_Level_dBm float =null,  
    
  @SESR_NORM float =null,  
  @SESR float =null,  
    
  @KNG_NORM float =null,  
  @KNG float =null,  
 -- @KNG_YEAR float,  
    
  @FADE_MARGIN_DB float =null, 
 
  --@FADE_RESERVE float,  
    
  @ESR_REQUIRED float =null,  
  @BBER_REQUIRED float =null,  
  @ESR_NORM float =null,  
  @BBER_NORM float =null,  
    
  @MARGIN_HEIGHT float =null,  
  @MARGIN_DISTANCE float =null,  
  
  @SESR_SUBREFRACTION float =null,  
  @SESR_RAIN float =null,  
  @SESR_INTERFERENCE float =null,   
  
  @KNG_SUBREFRACTION float =null,  
  @KNG_RAIN float =null,  
  @KNG_INTERFERENCE float =null,   
  
  
    
  @LOS_STATUS int =null,  
  @STATUS int =null,
  @STATUS_back int =null,
  
  @NIIR1998_water_area_percent float =null, 
  
  @CALC_RESULTS_XML TEXT = null,
  
  
  
  @SDB_bitrate_priority float = null,
  @SDB_bitrate_no_priority float = null
  
  -----------------------------------------------------------------
   -- @MODE='Profile_XML'
  -----------------------------------------------------------------
 -- @Profile_XML TEXT = null
    
)

AS
BEGIN

PRINT 0

if not exists (select * from link where id = @ID)
  return -9999
  
--  RAISERROR('id not exists ' )


DECLARE @result int;

 DECLARE
   @linkend1_id int,
   @linkend2_id int ;


 
  UPDATE Link 
  SET 
    Rx_Level_dBm	= COALESCE(@Rx_Level_dBm, Rx_Level_dBm),

    SESR_NORM		=COALESCE(@SESR_NORM, SESR_NORM),
    SESR			=COALESCE(@SESR,      SESR),

    KNG_NORM		=COALESCE(@KNG_NORM, KNG_NORM),
    KNG				=COALESCE(@KNG,      KNG),
   
    FADE_MARGIN_DB	=COALESCE(@FADE_MARGIN_DB, FADE_MARGIN_DB),

    ESR_REQUIRED	=COALESCE(@ESR_REQUIRED,   ESR_REQUIRED),
    BBER_REQUIRED	=COALESCE(@BBER_REQUIRED,  BBER_REQUIRED),
    ESR_NORM		=COALESCE(@ESR_NORM,       ESR_NORM),
    BBER_NORM		=COALESCE(@BBER_NORM,      BBER_NORM),

    MARGIN_HEIGHT	  =COALESCE(@MARGIN_HEIGHT,      MARGIN_HEIGHT),
    MARGIN_DISTANCE	  =COALESCE(@MARGIN_DISTANCE,    MARGIN_DISTANCE),
    
    SESR_SUBREFRACTION=COALESCE(@SESR_SUBREFRACTION, SESR_SUBREFRACTION),
    SESR_RAIN		  =COALESCE(@SESR_RAIN,          SESR_RAIN),
    SESR_INTERFERENCE =COALESCE(@SESR_INTERFERENCE,  SESR_INTERFERENCE),   
  
  
    KNG_SUBREFRACTION	=COALESCE(@KNG_SUBREFRACTION,  KNG_SUBREFRACTION),	
    KNG_RAIN		     	=COALESCE(@KNG_RAIN,           KNG_RAIN	),	     	
    KNG_INTERFERENCE	=COALESCE(@KNG_INTERFERENCE,   KNG_INTERFERENCE	),
  
  
    LOS_STATUS		=COALESCE(@LOS_STATUS,  LOS_STATUS),
    STATUS			=COALESCE(@STATUS,      STATUS),
    STATUS_back		=COALESCE(@STATUS_back, STATUS_back),
     
    NIIR1998_water_area_percent = COALESCE(@NIIR1998_water_area_percent, NIIR1998_water_area_percent),
    
    CALC_RESULTS_XML 	=COALESCE(@CALC_RESULTS_XML, CALC_RESULTS_XML),
     
    SDB_bitrate_priority    = COALESCE(@SDB_bitrate_priority, SDB_bitrate_priority),
    SDB_bitrate_no_priority = COALESCE(@SDB_bitrate_no_priority, SDB_bitrate_no_priority)
    
    

            
  WHERE 
    id = @ID


--return

  
  SET @result= @@ROWCOUNT


  IF @Rx_Level_dBm IS NOT NULL
  BEGIN

    UPDATE Linkend  
      SET 
          Rx_Level_dBm = @Rx_Level_dBm  
      WHERE 
          id in (select linkend1_id from link where id=@ID)
          or
          id in (select linkend2_id from link where id=@ID)
      
  END
    



--  RETURN @result


  RETURN @result


END

go


IF OBJECT_ID(N'[Security].[Users]') IS  NULL
begin

    CREATE TABLE [Security].[Users](
        [id] [int] NOT NULL,
        [Name] [varchar](50) NULL,
        [email] [varchar](50) NULL,
        [comments] [varchar](250) NULL,
        [address] [varchar](200) NULL,
        [phone] [varchar](100) NULL,
        [is_modify_project] [int] NULL CONSTRAINT [DF__Users__is_modify__2A8C3FA1]  DEFAULT ((1)),
        [is_modify_lib] [int] NULL CONSTRAINT [DF__Users__is_modify__29981B68]  DEFAULT ((0)),
     CONSTRAINT [Users_pk] PRIMARY KEY CLUSTERED 
    (
        [id] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]

end
go



IF OBJECT_ID(N'[Security].[GeoRegion]') IS  NULL
begin

    CREATE TABLE [Security].[GeoRegion](
        [id] [int] IDENTITY(0,1) NOT NULL,
        [Name] [varchar](47) NULL,
        [Oblast] [varchar](2) NULL,
        [MI_PRINX] [int] NULL,
        [SP_GEOMETRY] [geometry] NULL,
     CONSTRAINT [russia_obl_pk] PRIMARY KEY CLUSTERED 
    (
        [id] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

end

go
-------------------------------------------------------------------


IF OBJECT_ID(N'[Security].[GeoRegion]') IS  NULL
begin

    CREATE TABLE [Security].[GeoRegion](
        [id] [int] IDENTITY(0,1) NOT NULL,
        [Name] [varchar](47) NULL,
        [Oblast] [varchar](2) NULL,
        [MI_PRINX] [int] NULL,
        [SP_GEOMETRY] [geometry] NULL,
     CONSTRAINT [russia_obl_pk] PRIMARY KEY CLUSTERED 
    (
        [id] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

end

go
-------------------------------------------------------------------

IF OBJECT_ID(N'[Security].[Action]') IS  NULL
begin

    CREATE TABLE [Security].[Action](
        [id] [int] IDENTITY(1,1) NOT NULL,
        [name] [varchar](20) NULL,
        [caption] [varchar](100) NULL,
     CONSTRAINT [Action_type_pk] PRIMARY KEY CLUSTERED 
    (
        [id] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]

end

go

IF OBJECT_ID(N'[Security].[User_Security_XREF]') IS  NULL
begin

    CREATE TABLE [Security].[User_Security_XREF](
        [id] [int] IDENTITY(1,1) NOT NULL,
        [user_id] [int] NOT NULL,
        [Project_ID] [int] NULL,
        [GeoRegion_id] [int] NULL,
        [enabled] [bit] NULL,
        [action_id] [int] NULL,
     CONSTRAINT [User_Security_XREF_pk] PRIMARY KEY CLUSTERED 
    (
        [id] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]


    ALTER TABLE [Security].[User_Security_XREF]  WITH CHECK ADD  CONSTRAINT [User_Security_XREF_fk] FOREIGN KEY([action_id])
    REFERENCES [Security].[Action] ([id])
    ON UPDATE CASCADE
    ON DELETE CASCADE

    ALTER TABLE [Security].[User_Security_XREF] CHECK CONSTRAINT [User_Security_XREF_fk]

    ALTER TABLE [Security].[User_Security_XREF]  WITH CHECK ADD  CONSTRAINT [User_Security_XREF_fk_Users] FOREIGN KEY([user_id])
    REFERENCES [Security].[Users] ([id])
    ON DELETE CASCADE

    ALTER TABLE [Security].[User_Security_XREF] CHECK CONSTRAINT [User_Security_XREF_fk_Users]

    ALTER TABLE [Security].[User_Security_XREF]  WITH CHECK ADD  CONSTRAINT [User_Security_XREF_fk2] FOREIGN KEY([GeoRegion_id])
    REFERENCES [Security].[GeoRegion] ([id])

    ALTER TABLE [Security].[User_Security_XREF] CHECK CONSTRAINT [User_Security_XREF_fk2]

    ALTER TABLE [Security].[User_Security_XREF]  WITH NOCHECK ADD  CONSTRAINT [User_Security_XREF_fk2_Project] FOREIGN KEY([Project_ID])
    REFERENCES [dbo].[Project] ([id])
    ON DELETE CASCADE

    ALTER TABLE [Security].[User_Security_XREF] CHECK CONSTRAINT [User_Security_XREF_fk2_Project]


END

go



GO
/****** Object:  View [Security].[View_User_Security_XREF]    Script Date: 18.01.2021 14:15:25 ******/
IF OBJECT_ID(N'[Security].[View_User_Security_XREF]') IS NOT NULL
  DROP VIEW [Security].[View_User_Security_XREF]


GO

/****** Object:  View [Security].[View_User_Security_XREF]    Script Date: 18.01.2021 14:15:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [Security].[View_User_Security_XREF]
AS
SELECT   ------  Security.Users.Name,

sysusers.name as sysname,
    
 Security.User_Security_XREF.id, Security.User_Security_XREF.user_id, Security.User_Security_XREF.Project_ID, 
                      Security.User_Security_XREF.GeoRegion_id, Security.User_Security_XREF.enabled, dbo.GeoRegion.name AS GeoRegion_name, 
                      dbo.Project.name AS Project_name
FROM         Security.User_Security_XREF LEFT OUTER JOIN
                      dbo.Project ON Security.User_Security_XREF.Project_ID = dbo.Project.id LEFT OUTER JOIN
                      dbo.GeoRegion ON Security.User_Security_XREF.GeoRegion_id = dbo.GeoRegion.id LEFT OUTER JOIN
                      Security.Users ON Security.User_Security_XREF.user_id = Security.Users.id
                      
                      

      left join sysusers on sysusers.uid= Security.Users.id

GO

-------------------------------------------------------------------

IF OBJECT_ID(N'Security.view_Users') IS NOT NULL
  DROP VIEW Security.view_Users
          
GO



CREATE VIEW Security.view_Users
AS

  
   SELECT top 99999 U.*, 
   		sysusers.name as sysname
    
    from Security.users U 
      left join sysusers on sysusers.uid=U.id
      
      
   order by sysusers.name

go

--------------------------------------

IF OBJECT_ID(N'[Security].[ft_Projects_allowed]') IS NOT NULL
  DROP FUNCTION [Security].[ft_Projects_allowed]
          
GO



CREATE FUNCTION Security.ft_Projects_allowed
(
  @username sysname=null
)

RETURNS TABLE 

AS
RETURN 
     
 SELECT 
  Project_ID
FROM 
  Security.View_User_Security_XREF
  
where 
      Project_ID > 0
  and [enabled] = 1 
  and
  
     ( [sysname] = COALESCE( @username, USER_NAME())
     or
     is_member(sysname)=1
     )


go

IF OBJECT_ID(N'[Security].[sp_User_access_SEL]') IS NOT NULL
  DROP procedure [Security].[sp_User_access_SEL]
GO


/*
����� ������ �� ��������� user
*/
CREATE PROCEDURE Security.[sp_User_access_SEL]
(
  @USER_NAME varchar(50)=null
)

AS
BEGIN

--select USER_NAME()
if @USER_NAME is null
  set @USER_NAME=USER_NAME()


if @USER_NAME='dbo'
  select 
    @USER_NAME as user_NAME,  

  	CAST(1 as int) as is_modify_Project,
  	CAST(1 as int) as is_modify_lib

else

  select
    @USER_NAME as user_NAME, 
  	is_modify_Project, 
    is_modify_lib
        
  	from  Security.view_Users   
    where 
    	sysname = @USER_NAME
    
    
  --     id not in (select id from Security.Users) 



--select * from Security.Users


  --------------------------------------------------------
--  if (@ACTION = 'users') or  (@ACTION = 'select_users')
  --------------------------------------------------------
--  BEGIN
   --  INSERT INTO [Users] (name, UNAME, LOGIN)
--        SELECT name, name, suser_sname(sid) 
/*  SELECT uid,name, * --name, suser_sname(sid) as login
    FROM sysusers 
    where hasdbaccess=1 and NAME<>'dbo'  
*/    
--          WHERE (status=2) 
          --and 
            --    (name not IN (SELECT name FROM [Users])) 
      
--    from sysusers 
--     where hasdbaccess=1 and NAME<>'dbo'  



   --  UPDATE [users]
    --   set IsAdmin=1
     --  WHERE uname='dbo' and IsAdmin is null
      
   --  SELECT * FROM [users]
     
 -- END ELSE  		



  /* Procedure body */
END


go


IF OBJECT_ID(N'dbo.sp_LinkEndType_Band_Get_Channel_info') IS NOT NULL
  DROP procedure dbo.sp_LinkEndType_Band_Get_Channel_info


GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_LinkEndType_Band_Get_Channel_info
(
  @LinkEndType_Band_ID int,
  @Channel_NUMBER 	   int,  
  @Channel_TYPE 	   varchar(4),  
  
  @subband	 varchar(50) OUTPUT,
  @TxFreq_MHZ float OUTPUT,  
  @RxFreq_MHZ float OUTPUT
  
)

AS
BEGIN


  DECLARE

  -- @dFreqMin      float,
   @dFreq_high    float,
   @FREQ_MIN_LOW  float,
   @FREQ_MIN_HIGH float,


   @LOW   varchar(50),  
   @HIGH  varchar(50),  
   

   @CHANNEL_WIDTH float,
   @TXRX_SHIFT    float,   
   @dFreq_low     float
   
   ;
 
 
    SELECT 
    
         
          @LOW=LOW, 
          @HIGH=HIGH,
       --  CHANNEL_COUNT,
         
         @FREQ_MIN_LOW = FREQ_MIN_LOW, 
     --    @FREQ_MAX_LOW = FREQ_MAX_LOW,      
         @FREQ_MIN_HIGH = FREQ_MIN_HIGH,
     --    @FREQ_MAX_HIGH = FREQ_MAX_HIGH,
         
         @CHANNEL_WIDTH = CHANNEL_WIDTH, 
         @TXRX_SHIFT    = TXRX_SHIFT 
         
      FROM LinkEndType_band 
      WHERE id=@LinkEndType_band_ID;


if @Channel_TYPE = 'low'
begin
   set @subband = @LOW

    set @dFreq_low =@FREQ_MIN_LOW  + @CHANNEL_WIDTH * (@Channel_NUMBER-1);   
 
   
    set @TxFreq_MHZ= @dFreq_low 
    set @RxFreq_MHZ= @dFreq_low  + @TXRX_SHIFT

end else

if @Channel_TYPE = 'high'
begin
   set @subband = @HIGH
 
    set @dFreq_high=@FREQ_MIN_HIGH + @CHANNEL_WIDTH *  (@Channel_NUMBER-1);     
   
    set @TxFreq_MHZ= @dFreq_high 
    set @RxFreq_MHZ= @dFreq_high  - @TXRX_SHIFT

end 
    
   
  
END
go
--------------------------------------------------------

IF OBJECT_ID(N'dbo.sp_Object_Update_LinkEndType') IS NOT NULL
  DROP procedure dbo.sp_Object_Update_LinkEndType 


GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Object_Update_LinkEndType 
(
  @OBJNAME			varchar(50),
  @ID   				int,  -- Template_Cell ID
  
  @LinkEndType_ID 		int,
  @LinkEndType_Mode_ID 	int,  --,  --= null

  @LinkEndType_band_ID 	int = null,
  @CHANNEL_NUMBER 	    int = null
  
 -- @LinkEndType_mode_ID int=NULL
)
AS
BEGIN





  SET ANSI_WARNINGS OFF --String or binary data would be truncated.



/*
  set @OBJNAME	= 'pmp_terminal'
  set @ID   	= 498 
  
  set @LinkEndType_ID = 168
  set @LinkEndType_Mode_ID  = 1363 
*/


  ---------------------------------------------
  IF @OBJNAME IS NULL  RAISERROR ('@OBJNAME is NULL', 16, 1);
  IF @ID      IS NULL  RAISERROR ('@ID is NULL', 16, 1);
  IF @LinkEndType_ID IS NULL  RAISERROR ('@LinkEndType_ID is NULL', 16, 1);
  ---------------------------------------------
  
 -- IF @Mode IS NULL  RAISERROR ('@Mode is NULL', 16, 1);


  DECLARE 
     @RxFreq_MHZ float,
     @TxFreq_MHZ float,  
  
     @channel_type 	 varchar(4),  
     
     @subband	 varchar(50),  



	 @mode 	  int,
     @result  int,
	 @band 			  varchar(20),
     @freq_MHz 		  float,
     @failure_period  float,
     @equaliser_profit	float;

  --   @iLinkEnd_Type_ID  int ,
    -- @POWER_MAX 		float, 
 
  SELECT 
       @band = Band,
  --     @POWER_MAX 			=  POWER_MAX
       @failure_period 	= failure_period,
       @equaliser_profit = equaliser_profit  
                
    FROM LinkEndType
    WHERE (id = @LinkEndType_ID)   
   
  
  set @freq_MHz = dbo.fn_LinkEndType_GetAveFreq_MHz(@LinkEndType_ID);

--        db_Par(FLD_EQUIPMENT_TYPE,      IIF(rec.LinkEndType_ID>0, DEF_TYPE_EQUIPMENT, DEF_NON_TYPE_EQUIPMENT)),
--  //������������: 0-�������, 1-���������
--  DEF_TYPE_EQUIPMENT     = 0;
--  DEF_NON_TYPE_EQUIPMENT = 1;


---------------------------------------------
  IF @OBJNAME in ('LinkEnd','pmp_terminal','pmp_sector')
---------------------------------------------  
  BEGIN
  
  
    UPDATE LinkEnd SET
        EQUIPMENT_TYPE = 0,		    
          
        LinkEndType_ID	  = @LinkEndType_ID,
      --  LinkEndType_Mode_ID = @LinkEndType_Mode_ID,
--        Mode = @mode,

        BAND 			= @BAND,
        Tx_freq_MHz     = @freq_MHz,
        Rx_freq_MHz     = @freq_MHz,
      
  --   Mode 			  = @Mode,
      

    --  POWER_MAX         = @POWER_MAX,
  --    POWER_dBm         = @POWER_MAX,

   --   Tx_freq_MHz   	  = @freq_MHz,
  --    Rx_freq_MHz   	  = @freq_MHz
          
      failure_period   = @failure_period,
      equaliser_profit = @equaliser_profit      
     
    WHERE (id = @ID)  

   -------------------------------------------
   if (@LinkEndType_band_ID is not null ) 
   -------------------------------------------
   begin
     SELECT @channel_type = channel_type                  
        FROM LinkEnd
        WHERE (id = @ID)   
        
 
          
      exec sp_LinkEndType_Band_Get_Channel_info
            @LinkEndType_Band_ID  = @LinkEndType_band_ID,
            @Channel_NUMBER 	  = @Channel_NUMBER,  
            @Channel_TYPE 	      = @channel_type, 
            
            @subband 	          = @subband OUTPUT,                
            @RxFreq_MHZ = @RxFreq_MHZ OUTPUT,
            @TxFreq_MHZ = @TxFreq_MHZ OUTPUT  
                
print @subband


        UPDATE LinkEnd SET
            LinkEndType_Band_ID = @LinkEndType_Band_ID,		    
            Channel_NUMBER 	  = @Channel_NUMBER,  
            subband 	      = @subband, 
              
            Tx_freq_MHz     = @TxFreq_MHZ,
            Rx_freq_MHz     = @RxFreq_MHZ
          
         
        WHERE (id = @ID)  

   
   end

     


  END ELSE 
  
  
---------------------------------------------
  IF @OBJNAME='Template_linkend'
---------------------------------------------  
  BEGIN
    UPDATE Template_linkend SET
    --    EQUIPMENT_TYPE = 0,		    
          
        LinkEndType_ID	    = @LinkEndType_ID,
     --   LinkEndType_Mode_ID = @LinkEndType_Mode_ID,
     --   Mode = @mode,

        BAND = @BAND, 
        Tx_freq_MHz     = @freq_MHz,
        Rx_freq_MHz     = @freq_MHz
      
  --   Mode 			  = @Mode,
      

    --  POWER_MAX         = @POWER_MAX,
  --    POWER_dBm         = @POWER_MAX,

   --   Tx_freq_MHz   	  = @freq_MHz,
  --    Rx_freq_MHz   	  = @freq_MHz
          
    --  failure_period   = @failure_period,
    --  equaliser_profit = @equaliser_profit      
     
    WHERE (id = @ID) 


  END ELSE
    RETURN -1       


if @LinkEndType_mode_ID > 0
   exec @result = sp_Object_Update_LinkEndType_Mode 
     @OBJNAME=@OBJNAME, 
     @ID=@ID, 
   --  @LinkEndType_ID=@LinkEndType_ID, 
     @LinkEndType_mode_ID=@LinkEndType_mode_ID



  RETURN @@ROWCOUNT   
 

  
 -- exec sp_LinkEnd_Update_Mode @ID=@ID, @Mode=@Mode --, @LinkEndType_mode_ID=@LinkEndType_mode_ID
 
END
go

---------------------------------------------------------

IF OBJECT_ID(N'dbo.sp_Template_Linkend_Add') IS NOT NULL
  DROP procedure dbo.sp_Template_Linkend_Add


GO


CREATE procedure dbo.sp_Template_Linkend_Add
(
  @NAME 						varchar(100),
  @Template_pmp_SITE_ID	int,

  @OBJNAME 					VARCHAR(10), 

  @CELL_LAYER_ID		int =NULL,
  @LINKENDTYPE_ID		int =NULL,

  @CALC_RADIUS_km   float =NULL,
  @CALC_MODEL_ID 		int   =NULL,

  @COMBINER_ID 			int	  =NULL,
  @COMBINER_LOSS		float	=NULL,


  --@BAND 		VARCHAR(10) = null,

  @POWER_dBm	 	float = null, 
  @Tx_Freq_MHz  float = null,
  
   ---------------------------- 
  @IS_GET_INFO bit = 0
  
)
AS
begin    

  DECLARE
	  @new_ID int,

  	@K0 				float,     
		@K0_OPEN 		float,
		@K0_CLOSED 	float;  		


  IF @COMBINER_ID=0 SET @COMBINER_ID=NULL
  IF @CELL_LAYER_ID=0 SET @CELL_LAYER_ID=NULL
  IF @LINKENDTYPE_ID=0 SET @LINKENDTYPE_ID=NULL
  IF @CALC_MODEL_ID=0 SET @CALC_MODEL_ID=NULL


  INSERT INTO Template_Linkend
   (NAME, 
    Template_pmp_SITE_ID,
    
    CELL_LAYER_ID,
    LINKENDTYPE_ID,
    
    CALC_RADIUS_km,
    CALC_MODEL_ID,
    
    POWER_dBm,	
    POWER_max,	
    Tx_Freq_MHz
  --  Rx_Freq_MHz 
    
   )
  VALUES
   (@NAME, 
    @Template_pmp_SITE_ID, 
    
    @CELL_LAYER_ID,
    @LINKENDTYPE_ID,
    
    @CALC_RADIUS_km,
    @CALC_MODEL_ID,
    
    @POWER_dBm,
    @POWER_dBm,
    @Tx_Freq_MHz
     
   ) 
 
     
     
  set @new_id= IDENT_CURRENT ('Template_Linkend')
         
  
 -- set @new_id=@@IDENTITY
 
 
    
  exec sp_Object_Update_CalcModel
        @OBJNAME = 'Template_Linkend',
        @ID = @new_id,
        @CALC_MODEL_ID = @CALC_MODEL_ID
    
  
if ISNULL(@LINKENDTYPE_ID,0) > 0
  
    exec sp_Object_Update_LinkEndType 
       @OBJNAME='Template_linkend', 
       @ID=@new_ID, 
       @LinkEndType_ID=@LinkEndType_ID
   --    @LinkEndType_mode_ID=@LinkEndType_mode_ID
  
  
  if @IS_GET_INFO=1
    SELECT id,guid,name  FROM Template_Linkend WHERE ID=@new_id
  
  RETURN @new_id;    
 
  
end
go

-------------------------------------------------------------------------------


update _objects
  set view_name = 'view_LinkFreqPlan' ,
      isUseProjectID=0
      
  where name = 'LinkFreqPlan'
go

delete from _Objects
  where name='Link_Repeater'
go

insert into _Objects
(name, table_name_initial, display_name, IsUseProjectid, view_name) values
('Link_Repeater','Link_Repeater', '������������', 1, 'view_Link_Repeater')
go


update _objects
  set table_name='view_linkend_with_project_id'
where name='linkend'
go


update _objects
  set table_name='view_PMP_site_with_project_id',
      table_name_initial='view_PMP_site_with_project_id'
where name='PMP_site'
go


IF OBJECT_ID(N'[dbo].[view__object_fields_user]') IS NOT NULL
  DROP view [dbo].[view__object_fields_user]


GO

--------------------------------------------------------------



CREATE VIEW [dbo].[view__object_fields_user]
AS
SELECT TOP 100000 
       dbo._objects.name AS objname,
     --  dbo._objects.table_name,
       
       F.*,
       
       F.type AS field_type,
       F.name AS field_name
       
FROM dbo._object_fields_user F
     LEFT OUTER JOIN dbo._objects ON F.object_id =
     dbo._objects.id

go


-------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
updated: 2016.05.13
--------------------------------------------------------------- */

IF OBJECT_ID(N'[dbo].[sys_sp_Copy_table_record]') IS NOT NULL
  DROP procedure dbo.sys_sp_Copy_table_record
GO


--------------------------------
CREATE PROCEDURE dbo.sys_sp_Copy_table_record
(
  @TABLE_NAME VARCHAR(50)=null,
  @ID     INT=null,
  @NEW_ID INT=null
)
AS
BEGIN

/*  set @TABLE_NAME = 'link'
set @ID = 35455
set @NEW_ID = 58114

*/

  IF OBJECT_ID(@TABLE_NAME) is NULL
    RETURN -1

  DECLARE
    @sql VARCHAR(max),
 --   @exceptions VARCHAR(8000),  
  	@column_names VARCHAR(max);    
       

--  set @TABLE_NAME='link'
  /*
  
  set @exceptions = 
     ',id,name,guid,project_id,folder_id,'+
     'date_created,user_created,'+
     'date_modify,user_modify,'+
     'Linkend_id,PMP_SECTOR_ID,PMP_TERMINAL_ID,property_id,'+
     'pmp_sector_ID,pmp_terminal_ID,'+
     'LINKEND1_ID,LINKEND2_ID,'+
     'property1_id,property2_id,'+
     'length_km,kng_year,' 
*/

  set @column_names=''
   
  SELECT  @column_names=@column_names + ',' + column_name + '=m.' +column_name +  CHAR(13) + CHAR(10)
      --, * 
    FROM [INFORMATION_SCHEMA].[COLUMNS]
    WHERE (TABLE_NAME=@TABLE_NAME) 
        --   and charindex(','+column_name+',', @exceptions)=0
           and LEFT(column_name,1)<>'_'
           and LEFT(column_name,3)<>'---'
           and LEFT(column_name,2)<>'__'
           
           and column_name not in
               ('id','name','guid','project_id','folder_id',               
               'date_created','user_created',
               'date_modify','user_modify',
               'Linkend_id','PMP_SECTOR_ID','PMP_TERMINAL_ID','property_id',
               'pmp_sector_ID','pmp_terminal_ID',
               'LINKEND1_ID','LINKEND2_ID',
               'property1_id','property2_id',
               'length_km','kng_year'  ,
               'ELEVATION_angle'            
               
               )
             
           
           
           
 --   ORDER BY ORDINAL_POSITION  
    
  
    
  set @column_names = SUBSTRING(@column_names, 2, LEN(@column_names))


  set @SQL = 
    'UPDATE :TABLE_NAME set '+  CHAR(10) +
    + @column_names +
    ' from (select * from :TABLE_NAME WHERE id=:ID) m '+   CHAR(10) +
    ' WHERE (:TABLE_NAME.id = :new_id) ' +   CHAR(10) ;


  set @sql=REPLACE(@sql, ':TABLE_NAME', @table_name) 
  set @sql=REPLACE(@sql, ':ID',     CAST(@ID as VARCHAR(50))) 
  set @sql=REPLACE(@sql, ':new_id', CAST(@NEW_ID as VARCHAR(50))) 

--print @SQL
--select @SQL

  EXEC(@sql)

--  set @column_names = SUBSTRING(@column_names, 1, LEN(@column_names)-1)


--print @column_names



END

go



IF OBJECT_ID(N'[ui].[sp_Object_prepare_view]') IS  not  NULL
  DROP PROCEDURE [ui].[sp_Object_prepare_view]
GO
/****** Object:  StoredProcedure [ui].[sp_Object_prepare_view]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ui].[sp_Object_prepare_view]
(
--   @objname varchar(50) = 'PMP_SECTOR'
--   @objname varchar(50) = 'LinkEndType'
--   @objname varchar(50) = 'property'
   @objname varchar(50) = 'template_link'
   
--   @objname varchar(50) = 'PMP_LINK'
--   @objname varchar(50) = 'REL_MATRIX'
)

AS
BEGIN
/*
  if charindex('.',@TABLE_NAME_full)=0
    set @TABLE_NAME_full = 'dbo.'+@TABLE_NAME_full
 */

print @objname


declare
  @object_id int, 
  @columns varchar(max)='',
  @join varchar(max)='',
  @table_name_Initial varchar(50),
  
  @IsUseProjectID bit,
  
  @view_name   varchar(50),
  @table_name  varchar(50)



select 
       @object_id = id,
       @table_name_Initial = table_name_initial , 
       @table_name = table_name,
       @IsUseProjectID = IsUseProjectID,
       @view_name = 'dbo.'+view_name
  from _objects 
  where name=@objname
 

if CHARINDEX ('.',@table_name_initial)=0
  set @table_name_initial = 'dbo.' + @table_name_initial

--CHARINDEX ( expressionToFind , expressionToSearch [ , start_location ] )   


if @table_name_Initial is null
  RAISERROR ('@table_name_Initial is null', 16, 1 )
        
if IsNull(@view_name,'')=''
 set  @view_name = @table_name_Initial


/****** Script for SelectTopNRows command from SSMS  ******/

print @table_name_initial

declare 
 /* @is_project_id bit = IIF( Exists (SELECT *
                             FROM [INFORMATION_SCHEMA].[COLUMNS]
                             where  TABLE_NAME = @table_name_initial  and COLUMN_NAME = 'project_id' ) , 1,0),
*/



  @is_folder_id bit = IIF( Exists (SELECT *
                             FROM [INFORMATION_SCHEMA].[COLUMNS]
                             where
                                 (--TABLE_NAME = @table_name_initial 
                                  -- or 
                                  TABLE_SCHEMA+'.'+TABLE_NAME = @table_name_initial
                                 ) 
                                and COLUMN_NAME = 'folder_id' ) , 1,0)



if object_id(@table_name_Initial) is null
  RETURN -9999999


print @table_name_Initial

select MIN(id)  as id
    into #id
  from  dbo.view__object_fields_enabled
  where 
     objname = @objname
     and isnull(name,'') <> ''
  group by name



--return
set @columns = 'm.id, m.guid, ' + CHAR(10) 

/*
select @IsUseProjectID

return
*/

if @IsUseProjectID=1 
--if @is_project_id=1 
   set @columns = @columns + 'm.project_id,'+ CHAR(10) 

if @is_folder_id=1 
   set @columns = @columns +  'm.folder_id,'+ CHAR(10) 

------------------------------------------------------------------

select id, [name], [type] ,xref_tablename
   into #columns
  from  dbo.view__object_fields_enabled    
  where 
      id in (select * from #id)  and
      (
         Exists (SELECT *  FROM [INFORMATION_SCHEMA].[COLUMNS] 
                 where TABLE_SCHEMA+'.'+TABLE_NAME = @table_name_initial  and COLUMN_NAME =  [name]  )
       or  
         [type] = 'button' 

       or  
         [name] in ('lat','lon','lat1','lat2','lon1','lon2','linkendtype_name','band'  )  


      )
   
insert into #columns    
  select id, [name], [type] ,xref_tablename
    from dbo._object_fields_user
    where 
       object_id = @object_id 
       and IsNull(type,'') <> ''
       and [type]<>'delimiter'
   
  
/*  
select * from  #columns  
  
return
*/
   
/*

  select id, [name], [type] ,xref_tablename
    from dbo._object_fields_user
    where object_id = @object_id 

return  
*/
      
------------------------------------------------------------------


select 
   @columns = @columns + 'm.'+ [name] +
--   @columns = @columns + IIF(@columns='','',',')+ 'm.'+ [name] +
     case
        when type='list' then   replace( ', tbl_:name_'+ cast(id as varchar(10)) + '.value as :name_str',  ':name',  [name]  )          
   ---     when type='xref' then    ', '+ [xref_tablename]+ '.name as '+ [name]+'_str'     
        when type='xref' then   replace (replace ( ', :table_:id.name as '+ [name]+'_str'  , 
                                   ':id',    cast(id as varchar(10)) ),       
                                   ':table', xref_tablename)     
        
        else ''
      end  + ','+ CHAR(10) , 

   @join = @join +
     case
        when type in ('list','status') then 
         replace (replace (replace (' left join lib.ft_PickList ('':table'','':column'') tbl_:column_:id  on tbl_:column_:id.id = m.:column ',  
               ':table', @table_name_Initial  ),             
               ':id', cast(id as varchar(10))  ),             
               ':column', [name]) + CHAR(10) 

        when type ='xref' then 
          replace (replace (replace (' left join :table :table_:id on :table_:id.id = m.:column ',  
               ':id',    cast(id as varchar(10)) ),                    
               ':table', xref_tablename),                    
               ':column', [name]) + CHAR(10) 

        else ''
      end 


  from #columns 
/*  
  dbo.view__object_fields_enabled
  where 
      id in (select * from #id)  and
      (
         Exists (SELECT *  FROM [INFORMATION_SCHEMA].[COLUMNS] where TABLE_NAME = @table_name_initial  and COLUMN_NAME =  [name]  )
       or  
         [type] = 'button' 

       or  
         [name] in ('lat1','lat2','lon1','lon2')  


      )
      
*/      
--     objname = @objname
  --   and isnull(name,'') <> ''

if Charindex('DATE_CREATED', @columns)=0 -- and Charindex(@columns, 'DATE_CREATED')=0
  set @columns =  @columns + ' m.DATE_CREATED, m.DATE_MODIFY, m.USER_CREATED, m.USER_MODIFY' + CHAR(10) 
else
  set @columns =  @columns + '  m.USER_CREATED, m.USER_MODIFY' + CHAR(10) 



--set @columns =  @columns + ' m.DATE_CREATED, m.DATE_MODIFY, m.USER_CREATED, m.USER_MODIFY' + CHAR(10) 
--set @columns = 'm.id, ' + CHAR(10)+ @columns + CHAR(10) + ', m.DATE_CREATED, m.DATE_MODIFY, m.USER_CREATED, m.USER_MODIFY' + CHAR(10) 


--select @columns

--return


print @columns

--print @join

--

declare 
  @ui_view_name varchar(100) = 'ui.view_' + @objname 
--  @ui_view_name varchar(100) = 'ui.view_' + @table_name_Initial 
  
declare  
  @view varchar(max) = 'create view '+ @ui_view_name + CHAR(10)+ ' as '+ CHAR(10)+ ' select ' + @columns + ' from ' + @view_name + ' as m ' + CHAR(10) + @join
  
  
--  select @view



if object_id(@ui_view_name) is not null
   exec (' drop view '+ @ui_view_name)


 exec ( @view)


--print @view

--exec ( 'select top 1 * from '+@ui_view_name)


END

GO
/****** Object:  StoredProcedure [ui].[sp_Object_prepare_view_ALL]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[ui].[sp_Object_prepare_view_ALL]') IS  not  NULL

DROP PROCEDURE [ui].[sp_Object_prepare_view_ALL]
GO
/****** Object:  StoredProcedure [ui].[sp_Object_prepare_view_ALL]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ui].[sp_Object_prepare_view_ALL]
/*(
   --@objname varchar(50) = 'link'
   
    history.sp_Create_triggers_all 
)
*/
AS
BEGIN
  DECLARE 
    @RC int,
    @objname varchar(50)

declare 
  @tbl TABLE (objname varchar(50))


insert into @tbl (objname) values
('Antenna_Type'), 
('Cell_Layer') ,
('Clutter_Model'), 
('COLOR_SCHEMA'), 

('Calc_Model'), 
('TRX'), 
('Company'), 

('Combiner'), 
('terminal_type'), 

('pmp_site'), 
('PMP_SECTOR'), 
('PMP_terminal'), 

('property'), 
('link'), 
--('PMP_LINK'), 


('linkend'),  
('PROJECT'), 
('LinkEndType'), 
('LINKEND_antenna'), 
('linkline'), 
('Link_Repeater'),	
('link_sdb') ,
('LinkFreqPlan'), 

('map'), 

('REL_MATRIX'), 


('linknet'), 
('MSC'), 
('BSC'), 


('Link_Type'), 




('PMP_Calc_Region'),
('REPORT'),

('GEO_REGION'),

('template_link'),

('template_pmp_site'), 
('template_linkend'),
('template_linkend_antenna')





/*

select * from _objects


*/

while exists (SELECT * from @tbl)
begin
  select top 1 @objname = objname from @tbl
  delete top (1) from @tbl


  EXEC  [ui].[sp_Object_prepare_view]  @objname = @objname

end

/*
  EXEC @RC = [ui].[sp_Object_prepare_view] 
    @objname = 'link';

  EXEC @RC = [ui].[sp_Object_prepare_view] 
    @objname = 'linkend';

  EXEC @RC = [ui].[sp_Object_prepare_view] 
    @objname = 'property';
*/

END

GO

EXEC  dbo._UpdateAllViews ;
go

exec [ui].[sp_Object_prepare_view_ALL]
go

update _db_version_
set version='2020.01.18'
go


