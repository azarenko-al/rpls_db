

IF OBJECT_ID(N'dbo.sp_Link_Repeater_Antenna_Add') IS not  NULL
  DROP PROCEDURE dbo.sp_Link_Repeater_Antenna_Add
GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE procedure dbo.sp_Link_Repeater_Antenna_Add
(    
  @Link_Repeater_ID int,
  @Name VARCHAR(50), 
 
  -----------------------------------

  @ANTENNATYPE_ID		int =null,     

  @HEIGHT  	float=null, 
  @AZIMUTH 	float=null, 
  @TILT 	float=null,
  @LOSS  	float=null,

  -----------------------------------
  -- Custom
  -----------------------------------
  @BAND 		varchar(50)=null, 
  @Freq_MHz 	float=null,
  @GAIN	    	float=null,          --_dBi
  @DIAMETER   	float=null,      
  @VERT_WIDTH 	float=null,
  @HORZ_WIDTH 	float=null,
  @POLARIZATION_STR varchar(4)=null,
  


 @SLOT int
-- @DISPLAY_ORDER int

--  @REPEATER_ID 	    int =null


--@LOCATION_TYPE int = null


--@LAT 		float=null,
--@LON 		float=null,


)
AS
begin
  DECLARE 
    --@sName VARCHAR(100),
    
    @count int, 
    @new_id int, 
    @pmp_site_id int,
    
  --  @project_id  int, 
    @property_id int,         
           
    @LAT 		float,
    @LON 		float,
    @LOCATION_TYPE int
         
   
 
 --    @aLat float,
  --   @aLon float
     
  --258929
  
    -- Name     : ����������� � ���� � ��������� before insert
  -- Address  : ����������� � ���� � ��������� before insert
  


  SELECT 
      @property_id=PROPERTY_ID
        -- @project_id = project_id  
    FROM Link_Repeater 
    WHERE id = @Link_Repeater_ID

  IF (@property_id IS NULL)  
      RAISERROR ('RAISERROR - @PROPERTY_ID is NULL', 16, 1);




  SELECT 
    --  @project_id=PROJECT_ID, 
      @lat=LAT, 
      @lon=LON
    FROM Property  
    WHERE id = @property_id 

--SET @LOCATION_TYPE = 0;



--     (@PMP_SECTOR_ID IS NULL) and 
  --   (@PMP_TERMINAL_ID IS NULL)
  
  -- RAISERROR ('@LINKEND_ID,@PMP_SECTOR_ID,@PMP_TERMINAL_ID is NULL', 16, 1);
    

  if  (@ANTENNATYPE_ID  is not null )
  BEGIN
    SELECT 
      @BAND        = BAND,  

      @Gain        = Gain,  
      @Freq_MHz    = Freq,
--      @Freq_MHz    = Freq_MHz,
      @Diameter    = Diameter,
      @Polarization_STR= Polarization_STR,
      @Vert_Width  = Vert_Width,
      @Horz_Width  = Horz_Width
    FROM AntennaType 
    WHERE id = @ANTENNATYPE_ID      
  END



  INSERT INTO Link_Repeater_Antenna
     (
      NAME, 

      Link_Repeater_ID,
      
       HEIGHT ,   
      
    --  property_id,      
  --    project_id,
      
    --  DISPLAY_ORDER,
      SLOT
      
    --  PROPERTY_ID
      )
  VALUES
     (
      @NAME,      

      @Link_Repeater_ID,
      @HEIGHT, 
      
    --  @property_id,
    --  @project_id,
      
   --   @DISPLAY_ORDER,
      @SLOT
        
    --  @property_id
      ) 



  --set @new_id = @@IDENTITY
  
  set @new_id= IDENT_CURRENT ('Link_Repeater_Antenna')   
    


  UPDATE Link_Repeater_ANTENNA
  set    
      ANTENNATYPE_ID=@ANTENNATYPE_ID,  
     
--      HEIGHT     =@HEIGHT, 
      GAIN		=@GAIN, 

      BAND		=@BAND,
      FREQ_MHz	=@FREQ_MHz,
      
      LOSS	=@LOSS,  
      DIAMETER	=@DIAMETER, 
      VERT_WIDTH=@VERT_WIDTH, 
      HORZ_WIDTH=@HORZ_WIDTH, 
      
      POLARIZATION_STR=@POLARIZATION_STR,
      
      AZIMUTH    =@AZIMUTH,
      TILT       =@TILT, 
      
      LAT           =@LAT,  
      LON           =@LON, 
      LOCATION_TYPE =@LOCATION_TYPE
     
  WHERE id=@new_id



  RETURN @new_id;

        
end


go



IF OBJECT_ID(N'[dbo].[view_Link_Repeater_with_project_id]') IS NOT NULL
  DROP VIEW dbo.view_Link_Repeater_with_project_id
GO


CREATE VIEW dbo.view_Link_Repeater_with_project_id
AS
SELECT dbo.Link_Repeater.*,
       dbo.Property.project_id
       
FROM dbo.Link_Repeater
     LEFT OUTER JOIN dbo.Property ON dbo.Link_Repeater.property_id =  dbo.Property.id

go
-------------------------------------------------------




--------------------------------------------
IF OBJECT_ID(N'[dbo].[view_Link_repeater]') IS NOT NULL
  DROP VIEW dbo.view_Link_repeater
GO


CREATE VIEW dbo.view_Link_repeater
AS
SELECT 
  dbo.Link_Repeater.*,
  
  dbo.Property.Project_id,


  dbo.Property.lat,
  dbo.Property.lon,
  dbo.Property.ground_height,

A1.height as Antenna1_height,
A2.height as Antenna2_height,

A1.loss as Antenna1_loss,
A2.loss as Antenna2_loss,


A1.gain as Antenna1_gain,
A2.gain as Antenna2_gain,


A1.azimuth as azimuth1,
A2.azimuth as azimuth2,



A1.polarization_str as polarization1,
A2.polarization_str as polarization2,


A1.diameter as diameter1,
A2.diameter as diameter2,


A1.height as height1,
A2.height as height2,

A1.loss as loss1,
A2.loss as loss2,


A1.gain as gain1,
A2.gain as gain2

    

      
FROM

  dbo.Link_Repeater  
  LEFT OUTER JOIN dbo.Property ON (dbo.Link_Repeater.property_id = dbo.Property.id) 
  
  LEFT OUTER JOIN dbo.Link_Repeater_Antenna A1 ON (A1.Link_Repeater_id = dbo.Link_Repeater.id) and  (A1.slot=1)  
  LEFT OUTER JOIN dbo.Link_Repeater_Antenna A2 ON (A2.Link_Repeater_id = dbo.Link_Repeater.id) and  (A2.slot=2)
go


----------------------------------------


IF OBJECT_ID(N'dbo.view_Link_repeater_antenna') IS not  NULL
  DROP VIEW dbo.view_Link_repeater_antenna
GO


CREATE VIEW dbo.view_Link_repeater_antenna
-- 
--     
-- 
AS
SELECT TOP 100 PERCENT 
  dbo.Link_Repeater_Antenna.*,
  dbo.AntennaType.name AS AntennaType_name,
  dbo.AntennaType.band AS AntennaType_band
FROM
  dbo.Link_Repeater_Antenna
  LEFT OUTER JOIN dbo.Link_Repeater ON (dbo.Link_Repeater_Antenna.Link_Repeater_id = dbo.Link_Repeater.id)
--  LEFT OUTER JOIN dbo.Property ON (dbo.Link_Repeater.property_id = dbo.Property.id)
  LEFT OUTER JOIN dbo.AntennaType ON (dbo.Link_Repeater_Antenna.antennaType_id = dbo.AntennaType.id)
  
  ORDER BY
  dbo.Link_Repeater_Antenna.slot


go


-------------------------------------------------------------------------------------------------------------


IF OBJECT_ID(N'dbo.sp_Link_Set_link_repeater') IS not  NULL
  DROP PROCEDURE dbo.sp_Link_Set_link_repeater
GO



/* ---------------------------------------------------------------
 --------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Link_Set_link_repeater

(
  @ID int, 
  @LINK_repeater_ID int 
)
AS
BEGIN

  DECLARE 
    @result int

  DECLARE
    @len float, 
   
    @LINK_ID int,
    @property_rep_ID int,
    @property2_ID int,
    @property1_ID int ; 
  
-- IF @PROPERTY_ID IS NULL  RAISERROR ('@PROPERTY_ID is NULL', 16, 1);

if @LINK_repeater_ID=0 
  set @LINK_repeater_ID=null


  if not Exists (select * from link where id=@id)
    RAISERROR ('@link not exists', 16, 1);


  if not Exists (select * from Link_Repeater where ID = @LINK_repeater_ID)
    RAISERROR ('@Link_Repeater not exists', 16, 1);


--select * from link where id=@id


    UPDATE Link_Repeater  
      SET 
        LINK_ID=NULL,
        
        profile_XML_1=NULL,
        profile_XML_2=NULL,
        
        calc_results_xml_1=NULL,
        calc_results_xml_2=NULL,
        calc_results_xml_3=NULL
    
    
    WHERE  LINK_ID = @ID



----------------------------------
----------------------------------

----------------------------------
  if @LINK_repeater_ID is not null 
----------------------------------  
  begin   

    SELECT       
       @property1_ID  = l1.property_ID,
       @property2_ID  = l1.property_ID
     FROM link
       left join linkend L1 on L1.id=link.linkend1_id
       left join linkend L2 on L2.id=link.linkend2_id
     
     WHERE (link.id = @ID)   
   

   
 
    SELECT 
         @property_rep_ID=property_ID,
         @LINK_ID = LINK_ID
      FROM Link_Repeater
      WHERE (id = @LINK_repeater_ID)      
      
      
      
   set @len =  dbo.fn_Property_Get_Length_m (@property1_ID, @property_rep_ID);
   IF @len<50   RAISERROR (' @len<50 @PROPERTY1_ID is NULL', 16, 1);
     
   set @len =  dbo.fn_Property_Get_Length_m (@property2_ID, @property_rep_ID);
   IF @len<50  RAISERROR (' @len<50 @PROPERTY2_ID is NULL', 16, 1);

     


      print @property_rep_ID  
        
      
      IF @property1_ID IS NULL  RAISERROR ('@property1_ID is NULL', 16, 1);
      IF @property2_ID IS NULL  RAISERROR ('@property2_ID is NULL', 16, 1);

      
      if @property_rep_ID = @property1_ID or 
         @property_rep_ID = @property2_ID 
       return -1     
      
  
    ----------------------------------
 --   UPDATE Link           SET has_repeater=1             WHERE (id = @ID)
    UPDATE Link_Repeater  SET LINK_ID=@ID  WHERE (id = @LINK_repeater_ID)

        
  end
  

 set @result = @@ROWCOUNT


exec sp_Link_Update_Length_and_Azimuth @id = @id


  exec sp_Link_ClearCalc @ID=@ID

  return @result


END
go

--------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID(N'dbo.sp_Link_repeater_Update') IS not  NULL
  DROP PROCEDURE dbo.sp_Link_repeater_Update
GO



/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Link_repeater_Update
(
  @ID   int,  
  
--  @MODE VARCHAR(50) =null,  --'Calc_Results'
  ------------------------- 
  
  @CALC_RESULTS_XML_1 TEXT = null,
  @CALC_RESULTS_XML_2 TEXT = null,
  @CALC_RESULTS_XML_3 TEXT = null
  
    
)

AS
BEGIN


 
  UPDATE Link_repeater 
  SET 
     
    CALC_RESULTS_XML_1 = COALESCE(@CALC_RESULTS_XML_1, CALC_RESULTS_XML_1),
    CALC_RESULTS_XML_2 = COALESCE(@CALC_RESULTS_XML_2, CALC_RESULTS_XML_2),
    CALC_RESULTS_XML_3 = COALESCE(@CALC_RESULTS_XML_3, CALC_RESULTS_XML_3)
             
  WHERE id = @ID

 
    

  RETURN @@ROWCOUNT


END
go
