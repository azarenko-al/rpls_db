IF schema_id ('link_calc') IS NULL
  exec ('CREATE SCHEMA [link_calc]')
GO

IF schema_id ('link_climate') IS NULL
  exec ('CREATE SCHEMA [link_climate]')
GO

IF schema_id ('LinkFreqPlan') IS NULL
  exec ('CREATE SCHEMA [LinkFreqPlan]')
GO

IF schema_id ('geo') IS NULL
  exec ('CREATE SCHEMA [geo]')
GO

IF schema_id ('history') IS NULL
  exec ('CREATE SCHEMA [history]')
GO

IF schema_id ('ui') IS NULL
  exec ('CREATE SCHEMA [ui]')
GO

IF schema_id ('lib') IS NULL
  exec ('CREATE SCHEMA [lib]')
GO

IF schema_id ('map') IS NULL
  exec ('CREATE SCHEMA [map]')
GO

