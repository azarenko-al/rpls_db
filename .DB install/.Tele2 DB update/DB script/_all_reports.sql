


IF OBJECT_ID(N'[reports].[view_LinkEnd_report]') IS  not  NULL

DROP VIEW [reports].[view_LinkEnd_report]
GO
/****** Object:  View [reports].[view_LinkEnd_report]    Script Date: 16.05.2020 10:54:58 ******/


CREATE VIEW [reports].[view_LinkEnd_report]
AS
SELECT  


L.id,
L.name,

L.band,

L.LinkID,


L.rx_freq_MHz,
L.tx_freq_MHz,

--
--Property
--
dbo.Property.name as Property_name,
dbo.Property.lat as Property_lat,
dbo.Property.lon as Property_lon,

dbo.Property.lat_WGS as Property_lat_WGS,
dbo.Property.lon_WGS as Property_lon_WGS,


dbo.Property.code as Property_code,
dbo.Property.address as Property_address,
dbo.Property.ground_height as Property_ground_height,



--
--LinkEndType
--
       dbo.LinkEndType.name AS [LinkEndType_name],

--         dbo.LinkEndType.name AS [LinkEndType.name],
         
       dbo.LinkEndType_Mode.radiation_class AS radiation_class,
       dbo.LinkEndType_Mode.bitrate_Mbps AS bitrate_Mbps,
         
       --������ ������, ���
       dbo.LinkEndType_Mode.bandwidth AS bandwidth,
       dbo.LinkEndType_Mode.modulation_type,


         
      ant.height  AS antenna_height,
      ant.diameter  AS antenna_diameter,
      ant.polarization  AS antenna_polarization,

      ant.gain  AS antenna_gain,
      
      ant.vert_width  AS antenna_vert_width,
      ant.horz_width  AS antenna_horz_width,

      ant.loss  AS antenna_loss,
      ant.tilt  AS antenna_tilt,

      ant.ground_height  AS antenna_ground_height,

     height_full as antenna_height_full,


IsNull(L.passive_element_loss,0) + ant.loss_full  AS loss_full,


      dbo.fn_PickList_GetValueByCode('linkend', 'redundancy', L.redundancy)    AS redundancy_str,

         
--    L.power_W,
    L.power_dBm,
    
     dbo.fn_dBm_to_Wt(L.power_dBm) AS power_W,          
         
         
         
     L.tx_freq_MHz / 1000 AS tx_freq_GHz,
         
--         dbo.fn_LinkEnd_Azimuth(L.id) AS Azimuth1
     dbo.fn_LinkEnd_Azimuth(L.id, null) AS antenna_Azimuth,
         
     (SELECT MAX(power_max) FROM LinkEndType_Mode mode WHERE mode.LinkEndType_id=L.LinkEndType_id) as power_max,
     
     --dbo.fn_dBm_to_Wt 
     
     L.subband,
     L.channel_type,
     
   /*  freq_max_low,
     freq_min_low,
     freq_max_high,
     freq_min_high,*/
     
 
     IIF(L.channel_type='low',
       cast(freq_min_low as varchar(10)) +'-'+cast(freq_max_low as varchar(10)) ,
       cast(freq_min_high as varchar(10)) +'-'+cast(freq_max_high as varchar(10)) 
     ) as channel_min_max_str
     
                  
         
         
  FROM dbo.LinkEnd L
       LEFT OUTER JOIN dbo.LinkEndType_Mode ON L.LinkEndType_mode_id =  dbo.LinkEndType_Mode.id       
       LEFT OUTER JOIN dbo.LinkEndType_Band ON L.LinkEndType_Band_id =  dbo.LinkEndType_Band.id       

       LEFT OUTER JOIN dbo.Property    ON L.property_id    = dbo.Property.id       
       LEFT OUTER JOIN dbo.LinkEndType ON L.linkendtype_id = dbo.LinkEndType.id
       
      OUTER apply dbo.fn_Linkend_Antenna_info (L.id) ant

GO

IF OBJECT_ID(N'[reports].[View_Link_freq_order]') IS  not  NULL

/****** Object:  View [reports].[View_Link_freq_order]    Script Date: 16.05.2020 10:54:58 ******/
DROP VIEW [reports].[View_Link_freq_order]
GO
/****** Object:  View [reports].[View_Link_freq_order]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------
-- ��������� ������




CREATE VIEW [reports].[View_Link_freq_order]
AS
SELECT 

       LinkEnd1.loss_full        AS LinkEnd1_loss_full, 
       LinkEnd2.loss_full        AS LinkEnd2_loss_full,


--LinkEnd1.antenna_height_full  -  LinkEnd2.antenna_height_full as aaaaaaaaaaaa,


       LinkEnd1.antenna_height_full  AS LinkEnd1_antenna_height_full, 
       LinkEnd2.antenna_height_full  AS LinkEnd2_antenna_height_full, 
       
       LinkEnd1_antenna_tilt_calc = DEGREES (ATAN( (LinkEnd1.antenna_height_full - LinkEnd2.antenna_height_full) / (link.length_km * 1000) )) ,      
       LinkEnd2_antenna_tilt_calc = DEGREES (ATAN( (LinkEnd2.antenna_height_full - LinkEnd1.antenna_height_full) / (link.length_km * 1000) )) ,      

LinkEnd1.power_max as LinkEnd1_power_max,
LinkEnd2.power_max as LinkEnd2_power_max,
dbo.fn_dBm_to_Wt(LinkEnd1.power_max) as LinkEnd1_power_max_W,
dbo.fn_dBm_to_Wt(LinkEnd2.power_max) as LinkEnd2_power_max_W,


   --    dbo.link.length_km, 


       dbo.link.NAME, 
       dbo.link.project_id, 
       dbo.link.length_km, 
       dbo.link.id, 
       

       LinkEnd1.property_name          AS LinkEnd1_Property_name, 
       LinkEnd1.property_lat           AS LinkEnd1_Property_lat, 
       LinkEnd1.property_lon           AS LinkEnd1_Property_lon, 
       LinkEnd1.property_code          AS LinkEnd1_Property_code, 
       LinkEnd1.property_address       AS LinkEnd1_Property_address, 

       LinkEnd2.property_name          AS LinkEnd2_Property_name, 
       LinkEnd2.property_lat           AS LinkEnd2_Property_lat, 
       LinkEnd2.property_lon           AS LinkEnd2_Property_lon, 
       LinkEnd2.property_code          AS LinkEnd2_Property_code, 
       LinkEnd2.property_address       AS LinkEnd2_Property_address, 

----------------------
-- new
----------------------
       LinkEnd1.property_name          AS Property1_name, 
       LinkEnd1.property_lat           AS Property1_lat, 
       LinkEnd1.property_lon           AS Property1_lon, 

       LinkEnd1.property_lat_wgs           AS Property1_lat_wgs, 
       LinkEnd1.property_lon_wgs           AS Property1_lon_wgs, 


       LinkEnd1.property_code          AS Property1_code, 
       LinkEnd1.property_address       AS Property1_address, 

       LinkEnd2.property_name          AS Property2_name, 
       LinkEnd2.property_lat           AS Property2_lat, 
       LinkEnd2.property_lon           AS Property2_lon, 

       LinkEnd2.property_lat_wgs           AS Property2_lat_wgs, 
       LinkEnd2.property_lon_wgs           AS Property2_lon_wgs,  

       LinkEnd2.property_code          AS Property2_code, 
       LinkEnd2.property_address       AS Property2_address, 

       LinkEnd1.property_ground_height AS Property1_ground_height, 
       LinkEnd2.property_ground_height AS Property2_ground_height, 

----------------------
--  LinkEnd1
----------------------
       LinkEnd1.antenna_gain           AS LinkEnd1_antenna_gain, 
       LinkEnd1.antenna_height         AS LinkEnd1_antenna_height, 
       LinkEnd1.antenna_diameter       AS LinkEnd1_antenna_diameter, 
       LinkEnd1.antenna_polarization   AS LinkEnd1_antenna_polarization, 

       LinkEnd1.linkendtype_name       AS LinkEnd1_LinkEndType_name, 
       LinkEnd1.power_w               AS LinkEnd1_power_W, 
       LinkEnd1.rx_freq_mhz            AS LinkEnd1_rx_freq_MHz, 
       LinkEnd1.tx_freq_mhz            AS LinkEnd1_tx_freq_MHz, 


----------------------
--  LinkEnd2
----------------------
       LinkEnd2.antenna_diameter       AS LinkEnd2_antenna_diameter, 
       LinkEnd2.antenna_polarization   AS LinkEnd2_antenna_polarization, 
       LinkEnd2.antenna_height         AS LinkEnd2_antenna_height, 
       LinkEnd2.antenna_gain           AS LinkEnd2_antenna_gain, 
       LinkEnd2.antenna_tilt           AS LinkEnd2_antenna_tilt, 


       LinkEnd1.property_ground_height AS LinkEnd1_Property_ground_height, 
       LinkEnd2.property_ground_height AS LinkEnd2_Property_ground_height, 
       

/*       LinkEnd1.property_ground_height AS LinkEnd1_Property_ground_height, 
       LinkEnd2.property_ground_height AS LinkEnd2_Property_ground_height, 
*/

       LinkEnd2.rx_freq_mhz            AS LinkEnd2_rx_freq_MHz, 
       LinkEnd2.tx_freq_mhz            AS LinkEnd2_tx_freq_MHz, 
       
       
       LinkEnd2.power_w               AS LinkEnd2_power_W, 

       
       LinkEnd1.antenna_azimuth        AS LinkEnd1_antenna_Azimuth, 
       LinkEnd2.antenna_azimuth        AS LinkEnd2_antenna_Azimuth, 
       LinkEnd2.linkendtype_name       AS LinkEnd2_LinkEndType_name, 
       LinkEnd2.radiation_class        AS LinkEnd2_radiation_class, 
       LinkEnd1.radiation_class        AS LinkEnd1_radiation_class, 
       LinkEnd1.antenna_loss           AS LinkEnd1_antenna_loss, 
       LinkEnd2.antenna_loss           AS LinkEnd2_antenna_loss, 
--       LinkEnd2.power_w                AS LinkEnd2_power_W, 
--       LinkEnd1.power_w                AS LinkEnd1_power_W, 

       LinkEnd1.antenna_tilt           AS LinkEnd1_antenna_tilt, 

       LinkEnd2.antenna_ground_height  AS LinkEnd2_antenna_ground_height, 
       LinkEnd1.antenna_ground_height  AS LinkEnd1_antenna_ground_height, 

       
       LinkEnd2.bitrate_mbps           AS LinkEnd2_bitrate_Mbps, 
       LinkEnd1.bitrate_mbps           AS LinkEnd1_bitrate_Mbps, 
       LinkEnd1.power_dbm              AS LinkEnd1_power_dBm, 
       LinkEnd2.power_dbm              AS LinkEnd2_power_dBm, 
       LinkEnd2.antenna_vert_width     AS LinkEnd2_antenna_vert_width, 
       LinkEnd2.antenna_horz_width     AS LinkEnd2_antenna_horz_width, 
       LinkEnd1.antenna_vert_width     AS LinkEnd1_antenna_vert_width, 
       LinkEnd1.antenna_horz_width     AS LinkEnd1_antenna_horz_width, 
       LinkEnd1.NAME                   AS LinkEnd1_name, 
       LinkEnd2.NAME                   AS LinkEnd2_name, 
       LinkEnd1.bandwidth              AS LinkEnd1_bandwidth, 
       LinkEnd2.bandwidth              AS LinkEnd2_bandwidth, 
       LinkEnd1.band                   AS LinkEnd1_band, 
       LinkEnd2.band                   AS LinkEnd2_band, 
       LinkEnd1.linkid                 AS LinkEnd1_LinkID, 
       LinkEnd2.linkid                 AS LinkEnd2_LinkID, 
       LinkEnd1.modulation_type        AS LinkEnd1_modulation_type, 
       LinkEnd2.modulation_type        AS LinkEnd2_modulation_type,
             


       
       LinkEnd1.subband as LinkEnd1_subband,
       LinkEnd2.subband as LinkEnd2_subband,
       
       LinkEnd1.channel_min_max_str as LinkEnd1_channel_min_max_str,
       LinkEnd2.channel_min_max_str as LinkEnd2_channel_min_max_str
       
        
FROM   dbo.link 
       LEFT OUTER JOIN reports.view_linkend_report AS LinkEnd2 
                    ON dbo.link.linkend2_id = LinkEnd2.id 
       LEFT OUTER JOIN reports.view_linkend_report AS LinkEnd1 
                    ON dbo.link.linkend1_id = LinkEnd1.id 

go

--WHERE
--  ( dbo.link.linkend1_id IS NOT NULL ) 

--and 
 -- dbo.link.id = 253552


