

IF OBJECT_ID(N'[dbo].[sp_LinkEnd_Update]') IS NOT NULL
  DROP PROCEDURE [dbo].[sp_LinkEnd_Update]


GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_LinkEnd_Update
(
  @ID int,
  
  
  @CHANNEL_TYPE VARCHAR(4) = null --low,high
  
)
AS
BEGIN
  -- ERROR codes
  -- -1 - @nextLinkEnd_id IS NULL  


  DECLARE
    @sCHANNEL_TYPE VARCHAR(4),
--    @sSubBand VARCHAR(20),
--    @sSubBand_next VARCHAR(20),
--    @LINKENDType_ID int,
  	@nextLinkEnd_id int;
  	
  		
  set @nextLinkEnd_id = dbo.fn_LinkEnd_Get_Next_LinkEnd_ID (@ID)
  

  
  if @CHANNEL_TYPE='low'
    set @sCHANNEL_TYPE='high'
  ELSE
    set @sCHANNEL_TYPE='low'
    
--   CHANNEL_NUMBER_TYPE  =IIF(m.CHANNEL_NUMBER_TYPE=0, 1, 0),

  ----------------
  ----- MAIN ----- 
  ----------------  
  
  UPDATE LinkEnd 
  SET 
    CHANNEL_TYPE  =  COALESCE( @CHANNEL_TYPE, CHANNEL_TYPE)
 
  WHERE 
  		id = @ID
      
      
  ----------------
  ----- NEXT -----      
  ----------------
  
 UPDATE LinkEnd 
  SET 
    CHANNEL_TYPE  =  COALESCE( @sCHANNEL_TYPE, CHANNEL_TYPE)
 
  WHERE 
  		id = @nextLinkEnd_id  
    


END;
go
-----------------------------------------


IF OBJECT_ID(N'dbo.view_LinkEnd_with_project_id') IS NOT NULL
  DROP view [dbo].[view_LinkEnd_with_project_id]
       
GO


CREATE VIEW dbo.view_LinkEnd_with_project_id
AS
SELECT dbo.LinkEnd.*,
       dbo.Property.project_id
FROM dbo.LinkEnd
     LEFT OUTER JOIN dbo.Property ON dbo.LinkEnd.property_id =  dbo.Property.id
     
where 
  LinkEnd.ObjName is null or      
  LinkEnd.ObjName = 'linkend'

go
-------------------------------------------------------


