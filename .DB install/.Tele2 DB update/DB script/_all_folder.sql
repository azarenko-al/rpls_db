IF OBJECT_ID(N'dbo.sp_Folder_MoveObjectToFolder') IS  not  NULL
  DROP PROCEDURE [dbo].[sp_Folder_MoveObjectToFolder]
GO


CREATE procedure dbo.sp_Folder_MoveObjectToFolder
(
  @OBJNAME   varchar(50),
  @ID        int,
  @FOLDER_ID int 
)
AS
begin
  DECLARE
    @sTableName nvarchar(50),
    @sFolder_ID nvarchar(20),
    @sID nvarchar(20),
    @sql NVARCHAR(1000);


  IF @FOLDER_ID=0 set @FOLDER_ID=NULL

  SELECT @sTableName=Table_Name 
  	from _objects
    WHERE name=@OBJNAME

--  SET @sTableName=@OBJNAME
  SET @sID=CONVERT(VARCHAR(255), @ID)

  IF @FOLDER_ID is NULL
   SET @sFolder_ID='null'  
  ELSE  
   SET @sFolder_ID=CONVERT(VARCHAR(255), @FOLDER_ID)


  set @sql = 'UPDATE '+ @sTableName +' SET folder_id= '+ @sFolder_ID +' WHERE id='+ @sID

  exec sp_executesql @sql 
  
  RETURN 0  
end

go
