
IF OBJECT_ID(N'dbo.sp_Map_XREF_Update') IS not  NULL
  DROP PROCEDURE dbo.sp_Map_XREF_Update
GO


/* ---------------------------------------------------------------
created: Alex  
used in:  

delete !!!!!!!!
 
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Map_XREF_Update
(
  @ID       INT, -- MAP_XREF id

  @CHECKED BIT,

--  @priority   INT,
  @ZOOM_MIN   INT = NULL,
  @ZOOM_MAX   INT = NULL,

  @ALLOW_ZOOM BIT = NULL,
  
  @AUTOLABEL BIT = NULL,
  @LABEL_STYLE VARCHAR(MAX) = NULL,
      
  @STYLE VARCHAR(MAX) = NULL
)
AS
BEGIN
 ------------------------------------------------------
  IF @ID is null  RAISERROR ('@ID is null', 16, 1);
  ------------------------------------------------------
  
 
  UPDATE MAP_XREF 
    SET 
      CHECKED  = COALESCE(@CHECKED,CHECKED),
      
      ZOOM_MIN =COALESCE(@ZOOM_MIN,  ZOOM_MIN),
      ZOOM_MAX =COALESCE(@ZOOM_MAX,  ZOOM_MAX),

      ALLOW_ZOOM =COALESCE(@ALLOW_ZOOM,ALLOW_ZOOM),
        
      AutoLabel = COALESCE(@AutoLabel,AutoLabel),
      label_style = COALESCE(@label_style,label_style),

      style = COALESCE(@style, style)
 

--priority =@priority
    WHERE id=@ID
  
END

go

------------------------------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID(N'dbo.sp_Object_Get_Pos') IS not  NULL
  DROP PROCEDURE dbo.sp_Object_Get_Pos
GO




CREATE PROCEDURE dbo.[sp_Object_Get_Pos]
(
  @OBJNAME    VARCHAR(50),  
  @ID   INT

 
--  @LAT  float OUTPUT,
--  @Lon  float OUTPUT
  
)
AS
BEGIN

  if @OBJNAME is null
  begin
    set  @OBJNAME = 'linkend'
--    set  @ID = 3378
    set  @ID = 5119
  
  end
  
---------------------------------------------------
if @OBJNAME = 'linkend'
    SELECT                  
       A1.lat,
       A1.lon
                          
/*       @LAT = A1.lat,
       @Lon = A1.lon
*/
    FROM dbo.LinkEnd 
         cross apply [dbo].fn_Linkend_Antenna_pos (id) A1
         
    where id=@ID

 

return @@ROWCOUNT     

 
END
go


IF OBJECT_ID(N'[map].[sp_MapMaker]') IS  not  NULL

DROP PROCEDURE [map].[sp_MapMaker]
GO
/****** Object:  StoredProcedure [map].[sp_MapMaker]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE [map].[sp_MapMaker]
(
  @PROJECT_ID 	INT=null,
  @OBJNAME 		  VARCHAR(50)=null,
  @ID 	  		  INT=null
  
--  @CHECKSUM_ONLY  bit=0   -- get only chcksum, not selection

--  @MODE VARCHAR(50)=null,
 -- @FIELDS VARCHAR(50)=null
  
 -- @ID 	INT=null
  
 -- @TABLENAME 	VARCHAR(50)=null 
)

AS
BEGIN
  DECLARE
    @s VARCHAR(1000),
  	@checksum INT;
    
    /*
if @PROJECT_ID is null    set @PROJECT_ID=1165
if @OBJNAME is null       set @OBJNAME='Link'
   
  */

/*set @OBJNAME='Linkend_antenna' 
set @CHECKSUM_ONLY =1
set @MODE ='data'
*/

/*
set @OBJNAME='link' 
set @CHECKSUM_ONLY =1
*/

/*  SELECT top 1 @PROJECT_ID=ID 
  	FROM Project
*/    
  
-- if @OBJNAME IS NULL   set @OBJNAME='Linkend_antenna' 
-- if @OBJNAME IS NULL   set @OBJNAME='pmp_calc_region' 

/*
    SELECT CHECKSUM_AGG (BINARY_CHECKSUM(lat,lon ) )
  --  SELECT CHECKSUM_AGG (lat,lon  )
        FROM PMP_CalcRegion_Polygons
          
          */ 
          
      
 /* 
CREATE PROCEDURE dbo.sp_MapMaker_Get_Object_visible_columns
(
  @OBJNAME varchar(50),
  
  @COLUMNS VARCHAR(4000) OUTPUT
)*/          
          
          
  
  -------------------------------------------------------------
  IF @OBJNAME='link_repeater'  
  -------------------------------------------------------------
  BEGIN
    IF @ID IS NOT NULL 
    begin
      SELECT id,lat,lon FROM view_link_repeater WHERE (id=@ID)
      RETURN @@ROWCOUNT     
    end


    SELECT id,lat,lon 
      FROM view_Link_Repeater
      WHERE (PROJECT_ID=@PROJECT_ID)
        
--        WHERE ((@PROJECT_ID IS NOT NULL) and (PROJECT_ID=@PROJECT_ID)) or 
--        	  ((@ID IS NOT NULL) and (id=@ID))
   

  END else



  -------------------------------------------------------------
  IF @OBJNAME='Property'  
  -------------------------------------------------------------
  BEGIN
    IF @ID IS NOT NULL 
    begin
      SELECT id,lat,lon, lat_wgs,lon_wgs FROM Property WHERE (id=@ID)
      RETURN @@ROWCOUNT     
    end


    SELECT id,lat,lon , lat_wgs,lon_wgs
      FROM Property 
      WHERE (PROJECT_ID=@PROJECT_ID)
      order by id
        
--        WHERE ((@PROJECT_ID IS NOT NULL) and (PROJECT_ID=@PROJECT_ID)) or 
--        	  ((@ID IS NOT NULL) and (id=@ID))
   

  END else

  -------------------------------------------------------------
  IF @OBJNAME='Link'  
  -------------------------------------------------------------
  BEGIN
    -----------------------------------------------------
    IF @ID IS NOT NULL 
    begin
      SELECT * 
          --   CHECKSUM(lat1+lat2, lon1+lon2)  as CHECKSUM
        from VIEW_link_location
         WHERE (id=@ID)-- and (IsNull(objname,'link')='link')
         order by id
         
    
      RETURN @@ROWCOUNT     
    end
    
    -----------------------------------------------------

    SELECT * 
        --   CHECKSUM(lat1+lat2, lon1+lon2)  as CHECKSUM
      from VIEW_link_location
      WHERE (PROJECT_ID=@PROJECT_ID) --and (IsNull(objname,'link')='link')
      order by id

  END else


  -------------------------------------------------------------
  IF @OBJNAME='pmp_Link'  
  -------------------------------------------------------------
  BEGIN
    -----------------------------------------------------
    IF @ID IS NOT NULL 
    begin
      SELECT * 
          --   CHECKSUM(lat1+lat2, lon1+lon2)  as CHECKSUM
        from VIEW_link_location
         WHERE (id=@ID) and (IsNull(objname,'link')='pmp_link')
    
      RETURN @@ROWCOUNT     
    end
    
    -----------------------------------------------------

    SELECT * 
        --   CHECKSUM(lat1+lat2, lon1+lon2)  as CHECKSUM
      from VIEW_link_location
      WHERE (PROJECT_ID=@PROJECT_ID) and (IsNull(objname,'link')='pmp_link')

  END else


  -------------------------------------------------------------
  IF @OBJNAME='pmp_calc_region' or @OBJNAME='calc_region_pmp'
  -------------------------------------------------------------
  BEGIN
    -----------------------------------------------------
    IF @ID IS NOT NULL 
    begin
      SELECT id
       FROM pmp_calcregion
       WHERE (ID=@ID)
      
      RETURN @@ROWCOUNT     
    end  
    -----------------------------------------------------
    
  
      SELECT id              
        FROM pmp_calcregion 
        WHERE (PROJECT_ID=@PROJECT_ID)

      
      -- -0-M LEFT OUTER JOIN  Property P ON M.property_id = P.id
  --    WHERE M.PROJECT_ID=@PROJECT_ID  
    
--    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  --  RETURN @checksum
  END else 


  -------------------------------------------------------------
  IF @OBJNAME='PMP_site'
  -------------------------------------------------------------
  BEGIN
    -----------------------------------------------------
    IF @ID IS NOT NULL 
    begin
     -- SELECT id,lat,lon FROM Property WHERE (id=@ID)
            
      SELECT M.id, P.lat, P.lon 
       FROM PMP_site M 
       		LEFT OUTER JOIN  Property P ON M.property_id = P.id
       WHERE (M.ID=@ID)
      
      RETURN @@ROWCOUNT     
    end  
    -----------------------------------------------------
  
  
    SELECT M.id, P.lat, P.lon 
     FROM view_PMP_site M 
          LEFT OUTER JOIN  Property P ON M.property_id = P.id
     WHERE (M.PROJECT_ID=@PROJECT_ID)-- and ((@ID IS NULL) or (M.id=@ID))   
    
  END else

  -------------------------------------------------------------
  IF @OBJNAME='PMP_terminal'
  -------------------------------------------------------------
  BEGIN
    -----------------------------------------------------
    IF @ID IS NOT NULL 
    begin
     -- SELECT id,lat,lon FROM Property WHERE (id=@ID)
            
      SELECT M.id, P.lat, P.lon 
       FROM view_PMP_terminal M 
       		LEFT OUTER JOIN  Property P ON M.property_id = P.id
       WHERE (M.ID=@ID)
      
      RETURN @@ROWCOUNT     
    end  
    -----------------------------------------------------
  
  
    SELECT M.id, P.lat, P.lon 
     FROM view_PMP_TERMINAL M 
          LEFT OUTER JOIN  Property P ON M.property_id = P.id
     WHERE (M.PROJECT_ID=@PROJECT_ID)-- and ((@ID IS NULL) or (M.id=@ID))
       
  END else


  -------------------------------------------------------------
  IF @OBJNAME='MSC'  
  -------------------------------------------------------------
  BEGIN
    -----------------------------------------------------
    IF @ID IS NOT NULL 
    begin
     -- SELECT id,lat,lon FROM Property WHERE (id=@ID)
            
      SELECT M.id, P.lat, P.lon 
       FROM MSC M LEFT OUTER JOIN  Property P ON M.property_id = P.id
       WHERE (M.ID=@ID)
      
      RETURN @@ROWCOUNT     
    end  
    -----------------------------------------------------
    
    SELECT M.id, P.lat, P.lon 
       FROM MSC M 
       		LEFT OUTER JOIN  Property P ON M.property_id = P.id
       WHERE (M.PROJECT_ID=@PROJECT_ID)-- and ((@ID IS NULL) or (M.id=@ID))
   

  END else
   



  -------------------------------------------------------------
  IF @OBJNAME='BSC'  
  -------------------------------------------------------------
  BEGIN
    -----------------------------------------------------
    IF @ID IS NOT NULL 
    begin
     -- SELECT id,lat,lon FROM Property WHERE (id=@ID)
            
      SELECT M.id, P.lat, P.lon 
       FROM BSC M 
       		LEFT OUTER JOIN  Property P ON M.property_id = P.id
       WHERE (M.ID=@ID)
      
      RETURN @@ROWCOUNT     
    end
    -----------------------------------------------------
  
  
      SELECT M.id, P.lat, P.lon 
       FROM BSC M 
       		LEFT OUTER JOIN  Property P ON M.property_id = P.id
--       WHERE M.PROJECT_ID=@PROJECT_ID
       WHERE (M.PROJECT_ID=@PROJECT_ID)-- and ((@ID IS NULL) or (M.id=@ID))
      
  END else



  -------------------------------------------------------------
  IF @OBJNAME='Linkend'  
  -------------------------------------------------------------
  BEGIN
    -----------------------------------------------------
    IF @ID IS NOT NULL 
    begin
     -- SELECT id,lat,lon FROM Property WHERE (id=@ID)
            
     SELECT M.id, P.lat, P.lon 
       FROM Linkend M 
  --     		LEFT OUTER JOIN  Property P ON M.property_id = P.id
           cross apply [dbo].fn_Linkend_Antenna_pos (id) P
            
       WHERE (M.ID=@ID)
      
      RETURN @@ROWCOUNT     
    end
    -----------------------------------------------------
     

      
  
      SELECT M.id, p.lat,p.lon --, BINARY_CHECKSUM(lat,lon) as CHECKSUM
        FROM view_Linkend M 
--        FROM view_Linkend_with_project_id M 
--        	LEFT OUTER JOIN Property P ON M.property_id = P.id
           cross apply [dbo].fn_Linkend_Antenna_pos (id) P
            
        WHERE (m.PROJECT_ID=@PROJECT_ID)-- and ((@ID IS NULL) or (M.id=@ID))
      
  END else


/*
  -------------------------------------------------------------
  IF @OBJNAME='PMP_Sector_ant'  
  -------------------------------------------------------------
  BEGIN
    -----------------------------------------------------
    IF @ID IS NOT NULL 
    begin
      SELECT id, map_lat as lat, map_lon as lon, azimuth
       FROM view_pmp_sector_antennas ---M  LEFT OUTER JOIN  Property P ON M.property_id = P.id
       WHERE (ID=@ID)
      
      RETURN @@ROWCOUNT     
    end
    -----------------------------------------------------
    
      SELECT id,map_lat as lat, map_lon as lon, azimuth --, BINARY_CHECKSUM(lat,lon) as CHECKSUM
        FROM view_pmp_sector_antennas
        WHERE (PROJECT_ID=@PROJECT_ID) --and ((@ID IS NULL) or (id=@ID))
          
    
  END else
*/

  -------------------------------------------------------------
  IF @OBJNAME='Linkend_antenna'  
  -------------------------------------------------------------
  BEGIN
    -----------------------------------------------------
    IF @ID IS NOT NULL 
    begin
      SELECT id,map_lat as lat, map_lon as lon, azimuth
       FROM view_LinkEnd_antennas ---M  LEFT OUTER JOIN  Property P ON M.property_id = P.id
       WHERE (ID=@ID)
      
      RETURN @@ROWCOUNT     
    end
    -----------------------------------------------------
      
  
     SELECT id, map_lat as lat, map_lon as lon,  azimuth --, BINARY_CHECKSUM(lat,lon) as CHECKSUM
        FROM view_LinkEnd_antennas
        WHERE (PROJECT_ID=@PROJECT_ID) --and ((@ID IS NULL) or (id=@ID))


  END ELSE BEGIN
    SELECT 'Object not found: ' + @OBJNAME
    RETURN -1
    
  END
  
  
  RETURN @@ROWCOUNT


END

GO
/****** Object:  StoredProcedure [map].[sp_MapMaker_Data]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[map].[sp_MapMaker_Data]') IS  not  NULL

DROP PROCEDURE [map].[sp_MapMaker_Data]
GO
/****** Object:  StoredProcedure [map].[sp_MapMaker_Data]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [map].[sp_MapMaker_Data]
(
  @PROJECT_ID 	  INT, --=null,
  @OBJNAME 		  VARCHAR(50) --=null
)

AS
BEGIN
  DECLARE
    @s VARCHAR(1000),    
    @LABEL_SQL varchar(5000),    
  	@checksum INT;
    
    
if @PROJECT_ID is null
begin
  set @PROJECT_ID= (select top 1  id from Project)
  
  set @OBJNAME = 'property'

end


-------exec sp_MapMaker_get_object_label @ObjName=@ObjName, @IS_INCLUDE_CAPTION=0, @LABEL_SQL=@LABEL_SQL output

--select @LABEL_SQL

--print @LABEL_SQL


/*
CREATE PROCEDURE dbo.____sp_MapMaker_get_object_label
(
  @ObjName 	varchar(50),
  @IS_INCLUDE_CAPTION bit = null,
  
  @LABEL_SQL varchar(5000) output
)

*/


/*
select * from map.user_map_settings 
*/


declare 
  @caption_sql varchar(max) =
         (select top 1 caption_sql 
             from  map.user_map_settings
             where
                OBJNAME  = @OBJNAME and 
                user_name = user_name() and
                host_name = host_name() 

--                user_name = 'dbo' and
--                host_name = 'alex' 


          )  
          
if IsNull(@caption_sql,'') = '' 
  set @caption_sql = 'name'


set @caption_sql = replace (@caption_sql, ';', '+'',''+' )


print @caption_sql

declare 
  @sql varchar(max)


set @caption_sql = 'cast('+ @caption_sql +' as varchar(2000))'
--cast((:caption) as varchar(2000)) 


--cast((:caption) as varchar(2000))



  ----------------------
  IF @OBJNAME='Property'  
   set @sql = ' SELECT id, 
                   name, :caption as caption,  status, state_name,  mast_count,   --name, --+ CHAR(13)+ name 
                   
                   Linkend_Antenna_count
                   
              FROM view_Property_
              WHERE PROJECT_ID=:PROJECT_ID
              order by id
            '


--      SELECT id, (name + CHAR(10)+ 'id:'+ cast(id as VARCHAR(50))) as caption    --name,
      
      
--      SELECT CAST(id as INT), (name + CHAR(10)+ 'id:'+ cast(id as VARCHAR(50))) as caption    --name,
--      FROM Property

      
--      )) or

  --    WHERE ((@ID IS NULL) and (PROJECT_ID=@PROJECT_ID)) or
    --        ((@ID IS NOT NULL) and (id=@ID))
            
  else
  ----------------------
  IF @OBJNAME='Link'  
   set @sql = '    
                SELECT  id,
                        name, status_name, band, LinkEndType_name,  :caption as caption  --name,
                  FROM view_Link
                  WHERE (PROJECT_ID=:PROJECT_ID) -- and (IsNull(objname,''link'')=''link'')
                  order by id
  '
      
      
      -- and ((@ID IS NULL) or (id=@ID))
  else

  ----------------------
  IF @OBJNAME='pmp_Link'  
   set @sql = '    
  
    SELECT  id,
            name, status_name,  :caption as caption  --name,
      FROM view_Link
      WHERE (PROJECT_ID=:PROJECT_ID)  and (IsNull(objname,''link'')=''pmp_link'')
'      
      -- and ((@ID IS NULL) or (id=@ID))
  else


  ----------------------
  IF @OBJNAME='Linkend'  
   set @sql = '    
  
    SELECT id, 
           name, 
           :caption as caption  --name,
      FROM view_Linkend  --_base --_with_project_id
      WHERE (PROJECT_ID=:PROJECT_ID) 
      order by id
'      
      
      --and ((@ID IS NULL) or (id=@ID))
  else

          
  
  -------------------------------------------------------------
  IF @OBJNAME='link_repeater'  
  -------------------------------------------------------------
  BEGIN
   set @sql = '    
  
  SELECT id, 
           name, 
           :caption as caption  --name,
      FROM view_Link_Repeater
      WHERE (PROJECT_ID=:PROJECT_ID) --and ((@ID IS NULL) or (id=@ID))  
'  
        
--        WHERE ((@PROJECT_ID IS NOT NULL) and (PROJECT_ID=@PROJECT_ID)) or 
--        	  ((@ID IS NOT NULL) and (id=@ID))
   

  END else

    
  ----------------------
  IF @OBJNAME='Linkend_antenna'  
   set @sql = '    
  
    SELECT id,
          name, 
         :caption as caption
      FROM view_LinkEnd_antennas
      WHERE (PROJECT_ID=:PROJECT_ID) --and ((@ID IS NULL) or (id=@ID))     
'      
      --name, --cast(azimuth as VARCHAR(50)) 
  else
    
  ----------------------
  IF @OBJNAME='MSC'  
   set @sql = '    

    SELECT id,
       name, :caption as caption    --name,
      FROM MSC
      WHERE (PROJECT_ID=:PROJECT_ID)-- and ((@ID IS NULL) or (id=@ID))
'
  else

  ----------------------
  IF @OBJNAME='PMP_site'  
   set @sql = '    

    SELECT id,
           name, 
           :caption as caption    --name,
      FROM view_PMP_site
      WHERE (PROJECT_ID=:PROJECT_ID)-- and ((@ID IS NULL) or (id=@ID))
'
  else


  ----------------------
  IF @OBJNAME='PMP_terminal'  
   set @sql = '    
    SELECT id,
          name, :caption as caption    --name,
      FROM view_PMP_TERMINAL --_with_project_id
      WHERE (PROJECT_ID=:PROJECT_ID)-- and ((@ID IS NULL) or (id=@ID))
'      
  else
    
  ----------------------
  IF @OBJNAME='BSC'  
   set @sql = '    
  
    SELECT id,
       name, 
       :caption as caption    --name,
      FROM BSC
      WHERE (PROJECT_ID=:PROJECT_ID) --and ((@ID IS NULL) or (id=@ID))
'
  else
    
  ----------------------
  IF @OBJNAME='pmp_calc_region' or  @OBJNAME='calc_region_pmp'
   set @sql = '    
 
   SELECT id, 
           name, 
           :caption as caption    --name,
      FROM pmp_calcregion
      WHERE (PROJECT_ID=:PROJECT_ID) --and ((@ID IS NULL) or (id=@ID))
'

  ----------------------

  ELSE BEGIN
    RAISERROR ('Object not found', 16, 1);

    SELECT 'Object not found: ' + @OBJNAME
    RETURN -1      
  END


  set @sql = Replace (@sql, ':caption',    @caption_sql )
  set @sql = Replace (@sql, ':PROJECT_ID', cast(@PROJECT_ID as varchar(20)) )


print @sql

  exec (@sql)  
  
  
  RETURN @@ROWCOUNT

 

END

GO
/****** Object:  StoredProcedure [map].[sp_options_object_list_names]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[map].[sp_options_object_list_names]') IS  not  NULL

DROP PROCEDURE [map].[sp_options_object_list_names]
GO
/****** Object:  StoredProcedure [map].[sp_options_object_list_names]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [map].[sp_options_object_list_names]
(
  @OBJNAME varchar(50)='link'

)
AS
BEGIN
  
  select * 
    from dbo.view__object_fields_enabled
    where 
      OBJNAME = @OBJNAME 
--      and [type] in ('list', 'status','lib','xref' ) 
      and 
         ([type] in ('list', 'status','lib','xref' ) or IsNull(xref_TableName,'') <> '' ) -- is not null)
--         ([type] in ('list', 'status','lib','xref' ) or lib_Table_Name is not null)
      
   order by 
      caption   
  
  
END

GO
/****** Object:  StoredProcedure [map].[sp_options_object_list_values]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[map].[sp_options_object_list_values]') IS  not NULL

DROP PROCEDURE [map].[sp_options_object_list_values]
GO
/****** Object:  StoredProcedure [map].[sp_options_object_list_values]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [map].[sp_options_object_list_values]
(
  @ID int -- =19400

)
AS
BEGIN
  if @id is null
--    set @id = (select top 1 id from _object_fields where [type]= 'lib')
--    set @id = 58500
    set @id =	60500	


/*�������� ���������	name	id	type	xref_TableName
4. ��� ������������ ����������� (1- �����������������, 2-������������, 3-������)	underlying_terrain_type	60500	list	
*/


declare
  @report_tbl TABLE (id INt, code int, name varchar(100))


declare 
  @new_id int,
  @status_name varchar(50) ,
  @field_name varchar(50) ,
  @field_type varchar(50) ,
  @lib_Table_Name varchar(50),
  @xref_TableName varchar(50),
  @items varchar(max),
  
  @table_name_initial varchar(50)
  

  select @field_name  = name, 
         @field_type  = [type],
         @status_name = status_name,
         @items = items,
--         @lib_Table_Name     = IsNull(lib_Table_Name,'') ,
         @table_name_initial = table_name_initial,
         @xref_TableName = IsNull(xref_TableName,'')
         
    from dbo.view__object_fields_enabled
    where ID = @ID


/*select @field_name

return
*/

--CREATE FUNCTION [dbo].ft_VARCHAR_String_to_Table
/*
 select *
         
    from dbo.view__object_fields_enabled
    where ID = @ID
  */
 
 ---------------------
-- select * from [dbo].ft_VARCHAR_String_to_Table (@items, char(10))
 

  if @field_type = 'list' and 
    not exists (
               select * 
                  from lib.PickList    
                  where 
                     table_name=@table_name_initial and 
                     field_name = @field_name  )
  
  begin
  
    insert into  lib.PickList (table_name, field_name) values (@table_name_initial, @field_name )    
    set @new_id = @@IDENTITY


--select @field_name, @new_id

    insert into  lib.PickList_Values (PickList_id, value)  
       select @new_id, [Value]
         from [dbo].ft_VARCHAR_String_to_Table (@items, char(10))
  
  end

 ---------------------


--return

 

    
  print @field_type  
  print @lib_Table_Name  
  print @status_name  
   
    
   
--   select id,name  
--        from view_Status_Values where status_name = @status_name  


--select @xref_TableName


 
-----------------------------------------
  IF @field_type = 'status' 
-----------------------------------------
  begin
--    print '1--------------------'
  
    insert into @report_tbl  (id  , name ) 
      select id,name  
        from view_Status_Values where status_name = @status_name
  end 
  
  else      
 

-----------------------------------------
  IF @xref_TableName <> '' 
-----------------------------------------
    insert into @report_tbl  ( name  ) 
        exec ('select distinct name  from '+ @xref_TableName +' order by name')

/* 
    insert into @report_tbl  (id  , name  ) 
        exec ('select id, name  from '+ @xref_TableName +' order by name')
*/
      
 -- else     

/*-----------------------------------------
  IF @lib_Table_Name <> '' --is not null
-----------------------------------------
    insert into @report_tbl  (id , name )
      exec ('select id, name  from '+ @lib_Table_Name +' order by name')
*/

-----------------------------------------
  ELSE    
-----------------------------------------
    insert into @report_tbl (id  , name , code)
       select id, value as name , code
          from lib.PickList_Values  
          where 
            PickList_id in
                           (select id 
                              from lib.PickList    
                              where 
                                 table_name=@table_name_initial and 
                                 field_name = @field_name
                              )

          order by 
             Display_Index, [value]


select * from @report_tbl


/*  
    select * 
      from status  
      where parent_id in
         (select id 
            from lib.PickList    
            where name=@status_name and parent_id is null 
            )
      order by index_
*/

--table_name_initial



--   and [type] in ('list', 'status','lib','xref' ) 

  
END

GO
/****** Object:  StoredProcedure [map].[sp_User_Map_Settings_UPD]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[map].[sp_User_Map_Settings_UPD]') IS  not  NULL

DROP PROCEDURE [map].[sp_User_Map_Settings_UPD]
GO
/****** Object:  StoredProcedure [map].[sp_User_Map_Settings_UPD]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [map].[sp_User_Map_Settings_UPD]
(
  @OBJNAME   varchar(50) ,
  
  @names   	 varchar(max) =  null,
  @style_XML XML =  null
)
AS
BEGIN
  if @OBJNAME is null
  begin
    set @OBJNAME = 'link'
    
    set @names = 'name'
    
 end


declare 
  @id int = (select id 
                from  
                  map.user_map_settings
                where 
                    user_name = user_name() and  
                    host_name = host_name() and
                   OBJNAME = @OBJNAME  
                )
  if @id is null                
  begin
    insert into map.user_map_settings (OBJNAME) values( @OBJNAME )      
    set @id = @@IDENTITY
  end                             

---------------------------

  update map.user_map_settings
  set 
    attribute_column_names = COALESCE( @names, attribute_column_names),
    style_XML = COALESCE( @style_XML, style_XML)
  where
    id = @id

 
  return @ID
  
 
END

GO

