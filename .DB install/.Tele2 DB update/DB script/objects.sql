--  where table_name_initial is not null

update _objects
  set table_name_initial = table_name
  where table_name_initial is null

go

--------------------------------------------------------
update [_objects] 
  set view_name='view_property_' where name='property'


update [_objects] 
  set table_name_initial='link' where name='link'

update [_objects] 
  set table_name_initial='linkend' where name='linkend'

update [_objects] 
  set table_name_initial='linkend' -- , table_name='linkend'
  where name in ( 'pmp_sector', 'PMP_Terminal')

update [_objects] 
  set table_name='view_pmp_sector' , view_name='view_pmp_sector'
  where name in ( 'pmp_sector')

update [_objects] 
  set table_name='view_PMP_Terminal'  , view_name='view_PMP_Terminal'  
  where name in ( 'PMP_Terminal')

update [_objects] 
  set IsUseProjectID=1
  where name in ('Linkend', 'PMP_site', 'PMP_Terminal')



------------------------
update [_object_fields]  set XREF_TableName='CellLayer' where  XREF_TableName='Cell_layer'


update [_object_fields]  set Name='Terminal_Sensitivity_dbm' where  Name='Terminal_Sensitivity'
update [_object_fields]  set Name='TERMINAL_GAIN_db' where  Name='TERMINAL_GAIN'
update [_object_fields]  set Name='Terminal_Loss_db' where  Name='Terminal_Loss'




update _object_fields
  set items='1+0
1+1
2+0 XPIC
2+2 XPIC'
  where  name='kratnostBySpace'


update [_objects] 
  set table_name_initial='rel_matrix' where name='relief'

go


SET IDENTITY_INSERT  _objects   ON
GO    


delete from _objects where id in (184,187  )
delete from _object_fields where object_id in (184,187  )


GO
INSERT [dbo].[_objects] ([id], [name], [display_name], [table_name], [IsUseProjectID], [folder_name], [view_name], [table_name_initial]) VALUES (187, N'template_link', NULL, N'lib.template_link', NULL, NULL, NULL, N'lib.template_link')
go

INSERT [dbo].[_objects] ([id], [name], [display_name], [table_name], [IsUseProjectID], [folder_name], [view_name], [table_name_initial]) VALUES (184, N'link_sdb', N'�� SDB', N'link_sdb', 1, NULL, NULL, N'link_sdb')
GO

SET IDENTITY_INSERT  _objects   OFF
GO    



GO


INSERT [_object_fields] ([object_id], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [is_folder]) VALUES (187, NULL, 1, N'name', NULL, NULL, N'��������', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [_object_fields] ([object_id], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [is_folder]) VALUES (187, NULL, 2, N'LinkEndType_ID', NULL, N'xref', N'������������ ���', NULL, NULL, NULL, 1, NULL, NULL, N'LinkEndType', N'LinkEndType', N'LinkEndType_name', 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [_object_fields] ([object_id], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [is_folder]) VALUES (187, NULL, 5, N'height', NULL, NULL, N'������ �������', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [_object_fields] ([object_id], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [is_folder]) VALUES (187, NULL, 4, N'antennaType_id', NULL, N'xref', N'������ �������', NULL, NULL, NULL, 1, NULL, NULL, N'antenna_Type', N'antennaType', N'antennaType_name', 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [_object_fields] ([object_id], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [is_folder]) VALUES (187, NULL, 6, N'LinkEndType_Mode_ID', NULL, NULL, N'LinkEndType_Mode_ID', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL)
GO
INSERT [_object_fields] ([object_id], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [is_folder]) VALUES (187, NULL, 3, N'mode', NULL, N'button', N'�����', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, 0, NULL)
GO






GO
INSERT [dbo].[_object_fields] ( [object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, NULL, NULL, 1, N'name', NULL, NULL, N'��������', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[_object_fields] ([object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, 1, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[_object_fields] ([object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, NULL, NULL, 3, N'SESR_required', NULL, NULL, N'SESR ���� [%]', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[_object_fields] ([object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, NULL, NULL, 1, N'SESR', NULL, NULL, N'SESR ���� [%]', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[_object_fields] ([object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, NULL, NULL, 4, N'Kng_required', NULL, NULL, N'��� ���� [%]', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[_object_fields] ([object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, NULL, NULL, 1, N'Kng', NULL, NULL, N'��� ���� [%]', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[_object_fields] ([object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, NULL, NULL, 2, N'kng_year', NULL, NULL, N'��� ���� [���/���]', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO 

