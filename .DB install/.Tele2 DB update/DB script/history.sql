/* Data for the 'history.tables_for_history' table  (Records 1 - 25) */


if  COL_LENGTH('history.tables_for_history','parent_id_sql')  is null
  ALTER TABLE history.tables_for_history
     ADD parent_id_sql varchar(50) 
GO

if  COL_LENGTH('history.tables_for_history','use_project_id')  is null
  ALTER TABLE history.tables_for_history
    ADD use_project_id bit DEFAULT 0 NULL
GO

---------------------------------------------------

if  COL_LENGTH('history.history_log','xml_deleted')  is null
  ALTER TABLE history.history_log
    ADD xml_deleted xml NULL
GO



if  COL_LENGTH('history.history_log','xml_inserted')  is null
  ALTER TABLE history.history_log
    ADD xml_inserted xml NULL
GO

if  COL_LENGTH('history.history_log','record_parent_id')  is null
  ALTER TABLE history.history_log
    ADD record_parent_id int NULL
GO
                                                              
if  COL_LENGTH('history.history_log','record_parent_table_name')  is null
  ALTER TABLE history.history_log
    ADD record_parent_table_name varchar(50) COLLATE Cyrillic_General_CI_AS NULL
GO



delete from history.tables_for_history 
go


INSERT INTO history.tables_for_history (table_name, enabled, parent_column_name, table_table_caption, use_project_id, parent_id_sql, parent_table_name)
VALUES 
  (N'AntennaType', 1, NULL, N'������ ������', 0, NULL, NULL),
  (N'CalcModel', 1, NULL, N'������ �������', NULL, NULL, NULL),
  (N'CalcModelParams', 1, NULL, N'������ ������� - ���������', NULL, NULL, NULL),
  (N'CellLayer', 1, NULL, NULL, 0, NULL, NULL),
  (N'ColorSchema', 1, NULL, N'�������� �����', NULL, NULL, NULL),
  (N'ColorSchemaRanges', 1, N'color_schema_id', NULL, NULL, NULL, N'ColorSchema'),
  (N'lib.Template_Link', 1, NULL, N'lib.Template_Link', 0, NULL, NULL),
  (N'link', 1, NULL, N'������������� �������� (���)', 1, NULL, NULL),
  (N'Link_Repeater', 1, NULL, N'������������', NULL, NULL, NULL),
  (N'Link_Repeater_Antenna', 1, N'Link_Repeater_id', N'������������ - �������', NULL, NULL, N'Link_Repeater'),
  (N'link_SDB', 1, NULL, N'��� SDB c ������� Super Dual Band', 1, NULL, NULL),
  (N'LinkEnd', 1, N'property_id', N'���', 1, NULL, N'property'),
  (N'Linkend_Antenna', 1, N'', N'�������', 1, N'[link].fn_LinkEnd_Antenna_Get_Link_ID (I.id)', N'dbo.Link'),
  (N'LinkEndType', 1, NULL, N'������������ ���', NULL, NULL, NULL),
  (N'LinkEndType_Band', 1, N'LinkEndType_id', N'������������ - ���������', NULL, NULL, N'LinkEndType'),
  (N'LinkEndType_Mode', 1, N'LinkEndType_id', N'������������ - ������', NULL, NULL, N'LinkEndType'),
  (N'LinkFreqPlan', 1, NULL, N'��������-��������������� ���� (���)', 1, NULL, NULL),
  (N'LinkLine', 1, NULL, N'������������� ����� (���)', 1, NULL, NULL),
  (N'LinkNet', 1, NULL, N'���� ������������� �����', 1, NULL, NULL),
  (N'pmp_site', 1, NULL, N'pmp_site', 0, NULL, NULL),
  (N'project', 1, NULL, N'������', NULL, NULL, NULL),
  (N'property', 1, NULL, N'��������', 1, NULL, NULL),
  (N'Template_Linkend', 1, NULL, N'Template_Linkend', 0, NULL, NULL),
  (N'TEMPLATE_LINKEND_ANTENNA', 1, NULL, N'TEMPLATE_LINKEND_ANTENNA', 0, NULL, NULL),
  (N'Template_pmp_site', 1, NULL, N'Template_pmp_site', 0, NULL, NULL)
GO




