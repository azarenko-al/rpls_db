

IF OBJECT_ID(N'[Security].[Users]') IS  NULL
begin

    CREATE TABLE [Security].[Users](
        [id] [int] NOT NULL,
        [Name] [varchar](50) NULL,
        [email] [varchar](50) NULL,
        [comments] [varchar](250) NULL,
        [address] [varchar](200) NULL,
        [phone] [varchar](100) NULL,
        [is_modify_project] [int] NULL CONSTRAINT [DF__Users__is_modify__2A8C3FA1]  DEFAULT ((1)),
        [is_modify_lib] [int] NULL CONSTRAINT [DF__Users__is_modify__29981B68]  DEFAULT ((0)),
     CONSTRAINT [Users_pk] PRIMARY KEY CLUSTERED 
    (
        [id] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]

end
go



IF OBJECT_ID(N'[Security].[GeoRegion]') IS  NULL
begin

    CREATE TABLE [Security].[GeoRegion](
        [id] [int] IDENTITY(0,1) NOT NULL,
        [Name] [varchar](47) NULL,
        [Oblast] [varchar](2) NULL,
        [MI_PRINX] [int] NULL,
        [SP_GEOMETRY] [geometry] NULL,
     CONSTRAINT [russia_obl_pk] PRIMARY KEY CLUSTERED 
    (
        [id] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

end

go
-------------------------------------------------------------------


IF OBJECT_ID(N'[Security].[GeoRegion]') IS  NULL
begin

    CREATE TABLE [Security].[GeoRegion](
        [id] [int] IDENTITY(0,1) NOT NULL,
        [Name] [varchar](47) NULL,
        [Oblast] [varchar](2) NULL,
        [MI_PRINX] [int] NULL,
        [SP_GEOMETRY] [geometry] NULL,
     CONSTRAINT [russia_obl_pk] PRIMARY KEY CLUSTERED 
    (
        [id] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

end

go
-------------------------------------------------------------------

IF OBJECT_ID(N'[Security].[Action]') IS  NULL
begin

    CREATE TABLE [Security].[Action](
        [id] [int] IDENTITY(1,1) NOT NULL,
        [name] [varchar](20) NULL,
        [caption] [varchar](100) NULL,
     CONSTRAINT [Action_type_pk] PRIMARY KEY CLUSTERED 
    (
        [id] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]

end

go

IF OBJECT_ID(N'[Security].[User_Security_XREF]') IS  NULL
begin

    CREATE TABLE [Security].[User_Security_XREF](
        [id] [int] IDENTITY(1,1) NOT NULL,
        [user_id] [int] NOT NULL,
        [Project_ID] [int] NULL,
        [GeoRegion_id] [int] NULL,
        [enabled] [bit] NULL,
        [action_id] [int] NULL,
     CONSTRAINT [User_Security_XREF_pk] PRIMARY KEY CLUSTERED 
    (
        [id] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]


    ALTER TABLE [Security].[User_Security_XREF]  WITH CHECK ADD  CONSTRAINT [User_Security_XREF_fk] FOREIGN KEY([action_id])
    REFERENCES [Security].[Action] ([id])
    ON UPDATE CASCADE
    ON DELETE CASCADE

    ALTER TABLE [Security].[User_Security_XREF] CHECK CONSTRAINT [User_Security_XREF_fk]

    ALTER TABLE [Security].[User_Security_XREF]  WITH CHECK ADD  CONSTRAINT [User_Security_XREF_fk_Users] FOREIGN KEY([user_id])
    REFERENCES [Security].[Users] ([id])
    ON DELETE CASCADE

    ALTER TABLE [Security].[User_Security_XREF] CHECK CONSTRAINT [User_Security_XREF_fk_Users]

    ALTER TABLE [Security].[User_Security_XREF]  WITH CHECK ADD  CONSTRAINT [User_Security_XREF_fk2] FOREIGN KEY([GeoRegion_id])
    REFERENCES [Security].[GeoRegion] ([id])

    ALTER TABLE [Security].[User_Security_XREF] CHECK CONSTRAINT [User_Security_XREF_fk2]

    ALTER TABLE [Security].[User_Security_XREF]  WITH NOCHECK ADD  CONSTRAINT [User_Security_XREF_fk2_Project] FOREIGN KEY([Project_ID])
    REFERENCES [dbo].[Project] ([id])
    ON DELETE CASCADE

    ALTER TABLE [Security].[User_Security_XREF] CHECK CONSTRAINT [User_Security_XREF_fk2_Project]


END

go



GO
/****** Object:  View [Security].[View_User_Security_XREF]    Script Date: 18.01.2021 14:15:25 ******/
IF OBJECT_ID(N'[Security].[View_User_Security_XREF]') IS NOT NULL
  DROP VIEW [Security].[View_User_Security_XREF]


GO

/****** Object:  View [Security].[View_User_Security_XREF]    Script Date: 18.01.2021 14:15:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [Security].[View_User_Security_XREF]
AS
SELECT   ------  Security.Users.Name,

sysusers.name as sysname,
    
 Security.User_Security_XREF.id, Security.User_Security_XREF.user_id, Security.User_Security_XREF.Project_ID, 
                      Security.User_Security_XREF.GeoRegion_id, Security.User_Security_XREF.enabled, dbo.GeoRegion.name AS GeoRegion_name, 
                      dbo.Project.name AS Project_name
FROM         Security.User_Security_XREF LEFT OUTER JOIN
                      dbo.Project ON Security.User_Security_XREF.Project_ID = dbo.Project.id LEFT OUTER JOIN
                      dbo.GeoRegion ON Security.User_Security_XREF.GeoRegion_id = dbo.GeoRegion.id LEFT OUTER JOIN
                      Security.Users ON Security.User_Security_XREF.user_id = Security.Users.id
                      
                      

      left join sysusers on sysusers.uid= Security.Users.id

GO

-------------------------------------------------------------------

IF OBJECT_ID(N'Security.view_Users') IS NOT NULL
  DROP VIEW Security.view_Users
          
GO



CREATE VIEW Security.view_Users
AS

  
   SELECT top 99999 U.*, 
   		sysusers.name as sysname
    
    from Security.users U 
      left join sysusers on sysusers.uid=U.id
      
      
   order by sysusers.name

go

--------------------------------------

IF OBJECT_ID(N'[Security].[ft_Projects_allowed]') IS NOT NULL
  DROP FUNCTION [Security].[ft_Projects_allowed]
          
GO



CREATE FUNCTION Security.ft_Projects_allowed
(
  @username sysname=null
)

RETURNS TABLE 

AS
RETURN 
     
 SELECT 
  Project_ID
FROM 
  Security.View_User_Security_XREF
  
where 
      Project_ID > 0
  and [enabled] = 1 
  and
  
     ( [sysname] = COALESCE( @username, USER_NAME())
     or
     is_member(sysname)=1
     )


go

IF OBJECT_ID(N'[Security].[sp_User_access_SEL]') IS NOT NULL
  DROP procedure [Security].[sp_User_access_SEL]
GO


/*
����� ������ �� ��������� user
*/
CREATE PROCEDURE Security.[sp_User_access_SEL]
(
  @USER_NAME varchar(50)=null
)

AS
BEGIN

--select USER_NAME()
if @USER_NAME is null
  set @USER_NAME=USER_NAME()


if @USER_NAME='dbo'
  select 
    @USER_NAME as user_NAME,  

  	CAST(1 as int) as is_modify_Project,
  	CAST(1 as int) as is_modify_lib

else

  select
    @USER_NAME as user_NAME, 
  	is_modify_Project, 
    is_modify_lib
        
  	from  Security.view_Users   
    where 
    	sysname = @USER_NAME
    
    
  --     id not in (select id from Security.Users) 



--select * from Security.Users


  --------------------------------------------------------
--  if (@ACTION = 'users') or  (@ACTION = 'select_users')
  --------------------------------------------------------
--  BEGIN
   --  INSERT INTO [Users] (name, UNAME, LOGIN)
--        SELECT name, name, suser_sname(sid) 
/*  SELECT uid,name, * --name, suser_sname(sid) as login
    FROM sysusers 
    where hasdbaccess=1 and NAME<>'dbo'  
*/    
--          WHERE (status=2) 
          --and 
            --    (name not IN (SELECT name FROM [Users])) 
      
--    from sysusers 
--     where hasdbaccess=1 and NAME<>'dbo'  



   --  UPDATE [users]
    --   set IsAdmin=1
     --  WHERE uname='dbo' and IsAdmin is null
      
   --  SELECT * FROM [users]
     
 -- END ELSE  		



  /* Procedure body */
END


go


