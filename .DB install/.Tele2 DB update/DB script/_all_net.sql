
-------------------------------

IF OBJECT_ID(N'dbo.sp_LinkNet_Del') IS not  NULL
  DROP PROCEDURE dbo.sp_LinkNet_Del
GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE procedure dbo.sp_LinkNet_Del
(
  @ID  int
)
AS
begin   

/*SELECT id INTO #LinkFreqPlan
   from LinkFreqPlan
   where LinkNet_id=@ID*/

   
  delete from CalcMap  
     where LinkFreqPlan_id in 
       (SELECT id from LinkFreqPlan where LinkNet_id=@ID)
     


 /* delete from LinkFreqPlan_Link  where LinkFreqPlan_id=@ID
  delete from LinkFreqPlan_LinkEnd  where LinkFreqPlan_id=@ID
  delete from LinkFreqPlan_Neighbors  where LinkFreqPlan_id=@ID
 */ 
 
 


  delete from LinkFreqPlan_Resource  
      where LinkFreqPlan_id in (select id from LinkFreqPlan  where LinkNet_id=@ID)


delete from LinkFreqPlan  where LinkNet_id=@ID
   

  DELETE FROM LinkNet_Items_XREF WHERE LinkNet_ID=@ID
  DELETE FROM LinkNet WHERE ID=@ID
     
  DECLARE @result int 
  set @result =  @@ROWCOUNT ;
  
  
 IF Exists (select * from LinkNet where id=@ID)
     RAISERROR ('record Exists', 16, 1);
    
  
  return @result    
      
end
go
