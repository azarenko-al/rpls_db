


  IF OBJECT_ID(N'[history].[view_columns]') IS  not  NULL
DROP VIEW [history].[view_columns]
GO
/****** Object:  View [history].[view_columns]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [history].[view_columns]
AS

 select top 100 percent
 
col.*  , 


case
  when c.Column_name is not null then '(select name from '+
                                         FK.Target_Table_SCHEMA +'.'+ FK.Target_Table_name 
                                         +' m where m.id = '+ col.column_name +') as '+ FK.Target_Table_name + '_name'
  
  when col.data_type='float' then 'cast ('+ col.column_name+' as varchar(20)) as '+ col.column_name
  else col.column_name  
end    
 as xml_column_name,
 
 fk.Target_Table_SCHEMA,
 fk.Target_Table_name,
 fk.Target_Column,
 
 c.Column_name as fk_Column_name
  

 from  INFORMATION_SCHEMA.COLUMNS  col
    INNER JOIN sys.columns AS sc ON sc.object_id = object_id(col.table_schema + '.' + col.table_name) 
                                  AND sc.NAME = col.COLUMN_NAME
    inner join sys.objects obj on obj.object_Id=sc.object_Id 
  --  left join  sys.computed_columns cc on cc.name = col.COLUMN_NAME and cc.object_id = obj.object_Id

  left join history.view_FK FK on FK.Source_TABLE_SCHEMA=col.table_schema and 
                                  FK.Source_Table_name=col.table_name and 
                                  FK.Source_Column=col.column_name
                                  
  left join INFORMATION_SCHEMA.COLUMNS c on c.TABLE_SCHEMA=FK.Target_Table_SCHEMA and 
                                            c.Table_name=FK.Target_Table_name and                                        
                                            c.Column_name='name'
                                  
                                    

/*  ccu.constraint_name AS SourceConstraint
   ,ccu.TABLE_SCHEMA AS Source_TABLE_SCHEMA
   
     ,ccu.table_name AS Source_Table_name
    ,ccu.column_name AS Source_Column
    
    ,kcu.table_name AS Target_Table_name
    ,kcu.table_name AS Target_Table_SCHEMA
    ,kcu.column_name AS Target_Column
*/

where 
  obj.[type]='u'
  
  and col.DATA_TYPE<>'text'
  
--  and data_type='float'

  and col.COLUMN_NAME not like'[_][_]%'
  and col.COLUMN_NAME not like'%[_][_]'
  and col.COLUMN_NAME not like '[_]'
  and col.COLUMN_NAME not like'%[-]'
  and col.COLUMN_NAME not like'%[11]'

  and col.table_NAME not like'[_][_]%'
  and col.table_NAME not like'%[_][_]'
  and col.table_NAME not like'%[11]'
  


  and col.COLUMN_NAME not in
      (
--       'id', 
       'guid', 
       'date_modify',
       'date_created',
       'user_modify',
       'user_created'
--       'project_id'
      )


--  and cc.definition is null
  
  and sc.Is_computed<>1

order by 
  col.table_schema,
  col.table_name , 
  col.ordinal_position
  
  /*
  CREATE VIEW history.view_FK
AS
SELECT 
    ccu.constraint_name AS SourceConstraint
   ,ccu.TABLE_SCHEMA AS Source_TABLE_SCHEMA
   
     ,ccu.table_name AS Source_Table_name
    ,ccu.column_name AS Source_Column
    
    ,kcu.table_name AS Target_Table_name
    ,kcu.table_name AS Target_Table_SCHEMA
    ,kcu.column_name AS Target_Column
    
FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu
    INNER JOIN INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
        ON ccu.CONSTRAINT_NAME = rc.CONSTRAINT_NAME 
    INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu 
        ON kcu.CONSTRAINT_NAME = rc.UNIQUE_CONSTRAINT_NAME
*/

GO




--------------------------------------------------------------
IF OBJECT_ID(N'[history].[view_COLUMNS_group]') IS NOT NULL
DROP view [history].[view_COLUMNS_group]
GO


CREATE VIEW history.view_COLUMNS_group
AS
    
SELECT 
      
	 T.[TABLE_SCHEMA] +'.'+ T.TABLE_NAME as TABLE_NAME_full,

  SUBSTRING(
          (
              SELECT ','+  [xml_column_name] AS [text()]
                FROM history.view_columns C  
                WHERE 
                --     DATA_TYPE not in ('text') and 
                
                      C.TABLE_SCHEMA =  T.TABLE_SCHEMA and 
                      C.TABLE_NAME=  T.TABLE_NAME                       
              FOR XML PATH ('')
                ), 2, 10000000000) [sql_columns] 


  FROM  [INFORMATION_SCHEMA].[TABLES] T
  where TABLE_TYPE = 'BASE TABLE' and
  
        TABLE_NAME not in ('msfavorites') and 
        TABLE_NAME not like '%[11]' and
        TABLE_NAME not like '%[__]'

go


