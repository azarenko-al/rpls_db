IF OBJECT_ID(N'dbo.sp_LinkEndType_Band_Get_Channel_info') IS NOT NULL
  DROP procedure dbo.sp_LinkEndType_Band_Get_Channel_info


GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_LinkEndType_Band_Get_Channel_info
(
  @LinkEndType_Band_ID int,
  @Channel_NUMBER 	   int,  
  @Channel_TYPE 	   varchar(4),  
  
  @subband	 varchar(50) OUTPUT,
  @TxFreq_MHZ float OUTPUT,  
  @RxFreq_MHZ float OUTPUT
  
)

AS
BEGIN


  DECLARE

  -- @dFreqMin      float,
   @dFreq_high    float,
   @FREQ_MIN_LOW  float,
   @FREQ_MIN_HIGH float,


   @LOW   varchar(50),  
   @HIGH  varchar(50),  
   

   @CHANNEL_WIDTH float,
   @TXRX_SHIFT    float,   
   @dFreq_low     float
   
   ;
 
 
    SELECT 
    
         
          @LOW=LOW, 
          @HIGH=HIGH,
       --  CHANNEL_COUNT,
         
         @FREQ_MIN_LOW = FREQ_MIN_LOW, 
     --    @FREQ_MAX_LOW = FREQ_MAX_LOW,      
         @FREQ_MIN_HIGH = FREQ_MIN_HIGH,
     --    @FREQ_MAX_HIGH = FREQ_MAX_HIGH,
         
         @CHANNEL_WIDTH = CHANNEL_WIDTH, 
         @TXRX_SHIFT    = TXRX_SHIFT 
         
      FROM LinkEndType_band 
      WHERE id=@LinkEndType_band_ID;


if @Channel_TYPE = 'low'
begin
   set @subband = @LOW

    set @dFreq_low =@FREQ_MIN_LOW  + @CHANNEL_WIDTH * (@Channel_NUMBER-1);   
 
   
    set @TxFreq_MHZ= @dFreq_low 
    set @RxFreq_MHZ= @dFreq_low  + @TXRX_SHIFT

end else

if @Channel_TYPE = 'high'
begin
   set @subband = @HIGH
 
    set @dFreq_high=@FREQ_MIN_HIGH + @CHANNEL_WIDTH *  (@Channel_NUMBER-1);     
   
    set @TxFreq_MHZ= @dFreq_high 
    set @RxFreq_MHZ= @dFreq_high  - @TXRX_SHIFT

end 
    
   
  
END
go
--------------------------------------------------------

IF OBJECT_ID(N'dbo.sp_Object_Update_LinkEndType') IS NOT NULL
  DROP procedure dbo.sp_Object_Update_LinkEndType 


GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Object_Update_LinkEndType 
(
  @OBJNAME			varchar(50),
  @ID   				int,  -- Template_Cell ID
  
  @LinkEndType_ID 		int,
  @LinkEndType_Mode_ID 	int,  --,  --= null

  @LinkEndType_band_ID 	int = null,
  @CHANNEL_NUMBER 	    int = null
  
 -- @LinkEndType_mode_ID int=NULL
)
AS
BEGIN





  SET ANSI_WARNINGS OFF --String or binary data would be truncated.



/*
  set @OBJNAME	= 'pmp_terminal'
  set @ID   	= 498 
  
  set @LinkEndType_ID = 168
  set @LinkEndType_Mode_ID  = 1363 
*/


  ---------------------------------------------
  IF @OBJNAME IS NULL  RAISERROR ('@OBJNAME is NULL', 16, 1);
  IF @ID      IS NULL  RAISERROR ('@ID is NULL', 16, 1);
  IF @LinkEndType_ID IS NULL  RAISERROR ('@LinkEndType_ID is NULL', 16, 1);
  ---------------------------------------------
  
 -- IF @Mode IS NULL  RAISERROR ('@Mode is NULL', 16, 1);


  DECLARE 
     @RxFreq_MHZ float,
     @TxFreq_MHZ float,  
  
     @channel_type 	 varchar(4),  
     
     @subband	 varchar(50),  



	 @mode 	  int,
     @result  int,
	 @band 			  varchar(20),
     @freq_MHz 		  float,
     @failure_period  float,
     @equaliser_profit	float;

  --   @iLinkEnd_Type_ID  int ,
    -- @POWER_MAX 		float, 
 
  SELECT 
       @band = Band,
  --     @POWER_MAX 			=  POWER_MAX
       @failure_period 	= failure_period,
       @equaliser_profit = equaliser_profit  
                
    FROM LinkEndType
    WHERE (id = @LinkEndType_ID)   
   
  
  set @freq_MHz = dbo.fn_LinkEndType_GetAveFreq_MHz(@LinkEndType_ID);

--        db_Par(FLD_EQUIPMENT_TYPE,      IIF(rec.LinkEndType_ID>0, DEF_TYPE_EQUIPMENT, DEF_NON_TYPE_EQUIPMENT)),
--  //оборудование: 0-типовое, 1-нетиповое
--  DEF_TYPE_EQUIPMENT     = 0;
--  DEF_NON_TYPE_EQUIPMENT = 1;


---------------------------------------------
  IF @OBJNAME in ('LinkEnd','pmp_terminal','pmp_sector')
---------------------------------------------  
  BEGIN
  
  
    UPDATE LinkEnd SET
        EQUIPMENT_TYPE = 0,		    
          
        LinkEndType_ID	  = @LinkEndType_ID,
      --  LinkEndType_Mode_ID = @LinkEndType_Mode_ID,
--        Mode = @mode,

        BAND 			= @BAND,
        Tx_freq_MHz     = @freq_MHz,
        Rx_freq_MHz     = @freq_MHz,
      
  --   Mode 			  = @Mode,
      

    --  POWER_MAX         = @POWER_MAX,
  --    POWER_dBm         = @POWER_MAX,

   --   Tx_freq_MHz   	  = @freq_MHz,
  --    Rx_freq_MHz   	  = @freq_MHz
          
      failure_period   = @failure_period,
      equaliser_profit = @equaliser_profit      
     
    WHERE (id = @ID)  

   -------------------------------------------
   if (@LinkEndType_band_ID is not null ) 
   -------------------------------------------
   begin
     SELECT @channel_type = channel_type                  
        FROM LinkEnd
        WHERE (id = @ID)   
        
 
          
      exec sp_LinkEndType_Band_Get_Channel_info
            @LinkEndType_Band_ID  = @LinkEndType_band_ID,
            @Channel_NUMBER 	  = @Channel_NUMBER,  
            @Channel_TYPE 	      = @channel_type, 
            
            @subband 	          = @subband OUTPUT,                
            @RxFreq_MHZ = @RxFreq_MHZ OUTPUT,
            @TxFreq_MHZ = @TxFreq_MHZ OUTPUT  
                
print @subband


        UPDATE LinkEnd SET
            LinkEndType_Band_ID = @LinkEndType_Band_ID,		    
            Channel_NUMBER 	  = @Channel_NUMBER,  
            subband 	      = @subband, 
              
            Tx_freq_MHz     = @TxFreq_MHZ,
            Rx_freq_MHz     = @RxFreq_MHZ
          
         
        WHERE (id = @ID)  

   
   end

     


  END ELSE 
  
  
---------------------------------------------
  IF @OBJNAME='Template_linkend'
---------------------------------------------  
  BEGIN
    UPDATE Template_linkend SET
    --    EQUIPMENT_TYPE = 0,		    
          
        LinkEndType_ID	    = @LinkEndType_ID,
     --   LinkEndType_Mode_ID = @LinkEndType_Mode_ID,
     --   Mode = @mode,

        BAND = @BAND, 
        Tx_freq_MHz     = @freq_MHz,
        Rx_freq_MHz     = @freq_MHz
      
  --   Mode 			  = @Mode,
      

    --  POWER_MAX         = @POWER_MAX,
  --    POWER_dBm         = @POWER_MAX,

   --   Tx_freq_MHz   	  = @freq_MHz,
  --    Rx_freq_MHz   	  = @freq_MHz
          
    --  failure_period   = @failure_period,
    --  equaliser_profit = @equaliser_profit      
     
    WHERE (id = @ID) 


  END ELSE
    RETURN -1       


if @LinkEndType_mode_ID > 0
   exec @result = sp_Object_Update_LinkEndType_Mode 
     @OBJNAME=@OBJNAME, 
     @ID=@ID, 
   --  @LinkEndType_ID=@LinkEndType_ID, 
     @LinkEndType_mode_ID=@LinkEndType_mode_ID



  RETURN @@ROWCOUNT   
 

  
 -- exec sp_LinkEnd_Update_Mode @ID=@ID, @Mode=@Mode --, @LinkEndType_mode_ID=@LinkEndType_mode_ID
 
END
go

---------------------------------------------------------

IF OBJECT_ID(N'dbo.sp_Template_Linkend_Add') IS NOT NULL
  DROP procedure dbo.sp_Template_Linkend_Add


GO


CREATE procedure dbo.sp_Template_Linkend_Add
(
  @NAME 						varchar(100),
  @Template_pmp_SITE_ID	int,

  @OBJNAME 					VARCHAR(10), 

  @CELL_LAYER_ID		int =NULL,
  @LINKENDTYPE_ID		int =NULL,

  @CALC_RADIUS_km   float =NULL,
  @CALC_MODEL_ID 		int   =NULL,

  @COMBINER_ID 			int	  =NULL,
  @COMBINER_LOSS		float	=NULL,


  --@BAND 		VARCHAR(10) = null,

  @POWER_dBm	 	float = null, 
  @Tx_Freq_MHz  float = null,
  
   ---------------------------- 
  @IS_GET_INFO bit = 0
  
)
AS
begin    

  DECLARE
	  @new_ID int,

  	@K0 				float,     
		@K0_OPEN 		float,
		@K0_CLOSED 	float;  		


  IF @COMBINER_ID=0 SET @COMBINER_ID=NULL
  IF @CELL_LAYER_ID=0 SET @CELL_LAYER_ID=NULL
  IF @LINKENDTYPE_ID=0 SET @LINKENDTYPE_ID=NULL
  IF @CALC_MODEL_ID=0 SET @CALC_MODEL_ID=NULL


  INSERT INTO Template_Linkend
   (NAME, 
    Template_pmp_SITE_ID,
    
    CELL_LAYER_ID,
    LINKENDTYPE_ID,
    
    CALC_RADIUS_km,
    CALC_MODEL_ID,
    
    POWER_dBm,	
    POWER_max,	
    Tx_Freq_MHz
  --  Rx_Freq_MHz 
    
   )
  VALUES
   (@NAME, 
    @Template_pmp_SITE_ID, 
    
    @CELL_LAYER_ID,
    @LINKENDTYPE_ID,
    
    @CALC_RADIUS_km,
    @CALC_MODEL_ID,
    
    @POWER_dBm,
    @POWER_dBm,
    @Tx_Freq_MHz
     
   ) 
 
     
     
  set @new_id= IDENT_CURRENT ('Template_Linkend')
         
  
 -- set @new_id=@@IDENTITY
 
 
    
  exec sp_Object_Update_CalcModel
        @OBJNAME = 'Template_Linkend',
        @ID = @new_id,
        @CALC_MODEL_ID = @CALC_MODEL_ID
    
  
if ISNULL(@LINKENDTYPE_ID,0) > 0
  
    exec sp_Object_Update_LinkEndType 
       @OBJNAME='Template_linkend', 
       @ID=@new_ID, 
       @LinkEndType_ID=@LinkEndType_ID
   --    @LinkEndType_mode_ID=@LinkEndType_mode_ID
  
  
  if @IS_GET_INFO=1
    SELECT id,guid,name  FROM Template_Linkend WHERE ID=@new_id
  
  RETURN @new_id;    
 
  
end
go

-------------------------------------------------------------------------------


