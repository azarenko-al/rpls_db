if OBJECT_ID('Security.GeoRegion') is not null
begin
--  delete from Security.GeoRegion

  if OBJECT_ID('Security.GeoRegion_ck') is not null
    ALTER TABLE [Security].[GeoRegion] DROP CONSTRAINT [GeoRegion_ck]

end

GO

----------------------------------------------------------------------

if  COL_LENGTH('dbo.LinkFreqPlan','PolarizationLevel')  is null
  ALTER TABLE dbo.LinkFreqPlan  ADD PolarizationLevel float DEFAULT -30 NULL
GO


----------------------------------------------------------------------

--if  COL_LENGTH('dbo.Link','AM_data_xml')  is null
--  ALTER TABLE dbo.Link
--    ADD AM_data_xml xml NULL
--GO

if  COL_LENGTH('dbo.Link','AM_data_bin')  is null
  ALTER TABLE dbo.Link
    ADD AM_data_bin image NULL
GO


if  COL_LENGTH('dbo.Link','refraction_2')  is null
  ALTER TABLE dbo.Link
    ADD refraction_2 float NULL
GO

----------------------------------------------------------------------


if  COL_LENGTH('dbo._objects','table_name_initial')  is null
  ALTER TABLE dbo._objects
   ADD table_name_initial varchar(50) 
GO

if  COL_LENGTH('dbo._object_fields','is_use_on_map')  is null
  ALTER TABLE dbo._object_fields
    ADD is_use_on_map bit NULL
GO

if  COL_LENGTH('dbo._object_fields','is_user_column')  is null
  ALTER TABLE dbo._object_fields
    ADD is_user_column bit NULL
GO

----------------------------------------------------------------------
-- Template_LinkEnd
----------------------------------------------------------------------
if  COL_LENGTH('dbo.Template_LinkEnd','mode')  is null
  ALTER TABLE dbo.Template_LinkEnd ADD mode int NULL
GO

if  COL_LENGTH('dbo.Template_LinkEnd','channel_type')  is null
  ALTER TABLE dbo.Template_LinkEnd ADD channel_type int NULL
GO

if  COL_LENGTH('dbo.Template_LinkEnd','subband')  is null
  ALTER TABLE dbo.Template_LinkEnd ADD subband varchar(50) COLLATE Cyrillic_General_CI_AS NULL
GO

if  COL_LENGTH('dbo.Template_LinkEnd','channel_number')  is null
  ALTER TABLE dbo.Template_LinkEnd ADD channel_number int NULL
GO

if  COL_LENGTH('dbo.Template_LinkEnd','rx_freq_MHz')  is null
  ALTER TABLE dbo.Template_LinkEnd ADD rx_freq_MHz int NULL
GO

if  COL_LENGTH('dbo.Template_LinkEnd','tx_freq_MHz')  is null
  ALTER TABLE dbo.Template_LinkEnd ADD tx_freq_MHz int NULL
GO

----------------------------------------------------------------------
if  COL_LENGTH('dbo.Map_Objects','style')  is null
  ALTER TABLE dbo.Map_Objects  ADD style xml NULL
GO

if  COL_LENGTH('dbo.Map_XREF','style')  is null
  ALTER TABLE dbo.Map_XREF  ADD style xml NULL
GO

----------------------------------------------------------------------

if  COL_LENGTH('dbo.Property','name_rus')  is null
  ALTER TABLE dbo.Property
    ADD name_rus varchar(200) COLLATE Cyrillic_General_CI_AS NULL
GO

if  COL_LENGTH('dbo.Property','NRI')  is null
  ALTER TABLE dbo.Property
   ADD NRI varchar(30) COLLATE Cyrillic_General_CI_AS NULL
GO



--ALTER TABLE dbo.Link  
--   alter column length float not NULL
--GO





ALTER TABLE dbo.Template_LinkEnd
ADD POWER_MAX float NULL
GO
