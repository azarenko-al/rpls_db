
delete from Map_XREF  where MapObject_name='pmp_sector_ant'
go


delete from Map_objects  where name='pmp_sector_ant'
go

-------------------------------

IF OBJECT_ID(N'gis.sp_Relief_Add') IS not  NULL
  DROP PROCEDURE gis.sp_Relief_Add
GO



CREATE PROCEDURE gis.sp_Relief_Add
(
  @PROJECT_ID  	INT,
  
  @NAME 		varchar(250),  -- file name
  
  @FILESIZE 	BIGINT, --bytes
  @FILEDATE 	DATETIME,
  
  @STEP_X FLOAT=null,
  @STEP_Y FLOAT=null,
  
  @X_MIN FLOAT=null,
  @Y_MAX FLOAT=null,
  @X_MAX FLOAT=null,
  @Y_MIN FLOAT=null,  
    
  @STEP_LAT FLOAT=null,
  @STEP_LON FLOAT=null,

  @LAT_MIN FLOAT=null,
  @LON_MAX FLOAT=null,
  @LAT_MAX FLOAT=null,
  @LON_MIN FLOAT=null,
  
  
  @COMMENT varchar(250)=null,
  
  @GEOM_STR varchar(max)
    
)
AS
BEGIN
  ------------------------------------------------------
  IF @PROJECT_ID is null  RAISERROR ('@ID is null', 16, 1);
  IF @NAME is null  RAISERROR ('@NAME is null', 16, 1);
  ------------------------------------------------------
 
  DECLARE
    @new_id int,
    @id  INT;
    

--set @GEOM_STR = replace(@GEOM_STR,',','.')

    
 -- set @NAME=alltrim(@NAME)  

  SELECT @id=id 
  	FROM Relief 
--  	FROM view_Relief 
    WHERE (PROJECT_ID=@PROJECT_ID) and (NAME=@NAME) and (HostName = HOST_NAME())

  IF @id IS NOT NULL 
    RETURN @id;
            

  INSERT INTO Relief
     (PROJECT_ID, 
      NAME,
      geom
      )
  VALUES
     (@PROJECT_ID, 
      @Name,
      geometry::STPolyFromText(@geom_str, 4326).MakeValid()
      )
   /* 
     geom = case
       		  when @geom_str is not null 
                 then geometry::STPolyFromText(@geom_str, 4326).MakeValid()
              else null   
            end
*/  
    
  set @new_id= IDENT_CURRENT ('Relief')
    
  INSERT into Relief_XREF  (Relief_id) VALUES (@new_id)
  
  --set @new_id=@@IDENTITY;   
  
  UPDATE Relief
  SET 
     FILESIZE = @FILESIZE,
     FILEDATE = @FILEDATE,  
     
     STEP_X  =   @STEP_X,  
     STEP_Y  =   @STEP_Y,  
                  
     X_MIN   =    @X_MIN,  
     Y_MAX   =    @Y_MAX, 
     X_MAX   =    @X_MAX, 
     Y_MIN   =    @Y_MIN, 
                    
     STEP_LAT=    @STEP_LAT, 
     STEP_LON=    @STEP_LON, 
                 
     LAT_MIN =    @LAT_MIN,  
     LON_MAX =    @LON_MAX,  
     LAT_MAX =    @LAT_MAX,  
     LON_MIN =    @LON_MIN,  
          
     COMMENT   = @COMMENT
   
/*
     geom = case
       		  when @geom_str is not null 
                 then geometry::STPolyFromText(@geom_str, 4326).MakeValid()
              else null   
            end
*/
  WHERE 
    id = @new_id
  
  RETURN @new_id;
     


END
     
go


IF OBJECT_ID(N'gis.sp_Relief_by_LatLon_SEL') IS not  NULL
  DROP PROCEDURE gis.sp_Relief_by_LatLon_SEL
GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE gis.sp_Relief_by_LatLon_SEL
(
  @PROJECT_ID  	INT ,  

  @LAT float, 
  @LON float

)
AS
BEGIN
  if @LAT is null 
  begin
    set @LAT = 60.0176
    set @LON = 30.398

  end


  DECLARE 
    @g geometry =  geometry::Point(@LON, @LAT,  4326) --.ShortestLineTo (geometry::Point(@LON2,@LAT2,  4326)) ; 


  SELECT * 
     --    geom.ToString()
  FROM 
    Relief
  where   
    @g.STIntersects(geom) =1 
    and   (hostname=host_name())  
    and project_id = @project_id
    
  return @@ROWCOUNT  

END
go

-------------------------------------------------
IF OBJECT_ID(N'gis.sp_Relief_geom_clear_UPD_ALL') IS not  NULL
  DROP PROCEDURE gis.sp_Relief_geom_clear_UPD_ALL
GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE gis.sp_Relief_geom_clear_UPD_ALL

AS
BEGIN 
  
  UPDATE Relief
  SET      
     geom = null
         

  
  return @@ROWCOUNT  ;
  
  


END
go


-------------------------------------------------
IF OBJECT_ID(N'gis.sp_Relief_Intersects_line_SEL') IS not  NULL
  DROP PROCEDURE gis.sp_Relief_Intersects_line_SEL
GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE gis.sp_Relief_Intersects_line_SEL
(
  @PROJECT_ID  	INT,   

  @LAT1 float, 
  @LON1 float,
  
  @LAT2 float, 
  @LON2 float

)
AS
BEGIN
  if @LAT1 is null 
  begin
    set @LAT1 = 55.793038
    set @LON1 =37.808243

    set @LAT2 = 55.794038
    set @LON2 =37.818243

  end


  DECLARE 
    @g geometry =  geometry::Point(@LON1,@LAT1,  4326).ShortestLineTo (geometry::Point(@LON2,@LAT2,  4326)) ; 


  SELECT *, 
         geom.ToString()
  FROM 
    Relief
  where   
    @g.STIntersects(geom) =1 
    and   (hostname=host_name())  
    and project_id = @project_id
    and id in  (SELECT relief_id  FROM Relief_XREF WHERE checked = 1  ) 

    
  return @@ROWCOUNT  


    --POLYGON ((37.767419 55.815241, 
              --37.768846 55.770046, 
              --37.849021 55.770823, 
              --37.847687 55.816019, 
              --37.767419 55.815241))


  
  /*

SELECT *, 
         geo_WGS.STAsText (),
         geo_WGS.STSrid,
         geo_WGS.EnvelopeCenter().ToString()   
  FROM 
    GIS.Relief  
  
  
  
SELECT
  pt1.ToString(),
  pt2.ToString(),
  pt1.ShortestLineTo(pt2).ToString() AS line -- what goes here
FROM #tmp;


DECLARE @g geometry;  
DECLARE @h geometry;  
SET @g = geometry::STGeomFromText('POLYGON((0 0, 0 2, 2 2, 2 0, 0 0))', 0);  
SET @h = geometry::STGeomFromText('POLYGON((1 1, 3 1, 3 3, 1 3, 1 1))', 0);  
SELECT @g.STIntersection(@h).ToString();  
POINT (37.808243250129351 55.7930387796179)
*/
  /* Procedure body */
END


go


-------------------------------------------------
IF OBJECT_ID(N'gis.sp_Relief_UPD') IS not  NULL
  DROP PROCEDURE gis.sp_Relief_UPD
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE gis.sp_Relief_UPD
(
  @ID  	INT,
  
  @geom_str varchar(max)
    
)
AS
BEGIN 
--set @GEOM_STR = replace(@GEOM_STR,',','.')
  


  UPDATE Relief
  SET      
     geom =  geometry::STPolyFromText(@geom_str, 4326).MakeValid()
         

  WHERE 
    id = @id
  
  return @@ROWCOUNT  ;
  
  


END
go

-------------------------------------------------
IF OBJECT_ID(N'gis.sp_Relief_UPD_ALL') IS not  NULL
  DROP PROCEDURE gis.sp_Relief_UPD_ALL
GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE gis.sp_Relief_UPD_ALL

AS
BEGIN 
  
  UPDATE Relief
  SET      
     geom = geom.MakeValid()
         

  WHERE 
   geom is not null
  
  return @@ROWCOUNT  ;
  
  


END

go

-------------------------------------------------
IF OBJECT_ID(N'gis.sp_Relief_with_null_geom_SEL') IS not  NULL
  DROP PROCEDURE gis.sp_Relief_with_null_geom_SEL
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE gis.sp_Relief_with_null_geom_SEL
(
  @PROJECT_ID  	INT    
)
AS
BEGIN   
  select * from Relief
  where
     geom is null    
     and   (hostname=host_name())  
           
     and project_id = @project_id
  
  return @@ROWCOUNT  ;


END

go
