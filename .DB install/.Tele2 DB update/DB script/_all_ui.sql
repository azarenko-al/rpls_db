
   if  COL_LENGTH('dbo.status','values')  is null
    ALTER TABLE dbo.status  ADD [values] varchar(50)
GO


update _objects
  set view_name = 'view_LinkFreqPlan' ,
      isUseProjectID=0
      
  where name = 'LinkFreqPlan'
go

delete from _Objects
  where name='Link_Repeater'
go

insert into _Objects
(name, table_name_initial, display_name, IsUseProjectid, view_name) values
('Link_Repeater','Link_Repeater', '������������', 1, 'view_Link_Repeater')
go


update _objects
  set table_name='view_linkend_with_project_id'
where name='linkend'
go


update _objects
  set table_name='view_PMP_site_with_project_id',
      table_name_initial='view_PMP_site_with_project_id'
where name='PMP_site'
go


IF OBJECT_ID(N'[dbo].[view__object_fields_user]') IS NOT NULL
  DROP view [dbo].[view__object_fields_user]


GO

--------------------------------------------------------------



CREATE VIEW [dbo].[view__object_fields_user]
AS
SELECT TOP 100000 
       dbo._objects.name AS objname,
     --  dbo._objects.table_name,
       
       F.*,
       
       F.type AS field_type,
       F.name AS field_name
       
FROM dbo._object_fields_user F
     LEFT OUTER JOIN dbo._objects ON F.object_id =
     dbo._objects.id

go


-------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
updated: 2016.05.13
--------------------------------------------------------------- */

IF OBJECT_ID(N'[dbo].[sys_sp_Copy_table_record]') IS NOT NULL
  DROP procedure dbo.sys_sp_Copy_table_record
GO


--------------------------------
CREATE PROCEDURE dbo.sys_sp_Copy_table_record
(
  @TABLE_NAME VARCHAR(50)=null,
  @ID     INT=null,
  @NEW_ID INT=null
)
AS
BEGIN

/*  set @TABLE_NAME = 'link'
set @ID = 35455
set @NEW_ID = 58114

*/

  IF OBJECT_ID(@TABLE_NAME) is NULL
    RETURN -1

  DECLARE
    @sql VARCHAR(max),
 --   @exceptions VARCHAR(8000),  
  	@column_names VARCHAR(max);    
       

--  set @TABLE_NAME='link'
  /*
  
  set @exceptions = 
     ',id,name,guid,project_id,folder_id,'+
     'date_created,user_created,'+
     'date_modify,user_modify,'+
     'Linkend_id,PMP_SECTOR_ID,PMP_TERMINAL_ID,property_id,'+
     'pmp_sector_ID,pmp_terminal_ID,'+
     'LINKEND1_ID,LINKEND2_ID,'+
     'property1_id,property2_id,'+
     'length_km,kng_year,' 
*/

  set @column_names=''
   
  SELECT  @column_names=@column_names + ',' + column_name + '=m.' +column_name +  CHAR(13) + CHAR(10)
      --, * 
    FROM [INFORMATION_SCHEMA].[COLUMNS]
    WHERE (TABLE_NAME=@TABLE_NAME) 
        --   and charindex(','+column_name+',', @exceptions)=0
           and LEFT(column_name,1)<>'_'
           and LEFT(column_name,3)<>'---'
           and LEFT(column_name,2)<>'__'
           
           and column_name not in
               ('id','name','guid','project_id','folder_id',               
               'date_created','user_created',
               'date_modify','user_modify',
               'Linkend_id','PMP_SECTOR_ID','PMP_TERMINAL_ID','property_id',
               'pmp_sector_ID','pmp_terminal_ID',
               'LINKEND1_ID','LINKEND2_ID',
               'property1_id','property2_id',
               'length_km','kng_year'  ,
               'ELEVATION_angle'            
               
               )
             
           
           
           
 --   ORDER BY ORDINAL_POSITION  
    
  
    
  set @column_names = SUBSTRING(@column_names, 2, LEN(@column_names))


  set @SQL = 
    'UPDATE :TABLE_NAME set '+  CHAR(10) +
    + @column_names +
    ' from (select * from :TABLE_NAME WHERE id=:ID) m '+   CHAR(10) +
    ' WHERE (:TABLE_NAME.id = :new_id) ' +   CHAR(10) ;


  set @sql=REPLACE(@sql, ':TABLE_NAME', @table_name) 
  set @sql=REPLACE(@sql, ':ID',     CAST(@ID as VARCHAR(50))) 
  set @sql=REPLACE(@sql, ':new_id', CAST(@NEW_ID as VARCHAR(50))) 

--print @SQL
--select @SQL

  EXEC(@sql)

--  set @column_names = SUBSTRING(@column_names, 1, LEN(@column_names)-1)


--print @column_names



END

go



IF OBJECT_ID(N'[ui].[sp_Object_prepare_view]') IS  not  NULL
  DROP PROCEDURE [ui].[sp_Object_prepare_view]
GO
/****** Object:  StoredProcedure [ui].[sp_Object_prepare_view]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ui].[sp_Object_prepare_view]
(
--   @objname varchar(50) = 'PMP_SECTOR'
--   @objname varchar(50) = 'LinkEndType'
--   @objname varchar(50) = 'property'
   @objname varchar(50) = 'template_link'
   
--   @objname varchar(50) = 'PMP_LINK'
--   @objname varchar(50) = 'REL_MATRIX'
)

AS
BEGIN
/*
  if charindex('.',@TABLE_NAME_full)=0
    set @TABLE_NAME_full = 'dbo.'+@TABLE_NAME_full
 */

print @objname


declare
  @object_id int, 
  @columns varchar(max)='',
  @join varchar(max)='',
  @table_name_Initial varchar(50),
  
  @IsUseProjectID bit,
  
  @view_name   varchar(50),
  @table_name  varchar(50)



select 
       @object_id = id,
       @table_name_Initial = table_name_initial , 
       @table_name = table_name,
       @IsUseProjectID = IsUseProjectID,
       @view_name = 'dbo.'+view_name
  from _objects 
  where name=@objname
 

if CHARINDEX ('.',@table_name_initial)=0
  set @table_name_initial = 'dbo.' + @table_name_initial

--CHARINDEX ( expressionToFind , expressionToSearch [ , start_location ] )   


if @table_name_Initial is null
  RAISERROR ('@table_name_Initial is null', 16, 1 )
        
if IsNull(@view_name,'')=''
 set  @view_name = @table_name_Initial


/****** Script for SelectTopNRows command from SSMS  ******/

print @table_name_initial

declare 
 /* @is_project_id bit = IIF( Exists (SELECT *
                             FROM [INFORMATION_SCHEMA].[COLUMNS]
                             where  TABLE_NAME = @table_name_initial  and COLUMN_NAME = 'project_id' ) , 1,0),
*/



  @is_folder_id bit = IIF( Exists (SELECT *
                             FROM [INFORMATION_SCHEMA].[COLUMNS]
                             where
                                 (--TABLE_NAME = @table_name_initial 
                                  -- or 
                                  TABLE_SCHEMA+'.'+TABLE_NAME = @table_name_initial
                                 ) 
                                and COLUMN_NAME = 'folder_id' ) , 1,0)



if object_id(@table_name_Initial) is null
  RETURN -9999999


print @table_name_Initial

select MIN(id)  as id
    into #id
  from  dbo.view__object_fields_enabled
  where 
     objname = @objname
     and isnull(name,'') <> ''
  group by name



--return
set @columns = 'm.id, m.guid, ' + CHAR(10) 

/*
select @IsUseProjectID

return
*/

if @IsUseProjectID=1 
--if @is_project_id=1 
   set @columns = @columns + 'm.project_id,'+ CHAR(10) 

if @is_folder_id=1 
   set @columns = @columns +  'm.folder_id,'+ CHAR(10) 

------------------------------------------------------------------

select id, [name], [type] ,xref_tablename
   into #columns
  from  dbo.view__object_fields_enabled    
  where 
      id in (select * from #id)  and
      (
         Exists (SELECT *  FROM [INFORMATION_SCHEMA].[COLUMNS] 
                 where TABLE_SCHEMA+'.'+TABLE_NAME = @table_name_initial  and COLUMN_NAME =  [name]  )
       or  
         [type] = 'button' 

       or  
         [name] in ('lat','lon','lat1','lat2','lon1','lon2','linkendtype_name','band'  )  


      )
   
insert into #columns    
  select id, [name], [type] ,xref_tablename
    from dbo._object_fields_user
    where 
       object_id = @object_id 
       and IsNull(type,'') <> ''
       and [type]<>'delimiter'
   
  
/*  
select * from  #columns  
  
return
*/
   
/*

  select id, [name], [type] ,xref_tablename
    from dbo._object_fields_user
    where object_id = @object_id 

return  
*/
      
------------------------------------------------------------------


select 
   @columns = @columns + 'm.'+ [name] +
--   @columns = @columns + IIF(@columns='','',',')+ 'm.'+ [name] +
     case
        when type='list' then   replace( ', tbl_:name_'+ cast(id as varchar(10)) + '.value as :name_str',  ':name',  [name]  )          
   ---     when type='xref' then    ', '+ [xref_tablename]+ '.name as '+ [name]+'_str'     
        when type='xref' then   replace (replace ( ', :table_:id.name as '+ [name]+'_str'  , 
                                   ':id',    cast(id as varchar(10)) ),       
                                   ':table', xref_tablename)     
        
        else ''
      end  + ','+ CHAR(10) , 

   @join = @join +
     case
        when type in ('list','status') then 
         replace (replace (replace (' left join lib.ft_PickList ('':table'','':column'') tbl_:column_:id  on tbl_:column_:id.id = m.:column ',  
               ':table', @table_name_Initial  ),             
               ':id', cast(id as varchar(10))  ),             
               ':column', [name]) + CHAR(10) 

        when type ='xref' then 
          replace (replace (replace (' left join :table :table_:id on :table_:id.id = m.:column ',  
               ':id',    cast(id as varchar(10)) ),                    
               ':table', xref_tablename),                    
               ':column', [name]) + CHAR(10) 

        else ''
      end 


  from #columns 
/*  
  dbo.view__object_fields_enabled
  where 
      id in (select * from #id)  and
      (
         Exists (SELECT *  FROM [INFORMATION_SCHEMA].[COLUMNS] where TABLE_NAME = @table_name_initial  and COLUMN_NAME =  [name]  )
       or  
         [type] = 'button' 

       or  
         [name] in ('lat1','lat2','lon1','lon2')  


      )
      
*/      
--     objname = @objname
  --   and isnull(name,'') <> ''

if Charindex('DATE_CREATED', @columns)=0 -- and Charindex(@columns, 'DATE_CREATED')=0
  set @columns =  @columns + ' m.DATE_CREATED, m.DATE_MODIFY, m.USER_CREATED, m.USER_MODIFY' + CHAR(10) 
else
  set @columns =  @columns + '  m.USER_CREATED, m.USER_MODIFY' + CHAR(10) 



--set @columns =  @columns + ' m.DATE_CREATED, m.DATE_MODIFY, m.USER_CREATED, m.USER_MODIFY' + CHAR(10) 
--set @columns = 'm.id, ' + CHAR(10)+ @columns + CHAR(10) + ', m.DATE_CREATED, m.DATE_MODIFY, m.USER_CREATED, m.USER_MODIFY' + CHAR(10) 


--select @columns

--return


print @columns

--print @join

--

declare 
  @ui_view_name varchar(100) = 'ui.view_' + @objname 
--  @ui_view_name varchar(100) = 'ui.view_' + @table_name_Initial 
  
declare  
  @view varchar(max) = 'create view '+ @ui_view_name + CHAR(10)+ ' as '+ CHAR(10)+ ' select ' + @columns + ' from ' + @view_name + ' as m ' + CHAR(10) + @join
  
  
--  select @view



if object_id(@ui_view_name) is not null
   exec (' drop view '+ @ui_view_name)


 exec ( @view)


--print @view

--exec ( 'select top 1 * from '+@ui_view_name)


END

GO
/****** Object:  StoredProcedure [ui].[sp_Object_prepare_view_ALL]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[ui].[sp_Object_prepare_view_ALL]') IS  not  NULL

DROP PROCEDURE [ui].[sp_Object_prepare_view_ALL]
GO
/****** Object:  StoredProcedure [ui].[sp_Object_prepare_view_ALL]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ui].[sp_Object_prepare_view_ALL]
/*(
   --@objname varchar(50) = 'link'
   
    history.sp_Create_triggers_all 
)
*/
AS
BEGIN
  DECLARE 
    @RC int,
    @objname varchar(50)

declare 
  @tbl TABLE (objname varchar(50))


insert into @tbl (objname) values
('Antenna_Type'), 
('Cell_Layer') ,
('Clutter_Model'), 
('COLOR_SCHEMA'), 

('Calc_Model'), 
('TRX'), 
('Company'), 

('Combiner'), 
('terminal_type'), 

('pmp_site'), 
('PMP_SECTOR'), 
('PMP_terminal'), 

('property'), 
('link'), 
--('PMP_LINK'), 


('linkend'),  
('PROJECT'), 
('LinkEndType'), 
('LINKEND_antenna'), 
('linkline'), 
('Link_Repeater'),	
('link_sdb') ,
('LinkFreqPlan'), 

('map'), 

('REL_MATRIX'), 


('linknet'), 
('MSC'), 
('BSC'), 


('Link_Type'), 




('PMP_Calc_Region'),
('REPORT'),

('GEO_REGION'),

('template_link'),

('template_pmp_site'), 
('template_linkend'),
('template_linkend_antenna')





/*

select * from _objects


*/

while exists (SELECT * from @tbl)
begin
  select top 1 @objname = objname from @tbl
  delete top (1) from @tbl


  EXEC  [ui].[sp_Object_prepare_view]  @objname = @objname

end

/*
  EXEC @RC = [ui].[sp_Object_prepare_view] 
    @objname = 'link';

  EXEC @RC = [ui].[sp_Object_prepare_view] 
    @objname = 'linkend';

  EXEC @RC = [ui].[sp_Object_prepare_view] 
    @objname = 'property';
*/

END

GO
