


IF OBJECT_ID(N'[dbo].[fn_Object_Has_children]') IS NOT NULL
  DROP function [dbo].[fn_Object_Has_children]


GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE FUNCTION dbo.fn_Object_Has_children
(
  @NAME varchar(50),
  @ID  int
)
RETURNS bit
AS
BEGIN

 -- SELECT top 1 @ID = ID       
 --   FROM Property
  --  where id in (select top 1 Property_id from LinkEnd)
   
  

 -- set @Name='property';
  
--  set @Name=UPPER(@Name)    
  
 -- set @ID = dbo.fn_GetRandomPropertyID;
  
 -- SELECT top 1 @ID = ID       
 ----  FROM Property
  --  where id in (select top 1 Property_id from LinkEnd)
      
  -----------------------------------------------
  -- common
  -----------------------------------------------

  -----------------------------------------------
  if @NAME = 'property'
  -----------------------------------------------
  BEGIN      
    if EXISTS(SELECT * FROM LinkEnd WHERE property_id = @id)
      RETURN 1;

  --  if EXISTS(SELECT * FROM [Site] WHERE property_id = @id)
   --   RETURN 1;

    if EXISTS(SELECT * FROM MSC WHERE property_id = @id)
      RETURN 1;

    if EXISTS(SELECT * FROM BSC WHERE property_id = @id)
      RETURN 1;

    if EXISTS(SELECT * FROM PMP_site WHERE property_id = @id)
      RETURN 1;

    if EXISTS(SELECT * FROM view_Pmp_Terminal WHERE property_id = @id)
      RETURN 1;    

    if EXISTS(SELECT * FROM Link_Repeater  WHERE property_id = @id)
      RETURN 1;    


  END ELSE    

/*  -----------------------------------------------
  -- 2G
  -----------------------------------------------

  -----------------------------------------------
  if @NAME = 'site' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM Cell WHERE site_id = @id)
      RETURN 1 
  END ELSE    

  -----------------------------------------------
  if @NAME = 'cell' BEGIN
  -----------------------------------------------
    IF OBJECT_ID('view_Cell_antenna')>0
      if EXISTS(SELECT * FROM [view_Cell_antenna] WHERE cell_id = @id)
        RETURN 1 
  END ELSE    
*/

  -----------------------------------------------
  -- Link
  -----------------------------------------------

  -----------------------------------------------
  if @NAME = 'LinkFreqPlan' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM LinkFreqPlan_link  WHERE linkfreqplan_id= @id)
      RETURN 1
  END ELSE    

  -----------------------------------------------
  if @NAME = 'LinkEnd' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM  LINKEND_ANTENNA WHERE linkend_id= @id)
      RETURN 1
  END ELSE    

  -----------------------------------------------
  if @NAME = 'Link_Repeater' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM  LINK_Repeater_ANTENNA WHERE LINK_Repeater_id= @id)
      RETURN 1
  END ELSE    


  -----------------------------------------------
  if @NAME = 'Link' BEGIN
  -----------------------------------------------
      RETURN 1
  END ELSE    

  -----------------------------------------------
  if @NAME = 'Link_sdb' BEGIN
  -----------------------------------------------
 --  if EXISTS(SELECT * FROM Link.Link_sdb WHERE Linknet_id= @id)

      RETURN 1
  END ELSE    


  -----------------------------------------------
  if @NAME = 'LinkNet' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM LinkNet_Items_XREF WHERE Linknet_id= @id)
      RETURN 1
  END ELSE    

  -----------------------------------------------
  if @NAME = 'LinkLine' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM LinkLineLinkXREF WHERE linkline_id = @id)
      RETURN 1
  END ELSE    
  
  -----------------------------------------------
  -- PMP
  -----------------------------------------------

  -----------------------------------------------
  if (@NAME = 'PMP_Calc_Region') BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM pmp_CalcRegionCells  WHERE pmp_Calc_Region_id= @id)
      RETURN 1
  END ELSE    

  -----------------------------------------------
  if @NAME = 'PMP_site' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM view_PMP_Sector WHERE pmp_site_id = @id)
      RETURN 1
  END ELSE    
  

  
  -----------------------------------------------
  if @NAME = 'PMP_Sector' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM LINKEND_ANTENNA WHERE LINKEND_id = @id)
      RETURN 1
  END ELSE    

  -----------------------------------------------
  if @NAME = 'PMP_Terminal' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM LINKEND_ANTENNA WHERE LINKEND_id = @id)
      RETURN 1
  END ELSE    

  -----------------------------------------------
  -- Template
  -----------------------------------------------
  
  -----------------------------------------------
  if @NAME = 'Template_Site' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM Template_Site_Linkend WHERE template_site_id = @id)
      RETURN 1
  END ELSE   


/*

  -----------------------------------------------
  if @NAME = 'mast' BEGIN
  -----------------------------------------------
  RETURN 1
--    if EXISTS(SELECT * FROM Linkend_antenna WHERE mast_id = @id)
  --    RETURN 1
  END ELSE   
*/

/*
  -----------------------------------------------
  if @NAME = 'Template_Site_mast' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM lib.Template_Site_Linkend_antenna WHERE template_site_mast_id = @id)
      RETURN 1
  END ELSE  
   */


  -----------------------------------------------
  if @NAME = 'Template_Site_Linkend' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM Template_Site_LinkEnd_Antenna WHERE template_site_Linkend_id = @id)
      RETURN 1
  END ELSE   


  -----------------------------------------------
  -- Template
  -----------------------------------------------


  -----------------------------------------------
  if @NAME = 'Template_pmp_Site' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM Template_Linkend WHERE template_pmp_site_id = @id)
      RETURN 1
  END ELSE    
  
  -----------------------------------------------
  if @NAME = 'Template_LinkEnd' BEGIN
  -----------------------------------------------
    if EXISTS(SELECT * FROM Template_LinkEnd_Antenna WHERE template_LinkEnd_id = @id)
      RETURN 1
   END   

  
-- RAISERROR ('@TableName incorrect:', 16, 1 );
  
 RETURN 0; 
  
END

go



------------------------------------------------------------------

IF OBJECT_ID(N'[dbo].[sp_Explorer_Select_Object]') IS NOT NULL
  DROP procedure [dbo].[sp_Explorer_Select_Object]


GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Explorer_Select_Object
(
  @OBJNAME 			varchar(50),
  @PROJECT_ID  	int = null
)

AS
BEGIN

if @OBJNAME is null 
BEGIN
 -- set @OBJNAME = 'Antenna_Type';
  set @PROJECT_ID=1406
  set @OBJNAME = 'Link_Repeater';
  set @OBJNAME = 'template_link';

  
END	 


  declare 
  	@IsUseProjectID bit;
   -- @max_id int;


  SET ANSI_WARNINGS OFF --String or binary data would be truncated.

 -- if @OBJName IS NULL
   --  set @OBJName='LinkEndType'


DECLARE
  @tables table
                      (
                       objname    	varchar(100),
                       table_Name 	varchar(50)
                      )





DECLARE
  @temp table
                      (id     		int, 
                       name    		nvarchar(100),
                       is_folder	bit DEFAULT 0,
                       ObjName 		varchar(50),
                       
                       GUID    		uniqueidentifier,
                       parent_GUID  uniqueidentifier,
                       
                       property_name  	  varchar(200),
                    
                       folder_image_index int DEFAULT -1  --test
                       );

 

	  SELECT @IsUseProjectID =IsUseProjectID
    -- @table_name = table_name    @table_name VARCHAR(255),
    	FROM _objects
      WHERE (Name=@ObjName) 

 

  -- IF @IsUseProjectID = 0 
   --  set @PROJECT_ID = null 

  PRINT @PROJECT_ID





--SELECT * FROM	Folder	 WHERE (@PROJECT_ID is null)

 
/*
    select 
       id,Name,type, id,parent_id, 1,0
--	  SELECT 
  --  	  id,Name,type, id,parent_id, 1,0
      
  	FROM Folder
    --VIEW_Explorer_Folder
    WHERE (type=@ObjName) 
         and ((@PROJECT_ID is null) or (PROJECT_ID=@PROJECT_ID) )
    ORDER BY name asc;


return 
 */
 
 
 

 
    INSERT INTO @temp 
       (id,name,ObjName, GUID, parent_GUID, is_folder, folder_image_index)
	  SELECT 
    	  m.id,m.Name,m.type, m.GUID, s.GUID, 1,0
      
  	FROM Folder M
         LEFT OUTER JOIN Folder S ON m.parent_id = s.id

    --VIEW_Explorer_Folder
    WHERE (m.type=@ObjName) 
--          and ((@PROJECT_ID is null) or (m.PROJECT_ID=@PROJECT_ID) )
          and 
             ( ( IsNull(@IsUseProjectID,0) = 0) or (m.PROJECT_ID=@PROJECT_ID) )
          
    ORDER BY m.name;-- asc;

/*
  SELECT * FROM @temp
  
  return @@ROWCOUNT
  


 return */

 -- select @max_id=max(id)+1 from @temp; --  	Folder; 
  
  

  
-- 

-- test -----
-- sELECT * FROM #temp
-- return;  
--------  
 
INSERT INTO @tables (objname, table_Name) VALUES ('Antenna_Type', 'AntennaType')
INSERT INTO @tables (objname, table_Name) VALUES ('LinkEndType', 'LinkEndType')
INSERT INTO @tables (objname, table_Name) VALUES ('Calc_Model', 'CalcModel')
INSERT INTO @tables (objname, table_Name) VALUES ('combiner', 'Passive_Component')
INSERT INTO @tables (objname, table_Name) VALUES ('CELL_LAYER', 'CELLLAYER')
INSERT INTO @tables (objname, table_Name) VALUES ('Terminal_Type', 'TerminalType')

INSERT INTO @tables (objname, table_Name) VALUES ('Geo_Region', 'GeoRegion')
INSERT INTO @tables (objname, table_Name) VALUES ('template_pmp_Site', 'template_pmp_Site')

INSERT INTO @tables (objname, table_Name) VALUES ('Link_Type', 'LinkType')

INSERT INTO @tables (objname, table_Name) VALUES ('Link', 'Link')
INSERT INTO @tables (objname, table_Name) VALUES ('LinkEnd', 'LinkEnd')

INSERT INTO @tables (objname, table_Name) VALUES ('TEMPLATE_link', 			'lib.TEMPLATE_LINK')
INSERT INTO @tables (objname, table_Name) VALUES ('TEMPLATE_SITE', 			'TEMPLATE_SITE')

INSERT INTO @tables (objname, table_Name) VALUES ('TEMPLATE_SITE_LINKEND', 	'TEMPLATE_SITE_LINKEND')
INSERT INTO @tables (objname, table_Name) VALUES ('TEMPLATE_SITE_LINKEND_antenna', 'TEMPLATE_SITE_LINKEND_antenna')





  ---------------------------------------------
  IF @OBJName='Antenna_Type'
  ---------------------------------------------
  BEGIN           
  
 -- print ''
       
    INSERT INTO @temp (id,name,ObjName, guid,  parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid,  F.guid
      
        FROM AntennaType M 
             LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
        ORDER BY M.name;      
        
	END ELSE	

  ---------------------------------------------
  IF @OBJName='LinkEndType'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid,  F.guid
      
        FROM LinkEndType M 
           LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
        ORDER BY M.name;
	END ELSE


  ---------------------------------------------
  IF @OBJName='Calc_Model'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid, F.guid
      
        FROM CalcModel M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
        ORDER BY M.name;
	END ELSE	

  ---------------------------------------------
  IF @OBJName='combiner'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid,  parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid, F.guid
      
        FROM Passive_Component M 
           LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
        ORDER BY M.name;
	END ELSE

  ---------------------------------------------
  IF @OBJName='CELL_LAYER'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid,  parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid, F.guid
      
        FROM CELLLAYER M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
        ORDER BY M.name;
	END ELSE


  ---------------------------------------------
  IF @OBJName='color_Schema'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid,  F.guid
      
        FROM ColorSchema M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
        ORDER BY M.name;
  END ELSE

 ---------------------------------------------
  IF @OBJName='Terminal_Type'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid,  parent_GUID)
      SELECT M.id, M.name, @OBJName, M.guid, F.guid
      
        FROM TerminalType M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
         ORDER BY M.name;

	END ELSE

 ---------------------------------------------
  IF @OBJName='template_pmp_Site'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT M.id, M.name, @OBJName, M.guid, F.guid
      
        FROM Template_pmp_Site M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
         ORDER BY M.name;

  END ELSE

  ---------------------------------------------
  IF @OBJName='Clutter_model'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid, F.guid
      
        FROM Cluttermodel M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
        ORDER BY M.name;
  END ELSE

  ---------------------------------------------
  IF @OBJName='Geo_Region'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid,  parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid, F.guid
      
        FROM GeoRegion M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
        ORDER BY M.name;
  END ELSE

  ---------------------------------------------
  IF @OBJName='Link_Type'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid,  F.guid
      
        FROM LinkType M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
        ORDER BY M.name;
  END ELSE



  ---------------------------------------------
  -- WITH PROJECT_ID
  ---------------------------------------------

  ---------------------------------------------
  IF @OBJName='Link'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid,  F.guid
      
        FROM Link M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
         WHERE M.PROJECT_ID=@PROJECT_ID
         ORDER BY M.name;
  END ELSE
  
  ---------------------------------------------
  IF @OBJName='LinkLine'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid, F.guid
      
        FROM LinkLine M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
         WHERE M.PROJECT_ID=@PROJECT_ID
         ORDER BY M.name;
  END ELSE

  ---------------------------------------------
  IF @OBJName='Property'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid,  F.guid
      
        FROM Property M 
           LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
         WHERE M.PROJECT_ID=@PROJECT_ID
         ORDER BY M.name;
  END  ELSE
 
  ---------------------------------------------
  IF @OBJName='msc'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid, F.guid
      
        FROM msc M 
          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
         WHERE M.PROJECT_ID=@PROJECT_ID
         ORDER BY M.name;
  END  ELSE

  ---------------------------------------------
  IF @OBJName='pmp_site'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT  M.id, M.name, @OBJName, M.guid, F.guid
      
        FROM view_pmp_site M 
           LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
         WHERE M.PROJECT_ID=@PROJECT_ID
         ORDER BY M.name;
  END ELSE
  
  

  
 ---------------------------------------------
  IF @OBJName='template_link'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT M.id, M.name, @OBJName, guid, null
      
--      M.guid, F.guid
      
        FROM lib.Template_link M 
        
--          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
         ORDER BY M.name;

  END ELSE  
  
  
 ---------------------------------------------
  IF @OBJName='template_Site'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT M.id, M.name, @OBJName, guid, null
      
--      M.guid, F.guid
      
        FROM Template_Site M 
        
--          LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
         ORDER BY M.name;

  END ELSE  
  
  

 ---------------------------------------------
  IF @OBJName='template_Site_linkend'
  ---------------------------------------------
  BEGIN                        
    INSERT INTO @temp (id,name,ObjName, guid, parent_GUID)
      SELECT M.id, M.name, @OBJName, M.guid, null  --F.guid
      
        FROM Template_Site_linkend M 
     --     LEFT OUTER JOIN Folder F ON M.Folder_id = F.id
         ORDER BY M.name;

  END ELSE  
 
 ---------------------------------------------
  IF @OBJName='Link_Repeater'
  ---------------------------------------------
  BEGIN  
    print   'Link_Repeater'
                      
       
    INSERT INTO @temp (id,name,ObjName, guid,  parent_GUID, property_name)

      SELECT  M.id, M.name, @OBJName, M.guid,  null, property.name
        FROM view_Link_Repeater_with_project_id M 
        
             LEFT OUTER JOIN property ON property.id = M.property_id        
                
         WHERE M.PROJECT_ID=@PROJECT_ID
         ORDER BY M.name;
             
         
  END  ELSE        
    RAISERROR ('sp_Explorer_Select_Object - @OBJNAME not found: %s' , 16, 1, @OBJNAME );
 


  SELECT * FROM @temp
  
  return @@ROWCOUNT
  
END

go


-----------------------------------------------------------

IF OBJECT_ID(N'[dbo].[sp_Explorer_Select_Folder_Items]') IS NOT NULL
  DROP procedure [dbo].[sp_Explorer_Select_Folder_Items]


GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Explorer_Select_Folder_Items 
(
  @ObjName 			varchar(30),
  @FOLDER_ID   int,  
  @PROJECT_ID  int 
 -- @ID  			 	int
)

AS
BEGIN

if @ObjName is null
begin
  --set @ObjName = 'property'

--  set @ObjName = 'antenna_type'
--  set @PROJECT_ID =2055 -- (select top 1 id from Project order by id desc)
  
 -- set @FOLDER_ID = (select top 1 id from Folder where [type] = 'property' and  PROJECT_ID = @PROJECT_ID )

  set @FOLDER_ID = 2134
  set @ObjName = 'link'

  if @PROJECT_ID is null
     select @PROJECT_ID =project_id from FOLDER where id= @folder_id

end



  DECLARE
  	@IsUseProjectID bit,
    @IsUseCheck bit,
    @IsUseGeoRegion_ID bit,
    @IsUseGeoRegion1_ID bit,
    @IsUseGeoRegion2_ID bit,
    @s VARCHAR(50), 
    @sql NVARCHAR(500), 
    @table_name VARCHAR(50), 
    @GUID  uniqueidentifier;


/*  create TABLE #temp  (id      int, 
                       name    varchar(100),
                       ObjName varchar(50),
                       GUID    uniqueidentifier 
                      -- parent_GUID  uniqueidentifier
                       );
*/
 -- create TABLE #temp1 (id int);

  if @FOLDER_ID=0
    set @FOLDER_ID=NULL

-- set @ID = 506046; 

--  set @ObjName='property';
 -- set @ID = dbo.fn_GetRandomPropertyID;
  
  
  SELECT 
   --   @IsUseProjectID = IsUseProjectID,
      @table_name     = table_name
    FROM _objects
    WHERE (Name=@ObjName) 


print  @table_name


if @ObjName='link'      set @table_name='view_Link_base'
--if @ObjName='link'      set @table_name='view_Link_for_explorer'




if @table_name='pmp_site'      set @table_name='view_pmp_site_with_project_id' 
if @ObjName='pmp_terminal'     set @table_name='view_pmp_terminal' 
if @ObjName='linkend'          set @table_name='view_linkend_with_project_id' 
if @ObjName='LinkEnd_ANTENNA'  set @table_name='view_LinkEnd_ANTENNA' 

if @table_name='relief'      set @table_name='view_Relief' 



  ---------------------------------------------
  IF OBJECT_ID(@table_name) is null  RAISERROR ('OBJECT_ID is null', 16, 1);
  ---------------------------------------------



print  @table_name




--SELECT @IsUseProjectID, @table_name  

  set @IsUseCheck    = dbo.sys_fn_TableHasColumn(@table_name, 'Checked' )

print @IsUseCheck 

   
  set @IsUseProjectID    = dbo.sys_fn_TableHasColumn(@table_name, 'Project_ID' )
  
 -- select @IsUseProjectID, @table_name
  
--  return
  
  
--  set @IsUseGeoRegion_ID = dbo.sys_fn_TableHasColumn(@table_name, 'GeoRegion_ID' )

/*
  set @IsUseGeoRegion1_ID = dbo.sys_fn_TableHasColumn(@table_name, 'GeoRegion1_ID' )
  set @IsUseGeoRegion2_ID = dbo.sys_fn_TableHasColumn(@table_name, 'GeoRegion2_ID' )
*/

print @IsUseProjectID 


/*declare
  @name_format varchar(200) = (select [value] from SETTINGS where name='explorer_property_format')


print @name_format
*/


--  set @IsUseProjectID = sys_fn_TableHasColumn(@table_name, 'GeoRegion_ID' )
  


  -- select * from [INFORMATION_SCHEMA].[COLUMNS] WHERE (table_name=@table_name) and (column_name='GeoRegion_ID')

  /*
  set @IsUseGeoRegion_ID = 0
  if @ObjName='property'  
    set @IsUseGeoRegion_ID = 1 
    */
    
    
   
--  set @sql='SELECT id,guid,name, user_created,'+
 
  set @sql='SELECT id,guid,%name%, '+
           ' dbo.fn_Object_Has_children ('':ObjName'', id) as Has_children, ' +
           ' '':ObjName'' as ObjName'+
--           '  :GeoRegion_ID '+
--      //     '  :GeoRegion1_ID'+
--       //    '  :GeoRegion2_ID'+
           '  :checked'+
           ' FROM :table_name '+
           ' WHERE (1=1) '

/*
  if @ObjName = 'property'
    set @sql=REPLACE(@sql, '%name%', @name_format + ' as name') 
  else
 */
  
    set @sql=REPLACE(@sql, '%name%', 'name') 

 
  set @sql=REPLACE(@sql, ':ObjName', @ObjName) 
  set @sql=REPLACE(@sql, ':table_name', @table_name) 

  
  SET @s = CASE @IsUseCheck WHEN 1 THEN ', Checked ' ELSE '' END
  set @sql=REPLACE(@sql, ':Checked', @s)  
  
  /*
  SET @s = CASE @IsUseGeoRegion_ID WHEN 1 THEN ', GeoRegion_ID ' ELSE '' END
  set @sql=REPLACE(@sql, ':GeoRegion_ID', @s) 

  SET @s = CASE @IsUseGeoRegion1_ID WHEN 1 THEN ', GeoRegion1_ID ' ELSE '' END
  set @sql=REPLACE(@sql, ':GeoRegion1_ID', @s) 
  
  SET @s = CASE @IsUseGeoRegion2_ID WHEN 1 THEN ', GeoRegion2_ID ' ELSE '' END
  set @sql=REPLACE(@sql, ':GeoRegion2_ID', @s) 
*/
  

/*
  if @IsUseGeoRegion_ID=1 
    set @sql=REPLACE(@sql, ':GeoRegion_ID', ', GeoRegion_ID ') 
  ELSE
    set @sql=REPLACE(@sql, ':GeoRegion_ID', '') 
*/ 

 -- exec sp_executesql @sql

 
  if @IsUseProjectID=1 
     set @sql=@sql+ ' AND (PROJECT_ID=' + cast (@PROJECT_ID as varchar (50)) + ')'

  if @FOLDER_ID is not NULL 
     set @sql=@sql+ ' AND (FOLDER_ID=' + cast (@FOLDER_ID as varchar (50))+ ')'
  ELSE 
     set @sql=@sql+ ' AND (FOLDER_ID IS NULL)'
     
     
   set @sql=@sql+ ' ORDER BY name' 
  
--  SELECT @sql 

print @sql
  
  exec sp_executesql @sql


  return @@ROWCOUNT

 -- SELECT * FROM #temp

 -- drop TABLE #temp;
 -- drop TABLE #temp1;
  
END


go




IF OBJECT_ID(N'[dbo].[view_Link_for_explorer]') IS NOT NULL
  DROP view [dbo].[view_Link_for_explorer]


GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */

CREATE VIEW dbo.view_Link_for_explorer
AS
SELECT 
  dbo.Link.id,
  dbo.Link.name,

  dbo.Link.project_id,  --!!!!!!!!!!!!!!
  dbo.Link.guid,
  
  dbo.Link.folder_id,
  dbo.Link.user_created,
  
  Property1.id AS Property1_id,
  Property2.id AS Property2_id,
  
  Property1.georegion_id AS georegion1_id,
  Property2.georegion_id AS georegion2_id

  
FROM
  dbo.Link

  left JOIN dbo.linkend linkend1 ON dbo.Link.linkend1_id = linkend1.id
  left JOIN dbo.linkend linkend2 ON dbo.Link.linkend2_id = linkend2.id

  left JOIN dbo.Property Property1 ON linkend1.Property_id = Property1.id
  left JOIN dbo.Property Property2 ON linkend2.Property_id = Property2.id

go
  
/*WHERE
  dbo.Link.objname = 'link' OR 
  Link.objname IS NULL
  */



/****** Object:  View [Security].[View_User_Security_XREF]    Script Date: 18.01.2021 14:15:25 ******/
IF OBJECT_ID(N'[dbo].[sp_Explorer_Select_SubItems]') IS NOT NULL
  DROP procedure [dbo].[sp_Explorer_Select_SubItems]


GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE [dbo].[sp_Explorer_Select_SubItems]
(
  @ObjName varchar(50),
  @ID  int  ,
  
  @PROJECT_ID  int = null
    
)

AS
BEGIN
  DECLARE
  	@link_id int,
    @GeoRegion_ID int,
    @parent_guid uniqueidentifier,
    @linkend1_id int,
    @linkend2_id int;



  create TABLE #temp  (id      			int, 
                       name    			nvarchar(100),
                       ObjName 			varchar(50),
                       GUID    			uniqueidentifier,
                       parent_GUID  uniqueidentifier,
                       Has_children BIT,
                       
                       ref_id      	int, 
                       ref_GUID    	uniqueidentifier, 
                       ref_ObjName 	varchar(50),

                       GeoRegion_ID int,
                       GeoRegion1_ID int,
                       GeoRegion2_ID int
                      );

 
 -----------------------------------------------
  if @ObjName = 'Link_SDB'
  -----------------------------------------------
  BEGIN
   INSERT INTO #temp (id,name,guid,ObjName) 
      SELECT id, 'common band: '+ name, guid, 'Link' --, @parent_guid, P.GeoRegion_ID 
      -- + name
        FROM  link 
        where id in (select link1_id from dbo.link_sdb where id=@ID)
 
   INSERT INTO #temp (id,name,guid,ObjName) 
      SELECT id, 'E band:            '+ name, guid, 'Link' --, @parent_guid, P.GeoRegion_ID 
      -- +name
        FROM  link 
        where id in (select link2_id from dbo.link_sdb where id=@ID)
 
  
--    SELECT @parent_guid = GUID FROM LinkEnd where id=@ID
   
           
/* 
   INSERT INTO #temp (id,name,guid,ObjName) --, parent_guid, GeoRegion_ID
      SELECT id, name, guid, 'Link_SDB' --, @parent_guid, P.GeoRegion_ID 
        FROM  link.link_sdb -- LinkEnd_antenna A LEFT OUTER JOIN
                --      Property P ON A.property_id = P.id
          
        WHERE project_id=@PROJECT_ID 
        ORDER BY name; 
*/
  end ELSE  


  -----------------------------------------------
  if @ObjName = 'LinkEnd'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM LinkEnd where id=@ID
   
           
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid, 'LinkEnd_ANTENNA', @parent_guid, P.GeoRegion_ID 
        FROM LinkEnd_antenna A LEFT OUTER JOIN
                      Property P ON A.property_id = P.id
          
        WHERE A.LinkEnd_id=@ID 
        ORDER BY A.name; 

  end ELSE  

  -----------------------------------------------
  if @ObjName = 'property'
  -----------------------------------------------
  BEGIN    
    SELECT @parent_guid = GUID, @GeoRegion_ID = GeoRegion_ID 
      FROM Property where id=@ID
 
		-------------------------
		-- LinkEnd
		------------------------- 
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'LinkEnd',@parent_guid,
              dbo.fn_Object_Has_children ('LinkEnd',id) as Has_children, 
              @GeoRegion_ID
      FROM view_LinkEnd_base  
        WHERE property_id=@ID 
        ORDER BY name;
  

    ---------------

/*    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'Site',@parent_guid, 
              dbo.fn_Object_Has_children ('Site',id) as Has_children,
              @GeoRegion_ID 
        FROM Site WHERE property_id=@ID ORDER BY name;
*/

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'MSC',@parent_guid, 
              dbo.fn_Object_Has_children ('Site',id) as Has_children,
              @GeoRegion_ID 
      FROM MSC WHERE property_id=@ID ORDER BY name;

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'BSC',@parent_guid, 
              dbo.fn_Object_Has_children ('BSC',id) as Has_children,
              @GeoRegion_ID 
      FROM BSC WHERE property_id=@ID ORDER BY name;

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'pmp_site',@parent_guid, 
              dbo.fn_Object_Has_children ('pmp_site',id) as Has_children,
              @GeoRegion_ID 
      FROM pmp_site WHERE property_id=@ID ORDER BY name;

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'pmp_terminal',@parent_guid, 
              dbo.fn_Object_Has_children ('pmp_terminal',id) as Has_children,
              @GeoRegion_ID 
      FROM view_pmp_terminal WHERE property_id=@ID ORDER BY name;

     ----- new -----

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'link_repeater',@parent_guid, 
              dbo.fn_Object_Has_children ('link_repeater',id) as Has_children,
              @GeoRegion_ID 
      FROM link_repeater WHERE property_id=@ID ORDER BY name;


/*
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children )
      SELECT id,name,guid,'mast',@parent_guid, 
              dbo.fn_Object_Has_children ('mast',id) as Has_children
               
      FROM mast WHERE property_id=@ID ORDER BY name;

*/



		-------------------------
		-- link
		-------------------------
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion1_ID, GeoRegion2_ID)
      SELECT id,name,guid,'link',@parent_guid,
             dbo.fn_Object_Has_children ('link',id) as Has_children,
             GeoRegion1_ID, GeoRegion2_ID 

       -- FROM view_link  
        FROM view_Link_for_explorer
        WHERE  (Property1_id=@ID) or (Property2_id=@ID)
        ORDER BY name
    


  end ELSE
   
  -----------------------------------------------
  if @ObjName = 'Link'
  -----------------------------------------------
  BEGIN
    DECLARE
 	  @link_repeater_id int;   
    
  --  SELECT @GUID = GUID FROM Link where id=@ID
    SELECT @parent_guid=GUID, @linkend1_id=linkend1_id, @linkend2_id=linkend2_id 
    --, @link_repeater_id=link_repeater_id
      FROM Link 
      WHERE id=@ID


    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT L.id, L.name, L.guid,'LinkEnd', @parent_guid, 
           dbo.fn_Object_Has_children ('LinkEnd',L.id) as Has_children,
           P.GeoRegion_ID 
      FROM LinkEnd L LEFT OUTER JOIN
                      Property P ON L.property_id = P.id
      WHERE L.id IN (@linkend1_id)        
      ORDER BY L.name; 
      

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT L.id, L.name, L.guid,'LinkEnd', @parent_guid, 
           dbo.fn_Object_Has_children ('LinkEnd',L.id) as Has_children,
           P.GeoRegion_ID 
      FROM LinkEnd L LEFT OUTER JOIN
                      Property P ON L.property_id = P.id
      WHERE L.id IN ( @linkend2_id)        
      ORDER BY L.name; 


      
/*      
                INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid, 'LinkEnd_ANTENNA', @parent_guid, P.GeoRegion_ID 
        FROM LinkEnd_antenna A LEFT OUTER JOIN
                      Property P ON A.property_id = P.id
          
*/
      
      

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Link_Repeater',@parent_guid,
           dbo.fn_Object_Has_children ('Link_Repeater',id) as Has_children 
      FROM Link_Repeater 
--      WHERE id = @link_repeater_id
      WHERE link_id = @id
      
  end ELSE

  -----------------------------------------------
  if @ObjName = 'LinkLine_Link'
  -----------------------------------------------
  BEGIN
  --  SELECT @GUID = GUID FROM Link where id=@ID
    SELECT @link_id = link_id FROM LinkLineLinkXREF where id=@ID;
    
    SELECT @parent_guid=GUID, @linkend1_id=linkend1_id, @linkend2_id=linkend2_id 
      FROM Link WHERE id=@ID


    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'LinkEnd',@parent_guid,
           dbo.fn_Object_Has_children ('LinkEnd',id) as Has_children 
      FROM LinkEnd 
      WHERE id IN (@linkend1_id,@linkend2_id)
      ORDER BY name; 



  end ELSE
 

  -----------------------------------------------
  if @ObjName = 'LinkLine'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM LinkLine where id=@ID


    SELECT X.*, Link.name, Link.GUID as Link_GUID
       INTO #temp1
    FROM Link RIGHT OUTER JOIN
          LinkLineLinkXREF X ON Link.id = X.link_id
    WHERE LinkLine_id=@ID      
    ORDER BY X.index_


 --   SELECT * from ##temp1


    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, 
    		ref_id, ref_ObjName, ref_guid,   Has_children)
      SELECT id,name,guid, 'LinkLine_Link', @parent_guid,
            LINK_id, 'LINK', Link_GUID, 1
      FROM #temp1  
     
  end ELSE   
  
  

  -----------------------------------------------
  if @ObjName = 'Link_Repeater'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM LInk_Repeater where id=@ID
   
           
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid)
      SELECT id,name,guid,'LInk_Repeater_ANTENNA',@parent_guid 
        FROM LInk_Repeater_antenna   
        WHERE  LInk_Repeater_id=@ID ORDER BY name; 

  end ELSE    
  
  -----------------------------------------------
  if @ObjName = 'pmp_sector'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM view_PMP_Sector where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid,'LinkEnd_ANTENNA', @parent_guid , P.GeoRegion_ID
        FROM LinkEnd_ANTENNA A LEFT OUTER JOIN
                      Property P ON A.property_id = P.id
        WHERE A.LinkEnd_id=@ID
      
     
      
      /*
      
                INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid, 'LinkEnd_ANTENNA', @parent_guid, P.GeoRegion_ID 
        FROM LinkEnd_antenna A LEFT OUTER JOIN
                      Property P ON A.property_id = P.id*/
      
      
  end ELSE    
  
  -----------------------------------------------
  if (@ObjName = 'pmp_terminal') --or (@ObjName = 'pmpTerminal')
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM view_Pmp_Terminal where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid,'LinkEnd_ANTENNA', @parent_guid, P.GeoRegion_ID 
      FROM LinkEnd_ANTENNA A  LEFT OUTER JOIN
                      Property P ON A.property_id = P.id
      WHERE A.LinkEnd_id=@ID
     
      
    --  ORDER BY name; 

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid)
      SELECT id,name,guid,'link',@parent_guid
        FROM link  
        WHERE  @ID = linkend2_id
--        WHERE  @ID in (linkend1_id, linkend2_id)
    
  end ELSE  
  

  -----------------------------------------------
  if @ObjName = 'LinkFreqPlan'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM LinkFreqPlan where id=@ID
    

    SELECT m.*, Link.name as name, Link.GUID as Link_GUID
       INTO #temp2       
      FROM LinkFreqPlan_Link M 
         LEFT OUTER JOIN Link ON M.link_id = dbo.Link.id        
      WHERE m.LinkFreqPlan_id=@ID 
      ORDER BY name;
    
 --   SELECT * from #temp2
    
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, 
    		ref_id, ref_guid, ref_ObjName)
      SELECT M.id, Link.name, M.guid,'LinkFreqPlan_link',@parent_guid, 1,  
            M.link_id, Link.GUID,  'link'      
        FROM LinkFreqPlan_Link M 
           LEFT OUTER JOIN Link ON M.link_id = dbo.Link.id
        
        WHERE m.LinkFreqPlan_id=@ID 
        ORDER BY name;

  END ELSE

  -----------------------------------------------
  if @ObjName = 'LinkNet'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM LinkNet where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'LinkFreqPlan',@parent_guid, 
              dbo.fn_Object_Has_children ('LinkFreqPlan',id) as Has_children 
      FROM LinkFreqPlan 
      	WHERE LinkNet_id=@ID ORDER BY name;
      

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, ref_id, ref_ObjName,   Has_children)
      SELECT id,name,guid,'LINKNET_LINKLINE_REF',@parent_guid, 
              LINKLINE_id,  'LINKLINE',
              dbo.fn_Object_Has_children ('LINKLINE',LINKLINE_id) as Has_children 
      FROM VIEW_SH_LINKNET_LINKLINE 
      	WHERE LinkNet_id=@ID ORDER BY name;


    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, ref_id, ref_ObjName, Has_children)
      SELECT id,name,guid,'LINKNET_LINK_REF',@parent_guid, 
              LINK_id,'LINK',      
              dbo.fn_Object_Has_children ('LINK',LINK_id) as Has_children 
      FROM VIEW_SH_LINKNET_LINK 
      	WHERE LinkNet_id=@ID ORDER BY name;
           
  end  ELSE



  
  -----------------------------------------------
  if @ObjName = 'template_site'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM template_site where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_site_LinkEnd',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_site_LinkEnd',id) as Has_children 

      FROM Template_site_LinkEnd  
      WHERE  Template_Site_id=@ID
      
     
 /*   ---------------
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children )
      SELECT id,name,guid,'Template_site_mast',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_site_mast',id) as Has_children
             
     
      FROM Template_site_mast
      WHERE  Template_Site_id=@ID
     
      */
      
  end ELSE   

/*
  -----------------------------------------------
  if @ObjName = 'template_site_mast'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM lib.template_site_mast where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_site_LinkEnd_antenna',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_site_LinkEnd_antenna',id) as Has_children 

      FROM lib.Template_site_LinkEnd_antenna  WHERE  Template_site_mast_id=@ID
      
  end ELSE   
*/

  
  -----------------------------------------------
  if @ObjName = 'template_pmp_site'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM template_pmp_site where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_LinkEnd',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_LinkEnd',id) as Has_children 

      FROM Template_LinkEnd  
      WHERE  Template_pmp_Site_id=@ID
      
  end ELSE   


  -----------------------------------------------
  if @ObjName = 'template_site_linkend'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM template_site_linkend where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_site_LinkEnd_antenna',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_site_LinkEnd_antenna',id) as Has_children 

      FROM Template_site_LinkEnd_antenna  WHERE  Template_site_LinkEnd_id=@ID
      
  end ELSE   

  -----------------------------------------------
  if @ObjName = 'template_linkend'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM template_linkend where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_LinkEnd_antenna',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_LinkEnd_antenna',id) as Has_children 

      FROM Template_LinkEnd_antenna  WHERE  Template_LinkEnd_id=@ID
      
  end ELSE   


/*
  -----------------------------------------------
  if @ObjName = 'template_site_linkend'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM template_site_linkend where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_LinkEnd_antenna',@parent_guid, 
              dbo.fn_Object_Has_children ('template_site_linkend',id) as Has_children 

      FROM template_site_linkend  WHERE  Template_LinkEnd_id=@ID
      
  end ELSE   */


  
  
   -----------------------------------------------
  if @ObjName = 'site' BEGIN
  -----------------------------------------------
    SELECT @parent_guid = GUID FROM [site] where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Cell',@parent_guid, 
              dbo.fn_Object_Has_children ('cell',id) as Has_children 

      FROM cell 
      WHERE Site_id=@ID
      
  END ELSE      

   -----------------------------------------------
  if @ObjName = 'cell' BEGIN
  -----------------------------------------------
    SELECT @parent_guid = GUID FROM [cell] where id=@ID
    
    if OBJECT_ID('view_Cell_Antennas')>0
    BEGIN
      INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
        SELECT id,name,guid,'Cell_antenna',@parent_guid, 
                0 as Has_children 

        FROM view_Cell_Antennas 
        WHERE cell_id=@ID
    END   
      
  END ELSE      


  -----------------------------------------------
  if (@ObjName = 'calc_region_pmp') or (@ObjName = 'pmp_calc_region')
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM pmp_CalcRegion where id=@ID


    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children,
          ref_id, ref_ObjName, ref_guid )
      SELECT m.id,PMP_Site.name,m.guid,'PMP_calc_region_Site',@parent_guid, 
--      SELECT m.id,PMP_Site.name,m.guid,'PMP_Site',@parent_guid, 
              dbo.fn_Object_Has_children ('PMP_Site',PMP_Site.id) as Has_children , 
              
           PMP_Site.id,'PMP_Site',  PMP_Site.GUID  

      FROM pmp_CalcRegionCells M
         LEFT OUTER JOIN PMP_Site ON M.PMP_Site_id = PMP_Site.id
                
      WHERE (m.pmp_Calc_Region_id=@ID) and (PMP_Site_id>0)
      

/*    
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children )
     --     ref_id, ref_ObjName, ref_guid
      SELECT id,name,guid,'PMP_Site',@parent_guid, 
              dbo.fn_Object_Has_children ('PMP_Site',id) as Has_children  
              
--              M.link_id, Link.GUID,  'link' 

      FROM PMP_Site        
      WHERE 
        id IN (SELECT PMP_Site_id 
                 FROM pmp_CalcRegionCells 
                 WHERE pmp_Calc_Region_id=@ID)
      
      
*/      
      
    --  ORDER BY name;     
  ----  SELECT id, name, guid FROM '+ view_LINKEND_ANTENNAS +
    --                 ' WHERE linkend_id
    
/*
    INSERT INTO #temp (id,name,guid,ObjName)
      SELECT id,name,guid,'LinkEnd' FROM LinkEnd 
             WHERE (id IN (SELECT linkend1_id FROM LINK  WHERE id = @id)) OR 
                   (id IN (SELECT LINKEND2_id FROM LINK  WHERE id = @id))    
        ORDER BY name; 
*/
  end ELSE    


  -----------------------------------------------
  if @ObjName = 'pmp_site'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM pmp_site where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children )
      SELECT M.id, M.name, M.guid, 'PMP_Sector', @parent_guid, 
              dbo.fn_Object_Has_children ('pmp_sector',M.id) as Has_children 
--              P.GeoRegion_ID

      FROM view_PMP_Sector M  
         LEFT OUTER JOIN PMP_Site S ON M.pmp_site_id = S.id  
         LEFT OUTER JOIN Property P ON S.property_id = P.id
      
      
      
            --          Property P ON M.property_id = P.id
      WHERE  M.PMP_Site_id=@ID
      
      /*
         INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid, 'LinkEnd_ANTENNA', @parent_guid, P.GeoRegion_ID 
        FROM LinkEnd_antenna A LEFT OUTER JOIN
                      Property P ON A.property_id = P.id*/
      
      
      
    --  ORDER BY name;     
  ----  SELECT id, name, guid FROM '+ view_LINKEND_ANTENNAS +
    --                 ' WHERE linkend_id
    
/*
    INSERT INTO #temp (id,name,guid,ObjName)
      SELECT id,name,guid,'LinkEnd' FROM LinkEnd 
             WHERE (id IN (SELECT linkend1_id FROM LINK  WHERE id = @id)) OR 
                   (id IN (SELECT LINKEND2_id FROM LINK  WHERE id = @id))    
        ORDER BY name; 
*/
  end ELSE   
     RAISERROR ('@TableName incorrect: %s', 16, 1, @ObjName );
  
  end_label:

  SELECT * FROM #temp

 -- drop TABLE #temp;
 -- drop TABLE #temp1;
  
END

go

-------------------------------------------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID(N'[dbo].[sp_Explorer_Select_SubItems]') IS NOT NULL
  DROP PROCEDURE [dbo].[sp_Explorer_Select_SubItems]


GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE [dbo].[sp_Explorer_Select_SubItems]
(
  @ObjName varchar(50),
  @ID  int  ,
  
  @PROJECT_ID  int = null
    
)

AS
BEGIN
  DECLARE
  	@link_id int,
    @GeoRegion_ID int,
    @parent_guid uniqueidentifier,
    @linkend1_id int,
    @linkend2_id int;



  create TABLE #temp  (id      			int, 
                       name    			nvarchar(100),
                       ObjName 			varchar(50),
                       GUID    			uniqueidentifier,
                       parent_GUID  uniqueidentifier,
                       Has_children BIT,
                       
                       ref_id      	int, 
                       ref_GUID    	uniqueidentifier, 
                       ref_ObjName 	varchar(50),

                       GeoRegion_ID int,
                       GeoRegion1_ID int,
                       GeoRegion2_ID int
                      );

 
 -----------------------------------------------
  if @ObjName = 'Link_SDB'
  -----------------------------------------------
  BEGIN
   INSERT INTO #temp (id,name,guid,ObjName) 
      SELECT id, 'common band: '+ name, guid, 'Link' --, @parent_guid, P.GeoRegion_ID 
      -- + name
        FROM  link 
        where id in (select link1_id from link_sdb where id=@ID)
 
   INSERT INTO #temp (id,name,guid,ObjName) 
      SELECT id, 'E band:            '+ name, guid, 'Link' --, @parent_guid, P.GeoRegion_ID 
      -- +name
        FROM  link 
        where id in (select link2_id from link_sdb where id=@ID)
 
  
--    SELECT @parent_guid = GUID FROM LinkEnd where id=@ID
   
           
/* 
   INSERT INTO #temp (id,name,guid,ObjName) --, parent_guid, GeoRegion_ID
      SELECT id, name, guid, 'Link_SDB' --, @parent_guid, P.GeoRegion_ID 
        FROM  link.link_sdb -- LinkEnd_antenna A LEFT OUTER JOIN
                --      Property P ON A.property_id = P.id
          
        WHERE project_id=@PROJECT_ID 
        ORDER BY name; 
*/
  end ELSE  


  -----------------------------------------------
  if @ObjName = 'LinkEnd'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM LinkEnd where id=@ID
   
           
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid, 'LinkEnd_ANTENNA', @parent_guid, P.GeoRegion_ID 
        FROM LinkEnd_antenna A LEFT OUTER JOIN
                      Property P ON A.property_id = P.id
          
        WHERE A.LinkEnd_id=@ID 
        ORDER BY A.name; 

  end ELSE  

  -----------------------------------------------
  if @ObjName = 'property'
  -----------------------------------------------
  BEGIN    
    SELECT @parent_guid = GUID, @GeoRegion_ID = GeoRegion_ID 
      FROM Property where id=@ID
 
		-------------------------
		-- LinkEnd
		------------------------- 
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'LinkEnd',@parent_guid,
              dbo.fn_Object_Has_children ('LinkEnd',id) as Has_children, 
              @GeoRegion_ID
      FROM view_LinkEnd_base  
        WHERE property_id=@ID 
        ORDER BY name;
  

    ---------------

/*    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'Site',@parent_guid, 
              dbo.fn_Object_Has_children ('Site',id) as Has_children,
              @GeoRegion_ID 
        FROM Site WHERE property_id=@ID ORDER BY name;
*/

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'MSC',@parent_guid, 
              dbo.fn_Object_Has_children ('Site',id) as Has_children,
              @GeoRegion_ID 
      FROM MSC WHERE property_id=@ID ORDER BY name;

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'BSC',@parent_guid, 
              dbo.fn_Object_Has_children ('BSC',id) as Has_children,
              @GeoRegion_ID 
      FROM BSC WHERE property_id=@ID ORDER BY name;

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'pmp_site',@parent_guid, 
              dbo.fn_Object_Has_children ('pmp_site',id) as Has_children,
              @GeoRegion_ID 
      FROM pmp_site WHERE property_id=@ID ORDER BY name;

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'pmp_terminal',@parent_guid, 
              dbo.fn_Object_Has_children ('pmp_terminal',id) as Has_children,
              @GeoRegion_ID 
      FROM view_pmp_terminal WHERE property_id=@ID ORDER BY name;

     ----- new -----

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT id,name,guid,'link_repeater',@parent_guid, 
              dbo.fn_Object_Has_children ('link_repeater',id) as Has_children,
              @GeoRegion_ID 
      FROM link_repeater WHERE property_id=@ID ORDER BY name;


/*
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children )
      SELECT id,name,guid,'mast',@parent_guid, 
              dbo.fn_Object_Has_children ('mast',id) as Has_children
               
      FROM mast WHERE property_id=@ID ORDER BY name;

*/



		-------------------------
		-- link
		-------------------------
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion1_ID, GeoRegion2_ID)
      SELECT id,name,guid,'link',@parent_guid,
             dbo.fn_Object_Has_children ('link',id) as Has_children,
             GeoRegion1_ID, GeoRegion2_ID 

       -- FROM view_link  
        FROM view_Link_for_explorer
        WHERE  (Property1_id=@ID) or (Property2_id=@ID)
        ORDER BY name
    


  end ELSE
   
  -----------------------------------------------
  if @ObjName = 'Link'
  -----------------------------------------------
  BEGIN
    DECLARE
 	  @link_repeater_id int;   
    
  --  SELECT @GUID = GUID FROM Link where id=@ID
    SELECT @parent_guid=GUID, @linkend1_id=linkend1_id, @linkend2_id=linkend2_id 
    --, @link_repeater_id=link_repeater_id
      FROM Link 
      WHERE id=@ID


    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT L.id, L.name, L.guid,'LinkEnd', @parent_guid, 
           dbo.fn_Object_Has_children ('LinkEnd',L.id) as Has_children,
           P.GeoRegion_ID 
      FROM LinkEnd L LEFT OUTER JOIN
                      Property P ON L.property_id = P.id
      WHERE L.id IN (@linkend1_id)        
      ORDER BY L.name; 
      

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, GeoRegion_ID)
      SELECT L.id, L.name, L.guid,'LinkEnd', @parent_guid, 
           dbo.fn_Object_Has_children ('LinkEnd',L.id) as Has_children,
           P.GeoRegion_ID 
      FROM LinkEnd L LEFT OUTER JOIN
                      Property P ON L.property_id = P.id
      WHERE L.id IN ( @linkend2_id)        
      ORDER BY L.name; 


      
/*      
                INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid, 'LinkEnd_ANTENNA', @parent_guid, P.GeoRegion_ID 
        FROM LinkEnd_antenna A LEFT OUTER JOIN
                      Property P ON A.property_id = P.id
          
*/
      
      

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Link_Repeater',@parent_guid,
           dbo.fn_Object_Has_children ('Link_Repeater',id) as Has_children 
      FROM Link_Repeater 
--      WHERE id = @link_repeater_id
      WHERE link_id = @id
      
  end ELSE

  -----------------------------------------------
  if @ObjName = 'LinkLine_Link'
  -----------------------------------------------
  BEGIN
  --  SELECT @GUID = GUID FROM Link where id=@ID
    SELECT @link_id = link_id FROM LinkLineLinkXREF where id=@ID;
    
    SELECT @parent_guid=GUID, @linkend1_id=linkend1_id, @linkend2_id=linkend2_id 
      FROM Link WHERE id=@ID


    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'LinkEnd',@parent_guid,
           dbo.fn_Object_Has_children ('LinkEnd',id) as Has_children 
      FROM LinkEnd 
      WHERE id IN (@linkend1_id,@linkend2_id)
      ORDER BY name; 



  end ELSE
 

  -----------------------------------------------
  if @ObjName = 'LinkLine'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM LinkLine where id=@ID


    SELECT X.*, Link.name, Link.GUID as Link_GUID
       INTO #temp1
    FROM Link RIGHT OUTER JOIN
          LinkLineLinkXREF X ON Link.id = X.link_id
    WHERE LinkLine_id=@ID      
    ORDER BY X.index_


 --   SELECT * from ##temp1


    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, 
    		ref_id, ref_ObjName, ref_guid,   Has_children)
      SELECT id,name,guid, 'LinkLine_Link', @parent_guid,
            LINK_id, 'LINK', Link_GUID, 1
      FROM #temp1  
     
  end ELSE   
  
  

  -----------------------------------------------
  if @ObjName = 'Link_Repeater'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM LInk_Repeater where id=@ID
   
           
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid)
      SELECT id,name,guid,'LInk_Repeater_ANTENNA',@parent_guid 
        FROM LInk_Repeater_antenna   
        WHERE  LInk_Repeater_id=@ID ORDER BY name; 

  end ELSE    
  
  -----------------------------------------------
  if @ObjName = 'pmp_sector'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM view_PMP_Sector where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid,'LinkEnd_ANTENNA', @parent_guid , P.GeoRegion_ID
        FROM LinkEnd_ANTENNA A LEFT OUTER JOIN
                      Property P ON A.property_id = P.id
        WHERE A.LinkEnd_id=@ID
      
     
      
      /*
      
                INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid, 'LinkEnd_ANTENNA', @parent_guid, P.GeoRegion_ID 
        FROM LinkEnd_antenna A LEFT OUTER JOIN
                      Property P ON A.property_id = P.id*/
      
      
  end ELSE    
  
  -----------------------------------------------
  if (@ObjName = 'pmp_terminal') --or (@ObjName = 'pmpTerminal')
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM view_Pmp_Terminal where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid,'LinkEnd_ANTENNA', @parent_guid, P.GeoRegion_ID 
      FROM LinkEnd_ANTENNA A  LEFT OUTER JOIN
                      Property P ON A.property_id = P.id
      WHERE A.LinkEnd_id=@ID
     
      
    --  ORDER BY name; 

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid)
      SELECT id,name,guid,'link',@parent_guid
        FROM link  
        WHERE  @ID = linkend2_id
--        WHERE  @ID in (linkend1_id, linkend2_id)
    
  end ELSE  
  

  -----------------------------------------------
  if @ObjName = 'LinkFreqPlan'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM LinkFreqPlan where id=@ID
    

    SELECT m.*, Link.name as name, Link.GUID as Link_GUID
       INTO #temp2       
      FROM LinkFreqPlan_Link M 
         LEFT OUTER JOIN Link ON M.link_id = dbo.Link.id        
      WHERE m.LinkFreqPlan_id=@ID 
      ORDER BY name;
    
 --   SELECT * from #temp2
    
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children, 
    		ref_id, ref_guid, ref_ObjName)
      SELECT M.id, Link.name, M.guid,'LinkFreqPlan_link',@parent_guid, 1,  
            M.link_id, Link.GUID,  'link'      
        FROM LinkFreqPlan_Link M 
           LEFT OUTER JOIN Link ON M.link_id = dbo.Link.id
        
        WHERE m.LinkFreqPlan_id=@ID 
        ORDER BY name;

  END ELSE

  -----------------------------------------------
  if @ObjName = 'LinkNet'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM LinkNet where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'LinkFreqPlan',@parent_guid, 
              dbo.fn_Object_Has_children ('LinkFreqPlan',id) as Has_children 
      FROM LinkFreqPlan 
      	WHERE LinkNet_id=@ID ORDER BY name;
      

    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, ref_id, ref_ObjName,   Has_children)
      SELECT id,name,guid,'LINKNET_LINKLINE_REF',@parent_guid, 
              LINKLINE_id,  'LINKLINE',
              dbo.fn_Object_Has_children ('LINKLINE',LINKLINE_id) as Has_children 
      FROM VIEW_SH_LINKNET_LINKLINE 
      	WHERE LinkNet_id=@ID ORDER BY name;


    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, ref_id, ref_ObjName, Has_children)
      SELECT id,name,guid,'LINKNET_LINK_REF',@parent_guid, 
              LINK_id,'LINK',      
              dbo.fn_Object_Has_children ('LINK',LINK_id) as Has_children 
      FROM VIEW_SH_LINKNET_LINK 
      	WHERE LinkNet_id=@ID ORDER BY name;
           
  end  ELSE



  
  -----------------------------------------------
  if @ObjName = 'template_site'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM template_site where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_site_LinkEnd',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_site_LinkEnd',id) as Has_children 

      FROM Template_site_LinkEnd  
      WHERE  Template_Site_id=@ID
      
     
 /*   ---------------
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children )
      SELECT id,name,guid,'Template_site_mast',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_site_mast',id) as Has_children
             
     
      FROM Template_site_mast
      WHERE  Template_Site_id=@ID
     
      */
      
  end ELSE   

/*
  -----------------------------------------------
  if @ObjName = 'template_site_mast'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM lib.template_site_mast where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_site_LinkEnd_antenna',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_site_LinkEnd_antenna',id) as Has_children 

      FROM lib.Template_site_LinkEnd_antenna  WHERE  Template_site_mast_id=@ID
      
  end ELSE   
*/

  
  -----------------------------------------------
  if @ObjName = 'template_pmp_site'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM template_pmp_site where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_LinkEnd',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_LinkEnd',id) as Has_children 

      FROM Template_LinkEnd  
      WHERE  Template_pmp_Site_id=@ID
      
  end ELSE   


  -----------------------------------------------
  if @ObjName = 'template_site_linkend'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM template_site_linkend where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_site_LinkEnd_antenna',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_site_LinkEnd_antenna',id) as Has_children 

      FROM Template_site_LinkEnd_antenna  WHERE  Template_site_LinkEnd_id=@ID
      
  end ELSE   

  -----------------------------------------------
  if @ObjName = 'template_linkend'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM template_linkend where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_LinkEnd_antenna',@parent_guid, 
              dbo.fn_Object_Has_children ('Template_LinkEnd_antenna',id) as Has_children 

      FROM Template_LinkEnd_antenna  WHERE  Template_LinkEnd_id=@ID
      
  end ELSE   


/*
  -----------------------------------------------
  if @ObjName = 'template_site_linkend'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM template_site_linkend where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Template_LinkEnd_antenna',@parent_guid, 
              dbo.fn_Object_Has_children ('template_site_linkend',id) as Has_children 

      FROM template_site_linkend  WHERE  Template_LinkEnd_id=@ID
      
  end ELSE   */


  
  
   -----------------------------------------------
  if @ObjName = 'site' BEGIN
  -----------------------------------------------
    SELECT @parent_guid = GUID FROM [site] where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
      SELECT id,name,guid,'Cell',@parent_guid, 
              dbo.fn_Object_Has_children ('cell',id) as Has_children 

      FROM cell 
      WHERE Site_id=@ID
      
  END ELSE      

   -----------------------------------------------
  if @ObjName = 'cell' BEGIN
  -----------------------------------------------
    SELECT @parent_guid = GUID FROM [cell] where id=@ID
    
    if OBJECT_ID('view_Cell_Antennas')>0
    BEGIN
      INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children)
        SELECT id,name,guid,'Cell_antenna',@parent_guid, 
                0 as Has_children 

        FROM view_Cell_Antennas 
        WHERE cell_id=@ID
    END   
      
  END ELSE      


  -----------------------------------------------
  if (@ObjName = 'calc_region_pmp') or (@ObjName = 'pmp_calc_region')
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM pmp_CalcRegion where id=@ID


    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children,
          ref_id, ref_ObjName, ref_guid )
      SELECT m.id,PMP_Site.name,m.guid,'PMP_calc_region_Site',@parent_guid, 
--      SELECT m.id,PMP_Site.name,m.guid,'PMP_Site',@parent_guid, 
              dbo.fn_Object_Has_children ('PMP_Site',PMP_Site.id) as Has_children , 
              
           PMP_Site.id,'PMP_Site',  PMP_Site.GUID  

      FROM pmp_CalcRegionCells M
         LEFT OUTER JOIN PMP_Site ON M.PMP_Site_id = PMP_Site.id
                
      WHERE (m.pmp_Calc_Region_id=@ID) and (PMP_Site_id>0)
      

/*    
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children )
     --     ref_id, ref_ObjName, ref_guid
      SELECT id,name,guid,'PMP_Site',@parent_guid, 
              dbo.fn_Object_Has_children ('PMP_Site',id) as Has_children  
              
--              M.link_id, Link.GUID,  'link' 

      FROM PMP_Site        
      WHERE 
        id IN (SELECT PMP_Site_id 
                 FROM pmp_CalcRegionCells 
                 WHERE pmp_Calc_Region_id=@ID)
      
      
*/      
      
    --  ORDER BY name;     
  ----  SELECT id, name, guid FROM '+ view_LINKEND_ANTENNAS +
    --                 ' WHERE linkend_id
    
/*
    INSERT INTO #temp (id,name,guid,ObjName)
      SELECT id,name,guid,'LinkEnd' FROM LinkEnd 
             WHERE (id IN (SELECT linkend1_id FROM LINK  WHERE id = @id)) OR 
                   (id IN (SELECT LINKEND2_id FROM LINK  WHERE id = @id))    
        ORDER BY name; 
*/
  end ELSE    


  -----------------------------------------------
  if @ObjName = 'pmp_site'
  -----------------------------------------------
  BEGIN
    SELECT @parent_guid = GUID FROM pmp_site where id=@ID
    
    INSERT INTO #temp (id,name,guid,ObjName, parent_guid, Has_children )
      SELECT M.id, M.name, M.guid, 'PMP_Sector', @parent_guid, 
              dbo.fn_Object_Has_children ('pmp_sector',M.id) as Has_children 
--              P.GeoRegion_ID

      FROM view_PMP_Sector M  
         LEFT OUTER JOIN PMP_Site S ON M.pmp_site_id = S.id  
         LEFT OUTER JOIN Property P ON S.property_id = P.id
      
      
      
            --          Property P ON M.property_id = P.id
      WHERE  M.PMP_Site_id=@ID
      
      /*
         INSERT INTO #temp (id,name,guid,ObjName, parent_guid, GeoRegion_ID)
      SELECT A.id, A.name, A.guid, 'LinkEnd_ANTENNA', @parent_guid, P.GeoRegion_ID 
        FROM LinkEnd_antenna A LEFT OUTER JOIN
                      Property P ON A.property_id = P.id*/
      
      
      
    --  ORDER BY name;     
  ----  SELECT id, name, guid FROM '+ view_LINKEND_ANTENNAS +
    --                 ' WHERE linkend_id
    
/*
    INSERT INTO #temp (id,name,guid,ObjName)
      SELECT id,name,guid,'LinkEnd' FROM LinkEnd 
             WHERE (id IN (SELECT linkend1_id FROM LINK  WHERE id = @id)) OR 
                   (id IN (SELECT LINKEND2_id FROM LINK  WHERE id = @id))    
        ORDER BY name; 
*/
  end ELSE   
     RAISERROR ('@TableName incorrect: %s', 16, 1, @ObjName );
  
  end_label:

  SELECT * FROM #temp

 -- drop TABLE #temp;
 -- drop TABLE #temp1;
  
END
go


