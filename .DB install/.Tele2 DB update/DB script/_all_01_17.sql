


/****** Object:  UserDefinedFunction [dbo].[fn_Linkend_Antenna_pos]    Script Date: 16.05.2020 10:54:58 ******/
IF OBJECT_ID(N'[dbo].[fn_Linkend_Antenna_pos]') IS  not  NULL

DROP FUNCTION [dbo].[fn_Linkend_Antenna_pos]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Linkend_Antenna_pos]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE FUNCTION [dbo].[fn_Linkend_Antenna_pos] 
(
  @LINKEND_ID int   
 
)

RETURNS @ret TABLE 
(
    lat 	float,
    lon 	float,
    
    location_type integer
)

as

BEGIN

if exists 
(
  select *
  	from Linkend_Antenna 
       left join Property  on Property.id = Linkend_Antenna.Property_id
    where Linkend_Antenna.Linkend_id = @LINKEND_ID

)


  insert into @ret (lat,lon, location_type)
    select top 1 
      IIF (IsNull(location_type,0) = 0, Property.lat,  Linkend_Antenna.lat) as lat,
      IIF (IsNull(location_type,0) = 0, Property.lon,  Linkend_Antenna.lon) as lon,
      location_type 

      from Linkend_Antenna 
         left join Property  on Property.id = Linkend_Antenna.Property_id
      where Linkend_Antenna.Linkend_id = @LINKEND_ID

else

  insert into @ret (lat,lon)

    select 
        P.lat,
        P.lon      

      from  Linkend L  
        left join Property P  on P.id = L.Property_id
      where L.id = @LINKEND_ID
 
  

	RETURN
  
END

GO




IF OBJECT_ID(N'[dbo].[fn_Linkend_Link_info]') IS NOT NULL
  DROP FUNCTION [dbo].[fn_Linkend_Link_info]
GO


CREATE FUNCTION [dbo].fn_Linkend_Link_info
(	
  @Linkend_id int
  
)
RETURNS TABLE 
--(id int)
AS
RETURN 
(
        
  SELECT 
    L.id as Link_id, 
    L.name as Link_name,
    clutter_model_id,
    cluttermodel.name as clutter_model_name,
      
    length,
    length_km,    
      
    case
      when (LinkEnd1_id = @Linkend_id) then LinkEnd2_id
      when (LinkEnd2_id = @Linkend_id) then LinkEnd1_id
      else  null
    end as next_LinkEnd_id,
    
    @Linkend_id as LinkEnd_id,
    
    calc_method
    
  
    FROM Link L
      left outer join cluttermodel  on cluttermodel.id=L.clutter_model_id
      
    WHERE 
        ((LinkEnd1_id = @Linkend_id) or (LinkEnd2_id=@Linkend_id))
        -- and (1=0)


)


go



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- delete --

/****** Object:  StoredProcedure [link_climate].[sp_Link_climate_UPD]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[link_climate].[sp_Link_climate_UPD]') IS NOT NULL
  DROP PROCEDURE [link_climate].[sp_Link_climate_UPD]
GO


CREATE PROCEDURE [link_climate].[sp_Link_climate_UPD]
(
  @ID   int,  
  
  
------------------------
-- ITU
------------------------
  

@RRV_NAME 				 varchar(100)=NULL,

@terrain_type 			 int = null,          
@underlying_terrain_type int = null,

@Q_factor 			float = null,
@gradient_diel 		float = null,  
@gradient_deviation float = null,
@climate_factor 	float = null,

@factor_B 			float = null,
@factor_C 			float = null,
@factor_D 			float = null,

@rain_intensity 	float = null,
@steam_wet 			float = null,
@air_temperature    float = null,




/*
  // ITU
        db_Par(FLD_RRV_NAME      , aRec.comment ),

        //Q_factor_rec
        db_Par(FLD_terrain_type,            aRec.ITU.terrain_type ),
        db_Par(FLD_underlying_terrain_type, aRec.ITU.underlying_terrain_type),

        db_Par(FLD_Q_factor    ,  aRec.ITU.Q_factor ),

        db_Par(FLD_gradient_diel ,      aRec.ITU.gradient_diel),
        db_Par(FLD_gradient_deviation , aRec.ITU.gradient_deviation ),

        db_Par(FLD_climate_factor,      aRec.ITU.climate_factor),

        db_Par(FLD_factor_B,   aRec.ITU.factor_B),
        db_Par(FLD_factor_C,   aRec.ITU.factor_C),
        db_Par(FLD_factor_D,   aRec.ITU.factor_D),

        db_Par(FLD_rain_intensity,   aRec.ITU.rain_intensity1 ),
        db_Par(FLD_steam_wet,        aRec.ITU.steam_wet  ),

 */ 
------------------------
-- GOST
------------------------

@GOST_NAME 					  varchar(100)=NULL,

@GOST_gradient_diel 		  float = null,
@GOST_gradient_deviation 	  float = null,

@GOST_TERRAIN_TYPE 			  int = null,
@GOST_underlying_terrain_type int = null,
@GOST_steam_wet 		  	  float = null,

@GOST_air_temperature 		  float = null,
@GOST_atmosphere_pressure 	  float = null,

@GOST_Climate_factor 		  float = null,
@GOST_rain_intensity 		  float = null,



/*

   // ---------------------------------------------------------------
        //GOST
        // ---------------------------------------------------------------

         db_Par (FLD_GOST_NAME,      aRec.Comment),

(*            if rec.air_temperature=0 then rec.air_temperature:=15;
          if rec.atmosphere_pressure=0 then rec.atmosphere_pressure:=1013;
          if rec.GOST.rain_intensity=0 then rec.GOST.rain_intensity:=1;
*)
          db_Par(FLD_GOST_gradient_diel ,      aRec.ITU.gradient_diel),
          db_Par(FLD_GOST_gradient_deviation , aRec.ITU.gradient_deviation ),

          db_Par (FLD_GOST_TERRAIN_TYPE,             aRec.ITU.terrain_type), //3
          db_Par (FLD_GOST_underlying_terrain_type,  aRec.ITU.underlying_terrain_type),//4
          db_Par (FLD_GOST_steam_wet,                aRec.ITU.steam_wet), //5

        //  if rec.air_temperature>0 then
          db_Par (FLD_GOST_air_temperature, aRec.air_temperature), //6

         // if rec.atmosphere_pressure>0 then
          db_Par (FLD_GOST_atmosphere_pressure, aRec.atmosphere_pressure), //7

          db_Par (FLD_GOST_Climate_factor,  aRec.ITU.Climate_factor), //8

//           if rec.GOST.rain_intensity>0 then
          db_Par (FLD_GOST_rain_intensity,  aRec.GOST.rain_intensity), //9
*/

------------------------
-- NIIR1998
------------------------

  @NIIR1998_NAME 				varchar(100)=NULL,

  @NIIR1998_gradient_diel 		float = null,
  @NIIR1998_gradient_deviation  float = null,


  @NIIR1998_TERRAIN_TYPE 		int = null,       
  @NIIR1998_steam_wet 			float = null,          

  @NIIR1998_air_temperature 	float = null,
  @NIIR1998_atmosphere_pressure float = null,

  @NIIR1998_rain_region_number 	int = null,
  @NIIR1998_Qd_region_number 	int = null
    
)

AS
BEGIN

if not exists (select * from link where id = @ID)
  RAISERROR('RAISERROR: id not exists ',16,1)

 


--Alter table  property Disable trigger all

  


 
  UPDATE Link 
  SET 
    
------------------------
-- ITU
------------------------      
     
    RRV_NAME 			 	  = COALESCE(@RRV_NAME, RRV_NAME),

    terrain_type 		 	  = COALESCE(@terrain_type, terrain_type),	 
    underlying_terrain_type  = COALESCE(@underlying_terrain_type, underlying_terrain_type),

    Q_factor 		 	 	= COALESCE(@Q_factor, Q_factor),	
    gradient_diel 	 	 	= COALESCE(@gradient_diel, gradient_diel),	  
    gradient_deviation  	= COALESCE(@gradient_deviation, gradient_deviation),
    climate_factor  	 	= COALESCE(@climate_factor	, climate_factor),
    
    air_temperature        = COALESCE(@air_temperature	, air_temperature),

    factor_B 				= COALESCE(@factor_B, factor_B),	
    factor_C 	 			= COALESCE(@factor_C, factor_C),		
    factor_D 	 			= COALESCE(@factor_D, factor_D),		

    rain_intensity 		= COALESCE(@rain_intensity, rain_intensity),
    steam_wet 		 		= COALESCE(@steam_wet, steam_wet), 		   
      
------------------------
-- GOST
------------------------

    GOST_NAME 					 = COALESCE(@GOST_NAME,	GOST_NAME),

    GOST_gradient_diel 		 = COALESCE(@GOST_gradient_diel, GOST_gradient_diel	),	
    GOST_gradient_deviation 	 = COALESCE(@GOST_gradient_deviation, GOST_gradient_deviation),

    GOST_TERRAIN_TYPE 			 = COALESCE(@GOST_TERRAIN_TYPE,	GOST_TERRAIN_TYPE),
    GOST_underlying_terrain_type= COALESCE(@GOST_underlying_terrain_type, GOST_underlying_terrain_type),
    GOST_steam_wet 		  	 = COALESCE(@GOST_steam_wet, GOST_steam_wet),

    GOST_air_temperature 		 = COALESCE(@GOST_air_temperature, GOST_air_temperature),
    GOST_atmosphere_pressure 	 = COALESCE(@GOST_atmosphere_pressure, GOST_atmosphere_pressure),

    GOST_Climate_factor 		 = COALESCE(@GOST_Climate_factor, GOST_Climate_factor	),
    GOST_rain_intensity 		 = COALESCE(@GOST_rain_intensity, GOST_rain_intensity),
      
---------------------------------------------------------------
--GOST
---------------------------------------------------------------
    
   NIIR1998_NAME 				= COALESCE(@NIIR1998_NAME, 				  NIIR1998_NAME	),				

   NIIR1998_gradient_diel 		= COALESCE(@NIIR1998_gradient_diel, 	  NIIR1998_gradient_diel),
   NIIR1998_gradient_deviation = COALESCE(@NIIR1998_gradient_deviation,  NIIR1998_gradient_deviation),


   NIIR1998_TERRAIN_TYPE 		= COALESCE(@NIIR1998_TERRAIN_TYPE,		  NIIR1998_TERRAIN_TYPE),
   NIIR1998_steam_wet 			= COALESCE(@NIIR1998_steam_wet,			  NIIR1998_steam_wet),

   NIIR1998_air_temperature 	= COALESCE(@NIIR1998_air_temperature, 	  NIIR1998_air_temperature),
   NIIR1998_atmosphere_pressure= COALESCE(@NIIR1998_atmosphere_pressure, NIIR1998_atmosphere_pressure),
     
   NIIR1998_rain_region_number = COALESCE(@NIIR1998_rain_region_number,  NIIR1998_rain_region_number),
   NIIR1998_Qd_region_number 	= COALESCE(@NIIR1998_Qd_region_number,	  NIIR1998_Qd_region_number)
   
            
  WHERE 
    id = @ID

/*

select id, air_temperature,
    COALESCE(@air_temperature	, air_temperature)
 from link where id=@id 
*/  
--  Alter table  property ENABLE trigger all



--return



  RETURN @@ROWCOUNT


END

GO
/****** Object:  View [LinkFreqPlan].[view_LinkFreqPlan_LinkEnd]    Script Date: 16.05.2020 10:54:58 ******/


IF OBJECT_ID(N'[geo].[fn_Length_m]') IS NOT NULL

DROP FUNCTION [geo].[fn_Length_m]
GO
/****** Object:  UserDefinedFunction [geo].[fn_Length_m]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [geo].[fn_Length_m] 

(
  @LAT1 float=64,
  @Lon1 float=37,

  @LAT2 float=64.1,
  @LON2 float=37.1 
 


)
RETURNS float
AS
BEGIN
  DECLARE 
--    @s varchar(max) = 'LINESTRING('+ cast(@lon1 as varchar(10)) + cast(@lon1 as varchar(10)) +','+  cast(@lon2 as varchar(10)) + cast(@lon2 as varchar(10)) +' )',
    @g geography;  


    
--  SET @g = geography::STGeomFromText('LINESTRING('+ cast(@lat1 as varchar(10)) +' '+ cast(@lon1 as varchar(10)) +','+  cast(@lon2 as varchar(10)) +' '+ cast(@lon2 as varchar(10)) +' )', 4326);  
  SET @g = geography::STGeomFromText('LINESTRING('+ cast(@lon1 as varchar(10)) +' '+ cast(@lat1 as varchar(10)) +','+  cast(@lon2 as varchar(10)) +' '+ cast(@lat2 as varchar(10)) +' )', 4326);  
  return round(@g.STLength(), 0);  

END


/*

DECLARE @g geography;  
SET @g = geography::STGeomFromText('LINESTRING(-122.360 47.656, -122.343 47.656)', 4326);  
SELECT @g.STLength();  

*/

GO
/****** Object:  UserDefinedFunction [geo].[fn_Length_m_Between_Property]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[geo].[fn_Length_m_Between_Property]') IS NOT NULL


DROP FUNCTION [geo].[fn_Length_m_Between_Property]
GO
/****** Object:  UserDefinedFunction [geo].[fn_Length_m_Between_Property]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [geo].[fn_Length_m_Between_Property]
(

  @ID1   int,
  @ID2   int

)
RETURNS float
AS
BEGIN

 DECLARE  
   
    @lat1 float,
    @lon1 float,
    
    @lat2 float,
    @lon2 float


  SELECT       
       @LAT1 = lat,
       @LON1 = lon              
  FROM Property
     WHERE (id = @ID1)  


  SELECT       
       @LAT2 = lat,
       @LON2 = lon              
  FROM Property
     WHERE (id = @ID2)   




  return  geo.fn_Length_m(@LAT1, @Lon1, @LAT2, @Lon2)
 

  /* Function body */
END

GO

IF OBJECT_ID(N'[lib].[link_status]') IS  NULL

begin

CREATE TABLE [lib].[link_status](
	[id] [int] NOT NULL,
	[name] [varchar](50) NULL,
 CONSTRAINT [link_status_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END

go
IF OBJECT_ID(N'[lib].[Template_Link]') IS  NULL

begin

CREATE TABLE [lib].[Template_Link](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[folder_id] [int] NULL,
	[linkendtype_id] [int] NULL,
	[mode] [int] NULL,
	[linkendtype_mode_id] [int] NULL,
	[AntennaType_id] [int] NULL,
	[height] [int] NULL,
	[guid] [uniqueidentifier] NULL DEFAULT (newid()),
	[date_created] [datetime] NULL DEFAULT (getdate()),
	[date_modify] [datetime] NULL,
	[user_modify] [varchar](50) NULL,
	[user_created] [varchar](50) NULL DEFAULT (suser_name()),
 CONSTRAINT [PK_Template_Link] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [Template_Link_uq] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
 
ALTER TABLE [lib].[Template_Link]  WITH CHECK ADD  CONSTRAINT [Template_Link_fk] FOREIGN KEY([linkendtype_id])
REFERENCES [dbo].[LinkEndType] ([id])

ALTER TABLE [lib].[Template_Link] CHECK CONSTRAINT [Template_Link_fk]

ALTER TABLE [lib].[Template_Link]  WITH CHECK ADD  CONSTRAINT [Template_Link_fk2] FOREIGN KEY([AntennaType_id])
REFERENCES [dbo].[AntennaType] ([id])

ALTER TABLE [lib].[Template_Link] CHECK CONSTRAINT [Template_Link_fk2]

ALTER TABLE [lib].[Template_Link]  WITH CHECK ADD  CONSTRAINT [Template_Link_fk3] FOREIGN KEY([linkendtype_mode_id])
REFERENCES [dbo].[LinkEndType_Mode] ([id])

ALTER TABLE [lib].[Template_Link] CHECK CONSTRAINT [Template_Link_fk3]

END

go


IF OBJECT_ID(N'[lib].[PickList_Values]') IS  NULL

begin

CREATE TABLE [lib].[PickList_Values](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[PickList_id] [int] NOT NULL,
	[code] [int] NULL,
	[value] [varchar](100) NOT NULL,
	[Display_Index] [int] NULL,
	[comment] [text] NULL,
 CONSTRAINT [PK_PickList_Values_PickList_Values] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_PickList_Values_name_PickList_Values] UNIQUE NONCLUSTERED 
(
	[value] ASC,
	[PickList_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


end

GO
/****** Object:  Table [lib].[PickList]    Script Date: 16.05.2020 10:54:58 ******/


IF OBJECT_ID(N'[lib].[PickList]') IS  NULL

begin

CREATE TABLE [lib].[PickList](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[table_name] [varchar](50) NOT NULL,
	[field_name] [varchar](50) NOT NULL,
	[is_system] [bit] NULL,
 CONSTRAINT [PickList_pk_PickList] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

end
 
GO

/****** Object:  StoredProcedure [lib].[sp_Template_Link_apply_to_link]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[_UpdateAllViews]') IS NOT NULL
DROP procedure [dbo].[_UpdateAllViews]
GO

------------------------------------------------------------------
CREATE PROCEDURE dbo._UpdateAllViews
------------------------------------------------------------------

AS
BEGIN

  
  DECLARE 
    @name varchar(200),
    @sql nvarchar(100);
  
  DECLARE 
    @temp table (name sysname);
     
    /*
  SELECT     
    TABLE_SCHEMA,
    TABLE_name,
  --  View_Definition,
    *
    
  FROM INFORMATION_SCHEMA.views    */



--TABLE_name
--TABLE_SCHEMA    
--View_Definition
 -- return



INSERT INTO @temp
	select TABLE_SCHEMA+'.'+ TABLE_name 	  
     FROM INFORMATION_SCHEMA.views    
       where TABLE_name not like '%11'
     
--   and o.NAME not LIKE '%11'
     
     
     
--	from sysobjects WHERE (type = 'V') 
/*
	select *
     FROM INFORMATION_SCHEMA.views    
     where table_schema='security'
     

*/




/*
INSERT INTO @temp
	select name  	  
	from sysobjects WHERE (type = 'V') 

*/

  WHILE exists(SELECT * FROM @temp)
  BEGIN
    SELECT top 1 @name=name FROM @temp
    delete from @temp where name=@name
  
    print @name


    exec sp_refreshview @name

-- exec ('select top 1 * from '+ @name)

--return

  END
 

END

go





IF OBJECT_ID(N'[lib].[sp_Template_Link_apply_to_link]') IS NOT NULL
DROP PROCEDURE [lib].[sp_Template_Link_apply_to_link]
GO
/****** Object:  StoredProcedure [lib].[sp_Template_Link_apply_to_link]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE [lib].[sp_Template_Link_apply_to_link]
(
  @property_id int,
  @Template_Link_id int
)
AS
BEGIN
  declare
    @AntennaType_id INT,
    @LinkEndType_id INT,
    @LinkEndType_mode_id INT


 IF @property_id is null 
 begin
 
/*
    update lib.Template_Link
    set 
      AntennaType_id = ( select top 1 id from AntennaType),
      linkendtype_id = ( select top 1 id from linkendtype)
*/

   set @property_id      = (select top 1 id from [property] )
   set @Template_Link_id = (select top 1 id from lib.Template_Link )
 
 end



select 
       @LinkEndType_id = LinkEndType_id,
       @LinkEndType_mode_id = LinkEndType_mode_id,
       @AntennaType_id = AntennaType_id

  from lib.Template_Link
  where id = @Template_Link_id
   

/*select *
  from Template_Site
*/


 IF @property_id is null 
  --     EXISTS(SELECT * FROM Link WHERE (LINKEND1_ID=@LINKEND2_ID) and (LINKEND2_ID=@LINKEND1_ID))
      RAISERROR ('@property_id', 16, 1);



  declare 
  --  @Template_Antenna_id int ,
--    @Template_LinkEnd_id int ,

   
    @Antenna_id int,
    @LinkEnd_id int 
   
/*--------------------------------------------------
select @Template_LinkEnd_id = id
  from Template_Site_LinkEnd
  where Template_Site_id = @Template_Site_id
*/
/*select *
  from Template_Site_LinkEnd
  where Template_Site_id = @Template_Site_id

*/

/*
IF @Template_LinkEnd_id is null    RAISERROR ('@Template_LinkEnd_id', 16, 1);
---------------------------------------------------
  
select @Template_Antenna_id = id
  from Template_Site_LinkEnd_Antenna
  where Template_Site_LinkEnd_id = @Template_LinkEnd_id
*/

/*select *
  from Template_Site_LinkEnd_Antenna
  where Template_Site_LinkEnd_id = @Template_LinkEnd_id
  
*/
  
--IF @Template_Antenna_id is null    RAISERROR ('@Template_Antenna_id', 16, 1);
  

--------------------------------------------------


  INSERT INTO linkend  (property_id, NAME) VALUES (@property_id,'���');     
  set @LinkEnd_id = IDENT_CURRENT ('linkend')   

  INSERT INTO Linkend_Antenna  (Linkend_id, property_id, NAME) VALUES (@Linkend_id, @property_id, 'Antenna');
  set @Antenna_id = IDENT_CURRENT ('Linkend_Antenna')   


--------------------------------------------------

  if @LinkEndType_mode_id is null 
    select top 1 @LinkEndType_mode_id = id 
      from LinkEndType_mode 
      WHERE LinkendType_id=@LinkendType_id
      order by bitrate_Mbps desc   



  UPDATE Linkend  
  SET 
      LinkendType_id      = t.LinkendType_ID,
      LinkEndType_mode_id = m.id,
--      bitrate_Mbps        = m.bitrate_Mbps
        mode                = m.mode,
--      bitrate_Mbps        = m.bitrate_Mbps
        
        bitrate_Mbps    = m.bitrate_Mbps,
                 
        MODULATION_TYPE =m.MODULATION_TYPE,
        SIGNATURE_HEIGHT=m.SIGNATURE_HEIGHT, 
        SIGNATURE_WIDTH =m.SIGNATURE_WIDTH,  
        THRESHOLD_BER_3 =m.THRESHOLD_BER_3,  
        THRESHOLD_BER_6 =m.THRESHOLD_BER_6,  
            
        channel_width_MHz = m.bandwidth, -- //_MHz,  
        
        POWER_dBm       = m.POWER_MAX,
        POWER_max       = m.POWER_MAX,
        
        tx_freq_mhz =  tp.freq_min,
        rx_freq_mhz =  tp.freq_min
        
    --   MODULATION_COUNT      = m.MODULATION_level_COUNT,
     --  GOST_53363_modulation = m.GOST_53363_modulation        
      
      
  --    LinkendType_mode_id = m.LinkendType_mode_ID 
    
   from 
      (select * from LinkEndType  WHERE id=@LinkEndType_id) tp,
      (select * from LinkEndType_mode  WHERE id=@LinkEndType_mode_id) m,
      (select * from lib.Template_Link WHERE id=@Template_Link_id) t     
   WHERE 
      Linkend.id=@LinkEnd_id    


 --RETURN @@IDENTITY;    
 

--------------------------------------------------

  UPDATE Linkend_Antenna  
  SET 
      antennaType_id  =m.AntennaType_ID,  

   --   band            =m.band,

      Gain            = a.Gain,
      Height          = m.Height,

      band            =a.band,
  
      Freq_MHz        =a.Freq,
--          Freq_MHz        =m.Freq_MHz,
      Diameter        =a.Diameter,
      Polarization_str=A.Polarization_str,
      Vert_Width      =A.Vert_Width,
      Horz_Width      =A.Horz_Width 
      
      
      
      
   --   Freq_MHz        =m.Freq,
     --     Freq_MHz        =m.Freq_MHz,
/*      Diameter        =m.Diameter,
      Polarization_str=m.Polarization_str,
     
      Vert_Width      =m.Vert_Width,
      Horz_Width      =m.Horz_Width 
*/      
   from 
      (select * from AntennaType       WHERE id=@AntennaType_id) a,     
      (select * from lib.Template_Link WHERE id=@Template_Link_id) m     
   WHERE 
      Linkend_Antenna.id=@Antenna_id       

--  INSERT INTO Template_Site_LinkEnd_Antenna  (template_site_linkend_id, NAME) VALUES (@LinkEnd_id,'�������');

--template_site_LINKEND

  RETURN @LinkEnd_id
 
  
END


GO
/****** Object:  UserDefinedFunction [lib].[ft_PickList]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[lib].[ft_PickList]') IS NOT NULL

DROP FUNCTION [lib].[ft_PickList]
GO
/****** Object:  UserDefinedFunction [lib].[ft_PickList]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [lib].[ft_PickList] 
(
  @TABLE_NAME VARCHAR(50) = 'link',
  @FIELD_NAME VARCHAR(100) = 'status'
 
)

--346447
--RETURNS varchar(50)

RETURNS @ret TABLE 
(
    id INT primary key,
    value varchar(100)
    
)

as

BEGIN

  insert into @ret
   SELECT code as id , value
    
  --    FROM view_PickList_Values
      FROM  --view_PickList_Values
      (
      SELECT dbo.PickList.table_name, dbo.PickList.field_name, dbo.PickList_Values.*
        FROM dbo.PickList_Values LEFT OUTER JOIN
              dbo.PickList ON dbo.PickList_Values.PickList_id = dbo.PickList.id
      ) M        
      
      WHERE (TABLE_NAME = @TABLE_NAME) and
            (FIELD_NAME = @FIELD_NAME) and
            
          code is not null      



	RETURN
  
END

GO
/****** Object:  View [link_calc].[view_Link_for_calc]    Script Date: 16.05.2020 10:54:58 ******/


IF OBJECT_ID(N'[link_calc].[view_Link_for_calc]') IS not NULL


DROP VIEW [link_calc].[view_Link_for_calc]
GO
/****** Object:  View [link_calc].[view_Link_for_calc]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [link_calc].[view_Link_for_calc]
AS
SELECT 
    dbo.Link.*, 

 

a1.lat AS lat1, 
a1.lon AS lon1, 
a2.lat AS lat2, 
a2.lon AS lon2, 



Property1.ground_height AS Property1_ground_height, 
Property2.ground_height AS Property2_ground_height,


Property1.name AS Property1_name, 
Property2.name AS Property2_name, 
  

--Property1.id AS Property1_id, 
--Property2.id AS Property2_id, 
  
  Link_Repeater.id as Link_Repeater_id
   
    
  
        
FROM         dbo.LinkEnd LinkEnd2 

              RIGHT OUTER JOIN dbo.LinkEnd LinkEnd1 
              
 --     RIGHT OUTER JOIN dbo.LinkEndType ON LinkEndType.id = LinkEnd1.LinkEndType_id  
      
      RIGHT OUTER JOIN dbo.Link ON LinkEnd1.id = dbo.Link.linkend1_id ON LinkEnd2.id = dbo.Link.linkend2_id 
     
     
      RIGHT OUTER JOIN dbo.Property Property2  ON LinkEnd2.Property_id = Property2.id 
    
      RIGHT OUTER JOIN dbo.Property Property1  ON LinkEnd1.Property_id = Property1.id 

--      LEFT OUTER JOIN dbo.LinkType ON dbo.Link.linkType_id = dbo.LinkType.id LEFT OUTER JOIN dbo.ClutterModel ON dbo.Link.clutter_model_id = dbo.ClutterModel.id
                 
       left OUTER JOIN dbo.Link_Repeater ON dbo.Link.id =  dbo.Link_Repeater.Link_ID
              
                 
              
              
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND1_ID) A1
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND2_ID) A2
              
     


/*
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd1.kratnostBySpace) AS LinkEnd1_kratnostBySpace, 
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd2.kratnostBySpace) AS LinkEnd2_kratnostBySpace, 
*/
  
--where Link.id=5591
             
               
--WHERE     dbo.Link.id =  240806

GO
/****** Object:  StoredProcedure [link_calc].[sp_Link_select_for_calc]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[link_calc].[sp_Link_select_for_calc]') IS  not NULL

DROP PROCEDURE [link_calc].[sp_Link_select_for_calc]
GO
/****** Object:  StoredProcedure [link_calc].[sp_Link_select_for_calc]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:  
 -  ���������� ��� ��������� � ������� ������ VIEW
--------------------------------------------------------------- */

CREATE PROCEDURE [link_calc].[sp_Link_select_for_calc]
(
	@ID int = 250295
)

AS
BEGIN
 -- declare 
  --  @objname varchar(10);
    
  ---------------------------------------------
  
--  SELECT @objname = objname 
 --   FROM  Link
  --  WHERE  (id = @ID)
    
    
--  if ( @objname = 'link'  ) or ( @objname is null )
   
 SELECT * FROM view_link_for_calc WHERE id=@ID
-- SELECT * FROM view_link WHERE id=@ID
    
    
--  else 
--    SELECT * FROM view_PMP_LINK WHERE id=@ID
  
  return @@rowcount
         
END

GO
/****** Object:  StoredProcedure [link_calc].[sp_Link_calc_results_SEL]    Script Date: 16.05.2020 10:54:58 ******/
IF OBJECT_ID(N'[link_calc].[sp_Link_calc_results_SEL]') IS  not  NULL


DROP PROCEDURE [link_calc].[sp_Link_calc_results_SEL]
GO
/****** Object:  StoredProcedure [link_calc].[sp_Link_calc_results_SEL]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [link_calc].[sp_Link_calc_results_SEL]
(
  @ID int =null

)

AS
BEGIN

if @id is null
--  set @id = (select top 1 id from   dbo.Link where calc_results_xml is not null)

  set @id = (select top 1 link_id from Link_Repeater where calc_results_xml_1 is not null )




declare 
  @calc_results_xml   varchar(max),
  @calc_results_xml_1 varchar(max),
  @calc_results_xml_2 varchar(max),
  @calc_results_xml_3 varchar(max),
  @Link_Repeater_id   int


SELECT  
  @calc_results_xml   = dbo.Link.calc_results_xml,     
  
  @calc_results_xml_1 = Link_Repeater.calc_results_xml_1,
  @calc_results_xml_2 = Link_Repeater.calc_results_xml_2,
  @calc_results_xml_3 = Link_Repeater.calc_results_xml_3,
  
  @Link_Repeater_id = Link_Repeater.id  
  
FROM         
    dbo.Link 
     left OUTER JOIN dbo.Link_Repeater ON dbo.Link.id =  dbo.Link_Repeater.Link_ID
              
where Link.id = @id    


-------------------------------------------------------------


  declare 
    @s varchar(max) 



/*  declare 
    @s varchar(max) = 

'  
<?xml version="1.0" encoding="windows-1251"?>
<?xml-stylesheet type="text/xsl" href=""?>

<Document>
  <GROUP caption="1.���������� �������� ������ �� ���������">
    <ITEM id="1" caption=" ����� ��������� [km]" value="24.173" value2="24.173"/>
    <ITEM id="2" caption=" ������� ������� ������� [GHz]" value="13" value2="13"/>
    <ITEM id="184" caption=" ��� ����������� (1-������������., 2-�������.)" value="2" value2="2"/>
    <ITEM id="185" caption=" ��� ��������� (0-��������., 1-��������.(�����),2-��������.(��))" value="0" value2="0"/>
    <ITEM id="181" caption=" �������� �������, 0-�����.; 1-���� �-53363; 2-����; 3-E-����.; 4-���" value="0" value2="0"/>
  </GROUP>
   <GROUP caption="6.���������� ������� ���������� � ���.������������ V���">
    <ITEM id="152" caption=" ����������� �������� ��� ��� ���� [%]" value="0.0125" value2="0.0125"/>
    <ITEM id="42" caption=" ������������� ����� ���� [km]" value="24.173" value2="24.173"/>
       <ITEM id="159" caption=" ���������� ����� �������� � ���.������������ [km]" value="11.3649" value2="12.8081"/>
    <ITEM id="160" caption=" ������� Ho ��� ���� ���������� ������������ � ���.������������ [m]" value="6.80594" value2="6.80594"/>
    <ITEM id="161" caption=" ���������� ������� H(g) � ���.������������ [m]" value="10.75687" value2="10.75687"/>
    <ITEM id="162" caption=" ������������� ������� p(g) � ���.������������" value="1.58051" value2="1.58051"/>
    <ITEM id="15" caption=" ��������� ���������� � �������� ������������ V��� [dB]" value="0" value2="0"/>
  </GROUP> 
</Document>
 
 '
*/


-- select @xml 
   
 
declare 
  @report TABLE (
      tree_id int IDENTITY (1,1),
      tree_parent_id int,
          
      id int,
      caption varchar(100),
          
      value float,
      value2 float        
  )
  
 select * into #report  from @report 
 
  
declare
  @id_new int


  ----------------------------------------------------------
  --= @calc_results_xml 
  ----------------------------------------------------------
if @Link_Repeater_id is not null
begin

   insert into #report (caption) values ('1 -> ������������')
   set @id_new = @@IDEntity

   exec link_calc.sp_Link_calc_results_add_xml_SEL @TREE_PARENT_ID =@id_new,   @XML_TEXT  = @calc_results_xml_1
   
   -------------------------
   insert into #report (caption) values ('������������ -> 2')
   set @id_new = @@IDEntity
   
   exec link_calc.sp_Link_calc_results_add_xml_SEL @TREE_PARENT_ID =@id_new,  @XML_TEXT  = @calc_results_xml_2

   -------------------------
   insert into #report (caption) values ('1 -> ������������')
   set @id_new = @@IDEntity

   exec link_calc.sp_Link_calc_results_add_xml_SEL @TREE_PARENT_ID =@id_new,  @XML_TEXT  = @calc_results_xml_3
      

end else 
   exec link_calc.sp_Link_calc_results_add_xml_SEL   @XML_TEXT  = @calc_results_xml
   
      

  ----------------------------------------------------------
  
 
select * from #report

  return @@ROWCOUNT

   
END

GO
/****** Object:  StoredProcedure [link_calc].[sp_Link_calc_results_add_xml_SEL]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[link_calc].[sp_Link_calc_results_add_xml_SEL]') IS  not  NULL

DROP PROCEDURE [link_calc].[sp_Link_calc_results_add_xml_SEL]
GO
/****** Object:  StoredProcedure [link_calc].[sp_Link_calc_results_add_xml_SEL]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [link_calc].[sp_Link_calc_results_add_xml_SEL]
(
  @TREE_PARENT_ID int=null,
  @XML_TEXT       varchar(max) -- varchar(max) 
)

AS
BEGIN
  
declare
  @s varchar(max),
  @id_new int,
  @xml xml, 
  @node XML     



declare 
  @temp_tbl TABLE (node XML,   node_caption varchar(100)) 

  ----------------------------------------------------------
  --= @calc_results_xml 
  ----------------------------------------------------------
 set @s = @XML_TEXT 
  
 set @s = replace( @s, '<?xml version="1.0" encoding="windows-1251"?>','') 
-- set @s = replace( @s, '<?xml-stylesheet type="text/xsl" href=""?>','') 
 
  
set @xml = @s


--delete from  @temp_tbl

insert into  @temp_tbl
  select 
       r.n.query('.') , 
       r.n.value('@caption', 'varchar(100)') 
    FROM @xml.nodes('(//GROUP)') AS r(n)
        
        
  
  ----------------------------------------------------------
  
  while exists(select * from @temp_tbl)
  begin
    select top 1 @node=node from @temp_tbl
    delete top (1) from @temp_tbl
    
    
    
    insert into #report (caption, tree_parent_id) 
       select r.n.value('@caption', 'varchar(100)'), @TREE_PARENT_ID
          FROM @node.nodes('(//GROUP)') AS r(n)
        
    set @id_new = @@IDEntity
      
    insert into #report (tree_parent_id, caption,id, value, value2)          
       select          
            @id_new, 
             r.n.value('@caption', 'varchar(100)'),
             r.n.value('@id',  'int'),
             r.n.value('@value',  'float'),
             r.n.value('@value2', 'float')        
          FROM @node.nodes('(//ITEM)') AS r(n)
  end  

/*
insert into t_res (is_folder, caption )
  select distinct true, GROUP_caption
    from t_data_xml;

insert into t_res (id, value, caption, tree_parent_id )
  select m.id, m.value, m.caption, t_res.tree_id
    from t_data_xml m
      left join t_res on m.GROUP_caption = t_res.caption;

    */
  ----------------------------------------------------------
  
--select * from @report

  return @@ROWCOUNT

   
END

GO
/****** Object:  UserDefinedFunction [link_calc].[ft_Linkend_dop_antenna]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[link_calc].[ft_Linkend_dop_antenna]') IS not NULL

DROP FUNCTION  [link_calc].[ft_Linkend_dop_antenna]
GO
/****** Object:  UserDefinedFunction [link_calc].[ft_Linkend_dop_antenna]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE FUNCTION [link_calc].[ft_Linkend_dop_antenna] 
(
  @LINKEND_ID int    
)

--346447
--RETURNS varchar(50)

RETURNS @ret TABLE 
(
   id int,
   height  float , 
   gain float
 
)

as

BEGIN
  if @LINKEND_ID is null
    set @LINKEND_ID = 560386




  IF 2 = (select count(*)  from Linkend_Antenna where LINKEND_ID = @LINKEND_ID ) 
  begin
  --  select top 1 *  from Linkend_Antenna where LINKEND_ID = @LINKEND_ID order by height 
    
    insert into @ret
        select top 1 id, height, gain  
          from Linkend_Antenna where LINKEND_ID = @LINKEND_ID order by height      
  end
  
    
  RETURN
  
END

GO
/****** Object:  UserDefinedFunction [link].[fn_LinkEnd_name]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[link].[fn_LinkEnd_name]') IS  not  NULL


DROP FUNCTION [link].[fn_LinkEnd_name]
GO
/****** Object:  UserDefinedFunction [link].[fn_LinkEnd_name]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [link].[fn_LinkEnd_name]  
(
  @PROPERTY_ID int,
  
  @NAME varchar(200) 
)
RETURNS varchar(200)
AS
BEGIN

declare
  @n int = 0,
  @s varchar(200) 

  
  while @n < 100
  begin
    set @s = @NAME + iiF(@n=0, '', cast(@n as varchar(10)) ) 
        
    if not Exists (select * from dbo.LinkEnd   where property_id = @property_id and name = @s )
      return @s     
    
    
    set @n = @n +1
    
  end    
      
  
  return null

  /* Function body */
END

GO
/****** Object:  UserDefinedFunction [link].[fn_LinkEnd_Antenna_Get_Link_ID]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[link].[fn_LinkEnd_Antenna_Get_Link_ID]') IS NOT NULL

DROP FUNCTION [link].[fn_LinkEnd_Antenna_Get_Link_ID]
GO
/****** Object:  UserDefinedFunction [link].[fn_LinkEnd_Antenna_Get_Link_ID]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
purpose: get LINK id 
used in:   
--------------------------------------------------------------- */
CREATE FUNCTION [link].[fn_LinkEnd_Antenna_Get_Link_ID]
(
  @ID int  
)
RETURNS int 
BEGIN
          
--    583580
declare
  @LINK_ID int


;with A as
(
select linkend_id 
  from Linkend_Antenna 
  where id=@ID
)
  select @LINK_ID = id from link
    where 
      linkend1_id in (select * from A) or
      linkend2_id in (select * from A)

  RETURN @link_id
END

GO
/****** Object:  UserDefinedFunction [link].[ft_Link_calc_results_basic]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[link].[ft_Link_calc_results_basic]') IS NOT NULL

DROP FUNCTION [link].[ft_Link_calc_results_basic]
GO
/****** Object:  UserDefinedFunction [link].[ft_Link_calc_results_basic]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE FUNCTION [link].[ft_Link_calc_results_basic] 
(
  @ID int   
 
)

--346447
--RETURNS varchar(50)


--link_calc.ft_Linkend_dop_antenna 

RETURNS @ret TABLE 
(
    Rx_LEVEL     float, --<ITEM id="190" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_dop float, --<ITEM id="179" value="-200" caption=" ������� �������� ������� �� ����� ��������� (���) [dBm]"/>
    
    Rx_LEVEL_back     float, --<ITEM id="190" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_back_dop float, --<ITEM id="179" value="-200" caption=" ������� �������� ������� �� ����� ��������� (���) [dBm]"/>


    Rx_LEVEL_str varchar(50), --<ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_back_str varchar(50), --<ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    
    SESR float,
    KNG  float,
       
    Param_33 float, -- 33 �������� ����������� [dBm]
    Param_35 float,  -- ����������� �������� ���.������� (���) [dB]
    Param_36 float, 
    Param_37 float,  
    Param_40 float,

    Site2_dop_gain float,

    Rx_Level_draft float,
    Rx_Level_dop_draft float
    
  --  calc_results_xml  XML
    
)

as

BEGIN

---------------------------
if @id is null
  set @id = 253557
--  set @id = (select top 1 id from Link where calc_results_xml is not null )
---------------------------

DECLARE  
  @dop_gain float,
  @is_profile_reversed bit
  
 

 SELECT @is_profile_reversed=IsNull(is_profile_reversed ,0)  FROM dbo.link  where id=@id

 SELECT @dop_gain = LE.gain
   FROM dbo.link L  cross apply link_calc.ft_Linkend_dop_antenna 
            (IIF(@is_profile_reversed=1, Linkend1_id, Linkend2_id)) LE
    where  L.id=@id


/*    cross apply link_calc.ft_Linkend_dop_antenna  (Linkend2_id) LE
   where 
    L.id=@id
*/

/*
 SELECT @dop_gain = LE.gain
   FROM dbo.link L  cross apply link_calc.ft_Linkend_dop_antenna  (Linkend2_id) LE
    where  L.id=@id
    */
  

--link_calc.ft_Linkend_dop_antenna 


/*    <GROUP caption=" 13.���������� ������� ������ ������� �� ����� ���������">
      <ITEM id="33" value="30" caption=" �������� ����������� [dBm]"/>
      <ITEM id="35" value="42.97999" caption=" ����������� �������� ���.������� (���) [dB]"/>
      <ITEM id="36" value="39.89999" caption=" ����������� �������� ���.������� (���) [dB]"/>
      <ITEM id="37" value="7" caption=" ��������� ������ � ��� (���) [dB]"/>
      <ITEM id="39" value="0" caption=" �������������� ������ �� ��������� [dB]"/>
      <ITEM id="40" value="-140.48499" caption=" ���������� ������� � ��������� ������������ [dB]"/>
      <ITEM id="48" value="5.0954" caption=" ���������� �� ������� V��� ��-�� ������������� [dB]"/>
      <ITEM id="49" value="0" caption=" ���������� �� ������� V��� ��-�� ��������� [dB]"/>
      <ITEM id="94" value="-0.36859" caption=" ���������� ��-�� ���������� � ����� V��� [dB]"/>
      <ITEM id="99" value="-0.01261" caption=" ��������� ��-�� ������� ������ V��� [dB]"/>
      <ITEM id="100" value="4.71419" caption=" ������� ���������� V��=V���+V���+V���+V��� [dB]"/>
      <ITEM id="190" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (���) [dBm]"/>
      <ITEM id="179" value="-200" caption=" ������� �������� ������� �� ����� ��������� (���) [dBm]"/>
      <ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    </GROUP>

*/


   
  

--select * from link.ft_Link_Calc_results (@id)


  declare 
  	@result varchar(50);
   
  
  insert into @ret (Rx_LEVEL) values (null)

;WITH M as
(
  select * from link.ft_Link_Calc_results (@id)
)
  update @ret
  set
    Rx_LEVEL     = (select top 1 round([value],1) from m where id=190 ), -- <ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_dop = (select top 1 round([value],1) from m where id=179 ), -- <ITEM id="179" value="-200" caption=" ������� �������� ������� �� ����� ��������� (���) [dBm]"/>

    Rx_LEVEL_back     = (select top 1 round(value2,1) from m where id=190 ), -- <ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_back_dop = (select top 1 round(value2,1) from m where id=179 ), -- <ITEM id="179" value="-200" caption=" ������� �������� ������� �� ����� ��������� (���) [dBm]"/>


    SESR     = (select top 1 round([value],1) from m where id=16 ),
    KNG      = (select top 1 round([value],1) from m where id=17 ),
    

    Param_33   = (select top 1 round([value],1) from m where id=33 ),
    Param_35   = (select top 1 round([value],1) from m where id=35 ),
    Param_36   = (select top 1 round([value],1) from m where id=36 ),

    Param_37   = (select top 1 round([value],1) from m where id=37 ),
    Param_40   = (select top 1 round([value],1) from m where id=40 ),

   -- calc_results_xml = (select calc_results_xml from link where id=@id),

    Site2_dop_gain  = @dop_gain


/*      <ITEM id="25" value="24.38664" caption="dop-site1-��������  dB"/>
      <ITEM id="26" value="-9" caption="dop-site1-������ ���"/>
      <ITEM id="27" value="8929.6979" caption="dop-site1-������ � ���"/>
      <ITEM id="28" value="1.40183" caption="dop-site1-������"/>
      <ITEM id="29" value="0.7747" caption="dop-site2-�������"/>
      <ITEM id="30" value="2.7431" caption="dop-site2-�������� dB"/>
      <ITEM id="31" value="24.42277" caption="dop-site2-������ ���"/>
      <ITEM id="32" value="8.90334" caption="dop-site2-������ � ���"/>
      <ITEM id="33" value="30" caption="dop-site2-������"/>

*/
--group_caption='1.���������� �������� ������ �� ���������'


update @ret
  set 
    Rx_LEVEL_dop      = IIF(Rx_LEVEL_dop=-200, null, Rx_LEVEL_dop),
    Rx_LEVEL_back_dop = IIF(Rx_LEVEL_back_dop=-200, null, Rx_LEVEL_back_dop),
    
    Rx_Level_draft = Param_33 + Param_35 + Param_36 - Param_37 + Param_40 ,

-- <ITEM id="30" value="2.7431" caption="dop-site2-�������� dB"/>
    Rx_Level_dop_draft = Param_33 + Param_35 + @dop_gain - Param_37 + Param_40 
    
    

 

update @ret
  set 
      Rx_LEVEL_str = cast(Rx_LEVEL as varchar(50)) + 
                     IIF(Rx_LEVEL_dop is null, '', ' / '+ cast(Rx_LEVEL_dop as varchar(50))),

      Rx_LEVEL_back_str = cast(Rx_LEVEL_back as varchar(50)) + 
                          IIF(Rx_LEVEL_back_dop is null, '', ' / '+ cast(Rx_LEVEL_back_dop as varchar(50)))



--select * from @ret



  RETURN
  

/*


  (*
//  DEF_LENGTH_KM    = 1;
  DEF_STATUS       = 7;  //i?eaiaiinou eioa?aaea

  DEF_SESR         = 17;
  DEF_KNG          = 19;

  DEF_KNG_REQ      = 18;
  DEF_SESR_REQ     = 16;
  DEF_LOS          = 31;
  DEF_Rx_LEVEL_dBm = 101; //Iaaeaiiue o?iaaiu neaiaea ia aoiaa i?eaiieea (aAi)

  DEF_MAX_WEAKNESS     = 14;  //Aiionoeiia ineaaeaiea (aA)
  DEF_DIRECT_LINE_TILT = 95;
  
   
  */
    
    
	RETURN
  
END
/*
<?xml version="1.0" encoding="windows-1251"?>
<Document>
  <REPORT>
    <GROUP caption="���������� �������� ������ �� ���������">
      <ITEM id="1" value="2.84799" caption="����� ��������� [km]"/>
      <ITEM id="2" value="7.495" caption="������� ������� ������� [GHz]"/>
      <ITEM id="3" value="35" caption="������ ������� ����� ������� (���) [m]"/>
      <ITEM id="4" value="45" caption="������ ������� ������ ������� (���) [m]"/>
      <ITEM id="6" value="21" caption="������ ������� ������ ������� (���) [m]"/>
      <ITEM id="211" value="76" caption="������� ������ ������ (���) H[m]"/>
      <ITEM id="230" value="0" caption="��� ���-�� �� ���.(0-�������.���� 800�,1-����.����� 800� � ����� 1200�,2-����������.����� 1200�)"/>
      <ITEM id="184" value="2" caption="��� ����������� (1-������������., 2-�������.)"/>
      <ITEM id="185" value="0" caption="��� ��������� (0-��������., 1-��������.(�����),2-��������.(��))"/>
      <ITEM id="245" value="0" caption="����.(����) ������ �����������"/>
      <ITEM id="181" value="2" caption="�������� �������, 0-�����.; 1-���� �-53363; 2-����; 3-E-����.; 4-���"/>
    </GROUP>
    <GROUP caption="���������� ������� ����������� ����������� ���������">
      <ITEM id="7" value="1" caption="������� ����������� �� ���� ��������� (1-�������,0-���)"/>
      <ITEM id="8" value="1" caption="������� ����������� �� �������� p(a%) (1-�������,0-���)"/>
      <ITEM id="9" value="1" caption="������� ����������� �� �������� V��� (1-�������,0-���)"/>
      <ITEM id="10" value="1" caption="������� ����������� �� �������� SESR (1-�������,0-���)"/>
      <ITEM id="11" value="1" caption="������� ����������� �� �������� K�� (1-�������,0-���)"/>
      <ITEM id="12" value="1" caption="���������� ������������� ������� p_dop"/>
      <ITEM id="13" value="8.89016" caption="������������ ������������� ������� p(a%)"/>
      <ITEM id="14" value="-36.85576" caption="����������   ���������� � ���.������������ V��� [dB]"/>
      <ITEM id="15" value="0" caption="������������ ���������� � ���.������������ V��� [dB]"/>
      <ITEM id="16" value="0.006" caption="����������   ���������� �������� �� ������� SESR [%]"/>
      <ITEM id="17" value="1E-5" caption="������������ ���������� �������� �� ������� SESR [%]"/>
      <ITEM id="18" value="0.0125" caption="����������   ���������� ������������ K�� [%]"/>
      <ITEM id="19" value="1E-5" caption="������������ ���������� ������������ K�� [%]"/>
    </GROUP>
    <GROUP caption="���������� ������� �������� ��� a% ���������">
      <ITEM id="26" value="-9" caption="������� �������� ��������� ����.�������������, 10^-8 [1/�]"/>
      <ITEM id="153" value="10.32577" caption="����������� ���������� ���������, 10^-8 [1/m]"/>
      <ITEM id="217" value="0.55193" caption="�����.���.��� ��������� y(R)"/>
      <ITEM id="172" value="20" caption="���������� ����������� a% ���.�������� ������ p_dop [%]"/>
      <ITEM id="20" value="-0.00145" caption="����������� �������� g ��� a% ���������, 10^-8 [1/m]"/>
      <ITEM id="21" value="6370.29472" caption="������������� ������ ����� [km] (��� a% ���������)"/>
      <ITEM id="22" value="1.00004" caption="����������� ��������� (��� a% ���������)"/>
      <ITEM id="23" value="0.7747" caption="���������� ����� �������� [km] (��� a% ���������)"/>
      <ITEM id="24" value="2.7431" caption="������� Ho ��� ���� ���������� ������������ [m] (��� a% ���������)"/>
      <ITEM id="25" value="24.38664" caption="���������� ������� H(g) [m] (��� a% ���������)"/>
      <ITEM id="13" value="8.89016" caption="������������� ������� p(g)=p(a%)"/>
    </GROUP>
    <GROUP caption="���������� ������� �������� ��� ������� ���������">
      <ITEM id="26" value="-9" caption="����������� ��������, 10^-8 [1/m]"/>
      <ITEM id="27" value="8929.6979" caption="������������� ������ ����� [km]"/>
      <ITEM id="28" value="1.40183" caption="����������� ���������"/>
      <ITEM id="29" value="0.7747" caption="���������� ����� �������� [km]"/>
      <ITEM id="30" value="2.7431" caption="������� Ho ��� ���� ���������� ������������ [m]"/>
      <ITEM id="31" value="24.42277" caption="���������� ������� H(g) [m]"/>
      <ITEM id="32" value="8.90334" caption="������������� ������� p(g)"/>
    </GROUP>
    <GROUP caption="���������� ������� ���.����������� ���������� V���">
      <ITEM id="33" value="30" caption="�������� ����������� [dBm]"/>
      <ITEM id="34" value="-92.5" caption="��������� �������� ������� �� ����� ��������� [dBm]"/>
      <ITEM id="35" value="36.7" caption="����������� �������� ���.������� (���) [dB]"/>
      <ITEM id="36" value="36.7" caption="����������� �������� ���.������� (���) [dB]"/>
      <ITEM id="37" value="40" caption="��������� ������ � ��� (���) [dB]"/>
      <ITEM id="38" value="155.9" caption="�������������� ��������� ��� [dB]"/>
      <ITEM id="39" value="0" caption="�������������� ������ �� ��������� [dB]"/>
      <ITEM id="40" value="-119.04423" caption="���������� ������� � ��������� ������������ [dB]"/>
      <ITEM id="14" value="-36.85576" caption="���������� ���������� ���������� V��� [dB]"/>
    </GROUP>
    <GROUP caption="���������� ������� ���������� � ���.������������ V���">
      <ITEM id="152" value="0.0125" caption="����������� �������� ��� ��� ���� [%]"/>
      <ITEM id="42" value="2.84799" caption="������������� ����� ���� [km]"/>
      <ITEM id="155" value="0.2" caption="���������� ����� K��, ������������� �������������"/>
      <ITEM id="156" value="0.01" caption="���������� �������� K�� � ���.������������ [%]"/>
      <ITEM id="43" value="29.18731" caption="����������� �������� ��� ������������, 10^-8 [1/m]"/>
      <ITEM id="157" value="3301.17477" caption="������������� ������ ����� � ���.������������[km]"/>
      <ITEM id="158" value="0.51823" caption="����������� ��������� � ���.������������"/>
      <ITEM id="159" value="0.7747" caption="���������� ����� �������� � ���.������������ [km]"/>
      <ITEM id="160" value="2.7431" caption="������� Ho ��� ���� ���������� ������������ � ���.������������ [m]"/>
      <ITEM id="161" value="24.26943" caption="���������� ������� H(g) � ���.������������ [m]"/>
      <ITEM id="162" value="8.84744" caption="������������� ������� p(g) � ���.������������"/>
      <ITEM id="15" value="0" caption="��������� ���������� � �������� ������������ V��� [dB]"/>
    </GROUP>
    <GROUP caption="���������� ������� ���������� �� ������� ��� �����.����.">
      <ITEM id="44" value="1" caption="��� ��������� (1-��������,2-������������,3-��������)"/>
      <ITEM id="45" value="0" caption="K��������� �������� ���������"/>
      <ITEM id="48" value="0" caption="���������� �� ������� V��� ��-�� ������������� [dB]"/>
      <ITEM id="49" value="0" caption="���������� �� ������� V��� ��-�� ��������� [dB]"/>
    </GROUP>
    <GROUP caption="���������� ������� ��������� ���������� � �����">
      <ITEM id="89" value="7.5" caption="���������� ��������� �������� ���� [g/m2]"/>
      <ITEM id="182" value="15" caption="����������� ������� [��.�������]"/>
      <ITEM id="183" value="1013" caption="����������� �������� [����]"/>
      <ITEM id="90" value="0.00755" caption="����������� ���������� ��� ��������� [dB/km]"/>
      <ITEM id="91" value="0.00343" caption="����������� ���������� ��� �������� ���� [dB/km]"/>
      <ITEM id="92" value="-0.02152" caption="���������� ��-�� ���������� � ��������� [dB]"/>
      <ITEM id="93" value="-0.00978" caption="���������� ��-�� ���������� � ������� ���� [dB]"/>
      <ITEM id="94" value="-0.03131" caption="���������� ��-�� ���������� � ����� V��� [dB]"/>
    </GROUP>
    <GROUP caption="���������� ������� ���������� ��-�� ������� ������">
      <ITEM id="95" value="0.92534" caption="���� ������� ������ (U) [*]"/>
      <ITEM id="98" value="0.85015" caption="������� ����������� ���������� �(U)"/>
      <ITEM id="97" value="-0.01421" caption="��������� ����������� ���������� F(f) [dB/km]"/>
      <ITEM id="99" value="-0.03442" caption="��������� ��-�� ������� ������ V��� [dB]"/>
    </GROUP>
    <GROUP caption="���������� ������� ������ ������� �� ����� ���������">
      <ITEM id="33" value="30" caption="�������� ����������� [dBm]"/>
      <ITEM id="35" value="36.7" caption="����������� �������� ���.������� (���) [dB]"/>
      <ITEM id="36" value="36.7" caption="����������� �������� ���.������� (���) [dB]"/>
      <ITEM id="37" value="40" caption="��������� ������ � ��� (���) [dB]"/>
      <ITEM id="39" value="0" caption="�������������� ������ �� ��������� [dB]"/>
      <ITEM id="40" value="-119.04423" caption="���������� ������� � ��������� ������������ [dB]"/>
      <ITEM id="48" value="0" caption="���������� �� ������� V��� ��-�� ������������� [dB]"/>
      <ITEM id="49" value="0" caption="���������� �� ������� V��� ��-�� ��������� [dB]"/>
      <ITEM id="94" value="-0.03131" caption="���������� ��-�� ���������� � ����� V��� [dB]"/>
      <ITEM id="99" value="-0.03442" caption="��������� ��-�� ������� ������ V��� [dB]"/>
      <ITEM id="100" value="-0.06573" caption="������� ���������� V��=V���+V���+V���+V��� [dB]"/>
      <ITEM id="190" value="-55.70996" caption="������� �������� ������� �� ����� ��������� (���) [dBm]"/>
      <ITEM id="179" value="-55.69365" caption="������� �������� ������� �� ����� ��������� (���) [dBm]"/>
      <ITEM id="101" value="-55.69365" caption="������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
      <ITEM id="180" value="1" caption="T�� ���.��������� (0-��� ���.���.,1-����.,2-��������.,3-����.)"/>
    </GROUP>
    <GROUP caption="���������� ������� ���������� �������">
      <ITEM id="105" value="1.15909" caption="dV� ���������� ��-�� ���������.����� [dB]"/>
      <ITEM id="106" value="0" caption="dV� ���������� ��-�� ���. �������� [dB]"/>
      <ITEM id="107" value="1.15909" caption="dV���� ��������� ���������� ��-�� ������� �������� �������� [dB]"/>
    </GROUP>
    <GROUP caption="������� V���.��.�� ����������� ����.��� ������.�����.">
      <ITEM id="118" value="18" caption="������� ������� (�������� ��������) [Mbit/s]"/>
      <ITEM id="119" value="0" caption="��� ���������, 0-PSK (QPSK), 1-QAM (TCM)"/>
      <ITEM id="120" value="4" caption="����� ������� ���������"/>
      <ITEM id="123" value="0" caption="I���  �������, �������� ������������ [dB]"/>
      <ITEM id="124" value="-59.25843" caption="V���.��.�� ���������� ����.��� ������.��������� [dB]"/>
    </GROUP>
    <GROUP caption="������ ���������� ���������� �������� ��������� ������.">
      <ITEM id="14" value="-36.85576" caption="V��� ��� ����� ������� ����� � ������. ��������� [dB]"/>
      <ITEM id="107" value="1.15909" caption="dV���� ��������� ���������� ��-�� ������� �������� �������� [dB]"/>
      <ITEM id="100" value="-0.06573" caption="V�� ������� �������� ��������� ���������� [dB]"/>
      <ITEM id="189" value="0" caption="dVp ������� �� ATPC (���.���.����.���)[dB]"/>
      <ITEM id="124" value="-59.25843" caption="V���.��.�� ���������� ����.��� ������.��������� [dB]"/>
      <ITEM id="125" value="-35.62838" caption="V���.�� ��� ����� ������� �����������. ��������� [dB]"/>
      <ITEM id="126" value="-36.79003" caption="V���.� ��� ����� ������� ����������. ��������� [dB]"/>
      <ITEM id="127" value="-35.64724" caption="V���.� ��� ����� ������� ���������� � ������ [dB]"/>
    </GROUP>
    <GROUP caption="���.������� ���������������� ������������ ��������������">
      <ITEM id="126" value="-36.79003" caption="V���.� ��� ����� ������� ����������. ��������� [dB]"/>
      <ITEM id="128" value="1" caption="�������� ������ ������� V����(g)=V���.� [dB]"/>
      <ITEM id="129" value="80" caption="����������� �������� g ��� ������������, 10^-8 [1/m]"/>
      <ITEM id="168" value="1795.37767" caption="������������� ������ ����� (��� ����.�����.) [km]"/>
      <ITEM id="169" value="0.28184" caption="����������� ��������� (��� ����.�����.)"/>
      <ITEM id="130" value="0" caption="��������� ���������� ��� ������������ V����(g) [dB]"/>
      <ITEM id="131" value="0" caption="To ���������������� ������������ �������������� [%]"/>
    </GROUP>
    <GROUP caption="���.������� ����������������� ������������ ��������-���">
      <ITEM id="125" value="-35.62838" caption="V���.�� ��� ����� ������� �����������. ��������� [dB]"/>
      <ITEM id="133" value="20.5" caption="������������� ������ K [10^-6]"/>
      <ITEM id="132" value="1" caption="Q-������ ������ �����������"/>
      <ITEM id="134" value="1.5" caption="�������� ������������ b (������� ������� F^b)"/>
      <ITEM id="135" value="1" caption="�������� ������������ c (���.����. �*Q*c)"/>
      <ITEM id="136" value="3" caption="�������� ������������ d (������� ����� R^d)"/>
      <ITEM id="234" value="97.61425" caption="����.���������.�������.�����.��� ��� (���)"/>
      <ITEM id="235" value="1" caption="����.���������.�������.�����.��� ��� (���)"/>
      <ITEM id="236" value="0" caption="T��� �����������. ������������ �������������� ��� ��� (���) [%]"/>
      <ITEM id="237" value="0" caption="T��� �����������. ������������ �������������� ��� ��� (���) [%]"/>
      <ITEM id="176" value="3806713.87996" caption="���������� �����.��-�� ������������� �� ���� ��� [���]"/>
      <ITEM id="163" value="0.00971" caption="P��� ����������� �����������.��������� � ������ ������� ������ � ������ [%]"/>
      <ITEM id="137" value="0" caption="T��� �����������. ������������ �������������� [%]"/>
    </GROUP>
    <GROUP caption="���.������� ������������ �������������� ��-�� ������">
      <ITEM id="127" value="-35.64724" caption="V���.� ��� ����� ������� ���������� � ������ [dB]"/>
      <ITEM id="213" value="1" caption="����� ������ �� ����� ������. ������ (����-98)"/>
      <ITEM id="212" value="399.96337" caption="������������� ����� (��� ������� ���������� = V���.�) [mm/h] (����-98)"/>
      <ITEM id="139" value="0" caption="��� ����������� (0-��������������,1-������������,2-�����)"/>
      <ITEM id="140" value="1.33794" caption="����������� a"/>
      <ITEM id="141" value="0.00361" caption="����������� K"/>
      <ITEM id="154" value="10.94715" caption="�������� ��������� � ����� K*I^a[dB/km]"/>
      <ITEM id="142" value="1.64786" caption="����������� ������������� ��������� ����������� [km]"/>
      <ITEM id="143" value="0.5786" caption="����������� ���������������� ����������� �����"/>
      <ITEM id="214" value="1" caption="����� ������ �� ����� Qd"/>
      <ITEM id="216" value="0" caption="T�.� �������� ������������ �������������� �� ������ ����� [%]"/>
      <ITEM id="145" value="0" caption="T� �������� ������������ �������������� [%]"/>
    </GROUP>
    <GROUP caption="���������� ������� ������������ �������������� ��� SESR">
      <ITEM id="193" value="999.95883" caption="�����.����.��������� Cm (���)"/>
      <ITEM id="194" value="999.97684" caption="�����.����.��������� Cm (���)"/>
      <ITEM id="195" value="11.60021" caption="��������� �������� ������������ ���.��������� Tm.� (���)[c]"/>
      <ITEM id="196" value="11.62213" caption="��������� �������� ������������ ���.��������� Tm.� (���)[c]"/>
      <ITEM id="222" value="11.60021" caption="��������� �������� ������������ ���.��������� Tm.� [c]"/>
      <ITEM id="197" value="5.96214" caption="��� ������������ ���.��������� (���)[c]"/>
      <ITEM id="198" value="5.96295" caption="��� ������������ ���.��������� (���)[c]"/>
      <ITEM id="223" value="4.81675" caption="��� ������������ ���.��������� [c]"/>
      <ITEM id="225" value="0.06932" caption="���� ���.��������� � ����.����� 10� (���)"/>
      <ITEM id="226" value="0.06911" caption="���� ���.��������� � ����.����� 10� (���)"/>
      <ITEM id="227" value="0.06921" caption="���� ���.��������� � ����.����� 10� (��)"/>
      <ITEM id="228" value="0.10695" caption="���� ���.��������� � ����.����� 10�"/>
      <ITEM id="146" value="0" caption="SESR.� ���������������� ������������ [%]"/>
      <ITEM id="148" value="0" caption="SESR.� ����������������� ������������ [%]"/>
      <ITEM id="147" value="0" caption="SESR.� �������� ������������ [%]"/>
      <ITEM id="17" value="1E-5" caption="�������� ���������� �������� �� ������� SESR [%]"/>
      <ITEM id="16" value="0.006" caption="����������� �������� SESR �� ��������� [%]"/>
    </GROUP>
    <GROUP caption="���������� ������� ������������ �������������� ��� ���">
      <ITEM id="202" value="0.25" caption="�����.��������� ���.�(�) �� ����������� ������ � ��������� ����"/>
      <ITEM id="224" value="0.89304" caption="���� ���.��������� � ����.����� 10�"/>
      <ITEM id="199" value="0.93067" caption="���� ���.��������� � ����.����� 10� (���)"/>
      <ITEM id="200" value="0.93088" caption="���� ���.��������� � ����.����� 10� (���)"/>
      <ITEM id="201" value="0.93078" caption="���� ���.��������� � ����.����� 10� (��)"/>
      <ITEM id="149" value="0" caption="���.� ���������������� ������������ [%]"/>
      <ITEM id="151" value="0" caption="���.� ����������������� ������������ [%]"/>
      <ITEM id="150" value="0" caption="���.� �������� ������������ [%]"/>
      <ITEM id="170" value="0" caption="��� ���-1 (�����) [%]"/>
      <ITEM id="171" value="0" caption="��� ���-2 (������) [%]"/>
      <ITEM id="19" value="1E-5" caption="�������� ���������� ������������ K�� [%]"/>
      <ITEM id="18" value="0.0125" caption="����������� �������� K�� �� ��������� [%]"/>
    </GROUP>
  </REPORT>
</Document>
*/

GO
/****** Object:  UserDefinedFunction [link].[ft_Link_Calc_results]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[link].[ft_Link_Calc_results]') IS NOT NULL

DROP FUNCTION [link].[ft_Link_Calc_results]
GO
/****** Object:  UserDefinedFunction [link].[ft_Link_Calc_results]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [link].[ft_Link_Calc_results]
(
  @ID int =null
)
      
RETURNS @ret TABLE 
(    
  group_caption nvarchar(100),
  [caption] 	nvarchar(100),
  id 		    int,
  [value] 		float,
  [value2]		float

)

as

BEGIN

---------------------------
if @id is null
  set @id = (select top 1 id from Link where calc_results_xml is not null )
---------------------------


declare   
  @calc_results_xml varchar(max)  =  (SELECT calc_results_xml  FROM dbo.Link where id = @id) , 
  @xml xml -- =  (SELECT cast(calc_results_xml as XML) FROM dbo.Link where id = @id) 


set @xml = replace (@calc_results_xml, '<?xml version="1.0" encoding="windows-1251"?>', '<?xml version="1.0"?>')


insert into @ret
  select
 
       r.n.value('../@caption', 'nvarchar(100)'),
       r.n.value('@caption', 'nvarchar(100)'),
       r.n.value('@id',  'int'),
       r.n.value('@value',  'float'),
       r.n.value('@value2', 'float')        
     FROM @xml.nodes('(//ITEM)') AS r(n)


  RETURN
  
END

GO
/****** Object:  View [link].[view_Link_summary]    Script Date: 16.05.2020 10:54:58 ******/


IF OBJECT_ID(N'[link].[view_Link_summary]') IS NOT NULL

DROP VIEW [link].[view_Link_summary]
GO
/****** Object:  View [link].[view_Link_summary]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [link].[view_Link_summary]
-- added: calc_method

AS
SELECT 

a1.lat AS lat1, 
a1.lon AS lon1, 
a2.lat AS lat2, 
a2.lon AS lon2, 


--Round(100-dbo.Link.Kng,2) as Reliability,
  FORMAT ( 100 - dbo.Link.Kng , 'N5')  as Reliability,  -- ���������� 


dbo.Link.Is_CalcWithAdditionalRain,

  dbo.Link.calc_method,  
  dbo.Link.id, 
  dbo.Link.rx_level_dBm, 
  dbo.Link.SESR_norm,
  dbo.Link.KNG_norm, 
  dbo.Link.fade_margin_dB, 
  
 -- dbo.Link.SESR, 
  --cast(dbo.Link.SESR as varchar(10)) as SESR_str, 
  
  FORMAT(dbo.Link.SESR, 'N5') as SESR, 

---  dbo.Link.Kng, 

  FORMAT(dbo.Link.Kng, 'N5') as Kng, 
  
  
    
  FORMAT ( 100 - dbo.Link.Kng , 'N5')  as K_gotovnost,
  
--  FORMAT (link.fn_K_gotovnost (dbo.Link.Kng, dbo.Link.SESR) , 'N5')  as K_gotovnost,
  
 dbo.Link.objname,

 dbo.Link.linkend1_id,
 dbo.Link.linkend2_id,

 dbo.Link.is_profile_reversed,


Link_Repeater.id as Link_Repeater_id,

LinkEnd1.property_id as property1_id,
LinkEnd2.property_id as property2_id,


  dbo.Link.clutter_model_id, 
  dbo.Link.refraction, 
  dbo.Link.refraction_2, 
  dbo.Link.profile_step, 
  dbo.Link.profile_XML, 

  dbo.Link.length,
  dbo.Link.length_km,

  dbo.Link.calc_results_XML, 

  
  dbo.Link.status, 
  dbo.Link.status_back,
  dbo.Link.kng_year, 
  dbo.Link.NFrenel,
  
  dbo.Link.rain_intensity_extra, 
  dbo.Link.rain_IsCalcLength,  
  dbo.Link.rain_Weakening_vert, 
  

  LinkEnd1.bitrate_Mbps, 
  LinkEnd1.tx_freq_MHz, 
  LinkEnd1.band, 
  
  LinkEnd1.modulation_type, 
  LinkEnd1.channel_width_MHz,
  

  
  dbo.Link.rain_Weakening_horz, 
  dbo.Link.Rain_Length_km,
  dbo.Link.BER_required,
  dbo.Link.rain_Allowable_Intense_vert,
  dbo.Link.rain_signal_quality, 
  dbo.Link.rain_Allowable_Intense_horz,
  
  dbo.fn_LinkEnd_GetPolarizationStr(dbo.Link.linkend1_id) AS  Polarization_Str, 
  
  dbo.fn_Get_Space_spread_m(LinkEnd1.tx_freq_MHz) AS Space_spread_m, 
  
  dbo.fn_Get_Frenel_max_m( LinkEnd1.tx_freq_MHz,  dbo.Link.NFrenel, dbo.Link.length)  AS Frenel_max_m, 
                      
  dbo.fn_BerToStr(dbo.Link.BER_required) AS  BER_required_str, 
                      
  dbo.fn_PickList_GetValueByCode('link', 'status', dbo.Link.status) AS status_str, 
  dbo.fn_PickList_GetValueByCode('link', 'status', dbo.Link.status) AS status_name,
  
  Link_status.name as status_str1,
  Link_status.name as status_name1
  
                      
FROM dbo.Link LEFT JOIN
     dbo.LinkEnd LinkEnd1 ON dbo.Link.linkend1_id = LinkEnd1.id
     LEFT  JOIN
     dbo.LinkEnd LinkEnd2 ON dbo.Link.linkend2_id = LinkEnd2.id
     
       left OUTER JOIN dbo.Link_Repeater ON dbo.Link.id =  dbo.Link_Repeater.Link_ID
       
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND1_ID) A1
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND2_ID) A2

     LEFT  JOIN  lib.Link_status  Link_status ON Link_status.id = Link.status
     

/*where Link.id = 250295
*/

GO
/****** Object:  StoredProcedure [link].[sp_Link_Summary]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[link].[sp_Link_Summary]') IS NOT NULL

DROP PROCEDURE [link].[sp_Link_Summary]
GO
/****** Object:  StoredProcedure [link].[sp_Link_Summary]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE [link].[sp_Link_Summary]
(
	@ID int = 5610
)
AS
BEGIN
 -- set @ID=79793

 -- TEST
 --SELECT top 1 @id= id FROM link 


  --------------------------------------------------------------
  IF @ID IS NULL  RAISERROR ('@ID is NULL', 16, 1);
  --------------------------------------------------------------


SELECT * FROM link.view_link_summary WHERE id=@ID 


/*    
  IF EXISTS(SELECT * FROM LINK WHERE (id=@ID) and (linkend1_id>0))    
    SELECT * FROM view_link_summary WHERE id=@ID 
  ELSE
  
  IF EXISTS(SELECT * FROM LINK WHERE (id=@ID) and (pmp_terminal_id>0))
    SELECT * FROM view_pmp_link_summary WHERE id=@ID 
*/ 

     
  RETURN @@ROWCOUNT
  
  
  
  
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Link_Add]    Script Date: 16.05.2020 10:54:58 ******/

/****** Object:  View [dbo].[view_Link_base]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[view_Link_base]') IS not  NULL

DROP VIEW [dbo].[view_Link_base]
GO
/****** Object:  View [dbo].[view_Link_base]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */

CREATE VIEW [dbo].[view_Link_base]
AS
SELECT 
  dbo.Link.project_id,

  dbo.Link.id,
  dbo.Link.name,

--  dbo.Link.project_id,
  dbo.Link.guid,
  
   dbo.Link.folder_id,
--  dbo.Link.user_created,
  
  linkend1_id,
  linkend2_id,


  Property1.id AS Property1_id,
  Property2.id AS Property2_id
  
/*  Property1.georegion_id AS georegion1_id,
  Property2.georegion_id AS georegion2_id
*/
  
FROM
  dbo.Link

  left JOIN dbo.linkend linkend1 ON dbo.Link.linkend1_id = linkend1.id
  left JOIN dbo.linkend linkend2 ON dbo.Link.linkend2_id = linkend2.id

  left JOIN dbo.Property Property1 ON linkend1.Property_id = Property1.id
  left JOIN dbo.Property Property2 ON linkend2.Property_id = Property2.id

  
WHERE
  Link.objname = 'link' OR 
  Link.objname IS NULL

GO
/****** Object:  View [dbo].[view_Link_report]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[view_Link_report]') IS  not  NULL


DROP VIEW [dbo].[view_Link_report]
GO
/****** Object:  View [dbo].[view_Link_report]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_Link_report]
AS
SELECT     dbo.Link.*, 


Round(100-dbo.Link.Kng,2) as Reliability,


a1.lat AS lat1, 
a1.lon AS lon1, 
a2.lat AS lat2, 
a2.lon AS lon2, 




/*Property1.georegion_id AS georegion1_id, 
Property2.georegion_id AS georegion2_id,
*/


-- Property1
Property1.address AS Property1_address, 
Property2.address AS Property2_address,

Property1.name AS Property1_name, 
Property2.name AS Property2_name, 

--Property1.name AS [Property1_name], 
--Property2.name AS [Property2_name], 

--Property1.id AS Property1_id, 
--Property2.id AS Property2_id, 


Property1.code AS Property1_code, 
Property2.code AS Property2_code, 


Property1.ground_height AS Property1_ground_height, 
Property2.ground_height AS Property2_ground_height,

GeoRegion_1.name AS [Property1_GeoRegion_name], 
GeoRegion_2.name AS [Property2_GeoRegion_name], 


Property1.Name_ERP AS [Property1_name_ERP], 
Property1.name_SDB AS [Property1_name_SDB], 

    

  --LinkEnd1.Band, -- AS LinkEnd1_LinkID,        


  LinkEnd1.LinkID AS LinkEnd1_LinkID,        
  LinkEnd2.LinkID AS LinkEnd2_LinkID,        

  LinkEnd1.band AS band,        
  
  LinkEnd1.bitrate_Mbps AS bitrate_Mbps,  
  LinkEnd1.tx_freq_MHz AS tx_freq_MHz, 
  LinkEnd1.rx_freq_MHz AS rx_freq_MHz, 


/*
-- ��������������
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd1.kratnostBySpace) AS LinkEnd1_kratnostBySpace, 
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd2.kratnostBySpace) AS LinkEnd2_kratnostBySpace, 

*/

ant1.diameter AS antenna1_diameter, 
ant2.diameter AS antenna2_diameter, 

ant1.height AS antenna1_height, 
ant2.height AS antenna2_height, 

ant1.polarization AS antenna1_polarization, 
ant2.polarization AS antenna2_polarization, 

  
        
  dbo.ClutterModel.name AS Clutter_Model_Name, 
        
  dbo.LinkType.name AS LinkType_Name, 
--  dbo.Status.name AS status_name, 
  dbo.Status.name AS State_name, 

--  dbo.Status.name AS status_str, 

      
link_status_tbl.value as status_str, 
 
link_calc_method_tbl.value as calc_method_name, 


--     left OUTER JOIN [lib].ft_PickList ('link', 'calc_method')  as link_calc_method_tbl   on link_calc_method_tbl.id = Link.calc_method

 
  
/*        
  dbo.fn_PickList_GetValueByCode('link', 'status',   dbo.Link.status) AS status_str, 
  dbo.fn_PickList_GetValueByCode('link', 'GST_type',  dbo.Link.calc_method) AS GST_type_name, 
  dbo.fn_PickList_GetValueByCode('link', 'calc_method',  dbo.Link.calc_method) AS calc_method_name, 
  dbo.fn_PickList_GetValueByCode('link', 'LOS_status',  dbo.Link.LOS_status) AS LOS_status_name, 
  dbo.fn_PickList_GetValueByCode('link', 'terrain_type',  dbo.Link.terrain_type) AS terrain_type_name, 
  dbo.fn_PickList_GetValueByCode('link', 'underlying_terrain_type', dbo.Link.underlying_terrain_type) AS underlying_terrain_type_name ,
*/

  
  Link_Repeater.id as Link_Repeater_id,
  
  LinkEndType.name as LinkEndType_name,
  
  Link_calc_res.Rx_LEVEL_str,
  Link_calc_res.Rx_LEVEL_back_str,
  
  
  --    Rx_Level_draft = Param_33 + Param_35 + Param_36 - Param_37 + Param_40 

Site2_dop_gain,

  Link_calc_res.Rx_LEVEL_draft,
  Link_calc_res.Rx_LEVEL_dop_draft,
  

  cast(Link_calc_res.Rx_LEVEL_draft  as varchar(10)) + 
     COALESCE(' / ' + Cast(Link_calc_res.Rx_LEVEL_dop_draft as varchar(10)), '') as Rx_LEVEL_draft_str
  
        
/*  
     Rx_LEVEL     float, --<ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_dop float, --<ITEM id="179" value="-200" caption=" ������� �������� ������� �� ����� ��������� (���) [dBm]"/>
    
    Rx_LEVEL_back     float, --<ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_back_dop float, --<ITEM id="179" value="-200" caption=" ������� �������� ������� �� ����� ��������� (���) [dBm]"/>


    Rx_LEVEL_str varchar(50), --<ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_back_str varchar(50), --<ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    
    SESR float,
    KNG  float
*/
  
  
  
        
FROM         dbo.LinkEnd LinkEnd2 

              RIGHT OUTER JOIN dbo.LinkEnd LinkEnd1 
              
      RIGHT OUTER JOIN dbo.LinkEndType ON LinkEndType.id = LinkEnd1.LinkEndType_id  
      RIGHT OUTER JOIN dbo.Link ON LinkEnd1.id = dbo.Link.linkend1_id ON LinkEnd2.id = dbo.Link.linkend2_id 
      LEFT OUTER JOIN dbo.Status ON dbo.Link.status_id = dbo.Status.id 
      LEFT OUTER JOIN dbo.GeoRegion GeoRegion_2 
      RIGHT OUTER JOIN dbo.Property Property2 ON GeoRegion_2.id = Property2.georegion_id ON LinkEnd2.Property_id = Property2.id 
      LEFT OUTER JOIN dbo.GeoRegion GeoRegion_1 
      RIGHT OUTER JOIN dbo.Property Property1 ON GeoRegion_1.id = Property1.georegion_id ON LinkEnd1.Property_id = Property1.id 
      LEFT OUTER JOIN dbo.LinkType ON dbo.Link.linkType_id = dbo.LinkType.id LEFT OUTER JOIN dbo.ClutterModel ON dbo.Link.clutter_model_id = dbo.ClutterModel.id
                 
       left OUTER JOIN dbo.Link_Repeater ON dbo.Link.id =  dbo.Link_Repeater.Link_ID
              
                 
       OUTER apply fn_Linkend_Antenna_info(LinkEnd1.id) ant1
       OUTER apply fn_Linkend_Antenna_info(LinkEnd2.id) ant2
               
              
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND1_ID) A1
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND2_ID) A2
              
     
     left OUTER JOIN [lib].ft_PickList ('link', 'status')       as link_status_tbl        on link_status_tbl.id = Link.status
     left OUTER JOIN [lib].ft_PickList ('link', 'GST_type')     as link_GST_type_tbl      on link_GST_type_tbl.id = Link.GST_type
     left OUTER JOIN [lib].ft_PickList ('link', 'calc_method')  as link_calc_method_tbl   on link_calc_method_tbl.id = Link.calc_method
     left OUTER JOIN [lib].ft_PickList ('link', 'LOS_status')   as link_LOS_status_tbl    on link_LOS_status_tbl.id = Link.LOS_status

     left OUTER JOIN [lib].ft_PickList ('link', 'terrain_type') as link_terrain_types_tbl on link_terrain_types_tbl.id = Link.terrain_type

     left OUTER JOIN [lib].ft_PickList ('link', 'underlying_terrain_type') as link_underlying_terrain_type_tbl on link_underlying_terrain_type_tbl.id = Link.underlying_terrain_type

     left OUTER JOIN [lib].ft_PickList ('linkend', 'kratnostBySpace') as linkend_kratnostBySpace1_tbl on linkend_kratnostBySpace1_tbl.id = LinkEnd1.kratnostBySpace
     left OUTER JOIN [lib].ft_PickList ('linkend', 'kratnostBySpace') as linkend_kratnostBySpace2_tbl on linkend_kratnostBySpace2_tbl.id = LinkEnd2.kratnostBySpace
     
     
    OUTER apply link.[ft_Link_calc_results_basic] (Link.id) Link_calc_res


/*
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd1.kratnostBySpace) AS LinkEnd1_kratnostBySpace, 
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd2.kratnostBySpace) AS LinkEnd2_kratnostBySpace, 
*/
  
--where Link.id=253557
             
               
--WHERE     dbo.Link.id =  1683

GO
IF OBJECT_ID(N'[ui].[Filters]') IS  NULL

begin
 
CREATE TABLE [ui].[Filters](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[objname] [varchar](30) NULL,
	[name] [varchar](100) NULL,
	[comment] [text] NULL,
	[is_shared] [bit] NULL,
	[host_name] [varchar](30) NULL DEFAULT (host_name()),
	[sql_body] [text] NULL,
	[user_created] [varchar](50) NULL DEFAULT (suser_name()),
	[date_created] [datetime] NULL DEFAULT (getdate()),
 CONSTRAINT [Filters_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

end

GO
/****** Object:  StoredProcedure [ui].[sp_Custom_Column_DEL]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[ui].[sp_Custom_Column_DEL]') IS  not NULL

DROP PROCEDURE [ui].[sp_Custom_Column_DEL]
GO
/****** Object:  StoredProcedure [ui].[sp_Custom_Column_DEL]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------------------------------------------------
CREATE PROCEDURE [ui].[sp_Custom_Column_DEL]
------------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(100)
)
AS
BEGIN


IF OBJECT_ID(@TABLE_NAME) IS NULL
begin
  print 'OBJECT_ID(@TableName) is null):' + @TABLE_NAME
  RETURN -1;
end


declare 
  @s varchar(max) = 'ALTER TABLE '+ @TABLE_NAME +' DROP COLUMN  '+ @COLUMN_NAME 


print @s
exec (@s)

----------------------------------------

declare 
  @LIB_TABLE_NAME varchar(100) = 'lib.'+ + @TABLE_NAME + '_'+  @COLUMN_NAME  


IF OBJECT_ID(@LIB_TABLE_NAME) IS NULL
begin
  set  @s = 'drop TABLE '+ @LIB_TABLE_NAME
  
  print @s
  exec (@s)

end



end

GO
/****** Object:  StoredProcedure [ui].[sp_Custom_Column_INS]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[ui].[sp_Custom_Column_INS]') IS  not  NULL

DROP PROCEDURE [ui].[sp_Custom_Column_INS]
GO
/****** Object:  StoredProcedure [ui].[sp_Custom_Column_INS]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------------------------------------------------
CREATE PROCEDURE [ui].[sp_Custom_Column_INS]
------------------------------------------------------------------
(
  @TABLE_NAME  VARCHAR(50),
  @COLUMN_NAME VARCHAR(100),

  @COLUMN_DESC VARCHAR(200),

  @COLUMN_TYPE VARCHAR(50),
  @COLUMN_SIZE int,
  
-- int, float, bit,  varchar(n),  list 

  @DEFAULT VARCHAR(50)=null
 
)
AS
BEGIN

declare
@s   varchar(1000),
-- @null  varchar(50),
@def varchar(50)

set @def=''
if IsNull(@DEFAULT,'') <> ''
set @def = ' DEFAULT (' + @DEFAULT + ') '

/*
set @null=''
if ISNULL(@ISNULL,'') = ''
set @null = ' '
else
set @null = ' NOT NULL'
*/

IF OBJECT_ID(@TABLE_NAME) IS NULL
begin
  print 'OBJECT_ID(@TableName) is null):' + @TABLE_NAME
  RETURN -1;
end

/*------------------------
if dbo._ColumnExists(@TABLE_NAME, @COLUMN_NAME)=0
---------------------------
begin
set @s = 'ALTER TABLE '+ @TABLE_NAME +' ADD   '+ @COLUMN_NAME + ' ' + @COLUMN_TYPE  +' '+ @def  -- + @null
print @s
exec (@s)

--   set @s = 'ALTER TABLE '+ @TABLE_NAME +' ALTER COLUMN   '+ @COLUMN_NAME + ' ' + @COLUMN_TYPE  + @null
--  print @s
-- exec (@s)

print @@rowcount
end else
print 'column exists: ' + @TABLE_NAME +' - '+ @COLUMN_NAME
*/

end

GO
/****** Object:  StoredProcedure [ui].[sp_Filter_INS_UPD]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[ui].[sp_Filter_INS_UPD]') IS  not NULL

DROP PROCEDURE [ui].[sp_Filter_INS_UPD]
GO
/****** Object:  StoredProcedure [ui].[sp_Filter_INS_UPD]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ui].[sp_Filter_INS_UPD]
(
   @ID int = null,
   ---------------------------
   
   @OBJNAME   varchar(100)=null,

   @NAME 	  varchar(100)=null,
   @is_shared bit = null
 --  @BODY 	  varchar(max)=null
   
   ----------------------------
   --@ACTION varchar(10)=null
   
)
AS
BEGIN
 -- if @ACTION = 'select' 
  --  select * from Filters where OBJNAME=@OBJNAME
  
  if @ID is null    
  begin
    insert into ui.Filters  (objname) values (@objname)

    set @id = @@IDENTITY  
  end
  
  

  
--  if @ID is not null    
    UPDATE 
      ui.Filters  
    SET 
      objname = COALESCE( @objname, objname),
      name 	  = COALESCE( @name, name),
   --   body    = COALESCE( @body, body),

      is_shared = COALESCE(@is_shared, is_shared)
    WHERE 
      id = @id;

 
  /* Procedure body */
END

GO
/****** Object:  StoredProcedure [ui].[sp_Filter_SEL]    Script Date: 16.05.2020 10:54:58 ******/
IF OBJECT_ID(N'[ui].[sp_Filter_SEL]') IS  not NULL


DROP PROCEDURE [ui].[sp_Filter_SEL]
GO
/****** Object:  StoredProcedure [ui].[sp_Filter_SEL]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ui].[sp_Filter_SEL]
(  
   @OBJNAME varchar(100)=null
)
AS
BEGIN
if @OBJNAME is null
  set @OBJNAME = 'AntennaType'


    select * ,
       cast( IIF(user_created = user_name(),0,1 ) as bit) as is_readonly
    
    	from ui.Filters 
    	where OBJNAME=@OBJNAME --and 
            --  (is_shared = 1 or user_created = user_name())
        



  /* Procedure body */
END

GO

/****** Object:  Table [history].[tables_for_history]    Script Date: 16.05.2020 10:54:58 ******/

  IF OBJECT_ID(N'[history].[history_log]') IS  NULL
  begin


CREATE TABLE [history].[history_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[action] [varchar](10) NULL,
	[table_name] [varchar](50) NULL,
	[record_id] [int] NULL,
	[project_id1111111] [int] NULL,
	[property_id111111111] [int] NULL,
	[user_created] [varchar](50) NULL DEFAULT (suser_name()),
	[date_created] [datetime] NULL DEFAULT (getdate()),
	[xml_deleted] [xml] NULL,
	[xml_inserted] [xml] NULL,
	[record_parent_id] [int] NULL,
	[record_parent_table_name] [varchar](50) NULL,
 CONSTRAINT [history__pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

/****** Object:  Index [history_log_idx]    Script Date: 16.05.2020 10:54:58 ******/
CREATE NONCLUSTERED INDEX [history_log_idx] ON [history].[history_log]
(
	[table_name] ASC,
	[record_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


end
GO
/****** Object:  Table [history].[tables_for_history]    Script Date: 16.05.2020 10:54:58 ******/

  IF OBJECT_ID(N'[history].[tables_for_history]') IS  NULL

  begin

 
CREATE TABLE [history].[tables_for_history](
	[table_name] [varchar](50) NOT NULL,
	[enabled] [bit] NULL DEFAULT ((1)),
	[parent_column_name] [varchar](50) NULL,
	[table_table_caption] [varchar](100) NULL,
	[use_project_id] [bit] NULL DEFAULT ((0)),
	[parent_id_sql] [varchar](50) NULL,
	[parent_table_name] [varchar](50) NULL,
 CONSTRAINT [table_name_pk] PRIMARY KEY CLUSTERED 
(
	[table_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

end

GO
/* Data for the 'history.tables_for_history' table  (Records 1 - 25) */


if  COL_LENGTH('history.tables_for_history','parent_id_sql')  is null
  ALTER TABLE history.tables_for_history
     ADD parent_id_sql varchar(50) 
GO

if  COL_LENGTH('history.tables_for_history','use_project_id')  is null
  ALTER TABLE history.tables_for_history
    ADD use_project_id bit DEFAULT 0 NULL
GO

---------------------------------------------------

if  COL_LENGTH('history.history_log','xml_deleted')  is null
  ALTER TABLE history.history_log
    ADD xml_deleted xml NULL
GO



if  COL_LENGTH('history.history_log','xml_inserted')  is null
  ALTER TABLE history.history_log
    ADD xml_inserted xml NULL
GO

if  COL_LENGTH('history.history_log','record_parent_id')  is null
  ALTER TABLE history.history_log
    ADD record_parent_id int NULL
GO
                                                              
if  COL_LENGTH('history.history_log','record_parent_table_name')  is null
  ALTER TABLE history.history_log
    ADD record_parent_table_name varchar(50) COLLATE Cyrillic_General_CI_AS NULL
GO



delete from history.tables_for_history 
go


INSERT INTO history.tables_for_history (table_name, enabled, parent_column_name, table_table_caption, use_project_id, parent_id_sql, parent_table_name)
VALUES 
  (N'AntennaType', 1, NULL, N'������ ������', 0, NULL, NULL),
  (N'CalcModel', 1, NULL, N'������ �������', NULL, NULL, NULL),
  (N'CalcModelParams', 1, NULL, N'������ ������� - ���������', NULL, NULL, NULL),
  (N'CellLayer', 1, NULL, NULL, 0, NULL, NULL),
  (N'ColorSchema', 1, NULL, N'�������� �����', NULL, NULL, NULL),
  (N'ColorSchemaRanges', 1, N'color_schema_id', NULL, NULL, NULL, N'ColorSchema'),
  (N'lib.Template_Link', 1, NULL, N'lib.Template_Link', 0, NULL, NULL),
  (N'link', 1, NULL, N'������������� �������� (���)', 1, NULL, NULL),
  (N'Link_Repeater', 1, NULL, N'������������', NULL, NULL, NULL),
  (N'Link_Repeater_Antenna', 1, N'Link_Repeater_id', N'������������ - �������', NULL, NULL, N'Link_Repeater'),
  (N'link_SDB', 1, NULL, N'��� SDB c ������� Super Dual Band', 1, NULL, NULL),
  (N'LinkEnd', 1, N'property_id', N'���', 1, NULL, N'property'),
  (N'Linkend_Antenna', 1, N'', N'�������', 1, N'[link].fn_LinkEnd_Antenna_Get_Link_ID (I.id)', N'dbo.Link'),
  (N'LinkEndType', 1, NULL, N'������������ ���', NULL, NULL, NULL),
  (N'LinkEndType_Band', 1, N'LinkEndType_id', N'������������ - ���������', NULL, NULL, N'LinkEndType'),
  (N'LinkEndType_Mode', 1, N'LinkEndType_id', N'������������ - ������', NULL, NULL, N'LinkEndType'),
  (N'LinkFreqPlan', 1, NULL, N'��������-��������������� ���� (���)', 1, NULL, NULL),
  (N'LinkLine', 1, NULL, N'������������� ����� (���)', 1, NULL, NULL),
  (N'LinkNet', 1, NULL, N'���� ������������� �����', 1, NULL, NULL),
  (N'pmp_site', 1, NULL, N'pmp_site', 0, NULL, NULL),
  (N'project', 1, NULL, N'������', NULL, NULL, NULL),
  (N'property', 1, NULL, N'��������', 1, NULL, NULL),
  (N'Template_Linkend', 1, NULL, N'Template_Linkend', 0, NULL, NULL),
  (N'TEMPLATE_LINKEND_ANTENNA', 1, NULL, N'TEMPLATE_LINKEND_ANTENNA', 0, NULL, NULL),
  (N'Template_pmp_site', 1, NULL, N'Template_pmp_site', 0, NULL, NULL)
GO




/****** Object:  View [history].[view_FK]    Script Date: 16.05.2020 10:54:58 ******/

  IF OBJECT_ID(N'[history].[view_FK]') IS  not  NULL
DROP VIEW [history].[view_FK]
GO
/****** Object:  View [history].[view_FK]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [history].[view_FK]
AS
SELECT 
    ccu.constraint_name AS SourceConstraint

   ,ccu.TABLE_SCHEMA AS Source_TABLE_SCHEMA
     ,ccu.table_name AS Source_Table_name
    ,ccu.column_name AS Source_Column
    
    ,kcu.TABLE_SCHEMA AS Target_Table_SCHEMA
    ,kcu.table_name AS Target_Table_name
    ,kcu.column_name AS Target_Column
    
FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu
    INNER JOIN INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
        ON ccu.CONSTRAINT_NAME = rc.CONSTRAINT_NAME 
    INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu 
        ON kcu.CONSTRAINT_NAME = rc.UNIQUE_CONSTRAINT_NAME

GO
/****** Object:  View [history].[view_columns]    Script Date: 16.05.2020 10:54:58 ******/

  IF OBJECT_ID(N'[history].[view_columns]') IS  not  NULL
DROP VIEW [history].[view_columns]
GO
/****** Object:  View [history].[view_columns]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [history].[view_columns]
AS

 select top 100 percent
 
col.*  , 


case
  when c.Column_name is not null then '(select name from '+
                                         FK.Target_Table_SCHEMA +'.'+ FK.Target_Table_name 
                                         +' m where m.id = '+ col.column_name +') as '+ FK.Target_Table_name + '_name'
  
  when col.data_type='float' then 'cast ('+ col.column_name+' as varchar(20)) as '+ col.column_name
  else col.column_name  
end    
 as xml_column_name,
 
 fk.Target_Table_SCHEMA,
 fk.Target_Table_name,
 fk.Target_Column,
 
 c.Column_name as fk_Column_name
  

 from  INFORMATION_SCHEMA.COLUMNS  col
    INNER JOIN sys.columns AS sc ON sc.object_id = object_id(col.table_schema + '.' + col.table_name) 
                                  AND sc.NAME = col.COLUMN_NAME
    inner join sys.objects obj on obj.object_Id=sc.object_Id 
  --  left join  sys.computed_columns cc on cc.name = col.COLUMN_NAME and cc.object_id = obj.object_Id

  left join history.view_FK FK on FK.Source_TABLE_SCHEMA=col.table_schema and 
                                  FK.Source_Table_name=col.table_name and 
                                  FK.Source_Column=col.column_name
                                  
  left join INFORMATION_SCHEMA.COLUMNS c on c.TABLE_SCHEMA=FK.Target_Table_SCHEMA and 
                                            c.Table_name=FK.Target_Table_name and                                        
                                            c.Column_name='name'
                                  
                                    

/*  ccu.constraint_name AS SourceConstraint
   ,ccu.TABLE_SCHEMA AS Source_TABLE_SCHEMA
   
     ,ccu.table_name AS Source_Table_name
    ,ccu.column_name AS Source_Column
    
    ,kcu.table_name AS Target_Table_name
    ,kcu.table_name AS Target_Table_SCHEMA
    ,kcu.column_name AS Target_Column
*/

where 
  obj.[type]='u'
  
  and col.DATA_TYPE<>'text'
  
--  and data_type='float'

  and col.COLUMN_NAME not like'[_][_]%'
  and col.COLUMN_NAME not like'%[_][_]'
  and col.COLUMN_NAME not like '[_]'
  and col.COLUMN_NAME not like'%[-]'
  and col.COLUMN_NAME not like'%[11]'

  and col.table_NAME not like'[_][_]%'
  and col.table_NAME not like'%[_][_]'
  and col.table_NAME not like'%[11]'
  


  and col.COLUMN_NAME not in
      (
--       'id', 
       'guid', 
       'date_modify',
       'date_created',
       'user_modify',
       'user_created'
--       'project_id'
      )


--  and cc.definition is null
  
  and sc.Is_computed<>1

order by 
  col.table_schema,
  col.table_name , 
  col.ordinal_position
  
  /*
  CREATE VIEW history.view_FK
AS
SELECT 
    ccu.constraint_name AS SourceConstraint
   ,ccu.TABLE_SCHEMA AS Source_TABLE_SCHEMA
   
     ,ccu.table_name AS Source_Table_name
    ,ccu.column_name AS Source_Column
    
    ,kcu.table_name AS Target_Table_name
    ,kcu.table_name AS Target_Table_SCHEMA
    ,kcu.column_name AS Target_Column
    
FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu
    INNER JOIN INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
        ON ccu.CONSTRAINT_NAME = rc.CONSTRAINT_NAME 
    INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu 
        ON kcu.CONSTRAINT_NAME = rc.UNIQUE_CONSTRAINT_NAME
*/

GO
/****** Object:  View [history].[view_COLUMNS_group]    Script Date: 16.05.2020 10:54:58 ******/

  IF OBJECT_ID(N'[history].[view_COLUMNS_group]') IS  not  NULL
DROP VIEW [history].[view_COLUMNS_group]
GO
/****** Object:  View [history].[view_COLUMNS_group]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [history].[view_COLUMNS_group]
AS
    
SELECT 
      
	 T.[TABLE_SCHEMA] +'.'+ T.TABLE_NAME as TABLE_NAME_full,

  SUBSTRING(
          (
              SELECT ','+  [xml_column_name] AS [text()]
                FROM view_columns C  
                WHERE 
                --     DATA_TYPE not in ('text') and 
                
                      C.TABLE_SCHEMA =  T.TABLE_SCHEMA and 
                      C.TABLE_NAME=  T.TABLE_NAME                       
              FOR XML PATH ('')
                ), 2, 10000000000) [sql_columns] 


  FROM  [INFORMATION_SCHEMA].[TABLES] T
  where TABLE_TYPE = 'BASE TABLE' and
  
        TABLE_NAME not in ('msfavorites') and 
        TABLE_NAME not like '%[11]' and
        TABLE_NAME not like '%[__]'

GO
/****** Object:  StoredProcedure [history].[sp_Clear_Log]    Script Date: 16.05.2020 10:54:58 ******/

  IF OBJECT_ID(N'[history].[sp_Clear_Log]') IS  not  NULL
DROP PROCEDURE [history].[sp_Clear_Log]
GO
/****** Object:  StoredProcedure [history].[sp_Clear_Log]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [history].[sp_Clear_Log]

AS
BEGIN

  DELETE FROM  history.history_log 


END

GO
/****** Object:  StoredProcedure [history].[sp_Create_triggers]    Script Date: 16.05.2020 10:54:58 ******/
  IF OBJECT_ID(N'[history].[sp_Create_triggers]') IS  not  NULL

DROP PROCEDURE [history].[sp_Create_triggers]
GO
/****** Object:  StoredProcedure [history].[sp_Create_triggers]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [history].[sp_Create_triggers]
(
  @TABLE_NAME_full VARCHAR(50)=null
  
)
AS
BEGIN


 if @TABLE_NAME_full is null 
--    set   @TABLE_NAME  = 'property'
--    set   @TABLE_NAME_full  = 'AntennaType'
--    set   @TABLE_NAME_full  = 'LinkEnd'
    set   @TABLE_NAME_full  = 'lib.Template_Link'
    
--    set   @TABLE_NAME_full  = 'test.AntennaType'
    
--    set   @TABLE_NAME  = 'AntennaType'

  if charindex('.',@TABLE_NAME_full)=0
    set @TABLE_NAME_full = 'dbo.'+@TABLE_NAME_full
 

  DECLARE 
    @parent_id_sql VARCHAR(max),
    @parent_table_name VARCHAR(max),
    
    @sql_insert_parent VARCHAR(max)='',
    @sql_select_parent VARCHAR(max)='',
    
    
    @sql_columns  VARCHAR(max),
    @sql VARCHAR(max), 
    @TriggerName VARCHAR(50) = @TABLE_NAME_full + '_Audit_tr'


  if OBJECT_ID(@TriggerName) IS NOT NULL  
    exec('DROP TRIGGER '+ @TriggerName)

  ---------------------------------------    
  -- create history table
  ---------------------------------------    
--print @TABLE_NAME_full

  select 
         @sql_columns = sql_columns
    FROM history.view_COLUMNS_group      
    where TABLE_NAME_full = @TABLE_NAME_full


select @parent_id_sql     = IsNull(parent_id_sql,''),
       @parent_table_name = IsNull(parent_table_name,'')
       
  from history.tables_for_history
  where table_name = @TABLE_NAME_full or  
        'dbo.' + table_name = @TABLE_NAME_full


IF @parent_id_sql <> ''
begin
  set @sql_insert_parent = ',record_parent_id, record_parent_table_name'  
  set @sql_select_parent = ', '+ @parent_id_sql + ', '''+ @parent_table_name +'''
  '

end


-------------------------------------------------
-- FOR INSERT
-------------------------------------------------
set @sql=
  'CREATE TRIGGER :TriggerName ON :table_name 
    FOR UPDATE,INSERT,DELETE AS  
    begin
      set nocount on

      SET ANSI_WARNINGS  ON  --| OFF }
  
      declare
        @xml_inserted XML,
        @xml_deleted XML
                       
        declare  @tbl_inserted TABLE (xml_data XML, id int, RecNo int)
        declare  @tbl_deleted  TABLE (xml_data XML, id int, RecNo int)

              set @xml_inserted = ( SELECT :columns
                                       from inserted ----  as Record     
                                       for xml  path (''Record''),  elements    
                                     )

              set @xml_deleted = ( SELECT :columns
                                       from deleted ----  as Record     
                                       for xml  path (''Record''),  elements     
                                     )
                                       
        insert into @tbl_inserted
           select t.c.query (''.'') as xml_data,
                  t.c.value(''(id/text())[1]'',''int'') as ID,  
                  row_number () over (order by (select 100) ) as RowNum
              from @xml_inserted.nodes(''*'') t(c)  
                                       
        insert into @tbl_deleted
           select t.c.query (''.'') as xml_data,
                  t.c.value(''(id/text())[1]'',''int'') as ID,  
                  row_number () over (order by (select 100) ) as RowNum
              from @xml_deleted.nodes(''*'') t(c)  

             
  
----------------------------------------
-- Update
----------------------------------------        
  IF EXISTS (select * from inserted) AND EXISTS (select * from deleted)
  BEGIN
    INSERT history.history_log (action, table_name,  record_id,  xml_inserted, xml_deleted :insert_parent )
      SELECT ''Update'','':table_name'',  I.id, I.xml_data, D.xml_data  :select_parent
       from @tbl_inserted I
          left join @tbl_deleted D on I.id=D.id         

 
    RETURN;
  END

----------------------------------------
--  delete
----------------------------------------
  IF EXISTS (select * from deleted) AND NOT EXISTS (select * from inserted)
  BEGIN
    INSERT history.history_log (action, table_name, record_id, xml_deleted :insert_parent )
      SELECT ''delete'','':table_name'',  id, xml_data    :select_parent 
        from @tbl_deleted I 
                
    RETURN;
  END

----------------------------------------
-- Update inserts
----------------------------------------
  IF EXISTS (select * from inserted) AND not EXISTS (select * from deleted)
  BEGIN
    INSERT history.history_log (action, table_name ,  record_id, xml_inserted  :insert_parent )
      SELECT ''insert'','':table_name'',  id, xml_data  :select_parent
           from @tbl_inserted  I
 

    RETURN;
  END
        
         
end ' 
    
      
   set @sql = Replace(@sql, ':columns',          @sql_columns)
--  set @sql = Replace(@sql, ':record_parent_id', @parent_column_name)
  set @sql = Replace(@sql, ':table_name',  	    @TABLE_NAME_full)
  set @sql = Replace(@sql, ':TriggerName',      @TriggerName)

  set @sql = Replace(@sql, ':insert_parent',    @sql_insert_parent)
  set @sql = Replace(@sql, ':select_parent',    @sql_select_parent)

/*  set @sql_insert_parent = ',record_parent_id, record_parent_table_name'  
  set @sql_select_parent = ', '+ @parent_id_sql + ', '+ @parent_table_name 
*/


--select  @sql


  execute  ( @sql)


END


   
    /*
    IF EXISTS (select * from inserted) AND NOT EXISTS (select * from deleted)
  BEGIN
    INSERT [AUDIT].[LOG_TABLE_CHANGES] ([CHG_TYPE], [SCHEMA_NAME], [OBJECT_NAME], [XML_RECSET])
    SELECT 'INSERT', '[ACTIVE]', '[CARS_BY_COUNTRY]', (SELECT * FROM inserted as Record for xml auto, elements , root('RecordSet'), type)
    RETURN;
  END

  -- Detect deletes
  IF EXISTS (select * from deleted) AND NOT EXISTS (select * from inserted)
  BEGIN
    INSERT [AUDIT].[LOG_TABLE_CHANGES] ([CHG_TYPE], [SCHEMA_NAME], [OBJECT_NAME], [XML_RECSET])
    SELECT 'DELETE', '[ACTIVE]', '[CARS_BY_COUNTRY]', (SELECT * FROM deleted as Record for xml auto, elements , root('RecordSet'), type)
    RETURN;
  END

  -- Update inserts
  IF EXISTS (select * from inserted) AND EXISTS (select * from deleted)
  BEGIN
    INSERT [AUDIT].[LOG_TABLE_CHANGES] ([CHG_TYPE], [SCHEMA_NAME], [OBJECT_NAME], [XML_RECSET])
    SELECT 'UPDATE', '[ACTIVE]', '[CARS_BY_COUNTRY]', (SELECT * FROM deleted as Record for xml auto, elements , root('RecordSet'), type)
    RETURN;
  END
  
----------------------------------------
--  -- Detect deletes
----------------------------------------
--  IF EXISTS (select * from deleted) AND NOT EXISTS (select * from inserted)
--  BEGIN
--    INSERT [AUDIT].[LOG_TABLE_CHANGES] ([CHG_TYPE], [SCHEMA_NAME], [OBJECT_NAME], [XML_RECSET])
--    SELECT  (SELECT * FROM deleted as Record for xml auto, elements , root(''RecordSet''), type)
--    RETURN;
--  END

----------------------------------------
-- Update inserts
----------------------------------------
--  IF EXISTS (select * from inserted) AND EXISTS (select * from deleted)
--  BEGIN
--    INSERT [AUDIT].[LOG_TABLE_CHANGES] ([CHG_TYPE], [SCHEMA_NAME], [OBJECT_NAME], [XML_RECSET])
--    SELECT  (SELECT * FROM deleted as Record for xml auto, elements , root(''RecordSet''), type)
--    RETURN;
--  END


 
      declare
        @xml_inserted XML,
        @xml_deleted XML
             
declare  @tbl_inserted TABLE (xml_data XML, id int, RecNo int)
declare  @tbl_deleted  TABLE (xml_data XML, id int, RecNo int)

      set @xml_inserted = ( SELECT id,folder_id,name 
                               from inserted ----  as Record     
                               for xml  path ('Record'),  elements -- raw --,  elements -- auto, elements , root('RecordSet')     
                             )

      set @xml_deleted = ( SELECT id,folder_id,name 
                               from deleted ----  as Record     
                               for xml  path ('Record'),  elements -- raw --,  elements -- auto, elements , root('RecordSet')     
                             )
                             
insert into @tbl_inserted
   select t.c.query ('.') as xml_data,
          t.c.value('(id/text())[1]','int') as ID,  
          row_number () over (order by (select 100) ) as RowNum
      from @xml_inserted.nodes('*') t(c)  
                             
insert into @tbl_deleted
   select t.c.query ('.') as xml_data,
          t.c.value('(id/text())[1]','int') as ID,  
          row_number () over (order by (select 100) ) as RowNum
      from @xml_deleted.nodes('*') t(c)  

-----------
  
    INSERT history.history_log (action, table_name, record_id, xml_inserted, xml_deleted  )
      SELECT 'Update','test.AntennaType', I.id, I.xml_data, D.xml_data
        from @tbl_inserted I
          left join @tbl_deleted D on I.id=D.id        
  
  
*/

GO
/****** Object:  StoredProcedure [history].[sp_Create_triggers_all]    Script Date: 16.05.2020 10:54:58 ******/

 IF OBJECT_ID(N'[history].[sp_Create_triggers_all]') IS  not  NULL

DROP PROCEDURE [history].[sp_Create_triggers_all]
GO
/****** Object:  StoredProcedure [history].[sp_Create_triggers_all]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [history].[sp_Create_triggers_all] 
AS 
BEGIN

  DECLARE  
    @TableName VARCHAR(100);


exec history.sp_Drop_triggers_all 


SELECT * 
     into #temp 
  FROM history.tables_for_history
  WHERE enabled=1	



  while exists(SELECT * FROM #temp)
  BEGIN
    print  @TableName

    SELECT top 1 @TableName=Table_Name FROM #temp;  
    delete top (1) FROM #temp 
     

    exec history.sp_create_triggers 	@TABLE_NAME_FULL=@TableName

  END	

END

GO
/****** Object:  StoredProcedure [history].[sp_Drop_triggers_all]    Script Date: 16.05.2020 10:54:58 ******/
  IF OBJECT_ID(N'[history].[sp_Drop_triggers_all]') IS  not  NULL

DROP PROCEDURE [history].[sp_Drop_triggers_all]
GO
/****** Object:  StoredProcedure [history].[sp_Drop_triggers_all]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [history].[sp_Drop_triggers_all] 
AS 
BEGIN

  DECLARE  
    @Name VARCHAR(500);


select 
    schema_Name(schema_id) + '.' +name as name
    
 into #temp    
  
from 
  sys.objects
    where 
      type = 'TR' and name like '%_Audit_tr'



-----------------------------------


  while exists(SELECT * FROM #temp)
  BEGIN
 
    SELECT top 1 @name =[name]  FROM #temp;  
    delete top (1) FROM #temp 
     
    exec ('drop trigger '+ @name)

  END	   


END

GO
/****** Object:  UserDefinedFunction [history].[ft_Record_change_diff]    Script Date: 16.05.2020 10:54:58 ******/

 IF OBJECT_ID(N'[history].[ft_Record_change_diff]') IS  not  NULL

DROP FUNCTION [history].[ft_Record_change_diff]
GO
/****** Object:  UserDefinedFunction [history].[ft_Record_change_diff]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [history].[ft_Record_change_diff] 
(
 
  @xml_1 XML, --= '<RecordSet><Record><id>10504</id><folder_id>3785</folder_id><name>xgvxfgbcvbc</name>88<gain_type>99</gain_type><tilt_type>6</tilt_type>661.2<omni>0</omni>1.21.26840<polarization_str>h</polarization_str><band>15</band>0<comment/><band_id>7</band_id></Record></RecordSet>',
  @xml_2 XML --= '<RecordSet><Record><id>10504</id><folder_id>3785</folder_id><name>1.2-HSX4-142(H)������</name>42.6<gain_type>0</gain_type><tilt_type>0</tilt_type>143751.2<omni>0</omni>1.21.26840<polarization_str>h</polarization_str><band>15</band>0<comment/><band_id>7</band_id></Record></RecordSet>';

)

RETURNS @res TABLE (
 
  column_name varchar(100),
  old_value  varchar(max),
  new_value  varchar(max)

)
AS
BEGIN

if @xml_1 is null  and  @xml_2 is null
begin
  select top 1
         @xml_1 = xml_deleted, 
         @xml_2 = xml_inserted
     from history.history_log order by id desc
--     from history.history_log where id=517148




 -- set  @xml_1 = '<Record><id>10504</id><folder_id>3785</folder_id><name>xgvxfgbcvbc</name>88<gain_type>99</gain_type><tilt_type>6</tilt_type>661.2<omni>0</omni>1.21.26840<polarization_str>h</polarization_str><band>15</band>0<comment/><band_id>7</band_id></Record>'
 -- set  @xml_2 = '<Record><id>833</id><name>WiMIC (������)</name><folder_id>5303</folder_id><band>3</band><power_max>24</power_max><freq_min>3400</freq_min><freq_max>3550</freq_max><failure_period>10000</failure_period><equaliser_profit>0</equaliser_profit><freq_channel_count>1</freq_channel_count><kratnostByFreq>0</kratnostByFreq><kratnostBySpace>0</kratnostBySpace><level_tx_a>-50</level_tx_a><level_rx_a>-50</level_rx_a><comment>jjjjjjjjjjjjjj</comment></Record>';
end
 

declare @tbl_src TABLE ( 
    column_name varchar(100),
    column_value  varchar(max) )

declare   @tbl_dest TABLE ( 
    column_name varchar(100),
    column_value  varchar(max) )
      

insert into @tbl_src
select  
    C.value('local-name(.)', 'varchar(50)') as column_name,
    C.value('text()[1]', 'varchar(max)') as column_value
 
FROM @xml_1.nodes('/Record/*') AS T(C)

------------------------------------------------
insert into @tbl_dest
select  
    C.value('local-name(.)', 'varchar(50)') as column_name,
    C.value('text()[1]', 'varchar(max)') as column_value

FROM @xml_2.nodes('/Record/*') AS T(C)

------------------------------------------------
 
 insert into @res
    select  
       D.column_name,
       S.column_value as value_old,
       D.column_value as value_new

    FROM  @tbl_dest D
      left outer join @tbl_src S on S.column_name=D.column_name

    where
      IsNull(S.column_value,'') <> IsNull(D.column_value,'')
   
          

  return
 
END

GO
/****** Object:  StoredProcedure [history].[sp_Record_History_SEL]    Script Date: 16.05.2020 10:54:58 ******/
  IF OBJECT_ID(N'[history].[sp_Record_History_SEL]') IS  not  NULL

DROP PROCEDURE [history].[sp_Record_History_SEL]
GO
/****** Object:  StoredProcedure [history].[sp_Record_History_SEL]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [history].[sp_Record_History_SEL]
(
  @table_name varchar(50) = null,
  @id int = null
)
AS
BEGIN
/*  if @table_name is null
  begin
    set @table_name = 'Property'
    set @id = 6836 
  end
*/

  if @table_name is null
  begin
 /*   set @table_name = 'AntennaType'
--    set @table_name = 'test.AntennaType'
    set @id = 10490 
*/

    set @table_name = 'LinkEndType'
--    set 958 = 'test.AntennaType'
    set @id = 958
        
  end

  if charindex('.',@table_name)=0
    set @table_name = 'dbo.' + @table_name
  


 select
  --  id as tree_id,

    ROW_NUMBER() OVER(order by (select null))  AS Row_Num,
--    ROW_NUMBER() OVER(order by (select 100)) as varchar(10)) AS Row_Num,
 		cast(null as int) as parent_id,
 
        *, 
            
       case 
           when action='UPDATE' then '���������'
           when action='INSERT' then '�������'
           when action='DELETE' then '��������'
        end as action_str,        
        
        cast(null as varchar(100)) column_name,
        cast(null as varchar(max)) old_value,
        cast(null as varchar(max)) new_value
        
        
     into #temp 
     
  from history.history_log
  where 
    (table_name  = @table_name and record_id = @id)
    or
    (record_parent_table_name = @table_name  and record_parent_id=@id)


------------------------------------------------


/*
select * into #temp_  
  from #temp 
 
declare 
  @xml_inserted  XML,
  @xml_deleted  XML

while Exists ( SELECT * FROM #temp_)
begin
  select top 1 @xml_inserted=xml_inserted, @xml_deleted=xml_deleted
    from #temp_*/

/*
print  cast(@xml_deleted  as varchar(max)) 
print '--------------------------'

print  cast(@xml_inserted as varchar(max))
print '--------------------------'



 select  h.new_value, h.old_value
      from 
         history.ft_Record_change_diff (  @xml_deleted, @xml_inserted )  h --  ,
*/
  ------------------------------------------

/*  insert into #temp ( new_value, old_value)
    select  h.new_value, h.old_value
      from 
         history.ft_Record_change_diff (@xml_deleted, @xml_inserted )  h --  ,
*/


/*    
  delete top (1) from #temp_
        
end*/
 

insert into #temp (parent_id, column_name, new_value, old_value, xml_inserted, xml_deleted)
  select l.id, h.column_name, h.new_value, h.old_value,
          l.xml_inserted, l.xml_deleted 
--  select l.* , h.column_name, h.new_value, h.old_value
    from 
      #temp L
--      history.history_log  L
       outer apply    
         history.ft_Record_change_diff (  l.xml_deleted, l.xml_inserted )  h   
               
     where action='UPDATE'
 
  
  select * from #temp
    
  
END

GO
/****** Object:  View [dbo].[view__object_fields_enabled]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[view__object_fields_enabled]') IS  not  NULL

DROP VIEW      [dbo].[view__object_fields_enabled]
GO
/****** Object:  View [dbo].[view__object_fields_enabled]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view__object_fields_enabled]
AS
SELECT TOP 100000 
       dbo._objects.name AS objname,
       dbo._objects.table_name,
       dbo._objects.table_name_initial,
       
       dbo._object_fields.*,   
       
       dbo._object_fields.type AS field_type,
       dbo._object_fields.name AS field_name
FROM dbo._object_fields
     LEFT OUTER JOIN dbo._objects ON dbo._object_fields.object_id =
     dbo._objects.id
     
WHERE (dbo._object_fields.enabled = 1)

ORDER BY dbo._objects.name,
         dbo._object_fields.parent_id,
         dbo._object_fields.index_

GO
/****** Object:  UserDefinedTableType [dbo].[TVarChar_table]    Script Date: 16.05.2020 10:54:58 ******/
/****** Object:  UserDefinedTableType [dbo].[TVarChar_table]    Script Date: 16.05.2020 10:54:58 ******/

if TYPE_ID('TVarChar_table') is null
CREATE TYPE [dbo].[TVarChar_table] AS TABLE(
	[value] [varchar](100) NULL
)
GO
/****** Object:  UserDefinedFunction [dbo].[ft_VARCHAR_String_to_Table]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[ft_VARCHAR_String_to_Table]') IS  not  NULL


DROP FUNCTION [dbo].[ft_VARCHAR_String_to_Table]
GO
/****** Object:  UserDefinedFunction [dbo].[ft_VARCHAR_String_to_Table]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ft_VARCHAR_String_to_Table]
(
  @input     AS Varchar(max) ,
  @DELIMITER as Varchar(1)=','
)

RETURNS
   @Result TABLE(Value varchar(50))

AS

BEGIN
/*
0. �����������������
1. Rec.567 (Okumura-Hata)
2. Cost. 231
3. Dego 
4. Ksj-Bertoni
5. Metod B
6. Indoor
7. SOFDMA

*/


--  SET @input = replace(@input,char(10),'')
  SET @input = replace(@input,char(13),'')


  DECLARE @str VARCHAR(50)

  DECLARE @ind Int

  IF(@input is not null)

  BEGIN

        SET @ind = CharIndex(@DELIMITER, @input)

        WHILE @ind > 0

        BEGIN

              SET @str = SUBSTRING(@input,1,@ind-1)

              SET @input = SUBSTRING(@input,@ind+1,LEN(@input)-@ind)

              INSERT INTO @Result values (RTRIM(LTRIM( @str)))

              SET @ind = CharIndex(@DELIMITER, @input)

        END

        SET @str = @input

if RTRIM(LTRIM(@str))<>''
        INSERT INTO @Result values (RTRIM(LTRIM(@str)))

  END

  RETURN

END

GO


IF OBJECT_ID(N'[map].[user_map_settings]') IS   NULL


CREATE TABLE [map].[user_map_settings](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_name] [varchar](50) NOT NULL DEFAULT (suser_name()),
	[host_name] [varchar](50) NOT NULL CONSTRAINT [DF__user_map___host___4DFF6746]  DEFAULT (host_name()),
	[objname] [varchar](50) NOT NULL,
	[label_props] [xml] NULL,
	[attribute_column_names] [varchar](max) NULL,
	[style_xml] [xml] NULL,
	[caption_sql] [varchar](max) NULL,
 CONSTRAINT [user_map_settings_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [user_map_settings_uq] UNIQUE NONCLUSTERED 
(
	[user_name] ASC,
	[host_name] ASC,
	[objname] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]



GO
/****** Object:  StoredProcedure [map].[sp_Map_engine_layer_attributes_SEL]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[map].[sp_Map_engine_layer_attributes_SEL]') IS  not  NULL

DROP PROCEDURE [map].[sp_Map_engine_layer_attributes_SEL]
GO
/****** Object:  StoredProcedure [map].[sp_Map_engine_layer_attributes_SEL]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [map].[sp_Map_engine_layer_attributes_SEL]
(
  @OBJNAME  varchar(50)=null
)
AS
BEGIN
  if @OBJNAME is null
    set @OBJNAME = 'link'
    
    
--ui.user_map_settings


/*select attribute_column_names 
         from  map.user_map_settings
         where 
            user_name = user_name() and
            host_name = host_name() and
             OBJNAME = @OBJNAME 
*/

declare 
  @names varchar(max) =
     (select attribute_column_names 
         from  map.user_map_settings
         where 
            user_name = suser_name() and
            host_name = host_name() and
             OBJNAME = @OBJNAME 
      )
      
      
--prind      
      
      

declare 
  @tbl TVarChar_table

insert into @tbl
  select * from dbo.ft_VARCHAR_String_to_Table (@names, ',')

--select * from @tbl

-------------------


select 
  --  id, parent_id, name, caption,
  
   
    cast( IIF( name in (select * from  @tbl ), 1,0) as bit)  as checked,
    
    IIF( IsNull(Is_folder,0)=0,  ''''+ caption +':'' + IIF('+ name +' is null, '''', cast('+ name +' as varchar(max)) )'  , null) as [caption_sql],
    IIF( IsNull(Is_folder,0)=0,  'IIF('+ name +' is null, '''', cast('+ name +' as varchar(max)) )'  , null) as [caption_small_sql],
  
  --  cast(0 as bit) as checked,
    
     *

    into #temp

  from  dbo.view__object_fields_enabled  
  where is_in_view=1 and
        OBJNAME=@OBJNAME

------------------
select * from #temp
 
END


/*
 if @ID is null
    set @ID = 1111


declare 
  @names varchar(max) =
     (select top 1 attribute_column_names from  ui.user_map_settings)

declare 
  @tbl TVarChar_table

insert into @tbl
  select * from dbo.ft_VARCHAR_String_to_Table (@names)

select * from @tbl

--ft_VARCHAR_String_to_Table


select * from ui.user_map_settings
*/

GO
/****** Object:  StoredProcedure [map].[sp_Map_engine_layer_attributes_UPD]    Script Date: 16.05.2020 10:54:58 ******/

  IF OBJECT_ID(N'[map].[sp_Map_engine_layer_attributes_UPD]') IS  not  NULL

DROP PROCEDURE [map].[sp_Map_engine_layer_attributes_UPD]
GO
/****** Object:  StoredProcedure [map].[sp_Map_engine_layer_attributes_UPD]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [map].[sp_Map_engine_layer_attributes_UPD]
(
  @OBJNAME varchar(50)  ,
  @NAMES   varchar(max) ,
  
  @CAPTION_SQL  varchar(max) = null
  
)
AS
BEGIN
  if @OBJNAME is null
  begin
    set @OBJNAME = 'link'
    
    set @names = 'name'
    
 end


set @CAPTION_SQL = replace (@CAPTION_SQL , '"', '')
set @CAPTION_SQL = replace (@CAPTION_SQL , ';', '+ Char(10)+')

--set @CAPTION_SQL = 'name'  --replace (@NAMES , ',', '+ Char(10) +')



/*   s:=s.Replace('"','');
   s:=s.Replace(Chr(10),'+'+Chr(10)+'+');

*/



if not exists (select * from  map.user_map_settings
                   where 
                      user_name = suser_name() and  
                      host_name = host_name() and
                     OBJNAME = @OBJNAME  
                )
   insert into map.user_map_settings (OBJNAME, attribute_column_names) 
                             values( @OBJNAME, @names )      

ELSE
  update map.user_map_settings
  set 
    attribute_column_names = @names,
    CAPTION_SQL = @CAPTION_SQL
        
  where
     OBJNAME = @OBJNAME and
     user_name = suser_name() and  
     host_name = host_name()   
 

 
  return @@ROWCOUNT
  

 
END

GO
/****** Object:  StoredProcedure [map].[sp_MapMaker]    Script Date: 16.05.2020 10:54:58 ******/

/****** Object:  UserDefinedFunction [dbo].[fn_Linkend_Antenna_info]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[fn_Linkend_Antenna_info]') IS  not  NULL

DROP FUNCTION [dbo].[fn_Linkend_Antenna_info]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Linkend_Antenna_info]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:   
---------------------------------------------------------------

CREATE FUNCTION [dbo].[fn_Linkend_Antenna_info] 

 */


CREATE FUNCTION [dbo].[fn_Linkend_Antenna_info] 
(
  @LINKEND_ID int   
 
)

--346447
--RETURNS varchar(50)

RETURNS @ret TABLE 
(
    -- Columns returned by the function
    
    azimuth 	varchar(50),
    vert_width 	varchar(50),
    horz_width 	varchar(50),    
    gain 		varchar(50),
    polarization 	varchar(50),
    diameter 		varchar(50), 
    ground_height 	varchar(50),
    height 		varchar(50),
    tilt 		varchar(50),
    loss 		varchar(50),

    loss_full 	float,
    
    height_full float
    
   
)

as

BEGIN

  declare 
  	@result varchar(50);
   


  DECLARE   
    @azimuth_str 		varchar(50),
    @vert_width_str 	varchar(50),
    @horz_width_str 	varchar(50),    
    @gain_str 			varchar(50),
    @polarization_str 	varchar(50),
    @diameter_str 		varchar(50), 
    @ground_height_str 	varchar(50),
    @height_str 		varchar(50),
    @tilt_str 			varchar(50),
    @loss_str 			varchar(50),
    
    @loss_full	float =0, 
    @height_full		float    ;

/*
  set @diameter_str='';
  set @height_str='';
  set @polarization_str='';
  set @gain_str='';
  set @vert_width_str ='';
  set @horz_width_str ='';
  set @ground_height_str='';
  set @azimuth_str='';
  set @loss_str='';
  set @tilt_str='';
  
  */
  
  
  select 


      @azimuth_str 		= IsNull(@azimuth_str       + '/', '')+  CAST(azimuth as varchar(50)) , 
      @vert_width_str 	= IsNull(@vert_width_str  	+ '/', '')+  CAST(vert_width as varchar(50)), 
      @horz_width_str 	= IsNull(@horz_width_str  	+ '/', '')+  CAST(horz_width as varchar(50)), 
  	  @polarization_str = IsNull(@polarization_str  + '/', '')+  polarization_str, 
  	  @diameter_str 	= IsNull(@diameter_str  	+ '/', '')+  CAST(diameter as varchar(50)), 
   	  @gain_str     	= IsNull(@gain_str      	+ '/', '')+  CAST(gain as varchar(50)),

   	  @loss_str     	= IsNull(@loss_str      	+ '/', '')+  CAST(Isnull(loss,0) as varchar(50)),
   	  @tilt_str     	= IsNull(@tilt_str      	+ '/', '')+  CAST(tilt as varchar(50)),

  	  @height_str 		= IsNull(@height_str  		+ '/', '')+  CAST(A.height as varchar(50)),
      @ground_height_str= IsNull(@ground_height_str + '/', '')+  CAST(ground_height + A.height as varchar(50)), 

      @loss_full = @loss_full + isnull(A.loss,0), 
      @height_full = isnull(ground_height,0) + isnull(A.height,0) 
    
  
  	from Linkend_Antenna A  
          LEFT OUTER JOIN Property   ON A.property_id = Property.id
    WHERE Linkend_ID=@LINKEND_ID
    ORDER BY A.height
    
    
    INSERT INTO @ret (
      azimuth,
      vert_width,
      horz_width,
      gain,
      polarization ,
      diameter,
      ground_height,
      height,
      tilt,
      loss,
      
      loss_full,
      height_full
    ) 
    
    VALUES (
      @azimuth_str,
      @vert_width_str,
      @horz_width_str,
      @gain_str,
      @polarization_str,
      @diameter_str,
      @ground_height_str,
      @height_str,
      @tilt_str,
      @loss_str,
      
      @loss_full,
      @height_full
    )
    
    
   
    
    

/*
SELECT     dbo.Linkend_Antenna.*, ground_height 
FROM         dbo.Linkend_Antenna LEFT OUTER JOIN
                      dbo.Property ON dbo.Linkend_Antenna.property_id = dbo.Property.id
                      
          */            
   /*  
	set @result='param not defined: '+ @PARAM
     
  IF @PARAM='loss'
    set @result= SUBSTRING(@loss_str, 2, 100)    

  IF @PARAM='tilt'
    set @result= SUBSTRING(@tilt_str, 2, 100)  

  IF @PARAM='azimuth'
    set @result= SUBSTRING(@azimuth_str, 2, 100)  
            
  IF @PARAM='vert_width'
    set @result= SUBSTRING(@vert_width_str, 2, 100)  

  IF @PARAM='horz_width'
    set @result= SUBSTRING(@horz_width_str, 2, 100)  

  IF @PARAM='gain'
    set @result= SU99999BSTRING(@gain_str, 2, 100)  

  IF @PARAM='height'
    set @result= SUBSTRING(@height_str, 2, 100)  
  
  IF @PARAM='diameter'
    set @result= SUBSTRING(@diameter_str, 2, 100)  
  
  IF @PARAM='ground_height'
    set @result= SUBSTRING(@ground_height_str, 2, 100)  

  IF @PARAM='polarization'
    set @result= SUBSTRING(@polarization_str, 2, 100) 

 -- RETURN 'param not defined -?????????????'
*/
	RETURN
  
END

GO


/****** Object:  StoredProcedure [dbo].[sp_Link_Add]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[sp_Link_Add]') IS  not  NULL


DROP PROCEDURE [dbo].[sp_Link_Add]
GO
/****** Object:  StoredProcedure [dbo].[sp_Link_Add]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Link_Add]
(
    @PROJECT_ID int,
    
    @OBJNAME varchar(50) ,
    
    -------------------------------------
    -- TEMPLATE_SITE_ID
    -------------------------------------
    @TEMPLATE_LINK_ID int = null,    
--    @TEMPLATE_SITE_ID int = null,    
    @PROPERTY1_ID int = null,
    @PROPERTY2_ID int = null,
    
    @NAME varchar(250),
    
    @FOLDER_ID int = null,
    @CLUTTER_MODEL_ID int = null,
    
    @LINKEND1_ID int ,
    @LINKEND2_ID int ,
        
--    @PMP_SECTOR_ID int = null,    
--    @PMP_TERMINAL_ID int = null,
    
    
    @Comments varchar(250) = null,
    @Status_id int = null,
    
  --  @NAME_SDB varchar(20) = null,
--    @NAME_TN varchar(20) = null,
    
    @SESR float = null,
    @KNG float = null,
    @CALC_METHOD int = 0      --  null        --  @State_id   			int =null, -- ������: ��������, �����������...   --  @IS_GET_INFO 			bit = 0
)    
AS
begin

  SET ANSI_WARNINGS OFF --String or binary data would be truncated.



  IF @FOLDER_ID=0 SET @FOLDER_ID=NULL;
  --if @LINK_REPEATER_ID=0 set @LINK_REPEATER_ID=null


  DECLARE 
--    @len_m float, 
  --  @sName VARCHAR(100),
    @lat1 float,
    @lon1 float,
    @lat2 float,
    @lon2 float,

    @AZIMUTH1 float,
    @AZIMUTH2 float,
     
--    @property1_id INT,
--    @property2_id INT,
    
    @id INT,
    @new_id INT;

  --  @project_id  int,  

 --   @project1_id  int,  
  --  @project2_id  int;

  --  @project_id  int,  
 --   @objname  VARCHAR(10);
   
 
 --    @aLat float,
  --   @aLon float
     
  --258929
  
    -- Name     : ����������� � ���� � ��������� before insert
  -- Address  : ����������� � ���� � ��������� before insert
  
   
  
  IF @Name       IS NULL  RAISERROR ('@Name is NULL', 16, 1);
--  IF @LENGTH_M=0  RAISERROR ('@LENGTH_M=0', 16, 1);
  
--   set @sName = [dbo].fn_Link_get_new_name (@PROJECT_ID, @Name)


--  set @LENGTH_M = round(@LENGTH_M,1)



  
 
--------------------------
-- check if name exists
-------------------------- 
  SELECT top 1 @id=id 
  	FROM link 
    WHERE (name=@NAME) and (PROJECT_ID=@PROJECT_ID)
           and ((@Folder_id IS NULL) or (Folder_id=@Folder_id))  

  IF @id is not NULL
  BEGIN
    SELECT 'record already exists' as msg,  @id as ID     
    RETURN -@id;  
  END  







  IF NOT EXISTS(SELECT * FROM Status WHERE (id=@status_id))
  BEGIN 
    set @status_id=NULL
  END
/* 

  IF NOT EXISTS(SELECT * FROM lib_Link_State WHERE (id=@state_id))
  BEGIN 
    set @state_id=NULL
  END
*/


  IF NOT EXISTS (SELECT * FROM ClutterModel WHERE (ID=@CLUTTER_MODEL_ID))
  BEGIN 
    SELECT top 1 @CLUTTER_MODEL_ID=id FROM ClutterModel 
    
    if @CLUTTER_MODEL_ID is NULL  RAISERROR ('@CLUTTER_MODEL_ID is NULL', 16, 1);
    
   -- PRINT 'SELECT * FROM ClutterModel WHERE (ID=@CLUTTER_MODEL_ID)'
     
  END
  
  

  

  -----------------------------------------  
--  IF (@LINKEND1_ID IS NOT NULL ) and (@LINKEND2_ID IS NOT NULL )
  -----------------------------------------
--  BEGIN
 /*   IF NOT EXISTS(SELECT * FROM LinkEND WHERE ID=@LINKEND1_ID)
    BEGIN
      RAISERROR ('@LINKEND1_ID not exists', 16, 1);
      return -1      
    END      

    IF NOT EXISTS(SELECT * FROM LinkEND WHERE ID=@LINKEND2_ID)
    BEGIN
      RAISERROR ('@LINKEND2_ID not exists', 16, 1);
      return -2
    END      
*/
 /*
    IF EXISTS(SELECT * FROM Link WHERE (LINKEND1_ID in (@LINKEND1_ID,@LINKEND2_ID)) or 
                                       (LINKEND2_ID in (@LINKEND1_ID,@LINKEND2_ID)))
    BEGIN
      RAISERROR (' IF EXISTS(SELECT * FROM Link WHERE (LINKEND1_ID in (@LINKEND1_ID,@LINKEND2_ID)) or (LINKEND2_ID in (@LINKEND1_ID,@LINKEND2_ID)))', 16, 1);

      SELECT * FROM Link WHERE (LINKEND1_ID in (@LINKEND1_ID,@LINKEND2_ID)) or 
                               (LINKEND2_ID in (@LINKEND1_ID,@LINKEND2_ID))


      return -3
    END      
 
*/

   -- IF EXISTS(SELECT * FROM Link WHERE (LINKEND1_ID=@LINKEND1_ID) and (LINKEND2_ID=@LINKEND2_ID)) or 
  --     EXISTS(SELECT * FROM Link WHERE (LINKEND1_ID=@LINKEND2_ID) and (LINKEND2_ID=@LINKEND1_ID))
  --    RAISERROR ('@LINKEND1_ID<>@LINKEND2_ID', 16, 1);

--    set @objname='link'



--------------------------------------------------------------------
-- 
--------------------------------------------------------------------
if @TEMPLATE_LINK_ID is not null
begin
  IF @property1_id is null    RAISERROR ('@property1_id', 16, 1);
  IF @property2_id is null    RAISERROR ('@property2_id', 16, 1);


  delete from LINKEND where id in (@LINKEND1_ID,@LINKEND2_ID)

  exec @LINKEND1_ID = [lib].sp_Template_Link_apply_to_link @property_id=@property1_id, @TEMPLATE_LINK_ID=@TEMPLATE_LINK_ID
  exec @LINKEND2_ID = [lib].sp_Template_Link_apply_to_link @property_id=@property2_id, @TEMPLATE_LINK_ID=@TEMPLATE_LINK_ID

end else
begin
  SELECT @property1_id=property_id  FROM LinkEnd WHERE (ID=@LINKEND1_ID);            
  SELECT @property2_id=property_id  FROM LinkEnd WHERE (ID=@LINKEND2_ID);

end
 

--    IF @project1_id  IS NULL  BEGIN RAISERROR ('@project1_id is NULL', 16, 1);  return -4  END 
--    IF @project2_id  IS NULL  BEGIN RAISERROR ('@project2_id is NULL', 16, 1);  return -5  END 
--    IF @project1_id <> @project2_id  BEGIN RAISERROR ('@project1_id <> @project1_id', 16, 1);  return -6 END 
--    IF @property1_id = @property2_id  BEGIN RAISERROR ('@property1_id = @property2_id ', 16, 1);  return -8 END 
 
 
           
--    SELECT @property2_id=property_id 
  --    FROM LinkEnd WHERE (ID=@LINKEND2_ID)

    ----------------------------------
    --  set CHANNEL_TYPE 'low','high'
    ----------------------------------
  --  UPDATE LinkEnd set CHANNEL_TYPE='low'  WHERE (ID=@LINKEND1_ID);
  --  UPDATE LinkEnd set CHANNEL_TYPE='high' WHERE (ID=@LINKEND2_ID);


   
--  END ELSE  
 -- BEGIN
 -- print  ''
  
 --   set @objname='pmp_link'
    
 --   set @property1_id = dbo.fn_PMP_Sector_Get_Property_id(@PMP_SECTOR_ID)
--IF @property1_id  IS NULL  RAISERROR ('@property1_id is NULL   111111111', 16, 1); 

/*
    SELECT 
    	@property1_id=property_id   --, @project2_id=project_id 
     --   @project1_id =project_id 
      FROM view_PMP_sector --_with_project_id
      WHERE (ID=@Pmp_sector_ID)
      


    SELECT 
    	@project1_id=project_id 
   	FROM Property 
    WHERE (ID=@property1_id);


--    SELECT @property1_id = property_id
  --  FROM PMP_Sector WHERE id= @PMP_SECTOR_ID
    --LEFT OUTER JOIN PMP_site  ON PMP_Sector.pmp_site_id = PMP_site.id

    SELECT 
    	@property2_id=property_id  
    --    @project2_id=project_id 
      FROM view_PMP_TERMINAL --_with_project_id 
      WHERE (ID=@PMP_TERMINAL_ID)
     
     */
      
 --   IF @project1_id   IS NULL BEGIN RAISERROR ('@project1_id is NULL', 16, 1);   return -4  END 
--    IF @project2_id   IS NULL BEGIN RAISERROR ('@project2_id is NULL', 16, 1);   return -4  END     
--    IF @property1_id  IS NULL  RAISERROR ('@property1_id is NULL', 16, 1); 
--    IF @property2_id  IS NULL  RAISERROR ('@property2_id is NULL', 16, 1); 
--    IF @project1_id <> @project2_id BEGIN  RAISERROR ('@project1_id <> @project1_id', 16, 1);  return -4  END 
 

 -- END




--  exec sp_Object_GetNewName @OBJNAME='Link', @NAME=@NAME, 
 --       @PROJECT_ID=@project_id, @FOLDER_ID=@FOLDER_ID, @RESULT=@sName out


--  set @len_m = dbo.fn_Property_Get_Length_m (@property1_id, @property2_id);
  --IF @len_m > 100000 
   --  RAISERROR ('@len_m > 100 000 ', 16, 1);    
  


  INSERT INTO Link
     (PROJECT_ID, 
      NAME, 
      FOLDER_ID,  
      objname, 
  
      LINKEND1_ID, 
      LINKEND2_ID, 
    
--      PMP_SECTOR_ID, 
--      PMP_TERMINAL_ID,
      
      property1_id,
      property2_id,
         
      length --temp
      
      )
  VALUES
     (@project_id, 
      @NAME, --@sNAME, 
      @FOLDER_ID,
      @objname, 
      
      @LINKEND1_ID, 
      @LINKEND2_ID,
    
--      @PMP_SECTOR_ID, 
--      @PMP_TERMINAL_ID,
      
      @property1_id,
      @property2_id,
             
     
      1 --length 
      ) 
   
     
--  set @new_id=@@IDENTITY;
        
  set @new_id=IDENT_CURRENT ('Link')
     


Alter table  link  Disable trigger all

  
  
  UPDATE Link
  SET    
    CLUTTER_MODEL_ID = @CLUTTER_MODEL_ID,
     
    status_id=@status_id,
      
  --  NAME_SDB=@NAME_SDB,
  --  NAME_TN =@NAME_TN,
    Comments=@Comments,
    
   -- Link_REPEATER_ID=@Link_REPEATER_ID,
      
    SESR= @SESR,
    KNG = @KNG,
 --   SPACE_LIMIT = @SPACE_LIMIT,
    CALC_METHOD = @CALC_METHOD
    
  WHERE 
      id = @new_ID

  exec sp_Link_Update_Length_and_Azimuth @id = @new_ID
  
  Alter table  link ENABLE trigger all
  
  
  -------------------------------------------
  select @lat1=lat, @lon1=lon  FROM [Property]  WHERE id=@property1_id    
  select @lat2=lat, @lon2=lon  FROM [Property]  WHERE id=@property2_id

--RETURN




  
  --  IF @@IDENTITY=0  RAISERROR ('@@IDENTITY=0', 16, 1);
  
    
 -- if @IS_GET_INFO=1
    SELECT id,guid,name,  
    	--	LENGTH_M,	
    		@lat1 as lat1, @lon1 as lon1,  
    		@lat2 as lat2, @lon2 as lon2
      FROM link 
      WHERE ID=@new_id
  
  RETURN @new_id;     
    
--   RETURN @@IDENTITY;    
        
end

/*


Alter table  property Disable trigger all


--  set @new_id=@@IDENTITY
  
    UPDATE property
    SET     
      TOWER_HEIGHT = @TOWER_HEIGHT,
    
      GROUND_HEIGHT =@GROUND_HEIGHT,
      CLUTTER_HEIGHT=@CLUTTER_HEIGHT,
      clutter_name  =@clutter_name,
      
      LAT_WGS = @LAT_WGS,
      LON_WGS = @LON_WGS,      
      
      LAT_CK95 = @LAT_CK95,
      LON_CK95 = @LON_CK95,
            
      ADDRESS 	=@ADDRESS,
      CITY 		=@CITY,
      GEOREGION_ID =@GEOREGION_ID,


      State_id=@State_id, 
      Placement_id=@Placement_id,
     
      Comments=@Comments
    
 
      
   	WHERE 
    	id = @new_id
  
  exec sp_Property_Update_lat_lon_kras_wgs @ID=@new_id
  

Alter table  property ENABLE trigger all


*/

GO
/****** Object:  StoredProcedure [dbo].[sp_LinkEnd_Add]    Script Date: 16.05.2020 10:54:58 ******/


IF OBJECT_ID(N'[dbo].[sp_LinkEnd_Add]') IS  not  NULL

DROP PROCEDURE [dbo].[sp_LinkEnd_Add]
GO
/****** Object:  StoredProcedure [dbo].[sp_LinkEnd_Add]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE procedure [dbo].[sp_LinkEnd_Add]
( 
  @OBJNAME 			VARCHAR(20)=null,
     
 -- @Template_site_ID int =NULL,
  
 -- @PROJECT_ID INT,

  @NAME 			varchar(250),
  @PROPERTY_ID 	    int,

--  @EQUIPMENT_TYPE int =1,   -- 1- ��������� ������������

  @LINKENDTYPE_ID		int =NULL,
  @LINKENDTYPE_MODE_ID  int =null, 


  @CHANNEL_TYPE VARCHAR(4)=null,


  @Rx_Level_dBm			float = null, --������� ������� �� ����� ���������


  @PASSIVE_ELEMENT_ID   int =null, 
  @PASSIVE_ELEMENT_LOSS float = null,


  -----------------------------
  -- custom equipment
  -----------------------------
  @BAND 					VARCHAR(10) = null,

 -- @POWER_MAX 	float = null,
  @Tx_Freq_MHz      		float = null, 
  @POWER_dBm 				float = 25,    
--  @POWER_LOSS float = null,

  @MODULATION_TYPE  		VARCHAR(50) = '32QAM', 
  @SIGNATURE_HEIGHT_dB  	float = 14,   --������ ���������
  @SIGNATURE_WIDTH_MHz  	float = 24,   --������ ���������
  @THRESHOLD_BER_3  		float = -80, 
  @THRESHOLD_BER_6  		float = -73, 

 -- @EQUALISER_PROFIT  		float = null,
  @Freq_Spacing_MHz  		float = null,       

  @Loss          		float = null,
  @KNG            			float = null,
  @Bitrate_Mbps 			float=NULL, --float


 -----------------------------
  -- PMP Sector
  -----------------------------
--  @PMP_SITE_ID int= null,
 -- @PMP_Sector_ID int= null,
  @PMP_Site_ID  int,

  @Cell_Layer_ID int = null,
  @Calc_Model_ID int = null,
  @Calc_Radius_km float= null,


  -----------------------------
  -- PMP terminal 
  -----------------------------
--  @PMP_SITE_ID int= null,
  @Connected_PMP_Sector_ID int= null,


  @COMMENTS varchar(250)=null

 
--  @IS_GET_INFO bit = 0

)
AS
begin

  SET ANSI_WARNINGS OFF --String or binary data would be truncated.

--return


  ---------------------------------------------
--  IF IsNull(@PROPERTY_ID,0)=0  RAISERROR ('@PROPERTY_ID is NULL', 16, 1);
  IF @OBJNAME = 'pmp_sector' and IsNull(@Pmp_site_ID ,0) = 0
     RAISERROR ('@Pmp_site_ID  NULL', 16, 1);
 -- IF @project_id IS NULL  RAISERROR ('@project_id is NULL', 16, 1);


  IF @Name IS NULL  RAISERROR ('@Name is NULL', 16, 1);

  
  ---------------------------------------------

  DECLARE 
    @EQUIPMENT_TYPE int,
    
  --  @project1_id 	int,
 --   @sName			VARCHAR(200),
    @s 				VARCHAR(100), 
    @id 		    int, 
    @new_ID 		int 
     
   


  ---------------------------------
  -- PMP sector
  ---------------------------------
  if IsNull(@Pmp_site_ID ,0) > 0
     SELECT 
      @PROPERTY_ID=PROPERTY_ID  
    FROM  Pmp_site  
      WHERE id = @Pmp_site_ID 
  
  
   
  
  
  IF not EXISTS(SELECT * FROM Property WHERE (ID=@Property_ID) )
    RAISERROR ('Property not EXISTS', 16, 1);
  
  
--  IF not EXISTS(SELECT * FROM project WHERE (ID=@project_ID) )
 --   RAISERROR ('project not EXISTS', 16, 1);
  
  
  
set @name = link.fn_LinkEnd_name (@PROPERTY_ID, @NAME) 
 
 
/*  SELECT top 1 @id=id FROM LinkEnd  WHERE (PROPERTY_ID=@PROPERTY_ID) and (name=@NAME)
  
  IF @id is not NULL
  BEGIN
    SELECT 'record already exists' as msg,  @id as ID     
    RETURN -1 * @id;  
  END  
*/  
  
  

 -- SELECT @project1_id=project_id FROM PROPERTY WHERE (ID=@PROPERTY_ID);
  
  
--  IF @project1_id IS NULL  RAISERROR ('@PROPERTY_ID - @project1_id is NULL', 16, 1);  
--  IF @project_id <> @project1_id  RAISERROR ('@project_id <> @project1_id', 16, 1);


  

  if @LINKENDTYPE_ID=0       SET @LINKENDTYPE_ID=NULL
  if @LINKENDTYPE_MODE_ID=0  SET @LINKENDTYPE_MODE_ID=NULL
    
  if ISNULL(@SIGNATURE_HEIGHT_dB,0)=0  SET @SIGNATURE_HEIGHT_dB=14 --������ ���������
  if ISNULL(@SIGNATURE_WIDTH_MHz,0)=0  SET @SIGNATURE_WIDTH_MHz=24 --������ ���������
  
  
  
    
  
  
 --print @sName



 --set @PROJECT_ID = dbo.fn_Property_GetProjectID(@PROPERTY_ID);
 
 
 --print @PROJECT_ID
 
 
 INSERT INTO LinkEnd
   (
    NAME, 
    PROPERTY_ID,    
    OBJNAME     
   )
  VALUES
   (
    @NAME, 
    @PROPERTY_ID,
    @OBJNAME 
   ) 
   
 -- set @new_ID = @@IDENTITY 
 set @new_id=IDENT_CURRENT ('LinkEnd')
 

print @new_ID


 
Alter table  LinkEnd Disable trigger all

   
  
  
--    SET @sName=@NAME

 
 
  IF ISNULL(@LINKENDTYPE_ID,0) > 0 
  begin
    set @EQUIPMENT_TYPE =0
    set @Tx_Freq_MHz = dbo.fn_LinkEndType_GetAveFreq_MHz (@LINKENDTYPE_ID)
  END
  ELSE BEGIN
    set @EQUIPMENT_TYPE =1    
    
    if Isnull(@Tx_Freq_MHz,0)=0
      set @Tx_Freq_MHz = dbo.fn_Band_GetAveFreq_MHz (@BAND) 
  END


 

 -- set @new_id=IDENT_CURRENT ('LinkEnd')
  

  
  UPDATE LinkEnd 
  set 
     Rx_Level_dBm       = @Rx_Level_dBm,
     
     PASSIVE_ELEMENT_ID = @PASSIVE_ELEMENT_ID,
     
     CHANNEL_TYPE =  @CHANNEL_TYPE,
     
     equipment_type     = @equipment_type,
        
     COMMENTS = @COMMENTS,
     
 
--  UPDATE PMP_Sector 
--   set 
  --  equipment_type = @equipment_type,
        
 --   LINKENDTYPE_ID     =@LINKENDTYPE_ID,
 --   LINKENDTYPE_MODE_ID=@LINKENDTYPE_MODE_ID,
        
   -- rx_freq_MHz = @TX_FREQ_MHZ,
  --  tx_freq_MHz = @TX_FREQ_MHZ,      	     	       
        
    --PMP sector
    Calc_Model_ID = COALESCE( @Calc_Model_ID,  Calc_Model_ID),
    Cell_Layer_ID = COALESCE( @Cell_Layer_ID,  Cell_Layer_ID),
    Calc_Radius_km= COALESCE( @Calc_Radius_km, Calc_Radius_km),
   
 	PMP_Site_ID = COALESCE( @PMP_Site_ID , PMP_Site_ID),
     
    --PMP terminal
    Connected_PMP_Sector_ID = COALESCE( @Connected_PMP_Sector_ID , Connected_PMP_Sector_ID) 



  WHERE    
    ID = @new_ID   

  

  
   
  if ISNULL(@LINKENDTYPE_ID,0) > 0
  
    exec sp_Object_Update_LinkEndType 
       @OBJNAME='LinkEnd', 
       @ID=@new_ID, 
       @LinkEndType_ID=@LinkEndType_ID, 
       @LinkEndType_mode_ID=@LinkEndType_mode_ID

   
  ELSE
    ------------------------
    -- custom equipment
    ------------------------      
    UPDATE linkend 
    set 
      BAND  = @BAND, 

      tx_freq_MHz = @TX_FREQ_MHZ,
      rx_freq_MHz = @TX_FREQ_MHZ,

      POWER_dBm   = @POWER_dBm,  

      SIGNATURE_HEIGHT =  COALESCE(@SIGNATURE_HEIGHT_dB, SIGNATURE_HEIGHT), 
      SIGNATURE_WIDTH  =  COALESCE(@SIGNATURE_width_MHz, SIGNATURE_width),
     
      MODULATION_TYPE  =  COALESCE(@MODULATION_TYPE, MODULATION_TYPE),
      THRESHOLD_BER_3  =  COALESCE(@THRESHOLD_BER_3, THRESHOLD_BER_3), 
      THRESHOLD_BER_6  =  COALESCE(@THRESHOLD_BER_6, THRESHOLD_BER_6),
      Bitrate_Mbps     =  COALESCE(@Bitrate_Mbps,    Bitrate_Mbps), 
        
      
      
      LOSS=@LOSS,         
      KNG = @KNG,
                 
      MODULATION_COUNT      = m.level_COUNT,
      GOST_53363_modulation = m.GOST_53363_modulation 
     
     from 
        (select top 1 * from Lib.Modulations 
         WHERE name=@MODULATION_TYPE) m     
        
    WHERE    
      linkend.ID = @new_ID
       
      
Alter table  LinkEnd enable trigger all
     

if @Connected_PMP_Sector_ID is not null
  exec dbo.sp_PMP_Terminal_Plug  @ID = @new_ID, @PMP_SECTOR_ID = @Connected_PMP_Sector_ID  

/*   
CREATE PROCEDURE dbo.sp_PMP_Terminal_Plug
( 
  @ID 	    	  int,
  @PMP_SECTOR_ID  int
)
AS*/
       
  
  
--  if @IS_GET_INFO=1
  SELECT id,guid,name,PROPERTY_ID FROM LINKEND WHERE ID=@new_ID
  
  RETURN @new_ID;    

  


end

GO
/****** Object:  StoredProcedure [dbo].[sp_Linkend_Antenna_Add]    Script Date: 16.05.2020 10:54:58 ******/
----------------------------------------------

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
IF OBJECT_ID(N'[dbo].[fn_LinkEnd_Azimuth]') IS  not  NULL

DROP function [dbo].[fn_LinkEnd_Azimuth]
GO



CREATE FUNCTION dbo.fn_LinkEnd_Azimuth 
(
  @ID int,  --LinkEnd ID
  @NEXT_LinkEnd_ID int=null
)
RETURNS float
BEGIN
  DECLARE    
--    @property1_id int, 
--    @property2_id int,
  --  @Next_LinkEnd_ID int,
    
    @lat1 float,
    @lon1 float,
    
    @lat2 float,
    @lon2 float,

    @result float;

  if @NEXT_LinkEnd_ID is null 
     set @Next_LinkEnd_ID = dbo.fn_LinkEnd_Get_Next_LinkEnd_ID (@ID);
   


  IF @Next_LinkEnd_ID IS NULL
    return -1;




--5162


    
    SELECT                  
       @lat1=A1.lat,
       @lon1=A1.lon
    FROM dbo.LinkEnd 
         cross apply [dbo].fn_Linkend_Antenna_pos (id) A1
    where id=@ID
    
    
    

------------------
    SELECT                  
       @lat2=A1.lat,
       @lon2=A1.lon
    FROM dbo.LinkEnd 
         cross apply [dbo].fn_Linkend_Antenna_pos (id) A1
    where id=@Next_LinkEnd_ID
    
    
    
  if @LAT1 is null or 
     @LAT2 is null or
     @LON1 is null or
     @LON2 is null
   return null  

    

------------------


--return @Next_LinkEnd_ID
--set @Next_LinkEnd_ID = 0

/*  SELECT @property1_id = property_id
    FROM Linkend
    WHERE id = @ID


  SELECT @property2_id = property_id
    FROM Linkend
    WHERE id = @Next_LinkEnd_ID
 
*/
  RETURN dbo.fn_AZIMUTH (@lat1, @lon1, @lat2, @lon2, null);

--  RETURN dbo.fn_Property_Get_AZIMUTH (@property1_id, @property2_id);
 
    
END

go


IF OBJECT_ID(N'[dbo].[sp_Linkend_Antenna_Add]') IS  not  NULL

DROP PROCEDURE [dbo].[sp_Linkend_Antenna_Add]
GO
/****** Object:  StoredProcedure [dbo].[sp_Linkend_Antenna_Add]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE procedure [dbo].[sp_Linkend_Antenna_Add]
(    
  @NAME 				varchar(50),          
  @ANTENNATYPE_ID		int =null,     

  @HEIGHT  	float=null, 
  @AZIMUTH 	float=null, 
  @TILT 	float=null,
  @LOSS	float=null,

  -----------------------------------
  -- Custom
  -----------------------------------
  @BAND 		varchar(50)=null, 
  @Freq_MHz 	float=null,
  @GAIN	    	float=null,          --_dBi
  @DIAMETER   	float=null,      
  @VERT_WIDTH 	float=null,
  @HORZ_WIDTH 	float=null,
  @POLARIZATION_STR varchar(4)=null,
  
  -----------------------------------
  @LINKEND_ID 		int 

--  @PMP_SECTOR_ID 	int =null,
  
--  @PMP_TERMINAL_ID 	int =null
--  @REPEATER_ID 	    int =null


--@LOCATION_TYPE int = null


--@LAT 		float=null,
--@LON 		float=null,


)
AS
begin
 if @Name is null
  begin
    set @Name = 'sdfsdfsdf'
    set @LINKEND_ID = 5162
    
    set @AntennaType_ID = 11101
    set @Height = 20
    set @Diameter = 33
  
  end
  
  
IF @LINKEND_ID =0
   RAISERROR ('@LINKEND_ID =0', 16, 1);



IF not EXISTS (SELECT * FROM LINKEND WHERE id = @LINKEND_ID)
   RAISERROR ('not EXISTS (SELECT * FROM LINKEND WHERE id = @LINKEND_ID)', 16, 1);






  DECLARE 
    --@sName VARCHAR(100),
    
    @next_linkend_id int,
    @count int, 
    @new_id int, 
    @pmp_site_id int,
    
    @project_id  int, 
    @property_id int,         
           
    @LAT 		float,
    @LON 		float,
    @LOCATION_TYPE int
         
   
 
 --    @aLat float,
  --   @aLon float
     
  --258929
  
    -- Name     : ����������� � ���� � ��������� before insert
  -- Address  : ����������� � ���� � ��������� before insert
  
    
  
--  IF @PROJECT_ID IS NULL  RAISERROR ('@PROJECT_ID is NULL', 16, 1);
  IF @Name IS NULL  RAISERROR ('@Name is NULL', 16, 1);

/*
  IF (@LINKEND_ID IS NULL) and 
     (@PMP_SECTOR_ID IS NULL) and 
     (@PMP_TERMINAL_ID IS NULL) --and
--     (@REPEATER_ID IS NULL) 
          
   RAISERROR ('@LINKEND_ID,@PMP_SECTOR_ID,@PMP_TERMINAL_ID is NULL', 16, 1);
*/
   

  ----------------------------
--  IF ISNULL(@LINKEND_ID,0)>0 
  ----------------------------
--  BEGIN
    SELECT @count=Count(*)  FROM Linkend_Antenna   WHERE LINKEND_ID = @LINKEND_ID  

    if @count=2   RETURN -2;    
--  END   
   
   


  -----------------------------------  
 -- IF (@LINKEND_ID IS NOT NULL) 
--  BEGIN
 --   IF NOT EXISTS(SELECT * FROM LINKEND WHERE id = @LINKEND_ID)
  --    RAISERROR ('@LINKEND_ID NOT EXISTS: %d', 16, 1, @LINKEND_ID);    

      
  
    -- antenna 2 --
    set @Next_LinkEnd_ID = dbo.fn_LinkEnd_Get_Next_LinkEnd_ID(@LINKEND_ID)  
        
/*  select @LINKEND_ID
    
    
    return
          */
    
    if @Next_LinkEnd_ID is not NULL
       set @AZIMUTH = dbo.fn_LinkEnd_Azimuth(@LINKEND_ID,null)  
   
  
--  return
   
 -- END ELSE

/*  -----------------------------------
  IF (@PMP_SECTOR_ID IS NOT NULL) 
  BEGIN  
    set @property_id = dbo.fn_PMP_Sector_Get_Property_id (@PMP_SECTOR_ID)
    
    
  END ELSE
*/
   
/*  -----------------------------------
  IF (@PMP_TERMINAL_ID IS NOT NULL) 
  BEGIN  
    SELECT @property_id=PROPERTY_ID 
      FROM PmpTerminal  WHERE id = @PMP_TERMINAL_ID
      
  END ELSE
*/  
/*
  -----------------------------------
  IF (@REPEATER_ID IS NOT NULL) 
  BEGIN  
    SELECT @property_id=PROPERTY_ID 
      FROM Link_Repeater WHERE id = @REPEATER_ID
      
  END ELSE
  BEGIN
    RAISERROR ('OWNER is incorrect', 16, 1);
    return -2
  END
 */ 	



 -- IF (@property_id IS NULL)  
  --  RAISERROR ('RAISERROR - @PROPERTY_ID is NULL', 16, 1);

    SELECT @property_id=PROPERTY_ID   
      FROM LINKEND   
      WHERE id = @LINKEND_ID

if @property_id is null
   RAISERROR ('@property_id is null', 16, 1);


  SELECT 
   --   @project_id=PROJECT_ID, 
      @lat=LAT, 
      @lon=LON
    FROM Property  
    WHERE id = @property_id 

--SET @LOCATION_TYPE = 0;



--     (@PMP_SECTOR_ID IS NULL) and 
  --   (@PMP_TERMINAL_ID IS NULL)
  
  -- RAISERROR ('@LINKEND_ID,@PMP_SECTOR_ID,@PMP_TERMINAL_ID is NULL', 16, 1);
    

  if  (@ANTENNATYPE_ID  is not null )
  BEGIN
    SELECT 
      @BAND        = BAND,  

      @Gain        = Gain,  
      @Freq_MHz    = Freq,
--      @Freq_MHz    = Freq_MHz,
      @Diameter    = Diameter,
      @Polarization_STR= Polarization_STR,
      @Vert_Width  = Vert_Width,
      @Horz_Width  = Horz_Width
    FROM AntennaType 
    WHERE id = @ANTENNATYPE_ID      
  END



  INSERT INTO Linkend_Antenna
     (
      NAME, 
     
/*     ANTENNATYPE_ID,  
      BAND,
      FREQ_MHz,
      
      LOSS_dB,  GAIN, DIAMETER, 
      VERT_WIDTH, HORZ_WIDTH, 
      POLARIZATION_STR,
      HEIGHT, AZIMUTH, TILT, 
      
      LAT, LON, LOCATION_TYPE,
*/ 
     
      LINKEND_ID, 
--      PMP_SECTOR_ID,
  --    PMP_TERMINAL_ID,
   --   REPEATER_ID,
      
      PROPERTY_ID
      )
  VALUES
     (
      @NAME, 
     
/*     @ANTENNATYPE_ID,  
      @BAND,
      @Freq_MHz, 
     
      @LOSS_dB, @GAIN, @DIAMETER, 
      @VERT_WIDTH, @HORZ_WIDTH, 
      @POLARIZATION_STR,  
      @HEIGHT, @AZIMUTH, @TILT, 
      
      @LAT, @LON, @LOCATION_TYPE,
*/
      @LINKEND_ID,     
--      @PMP_SECTOR_ID,
--      @PMP_TERMINAL_ID,
--      @REPEATER_ID,
        
      @property_id
      ) 



 --  set @new_id = @@IDENTITY
  
  set @new_id= IDENT_CURRENT ('Linkend_Antenna')   
    

 
Alter table  Linkend_Antenna Disable trigger all

  


  UPDATE Linkend_Antenna
  set    
      ANTENNATYPE_ID=@ANTENNATYPE_ID,  
     
      BAND		=@BAND,
      FREQ_MHz	=@FREQ_MHz,
      
      LOSS	=@LOSS,  
      GAIN		=@GAIN, 
      DIAMETER	=@DIAMETER, 
      VERT_WIDTH=@VERT_WIDTH, 
      HORZ_WIDTH=@HORZ_WIDTH, 
      
      POLARIZATION_STR=@POLARIZATION_STR,
      HEIGHT     =@HEIGHT, 
      AZIMUTH    =@AZIMUTH,
      TILT       =@TILT, 
      
      LAT           =@LAT,  
      LON           =@LON, 
      LOCATION_TYPE =@LOCATION_TYPE
     
  WHERE id=@new_id



Alter table  Linkend_Antenna enable trigger all


  ----------------------------
  IF ISNULL(@LINKEND_ID,0)>1 
  ----------------------------
  BEGIN
    SELECT @count=Count(*) 
      FROM Linkend_Antenna
      WHERE LINKEND_ID = @LINKEND_ID
  
    if @count=2  
      UPDATE LINKEND 
        SET  KRATNOSTBYSPACE=1
        WHERE id = @LINKEND_ID
  END

/*  ----------------------------
  IF ISNULL(@PMP_TERMINAL_ID,0)>1
  ----------------------------
  BEGIN
    SELECT @count=Count(*) 
      FROM Linkend_Antenna
      WHERE PMP_TERMINAL_ID = @PMP_TERMINAL_ID

    if @count=2  
      UPDATE PMPTERMINAL
        SET  KRATNOSTBYSPACE=1
        WHERE id = @PMP_TERMINAL_ID


--    UPDATE PMPTERMINAL
  --    SET  KRATNOSTBYSPACE=1
    --  WHERE id = @PMP_TERMINAL_ID
  END
*/

--  SELECT id,guid,name FROM link WHERE ID=@new_id


  RETURN @new_id;


/*       if iAntennaCount=1 then
         dmLinkEnd.Update (aPmpTerminalID, [db_Par(FLD_KRATNOST_BY_SPACE, 1)]);
*/

        
end

GO
/****** Object:  StoredProcedure [dbo].[sp_PMP_Terminal_Plug]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[sp_PMP_Terminal_Plug]') IS  not  NULL

DROP PROCEDURE [dbo].[sp_PMP_Terminal_Plug]
GO
/****** Object:  StoredProcedure [dbo].[sp_PMP_Terminal_Plug]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* 
������������ PMP_Terminal �� ������ ������
 */
/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */ 
CREATE PROCEDURE [dbo].[sp_PMP_Terminal_Plug]
( 
  @ID 	    	  int,
  @PMP_SECTOR_ID  int
)
AS
BEGIN
  ---------------------------------------------
--  IF @ID IS NULL  RAISERROR ('@ID is NULL', 16, 1);
--  IF @PMP_Sector_ID IS NULL  RAISERROR ('@PMP_Sector_ID is NULL', 16, 1);
  ---------------------------------------------
 
if  @ID IS NULL 
begin
  set @ID = 9406
  set @PMP_SECTOR_ID = 9387
  
end



  DECLARE 
    @result 	int,

    @pmp_site_id 	int,
    @link_id int,

	@sector_project_id 		int,    
	@terminal_project_id 	int,    

    @property_pmp_site_id 	    int,
    @property_terminal_id   int,
    
    @pmp_site_name 			VARCHAR(50),
    @pmp_terminal_name  	VARCHAR(50),
    @new_name  				VARCHAR(100),
    @property_name 			VARCHAR(100)
 
  
  SELECT @pmp_site_id       = pmp_site_id, 
         @sector_project_id = project_id
    FROM view_PMP_sector --_with_project_id       
    WHERE id = @PMP_SECTOR_ID


--IF @sector_project_id  IS NULL  RAISERROR ('@sector_project_id is NULL', 16, 1); 

  -------------------------------------------------------------
  IF @pmp_site_id IS NULL  RAISERROR ('@pmp_site_id is NULL', 16, 1);
  -------------------------------------------------------------





  
  SELECT @pmp_site_name    = name, 	
  		 @property_pmp_site_id = property_id
    FROM PMP_Site     
    WHERE id = @pmp_site_id

  SELECT  @pmp_terminal_name    = name,  
  		  @property_terminal_id = property_id,
          @terminal_project_id  = project_id,
          
          @property_name = property_name
            
    FROM view_PMP_TERMINAL --_with_project_id   
    WHERE id = @ID





  IF @terminal_project_id <> @sector_project_id
     RAISERROR ('@terminal_project_id <> @sector_project_id', 16, 1);



update LinkEnd 
  set 
    connected_pmp_sector_id = @PMP_SECTOR_ID,
    connected_pmp_site_id   = (select pmp_site_id from LinkEnd where ID=  @PMP_SECTOR_ID)
  where 
    id = @ID 



  SELECT @link_id = id
    FROM Link  
    WHERE linkend2_id = @ID


  SET @new_name = @pmp_site_name +' <-> '+ @property_name + ' / ' +@pmp_terminal_name
  
  --SET @new_name = @pmp_site_name +' <-> '+ @pmp_terminal_name
--  SET @new_name = @pmp_terminal_name +' <-> '+ @pmp_site_name

/*

print  @pmp_terminal_name
print  @pmp_site_name
print  @new_name

*/



  --------------------------------
  IF @link_id IS NOT NULL
  --------------------------------
  BEGIN
    UPDATE Link  
      SET name = @new_name,
          linkend1_id   = @PMP_SECTOR_ID,
        --  property1_id  = @property_pmp_site_id,
          
          
          profile_XML = null

--          pmp_sector_id = @PMP_SECTOR_ID,
--          property1_id  = @property_site_id
--          length=1 
    WHERE 
      id = @link_id
    
    
    exec sp_Link_ClearCalc @link_id
    exec sp_Link_Update_Length_and_Azimuth @link_id

      
  END ELSE
  --------------------------------
  BEGIN
    IF @PMP_SECTOR_ID  IS NULL  RAISERROR ('@PMP_SECTOR_ID is NULL', 16, 1); 
    IF @sector_project_id  IS NULL  RAISERROR ('@sector_project_id is NULL', 16, 1);   
  
  
--  return
  


--select   @PMP_SECTOR_ID,@ID 

--return

   
/*
  dbo.ft_AZIMUTH_and_length   
  
  
    length_m float,
    AZIMUTH  float,
    AZIMUTH_back  float
    
   */

/*    
    set @AZIMUTH_rep1 = dbo.fn_AZIMUTH(@LAT_rep, @Lon_rep,  @LAT1, @LON1, null)
    set @AZIMUTH_rep2 = dbo.fn_AZIMUTH(@LAT_rep, @Lon_rep,  @LAT2, @LON2, null)
            

*/
  
     EXEC @link_id = sp_Link_Add   
            @project_id     = @sector_project_id,
            @OBJNAME        = 'pmp_link', 
            @NAME			= @new_name,
            
            @linkend1_id    = @PMP_SECTOR_ID,
            @linkend2_id    = @ID 
            
          --  @length_M				= 1

  --return


  -- set @link_id = @@IDENTITY
   
  -- print @link_id
   
--  if @IS_GET_INFO=1
--    SELECT id,guid,name  FROM LINKEnD WHERE ID=@ID
  
--  RETURN @id;     
         
  END
 

print @link_id

--return  


IF @link_id IS NULL  
  RAISERROR ('dbo.sp_PMP_Terminal_Plug - @link_id is NULL', 16, 1);


--exec sp_Link_Update_Length_and_Azimuth @id = @link_id

 
  SELECT * FROM view_Link
--  SELECT * FROM view_Link_lat_lon   
    WHERE id=@link_id
   
  
  RETURN @link_id;
  
  /* 
  select LAT,LON for calc len
   */
  
END

GO
/****** Object:  StoredProcedure [dbo].[sp_PMP_Terminal_Select_PMP_Sectors_for_Plug]    Script Date: 16.05.2020 10:54:58 ******/


IF OBJECT_ID(N'[dbo].[sp_PMP_Terminal_Select_PMP_Sectors_for_Plug]') IS  not  NULL

DROP PROCEDURE [dbo].[sp_PMP_Terminal_Select_PMP_Sectors_for_Plug]
GO
/****** Object:  StoredProcedure [dbo].[sp_PMP_Terminal_Select_PMP_Sectors_for_Plug]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* 
  ����� PMP_Sectors ��� PMP ��������� 
  
created: Alex  
used in: d_PmpTerminal_Plug  
 */
 /* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE [dbo].[sp_PMP_Terminal_Select_PMP_Sectors_for_Plug]
( 
  @ID int,
  @PROJECT_ID int = NULL
)
AS
BEGIN
  ---------------------------------------------
--  IF @ID IS NULL  RAISERROR ('@ID is NULL', 16, 1);
  ---------------------------------------------
  DECLARE 
  --  @project_id int,
    @property_id  int;


--if IsNull(@ID,0) > 0
    SELECT 
        @property_id = property_id ,        
    	@project_id = project_id
      FROM view_PMP_TERMINAL --_with_project_id       
      WHERE id = @ID



  IF @project_id is null   RAISERROR ('@project_id is null ', 16, 1);


--fn_Property_Get_Length_m


  SELECT  id,name,pmp_site_id, pmp_site_name, Property_name, band, 
        dbo.fn_Property_Get_Length_m (@Property_id, Property_id) as Length_m,
        cast(0 as bit) as checked,
        
        @id as pmp_Terminal_id
  
    FROM  view_PMP_sector --_full 
    WHERE (project_id=@project_id)
         
         and 
          
          -- (@property_id IS null) or
          (Property_id <> IsNull(@Property_id,0) ) 
          
    ORDER BY pmp_site_name, name
  
  
  RETURN @@ROWCOUNT
    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Template_Linkend_Copy_to_PMP_SITE]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[pmp].[sp_Template_Linkend_Copy_to_PMP_SITE]') IS  not  NULL

DROP PROCEDURE [pmp].[sp_Template_Linkend_Copy_to_PMP_SITE]
GO
/****** Object:  StoredProcedure [dbo].[sp_Template_Linkend_Copy_to_PMP_SITE]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE pmp.sp_Template_Linkend_Copy_to_PMP_SITE 
(
  @Template_Linkend_ID INT = 1012, 
  @PMP_SITE_ID int = 167
)    
AS
BEGIN
  SET ANSI_WARNINGS OFF --String or binary data would be truncated.

  DECLARE  
    @id_new int,
    @property_ID int;
 --   @PROJECT_ID int; 
 --   @sName VARCHAR(100);

           
   
  SELECT 
--  		@PROJECT_ID = PROJECT_ID,
        @property_ID = property_ID 
--  	FROM view_PMP_Site 
  	FROM PMP_Site 
    WHERE (ID = @PMP_SITE_ID) 
  


 -- if @ID is null
 --   set @ID = 70365
  
  if @Template_Linkend_ID is null
  BEGIN
   SELECT top 1  @Template_Linkend_id =id FROM Template_Linkend
  
--    set @ID = 70365
 --   set @LINKEND_ID =124645
  END

    	
 -- print @ID
  
  INSERT INTO LinkEnd
--  INSERT INTO PMP_Sector
        
      (
      objname,   
 	  PMP_SITE_ID,
      property_ID,
      
       name )
   select 
      'PMP_Sector',   
      @PMP_SITE_ID,
      @property_ID,
      
      name
   from 
   		Template_Linkend     
   where 
   		id = @Template_Linkend_ID;      
    
   set @id_new= IDENT_CURRENT ('LinkEnd')


-----------------------------------------------------------   

Alter table  LinkEnd Disable trigger all

      
 UPDATE LinkEnd 
 SET  
   		band = m.band,
        LinkEndType_ID 		= m.LinkEndType_ID,
        LinkEndType_Mode_ID = m.LinkEndType_Mode_ID,
        Mode 				= m.mode,
        subband				= m.subband,
        bitrate_Mbps    	= m.bitrate_Mbps,
        channel_type        = m.channel_type,
                 
        
      --  MODULATION_count=m.MODULATION_level_count,

        MODULATION_TYPE =m.MODULATION_TYPE,
        SIGNATURE_HEIGHT=m.SIGNATURE_HEIGHT, 
        SIGNATURE_WIDTH =m.SIGNATURE_WIDTH,  
        THRESHOLD_BER_3 =m.THRESHOLD_BER_3,  
        THRESHOLD_BER_6 =m.THRESHOLD_BER_6,  
        
        POWER_MAX       = m.POWER_MAX,
        POWER_dBm       = m.POWER_MAX,
        
        rx_freq_MHz = m.rx_freq_MHz,
        tx_freq_MHz = m.tx_freq_MHz,
        
        channel_width_MHz =m.channel_width_MHz,        
        
        cell_layer_id = m.cell_layer_id,
        calc_model_id = m.calc_model_id,
        k0 			= m.k0,
        k0_open 	= m.k0_open,
        k0_closed 	= m.k0_closed,
     
 --   trx_id 		= m.trx_id,
                  
        calc_radius_km = m.calc_radius_km

    --    combiner_id    = m.combiner_id,
     --   combiner_loss  = m.combiner_loss
                
        
        
--       MODULATION_COUNT      = m.MODULATION_level_COUNT,
--       GOST_53363_modulation = m.GOST_53363_modulation,
        
   --     Freq_spacing  =  m.BANDWIDTH * CHANNEL_SPACING    -- calculated        
        
          
       from 
          (select * from Template_Linkend WHERE id=@Template_Linkend_id) m     
       WHERE 
         (LinkEnd.id = @id_new)       
  
 print @@ROWCOUNt
 
Alter table  LinkEnd enable trigger all
 
  
-----------------------------------------------------------   

    
 --	set @id_new= @@IDENTITY;
    
  exec [pmp].sp_Template_Linkend_Antenna_Copy_to_PMP_Sector_Antenna 
       @Template_Linkend_id = @Template_Linkend_ID, 
       @Linkend_ID          = @id_new
    
  RETURN @id_new       
 
END


  
/*
CREATE TABLE dbo.Template_LinkEnd (
  id int IDENTITY(1, 1) NOT NULL,
  name varchar(50) COLLATE Cyrillic_General_CI_AS NOT NULL,
  template_pmp_site_id int NOT NULL,
  objname varchar(10) COLLATE Cyrillic_General_CI_AS NULL,
  cell_layer_id int NULL,
  calc_model_id int NULL,
  k0 float NULL,
  k0_open float NULL,
  k0_closed float NULL,
  trx_id int NULL,
  linkendtype_id int NULL,
  power_W float NULL,
  power_dBm float NULL,
  THRESHOLD_BER_3 float NULL,
  THRESHOLD_BER_6 float NULL,
  tx_freq_MHz float NULL,
  calc_radius_km int NULL,
  calc_radius_noise_km int NULL,
  combiner_id int NULL,
  combiner_loss float NULL,
  Comment varchar(250) COLLATE Cyrillic_General_CI_AS NULL,
  guid uniqueidentifier DEFAULT newid() NULL,
  date_created datetime DEFAULT getdate() NULL,
  date_modify datetime NULL,
  user_modify varchar(50) COLLATE Cyrillic_General_CI_AS NULL,
  user_created varchar(50) COLLATE Cyrillic_General_CI_AS DEFAULT suser_name() NULL,
  band varchar(50) COLLATE Cyrillic_General_CI_AS NULL,
  LinkEndType_mode_id int NULL,
  LinkEndType_band_id__ int NULL,
  mode int NULL,
  channel_type int NULL,
  rx_freq_MHz float NULL,
  subband varchar(50) COLLATE Cyrillic_General_CI_AS NULL,
  channel_number int NULL,
  bitrate_Mbps float NULL,
  SIGNATURE_HEIGHT float NULL,
  SIGNATURE_WIDTH float NULL,
  channel_width_MHz float NULL,
  POWER_MAX float NULL,
  POWER_LOSS float NULL,
  Modulation_type varchar(30) COLLATE Cyrillic_General_CI_AS NULL,

 UPDATE LinkEnd SET  
        LinkEndType_Mode_ID = m.ID,
        Mode 			= m.mode,
        bitrate_Mbps    = m.bitrate_Mbps,
                 
      --  MODULATION_count=m.MODULATION_level_count,

        MODULATION_TYPE =m.MODULATION_TYPE,
        SIGNATURE_HEIGHT=m.SIGNATURE_HEIGHT, 
        SIGNATURE_WIDTH =m.SIGNATURE_WIDTH,  
        THRESHOLD_BER_3 =m.THRESHOLD_BER_3,  
        THRESHOLD_BER_6 =m.THRESHOLD_BER_6,  
        
    --    POWER_MAX       = m.POWER_MAX,
        POWER_dBm       = m.POWER_MAX,
        
       MODULATION_COUNT      = m.MODULATION_level_COUNT,
       GOST_53363_modulation = m.GOST_53363_modulation,
        
        Freq_spacing  =  m.BANDWIDTH * CHANNEL_SPACING    -- calculated        
        
          
       from 
          (select * from view_LinkEndType_Mode WHERE id=@LinkEndType_Mode_ID) m     
       WHERE 
         (LinkEnd.id = @LinkEnd_id) 

  UPDATE PMP_Site 
     SET 
  	   calc_radius_km  =m.calc_radius_km       
     from 
        (select * from Template_PMP_Site WHERE id=@ID) m     
     WHERE 
        PMP_Site.id=@PMP_Site_ID      
   
Alter table  Pmp_Site Disable trigger all



 if @TEMPLATE_PMP_SITE_ID>0 
   exec dbo.sp_Template_PMP_Site_Copy_to_PMP_Site 
   			@ID = @TEMPLATE_PMP_SITE_ID, 
            @Pmp_site_id = @new_id


Alter table  Pmp_Site enable trigger all

*/


GO
/****** Object:  View [dbo].[view_Link]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[view_Link]') IS not  NULL

DROP VIEW [dbo].[view_Link]
GO
/****** Object:  View [dbo].[view_Link]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_Link]
AS
SELECT     dbo.Link.*, 


Round(100-dbo.Link.Kng,2) as Reliability,


a1.lat AS lat1, 
a1.lon AS lon1, 
a2.lat AS lat2, 
a2.lon AS lon2, 




Property1.georegion_id AS georegion1_id, 
Property2.georegion_id AS georegion2_id,

-- Property1
Property1.address AS Property1_address, 
Property2.address AS Property2_address,

Property1.name AS Property1_name, 
Property2.name AS Property2_name, 

--Property1.name AS [Property1_name], 
--Property2.name AS [Property2_name], 

--Property1.id AS Property1_id, 
--Property2.id AS Property2_id, 


Property1.NRI AS Property1_NRI, 
Property2.NRI AS Property2_NRI, 

Property1.name_rus AS Property1_name_rus, 
Property2.name_rus AS Property2_name_rus, 



Property1.ground_height AS Property1_ground_height, 
Property2.ground_height AS Property2_ground_height,

GeoRegion_1.name AS [Property1_GeoRegion_name], 
GeoRegion_2.name AS [Property2_GeoRegion_name], 


Property1.Name_ERP AS [Property1_name_ERP], 
Property1.name_SDB AS [Property1_name_SDB], 

    

  --LinkEnd1.Band, -- AS LinkEnd1_LinkID,        


  LinkEnd1.LinkID AS LinkEnd1_LinkID,        
  LinkEnd2.LinkID AS LinkEnd2_LinkID,        

  LinkEnd1.band AS band,        
  
  LinkEnd1.bitrate_Mbps AS bitrate_Mbps,  
  LinkEnd1.tx_freq_MHz AS tx_freq_MHz, 
  LinkEnd1.rx_freq_MHz AS rx_freq_MHz, 


/*
-- ��������������
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd1.kratnostBySpace) AS LinkEnd1_kratnostBySpace, 
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd2.kratnostBySpace) AS LinkEnd2_kratnostBySpace, 

*/

ant1.diameter AS antenna1_diameter, 
ant2.diameter AS antenna2_diameter, 

ant1.height AS antenna1_height, 
ant2.height AS antenna2_height, 

ant1.polarization AS antenna1_polarization, 
ant2.polarization AS antenna2_polarization, 

  
        
  dbo.ClutterModel.name AS Clutter_Model_Name, 
        
  dbo.LinkType.name AS LinkType_Name, 
  dbo.Status.name AS status_name, 
  dbo.Status.name AS State_name, 
  
  
    dbo.fn_PickList_GetValueByCode('link', 'status',   dbo.Link.status) AS status_str, 
  
-- dbo.Status.name AS status_str, 


 -- link_status_tbl.value as status_name, 
--  link_status_tbl.value as status_str, 
      
  
  
/*        
  dbo.fn_PickList_GetValueByCode('link', 'status',   dbo.Link.status) AS status_str, 
  dbo.fn_PickList_GetValueByCode('link', 'GST_type',  dbo.Link.calc_method) AS GST_type_name, 
  dbo.fn_PickList_GetValueByCode('link', 'calc_method',  dbo.Link.calc_method) AS calc_method_name, 
  dbo.fn_PickList_GetValueByCode('link', 'LOS_status',  dbo.Link.LOS_status) AS LOS_status_name, 
  dbo.fn_PickList_GetValueByCode('link', 'terrain_type',  dbo.Link.terrain_type) AS terrain_type_name, 
  dbo.fn_PickList_GetValueByCode('link', 'underlying_terrain_type', dbo.Link.underlying_terrain_type) AS underlying_terrain_type_name ,
*/

  
  Link_Repeater.id as Link_Repeater_id,
  
  LinkEndType.name as LinkEndType_name
        
        
FROM         dbo.LinkEnd LinkEnd2 

              RIGHT OUTER JOIN dbo.LinkEnd LinkEnd1 
              
              RIGHT OUTER JOIN dbo.LinkEndType ON LinkEndType.id = LinkEnd1.LinkEndType_id  
              RIGHT OUTER JOIN dbo.Link ON LinkEnd1.id = dbo.Link.linkend1_id ON LinkEnd2.id = dbo.Link.linkend2_id 
              LEFT OUTER JOIN dbo.Status ON dbo.Link.status_id = dbo.Status.id 
              LEFT OUTER JOIN dbo.GeoRegion GeoRegion_2 
              RIGHT OUTER JOIN dbo.Property Property2 ON GeoRegion_2.id = Property2.georegion_id ON LinkEnd2.Property_id = Property2.id 
              LEFT OUTER JOIN dbo.GeoRegion GeoRegion_1 
              RIGHT OUTER JOIN dbo.Property Property1 ON GeoRegion_1.id = Property1.georegion_id ON LinkEnd1.Property_id = Property1.id 
              LEFT OUTER JOIN dbo.LinkType ON dbo.Link.linkType_id = dbo.LinkType.id LEFT OUTER JOIN dbo.ClutterModel ON dbo.Link.clutter_model_id = dbo.ClutterModel.id
                 
               left OUTER JOIN dbo.Link_Repeater ON dbo.Link.id =  dbo.Link_Repeater.Link_ID
              
                 
               OUTER apply [dbo].fn_Linkend_Antenna_info(LinkEnd1.id) ant1
               OUTER apply [dbo].fn_Linkend_Antenna_info(LinkEnd2.id) ant2
               
              
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND1_ID) A1
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND2_ID) A2

   --  left OUTER JOIN [lib].ft_PickList ('link', 'status')       as link_status_tbl        on link_status_tbl.id = Link.status


--where Link.id=240806

              
   /*  
     left OUTER JOIN [lib].ft_PickList ('link', 'status')       as link_status_tbl        on link_status_tbl.id = Link.status
     left OUTER JOIN [lib].ft_PickList ('link', 'GST_type')     as link_GST_type_tbl      on link_GST_type_tbl.id = Link.GST_type
     left OUTER JOIN [lib].ft_PickList ('link', 'calc_method')  as link_calc_method_tbl   on link_calc_method_tbl.id = Link.calc_method
     left OUTER JOIN [lib].ft_PickList ('link', 'LOS_status')   as link_LOS_status_tbl    on link_LOS_status_tbl.id = Link.LOS_status

     left OUTER JOIN [lib].ft_PickList ('link', 'terrain_type') as link_terrain_types_tbl on link_terrain_types_tbl.id = Link.terrain_type

     left OUTER JOIN [lib].ft_PickList ('link', 'underlying_terrain_type') as link_underlying_terrain_type_tbl on link_underlying_terrain_type_tbl.id = Link.underlying_terrain_type

     left OUTER JOIN [lib].ft_PickList ('linkend', 'kratnostBySpace') as linkend_kratnostBySpace1_tbl on linkend_kratnostBySpace1_tbl.id = LinkEnd1.kratnostBySpace
     left OUTER JOIN [lib].ft_PickList ('linkend', 'kratnostBySpace') as linkend_kratnostBySpace2_tbl on linkend_kratnostBySpace2_tbl.id = LinkEnd2.kratnostBySpace
*/

/*
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd1.kratnostBySpace) AS LinkEnd1_kratnostBySpace, 
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd2.kratnostBySpace) AS LinkEnd2_kratnostBySpace, 
*/
  
--where Link.id=5591
             
               
--WHERE     (dbo.Link.objname = 'link') or  (Link.objname is null)

GO
/****** Object:  View [dbo].[view_Link_report]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[view_Link_report]') IS  not  NULL


DROP VIEW [dbo].[view_Link_report]
GO
/****** Object:  View [dbo].[view_Link_report]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_Link_report]
AS
SELECT     dbo.Link.*, 


Round(100-dbo.Link.Kng,2) as Reliability,


a1.lat AS lat1, 
a1.lon AS lon1, 
a2.lat AS lat2, 
a2.lon AS lon2, 




/*Property1.georegion_id AS georegion1_id, 
Property2.georegion_id AS georegion2_id,
*/


-- Property1
Property1.address AS Property1_address, 
Property2.address AS Property2_address,

Property1.name AS Property1_name, 
Property2.name AS Property2_name, 

--Property1.name AS [Property1_name], 
--Property2.name AS [Property2_name], 

--Property1.id AS Property1_id, 
--Property2.id AS Property2_id, 


Property1.code AS Property1_code, 
Property2.code AS Property2_code, 


Property1.ground_height AS Property1_ground_height, 
Property2.ground_height AS Property2_ground_height,

GeoRegion_1.name AS [Property1_GeoRegion_name], 
GeoRegion_2.name AS [Property2_GeoRegion_name], 


Property1.Name_ERP AS [Property1_name_ERP], 
Property1.name_SDB AS [Property1_name_SDB], 

    

  --LinkEnd1.Band, -- AS LinkEnd1_LinkID,        


  LinkEnd1.LinkID AS LinkEnd1_LinkID,        
  LinkEnd2.LinkID AS LinkEnd2_LinkID,        

  LinkEnd1.band AS band,        
  
  LinkEnd1.bitrate_Mbps AS bitrate_Mbps,  
  LinkEnd1.tx_freq_MHz AS tx_freq_MHz, 
  LinkEnd1.rx_freq_MHz AS rx_freq_MHz, 


/*
-- ��������������
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd1.kratnostBySpace) AS LinkEnd1_kratnostBySpace, 
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd2.kratnostBySpace) AS LinkEnd2_kratnostBySpace, 

*/

ant1.diameter AS antenna1_diameter, 
ant2.diameter AS antenna2_diameter, 

ant1.height AS antenna1_height, 
ant2.height AS antenna2_height, 

ant1.polarization AS antenna1_polarization, 
ant2.polarization AS antenna2_polarization, 

  
        
  dbo.ClutterModel.name AS Clutter_Model_Name, 
        
  dbo.LinkType.name AS LinkType_Name, 
--  dbo.Status.name AS status_name, 
  dbo.Status.name AS State_name, 

--  dbo.Status.name AS status_str, 

      
link_status_tbl.value as status_str, 
 
link_calc_method_tbl.value as calc_method_name, 


--     left OUTER JOIN [lib].ft_PickList ('link', 'calc_method')  as link_calc_method_tbl   on link_calc_method_tbl.id = Link.calc_method

 
  
/*        
  dbo.fn_PickList_GetValueByCode('link', 'status',   dbo.Link.status) AS status_str, 
  dbo.fn_PickList_GetValueByCode('link', 'GST_type',  dbo.Link.calc_method) AS GST_type_name, 
  dbo.fn_PickList_GetValueByCode('link', 'calc_method',  dbo.Link.calc_method) AS calc_method_name, 
  dbo.fn_PickList_GetValueByCode('link', 'LOS_status',  dbo.Link.LOS_status) AS LOS_status_name, 
  dbo.fn_PickList_GetValueByCode('link', 'terrain_type',  dbo.Link.terrain_type) AS terrain_type_name, 
  dbo.fn_PickList_GetValueByCode('link', 'underlying_terrain_type', dbo.Link.underlying_terrain_type) AS underlying_terrain_type_name ,
*/

  
  Link_Repeater.id as Link_Repeater_id,
  
  LinkEndType.name as LinkEndType_name,
  
  Link_calc_res.Rx_LEVEL_str,
  Link_calc_res.Rx_LEVEL_back_str,
  
  
  --    Rx_Level_draft = Param_33 + Param_35 + Param_36 - Param_37 + Param_40 

Site2_dop_gain,

  Link_calc_res.Rx_LEVEL_draft,
  Link_calc_res.Rx_LEVEL_dop_draft,
  

  cast(Link_calc_res.Rx_LEVEL_draft  as varchar(10)) + 
     COALESCE(' / ' + Cast(Link_calc_res.Rx_LEVEL_dop_draft as varchar(10)), '') as Rx_LEVEL_draft_str
  
        
/*  
     Rx_LEVEL     float, --<ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_dop float, --<ITEM id="179" value="-200" caption=" ������� �������� ������� �� ����� ��������� (���) [dBm]"/>
    
    Rx_LEVEL_back     float, --<ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_back_dop float, --<ITEM id="179" value="-200" caption=" ������� �������� ������� �� ����� ��������� (���) [dBm]"/>


    Rx_LEVEL_str varchar(50), --<ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    Rx_LEVEL_back_str varchar(50), --<ITEM id="101" value="-29.8908" caption=" ������� �������� ������� �� ����� ��������� (����.) [dBm]"/>
    
    SESR float,
    KNG  float
*/
  
  
  
        
FROM         dbo.LinkEnd LinkEnd2 

              RIGHT OUTER JOIN dbo.LinkEnd LinkEnd1 
              
      RIGHT OUTER JOIN dbo.LinkEndType ON LinkEndType.id = LinkEnd1.LinkEndType_id  
      RIGHT OUTER JOIN dbo.Link ON LinkEnd1.id = dbo.Link.linkend1_id ON LinkEnd2.id = dbo.Link.linkend2_id 
      LEFT OUTER JOIN dbo.Status ON dbo.Link.status_id = dbo.Status.id 
      LEFT OUTER JOIN dbo.GeoRegion GeoRegion_2 
      RIGHT OUTER JOIN dbo.Property Property2 ON GeoRegion_2.id = Property2.georegion_id ON LinkEnd2.Property_id = Property2.id 
      LEFT OUTER JOIN dbo.GeoRegion GeoRegion_1 
      RIGHT OUTER JOIN dbo.Property Property1 ON GeoRegion_1.id = Property1.georegion_id ON LinkEnd1.Property_id = Property1.id 
      LEFT OUTER JOIN dbo.LinkType ON dbo.Link.linkType_id = dbo.LinkType.id LEFT OUTER JOIN dbo.ClutterModel ON dbo.Link.clutter_model_id = dbo.ClutterModel.id
                 
       left OUTER JOIN dbo.Link_Repeater ON dbo.Link.id =  dbo.Link_Repeater.Link_ID
              
                 
       OUTER apply fn_Linkend_Antenna_info(LinkEnd1.id) ant1
       OUTER apply fn_Linkend_Antenna_info(LinkEnd2.id) ant2
               
              
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND1_ID) A1
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND2_ID) A2
              
     
     left OUTER JOIN [lib].ft_PickList ('link', 'status')       as link_status_tbl        on link_status_tbl.id = Link.status
     left OUTER JOIN [lib].ft_PickList ('link', 'GST_type')     as link_GST_type_tbl      on link_GST_type_tbl.id = Link.GST_type
     left OUTER JOIN [lib].ft_PickList ('link', 'calc_method')  as link_calc_method_tbl   on link_calc_method_tbl.id = Link.calc_method
     left OUTER JOIN [lib].ft_PickList ('link', 'LOS_status')   as link_LOS_status_tbl    on link_LOS_status_tbl.id = Link.LOS_status

     left OUTER JOIN [lib].ft_PickList ('link', 'terrain_type') as link_terrain_types_tbl on link_terrain_types_tbl.id = Link.terrain_type

     left OUTER JOIN [lib].ft_PickList ('link', 'underlying_terrain_type') as link_underlying_terrain_type_tbl on link_underlying_terrain_type_tbl.id = Link.underlying_terrain_type

     left OUTER JOIN [lib].ft_PickList ('linkend', 'kratnostBySpace') as linkend_kratnostBySpace1_tbl on linkend_kratnostBySpace1_tbl.id = LinkEnd1.kratnostBySpace
     left OUTER JOIN [lib].ft_PickList ('linkend', 'kratnostBySpace') as linkend_kratnostBySpace2_tbl on linkend_kratnostBySpace2_tbl.id = LinkEnd2.kratnostBySpace
     
     
    OUTER apply link.[ft_Link_calc_results_basic] (Link.id) Link_calc_res


/*
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd1.kratnostBySpace) AS LinkEnd1_kratnostBySpace, 
dbo.fn_PickList_GetValueByCode('linkend', 'kratnostBySpace', LinkEnd2.kratnostBySpace) AS LinkEnd2_kratnostBySpace, 
*/
  
--where Link.id=253557
             
               
--WHERE     dbo.Link.id =  1683

GO
/****** Object:  StoredProcedure [dbo].[sp_Map_XREF_Update]    Script Date: 16.05.2020 10:54:58 ******/


GO
/****** Object:  StoredProcedure [dbo].[sp_MapDesktop_ObjectMap_set_default]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[sp_MapDesktop_ObjectMap_set_default]') IS  not  NULL

DROP PROCEDURE [dbo].[sp_MapDesktop_ObjectMap_set_default]
GO
/****** Object:  StoredProcedure [dbo].[sp_MapDesktop_ObjectMap_set_default]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  

copy checked,label_style   
--------------------------------------------------------------- */

CREATE PROCEDURE [dbo].[sp_MapDesktop_ObjectMap_set_default]
(
  @ID INT 
)
AS
BEGIN    
  UPDATE Map_Objects
    set  
      checked     = m.checked,
      label_style = m.label_style,
      style       = m.style
      
    from 
      (select * from Map_XREF WHERE id=@ID) m     
    WHERE 
      (Map_Objects.name = m.MapObject_name)      
      
    
  RETURN @@ROWCOUNT      
END

GO
/****** Object:  StoredProcedure [dbo].[sp_MapDesktop_Select_ObjectMaps]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[dbo].[sp_MapDesktop_Select_ObjectMaps]') IS  not  NULL

DROP PROCEDURE [dbo].[sp_MapDesktop_Select_ObjectMaps]
GO
/****** Object:  StoredProcedure [dbo].[sp_MapDesktop_Select_ObjectMaps]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE [dbo].[sp_MapDesktop_Select_ObjectMaps]
(
  @ID  			INT = null,
  @CHECKED 	bit =null  
)
AS
BEGIN

if @ID is null
  set @ID = (select top 1 id from Map_Desktops)


  ---------------------------------------------
  -- insert new maps
  ---------------------------------------------    

 /*  SELECT id 
   	  INTO #new_maps
--    FROM [CalcMap]*/


  SELECT MapObject_name 
   	  INTO #current_maps
   FROM Map_XREF 
   where (Map_Desktop_id=@ID) and (MapObject_name is not null)
  
/*
   SELECT id 
   	  INTO #new_maps
--    FROM [Map]
    FROM [view_MapFile_ByHostName]
    WHERE (project_id=@project_id) and
          (id NOT IN  (SELECT map_ID FROM #current_maps))*/
          
/*          
              SELECT map_ID 
                 FROM Map_XREF 
                 where (Map_Desktop_id=@ID) and (map_id>0) 
             ))
*/ 
       
 -- IF EXISTS(SELECT * FROM #new_maps) 
 -- BEGIN

--fixed
if exists(SELECT * FROM Map_Desktops where id=@ID )

    INSERT into Map_XREF 
        (Map_Desktop_id, 	
         MapObject_name, 
         priority, 
         checked,
         label_style,
         style,
         
         AutoLabel
           
         ) --zoom_min, zoom_max, allow_zoom, 
    SELECT 
         @ID, 						
         name, 
         priority, 
         checked,
         label_style,
         style,
         
         
          case when name='property' then  1 else null end 
         
           --zoom_min, zoom_max, allow_zoom, 
         
     FROM [Map_Objects]
     WHERE 
     	name NOT IN (SELECT MapObject_name FROM #current_maps  )  
    
      
 -- END



  DROP table #current_maps
 -- DROP table #new_maps



  ---------------------------------------------
  -- SELECT
  ---------------------------------------------    

  SELECT * FROM VIEW_MapDesktop_ObjectMaps
    WHERE (Map_Desktop_id = @ID)        
            and    
          ((@CHECKED is NULL) or (IsNull(checked,0)=1))
--          ((@CHECKED is NULL) or (IsNull(checked,1)=1))
    order by priority
  
  RETURN @@ROWCOUNT
               
END

GO
/****** Object:  View [reports].[view_LinkEnd_report]    Script Date: 16.05.2020 10:54:58 ******/




---------------------------------------------------
IF OBJECT_ID(N'[dbo].[view_LinkEnd_report]') IS  not  NULL

DROP VIEW [dbo].[view_LinkEnd_report]
GO

/*---

*/
CREATE VIEW dbo.view_LinkEnd_report
AS
SELECT L.*,
         dbo.LinkEndType.name AS [LinkEndType_name],

         dbo.LinkEndType.name AS [LinkEndType.name],
         
         dbo.LinkEndType_Mode.radiation_class AS radiation_class,
         
         --��ਭ� ������, ���
         dbo.LinkEndType_Mode.bandwidth AS bandwidth,
         
      ant.height  AS antenna_height,
      ant.diameter  AS antenna_diameter,
      ant.polarization  AS antenna_polarization,

      ant.gain  AS antenna_gain,
      
      ant.vert_width  AS antenna_vert_width,
      ant.horz_width  AS antenna_horz_width,

      ant.loss  AS antenna_loss,
      ant.tilt  AS antenna_tilt,

      ant.ground_height  AS antenna_ground_height,
         
/*         
         dbo.fn_Linkend_Antenna_GetValue(L.id, 'height') AS antenna_height,
         dbo.fn_Linkend_Antenna_GetValue(L.id, 'diameter') AS antenna_diameter,
         dbo.fn_Linkend_Antenna_GetValue(L.id, 'polarization') AS   antenna_polarization,
         dbo.fn_Linkend_Antenna_GetValue(L.id, 'gain') AS antenna_gain,
         dbo.fn_Linkend_Antenna_GetValue(L.id, 'vert_width') AS  antenna_vert_width,
         dbo.fn_Linkend_Antenna_GetValue(L.id, 'horz_width') AS   antenna_horz_width,

         dbo.fn_Linkend_Antenna_GetValue(L.id, 'loss') AS   antenna_loss,
         dbo.fn_Linkend_Antenna_GetValue(L.id, 'tilt') AS   antenna_tilt,
         */

         dbo.fn_PickList_GetValueByCode('linkend', 'redundancy', L.redundancy)    AS redundancy_str,

    --     dbo.fn_Linkend_Antenna_GetValue(L.id, 'ground_height') AS  antenna_ground_height,
         
         dbo.fn_dBm_to_Wt(L.power_dBm) AS power_Wt,          
         
         
         
         L.tx_freq_MHz / 1000 AS tx_freq_GHz,
         
--         dbo.fn_LinkEnd_Azimuth(L.id) AS Azimuth1
         dbo.fn_LinkEnd_Azimuth(L.id, NULL) AS antenna_Azimuth,
         
         (SELECT MAX(power_max) FROM LinkEndType_Mode mode WHERE mode.id=L.LinkEndType_id) as power_max1
                  
         
         
  FROM dbo.LinkEnd L
       LEFT OUTER JOIN dbo.LinkEndType_Mode ON L.LinkEndType_mode_id =
       dbo.LinkEndType_Mode.id
       LEFT OUTER JOIN dbo.LinkEndType ON L.linkendtype_id = dbo.LinkEndType.id
       
      OUTER apply fn_Linkend_Antenna_info (L.id) ant 
       
go       
     --  where l.id= 186419
     
  /*   
       ant.diameter  AS antenna_diameter,
      ant.height  AS antenna_height,
      ant.polarization  AS antenna_polarization,

LinkEndType_mode.bandwidth                      


                      
                      
FROM         dbo.LinkEnd L LEFT OUTER JOIN
          dbo.Passive_Component ON L.passive_element_id = dbo.Passive_Component.id LEFT OUTER JOIN
          dbo.LinkEndType_mode ON L.linkendtype_mode_id = dbo.LinkEndType_mode.id LEFT OUTER JOIN
          dbo.LinkEndType ON L.linkendtype_id = dbo.LinkEndType.id LEFT OUTER JOIN
          dbo.Property ON L.property_id = dbo.Property.id
                      
                      
          cross apply fn_Linkend_Link_info(L.id) link
          cross apply fn_Linkend_Antenna_info (L.id) ant
          */
---------------------------------------------------



/*

*/

GO

delete from  lib.link_status 
GO

  
--SET IDENTITY_INSERT  lib.link_status   ON
GO    


INSERT INTO  lib.link_status (id, name)
SELECT 1,N'��������'
UNION ALL SELECT 2,N'�� ��������'

GO

--SET IDENTITY_INSERT  lib.link_status   OFF
GO    


update Linkend_Antenna
  set height=1 
  where IsNull(height,0) <1
go


exec sp_MSforeachtable @command1="print '?'", @command2="ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"
GO
--  where table_name_initial is not null

update _objects
  set table_name_initial = table_name
  where table_name_initial is null

go

--------------------------------------------------------
update [_objects] 
  set view_name='view_property_' where name='property'


update [_objects] 
  set table_name_initial='link' where name='link'

update [_objects] 
  set table_name_initial='linkend' where name='linkend'

update [_objects] 
  set table_name_initial='linkend' -- , table_name='linkend'
  where name in ( 'pmp_sector', 'PMP_Terminal')

update [_objects] 
  set table_name='view_pmp_sector' , view_name='view_pmp_sector'
  where name in ( 'pmp_sector')

update [_objects] 
  set table_name='view_PMP_Terminal'  , view_name='view_PMP_Terminal'  
  where name in ( 'PMP_Terminal')

update [_objects] 
  set IsUseProjectID=1
  where name in ('Linkend', 'PMP_site', 'PMP_Terminal')



------------------------
update [_object_fields]  set XREF_TableName='CellLayer' where  XREF_TableName='Cell_layer'


update [_object_fields]  set Name='Terminal_Sensitivity_dbm' where  Name='Terminal_Sensitivity'
update [_object_fields]  set Name='TERMINAL_GAIN_db' where  Name='TERMINAL_GAIN'
update [_object_fields]  set Name='Terminal_Loss_db' where  Name='Terminal_Loss'




update _object_fields
  set items='1+0
1+1
2+0 XPIC
2+2 XPIC'
  where  name='kratnostBySpace'


update [_objects] 
  set table_name_initial='rel_matrix' where name='relief'

go


SET IDENTITY_INSERT  _objects   ON
GO    


delete from _objects where id in (184,187  )
delete from _object_fields where object_id in (184,187  )


GO
INSERT [dbo].[_objects] ([id], [name], [display_name], [table_name], [IsUseProjectID], [folder_name], [view_name], [table_name_initial]) VALUES (187, N'template_link', NULL, N'lib.template_link', NULL, NULL, NULL, N'lib.template_link')
go

INSERT [dbo].[_objects] ([id], [name], [display_name], [table_name], [IsUseProjectID], [folder_name], [view_name], [table_name_initial]) VALUES (184, N'link_sdb', N'�� SDB', N'link_sdb', 1, NULL, NULL, N'link_sdb')
GO

SET IDENTITY_INSERT  _objects   OFF
GO    



GO


INSERT [_object_fields] ([object_id], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [is_folder]) VALUES (187, NULL, 1, N'name', NULL, NULL, N'��������', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [_object_fields] ([object_id], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [is_folder]) VALUES (187, NULL, 2, N'LinkEndType_ID', NULL, N'xref', N'������������ ���', NULL, NULL, NULL, 1, NULL, NULL, N'LinkEndType', N'LinkEndType', N'LinkEndType_name', 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [_object_fields] ([object_id], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [is_folder]) VALUES (187, NULL, 5, N'height', NULL, NULL, N'������ �������', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [_object_fields] ([object_id], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [is_folder]) VALUES (187, NULL, 4, N'antennaType_id', NULL, N'xref', N'������ �������', NULL, NULL, NULL, 1, NULL, NULL, N'antenna_Type', N'antennaType', N'antennaType_name', 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [_object_fields] ([object_id], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [is_folder]) VALUES (187, NULL, 6, N'LinkEndType_Mode_ID', NULL, NULL, N'LinkEndType_Mode_ID', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL)
GO
INSERT [_object_fields] ([object_id], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [is_folder]) VALUES (187, NULL, 3, N'mode', NULL, N'button', N'�����', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, 0, NULL)
GO






GO
INSERT [dbo].[_object_fields] ( [object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, NULL, NULL, 1, N'name', NULL, NULL, N'��������', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[_object_fields] ([object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, 1, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[_object_fields] ([object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, NULL, NULL, 3, N'SESR_required', NULL, NULL, N'SESR ���� [%]', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[_object_fields] ([object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, NULL, NULL, 1, N'SESR', NULL, NULL, N'SESR ���� [%]', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[_object_fields] ([object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, NULL, NULL, 4, N'Kng_required', NULL, NULL, N'��� ���� [%]', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[_object_fields] ([object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, NULL, NULL, 1, N'Kng', NULL, NULL, N'��� ���� [%]', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[_object_fields] ([object_id], [is_folder], [parent_id], [index_], [name], [alias], [type], [caption], [is_readonly], [hidden], [sorted], [enabled], [hint], [items], [xref_ObjectName], [xref_TableName], [xref_FieldName], [is_in_list], [is_in_view], [column_caption], [column_index], [show_button], [isGroupAssignmentEnabled], [status_name], [view_fieldname], [IsMapCriteria], [is_user_column], [is_use_on_map]) VALUES ( 184, NULL, NULL, 2, N'kng_year', NULL, NULL, N'��� ���� [���/���]', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO 


--================================================================================
-------exec history.sp_Create_triggers_all 
go



--EXEC  dbo._UpdateAllViews ;
--go


--update _db_version_
--set version='2020.01.18'
--go


