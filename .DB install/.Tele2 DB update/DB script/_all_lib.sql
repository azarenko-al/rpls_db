
IF OBJECT_ID(N'[lib].[view_Lookup_Bitrate]') IS NOT NULL
DROP view [lib].[view_Lookup_Bitrate]
GO

CREATE VIEW lib.view_Lookup_Bitrate
AS
  select distinct bitrate_Mbps
  from LinkendType_mode

go




/****** Object:  UserDefinedFunction [lib].[ft_AntennaType_mask_SEL]    Script Date: 16.05.2020 10:54:58 ******/
  IF OBJECT_ID(N'[lib].[ft_AntennaType_mask_SEL]') IS  not  NULL

DROP FUNCTION [lib].[ft_AntennaType_mask_SEL]
GO
/****** Object:  UserDefinedFunction [lib].[ft_AntennaType_mask_SEL]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE FUNCTION [lib].[ft_AntennaType_mask_SEL] 
(
  @ID int  ,
  @MASK_TYPE varchar(10) --horz,vert
 
)

--346447
--RETURNS varchar(50)

RETURNS @ret TABLE 
(  
    angle float,
    loss  float     
)

as

BEGIN

--set @ID=16683

--  select * from AntennaType   where  id  = 16683 


------------------------------------------------
-- test
------------------------------------------------
if @ID is null 
begin
  select top 1 @ID = id   from AntennaType   where horz_mask is not null
   
  set  @MASK_TYPE = 'vert'
end    
------------------------------------------------

 -- declare 
 -- 	@result varchar(50);
   


  DECLARE
    @mask varchar(max), -- ='0=32;1=32;2.5=48;5=51.5;15=54;23=62;33=63;36=66;60=67;75=74;180=74;285=74;300=67;324=66;327=63;337=62;345=54;355=51.5;357.5=48;359=32;',
    @xml XML


select @mask =  
   case
     when @MASK_TYPE= 'horz' then horz_mask
     when @MASK_TYPE= 'vert' then vert_mask
   end  

  from AntennaType 
  where id=@id



--set @mask = '0,0=38,0;4,0=38,0;10,0=52,0;15,0=55,0;22,0=55,0;25,0=57,0;48,0=57,0;60,0=60,0;72,0=60,0;85,0=66,0;180,0=66,0;275,0=66,0;288,0=60,0;300,0=60,0;312,0=57,0;335,0=57,0;338,0=55,0;345,0=55,0;350,0=52,0;356,0=38,0'

set @mask = replace (@mask, ',','.')


  if right(@mask, 1) = ';'
    set @mask = substring(@mask, 1, len(@mask) - 1 )

      
      
  set  @mask = LTrim(RTrim( @mask))  
  set  @mask = replace (@mask, '=', '" loss="')  
  set  @mask = replace (@mask, ';', '"/><item angle="')  

  set  @mask = '<item angle="'+  @mask + '"/>'

 -- print @mask 

  set  @xml = CONVERT (xml, @mask  )

  insert into @ret
   select * from 
     (
      SELECT  
         Tbl.Col.value('@angle', 'float') as angle,  
         Tbl.Col.value('@loss', 'float')  as loss 
            
      FROM   @xml.nodes('//item') Tbl(Col)  
     ) m 
     order by angle

	RETURN
  
END

GO
/****** Object:  StoredProcedure [lib].[ft_AntennaType_restore_from_mask]    Script Date: 16.05.2020 10:54:58 ******/

  IF OBJECT_ID(N'[lib].[ft_AntennaType_restore_from_mask]') IS  not  NULL
DROP PROCEDURE [lib].[ft_AntennaType_restore_from_mask]
GO
/****** Object:  StoredProcedure [lib].[ft_AntennaType_restore_from_mask]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--CREATE PROCEDURE lib.ft_AntennaType_restore_polarization_ratio_from_vert_mask
CREATE PROCEDURE [lib].[ft_AntennaType_restore_from_mask]
(
  @ID int
)
AS
BEGIN

---------------------------------------------
-- test
---------------------------------------------
if @ID is null 
begin
set @id = 18553

--     select top 1 @ID = id  from AntennaType  where vert_mask is not null

--  select top 1 * from AntennaType  where ID = @id 

  
end 
/*
declare 
  @loss_vert float,
  @loss_horz float


select top 1 @loss_vert = loss from lib.ft_AntennaType_mask_SEL (@ID,'vert') where angle=0  
select top 1 @loss_horz = loss from lib.ft_AntennaType_mask_SEL (@ID,'horz') where angle=0  

print @loss_vert
print @loss_horz
*/

/*
select * from lib.ft_AntennaType_mask_SEL (@ID,'vert')
select * from lib.ft_AntennaType_mask_SEL (@ID,'horz') 
*/

  UPDATE AntennaType
    set 
      polarization_ratio =
       
            (select top 1 loss from lib.ft_AntennaType_mask_SEL (@ID,'vert') where angle=0) +
            (select top 1 loss from lib.ft_AntennaType_mask_SEL (@ID,'horz') where angle=0)
            
 
   where ID=@ID
       
END

GO
/****** Object:  StoredProcedure [lib].[sp_Template_Link_Add]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[lib].[sp_Template_Link_Add]') IS NOT NULL

DROP PROCEDURE [lib].[sp_Template_Link_Add]
GO
/****** Object:  StoredProcedure [lib].[sp_Template_Link_Add]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE [lib].[sp_Template_Link_Add]
(
  @NAME VARCHAR(100)      
)
AS
BEGIN
  declare 
    @id int 

  INSERT INTO Template_Link  (NAME) VALUES (@NAME);     
  
  set @id = IDENT_CURRENT ('lib.Template_Link')   

  RETURN @id
 
  
END

GO







IF OBJECT_ID(N'lib.Modulations') IS  NULL

CREATE TABLE lib.Modulations (
  id int IDENTITY(1, 1) NOT NULL,
  NAME varchar(50) COLLATE Cyrillic_General_CI_AS NOT NULL,
  level_count float NULL,
  GOST_53363_modulation varchar(50) COLLATE Cyrillic_General_CI_AS NULL,
  PRIMARY KEY CLUSTERED (id)
    WITH (
      PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF,
      ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
ON [PRIMARY]
GO



IF OBJECT_ID(N'dbo.sp_Template_Linkend_Antenna_Add') IS not  NULL
  DROP PROCEDURE dbo.sp_Template_Linkend_Antenna_Add
GO



/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE procedure dbo.sp_Template_Linkend_Antenna_Add
(    
  @NAME 									varchar(50),          
  @TEMPLATE_LINKEND_ID   	int,
  
  @ANTENNATYPE_ID		int =null,     

  @HEIGHT  	float=null, 
  @AZIMUTH 	float=null, 
  @TILT 		float=null,
  @LOSS 	float=null 


)
AS
begin
  ---------------------------------------------
  IF @Name IS NULL  RAISERROR ('@Name is NULL', 16, 1);
  ---------------------------------------------

  DECLARE
  	@new_id int;
  		

  set @HEIGHT=ROUND(@HEIGHT,1)
  set @AZIMUTH=ROUND(@AZIMUTH,1)
  

  INSERT INTO Template_Linkend_Antenna
     (NAME, 
      TEMPLATE_LINKEND_ID,
      ANTENNATYPE_ID,  
    
      HEIGHT, AZIMUTH, TILT,
      LOSS
    
      )
  VALUES
     (@NAME, 
      @TEMPLATE_LINKEND_ID,
     	@ANTENNATYPE_ID,  
     
      @HEIGHT, @AZIMUTH, @TILT,
      @LOSS     
      ) 
  
  
    
  set @new_id = IDENT_CURRENT ('Template_Linkend_Antenna')
   
  
 -- set @new_id = @@IDENTITY;

/*  IF IsNull(@ANTENNATYPE_ID,0)>0
    exec sp_Object_Update_AntType 
    	@OBJNAME='Template_Linkend_Antenna',
      @ID=@new_id,
      @AntennaType_ID=@ANTENNATYPE_ID;
 */ 

  RETURN @new_id;
     
        
end

go


IF OBJECT_ID(N'lib.sp_CalcModel_Del') IS not  NULL
  DROP PROCEDURE lib.sp_CalcModel_Del
GO

/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE lib.sp_CalcModel_Del
(
  @ID INT  -- CalcMap ID
)
AS
BEGIN
 
  update Pmp_CalcRegion
    set Calc_Model_id = null
    where Calc_Model_id = @id
  
  update Template_LinkEnd
    set Calc_Model_id = null
    where Calc_Model_id = @id

  update LinkEnd
    set Calc_Model_id = null
    where Calc_Model_id = @id


  --DELETE FROM Map_XREF WHERE CalcMap_id =@ID

  DELETE FROM CalcModel WHERE  id=@ID;
  
--  select * from  CalcMap WHERE  id=@ID;
  
  
 RETURN @@ROWCOUNT
    

END
go
------------------------------------------------------------------
IF OBJECT_ID(N'dbo.sp_Object_Update_LinkEndType_Mode') IS not  NULL
  DROP PROCEDURE dbo.sp_Object_Update_LinkEndType_Mode  
GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */

CREATE PROCEDURE dbo.sp_Object_Update_LinkEndType_Mode  
(
  @OBJNAME	varchar(50),
  @ID   	int,    
  @LinkEndType_Mode_ID 	int   
)
AS
BEGIN
  IF @OBJNAME IS NULL  RAISERROR ('@OBJNAME is NULL', 16, 1);
  IF @ID IS NULL  RAISERROR ('@ID is NULL', 16, 1);



---------------------------------------------
  IF @OBJNAME in ('LinkEnd','pmp_sector','pmp_terminal')
---------------------------------------------
  BEGIN
       
    IF EXISTS(SELECT * FROM view_LinkEndType_Mode WHERE id = @LinkEndType_Mode_ID)
    BEGIN
      UPDATE LinkEnd SET  
        LinkEndType_Mode_ID = m.ID,
        Mode 			= m.mode,
        bitrate_Mbps    = m.bitrate_Mbps,
                 
        MODULATION_TYPE =m.MODULATION_TYPE,
        SIGNATURE_HEIGHT=m.SIGNATURE_HEIGHT, 
        SIGNATURE_WIDTH =m.SIGNATURE_WIDTH,  
        THRESHOLD_BER_3 =m.THRESHOLD_BER_3,  
        THRESHOLD_BER_6 =m.THRESHOLD_BER_6,  
            
        channel_width_MHz = m.bandwidth, -- //_MHz,  
        
        POWER_dBm       = m.POWER_MAX,
        
       MODULATION_COUNT      = m.MODULATION_level_COUNT,
       GOST_53363_modulation = m.GOST_53363_modulation,        
        
        Freq_spacing  =  m.bandwidth * IsNull(CHANNEL_SPACING,0) 

        
       from 
          (select * from view_LinkEndType_Mode WHERE id=@LinkEndType_Mode_ID) m     
       WHERE 
         (LinkEnd.id = @ID) 
 
     END 
     
  END ELSE       


  
---------------------------------------------
  IF @OBJNAME='Template_linkend'
---------------------------------------------
  BEGIN
  
    IF EXISTS(SELECT * FROM LinkEndType_Mode WHERE id = @LinkEndType_Mode_ID)
    BEGIN
      UPDATE Template_linkend SET  
        LinkEndType_Mode_ID = m.ID,
        
        
        THRESHOLD_BER_3 =m.THRESHOLD_BER_3,  
        THRESHOLD_BER_6 =m.THRESHOLD_BER_6,
        
        POWER_dBm       = m.POWER_MAX 
        
       from 
          (select * from view_LinkEndType_Mode WHERE id=@LinkEndType_Mode_ID) m     
       WHERE 
         (Template_linkend.id = @ID)     
     END 
     
  END ELSE begin
    RAISERROR ('sp_Object_Update_LinkEndType_Mode  error ', 16, 1);       
    return -1
  END
  
  RETURN @@ROWCOUNT 

END
go
---------------------------------------------------

IF OBJECT_ID(N'dbo.sp_Template_Linkend_Del') IS not  NULL
  DROP PROCEDURE dbo.sp_Template_Linkend_Del
GO




CREATE procedure dbo.sp_Template_Linkend_Del
( 

  @ID	int
)
AS
begin    
  delete from Template_LinkEnd_Antenna    where Template_LinkEnd_ID = @id

  delete from Template_Linkend   where ID = @id
   
  return @@ROWCOUNT
  
end

go




