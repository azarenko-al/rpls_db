/****** Object:  View [Security].[View_User_Security_XREF]    Script Date: 18.01.2021 14:15:25 ******/
IF OBJECT_ID(N'[dbo].[view_PMP_CalcRegion_Pmp_Sectors]') IS NOT NULL
  DROP VIEW [dbo].[view_PMP_CalcRegion_Pmp_Sectors]


GO


CREATE VIEW dbo.view_PMP_CalcRegion_Pmp_Sectors
AS
SELECT DISTINCT dbo.PMP_CalcRegionCells.pmp_calc_region_id,
       
       dbo.PMP_CalcRegionCells.id AS xref_id,
       
       dbo.PMP_CalcRegionCells.checked,
       
       dbo.PMP_CalcRegionCells.pmp_sector_id,
       
       dbo.PMP_site.guid AS [PMP_site_guid],
       
       dbo.PMP_site.name AS PMP_site_name,
       dbo.CellLayer.name AS CellLayer_name,
       dbo.CalcModel.name AS [CalcModel_name],
       
       dbo.LinkEnd.*,
       
       dbo.Property.lat AS lat,
       dbo.Property.lon AS lon,
       dbo.PMP_site.calc_radius_km AS site_calc_radius_km
       
FROM dbo.Property
     RIGHT OUTER JOIN dbo.PMP_site ON dbo.Property.id = dbo.PMP_site.property_id
     RIGHT OUTER JOIN dbo.CellLayer
     RIGHT OUTER JOIN dbo.CalcModel
     RIGHT OUTER JOIN dbo.LinkEnd ON dbo.CalcModel.id =
     dbo.LinkEnd.calc_model_id ON dbo.CellLayer.id =  dbo.LinkEnd.cell_layer_id 
     ON dbo.PMP_site.id =  dbo.LinkEnd.pmp_site_id
     RIGHT OUTER JOIN dbo.PMP_CalcRegionCells ON dbo.LinkEnd.id =  dbo.PMP_CalcRegionCells.pmp_sector_id
     
WHERE (dbo.PMP_CalcRegionCells.pmp_sector_id > 0)

go

/****** Object:  View [Security].[View_User_Security_XREF]    Script Date: 18.01.2021 14:15:25 ******/
IF OBJECT_ID(N'[dbo].[view_PMP_CalcRegion_Pmp_Antenna]') IS NOT NULL
  DROP VIEW [dbo].[view_PMP_CalcRegion_Pmp_Antenna]


GO


CREATE VIEW dbo.view_PMP_CalcRegion_Pmp_Antenna
AS
SELECT DISTINCT 
       dbo.Linkend_Antenna.*,

    dbo.PMP_CalcRegionCells.id AS xref_id,
       dbo.PMP_CalcRegionCells.checked,
       dbo.PMP_CalcRegionCells.pmp_calc_region_id,
       dbo.PMP_CalcRegionCells.antenna_id,
       
       dbo.Linkend.guid AS [Linkend_guid],
       dbo.AntennaType.name AS [AntennaType_name],
       
       dbo.Property.lat AS Property_lat,
       dbo.Property.lon AS Property_lon
       
FROM dbo.Linkend_Antenna
     LEFT OUTER JOIN dbo.Property ON dbo.Linkend_Antenna.property_id =    dbo.Property.id
     LEFT OUTER JOIN dbo.AntennaType ON dbo.Linkend_Antenna.antennaType_id =    dbo.AntennaType.id
     LEFT OUTER JOIN dbo.Linkend ON dbo.Linkend_Antenna.Linkend_id =    dbo.Linkend.id
     RIGHT OUTER JOIN dbo.PMP_CalcRegionCells ON dbo.Linkend_Antenna.id =    dbo.PMP_CalcRegionCells.antenna_id
     
WHERE (dbo.PMP_CalcRegionCells.antenna_id > 0)
----------------------
go




IF OBJECT_ID(N'[dbo].[view_PMP_CalcRegion_Used_Cell_Layers]') IS NOT NULL
  DROP VIEW dbo.view_PMP_CalcRegion_Used_Cell_Layers
GO


CREATE VIEW dbo.view_PMP_CalcRegion_Used_Cell_Layers
AS
SELECT DISTINCT dbo.PMP_CalcRegionCells.pmp_calc_region_id,
       dbo.CellLayer.id,
       dbo.CellLayer.name,
       CAST (1 AS bit) AS checked
FROM dbo.CellLayer
     RIGHT OUTER JOIN dbo.LinkEnd ON dbo.CellLayer.id =
     dbo.LinkEnd.cell_layer_id
     RIGHT OUTER JOIN dbo.PMP_CalcRegionCells ON dbo.LinkEnd.id =
     dbo.PMP_CalcRegionCells.pmp_sector_id
WHERE (dbo.LinkEnd.cell_layer_id > 0)

go

-----------------------------------------------------



IF OBJECT_ID(N'dbo.sp_PMP_CalcRegion_Select_Item') IS NOT NULL
  DROP procedure dbo.sp_PMP_CalcRegion_Select_Item
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_PMP_CalcRegion_Select_Item 
(
	@ID INT 
)    
AS
BEGIN

  if @ID is null
    set @id= (select top 1 id from Pmp_CalcRegion)


  -------------------------------------------------------------------------
--  IF @ID IS NULL  RAISERROR ('@ID is NULL', 16, 1);
  -------------------------------------------------------------------------



--SELECT top 10 * FROM  view_Pmp_CalcRegion_PMP_Sites  


--  DECLARE--
  --  @objname VARCHAR(50);
    	
    		

 -- SET ANSI_WARNINGS OFF --String or binary data would be truncated.


 -- if @ID is null
 --   set @ID = 70365
  
  if @ID is null
  BEGIN
   SELECT top 1  @id =id --, @objname =objname
     FROM pmp_CalcRegion  
  
  print 'test'
  
--    set @ID = 70365
 --   set @LINKEND_ID =124645
  END


 -- SELECT @objname=objname 
 --   FROM pmp_CalcRegion;


  print @ID

  create table [#temp]
      (
       XREF_ID     int NOT NULL,              
       
       ID          int NOT NULL,              
       guid        uniqueidentifier,
       parent_guid uniqueidentifier,
      
       checked 		bit,
       
       name        nVARCHAR(50) NOT NULL,
       objname     VARCHAR(10) NOT NULL,
               
       BAND        VARCHAR(10),
  
      ---------------
      --property
      ---------------       
      property_ID int, 

      property_lat float, 
      property_lon float, 
                  
      property_lat_str VARCHAR(50), 
      property_lon_str VARCHAR(50), 

    
      ---------------
      --PMP_Sector
      ---------------       
       calc_model_Name VARCHAR(50),
       calc_model_id 	Float,
       k0_open 				Float,
       k0_closed 			Float,
       k0 						Float,

       TX_FREQ_MHz    Float,       
       POWER_dBm      Float,    
       POWER_W        Float,
       
       color 		  int,
     
       CellLayer_id 	int,
       CellLayer_name VARCHAR(50),
  
  	   calc_radius_km  float,
    
       THRESHOLD_BER_3 Float,
       THRESHOLD_BER_6 Float,    
       
      ---------------
      --ANTENNA
      ---------------
      AntennaType_ID      int, 
      ANTENNATYPE_NAME    VARCHAR(50),

      DIAMETER            Float,
      HEIGHT              Float,
      HEIGHT_OLD          Float,      
      AZIMUTH             Float, 

      POLARIZATION_str    VARCHAR(4),
      GAIN                Float,
      TILT                Float,
      vert_width          Float,
      horz_width          Float,
      ANTENNA_loss        Float 
                     
      ) ;      

 -- SELECT * FROM #temp   

-- RETURN @@ROWCOUNT
  	  
 
--------------------------------------------------
-- PMP_Site
--------------------------------------------------     
  insert into [#temp]
    (xref_id,
     id,guid,name,objname,
     checked,
      
     property_ID,    
     
     property_lat, 
     property_lon,
     property_lat_str, 
     property_lon_str
     
     )
   select 
     xref_id,
     id,guid,name,'PMP_Site',
     checked,
     
    property_ID,
   
     lat, 
     lon,
     lat_str, 
     lon_str
     
         
   from 
     view_Pmp_CalcRegion_PMP_Sites
   where 
     pmp_Calc_Region_id = @ID
             
     
 -- SELECT * FROM #temp   
     
 -- RETURN @@ROWCOUNT
  
  
-- RETURN
 
 	     
     
--------------------------------------------------
-- PMP_Sector
--------------------------------------------------     
  insert into [#temp]
    (xref_id,
     id,guid,parent_guid,name,objname, 
    
     checked,    
     color,    
     BAND,
    
     POWER_dBm,
     POWER_W,
     TX_FREQ_MHz,
    
     calc_radius_km, 
     calc_model_Name, 
     calc_model_id, 
     k0_open, k0_closed, k0,
     
     CellLayer_id,
     CellLayer_name,
     
     THRESHOLD_BER_3,
     THRESHOLD_BER_6 
  
   --  property_ID        
               
     )
   select 
     xref_id,
     id,guid,[PMP_Site_guid],name,'PMP_Sector',
     
     checked,
     color,
     BAND,
     

     POWER_dBm,
     POWER_W,
     TX_FREQ_MHz,
     
     calc_radius_km,
     
     [CalcModel_Name], 
     calc_model_id, 
     k0_open, k0_closed, k0,
     
     Cell_Layer_id,
     CellLayer_name,
     
     THRESHOLD_BER_3,
     THRESHOLD_BER_6 
     
  --   property_ID       
     
   from 
     view_Pmp_CalcRegion_Pmp_Sectors
   where 
     pmp_Calc_Region_id = @ID
   
   
--   return
    

--------------------------------------------------
-- antennas
--------------------------------------------------    
  insert into [#temp]
    (xref_id,
     checked,
     id,guid,parent_guid,name,objname,
     BAND,
     HEIGHT,
     DIAMETER,AZIMUTH,GAIN,TILT,

     POLARIZATION_str,
     AntennaType_ID,AntennaType_NAME, 
     vert_width,horz_width,
 
     property_ID,
     property_lat, 
     property_lon,
     property_lat_str, 
     property_lon_str
           
      
   --  ANTENNA_loss,
   --  is_master
    )    
   select 
     xref_id,
     checked,
     id,guid,[Linkend_guid],name,'antenna',
     
     BAND,
     HEIGHT, 
     DIAMETER,AZIMUTH,GAIN,TILT,     
     POLARIZATION_str,
     AntennaType_ID,[AntennaType_NAME], 
     vert_width,horz_width,
   
     property_ID,
     property_lat, 
     property_lon,
     
     dbo.FormatDegree(property_lat),
     dbo.FormatDegree(property_lon)

      
  --   loss,
  --   is_master
     
   from view_Pmp_CalcRegion_Pmp_Antenna
   where pmp_Calc_Region_id = @ID


  SELECT * FROM #temp   

  RETURN @@ROWCOUNT

END

go
--------------------------------------------

IF OBJECT_ID(N'dbo.sp_PMP_CalcRegion_Init') IS NOT NULL
  DROP procedure dbo.sp_PMP_CalcRegion_Init
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_PMP_CalcRegion_Init
(
	@ID INT 
)    
AS
BEGIN
  if @ID is null
    set @id=2029




 -- if @ID is null
 --   set @ID = 70365
  
/*  if @ID is null
  BEGIN
    SELECT top 1  @id =id FROM pmp_CalcRegion  
  
--    set @ID = 70365
 --   set @LINKEND_ID =124645
  END
*/

  --------------------------------------------------------
  IF @ID is null  RAISERROR ('@ID is null', 16, 1);
  --------------------------------------------------------

 

  print @ID
  
 -- BEGIN TRANSACTION

  --------------------------------------------------------

  SELECT DISTINCT pmp_site_id INTO #pmp_site  
    FROM pmp_CalcRegionCells
    WHERE (pmp_Calc_Region_id = @ID) and (pmp_site_id>0)


  SELECT DISTINCT pmp_sector_id INTO #pmp_sector 
    FROM pmp_CalcRegionCells
    WHERE (pmp_Calc_Region_id = @ID) and (pmp_sector_id>0)
    
  SELECT Antenna_id INTO #Antenna 
    FROM pmp_CalcRegionCells
    WHERE (pmp_Calc_Region_id = @ID) and (Antenna_id>0)


  --------------------------------------------------------

  SELECT id INTO #pmp_sector_all 
--    FROM PMP_Sector
    FROM LinkEnd
    WHERE pmp_site_id in
           (SELECT pmp_site_id FROM #pmp_site) 
    
  SELECT id INTO #Antenna_all 
    FROM Linkend_Antenna
    WHERE LinkEnd_id in
           (SELECT id FROM #pmp_sector_all) 



  --------------------------------------------------------
    
	INSERT INTO pmp_CalcRegionCells (pmp_Calc_Region_id, pmp_sector_id)   
    SELECT @ID,id  
      FROM #pmp_sector_all
      WHERE id NOT IN 
         (SELECT pmp_sector_id FROM #pmp_sector) 

--return

    
	INSERT INTO pmp_CalcRegionCells (pmp_Calc_Region_id, Antenna_id)   
    SELECT @ID,id  
      FROM #Antenna_all
      WHERE id NOT IN 
         (SELECT Antenna_id FROM #Antenna) 

  --------------------------------------------------------
 
 
/*  SELECT * FROM  #pmp_sector  
  SELECT * FROM  #pmp_site  
*/

  drop table #pmp_site
  drop table #pmp_sector
  drop table #pmp_sector_all
  drop table #Antenna
  drop table #Antenna_all

-- commit TRANSACTION


  SELECT * FROM pmp_CalcRegionCells 
  	WHERE pmp_Calc_Region_id=@ID

END
go

