IF OBJECT_ID(N'[dbo].[view_Template_Linkend_Antenna]') IS NOT NULL

DROP VIEW [dbo].[view_Template_Linkend_Antenna]
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW dbo.view_Template_Linkend_Antenna
AS
SELECT a2.*,
       dbo.AntennaType.name AS [AntennaType_name],
       a1.guid AS [Template_Linkend_guid],
       a1.template_pmp_site_id AS template_pmp_site_id
       
FROM dbo.Template_LinkEnd_Antenna a2
     LEFT OUTER JOIN dbo.Template_LinkEnd a1 ON a2.template_linkend_id = a1.id
     LEFT OUTER JOIN dbo.AntennaType ON a2.antennaType_id = dbo.AntennaType.id

go

---------------------------------------------------

IF OBJECT_ID(N'[dbo].[view_Template_Linkend]') IS NOT NULL

DROP VIEW [dbo].[view_Template_Linkend]
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW dbo.view_Template_Linkend
AS
SELECT M.*,
       dbo.CalcModel.name AS [CalcModel_name],
       dbo.CellLayer.name AS [CellLayer_name],
       dbo.LinkEndType.name AS [LinkEndType_name],
       dbo.Passive_Component.name AS [Passive_Component_name]
FROM dbo.Template_LinkEnd M
     LEFT OUTER JOIN dbo.Passive_Component ON M.combiner_id =
     dbo.Passive_Component.id
     LEFT OUTER JOIN dbo.LinkEndType ON M.linkendtype_id = dbo.LinkEndType.id
     LEFT OUTER JOIN dbo.CellLayer ON M.cell_layer_id = dbo.CellLayer.id
     LEFT OUTER JOIN dbo.CalcModel ON M.calc_model_id = dbo.CalcModel.id
go




-----------------------------------------------------------
IF OBJECT_ID(N'[dbo].[view_PMP_TERMINAL]') IS NOT NULL

DROP VIEW [dbo].[view_PMP_TERMINAL]
GO


CREATE VIEW dbo.view_PMP_TERMINAL
AS
SELECT LinkEnd.*,

       dbo.Property.name AS property_name,
       dbo.Property.lat AS lat,
       dbo.Property.lon AS lon,
       dbo.Property.ground_height AS ground_height,
       
       dbo.LinkEndType.name AS LinkEndType_name,
       
       dbo.PMP_site.name AS PMP_site_name,          
       
       PMP_site.name   AS connected_pmp_site_name, 
       pmp_sector.name AS connected_pmp_sector_name, 
       
       dbo.Property.project_id       
   
  --     dbo.PMP_Sector.name AS PMP_Sector_name
       
FROM LinkEnd 
     LEFT OUTER JOIN dbo.PMP_site ON LinkEnd.pmp_site_id =     dbo.PMP_site.id
     LEFT OUTER JOIN dbo.LinkEnd pmp_sector ON LinkEnd.connected_pmp_sector_id =  pmp_sector.id
     
--     LEFT OUTER JOIN dbo.PMP_Sector ON LinkEnd.pmp_sector_id =     dbo.PMP_Sector.id
     
     LEFT OUTER JOIN dbo.LinkEndType ON LinkEnd.linkendtype_id =     dbo.LinkEndType.id
 --    LEFT OUTER JOIN dbo.Property ON LinkEnd.property_id =     dbo.Property.id
     
     
     LEFT OUTER JOIN dbo.Property ON dbo.LinkEnd.property_id = dbo.Property.id
     

     
     
where 
  LinkEnd.ObjName = 'PMP_terminal'

go



------------------------------------------------------------

IF OBJECT_ID(N'[dbo].[view_PMP_sector]') IS NOT NULL

DROP VIEW [dbo].[view_PMP_sector]
GO


CREATE VIEW dbo.view_PMP_sector
AS     
SELECT 

      dbo.LinkEnd.*,

      dbo.LinkEndType.name AS linkendtype_name,
             
      dbo.Property.guid AS [Property_guid],
      dbo.Property.georegion_id AS georegion_id,

      dbo.Property.project_id AS project_id,

             
      dbo.Property.lon,              
      dbo.Property.lat,
             
      dbo.CalcModel.name AS Calc_Model_Name,
      dbo.CellLayer.name AS cell_layer_name,
             
      dbo.Property.name AS Property_name,
             
      dbo.PMP_site.name AS PMP_site_name,
      dbo.PMP_site.guid AS PMP_site_guid
       
       
FROM dbo.PMP_site
     LEFT OUTER JOIN dbo.Property ON dbo.PMP_site.property_id = dbo.Property.id
     RIGHT OUTER JOIN dbo.CellLayer
     RIGHT OUTER JOIN dbo.LinkEnd ON dbo.CellLayer.id =   dbo.LinkEnd.cell_layer_id
     LEFT OUTER JOIN dbo.CalcModel ON dbo.LinkEnd.calc_model_id =   dbo.CalcModel.id
     LEFT OUTER JOIN dbo.LinkEndType ON dbo.LinkEnd.linkendtype_id =    dbo.LinkEndType.id ON dbo.PMP_site.id = dbo.LinkEnd.pmp_site_id
     



     
where 
  LinkEnd.ObjName = 'PMP_sector'

go

-------------------------------------------------------------------------------


IF OBJECT_ID(N'[dbo].[view_PMP_sector]') IS NOT NULL
  DROP VIEW dbo.view_PMP_sector
GO


CREATE VIEW dbo.view_PMP_sector
AS     
SELECT 

      dbo.LinkEnd.*,

      dbo.LinkEndType.name AS linkendtype_name,
             
      dbo.Property.guid AS [Property_guid],
      dbo.Property.georegion_id AS georegion_id,

      dbo.Property.project_id AS project_id,

             
      dbo.Property.lon,              
      dbo.Property.lat,
             
      dbo.CalcModel.name AS Calc_Model_Name,
      dbo.CellLayer.name AS cell_layer_name,
             
      dbo.Property.name AS Property_name,
             
      dbo.PMP_site.name AS PMP_site_name,
      dbo.PMP_site.guid AS PMP_site_guid
       
       
FROM dbo.PMP_site
     LEFT OUTER JOIN dbo.Property ON dbo.PMP_site.property_id = dbo.Property.id
     RIGHT OUTER JOIN dbo.CellLayer
     RIGHT OUTER JOIN dbo.LinkEnd ON dbo.CellLayer.id =   dbo.LinkEnd.cell_layer_id
     LEFT OUTER JOIN dbo.CalcModel ON dbo.LinkEnd.calc_model_id =   dbo.CalcModel.id
     LEFT OUTER JOIN dbo.LinkEndType ON dbo.LinkEnd.linkendtype_id =    dbo.LinkEndType.id ON dbo.PMP_site.id = dbo.LinkEnd.pmp_site_id
     



     
where 
  LinkEnd.ObjName = 'PMP_sector'
go



-----------------------------------------------------
IF OBJECT_ID(N'[dbo].[view_PMP_Link]') IS NOT NULL
  DROP VIEW dbo.view_PMP_Link
GO


CREATE VIEW dbo.view_PMP_Link
AS
SELECT     dbo.Link.*,

/*    Property1.lat AS lat1, Property1.lon AS lon1, 
    Property2.lat AS lat2, Property2.lon AS lon2, 
*/         
    dbo.LinkEnd.tx_freq_MHz AS tx_freq_MHz, 
                              
    dbo.LinkEnd.Bitrate_Mbps AS Bitrate_Mbps, 
    dbo.LinkEnd.band AS band, 

/*    dbo.fn_PickList_GetValueByCode('link', 'status',  dbo.Link.status) as  status_name,
                      
    dbo.fn_PickList_GetValueByCode('link', 'GST_type', dbo.Link.calc_method) AS GST_type_name, 
    dbo.fn_PickList_GetValueByCode('link', 'calc_method', dbo.Link.GST_type) AS calc_method_name, 
    dbo.fn_PickList_GetValueByCode('link',  'LOS_status', dbo.Link.LOS_status) AS LOS_status_name,                       
    dbo.fn_PickList_GetValueByCode('link', 'terrain_type', dbo.Link.terrain_type)  AS terrain_type_name, 
    dbo.fn_PickList_GetValueByCode('link', 'underlying_terrain_type', dbo.Link.underlying_terrain_type)   AS underlying_terrain_type_name, 
*/
    
  --  dbo.Status.name AS status_name, 
    
    dbo.ClutterModelType.name AS Clutter_Model_Name 
                      
    
/*    Property1.name AS Property1_name, 
    Property2.name AS Property2_name,


    Property1.georegion_id AS georegion1_id, 
    Property2.georegion_id AS georegion2_id,
                      
    Property1.ground_height AS Property1_ground_height, 
    Property2.ground_height AS Property2_ground_height,
*/

 --Link_Repeater.id as Link_Repeater_id
  

 FROM         dbo.LinkEnd 
    RIGHT OUTER JOIN   dbo.Status 
    RIGHT OUTER JOIN      dbo.Link 
    LEFT OUTER JOIN      dbo.ClutterModelType ON dbo.Link.clutter_model_id =   dbo.ClutterModelType.id ON dbo.Status.id = dbo.Link.status  ON dbo.LinkEnd.id = dbo.Link.linkend2_id
--    LEFT OUTER JOIN dbo.Property Property2 ON dbo.Link.Property2_id = Property2.id 
 --   LEFT OUTER JOIN dbo.Property Property1 ON dbo.Link.Property1_id = Property1.id
            
  --  left OUTER JOIN dbo.Link_Repeater ON dbo.Link.id =  dbo.Link_Repeater.Link_ID
            
WHERE   
   (dbo.Link.objname in ( 'pmp_link',  'pmp'))

go

---------------------------------------------------------------------------------
IF OBJECT_ID(N'[dbo].[view_PMP_TERMINAL]') IS NOT NULL
  DROP VIEW dbo.view_PMP_TERMINAL
GO


CREATE VIEW dbo.view_PMP_TERMINAL
AS
SELECT LinkEnd.*,

       dbo.Property.name AS property_name,
       dbo.Property.lat AS lat,
       dbo.Property.lon AS lon,
       dbo.Property.ground_height AS ground_height,
       
       dbo.LinkEndType.name AS LinkEndType_name,
       
       dbo.PMP_site.name AS PMP_site_name,          
       
       PMP_site.name   AS connected_pmp_site_name, 
       pmp_sector.name AS connected_pmp_sector_name, 
       
       dbo.Property.project_id       
   
  --     dbo.PMP_Sector.name AS PMP_Sector_name
       
FROM LinkEnd 
     LEFT OUTER JOIN dbo.PMP_site ON LinkEnd.pmp_site_id =     dbo.PMP_site.id
     LEFT OUTER JOIN dbo.LinkEnd pmp_sector ON LinkEnd.connected_pmp_sector_id =  pmp_sector.id
     
--     LEFT OUTER JOIN dbo.PMP_Sector ON LinkEnd.pmp_sector_id =     dbo.PMP_Sector.id
     
     LEFT OUTER JOIN dbo.LinkEndType ON LinkEnd.linkendtype_id =     dbo.LinkEndType.id
 --    LEFT OUTER JOIN dbo.Property ON LinkEnd.property_id =     dbo.Property.id
     
     
     LEFT OUTER JOIN dbo.Property ON dbo.LinkEnd.property_id = dbo.Property.id
     

     
     
where 
  LinkEnd.ObjName = 'PMP_terminal'
go


---------------------------------------------------------------------

IF OBJECT_ID(N'[dbo].[view_PMP_site_with_project_id]') IS NOT NULL
  DROP VIEW dbo.view_PMP_site_with_project_id
GO

CREATE VIEW dbo.view_PMP_site_with_project_id
AS
SELECT 
       P.project_id,
       dbo.PMP_site.*
FROM dbo.PMP_site
     LEFT OUTER JOIN dbo.Property P ON dbo.PMP_site.property_id = P.id
go











IF OBJECT_ID(N'pmp.sp_Template_Linkend_Antenna_Copy_to_PMP_Sector_Antenna') IS NOT NULL
  DROP procedure pmp.sp_Template_Linkend_Antenna_Copy_to_PMP_Sector_Antenna
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */

CREATE PROCEDURE pmp.sp_Template_Linkend_Antenna_Copy_to_PMP_Sector_Antenna
(
  @TEMPLATE_LINKEND_ID 	int,
--  @PROPERTY_ID 			int,
  @LINKEND_ID 	     	int 
)    
AS
BEGIN
  SET ANSI_WARNINGS OFF --String or binary data would be truncated.
 

  DECLARE  
    @property_ID int
 --   @PROJECT_ID int; 
   
  
  SELECT 
--  		@PROJECT_ID = PROJECT_ID,
        @property_ID = property_ID 
--  	FROM view_PMP_Site 
  	FROM LINKEND 
    WHERE (ID = @LINKEND_ID) 
  

--------------------------------------------------
-- view_LinkEnd_antennas
--------------------------------------------------    
  insert into Linkend_Antenna
    (
   --  PROJECT_ID,
     Linkend_id,
     property_ID,
     
     name, --objname,
     
     BAND,
     
     HEIGHT, 
     DIAMETER,GAIN,TILT,
 
     POLARIZATION_str,
     AntennaType_ID, 
     vert_width,horz_width,
     loss,
     
     azimuth
     
    )    
   select 
   --  @PROJECT_ID,
     @Linkend_id, 
     @property_ID, 
    
     name,
     BAND,
     
     HEIGHT, 
     DIAMETER,GAIN,TILT,
 
     POLARIZATION_str,
     AntennaType_ID,
     vert_width,horz_width,
     loss,
     azimuth
     
   from 
   	Template_Linkend_Antenna 
   WHERE 
   	Template_Linkend_id=@Template_Linkend_id
   
 
 
END

go

--------------------------------------------------------
IF OBJECT_ID(N'pmp.sp_Template_Linkend_Copy_to_PMP_SITE') IS NOT NULL
  DROP procedure pmp.sp_Template_Linkend_Copy_to_PMP_SITE 
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE pmp.sp_Template_Linkend_Copy_to_PMP_SITE 
(
  @Template_Linkend_ID INT = 1012, 
  @PMP_SITE_ID int = 167
)    
AS
BEGIN
  SET ANSI_WARNINGS OFF --String or binary data would be truncated.

  DECLARE  
    @id_new int,
    @property_ID int;
 --   @PROJECT_ID int; 
 --   @sName VARCHAR(100);

           
   
  SELECT 
--  		@PROJECT_ID = PROJECT_ID,
        @property_ID = property_ID 
--  	FROM view_PMP_Site 
  	FROM PMP_Site 
    WHERE (ID = @PMP_SITE_ID) 
  


 -- if @ID is null
 --   set @ID = 70365
  
  if @Template_Linkend_ID is null
  BEGIN
   SELECT top 1  @Template_Linkend_id =id FROM Template_Linkend
  
--    set @ID = 70365
 --   set @LINKEND_ID =124645
  END

    	
 -- print @ID
  
  INSERT INTO LinkEnd
--  INSERT INTO PMP_Sector
        
      (
      objname,   
 	  PMP_SITE_ID,
      property_ID,
      
       name )
   select 
      'PMP_Sector',   
      @PMP_SITE_ID,
      @property_ID,
      
      name
   from 
   		Template_Linkend     
   where 
   		id = @Template_Linkend_ID;      
    
   set @id_new= IDENT_CURRENT ('LinkEnd')


-----------------------------------------------------------   

Alter table  LinkEnd Disable trigger all

      
 UPDATE LinkEnd 
 SET  
   		band = m.band,
        LinkEndType_ID 		= m.LinkEndType_ID,
        LinkEndType_Mode_ID = m.LinkEndType_Mode_ID,
        Mode 				= m.mode,
        subband				= m.subband,
        bitrate_Mbps    	= m.bitrate_Mbps,
        channel_type        = m.channel_type,
                 
        
      --  MODULATION_count=m.MODULATION_level_count,

        MODULATION_TYPE =m.MODULATION_TYPE,
        SIGNATURE_HEIGHT=m.SIGNATURE_HEIGHT, 
        SIGNATURE_WIDTH =m.SIGNATURE_WIDTH,  
        THRESHOLD_BER_3 =m.THRESHOLD_BER_3,  
        THRESHOLD_BER_6 =m.THRESHOLD_BER_6,  
        
        POWER_MAX       = m.POWER_MAX,
        POWER_dBm       = m.POWER_dBm,
        POWER_W         = m.POWER_W,
        
        rx_freq_MHz = m.rx_freq_MHz,
        tx_freq_MHz = m.tx_freq_MHz,
        
        channel_width_MHz =m.channel_width_MHz,        
        
        cell_layer_id = m.cell_layer_id,
        calc_model_id = m.calc_model_id,
        k0 			= m.k0,
        k0_open 	= m.k0_open,
        k0_closed 	= m.k0_closed,
     
 --   trx_id 		= m.trx_id,
                  
        calc_radius_km = m.calc_radius_km

    --    combiner_id    = m.combiner_id,
     --   combiner_loss  = m.combiner_loss
                
        
        
--       MODULATION_COUNT      = m.MODULATION_level_COUNT,
--       GOST_53363_modulation = m.GOST_53363_modulation,
        
   --     Freq_spacing  =  m.BANDWIDTH * CHANNEL_SPACING    -- calculated        
        
          
       from 
          (select * from Template_Linkend WHERE id=@Template_Linkend_id) m     
       WHERE 
         (LinkEnd.id = @id_new)       
  
 print @@ROWCOUNt
 
Alter table  LinkEnd enable trigger all
 
  
-----------------------------------------------------------   

    
 --	set @id_new= @@IDENTITY;
    
  exec [pmp].sp_Template_Linkend_Antenna_Copy_to_PMP_Sector_Antenna 
       @Template_Linkend_id = @Template_Linkend_ID, 
       @Linkend_ID          = @id_new
    
  RETURN @id_new       
 
END

go

-----------------------------------------------------
IF OBJECT_ID(N'pmp.sp_Template_Pmp_Site_Copy_to_PMP_Site') IS NOT NULL
  DROP procedure pmp.sp_Template_Pmp_Site_Copy_to_PMP_Site
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE pmp.sp_Template_Pmp_Site_Copy_to_PMP_Site
(
  @ID INT,
  @PMP_Site_ID int 
)  
AS
BEGIN
 DECLARE
   @id1 int
 --  @Template_LinkEnd_id int; 



  UPDATE PMP_Site 
     SET 
  	   calc_radius_km  =m.calc_radius_km       
     from 
        (select * from Template_PMP_Site WHERE id=@ID) m     
     WHERE 
        PMP_Site.id=@PMP_Site_ID       

  -------------------------------


/* SELECT *  
    
    FROM Template_LinkEnd
    WHERE Template_pmp_site_id = @ID
*/
  SELECT id  
     INTO #temp
    FROM Template_LinkEnd
    WHERE Template_pmp_site_id = @ID


  WHILE EXISTS(SELECT * FROM #temp)
  BEGIN
    SELECT TOP 1 @id1=id FROM #temp
    DELETE FROM #temp WHERE id=@id1  
    
    exec pmp.sp_Template_Linkend_Copy_to_PMP_SITE 
--               @ID          = @Template_LinkEnd_id, 
               @Template_Linkend_ID   = @id1, 
               @PMP_SITE_ID = @PMP_Site_ID    
               
  END

  
END

go


---------------------------------------------------------


IF OBJECT_ID(N'[dbo].[sp_Pmp_Site_Add]') IS NOT NULL
  DROP procedure dbo.sp_Pmp_Site_Add
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE procedure dbo.sp_Pmp_Site_Add
(
  @PROJECT_ID 	    int=null,   
  @PROPERTY_NAME 	varchar(100)=null,
  @LAT 				float=null,
  @LON 				float=null,
  @LAT_WGS			float=null,
  @LON_WGS			float=null,


  @PROPERTY_ID 	    int,   
  @NAME 			 varchar(100),
  
  @CALC_RADIUS_KM   float=null,   
  @TEMPLATE_PMP_SITE_ID int=null,  -- if not null -> apply !
    
  @IS_GET_INFO 			bit = 0    
)
AS
begin
  -------------------------------------------------------------------------
--  IF @PROPERTY_ID IS NULL  RAISERROR ('@PROPERTY_ID is NULL', 16, 1);

--  IF NOT EXISTS(SELECT * FROM PROPERTY WHERE (ID=@PROPERTY_ID) )
--    RAISERROR ('@PROPERTY_ID is not EXISTS', 16, 1);
  -------------------------------------------------------------------------
  

  DECLARE 
   @new_id int
 --  @project_id int;
 

-- set @project_id = dbo.fn_Property_GetProjectID(@PROPERTY_ID)


--print 'pmp PROJECT_ID' + cast(@PROJECT_ID as VARCHAR(50))

/*
  SELECT @PROJECT_ID = PROJECT_ID 
  	FROM PROPERty  
    WHERE (ID = @PROPERTY_ID) 
    
*/
  IF IsNull(@PROPERTY_ID,0) < 1
  begin
  	INSERT INTO property (project_id,name, lat,lon) VALUES ( @project_id,@PROPERTY_NAME, @lat,@lon) 
    
    set @PROPERTY_ID= IDENT_CURRENT ('property')
   
  end




  INSERT INTO Pmp_Site
     (--PROJECT_ID, 
      NAME, 
      PROPERTY_ID,
      CALC_RADIUS_KM
      )
  VALUES
     (--@project_id, 
      @Name,
      @PROPERTY_ID,
      @CALC_RADIUS_KM
     );
  
  
    
  set @new_id= IDENT_CURRENT ('Pmp_Site')
   
--  set @new_id=@@IDENTITY;
----------------------------------------------------  
   
Alter table  Pmp_Site Disable trigger all



 if @TEMPLATE_PMP_SITE_ID>0 
   exec pmp.sp_Template_PMP_Site_Copy_to_PMP_Site 
   			@ID = @TEMPLATE_PMP_SITE_ID, 
            @Pmp_site_id = @new_id


Alter table  Pmp_Site enable trigger all



  if @IS_GET_INFO=1
    SELECT id,guid,name  FROM Pmp_Site WHERE ID=@new_id
  
  RETURN @new_id; 
      
        
end

go

--------------------------------------

IF OBJECT_ID(N'[dbo].[view_Linkend_base]') IS NOT NULL
  DROP VIEW dbo.view_Linkend_base
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW dbo.view_Linkend_base
AS     
SELECT 
   Linkend.* ,
   
   LinkEndType.name as LinkEndType_name,   
   
   dbo.lib_vendor.name AS Vendor_name, 
   dbo.lib_VENDOR_Equipment.name AS Vendor_Equipment_name 
        
FROM 
   dbo.Linkend
   LEFT OUTER JOIN  dbo.LinkEndType on Linkend.LinkEndType_id = dbo.LinkEndType.id
   LEFT OUTER JOIN
          dbo.lib_VENDOR_Equipment ON dbo.LinkEndType.vendor_equipment_id = dbo.lib_VENDOR_Equipment.id LEFT OUTER JOIN
          dbo.lib_vendor ON dbo.LinkEndType.vendor_id = dbo.lib_vendor.id
   
   
   where 
      IsNull(objname, 'linkend') = 'linkend'

go



-------------------------------------------------------------
IF OBJECT_ID(N'[dbo].[view_PMP_site]') IS NOT NULL
  DROP VIEW dbo.view_PMP_site
GO


CREATE VIEW dbo.view_PMP_site
AS
SELECT P.georegion_id,
       P.project_id,
       P.lat,
       P.lon,
       P.address,
       P.name AS Property_name,
       S.name AS status_str,   
       dbo.PMP_site.*
FROM dbo.PMP_site
     LEFT OUTER JOIN dbo.Status S ON dbo.PMP_site.status_id = S.id
     LEFT OUTER JOIN dbo.Property P ON dbo.PMP_site.property_id = P.id

go



/****** Object:  View [Security].[View_User_Security_XREF]    Script Date: 18.01.2021 14:15:25 ******/
IF OBJECT_ID(N'[dbo].[view_PMP_CalcRegion_PMP_Sites]') IS NOT NULL
  DROP VIEW [dbo].[view_PMP_CalcRegion_PMP_Sites]


GO

CREATE VIEW dbo.view_PMP_CalcRegion_PMP_Sites
AS
SELECT DISTINCT TOP 100 PERCENT dbo.PMP_CalcRegionCells.id AS xref_id,
       dbo.PMP_CalcRegionCells.checked,
       dbo.PMP_CalcRegionCells.pmp_calc_region_id,
       dbo.PMP_site.id,
       dbo.PMP_site.name,
       
       dbo.Property.project_id,
       
       dbo.PMP_site.guid,
       dbo.Property.lat,
       dbo.Property.lon,
       dbo.Property.lat_str,
       dbo.Property.lon_str,
       dbo.PMP_site.property_id
       
FROM dbo.PMP_site
     LEFT OUTER JOIN dbo.Property ON dbo.PMP_site.property_id = dbo.Property.id
     RIGHT OUTER JOIN dbo.PMP_CalcRegionCells ON dbo.PMP_site.id =
     dbo.PMP_CalcRegionCells.pmp_site_id
     
WHERE (dbo.PMP_CalcRegionCells.pmp_site_id > 0) AND
      (dbo.PMP_CalcRegionCells.pmp_sector_id IS NULL)
      
ORDER BY dbo.PMP_site.name

go


-------------------------------------------------------------------------

IF OBJECT_ID(N'dbo.sp_Pmp_Site_Select_Item') IS NOT NULL
  DROP procedure dbo.sp_Pmp_Site_Select_Item 
GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Pmp_Site_Select_Item 
(
	@ID INT 
)    
AS
BEGIN
  -------------------------------------------------------------------------
  IF @ID IS NULL  RAISERROR ('@ID is NULL', 16, 1);
  -------------------------------------------------------------------------
    
  -------------------------------------------------------------------------
  IF OBJECT_ID('view_PMP_sector') is null  RAISERROR ('OBJECT_ID is NULL', 16, 1);
  -------------------------------------------------------------------------
     


  
  SET ANSI_WARNINGS OFF --String or binary data would be truncated.


 -- if @ID is null
 --   set @ID = 70365
  
  if @ID is null
  BEGIN
   SELECT top 1  @id =id FROM PMP_site
  
--    set @ID = 70365
 --   set @LINKEND_ID =124645
  END

    	
  print @ID

  declare @temp table 
  
      (ID          int NOT NULL,              
       guid        uniqueidentifier,
       parent_guid uniqueidentifier,
       
       name        NVARCHAR(100) NOT NULL,
       objname     VARCHAR(10) NOT NULL,
               
       BAND        VARCHAR(10),
            
       LinkEnd_ID 	int,


      ---------------
      --PmpSite sector
      ---------------
       cell_layer_ID  INT,
       cell_layer_name NVARCHAR(50),
      
       Color 						int,
       calc_radius_km		float,
       
       LinkEndType_ID 	int,
       LinkEndType_Name NVARCHAR(50),

       LinkEndType_Mode_ID 	int,

       
       calc_model_Name NVARCHAR(50),
       calc_model_id 	Float,
       k0_open 				Float,
       k0_closed 			Float,
       k0 						Float,
       
       POWER_dBm      Float,    
       POWER_W      	Float,

       THRESHOLD_BER_3 Float,
       THRESHOLD_BER_6 Float,       
       TX_FREQ_MHz    Float,
       RX_FREQ_MHz    Float,             

      LinkEnd_loss        Float,

      ---------------
      --ANTENNA
      ---------------
      AntennaType_ID      int, 
      AntennaType_Name    NVARCHAR(50),

      DIAMETER            Float,
      HEIGHT              Float,   
      AZIMUTH             Float, 
      POLARIZATION_str    VARCHAR(4),
      GAIN                Float,
      TILT                Float,
      vert_width          Float,
      horz_width          Float,
      ANTENNA_loss     Float
      
   --   is_master 					bit --master antenna
               
      )  

--------------------------------------------------
-- pmp_sector
--------------------------------------------------     
  insert into @temp
    (id,guid,name,objname, 
    
     color,
    
     cell_layer_id,cell_layer_name,
     
     calc_radius_km,
    
     LinkEndType_ID,LinkEndType_Name,  
     LinkEndType_Mode_ID,
     
     BAND,
     THRESHOLD_BER_3, THRESHOLD_BER_6, 
     POWER_dBm, 
     POWER_W,
     
     TX_FREQ_MHz, RX_FREQ_MHz,
     LinkEnd_Loss,
     
     calc_model_Name, calc_model_id, 
     k0_open, k0_closed, k0,
     
     LinkEnd_ID
     
     )
   select 
     id,guid,name,
     'pmp_sector',

	 	 color,	

     cell_layer_id,cell_layer_name,
     
     calc_radius_km,

     LinkEndType_ID, LinkEndType_Name, 
     LinkEndType_Mode_ID,


     BAND,
     THRESHOLD_BER_3, THRESHOLD_BER_6, 
     POWER_dBm, 
     dbo.fn_dBm_to_Wt(POWER_dBm),
     
     TX_FREQ_MHz, RX_FREQ_MHz,
  
     passive_element_loss,
     
     
     calc_model_Name, calc_model_id, 
     k0_open, k0_closed, k0,
     
     ID
     
--   from view_PMP_sectors_full      
   from view_PMP_sector
   where pmp_site_id = @ID
     
   
 

--------------------------------------------------
-- antennas
--------------------------------------------------    
  insert into @temp
    (id,guid,parent_guid,name,
    objname,
     BAND,
     HEIGHT, 
     DIAMETER,AZIMUTH,GAIN,TILT,
     POLARIZATION_str,
     AntennaType_ID,AntennaType_NAME,  
     vert_width,horz_width,
     ANTENNA_loss
     
    -- is_master
    )    
   select 
     id,guid,[linkend_guid],name,
     'antenna',  --[LinkEnd.guid]
     BAND,
     
     HEIGHT, 
     DIAMETER,AZIMUTH,GAIN,TILT,
     POLARIZATION_str,
     AntennaType_ID,AntennaType_NAME, 
     vert_width,horz_width,
     loss
--     is_master
     
 --  from view_PMP_sector_antennas 
   from view_LInkend_antennas 
   where LInkend_id  in (select LinkEnd_ID from  @temp)  


  SELECT * FROM @temp                   
 
 
  RETURN @@ROWCOUNT
  
END

go

-------------------------------------------------------------------------
IF OBJECT_ID(N'dbo.sp_Pmp_Site_Update_Item') IS NOT NULL
  DROP procedure dbo.sp_Pmp_Site_Update_Item 
GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Pmp_Site_Update_Item 
(
  @ID 	        INT, 
  @OBJNAME 		varchar(50),

  @CELL_LAYER_ID int=null,
  
  @COLOR 	     int=null,
  @POWER_dBm     Float=null,
  @POWER_W 	     Float=null,

  @TX_FREQ_MHz Float=null,


  @THRESHOLD_BER_3  Float=null, 
  @THRESHOLD_BER_6  Float=null,  
  
  @CALC_RADIUS_KM   Float=null, 
  
  @K0_OPEN 			Float=null,      
  @K0_CLOSED 		Float=null,     
  @K0 				Float=null,             
        
  --ANTENNA
  @HEIGHT           Float=null,   
  @AZIMUTH          Float=null, 
  @TILT             Float=null,

  @LOSS         Float=null 
  
 -- @CELL_LAYER_ID int
) 

AS
BEGIN
  ---------------------------------------------
  IF @ID IS NULL  RAISERROR ('@ID is NULL', 16, 1);
  IF @OBJNAME IS NULL  RAISERROR ('@OBJNAME is NULL', 16, 1);
  ---------------------------------------------


---------------------------------------------
 if @OBJNAME = 'PMP_Sector'
---------------------------------------------
   UPDATE Linkend 
     SET      
        THRESHOLD_BER_3 = COALESCE(@THRESHOLD_BER_3, THRESHOLD_BER_3),
        THRESHOLD_BER_6 = COALESCE(@THRESHOLD_BER_6, THRESHOLD_BER_6),
        TX_FREQ_MHZ     = @TX_FREQ_MHZ,
     
  		Color 		= COALESCE(@Color, Color), 
        POWER_dBm   = COALESCE(@POWER_dBm, POWER_dBm),
                
        CALC_RADIUS_KM= COALESCE(@CALC_RADIUS_KM, CALC_RADIUS_KM),
        
        K0_OPEN		=    COALESCE( @K0_OPEN,    K0_OPEN),
        K0_CLOSED	=    COALESCE( @K0_CLOSED,  K0_CLOSED),
        K0			=    COALESCE( @K0,         K0), 
        
        CELL_LAYER_ID = COALESCE(@CELL_LAYER_ID, CELL_LAYER_ID)
        
     WHERE 
        id=@ID    
        
  ELSE
---------------------------------------------
 if @OBJNAME = 'antenna'
---------------------------------------------
   UPDATE Linkend_Antenna 
     SET 
  		HEIGHT  =COALESCE( @HEIGHT, HEIGHT),
        AZIMUTH =COALESCE( @AZIMUTH, AZIMUTH) ,
        TILT    =COALESCE( @TILT, TILT),  
        LOSS =COALESCE( @LOSS, LOSS)  
     WHERE 
        id=@ID    
  ELSE
    RAISERROR ('@ID is null', 16, 1);

        
  RETURN @@ROWCOUNT
          
END
go

---------------------------------------------------


IF OBJECT_ID(N'dbo.sp_Pmp_Sector_Del') IS NOT NULL
  DROP procedure dbo.sp_Pmp_Sector_Del
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Pmp_Sector_Del 
(
  @ID int
)

AS
BEGIN

/*
  DECLARE @iID int

  DECLARE C CURSOR FOR
    SELECT id
       FROM Link
       WHERE (Pmp_Sector_id = @ID)
  open C
  
  fetch C INTO @iID
  while @@FETCH_STATUS = 0
    begin
      exec sp_Link_Del @iID
      
      fetch C INTO @iID
    end
  close C
  deallocate C*/
  
  /*

UPDATE Pmp_Terminal
  SET pmp_sector_id = NULL,
      pmp_site_id   = NULL
  WHERE pmp_sector_id = @ID*/


--delete from Linkend_Antenna  where PMP_Sector_id =@ID
--delete from PMP_Sector where id=@ID


-------------------------------------------------------------------------
-- delete PMP Links 
-------------------------------------------------------------------------


delete FROM Link
  WHERE @ID in (Linkend1_id, Linkend2_id)


UPDATE Linkend
  SET connected_pmp_sector_id = NULL,
      pmp_site_id   = NULL
  WHERE connected_pmp_sector_id =@ID 
   

delete from Linkend_Antenna
  where Linkend_id =@ID 

delete from PMP_CalcRegionCells
  where pmp_sector_id =@ID 


delete from Linkend 
  where id=@ID 
  
/*
BEGIN TRY  
     { sql_statement | statement_block }  
END TRY  
BEGIN CATCH  
     [ { sql_statement | statement_block } ]  
END CATCH

BEGIN TRANSACTION;  
  
BEGIN TRY  
    -- Generate a constraint violation error.  
    DELETE FROM Production.Product  
    WHERE ProductID = 980;  
END TRY  
BEGIN CATCH  
    SELECT   
        ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_SEVERITY() AS ErrorSeverity  
        ,ERROR_STATE() AS ErrorState  
        ,ERROR_PROCEDURE() AS ErrorProcedure  
        ,ERROR_LINE() AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
  
    IF @@TRANCOUNT > 0  
        ROLLBACK TRANSACTION;  
END CATCH;  
  
IF @@TRANCOUNT > 0  
    COMMIT TRANSACTION;  
  */

/*
-------------------------------------------------------------------------
if exists (select * from Linkend  where id=@ID)    
   RAISERROR ('sp_Pmp_Sector_Del - exists (select * from Linkend  where id=@ID)', 16, 1);
-------------------------------------------------------------------------
  
  */
  
  RETURN @@ROWCOUNT
  
END
  
go

------------------------------------------------------------------------------

IF OBJECT_ID(N'dbo.sp_Pmp_Site_GetSubItemList') IS NOT NULL
  DROP procedure dbo.sp_Pmp_Site_GetSubItemList 
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Pmp_Site_GetSubItemList 
(
	@ID INT
)
AS
BEGIN

  create table [#temp]
      (ID       int NOT NULL,  
       guid  		uniqueidentifier,
       name     VARCHAR(100) NOT NULL,
       objname  VARCHAR(20)  NOT NULL
      ); 
          

--  SELECT id,guid,'PMP_site' as objname
  --       FROM pmp_site
    --     WHERE (id = @aID)

   -- UNION   

 
  SELECT id
    into [#PMP_Sector]
  FROM LinkEnd
  WHERE (pmp_site_id = @ID);


            
  insert into [#temp] (id,guid,NAME,objname)   
    SELECT id,guid,name,'PMP_SECTOR'
      FROM LinkEnd
      WHERE (pmp_site_id = @ID);
    
    
  insert into [#temp] (id,guid,NAME,objname)  
    SELECT id,guid,name,'PMP_SECTOR_ant'
      FROM VIEW_LinkEnd_ANTENNAS
      WHERE  (pmp_site_id =@ID);
        
      
  insert into [#temp] (id,guid,NAME,objname)  
    SELECT id,guid,name,'LINK' 
       FROM LINK
       WHERE 
         LinkEnd1_id IN (SELECT id FROM LinkEnd WHERE pmp_site_id=@ID)
         or
         LinkEnd2_id IN (SELECT id FROM LinkEnd WHERE pmp_site_id=@ID)

--       WHERE (pmp_site_id = @ID)
        
       
  SELECT * FROM [#temp] 
                                        
END

go


-------------------------------------------------------------
IF OBJECT_ID(N'dbo.sp_Pmp_Site_Del') IS NOT NULL
  DROP procedure dbo.sp_Pmp_Site_Del 
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */ 
CREATE PROCEDURE dbo.sp_Pmp_Site_Del 
(
  @ID int
)
AS

BEGIN 

declare 
  @linkends TABLE (id int)
  
  insert into @linkends
    SELECT id 
      FROM LinkEnd  WHERE pmp_site_id = @ID


  DELETE FROM link
    where 
      Linkend1_id in  (SELECT id FROM @linkends)
      or 
      Linkend2_id in  (SELECT id FROM @linkends)
     

  DELETE FROM Linkend_Antenna  
    where Linkend_id in  (select id FROM @linkends)  

--  DELETE FROM pmp_sector  
--    where pmp_site_id = @ID

  DELETE FROM pmp_CalcRegionCells 
    WHERE PMP_Sector_ID in  (select id FROM @linkends)  


  DELETE FROM pmp_CalcRegionCells 
    WHERE (PMP_SITE_ID=@ID) 
            

 -- select * from LinkEnd where PMP_Site_id=@ID

  
  update LinkEnd
    set connected_pmp_sector_id = NULL,
        connected_pmp_site_id   = NULL
    where    
       connected_pmp_sector_id in  (SELECT id FROM @linkends)
  

 -- select * from LinkEnd where connected_pmp_sector_id is not null and  PMP_Site_id=@ID


  delete from LinkEnd where PMP_Site_id=@ID

  delete from Pmp_Site where id=@ID
    
--  if @@ERROR


  RETURN @@ROWCOUNT  
  
    
  --//DELETE FROM pmp_site 
  --  where id in (SELECT id FROM DELETED)

   
 /* delete from link
     WHERE (Pmp_Site_id = @ID)
    
    
  UPDATE Pmp_Terminal
    SET pmp_site_id = NULL,
        pmp_sector_id = NULL
    where PMP_Site_id=@ID


  delete from pmp_CalcRegionCells   where PMP_Site_id=@ID    
  delete from PMP_Sector where PMP_Site_id=@ID
  */
  
  
  
END
go


---------------------------------------------------

IF OBJECT_ID(N'dbo.fn_Property_Get_Length_m ') IS NOT NULL
  DROP FUNCTION dbo.fn_Property_Get_Length_m 
GO


CREATE FUNCTION dbo.fn_Property_Get_Length_m 
(

  @ID1   int,
  @ID2   int

)
RETURNS float
AS
BEGIN

 DECLARE  
   
    @lat1 float,
    @lon1 float,
    
    @lat2 float,
    @lon2 float


  SELECT       
       @LAT1 = lat,
       @LON1 = lon              
  FROM Property
     WHERE (id = @ID1)  


  SELECT       
       @LAT2 = lat,
       @LON2 = lon              
  FROM Property
     WHERE (id = @ID2)   




--  return  geo.fn_AZIMUTH_m(@LAT1, @Lon1, @LAT2, @Lon2)
  return  dbo.fn_AZIMUTH(@LAT1, @Lon1, @LAT2, @Lon2, 'LENGTH')


  /* Function body */
END
go




IF OBJECT_ID(N'dbo.sp_PMP_Terminal_Select_PMP_Sectors_for_Plug') IS NOT NULL
  DROP procedure dbo.sp_PMP_Terminal_Select_PMP_Sectors_for_Plug
GO


/* 
  ����� PMP_Sectors ��� PMP ��������� 
  
created: Alex  
used in: d_PmpTerminal_Plug  
 */
 /* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_PMP_Terminal_Select_PMP_Sectors_for_Plug
( 
  @ID int,
  @PROJECT_ID int = NULL
)
AS
BEGIN
  ---------------------------------------------
--  IF @ID IS NULL  RAISERROR ('@ID is NULL', 16, 1);
  ---------------------------------------------
  DECLARE 
  --  @project_id int,
    @property_id  int;


--if IsNull(@ID,0) > 0
    SELECT 
        @property_id = property_id ,        
    	@project_id = project_id
      FROM view_PMP_TERMINAL --_with_project_id       
      WHERE id = @ID



  IF @project_id is null   RAISERROR ('@project_id is null ', 16, 1);


--fn_Property_Get_Length_m


  SELECT  id,name,pmp_site_id, pmp_site_name, Property_name, band, 
        0 as Length_m,
        cast(0 as bit) as checked,
        
        @id as pmp_Terminal_id
  
    FROM  view_PMP_sector --_full 
    WHERE (project_id=@project_id)
         
         and 
          
          -- (@property_id IS null) or
          (Property_id <> IsNull(@Property_id,0) ) 
          
    ORDER BY pmp_site_name, name
  
  
  RETURN @@ROWCOUNT
    
END

go

----------------------------------------------

IF OBJECT_ID(N'dbo.sp_Pmp_Terminal_Del') IS NOT NULL
  DROP procedure dbo.sp_Pmp_Terminal_Del 
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Pmp_Terminal_Del 
(
  @ID int
)
AS
BEGIN

/*
DELETE FROM link
  where pmp_terminal_id in (SELECT id FROM DELETED)
  
  
DELETE FROM Linkend_Antenna
  where Pmp_Terminal_id in (SELECT id FROM DELETED)  
  
  
DELETE FROM PmpTerminal
  where id in (SELECT id FROM DELETED)
*/


--select * from link_SDB where link1_id = 6626 or link2_id = 6626


--  select *  FROM Link  WHERE @id in (Linkend1_id, Linkend2_id)

--return


  DELETE FROM Link  WHERE @id in (Linkend1_id, Linkend2_id)

  DELETE FROM Linkend_Antenna  where Linkend_id = @ID

  DELETE FROM LinkEnd  WHERE id=@ID
  
         
  RETURN @@ROWCOUNT    
       
END
go

------------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID(N'[dbo].[sp_Template_Pmp_Site_Select_Item]') IS NOT NULL
  DROP PROCEDURE dbo.sp_Template_Pmp_Site_Select_Item
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Template_Pmp_Site_Select_Item 
(
	@ID INT 
)    
AS
BEGIN
  SET ANSI_WARNINGS OFF --String or binary data would be truncated.


 -- if @ID is null
 --   set @ID = 70365
  
  if @ID is null
  BEGIN
   SELECT top 1  @id =id FROM Template_pmp_Site
  
--    set @ID = 70365
 --   set @LINKEND_ID =124645
  END

    	
  print @ID

  create table [#temp]
      (ID          int NOT NULL,              
       guid        uniqueidentifier,
       parent_guid uniqueidentifier,
       
       name        NVARCHAR(100) NOT NULL,
       objname     VARCHAR(10) NOT NULL,
               
       BAND        VARCHAR(10),
            
      ---------------
      --PmpSite sector
      ---------------
       cell_layer_ID  INT,
       cell_layer_name VARCHAR(50),
      
     --  Color 						int,
       calc_radius_km		float,
       
       LinkEndType_ID 	int,
       LinkEndType_Name VARCHAR(50),
       
       calc_model_Name VARCHAR(50),
       calc_model_id 	Float,
       k0_open 				Float,
       k0_closed 			Float,
       k0 						Float,
       
       POWER_dBm      Float,    
       POWER_W      	Float,

       THRESHOLD_BER_3 Float,
       THRESHOLD_BER_6 Float,       
       TX_FREQ_MHz    Float,
     --  RX_FREQ_MHz    Float,             

   --   LinkEnd_loss        Float,

       combiner_name			VARCHAR(50),
       combiner_id 				int,
       combiner_loss    	float,


      ---------------
      --ANTENNA
      ---------------
      AntennaType_ID      int, 
      AntennaType_Name    VARCHAR(50),

      DIAMETER            Float,
      HEIGHT              Float,   
      AZIMUTH             Float, 
      POLARIZATION_str    VARCHAR(4), --s
      GAIN                Float,
      TILT                Float,
      vert_width          Float,
      horz_width          Float,
      ANTENNA_loss     Float,
      
   --   is_master 					bit --master antenna
               
      ) ;      



--------------------------------------------------
-- LinkEnd
--------------------------------------------------     
  insert into [#temp]
    (id,guid,name,objname, 
    
  --   color,
    
     cell_layer_id, cell_layer_name,
     
     calc_radius_km,

     BAND,   
     LinkEndType_ID,LinkEndType_Name,  
     THRESHOLD_BER_3, THRESHOLD_BER_6, 
     POWER_dBm, POWER_W,
     TX_FREQ_MHz, 
     --RX_FREQ_MHz,
  --   LinkEnd_Loss,
     
     calc_model_id, calc_model_Name, 
     k0_open, k0_closed, k0,
     
     combiner_name,
     combiner_id,
     combiner_loss
     
     )
   select 
     id,guid,name,'pmp_sector',

	-- 	color,	

     cell_layer_id, [CellLayer_name],
     
     calc_radius_km,

     BAND,
     LinkEndType_ID, [LinkEndType_name], 
   
     THRESHOLD_BER_3, THRESHOLD_BER_6, 
     
     POWER_dBm, dbo.fn_dBm_to_Wt(POWER_dBm),
     TX_FREQ_MHz, 
     --RX_FREQ_MHz,
    --  passive_element_loss,
     
     
     calc_model_id, [CalcModel_name], 
     k0_open, k0_closed, k0,
     
     [Passive_Component_name],
     combiner_id,
     combiner_loss  --_dB
     
     
   from 
     VIEW_Template_Linkend     
   where 
     template_pmp_site_id = @ID
     

--------------------------------------------------
-- view_LinkEnd_antennas
--------------------------------------------------    
  insert into [#temp]
    (id,guid,parent_guid,name,
     objname,
     BAND,
  
     AntennaType_ID, AntennaType_NAME,  

     HEIGHT, 
     DIAMETER,AZIMUTH,GAIN,TILT,
     POLARIZATION_str,
     vert_width,horz_width,
     ANTENNA_loss
     
    -- is_master
    )    
   select 
     id,guid,[Template_Linkend_guid],name,
     'antenna',  --[LinkEnd.guid]
      BAND,
     
     AntennaType_ID, [AntennaType_name], 

     HEIGHT, 
     DIAMETER,AZIMUTH,GAIN,TILT,
     POLARIZATION_str,
     vert_width,horz_width,
     loss
--     is_master
     
   from 
     view_Template_Linkend_Antenna 
   where 
      template_pmp_site_id = @ID


  SELECT * FROM #temp                                
 
  RETURN @@ROWCOUNT
  
 
END
go

