if  COL_LENGTH('dbo.link','SDB_bitrate_priority')  is null
  ALTER TABLE dbo.link
     ADD SDB_bitrate_priority float
GO

if  COL_LENGTH('dbo.link','SDB_bitrate_no_priority')  is null
  ALTER TABLE dbo.link
     ADD SDB_bitrate_no_priority float
GO



IF OBJECT_ID(N'dbo.sp_Link_rename_linkends_UPD') IS  not  NULL
  DROP PROCEDURE [dbo].[sp_Link_rename_linkends_UPD]
GO


IF OBJECT_ID(N'[dbo].[view_LinkEnd]') IS NOT NULL

DROP VIEW [dbo].[view_LinkEnd]
GO


/* task -> rename




*/
CREATE VIEW dbo.view_LinkEnd
AS
SELECT   
  L.*, 
  dbo.LinkEndType.name AS LinkEndType_name, 
  dbo.Property.name AS  property_name, 

  dbo.Property.project_id , 
   

  
  dbo.Property.lat AS lat,      
  dbo.Property.lon AS lon, 
  
        dbo.Property.ground_height AS ground_height,
        dbo.Property.guid AS [Property_guid], 
                      
                      
        dbo.Passive_Component.name AS [Passive_Component_name], 



        dbo.fn_PickList_GetValueByCode('linkend', 'equipment_type', L.equipment_type) AS equipment_type_name, 
        dbo.fn_PickList_GetValueByCode('linkend',    'kratnostBySpace', L.kratnostBySpace) AS     kratnostBySpace_name, 
        dbo.fn_PickList_GetValueByCode('linkend', 'kratnostByFreq'      , L.kratnostByFreq) AS kratnostByFreq_name, 
        dbo.fn_PickList_GetValueByCode('linkend', 'redundancy',      L.redundancy) AS redundancy_name, 
      
  
--           dbo.fn_LinkEnd_Get_Link_Name(L.id) AS Link_name, 

--        dbo.fn_LinkEnd_Get_Link_Calc_method(L.id) AS     Link_calc_method_id,       
--    dbo.fn_LinkEnd_Get_Link_ID(L.id) AS Link_id, 
        
        link.calc_method AS Link_calc_method_id,       
--        link.calc_method_name AS Link_calc_method_name,
        link.Link_name,       
        link.Link_id,       
        
        dbo.Property.georegion_id AS georegion_id,
                      
                      
                      
dbo.fn_dBm_to_Wt(L.power_dBm)      AS power_Wt, 
                      

      ant.diameter  AS antenna_diameter,
      ant.height  AS antenna_height,
      ant.polarization  AS antenna_polarization,

/*
dbo.fn_Linkend_Antenna_GetValue(L.id, 'diameter')      AS antenna_diameter, 
dbo.fn_Linkend_Antenna_GetValue(L.id, 'height')        AS antenna_height, 
dbo.fn_Linkend_Antenna_GetValue(L.id, 'polarization')  AS antenna_polarization,

*/

LinkEndType_mode.bandwidth                      


                      
                      
FROM         dbo.LinkEnd L LEFT OUTER JOIN
          dbo.Passive_Component ON L.passive_element_id = dbo.Passive_Component.id LEFT OUTER JOIN
          dbo.LinkEndType_mode ON L.linkendtype_mode_id = dbo.LinkEndType_mode.id LEFT OUTER JOIN
          dbo.LinkEndType ON L.linkendtype_id = dbo.LinkEndType.id LEFT OUTER JOIN
          dbo.Property ON L.property_id = dbo.Property.id
                      
                      
          OUTER apply fn_Linkend_Link_info(L.id) link
          OUTER apply fn_Linkend_Antenna_info (L.id) ant
          
          
       --   where
       
      /* 
   dbo.lib_vendor.name AS Vendor_name, 
   dbo.lib_VENDOR_Equipment.name AS Vendor_Equipment_name 
        
FROM 
   dbo.Linkend
   LEFT OUTER JOIN  dbo.LinkEndType on Linkend.LinkEndType_id = dbo.LinkEndType.id
   LEFT OUTER JOIN
          dbo.lib_VENDOR_Equipment ON dbo.LinkEndType.vendor_equipment_id = dbo.lib_VENDOR_Equipment.id LEFT OUTER JOIN
          dbo.lib_vendor ON dbo.LinkEndType.vendor_id = dbo.lib_vendor.id
       */
go

--------------------------------------------------------



/* ---------------------------------------------------------------
created: Alex  
used in:  

description
  linkend1_name=@property1_name +'->'+ @property2_name;
  linkend2_name=@property2_name +'->'+ @property1_name;
  link_name=@property1_name +'<->'+ @property2_name;

--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Link_rename_linkends_UPD
(
  @ID   int = 10762
)

AS
BEGIN
  ---------------------------------------------------
  IF @ID IS NULL  RAISERROR ('@ID is NULL', 16, 1);
  ---------------------------------------------------

  DECLARE 
 
    @linkend1_id int,
    @linkend2_id int,


    @LINKEND1_name nVARCHAR(100),
    @LINKEND2_name nVARCHAR(100),
          
    @property1_ID int,
    @property2_ID int,

    @property1_name nVARCHAR(100),
    @property2_name nVARCHAR(100)
     ;
  	

    
  SELECT 
   --    @project_ID  = project_ID,
       @LINKEND1_ID = LINKEND1_ID,
       @LINKEND2_ID = LINKEND2_ID,

       @property1_id = property1_id,
       @property2_id = property2_id,

       @property1_name = [property1_name],
       @property2_name = [property2_name]

    FROM VIEW_Link
    WHERE (id = @ID);
    
   
   

  set @linkend1_name=@property1_name +'->'+ @property2_name;
  set @linkend2_name=@property2_name +'->'+ @property1_name;
 -- set @link_name=@property1_name +'<->'+ @property2_name;



  exec sp_Object_GetNewName_new @OBJNAME='LinkEnd', @NAME=@LINKEND1_name, 
    @property_ID=@property1_id, @RESULT=@LINKEND1_name out


  exec sp_Object_GetNewName_new @OBJNAME='LinkEnd', @NAME=@LINKEND2_name, 
    @property_ID=@property2_id, @RESULT=@LINKEND2_name out


  UPDATE Linkend SET name = @LINKEND1_name WHERE (id = @linkend1_id) 
  UPDATE Linkend SET name = @LINKEND2_name WHERE (id = @linkend2_id) 



  RETURN @@ROWCOUNT 

 
END

go



-----------------------------------

IF OBJECT_ID(N'[dbo].[view_LinkEnd_with_project_id]') IS NOT NULL
  DROP VIEW dbo.view_LinkEnd_with_project_id
GO

CREATE VIEW dbo.view_LinkEnd_with_project_id
AS
SELECT dbo.LinkEnd.*,
       dbo.Property.project_id
FROM dbo.LinkEnd
     LEFT OUTER JOIN dbo.Property ON dbo.LinkEnd.property_id =  dbo.Property.id
     
where 
  LinkEnd.ObjName is null or      
  LinkEnd.ObjName = 'linkend'

GO
------------------------------------------------------


IF OBJECT_ID(N'[dbo].[view_Linkend_Antenna_with_project_id]') IS NOT NULL
  DROP VIEW dbo.view_Linkend_Antenna_with_project_id 
GO

CREATE VIEW dbo.view_Linkend_Antenna_with_project_id 
AS
SELECT dbo.Linkend_Antenna.*,
       dbo.Property.project_id AS project_id
FROM dbo.Linkend_Antenna
     LEFT OUTER JOIN dbo.Property ON dbo.Linkend_Antenna.property_id =
     dbo.Property.id

go

-----------------------------------------------------------------------

IF OBJECT_ID(N'[dbo].[view_LinkEnd_antennas]') IS NOT NULL

DROP VIEW [dbo].[view_LinkEnd_antennas]
GO



CREATE VIEW dbo.view_LinkEnd_antennas
AS
SELECT
       dbo.Linkend_Antenna.*,

       dbo.LinkEnd.guid AS [LinkEnd_guid],

       dbo.LinkEnd.pmp_site_id, -- AS [LinkEnd_guid],

       
       dbo.AntennaType.fronttobackratio,
       dbo.AntennaType.polarization_ratio,
       dbo.AntennaType.name AS antennaType_name,
             
          
       dbo.Property.georegion_id AS georegion_id,
       dbo.Property.project_id,
       
       dbo.Property.lat AS Property_lat,
       dbo.Property.lon AS Property_lon,
       
       
       IIF (IsNull(location_type,0) = 0, Property.lat,  Linkend_Antenna.lat) as map_lat,
       IIF (IsNull(location_type,0) = 0, Property.lon,  Linkend_Antenna.lon) as map_lon
       
       
/*       case IsNull(location_type,0)
         when 0 then Property.lat 
         else Linkend_Antenna.lat
       end AS map_lat,
        
       case IsNull(location_type,0)
         when 0 then Property.lon
         else Linkend_Antenna.lon
       end AS map_lon
*/
       
       
FROM dbo.Linkend_Antenna  
     LEFT OUTER JOIN dbo.Property ON dbo.Linkend_Antenna.property_id =
     dbo.Property.id
     LEFT OUTER JOIN dbo.AntennaType ON dbo.Linkend_Antenna.antennaType_id =
     dbo.AntennaType.id
     LEFT OUTER JOIN dbo.LinkEnd ON dbo.Linkend_Antenna.linkend_id =
     dbo.LinkEnd.id

go




IF OBJECT_ID(N'[dbo].[VIEW_link_location]') IS NOT NULL
  DROP VIEW [dbo].[VIEW_link_location]
GO


CREATE VIEW dbo.VIEW_link_location
AS
SELECT dbo.Link.id,
       dbo.Link.objname,

       dbo.Link.name,


--property1_ID,
--property2_ID,

--       P1.id AS property1_ID,
--       P2.id AS property2_ID,

      
       A1.lat AS lat1,
       A1.lon AS lon1,
       A2.lat AS lat2,
       A2.lon AS lon2, 
  
    
/*       P1.lat_wgs AS lat_wgs1,
       P1.lon_wgs AS lon_wgs1,
       P2.lat_wgs AS lat_wgs2,
       P2.lon_wgs AS lon_wgs2, 
*/

      
       Link_Repeater.id as  Link_Repeater_id,
      
       Property_repeater.ID AS link_repeater_Property_ID,
       
       Property_repeater.lat AS repeater_lat,
       Property_repeater.lon AS repeater_lon,
      

       dbo.Link.project_id,
 
       Link.LINKEND1_ID,
       Link.LINKEND2_ID,
 
--      Link.pmp_terminal_id,  
 

 
      -- dbo.Link.has_repeater,
       
       CHECKSUM(A1.lat + A2.lat, A1.lon + A2.lon) AS CHECKSUM
       
FROM dbo.Link 
     
    
     LEFT OUTER JOIN dbo.Link_Repeater ON dbo.Link.id =   dbo.Link_Repeater.Link_ID 
     LEFT OUTER JOIN dbo.Property Property_repeater   ON Property_repeater.id =  dbo.Link_Repeater.property_id 
     
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND1_ID) A1
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND2_ID) A2
     

    -- where Link.id= 159650
    /*
           
    
    
SELECT        dbo.Link.id, dbo.Link.objname, dbo.Link.name, dbo.Link.Property1_id, dbo.Link.Property2_id, dbo.Link_Repeater.id AS Link_Repeater_id, Property_repeater.id AS link_repeater_Property_ID, 
                         Property_repeater.lat AS repeater_lat, Property_repeater.lon AS repeater_lon, dbo.Link.project_id, dbo.Link.linkend1_id, dbo.Link.linkend2_id, dbo.Link.pmp_terminal_id
FROM            dbo.Link LEFT OUTER JOIN
                         dbo.Link_Repeater ON dbo.Link.id = dbo.Link_Repeater.Link_ID LEFT OUTER JOIN
                         dbo.Property AS Property_repeater ON Property_repeater.id = dbo.Link_Repeater.property_id    
    
    
    
FROM dbo.Property P1
     RIGHT OUTER JOIN dbo.Property P2
     
     RIGHT OUTER JOIN dbo.Property Property_repeater
     RIGHT OUTER JOIN dbo.Link
     LEFT OUTER JOIN dbo.Link_Repeater ON dbo.Link.id =
     dbo.Link_Repeater.Link_ID ON Property_repeater.id =
     dbo.Link_Repeater.property_id ON P2.id = dbo.Link.Property2_id ON P1.id =   dbo.Link.Property1_id
     
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND1_ID) A1
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND2_ID) A2
     */

go

--ALTER TABLE dbo.Link  
--   alter column length float not NULL
--GO



-----------------------------------------

IF OBJECT_ID(N'[dbo].[sp_Linkend_Antenna_GetNewName]') IS not  NULL
  DROP PROCEDURE [dbo].[sp_Linkend_Antenna_GetNewName]
GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Linkend_Antenna_GetNewName
(
  @OBJNAME    VARCHAR(50),  
  @ID INT   
)
AS
BEGIN
  declare 
    @new_name 	VARCHAR(50),
    @i 					int;


  if @OBJNAME = ''   RAISERROR('@OBJNAME = ''''', 16, 1)
 

  SET @i=0
  
  ---------------------------------------------
  IF @OBJNAME='template_Linkend'
  ---------------------------------------------  
    WHILE 1=1
    BEGIN
      SET @i=@i+1
--      SET @new_name = '�������' + CONVERT(VARCHAR(255), @i)
      SET @new_name = 'Antenna' + CONVERT(VARCHAR(255), @i)
       
      IF not EXISTS(SELECT * FROM template_Linkend_Antenna  
                    WHERE (template_LinkEnd_id=@ID) and (name=@new_name))
        BREAK;        
    END 
  ELSE    


  ---------------------------------------------
  IF @OBJNAME in ('Linkend', 'pmp_sector', 'pmp_terminal')
  ---------------------------------------------  
    WHILE 1=1
    BEGIN
      SET @i=@i+1
--      SET @new_name = '�������' + CONVERT(VARCHAR(255), @i)
      SET @new_name = 'Antenna' + CONVERT(VARCHAR(255), @i)
       
      IF not EXISTS(SELECT * FROM Linkend_Antenna  WHERE (LinkEnd_id=@ID) and (name=@new_name))
        BREAK;        
    END 
  ELSE  

/*  ---------------------------------------------
  IF @OBJNAME='pmp_sector'
  ---------------------------------------------  
    WHILE 1=1
    BEGIN
      SET @i=@i+1
--      SET @new_name = '�������' + CONVERT(VARCHAR(255), @i)
      SET @new_name = 'Antenna' + CONVERT(VARCHAR(255), @i)
       
      IF not EXISTS(SELECT * FROM Linkend_Antenna  WHERE (pmp_sector_id=@ID) and (name=@new_name))
        BREAK;        
    END 
  ELSE  */
  
/*
  ---------------------------------------------
  IF @OBJNAME='repeater'
  ---------------------------------------------  
    WHILE 1=1
    BEGIN
      SET @i=@i+1
      SET @new_name = '�������' + CONVERT(VARCHAR(255), @i)
       
      IF not EXISTS(SELECT * FROM Linkend_Antenna  WHERE (repeater_id=@ID) and (name=@new_name))
        BREAK;        
    END 
  ELSE  */
/*
  ---------------------------------------------
  IF @OBJNAME='pmp_terminal'
  ---------------------------------------------  
    WHILE 1=1
    BEGIN
      SET @i=@i+1
      SET @new_name = 'Antenna' + CONVERT(VARCHAR(255), @i)
       
      IF not EXISTS(SELECT * FROM Linkend_Antenna  WHERE (pmp_terminal_id=@ID) and (name=@new_name))
        BREAK;        
    END
  
  ELSE 
  
    */
     
  
     RAISERROR('@OBJNAME = ''''', 16, 1)  
 

  SELECT @new_name 

END

go





-----------------------------------------------------------------------

IF OBJECT_ID(N'[dbo].[VIEW_link_location]') IS not  NULL
  DROP view [dbo].[VIEW_link_location]
GO



CREATE VIEW dbo.VIEW_link_location
AS
SELECT dbo.Link.id,
       dbo.Link.objname,

       dbo.Link.name,


--property1_ID,
--property2_ID,

--       P1.id AS property1_ID,
--       P2.id AS property2_ID,

      
       A1.lat AS lat1,
       A1.lon AS lon1,
       A2.lat AS lat2,
       A2.lon AS lon2, 
  
    
/*       P1.lat_wgs AS lat_wgs1,
       P1.lon_wgs AS lon_wgs1,
       P2.lat_wgs AS lat_wgs2,
       P2.lon_wgs AS lon_wgs2, 
*/

      
       Link_Repeater.id as  Link_Repeater_id,
      
       Property_repeater.ID AS link_repeater_Property_ID,
       
       Property_repeater.lat AS repeater_lat,
       Property_repeater.lon AS repeater_lon,
      

       dbo.Link.project_id,
 
       Link.LINKEND1_ID,
       Link.LINKEND2_ID,
 
--      Link.pmp_terminal_id,  
 

 
      -- dbo.Link.has_repeater,
       
       CHECKSUM(A1.lat + A2.lat, A1.lon + A2.lon) AS CHECKSUM
       
FROM dbo.Link 
     
    
     LEFT OUTER JOIN dbo.Link_Repeater ON dbo.Link.id =   dbo.Link_Repeater.Link_ID 
     LEFT OUTER JOIN dbo.Property Property_repeater   ON Property_repeater.id =  dbo.Link_Repeater.property_id 
     
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND1_ID) A1
     cross apply [dbo].fn_Linkend_Antenna_pos (LINKEND2_ID) A2
     

go
   

------------------------------------------



IF OBJECT_ID(N'[dbo].[sp_Linkend_Antenna_Copy_for_LINKEND]') IS not  NULL
  DROP procedure [dbo].[sp_Linkend_Antenna_Copy_for_LINKEND]
GO

/* ---------------------------------------------------------------
created: Alex  
used in:   
error codes: 
	-1 - record exists
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Linkend_Antenna_Copy_for_LINKEND
(
  @SOURCE_LINKEND_ID  int,
  @DEST_LINKEND_ID    int 
   
)
AS
BEGIN
  DECLARE
    @new_id int,
    @Linkend_id int;
    
 
  SELECT name
      INTO #temp   
    FROM Linkend_Antenna 
    where 
      Linkend_id=@DEST_LINKEND_ID
     
  
   Insert Into Linkend_Antenna 
     (
        name,                        
     --   PROJECT_ID,  
                    
        Linkend_id,          
        property_id,
                    
        ANTENNATYPE_ID,
                    
        BAND,		          
        FREQ_MHz,	          
                               
        LOSS,	          
        GAIN,		          
        DIAMETER,	          
        VERT_WIDTH,          
        HORZ_WIDTH,
                                      
        POLARIZATION_STR,
        HEIGHT,
        AZIMUTH,
        TILT,
                    
        LAT,
        LON,
        LOCATION_TYPE                        
                   
       ) 
    SELECT

        NAME,         
    --    PROJECT_ID, 
                               
        @DEST_LINKEND_ID,            
        property_id,
                    
        ANTENNATYPE_ID,
        BAND,		          
        FREQ_MHz,	          
                               
        LOSS,	          
        GAIN,		          
        DIAMETER,	          
        VERT_WIDTH,          
        HORZ_WIDTH,
                   
        POLARIZATION_STR,
        HEIGHT,
        AZIMUTH,
        TILT,
                              
        LAT,
        LON,
        LOCATION_TYPE
                           
 FROM Linkend_Antenna                 
   where 
      (Linkend_id=@SOURCE_LINKEND_ID) and 
      (name NOT IN (SELECT name FROM #temp))

    
   
  RETURN IDENT_CURRENT ('Linkend_Antenna')   
       
 -- RETURN @@ROWCOUNT    
    

END

go


---------------------------------------------------------------

IF OBJECT_ID(N'[dbo].[sp_LinkEnd_Copy]') IS not  NULL
  DROP procedure [dbo].[sp_LinkEnd_Copy]
GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE procedure dbo.sp_LinkEnd_Copy
( 
  @ID 	int,
  @NAME varchar(250)
)
AS
begin
  DECLARE    
    @new_ID 	 int,
  --  @project_id  int,
    @PROPERTY_ID int;

/*
 select top 1 @id =id FROM linkend     
set @NAME='gdfgdfgdfgd'
*/


  SELECT @PROPERTY_ID=PROPERTY_ID
        -- @project_id=PROJECT_ID
    FROM Linkend
    WHERE id=@ID;
    


  INSERT INTO LinkEnd
   (--PROJECT_ID, 
    NAME, 
    PROPERTY_ID
   )
  VALUES
   (--@PROJECT_ID, 
    @NAME, 	
    @PROPERTY_ID
   ) 
 
  
  set @new_id=IDENT_CURRENT ('Linkend')       
 -- set @new_ID = @@IDENTITY
 

  exec sp_Linkend_Antenna_Copy_for_LINKEND @SOURCE_LINKEND_ID=@ID,  @DEST_LINKEND_ID=@new_ID   


  exec sys_sp_Copy_table_record  
     @TABLE_NAME = 'LinkEnd', 
     @ID=@ID,
     @NEW_ID =@NEW_ID


  return @new_ID
     
  
end

go

IF OBJECT_ID(N'[dbo].[sp_LinkEnd_Add]') IS not  NULL
  DROP procedure [dbo].[sp_LinkEnd_Add]
GO



/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE procedure dbo.sp_LinkEnd_Add
( 
  @OBJNAME 			VARCHAR(20)=null,
     
 -- @Template_site_ID int =NULL,
  
 -- @PROJECT_ID INT,

  @NAME 			varchar(250),
  @PROPERTY_ID 	    int,

--  @EQUIPMENT_TYPE int =1,   -- 1- ��������� ������������

  @LINKENDTYPE_ID		int =NULL,
  @LINKENDTYPE_MODE_ID  int =null, 


  @CHANNEL_TYPE VARCHAR(4)=null,


  @Rx_Level_dBm			float = null, --������� ������� �� ����� ���������


  @PASSIVE_ELEMENT_ID   int =null, 
  @PASSIVE_ELEMENT_LOSS float = null,


  -----------------------------
  -- custom equipment
  -----------------------------
  @BAND 					VARCHAR(10) = null,

 -- @POWER_MAX 	float = null,
  @Tx_Freq_MHz      		float = null, 
  @POWER_dBm 				float = 25,    
--  @POWER_LOSS float = null,

  @MODULATION_TYPE  		VARCHAR(50) = '32QAM', 
  @SIGNATURE_HEIGHT_dB  	float = 14,   --������ ���������
  @SIGNATURE_WIDTH_MHz  	float = 24,   --������ ���������
  @THRESHOLD_BER_3  		float = -80, 
  @THRESHOLD_BER_6  		float = -73, 

 -- @EQUALISER_PROFIT  		float = null,
  @Freq_Spacing_MHz  		float = null,       

  @Loss          		float = null,
  @KNG            			float = null,
  @Bitrate_Mbps 			float=NULL, --float


 -----------------------------
  -- PMP Sector
  -----------------------------
--  @PMP_SITE_ID int= null,
 -- @PMP_Sector_ID int= null,
  @PMP_Site_ID  int,

  @Cell_Layer_ID int = null,
  @Calc_Model_ID int = null,
  @Calc_Radius_km float= null,


  -----------------------------
  -- PMP terminal 
  -----------------------------
--  @PMP_SITE_ID int= null,
  @Connected_PMP_Sector_ID int= null,


  @COMMENTS varchar(250)=null

 
--  @IS_GET_INFO bit = 0

)
AS
begin

  SET ANSI_WARNINGS OFF --String or binary data would be truncated.

--return


  ---------------------------------------------
--  IF IsNull(@PROPERTY_ID,0)=0  RAISERROR ('@PROPERTY_ID is NULL', 16, 1);
  IF @OBJNAME = 'pmp_sector' and IsNull(@Pmp_site_ID ,0) = 0
     RAISERROR ('@Pmp_site_ID  NULL', 16, 1);
 -- IF @project_id IS NULL  RAISERROR ('@project_id is NULL', 16, 1);


  IF @Name IS NULL  RAISERROR ('@Name is NULL', 16, 1);

  
  ---------------------------------------------

  DECLARE 
    @EQUIPMENT_TYPE int,
    
  --  @project1_id 	int,
 --   @sName			VARCHAR(200),
    @s 				VARCHAR(100), 
    @id 		    int, 
    @new_ID 		int 
     
   


  ---------------------------------
  -- PMP sector
  ---------------------------------
  if IsNull(@Pmp_site_ID ,0) > 0
     SELECT 
      @PROPERTY_ID=PROPERTY_ID  
    FROM  Pmp_site  
      WHERE id = @Pmp_site_ID 
  
  
   
  
  
  IF not EXISTS(SELECT * FROM Property WHERE (ID=@Property_ID) )
    RAISERROR ('Property not EXISTS', 16, 1);
  
  
--  IF not EXISTS(SELECT * FROM project WHERE (ID=@project_ID) )
 --   RAISERROR ('project not EXISTS', 16, 1);
  
  
  
set @name = link.fn_LinkEnd_name (@PROPERTY_ID, @NAME) 
 
 
/*  SELECT top 1 @id=id FROM LinkEnd  WHERE (PROPERTY_ID=@PROPERTY_ID) and (name=@NAME)
  
  IF @id is not NULL
  BEGIN
    SELECT 'record already exists' as msg,  @id as ID     
    RETURN -1 * @id;  
  END  
*/  
  
  

 -- SELECT @project1_id=project_id FROM PROPERTY WHERE (ID=@PROPERTY_ID);
  
  
--  IF @project1_id IS NULL  RAISERROR ('@PROPERTY_ID - @project1_id is NULL', 16, 1);  
--  IF @project_id <> @project1_id  RAISERROR ('@project_id <> @project1_id', 16, 1);


  

  if @LINKENDTYPE_ID=0       SET @LINKENDTYPE_ID=NULL
  if @LINKENDTYPE_MODE_ID=0  SET @LINKENDTYPE_MODE_ID=NULL
    
  if ISNULL(@SIGNATURE_HEIGHT_dB,0)=0  SET @SIGNATURE_HEIGHT_dB=14 --������ ���������
  if ISNULL(@SIGNATURE_WIDTH_MHz,0)=0  SET @SIGNATURE_WIDTH_MHz=24 --������ ���������
  
  
  
    
  
  
 --print @sName



 --set @PROJECT_ID = dbo.fn_Property_GetProjectID(@PROPERTY_ID);
 
 
 --print @PROJECT_ID
 
 
 INSERT INTO LinkEnd
   (
    NAME, 
    PROPERTY_ID,    
    OBJNAME     
   )
  VALUES
   (
    @NAME, 
    @PROPERTY_ID,
    @OBJNAME 
   ) 
   
 -- set @new_ID = @@IDENTITY 
 set @new_id=IDENT_CURRENT ('LinkEnd')
 

print @new_ID


 
Alter table  LinkEnd Disable trigger all

   
  
  
--    SET @sName=@NAME

 
 
  IF ISNULL(@LINKENDTYPE_ID,0) > 0 
  begin
    set @EQUIPMENT_TYPE =0
    set @Tx_Freq_MHz = dbo.fn_LinkEndType_GetAveFreq_MHz (@LINKENDTYPE_ID)
  END
  ELSE BEGIN
    set @EQUIPMENT_TYPE =1    
    
    if Isnull(@Tx_Freq_MHz,0)=0
      set @Tx_Freq_MHz = dbo.fn_Band_GetAveFreq_MHz (@BAND) 
  END


 

 -- set @new_id=IDENT_CURRENT ('LinkEnd')
  

  
  UPDATE LinkEnd 
  set 
     Rx_Level_dBm       = @Rx_Level_dBm,
     
     PASSIVE_ELEMENT_ID = @PASSIVE_ELEMENT_ID,
     
     CHANNEL_TYPE =  @CHANNEL_TYPE,
     
     equipment_type     = @equipment_type,
        
     COMMENTS = @COMMENTS,
     
 
--  UPDATE PMP_Sector 
--   set 
  --  equipment_type = @equipment_type,
        
 --   LINKENDTYPE_ID     =@LINKENDTYPE_ID,
 --   LINKENDTYPE_MODE_ID=@LINKENDTYPE_MODE_ID,
        
   -- rx_freq_MHz = @TX_FREQ_MHZ,
  --  tx_freq_MHz = @TX_FREQ_MHZ,      	     	       
        
    --PMP sector
    Calc_Model_ID = COALESCE( @Calc_Model_ID,  Calc_Model_ID),
    Cell_Layer_ID = COALESCE( @Cell_Layer_ID,  Cell_Layer_ID),
    Calc_Radius_km= COALESCE( @Calc_Radius_km, Calc_Radius_km),
   
 	PMP_Site_ID = COALESCE( @PMP_Site_ID , PMP_Site_ID),
     
    --PMP terminal
    Connected_PMP_Sector_ID = COALESCE( @Connected_PMP_Sector_ID , Connected_PMP_Sector_ID) 



  WHERE    
    ID = @new_ID   

  

  
   
  if ISNULL(@LINKENDTYPE_ID,0) > 0
  
    exec sp_Object_Update_LinkEndType 
       @OBJNAME='LinkEnd', 
       @ID=@new_ID, 
       @LinkEndType_ID=@LinkEndType_ID, 
       @LinkEndType_mode_ID=@LinkEndType_mode_ID

   
  ELSE
    ------------------------
    -- custom equipment
    ------------------------      
    UPDATE linkend 
    set 
      BAND  = @BAND, 

      tx_freq_MHz = @TX_FREQ_MHZ,
      rx_freq_MHz = @TX_FREQ_MHZ,

      POWER_dBm   = @POWER_dBm,  

      SIGNATURE_HEIGHT =  COALESCE(@SIGNATURE_HEIGHT_dB, SIGNATURE_HEIGHT), 
      SIGNATURE_WIDTH  =  COALESCE(@SIGNATURE_width_MHz, SIGNATURE_width),
     
      MODULATION_TYPE  =  COALESCE(@MODULATION_TYPE, MODULATION_TYPE),
      THRESHOLD_BER_3  =  COALESCE(@THRESHOLD_BER_3, THRESHOLD_BER_3), 
      THRESHOLD_BER_6  =  COALESCE(@THRESHOLD_BER_6, THRESHOLD_BER_6),
      Bitrate_Mbps     =  COALESCE(@Bitrate_Mbps,    Bitrate_Mbps), 
        
      
      
      LOSS=@LOSS,         
      KNG = @KNG,
                 
      MODULATION_COUNT      = m.level_COUNT,
      GOST_53363_modulation = m.GOST_53363_modulation 
     
     from 
        (select top 1 * from Lib.Modulations 
         WHERE name=@MODULATION_TYPE) m     
        
    WHERE    
      linkend.ID = @new_ID
       
      
Alter table  LinkEnd enable trigger all
     

if @Connected_PMP_Sector_ID is not null
begin
  declare 
    @link_id int

  exec @link_id= dbo.sp_PMP_Terminal_Plug  @ID = @new_ID, @PMP_SECTOR_ID = @Connected_PMP_Sector_ID  

  IF @link_id is null
    RAISERROR ('procedure dbo.sp_LinkEnd_Add -  exec @link_id= dbo.sp_PMP_Terminal_Plug - @link_id is null', 16, 1);
  

end


/*   
CREATE PROCEDURE dbo.sp_PMP_Terminal_Plug
( 
  @ID 	    	  int,
  @PMP_SECTOR_ID  int
)
AS*/
       
  
  
--  if @IS_GET_INFO=1
  SELECT id,guid,name,PROPERTY_ID FROM LINKEND WHERE ID=@new_ID
  
  RETURN @new_ID;    

  


end

go

--------------------------------------------
IF OBJECT_ID(N'[dbo].[sp_Link_select_line_points]') IS not  NULL
  DROP procedure [dbo].[sp_Link_select_line_points]
GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Link_select_line_points
(
  @ID int
)
AS
BEGIN
 -- TEST
 -- SELECT top 1 @id= id FROM link 

  -------------------------------------------------------
--  IF @ID IS NULL  RAISERROR ('@ID is NULL', 16, 1);
  -------------------------------------------------------

--  IF EXISTS(SELECT * FROM LINK WHERE (id=@ID) and (linkend1_id>0))
    SELECT lat1,lon1,lat2,lon2 
      FROM view_Link
      WHERE (id=@ID)
  
/*  ELSE

  IF EXISTS(SELECT * FROM LINK WHERE (id=@ID) and (pmp_terminal_id>0))
    SELECT lat1,lon1,lat2,lon2 
      FROM view_PMP_Link
      WHERE (id=@ID)

*/
  RETURN @@ROWCOUNT
 

END

go
---------------------------------------------------------------




if  COL_LENGTH('dbo.Link_repeater','is_active')  is null
  ALTER TABLE dbo.Link_repeater
    ADD is_active int

go



if  COL_LENGTH('dbo.LinkEnd','XPIC_use_2_stvola_diff_pol')  is null
  ALTER TABLE dbo.Linkend
    ADD XPIC_use_2_stvola_diff_pol int

go


IF OBJECT_ID(N'[dbo].[view_Linkend_Antenna_for_calc]') IS not  NULL
  DROP view [dbo].[view_Linkend_Antenna_for_calc]
GO


CREATE VIEW dbo.view_Linkend_Antenna_for_calc
AS
SELECT
       dbo.AntennaType.band AS antennaType_band,
       dbo.Linkend_Antenna.*
       
FROM dbo.Linkend_Antenna
     LEFT OUTER JOIN dbo.AntennaType ON dbo.Linkend_Antenna.antennaType_id =  dbo.AntennaType.id

go



IF OBJECT_ID(N'[dbo].[sp_Link_Select_Item]') IS not  NULL
  DROP PROCEDURE [dbo].[sp_Link_Select_Item]
GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Link_Select_Item 
(
  @ID INT,
  @LINKEND_ID  int 
  
--  @PMP_SECTOR_ID  int =null, 
--  @PMP_TERMINAL_ID int =null 
  
)    
AS
BEGIN



  SET ANSI_WARNINGS OFF --String or binary data would be truncated.


  if @ID is null 
  begin
     set @ID = (select top 1 id from link)
     set @LINKEND_ID = 0

  end

  
   IF @LINKEND_ID=0  
     SET @LINKEND_ID=NULL
 
-- return
 


--  if @ID is null
--  BEGIN
--   SELECT top 1  @id =id  FROM Link   
--     WHERE pmp_sector_id>0
--   SELECT top 1  @id =id, @LINKEND_ID=LINKEND1_ID FROM Link   WHERE @LINKEND_ID>0

  
--    set @ID = 70365
 --   set @LINKEND_ID =124645
--  END

  
  declare    
--    @objname VARCHAR(50),
    
  --  @bOneItem BIT,
    
    @property_id  int,
    @property1_id int,
    @property2_id int,
    
    @parent_guid uniqueidentifier,
        
    @linkend1_id_ int,
    @linkend2_id_ int
--    @pmp_sector_id_   int,
--    @pmp_terminal_id_ int    
    ;
  	




  declare 
     @temp table
     
--  create table [#temp]
      (ID          int NOT NULL,              
       objname     VARCHAR(20) NOT NULL,

       guid        uniqueidentifier,
       parent_guid uniqueidentifier,
       
       name        nVARCHAR(100) NOT NULL,
               
       BAND        VARCHAR(50),
            
      EquipmentType_Name VARCHAR(100),
      
      ---------------
      --propery
      ---------------
       ADDRESS VARCHAR(200),
       
       lat   float,  
       lon   float, 
       

      ---------------
      --LinkEnd
      ---------------
       LinkEndType_ID 			int,      
       LinkEndType_Name 		nVARCHAR(100),
       LinkEndType_Mode_ID 	int,       
      
       MODULATION_TYPE VARCHAR(50),
       bitrate_Mbps    float,  
       
       POWER_dBm      Float,    
       THRESHOLD_BER_3 Float,
       THRESHOLD_BER_6 Float,       
       TX_FREQ_MHz    Float,
       RX_FREQ_MHz    Float,             

       LinkEnd_loss   Float,
  
      ---------------
      --ANTENNA
      ---------------
      AntennaType_ID      int, 
      ANTENNATYPE_NAME    nVARCHAR(100),

      DIAMETER            Float,
      HEIGHT              Float,
      HEIGHT_OLD          Float,      
      AZIMUTH             Float, 

      POLARIZATION_str    VARCHAR(4),
      GAIN                Float,
      TILT                Float,
      vert_width          Float,
      horz_width          Float,
      ANTENNA_loss     Float,
      
      location_type int,
      
      is_master 					bit --master antenna
               
      ) ;      

 
 
  SELECT 
 --      @objname = ISNULL(objname,'link'),

       @property1_id = linkend1.property_id,
       @property2_id = linkend2.property_id,
 
/*
       @property1_id = property1_id,
       @property2_id = property2_id,
*/
       @LINKEND1_ID_ = LINKEND1_ID,
       @LINKEND2_ID_ = LINKEND2_ID

--       @pmp_sector_id_  =pmp_sector_id,
--       @pmp_terminal_id_=pmp_terminal_id
              
    FROM link --view_Link_base
      left JOIN dbo.linkend linkend1 ON dbo.Link.linkend1_id = linkend1.id
      left JOIN dbo.linkend linkend2 ON dbo.Link.linkend2_id = linkend2.id
    
--    FROM Link
    WHERE (link.id = @ID)   


    
--------------------------------------------------
-- �������� ���� �������
--------------------------------------------------
  if (IsNull(@LINKEND_ID,0)>0)
--   and        
--     (IsNull(@LINKEND1_ID_,0)>0) and (IsNull(@LINKEND2_ID_,0)>0) 
  BEGIN
    if @LINKEND_ID = @LINKEND1_ID_
      set @property_id = @property1_id  ELSE 
      
    if @LINKEND_ID = @LINKEND2_ID_
      set @property_id = @property2_id
    ELSE 
      RETURN -1;

    set @property1_id = @property_id
    set @property2_id = @property_id
    set @LINKEND1_ID_ = @LINKEND_ID
    set @LINKEND2_ID_ = @LINKEND_ID
  END  



--------------------------------------------------
-- Property
--------------------------------------------------     
--  insert into [#temp]
  insert into @temp
    (id,guid,name,objname,
     ADDRESS, lat, lon
     )
   select 
     id,guid,name,'Property',
     ADDRESS, lat, lon
     
   from Property      
--   where id in (@property_id) 
   where id in (@property_id, @property1_id, @property2_id) 
   
--  SELECT * FROM #temp  
          
      
      
--------------------------------------------------
-- LinkEnd
--------------------------------------------------     

--    insert into [#temp]
    insert into @temp
      (id,guid,parent_guid,name,objname, 
       LinkEndType_ID,LinkEndType_Name,  EquipmentType_Name,
       LinkEndType_Mode_ID,     
       
       BAND,
       THRESHOLD_BER_3, THRESHOLD_BER_6, 
       POWER_dBm, 
       TX_FREQ_MHz, RX_FREQ_MHz,
            
       bitrate_Mbps,
       MODULATION_TYPE,
       
       LinkEnd_Loss     
       )
     select -- top 1
       id,guid,[Property_guid],name,'LinkEnd',
       LinkEndType_ID, LinkEndType_Name,  LinkEndType_Name,
       LinkEndType_Mode_ID,     

       BAND,
       THRESHOLD_BER_3, THRESHOLD_BER_6, 
       POWER_dBm, 
       TX_FREQ_MHz, RX_FREQ_MHz,
       
       bitrate_Mbps,
       MODULATION_TYPE,

       passive_element_loss
       
     from 
       view_LinkEnd      
--       view_LinkEnd_short      
     where 
        id in (@LINKEND_ID,  @LINKEND1_ID_, @LINKEND2_ID_)
--        id in (@LINKEND_ID)
      

--select * from @temp
--RETURN  

/*
select @LINKEND_ID,  @LINKEND1_ID_, @LINKEND2_ID_
--RETURN  

select * from view_LinkEnd  where id in (@LINKEND_ID,  @LINKEND1_ID_, @LINKEND2_ID_)

RETURN
*/ 

    --------------------
    -- PMP_sector
    --------------------
--    insert into [#temp]

    --------------------
    -- PMP TERMINAL
    --------------------
--    insert into [#temp]
 --  if (@pmp_TERMINAL_id is not null) or (@pmp_TERMINAL_id_ is not null)   



    set @LINKEND1_ID_ = IsNull(@LINKEND1_ID_,0)
    set @LINKEND2_ID_ = IsNull(@LINKEND2_ID_,0)

--    set @pmp_sector_id_   = IsNull(@pmp_sector_id_,0)
--    set @pmp_terminal_id_ = IsNull(@pmp_terminal_id_,0)

    set @LINKEND_ID = IsNull(@LINKEND_ID,0)
--    set @pmp_sector_id   = IsNull(@pmp_sector_id,0)
--    set @pmp_terminal_id = IsNull(@pmp_terminal_id,0)
    


--------------------------------------------------
-- view_LinkEnd_antennas
--------------------------------------------------    
--  insert into [#temp]
  insert into @temp
    (id,guid,parent_guid,name,
     objname,
    
     BAND,
     HEIGHT, HEIGHT_OLD, DIAMETER,AZIMUTH,GAIN,TILT,POLARIZATION_str,
     AntennaType_ID,AntennaType_NAME, EquipmentType_Name,
     vert_width,horz_width,
     ANTENNA_loss,
     is_master,
     
     location_type,
     lat, lon
    )    
   select 
     id,guid,[LinkEnd_guid],name,
     'LINKEND_antenna',
     
     BAND,
     HEIGHT, HEIGHT, DIAMETER,AZIMUTH,GAIN,TILT, POLARIZATION_str,     
     AntennaType_ID,AntennaType_NAME, AntennaType_NAME, 
     vert_width,horz_width,
     loss,
     is_master,

     location_type,
     lat, lon
     
   from view_LinkEnd_antennas 
   where
--        (LINKEND_id      in (@LINKEND_id) )
        (LINKEND_id      in (@LINKEND_id, @LINKEND1_id_, @LINKEND2_id_) )
        
        -- or 
        
    --    (pmp_sector_id   in (@pmp_sector_id,  @pmp_sector_id_) )   or 
     --   (pmp_terminal_id in (@pmp_terminal_id, @pmp_terminal_id_) )
        

--------------------------------------------------
-- view_pmp_sector_antennas
--------------------------------------------------    

 

--------------------------------------------------
-- view_pmp_terminal_antennas
--------------------------------------------------    

-- insert into [#temp]


--  SELECT * FROM #temp  
  SELECT * FROM @temp  
  
  
--  IF @@ROWCOUNT =0  RAISERROR ('@@ROWCOUNT =0', 16, 1);

  
  RETURN @@ROWCOUNT  
  
END


   go

/****** Object:  StoredProcedure [link_calc].[sp_Link_Calc_Reserve]    Script Date: 16.05.2020 10:54:58 ******/

IF OBJECT_ID(N'[link_calc].[sp_Link_Calc_Reserve]') IS not  NULL

DROP PROCEDURE [link_calc].[sp_Link_Calc_Reserve]
GO
/****** Object:  StoredProcedure [link_calc].[sp_Link_Calc_Reserve]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ---------------------------------------------------------------
created: Alex  
used in:  
errors:
  -1: 
  
������ ��������������� ������
  
--------------------------------------------------------------- */
CREATE PROCEDURE link_calc.sp_Link_Calc_Reserve
(
  @Template_Link_id INT,  

  @Property1_id INT,
  @Property2_id INT,  

  @LINKEND1_ID int,
  @LINKEND2_ID int ,
  
  @LAT1 FLOAT=NULL, 
  @LON1 FLOAT=NULL,
    
  @LAT2 FLOAT=NULL, 
  @LON2 FLOAT=NULL,
  
  @AntennaType_ID int=null,
  @LinkEndType_mode_ID int=null

)
AS
BEGIN
/*
 set  @Template_Link_id=1

 
 set @Property1_id =2077607
 set @Property2_id =2077608  

 set @LINKEND1_ID =null
 set @LINKEND2_ID =549866
*/

  declare
--    @Property1_id INT,
--    @Property2_id INT,  
  
/*    @lat1 float,
    @lon1 float,
    @lat2 float,
    @lon2 float,
*/  
   -- @AZIMUTH float, 
    @LENGTH_M float,   
   
    @gain1 float,
    @gain2 float,
    @power float,
    @THRESHOLD float,
    @THRESHOLD_BER_6 float,
    @THRESHOLD_BER_3 float,
   
    @FREQ_GHz float,
    @TX_FREQ_MHz float,
    @RX_FREQ_MHz float,
    @fade_margin float,
    @rx_level float,
    @L float
    ;
    

  -------------------------------------------------------
 -- IF @LENGTH_KM=0  RAISERROR ('@LENGTH_KM=0', 16, 1);
--  IF @FREQ_GHz=0   RAISERROR ('@FREQ_GHz=0', 16, 1);
  -------------------------------------------------------

  if IsNull(@Template_Link_id,0) =0 and
     IsNull(@LINKEND1_ID,0)>0  and  
     IsNull(@LINKEND2_ID,0)>0
  BEGIN
    select @property1_id=property_id  FROM LINKEND  WHERE id=@LINKEND1_ID
    select @property2_id=property_id  FROM LINKEND  WHERE id=@LINKEND2_ID
      

    SELECT top 1 @gain1=gain FROM Linkend_Antenna WHERE LINKEND_id=@LINKEND1_ID
    SELECT top 1 @gain2=gain FROM Linkend_Antenna WHERE LINKEND_id=@LINKEND2_ID



      SELECT top 1 
        @power 		=ISNULL(power_dBm,0),
        @THRESHOLD_BER_6=ISNULL(THRESHOLD_BER_6,0),
        @THRESHOLD_BER_3=ISNULL(THRESHOLD_BER_3,0),
        @TX_FREQ_MHz	=ISNULL(TX_FREQ_MHz,0),
        @RX_FREQ_MHz	=ISNULL(RX_FREQ_MHz,0)
      FROM Linkend 
      WHERE id=@LINKEND1_ID

  END


 -- select @lat1=lat, @lon1=lon FROM Property WHERE id=@property1_id
--  select @lat2=lat, @lon2=lon FROM Property WHERE id=@property2_id
  if @property1_id is not null
    set @LENGTH_M = geo.fn_Length_m_Between_Property (@property1_id,@property2_id)
  else
  if @lat1 is not null
    set @LENGTH_M = geo.fn_Length_m (@lat1, @lon1, @lat2, @lon2)


/*CREATE FUNCTION geo.fn_Length_m 

(
  @LAT1 float=64,
  @Lon1 float=37,

  @LAT2 float=64.1,
  @LON2 float=37.1 
 
)
*/

 declare
--    @AntennaType_id INT,
    @LinkEndType_id INT
 --   @LinkEndType_mode_id INT

  ---------------------------------------------------------------------------  
  if IsNull(@Template_Link_id,0)>0
  ---------------------------------------------------------------------------
  begin
    select 
           @LinkEndType_id = LinkEndType_id,
           @LinkEndType_mode_id = LinkEndType_mode_id,
           @AntennaType_id = AntennaType_id

      from lib.Template_Link
      where id = @Template_Link_id

    if @LinkEndType_mode_id is null 
      select top 1 @LinkEndType_mode_id = id 
        from LinkEndType_mode 
        WHERE LinkendType_id=@LinkendType_id
        order by bitrate_Mbps desc   


     SELECT top 1 
        @power 	=ISNULL(power_max,0),
        @THRESHOLD_BER_6=ISNULL(THRESHOLD_BER_6,0),
        @THRESHOLD_BER_3=ISNULL(THRESHOLD_BER_3,0)
  --      @TX_FREQ_MHz	=ISNULL(TX_FREQ_MHz,0),
  --      @RX_FREQ_MHz	=ISNULL(RX_FREQ_MHz,0)
      FROM LinkEndType_mode 
      WHERE id=@LinkEndType_mode_id

     SELECT top 1 
  --      @power_dBm 	=ISNULL(power_dBm,0),
  --      @THRESHOLD_BER_6=ISNULL(THRESHOLD_BER_6,0),
  --      @THRESHOLD_BER_3=ISNULL(THRESHOLD_BER_3,0),
        @TX_FREQ_MHz	=ISNULL(freq_min,0),
        @RX_FREQ_MHz	=ISNULL(freq_min,0)
      FROM LinkEndType
      WHERE id=@LinkEndType_id


    SELECT top 1 @gain1=gain, @gain2=gain  FROM AntennaType WHERE id=@AntennaType_id
    
  end

else
--=======================================================================
  if IsNull(@Template_Link_id,0)=0 and
     IsNull(@LinkEndType_mode_id,0)>0 and 
  	 IsNull(@AntennaType_id,0)>0 
  ---------------------------------------------------------------------------     
  begin    

     SELECT top 1 
        @power 	=ISNULL(power_max,0),
        @THRESHOLD_BER_6=ISNULL(THRESHOLD_BER_6,0),
        @THRESHOLD_BER_3=ISNULL(THRESHOLD_BER_3,0)
  --      @TX_FREQ_MHz	=ISNULL(TX_FREQ_MHz,0),
  --      @RX_FREQ_MHz	=ISNULL(RX_FREQ_MHz,0)
      FROM LinkEndType_mode 
      WHERE id=@LinkEndType_mode_id

     SELECT top 1 
  --      @power_dBm 	=ISNULL(power_dBm,0),
  --      @THRESHOLD_BER_6=ISNULL(THRESHOLD_BER_6,0),
  --      @THRESHOLD_BER_3=ISNULL(THRESHOLD_BER_3,0),
        @TX_FREQ_MHz	=ISNULL(freq_min,0),
        @RX_FREQ_MHz	=ISNULL(freq_min,0)
      FROM LinkEndType
      WHERE id=
              (select LinkEndType_id from LinkEndType_mode WHERE id=@LinkEndType_mode_id)


    SELECT top 1 @gain1=gain, @gain2=gain  FROM AntennaType WHERE id=@AntennaType_id
    
  end







  -------------------------------------------------------
  IF @gain1 IS NULL BEGIN    
     SELECT 'PPC1: �������� = 0'
     RETURN -1
  END
  
  IF @gain2 IS NULL BEGIN    
     SELECT 'PPC2: �������� = 0'
     RETURN -1
  END  
  
  -------------------------------------------------------



  set @FREQ_GHz = @TX_FREQ_MHz / 1000;
  if @FREQ_GHz=0  set @FREQ_GHz = @RX_FREQ_MHz / 1000;

  set @THRESHOLD = @THRESHOLD_BER_6;
  if @THRESHOLD=0  set @THRESHOLD = @THRESHOLD_BER_3;


  set @L = 41890 * @LENGTH_M/1000 * @FREQ_GHz;
--  set @L = 41890 * @LENGTH_KM * @FREQ_GHz;

  if @L >0
    set @L = 20 * LOG10(@L)
  ELSE
  BEGIN
    SELECT 'FREQ_GHz = 0'
    print  @FREQ_GHz
  --  print  @FREQ_GHz
  
   RETURN -1

  END;

  --�������� �� ����� ��������� dBm
  set @rx_level = @power + @gain1 + @gain2 - @L

  --����� �� ��������� dB
  set @fade_margin =@rx_level - @THRESHOLD

  SELECT @rx_level      as rx_level_dBm, 
   		 @fade_margin   as fade_margin_dB,
         @LENGTH_M/1000 as LENGTH_KM 
     --   @LENGTH_KM 


  
  RETURN @@ROWCOUNT

  

END


go

IF OBJECT_ID(N'[dbo].[sp_Link_Repeater_Add]') IS not  NULL
  DROP PROCEDURE [dbo].[sp_Link_Repeater_Add]
GO


--------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE procedure dbo.sp_Link_Repeater_Add
(    
  @NAME 			varchar(250),
  @PROPERTY_ID 	    int,


  @IS_ACTIVE BIT = 0,

  @ANTENNA1_AntennaType_ID  int=null,
  @ANTENNA2_AntennaType_ID  int=null,

  @ANTENNA1_HEIGHT 			float=null,
  @ANTENNA2_HEIGHT 			float=null,

  @ANTENNA1_gain 			float=null,
  @ANTENNA2_gain 			float=null
)
AS
begin
  ---------------------------------------------
  IF @PROPERTY_ID IS NULL  RAISERROR ('@PROPERTY_ID is NULL', 16, 1);
  IF @Name IS NULL  RAISERROR ('@Name is NULL', 16, 1);
  ---------------------------------------------

  DECLARE     
    @Antenna1_ID 	int,
    @Antenna2_ID 	int,
   ------ @PROJECT_ID int,
    
  --  @project_id 	int,
    @new_ID 		int 
        

-- set @PROJECT_ID = dbo.fn_Property_GetProjectID(@PROPERTY_ID)

 --IF @project_id IS NULL  RAISERROR ('@project_id is NULL', 16, 1);

   /*
  SELECT @PROJECT_ID = project_id 
    FROM Property 
    where id= @PROPERTY_ID*/
 
 
-- (SELECT project_id FROM	Property where Property.id= Link_Repeater.Property_id) 

  INSERT INTO [Link_Repeater]
   (
    NAME, 
    PROPERTY_ID ,
    IS_ACTIVE
   --- project_id    
   )
  VALUES
   (
    @NAME, 
    @PROPERTY_ID,
    @IS_ACTIVE 
    
  --  (SELECT project_id FROM	Property where Property.id= Link_Repeater.Property_id) 
    
 -----   @PROJECT_ID
   ) 
   
  set @new_id= IDENT_CURRENT ('Link_Repeater')   
     
   
  --set @new_ID = @@IDENTITY



  if @ANTENNA1_AntennaType_ID = 0
    set @ANTENNA1_AntennaType_ID = null


  if @ANTENNA2_AntennaType_ID = 0
    set @ANTENNA2_AntennaType_ID = null


  
--  IF (@ANTENNA1_AntennaType_ID IS NOT NULL) and 
--     (@ANTENNA2_AntennaType_ID IS NOT NULL) 
--  BEGIN
    exec @Antenna1_ID = sp_Link_Repeater_Antenna_Add
      @NAME 		    = 'Antenna1',
      @ANTENNATYPE_ID   = @ANTENNA1_AntennaType_ID,
      @HEIGHT           = @ANTENNA1_HEIGHT,
      @GAIN             = @ANTENNA1_GAIN,
      @Link_Repeater_ID = @new_ID,
    --  @DISPLAY_ORDER    = 1,
      @SLOT    = 1

print @Antenna1_ID
-- IF @Antenna1_ID IS NULL  RAISERROR ('@Antenna1_ID is NULL', 16, 1);


    exec @Antenna2_ID = sp_Link_Repeater_Antenna_Add
      @NAME 		    = 'Antenna2',
      @ANTENNATYPE_ID   = @ANTENNA2_AntennaType_ID,
      @HEIGHT           = @ANTENNA2_HEIGHT,
      @GAIN             = @ANTENNA2_GAIN,
      @Link_Repeater_ID = @new_ID,
    --  @DISPLAY_ORDER    = 2,
      @SLOT    = 2
      
      
      
print @Antenna2_ID
-- IF @Antenna2_ID IS NULL  RAISERROR ('@Antenna2_ID is NULL', 16, 1);
      
      
/*
    UPDATE Link_Repeater
    set    
        ANTENNA1_ID=@Antenna1_ID,  
        ANTENNA2_ID=@Antenna2_ID  
       
    WHERE id=@new_id    
    
    */    
 -- END
  	     
  

  IF @Antenna1_ID IS NULL or @Antenna2_ID IS NULL
  begin
    DELETE FROm  Link_Repeater WHERE id= @new_ID
    return -1
  end

   
  
 -- if @IS_GET_INFO=1
  
  SELECT @new_ID as id,
         @Antenna1_ID as Antenna1_ID,
         @Antenna2_ID as Antenna2_ID  
  
  RETURN @new_ID;      
  
  
end

 go



------------------------------------------------------------------------------------


IF OBJECT_ID(N'[dbo].[sp_Link_Update_Length_and_Azimuth]') IS not  NULL
  DROP procedure [dbo].[sp_Link_Update_Length_and_Azimuth]
GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
error:
  -1 : not exists
  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Link_Update_Length_and_Azimuth
(
  @ID   int = 20972
)

AS
BEGIN
  if @ID is null
    set @ID = 10661


--  DECLARE
  --   @AZIMUTH2 float;

--  set @AZIMUTH1=ROUND(@AZIMUTH1,1)
 -- set @AZIMUTH2=ROUND(@AZIMUTH2,1)
/*
  IF @AZIMUTH1<180
     set @AZIMUTH2= @AZIMUTH1 + 180
  ELSE
     set @AZIMUTH2= @AZIMUTH1 - 180
  
   */

  DECLARE  
    @result INT,
  
 --   @Property1_id INT,
 --   @Property2_id INT,
  --  @property_rep_ID int,
    
    @LINK_repeater_ID  int,
    
    @AZIMUTH_rep1 float,
    @AZIMUTH_rep2 float,    
    
    @objname varchar(10),
    
    @AZIMUTH1 float, 
    @AZIMUTH2 float,
     
    @LENGTH_M float, 
        
    @LENGTH1_M float,
    @LENGTH2_M float,    
        
    @lat_rep float,
    @lon_rep float,
    
 --   @has_repeater bit,

    @lat1 float,
    @lon1 float,
    @lat2 float,
    @lon2 float;

  DECLARE
     @ilinkend1_id int,
     @ilinkend2_id int
     
  --   @pmp_terminal_id int ;



-- IF @PROPERTY_ID IS NULL  RAISERROR ('@PROPERTY_ID is NULL', 16, 1);



  IF NOT EXISTS(SELECT * FROM Link WHERE (id=@ID))
    RAISERROR (' IF NOT EXISTS(SELECT * FROM Link WHERE (id=@ID))', 16, 1);
  --  RETURN -1


  	
  SELECT    
      @objname = objname,  
     
       @iLINKEND1_ID = LINKEND1_ID,
       @iLINKEND2_ID = LINKEND2_ID,
 --      @pmp_terminal_id = pmp_terminal_id,  
  
  
       @LAT1 = lat1,
       @LON1 = lon1,
         
       @LAT2 = lat2,
       @LON2 = lon2,
              
       @LINK_repeater_ID = link_repeater_ID,      
       @LAT_rep = repeater_lat,
       @LON_rep = repeater_lon
               
              
  FROM VIEW_link_location
     WHERE (id = @ID)   


  IF (@LAT1=0) or (@LAT2=0) or (@Lon1=0) or (@Lon2=0) 
    RAISERROR (' PROCEDURE dbo.sp_Link_Update_Length_and_Azimuth - (@LAT1=0) or (@LAT2=0) or (@Lon1=0) or (@Lon2=0)  ', 16, 1);




  -------------------------------------------
  IF @LINK_repeater_ID IS NOT NULL
  -------------------------------------------
  BEGIN
  

    
    set @AZIMUTH_rep1 = dbo.fn_AZIMUTH(@LAT_rep, @Lon_rep,  @LAT1, @LON1, null)
    set @AZIMUTH_rep2 = dbo.fn_AZIMUTH(@LAT_rep, @Lon_rep,  @LAT2, @LON2, null)
              
      
 --   return
    
          
    set @AZIMUTH1 = dbo.fn_AZIMUTH(@LAT1, @Lon1,  @LAT_rep, @Lon_rep, null)
    set @AZIMUTH2 = dbo.fn_AZIMUTH(@LAT2, @Lon2,  @LAT_rep, @Lon_rep, null)
           
           
    set @LENGTH1_M = dbo.fn_AZIMUTH(@LAT1, @Lon1, @LAT_rep, @Lon_rep, 'LENGTH')
    set @LENGTH2_M = dbo.fn_AZIMUTH(@LAT2, @Lon2, @LAT_rep, @Lon_rep, 'LENGTH')
    SET @LENGTH_M = @LENGTH1_M + @LENGTH2_M               
    
    
    UPDATE Link_Repeater_Antenna
      SET AZIMUTH = @AZIMUTH_rep1
      WHERE (Link_Repeater_id = @LINK_repeater_ID) and (slot = 1)
    
    UPDATE Link_Repeater_Antenna
      SET AZIMUTH = @AZIMUTH_rep2
      WHERE (Link_Repeater_id = @LINK_repeater_ID) and (slot = 2)

           
   --   select * from Link_Repeater_Antenna  WHERE (Link_Repeater_id = @LINK_repeater_ID) 
      
      
  
  END 
  -------------------------------------------
  ELSE BEGIN
  -------------------------------------------  
       
    set @AZIMUTH1 = dbo.fn_AZIMUTH(@LAT1, @Lon1,  @LAT2, @LON2, null)
    set @AZIMUTH2 = dbo.fn_AZIMUTH(@LAT2, @Lon2,  @LAT1, @LON1, null)
    set @LENGTH_M = dbo.fn_AZIMUTH(@LAT2, @Lon2,  @LAT1, @LON1, 'LENGTH')
 
  
--  select @AZIMUTH1, @AZIMUTH2, @LENGTH_M
  
/*
DECLARE @g geography = 'LINESTRING(-120 45, -120 0, -90 0)';   
SELECT @g.EnvelopeAngle();  
*/
  
  END


  UPDATE Link
    SET   
      LENGTH   = @LENGTH_M, 
      AZIMUTH1 = @AZIMUTH1, 
      AZIMUTH2 = @AZIMUTH2

    WHERE 
        id = @ID
  
  
  set @result = @@ROWCOUNT
  

--  if @iLINKEND1_ID IS NOT NULL
--  BEGIN
  if IsNull(@objname,'') <> 'pmp_link' 
    UPDATE Linkend_Antenna SET Azimuth = @AZIMUTH1 WHERE (LINKEND_id = @iLINKEND1_ID) 
  
  
--@iLINKEND2_ID 
  UPDATE Linkend_Antenna SET Azimuth = @AZIMUTH2 WHERE (LINKEND_id = @iLINKEND2_ID) 
--  END
  
/*  
  select @objname,  @AZIMUTH1, @AZIMUTH2, @LENGTH_M, @iLINKEND1_ID,  @iLINKEND2_ID


select * from Linkend_Antenna WHERE (LINKEND_id = @iLINKEND1_ID) 
select * from Linkend_Antenna WHERE (LINKEND_id = @iLINKEND2_ID) 


*/
  

  RETURN @result
  
END

go


----------------------------------------------------------
IF OBJECT_ID(N'dbo.sp_Task_Link_Optimize_equipment') IS not  NULL
  DROP PROCEDURE dbo.sp_Task_Link_Optimize_equipment
GO


---------------------------------------------------------
CREATE PROCEDURE dbo.sp_Task_Link_Optimize_equipment
---------------------------------------------------------
(
  @OBJNAME varchar(100),

  @BAND varchar(50), 

  
--  @TABLENAME varchar(100),
  
  @BitRate_Mbps int=null
 -- @BAND varchar(50)=null
)

AS
BEGIN
/*
  set @OBJNAME = 'antenna_type'
  set @TABLENAME = 'AntennaType' 

  set @BITRATE = 250;


  set @BITRATE = COALESCE(@BITRATE, 0);
*/


--set @OBJNAME = 'antenna_type'


/* DECLARE 
    @folders table (id int, 
    							  name varchar(100), 
                    path varchar(1000));
*/

--  INSERT INTO @folders (id, path) 
    SELECT 
           id , dbo.fn_Folder_FullPath(id) as folder_name
       INTO #temp
    
       FROM Folder WHERE [type]=@OBJNAME


--	SELECT * from #temp   
  
  
  
--	SELECT M.* , F.path from AntennaType M left join  @folders F on M.folder_id = F.id 



/*


------------------------------
if @OBJNAME = 'LinkEndType_mode'
------------------------------
  SELECT * FROM  LinkEndType_mode  WHERE  BITRATE_MBPS >= @BITRATE
*/



------------------------------
if @OBJNAME = 'antenna_type' or @OBJNAME = 'antenna' 
------------------------------
   SELECT  -- top 5 
        M.* , F.folder_name, cast(0 as bit) as checked 
     from -- AntennaType
      view_Task_Opti_Antennas

           M left join  #temp F on M.folder_id = F.id 
           
    WHERE diameter>0       
      and   
--        (band = COALESCE(@band , band) )
        (band = @band) 


--  IF @OBJNAME = 'antenna_type' 

------------------------------
ELSE
------------------------------

  SELECT  -- top 5 
  	  M.* , F.folder_name, cast(0 as bit) as checked 
    from LinkEndType M 
    left join  #temp F on M.folder_id = F.id
    
    WHERE
        (band = @band)
        and  (m.id in (SELECT LinkEndType_id FROM  LinkEndType_mode  WHERE  BITRATE_MBPS >= @BITRATE_MBPS))
      
--    where m.id in
 --      (select LinkEndType_id from LinkEndType_Mode   )


  return @@ROWCOUNT

-- where bitrate_mbps >  @BITRATE

  /* Procedure body */
END
go


----------------------------------------------------------
IF OBJECT_ID(N'dbo.view_Task_Opti_Antennas') IS not  NULL
  DROP VIEW dbo.view_Task_Opti_Antennas 
GO


CREATE VIEW dbo.view_Task_Opti_Antennas 
AS
  SELECT 
    AntennaType.id,
    AntennaType.folder_id,
    AntennaType.name,
    AntennaType.gain,
    AntennaType.freq,
    AntennaType.diameter,
    AntennaType.vert_width,
    AntennaType.horz_width,
    AntennaType.band,
    AntennaType.polarization_str
  FROM
    AntennaType
    
    where diameter>0

go

-------------------------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID(N'dbo.sp_Link_Select') IS not  NULL
  DROP PROCEDURE dbo.sp_Link_Select
GO


/* ---------------------------------------------------------------
created: Alex  
used in:  
 -  ���������� ��� ��������� � ������� ������ VIEW
--------------------------------------------------------------- */
CREATE  PROCEDURE dbo.sp_Link_Select 
(
	@ID int = 10737
)

AS
BEGIN
 -- declare 
  --  @objname varchar(10);
    
  ---------------------------------------------
  
--  SELECT @objname = objname 
 --   FROM  Link
  --  WHERE  (id = @ID)
    
    
--  if ( @objname = 'link'  ) or ( @objname is null )
   
 SELECT * FROM view_link_report WHERE id=@ID
-- SELECT * FROM view_link WHERE id=@ID
    
    
--  else 
--    SELECT * FROM view_PMP_LINK WHERE id=@ID
  
  return @@rowcount
         
END
go

----------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID(N'[dbo].[sp_Link_Update_LinkEndType]') IS NOT NULL
  DROP procedure dbo.sp_Link_Update_LinkEndType
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Link_Update_LinkEndType 
(
--  @OBJNAME			varchar(50),
  @LINK_ID   	  	int, -- Template_Cell ID
  
  @LinkEndType_ID 		int,
  @LinkEndType_Mode_ID 	int--,  --= null
  
 -- @LinkEndType_mode_ID int=NULL
)
AS
BEGIN

/*
  set @OBJNAME	= 'pmp_terminal'
  set @ID   	= 498 
  
  set @LinkEndType_ID = 168
  set @LinkEndType_Mode_ID  = 1363 
*/


  ---------------------------------------------
--  IF @OBJNAME IS NULL  RAISERROR ('@OBJNAME is NULL', 16, 1);
  IF @LINK_ID        IS NULL  RAISERROR ('@LINK_ID is NULL', 16, 1);
  IF @LinkEndType_ID IS NULL  RAISERROR ('@LinkEndType_ID is NULL', 16, 1);
  ---------------------------------------------
  
 -- IF @Mode IS NULL  RAISERROR ('@Mode is NULL', 16, 1);


  DECLARE 
     @linkend1_id INT,
     @linkend2_id INT,
  
 	   @mode 			  int,
	   @band 			  varchar(20),
     @freq_MHz 		  float,
     @failure_period  float,
     @equaliser_profit	float;

  --   @iLinkEnd_Type_ID  int ,
    -- @POWER_MAX 		float, 
 
  SELECT 
       @band = Band,
  --     @POWER_MAX 			=  POWER_MAX
       @failure_period 	= failure_period,
       @equaliser_profit = equaliser_profit  
                
    FROM LinkEndType
    WHERE (id = @LinkEndType_ID)   
   
  
  set @freq_MHz = dbo.fn_LinkEndType_GetAveFreq_MHz(@LinkEndType_ID);


  SELECT @linkend1_id = LINKEND1_ID, 
    	   @linkend2_id = LINKEND2_ID
      FROM Link 
      WHERE (id = @LINK_ID)  


--        db_Par(FLD_EQUIPMENT_TYPE,      IIF(rec.LinkEndType_ID>0, DEF_TYPE_EQUIPMENT, DEF_NON_TYPE_EQUIPMENT)),
--  //������������: 0-�������, 1-���������
--  DEF_TYPE_EQUIPMENT     = 0;
--  DEF_NON_TYPE_EQUIPMENT = 1;


---------------------------------------------
 -- IF @OBJNAME='LinkEnd'
---------------------------------------------  
 -- BEGIN
  UPDATE LinkEnd SET
      EQUIPMENT_TYPE = 0,		    
          
      LinkEndType_ID	  = @LinkEndType_ID,
    --  LinkEndType_Mode_ID = @LinkEndType_Mode_ID,
--        Mode = @mode,

      BAND 			= @BAND,
      Tx_freq_MHz     = @freq_MHz,
      Rx_freq_MHz     = @freq_MHz,
      
--   Mode 			  = @Mode,
      

  --  POWER_MAX         = @POWER_MAX,
--    POWER_dBm         = @POWER_MAX,

 --   Tx_freq_MHz   	  = @freq_MHz,
--    Rx_freq_MHz   	  = @freq_MHz
          
    failure_period   = @failure_period,
    equaliser_profit = @equaliser_profit      
     
  WHERE (id in (@linkend1_id, @linkend2_id) )  

 -- END  
  

   exec sp_Object_Update_LinkEndType_Mode 
     @OBJNAME='LinkEnd', 
--     @ID=@LINK_ID, 
     @ID=@linkend1_id, 
   --  @LinkEndType_ID=@LinkEndType_ID, 
     @LinkEndType_mode_ID=@LinkEndType_mode_ID


   exec sp_Object_Update_LinkEndType_Mode 
     @OBJNAME='LinkEnd', 
--     @ID=@LINK_ID, 
     @ID=@linkend2_id, 
   --  @LinkEndType_ID=@LinkEndType_ID, 
     @LinkEndType_mode_ID=@LinkEndType_mode_ID



  RETURN @@ROWCOUNT   
 

  
 -- exec sp_LinkEnd_Update_Mode @ID=@ID, @Mode=@Mode --, @LinkEndType_mode_ID=@LinkEndType_mode_ID
 
END
/*

  ---------------------------------------------
  IF @Linkend_ID is not null
  ---------------------------------------------
  BEGIN
    UPDATE Linkend_Antenna SET 
  		    Gain            =m.Gain,

          Freq_MHz         =m.Freq,
--          Freq_MHz        =m.Freq_MHz,
          Diameter        =m.Diameter,
          Polarization_str=m.Polarization_str,
          Vert_Width      =m.Vert_Width,
          Horz_Width      =m.Horz_Width,
          
          antennaType_id  =@AntennaType_ID  
      
     from 
        (select * from AntennaType WHERE id=@AntennaType_ID) m     
     WHERE 
        Linkend_Antenna.Linkend_ID=@Linkend_ID
  END ELSE

    
  
  */
go
------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID(N'[dbo].[sp_Link_Update_AntennaType]') IS NOT NULL
  DROP PROCEDURE dbo.sp_Link_Update_AntennaType 
GO


/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Link_Update_AntennaType 
(
--  @ID   			int,   
  @Link_ID   			int,   
  @AntennaType_ID int   
)
AS
BEGIN
  DECLARE
     @temp  table (id int);

  DECLARE 
    @id1 INT,
    @linkend1_id INT,
    @linkend2_id INT
     
  select 
  		@linkend1_id=linkend1_id,  
      @linkend2_id=linkend2_id 
    FROM Link 
    WHERE id = @Link_ID

   
  
  INSERT INTO @temp (id) 
    select id FROM Linkend_Antenna 
      WHERE linkend_id in (@linkend1_id,@linkend2_id)
    
    
    
  WHILE Exists( select id FROM @temp  )    
  BEGIN
     select top 1 @id1=id  FROM @temp
  
    exec sp_Linkend_Antenna_Update_AntennaType @ID=@id1, @AntennaType_ID=@AntennaType_ID

    DELETE FROM @temp WHERE id=@id1
  END;


  RETURN @@ROWCOUNT   
  
 
END
go
----------------------------------------------------------------------------

IF OBJECT_ID(N'link.sp_Link_Reverse') IS  not  NULL
  DROP PROCEDURE link.sp_Link_Reverse
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
������� �������
--------------------------------------------------------------- */
CREATE  PROCEDURE link.sp_Link_Reverse

(
  @ID int
)  

AS
BEGIN
  if  @ID is null
    set @id = (select top 1 id from link)

--select linkend1_id,linkend2_id from link where  (id = @ID)


  delete from Link_reflection_points where link_id=@ID

  UPDATE link
    SET 
      linkend1_id = linkend2_id,
      linkend2_id = linkend1_id,
      
      Profile_XML = null
      

    WHERE (id = @ID)


--select linkend1_id,linkend2_id from link where  (id = @ID)

  return @@ROWCOUNT

END;

go
----------------------------------------------


IF OBJECT_ID(N'dbo.sp_Link_Add') IS  not  NULL
  DROP PROCEDURE [dbo].[sp_Link_Add]
GO


/****** Object:  StoredProcedure [dbo].[sp_Link_Add]    Script Date: 16.05.2020 10:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Link_Add]
(
    @PROJECT_ID int,
    
    @OBJNAME varchar(50) ,
    
    -------------------------------------
    -- TEMPLATE_SITE_ID
    -------------------------------------
    @TEMPLATE_LINK_ID int = null,    
--    @TEMPLATE_SITE_ID int = null,    
    @PROPERTY1_ID int = null,
    @PROPERTY2_ID int = null,
    
    @NAME varchar(250),
    
    @FOLDER_ID int = null,
    @CLUTTER_MODEL_ID int = null,
    
    @LINKEND1_ID int ,
    @LINKEND2_ID int ,
        
--    @PMP_SECTOR_ID int = null,    
--    @PMP_TERMINAL_ID int = null,
    
    
    @Comments varchar(250) = null,
    @Status_id int = null,
    
  --  @NAME_SDB varchar(20) = null,
--    @NAME_TN varchar(20) = null,
    
    @SESR float = null,
    @KNG float = null,
    @CALC_METHOD int = 0      --  null        --  @State_id   			int =null, -- ������: ��������, �����������...   --  @IS_GET_INFO 			bit = 0
)    
AS
begin

  SET ANSI_WARNINGS OFF --String or binary data would be truncated.



  IF @FOLDER_ID=0 SET @FOLDER_ID=NULL;
  --if @LINK_REPEATER_ID=0 set @LINK_REPEATER_ID=null


  DECLARE 
--    @len_m float, 
  --  @sName VARCHAR(100),
    @lat1 float,
    @lon1 float,
    @lat2 float,
    @lon2 float,

    @AZIMUTH1 float,
    @AZIMUTH2 float,
     
--    @property1_id INT,
--    @property2_id INT,
    
    @id INT,
    @new_id INT;

  --  @project_id  int,  

 --   @project1_id  int,  
  --  @project2_id  int;

  --  @project_id  int,  
 --   @objname  VARCHAR(10);
   
 
 --    @aLat float,
  --   @aLon float
     
  --258929
  
    -- Name     : ����������� � ���� � ��������� before insert
  -- Address  : ����������� � ���� � ��������� before insert
  
   
  
  IF @Name       IS NULL  RAISERROR ('@Name is NULL', 16, 1);
--  IF @LENGTH_M=0  RAISERROR ('@LENGTH_M=0', 16, 1);
  
--   set @sName = [dbo].fn_Link_get_new_name (@PROJECT_ID, @Name)


--  set @LENGTH_M = round(@LENGTH_M,1)



  
 
--------------------------
-- check if name exists
-------------------------- 
  SELECT top 1 @id=id 
  	FROM link 
    WHERE (name=@NAME) and (PROJECT_ID=@PROJECT_ID)
           and ((@Folder_id IS NULL) or (Folder_id=@Folder_id))  

  IF @id is not NULL
  BEGIN
    SELECT 'record already exists' as msg,  @id as ID     
    RETURN -@id;  
  END  







  IF NOT EXISTS(SELECT * FROM Status WHERE (id=@status_id))
  BEGIN 
    set @status_id=NULL
  END
/* 

  IF NOT EXISTS(SELECT * FROM lib_Link_State WHERE (id=@state_id))
  BEGIN 
    set @state_id=NULL
  END
*/


  IF NOT EXISTS (SELECT * FROM ClutterModel WHERE (ID=@CLUTTER_MODEL_ID))
  BEGIN 
    SELECT top 1 @CLUTTER_MODEL_ID=id FROM ClutterModel 
    
    if @CLUTTER_MODEL_ID is NULL  RAISERROR ('@CLUTTER_MODEL_ID is NULL', 16, 1);
    
   -- PRINT 'SELECT * FROM ClutterModel WHERE (ID=@CLUTTER_MODEL_ID)'
     
  END
  
  

  

  -----------------------------------------  
--  IF (@LINKEND1_ID IS NOT NULL ) and (@LINKEND2_ID IS NOT NULL )
  -----------------------------------------
--  BEGIN
 /*   IF NOT EXISTS(SELECT * FROM LinkEND WHERE ID=@LINKEND1_ID)
    BEGIN
      RAISERROR ('@LINKEND1_ID not exists', 16, 1);
      return -1      
    END      

    IF NOT EXISTS(SELECT * FROM LinkEND WHERE ID=@LINKEND2_ID)
    BEGIN
      RAISERROR ('@LINKEND2_ID not exists', 16, 1);
      return -2
    END      
*/
 /*
    IF EXISTS(SELECT * FROM Link WHERE (LINKEND1_ID in (@LINKEND1_ID,@LINKEND2_ID)) or 
                                       (LINKEND2_ID in (@LINKEND1_ID,@LINKEND2_ID)))
    BEGIN
      RAISERROR (' IF EXISTS(SELECT * FROM Link WHERE (LINKEND1_ID in (@LINKEND1_ID,@LINKEND2_ID)) or (LINKEND2_ID in (@LINKEND1_ID,@LINKEND2_ID)))', 16, 1);

      SELECT * FROM Link WHERE (LINKEND1_ID in (@LINKEND1_ID,@LINKEND2_ID)) or 
                               (LINKEND2_ID in (@LINKEND1_ID,@LINKEND2_ID))


      return -3
    END      
 
*/

   -- IF EXISTS(SELECT * FROM Link WHERE (LINKEND1_ID=@LINKEND1_ID) and (LINKEND2_ID=@LINKEND2_ID)) or 
  --     EXISTS(SELECT * FROM Link WHERE (LINKEND1_ID=@LINKEND2_ID) and (LINKEND2_ID=@LINKEND1_ID))
  --    RAISERROR ('@LINKEND1_ID<>@LINKEND2_ID', 16, 1);

--    set @objname='link'



--------------------------------------------------------------------
-- 
--------------------------------------------------------------------
if @TEMPLATE_LINK_ID is not null
begin
  IF @property1_id is null    RAISERROR ('@property1_id', 16, 1);
  IF @property2_id is null    RAISERROR ('@property2_id', 16, 1);


  delete from LINKEND where id in (@LINKEND1_ID,@LINKEND2_ID)

  exec @LINKEND1_ID = [lib].sp_Template_Link_apply_to_link @property_id=@property1_id, @TEMPLATE_LINK_ID=@TEMPLATE_LINK_ID
  exec @LINKEND2_ID = [lib].sp_Template_Link_apply_to_link @property_id=@property2_id, @TEMPLATE_LINK_ID=@TEMPLATE_LINK_ID

end else
begin
  SELECT @property1_id=property_id  FROM LinkEnd WHERE (ID=@LINKEND1_ID);            
  SELECT @property2_id=property_id  FROM LinkEnd WHERE (ID=@LINKEND2_ID);

end
 

--    IF @project1_id  IS NULL  BEGIN RAISERROR ('@project1_id is NULL', 16, 1);  return -4  END 
--    IF @project2_id  IS NULL  BEGIN RAISERROR ('@project2_id is NULL', 16, 1);  return -5  END 
--    IF @project1_id <> @project2_id  BEGIN RAISERROR ('@project1_id <> @project1_id', 16, 1);  return -6 END 
--    IF @property1_id = @property2_id  BEGIN RAISERROR ('@property1_id = @property2_id ', 16, 1);  return -8 END 
 
 
           
--    SELECT @property2_id=property_id 
  --    FROM LinkEnd WHERE (ID=@LINKEND2_ID)

    ----------------------------------
    --  set CHANNEL_TYPE 'low','high'
    ----------------------------------
  --  UPDATE LinkEnd set CHANNEL_TYPE='low'  WHERE (ID=@LINKEND1_ID);
  --  UPDATE LinkEnd set CHANNEL_TYPE='high' WHERE (ID=@LINKEND2_ID);


   
--  END ELSE  
 -- BEGIN
 -- print  ''
  
 --   set @objname='pmp_link'
    
 --   set @property1_id = dbo.fn_PMP_Sector_Get_Property_id(@PMP_SECTOR_ID)
--IF @property1_id  IS NULL  RAISERROR ('@property1_id is NULL   111111111', 16, 1); 

/*
    SELECT 
    	@property1_id=property_id   --, @project2_id=project_id 
     --   @project1_id =project_id 
      FROM view_PMP_sector --_with_project_id
      WHERE (ID=@Pmp_sector_ID)
      


    SELECT 
    	@project1_id=project_id 
   	FROM Property 
    WHERE (ID=@property1_id);


--    SELECT @property1_id = property_id
  --  FROM PMP_Sector WHERE id= @PMP_SECTOR_ID
    --LEFT OUTER JOIN PMP_site  ON PMP_Sector.pmp_site_id = PMP_site.id

    SELECT 
    	@property2_id=property_id  
    --    @project2_id=project_id 
      FROM view_PMP_TERMINAL --_with_project_id 
      WHERE (ID=@PMP_TERMINAL_ID)
     
     */
      
 --   IF @project1_id   IS NULL BEGIN RAISERROR ('@project1_id is NULL', 16, 1);   return -4  END 
--    IF @project2_id   IS NULL BEGIN RAISERROR ('@project2_id is NULL', 16, 1);   return -4  END     
--    IF @property1_id  IS NULL  RAISERROR ('@property1_id is NULL', 16, 1); 
--    IF @property2_id  IS NULL  RAISERROR ('@property2_id is NULL', 16, 1); 
--    IF @project1_id <> @project2_id BEGIN  RAISERROR ('@project1_id <> @project1_id', 16, 1);  return -4  END 
 

 -- END




--  exec sp_Object_GetNewName @OBJNAME='Link', @NAME=@NAME, 
 --       @PROJECT_ID=@project_id, @FOLDER_ID=@FOLDER_ID, @RESULT=@sName out


--  set @len_m = dbo.fn_Property_Get_Length_m (@property1_id, @property2_id);
  --IF @len_m > 100000 
   --  RAISERROR ('@len_m > 100 000 ', 16, 1);    
  


  INSERT INTO Link
     (PROJECT_ID, 
      NAME, 
      FOLDER_ID,  
      objname, 
  
      LINKEND1_ID, 
      LINKEND2_ID, 
    
--      PMP_SECTOR_ID, 
--      PMP_TERMINAL_ID,
      
      property1_id,
      property2_id,
         
      length --temp
      
      )
  VALUES
     (@project_id, 
      @NAME, --@sNAME, 
      @FOLDER_ID,
      @objname, 
      
      @LINKEND1_ID, 
      @LINKEND2_ID,
    
--      @PMP_SECTOR_ID, 
--      @PMP_TERMINAL_ID,
      
      @property1_id,
      @property2_id,
             
     
      1 --length 
      ) 
   
     
--  set @new_id=@@IDENTITY;
        
  set @new_id=IDENT_CURRENT ('Link')
     


Alter table  link  Disable trigger all

  
  
  UPDATE Link
  SET    
    CLUTTER_MODEL_ID = @CLUTTER_MODEL_ID,
     
    status_id=@status_id,
      
  --  NAME_SDB=@NAME_SDB,
  --  NAME_TN =@NAME_TN,
    Comments=@Comments,
    
   -- Link_REPEATER_ID=@Link_REPEATER_ID,
      
    SESR= @SESR,
    KNG = @KNG,
 --   SPACE_LIMIT = @SPACE_LIMIT,
    CALC_METHOD = @CALC_METHOD
    
  WHERE 
      id = @new_ID

  exec sp_Link_Update_Length_and_Azimuth @id = @new_ID
  
  Alter table  link ENABLE trigger all
  
  
  -------------------------------------------
  select @lat1=lat, @lon1=lon  FROM [Property]  WHERE id=@property1_id    
  select @lat2=lat, @lon2=lon  FROM [Property]  WHERE id=@property2_id

--RETURN


exec dbo.sp_Link_rename_linkends_UPD @new_ID




  
  --  IF @@IDENTITY=0  RAISERROR ('@@IDENTITY=0', 16, 1);
  
    
 -- if @IS_GET_INFO=1
    SELECT id,guid,name,  
    	--	LENGTH_M,	
    		@lat1 as lat1, @lon1 as lon1,  
    		@lat2 as lat2, @lon2 as lon2
      FROM link 
      WHERE ID=@new_id
  
  RETURN @new_id;     
    
--   RETURN @@IDENTITY;    
        
end

/*


Alter table  property Disable trigger all


--  set @new_id=@@IDENTITY
  
    UPDATE property
    SET     
      TOWER_HEIGHT = @TOWER_HEIGHT,
    
      GROUND_HEIGHT =@GROUND_HEIGHT,
      CLUTTER_HEIGHT=@CLUTTER_HEIGHT,
      clutter_name  =@clutter_name,
      
      LAT_WGS = @LAT_WGS,
      LON_WGS = @LON_WGS,      
      
      LAT_CK95 = @LAT_CK95,
      LON_CK95 = @LON_CK95,
            
      ADDRESS 	=@ADDRESS,
      CITY 		=@CITY,
      GEOREGION_ID =@GEOREGION_ID,


      State_id=@State_id, 
      Placement_id=@Placement_id,
     
      Comments=@Comments
    
 
      
   	WHERE 
    	id = @new_id
  
  exec sp_Property_Update_lat_lon_kras_wgs @ID=@new_id
  

Alter table  property ENABLE trigger all


*/

GO
---------------------------------------------------------

IF OBJECT_ID(N'link.sp_property_search_top_10_SEL') IS  not  NULL
  DROP PROCEDURE link.sp_property_search_top_10_SEL
GO



CREATE PROCEDURE link.sp_property_search_top_10_SEL
(
  @project_id int, -- = 4,
  @SEARCH_TEXT varchar(50) =null
)

AS
BEGIN
  set @SEARCH_TEXT = RTRIM (LTRIM (IsNull(@SEARCH_TEXT,'')  ))
  
  if @SEARCH_TEXT=''
    select top 50 id,name
      from Property
      where project_id=@project_id
       -- and name LIKE '%' + TRIM(@SEARCH_TEXT) + '%'
      
  ELSE

    select  top 10 id,name
      from Property
      where project_id=@project_id
        and name LIKE '%' + @SEARCH_TEXT + '%'



  /* Procedure body */
END

go





