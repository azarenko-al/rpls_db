

IF OBJECT_ID(N'[dbo].[view_Property_]') IS NOT NULL
  DROP view [dbo].[view_Property_]


GO


GO

CREATE VIEW dbo.view_Property_
AS
SELECT dbo.Property.*,
       dbo.GeoRegion.name AS GeoRegion_Name,
       
       Status_Values_placement.name AS placement_name,
       Status_Values_for_state.name AS state_name,
              
       
       dbo.fn_FormatDegree(lat_wgs,'') as lat_wgs_str_,
       
       (select Count(*) from Linkend_Antenna where Property_id=id) as Linkend_Antenna_count
       
       
       
       
FROM dbo.Property
     LEFT OUTER JOIN dbo.GeoRegion ON dbo.Property.georegion_id =  dbo.GeoRegion.id
     LEFT OUTER JOIN dbo.Status Status_Values_placement ON  dbo.Property.placement_id = Status_Values_placement.id
     LEFT OUTER JOIN dbo.Status Status_Values_for_state ON dbo.Property.state_id  = Status_Values_for_state.id

go
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


----------------------------------------------------------------------
IF OBJECT_ID(N'dbo.sp_Property_Select_Item') IS NOT NULL
  DROP PROCEDURE dbo.sp_Property_Select_Item


GO




/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Property_Select_Item 
(
	@ID INT
)    
AS
BEGIN
  if @ID is null
    set @ID = 3917
  
  print @ID

  declare  @temp table
    (
       ID          int NOT NULL,              
       guid        uniqueidentifier,
       parent_guid uniqueidentifier,
       
       name        NVARCHAR(150) NOT NULL,               
       objname     VARCHAR(30),               
            

      ---------------
      --ANTENNA
      ---------------
      AntennaType_ID      int, 
      ANTENNATYPE_NAME    NVARCHAR(150),

      BAND        		  VARCHAR(10),

      DIAMETER            Float,
      HEIGHT              Float,
      AZIMUTH             Float, 
      POLARIZATION_str    VARCHAR(4),
      GAIN                Float,
      TILT                Float,
      vert_width          Float,
      horz_width          Float,
      ANTENNA_loss     Float,
      
      mast_index int,
      
      date_created datetime
                     
      ) ;      
         



--------------------------------------------------
-- LinkEnd
--------------------------------------------------     
  insert into @temp  
     (id,guid,name,objname, date_created,

      AntennaType_NAME 
     
      )  
   select  
      id,guid,name,'LinkEnd', date_created , 
      LinkEndType_name       
      
   from view_LinkEnd_base      
   where Property_id = @ID


      

  insert into @temp
    (id,guid,name,objname, date_created )
   select 
     id,guid,name,'pmp_terminal' , date_created       
   from view_pmp_terminal      
   where Property_id = @ID

 
--------------------------------------------------
-- pmp_site
--------------------------------------------------        

  insert into @temp
    (id,guid,name,objname, date_created )
   select 
     id,guid,name,'pmp_site', date_created        
   from pmp_site     
   where Property_id = @ID


  insert into @temp
    (id,guid,name,objname, parent_guid  )
   select 
     M.id, M.guid, M.name, 'pmp_sector' , p.guid       
   from view_pmp_sector M
       LEFT OUTER JOIN  pmp_site P ON p.id = m.pmp_site_id    
   where P.Property_id = @ID

/*

  SELECT * FROM @temp                                


return

*/



--------------------------------------------------
-- view_LinkEnd_antennas
--------------------------------------------------    
  insert into @temp
    (
     parent_guid,
    
     id,guid,name, 
     BAND,
     HEIGHT, 
     DIAMETER,AZIMUTH,GAIN,TILT,
     POLARIZATION_str,
     
     AntennaType_ID,AntennaType_NAME, 
     
     vert_width,horz_width,
     ANTENNA_loss,
     mast_index 
    )    
   select 
     m.LinkEnd_guid,
   
/*     case
       when m.LinkEnd_id >0 then L.guid
       when m.pmp_sector_id >0 then S.guid
       when m.pmp_terminal_id >0 then t.guid
       else null
     end,   
*/
   
     m.id, m.guid,m.name,
     m.BAND,
     m.HEIGHT,  
     m.DIAMETER,m.AZIMUTH,m.GAIN,m.TILT,
     m.POLARIZATION_str,
     
     m.AntennaType_ID, m.AntennaType_NAME,
     
     m.vert_width,
     m.horz_width,
     m.loss,
     m.mast_index     
     
   from view_LinkEnd_antennas M   
--       LEFT OUTER JOIN  LinkEnd L    ON L.id = m.LinkEnd_id    
--       LEFT OUTER JOIN  pmp_sector S ON S.id = m.pmp_sector_id    
 --      LEFT OUTER JOIN  pmpterminal T ON T.id = m.pmp_terminal_id    
    
   where m.Property_id = @ID 
/*


  SELECT * FROM @temp                                


return
*/




--------------------------------------------------
-- Link_Repeater
--------------------------------------------------

  insert into @temp
    (id,guid,name,objname )
   select 
     id,guid,name,'Link_Repeater'        
   from Link_Repeater 
   where Property_id = @ID
   

--------------------------------------------------
-- view_Link_Repeater
--------------------------------------------------    
  insert into @temp
    (
     parent_guid,
    
     id,guid,name, 
     BAND,
     HEIGHT, 
     DIAMETER,AZIMUTH,GAIN,TILT,
     POLARIZATION_str,
     AntennaType_ID,AntennaType_NAME, 
     vert_width,horz_width,
     ANTENNA_loss    
    )    
   select 
     L.guid     ,
   
     m.id, m.guid,m.name,
     m.BAND,
     m.HEIGHT,  
     m.DIAMETER,m.AZIMUTH,m.GAIN,m.TILT,
     m.POLARIZATION_str,
     m.AntennaType_ID,
     m.AntennaType_NAME,
     m.vert_width,
     m.horz_width,
     m.loss    
     
   from view_Link_Repeater_antenna M   
       LEFT OUTER JOIN  Link_Repeater L    ON L.id = m.Link_Repeater_id   
    
   where l.Property_id = @ID  --and LinkEnd_id>0



  SELECT * FROM @temp                                

 
END
go


---------------------------------------------------------


IF OBJECT_ID(N'dbo.sp_Linkend_Antenna_Select') IS NOT NULL
  DROP procedure dbo.sp_Linkend_Antenna_Select


GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Linkend_Antenna_Select
(
  @OBJNAME    VARCHAR(50),  
  @ID INT   
)
AS
BEGIN 

/* case ObjNameToType(FObjName) of
  //  otCell:        begin sTable:=view_CELL_ANTENNAS;         sField:=FLD_CELL_ID;      end;
    otLinkEnd:     begin sTable:=view_LINKEND_ANTENNAS;      sField:=FLD_LINKEND_ID;      end;
    otPMPTERMINAL: begin sTable:=view_PMP_TERMINAL_ANTENNAS; sField:=FLD_PMP_TERMINAL_ID; end;
    otPMPSector:   begin sTable:=view_PMP_Sector_ANTENNAS;   sField:=FLD_PMP_Sector_ID;   end;

   // otCell_3G:     begin sTable:=VIEW_CELL_ANTENNAS_3G;   sField:=FLD_CELL_3G_ID;   end;
  else
    raise Exception.Create('procedure TdmAntenna_view.AddAntennas-ObjNameToType(aObjName)');
  end;


  s := Format('SELECT * FROM %s WHERE %s=%d', [sTable, sField, FID]);
//  s := 'SELECT * FROM ' +sTable+ ' WHERE '+ sField + '='
*/

  if @OBJNAME = ''   RAISERROR('@OBJNAME = ''''', 16, 1)
 
/*
  ---------------------------------------------
  IF @OBJNAME='Repeater'
  ---------------------------------------------  
    SELECT * FROM view_LinkEnd_antennas WHERE Repeater_id=@ID     
  ELSE
*/
  ---------------------------------------------
  IF @OBJNAME in ('Linkend', 'pmp_sector', 'pmp_terminal')
  ---------------------------------------------  
    SELECT * 
 --     FROM LinkEnd_antenna 
      FROM view_LinkEnd_antennas 
      WHERE linkend_id=@ID     
  ELSE

/*  ---------------------------------------------
  IF @OBJNAME='pmp_sector'
  ---------------------------------------------  
    SELECT * FROM view_Linkend_ANTENNAS WHERE PMP_Sector_ID=@ID     
  ELSE


*/
  ---------------------------------------------
  IF @OBJNAME='template_site_linkend'
  ---------------------------------------------  
    SELECT * 
      FROM view_Template_Site_LinkEnd_antenna 
      WHERE Template_Site_LinkEnd_ID=@ID     
  ELSE




  ---------------------------------------------
--  IF @OBJNAME='pmp_terminal'
  ---------------------------------------------  
--    SELECT * FROM view_PMP_TERMINAL_ANTENNAS WHERE PMP_TERMINAL_ID=@ID     
--  ELSE
    return -1
    
    
  RETURN @@ROWCOUNT
  
END
go
-----------------------------------------------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */

IF OBJECT_ID(N'[dbo].[sp_Property_GetItemsList_]') IS NOT NULL
  DROP PROCEDURE [dbo].[sp_Property_GetItemsList_]


GO


CREATE PROCEDURE dbo.sp_Property_GetItemsList_
(
  @ID INT
)
AS
BEGIN
  DECLARE  @MyTable TABLE       
          (ID          int NOT NULL,            
           guid        uniqueidentifier,
           name        VARCHAR(100) NOT NULL,
           objname     VARCHAR(50) NOT NULL
           
          ) ;                                



-- create table [#MyTable]


   
INSERT INTO @MyTable  (id,guid,NAME,objname)          
  SELECT id,guid,NAME, 'MSC'       FROM MSC WHERE Property_id=@ID
    UNION
  SELECT id,guid,NAME, 'BSC'       FROM BSC WHERE Property_id=@ID
    UNION
  SELECT id,guid,NAME, 'LINKEND'   FROM LINKEND WHERE Property_id=@ID
--    UNION
--  SELECT id,guid,NAME, 'SITE'      FROM SITE WHERE Property_id=@ID
    UNION
  SELECT id,guid,NAME, 'PMP_SITE'  FROM PMP_SITE WHERE Property_id=@ID
    UNION
  SELECT id,guid,NAME, 'PMP_TERMINAL' FROM view_PMP_TERMINAL WHERE Property_id=@ID
    UNION
  SELECT id,guid,NAME, 'link_repeater' FROM link_repeater WHERE Property_id=@ID
--    UNION
--  SELECT id,guid,NAME, 'site'       FROM Site WHERE Property_id=@ID
  --  UNION
    
--  SELECT cell.id, cell.guid, cell.NAME, 'SiteCell' FROM cell
--    left outer join site on site.id=cell.site_id  WHERE site.Property_id=@ID


       
-- LinkLines --
/*
INSERT INTO @MyTable  (id,guid,NAME,objname)          
  SELECT l.id, l.guid, l.NAME, 'link' 
      FROM Link L
         left join LinkEnd  LinkEnd1 on 
      WHERE (LinkEnd1.Property_id=@ID) or (Property2_id=@ID)
*/      
      --- (linkend1_id IN (SELECT id FROM [#MyTable] WHERE objname='LINKEND' ) ) or
        --    (linkend2_id IN (SELECT id FROM [#MyTable] WHERE objname='LINKEND' ) ) 
                       
            
INSERT INTO @MyTable  (id,guid,NAME,objname)          
  -- antennas --
  SELECT id,guid,NAME, 'LINKEND_ANTenna' 
      FROM LinkEnd_antenna
      WHERE (linkend_id>0) and (Property_id=@ID)
      -- IN (SELECT id FROM [#MyTable] WHERE objname='LINKEND' )

/*
--PMP antennas--                
INSERT INTO @MyTable  (id,guid,NAME,objname)     
  SELECT id,guid,NAME, 'PMP_TERMINAL_ant' 
      FROM LinkEnd_antenna
      WHERE (LinkEnd_id >0) and (Property_id=@ID)
      

--PMP antennas--                
INSERT INTO @MyTable  (id,guid,NAME,objname)     
  SELECT id,guid,NAME, 'PMP_sector_ant' 
      FROM LinkEnd_antenna
      WHERE (LinkEnd_id >0) and (Property_id=@ID)
      */
      

/*
--PMP antennas--                
INSERT INTO @MyTable  (id,guid,NAME,objname)     
  SELECT id,guid,NAME, 'link_repeater_antenna' 
      FROM link_repeater_antenna
      WHERE  (Property_id=@ID)*/
            
      

--       IN (SELECT id FROM [#MyTable] WHERE objname='PMP_TERMINAL' )
            
        
-- PMP links--          

/*       
INSERT INTO [#MyTable]  (id,guid,NAME,objname)     
  SELECT id,guid,NAME, 'link' 
      FROM link
      WHERE pmp_terminal_id IN (SELECT id FROM [#MyTable] WHERE objname='PMP_TERMINAL' )
*/
            
      

/*            
-- Cell --  
INSERT INTO [#MyTable]  (id,guid,NAME,objname)          
  SELECT id,guid,NAME, 'CELL' 
      FROM Cell
      WHERE site_id IN (SELECT id FROM [#MyTable] WHERE objname='SITE' )
      
-- Cell antennas --                
INSERT INTO [#MyTable]  (id,guid,NAME,objname)     
  SELECT id,guid,NAME, 'CELL_ANT' 
      FROM view_Cell_antennas
      WHERE cell_id IN (SELECT id FROM [#MyTable] WHERE objname='cell' )
*/

      
                
   --   linkend1_id = @aID) OR
  
  
   SELECT DISTINCT * FROM @MyTable;       
   
 END

go
----------------------------------------------------------------------------------------
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID(N'[dbo].[sp_Property_Del_]') IS NOT NULL
  DROP PROCEDURE [dbo].[sp_Property_Del_]


GO



/* ---------------------------------------------------------------
created: Alex  
used in:   
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Property_Del_ 
(
  @ID int = 7997
)
AS

  
BEGIN  
 DECLARE
    @id_ int
  
   /*

  SELECT link.id 
      INTO #link
    FROM link 
      left join linkend L1 on L1.id = link.linkend1_id
      left join linkend L2 on L2.id = link.linkend2_id
      
    WHERE
      (L1.property_id = @Property_id) or
      (L2.property_id = @Property_id) 
*/

  delete from Link 
    where 
      linkend1_id in (select id from [linkend] where property_id=@ID) 
      or
      linkend2_id in (select id from [linkend] where property_id=@ID) 


 
--  delete from Link where (property1_ID=@ID) or (property2_ID=@ID)


  delete from Link_Repeater where property_id=@ID


  delete from Linkend_Antenna where property_id=@ID
  delete from Linkend where property_id=@ID

  delete from BSC      where property_id=@ID
  delete from MSC      where property_id=@ID

--  delete from PMPTERMINAL where property_id=@ID
  delete from PMP_SITE where property_id=@ID


--  delete from SITE where property_id=@ID




  delete from property where id=@ID
  


  IF Exists (select * from property where id=@ID)
     RAISERROR ('record Exists', 16, 1);
  
  
  
  RETURN @@ROWCOUNT
    
  
  
END
go