
if  COL_LENGTH('dbo.Link','SDB_bitrate_priority')  is null
  ALTER TABLE dbo.Link  ADD SDB_bitrate_priority float 
go

if  COL_LENGTH('dbo.Link','SDB_bitrate_no_priority')  is null
  ALTER TABLE dbo.Link  ADD SDB_bitrate_no_priority float 
go



------------------------------------------------------------
IF OBJECT_ID(N'[link].[view_Link_SDB]') IS NOT NULL
  DROP view [link].[view_Link_SDB]


GO

GO

CREATE VIEW link.view_Link_SDB

AS
SELECT     dbo.Link.*, 


  LinkEnd.channel_width_MHz,
  LinkEnd.band,
  LinkEnd.bitrate_Mbps,
  
  
  dbo.fn_PickList_GetValueByCode('link', 'status',  dbo.Link.status) AS status_str 
--lib.link_status

        
        
FROM         dbo.LinkEnd  RIGHT OUTER JOIN
              dbo.Link ON LinkEnd.id = dbo.Link.linkend1_id
              
              
go
--where Link.id = 1684


------------------------------------------------------------
IF OBJECT_ID(N'[link].[sp_Link_SDB_items_SEL]') IS NOT NULL
  DROP procedure [link].[sp_Link_SDB_items_SEL]


GO


--------------------------------------------------------------- */
CREATE procedure link.[sp_Link_SDB_items_SEL]
--------------------------------------------------------------- */

(
  @ID  INT

)
AS
begin
if @id is null
  select top 1 @id=id from link_sdb



 declare @tbl TABLE
   (
     name_sdb varchar(100),

     name varchar(100),
     
     SESR float,
     kng  float,

     SESR_required float,
     kng_required  float,
     
     bandwidth float,
     length_m float,
     
     band varchar(50),
     
     SDB_bitrate_priority float,
     SDB_bitrate_no_priority  float,
    
     status int,
     status_str varchar(50)

 
    
   )
  --, sesr_required,kng_required
 INSERT INTO @tbl (name_sdb, name, sesr,kng,  sesr_required,kng_required,  length_m, band, SDB_bitrate_priority,  SDB_bitrate_no_priority, status_str, bandwidth)

 
   select 'Common-Band', name, sesr,kng, sesr_norm,kng_norm,  length, band, SDB_bitrate_priority,  SDB_bitrate_no_priority, status_str, channel_width_MHz
     from link.view_Link_SDB  where id in (select link1_id from link_sdb where id=@ID)
     
    UNION

   select 'E-Band', name, sesr,kng,  sesr_norm,kng_norm,  length, band, SDB_bitrate_priority,  SDB_bitrate_no_priority, status_str, channel_width_MHz
     from link.view_Link_SDB  where id in (select link2_id from link_sdb where id=@ID)
 
/*

 select
        
        POWER( 10, SUM( LOG10(bandwidth ))) 
        
        
     from @tbl
     */
     
declare 
  @sesr1 float,     
  @sesr2 float, 
  @kng1 float,     
  @kng2 float 
     
  
  select @sesr1=sesr, @kng1=kng from @tbl where name_sdb = 'common-band' 
  select @sesr2=sesr, @kng2=kng from @tbl where name_sdb = 'e-band' 
  
--  select @sesr1
  

------------------------------------------------
 INSERT INTO @tbl (name_sdb, 
 		sesr_required, kng_required, 
        
 		sesr, kng, 
        
        SDB_bitrate_priority,  SDB_bitrate_no_priority
    --    status_str
         )
 
   select 'SDB', 
   		Min(sesr_required),
        Min(kng_required),
        
        
        0.01 * @sesr1 * @sesr2, --  POWER( 10.0, SUM( LOG10( sesr))) ,
        0.01 * @kng1  * @kng2, --  POWER( 10.0, SUM( LOG10( sesr))) ,
       
        
        min(SDB_bitrate_priority),  
        min(SDB_bitrate_no_priority)
        
      --  dbo.fn_PickList_GetValueByCode('link', 'status',  dbo.Link.status) AS status_str 
        
        
     from @tbl

----------------------------------------------------------------

update @tbl
  set status = iif (sesr_required > sesr and kng_required > kng,  1, 0)    
where 
  name_sdb = 'SDB' 

update @tbl
  set status_str = dbo.fn_PickList_GetValueByCode('link', 'status',  status)     
where 
  name_sdb = 'SDB' 


---------------------------------------------------------------

--  select * from @tbl where name_sdb = 'SDB' 


update R
  set 
    r.sesr =  m.sesr,
    r.kng  =  m.kng,

    r.sesr_required =  m.sesr_required,
    r.kng_required  =  m.kng_required,

    r.status  =  m.status
 

from link_sdb R, 
  (select * from @tbl where name_sdb = 'SDB' ) M

where id = @ID 


--  select top 1 @id=id from link.link_sdb

---------------------------------------------------

update @tbl
  set name_sdb = 'SDB (Super Dual Band)'
where 
  name_sdb = 'SDB' 
  
---------------------------------------------------

 
 select * from @tbl


/*

 -----------------------------------------------
  if @ObjName = 'Link_SDB'
  -----------------------------------------------
  BEGIN
   INSERT INTO #temp (id,name,guid,ObjName) 
      SELECT id, 'common band: '+ name, guid, 'Link' --, @parent_guid, P.GeoRegion_ID 
      -- + name
        FROM  link 
        where id in (select link1_id from link.link_sdb where id=@ID)
 
   INSERT INTO #temp (id,name,guid,ObjName) 
      SELECT id, 'E band: '+ name, guid, 'Link' --, @parent_guid, P.GeoRegion_ID 
      -- +name
        FROM  link 
        where id in (select link2_id from link.link_sdb where id=@ID)
 
  

delete from link.Link_SDB
  where id=@ID*/


RETURN @@IDENTITY;

end
go


------------------------------------------------------------
IF OBJECT_ID(N'[link].[sp_Link_SDB_Del]') IS NOT NULL
  DROP procedure [link].[sp_Link_SDB_Del]


GO



--------------------------------------------------------------- */
CREATE procedure link.sp_Link_SDB_Del
--------------------------------------------------------------- */

(
  @ID  INT

)
AS
begin

delete from Link_SDB where id=@ID


RETURN @@IDENTITY;

end

go
-------------------------------------------------------------
IF OBJECT_ID(N'[link].[sp_Link_SDB_Add]') IS NOT NULL
  DROP procedure [link].[sp_Link_SDB_Add]


GO



--------------------------------------------------------------- */
CREATE procedure link.sp_Link_SDB_Add
--------------------------------------------------------------- */

(
  @PROJECT_ID  INT,
  @NAME 		 varchar(100),
  
  @link1_id int,
  @link2_id int

)
AS
begin


  INSERT INTO Link_SDB
  (
    PROJECT_ID,
    NAME,
     
    link1_id,
    link2_id 
  )
  VALUES
  (
    @project_id,
    @NAME,

    @link1_id,
    @link2_id 

  )

RETURN IDENT_CURRENT ('Link_sdb')


end

go
----------------------------------------------------------

IF OBJECT_ID(N'[dbo].[sp_Link_Update]') IS NOT NULL
  DROP PROCEDURE [dbo].[sp_Link_Update]


GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* ---------------------------------------------------------------
created: Alex  
used in:  
--------------------------------------------------------------- */
CREATE PROCEDURE dbo.sp_Link_Update
(
  @ID   int,  
  
--  @MODE VARCHAR(50) =null,  --'Calc_Results'
  ------------------------- 


  @Rx_Level_dBm float =null,  
    
  @SESR_NORM float =null,  
  @SESR float =null,  
    
  @KNG_NORM float =null,  
  @KNG float =null,  
 -- @KNG_YEAR float,  
    
  @FADE_MARGIN_DB float =null, 
 
  --@FADE_RESERVE float,  
    
  @ESR_REQUIRED float =null,  
  @BBER_REQUIRED float =null,  
  @ESR_NORM float =null,  
  @BBER_NORM float =null,  
    
  @MARGIN_HEIGHT float =null,  
  @MARGIN_DISTANCE float =null,  
  
  @SESR_SUBREFRACTION float =null,  
  @SESR_RAIN float =null,  
  @SESR_INTERFERENCE float =null,   
  
  @KNG_SUBREFRACTION float =null,  
  @KNG_RAIN float =null,  
  @KNG_INTERFERENCE float =null,   
  
  
    
  @LOS_STATUS int =null,  
  @STATUS int =null,
  @STATUS_back int =null,
  
  @NIIR1998_water_area_percent float =null, 
  
  @CALC_RESULTS_XML TEXT = null,
  
  
  
  @SDB_bitrate_priority float = null,
  @SDB_bitrate_no_priority float = null
  
  -----------------------------------------------------------------
   -- @MODE='Profile_XML'
  -----------------------------------------------------------------
 -- @Profile_XML TEXT = null
    
)

AS
BEGIN

PRINT 0

if not exists (select * from link where id = @ID)
  return -9999
  
--  RAISERROR('id not exists ' )


DECLARE @result int;

 DECLARE
   @linkend1_id int,
   @linkend2_id int ;


 
  UPDATE Link 
  SET 
    Rx_Level_dBm	= COALESCE(@Rx_Level_dBm, Rx_Level_dBm),

    SESR_NORM		=COALESCE(@SESR_NORM, SESR_NORM),
    SESR			=COALESCE(@SESR,      SESR),

    KNG_NORM		=COALESCE(@KNG_NORM, KNG_NORM),
    KNG				=COALESCE(@KNG,      KNG),
   
    FADE_MARGIN_DB	=COALESCE(@FADE_MARGIN_DB, FADE_MARGIN_DB),

    ESR_REQUIRED	=COALESCE(@ESR_REQUIRED,   ESR_REQUIRED),
    BBER_REQUIRED	=COALESCE(@BBER_REQUIRED,  BBER_REQUIRED),
    ESR_NORM		=COALESCE(@ESR_NORM,       ESR_NORM),
    BBER_NORM		=COALESCE(@BBER_NORM,      BBER_NORM),

    MARGIN_HEIGHT	  =COALESCE(@MARGIN_HEIGHT,      MARGIN_HEIGHT),
    MARGIN_DISTANCE	  =COALESCE(@MARGIN_DISTANCE,    MARGIN_DISTANCE),
    
    SESR_SUBREFRACTION=COALESCE(@SESR_SUBREFRACTION, SESR_SUBREFRACTION),
    SESR_RAIN		  =COALESCE(@SESR_RAIN,          SESR_RAIN),
    SESR_INTERFERENCE =COALESCE(@SESR_INTERFERENCE,  SESR_INTERFERENCE),   
  
  
    KNG_SUBREFRACTION	=COALESCE(@KNG_SUBREFRACTION,  KNG_SUBREFRACTION),	
    KNG_RAIN		     	=COALESCE(@KNG_RAIN,           KNG_RAIN	),	     	
    KNG_INTERFERENCE	=COALESCE(@KNG_INTERFERENCE,   KNG_INTERFERENCE	),
  
  
    LOS_STATUS		=COALESCE(@LOS_STATUS,  LOS_STATUS),
    STATUS			=COALESCE(@STATUS,      STATUS),
    STATUS_back		=COALESCE(@STATUS_back, STATUS_back),
     
    NIIR1998_water_area_percent = COALESCE(@NIIR1998_water_area_percent, NIIR1998_water_area_percent),
    
    CALC_RESULTS_XML 	=COALESCE(@CALC_RESULTS_XML, CALC_RESULTS_XML),
     
    SDB_bitrate_priority    = COALESCE(@SDB_bitrate_priority, SDB_bitrate_priority),
    SDB_bitrate_no_priority = COALESCE(@SDB_bitrate_no_priority, SDB_bitrate_no_priority)
    
    

            
  WHERE 
    id = @ID


--return

  
  SET @result= @@ROWCOUNT


  IF @Rx_Level_dBm IS NOT NULL
  BEGIN

    UPDATE Linkend  
      SET 
          Rx_Level_dBm = @Rx_Level_dBm  
      WHERE 
          id in (select linkend1_id from link where id=@ID)
          or
          id in (select linkend2_id from link where id=@ID)
      
  END
    



--  RETURN @result


  RETURN @result


END

go
