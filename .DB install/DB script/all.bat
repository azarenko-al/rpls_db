set path=C:\Windows\System32\;f:\SQL\selected

set blank=f:\_blank.txt
set dest=f:\_all.sql
cd SQL\selected 

rem --------------------------------------------------
rem  schema
rem --------------------------------------------------

copy /b  ^
  "f:\schema.sql" +^
  "F:\db_update.sql" +^
  %blank% ^
  %dest%



rem --------------------------------------------------
rem link_climate
rem --------------------------------------------------
copy /b ^
  %dest% +^
  link_climate\link_climate.sp_Link_climate_UPD.StoredProcedure.sql +^
  %blank% ^
  %dest%




rem --------------------------------------------------
rem LinkFreqPlan
rem --------------------------------------------------
copy /b  ^
  %dest% +^
  LinkFreqPlan\LinkFreqPlan.view_LinkFreqPlan_LinkEnd.View.sql +^
  LinkFreqPlan\LinkFreqPlan.view_LinkFreqPlan_LinkEnd_antennas.View.sql +^
  %blank% ^
  %dest%

rem --------------------------------------------------
rem geo
rem --------------------------------------------------
copy /b  ^
  %dest% +^
  geo\geo.fn_Length_m.UserDefinedFunction.sql +^
  geo\geo.fn_Length_m_Between_Property.UserDefinedFunction.sql +^
  %blank% ^
  %dest%



rem --------------------------------------------------
rem lib
rem --------------------------------------------------
copy  /b ^
  %dest% +^
  lib\lib.link_status.Table.sql   +^
  lib\lib.Template_Link.Table.sql +^
  lib\lib.PickList_Values.Table.sql +^
  lib\lib.PickList.Table.sql        +^
  lib\lib.ft_AntennaType_mask_SEL.UserDefinedFunction.sql +^
  lib\lib.ft_AntennaType_restore_from_mask.StoredProcedure.sql +^
  lib\lib.sp_Template_Link_Add.StoredProcedure.sql  +^
  lib\lib.sp_Template_Link_apply_to_link.StoredProcedure.sql +^
  lib\lib.ft_PickList.UserDefinedFunction.sql +^
  %blank% ^
  %dest%



rem --------------------------------------------------
rem link_calc
rem --------------------------------------------------
copy  /b ^
  %dest% +^
  link_calc\link_calc.view_Link_for_calc.View.sql +^
  link_calc\link_calc.sp_Link_select_for_calc.StoredProcedure.sql   +^
  link_calc\link_calc.sp_Link_calc_results_SEL.StoredProcedure.sql  +^
  link_calc\link_calc.sp_Link_calc_results_add_xml_SEL.StoredProcedure.sql  +^
  link_calc\link_calc.sp_Link_Calc_Reserve.StoredProcedure.sql  +^
  link_calc\link_calc.ft_Linkend_dop_antenna.UserDefinedFunction.sql +^
  %blank% ^
  %dest%



rem --------------------------------------------------
rem link
rem --------------------------------------------------
copy /b ^
  %dest% +^
  link\link.fn_LinkEnd_name.UserDefinedFunction.sql                +^
  link\link.fn_LinkEnd_Antenna_Get_Link_ID.UserDefinedFunction.sql +^
  link\link.ft_Link_calc_results_basic.UserDefinedFunction.sql +^
  link\link.ft_Link_Calc_results.UserDefinedFunction.sql      +^
  link\link.view_Link_summary.View.sql   +^
  link\link.sp_Link_Summary.StoredProcedure.sql +^
  dbo\dbo.sp_Link_Add.StoredProcedure.sql    +^
  dbo\dbo.view_Link_base.View.sql      +^
  %blank% ^
  %dest%

rem exit



rem --------------------------------------------------
rem dbo report
rem --------------------------------------------------
copy /b ^
  %dest% +^
  dbo\dbo.view_Link_report.View.sql +^
  dbo\dbo.sp_Explorer_Select_Object.StoredProcedure.sql +^
  %blank% ^
  %dest%


rem --------------------------------------------------
rem UI
rem --------------------------------------------------
copy /b  ^
  %dest% +^
  ui\ui.explorer.Table.sql +^
  ui\ui.Filters.Table.sql  +^
  ui\ui.sp_Custom_Column_DEL.StoredProcedure.sql  +^
  ui\ui.sp_Custom_Column_INS.StoredProcedure.sql  +^
  ui\ui.sp_Filter_INS_UPD.StoredProcedure.sql  +^
  ui\ui.sp_Filter_SEL.StoredProcedure.sql      +^
  ui\ui.sp_Object_prepare_view.StoredProcedure.sql  +^
  ui\ui.sp_Object_prepare_view_ALL.StoredProcedure.sql  +^
  %blank% ^
  %dest%


rem --------------------------------------------------
rem history
rem --------------------------------------------------
copy /b  ^
  %dest% +^
  history\history.history_log.Table.sql+^
  history\history.tables_for_history.Table.sql +^
  "F:\history.sql" +^
  history\history.view_FK.View.sql+^
  history\history.view_columns.View.sql +^
  history\history.view_COLUMNS_group.View.sql+^
  history\history.sp_Clear_Log.StoredProcedure.sql  +^
  history\history.sp_Create_triggers.StoredProcedure.sql +^
  history\history.sp_Create_triggers_all.StoredProcedure.sql+^
  history\history.sp_Drop_triggers_all.StoredProcedure.sql +^
  history\history.ft_Record_change_diff.UserDefinedFunction.sql+^
  history\history.sp_Record_History_SEL.StoredProcedure.sql +^
  %blank% ^
  %dest%


rem --------------------------------------------------
rem map
rem --------------------------------------------------
copy /b  ^
  %dest% +^
  dbo\dbo.view__object_fields_enabled.View.sql     +^
  dbo\dbo.TVarChar_table.UserDefinedTableType.sql  +^
  dbo\dbo.ft_VARCHAR_String_to_Table.UserDefinedFunction.sql +^
  map\map.user_map_settings.Table.sql              +^
  map\map.sp_Map_engine_layer_attributes_SEL.StoredProcedure.sql  +^
  map\map.sp_Map_engine_layer_attributes_UPD.StoredProcedure.sql  +^
  map\map.sp_MapMaker.StoredProcedure.sql       +^
  map\map.sp_MapMaker_Data.StoredProcedure.sql  +^
  map\map.sp_options_object_list_names.StoredProcedure.sql   +^
  map\map.sp_options_object_list_values.StoredProcedure.sql  +^
  map\map.sp_User_Map_Settings_UPD.StoredProcedure.sql       +^
  %blank% ^
  %dest%



rem --------------------------------------------------
rem dbo
rem --------------------------------------------------
copy /b  ^
  %dest% +^
  dbo\dbo.fn_Linkend_Antenna_info.UserDefinedFunction.sql  +^
  dbo\dbo.fn_Linkend_Antenna_pos.UserDefinedFunction.sql   +^
  dbo\dbo.sp_Link_Add.StoredProcedure.sql     +^
  dbo\dbo.sp_LinkEnd_Add.StoredProcedure.sql     +^
  dbo\dbo.sp_Linkend_Antenna_Add.StoredProcedure.sql  +^
  dbo\dbo.sp_PMP_Terminal_Plug.StoredProcedure.sql    +^
  dbo\dbo.sp_PMP_Terminal_Select_PMP_Sectors_for_Plug.StoredProcedure.sql +^
  dbo\dbo.sp_Template_Linkend_Copy_to_PMP_SITE.StoredProcedure.sql        +^
  dbo\dbo.view_Link.View.sql                     +^
  dbo\dbo.view_Link_report.View.sql              +^
  dbo\dbo.sp_Map_XREF_Update.StoredProcedure.sql +^
  dbo\dbo.sp_MapDesktop_ObjectMap_set_default.StoredProcedure.sql +^
  dbo\dbo.sp_MapDesktop_Select_ObjectMaps.StoredProcedure.sql     +^
  %blank% ^
  %dest%

rem --------------------------------------------------
rem reports
rem --------------------------------------------------
copy /b  ^
  %dest% +^
  reports\reports.view_LinkEnd_report.View.sql        +^
  reports\reports.View_Link_freq_order.View.sql       +^
  %blank% ^
  %dest%





rem --------------------------------------------------
rem UI
rem --------------------------------------------------
copy /b  ^
  %dest% +^
  "F:\lib.sql" +^
  "F:\objects.sql" +^
  "F:\exec.sql" +^
  %blank% ^
  %dest%






