IF OBJECT_ID(N'[dbo].[sp_User_access_SEL]') IS NOT NULL
  DROP procedure [dbo].[sp_User_access_SEL]
GO


/*
����� ������ �� ��������� user
*/
CREATE PROCEDURE Security.[sp_User_access_SEL]
(
  @USER_NAME varchar(50)=null
)

AS
BEGIN

--select USER_NAME()
if @USER_NAME is null
  set @USER_NAME=USER_NAME()


if @USER_NAME='dbo'
  select 
    @USER_NAME as user_NAME,  

  	CAST(1 as int) as is_modify_Project,
  	CAST(1 as int) as is_modify_lib

else

  select
    @USER_NAME as user_NAME, 
  	is_modify_Project, 
    is_modify_lib
        
  	from  Security.view_Users   
    where 
    	sysname = @USER_NAME
    
    
  --     id not in (select id from Security.Users) 



--select * from Security.Users


  --------------------------------------------------------
--  if (@ACTION = 'users') or  (@ACTION = 'select_users')
  --------------------------------------------------------
--  BEGIN
   --  INSERT INTO [Users] (name, UNAME, LOGIN)
--        SELECT name, name, suser_sname(sid) 
/*  SELECT uid,name, * --name, suser_sname(sid) as login
    FROM sysusers 
    where hasdbaccess=1 and NAME<>'dbo'  
*/    
--          WHERE (status=2) 
          --and 
            --    (name not IN (SELECT name FROM [Users])) 
      
--    from sysusers 
--     where hasdbaccess=1 and NAME<>'dbo'  



   --  UPDATE [users]
    --   set IsAdmin=1
     --  WHERE uname='dbo' and IsAdmin is null
      
   --  SELECT * FROM [users]
     
 -- END ELSE  		



  /* Procedure body */
END


go

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID(N'[dbo].[sp_Project_summary]') IS NOT NULL
  DROP procedure [dbo].[sp_Project_summary]
GO


CREATE PROCEDURE dbo.sp_Project_summary
    @USER_NAME sysname = null
AS
BEGIN
  if @USER_NAME is null  
    set @USER_NAME = USER_NAME()

 -- IF 
 
/* create table #ReadOnly_Projects (id int)
 
  if OBJECT_ID('view_User_Projects_RFP') is not null  
    INSERT INTO #ReadOnly_Projects (id)
       select Project_id  from view_User_Projects_RFP  WHERE (USER_NAME=USER_NAME())
 
*/

--  SELECT *

/*     CASE 
       WHEN exists(SELECT * from #ReadOnly_Projects T WHERE T.id=M.id) THEN CAST(1 as bit)
     END  as ReadOnly
*/   
 --   FROM view_Project_summary_ M
 
 
  -- SELECT * FROM view_Project_summary_
 
 
 -- drop #temp
 
 
 
 SELECT  
   CAST(0 as bit) as checked,


                    (SELECT     COUNT(*)     FROM          property
                     WHERE      project_id = m.id) AS property_count,
                       
                    (SELECT     COUNT(*)   FROM          view_PMP_site
                      WHERE      project_id = m.id) AS pmp_site_count,
                            
                      --view_PMP_site_with_project_id
                            
                    (SELECT     COUNT(*)     FROM          view_PMP_TERMINAL --_with_project_id
                      WHERE      project_id = m.id) AS pmp_terminal_count,
                            
                    (SELECT     COUNT(*)        FROM          pmp_calcregion
                      WHERE      project_id = m.id) AS pmp_calcregion_count,
                            
                    (SELECT     COUNT(*)       FROM          link
                      WHERE      project_id = m.id) AS link_count,
 

                    (SELECT     COUNT(*)    FROM          view_Link_Repeater_with_project_id
                      WHERE      (project_id = m.id)   ) AS link_Repeater_count,


                           
                    (SELECT     COUNT(*)     FROM          linkline
                      WHERE      project_id = m.id) AS linkline_count,
                      
                    (SELECT     COUNT(*)      FROM          linknet
                      WHERE      project_id = m.id) AS linknet_count,
                       
 
                    (SELECT     COUNT(*)    FROM          view_LinkFreqPlan
                      WHERE project_id = m.id )  AS linkfreqplan_count,
                            

                    (SELECT     COUNT(*)    FROM          view_LinkEnd_with_project_id
                      WHERE      project_id = m.id) AS linkend_count,
                            
                    (SELECT     COUNT(*)   FROM          view_Linkend_Antenna_with_project_id
                      WHERE      project_id = m.id) AS linkend_antenna_count,

/*

                     (SELECT     COUNT(*)        FROM          Site
                        WHERE      project_id = m.id) AS Site_count,


                     (SELECT     COUNT(*)        FROM          Cell
                        WHERE      project_id = m.id) AS Cell_count,
*/

                     (SELECT     COUNT(*)        FROM          link_SDB
                        WHERE      project_id = m.id) AS link_SDB_count,
 
                                                        
                      (SELECT     MAX(datetime)    FROM          activity_log
                        WHERE      (project_id = m.id)) AS last_datetime, 
                            
                            
                        M.*
--                            m.user_created
                            
                            
FROM         dbo.Project M


where 
    @USER_NAME = 'dbo' 
  or user_created =  @USER_NAME 
  or id in (select project_id from Security.ft_Projects_allowed (@USER_NAME))
 


END

go
