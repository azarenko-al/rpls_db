object Form1: TForm1
  Left = 420
  Top = 107
  Width = 666
  Height = 530
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object dbgrd1: TDBGrid
    Left = 12
    Top = 133
    Width = 449
    Height = 333
    DataSource = DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object Button1: TButton
    Left = 496
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 1
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=Onega'
    LoginPrompt = False
    Left = 48
    Top = 8
  end
  object qry_Properties: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select id, name, lat, lon FROM property WHERE project_id=393')
    Left = 124
    Top = 4
  end
  object ds_qry_Properties: TDataSource
    DataSet = qry_Properties
    Left = 124
    Top = 52
  end
  object qry_Links: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select id, name, lat1, lon1,  lat2, lon2'
      '  FROM view_link   WHERE project_id=393')
    Left = 228
    Top = 4
  end
  object DataSource1: TDataSource
    DataSet = qry_Links
    Left = 228
    Top = 52
  end
end
