unit dm_Security;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms, Dialogs,variants,

  dm_Main,

  u_DB

  //, dm_Object_base
{
  u_func,
  u_func_DB,
  u_func_dlg,
  u_const_db,
  u_const_str,
  u_geo,

  u_types,
  u_classes,

  dm_UserRegion,

  dm_Main,
  dm_Object_base
}

  ;


type
{  TdmUserAddRec = record
    Name, Login, Password, Phone, Mail, Address, Description: string;

    GroupID,
    FolderID: integer;
  end;

  TUserObjects = record
    ObjID: Integer;
    ObjName: string;
    Enabled: boolean;
  end;}

//  TUserObjectsArr = array of TUserObjects;


  TdmSecurity = class(TDataModule)
    qry_Users: TADOQuery;
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    FUserObjectsArr: TUserObjectsArr;

  public
    EditRights : record
      Can_edit_Library: boolean;
      Can_Edit_Link_objects: boolean;
      Can_Edit_3G_objects: boolean;
    end;

//    function  Add (aRec: TdmUserAddRec): integer;
 //   function  Del (aID: Integer): boolean;
    function  ObjectCanBeModified (aObjectName: string; aID: Integer): boolean;
    function  ObjectsCanBeModified (aObjectName: string; aIDArray: TIntArray): boolean;

  //  function  DlgChangePassword(aID: Integer): boolean;

//    function  GetPassword (aID: Integer): string;
 //   function  ChangePassword(aID: Integer; sOldPwd, sNewPwd: string): boolean;
    procedure ClearCashedObjects();
    function Login(aName: string): boolean;
//    procedure GetUserList(var aIDList: TIDList);

//    function  GetGroupID(aUserID: Integer): Integer;

//    function  DelFromGroup(aUserID: Integer): boolean;
  //
  ///  function  MoveToAnotherGroup(aUserID, aGroupID: Integer): boolean;
  end;

{const
  CAPTION_ACT_MOVE_TO_GROUP = '����������� ������������ � ������ ������';
  CAPTION_ACT_ADD_USER_TO_GROUP = '�������� ������������ � ������';
  CAPTION_ACT_DEL_USER_FROM_GROUP = '������� ������������ �� ������';
  CAPTION_ACT_CHANGE_PASSWORD = '�������� ������';
}

function dmSecurity: TdmSecurity;

//==================================================================
implementation {$R *.dfm}
//==================================================================
//var
 // FdmSecurity: TdmSecurity;

function dmSecurity: TdmSecurity;
const
  FdmSecurity: TdmSecurity;
begin
  if not Assigned(FdmSecurity) then
    FdmSecurity:=TdmSecurity.Create (Application);

  Result:= FdmSecurity;
end;

//---------------------------------------------------------
procedure TdmSecurity.DataModuleCreate(Sender: TObject);
//---------------------------------------------------------
begin
  inherited;

  db_SetComponentADOConnection(dm );

//  TableName:=TBL_USER;
 // DisplayName:=STR_USER;

{  SetLength(FUserObjectsArr, 0);

  sp_AddUser.ProcedureName:=  ('AddUser');
  sp_DelUser.ProcedureName:=  ('DelUser');
}

(*  sp_addlogin      .ProcedureName:=  ('addlogin');
  sp_grantdbaccess .ProcedureName:=  ('grantdbaccess');
  sp_addrolemember .ProcedureName:=  ('addrolemember');
  sp_dropuser      .ProcedureName:=  ('dropuser');
  sp_droplogin     .ProcedureName:=  ('droplogin');*)
end;
{
//---------------------------------------------------------
function TdmSecurity.GetGroupID(aUserID: Integer): Integer;
//---------------------------------------------------------
begin
  Result:= GetIntFieldValue(aUserID, FLD_GROUP_ID);
end;}

{
//---------------------------------------------------------
function TdmSecurity.DelFromGroup(aUserID: Integer): boolean;
//---------------------------------------------------------
begin
  Update(aUserID, [db_Par(FLD_GROUP_ID, NULL)]);
  Result:= true;
end;

//---------------------------------------------------------
function TdmSecurity.MoveToAnotherGroup(aUserID, aGroupID: Integer): boolean;
//---------------------------------------------------------
begin
  Update(aUserID, [db_Par(FLD_GROUP_ID, aGroupID)]);
  Result:= true;
///  gl_DB.UpdateRecord(TBL_USER, iUserID, [db_Par(FLD_GROUP_ID, iGroupID)]);
end;


//---------------------------------------------------------
function TdmSecurity.DlgChangePassword(aID: Integer): boolean;
//---------------------------------------------------------
var
  sNewPwd: string;
begin
  sNewPwd:= InputBox('����� ������', '������� ����� ������', '');

  if sNewPwd<>'' then
    Result:= ChangePassword(aID, '', sNewPwd);  ////sOldPwd

  if not Result then
    MsgDlg('������ ��� ���������� ������');
end;

//---------------------------------------------------------
function TdmSecurity.ChangePassword (aID: Integer; sOldPwd, sNewPwd: string): boolean;
//---------------------------------------------------------
const
  SQL_COMMAND_TEXT = 'EXEC sp_password NULL, ''%s'', ''@s''';
var
  sUserLogin: string;
begin
  if sOldPwd = sNewPwd then begin
    Result:= true;
    exit;
  end;

  sUserLogin:= GetStringFieldValue(aID, FLD_LOGIN);

  Result:= gl_Db.ExecCommandSimple( Format('EXEC sp_password NULL, ''%s'', ''%s''', [sNewPwd, sUserLogin]) );

  if Result then
    gl_DB.UpdateRecord(TBL_USERS, aID, [db_Par(FLD_PASSWORD, sNewPwd)]);
end;

//---------------------------------------------------------
function TdmSecurity.GetPassword (aID: Integer): string;
//---------------------------------------------------------
begin
  Result:= gl_DB.GetStringFieldValueByID(TBL_USERS, FLD_PASSWORD, aID);
end;
}

//---------------------------------------------------------
procedure TdmSecurity.ClearCashedObjects();
//---------------------------------------------------------
begin
  SetLength(FUserObjectsArr, 0);
end;
{
//---------------------------------------------------------
function TdmSecurity.Add (aRec: TdmUserAddRec): integer;
//---------------------------------------------------------
var
  sMsg: string;
begin
  Result:= 0;
  sMsg:= '';

  try
    sp_AddUser.Close;

    with sp_AddUser.Parameters, aRec do
    begin
      Refresh;
      ParamByName('@Group_ID')    .Value := aRec.GroupID;
      ParamByName('@Login')       .Value := aRec.Login;
      ParamByName('@Password')    .Value := aRec.Password;
      ParamByName('@Name')        .Value := aRec.Name;
      ParamByName('@Phone')       .Value := aRec.Phone;
      ParamByName('@Mail')        .Value := aRec.Mail;
      ParamByName('@Address')     .Value := aRec.Address;
      ParamByName('@Description') .Value := aRec.Description;
      ParamByName('@RESULT')      .Value := 0;
    end;

    sp_AddUser.ExecProc;
  except
    on e: Exception do
      sMsg:= Format('������������ �� ��������. ������: %s. ', [e.Message] );
  end;

  try
    Result:= sp_AddUser.Parameters.ParamByName('@RESULT').Value;
  except   end;

  if Result<0 then
    sMsg:= sMsg+ Format('��� ������: %d', [Result]);

  if sMsg<>'' then
    ErrDlg(sMsg);
end;}

function TdmSecurity.Login(aName: string): boolean;
begin
  // TODO -cMM: TdmSecurity.Login default body inserted
end;

{
//---------------------------------------------------------
function  TdmSecurity.Del (aID: Integer): boolean;
//---------------------------------------------------------
var
  sLogin: string;
begin
  Result:= true;

  sLogin:= gl_DB.GetStringFieldValueByID(TBL_USERS, FLD_LOGIN, aID);

  if aID = dmMain.SysUserID then
  begin
    MsgDlg('���������� ������� ����� ������������');
    Result:= false;
    exit
  end;

  try
    sp_DelUser.Parameters.Refresh;
    sp_DelUser.Parameters.ParamByName('@Login').Value       := sLogin;

    sp_DelUser.ExecProc;
  except
    Result:= false;
  end;
end;}

//---------------------------------------------------------
function TdmSecurity.ObjectsCanBeModified (aObjectName: string; aIDArray: TIntArray): boolean;
//---------------------------------------------------------
var
  I: Integer;
begin
  CursorHourGlass;

  Result:= dmUserRegion.ObjectsCanBeModifiedByCurrentUser (gl_Obj.ItemByName[aObjectName].Type_, aIDArray);

  CursorDefault;
end;

//---------------------------------------------------------
function TdmSecurity.ObjectCanBeModified (aObjectName: string; aID: Integer): boolean;
//---------------------------------------------------------
var
  I: Integer;
begin
  Result:= true;

  if aID = 0 then
    exit;

  for i:= 0 to High(FUserObjectsArr) do
    if (FUserObjectsArr[i].ObjID = aID) and (FUserObjectsArr[i].ObjName = aObjectName) then
    begin
      Result:= FUserObjectsArr[i].Enabled;
      exit;
    end;

  CursorHourGlass;

  Result:= dmUserRegion.ObjectCanBeModifiedByCurrentUser (gl_Obj.ItemByName[aObjectName].Type_, aID);

  SetLength(FUserObjectsArr, High(FUserObjectsArr)+2);

  FUserObjectsArr[High(FUserObjectsArr)].ObjID:=   aID;
  FUserObjectsArr[High(FUserObjectsArr)].ObjName:= aObjectName;
  FUserObjectsArr[High(FUserObjectsArr)].Enabled:= Result;

  CursorRestore;
end;
{
//---------------------------------------------------------
procedure TdmSecurity.GetUserList(var aIDList: TIDList);
//---------------------------------------------------------
begin
  db_OpenTable(qry_Temp, TBL_USER);

  aIDList.LoadFromDataSet(qry_Temp);
end;}


end.



























/////  ParamByName('@Result').Value      := aRec.Description;



(*  if gl_DB.InTransAction() then
  begin
//    MsgDlg('���������� ���������� ������������� � �������� ����������. ���������� �����.')
    exit;
  end;*)


(*  sp_addlogin.Close;
  sp_grantdbaccess.Close;
  sp_addrolemember.Close;
  sp_dropuser.Close;
  sp_droplogin.Close;*)


(*  @IsSetLogin TBoolean,	-- ���� ������� ������������ �� �������
  @IsSetUser  TBoolean,	-- ���� ������� ������������ � ����
  @SetLoginError int,	-- ��������� ���������� ������������ �� ������
  @SetUserError int, 	-- ��������� ���������� ������������ � ����
  @UserError int, 	-- ��������� ���������� ������������ � �������
  @user_id int*)

(*  db_OpenQuery(qry_Temp, 'SELECT status from master..syslogins WHERE name = :login',
              [db_Par(FLD_LOGIN, aRec.Login)  ]);

  if qry_Temp.IsEmpty then
*)
(*  -- �������� ������� ������������ �� ������� � ���������� ���� ����
  if not exists (Select Status From master..syslogins Where Name=@Login)
  begin
    set @IsSetLogin = 1
    exec sp_addlogin @Login, @Password --, NULL, 'us_english'
    set @SetLoginError = @@Error
  end
  -- �������� ������� ������������ � ������� ���� � ���������� ���� ����
  if not exists (Select Status From sysusers Where Name=@Login)
  begin
    set @IsSetUser = 1
    exec sp_grantdbaccess @Login, @Login
    -- ������ �� ������?
    if @@Error <> 0
    begin
      set @SetUserError = @@Error
      -- ������ ���������� ������������ �� ������, ���� ��� ���� ����� ��� ����
      if exists (SELECT Status FROM master..syslogins WHERE Name=@Login) and (@IsSetLogin=1)
        EXEC sp_droplogin @Login
    end
  end
  -- ���� ������? ������������ �� �������� - ���������

  if (@SetLoginError <> 0) or (@SetUserError <> 0)
    return (-1)

  -- ���������� ���� db_DataReader ������������, ��� �� �� ��� ������ ������
  exec sp_addrolemember 'db_owner', @Login
  if @@Error <> 0
    return (-1)

  -- ���������� ������������ � ���� ��������, ��� ������ ��� ������ �������
  begin transaction
    insert into Users (login, name, phone, mail, address, group_id, Comments) values --access,
      (@login, @Name, @phone, @mail, @address, @group_id, @Description) --,1
    set @UserError = @@Error
    if @UserError = 0
    begin
      set @user_id = @@identity
    end
    if (@UserError = 0)  --(@GroupError = 0) and
    begin
      commit
      return @user_id
      -- ������������ ������� ��������, ���� � ��� ����� � ����� ��������,
      --   ������ ��� id
    end
    else
    begin
      rollback
      -- ����� ������� � ������ ���������� ������������� � ���� � �� ������, ���� �� ������ �������
      IF EXISTS (SELECT Status FROM sysusers WHERE Name=@Login) and (@IsSetUser=1)
        EXEC sp_dropuser @Login
      IF EXISTS (SELECT Status FROM master..syslogins WHERE Name=@Login) and (@IsSetLogin=1)
        EXEC sp_droplogin @Login
      return -2
    end
--  exec ChangeAllRole @group_id*/
GO*)


(*    Result:= gl_DB.AddRecordID (TBL_USER, true,
     [db_Par (FLD_PROJECT_ID,  dmMain.ProjectID),
      db_Par (FLD_FOLDER_ID,   IIF(FolderID>0,FolderID,NULL)),
      db_Par (FLD_NAME,        aRec.Name)
     ]);*)
  //Result:=gl_DB.GetIdentCurrent(TBL_USER);

