object dmSecurity: TdmSecurity
  OldCreateOrder = False
  Left = 666
  Top = 273
  Height = 521
  Width = 478
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 40
    Top = 24
  end
  object DataSource1: TDataSource
    DataSet = ADOTable1
    Left = 64
    Top = 136
  end
  object ADOTable1: TADOTable
    Connection = ADOConnection1
    CursorType = ctStatic
    Filter = 'country='#39'ru'#39
    Filtered = True
    TableName = 'GeoRegion'
    Left = 64
    Top = 88
    object ADOTable1id: TAutoIncField
      FieldName = 'id'
      ReadOnly = True
    end
    object ADOTable1name: TStringField
      FieldName = 'name'
      ReadOnly = True
      Size = 150
    end
    object ADOTable1code: TIntegerField
      FieldName = 'code'
      ReadOnly = True
    end
    object ADOTable1country: TStringField
      FieldName = 'country'
      ReadOnly = True
      Size = 2
    end
  end
  object ClientDataSet1: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProvider1'
    Left = 72
    Top = 200
    object ClientDataSet1id: TAutoIncField
      DisplayWidth = 7
      FieldName = 'id'
      ReadOnly = True
    end
    object ClientDataSet1name: TStringField
      DisplayWidth = 180
      FieldName = 'name'
      Size = 150
    end
    object ClientDataSet1code: TIntegerField
      DisplayWidth = 6
      FieldName = 'code'
    end
    object ClientDataSet1country: TStringField
      DisplayWidth = 7
      FieldName = 'country'
      Size = 2
    end
  end
  object DataSetProvider1: TDataSetProvider
    DataSet = ADOTable1
    Left = 72
    Top = 256
  end
  object DataSource2: TDataSource
    DataSet = ClientDataSet1
    Left = 160
    Top = 200
  end
  object ds_Roles: TDataSource
    DataSet = tbl_Roles
    Left = 168
    Top = 136
  end
  object tbl_Roles: TADOTable
    Connection = ADOConnection_Security
    CursorType = ctStatic
    Filtered = True
    TableName = 'GeoRegion'
    Left = 168
    Top = 88
    object AutoIncField1: TAutoIncField
      FieldName = 'id'
      ReadOnly = True
    end
    object StringField1: TStringField
      FieldName = 'name'
      ReadOnly = True
      Size = 150
    end
    object IntegerField1: TIntegerField
      FieldName = 'code'
      ReadOnly = True
    end
    object StringField2: TStringField
      FieldName = 'country'
      ReadOnly = True
      Size = 2
    end
  end
  object ADOConnection_Security: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=Security'
    LoginPrompt = False
    Left = 160
    Top = 24
  end
  object ds_Users: TDataSource
    DataSet = tbl_Users
    Left = 240
    Top = 136
  end
  object tbl_Users: TADOTable
    Active = True
    Connection = ADOConnection_Security
    CursorType = ctStatic
    Filtered = True
    TableName = 'Users'
    Left = 240
    Top = 88
    object tbl_UsersUserId: TAutoIncField
      FieldName = 'UserId'
      ReadOnly = True
    end
    object tbl_UsersFirstName: TWideStringField
      FieldName = 'FirstName'
      Size = 50
    end
    object tbl_UsersLastName: TWideStringField
      FieldName = 'LastName'
      Size = 50
    end
    object tbl_UsersEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object tbl_UsersLoginId: TWideStringField
      FieldName = 'LoginId'
      Size = 50
    end
    object tbl_UsersPassword: TWideStringField
      FieldName = 'Password'
      Size = 50
    end
    object tbl_UsersWindowsId: TWideStringField
      FieldName = 'WindowsId'
      Size = 500
    end
    object tbl_UsersUseWindowsAuth: TBooleanField
      FieldName = 'UseWindowsAuth'
    end
    object tbl_UsersBuiltInAccount: TBooleanField
      FieldName = 'BuiltInAccount'
    end
    object tbl_UsersSettingsXml: TMemoField
      FieldName = 'SettingsXml'
      BlobType = ftMemo
    end
    object tbl_UsersIsActive: TBooleanField
      FieldName = 'IsActive'
    end
    object tbl_UsersUseWindowsClient: TBooleanField
      FieldName = 'UseWindowsClient'
    end
    object tbl_UsersUseWebClient: TBooleanField
      FieldName = 'UseWebClient'
    end
    object tbl_UsersUseVSClient: TBooleanField
      FieldName = 'UseVSClient'
    end
    object tbl_UsersUseMixedMode: TBooleanField
      FieldName = 'UseMixedMode'
    end
    object tbl_UsersEnterpriseConnectionType: TIntegerField
      FieldName = 'EnterpriseConnectionType'
    end
    object tbl_UsersUseIPhoneClient: TBooleanField
      FieldName = 'UseIPhoneClient'
    end
    object tbl_UsersLastLoginDateTime: TDateTimeField
      FieldName = 'LastLoginDateTime'
    end
    object tbl_UsersReadMessages: TBlobField
      FieldName = 'ReadMessages'
    end
    object tbl_UsersIPhoneSettings: TBlobField
      FieldName = 'IPhoneSettings'
    end
  end
  object qry_Roles: TADOQuery
    Active = True
    Connection = ADOConnection_Security
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    AfterPost = qry_RolesAfterPost
    Parameters = <>
    SQL.Strings = (
      'select * from fn_UserSecurityRoles(1)')
    Left = 56
    Top = 352
  end
  object ds_Roles1: TDataSource
    DataSet = qry_Roles
    Left = 56
    Top = 408
  end
  object qry_Roles_XREF: TADOQuery
    Active = True
    Connection = ADOConnection_Security
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    Parameters = <>
    SQL.Strings = (
      'select * from UserSecurityRoles  where userID=1')
    Left = 160
    Top = 344
  end
  object ds_Roles_XREF: TDataSource
    DataSet = qry_Roles_XREF
    Left = 160
    Top = 408
  end
end
