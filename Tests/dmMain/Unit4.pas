unit Unit4;

interface

uses
  SysUtils, Classes, DB, ADODB, Provider, DBClient;

type
  TdmSecurity = class(TDataModule)
    ADOConnection1: TADOConnection;
    DataSource1: TDataSource;
    ADOTable1: TADOTable;
    ADOTable1id: TAutoIncField;
    ADOTable1name: TStringField;
    ADOTable1code: TIntegerField;
    ADOTable1country: TStringField;
    ClientDataSet1: TClientDataSet;
    ClientDataSet1id: TAutoIncField;
    ClientDataSet1name: TStringField;
    ClientDataSet1code: TIntegerField;
    ClientDataSet1country: TStringField;
    DataSetProvider1: TDataSetProvider;
    DataSource2: TDataSource;
    ds_Roles: TDataSource;
    tbl_Roles: TADOTable;
    AutoIncField1: TAutoIncField;
    StringField1: TStringField;
    IntegerField1: TIntegerField;
    StringField2: TStringField;
    ADOConnection_Security: TADOConnection;
    ds_Users: TDataSource;
    tbl_Users: TADOTable;
    tbl_UsersUserId: TAutoIncField;
    tbl_UsersFirstName: TWideStringField;
    tbl_UsersLastName: TWideStringField;
    tbl_UsersEmail: TWideStringField;
    tbl_UsersLoginId: TWideStringField;
    tbl_UsersPassword: TWideStringField;
    tbl_UsersWindowsId: TWideStringField;
    tbl_UsersUseWindowsAuth: TBooleanField;
    tbl_UsersBuiltInAccount: TBooleanField;
    tbl_UsersSettingsXml: TMemoField;
    tbl_UsersIsActive: TBooleanField;
    tbl_UsersUseWindowsClient: TBooleanField;
    tbl_UsersUseWebClient: TBooleanField;
    tbl_UsersUseVSClient: TBooleanField;
    tbl_UsersUseMixedMode: TBooleanField;
    tbl_UsersEnterpriseConnectionType: TIntegerField;
    tbl_UsersUseIPhoneClient: TBooleanField;
    tbl_UsersLastLoginDateTime: TDateTimeField;
    tbl_UsersReadMessages: TBlobField;
    tbl_UsersIPhoneSettings: TBlobField;
    qry_Roles: TADOQuery;
    ds_Roles1: TDataSource;
    qry_Roles_XREF: TADOQuery;
    ds_Roles_XREF: TDataSource;
    procedure qry_RolesAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public

  end;

var
  dmSecurity: TdmSecurity;

implementation

{$R *.dfm}

const
  FLD_CHECKED = 'checked';
  FLD_SecurityRoleId = 'SecurityRoleId';
  FLD_UserId = 'UserId';



procedure TdmSecurity.qry_RolesAfterPost(DataSet: TDataSet);
var
  bChecked,bLocated: Boolean;
  iID: Integer;
begin
  iID:=DataSet[FLD_SecurityRoleId];
  bLocated := qry_Roles_XREF.Locate(FLD_SecurityRoleId, iID, []);
  bChecked := DataSet[FLD_CHECKED];

  if (not bChecked) and (bLocated) then
      qry_Roles_XREF.Delete;

  if (bChecked) and (not bLocated) then
     db_AddRecord (qry_Roles_XREF,
                  [db_Par(FLD_UserId, 1),
                   db_Par(FLD_SecurityRoleId, iID) ]);

end;


end.
