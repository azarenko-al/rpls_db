program test_Full;

uses
  dm_Test in 'dm_Test.pas' {dmTest: TDataModule},
  dm_Test_AntType in 'dm_Test_AntType.pas' {dmTest_AntType: TDataModule},
  dm_Test_CalcMap in 'dm_Test_CalcMap.pas' {dmTest_CalcMap: TDataModule},
  dm_Test_CellLayer in 'dm_Test_CellLayer.pas' {dmTest_CellLayer: TDataModule},
  dm_Test_ColorSchema in 'dm_Test_ColorSchema.pas' {dmTest_ColorSchema: TDataModule},
  dm_Test_Combiner in 'dm_Test_Combiner.pas' {dmTest_Combiner: TDataModule},
  dm_Test_dmMain in 'dm_Test_dmMain.pas' {dmTest_dmMain: TDataModule},
  dm_Test_Folder in 'dm_Test_Folder.pas' {dmTest_Folder: TDataModule},
  dm_Test_GeoRegion in '..\..\src\PMP_CalcRegion\dm_Test_GeoRegion.pas' {dmTest_GeoRegion: TDataModule},
  dm_Test_Link in 'dm_Test_Link.pas' {dmTest_Link: TDataModule},
  dm_Test_LinkEnd in 'dm_Test_LinkEnd.pas' {dmTest_LinkEnd: TDataModule},
  dm_Test_LinkEndType in 'dm_Test_LinkEndType.pas' {dmTest_LinkEndType: TDataModule},
  dm_Test_LinkFreqPlan in 'dm_Test_LinkFreqPlan.pas' {dmTest_LinkFreqPlan: TDataModule},
  dm_Test_LinkLine in 'dm_Test_LinkLine.pas' {dmTest_LinkLine: TDataModule},
  dm_Test_LinkNet in 'dm_Test_LinkNet.pas' {dmTest_LinkNet: TDataModule},
  dm_Test_LinkType in 'dm_Test_LinkType.pas' {dmTest_LinkType: TDataModule},
  dm_Test_Map in 'dm_Test_Map.pas' {dmTest_Map: TDataModule},
  dm_Test_Map_Desktop in 'dm_Test_Map_Desktop.pas' {dmTest_Map_Desktop: TDataModule},
  dm_Test_MapFile in 'dm_Test_MapFile.pas' {dmTest_MapFile: TDataModule},
  dm_Test_MSC in 'dm_Test_MSC.pas' {dmTest_MSC: TDataModule},
  dm_Test_PmP_CalcRegion in 'dm_Test_PmP_CalcRegion.pas' {dmTest_PmP_CalcRegion: TDataModule},
  dm_Test_PmpSector in 'dm_Test_PmpSector.pas' {dmTest_PmpSector: TDataModule},
  dm_Test_PmpSite in 'dm_Test_PmpSite.pas' {dmTest_PmpSite: TDataModule},
  dm_Test_PmpTerminal in 'dm_Test_PmpTerminal.pas' {dmTest_PmpTerminal: TDataModule},
  dm_Test_Project in 'dm_Test_Project.pas' {dmTest_Project: TDataModule},
  dm_Test_Property in 'dm_Test_Property.pas' {dmTest_Property: TDataModule},
  dm_Test_RelFile in 'dm_Test_RelFile.pas' {dmTest_RelFile: TDataModule},
  dm_Test_Report in 'dm_Test_Report.pas' {dmTest_Report: TDataModule},
  dm_Test_Status in 'dm_Test_Status.pas' {dmTest_Status: TDataModule},
  dm_Test_Terminal in 'dm_Test_Terminal.pas' {dmTest_Terminal: TDataModule},
  dm_Test_Trx in 'dm_Test_Trx.pas' {dmTest_Trx: TDataModule},
  f_Main in 'f_Main.pas' {Form9},
  Forms;

// FastMM4;

{$R *.res}

{$define EnableMemoryLeakReporting}



begin
  Application.Initialize;
  Application.CreateForm(TForm9, Form9);
  Application.Run;

end.
