unit dm_Test_Folder;

interface

uses
  Classes, ADODB, Forms, DB,

  u_func,

  dm_Folder
  ;

type
  TdmTest_Folder = class(TDataModule)
    qry_Source: TADOQuery;
    tb_Projects: TADOTable;
//    procedure DataModuleCreate(Sender: TObject);
  private
    procedure TestAdd;
    { Private declarations }
  public
    class procedure Init;

    procedure Execute; virtual;

  end;

var
  dmTest_Folder: TdmTest_Folder;

implementation

{$R *.dfm}



//--------------------------------------------------------------------
class procedure TdmTest_Folder.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_Folder) then
    Application.CreateForm(TdmTest_Folder, dmTest_Folder);

end;

//---------------------------------------------------------------------------
procedure TdmTest_Folder.Execute;
//---------------------------------------------------------------------------
begin
 TestAdd

end;

//---------------------------------------------------------------------------
procedure TdmTest_Folder.TestAdd;
//---------------------------------------------------------------------------
var
  rec: TdmFolderAddRec;
begin
  FillChar(rec, SizeOf(rec), 0);

  rec.Name:=GetRandomStr();


end;



end.
