unit dm_Test_CellLayer;

interface

uses
  Classes, ADODB, Forms, DB ,   SysUtils,

  u_DB,

  dm_Onega_DB_data,

//  fr_Cell_Layer_View,

  u_const_db,


  u_func,

  dm_Cell_Layer, dm_Test


  ;

type
  TdmTest_CellLayer = class(TdmTest)
    tb_Projects: TADOTable;
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure Add;
    procedure TestView;
    { Private declarations }
  public
    class procedure Init;

    procedure Execute;

  end;

var
  dmTest_CellLayer: TdmTest_CellLayer;

implementation

{$R *.dfm}



procedure TdmTest_CellLayer.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmTest_CellLayer.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_CellLayer) then
    Application.CreateForm(TdmTest_CellLayer, dmTest_CellLayer);

end;

//---------------------------------------------------------------------------
procedure TdmTest_CellLayer.Execute;
//---------------------------------------------------------------------------
begin
  Add();

  TestView();

end;



//---------------------------------------------------------------------------
procedure TdmTest_CellLayer.Add;
//---------------------------------------------------------------------------
var
  rec: TdmCellLayerAddRec;
begin
  FillChar(rec, SizeOf(rec), 0);

  rec.Name:=GetRandomStr();



end;



//---------------------------------------------------------------------------
procedure TdmTest_CellLayer.TestView;
//---------------------------------------------------------------------------
var
  iID: Integer;
 // rec: TdmAntTypeInfoRec;

//  oView: Tframe_CellLayer_View;
begin
 // FillChar(rec, SizeOf(rec), 0);

 // rec.Name:=GetRandomStr();


  dmOnega_DB_data.OpenQuery(ADOQuery1, 'SELECT top 100 * FROM '+ TBL_CellLayer);


  with ADOQuery1 do
    while not EOF do
  begin
    iID:=FieldValues[FLD_ID];

//
//    oView:=Tframe_CellLayer_View.Create(nil);
//    oView.View(iID,'');
//  //  oView.Show;
//
//    FreeAndNil (oView);
//

    Next;
  end;


end;



begin
  TdmTest_CellLayer.Init;


end.
