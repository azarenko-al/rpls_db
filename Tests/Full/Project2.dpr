program Project2;

uses
  Forms,
  dm_Test_AntType in 'dm_Test_AntType.pas' {dmTest_AntType: TDataModule},
  dm_Test_dmMain in 'dm_Test_dmMain.pas' {dmTest_dmMain: TDataModule},
  dm_Test_CalcModel in 'dm_Test_CalcModel.pas' {dmTest_CalcModel: TDataModule},
  dm_Test_ColorSchema in 'dm_Test_ColorSchema.pas' {dmTest_ColorSchema: TDataModule},
  dm_Test_Project in 'dm_Test_Project.pas' {dmTest_Project: TDataModule},
  f_Main in 'f_Main.pas' {Form9};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm9, Form9);
  Application.Run;
end.
