unit dm_Test_Link;

interface

uses
  Classes, ADODB, Forms, DB,  SysUtils,

  u_DB,


  
  u_const_db,

  dm_Onega_DB_data,

   fr_Link_view,

  dm_act_Link,

  u_func,

  dm_Link
  ;

type
  TdmTest_Link = class(TDataModule)
    tb_Projects: TADOTable;
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure TestAdd;
    procedure TestView;
    { Private declarations }
  public
    class procedure Init;

    procedure Execute;

  end;

var
  dmTest_Link: TdmTest_Link;

implementation

{$R *.dfm}



procedure TdmTest_Link.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmTest_Link.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_Link) then
    Application.CreateForm(TdmTest_Link, dmTest_Link);

end;

//---------------------------------------------------------------------------
procedure TdmTest_Link.Execute;
//---------------------------------------------------------------------------
begin
  TestAdd;
  TestView;
end;



//---------------------------------------------------------------------------
procedure TdmTest_Link.TestAdd;
//---------------------------------------------------------------------------
var
  rec: TdmLinkAddRec;
begin
  FillChar(rec, SizeOf(rec), 0);

  rec.NewName:=GetRandomStr();

end;



//---------------------------------------------------------------------------
procedure TdmTest_Link.TestView;
//---------------------------------------------------------------------------
var
  iID: Integer;
 // rec: TdmAntTypeInfoRec;

  oView: Tframe_Link_View;
begin
 // FillChar(rec, SizeOf(rec), 0);

 // rec.Name:=GetRandomStr();


  dmOnega_DB_data.OpenQuery(ADOQuery1, 'SELECT top 100 * FROM '+ TBL_Link);


  with ADOQuery1 do
    while not EOF do
  begin
    iID:=FieldValues[FLD_ID];


    oView:=Tframe_Link_View.Create(nil);
    oView.View(iID,'');
  //  oView.Show;

    FreeAndNil (oView);


    Next;
  end;


end;


end.
