unit dm_Test_CalcRegion;

interface

uses
  Classes, ADODB, Forms, DB, SysUtils,

  u_func,

  u_const_db,

  dm_Onega_DB_data,                                      

fr_CalcRegion_view,

  dm_act_pmp_CalcRegion,
  dm_CalcRegion,


  dm_Test

  ;

type
  TdmTest_PmP_CalcRegion = class(TdmTest)
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure TestView;
    { Private declarations }
  public
    class procedure Init;

    procedure Execute;

  end;

var
  dmTest_PmP_CalcRegion: TdmTest_PmP_CalcRegion;

implementation

{$R *.dfm}



procedure TdmTest_PmP_CalcRegion.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmTest_PmP_CalcRegion.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_PmP_CalcRegion) then
    Application.CreateForm(TdmTest_PmP_CalcRegion, dmTest_PmP_CalcRegion);

end;

//---------------------------------------------------------------------------
procedure TdmTest_PmP_CalcRegion.Execute;
//---------------------------------------------------------------------------
var
  rec: TdmCalcRegion_pmp_AddRec;
begin
  FillChar(rec, SizeOf(rec), 0);

  rec.NewName:=GetRandomStr();


end;


//---------------------------------------------------------------------------
procedure TdmTest_PmP_CalcRegion.TestView;
//---------------------------------------------------------------------------
var
  iID: Integer;
 // rec: TdmAntTypeInfoRec;

  oView: Tframe_CalcRegion_View;
begin
 // FillChar(rec, SizeOf(rec), 0);

 // rec.Name:=GetRandomStr();


  dmOnega_DB_data.OpenQuery(ADOQuery1, 'SELECT top 100 * FROM '+ TBL_PMP_CalcRegion);


  with ADOQuery1 do
    while not EOF do
  begin
    iID:=FieldValues[FLD_ID];


    oView:=Tframe_CalcRegion_View.Create(nil);
    oView.View(iID,'');
  //  oView.Show;

    FreeAndNil (oView);


    Next;
  end;


end;


begin
  TdmTest_PmP_CalcRegion.Init;


end.
