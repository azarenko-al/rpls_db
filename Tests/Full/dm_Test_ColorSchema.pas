unit dm_Test_ColorSchema;

interface

uses
  Classes, ADODB, Forms, DB ,   SysUtils,


 dm_Onega_DB_data,


  u_const_db,                                                         

  dm_ColorSchema,
  dm_act_ColorSchema,

  fr_ColorSchema_View,

  u_func, dm_Test

  ;

type
  TdmTest_ColorSchema = class(TdmTest)
    tb_Projects: TADOTable;
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure TestAdd;
    procedure TestView;
    { Private declarations }
  public
    class procedure Init;

    procedure Execute;

  end;

var
  dmTest_ColorSchema: TdmTest_ColorSchema;

implementation

{$R *.dfm}



procedure TdmTest_ColorSchema.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmTest_ColorSchema.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_ColorSchema) then
    Application.CreateForm(TdmTest_ColorSchema, dmTest_ColorSchema);

end;


//---------------------------------------------------------------------------
procedure TdmTest_ColorSchema.TestAdd;
//---------------------------------------------------------------------------
var
  iID: Integer;
  rec: TdmColorSchemaAddRec;
begin
  FillChar(rec, SizeOf(rec), 0);

  rec.NewName:=GetRandomStr();

  iID:=dmColorSchema.Add(rec);

  Assert(iID>0);

  dmColorSchema.Del(iID);


end;



//---------------------------------------------------------------------------
procedure TdmTest_ColorSchema.Execute;
//---------------------------------------------------------------------------

begin
  TestAdd ;

  TestView();

end;


//---------------------------------------------------------------------------
procedure TdmTest_ColorSchema.TestView;
//---------------------------------------------------------------------------
var
  iID: Integer;
//  rec: TdmAntTypeInfoRec;

  oView: Tframe_ColorSchema_View;
begin
 // FillChar(rec, SizeOf(rec), 0);

 // rec.Name:=GetRandomStr();


  dmOnega_DB_data.OpenQuery(ADOQuery1, 'SELECT top 100 * FROM '+ TBL_ColorSchema);


  with ADOQuery1 do
    while not EOF do
  begin
    iID:=FieldValues[FLD_ID];


    oView:=Tframe_ColorSchema_View.Create(nil);
    oView.View(iID,'');
  //  oView.Show;

    FreeAndNil (oView);


    Next;
  end;


end;




end.
