unit dm_Test_LinkNet;

interface

uses
  Classes, ADODB, Forms, DB,  SysUtils,

  u_DB,

  u_func,

  dm_Onega_DB_data,

  dm_Project,


  u_const_db,

                                 
  fr_LinkNet_view,
  dm_act_LinkNet,
  dm_LinkNet, dm_Test


  ;

type
  TdmTest_LinkNet = class(TdmTest)
    tb_Projects: TADOTable;
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure TestAdd;
    procedure TestView;
    { Private declarations }
  public
    class procedure Init;

    procedure Execute; virtual;

  end;

var
  dmTest_LinkNet: TdmTest_LinkNet;

implementation

{$R *.dfm}



procedure TdmTest_LinkNet.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmTest_LinkNet.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_LinkNet) then
    Application.CreateForm(TdmTest_LinkNet, dmTest_LinkNet);

end;

//---------------------------------------------------------------------------
procedure TdmTest_LinkNet.Execute;
//---------------------------------------------------------------------------
begin
  TestAdd;

  TestView;


end;



//---------------------------------------------------------------------------
procedure TdmTest_LinkNet.TestAdd;
//---------------------------------------------------------------------------
var
  iID: Integer;
  iProject: Integer;
  rec: TdmLinkNetAddRec;
begin
  iProject:=dmProject.AddByName('____test___' + GetRandomStr());
  Assert(iProject>0);



  FillChar(rec, SizeOf(rec), 0);
  rec.Name:=GetRandomStr();


  iID:=dmLinkNet.Add_new(rec, nil, nil);

  Assert(iID>0);


  dmLinkNet.Del(iID);


  dmProject.Del(iProject);
end;



//---------------------------------------------------------------------------
procedure TdmTest_LinkNet.TestView;
//---------------------------------------------------------------------------
var
  iID: Integer;
 // rec: TdmAntTypeInfoRec;

  oView: Tframe_LinkNet_View;
begin
 // FillChar(rec, SizeOf(rec), 0);

 // rec.Name:=GetRandomStr();


  dmOnega_DB_data.OpenQuery(ADOQuery1, 'SELECT top 100 * FROM '+ TBL_LinkNet);


  with ADOQuery1 do
    while not EOF do
  begin
    iID:=FieldValues[FLD_ID];


    oView:=Tframe_LinkNet_View.Create(nil);
    oView.View(iID,'');
  //  oView.Show;

    FreeAndNil (oView);


    Next;
  end;


end;

begin
  TdmTest_LinkNet.Init;

end.


  iProject:=dmProject.AddByName('____test___' + GetRandomStr());
  Assert(iProject>0);

  dmProject.Del(iProject);

