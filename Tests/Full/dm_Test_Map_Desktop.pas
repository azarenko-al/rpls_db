unit dm_Test_Map_Desktop;

interface

uses
  Classes, ADODB, Forms, DB,

  u_func,

  dm_Map_Desktop, dm_Test

  ;                 

type
  TdmTest_Map_Desktop = class(TdmTest)
    qry_Source: TADOQuery;
    tb_Projects: TADOTable;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class procedure Init;

    procedure Execute;  virtual;

  end;

var
  dmTest_Map_Desktop: TdmTest_Map_Desktop;

implementation

{$R *.dfm}



procedure TdmTest_Map_Desktop.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmTest_Map_Desktop.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_Map_Desktop) then
    Application.CreateForm(TdmTest_Map_Desktop, dmTest_Map_Desktop);

end;

//---------------------------------------------------------------------------
procedure TdmTest_Map_Desktop.Execute;
//---------------------------------------------------------------------------

//var
//  rec: TdmAntennaAddRec;

begin
//  FillChar(rec, SizeOf(rec), 0);

//  rec.NewName:=GetRandomStr();

end;



begin
  TdmTest_Map_Desktop.Init;

end.
