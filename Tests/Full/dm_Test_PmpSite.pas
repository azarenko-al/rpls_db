unit dm_Test_PmpSite;

interface

uses
  Classes, ADODB, Forms, DB, SysUtils,


  //dm_Project,



  dm_Project,

  u_func,

  dm_Pmp_Site,

  u_const_db,

  dm_Onega_DB_data,

  fr_Pmp_Site_View,

  dm_Test


  ;                  

type
  TdmTest_PmpSite = class(TdmTest)
    tb_Projects: TADOTable;
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure TestAdd;
    procedure TestView;
    { Private declarations }
  public
    class procedure Init;

    procedure Execute;

  end;

var
  dmTest_PmpSite: TdmTest_PmpSite;

implementation

{$R *.dfm}



procedure TdmTest_PmpSite.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmTest_PmpSite.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_PmpSite) then
    Application.CreateForm(TdmTest_PmpSite, dmTest_PmpSite);

end;




//---------------------------------------------------------------------------
procedure TdmTest_PmpSite.TestAdd;
//---------------------------------------------------------------------------
var
  iProjectID: Integer;
  rec: TdmPMP_Site_Add_rec;
begin
  iProjectID:=dmProject.AddByName('___test_' + GetRandomStr());
  //--------------------------------------------------------------


  FillChar(rec, SizeOf(rec), 0);

  rec.NewName:=GetRandomStr();



  //--------------------------------------------------------------
  dmProject.Del(iProjectID);


end;


//---------------------------------------------------------------------------
procedure TdmTest_PmpSite.Execute;
//---------------------------------------------------------------------------
begin
  TestAdd;

  TestView;
end;


//---------------------------------------------------------------------------
procedure TdmTest_PmpSite.TestView;
//---------------------------------------------------------------------------
var
  iID: Integer;
  //rec: TdmAntTypeInfoRec;

  oView: Tframe_Pmp_Site_View;
begin
//  FillChar(rec, SizeOf(rec), 0);

 // rec.Name:=GetRandomStr();


  dmOnega_DB_data.OpenQuery(ADOQuery1, 'SELECT top 100 * FROM '+ TBL_Pmp_Site);


  with ADOQuery1 do
    while not EOF do
  begin
    iID:=FieldValues[FLD_ID];


    oView:=Tframe_Pmp_Site_View.Create(nil);
    oView.View(iID,'');
  //  oView.Show;

    FreeAndNil (oView);


    Next;
  end;


end;




begin


 TdmTest_PmpSite.Init;
end.
