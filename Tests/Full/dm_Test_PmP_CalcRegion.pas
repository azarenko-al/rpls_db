unit dm_Test_PmP_CalcRegion;

interface

uses
  Classes, ADODB, Forms, DB, SysUtils,

  u_func,

  dm_Project,

  u_GEO,

  u_const_db,

  dm_Onega_DB_data,                                      

fr_CalcRegion_view,

  dm_act_pmp_CalcRegion,
  dm_CalcRegion, dm_Test

  ;

type
  TdmTest_PmP_CalcRegion = class(TdmTest)
    ADOQuery1: TADOQuery;
 //   procedure DataModuleCreate(Sender: TObject);
  private
    procedure TestAdd;
    procedure TestView;

  public
    class procedure Init;

    procedure Execute; virtual;

  end;

var
  dmTest_PmP_CalcRegion: TdmTest_PmP_CalcRegion;

implementation

{$R *.dfm}



//--------------------------------------------------------------------
class procedure TdmTest_PmP_CalcRegion.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_PmP_CalcRegion) then
    Application.CreateForm(TdmTest_PmP_CalcRegion, dmTest_PmP_CalcRegion);

end;


//---------------------------------------------------------------------------
procedure TdmTest_PmP_CalcRegion.TestAdd;
//---------------------------------------------------------------------------
var
  iID: Integer;
  rec: TdmCalcRegion_pmp_AddRec;

  bl: TBLPoint;
  iProjectID: Integer;
begin
  iProjectID:=dmProject.AddByName('___test_' + GetRandomStr());

  FillChar(rec, SizeOf(rec), 0);

//    rec.GeoRegionID:= dmGeoRegion.GetID_ByBLPoint(rec.BLPoint_Pulkovo);


  rec.ProjectID:=iProjectID;

  rec.NewName:=GetRandomStr();


  rec.CalcModelID:= GetTableRandomRecordID (TBL_CalcModel);
  rec.ClutterModelID:= GetTableRandomRecordID (TBL_ClutterModel);


  bl.B:=56;
  bl.L:=45;

  //// ---------------------------------------------------------------
//function geo_MakeBLPointsByCircle_new(aCenter: TBLPoint; aRadius_m: Double):
//    TBLPointArray;



  rec.PointArr1:=geo_MakeBLPointsByCircle_new(bl, 1000);

  iID:=dmCalcRegion_pmp.Add(rec);

  Assert(iID>0);

  dmCalcRegion_pmp.Del(iID);


  dmProject.Del (iProjectID);


end;


//---------------------------------------------------------------------------
procedure TdmTest_PmP_CalcRegion.Execute;
//---------------------------------------------------------------------------
begin
  TestAdd;

  TestView
end;


//---------------------------------------------------------------------------
procedure TdmTest_PmP_CalcRegion.TestView;
//---------------------------------------------------------------------------
var
  iID: Integer;
 // rec: TdmAntTypeInfoRec;

  oView: Tframe_CalcRegion_View;
begin
 // FillChar(rec, SizeOf(rec), 0);

 // rec.Name:=GetRandomStr();


  dmOnega_DB_data.OpenQuery(ADOQuery1, 'SELECT top 100 * FROM '+ TBL_PMP_CalcRegion);


  with ADOQuery1 do
    while not EOF do
  begin
    iID:=FieldValues[FLD_ID];


    oView:=Tframe_CalcRegion_View.Create(nil);
    oView.View(iID,'');
  //  oView.Show;

    FreeAndNil (oView);


    Next;
  end;


end;


begin
  TdmTest_PmP_CalcRegion.Init;


end.
