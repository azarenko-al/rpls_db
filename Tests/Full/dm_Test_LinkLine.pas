unit dm_Test_LinkLine;

interface

uses
  Classes, ADODB, Forms, DB,   SysUtils,

  u_DB,

  fr_LinkLine_View,

  dm_Onega_DB_data,

  u_const_db,

  dm_Test,

  u_func,

  dm_LinkLine


  ;

type
  TdmTest_LinkLine = class(TdmTest)
    tb_Projects: TADOTable;
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure TestAdd;
    procedure TestView;
    { Private declarations }
  public
    class procedure Init;

    procedure Execute; virtual;

  end;

var
  dmTest_LinkLine: TdmTest_LinkLine;

implementation


{$R *.dfm}



procedure TdmTest_LinkLine.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmTest_LinkLine.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_LinkLine) then
    Application.CreateForm(TdmTest_LinkLine, dmTest_LinkLine);

end;

//---------------------------------------------------------------------------
procedure TdmTest_LinkLine.Execute;
//---------------------------------------------------------------------------
begin
  TestAdd;

  TestView;

end;



//---------------------------------------------------------------------------
procedure TdmTest_LinkLine.TestAdd;
//---------------------------------------------------------------------------
var
  iID: Integer;
  rec: TdmLinkLineAddRec;
begin
  FillChar(rec, SizeOf(rec), 0);

  rec.Name:=GetRandomStr();

  iID:=dmLinkLine.Add(rec, nil);

  Assert(iID>0);


  dmLinkLine.Del(iID);


end;


//---------------------------------------------------------------------------
procedure TdmTest_LinkLine.TestView;
//---------------------------------------------------------------------------
var
  iID: Integer;
 // rec: TdmAntTypeInfoRec;

  oView: Tframe_LinkLine_View;
begin
 // FillChar(rec, SizeOf(rec), 0);

 // rec.Name:=GetRandomStr();


  dmOnega_DB_data.OpenQuery(ADOQuery1, 'SELECT top 100 * FROM '+ TBL_LinkLine);


  with ADOQuery1 do
    while not EOF do
  begin
    iID:=FieldValues[FLD_ID];


    oView:=Tframe_LinkLine_View.Create(nil);
    oView.View(iID,'');
  //  oView.Show;

    FreeAndNil (oView);


    Next;
  end;


end;


 begin
   TdmTest_LinkLine.Init;

end.
