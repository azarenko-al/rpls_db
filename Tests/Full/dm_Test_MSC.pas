unit dm_Test_MSC;

interface

uses
  Classes, ADODB, Forms, DB,
               
  u_func,

  dm_MSC, dm_Test
  ;

type
  TdmTest_MSC = class(TdmTest)
    qry_Source: TADOQuery;                                             
    tb_Projects: TADOTable;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class procedure Init;

    procedure Execute;

  end;

var
  dmTest_MSC: TdmTest_MSC;

implementation

{$R *.dfm}



procedure TdmTest_MSC.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmTest_MSC.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_MSC) then
    Application.CreateForm(TdmTest_MSC, dmTest_MSC);

end;

//---------------------------------------------------------------------------
procedure TdmTest_MSC.Execute;
//---------------------------------------------------------------------------
var
  rec: TdmMSCAddRec;
begin
  FillChar(rec, SizeOf(rec), 0);

  rec.NewName:=GetRandomStr();

end;

begin
TdmTest_MSC.Init;

end.
