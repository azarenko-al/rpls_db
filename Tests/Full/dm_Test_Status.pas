unit dm_Test_Status;

interface

uses
  Classes, ADODB, Forms, DB,

  dm_Test


  ;

type
  TdmTest_Status = class(TdmTest)
    qry_Source: TADOQuery;
    tb_Projects: TADOTable;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class procedure Init;

    procedure Execute;

  end;

var
  dmTest_Status: TdmTest_Status;

implementation

{$R *.dfm}



procedure TdmTest_Status.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmTest_Status.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_Status) then
    Application.CreateForm(TdmTest_Status, dmTest_Status);

end;

//---------------------------------------------------------------------------
procedure TdmTest_Status.Execute;
//---------------------------------------------------------------------------
begin



end;

end.
