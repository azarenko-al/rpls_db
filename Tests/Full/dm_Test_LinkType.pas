unit dm_Test_LinkType;

interface

uses
  Classes, ADODB, Forms, DB,


  dm_Test


  ;

type
  TdmTest_LinkType = class(TdmTest)
    tb_Projects: TADOTable;
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class procedure Init;

    procedure Execute; virtual;

  end;

var
  dmTest_LinkType: TdmTest_LinkType;

implementation

{$R *.dfm}



procedure TdmTest_LinkType.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmTest_LinkType.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_LinkType) then
    Application.CreateForm(TdmTest_LinkType, dmTest_LinkType);

end;

//---------------------------------------------------------------------------
procedure TdmTest_LinkType.Execute;
//---------------------------------------------------------------------------
begin


end;

begin
  TdmTest_LinkType.Init;

end.
