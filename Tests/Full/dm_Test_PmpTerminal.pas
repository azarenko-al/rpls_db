unit dm_Test_PmpTerminal;

interface

uses
  Classes, ADODB, Forms, DB,

  u_func,

  dm_LinkEnd,

  dm_Test,

  dm_PmpTerminal
  ;

type
  TdmTest_PmpTerminal = class(TdmTest)
    qry_Source: TADOQuery;
    tb_Projects: TADOTable;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class procedure Init;

    procedure Execute;  virtual;

  end;

var
  dmTest_PmpTerminal: TdmTest_PmpTerminal;

implementation

{$R *.dfm}



procedure TdmTest_PmpTerminal.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmTest_PmpTerminal.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_PmpTerminal) then
    Application.CreateForm(TdmTest_PmpTerminal, dmTest_PmpTerminal);

end;

//---------------------------------------------------------------------------
procedure TdmTest_PmpTerminal.Execute;
//---------------------------------------------------------------------------
var
  rec: TdmLinkEndAddRec;
begin
  FillChar(rec, SizeOf(rec), 0);

 // rec.Name:=GetRandomStr();


end;

end.
