unit dm_Test_Project;

interface

uses
  Classes, ADODB, Forms, DB,   SysUtils,

  u_DB,

  dm_Onega_DB_data,
                         

  u_const_db,

  u_func,


  fr_Project_view,

  dm_Project, dm_Test


  ;

type
  TdmTest_Project = class(TdmTest)
    tb_Projects: TADOTable;
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure TestAdd;
    procedure TestView;
//    function GetRandomStr: string;
    { Private declarations }
  public
    class procedure Init;

    procedure Execute;

  end;

var
  dmTest_Project: TdmTest_Project;

implementation

{$R *.dfm}



procedure TdmTest_Project.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmTest_Project.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_Project) then
    Application.CreateForm(TdmTest_Project, dmTest_Project);

end;


//---------------------------------------------------------------------------
procedure TdmTest_Project.TestAdd;
//---------------------------------------------------------------------------
var
  iProject: Integer;
  rec: TdmProjectAddRec;
  s: string;

begin
  FillChar(rec, SizeOf(rec), 0);

  rec.Name:=GetRandomStr();


  iProject:=dmProject.Add(rec);

  Assert(iProject>0);


  dmProject.Del(iProject);

//TdmProjectAddRec


end;



//---------------------------------------------------------------------------
procedure TdmTest_Project.TestView;
//---------------------------------------------------------------------------
var
  iID: Integer;
  //rec: TdmAntTypeInfoRec;

  oView: Tframe_Project_View;
begin
//  FillChar(rec, SizeOf(rec), 0);

 // rec.Name:=GetRandomStr();


  dmOnega_DB_data.OpenQuery(ADOQuery1, 'SELECT top 100 * FROM '+ TBL_Project);


  with ADOQuery1 do
    while not EOF do
  begin
    iID:=FieldValues[FLD_ID];


    oView:=Tframe_Project_View.Create(nil);
    oView.View(iID,'');
  //  oView.Show;

    FreeAndNil (oView);


    Next;
  end;


end;



//---------------------------------------------------------------------------
procedure TdmTest_Project.Execute;
//---------------------------------------------------------------------------
begin
  TestAdd;

  TestView;

end;



begin
  TdmTest_Project.Init;
 
end.
