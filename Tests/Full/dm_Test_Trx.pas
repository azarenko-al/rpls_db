unit dm_Test_Trx;

interface

uses
  Classes, ADODB, Forms, DB,

  u_func,

  dm_Test,

  dm_TrxType

  ;

type
  TdmTest_Trx = class(TdmTest)
    qry_Source: TADOQuery;
    tb_Projects: TADOTable;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class procedure Init;

    procedure Execute; virtual;

  end;

var
  dmTest_Trx: TdmTest_Trx;

implementation

{$R *.dfm}



procedure TdmTest_Trx.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmTest_Trx.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_Trx) then
    Application.CreateForm(TdmTest_Trx, dmTest_Trx);                

end;

//---------------------------------------------------------------------------
procedure TdmTest_Trx.Execute;
//---------------------------------------------------------------------------
var
  rec: TdmTrxTypeAddRec;
begin
  FillChar(rec, SizeOf(rec), 0);

  rec.NewName:=GetRandomStr();


end;

begin
   TdmTest_Trx.Init;

end.
