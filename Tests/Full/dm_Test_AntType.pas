
unit dm_Test_AntType;

interface

uses
  Classes, ADODB, Forms, DB, SysUtils,

  u_DB,

  u_func,

  dm_Test,

  dm_Onega_DB_data,


  u_const_db,

  fr_AntType_view,

  dm_act_AntType,

  dm_AntType;

type
  TdmTest_AntType = class(TdmTest)
    tb_Projects: TADOTable;
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure TestView;
    { Private declarations }
  protected
    procedure Execute; override;
  public
    class procedure Init;

  end;




var
  dmTest_AntType: TdmTest_AntType;

implementation

{$R *.dfm}



procedure TdmTest_AntType.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmTest_AntType.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_AntType) then
    Application.CreateForm(TdmTest_AntType, dmTest_AntType);

end;

//---------------------------------------------------------------------------
procedure TdmTest_AntType.Execute;
//---------------------------------------------------------------------------
var

  rec: TdmAntTypeInfoRec;

  oView: Tframe_AntType_View;
begin
  FillChar(rec, SizeOf(rec), 0);

 // rec.Name:=GetRandomStr();


  TestView();


end;



//---------------------------------------------------------------------------
procedure TdmTest_AntType.TestView;
//---------------------------------------------------------------------------
var
  iID: Integer;
  //rec: TdmAntTypeInfoRec;

  oView: Tframe_AntType_View;
begin
//  FillChar(rec, SizeOf(rec), 0);

 // rec.Name:=GetRandomStr();


  dmOnega_DB_data.OpenQuery(ADOQuery1, 'SELECT top 100 * FROM '+ TBL_AntennaType);


  with ADOQuery1 do
    while not EOF do
  begin
    iID:=FieldValues[FLD_ID];


    oView:=Tframe_AntType_View.Create(nil);
    oView.View(iID,'');
  //  oView.Show;

    FreeAndNil (oView);


    Next;
  end;


end;


begin
  TdmTest_AntType.Init;


end.
