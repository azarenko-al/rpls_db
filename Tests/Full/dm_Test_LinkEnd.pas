unit dm_Test_LinkEnd;

interface

uses
  Classes, ADODB, Forms, DB,  SysUtils,

  u_DB,


dm_Project,

  u_const_db,
  u_func,

  dm_Onega_DB_data,

  fr_LinkEnd_View,

  dm_act_LinkEnd,

  dm_LinkEnd, dm_Test
  ;

type
  TdmTest_LinkEnd = class(TdmTest)
    tb_Projects: TADOTable;
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure TestAdd;
    procedure TestView;
    { Private declarations }
  public
    class procedure Init;

    procedure Execute;

  end;

var
  dmTest_LinkEnd: TdmTest_LinkEnd;

implementation

{$R *.dfm}



procedure TdmTest_LinkEnd.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmTest_LinkEnd.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_LinkEnd) then
    Application.CreateForm(TdmTest_LinkEnd, dmTest_LinkEnd);

end;

//---------------------------------------------------------------------------
procedure TdmTest_LinkEnd.Execute;
//---------------------------------------------------------------------------
begin
  TestAdd;
  TestView;

end;



//---------------------------------------------------------------------------
procedure TdmTest_LinkEnd.TestAdd;
//---------------------------------------------------------------------------
var
  iProject: Integer;
  rec: TdmLinkEndAddRec;
begin
  iProject:=dmProject.AddByName('____test___' + GetRandomStr());


  FillChar(rec, SizeOf(rec), 0);

  rec.NewName:=GetRandomStr();



  Assert(iProject>0);

  dmProject.Del(iProject);


end;




//---------------------------------------------------------------------------
procedure TdmTest_LinkEnd.TestView;
//---------------------------------------------------------------------------
var
  iID: Integer;
//  rec: TdmAntTypeInfoRec;

  oView: Tframe_LinkEnd_View;
begin
 // FillChar(rec, SizeOf(rec), 0);

 // rec.Name:=GetRandomStr();


  dmOnega_DB_data.OpenQuery(ADOQuery1, 'SELECT top 100 * FROM '+ TBL_LinkEnd);


  with ADOQuery1 do
    while not EOF do
  begin
    iID:=FieldValues[FLD_ID];


    oView:=Tframe_LinkEnd_View.Create(nil);
    oView.View(iID,'');
  //  oView.Show;

    FreeAndNil (oView);


    Next;
  end;


end;


begin
  TdmTest_LinkEnd.Init;

end.
