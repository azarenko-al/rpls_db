unit dm_Test_Property;

interface

uses
  Classes, ADODB, Forms, DB,   SysUtils,

  u_DB,

  dm_Main,

  u_geo,


  dm_Project,

  dm_GeoRegion,

 // dm_DB_RPLS_Lib,

  dm_Onega_DB_data,

  fr_Property_View,

  u_const_db,

  u_Property_Add,
  
  u_func,

  dm_Property,

  dm_Test

  ;

type
  TdmTest_Property = class(TdmTest)
    tb_Projects: TADOTable;
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure Add;
    procedure AddSimple(aProjectID : integer);
    procedure TestView;
    { Private declarations }
  public
    class procedure Init;

    procedure Execute;

  end;

var
  dmTest_Property: TdmTest_Property;

implementation

{$R *.dfm}



procedure TdmTest_Property.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmTest_Property.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_Property) then
    Application.CreateForm(TdmTest_Property, dmTest_Property);

end;

//---------------------------------------------------------------------------
procedure TdmTest_Property.Execute;
//---------------------------------------------------------------------------
begin
  Add();

  TestView;


end;



//---------------------------------------------------------------------------
procedure TdmTest_Property.Add;
//---------------------------------------------------------------------------
var
  iProperty: Integer;
  rec: TdmPropertyAddRec1;

  bl:  TBLPoint;
  iProject: Integer;

  oAdd: TPropertyAddToDB;
begin
  iProject:=dmProject.AddByName('aaaaaaaaaaaaaaaaaaaaa');
  Assert(iProject>0);

//  dmMain.ProjectID:=iProject;

  oAdd:=TPropertyAddToDB.create;

  FillChar(rec, SizeOf(rec), 0);


  rec.Project_ID:=iProject;

  rec.Name:=GetRandomStr();


  rec.BLPoint_Pulkovo.B:=56.66;
  rec.BLPoint_Pulkovo.L:=46.66;

  rec.GeoRegionID:= dmGeoRegion.GetID_ByBLPoint(rec.BLPoint_Pulkovo);


  iProperty := dmProperty.Add (rec);

  Assert(iProperty>0);

  dmProperty.Del(iProperty);


 // obj: TPropertyAddToDB;


  FreeAndNil(oAdd);


  dmProject.Del(iProject);

//  bl.

//  dmProperty.GetNearestIDandPos()


  //TPropertyAddToDB


end;


//---------------------------------------------------------------------------
procedure TdmTest_Property.AddSimple(aProjectID : integer);
//---------------------------------------------------------------------------
var
  iProperty: Integer;
  rec: TdmPropertyAddRec1;

  bl:  TBLPoint;
  iProject: Integer;

  oAdd: TPropertyAddToDB;
begin
 // iProject:=dmProject.AddByName('aaaaaaaaaaaaaaaaaaaaa');
 // Assert(iProject>0);

//  dmMain.ProjectID:=iProject;

  oAdd:=TPropertyAddToDB.create;

  FillChar(rec, SizeOf(rec), 0);

  rec.Project_ID:=iProject;
  rec.Name:=GetRandomStr();


  rec.BLPoint_Pulkovo.B:=56.66;
  rec.BLPoint_Pulkovo.L:=46.66;

  rec.GeoRegionID:= dmGeoRegion.GetID_ByBLPoint(rec.BLPoint_Pulkovo);


  iProperty := dmProperty.Add (rec);

  Assert(iProperty>0);

  dmProperty.Del(iProperty);


 // obj: TPropertyAddToDB;


  FreeAndNil(oAdd);


  dmProject.Del(iProject);

//  bl.

//  dmProperty.GetNearestIDandPos()


  //TPropertyAddToDB


end;



//---------------------------------------------------------------------------
procedure TdmTest_Property.TestView;
//---------------------------------------------------------------------------
var
  iID: Integer;
//  rec: TdmAntTypeInfoRec;

  oView: Tframe_Property_View;
begin
//  FillChar(rec, SizeOf(rec), 0);

 // rec.Name:=GetRandomStr();


  dmOnega_DB_data.OpenQuery(ADOQuery1, 'SELECT top 100 * FROM '+ TBL_Property);


  with ADOQuery1 do
    while not EOF do
  begin
    iID:=FieldValues[FLD_ID];


    oView:=Tframe_Property_View.Create(nil);
    oView.View(iID,'');
  //  oView.Show;

    FreeAndNil (oView);


    Next;
  end;


end;


begin
   TdmTest_Property.Init;
end.
