unit f_Main;

interface

uses
  Classes, Controls, Forms,


  dm_Test_dmMain, dm_Test_AntType, dm_Test_Project, dm_Test_CalcModel,
  dm_Test_ColorSchema, dm_Test_Combiner, dm_Test_Terminal, dm_Test_Trx,
  dm_Test_Report, dm_Test_LinkType, dm_Test_Property, dm_Test_LinkEnd,
  dm_Test_Link, dm_Test_PmpSite, dm_Test_pmp_CalcRegion
  ;

type
  TForm9 = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form9: TForm9;

implementation


{$R *.dfm}

procedure TForm9.FormCreate(Sender: TObject);
begin
  dmTest_dmMain.Execute;

  dmTest_PmP_CalcRegion.Execute;

  dmTest_Property.Execute;

  dmTest_Project.Execute;




  dmTest_AntType.Execute;

  //---------------------------------------------------------------------------

  dmTest_LinkEnd.Execute;

  dmTest_Link.Execute;

  dmTest_PmpSite.Execute;



  //---------------------------------------------------------------------------


  dmTest_CalcModel.Execute;

  dmTest_ColorSchema.Execute;

  dmTest_Combiner.Execute;

  dmTest_Terminal.Execute;

  dmTest_Trx.Execute;

  dmTest_Report.Execute;

  dmTest_LinkType.Execute;





end;

end.
