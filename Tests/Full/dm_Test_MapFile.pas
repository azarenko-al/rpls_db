unit dm_Test_MapFile;

interface

uses
  Classes, ADODB, Forms, DB,   SysUtils,

  dm_Project,

  u_DB,                                       

  u_func,
  u_files,

  dm_Onega_DB_data,

  dm_Test,


  u_const_db,

  dm_MapFile

  ;

type
  TdmTest_MapFile = class(TdmTest)
    tb_Projects: TADOTable;
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure TestAdd;

    { Private declarations }
  public
    class procedure Init;

    procedure Execute;

  end;

var
  dmTest_MapFile: TdmTest_MapFile;

implementation

{$R *.dfm}



procedure TdmTest_MapFile.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmTest_MapFile.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_MapFile) then
    Application.CreateForm(TdmTest_MapFile, dmTest_MapFile);

end;

//---------------------------------------------------------------------------
procedure TdmTest_MapFile.TestAdd;
//---------------------------------------------------------------------------
const
  DEF_DIR = 'F:\GIS\NNovgorod_plan';

var
  iMap: Integer;
  iProjectID: Integer;
  oStrList: TStringList;
  rec: TdmMapFileAddRec;
begin
  oStrList:=TStringList.Create;
  ScanDir1 (DEF_DIR, '*.tab', oStrList);//, False);

  Assert(oStrList.Count>0);


  iProjectID:=dmProject.AddByName('___test_' + GetRandomStr());



  FillChar(rec, SizeOf(rec), 0);

  rec.FileName:=GetRandomStr();

  iMap:=dmMapFile.AddFileName(oStrList[0]);

  dmMapFile.Del(iMap);

//  FromDir(DEF_DIR);


//  dmMapFile.AddFileName();



  dmMapFile.AddFromDir(DEF_DIR);


  //F:\GIS\NNovgorod_plan

  dmProject.Del (iProjectID);


  oStrList.Free;


end;


//---------------------------------------------------------------------------
procedure TdmTest_MapFile.Execute;
//---------------------------------------------------------------------------
begin
 TestAdd;


end;




begin

 TdmTest_MapFile.Init;

end.


  oStrList:=TStringList.Create;

{ TODO : reduce }
  // -------------------------------------------------------------------
  //FindFilesByMaskEx (aFileDir, '*.tab', oStrList);//, False);
  ScanDir1 (aFileDir, '*.tab', oStrList);//, False);
