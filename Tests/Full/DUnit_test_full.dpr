//{$DEFINE DUNIT_CONSOLE_MODE}

program DUnit_test_full;

uses
 // FastMM4,
  SysUtils,
  TestFramework,
  TestExtensions,
  GUITestRunner,
  TextTestRunner,
  Unit3 in 'Unit3.pas',
  u_Map_test in 'u_Map_test.pas',
  dm_Test_dmMain in 'dm_Test_dmMain.pas' {dmTest_dmMain: TDataModule};

{$IFDEF DUNIT_CONSOLE_MODE}
  {$APPTYPE CONSOLE}
{$ELSE}
  {$R *.RES}
{$ENDIF}

{$define EnableMemoryLeakReporting}



begin
{$IFDEF DUNIT_CONSOLE_MODE}
  if not FindCmdLineSwitch('Graphic', ['-','/'], True) then
    TextTestRunner.RunRegisteredTests(rxbHaltOnFailures)
  else
{$ENDIF}
    GUITestRunner.RunRegisteredTests;
end.
