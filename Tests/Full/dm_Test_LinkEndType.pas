unit dm_Test_LinkEndType;

interface

uses
  Classes, ADODB, Forms, DB, SysUtils,

  dm_Onega_DB_data,


  u_const_db,

  u_func,

  fr_LinkEndType_view,

  dm_LinkEndType, dm_Test


  ;

type
  TdmTest_LinkEndType = class(TdmTest)
    tb_Projects: TADOTable;
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure TestAdd;
    procedure TestView;

    { Private declarations }
  public
    class procedure Init;

    procedure Execute;

  end;

var
  dmTest_LinkEndType: TdmTest_LinkEndType;

implementation

{$R *.dfm}



procedure TdmTest_LinkEndType.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmTest_LinkEndType.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_LinkEndType) then
    Application.CreateForm(TdmTest_LinkEndType, dmTest_LinkEndType);

end;

//---------------------------------------------------------------------------
procedure TdmTest_LinkEndType.Execute;
//---------------------------------------------------------------------------
begin
  TestAdd;
  TestView;
end;

//---------------------------------------------------------------------------
procedure TdmTest_LinkEndType.TestAdd;
//---------------------------------------------------------------------------
var
  rec: TdmLinkEndTypeAddRec;
begin
  FillChar(rec, SizeOf(rec), 0);

  rec.Name:=GetRandomStr();

end;



//---------------------------------------------------------------------------
procedure TdmTest_LinkEndType.TestView;
//---------------------------------------------------------------------------
var
  iID: Integer;
  //rec: TdmAntTypeInfoRec;

  oView: Tframe_LinkEndType_View;
begin
//  FillChar(rec, SizeOf(rec), 0);

 // rec.Name:=GetRandomStr();


  dmOnega_DB_data.OpenQuery(ADOQuery1, 'SELECT top 100 * FROM '+ TBL_LinkEndType);


  with ADOQuery1 do
    while not EOF do
  begin
    iID:=FieldValues[FLD_ID];


    oView:=Tframe_LinkEndType_View.Create(nil);
    oView.View(iID,'');
  //  oView.Show;

    FreeAndNil (oView);


    Next;
  end;


end;



begin
  TdmTest_LinkEndType.Init;
end.
