unit dm_Test_LinkFreqPlan;

interface

uses

  Classes, ADODB, Forms, DB,  SysUtils,

  dm_Project,

  u_DB,

  u_func,

  dm_Test,

  dm_Onega_DB_data,

  u_const_db,

  fr_LinkFreqPlan_View,

  dm_act_LinkFreqPlan,
  dm_LinkFreqPlan


  ;

type
  TdmTest_LinkFreqPlan = class(TdmTest)
    tb_Projects: TADOTable;
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure TestView;
procedure TestAdd;


  public
    class procedure Init;

    procedure Execute; override;

  end;

var
  dmTest_LinkFreqPlan: TdmTest_LinkFreqPlan;

implementation

{$R *.dfm}



procedure TdmTest_LinkFreqPlan.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmTest_LinkFreqPlan.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_LinkFreqPlan) then
    Application.CreateForm(TdmTest_LinkFreqPlan, dmTest_LinkFreqPlan);

end;

//---------------------------------------------------------------------------
procedure TdmTest_LinkFreqPlan.Execute;
//---------------------------------------------------------------------------
begin
  TestAdd;
  TestView;

end;


//---------------------------------------------------------------------------
procedure TdmTest_LinkFreqPlan.TestAdd;
//---------------------------------------------------------------------------
var
  iProjectID: Integer;
  rec: TdmFreqPlanRRLAddRec;
begin
  iProjectID:=dmProject.AddByName('___test_' + GetRandomStr());


  FillChar(rec, SizeOf(rec), 0);

  rec.NewName:=GetRandomStr();




  dmProject.Del (iProjectID);
        
end;




//---------------------------------------------------------------------------
procedure TdmTest_LinkFreqPlan.TestView;
//---------------------------------------------------------------------------
var
  iID: Integer;
  //rec: TdmAntTypeInfoRec;

  oView: Tframe_LinkFreqPlan_View;
begin
//  FillChar(rec, SizeOf(rec), 0);

 // rec.Name:=GetRandomStr();


  dmOnega_DB_data.OpenQuery(ADOQuery1, 'SELECT top 100 * FROM '+ TBL_LinkFreqPlan);


  with ADOQuery1 do
    while not EOF do
  begin
    iID:=FieldValues[FLD_ID];


    oView:=Tframe_LinkFreqPlan_View.Create(nil);
    oView.View(iID,'');
  //  oView.Show;

    FreeAndNil (oView);


    Next;
  end;


end;



begin
  TdmTest_LinkFreqPlan.Init;
end.
