program test_db_check;

uses
  Forms,
  Unit7 in 'Unit7.pas' {Form7},
  dm_DB_check in '..\..\src\DataModules\dm_DB_check.pas' {dmDB_check: TDataModule},
  dm_Main in '..\..\src\DataModules\dm_Main.pas' {dmMain: TDataModule},
  u_ini_install_db in '..\..\..\RPLS_DB tools\shared\u_ini_install_db.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm7, Form7);
  Application.CreateForm(TdmDB_check, dmDB_check);
  Application.CreateForm(TdmMain, dmMain);
  Application.Run;
end.
