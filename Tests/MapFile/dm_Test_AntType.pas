unit dm_Test_AntType;

interface

uses
  SysUtils, Classes, ADODB, DB, Forms;

type
  TdmTest_AntType = class(TDataModule)
    qry_Source: TADOQuery;
    tb_Projects: TADOTable;
  private
    { Private declarations }
  public
    class procedure Init;

  end;

var
  dmTest_AntType: TdmTest_AntType;

implementation

{$R *.dfm}


//--------------------------------------------------------------------
class procedure TdmTest_AntType.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmTest_AntType) then
    Application.CreateForm(TdmTest_AntType, dmTest_AntType);

end;



end.
