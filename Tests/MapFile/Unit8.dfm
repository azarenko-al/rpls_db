object Form8: TForm8
  Left = 672
  Top = 403
  Width = 1006
  Height = 551
  Caption = 'Form8'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 16
    Top = 8
    Width = 465
    Height = 265
    DataSource = ds_ClientDataSet_items
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBGrid2: TDBGrid
    Left = 512
    Top = 8
    Width = 465
    Height = 137
    DataSource = DataSource1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object Button1: TButton
    Left = 576
    Top = 376
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 2
    OnClick = Button1Click
  end
  object ClientDataSet_groups: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'id'
        DataType = ftInteger
      end
      item
        Name = 'caption'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'link_id'
        DataType = ftInteger
      end
      item
        Name = 'name'
        DataType = ftString
        Size = 50
      end>
    IndexDefs = <>
    PacketRecords = 0
    Params = <>
    StoreDefs = True
    Left = 184
    Top = 376
    Data = {
      6A0000009619E0BD0100000018000000040000000000030000006A0002696404
      000100000000000763617074696F6E0100490000000100055749445448020002
      006400076C696E6B5F69640400010000000000046E616D650100490000000100
      0557494454480200020032000000}
    object ClientDataSet_groupsid: TAutoIncField
      AutoGenerateValue = arAutoInc
      FieldName = 'id'
    end
    object StringField6: TStringField
      FieldName = 'name'
      Size = 50
    end
  end
  object ds_ClientDataSet_items: TDataSource
    DataSet = ClientDataSet_groups
    Left = 184
    Top = 440
  end
  object RxMemoryData1: TRxMemoryData
    Active = True
    Filtered = True
    FieldDefs = <
      item
        Name = 'id'
        DataType = ftAutoInc
      end
      item
        Name = 'dddddddddd'
        DataType = ftString
        Size = 20
      end>
    Left = 456
    Top = 360
    object RxMemoryData1id: TAutoIncField
      FieldName = 'id'
    end
    object RxMemoryData1dddddddddd: TStringField
      FieldName = 'dddddddddd'
    end
  end
  object DataSource1: TDataSource
    DataSet = dxMemData1
    Left = 456
    Top = 424
  end
  object dxMemData1: TdxMemData
    Active = True
    Indexes = <>
    Persistent.Data = {
      5665728FC2F5285C8FFE3F02000000040000000C000300696400140000000100
      0B006464646464646464646400}
    SortOptions = []
    Left = 640
    Top = 280
    object dxMemData1id: TAutoIncField
      DisplayWidth = 10
      FieldName = 'id'
    end
    object dxMemData1dddddddddd: TStringField
      DisplayWidth = 20
      FieldName = 'dddddddddd'
    end
  end
end
