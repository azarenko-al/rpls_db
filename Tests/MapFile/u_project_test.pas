{******************************************************************************}
{                                                                              }
{          ����:                                                          }
{             �����:                                                          }
{            ������:                                                          }
{            �����:                                                          }
{           �����������:                                                          }
{                                                                              }
{******************************************************************************}

unit u_Project_test;

interface

uses
  Windows,  Dialogs, SysUtils, Classes, TestFramework, TestExtensions,

  dm_Main,

  dm_Project,

  u_const,

  u_vars,

  u_reg,

  u_db_manager,

  I_db_login

  ;

type
  TTest_Project = class(TTestCase)
  protected
    procedure SetUp; override;
    procedure TearDown; override;

  private
    FProjectID: Integer;

  published
    procedure Add;
  end;

implementation

uses StrUtils;

//---------------------------------------------------------------------------
procedure TTest_Project.Setup;
//---------------------------------------------------------------------------
var
  b: Boolean;
  rec : TdbLoginRec;
begin
  FillChar(rec, SizeOf(rec), 0);

  rec.Server:='server';
  rec.DataBase:='onega_link';
  rec.Login:='sa';
  rec.IsUseWinAuth := False;

  b:=dmMain.OpenDB_rec(rec);

  Check(b);


end;


procedure TTest_Project.TearDown;

begin
 // dmMain.c



end;

//---------------------------------------------------------------------------
procedure TTest_Project.Add;
//---------------------------------------------------------------------------
var
  b: Boolean;
  iProject: Integer;
  rec: TdmProjectAddRec;

begin
  FillChar(rec, SizeOf(rec), 0);

  Randomize;
  rec.Name:='project' + IntToStr( Random(100000));

  rec.MapFileDir1:='f:\gis';
  rec.MapFileDir2:='f:\gis';
  rec.MapFileDir3:='f:\gis';
  rec.RelMatrixDir1:='f:\gis';
  rec.RelMatrixDir2:='f:\gis';
  rec.RelMatrixDir3:='f:\gis';

  iProject:=dmProject.Add(rec);

  Check(iProject>0);


  b:=dmProject.Del(iProject);
  Check(b);

end;


initialization
  TestFramework.RegisterTest(TTest_Project.Suite);

end.

