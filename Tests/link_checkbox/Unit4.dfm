object Form4: TForm4
  Left = 578
  Top = 342
  Width = 770
  Height = 515
  Caption = 'Form4'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 576
    Top = 40
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Chart1: TChart
    Left = 16
    Top = 176
    Width = 321
    Height = 294
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Foot.Visible = False
    Legend.Visible = False
    MarginBottom = 2
    MarginLeft = 0
    MarginRight = 0
    MarginTop = 2
    Title.AdjustFrame = False
    Title.Text.Strings = (
      'TChart')
    Title.Visible = False
    BottomAxis.Automatic = False
    BottomAxis.AutomaticMaximum = False
    BottomAxis.Axis.Width = 1
    BottomAxis.AxisValuesFormat = '#,##0.##'
    BottomAxis.ExactDateTime = False
    BottomAxis.Grid.Color = clSilver
    BottomAxis.Grid.Style = psDashDotDot
    BottomAxis.Grid.SmallDots = True
    BottomAxis.Increment = 3.000000000000000000
    BottomAxis.LabelsSeparation = 5
    BottomAxis.LabelStyle = talValue
    BottomAxis.Maximum = 25.000000000000000000
    BottomAxis.MinorTickCount = 4
    LeftAxis.Axis.Color = clGray
    LeftAxis.Axis.Width = 1
    LeftAxis.ExactDateTime = False
    LeftAxis.Grid.Color = clSilver
    LeftAxis.Grid.Style = psDashDot
    LeftAxis.Grid.SmallDots = True
    LeftAxis.Grid.Visible = False
    LeftAxis.Increment = 10.000000000000000000
    LeftAxis.LabelsOnAxis = False
    LeftAxis.LabelsSeparation = 0
    LeftAxis.LabelsSize = 25
    LeftAxis.MinorTickCount = 4
    LeftAxis.TickInnerLength = 1
    LeftAxis.TickLength = 2
    LeftAxis.Ticks.Color = 4194368
    LeftAxis.Title.Angle = 0
    LeftAxis.TitleSize = 22
    RightAxis.Automatic = False
    RightAxis.AutomaticMaximum = False
    RightAxis.Axis.Color = 4194368
    RightAxis.Axis.Width = 1
    RightAxis.Grid.Visible = False
    RightAxis.Maximum = 992.500000000000000000
    RightAxis.MinorTickCount = 4
    TopAxis.Axis.Width = 1
    TopAxis.MinorTickLength = 0
    View3D = False
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 1
    AutoSize = True
    ColorPaletteIndex = 13
    object ser_Earth_bottom: TFastLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      SeriesColor = clGray
      LinePen.Color = clGray
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
      Left = 4
    end
    object ser_Earth_area_Top: TAreaSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      SeriesColor = clWhite
      VertAxis = aBothVertAxis
      AreaBrush = bsHorizontal
      AreaChartBrush.Color = clWhite
      AreaChartBrush.Style = bsHorizontal
      AreaChartBrush.Image.Data = {
        07544269746D61707E000000424D7E000000000000003E000000280000001000
        0000100000000100010000000000400000000000000000000000020000000200
        000000000000FFFFFF00EEEE0000FFFF0000BBBB0000FFFF0000EEEE0000FFFF
        0000BBBB0000FFFF0000EEEE0000FFFF0000BBBB0000FFFF0000EEEE0000FFFF
        0000BBBB0000FFFF0000}
      AreaLinesPen.Color = clGray
      AreaLinesPen.Visible = False
      DrawArea = True
      Pointer.InflateMargins = False
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
      Left = 4
    end
    object ser_Earth_top: TFastLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      SeriesColor = clGray
      LinePen.Color = clGray
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
      Left = 4
    end
    object ser_Earth_area_bottom: TAreaSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      SeriesColor = clWhite
      ShowInLegend = False
      AreaChartBrush.Color = clBlack
      AreaLinesPen.Visible = False
      DrawArea = True
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
      Left = 4
    end
    object Series_Earth_lines: TFastLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      IgnoreNulls = False
      LinePen.Color = clRed
      LinePen.Style = psDot
      XValues.Name = 'X'
      XValues.Order = loNone
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object Series_Clu: TAreaSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      AreaLinesPen.Visible = False
      Dark3D = False
      DrawArea = True
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loNone
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object ser_Tower: TLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      SeriesColor = clBlue
      LinePen.Width = 3
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loNone
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object ser_Sites: TPointSeries
      Marks.Arrow.SmallDots = True
      Marks.Arrow.Visible = False
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.SmallDots = True
      Marks.Callout.Arrow.Visible = False
      Marks.Callout.Length = 8
      Marks.BackColor = clWhite
      Marks.Brush.Color = clBlack
      Marks.Color = clWhite
      Marks.Shadow.HorizSize = 0
      Marks.Shadow.Transparency = 5
      Marks.Shadow.VertSize = 0
      Marks.Visible = True
      SeriesColor = clBlue
      ClickableLine = False
      Pointer.Brush.Color = clRed
      Pointer.HorizSize = 5
      Pointer.InflateMargins = True
      Pointer.Style = psCircle
      Pointer.VertSize = 5
      Pointer.Visible = True
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object ser_Direct: TLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      SeriesColor = clBlack
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object ser_Frenel: TLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loNone
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object ser_Refl: TLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      TreatNulls = tnIgnore
      XValues.Name = 'X'
      XValues.Order = loNone
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object ChartTool1: TMagnifyTool
      Active = False
      Callout.Brush.Color = clBlack
      Callout.Arrow.Visible = False
      Shape.Brush.Color = clWhite
      Shape.Brush.Style = bsClear
      Shape.Left = 10
      Shape.Shadow.HorizSize = 0
      Shape.Shadow.VertSize = 0
      Shape.Top = 10
      Shape.AutoSize = False
      Shape.Cursor = crHandPoint
      Height = 50
      Width = 50
      Percent = 50.000000000000000000
    end
  end
  object DBCheckBox_Is_calc_with_passive: TDBCheckBox
    Left = 273
    Top = 2
    Width = 168
    Height = 22
    Caption = #1059#1095#1105#1090' '#1087#1072#1089#1089#1080#1074#1085#1099#1093' '#1101#1083#1077#1084#1077#1085#1090#1086#1074
    DataField = 'is_UsePassiveElements'
    DataSource = DataSource1
    TabOrder = 2
    ValueChecked = 'True'
    ValueUnchecked = 'False'
    OnClick = DBCheckBox_Is_calc_rainsClick
  end
  object DBCheckBox_Is_calc_rains: TDBCheckBox
    Left = 0
    Top = 2
    Width = 273
    Height = 22
    Hint = #1059#1095#1105#1090' '#1074#1083#1080#1103#1085#1080#1103' '#1076#1086#1087'. '#1086#1089#1072#1076#1082#1086#1074' '#1085#1072' '#1079#1072#1087#1072#1089' '#1042#1063' '#1091#1088#1086#1074#1085#1103
    Caption = #1059#1095#1105#1090' '#1074#1083#1080#1103#1085#1080#1103' '#1076#1086#1087'. '#1086#1089#1072#1076#1082#1086#1074' '#1085#1072' '#1079#1072#1087#1072#1089' '#1042#1063' '#1091#1088#1086#1074#1085#1103
    DataField = 'Is_CalcWithAdditionalRain'
    DataSource = DataSource1
    TabOrder = 3
    ValueChecked = 'True'
    ValueUnchecked = 'False'
    OnClick = DBCheckBox_Is_calc_rainsClick
  end
  object DBGrid1: TDBGrid
    Left = 16
    Top = 48
    Width = 320
    Height = 120
    DataSource = DataSource1
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DataSource1: TDataSource
    DataSet = qry_Link
    Left = 616
    Top = 120
  end
  object qry_Link: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select top 1  *   from  link')
    Left = 616
    Top = 184
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=server_onega_link'
    LoginPrompt = False
    Left = 504
    Top = 72
  end
end
