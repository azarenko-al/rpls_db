unit Unit4;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TeEngine, Series, ExtCtrls, TeeProcs, Chart, TeeTools,
  TeeMagnifyTool, Grids, DBGrids, DB, ADODB, DBCtrls;

type
  TForm4 = class(TForm)
    Button1: TButton;
    Chart1: TChart;
    ser_Earth_bottom: TFastLineSeries;
    ser_Earth_area_Top: TAreaSeries;
    ser_Earth_top: TFastLineSeries;
    ser_Earth_area_bottom: TAreaSeries;
    Series_Earth_lines: TFastLineSeries;
    Series_Clu: TAreaSeries;
    ser_Tower: TLineSeries;
    ser_Sites: TPointSeries;
    ser_Direct: TLineSeries;
    ser_Frenel: TLineSeries;
    ser_Refl: TLineSeries;
    ChartTool1: TMagnifyTool;
    DataSource1: TDataSource;
    qry_Link: TADOQuery;
    DBCheckBox_Is_calc_with_passive: TDBCheckBox;
    DBCheckBox_Is_calc_rains: TDBCheckBox;
    ADOConnection1: TADOConnection;
    DBGrid1: TDBGrid;
    procedure Button1Click(Sender: TObject);
    procedure DBCheckBox_Is_calc_rainsClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form4: TForm4;

implementation

{$R *.dfm}

procedure TForm4.Button1Click(Sender: TObject);
begin
//  Series1.YValues[0]:=-20;
//  Series1.YValues[1]:=-30;
//  Series1.YValues[2]:=-56;

end;

procedure TForm4.DBCheckBox_Is_calc_rainsClick(Sender: TObject);
begin
  ShowMessage('Click');
end;

procedure TForm4.FormCreate(Sender: TObject);
begin
//  Series1.OffsetPercent:=0;
//  Series1.AddY(-60);
//  Series1.AddY(-30);
//  Series1.AddY(-20);

end;

end.

