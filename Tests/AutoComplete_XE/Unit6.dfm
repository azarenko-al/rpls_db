object Form6: TForm6
  Left = 793
  Top = 447
  Caption = 'Form6'
  ClientHeight = 434
  ClientWidth = 533
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 8
    Top = 72
    Width = 345
    Height = 145
    DataSource = ds_Recommendation
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object ComboBox1: TComboBox
    Left = 8
    Top = 8
    Width = 345
    Height = 21
    AutoComplete = False
    DropDownCount = 20
    TabOrder = 1
    Text = 'ComboBox111111'
    OnChange = ComboBox1Change
  end
  object Button1: TButton
    Left = 416
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 416
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 3
    OnClick = Button2Click
  end
  object ComboBox2: TComboBox
    Left = 8
    Top = 376
    Width = 345
    Height = 21
    DropDownCount = 20
    TabOrder = 4
    Text = 'ComboBox111111'
    OnChange = ComboBox1Change
    OnCloseUp = ComboBox2CloseUp
  end
  object Button3: TButton
    Left = 416
    Top = 159
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 5
    OnClick = Button3Click
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredValues = <>
    Left = 98
    Top = 272
  end
  object ds_Recommendation: TDataSource
    DataSet = ADOStoredProc_Lookup
    Left = 340
    Top = 315
  end
  object ADOStoredProc_Lookup: TADOStoredProc
    Active = True
    AutoCalcFields = False
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'link.sp_property_search_top_10_SEL'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@project_id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 65
      end
      item
        Name = '@SEARCH_TEXT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = #1083#1072#1079#1072#1088#1077
      end>
    Left = 336
    Top = 272
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;User ID=sa;Initial Catalog=megafon_onega_link_2021_06_0' +
      '2;Data Source=ALEX2;Use Procedure for Prepare=1;Auto Translate=T' +
      'rue;Packet Size=4096;Workstation ID=ALEX;Use Encryption for Data' +
      '=False;Tag with column collation when possible=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 218
    Top = 264
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 500
    OnTimer = Timer1Timer
    Left = 448
    Top = 248
  end
end
