unit u_ComboBox_AutoComplete_v1;


interface
uses
  Windows, db, SysUtils, Classes, Messages,  Forms, Controls, StdCtrls;


type
  TOnSelectedEvent = procedure (aID: integer) of object;
//  TOnItemsUpdateEvent = procedure (aList: TStrings) of object;

  TComboBoxAutoComplete_v1 = class(TComboBox)
  private
//    FOnItemsUpdate: TNotifyEvent;
    FOnSelected: TOnSelectedEvent;
    FOnUpdateItems: TNotifyEvent;

//    FStoredItems: TStringList;
//    procedure FilterItems_old;
//    procedure StoredItemsChange(Sender: TObject);
//    procedure SetStoredItems(const Value: TStringList);
    procedure CNCommand(var AMessage: TWMCommand); message CN_COMMAND;
    procedure DoOnCloseUp(Sender: TObject);
    procedure DropDown;
//    procedure DoOnKeyPress(Sender: TObject; var Key: Char);
    procedure FilterItems;
    procedure Select;
  public
    constructor Create(AOwner: TComponent); override;
//    destructor Destroy; override;

    procedure AssignComboBox(aComboBox: TComboBox);
    function GetID: Integer;
    procedure LoadItemsFromDataset(aDataset: TDataset);
//    procedure SetTextByID(aID: Integer);

//    property StoredItems: TStringList read FStoredItems write SetStoredItems;

    property OnSelected: TOnSelectedEvent read FOnSelected write FOnSelected;
    property OnUpdateItems: TNotifyEvent read FOnUpdateItems write FOnUpdateItems;

  end;


implementation


//------------------------------------------------------------------------
constructor TComboBoxAutoComplete_v1.Create(AOwner: TComponent);
//------------------------------------------------------------------------
begin
  inherited;
 // Parent:=AOwner;

  AutoComplete:=False;
  AutoDropDown:=False;

//  FStoredItems := TStringList.Create;

 // FStoredItems.OnChange := StoredItemsChange;

//  OnKeyPress:=DoOnKeyPress;
  OnCloseUp:=DoOnCloseUp;

  Sorted:=True;

  DropDownCount := 24;

  Left := 10;
  Top := 10;

//  Text := 'Br';

  {
 

  // here's how to fill the StoredItems
  StoredItems.BeginUpdate;
  try
    StoredItems.Add('Mr John Brown');
    StoredItems.Add('Mrs Amanda Brown');
    StoredItems.Add('Mr Brian Jones');
    StoredItems.Add('Mrs Samantha Smith');
  finally
    StoredItems.EndUpdate;
  end;
  }

end;


{
destructor TComboBoxAutoComplete_v1.Destroy;
begin
//  FStoredItems.Free;
//  inherited;
end;
 }


//------------------------------------------------------------------------
procedure TComboBoxAutoComplete_v1.DoOnCloseUp(Sender: TObject);
//------------------------------------------------------------------------
var
  iID: Integer;
begin
  if ItemIndex<0 then
    exit;


  iID:= Integer( Items.Objects [ItemIndex] );

  if Assigned(FOnSelected) then
      FOnSelected( iID);

//  ShowMessage( IntToStr( ItemIndex )  );

//  Select();

//    if Assigned(FOnSelected) then
//      FOnSelected(self)

 //  ShowMessage('ComboBox1CloseUp  ');
end;



//------------------------------------------------------------------------
procedure TComboBoxAutoComplete_v1.Select;
//------------------------------------------------------------------------
begin
//   Select();

  if Assigned(FOnSelected) then
    FOnSelected(GetID());

 //  ShowMessage('ComboBox1CloseUp  ');
end;


{
procedure TComboBoxAutoComplete_v1.DoOnKeyPress(Sender: TObject; var Key: Char);
begin
  if Key=Chr(VK_RETURN)  then
    Select();
end;
 }

procedure TComboBoxAutoComplete_v1.DropDown;
begin
//  SendMessage(Handle, CB_SHOWDROPDOWN, Integer(True), 0);

  Perform(CB_SHOWDROPDOWN, integer(true), 0);
end;


procedure TComboBoxAutoComplete_v1.CNCommand(var AMessage: TWMCommand);
begin
  // we have to process everything from our ancestor
  inherited;
  // if we received the CBN_EDITUPDATE notification
  if AMessage.NotifyCode = CBN_EDITUPDATE then
    // fill the items with the matches
    FilterItems;
end;


//------------------------------------------------------------------------
procedure TComboBoxAutoComplete_v1.FilterItems;
//------------------------------------------------------------------------
var
//  I: Integer;

  Selection: record
    StartPos, EndPos: Integer;
  end;

//  sText: string;
begin

//  Screen.Cursor := crNone;
//  Screen.Cursor := crDefault;


  // store the current combo edit selection
/////////////
 SendMessage(Handle, CB_GETEDITSEL, WPARAM(@Selection.StartPos), LPARAM(@Selection.EndPos));

// sText:= '';

//Exit;

//  LockWindowUpdate(Handle);



  // begin with the items update
//  Items.BeginUpdate;
  try
    if Assigned(FOnUpdateItems) then
      FOnUpdateItems (Self);
{
    // if the combo edit is not empty, then clear the items
    // and search through the FStoredItems
    if Text <> '' then begin
      // clear all items
      Items.Clear;
      // iterate through all of them
      for I := 0 to FStoredItems.Count - 1 do begin
        // check if the current one contains the text in edit
//      if ContainsText(FStoredItems[I], Text) then

//        if Pos(   Text, FStoredItems[I])>0 then begin
        if Pos( AnsiLowerCase(Text), AnsiLowerCase(FStoredItems[I]))>0 then
        begin
          // and if so, then add it to the items
          Items.Add(FStoredItems[I]);
        end;
      end;
    end else begin
      // else the combo edit is empty
      // so then we'll use all what we have in the FStoredItems
      Items.Assign(FStoredItems)
    end;
    }
  finally
    // finish the items update
//    Items.EndUpdate;
  end;

 // Exit;


  // and restore the last combo edit selection
//  sText := Text;
//  SendMessage(Handle, CB_SHOWDROPDOWN, Integer(True), 0);

{
  if (Items<>nil) and (Items.Count>0) then begin
    ItemIndex := 0;
  end else begin
    ItemIndex := -1;
  end;

  }

//  Text := sText;

//  LockWindowUpdate(0);

////////////

  SendMessage(Handle, CB_SETEDITSEL, 0, MakeLParam(Selection.StartPos, Selection.EndPos));

  POstMessage (handle, CB_SHOWDROPDOWN, integer(true), 0);


//    if FComboBoxAutoComplete.Items.Count>1 then
//    FComboBoxAutoComplete.DropDown;

end;



// ---------------------------------------------------------------
procedure TComboBoxAutoComplete_v1.AssignComboBox(aComboBox: TComboBox);
// ---------------------------------------------------------------
begin
  aComboBox.Visible:=false;

  Left:= aComboBox.Left;
  Top:= aComboBox.Top;
  Width:= aComboBox.Width;

  Anchors:= aComboBox.Anchors;

  DropDownCount:= aComboBox.DropDownCount;
end;


//------------------------------------------------------------------------
function TComboBoxAutoComplete_v1.GetID: Integer;
//------------------------------------------------------------------------
var
  i: Integer;
  s: string;
begin
  i:=Items.IndexOf(Text);

  if i>=0 then
    Result:= Integer( Items.Objects[i] )
  else
    Result := -1;
end;


procedure TComboBoxAutoComplete_v1.LoadItemsFromDataset(aDataset: TDataset);
var
  iID: Integer;
  sName: string;
begin
  LockWindowUpdate(Handle);

  Items.BeginUpdate;
  Items.Clear;

//  FComboBoxAutoComplete.

//  ADOStoredProc_Lookup.DisableControls;
  aDataset.First;

  with aDataset do
    while not EOF do
  begin
    iID  := FieldByName('ID').AsInteger;
    sName:= FieldByName('NAME').AsString;

    Items.AddObject(sName, Pointer(iID));
//    FComboBoxAutoComplete.StoredItems.AddObject(sName, Pointer(iID));

    Next;
  end;

//  SelStart := Length(Text);

  Items.EndUpdate;

  LockWindowUpdate(0);

//  ADOStoredProc_Lookup.EnableControls;

end;


{
//------------------------------------------------------------------------
procedure TComboBoxAutoComplete_v1.SetTextByID(aID: Integer);
//------------------------------------------------------------------------
var
  i: Integer;
begin
  i:=FStoredItems.IndexOfObject(Pointer(aID));

  if i>=0 then
    Text:=FStoredItems[i]
  else
    Text := '';
end;
 }


 {
procedure TComboBoxAutoComplete_v1.SetStoredItems(const Value: TStringList);
begin
  if Assigned(FStoredItems) then
    FStoredItems.Assign(Value)
  else
    FStoredItems := Value;
end;
  }

end.

