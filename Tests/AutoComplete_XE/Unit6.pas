unit Unit6;

interface

uses
  u_ComboBox_AutoComplete_v1,
  u_db,

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids,
  Data.DB, Data.Win.ADODB, RxPlacemnt, Vcl.ExtCtrls;

type
  TForm6 = class(TForm)
    FormStorage1: TFormStorage;
    ds_Recommendation: TDataSource;
    ADOStoredProc_Lookup: TADOStoredProc;
    ADOConnection1: TADOConnection;
    DBGrid1: TDBGrid;
    ComboBox1: TComboBox;
    Button1: TButton;
    Button2: TButton;
    ComboBox2: TComboBox;
    Timer1: TTimer;
    Button3: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure ComboBox2CloseUp(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private

    FComboBoxAutoComplete : TComboBoxAutoComplete_v1;
//    procedure ChangeText;
    procedure DoOnItemsUpdateEvent(aList: TStrings);
    procedure Select_property(aPropertyID: integer);

    procedure DoOnUpdateItems(Sender: TObject);
//    procedure Update_Items(aStrings: TStrings);

    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form6: TForm6;

implementation

{$R *.dfm}


procedure TForm6.FormCreate(Sender: TObject);
begin
  FComboBoxAutoComplete := TComboBoxAutoComplete_v1.Create(Self);
  FComboBoxAutoComplete.Parent:=Self;
  FComboBoxAutoComplete.AssignComboBox (ComboBox1);

//  FComboBoxAutoComplete.OnSelected:=Select_property;
  FComboBoxAutoComplete.OnChange:= ComboBox1Change;
  FComboBoxAutoComplete.OnCloseUp:= ComboBox2CloseUp;

  FComboBoxAutoComplete.OnUpdateItems:= DoOnUpdateItems;

  db_StoredProc_Open(ADOStoredProc_Lookup, 'link.sp_property_search_top_10_SEL',
  [
    'project_id', 65,
    'SEARCH_text', ''

  ]);

  FComboBoxAutoComplete.LoadItemsFromDataset(ADOStoredProc_Lookup);

//  Update_Items (FComboBoxAutoComplete.Items);
 // Update_Items (FComboBoxAutoComplete.StoredItems);

end;

procedure TForm6.Button1Click(Sender: TObject);
//var
//  sText: string;
begin
//  ChangeText
end;

procedure TForm6.Button2Click(Sender: TObject);
begin
//  FComboBoxAutoComplete.DropDown;
end;

procedure TForm6.Button3Click(Sender: TObject);
begin
//  FComboBoxAutoComplete.Focused:=true;

//  FComboBoxAutoComplete.SelLength := 0;
//  FComboBoxAutoComplete.setfocus;
//  FComboBoxAutoComplete.SelStart := Length(FComboBoxAutoComplete.Text);

//  FComboBoxAutoComplete.Perform( EM_SETSEL, Length(FComboBoxAutoComplete.Text), Length(FComboBoxAutoComplete.Text));

//  PostMessage(FComboBoxAutoComplete.Handle, WM_CHAR, VK_END, 0);
end;



procedure TForm6.ComboBox1Change(Sender: TObject);
begin
 // Timer1.Enabled:=true;

//  ChangeText;
end;

procedure TForm6.ComboBox2CloseUp(Sender: TObject);
begin
   ShowMessage('ComboBox2CloseUp');
end;

procedure TForm6.DoOnItemsUpdateEvent(aList: TStrings);
var
  sText: string;
begin


end;



{
procedure TForm6.ChangeText;
var
  sText: string;
begin
  sText:=Trim(FComboBoxAutoComplete.Text);

  db_StoredProc_Open(ADOStoredProc_Lookup, 'link.sp_property_search_top_10_SEL',
  [
    'project_id', 65,
    'SEARCH_text', sText

  ]);

  FComboBoxAutoComplete.LoadItemsFromDataset(ADOStoredProc_Lookup);


  if FComboBoxAutoComplete.Items.Count>1 then
    FComboBoxAutoComplete.DropDown;

end;
}

procedure TForm6.DoOnUpdateItems(Sender: TObject);
var
  sText: string;
begin
  sText:=Trim(FComboBoxAutoComplete.Text);

  db_StoredProc_Open(ADOStoredProc_Lookup, 'link.sp_property_search_top_10_SEL',
  [
    'project_id', 65,
    'SEARCH_text', sText

  ]);

  FComboBoxAutoComplete.LoadItemsFromDataset(ADOStoredProc_Lookup);

end;




// ---------------------------------------------------------------
procedure TForm6.Select_property(aPropertyID: integer);
// ---------------------------------------------------------------
begin

//  Debug ('Tframe_Link_add.Select_property');
//
//  PropertyID:=aPropertyID;
//
////  Update_Items();
//
//  Open_PropertyLinkEnds (aPropertyID);//, mem_LinkEnds);
//
////  UpdatePanel();
////  StatusBar1.Panels[0].Text:= 'Property ID: '+ IntToStr(PropertyID);
//
//  if Assigned(FOnChange) then
//   FOnChange (Self);

  //
  ShowMessage('Select_property : '+ IntToStr(aPropertyID));
end;


procedure TForm6.Timer1Timer(Sender: TObject);
begin
//  Timer1.Enabled:=false;
  //ChangeText;
end;


end.

  {

//  FComboBoxAutoComplete := TComboBoxAutoComplete.Create(ComboBox1);

//  FComboBoxAutoComplete.Parent:=GroupBox_Prop;

//  FComboBoxAutoComplete.AssignComboBox (ComboBox1);

//  FComboBoxAutoComplete.OnSelected:=Select_property;
//  ComboBox1KeyPress;

//
//
//procedure TForm5.ComboBox1KeyPress(Sender: TObject; var Key: Char);
//begin
//  if Key= Chr(VK_RETURN)  then
//    ShowMessage('');
//
//end;




   db_OpenStoredProc(ADOStoredProc_Lookup, 'link.sp_property_search_top_10_SEL',
  [
    'project_id', dmMain.projectID,
    'SEARCH_text', ''

  ]);




  // ---------------------------------------------------------------
procedure Tframe_Link_add.Update_PROPERTY;
// ---------------------------------------------------------------
begin
//  LockWindowUpdate(FComboBoxAutoComplete.Handle);
{
  dmLink_add_data.Update_PROPERTY(FComboBoxAutoComplete.Items);

  FComboBoxAutoComplete.StoredItems.Assign(FComboBoxAutoComplete.Items);

  LockWindowUpdate(0);
 }
{

  db_OpenQuery (qry_Prop, Format(SQL_SELECT, [dmMain.ProjectID]));


 // comboBox1.Beg

  LockWindowUpdate(comboBox1.Handle);



  LockWindowUpdate(0);

}
{
  LockWindowUpdate(FComboBoxAutoComplete.Handle);

//  FComboBoxAutoComplete.BeginUpdate;
  FComboBoxAutoComplete.StoredItems.Assign(FComboBoxAutoComplete.Items);
//  FComboBoxAutoComplete.EndUpdate;

  LockWindowUpdate(0);
}
end;

