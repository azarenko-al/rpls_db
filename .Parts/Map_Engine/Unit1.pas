unit Unit1;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, ActiveX, Classes, ComObj, Map_Engine_TLB, StdVcl;

type
  TMapDesktopX = class(TTypedComObject, IMapDesktopX)
  protected
  end;

implementation

uses ComServ;

initialization
  TTypedComObjectFactory.Create(ComServer, TMapDesktopX, Class_MapDesktopX,
    ciMultiInstance, tmApartment);
end.
