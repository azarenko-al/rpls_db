unit dm_WEB_search;

interface

uses

  //Winapi.
  Windows, 
  SysUtils,
  Variants,
  WinInet, IniFiles, IdURI,

//  u_web,
//  u_xml_new,
 
  u_db,

  u_files,
  
  u_func, //_arr,
  u_func_arr,
 
 
  //Xml.Win.
  msxmldom, //Xml.
  XMLDoc, //Xml.
  xmldom, //Xml.
  XMLIntf, //Data.DB, dxmdaset,
  Classes, DB,  dxmdaset;

type
  TdmWEB_search = class(TDataModule)
    XMLDocument1: TXMLDocument;
    mem_Data: TdxMemData;
    mem_Dataposition: TStringField;
    mem_Datalat: TFloatField;
    mem_Datalon: TFloatField;
    ds_Data: TDataSource;
    procedure DataModuleCreate(Sender: TObject);
  private
    FIni_params: record
      Yandex : string;
      OSM    : string;
    end;  
  
    procedure LoadFromIniFile;

 
  public
    procedure Search_OSM(aAddress: string);
    procedure Search_Yandex(aAddress: string);
 
  end;

var
  dmWEB_search: TdmWEB_search;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

          

function xml_GetNodeByPath(aNode: IXMLNode; aPathArr: array of string): IXMLNode; forward;
function DownloadFile(aURL, aFileName: string): Boolean; forward;
 

                   
procedure TdmWEB_search.DataModuleCreate(Sender: TObject);
begin
  LoadFromIniFile();

end;

// ---------------------------------------------------------------
procedure TdmWEB_search.LoadFromIniFile;
// ---------------------------------------------------------------
var
  oIni: TIniFile;
  sFile: string;
begin
  inherited;


  {$IFDEF dll}

  sFile := ExtractFilePath( System.SysUtils.GetModuleName(HInstance) ) + 'Geocoding.ini';
  
 // sFile:=ExtractFilePath(Application.ExeName) + 'snmp.ini';
 
  {$ELSE}      
  sFile:='C:\ONEGA\RPLS_DB\bin\dll\Geocoding.ini';
  {$ENDIF}                            
                    
//  CodeSite.Send(sFile);

  Assert ( FileExists(sFile) , sFile);
                                         
  
  oIni:=TIniFile.Create (sFile);

  FIni_params.Yandex:=oIni.ReadString ('main','Yandex','');
  FIni_params.OSM   :=oIni.ReadString ('main','OSM','');

  
  FreeAndNil (oIni);
  
end;

// ---------------------------------------------------------------
procedure TdmWEB_search.Search_Yandex(aAddress: string);
// ---------------------------------------------------------------
                          
var
  i: Integer;
  s: string;
  vAddressNode: IXMLNode;
  vGeoObjectCollection: IXMLNode;
  vNode: IXMLNode;
  vPointNode: IXMLNode;

  arrStr: TStrArray;
  eLat_WGS: Double;
  eLon_WGS: Double;
  sFile: string;
  
begin
  sFile:=GetTempXmlFileName();

      
  {$IFDEF debug}
  sFile:='d:\test1.xml' ;
  {$ENDIF}
               
      
  mem_Data.Close;
  mem_Data.Open;
 

  DownloadFile (FIni_params.Yandex +  TIdURI.ParamsEncode (aAddress), sFile);

  XMLDocument1.LoadFromFile(sFile);

 //----------------------------------------
  
  i:=XMLDocument1.ChildNodes.Count;

  vNode:=XMLDocument1.ChildNodes.FindNode('ymaps');
  
  vGeoObjectCollection:=xml_GetNodeByPath (vNode, ['GeoObjectCollection' ]);

  if Assigned(vGeoObjectCollection) then
    for I := 0 to vGeoObjectCollection.ChildNodes.Count -1 do
      if vGeoObjectCollection.ChildNodes[i].LocalName = 'featureMember' then
      begin
        vAddressNode:=xml_GetNodeByPath (vGeoObjectCollection.ChildNodes[i], ['GeoObject','metaDataProperty','GeocoderMetaData ','AddressDetails','Country','AddressLine' ]);
        s:=vAddressNode.Text;

        vPointNode:=xml_GetNodeByPath (vGeoObjectCollection.ChildNodes[i], ['GeoObject','Point','pos' ]);
        s:=vPointNode.Text;


        arrStr:= StringToStrArray(s, ' ');

        if High(arrStr) >= 1 then
        begin
          eLat_WGS:= AsFloat(arrStr[1]);
          eLon_WGS:= AsFloat(arrStr[0]);


          db_AddRecord_(mem_Data, [
             'address', vAddressNode.Text,
             'lat', eLat_WGS,
             'lon', eLon_WGS
             ]);
          
        end;

      end;  
  
end;
            
/////////////////////////////////////////////////////////////////

// ---------------------------------------------------------------
procedure TdmWEB_search.Search_OSM(aAddress: string);
// ---------------------------------------------------------------

var
  i: Integer;
  sPath: WideString;

  vRoot: IXMLNode;
  vNode: IXMLNode;
  vPointNode: IXMLNode;

  eLat_WGS: Double;
  eLon_WGS: Double;
  s: string;
  sFile: string;
  
begin
  sFile:=GetTempXmlFileName();
      
  {$IFDEF debug}
  sFile:='d:\test.xml' ;
  {$ENDIF}

      
  mem_Data.Close;
  mem_Data.Open;
          

  DownloadFile ( FIni_params.OSM +  TIdURI.ParamsEncode ( aAddress), sFile);
 

  XMLDocument1.LoadFromFile(sFile); 

//  DeleteFile(sFile);

 //----------------------------------------
  
  i:=XMLDocument1.ChildNodes.Count;

//  vNode:=XMLDocument1.DocumentElement;
  
  s:=XMLDocument1.DocumentElement.LocalName;

  vRoot:=XMLDocument1.DocumentElement.ChildNodes.FindNode('searchresults');
  
  vRoot:=XMLDocument1.DocumentElement;

   if Assigned(vRoot) then
    for I := 0 to vRoot.ChildNodes.Count -1 do
      if vRoot.ChildNodes[i].LocalName = 'place' then
      begin
        vNode:=vRoot.ChildNodes[i];

        eLat_WGS:= AsFloat(vNode.Attributes['lat']);
        eLon_WGS:= AsFloat(vNode.Attributes['lon']);
        

        db_AddRecord_(mem_Data, [
             'address', vNode.Attributes['display_name'],
             'lat', eLat_WGS,
             'lon', eLon_WGS
             ]);
    end;  
  
end;



// ---------------------------------------------------------------
function DownloadFile(aURL, aFileName: string): Boolean;
// ---------------------------------------------------------------
const
  BufferSize = 1024;
var
  hSession, hURL: HInternet;
  Buffer: array[1..BufferSize] of Byte;
  BufferLen: DWORD;
  F: File;
begin
   Result := False;
   hSession := InternetOpen('', INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0) ;

   // Establish the secure connection
   InternetConnect (
     hSession,
     PChar(aURL),
     INTERNET_DEFAULT_HTTPS_PORT,
     nil, //.. PChar(''),
     nil, //PChar(''),
     INTERNET_SERVICE_HTTP,
     0,
     0
   );

  try
    hURL := InternetOpenURL(hSession, PChar(aURL), nil, 0, 0, 0) ;
    try
      AssignFile(f, aFileName);
      Rewrite(f,1);
      try
        repeat
          InternetReadFile(hURL, @Buffer, SizeOf(Buffer), BufferLen) ;
          BlockWrite(f, Buffer, BufferLen)
        until BufferLen = 0;
      finally
        CloseFile(f) ;
        Result := True;
      end;
    finally
      InternetCloseHandle(hURL)
    end
  finally
    InternetCloseHandle(hSession)
  end;

  Assert( FileExists(aFileName), aFileName);
end;





//--------------------------------------------------------
function Eq(aValue1, aValue2: string): boolean;
//--------------------------------------------------------
begin
  Result := AnsiCompareText(Trim(aValue1),Trim(aValue2))=0;
end;


//-----------------------------------------------------------
function xml_GetNodeByPath(aNode: IXMLNode; aPathArr: array of string):     IXMLNode;
//-----------------------------------------------------------
var i,j,iCOunt:integer;   
   vElement: IXMLNode;   strArr: array of string;
  sTag: string;
begin
  Result:=nil;

  if VarIsNull(aNode) then
    Exit;

//  iCOunt:=aNode.childNodes.Count;

 // if High(aPathArr)>=0 then

  for i:=0 to aNode.childNodes.Count-1 do
  begin
    vElement:=aNode.childNodes[i];

//    if not xml_IsElement(vElement) then
//      Continue;

    sTag:=vElement.LocalName;

    if sTag='' then Continue;

    if Eq(sTag ,aPathArr[0]) then
      if High(aPathArr)=0
       then begin 
         Result:=vElement; 
         Exit; 
       end
       else begin
         SetLength(strArr, High(aPathArr));
         for j:=0 to High(aPathArr)-1 do 
           strArr[j]:=aPathArr[j+1];

         Result:=xml_GetNodeByPath (vElement, strArr);
       end;
  end;


end;


end.
