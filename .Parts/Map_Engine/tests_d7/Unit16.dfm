object Form16: TForm16
  Left = 1125
  Top = 408
  Width = 776
  Height = 545
  Caption = 'Form16'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 189
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Yandex'
    TabOrder = 0
    OnClick = Button1Click
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 312
    Width = 768
    Height = 205
    Align = alBottom
    DataSource = dmWEB_search.ds_Data
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Address'
        Width = 528
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'lat'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'lon'
        Visible = True
      end>
  end
  object Button3: TButton
    Left = 280
    Top = 24
    Width = 75
    Height = 25
    Caption = 'OSM'
    TabOrder = 2
    OnClick = Button3Click
  end
  object Memo1: TMemo
    Left = 8
    Top = 120
    Width = 514
    Height = 137
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssBoth
    TabOrder = 3
  end
end
