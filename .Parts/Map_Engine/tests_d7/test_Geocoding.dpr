program test_Geocoding;

uses
  Vcl.Forms,
  Unit16 in 'Unit16.pas' {Form16},
  dm_WEB_search in 'dm_WEB_search.pas' {dmWEB_search: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmWEB_search, dmWEB_search);
  Application.CreateForm(TForm16, Form16);
  Application.Run;
end.
