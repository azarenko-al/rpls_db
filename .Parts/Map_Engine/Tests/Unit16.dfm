object Form16: TForm16
  Left = 982
  Top = 336
  Caption = 'Form16'
  ClientHeight = 610
  ClientWidth = 933
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 189
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Yandex'
    TabOrder = 0
    OnClick = Button1Click
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 312
    Width = 933
    Height = 149
    Align = alBottom
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Address'
        Width = 528
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'lat_WGS'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'lon_WGS'
        Visible = True
      end>
  end
  object Button3: TButton
    Left = 280
    Top = 24
    Width = 75
    Height = 25
    Caption = 'OSM'
    TabOrder = 2
    OnClick = Button3Click
  end
  object Memo1: TMemo
    Left = 8
    Top = 120
    Width = 529
    Height = 129
    ParentCustomHint = False
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssBoth
    TabOrder = 3
  end
  object DBGrid2: TDBGrid
    Left = 0
    Top = 461
    Width = 933
    Height = 149
    Align = alBottom
    DataSource = ds_Data
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Address'
        Width = 528
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'lat_WGS'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'lon_WGS'
        Visible = True
      end>
  end
  object Button2: TButton
    Left = 496
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Yandex'
    TabOrder = 5
    OnClick = Button2Click
  end
  object Button4: TButton
    Left = 587
    Top = 24
    Width = 75
    Height = 25
    Caption = 'OSM'
    TabOrder = 6
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 496
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Button5'
    TabOrder = 7
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 592
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Button6'
    TabOrder = 8
    OnClick = Button6Click
  end
  object Edit1: TEdit
    Left = 8
    Top = 66
    Width = 441
    Height = 21
    TabOrder = 9
    Text = #1088#1086#1089#1089#1080#1103' '#1089#1072#1085#1082#1090'-'#1087#1077#1090#1077#1088#1073#1091#1088#1075' '#1075#1088#1072#1078#1076#1072#1085#1089#1082#1080#1081' '#1087#1088#1086#1089#1087#1077#1082#1090' 122'#1082'1'
  end
  object Button7: TButton
    Left = 576
    Top = 118
    Width = 75
    Height = 25
    Caption = 'lat lon '
    TabOrder = 10
    OnClick = Button7Click
  end
  object Button8: TButton
    Left = 576
    Top = 192
    Width = 75
    Height = 25
    Caption = 'Button8'
    TabOrder = 11
    OnClick = Button8Click
  end
  object LabeledEdit1: TLabeledEdit
    Left = 792
    Top = 66
    Width = 121
    Height = 21
    EditLabel.Width = 61
    EditLabel.Height = 13
    EditLabel.Caption = 'LabeledEdit1'
    TabOrder = 12
  end
  object LabeledEdit2: TLabeledEdit
    Left = 792
    Top = 116
    Width = 121
    Height = 21
    EditLabel.Width = 61
    EditLabel.Height = 13
    EditLabel.Caption = 'LabeledEdit1'
    TabOrder = 13
  end
  object Button9: TButton
    Left = 792
    Top = 152
    Width = 75
    Height = 25
    Caption = 'Button9'
    TabOrder = 14
  end
  object mem_Data: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 213
    Top = 484
    object mem_Dataposition: TStringField
      DisplayLabel = #1040#1076#1088#1077#1089
      FieldName = 'Address'
      Size = 100
    end
    object mem_Datalat: TFloatField
      FieldName = 'lat_WGS'
    end
    object mem_Datalon: TFloatField
      FieldName = 'lon_WGS'
    end
  end
  object ds_Data: TDataSource
    DataSet = mem_Data
    Left = 209
    Top = 540
  end
  object FormStorage1: TFormStorage
    StoredProps.Strings = (
      'Edit1.Text')
    StoredValues = <>
    Left = 704
    Top = 112
  end
end
