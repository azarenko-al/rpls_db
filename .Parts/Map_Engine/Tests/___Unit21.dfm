object Form21: TForm21
  Left = 483
  Top = 486
  Caption = 'Form21'
  ClientHeight = 844
  ClientWidth = 1516
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button4: TButton
    Left = 264
    Top = 80
    Width = 153
    Height = 25
    Caption = 'Dlg_Attributes prop'
    TabOrder = 0
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 264
    Top = 120
    Width = 153
    Height = 25
    Caption = 'Dlg_Label_style'
    TabOrder = 1
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 264
    Top = 168
    Width = 153
    Height = 25
    Caption = 'Dlg_Style'
    TabOrder = 2
    OnClick = Button6Click
  end
  object GroupBox1: TGroupBox
    Left = 40
    Top = 46
    Width = 153
    Height = 19
    Caption = 'COM'
    TabOrder = 3
  end
  object Button1: TButton
    Left = 40
    Top = 80
    Width = 153
    Height = 25
    Caption = 'Tdlg_Layer_Attributes'
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 40
    Top = 120
    Width = 153
    Height = 25
    Caption = 'Dlg_Label_style'
    TabOrder = 5
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 40
    Top = 168
    Width = 153
    Height = 25
    Caption = 'Dlg_Style'
    TabOrder = 6
    OnClick = Button3Click
  end
  object Map1: TMap
    Left = 8
    Top = 440
    Width = 673
    Height = 197
    ParentColor = False
    TabOrder = 7
    ControlData = {
      2CA107008E4500005C140000010000000F0000FFFFFEFF0D470065006F004400
      69006300740069006F006E0061007200790000FFFEFF3745006D007000740079
      002000470065006F007300650074004E0061006D00650020007B003900410039
      00410043003200460034002D0038003300370035002D0034003400640031002D
      0042004300450042002D00340037003600410045003900380036004600310039
      0030007D00FFFEFF001E00FFFEFF000600010A000000000000FFFEFF00500001
      0100000001F4010000050000800C0000000014000000000002000E00E8030005
      00000100000000E9E7E2000000000000000001370000000000E9E7E200000000
      0000000352E30B918FCE119DE300AA004BB85101CC00009001DC7C0100054172
      69616C000352E30B918FCE119DE300AA004BB851010200009001A42C02000B4D
      61702053796D626F6C730000000000000001000100FFFFFF000200E9E7E20000
      0000000001000000010000FFFEFF3245006D0070007400790020005400690074
      006C00650020007B00300031004100390035003000340042002D004300450031
      0033002D0034003400310035002D0041003500410030002D0035003100440038
      00430032004600310035003200300034007D0000000000FFFFFF000100000000
      000000000000000000000000000000000000000352E30B918FCE119DE300AA00
      4BB85101CC00009001348C030005417269616C000352E30B918FCE119DE300AA
      004BB85101CC00009001348C030005417269616C000000000000000000000000
      00000000000000000000000000000000000000000000000000EEA94000000000
      000067400100000101000100FFFEFF3657003A005C00520050004C0053005F00
      440042005C002E00500061007200740073005C004D00610070005F0045006E00
      670069006E0065005C0044006100740061005C007E0070006D0070005F006300
      61006C0063005F0072006500670069006F006E002E00740061006200FFFEFF0A
      4C0061007900650072005F004E0061006D006500010100000000000000000000
      0000000000000000000000000000640000010200000000000000000000000000
      00000000000088C3400100000101000000000000FFFFFF000000000000000000
      000000000000000000000000000000000352E30B918FCE119DE300AA004BB851
      01CC00009001444201000D4D532053616E73205365726966000352E30B918FCE
      119DE300AA004BB85101CC00009001C0D401000D4D532053616E732053657269
      6600000000000000010001000000000000000000000000000000000000000000
      001801000001000000FFFFFFFF1C000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000002000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008076C0000000
      00008056C0000000000080764000000000008056400000000001000000180100
      0001000000FFFFFFFF030000000000000052B81E85EB91374066666666669E61
      C03333333333F353C00000000000000000666666666666D6BF48E17A14AE47E9
      BF295C8FC2F528CCBF000000000000000000000000020000006484BE2702174B
      4000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008076C000000000008056
      C0000000000080764000000000008056400000000001000000EE00000000003E
      405AC87A1100004A40F0DE51400A174B40000000000000000001000000000000
      00}
  end
  object Button7: TButton
    Left = 502
    Top = 337
    Width = 163
    Height = 25
    Caption = 'oThemeMaker.Apply_Theme'
    TabOrder = 8
    OnClick = Button7Click
  end
  object Button8: TButton
    Left = 16
    Top = 337
    Width = 257
    Height = 25
    Caption = 'CoMapEngineX.Create.Apply_Theme'
    TabOrder = 9
    OnClick = Button8Click
  end
  object Button9: TButton
    Left = 16
    Top = 368
    Width = 257
    Height = 25
    Caption = 'TMapEngineX.Create.Create.Apply_Theme'
    TabOrder = 10
    OnClick = Button9Click
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 658
    Width = 1516
    Height = 186
    ActivePage = TabSheet1
    Align = alBottom
    TabOrder = 11
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 1508
        Height = 158
        Align = alClient
        DataSource = ds_ObjectMaps
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      object DBGrid2: TDBGrid
        Left = 0
        Top = 0
        Width = 1508
        Height = 158
        Align = alClient
        DataSource = DataSource1
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
  end
  object Button10: TButton
    Left = 16
    Top = 240
    Width = 225
    Height = 25
    Caption = 'Dlg_Search_Yandex'
    TabOrder = 12
    OnClick = Button10Click
  end
  object Button11: TButton
    Left = 256
    Top = 240
    Width = 225
    Height = 25
    Caption = 'TMapEngineX.Create.Dlg_Search_Yandex'
    TabOrder = 13
    OnClick = Button11Click
  end
  object Button12: TButton
    Left = 16
    Top = 280
    Width = 225
    Height = 25
    Caption = 'CoMapEngineX.Create.Dlg_Search_Property'
    TabOrder = 14
    OnClick = Button12Click
  end
  object Button13: TButton
    Left = 256
    Top = 271
    Width = 225
    Height = 25
    Caption = 'TMapEngineX.Create.Dlg_Search_Property'
    TabOrder = 15
    OnClick = Button13Click
  end
  object Button14: TButton
    Left = 256
    Top = 302
    Width = 225
    Height = 25
    Caption = ' TMapEngineX.Create.ShowWindow ('
    TabOrder = 16
    OnClick = Button14Click
  end
  object PageControl2: TPageControl
    Left = 744
    Top = 184
    Width = 697
    Height = 441
    ActivePage = TabSheet3
    TabOrder = 17
    object TabSheet3: TTabSheet
      Caption = 'TabSheet3'
      object DBMemo1: TDBMemo
        Left = 0
        Top = 0
        Width = 689
        Height = 413
        Align = alClient
        DataField = 'style'
        DataSource = ds_ObjectMaps
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssBoth
        TabOrder = 0
        WordWrap = False
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'TabSheet4'
      ImageIndex = 1
    end
    object TabSheet5: TTabSheet
      Caption = 'TabSheet5'
      ImageIndex = 2
    end
  end
  object Button15: TButton
    Left = 1200
    Top = 120
    Width = 75
    Height = 25
    Caption = 'Button15'
    TabOrder = 18
    OnClick = Button15Click
  end
  object Edit1: TEdit
    Left = 0
    Top = 0
    Width = 1516
    Height = 21
    Align = alTop
    TabOrder = 19
    Text = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=SERVER1;Use Procedu' +
      're for Prepare=1;Auto Translate=True;Packet Size=4096;Workstatio' +
      'n ID=ALEX;Use Encryption for Data=False;Tag with column collatio' +
      'n when possible=False'
  end
  object Button16: TButton
    Left = 440
    Top = 80
    Width = 153
    Height = 25
    Caption = 'Dlg_Attributes linkend'
    TabOrder = 20
    OnClick = Button16Click
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=SERVER1;Use Procedu' +
      're for Prepare=1;Auto Translate=True;Packet Size=4096;Workstatio' +
      'n ID=ALEX;Use Encryption for Data=False;Tag with column collatio' +
      'n when possible=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 1144
    Top = 40
  end
  object cxLookAndFeelController1: TcxLookAndFeelController
    Kind = lfFlat
    Left = 630
    Top = 192
  end
  object ds_ObjectMaps: TDataSource
    DataSet = ADOStoredProc_Object_Maps
    Left = 752
    Top = 116
  end
  object ADOStoredProc_Object_Maps: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    Filtered = True
    LockType = ltBatchOptimistic
    ProcedureName = 'sp_MapDesktop_select_ObjectMaps'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CHECKED'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 752
    Top = 64
  end
  object FormPlacement1: TFormPlacement
    Left = 631
    Top = 256
  end
  object t_project: TADOTable
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 'project'
    Left = 864
    Top = 64
  end
  object DataSource1: TDataSource
    DataSet = t_project
    Left = 864
    Top = 116
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    Filtered = True
    LockType = ltBatchOptimistic
    ProcedureName = 'sp_MapDesktop_select_ObjectMaps'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 9
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CHECKED'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 992
    Top = 48
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredValues = <>
    Left = 1320
    Top = 56
  end
end
