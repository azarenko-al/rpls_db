unit ___Unit21;

interface

uses
  u_Options_classes,

  u_Theme,
             
  u_MapX,

  u_db,
  
  x_Map_Engine,
  
  Map_Engine_TLB,
  
  Winapi.Windows, System.SysUtils, System.Variants, System.Classes, 
  Vcl.Controls, Vcl.Forms, Data.DB, Data.Win.ADODB, Vcl.StdCtrls,
  cxLookAndFeels, 
  Vcl.DBGrids, Vcl.DBCtrls, 
  MapXLib_TLB, RxPlacemnt, Vcl.ComCtrls, cxClasses, Vcl.Grids, Vcl.OleCtrls,
  dxSkinsCore, dxSkinsDefaultPainters;

type
  TForm21 = class(TForm)
    ADOConnection1: TADOConnection;
    cxLookAndFeelController1: TcxLookAndFeelController;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    ds_ObjectMaps: TDataSource;
    ADOStoredProc_Object_Maps: TADOStoredProc;
    GroupBox1: TGroupBox;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Map1: TMap;
    Button7: TButton;
    FormPlacement1: TFormPlacement;
    Button8: TButton;
    Button9: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    t_project: TADOTable;
    DataSource1: TDataSource;
    Button10: TButton;
    Button11: TButton;
    Button12: TButton;
    Button13: TButton;
    Button14: TButton;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    DBMemo1: TDBMemo;
    Button15: TButton;
    ADOStoredProc1: TADOStoredProc;
    FormStorage1: TFormStorage;
    Edit1: TEdit;
    Button16: TButton;
    procedure Button10Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure Button13Click(Sender: TObject);
    procedure Button14Click(Sender: TObject);
    procedure Button15Click(Sender: TObject);
    procedure Button16Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
  private
    FDataset: Data.DB.TDataset;
  
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form21: TForm21;

implementation

{$R *.dfm}

procedure TForm21.Button10Click(Sender: TObject);
begin
  CoMapEngineX.Create.Dlg_Search_Yandex (Map1.DefaultInterface)

end;

procedure TForm21.Button11Click(Sender: TObject);
begin
  TMapEngineX.Create.Dlg_Search_Yandex (Map1.DefaultInterface)

end;

procedure TForm21.Button12Click(Sender: TObject);
begin
  CoMapEngineX.Create.Dlg_Search_Property (Map1.DefaultInterface, 
    ADOConnection1.ConnectionString, t_project['id'] );

end;


procedure TForm21.Button13Click(Sender: TObject);
begin
  TMapEngineX.Create.Dlg_Search_Property (Map1.DefaultInterface, 
    ADOConnection1.ConnectionString, t_project['id'] );

end;

procedure TForm21.Button14Click(Sender: TObject);
begin
  TMapEngineX.Create.Dlg_Feature_Info (Map1.DefaultInterface, 
    60.0466758346795, 30.4123858583758 );


 // Tfrm_Map_tool_Info.ShowWindow(aMap: CMapX; aLat, aLon: Double);

end;

procedure TForm21.Button15Click(Sender: TObject);
var
  iID: Integer;
  k: Integer;
  sObjName: string;
  sStyle: string;
  v: IMapEngineX;

begin
  v:=CoMapEngineX.Create;

  db_StoredProc_Open(ADOStoredProc1,'sp_MapDesktop_Select',  [FLD_PROJECT_ID, 1]);

//  db_StoredProc_Open(ADOStoredProc1,'sp_MapDesktop_Select',  [FLD_PROJECT_ID, 1026]);
  iID := ADOStoredProc1.FieldByName(FLD_ID).AsInteger;


  k:=db_StoredProc_Open(ADOStoredProc1,'sp_MapDesktop_select_ObjectMaps',  [FLD_ID, iID]);
  

with ADOStoredProc1 do
    while not Eof do
    begin
      sObjName:=FieldByName ('MapObject_name').AsString;
      sStyle  :=FieldByName ('Style').AsString;

   //   sStyle  := ''; //FieldByName (FLD_Style).AsString;


      v.Apply_Theme( Map1.DefaultInterface,sObjName, sStyle) ;


//      Init_IMapEngineX.Apply_Theme( Map1.DefaultInterface, sObjName, sStyle );

      NExt;
    end;  

end;

procedure TForm21.Button16Click(Sender: TObject);
begin
  TMapEngineX.Create.Dlg_Attributes (ADOConnection1.ConnectionString, 'linkend');

end;



procedure TForm21.FormCreate(Sender: TObject);
begin
  ADOStoredProc_Object_Maps.Active:=true;

  FDataset:=ADOStoredProc_Object_Maps;

  mapx_SetBounds(Map1, mapx_GetPlanBounds(Map1) );
  
//  mapx_
  
end;


procedure TForm21.Button1Click(Sender: TObject);
var
  sObjName: string;
  v: IMapEngineX;
begin
  sObjName := FDataset.FieldByName('MapObject_name').AsString;


  v:=CoMapEngineX.Create;
  v.Dlg_Attributes(ADOConnection1.ConnectionString, sObjName);

 // Tdlg_Layer_Attributes.ExecDlg (ADOConnection1.ConnectionString, 'property');
  
end;

procedure TForm21.Button2Click(Sender: TObject);
var
  sText: Widestring;
  v: IMapEngineX;
  
begin
  sText := FDataset.FieldByName('Label_Style').AsString;

  v:=CoMapEngineX.Create;
  
  if v.Dlg_Label_style (sText) = S_OK then 
    db_UpdateRecord__(FDataset, ['Label_Style', sText]);
  

end;

procedure TForm21.Button3Click(Sender: TObject);
 var
  sObjName: string;
 
  sXML: Widestring;
begin
  sObjName := FDataset.FieldByName('MapObject_name').AsString;
  sXML     := FDataset.FieldByName('style').AsString;


  if CoMapEngineX.Create.Dlg_Style (ADOConnection1.ConnectionString, sObjName, sXML) = S_OK then
    db_UpdateRecord__(FDataset, ['style', sXML]);
    

  
end;

procedure TForm21.Button4Click(Sender: TObject);
var
  sObjName: string;
begin
//  sObjName := FDataset.FieldByName('MapObject_name').AsString;


  TMapEngineX.Create.Dlg_Attributes (ADOConnection1.ConnectionString, 'property');

end;

// ---------------------------------------------------------------
procedure TForm21.Button5Click(Sender: TObject);
// ---------------------------------------------------------------
var
  sText: WideString;
begin
  sText := FDataset.FieldByName('Label_Style').AsString;

  if TMapEngineX.Create.Dlg_Label_style (sText) = S_OK then
    db_UpdateRecord__(FDataset, ['Label_Style', sText]);

end;

// ---------------------------------------------------------------
procedure TForm21.Button6Click(Sender: TObject);
// ---------------------------------------------------------------
var
  oThemeMaker: TThemeMaker;
  sObjName: string;
  sStyle_XML: Widestring;
begin
  sObjName := FDataset.FieldByName('MapObject_name').AsString;
  sStyle_XML     := FDataset.FieldByName('style').AsString;


//  TMapEngineX.Create.Dlg_Style (ADOConnection1.ConnectionString, sObjName);

  if TMapEngineX.Create.Dlg_Style (ADOConnection1.ConnectionString, sObjName, sStyle_XML) = S_OK then 
  begin
    db_UpdateRecord__(FDataset, ['style', sStyle_XML]);

    //-------------------------
    
    oThemeMaker:=TThemeMaker.Create;
    oThemeMaker.Apply_Theme_for_object(Map1.DefaultInterface, sObjName, sStyle_XML);

    FreeAndNil(oThemeMaker);
    

  end;  

  
end;

procedure TForm21.Button7Click(Sender: TObject);
var
  oObj: TOptionsObject;
  oDict: TOptionsObjectDict;
  sName: string;
  sStyle: string;

  oThemeMaker: TThemeMaker;
  
begin
  oDict:=TOptionsObjectDict.Create;

  
  ADOStoredProc_Object_Maps.First;

  with ADOStoredProc_Object_Maps do
    while not EOF do
    begin
      sName :=FieldByName('MapObject_name').AsString;
      sStyle:=FieldByName('Style').AsString;

      oObj:=TOptionsObject.Create;
      oObj.LoadFromXML(sStyle);

      oDict.Add(sName, oObj);
  
      Next;
    end;

  ADOStoredProc_Object_Maps.First;


    
  oThemeMaker:=TThemeMaker.Create;
  oThemeMaker.Apply_Theme(Map1.DefaultInterface, oDict);

  FreeAndNil(oThemeMaker);
  

//  oDict[sName].
  
  

  FreeAndNil(oDict);
  
end;

procedure TForm21.Button8Click(Sender: TObject);
 var
  sObjName: string;
 
  sXML: Widestring;
begin
  sObjName := FDataset.FieldByName('MapObject_name').AsString;
  sXML     := FDataset.FieldByName('style').AsString;


  CoMapEngineX.Create.Apply_Theme( Map1.DefaultInterface,sObjName, sXML) ;
  //  db_UpdateRecord(FDataset, 'XML', sXML);
    

  
end;



procedure TForm21.Button9Click(Sender: TObject);
 var
  sObjName: string;
 
  sXML: Widestring;
begin
  sObjName := FDataset.FieldByName('MapObject_name').AsString;
  sXML     := FDataset.FieldByName('style').AsString;


  TMapEngineX.Create.Create.Apply_Theme( Map1.DefaultInterface, sObjName, sXML) ;
  //  db_UpdateRecord(FDataset, 'XML', sXML);
    

  
end;



end.
                                                       