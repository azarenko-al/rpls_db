program map_engine_test;

uses
  Vcl.Forms,
  ___Unit21 in '___Unit21.pas' {Form21},
  d_Layer_Attributes in '..\src\d_Layer_Attributes.pas' {dlg_Layer_Attributes},
  x_map_engine in '..\x_map_engine.pas',
  Map_Engine_TLB in '..\Map_Engine_TLB.pas',
  d_Column_get in '..\src\options\d_Column_get.pas' {dlg_Column_get},
  d_Symbol_Select in '..\src\options\d_Symbol_Select.pas' {SymbolSelectDialog},
  f_Options in '..\src\options\f_Options.pas' {frm_Main_Options},
  u_Options_classes in '..\src\options\u_Options_classes.pas',
  u_Theme in '..\src\options\u_Theme.pas',
  d_Map_Search_Property in '..\src\search\d_Map_Search_Property.pas' {dlg_Map_Search_Property},
  f_Map_tool_Info in '..\src\f_Map_tool_Info.pas' {frm_Map_tool_Info},
  d_Map_Label_Setup in 'W:\common XE\Map\d_Map_Label_Setup.pas' {dlg_Map_Label_Setup},
  d_Map_Search_WEB in '..\src\search\d_Map_Search_WEB.pas' {dlg_Map_Search_WEB},
  u_web in 'W:\common XE\u_web.pas',
  u_xml_new in 'W:\common XE\u_xml_new.pas',
  u_ini_geocoding in '..\..\GeoCoding\src\u_ini_geocoding.pas',
  Unit4 in 'Unit4.pas' {Form4};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
//..  Application.CreateForm(Tdlg_Map_Search_WEB, dlg_Map_Search_WEB);
    Application.CreateForm(TForm21, Form21);
  Application.Run;
end.
