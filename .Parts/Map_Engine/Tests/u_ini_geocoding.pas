unit u_ini_geocoding;


interface

uses
  SysUtils, Dialogs,

  CodeSiteLogging,  Windows, Forms, IniFiles;

  
type       

  TIni_Geocoding_rec = record
  public
    Active: boolean;

    Yandex : string;   
    OSM    : string;

    URL_get_address : string;   
                    
    procedure LoadFromFile;

  end;


var 
  g_Ini_Geocoding: TIni_Geocoding_rec;

 
implementation
           


// ---------------------------------------------------------------
procedure TIni_Geocoding_rec.LoadFromFile;
// ---------------------------------------------------------------
var
  oIni: TIniFile;
  sFile: string;
begin
  inherited;


  {$IFDEF dll}

  sFile := ExtractFilePath( System.SysUtils.GetModuleName(HInstance) ) + 'Geocoding.ini';
  
 // sFile:=ExtractFilePath(Application.ExeName) + 'snmp.ini';
 
  {$ELSE}      
  sFile:='C:\ONEGA\RPLS_DB\bin\dll\Geocoding.ini';
  {$ENDIF}


  CodeSite.Send(sFile);

  Assert ( FileExists(sFile) , sFile);



  oIni:=TIniFile.Create (sFile);

  Active:=LowerCase(trim(oIni.ReadString ('main','Active','true')))='true';

  Yandex:=oIni.ReadString ('main','Yandex','');
  OSM   :=oIni.ReadString  ('main','OSM','');

  URL_get_address :=oIni.ReadString ('get_address','Yandex','');
  
  FreeAndNil (oIni);
  
end;


begin
  g_Ini_Geocoding.LoadFromFile;



end.


