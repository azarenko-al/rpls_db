unit dm_WEB_search;

interface

uses

  Winapi.Windows, System.SysUtils, Variants, Forms,
  WinInet, IniFiles, IdURI,

  CodeSiteLogging,
  
  u_ini_geocoding,

//  u_web,
//  u_xml_new,
 
  u_db,

  u_files,
  
  u_func, 
  u_func_arr,
 
 
  Xml.Win.msxmldom, Xml.XMLDoc, Xml.xmldom, Xml.XMLIntf, Data.DB, dxmdaset,
  System.Classes;

type
  TdmWEB_search = class(TDataModule)
    XMLDocument1: TXMLDocument;
    mem_Data: TdxMemData;
    mem_Dataposition: TStringField;
    mem_Datalat: TFloatField;
    mem_Datalon: TFloatField;
    procedure DataModuleCreate(Sender: TObject);
  private

//    FIni_params: record
//      Yandex : string;
//      OSM    : string;
//    end;  

  
//    procedure LoadFromIniFile;

 
  public
    Result_Url: string;

 //   Result : record
    AddressNode : string;
 
    
    class function Init: TdmWEB_search;

    function Search_OSM(aAddress: string): Boolean;
    function Search_Yandex(aAddress: string; aResults: byte = 10): Boolean;

    function Get_Address_By_Lat_Lon_WGS(aLat, aLon: Double; var aResult_Address:
        String): Boolean;

  end;

var
  dmWEB_search: TdmWEB_search;


function DownloadFile(aURL, aFileName: string): Boolean; //forward;
  
  
implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

          

function xml_GetNodeByPath(aNode: IXMLNode; aPathArr: array of string): IXMLNode; forward;
 


class function TdmWEB_search.Init: TdmWEB_search;
begin
  if not Assigned(dmWEB_search) then
    Application.CreateForm(TdmWEB_search, dmWEB_search);

  Result:=dmWEB_search;
end;


                   
procedure TdmWEB_search.DataModuleCreate(Sender: TObject);
begin

end;


(*
// ---------------------------------------------------------------
procedure TdmWEB_search.LoadFromIniFile;
// ---------------------------------------------------------------
var
  oIni: TIniFile;
  sFile: string;
begin
  inherited;


  {$IFDEF dll}

  sFile := ExtractFilePath( System.SysUtils.GetModuleName(HInstance) ) + 'Geocoding.ini';
  
 // sFile:=ExtractFilePath(Application.ExeName) + 'snmp.ini';
 
  {$ELSE}      
  sFile:='C:\ONEGA\RPLS_DB\bin\dll\Geocoding.ini';
  {$ENDIF}                            
                    
//  CodeSite.Send(sFile);

  Assert ( FileExists(sFile) , sFile);
                                         
  
  oIni:=TIniFile.Create (sFile);

  FIni_params.Yandex:=oIni.ReadString ('main','Yandex','');
  FIni_params.OSM   :=oIni.ReadString ('main','OSM','');

  
  FreeAndNil (oIni);
  
end;

*)


// ---------------------------------------------------------------
function TdmWEB_search.Get_Address_By_Lat_Lon_WGS(aLat, aLon: Double; var  aResult_Address: String): Boolean;
// ---------------------------------------------------------------
var
  s: string;
  sOSM: string;
begin
  s:= Format('%1.6f,%1.6f',[aLon, aLat]);
  sOSM:= Format('%1.6f,%1.6f',[aLat, aLon]);

  CodeSite.Send(s);
              
  if Search_Yandex(s, 1) then 
  begin
    aResult_Address:=dmWEB_search.AddressNode;
    Result:=True;
      
  end else
  
  if Search_OSM(sOSM) then 
  begin
    aResult_Address:=dmWEB_search.AddressNode;
    Result:=True;
      
  end else
    Result:=False;  

end;


// ---------------------------------------------------------------
function TdmWEB_search.Search_Yandex(aAddress: string; aResults: byte = 10):  Boolean;
// ---------------------------------------------------------------
                          
var
  i: Integer;
  s: string;
  vAddressNode: IXMLNode;
  vGeoObjectCollection: IXMLNode;
  vNode: IXMLNode;
  vPointNode: IXMLNode;

  arrStr: TStrArray;
  eLat_WGS: Double;
  eLon_WGS: Double;
  sFile: string;
  
begin
  Result:=False;
  
  sFile:=GetTempXmlFileName();

  AddressNode:='';    
      
  {$IFDEF debug}
  sFile:='d:\test1.xml' ;
  {$ENDIF}
               
      
  mem_Data.Close;
  mem_Data.Open;
 
  Result_Url:=g_Ini_Geocoding.Yandex +  TIdURI.ParamsEncode (aAddress);

  Result_Url:=ReplaceStr(Result_Url, ':results', IntToStr(aResults));
  

CodeSite.Send (Result_Url);

   try
     if not DownloadFile (Result_Url, sFile) then
       exit;

     XMLDocument1.LoadFromFile(sFile);
     
   except on E: Exception do
     Exit;
     
   end;
 

 //----------------------------------------
  
  i:=XMLDocument1.ChildNodes.Count;

  vNode:=XMLDocument1.ChildNodes.FindNode('ymaps');
  if not Assigned(vGeoObjectCollection) then
    Exit;
  
  vGeoObjectCollection:=xml_GetNodeByPath (vNode, ['GeoObjectCollection' ]);

  if Assigned(vGeoObjectCollection) then
    for I := 0 to vGeoObjectCollection.ChildNodes.Count -1 do
      if vGeoObjectCollection.ChildNodes[i].LocalName = 'featureMember' then
      begin
        vAddressNode:=xml_GetNodeByPath (vGeoObjectCollection.ChildNodes[i], ['GeoObject','metaDataProperty','GeocoderMetaData ','AddressDetails','Country','AddressLine' ]);
        s:=vAddressNode.Text;

        vPointNode:=xml_GetNodeByPath (vGeoObjectCollection.ChildNodes[i], ['GeoObject','Point','pos' ]);
        s:=vPointNode.Text;


        arrStr:= StringToStrArray(s, ' ');

        if High(arrStr) >= 1 then
        begin
          Result:=True;


          eLat_WGS:= AsFloat(arrStr[1]);
          eLon_WGS:= AsFloat(arrStr[0]);

          AddressNode:=vAddressNode.Text;

          db_AddRecord_(mem_Data, [
             'address', vAddressNode.Text,
             'lat_WGS', eLat_WGS,
             'lon_WGS', eLon_WGS
             ]);
          
        end;

      end;  
  
end;


            
/////////////////////////////////////////////////////////////////

// ---------------------------------------------------------------
function TdmWEB_search.Search_OSM(aAddress: string): Boolean;
// ---------------------------------------------------------------

var
  i: Integer;
  sPath: WideString;

  vRoot: IXMLNode;
  vNode: IXMLNode;
  vPointNode: IXMLNode;

  eLat_WGS: Double;
  eLon_WGS: Double;
  s: string;
  sFile: string;
  
begin
  Result:=False;
 
  sFile:=GetTempXmlFileName();
      
  {$IFDEF debug}
  sFile:='d:\test.xml' ;
  {$ENDIF}

      
  mem_Data.Close;
  mem_Data.Open;
          

  Result_Url:=g_Ini_Geocoding.OSM +  TIdURI.ParamsEncode ( aAddress);

  try
    if not DownloadFile ( g_Ini_Geocoding.OSM +  TIdURI.ParamsEncode ( aAddress), sFile) then 
      exit;

    XMLDocument1.LoadFromFile(sFile); 

  except on E: Exception do
    Exit;
  end;


//  DeleteFile(sFile);

 //----------------------------------------
  
  i:=XMLDocument1.ChildNodes.Count;

//  vNode:=XMLDocument1.DocumentElement;
  
  s:=XMLDocument1.DocumentElement.LocalName;

  vRoot:=XMLDocument1.DocumentElement.ChildNodes.FindNode('searchresults');
  
  vRoot:=XMLDocument1.DocumentElement;

   if Assigned(vRoot) then
    for I := 0 to vRoot.ChildNodes.Count -1 do
      if vRoot.ChildNodes[i].LocalName = 'place' then
      begin
        Result:=true;
      
        vNode:=vRoot.ChildNodes[i];

        eLat_WGS:= AsFloat(vNode.Attributes['lat']);
        eLon_WGS:= AsFloat(vNode.Attributes['lon']);
        
        AddressNode:=vNode.Attributes['display_name'];

        db_AddRecord_(mem_Data, [
             'address', vNode.Attributes['display_name'],
             'lat_WGS', eLat_WGS,
             'lon_WGS', eLon_WGS
             ]);
    end;  
  
end;

//
//        sURL:= CONST_YANDEX_GEOCODE + AsString(oGeoItem.Lon_WGS) +','+ AsString(oGeoItem.Lat_WGS) + '&results=1';
//        oXMLStrList.Text:= webgeo_GetTextByURL(hSession, sURL);
//
//        oGeoItem.Address:= webgeo_GetAddressFromXML (oXMLStrList, aAddCountry);



// ---------------------------------------------------------------
function DownloadFile(aURL, aFileName: string): Boolean;
// ---------------------------------------------------------------
const
  BufferSize = 1024;
var
  hSession, hURL: HInternet;
  Buffer: array[1..BufferSize] of Byte;
  BufferLen: DWORD;
  F: File;
begin
   Result := False;
   hSession := InternetOpen('', INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0) ;

   try
     // Establish the secure connection
     InternetConnect (
       hSession,
       PChar(aURL),
       INTERNET_DEFAULT_HTTPS_PORT,
       nil, // PChar(''),
       nil, //PChar(''),
       INTERNET_SERVICE_HTTP,
       0,
       0
     );        
   except on E: Exception do
     Exit;
   end;

  try
    hURL := InternetOpenURL(hSession, PChar(aURL), nil, 0, 0, 0) ;
    try
      AssignFile(f, aFileName);
      Rewrite(f,1);
      try
        repeat
          InternetReadFile(hURL, @Buffer, SizeOf(Buffer), BufferLen) ;
          BlockWrite(f, Buffer, BufferLen)
        until BufferLen = 0;
      finally
        CloseFile(f) ;
        Result := True;
      end;
    finally
      InternetCloseHandle(hURL)
    end
  finally
    InternetCloseHandle(hSession)
  end;

  Assert( FileExists(aFileName), aFileName);
end;





//--------------------------------------------------------
function Eq(aValue1, aValue2: string): boolean;
//--------------------------------------------------------
begin
  Result := AnsiCompareText(Trim(aValue1),Trim(aValue2))=0;
end;


//-----------------------------------------------------------
function xml_GetNodeByPath(aNode: IXMLNode; aPathArr: array of string):     IXMLNode;
//-----------------------------------------------------------
var i,j,iCOunt:integer;   
   vElement: IXMLNode;   strArr: array of string;
  sTag: string;
begin
  Result:=nil;

  if VarIsNull(aNode) then
    Exit;

//  iCOunt:=aNode.childNodes.Count;

 // if High(aPathArr)>=0 then

  for i:=0 to aNode.childNodes.Count-1 do
  begin
    vElement:=aNode.childNodes[i];

//    if not xml_IsElement(vElement) then
//      Continue;

    sTag:=vElement.LocalName;

    if sTag='' then Continue;

    if Eq(sTag ,aPathArr[0]) then
      if High(aPathArr)=0
       then begin 
         Result:=vElement; 
         Exit; 
       end
       else begin
         SetLength(strArr, High(aPathArr));
         for j:=0 to High(aPathArr)-1 do 
           strArr[j]:=aPathArr[j+1];

         Result:=xml_GetNodeByPath (vElement, strArr);
       end;
  end;


end;


end.


{


unit u_ini_geocoding;


interface

uses
  SysUtils, Dialogs,

  CodeSiteLogging,  Windows, Forms, IniFiles;

  
type       

  TIni_Geocoding_rec = record
  public
    Yandex : string;   
    OSM    : string;
                    
    procedure LoadFromFile;

  end;


var 
  g_Ini_Geocoding: TIni_Geocoding_rec;



  
function TGeoCoding.Get_Address_By_Lat_Lon_WGS(aLat: Double; aLon: Double; out  aResult_Address: WideString): HResult;
var
  s: string;
begin
  TdmWEB_search.Init;

  s:= Format('%1.6f,%1.6f',[aLon, aLat]);
              
  if dmWEB_search.Search_Yandex(s, 1) then 
    aResult_Address:=dmWEB_search.AddressNode;
  
//    sURL:= CONST_YANDEX_GEOCODE + AsString(oGeoItem.Lon_WGS) +','+ AsString(oGeoItem.Lat_WGS) + '&results=1';
//        oXMLStrList.Text:= webgeo_GetTextByURL(hSession, sURL);  

// Result := ;
end;
