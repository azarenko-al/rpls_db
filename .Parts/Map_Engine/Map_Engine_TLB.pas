unit Map_Engine_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 02.05.2019 17:57:00 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\RPLS_DB\.Parts\Map_Engine\Project20 (1)
// LIBID: {558F0597-AB43-4BCB-A1C6-0DBA4FA80837}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (stdvcl40.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface


  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  Map_EngineMajorVersion = 1;
  Map_EngineMinorVersion = 0;

  LIBID_Map_Engine: TGUID = '{558F0597-AB43-4BCB-A1C6-0DBA4FA80837}';

  IID_IMapEngineX: TGUID = '{E4D1BAE7-E5AF-4492-9C57-C5981D562E04}';
  CLASS_MapEngineX: TGUID = '{8D29B21C-85C6-4ABB-855C-4264098105C9}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IMapEngineX = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  MapEngineX = IMapEngineX;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  PWideString1 = ^WideString; {*}


// *********************************************************************//
// Interface: IMapEngineX
// Flags:     (256) OleAutomation
// GUID:      {E4D1BAE7-E5AF-4492-9C57-C5981D562E04}
// *********************************************************************//
  IMapEngineX = interface(IUnknown)
    ['{E4D1BAE7-E5AF-4492-9C57-C5981D562E04}']
    function Dlg_Attributes(const aConnectionStr: WideString; const aObjName: WideString): HResult; stdcall;
    function Dlg_Label_style(var aText: WideString): HResult; stdcall;
    function Dlg_Style(const aConnectionStr: WideString; const aObjName: WideString; 
                       var aXML: WideString): HResult; stdcall;
    function Apply_Theme(const aMap: IUnknown; const aObjName: WideString; 
                         const aStyle_XML: WideString): HResult; stdcall;
    function Dlg_Feature_Info(const aMap: IUnknown; aLat: Double; aLon: Double): HResult; stdcall;
    function Dlg_Search_Yandex(const aMap: IUnknown): HResult; stdcall;
    function Dlg_Search_Property(const aMap: IUnknown; const aConnectionStr: WideString; 
                                 aProject_ID: Integer): HResult; stdcall;
    function Dlg_Property_geocoding(const aConnectionStr: WideString; const aIDS: WideString): HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoMapEngineX provides a Create and CreateRemote method to          
// create instances of the default interface IMapEngineX exposed by              
// the CoClass MapEngineX. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoMapEngineX = class
    class function Create: IMapEngineX;
    class function CreateRemote(const MachineName: string): IMapEngineX;
  end;

implementation

uses System.Win.ComObj;

class function CoMapEngineX.Create: IMapEngineX;
begin
  Result := CreateComObject(CLASS_MapEngineX) as IMapEngineX;
end;

class function CoMapEngineX.CreateRemote(const MachineName: string): IMapEngineX;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_MapEngineX) as IMapEngineX;
end;

end.

