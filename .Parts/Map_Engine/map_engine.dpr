library map_engine;

uses
  ComServ,
  Map_Engine_TLB in 'Map_Engine_TLB.pas',
  x_map_engine in 'x_map_engine.pas' {MapEngineX_v1: CoClass},
  d_Layer_Attributes in 'src\d_Layer_Attributes.pas' {dlg_Layer_Attributes},
  d_Map_Label_Setup in 'W:\common XE\Map\d_Map_Label_Setup.pas' {dlg_Map_Label_Setup},
  d_Column_get in 'src\options\d_Column_get.pas' {dlg_Column_get},
  d_Symbol_Select in 'src\options\d_Symbol_Select.pas' {SymbolSelectDialog},
  f_Options in 'src\options\f_Options.pas' {frm_Main_Options},
  u_Options_classes in 'src\options\u_Options_classes.pas',
  u_Theme in 'src\options\u_Theme.pas',
  f_Map_tool_Info in 'src\f_Map_tool_Info.pas',
  d_Map_Search_Property in 'src\search\d_Map_Search_Property.pas' {dlg_Map_Search_Property},
  u_search_web_geocode__11111111 in 'src\search\u_search_web_geocode__11111111.pas',
  d_Map_Search_WEB in 'src\search\d_Map_Search_WEB.pas' {dlg_Map_Search_WEB},
  u_xml_new in 'W:\common XE\u_xml_new.pas',
  u_ini_geocoding in '..\GeoCoding\src\u_ini_geocoding.pas',
  dm_WEB_search in '..\GeoCoding\src\dm_WEB_search.pas' {dmWEB_search: TDataModule};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer,
  DllInstall;

{$R *.TLB}

{$R *.RES}

begin
end.
