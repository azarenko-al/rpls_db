object Form1: TForm1
  Left = 1096
  Top = 60
  Caption = 'Form1'
  ClientHeight = 416
  ClientWidth = 1137
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    1137
    416)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 0
    Top = 376
    Width = 1137
    Height = 40
    Align = alBottom
    Shape = bsTopLine
    ExplicitTop = 375
    ExplicitWidth = 1381
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 457
    Height = 376
    Align = alLeft
    TabOrder = 0
    object cxGrid1DBTableView1_params: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ds_ADOStoredProc1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1_paramscaption: TcxGridDBColumn
        DataBinding.FieldName = 'caption'
        Width = 140
      end
      object cxGrid1DBTableView1_paramsname: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Width = 138
      end
      object cxGrid1DBTableView1_paramsColumn1: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Width = 103
      end
      object cxGrid1DBTableView1_paramsColumn2: TcxGridDBColumn
        DataBinding.FieldName = 'type'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1_params
    end
  end
  object cxGrid2: TcxGrid
    Left = 465
    Top = 0
    Width = 304
    Height = 376
    Align = alLeft
    TabOrder = 1
    ExplicitLeft = 497
    ExplicitTop = -216
    object cxGridDBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ds_ADOStoredProc2_values
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      object cxGridDBTableView1name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Width = 109
      end
      object cxGridDBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Width = 80
      end
      object cxGridDBTableView1Column2: TcxGridDBColumn
        DataBinding.FieldName = 'code'
        Width = 85
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 457
    Top = 0
    Width = 8
    Height = 376
    Control = cxGrid1
  end
  object Button1: TButton
    Left = 973
    Top = 383
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = #1042#1099#1073#1088#1072#1090#1100
    ModalResult = 1
    TabOrder = 3
  end
  object Button2: TButton
    Left = 1054
    Top = 383
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = #1047#1072#1082#1088#1099#1090#1100
    ModalResult = 8
    TabOrder = 4
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = ADOConnection2
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'map.sp_options_object_list_names'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@OBJNAME'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 864
    Top = 112
  end
  object ds_ADOStoredProc1: TDataSource
    DataSet = ADOStoredProc1
    Left = 866
    Top = 168
  end
  object ADOConnection2: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Password=sa;Persist Security Info=True;User I' +
      'D=sa;Data Source=onega_link'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'MSDASQL.1'
    Left = 864
    Top = 32
  end
  object ds_ADOStoredProc2_values: TDataSource
    DataSet = ADODataSet1_values
    Left = 1002
    Top = 168
  end
  object ADODataSet1_values: TADODataSet
    Connection = ADOConnection2
    CursorType = ctStatic
    LockType = ltReadOnly
    CommandText = 'exec map.sp_options_object_list_values  @id = :id'
    DataSource = ds_ADOStoredProc1
    Parameters = <
      item
        Name = 'id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 272219
      end>
    Left = 1000
    Top = 112
  end
end
