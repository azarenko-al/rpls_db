unit u_Options_classes;

interface
uses
  u_xml_document,

  System.IOUtils,
  MapXLib_TLB,    
  
  u_func,
  u_db,
  
  SysUtils, Variants,
  System.Generics.Collections, 

  Data.DB, Xml.XMLIntf, Data.Win.ADODB;


type          
  TOptionsObject = class; 
  TOptionsStyleItem = class;
  TOptionsObjectStyle = class;
  TOptionsObjectDict = class;


  
 // TFeatureType = (miFeatureTypeRegion,  miFeatureTypeLine ,  miFeatureTypeSymbol);

  
  TStyleRec = record
 //   rec_ID : integer;

    FeatureType: (ftLine,ftSymbol,ftRegion);
 
  
    Line_Color            : integer;
    Line_Width            : integer;
    Line_Style            : integer;

    //Symbol    
    Symbol_Character      : integer;
    Symbol_Font_Name       : string;
    
    Symbol_Font_Color      : integer;
    Symbol_Font_Size       : integer;
 

    //Region Border
    RegionBorderWidth    : integer;
    RegionBorderStyle    : integer;
    RegionBorderColor    : integer;

    //Region fill
    RegionPattern        : integer;
    RegionColor: integer;  //���� ����� ��� �����
    RegionBackColor: integer;      

    
  private
  public

    procedure LoadFromDB(aDataset: TDataSet);
    procedure SaveToDB(aDataset: TDataSet);

    procedure SaveToXML(aItem: IXMLNode);
    procedure LoadFromXML(aItem: IXMLNode);

    procedure Set_Default;

    procedure Assign(aStyle: CMapXStyle; aFeatureType: byte);

  end;



  //------------------------
 
  
  TOptionsStyleItem = class
  public
    Field_Value: string;
    Enabled: boolean;

    Style: TStyleRec;

    constructor Create;
    
    procedure LoadFromDB(aStyleName: string; aDataset: TDataSet);
  end;


  TOptionsStyleItemList= class(TList<TOptionsStyleItem>)
  public
    function FindByValue(aValue: string): TOptionsStyleItem;

  end;

  TStyleKind = (
                sk_None,
                sk_Line_Color,
                sk_Line_Width,
                sk_Line_Style,

                sk_Symbol_Character,
                sk_Symbol_Font_Color,
                sk_Symbol_Font_Size
               ) ;

  
  //------------------------------------------------ 
  TOptionsObjectStyle = class
  //------------------------------------------------ 
  public
    Name          : string;
    
    Field_Caption : string;
    Field_Name    : string;
    Enabled       : boolean;

    Items: TOptionsStyleItemList;

    constructor Create;
    
    
    procedure LoadFromXMLNode(aItem: IXMLNode);
    procedure SaveToXMLNode(aItem: IXMLNode);

    function GetStyleKind: TStyleKind;

  end;
  
  TOptionsStyleList= class(TList<TOptionsObjectStyle>);
//  TOptionsStyleList= class(TDictionary<String, TOptionsObjectStyle>);

//    TOptionsStyleDict= class(TDictionary<String, TOptionsObject>);

  
  //------------------------------------------------ 
  TOptionsObject = class  
  //------------------------------------------------   
  private
  public
//    FeatureType_str : string;
    FeatureType : Byte; //(fkLine,fkSymbol,fkRegion);

    // // TFeatureType = (miFeatureTypeRegion,  miFeatureTypeLine ,  miFeatureTypeSymbol);

    
    ObjName      : string;

    DefaultStyle: TStyleRec;
  
    Styles: TOptionsStyleList;

    constructor Create;                                

    procedure Remove_Disabled_Items;              
    
    procedure SetFeature_type(aValue: string);
    function GetFeatureType_str: string;

    function SaveToXML: string;
    
    procedure LoadFromXML(aText: string);
    procedure LoadFromFile(aFileName: string);
    
    procedure LoadFromDB(aDataset_Object, aDataset_Styles, aDataset_Style_items: TDataSet);
    procedure SaveToDB(aADOConnection: TADOConnection; aDataset_Object,    aDataset_Styles, aDataset_Style_items: TDataSet);

  end;
  
  TOptionsObjectDict= class(TDictionary<String, TOptionsObject>);
    
                 
implementation

uses
  Vcl.Graphics;

constructor TOptionsObject.Create;
begin
  inherited;

  Styles:=TOptionsStyleList.Create;

  DefaultStyle.Set_Default;

end;


function TOptionsObject.GetFeatureType_str: string;
begin
  case FeatureType of
    miFeatureTypeLine: result:='line';
    miFeatureTypeSymbol: result:='Symbol';
    miFeatureTypeRegion: result:='Region';
  else
    result:='';
  end;
 
end;


// ---------------------------------------------------------------
procedure TOptionsObject.LoadFromDB(aDataset_Object, aDataset_Styles,
    aDataset_Style_items: TDataSet);
// ---------------------------------------------------------------

  procedure DoLoadFromDB_Style_items (aStyle: TOptionsObjectStyle);
  var
    oItem: TOptionsStyleItem;
  begin
    with aDataset_Style_items do
      while not Eof do
      begin
        oItem:=TOptionsStyleItem.Create;        
        aStyle.Items.Add(oItem);

        oItem.LoadFromDB (aStyle.Name, aDataset_Style_items);
               
        Next;
      end;  
  
  end;
            

var

  oStyle: TOptionsObjectStyle;
  s: string;
 // sName: string;

  sobjname: string;
begin
  objname    :=aDataset_Object['objname'];
  s:=aDataset_Object['Feature_type'];
  
  SetFeature_type(s);
  
  DefaultStyle.LoadFromDB(aDataset_Object);

  Styles.Clear;
  
  aDataset_Styles.First;

  with aDataset_Styles do
    while not Eof do
    begin
  //    if aDataset_Styles.FieldByName('Enabled').AsBoolean then
//    begin
//      sName:=FieldValues['name'];

//      if Styles.ContainsKey(sName) then
//        Continue;
      
      
      oStyle:=TOptionsObjectStyle.Create;        
      Styles.Add(oStyle);

      oStyle.Name         := aDataset_Styles.FieldByName('Name').AsString;
      oStyle.Field_Name   := aDataset_Styles.FieldByName('Field_Name').AsString;
      oStyle.Field_Caption:= aDataset_Styles.FieldByName('Field_Caption').AsString;
      oStyle.Enabled      := aDataset_Styles.FieldByName('Enabled').AsBoolean;

      DoLoadFromDB_Style_items(oStyle);
 //   end;    
           
      Next;
    end;  

end;


// ---------------------------------------------------------------
procedure TOptionsObject.SaveToDB(aADOConnection: TADOConnection; aDataset_Object, aDataset_Styles, aDataset_Style_items: TDataSet);
// ---------------------------------------------------------------

    procedure DoLoadFromDB_Style_items (aStyle: TOptionsObjectStyle);
    var
      oItem: TOptionsStyleItem;
    begin
      with aDataset_Style_items do
        while not Eof do
        begin          
          oItem:=TOptionsStyleItem.Create;        
          aStyle.Items.Add(oItem);

          oItem.LoadFromDB (aStyle.Name, aDataset_Style_items);
               
          Next;
        end;     
    end;  


var

  oStyle: TOptionsObjectStyle;
  oStyleItem: TOptionsStyleItem;
  sName: string;

  sobjname: string;
  I: Integer;
  iID: Integer;
  j: Integer;
  sKey: string; 
begin
 // objname:=aDataset_Object.FieldValues['objname'];

  DefaultStyle.SaveToDB(aDataset_Object);

//  aDataset_Styles.DisableControls;
//  aDataset_Style_items.DisableControls;
  

  for i:=0 to Styles.count-1 do
    if aDataset_Styles.Locate('name', Styles[i].Name, []) then
    begin
      oStyle:= Styles[i];

      iID:=aDataset_Styles['id'];

    
      db_UpdateRecord__(aDataset_Styles,
      [
        'Enabled',       oStyle.Enabled,
      //  'Name',          Items[i].Name,
        'Field_Name',    oStyle.Field_Name ,
        'Field_Caption', oStyle.Field_Caption
      ]);     
   
                                                                                   
      aADOConnection.Execute('delete * from object_item_values where parent_id=' + IntToStr(iID));
   
      aDataset_Style_items.Close;
      aDataset_Style_items.Open;
   
//      with aDataset_Style_items do
 ///       while Locate('parent_id', iID, []) do 
    //      Delete;
                             
        
        
      for j := 0 to oStyle.Items.Count-1 do
      //  if aDataset_Styles.Locate('name', oStyle.Items[j].Name, []) then     
        begin
          oStyleItem:=oStyle.Items[j];
          
    //      Assert(oStyleItem.Field_Value <> '');

         try

          db_AddRecord_(aDataset_Style_items,
          [
            'parent_id',     iID,
            'Enabled',       oStyleItem.Enabled,
            'Field_Value',   oStyleItem.Field_Value,


            'Line_Color', oStyleItem.Style.Line_Color,
            'Line_Width', oStyleItem.Style.Line_Width,
            'Line_Style', oStyleItem.Style.Line_Style,

            'Symbol_Character',  oStyleItem.Style.Symbol_Character,
            'Symbol_Font_Name',  oStyleItem.Style.Symbol_Font_Name,
            'Symbol_Font_Color', oStyleItem.Style.Symbol_Font_Color,
            'Symbol_Font_Size',  oStyleItem.Style.Symbol_Font_Size, 


            'Region_Border_Width',  oStyleItem.Style.RegionBorderWidth, 
            'Region_Border_Color',  oStyleItem.Style.RegionBorderColor,
            'Region_Border_Style',  oStyleItem.Style.RegionBorderStyle,

            'Region_Color',      oStyleItem.Style.RegionColor,
            'Region_Back_Color', oStyleItem.Style.RegionBackColor

          ]);       
     

         except
         end
                   
        end;      
    end;


//  aDataset_Styles.EnableControls;
//  aDataset_Style_items.EnableControls;

    
//  aDataset_Styles.First;

 

end;


procedure TOptionsObject.LoadFromFile(aFileName: string);
begin
   LoadFromXML( TFile.ReadAllText(aFileName));
end;


procedure TOptionsObject.LoadFromXML(aText: string);
var
  I: Integer;
 // oOptionsObject: TOptionsObject;
  oXML: TXmlDocumentEx;
//  vRoot: IXMLNode;
  vObject,vStyle: IXMLNode;

  oStyle: TOptionsObjectStyle;
  sFeature_type: string;
  sName: string;

//
//  rec: TStyleRec;
//
//  oStyle: TOptionsObjectStyle;
//
begin
  Styles.Clear;

  if aText='' then
    Exit;


  oXML:=TXmlDocumentEx.Create;
  oXML.LoadFromText(aText);

  if oXML.DocumentElement.ChildNodes.Count>0 then
  begin
    vObject:=oXML.DocumentElement.ChildNodes[0];

    ObjName      :=vObject.Attributes['name'];
    sFeature_type:=vObject.Attributes['feature_type'];

    Assert(ObjName<>'');
    Assert(sFeature_type<>'');
    
    SetFeature_type (sFeature_type);
    
  //  rec.LoadFromDB(t_Objects);
    DefaultStyle.LoadFromXML(vObject);


    for I := 0 to vObject.ChildNodes.Count-1 do       
    begin
      vStyle:=vObject.ChildNodes[i];
//      sName :=vStyle.Attributes['name'];

//      if Styles.ContainsKey(sName) then 
//        continue;
      
   
      oStyle:=TOptionsObjectStyle.Create;        
      Styles.Add( oStyle);
    
      oStyle.LoadFromXMLNode(vStyle);
     
    end;

    
//    break
  end;
    
  

  
  
  FreeAndNil(oXML);     

end;

// ---------------------------------------------------------------
procedure TOptionsObject.Remove_Disabled_Items;
// ---------------------------------------------------------------
var
  I: Integer;
  j: Integer;

  sKey: string;
  
begin
  for i:=Styles.Count-1 downto 0 do
    if Styles[i].Enabled then
    begin
    
      for j := Styles[i].Items.Count-1 downto 0 do
        if not Styles[i].Items[j].Enabled then
          Styles[i].Items.Delete(j);


       if Styles[i].Items.Count = 0 then    
         Styles.Delete(i);

    end else

    if not Styles[i].Enabled then
      Styles.Delete(i);

end;


// ---------------------------------------------------------------
function TOptionsObject.SaveToXML: string;
// ---------------------------------------------------------------
var
  i: Integer;
 // oOptionsObject: TOptionsObject;
  oXML: TXmlDocumentEx;
  vRoot: IXMLNode;
  vNode: IXMLNode;

//  rec: TStyleRec;

  oStyle: TOptionsObjectStyle;
 
  
begin
//  assert (FeatureType_str<>'');

  oXML:=TXmlDocumentEx.Create;
   

  vRoot:=oXML.DocumentElement.AddChild('OBJECT');
  vRoot.Attributes['name']        :=ObjName;
  vRoot.Attributes['feature_type']:=GetFeatureType_str;

//  rec.LoadFromDB(t_Objects);
  DefaultStyle.SaveToXML(vRoot);
  
  
  for i:=0 to Styles.Count-1 do
  begin
    vNode:=vRoot.AddChild('STYLE');
   
    Styles[i].SaveToXMLNode(vNode);
   
  end;
    
  

  Result:=oXML.SaveToText;
   
 // oXML.SaveToFile('d:\dasdas.xml');
  

  FreeAndNil(oXML);
  

end;

// ---------------------------------------------------------------
procedure TOptionsObject.SetFeature_type(aValue: string);
// ---------------------------------------------------------------
begin
  //FeatureType_str := aValue;

  if Eq(aValue,'line') then
    FeatureType:=miFeatureTypeLine  else
    
  if Eq(aValue,'Symbol') then
    FeatureType:=miFeatureTypeSymbol  else

  if Eq(aValue,'Region') then
    FeatureType:=miFeatureTypeRegion         

    // // TFeatureType = (miFeatureTypeRegion,  miFeatureTypeLine ,  miFeatureTypeSymbol);

  
end;

// ---------------------------------------------------------------
constructor TOptionsObjectStyle.Create;
// ---------------------------------------------------------------
begin
  inherited;

  Items:=TOptionsStyleItemList.Create;
 
end;

//-----------------------------------------------  
procedure TOptionsObjectStyle.SaveToXMLNode(aItem: IXMLNode);
//-----------------------------------------------
var
  vNode: IXMLNode;
  oItem: TOptionsStyleItem;
  
begin      

  aItem.Attributes['name']         :=Name;
  aItem.Attributes['Field_Name']   :=Field_Name;
  aItem.Attributes['Field_Caption']:=Field_Caption;
  aItem.Attributes['Enabled']      :=Enabled;

    
  for oItem in Items do
  begin

    vNode:=aItem.AddChild('ITEM');
//    vNode.Attributes['field_value']:=FieldValues['field_value'];
    vNode.Attributes['field_value']:=oItem.Field_Value;
    vNode.Attributes['Enabled']:=oItem.Enabled;
      
    //-----------------------------------------------  
    if Eq(Name, 'line_style') then
      vNode.Attributes['line_style']:=oItem.Style.Line_Style;//  FieldValues['line_style'];
                   
    if Eq(Name, 'line_width') then
      vNode.Attributes['line_width']:=oItem.Style.Line_Width;// FieldValues['line_width'];

    if Eq(Name, 'line_color') then
      vNode.Attributes['line_color']:=oItem.Style.Line_Color;// FieldValues['line_color'];

    //-----------------------------------------------  
    if Eq(Name, 'Symbol_font_size') then
      vNode.Attributes['Symbol_font_size']:=oItem.Style.Symbol_font_size;//FieldValues['Symbol_font_size'];
                                                 
    if Eq(Name, 'Symbol_font_color') then
      vNode.Attributes['Symbol_font_color']:=oItem.Style.Symbol_font_color;//FieldValues['Symbol_font_color'];

    if Eq(Name, 'Symbol_Character') then
    begin
      vNode.Attributes['Symbol_Character']:=oItem.Style.Symbol_Character;//FieldValues['Symbol_Character'];
      vNode.Attributes['Symbol_font_name']:=oItem.Style.Symbol_font_name;//FieldValues['Symbol_font_name'];
            
    end;      
  end; 
     
end;


constructor TOptionsStyleItem.Create;
begin
  inherited;

  Style.Set_Default();
   
end;

//-----------------------------------------------  
procedure TOptionsStyleItem.LoadFromDB(aStyleName: string; aDataset:
    TDataSet);
//-----------------------------------------------
    
begin
  with aDataset do
  begin

      Field_Value:= FieldByName('Field_value').AsString;
      Enabled    := FieldByName('Enabled').AsBoolean;
                                     

      //-----------------------------------------------  
      if Eq(aStyleName, 'line_style') then
        Style.line_style:=FieldByName('line_style').AsInteger;
                   
      if Eq(aStyleName, 'line_width') then
        Style.line_width:=FieldByName('line_width').AsInteger;

      if Eq(aStyleName, 'line_color') then
        Style.line_color:=FieldByName('line_color').AsInteger;

      //-----------------------------------------------  
      if Eq(aStyleName, 'Symbol_font_size') then
        Style.Symbol_font_size:=FieldByName('Symbol_font_size').AsInteger;
                                                 
      if Eq(aStyleName, 'Symbol_font_color') then
        Style.Symbol_font_color:=FieldByName('Symbol_font_color').AsInteger;

      if Eq(aStyleName, 'Symbol_Character') then
      begin
        Style.Symbol_Character:=FieldByName('Symbol_Character').AsInteger;
        Style.Symbol_Font_Name:=FieldByName('Symbol_font_name').AsString;
            
      end;             

   end;    
end;

// ---------------------------------------------------------------
function TOptionsObjectStyle.GetStyleKind: TStyleKind;
// ---------------------------------------------------------------
begin
  if eq(Name, 'Line_Color') then Result:=sk_Line_Color else
  if eq(Name, 'Line_Width') then Result:=sk_Line_Width else
  if eq(Name, 'Line_Style') then Result:=sk_Line_Style else

  if eq(Name, 'Symbol_Character')  then Result:=sk_Symbol_Character else
  if eq(Name, 'Symbol_Font_Color') then Result:=sk_Symbol_Font_Color else
  if eq(Name, 'Symbol_Font_Size')  then Result:=sk_Symbol_Font_Size 
               
end;

//-----------------------------------------------
procedure TOptionsObjectStyle.LoadFromXMLNode(aItem: IXMLNode);
//-----------------------------------------------
var
  i: Integer;
  vNode: IXMLNode;
  oItem: TOptionsStyleItem;
  v: Variant;
  
begin      
  Items.Clear;

  ////////////////////////////////////////
  
  Name:=         aItem.Attributes['name'];
  Field_Name:=   aItem.Attributes['Field_Name'];
  Field_Caption:=aItem.Attributes['Field_Caption'];
  Enabled:=      aItem.Attributes['Enabled'];

  

  for i := 0 to aItem.ChildNodes.Count-1 do
//  for oItem in Items do
  begin
    oItem:=TOptionsStyleItem.Create;
    Items.Add(oItem);
    
    vNode:=aItem.ChildNodes[i];

    v:=vNode.Attributes['Enabled'];
    
    oItem.Enabled    := Eq( VarToStr ( vNode.Attributes['Enabled']), 'true');
    oItem.Field_Value:=VarToStr ( vNode.Attributes['field_value'] );
      
    //-----------------------------------------------  
    if Eq(Name, 'line_style') then
      oItem.Style.Line_Style:=vNode.Attributes['line_style'] else

    if Eq(Name, 'line_width') then
      oItem.Style.Line_Width := vNode.Attributes['line_width']  else

    if Eq(Name, 'line_color') then
      oItem.Style.Line_Color := vNode.Attributes['line_color']  else

    //-----------------------------------------------  
    if Eq(Name, 'Symbol_font_size') then
      oItem.Style.Symbol_font_size:=vNode.Attributes['Symbol_font_size']  else

    if Eq(Name, 'Symbol_font_color') then
      oItem.Style.Symbol_font_color:=vNode.Attributes['Symbol_font_color']  else

    if Eq(Name, 'Symbol_Character') then
    begin
      oItem.Style.Symbol_Character:=vNode.Attributes['Symbol_Character'];
      oItem.Style.Symbol_font_name:=vNode.Attributes['Symbol_font_name'];

    end;     
  
  end;
    
  
     
end;

// ---------------------------------------------------------------
procedure TStyleRec.Assign(aStyle: CMapXStyle; aFeatureType: byte);
// ---------------------------------------------------------------
var
  
  vFontDisp: Variant;

begin   


  case aFeatureType of
    miFeatureTypeRegion: begin
                           //Region Border
                          aStyle.RegionBorderColor:=RegionBorderColor;
                          aStyle.RegionBorderStyle:=RegionBorderStyle;
                          aStyle.RegionBorderWidth:=RegionBorderWidth;

                          aStyle.RegionPattern  :=RegionPattern;
                          aStyle.RegionColor    :=RegionColor;
                          aStyle.RegionBackColor:=RegionBackColor;

                          //Region fill
                        //  RegionPattern        : integer;

//                            vFeature.Style.RegionPattern:=aItem.DefaultStyle. RegionBorderStyle;
                             
                         end;  
    miFeatureTypeLine : begin          
                           aStyle.LineColor:= Line_Color; 
                           aStyle.LineWidth:= Line_Width;
                           aStyle.LineStyle:= Line_Style;
                         end;  
    miFeatureTypeSymbol: begin
                           vFontDisp:=aStyle.SymbolFont;
    
                           aStyle.SymbolCharacter:=Symbol_Character;
                           vFontDisp.Name:=Symbol_Font_Name;
                           vFontDisp.Size:=Symbol_Font_Size;      
                                                                         
                           aStyle.SymbolFontColor:= Symbol_Font_Color;
                         end; 
      
  end;


end;

//-----------------------------------------------
procedure TStyleRec.LoadFromDB(aDataset: TDataSet);
//-----------------------------------------------  
begin

  Line_Color:=aDataset.FieldByName('Line_Color').AsInteger;
  Line_Width:=aDataset.FieldByName('Line_Width').AsInteger;
  Line_Style:=aDataset.FieldByName('Line_Style').AsInteger;

  Symbol_Character:= aDataset.FieldByName('Symbol_Character').AsInteger;
  Symbol_Font_Name := aDataset.FieldByName('Symbol_Font_Name').AsString;
  Symbol_Font_Color:= aDataset.FieldByName('Symbol_Font_Color').AsInteger;
  Symbol_Font_Size := aDataset.FieldByName('Symbol_Font_Size').AsInteger;


  RegionBorderWidth := aDataset.FieldByName('Region_Border_Width').AsInteger;
  RegionBorderColor := aDataset.FieldByName('Region_Border_Color').AsInteger;
  RegionBorderStyle := aDataset.FieldByName('Region_Border_Style').AsInteger;

  RegionPattern   := aDataset.FieldByName('Region_Pattern').AsInteger;  
  RegionColor     := aDataset.FieldByName('Region_Color').AsInteger;
  RegionBackColor := aDataset.FieldByName('Region_Back_Color').AsInteger;

end;


//-----------------------------------------------  
procedure TStyleRec.SaveToDB(aDataset: TDataSet);
//-----------------------------------------------
begin
  db_UpdateRecord__(aDataset,
  [
    'Line_Color', Line_Color,
    'Line_Width', IIF(Line_Width>0, Line_Width, 1),
    'Line_Style', Line_Style,

    'Symbol_Character',  Symbol_Character,
    'Symbol_Font_Name',  Symbol_Font_Name,
    'Symbol_Font_Color', Symbol_Font_Color,
    'Symbol_Font_Size',  IIF(Symbol_Font_Size>0, Symbol_Font_Size, 20),


    'Region_Border_Width',  IIF(RegionBorderWidth>0, RegionBorderWidth, 1),
    'Region_Border_Color',  RegionBorderColor,
    'Region_Border_Style',  RegionBorderStyle,

    'Region_Pattern',      RegionPattern,
    'Region_Color',      RegionColor,
    'Region_Back_Color', RegionBackColor

  ]);


end;



//-----------------------------------------------  
procedure TStyleRec.Set_Default;
//-----------------------------------------------
begin
  Randomize;
   
  
  Line_Color            :=Random(256*256*256); //  clRed;
  Line_Width            :=1;
  Line_Style            :=1;

  //Symbol    
  Symbol_Character      :=50;
  Symbol_Font_Name      :='Map Symbols';
    
  Symbol_Font_Color     :=Random(256*256*256); //clRed;
  Symbol_Font_Size      :=18;
 

  //Region Border
  RegionBorderWidth    :=1;
  RegionBorderStyle    :=1;
  RegionBorderColor    :=clRed;

  //Region fill
  RegionPattern   :=1;
  RegionColor    :=clNavy;  //���� ����� ��� �����
  RegionBackColor:=clBlue;



end;



//-----------------------------------------------  
procedure TStyleRec.LoadFromXML(aItem: IXMLNode);
//-----------------------------------------------  
begin
  Line_Color:=aItem.Attributes['LineColor'];
  Line_Width:=aItem.Attributes['LineWidth'];
  Line_Style:=aItem.Attributes['LineStyle'];
  
  Symbol_Character:= aItem.Attributes['SymbolCharacter'];
  Symbol_Font_Name := aItem.Attributes['SymbolFontName']; 
  Symbol_Font_Color:= aItem.Attributes['SymbolFontColor'];
  Symbol_Font_Size := aItem.Attributes['SymbolFontSize']; 


  RegionBorderWidth := aItem.Attributes['RegionBorderWidth'];
  RegionBorderColor := aItem.Attributes['RegionBorderColor'];
  RegionBorderStyle := aItem.Attributes['RegionBorderStyle'];
  
  
  RegionPattern   := aItem.Attributes['RegionPattern'] ;
  RegionColor     := aItem.Attributes['RegionColor'] ;
  RegionBackColor := aItem.Attributes['RegionBackColor'];

end;

//-----------------------------------------------  
procedure TStyleRec.SaveToXML(aItem: IXMLNode);
//-----------------------------------------------  
begin
  aItem.Attributes['LineColor']:=Line_Color;
  aItem.Attributes['LineWidth']:=Line_Width;
  aItem.Attributes['LineStyle']:=Line_Style;
  
  aItem.Attributes['SymbolCharacter']:=Symbol_Character;
  aItem.Attributes['SymbolFontName']:=Symbol_Font_Name;
  aItem.Attributes['SymbolFontColor']:=Symbol_Font_Color;
  aItem.Attributes['SymbolFontSize']:=Symbol_Font_Size;


  aItem.Attributes['RegionBorderWidth']:=RegionBorderWidth;
  aItem.Attributes['RegionBorderColor']:=RegionBorderColor;
  aItem.Attributes['RegionBorderStyle']:=RegionBorderStyle;
  
  aItem.Attributes['RegionPattern']:=RegionPattern;
  aItem.Attributes['RegionColor']:=RegionColor;
  aItem.Attributes['RegionBackColor']:=RegionBackColor;
          
   

end;

// ---------------------------------------------------------------
function TOptionsStyleItemList.FindByValue(aValue: string): TOptionsStyleItem;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Result:=nil;

  for I := 0 to Count-1 do
    if Eq(Items[i].Field_Value, aValue) then
    begin
      Result:=Items[i];
      Exit;
    end;  
    
end;



end.



