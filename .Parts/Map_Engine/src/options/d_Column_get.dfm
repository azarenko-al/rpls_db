object dlg_Column_get: Tdlg_Column_get
  Left = 400
  Top = 336
  BorderWidth = 5
  Caption = 'dlg_Column_get'
  ClientHeight = 522
  ClientWidth = 1136
  Color = clBtnFace
  Constraints.MaxWidth = 1154
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  DesignSize = (
    1136
    522)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 0
    Top = 489
    Width = 1136
    Height = 33
    Align = alBottom
    Shape = bsTopLine
    ExplicitTop = 456
    ExplicitWidth = 1054
  end
  object cxGrid1_names: TcxGrid
    Left = 0
    Top = 0
    Width = 457
    Height = 489
    Align = alLeft
    TabOrder = 2
    object cxGrid1_namesDBTableView1_params: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ds_ADOStoredProc1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      object cxGrid1_namesDBTableView1_paramscaption: TcxGridDBColumn
        DataBinding.FieldName = 'caption'
        Width = 116
      end
      object cxGrid1_namesDBTableView1_paramsname: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Visible = False
        Width = 115
      end
      object cxGrid1_namesDBTableView1_paramsColumn1: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Width = 86
      end
      object cxGrid1_namesDBTableView1_paramsColumn2: TcxGridDBColumn
        DataBinding.FieldName = 'type'
        Visible = False
        Width = 52
      end
      object cxGrid1_namesDBTableView1_paramsColumn3: TcxGridDBColumn
        DataBinding.FieldName = 'xref_TableName'
        Visible = False
        Width = 74
      end
    end
    object cxGrid1_namesLevel1: TcxGridLevel
      GridView = cxGrid1_namesDBTableView1_params
    end
  end
  object cxGrid2_values: TcxGrid
    Left = 465
    Top = 0
    Width = 304
    Height = 489
    Align = alLeft
    TabOrder = 3
    object cxGridDBTableView1_values: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ds_ADOStoredProc2_values
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      object cxGridDBTableView1_valuesname: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Width = 109
      end
      object cxGridDBTableView1_valuesColumn1: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Width = 80
      end
      object cxGridDBTableView1_valuesColumn2: TcxGridDBColumn
        DataBinding.FieldName = 'code'
        Visible = False
        Width = 85
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1_values
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 457
    Top = 0
    Width = 8
    Height = 489
    Control = cxGrid1_names
  end
  object Button2: TButton
    Left = 1061
    Top = 497
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = #1047#1072#1082#1088#1099#1090#1100
    ModalResult = 8
    TabOrder = 1
  end
  object Button1: TButton
    Left = 973
    Top = 497
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = #1042#1099#1073#1088#1072#1090#1100
    ModalResult = 1
    TabOrder = 0
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = ADOConnection2
    CursorType = ctStatic
    LockType = ltReadOnly
    AfterScroll = ADOStoredProc1AfterScroll
    ProcedureName = 'map.sp_options_object_list_names'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@OBJNAME'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = 'property'
      end>
    Left = 864
    Top = 112
  end
  object ds_ADOStoredProc1: TDataSource
    DataSet = ADOStoredProc1
    Left = 866
    Top = 168
  end
  object ADOConnection2: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Password=sa;Persist Security Info=True;User I' +
      'D=sa;Data Source=onega_link'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'MSDASQL.1'
    Left = 864
    Top = 32
  end
  object ds_ADOStoredProc2_values: TDataSource
    DataSet = ADOStoredProc_Values
    Left = 1002
    Top = 168
  end
  object cxPropertiesStore1: TcxPropertiesStore
    Components = <>
    StorageName = 'cxPropertiesStore1'
    StorageType = stRegistry
    Left = 864
    Top = 256
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredValues = <>
    Left = 1000
    Top = 32
  end
  object ADOStoredProc_Values: TADOStoredProc
    Connection = ADOConnection2
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'map.sp_options_object_list_values'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1394
      end>
    Left = 1000
    Top = 112
  end
end
