object SymbolSelectDialog: TSymbolSelectDialog
  Left = 1039
  Top = 311
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderStyle = bsDialog
  Caption = 'dlg_Font_Select_Char'
  ClientHeight = 591
  ClientWidth = 655
  Color = clBtnFace
  Constraints.MaxHeight = 619
  Constraints.MaxWidth = 663
  Constraints.MinHeight = 100
  Constraints.MinWidth = 100
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Scaled = False
  OnCreate = FormCreate
  OnDeactivate = FormDeactivate
  OnKeyDown = FormKeyDown
  DesignSize = (
    655
    591)
  PixelsPerInch = 96
  TextHeight = 13
  object StringGrid1: TStringGrid
    Left = 0
    Top = 29
    Width = 655
    Height = 500
    Align = alTop
    ColCount = 16
    DefaultColWidth = 32
    DefaultRowHeight = 32
    FixedCols = 0
    RowCount = 14
    FixedRows = 0
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnSelectCell = StringGrid1SelectCell
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 572
    Width = 655
    Height = 19
    Panels = <>
  end
  object Button1: TButton
    Left = 484
    Top = 543
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = #1042#1099#1073#1088#1072#1090#1100
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object Button2: TButton
    Left = 572
    Top = 543
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = #1047#1072#1082#1088#1099#1090#1100
    ModalResult = 8
    TabOrder = 3
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 655
    Height = 29
    BorderWidth = 2
    ButtonHeight = 20
    Caption = 'ToolBar1'
    TabOrder = 4
    object FontComboBox1: TFontComboBox
      Left = 0
      Top = 0
      Width = 281
      Height = 20
      TabOrder = 0
      OnChange = FontComboBox1Change
    end
  end
  object FormPlacement1: TFormPlacement
    UseRegistry = True
    Left = 96
    Top = 64
  end
end
