unit u_Theme;

interface


uses
  Windows, ActiveX, Classes, ComObj, SysUtils, Variants, Forms,  Graphics,
  CodeSiteLogging,  Dialogs, ComServ, 
  MapXLib_TLB,    

  u_MapX,
  
  u_Options_classes,

  StdVcl;   


type
  TThemeMaker = class   
  private
    procedure InsertFields(aItem: TOptionsObject; aDataSet: CMapXDataset);
    procedure UpdateLayerFeatures(aItem: TOptionsObject; aLayer: CMapxLayer); 
    procedure SetDefault(aItem: TOptionsObject; aLayer: CMapxLayer);
  public
    procedure Apply_Theme(const aMap: CMapX; aDict: TOptionsObjectDict);
    procedure Apply_Theme_for_object(const aMap: CMapX; aObjName, aStyle_XML:  string);
  end;


implementation



// ---------------------------------------------------------------
procedure TThemeMaker.Apply_Theme(const aMap: CMapX; aDict: TOptionsObjectDict);
// ---------------------------------------------------------------
var
  I: Integer;
  k: Integer;

  oItem: TOptionsObject;
  s: string;
  s11: string;
  vLayer: CMapxLayer;

  vDataSet: CMapXDataset;

  vAllFeatures: CMapXFeatures;
  vFeature: CMapXFeature;

  
begin   

  assert(Assigned(aDict));
      
  

  for I := 1 to aMap.Layers.Count do
  begin
    vLayer:=aMap.Layers[i];


 //  s:=vLayer.FileSpec;

//    
//  //----------------------
//  vAllFeatures:=vLayer.AllFeatures;
//  k:=vAllFeatures.Count;
//  vFeature:=vAllFeatures.Item[1];
//  k:=vFeature.type_;
//  //----------------------------------
//


    
    if vLayer.type_ <> miLayerInfoTypeTab then
      Continue;

      
   if Pos('~',vLayer.FileSpec)=0 then
      Continue;
   
   

    
    s:= ChangeFileExt( ExtractFileName (vLayer.FileSpec),'');

    if s[1]='~' then
      s:=Copy(s,2, 100);
    


   CodeSite.Send('vLayer.FileSpec ' + vLayer.FileSpec);


   if not aDict.TryGetValue(s, oItem) then 
   begin 
     CodeSite.Send('if not aDict.TryGetValue(s, oItem) - ' + s);

     Continue;   
   end;
   

  // CodeSite.Send('1');
    

    oItem.Remove_Disabled_Items;


    k:=oItem.Styles.Count;
                                   
//    oItem:=aDict[s];
    
//    s11:=oItem.ObjectName;
    
    
//    if Assigned(oItem) then    
 //   begin
 
//     SetDefault (oItem, vLayer);             
 

     k:=vLayer.DataSets.Count;
       
     if vLayer.DataSets.Count=0 then
       vDataSet:=aMap.DataSets.Add (miDataSetLayer, vLayer, EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam)                                       
     else 
       vDataSet:=vLayer.DataSets[vLayer.DataSets.Count];

 //  CodeSite.Send('2');
       

      Assert(Assigned(vDataSet));

//   CodeSite.Send('3');

k:=vDataSet.Themes.Count;

          
       vDataSet.Themes.RemoveAll;
    //  end;
                                             
//    end;

            
//   CodeSite.Send('4');



            
//       k:=oItem.Fields.Count;

       if oItem.Styles.Count=0 then
       begin
         CodeSite.Send('oItem.Styles.Count=0 ');

         SetDefault (oItem, vLayer);             

       end;  

       //-------------------------  

            
       if oItem.Styles.Count=1 then
       begin
         CodeSite.Send('oItem.Styles.Count=1 ');

         InsertFields (oItem, vDataSet);
       end else   
         
       if oItem.Styles.Count>1 then
       begin
         CodeSite.Send('oItem.Styles.Count - ' + IntToStr(oItem.Styles.Count) );

         UpdateLayerFeatures (oItem, vLayer);
       end;  
                 
  end;
    

    
//  aMap.refresh;
    

end;




// ---------------------------------------------------------------
procedure TThemeMaker.SetDefault(aItem: TOptionsObject; aLayer: CMapxLayer);
// ---------------------------------------------------------------
var
  //CMapXStyle
  
   rStyle: TmiStyleRec;
   
begin
  CodeSite.Send('procedure TThemeMaker.SetDefault(aItem: TOptionsObject; aLayer: CMapxLayer); - start');

  Assert ( Assigned(aItem));
  Assert ( Assigned(aLayer));
  

  CodeSite.Send('1');
  
//    if Assigned(oItem) then
//    begin
    FillChar(rStyle,SizeOf(rStyle),0);

    
    // // TFeatureType = (miFeatureTypeRegion,  miFeatureTypeLine ,  miFeatureTypeSymbol);


    case aItem.FeatureType of
      miFeatureTypeSymbol: begin                          

              rStyle.SymbolFontName :=aItem.DefaultStyle.Symbol_Font_Name;
              rStyle.SymbolCharacter:=aItem.DefaultStyle.Symbol_Character;
  
              rStyle.SymbolFontColor:=aItem.DefaultStyle.Symbol_Font_Color;
              rStyle.SymbolFontSize :=aItem.DefaultStyle.Symbol_Font_Size;

         //     rStyle.
                 //            vFeature.Style.SymbolFontHalo:= True;
                  //           vFeature.Style.SymbolFontShadow:= True;
              


              
//                rStyle.SymbolFontIsHalo:=aItem.DefaultStyle.Symbol_FontIsHalo;
                

    //  CodeSite.Send( 'vFontDisp.Name1: '+ vFontDisp.Name);
                                                                            
              mapx_SetMapXLayertStyle(aLayer, miFeatureTypeSymbol, rStyle);
                                    
            end;


      miFeatureTypeLine: begin  
             rStyle.LineColor:=aItem.DefaultStyle.Line_Color;
             rStyle.LineStyle:=aItem.DefaultStyle.Line_Style;
             rStyle.LineWidth:=aItem.DefaultStyle.Line_Width;
             
             mapx_SetMapXLayertStyle(aLayer, miFeatureTypeLine, rStyle);

           end;
                    
    //  dtAsVector: ;
      miFeatureTypeRegion: begin

            rStyle.RegionBorderColor:=aItem.DefaultStyle.RegionBorderColor;//  mapx_MakeColor (aStyle.RegionBorderColor);
            rStyle.RegionBorderWidth:=aItem.DefaultStyle.RegionBorderWidth;//aStyle.RegionBorderWidth;
            rStyle.RegionBorderStyle:=aItem.DefaultStyle.RegionBorderStyle;//aStyle.RegionBorderStyle;

            rStyle.RegionPattern    :=aItem.DefaultStyle.RegionPattern;   //aStyle.RegionPattern;
            rStyle.RegionBackColor  :=aItem.DefaultStyle.RegionBackColor; //aStyle.RegionBackColor;
            rStyle.RegionColor      :=aItem.DefaultStyle.RegionColor;     //aStyle.RegionColor;

            mapx_SetMapXLayertStyle(aLayer, miFeatureTypeRegion, rStyle);
                       
        end;
    end;
//  end;
end;



// ---------------------------------------------------------------
procedure TThemeMaker.InsertFields(aItem: TOptionsObject; aDataSet:  CMapXDataset);
// ---------------------------------------------------------------
var
  j,i: Integer;
  k: Integer;

  vFontDisp: Variant;

  vCategory: CMapXIValueCategory;
  sValue, sField: string;
  vTheme: CMapXTheme;
  s: string;
  vField: CMapXField;
  oStyle: TOptionsObjectStyle;
  
  oStyleItem: TOptionsStyleItem;
  
begin

 

//  aDataSet.Themes.RemoveAll;
//  SetDefault (aItem, vLayer);             
  

  CodeSite.Send('aDataSet.Fields count ' + IntToStr (aDataSet.Fields.Count));

  for i := 1 to aDataSet.Fields.Count do
    CodeSite.Send('aDataSet.Fields[i].Name ' + aDataSet.Fields.Item[i].Name);
      
    
    

     if aItem.Styles.Count > 0 then
//     for I := 0 to aItem.Styles.Count-1 do
       
     begin
       oStyle:=aItem.Styles[0];
//       oStyle:=aItem.Styles[i];
      
       sField:=oStyle.Field_Name;


       CodeSite.Send('oStyle.FieldName ' + sField);
         
       
       k:=mapx_GetFieldIndex_In_Fields (sField, aDataSet.Fields );
         
       if k<=0 then
       begin
        // ShowMessage('Error - field not found: '+ sField);
         CodeSite.SendError(sField);

         Exit;
       end;
         
        try       
          vField:=aDataSet.Fields.Item[sField];
        except      
          CodeSite.SendError('vField:=aDataSet.Fields.Item[sField];  - ' + sField); 

          //ShowMessage('error'+  sField);
          Exit;  
        end;                  

    

         
       Assert(Assigned(vField));
         
       s:=vField.Name;
       
       vTheme:= aDataSet.Themes.Add(miThemeIndividualValue, aDataSet.Fields.Item[sField], EmptyParam, false);
       Assert(Assigned(vTheme));

       vTheme.Visible := True;
       vTheme.Legend.Visible := True;

     //  vTheme.Legend.LegendTexts.AutoGenerate;
//         vTheme.Legend.LegendTexts.
         
       //  vTheme.Legend.Visible := False;
       vTheme.AutoRecompute := True;
       vTheme.Legend.LegendTexts.AutoGenerate:= true;


       Assert (oStyle.Items.Count>0);
       vTheme.ThemeProperties.NumRanges := oStyle.Items.Count;
  

      // vTheme.ThemeProperties.DistMethod

    //   vCategory := vTheme.ThemeProperties. IndividualValueCategories.Item[j+1];

                                   

        vCategory := vTheme.ThemeProperties.IndividualValueCategories.AllOthersCategory;          
    //    vCategory.Style:=aDataSet.Layer.Style.Clone;

        aItem.DefaultStyle.Assign(vCategory.Style, aItem.FeatureType);
        
           
//          vCategory.Style.SymbolFontColor:= clNavy;

          
//          vCategory.Style.LineColor := mapx_MakeColor (aItem.DefaultStyle.LineColor);  // clred;// clRed;
//          vCategory.Style.LineWidth := aItem.DefaultStyle.LineWidth; //5;// clRed;
//          vCategory.Style.LineStyle := aItem.DefaultStyle.LineStyle; //1;// clRed;

         
          
//          vCategory.Style.

//
//          th.Visible := True;
//          th.Legend.Visible := False;
//          th.AutoRecompute:= True;
//         

         
         
       //oList:=aItem.Fields[i];
            
       for j := 0 to oStyle.Items.Count-1 do
       begin
         oStyleItem:=oStyle.Items[j];

         sValue:=oStyleItem.Field_Value;
                                        

         vCategory := vTheme.ThemeProperties.IndividualValueCategories.Item[j+1];
         vCategory.Style:=aDataSet.Layer.Style.Clone;
//         vCategory.
         
          
         vCategory.Value := sValue; 

         vFontDisp:=vCategory.Style.SymbolFont;

         vCategory.Style.SymbolFontHalo:= True;

          
         case oStyle.GetStyleKind of

            sk_Line_Color: vCategory.Style.LineColor:=oStyleItem.Style.Line_Color;                           
            sk_Line_Width: vCategory.Style.LineWidth:=oStyleItem.Style.Line_Width;                  
            sk_Line_Style: vCategory.Style.LineStyle:=oStyleItem.Style.Line_Style;


            sk_Symbol_Character : begin
                                    vCategory.Style.SymbolCharacter:=oStyleItem.Style.Symbol_Character;
                                    vFontDisp.Name:=oStyleItem.Style.Symbol_Font_Name;
                                  end;  
                                  
            sk_Symbol_Font_Color  : vCategory.Style.SymbolFontColor:= oStyleItem.Style.Symbol_Font_Color;
            sk_Symbol_Font_Size   : vFontDisp.Size:=oStyleItem.Style.Symbol_Font_Size;
         
    
       end;    
                                   

       end;         

   end;    
end;  

// ---------------------------------------------------------------
procedure TThemeMaker.UpdateLayerFeatures(aItem: TOptionsObject; aLayer:  CMapxLayer);
// ---------------------------------------------------------------
var
  j,i: Integer;
  k: Integer;

  vFontDisp: Variant;

  vCategory: CMapXIValueCategory;
  sValue, sField: string;
 
  s: string;
  vField: CMapXField;


  vFeature:  CMapXFeature;
  
  oStyle: TOptionsObjectStyle;
  
  oStyleItem: TOptionsStyleItem;
             
  v: Variant;

  vDataSet: CMapXDataset;
  
  vRowValues: CMapXRowValues;
  
  vAllFeatures: CMapXFeatures;

   
  
begin

  aLayer.BeginAccess(miAccessReadWrite);

  vDataSet:=aLayer.DataSets[aLayer.DataSets.Count];
                     
  vAllFeatures:=aLayer.AllFeatures;

  k:=vAllFeatures.Count;
  
  for i:=1 to vAllFeatures.Count do
  begin
    vFeature:=vAllFeatures[i];

//    k:=vFeature.type_;
                                                                   
    vRowValues:=vDataset.RowValues[vFeature];

   k:=vFeature.type_;
                


                
//
//  // feature types
//  miFeatureTypeRegion  = 0;
//  miFeatureTypeLine    = 1;
//  miFeatureTypeSymbol  = 2;
//  miFeatureTypeMixed   = 3;
//  miFeatureTypeUnknown = 4;
//  miFeatureTypeText    = 5;
//  miFeatureTypeNull    = 6;


                
    case vFeature.type_ of

      miFeatureTypeLine : begin          
                             vFeature.Style.LineColor:= aItem.DefaultStyle.Line_Color; 
                             vFeature.Style.LineWidth:= aItem.DefaultStyle.Line_Width;
                             vFeature.Style.LineStyle:= aItem.DefaultStyle.Line_Style;
                           end;
                           
      miFeatureTypeSymbol: begin

                        
      
                             vFontDisp:=vFeature.Style.SymbolFont;


//                             CodeSite.Send('miFeatureTypeSymbol - 1 - ' + IntToStr(aItem.DefaultStyle.Symbol_Character));

                             vFeature.Style.SymbolCharacter:=aItem.DefaultStyle.Symbol_Character;

                             vFeature.Style.SymbolFontColor:= aItem.DefaultStyle.Symbol_Font_Color;

                             vFeature.Style.SymbolFontHalo:= True;
                             vFeature.Style.SymbolFontShadow:= True;
                             
                             
                             vFontDisp.Name:=aItem.DefaultStyle.Symbol_Font_Name;
                             vFontDisp.Size:=aItem.DefaultStyle.Symbol_Font_Size;      

                           
                                                                         
                           end; 

      miFeatureTypeRegion: begin
                             //Region Border
                            vFeature.Style.RegionBorderColor:=aItem.DefaultStyle.RegionBorderColor;
                            vFeature.Style.RegionBorderStyle:=aItem.DefaultStyle.RegionBorderStyle;
                            vFeature.Style.RegionBorderWidth:=aItem.DefaultStyle.RegionBorderWidth;


                            //Region fill
                          //  RegionPattern        : integer;
                            vFeature.Style.RegionPattern  :=aItem.DefaultStyle.RegionPattern;
                            vFeature.Style.RegionColor    :=aItem.DefaultStyle.RegionColor;
                            vFeature.Style.RegionBackColor:=aItem.DefaultStyle.RegionBackColor;

//                            vFeature.Style.RegionPattern:=aItem.DefaultStyle. RegionBorderStyle;
                             
                           end;  
      
    end;


// CodeSite.Send('3');

    
    
    
     for j := 0 to aItem.Styles.Count-1 do       
     begin
      // CodeSite.Send('4');


       oStyle:=aItem.Styles[j];      
       sField:=oStyle.Field_Name;
                       
       CodeSite.Send('oStyle.Field_Name - '+ oStyle.Field_Name);
                       
       k:=mapx_GetFieldIndex_In_Fields (sField, vDataSet.Fields );
         

     //  CodeSite.Send('5');

       if k<=0 then
       begin
         ShowMessage('Error - field not found: '+ sField);
         CodeSite.SendError(sField);

         Exit;
       end;
         
        try       
          vField:=vDataSet.Fields.Item[sField];
        except      
          ShowMessage('error'+  sField);
          Continue;  
        end;

       CodeSite.Send('6');

 
        v:=vRowValues.Item[vField].Value;

        //--------------------------------  

       CodeSite.Send(oStyle.Name);

       if Not Assigned (oStyle) then
         ShowMessage('Not Assigned (oStyle)');
       
        assert (Assigned (oStyle));

        s:= VarToStr(v);
        CodeSite.Send('VarToStr(v)- ' + s);

        oStyleItem:=oStyle.Items.FindByValue (s);
        if not Assigned(oStyleItem) then
        begin
          CodeSite.Send('Continue');

          Continue;
        
        end;
          
        CodeSite.Send('7');


        assert (oStyleItem.Style.Line_Width>0);
          

        vFontDisp:=vFeature.Style.SymbolFont;  

        vFeature.Style.SymbolFontHalo:= True;
                               

        case oStyle.GetStyleKind of  
        
          sk_Line_Color: vFeature.Style.LineColor:=oStyleItem.Style.Line_Color;                                                    
          sk_Line_Width: vFeature.Style.LineWidth:=oStyleItem.Style.Line_Width;                            
          sk_Line_Style: vFeature.Style.LineStyle:=oStyleItem.Style.Line_Style;
        

          sk_Symbol_Character : begin
                                  vFeature.Style.SymbolCharacter:=oStyleItem.Style.Symbol_Character;
                                  vFontDisp.Name:=oStyleItem.Style.Symbol_Font_Name;
                                end;  
                                  
          sk_Symbol_Font_Color  : vFeature.Style.SymbolFontColor:= oStyleItem.Style.Symbol_Font_Color;
          sk_Symbol_Font_Size   : vFontDisp.Size:=oStyleItem.Style.Symbol_Font_Size;
        
        
        end;           

     end;  

     
//     vFeature.Style.LineColor:=clNavy; 
//     vFeature.Style.LineWidth:=3; 
//     vFeature.Style.LineStyle:=3;

    
     vFeature.Update(EmptyParam, EmptyParam);
       
  end;  


  aLayer.EndAccess(miAccessEnd);


end;  


// ---------------------------------------------------------------
procedure TThemeMaker.Apply_Theme_for_object(const aMap: CMapX; aObjName,
    aStyle_XML: string);
// ---------------------------------------------------------------
var
  oObj: TOptionsObject;
  oDict: TOptionsObjectDict;
 // sName: string;
  sStyle: string;   
      
begin
 // CodeSite.Send('procedure TThemeMaker.Apply_Theme_for_object - 1');

  oDict:=TOptionsObjectDict.Create;

//  CodeSite.Send('oDict:=TOptionsObjectDict.Create');
  
  
  oObj:=TOptionsObject.Create;

//  CodeSite.Send('1');

  oObj.LoadFromXML(aStyle_XML);

 // CodeSite.Send('2');
  
  oDict.Add(aObjName, oObj);

  Apply_Theme(aMap, oDict);
            
  
  FreeAndNil(oDict);
end;




end.


