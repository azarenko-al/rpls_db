unit d_Column_get;

interface

uses
  u_db,
  u_cx,

 
  CodeSiteLogging,

  System.SysUtils, System.Variants, System.Classes, 
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, 
  Data.DB, Data.Win.ADODB, RxPlacemnt, 
  
  
  cxGridDBTableView, cxGridLevel, 
  cxGrid, Vcl.ExtCtrls, cxSplitter, 
  
  
  cxPropertiesStore, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxClasses, cxGridCustomView;

type
  Tdlg_Column_get = class(TForm)
    Button1: TButton;
    Button2: TButton;
    ADOStoredProc1: TADOStoredProc;
    ds_ADOStoredProc1: TDataSource;
    ADOConnection2: TADOConnection;
    ds_ADOStoredProc2_values: TDataSource;
    cxGrid1_names: TcxGrid;
    cxGrid1_namesDBTableView1_params: TcxGridDBTableView;
    cxGrid1_namesLevel1: TcxGridLevel;
    cxGrid1_namesDBTableView1_paramsname: TcxGridDBColumn;
    cxGrid1_namesDBTableView1_paramscaption: TcxGridDBColumn;
    cxGrid2_values: TcxGrid;
    cxGridDBTableView1_values: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1_valuesname: TcxGridDBColumn;
    Bevel1: TBevel;
    cxSplitter1: TcxSplitter;
    cxGrid1_namesDBTableView1_paramsColumn1: TcxGridDBColumn;
    cxGridDBTableView1_valuesColumn1: TcxGridDBColumn;
    cxGridDBTableView1_valuesColumn2: TcxGridDBColumn;
    cxGrid1_namesDBTableView1_paramsColumn2: TcxGridDBColumn;
    cxPropertiesStore1: TcxPropertiesStore;
    FormStorage1: TFormStorage;
    cxGrid1_namesDBTableView1_paramsColumn3: TcxGridDBColumn;
    ADOStoredProc_Values: TADOStoredProc;
//    procedure FormDestroy(Sender: TObject);
    procedure ADOStoredProc1AfterScroll(DataSet: TDataSet);
//    procedure cxGrid1_namesDBTableView1_paramsSelectionChanged(Sender:
  //      TcxCustomGridTableView);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FDataset_values: Data.DB.TDataSet;
    { Private declarations }
  public

    class function ExecDlg(aConnectionStr, aObjName: string; var aField_Name,
        aField_caption: string; aSList: TStrings): Boolean;

  end;


 
implementation

{$R *.dfm}


 
procedure Tdlg_Column_get.ADOStoredProc1AfterScroll(DataSet: TDataSet);
begin
  db_StoredProc_Open(ADOStoredProc_Values, 
    ADOStoredProc_Values.ProcedureName, ['id', ADOStoredProc1['id']]) ;
end;

 

// ---------------------------------------------------------------
procedure Tdlg_Column_get.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
//  ADOStoredProc1.AfterScroll:=nil;
  
  if ADOConnection2.Connected then
    ShowMessage('procedure Tdlg_Column_get.FormCreate(Sender: TObject); ');

  FDataset_values:=ADOStoredProc_Values;

//  ADOQuery1_values.SQL.Text:='exec  map.sp_options_object_list_values  :id';


  Caption:='����� ��������';

  cxGrid2_values.Align:=alClient;
    
  Width:=800;



  CodeSite.Send('procedure Tdlg_Column_get.FormCreate(Sender: TObject);');
                          
     
  cx_SetColumnCaptions1(cxGrid1_namesDBTableView1_params,
  [
    'caption', '�������� ���������' 
            
  ]);   

  cx_SetColumnCaptions1(cxGridDBTableView1_values,
  [
    'name', '��������' 
            
  ]);   

  CodeSite.Send('procedure Tdlg_Column_get.FormCreate(Sender: TObject);  end');
   

//  TcxGridStorageOption = (gsoUseFilter, gsoUseSummary);
//  TcxGridStorageOptions = set of TcxGridStorageOption;

   
//  cxGridDBTableView1_values.RestoreFromStorage(cxGridDBTableView1_values.Name, cxPropertiesStore1);
   
end;

//------------------------------------------------------------------------------
class function Tdlg_Column_get.ExecDlg(aConnectionStr, aObjName: string; var
    aField_Name, aField_caption: string; aSList: TStrings): Boolean;
//------------------------------------------------------------------------------

begin      
  with Tdlg_Column_get.Create(Application) do
  begin
    Assert (Assigned (aSList));
          
    db_OpenADOConnectionString(ADOConnection2, aConnectionStr);

    CodeSite.Send('class function Tdlg_Column_get.ExecDlg(aConnectionStr, aObjName: string; var');

    //ADOQuery1_values.Open;
    
    ADOStoredProc1.AfterScroll:=nil;
    
    db_StoredProc_Open(ADOStoredProc1, ADOStoredProc1.ProcedureName, ['ObjName', aObjName]) ;

 
    
//    CodeSite.Send('1');

  //  ADODataSet1_values.OPen;

    if aField_Name<>'' then
      ADOStoredProc1.Locate('Field_Name', aField_Name, []);

  //  CodeSite.Send('2');
     
      
    Result:=ShowModal=mrOk;
    
    if Result then
    begin
      aField_Name   :=ADOStoredProc1 ['field_name'];
      aField_caption:=ADOStoredProc1['caption'];

      FDataset_values.DisableControls;
      FDataset_values.First;
      
      WITh FDataset_values do
        while not eof do
        begin
          aSList.Add( FieldByName('name').AsString );
          
          Next;
        end;        
    end; 

    ADOConnection2.Close;
    

 //   ADOStoredProc1.Close;
  //  ADODataSet1_values.Close;
     
      
    Free;
  end;

end;


procedure Tdlg_Column_get.FormActivate(Sender: TObject);
begin
  ADOStoredProc1.AfterScroll:=ADOStoredProc1AfterScroll;
  ADOStoredProc1AfterScroll(nil);

end;

end.
