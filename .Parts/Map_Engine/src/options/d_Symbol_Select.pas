unit d_Symbol_Select;

interface

uses
  
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, ActnList, ExtCtrls,  ComCtrls, Vcl.StdCtrls, RxCombos, 
  RxPlacemnt, Vcl.ToolWin;

type
  TOnSymbolOverEvent = procedure(Sender: TObject; Symbol: Char) of object;

  TSymbolSelectDialog = class(TForm)
    StringGrid1: TStringGrid;
    StatusBar1: TStatusBar;
    Button1: TButton;
    Button2: TButton;
    ToolBar1: TToolBar;
    FontComboBox1: TFontComboBox;
    FormPlacement1: TFormPlacement;
    procedure FontComboBox1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);

    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

   
    procedure StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
  private
    FCharCode: byte;
    FRowCount,
    FColCount: Integer;

  //  OnSymbolOver  : TOnSymbolOverEvent;


    procedure SetSelectedSymbol(const Value: byte);
    procedure SetFontName(const Value: String);
  //  procedure Paint; override;

  public
  //  OnSymbolSelect: TNotifyEvent;
      
    class function ExecDlg(var aFontName: String; var aCharCode: byte): Boolean;

//    property FontName: String write SetFontName;
    property CharCode: byte read FCharCode write SetSelectedSymbol;

  end;


//============================================================================//
//============================================================================//
implementation  {$R *.DFM}



//------------------------------------------------------------------------------
class function TSymbolSelectDialog.ExecDlg(var aFontName: String; var
    aCharCode: byte): Boolean;
//------------------------------------------------------------------------------

begin      
  with TSymbolSelectDialog.Create(Application) do
  begin
  //  ADOStoredProc1.Connection:=aADOConnection;
    if aFontName='' then
      aFontName:='Map Symbols';
  
//    FontName:=aFontName;
    CharCode:=aCharCode;
                         
    FontComboBox1.FontName:=aFontName;
      
    SetFontName(aFontName);

    Result:=ShowModal=mrOk;
    
    if Result then
    begin
      aFontName:=FontComboBox1.FontName; //StringGrid1.Font.Name;
      aCharCode:=CharCode;

      
    end;  
      
    Free;
  end;

end;

procedure TSymbolSelectDialog.FontComboBox1Change(Sender: TObject);
begin                               
  SetFontName(FontComboBox1.FontName);  
end;


procedure TSymbolSelectDialog.FormCreate(Sender: TObject);
begin
  Caption:='����� �������';
  FRowCount:=14;
  FColCount:=16;

 // StringGrid1.Align:=alClient;
end;


//-------------------------------------------------------------------
//  ���������������� �� ������������ �������
//-------------------------------------------------------------------
procedure TSymbolSelectDialog.SetSelectedSymbol(const Value: byte);
//-------------------------------------------------------------------
var
  I: Integer;
begin
  FCharCode:=Value;
  if FCharCode<32 then
   FCharCode:=32;
    
  with StringGrid1 do
  begin
    I  :=(FCharCode-32) div FColCount; // ������
    Col:=(ord(FCharCode)-32) mod FColCount; // �������

    if (I>RowCount-1) then 
      Row:=0 
    else 
      Row:=I;
  end;
end;


procedure TSymbolSelectDialog.FormDeactivate(Sender: TObject);
begin
  Hide;
end;

//-------------------------------------------------------------------
// ���������� ����� ���������
//-------------------------------------------------------------------
procedure TSymbolSelectDialog.SetFontName(const Value: String);
var
  Sym,r,c: Integer;
begin
 // Canvas.Font.Name:=Value;
//  Canvas.Font.Size:=18;

  if Value<>'' then
    StringGrid1.Font.Name := Value;
    
  StringGrid1.Font.Size := 18;
  Sym:=32;

  with StringGrid1 do
    for r := 0 to RowCount - 1 do
      for c := 0 to ColCount - 1 do
      begin
        Cells[c, r]:=Char(Sym);
        inc(Sym);
      end;
end;



procedure TSymbolSelectDialog.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key=VK_ESCAPE then
    Hide else
end;

procedure TSymbolSelectDialog.StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  FCharCode:=32+aRow*FColCount+aCol;
 // Hide;

//  if Assigned(OnSymbolSelect) then
//    OnSymbolSelect(Self);
end;


end.
