unit dm_Main_options1111111;

interface

uses
  u_func,

  u_db,
  

  
  
  System.SysUtils, IOUtils, System.Classes, Data.DB, Data.Win.ADODB;

type
  TdmMain_options = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure Open;
    procedure View(aObjName: string);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmMain_options: TdmMain_options;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmMain_options.DataModuleCreate(Sender: TObject);
var
  s: string;
  sAppPath: string;
  sFile_MDB: string;
begin

 {$IFDEF dll}


  sAppPath:= IncludeTrailingBackslash (System.IOUtils.TPath.GetHomePath) + 'RPLS_DB_LINK\';
                         
  CodeSite.Send (sAppPath);  


  s :=System.SysUtils.GetModuleName(HInstance);

  CodeSite.Send (s);  
     
  sFile_MDB:=sAppPath + 'options.mdb';

  if not FileExists(sFile_MDB) then
  begin
    ForceDirectories(sAppPath); 

    TFile.Copy  (s, sFile_MDB);     
  end;
      
  mdb_OpenConnection (ADOConnection_MDB, sFile_MDB);

  db_SetComponentADOConnection(Self, ADOConnection_MDB);
  

      
  {$ENDIF}

  
//  s:= ExtractFilePath(GetModuleFileName()) + 'options.mdb';

  
end;




procedure TdmMain_options.Open;
begin
  t_Objects.Open;
  t_Object_style.Open;
  t_object_item_values.Open;
  t_lib_style_names.Open;
  
end;



// ---------------------------------------------------------------
procedure TdmMain_options.View(aObjName: string);
// ---------------------------------------------------------------
var
  sFeature_type: string;
  oDataset: TDataset;
begin

  oDataset:=t_Object_style;
  
  
  if not T_Objects.Locate('objname',aObjName,[]) then
    raise Exception.Create('Error Message');


  sFeature_type:=T_Objects['Feature_type'];

  
  if Eq(sFeature_type , 'symbol')  then
  begin
    if not oDataset.Locate('name','Symbol_Character',[]) then
      db_AddRecord_( oDataset,[ 'name','Symbol_Character'] );
      
    if not oDataset.Locate('name','Symbol_font_size',[]) then
      db_AddRecord_( oDataset,[ 'name','Symbol_font_size'] );

    if not oDataset.Locate('name','Symbol_font_color',[]) then
      db_AddRecord_( oDataset,[ 'name','Symbol_font_color'] );
 
  end else  

  if Eq(sFeature_type , 'line')  then
  begin
    if not oDataset.Locate('name','line_width',[]) then
      db_AddRecord_( oDataset,[ 'name','line_width'] );

    if not oDataset.Locate('name','line_style',[]) then
      db_AddRecord_( oDataset,[ 'name','line_style'] );

    if not oDataset.Locate('name','line_color',[]) then
      db_AddRecord_( oDataset,[ 'name','line_color'] );

  end

   
//
//  if not T_Objects.Locate('objname',aObjName,[]) then
//    raise Exception.Create('Error Message');
    



end;



end.


{

 sAppPath:= IncludeTrailingBackslash (System.IOUtils.TPath.GetHomePath) + 'RPLS_DB_LINK\';
  
//  s:=Application.
//  CodeSite.Send('System.IOUtils.TPath.GetDocumentsPath - ' + s);

                             
//  s:=System.IOUtils.TPath.Get DocumentsPath;
//  CodeSite.Send('System.IOUtils.TPath.GetDocumentsPath - ' + s);

  
//  System.IOUtils.TPath.GetTempPath
  

  //w:\GIS panorama\SXF_to_RLF\Data\

  s:= ExtractFilePath(GetModuleFileName()) + 'options.mdb';
  Assert(FileExists(s), s);

  sFile_MDB:=sAppPath + 'options.mdb';

  if not FileExists(sFile_MDB) then
  begin
    ForceDirectories(sAppPath);


    TFile.Copy  (s, sFile_MDB);
    
  end;



  s:= ExtractFilePath(GetModuleFileName()) + 'options.mdb';
  Assert(FileExists(s), s);

  sFile_MDB:=sAppPath + 'options.mdb';

  if not FileExists(sFile_MDB) then
  begin
    ForceDirectories(sAppPath);


    TFile.Copy  (s, sFile_MDB);
    
  end;
    
  
  
  // C:\ONEGA\RPLS_DB\bin\WMS
//  if not FileExists(s) then
//     s:= 'C:\ONEGA\RPLS_DB\bin\WMS\' + 'options.mdb';
  
//    s:= ExtractFilePath(Application.ExeName) + 'WMS\options.mdb';

  Assert(FileExists(sFile_MDB), sFile_MDB);
                                             
  mdb_OpenConnection (ADOConnection1, sFile_MDB);

  db_SetComponentADOConnection(Self, ADOConnection1);
    