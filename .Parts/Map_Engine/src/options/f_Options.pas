unit f_Options;

interface

uses

  u_db_mdb,


  u_db,
  u_func,

  u_Options_classes,

  d_Symbol_Select,


  d_Column_get,

  u_cx,


  CodeSiteLogging,Dialogs,

  SysUtils, Variants, forms, cxGraphics, cxLookAndFeels, cxStyles, cxEdit, Data.DB,
  cxImageComboBox, Vcl.Menus, RxPlacemnt,
  Vcl.ImgList, Vcl.Controls, Data.Win.ADODB, Vcl.StdCtrls,
  Vcl.ExtCtrls, cxVGrid, cxDBVGrid, cxGridLevel,
  cxGridCustomTableView, cxGridDBTableView, cxGridCustomView,
  System.Classes, cxGrid,  System.Math,


   IOUtils, cxControls, cxLookAndFeelPainters, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, cxDBData, cxCheckBox, cxDBLookupComboBox,
  cxButtonEdit, cxColorComboBox, cxSpinEdit, cxFontNameComboBox, cxClasses,
  cxInplaceContainer, cxGridTableView, dxSkinsCore, dxSkinsDefaultPainters,
  dxDateRanges, Datasnap.DBClient, dxmdaset
          
    ;

type
  Tfrm_Main_Options = class(TForm)
    ADOConnection_MDB: TADOConnection;
    ds_Objects: TDataSource;
    t_Objects: TADOTable;
    cxGrid_styles: TcxGrid;
    cxGrid_stylesDBTableView_Style: TcxGridDBTableView;
    cxGrid_stylesLevel1: TcxGridLevel;
    ds_Object_style: TDataSource;
    t_Object_style: TADOTable;
    cxGrid_stylesDBTableView_Styleid: TcxGridDBColumn;
    cxGrid_stylesDBTableView_Styleparent_id: TcxGridDBColumn;
    cxGrid_stylesDBTableView_Styleenabled: TcxGridDBColumn;
    col_style_name: TcxGridDBColumn;
    col_Name: TcxGridDBColumn;
    cxGrid_stylesDBTableView_Stylefield_name: TcxGridDBColumn;
    ds_object_item_values: TDataSource;
    t_object_item_values: TADOTable;
    cxGrid_values: TcxGrid;
    cxGridDBTableView_Values: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    cxLookAndFeelController1: TcxLookAndFeelController;
    cxGridDBTableView_Valuesenabled: TcxGridDBColumn;
    cxGridDBTableView_Valuesfield_value: TcxGridDBColumn;
    col_line_color: TcxGridDBColumn;
    col_line_style: TcxGridDBColumn;
    col_line_width: TcxGridDBColumn;
    col_region_border_style: TcxGridDBColumn;
    col_region_border_color: TcxGridDBColumn;
    col_region_border_width: TcxGridDBColumn;
    col_region_style: TcxGridDBColumn;
    col_region_color: TcxGridDBColumn;
    col_region_back_color: TcxGridDBColumn;
    col_Symbol_Character: TcxGridDBColumn;
    col_Symbol_font_name: TcxGridDBColumn;
    col_Symbol_font_size: TcxGridDBColumn;
    col_Symbol_font_color: TcxGridDBColumn;
    img_LineStyle: TImageList;
    img_RegionStyle: TImageList;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    row_line_color: TcxDBEditorRow;
    row_line_style: TcxDBEditorRow;
    row_Line_width: TcxDBEditorRow;
    row_region_border_style: TcxDBEditorRow;
    row_region_border_color: TcxDBEditorRow;
    row_region_border_width: TcxDBEditorRow;
    row_region_Pattern: TcxDBEditorRow;
    row_region_color: TcxDBEditorRow;
    row_region_back_color: TcxDBEditorRow;
    row_Symbol_Character: TcxDBEditorRow;
    row_Symbol_font_name: TcxDBEditorRow;
    row_Symbol_font_size: TcxDBEditorRow;
    row_Symbol_font_color: TcxDBEditorRow;
    row_Line: TcxCategoryRow;
    row_Symbol: TcxCategoryRow;
    row_Region: TcxCategoryRow;
    ds_t_lib_style_names: TDataSource;
    t_lib_style_names: TADOTable;
    col_field_caption: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_Symbol: TcxStyle;
    FormPlacement1: TFormPlacement;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Bevel2: TBevel;
    Button1: TButton;
    Button2: TButton;
    t_Objects_: TdxMemData;
    t_Objects_id: TAutoIncField;
    t_Objects_objname: TWideStringField;
    t_Objects_caption: TWideStringField;
    t_Objects_feature_type: TWideStringField;
    t_Objects_______: TWideStringField;
    t_Objects_Symbol_Character: TSmallintField;
    t_Objects_Symbol_font_name: TWideStringField;
    t_Objects_Symbol_font_size: TIntegerField;
    t_Objects_Symbol_font_color: TIntegerField;
    t_Objects__: TWideStringField;
    t_Objects_line_color: TIntegerField;
    t_Objects_line_style: TIntegerField;
    t_Objects_line_width: TIntegerField;
    t_Objects______: TWideStringField;
    t_Objects_region_border_style: TIntegerField;
    t_Objects_region_border_color: TIntegerField;
    t_Objects_region_border_width: TIntegerField;
    t_Objects_region_pattern: TIntegerField;
    t_Objects_region_color: TIntegerField;
    t_Objects_region_back_color: TIntegerField;
    t_lib_style_names_: TdxMemData;
    t_lib_style_names_name: TWideStringField;
    t_lib_style_names_caption: TWideStringField;
    t_Object_style_: TClientDataSet;
    t_Object_style_id: TAutoIncField;
    t_Object_style_parent_id: TIntegerField;
    t_Object_style_enabled: TBooleanField;
    t_Object_style_name: TWideStringField;
    t_Object_style_caption: TWideStringField;
    t_Object_style_field_name: TWideStringField;
    t_Object_style_field_caption: TWideStringField;
    t_Object_style_t_object_item_values: TDataSetField;
    t_object_item_values_: TClientDataSet;
    t_object_item_values_id: TAutoIncField;
    t_object_item_values_parent_id: TIntegerField;
    t_object_item_values_enabled: TBooleanField;
    t_object_item_values_field_value: TWideStringField;
    t_object_item_values__: TWideStringField;
    t_object_item_values_line_color: TIntegerField;
    t_object_item_values_line_style: TIntegerField;
    t_object_item_values_line_width: TIntegerField;
    t_object_item_values______: TWideStringField;
    t_object_item_values_region_border_style: TIntegerField;
    t_object_item_values_region_border_color: TIntegerField;
    t_object_item_values_region_border_width: TIntegerField;
    t_object_item_values_region_style: TIntegerField;
    t_object_item_values_region_color: TIntegerField;
    t_object_item_values_region_back_color: TIntegerField;
    t_object_item_values_______: TWideStringField;
    t_object_item_values_Symbol_Character: TSmallintField;
    t_object_item_values_Symbol_font_name: TWideStringField;
    t_object_item_values_Symbol_font_size: TIntegerField;
    t_object_item_values_Symbol_font_color: TIntegerField;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
//    procedure Button3Click(Sender: TObject);
//    procedure Button4Click(Sender: TObject);
//    procedure Button5Click(Sender: TObject);
//    procedure Button6Click(Sender: TObject);
    procedure col_Symbol_CharacterCustomDrawCell(Sender: TcxCustomGridTableView;
        ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone:
        Boolean);
    procedure col_Symbol_CharacterPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
//    procedure cxDBVerticalGrid1DrawValue(Sender: TObject; ACanvas: TcxCanvas;
 //       APainter: TcxvgPainter; AValueInfo: TcxRowValueInfo; var Done: Boolean);
    procedure cxDBVerticalGrid1Symbol_CharacterEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure row_Symbol_CharacterPropertiesGetDisplayText(
      Sender: TcxCustomEditorRowProperties; ARecord: Integer;
      var AText: string);
    procedure col_field_captionPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure t_ObjectsAfterScroll(DataSet: TDataSet);
    procedure row_Symbol_CharacterEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDBVerticalGrid1StylesGetContentStyle(Sender: TObject;
      AEditProp: TcxCustomEditorRowProperties; AFocused: Boolean;
      ARecordIndex: Integer; var AStyle: TcxStyle);
    procedure cxGrid_stylesDBTableView_StyleFocusedRecordChanged(Sender:
        TcxCustomGridTableView; APrevFocusedRecord, AFocusedRecord:
        TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
    procedure N1Click(Sender: TObject);
  private
    FConnectionStr: string;
  
    FObjName : string;

    FOptionsObject: TOptionsObject;




//    procedure Change_Object(aObjName: string);
    procedure Dlg_Select_Symbol(aDataSet: TDataSet);
    procedure Dlg_Select_Field;


  
    procedure Init_GridDBColumn(aColumn: TcxGridDBColumn; aImageList: TImageList);
    procedure Init_TcxImageComboBoxProperties(aProperties: TcxCustomEditProperties; aImageList: TImageList);

//    procedure LoadFromDB_OptionsObject(aObject: TOptionsObject);
//    procedure LoadFromDB_OptionsObjectStyle(aObjectObjectStyle:   TOptionsObjectStyle);
    procedure Update_values_view(aName: string);


    procedure View(aObjName, aXML: string);

  public
    class function ExecDlg(aConnectionStr, aObjName: string; var aXML: WideString):
        Boolean;

    procedure Open_DB;

  end;



implementation


{$R *.dfm}


procedure Tfrm_Main_Options.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FOptionsObject);
end;

//------------------------------------------------------------------------------
class function Tfrm_Main_Options.ExecDlg(aConnectionStr, aObjName: string; var aXML: WideString): Boolean;
//------------------------------------------------------------------------------
//var
 // sSQL: string;
begin
  with Tfrm_Main_Options.Create(Application) do
  begin
    FConnectionStr:=aConnectionStr;



     t_Object_style.DisableControls;

     t_Objects.Open; 
 

     View(aObjName, aXML);

     t_Object_style.EnableControls;

    
    Result:=ShowModal=mrOk;

    if Result then
    begin
      db_PostDataset(t_Objects);
    
      FOptionsObject.LoadFromDB (t_Objects, t_Object_style, t_object_item_values);

      aXML:=FOptionsObject.SaveToXML();
    
    end;

    Free;
  end;

end;

// ---------------------------------------------------------------
procedure Tfrm_Main_Options.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  if ADOConnection_MDB.Connected then
    ShowMessage('procedure Tfrm_Main_Options.FormCreate(Sender: TObject); - ADOConnection_MDB.Connected');

             
    
  FOptionsObject:=TOptionsObject.Create;


  Caption:='��������� �����������';

  Constraints.MaxWidth:=600;
  Constraints.MinWidth:=600;
  
//  Width:=600;

  cxGrid_styles.Width:=cxGrid_values.Width;

  Init_TcxImageComboBoxProperties (col_Line_Style.Properties, img_LineStyle );
  Init_TcxImageComboBoxProperties (col_region_border_style.Properties, img_LineStyle );
  Init_TcxImageComboBoxProperties (col_region_style.Properties, img_RegionStyle );
        
  
  Init_TcxImageComboBoxProperties (row_line_style.Properties.EditProperties, img_LineStyle );
  Init_TcxImageComboBoxProperties (row_region_border_style.Properties.EditProperties, img_LineStyle );
  Init_TcxImageComboBoxProperties (row_region_Pattern.Properties.EditProperties, img_RegionStyle );
  

  cx_SetColumnCaptions1(cxGridDBTableView_Values,
  [
    'field_value', '��������',
    'enabled',     '���',

    'Line_width',  '�������',
    'line_style',  '�����',
    'line_color',  '����',

    'font_color',  '����',
    'font_size',   '������',
    'font_name',   '�����' ,

    'Symbol_Character',  '������',
    'Symbol_font_name',  '�����',
    'Symbol_font_color', '����',
    'Symbol_font_size',  '������'

  ]);

  cx_SetColumnCaptions1(cxGrid_stylesDBTableView_Style,
  [
    'name',          '��������',
    'field_caption', '����',
    'field_name',    '���� � ��',
    'enabled',       '���'

  ]);

  row_Line_width.Properties.Caption:='�������';
  row_line_style.Properties.Caption:='�����';
  row_line_color.Properties.Caption:='����';
  
  row_Symbol_Character.Properties.Caption :='������'; 
  row_Symbol_font_name.Properties.Caption :='�����'; 
  row_Symbol_font_size.Properties.Caption :='������'; 
  row_Symbol_font_color.Properties.Caption:='����';

  row_region_border_style.Properties.Caption:='������� - �����';
  row_region_border_color.Properties.Caption:='������� - ����';
  row_region_border_width.Properties.Caption:='������� - �������';

  row_region_Pattern.Properties.Caption   :='��� - �����';
  row_region_color.Properties.Caption     :='��� - ���� �����';
  row_region_back_color.Properties.Caption:='��� - ���� �������';
           


  Open_DB();
  
  
end;




//TcxCustomEditProperties

// ---------------------------------------------------------------
procedure Tfrm_Main_Options.Init_GridDBColumn(aColumn: TcxGridDBColumn; aImageList: TImageList);
// ---------------------------------------------------------------
var
  I: Integer;  
begin  
  with TcxImageComboBoxProperties(aColumn.Properties) do 
  begin
    Items.Clear;

    Images:=aImageList;
    
    for I := 1 to Items.Count do
      Items.Add.Value:=i;
      
  end;   
end;

// ---------------------------------------------------------------
procedure Tfrm_Main_Options.Init_TcxImageComboBoxProperties(aProperties: TcxCustomEditProperties; aImageList: TImageList);
// ---------------------------------------------------------------
var
  I: Integer;         
   
begin    
  with TcxImageComboBoxProperties(aProperties) do 
  begin
    Items.Clear;
    
    Images:=aImageList;
    
    for I := 1 to aImageList.Count do
      Items.Add.Value:=i;
  end;
end;


// ---------------------------------------------------------------
procedure Tfrm_Main_Options.View(aObjName, aXML: string);
// ---------------------------------------------------------------
var
  iID: Integer;
  sFeature_type: string;
  oDataset: TDataset;
begin
  FObjName:=aObjName;


//  t_Object_style.Open;

  oDataset:=t_Object_style;
  oDataset.DisableControls;



  if not T_Objects.Locate('objname',aObjName,[]) then
    raise Exception.Create('Error Message');


  sFeature_type:=T_Objects['Feature_type'];
  iID:=T_Objects['id'];

  // ---------------------------------------------------------------
  if Eq(sFeature_type , 'symbol')  then
  // ---------------------------------------------------------------
  begin
    if not oDataset.Locate('parent_id;name', VarArrayOf([iID,'Symbol_Character']),[]) then
      db_AddRecord_( oDataset,['parent_id',iID, 
                               'name','Symbol_Character'] );
      
    if not oDataset.Locate('parent_id;name',VarArrayOf([iID,'Symbol_font_size']),[]) then
      db_AddRecord_( oDataset,['parent_id',iID, 
                               'name','Symbol_font_size'] );

    if not oDataset.Locate('parent_id;name',VarArrayOf([iID,'Symbol_font_color']),[]) then
      db_AddRecord_( oDataset,['parent_id',iID, 
                               'name','Symbol_font_color'] );
 
  end else  

  // ---------------------------------------------------------------
  if Eq(sFeature_type , 'line')  then
  // ---------------------------------------------------------------
  begin
    if not oDataset.Locate('parent_id;name',VarArrayOf([iID,'line_width']),[]) then
      db_AddRecord_( oDataset,['parent_id',iID, 
                               'name','line_width'] );

    if not oDataset.Locate('parent_id;name',VarArrayOf([iID,'line_style']),[]) then
      db_AddRecord_( oDataset,['parent_id',iID, 
                               'name','line_style'] );

    if not oDataset.Locate('parent_id;name',VarArrayOf([iID,'line_color']),[]) then
      db_AddRecord_( oDataset,['parent_id',iID,
                               'name','line_color'] );
  end ;


  if aXML<>'' then
  begin
    FOptionsObject.LoadFromXML(aXML);
    FOptionsObject.SaveToDB(ADOConnection_MDB,  t_Objects, t_Object_style, t_object_item_values);



  end;

//  t_Object_style.Resync([]);

//  t_Object_style.Locate('enabled', True, []);


  oDataset.EnableControls;
       

end;


// ---------------------------------------------------------------
procedure Tfrm_Main_Options.Dlg_Select_Field;
// ---------------------------------------------------------------
var 
  I: Integer;
  sField_Name: string;
  oSList: TStringList;
  sField_caption: string;
  
begin
  oSList:=TStringList.Create;

//
////  TForm1.ExecDlg;
//  if TForm1.ExecDlg( FConnectionStr, FObjName, sField_Name,
//                              sField_caption, oSList) then 
//
//
//  FreeAndNil(oSList);
//
//
//  Exit;


  sField_Name:= t_Object_style.FieldByName('field_name').AsString;


  CodeSite.Send('procedure Tfrm_Main_Options.Dlg_Select_Field;');
  
  if Tdlg_Column_get.ExecDlg( FConnectionStr, FObjName, sField_Name,
                              sField_caption, oSList) then 
  begin
    db_UpdateRecord__(t_Object_style, 
      [
       'field_name', sField_Name, 
       'Field_caption', sField_caption
      ] );

    t_object_item_values.DisableControls;
                             
    while not t_object_item_values.EOF do
      t_object_item_values.Delete;
      
      

    for I := 0 to oSList.Count-1 do
      db_AddRecord_(t_object_item_values, 
        [
         'field_value', oSList[i]
        ] );
     
    t_object_item_values.Close; 
    t_object_item_values.Open; 
      
    t_object_item_values.EnableControls;;
      
  end;  


    
  FreeAndNil(oSList);
    
end;


procedure Tfrm_Main_Options.col_field_captionPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  Dlg_Select_Field
end;


procedure Tfrm_Main_Options.col_Symbol_CharacterCustomDrawCell(Sender:
    TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo:
    TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  sFontName: string;
begin
  sFontName:=AsString(AViewInfo.GridRecord.Values[col_Symbol_font_name.Index]);

  if sFontName<>'' then
  begin
    ACanvas.Font.Name:=sFontName;

    ACanvas.DrawTexT(Char(StrToIntDef(AViewInfo.Text,0)), AViewInfo.TextAreaBounds, AViewInfo.AlignmentHorz, AViewInfo.AlignmentVert, AViewInfo.MultiLine, false);
    
    ADone := True;  
  end;  
     
end;



procedure Tfrm_Main_Options.cxDBVerticalGrid1Symbol_CharacterEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin 
  Dlg_Select_Symbol(t_object_item_values);    
end;



procedure Tfrm_Main_Options.Dlg_Select_Symbol(aDataSet: TDataSet);
var
  iCode: byte;
  sFontName: string;
begin  
  sFontName:= aDataSet.FieldByName('Symbol_font_name').AsString;
  iCode    := aDataSet.FieldByName('Symbol_Character').AsInteger;
  
 if TSymbolSelectDialog.ExecDlg( sFontName, iCode ) then  
       db_UpdateRecord__(aDataSet,
       [
          'Symbol_font_name',sFontName,
          'Symbol_Character', ord( iCode )  
     
       ]);          

end;



procedure Tfrm_Main_Options.row_Symbol_CharacterEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  Dlg_Select_Symbol(t_Objects);    
end;


procedure Tfrm_Main_Options.row_Symbol_CharacterPropertiesGetDisplayText(
  Sender: TcxCustomEditorRowProperties; ARecord: Integer; var AText: string);
begin
  if AText<>'' then
    AText:=Char(StrToIntDef(AText,0)) ;
end;


procedure Tfrm_Main_Options.col_Symbol_CharacterPropertiesButtonClick( Sender: TObject; AButtonIndex: Integer);
begin
  Dlg_Select_Symbol(t_object_item_values);  
end;



// ---------------------------------------------------------------
procedure Tfrm_Main_Options.Update_values_view(aName: string);
// ---------------------------------------------------------------
var
  v: Variant;
begin
//  Exit;
  // CodeSite.Send('RecordChanged');

  // v:=t_Object_style['name'];

   
   
   col_line_width.Visible := Eq(aName, 'line_width');
   col_line_style.Visible := Eq(aName, 'line_style');
   col_line_color.Visible := Eq(aName, 'line_color');

   col_Symbol_Character.Visible := Eq(aName, 'Symbol_Character');
   col_Symbol_font_name.Visible := Eq(aName, 'Symbol_Character');

   col_Symbol_font_size.Visible  := Eq(aName, 'Symbol_font_size');
   col_Symbol_font_color.Visible := Eq(aName, 'Symbol_font_color');
 

   col_region_border_style.Visible := False;
   col_region_border_color.Visible := False;
   col_region_border_width.Visible := False;
   col_region_style.Visible := False;
   col_region_color.Visible := False;
   col_region_back_color.Visible := False;
   
//   if v = 'Symbol_Character' then
   
   
end;


// ---------------------------------------------------------------
procedure Tfrm_Main_Options.t_ObjectsAfterScroll(DataSet: TDataSet);
// ---------------------------------------------------------------
var
  v: Variant;
begin
  // CodeSite.Send('RecordChanged');

   v:=DataSet['feature_type'];
   
   row_Line.Visible   := Eq(v, 'Line');
   row_Symbol.Visible := Eq(v, 'Symbol');
   row_Region.Visible := Eq(v, 'Region');


   cxGrid_styles.Visible:= not Eq(v, 'Region');
   cxGrid_values.Visible:= not Eq(v, 'Region');
   
end;


// ---------------------------------------------------------------
procedure Tfrm_Main_Options.cxDBVerticalGrid1StylesGetContentStyle(
  Sender: TObject; AEditProp: TcxCustomEditorRowProperties; AFocused: Boolean;
  ARecordIndex: Integer; var AStyle: TcxStyle);
// ---------------------------------------------------------------  
begin
  if AEditProp.Row = row_Symbol_Character then 
    if not VarIsNull(row_Symbol_font_name.Properties.Value) then
  begin
    cxStyle_Symbol.Font.Name:= row_Symbol_font_name.Properties.Value;
    AStyle:=cxStyle_Symbol;
  end   
end;

// ---------------------------------------------------------------
procedure Tfrm_Main_Options.cxGrid_stylesDBTableView_StyleFocusedRecordChanged(
    Sender: TcxCustomGridTableView; APrevFocusedRecord, AFocusedRecord:
    TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
// ---------------------------------------------------------------
var
 // s: string;
  v: Variant;
begin
  if AFocusedRecord = nil then
    Exit;

  try 
    v:=AFocusedRecord.Values[col_Name.Index];

  //  s:=VarToStr(v);

    Update_values_view (v);
  
  except

  end;
  
end;

procedure Tfrm_Main_Options.N1Click(Sender: TObject);
begin
  Dlg_Select_Field();
end;

// ---------------------------------------------------------------
procedure Tfrm_Main_Options.Open_DB;
// ---------------------------------------------------------------
var
  s: string;

  sAppPath: string;
  sDest_MDB: string;
  sSrc_mdb: string;
begin
  CodeSite.Send (System.IOUtils.TPath.GetHomePath);

  s:= Format('map_engine_%d.mdb',[RandomRange(100000, MaxInt)]);


 {$IFDEF dll}

  sAppPath:= IncludeTrailingBackslash (System.IOUtils.TPath.GetHomePath) + 'RPLS_DB_LINK\';

  CodeSite.Send ('Tfrm_Main_Options.Open_DB -'+ sAppPath);


  sSrc_mdb := ExtractFilePath( System.SysUtils.GetModuleName(HInstance) ) + 'map_engine.mdb' ;

//  showmessage (s);
  
  CodeSite.Send (sSrc_mdb);


//  sDest_MDB:=sAppPath + 'map_engine.mdb';
  sDest_MDB:=sAppPath + Format('map_engine_%d.mdb',[RandomRange(100000, MaxInt)]);

  //RandomRange(100000, MaxInt);

//  if FileExists(sDest_MDB) then
//    TFile.Delete(sDest_MDB);
  
///////////  
  //if not FileExists(sDest_MDB) then

  
//  CodeSite.Send( DateTimeToString( TFile.GetLastWriteTime(sAppPath)) );
  
  
  if FileExists(sDest_MDB) then
    TFile.Delete(sDest_MDB);

//  begin
    ForceDirectories(sAppPath); 

    TFile.Copy  (sSrc_mdb, sDest_MDB);
 // end;
      
  mdb_OpenConnection (ADOConnection_MDB, sDest_MDB);

//  db_SetComponentADOConnection(Self, ADOConnection_MDB);
  

      
  {$ENDIF}

  t_lib_style_names.Open;
  t_object_item_values.Open;
  t_Object_style.Open;
  t_Objects.Open; 

  
//  s:= ExtractFilePath(GetModuleFileName()) + 'options.mdb';

  
end;






end.


