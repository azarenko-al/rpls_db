unit d_Layer_Attributes;

interface

uses
  

  u_db,


  CodeSiteLogging, Windows, Messages,
  
  System.SysUtils, System.Variants, System.Classes, 
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxLookAndFeels,
  
  Data.DB, Data.Win.ADODB,  cxTL, 
  cxDBTL, RxPlacemnt, Vcl.StdCtrls,
  Vcl.ExtCtrls, 
  
  Vcl.DBGrids, cxGraphics, cxControls, cxLookAndFeelPainters, cxCustomData,
  cxStyles, cxMaskEdit, cxCheckBox, cxClasses, Vcl.Grids, cxInplaceContainer,
  cxTLData, cxTLdxBarBuiltInMenu;

type
  Tdlg_Layer_Attributes = class(TForm)
    ADOConnection1: TADOConnection;
    DataSource1: TDataSource;
    ADOStoredProc_SEL: TADOStoredProc;
    cxDBTreeList1: TcxDBTreeList;
    cxDBTreeList1id: TcxDBTreeListColumn;
    cxDBTreeList1parent_id: TcxDBTreeListColumn;
    cxDBTreeList1name: TcxDBTreeListColumn;
    ADOStoredProc_UPD: TADOStoredProc;
    cxDBTreeList1cxDBTreeListColumn1: TcxDBTreeListColumn;
    cxDBTreeList1cxDBTreeListColumn2: TcxDBTreeListColumn;
    cxLookAndFeelController1: TcxLookAndFeelController;
    pn_Buttons11: TPanel;
    Bevel1: TBevel;
    Button1: TButton;
    Button2: TButton;
    cxDBTreeList1cxDBTreeListColumn3: TcxDBTreeListColumn;
    col_is_folder: TcxDBTreeListColumn;
    COL_name: TcxDBTreeListColumn;
    FormStorage1: TFormStorage;
    cb_Add_caption: TCheckBox;
    DBGrid1: TDBGrid;
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure b_SaveClick(Sender: TObject);
    procedure cxDBTreeList1Editing(Sender: TcxCustomTreeList; AColumn:
        TcxTreeListColumn; var Allow: Boolean);
    procedure FormPlacement1RestorePlacement(Sender: TObject);
  private
    FObjName: string;

    procedure Save;
    
  public
    class procedure ExecDlg(aConnectionStr, aObjName: string);

  end;


  
implementation

{$R *.dfm}


procedure Tdlg_Layer_Attributes.Button1Click(Sender: TObject);
begin
  Save;
end;

procedure Tdlg_Layer_Attributes.Button3Click(Sender: TObject);
begin

 // CloseApplicationForms;
  
end;


procedure Tdlg_Layer_Attributes.FormCreate(Sender: TObject);
begin
  if ADOConnection1.Connected then
    ShowMessage('ADOConnection1.Connected');
                       
            
  cxDBTreeList1.Align:=alClient;

  Caption:='Настройка подписей';
end;



//------------------------------------------------------------------------------
procedure Tdlg_Layer_Attributes.b_SaveClick(Sender: TObject);
begin
  Save;
end;

procedure Tdlg_Layer_Attributes.cxDBTreeList1Editing(Sender: TcxCustomTreeList;  AColumn: TcxTreeListColumn; var Allow: Boolean);
begin
  if cxDBTreeList1.FocusedNode.Values[col_is_folder.ItemIndex] = True then 
    Allow:=False;

end;


//------------------------------------------------------------------------------
class procedure Tdlg_Layer_Attributes.ExecDlg(aConnectionStr, aObjName: string);
//------------------------------------------------------------------------------
var
  k: Integer;
  sSQL: string;
begin      
  with Tdlg_Layer_Attributes.Create(Application) do
  begin
    FObjName:=aObjName;
  
    db_OpenADOConnectionString (ADOConnection1, aConnectionStr);
  
//    ADOConnection1.Close;
//    ADOConnection1.ConnectionString:=aConnectionStr;
//    ADOConnection1.Open;
    

    k:=db_StoredProc_Open(ADOStoredProc_SEL, ADOStoredProc_SEL.ProcedureName,
       ['objname', aObjName]);


    cxDBTreeList1.FullExpand;   
    
   // PostMessage (Handle, WM_CLOSE, 0,0);
    
    ShowModal;

    Free;
  end;

end;

procedure Tdlg_Layer_Attributes.FormPlacement1RestorePlacement(Sender: TObject);
begin

end;

// ---------------------------------------------------------------
procedure Tdlg_Layer_Attributes.Save;
// ---------------------------------------------------------------
var 
  k: Integer;
  oStrList: TStringList;
  oStrList_full: TStringList;
  s: string;
begin
  ADOStoredProc_SEL.DisableControls;

  oStrList:=TStringList.Create;
  oStrList_full:=TStringList.Create;
  oStrList_full.Delimiter:=';';
//  oStrList_full.CommaText:=' ';
  
  ADOStoredProc_SEL.Filter:='checked=True AND NAME<>'''' ';
  ADOStoredProc_SEL.Filtered:=True;

  ADOStoredProc_SEL.First;

  with ADOStoredProc_SEL do
    while not EOF do
    begin
   //   if FieldByName ('name').AsString<>'' then
      oStrList.Add(FieldByName ('name').AsString);


     if cb_Add_caption.Checked then       
        oStrList_full.Add(FieldByName ('CAPTION_SQL').AsString)
     else
        oStrList_full.Add(FieldByName ('CAPTION_small_SQL').AsString) ;
        
        
      Next;
    end;


   s:=oStrList_full.DelimitedText; 
//--   s:=s.Replace('"','');
//--   s:=s.Replace(Chr(10),'+'+Chr(10)+'+');

   //oStrList_full.Delimiter:=Chr(10);

  CodeSite.Send( ADOStoredProc_UPD.ProcedureName );
   
    
   k:= db_StoredProc_Exec(ADOStoredProc_UPD, ADOStoredProc_UPD.ProcedureName,
       [
        'objname', FObjName,
        'NAMES',   oStrList.DelimitedText ,
       'CAPTION_SQL', s //  oStrList_full.DelimitedText
       ]);


  ADOStoredProc_SEL.Filtered:=False;
                                                
       
  CodeSite.Send( 'k='+ IntToStr(k) );
  CodeSite.Send( oStrList.DelimitedText );
      
  FreeAndNil(oStrList);
  FreeAndNil(oStrList_full);

  ADOStoredProc_SEL.EnableControls;
  
end;



end.
