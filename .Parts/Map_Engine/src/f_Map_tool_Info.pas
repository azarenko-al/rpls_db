unit f_Map_tool_Info;

interface

uses

  u_mapX,

  u_db,
  
  u_GEO,
  u_func, 


  

  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, rxPlacemnt, ToolWin, StdCtrls, Variants, MapXLib_TLB,

  Grids, ValEdit, 
  
  Data.DB, Vcl.DBGrids, dxmdaset
  ;


type

  Tfrm_Map_tool_Info = class(TForm)
    FormStorage1: TFormStorage;
    t_Layers: TdxMemData;
    t_Layersname: TStringField;
    t_Layersfilename: TStringField;
    t_Values: TdxMemData;
    StringField1: TStringField;
    ds_Layers: TDataSource;
    ds_Valuess: TDataSource;
    DBGrid1: TDBGrid;
    t_Valuesvalue: TStringField;
    DBGrid2_values: TDBGrid;
    Splitter1: TSplitter;
//    procedure FormDestroy(Sender: TObject);
    procedure FormCreate  (Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
//    procedure ValueListEditor_layersSelectCell(Sender: TObject; ACol, ARow:
 //       Integer; var CanSelect: Boolean);

  private
    FBLPoint: TBLPoint;

   // FDisableEdit : Boolean;

//    procedure LoadLayerFields;

    procedure LoadLayerFields_new(aLayer: CMapXLayer; aFeatures: CMapXFeatures);
    procedure LoadLayers;
    
 protected
    FMap: CMapX;

    procedure ShowPoint(aBLPoint: TBLPoint);

  public

    class procedure ShowWindow(aMap: CMapX; aLat, aLon: Double);
  end;


//====================================================
// implementation
//====================================================
implementation {$R *.DFM}


//---------------------------------------------------
class procedure Tfrm_Map_tool_Info.ShowWindow(aMap: CMapX; aLat, aLon: Double);
//---------------------------------------------------
var
  oForm: Tfrm_Map_tool_Info;

  bl: TBLPoint;
  
//  vIMapView: IMapViewX;
begin
  bl.B:=aLat;
  bl.L:=aLon;

//  if not Assigned(IActiveMapView) then
 //   Exit;

// CodeSite.Send('lat-'+ FloatToStr(aLat));
// CodeSite.Send('lat-'+ FloatToStr(aLon));
 
// ShowMessage('1');

  oForm:= Tfrm_Map_tool_Info(IsFormExists ( Tfrm_Map_tool_Info.ClassName));


// ShowMessage('2');
  

  if not Assigned(oForm) then
//  begin
  //  vIMapView:=IActiveMapView;
  oForm:= Tfrm_Map_tool_Info.Create(Application);
    
//  end;

  oForm.FMap:=aMap;
  oForm.ShowPoint(bl);

  oForm.Show;        

  

 { with oForm do
  begin
    Screen.Cursor:= crHourGlass;

    try
      FMap:= aMap;
      FBLPoint.B:= aBLPoint.B;
      FBLPoint.L:= aBLPoint.L;

      LoadLayers();

      dx_LayersChangeNode(nil, nil, nil);

      Show;

    finally
      Screen.Cursor:= crDefault;
    end;
  end;
}

end;

//------------------------------------------------------------------------------
procedure Tfrm_Map_tool_Info.FormCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;
  Height:= 270;

 // dx_Layers.Align:=      alTop;
  //dx_LayerFields.Align:= alClient;

//  cxDBVerticalGrid1.Align:= alClient;
(*
  db_CreateField(mem_Layers, [db_Field(FLD_NAME,    ftString, 100),
                              db_Field(FLD_FILENAME,ftString, 500) ]);

  mem_Layers.Open;
*)

  //
 // FRegPath:=REGISTRY_COMMON_FORMS+ClassName+'\';

  //ValueListEditor_layers.Height:=reg_ReadInteger(FRegPath, ValueListEditor_layers.Name, ValueListEditor_layers.Height);

 //   if not mapx_CreateMemFromMapFields (sFileName, mem_LayerFields ) then


  DBGrid2_values.Align:=alClient;

end;


//------------------------------------------------------------------------------
procedure Tfrm_Map_tool_Info.LoadLayers;
//------------------------------------------------------------------------------
var
  sShortFileName: string;
  sFileName: string;
  iCount: integer;
  i: integer;
  iID: Integer;
  s: string;
  vLayer: CMapXLayer;
  vPt : CMapXPoint;
  vFeatures: CMapXFeatures;

begin
  vPt := CoPoint.Create;
  vPt.Set_(FBLPoint.L, FBLPoint.B);


//  ValueListEditor_layers.Strings.Clear;
//  ValueListEditor1.Strings.Clear;

  t_Layers.Close;
  t_Layers.Open;

  t_Values.Close;
  t_Values.Open;
  
  

  for i:=1 to FMap.Layers.Count do
  begin
      vLayer:= FMap.Layers.Item[i];

    if (not vLayer.Visible) or
      (vLayer.Type_ <> miLayerTypeNormal)
    then
      Continue;

    vLayer.BeginAccess(miAccessRead);

    vFeatures:= vLayer.SearchAtPoint(vPt, EmptyParam);

    iCount:= vFeatures.Count;

    if vFeatures.Count > 0 then
    begin
      sFileName:=      vLayer.Filespec;
      sShortFileName:= ExtractFileName(sFileName);

      db_AddRecord_(t_Layers, 
      [
        FLD_NAME,     vLayer.Name,
        FLD_FileName, sShortFileName
      ]);

      
     iID:=t_Layers['recId'];



      LoadLayerFields_new(vLayer, vFeatures);
                
      
//      ValueListEditor_layers.InsertRow(vLayer.Name, sFileName, True);

      end;

    vLayer.EndAccess(miAccessEnd);
  end;


  t_Values.First;


end;



//------------------------------------------------------------------------------
procedure Tfrm_Map_tool_Info.LoadLayerFields_new(aLayer: CMapXLayer; aFeatures:
    CMapXFeatures);
//------------------------------------------------------------------------------
var
  iDataset: Integer;
  sFieldName: string;
  j ,k: Integer;
  s: string;

//  vLayer: CMapXLayer;
  vPt : CMapXPoint;
//  vFeatures: CMapXFeatures;
  vFeature: CMapXFeature;

  vDS: CMapXDataset;
  vField: CMapXField;

begin
 // Exit;

(*
  if mem_Layers.IsEmpty then
  begin
    db_ClearFields(mem_LayerFields);
    exit;
  end;
*)
 // sFileName:= mem_Layers[FLD_FILENAME];



//  vLayer:=mapx_GetLayerByFileName(Map, aFileName);
//  if VarIsNUll(vLayer) then
//    Exit;
//
//
//
//
//  if not Assigned(vLayer) then
//  begin
// //   mem_LayerFields.EnableControls;
//    exit;
//  end;

  
(*
  if not mapx_CreateMemFromMapFile (aFileName, mem_LayerFields ) then
  begin
//    mem_LayerFields.EnableControls;
    Exit;
  end;*)

//  mem_LayerFields.DisableControls;
 // db_ClearFields(mem_LayerFields);
 // mem_LayerFields.Open;


//  vPt := CoPoint.Create;
//  vPt.Set_(FBLPoint.L, FBLPoint.B);


//   vFeatures:= vLayer.SearchAtPoint(vPt, EmptyParam);

 // vLayer.BeginAccess(miAccessRead);

  for j:=1 to aFeatures.Count do
  begin

    vFeature:= aFeatures.Item[j];


    if (aLayer.Datasets.Count = 0) then
      FMap.Datasets.Add (miDataSetLayer, aLayer, EmptyParam, EmptyParam,
                         EmptyParam, EmptyParam, EmptyParam, EmptyParam) ;


 //   ValueListEditor1.Strings.Clear;


    for iDataset:=1 to aLayer.Datasets.Count do
    begin
      vDS := aLayer.Datasets.Item[iDataset];


    //  mem_LayerFields.Append;
      for k := 1 to vDS.Fields.Count do
      begin

        vField := vDS.Fields.Item[k];

        sFieldName := vField.Name;

        s:=AsString(mapx_GetFieldValue(aLayer, iDataset,  vFeature, sFieldName));

     //   ValueListEditor1.InsertRow(sFieldName, s,True);


        db_AddRecord_(t_Values, 
        [
          FLD_NAME,  sFieldName,
          FLD_VALUE, s
        ]);
                

        
      end;
    end;
  end;

  
  db_AddRecord_(t_Values, 
    [
      FLD_NAME, null
    ]);

  
end;


procedure Tfrm_Map_tool_Info.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;


// ---------------------------------------------------------------
procedure Tfrm_Map_tool_Info.ShowPoint(aBLPoint: TBLPoint);
// ---------------------------------------------------------------
begin
  FBLPoint.B:= aBLPoint.B;
  FBLPoint.L:= aBLPoint.L;

//  FDisableEdit := True;

  LoadLayers();

//  FDisableEdit := False;

 // dx_LayersChangeNode(nil, nil, nil);

 // Show;

end;



end.

{

procedure Tfrm_Map_tool_Info.ValueListEditor_layersSelectCell(Sender: TObject;
    ACol, ARow: Integer; var CanSelect: Boolean);
var
  s: string;
begin
  if FDisableEdit then
    Exit;

  s := ValueListEditor_layers.Strings.ValueFromIndex[ARow-1];

  LoadLayerFields_new(s);
end;

