object frm_Map_tool_Info: Tfrm_Map_tool_Info
  Left = 1157
  Top = 493
  BorderIcons = [biSystemMenu]
  Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103
  ClientHeight = 421
  ClientWidth = 515
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poDefault
  Visible = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 104
    Width = 515
    Height = 3
    Cursor = crVSplit
    Align = alTop
    ExplicitWidth = 213
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 515
    Height = 104
    Align = alTop
    DataSource = ds_Layers
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'name'
        Width = 121
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'filename'
        Width = 335
        Visible = True
      end>
  end
  object DBGrid2_values: TDBGrid
    Left = 0
    Top = 317
    Width = 515
    Height = 104
    Align = alBottom
    DataSource = ds_Valuess
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'name'
        Width = 128
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'value'
        Width = 327
        Visible = True
      end>
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredProps.Strings = (
      'DBGrid1.Height')
    StoredValues = <>
    Left = 32
    Top = 128
  end
  object t_Layers: TdxMemData
    Active = True
    Indexes = <>
    Persistent.Option = poNone
    SortOptions = []
    Left = 176
    Top = 128
    object t_Layersname: TStringField
      DisplayLabel = #1057#1083#1086#1081
      FieldName = 'name'
      Size = 50
    end
    object t_Layersfilename: TStringField
      DisplayLabel = #1060#1072#1081#1083
      FieldName = 'filename'
      Size = 200
    end
  end
  object t_Values: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 312
    Top = 128
    object StringField1: TStringField
      DisplayLabel = #1055#1086#1083#1077
      FieldName = 'name'
      Size = 50
    end
    object t_Valuesvalue: TStringField
      DisplayLabel = #1047#1085#1072#1095#1077#1085#1080#1077
      FieldName = 'value'
      Size = 100
    end
  end
  object ds_Layers: TDataSource
    DataSet = t_Layers
    Left = 176
    Top = 192
  end
  object ds_Valuess: TDataSource
    DataSet = t_Values
    Left = 312
    Top = 192
  end
end
