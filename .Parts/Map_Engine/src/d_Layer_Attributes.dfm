object dlg_Layer_Attributes: Tdlg_Layer_Attributes
  Left = 1095
  Top = 335
  BorderWidth = 5
  Caption = 'dlg_Layer_Attributes'
  ClientHeight = 566
  ClientWidth = 814
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxDBTreeList1: TcxDBTreeList
    Left = 0
    Top = 0
    Width = 489
    Height = 411
    Align = alLeft
    Bands = <
      item
      end>
    DataController.DataSource = DataSource1
    DataController.ParentField = 'parent_id'
    DataController.KeyField = 'id'
    Navigator.Buttons.CustomButtons = <>
    OptionsView.SimpleCustomizeBox = True
    RootValue = -1
    TabOrder = 0
    OnEditing = cxDBTreeList1Editing
    object cxDBTreeList1id: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'id'
      Width = 74
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1parent_id: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'parent_id'
      Width = 100
      Position.ColIndex = 4
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1name: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'name'
      Width = 122
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1cxDBTreeListColumn1: TcxDBTreeListColumn
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.ImmediatePost = True
      Caption.Text = #1054#1090#1086#1073#1088#1072#1078#1072#1090#1100
      DataBinding.FieldName = 'checked'
      Width = 77
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1cxDBTreeListColumn2: TcxDBTreeListColumn
      Caption.Text = #1053#1072#1079#1074#1072#1085#1080#1077' '#1087#1086#1083#1103
      DataBinding.FieldName = 'caption'
      Options.Editing = False
      Width = 214
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1cxDBTreeListColumn3: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'caption_sql'
      Width = 273
      Position.ColIndex = 5
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_is_folder: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'is_folder'
      Width = 100
      Position.ColIndex = 6
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object COL_name: TcxDBTreeListColumn
      DataBinding.FieldName = 'name'
      Width = 100
      Position.ColIndex = 7
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object pn_Buttons11: TPanel
    Left = 0
    Top = 531
    Width = 814
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 1
    DesignSize = (
      814
      35)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 814
      Height = 2
      Align = alTop
      Shape = bsTopLine
      ExplicitWidth = 634
    end
    object cb_Add_caption: TCheckBox
      Left = 0
      Top = 8
      Width = 161
      Height = 17
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1085#1072#1079#1074#1072#1085#1080#1077' '#1087#1086#1083#1103
      TabOrder = 2
    end
    object Button2: TButton
      Left = 732
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ModalResult = 2
      TabOrder = 1
    end
    object Button1: TButton
      Left = 651
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      Default = True
      ModalResult = 1
      TabOrder = 0
      OnClick = Button1Click
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 411
    Width = 814
    Height = 120
    Align = alBottom
    DataSource = DataSource1
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Visible = False
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Password=sa;Persist Security Info=True;User I' +
      'D=sa;Data Source=onega_link'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 696
    Top = 128
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc_SEL
    Left = 544
    Top = 64
  end
  object ADOStoredProc_SEL: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'map.sp_Map_engine_layer_attributes_SEL'
    Parameters = <>
    Left = 544
    Top = 16
  end
  object ADOStoredProc_UPD: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    ProcedureName = 'map.sp_Map_engine_layer_attributes_UPD'
    Parameters = <>
    Left = 656
    Top = 16
  end
  object cxLookAndFeelController1: TcxLookAndFeelController
    Kind = lfFlat
    Left = 543
    Top = 232
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredProps.Strings = (
      'cb_Add_caption.Checked')
    StoredValues = <>
    Left = 544
    Top = 160
  end
end
