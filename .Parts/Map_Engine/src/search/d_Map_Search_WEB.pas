unit d_Map_Search_WEB;

interface
      

{$DEFINE debug}

uses
 // u_xml,

 IdURI,
 
 dm_WEB_search,
              
 HTTPApp,

//IdURI,
 
 u_ini_geocoding,

 //u_web,
 u_xml_new, 

 u_geo_convert,
  
  u_mapx,

  u_geo,

  u_files,
  
  u_func,
  u_func_arr,               

  u_db,

  Forms, SysUtils,   MapXLib_TLB,    
  Controls, Grids, DBGrids, StdCtrls, Buttons, ToolWin, DB, dxmdaset,
  Classes, ActnList, rxPlacemnt, ComCtrls, Vcl.ExtCtrls,
  
  Xml.XMLIntf, Xml.XMLDoc, Xml.xmldom, Xml.Win.msxmldom, System.Actions,
  Vcl.Menus;

type


  Tdlg_Map_Search_WEB = class(TForm)
    ToolBar1: TToolBar;
    ed_Search: TEdit;
    DBGrid1: TDBGrid;
    FormStorage1: TFormStorage;
    ActionList1: TActionList;
    act_Search: TAction;
    mem_Data: TdxMemData;
    ds_Data: TDataSource;
    mem_Dataposition: TStringField;
    mem_Datalat: TFloatField;
    mem_Datalon: TFloatField;
    BitBtn1: TBitBtn;
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    Button3: TButton;
    ComboBox_Service: TComboBox;
    ToolButton2: TToolButton;
    ToolButton1: TToolButton;
    XMLDocument1: TXMLDocument;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    act_Locate: TAction;
    procedure act_LocateExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure act_SearchExecute(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
//    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    FMap: CMapX;
  
    procedure LocateOnMap;
  //   FEvents: ISearchEventsX;
//    FProc: TOnSearchFind;


    procedure Search;
    
    procedure Search_OSM(aAddress: string);
    procedure Search_Yandex(aAddress: string);
//    procedure UpdateStatusBar;

  public
    class procedure ExecDlg(aMap: CMapX);

  end;

 
var
  dlg_Map_Search_WEB: Tdlg_Map_Search_WEB;
   
  

implementation

{$R *.dfm}

procedure Tdlg_Map_Search_WEB.act_LocateExecute(Sender: TObject);
begin
  LocateOnMap;
end;

procedure Tdlg_Map_Search_WEB.FormDestroy(Sender: TObject);
begin
 mapx_Layer_Delete(FMap, 'temp');
end;

{
var
  dlg_Yandex_search: Tdlg_Yandex_search;
 }




//------------------------------------------------------------------------------
class procedure Tdlg_Map_Search_WEB.ExecDlg(aMap: CMapX);
//------------------------------------------------------------------------------
var
  sSQL: string;
begin
  with Tdlg_Map_Search_WEB.Create(Application) do
  begin
    FMap:=aMap;

//    FProc:=aProc;

  //  if aEvents<>nil then
   //   FEvents:=aEvents;

    
//    UpdateStatusBar;
    
    ShowModal;

    Free;
  end;

end;



procedure Tdlg_Map_Search_WEB.act_SearchExecute(Sender: TObject);
begin
  if Sender=act_Search then
    Search

end;

procedure Tdlg_Map_Search_WEB.DBGrid1DblClick(Sender: TObject);
begin
  LocateOnMap;
end;



procedure Tdlg_Map_Search_WEB.LocateOnMap;
var
  vLayer: CMapxLayer;
  eLat: Double;
  eLon: Double;
  
//  vIMapView: IMapViewX;
begin
         
  if mem_Data.RecordCount>0 then
  begin
    eLat:=mem_Data[FLD_LAT];
    eLon:=mem_Data[FLD_LON];  

    FMap.ZoomTo(FMap.Zoom,  eLon, eLat); 
    
    mapx_Layer_Delete(FMap,'temp');
    
    vLayer:=mapx_TempLayer_Create(FMap,'temp');
  
//    mapx_DeleteLayerFeatures(FMap,'temp');
  
    mapx_AddSymbol_(FMap, vLayer, eLat, eLon , MI_SYMBOL_CROSS);

//    function mapx_AddSymbol_  (aMap: CMapX;
//                          aVLayer: CMapXLayer;
//                          
//                          aLat: double;
//                          aLon: double;
    

    
  end;

// x:=aMap.CenterX;
//  y:=aMap.CenterY;
//
//  
//  aMap.ZoomTo (aMap.Zoom, aBLPoint.L, aBLPoint.B); 
  
//    if Assigned(FProc) then
//      FProc(qry_Prop[FLD_LAT], qry_Prop[FLD_LON]);
      
end;



// ---------------------------------------------------------------
procedure Tdlg_Map_Search_WEB.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin

  Caption:= '����� �� ������';
  DBGrid1.Align:= alClient;

  //---------------------------------------------
//  ..����������
  
//  db_CreateField(mem_Data, [db_Field(FLD_ID,       ftInteger),
//                            db_Field(FLD_COMMENT,  ftString),
//                            db_Field(FLD_POSITION, ftString),
//                            db_Field(FLD_LAT,      ftFloat),
//                            db_Field(FLD_LON,      ftFloat), 
//
//                            db_Field(FLD_LAT_STR,  ftString),
//                            db_Field(FLD_LON_STR,  ftString) 
//
//                            ]);

end;


//------------------------------------------------------------------------------
procedure Tdlg_Map_Search_WEB.Search;
//------------------------------------------------------------------------------
var
  i: integer;
  sComment, sValue: string;
  oStrList: TStringList;
  arrStr: TStrArray;
  posBL: TBLPoint;

  bl_Pulkovo: TBLPoint;
begin
  if trim(ed_Search.Text) = '' then 
    exit;

//  db_Clear(mem_Data);

    
//  mem_Data.DisableControls;
  case ComboBox_Service.ItemIndex of
    0: Search_Yandex(ed_Search.Text);
  else
       Search_OSM(ed_Search.Text);
    
  end;


//  mem_Data.EnableControls;

  mem_Data.First;
  

//  UpdateStatusBar;
end;

//
//procedure Tdlg_Map_Search_WEB.Button2Click(Sender: TObject);
//begin
//  ed_Search.Text:='������';
//end;
//

                              
// ---------------------------------------------------------------
procedure Tdlg_Map_Search_WEB.Search_Yandex(aAddress: string);
// ---------------------------------------------------------------

var
  i: Integer;
  s: string;
  vAddressNode: IXMLNode;
  vGeoObjectCollection: IXMLNode;
  vNode: IXMLNode;
  vPointNode: IXMLNode;

  arrStr: TStrArray;
  eLat_WGS: Double;
  eLon_WGS: Double;
  sFile: string;

  bl: TBLPoint;
  
begin
  sFile:=GetTempXmlFileName();

      
  {$IFDEF debug}
//  sFile:='d:\test.xml' ;
  {$ENDIF}


      
  mem_Data.Close;
  mem_Data.Open;

  
//CodeSite.Send(g_Ini_Geocoding.Yandex + aAddress);

//  DownloadFile ( g_Ini_Geocoding.Yandex + AnsiToUtf8(aAddress), sFile);

  DownloadFile (g_Ini_Geocoding.Yandex +  TIdURI.ParamsEncode(aAddress), sFile);
  
//  DownloadFile ('https://geocode-maps.Yandex.ru/1.x/?apikey=3aba4e44-9a57-40dc-b302-56487078c548&results=10&geocode='+ URLEncode( aAddress), sFile);

//  DownloadFile ('https://geocode-maps.yandex.ru/1.x/?apikey=3aba4e44-9a57-40dc-b302-56487078c548&results=10&geocode=%D0%A2%D0%B2%D0%B5%D1%80%D1%81%D0%BA%D0%B0%D1%8F', sFile);
  

  
//  Memo1.Text := IdHTTP1.Get('https://geocode-maps.Search_Yandex.ru/1.x/?apikey=3aba4e44-9a57-40dc-b302-56487078c548&geocode=Tverskaya&results=10');

  XMLDocument1.LoadFromFile(sFile); //  FromXML (Memo1.Text);

//  DeleteFile(sFile);

 //----------------------------------------
  
  i:=XMLDocument1.ChildNodes.Count;

  vNode:=XMLDocument1.ChildNodes.FindNode('ymaps');
  
  vGeoObjectCollection:=xml_GetNodeByPath (vNode, ['GeoObjectCollection' ]);

  if Assigned(vGeoObjectCollection) then
    for I := 0 to vGeoObjectCollection.ChildNodes.Count -1 do
      if vGeoObjectCollection.ChildNodes[i].LocalName = 'featureMember' then
      begin
        vAddressNode:=xml_GetNodeByPath (vGeoObjectCollection.ChildNodes[i], ['GeoObject','metaDataProperty','GeocoderMetaData ','AddressDetails','Country','AddressLine' ]);
        s:=vAddressNode.Text;

        vPointNode:=xml_GetNodeByPath (vGeoObjectCollection.ChildNodes[i], ['GeoObject','Point','pos' ]);
        s:=vPointNode.Text;
        

        arrStr:= StringToStrArray(s, ' ');

        if High(arrStr) >= 1 then
        begin
          eLat_WGS:= AsFloat(arrStr[1]);
          eLon_WGS:= AsFloat(arrStr[0]);

          bl.B:=eLat_WGS;
          bl.L:=eLon_WGS;

          bl:=geo_WGS84_to_Pulkovo42(bl);
                                        
          

          db_AddRecord_(mem_Data, [
             'address', vAddressNode.Text,
             'lat', bl.B,
             'lon', bl.L

//             'lat', eLat_WGS,
//             'lon', eLon_WGS

             ]);
          
        end;

      end;  
  
end;



/////////////////////////////////////////////////////////////////

// ---------------------------------------------------------------
procedure Tdlg_Map_Search_WEB.Search_OSM(aAddress: string);
// ---------------------------------------------------------------


var
  i: Integer;
  sPath: WideString;

  vRoot: IXMLNode;
//  vAddressNode: IXMLNode;
 // vGeoObjectCollection: IXMLNode;
  vNode: IXMLNode;
  vPointNode: IXMLNode;

//  arrStr: TStrArray;
  eLat_WGS: Double;
  eLon_WGS: Double;
  s: string;
  sFile: string;

  bl: TBLPoint;
  
begin
  sFile:=GetTempXmlFileName();
//  sFile:='d:\data.xml';
      
  {$IFDEF debug}
//  sFile:='d:\test.xml' ;
  {$ENDIF}

      
  mem_Data.Close;
  mem_Data.Open;


// s:=g_Ini_Geocoding.OSM + HTMLEncode(aAddress);

//  DownloadFile (sPath, sFile);

///CodeSite.Send(g_Ini_Geocoding.OSM + aAddress);

  DownloadFile ( g_Ini_Geocoding.OSM + TIdURI.ParamsEncode( aAddress), sFile);
 
//  DownloadFile ( g_Ini_Geocoding.OSM + HTTPEncode( aAddress), sFile);

// 

//  DownloadFile ('https://nominatim.openstreetmap.org/search/query?email=myesmail@mysserver.com&format=xml&q=' +  AnsiToUtf8(aAddress), sFile);

//  DownloadFile ('https://nominatim.openstreetmap.org/search/query?email=myesmail@mysserver.com&format=xml&q=moscow', sFile);


  //https://nominatim.openstreetmap.org/search/query?format=xml&q={title}


  
//  Memo1.Text := IdHTTP1.Get('https://geocode-maps.Search_Yandex.ru/1.x/?apikey=3aba4e44-9a57-40dc-b302-56487078c548&geocode=Tverskaya&results=10');

  XMLDocument1.LoadFromFile(sFile); //  FromXML (Memo1.Text);

//  DeleteFile(sFile);

 //----------------------------------------
  
  i:=XMLDocument1.ChildNodes.Count;

//  vNode:=XMLDocument1.DocumentElement;
  
  s:=XMLDocument1.DocumentElement.LocalName;

  vRoot:=XMLDocument1.DocumentElement.ChildNodes.FindNode('searchresults');
  
  vRoot:=XMLDocument1.DocumentElement;
  
//  vGeoObjectCollection:=xml_GetNodeByPath (vNode, ['GeoObjectCollection' ]);

   if Assigned(vRoot) then
    for I := 0 to vRoot.ChildNodes.Count -1 do
      if vRoot.ChildNodes[i].LocalName = 'place' then
      begin
        vNode:=vRoot.ChildNodes[i];

        eLat_WGS:= AsFloat(vNode.Attributes['lat']);
        eLon_WGS:= AsFloat(vNode.Attributes['lon']);

        bl.B:=eLat_WGS;
        bl.L:=eLon_WGS;

        bl:=geo_WGS84_to_Pulkovo42(bl);
        

        db_AddRecord_(mem_Data, [
             'address', vNode.Attributes['display_name'],
             'lat', bl.B,
             'lon', bl.L

//             'lat', eLat_WGS,
//             'lon', eLon_WGS
             ]);
    end;  
  
end;





end.


{


//------------------------------------------------------------------------------
class function Tdlg_Map_Goto_Address.ShowDlg(): boolean;
//------------------------------------------------------------------------------
var
  oForm: TForm;
begin
  oForm:= FindForm (Self.ClassName);

  if oForm = nil then
     oForm:= Tdlg_Map_Goto_Address.Create(Application);

  oForm.Show();
end;



//--------------------------------------------------------------------
procedure Tframe_MapView_base.TempLayer_Clear(aName: string);
//--------------------------------------------------------------------
begin
  mapx_DeleteLayerFeatures(FMap, aName);
end;


//--------------------------------------------------------------------
procedure Tframe_MapView_base.RemoveLayerByName(aName: string);
//--------------------------------------------------------------------
var
  iIndex: Integer;
begin
  iIndex:= GetLayerIndexByName_from_1(aName);
  if iIndex<>-1 then
    FMap.Layers.Remove (iIndex);
end;



  
  try
    oStrList:= TStringList.Create;

    //---------------------------------------
    // ������� ������ �� yandex-������ ...
    //---------------------------------------

    oStrList.Text:= webgeo_GetXMLByAddress(ed_Search.Text, 15);
    sComment:='';

    //---------------------------------------
    // ��������� XML-����� �� yandex-������� ...
    //---------------------------------------

    for i:=0 to oStrList.Count-1 do
    begin
      if parser_IsTag(oStrList[i], 'CountryName', sValue) then
      begin
        if sComment <> '' then sComment:= ',  '+ sComment;
        sComment:= sValue + sComment;
      end;

      if parser_IsTag(oStrList[i], 'AddressLine', sValue) then
      begin
        if sComment <> '' then sComment:= sComment + ',  ';
        sComment:= sComment + sValue;
      end;

      if parser_IsTag(oStrList[i], 'SubAdministrativeAreaName', sValue) then
      begin
        if sComment <> '' then sComment:= sComment + ',  ';
        sComment:= sComment + sValue;
      end;

      if parser_IsTag(oStrList[i], 'pos', sValue) then
      begin
        arrStr:= StringToStrArray(sValue, ' ');

        if High(arrStr) >= 1 then
        begin
          posBL.B:= AsFloat(arrStr[1]);
          posBL.L:= AsFloat(arrStr[0]);


          bl_Pulkovo:=geo_WGS84_to_Pulkovo42(posBL);



          
          db_AddRecord_(mem_Data, [
                                  FLD_ID,       mem_Data.RecordCount+1,
                                  FLD_COMMENT,  sComment,
                                  FLD_POSITION, geo_FormatBLPoint(posBL),
                                  FLD_LAT,      AsFloat(bl_Pulkovo.B),
                                  FLD_LON,      AsFloat(bl_Pulkovo.L),

                                  FLD_LAT_STR, geo_FormatDegree(bl_Pulkovo.B),
                                  FLD_LON_STR, geo_FormatDegree(bl_Pulkovo.L)
                                   
                                  ]);
        end;

        sComment:='';
      end;
    end;


    
// ---------------------------------------------------------------
procedure Tdlg_Map_Search_Yandex.UpdateStatusBar;
// ---------------------------------------------------------------
begin
  //StatusBar1.SimpleText:=  Format('%d �������',[mem_Data.RecordCount]);
end;
