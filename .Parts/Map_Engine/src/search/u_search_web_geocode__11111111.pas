unit u_search_web_geocode__11111111;

interface
uses
  Windows, SysUtils, Variants, Classes, WinInet, Dialogs,

 // System.SysUtils, System.Classes,  Vcl.Dialogs,  

  IdHTTP,

//  u_func_dlg,
  u_func_arr,
  u_func;  


const
//  CONST_YANDEX_GEOCODE = 'https://geocode-maps.yandex.ru/1.x/?apikey=3aba4e44-9a57-40dc-b302-56487078c548&geocode=';
  CONST_YANDEX_GEOCODE = 'https://geocode-maps.yandex.ru/1.x/?apikey=3aba4e44-9a57-40dc-b302-56487078c548&geocode=';


  //https://geocode-maps.yandex.ru/1.x/?apikey=3aba4e44-9a57-40dc-b302-56487078c548&geocode=bdfyjdj&results=10

  //https://geocode-maps.yandex.ru/1.x/?apikey=3aba4e44-9a57-40dc-b302-56487078c548&geocode=Tverskaya+6
  

  //https://nominatim.openstreetmap.org/search/<query>?q=moscow&format=xml
  { TODO : add  https://nominatim.openstreetmap.org/search/<query>?q=moscow&format=xml }
  
type
  //===============================
  TGeoObjItem = class
  //===============================
  public
    ID      : integer;
    Name    : string;
    Lat_WGS : double;
    Lon_WGS : double;
    Address : string;
    Tag     : integer;
  end;


  //===============================
  TGeoObjList = class (TList)
  //===============================
  private
    function GetItem(aIndex: integer): TGeoObjItem;
  public
    destructor Destroy; override;

    function  Add_(aID: integer; const aName: string; aLat_WGS, aLon_WGS: double): TGeoObjItem; overload;
    function  Add_(aID: integer; const aName, aAddress: string): TGeoObjItem; overload;

    procedure Refresh_LatLon_by_Address();
    procedure Refresh_Address_by_LatLon(aAddCountry: boolean=true);

    procedure Clear();

    property Items_[aIndex: integer]: TGeoObjItem read GetItem; default;
  end;


  //-------------------------------

  function webgeo_GetTextByURL(aWebSession: Pointer; const aURL_Utf8: string; aProxyLogin: string='';
           aProxyPassword: string=''; aShowError: boolean=false): string; overload;

  function webgeo_GetTextByURL(const aURL: string; aProxyLogin: string = '';
      aProxyPassword: string = ''; aShowError: boolean = false): string; overload;

  function webgeo_GetXMLByAddress(const aAddress: string; aMaxResultCount: integer=15;
           aProxyLogin: string=''; aProxyPassword: string='';
           aShowError: boolean=false): string;

  function webgeo_GetXMLByLatLon(aLatWGS, aLonWGS: double; aMaxResultCount: integer=1;
           aProxyLogin: string=''; aProxyPassword: string='';
           aShowError: boolean=false): string;

  function webgeo_GetAddressByLatLon(aLatWGS, aLonWGS: double; aProxyLogin: string='';
           aProxyPassword: string=''; aShowError: boolean=false;
           aAddCountry: boolean=true): string;

  function webgeo_GetCoordByAddress(const aAddress: string; var aLatWGS,
           aLonWGS: double; var aAddress_res: string): boolean;


  function webgeo_GetAddressFromXML(aXMLStrList: TStringList; aAddCountry: boolean=true): string;
  function webgeo_GetLatLonFromXML(aXMLStrList: TStringList; var aLat_WGS, aLon_WGS: double; var aAddress: string): boolean;

  function parser_IsTag(const aLineXML, aTag: string; var aValue: string): boolean;

  
//==============================================================================
implementation


//==============================================================================



//------------------------------------------------------------------------------
function parser_IsTag(const aLineXML, aTag: string; var aValue: string): boolean;
//------------------------------------------------------------------------------
var k: integer;
begin
  k:= Pos (AnsiUpperCase('<'+aTag+'>'), AnsiUpperCase(aLineXML));
  Result:= k > 0;

  if not Result then exit;

  aValue:= aLineXML;
  delete(aValue, 1, k+Length(aTag)+1);

  k:= Pos (AnsiUpperCase('</'+aTag+'>'), AnsiUpperCase(aValue));
  if k <= 0 then exit;

  aValue:= copy(aValue, 1, k-1);
end;



//------------------------------------------------------------------------------
// ������� Web-������ � ��������� XML-����� ...
//------------------------------------------------------------------------------
function webgeo_GetTextByURL(const aURL: string; aProxyLogin: string = '';
    aProxyPassword: string = ''; aShowError: boolean = false): string;
//------------------------------------------------------------------------------
var
  hSession: Pointer;
  IdHTTP1: TIdHTTP;

begin
  
  Result:='';
  
  try
    hSession:= InternetOpen (pChar('Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  .NET CLR 2.0.50727)'), INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
    Result:= webgeo_GetTextByURL(hSession, aURL, aProxyLogin, aProxyPassword, aShowError);
  finally
    InternetCloseHandle(hSession);
        
  end;
  
end;



//------------------------------------------------------------------------------
// ��������� XML-����� ��� Web-������ ...
//------------------------------------------------------------------------------
function webgeo_GetTextByURL(aWebSession: Pointer; const aURL_Utf8: string; aProxyLogin: string='';
         aProxyPassword: string=''; aShowError: boolean=false): string;
//------------------------------------------------------------------------------
var
  hFile: Pointer;
  iStatusType, iTypeSize, iReserved: dword;
  arrType: array [1..20] of char;
  sError: string;

          //---------------------------------------------------
          function DoProxyAuthentication(): boolean;
          //--------------------------------------------------- 
          begin
            Result:= false;
            if aProxyLogin = '' then exit;

            try
              InternetSetOption (hFile, INTERNET_OPTION_PROXY_USERNAME, PChar(aProxyLogin), Length(aProxyLogin));
              InternetSetOption (hFile, INTERNET_OPTION_PROXY_PASSWORD, PChar(aProxyPassword), Length(aProxyPassword));
              HttpSendRequest (hFile, nil, 0, nil, 0);

              iTypeSize   := SizeOf(arrType);
              iReserved   := 0;
              iStatusType := 0;

              if HttpQueryInfo(hFile, HTTP_QUERY_STATUS_CODE, @arrType, iTypeSize, iReserved) then
                 iStatusType:= StrToInt (pchar(@arrType));

              Result:= (iStatusType <> HTTP_STATUS_PROXY_AUTH_REQ);

              if not Result then
                 sError:='������ �������������� �� Proxy';
            except
            end;
          end;

var
  iBufferLen: LongWord;
  arrBuffer: array [1..64535] of Ansichar;
  bOk: boolean;          
begin
  Result:='';
  sError:='';
  bOk:=true;

  //--------------------------------
  
  if not Assigned(aWebSession) then
  begin
//    sError:= GetLastErrorMessage(GetLastError());
    bOk:=false;
  end;

  //--------------------------------

  if bOk then
  begin
    hFile:= InternetOpenUrl (aWebSession, PChar(aURL_Utf8), nil, 0, INTERNET_FLAG_DONT_CACHE or INTERNET_FLAG_KEEP_CONNECTION or INTERNET_FLAG_RELOAD, 0);

    if not Assigned(hFile) then
    begin
//      sError:= GetLastErrorMessage(GetLastError());
      bOk:=false;
    end;
  end;

  //--------------------------------

  if bOk then
  try
    iTypeSize   := SizeOf(arrType);
    iReserved   := 0;
    iStatusType := 0;

    if HttpQueryInfo(hFile, HTTP_QUERY_STATUS_CODE, @arrType, iTypeSize, iReserved) then
       iStatusType:= StrToInt (pchar(@arrType));

    if iStatusType = HTTP_STATUS_PROXY_AUTH_REQ then
       bOk:= DoProxyAuthentication();

    //------------------------------

    if bOk then
    repeat
      if not InternetReadFile(hFile, @arrBuffer, SizeOf(arrBuffer), iBufferLen) then Break;
      Result:= Result + copy(arrBuffer, 1, iBufferLen);
    until
      (iBufferLen = 0) and (iBufferLen < SizeOf(arrBuffer));

  finally
    InternetCloseHandle(hFile);
  end;

  //--------------------------------
  
  if bOk then Result:= Utf8ToAnsi(Result)
         else 
           if aShowError then ShowMessage(sError);
end;



//------------------------------------------------------------------------------
// ��������� � yandex-������� ����� �� ������ �������������� � ���� XML-��������� ...
//------------------------------------------------------------------------------
function webgeo_GetXMLByAddress(const aAddress: string; aMaxResultCount: integer=15;
         aProxyLogin: string=''; aProxyPassword: string='';
         aShowError: boolean=false): string;
//------------------------------------------------------------------------------
var sURL: string;
begin
  sURL:= CONST_YANDEX_GEOCODE + AnsiToUtf8(aAddress);

  if aMaxResultCount > 0 then
     sURL:= sURL + format('&results=%d', [aMaxResultCount]);

  Result:= webgeo_GetTextByURL (sURL, aProxyLogin, aProxyPassword, aShowError);
end;



//------------------------------------------------------------------------------
function webgeo_GetXMLByLatLon(aLatWGS, aLonWGS: double; aMaxResultCount: integer=1;
         aProxyLogin: string=''; aProxyPassword: string='';
         aShowError: boolean=false): string;
//------------------------------------------------------------------------------
var sURL: string;
begin
  sURL:= CONST_YANDEX_GEOCODE + AsString(aLonWGS) +','+ AsString(aLatWGS);

  if aMaxResultCount > 0 then
     sURL:= sURL + format('&results=%d', [aMaxResultCount]);

  Result:= webgeo_GetTextByURL (sURL, aProxyLogin, aProxyPassword, aShowError);
end;



//------------------------------------------------------------------------------
function webgeo_GetAddressByLatLon(aLatWGS, aLonWGS: double; aProxyLogin: string='';
         aProxyPassword: string=''; aShowError: boolean=false;
         aAddCountry: boolean=true): string;
//------------------------------------------------------------------------------
var
  oXMLStrList: TStringList;
begin
  Result:='';

  try
    oXMLStrList:= TStringList.Create;
    oXMLStrList.Text:= webgeo_GetXMLByLatLon(aLatWGS, aLonWGS, 1, aProxyLogin, aProxyPassword, aShowError);  
    Result:= webgeo_GetAddressFromXML(oXMLStrList, aAddCountry);
  finally
    FreeAndNil(oXMLStrList);
  end;
end;



//------------------------------------------------------------------------------
function webgeo_GetAddressFromXML(aXMLStrList: TStringList; aAddCountry: boolean=true): string;
//------------------------------------------------------------------------------
var
  i: integer;
  sValue: string;
begin
  Result:='';
  
  for i:=0 to aXMLStrList.Count-1 do
  begin
    if aAddCountry and parser_IsTag(aXMLStrList[i], 'CountryName', sValue) then
    begin
      if Result <> '' then Result:= ',  '+ Result;
      Result:= sValue + Result;
    end;

    if parser_IsTag(aXMLStrList[i], 'AddressLine', sValue) then
    begin
      if Result <> '' then Result:= Result + ',  ';
      Result:= Result + sValue;
    end;
  end;
end;



//------------------------------------------------------------------------------
function webgeo_GetCoordByAddress(const aAddress: string; var aLatWGS,
         aLonWGS: double; var aAddress_res: string): boolean;
//------------------------------------------------------------------------------
var
  i: integer;
  sValue: string;
  oXMLStrList: TStringList;
  arrStr: TStrArray; 
begin
  Result:=false;

  aAddress_res:='';
  aLatWGS:=0;
  aLonWGS:=0;
  
  try
    oXMLStrList:= TStringList.Create;

    //---------------------------------------
    // ������� ������ �� yandex-������ ...
    //---------------------------------------

    oXMLStrList.Text:= webgeo_GetXMLByAddress (trim(aAddress), 1);

    //---------------------------------------
    // ��������� XML-����� �� yandex-������� ...
    //---------------------------------------

    Result:= webgeo_GetLatLonFromXML (oXMLStrList, aLatWGS, aLonWGS, aAddress_res);
  finally
    FreeAndNil(oXMLStrList);
  end;
end;



//------------------------------------------------------------------------------
function webgeo_GetLatLonFromXML(aXMLStrList: TStringList; var aLat_WGS,
         aLon_WGS: double; var aAddress: string): boolean;
//------------------------------------------------------------------------------
var
  i: integer;
  sValue, sAddress: string;
  arrStr: TStrArray;
begin
  Result:=false;
  sAddress:='';

  //---------------------------------------

  for i:=0 to aXMLStrList.Count-1 do
  begin
    if parser_IsTag(aXMLStrList[i], 'CountryName', sValue) then
    begin
      if sAddress <> '' then sAddress:= ',  '+ sAddress;
      sAddress:= sValue + sAddress;
    end;

    if parser_IsTag(aXMLStrList[i], 'AddressLine', sValue) then
    begin
      if sAddress <> '' then sAddress:= sAddress + ',  ';
      sAddress:= sAddress + sValue;
    end;

    if parser_IsTag(aXMLStrList[i], 'SubAdministrativeAreaName', sValue) then
    begin
      if sAddress <> '' then sAddress:= sAddress + ',  ';
      sAddress:= sAddress + sValue;
    end;

    if parser_IsTag(aXMLStrList[i], 'pos', sValue) then
    begin
      arrStr:= StringToStrArray(sValue, ' ');

      if High(arrStr) >= 1 then
      begin
        aLat_WGS:= AsFloat(arrStr[1]);
        aLon_WGS:= AsFloat(arrStr[0]);
      end;
    end;
  end;

  //---------------------------------------

  Result:= (sAddress <> '') and (aLat_WGS <> 0) and (aLon_WGS <> 0);
  if Result then aAddress:=sAddress;

  SetLength(arrStr,0);
end;




//------------------------------------------------------------------------------
//                      TGeoObjList = class (TList)
//------------------------------------------------------------------------------
destructor TGeoObjList.Destroy;
//------------------------------------------------------------------------------
begin
  Clear();
  inherited;
end;


//------------------------------------------------------------------------------
function TGeoObjList.Add_(aID: integer; const aName: string; aLat_WGS, aLon_WGS: double): TGeoObjItem;
//------------------------------------------------------------------------------
begin
  Result:= TGeoObjItem.Create;
  inherited Add(Result);

  Result.ID      := aID;
  Result.Name    := aName;
  Result.Lat_WGS := aLat_WGS;
  Result.Lon_WGS := aLon_WGS;
end;


//------------------------------------------------------------------------------
function TGeoObjList.Add_(aID: integer; const aName, aAddress: string): TGeoObjItem;
//------------------------------------------------------------------------------
begin
  Result:= Add_(aID, aName, 0, 0);
  Result.Address:= aAddress;
end;



//------------------------------------------------------------------------------
procedure TGeoObjList.Refresh_LatLon_by_Address();
//------------------------------------------------------------------------------
var
  i,k: integer;
  hSession: Pointer;
  oXMLStrList: TStringList;
  oGeoItem: TGeoObjItem;
  sURL, sValue: string;
begin
  try
    hSession:= InternetOpen (pChar('Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727)'), INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);

    if Assigned(hSession) then
    try
      oXMLStrList:= TStringList.Create;

      for i:=0 to Count-1 do
      begin
        oGeoItem:= GetItem(i);
        oGeoItem.Address:= trim(oGeoItem.Address);
        if oGeoItem.Address = '' then Continue;

        oXMLStrList.Clear;

        sURL:= CONST_YANDEX_GEOCODE + AnsiToUtf8(oGeoItem.Address) + '&results=1';
        oXMLStrList.Text:= webgeo_GetTextByURL(hSession, sURL);

        webgeo_GetLatLonFromXML (oXMLStrList, oGeoItem.Lat_WGS, oGeoItem.Lon_WGS, oGeoItem.Address);
      end;
    finally
      FreeAndNil(oXMLStrList);
    end;

  finally
    InternetCloseHandle(hSession);
  end;
end;


//------------------------------------------------------------------------------
procedure TGeoObjList.Refresh_Address_by_LatLon(aAddCountry: boolean=true);
//------------------------------------------------------------------------------
var
  i,k: integer;
  hSession: Pointer;
  oXMLStrList: TStringList;
  oGeoItem: TGeoObjItem; 
  sURL, sValue: string;
begin
  try
    hSession:= InternetOpen (pChar('Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727)'), INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);

    if Assigned(hSession) then
    try
      oXMLStrList:= TStringList.Create;
      
      for i:=0 to Count-1 do
      begin
        oGeoItem:= GetItem(i);
        oXMLStrList.Clear;

        sURL:= CONST_YANDEX_GEOCODE + AsString(oGeoItem.Lon_WGS) +','+ AsString(oGeoItem.Lat_WGS) + '&results=1';
        oXMLStrList.Text:= webgeo_GetTextByURL(hSession, sURL);

        oGeoItem.Address:= webgeo_GetAddressFromXML (oXMLStrList, aAddCountry);
      end;
    finally
      FreeAndNil(oXMLStrList);
    end;

  finally
    InternetCloseHandle(hSession);
  end;
end;



//------------------------------------------------------------------------------
function TGeoObjList.GetItem(aIndex: integer): TGeoObjItem;
//------------------------------------------------------------------------------
begin
  Result:= TGeoObjItem (inherited Items[aIndex]);
end;


//------------------------------------------------------------------------------
procedure TGeoObjList.Clear();
//------------------------------------------------------------------------------
var i: integer;
begin
  for i:=Count-1 downto 0 do GetItem(i).Free;
  inherited Clear;
end;


end.

