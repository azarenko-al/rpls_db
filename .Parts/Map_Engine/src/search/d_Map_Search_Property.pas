unit d_Map_Search_Property;

interface

uses
  Forms,  Dialogs, SysUtils,
  Buttons, rxPlacemnt, Classes, ActnList, DB, ADODB, ComCtrls, Controls,
  Grids, DBGrids, StdCtrls, ToolWin, ExtCtrls, System.Actions, cxLookAndFeels,

//  RPLS_search_TLB,

//  dm_Main_search,

 // dm_Main,

//  u_const_db,
  u_db,

  u_func,
  
  u_mapx,
  
//  u_cx,
  MapXLib_TLB,    
  
  dxBar, cxBarEditItem, cxClasses;

type
 // TOnSearchFind = procedure (aLat: Double; aLon: Double) of object;


  Tdlg_Map_Search_Property = class(TForm)
    ds_Prop: TDataSource;
    qry_Prop: TADOQuery;
    DBGrid1: TDBGrid;
    ActionList1: TActionList;
    StatusBar1: TStatusBar;
    FormStorage1: TFormStorage;
    act_Search: TAction;
    Panel1: TPanel;
    Label1: TLabel;
    ed_Name: TEdit;
    Label3: TLabel;
    ed_Address: TEdit;
    Button1: TButton;
    ADOConnection2: TADOConnection;
    Bevel2: TBevel;
    Button3: TButton;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    cxBarEditItem1: TcxBarEditItem;
    cxLookAndFeelController1: TcxLookAndFeelController;
    cxBarEditItem2: TcxBarEditItem;
    cxBarEditItem3: TcxBarEditItem;
    dxBarButton1: TdxBarButton;
    procedure FormDestroy(Sender: TObject);
    procedure act_SearchExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure ed_CodeChange(Sender: TObject);
  private
    FMap: CMapX;
 //   FProject_id: Integer;
  
 //   FEvents: ISearchEventsX;

//    FProc: TOnSearchFind;


    procedure LocateOnMap;
//    procedure Open;
    procedure Search;
    procedure UpdateStatusBar;
    
  public
  //  FMapEngine.SetCenter_LatLon(aLat,aLon);

  
 //   class procedure ExecDlg;
    class procedure ExecDlg(aMap: CMapX; aConnectionStr: string; aProject_id:
        Integer);

  end;

{var
  dlg_search_Property: Tdlg_search_Property;
}


implementation

{$R *.dfm}


//------------------------------------------------------------------------------
class procedure Tdlg_Map_Search_Property.ExecDlg(aMap: CMapX; aConnectionStr:
    string; aProject_id: Integer);
//------------------------------------------------------------------------------
var
  sSQL: string;
  oForm: Tdlg_Map_Search_Property;
  
begin
//  if True then

  
//var 
//  Form: TForm;
//begin
//  oForm := Application.FindComponent(Tdlg_Map_Search_Property.ClassName) as TForm;
//  if Assigned(Form) then
//    Form.Show
//  else  
  

//  ShowMessage('class procedure Tdlg_search_Property.ExecDlg    - 1');

  with Tdlg_Map_Search_Property.Create(Application) do
  begin
    FMap:=aMap;
  
   // FEvents:=aEvents;

    db_OpenADOConnectionString(ADOConnection2, aConnectionStr);
   
//    ADOConnection2.Close;
//    ADOConnection2.ConnectionString:=aConnectionStr;
//    ADOConnection2.Open;
    

   
//    FProc:=aProc;

    Assert(aProject_id > 0);
//    FProject_id:=aProject_id;
    
    
    sSQL:=Format('SELECT id,name,address,lat,lon FROM PROPERTY  WHERE project_id=%d', [aProject_id]);
//    sSQL:=Format('SELECT id,name,address,code,lat,lon FROM PROPERTY  WHERE project_id=%d', [aProject_id]);

    db_OpenQuery(qry_Prop, sSQL);
    
    UpdateStatusBar;
    
//    Show;
    
    ShowModal;    
    Free;
  end;

end;

// ---------------------------------------------------------------
procedure Tdlg_Map_Search_Property.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  Caption:='����� ��������...';

//  TdmMain_search.Init;

  DBGrid1.Align:=alClient;

//  qry_Prop.Connection:=dmMain.ADOConnection1;
  
  qry_Prop.Filtered:=False;
                      
end;

procedure Tdlg_Map_Search_Property.FormDestroy(Sender: TObject);
begin
  mapx_Layer_Delete(FMap, 'temp');
end;


procedure Tdlg_Map_Search_Property.act_SearchExecute(Sender: TObject);
begin
  if Sender=act_Search then
    Search;
end;

procedure Tdlg_Map_Search_Property.Button1Click(Sender: TObject);
begin
  Search;


//  cx_Search(cxGridDBTableView1, Trim(Edit1.Text));
end;                 

procedure Tdlg_Map_Search_Property.cxGridDBTableView1DblClick(Sender: TObject);
begin
  //ShowMessage('');
end;


procedure Tdlg_Map_Search_Property.DBGrid1DblClick(Sender: TObject);
begin
  LocateOnMap();
end;

procedure Tdlg_Map_Search_Property.ed_CodeChange(Sender: TObject);
begin
  Search;
end;


procedure Tdlg_Map_Search_Property.LocateOnMap;
var
  vLayer: CMapxLayer;
  eLat: Double;
  eLon: Double;
  
//  vIMapView: IMapViewX;
begin
  
 
  if qry_Prop.RecordCount>0 then
  begin
    eLat:=qry_Prop[FLD_LAT];
    eLon:=qry_Prop[FLD_LON];  

    FMap.ZoomTo(FMap.Zoom,  eLon, eLat); 

    mapx_Layer_Delete(FMap,'temp');
                         
    vLayer:=mapx_TempLayer_Create(FMap,'temp');
                       
    mapx_AddSymbol_(FMap, vLayer, eLat, eLon, MI_SYMBOL_CROSS );

//    function mapx_AddSymbol_  (aMap: CMapX;
//                          aVLayer: CMapXLayer;
//                          
//                          aLat: double;
//                          aLon: double;
    

    
  end;

// x:=aMap.CenterX;
//  y:=aMap.CenterY;
//
//  
//  aMap.ZoomTo (aMap.Zoom, aBLPoint.L, aBLPoint.B); 
  
//    if Assigned(FProc) then
//      FProc(qry_Prop[FLD_LAT], qry_Prop[FLD_LON]);
      
end;



// ---------------------------------------------------------------
procedure Tdlg_Map_Search_Property.UpdateStatusBar;
// ---------------------------------------------------------------
begin
  StatusBar1.SimpleText:=  Format('%d �������',[qry_Prop.RecordCount]);
end;

// ---------------------------------------------------------------
procedure Tdlg_Map_Search_Property.Search;
// ---------------------------------------------------------------
var
  sAddress: string;
//  sCode: string;
  sName: string;
begin
  sName   :=Trim(ed_name.Text);
  sAddress:=Trim(ed_Address.Text);
 // sCode   :=Trim(ed_Code.Text);


  qry_Prop.Filter :='';

  if sName<>'' then
    qry_Prop.Filter := Format('(name LIKE %s)', ['%'+sName+'%']);

  if sAddress<>'' then
    qry_Prop.Filter := Format('(Address LIKE %s)', ['%'+sAddress+'%']);

//  if sCode<>'' then
//    qry_Prop.Filter := Format('(Code LIKE %s)', ['%'+sCode+'%']);



  qry_Prop.Filtered :=qry_Prop.Filter<>'';
    
  UpdateStatusBar;

//  cx_Search(cxGridDBTableView1, Trim(Edit1.Text));   
end;



end.


{
  cx_Search(cxGrid1DBBandedTableView1_parcel, Trim(ed_Search.Text));
       }


       
{


//------------------------------------------------------------------------------
class function Tdlg_Map_Goto_Address.ShowDlg(): boolean;
//------------------------------------------------------------------------------
var
  oForm: TForm;
begin
  oForm:= FindForm (Self.ClassName);

  if oForm = nil then
     oForm:= Tdlg_Map_Goto_Address.Create(Application);

  oForm.Show();
end;
