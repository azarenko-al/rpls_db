object dlg_Map_Search_Property: Tdlg_Map_Search_Property
  Left = 979
  Top = 733
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'dlg_Map_Search_Property'
  ClientHeight = 510
  ClientWidth = 768
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    768
    510)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel2: TBevel
    Left = 0
    Top = 472
    Width = 768
    Height = 38
    Align = alBottom
    Constraints.MaxHeight = 38
    Constraints.MinHeight = 38
    Shape = bsTopLine
    ExplicitLeft = -114
    ExplicitTop = 295
    ExplicitWidth = 852
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 57
    Width = 768
    Height = 67
    Align = alTop
    DataSource = ds_Prop
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'name'
        Title.Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        Width = 226
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'address'
        Title.Caption = #1040#1076#1088#1077#1089
        Width = 362
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'code'
        Title.Caption = #1050#1086#1076
        Visible = False
      end>
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 453
    Width = 768
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 28
    Width = 768
    Height = 29
    Align = alTop
    BevelOuter = bvLowered
    BorderWidth = 3
    TabOrder = 2
    Visible = False
    object Label1: TLabel
      Left = 5
      Top = 6
      Width = 51
      Height = 13
      Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '
    end
    object Label3: TLabel
      Left = 187
      Top = 6
      Width = 31
      Height = 13
      Caption = #1040#1076#1088#1077#1089
    end
    object ed_Name: TEdit
      Left = 61
      Top = 3
      Width = 118
      Height = 21
      TabOrder = 0
      OnChange = ed_CodeChange
    end
    object ed_Address: TEdit
      Left = 230
      Top = 3
      Width = 133
      Height = 21
      TabOrder = 1
      OnChange = ed_CodeChange
    end
    object Button1: TButton
      Left = 369
      Top = 3
      Width = 65
      Height = 22
      Action = act_Search
      TabOrder = 2
    end
  end
  object Button3: TButton
    Left = 689
    Top = 482
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = #1047#1072#1082#1088#1099#1090#1100
    ModalResult = 2
    TabOrder = 3
  end
  object ds_Prop: TDataSource
    DataSet = qry_Prop
    Left = 360
    Top = 201
  end
  object qry_Prop: TADOQuery
    Connection = ADOConnection2
    CursorType = ctStatic
    Filter = 'address like %210%'
    Filtered = True
    Parameters = <>
    SQL.Strings = (
      'select top 100 '
      ' id,name, address, lat, lon'
      ' '
      '  from property')
    Left = 360
    Top = 144
  end
  object ActionList1: TActionList
    Left = 56
    Top = 200
    object act_Search: TAction
      Caption = #1055#1086#1080#1089#1082
      OnExecute = act_SearchExecute
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    UseRegistry = True
    StoredValues = <>
    Left = 56
    Top = 144
  end
  object ADOConnection2: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Password=sa;Persist Security Info=True;User I' +
      'D=sa;Data Source=onega_link'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'MSDASQL.1'
    Left = 227
    Top = 144
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    CanCustomize = False
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = True
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 515
    Top = 149
    DockControlHeights = (
      0
      0
      28
      0)
    object dxBarManager1Bar1: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 991
      FloatTop = 810
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 121
          Visible = True
          ItemName = 'cxBarEditItem1'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 226
          Visible = True
          ItemName = 'cxBarEditItem2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton1'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = #1053#1072#1079#1074#1072#1085#1080#1077
      Category = 0
      Hint = #1053#1072#1079#1074#1072#1085#1080#1077
      Visible = ivAlways
      ShowCaption = True
      Width = 130
    end
    object cxBarEditItem2: TcxBarEditItem
      Caption = #1040#1076#1088#1077#1089
      Category = 0
      Hint = #1040#1076#1088#1077#1089
      Visible = ivAlways
      ShowCaption = True
      Width = 130
    end
    object cxBarEditItem3: TcxBarEditItem
      Caption = #1050#1086#1076
      Category = 0
      Hint = #1050#1086#1076
      Visible = ivAlways
      ShowCaption = True
      Width = 130
    end
    object dxBarButton1: TdxBarButton
      Action = act_Search
      Category = 0
    end
  end
  object cxLookAndFeelController1: TcxLookAndFeelController
    Kind = lfFlat
    Left = 648
    Top = 152
  end
end
