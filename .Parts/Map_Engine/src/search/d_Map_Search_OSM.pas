unit d_Map_Search_OSM;

interface

uses
  u_geo_convert_new,

//  function geo_WGS84_to_Pulkovo42(aBLPoint: TBLPoint): TBLPoint;

//
////--------------------------------------------------------------------
//procedure Tframe_MapView_base.TempLayer_Clear(aName: string);
////--------------------------------------------------------------------
//begin
//  mapx_DeleteLayerFeatures(FMap, aName);
//end;
//


{  System. System.Variants, System.Classes, 
  Vcl.Controls, Vcl. RxPlacemnt, 
  Vcl.ActnList, Data.DB, Vcl.DBGrids, Vcl.StdCtrls,
  Vcl.ComCtrls, dxmdaset, Vcl.Buttons,
}
 // RPLS_search_TLB,

//  dm_Main_search,

//  u_search_web_geocode,

  u_mapx,

  u_geo,

  u_func,
  u_func_arr,


  u_db,


  Forms, Dialogs,
  MapXLib_TLB,    


  System.Bindings.Outputs, Vcl.Bind.Editors, Datasnap.DBClient,
  Data.Bind.Components, REST.Response.Adapter, REST.Client,
  Data.Bind.ObjectScope, Data.DB, dxmdaset, System.Classes, System.Actions,
  Vcl.ActnList, RxPlacemnt, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls,
  Vcl.Controls, Vcl.Grids, Vcl.DBGrids, Vcl.Buttons, Vcl.ToolWin, IPPeerClient,
  Data.Bind.EngExt, Vcl.Bind.DBEngExt, System.Rtti, Vcl.Menus, IPPeerServer,
  Datasnap.DSCommonServer, Datasnap.DSTCPServerTransport;

//  System.Actions, Vcl.Grids, Vcl.ToolWin, DB, Classes, ActnList, ComCtrls,


type
//  TOnSearchFind = procedure (aLat: Double; aLon: Double) of object;


  Tdlg_Map_Search_OSM = class(TForm)
    ToolBar1: TToolBar;
    ed_Search: TEdit;
    FormStorage1: TFormStorage;
    ToolButton1: TToolButton;
    ActionList1: TActionList;
    act_Search: TAction;
    BitBtn1: TBitBtn;
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    Button3: TButton;
    RESTClient1: TRESTClient;
    RESTRequest1: TRESTRequest;
    RESTResponse1: TRESTResponse;
    RESTResponseDataSetAdapter1: TRESTResponseDataSetAdapter;
    DBGrid2: TDBGrid;
    BindingsList1: TBindingsList;
    LinkControlToField1: TLinkControlToField;
    ClientDataSet1: TClientDataSet;
    DataSource1: TDataSource;
    act_LocateOnMap: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    DSTCPServerTransport1: TDSTCPServerTransport;
    procedure act_LocateOnMapExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure act_SearchExecute(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
//    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    FMap:  MapXLib_TLB.CMapX;

    FDataset: TDataset;
  
    procedure LocateOnMap;
  //   FEvents: ISearchEventsX;
//    FProc: TOnSearchFind;


    procedure Search;
    procedure UpdateStatusBar;

  public
    class procedure ExecDlg(aMap: CMapX);

  end;


var
  dlg_Map_Search_OSM: Tdlg_Map_Search_OSM;
  

implementation

{$R *.dfm}


procedure Tdlg_Map_Search_OSM.act_LocateOnMapExecute(Sender: TObject);
begin
  LocateOnMap;
end;

procedure Tdlg_Map_Search_OSM.FormDestroy(Sender: TObject);
begin
  if assigned(FMap) then
    mapx_Layer_Delete(FMap, 'temp');
    
end;

{
var
  dlg_Yandex_search: Tdlg_Yandex_search;
 }




//------------------------------------------------------------------------------
class procedure Tdlg_Map_Search_OSM.ExecDlg(aMap: CMapX);
//------------------------------------------------------------------------------
var
  sSQL: string;
begin
  with Tdlg_Map_Search_OSM.Create(Application) do
  begin
    FMap:=aMap;

//    FProc:=aProc;

  //  if aEvents<>nil then
   //   FEvents:=aEvents;

    
    UpdateStatusBar;
    
    ShowModal;

    Free;
  end;

end;



procedure Tdlg_Map_Search_OSM.act_SearchExecute(Sender: TObject);
begin
  if Sender=act_Search then
    Search

end;

procedure Tdlg_Map_Search_OSM.DBGrid1DblClick(Sender: TObject);
begin
  LocateOnMap;
end;

procedure Tdlg_Map_Search_OSM.DBGrid2DblClick(Sender: TObject);
begin
   LocateOnMap;
end;



procedure Tdlg_Map_Search_OSM.LocateOnMap;
var
  vLayer: CMapxLayer;
  eLat: Double;
  eLon: Double;
  
//  vIMapView: IMapViewX;
begin
         
  if FDataset.RecordCount>0 then
  begin
    eLat:=FDataset[FLD_LAT];
    eLon:=FDataset[FLD_LON];  

    FMap.ZoomTo(FMap.Zoom,  eLon, eLat); 
    
    mapx_Layer_Delete(FMap,'temp');
    
    vLayer:=mapx_TempLayer_Create(FMap,'temp');
  
//    mapx_DeleteLayerFeatures(FMap,'temp');
  
    mapx_AddSymbol_(FMap, vLayer, eLat, eLon , MI_SYMBOL_CROSS);

//    function mapx_AddSymbol_  (aMap: CMapX;
//                          aVLayer: CMapXLayer;
//                          
//                          aLat: double;
//                          aLon: double;
    

    
  end;

// x:=aMap.CenterX;
//  y:=aMap.CenterY;
//
//  
//  aMap.ZoomTo (aMap.Zoom, aBLPoint.L, aBLPoint.B); 
  
//    if Assigned(FProc) then
//      FProc(qry_Prop[FLD_LAT], qry_Prop[FLD_LON]);
      
end;



// ---------------------------------------------------------------
procedure Tdlg_Map_Search_OSM.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
if ClientDataSet1.Active then
  ShowMessage('procedure Tdlg_Map_Search_OSM.FormCreate(Sender: TObject); - ClientDataSet1.Active');
  


  Caption:= '����� �� ������';
//  DBGrid1.Align:= alClient;

  FDataset:=ClientDataSet1;

  DBGrid2.Align:= alClient;

  
  
  //---------------------------------------------
//  ..����������
  
//  db_CreateField(mem_Data, [db_Field(FLD_ID,       ftInteger),
//                            db_Field(FLD_COMMENT,  ftString),
//                            db_Field(FLD_POSITION, ftString),
//                            db_Field(FLD_LAT,      ftFloat),
//                            db_Field(FLD_LON,      ftFloat), 
//
//                            db_Field(FLD_LAT_STR,  ftString),
//                            db_Field(FLD_LON_STR,  ftString) 
//
//                            ]);

end;

// ---------------------------------------------------------------
procedure Tdlg_Map_Search_OSM.UpdateStatusBar;
// ---------------------------------------------------------------
begin
 // StatusBar1.SimpleText:=  Format('%d �������',[mem_Data.RecordCount]);
end;

//------------------------------------------------------------------------------
procedure Tdlg_Map_Search_OSM.Search;
//------------------------------------------------------------------------------
var
  i: integer;
  sComment, sValue: string;
  oStrList: TStringList;
  arrStr: TStrArray;
  posBL: TBLPoint;

  bl_Pulkovo: TBLPoint;
begin
  RESTRequest1.Params[0].Value:=ed_Search.Text;


  RESTRequest1.Execute;



 {
  if trim(ed_Search.Text) = '' then 
    exit;

  db_Clear(mem_Data);

    
  mem_Data.DisableControls;

  try
    oStrList:= TStringList.Create;

    //---------------------------------------
    // ������� ������ �� yandex-������ ...
    //---------------------------------------

  
    oStrList.Text:= webgeo_GetXMLByAddress(ed_Search.Text, 15);
    sComment:='';

    //---------------------------------------
    // ��������� XML-����� �� yandex-������� ...
    //---------------------------------------

    for i:=0 to oStrList.Count-1 do
    begin
      if parser_IsTag(oStrList[i], 'CountryName', sValue) then
      begin
        if sComment <> '' then sComment:= ',  '+ sComment;
        sComment:= sValue + sComment;
      end;

      if parser_IsTag(oStrList[i], 'AddressLine', sValue) then
      begin
        if sComment <> '' then sComment:= sComment + ',  ';
        sComment:= sComment + sValue;
      end;

      if parser_IsTag(oStrList[i], 'SubAdministrativeAreaName', sValue) then
      begin
        if sComment <> '' then sComment:= sComment + ',  ';
        sComment:= sComment + sValue;
      end;

      if parser_IsTag(oStrList[i], 'pos', sValue) then
      begin
        arrStr:= StringToStrArray(sValue, ' ');

        if High(arrStr) >= 1 then
        begin
          posBL.B:= AsFloat(arrStr[1]);
          posBL.L:= AsFloat(arrStr[0]);


          bl_Pulkovo:=geo_WGS84_to_Pulkovo42(posBL);



          
          db_AddRecord_(mem_Data, [
                                  FLD_ID,       mem_Data.RecordCount+1,
                                  FLD_COMMENT,  sComment,
                                  FLD_POSITION, geo_FormatBLPoint(posBL),
                                  FLD_LAT,      AsFloat(bl_Pulkovo.B),
                                  FLD_LON,      AsFloat(bl_Pulkovo.L),

                                  FLD_LAT_STR, geo_FormatDegree(bl_Pulkovo.B),
                                  FLD_LON_STR, geo_FormatDegree(bl_Pulkovo.L)
                                   
                                  ]);
        end;

        sComment:='';
      end;
    end;

    mem_Data.First;
  finally
    FreeAndNil(oStrList);
  end;

  mem_Data.EnableControls;

 }
  
//  UpdateStatusBar;
end;



end.


{

 
procedure Tdlg_Map_Search_OSM.Button2Click(Sender: TObject);
begin
  ed_Search.Text:='������';
end;




procedure Tdlg_Map_Search_OSM.FormClose(Sender: TObject; var Action:
    TCloseAction);
begin
 // Action:=caFree;
end;



//------------------------------------------------------------------------------
class function Tdlg_Map_Goto_Address.ShowDlg(): boolean;
//------------------------------------------------------------------------------
var
  oForm: TForm;
begin
  oForm:= FindForm (Self.ClassName);

  if oForm = nil then
     oForm:= Tdlg_Map_Goto_Address.Create(Application);

  oForm.Show();
end;



//--------------------------------------------------------------------
procedure Tframe_MapView_base.TempLayer_Clear(aName: string);
//--------------------------------------------------------------------
begin
  mapx_DeleteLayerFeatures(FMap, aName);
end;


//--------------------------------------------------------------------
procedure Tframe_MapView_base.RemoveLayerByName(aName: string);
//--------------------------------------------------------------------
var
  iIndex: Integer;
begin
  iIndex:= GetLayerIndexByName_from_1(aName);
  if iIndex<>-1 then
    FMap.Layers.Remove (iIndex);
end;

