unit d_Property_GeoCoding;

interface

uses
{  System. System.Variants, System.Classes,
  Vcl.Controls, Vcl. RxPlacemnt,
  Vcl.ActnList, Data.DB, Vcl.DBGrids, Vcl.StdCtrls,
  Vcl.ComCtrls, dxmdaset, Vcl.Buttons,
}
    Forms, SysUtils,
 // RPLS_search_TLB,

//  dm_Main,
//  dm_Onega_DB_data,


//  u_const_db,

//  dm_Property,

//  u_search_web_geocode,

  u_geo,

  u_func,
  u_func_arr,


  u_db,

//  System.Actions, Vcl.Grids, Vcl.ToolWin, DB, Classes, ActnList, ComCtrls,
  Controls, Grids, DBGrids, StdCtrls, Buttons, ToolWin, DB, dxmdaset,
  Classes, ActnList, rxPlacemnt, ComCtrls, u_classes, ADODB, ExtCtrls,
  System.Actions;

type
 // TOnSearchFind = procedure (aLat: Double; aLon: Double) of object;


  Tdlg_Property_GeoCoding = class(TForm)
    DBGrid1: TDBGrid;
    FormStorage1: TFormStorage;
    ds_Data: TDataSource;
    StatusBar1: TStatusBar;
    qry_Props: TADOQuery;
    Panel1: TPanel;
    Button1: TButton;
    ActionList2: TActionList;
    act_Save: TAction;
    act_Run: TAction;
    Button2: TButton;
    Button3: TButton;
    procedure act_SaveExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
//    procedure act_SearchExecute(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
//    procedure Button3Click(Sender: TObject);
//    procedure DBGrid1DblClick(Sender: TObject);
//    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
  //   FEvents: ISearchEventsX;
   // FProc: TOnSearchFind;

    FGeoObjList: TGeoObjList;


    procedure OpenDB(aWhere: string);
    procedure Run;
    procedure Save;
    procedure UpdateStatusBar;

  public
    class procedure ExecDlg(aSelectedIDList: TIDList);

  end;


implementation

{$R *.dfm}

//------------------------------------------------------------------------------
class procedure Tdlg_Property_GeoCoding.ExecDlg(aSelectedIDList: TIDList);
//------------------------------------------------------------------------------
var
  s: string;
  sSQL: string;
begin
  with Tdlg_Property_GeoCoding.Create(Application) do
  begin
    s:=aSelectedIDList.MakeWhereString();

    OpenDB (s);


 //   FProc:=aProc;

  //  if aEvents<>nil then
   //   FEvents:=aEvents;

    
    UpdateStatusBar;

    ShowModal;

    Free;
  end;

end;

procedure Tdlg_Property_GeoCoding.act_SaveExecute(Sender: TObject);
begin
  if Sender=act_Save then
    Save else

  if Sender=act_Run then
    Run

//
end;

procedure Tdlg_Property_GeoCoding.FormDestroy(Sender: TObject);
begin
  FreeAndNil (FGeoObjList);
end;






// ---------------------------------------------------------------
procedure Tdlg_Property_GeoCoding.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin

  Caption:= '��������������';
  DBGrid1.Align:= alClient;

  FGeoObjList:=TGeoObjList.Create;

  //---------------------------------------------
//  ..����������
{
  db_CreateField(mem_Data, [db_Field(FLD_ID,     ftInteger),
                            db_Field(FLD_NAME,   ftString),
                            db_Field(FLD_ADDRESS,  ftString),
                         //   db_Field(FLD_POSITION, ftString),
                            db_Field(FLD_LAT,      ftFloat),
                            db_Field(FLD_LON,      ftFloat),

                            db_Field(FLD_LAT_STR,  ftString),
                            db_Field(FLD_LON_STR,  ftString)

                            ]);
}
end;

// ---------------------------------------------------------------
procedure Tdlg_Property_GeoCoding.UpdateStatusBar;
// ---------------------------------------------------------------
begin
  StatusBar1.SimpleText:=  Format('%d �������',[qry_Props.RecordCount]);
end;


// ---------------------------------------------------------------
procedure Tdlg_Property_GeoCoding.OpenDB(aWhere: string);
// ---------------------------------------------------------------
const
  SQL_SELECT_PROPERTY =
    'SELECT id, address, name, lat, lon, lat_wgs, lon_wgs'+
    ' FROM PROPERTY  WHERE (project_id=:project_id)  and  ';
//    ' ORDER BY name';
begin

//  dmOnega_DB_data.OpenQuery_(qry_Props,
//     SQL_SELECT_PROPERTY + ' (ID in (' + aWhere + '))',
//     [
//      FLD_PROJECT_ID, dmMain.ProjectID
//     ]);
//
   

  UpdateStatusBar;

//  StatusBar1.SimpleText:=  Format('%d �������',[mem_Data.RecordCount]);
end;


//------------------------------------------------------------------------------
procedure Tdlg_Property_GeoCoding.Run;
//------------------------------------------------------------------------------
var
  I: Integer;
//  i: integer;

  bl: TBLPoint;
  iID: Integer;
  sAddress: string;
begin

 // db_Clear(mem_Data);
  FGeoObjList.Clear;


  qry_Props.DisableControls;
  qry_Props.First;

  with qry_Props do
    while not EOF  do
    begin
      iID:=FieldByName(FLD_ID).AsInteger;
      bl.B:=FieldByName(FLD_LAT_WGS).AsFloat;
      bl.L:=FieldByName(FLD_LON_WGS).AsFloat;


      FGeoObjList.Add_(iID, '', bl.B, bl.L);

      //sAddress:= webgeo_GetAddressByLatLon (bl.B, bl.L);

      Next;
    end;

  FGeoObjList.Refresh_Address_by_LatLon(False);

  for I := 0 to FGeoObjList.Count - 1 do    // Iterate
    if FGeoObjList[i].Address<>'' then
    begin
      if qry_Props.Locate(FLD_ID, FGeoObjList[i].ID, []) then
         db_UpdateRecord__(qry_Props,
        [
           FLD_Address, FGeoObjList[i].Address
        ]);
    end;


  qry_Props.First;
  qry_Props.EnableControls;

end;


//------------------------------------------------------------------------------
procedure Tdlg_Property_GeoCoding.Save;
//------------------------------------------------------------------------------
var
  iID: Integer;
  sAddress: string;
begin

 // db_Clear(mem_Data);


  qry_Props.DisableControls;
  qry_Props.First;

  with qry_Props do
    while not EOF  do
    begin
      iID:=FieldByName(FLD_ID).AsInteger;
      sAddress:=FieldByName(FLD_ADDRESS).AsString;


//      dmProperty.UpdateAddress(iID, sAddress);

      //sAddress:= webgeo_GetAddressByLatLon (bl.B, bl.L);

      Next;
    end;


  qry_Props.First;
  qry_Props.EnableControls;

  Close;
end;






end.

