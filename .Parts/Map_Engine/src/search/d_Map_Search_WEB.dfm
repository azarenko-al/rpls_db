object dlg_Map_Search_WEB: Tdlg_Map_Search_WEB
  Left = 1095
  Top = 416
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'dlg_Map_Search_WEB'
  ClientHeight = 388
  ClientWidth = 687
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 687
    Height = 38
    BorderWidth = 2
    ButtonHeight = 21
    Caption = 'ToolBar1'
    TabOrder = 0
    object ed_Search: TEdit
      Left = 0
      Top = 0
      Width = 370
      Height = 21
      TabOrder = 0
      Text = #1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075', '#1043#1088#1072#1078#1076#1072#1085#1089#1082#1080#1081' '#1087#1088#1086#1089#1087#1077#1082#1090', 122'
    end
    object ToolButton2: TToolButton
      Left = 370
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 0
      Style = tbsSeparator
    end
    object ComboBox_Service: TComboBox
      Left = 378
      Top = 0
      Width = 89
      Height = 21
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 2
      Text = 'Yandex'
      Items.Strings = (
        'Yandex'
        'OSM')
    end
    object ToolButton1: TToolButton
      Left = 467
      Top = 0
      Width = 8
      Caption = 'ToolButton1'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object BitBtn1: TBitBtn
      Left = 475
      Top = 0
      Width = 71
      Height = 21
      Action = act_Search
      Caption = #1055#1086#1080#1089#1082
      Default = True
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000000000000000000000000000000000008000FF8000FF
        8000FF8000FF8000FF8000FF8000FF8000FF8000FF8000FF8000FF8000FF8000
        FF8000FF8000FF8000FF8000FF8000FF8000FF8000FF8000FF8000FF8000FF80
        00FF8000FF8000FF8000FF8000FF9A9A9A8080809A9A9A8000FF8000FF8000FF
        8000FF8000FF8000FF8000FF8000FF8000FF8000FF8000FF8000FF9A9A9A0202
        020000000000008000FF8000FF8000FF8000FF8000FF8000FF8000FF8000FF80
        00FF8000FF8000FF9A9A9A0000000000000000004949498000FF8000FF8000FF
        8000FF8000FF8000FF9A9A9A9A9A9A8000FF8000FF9A9A9A0A0A0A1E1E1E0000
        004F4F4F8000FF8000FF8000FF8000FF8000FF9E9E9E4242420F0F0F13131355
        55559898982E2E2E4545450202027A7A7A8000FF8000FF8000FF8000FF8000FF
        767676333333858585C2C2C2B0B0B05555552E2E2E5B5B5B303030ABABAB8000
        FF8000FF8000FF8000FF8000FF9A9A9A3C3C3CBEBEBEFFFFFFFFFFFFFFFFFFFF
        FFFF8080802121219A9A9A8000FF8000FF8000FF8000FF8000FF8000FF6A6A6A
        808080FFFBF7FEEFE5FDEBDDFDE9D9FFF1E6F4E9E02121219E9E9E8000FF8000
        FF8000FF8000FF8000FF8000FF5B5B5BADADADFFFBF6FEEFE5FEEDE1FDEBDDFE
        ECDDFDF3EC5151516C6C6C8000FF8000FF8000FF8000FF8000FF8000FF686868
        969696FFFDFAFEF2E9FEEFE5FEECE1FEEFE4FAF1EA4545458787878000FF8000
        FF8000FF8000FF8000FF8000FF9A9A9A5F5F5FF1F1F1FFFCF9FFF5EEFFF4ECFF
        FCF8AEAEAE2424249A9A9A8000FF8000FF8000FF8000FF8000FF8000FF9A9A9A
        5B5B5B6C6C6CD8D8D8F5F5F5EFEFEFABABAB3333338383838000FF8000FF8000
        FF8000FF8000FF8000FF8000FF8000FF9A9A9A6E6E6E4D4D4D5555554949493C
        3C3C8C8C8C8000FF8000FF8000FF8000FF8000FF8000FF8000FF8000FF8000FF
        8000FF8000FF9A9A9A9A9A9A9A9A9A9A9A9A8000FF8000FF8000FF8000FF8000
        FF8000FF8000FF8000FF8000FF8000FF8000FF8000FF8000FF8000FF8000FF80
        00FF8000FF8000FF8000FF8000FF8000FF8000FF8000FF8000FF}
      TabOrder = 1
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 38
    Width = 687
    Height = 153
    Align = alTop
    DataSource = ds_Data
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'Address'
        Width = 438
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'lat'
        Width = 81
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'lon'
        Width = 77
        Visible = True
      end>
  end
  object pn_Buttons: TPanel
    Left = 0
    Top = 348
    Width = 687
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 40
    Constraints.MinHeight = 40
    TabOrder = 2
    DesignSize = (
      687
      40)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 687
      Height = 2
      Align = alTop
      Shape = bsTopLine
      ExplicitWidth = 634
    end
    object Button3: TButton
      Left = 605
      Top = 10
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ModalResult = 2
      TabOrder = 0
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    UseRegistry = True
    StoredProps.Strings = (
      'ed_Search.Text'
      'ComboBox_Service.ItemIndex')
    StoredValues = <>
    Left = 72
    Top = 208
  end
  object ActionList1: TActionList
    Left = 72
    Top = 272
    object act_Search: TAction
      OnExecute = act_SearchExecute
    end
    object act_Locate: TAction
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1085#1072' '#1082#1072#1088#1090#1077
      OnExecute = act_LocateExecute
    end
  end
  object mem_Data: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 197
    Top = 268
    object mem_Dataposition: TStringField
      DisplayLabel = #1040#1076#1088#1077#1089
      FieldName = 'Address'
      Size = 100
    end
    object mem_Datalat: TFloatField
      DisplayLabel = #1064#1080#1088#1086#1090#1072
      FieldName = 'lat'
    end
    object mem_Datalon: TFloatField
      DisplayLabel = #1044#1086#1083#1075#1086#1090#1072
      FieldName = 'lon'
    end
  end
  object ds_Data: TDataSource
    DataSet = mem_Data
    Left = 198
    Top = 212
  end
  object XMLDocument1: TXMLDocument
    Active = True
    Left = 336
    Top = 216
    DOMVendorDesc = 'MSXML'
  end
  object PopupMenu1: TPopupMenu
    Left = 48
    Top = 80
    object N1: TMenuItem
      Action = act_Locate
    end
  end
end
