unit x_map_engine;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses      


  u_func,
 
  dxCore,
  CodeSiteLogging,
 
  MapXLib_TLB, 

  Map_Engine_TLB,

  d_Map_Search_WEB,
//  d_Map_Search_OSM,

  d_Map_Search_Property ,
  f_Map_tool_Info,
  
  
  d_Layer_Attributes,
  
  f_Options,

  d_Map_Label_Setup,
        
  
  u_Theme,
  
                      
  SysUtils, Forms, Windows, ActiveX, Classes,  StdVcl, System.Win.ComObj ;

type


  {$IFDEF dll}
  TMapEngineX = class(TTypedComObject, IMapEngineX)
           
  {$ELSE}  
  TMapEngineX = class(TInterfacedObject, IMapEngineX)
  {$ENDIF}  

  
  public
    function Dlg_Feature_Info(const aMap: IUnknown; aLat: Double; aLon: Double): HResult; stdcall;
                        
    function Dlg_Search_Property(const aMap: IUnknown; const aConnectionStr:     WideString; aProject_id: Integer): HResult; stdcall;

    function Dlg_Search_Yandex(const aMap: IUnknown): HResult; stdcall;

    
    function Apply_Theme(const aMap: IUnknown; const aObjName, aStyle_XML: WideString): HResult;  stdcall;


   function Dlg_Property_geocoding(const aConnectionStr, aIDS: WideString): HResult;     stdcall;

    
    function Dlg_Attributes(const aConnectionStr, aObjName: WideString): HResult; stdcall;
    function Dlg_Label_style(var aText: WideString): HResult; stdcall;
    function Dlg_Style(const aConnectionStr, aObjName: WideString; var aXML: WideString): HResult;  stdcall;

  end;

implementation

uses ComServ;


function TMapEngineX.Dlg_Attributes(const aConnectionStr, aObjName: WideString): HResult;
begin
  CodeSite.Send('TMapEngineX.Dlg_Attributes ' );

  Tdlg_Layer_Attributes.ExecDlg (aConnectionStr, aObjName);

  
end;


function TMapEngineX.Dlg_Label_style(var aText: WideString): HResult;
begin
  Result := S_OK;
   
  CodeSite.Send('TMapEngineX.Dlg_Label_style ' );

//  Show

  
  if not Tdlg_Map_Label_Setup.ExecDlg_Obj_new(aText) then 
    Result := S_FALSE

   //; //aTabFileName,        //,  aMap

end;



function TMapEngineX.Dlg_Style(const aConnectionStr, aObjName: WideString; var aXML: WideString): HResult;          
begin
  Result := S_OK;

 // Application.CreateForm(TdmMain_options, dmMain_options);

  if not Tfrm_Main_Options.ExecDlg(aConnectionStr, aObjName, aXML) then 
    Result := S_FALSE;


//  FreeAndNil(dmMain_options);

end;

// ---------------------------------------------------------------
function TMapEngineX.Apply_Theme(const aMap: IUnknown; const aObjName, aStyle_XML: WideString): HResult;
          
var
  oThemeMaker: TThemeMaker;

  vCMapX: CMapX;  
begin
  vCMapX:=aMap as CMapX;


  oThemeMaker:=TThemeMaker.Create;
  oThemeMaker.Apply_Theme_for_object(vCMapX, aObjName, aStyle_XML);

  FreeAndNil(oThemeMaker);

end;

function TMapEngineX.Dlg_Search_Property(const aMap: IUnknown; const
    aConnectionStr: WideString; aProject_id: Integer): HResult;
begin

  Tdlg_Map_Search_Property.ExecDlg(aMap as CMapX, aConnectionStr, aProject_id);
  
end;


function TMapEngineX.Dlg_Search_Yandex(const aMap: IUnknown): HResult;
begin
  Tdlg_Map_Search_WEB.ExecDlg(aMap as CMapX);
//  Tdlg_Map_Search_OSM.ExecDlg(aMap as CMapX);

end;



function TMapEngineX.Dlg_Feature_Info(const aMap: IUnknown; aLat, aLon:    Double): HResult;          
begin
  Tfrm_Map_tool_Info.ShowWindow(aMap as CMapX, aLat, aLon);

end;


function TMapEngineX.Dlg_Property_geocoding(const aConnectionStr, aIDS: WideString): HResult;
          
begin

end;

initialization
  {$IFDEF dll}
  dxInitialize;
 
  TTypedComObjectFactory.Create(ComServer, TMapEngineX, Class_MapEngineX, ciMultiInstance, tmApartment);

  {$ENDIF}
finalization
  {$IFDEF dll}

  //!!!!!!!!!!!!!!
  CloseApplicationForms;
    
  dxFinalize;
  {$ENDIF}    
end.


