unit i_SNMP;

interface

uses
  Windows, shellapi,  Forms, Sysutils, Dialogs,  //ActiveX;

   u_com,
   snmp_lib_TLB;



type

  TSnmpClient = class
    constructor Create;
    destructor Destroy; override;
  private
    dll: THandle;
  public
    Intf: ISnmpX ;
  end;

  
function Init_ISnmp: ISnmpX;



implementation



// ---------------------------------------------------------------
function Init_ISnmp: ISnmpX;
// ---------------------------------------------------------------
const
  DEF_FILE = 'Snmp.dll';

var
  dll: Integer;
  sFile: string;

begin

  sFile:= ExtractFilePath(Application.ExeName) + 'DLL\' + DEF_FILE;

//  if not FileExists(sFile) then
//    sFile:= ExtractFilePath(Application.ExeName) + 'WMS\' + DEF_FILE;

  Assert(FileExists(sFile), sFile);

  dll:=GetComObject(sFile, CLASS_Snmp, ISnmpX, Result);

  Assert (dll>0, 'Init_ISnmp dll=0');

end;


//------------------------------------------------------------
constructor TSnmpClient.Create;
const
  DEF_FILE = 'Snmp.dll';

var
  dll: Integer;
  sFile: string;

begin
  sFile:= ExtractFilePath(Application.ExeName) + 'DLL\' + DEF_FILE;
  Assert(FileExists(sFile), sFile);

  dll:=GetComObject(sFile, CLASS_Snmp, ISnmpX, Intf);

end;

destructor TSnmpClient.Destroy;
begin
  Intf:=nil; 
  
  if dll >= 32 then
    FreeLibrary(dll)
end;



end.


