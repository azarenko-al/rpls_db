unit Unit1;

interface

uses
  u_db,


  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Data.DB,
  Data.Win.ADODB, Vcl.Grids, Vcl.DBGrids, RxPlacemnt, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxCustomData, cxStyles, cxTL,
  cxMaskEdit, cxCheckBox, cxInplaceContainer, cxDBTL, cxTLData, cxClasses;

type
  TForm1 = class(TForm)
    DBGrid1: TDBGrid;
    ADOConnection1: TADOConnection;
    ds_Table: TDataSource;
    AdoTable: TADOTable;
    Button2: TButton;
    Bevel1: TBevel;
    FormPlacement1: TFormPlacement;
    cxLookAndFeelController1: TcxLookAndFeelController;
    cxDBTreeList1: TcxDBTreeList;
    cxDBTreeList1table_schema: TcxDBTreeListColumn;
    cxDBTreeList1table_name: TcxDBTreeListColumn;
    cxDBTreeList1enabled: TcxDBTreeListColumn;
    cxDBTreeList1parent_column_name: TcxDBTreeListColumn;
    cxDBTreeList1parent_table_name: TcxDBTreeListColumn;
    cxDBTreeList1table_table_caption: TcxDBTreeListColumn;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class procedure ExecDlg(aConnectionStr: WideString; aTable_Name: string; aID:
        Integer);
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

//-----------------------------------------------------------------
class procedure TForm1.ExecDlg(aConnectionStr: WideString; aTable_Name: string;
    aID: Integer);
//-----------------------------------------------------------------
var
  k: Integer;
begin
  with TForm1.Create(Application) do
  begin   

     ADOConnection1.Close;
     ADOConnection1.ConnectionString:= aConnectionStr;
     ADOConnection1.Open;

    
     k:=db_StoredProc_Open (ADOStoredProc1, 'history.sp_History_log_SEL', 
      [ 'table_name', aTable_Name,
        'id', aID
      ]);
     

  
     ShowModal;

    Free;
  end;
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  Caption:='Настройка хранения истории изменений';

  cxDBTreeList1.Align:=alClient;
end;


end.
