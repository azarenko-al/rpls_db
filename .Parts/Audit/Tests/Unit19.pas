unit Unit19;

interface

uses
  Audit_TLB,

  d_Audit,
  
  d_Audit_setup,
  

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls, cxTL,
  cxMaskEdit, cxTLdxBarBuiltInMenu, cxInplaceContainer, cxDBTL, cxTLData,
  cxCalendar, cxMemo, cxTextEdit, Vcl.ExtCtrls;

type
  TForm19 = class(TForm)
    ADOConnection1: TADOConnection;
    ds_Objects: TDataSource;
    DBGrid1: TDBGrid;
    t_AntennaType: TADOTable;
    DataSource1: TDataSource;
    ADOStoredProc1: TADOStoredProc;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    cxDBTreeList1: TcxDBTreeList;
    cxDBTreeList1Row_Num: TcxDBTreeListColumn;
    cxDBTreeList1parent_id: TcxDBTreeListColumn;
    cxDBTreeList1id: TcxDBTreeListColumn;
    cxDBTreeList1action: TcxDBTreeListColumn;
    cxDBTreeList1table_name: TcxDBTreeListColumn;
    cxDBTreeList1record_id: TcxDBTreeListColumn;
    cxDBTreeList1project_id1111111: TcxDBTreeListColumn;
    cxDBTreeList1property_id111111111: TcxDBTreeListColumn;
    cxDBTreeList1user_created: TcxDBTreeListColumn;
    cxDBTreeList1date_created: TcxDBTreeListColumn;
    cxDBTreeList1record_parent_id11111111111: TcxDBTreeListColumn;
    cxDBTreeList1action_str: TcxDBTreeListColumn;
    cxDBTreeList1column_name: TcxDBTreeListColumn;
    cxDBTreeList1old_value: TcxDBTreeListColumn;
    cxDBTreeList1new_value: TcxDBTreeListColumn;
    DBGrid2: TDBGrid;
    qry_Log: TADOQuery;
    DataSource2: TDataSource;
    DBGrid3: TDBGrid;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form19: TForm19;

implementation

{$R *.dfm}

procedure TForm19.Button1Click(Sender: TObject);
var
  id: Integer;
begin
  id:=t_AntennaType['id'];

  Tdlg_Audit.ExecDlg(ADOConnection1.ConnectionString, 'AntennaType', id);

//  CoAudit_.Create.ExecDlg(ADOConnection1.ConnectionString, 'AntennaType', id);
  
end;

procedure TForm19.Button2Click(Sender: TObject);
begin
 // CoAudit_.Create.ExecDlg(ADOConnection1.ConnectionString, 'AntennaType', 5822);

end;

procedure TForm19.Button3Click(Sender: TObject);
var
  id: Integer;
begin
  id:=t_AntennaType['id'];

  Tdlg_Audit.ExecDlg(ADOConnection1.ConnectionString, 'AntennaType', id);
end;

procedure TForm19.Button4Click(Sender: TObject);
begin
  //10712
//  Tdlg_Audit.ExecDlg(ADOConnection1.ConnectionString, 'link', 10712);
  
end;

procedure TForm19.Button5Click(Sender: TObject);
begin

  Tdlg_Audit_Setup.ExecDlg(ADOConnection1.ConnectionString);    


end;

end.
