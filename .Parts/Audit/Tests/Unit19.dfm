object Form19: TForm19
  Left = 1183
  Top = 199
  Caption = 'Form19'
  ClientHeight = 778
  ClientWidth = 1098
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 286
    Width = 1098
    Height = 139
    Align = alBottom
    DataSource = ds_Objects
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Button1: TButton
    Left = 584
    Top = 24
    Width = 139
    Height = 25
    Caption = 'Tdlg_Audit.ExecDlg'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 816
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Audit com'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 816
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Audit'
    TabOrder = 3
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 816
    Top = 128
    Width = 75
    Height = 25
    Caption = 'link'
    TabOrder = 4
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 24
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Setup'
    TabOrder = 5
    OnClick = Button5Click
  end
  object cxDBTreeList1: TcxDBTreeList
    Left = 0
    Top = 539
    Width = 1098
    Height = 119
    Align = alBottom
    Bands = <
      item
      end>
    DataController.ParentField = 'parent_id'
    DataController.KeyField = 'id'
    Navigator.Buttons.CustomButtons = <>
    RootValue = -1
    TabOrder = 6
    ExplicitTop = 777
    ExplicitWidth = 2560
    object cxDBTreeList1Row_Num: TcxDBTreeListColumn
      DataBinding.FieldName = 'Row_Num'
      Width = 65
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1parent_id: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'parent_id'
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1id: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'id'
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1action: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'action'
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1table_name: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'table_name'
      Width = 100
      Position.ColIndex = 5
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1record_id: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'record_id'
      Position.ColIndex = 6
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1project_id1111111: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'project_id1111111'
      Position.ColIndex = 7
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1property_id111111111: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'property_id111111111'
      Position.ColIndex = 8
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1user_created: TcxDBTreeListColumn
      DataBinding.FieldName = 'user_created'
      Width = 100
      Position.ColIndex = 9
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1date_created: TcxDBTreeListColumn
      DataBinding.FieldName = 'date_created'
      Width = 100
      Position.ColIndex = 10
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1record_parent_id11111111111: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'record_parent_id11111111111'
      Position.ColIndex = 11
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1action_str: TcxDBTreeListColumn
      DataBinding.FieldName = 'action_str'
      Width = 79
      Position.ColIndex = 4
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1column_name: TcxDBTreeListColumn
      DataBinding.FieldName = 'column_name'
      Width = 100
      Position.ColIndex = 12
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1old_value: TcxDBTreeListColumn
      DataBinding.FieldName = 'old_value'
      Width = 100
      Position.ColIndex = 13
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1new_value: TcxDBTreeListColumn
      DataBinding.FieldName = 'new_value'
      Width = 100
      Position.ColIndex = 14
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object DBGrid2: TDBGrid
    Left = 0
    Top = 658
    Width = 1098
    Height = 120
    Align = alBottom
    DataSource = DataSource1
    TabOrder = 7
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object DBGrid3: TDBGrid
    Left = 0
    Top = 425
    Width = 1098
    Height = 114
    Align = alBottom
    DataSource = DataSource2
    TabOrder = 8
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=SERVER1;Use Procedu' +
      're for Prepare=1;Auto Translate=True;Packet Size=4096;Workstatio' +
      'n ID=ALEX;Use Encryption for Data=False;Tag with column collatio' +
      'n when possible=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 192
    Top = 24
  end
  object ds_Objects: TDataSource
    DataSet = t_AntennaType
    Left = 329
    Top = 72
  end
  object t_AntennaType: TADOTable
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    IndexName = 'IX_AntennaType'
    TableName = 'AntennaType'
    Left = 328
    Top = 24
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc1
    Left = 505
    Top = 80
  end
  object ADOStoredProc1: TADOStoredProc
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'history.sp_Record_History_SEL'
    Parameters = <>
    Left = 504
    Top = 24
  end
  object qry_Log: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'record_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1111
      end>
    SQL.Strings = (
      'select * '
      ''
      'from history.History_log '
      ''
      ' '
      'where record_id = :record_id')
    Left = 64
    Top = 80
  end
  object DataSource2: TDataSource
    DataSet = qry_Log
    Left = 65
    Top = 136
  end
end
