object dlg_Audit: Tdlg_Audit
  Left = 355
  Top = 561
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'dlg_Audit'
  ClientHeight = 350
  ClientWidth = 839
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    839
    350)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 0
    Top = 311
    Width = 839
    Height = 39
    Align = alBottom
    Shape = bsTopLine
    ExplicitTop = 414
    ExplicitWidth = 914
  end
  object Button2: TButton
    Left = 760
    Top = 318
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = #1047#1072#1082#1088#1099#1090#1100
    ModalResult = 8
    TabOrder = 0
  end
  object b_Setup: TButton
    Left = 8
    Top = 318
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
    TabOrder = 1
    OnClick = b_SetupClick
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 292
    Width = 839
    Height = 19
    Panels = <
      item
        Width = 150
      end
      item
        Width = 50
      end>
  end
  object cxDBTreeList1: TcxDBTreeList
    Left = 0
    Top = 0
    Width = 839
    Height = 110
    Align = alTop
    Bands = <
      item
      end>
    DataController.DataSource = ds_Objects
    DataController.ParentField = 'parent_id'
    DataController.KeyField = 'id'
    Navigator.Buttons.CustomButtons = <>
    OptionsData.Editing = False
    OptionsData.Deleting = False
    RootValue = -1
    TabOrder = 3
    object cxDBTreeList1Row_Num: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      DataBinding.FieldName = 'Row_Num'
      Options.Editing = False
      Width = 65
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1parent_id: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'parent_id'
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1id: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'id'
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1action: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'action'
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1table_name: TcxDBTreeListColumn
      DataBinding.FieldName = 'table_name'
      Width = 100
      Position.ColIndex = 13
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1record_id: TcxDBTreeListColumn
      DataBinding.FieldName = 'record_id'
      Position.ColIndex = 14
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1project_id1111111: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'project_id1111111'
      Position.ColIndex = 5
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1property_id111111111: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'property_id111111111'
      Position.ColIndex = 6
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1user_created: TcxDBTreeListColumn
      DataBinding.FieldName = 'user_created'
      Options.Editing = False
      Width = 83
      Position.ColIndex = 7
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1date_created: TcxDBTreeListColumn
      DataBinding.FieldName = 'date_created'
      Options.Editing = False
      Width = 117
      Position.ColIndex = 8
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1record_parent_id11111111111: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'record_parent_id11111111111'
      Position.ColIndex = 9
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1action_str: TcxDBTreeListColumn
      DataBinding.FieldName = 'action_str'
      Options.Editing = False
      Width = 79
      Position.ColIndex = 4
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1column_name: TcxDBTreeListColumn
      DataBinding.FieldName = 'column_name'
      Options.Editing = False
      Width = 100
      Position.ColIndex = 10
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1old_value: TcxDBTreeListColumn
      DataBinding.FieldName = 'old_value'
      Options.Editing = False
      Width = 182
      Position.ColIndex = 11
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1new_value: TcxDBTreeListColumn
      DataBinding.FieldName = 'new_value'
      Options.Editing = False
      Width = 193
      Position.ColIndex = 12
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=SERVER1;Use Procedu' +
      're for Prepare=1;Auto Translate=True;Packet Size=4096;Workstatio' +
      'n ID=ALEX;Use Encryption for Data=False;Tag with column collatio' +
      'n when possible=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 48
    Top = 136
  end
  object ds_Objects: TDataSource
    DataSet = ADOStoredProc1
    Left = 185
    Top = 192
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'history.sp_Record_History_SEL'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@table_name'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = 'LinkEndType'
      end
      item
        Name = '@id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 9530
      end>
    Left = 184
    Top = 136
  end
  object cxLookAndFeelController1: TcxLookAndFeelController
    Kind = lfFlat
    Left = 321
    Top = 136
  end
  object FormPlacement1: TFormPlacement
    UseRegistry = True
    Left = 320
    Top = 200
  end
  object qry_Log: TADOQuery
    Connection = ADOConnection1
    Parameters = <>
    Left = 456
    Top = 136
  end
  object DataSource1: TDataSource
    DataSet = qry_Log
    Left = 457
    Top = 192
  end
  object FormStorage1: TFormStorage
    Options = [fpPosition]
    UseRegistry = True
    StoredValues = <>
    Left = 624
    Top = 136
  end
end
