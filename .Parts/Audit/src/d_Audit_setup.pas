unit d_Audit_setup;

interface

uses
  u_db,
  u_cx,
  

  System.Classes, 
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Data.DB,
  Data.Win.ADODB, RxPlacemnt, 
  cxLookAndFeels, 
  cxDBTL, 
  Vcl.ActnList, cxGraphics, cxControls, cxLookAndFeelPainters, cxCustomData,
  cxStyles, cxTL, cxMaskEdit, cxTLdxBarBuiltInMenu, System.Actions, cxClasses,
  cxInplaceContainer, cxTLData;

type
  Tdlg_Audit_setup = class(TForm)
    ADOConnection1: TADOConnection;
    ds_Table: TDataSource;
    t_Table: TADOTable;
    Button2: TButton;
    Bevel1: TBevel;
    FormPlacement1: TFormPlacement;
    cxLookAndFeelController1: TcxLookAndFeelController;
    cxDBTreeList1: TcxDBTreeList;
    cxDBTreeList1table_name: TcxDBTreeListColumn;
    cxDBTreeList1enabled: TcxDBTreeListColumn;
    cxDBTreeList1parent_column_name: TcxDBTreeListColumn;
    cxDBTreeList1parent_table_name: TcxDBTreeListColumn;
    cxDBTreeList1table_table_caption: TcxDBTreeListColumn;
    ActionList1: TActionList;
    b_Save: TButton;
    ADOStoredProc1_create: TADOStoredProc;
    b_Drop_all: TButton;
    procedure b_Drop_allClick(Sender: TObject);
    procedure b_SaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class procedure ExecDlg(aConnectionStr: WideString);
   
  end;

  
//var
//  dlg_Audit_options: Tdlg_Audit_options;
//
//
implementation

{$R *.dfm}


procedure Tdlg_Audit_setup.b_Drop_allClick(Sender: TObject);
begin
  db_StoredProc_Exec (ADOStoredProc1_create, 'history.sp_Drop_triggers_all',  [ ]);
end;


procedure Tdlg_Audit_setup.b_SaveClick(Sender: TObject);
var
  k: Integer;
begin
  k:=db_StoredProc_Exec (ADOStoredProc1_create, 'history.sp_Create_triggers_all',  [ ]);
              
end;

//-----------------------------------------------------------------
class procedure Tdlg_Audit_setup.ExecDlg(aConnectionStr: WideString);
//-----------------------------------------------------------------
var
  k: Integer;
begin
  with Tdlg_Audit_setup.Create(Application) do
  begin   

     db_OpenADOConnectionString(ADOConnection1, aConnectionStr);
     

     t_Table.Open;

//     ADOConnection1.Close;
//     ADOConnection1.ConnectionString:= aConnectionStr;
//     ADOConnection1.Open;


    
//     k:=db_StoredProc_Open (ADOStoredProc1, 'history.sp_History_log_SEL', 
//      [ 'table_name', aTable_Name,
//        'id', aID
//      ]);
//

  
     ShowModal;

    Free;
  end;
end;



// ---------------------------------------------------------------
procedure Tdlg_Audit_setup.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
   if ADOConnection1.Connected then
     ShowMessage('procedure Tdlg_Audit.FormCreate(Sender: TObject);; ');
   


  Caption:='������� ���������: ���������';

  cxDBTreeList1.Align:=alClient;

  
  cx_SetColumnCaptions1(cxDBTreeList1,
  [
    'table_name',          '�������', 
    'table_table_caption', '��������', 
    'parent_table_name',   '������� �������', 
    'parent_column_name',  '������������ ����', 
    'enabled',             '���' 
  ]);
  
end;


end.
