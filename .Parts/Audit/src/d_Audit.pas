unit d_Audit;

interface

uses
  u_db,
  u_cx,

  d_Audit_setup,
  
  System.SysUtils, System.Variants, System.Classes, 
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxLookAndFeels,
  
  Data.DB, 
  Data.Win.ADODB, 
  RxPlacemnt, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls,
  cxTL, cxDBTL, cxGraphics, cxControls, cxLookAndFeelPainters, cxCustomData,
  cxStyles, cxTextEdit, cxMaskEdit, cxTLdxBarBuiltInMenu, cxClasses,
  cxInplaceContainer, cxTLData;

type
  Tdlg_Audit = class(TForm)
    ADOConnection1: TADOConnection;
    ds_Objects: TDataSource;
    ADOStoredProc1: TADOStoredProc;
    cxLookAndFeelController1: TcxLookAndFeelController;
    FormPlacement1: TFormPlacement;
    Bevel1: TBevel;
    Button2: TButton;
    b_Setup: TButton;
    StatusBar1: TStatusBar;
    cxDBTreeList1: TcxDBTreeList;
    cxDBTreeList1Row_Num: TcxDBTreeListColumn;
    cxDBTreeList1parent_id: TcxDBTreeListColumn;
    cxDBTreeList1id: TcxDBTreeListColumn;
    cxDBTreeList1action: TcxDBTreeListColumn;
    cxDBTreeList1table_name: TcxDBTreeListColumn;
    cxDBTreeList1record_id: TcxDBTreeListColumn;
    cxDBTreeList1project_id1111111: TcxDBTreeListColumn;
    cxDBTreeList1property_id111111111: TcxDBTreeListColumn;
    cxDBTreeList1user_created: TcxDBTreeListColumn;
    cxDBTreeList1date_created: TcxDBTreeListColumn;
    cxDBTreeList1record_parent_id11111111111: TcxDBTreeListColumn;
    cxDBTreeList1action_str: TcxDBTreeListColumn;
    cxDBTreeList1column_name: TcxDBTreeListColumn;
    cxDBTreeList1old_value: TcxDBTreeListColumn;
    cxDBTreeList1new_value: TcxDBTreeListColumn;
    qry_Log: TADOQuery;
    DataSource1: TDataSource;
    FormStorage1: TFormStorage;
    procedure b_SetupClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    

  private
    FConnectionStr: WideString;
                  
  public
    class procedure ExecDlg(aConnectionStr: WideString; aTable_Name: string; aID:
        Integer);

  end;

//var
//  dlg_Audit: Tdlg_Audit;

  
implementation

{$R *.dfm}

procedure Tdlg_Audit.b_SetupClick(Sender: TObject);
begin
  Tdlg_Audit_setup.ExecDlg(FConnectionStr);
end;

// ---------------------------------------------------------------
procedure Tdlg_Audit.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin      
   if ADOConnection1.Connected then
     ShowMessage('procedure Tdlg_Audit.FormCreate(Sender: TObject);; ');
   

 // DBGrid1.Align:=alClient;

  Caption:='������� ���������';
  cxDBTreeList1.Align:=alClient;

{  cx_SetColumnCaptions1(cxGridDBTableView1,

  [
    'action_str', '��������',
    'table_name', '�������',   
    'date_created', '������',
    'user_created', '������������',
    'name', '�������� ����',
    'old_value', '������ ��������',
    'new_value', '����� ��������'
  
  ] );
 }

  cx_SetColumnCaptions1(cxDBTreeList1,

  [
    'Row_Num', '�',
    
    'action_str', '��������',
    'table_name', '�������',   
    'date_created', '����',
    'user_created', '������������',
    'column_name', '�������� ����',
    'old_value', '������ ��������',
    'new_value', '����� ��������'
  
  ] );  
  
end;


//-----------------------------------------------------------------
class procedure Tdlg_Audit.ExecDlg(aConnectionStr: WideString; aTable_Name: string; aID: Integer);
//-----------------------------------------------------------------
var
  k: Integer;
begin
  with Tdlg_Audit.Create(Application) do
  begin     
    StatusBar1.Panels[0].Text:= aTable_Name;
    StatusBar1.Panels[1].Text:= IntToStr(aID);
  
    FConnectionStr:=aConnectionStr;

    db_OpenADOConnectionString(ADOConnection1, aConnectionStr);

//     ADOConnection1.Close;
//     ADOConnection1.ConnectionString:= aConnectionStr;
//     ADOConnection1.Open;

    
     k:=db_StoredProc_Open (ADOStoredProc1, 'history.sp_Record_History_SEL', 
      [ 'table_name', aTable_Name,
        'id', aID
      ]);

  {   
     db_OpenQuery_ (qry_Log, 'select * from history.History_log where recod_id=:recod_id and table_name like %:table_name', 
      [ 'table_name', aTable_Name,
        'id', aID
      ]);
  }
     
     cxDBTreeList1.FullExpand;

  
     ShowModal;

    Free;
  end;
end;

end.
 
