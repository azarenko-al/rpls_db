library audit;

uses
  ComServ,
  Audit_TLB in 'Audit_TLB.pas',
  x_audit in 'x_audit.pas' {Audit: CoClass},
  d_Audit in 'src\d_Audit.pas' {dlg_Audit},
  d_Audit_setup in 'src\d_Audit_setup.pas' {dlg_Audit_setup};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer,
  DllInstall;

{$R *.TLB}

{$R *.RES}

begin
end.
