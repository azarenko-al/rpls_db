unit Audit_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 26.04.2019 19:26:22 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\RPLS_DB\.Parts\Audit\Project20 (1)
// LIBID: {D28E507B-31FA-483B-A6CF-10D8C9FE1EE4}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (stdvcl40.dll)
// SYS_KIND: SYS_WIN32
// Errors:
//   Hint: TypeInfo 'Audit' changed to 'Audit_'
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface


  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  AuditMajorVersion = 1;
  AuditMinorVersion = 0;

  LIBID_Audit: TGUID = '{D28E507B-31FA-483B-A6CF-10D8C9FE1EE4}';

  IID_IAuditX: TGUID = '{72417BD8-6015-4D3D-8C78-A71EB7653AA6}';
  CLASS_Audit_: TGUID = '{D9D1ECC5-E2D3-4D46-BEF5-31D96AC2AD6C}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IAuditX = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  Audit_ = IAuditX;


// *********************************************************************//
// Interface: IAuditX
// Flags:     (256) OleAutomation
// GUID:      {72417BD8-6015-4D3D-8C78-A71EB7653AA6}
// *********************************************************************//
  IAuditX = interface(IUnknown)
    ['{72417BD8-6015-4D3D-8C78-A71EB7653AA6}']
    function ExecDlg(const aConnectionStr: WideString; const aTable_Name: WideString; aID: Integer): HResult; stdcall;
    function Dlg_Setup(const aConnectionStr: WideString): HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoAudit_ provides a Create and CreateRemote method to          
// create instances of the default interface IAuditX exposed by              
// the CoClass Audit_. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoAudit_ = class
    class function Create: IAuditX;
    class function CreateRemote(const MachineName: string): IAuditX;
  end;

implementation

uses System.Win.ComObj;

class function CoAudit_.Create: IAuditX;
begin
  Result := CreateComObject(CLASS_Audit_) as IAuditX;
end;

class function CoAudit_.CreateRemote(const MachineName: string): IAuditX;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Audit_) as IAuditX;
end;

end.

