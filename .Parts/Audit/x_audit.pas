unit x_audit;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses

  d_Audit,
  d_Audit_Setup,
  
  dxCore,
  


  Windows, ActiveX, Classes, ComObj, Audit_TLB, StdVcl;

type
  TAudit = class(TTypedComObject, IAuditX)
  protected
    function ExecDlg(const aConnectionStr, aTable_Name: WideString; aID: Integer):
        HResult; stdcall;

    function Dlg_Setup(const aConnectionStr: WideString): HResult; stdcall;
        
  end;

  
implementation

uses ComServ;


function TAudit.Dlg_Setup(const aConnectionStr: WideString): HResult;
begin
  Tdlg_Audit_setup.ExecDlg(aConnectionStr);    
end;


function TAudit.ExecDlg(const aConnectionStr, aTable_Name: WideString; aID: Integer): HResult;
begin
  Tdlg_Audit.ExecDlg(aConnectionStr, aTable_Name, aID);
end;



initialization
  {$IFDEF dll}

  dxInitialize;
 
  TTypedComObjectFactory.Create(ComServer, TAudit, CLASS_Audit_,
    ciMultiInstance, tmApartment);

  {$ENDIF}        
finalization
  {$IFDEF dll}

  //CodeSite.Send('dxFinalize');
  
  dxFinalize;
  {$ENDIF}    
end.


