object Form1: TForm1
  Left = 1233
  Top = 252
  Caption = 'Form1'
  ClientHeight = 668
  ClientWidth = 852
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    852
    668)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 0
    Top = 630
    Width = 852
    Height = 38
    Align = alBottom
    Constraints.MaxHeight = 38
    Constraints.MinHeight = 38
    Shape = bsTopLine
    ExplicitTop = 459
    ExplicitWidth = 805
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 464
    Width = 852
    Height = 166
    Align = alBottom
    DataSource = ds_Table
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Button2: TButton
    Left = 766
    Top = 638
    Width = 80
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = #1047#1072#1082#1088#1099#1090#1100
    ModalResult = 8
    TabOrder = 1
  end
  object cxDBTreeList1: TcxDBTreeList
    Left = 0
    Top = 152
    Width = 852
    Height = 312
    Hint = ''
    Align = alBottom
    Bands = <
      item
      end>
    DataController.DataSource = ds_Table
    DataController.ParentField = 'parent_table_name'
    DataController.KeyField = 'table_name'
    Navigator.Buttons.CustomButtons = <>
    RootValue = -1
    TabOrder = 2
    object cxDBTreeList1table_schema: TcxDBTreeListColumn
      DataBinding.FieldName = 'table_schema'
      Width = 100
      Position.ColIndex = 5
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1table_name: TcxDBTreeListColumn
      DataBinding.FieldName = 'table_name'
      Width = 182
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1enabled: TcxDBTreeListColumn
      DataBinding.FieldName = 'enabled'
      Width = 52
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1parent_column_name: TcxDBTreeListColumn
      DataBinding.FieldName = 'parent_column_name'
      Width = 131
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1parent_table_name: TcxDBTreeListColumn
      DataBinding.FieldName = 'parent_table_name'
      Width = 149
      Position.ColIndex = 4
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1table_table_caption: TcxDBTreeListColumn
      DataBinding.FieldName = 'table_table_caption'
      Width = 180
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=SERVER1;Use Procedu' +
      're for Prepare=1;Auto Translate=True;Packet Size=4096;Workstatio' +
      'n ID=ALEX;Use Encryption for Data=False;Tag with column collatio' +
      'n when possible=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 48
    Top = 8
  end
  object ds_Table: TDataSource
    DataSet = AdoTable
    Left = 161
    Top = 72
  end
  object AdoTable: TADOTable
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 'history.tables_for_history'
    Left = 160
    Top = 8
  end
  object FormPlacement1: TFormPlacement
    UseRegistry = True
    Left = 680
    Top = 64
  end
  object cxLookAndFeelController1: TcxLookAndFeelController
    Kind = lfFlat
    Left = 681
    Top = 8
  end
end
