object Form28: TForm28
  Left = 0
  Top = 393
  Caption = 'Form28'
  ClientHeight = 588
  ClientWidth = 1264
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    1264
    588)
  PixelsPerInch = 96
  TextHeight = 13
  object Map1: TMap
    Left = 0
    Top = 0
    Width = 713
    Height = 588
    ParentColor = False
    Align = alLeft
    TabOrder = 0
    ExplicitTop = 8
    ControlData = {
      2CA10700B1490000C63C0000010000000F0000FFFFFEFF0D470065006F004400
      69006300740069006F006E0061007200790000FFFEFF3745006D007000740079
      002000470065006F007300650074004E0061006D00650020007B003900410039
      00410043003200460034002D0038003300370035002D0034003400640031002D
      0042004300450042002D00340037003600410045003900380036004600310039
      0030007D00FFFEFF001E00FFFEFF000600010A000000000000FFFEFF00500001
      0100000001F4010000050000800C0000000014000000000002000E00E8030005
      00000100000000E9E7E2000000000000000001370000000000E9E7E200000000
      0000000352E30B918FCE119DE300AA004BB85101CC00009001DC7C0100054172
      69616C000352E30B918FCE119DE300AA004BB851010200009001A42C02000B4D
      61702053796D626F6C730000000000000001000100FFFFFF000200E9E7E20000
      0000000001000000010000FFFEFF3245006D0070007400790020005400690074
      006C00650020007B00300031004100390035003000340042002D004300450031
      0033002D0034003400310035002D0041003500410030002D0035003100440038
      00430032004600310035003200300034007D0000000000FFFFFF000100000000
      000000000000000000000000000000000000000352E30B918FCE119DE300AA00
      4BB85101CC00009001348C030005417269616C000352E30B918FCE119DE300AA
      004BB85101CC00009001348C030005417269616C000000000000000000000000
      000000000000000000000000000000000000000000000000004EAB4000000000
      00807D400100000100001801000001000000FFFFFFFF1C000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000020000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008076C000000000008056C0000000000080764000000000008056400000
      0000010000001801000001000000FFFFFFFF1C00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000200
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000080
      76C000000000008056C000000000008076400000000000805640000000000100
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000}
  end
  object FilenameEdit1: TFilenameEdit
    Left = 707
    Top = 48
    Width = 305
    Height = 21
    Anchors = [akTop, akRight]
    NumGlyphs = 1
    TabOrder = 1
    Text = 'C:\ONEGA\RPLS_DB\MAPS\russia_obl.TAB'
  end
  object Button1: TButton
    Left = 1035
    Top = 39
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Button1'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 707
    Top = 128
    Width = 233
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Load_CMap'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 707
    Top = 168
    Width = 233
    Height = 25
    Anchors = [akTop, akRight]
    Caption = '  FITileManagerX.Set_Center(60, 210, 7);'
    TabOrder = 4
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 707
    Top = 208
    Width = 233
    Height = 25
    Anchors = [akTop, akRight]
    Caption = '  FITileManagerX.Set_Center(60, 60, 7);'
    TabOrder = 5
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 707
    Top = 256
    Width = 233
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Set_Center(0, 0, 3);'
    TabOrder = 6
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 707
    Top = 304
    Width = 233
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Set_Center(0, -180, 3);'
    TabOrder = 7
    OnClick = Button6Click
  end
  object Button7: TButton
    Left = 967
    Top = 175
    Width = 81
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'blRect'
    TabOrder = 8
    OnClick = Button7Click
  end
  object Button8: TButton
    Left = 1146
    Top = 175
    Width = 97
    Height = 25
    Caption = 'PropertyPage'
    TabOrder = 9
    OnClick = Button8Click
  end
  object Button9: TButton
    Left = 707
    Top = 344
    Width = 233
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'set_Center(59.8243, 30.16, 12);'
    TabOrder = 10
    OnClick = Button9Click
  end
  object Button10: TButton
    Left = 707
    Top = 384
    Width = 233
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'set_Center(59.8243, 30.16, 17);'
    TabOrder = 11
    OnClick = Button10Click
  end
  object Button11: TButton
    Left = 789
    Top = 415
    Width = 231
    Height = 25
    Caption = 'Set_Center(59.8243, 30.16, 18);'
    TabOrder = 12
    OnClick = Button11Click
  end
  object Button12: TButton
    Left = 1048
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Button12'
    TabOrder = 13
    OnClick = Button12Click
  end
  object FilenameEdit_GK: TFilenameEdit
    Left = 707
    Top = 8
    Width = 305
    Height = 21
    Anchors = [akTop, akRight]
    NumGlyphs = 1
    TabOrder = 14
    Text = 'C:\ONEGA\RPLS_DB\MAPS\russia_obl.TAB'
  end
  object Button13: TButton
    Left = 1074
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Button13'
    TabOrder = 15
    OnClick = Button13Click
  end
  object ComboBox1: TComboBox
    Left = 789
    Top = 456
    Width = 164
    Height = 21
    Style = csDropDownList
    TabOrder = 16
    OnClick = ComboBox1Click
  end
  object Button14: TButton
    Left = 959
    Top = 454
    Width = 75
    Height = 25
    Caption = 'Button14'
    TabOrder = 17
    OnClick = Button14Click
  end
  object Button15: TButton
    Left = 1096
    Top = 328
    Width = 75
    Height = 25
    Caption = 'getZ'
    TabOrder = 18
    OnClick = Button15Click
  end
  object Button16: TButton
    Left = 1096
    Top = 392
    Width = 75
    Height = 25
    Caption = 'GoogleMap'
    TabOrder = 19
    OnClick = Button16Click
  end
  object Button17: TButton
    Left = 1096
    Top = 423
    Width = 75
    Height = 25
    Caption = 'YandexMap'
    TabOrder = 20
    OnClick = Button17Click
  end
  object Button18: TButton
    Left = 1184
    Top = 8
    Width = 75
    Height = 25
    Caption = 'ActiveX'
    TabOrder = 21
    OnClick = Button18Click
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredValues = <>
    Left = 1171
    Top = 72
  end
end
