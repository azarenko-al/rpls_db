unit Unit2;

interface

uses
  Dialogs, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.OleCtrls, MapXLib_TLB, RxPlacemnt, Vcl.StdCtrls, Vcl.Mask,  Vcl.Controls, Vcl.Forms, 

//IniFiles,

u_Geo,
  u_MapX,
  d_WMS_log,

  wms_TLB,
  x_Tile_Manager,
  

 
  //RxToolEdit,
    RxToolEdit;

type
  TForm28 = class(TForm)
    Map1: TMap;
    FilenameEdit1: TFilenameEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    FormStorage1: TFormStorage;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    Button9: TButton;
    Button10: TButton;
    Button11: TButton;
    Button12: TButton;
    FilenameEdit_GK: TFilenameEdit;
    Button13: TButton;
    ComboBox1: TComboBox;
    Button14: TButton;
    Button15: TButton;
    Button16: TButton;
    Button17: TButton;
    Button18: TButton;
    procedure Button10Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure Button13Click(Sender: TObject);
    procedure Button14Click(Sender: TObject);
    procedure Button15Click(Sender: TObject);
    procedure Button16Click(Sender: TObject);
    procedure Button17Click(Sender: TObject);
    procedure Button18Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure ComboBox1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
 //   FITileManagerX: ITileManagerX;

    FITileManagerX: TTileManagerX;

    FITileManagerX_ActiveX: ITileManagerX;

  public

  end;

var
  Form28: TForm28;

implementation

{$R *.dfm}


procedure TForm28.FormCreate(Sender: TObject);
var
  iBlock_X,  iBlock_Y: integer;

begin
  FITileManagerX:=TTileManagerX.Create;
  FITileManagerX.Init(Map1.DefaultInterface);

//  FITileManagerX.Set_WMS_layer('GoogleMap');
  FITileManagerX.Set_WMS_layer('YandexMap');


  Tdlg_WMS_log.ShowForm;
  
  
  Exit;
  

  //-------------------------------------
  

  Map1.Layers.Add(FilenameEdit1.FileName, 0);
  Map1.CurrentTool:=1001;

  // -------------------------------------

//  FITileManagerX:=CoTileManagerX.Create;  

//  FITileManagerX.GetBlockByXY(50, 180, 7, iBlock_X,  iBlock_Y);
  
  FITileManagerX.Set_Dir('d:\wms_111');

//  FITileManagerX.Set_Center(0, 0, 3);
  
//  FITileManagerX.Set_Center(60, 179, 4);

//  FITileManagerX.Set_Center(0, 0, 3);
//  FITileManagerX.Set_Center(34, 175, 4);

//  FITileManagerX.Set_Center(34, 0, 4);

//  FITileManagerX.Set_Center(0, -180, 3);
  
   FITileManagerX.Set_Center(59.8243, 30.16, 19);

//  FITileManagerX.Set_Center(0, -45, 4);
  
//  FITileManagerX.Set_Center(0, 0, 1);

//  FITileManagerX.Set_Center(59, 30, 5);


  
  
//  FITileManagerX.Load_Tile(0,0,3);

  
//  FITileManagerX.Load_CMap;
  
end;

procedure TForm28.Button1Click(Sender: TObject);
begin
//  Map1.PropertyPage;

end;

procedure TForm28.Button2Click(Sender: TObject);
begin

FITileManagerX.Load_CMap;

end;

procedure TForm28.Button3Click(Sender: TObject);
begin
  FITileManagerX.Set_Center(60, 210, 7);

end;

procedure TForm28.Button4Click(Sender: TObject);
begin
  FITileManagerX.Set_Center(60, 60, 7);

end;

procedure TForm28.Button5Click(Sender: TObject);
begin
  FITileManagerX.Set_Center(0, 0, 3);

end;

procedure TForm28.Button6Click(Sender: TObject);
begin
  FITileManagerX.Set_Center(0, -90, 3);

end;

procedure TForm28.Button7Click(Sender: TObject);
var
  blRect: TBLRect;
begin
  blRect:=mapx_XRectangleToBLRect(Map1.Bounds);  
  
  
  mapx_SetBounds_CMapX(Map1.DefaultInterface, blRect);
            
end;

procedure TForm28.Button8Click(Sender: TObject);
begin
  Map1.PropertyPage;
end;

procedure TForm28.Button9Click(Sender: TObject);
begin
  FITileManagerX.Set_Center(59.8243, 30.16, 12);

end;


procedure TForm28.Button10Click(Sender: TObject);
begin
  FITileManagerX.Set_Center(59.8243, 30.16, 17);

end;

procedure TForm28.Button11Click(Sender: TObject);
begin
   FITileManagerX.Set_Center(59.8243, 30.16, 18);
end;


procedure TForm28.Button12Click(Sender: TObject);
var
  e: Double;
  I: Integer;
  k: Integer;
begin
  for I := 1 to Map1.Layers.Count do
  begin
    k:=Map1.Layers[i].CoordSys.Datum.Ellipsoid; //3,8       //28,10
    k:=Map1.Layers[i].CoordSys.type_;
    e:=Map1.Layers[i].CoordSys.Datum.PrimeMeridian;
    e:=Map1.Layers[i].CoordSys.Datum.SemiMajorAxis;


    e:=Map1.Layers[i].CoordSys.FalseEasting;

  end;

end;


procedure TForm28.Button13Click(Sender: TObject);
begin
  Map1.Layers.Add(FilenameEdit_GK.FileName, 0);
end;

procedure TForm28.Button14Click(Sender: TObject);
var
  s: Widestring;
begin
  FITileManagerX.Get_Layers_text(s);

  ComboBox1.Items.Text:=s;
  ComboBox1.ItemIndex:=1;

end;

procedure TForm28.Button15Click(Sender: TObject);
begin
  ShowMessage ( IntToStr ( FITileManagerX.Get_Z ));
end;

procedure TForm28.Button16Click(Sender: TObject);
begin
  FITileManagerX.Set_WMS_layer('GoogleMap');
end;

procedure TForm28.Button17Click(Sender: TObject);
begin
  FITileManagerX.Set_WMS_layer('YandexMap');
end;

procedure TForm28.Button18Click(Sender: TObject);
//var
//  iBlock_X,  iBlock_Y: integer;

begin

  FITileManagerX_ActiveX:=CoTileManagerX.Create;
  FITileManagerX.Init(Map1.DefaultInterface);

//  FITileManagerX.Set_WMS_layer('GoogleMap');
  FITileManagerX.Set_WMS_layer('YandexMap');

  FITileManagerX.Load_CMap;

//  Tdlg_WMS_log.ShowForm;



end;

procedure TForm28.ComboBox1Click(Sender: TObject);
begin
//  ComboBox1.ItemIndex
  FITileManagerX.Set_WMS_Layer_Index(ComboBox1.ItemIndex);
  
//  ShowMessage('ComboBox1Click');
end;


procedure TForm28.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FITileManagerX);

end;

end.

{
    function Init(const aMapX: IDispatch): HResult; stdcall;
    function Clear: HResult; stdcall;
    function Dlg_Log: HResult; stdcall;
    function Dlg_Set_center: HResult; stdcall;
    function Set_Z(Value: Integer): HResult; stdcall;
    function Get_Z: Integer; stdcall;
    function Load_CMap: HResult; stdcall;

