program test_WMS;

uses
  Vcl.Forms,
  Unit2 in 'Unit2.pas' {Form28},
  WMS_TLB in '..\WMS_TLB.pas',
  x_Tile_Manager in '..\x_Tile_Manager.pas',
  dm_Tile_Loader in '..\src\dm_Tile_Loader.pas' {dmTile_Loader: TDataModule},
  u_WMS_file in '..\src\u_WMS_file.pas',
  d_WMS_log in '..\src\d_WMS_log.pas' {dlg_WMS_log};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm28, Form28);
  Application.Run;
end.
