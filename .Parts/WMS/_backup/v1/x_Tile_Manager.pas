﻿unit x_Tile_Manager;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, ActiveX, Classes, ComObj, wms_TLB,

  d_WMS_log,
            
                
  MapXLib_TLB,
  
  dm_Tile_Loader,                     

  CodeSiteLogging,    
  
  u_com,
              
  u_MapX_func,    
 
  
  StdVcl, 

  u_WMS_file;
  

type

  {$IFDEF dll}
  TTileManagerX = class(TTypedComObject,  ITileManagerX)
  {$ELSE}
  TTileManagerX = class(TInterfacedObject, ITileManagerX)  
  {$ENDIF}

  private
    FCMapX : CMapX;
  
    FdmTileLoader: TdmTile_Loader;

    FWmsFile: TWmsFile;

    function Set_WMS_Layer_Index(aIndex: integer): Boolean;
  protected
    function GetClassGUID: TGUID; stdcall;
    function Exec(aCmd: Integer): HResult; stdcall;
  
  public
    function Init(const aMapX: IDispatch): HResult; stdcall;
    function Clear: HResult; stdcall;
    function Dlg_Log: HResult; stdcall;
    function Dlg_Set_center: HResult; stdcall;
    function Set_Z(Value: Integer): HResult; stdcall;
    function Get_Z: Integer; stdcall;
    function Load_CMap: HResult; stdcall;
    function Set_Center(aLat, aLon: Double; aZ: Integer): HResult; stdcall;
    function Set_Dir(const Value: WideString): HResult; stdcall;
    function Set_WMS_layer(const aName: WideString): HResult; stdcall;


    procedure Load_Tile(aX,aY,aZ: integer);

        
  end;

implementation

uses ComServ, System.SysUtils, d_WMS_Set_Center;
              

// ---------------------------------------------------------------
function TTileManagerX.Init(const aMapX: IDispatch): HResult;
// ---------------------------------------------------------------

const
  DEF_FILE_wmslist_new = 'wmslist_new.xml';

var
  s: string;
  sFile: string;
begin    
  FdmTileLoader:=TdmTile_Loader.Create(nil); 


  sFile:=GetModuleFileName();

  CodeSite.Send(sFile);

 // ShowMessage(sFile);


   sFile:= ExtractFilePath(sFile) + DEF_FILE_wmslist_new;

// 
//  {$IFDEF dll}
//  {$ELSE}
//  sFile:= 'C:\ONEGA\RPLS_DB\bin\WMS\' + DEF_FILE_wmslist_new;
//  {$ENDIF}
   

   CodeSite.Send(sFile);
 
  //Log (sFile);

  Assert(FileExists(sFile), sFile);

  
 // sFile:= SearchFileInPath('wmslist_new.xml');

//  ShowMessage(sFile);
  
  


//  if not FileExists(sFile) then
  
 
///  ShowMessage(sFile);
                  
    
  FWmsFile:=TWmsFile.Create;
  FWmsFile.LoadFromFile(sFile);


  Set_WMS_layer('YandexSat');
  

// (Caption: 'Google - Êàðòà';    Name: 'GoogleMap'),
//    (Caption: 'Google - Ëàíäøàôò'; Name: 'GoogleLand'),
//    (Caption: 'Google - Ãèáðèä';   Name: 'GoogleHybr'),
//
//    (Caption: 'Yandex - Ñïóòíèê';  Name: 'YandexSat'),
//    (Caption: 'Yandex - Êàðòà';     Name: 'YandexMap') ,
//    (Caption: 'Yandex - Ãèáðèä';    Name: 'YandexText'),
//
//
//    (Caption: '2GIS';   Name: '2GIS'),
//
//    (Caption: 'OpenStreetMap - Êàðòà';   Name: 'OpenStreetMap')
//

                
//  Set_WMS_Layer_Index(4);


  FCMapX:= aMapX as CMapX;
  
//  cs.
  
 // GetModuleName
  
                
end;



function TTileManagerX.Clear: HResult;
begin
   Assert(FCMapX<>nil, 'FCMapX=nil');

   mapx_TilesClear(FCMapX); //aMapX as CMapX);

end;


function TTileManagerX.Get_Z: Integer;
begin
  Result:=FdmTileLoader.Params.Result_Z;

end;


// ---------------------------------------------------------------
function TTileManagerX.Load_CMap: HResult;
begin
  CodeSite.Send('TTileManagerX.Load_CMap ...');

  Assert(FCMapX<>nil, 'FCMapX=nil');

  FdmTileLoader.Load_CMap(FCMapX); //aMapX as CMapX);

end;


// ---------------------------------------------------------------
function TTileManagerX.Set_Center(aLat, aLon: Double; aZ: Integer): HResult;
// ---------------------------------------------------------------
var
  x: double;
  y: double;
begin
  FdmTileLoader.Params.Z:=aZ;

  Assert (Assigned (FCMapX ), 'Assigned (FCMapX )');
  
 

//  x:=FCMapX.CenterX;
//  y:=FCMapX.CenterY;

  
  
  CodeSite.Send( Format('CenterX:%1.6f  CenterY:%1.6f',[FCMapX.CenterX, FCMapX.CenterY]));
  
  FCMapX.AutoRedraw :=False; 

  
  FCMapX.ZoomTo (FCMapX.Zoom, aLon, aLat);   // X: Double; Y: Double);

  FdmTileLoader.Load_CMap(FCMapX);

  
  FdmTileLoader.Params.Z:=0;
  
end;


function TTileManagerX.Set_Dir(const Value: WideString): HResult;
begin
  Assert(Value<>'');
  FdmTileLoader.Params.Dir:=Value;  
  ForceDirectories(Value);
      
end;


// ---------------------------------------------------------------
function TTileManagerX.Set_WMS_layer(const aName: WideString): HResult;
// ---------------------------------------------------------------
var
  I: Integer;
  i1: Integer;
  i2: Integer;
  iIndex: Integer;
  s: string;
  sHost: string;
begin
  Result:=S_OK;

  CodeSite.Send('Set_WMS_Layer - ' + aName);


  Assert(FWmsFile.Layers.Count>0);

  iIndex:=FWmsFile.Layers.IndexOf(aName);

  if iIndex<0 then
    Exit;

//
//  s:=FWmsFile.Layers[iIndex].ConnectedSting.URL;
//
//  i1:=Pos('//',s);
//  i2:=Pos('/',s,i1+3);
//
//  sHost:=Copy(s, i1+2, i2-i1-2 );
//    //http://gis.ncdc.noaa.gov/
//
//  Ping(sHost);   


    
 // FdmTileLoader.IdIcmpClient1.  
//  FdmTileLoader.IdIcmpClient1.Ping();
    
//    for I := 0 to FWmsFile.Layers.Count-1 do
//      CodeSite.Send(FWmsFile.Layers[i].UnicId_name);
  
  

  Assert(iIndex>=0);

  

  Result:=S_OK;
  
  
 
  Set_WMS_Layer_Index (iIndex);

  CodeSite.Send('Set_WMS_Layer - Done');
  
end;


function TTileManagerX.Set_Z(Value: Integer): HResult;
begin
  FdmTileLoader.Params.Z:=Value;

end;



// ---------------------------------------------------------------
function TTileManagerX.Set_WMS_Layer_Index(aIndex: integer): Boolean;
// ---------------------------------------------------------------
var
  i1: Integer;
  i2: Integer;

  oLayer: TWmsLayer_;
  
  s: string;
  sHost: string;
begin
  Assert(FWmsFile.Layers.Count>0);

  if aIndex>=0 then
  begin
    oLayer:=FWmsFile.Layers[aIndex];
    
    Assert(oLayer.ConnectedSting.URL<>'');

    FdmTileLoader.Params.URL:=oLayer.ConnectedSting.URL;
    FdmTileLoader.Params.LayerName:=oLayer.UnicId_name;
//    FdmTileLoader.Params.ImgExt:=oLayer.ImgExt;

    
    Assert(oLayer.UnicId_name<>'');
    
    FdmTileLoader.Params.EPSG:=oLayer.Get_EPSG();



    s:=FWmsFile.Layers[aIndex].ConnectedSting.URL;

    i1:=Pos('//',s);
    i2:=Pos('/',s,i1+3);

    sHost:=Copy(s, i1+2, i2-i1-2 );    //http://gis.ncdc.noaa.gov/

    
//    FdmTileLoader.Params.UseCache:=not Ping_(FdmTileLoader.IdIcmpClient1, sHost ); 
//
//    if FdmTileLoader.Params.UseCache then
//      CodeSite.SendError('FdmTileLoader.Params.UseCache=True');
   
 //   FdmTileLoader.Params.UseCache:=not Ping(  sHost); 
    

  end;

  Result:=aIndex>=0;

  
 // CodeSite.Send('Set_WMS_Layer_Index - Done');
  
//    ShowMessage(aName);
  
end;


function TTileManagerX.GetClassGUID: TGUID;
begin
  Result:=IID_ITileManagerX;
end;


function TTileManagerX.Dlg_Set_center: HResult;
begin
  Tdlg_WMS_set_center.ExecDlg(FCMapX);
end;

function TTileManagerX.Exec(aCmd: Integer): HResult;
begin
  case aCmd of
    DEF_DLG_LOG : Tdlg_WMS_log.ShowForm;
  end;

//  if aCmd=Dlg_Log then
//     Tdlg_WMS_log.ShowForm;

end;


function TTileManagerX.Dlg_Log: HResult;
begin
  Tdlg_WMS_log.ShowForm;
end;


procedure TTileManagerX.Load_Tile(aX,aY,aZ: integer);
begin
  FdmTileLoader.Load_Tile(aX,aY,aZ);

end;


initialization

{$IFDEF dll}
  TTypedComObjectFactory.Create(ComServer, TTileManagerX, Class_TileManagerX,
    ciMultiInstance, tmApartment);

{$ENDIF}    
end.


    (*

      
destructor TTileManagerX.Destroy;
begin
  FreeAndNil(FWmsFile);
  FreeAndNil(FdmTileLoader);  

  inherited;  
end;



function TTileManagerX.Set_Dir(const aValue: WideString): HResult;
begin
  Assert(aValue<>'');
  FdmTileLoader.Params.Dir:=aValue;  
  ForceDirectories(aValue);
      
end;      



 //initialization
//  TTypedComObjectFactory.Create(ComServer, TTileManagerX, Class_TileManagerX,
//    ciMultiInstance, tmApartment);
//end.
//

