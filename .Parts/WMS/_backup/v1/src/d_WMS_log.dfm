object dlg_WMS_log: Tdlg_WMS_log
  Left = 1529
  Top = 560
  Caption = 'WMS debug'
  ClientHeight = 267
  ClientWidth = 551
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 305
    Height = 267
    Align = alLeft
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object FormPlacement1: TFormPlacement
    IniFileName = 'Software\Onega\Forms\WMS_log'
    UseRegistry = True
    Left = 96
    Top = 32
  end
end
