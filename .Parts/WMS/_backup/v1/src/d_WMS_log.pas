unit d_WMS_log;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,

  
   
  
  rxPlacemnt
  ;

type
  Tdlg_WMS_log = class(TForm)
    Memo1: TMemo;
    FormPlacement1: TFormPlacement;
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
  public

    procedure Add(aMsg: string);
    
    class function ShowForm: Tdlg_WMS_log;
    
//    class procedure Add_msg(aMsg: string);
  end;


  var
  dlg_WMS_log: Tdlg_WMS_log;



implementation

{$R *.dfm}



procedure Tdlg_WMS_log.FormDestroy(Sender: TObject);
begin
  dlg_WMS_log:=nil;
end;


procedure Tdlg_WMS_log.FormCreate(Sender: TObject);
begin
  Memo1.Align:=alClient;
end;

procedure Tdlg_WMS_log.Add(aMsg: string);
begin
  Memo1.Lines.Insert(0,aMsg);
end;

//
//class procedure Tdlg_WMS_log.Add_msg(aMsg: string);
//begin
//  if assigned(dlg_WMS_log) then
//    dlg_WMS_log.Add(aMsg);
//
//
//end;


procedure Tdlg_WMS_log.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;

// ---------------------------------------------------------------
class function Tdlg_WMS_log.ShowForm: Tdlg_WMS_log;
// ---------------------------------------------------------------
//var
//  oForm: TForm;
begin
 // CodeSite.Send('Tdlg_WMS_log - 1');

 // result:= Tdlg_WMS_log( FindForm(Tdlg_WMS_log.ClassName) );

  if not assigned(dlg_WMS_log) then
    dlg_WMS_log:= Tdlg_WMS_log.Create(Application);

 // dlg_WMS_log:=result;

//  CodeSite.Send('Tdlg_WMS_log - 2');

  
  dlg_WMS_log.Show;
  
end;


end.
