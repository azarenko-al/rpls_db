unit u_WMS_file;

interface

uses 
  Windows, System.Classes, SysUtils, dialogs, Forms,

  Xml.XMLIntf,
                       
  
 wmslist, 

  Vcl.Menus;


type
  TWmsLayerList = class;
                          

 //-------------------------------------------------------------------
  TWmsFile = class
  //-------------------------------------------------------------------
  private
    TileMatrixes_Identifier_CRS: TstringList;

  public
 //   Runtime: record
    //      XML : string;
    //    end;
  
    Topics: TstringList;
                    
    Layers: TWmsLayerList;

    constructor Create;
    destructor Destroy; override;
              
    procedure LoadFromFile(aFileName: string);
    procedure LoadPopupMenu(aPopupMenu: TPopupMenu; aOnClick: TNotifyEvent);
           
  end;     


 //-------------------------------------------------------------------
  TWmsLayer_ = class
  //-------------------------------------------------------------------
  public    

    Topic_Name: string;
//    Topic_Index: integer; 

    Name_Caption: string;     //Name="�������" UnicId="GoogleSat">
    UnicId_name: string; 

  //  ImgExt: string;  //extension

    
    ConnectedSting: record
        URL            : string;
        Projection_CRS : string;
        Matrix_name    : string;
    end;
    

    function Get_EPSG: Integer;
  end;


  //-------------------------------------------------------------------
  TWmsLayerList = class(TStringList)
  //-------------------------------------------------------------------
  private
    function GetItems(Index: Integer): TWmsLayer_;
  public
    function AddItem(aTopic, aName, aUnicId: string): TWmsLayer_;

    property Items[Index: Integer]: TWmsLayer_ read GetItems; default; 
  end;

  


implementation



constructor TWmsFile.Create;
var
  s: string;
begin
  inherited Create;
  Topics := TstringList.Create();
  Layers := TWmsLayerList.Create();
  TileMatrixes_Identifier_CRS := TstringList.Create();
    
  
end;


destructor TWmsFile.Destroy;
begin
  FreeAndNil(TileMatrixes_Identifier_CRS);
  FreeAndNil(Layers);
  FreeAndNil(Topics);
  inherited Destroy;
end;



  
// ---------------------------------------------------------------
function TWmsLayerList.AddItem(aTopic, aName, aUnicId: string): TWmsLayer_;
// ---------------------------------------------------------------
//var
//  k: Integer;
begin

//Name="�������" aUnicId="

  Result := TWmsLayer_.Create;
  Result.Name_Caption:=aName;
  Result.UnicId_name:=aUnicId;  
  Result.Topic_Name:=aTopic;  
//  Result.ImgExt:=aImgExt;  

  AddObject(aUnicId, Result);
  
  
//  Result.NAME:=aName;
//
//  k:=AddObject(aName, Result);

//  Result := TWmsLayer_ (Add(aName));    

end;



function TWmsLayerList.GetItems(Index: Integer): TWmsLayer_;
begin
  Result := TWmsLayer_(inherited Objects[Index]);
end;



// ---------------------------------------------------------------
procedure TWmsFile.LoadPopupMenu(aPopupMenu: TPopupMenu; aOnClick:
    TNotifyEvent);
// ---------------------------------------------------------------
var
  I: Integer;
  j: Integer;
  oMenuItem_child, menuItem: TMenuItem;
begin
  aPopupMenu.Items.Clear;
  

  Assert(Topics.Count>0);

  for I := 0 to Topics.Count-1 do
  begin
    menuItem := TMenuItem.Create(aPopupMenu) ;      
    menuItem.Caption := Topics[i];
    
    aPopupMenu.Items.Add(menuItem) ;
                            

    for j := 0 to Layers.Count-1 do
      if Layers[j].Topic_Name = Topics[i] then
      begin       

        oMenuItem_child := TMenuItem.Create(aPopupMenu) ;
        oMenuItem_child.Caption := Layers[j].Name_Caption + '-' + Layers[j].UnicId_name;  
        oMenuItem_child.Tag:=j;

        oMenuItem_child.OnClick:=aOnClick;
        
        menuItem.Add(oMenuItem_child)      
      end;

  end;
    

  
end;

// ---------------------------------------------------------------
procedure TWmsFile.LoadFromFile(aFileName: string);
//// ---------------------------------------------------------------
//const
//  DEF_FILE = 'W:\WMS\tests\data\wmslist.xml';

var
  I: Integer;
  iCount: Integer;
  iTopic: Integer;
  k: Integer;
  oLayer: TWmsLayer_;
  s: string;
  s1: string;
  s2: string;
  v: IXMLGeoportalType;

  vTopic: IXMLTopicType;
  
//  vDoc_new: IXMLDocument;
 // vLayer_new,vTopics_new,vTopic_new: IXMLNode;
  
 // s : string;

begin
//  Loadres()
//  s:=LoadStr (WMLIST_xml);

  


 // vDoc_new:=NewXMLDocument;

  
  
//  vTopics:=v.AddChild('aaaaaa');
//  v.SaveToXML(s);
//  


  v:= LoadGeoportal(aFileName);

  Topics.Clear;
  TileMatrixes_Identifier_CRS.Clear;  
  Layers.Clear;  

                  
  TileMatrixes_Identifier_CRS.Clear;

  iCount:=v.Defines.TileMatrixSet.Count;

  for i := 0 to iCount-1 do
  begin
    s1:=v.Defines.TileMatrixSet[i].Identifier;
    s2:=v.Defines.TileMatrixSet[i].CRS;
    
    TileMatrixes_Identifier_CRS.Values[s1]:=s2;
  end;             
  

  iCount:=v.Portal.Topic.Count;

  //vNewDoc.Add

 // vTopics_new:=vDoc_new.AddChild('topics');
  

  for iTopic := 0 to iCount-1 do
  begin
    vTopic:=v.Portal.Topic[iTopic];

 ////   vTopic_new:=vTopics_new.AddChild('topic');
   // vTopic_new.Attributes['name']:= vTopic.Name;
  
  
    s:=vTopic.Name;
        
    if vTopic.Layer.Count>0 then
      Topics.Add(vTopic.Name);
                                   
    for I := 0 to vTopic.Layer.Count-1 do
    begin 
   //   vLayer_new:=vTopic_new.AddChild('layer');
//      vLayer_new.Attributes['name']:= vTopic.Layer[i].Name;
  //    vLayer_new.Attributes['name']:= vTopic.Layer[i].Name;
  //    vLayer_new.Attributes['SID'] := vTopic.Layer[i].UnicId;

    
      oLayer:= Layers.AddItem(
              vTopic.Name, 
              vTopic.Layer[i].Name, 
              vTopic.Layer[i].UnicId
              
//              vTopic.Img

               );
                                
              
       oLayer.ConnectedSting.URL:= vTopic.Layer[i].ConnectedSting.URL;

//               Assert(oLayer.ConnectedSting.URL<>'');
               
      oLayer.ConnectedSting.Matrix_name   :=vTopic.Layer[i].ConnectedSting.Matrix.Name;
      oLayer.ConnectedSting.Projection_CRS:=vTopic.Layer[i].ConnectedSting.Projection.CRS;

      if (oLayer.ConnectedSting.Projection_CRS='') and
         (oLayer.ConnectedSting.Matrix_name<>'')
      then
        oLayer.ConnectedSting.Projection_CRS:=TileMatrixes_Identifier_CRS.Values[oLayer.ConnectedSting.Matrix_name];
                
      
    end;
            
  end;


 // Runtime.XML:=vDoc_new.XML.Text; 

end;


procedure Test;
var
  obj: TWmsFile;
begin
  Application.Initialize;

  obj:=TWmsFile.Create;
  obj.LoadFromFile('W:\WMS\tests\data\wmslist.xml');

 // obj.Exec;
  
end;


function TWmsLayer_.Get_EPSG: Integer;
var
  k: Integer;
begin
  k:=Pos(':', ConnectedSting.Projection_CRS);
  if k>0 then
    Result := StrToIntDef(Copy(ConnectedSting.Projection_CRS, k+1, 100),0);
end;


//
//
//initialization
//  CoInitialize(nil);
//


//begin
  
 // Test
         

end. 


{


Procedure LoadStringTable(TableName:String;SL:TStrings);
var
 RS: TResourceStream;
begin
 RS := TResourceStream.Create( HInstance, TableName, RT_RCDATA);  
 try
  SL.LoadFromStream(RS); 
 finally
  RS.Free;
 end;
end;

// Usage :

var  
 String1, String2, String3 : String;
 StrList:TStringList;
Begin
 StrList:=TStringList.Create;
 try
  LoadStringTable('RCData',StrList);
  String1:= StrList[0];
  String2:= StrList[1];
  String3:= StrList[2];
 finally
  StrList.Free;
 end;
end;
