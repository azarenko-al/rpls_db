library wms;

uses
  ComServ,
  WMS_TLB in 'WMS_TLB.pas',
  x_Tile_Manager in 'x_Tile_Manager.pas' {TileManagerX: CoClass},
  dm_Tile_Loader in 'src\dm_Tile_Loader.pas' {dmTile_Loader: TDataModule},
  u_COM in 'W:\common XE\Package_Common\u_COM.pas',
  u_proxy in 'W:\common XE\u_proxy.pas',
  u_Mapinfo_WOR_classes in 'W:\common XE\Map\u_Mapinfo_WOR_classes.pas',
  u_WMS_file in 'src\u_WMS_file.pas',
  d_WMS_log in 'src\d_WMS_log.pas' {dlg_WMS_log},
  d_WMS_tools in 'src\d_WMS_tools.pas' {dlg_WMS_tools},
  d_WMS_Set_Center in 'src\d_WMS_Set_Center.pas' {dlg_WMS_set_center},
  mapapi in 'C:\Program Files\Panorama\GisTool12Free\common new\mapapi.pas',
  wmslist in 'src\wmslist.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer,
  DllInstall;

{$R *.TLB}

{$R *.RES}

begin
end.
