unit u_Tile_Loader_1111111;


interface

{.$DEFINE use_debug}

{$DEFINE grid}

uses
  Dialogs, JPEG, PNGImage, ExtCtrls, Graphics,  SysUtils, Classes, Forms, Variants,
  StdVcl,  StrUtils, URLMon, ShellApi, System.IniFiles, DateUtils,
  System.IOUtils,

 u_func,  

d_WMS_log,

  u_com,
               
  u_Mapinfo_WOR_classes,

  MapXLib_TLB,
  
  CodeSiteLogging,

  u_mapX_lib,
  
  u_MapX_func,
    
  u_mapX,
  
//  u_mapX_classes,
   

 // u_Mapinfo_WOR_classes,
   
  u_panorama_coord_convert,
  

 // WMS_TLB,

  Math,

  u_geo,

  u_files;
           


type
  TXYBounds = record
    x_min: Double;
    x_max: Double;
    y_min: Double;
    y_max: Double;
    
  end;

  TOffserRec= record
    Lat: Double;
    lon: Double;
  end;

  
  
  TTileBounds = packed record
                   block_X_min : integer;
                   block_X_max : integer;

                   block_Y_min : integer;
                   block_Y_max : integer;
                                                            
                   Z : integer;              
             end;

  
 TLoad_params = record
    Lat: Double;
    Lon: Double;

    Width: Integer;
    Height: Integer;
  end;                          
           

  TTile_Loader__11111111111 = class
    procedure DataModuleCreate(Sender: TObject);
  private
    FBlockX_col_count: word;
    FBlockY_row_count: word;

    FTileBounds: TTileBounds;
      
    FTileArray: array of
                array of record
                  block_x,block_y: Word;
                end; 
    
  
    procedure  CheckBlock(var aBlock: integer; aZ: integer);

 

    function GetFileDir(aZ: integer): string;
    function GetFileName_TAB(aX, aY, aZ: integer; aVersion: Integer = 0): string;
      
    function GetFileName_bounds(): string;

    function GetTileBounds_view(aLat, aLon: Double; aZ, aWidth, aHeight: Integer): TXYBounds;
//        var aTileBounds: TTileBounds

    function GetRangeTileBounds_MinMax(  aTileBounds: TTileBounds): TXYBounds;


//    aX_block_min, aX_block_max, aY_block_min,
  //      aY_block_max, aZ: Integer): TXYBounds;
  
    function GetTileBounds_mercatorXY(aBlockX, aBlockY, aZ: Integer): TXYBounds;
    function GetZ(aBoundsX: TXYBounds; aWidth: Integer): Integer;

    function Load(aParams: TLoad_params; var aFileName: WideString; var aXYBounds:
        TXYBounds): HResult;

        //; var aTileBounds: TTileBounds

    function LoadByRect(aRect: TTileBounds; var aFileName: WideString): Boolean;
    function LoadByRect_new(var aFileName: WideString): Boolean;   //aRect: TTileBounds; 

    procedure Load_Ini;
    
    function Load_HTTP(aFileName: string; aX, aY, aZ: integer): boolean;

    procedure Log(aMsg: string);
    
//    procedure MakeTileMap__1111111(aFileName: string; aZ: Integer; ablRect:
//        TBLRect; aTileBounds: TTileBounds);


    procedure SaveTileFile_WGS_EPSG_3395(aFileName: string; aX, aY, aZ: Integer;
        aImgFileName: string; aEPSG: Integer);
        
    procedure SaveTileFile_WGS_EPSG_3395_bounds(aFileName, aImgFileName: string;
        aEPSG, aZ: Integer; aBounds: TXYBounds; aImage_width, aImage_height:
        Integer);
  public
    procedure Load_Tile(aX,aY,aZ: integer);


    procedure GetBlockByXY(aX, aY: double; aZ: integer; var aBlock_X, aBlock_Y:  integer);

  public

    Params: record
//              UseCache : boolean;

              URL: string;
              Dir: string;

              LayerName: string;
           //   ImgExt: string;

              EPSG: integer;

              Z: integer;                 
             
             //out
              Result_Z: integer;
            end;
                 
    
    function Load_CMap(const aMapX: CMapX): HResult;
    procedure Test;


  end;

  
implementation    
  

//const
//  DEF_DIR_PREFIX = 'ver3';


const
// epsg 3857
  DEF_MAX_20037508 =  20037508.34279000;




function DownloadFile(aSourceFile_URL, aDestFile: string): Boolean;
begin
  try 
    Result := UrlDownloadToFile(nil, PChar(aSourceFile_URL), PChar(aDestFile), 0, nil) = 0; 
  except
    Result := False; 
  end; 
end; 

  
  
  
// ---------------------------------------------------------------
procedure TTile_Loader__11111111111.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin  
//  Params.LayerName:='YandexMap';
  Params.Dir:= GetTempFileDir() ;
               
  Load_Ini;         
end;    


// ---------------------------------------------------------------
procedure TTile_Loader__11111111111.Load_Ini;
// ---------------------------------------------------------------
var
  ini: TIniFile;
  s,sFile: string;
  sModule: string;

begin

  sFile :=System.SysUtils.GetModuleName(HInstance);
  

  s:=ExtractFileExt(sFile);
 
  
  sModule:=GetModuleFileName();

  s:=ExtractFileExt(sModule);
  
  if s='.exe' then
    sFile:= 'C:\ONEGA\RPLS_DB\bin\WMS\wms.ini' 
  else
  
//  if GetModuleFileName()<>'' then
    sFile:= ChangeFileExt( GetModuleFileName(), '.ini');
 // else
  //  sFile:= 'C:\ONEGA\RPLS_DB\bin\WMS\wms.ini'; 

//   sFile:= ChangeFileExt( GetModuleFileName(), '.ini');
   
  Log(sFile);


 // Exit;
   
//  sFile:=''

  if FileExists(sFile) then
  begin
    ini:=TIniFile.Create(sFile);
    s:=ini.ReadString('main','dir','');

    if s<>'' then
       Params.Dir:= IncludeTrailingBackslash(s);
    


    FreeAndNil(ini);
  end;      


//  FreeAndNil(oSList);
  
  //135
  
end;
 

// ---------------------------------------------------------------
function TTile_Loader__11111111111.GetFileDir(aZ: integer): string;
// ---------------------------------------------------------------
begin
  Assert(Params.Dir<>'', 'Params.Dir');
  Assert(Params.LayerName<>'', 'Params.LayerName');  
         
 
  Result:=IncludeTrailingBackslash(Trim(Params.Dir)) +  Format('%s\z%d\',[ Params.LayerName,   aZ]);  
 
end;

// ---------------------------------------------------------------
function TTile_Loader__11111111111.GetFileName_TAB(aX, aY, aZ: integer; aVersion: Integer
    = 0): string;
// ---------------------------------------------------------------
begin
//  Result:=GetFileDir(aZ) + Format('x%d\y%d\',[aX,aY]) + Format('tile_z%d_x%d_y%d.tab',[aZ,aX,aY]);
//  Result:=GetFileDir(aZ) + Format('0\x%d\0\',[aX]) + Format('y%d.tab',[aVersion,aY]);
  Result:=GetFileDir(aZ) + Format('%d\x%d\0\',[aVersion,aX]) + Format('y%d.tab',[aY]);  
  
end;   

// ---------------------------------------------------------------
function TTile_Loader__11111111111.GetFileName_bounds(): string;
// ---------------------------------------------------------------
begin
  with FTileBounds do

  Result:=GetFileDir(Z) +  Format('wms_tile_z%d_x%d_%d_y%d_%d.tab',
     [Z, 
      block_X_min, block_X_max, 
      block_Y_min, block_Y_max]);

end;   



// ---------------------------------------------------------------
procedure TTile_Loader__11111111111.GetBlockByXY(aX, aY: double; aZ: integer; var aBlock_X, aBlock_Y: integer);
// ---------------------------------------------------------------
var
  eStep: Double;
  iMax: Integer;

begin
  iMax:= Trunc(Math.Power(2,aZ));

  eStep:=(2*DEF_MAX_20037508)/iMax;

  
  aBlock_Y:= Trunc( (DEF_MAX_20037508-aX) / eStep) ;
  aBlock_X:= Trunc( (aY - (- DEF_MAX_20037508)) / eStep) ;

  aBlock_Y:=aBlock_Y mod iMax;
  aBlock_X:=aBlock_X mod iMax;
  
  
end;  



// ---------------------------------------------------------------
function TTile_Loader__11111111111.GetTileBounds_view(aLat, aLon: Double; aZ, aWidth,   aHeight: Integer 
   // var aTileBounds: TTileBounds
    ): TXYBounds;
// ---------------------------------------------------------------

var
  iBlock_Y1: Integer;
  iBlock_Y2: Integer;
  iBlock_X1: Integer;
  iBlock_X2: Integer;
  e1: Double;
  eLat1: Double;
  eLat2: Double;
  eLon1: Double;
  eLon2: Double;
  eStep: Double;
  eScale: double;
  iCol: word;

  iMax: Integer;
  iMaxCols: word;
  iMaxRows: word;
  iRow: word;
  iX: word;
  iY: word;
  X, Y: double;
   
  r: TXYBounds;
begin
  FillChar(FTileBounds,SizeOf(FTileBounds),0);
  FillChar(Result,SizeOf(Result),0);          
  
  iMax:= Trunc(Math.Power(2,aZ));

  Assert(iMax>0);
  
  eStep:=(2*DEF_MAX_20037508)/iMax;

  eScale:=eStep / 256;

  Assert(eScale>0);
  
 // e1:=
  
  //= 256
//  eLat_step:=(2*85)/iMax; 
   
//  eLon_step:=(2*DEF_MAX_20037508)/iMax; 
//  eLon_step:=(2*180)/iMax;  
  
  //-----------------------
  // ����������� �����   
  //-----------------------
//  Geo_EPSG_To_EPSG_plane(DEF_WGS_EPSG_4326, DEF_WGS_MERCATOR_EPSG_3395, aLat, aLon, X, Y);
  Geo_EPSG_To_EPSG_plane(DEF_WGS_EPSG_4326, Params.EPSG, aLat, aLon, X, Y);

//  Params.EPSG

  

  r.X_Min:= x - (aHeight/2) * eScale;  
  r.X_Max:= x + (aHeight/2) * eScale;

  r.Y_Min:= y - (aWidth/2) * eScale;  
  r.Y_Max:= y + (aWidth/2) * eScale;
                      
  

  Plane_EPSG_To_EPSG_geo(Params.EPSG, DEF_WGS_EPSG_4326,  r.X_Max, r.Y_Min, eLat1, eLon1);
  Plane_EPSG_To_EPSG_geo(Params.EPSG, DEF_WGS_EPSG_4326,  r.X_Min, r.Y_Max, eLat2, eLon2);
  

  Result.X_Max:=eLat1;
  Result.Y_Min:=eLon1;  

  Result.X_Min:=eLat2;
  Result.Y_Max:=eLon2;  

  // ---------------------------------------------------------------

  GetBlockByXY (r.X_Max, r.Y_Min,  aZ, iBlock_X1, iBlock_Y1) ;
  GetBlockByXY (r.X_Min, r.Y_Max,  aZ, iBlock_X2, iBlock_Y2) ;  

  FTileBounds.block_X_min:=iBlock_X1;
  FTileBounds.block_Y_min:=iBlock_Y1;

  FTileBounds.block_X_max:=iBlock_X2;
  FTileBounds.block_Y_max:=iBlock_Y2;

  FTileBounds.Z:=aZ;
  
  // ---------------------------------------------------------------

//  FBlockY_row_count:=iBlock_Y2-iBlock_Y1+1;
  FBlockY_row_count:=iBlock_Y2-iBlock_Y1+1;  
  FBlockX_col_count:=IIF(iBlock_X2-iBlock_X1>0, iBlock_X2-iBlock_X1+1,  iMax-iBlock_X1 + iBlock_X2);


  SetLength (FTileArray, FBlockY_row_count, FBlockX_col_count); 

  for iY := 0 to FBlockY_row_count-1 do
  for iX := 0 to FBlockX_col_count-1 do
    begin
      FTileArray [iY,iX].block_x:=(iBlock_X1 + iX) mod iMax;
      FTileArray [iY,iX].block_y:=(iBlock_Y1 + iY) mod iMax;
    end;
    

  
{
  Assert(aTileBounds.block_Y_min <= aTileBounds.block_Y_max);
  Assert(aTileBounds.block_X_min <= aTileBounds.block_X_max);  

  CheckBlock(aTileBounds.block_X_min, aZ);
  CheckBlock(aTileBounds.block_X_max, aZ);  
  CheckBlock(aTileBounds.block_Y_min, aZ);  
  CheckBlock(aTileBounds.block_Y_max, aZ);  
 }
  
end;  

// ---------------------------------------------------------------
function TTile_Loader__11111111111.GetZ(aBoundsX: TXYBounds; aWidth: Integer):
    Integer;
// ---------------------------------------------------------------

var
  eStep: Double;

  iMax: Integer;
  iZ: Integer;
   
  r: TXYBounds;

var
  e: Double;
   eCols: double;
  
begin     
   if aBoundsX.x_max - aBoundsX.y_min = 0 then
   begin
     Result:=1;
     Exit;
   end;

  
  Geo_EPSG_To_EPSG_plane(DEF_WGS_EPSG_4326, Params.EPSG, 
//  Geo_EPSG_To_EPSG_plane(DEF_WGS_EPSG_4326, DEF_WGS_MERCATOR_EPSG_3395, 
    aBoundsX.x_max, aBoundsX.y_min, 
    r.x_max, r.y_min);

//  Geo_EPSG_To_EPSG_plane(DEF_WGS_EPSG_4326, DEF_WGS_MERCATOR_EPSG_3395, 
  Geo_EPSG_To_EPSG_plane(DEF_WGS_EPSG_4326, Params.EPSG, 
    aBoundsX.x_min, aBoundsX.y_max, 
    r.x_min, r.y_max);


  eCols:=0;

  for iZ := 0 to 22 do
  begin
    iMax:= Trunc(Math.Power(2,iZ));  
    eStep:=(2*DEF_MAX_20037508)/iMax;    
    
    eCols:= (r.y_max - r.y_min)/eStep;

    e:=eCols*225;
    
    if e > aWidth then
    begin
      Result:=iZ-1;
      Exit;
    end;
  end;

  Result:=22;
  
end;



// ---------------------------------------------------------------
function TTile_Loader__11111111111.GetTileBounds_mercatorXY(aBlockX, aBlockY, aZ:
    Integer): TXYBounds;
// ---------------------------------------------------------------
var
  eStep: Double;
  iMax: Integer;
  
begin      
  
  iMax:= Trunc(Math.Power(2,aZ));

  eStep:=(2*DEF_MAX_20037508)/iMax;
  //= 256

  Result.X_Max:=  DEF_MAX_20037508 - eStep * aBlockY ;
  Result.X_Min:=  DEF_MAX_20037508 - eStep *(aBlockY+1) ;
  if Result.X_Min < 0.00 then
    Result.X_Min:=0;
                         
  
  Result.Y_Min:= -DEF_MAX_20037508 + eStep * aBlockX ;
  Result.Y_Max:= -DEF_MAX_20037508 + eStep *(aBlockX+1) ;

  if Result.Y_Min < 0.00 then
    Result.Y_Min:=0;
  
end;

// ---------------------------------------------------------------
function TTile_Loader__11111111111.GetRangeTileBounds_MinMax( aTileBounds: TTileBounds) : TXYBounds;

//aX_block_min, aX_block_max, aY_block_min, aY_block_max, aZ: Integer): TXYBounds;
// ---------------------------------------------------------------
var       
  oBounds_min : TXYBounds;  
  oBounds_max : TXYBounds;
begin
    
  oBounds_min:=GetTileBounds_mercatorXY(aTileBounds.block_X_min, aTileBounds.block_Y_min, aTileBounds.Z);
  oBounds_max:=GetTileBounds_mercatorXY(aTileBounds.block_X_max, aTileBounds.block_Y_max, aTileBounds.Z);

  Result.Y_Min:=oBounds_min.y_min; //, oBounds_max.y_min);
  Result.Y_Max:=oBounds_min.y_max; //, oBounds_max.y_max);  

  Result.X_Min:=oBounds_min.x_min; //, oBounds_max.x_min);
  Result.X_Max:=oBounds_min.x_max; //, oBounds_max.x_max);  


//  Result.Y_Min:=Min(oBounds_min.y_min, oBounds_max.y_min);
//  Result.Y_Max:=Max(oBounds_min.y_max, oBounds_max.y_max);  
//
//  Result.X_Max:=Max(oBounds_min.x_max, oBounds_max.x_max);  
//  Result.X_Min:=Min(oBounds_min.x_min, oBounds_max.x_min);
       
end;       

// ---------------------------------------------------------------
procedure TTile_Loader__11111111111.Load_Tile(aX,aY,aZ: integer);
// ---------------------------------------------------------------
var
  sFile: string;    

begin
//  CodeSite.Send('TTile_Loader__11111111111.Load_Tile');


  sFile:=GetFileName_TAB(ax,ay,az, 0);
  if not FileExists(sFile) then
    sFile:=GetFileName_TAB(ax,ay,az, 1);

  
 if not FileExists(sFile) then     
     Load_HTTP(sFile,ax,ay,az)
    
end;

// ---------------------------------------------------------------
function TTile_Loader__11111111111.LoadByRect(aRect: TTileBounds; var aFileName:
    WideString): Boolean;
// ---------------------------------------------------------------
var
  //sFile: string;
 
//  x: Integer;
//  y: Integer;

  dtStart: TDateTime;
  iCol: word;
  iRow: word;

begin   
//  CodeSite.Send('TTile_Loader__11111111111.LoadByRect');


  Result:=False;

  if aRect.Z>=17 then
  begin
    Dec(aRect.block_X_min, 2);
    Inc(aRect.block_X_max, 2);
    
  end;
    //TOP

//  CodeSite.Send('3');
     
   
    
//  FTestWorkspace.Clear;
  CheckBlock(aRect.block_X_min, aRect.Z);
  CheckBlock(aRect.block_X_max, aRect.Z);  
  CheckBlock(aRect.block_Y_min, aRect.Z);  
  CheckBlock(aRect.block_Y_max, aRect.Z);  
  

//  CodeSite.Send('4');
  

  aFileName:=GetFileName_bounds ();

//  CodeSite.Send('5');
//  CodeSite.Send(aFileName);
  
  
  if FileExists(aFileName) then
  begin
    Result:=True;

//  CodeSite.Send('FileExists(aFileName) ');

    Exit;
  end;    
  
//  CodeSite.Send('6');

  
  dtStart:=Now();


//  SetLength (FTileArray, iMaxCols, iMaxRows); 
//    FBlockX_col_count: word;
//    FBlockY_row_count: word;



    for iRow := 0 to FBlockY_row_count-1 do
    for iCol := 0 to FBlockX_col_count-1 do
      with FTileArray[iRow,iCol] do
      begin
 //     FTileArray [iRow,iCol].block_x:=(iBlock_X1 + iCol) mod iMax;
  //    FTileArray [iRow,iCol].block_y:=(iBlock_Y1 + iRow) mod iMax;

         Load_Tile (block_x, block_y, aRect.z);

      end;
    

    {
  
//  if not Params.UseCache then        
    for x := aRect.block_X_min to aRect.block_X_max do
    begin      
      for y := aRect.block_Y_min to aRect.block_Y_max do

        Load_Tile (x,y, aRect.z);


    end;
     }


//  FTestWorkspace.SaveToFile(GetFileDir(aRect.Z) + 'workspace.wor');


  //SecondsBetwe
 

  Log('LoadByRect Done: ' + DateTimeToStr(Now()));
          

 // ShowMessage('LoadByRect Done');
       

       //aRect.Z       //aFileName: WideString; aZ: Integer


       
  result:=LoadByRect_new(aFileName);  //aRect, 

        
 // end;

//  s:=IdHTTP1.ResponseText;
  
end;

// ---------------------------------------------------------------
function TTile_Loader__11111111111.LoadByRect_new(var aFileName:  WideString): Boolean;
/// aRect: TTileBounds;
// ---------------------------------------------------------------
var
  iH: Integer;
  iW: Integer;
  iX: Integer;
  iY: Integer;
 
  s: string;
  sDir: string;
  sFile: string;
//  x: Integer;
//  y: Integer;


  oJPEGImage: TJPEGImage;
  oPNGGImage: TPngObject;//  TPNGImage;

  oImage: TBitmap;
  oImage_file: TImage;
  sImageFile: string;
  sImageFile_jpg: string;
  sImageFile_png: string;

  boundsX: TXYBounds;
  iBlock_X: Integer;
  iBlock_Y: Integer;
  k: Integer;
//  oIni: TIniFile;
  sFile_bmp: string;
//  sFile_img: string;
  sFile_png: string;

begin      

//  CodeSite.Send('TTile_Loader__11111111111.LoadByRect_new');

  Log('make image...');
  SaveTime;
  

  result:=False;


  oImage:=TBitmap.Create();
//  oImage.PixelFormat  :=pf16bit;
  oImage.PixelFormat  :=pf24bit;

  oImage_file:=TImage.Create(nil);


//    FBlockX_col_count: word;
//    FBlockY_row_count: word;


  iW:=FBlockX_col_count * 256;
  iH:=FBlockY_row_count * 256;

//  iW:=(aRect.block_X_max - aRect.block_X_min +1) * 256;
//  iH:=(aRect.block_Y_max - aRect.block_Y_min +1) * 256;

  try
    oImage.Width :=iW;
    oImage.Height:=iH;

  except
    s:=Format('Width %d  Height %d', [iW, iH]);

    ShowMessage(s);

    FreeAndNil(oImage);
    FreeAndNil(oImage_file);

    exit;
  end;    // try/finally


//  
//  for iRow := 0 to High(FTileArray) do
//    for iCol := 0 to High(FTileArray[iRow]) do
//    begin
// //     FTileArray [iRow,iCol].block_x:=(iBlock_X1 + iCol) mod iMax;
//  //    FTileArray [iRow,iCol].block_y:=(iBlock_Y1 + iRow) mod iMax;
//
//      Load_Tile (x,y, aRect.z);
//
//    end;  
    
  for iY := 0 to FBlockY_row_count-1 do
  for iX := 0 to FBlockX_col_count-1 do
    with FTileArray [iY,iX] do
    begin

//      FTileArray [iX,iY].block_x:=(iBlock_X1 + iX) mod iMax;
//      FTileArray [iX,iY].block_y:=(iBlock_Y1 + iY) mod iMax;
//    end;
//
//    
//  for iRow := 0 to High(FTileArray) do
//    for iCol := 0 to High(FTileArray[iRow]) do
//      with FTileArray[iRow,iCol] do
//      begin
//

//  for x := aRect.block_X_min to aRect.block_X_max do     
//    for y := aRect.block_Y_min to aRect.block_Y_max do
//    begin   

      sFile:=GetFileName_TAB(block_x, block_y, FTileBounds.Z, 0);
      if not FileExists(sFile) then
        sFile:=GetFileName_TAB(block_x, block_y, FTileBounds.Z, 1);
      
    
      assert (sFile<>'');
    
                  
      sImageFile_jpg:= ChangeFileExt(sFile,'.jpg');        
      sImageFile_png:= ChangeFileExt(sFile,'.png');      

   //   sExt:=

      //---------------------------------------------
      if FileExists(sImageFile_jpg) then
      //---------------------------------------------
      begin
        oJPEGImage:=TJPEGImage.Create();  
        oJPEGImage.LoadFromFile(sImageFile_jpg);
       // oJPEGImage.CompressionQuality:=

        oImage_file.Picture.LoadFromFile(sImageFile_jpg);

        FreeAndNil(oJPEGImage);  
        
      end
      
      else
      //---------------------------------------------
      if FileExists(sImageFile_png) then
      //---------------------------------------------
        oImage_file.Picture.LoadFromFile(sImageFile_png)
      else
      
      begin
        Log('ERROR: file not found - '+ sFile);
        
        Continue;
      end;
      



//      iBlock_X:=block_x - aRect.block_X_min;
//      iBlock_Y:=block_y - aRect.block_Y_min;

//      iBlock_X:=block_x - aRect.block_X_min;
//      iBlock_Y:=block_y - aRect.block_Y_min;

//      oImage.Canvas.Draw(256*iBlock_X, 256*iBlock_Y, oImage_file.Picture.Graphic);

      oImage.Canvas.Draw(256*iX, 256*iY, oImage_file.Picture.Graphic);

    end;

 // end;



{$IFDEF grid} 
 

  oImage.Canvas.Pen.Color:=clWhite;
  oImage.Canvas.Pen.Width:=1;  


//    FBlockX_col_count: word;
//    FBlockY_row_count: word;

  
//  for iX := aRect.block_X_min to aRect.block_X_max do     
  for iX := 0 to FBlockX_col_count-1 do     
  begin
//    iX:=(x - aRect.block_X_min)*256;

    oImage.Canvas.MoveTo(iX*256, 0);
    oImage.Canvas.LineTo(iX*256, oImage.Height);      
    
  end;       

  
  for iY := 0 to FBlockY_row_count-1 do
//  for y := aRect.block_Y_min to aRect.block_Y_max do
  begin  
//    iY:=(y - aRect.block_Y_min)*256;
           
    oImage.Canvas.MoveTo(0, iY*256);
    oImage.Canvas.LineTo(oImage.Width, iY*256);
            
  end;

 // end;

  oImage.Canvas.MoveTo(0,0);
  oImage.Canvas.LineTo(0,oImage.Height-1);
  oImage.Canvas.LineTo(oImage.Width,oImage.Height-1);
  oImage.Canvas.LineTo(oImage.Width-1,0);  


{$ENDIF} 



  boundsX:=GetRangeTileBounds_MinMax (FTileBounds);

  sFile:=GetFileName_bounds ();
                                 

  sFile_bmp:= ChangeFileExt(sFile,'.bmp');
  oImage.SaveToFile(sFile_bmp);

  sFile_png:= ChangeFileExt(sFile,'.png');


//  img_BMP_to_PNG (sFile_bmp,sFile_png);


  oPNGGImage:=TPngObject.Create();
  oPNGGImage.Assign(oImage);

//  sFile_img:= ChangeFileExt(sFile,'.bmp');

//  oImage.SaveToFile(sFile_img);
  oPNGGImage.SaveToFile(sFile_png);
  FreeAndNil(oPNGGImage);



  SaveTileFile_WGS_EPSG_3395_bounds(sFile, sFile_png, Params.EPSG, FTileBounds.z,
     boundsX, iW, iH);


  aFileName:=sFile;
//  aZ:=aRect.Z;


//  if Assigned(FOnFileLoad) then
 //   FOnFileLoad(sFile, aRect.Z);


  FreeAndNil(oImage);
  FreeAndNil(oImage_file);


  SysUtils.DeleteFile(sFile_bmp);

  
  result:=True;

  Log('make image done - '+ GetTimeDiff());

  
//  aMapX.OnMapViewChanged:=nil; // procedure Tfrm_Map.Map1MapViewChanged(Sender: TObject);
  

end;


// ---------------------------------------------------------------
function TTile_Loader__11111111111.Load_CMap(const aMapX: CMapX): HResult;
// ---------------------------------------------------------------
var
  k: Integer;

  blRect: TBLRect;
  e: Double;

  iResult: HResult;
 

  sFileName: WideString; 

  r: TLoad_params;

  rBounds: TXYBounds;
  s: string;
              
//  FOnMapViewChanged: TNotifyEvent;

  v:CMapXLayer;

  rTileBounds: TTileBounds;

 // rTileBounds: TTileBounds;
  
label
  label_exit  ;
  
const
  def_offset = 0;  // 0.00001;
    
begin
//  Load_Ini;

//  k:=aMapX.NumericCoordSys.Datum.Ellipsoid;
                                                    
 if aMapX.NumericCoordSys.Datum.Ellipsoid<>28 then
 begin   
   aMapX.NumericCoordSys.Set_ (miLongLat, DATUM_WGS84,   //28;
        EmptyParam, EmptyParam, EmptyParam, EmptyParam,
        EmptyParam, EmptyParam, EmptyParam, EmptyParam,
        EmptyParam, EmptyParam, EmptyParam, EmptyParam);

   Log('aMapX.NumericCoordSys.Set_ (miLongLat, DATUM_WGS84,');
 end;


  Assert(Params.Dir>'');
  Assert(Params.LayerName>'', 'Params.LayerName = ...');
     

  CursorHourGlass;

//  aMapX. OnMapViewChanged;
  
//  aMapX.OnMapViewChanged:=nil; // procedure Tfrm_Map.Map1MapViewChanged(Sender: TObject);

                
  FillChar(r,SizeOf(r),0);

CodeSite.Send('aMapX.CenterY : ' + FloatToStr(aMapX.CenterY));
CodeSite.Send('aMapX.CenterX : ' + FloatToStr(aMapX.CenterX));
CodeSite.Send('aMapX.Zoom : ' + FloatToStr(aMapX.Zoom));

  if aMapX.CenterY < 1 then
    aMapX.ZoomTo (3000, 57, 59);
  
      
  r.Lat:=aMapX.CenterY;//BLPoint.b;
  r.Lon:=aMapX.CenterX;//BLPoint.l;  
  
  

//  FCenterLat:=r.Lat;
///  FCenterLon:=r.Lon;

  
  r.Width :=Round(aMapX.MapScreenWidth);
  r.Height:=Round(aMapX.MapScreenHeight);  


  blRect:=mapx_XRectangleToBLRect(aMapX.Bounds);
//  blRect:=mapx_GetBounds(Map1);

  FillChar(rBounds,SizeOf(rBounds),0);

  if Abs(blRect.TopLeft.b - blRect.BottomRight.b) > 0 then
  begin

    rBounds.x_max:=blRect.TopLeft.B + def_offset;
    rBounds.y_min:=blRect.TopLeft.L - def_offset;
    rBounds.x_min:=blRect.BottomRight.B - def_offset;
    rBounds.y_max:=blRect.BottomRight.L + def_offset;
    
  end;
  



      
  iResult:=Load(r, sFileName, rBounds);  //, rTileBounds

//CodeSite.Send(sFileName);
  
  
  Assert(sFileName<>'', 'sFileName = '+ sFileName);

  if mapx_FileExists(aMapX, sFileName) then
    goto label_exit;

//  if sFileName then
  

  
  blRect.TopLeft.B:=rBounds.x_max;
  blRect.TopLeft.L:=rBounds.y_min;  
  blRect.BottomRight.B:=rBounds.x_min;
  blRect.BottomRight.L:=rBounds.y_max;  
  
                                            

  //!!!!!!!!!!!
  aMapX.AutoRedraw :=False; 

  
                                            
  mapx_TilesClear (aMapX);

  aMapX.Layers.Add (sFileName, aMapX.Layers.Count+1) ;

  
//--  map_MapAdd_Ex_CMapX(aMapX, sFileName);



  mapx_SetBounds_CMapX(aMapX, blRect);
         
         
  
  //!!!!!
  aMapX.AutoRedraw :=True;    
 


         
label_exit:


//  aMapX.         

//  aMapX.OnMapViewChanged:=FOnMapViewChanged; // procedure Tfrm_Map.Map1MapViewChanged(Sender: TObject);
           
   
  CursorDefault;
 
end;

// ---------------------------------------------------------------
function TTile_Loader__11111111111.Load(aParams: TLoad_params; var aFileName: WideString;
                           var aXYBounds: TXYBounds): HResult;
                           //; var aTileBounds: TTileBounds
// ---------------------------------------------------------------
var
  iZ: Integer;

begin    
//  CodeSite.Send('TTile_Loader__11111111111.Load');

  if Params.Z>0 then
    iZ:=Params.Z
  else  
    iZ:=GetZ (aXYBounds,aParams.Width);


  Params.Result_Z:= iZ;
                              

  Assert(aParams.Width>0);
  Assert(aParams.Height>0);  


  aXYBounds:=GetTileBounds_view(aParams.Lat, aParams.Lon, iZ,  aParams.Width, aParams.Height); //, aTileBounds
      

  FTileBounds.Z:=iZ;

  if LoadByRect(FTileBounds,  aFileName) then
    Result:= S_OK
  else
    Result:= S_FALSE;
  
end;

// ---------------------------------------------------------------
procedure TTile_Loader__11111111111.CheckBlock(var aBlock: integer; aZ: integer);
// ---------------------------------------------------------------
var
  iMax: integer;
begin
  iMax:= Trunc(Math.Power(2, aZ))-1;
 
  if aBlock<0 then
    aBlock:=0 else

  if aBlock>=iMax then 
    aBlock:=iMax; 
end;





procedure TTile_Loader__11111111111.Log(aMsg: string);
begin
//
//  if assigned(dlg_WMS_log) then
//    dlg_WMS_log.Add(aMsg);
//  
// 

  CodeSite.Send(aMsg);

//  CodeSite.
end;


// ---------------------------------------------------------------
procedure TTile_Loader__11111111111.SaveTileFile_WGS_EPSG_3395(aFileName: string; aX, aY,
    aZ: Integer; aImgFileName: string; aEPSG: Integer);
// ---------------------------------------------------------------
var
  boundsX: TXYBounds;  
begin
  boundsX:=GetTileBounds_mercatorXY(aX, aY, aZ);

  SaveTileFile_WGS_EPSG_3395_bounds(aFileName, aImgFileName, aEPSG, aZ, boundsX, 255,255);
  
end;

// ---------------------------------------------------------------
procedure TTile_Loader__11111111111.SaveTileFile_WGS_EPSG_3395_bounds(aFileName,
    aImgFileName: string; aEPSG, aZ: Integer; aBounds: TXYBounds;
    aImage_width, aImage_height: Integer);
// ---------------------------------------------------------------
const

//  DEF_CoordSys_WGS =
//    'CoordSys Earth Projection 1, 104';


  DEF_FILE = 
  '!table'+ CRLF +
  '!version 300'+ CRLF +
  '!charset WindowsCyrillic'+ CRLF +
  ''+ CRLF +
  'Definition Table'+  CRLF +
  '  File ":file"'+ CRLF +
  '  Type "RASTER"'+  CRLF +
  
  '  (:point1_lon,:point1_lat) (:img_width,:img_height) Label "point 1",'+ CRLF +
  '  (:point2_lon,:point2_lat) (0,:img_height) Label "point 2",'+CRLF +
  '  (:point3_lon,:point3_lat) (0,0) Label "point 3",'+ CRLF +
  '  (:point4_lon,:point4_lat) (:img_width,0) Label "point 4"'+ CRLF +
  

//  '  CoordSys Earth Projection 1, 104'+ CRLF +
  '  CoordSys Earth Projection 10, 104, "m", 0'+ CRLF +
 // '  Units "m"'+ CRLF +
//  '  Units "degree"'+ CRLF +  
  '  RasterStyle 4 1'+ CRLF +
  '  RasterStyle 7 0'; //+ CRLF +
    
           
var
//  eOffset_lat: Double;
//  eOffset_lon: Double;
 
  s: string;

  points: array[0..3] of TLatLon;

  points_WGS: array[0..3] of TLatLon;

  rTestPoint: TLatLon;
  rTestPoint_wgs: TLatLon;
  
  oIni: TIniFile;
 
  I: Integer;
//  iOffset: Integer;
  sFile: string;

const
// epsg 3857
 // DEF_MAX =  20037508.34279000;          

//  DEF_WEB_EPSG_3857 = 3857;
  
 // DEF_WGS_EPSG_41001 = 41001;  //      EPSG:3395<

  DEF_EPSG_3395 = 3395;
  
  DEF_OFFSET_MIN=0;
  DEF_OFFSET_CENTER=30;  
  DEF_OFFSET_MAX=500;  

//  DEF_Offset_LON=+15;
//  DEF_Offset_LAT=25;
  

begin

  
  
  aBounds.X_Max:=aBounds.X_Max ;
  aBounds.X_Min:=aBounds.X_Min ;
  
  aBounds.Y_Min:=aBounds.Y_Min ;
  aBounds.Y_Max:=aBounds.Y_Max ;
  

//  Log ( Format(' bounds.X_Max: %n   Y_Max: %n ',[aBounds.X_Max, aBounds.Y_Max]));
//  Log ( Format(' bounds.X_Min: %n   Y_Min: %n ',[aBounds.X_Min, aBounds.Y_Min]));
  

  Assert(aEPSG>0);



//  aEPSG          


  // Geo_EPSG_To_EPSG_plane();

//     procedure geo_Plane_EPSG_To_GeoWGS_plane(aEpsg_from, aEpsg_to: Integer; aX, aY:
  //    double; var aX_out, aY_out: double); external DEF_LIB_geo_convert;
  
  
//  function mapGeoWGS84ToPlane42(Map:HMap; var Bx, Ly : double) : integer;
//  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
//  external sGisAcces;
//

  Plane_EPSG_To_EPSG_plane(aEPSG,  DEF_EPSG_3395,
    aBounds.X_Min, aBounds.Y_Max, points[0].Lat  , points[0].Lon  );


  Plane_EPSG_To_EPSG_plane(aEPSG, DEF_EPSG_3395,
    aBounds.X_Min, aBounds.Y_Min, points[1].Lat  , points[1].Lon  );

  Plane_EPSG_To_EPSG_plane(aEPSG, DEF_EPSG_3395,
    aBounds.X_Max, aBounds.Y_Min, points[2].Lat  , points[2].Lon  );

  Plane_EPSG_To_EPSG_plane(aEPSG, DEF_EPSG_3395,
    aBounds.X_Max, aBounds.Y_Max, points[3].Lat  , points[3].Lon  );

  
    
  
//
     
      
      for I := 0 to High(points) do
        Plane_EPSG_To_EPSG_geo(DEF_EPSG_3395, DEF_WGS_EPSG_4326, 
        
//        Plane_EPSG_To_EPSG_geo(DEF_WGS_MERCATOR_EPSG_3395, DEF_WGS_EPSG_4326, 
           points[i].Lat, points[i].Lon, 
           points_wgs[i].Lat, points_wgs[i].Lon 
           );     
    
    


    

  s:=DEF_FILE;

  sFile:=ExtractFileName(aImgFileName);
  
  s:=ReplaceStr(s, ':file', sFile);

  //aImage_width,  aImage_height
  
  s:=ReplaceStr(s, ':img_width',  IntToStr(aImage_width));
  s:=ReplaceStr(s, ':img_height', IntToStr(aImage_height));  


  for I := 0 to 3 do
  begin
    s:=ReplaceStr(s, Format(':point%d_lon',[i+1]), FloatToStr(Trunc(points[i].Lon)));
    s:=ReplaceStr(s, Format(':point%d_lat',[i+1]), FloatToStr(Trunc(points[i].Lat )));

  end;
         

 // ShowMessage(s);
  
  StrToFile_ (aFileName, s );

end;

// ---------------------------------------------------------------
function TTile_Loader__11111111111.Load_HTTP(aFileName: string; aX, aY, aZ: integer):
    boolean;
// ---------------------------------------------------------------

    // ---------------------------------------------------------------
    function DoGetImageFormat(aFileName: string): string;
    // ---------------------------------------------------------------
    var
      oFileStream: TFileStream;
      buf: array[0..3] of AnsiChar ;
      iRead: Integer;
      k: Integer;

      sComp: AnsiString;
    begin
      
//
//var
//  Comp,Search : AnsiString;
//begin
//  SetString(Comp, PAnsiChar(@CompArr[0]), Length(CompArr));
//  SetString(Search, PAnsiChar(@SearchArr[0]), Length(SearchArr));
//  Result := Pos(Search,Comp) - 1;
//end;      
      

      oFileStream:=TFileStream.Create (aFileName, fmShareDenyNone);
      oFileStream.Position:=6;
    //    oFileStream:=TFileStream.Create (aFileName, fmOpenReadWrite);
      iRead:=oFileStream.Read (buf, SizeOf(buf));

      SetString(sComp, PAnsiChar(@buf[0]), Length(buf));

    //  k:=StrComp(buf, 'JFIF') ;
      
      if sComp= 'JFIF' then 
        result:='.jpg'
      else
        result:='.png';      
                   
      
      
      FreeAndNil(oFileStream);
    end;
    





var
  b: Boolean;
  iSize: Int64;
  iSize1: Int64;
  k: Integer;
  oFileStream: TFileStream;
  s: string;
  sContentType: string;
  sExt: string;
  sFile_bin: string;
  sFile_img: string;
  sURL: string;
begin
//  CodeSite.Send('TTileManagerX.Load_HTTP ...');


  Assert(Params.URL<>'');
  Assert(aZ<30);


  if FileExists(ChangeFileExt(aFileName,'.png')) or 
     FileExists(ChangeFileExt(aFileName,'.jpg'))
  then
  begin
    Result:=True;
    Exit;
  end;
  
 

 // sFile_img:=ChangeFileExt(aFileName,'.png');


  


  s:=ExtractFileDir(aFileName);
  ForceDirectories(s);
  

//  sFile_bin:=ChangeFileExt(aFileName,'.bin');
  

  {
  if RightStr(Params.URL,4)='.png' then
    sFile_img:=ChangeFileExt(aFileName,'.png') 
  else                          
    sFile_img:=ChangeFileExt(aFileName,'.jpg');
   }
  
  if Pos('#z',Params.URL)>0 then
  begin
    sURL:= Params.URL;

    sURL:= ReplaceStr (sURL, '#z', IntToStr(aZ) );
    sURL:= ReplaceStr (sURL, '#y', IntToStr(aY) );    
    sURL:= ReplaceStr (sURL, '#x', IntToStr(aX) );    

//    if RightStr(sURL,4)='.png' then
//      sFile_img:= ChangeFileExt(aFileName,'.png'); 

  end else
    sURL:=Params.URL + Format('&y=%d&x=%d&z=%d',[ay,ax,az]) ;//+ '&format=image/png';
//    sURL:=Params.URL + Format('&y=%d&x=%d&z=%d',[ay,ax,az]);


// sURL:=sURL + '&format=image/png';

  Log ('Loading... ' + sURL)   ;

//   function IsStrInStrArray(aStr: string; aStrArray: array of string): boolean;
  
  s:=Params.LayerName;

//  sExt:= IIF ((LeftStr(Params.LayerName,6)='Google') 
//       or     (LeftStr(Params.LayerName,6)='Yandex')
//               , '.jpg', '.png');

//  if IsStrInStrArray ()
               


        
  
  
//  s:=Params.ImgExt;  

//  if Pos then


//  CompareText()
  
//  sFile_img:=ChangeFileExt(aFileName, sExt);
  sFile_bin:=ChangeFileExt(aFileName, '.bin');
  
  
  b:=DownloadFile (sURL, sFile_bin);
  if not b then
    Exit;

  sExt:=DoGetImageFormat (sFile_bin);
  sFile_img:=ChangeFileExt(aFileName, sExt);
  
  TFile.Move(sFile_bin, sFile_img);
  
//  b:=DownloadFile (sURL, sFile_img);

  //DoCheckImageFormat (sFile_bin);
  
//
//  oFileStream:=TFileStream.Create (aFileName, fmShareDenyNone);
////    oFileStream:=TFileStream.Create (aFileName, fmOpenReadWrite);
//  oFileStream.Read (rFileHeader, SizeOf(rFileHeader));
//
//  FreeAndNil(oFileStream);
//  
  
  
//  oFileStream:=TFileStream.Create(  sFile_img, fmCreate);
//  oFileStream:=TFileStream.Create(  sFile_bin, fmCreate);
       
//  try                                                                      


                                                 
  //  Log('IdHTTP1.Get start: ' + sFile_img + ' - '+ DateTimeToStr(Now()));

 //   IdHTTP1.Request.ContentType:='image/png';
 //   IdHTTP1.Get(sURL, oFileStream);


                

//  s:=IdHTTP1.Get(sURL);

                

//    Log('IdHTTP1.Response.ContentType - ' + IdHTTP1.Response.ContentType);   
//    Log('IdHTTP1.Content-Disposition - '  + IdHTTP1.Response.ContentDisposition);

//      Content-Type: application/octet-stream
//Content-Disposition: attachment; filename="picture.png"

    
    

    
    //content-type:image/png
    
    Result:=True;
    
//    Log ('Done. ' + sURL + '->' + sFile_img)   ;

    
  //  Log('IdHTTP1.Get done: ' + sFile_img + ' - ' + DateTimeToStr(Now()));


 //   iSize1:=GetFileSize (sFile_img);
 {
    iSize:=oFileStream.Size;
    if iSize=0 then
      Result:=False;   
    
    if iSize=0 then
      Log('Error - '+ sFile_img + '  -  iSize=0');


  FreeAndNil(oFileStream);
 } 
    
//    Assert(iSize>0);
    

  //  k:=IdHTTP1.ResponseCode; 

    
  //  if iSize1=0 then
  //  begin
    //  k:=IdHTTP1.ResponseCode; 

    //  Log(sURL);
      
   //   ShowMessage('iSize1=0');
  //  end;      

    {
    if iSize>0 then
    begin
      sContentType:=IdHTTP1.Response.ContentType;

      
    
      if sContentType = 'application/octet-stream'  then sFile_img:=ChangeFileExt(aFileName,'.jpg') else
      if sContentType = 'image/png'  then sFile_img:=ChangeFileExt(aFileName,'.png') else
      if sContentType = 'image/jpeg' then sFile_img:=ChangeFileExt(aFileName,'.jpg') 
      else
      begin
//      Content-Type: application/octet-stream
//Content-Disposition: attachment; filename="picture.png"

     //   ShowMessage('Error Message:   IdHTTP1.Response.ContentType '+  sContentType + ' - ' + sFile_bin);
      
        Log('Error!! sContentType - ' + IdHTTP1.Response.ContentType);
        Log('Error!! ResponseText - ' + IdHTTP1.Response.ResponseText);

      
        Log('Error!! IdHTTP1.Response.ContentType '+  sContentType + ' - ' + IdHTTP1.Response.ResponseText  +' ' + sFile_bin);

      
     //   raise Exception.Create('Error Message:   IdHTTP1.Response.ResponseText '+  IdHTTP1.Response.ResponseText + ' - ' + sFile_bin);

        Result:=False;    
        Exit;
      end;
//       sFile_img:='';
        

      TFile.Move (sFile_bin, sFile_img);
//
     }
        
//
//    {$IFDEF use_debug}    
//    {$ELSE}      
//
//    {$ENDIF}      

     
  //  end;  

//  finally

//  except on E: Exception do
//  begin
//    Result:=False;            
//
//    Log('Error  - '+ e.Message);

 //   ShowMessage(e.Message);

  {  
    if IdHTTP1.ProxyParams.BasicAuthentication then
    begin
       s:=BoolToStr(IdHTTP1.ProxyParams.BasicAuthentication);
    

       Log (  Format('Proxy (%s) -  ProxyServer: %s;  ProxyPort: %d', 
         [
          BoolToStr(IdHTTP1.ProxyParams.BasicAuthentication),
          IdHTTP1.ProxyParams.ProxyServer,
          IdHTTP1.ProxyParams.ProxyPort
         ] ));
      
    end;
    

    
    FreeAndNil(oFileStream);

    }
 // end;    
          
 // end;



//  if iSize=0

 // iSize1:=GetFileSize (sFile_img);

//  if iSize1=0 then
 //   ShowMessage('iSize1=0');
 //   Assert(iSize1>0);
  
  
  if not Result then
  begin  
//    ShowMessage(sFile_img + ' iSize=0  ');
  
//    Log (sFile_img + ' iSize=0  ');
//    SysUtils.DeleteFile(sFile_img);
//    SysUtils.DeleteFile(sFile_bin);    
    
  end ;
//    TFile.FileS
  

  Log('TTileManagerX.Load_HTTP ... Done');
  

  
  // TODO -cMM: TTile_Loader__11111111111.Load_HTTP default body inserted
end;





// ---------------------------------------------------------------
procedure TTile_Loader__11111111111.Test;
// ---------------------------------------------------------------
var
  aBlock_Y1: Integer;
  eLat: Double;
  eLon: Double;
  Block_X1: Integer;
  X,y: Double;
begin
  eLat:=60;
  eLon:=181;

  Geo_EPSG_To_EPSG_plane(DEF_WGS_EPSG_4326, Params.EPSG, eLat, eLon, X, Y);

  GetBlockByXY (x, y,  7, Block_X1, aBlock_Y1) ;

end;  


//var
//  obj: TTile_Loader__11111111111;

begin
//  obj:=TTile_Loader__11111111111.Create;
//  obj.Test;
  
end.

{
  

  Plane_EPSG_To_EPSG_geo(Params.EPSG, DEF_WGS_EPSG_4326,  r.X_Max, r.Y_Min, eLat1, eLon1);
  Plane_EPSG_To_EPSG_geo(Params.EPSG, DEF_WGS_EPSG_4326,  r.X_Min, r.Y_Max, eLat2, eLon2);
  

  Result.X_Max:=eLat1;
  Result.Y_Min:=eLon1;  

  Result.X_Min:=eLat2;
  Result.Y_Max:=eLon2;  

  // ---------------------------------------------------------------

  GetBlockByXY (r.X_Max, r.Y_Min,  aZ, Block_X1, aBlock_Y1) ;
  GetBlockByXY (r.X_Min, r.Y_Max,  aZ, Block_X2, aBlock_Y2) ;  




  
// ---------------------------------------------------------------
procedure TTile_Loader__11111111111.MakeTileMap__1111111(aFileName: string; aZ: Integer;
    ablRect: TBLRect; aTileBounds: TTileBounds);
// ---------------------------------------------------------------
var
  oMapFile: TmiMap;

  rStyle: TmiStyleRec;
  BLPointArray: TBLPointArray;
//  BLPointArray_new: TBLPointArray;
  
  BLPoint: TBLPoint;  
  iMax: Integer;
  iBlockX: Integer;
  iBlockY: Integer;

  blRect: TBLRect;
  blRect_wgs: TBLRect;
  I: Integer;
  
  r: TXYBounds;
  s: string;

  oTMifWorkspace:   TMifWorkspace;
  sFile: string;

  
begin
  if ablRect.TopLeft.B=0 then
    Exit;
           

  oTMifWorkspace:=TMifWorkspace.create;
  oTMifWorkspace.AddFile(aFileName);

//         

  oMapFile:=TmiMap.Create;
  oMapFile.CreateFile1(aFileName, //'d:\1.tab',
                     [
                      mapX_Field ('name',   miTypeString),

                      mapX_Field ('x',   miTypeInt),
                      mapX_Field ('y',   miTypeInt),
                      mapX_Field ('z',   miTypeInt),

                      mapX_Field ('x_max',   miTypeFloat),
                      mapX_Field ('y_min',   miTypeFloat),
                      mapX_Field ('x_min',   miTypeFloat),
                      mapX_Field ('y_max',   miTypeFloat)

                     ]);   
  
//  oMapFile.BeginAccess;

  
  rStyle.Clear;
  rStyle.RegionPattern:=1;
  rStyle.RegionBorderStyle:=2;
  rStyle.RegionBorderWidth:=1;
  rStyle.RegionBorderColor:=clBlack;
                        
  oMapFile.PrepareStyle(miFeatureTypeRegion, rStyle);
 
  BLPointArray:=ablRect.ToBLPointArray;
  oMapFile.WriteRegion(BLPointArray, 
     [
      'name', 'blRect',

      'x_max', ablRect.TopLeft.B,
      'y_min', ablRect.TopLeft.L,
      'x_min', ablRect.BottomRight.B,
      'y_max', ablRect.BottomRight.L

     ]);


  //aTileBounds
  
  iMax:= Trunc(Math.Power(2,aZ));


//  asseer
  
  rStyle.RegionBorderColor:=clRed;
  oMapFile.PrepareStyle(miFeatureTypeRegion, rStyle);

  
  for iBlockX := aTileBounds.block_X_min to aTileBounds.block_X_max do
    for iBlockY := aTileBounds.block_Y_min to aTileBounds.block_Y_max do   
    begin        
    
     sFile:=GetFileName_TAB(iBlockX, iBlockY, aZ, 0);
     if not FileExists(sFile) then
        sFile:=GetFileName_TAB(iBlockX, iBlockY, aZ, 1);
                             
  //   assert (FileExists(sFile) );

      if FileExists(sFile) then
       oTMifWorkspace.AddFile(sFile);


  //   continue;

     
    
      r:=GetTileBounds_mercatorXY(iBlockX,iBlockY,aZ);

      blRect.TopLeft.B:=r.x_max;
      blRect.TopLeft.L:=r.y_min;

      blRect.BottomRight.B:=r.x_min;
      blRect.BottomRight.L:=r.y_max;

    
      
      BLPointArray:=blRect.ToBLPointArray;


//      BLPointArray_new:=blRect.ToBLPointArray;

     Assert(Length(BLPointArray)=5);


//
     
      
      for I := 0 to High(BLPointArray) do
        Plane_EPSG_To_EPSG_geo(Params.EPSG, DEF_WGS_EPSG_4326, 
        
//        Plane_EPSG_To_EPSG_geo(DEF_WGS_MERCATOR_EPSG_3395, DEF_WGS_EPSG_4326, 
           BLPointArray[i].B, BLPointArray[i].L, 
           BLPointArray[i].B, BLPointArray[i].L); 

         
  
      s:= Format('x:%d  y:%d',[iBlockX,iBlockY]) + LF+ blRect_wgs.ToString;
  

     Assert(Length(BLPointArray)=5);
    
   
    //  oMapFile.
    // + LF + r.ToString
      oMapFile.WriteRegion(BLPointArray,
         [
          'name', s,
          'x', iBlockX,
          'y', iBlockY,
          'z', aZ,

          'x_max', r.x_max,
          'y_min', r.y_min,
          'x_min', r.x_min,
          'y_max', r.y_max

         ]);

      
    end;
    

///////////  oTMifWorkspace.SaveToFile( ChangeFileExt(aFileName,'.wor') );
                          
                              
//  oMapFile.EndAccess;

  FreeAndNil(oMapFile);
  
  FreeAndNil(oTMifWorkspace);
  
end;
  

  
