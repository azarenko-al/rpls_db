unit u_WMS_file;

interface

uses
  Windows, System.Classes, SysUtils, dialogs, Forms,  System.Generics.Collections,
  CodeSiteLogging, Xml.XMLIntf,

  wmslist,
  u_func;


type


 //-------------------------------------------------------------------
  TWmsLayer_ = class
  //-------------------------------------------------------------------
  public    
    Topic_Name: string;
            
    Layer_Name: string;     //Name="�������" UnicId="GoogleSat">
    Layer_UnicId: string; 
    
    ConnectedSting: record
        URL            : string;
        Projection_CRS : string;
        Matrix_name    : string;
    end;
    

    function GetDisplayName: string;
    function Get_EPSG: Integer;

//		<Topic Link="wmslist\google.bmp" Name="Google" img="">
//				<Layer Name="Test" UnicId="Test">
//					<ConnectedSting>
//						<URL><![CDATA[http://alex3:8088/geoserver/gwc/service/gmaps?layers=russia&zoom=#z&x=#x&y=#y&format=image/png]]></URL>
//						<ID Number="1" Value="RosLandSat"/>
//						<Name Number="1" Value="RosLandSat"/>
//						<Matrix Name="urn:ogc:def:wkss:OGC:1.0:GoogleMapsCompatible"/>
//					</ConnectedSting>
//					<PortalStadndart>UTMS</PortalStadndart>
//					<Alghoritm>20</Alghoritm>
//					<Transparent>0xffffffff</Transparent>
//				</Layer>

  end;

  


  //-------------------------------------------------------------------
  TWmsFile = class(TList<TWmsLayer_>)
  //-------------------------------------------------------------------
//  TOptionsParameterDict= class(TDi1ctionary<String, TOptionsParameterItem>);
  
  private
  public
    function AddItem: TWmsLayer_;
    function Find(aUnicId: string): TWmsLayer_;

    function IndexOf(aUnicId: string): integer;
    function GetText: string;
    
    procedure LoadFromFile(aFileName: string);

  end;
  


implementation

  

// ---------------------------------------------------------------
procedure TWmsFile.LoadFromFile(aFileName: string);
//// ---------------------------------------------------------------
//const
//  DEF_FILE = 'W:\WMS\tests\data\wmslist.xml';

var
  I: Integer;
  iCount: Integer;
  iTopic: Integer;
  k: Integer;
  oLayer: TWmsLayer_;
  s: string;
  s1: string;
  s2: string;
  oTileMatrixes_Identifier_CRS: TstringList;
  v: IXMLGeoportalType;

  vTopic: IXMLTopicType;
  

begin
  oTileMatrixes_Identifier_CRS := TstringList.Create();

  Clear;  
  

  v:= LoadGeoportal(aFileName);

//  Topics.Clear;
//  TileMatrixes_Identifier_CRS.Clear;
  //Layers.

                  
  oTileMatrixes_Identifier_CRS.Clear;

  iCount:=v.Defines.TileMatrixSet.Count;

  for i := 0 to iCount-1 do
  begin
    s1:=v.Defines.TileMatrixSet[i].Identifier;
    s2:=v.Defines.TileMatrixSet[i].CRS;
    
    oTileMatrixes_Identifier_CRS.Values[s1]:=s2;
  end;             
  

  iCount:=v.Portal.Topic.Count;

  //vNewDoc.Add

 // vTopics_new:=vDoc_new.AddChild('Topics');


  for iTopic := 0 to iCount-1 do
  begin
    vTopic:=v.Portal.Topic[iTopic];

//	<Portal>
//		<Topic Link="wmslist\google.bmp" Name="Google" img="">
    

 ////   vTopic_new:=vTopics_new.AddChild('topic');
   // vTopic_new.Attributes['name']:= vTopic.Name;
  
  
    s:=vTopic.Name;
        
     CodeSite.Send( 'vTopic.Name - ' + vTopic.Name );   

  //  if vTopic.Layer.Count>0 then
  //    Topics.Add(vTopic.Name);

    for I := 0 to vTopic.Layer.Count-1 do
    begin     
    
//		<Topic Link="wmslist\google.bmp" Name="Google" img="">    
//				<Layer Name="Test" UnicId="Test">
//					<ConnectedSting>
//						<URL><![CDATA[http://alex3:8088/geoserver/gwc/service/gmaps?layers=russia&zoom=#z&x=#x&y=#y&format=image/png]]></URL>
//						<ID Number="1" Value="RosLandSat"/>
//						<Name Number="1" Value="RosLandSat"/>
//						<Matrix Name="urn:ogc:def:wkss:OGC:1.0:GoogleMapsCompatible"/>
//					</ConnectedSting>
//					<PortalStadndart>UTMS</PortalStadndart>
//					<Alghoritm>20</Alghoritm>
//					<Transparent>0xffffffff</Transparent>
//				</Layer>
//

    
CodeSite.Send(  vTopic.Layer[i].Name );   
    
      oLayer:= AddItem();

      oLayer.Topic_Name  :=vTopic.Name;
      oLayer.Layer_Name  :=vTopic.Layer[i].Name;  
      oLayer.Layer_UnicId:=vTopic.Layer[i].UnicId;  
                                                                               
              
      oLayer.ConnectedSting.URL:= vTopic.Layer[i].ConnectedSting.URL;

//               Assert(oLayer.ConnectedSting.URL<>'');
               
      oLayer.ConnectedSting.Matrix_name   :=vTopic.Layer[i].ConnectedSting.Matrix.Name;
      oLayer.ConnectedSting.Projection_CRS:=vTopic.Layer[i].ConnectedSting.Projection.CRS;

      if (oLayer.ConnectedSting.Projection_CRS='') and
         (oLayer.ConnectedSting.Matrix_name<>'')
      then
        oLayer.ConnectedSting.Projection_CRS:=oTileMatrixes_Identifier_CRS.Values[oLayer.ConnectedSting.Matrix_name];
                
      
    end;
            
  end;

  FreeAndNil(oTileMatrixes_Identifier_CRS);

 // Runtime.XML:=vDoc_new.XML.Text; 

end;

 {
procedure Test;
var
  obj: TWmsFile;
begin
  Application.Initialize;

  obj:=TWmsFile.Create;
  obj.LoadFromFile('W:\WMS\tests\data\wmslist.xml');

 // obj.Exec;
  
end;
}

// ---------------------------------------------------------------
function TWmsFile.AddItem: TWmsLayer_;
// ---------------------------------------------------------------
begin
  Result := TWmsLayer_.Create;
                      
//  Result.Name_Caption:=aName;
//  Result.UnicId_name :=aUnicId;  
//  Result.Topic_Name  :=aTopic;  

  Add (Result);

end;

// ---------------------------------------------------------------
function TWmsFile.Find(aUnicId: string): TWmsLayer_;
// ---------------------------------------------------------------
var
  I: Integer;
begin          
  for I := 0 to Count-1 do
    if Eq(Items[i].Layer_UnicId, aUnicId) then
      Exit(Items[i]);   
    
  Result := nil;
end;

// ---------------------------------------------------------------
function TWmsFile.GetText: string;
// ---------------------------------------------------------------
var
  oSList: TstringList;
  I: Integer;
begin
  oSList:=TstringList.Create;
 // oSList.Sorted:=true;

  for I := 0 to Count-1 do
    oSList.Add(Items[i].GetDisplayName);

  Result:=oSList.Text;
    
  FreeAndNil(oSList); 
end;



function TWmsFile.IndexOf(aUnicId: string): integer;
var
  I: Integer;
begin          
  for I := 0 to Count-1 do
    if Eq(Items[i].Layer_UnicId, aUnicId) then
      Exit(i);   
    
  Result := -1;
end;


function TWmsLayer_.GetDisplayName: string;
begin
  Result:= Topic_Name + ' - ' +  Layer_Name;
end;


function TWmsLayer_.Get_EPSG: Integer;
var
  k: Integer;
begin
  k:=Pos(':', ConnectedSting.Projection_CRS);
  if k>0 then
    Result := StrToIntDef(Copy(ConnectedSting.Projection_CRS, k+1, 100),0);
end;



end. 

  
//
//
//initialization
//  CoInitialize(nil);
//


//begin
  
 // Test
         
{


Procedure LoadStringTable(TableName:String;SL:TStrings);
var
 RS: TResourceStream;
begin
 RS := TResourceStream.Create( HInstance, TableName, RT_RCDATA);  
 try
  SL.LoadFromStream(RS); 
 finally
  RS.Free;
 end;
end;

// Usage :

var  
 String1, String2, String3 : String;
 StrList:TStringList;
Begin
 StrList:=TStringList.Create;
 try
  LoadStringTable('RCData',StrList);
  String1:= StrList[0];
  String2:= StrList[1];
  String3:= StrList[2];
 finally
  StrList.Free;
 end;
end;



// ---------------------------------------------------------------
procedure TWmsFile.LoadPopupMenu(aPopupMenu: TPopupMenu; aOnClick:  TNotifyEvent);
// ---------------------------------------------------------------
var
  I: Integer;
  j: Integer;
  oMenuItem_child, menuItem: TMenuItem;
begin
  aPopupMenu.Items.Clear;
  

  Assert(Topics.Count>0);

  for I := 0 to Topics.Count-1 do
  begin
    menuItem := TMenuItem.Create(aPopupMenu) ;      
    menuItem.Caption := Topics[i];

    aPopupMenu.Items.Add(menuItem) ;
                            

    for j := 0 to Layers.Count-1 do
      if Layers[j].Topic_Name = Topics[i] then
      begin

        oMenuItem_child := TMenuItem.Create(aPopupMenu) ;
        oMenuItem_child.Caption := Layers[j].Name_Caption + '-' + Layers[j].UnicId_name;  
        oMenuItem_child.Tag:=j;

        oMenuItem_child.OnClick:=aOnClick;
        
        menuItem.Add(oMenuItem_child)      
      end;

  end;
    

  
end;




//
//constructor TWmsFile.Create;
//begin
//  inherited Create;
//  
////  Topics := TstringList.Create();
//  Layers := TWmsFile.Create();
//
//end;
//
//
//destructor TWmsFile.Destroy;
//begin
//  FreeAndNil(Layers);
////  FreeAndNil(Topics);
//  inherited Destroy;
//end;
//
//

  //TWmsFile = class;

     {
 //-------------------------------------------------------------------
  TWmsFile = class
  //-------------------------------------------------------------------
  public
    Layers: TWmsFile;

    constructor Create;
    destructor Destroy; override;
              
    procedure LoadFromFile(aFileName: string);
  end;     
   }
