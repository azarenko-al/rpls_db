unit d_WMS_Set_Center;

interface

uses
  System.SysUtils, System.Classes,  Vcl.Controls, Vcl.Forms, RxPlacemnt, Vcl.StdCtrls, Vcl.ExtCtrls,
  MapXLib_TLB;

type
  Tdlg_WMS_set_center = class(TForm)
    FormStorage1: TFormStorage;
    ed_Lat: TLabeledEdit;
    ed_lon: TLabeledEdit;
    ComboBox1: TComboBox;
    CheckBox1: TCheckBox;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    FMapX : CMapX;
 
 
  public
    class procedure ExecDlg(aCMapX : CMapX);
    procedure Search;

  end;

//var
//  dlg_WMS_set_center: Tdlg_WMS_set_center;

implementation

{$R *.dfm}

procedure Tdlg_WMS_set_center.Button1Click(Sender: TObject);
begin
  Search;
end;


class procedure Tdlg_WMS_set_center.ExecDlg(aCMapX : CMapX);
begin
  with Tdlg_WMS_set_center.Create(Application) do
  begin
    FMapX:= aCMapX;
  
//    rg_CoordSystem.Items.IndexOfObject()

    ShowModal;

    Free;
  end;

  // TODO -cMM: Tdlg_WMS_set_center.ExecDlg default body inserted
end;

procedure Tdlg_WMS_set_center.Search;
var
  eLon, eLat: double;
begin
  eLat:= StrToFloatDef(ed_Lat.Text,0) ;
  eLon:= StrToFloatDef(ed_Lon.Text,0) ;
                                          //   43.1132639998, 131.8896810105,


//  ITileManager.Set_Center( 43.1132639998, 131.8896810105, 19 );

  
  FMapX.ZoomTo (FMapX.Zoom, 131.8896810105, 43.1132639998);   // X: Double; Y: Double);
  
//  FMapX.ZoomTo (FMapX.Zoom, eLon, eLat);   // X: Double; Y: Double);


  // TODO -cMM: Tdlg_WMS_set_center.Search default body inserted
end;

end.


{
 FCMapX : CMapX;
