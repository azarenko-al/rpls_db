﻿unit dm_Tile_Loader;

interface

//{.$DEFINE use_debug}

//{$DEFINE grid}

uses
//  u_GDAL_bat,
//  u_GDAL_classes,
  u_run,

  u_func,
  u_web,

  d_WMS_log,

  u_com,

  MapXLib_TLB,

  CodeSiteLogging,

  u_MapX_func,

  u_mapX,

  u_panorama_coord_convert,

  Math,

  u_geo,

  u_files,

  IOUtils,  Vcl.ExtActns,
  Dialogs, JPEG, PNGImage, ExtCtrls, Graphics,  SysUtils, Classes, Forms, Variants,
  StdVcl,  StrUtils, URLMon, ShellApi, System.IniFiles, DateUtils
  ;
           


type
  TXYBounds = record
    x_min: Double;
    x_max: Double;
    y_min: Double;
    y_max: Double;
    
  end;

  TOffserRec= record
    Lat: Double;
    lon: Double;
  end;

  
  
  TTileBounds = packed record
                   block_X_min : longword;
                   block_X_max : longword;

                   block_Y_min : longword;
                   block_Y_max : longword;

                   Max: longword;

                   Z : integer;
                end;



 TLoad_params = record
    Lat: Double;
    Lon: Double;

    Width: Integer;
    Height: Integer;
  end;


           

  TdmTile_Loader = class(TDataModule)
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FMapX: CMapX;

//    FOldData : record    
//      BLRect: TBLRect;
//      LayerName: string;
//    end  ;

//    FGDAL_path: string;
//    FGDAL_bat: TGDAL_bat;
    
    //----------------------------------------------------
    FBlockX_col_count: word;
    FBlockY_row_count: word;

    FTileBounds: TTileBounds;
      
    FTileArray: array of
                array of record
                  block_x,block_y: LongWord ;

                  LeftTop: record
                    X, Y: double;
                  end;
                end; 
  
    //----------------------------------------------------
    
  
    procedure  CheckBlock(var aBlock: integer; aZ: integer);

    procedure GetBlockByXY(aX, aY: double; aZ: integer; var aBlock_X, aBlock_Y:
        integer);
 

    function GetFileDir(aZ: integer): string;
    function GetFileName_TAB(aX, aY, aZ: integer; aVersion: Integer = 0): string;
      
    function GetFileName_bounds(): string;

    function GetTileBounds_view(aLat, aLon: Double; aZ, aWidth, aHeight: Integer):  TXYBounds;

    function GetRangeTileBounds_MinMax(  ): TXYBounds;
  
    function GetTileBounds_mercatorXY(aBlockX, aBlockY, aZ: Integer): TXYBounds;
    function GetZ(aBoundsX: TXYBounds; aWidth: Integer): Integer;

    function Load(aParams: TLoad_params; var aFileName: WideString; var aBounds:  TXYBounds): HResult;

    function LoadByRect(var aFileName: WideString): Boolean;
    function LoadByRect_new (var aFileName: WideString): Boolean;
//    procedure Load_GDAL_Ini;

    procedure Load_Ini;
    
    function Load_HTTP(aFileName: string; aX, aY, aZ: integer): boolean;

    procedure Log(aMsg: string);
//    function RasterGK_Exists(var aZone: Integer): Boolean;

//    procedure SaveTileFile_WGS_EPSG_3395__(aFileName: string; aX, aY, aZ: Integer; aImgFileName: string; aEPSG: Integer);

    procedure SaveTileFile_WGS_EPSG_3395_bounds(aFileName, aImgFileName: string;
        aEPSG: Integer; aBounds: TXYBounds; aImage_width, aImage_height, aZ: Integer);

    function Z_Max: integer;

    //FTileBounds.Z

  public
    procedure Load_Tile(aX,aY,aZ: integer);

  public

    Params: record

              Options: record
                IsShowGrid : boolean;
                IsUseCache : boolean;
              end;

              URL: string;
              Dir: string;

              LayerName: string;

              EPSG: integer;

              Z: integer;
        //      Z_max: integer;

             //out
              Result_Z: integer;
            end;
                 
    
    function Load_CMap(const aMapX: CMapX): HResult;


  end;

  
implementation    
  


{$R *.dfm}

//const
//  DEF_DIR_PREFIX = 'ver3';


const
// epsg 3857
  DEF_MAX_20037508 =  20037508.34279000;
//  DEF_MAX_Z = 19; //18;
//  DEF_MAX_Z = 20; //18;



  {
// ---------------------------------------------------------------
function DownloadFile(aSourceFile_URL, aDestFile: string): Boolean;
// ---------------------------------------------------------------
var
//Vcl.ExtActns
  pdfStreamed: TDownloadUrl;

begin
  pdfStreamed:= TDownLoadURL.Create(nil);

  with pdfStreamed do  begin    URL := aSourceFile_URL;    FileName := aDestFile;    ExecuteTarget(nil);  end;  FreeAndNil(pdfStreamed);  Result:=FileExists(aDestFile);end;


// ---------------------------------------------------------------
function DownloadFile_old(aSourceFile_URL, aDestFile: string): Boolean;
// ---------------------------------------------------------------
var
//Vcl.ExtActns
  pdfStreamed: TDownloadUrl;

begin
 pdfStreamed:= TDownLoadURL.Create(nil);

  try
    Result := UrlDownloadToFile(nil, PChar(aSourceFile_URL), PChar(aDestFile), 0, nil) = 0;
  except
    Result := False;
  end;
end;
 }

  
// ---------------------------------------------------------------
procedure TdmTile_Loader.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
var
  r: TXYBounds;
  
begin
//  r:=GetTileBounds_mercatorXY (0, 0, 0);
//  r:=GetTileBounds_mercatorXY (0, 0, 1);
         
//  Params.Options.IsShowGrid:=True;
  Params.Options.IsUseCache:=False;
  
//  Params.Options.IsUseCache:=True;
 

//  Params.LayerName:='YandexMap';
  Params.Dir:= GetTempFileDir() ;
               
  Load_Ini;     
  
//  Load_GDAL_Ini;

//  FGDAL_bat:=TGDAL_bat.Create;

end;


procedure TdmTile_Loader.DataModuleDestroy(Sender: TObject);
begin
//  FreeAndNil(FGDAL_bat);
  
end;


// ---------------------------------------------------------------
procedure TdmTile_Loader.Load_Ini;
// ---------------------------------------------------------------
var
  ini: TIniFile;
  s,sFile: string;
  sModule: string;

begin

  sFile :=System.SysUtils.GetModuleName(HInstance);
  

  s:=ExtractFileExt(sFile);
 
  
  sModule:=GetModuleFileName();

  s:=ExtractFileExt(sModule);
  
  if s='.exe' then
    sFile:= 'C:\ONEGA\RPLS_DB\bin\WMS\wms.ini' 
  else
  
//  if GetModuleFileName()<>'' then
    sFile:= ChangeFileExt( GetModuleFileName(), '.ini');
 // else
  //  sFile:= 'C:\ONEGA\RPLS_DB\bin\WMS\wms.ini'; 

//   sFile:= ChangeFileExt( GetModuleFileName(), '.ini');
   
  Log(sFile);


 // Exit;
   
//  sFile:=''

  if FileExists(sFile) then
  begin
    ini:=TIniFile.Create(sFile);
    s:=ini.ReadString('main','dir','');

    if s<>'' then
       Params.Dir:= IncludeTrailingBackslash(s);

//    s:=ini.ReadString('main','gdal','');
//
//    if s<>'' then
//       Params.Dir:= IncludeTrailingBackslash(s);



    FreeAndNil(ini);
  end;      


//  FreeAndNil(oSList);
  
  //135
  
end;



// ---------------------------------------------------------------
function TdmTile_Loader.GetFileDir(aZ: integer): string;
// ---------------------------------------------------------------
begin
  Assert(Params.Dir<>'', 'Params.Dir');
  Assert(Params.LayerName<>'', 'Params.LayerName');  
         
 
  Result:=IncludeTrailingBackslash(Trim(Params.Dir)) +  Format('%s\z%d\',[ Params.LayerName,   aZ]);  
 
end;

// ---------------------------------------------------------------
function TdmTile_Loader.GetFileName_TAB(aX, aY, aZ: integer; aVersion: Integer   = 0): string;
// ---------------------------------------------------------------
begin
  Result:=GetFileDir(aZ) + Format('%d\x%d\0\',[aVersion,aX]) + Format('y%d.tab',[aY]);  
  
end;   

// ---------------------------------------------------------------
function TdmTile_Loader.GetFileName_bounds(): string;
// ---------------------------------------------------------------
var
  iZone: Integer;
begin
  with FTileBounds do 
    Result:=GetFileDir(Z) +  Format('wms_tile_z%d_x%d_%d_y%d_%d.tab',
       [Z, 
        block_X_min, block_X_max, 
        block_Y_min, block_Y_max]);

{
  if RasterGK_Exists(iZone) then
  begin
    Result:= ChangeFileExt(Result, Format('_gk_%d.tab',[iZone]));

    Log('TdmTile_Loader.GetFileName_bounds - '+Result);

  end;
 }
  //  IsUseCache         
end;   



// ---------------------------------------------------------------
procedure TdmTile_Loader.GetBlockByXY(aX, aY: double; aZ: integer; var aBlock_X, aBlock_Y: integer);
// ---------------------------------------------------------------
var
  eStep: Double;
  iMax: longint;

 // r: TXYBounds;

begin
  iMax:= Trunc(Math.Power(2,aZ));

  eStep:=(2*DEF_MAX_20037508)/iMax;

  
  aBlock_Y:= Trunc( (DEF_MAX_20037508-aX) / eStep) ;
  aBlock_X:= Trunc( (aY - (- DEF_MAX_20037508)) / eStep) ;

  if aZ<3 then  
  begin
    if aBlock_X>=iMax then aBlock_X:=iMax-1;
    if aBlock_X<0     then aBlock_X:=0;
  end;  
  
  if aBlock_Y>=iMax then aBlock_Y:=iMax-1;
  if aBlock_Y<0     then aBlock_Y:=0;
                          
end;  



// ---------------------------------------------------------------
function TdmTile_Loader.GetTileBounds_view(aLat, aLon: Double; aZ, aWidth, aHeight: Integer): TXYBounds;
// ---------------------------------------------------------------
var
  wgs_Lat, wgs_Lon: Double;
  wgs_Lat1, wgs_Lon1: Double;

  iBlock_Y1: Integer;
  iBlock_Y2: Integer;
  iBlock_X1: Integer;
  iBlock_X2: Integer;
  e1: Double;
  eLat1: Double;
  eLat2: Double;
  eLon1: Double;
  eLon2: Double;
  eStep: Double;
  eScale: double;
  
  iMax: longint;
  iX: word;
  iY: word;
  X, Y: double;            
   
  r: TXYBounds;
  
begin
  Log('TdmTile_Loader.GetTileBounds_view');


  FillChar(FTileBounds,SizeOf(FTileBounds),0);
  FillChar(Result,SizeOf(Result),0);          
  
  iMax:= Trunc(Math.Power(2,aZ));
  Assert(iMax>0);
  
  eStep:=(2*DEF_MAX_20037508)/iMax;

  eScale:=eStep / 256;           
  Assert(eScale>0);
    
  //-----------------------
  // центральная точка   
  //-----------------------
  Geo_EPSG_To_EPSG_plane(DEF_WGS_EPSG_4326, Params.EPSG, aLat, aLon, X, Y);

  
  Log ( Format('центральная точка - Geo_EPSG_To_EPSG_plane(DEF_WGS_EPSG_4326 - x:%1.5f, y:%1.5f',[X, Y]));
                     
  r.X_Min:= x - (aHeight/2) * eScale;  
  r.X_Max:= x + (aHeight/2) * eScale;

  r.Y_Min:= y - (aWidth/2) * eScale;  
  r.Y_Max:= y + (aWidth/2) * eScale;
                                    
                            
  //-------------------------------------
  // 3395 -> 4326
  //-------------------------------------
  Plane_EPSG_To_EPSG_geo(Params.EPSG, DEF_WGS_EPSG_4326, r.X_Max, r.Y_Min, eLat1, eLon1);
  Plane_EPSG_To_EPSG_geo(Params.EPSG, DEF_WGS_EPSG_4326, r.X_Min, r.Y_Max, eLat2, eLon2);

 

  Result.X_Max:=eLat1;
  Result.Y_Min:=eLon1;  

  Result.X_Min:=eLat2;
  Result.Y_Max:=eLon2;  

  // ---------------------------------------------------------------
  // top left
  GetBlockByXY (r.X_Max, r.Y_Min,  aZ, iBlock_X1, iBlock_Y1) ;
  //bottom right
  GetBlockByXY (r.X_Min, r.Y_Max,  aZ, iBlock_X2, iBlock_Y2) ;
           
  // ---------------------------------------------------------------

  FTileBounds.block_X_min:=iBlock_X1;
  FTileBounds.block_Y_min:=iBlock_Y1;

  FTileBounds.block_X_max:=iBlock_X2;// mod iMax;
  FTileBounds.block_Y_max:=iBlock_Y2;// mod iMax;

  FTileBounds.Max:=iMax;
  FTileBounds.Z:=aZ;

  Log ( Format('FTileBounds.block_X_min %d',[FTileBounds.block_X_min]));
  Log ( Format('FTileBounds.block_Y_max %d',[FTileBounds.block_Y_max]));

  Log ( Format('FTileBounds.block_Y_min %d',[FTileBounds.block_Y_min]));
  Log ( Format('FTileBounds.block_X_max %d',[FTileBounds.block_X_max]));
  
  Log ( Format('FTileBounds.Z %d',          [FTileBounds.Z]));

  
  
  // ---------------------------------------------------------------
  FBlockY_row_count:=iBlock_Y2-iBlock_Y1+1;  

  FBlockX_col_count:=iBlock_X2-iBlock_X1+1;
      

  Log (Format('FBlockY_row_count %d', [FBlockY_row_count]));
  Log (Format('FBlockX_col_count %d', [FBlockX_col_count]));
   
  SetLength (FTileArray, FBlockY_row_count, FBlockX_col_count); 

  for iY := 0 to FBlockY_row_count-1 do
    for iX := 0 to FBlockX_col_count-1 do
    begin
    
      FTileArray [iY,iX].block_x:=(iBlock_X1 + iX) mod iMax;
      FTileArray [iY,iX].block_y:=(iBlock_Y1 + iY) mod iMax;


      r:=GetTileBounds_mercatorXY (FTileArray [iY,iX].block_x, FTileArray [iY,iX].block_y, aZ);
      FTileArray [iY,iX].LeftTop.X:=r.x_max;
      FTileArray [iY,iX].LeftTop.y:=r.y_min;           
    end;
    
  
end;  

// ---------------------------------------------------------------
function TdmTile_Loader.GetZ(aBoundsX: TXYBounds; aWidth: Integer): Integer;
// ---------------------------------------------------------------
var
  eStep: Double;

  iMax: longint;
  iZ: Integer;
   
  r: TXYBounds;

  e: Double;
  eCols: double;
  
begin     
   if aBoundsX.x_max - aBoundsX.y_min = 0 then
   begin
//     Result:=1;
     Exit(1);
   end;

  
  Geo_EPSG_To_EPSG_plane(DEF_WGS_EPSG_4326, Params.EPSG, aBoundsX.x_max, aBoundsX.y_min,   r.x_max, r.y_min);
  Geo_EPSG_To_EPSG_plane(DEF_WGS_EPSG_4326, Params.EPSG, aBoundsX.x_min, aBoundsX.y_max,   r.x_min, r.y_max);


  eCols:=0;

  for iZ := 0 to  Z_Max() do
//  for iZ := 0 to  DEF_MAX_Z do
  begin
    iMax:= Trunc(Math.Power(2,iZ));  
    eStep:=(2*DEF_MAX_20037508)/iMax;    
    
    eCols:= (r.y_max - r.y_min)/eStep;

    e:=eCols*225;
    
    if e > aWidth then
      Exit (iZ-1);
 
  end;

  Result:=Z_Max();
//  Result:=DEF_MAX_Z;

end;



// ---------------------------------------------------------------
function TdmTile_Loader.GetTileBounds_mercatorXY(aBlockX, aBlockY, aZ:  Integer): TXYBounds;
// ---------------------------------------------------------------
var
  eStep: Double;
  iMax: Longint ;
  
begin      
  
  iMax:= Trunc(Math.Power(2,aZ));

    
  eStep:=(2*DEF_MAX_20037508)/iMax;
  //= 256

  Result.X_Max:=  DEF_MAX_20037508 - eStep * aBlockY ;
  Result.X_Min:=  DEF_MAX_20037508 - eStep *(aBlockY+1) ;
  if Abs(Result.X_Min) < 1 then
    Result.X_Min:=0;
                         
  
  Result.Y_Min:= -DEF_MAX_20037508 + eStep * aBlockX ;
  Result.Y_Max:= -DEF_MAX_20037508 + eStep *(aBlockX+1) ;

  if Abs(Result.Y_Min) < 0.001 then
    Result.Y_Min:=0;

  if Abs(Result.Y_Max) < 0.001 then
    Result.Y_Max:=0;

  
end;

// ---------------------------------------------------------------
function TdmTile_Loader.GetRangeTileBounds_MinMax(): TXYBounds;
// ---------------------------------------------------------------
var       
  oBounds_min : TXYBounds;  
  oBounds_max : TXYBounds;
begin
  with FTileBounds do 
  begin
    oBounds_min:=GetTileBounds_mercatorXY(block_X_min, block_Y_min, Z);
    oBounds_max:=GetTileBounds_mercatorXY(block_X_max, block_Y_max, Z);
  
    Result.Y_Min:=oBounds_min.y_min;
    Result.Y_Max:=oBounds_max.y_max;  

    Result.X_Min:=oBounds_max.x_min;
    Result.X_Max:=oBounds_min.x_max;  
        
  end;

end;       

// ---------------------------------------------------------------
procedure TdmTile_Loader.Load_Tile(aX,aY,aZ: integer);
// ---------------------------------------------------------------
var
  sFile: string;    
begin
  Log(Format('TdmTile_Loader.Load_Tile - x:%d, y:%d, z:%d', [aX,aY,aZ]));


  sFile:=GetFileName_TAB(ax,ay,az, 0);
  if not FileExists(sFile) then
    sFile:=GetFileName_TAB(ax,ay,az, 1);

  
 if not FileExists(sFile) then     
     Load_HTTP(sFile,ax,ay,az)
    
end;

// ---------------------------------------------------------------
function TdmTile_Loader.LoadByRect(var aFileName: WideString): Boolean;
// ---------------------------------------------------------------
var
  //sFile: string;
 
  x: Integer;
  y: Integer;

//  dtStart: TDateTime;
  iCol: Integer;
  iRow: Integer;

begin   
  Result:=False;
    

  aFileName:=GetFileName_bounds ();

//  CodeSite.Send('5');
//  CodeSite.Send(aFileName);
  
  if Params.Options.IsUseCache then  
    if FileExists(aFileName) then
      Exit(True);
      

  Log('LoadByRect begin: ' + DateTimeToStr(Now()));


  for iRow := 0 to FBlockY_row_count-1 do
  for iCol := 0 to FBlockX_col_count-1 do
    with FTileArray[iRow,iCol] do
    begin
      Load_Tile (block_x, block_y, FTileBounds.z);
    end;
                      

//  FTestWorkspace.SaveToFile(GetFileDir(aRect.Z) + 'workspace.wor');


  //SecondsBetwe
 

  Log('LoadByRect Done: ' + DateTimeToStr(Now()));
          

 // ShowMessage('LoadByRect Done');
      

  //aRect.Z   //aFileName: WideString; aZ: Integer
                       
       
  result:=LoadByRect_new( aFileName);  //aRect,

        
 // end;

//  s:=IdHTTP1.ResponseText;
  
end;

// ---------------------------------------------------------------
function TdmTile_Loader.LoadByRect_new( var aFileName: WideString): Boolean;
// ---------------------------------------------------------------
var
  iH: Integer;
  iW: Integer;
  iX: Integer;
  iY: Integer;
 
  s: string;
  sDir: string;
  sFile: string;

  oJPEGImage: TJPEGImage;
  oPNGGImage: TPngObject;

  oImage: TBitmap;
  oImage_file: TImage;
  sImageFile: string;
  sImageFile_jpg: string;
  sImageFile_png: string;

  boundsX: TXYBounds;
  k: Integer;
 
  sFile_bmp: string; 
  sFile_png: string;

begin      

//  CodeSite.Send('TdmTile_Loader.LoadByRect_new');

  Log('make image...');
  SaveTime;
  

  result:=False;


  oImage:=TBitmap.Create();
  oImage.PixelFormat  :=pf24bit;

  oImage_file:=TImage.Create(nil);


  iH:=FBlockY_row_count * 256;
  iW:=FBlockX_col_count * 256;


  try
    oImage.Width :=iW;
    oImage.Height:=iH;

  except
    s:=Format('Width %d  Height %d', [iW, iH]);

    ShowMessage(s);

    FreeAndNil(oImage);
    FreeAndNil(oImage_file);

    exit;
  end;    // try/finally


  for iY := 0 to FBlockY_row_count-1 do
  for iX := 0 to FBlockX_col_count-1 do
    with FTileArray [iY,iX] do
    begin   

      sFile:=GetFileName_TAB(block_x, block_y, FTileBounds.Z, 0);
      if not FileExists(sFile) then
        sFile:=GetFileName_TAB(block_x, block_y, FTileBounds.Z, 1);
                 
    
      assert (sFile<>'');
    
                  
      sImageFile_jpg:= ChangeFileExt(sFile,'.jpg');        
      sImageFile_png:= ChangeFileExt(sFile,'.png');      

   //   sExt:=

      //---------------------------------------------
      if FileExists(sImageFile_jpg) then
      //---------------------------------------------
      begin
        oJPEGImage:=TJPEGImage.Create();  
        oJPEGImage.LoadFromFile(sImageFile_jpg);

        oImage_file.Picture.LoadFromFile(sImageFile_jpg);

        FreeAndNil(oJPEGImage);  
        
      end
      
      else
      //---------------------------------------------
      if FileExists(sImageFile_png) then
      //---------------------------------------------
        oImage_file.Picture.LoadFromFile(sImageFile_png)
      else
      
      begin
        Log('ERROR: file not found - '+ sFile);
        
        Continue;
      end;
      

      oImage.Canvas.Draw(256*iX, 256*iY, oImage_file.Picture.Graphic);
    

    end;

 // end;

  // ---------------------------------------------------------------
  // if Params.Options.IsShowGrid then 
  // ---------------------------------------------------------------
  if Params.Options.IsShowGrid then 
  begin     
    oImage.Canvas.Pen.Color:=clWhite;
    oImage.Canvas.Pen.Width:=1;  
                    

    for iX := 0 to FBlockX_col_count-1 do     
    begin   
      oImage.Canvas.MoveTo(iX*256, 0);
      oImage.Canvas.LineTo(iX*256, oImage.Height);          
    end;       

  
    for iY := 0 to FBlockY_row_count-1 do
    begin             
      oImage.Canvas.MoveTo(0, iY*256);
      oImage.Canvas.LineTo(oImage.Width, iY*256);            
    end;

    
    oImage.Canvas.MoveTo(0,0);
    oImage.Canvas.LineTo(0,oImage.Height-1);
    oImage.Canvas.LineTo(oImage.Width,oImage.Height-1);
    oImage.Canvas.LineTo(oImage.Width-1,0);  
    
  end;  
  // ---------------------------------------------------------------
 


  boundsX:=GetRangeTileBounds_MinMax();

  
  sFile:=GetFileName_bounds(); // (aRect);
                                 

  sFile_bmp:= ChangeFileExt(sFile,'.bmp');
  oImage.SaveToFile(sFile_bmp);

  sFile_png:= ChangeFileExt(sFile,'.png');


  oPNGGImage:=TPngObject.Create();
  oPNGGImage.Assign(oImage);


  oPNGGImage.SaveToFile(sFile_png);
  FreeAndNil(oPNGGImage);
                        

  SaveTileFile_WGS_EPSG_3395_bounds(sFile, sFile_png, Params.EPSG,  boundsX, iW, iH, FTileBounds.Z);
                     
  aFileName:=sFile;


  FreeAndNil(oImage);
  FreeAndNil(oImage_file);


  SysUtils.DeleteFile(sFile_bmp);

  
  result:=True;

  Log('make image done - '+ GetTimeDiff());
          
end;


// ---------------------------------------------------------------
function TdmTile_Loader.Load_CMap(const aMapX: CMapX): HResult;
// ---------------------------------------------------------------
var
  k: Integer;

  blRect: TBLRect;
  e: Double;

  iResult: HResult;               

  sFileName: WideString; 

  r: TLoad_params;

  rBounds: TXYBounds;
  s: string;
              

  v:CMapXLayer;

  rTileBounds: TTileBounds;
  
label
  label_exit  ;
  
//const
//  def_offset = 0;  // 0.00001;
    
begin
  FMapX:=aMapX;

//  Load_Ini;

//  k:=aMapX.NumericCoordSys.Datum.Ellipsoid;
                                                    
 if aMapX.NumericCoordSys.Datum.Ellipsoid<>28 then
 begin   
   aMapX.NumericCoordSys.Set_ (miLongLat, DATUM_WGS84,   //28;
        EmptyParam, EmptyParam, EmptyParam, EmptyParam,
        EmptyParam, EmptyParam, EmptyParam, EmptyParam,
        EmptyParam, EmptyParam, EmptyParam, EmptyParam);

   Log('aMapX.NumericCoordSys.Set_ (miLongLat, DATUM_WGS84,');
 end;


  Assert(Params.Dir>'');
  Assert(Params.LayerName>'', 'Params.LayerName = ...');
     

  CursorHourGlass;

//  aMapX. OnMapViewChanged;
  
//  aMapX.OnMapViewChanged:=nil; // procedure Tfrm_Map.Map1MapViewChanged(Sender: TObject);

                
  FillChar(r,SizeOf(r),0);

Log('aMapX.CenterY : ' + FloatToStr(aMapX.CenterY));
Log('aMapX.CenterX : ' + FloatToStr(aMapX.CenterX));
Log('aMapX.Zoom :  '   + FloatToStr(aMapX.Zoom));

//  if aMapX.CenterY < -1 then

  if aMapX.CenterY < -90 then     
    aMapX.CenterY := -90;      
          
//    aMapX.ZoomTo (3000, 57, 59);
  
      
  r.Lat:=aMapX.CenterY;
  r.Lon:=aMapX.CenterX; 
  
                           
  
  r.Width :=Round(aMapX.MapScreenWidth);
  r.Height:=Round(aMapX.MapScreenHeight);  


  blRect:=mapx_XRectangleToBLRect(aMapX.Bounds);


//  
//  if not geo_CompareBLRects(blRect, FOldData.BLRect) or (Params.LayerName<>FOldData.LayerName)
//  then begin
// // ShowMessage('if not geo_CompareBLRects(blRect, FOldData.BLRect) or (Params.LayerName<>FOldData.LayerName)');
//
//    FOldData.BLRect:=blRect;
//    FOldData.LayerName:=Params.LayerName;  
//  end else
//    exit;

  
//  blRect:=mapx_GetBounds(Map1);

  FillChar(rBounds,SizeOf(rBounds),0);

  if Abs(blRect.TopLeft.b - blRect.BottomRight.b) > 0 then
  begin     
    rBounds.x_max:=blRect.TopLeft.B;// + def_offset;
    rBounds.y_min:=blRect.TopLeft.L;// - def_offset;
    rBounds.x_min:=blRect.BottomRight.B;// - def_offset;
    rBounds.y_max:=blRect.BottomRight.L;// + def_offset;

  end;
        
      
  iResult:=Load(r, sFileName, rBounds);

//CodeSite.Send(sFileName);
  
  
  Assert(sFileName<>'', 'sFileName = '+ sFileName);

  if mapx_FileExists(aMapX, sFileName) then
    goto label_exit;

//  if sFileName then
                           
  blRect.TopLeft.B:=rBounds.x_max;
  blRect.TopLeft.L:=rBounds.y_min;  
  blRect.BottomRight.B:=rBounds.x_min;
  blRect.BottomRight.L:=rBounds.y_max;  
                                                  

  //!!!!!!!!!!!
  aMapX.AutoRedraw :=False; 

  
                                            
  mapx_TilesClear (aMapX);

  aMapX.Layers.Add (sFileName, aMapX.Layers.Count+1) ;

  
//--  map_MapAdd_Ex_CMapX(aMapX, sFileName);




  mapx_SetBounds_CMapX(aMapX, blRect);
         
         
  
  //!!!!!
  aMapX.AutoRedraw :=True;    
 


         
label_exit:


//  aMapX.         

//  aMapX.OnMapViewChanged:=FOnMapViewChanged; // procedure Tfrm_Map.Map1MapViewChanged(Sender: TObject);
           
   
  CursorDefault;
 
end;

// ---------------------------------------------------------------
function TdmTile_Loader.Load(aParams: TLoad_params; var aFileName: WideString;  var aBounds: TXYBounds): HResult;
// ---------------------------------------------------------------
var
  iZ: Integer;

begin    
  Log('TdmTile_Loader.Load');
            
  if Params.Z>0 then
    iZ:=Params.Z
  else  
    iZ:=GetZ (aBounds,aParams.Width);
                            
  Params.Result_Z:= iZ;      

  Assert(aParams.Width>0);
  Assert(aParams.Height>0);  

 
  aBounds:=GetTileBounds_view(aParams.Lat, aParams.Lon, iZ, aParams.Width, aParams.Height);
                          
  if LoadByRect( aFileName) then
    Result:= S_OK
  else
    Result:= S_FALSE;
                  
end;

// ---------------------------------------------------------------
procedure TdmTile_Loader.CheckBlock(var aBlock: integer; aZ: integer);
// ---------------------------------------------------------------    
var
  iMax: longint;
begin
  iMax:= Trunc(Math.Power(2, aZ))-1;
 
  if aBlock<0 then
    aBlock:=0 else

  if aBlock>=iMax then 
    aBlock:=iMax; 
end;


procedure TdmTile_Loader.Log(aMsg: string);
begin
  Tdlg_WMS_log.Add(aMsg);

  CodeSite.Send(aMsg);

end;

{
// ---------------------------------------------------------------
function TdmTile_Loader.RasterGK_Exists(var aZone: Integer): Boolean;
// ---------------------------------------------------------------
var
  e: Double;
  I: Integer;

  blRect_main,
  blRect: TBLRect;
  
begin
//  Exit(False);
  

  blRect_main:=mapx_XRectangleToBLRect(FMapX.Bounds);

//  if Params.Z < 15 then
//    Exit(False);

  for I := 1 to FMapX.Layers.Count do
    if
       (FMapX.Layers[i].type_ = miLayerTypeRaster) and 
       (FMapX.Layers[i].Visible) and 
       (FMapX.Layers[i].CoordSys.Datum.Ellipsoid=3) and //3,8       //28,10
       (FMapX.Layers[i].CoordSys.type_=8) then 
    begin
      log ('RasterGK_Exists '+FMapX.Layers[i].FileSpec);
    
      if LeftStr(ExtractFileName (FMapX.Layers[i].FileSpec),4) = 'wms_' then 
        Continue;
                   

      blRect:=mapx_XRectangleToBLRect(FMapX.Layers[i].Bounds);
                            
CodeSite.Send('function TdmTile_Loader.RasterGK_Exists(var aZone: Integer): Boolean; -- ' +blRect.ToString);

      if Abs(blRect.TopLeft.L - blRect.BottomRight.L) > 1 then 
        Continue;
      
      if not geo_IsBLRects_Crossed (blRect, blRect_main) then 
        Continue;

      
      e    :=FMapX.Layers[i].CoordSys.FalseEasting;
      aZone:=Trunc(e / 1000000 );
      exit (true);
    end;  

  result:=False;
    
end;
  }
 
// ---------------------------------------------------------------
procedure TdmTile_Loader.SaveTileFile_WGS_EPSG_3395_bounds(aFileName,
    aImgFileName: string; aEPSG: Integer; aBounds: TXYBounds; aImage_width,
    aImage_height, aZ: Integer);
// ---------------------------------------------------------------
const
  DEF_FILE_meter_10_104 =
  '!table'+ CRLF +
  '!version 300'+ CRLF +
  '!charset WindowsCyrillic'+ CRLF +
  ''+ CRLF +
  'Definition Table'+  CRLF +
  '  File ":file"'+ CRLF +
  '  Type "RASTER"'+  CRLF +

  //left top
  '  (:point3_lon,:point3_lat) (0,0) Label "point 1",'+ CRLF +
  //right bottom
  '  (:point1_lon,:point1_lat) (:img_width,:img_height) Label "point 2",'+ CRLF +
  //left bottom
  '  (:point2_lon,:point2_lat) (0,:img_height) Label "point 3",'+CRLF +
  //rigth top
  '  (:point4_lon,:point4_lat) (:img_width,0) Label "point 4"'+ CRLF +


  '  CoordSys Earth Projection 10, 104, "m", 0'+ CRLF +
  '  Units "m"'+ CRLF +
//  '  Units "degree"'+ CRLF +
  '  RasterStyle 4 1'+ CRLF +
  '  RasterStyle 7 0'; //+ CRLF +

var
  s: string;

  points: array[0..3] of TLatLon;

  I: Integer;
//  iZone_gk: Integer;

  sFile: string;

//  rGDAL_json: TGDAL_json_rec;
//  sFile_bat: string;
//  sJsonFile: string;

const
  DEF_EPSG_3395 = 3395;


begin
  
  aBounds.X_Max:=aBounds.X_Max ;
  aBounds.X_Min:=aBounds.X_Min ;
  
  aBounds.Y_Min:=aBounds.Y_Min ;
  aBounds.Y_Max:=aBounds.Y_Max ;
  

  Assert(aEPSG>0);
            
//
  //right bottom
  Plane_EPSG_To_EPSG_plane(aEPSG, DEF_EPSG_3395, aBounds.X_Max, aBounds.Y_Min, points[2].Lat  , points[2].Lon  );    
  //left top
  Plane_EPSG_To_EPSG_plane(aEPSG, DEF_EPSG_3395, aBounds.X_Min, aBounds.Y_Max, points[0].Lat  , points[0].Lon  );
  Plane_EPSG_To_EPSG_plane(aEPSG, DEF_EPSG_3395, aBounds.X_Min, aBounds.Y_Min, points[1].Lat  , points[1].Lon  );
  Plane_EPSG_To_EPSG_plane(aEPSG, DEF_EPSG_3395, aBounds.X_Max, aBounds.Y_Max, points[3].Lat  , points[3].Lon  );
  

  

// ---------------------------------------------------------------    
// meter
// ---------------------------------------------------------------    

  s:=DEF_FILE_meter_10_104;

  sFile:=ExtractFileName(aImgFileName);
  
  s:=ReplaceStr(s, ':file', sFile);
  
  s:=ReplaceStr(s, ':img_width',  IntToStr(aImage_width));
  s:=ReplaceStr(s, ':img_height', IntToStr(aImage_height));  

  for I := 0 to 3 do
  begin
    s:=ReplaceStr(s, Format(':point%d_lon',[i+1]), FloatToStr(Trunc(points[i].Lon)));
    s:=ReplaceStr(s, Format(':point%d_lat',[i+1]), FloatToStr(Trunc(points[i].Lat )));
  end;

 // StrToFile_ (aFileName, s );

  IOUtils.TFile.WriteAllText(aFileName, s);


  {
  if not RasterGK_Exists (iZone_gk) then
    Exit;  
    
  ////////////////////////
  ////////////////////////

  Log (s);
    

  s:=ExtractFileName(aImgFileName);  
  Log(s);
  
  sFile:=ChangeFileExt(ExtractFileName(aImgFileName),'');
  Log(sFile);

  sFile_bat:=ChangeFileExt(aFileName,'.bat');
  Log(sFile_bat);

  Assert(FGDAL_path<>'');
  
s:= 'set      path='+FGDAL_path+'bin\;'         + LF +
    'set GDAL_DATA='+FGDAL_path+'share\epsg_csv'+ LF +

  
//s:= 'set      path=C:\ONEGA\RPLS_DB\bin\OSGeo4W\bin\;'         + LF +
//    'set GDAL_DATA=C:\ONEGA\RPLS_DB\bin\OSGeo4W\share\epsg_csv'+ LF +

  
    Format('gdal_translate.exe -a_srs EPSG:3395 -of GTiff  -a_ullr %1.2f  %1.2f %1.2f %1.2f  %s %s.tif ', [  
                              aBounds.Y_Min, //xyRect_UTM.TopLeft.Y,
                              aBounds.X_Max, //xyRect_UTM.TopLeft.X,

                              aBounds.Y_Max, //xyRect_UTM.BottomRight.Y,
                              aBounds.X_Min, //xyRect_UTM.BottomRight.X,
                              ExtractFileName(aImgFileName),
                              sFile
                             ]) + LF +
                             
    Format('gdalinfo -json   "%s.tif" >  "%s.tif.json"',       [sFile, sFile]) + LF + LF +
    
    //iZone_gk
//    Format('gdalwarp -t_srs EPSG:28407 "%s.tif"  "%s.gk.tif" ',[sFile, sFile]) + LF +
    Format('gdalwarp -t_srs EPSG:%d "%s.tif"  "%s.gk.tif" ',[28400+iZone_gk, sFile, sFile]) + LF +
    Format('gdalinfo -json   "%s.gk.tif" >  "%s.gk.tif.json"', [sFile, sFile]);
                                                            
                              
  IOUtils.TFile.WriteAllText(sFile_bat, s);                              

                          
  SetCurrentDir(ExtractFilePath(aFileName));
  
  RunApp_hidden(sFile_bat,'');
  
  sJsonFile:=ChangeFileExt(aFileName, '.gk.tif.json');
  Log(sJsonFile);
  
  if rGDAL_json.LoadFromFile (sJsonFile) then 
  begin
    rGDAL_json.Zone_UTM:=iZone_gk; //FRelMatrix.ZoneNum_GK;
    rGDAL_json.SaveToFile_TAB_Pulkovo (
                                        aFileName  ,
                                     //   ChangeFileExt(aFileName, '.gk.tab'),
                                        ChangeFileExt(aFileName, '.gk.tif') ); 
                                             
  end;
    
    
   }
  
end;

// ---------------------------------------------------------------
function TdmTile_Loader.Load_HTTP(aFileName: string; aX, aY, aZ: integer):
    boolean;
// ---------------------------------------------------------------

    // ---------------------------------------------------------------
    function DoGetImageFormat(aFileName: string): string;
    // ---------------------------------------------------------------
    var
      oFileStream: TFileStream;
      buf: array[0..3] of AnsiChar ;
      iRead: Integer;
      k: Integer;

      sComp: AnsiString;
    begin

      oFileStream:=TFileStream.Create (aFileName, fmShareDenyNone);
      oFileStream.Position:=6;
      iRead:=oFileStream.Read (buf, SizeOf(buf));

      SetString(sComp, PAnsiChar(@buf[0]), Length(buf));

      
      if sComp= 'JFIF' then 
        result:='.jpg'
      else
        result:='.png';      
                   
      
      
      FreeAndNil(oFileStream);
    end;


var
  b: Boolean;
  iSize: Int64;
  iSize1: Int64;
  k: Integer;
  oFileStream: TFileStream;
  s: string;
  sContentType: string;
  sExt: string;
  sFile_bin: string;
  sFile_img: string;
  sURL: string;
begin
//  CodeSite.Send('TTileManagerX.Load_HTTP ...');


  Assert(Params.URL<>'');
  Assert(aZ<30);


  if FileExists(ChangeFileExt(aFileName,'.png')) or 
     FileExists(ChangeFileExt(aFileName,'.jpg'))
  then
  begin
//    Result:=True;
    Exit(True);
  end;
  
   


  s:=ExtractFileDir(aFileName);
  ForceDirectories(s);
  
  
  if Pos('#z',Params.URL)>0 then
  begin
    sURL:= Params.URL;

    sURL:= ReplaceStr (sURL, '#z', IntToStr(aZ) );
    sURL:= ReplaceStr (sURL, '#y', IntToStr(aY) );    
    sURL:= ReplaceStr (sURL, '#x', IntToStr(aX) );    

//    if RightStr(sURL,4)='.png' then
//      sFile_img:= ChangeFileExt(aFileName,'.png'); 

  end else
    sURL:=Params.URL + Format('&y=%d&x=%d&z=%d',[ay,ax,az]) ;//+ '&format=image/png';
//    sURL:=Params.URL + Format('&y=%d&x=%d&z=%d',[ay,ax,az]);


// sURL:=sURL + '&format=image/png';

  Log ('Loading... ' + sURL)   ;

//   function IsStrInStrArray(aStr: string; aStrArray: array of string): boolean;
  
  s:=Params.LayerName;

  
//  sFile_img:=ChangeFileExt(aFileName, sExt);
  sFile_bin:=ChangeFileExt(aFileName, '.bin');
  
  
Log('Load: ' + sURL );

  if Pos('https',sURL)>0 then
    b:=DownloadFile_HTTPS (sURL, sFile_bin)
  else
    b:=DownloadFile (sURL, sFile_bin);

//  b:=DownloadFile (sURL, sFile_bin);
  if not b then
  begin
Log('error: ' + sURL );
    Exit;
  end;

  sExt:=DoGetImageFormat (sFile_bin);
  sFile_img:=ChangeFileExt(aFileName, sExt);
  
  TFile.Move(sFile_bin, sFile_img);
                               
                                
    //content-type:image/png
    
  Result:=True;
    

  
  if not Result then
  begin  
//    ShowMessage(sFile_img + ' iSize=0  ');
  
//    Log (sFile_img + ' iSize=0  ');
//    SysUtils.DeleteFile(sFile_img);
//    SysUtils.DeleteFile(sFile_bin);    
    
  end ;
//    TFile.FileS


  Log('TTileManagerX.Load_HTTP ... Done');

end;


function TdmTile_Loader.Z_Max: integer;
begin
  if Eq(Params.LayerName,'OpenStreetMap') or Eq(Params.LayerName,'2gis')  then
    Result := 18
  else
    Result := 20;
end;


  
end.


{
 // ---------------------------------------------------------------
procedure TdmTile_Loader.Load_GDAL_Ini;
// ---------------------------------------------------------------
var
  ini: TIniFile;
  s,sFile: string;
  sModule: string;

begin
  sFile :=System.SysUtils.GetModuleName(HInstance);

  s:=ExtractFileExt(sFile);


  sModule:=GetModuleFileName();

  s:=ExtractFileExt(sModule);

  if s='.exe' then
    sFile:= 'C:\ONEGA\RPLS_DB\bin\gdal.ini'
  else begin

//  if GetModuleFileName()<>'' then
    sFile:= ExtractFilePath( GetModuleFileName() ) +  'gdal.ini';

Log (sFile);

    if not FileExists(sFile) then
    begin
      sFile:= ExtractFilePath (ExtractFileDir( GetModuleFileName() )) +  'gdal.ini';
      Log (sFile);

      log ('ExtractFilePath -    ' + ExtractFilePath (ExtractFileDir( GetModuleFileName() )));

  //    sFile:= GetParentFileDir( GetModuleFileName() ) +  'gdal.ini';
//  Log(sFile);

    end;

//      ExtractPare

  end;

//  function GetParentFileDir (aFileName: string): string;
//begin
//  Result:=IncludeTrailingBackslash(ExtractFileDir (ExcludeTrailingBackslash(aFileName)));
//end;


 // else
  //  sFile:= 'C:\ONEGA\RPLS_DB\bin\WMS\wms.ini';

//   sFile:= ChangeFileExt( GetModuleFileName(), '.ini');

  Log(sFile);


 // Exit;

//  sFile:=''
 //Assert( FileExists(sFile) );


  if FileExists(sFile) then
  begin
    ini:=TIniFile.Create(sFile);
    s:=ini.ReadString('main','path','');

    FGDAL_path:= IncludeTrailingBackslash (ExtractFilePath(sFile) + s);

  Log('FGDAL_path  '+FGDAL_path);

 //   if s<>'' then
  //     Params.Dir:= IncludeTrailingBackslash(s);

//    s:=ini.ReadString('main','gdal','');
//
//    if s<>'' then
//       Params.Dir:= IncludeTrailingBackslash(s);



    FreeAndNil(ini);
  end else
    ShowMessage('not exists: ' + sFile);


//  FreeAndNil(oSList);

  //135

end;


