unit d_WMS_tools;

interface

uses
  System.Classes, 
  Vcl.Controls, Vcl.Forms, 

  CodeSiteLogging,

  u_func, RxPlacemnt

  ;

type
  Tdlg_WMS_tools = class(TForm)
    FormStorage1: TFormStorage;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class function ShowForm: Tdlg_WMS_tools;
 
  end;


implementation

{$R *.dfm}

procedure Tdlg_WMS_tools.FormCreate(Sender: TObject);
begin

end;


// ---------------------------------------------------------------
class function Tdlg_WMS_tools.ShowForm: Tdlg_WMS_tools;
// ---------------------------------------------------------------
//var
//  oForm: TForm;
begin
  CodeSite.Send('Tdlg_WMS_log - 1');

  result:= Tdlg_WMS_tools( FindForm(Tdlg_WMS_tools.ClassName) );

  if not assigned(result) then
    result:= Tdlg_WMS_tools.Create(Application);

 // dlg_WMS_log:=result;

  CodeSite.Send('Tdlg_WMS_log - 2');

  
  result.Show;
  
end;

end.
