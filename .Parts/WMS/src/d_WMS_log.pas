unit d_WMS_log;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,                                    
   
   u_func,
  
  rxPlacemnt, Vcl.Menus 
  ;

type
  Tdlg_WMS_log = class(TForm)
    Memo1: TMemo;
    FormPlacement1: TFormPlacement;
    PopupMenu1: TPopupMenu;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
  public     
    class procedure Add(aMsg: string);

    class function ShowForm: Tdlg_WMS_log;
    
  end;

 
implementation

{$R *.dfm}
                

// ---------------------------------------------------------------
class function Tdlg_WMS_log.ShowForm: Tdlg_WMS_log;
// ---------------------------------------------------------------
var
  oForm: Tdlg_WMS_log;
  
begin
 // CodeSite.Send('Tdlg_WMS_log - 1');

  oForm:= Tdlg_WMS_log( FindForm(Tdlg_WMS_log.ClassName) );

  if not assigned(oForm) then
    oForm:= Tdlg_WMS_log.Create(Application);


//  CodeSite.Send('Tdlg_WMS_log - 2');

  
  oForm.Show;
  
end;

// ---------------------------------------------------------------
class procedure Tdlg_WMS_log.Add(aMsg: string);
// ---------------------------------------------------------------
var
  oForm: Tdlg_WMS_log;
begin
  oForm:= Tdlg_WMS_log( FindForm(Tdlg_WMS_log.ClassName) );

  if assigned(oForm) then
  begin
    oForm.Memo1.Lines.Add(aMsg);
    SendMessage (oForm.Memo1.Handle, EM_LINESCROLL, 0, 1);
  
  end;
 
    
end;


procedure Tdlg_WMS_log.FormCreate(Sender: TObject);
begin
  Memo1.Align:=alClient;
end;



procedure Tdlg_WMS_log.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;

end.
