unit u_domain;



interface

uses

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB, Data.Win.ADODB,
  RxPlacemnt, Vcl.ComCtrls;

// 
//type
//  COMPUTER_NAME_FORMAT = (
//    ComputerNameNetBIOS,
//    ComputerNameDnsHostname,
//    ComputerNameDnsDomain,
//    ComputerNameDnsFullyQualified,
//    ComputerNamePhysicalNetBIOS,
//    ComputerNamePhysicalDnsHostname,
//    ComputerNamePhysicalDnsDomain,
//    ComputerNamePhysicalDnsFullyQualified,
//    ComputerNameMax);

    
function GetComputerNameExString(ANameFormat: COMPUTER_NAME_FORMAT): WideString;



implementation
 
function GetComputerNameExW(NameType: COMPUTER_NAME_FORMAT; lpBuffer: LPWSTR;
  var nSize: DWORD): BOOL; stdcall; external kernel32 name 'GetComputerNameExW';

  
function GetComputerNameExString(ANameFormat: COMPUTER_NAME_FORMAT): WideString;
var
  nSize: DWORD;
begin
  nSize := 1024;
  SetLength(Result, nSize);
  if GetComputerNameExW(ANameFormat, PWideChar(Result), nSize) then
    SetLength(Result, nSize)
  else
    Result := '';
end;


end.
