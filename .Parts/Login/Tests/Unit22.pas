unit Unit22;

interface

uses

  login_TLB,

  x_Login,

  u_func,
  
//  u_domain,
  
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB, Data.Win.ADODB,
  RxPlacemnt, Vcl.ComCtrls;

type
  TForm22 = class(TForm)
    Button1: TButton;
    ADOConnection1: TADOConnection;
    Button2: TButton;
    StatusBar1: TStatusBar;
    FormPlacement1: TFormPlacement;
    StatusBar2: TStatusBar;
    Button3: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form22: TForm22;

implementation

{$R *.dfm}





procedure TForm22.FormCreate(Sender: TObject);
var
  s: string;
  s1: string;
begin

//
//type
//  COMPUTER_NAME_FORMAT = (
//    ComputerNameNetBIOS,
//    ComputerNameDnsHostname,
//    ComputerNameDnsDomain,
//    ComputerNameDnsFullyQualified,
//    ComputerNamePhysicalNetBIOS,
//    ComputerNamePhysicalDnsHostname,
//    ComputerNamePhysicalDnsDomain,
//    ComputerNamePhysicalDnsFullyQualified,
//    ComputerNameMax);
//
//function GetComputerNameExString(ANameFormat: COMPUTER_NAME_FORMAT): WideString;
//



//  s:=GetComputerNameExString (ComputerNameDnsHostname);
//  s:=GetComputerNameExString (ComputerNameMax);
//  s:=GetComputerNameExString (ComputerNameDnsFullyQualified);

// Another simpler solution is to get the computer name via the environment variable using GetEnvironmentVariable function as follows:

//Result := 
//s1:=GetComputerNetName;



//  s:=GetEnvironmentVariable('USERNAME');
//  s:=GetEnvironmentVariable('USERDNSDOMAIN');

  s:= Format('%s\%s',[GetComputerNetName, GetEnvironmentVariable('USERNAME')]);

end;

procedure TForm22.Button1Click(Sender: TObject);
const
  REGISTRY_LOGIN_ = 'Software\Onega\RPLS_DB_LINK\Login_new1\';
var
  r: TLoginRec;
begin
  TLoginX.Create.Dlg(REGISTRY_LOGIN_, r, ADOConnection1.ConnectionObject);


//  CoLoginX.Create.Dlg(REGISTRY_LOGIN_, r, ADOConnection1.ConnectionObject);
    
  
//
//  ILoginX = interface(IUnknown)
//    ['{BC1B3AB5-18F8-4A43-BE31-04B76AEE2DC2}']
//    function Dlg(const aRegPath: WideString; var aRec: TLoginRec; const aConnectionObject: IUnknown): HResult; stdcall;
//  end;
//

//  CoLoginX.Create.Dlg()
end;


procedure TForm22.Button2Click(Sender: TObject);
var
  r: TLoginRec;
begin
  if S_OK = TLoginX.Create.Open_ini(r, ADOConnection1.ConnectionObject) then 
  begin
    StatusBar1.SimpleText:=r.status_str;
    StatusBar2.SimpleText:=r.connection_str;
  end;
  
end;

procedure TForm22.Button3Click(Sender: TObject);

const
  REGISTRY_LOGIN_ = 'Software\Onega\RPLS_DB_LINK\Login_new1\';
var
  r: TLoginRec;
begin
 // TLoginX.Create.Dlg(REGISTRY_LOGIN_, r, ADOConnection1.ConnectionObject);


  CoLoginX.Create.Dlg(REGISTRY_LOGIN_, r, ADOConnection1.ConnectionObject);
    

end;


end.


(*
 ILoginX = interface(IUnknown)
    ['{BC1B3AB5-18F8-4A43-BE31-04B76AEE2DC2}']
    function Dlg(const aRegPath: WideString; var aRec: TLoginRec; const aConnectionObject: IUnknown): HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoLoginX provides a Create and CreateRemote method to          
// create instances of the default interface ILoginX exposed by              
// the CoClass LoginX. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoLoginX = class
    class function Create: ILoginX;
    class function CreateRemote(const MachineName: string): ILoginX;
  end;
