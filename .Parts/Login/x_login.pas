unit x_login;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  d_DB_Login,

  u_SQL,

  dxCore,
  IOUtils,
  CodeSiteLogging,

  Windows, ActiveX, Classes, ComObj, login_TLB, StdVcl;

type


  {$IFDEF dll}
  TLoginX = class(TTypedComObject, ILoginX)             
  {$ELSE}  
  TLoginX = class(TInterfacedObject, ILoginX)
  {$ENDIF}  
  
  public
    function Dlg(const aRegPath: WideString; var aRec: TLoginRec; const aConnectionObject: IUnknown): HResult;
          stdcall;
          
    function Open_ini(var aRec: TLoginRec; const aConnectionObject: IUnknown): HResult;
          stdcall;
  end;

implementation

uses ComServ, Data.Win.ADODB, System.SysUtils;


function TLoginX.Dlg(const aRegPath: WideString; var aRec: TLoginRec; const aConnectionObject: IUnknown): HResult;          
var 
  r: TdbLoginRec;
begin
  Result:=S_FALSE;

  if not Tdlg_DB_Login.ExecDlg (aRegPath, r, aConnectionObject as _Connection) then 
  begin
    Result:=S_OK;
     
    aRec.connection_str:=db_ADO_MakeConnectionString (r);
    aRec.status_str    :=db_GetConnectionStatusStr (r);

  end;  
  


end;

// ---------------------------------------------------------------
function TLoginX.Open_ini(var aRec: TLoginRec; const aConnectionObject: IUnknown): HResult;          
// ---------------------------------------------------------------
var
  sAppPath: string;
 // sUser_Ini: string;
  sSrc_Ini: string;

  r: TdbLoginRec;
  sConnectionString: string;

  oConnectionObject: _Connection;
begin
  Result:=S_FALSE;

  oConnectionObject:= aConnectionObject as _Connection;
  
//  Assert(Assigned(aConnectionObject), 'Value not assigned');

  sAppPath:= IncludeTrailingBackslash (System.IOUtils.TPath.GetHomePath) + 'RPLS_DB_LINK\';
  

  {$IFDEF dll}  
  sSrc_Ini := ExtractFilePath( System.SysUtils.GetModuleName(HInstance) ) + 'login.ini' ;
  {$ELSE}
  sSrc_Ini := 'C:\ONEGA\RPLS_DB\bin\dll\' + 'login.ini' ;
  {$ENDIF}

 // sUser_Ini:=sAppPath + 'login.ini';
           
//           oConnectionObject.ConnectionTimeout:=
           

  if db_OpenFromIni(sSrc_Ini, 'main', r, oConnectionObject) then 
  begin
    Result:=S_OK;      

    aRec.connection_str:=db_ADO_MakeConnectionString (r);
    aRec.status_str    :=db_GetConnectionStatusStr (r);
                                    
    
  end;

    
//   if db_LoginRec_LoadFromIni(sSrc_Ini, 'main', r) then 
//   begin
//     sConnectionString :=db_ADO_MakeConnectionString (r);
//
//     try
//       if (adStateOpen and aConnectionObject.State) <> 0 then
//         oConnectionObject.Close;
//
//     
////       oConnectionObject.Close;
//       oConnectionObject.Open(sConnectionString,'','',0);
//       
//       aRec.connection_string:=sConnectionString;
//       
//     except on E: Exception do
//     
//       Exit;
//     
//     end;
//     
//     Result:=S_OK;
//
//   end;
//            
           

end;

initialization
  {$IFDEF dll}
  TTypedComObjectFactory.Create(ComServer, TLoginX, Class_LoginX,
    ciMultiInstance, tmApartment);
  {$ENDIF}         
end.

(*

// ---------------------------------------------------------------
function TDBLoginX.Open(const aRegPath: ShortString; var aLoginRec:
    TdbLoginRec; aOptions: TDBLoginOptions; aConnectionObject: _Connection;
    aAppHandle: Integer): boolean;
// ---------------------------------------------------------------




initialization
  {$IFDEF dll}

  dxInitialize;
 
  TTypedComObjectFactory.Create(ComServer, TReliefX, Class_ReliefX,  ciMultiInstance, tmApartment);

  {$ENDIF}        
finalization
  {$IFDEF dll}

  //!!!!!!!!!!!!!!
  CloseApplicationForms;
  
  
  //CodeSite.Send('dxFinalize');
  
  dxFinalize;
  {$ENDIF}    
end.


//  showmessage (s);
  
//  CodeSite.Send (sSrc_mdb);
     

//  if FileExists(sDest_MDB) then
//    TFile.Delete(sDest_MDB);
  
///////////  
  //if not FileExists(sDest_MDB) then

  
//  CodeSite.Send( DateTimeToString( TFile.GetLastWriteTime(sAppPath)) );
  
//  
//  if FileExists(sDest_MDB) then
//    TFile.Delete(sDest_MDB);
//
////  begin
//    ForceDirectories(sAppPath); 
//
//    TFile.Copy  (sSrc_mdb, sDest_MDB);
// // end;
      
 // mdb_OpenConnection (ADOConnection_MDB, sDest_MDB);

//  db_SetComponentADOConnection(Self, ADOConnection_MDB);
  


  
//--------------------------------------------------------------------
function db_LoginRec_LoadFromReg(aRegPath: String; var aDBLoginRec: TdbLoginRec): Boolean;
//--------------------------------------------------------------------
begin
 /////////// FillChar(aDBLoginRec, SizeOf(aDBLoginRec), 0);

  with TRegIniFile.Create(aRegPath), aDBLoginRec do
  begin

    Server        := ReadString ('', 'Server',     Server);
    DataBase      := ReadString ('', 'DataBase',   DataBase);
    Login         := ReadString ('', 'Login',      Login);
    Password      := ReadString ('', 'Password',   Password);
    IsUseWinAuth  := ReadBool   ('', 'UseWinAuth', IsUseWinAuth);
    NetworkLibrary:= ReadString ('', 'NetworkLibrary', NetworkLibrary);
    NetworkAddress:= ReadString ('', 'NetworkAddress', NetworkAddress);

(*
    Server        := ReadString ('', 'Server',   '');
    DataBase      := ReadString ('', 'DataBase', '');
    Login         := ReadString ('', 'Login',    '');
    Password      := ReadString ('', 'Password',   '');
    IsUseWinAuth  := ReadBool   ('', 'UseWinAuth', False);
    NetworkLibrary:= ReadString ('', 'NetworkLibrary', 'DBNMPNTW');
    NetworkAddress:= ReadString ('', 'NetworkAddress', Server);
*)

  //  if Login='' then Login:='sa';

    Free;
  end;

  Result := (aDBLoginRec.Server <> '') and (aDBLoginRec.DataBase <> '');
end;
