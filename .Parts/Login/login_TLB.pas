unit Login_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 21.06.2019 14:31:43 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\RPLS_DB\.Parts\Login\Project20 (1)
// LIBID: {06980A7F-0700-4038-B6BE-FE6E28ACB7C1}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (stdvcl40.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Winapi.Windows, System.Classes, System.Variants, System.Win.StdVCL, Vcl.Graphics, Vcl.OleServer, Winapi.ActiveX;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  LoginMajorVersion = 1;
  LoginMinorVersion = 0;

  LIBID_Login: TGUID = '{06980A7F-0700-4038-B6BE-FE6E28ACB7C1}';

  IID_ILoginX: TGUID = '{BC1B3AB5-18F8-4A43-BE31-04B76AEE2DC2}';
  CLASS_LoginX: TGUID = '{55097153-D83F-474A-9865-2006EB85ADAE}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ILoginX = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  LoginX = ILoginX;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  PUserType1 = ^TLoginRec; {*}


  TLoginRec = record
    server: WideString;
    Login: WideString;
    Pass: WideString;
    DataBase: WideString;
    connection_str: WideString;
    status_str: WideString;
  end;


// *********************************************************************//
// Interface: ILoginX
// Flags:     (256) OleAutomation
// GUID:      {BC1B3AB5-18F8-4A43-BE31-04B76AEE2DC2}
// *********************************************************************//
  ILoginX = interface(IUnknown)
    ['{BC1B3AB5-18F8-4A43-BE31-04B76AEE2DC2}']
    function Dlg(const aRegPath: WideString; var aRec: TLoginRec; const aConnectionObject: IUnknown): HResult; stdcall;
    function Open_ini(var aRec: TLoginRec; const aConnectionObject: IUnknown): HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoLoginX provides a Create and CreateRemote method to          
// create instances of the default interface ILoginX exposed by              
// the CoClass LoginX. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoLoginX = class
    class function Create: ILoginX;
    class function CreateRemote(const MachineName: string): ILoginX;
  end;

implementation

uses System.Win.ComObj;

class function CoLoginX.Create: ILoginX;
begin
  Result := CreateComObject(CLASS_LoginX) as ILoginX;
end;

class function CoLoginX.CreateRemote(const MachineName: string): ILoginX;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_LoginX) as ILoginX;
end;

end.

