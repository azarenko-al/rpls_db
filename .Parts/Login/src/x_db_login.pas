unit x_db_login;

interface

uses
  adodb, ADOInt,SysUtils, Forms, Dialogs,

  dm_Connection,

  d_DB_Login_1,
  d_DB_Login,

  u_sql,

  u_func,
  u_reg,

  I_DB_login
  ;


type
  TDBLoginX = class(TInterfacedObject, IDBLoginX)
  private
    FOldHandle : Integer;
    FAppHandle : Integer;

    function OpenFromReg(aRegPath: WideString; var aLoginRec: TdbLoginRec; aConnectionObject:
        _Connection): boolean; stdcall;

  public
    constructor Create;
    destructor Destroy; override;

    procedure InitAppHandle(aAppHandle: Integer); stdcall;

    function ExecDlg(var aLoginRec: TdbLoginRec; aAppHandle: Integer): Boolean; stdcall;

    function Open(const aRegPath: ShortString; var aLoginRec: TdbLoginRec;
        aOptions: TDBLoginOptions; aConnectionObject: _Connection; aAppHandle:
        Integer): boolean; stdcall;

    function OpenDatabase_(var aLoginRec: TdbLoginRec; aConnectionObject:_Connection):
        Boolean; stdcall;

    function DatabaseExists(aRec: TdbLoginRec; aDatabaseName: ShortString):
        Boolean; stdcall;

  end;


//  function db_DatabaseExists(aRec: TdbLoginRec; aDatabaseName: WideString): Boolean;
  //    stdcall;


//  function DllGetInterface(const IID: TGUID; var Obj): HResult; stdcall;

  function DllGetInterface_IDBLoginX (var Obj): HResult; stdcall;


exports
 // db_DatabaseExists,
  DllGetInterface_IDBLoginX
 // DllGetInterface

  ;

implementation



constructor TDBLoginX.Create;
begin
  inherited;
 // TdmConnection.Init;
end;


destructor TDBLoginX.Destroy;
begin
  dmConnection_Free();

 // FreeAndNil(dmConnection);

  if FAppHandle>0 then
    Application.Handle := 0;

  inherited;

 // ShowMessage('destructor TDBLoginX.Destroy;');
end;


// ---------------------------------------------------------------
function TDBLoginX.ExecDlg(var aLoginRec: TdbLoginRec; aAppHandle: Integer): Boolean;
// ---------------------------------------------------------------
begin
  FOldHandle:=Application.Handle;
  Application.Handle := aAppHandle;

  Result := Tdlg_DB_Login1.ExecDlg(aLoginRec);

  Application.Handle := FOldHandle;
end;

procedure TDBLoginX.InitAppHandle(aAppHandle: Integer);
begin
  FAppHandle :=aAppHandle;

  Application.Handle := aAppHandle;     
end;


//--------------------------------------------------------------------
function TDBLoginX.OpenFromReg(aRegPath: WideString; var aLoginRec: TdbLoginRec;
    aConnectionObject: _Connection): boolean;
//--------------------------------------------------------------------
var
  sConnectionString: string;
begin
  Assert(Assigned(aConnectionObject), 'Value not assigned');

  db_LoginRec_LoadFromReg (aRegPath, aLoginRec);

  sConnectionString:=db_ADO_MakeConnectionString (aLoginRec);

  try

    if (adStateOpen and aConnectionObject.State) <> 0 then
      aConnectionObject.Close;

    aConnectionObject.Open(sConnectionString,'','',0);

    Result := True;
  except
    Result := False;
  end;

end;

// ---------------------------------------------------------------
function TDBLoginX.Open(const aRegPath: ShortString; var aLoginRec:
    TdbLoginRec; aOptions: TDBLoginOptions; aConnectionObject: _Connection;
    aAppHandle: Integer): boolean;
// ---------------------------------------------------------------
var
  bShowDlg: boolean;
begin
  Assert(aAppHandle>0, 'Value <=0');
  Assert(Assigned(aConnectionObject), 'Value not assigned');

//z  Exit;

//  ShowMessage();

  FOldHandle:=Application.Handle;
  Application.Handle := aAppHandle;

//  ShowMessage(IntToStr(FOldHandle));

  db_LoginRec_LoadFromReg (aRegPath, aLoginRec);

  //Result:=Tdlg_DB_Login.ExecDlg (aRegPath, aLoginRec, aConnectionObject);

  //  TDBLoginOpenMode = (omFromReg, omShowDlg, omShowDlg1);

  if aOptions=[] then
//  if not aForceDialog then
    bShowDlg:=reg_ReadBool (aRegPath, 'IsShowDialog', True)
  else
    bShowDlg:=False;

  if (loFromReg in aOptions) then
    bShowDlg:=False;

//    aForceDialog:=reg_ReadBool (aRegPath, 'IsShowDialog', True);

  if bShowDlg or (loShowDlg in aOptions) then

//  if aForceDialog then
 // begin                                            //rLoginRec

    Result:=Tdlg_DB_Login.ExecDlg (aRegPath, aLoginRec, aConnectionObject)

  else begin

    db_LoginRec_LoadFromReg (aRegPath, aLoginRec);

(*    TdmLogin.Init;
    Result := dmLogin.OpenDatabase_(aLoginRec, aConnectionObject);
*)
    Result:=db_OpenFromReg (aRegPath, aLoginRec, aConnectionObject);

    if not Result then
      Result:=Tdlg_DB_Login.ExecDlg (aRegPath, aLoginRec, aConnectionObject)
  end;

  if Result then
    aLoginRec.ConnectionStatusStr := db_GetConnectionStatusStr(aLoginRec);

  Application.Handle := FOldHandle;
  //  Application.Handle := 0;

end;


function TDBLoginX.DatabaseExists(aRec: TdbLoginRec; aDatabaseName:
    ShortString): Boolean;
begin
 // TdmConnection.Init;
  Result := dmConnection1.IsDatabaseExists(aRec, aDatabaseName);
//  FreeAndNil(dmLogin);
end;

function TDBLoginX.OpenDatabase_(var aLoginRec: TdbLoginRec;
    aConnectionObject:_Connection): Boolean;
begin
  Result :=dmConnection1.OpenDatabase_(aLoginRec, aConnectionObject);

end;

// ---------------------------------------------------------------
function DllGetInterface_IDBLoginX (var Obj): HResult; stdcall;
// ---------------------------------------------------------------
begin
  Result:=S_FALSE; Integer(Obj):=0;

  with TDBLoginX.Create do
    if GetInterface(IDBLoginX,OBJ) then
      Result:=S_OK

end;



end.





(*
function Get_IDBLogin: IDBLoginX; stdcall;
begin
  with TDBLoginX.Create do
    GetInterface(IDBLoginX, Result);
    //  Result := nil;;
end;

*)


//function TDBLoginX.Dlg_Login1: boolean;
//begin
//  Result:=Tdlg_DB_Login.ExecDlg1 ();
//
//end;
(*
// ---------------------------------------------------------------
procedure TDBLoginX.Init(aAppHandle: Integer);
// ---------------------------------------------------------------
begin
 // ShowMessage('procedure TDBLoginX.Init();'  + IntToStr(aAppHandle));

 // FOldHandle :=Application.Handle;
 Application.Handle := aAppHandle;

//  dmLogin.SetConnection(aConnection);

 // FIsInitDone := True;
end;
*)


  //  aConnectionObject.Close;

(*
  function TADOConnection.GetConnected: Boolean;
begin
  WaitForConnectComplete;
  Result := Assigned(ConnectionObject) and ((adStateOpen and ConnectionObject.State) <> 0);
end;*)




{


function TDBLoginX.Open1(const aRegPath: String; var aLoginRec: TdbLoginRec;
    aForceDialog: Boolean; aADOConnection: TADOConnection): boolean;
begin

//z  Exit;

//  ShowMessage();

//  FOldHandle:=Application.Handle;
//  Application.Handle := aAppHandle;

  Assert(Assigned(aConnectionObject), 'Value not assigned');

  db_LoadConnectRecFromReg (aRegPath, aLoginRec);

  //Result:=Tdlg_DB_Login.ExecDlg (aRegPath, aLoginRec, aConnectionObject);


  if not aForceDialog then
    aForceDialog:=reg_ReadBool (aRegPath, 'IsShowDialog', True);

  if aForceDialog then
 // begin                                            //rLoginRec

    Result:=Tdlg_DB_Login.ExecDlg (aRegPath, aLoginRec, aConnectionObject)

  else begin
    Result:=OpenFromReg1 (aRegPath, aLoginRec, aConnection);
    if not Result then
      Result:=Tdlg_DB_Login.ExecDlg1 (aRegPath, aLoginRec, aConnection)

  end;

  if Result then
    aLoginRec.ConnectionStatusStr := db_GetConnectionStatusStr(aLoginRec);

//  Application.Handle := FOldHandle;


//  Application.Handle := 0;

end;




// ---------------------------------------------------------------
function TDBLoginX.Open1(const aRegPath: String; var aLoginRec: TdbLoginRec;
    aForceDialog: Boolean; aADOConnection: TADOConnection): boolean;
// ---------------------------------------------------------------
begin

//z  Exit;

//  ShowMessage();

//  FOldHandle:=Application.Handle;
//  Application.Handle := aAppHandle;

  //Assert(Assigned(aConnectionObject), 'Value not assigned');

  db_LoadConnectRecFromReg (aRegPath, aLoginRec);

  //Result:=Tdlg_DB_Login.ExecDlg (aRegPath, aLoginRec, aConnectionObject);


  if not aForceDialog then
    aForceDialog:=reg_ReadBool (aRegPath, 'IsShowDialog', True);

  if aForceDialog then
 // begin                                            //rLoginRec

    Result:=Tdlg_DB_Login.ExecDlg1 (aRegPath, aLoginRec, aADOConnection)

  else begin
    Result:=db_OpenFromReg1 (aRegPath, aLoginRec, aADOConnection);
    if not Result then
      Result:=Tdlg_DB_Login.ExecDlg1 (aRegPath, aLoginRec, aADOConnection)

  end;

  if Result then
    aLoginRec.ConnectionStatusStr := db_GetConnectionStatusStr(aLoginRec);

//  Application.Handle := FOldHandle;


//  Application.Handle := 0;

end;





(*
//--------------------------------------------------------------------
function TDBLoginX.OpenFromReg(aRegPath: ShortString; var aLoginRec: TdbLoginRec;
    aConnectionObject: _Connection): boolean;
//--------------------------------------------------------------------
var
  sConnectionString: string;
begin
  Assert(Assigned(aConnectionObject), 'Value not assigned');

  db_LoadConnectRecFromReg (aRegPath, aLoginRec);

  sConnectionString:=db_ADO_MakeConnectionString (aLoginRec);

  try

    if (adStateOpen and aConnectionObject.State) <> 0 then
      aConnectionObject.Close;

    aConnectionObject.Open(sConnectionString,'','',0);

    Result := True;
  except
    Result := False;
  end;

end;


//--------------------------------------------------------------------
function TDBLoginX.OpenFromReg1(aRegPath: string; var aLoginRec: TdbLoginRec; aConnection:
    TADOConnection): boolean;
//--------------------------------------------------------------------
var
  sConnectionString: string;
begin
  Assert(Assigned(aConnectionObject), 'Value not assigned');

  db_LoadConnectRecFromReg (aRegPath, aLoginRec);

  sConnectionString:=db_ADO_MakeConnectionString (aLoginRec);

  try

//    if (adStateOpen and aConnectionObject.State) <> 0 then
 //     aConnectionObject.Close;

    aConnection.Open(sConnectionString);

    Result := True;
  except
    Result := False;
  end;

end;

*)


//  ShowMessage('');

 // ShowMessage('constructor TDBLoginX.Create;'+IntToStr(Application.Handle));

//  FOldHandle:=Application.Handle;

(*
  ShowMessage('constructor TDBLoginX.Create;'+IntToStr(Application.Handle));

  Application.Initialize;

  ShowMessage('Application.Initialize - constructor TDBLoginX.Create;'+IntToStr(Application.Handle));
*)



// ---------------------------------------------------------------
function TDBLoginX.Open_ADOCon(const aRegPath: String; var aLoginRec: TdbLoginRec; aKind:
    TDBLoginOpenMode; aForceDialog: Boolean; aADOConnection: TADOConnection): boolean;
// ---------------------------------------------------------------
begin

//z  Exit;

//  ShowMessage();

//  FOldHandle:=Application.Handle;
//  Application.Handle := aAppHandle;

  Assert(Assigned(aADOConnection), 'Value not assigned');

  db_LoadConnectRecFromReg (aRegPath, aLoginRec);

  //Result:=Tdlg_DB_Login.ExecDlg (aRegPath, aLoginRec, aConnectionObject);


  if not aForceDialog then
    aForceDialog:=reg_ReadBool (aRegPath, 'IsShowDialog', True);

  if aForceDialog then
 // begin                                            //rLoginRec

    Result:=Tdlg_DB_Login.ExecDlg_ADOCOnn (aRegPath, aLoginRec, aADOConnection)

  else begin
    Result:=OpenFromReg_ADOConn(aRegPath, aLoginRec, aADOConnection);
    if not Result then
      Result:=Tdlg_DB_Login.ExecDlg_ADOCOnn (aRegPath, aLoginRec, aADOConnection)

  end;

  if Result then
    aLoginRec.ConnectionStatusStr := db_GetConnectionStatusStr(aLoginRec);

//  Application.Handle := FOldHandle;


//  Application.Handle := 0;

end;




//--------------------------------------------------------------------
function TDBLoginX.OpenFromReg_ADOConn(aRegPath: string; var aLoginRec: TdbLoginRec;
    aADOConnection: TADOConnection): boolean;
//--------------------------------------------------------------------
var
  sConnectionString: string;
begin
  Assert(Assigned(aADOConnection), 'Value not assigned');

  u_sql.db_LoadConnectRecFromReg (aRegPath, aLoginRec);

  sConnectionString:=db_ADO_MakeConnectionString (aLoginRec);

  try

//    if (adStateOpen and aConnectionObject.State) <> 0 then
 //     aConnectionObject.Close;

    aADOConnection.ConnectionString :=sConnectionString;
    aADOConnection.Open;

    Result := True;
  except
    Result := False;
  end;

end;



 // DBLogin_ExecDlg,
//  DBLogin_Open_ADOCon


 //   function OpenFromReg_ADOConn(aRegPath: string; var aLoginRec: TdbLoginRec;
   //     aADOConnection: TADOConnection): boolean;



   
(*  function DBLogin_ExecDlg(var aLoginRec: TdbLoginRec; aAppHandle: Integer): Boolean;
      stdcall;
*)

(*
function DBLogin_Open_ADOCon(const aRegPath: ShortString; var aLoginRec: TdbLoginRec;
      aForceDialog: Boolean; aAppHandle: Integer; aConnectionObject: _Connection): boolean;
      stdcall;

*)

(*  function DBLogin_Open_ADOCon(const aRegPath: ShortString; var aLoginRec: TdbLoginRec;
      aKind: TDBLoginOpenMode; aForceDialog: Boolean; aADoConnection: TADOConnection):
      boolean; stdcall;
*)



// function Get_IDBLogin: IDBLoginX; stdcall;

(*
exports
  Get_IDBLogin;
*)




//(*function DBLogin_ExecDlg(var aLoginRec: TdbLoginRec; aAppHandle: Integer): Boolean;
//begin
//  with TDBLoginX.Create do
//  begin
//    Result := ExecDlg(aLoginRec,aAppHandle);
//    Free;
//  end;
//end;
//*)
//
//
//function TDBLoginX.MakeADOConnectionString(aRec: TdbLoginRec): ShortString;
//begin
//  Result := db_ADO_MakeConnectionString(aRec);
//end;




(*
  function DBLogin_Open_ADOCon(const aRegPath: ShortString; var aLoginRec: TdbLoginRec;
      aForceDialog: Boolean; aAppHandle: Integer; aConnectionObject: _Connection): boolean;
begin
  with TDBLoginX.Create do
  begin
//    Result := Open1(aRegPath, aLoginRec, aForceDialog, aADoConnection);
    Result := Open(aRegPath, aLoginRec, aForceDialog, aAppHandle, aConnectionObject);
    Free;
  end;

end;
*)

(*
// ---------------------------------------------------------------
function DBLogin_Open_ADOCon(const aRegPath: ShortString; var aLoginRec: TdbLoginRec;
    aKind: TDBLoginOpenMode; aForceDialog: Boolean; aADoConnection: TADOConnection):
    boolean;
// ---------------------------------------------------------------
begin
  with TDBLoginX.Create do
  begin
//    Result := Open1(aRegPath, aLoginRec, aForceDialog, aADoConnection);
    Result := Open_ADOCon(aRegPath, aLoginRec, aKind, aForceDialog,  aADoConnection);
    Free;
  end;
end;*)

 //   function Open_ADOCon(const aRegPath: String; var aLoginRec: TdbLoginRec; aKind:
  //      TDBLoginOpenMode; aForceDialog: Boolean; aADOConnection: TADOConnection): boolean;

  //    function MakeADOConnectionString(aRec: TdbLoginRec): ShortString; stdcall;


  //  function Open1(const aRegPath: String; var aLoginRec: TdbLoginRec; aForceDialog: Boolean;
  //      aConnection: TADOConnection): boolean; stdcall;


  
(*
uses
   d_DB_Login_1,
   d_DB_Login;

*)

(*
function db_DatabaseExists(aRec: TdbLoginRec; aDatabaseName: WideString): Boolean;
    stdcall;
begin
  TdmLogin.Init;
  Result := dmLogin.IsDatabaseExists(aRec, aDatabaseName);
//  FreeAndNil(dmLogin);
end;
*)

