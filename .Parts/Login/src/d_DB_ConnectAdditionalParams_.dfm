object dlg_DB_ConnectAdditionalParams_: Tdlg_DB_ConnectAdditionalParams_
  Left = 1016
  Top = 708
  BorderStyle = bsDialog
  Caption = #1048#1079#1084#1077#1085#1077#1085#1080#1077' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1080' '#1089#1077#1090#1077#1074#1086#1081' '#1073#1080#1073#1083#1080#1086#1090#1077#1082#1080
  ClientHeight = 291
  ClientWidth = 511
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object rg_LIbraries: TRadioGroup
    Left = 8
    Top = 10
    Width = 169
    Height = 71
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = #1057#1077#1090#1077#1074#1099#1077' '#1073#1080#1073#1083#1080#1086#1090#1077#1082#1080
    ItemIndex = 0
    Items.Strings = (
      '1. '#1048#1084#1077#1085#1086#1074#1072#1085#1085#1099#1077' '#1082#1072#1085#1072#1083#1099
      '2. TCP/IP')
    TabOrder = 0
    OnClick = rg_LIbrariesClick
  end
  object GroupBox1: TGroupBox
    Left = 187
    Top = 10
    Width = 313
    Height = 127
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1089#1086#1077#1076#1080#1085#1077#1085#1080#1103
    TabOrder = 1
    object lb_ServerName: TLabel
      Left = 16
      Top = 24
      Width = 63
      Height = 13
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = #1048#1084#1103' '#1089#1077#1088#1074#1077#1088#1072
    end
    object lb_PortNumber: TLabel
      Left = 37
      Top = 75
      Width = 64
      Height = 13
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = #1053#1086#1084#1077#1088' '#1087#1086#1088#1090#1072
    end
    object cb_DynamicPort: TCheckBox
      Left = 16
      Top = 53
      Width = 185
      Height = 17
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = #1044#1080#1085#1072#1084#1080#1095#1077#1089#1082#1080' '#1086#1087#1088#1077#1076#1077#1083#1080#1090#1100' '#1087#1086#1088#1090
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = cb_DynamicPortClick
    end
    object ed_ServerName: TEdit
      Left = 153
      Top = 21
      Width = 149
      Height = 21
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      TabOrder = 1
    end
    object ed_PortNumber: TEdit
      Left = 153
      Top = 72
      Width = 149
      Height = 21
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      TabOrder = 2
    end
  end
  object pn_Buttons: TPanel
    Left = 0
    Top = 256
    Width = 511
    Height = 35
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 2
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 511
      Height = 2
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Align = alTop
      Shape = bsTopLine
    end
    object Panel1: TPanel
      Left = 320
      Top = 2
      Width = 191
      Height = 33
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btn_Ok: TButton
        Left = 20
        Top = 5
        Width = 75
        Height = 23
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Action = act_Ok
        Default = True
        ModalResult = 1
        TabOrder = 0
      end
      object btn_Cancel: TButton
        Left = 108
        Top = 5
        Width = 75
        Height = 23
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        ModalResult = 2
        TabOrder = 1
      end
    end
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 71
    Top = 148
    object act_Ok: TAction
      Caption = 'Ok'
      OnExecute = act_OkExecute
    end
  end
end
