inherited dlg_DB_ConnectAdditionalParams: Tdlg_DB_ConnectAdditionalParams
  Left = 926
  Top = 337
  Width = 547
  Height = 363
  Caption = 'dlg_DB_ConnectAdditionalParams'
  ParentFont = True
  OldCreateOrder = True
  PixelsPerInch = 120
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 295
    Width = 539
    inherited Bevel1: TBevel
      Width = 539
    end
    inherited Panel3: TPanel
      Left = 352
      Width = 187
      inherited btn_Ok: TButton
        Left = 16
      end
      inherited btn_Cancel: TButton
        Left = 100
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 539
    inherited Bevel2: TBevel
      Width = 539
    end
    inherited pn_Header: TPanel
      Width = 539
    end
  end
  object rg_LIbraries: TRadioGroup [2]
    Left = 8
    Top = 72
    Width = 169
    Height = 70
    Caption = #1057#1077#1090#1077#1074#1099#1077' '#1073#1080#1073#1083#1080#1086#1090#1077#1082#1080
    ItemIndex = 0
    Items.Strings = (
      '1. '#1048#1084#1077#1085#1086#1074#1072#1085#1085#1099#1077' '#1082#1072#1085#1072#1083#1099
      '2. TCP/IP')
    TabOrder = 2
    OnClick = rg_LIbrariesClick
  end
  object GroupBox1: TGroupBox [3]
    Left = 192
    Top = 72
    Width = 337
    Height = 217
    Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1103
    TabOrder = 3
    object lb_ServerName: TLabel
      Left = 16
      Top = 24
      Width = 67
      Height = 13
      Caption = #1048#1084#1103' '#1089#1077#1088#1074#1077#1088#1072
    end
    object lb_PortNumber: TLabel
      Left = 72
      Top = 75
      Width = 66
      Height = 13
      Caption = #1053#1086#1084#1077#1088' '#1087#1086#1088#1090#1072
    end
    object cb_DynamicPort: TCheckBox
      Left = 16
      Top = 53
      Width = 185
      Height = 17
      Caption = #1044#1080#1085#1072#1084#1080#1095#1077#1089#1082#1080' '#1086#1087#1088#1077#1076#1077#1083#1080#1090#1100' '#1087#1086#1088#1090
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = cb_DynamicPortClick
    end
    object ed_ServerName: TEdit
      Left = 153
      Top = 21
      Width = 149
      Height = 21
      TabOrder = 1
    end
    object ed_PortNumber: TEdit
      Left = 153
      Top = 72
      Width = 149
      Height = 21
      TabOrder = 2
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
  end
end
