unit fr_DB_Login;

interface

uses
  Variants, Classes, Controls, Forms, 
  Dialogs, StdCtrls, ExtCtrls, Mask,  adodb,
  
  u_func,
  dm_Login,
  
  I_DB_login,

  u_sql,  

//  u_db_login,

  rxToolEdit;

type
  Tframe_DB_Login = class(TFrame)
    lbLogin: TLabel;
    lbPass: TLabel;
    lbDatabase: TLabel;
    lbAuth: TLabel;
    lbServer: TLabel;
    Bevel1: TBevel;
    ed_Login: TEdit;
    ed_Password: TEdit;
    cb_Database: TComboBox;
    ed_Server: TComboEdit;
    rg_Auth: TRadioGroup;
    btn_Additional: TButton;
    procedure cb_DatabaseDropDown(Sender: TObject);
    procedure ed_ServerButtonClick(Sender: TObject);
    procedure FrameClick(Sender: TObject);
    procedure rg_AuthClick(Sender: TObject);
  private
    FADOConnection: TADOConnection;
    FLoginRec: TdbLoginRec;
    FRegPath: string;
    procedure Connect;
    procedure LoadFromReg(aRegPath: string);
    procedure Refresh_Database_List;
    procedure SaveEditsToRec;
    procedure Select_Server_Name;
    { Private declarations }
  public
    procedure Init;
    { Public declarations }
  end;

implementation

{$R *.dfm}


procedure Tframe_DB_Login.Init;
begin
  lbLogin.Caption   :='������������';
  lbPass.Caption    :='������';
  LbServer.Caption  :='������';
  lbDatabase.Caption:='���� ������';
  lbAuth.Caption    :='�����������';

end;

procedure Tframe_DB_Login.cb_DatabaseDropDown(Sender: TObject);
begin
  Refresh_Database_List();
end;

//----------------------------------------------------------------------
procedure Tframe_DB_Login.Connect;
//----------------------------------------------------------------------
begin
  SaveEditsToRec();

 // if dmLogin.CheckConnectionToSQLServer (FLoginRec) then
  begin
    db_SaveConnectRecToReg (FRegPath, FLoginRec);

    FADOConnection.Close;
    FADOConnection.ConnectionString:=db_ADO_MakeConnectionString (FLoginRec);
    FADOConnection.DefaultDatabase:=FLoginRec.DataBase;

    try
      FADOConnection.Open;

//      FLoginRec.User1:= db_GetLoggedUserName(FADOConnection);

    except
      ShowMessage ( '�� ������ ������������ � ���� ������.' );
    end;


  //  if FADOConnection.Connected then
    //  ModalResult:=mrOK;
  end;
end;


procedure Tframe_DB_Login.ed_ServerButtonClick(Sender: TObject);
begin
  Select_Server_Name();
end;

procedure Tframe_DB_Login.FrameClick(Sender: TObject);
begin

end;

//-----------------------------------------------------------------
procedure Tframe_DB_Login.LoadFromReg(aRegPath: string);
//-----------------------------------------------------------------
begin
  db_LoadConnectRecFromReg (FRegPath, FLoginRec);

  with FLoginRec do
  begin
    //default values
    if Server=''    then Server   :='(local)';
    if Login=''     then Login    :='sa';
  //  if Password=''  then Password :='sa';
    if Database=''  then Database :='onega';


    ed_Server.Text    := Server;
    ed_Login.Text     := Login;
    ed_Password.Text  := Password;
    cb_Database.Text  := Database;

    rg_Auth.ItemIndex  :=IIF(IsUseWinAuth,1,0);

//    cb_Auth.ItemIndex := IIF(IsUseWinAuth,1,0);
  end;

 // cb_ShowOnStart.Checked:=reg_ReadBool (FRegPath, 'IsShowDialog', True);

end;

//-----------------------------------------------------------------
procedure Tframe_DB_Login.Refresh_Database_List;
//-----------------------------------------------------------------
begin
  SaveEditsToRec();

  dmLogin.GetDatabaseList(FLoginRec, cb_Database.Items);
 // db_GetDatabaseList (FLoginRec, cb_Database.Items);

  if cb_Database.Items.IndexOf(cb_Database.Text)=-1 then
    cb_Database.Text:= '';
end;

procedure Tframe_DB_Login.rg_AuthClick(Sender: TObject);
begin
  ed_Login.Enabled:=rg_Auth.ItemIndex=0;
  ed_Password.Enabled:=rg_Auth.ItemIndex=0;

end;

//--------------------------------------------------------------------
procedure Tframe_DB_Login.SaveEditsToRec;
//--------------------------------------------------------------------
begin
  with FLoginRec do
  begin
    Server      :=ed_Server.Text;
    Login       :=ed_Login.Text;
    Password    :=ed_Password.Text;
    Database    :=cb_Database.Text;

    IsUseWinAuth:=rg_Auth.ItemIndex=1;
  end;
end;

//----------------------------------------------------------------------
procedure Tframe_DB_Login.Select_Server_Name;
//----------------------------------------------------------------------
var  sName : string;
begin
  sName := db_Dlg_GetSqlServerName_old (Self.Handle);
//  Application.ProcessMessages;  //refresh screen

  if sName<>'' then
  begin
    ed_Server.Text := sName;
    Refresh_Database_List;
  end;

end;

end.
