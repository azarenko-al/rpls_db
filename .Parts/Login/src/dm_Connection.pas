unit dm_Connection;

interface

uses
  Classes, DB, ADOInt, ADODB, Forms, Controls, SysUtils, Dialogs,

//  I_db_login,

  u_db,
  u_SQL;


type
  TdmConnection = class(TDataModule)
    qry_Databases: TADOQuery;
    ADOConnection1: TADOConnection;
    qry_Roles: TADOQuery;
    ADOQuery1: TADOQuery;
    qry_User: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
//  procedure SQLServer_DropAllObjects(aADOConnection: TADOConnection);
  public
   // class procedure Init;

    function CheckConnect (aLoginRec: TdbLoginRec): Boolean;

    function SQLServer_GetDatabaseList(aLoginRec: TdbLoginRec; aDatabaseList:   TStrings): boolean;
    
    function IsDatabaseExists(aLoginRec: TdbLoginRec; aDatabaseName: String):   boolean;

    function OpenDatabase(var aLoginRec: TdbLoginRec; aADOConnection:   TADOConnection): Boolean;

    function OpenDatabase_(var aLoginRec: TdbLoginRec;   aConnectionObject:_Connection): Boolean;

//    procedure SetConnection(aConnection: _Connection);
  end;


function dmConnection: TdmConnection;
procedure dmConnection_Free;


implementation
{$R *.dfm}

var
  FdmConnection: TdmConnection;

// ---------------------------------------------------------------
function dmConnection: TdmConnection;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmConnection) then
    FdmConnection:=TdmConnection.Create(Application);

  Result := FdmConnection;
end;

// ---------------------------------------------------------------
procedure dmConnection_Free;
// ---------------------------------------------------------------
begin
  if Assigned(FdmConnection) then
    FreeAndNil(FdmConnection);

//    FdmConnection:=TdmConnection.Create(Application);

end;



(*

class procedure TdmConnection.Init;
begin
  if not Assigned(dmConnection1) then
    dmConnection1:=TdmConnection.Create(Application);
end;
*)

procedure TdmConnection.DataModuleCreate(Sender: TObject);
begin
 // ShowMessage('procedure TdmLogin.DataModuleCreate(Sender: TObject);');

  Assert(Assigned(ADOConnection1), 'ADOConnection1 not assigned');

end;


procedure TdmConnection.DataModuleDestroy(Sender: TObject);
begin
//  ShowMessage('procedure TdmLogin.DataModuleDestroy(Sender: TObject);');

//  dmConnection1:=nil;
end;


//----------------------------------------------------------------------
function TdmConnection.CheckConnect(aLoginRec: TdbLoginRec): Boolean;
//----------------------------------------------------------------------
var
  sConnectionString: string;
begin
  Result:=False;

  ADOConnection1.Close;
  ADOConnection1.ConnectionString:=db_ADO_MakeConnectionString (aLoginRec);
//  oADOConnection.DefaultDatabase:=FLoginRec.DataBase;

  sConnectionString :=ADOConnection1.ConnectionString;

  try
    ADOConnection1.Open;
    Result:= ADOConnection1.Connected;

   // ADOConnection1.Connected:=False;

  //  ADOConnection1.Cancel;
    ADOConnection1.Close;

    ShowMessage ( '����������� � ���� ������ �����������.');

  except
    ShowMessage ( '������: �� ������ ������������ � ���� ������: '+sConnectionString );
  end;

 // ADOConnection1.Close;

//  Result:= ADOConnection1.Connected;

end;


//--------------------------------------------------------------------
function TdmConnection.SQLServer_GetDatabaseList(aLoginRec: TdbLoginRec;
    aDatabaseList: TStrings): boolean;
//--------------------------------------------------------------------
const

  SQL_SELECT_Databases1 =
    'SELECT name as Database_NAME '+
    ' FROM master..sysdatabases   '+
    ' WHERE name NOT in (''master'',''tempdb'',''model'',''msdb'') ';
      //WHERE dbid > 6


 // SQL_SELECT_Databases ='EXEC sp_databases';

var
  i: Integer;
begin
  Assert(Assigned(ADOConnection1));

  Screen.Cursor :=crHourGlass;
  // -------------------------

  aDatabaseList.Clear;

  aLoginRec.DataBase:='';

 //// aLoginRec.DataBase:='master';

  ADOConnection1.Close;
 ///// ADOConnection1.DefaultDatabase:= 'master';
  ADOConnection1.ConnectionString:= db_ADO_MakeConnectionString (aLoginRec);

  try
    ADOConnection1.Open;

 //   qry_Databases.sql.Text:= SQL_SELECT_Databases;// 'exec sp_databases';
  //  qry_Databases.Open;

    qry_Databases.sql.Text:= SQL_SELECT_Databases1;// 'exec sp_databases';
    qry_Databases.Open;


//   db_View(qry_Databases);


    with qry_Databases do
      while not Eof do
      begin
//        aDatabaseList.Add(FieldByName('NAME').AsString);
        aDatabaseList.Add(FieldByName('Database_NAME').AsString);
        Next;
      end;

    Result := True;
  except
    result := False;
  end;

  // -------------------------
  Screen.Cursor :=crDefault;
(*
      'SELECT name FROM master..sysdatabases '+
    ' WHERE name NOT in (''master'',''tempdb'',''model'',''msdb'') ';
      //WHERE dbid > 6*)


  aDatabaseList.Delete(aDatabaseList.IndexOf('master'));
  aDatabaseList.Delete(aDatabaseList.IndexOf('tempdb'));
  aDatabaseList.Delete(aDatabaseList.IndexOf('model'));
  aDatabaseList.Delete(aDatabaseList.IndexOf('msdb'));

//  ShowMessage(aDatabaseList.Text);

end;


//--------------------------------------------------------------------
function TdmConnection.IsDatabaseExists(aLoginRec: TdbLoginRec; aDatabaseName:
    String): boolean;
//--------------------------------------------------------------------
var
  oStrList: TStringList;
begin
  oStrList:=TStringList.Create;

  Result := SQLServer_GetDatabaseList(aLoginRec, oStrList);
  if Result then
    Result := oStrList.IndexOf(aDatabaseName)>=0;

//  oStrList.Free;

  FreeAndNil(oStrList);

end;

// ---------------------------------------------------------------
function TdmConnection.OpenDatabase_(var aLoginRec: TdbLoginRec;
    aConnectionObject:_Connection): Boolean;
// ---------------------------------------------------------------
var
  v_old: _Connection;
begin
  v_old :=ADOConnection1.ConnectionObject;
  ADOConnection1.ConnectionObject := aConnectionObject;

  Result := OpenDatabase(aLoginRec, ADOConnection1);

  ADOConnection1.ConnectionObject := v_old;
end;


// ---------------------------------------------------------------
function TdmConnection.OpenDatabase(var aLoginRec: TdbLoginRec; aADOConnection: TADOConnection): Boolean;
// ---------------------------------------------------------------
var
  sUserName: string;
begin
  Assert(aLoginRec.server <> '' );

  Result:= False;

//  aLoginRec.DataBase :='master';

  aADOConnection.Close;
  aADOConnection.ConnectionString:=db_ADO_MakeConnectionString (aLoginRec);
//  oADOConnection.DefaultDatabase:=FLoginRec.DataBase;

//  sConnectionString :=ADOConnection1.ConnectionString;

  try
    aADOConnection.Open;
//    Result:= ADOConnection1.Connected;

    qry_User.Connection := aADOConnection;
    qry_User.SQL.Text := 'SELECT user_name()';
    qry_User.Open;

    sUserName := qry_User.Fields[0].AsString;

    qry_Roles.Connection := aADOConnection;
    qry_Roles.SQL.Text := 'exec sp_helprolemember ''db_owner''';
    qry_Roles.Open;

  (*
    qry_sysdatabases.Connection := aADOConnection;
    qry_sysdatabases.SQL.Text :=
       Format('SELECT * FROM master..sysdatabases WHERE name=''%s''', [aLoginRec.DataBase]);
    qry_sysdatabases.Open;
*)
  //  aLoginRec.Results.DBID := qry_sysdatabases.FieldByName('DBID').AsInteger;

 //
    aLoginRec.Results.Is_DB_owner:= qry_Roles.Locate('MemberName', sUserName, []);
    aLoginRec.Results.UserName   := sUserName;

//    aLoginRec.Results.

  //  aLoginRec.Results.DatabaseListText := GetDatabaseListText(aADOConnection);

    Result:=True;
  except
  end;

end;



initialization

finalization
//  if Assigned(dmConnection1) then
 //   FreeAndNil(dmConnection1);

 // FreeAndNil(dmConnection1);


end.




