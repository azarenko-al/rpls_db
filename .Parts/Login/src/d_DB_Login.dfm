object dlg_DB_Login: Tdlg_DB_Login
  Left = 872
  Top = 382
  Cursor = crArrow
  BorderIcons = [biSystemMenu, biHelp]
  BorderStyle = bsDialog
  Caption = 'dlg_DB_Login'
  ClientHeight = 210
  ClientWidth = 436
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -10
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    436
    210)
  PixelsPerInch = 96
  TextHeight = 13
  object lbLogin: TLabel
    Left = 6
    Top = 72
    Width = 26
    Height = 13
    Caption = 'Login'
    FocusControl = ed_Login
  end
  object lbPass: TLabel
    Left = 6
    Top = 97
    Width = 46
    Height = 13
    Caption = 'Password'
    FocusControl = ed_Password
  end
  object Bevel2: TBevel
    Left = 343
    Top = 2
    Width = 5
    Height = 215
    Anchors = [akTop, akRight]
    Shape = bsLeftLine
  end
  object lbDatabase: TLabel
    Left = 6
    Top = 125
    Width = 46
    Height = 13
    Caption = 'Database'
    FocusControl = cb_Database
  end
  object lbAuth: TLabel
    Left = 6
    Top = 43
    Width = 22
    Height = 13
    Caption = 'Auth'
    WordWrap = True
  end
  object lbServer: TLabel
    Left = 6
    Top = 9
    Width = 31
    Height = 13
    Caption = 'Server'
  end
  object ed_Login: TEdit
    Left = 92
    Top = 69
    Width = 227
    Height = 21
    Cursor = crIBeam
    TabOrder = 1
  end
  object ed_Password: TEdit
    Left = 92
    Top = 94
    Width = 227
    Height = 21
    Cursor = crIBeam
    TabOrder = 2
  end
  object btn_Ok: TButton
    Left = 357
    Top = 5
    Width = 70
    Height = 23
    Action = act_Ok
    Anchors = [akTop, akRight]
    Default = True
    ModalResult = 1
    TabOrder = 5
  end
  object CancelBtn: TButton
    Left = 357
    Top = 31
    Width = 70
    Height = 23
    Anchors = [akTop, akRight]
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 6
  end
  object cb_Database: TComboBox
    Left = 92
    Top = 122
    Width = 227
    Height = 21
    DropDownCount = 30
    Sorted = True
    TabOrder = 3
    OnDropDown = cb_DatabaseDropDown
  end
  object btn_Additional: TButton
    Left = 189
    Top = 156
    Width = 132
    Height = 23
    Caption = 'Additional'
    TabOrder = 4
    OnClick = btn_AdditionalClick
  end
  object cb_Server: TComboBox
    Left = 92
    Top = 7
    Width = 227
    Height = 21
    DropDownCount = 20
    Sorted = True
    TabOrder = 0
    OnChange = cb_ServerChange
    OnDropDown = cb_ServerDropDown
  end
  object Button1: TButton
    Left = 357
    Top = 64
    Width = 70
    Height = 23
    Action = act_Test
    Anchors = [akTop, akRight]
    TabOrder = 7
  end
  object gb_Authorization1: TGroupBox
    Left = 93
    Top = 35
    Width = 226
    Height = 29
    TabOrder = 8
    object rb_SQL_server: TRadioButton
      Left = 7
      Top = 8
      Width = 85
      Height = 19
      Caption = 'SQL Server'
      Checked = True
      TabOrder = 0
      TabStop = True
    end
    object rb_Windows: TRadioButton
      Left = 95
      Top = 8
      Width = 94
      Height = 18
      Caption = 'Windows'
      TabOrder = 1
    end
  end
  object Button2: TButton
    Left = 7
    Top = 156
    Width = 60
    Height = 23
    Action = act_Templates
    TabOrder = 9
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 378
    Top = 126
    object act_Ok: TAction
      Caption = 'Ok'
    end
    object act_Test: TAction
      Caption = #1058#1077#1089#1090
      OnExecute = act_Database_RefreshExecute
    end
    object act_Templates: TAction
      Caption = #1064#1072#1073#1083#1086#1085#1099
      OnExecute = act_Database_RefreshExecute
    end
  end
end
