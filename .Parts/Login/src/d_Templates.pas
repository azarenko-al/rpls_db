unit d_Templates;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, DBGrids, RxMemDS, ActnList, ToolWin, ComCtrls,
  StdCtrls, Mask, rxToolEdit,
  cxVGrid, cxControls, cxInplaceContainer, rxPlacemnt, ExtCtrls,


//  I_DB_login,

  u_db_ini1,
  u_db,

  u_SQL,

  u_func,


  Menus, System.Actions
  ;

type
  Tdlg_Templates = class(TForm)
    RxMemoryData1: TRxMemoryData;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    RxMemoryData1server: TStringField;
    RxMemoryData1database: TStringField;
    RxMemoryData1login: TStringField;
    ActionList1: TActionList;
    act_Add: TAction;
    Button1: TButton;
    Button2: TButton;
    RxMemoryData1password: TStringField;
    act_Select: TAction;
    CheckBox1: TCheckBox;
    Panel1: TPanel;
    Panel2: TPanel;
    btn_Ok: TButton;
    Button4: TButton;
    FilenameEdit1: TFilenameEdit;
    act_Del: TAction;
    PopupMenu1: TPopupMenu;
    actDel1: TMenuItem;
    Button5: TButton;
    RxMemoryData1auth: TStringField;
    FormPlacement1: TFormPlacement;
    procedure FormDestroy(Sender: TObject);
    procedure act_DelExecute(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
//    procedure Button1Click(Sender: TObject);
   // procedure Button2Click(Sender: TObject);
//    procedure DBGrid1DrawDataCell(Sender: TObject; const Rect: TRect; Field:
  //      TField; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
  private
    FIniFileName : string;

    FLoginRec: TdbLoginRec;

    FDataset: TDataset;

    procedure Add;
    function DatabaseToRecord(var aLoginRec: TdbLoginRec): boolean;
    function Find(aLoginRec: TdbLoginRec): Boolean;

    //procedure GetSelectedRec(var aLoginRec: TdbLoginRec);

    { Private declarations }
  public
    class function ExecDlg(var aLoginRec: TdbLoginRec): boolean;

  end;


implementation
{$R *.dfm}


const
  FLD_AUTH = 'AUTH';



//-------------------------------------------------------
class function Tdlg_Templates.ExecDlg(var aLoginRec: TdbLoginRec): boolean;
//-------------------------------------------------------
var
 // iInd: Integer;
  S: string;
 // sNetworkLibrary, sNetworkAddress: string;
begin
  with Tdlg_Templates.Create(Application) do
  begin
    FLoginRec:=aLoginRec;
    Find (FLoginRec);

    Result:= (ShowModal=mrOk);

    if Result then
      DatabaseToRecord(aLoginRec);


    Free;
  end;    
end;

//--------------------------------------------------------
procedure Tdlg_Templates.FormCreate(Sender: TObject);
//--------------------------------------------------------
const
  FLD_SERVER   ='SERVER';
  FLD_DATABASE ='DATABASE';
  FLD_LOGIN    ='LOGIN';
  FLD_PASSWORD ='PASSWORD';
  FLD_AUTH = 'AUTH';

begin
  Caption:='�������';

  DBGrid1.Columns[0].Title.Caption:='������';
  DBGrid1.Columns[1].Title.Caption:='���� ������';
  DBGrid1.Columns[2].Title.Caption:='�����';
  DBGrid1.Columns[3].Title.Caption:='������';
  DBGrid1.Columns[4].Title.Caption:='�����������';


  DBGrid1.Align:=alCLient;


  FIniFileName:=FilenameEdit1.FileName;


  FDataset:=RxMemoryData1;

{
  db_CreateField (rxmemoryData1,
    [
      db_Field(FLD_SERVER,   ftString, 50),
      db_Field(FLD_DATABASE, ftString, 50),
      db_Field(FLD_LOGIN,    ftString),
      db_Field(FLD_PASSWORD, ftString),
      db_Field(FLD_AUTH,     ftString)
     ] );

}

 // ShowMessage(Application.ExeName);

  FIniFileName:=ChangeFileExt(Application.ExeName, '.templates.ini');
//  FIniFileName:='d:\templates.ini';


  db_LoadDataFromIni(FDataset, FIniFileName, 'main');

end;


procedure Tdlg_Templates.FormDestroy(Sender: TObject);
begin
  db_PostDataset(FDataset);

  db_SaveDataToIni(FDataset, FIniFileName, 'main');

end;

//-------------------------------------------------------
function Tdlg_Templates.DatabaseToRecord(var aLoginRec: TdbLoginRec): boolean;
//-------------------------------------------------------
var
  s: string;
begin
  result := False;

  with FDataset do
    if not IsEmpty then
    begin
      aLoginRec.Server  := FieldByName(FLD_SERVER).AsString;
      aLoginRec.DataBase:= FieldByName(FLD_DataBase).AsString;
      aLoginRec.Login   := FieldByName(FLD_Login).AsString;
      aLoginRec.Password:= FieldByName(FLD_Password).AsString;

      s:=FieldByName(FLD_AUTH).AsString;
      aLoginRec.IsUseWinAuth:= Eq(s,'windows');

      result := True;

    end;

end;


//-------------------------------------------------------
procedure Tdlg_Templates.Add;
//-------------------------------------------------------
begin
  Assert(Assigned( FDataset));

  if not Find(FLoginRec) then
    db_AddRecord_ (FDataset,
     [
      FLD_SERVER,   FLoginRec.Server,
      FLD_DataBase, FLoginRec.DataBase,
      FLD_Login,    FLoginRec.Login,
      FLD_Password, FLoginRec.Password,

      FLD_AUTH, IIF(FLoginRec.IsUseWinAuth, 'windows', 'sql server')

  //          s:=FieldByName(FLD_AUTH).AsString;
  //      aLoginRec.IsUseWinAuth:= Eq(s,'windows');


     ]);

end;


//-------------------------------------------------------
function Tdlg_Templates.Find(aLoginRec: TdbLoginRec): Boolean;
//-------------------------------------------------------
begin
  result := FDataset.Locate (FLD_SERVER +';'+ FLD_DataBase,
     VarArrayOf([aLoginRec.Server, aLoginRec.DataBase]), []);

end;


procedure Tdlg_Templates.act_DelExecute(Sender: TObject);
begin
  if Sender=act_Del then
    FDataset.Delete;

  if Sender=act_Add then
    Add();

end;

procedure Tdlg_Templates.DBGrid1DblClick(Sender: TObject);
begin
  btn_Ok.Click;
end;


end.
