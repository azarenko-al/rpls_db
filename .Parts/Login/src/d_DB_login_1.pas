unit d_DB_Login_1;

interface

uses SysUtils, Classes, Controls, Forms, Dialogs, StdCtrls, ExtCtrls,
  Db, ADODB, ActnList, Graphics,

  i_db_login,

  d_DB_ConnectAdditionalParams_,
  u_sql,

  dm_Connection,

  u_func
  ;

type

  Tdlg_DB_Login1 = class(TForm)
    lbLogin: TLabel;
    lbPass: TLabel;
    ed_Login: TEdit;
    ed_Password: TEdit;
    btn_Ok: TButton;
    CancelBtn: TButton;
    Bevel2: TBevel;
    lbDatabase: TLabel;
    lbAuth: TLabel;
    cb_Database: TComboBox;
    lbServer: TLabel;
    Bevel1: TBevel;
    ActionList1: TActionList;
    act_Ok: TAction;
    btn_Additional: TButton;
    rg_Auth: TRadioGroup;
    Bevel3: TBevel;
    cb_Server: TComboBox;
    Button1: TButton;
    act_Test: TAction;
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_Database_Refresh111Execute(Sender: TObject);
    procedure act_TestExecute(Sender: TObject);
    procedure btn_AdditionalClick(Sender: TObject);
    procedure cb_Auth1Change(Sender: TObject);
    procedure cb_DatabaseDropDown(Sender: TObject);
    procedure cb_ServerDropDown(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ed_ServerButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure rg_AuthClick(Sender: TObject);
  private
    FLoginRec: TdbLoginRec;

    procedure CheckConnect;
    procedure LoadEditsFromRec;
    procedure Refresh_Database_List;
    procedure SaveEditsToRec();
    procedure Select_Server_Name;

  public
    class function ExecDlg(var aLoginRec: TdbLoginRec): boolean;
  end;



//=================================================================
implementation {$R *.DFM}
//=================================================================


//-----------------------------------------------------------------
class function Tdlg_DB_Login1.ExecDlg(var aLoginRec: TdbLoginRec): boolean;
//-----------------------------------------------------------------
begin
  with Tdlg_DB_Login1.Create(Application) do
  begin
    FLoginRec :=aLoginRec;

    LoadEditsFromRec();

    ShowModal;

    Result := ModalResult=mrOk;
    if Result then
    begin
      SaveEditsToRec();
      aLoginRec:=FLoginRec;
    end;

    Free;
  end;
end;


//-------------------------------------------------------------------
procedure Tdlg_DB_Login1.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
(*  FRegPath:=REGISTRY_COMMON_FORMS + ClassName + '\';

  FormStorage1.IniFileName:=FRegPath;
  FormStorage1.Active := True;
*)
  //Width:=493;

  Caption:='����������� � ��';

  lbLogin.Caption   :='������������';
  lbPass.Caption    :='������';
  LbServer.Caption  :='������';
  lbDatabase.Caption:='���� ������';
  lbAuth.Caption    :='�����������';


//  cb_ShowOnStart.Caption:='���������� ��� ������';
//  btn_Additional.Caption:='�������������';
  btn_Additional.Caption:='������� ����������...';

end;


procedure Tdlg_DB_Login1.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
begin
  btn_Ok.Enabled:=  (Trim(cb_Server.Text)<>'') and
                    (Trim(cb_Database.Text)<>'');
end;

//--------------------------------------------------------------------
procedure Tdlg_DB_Login1.SaveEditsToRec;
//--------------------------------------------------------------------
begin
  with FLoginRec do
  begin
    Server      :=cb_Server.Text;
    Login       :=ed_Login.Text;
    Password    :=ed_Password.Text;
    Database    :=cb_Database.Text;

    IsUseWinAuth:=rg_Auth.ItemIndex=1;
  end;
end;

//--------------------------------------------------------------------
procedure Tdlg_DB_Login1.LoadEditsFromRec;
//--------------------------------------------------------------------
begin
  with FLoginRec do
  begin
    cb_Server.Text:=      Server;
    ed_Login.Text:=       Login;
    ed_Password.Text:=    Password;
    cb_Database.Text:=    Database;

    rg_Auth.ItemIndex:=IIF(IsUseWinAuth,1,0);
  end;
end;


//-----------------------------------------------------------------
procedure Tdlg_DB_Login1.act_Database_Refresh111Execute(Sender: TObject);
//-----------------------------------------------------------------
begin
  Refresh_Database_List;
end;

//----------------------------------------------------------------------
procedure Tdlg_DB_Login1.act_TestExecute(Sender: TObject);
begin
  if Sender=act_Test then
    CheckConnect;
end;


procedure Tdlg_DB_Login1.btn_AdditionalClick(Sender: TObject);
begin
  TDlg_DB_ConnectAdditionalParams_.ExecDlg(FLoginRec);
end;

procedure Tdlg_DB_Login1.cb_Auth1Change(Sender: TObject);
begin
  ed_Login.Enabled:=rg_Auth.ItemIndex=0;
  ed_Password.Enabled:=rg_Auth.ItemIndex=0;

end;

procedure Tdlg_DB_Login1.cb_DatabaseDropDown(Sender: TObject);
begin
  refresh_Database_List;
end;

procedure Tdlg_DB_Login1.cb_ServerDropDown(Sender: TObject);
begin
  if cb_Server.Items.Count=0 then
    db_ListAvailableSQLServers(cb_Server.Items);
end;

//----------------------------------------------------------------------
procedure Tdlg_DB_Login1.CheckConnect;
//----------------------------------------------------------------------
begin
  SaveEditsToRec();

  dmConnection1.CheckConnect(FLoginRec);
end;

//----------------------------------------------------------------------
procedure Tdlg_DB_Login1.ed_ServerButtonClick(Sender: TObject; AbsoluteIndex: Integer);
//----------------------------------------------------------------------
begin
  Select_Server_Name();
end;


//----------------------------------------------------------------------
procedure Tdlg_DB_Login1.Select_Server_Name;
//----------------------------------------------------------------------
var  sName : string;
begin
  sName := db_Dlg_GetSqlServerName_old (Self.Handle);

  if sName<>'' then
  begin
    cb_Server.Text := sName;
    Refresh_Database_List;
  end;
end;


//-----------------------------------------------------------------
procedure Tdlg_DB_Login1.Refresh_Database_List;
//-----------------------------------------------------------------
begin
  SaveEditsToRec();

  dmConnection1.SQLServer_GetDatabaseList(FLoginRec, cb_Database.Items);

  if cb_Database.Items.IndexOf(cb_Database.Text)=-1 then
    cb_Database.Text:= '';
end;


procedure Tdlg_DB_Login1.rg_AuthClick(Sender: TObject);
begin
  ed_Login.Enabled:=rg_Auth.ItemIndex=0;
  ed_Password.Enabled:=rg_Auth.ItemIndex=0;

  ed_Login.Color   :=IIF(rg_Auth.ItemIndex=0, clWindow, Color);
  ed_Password.Color:=IIF(rg_Auth.ItemIndex=0, clWindow, Color);
end;


end.


