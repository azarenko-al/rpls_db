unit d_DB_ConnectAdditionalParams;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms, 
  ActnList, StdCtrls, ExtCtrls, 

  d_Wizard,
  I_DB_login,
//  u_db_login,
  u_func,

  cxPropertiesStore, rxPlacemnt

  ;

type

  Tdlg_DB_ConnectAdditionalParams = class(Tdlg_Wizard)
    rg_LIbraries: TRadioGroup;
    GroupBox1: TGroupBox;
    lb_ServerName: TLabel;
    cb_DynamicPort: TCheckBox;
    ed_ServerName: TEdit;
    lb_PortNumber: TLabel;
    ed_PortNumber: TEdit;
    procedure cb_DynamicPortClick(Sender: TObject);
    procedure rg_LIbrariesClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);

  public
    class function ExecDlg (var aLoginRec: TdbLoginRec ): boolean;
  end;


//=======================================================
implementation {$R *.DFM}
//=======================================================

//-------------------------------------------------------
class function Tdlg_DB_ConnectAdditionalParams.ExecDlg( var aLoginRec: TdbLoginRec  ): boolean;
//-------------------------------------------------------
var
  iInd: Integer;
  S: string;
  sNetworkLibrary, sNetworkAddress: string;
begin
  with Tdlg_DB_ConnectAdditionalParams.Create(Application) do
  begin

    sNetworkLibrary:=aLoginRec.NetworkLibrary;
    sNetworkAddress:=aLoginRec.NetworkAddress;


    if Eq(sNetworkLibrary, 'DBNMPNTW') then
      rg_Libraries.ItemIndex:=0
    else

    if Eq(sNetworkLibrary, 'DBMSSOCN') then
      rg_Libraries.ItemIndex:=1
    else
    ;

    if rg_Libraries.ItemIndex = 1 then
    begin
      iInd:= Pos(',', sNetworkAddress);

      if iInd>0 then begin
        ed_ServerName.Text:= Copy(sNetworkAddress, 1, iInd-1);
        if Length(sNetworkAddress) <> iInd then
        begin
          ed_PortNumber.Text:= Copy(sNetworkAddress, iInd+1, Length(sNetworkAddress)-iInd);
          cb_DynamicPort.Checked:= false;
        end;
      end else

      if iInd = 0 then
        ed_ServerName.Text:= sNetworkAddress;
    end;

    rg_LibrariesClick(nil);
    cb_DynamicPortClick(nil);

    Result:= (ShowModal=mrOk);

    case rg_Libraries.ItemIndex of    //
      0: begin
           aLoginRec.NetworkLibrary:='DBNMPNTW';
           aLoginRec.NetworkAddress:=aLoginRec.Server;
       //    aParamsStr:= 'DBNMPNTW,'+aServer;
         end;

      //TCP-IP
      1: begin
           aLoginRec.NetworkLibrary:='DBMSSOCN';
           aLoginRec.NetworkAddress:=Trim(ed_ServerName.Text);

           if not cb_DynamicPort.Checked then
             aLoginRec.NetworkAddress:=aLoginRec.NetworkAddress+ ','+ed_PortNumber.Text;
         end;
    end;


    Free;
  end;    // with
end;

//-------------------------------------------------------
procedure Tdlg_DB_ConnectAdditionalParams.FormCreate(Sender: TObject);
//-------------------------------------------------------
begin
  inherited;
  Caption:= 'Настройка клиента';
  SetActionName('Изменение конфигурации сетевой библиотеки');
end;

//-------------------------------------------------------
procedure Tdlg_DB_ConnectAdditionalParams.cb_DynamicPortClick(Sender: TObject);
//-------------------------------------------------------
begin
  lb_PortNumber.Enabled:= not cb_DynamicPort.Checked;
  ed_PortNumber.Enabled:= not cb_DynamicPort.Checked;
  ed_PortNumber.Color:= IIF(ed_PortNumber.Enabled, clWindow, clInactiveCaptionText);
end;

//-------------------------------------------------------
procedure Tdlg_DB_ConnectAdditionalParams.rg_LibrariesClick(Sender: TObject);
//-------------------------------------------------------

  procedure DoHideAndShowTCPIP(aShow: boolean);
  begin
    lb_ServerName.Visible:= aShow;
    ed_ServerName.Visible:= aShow;
    lb_PortNumber.Visible:= aShow;
    ed_PortNumber.Visible:= aShow;
    cb_DynamicPort.Visible:= aShow;
  end;

begin
  case rg_Libraries.ItemIndex of    //
    0: DoHideAndShowTCPIP(false);
    1: DoHideAndShowTCPIP(true);
  end;    // case
end;

//-------------------------------------------------------
procedure Tdlg_DB_ConnectAdditionalParams.ActionList1Update(
  Action: TBasicAction; var Handled: Boolean);
//-------------------------------------------------------
begin
  case rg_Libraries.ItemIndex of
    1: act_Ok.Enabled:= (ed_ServerName.Text<>'');
  end;
end;

end.






