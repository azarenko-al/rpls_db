unit d_DB_ConnectAdditionalParams_;

interface

uses
  SysUtils, Variants, Classes, Graphics, Controls, Forms,
  StdCtrls, ExtCtrls, ActnList,

 // I_db_login,

  u_func, System.Actions

  , u_SQL;

type
  Tdlg_DB_ConnectAdditionalParams_ = class(TForm)
    rg_LIbraries: TRadioGroup;
    GroupBox1: TGroupBox;
    lb_ServerName: TLabel;
    lb_PortNumber: TLabel;
    cb_DynamicPort: TCheckBox;
    ed_ServerName: TEdit;
    ed_PortNumber: TEdit;
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    ActionList1: TActionList;
    act_Ok: TAction;
    Panel1: TPanel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_OkExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cb_DynamicPortClick(Sender: TObject);
    procedure rg_LIbrariesClick(Sender: TObject);
  private
    { Private declarations }
  public
    class function ExecDlg(var aLoginRec: TdbLoginRec): boolean;
  end;

//var
//  dlg_DB_ConnectAdditionalParams_: Tdlg_DB_ConnectAdditionalParams_;

implementation

{$R *.dfm}

//-------------------------------------------------------
class function Tdlg_DB_ConnectAdditionalParams_.ExecDlg(var aLoginRec:
    TdbLoginRec): boolean;
//-------------------------------------------------------
var
  iInd: Integer;
  S: string;
  sNetworkLibrary, sNetworkAddress: string;
begin
  with Tdlg_DB_ConnectAdditionalParams_.Create(Application) do
  begin

    sNetworkLibrary:=aLoginRec.NetworkLibrary;
    sNetworkAddress:=aLoginRec.NetworkAddress;


    if Eq(sNetworkLibrary, 'DBNMPNTW') then
      rg_Libraries.ItemIndex:=0
    else

    if Eq(sNetworkLibrary, 'DBMSSOCN') then
      rg_Libraries.ItemIndex:=1
    else
    ;

    if rg_Libraries.ItemIndex = 1 then
    begin
      iInd:= Pos(',', sNetworkAddress);

      if iInd>0 then begin
        ed_ServerName.Text:= Copy(sNetworkAddress, 1, iInd-1);
        if Length(sNetworkAddress) <> iInd then
        begin
          ed_PortNumber.Text:= Copy(sNetworkAddress, iInd+1, Length(sNetworkAddress)-iInd);
          cb_DynamicPort.Checked:= false;
        end;
      end else

      if iInd = 0 then
        ed_ServerName.Text:= sNetworkAddress;
    end;

    rg_LibrariesClick(nil);
    cb_DynamicPortClick(nil);

    Result:= (ShowModal=mrOk);

    case rg_Libraries.ItemIndex of    //
      0: begin
           aLoginRec.NetworkLibrary:='DBNMPNTW';
           aLoginRec.NetworkAddress:=aLoginRec.Server;
       //    aParamsStr:= 'DBNMPNTW,'+aServer;
         end;

      //TCP-IP
      1: begin
           aLoginRec.NetworkLibrary:='DBMSSOCN';
           aLoginRec.NetworkAddress:=Trim(ed_ServerName.Text);

           if not cb_DynamicPort.Checked then
             aLoginRec.NetworkAddress:=aLoginRec.NetworkAddress+ ','+ed_PortNumber.Text;
         end;
    end;


    Free;
  end;    // with
end;


procedure Tdlg_DB_ConnectAdditionalParams_.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
begin
  case rg_Libraries.ItemIndex of
    1: act_Ok.Enabled:= (ed_ServerName.Text<>'');
  end;
end;

procedure Tdlg_DB_ConnectAdditionalParams_.act_OkExecute(Sender: TObject);
begin
//!!!!!!!!!!!
end;


procedure Tdlg_DB_ConnectAdditionalParams_.FormCreate(Sender: TObject);
begin
 // Caption:= 'Настройка клиента';
//  SetActionName('Изменение конфигурации сетевой библиотеки');

end;

procedure Tdlg_DB_ConnectAdditionalParams_.cb_DynamicPortClick(Sender: TObject);
begin
  lb_PortNumber.Enabled:= not cb_DynamicPort.Checked;
  ed_PortNumber.Enabled:= not cb_DynamicPort.Checked;
  ed_PortNumber.Color:= IIF(ed_PortNumber.Enabled, clWindow, Color);
end;


procedure Tdlg_DB_ConnectAdditionalParams_.rg_LIbrariesClick(Sender: TObject);
(*
  procedure DoHideAndShowTCPIP(aShow: boolean);
  begin
//    lb_ServerName.Visible:= aShow;
    ed_ServerName.Enabled:= aShow;
 //   lb_PortNumber.Visible:= aShow;
    ed_PortNumber.Enabled:= aShow;
    cb_DynamicPort.Enabled:= aShow;

   // ed_ServerName

  end;*)

var
  b: Boolean;
begin
  b := rg_Libraries.ItemIndex=1;


  ed_ServerName.Enabled:= b;
//   lb_PortNumber.Visible:= aShow;
  ed_PortNumber.Enabled:= b;
  cb_DynamicPort.Enabled:= b;


//  DoHideAndShowTCPIP(rg_Libraries.ItemIndex=1);

(*  case rg_Libraries.ItemIndex of
    0: DoHideAndShowTCPIP(false);
    1: DoHideAndShowTCPIP(true);
  end;
*)
end;



end.

