object frame_DB_Login: Tframe_DB_Login
  Left = 0
  Top = 0
  Width = 412
  Height = 196
  TabOrder = 0
  OnClick = FrameClick
  object lbLogin: TLabel
    Left = 8
    Top = 11
    Width = 26
    Height = 13
    Caption = 'Login'
    FocusControl = ed_Login
  end
  object lbPass: TLabel
    Left = 8
    Top = 35
    Width = 46
    Height = 13
    Caption = 'Password'
    FocusControl = ed_Password
  end
  object lbDatabase: TLabel
    Left = 8
    Top = 99
    Width = 46
    Height = 13
    Caption = 'Database'
    FocusControl = cb_Database
  end
  object lbAuth: TLabel
    Left = 8
    Top = 131
    Width = 22
    Height = 13
    Caption = 'Auth'
    WordWrap = True
  end
  object lbServer: TLabel
    Left = 8
    Top = 75
    Width = 31
    Height = 13
    Caption = 'Server'
  end
  object Bevel1: TBevel
    Left = 8
    Top = 59
    Width = 333
    Height = 5
    Shape = bsTopLine
  end
  object ed_Login: TEdit
    Left = 91
    Top = 3
    Width = 250
    Height = 21
    Cursor = crIBeam
    TabOrder = 0
  end
  object ed_Password: TEdit
    Left = 91
    Top = 31
    Width = 250
    Height = 21
    Cursor = crIBeam
    TabOrder = 1
  end
  object cb_Database: TComboBox
    Left = 91
    Top = 95
    Width = 251
    Height = 21
    DropDownCount = 20
    ItemHeight = 13
    Sorted = True
    TabOrder = 3
    OnDropDown = cb_DatabaseDropDown
  end
  object ed_Server: TComboEdit
    Left = 91
    Top = 67
    Width = 251
    Height = 21
    GlyphKind = gkEllipsis
    ButtonWidth = 16
    NumGlyphs = 1
    TabOrder = 2
    OnButtonClick = ed_ServerButtonClick
  end
  object rg_Auth: TRadioGroup
    Left = 91
    Top = 118
    Width = 182
    Height = 35
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'SQL Server'
      'Windows')
    TabOrder = 4
    OnClick = rg_AuthClick
  end
  object btn_Additional: TButton
    Left = 236
    Top = 162
    Width = 69
    Height = 23
    Caption = 'Additional'
    TabOrder = 5
  end
end
