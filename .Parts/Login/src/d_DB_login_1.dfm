object dlg_DB_Login1: Tdlg_DB_Login1
  Left = 604
  Top = 468
  Cursor = crArrow
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'dlg_DB_Login1'
  ClientHeight = 230
  ClientWidth = 490
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 120
  TextHeight = 13
  object lbLogin: TLabel
    Left = 12
    Top = 90
    Width = 26
    Height = 13
    Caption = 'Login'
    FocusControl = ed_Login
  end
  object lbPass: TLabel
    Left = 12
    Top = 110
    Width = 46
    Height = 13
    Caption = 'Password'
    FocusControl = ed_Password
  end
  object Bevel2: TBevel
    Left = 356
    Top = 0
    Width = 4
    Height = 200
    Shape = bsLeftLine
  end
  object lbDatabase: TLabel
    Left = 12
    Top = 147
    Width = 46
    Height = 13
    Caption = 'Database'
    FocusControl = cb_Database
  end
  object lbAuth: TLabel
    Left = 12
    Top = 57
    Width = 22
    Height = 13
    Caption = 'Auth'
    WordWrap = True
  end
  object lbServer: TLabel
    Left = 12
    Top = 11
    Width = 31
    Height = 13
    Caption = 'Server'
  end
  object Bevel1: TBevel
    Left = 12
    Top = 38
    Width = 333
    Height = 5
    Shape = bsTopLine
  end
  object Bevel3: TBevel
    Left = 12
    Top = 134
    Width = 333
    Height = 5
    Shape = bsTopLine
  end
  object ed_Login: TEdit
    Left = 96
    Top = 81
    Width = 249
    Height = 21
    Cursor = crIBeam
    TabOrder = 2
  end
  object ed_Password: TEdit
    Left = 95
    Top = 106
    Width = 250
    Height = 21
    Cursor = crIBeam
    TabOrder = 3
  end
  object btn_Ok: TButton
    Left = 372
    Top = 4
    Width = 75
    Height = 25
    Action = act_Ok
    Default = True
    ModalResult = 1
    TabOrder = 6
  end
  object CancelBtn: TButton
    Left = 372
    Top = 36
    Width = 75
    Height = 25
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 7
  end
  object cb_Database: TComboBox
    Left = 95
    Top = 143
    Width = 250
    Height = 21
    DropDownCount = 20
    ItemHeight = 13
    Sorted = True
    TabOrder = 4
    OnDropDown = cb_DatabaseDropDown
  end
  object btn_Additional: TButton
    Left = 240
    Top = 170
    Width = 105
    Height = 23
    Caption = 'Additional'
    TabOrder = 5
    OnClick = btn_AdditionalClick
  end
  object rg_Auth: TRadioGroup
    Left = 95
    Top = 41
    Width = 186
    Height = 35
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'SQL Server'
      'Windows')
    TabOrder = 1
    OnClick = rg_AuthClick
  end
  object cb_Server: TComboBox
    Left = 95
    Top = 8
    Width = 250
    Height = 21
    DropDownCount = 20
    ItemHeight = 13
    Sorted = True
    TabOrder = 0
    OnDropDown = cb_ServerDropDown
  end
  object Button1: TButton
    Left = 368
    Top = 170
    Width = 75
    Height = 23
    Action = act_Test
    TabOrder = 8
  end
  object ActionList1: TActionList
    Left = 424
    Top = 76
    object act_Ok: TAction
      Caption = 'Ok'
    end
    object act_Test: TAction
      Caption = #1058#1077#1089#1090
      OnExecute = act_TestExecute
    end
  end
end
