unit d_DB_Login;

interface

uses SysUtils, Classes, Controls, Forms, Dialogs, StdCtrls, ExtCtrls,
  Db, ADODB, ActnList, Graphics,   ADOInt,

  d_DB_ConnectAdditionalParams_,

//  I_DB_login,

  d_Templates,

  dm_Connection,

 // u_db_login,
  u_func,
  u_reg,

  u_sql, 


  Menus, System.Actions
  ;

type

  Tdlg_DB_Login = class(TForm)
    lbLogin: TLabel;
    lbPass: TLabel;
    ed_Login: TEdit;
    ed_Password: TEdit;
    btn_Ok: TButton;
    CancelBtn: TButton;
    Bevel2: TBevel;
    lbDatabase: TLabel;
    lbAuth: TLabel;
    cb_Database: TComboBox;
    lbServer: TLabel;
    ActionList1: TActionList;
    act_Ok: TAction;
    btn_Additional: TButton;
    cb_Server: TComboBox;
    Button1: TButton;
    act_Test: TAction;
    gb_Authorization1: TGroupBox;
    rb_SQL_server: TRadioButton;
    rb_Windows: TRadioButton;
    Button2: TButton;
    act_Templates: TAction;
    procedure FormDestroy(Sender: TObject);
//    procedure FormDestroy(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_Database_RefreshExecute(Sender: TObject);

//    procedure act_OkExecute(Sender: TObject);
    procedure btn_AdditionalClick(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
//    procedure Button3Click(Sender: TObject);
    procedure cb_DatabaseDropDown(Sender: TObject);
    procedure cb_ServerChange(Sender: TObject);
    procedure cb_ServerDropDown(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ed_ServerButtonClick(Sender: TObject; AbsoluteIndex: Integer);
//    procedure N11Click(Sender: TObject);
//    procedure N21Click(Sender: TObject);
//    procedure onega2013rfpandlink1Click(Sender: TObject);
    procedure rg_Auth1Click(Sender: TObject);
//    procedure sdf1Click(Sender: TObject);
//    procedure test2onegalink20121Click(Sender: TObject);
  private
    FLoginRec: TdbLoginRec;
    FRegPath: string;

    FConnectionObject: _Connection;
    FADOConnection: TADOConnection;

    procedure Connect;

    procedure CheckConnect;
    procedure Connect_ADOConnection;
    procedure LoadFromReg(aRegPath: string);
    procedure Database_List_Refresh;
    procedure LoadFromRec(aRec: TdbLoginRec);

    procedure SaveEditsToClass(var aLoginRec: TdbLoginRec);

    procedure Select_Server_Name;
//    procedure Set1;
//    procedure Set2;
  public

    class function ExecDlg_ini(aIniFileName: string; var aLoginRec: TdbLoginRec;
        aConnectionObject: _Connection): Boolean;

    class function ExecDlg_ADOConn1(aRegPath: string; var aLoginRec: TdbLoginRec;
        aConnection: TADOConnection): boolean;

    class function ExecDlg (aRegPath: string; var aLoginRec: TdbLoginRec; aConnectionObject: _Connection ): boolean;
  end;


//=================================================================
implementation {$R *.DFM}
//=================================================================

const
  REGISTRY_COMMON_FORMS = 'Software\Onega\Common\Forms\';



// ---------------------------------------------------------------
class function Tdlg_DB_Login.ExecDlg_ADOConn1(aRegPath: string; var aLoginRec:  TdbLoginRec; aConnection: TADOConnection): boolean;
// ---------------------------------------------------------------
begin
  with Tdlg_DB_Login.Create(Application) do
  begin
    FRegPath:=aRegPath;

    FADOConnection :=aConnection;

    LoadFromReg (aRegPath);

    ShowModal;

    Result := ModalResult=mrOk;

    if Result then
    begin
      Connect_ADOConnection;
      aLoginRec:=FLoginRec;
    end;

    Free;
  end;
end;

// ---------------------------------------------------------------
//class function Tdlg_DB_Login.ExecDlg_ADOConn1_ini(var aLoginRec: TdbLoginRec; aAdoConnection: TADOConnection): Boolean;
class function Tdlg_DB_Login.ExecDlg_ini(aIniFileName: string; var aLoginRec: TdbLoginRec; aConnectionObject: _Connection): Boolean;
// ---------------------------------------------------------------
begin
  with Tdlg_DB_Login.Create(Application) do
  begin
   // FRegPath:=aRegPath;
    FConnectionObject :=aConnectionObject;

//    FADOConnection :=aAdoConnection;

//    db_LoginRec_LoadFromIni(aIniFileName, );

    FLoginRec:=aLoginRec;

    LoadFromRec (FLoginRec);

//    LoadFromReg (aRegPath);

    ShowModal;

    Result := ModalResult=mrOk;

    if Result then
    begin
      Connect_ADOConnection;
      aLoginRec:=FLoginRec;
    end;

    Free;
  end;
end;

//-----------------------------------------------------------------
class function Tdlg_DB_Login.ExecDlg (
                                      aRegPath: string;
                                      var aLoginRec: TdbLoginRec;
                                      aConnectionObject: _Connection
                                      ): boolean;
//-----------------------------------------------------------------
begin

  with Tdlg_DB_Login.Create(Application) do
  begin
    FRegPath  := aRegPath;
    FLoginRec := aLoginRec;

    FConnectionObject :=aConnectionObject;

    LoadFromReg (aRegPath);

    ShowModal;

    Result := ModalResult=mrOk;
    if Result then
    begin
      Connect;
  //  begin
      aLoginRec:=FLoginRec;
   // end;
    end;

    Free;
  end;

 // Application.Handle := FOldHandle;
end;

//-------------------------------------------------------------------
procedure Tdlg_DB_Login.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
//  FRegPath:=REGISTRY_COMMON_FORMS + ClassName + '\';

//  FormStorage1.IniFileName:=FRegPath;
//  FormStorage1.Active := True;

//  Height:=250;

  Height:=270;
  Width :=480;// 460;

  Caption:='����������� � ��';// + GetAppVersionStr();
//  Caption:='����������� � ��' + GetAppVersionStr();

  lbLogin.Caption   :='������������';
  lbPass.Caption    :='������';
  LbServer.Caption  :='������';
  lbDatabase.Caption:='���� ������';
  lbAuth.Caption    :='�����������';

(*  cb_Auth.Items.Clear;
  cb_Auth.Items.Add('������������ ������� ������ SQL Server');
  cb_Auth.Items.Add('������������ ������� ������ Windows');
*)


 // cb_ShowOnStart.Caption:='���������� ��� ������';
  btn_Additional.Caption:='�������������';

end;

//-----------------------------------------------------------------
procedure Tdlg_DB_Login.LoadFromReg(aRegPath: string);
//-----------------------------------------------------------------
begin
//  db_LoginRec_LoadFromReg (FRegPath, FLoginRec);
  db_LoginRec_LoadFromReg (aRegPath, FLoginRec);

  LoadFromRec (FLoginRec);

//  with FLoginRec do
//  begin
//    cb_Server.Text    := Server;
//    ed_Login.Text     := Login;
//    ed_Password.Text  := Password;
//    cb_Database.Text  := Database;
//
//    rb_Windows.Checked := IsUseWinAuth;
//
//  end;


//  cb_ShowOnStart.Checked:=reg_ReadBool (FRegPath, 'IsShowDialog', True);
//  cb_ShowOnStart.Checked:=reg_ReadBool (aRegPath, 'IsShowDialog', True);

end;

//-----------------------------------------------------------------
procedure Tdlg_DB_Login.LoadFromRec(aRec: TdbLoginRec);
//-----------------------------------------------------------------
begin
//  db_LoginRec_LoadFromReg (FRegPath, FLoginRec);
//  db_LoginRec_LoadFromReg (aRegPath, FLoginRec);

  cb_Server.Text    := aRec.Server;
  ed_Login.Text     := aRec.Login;
  ed_Password.Text  := aRec.Password;
  cb_Database.Text  := aRec.Database;

  rb_Windows.Checked := aRec.IsUseWinAuth;



//  cb_ShowOnStart.Checked:=reg_ReadBool (FRegPath, 'IsShowDialog', True);
//  cb_ShowOnStart.Checked:=reg_ReadBool (aRegPath, 'IsShowDialog', True);

end;



// ---------------------------------------------------------------
procedure Tdlg_DB_Login.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
// ---------------------------------------------------------------
begin
  btn_Ok.Enabled:=  (Trim(cb_Server.Text)<>'') and
                    (Trim(cb_Database.Text)<>'');


  ed_Login.ReadOnly := rb_Windows.Checked;
  ed_Password.ReadOnly := rb_Windows.Checked;


  if not rb_Windows.Checked then
  begin
    ed_Login.Color :=clWindow;
    ed_Password.Color :=clWindow;
  end else
  begin
    ed_Login.ParentColor:=True;
    ed_Password.ParentColor:=True;

//    ComboEdit1.ParentColor:=True;

  end;

 // act_Ok.enabled :=(not rb_BD.Checked) or
  //                 (rb_BD.Checked and (FLinkTypeID>0));


end;

//--------------------------------------------------------------------
procedure Tdlg_DB_Login.SaveEditsToClass(var aLoginRec: TdbLoginRec);
//--------------------------------------------------------------------

//FLoginRec: TdbLoginRec;

begin
//  with FLoginRec do
  with aLoginRec do
  begin
    Server      :=cb_Server.Text;
    Login       :=ed_Login.Text;
    Password    :=ed_Password.Text;
    Database    :=cb_Database.Text;

   // IsUseWinAuth:=rg_Auth.ItemIndex=1;

   //    rb_Windows.Checked := IsUseWinAuth;


    IsUseWinAuth:= rb_Windows.Checked; //cb_Authorization.ItemIndex=1;

  end;
end;

//-----------------------------------------------------------------
procedure Tdlg_DB_Login.act_Database_RefreshExecute(Sender: TObject);
//-----------------------------------------------------------------
var
  rLogin: TdbLoginRec;

begin
  if Sender=act_Test then
    CheckConnect;

    
  if Sender=act_Templates then
  begin
    SaveEditsToClass(rLogin);
    
    if Tdlg_Templates.ExecDlg(rLogin) then
    begin
   //  ShowMessage('1');

      LoadFromRec(rLogin);
//      LoadFromRec(rLogin);
    end;
  end;

end;

procedure Tdlg_DB_Login.btn_AdditionalClick(Sender: TObject);
begin
  TDlg_DB_ConnectAdditionalParams_.ExecDlg(FLoginRec);
end;

procedure Tdlg_DB_Login.cb_DatabaseDropDown(Sender: TObject);
begin
  Database_List_Refresh;
end;

procedure Tdlg_DB_Login.cb_ServerChange(Sender: TObject);
begin
  cb_Database.Items.Clear;
end;

procedure Tdlg_DB_Login.cb_ServerDropDown(Sender: TObject);
begin
  if cb_Server.Items.Count=0 then
    db_ListAvailableSQLServers (cb_Server.Items);
end;

//----------------------------------------------------------------------
procedure Tdlg_DB_Login.ed_ServerButtonClick(Sender: TObject; AbsoluteIndex: Integer);
//----------------------------------------------------------------------
begin
  Select_Server_Name();
end;

//----------------------------------------------------------------------
procedure Tdlg_DB_Login.Connect;
//----------------------------------------------------------------------
var
  sConnectionString: string;
  b: boolean;
begin
  SaveEditsToClass(FLoginRec);

 //
(*
  TdmLogin.Init;
  b := dmLogin.OpenDatabase_(FLoginRec, FConnectionObject);

  if b then
    db_LoginRec_SaveToReg (FRegPath, FLoginRec)
  else
    ShowMessage ('�� ������ ������������ � ���� ������.' );

*)

//  db_LoginRec_LoadFromReg (FRegPath, FLoginRec);

  sConnectionString :=db_ADO_MakeConnectionString (FLoginRec);

  try
    Assert(Assigned(FConnectionObject), 'Value not assigned');


    if (adStateOpen and FConnectionObject.State) <> 0 then
      FConnectionObject.Close;

 //   ShowMessage('1');
  //  ShowMessage(sConnectionString);

    FConnectionObject.Open(sConnectionString,'','',0);

  //  ShowMessage('2');

 Assert(FRegPath <> '');

    db_LoginRec_SaveToReg (FRegPath, FLoginRec);

  except
    ShowMessage ( '�� ������ ������������ � ���� ������. '+ sConnectionString );
  end;    

end;

//----------------------------------------------------------------------
procedure Tdlg_DB_Login.Connect_ADOConnection;
//----------------------------------------------------------------------
var
  sConnectionString: string;
  b: boolean;
begin
  SaveEditsToClass(FLoginRec);

 // TdmConnection.Init;
  b := dmConnection.OpenDatabase(FLoginRec, FADOConnection);

  if b then
    db_LoginRec_SaveToReg (FRegPath, FLoginRec)
  else
    ShowMessage ('�� ������ ������������ � ���� ������.' );

(*
  sConnectionString :=db_ADO_MakeConnectionString (FLoginRec);

  try
    FADOConnection.Close;
    FADOConnection.ConnectionString :=sConnectionString;
    FADOConnection.Open;

    db_LoginRec_SaveToReg (FRegPath, FLoginRec);
  except
    ShowMessage ( '�� ������ ������������ � ���� ������.' );
  end;
*)

end;

//----------------------------------------------------------------------
procedure Tdlg_DB_Login.CheckConnect;
//----------------------------------------------------------------------
begin
  SaveEditsToClass(FLoginRec);
  dmConnection.CheckConnect(FLoginRec);
end;

//----------------------------------------------------------------------
procedure Tdlg_DB_Login.Select_Server_Name;
//----------------------------------------------------------------------
var
  sName : string;
begin
  sName := db_Dlg_GetSqlServerName_old (Self.Handle);

  if sName<>'' then
  begin
    cb_Server.Text := sName;
    Database_List_Refresh;
  end;
end;

//-----------------------------------------------------------------
procedure Tdlg_DB_Login.Database_List_Refresh;
//-----------------------------------------------------------------
begin
  SaveEditsToClass(FLoginRec);

  dmConnection.SQLServer_GetDatabaseList(FLoginRec, cb_Database.Items);

  if cb_Database.Items.IndexOf(cb_Database.Text)=-1 then
    cb_Database.Text:= '';
end;
 

// ---------------------------------------------------------------
procedure Tdlg_DB_Login.rg_Auth1Click(Sender: TObject);
// ---------------------------------------------------------------
var
  b: Boolean;
begin
//  b:=cb_Authorization.ItemIndex=0;

  b:=rb_sql_server.Checked;

  ed_Login.Color   :=IIF(b, clWindow, Color);
  ed_Password.Color:=IIF(b, clWindow, Color);

  ed_Login.Enabled   :=b;
  ed_Password.Enabled:=b;

end;


procedure Tdlg_DB_Login.FormDestroy(Sender: TObject);
begin
  dmConnection_Free;
end;


end.

 {

procedure Tdlg_DB_Login.sdf1Click(Sender: TObject);
begin
  cb_Server.Text :='MISHA\SQLEXPRESS';
  ed_Login.Text :='alex';
  ed_Password.Text :='alex1';
  cb_Database.Text :='onega';
end;


procedure Tdlg_DB_Login.Set1;
begin
  //server sa onega_link

  cb_Server.Text :='server';
  ed_Login.Text :='sa';
  ed_Password.Text :='';
  cb_Database.Text :='onega_link';

end;

procedure Tdlg_DB_Login.Set2;
begin
  //alex sa sa onega_link_test1

  cb_Server.Text :='alex';
  ed_Login.Text :='sa';
  ed_Password.Text :='sa';
  cb_Database.Text :='onega_link_test1';

end;


procedure Tdlg_DB_Login.test2onegalink20121Click(Sender: TObject);
begin
  cb_Server.Text :='test2';
  ed_Login.Text :='sa';
  ed_Password.Text :='sa';
  cb_Database.Text :='onega_link_2012';

end;



procedure Tdlg_DB_Login.onega2013rfpandlink1Click(Sender: TObject);
begin
  cb_Server.Text :='TEST2\SQLEXPRESS';
  ed_Login.Text :='sa';
  ed_Password.Text :='sa';
  cb_Database.Text :='onega_2013_rfp_and_link';


end;




procedure Tdlg_DB_Login.FormDestroy(Sender: TObject);
begin
 // if FRegPath<>'' then
  //  reg_WriteBool (FRegPath, 'IsShowDialog', cb_ShowOnStart.Checked);

 // inherited;
end;
