library login;

uses
  ComServ,
  login_TLB in 'login_TLB.pas',
  x_login in 'x_login.pas' {LoginX: CoClass},
  d_DB_Login in 'src\d_DB_Login.pas' {dlg_DB_Login},
  d_DB_ConnectAdditionalParams_ in 'src\d_DB_ConnectAdditionalParams_.pas' {dlg_DB_ConnectAdditionalParams_},
  I_db_login in 'src\I_db_login.pas',
  d_Templates in 'src\d_Templates.pas' {dlg_Templates},
  dm_Connection in 'src\dm_Connection.pas' {dmConnection: TDataModule},
  u_SQL in 'W:\common XE\Package_Common\u_SQL.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer,
  DllInstall;

{$R *.TLB}

{$R *.RES}

begin
end.
