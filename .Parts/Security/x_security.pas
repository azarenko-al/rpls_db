unit x_security;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  d_User_security,


  dxCore,    
  CodeSiteLogging,
  
  Windows, ActiveX, Classes, ComObj, Security_TLB, StdVcl;

type    
 
  TSecurityX = class(TTypedComObject, ISecurityX)
         

  protected
    function Dlg(const aConnectionStr: WideString): HResult; stdcall;
  end;

implementation

uses ComServ;


function TSecurityX.Dlg(const aConnectionStr: WideString): HResult;
begin    
   Tdlg_User_security.ExecDlg(aConnectionStr);   

end;
          


initialization
  {$IFDEF dll}
  dxInitialize;
 
  TTypedComObjectFactory.Create(ComServer, TSecurityX, Class_SecurityX,
    ciMultiInstance, tmApartment);

  {$ENDIF}
finalization
    {$IFDEF dll}
  dxFinalize;
    {$ENDIF}    
end.


