unit d_User_security;

interface

uses
  u_db,


  Variants, Classes, Controls, Forms, Dialogs,
  cxGridLevel, cxGridCustomTableView,
  cxGridDBTableView, cxGrid,
  ComCtrls, DB, ADODB, StdCtrls, ExtCtrls,

 // dm_Onega_DB_data,


  rxPlacemnt, cxGridTableView, cxClasses, cxControls,
  cxGridCustomView, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxCheckBox, Vcl.Mask, Vcl.DBCtrls;
  

type
  Tdlg_User_security = class(TForm)
    ADOConnection1: TADOConnection;
    col_Enabled: TcxGridDBColumn;
    col_ENABLED1: TcxGridDBColumn;
    col_Geo_Region_name: TcxGridDBColumn;
    col_GeoRegion_ID: TcxGridDBColumn;
    col_GeoRegion_ReadOnly: TcxGridDBColumn;
    col_Project_name: TcxGridDBColumn;
    col_Project_ReadOnly: TcxGridDBColumn;
    cxGrid2: TcxGrid;
    cxGrid3: TcxGrid;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1id: TcxGridDBColumn;
    cxGridDBTableView1Project_ID: TcxGridDBColumn;
    cxGridDBTableView1User_ID: TcxGridDBColumn;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridLevel2: TcxGridLevel;
    ds_GeoRegions: TDataSource;
    ds_Projects: TDataSource;
    FormStorage1: TFormStorage;
    PageControl1: TPageControl;
    sp_GeoRegions: TADOStoredProc;
    sp_Projects: TADOStoredProc;
    TabSheet_GeoRegions: TTabSheet;
    TabSheet_Projects: TTabSheet;
    Bevel2: TBevel;
    Button2: TButton;
    Panel1: TPanel;
    q_User: TADOQuery;
    ds_User: TDataSource;
    DBEdit1: TDBEdit;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FRegPath : string;


//    function Open(aADOConnection: TADOConnection; aUser_id: Integer = 0;
 //       aUser_name: string = ''): boolean;

  public
    class procedure ExecDlg(aConnectionStr: string);

  end;


implementation
{$R *.dfm}

const
//  sp_User_Security = 'sp_User_Security';
  FLD_ACTION = 'ACTION';


const
  REGISTRY_COMMON_FORMS  = 'Software\Onega\Common\Forms\';



// ---------------------------------------------------------------
class procedure Tdlg_User_security.ExecDlg(aConnectionStr: string);
// ---------------------------------------------------------------
var
  iResult: Integer;
begin



   with Tdlg_User_security.Create(Application) do
   begin

     db_OpenADOConnectionString(ADOConnection1, aConnectionStr);


   
//     if aUser_name<>'' then
//       Caption := Caption + ' : ' + aUser_name;


//     db_Op

   //  sp_Projects.Connection := aADOConnection;
    // sp_GeoRegions.Connection := aADOConnection;

  //   dmOnega_DB_data.
     iResult:=db_OpenStoredProc(sp_Projects,  sp_Projects.ProcedureName,
        [
         db_Par(FLD_ACTION, 'select_projects_enabled') 
     //    db_Par(FLD_User_NAME, aUser_name)
        // db_Par(FLD_ID, aID),
         ]);

     iResult:=db_OpenStoredProc(sp_GeoRegions,  sp_GeoRegions.ProcedureName,
        [
         db_Par(FLD_ACTION, 'select_GeoRegions_enabled') 
     //    db_Par(FLD_User_NAME, aUser_name)
        // db_Par(FLD_ID, aID),
         ]);  

     db_OpenQuery(q_User, 'select user_name() as name' ) ;
          

     ShowModal;

     Free;
   end;

end;

// ---------------------------------------------------------------
procedure Tdlg_User_security.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  if ADOConnection1.Connected  then
    ShowMessage('ADOConnection1.Connected');
    


  Caption :='����� ������������';

  col_Project_name.Caption :='������';

  col_Project_ReadOnly.Caption   :='������ ��� ������';
  col_GeoRegion_ReadOnly.Caption :='������ ��� ������';

  PageControl1.Align:=alClient;

  cxGrid2.Align:=alClient;
  cxGrid3.Align:=alClient;

  col_Enabled.Visible := False;
  col_Enabled1.Visible := False;

  col_Geo_Region_name.Caption :='����������� ���.������� (��� ��������������)';
  col_Project_name.Caption    :='����������� ������� (��� ��������������)';


  col_Project_ReadOnly.Visible := False;
  col_GeoRegion_ReadOnly.Visible := False;

 // TabSheet_GeoRegions.TabVisible := False;


  TabSheet_GeoRegions.Caption :='���.�������';
  TabSheet_Projects.Caption :='�������';


 // TabSheet_Projects.TabVisible := False;


  FRegPath:=REGISTRY_COMMON_FORMS + ClassName;

  cxGridDBTableView1.RestoreFromRegistry(FRegPath + '\'+ cxGridDBTableView1.Name);
  cxGridDBTableView2.RestoreFromRegistry(FRegPath + '\'+ cxGridDBTableView2.Name);

///   aObject.StoreToRegistry(s, True,  [gsoUseFilter])

end;



procedure Tdlg_User_security.FormDestroy(Sender: TObject);
begin
  cxGridDBTableView1.StoreToRegistry(FRegPath + '\'+ cxGridDBTableView1.Name, True,  [gsoUseFilter]);
  cxGridDBTableView2.StoreToRegistry(FRegPath + '\'+ cxGridDBTableView2.Name, True,  [gsoUseFilter]);

end;

end.
