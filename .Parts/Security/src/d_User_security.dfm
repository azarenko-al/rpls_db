object dlg_User_security: Tdlg_User_security
  Left = 1140
  Top = 369
  BorderWidth = 5
  Caption = 'dlg_User_security'
  ClientHeight = 536
  ClientWidth = 532
  Color = clBtnFace
  DefaultMonitor = dmDesktop
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    532
    536)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel2: TBevel
    Left = 0
    Top = 503
    Width = 532
    Height = 33
    Align = alBottom
    Shape = bsTopLine
    ExplicitTop = 556
    ExplicitWidth = 534
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 33
    Width = 532
    Height = 266
    ActivePage = TabSheet_GeoRegions
    Align = alTop
    TabOrder = 0
    ExplicitTop = 56
    object TabSheet_GeoRegions: TTabSheet
      Caption = 'Geo Regions'
      ImageIndex = 1
      object cxGrid3: TcxGrid
        Left = 0
        Top = 0
        Width = 524
        Height = 201
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView2: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataModeController.GridMode = True
          DataController.DataSource = ds_GeoRegions
          DataController.KeyFieldNames = 'GeoRegion_ID'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsSelection.CellSelect = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object col_ENABLED1: TcxGridDBColumn
            DataBinding.FieldName = 'ENABLED'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssUnchecked
            Visible = False
            Width = 86
          end
          object col_GeoRegion_ID: TcxGridDBColumn
            DataBinding.FieldName = 'GeoRegion_ID'
            Visible = False
            Width = 75
          end
          object col_Geo_Region_name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 339
          end
          object cxGridDBColumn3: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
            Width = 49
          end
          object cxGridDBColumn4: TcxGridDBColumn
            DataBinding.FieldName = 'User_ID'
            Visible = False
          end
          object cxGridDBColumn5: TcxGridDBColumn
            DataBinding.FieldName = 'Project_ID'
            Visible = False
          end
          object col_GeoRegion_ReadOnly: TcxGridDBColumn
            DataBinding.FieldName = 'ReadOnly'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssUnchecked
            Width = 98
          end
        end
        object cxGridLevel2: TcxGridLevel
          GridView = cxGridDBTableView2
        end
      end
    end
    object TabSheet_Projects: TTabSheet
      Caption = 'Projects'
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 524
        Height = 209
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataModeController.GridMode = True
          DataController.DataSource = ds_Projects
          DataController.KeyFieldNames = 'Project_ID'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsSelection.CellSelect = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object col_Enabled: TcxGridDBColumn
            DataBinding.FieldName = 'ENABLED'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssUnchecked
            Visible = False
            Width = 78
          end
          object col_Project_name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 379
          end
          object cxGridDBTableView1id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
            Width = 49
          end
          object cxGridDBTableView1User_ID: TcxGridDBColumn
            DataBinding.FieldName = 'User_ID'
            Visible = False
          end
          object cxGridDBTableView1Project_ID: TcxGridDBColumn
            DataBinding.FieldName = 'Project_ID'
            Visible = False
          end
          object col_Project_ReadOnly: TcxGridDBColumn
            DataBinding.FieldName = 'ReadOnly'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssUnchecked
            Width = 116
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
  end
  object Button2: TButton
    Left = 454
    Top = 511
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = #1047#1072#1082#1088#1099#1090#1100
    ModalResult = 2
    TabOrder = 1
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 532
    Height = 33
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object DBEdit1: TDBEdit
      Left = 4
      Top = 1
      Width = 293
      Height = 21
      DataField = 'name'
      DataSource = ds_User
      ParentColor = True
      TabOrder = 0
    end
  end
  object sp_Projects: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'Security.sp_User_Projects_SEL'
    Parameters = <>
    Left = 288
    Top = 360
  end
  object sp_GeoRegions: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'Security.sp_User_GeoRegion_SEL'
    Parameters = <>
    Left = 176
    Top = 360
  end
  object ds_Projects: TDataSource
    DataSet = sp_Projects
    Left = 288
    Top = 416
  end
  object ds_GeoRegions: TDataSource
    DataSet = sp_GeoRegions
    Left = 176
    Top = 416
  end
  object ADOConnection1: TADOConnection
    CommandTimeout = 60
    ConnectionString = 
      'Provider=MSDASQL.1;Password=sa;Persist Security Info=True;User I' +
      'D=sa;Data Source=onega_link'
    ConnectionTimeout = 30
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 40
    Top = 352
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    UseRegistry = True
    StoredProps.Strings = (
      'PageControl1.ActivePage')
    StoredValues = <>
    Left = 48
    Top = 424
  end
  object q_User: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select user_name() as name')
    Left = 408
    Top = 360
  end
  object ds_User: TDataSource
    DataSet = q_User
    Left = 408
    Top = 416
  end
end
