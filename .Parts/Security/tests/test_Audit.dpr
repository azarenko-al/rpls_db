program test_Audit;

uses
  Vcl.Forms,
  d_Audit in '..\src\d_Audit.pas' {dlg_Audit},
  Audit_TLB in '..\Audit_TLB.pas',
  Unit19 in 'Unit19.pas' {Form19};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
//  Application.CreateForm(Tdlg_Audit, dlg_Audit);
  Application.CreateForm(TForm19, Form19);
  Application.Run;
end.
