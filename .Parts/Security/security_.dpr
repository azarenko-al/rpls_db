library security_;

uses
  ComServ,
  Security_TLB in 'Security_TLB.pas',
  x_security in 'x_security.pas' {SecurityX: CoClass},
  d_User_security in 'src\d_User_security.pas' {dlg_User_security};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer,
  DllInstall;

{$R *.TLB}

{$R *.RES}

begin
end.
