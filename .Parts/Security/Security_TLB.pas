unit Security_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 24.04.2019 11:47:53 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\RPLS_DB\.Parts\Security\Project20 (1)
// LIBID: {E04C3495-6F52-49DA-B4F3-A78F652760A4}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\Windows\system32\stdvcl40.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Winapi.Windows, System.Classes, System.Variants, System.Win.StdVCL, Vcl.Graphics, Vcl.OleServer, Winapi.ActiveX;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  SecurityMajorVersion = 1;
  SecurityMinorVersion = 0;

  LIBID_Security: TGUID = '{E04C3495-6F52-49DA-B4F3-A78F652760A4}';

  IID_ISecurityX: TGUID = '{F0E543B5-8485-4F2A-84F0-660782689212}';
  CLASS_SecurityX: TGUID = '{05929B32-B5F8-461E-B637-3BB48C3710BF}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ISecurityX = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  SecurityX = ISecurityX;


// *********************************************************************//
// Interface: ISecurityX
// Flags:     (256) OleAutomation
// GUID:      {F0E543B5-8485-4F2A-84F0-660782689212}
// *********************************************************************//
  ISecurityX = interface(IUnknown)
    ['{F0E543B5-8485-4F2A-84F0-660782689212}']
    function Dlg(const aConnectionStr: WideString): HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoSecurityX provides a Create and CreateRemote method to          
// create instances of the default interface ISecurityX exposed by              
// the CoClass SecurityX. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSecurityX = class
    class function Create: ISecurityX;
    class function CreateRemote(const MachineName: string): ISecurityX;
  end;

implementation

uses System.Win.ComObj;

class function CoSecurityX.Create: ISecurityX;
begin
  Result := CreateComObject(CLASS_SecurityX) as ISecurityX;
end;

class function CoSecurityX.CreateRemote(const MachineName: string): ISecurityX;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SecurityX) as ISecurityX;
end;

end.

