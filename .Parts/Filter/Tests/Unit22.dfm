object Form22: TForm22
  Left = 1426
  Top = 426
  Caption = 'Form22'
  ClientHeight = 413
  ClientWidth = 594
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button3: TButton
    Left = 24
    Top = 144
    Width = 153
    Height = 25
    Caption = 'Link'
    TabOrder = 0
    OnClick = Button3Click
  end
  object Button1: TButton
    Left = 24
    Top = 224
    Width = 153
    Height = 25
    Caption = 'property'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 232
    Top = 144
    Width = 153
    Height = 25
    Caption = 'Link'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button4: TButton
    Left = 232
    Top = 224
    Width = 153
    Height = 25
    Caption = 'property'
    TabOrder = 3
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 24
    Top = 288
    Width = 153
    Height = 25
    Caption = 'AntennaType'
    TabOrder = 4
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 232
    Top = 288
    Width = 153
    Height = 25
    Caption = 'AntennaType'
    TabOrder = 5
    OnClick = Button6Click
  end
  object Panel1: TPanel
    Left = 24
    Top = 80
    Width = 169
    Height = 33
    Caption = 'no com'
    TabOrder = 6
  end
  object Panel2: TPanel
    Left = 232
    Top = 80
    Width = 169
    Height = 33
    Caption = 'com'
    TabOrder = 7
  end
  object Button7: TButton
    Left = 248
    Top = 352
    Width = 75
    Height = 25
    Caption = 'Button7'
    TabOrder = 12
    OnClick = Button7Click
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=SERVER1;Use Procedu' +
      're for Prepare=1;Auto Translate=True;Packet Size=4096;Workstatio' +
      'n ID=ALEX;Use Encryption for Data=False;Tag with column collatio' +
      'n when possible=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 461
    Top = 112
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredValues = <>
    Left = 464
    Top = 192
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    LookAndFeel.Kind = lfFlat
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 464
    Top = 268
    DockControlHeights = (
      0
      0
      25
      0)
    object dxBarManager1Bar2: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 2'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 1172
      FloatTop = 327
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 256
          Visible = True
          ItemName = 'ed_Filter'
        end
        item
          Visible = True
          ItemName = 'dxBarCombo1'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object ed_Filter: TcxBarEditItem
      Caption = #1060#1080#1083#1100#1090#1088
      Category = 0
      Hint = #1060#1080#1083#1100#1090#1088
      Visible = ivAlways
      ShowCaption = True
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end
        item
          Caption = #1059#1076#1072#1083#1080#1090#1100
        end>
      Properties.LookupItems.Strings = (
        'asdfaSD'
        'asdASDa'
        'sasdfasdf'
        'asdfasdf')
      Properties.ReadOnly = True
      Properties.UseNullString = True
      Properties.OnButtonClick = ed_FilterPropertiesButtonClick
    end
    object dxBarCombo1: TdxBarCombo
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Text = 'link'
      Items.Strings = (
        'link'
        'property')
      ItemIndex = 0
    end
  end
end
