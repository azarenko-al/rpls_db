program filter_test;

uses
  Vcl.Forms,
  Unit22 in 'Unit22.pas' {Form22},
  Filter_TLB in '..\Filter_TLB.pas',
  d_Filter_setup in '..\src\d_Filter_setup.pas' {dlg_Filter_setup},
  d_Filter in '..\src\d_Filter.pas' {dlg_Filter};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm22, Form22);
  Application.Run;
end.
