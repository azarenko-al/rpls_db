unit d_Filter_setup;

interface

uses
  u_db,
  u_cx,

 Dialogs,
  
  System.SysUtils, System.Variants, System.Classes, 
  Vcl.Controls, Vcl.Forms, cxLookAndFeels,
  cxStyles, 
  Data.DB, 
  Data.Win.ADODB, Vcl.StdCtrls, Vcl.ExtCtrls, dxBar, cxGridLevel,
  cxGridDBTableView, 
  cxGrid, cxSplitter, RxPlacemnt, Vcl.DBCtrls, Vcl.DBGrids,
  Vcl.ComCtrls, Vcl.ActnList, dxBarExtItems, cxGraphics, cxControls,
  cxLookAndFeelPainters, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, cxCheckBox, System.Actions, cxClasses, Vcl.Grids,
  cxGridCustomTableView, cxGridTableView, cxGridCustomView;

type
  Tdlg_Filter_setup = class(TForm)
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1id: TcxGridDBColumn;
    cxGridDBTableView1objname: TcxGridDBColumn;
    cxGridDBTableView1name: TcxGridDBColumn;
    cxGridDBTableView1user_created: TcxGridDBColumn;
    cxGridDBTableView1is_shared: TcxGridDBColumn;
    cxGridDBTableView1is_can_update: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    ds_Filters: TDataSource;
    ADOConnection1: TADOConnection;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_ReadOnly: TcxStyle;
    Bevel1: TBevel;
    b_Save: TButton;
    b_Close: TButton;
    q_Results: TADOQuery;
    ds_Results: TDataSource;
    cxLookAndFeelController1: TcxLookAndFeelController;
    FormStorage1: TFormStorage;
    cxGridDBTableView1Column1: TcxGridDBColumn;
    ActionList1: TActionList;
    act_Run: TAction;
    dxBarManager1Bar2: TdxBar;
    dxBarButton2: TdxBarButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBMemo1: TDBMemo;
    DBGrid1: TDBGrid;
    cxSplitter1: TcxSplitter;
    dxBarDockControl1: TdxBarDockControl;
    q_Filters: TADOQuery;
    cxGridDBTableView1Column2: TcxGridDBColumn;
    DBNavigator1: TDBNavigator;
    dxBarContainerItem1: TdxBarContainerItem;
    dxBarControlContainerItem1: TdxBarControlContainerItem;
    act_Ins_Defaul: TAction;
    dxBarButton1: TdxBarButton;
    cxSplitter2: TcxSplitter;
    procedure act_Ins_DefaulExecute(Sender: TObject);
    procedure act_RunExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure q_FiltersNewRecord(DataSet: TDataSet);
  private
    FObjName: string;
     
    procedure SetComponentADOConnection(aADOConnection: TADOConnection);

    { Private declarations }
  public
    class procedure ExecDlg(aADOConnection: TADOConnection; aObjName: string);

  end;

 
implementation

{$R *.dfm}

const
  FLD_sql_body = 'sql_body';

const
  DEF_SQL =    
    '  select * '+
    '      from ui.Filters '+
//    '      where OBJNAME=:OBJNAME and user_created = user_name()';
    '      where OBJNAME=:OBJNAME ';
 
  

// ---------------------------------------------------------------
class procedure Tdlg_Filter_setup.ExecDlg(aADOConnection: TADOConnection;  aObjName: string);
// ---------------------------------------------------------------
begin
  with Tdlg_Filter_setup.Create (Application) do
  begin   
    FObjName:=aObjName;
  
    SetComponentADOConnection(aADOConnection);
            
    
//    ADOConnection1.Close;
//    ADOConnection1.ConnectionString:=aADOConnection;  
//    ADOConnection1.Open;


    db_OpenQuery_(q_Filters, DEF_SQL, ['objname', aObjName] );
               

    ShowModal;
           
    Free;
  end;

end;


procedure Tdlg_Filter_setup.act_Ins_DefaulExecute(Sender: TObject);

  
var
  s: string;
begin
//  s:=q_Filters.FieldByName(FLD_sql_body).AsString + ' ';
  //db_PostDataset(q_Filters);
  db_PostDataset(q_Filters);


  DBMemo1.Text:=DBMemo1.Text + ' ' + 'select * from ui.view_'+ FObjName;
      
  db_UpdateRecord__(q_Filters, [FLD_sql_body, 'select * from ui.view_'+ FObjName  ]);
  
end;


procedure Tdlg_Filter_setup.act_RunExecute(Sender: TObject);
begin
  db_OpenQuery(q_Results, DBMemo1.Text);
  
end;

procedure Tdlg_Filter_setup.FormCreate(Sender: TObject);
begin
   {$IFDEF dll}
  if ADOConnection1.Connected then
    ShowMessage('procedure FormCreate(Sender: TObject); - ADOConnection_MDB.Connected');
    {$ENDIF}    




  Caption:='��������� ��������';
  
  cxGrid2.Align:=alClient;

  DBGrid1.Align:=alClient;

  cx_SetColumnCaptions1(cxGridDBTableView1,

  [
    'date_created', '������',
    'is_shared', 'fsdfsdf'

  ]);
  
end;


procedure Tdlg_Filter_setup.q_FiltersNewRecord(DataSet: TDataSet);
begin
  Dataset['objname']:=FObjName;
end;


procedure Tdlg_Filter_setup.SetComponentADOConnection(aADOConnection: TADOConnection);
begin
  db_SetComponentADOConnection(Self, aADOConnection);

end;


end.
