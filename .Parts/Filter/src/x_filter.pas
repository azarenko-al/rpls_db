unit x_filter;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Filter_TLB ,
            
  d_Filter,

  dxCore,
  CodeSiteLogging,
  
  Windows, ActiveX, Classes, ComObj,  StdVcl;

type
  TFilterX = class(TTypedComObject, IFilterX)
  protected
    function Dlg(const aConnectionStr, aObjName: WideString; var aID: Integer; var
        aName, aSQL: WideString): HResult; stdcall;
  public
    constructor Create;
  end;

  
implementation

uses ComServ;


constructor TFilterX.Create;
begin
  inherited;
  // TODO -cMM: TFilterX.Create default body inserted
end;

function TFilterX.Dlg(const aConnectionStr, aObjName: WideString; var aID:
    Integer; var aName, aSQL: WideString): HResult;
var
  rec: TFilter_result;
begin

  if not Tdlg_Filter.ExecDlg(aConnectionStr, aObjName,  rec) then 
    Result:=S_FALSE 
  else begin
    Result:=S_OK;

    aID  :=rec.ID;
    aName:=rec.name;
    aSQL :=rec.SQL;
  end;  
 
end;


initialization
  {$IFDEF dll}
  dxInitialize;
 
  TTypedComObjectFactory.Create(ComServer, TFilterX, Class_FilterX,
    ciMultiInstance, tmApartment);

     {$ENDIF}  
finalization
    {$IFDEF dll}
  dxFinalize;
    {$ENDIF}    
end.



