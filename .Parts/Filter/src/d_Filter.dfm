object dlg_Filter: Tdlg_Filter
  Left = 862
  Top = 447
  Caption = 'dlg_Filter'
  ClientHeight = 529
  ClientWidth = 745
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    745
    529)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 0
    Top = 485
    Width = 745
    Height = 44
    Align = alBottom
    Shape = bsTopLine
    ExplicitTop = 697
    ExplicitWidth = 1210
  end
  object cxGrid2: TcxGrid
    Left = 0
    Top = 0
    Width = 537
    Height = 485
    Align = alLeft
    TabOrder = 0
    ExplicitTop = 28
    ExplicitHeight = 207
    object cxGridDBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ds_ADOStoredProc_SEL
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      object cxGridDBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Width = 34
      end
      object cxGridDBTableView1objname: TcxGridDBColumn
        DataBinding.FieldName = 'objname'
        Visible = False
        Width = 88
      end
      object cxGridDBTableView1name: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'name'
        Width = 281
      end
      object cxGridDBTableView1user_created: TcxGridDBColumn
        Caption = #1042#1083#1072#1076#1077#1083#1077#1094
        DataBinding.FieldName = 'user_created'
        Width = 102
      end
      object cxGridDBTableView1Column2: TcxGridDBColumn
        Caption = #1057#1086#1079#1076#1072#1085
        DataBinding.FieldName = 'date_created'
        Width = 140
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object b_Save: TButton
    Left = 574
    Top = 496
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = #1042#1099#1073#1088#1072#1090#1100
    Default = True
    ModalResult = 1
    TabOrder = 1
    ExplicitLeft = 971
    ExplicitTop = 599
  end
  object b_Close: TButton
    Left = 662
    Top = 496
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = #1047#1072#1082#1088#1099#1090#1100
    ModalResult = 8
    TabOrder = 2
    ExplicitLeft = 1059
    ExplicitTop = 599
  end
  object Button1: TButton
    Left = 5
    Top = 496
    Width = 75
    Height = 25
    Action = act_Setup
    Anchors = [akLeft, akBottom]
    TabOrder = 3
    ExplicitTop = 467
  end
  object ADOStoredProc_SEL: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'ui.sp_Filter_SEL;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@OBJNAME'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = 'link'
      end>
    Left = 608
    Top = 224
  end
  object ds_ADOStoredProc_SEL: TDataSource
    DataSet = ADOStoredProc_SEL
    Left = 609
    Top = 280
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=SERVER1;Use Procedu' +
      're for Prepare=1;Auto Translate=True;Packet Size=4096;Workstatio' +
      'n ID=ALEX;Use Encryption for Data=False;Tag with column collatio' +
      'n when possible=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 600
    Top = 24
  end
  object FormStorage1: TFormStorage
    Options = [fpPosition]
    UseRegistry = True
    StoredValues = <>
    Left = 600
    Top = 152
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 600
    Top = 96
    object act_Setup: TAction
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
      OnExecute = act_SetupExecute
    end
  end
  object cxLookAndFeelController1: TcxLookAndFeelController
    Kind = lfFlat
    Left = 610
    Top = 360
  end
end
