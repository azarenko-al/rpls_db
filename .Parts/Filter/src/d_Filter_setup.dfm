object dlg_Filter_setup: Tdlg_Filter_setup
  Left = 833
  Top = 272
  Caption = 'dlg_Filter_setup'
  ClientHeight = 592
  ClientWidth = 1200
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    1200
    592)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 0
    Top = 548
    Width = 1200
    Height = 44
    Align = alBottom
    Shape = bsTopLine
    ExplicitTop = 697
    ExplicitWidth = 1210
  end
  object cxGrid2: TcxGrid
    Left = 0
    Top = 31
    Width = 545
    Height = 241
    Align = alLeft
    TabOrder = 0
    ExplicitHeight = 223
    object cxGridDBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ds_Filters
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGridDBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Width = 41
      end
      object cxGridDBTableView1objname: TcxGridDBColumn
        DataBinding.FieldName = 'objname'
        Visible = False
        Width = 88
      end
      object cxGridDBTableView1name: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'name'
        Width = 225
      end
      object cxGridDBTableView1user_created: TcxGridDBColumn
        Caption = #1042#1083#1072#1076#1077#1083#1077#1094
        DataBinding.FieldName = 'user_created'
        Options.Editing = False
        Width = 102
      end
      object cxGridDBTableView1Column2: TcxGridDBColumn
        DataBinding.FieldName = 'date_created'
        Options.Editing = False
        Width = 100
      end
      object cxGridDBTableView1is_shared: TcxGridDBColumn
        DataBinding.FieldName = 'is_shared'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Visible = False
        Width = 71
      end
      object cxGridDBTableView1is_can_update: TcxGridDBColumn
        DataBinding.FieldName = 'is_readonly'
        Visible = False
        Width = 77
      end
      object cxGridDBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'comment'
        Visible = False
        Options.Editing = False
        Width = 112
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object b_Save: TButton
    Left = 1029
    Top = 559
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    Default = True
    ModalResult = 1
    TabOrder = 1
    ExplicitTop = 541
  end
  object b_Close: TButton
    Left = 1117
    Top = 559
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = #1047#1072#1082#1088#1099#1090#1100
    ModalResult = 8
    TabOrder = 2
    ExplicitTop = 541
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 280
    Width = 1200
    Height = 268
    ActivePage = TabSheet1
    Align = alBottom
    Style = tsFlatButtons
    TabOrder = 5
    ExplicitTop = 262
    object TabSheet1: TTabSheet
      Caption = #1047#1072#1087#1088#1086#1089
      object DBMemo1: TDBMemo
        Left = 0
        Top = 28
        Width = 1192
        Height = 85
        Align = alTop
        DataField = 'sql_body'
        DataSource = ds_Filters
        ScrollBars = ssBoth
        TabOrder = 0
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 121
        Width = 1192
        Height = 116
        Align = alBottom
        DataSource = ds_Results
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
      object cxSplitter1: TcxSplitter
        Left = 0
        Top = 113
        Width = 1192
        Height = 8
        HotZoneClassName = 'TcxSimpleStyle'
        AlignSplitter = salTop
        Control = DBMemo1
      end
      object dxBarDockControl1: TdxBarDockControl
        Left = 0
        Top = 0
        Width = 1192
        Height = 28
        Align = dalTop
        BarManager = dxBarManager1
      end
    end
  end
  object DBNavigator1: TDBNavigator
    Left = 184
    Top = 72
    Width = 136
    Height = 25
    DataSource = ds_Filters
    VisibleButtons = [nbInsert, nbDelete, nbPost, nbCancel]
    Flat = True
    TabOrder = 8
  end
  object cxSplitter2: TcxSplitter
    Left = 0
    Top = 272
    Width = 1200
    Height = 8
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salBottom
    Control = PageControl1
    ExplicitTop = 254
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 753
    Top = 56
    DockControlHeights = (
      0
      0
      31
      0)
    object dxBarManager1Bar1: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 728
      FloatTop = 431
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarControlContainerItem1'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 2'
      CaptionButtons = <>
      DockControl = dxBarDockControl1
      DockedDockControl = dxBarDockControl1
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 635
      FloatTop = 188
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton2'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarButton2: TdxBarButton
      Action = act_Run
      Category = 0
    end
    object dxBarContainerItem1: TdxBarContainerItem
      Caption = 'New Item'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarControlContainerItem1: TdxBarControlContainerItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Control = DBNavigator1
    end
    object dxBarButton1: TdxBarButton
      Action = act_Ins_Defaul
      Category = 0
    end
  end
  object ds_Filters: TDataSource
    DataSet = q_Filters
    Left = 857
    Top = 112
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=SERVER1;Use Procedu' +
      're for Prepare=1;Auto Translate=True;Packet Size=4096;Workstatio' +
      'n ID=ALEX;Use Encryption for Data=False;Tag with column collatio' +
      'n when possible=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 752
    Top = 192
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 752
    Top = 112
    PixelsPerInch = 96
    object cxStyle_ReadOnly: TcxStyle
      AssignedValues = [svColor]
      Color = clSilver
    end
  end
  object q_Results: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select top 100 * from link ')
    Left = 976
    Top = 56
  end
  object ds_Results: TDataSource
    DataSet = q_Results
    Left = 977
    Top = 112
  end
  object cxLookAndFeelController1: TcxLookAndFeelController
    Kind = lfFlat
    Left = 1080
    Top = 48
  end
  object FormStorage1: TFormStorage
    Options = [fpPosition]
    UseRegistry = True
    StoredProps.Strings = (
      'DBMemo1.Height'
      'TabSheet1.Height')
    StoredValues = <>
    Left = 1080
    Top = 112
  end
  object ActionList1: TActionList
    Left = 920
    Top = 200
    object act_Run: TAction
      Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100' '#1079#1072#1087#1088#1086#1089
      OnExecute = act_RunExecute
    end
    object act_Ins_Defaul: TAction
      Caption = #1064#1072#1073#1083#1086#1085
      OnExecute = act_Ins_DefaulExecute
    end
  end
  object q_Filters: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    OnNewRecord = q_FiltersNewRecord
    Parameters = <
      item
        Name = 'OBJNAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = 'link'
      end>
    SQL.Strings = (
      ''
      '  select * '
      '      from ui.Filters '
      '      where OBJNAME=:OBJNAME ')
    Left = 856
    Top = 56
  end
end
