unit d_Filter;

interface

uses
  u_db,
  u_cx,

  d_Filter_setup,

Dialogs,  

  System.SysUtils, System.Variants, System.Classes, 
  Vcl.Controls, Vcl.Forms, cxLookAndFeels,
  
  Data.DB, 
  Data.Win.ADODB, Vcl.StdCtrls, Vcl.ExtCtrls, cxGridLevel,
  cxGridDBTableView, 
  cxGrid, RxPlacemnt, 
  Vcl.ActnList, cxGraphics, cxControls, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  System.Actions, cxGridCustomTableView, cxGridTableView, cxClasses,
  cxGridCustomView;

type
  TFilter_result = record
    ID: Integer ;
    name : string;
    SQL : string;
  end;


  Tdlg_Filter = class(TForm)
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1id: TcxGridDBColumn;
    cxGridDBTableView1objname: TcxGridDBColumn;
    cxGridDBTableView1name: TcxGridDBColumn;
    cxGridDBTableView1user_created: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    ADOStoredProc_SEL: TADOStoredProc;
    ds_ADOStoredProc_SEL: TDataSource;
    ADOConnection1: TADOConnection;
    Bevel1: TBevel;
    b_Save: TButton;
    b_Close: TButton;
    FormStorage1: TFormStorage;
    ActionList1: TActionList;
    act_Setup: TAction;
    Button1: TButton;
    cxGridDBTableView1Column2: TcxGridDBColumn;
    cxLookAndFeelController1: TcxLookAndFeelController;
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_SetupExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FObjName: string;
  public
    class function ExecDlg(aConnection_str, aObjName: string; var aRes:  TFilter_result): Boolean;

  end;

 
implementation

{$R *.dfm}


class function Tdlg_Filter.ExecDlg(aConnection_str, aObjName: string; var aRes: TFilter_result): Boolean;
begin
  with Tdlg_Filter.Create (Application) do
  begin    
    FObjName:=aObjName;
     
    db_OpenADOConnectionString(ADOConnection1, aConnection_str);
    
    
//    ADOConnection1.Close;
//    ADOConnection1.ConnectionString:=aConnection_str;  
//    ADOConnection1.Open;


    db_StoredProc_Open(ADOStoredProc_SEL, ADOStoredProc_SEL.ProcedureName, ['objname', aObjName]);

    

    Result:= ShowModal=mrOk;

    if Result then 
    begin
      aRes.ID  :=ADOStoredProc_SEL.FieldByName('id').AsInteger;
      aRes.name:=ADOStoredProc_SEL.FieldByName('name').AsString;
      aRes.SQL :=ADOStoredProc_SEL.FieldByName('sql_body').AsString;
    end;  

    Free;
  end;

end;

// ---------------------------------------------------------------
procedure Tdlg_Filter.act_SetupExecute(Sender: TObject);
// ---------------------------------------------------------------
begin
  Tdlg_Filter_setup.ExecDlg(ADOConnection1, FObjName);

  ADOStoredProc_SEL.Close;
  ADOStoredProc_SEL.Open;
  
end;

procedure Tdlg_Filter.FormCreate(Sender: TObject);
begin
   {$IFDEF dll}
  if ADOConnection1.Connected then
    ShowMessage('procedure FormCreate(Sender: TObject); - ADOConnection_MDB.Connected');
    {$ENDIF}    



  Caption:='�������';
  
  cxGrid2.Align:=alClient;

  cx_SetColumnCaptions1(cxGridDBTableView1,
  [
  

  ]);
  
//  DBGrid1.Align:=alClient;
end;

procedure Tdlg_Filter.ActionList1Update(Action: TBasicAction; var Handled:
    Boolean);
begin
  b_Save.Enabled:= ADOStoredProc_SEL.RecordCount > 0;
end;

end.
