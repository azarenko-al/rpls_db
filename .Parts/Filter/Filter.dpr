library Filter;

uses
  ComServ,
  Filter_TLB in 'Filter_TLB.pas',
  x_filter in 'src\x_filter.pas' {FilterX: CoClass},
  d_Filter_setup in 'src\d_Filter_setup.pas' {dlg_Filter_setup},
  d_Filter in 'src\d_Filter.pas' {dlg_Filter};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer,
  DllInstall;

{$R *.TLB}

{$R *.RES}

begin
end.
