library snmp;

uses
  ComServ,
  snmp_TLB in 'snmp_TLB.pas',
  u_snmp in 'src\u_snmp.pas',
  x_snmp in 'x_snmp.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer,
  DllInstall;

{$R *.TLB}

{$R *.RES}

begin
end.
