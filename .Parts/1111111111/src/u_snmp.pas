unit u_snmp;

interface

uses
  snmp_TLB,

  SNMPsend, asn1util,

  System.Generics.Collections,
  System.SysUtils, CodeSiteLogging,  
  Windows, Forms, IniFiles;

  
type
  TSNMP_msg = class
    Msg: string;   
    OID: string;
  public
    constructor Create_(aOID: string; aMsg: string);
  end;

  

  TSNMP_wrap = class(TObject)
  private
    FHost: string;   
    FOID: string;

    Items: TDictionary<integer, TSNMP_msg>;
                    
    procedure LoadFromFile;
  public
    constructor Create;

    function Send(aMsg: string): Boolean;
    function Send_code(aCode: Integer; aMsg: string): Boolean;

  end;

function GetComputerName: string;

implementation



constructor TSNMP_wrap.Create;
begin
  inherited;

  LoadFromFile();

  //TOptionsParameterDict= class(TDictionary<String, TOptionsParameterItem>);
  // TODO -cMM: TSNMP_wrap.Create default body inserted
end;

// ---------------------------------------------------------------
procedure TSNMP_wrap.LoadFromFile;
// ---------------------------------------------------------------
var
  oIni: TIniFile;
  sFile: string;
begin
  inherited;


  {$IFDEF dll}
  sFile:=ExtractFilePath(Application.ExeName) + 'snmp.ini';
  {$ELSE}      
  sFile:='W:\RPLS_DB\.Parts\SNMP\bin\snmp.ini';
  {$ENDIF}      
             
                    
  CodeSite.Send(sFile);
                
  
  oIni:=TIniFile.Create (sFile);
//  oIni:=TIniFile.Create ('W:\RPLS_DB\.Parts\SNMP\bin\snmp.ini');

  FHost:=oIni.ReadString ('main','Host','');
//  FPort:=oIni.ReadInteger('main','Port',0);
  FOID:=oIni.ReadString  ('main','OID','');

  
  Items.Add(SNMP_ERROR_PROFILE ,   
    TSNMP_msg.Create_(oIni.ReadString ('error_profile','msg',''),
                      oIni.ReadString ('error_profile','OID','') ));
  
  Items.Add(SNMP_ERROR_DB_CONNECT ,   
    TSNMP_msg.Create_(oIni.ReadString ('error_db_conn','msg',''),
                      oIni.ReadString ('error_db_conn','OID','') ));
                       
  Items.Add(SNMP_ERROR_LICENCE ,   
    TSNMP_msg.Create_(oIni.ReadString ('error_licence','msg',''),
                      oIni.ReadString ('error_licence','OID','') ));

  
   // enum Message_type
  {
    SNMP_ERROR_PROFILE = 0,
    SNMP_ERROR_DB_CONNECT = 1,
    SNMP_ERROR_LICENCE = 2
  };

//
//  
//[error_licence]
//OID=1.2.2.2.2.2.2.1
//msg=������ � ���������
//
//[error_profile]
//OID=1.2.2.2.2.2.2.2
//msg=������ ��� ���������� �������
//
//[error_db_conn]
//OID=1.2.2.2.2.2.2.3
//msg=������ ��� ���������� � ��
//
//

  
//  TSNMP_msg.Create;
  
  
  FreeAndNil (oIni);
  
end;

// ---------------------------------------------------------------
function TSNMP_wrap.Send(aMsg: string): Boolean;
// ---------------------------------------------------------------
var
  snmpsend: TSNMPSend;
begin
  snmpsend := TSNMPSend.Create;

  snmpSend.TargetHost := FHost; // '192.168.0.184'; //IP of SNMP server

//  snmpSend.TargetHost := '127.0.0.1'; //IP of SNMP server     OID
//  snmpSend.TargetPort := IntToStr(FPort); //  '162'; //FPort of SNMP server
  snmpSend.Query.Version:= 1;
  snmpsend.Query.Community := 'public';
  
  snmpsend.Query.MIBAdd (FOID, GetComputerName() + ' - ' +  aMsg , ASN1_OCTSTR);  
  snmpsend.Query.PDUType := PDUTrapV2;

  Result:=snmpsend.SendTrap;
  

//  if snmpsend.SendTrap then
//    edit4.Text:= 'success'
//  else
//    edit4.Text:= 'failed';

    
end;

// ---------------------------------------------------------------
function TSNMP_wrap.Send_code(aCode: Integer; aMsg: string): Boolean;
// ---------------------------------------------------------------
var
  snmpsend: TSNMPSend;

  oItem: TSNMP_msg;
begin
  Result:=False;
  
  if not Items.TryGetValue (aCode, oItem) then
    Exit;         
         


  snmpsend := TSNMPSend.Create;

  snmpSend.TargetHost := FHost; // '192.168.0.184'; //IP of SNMP server

//  snmpSend.TargetHost := '127.0.0.1'; //IP of SNMP server     OID
//  snmpSend.TargetPort := IntToStr(FPort); //  '162'; //FPort of SNMP server
  snmpSend.Query.Version:= 1;
  snmpsend.Query.Community := 'public';
  
  snmpsend.Query.MIBAdd (oItem.OID, GetComputerName() + ' - ' + oItem.Msg+' '+ aMsg , ASN1_OCTSTR);  
  snmpsend.Query.PDUType := PDUTrapV2;

  Result:=snmpsend.SendTrap;
  

//  if snmpsend.SendTrap then
//    edit4.Text:= 'success'
//  else
//    edit4.Text:= 'failed';

    
end;




function GetComputerName: string;
const
  MAX_COMPUTERNAME_LENGTH = 00;
var
  buffer: array[0..MAX_COMPUTERNAME_LENGTH + 1] of Char;
  Size: Cardinal;
begin
  Size := MAX_COMPUTERNAME_LENGTH + 1;
  Windows.GetComputerName(@buffer, Size);
  Result := StrPas(buffer)
end;


constructor TSNMP_msg.Create_(aOID: string; aMsg: string);
begin
  inherited;

  Msg:=aMsg;
  OID:=aOID;
  
end;



end.


//
//
// enum Message_type
//  {
//    ERROR_PROFILE = 0,
//    ERROR_DB_CONNECT = 1,
//    ERROR_LICENCE = 2
//  };




// 
//
// [main]
//Host=192.168.0.184
//Port=162
//
//[error_profile]
//MIB=1.2.2.2.2.2.2.2
//value=error_profile
//
//[error_db_conn]
//MIB=1.2.2.2.2.2.2.3
//value=error_db_conn
//
//

//  TOptionsStyleList= class(TList<TOptionsObjectStyle>);
////  TOptionsStyleList= class(TDictionary<String, TOptionsObjectStyle>);



//
//
//procedure TForm1.Button2Click(Sender: TObject);
//var
//  snmpsend: TSNMPSend;
//begin
//  snmpsend := TSNMPSend.Create;
//
//  snmpSend.TargetHost := '192.168.0.184'; //IP of SNMP server
//
////  snmpSend.TargetHost := '127.0.0.1'; //IP of SNMP server
//  snmpSend.TargetPort := '162'; //Port of SNMP server
//  snmpSend.Query.Version:= 1;
//  snmpsend.Query.Community := 'public';
//  snmpsend.Query.MIBAdd('1.2.2.2.2.2.2.2','TEST TRAP', ASN1_OCTSTR);
//  snmpsend.Query.MIBAdd('1.3.3.3.3.3.3.3','TEST TRAP', ASN1_OCTSTR);  
//  snmpsend.Query.PDUType := PDUTrapV2;
//  if snmpsend.SendTrap then
//    edit4.Text:= 'success'
//  else
//    edit4.Text:= 'failed';
//end;

//

