unit Snmp_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 03.05.2019 23:35:00 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\RPLS_DB\.Parts\SNMP\Project17 (1)
// LIBID: {8C7E482B-B49C-4C48-9B7A-751BD47ED717}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (stdvcl40.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Winapi.Windows, System.Classes, System.Variants, System.Win.StdVCL, Vcl.Graphics, Vcl.OleServer, Winapi.ActiveX;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  SnmpMajorVersion = 1;
  SnmpMinorVersion = 0;

  LIBID_Snmp: TGUID = '{8C7E482B-B49C-4C48-9B7A-751BD47ED717}';

  IID_ISnmpX: TGUID = '{D782B59F-2412-486A-A99D-CB7316146705}';
  CLASS_SnmpX: TGUID = '{606B6CDB-79D7-49FB-84FA-516DA78FD826}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum Message_code
type
  Message_code = TOleEnum;
const
  SNMP_ERROR_PROFILE = $00000000;
  SNMP_ERROR_DB_CONNECT = $00000001;
  SNMP_ERROR_LICENCE = $00000002;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ISnmpX = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  SnmpX = ISnmpX;


// *********************************************************************//
// Interface: ISnmpX
// Flags:     (256) OleAutomation
// GUID:      {D782B59F-2412-486A-A99D-CB7316146705}
// *********************************************************************//
  ISnmpX = interface(IUnknown)
    ['{D782B59F-2412-486A-A99D-CB7316146705}']
    function Send(aCode: Integer; const aMsg: WideString): HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoSnmpX provides a Create and CreateRemote method to          
// create instances of the default interface ISnmpX exposed by              
// the CoClass SnmpX. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSnmpX = class
    class function Create: ISnmpX;
    class function CreateRemote(const MachineName: string): ISnmpX;
  end;

implementation

uses System.Win.ComObj;

class function CoSnmpX.Create: ISnmpX;
begin
  Result := CreateComObject(CLASS_SnmpX) as ISnmpX;
end;

class function CoSnmpX.CreateRemote(const MachineName: string): ISnmpX;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SnmpX) as ISnmpX;
end;

end.

