unit Unit1;

interface

uses
  snmp_TLB,
  
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SNMPsend, StdCtrls, asn1util;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Label1: TLabel;
    Button1: TButton;
    Label2: TLabel;
    Label3: TLabel;
    Edit2: TEdit;
    Label4: TLabel;
    Edit3: TEdit;
    Memo1: TMemo;
    Memo2: TMemo;
    Label5: TLabel;
    Label6: TLabel;
    Button2: TButton;
    Edit4: TEdit;
    Button3: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
var
  response,s:Ansistring;
  n:integer;
begin
  if SNMPget(Edit3.Text,Edit2.Text,Edit1.Text,response)
    then
      begin
        label2.Caption:='Success...';
        memo1.Lines.Clear;
        memo2.Lines.Clear;
        memo1.Lines.Text:=response;
        s:='';
        for n:=0 to Length(response) do
          begin
            s:=s+'$'+IntToHex(Ord(response[n]),2)+' ';
          end;
        memo2.Lines.Text:=s;
      end
    else
      begin
        label2.Caption:='Not contacted... (Check SNMP host, community or MIB OID!)';
        memo1.Lines.Clear;
        memo2.Lines.Clear;
      end;
end;



function GetComputerName: string;
var
  buffer: array[0..MAX_COMPUTERNAME_LENGTH + 1] of Char;
  Size: Cardinal;
begin
  Size := MAX_COMPUTERNAME_LENGTH + 1;
  Windows.GetComputerName(@buffer, Size);
  Result := StrPas(buffer)
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  ShowMessage(GetComputerName);
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  snmpsend: TSNMPSend;
begin
  snmpsend := TSNMPSend.Create;

  snmpSend.TargetHost := '192.168.0.184'; //IP of SNMP server

//  snmpSend.TargetHost := '127.0.0.1'; //IP of SNMP server
  snmpSend.TargetPort := '162'; //Port of SNMP server
  snmpSend.Query.Version:= 1;
  snmpsend.Query.Community := 'public';
  snmpsend.Query.MIBAdd('1.2.2.2.2.2.2.2','TEST TRAP', ASN1_OCTSTR);
  snmpsend.Query.MIBAdd('1.3.3.3.3.3.3.3','TEST TRAP', ASN1_OCTSTR);  
  snmpsend.Query.PDUType := PDUTrapV2;
  if snmpsend.SendTrap then
    edit4.Text:= 'success'
  else
    edit4.Text:= 'failed';
end;




procedure TForm1.Button3Click(Sender: TObject);
begin
  CoSnmpX.Create.Send(0, 'dfgsdfgsdfgsd');
end;



end.

