object dlg_Wizard: Tdlg_Wizard
  Left = 1394
  Top = 421
  Caption = 'Custom Wizard'
  ClientHeight = 458
  ClientWidth = 492
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -10
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ActionList1: TActionList
    Left = 321
    Top = 84
    object act_Ok: TAction
      Category = 'Main'
      Caption = 'Ok'
      OnExecute = act_OkExecute
    end
    object act_Cancel: TAction
      Category = 'Main'
      Caption = #1054#1090#1084#1077#1085#1072
      OnExecute = act_CancelExecute
    end
  end
  object FormStorage1: TFormStorage
    Active = False
    IniFileName = 'Software\Onega\'
    Options = [fpPosition]
    StoredValues = <>
    Left = 288
    Top = 84
  end
  object cxPropertiesStore: TcxPropertiesStore
    Active = False
    Components = <>
    StorageName = 'cxPropertiesStore'
    StorageType = stRegistry
    Left = 127
    Top = 77
  end
end
