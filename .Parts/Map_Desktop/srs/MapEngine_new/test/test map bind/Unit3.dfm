object Form3: TForm3
  Left = 266
  Top = 309
  Width = 863
  Height = 471
  Caption = 'Form3'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Map1: TMap
    Left = 0
    Top = 0
    Width = 409
    Height = 437
    ParentColor = False
    Align = alLeft
    TabOrder = 0
    ControlData = {
      8D1A0600452A00002A2D0000010000000F0000FF0678456D70747905456D7074
      7900E8030000000000000000000002000E001E00000000000000000000000000
      0000000000000000000000000000000000000600010000000000500001030000
      640000000001F4010000050000800C000000000000000000000000FFFFFF0001
      00000000000000000000000000000000000000000000000352E30B918FCE119D
      E300AA004BB85101CC000090011009050005417269616C000352E30B918FCE11
      9DE300AA004BB85101CC00009001348C030005417269616C0000000000000000
      00000000000000000000000000000000000000000000000000000000FFFFFF00
      0000000000000001370000000000FFFFFF000000000000000352E30B918FCE11
      9DE300AA004BB85101CC00009001DC7C010005417269616C000352E30B918FCE
      119DE300AA004BB851010200009001A42C02000B4D61702053796D626F6C7300
      00000000000001000100FFFFFF000200FFFFFF00000000000001000000010001
      1801000080041500010000007811DA5A1C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000002000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000008076C0
      00000000008056C0000000000080764000000000008056400000000018010000
      8004150001000000AA0200001C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000009841CF002CEB13002CEB1300
      D98B417E8810477E1F00000064EB1300F796427E000000000000000000000088
      B3400000000000408F400001000001}
  end
  object Button1: TButton
    Left = 432
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 1
    OnClick = Button1Click
  end
  object DBGrid1: TDBGrid
    Left = 432
    Top = 280
    Width = 393
    Height = 137
    DataSource = DataSource1
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object Button2: TButton
    Left = 672
    Top = 61
    Width = 123
    Height = 25
    Caption = 'CreateLayerFromDb'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 672
    Top = 98
    Width = 75
    Height = 25
    Caption = 'bind'
    TabOrder = 4
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 672
    Top = 24
    Width = 75
    Height = 25
    Caption = 'PropertyPage'
    TabOrder = 5
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 672
    Top = 136
    Width = 91
    Height = 25
    Caption = 'AutoLabel'
    TabOrder = 6
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 440
    Top = 120
    Width = 75
    Height = 25
    Caption = 'OpenDB'
    TabOrder = 7
    OnClick = Button6Click
  end
  object Button7: TButton
    Left = 432
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Button7'
    TabOrder = 8
  end
  object Button8: TButton
    Left = 624
    Top = 216
    Width = 75
    Height = 25
    Caption = 'Button8'
    TabOrder = 9
  end
  object Button9: TButton
    Left = 712
    Top = 216
    Width = 75
    Height = 25
    Caption = 'Button9'
    TabOrder = 10
  end
  object ADOConnection1: TADOConnection
    CommandTimeout = 60
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega_1'
    ConnectionTimeout = 30
    LoginPrompt = False
    Left = 560
    Top = 64
  end
  object qry_Temp: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select top 5000 id, lat,lon,name , checksum(lat,lon) as checksum'
      'from property')
    Left = 560
    Top = 8
  end
  object DataSource1: TDataSource
    DataSet = qry_Temp
    Left = 464
    Top = 208
  end
end
