unit x_map_desktop;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  u_func,

  dxCore,
  CodeSiteLogging,

  Windows, ActiveX, Classes, ComObj, MapDesktop_TLB, StdVcl;

type
  TMapDesktopX = class(TTypedComObject, IMapDesktopX)
  protected
    function Dlg: HResult; stdcall;
  end;

implementation

uses ComServ;

function TMapDesktopX.Dlg: HResult;
begin

end;


initialization
  {$IFDEF dll}

  dxInitialize;

  TTypedComObjectFactory.Create(ComServer, TMapDesktopX, Class_MapDesktopX,
    ciMultiInstance, tmApartment);

  {$ENDIF}            
finalization
  {$IFDEF dll}

  //!!!!!!!!!!!!!!
  CloseApplicationForms;
  
  
  //CodeSite.Send('dxFinalize');
  
  dxFinalize;
  {$ENDIF}    
end.




(*


initialization
  {$IFDEF dll}

  dxInitialize;
 
  TTypedComObjectFactory.Create(ComServer, TReliefX, Class_ReliefX,  ciMultiInstance, tmApartment);

  {$ENDIF}        
finalization
  {$IFDEF dll}

  //!!!!!!!!!!!!!!
  CloseApplicationForms;
  
  
  //CodeSite.Send('dxFinalize');
  
  dxFinalize;
  {$ENDIF}    
end.