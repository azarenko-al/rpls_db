unit d_Wizard;

interface

uses
  Classes, Controls, Forms, cxPropertiesStore, rxPlacemnt, ActnList, StdCtrls, ExtCtrls,
  cxClasses, System.Actions
  ;

type
  Tdlg_Wizard = class(TForm)
    ActionList1: TActionList;
    act_Ok: TAction;
    act_Cancel: TAction;
    FormStorage1: TFormStorage;
    cxPropertiesStore: TcxPropertiesStore;
    procedure act_CancelExecute(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  protected
    FRegPath: string;

  protected
    procedure AddComponentProp(aComponent: TComponent; aField: string);
    procedure HideHeader;
    procedure SetActionName (aValue: string);

    procedure SetDefaultSize();
    procedure SetDefaultWidth();
  public
    procedure RestoreFrom;
  end;


const
  DEF_PropertiesValue ='Properties.Value';


//==================================================================
implementation {$R *.DFM}
//==================================================================

const
  REGISTRY_COMMON_FORMS  = 'Software\Onega\Common\Forms\';
                                                                

procedure cx_AddComponentProp(aPropertiesStore: TcxPropertiesStore; aComponent: TComponent; aFieldName: string);
var
  oStoreComponent: TcxPropertiesStoreComponent;
begin
  oStoreComponent :=  TcxPropertiesStoreComponent(aPropertiesStore.Components.Add);

  oStoreComponent.Component:=aComponent;
  oStoreComponent.Properties.Add(aFieldName);
end;


//-------------------------------------------------------------------
procedure Tdlg_Wizard.FormCreate(Sender: TObject);
//-------------------------------------------------------------------

begin
  inherited;

  Assert(not FormStorage1.Active);
         
  Panel3.Width := 185;

  btn_Ok.Left := 5;
  btn_Ok.Anchors:=[akTop, akLeft];

  btn_Cancel.Left := 90;
  btn_Cancel.Anchors:=[akTop, akLeft];


//  (akTop, akLeft, akRight, akBottom);


  lb_Action.Top:=8;
  lb_Action.Left:=5;
 // pn_Header.BorderWidth:=0;

  pn_Header.Visible := True;

  btn_Ok.Action:=act_Ok;

  FRegPath:= REGISTRY_COMMON_FORMS + ClassName +'\1\';

  Assert(not FormStorage1.Active);

  FormStorage1.IniFileName:=FRegPath + FormStorage1.name;
  FormStorage1.IniSection:='Window';
  FormStorage1.Active:=True;


  Assert(not cxPropertiesStore.Active);

  cxPropertiesStore.StorageName:=FRegPath + cxPropertiesStore.Name;
  cxPropertiesStore.Active := True;

 // SetActionName('');

 // if not pn_Header.Visible then
//    AlignTop();

end;


procedure Tdlg_Wizard.SetActionName(aValue: string);
begin
  lb_Action.Caption:=aValue;
  pn_Top_.Visible := True;
end;


procedure Tdlg_Wizard.SetDefaultSize;
begin
  with Constraints do begin
    MaxHeight:=385;
    MinHeight:=MaxHeight;
  end;

  SetDefaultWidth();
end;


procedure Tdlg_Wizard.SetDefaultWidth();
begin
  with Constraints do begin
    MaxWidth :=505;
    MinWidth :=MaxWidth;
  end;
end;



procedure Tdlg_Wizard.AddComponentProp(aComponent: TComponent; aField: string);
begin
  cx_AddComponentProp(cxPropertiesStore, aComponent, aField);
end;


procedure Tdlg_Wizard.RestoreFrom;
begin
  cxPropertiesStore.RestoreFrom;
end;




procedure Tdlg_Wizard.act_CancelExecute(Sender: TObject);
begin
  Close;
end;

procedure Tdlg_Wizard.act_OkExecute(Sender: TObject);
begin
  //!!!!!!!!!!!!!
end;

procedure Tdlg_Wizard.HideHeader;
begin
  pn_Top_.Visible := False;
//  pn_Header.Visible := True;
end;


end.

