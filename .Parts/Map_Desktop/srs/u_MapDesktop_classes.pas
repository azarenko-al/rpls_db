unit u_MapDesktop_classes;

interface

uses Classes,SysUtils, MapXLib_TLB, 

//  TOptionsStyleList= class(TList<TOptionsObjectStyle>);


  u_Mapx_Style,
  u_func,

//  SysUtils, Variants,
  System.Generics.Collections, //Data.DB, Xml.XMLIntf, Data.Win.ADODB;

  
  u_const_db,

  DB;

type
  TMapKind = (mkNone, mkGeoMap,mkObjectMap,mkCalcMap);

  // -------------------------------------------------------------------
  TMapItem = class //(TCollectionItem)
  // -------------------------------------------------------------------
  private
    FFileName: string;

//    procedure Assign(aSource: TPersistent); //override;
    procedure SetFileName(const Value: string);
  public
    MapKind       : TMapKind;

 //   ID1            : Integer; // record ID
    map_ID         : Integer;

    Caption       : string;
    Name          : string;
    
    ObjectName    : string;

    CalcRegion_ID: integer;

    AutoLabel     : boolean;
    IsLabelStyleCnanged1 : Boolean;  //����� ��� ������ � �������

    ZoomMin       : integer;
    ZoomMax       : integer;
    IsZoomAllowed : Boolean;

    IsSelectable  : boolean;
    IsEditable    : boolean;
    IsTransparent : boolean;

    IsFiltered11   : Boolean;

   // AllowZoom     : boolean;

 //   MapAlignment  : (maNone,maTop,maBottom);

//    Visible : Boolean;  //���������� ����� � ����

    RunTime: record
               IS_FILE_EXISTS : Boolean;
             end;

    LabelProperties: TmiLabelProperties;

    constructor Create; //(Collection: TCollection); override;
    destructor Destroy; override;

    procedure ApplyStyle(aLayer: CMapXLayer);

    property FileName: string read FFileName write SetFileName;
  end;


//  TOptionsStyleList= class(TList<TOptionsObjectStyle>);


  // ---------------------------------------------------------------
  TMapItemList = class(TList<TMapItem>)
  // ---------------------------------------------------------------
  private
 //   function GetItems(Index: Integer): TMapItem;
    procedure LoadFromDataset_1111111111111(aDataset: TDataset; aFileDir: string);
  public
 //   Visible      : Boolean;  //�������� ������ ����
  //  DisplayIndex : Integer;

//    MapKind1: TMapKind;
  //  IniSection: string;


  (*  IsShowObjectMap: Boolean;
    IsShowLabelsForObjectMap: Boolean;
    IsShowRegionMap: Boolean;
    IsShowCalcMap: Boolean;
    IsTransparent: boolean;
*)
//    constructor Create;

//    function AddItem: TMapItem;

    function FindByFileName(aFileName: string): TMapItem;

    function IndexByObjectName(aObjectName: string): Integer;

//    procedure SaveToIniFile(aFileName: string);
//    procedure LoadFromIniFile(aFileName: string);

    function FileNameExists(aFileName: string): boolean;
    function FindByObjectName(aObjectName: string): TMapItem;

 //   property Items[Index: Integer]: TMapItem read GetItems; default;
  end;


  // ---------------------------------------------------------------
  TMapDesktopItem = class //(TCollectionItem)
  // ---------------------------------------------------------------
  private

  public
    IsShowGeoMap: Boolean;
    IsShowCalcMap: Boolean;
    IsShowObjectMap: Boolean;

    is_calc_map_at_bottom : Boolean;


    // -------------------------
    MapList: TMapItemList;

    constructor Create; //(Collection: TCollection); // override;
    destructor Destroy; override;

  end;

//
//  TMapDesktopList1 = class(TCollection)
//  private
//    function GetItems(Index: Integer): TMapDesktopItem;
//  public
//    constructor Create;
//
//    property Items[Index: Integer]: TMapDesktopItem read GetItems; default;
//  end;



implementation

//constructor TMapItemList.Create;
//begin
//  inherited Create (TMapItem);
//end;

//function TMapItemList.AddItem: TMapItem;
//begin
//  Result := TMapItem(inherited Add);
////  Result.MapKind :=MapKind1;
//end;


function TMapItemList.FindByFileName(aFileName: string): TMapItem;
var I: Integer;
begin
  Result := nil;

  for I := 0 to Count - 1 do
    if Eq(Items[i].FileName, aFileName) then
    begin
      Result := Items[i];
      Exit;
    end;
end;


function TMapItemList.FileNameExists(aFileName: string): boolean;
var
  oMapItem: TMapItem;
begin
  oMapItem  := FindByFileName(aFileName);
  Result := Assigned(oMapItem);// and (oMapItem.Visible);
end;


function TMapItemList.FindByObjectName(aObjectName: string): TMapItem;
var I: Integer;
begin
  Result := nil;

  for I := 0 to Count - 1 do
    if Items[i].ObjectName<>'' then
    if Eq(Items[i].ObjectName, aObjectName) then
    begin
      Result := Items[i];
      Exit;
    end;
end;


//function TMapItemList.GetItems(Index: Integer): TMapItem;
//begin
//  Result := TMapItem(inherited Items[Index]);
//end;

// ---------------------------------------------------------------
function TMapItemList.IndexByObjectName(aObjectName: string): Integer;
// ---------------------------------------------------------------
var I: Integer;
begin
  Result := -1;

  for I := 0 to Count - 1 do
    if Eq(Items[i].ObjectName, aObjectName) then
    begin
      Result := i;
      Exit;
    end;
end;


constructor TMapItem.Create; //(Collection: TCollection);
begin
  inherited Create; //(Collection);
  LabelProperties := TmiLabelProperties.Create();
end;

destructor TMapItem.Destroy;
begin
  FreeAndNil(LabelProperties);
  inherited Destroy;
end;

//
//// ---------------------------------------------------------------
//procedure TMapItem.Assign(aSource: TPersistent);
//// ---------------------------------------------------------------
//var
//  oSrc: TMapItem;
//begin
//  if aSource is TMapItem then
//  begin
//    oSrc:=TMapItem(aSource);
//    //------------------------------------
//    FileName      :=oSrc.FileName;
//    Caption       :=oSrc.Caption;
//    Name          :=oSrc.Name;
//    ObjectName    :=oSrc.ObjectName;
//
//    CalcRegion_ID :=oSrc.CalcRegion_ID;
//
//    AutoLabel     :=oSrc.AutoLabel;
//  //  LabelBackColor:=oSrc.LabelBackColor;
//
//   // MapType       :=oSrc.MapType;
//
//    IsZoomAllowed   :=oSrc.IsZoomAllowed;
//    ZoomMin       :=oSrc.ZoomMin;
//    ZoomMax       :=oSrc.ZoomMax;
//
//    IsSelectable  :=oSrc.IsSelectable;
//    IsEditable    :=oSrc.IsEditable;
//    IsTransparent :=oSrc.IsTransparent;
//
////    IsFiltered    :=oSrc.IsFiltered;
//
//
// //   Visible  := osrc.Visible;
//  //  MapAlignment  :=oSrc.MapAlignment;
//  end;
//end;


procedure TMapItem.SetFileName(const Value: string);
begin
  Assert(Value<>'', 'Value <>''''');
  FFileName := Value;
end;



//constructor TMapDesktopList1.Create;
//begin
//  inherited Create (TMapDesktopItem);
//end;
//
//function TMapDesktopList1.GetItems(Index: Integer): TMapDesktopItem;
//begin
//  Result := TMapDesktopItem(inherited Items[Index]);
//end;

// ---------------------------------------------------------------
procedure TMapItemList.LoadFromDataset_1111111111111(aDataset: TDataset;
    aFileDir: string);
// ---------------------------------------------------------------
var
  oMapItem: TMapItem;
  s: string;
begin
  Clear;

  with aDataset do
    while not EOF do
  begin
    oMapItem :=TMapItem.Create;
    Add(oMapItem);
    
    oMapItem.FileName:= aFileDir + FieldByName(FLD_FILENAME).AsString;

    oMapItem.ZoomMin := FieldByName(FLD_ZOOM_MIN).AsInteger;
    oMapItem.ZoomMax := FieldByName(FLD_ZOOM_MAX).AsInteger;

    oMapItem.IsZoomAllowed:= FieldByName(FLD_ALLOW_ZOOM).AsBoolean;
    oMapItem.AutoLabel    := FieldByName(FLD_AutoLabel).AsBoolean;

    s:=FieldByName(FLD_label_style).AsString;

    oMapItem.LabelProperties.AsText := s;

    oMapItem.RunTime.IS_FILE_EXISTS := FileExists (oMapItem.FileName);

    Next;
  end;
end;

// ---------------------------------------------------------------
constructor TMapDesktopItem.Create; //(Collection: TCollection);
// ---------------------------------------------------------------
begin
  inherited Create; //(Collection);

  MapList := TMapItemList.Create();

end;

// ---------------------------------------------------------------
destructor TMapDesktopItem.Destroy;
// ---------------------------------------------------------------
begin
  FreeAndNil(MapList);

  inherited Destroy;
end;

// ---------------------------------------------------------------
procedure TMapItem.ApplyStyle(aLayer: CMapXLayer);
// ---------------------------------------------------------------
begin
  Assert(Assigned (aLayer));


  aLayer.ZoomLayer:=IsZoomAllowed;

  if (ZoomMax>0)  then
  begin
    aLayer.ZoomMin:=ZoomMin;
    aLayer.ZoomMax:=ZoomMax;
  end;



  case aLayer.Type_ of

    miLayerTypeRaster: begin
      aLayer.ZoomLayer:=False;

    //    vLayer.ZoomLayer:=Map1.Layers.Move (Map1.Layers.Count, 0);
    end;

    miLayerTypeNormal:
    begin
    //  aLayer.Style.RegionTransparent := True;



     // if aLayer.AutoLabel <> AutoLabel then
        aLayer.AutoLabel := AutoLabel;

    //  if aLayer.Selectable<> IsSelectable then
        aLayer.Selectable:= IsSelectable;

    //  if aLayer.Editable <> IsEditable then
        aLayer.Editable  := IsEditable;

//      aLayer.AutoLabel := aRec.AutoLabel;
 //     aLayer.Selectable:= aRec.IsSelectable;
  //    aLayer.Editable  := aRec.IsEditable;

    //  if aRec.IsTransparent then
     //   mapx_MakeLayerTransparent(aLayer);


    //  aLayer.AutoLabel := AutoLabel;

      // oMapItem.AutoLabel and    //(//) (oMapItem.IsLabelStyleCnanged1)
      if (LabelProperties.Text<>'') then
        LabelProperties.SaveToMapinfoLayer (aLayer);

    end;
  end;


end;




end.


