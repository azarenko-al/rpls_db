object dmMap_Desktop: TdmMap_Desktop
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 1298
  Top = 617
  Height = 310
  Width = 602
  object ds_Desktops: TDataSource
    DataSet = ADOStoredProc_MapDesktops
    Left = 220
    Top = 96
  end
  object ADOStoredProc_Maps: TADOStoredProc
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 400
    Top = 24
  end
  object ADOStoredProc_Object_Maps: TADOStoredProc
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 400
    Top = 96
  end
  object ADOStoredProc_CalcMaps: TADOStoredProc
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 400
    Top = 152
  end
  object ADOStoredProc_MapDesktops: TADOStoredProc
    LockType = ltBatchOptimistic
    AfterPost = ADOStoredProc_MapDesktopsAfterPost
    Parameters = <>
    Left = 224
    Top = 32
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=SERVER1;Use Procedu' +
      're for Prepare=1;Auto Translate=True;Packet Size=4096;Workstatio' +
      'n ID=ALEX;Use Encryption for Data=False;Tag with column collatio' +
      'n when possible=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 33
    Top = 24
  end
end
