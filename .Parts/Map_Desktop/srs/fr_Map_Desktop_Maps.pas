unit fr_Map_Desktop_Maps;

interface


uses
  cxCustomData, SysUtils,  cxGridLevel, cxGraphics, cxGrid,
  cxGridCustomView, StdCtrls, ExtCtrls, cxStyles, MapXLib_TLB, DBCtrls,
  Windows, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, Menus, Db, ADODB, Variants, 
  cxGridBandedTableView, ImgList, cxClasses, cxControls,  cxCheckBox,
  ToolWin, ComCtrls, AppEvnts, Grids, DBGrids, cxInplaceContainer, cxTL,
  cxGridCustomTableView, cxGridTableView, cxGridDBBandedTableView,

  u_log,

 // dm_Act_MapFile,

//  u_Storage,

  dm_Onega_DB_data,

 // I_MapFile,

  dm_Map_Desktop,

  u_MapX,

//  i_MapX_tools,

  d_Map_Label_Setup,


//  dm_MapFile,

//  u_run,

  u_func,
  u_cx,
  u_db,

//  u_vars,

//  u_const,
//  u_const_db,
//  u_const_msg, 
//
//  cxLookAndFeels, cxLookAndFeelPainters, cxFilter, cxData,
//  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxSpinEdit, cxButtonEdit,
//  System.Actions,
//
  u_const_str,


  cxDBTL, cxTLData, cxLookAndFeels, cxLookAndFeelPainters, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxSpinEdit, cxButtonEdit,
  System.Actions;


type
  Tframe_Map_Desktop_Maps = class(TForm)
    act_Zoom_Allow: TAction;
    act_Bottom: TAction;
    act_Check: TAction;
    act_Check_All: TAction;
    act_Zoom_Disallow: TAction;
    act_Down: TAction;
    act_Map_Add: TAction;
    act_Map_Del: TAction;
    act_Zoom_SetMax: TAction;
    act_Zoom_SetMin: TAction;
    act_Top: TAction;
    act_UnCheck: TAction;
    act_Uncheck_All: TAction;
    act_Up: TAction;
    ActionList2: TActionList;
    actSetMaxZoom1: TMenuItem;
    actSetMinZoom1: TMenuItem;
    Check1: TMenuItem;
    menu_Maps: TPopupMenu;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    UnCheck1: TMenuItem;
    ImageList2: TImageList;
    ActionList1_new: TActionList;
    act_Label_change: TAction;
    ds_Maps: TDataSource;
    cxGrid1: TcxGrid;
    cxGridDBBandedTableView1: TcxGridDBBandedTableView;
    col_name: TcxGridDBBandedColumn;
    col_Checked: TcxGridDBBandedColumn;
    col_zoom_max: TcxGridDBBandedColumn;
    col_zoom_min: TcxGridDBBandedColumn;
    col_zoom_allow: TcxGridDBBandedColumn;
    col_AutoLabel: TcxGridDBBandedColumn;
    cxGridDBBandedColumn811: TcxGridDBBandedColumn;
    col_ID: TcxGridDBBandedColumn;
    col_FileExists: TcxGridDBBandedColumn;
    col_Map_index: TcxGridDBBandedColumn;
    col_label_style: TcxGridDBBandedColumn;
    cxGridLevel1: TcxGridLevel;
    N1: TMenuItem;
    N2: TMenuItem;
    actUp1: TMenuItem;
    actTop1: TMenuItem;
    actBottom1: TMenuItem;
    actDown1: TMenuItem;
    col_priority: TcxGridDBBandedColumn;
    ActionList_move: TActionList;
    act_Up1: TAction;
    act_Down1: TAction;
    Action3: TAction;
    Action4: TAction;
    col_Map_ID: TcxGridDBBandedColumn;
    act_Refresh: TAction;
    N3: TMenuItem;
    N4: TMenuItem;
    ADOStoredProc_Maps: TADOStoredProc;
    act_Check_Exists: TAction;
    acrCheckExists1: TMenuItem;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_strikeout: TcxStyle;
    col_is_file_exists: TcxGridDBBandedColumn;
    ToolBar1: TToolBar;
    cb_Check_All: TDBCheckBox;
    ApplicationEvents1: TApplicationEvents;
    act_Show_in_Map: TAction;
    actShowinMap1: TMenuItem;
    act_Add_GeoRegion_map: TAction;
    actAddGeoRegionmap1: TMenuItem;
    ADOStoredProc_Mapsid: TIntegerField;
    ADOStoredProc_MapsMap_Desktop_id: TIntegerField;
    ADOStoredProc_MapsMap_id: TIntegerField;
    ADOStoredProc_MapsCalcMap_id: TIntegerField;
    ADOStoredProc_MapsMapObject_name: TStringField;
    ADOStoredProc_Mapszoom_min: TIntegerField;
    ADOStoredProc_Mapszoom_max: TIntegerField;
    ADOStoredProc_Mapsallow_zoom: TBooleanField;
    ADOStoredProc_Mapspriority: TIntegerField;
    ADOStoredProc_Mapschecked: TBooleanField;
    ADOStoredProc_Mapsis_file_exists: TBooleanField;
    ADOStoredProc_MapsAutoLabel: TBooleanField;
    ADOStoredProc_Mapslabel_style: TMemoField;
    ADOStoredProc_Mapsname: TStringField;
    ADOStoredProc_Mapsis_file_exists_: TBooleanField;
    ADOStoredProc_Mapsshort_filename: TStringField;
    col_short_filename: TcxGridDBBandedColumn;
    ADOStoredProc1__11111: TADOStoredProc;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    StringField1: TStringField;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    BooleanField1: TBooleanField;
    IntegerField7: TIntegerField;
    BooleanField2: TBooleanField;
    BooleanField3: TBooleanField;
    BooleanField4: TBooleanField;
    MemoField1: TMemoField;
    StringField2: TStringField;
    BooleanField5: TBooleanField;
    StringField3: TStringField;
    DataSource1: TDataSource;
    procedure ActionList2Update(Action: TBasicAction; var Handled: Boolean);
    procedure ADOStoredProc_MapsCalcFields(DataSet: TDataSet);
//    procedure act_Check_ExistsHint(var HintStr: string; var CanShow: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure Move_Actions(Sender: TObject);
  //  procedure ActionList_moveUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure DoOnExec(Sender: TObject);
  //  procedure act_Show_in_MapExecute(Sender: TObject);
 //   procedure act_Label_changeExecute(Sender: TObject);
//    procedure act_RefreshExecute(Sender: TObject);
 //   procedure ADOStoredProc_MapsCalcFields(DataSet: TDataSet);
    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
  //  procedure Button1Click(Sender: TObject);

  //  procedure col_AutoLabelToggleClick(Sender: TObject; const Text: string; State:
    //    TdxCheckBoxState);

 //   procedure dx_MapsExit(Sender: TObject);
  //  procedure col_CheckedToggleClick(Sender: TObject; const Text: String; State: TdxCheckBoxState);
  //  procedure col_Label_StyleEditButtonClick(Sender: TObject);
     procedure col__Label_StylePropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
//    procedure col_CheckedPropertiesChange(Sender: TObject);
    procedure cxGrid1Exit(Sender: TObject);
    procedure col_AutoLabelPropertiesChange(Sender: TObject);
    procedure col_CheckedHeaderClick(Sender: TObject);
    procedure col_zoom_allowHeaderClick(Sender: TObject);
    procedure DoAfterPost(DataSet: TDataSet);
    procedure cxGridDBBandedTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure cb_Check_AllClick(Sender: TObject);

//    procedure cxGridDBBandedTableView1CustomDrawCell(Sender:
//        TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo:
//        TcxGridTableDataCellViewInfo; var ADone: Boolean);
 //   procedure act_Show_in_MapExecute(Sender: TObject);

  private
    FID : Integer;
    FMap: TMap;    
    FDataSet: TDataSet;

    FOnMapChecked: TNotifyEvent;
    
//    FRegPath: string;

    procedure Check_All;
    procedure Dlg_Label_Style_new;

    procedure DoMapChecked;
    procedure CheckIfExists;
    procedure Zoom_All;

  public
//    IsAutoUpdate : Boolean;

    procedure View(aID: integer; aMap: TMap=nil);

    property OnMapChecked: TNotifyEvent read FOnMapChecked write FOnMapChecked;

  end;


implementation
{$R *.dfm}         


//--------------------------------------------------------------------
procedure Tframe_Map_Desktop_Maps.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
var
  I: Integer;
  s: string;
begin
  inherited;


  cb_Check_All.DataSource := dmMap_Desktop.ds_Desktops;
  cb_Check_All.Caption:='�������� ����';

  act_Show_in_Map.Caption:='�������� �� �����';


  act_Add_GeoRegion_map.Caption:='�������� ����� ���. ��������';


 // Assert(Assigned(IMapFile));
//  FDataSet :=dmMap_Desktop.mem_Maps;
  FDataSet :=ADOStoredProc_Maps;

  cxGrid1.Align:=alClient;

//  cxGridDBBandedTableView1.DataController.DataSource:=


(*  dx_Maps.Align    := alClient;
  dx_Maps.KeyField := FLD_DXTREE_ID;
  dx_Maps.ParentField := FLD_DXTREE_PARENT_ID;
*)
 // cxGrid1.Align := alClient;

//  dx_Maps.DataSource    := dmMap_Desktop.ds_Maps;

  Assert(Assigned(dmMap_Desktop), 'Value not assigned');

 // ds_Maps.DataSet:=FDataSet;

  act_Zoom_SetMax.Caption := '��������� ������������ �������';
  act_Zoom_SetMin.Caption := '��������� ����������� �������';

  act_Check.Caption      := '��������';
  act_UnCheck.Caption    := '����� �������';

  col_AutoLabel.Caption     := '�������';

  act_Check_All.Caption  := '�������� ���';
  act_UnCheck_All.Caption:= '����� ������� �� ����';

  col_Name.Caption           := STR_FILENAME;
  col_Short_FileName.Caption := STR_NAME;
  col_Checked.Caption        := STR_CHECKED;
  col_priority.Caption       := STR_priority;

////////
  col_ID.Visible:=False;
  col_Map_ID.Visible:=False;

  col_is_file_exists.Visible := False;

  col_Checked.Visible := True;

(*
  col_Caption.Visible := True;
  co_AUTOLABEL.Visible := True;
  col_Style.Visible := True;
*)


(*  col_tree_id.Visible       := False;
  col_parent_tree_id.Visible:= False;
*)

 // col_IS_FILE_EXISTS.FieldName:=FLD_IS_FILE_EXISTS;

(*
  col__Map_Name.Caption      := STR_NAME;
  col__Map_FileName.Caption  := STR_FILENAME;
  col__Checked.Caption       := STR_CHECKED;
  col__AutoLabel.Caption     := '�������';

*)

 // col_Data.Visible := False;



{


  col_ID.Visible:=        dmMain.DEBUG;
  col_Index.Visible:=     dmMain.DEBUG;
//  col_map_index.Visible:= dmMain.DEBUG;

  if not dmMain.DEBUG then begin
    dx_CheckRegistry (dx_Maps,    REGISTRY_FORMS  + ClassName + '\'+ dx_Maps.Name);
    dx_Maps.LoadFromRegistry    (REGISTRY_FORMS  + ClassName + '\'+ dx_Maps.Name);
  end;
}

//  FRegPath:=REGISTRY_COMMON_FORMS + ClassName + '\6\';

//  dx_Maps.LoadFromRegistry (FRegPath + dx_Maps.Name);

//  dmOnega_DB_data.Pmp_Site_Select_Item(ADOQuery1, aID);



 ///////////zzzzzzzzz
//  cxGridDBBandedTableView1.RestoreFromRegistry (FRegPath + cxGridDBBandedTableView1.Name);

  //g_Storage.RestoreFromRegistry(cxGridDBBandedTableView1, className);

  cxGrid_RemoveUnusedColumns(cxGridDBBandedTableView1);


//  for I := cxGridDBBandedTableView1.ColumnCount - 1  downto 0 do
//  begin
//    s:=cxGridDBBandedTableView1.Columns[i].Caption;
//
//    if s='' then
////      cxGridDBBandedTableView1.Columns.re
//      cxGridDBBandedTableView1.Columns[i].Free;
//
//(*
//    s:=cxGridDBBandedTableView1.Columns[i].Name;
//    g_Log.Add('name: '+ s);
//
//
//    s:=cxGridDBBandedTableView1.Columns[i].Caption;
//    g_Log.Add('Caption: '+ s);
//*)
//  end;



 /////////// col_Map_index.Sorted:=csUp;

 ///////// dmMap_Desktop.mem_Maps.SortedField:=FLD_map_index;



end;

//--------------------------------------------------------------------
procedure Tframe_Map_Desktop_Maps.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  db_PostDataset(ADOStoredProc_Maps);

  FDataset:=nil;


//  if not dmMain.DEBUG then begin
//  dx_Maps.SaveToRegistry (FRegPath + dx_Maps.Name);
//  end;
//  cxGridDBBandedTableView1.StoreToRegistry (FRegPath + cxGridDBBandedTableView1.Name);

  //g_Storage.StoreToRegistry(cxGridDBBandedTableView1, className);

  inherited;
end;


//-------------------------------------------------------------------
procedure Tframe_Map_Desktop_Maps.Move_Actions(Sender: TObject);
//-------------------------------------------------------------------
var
  i: Integer;
  k: Integer;
  iID: integer;
begin
  if ((Sender=act_Down) or (Sender=act_Bottom)) and FDataset.Eof then
    Exit;

  if ((Sender=act_Up) or (Sender=act_Top)) and FDataset.Bof then
    Exit;


  if (Sender=act_Up)      then k :=1  else
  if (Sender=act_Down)    then k :=-1 else
  if (Sender=act_Top)     then k :=1000 else
  if (Sender=act_Bottom)  then k :=-1000 else k:=0;

  iID:= FDataset.FieldByName(FLD_ID).AsInteger;

  i := dmOnega_DB_data.MapDesktop_Move_Maps(iID, k);

  View(FID, FMap);
end;


//------------------------------------------------------------------
procedure Tframe_Map_Desktop_Maps.DoOnExec(Sender: TObject);
//------------------------------------------------------------------
var
  bChecked, bAllowZoom: boolean;
  sZoom: string;
  dZoom: double;
  i, iID: integer;
  s: string;

  sFileName: string;
  vLayer: CMapXLayer;
begin


  // ---------------------------------------------------------------
  if Sender=act_Show_in_Map then
  // ---------------------------------------------------------------
  begin
//    sFileName := FDataset.FieldByName(FLD_filename).AsString;
    sFileName := FDataset.FieldByName(FLD_name).AsString;

    vLayer :=mapx_GetLayerByFileName(FMap, sFileName);
    if Assigned(vLayer) then
      mapx_SetBoundsFromLayer(FMap, vLayer);

  end else

//   sFileName := '';

  // -------------------------
  if (Sender = act_Check_Exists) then
  // -------------------------
    CheckIfExists()
  else

//  if (Sender = act_Refresh) then
//    View(FID);


  if (Sender = act_Refresh) then
    View(FID, FMap)
   else

  // ---------------------------------------------------------------
  if Sender=act_Add_GeoRegion_map then
  // ---------------------------------------------------------------
  begin
//    sFileName :=GetApplicationDir () + 'maps\russia.Tab';
    sFileName :=GetApplicationDir () + 'DATA\region.Tab';

//    dmMapFile.AddFileName (sFileName);


    if FileExists(sFileName) then
    begin
      vLayer:=mapx_GetLayerByFileName(FMap,  sFileName);
      if Assigned(vLayer) then
        exit;

      vLayer:=FMap.Layers.Add (sFileName, EmptyParam);
      vLayer.LabelProperties.Visible:=true;

    end else
      ShowMessage('���� �� ������: '+ sFileName);

//    sFileName := g_Appl

  //  ShowMessage('act_Add_GeoRegion_map');
  end else

  // -------------------------------------------------------------------
  if (Sender = act_Map_Add) then
  // -------------------------------------------------------------------
  begin
   // Assert(Assigned(IMapFile));



//    if not dmAct_MapFile.Dlg_LoadMapsFromFiles() then
//      Exit;

 //   Assert(dmMap_Desktop.qry_Desktops.Active);

   // dmOnega_DB_data.MapDesktop_Init (FID);

   /////// View(FID, FMap);

    DoMapChecked();
  end  else

  // -------------------------------------------------------------------
  if (Sender = act_Map_Del) then
  // -------------------------------------------------------------------
  begin
    if not ConfirmDlg('������� ����� ?') then
      Exit;


    with cxGridDBBandedTableView1.Controller do
      for i := 0 to SelectedRowCount - 1 do
      begin
        iID:=SelectedRows[i].Values[col_Map_id.Index];

        sFileName:=SelectedRows[i].Values[col_Name.Index];


//            iID:= FDataset.FieldByName(FLD_ID).AsInteger



//        iID:=SelectedRows[i].Values[col_id.Index];

        mapx_RemoveMapFile (Fmap, sFileName);

        dmOnega_DB_data.Map_Del(iID);
     end;

    View(FID);

  //  DoMapChecked();

  end else

  // -------------------------------------------------------------------
  if (Sender = act_Zoom_Allow) or (Sender = act_Zoom_DisAllow) then
  // -------------------------------------------------------------------
  begin
    bAllowZoom:=(Sender = act_Zoom_Allow);

    cxGridDBBandedTableView1.DataController.SelectAll;

    cx_UpdateSelectedItems_BandedTable(cxGridDBBandedTableView1,
                                // FLD_ID,
                                 FLD_ALLOW_ZOOM, bAllowZoom);

    cxGridDBBandedTableView1.DataController.ClearSelection;

    DoMapChecked;

  end else

  // -------------------------------------------------------------------
  if (Sender = act_Zoom_SetMin) or (Sender = act_Zoom_SetMax) then
  // -------------------------------------------------------------------
  begin
    if InputQuery('�������', '������� �������', sZoom) then
    begin
      dZoom:= AsFloat(sZoom);

      s :=FLD_ZOOM_MIN;

      if Sender = act_Zoom_SetMin then
        s:=FLD_ZOOM_MIN
      else
        s:=FLD_ZOOM_MAX;

      cx_UpdateSelectedItems_BandedTable(cxGridDBBandedTableView1, s, dZoom);

      DoMapChecked;
    end;
  end else

  // -------------------------------------------------------------------
  if (Sender = act_Check) or (Sender = act_UnCheck) then
  // -------------------------------------------------------------------
  begin
    bChecked:=(Sender = act_Check);

    cx_UpdateSelectedItems_BandedTable(cxGridDBBandedTableView1,
                                 FLD_CHECKED, bChecked);

    DoMapChecked;
  end  else

  // -------------------------------------------------------------------
  if (Sender = act_Check_All) or (Sender = act_UnCheck_All) then
  // -------------------------------------------------------------------
  begin
    bChecked:=(Sender = act_Check_All);


    cxGridDBBandedTableView1.DataController.SelectAll;

    cx_UpdateSelectedItems_BandedTable(cxGridDBBandedTableView1,
                                 FLD_CHECKED, bChecked);

    cxGridDBBandedTableView1.DataController.ClearSelection;

    DoMapChecked;

  end  else
    raise Exception.Create('');

end;



// ---------------------------------------------------------------
procedure Tframe_Map_Desktop_Maps.Dlg_Label_Style_new;
// ---------------------------------------------------------------
var
  s: string;
  b: Boolean;
  sTabFileName: string;

begin
  s           := FDataset.FieldByName(FLD_label_style).AsString;
 // sTabFileName:= FDataset.FieldByName(FLD_FILENAME).AsString;
  sTabFileName:= FDataset.FieldByName(FLD_NAME).AsString;


//  Assert(Assigned(oMapItem), 'Value not assigned');

//sTabFileName,

//  if MapX_Label_Setup_Dlg(Application.Handle, s,  FMap) then

  if Tdlg_Map_Label_Setup.ExecDlg_Obj_new(s) then //aTabFileName,        //,  aMap
//  if MapX_Label_Setup_Dlg( s) then
//  if Tdlg_Map_Label_Setup.ExecDlg_Obj_new(s, sTabFileName) then
  begin
    db_UpdateRecord(FDataset, FLD_label_style, s);

    b:= ADOStoredProc_Maps.FieldByName(FLD_AutoLabel).AsBoolean;

    if b then
      DoMapChecked();


(*    oMapItem.IsLabelStyleCnanged := True;

    if oMapItem.AutoLabel then
      DoMapChecked();
*)
  end;
end;



procedure Tframe_Map_Desktop_Maps.col__Label_StylePropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  Dlg_Label_Style_new();
end;          


// ---------------------------------------------------------------
procedure Tframe_Map_Desktop_Maps.View(aID: integer; aMap: TMap=nil);
// ---------------------------------------------------------------
var
  i: Integer;
  iID: integer;
begin
  FID :=aID;

  if aMap<>nil then
    FMap:=aMap;


//  db_ViewDataset(FDataset);

  if FDataset.Active then
    iID:= FDataset.FieldByName(FLD_ID).AsInteger
  else
    iID:= 0;

//  dmOnega_DB_data.fn_MapDesktop_Select_GeoMaps (q_Maps, aID);

  i:=dmOnega_DB_data.MapDesktop_Select_GeoMaps (ADOStoredProc_Maps, aID);

//  db_View(ADOStoredProc_Maps);


 // db_CreateCalculatedField(ADOStoredProc_Maps, FLD_CAPTION, ftString, 100);


  if iID>0 then
    FDataset.Locate(FLD_ID, iID, []);
end;



procedure Tframe_Map_Desktop_Maps.cxGrid1Exit(Sender: TObject);
begin
  db_PostDataset (FDataset);
end;

// ---------------------------------------------------------------
procedure Tframe_Map_Desktop_Maps.DoMapChecked;
// ---------------------------------------------------------------
var
  bChecked: Boolean;
  sFileName: string;
begin
 // if not IsAutoUpdate then
//    Exit;


  db_PostDataset(FDataSet);


  sFileName := FDataset.FieldByName(FLD_name).AsString;
  bChecked  := FDataset.FieldByName(FLD_Checked).AsBoolean;



  if Assigned(FOnMapChecked) then
    FOnMapChecked(Self);
end;


// ---------------------------------------------------------------
procedure Tframe_Map_Desktop_Maps.col_AutoLabelPropertiesChange(Sender: TObject);
// ---------------------------------------------------------------
//var
//  oColumn: TcxGridDBBandedColumn;
var
  bChecked: Boolean;
  sFileName: string;

  vLayer: CMapXLayer;
begin
//  oColumn := cxGridDBBandedTableView1.Controller.FocusedColumn;

 // DoMapChecked;


//  db_PostDataset(FDataset);

 // if not IsAutoUpdate then
  //  Exit;    


  bChecked:=(Sender as TcxCheckBox).Checked;

//  ShowMessage('col__CheckedPropertiesChange');
  sFileName := FDataset.FieldByName(FLD_name).AsString;


  vLayer :=mapx_GetLayerByFileName(FMap, sFileName);
  if Assigned(vLayer) then
  begin
    if cxGridDBBandedTableView1.Controller.FocusedColumn=col_Checked then
    begin
      vLayer.Visible :=bChecked;
//      vLayer.t

      if bChecked then
        map_Tile_Align_Back(FMap);

    end  else

    if cxGridDBBandedTableView1.Controller.FocusedColumn=col_AutoLabel then
      vLayer.AutoLabel:=bChecked else

    if cxGridDBBandedTableView1.Controller.FocusedColumn=col_AutoLabel then
      vLayer.ZoomLayer:=bChecked;

  end
  else
    if bChecked then
       DoMapChecked;

(*

 if cxGrid1DBTableView1.Controller.FocusedColumn=col__Style then

  if not bChecked then
  begin
    vLayer :=mapx_GetLayerByFileName(FMap, sFileName);
    if Assigned(vLayer) then
      vLayer.Visible :=False;
  end else*)

//  ;; else
  //  dmMapEngine_store.Open_Object_Map(sObjName);

         

(*
  db_PostDataset(FDataSet);

  if Assigned(FOnMapChecked) then
    FOnMapChecked(Self);
*)

(*
  if oColumn=col_AutoLabel then
    DoMapChecked() else

  if oColumn=col_Checked then
     DoMapChecked() else ;

  if oColumn=col_zoom_allow then
     //DoMapChecked() else ;
*)

end;

// ---------------------------------------------------------------
procedure Tframe_Map_Desktop_Maps.Check_All;
// ---------------------------------------------------------------
var
  b: boolean;
begin
  FDataSet.First;
  b:= FDataSet.FieldByName(FLD_CHECKED).AsBoolean;

  if (not b) then act_Check_All.Execute
             else act_UnCheck_All.Execute;

  DoMapChecked();
end;

// ---------------------------------------------------------------
procedure Tframe_Map_Desktop_Maps.Zoom_All;
// ---------------------------------------------------------------
var
  b: boolean;
begin
  FDataSet.First;
  b:= FDataSet.FieldByName(FLD_ALLOW_ZOOM).AsBoolean;

  if (not b) then act_Zoom_Allow.Execute
             else act_Zoom_DisAllow.Execute;

  DoMapChecked();
end;


procedure Tframe_Map_Desktop_Maps.col_CheckedHeaderClick(Sender: TObject);
begin
  Check_All;
end;


procedure Tframe_Map_Desktop_Maps.col_zoom_allowHeaderClick(
  Sender: TObject);
var
  s: string;
begin
 // ShowMessage(Sender.ClassName);

 // s:=cxGridDBBandedTableView1.Controller.FocusedColumn.FieldName;
 // ShowMessage(s);

  Zoom_All;
end;



// ---------------------------------------------------------------
procedure Tframe_Map_Desktop_Maps.DoAfterPost(DataSet: TDataSet);
// ---------------------------------------------------------------
var
  k: Integer;
begin

//  dmOnega_DB_data.ExecStoredProc (sp_MapDesktop_Map_Update,
  k:=dmOnega_DB_data.ExecStoredProc_ (sp_Map_XREF_Update,

        [FLD_ID,      DataSet[FLD_ID] ,
         FLD_CHECKED, DataSet[FLD_CHECKED],

     //    db_Par (FLD_priority, DataSet[FLD_priority]),
         FLD_ALLOW_ZOOM, DataSet[FLD_ALLOW_ZOOM],
         FLD_ZOOM_MIN, DataSet[FLD_ZOOM_MIN],
         FLD_ZOOM_MAX, DataSet[FLD_ZOOM_MAX],

         FLD_AutoLabel, DataSet[FLD_AutoLabel],

         FLD_label_style, DataSet[FLD_label_style]
        ]);
end;

//-------------------------------------------------------------------
procedure Tframe_Map_Desktop_Maps.CheckIfExists;
//-------------------------------------------------------------------
var
  b: boolean;
 // sFileName: string;
  sTabFileName: string;
begin
  FDataSet.AfterPost :=nil;

  with FDataSet  do
    while not EOF do
  begin
    sTabFileName:= FDataset.FieldByName(FLD_NAME).AsString;

    b:=FileExists(sTabFileName);

    db_UpdateRecord(FDataSet, [db_Par(FLD_IS_FILE_EXISTS, b)]);

    Next;
  end;

  FDataSet.AfterPost :=DoAfterPost;

(*

  sFileName:=aDataset.FieldByName(FLD_NAME).AsString;
  b:=FileExists(sFileName);

//  if aDataset.FieldByName(FLD_EXIST).AsBoolean <> b then
  if aDataset.FieldValues[FLD_EXIST] <> b then
    db_UpdateRecord(aDataSet, [db_Par(FLD_EXIST, b)]);
*)
end;



procedure Tframe_Map_Desktop_Maps.cxGridDBBandedTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  if ARecord.Values[col_is_file_exists.Index]=False then
     AStyle := cxStyle_strikeout;

end;


procedure Tframe_Map_Desktop_Maps.cb_Check_AllClick(Sender: TObject);
begin
  DoMapChecked;
end;


procedure Tframe_Map_Desktop_Maps.ApplicationEvents1Message(var Msg: tagMSG;
    var Handled: Boolean);
begin
  case Msg.message of
    WM_Explorer_REFRESH_MAP_FILES: View(FID);

   // WE_PROJECT_UPDATE_MAP_LIST: View(FID);
  end;
end;


// ---------------------------------------------------------------
procedure Tframe_Map_Desktop_Maps.ActionList2Update(Action: TBasicAction; var
    Handled: Boolean);
// ---------------------------------------------------------------
var
  b: Boolean;
begin
  b:=FDataSet.RecordCount>0;

  act_Check.Enabled:=b;
  act_UnCheck.Enabled:=b;
  act_Uncheck_All.Enabled:=b;
  act_Check_All.Enabled:=b;

  act_Zoom_allow.Enabled:=b;
  act_Zoom_Disallow.Enabled:=b;
  act_Zoom_SetMax.Enabled:=b;
  act_Zoom_SetMin.Enabled:=b;

  act_Map_Del.Enabled:=b;

  act_Check_Exists.Enabled:=b;

  act_Up.Enabled:=b;
  act_Down.Enabled:=b;
  act_Top.Enabled:=b;
  act_Bottom.Enabled:=b;

  act_Show_in_Map.Enabled:=b;

end;

// ---------------------------------------------------------------
procedure Tframe_Map_Desktop_Maps.ADOStoredProc_MapsCalcFields(DataSet:
    TDataSet);
// ---------------------------------------------------------------
const
  FLD_short_filename = 'short_filename';
var
  s: string;
begin
  s:=ExtractFileName(DataSet.FieldByName(FLD_name).AsString);
  DataSet[FLD_short_filename] := s;
end;

end.






  {

//------------------------------------------------------------------
procedure Tframe_Map_Desktop_Maps.col_CheckedToggleClick(Sender: TObject; const Text: String; State: TdxCheckBoxState);
//------------------------------------------------------------------
begin
//  db_UpdateRecord(FDataSet, [db_Par(FLD_CHECKED, AsBoolean(Text))]);
//  IsChanged:= true;

//  DoMapChecked();
end;
   )
   }
