library map_desktop;

uses
  ComServ,
  MapDesktop_TLB in 'MapDesktop_TLB.pas',
  x_map_desktop in 'srs\x_map_desktop.pas' {MapDesktopX: CoClass},
  u_MapDesktop_classes in 'srs\u_MapDesktop_classes.pas',
  u_const_db in '..\_common\u_const_db.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer,
  DllInstall;

{$R *.TLB}

{$R *.RES}

begin
end.
