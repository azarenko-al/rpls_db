program map_desktop_test;

uses
  Vcl.Forms,
  __Unit23 in '__Unit23.pas' {Form23},
  u_MapDesktop_classes in '..\srs\u_MapDesktop_classes.pas',
  x_map_desktop in '..\srs\x_map_desktop.pas',
  dm_Map_Desktop in '..\srs\dm_Map_Desktop.pas' {dmMap_Desktop: TDataModule},
  MapDesktop_TLB in '..\MapDesktop_TLB.pas',
  fr_CalcMaps in '..\srs\fr_CalcMaps.pas' {frame_CalcMaps},
  fr_Map_Desktop_Maps in '..\srs\fr_Map_Desktop_Maps.pas' {frame_Map_Desktop_Maps},
  fr_Map_Desktop_ObjectMaps in '..\srs\fr_Map_Desktop_ObjectMaps.pas' {frame_Map_Desktop_ObjectMaps},
  d_Map_Desktops in '..\srs\d_Map_Desktops.pas',
  d_Transparent in '..\srs\d_Transparent.pas' {dlg_Transparent},
  dm_Onega_DB_data in '..\..\_common\dm_Onega_DB_data.pas' {dmOnega_DB_data: TDataModule},
  dm_MapEngine_new in '..\srs\MapEngine_new\dm_MapEngine_new.pas' {dmMapEngine_new: TDataModule},
  dm_MapEngine_store in '..\srs\MapEngine_new\dm_MapEngine_store.pas' {dmMapEngine_store: TDataModule},
  u_MapEngine_classes in '..\srs\MapEngine_new\u_MapEngine_classes.pas',
  u_const_db in '..\..\_common\u_const_db.pas',
  u_const_str in '..\..\_common\u_const_str.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm23, Form23);
  Application.Run;
end.
