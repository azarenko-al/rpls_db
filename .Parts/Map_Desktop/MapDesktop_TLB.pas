unit MapDesktop_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 02.05.2019 20:36:41 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\RPLS_DB\.Parts\Map_Desktop\Project20 (1)
// LIBID: {42D01B3B-6F97-4EB8-8F41-6861FA72131B}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\Windows\system32\stdvcl40.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Winapi.Windows, System.Classes, System.Variants, System.Win.StdVCL, Vcl.Graphics, Vcl.OleServer, Winapi.ActiveX;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  MapDesktopMajorVersion = 1;
  MapDesktopMinorVersion = 0;

  LIBID_MapDesktop: TGUID = '{42D01B3B-6F97-4EB8-8F41-6861FA72131B}';

  IID_IMapDesktopX: TGUID = '{2E4D03E3-D9F1-4FBB-823F-83501EE9F8AF}';
  CLASS_MapDesktopX: TGUID = '{BC187988-9109-4607-971D-16D5C73B17F1}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IMapDesktopX = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  MapDesktopX = IMapDesktopX;


// *********************************************************************//
// Interface: IMapDesktopX
// Flags:     (256) OleAutomation
// GUID:      {2E4D03E3-D9F1-4FBB-823F-83501EE9F8AF}
// *********************************************************************//
  IMapDesktopX = interface(IUnknown)
    ['{2E4D03E3-D9F1-4FBB-823F-83501EE9F8AF}']
    function Dlg: HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoMapDesktopX provides a Create and CreateRemote method to          
// create instances of the default interface IMapDesktopX exposed by              
// the CoClass MapDesktopX. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoMapDesktopX = class
    class function Create: IMapDesktopX;
    class function CreateRemote(const MachineName: string): IMapDesktopX;
  end;

implementation

uses System.Win.ComObj;

class function CoMapDesktopX.Create: IMapDesktopX;
begin
  Result := CreateComObject(CLASS_MapDesktopX) as IMapDesktopX;
end;

class function CoMapDesktopX.CreateRemote(const MachineName: string): IMapDesktopX;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_MapDesktopX) as IMapDesktopX;
end;

end.

