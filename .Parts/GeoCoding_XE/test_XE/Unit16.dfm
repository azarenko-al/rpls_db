object Form16: TForm16
  Left = 1369
  Top = 562
  Caption = 'Form16'
  ClientHeight = 226
  ClientWidth = 760
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid2: TDBGrid
    Left = 0
    Top = 77
    Width = 760
    Height = 149
    Align = alBottom
    DataSource = ds_Data
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Address'
        Width = 528
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'lat_WGS'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'lon_WGS'
        Visible = True
      end>
  end
  object Button2: TButton
    Left = 50
    Top = 34
    Width = 75
    Height = 25
    Caption = 'Yandex'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button4: TButton
    Left = 141
    Top = 34
    Width = 75
    Height = 25
    Caption = 'OSM'
    TabOrder = 2
    OnClick = Button4Click
  end
  object mem_Data: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 373
    Top = 34
    object mem_Dataposition: TStringField
      DisplayLabel = #1040#1076#1088#1077#1089
      FieldName = 'Address'
      Size = 100
    end
    object mem_Datalat: TFloatField
      FieldName = 'lat_WGS'
    end
    object mem_Datalon: TFloatField
      FieldName = 'lon_WGS'
    end
  end
  object ds_Data: TDataSource
    DataSet = mem_Data
    Left = 369
    Top = 90
  end
end
