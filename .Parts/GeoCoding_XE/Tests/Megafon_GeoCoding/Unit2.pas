unit Unit2;

interface

uses
  u_megafon_GeoCoding,

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, RxPlacemnt, Vcl.ExtCtrls,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP;

type
  TForm2 = class(TForm)
    FormStorage1: TFormStorage;
    IdHTTP1: TIdHTTP;
    LabeledEdit1: TLabeledEdit;
    Memo1: TMemo;
    Memo2: TMemo;
    Button1: TButton;
    Edit1: TEdit;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.Button1Click(Sender: TObject);
var
  oStream: TStringStream;
  sURL: string;
begin
  sURL:=Trim (LabeledEdit1.Text);

  oStream := TStringStream.Create('', TEncoding.UTF8);

  IdHTTP1.Get( sURL, oStream);

  memo1.Text := oStream.DataString;
  Memo2.Text:= LoadFromJSON(memo1.Text);

//  memo1.Text := IdHTTP1.Get( sURL );
//  Memo2.Text:= LoadFromJSON(memo1.Text);


end;


end.
