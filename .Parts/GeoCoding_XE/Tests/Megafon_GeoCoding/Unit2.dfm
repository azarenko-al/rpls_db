object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 486
  ClientWidth = 815
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object LabeledEdit1: TLabeledEdit
    Left = 24
    Top = 24
    Width = 601
    Height = 21
    EditLabel.Width = 19
    EditLabel.Height = 13
    EditLabel.Caption = 'URL'
    TabOrder = 0
    Text = 'http://localhost/GeoCoding/geocode.json'
  end
  object Memo1: TMemo
    Left = 24
    Top = 168
    Width = 601
    Height = 169
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object Memo2: TMemo
    Left = 24
    Top = 360
    Width = 601
    Height = 73
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssBoth
    TabOrder = 2
  end
  object Button1: TButton
    Left = 656
    Top = 22
    Width = 75
    Height = 25
    Caption = 'run'
    TabOrder = 3
    OnClick = Button1Click
  end
  object Edit1: TEdit
    Left = 24
    Top = 80
    Width = 601
    Height = 21
    TabOrder = 4
    Text = 
      'http://esb-vos15:10011/yandex/geocode?user=oneplan&geocode=48.42' +
      '04382,53.1786766&format=json'
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredProps.Strings = (
      'LabeledEdit1.Text')
    StoredValues = <>
    Left = 680
    Top = 80
  end
  object IdHTTP1: TIdHTTP
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 680
    Top = 168
  end
end
