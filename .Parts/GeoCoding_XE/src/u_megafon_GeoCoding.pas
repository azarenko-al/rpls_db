﻿unit u_megafon_GeoCoding;

interface
uses
  Graphics, StrUtils, System.SysUtils, Dialogs, JSON, IOUtils;


function LoadFromJSON(aJSON: String): string;


implementation


function LoadFromFile_JSON(aFileName: String): string;
var
  i: Integer;
  oObject: TJSONObject;
  oNode: TJSONObject;
  oNode1: TJSONObject;
  oNodeArr: TJSONArray;
  oValue: TJSONValue;
  s: string;
  v: Variant;

 // Data: TArray<Byte>;

begin
  if aFileName='' then
    aFileName:='C:\YandexDisk\Megafon\json\geocode.json';


  s:=TFile.ReadAllText(aFileName);
  s:=LoadFromJSON (s);
  Exit;


//  aFileName:='C:\YandexDisk\Megafon\json\format.json';

//  s:='{''r' + TFile.ReadAllText(aFileName, TEncoding.UTF8);

//  oObject := TJSONObject.ParseJSONValue(s) as TJSONObject;
  oObject := TJSONObject.ParseJSONValue(TFile.ReadAllText(aFileName)) as TJSONObject;

//  LJSONValue:=TJSONObject.ParseJSONValue(TEncoding.UTF8.GetBytes(JsonUt8),0);
//  Edit1.Text:=LJSONValue.ToString;

//  oObject.GetValue('response').

  //oNode:=TJSONObject(
//  oNode:=oObject.Get('response')  as TJSONObject;

  //.GetValue('GeoObjectCollection') as TJSONObject;

  oNodeArr:=TJSONObject(TJSONObject(oObject
            .GetValue('response'))
            .GetValue('GeoObjectCollection'))
            .GetValue('featureMember') as TJSONArray;

  for i:=0 to oNodeArr.Count-1 do
  begin
    oValue:=TJSONObject(TJSONObject(TJSONObject(TJSONObject(TJSONObject(oNodeArr.Get(i))
          .GetValue('GeoObject'))
          .GetValue('metaDataProperty'))
          .GetValue('GeocoderMetaData'))
          .GetValue('Address'))
          .GetValue('formatted')  as TJSONValue;

//     oValue.ToBytes(Data, 0);

     s:=oValue.Value;
     Result:=oValue.Value;
     Exit;
                //

    //oNode:=TJSONObject(oNodeArr.Get(i).GetValue('metaDataProperty') );

   //   oNodeArr:=TJSONObject(oObject.GetValue('response')).GetValue('featureMember') as TJSONArray;

  end;
  ;

//  oNode:=oObject.GetValue('response') as TJSONObject;

  //GeoObjectCollection


  // TODO -cMM: LoadFromFile_JSON default body inserted
end;


function LoadFromJSON(aJSON: String): string;
var
  i: Integer;
  oObject: TJSONObject;
  oNode: TJSONObject;
  oNode1: TJSONObject;
  oNodeArr: TJSONArray;
  oValue: TJSONValue;
  s: string;
  v: Variant;

 // Data: TArray<Byte>;

begin
//  if aFileName='' then
//    aFileName:='C:\YandexDisk\Megafon\json\geocode.json';

//  aFileName:='C:\YandexDisk\Megafon\json\format.json';

//  s:='{''r' + TFile.ReadAllText(aFileName, TEncoding.UTF8);

//  oObject := TJSONObject.ParseJSONValue(s) as TJSONObject;
  oObject := TJSONObject.ParseJSONValue(aJSON) as TJSONObject;

//  LJSONValue:=TJSONObject.ParseJSONValue(TEncoding.UTF8.GetBytes(JsonUt8),0);
//  Edit1.Text:=LJSONValue.ToString;

//  oObject.GetValue('response').

  //oNode:=TJSONObject(
//  oNode:=oObject.Get('response')  as TJSONObject;

  //.GetValue('GeoObjectCollection') as TJSONObject;

  oNodeArr:=TJSONObject(TJSONObject(oObject
            .GetValue('response'))
            .GetValue('GeoObjectCollection'))
            .GetValue('featureMember') as TJSONArray;

  for i:=0 to oNodeArr.Count-1 do
  begin
    oValue:=TJSONObject(TJSONObject(TJSONObject(TJSONObject(TJSONObject(oNodeArr.Get(i))
          .GetValue('GeoObject'))
          .GetValue('metaDataProperty'))
          .GetValue('GeocoderMetaData'))
          .GetValue('Address'))
          .GetValue('formatted')  as TJSONValue;

//     oValue.ToBytes(Data, 0);

     s:=oValue.Value;
     Result:=oValue.Value;
     Exit;
                //

    //oNode:=TJSONObject(oNodeArr.Get(i).GetValue('metaDataProperty') );

   //   oNodeArr:=TJSONObject(oObject.GetValue('response')).GetValue('featureMember') as TJSONArray;

  end;
  ;

//  oNode:=oObject.GetValue('response') as TJSONObject;

  //GeoObjectCollection


  // TODO -cMM: LoadFromFile_JSON default body inserted
end;



begin
//  LoadFromFile_JSON('');
end.

{
// ---------------------------------------------------------------
function TGDAL_json_rec.LoadFromFile(aFileName: string): Boolean;
// ---------------------------------------------------------------

      procedure DoGetCorner(aJSONArray: TJSONArray; var aCorner: Tcorner);
      var
        s1: string;
        s2: string;
      begin
        s1:=(aJSONArray.Get(0) as TJSONValue).Value;
        s2:=(aJSONArray.Get(1) as TJSONValue).Value;


        aCorner.y:=s1.ToDouble();
        aCorner.x:=s2.ToDouble();


        assert (aCorner.x<>0);
        assert (aCorner.y<>0);

      end;


var
  I: Integer;
  k1: Integer;
  k2: Integer;
  k3: Integer;
  oCornerCoordinates: TJSONObject;
  oNode: TJSONObject;
  oObject: TJSONObject;
  oWgs84Extent: TJSONObject;


  oValue: TJSONValue;

  oArray: TJSONArray;
  oArray1: TJSONArray;
  oArray2: TJSONArray;
  obands: TJSONArray;
  oJSONObject: TJSONObject;
  oJSONValue: TJSONValue;
  s: string;

  s1: string;
  s2: string;
  v: Variant;


begin
  FillChar(Self, SizeOf(Self),0);

  Result:=False;

  aFileName:=ChangeFileExt(aFileName,'.json');
  Assert (FileExists (aFileName), aFileName);

 // FileName:=aFileName;
  Assert (TFile.ReadAllText(aFileName) <> '', 'blank file: ' + aFileName);




  oObject := TJSONObject.ParseJSONValue(TFile.ReadAllText(aFileName)) as TJSONObject;

  if not Assigned (oObject) then
    Exit;

  Assert (Assigned (oObject));

// ---------------------------------------------------------------
// size
// ---------------------------------------------------------------
  oArray:=oObject.GetValue('size') as TJSONArray;
    Size.cols:=(oArray.Get(0) as TJSONValue).Value.ToInteger;
    Size.rows:=(oArray.Get(1) as TJSONValue).Value.ToInteger;

//      "size":[
//    4479,
//    5043
//  ],


  assert (Size.rows>0);
  assert (Size.cols>0);

// ---------------------------------------------------------------
  oCornerCoordinates:=oObject.GetValue('cornerCoordinates') as TJSONObject;;

    DoGetCorner (oCornerCoordinates.GetValue('upperLeft')  as TJSONArray, CornerCoordinates.upperLeft);
    DoGetCorner (oCornerCoordinates.GetValue('lowerLeft')  as TJSONArray, CornerCoordinates.lowerLeft);
    DoGetCorner (oCornerCoordinates.GetValue('upperRight') as TJSONArray, CornerCoordinates.upperRight);
    DoGetCorner (oCornerCoordinates.GetValue('lowerRight') as TJSONArray, CornerCoordinates.lowerRight);

// ---------------------------------------------------------------
// ---------------------------------------------------------------
  oNode:=oObject.GetValue('coordinateSystem') as TJSONObject;;
  if Assigned(oNode) then
  begin
    coordinateSystem.wkt:= lowerCase( oNode.GetValue('wkt').Value );

    k1:=Pos('utm zone', coordinateSystem.wkt);
    if k1>0 then
    begin
      k2:=Pos('"', coordinateSystem.wkt, k1);
      k3:=k1+ Length('utm zone ');
      s:=Copy(coordinateSystem.wkt, k3, k2 - k3-1);

      Zone_UTM:=StrToIntDef(s, 0);

    end;

    //Gauss-Kruger zone 8

    k1:=Pos('gauss-kruger zone', coordinateSystem.wkt);
    if k1>0 then
    begin
      k2:=Pos('"', coordinateSystem.wkt, k1);
      k3:=k1+ Length('gauss-kruger zone ');
      s:=Copy(coordinateSystem.wkt, k3, k2 - k3);

      Zone_UTM:=StrToIntDef(s, 0) + 30;

    end;


  end;




//    "coordinateSystem":{
//    "wkt":"PROJCRS[\"WGS 84 \/ UTM Zone_UTM 48N\",\n    BASEGEOGCRS[\"WGS 84\",\n
//    "dataAxisToSRSAxisMapping":[
//      1,
//      2
//    ]



// ---------------------------------------------------------------
// wgs84Extent
// ---------------------------------------------------------------
  oWgs84Extent:=oObject.GetValue('wgs84Extent') as TJSONObject;

  if Assigned(oWgs84Extent) then
  begin
    oArray:=oWgs84Extent.GetValue('coordinates') as TJSONArray;

    oArray1:=(oArray.Get(0) as TJSONArray); //.Value;

  //  k:=oArray1.Size;

    SetLength (Points_WGS, oArray1.Size);

    for I := 0 to oArray1.Size-1 do
    begin
      oArray2:=(oArray1.Get(i) as TJSONArray);

      Points_WGS[i].Lon:=(oArray2.Get(0) as TJSONValue).Value.ToDouble;
      Points_WGS[i].Lat:=(oArray2.Get(1) as TJSONValue).Value.ToDouble;

      if Points_WGS[i].Lon < 0 then
        Points_WGS[i].Lon:=360 + Points_WGS[i].Lon;


    end;



  end;

// ---------------------------------------------------------------
// bands
// ---------------------------------------------------------------
  oBands:=oObject.GetValue('bands') as TJSONArray;
  if Assigned(oBands) then
  begin
    oJSONObject:=oBands.Get(0) as TJSONObject;

    Bands.Type_      :=oJSONObject.GetValue('type').Value;

    oJSONValue:=oJSONObject.GetValue('noDataValue');
    if Assigned(oJSONValue) then
       Bands.noDataValue:= StrToFloat( oJSONValue.Value);
  end;

//    Bands.Type_:=(oBands.Get(0) as TJSONValue).Value.ToInteger;


  Bounds.Lat_max:=CornerCoordinates.upperLeft.x;
  Bounds.Lat_min:=CornerCoordinates.lowerRight.x;

  Bounds.Lon_min:=CornerCoordinates.upperLeft.y;
  Bounds.Lon_max:=CornerCoordinates.lowerRight.y;




  Result:=True;

end;

