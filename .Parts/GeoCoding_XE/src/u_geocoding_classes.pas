unit u_geocoding_classes;


interface

uses
  SysUtils, DB, Dialogs,  CodeSiteLogging, System.Generics.Collections,
   Windows, Forms ;


type       

  TURL_rec = record
  public
    Name : string;
    URL : string;
  end;


  TURL_List = class(TList<TURL_rec>)
  public
    procedure LoadFromDB(aDataset: DB.TDataset);
    function FindURLByName(aName: string): string;
  end;



implementation

function TURL_List.FindURLByName(aName: string): string;
var
  I: Integer;
begin
  for I := 0 to Count-1 do
    if LowerCase(aName) = Items[i].Name then
      Exit (Items[i].URL);

  Result := '';

end;

procedure TURL_List.LoadFromDB(aDataset: DB.TDataset);
var
 r: TURL_rec;
begin
  with aDataset do
    while not EOF  do
    begin
      r.Name:=LowerCase( FieldValues['name'] );
      r.URL :=FieldValues['URL'];
      Add (r);

      Next;
    end;

end;

end.


(*
{

// ---------------------------------------------------------------
procedure TIni_Geocoding_rec.LoadFromFile;
// ---------------------------------------------------------------
var
  oIni: TIniFile;
  sFile: string;
begin
  inherited;


  {$IFDEF dll}

  sFile := ExtractFilePath( System.SysUtils.GetModuleName(HInstance) ) + 'Geocoding.ini';
  
 // sFile:=ExtractFilePath(Application.ExeName) + 'snmp.ini';
 
  {$ELSE}      
  sFile:='C:\ONEGA\RPLS_DB\bin\dll\Geocoding.ini';
  {$ENDIF}


  CodeSite.Send(sFile);

  Assert ( FileExists(sFile) , sFile);



  oIni:=TIniFile.Create (sFile);

  Active:=LowerCase(trim(oIni.ReadString ('main','Active','true')))='true';

  Yandex:=oIni.ReadString ('main','Yandex','');
  OSM   :=oIni.ReadString  ('main','OSM','');

  URL_get_address :=oIni.ReadString ('get_address','Yandex','');
  
  FreeAndNil (oIni);
  
end;


begin
  g_Ini_Geocoding.LoadFromFile;






