unit i_GeoCoding;

interface

uses
  Windows, shellapi,  Forms, Sysutils, Dialogs,  //ActiveX;

//  u_debug,

   u_com,
   GeoCoding_TLB;


type
  TGeoCoding_ComClient = class
  private
  //  FFileName : string;

    dll: THandle;
    fIntf: IGeoCodingX;
  public
//    class procedure Init;

    constructor Create;
    destructor Destroy; override;

//    procedure NewMethod;

//    function Get_ITileManagerX: ITileManagerX;

    property Intf: IGeoCodingX read fIntf;
  end;


function Init_GeoCodingX: IGeoCodingX;
procedure Free_IGeoCodingX;


implementation

var
  dll: Integer;


// ---------------------------------------------------------------
function Init_GeoCodingX: IGeoCodingX;
// ---------------------------------------------------------------
const
  DEF_FILE = 'GeoCoding.dll';

var

  sFile: string;

begin

  sFile:= ExtractFilePath(Application.ExeName) + 'DLL\' + DEF_FILE;

//  if not FileExists(sFile) then
//    sFile:= ExtractFilePath(Application.ExeName) + 'WMS\' + DEF_FILE;

  Assert(FileExists(sFile), sFile);

  dll:=GetComObject(sFile, CLASS_GeoCoding_, IGeoCodingX, Result);

  Assert (dll>0, 'GetComObject dll=0');

  Assert (Assigned (Result), 'Result');


 // Debug_int(dll);


end;

procedure Free_IGeoCodingX;
begin
  FreeLibrary(dll); // Unload the DLL.

//  LoadLibrary(PChar(aDllFileName));

end;

//var
//  FISnmp: ISnmp;



// ---------------------------------------------------------------
constructor TGeoCoding_ComClient.Create;
// ---------------------------------------------------------------
const
  DEF_FILE = 'GeoCoding.dll';

var
  dll: Integer;
  sFile: string;

begin

  sFile:= ExtractFilePath(Application.ExeName) + 'DLL\' + DEF_FILE;

//  if not FileExists(sFile) then
//    sFile:= ExtractFilePath(Application.ExeName) + 'WMS\' + DEF_FILE;

  Assert(FileExists(sFile), sFile);

  dll:=GetComObject(sFile, CLASS_GeoCoding_, IGeoCodingX, fIntf);

  Assert (dll>0, 'GeoCoding dll=0');

  Assert (Assigned (fIntf), 'Result');



end;

destructor TGeoCoding_ComClient.Destroy;
begin
  FIntf:=nil;

  if dll >= 32 then
    FreeLibrary(dll);

  inherited;
end;


begin
  dll:=0;

end.


(*


{



//-------------------------------------------------------------------
function GetComObject(aDllFileName: PChar; const CLSID, IID: TGUID; var aObj): THandle;
//-------------------------------------------------------------------
type
  TGetObject = function(const CLSID, IID: TGUID; var aObj): HResult; stdcall;
var
  GetObject: TGetObject;
  vFactory: IClassFactory;

begin
  Result:=LoadLibrary(aDllFileName);
  Integer(aObj):=0;

  if Result >= 32 then
  begin
    @GetObject:=GetProcAddress(Result,'DllGetClassObject');

    if Assigned(GetObject) then
      if GetObject(CLSID,IClassFactory,vFactory) = S_OK then
        vFactory.CreateInstance(nil,IID,aObj);

    vFactory:=nil
  end
end;

}



type

  TComClient11111111 = class
//    constructor Create(aFileName: string);
    destructor Destroy; override;
  private
    dll: THandle;

    FFileName: string;

//    fIntf: ITileManagerX;
  public
    procedure OpenFile(aFileName: string);

  //  constructor Create(aFileName: string);

//    procedure OpenFile;

  //  property Intf: ITileManagerX read fIntf;
  end;



  TWmsComClient11111111111111 = class
    constructor Create;
    destructor Destroy; override;
  private
    FFileName : string;

    dll: THandle;
    fIntf: ITileManagerX;
  public
    class procedure Init;
//    procedure NewMethod;

    function Get_ITileManagerX: ITileManagerX;

    property Intf: ITileManagerX read fIntf;
  end;



  

class procedure TWmsComClient11111111111111.Init;
begin
  // TODO -cMM: TWmsComClient11111111111111.Init default body inserted
end;



// ---------------------------------------------------------------
constructor TWmsComClient11111111111111.Create;
// ---------------------------------------------------------------
  // C:\ONEGA\RPLS_DB\bin\WMS
const
//  DEF_FILE = 'WMS_10_21.dll';
  DEF_FILE = 'wms.dll';

var
  sFile: string;
begin
  inherited;

  //ITileManagerX

  sFile:= ExtractFilePath(Application.ExeName) + DEF_FILE;
  if not FileExists(sFile) then
    sFile:= ExtractFilePath(Application.ExeName) + 'WMS\' + DEF_FILE;

  Assert(FileExists(sFile), sFile);


  FFileName:=sFile;


  dll:=GetComObject(sFile, // 'dll_cut.dll',
                    CLASS_TileManagerX, ITileManagerX, FIntf);

end;


destructor TWmsComClient11111111111111.Destroy;
var
  b: Boolean;

begin
//  FIntf:=nil;

  if dll >= 32 then
  begin
 //   b:=FreeLibrary(dll);

  end;

end;


function TWmsComClient11111111111111.Get_ITileManagerX: ITileManagerX;
begin
  dll:= GetComObject(FFileName, // 'dll_cut.dll',
                    CLASS_TileManagerX, ITileManagerX, Result);

                    
end;



destructor TComClient11111111.Destroy;
begin
  if dll >= 32 then
    FreeLibrary(dll);

  inherited;
end;

procedure TComClient11111111.OpenFile(aFileName: string);
begin
  // TODO -cMM: TComClient11111111.OpenFile default body inserted
end;


try
  { If the load failed, LibHandle will be zero.
   If this occurs, raise an exception. }
  if LibHandle = 0 then
   raise EDLLLoadError.Create('Unable to Load DLL');
  { If the code makes it here, the DLL loaded successfully; now obtain
   the link to the DLL's exported function so that it can be called. }
  @ShowCalendar := GetProcAddress(LibHandle, 'ShowCalendar');
  { If the function is imported successfully, then set
   lblDate.Caption to reflect the returned date from
   the function. Otherwise, show the return raise an exception. }
  if not (@ShowCalendar = nil) then
   lblDate.Caption := DateToStr(ShowCalendar(Application.Handle, Caption))
  else
   RaiseLastWin32Error;
 finally
  FreeLibrary(LibHandle); // Unload the DLL.
 end;
end;
