object Form16: TForm16
  Left = 708
  Top = 391
  Caption = 'Form16'
  ClientHeight = 521
  ClientWidth = 761
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid2: TDBGrid
    Left = 0
    Top = 372
    Width = 761
    Height = 149
    Align = alBottom
    DataSource = ds_Data
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Address'
        Width = 528
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'lat_WGS'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'lon_WGS'
        Visible = True
      end>
  end
  object Button2: TButton
    Left = 50
    Top = 34
    Width = 75
    Height = 25
    Caption = 'Yandex'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button4: TButton
    Left = 141
    Top = 34
    Width = 75
    Height = 25
    Caption = 'OSM'
    TabOrder = 2
    OnClick = Button4Click
  end
  object DBGrid1: TDBGrid
    Left = 24
    Top = 168
    Width = 673
    Height = 169
    DataSource = DataSource1
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object mem_Data: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 317
    Top = 26
    object mem_Dataposition: TStringField
      DisplayLabel = #1040#1076#1088#1077#1089
      FieldName = 'Address'
      Size = 100
    end
    object mem_Datalat: TFloatField
      FieldName = 'lat_WGS'
    end
    object mem_Datalon: TFloatField
      FieldName = 'lon_WGS'
    end
  end
  object ds_Data: TDataSource
    DataSet = mem_Data
    Left = 313
    Top = 82
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=server1'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 432
    Top = 24
  end
  object ADOTable1: TADOTable
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 'Map.geoCoding'
    Left = 528
    Top = 24
  end
  object DataSource1: TDataSource
    DataSet = ADOTable1
    Left = 529
    Top = 82
  end
end
