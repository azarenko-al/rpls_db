unit u_const_str;

interface

const
  CRLF = #13+#10;

  STR_priority = '���������';

//  STR_GRID= '������������ �������';
  STR_GRID_Restore_default= '������������ �������';

  STR_Copy_record = '�����������';

  STR_Mast_index = '�����';

  STR_Template_Site_LinkEnd_name = '���';


  STR_ADD_ANTENNA = '������� �������';

  STR_HEIGHT_MAX = '������ max';

const
  //dict
  DN_FOLDER_ANTENNA_TYPES = '1.�������';
  DN_FOLDER_LinkEndType   = '2.������������ ������������� �����';
  DN_FOLDER_CELL_LAYER    = '3.��������� ����';
  DN_FOLDER_TRXS          = '4.������������ ��������� ����� (TRX)';
  DN_COMBINERS            = '5.��������� ��������';//'5.����������';
  DN_FOLDER_TERMINAL      = '6.����������� ���������';
  DN_TEMPLATE_PMP_SITE    = '7.������� PMP ��';
  DN_FOLDER_LinkType      = '8.������� ���';
  DN_FOLDER_CLUTTER_MODEL = '9.���� ���������';
  DN_FOLDER_CALC_MODEL    = '10.������ �������';
  DN_FOLDER_COLOR_SCHEMAS = '11.�������� �����';
  DN_FOLDER_REPORT        = '12.������';
  DN_FOLDER_GEOREGION     = '13.�������';
  DN_FOLDER_TRANSFER_TECH = '14.���������� �������� ������ � ����';
  DN_FOLDER_COMPANY       = '15.����������';
  DN_FOLDER_STATUSLIST    = '16.������';

  DN_FOLDER_TEMPLATE_Site_Linkend = '17.������� ������������';
  DN_FOLDER_TEMPLATE_Site = '17.������� ������������';

            //Site_Linkend


  STR_TEMPLATE_SITE = '������ ������������';


  DN_FOLDER_GEOCITY       = '������';

//  DN_FOLDER_REGION        = '13.�������';

  DN_FOLDER_TEMPLATE_LINK = '������� ��';


 // DN_FOLDER_REGION        = '�������';
 // DN_FOLDER_REGION        = '�������';

  DN_FOLDER_LINK_REPEATER = '��������� ���������';
  DN_FOLDER_BSC           = 'BSC';
  DN_FOLDER_CALC_MODELS   = '������ �������';
  DN_FOLDER_CALC_REGION   = '������ �������';
  DN_FOLDER_DICT         = '�����������'; //'�� ���������';
  DN_FOLDER_DRIVE_TEST    = '���������';
  DN_FOLDER_FREQ_PLAN     = '���';
  DN_FOLDER_LAC           = 'LAC';
  DN_FOLDER_LINK_FREQPLAN = '�� ��������� �����';
  DN_FOLDER_LINK_LINES    = '�� �����';     //'�� �����';
  DN_FOLDER_LINKENDS      = '�� �������';
  DN_FOLDER_LinkLINE_Type = 'LinkLINE Type';
  DN_FOLDER_LINKNET       = '�� c���';
  DN_FOLDER_LINKS         = '�� ���������'; //'�� ���������';
  DN_FOLDER_LINKS_SDB     = '�� ���������_SDB'; //'�� ���������';
  DN_FOLDER_MAP           = '�����';
  DN_FOLDER_MSC           = 'MSC';
  DN_FOLDER_PMP           = '�� ������������ (PMP)';
  DN_FOLDER_PMPTERMINAL   = '��������� ������������ (PMP)';
  DN_FOLDER_CALC_REGION_PMP   = '������ ������� (PMP)';

//  DN_FOLDER_DIGITAL_PATH = '�������� ����� (��)';



  DN_FOLDER_PROJECTS      = '�������';      //'�� ���������';
  DN_FOLDER_PROPERTIES    = '��������';      //'�� ���������';
  DN_FOLDER_REL_MATRIX    = '������� �����';
  DN_FOLDER_SITE          = '�C ��������� ����� (GSM)'; //'�C GSM';
  DN_FOLDER_TRAFFIC       = '������ �������';
  DN_FOLDER_USER          = '������������';
  DN_FOLDER_USERGROUP     = '������ �������������';
  DN_FOLDER_X_CONN        = 'X-����������';
 // DN_FOLDER_TRAU          = 'TRAU';

  DN_TEMPLATE_LINKEND     = '';

//------------------------------------------------------------------------------
//                         ��������� �����
//------------------------------------------------------------------------------

  S_OBJECT_DEPENDENCE= '�������� �����������';

//��������� �����
  STR_EQUIPMENT_TYPE  = '��� ������������';
  STR_LINKENDTYPE     = '������������ ���';
  STR_EQUIPMENT_TYPED = '������������ �������';
  STR_EQUIPMENT_NOT_TYPED = '������������ �� �������';

  STR_ERROR_NEGATIVE_VALUE =
          '������������ ��������! '+ #13#10 +
          '%s �� ����� ����� ������������� ��������!';

  STR_NET_CODE = '������� ��� �������';


  STR_DLG_ADD_SITE_linkend_TEMPLATE = '�������� �������';


  //---------------------------------------------------
  // dialog captions
  //---------------------------------------------------
  DLG_CAPTION_OBJECT_CREATE = '�������� �������';
  DLG_CAPTION_ADD_FOLDER     = '�������� �����';
//  DLG_CAPTION_ADD_OBJECT     = '�������� �������';
  DLG_CAPTION_PROJECT_SELECT = '����� �������';
  DLG_CAPTION_MAP ='�����';


  STR_ThRESHOLD_BER_3 = '������ BER 10-3';
  STR_ThRESHOLD_BER_6 = '������ BER 10-6';


  STR_WRONG_ANT_TYPE             = '�������� ��� �������';

  STR_COMPANY = '����������';

  STR_Sensitivity_dBm = '���������������� [dBm]';
//  STR_SENSE_dBm= '���������������� [dBm]';


  STR_DLG_ADD_COMPANY        = '����������';

  STR_DLG_ADD_Antenna        = '�������';
  STR_DLG_ADD_ANTENNA_TYPE   = '���� ������';
  STR_DLG_ADD_BSC            = 'BSC';
  STR_DLG_ADD_CALC_Model     = '������ �������';
  STR_DLG_ADD_CALC_REGION    = '������ �������';
  STR_DLG_ADD_CELL           = '������� �� GSM';
  STR_DLG_ADD_CELL_LAYER_TYPE= '��������� ����';
  STR_DLG_ADD_CELL_TEMPLATE  = '������ ������� ��';
  STR_DLG_ADD_CLU_MODEL      = '������ ����� ���������';
  STR_DLG_ADD_COLOR_SCHEMA   = '�������� �����';
  STR_DLG_ADD_COMBINER_TYPE  = '��������� ��������';
  STR_DLG_ADD_DRIVE          = '���������';
  STR_DLG_ADD_Freq_Plan      = '���';
  STR_DLG_ADD_LINK           = '�� ���������';
  STR_DLG_ADD_Link_SDB       = '�� ��������� SDB';

  STR_DLG_ADD_LINKEND        = '�� �������';
  STR_DLG_ADD_LINKEND_TYPE   = '������������';
  STR_DLG_ADD_LINKLINE       = '�� �����';
  STR_DLG_ADD_LINKTYPE       = '������� ���';
  STR_DLG_ADD_MSC            = 'MSC';
  STR_DLG_ADD_PMP_LINK       = '�MP ���������';
  STR_DLG_ADD_PMP_SECTOR     = '������� �C PMP';
  STR_DLG_ADD_PMPTERMINAL    = 'TC PMP';
  STR_DLG_ADD_PROJECT        = '������';
  STR_DLG_ADD_PROP           = '��������';
  STR_DLG_ADD_REPORT         = '������';
  STR_DLG_ADD_SITE           = '�� GSM';
  STR_DLG_ADD_TERMINAL_TYPE  = '����������� ���������';
  STR_DLG_ADD_Traffic        = '������ �������';
  STR_DLG_ADD_TransferTech   = '���������� ��������';
  STR_DLG_ADD_UserGroup      = '������ �������������';
  STR_DLG_ADD_XCONN          = 'X-connector';
  STR_DLG_ADD_SITE_TEMPLATE  = '������ ��';
  STR_DLG_COPY_CALC_MODEL    = '����������� ������ �������';


  STR_Channel_Type = '��� ������';


  //3G---------------------------------------------
  STR_DLG_ADD_3G_NET_CONFIG  = '������������ 3G-����';


//  STR_property = '��������';

//  STR_DLG_ADD_3G_TRAFFIC   = '������ 3G-�������';

  STR_TRXNOISE = '����������� ���� ��������� [dB]';
  STR_K_NOISE  = 'K� [dB]';

 
  STR_PMPTERMINAL   = 'PmpTerminal';

  STR_PMP_SITE = '�� PMP';

  STR_DLG_ADD_LINKNET       = '�� ����';
  STR_DLG_ADD_PMP = 'PMP';

//  STR_DLG_ADD_TRAU = 'TRAU';
  STR_DLG_ADD_USER = '�������� ������������';

  STR_EXPORT_TO_XML  = '������� � XML';
  STR_IMPORT_FROM_XML= '������ �� XML';



  STR_DNA_ANGLE='���� [����]';
  STR_DNA_LOSS ='������ [dB]';

  STR_LINK_NET = 'LINK_NET';

  STR_GSM  = 'GSM';
//  STR_PMP  = 'PMP';

  STR_TEMPLATE_PMP_SITE = '������ PMP ��';

  STR_MAPS        = '�����';
  STR_REL_MATRIX  = '������� �����';
  


  STR_RELIEF_HEIGHT  = '������ �������';
  STR_MP_HEIGHT      = '������ ��';
  STR_MP_TYPE        = '��� ��';

  STR_AREA           = '�������';
  STR_DISTINCT       = '�����';
  STR_TOWN           = '��������� �����';

  // dlg PROJECT add
  STR_PROJECT_SHARED_DIR = '����� �����';
  STR_PROJECT_LOCAL_DIR  = '��������� �����';

  // object names ------------------
  STR_LINK_NAME  = '�������� �� ���������';

  STR_FREQ_PLAN_RRL = '���';
  STR_DLG_ADD_Freq_Plan_RRL = '�������� ���';

  STR_ANTENNA      = '�������';
  STR_ANTENNA_TYPE = '���';
  STR_BSC        = 'BSC';
  STR_CALC_REGION='����� �������';
  STR_CALC_MODEL ='������ �������';
  STR_CELL_LAYER  = '�������� ����';
  STR_CELL       = '������';
  STR_CLU_MODEL  = '������ ����� ���������';
  STR_DRIVE      = '���������';
  STR_FREQ_PLAN  = '���';
  STR_LINK       = '�� ��������';
  STR_LINKLINE   = '�� �����';
  STR_TRX_TYPE   = '����������������';
  STR_XCONN      = 'X-���������';

  STR_REPORT     = '�����';


  STR_PARAMS     = '���������';
  STR_MAP        = '�����';

  STR_PARAM      = '��������';
  STR_VALUE      = '��������';

  STR_LINKEND     = '���';
  STR_LINKEND_TYPE= '��� ���';
  STR_CALC_MODEL_NAME  = '������ �������';
  STR_MSC         = 'MSC';
  STR_TRAFFIC     = '������';
  STR_PROPERTY    = '��������';
  STR_PROJECT     = '������';
  STR_SITE        = '��';
  STR_COORDS      = '����������';

  STR_LOSS      = '������ [dB]';
  STR_ANTENNA_LOSS = '������ ��� [dB]';

//  STR_Reg_equipment_type   = 'Reg_equipment_type';
  STR_ACT_COLLAPSE = '������';
  STR_ACT_EXPAND   = '��������';

  STR_ACT_CHECK    = '��������';
  STR_ACT_UNCHECK  = '����� �������';

  STR_GRID_EXPORT = '������� � ����';

  STR_ACTIONS = '��������';

  STR_RECNO = '�';
  STR_LAT='������';
  STR_LON='�������';

  STR_LAT_PULKOVO='������ (���� 42)';
  STR_LON_PULKOVO='������� (���� 42)';


  STR_LINKTYPE = '��� ���';

  STR_FOLDER = '�����';

  MSG_DEL_LINK='������� �������� ?';

  STR_IMPORT_FILENAME = '������ DNA �� �����';

  STR_LABEL_NAME = '��������';
  STR_IMPORT = '������';

//  STR_SITE_ADD      = '������� ��';
 // STR_LINKEND_ADD   = '������� ���';

  STR_ADD_CELL      = '�������� ������';

  // menu actions
  STR_ITEM_ADD       = '������� �������';
  STR_FOLDER_ADD     = '�����';
  STR_FOLDER_ADD_    = '������� �����';
  STR_FOLDER_DEL_    = '������� �����';
  STR_FOLDER_RENAME  = '������������� �����';
  STR_FOLDER_MOVE_TO = '����������� � �����';

  STR_SUBFOLDER_ADD = '�������� ����������� �����';
  STR_FILE_ADD      = '�������� ����';
  STR_FILE_DEL      = '������� ����';
  STR_ACT_COPY      = '�����������';

//  STR_IMPORT = '������';

  STR_MOVE      = '�����������';
  STR_RENAME    = '�������������';
  STR_ACT_DEL   = '�������';
  STR_ACT_ADD   = '��������';
  STR_CHECKED   = '���';

  STR_ASK_DEL_LIST      = '������� ������ ?';
  STR_ASK_DEL           = '������� ?';
  STR_ASK_FOLDER_DEL    = '������� �����?';
  STR_ASK_FILE_DEL      = '������� ���� ?';

  STR_TERMINAL_TYPE = 'Terminal';

  STR_NAME       = '��������';
  STR_FIELD_NAME = '��������';
  STR_CONDITION  = '�������';
//  STR_IMPORT= '������';

  //captions
  STR_STEP     = '��� [m]';
  STR_FileName = '��� �����';
  STR_FileSize = '������';
  STR_ENABLED  = '������������';

  STR_ANT_HEIGHT   = 'H [m]';
  STR_ANT_DIAMETER = 'D [m]';

  STR_AZIMUTH  = '������ [����]';
  STR_HEIGHT   = '������ [m]';
  STR_GAIN  = '�������� [dB]';
//  STR_GAIN_dB  = 'Gain [dB]';
  STR_DIAMETER = 'D [m]';
  STR_TILT     = '������ [����]';
//  STR_SENSE_dBm= '���������������� [dBm]';
  STR_POWER_dBm= '�������� [dBm]';
  STR_POWER_W  = '�������� [W]';

  STR_ROW_RADIUS_km = '������ ������� [km]';

  STR_ADDRESS = '�����';

  STR_MODEL   = '������';

  STR_RX_FREQ_MHz = 'F ��� [MHz]';
  STR_TX_FREQ_MHz = 'F ��� [MHz]';

  STR_CALC_RADIUS_km    = '������ ������� [km]';
  STR_COLOR             = '����';
  STR_TRX               = '����������������';
  STR_CELL_LAYER_NAME   = '�������� �����';
  STR_TYPE              = '���';
  STR_TRANS_POWER_W     = '� ��� [W]';
  STR_TRANS_POWER_DBM   = '� ��� [dBm]';
  STR_TRANS_SENSE_dBm   = '� �� [dBm]';
  STR_TRANS_FREQ_MHz    = 'F ��� [MHz]';

//   STR_LINK_NAME='�������� �� ���������';

  STR_COL_FREQ_RESOURCE = '��������� ������';
  STR_COL_CALIBRATION   = '����������';
  STR_COL_NAME = '��������';


  STR_FREQ_REQ_COUNT = '����';
  STR_FREQ_FORBIDDEN = '����������� �������';
  STR_FREQ_FIXED     = '������������ �������';


  STR_LENGTH   ='����� [m]';

  STR_AVE_ERROR = '��.��';    
  STR_CKO       ='CKO';
  STR_DISPERSION='���������';
  STR_COUNT     ='���-��';
  STR_CORR      ='����';


  STR_CALC  = '������';
  STR_CELLS = '�������';

  STR_FREQ_MHz     = '������� [MHz]';
  STR_CALC_REGIONS='������ �������';

  STR_K0_CLOSED = 'K0 ����';
  STR_K0_OPEN   = 'K0 ����';
  STR_K0        = 'K0';

  STR_PMP_SECTOR  = 'PMP ������';


  STR_BER3 = '10^-3';
  STR_BER6 = '10^-6';

  STR_TYPED         = '�������';
  STR_NOT_TYPED     = '���������';

  STR_TYPED1        = '�������';
  STR_NOT_TYPED1    = '���������';

  STR_COMBINER          = '���������';
  STR_PASSIVE_ELEMENT   = '��������� �������';

  STR_SETUP_TABLE = '���������';

  STR_SHOW_IN_CENTER 	  = '�������� � ������';
  STR_SHOW_IN_CENTER_HINT = '�������� �������� ������ � ������ �����';


  STR_CAPITAL = '�������';
  STR_COUNTRY = '������';
  STR_CODE    = '���';

  STR_GRID_SETUP = '�������� �������';

  STR_LOGIN       = '��� � ����';
  STR_PASSWORD    = '������';
  STR_PHONE       = '�������';
  STR_EMAIL       = 'E-mail';
  STR_FULL_NAME   = '������ ���';
 // STR_COMMENT     = '�����������';


  STR_USER      = '������������';
  STR_UserGroup = '������';



//    STR_NET_CODE = '������� ��� �������';


//  STR_ANTENNA_HEEL             = '���� (�)';
//  STR_ANTENNA_SERVING_DISTANCE = '��������� ������������ [km]';

//  STR_TP  = '�������� ������';

const
  STR_LOCATION_TYPE = '��������';

  STR_LOCATION_TYPE_ITEMS = '0 - �������� � ��������' + CRLF +
                            '1 - ���� ����������';


  MSG_NOT_ENOUGH_RIGHTS   = '������������ ���� ������� ��� ���������� ��������';
  MSG_NOT_ABLE_TO_CONNECT = '�� ������ �������������� � ��. ��������� ��������� �����������';

  STR_FILTER_IS     = '����';
  STR_FILTER_IS_NOT = '���';


    //----------
  // site info sql
  STR_BS_NAME                   = '��� ��';
  STR_BS_NUMBER                 = '����� ��';
  STR_BS_CALC_RADIUS            = '����������� ��������� ������� ������� [km]';
  STR_BS_CALC_RADIUS_NOISE      = '����������� ��������� ������� ������ [km]';
  STR_BS_STATUS                 = '������ ��';
  STR_BAND                      = '��������';
//  STR_BAND_900_STATUS           = '������ ��������� 900 MHz';
//  STR_BAND_1800_STATUS          = '������ ��������� 1800 MHz';
  STR_FREQ_ALLOW                = '����������� �������';
  STR_HYSTERESIS                = '���������� [dB]';
  STR_VERT_WIDTH                = '������ ��� ���� [�]';
  STR_HORZ_WIDTH                = '������ ��� ����� [�]';
  STR_POLARIZATION              = '�����������';
  STR_COMMENT                   = '����������';

  STR_COMBINER_NAME             = '��� ����������';
  STR_COMBINER_LOSS             = '������ ���������� [dB]';
  STR_PROPERTY_NAME             = '��� ��������';
  STR_property_status           = '��� ��������';
//  STR_property_comment          = '����������� (��������)';
//  STR_cell_calc_radius          = '������ ������� [km] (������)';
  //----------


  STR_ANTENNA_Location_type = '���� ����������';



//  STR_USER          = '������������';
  STR_USER_CREATED  = '������';



const
  // menu group
  STR_GSM_GROUP_CAPTION='�� ��������� ����� (2G) ���';
  STR_3G_GROUP_CAPTION ='�� ��������� ����� (3G) ���';
  STR_PP_GROUP_CAPTION ='������-� �� �����  ��������';
  STR_PMP_GROUP_CAPTION='������� ����������� �������';
  STR_MSC_GROUP_CAPTION='� ���������� � ���������� �';


//  STR_Audit = '�����';
  STR_Audit = '������� ���������';


implementation


end.



 
//  MSG_DEL_LINK_LINE='������� ?';

//  STR_ACTIONS = '��������';


//  STR_CALC_REGION='����� �������';
//  STR_CALC_MODEL ='������ �������';




  {
  ', '���������� � ����������',
  ', '�� �����',         GUID_LI
  ', '��������� ����� (2G)',    STR_GSM_GROUP_CAPTION='���������� GSM ����������';
  ', '�����������',        GUID_STR_3G_GROUP_CAPTION ='���������� 3G  ����������';
  ', '��������� ����� (3G)',    STR_PP_GROUP_CAPTION ='���������� PP  ����������';
  STR_PMP_GROUP_CAPTION='���������� PMP ����������';
 }


{
 '���������� � ����������',     GUID_COMMON_GROUP);
  DoAssign1 (otLINK_GROUP,    OBJ_LINK_GROUP,     '', '�� �����',         GUID_LINK_GROUP);
  DoAssign1 (otGSM_GROUP,     OBJ_GSM_GROUP,      '', '��������� ����� (2G)',      GUID_GSM_GROUP); //'GSM',
  DoAssign1 (otPMP_GROUP,     OBJ_PMP_GROUP,      '', '�����������',        GUID_PMP_GROUP);
  DoAssign1 (ot3G_GROUP,      OBJ_3G_GROUP,       '', '��������� ����� (3G)',         GUID_3G_GROUP);
}







(*
  DN_FOLDER_SITE_3G       = '�C ��������� ����� (3G)';
  DN_FOLDER_3G            = '�C ��������� ����� (3G)';
  DN_FOLDER_CALC_REGION_3G= '������ ������� (3G)';

*)



  //  DN_SITE_TEMPLATE = '������� ��';

//------------------------------------------------------------------------------
//                        ������� ��, ��������.
//------------------------------------------------------------------------------

(*  BS_GSM_TYPE     = '�� GSM';
  BS_PMP_TYPE     = '�� PMP';
  BS_3G_TYPE      = '�� 3G';

  SECTOR_GSM_TYPE = '������ �� GSM';
  SECTOR_PMP_TYPE = '������ �� PMP';
  SECTOR_3G_TYPE  = '������ �� 3G';
*)


{
  STR_NOT_TYPED_FREQ             = '������� (��������) [GHz]';
  STR_NOT_TYPED_POWER            = '�������� �������� ��� [dBm]';
  STR_NOT_TYPED_LOSS             = '������ ��� (�� ������) [dB]';
  STR_NOT_TYPED_KRATNOSTBYSPACE  = '��������� ���������� �� ������������';
  STR_NOT_TYPED_KRATNOSTBYFREQ   = '��������� ���������� �� �������';
  STR_NOT_TYPED_RAZNOS           = 'Min ������ ������� [MHz]';
  STR_NOT_TYPED_EQUALISER_PROFIT = '�������, �������� ������������ [dB]';
  STR_NOT_TYPED_SIGNATURE_WIDTH  = '������ ��������� [MHz]';
  STR_NOT_TYPED_SIGNATURE_HEIGHT = '������ ��������� [dB]';
  STR_NOT_TYPED_SENS             = '���������������� ��� BER 10^-3 [dBm]';
  STR_NOT_TYPED_SENS_6           = '���������������� ��� BER 10^-6 [dBm]';
  STR_NOT_TYPED_KNG              = '���  ������������ [%]';
}

{
  MSG_NOT_ENOUGH_RIGHTS = '������������ ���� ������� ��� ���������� ��������';
  MSG_NOT_ABLE_TO_CONNECT = '�� ������ �������������� � ��. ��������� ��������� �����������';

  MSG_CONNECT_ERROR_F = '�� ������ �������������� � ��. ����� ������: %s';
}



