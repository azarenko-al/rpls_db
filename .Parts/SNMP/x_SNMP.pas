unit x_SNMP;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  u_Snmp,

  CodeSiteLogging,

  SysUtils, Windows, ActiveX, Classes, ComObj,  snmp_TLB, StdVcl;

type
  TSnmpX = class(TTypedComObject, ISnmpX_)
  protected
    function Send(aCode: Integer; const aMsg: WideString): HResult; stdcall;
  end;

  
implementation

uses ComServ;


// ---------------------------------------------------------------
function TSnmpX.Send(aCode: Integer; const aMsg: WideString): HResult;
// ---------------------------------------------------------------
var   
  FSNMP: TSNMP_wrap;

begin
//  CodeSite.Send( IntToStr(Result));
  
  FSNMP:=TSNMP_wrap.Create;
  
  if not FSNMP.Send_code(aCode, aMsg) then 
    Result:=S_FALSE
  else
    Result:=S_OK;
                  
  FreeAndNil(FSNMP);
end;



initialization

{$IFDEF dll}
  TTypedComObjectFactory.Create(ComServer, TSnmpX, Class_SnmpX_,
    ciMultiInstance, tmApartment);
{$ENDIF}      
    
end.



(*

{


unit x_snmp;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  u_Snmp,

  CodeSiteLogging,

  Windows, ActiveX, Classes, ComObj, SNMP_TLB, StdVcl;

type
  TSnmpX = class(TTypedComObject, ISnmpX)
  private
    FSNMP: TSNMP_wrap;

  protected
    procedure Initialize; override;
    
  public
    destructor Destroy;

    function Send(aCode: Integer; const aMsg: WideString): HResult; stdcall;

  end;

implementation

uses ComServ, System.SysUtils;


// ---------------------------------------------------------------
procedure TSnmpX.Initialize;
// ---------------------------------------------------------------
begin
  inherited;

end;

    

destructor TSnmpX.Destroy;
begin
  inherited;
//  freeAndNil (FSNMP);
end;


function TSnmpX.Send(aCode: Integer; const aMsg: WideString): HResult;
begin
 // Assert(Assigned(FSNMP));

//  CodeSite.Send( IntToStr(Result));
  FSNMP:=TSNMP_wrap.Create;
  
  if not FSNMP.Send_code(aCode, aMsg) then 
    Result:=S_FALSE
  else
    Result:=S_OK;

  freeAndNil (FSNMP);    
end;



initialization
{$IFDEF dll}
  TTypedComObjectFactory.Create(ComServer, TSnmpX, Class_SnmpX,
    ciMultiInstance, tmApartment);
{$ENDIF}   
    
end.

 (*

uses
  u_Snmp,
  
//  SNMPsend, asn1util, 
  
  System.Generics.Collections,
  Dialogs,

  CodeSiteLogging,

  Windows, Forms, ActiveX, Classes, ComObj, 
  snmp_TLB, 
  
  StdVcl;


  
  protected
    procedure Initialize; override;

  public
    destructor Destroy; override;
  end;

implementation

uses ComServ, System.IniFiles, System.SysUtils;

// ---------------------------------------------------------------
procedure TSnmp.Initialize;
// ---------------------------------------------------------------
begin
  inherited;

  FSNMP:=TSNMP_wrap.Create;
end;


destructor TSnmp.Destroy;
begin
  inherited;
  freeAndNil (FSNMP);
end;


procedure TSnmp.Send(aCode: Message_code; const aMsg: WideString);
begin
//  CodeSite.Send( IntToStr(Result));
  
  if not FSNMP.Send_code(aCode, aMsg) then 
    Result:=S_FALSE
  else
    Result:=S_OK;
    
end;
            

initialization
{$IFDEF dll}
  TTypedComObjectFactory.Create(ComServer, TSnmp, CLASS_Snmp,
    ciMultiInstance, tmApartment);
{$ENDIF}      
end.