library snmp;

uses
  ComServ,
  snmp_TLB in 'snmp_TLB.pas',
  x_SNMP in 'x_SNMP.pas' {SnmpX: CoClass},
  u_snmp in 'src\u_snmp.pas',
  i_SNMP in 'i_SNMP.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer,
  DllInstall;

{$R *.TLB}

{$R *.RES}

begin
end.
