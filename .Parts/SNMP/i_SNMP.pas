unit i_SNMP;

interface

uses
  Windows, shellapi,  Forms, Sysutils, Dialogs,  //ActiveX;

   u_com,
   snmp_TLB;



type

  TComClient_Snmp = class
  private
    dll: THandle;
  public
    Intf: ISnmpX_ ;

    constructor Create;
    destructor Destroy; override;

  end;

  
//function Init_ISnmp: ISnmpX_;
//procedure Free_ISnmp;



implementation



//------------------------------------------------------------
constructor TComClient_Snmp.Create;
//------------------------------------------------------------
const
  DEF_FILE = 'Snmp.dll';

var
//  dll: Integer;
  sFile: string;

begin
  sFile:= ExtractFilePath(Application.ExeName) + 'DLL\' + DEF_FILE;
  if not FileExists(sFile) then
    sFile:= ExtractFilePath(Application.ExeName) + DEF_FILE;

  Assert(FileExists(sFile), sFile);

  dll:=GetComObject(sFile, CLASS_SnmpX_, ISnmpX_, Intf);

  Assert (Intf<>nil);
  
  Assert (dll>0, 'Init_ISnmp dll=0');

end;


//------------------------------------------------------------
destructor TComClient_Snmp.Destroy;
//------------------------------------------------------------
begin
  Intf:=nil; 
  
  if dll >= 32 then
    FreeLibrary(dll)
end;



end.



{

procedure Free_ISnmp;
begin
  if dll >= 32 then
    FreeLibrary(dll)
end;


var
  dll: Integer;


// ---------------------------------------------------------------
function Init_ISnmp: ISnmpX_;
// ---------------------------------------------------------------
const
  DEF_FILE = 'Snmp.dll';

var
  sFile: string;

begin

  sFile:= ExtractFilePath(Application.ExeName) + 'DLL\' + DEF_FILE;

  if not FileExists(sFile) then
    sFile:= ExtractFilePath(Application.ExeName) + DEF_FILE;

  Assert(FileExists(sFile), sFile);

  dll:=GetComObject(sFile, CLASS_SnmpX_, ISnmpX_, Result);

  Assert (Result<>nil);
  
  Assert (dll>0, 'Init_ISnmp dll=0');

end;




