unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SNMPsend, StdCtrls, asn1util, RxPlacemnt;

type
  TForm1 = class(TForm)
    ed_Server: TEdit;
    Label1: TLabel;
    Button1: TButton;
    Label2: TLabel;
    Label3: TLabel;
    ed_Community: TEdit;
    Label4: TLabel;
    ed_OID: TEdit;
    Memo1: TMemo;
    Memo2: TMemo;
    Label5: TLabel;
    Label6: TLabel;
    btn_Send_Trap: TButton;
    Edit4: TEdit;
    FormStorage1: TFormStorage;
    procedure Button1Click(Sender: TObject);
    procedure btn_Send_TrapClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
var
  response,s: AnsiString;
  n:integer;
begin
  if SNMPget(ed_OID.Text, ed_Community.Text, ed_Server.Text,response)
    then
      begin
        label2.Caption:='Success...';
        memo1.Lines.Clear;
        memo2.Lines.Clear;
        memo1.Lines.Text:=response;
        s:='';
        for n:=0 to Length(response) do
          begin
            s:=s+'$'+IntToHex(Ord(response[n]),2)+' ';
          end;
        memo2.Lines.Text:=s;
      end
    else
      begin
        label2.Caption:='Not contacted... (Check SNMP host, community or MIB OID!)';
        memo1.Lines.Clear;
        memo2.Lines.Clear;
      end;
end;


procedure TForm1.btn_Send_TrapClick(Sender: TObject);
var
  snmpsend: TSNMPSend;
begin
  snmpsend := TSNMPSend.Create;
  snmpSend.TargetHost := ed_Server.Text; //IP of SNMP server
  snmpSend.TargetPort := '162'; //Port of SNMP server
  snmpSend.Query.Version:= 1;
  snmpsend.Query.Community := ed_Community.Text;
  snmpsend.Query.MIBAdd(ed_OID.Text, '11122233', ASN1_INT);
//  snmpsend.Query.MIBAdd('1.3.3.3.3.3.3.3','TEST TRAP', ASN1_OCTSTR);  

  snmpsend.Query.PDUType := PDUTrapV2;
  if snmpsend.SendTrap then
    edit4.Text:= 'success'
  else
    edit4.Text:= 'failed';
end;

end.

{
 ASN1_BOOL = $01;
  ASN1_INT = $02;
  ASN1_OCTSTR = $04;
  ASN1_NULL = $05;
  ASN1_OBJID = $06;
  ASN1_ENUM = $0a;
  ASN1_SEQ = $30;
  ASN1_SETOF = $31;
  ASN1_IPADDR = $40;
  ASN1_COUNTER = $41;
  ASN1_GAUGE = $42;
  ASN1_TIMETICKS = $43;
  ASN1_OPAQUE = $44;



