unit x_map_maker;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, ActiveX, Classes, ComObj, Map_Maker_TLB, StdVcl;

type
  TMapMakerX = class(TTypedComObject, IMapMakerX)
  protected
    function Init(const aConnectionStr: WideString; aProject_ID: Integer; const aProject_Dir: WideString): HResult;
          stdcall;

  end;

implementation

uses ComServ;

function TMapMakerX.Init(const aConnectionStr: WideString; aProject_ID: Integer; const aProject_Dir: WideString): HResult;
          
begin

end;

initialization
  TTypedComObjectFactory.Create(ComServer, TMapMakerX, Class_MapMakerX,
    ciMultiInstance, tmApartment);
end.
