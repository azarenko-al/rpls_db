unit dm_MapEngine_new;

interface

 {$DEFINE test}

uses
  Classes, Forms, DB, ADODB, Variants,  sysUtils,  IniFiles, dialogs,
  MapXLib_TLB,

  dm_Main_map_maker,
  
 // dm_Onega_DB_data,

  d_Progress,


 // u_log,
 // dm_Main,

  u_mapX_lib,
  u_mapX,

 // u_mitab,

  u_func,

  u_const_db,

  u_db,


  u_Geo_for_link,
  u_Geo
  ;

type
  TMapEngineFlags = set of (flChecksumOnly);


type
  TdmMapEngine_new = class(TDataModule)
    qry_Poly: TADOQuery;
    qry_Temp: TADOQuery;
    ADOStoredProc1: TADOStoredProc;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FFileDir: string;

    FLineChecksumList: TStringList;
    FLineChecksumList_new: TStringList;
                    
   // FmiMap: TmiMap;

//    FMap: TMap;

    FmiMap: TmiMap;

    procedure UpdatekLayer(aLayer: CMapXLayer; aDataSet: TCustomAdoDataSet);
    //procedure AddOne(aProject_id: Integer; aObjName, aMapFileName: string);

    procedure CloseMapFile;
// TODO: Add_Line_from_Dataset
////    procedure AddCellAnt(aDataset: TADOQuery);
//  procedure Add_Line_from_Dataset(aDataset: TADOQuery);

    procedure CreateAndOpenMapFile(aFileName: string);
    procedure CreateAzimuthLayerFromDb(aDataSet: TCustomAdoDataSet; aFileName:
        string);

    procedure CreateLineLayerFromDb(aDataSet: TCustomAdoDataSet; aFileName: string);

    function CreatePointLayerFromDb(aDataSet: TCustomAdoDataSet; aFileName:
        string): Boolean;

    procedure CreatePolyLayerFromDb(aDataSet: TCustomAdoDataSet; aFileName: string);

    function CreateTempFile(aFileName: string): Boolean;

    function DelCheckSum(aID: Integer): Integer;
//    function Data_OpenData_Object(aObjName: string; aID: Integer): boolean;

    function GetCountByCheckSum1(aCheckSum : Integer): Integer;

    function GetPoly(aID: Integer): TBLPointArray;
    procedure Log(aMsg: string);
//    function mapx_CreateLayerFromSafeArray(aArr: variant; aFileName: string;
//        aValueFld: TmiFieldExt): CMapxLayer;
    function SetCheckSum(aID, aCheckSum: Integer): Integer;

//    function OpenData_ADOStoredProc(aProject_id: Integer; aObjName: string):
  //      Integer;
  public
    class procedure Init;

    procedure Clear;


    procedure Feature_Add(aMap: TMap; aCMapXLayer: CMapXLayer; aObjName: string;
        aID: Integer);

    procedure Make_Object_Map(aProject_id: Integer; aObjName, aMapFileName: string);

    function Data_Open_StoredProc_data(aADOStoredProc: TADOStoredProc; aProject_id:
        Integer; aObjName: string): Integer;

    function Feature_Del1(aMap: TMap; aCMapXLayer: CMapXLayer; aObjName: string;
        aID: Integer): Integer;

    procedure SetFileDir(aValue: string);

//    procedure test_Glg_Show_;
  end;



var
  dmMapEngine_new: TdmMapEngine_new;

implementation
{$R *.dfm}

//uses dm_Main_map_maker;

const
//  SP_MapMaker = '__sp_MapMaker_select';
  SP_MapMaker      = 'sp_MapMaker';
  SP_MapMaker_Data = 'map.sp_MapMaker_Data';
//  SP_MapMaker_Data = 'sp_MapMaker_Data';

  FLD_GEONAME ='GEONAME';

  OBJ_LINK = 'LINK';



class procedure TdmMapEngine_new.Init;
begin
  if not Assigned(dmMapEngine_new) then
    Application.CreateForm(TdmMapEngine_new, dmMapEngine_new);
//    dmMapEngine_new := TdmMapEngine_new.Create(Application);

end;


procedure TdmMapEngine_new.SetFileDir(aValue: string);
begin
  FFileDir := IncludeTrailingBackslash(aValue);

end;

// ---------------------------------------------------------------
procedure TdmMapEngine_new.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  db_SetComponentADOConnection(Self, dmMain_map_maker.ADOConnection1);

  ADOStoredProc1.ProcedureName:=SP_MapMaker;
  FLineChecksumList:=TStringList.Create;
  FLineChecksumList_new:=TStringList.Create;


end;


procedure TdmMapEngine_new.DataModuleDestroy(Sender:
 TObject);
begin
  FreeAndNil(FLineChecksumList);
  FreeAndNil(FLineChecksumList_new);
end;



// ---------------------------------------------------------------
procedure TdmMapEngine_new.CreateAndOpenMapFile(aFileName: string);
// ---------------------------------------------------------------
begin

  if not CreateTempFile (aFileName) then
    raise Exception.Create('procedure TdmMapEngine_new.CreateAndOpenMapFile(aFileName: string);');
  //  Exit;



//    g_Log.Error();

  Assert(FileExists(aFileName));

  FmiMap:=TmiMap.Create;
  FmiMap.OpenFile(aFileName);
  FmiMap.BeginAccess;

end;


// ---------------------------------------------------------------
procedure TdmMapEngine_new.CloseMapFile;
// ---------------------------------------------------------------
begin
  FmiMap.EndAccess;
  FreeAndNil(FmiMap);
end;


// ---------------------------------------------------------------
procedure TdmMapEngine_new.CreateLineLayerFromDb(aDataSet: TCustomAdoDataSet;
    aFileName: string);
// ---------------------------------------------------------------
var
  bHasRepeater: Boolean;

  blVector: TBLVector;
  sName: string;
  iID: integer;

  repeater_bl: TBLPoint;
  blPoints: TBLPointArray;
  blPoint: TBLPoint;
  e: Double;
  iCheckSum: Integer;
  iCount: Integer;
  iRepeater_ID: Integer;

  bTerminated: boolean;

//  CMapXFeature

begin
  bTerminated:=False;

  Progress_Show('���������� �����...');

  CreateAndOpenMapFile(aFileName);

//  SaveTime;

 // i := 0;

// db_View(aDataSet);

  with aDataSet do
    while not EOF do
  begin
    if RecNo mod 100 = 0 then
       Progress_SetProgress1(RecNo, RecordCount, bTerminated);

    if bTerminated then
      Break;


  //  Inc(i);

    iCheckSum:= FieldValues[FLD_CheckSum];
    iID      := FieldValues[FLD_ID];
    blVector :=db_ExtractBLVector (aDataset);


    Assert (blVector.Point1.L<>0, 'blVector.Point1.L<>0');
    Assert (blVector.Point1.B<>0, 'blVector.Point1.B<>0');

    Assert (blVector.Point2.L<>0, 'blVector.Point2.L<>0');
    Assert (blVector.Point2.B<>0, 'blVector.Point2.B<>0');


    //get and Inc
//    iCount := GetCountByCheckSum(iCheckSum);
    iCount := SetCheckSum(iID,iCheckSum);


//ndField(FLD_REPEATER_LAT));

    iRepeater_ID:= FieldByName(FLD_LINK_REPEATER_ID).AsInteger;


//    bHasRepeater := Assigned (ADOStoredProc1.Fi
   // bHasRepeater:= FieldByName(FLD_HAS_REPEATER).AsBoolean;


    repeater_bl.B:= FieldByName(FLD_REPEATER_LAT).AsFloat;
    repeater_bl.L:= FieldByName(FLD_REPEATER_LON).AsFloat;


//    bHasRepeater := repeater_bl.B<>0;

    // ---------------------------------------------------------------
    if iRepeater_ID=0 then
    // ---------------------------------------------------------------
    begin
      if iCount>0 then
        blPoints := geo_Link_GetPoints(blVector, iCount)
      else begin
        SetLength(blPoints, 2);
        blPoints[0]:=blVector.Point1;
        blPoints[1]:=blVector.Point2;
      end;

  //    FmiMap.WriteLineArr (blPoints, [mapx_Par(FLD_GEONAME, iID)])
    end else

    // ---------------------------------------------------------------
    if iRepeater_ID>0 then
    // ---------------------------------------------------------------
    begin
      Assert (repeater_bl.L<>0);
      Assert (repeater_bl.B<>0);


      {
      e:=geo_Distance_km(blVector.Point1, repeater_bl);
      Assert(e<50, 'geo_Distance_km');

      e:=geo_Distance_km(blVector.Point2, repeater_bl);
      Assert(e<50, 'geo_Distance_km');
      }

      SetLength(blPoints, 3);
      blPoints[0]:=blVector.Point1;
      blPoints[1]:=repeater_bl;
      blPoints[2]:=blVector.Point2;

  //    FmiMap.WriteLineArr (blPoints, [ mapx_Par(FLD_GEONAME, iID)  ]);

    end;

    FmiMap.WriteLineArr (blPoints, [ FLD_GEONAME, iID ]);


    Next;
  end;

//  ShowMessage(IntToStr(i));

 // ShowTime;

  CloseMapFile;

  Progress_Free;


 // FreeAndNil(oSList);
end;


// ---------------------------------------------------------------
procedure TdmMapEngine_new.CreateAzimuthLayerFromDb(aDataSet:  TCustomAdoDataSet; aFileName: string);
// ---------------------------------------------------------------
const
  DEF_Length_m  = 500;

var
  bl1,bl2: TBLPoint;
  sName: string;
  iID: integer;
  blPoints: TBLPointArray;
//  blPointsF: TBLPointArrayF;
  eAzimuth: Double;
//  i: Integer;

begin
  CreateAndOpenMapFile(aFileName);


//  db_view (aDataSet);

 // SaveTime;

//  i := 0;


  with aDataSet do
    while not EOF do
  begin
  //  Inc(i);

    iID := FieldValues[FLD_ID];
    bl1 := db_ExtractBLPoint (aDataset);

//        blPoint1 :=db_ExtractBLPoint (aDataset);

    eAzimuth :=FieldByName(FLD_AZIMUTH).AsFloat;

    bl2:= geo_RotateByAzimuth (bl1, DEF_Length_m, eAzimuth);


    SetLength(blPoints, 2);

    blPoints[0]:=bl1;
    blPoints[1]:=bl2;


//    blPointsF.Count:=2;
  //  blPointsF.Items[0]:=bl1;
   // blPointsF.Items[1]:=bl2;

    FmiMap.WriteLineArr (blPoints, [FLD_GEONAME, iID]);


    Next;
  end;


//  ShowMessage(IntToStr(i));

//  ShowTime;

  CloseMapFile;

end;


// ---------------------------------------------------------------
procedure TdmMapEngine_new.CreatePolyLayerFromDb(aDataSet: TCustomAdoDataSet; aFileName: string);
// ---------------------------------------------------------------
var
  iID: integer;
  blPoints: TBLPointArray;
begin
  CreateAndOpenMapFile(aFileName);

 // SaveTime;

  with aDataSet do
    while not EOF do
  begin
    iID := FieldValues[FLD_ID];

    blPoints:=GetPoly (iID);

    if Length(blPoints)>0 then
//      FmiMap.WriteL (blPoints,  [mapx_Par(FLD_GEONAME, iID)]);
      FmiMap.WriteRegion (blPoints,  [FLD_GEONAME, iID]);

    Next;
  end;

 // ShowTime;

  CloseMapFile;

end;


// ---------------------------------------------------------------
function TdmMapEngine_new.GetPoly(aID: Integer): TBLPointArray;
// ---------------------------------------------------------------

var
  bl: TBLPoint;

  blPointsF: TBLPointArrayF;
  i: Integer;
  s: string;

begin
  s:=Format('SELECT * FROM '+ TBL_PMP_CalcRegion_Polygons +
            ' WHERE pmp_Calc_Region_id=%d  ORDER BY id', [aID]);

  dmOnega_DB_data.OpenQuery(qry_Poly, s);

  SetLength(Result, qry_Poly.RecordCount);


  while not qry_Poly.EOF do
  begin
    bl :=db_ExtractBLPoint (qry_Poly);

    i:=qry_Poly.RecNo-1;

    Result[i] := bl;

    qry_Poly.Next;
  end;
end;                                 

//------------------------------------------------------------------------------
function TdmMapEngine_new.CreateTempFile(aFileName: string): Boolean;
//------------------------------------------------------------------------------
var
  s: string;
begin
 // g_Log.SysMsg('procedure TdmMapEngine_new.CreateTempFile(aFileName: string);');
//  g_Log.SysMsg(aFileName);



  s:='SELECT id,lat,lon, lat_wgs,lon_wgs FROM property WHERE id=-1';
 // s:='SELECT top 10 id,lat,lon FROM property ';

  dmOnega_DB_data.OpenQuery(qry_Temp, s);


 // CreatePointLayerFromDb(qry_Temp, 'c:\1.tab');


   Result := CreatePointLayerFromDb(qry_Temp, aFileName);

end;



//------------------------------------------------------------------------------
procedure TdmMapEngine_new.UpdatekLayer(aLayer: CMapXLayer; aDataSet:
    TCustomAdoDataSet);
//------------------------------------------------------------------------------
var

  vFeatures: CMapXFeatures;
  vFeature1,vFeature: CMapXFeature;
  i: Integer;
  iAll: Integer;
  k: Integer;
  s: string;
  s1: string;
  v: Variant;
  v1: Variant;
  v2: Variant;

  vFeatureID: Variant;


  BLRect: TBLRect;
  BLRect1: TBLRect;

  blRects: TBLRectArray;
  sFile: string;
  sName: string;

begin
  s := Format('%s LIKE "%d"', [FLD_GEONAME, 0]);

//  aLayer.Bounds.


// aCMapXLayer.Find.S  Fin

  vFeatures:=aLayer.Search (s, EmptyParam);
  i:=vFeatures.Count;
  iAll:=aLayer.AllFeatures.Count;

  if vFeatures.Count=0 then
    Exit;


 g_log.AddRecord('procedure TdmMapEngine_new.UpdatekLayer(aLayer: CMapXLayer);','');



 // BLRect:= mapx_GetLayerBounds(aLayer);


    s:= Format('SELECT min(lat) as min_lat,  max(lat) as max_lat, min(lon) as min_lon,  max(lon) as max_lon  FROM property WHERE project_id = %d',
           [dmMain.ProjectID]);

 // s:='SELECT top 10 id,lat,lon FROM property ';

  dmOnega_DB_data.OpenQuery(qry_Temp, s);


  BLRect.TopLeft.B:=qry_Temp.FieldByName (FLD_MAX_LAT).AsFloat;
  BLRect.TopLeft.L:=qry_Temp.FieldByName (FLD_MIN_LON).AsFloat;
  BLRect.BottomRight.B:=qry_Temp.FieldByName (FLD_MIN_LAT).AsFloat;
  BLRect.BottomRight.L:=qry_Temp.FieldByName (FLD_MAX_LON).AsFloat;


  with BLRect do
    aLayer.bounds.Set_(TopLeft.L, TopLeft.B,  BottomRight.L, BottomRight.B);


   sName:= aLayer.name;
   sFile:= ChangeFileExt(aLayer.FileSpec, 'ini');

   with TIniFile.Create(sFile) do
   begin
     WriteFloat('main','MAX_LAT',BLRect.TopLeft.B);
     WriteFloat('main','MIN_LON',BLRect.TopLeft.L);
     WriteFloat('main','MIN_LAT',BLRect.BottomRight.B);
     WriteFloat('main','MAX_LON',BLRect.BottomRight.L);

   end;


//  aLayer.BeginAccess(miAccess ReadWrite);

  for I := 1 to vFeatures.Count do
  begin
    vFeature:=vFeatures.Item[i];

    aLayer.DeleteFeature(vFeature.FeatureID);

  end;


  aLayer.Pack(miPackAll);

end;




//------------------------------------------------------------------------------
function TdmMapEngine_new.CreatePointLayerFromDb(aDataSet: TCustomAdoDataSet;
    aFileName: string): Boolean;
//------------------------------------------------------------------------------
var
  e: Double;
  vBindLayerObject: CMapXBindLayer;
  vDataSet: CMapxDataset;
  VLayer: CMapXLayer;
  vFld: CMapXField;

  vFlds: CMapXFields;
  vLayerInfoObject: CMapXLayerInfo;
  iIndex, i: Integer;
  k: Integer;
  k1: Integer;

  vCoordSys: CMapXCoordSys;

  vMapXDatum: CMapXDatum;

  oMap: TMap;
  s: string;
begin
 // g_Log.SysMsg('-------------------');

  g_Log.SysMsg('TdmMapEngine_new.CreatePointLayerFromDb: '+aFileName);


  k:=aDataSet.Recordset.RecordCount;


  try
    oMap:=TMap.Create(nil);
  except
    on E: Exception do
      ShowMessage(E.Message);
  end;

 // sTempFileName:= 'C:\___1.tab';

 // sTempFileName:=aFileName;

  mapx_DeleteFile(aFileName);


   vMapXDatum:=CoDatum.create;
   vMapXDatum.Set_(3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0);


  vCoordSys := CoCoordSys.Create;

//  vCoordSys.Datum.Set_(3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0);

  vCoordSys.Set_(miLongLat,  vMapXDatum, // 9999, // DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam);

//  vCoordSys.Datum.Set_(3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0);


{nfo.prj ��������� ������� "������� / ������ (������� 1942)  - ���� 51794-2008", 1, 9999, 3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0
> � �������� ����� � WGS84 � ������� 42..
}

  vBindLayerObject := CoBindLayer.Create;
  vBindLayerObject.CoordSys   :=vCoordSys;
  vBindLayerObject.LayerName  := 'Layer_Name'; //'Temp_bind_layer';

  vBindLayerObject.RefColumn1 := 'lon';
  vBindLayerObject.RefColumn2 := 'Lat';


//  vBindLayerObject.RefColumn1 := 'lon_wgs';
//  vBindLayerObject.RefColumn2 := 'Lat_wgs';


  vBindLayerObject.LayerType  := miBindLayerTypeXY;
  vBindLayerObject.FileSpec   := aFileName;
//  vBindLayerObject.

//  SaveTime;

//  db_View(aDataSet);
  try
    k:=  oMap.Layers.Count;

      k1:= oMap.DataSets.Count;

    vDataSet := oMap.DataSets.Add(miDatasetADO, aDataSet.Recordset, EmptyParam,
                       FLD_ID,
   //                    FLD_GEONAME,

                       EmptyParam,
                       vBindLayerObject,
                       EmptyParam,
                       EmptyParam);

     Assert(Assigned(vDataSet));

    k:=  oMap.Layers.Count;


      k1:= oMap.DataSets.Count;


//     ShowMessage(s);

   // ShowTime;
  except
    on E: Exception do
      ShowMessage(E.Message);

  end;




   if oMap.Layers.Count > 0 then
     UpdatekLayer(oMap.Layers.Item[1] , aDataSet  );

 //////// oMap.DataSets.RemoveAll;

  FreeAndNil(oMap);


  Result := FileExists(aFileName);

  if not Result then
    g_Log.SysMsg('FileNotExists: '+ aFileName);

end;


// ---------------------------------------------------------------
function TdmMapEngine_new.Data_Open_StoredProc_data(aADOStoredProc:
    TADOStoredProc; aProject_id: Integer; aObjName: string): Integer;
// ---------------------------------------------------------------
//var
//  sMode: string;

begin
  Result :=db_OpenStoredProc(aADOStoredProc, SP_MapMaker_data,
   [
    db_Par(FLD_project_id, aProject_id),
    db_Par(FLD_objname, aObjName)

   ]);

 // db_View(aADOStoredProc);


end;

// ---------------------------------------------------------------
procedure TdmMapEngine_new.Make_Object_Map(aProject_id: Integer; aObjName,
    aMapFileName: string);
// ---------------------------------------------------------------
var
  I: Integer;
  iChecksum_old: Integer;
  iChecksum_new: Integer;
  iCount_new: Integer;
  iCount_old: Integer;
  oIni : TIniFile;
  r: Integer;

  sIniFile: string;
  sName: string;

begin
  Assert(aObjName<>'', 'aObjName<>''''');


  if Eq(aObjName, OBJ_LINK) then
  begin
    FLineChecksumList.Clear;
    FLineChecksumList_new.Clear;
  end;


//  sIniFile := FFileDir + Format('%s.ini',[aObjName]);
 // sMapFile := FFileDir + Format('%s.tab',[aObjName]);

 // sMapFile := aMapFileName;
(*
  sIniFile  := ChangeFileExt(aMapFileName, '.ini');

//  DeleteFile(aMapFileName);
 // DeleteFile(sIniFile);

  if FileExists(sIniFile) then
  begin
    oIni:=TIniFile.Create(sIniFile);
    iChecksum_old:=oIni.ReadInteger('Make_Object_Map','checksum',0);
    iCount_old   :=oIni.ReadInteger('Make_Object_Map','count',0);
    FreeAndNil(oIni);
  end else
    iChecksum_old:=0;

  // -------------------------
  // -------------------------



  iChecksum_new :=OpenData_ADOStoredProc (aProject_id, aObjName, True, [flChecksumOnly]);
  iCount_new    :=ADOStoredProc1.RecordCount;

  if iChecksum_new=0 then
  begin
    Log('iChecksum_new :=OpenData_ADOStoredProc (aProject_id, aObjName, True);  ' + aObjName);
  end;

  if (iChecksum_new <> 0) and
     (iChecksum_old = iChecksum_new) and
     (iCount_old    = iCount_new)
  then
    Exit;
*)

 // iChecksum_new :=


  r := db_OpenStoredProc(ADOStoredProc1, SP_MapMaker,
     [
      db_Par(FLD_project_id, aProject_id),
      db_Par(FLD_objname,    aObjname)
     ]);


//  r:=OpenData_ADOStoredProc (aProject_id, aObjName);

//  db_View(ADOStoredProc1);



  if Eq(aObjName,'pmp_calc_region') then
    CreatePolyLayerFromDb(ADOStoredProc1, aMapFileName) else

  if Assigned (ADOStoredProc1.FindField(FLD_AZIMUTH)) then
    CreateAzimuthLayerFromDb(ADOStoredProc1, aMapFileName) else

  if Assigned (ADOStoredProc1.FindField(FLD_LAT)) then
    CreatePointLayerFromDb(ADOStoredProc1, aMapFileName) else

  if Assigned (ADOStoredProc1.FindField(FLD_LAT1)) then
    CreateLineLayerFromDb(ADOStoredProc1, aMapFileName)
  else
  begin
    ShowMessage('procedure TdmMapEngine_new.Make_Object_Map - '+aObjName);
//    raise Exception.Create('procedure TdmMapEngine_new.Make_Object_Map - '+aObjName);

 //   db_View(ADOStoredProc1);

  end;

(*
  // ---------------------------------------------
  oIni:=TIniFile.Create(sIniFile);
  oIni.WriteInteger('Make_Object_Map','checksum',iChecksum_new);
  oIni.WriteInteger('Make_Object_Map','count',   ADOStoredProc1.RecordCount);
  FreeAndNil(oIni);
  // ---------------------------------------------
*)

end;


procedure TdmMapEngine_new.Clear;
begin
  FLineChecksumList.Clear;
  FLineChecksumList_new.Clear;
end;


// ---------------------------------------------------------------
procedure TdmMapEngine_new.Feature_Add(aMap: TMap; aCMapXLayer: CMapXLayer; aObjName: string; aID: Integer);
// ---------------------------------------------------------------
const
  DEF_OFFSET_M = 100;

var
  bHasRepeater: Boolean;
  vCMapXLayer: CMapXLayer;
  vDS: CMapXDataset;
  vCMapXFeature: CMapXFeature;

  blPoint: TBLPoint;
  blVector: TblVector;
  blPoints: TBLPointArray;
  blPointArr: TBLPointArray;

  repeater_bl: TBLPoint;

  i: Integer;
  i1: Integer;
  i2: Integer;
  iCheckSum: Integer;
  iCount: Integer;
  k: Integer;
  r: Integer;
  s: string;
begin
 // vCMapXFeature:=nil;

  Assert(aObjName<>'', 'Value <=0');

  r:=db_OpenStoredProc(ADOStoredProc1, SP_MapMaker,
     [
      db_Par(FLD_ID, aID),
      db_Par(FLD_objname, aobjname)

     ]);

  if ADOStoredProc1.IsEmpty then
    Exit;


 // if Data_OpenData_Object(aObjName, aID) then
//  begin
  //    db_View(ADOStoredProc1);

    // ---------------------------------------------------------------
    if Eq(aObjName,'pmp_calc_region') then
    begin
      blPointArr :=GetPoly(aID);
      vCMapXFeature:= mapx_WriteRegion_(aMap, aCMapXLayer, blPointArr); //, [mapx_Par(FLD_GEONAME,aID)]
               
    //  vCMapXFeature.

    end else

    // ---------------------------------------------------------------
    if Assigned (ADOStoredProc1.FindField(FLD_LAT)) then
    begin
      blPoint := db_ExtractBLPoint(ADOStoredProc1);
      vCMapXFeature:= mapx_WritePoint_(aMap, aCMapXLayer, blPoint); //, [mapx_Par(FLD_GEONAME,aID)]
    end else

    // ---------------------------------------------------------------
    if Assigned (ADOStoredProc1.FindField(FLD_LAT1)) then
    begin
      iCheckSum:= ADOStoredProc1[FLD_CheckSum];


      blVector := db_ExtractBLVector(ADOStoredProc1);

      repeater_bl.B:= ADOStoredProc1.FieldByName(FLD_REPEATER_LAT).AsFloat;
      repeater_bl.L:= ADOStoredProc1.FieldByName(FLD_REPEATER_LON).AsFloat;

      bHasRepeater := repeater_bl.B<>0;


      // ---------------------------------------------------------------
      if not bHasRepeater then
      // ---------------------------------------------------------------
      begin
        if iCount>0 then
          blPoints := geo_Link_GetPoints(blVector, iCount, DEF_OFFSET_M)
        else begin
          SetLength(blPoints, 2);
          blPoints[0]:=blVector.Point1;
          blPoints[1]:=blVector.Point2;
        end;

    //    FmiMap.WriteLineArr (blPoints, [mapx_Par(FLD_GEONAME, iID)])
      end  else

      // ---------------------------------------------------------------
      if bHasRepeater then
      // ---------------------------------------------------------------
      begin
        SetLength(blPoints, 3);
        blPoints[0]:=blVector.Point1;
        blPoints[1]:=repeater_bl;
        blPoints[2]:=blVector.Point2;

    //    FmiMap.WriteLineArr (blPoints, [ mapx_Par(FLD_GEONAME, iID)  ]);

      end;

      vCMapXFeature:= mapx_WritePoly_ (aMap, aCMapXLayer, blPoints); //, [mapx_Par(FLD_GEONAME,aID)]

    end else
      raise Exception.Create('');

    // -------------------------
    // write data
    // -------------------------
    if Assigned(vCMapXFeature) then
    begin
      s:=aObjName+'-Points';

      k :=aCMapXLayer.Datasets.Count;

   //   Assert(aCMapXLayer.Datasets.Count > 0);

      if aCMapXLayer.Datasets.Count=0 then
      begin
        vDS:=aMap.Datasets.Add (miDataSetLayer, aCMapXLayer, EmptyParam,EmptyParam,EmptyParam,EmptyParam,EmptyParam,EmptyParam ) ;
     //   vDS.Name:=s;

        Assert(Assigned(vDS), 'vDS not assigned');
      end;

    //  vDS:=aCMapXLayer.Datasets.Item[1];

    //  s:=vDS.Name;

     // vDS:= mapx_GetDatasetByName(aCMapXLayer, s);

   //   Assert(Assigned(vDS), 'vDS not assigned');


    //  vDS:= mapx_GetDatasetByIndex (aCMapXLayer.Datasets, 1);

      vDS:= mapx_GetDatasetByName (aCMapXLayer, aObjName+'-Points');

       Assert(Assigned(vDS), 'vDS not assigned');


      mapx_WriteFeatureFieldValues(aCMapXLayer, vDS, vCMapXFeature,
                        [mapx_Par(FLD_GEONAME,aID)]);



    end else
      raise Exception.Create('');


 // end;

//   else
//    raise Exception.Create('if Data_OpenData_Object(aObjName, aID) : '+ aObjName);

end;

// ---------------------------------------------------------------
function TdmMapEngine_new.Feature_Del1(aMap: TMap; aCMapXLayer: CMapXLayer;
    aObjName: string; aID: Integer): Integer;
// ---------------------------------------------------------------
var
  s: string;
  vFeatures: CMapXFeatures;
  vFeature: CMapXFeature;
  i: Integer;
  iAll: Integer;
  s1: string;
  v: Variant;
  v1: Variant;
  v2: Variant;
  vFeatureID: Variant;

begin
  Result := 0;

  s := Format('%s LIKE "%d"', [FLD_GEONAME, aID]);


  Assert(Assigned(aCMapXLayer), 'Value not assigned');


//  if Eq(aObjName, OBJ_LINK) then
//    DelCheckSum(aID);
//



//  aCMapXLayer.BeginAccess();
// aCMapXLayer.Find.S  Fin

  vFeatures:=aCMapXLayer.Search (s, EmptyParam);
  i:=vFeatures.Count;
  iAll:=aCMapXLayer.AllFeatures.Count;

  if vFeatures.Count=0 then
    Exit;

  vFeature:=vFeatures.Item[1];


  aCMapXLayer.BeginAccess(miAccessReadWrite);
  aMap.AutoRedraw := False;


//     else

//  if aCMapXLayer.AllFeatures.Count>0 then
//    vFeature:=aCMapXLayer.AllFeatures.Item(1)
//  else
  //  vFeature:=nil;

//  if Assigned(vFeature) then
  begin
    try
      v1:=vFeature.FeatureKey;
      vFeatureID:=vFeature.FeatureID;

   //   aCMapXLayer.AllFeatures.RemoveByID(vFeature.FeatureID);

       

    //  aCMapXLayer.ClearCustomLabels

    //  if aCMapXLayer.AllFeatures.Count then
     //   aCMapXLayer.


      aCMapXLayer.DeleteFeature(vFeature.FeatureID);

    except
      on E: Exception do
        g_log.Error(E.Message);
    end;

    Result := 1;
  end;

  aCMapXLayer.EndAccess(miAccessEnd);
  aMap.AutoRedraw := True;


end;



procedure TdmMapEngine_new.Log(aMsg: string);
begin
  g_log.Msg(aMsg);
end;

// ---------------------------------------------------------------
function TdmMapEngine_new.GetCountByCheckSum1(aCheckSum : Integer): Integer;
// ---------------------------------------------------------------
var
  sParam: string;
  sValue: string;
begin
  sParam:=IntToStr(aCheckSum);
  sValue := FLineChecksumList.Values[sParam];

  Result := AsInteger(sValue)+1;

  FLineChecksumList.Values[sParam] := IntToStr(Result);

end;



// ---------------------------------------------------------------
function TdmMapEngine_new.DelCheckSum(aID: Integer): Integer;
// ---------------------------------------------------------------
begin
  FLineChecksumList_new.Values[IntToStr(aID)] := '';
end;


// ---------------------------------------------------------------
function TdmMapEngine_new.SetCheckSum(aID, aCheckSum: Integer): Integer;
// ---------------------------------------------------------------
var
  I: Integer;
  iCount: Integer;
begin
  iCount := 0;

  for I := 0 to FLineChecksumList_new.Count - 1 do
    if FLineChecksumList_new.ValueFromIndex[i] = IntToStr(aCheckSum) then
      Inc(iCount);


  FLineChecksumList_new.Values[IntToStr(aID)] := IntToStr(aCheckSum);

  Result := iCount;

end;




end.





procedure TdmMapEngine_new.test_Glg_Show_;
begin
//  ShellExec_Notepad_temp(FLineChecksumList.Text);

end;


