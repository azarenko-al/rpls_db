unit dm_Main_map_maker;

interface

uses
  u_db,
  u_func,

  Variants,
  System.SysUtils, System.Classes, Data.DB, Data.Win.ADODB;

type
  TdmMain_map_maker = class(TDataModule)
    ADOConnection1: TADOConnection;
    ADOQuery1: TADOQuery;
    ADOStoredProc1: TADOStoredProc;
  private
    Project_ID: Integer;
    Project_Dir: String;

    function Relief_Select(aProject_ID: Integer; aChecked: boolean = False):
        Integer;

  public
    procedure Init_DB(aConnectionStr: String; aProject_ID: Integer; aProject_Dir: String);

  end;

var
  dmMain_map_maker: TdmMain_map_maker;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmMain_map_maker.Init_DB(aConnectionStr: String; aProject_ID:  Integer; aProject_Dir: String);
begin
  Project_ID :=aProject_ID;
  Project_Dir:=aProject_Dir;

  ADOConnection1.ConnectionString:=aConnectionStr;

end;


function TdmMain_map_maker.Relief_Select(aProject_ID: Integer; aChecked: boolean =  False): Integer;
begin
//  Result := db_StoredProc_Open(ADOStoredProc1, 'sp_Relief_Select', [])

  Result := db_StoredProc_Open(ADOStoredProc1, 'sp_Relief_Select',
      [FLD_Project_ID, aProject_ID,
       FLD_Checked, IIF(aChecked, aChecked, null)
      ]);
end;



end.



{


function TdmOnega_DB_data.Relief_Select(aADOStoredProc: TADOStoredProc;
    aProject_ID: Integer; aChecked: boolean = False): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_Relief_Select',
      [FLD_Project_ID, aProject_ID,
       FLD_Checked, IIF(aChecked, aChecked, null)
      ]);
end;


