library Map_maker;

uses
  ComServ,
  Map_Maker_TLB in 'Map_Maker_TLB.pas',
  x_map_maker in 'x_map_maker.pas' {MapMakerX: CoClass},
  dm_Main_map_maker in 'srs\dm_Main_map_maker.pas' {dmMain_map_maker: TDataModule},
  dm_MapEngine_new in 'srs\dm_MapEngine_new.pas' {dmMapEngine_new: TDataModule},
  dm_MapEngine_store in 'srs\dm_MapEngine_store.pas' {dmMapEngine_store: TDataModule},
  u_MapEngine_classes in 'srs\u_MapEngine_classes.pas',
  u_const_db in '..\_common\u_const_db.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer,
  DllInstall;

{$R *.TLB}

{$R *.RES}

begin
end.
