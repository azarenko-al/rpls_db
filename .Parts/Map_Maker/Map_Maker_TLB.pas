unit Map_Maker_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 17.05.2019 19:38:08 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\RPLS_DB\.Parts\Map_Maker\Project20 (1)
// LIBID: {35C1A564-221C-405A-B69F-F90BDC6C8C37}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\Windows\system32\stdvcl40.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Winapi.Windows, System.Classes, System.Variants, System.Win.StdVCL, Vcl.Graphics, Vcl.OleServer, Winapi.ActiveX;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  Map_MakerMajorVersion = 1;
  Map_MakerMinorVersion = 0;

  LIBID_Map_Maker: TGUID = '{35C1A564-221C-405A-B69F-F90BDC6C8C37}';

  IID_IMapMakerX: TGUID = '{3E50F63F-7012-4B2F-AA41-E6608497D7CD}';
  CLASS_MapMakerX: TGUID = '{CB345545-9B33-49DD-BC88-5B1EF6B350FF}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IMapMakerX = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  MapMakerX = IMapMakerX;


// *********************************************************************//
// Interface: IMapMakerX
// Flags:     (256) OleAutomation
// GUID:      {3E50F63F-7012-4B2F-AA41-E6608497D7CD}
// *********************************************************************//
  IMapMakerX = interface(IUnknown)
    ['{3E50F63F-7012-4B2F-AA41-E6608497D7CD}']
    function Init(const aConnectionStr: WideString; aProject_ID: Integer; 
                  const aProject_Dir: WideString): HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoMapMakerX provides a Create and CreateRemote method to          
// create instances of the default interface IMapMakerX exposed by              
// the CoClass MapMakerX. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoMapMakerX = class
    class function Create: IMapMakerX;
    class function CreateRemote(const MachineName: string): IMapMakerX;
  end;

implementation

uses System.Win.ComObj;

class function CoMapMakerX.Create: IMapMakerX;
begin
  Result := CreateComObject(CLASS_MapMakerX) as IMapMakerX;
end;

class function CoMapMakerX.CreateRemote(const MachineName: string): IMapMakerX;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_MapMakerX) as IMapMakerX;
end;

end.

