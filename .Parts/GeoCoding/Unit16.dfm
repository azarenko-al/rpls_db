object Form16: TForm16
  Left = 826
  Top = 598
  Caption = 'Form16'
  ClientHeight = 521
  ClientWidth = 761
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid2: TDBGrid
    Left = 0
    Top = 372
    Width = 761
    Height = 149
    Align = alBottom
    DataSource = ds_Data
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Address'
        Width = 528
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'lat_WGS'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'lon_WGS'
        Visible = True
      end>
  end
  object Button2: TButton
    Left = 8
    Top = 58
    Width = 75
    Height = 25
    Caption = 'Yandex'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button4: TButton
    Left = 99
    Top = 58
    Width = 75
    Height = 25
    Caption = 'OSM'
    TabOrder = 2
    OnClick = Button4Click
  end
  object DBGrid1: TDBGrid
    Left = 24
    Top = 168
    Width = 673
    Height = 169
    DataSource = DataSource1
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object ed_Connection_str: TLabeledEdit
    Left = 24
    Top = 120
    Width = 697
    Height = 21
    EditLabel.Width = 91
    EditLabel.Height = 13
    EditLabel.Caption = 'ed_Connection_str'
    TabOrder = 4
    Text = 
      'Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Initi' +
      'al Catalog=onega_link;Data Source=SERVER1;Use Procedure for Prep' +
      'are=1;Auto Translate=True;Packet Size=4096;Workstation ID=ALEX2;' +
      'Use Encryption for Data=False;Tag with column collation when pos' +
      'sible=False;;Password=sa'
  end
  object Button1: TButton
    Left = 8
    Top = 8
    Width = 75
    Height = 25
    Caption = 'open'
    TabOrder = 5
    OnClick = Button1Click
  end
  object mem_Data: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 405
    Top = 10
    object mem_Dataposition: TStringField
      DisplayLabel = #1040#1076#1088#1077#1089
      FieldName = 'Address'
      Size = 100
    end
    object mem_Datalat: TFloatField
      FieldName = 'lat_WGS'
    end
    object mem_Datalon: TFloatField
      FieldName = 'lon_WGS'
    end
  end
  object ds_Data: TDataSource
    DataSet = mem_Data
    Left = 401
    Top = 66
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=server1'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 520
    Top = 24
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc1
    Left = 617
    Top = 66
  end
  object ADOStoredProc1: TADOStoredProc
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    ProcedureName = 'Map.sp_geoCoding_SEL'
    Parameters = <>
    Left = 616
    Top = 24
  end
  object FormStorage1: TFormStorage
    StoredProps.Strings = (
      'ed_Connection_str.Text')
    StoredValues = <>
    Left = 696
    Top = 24
  end
end
