object dmWEB_search: TdmWEB_search
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 1217
  Top = 240
  Height = 270
  Width = 260
  object XMLDocument1: TXMLDocument
    Active = True
    Left = 37
    Top = 16
    DOMVendorDesc = 'MSXML'
  end
  object mem_Data: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 37
    Top = 84
    object mem_Dataposition: TStringField
      DisplayLabel = #1040#1076#1088#1077#1089
      FieldName = 'Address'
      Size = 100
    end
    object mem_Datalat: TFloatField
      FieldName = 'lat_WGS'
    end
    object mem_Datalon: TFloatField
      FieldName = 'lon_WGS'
    end
  end
  object ADOConnection1: TADOConnection
    Left = 144
    Top = 24
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = ADOConnection1
    Parameters = <>
    Left = 144
    Top = 88
  end
end
