library GeoCoding;

uses
  ComServ,
  GeoCoding_TLB in 'GeoCoding_TLB.pas',
  x_GeoCoding in 'x_GeoCoding.pas' {GeoCoding: CoClass},
  dm_WEB_search in 'src\dm_WEB_search.pas' {dmWEB_search: TDataModule},
  u_ini_geocoding in 'src\u_ini_geocoding.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer,
  DllInstall;

{$R *.TLB}

{$R *.RES}

begin
end.
