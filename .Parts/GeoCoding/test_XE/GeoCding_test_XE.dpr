program GeoCding_test_XE;

uses
  Forms,
  Unit16 in '..\Unit16.pas' {Form16},
  GeoCoding_TLB in 'GeoCoding_TLB.pas',
  x_GeoCoding in '..\x_GeoCoding.pas',
  u_geocoding_classes in '..\..\GeoCoding_XE\src\u_geocoding_classes.pas',
  dm_WEB_search in '..\src\dm_WEB_search.pas' {dmWEB_search: TDataModule},
  i_GeoCoding in '..\..\GeoCoding_XE\i_GeoCoding.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm16, Form16);
  Application.Run;
end.
