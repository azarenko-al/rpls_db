unit GeoCoding_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 22.10.2019 21:29:13 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\RPLS_DB\.Parts\GeoCoding\GeoCoding (1)
// LIBID: {EDECE69F-6DE9-4E35-ACEE-8987DCB792B4}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (stdvcl40.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Windows, Classes, Variants, Graphics, OleServer, ActiveX;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  GeoCodingMajorVersion = 1;
  GeoCodingMinorVersion = 0;

  LIBID_GeoCoding: TGUID = '{EDECE69F-6DE9-4E35-ACEE-8987DCB792B4}';

  IID_IGeoCodingX: TGUID = '{1C3CE822-AD86-4DF6-9977-2312469F1BE5}';
  CLASS_GeoCoding_: TGUID = '{763D52CD-0E7C-4982-9197-9649CDA78523}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IGeoCodingX = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  GeoCoding_ = IGeoCodingX;


// *********************************************************************//
// Interface: IGeoCodingX
// Flags:     (256) OleAutomation
// GUID:      {1C3CE822-AD86-4DF6-9977-2312469F1BE5}
// *********************************************************************//
  IGeoCodingX = interface(IUnknown)
    ['{1C3CE822-AD86-4DF6-9977-2312469F1BE5}']
    function Search_OSM(const aAddress: WideString; const aFileName: WideString): HResult; stdcall;
    function Search_Yandex(const aAddress: WideString; const aFileName: WideString): HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoGeoCoding_ provides a Create and CreateRemote method to
// create instances of the default interface IGeoCodingX exposed by
// the CoClass GeoCoding_. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoGeoCoding_ = class
    class function Create: IGeoCodingX;
    class function CreateRemote(const MachineName: string): IGeoCodingX;
  end;

implementation

uses ComObj;

class function CoGeoCoding_.Create: IGeoCodingX;
begin
  Result := CreateComObject(CLASS_GeoCoding_) as IGeoCodingX;
end;

class function CoGeoCoding_.CreateRemote(const MachineName: string): IGeoCodingX;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_GeoCoding_) as IGeoCodingX;
end;

end.

