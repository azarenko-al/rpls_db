unit x_GeoCoding;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  dm_WEB_search,
//  u_ini_geocoding,

  GeoCoding_TLB,

  Windows, ActiveX, Classes, ComObj,  SysUtils, StdVcl;

type
  TGeoCoding = class(TTypedComObject, IGeoCodingX)
  protected
    function Search_OSM(const aAddress: WideString; const aFileName: WideString):   HResult; stdcall;
    function Search_Yandex(const aAddress: WideString; const aFileName:  WideString): HResult; stdcall;

    function Get_Address_By_Lat_Lon_WGS(aLat: Double; aLon: Double; out  aResult_Address: WideString): HResult; stdcall;

    function Init(const aConnectionStr: WideString): HResult; stdcall;


  public
    destructor Destroy; override;
  end;

implementation

uses ComServ;

destructor TGeoCoding.Destroy;
begin
  inherited;
  FreeAndNil(dmWEB_search);

end;


function TGeoCoding.Init(const aConnectionStr: WideString): HResult;
begin
  TdmWEB_search.Init (aConnectionStr);

  Result := S_OK;
end;


//------------------------------------------------------------------------------------------------
function TGeoCoding.Get_Address_By_Lat_Lon_WGS(aLat: Double; aLon: Double; out  aResult_Address: WideString): HResult;
//------------------------------------------------------------------------------------------------
var
  sAddress: String;
begin
//  if not g_Ini_Geocoding.Active then
//    Exit(S_FALSE);
  Assert(Assigned(dmWEB_search));
//  TdmWEB_search.Init (;

  if dmWEB_search.Get_Address_By_Lat_Lon_WGS (aLat, aLon, sAddress) then
    aResult_Address:=sAddress;

  Result := S_OK;
end;



function TGeoCoding.Search_OSM(const aAddress: WideString; const aFileName:  WideString): HResult;
begin
  Assert(Assigned(dmWEB_search));

//  if not g_Ini_Geocoding.Active then
//    Exit(S_FALSE);

//  TdmWEB_search.Init;
  dmWEB_search.Search_OSM(aAddress);
  dmWEB_search.mem_Data.SaveToBinaryFile(aFileName);

  Result := S_OK;
end;


function TGeoCoding.Search_Yandex(const aAddress: WideString; const aFileName:  WideString): HResult;
begin
  Assert(Assigned(dmWEB_search));

//  if not g_Ini_Geocoding.Active then
//    Exit(S_FALSE);

//  TdmWEB_search.Init;
  dmWEB_search.Search_Yandex(aAddress);
  dmWEB_search.mem_Data.SaveToBinaryFile(aFileName);

  Result := S_OK;

end;

  

initialization
  {$IFDEF dll}
  TTypedComObjectFactory.Create(ComServer, TGeoCoding, CLASS_GeoCoding_,
    ciMultiInstance, tmApartment);
  {$ENDIF}    
end.

