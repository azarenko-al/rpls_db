unit mapapi;

interface


uses Windows, maptype, GtkLink;
  

{****** MAPAPI.INC *************************************************
*                                                                  *
*              Copyright (c) PANORAMA Group 1991-2014              *
*                      All Rights Reserved                         *
*                                                                  *
********************************************************************
*                                                                  *
*     �������� ���������� ������� � ������� "����������� �����"    *
*        ��������� ��� �������� �� ������ C, PASCAL, BASIC         *
*                                                                  *
* Edentical for 11 and 12 version GTK                              * 
********************************************************************
*                                                                  *
*     �������������� ������� � Windows :                           *
*                                                                  *
*    // �������� ����������                                        *
*    HINSTANCE libInst = ::LoadLibrary("gisacces.dll");            *
*                                                                  *
*    // ����� �������                                              *
*    HMAP (WINAPI * lpfn_OpenData)(const char*, long int);         *
*    (FARPROC)lpfn_OpenMap = GetProcAddress(libInst, "OpenData");  *
*    HMAP hMap = (*lpfn_OpenData)("Noginsk.map",0);                *
*                                                                  *
*    // �������� ����������                                        *
*    ::FreeLibrary(libInst);                                       *
*                                                                  *
*******************************************************************/
// 29/10/2014 ** �������       ** 
// 23/10/2014 ** �������       ** �������� �������� ������� mapAppendDraw � mapDrawParameters
// 15/10/2014 ** ���������� �  ** �������� �������� ������� mapVisibilityZone
// 03/05/2014 ** ���������� �  ** MODY
// 03/04/2014 ** �������� ** inc = h
// 25/03/2014 ** ���������� �  ** �������� �������� ������� mapVisibilityZone
// 11/12/2013 ** ������� �     ** ������ ��� ��������� x64 ���������� �������: GetGroupObjectsAccess, SetGroupObjectsAccess,
//                                mapPaintObjectToImage, mapPaintToEmf, mapPaintToEmfEx � ��.
// 26/07/2013 ** ����� �.�.    ** �������� �������� (������� ���������)
// 09/06/2012 ** �����         ** MapSortingWithEventPro, MapSortingSitePro,
//                                MapSortingWithEventUn, mapOpenAnyDataPro,
//                                mapAppendAnyData, mapAppendAnyDataPro,
//                                mapOpenAnyData, mapOpenDataUn
// 02/04/2013 ** ������� �     ** ������� ���������� ������� mapOpenMapUn
// 15/03/2013 ** ������� �     ** �������� ���������� ������� mapGetGeoPoint � mapGetGeoPointWGS84
// 28/01/2013 ** ������ � ** inc = h
// 27/01/2013 ** ������ � ** inc = h
// 28/06/2012 ** ���������� �  ** �������� �������� ������b mapGetDocProjection
// 09/06/2012 ** �����         ** �������� �������� ������� �������������� ��������� � ������ (��� ��������� �� ���������)
// 28/03/2012 ** ���������� �  ** mapsetgridparm
// 06/02/2012 ** ����������� � ** inc = h
// 23/06/2011 ** ������� �.�.  ** mapObjectNameUn
// 20/06/2011 ** ����������� � ** inc = h
// 25/05/2011 ** ������� �.�.  ** �������� mapGetSiteDatum, mapGetSiteEllipsoidParameters
// 26/04/2011 ** �����������   ** HIDE
// 20/04/2011 ** ������� �.�.  ** �������� �������� mapVisibilityZone
// 15/02/2010 ** �����������   ** ���������� mapRelocateObject
// 09/02/2010 ** �����������   ** mapSetRegion
// 01/13/2010 ** �����������   ** ��������� ������ � ifdef
// 17/11/2010 ** �����������   ** inc = h
// 07/09/2010 ** �����         ** �������� �������� mapLoadLibrary
// 02/09/2010 ** �����         ** mapSetAppendDataMode
// 22/03/2010 ** �������       ** ������� ����������� ������� mapGetScreenHeight
// 26/10/2009 ** �������       ** ���������




/******************************************************************
********************  ������� ��������� ***************************
*******************************************************************

    X� Xm
  ^
  | (0,0) (������ ���������� "Picture")
  +---------------------------------------------------> X�
  |                                                 ^
  |        (���������� ���� "Client")               |
  |   (0,0)                      X�                 |
  |     ----------------------->                    |
  |     |               |                           |
  |     |        x..... x  ���� ������������        |
  |     |        ....A. |                           |
  |     |        ...+.. |                           |
  |     |        ...... |                         ��������
  |     ---------x------x                    ������ ("Region")
  |     |          ^                                |
  |     |          |                                |
  |     |         ������� �������� ������           |
  |     V                  ("Draw")                 |
  |       Y�                                        |
  |                                                 V
  +----------------------------------------------------> Y� Ym
  |
  |  0,0 (������ ������ "Map" � ��������� � ������,
  |       ������� �.�. ����� �����)
  |
  V        ���������� �.A � ������ X�,Y�; � ����������� X�,Y�
    Y�

******************************************************************}

//   ����������� �����������, ����������� ��������� "MAPAPI",
// ����� ����������� � ��������� ������������ ��������
// (Windows, Linux, QNX, OC - PB � �.�.).
//   ��� ��������� ��������� API - ������� ����� ���������
// ANSI ��� Windows � KO� - 8 ��� UNIX-�������� ������.
// ��������� ���� HWND � HDC � Windows �������� ����������������
// ���� � ������������ ��������� ��������������.
//   ��� ������, ����������� ������� X-Window,
// ��������� HWND � HDC ���������� ��� long int (mapsyst.h),
// �� �������� ��������� �� ��������� ���� XCONTEXT.
//  typedef struct XCONTEXT
//  {
//   Display     xcDisplay;  //  ����� � � - ��������
//   Window      xcWindow;   //  ������������� ����
//   GC          xcContext;  //  ����������� �������� ����
//   DRAWPOINT   xcPoint;    //  ������������ ������� ������ � ���� :
//                           //  �������, �����  ���� � ��������
// }
//   XCONTEXT;



  //**********************************************************
  //*                                                        *
  //*          �������/������� ������ � ������ �����         *
  //*                                                        *
  //*********************************************************/

  // ������� ������ � �������������� ������������ �� ����
  // (���������,���������,���������...)
  // name - ��� ������������ ����� (MAP, SIT, MTW, RSW, MPT) � ��������� UNICODE
  // mode - ����� ������/������ (GENERIC_READ, GENERIC_WRITE ��� 0)
  // GENERIC_READ - ��� ������ ������ �� ������, ��� ���� �� �����������
  // ����� \Log\name.log � \Log\name.tac - �������� ������ � ������ ����������
  // error - ����� ���������� ������� ���������� �������� ��� ������
  //        (����� HMAP ����� 0) ��� 0; ���� ������ ��������� � maperr.rh
  // password - ������ ������� � ������ �� �������� ����������� 256-������ ���
  //            ��� ���������� ������ (��� ������ ������ �� �����������������)
  // size     - ����� ������ � ������ !!!
  // �������� ������ ����������, ���� ��� �������� ����� �� ��� ������.
  // ���� ������ �� �������, � �� ��� ������ ��� ��������,
  // �� ������������� ���������� ������ scnGetMapPassword �� mapscena.dll (gisdlgs.dll)
  // ���� ������ ��������� ��������� (mapIsMessageEnable()), �� ������
  // �� ����������, � ��� ���������� ������ ���������� ����� �������� ������
  // ��� ������ ���������� ����

function mapOpenAnyDataPro(const aName : PWCHAR;
                           mode : integer;
                           error : PInteger;
                           const password : PWCHAR;
                           size : integer):HMap;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapOpenAnyData(const aName : PWCHAR; lmode : integer;
                        error : PInteger = nil):HMap;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapOpenDataUn(const name : PWCHAR; const mode: integer = 0):HMap;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ������� ��������� ������ (���������� mapOpenData)                    // 03/04/2014
  // ���������� ������������� �������� ��������� �����
  // mapname - ��� ����� ����� MAP(�������������), SIT (����������������),
  //           SITX (��������� ������ � ���������� ���������� ������)
  // mode - ����� ������/������ (GENERIC_READ, GENERIC_WRITE, ��� 0)
  // GENERIC_READ - ��� ������ ������ �� ������, ��� ���� �� �����������
  // ����� \Log\name.log � \Log\name.tac - �������� ������ � ������ ����������
  // password - ������ ������� � ������ �� �������� ����������� 256-������ ���
  //            ��� ���������� ������ (��� ������ ������ �� �����������������)
  // size     - ����� ������ � ������ !!!
  // �������� ������ ����������, ���� ��� �������� ����� �� ��� ������.
  // ���� ������ �� �������, � �� ��� ������ ��� ��������,
  // �� ������������� ���������� ������ scnGetMapPassword �� mapscena.dll (gisdlgs.dll)
  // ���� ������ ��������� ��������� (mapIsMessageEnable()), �� ������
  // �� ����������, � ��� ���������� ������ ���������� ����� �������� ������
  // ��� ������ ���������� ����

function mapOpenMapPro(const mapname: PWCHAR; mode: integer; const password: PWCHAR; size: integer):HMap;   // 03/04/2014
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;



function mapOpenMapUn(const mapname: PWCHAR; const mode: integer = 0):HMap;    //02/03/2013
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

  // ��������� �������� �� ������������� ������ ����������           // 28/02/14       // 03/04/2014
  // aMap -  ������������� �������� ������
  // ��� ������ ���������� ����

function mapIsMapHandleCorrect(aMap:HMAP):integer;    //02/03/2013
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;



  // ������/��������� ���������� ��������� ����������� �������� �����
  // ����� ����� ���������
  // flag - ������� �������� ��������� ���������� �������� ���������
  //        ��� �������� �����; ��������� - ���������
  // ���������� ������ �������� �����

function  mapSetStructureControlFlag(flag: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ���������� ����� ���������� ������ � �����
  // hMap -  ������������� �������� ������
  // mode - ����� ���������� ������ (1 - ����������, 0 - �����������)
  // ��� ���������� ������ �� ��������������� �������� ��������� ��
  // ���� �������� ������ � �� ����������� �������, ��� �����������
  // �������� ������� ���������� ������ �������
  // �� ��������� ���������� ������ ������������� ������� ����� ����������
  // � ������������ ��� ���������� ��������� � �������
  // �������� ����������� ������������� � ��� ��������������� ���������,
  // � ������� ����� ��� ������������ ����������� � ������������ ����������
  // ������
  // ���������� ������� �������� ������

function mapSetAppendDataMode(hMap : HMAP; mode : integer):integer;  // 02/09/2010
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


  // �������� ������ � �������� ����� (�����; �����; �������)
  // hMap -  ������������� �������� ������
  // name - ��� ������������ ����� (SIT; MTW; MTQ; RSW; MPT)
  // mode - ����� ������/������ (GENERIC_READ; GENERIC_WRITE ��� 0)
  // ���������� ������������� ���� ������ (FILE_MAP - ��� ����������������
  // �����; FILE_RSW - ��� ������; FILE_MTW - ��� �������; FILE_MTL - ���
  // ������� �����; FILE_MTQ - ��� ������� �������); ������ ����������� �
  // ������ ����������; ���� ������ ��� ���� �������; ����� �������� ������
  // (����; �������; ������) �� ��������
  // transform - ������� ����������������� ���������������� �����
  //             � ����� �������� ������ (���� �������� ������):
  //             0 - �� ���������������� ������ (��������������� "�� ����"),
  //             1 - ���������������� ������ ��� �������� � ��������� �����
  //                 � ����� ��������,
  //            -1 - ������ ������ ������������.
  // � ��������� ������ (-1) ��������������, ��� 0.
  // ��� ������ ���������� ����

function  mapAppendData(Map : HMap; const name : GTKPChar; mode: integer = 0) : integer; // 30/09/2009
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapAppendDataEx(Map : HMap; const name : GTKPChar; mode: integer = 0;
                          transform : integer = -1) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // �������� ������ � �������� ����� (�����, �����, �������)
  // hMap -  ������������� �������� ������
  // name - ��� ������������ ����� (SIT, MTW, MTQ, RSW, MPT) � ��������� UNICODE
  // mode - ����� ������/������ (GENERIC_READ, GENERIC_WRITE ��� 0)
  // transform - ������� ����������������� ���������������� �����
  //             � ����� �������� ������ (���� �������� ������):
  //             0 - �� ���������������� ������ (��������������� "�� ����"),
  //             1 - ���������������� ������ ��� �������� � ��������� �����
  //                 � ����� ��������,
  //            -1 - ������ ������ ������������.
  // � ��������� ������ (-1) ��������������, ��� 0.
  // password - ������ ������� � ������ �� �������� ����������� 256-������ ���
  //            ��� ���������� ������ (��� ������ ������ �� �����������������)
  // size     - ����� ������ � ������ !!!
  // �������� ������ ����������, ���� ��� �������� ����� �� ��� ������.
  // ���� ������ �� �������, � �� ��� ������ ��� ��������,
  // �� ������������� ���������� ������ scnGetMapPassword �� mapscena.dll (gisdlgs.dll)
  // ���� ������ ��������� ��������� (mapIsMessageEnable()), �� ������
  // �� ����������, � ��� ���������� ������ ���������� ����� �������� ������
  // ���������� ������������� ���� ������ (FILE_MAP - ��� ����������������
  // �����, FILE_RSW - ��� ������, FILE_MTW - ��� �������, FILE_MTL - ���
  // ������� �����, FILE_MTQ - ��� ������� �������), ������ ����������� �
  // ������ ����������, ���� ������ ��� ���� �������, ����� �������� ������
  // (����, �������, ������) �� ��������
  // ��� ������ ���������� ����

function  mapAppendAnyData(Map : HMap; const name : PWCHAR;
                           mode : integer = 0; transform : integer = -1) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapAppendDataUn(Map : HMap; const name : PWCHAR; mode: integer = 0) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapAppendAnyDataPro(Map : HMap; const name : PWCHAR;
                              mode : integer = 0; transform : integer = -1;
                              const password : PWCHAR = nil; size : integer = 0) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ��������� ������ ������ �� ����� �����
  // ��� ������ ���������� ����

function  mapGetDataSize(const name: GTKPChar): double;  // 06/12/05 // 30/09/2009
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetDataSizeUn(const name: PWChar): double;  // 03/04/2014
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ��� ������� ����� � �������
  // hMap -  ������������� �������� ������
  // ��� ������ ���������� ������ ������

function mapGetMainName(aMapHandle : HMap) : GTKPChar;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function mapGetMainNameEx(aMapHandle : HMap) : PWChar;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

  // ��������� � ��������� UNICODE ��� ������� ����� � ��������� ��� ��� �������,
  // ���� ������ ������
  // hMap -  ������������� �������� ������
  // name - ����� ������ ��� ������ ����������
  // size - ����� ������ � ������
  // ��� ������ ���������� ����

function  mapGetMainNameUn(Map : HMap; name : PWCHAR; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ��� (������ ���� � �����) ������� ����� � ���������
  // ��� � ������� (MPT)
  // hMap -  ������������� �������� ������
  // ��� ������ ���������� ������ ������

function  mapGetMainMapName(Map : HMap) : PChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ��������� ��� (������ ���� � �����) ������� ����� � ���������
  // ��� � ������� (MPT)
  // hMap     -  ������������� �������� ������
  // name     - ����� ��� ������������ ������
  // namesize - ������ ������ � ������
  // ��� ������ ���������� 0

function  mapGetMainMapNameUn(Map : HMap; name : PWCHAR; namesize : integer) : Integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;



  // �������� ������������ ���������� ������ �; ���� ����; �� ����������
  // ��������� �� �������� ���������� 0 - ������ ������������� ���������
  //                                  1 - �������������
  // ���������� MAPREGISTEREX; LISTREGISTER ������� � mapcreat.h
  // ��� ������ ���������� ����

function  mapCheckAndUpdate(var mapreg : TMAPREGISTEREX; var listreg : TLISTREGISTER; priority: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;



  // ���������� ���������� ������ � ����������� �� ���� �����           // 22/12/05
  // ���������� MAPREGISTEREX; LISTREGISTER ������� � mapcreat.h
  // ��� ������ ���������� ����

function  mapRegisterFromMapType(maptype : integer; var mapreg : TMAPREGISTEREX) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ���������� ��������� ��� ��������
  // code - ����� �������� �� MAPPROJECTION (mapcreat.h)
  // ���������� ���������� ������ PROJECTIONPARAMETERS (mapcreat.h)
  // ��������: �������� 49 = EPP_AXISMERIDIAN|EPP_FALSEEASTING|EPP_FALSENORTHING
  // ��� ������ ���������� ����

function  mapGetProjectionParameters(code : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ����� ����� (����� ��������� �����)
  // mapname - ������ ��� ����� ����� (MAP, SIT, SITX)
  // rscname - ������ ��� ����� �������� (RSC)
  // ���������� ������������� �������� ��������� �����
  // ��������� MAPREGISTEREX � LISTREGISTER ������� � mapcreat.h
  // sheetname - �������� ��� �����
  // password  - ������ ������� � ������ �� �������� ����������� 256-������ ���
  //             ��� ���������� ������ (��� ������ ������ �� �����������������)
  //             (�������������� ��� ���� � ����������� SITX - ��������� �
  //              ����� �����) ��� 0
  // size      - ����� ������ � ������ !!! ��� 0
  // ���������� ������������� �������� ��������� �����
  // ��� ������ ���������� ����

function  mapCreateMapUn(const mapname : PWCHAR;          // 03/05/13
                         const rscname : PWCHAR;
                         mapreg : PMAPREGISTEREX;
                         listreg : PLISTREGISTER;
                         const sheetname : PWCHAR;
                         const password : PWCHAR;
                         size: integer):HMap;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


function  mapCreateMapEx(const mapname, rscname: GTKPChar; var mapreg : TMAPREGISTEREX; var Sheet : TLISTREGISTER): HMap;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ����
  // (����� � ������� ������� ���������)
  // mapname - ������ ��� ����� �����
  // rscname - ������ ��� ����� ��������
  // ��������� MAPREGISTEREX; LISTREGISTER ������� � mapcreate.h
  // ���������� ������������� �������� ��������� ����� (TMapAccess*)
  // ��������� CREATEPLAN ������� � maptype.h
  // ��� ������ ���������� ����

function mapCreatePlane(const mapname:GTKPChar;const rscname:GTKPChar;
                        var createplane:TCREATEPLANE):HMap;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;




  // ������� ���������������� �����
  // mapname - ������ ��� ����� ����� � ��������� UNICODE
  // rscname - ������ ��� ����� �������� � ��������� UNICODE
  // ���������� ������������� �������� ��������� ����� (TMapAccess*)
  // ��������� CREATESITEUN ������� � maptype.h
  // ��� ������ ���������� ����

function  mapCreateSiteUn(const mapname, rscname: PWCHAR; var Site: TCREATESITEUN):HSite; // 30/09/2009
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ������� ���������������� ����� �� �������� �����
  // hmap -  ������������� �������� ������
  // mapname - ������ ��� ����� ����� � ��������� UNICODE
  // rscname - ������ ��� ����� �������� � ��������� UNICODE
  // ��������� CREATESITEUN ������� � maptype.h
  // ���������� ������������� �������� ���������������� �����
  // ��� ������ ���������� ����
function  mapCreateAndAppendSiteUn (Map : HMap; const mapname, rscname: PWCHAR;
 var Site: TCREATESITEUN):HSite;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ��������� ���������������� �����
  // rscname - ������ ��� ����� ��������
  // ����� ����� ����������� � ������� ���������� �������
  // � ����� ���������� �����; ������������ �������������
  // ��� �������� ����� ��� ����� ������ ���������
  // ��� ������ ���������� ����

function  mapCreateTempSite(const rscname : GTKPChar): HSite;    // 30/09/2009
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapCreateTempSiteUn(const rscname : PWCHAR) : HSITE;   // 20130127 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ��������� ���������������� �����                    // 27/05/10
  // rscname - ������ ��� ����� ��������
  // mapreg  - ��������� ����������� ��������� �����
  // ��������� MAPREGISTER � LISTREGISTER ������� � mapcreat.h
  // ����� ����� ����������� � ������� ���������� �������
  // � ����� ���������� �����, ������������ �������������
  // ��� �������� ����� ��� ����� ������ ���������
  // ��� ������ ���������� ����

function mapCreateTempSiteEx(const rscname : GTKPCHAR;
                              mapreg : PMAPREGISTEREX) : HMAP;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapCreateTempSiteExUn(const rscname : PWChar;                         // 03/04/2014
                              mapreg : PMAPREGISTEREX;
                              datum : PDATUMPARAM; ellipsoid : PELLIPSOIDPARAM) : HMAP;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ������� ��������� ���������������� ����� �� �������� �����   // 12/01/07
  // hmap    -  ������������� �������� ������
  // rscname - ������ ��� ����� ��������; ���� ����� 0 - ����������
  // �� �������� �����
  // ����� ����� ����������� � ������� ���������� �������
  // � ����� ���������� �����; ������������ �������������
  // ��� �������� ���������������� ����� ��� ����� ������ ������������� ���������
  // ���������� ������������� �������� ���������������� �����
  // ��� ������ ���������� ����

function  mapCreateAndAppendTempSite(Map : HMap; const rscname : GTKPChar) : HSite;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapCreateAndAppendTempSiteUn(Map : HMap; const rscname : PWChar) : HSite;    // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ��������� Datum ��� �����                      // 01/03/10
  // ����� ����������� ��� �� ������ �������� �� ����� ��� 
  // � ������ ������ - ��� ����� �������� ������������� ���������� ��������
  // ��� ������ ���������� ����

function mapSetSiteDatum(aMAp : HMAP; aSite : HSITE; parm : PDATUMPARAM) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ��������� Datum ��� �����                      // 01/03/10
  // ��� ������ ���������� ����

//function mapGetSiteDatum(aMap : HMAP; aSite : HSITE; var parm : PDATUMPARAM) : integer;  // 25/05/2011
function mapGetSiteDatum(aMap : HMAP; aSite : HSITE; var parm : TDATUMPARAM) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ��������� ���������� ��� �����           // 06/05/10
  // ����� ����������� ��� �� ������ �������� �� ����� ���
  // � ������ ������ - ��� ����� �������� ������������� ���������� ��������
  // ��� ������ ���������� ����

function mapSetSiteEllipsoidParameters(aMap : HMAP; aSite : HSITE;
                        parm: PELLIPSOIDPARAM) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ��������� ���������� ��� �����             // 06/05/10
  // ��� ������ ���������� ����

function  mapGetSiteEllipsoidParameters(aMap : HMAP; aSite : HSITE;
//                        var parm: PELLIPSOIDPARAM) : integer;   // 25/05/2011
                        var parm: TELLIPSOIDPARAM) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ����������� ������� �����           // 02/02/05
  // ��� ����������� �������. HMAP �������� �������; �
  // ����� ����������� �����
  // name     - ��� ����������� �����
  // password - ������ ������� � ������ �� �������� ����������� 256-������ ���
  //            ��� ���������� ������ (��� ������ ������ �� �����������������)
  // size     - ����� ������ � ������ !!!
  // ��� ������ ���������� ���� (� ���� ������ ����� ��������
  // �������� ������� ����� ��� �������)

function  mapReOpenMapPro(Map : HMap; const mapname : PWChar; const password : PWChar; size:Integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;     // 03/04/2014

function  mapReOpenMap(Map : HMap; const mapname : GTKPChar) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ��� ������ ����������� �����
  // hmap -  ������������� �������� ������
  // ������������� HMAP ���������� ���������������� !

procedure mapCloseData(MapHandle:HMap); //22/09/2009
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

  // ������� ��� ������ ����������� �����
  // hmap -  ������������� �������� ������
  // ������������� HMAP ���������� ���������������� !

procedure mapCloseMap(Map : HMap);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ����������� ������ �����         // 09/01/07
  // oldname - ������ ��� ������
  // newname - ����� ��� ������
  // ��� ������ ���������� ����

function  mapCopyMap(const oldname : GTKPChar; const newname : GTKPChar) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapCopyMapUn(const oldname : PWChar; const newname : PWChar) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� � ������� ��������� ����� (��� ����� ������)
  // hmap - ������������� �������� ������
  // ����� �������� ������������� hMap �� ������ ��������������;
  // ��� ����� mapCloseData()
  // ��� ������ ���������� ����

function  mapDeleteMap(Map : HMap) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // �������� ������ �����            // 09/01/07
  // name - ��� ������
  // ������������� ���������� ������� ��������
  // ��� ������ ���������� ����

function  mapDeleteMapByName(name : GTKPChar) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapDeleteMapByNameUn(name : PWChar) : integer;      // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ������ ������ (�����; ����������; ������; �������...)
  // name - ��� ����� ������� (MPT : ���������; ��� � INI)
  // ��� ������ ���������� ����

function  mapOpenProject(const name : GTKPChar):HMap;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapOpenProjectUn(const name : PWChar):HMap;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF} // 03/04/2014
external sGisAcces;

  // �������� ������ ������ (�����; ����������; ������; �������...)
  // name - ��� ����� ������� (MPT : ���������; ��� � INI)
  // ��� ������ ���������� ����

function  mapAppendProject(Map : HMap; const name : GTKPChar):integer;  // 20/05/05
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapAppendProjectUn(Map : HMap; const name : PWChar):integer;  // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ��� ��������� �������
  // ��� ������ ���������� ������ ������

function  mapGetProjectName(Map : HMap) : GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ��� ��������� �������
  // hmap     -  ������������� �������� ������
  // name     - ����� ��� ���������� ������������ ������
  // namesize - ������ ������ � ������
  // ��� ������ ���������� ����

function  mapGetProjectNameUn(Map : HMap; name:PWChar ; namesize:Integer) : Integer;    // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������� �������� ������ � ������� ������
  // hmap -  ������������� �������� ������
  // name - ��� ����� ������� (MPT : ���������; ��� � INI)
  // ��� ������ ���������� ����

function  mapSaveProject(Map : HMap; const name: GTKPChar) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapSaveProjectUn(Map : HMap; const name: PWChar) : integer;       // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;



  // ��������� ������� ��������� ���� ����� � INI-���� �����;   // 03/10/06
  // ��� ����� ����� ��������� ����� mapGetMapIniName()
  // ���������� ����� ��������� ���� �����
  // ��������� �������� �������� ������; �������; �������; �������� ���������;
  // ���������������; ������ ������������ ��������...
  // hmap  -  ������������� �������� ������
  // point -  ���������� ������ ���� � ������ (����� ���� 0)
  // ��� ������ ���������� ����

function mapSaveMapState(Map : HMAP; var point : TDOUBLEPOINT) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������������ ��������� ���� ����� �� INI-����� �����;      // 03/10/06
  // ��� ����� ����� ��������� ����� mapGetMapIniName()
  // ���������� ����� �������� �����
  // ��������������� �������� ������ ������; �������; �������; �������� ���������;
  // ���������������; ������ ������������ ��������...
  // hmap  -  ������������� �������� ������
  // point -  ���������� ������ ���� � ������ (����� ���� 0)
  // ��� ������ ���������� ����

function mapRestoreMapState(Map : HMAP; var point : TDOUBLEPOINT) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� - ���� �� �����-���� �������� ������
  // ������ - ���������; ���������; ���������...
  // hmap -  ������������� �������� ������
  // ���� �������� ������ ��� ��� ������ - ���������� ����

function  mapIsActive(Map : HMap) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� - ���� �� �����-���� �������� ��������� �����        // 15/07/10
  // ���� �������� ��������� ���� ��� ��� ������ - ���������� ����

function mapIsVectorMapActive(aMap : HMAP): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� - ���� �� �����-���� �������� ��������� �����,        // 15/07/10
  // ��������� ��� ��������������
  // ���� ��������� ��� �������������� ��������� ���� ��� 
  // ��� ������ - ���������� ����

function mapIsVectorMapEdit(aMap : HMAP): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� - ���� �� �����-���� �������� ��������� �����        // 15/07/10
  // ���� �������� ��������� ���� ��� ��� ������ - ���������� ����

function mapIsVectorActive(aMap : HMAP): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������������ ������ ����������� �����
  // � ������ � �� ����� (��� ��������������������� �������
  // � ������)
  // hmap -  ������������� �������� ������
  // ���� ��������� ������ � ������ ���������� (�� ������
  // � �����) - ���������� ��������� �������� (1), ����� 0
  // ���� ����� ������ ���� ������� - ���������� 2
  // (������ �� ��� ������ ���������!)
  // ���� ��������� ���������� - ���������� ������������
  // ����������� �����
  // ����� ��������� ������������� ��������� ������������
  // � �������� ������ ����������

function mapAdjustData(Map:HMap):integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ����������� ��� ���������� ������� Adjust
  // mode - ������� ����������� ��������� ������� Adjust;
  //        ���� ����� 0 - ������� �� ��������������.
  // ��� ���������� ���������� �������� (������ �������
  // ����������; ����������������� ������ � �.�.) �������������
  // ��������� ������� Adjust; ���� ��� ����� ���� ������� ��
  // ������ ������� ����������. ������� Adjust ����� ��������
  // ������������ ���� � ����������������� ������.
  // ���������� ������� ��������

function  mapSetAdjustMode(Map : HMap; mode : integer) : integer;       // 25/04/08
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������ �� ����                              // 10/08/07
  // ��� ������ �� Sparc-��������� ����� ��������� ������ � ����� Intel-���������
  // hMap - ������������� ������� �����
  // hSite - ������������� ���������������� �����
  // ��� ������ ���������� 0

function  mapFlushData(Map : HMap; Site : HSite) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // �������(��������) ����� ���� � ������
  // hmap -  ������������� �������� ������
  // ��������� LISTREGISTER ������� � mapcreat.h
  // ��� ������ ���������� ����

function  mapCreateList(Map : HMap; var sheet : TLISTREGISTER) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ��������� ���� �����
  // list - ����� ����� (� 1)
  // ��� ������ ���������� ����

function  mapDeleteList(Map : HMap;list : integer ) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // �������� ����� �� ������� ������ ����� � ������
  // hmap -  ������������� �������� ������
  // mapname - ����������� �����;
  // handle - ������������� ����;������� ����� ����������
  // � ���� �������� (0x585 - 0x588)
  // ��� ������ ���������� ����

{$IFNDEF LINUXAPI} // 22/09/2009

function mapAppendMapToMap(Map:HMAP;mapname:GTKPChar;handle:HWND):integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapAppendMapToMapUn(Map:HMAP;mapname:PWChar;handle:HWND):integer;    // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

{$ENDIF}

  // C������ ������ ������ ��������� �����
  // (�������� ���������� - ��. �������������� �������)
  // �� ��������� ������� FLOAT 2-�� ������
  // hmap - ������������� �������� ������
  // sheetnumber - ����� ����� � ������� ����� ����������
  // kind - ��� ����������� �������; ������ � maptype.h
  // ����������� ������
  // text - ������� ������� � ������� (������� ���� "�������")
  // (��������������� ������������� ��� ������ mapPutText(...))
  // ����� ������ ������� ���� What...() � Seek...() ��� ���������
  // ����������� ������� ����� ���������� (text;kind;list � �.�.)
  // ��� ������ ���������� ����
function mapCreateObject(Map:HMap;list:integer= 1;kind:integer= KM_IDDOUBLE2;text:integer=0):HObj;
{$ifndef HIDEFORMAT}
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;
{$ENDIF}


  // �������� ���������� �������
  // (��� ���������� ����������; ��� ������� �������)
  // hmap - ������������� �������� ������
  // sheetnumber - ����� ����� � ������� ����� ����������
  // kind - ��� ����������� �������; ������ � maptype.h
  // ��� ������ ���������� ����

function mapClearObject(aObj : HObj; aSheetNumber : integer=1; aKind : integer= KM_IDFLOAT2): integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 


  // C������ ����� ������� ��������� �����
  // hmap - ������������� �������� ������
  // info - ������������� ������� ����� � ������
  // ��� ������� ����������� � ������ �� �������������
  // �������������� HOBJ ��������� ����� ������� mapFreeObject()
  // ��� ������ ���������� ����

function mapCreateCopyObject(Map:HMap;Obj:HObj):HObj;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ����� ������� ��������� (src) ����� � ������ ������ (dest)
  // ��� ������� ����������� � ������ �� �������������
  // �������������� HOBJ ��������� ����� ������� mapFreeObject()
  // ��� ������ ���������� ����

function  mapReadCopyObject(Dest,Src : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ����� ���������� ��������� (src) ����� � ������ ������ (dest)   // 03/04/2014
  // ��� ������� ����������� � ������ �� �������������
  // �������������� HOBJ ��������� ����� ������� mapFreeObject()
  // ��� ������ ���������� ����

function  mapReadCopySubject(Dest,Src : HObj; subject:Integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // C������ ����� ������� ��������� �����; ��� ������ ������� (!)
  // hmap - ������������� �������� ������
  // info - ������������� ������� ����� � ������
  // ��� ������� ����������� � ������ �� �������������
  // �������������� HOBJ ��������� ����� ������� FreeObject()
  // ��� ������ ���������� ����

function  mapCreateCopyObjectAsNew(Map : HMap;Obj : HObj) : HObj;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // C������ ����� ������� ��������� �����; ��� ������ �������
  // ��� ������� ����������� � ������ �� �������������
  // �������������� HOBJ ��������� ����� ������� FreeObject()
  // src  - �������� ������;
  // dest - ����� ������� (��� ���������� - ����� ������ �����)
  // ��������������; ��� �� ���������� � ����� ���-�� ��������.
  // ��� ������ ���������� ����

function  mapCopyObjectAsNew(Dest,Src : HObj): integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� �������� ������� ��������� ����� �� ������
  // info - ������������� ������� ����� � ������
  // ��� ���������� ������� �� ����� ����������
  // �� ������ mapFreeObject(...) ��������� �������
  // mapCommitObject(...)
  // ��� ������ ���������� ����

procedure mapFreeObject(Obj : HObj);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ��� ������ ��������� �������� ������� � ������
  // ���� ������ - ��. maperr.rh

function  mapGetAccessError: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������� ������; ������� ����������� ��������� ����� ��������������
 // ���� ����������� ����
 // ����������� ��� ��������� � ������ catch(..) ��� ������ �����������
 // ����� ��� ������ ��������������� ����������
 // hmap - ������������� �������� ������;
 // info - ������������� ������������� �������;
 // � ������� ����� �������� ��������� ������.
 // ���� ����� ������ �� ���������� - ���������� ����

function  mapReadLastViewObject(Map : HMap; Obj : HObj) : integer; 
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������, ������������� �� ������ �� �������                               
  // ������ ������� ����� ���� ���������� � ��������
  // � ��������� ��������
  // ��� ������ ���������� ����

function  mapIsObjectStretch(info : HOBJ) : integer;        // 20130127 Korolev
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces; 


  //**********************************************************
  //*                                                        *
  //*    ������� ����������� � ������ ����������� �����      *
  //*  ��������� ��������� ������������ (HDC) � �����������  *
  //*  �������� �� ������� ������ � ���� ::SetViewportOrgEx()*
  //*                                                        *
  //*  ������ ��������; �������� �� ���� ����� �� ����� ���  *
  //*  � ������; �� ����� ������� �������� ������!           *
  //*  ������������ ������ - ��. PaintToDib;PaintToImage;    *
  //*                            PaintToXImage               *
  //*                                                        *
  //**********************************************************

 // ���������� ����� ��������� �������� ��������� ��� �����������,       // 21/05/10
 // ������ � ������� ���������
 // hmap  - ������������� �������� ������ (���������)
 // ��������� MAPREGISTER, DATUMPARAM � ELLIPSOIDPARAM ������� � mapcreat.h
 // ������������� ����� ��������� �������� ����� ��� ���������
 // ��������������� �������� ������������� ��������� (mapIsGeoSupported() != 0)
 // ����� ��������� ����� ���������� �������� ����������� ����� �����������
 // � �������� ��������. ��������� �����, ������� ������ ���������
 // ��������, ���������������� � �������� �����������.
 // ������� � ������ �� ���������������� ��� ����������� � ������ ����� 
 // �� �� ��������� ��������, ��� � ����� ��������� ���������
 // ��� �������� � ������������ (mapPlaneToGeo, mapGeoToPlane, 
 // mapPlaneToGeoWGS84, mapAppendPointPlane, mapInsertPointPlane,
 // mapUpdatePointPlane, mapAppendPointGeo � ������) �����������
 // � ������� ��������� ���������, ������������ ������ ����������� ��������
 // ��� ������\������ ��������� � ���������� ����� ����������� ��������
 // �� ������� ��������� ���������
 // ��������, ��� ������ ��������� �� WGS84 �� ����� � ��-42 �����
 // ���������� ����� ��������� ���������, ��� "������/������� �� WGS-84"
 // � ��������� ������ ��������� �������� mapAppendPointGeo, �� ��������
 // � �������������� ��������� ���������, ��� ������� ���������� ��������
 // mapGetGeoPoint (��� �������� mapGetGeoPointWGS84, ������������ ���������
 // ���������).
  // ����� ���������� ������� ��������� �������� � ������� ���������, ��� � ������
 // ����� � ��������� ����� �������� � �������� ���������� (����� hMap) ����,
 // ��� ������� mapClearDocProjection.
 // ��� ������ ���������� ����

function mapSetDocProjection(aMap : HMAP;
                             map : PMAPREGISTEREX;
                             datum : PDATUMPARAM;
                             ellparm : PELLIPSOIDPARAM) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapClearDocProjection(aMap : HMAP) : integer;      // 03/04/2014
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;



 // ��������� ����� ��������� �������� ��������� ��� �����������,       // 21/05/10
 // ������ � ������� ���������
 // hmap  - ������������� �������� ������ (���������)
 // ��������� MAPREGISTER, DATUMPARAM � ELLIPSOIDPARAM ������� � mapcreat.h
 // ���� ��������� �� ��������������� �������� mapSetMapInfoEx,
 // �� ��� ������������ ���������� �����, �������� � ��������� ������
 // ��� ������ ���������� ����

function mapGetDocProjection(aMap : HMAP;
                             map : PMAPREGISTEREX; // 28/06/2012
                             datum : PDATUMPARAM;
                             ellparm : PELLIPSOIDPARAM) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // ��������� - ��������������� �� ����� ��������� �������� ���������
 // ��� �����������, ������ � ������� ���������
 // hmap  - ������������� �������� ������ (���������)
 // ��� ������ ���������� ����

function mapIsDocProjection(aMap : HMAP): integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // ��������� ������� ������ ����������� ����� � ��������
 // ��� �������� ��������
 // hmap - ������������� �������� ������
 // � ���������� width ��������� ������ ����������� (dx);
 // � ���������� height - ������ (dy)

procedure mapGetPictureSize(Map:HMap;
                            var width:integer;
                            var height:integer);
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}; // 23/09/2009
  external sGisAcces;

function  mapGetPictureHeight(Map : HMap) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetPictureWidth(Map : HMap) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������ ������� ����������� ����� � ������ �� ���������
 // ��� �������� �������� �����������                  // 22/11/06
 // ��� ������ ���������� ����

function  mapGetPixelWidth(Map : HMap) : double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������ ������� ����������� ����� � ������ �� ���������
 // ��� �������� �������� �����������
 // ��� ������ ���������� ����

function  mapGetPixelHeight(Map : HMap) : double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������������� ���������� ������� �����
 // hmap - ������������� �������� ������
 // ��� ������ ���������� ����

function  mapGetMapPalette(Map : HMap) : HPalette;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� �������� ����� ������� �� ������� (index)
 // hmap - ������������� �������� ������
 // ����� ������� ������� ����� ����

function mapGetMapColor(Map:HMap;index:longint):COLORREF; // 23/09/2009
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ��������� ����� ������ ���������� �������
 // hmap - ������������� �������� ������
 // ��� ������ ���������� ����

function  mapGetColorCount(Map : HMap) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� �������� ����� �� �������� ����������
 // � ������� �������� � ������� ��������
 // hmap - ������������� �������� ������
 // hdc   - ������������� ��������� ���������� ������;
 // erase - ������� �������� ���� ����� �������;
 // (0 - ��� �� �������; !=0 - �������� �������� ������ ����;
 //  ��� ��������� ������� ������ (VT_SCREEN) ������ �������
 //  ������ ����; ����� �������� -2 (����� 2))
 // rect  - ���������� ��������� ����� (Draw)
 // � ����������� (Picture).
 // � ������������� ������ 32000�32000 �������� ������
 // ��� Windows NT (Windows 95 ����� 16-������� �������)
 // ������ ��������; �������� �� ���� �����; �� �����
 // ������� �������� ������! ����� - ��. PaintToDib;PaintToImage...

procedure mapPaint(Map : HMap; DC:HDC; erase: integer;var Rect : TRect);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� �������� �����, �������������� �� �������� ������ � ������  // 31/03/11
 // � ������� ������� �������� (����������� ��� ��������� OGC WMS-��������)
 // frame  - ���������� ��������� ����� � ������� ��������� ��������� � ������
 // (��. mapSetDocProjection)
 // width  - ������ ����������� � ��������
 // height - ������ ����������� � ��������
 // alpha - ���� ������������� ����� ������ 0 - �� ������������ 1 - ������������
 // filename  - ������ ��� ������������ ����� ������� png
 // ��� ������ ���������� ����

function mapPaintByFrame(Map : HMap; DC:HDC; erase: integer;                             // 20130127 Korolev
                           frame : PDFRAME; width, height : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapPaintByFrameEx(Map : HMAP; DC : HDC; erase : integer; frame : PDFRAME;       // 20130127 Korolev 
                                           width, height, alpha : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
									
function mapPaintByFrameToFile(Map : HMAP;  const filename : GTKPCHAR; erase : integer;  // 20130127 Korolev
                               frame : PDFRAME; width, height : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;						

 // ���������� �������� ����� �� �������� ����������
 // � ������� �������� � ������� ��������
 // amap  - ������������� �������� ������
 // dc   - ������������� ��������� ���������� ������;
 // erase - ������� �������� ���� ����� �������;
 //        (0 - ��� �� �������; !=0 - �������� �������� ������ ����;
 //        ��� ��������� ������� ������ (VT_SCREEN) ������ �������
 //        ������ ����; ����� �������� -2 (����� 2))
 // rect - ���������� ��������� ����� (Draw) � ����������� (Picture)
 // ��������� �������� � �������� ������������� ��� Windows95 � NT;
 // �� ������� ����� ������� ���������
 //              ::SetViewportOrgEx(hDC; dx ; dy; 0);
 // ��� dx;dy - ��������� ������������� ��������� � ���������
 // ������� !
 // ������ ��������; �������� �� ���� �����; �� �����
 // ������� �������� ������! ����� - ��. PaintToDib;PaintToImage...
 // alpha - ���� ������������� ����� ������ 0 - �� ������������ 1 - ������������
 // filename  - ������ ��� ������������ ����� ������� png
 // alpha - ���� ������������� ����� ������ 0 - �� ������������ 1 - ������������
 // ���� image != 0 � object != 0 ������������� ��������� ������
 // mapPaintMapObject95Ex  (���������� ������������ ������ � �������� ���������
 // � �������� ������ ������������)
 // image - �������� ���� ������� (��. MAPGDI.H),
 // obj - ������������� �������

procedure mapPaint95(amap : HMap; DC:HDC; erase: integer; var Rect : TRect);                         // 20130127 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

procedure mapPaint95Ex(amap : HMAP; dc : HDC; erase : integer; var rect : TRECT; alpha : integer);   // 20130127 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapPaint95ToFile(amap : HMAP; const filename : GTKPCHAR; erase : integer; var rect : TRECT; // 20130127 Korolev
                          alpha : integer; image : PPAINTPARM; obj : HOBJ) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapPaint95ToFileUn(amap : HMAP; const filename : PWCHAR;
                          erase : integer; var rect : TRECT;                            // 03/04/2014
                          alpha : integer; image : PPAINTPARM; obj : HOBJ) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // ���������� �������� ����� �� �������� ����������
 // � ������� �������� � ������� �������� �
 // �������� �� ����� �������; �������������� �������� �������
 // aMap   - ������������� �������� ������
 // hdc    - �������� ����������
 // erase  - ������� �������� ���� ����� �������;
 //          (0 - ��� �� �������; !=0 - �������� �������� ������ ����;
 //          ��� ��������� ������� ������ (VT_SCREEN) ������ �������
 //          ������ ����; ����� �������� -2 (����� 2))
 // rect   - ���������� ��������� ����� (Draw) � ����������� (Picture)
 // select - ������� ������ ��������; ���� ����� 0; �� �����������
 //          ������� ����������� ������\��������� (��. mapTotalPaintSelect95).
 // color  - ����; ������� ����� ���������� ������� �� �����
 // ��������� �������� � �������� ������������� ��� Windows95 � NT;
 // �� ������� ����� ������� ���������
 //              ::SetViewportOrgEx(hDC; dx ; dy; 0);
 // ��� dx;dy - ��������� ������������� ��������� � ��������� ������� !
 // ������ ��������; �������� �� ���� �����; �� �����
 // ������� �������� ������! 
 // alpha - ���� ������������� ����� ������ 0 - �� ������������ 1 - ������������
 // filename  - ������ ��� ������������ ����� ������� png
 // alpha - ���� ������������� ����� ������ 0 - �� ������������ 1 - ������������
 // ���� image != 0 � object != 0 ������������� ��������� ������
 // mapPaintMapObject95Ex  (���������� ������������ ������ � �������� ���������
 // � �������� ������ ������������)
 // image - �������� ���� ������� (��. MAPGDI.H),
 // object - ������������� �������

procedure mapPaint95AndSelect(aMap : HMap; DC:HDC; erase: integer; var Rect : TRect;
                               select:HSelect; color : TColorRef); // 03/11/06
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

procedure mapPaint95AndSelectEx(aMap : HMAP; DC : HDC; erase : integer; var rect : TRECT;    // 20130127 Korolev
                                select : HSELECT; color : COLORREF; alpha : integer); 
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;								

function  mapPaint95AndSelectToFile(aMap : HMAP; const filename : GTKPCHAR; erase : integer; // 20130127 Korolev
                                    var rect : TRect; select : HSELECT; color : TCOLORREF; alpha : integer;
                                    image : PPAINTPARM; obj : HOBJ) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
                                                                                         // 03/04/2014

function  mapPaint95AndSelectToFileUn(aMap : HMAP; const filename : PWCHAR; erase : integer; // 20130127 Korolev
                                    var rect : TRect; select : HSELECT; color : TCOLORREF; alpha : integer;
                                    image : PPAINTPARM; obj : HOBJ) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;




 // ���������� ������� ����� ��� ��������� ���������� �� �����
 // �������� (��� ������ mapPaint95AndSelect � �.�.)                   // 19/10/07
 // thick - ������� ����� � mkm (�� �������� - PIX2MKM(pixel))
 // ���������� ������������� ����� ��������

function  mapSetSelectLineThick(Map : HMap; thick: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ��������� ������ ����� � �������� ���������
 // � �������� ������ �����
 // ����� �������������� ��� ������ �������� ��������
 // hmap - ������������� �������� ������
 // hdc   - ������������� ��������� ���������� ������;
 // rect  - ���������� ��������� ����� (Draw)
 // info - ������������� ������� ����� � ������
 // ��� ������ � ���������� ���������� ����
 // ������ ��������; �������� �� ���� �����; �� �����
 // ������� �������� ������! ����� - ��. PaintToDib;PaintToImage...

function  mapPaintObject(Map : HMap; DC:HDC; var Rect : TRect; Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ��������� ������ ����� � �������� ���������
 // � �������� ������ �����
 // ����� �������������� ��� ������ �������� ��������
 // hmap - ������������� �������� ������
 // hdc   - ������������� ��������� ���������� ������;
 // rect  - ���������� ��������� ����� (Draw)
 // info - ������������� ������� ����� � ������
 // erase -  ������� �������� ���� ����� �������;
 // (0 - ��� �� �������; !=0 - �������� �������� ������ ����);
 // dontclip - ������� ������ ������� �������� �� ����� ��� �����������
 // (1 - �� ��������; 0 - ��������� �������)            // 21/03/01
 // ��� ������ � ���������� ���������� ����
 // ������ ��������; �������� �� ���� �����; �� �����
 // ������� �������� ������! ����� - ��. PaintToDib;PaintToImage...

function  mapPaintObjectEx(Map : HMap; DC:HDC; var Rect : TRect; Obj : HObj;
                            erase: integer; dontclip : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ��������� ������ ����� � �������� ���������
 // � �������� ������ �����
 // ����� �������������� ��� ������ �������� ��������
 // hmap - ������������� �������� ������
 // hdc   - ������������� ��������� ���������� ������;
 // rect  - ���������� ��������� ����� (Draw)
 // info - ������������� ������� ����� � ������
 // ��������� �������� � �������� ������������� ��� Windows95 � NT;
 // ��� ������ � ���������� ���������� ����
 // ������ ��������; �������� �� ���� �����; �� �����
 // ������� �������� ������! ����� - ��. PaintToDib;PaintToImage...
 // alpha - ���� ������������� ����� ������ 0 - �� ������������ 1 - ������������

function  mapPaintObject95(Map : HMap; DC:HDC; var Rect : TRect; Obj : HObj) : integer;    // 20130127 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapPaintObject95Ex(map : HMAP; dc : HDC; var Rect : TRect;                       // 20130127 Korolev
                             info : HOBJ; alpha : integer) : integer; 
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;							 

 // ���������� ������������ ������ � �������� ��������� ���� (�����)
 // � �������� ������ ������������
 // hmap - ������������� �������� ������
 // hdc   - ������������� ��������� ���������� ������;
 // rect  - ���������� ��������� ����� (Draw)
 // image - �������� ���� ������� (��. MAPGDI.H); ���� ������
 // ������ ���������� ����� �������� ������ - �������� ���������
 // ����� ���������� � ����
 // info - ������������� ������� ����� � ������
 // ��� ������ � ���������� ���������� ����
 // ������ ��������; �������� �� ���� �����; �� �����
 // ������� �������� ������! ����� - ��. PaintToDib;PaintToImage...
 // filename  - ������ ��� ������������ ����� ������� png
 // alpha - ���� ������������� ����� ������ 0 - �� ������������ 1 - ������������

function  mapPaintMapObject95(Map : HMap; DC:HDC; var Rect : TRect;
                              var image  : TPAINTPARM; Obj : HObj) : integer; // 30/09/2009
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapPaintMapObject95Ex(Map : HMAP; dc : HDC; var Rect : TRect;                    // 20130127 Korolev
                                image : PPAINTPARM; obj : HOBJ; alpha : integer) : integer; 
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;								
									  
function  mapPaintMapObject95ToFile(Map : HMAP; const filename : GTKPCHAR;                 // 20130127 Korolev
                                    var Rect : TRect; image : PPAINTPARM;
                                    obj : HOBJ; alpha : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapPaintMapObject95ToFileUn(Map : HMAP; const filename : PWCHAR;                 // 03/04/2014
                                    var Rect : TRect; image : PPAINTPARM;
                                    obj : HOBJ; alpha : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������������ ������ � �������� ���������
 // � �������� ������ ������������
 // hmap - ������������� �������� ������
 // hdc   - ������������� ��������� ���������� ������;
 // rect - ���������� ��������� ����� (Draw) � ����������� (Picture)
 // image - �������� ���� ������� (��. MAPGDI.H); ���� ������
 // ������ ���������� ����� �������� ������ - �������� ���������
 // ����� ���������� � ����;
 // info - ������������� ������� ����� � ������
 // offset - �������� ��������� ������� (� ������������ � place)
 // ��� ������ � ���������� ���������� ����

function mapPaintOffsetMapObject95(Map:HMap;DC:HDC;var rect:TRect; // 23/09/2009
                                   var image:TPAINTPARM;Obj:HObj;
                                   offset:TDOUBLEPOINT;
                                   place:integer=cPP_MAP):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


 // ���������� ������� ���� ������� �� ������ ������      
 // � �������������� �������� (incode)
 // hmap - ������������� �������� �����
 // hrsc - ������������� �������������� �������� �����
 // hdc  - ������������� ��������� ���������� ������;
 // rect - ���������� ���������� ������� ���� ������ (������ ����)
 // ��� ������ ���������� ����

function  mapPaintExampleRscObject(hMap:HMAP;hRsc:HRSC;hdc:HDC;
                                   rect: Pointer;incode:Integer):Integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

// ���������� ������� ���� �������
// ��� ������ ���������� ����

function  mapPaintExample(Map : HMap;var  parm : TPAINTEXAMPLE) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

// 11/12/13 {$ifdef WIN32}
{$IFNDEF LINUXAPI}          // 11/12/13
 // ������� ����������� ����� � DIB
 // ������ ������� ����������� ������ ��� ��������� Windows !
 // ��������� �������� �����; �������� ���������� rect.
 // ���������� �������;������������� �� BITMAINFOHEADER
 // ����������� �������� � ������� �������� � ������� ��������
 // ������ ����� 1;2;3 ��� 4 ����.
 // ���� ������� ��������� DIB ������ �������� ��������� -
 // ����������� �������� � �������� �������� DIB.
 //  hmap      - ������������� �������� ������
 //  dibinfo   - ��������� ������������ DIB � ������
 //  lpDibBits - ����� ������� ����� ������� �������.
 //  rect      - ���������� ��������� ����� (Draw)
 //              � ����������� (Picture).
 // ������ ������� DIB; ���������� ���������� dibinfo->biSizeImage;
 // ������ ��������� ��; ��� ������ ������ DIB ������ ����
 // ������ 4 ������ (32 �����):
 //  dibinfo->biSizeImage = dibinfo->biHeight *
 //    ((dibinfo->biWidth * dibinfo->biBitCount + 31) / 32) * 4;
 // ��� ������ � ���������� ���������� ����

{$IFNDEF LINUXAPI}
function mapPaintToDib(Map:HMap;var dibinfo:TBITMAPINFOHEADER;
                       lpDibBits: pointer; var rect: TRect):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;
{$ENDIF}

{$ifndef WINCE}
 // ������� ����������� ����� � Image (������)
 // ������ ������� ����������� ������ ��� ��������� Windows !
 // ��������� �������� �����; �������� ���������� rect.
 // ����������� ������� � ���� palette.
 // ����������� �������� � ������� �������� � ������� ��������
 // ������� Image ������ 256 ������; ������ ����� 1 ���� !
 // ���� ������� ��������� Image ������ �������� ��������� -
 // ����������� �������� � �������� �������� Image.
 //  hmap          - ������������� �������� ������
 //  palette       - ����� ������� (256 RGBQUAD-������)
 //  lpImage       - ����� ������� ����� ������� �����������.
 //  width; height - ������ � ������ Image.
 //  rect          - ���������� ��������� ����� (Draw)
 //                  � ����������� (Picture).
 // ��� ������ � ���������� ���������� ����

function  mapPaintToImage(Map : HMap; var palette : TRGBQUAD;
                           lpImage: GTKPCHAR; width, height: integer; // 30/09/2009
                           var Rect : TRect) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
{$endif}  // !WINCE
{$endif}  // LINUXAPI   // 11/12/13  // WIN32API

 // ������� ����������� ������� � DIB                 // 25/11/08
 //  hmap      - ������������� �������� ������
 //  lpDibBits - ����� ������� ����� ������ ����������� (32 ����)
 //  width     - ������ ����������� (������ 32);
 //  height    - ������ �����������;
 //  rect      - ���������� ��������� ����� (Draw)
 //              � ����������� (Picture).
 //  image     - �������� ���� ������� (��. MAPGDI.H); ���� ������
 //              ������ ���������� ����� �������� ������ - �������� ���������
 //              ����� ���������� � ����;
 //  info      - ������������� ������� ����� � ������
 // ��� ������ � ���������� ���������� ����

function  mapPaintMapObjectToDib(Map : HMap; lpImage : GTKPChar; width, height: integer;
    var Rect : TRect ; image : PPAINTPARM; info: HObj) : integer; // 26/10/2009 
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 {$IFNDEF LINUXAPI}

// ���������� �������� �����, �������������� �� �������� ������ � ������
// � ������� ������� ��������
// frame  - ���������� ��������� ����� � ������� ��������� ��������� � ������
// (��. mapSetDocProjection)
// width  - ������ ����������� � ��������
// height - ������ ����������� � ��������
// �������� ��������� XIMAGEDESC � maptype.h
// alpha - ���� ������������� ����� ������ 0 - �� ������������ 1 - ������������
// ��� ������ ���������� ����

function   mapPaintByFrameToXImage(Map : HMap;var imagedesc: TXIMAGEDESC;
                                     var frame: TDFRAME; width,height,alpha : integer)  : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // ������� ����������� ����� � XImage (������)
 // ������ ������� ����������� ��� XWindow !
 // �������� ��������� XIMAGEDESC � maptype.h
 // hmap - ������������� �������� ������
 // x;y - ���������� ������ �������� ���� ������
 // ������� ������� XImage ��� ���������� �����������
 // rect - ��������� �������� �����
 // ��� ������ � ���������� ���������� ����

function   mapPaintToXImage(Map : HMap;var imagedesc: TXIMAGEDESC;
                                     x,y : integer; var Rect : TRect)  : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ����������� ����� � XImage (������) � ��������
  // �� ����� ���������� �������                                   // 05/11/06
  // ������ ������� ����������� ��� XWindow !
  // �������� ��������� XIMAGEDESC � maptype.h
  // x;y - ���������� ������ �������� ���� ������
  // ������� ������� XImage ��� ���������� �����������
  // rect - ��������� �������� �����
  // select - ������� ������ ��������; ���� ����� 0; �� �����������
  //          ������� ����������� ������\��������� (��. mapTotalPaintSelect95).
  // color  - ����; ������� ����� ���������� ������� �� �����
  // ��� ������ � ���������� ���������� ����

function   mapPaintAndSelectToXImage(Map : HMap; var imagedesc: TXIMAGEDESC;
                                     x,y : integer; var Rect : TRect;
                                     select : HSelect; color : TCOLORREF) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ����������� ����� � ������� � XImage (������)         // 05/06/08
  // ������ ������� ����������� ��� XWindow !
  // �������� ��������� XIMAGEDESC � maptype.h
  // x;y - ���������� ������ �������� ���� ������
  // ������� ������� XImage ��� ���������� �����������
  // rect - ��������� �������� �����
  // ��� ������ � ���������� ���������� ����

function   mapPaintMapAndObjectToXImage(Map : HMap; Obj : HObj;
  var imagedesc: TXIMAGEDESC; x,y : integer; var Rect : TRect)  : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������� ����������� ����� � XImage (������)                // 07/02/03
 // ������ ������� ����������� ��� XWindow !
 // �������� ��������� XIMAGEDESC � maptype.h
 // x;y - ���������� ������ �������� ���� ������
 // ������� ������� XImage ��� ���������� �����������
 // rect - �������� ��� ������ �����������
 // func - ������� ����������� �������
 // parm - ��������� �����������
 // data - ������� ��� �����������
 // colors - ���������� ������
 // palette - �������
 // ��� ������ � ���������� ���������� ����
function   mapPaintExampleObjectByFuncDataToXImage(Map : HMap;
     var imagedesc: TXIMAGEDESC;x,y : integer; var Rect : TRect; func : integer;
     param : GTKPCHAR; data: TPOLYDATAEX; colors : integer;
     palette : TCOLORREF) : integer; // 03/08/05
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
  // ������� ����������� ����� � XImage (������)                // 18/02/03
  // ������ ������� ����������� ��� XWindow !
  // �������� ��������� XIMAGEDESC � maptype.h
  // rect - �������� ��� ������ �����������
  // func - ������� ����������� �������
  // parm - ��������� �����������
  // colors - ���������� ������
  // palette - �������
  // text - ����������� �����
  // local - �����������
  // ��� ������ � ���������� ���������� ����
function  mapPaintExampleObjectByFuncToXImage(Map : HMap; var imagedesc: TXIMAGEDESC;
      var Rect : TRect;	func : integer; param : GTKPCHAR {� �++ ���� �� PWChar} ; colors : integer; // 30/09/2009
        palette : TCOLORREF; text : GTKPChar;local : integer) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapPaintExampleObjectByFuncToXImageUn(Map : HMap; var imagedesc: TXIMAGEDESC;
      var Rect : TRect;	func : integer; param : GTKPCHAR ; colors : integer; // 03/04/2014
        palette : TCOLORREF; text : PWChar;local : integer) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

{$ENDIF} // LINUXAPI

// 11/12/13 {$IFDEF WIN32}
{$IFNDEF LINUXAPI}            // 11/12/13
{$IFNDEF WINCE}

 // ������� ����������� ������� � Image
 // ������ ������� ����������� ������ ��� ��������� Windows !
 // ��������� �������� �������; �������� ���������� rect.
 // ����������� �������� � ������� ��������
 // ������� Image ������ 256 ������; ������ ����� 1 ���� !
 // ���� ������� ��������� Image ������ �������� ��������� -
 // ����������� �������� � �������� �������� Image.
 // hmap - ������������� �������� ������
 // lpImage - ����� ������� ����� ������� �����������.
 // ImageAreaWidth � ImageAreaHeight - ������ � ������ Image.
 // rect - ���������� ��������� ����� (Draw)
 // � ����������� (Picture).
 // info - ������������� ������� ����� � ������
 // ��� ������ � ���������� ���������� ����

function  mapPaintObjectToImage(Map : HMap; lpImage : GTKPCHAR;  // 30/09/2009
       ImageAreaWidth, ImageAreaHeight: integer;
                   var Rect : TRect; Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������� ����������� ����� � ��������
 // ������ ������� ����������� ������ ��� ��������� Windows !
 // ��� ������ � ���������� ���������� ����

function mapPaintToEmf(map : HMAP; const EmfName : GTKPChar;
                       var parm : TMETAFILEBUILDPARM) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function mapPaintToEmUnf(map : HMAP; const EmfName : PWChar;               // 03/04/2014
                       var parm : TMETAFILEBUILDPARM) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function mapPaintToEmfEx(map : HMAP; const EmfName : GTKPChar;
                         var parm : TMETAFILEBUILDPARMEX) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;
  function mapPaintToEmfExUn(map : HMAP; const EmfName : PWChar;
                         var parm : TMETAFILEBUILDPARMEX) : integer; // 03/04/2014
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;
{$endif}  // !WINCE
{$endif}  // LINUXAPI // 11/12/13 // WIN32API

 // ���������� ������������ ������ � �������� ���������
 // � �������� ������ ������������
 // hmap - ������������� �������� ������
 // hdc  - ������������� ��������� ���������� ������;
 // rect - ���������� ��������� ����� (Draw)
 // � ����������� (Picture).
 // image - �������� ���� ������� (��. MAPGDI.H);
 // data  - ���������� �������.
 // ���������� ������� ������ � ������������ � ���������� place
 // � ������������� ������ 32000�32000 �������� ������
 // ��� Windows NT (Windows 95 ����� 16-������� �������)
 // ��� ������ � ���������� ���������� ����

function  mapPaintUserObject(Map : HMap;DC : HDC; var Rect : TRect;
                                            var image : TPAINTPARM; var data : TPLACEDATA;
                                            place: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������������ ������ � �������� ���������
 // hmap - ������������� �������� ������
 // hdc  - ������������� ��������� ���������� ������;
 // rect - ���������� ��������� ����� (Draw)
 // � ����������� (Picture).
 // image - �������� ���� ������� (��. MAPGDI.H);
 // data  - ���������� �������.
 // offset - �������� ��������� ������� (� ������������ � place)
 // ���������� ������� ������ � ������������ � ���������� place
 // ��������� �������� � �������� ������������� ��� Windows95 � NT;
 // ��� ������ � ���������� ���������� ����

function  mapPaintOffsetUserObject95(Map : HMap;DC : HDC;
                                                     var Rect : TRect;
                                                     var image : TPAINTPARM;
                                                     var data : TPLACEDATA;
                                                     var offset : TDoublePoint;
                                                     place: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������������ ������ � �������� ���������
 // hmap - ������������� �������� ������
 // hdc  - ������������� ��������� ���������� ������;
 // rect - ���������� ��������� ����� (Draw)
 // � ����������� (Picture);
 // image - �������� ���� ������� (��. MAPGDI.H);
 // data  - ���������� �������.
 // ���������� ������� ������ � ������������ � ���������� place
 // ��������� �������� � �������� ������������� ��� Windows95 � NT;
 // ��� ������ � ���������� ���������� ����

function  mapPaintUserObject95(Map : HMap; DC : HDC;
                                              var Rect : TRect;
                                              var image : TPAINTPARM;
                                              var data : TPLACEDATA;
                                              place: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������� ���� ������� �� ������ ������
 // � �������������� �������� (incode)
 // hmap - ������������� �������� ������
 // hdc  - ������������� ��������� ���������� ������;
 // rect - ���������� ��������� ����� (Draw)
 // � ����������� (Picture);
 // ������������ � �������� ������ ���� �������
 // ��� ������ ���������� ����

function  mapPaintExampleObject(Map : HMap; DC : HDC;
                                               var Rect : TRect;
                                               incode: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������� ���� ������� c ������ ���� ������������ ��
 // ������ ������ � �������������� �������� (incode)
 // ������������ � �������� ������ ���� �������
 // hmap - ������������� �������� ������
 // hdc  - ������������� ��������� ���������� ������;
 // rect - ���������� ��������� ����� (Draw)
 // � ����������� (Picture);
 // visualtype - ��� ������������
 // ��� ������ ���������� ����

function  mapPaintExampleObjectEx(Map : HMap; DC : HDC;var Rect : TRect;
                                                 incode ,visualtype: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������� ���� ������� c ������ ���� ������������ ��      // 23/02/14 // 03/04/2014
 // ������ ������ � �������������� �������� (incode)
 // ������������ � �������� ������ ���� �������
 // hmap - ������������� �������� ������
 // hdc  - ������������� ��������� ���������� ������,
 // rect - ���������� ��������� ����� (Draw)
 // � ����������� (Picture),
 // visualtype - ��� ������������ (VT_SCREEN, VT_PRINT)
 // text - ����� ��� ����������� ����� ���� �������
 // factor - ������� ��������� ������� ����� ��� ����������� ������� ������
 //          � ��������� ������� (50 - ����� � 2 ����, 150 - ��������� � 1,5 ����)
 // ��� ������ ���������� ����

 function  mapPaintExampleObjectUn(Map : HMap; DC : HDC;var Rect : TRect;
                                                 incode ,visualtype: integer;
                                                 const text : PWChar; factor:Integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // ���������� ������� ���� ������� �� ������ �������
 // hmap       - ������������� �������� ������
 // hdc        - �������� ����������
 // rect       - ������� �����������
 // func       - ����� �������
 // parm       - ��������� �� ��������� �������
 // colors     - ����� ������ �������
 // palette    - ��������� �� �������
 // visualtype - ��� ������������
 // text       - ����� ������ � ��������� WINDOWS; ����� ������� IMG_TEXT
 //              (��������� ������ � ���������� ��������� IMGTEXT)
 // ��������� colors; palette; visualtype; text - ����� ���� �������� :
 //  colors;palette - ���� � ���������� ������� ������� ������� �� ������������;
 //  visualtype     - ���� ����������� ����������� � ������� ������ ������������;
 //  text           - ��� ����������� ������������ ������ ("AaBbCc").
 // ��� ������ ���������� ����

function  mapPaintExampleObjectFunc(Map : HMap; DC : HDC; RECT : TRect; // 27/04/01
             func: integer; parm : GTKPChar; // 30/09/2009
             colors : integer; palette : TCOLORREF;
             visualtype : integer; text : GTKPChar) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapPaintExampleObjectFuncUn(Map : HMap; DC : HDC; RECT : TRect; // 03/04/2014
             func: integer; parm : GTKPChar; // 30/09/2009
             colors : integer; palette : TCOLORREF;
             visualtype : integer; text : PWChar) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // ���������� ������� ���� ������� �� �������� �������
 // � �������� ������ (� �������������� ������� - 100%)
 // hmap       - ������������� �������� ������;
 // hdc        - �������� ����������
 // rect       - ������� �����������
 // func       - ����� �������
 // parm       - ��������� �� ��������� �������
 // data       - ������� ����������� � ������ ���������� ������
 // colors     - ����� ������ �������
 // palette    - ��������� �� �������
 // visualtype - ��� ������������
 // text - ����� ������ � ��������� WINDOWS
 //        (���� data != 0; �� text ������������)
 // ��� ������ ���������� ����

function  mapPaintExampleObjectRealByFuncData(Map : HMap; DC : HDC; var RECT : TRect;
             Func : integer; parm  : GTKPChar;var data : TPOLYDATA; // 30/09/2009
             Colors : integer; palette : TCOLORREF;
             visualtype : integer; text : GTKPChar) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapPaintExampleObjectRealByFuncDataUn(Map : HMap; DC : HDC; var RECT : TRect;       // 03/04/2014
             Func : integer; parm  : GTKPChar;var data : TPOLYDATA;
             Colors : integer; palette : TCOLORREF;
             visualtype : integer; text : PWChar) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // �������� �� ����� �������; ��������������� �������� ��������
 // hmap   - ������������� �������� ������
 // hdc    - �������� ����������
 // rect   - ������� �����������
 // select - ������� ������ ��������
 // color  - ����; ������� ����� ���������� ������� �� �����
 // ������������ ������ ��������; ����� ���������� ������ (�������� �����)

procedure mapPaintSelect95(Map:HMap;aHdc:HDC;var rect:TRect; // 23/09/2009
                           select:HSELECT;color:COLORREF);
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ����������/��������� ������ ��������� ��������� � ��������
 // �������� �� �����
 // seltype - ������ ��������� (STF_CONTOUR - ������ �������;
 // STF_OBJECT - ���� ������)
 // ���������� ��������; ������� ���� ����� �����������

function  mapSetSelectType(seltype: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetSelectType: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

{$IFNDEF LINUXAPI}
 // ���������� �������� ����� �� �������� ���������� � ������ ����������
 // � ������� �������� � ������� ��������
 // hmap   - ������������� �������� ������
 // rect - ���������� ��������� ����� (Draw) � ����������� (Picture)
 // parm - ��������� ������; ��������� �� PRINTPARM (��. prnapi.h)

procedure mapPrint(Map : HMap; DC: HDC; var Rect : TRect;
                             const parm  : GTKPChar);  // 15/12/05 // 30/09/2009
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
{$ENDIF}


 // ���������� ��������� ����� DIB � ����             
 // millisec - �������� ������ ������������� ����������� � �������� ������
 //            � ������������� (��� ������� �������� ����������� ��������� ���� ��� �� ����������)
 // ���������� ���������� ��������

function  mapSetPaintStepEx(map : HMAP; millisec : integer) : integer; // 20130127 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� �������� ���������� ������ DIB � ����
 // ���������� �������� ������ ������������� ����������� � �������� ������
 // � ������������� (��� ������� �������� ����������� ��������� ���� ��� �� ����������)
 // ���� hmap ����� ����, �� ���������� ����

function  mapGetPaintStepEx(map : HMAP) : integer;                     // 20130127 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // ���������� ����� ������������� ����������� ��������       // 21/03/08
 // ��� �������� ���������� �������� ������� ��� ������
 // flag - ��������� �������� ������������� ����� ������������� ������
 //        (��� ���� ����� ����������� ��������� ������� �������������
 //         �������� � 2 ����)
 // ��� ������ ��������� ���������� ����� ������������� �����������
 // ���������� ���������� ��������

function  mapSetTextQuality(flag : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ����� ������������� ����������� ��������       
 // ��� �������� ���������� �������� ������� ��� ������

function  mapGetTextQuality() : integer;                               // 20130127 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  //**********************************************************
  //*                             *
  //*      ������� ����������� � ������ ����������� �����    *
  //*   ����� ������������� ����; � ������� ��������� ������ *
  //*                ����������� ����� � ����                *
  //*                             *
  //**********************************************************

 // �������� �� ����� �������; �������������� �������� �������
 // hmap   - ������������� �������� ������
 // hwnd   - ������������� ���� ������
 // point - ���������� �������� ������ ���� ���� �� �����
 // � �������������� ��������� place ������� ���������
 // select - ������� ������ ��������;
 // color  - ����; ������� ����� ���������� ������� �� �����

procedure mapViewSelect(Map:HMap;WND:HWND; var point:TDOUBLEPOINT;
                        select:HSELECT;color:COLORREF;place:integer); // 23/09/2009 // 30/09/2009 
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 


 // ���������� �������� ����� � ��������� ������� ����
 // � ������� �������� � ������� ��������
 // erase - ������� �������� ���� ����� �������;
 // (0 - ��� �� �������; !=0 - �������� �������� ������ ����);
 // point - ���������� �������� ������ ���� ���������
 // � �������������� ��������� place ������� ���������

procedure mapView(Map:HMap;WND:HWND;erase:integer;
                  var point:TDOUBLEPOINT;place:integer=cPP_MAP); // 23/09/2009 // 30/09/2009
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


// 11/12/13 {$IFDEF WIN32}
{$IFNDEF LINUXAPI}            // 11/12/13
 // ���������� �������� ����� � ��������� ������� ����        // 26/12/07
 // � ������� �������� � ������� ��������
 // hMap   - ������������� �������� ������
 // hwnd  - ������������� ����;
 // erase - ������� �������� ���� ����� �������;
 // (0 - ��� �� �������; !=0 - �������� �������� ������ ����);
 // rect - ���������� ��������� ����� (Draw)
 // � ����������� (Picture);
 // point - �������� ��������� � ������������ ����

procedure mapViewEx(Map : HMap; Wnd : HWnd; erase : integer ;
                               var Rect : TRect; var Point : TPoint);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
{$endif}  // WIN32API

 // ���������� ������������ ������ � ��������
 // ��������� ������� ����
 // hmap   - ������������� �������� ������
 // hwnd  - ������������� ����;
 // point - ���������� �������� ������ ���� ���� �� �����
 // � �������������� ��������� place ������� ���������
 // info - ������������� ������� ����� � ������
 // ��� ������ � ���������� ���������� ����

procedure mapViewObject(Map:HMap;WND:HWND; var point:TDOUBLEPOINT; // 23/09/2009
                        Obj:HObj;place:integer=cPP_MAP); // 30/09/2009
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


 // ���������� ������������ ������ ����� � �������� ��������� ���� (�����)     // 26/03/09
 // � �������� ������ ������������
 // hmap   - ������������� �������� ������
 // hwnd  - ������������� ����;
 // point - ���������� �������� ������ ���� ���� �� �����
 // � �������������� ��������� place ������� ���������
 // image - �������� ���� ������� (��. MAPGDI.H); ���� ������
 // ������ ���������� ����� �������� ������ - �������� ���������
 // ����� ���������� � ����;
 // info - ������������� ������� ����� � ������
 // ��� ������ � ���������� ���������� ����

function mapViewMapObject(Map:HMap;WND:HWND;var point:TDOUBLEPOINT;
                          var image:TPAINTPARM;Obj:HObj;place:integer=cPP_MAP):integer; // 23/09/2009 // 30/09/2009
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ���������� ������������ ������ ����� � �������� ��������� ���� (�����)     // 26/03/09
 // � �������� ������ ������������
 // hmap   - ������������� �������� ������
 // image - �������� ���� ������� (��. MAPGDI.H); ���� ������
 // ������ ���������� ����� �������� ������ - �������� ���������
 // ����� ���������� � ����;
 // info - ������������� ������� ����� � ������
 // offset - �������� ��������� ������� (� ������������ � place)
 // ��� ������ � ���������� ���������� ����

function mapViewOffsetMapObject(Map:HMap;WND:HWND;var point:TDOUBLEPOINT; // 23/09/2009
                                var image:TPAINTPARM;Obj:HObj;
                                var offset:TDOUBLEPOINT;
                                place:integer=cPP_MAP):integer; // 30/09/2009
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

  // ���������� ������������ ������ � �������� ��������� ���� (�����)
  // hmap   - ������������� �������� ������
  // hwnd  - ������������� ����;
  // point - ���������� �������� ������ ���� ���� �� �����
  // � �������������� ��������� place ������� ���������
  // image - �������� ���� ������� (��. MAPGDI.H);
  // data  - ���������� �������.
  // offset - �������� ��������� ������� (� ������������ � place)
  // ���������� ������� ������ � ������������ � ���������� place

function mapViewOffsetUserObject(Map:HMap;WND:HWND;var point:TDOUBLEPOINT; // 23/09/2009
                                 var image:TPAINTPARM;var data:TPLACEDATA;
                                 var offset:TDOUBLEPOINT;
                                 place:integer=cPP_MAP):integer; // 30/09/2009
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;



 // ���������� ������������ ������ � �������� ��������� ���� (�����)
 // hmap   - ������������� �������� ������
 // hwnd  - ������������� ����;
 // point - ���������� �������� ������ ���� ���� �� �����
 // � �������������� ��������� place ������� ���������
 // image - �������� ���� ������� (��. MAPGDI.H);
 // data  - ���������� �������.
 // ���������� ������� ������ � ������������ � ���������� place

function  mapViewUserObject(Map : HMap;Wnd : HWnd; var Point : TDoublePoint;
                                    var image : TPAINTPARM; var data : TPLACEDATA;
                                    place : integer=cPP_MAP) : integer; // 30/09/2009
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������ � ���� ����������� � �������� �����
 // hmap   - ������������� �������� ������
 // hwnd  - ������������� ����;
 // point - ���������� �������� ������ ���� ���� �� �����
 // � �������������� ��������� place ������� ���������
 // linethick - ������� ����� ������� � �������� (������������� 1 - 2)
 // x;y   - ���������� ������ ������� (������������ place)

 procedure mapViewMarker(Map : HMap; Wnd : HWnd;var Point : TDoublePoint ;
                                  linethick : integer; x,y:double; place: integer);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ����� � ���� ��������� ���������            // 21/04/09
 // hmap   - ������������� �������� ������
 // hwnd  - ������������� ����;
 // point - ���������� �������� ������ ���� ���� �� �����
 // � �������������� ��������� place ������� ���������
 // image - �������� ���� ������� (��. MAPGDI.H);
 // first - ���������� ������ �����;
 // second - ���������� ������ �����.

 procedure mapViewUserLine(Map : HMap; Wnd : HWnd; Point : PDoublePoint ;
                                      image : PPAINTPARM; first, second: PDoublePoint;
                                      place: integer);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 //**********************************************************
 //*                             *
 //*        ������� ���������� ��������� �����������        *
 //*                             *
 //**********************************************************


  // ���������� ����� �������, ������� ����� ������������         
  // ���������� ��� ���������� ����������� ����� � ������
  // ��� � �������� �������� �����������
  // ������������� ����� ������������� ����� ������ ������� Paint,
  // � �� ��������� ��������� ����� - ���������
  // (������������� ������� �����).
  // ���������� ������� �� ������ ���� �������� Paint !
  // call - ����� ���������� ������� (��. maptype.h),
  // parm - ��������, ������� ����� ������� ���������� �������.
  // ����� ��������� parm ���������� ������� ���������� �������
  // ����������� ����� (0/1) ��� �� ���������� ���������� �� ������
  // ���� ���������� ������� ������ ��������� ��������, ��
  // ������� ����������� ����� ������� (��������, ��� �������
  // ������� ��������� ��������� �������� ������ ���� �
  // ���������� ������� Esc, ������� ���������� 1, �����������
  // �����������).

procedure mapSetBreakCallAndParmEx(Map : HMAP; calladdr : TBREAKCALLEX; parm : integer); // 20130127 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ����� �������; ������� ����� ����������
  // ����� ������������� ����������� ����� � �������
  // ������ ��� ����� ������� ����������� ����� �� �����.
  // ���������� ������� �� ������ ���� �������� Paint !
  // hmap   - ������������� �������� ������
  // call - ����� ���������� ������� (��. maptype.h);
  // parm - ��������; ������� ����� ������� ���������� �������.
  // � ���������� ������� ����� �������� "��� ������"
  // � ����������� ����������� ������� ������� ��� �������
  // ���� PaintUserObject.

procedure mapSetBeforePaintCallAndParm(Map : HMap; call : TBeforePaint;
                      parm : integer );
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ���������� ����� �������, ������� ����� ������������
  // ���������� ��� ���������� ����������� ����� � ������
  // ��� �� �����. 
  // ���������� �������. �������� ����� ����������� ������ �����
  // ��������� ���������� ����� ��������, ��������� ��������
  // mapSetPaintStep, ��� ������ ������ (������, �������).
  // ��������� �������� ����������� �����,
  // ������� ��������� ����������� (�������, ������� �����...)
  // � ��������� ����� ������� �����������. �������������
  // ����� ������������� ����� ������ ������� Paint,
  // � �� ��������� ��������� ����� - ���������
  // (������������� ������� �����).
  // ���������� ������� �� ������ ���� �������� Paint !
  // hmap   - ������������� �������� ������
  // call - ����� ���������� ������� (��. maptype.h),
  // parm - ��������, ������� ����� ������� ���������� �������.
  // ���� ���������� ������� ������ ��������� ��������, ��
  // ������� ����������� ����� ������� (��������, ��� �������
  // ������� ��������� ��������� �������� ������ ���� �
  // ���������� ������� Esc, ������� ���������� 1, �����������
  // �����������).

procedure mapSetBreakCallAndParm(Map : HMap; call : TBeforePaint;
                      parm : Pointer);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  //*********************************************************
  //*                             *
  //*     ������� ��������� ����������� ����������� �����    *
  //*                     � ���������                        *
  //*                             *
  //*********************************************************/

 // ������� ��������� ����������� ����� � ���������
 //  hmap     - ������������� �������� ������
 //  angle    - ���� �������� ����� � ����� � �������� �
 //             ���-�������� ���� ����� (�� -Pi �� Pi)
 //  fixation - ���� ������� �������� �������� ����������� �����
 //             ������������ ����������� ��������� (�� 0 �� Pi/6).
 //             �� ��������� = Pi/18 (10 ��������)
 // ���� fixation ������������ ��� ����������� �������� ����������� 
 // ��� �������� �� ���������� ����� �� ������ (��� ����� �� ������), 
 // ����� ��� ���������������� ������ ������� �������� ������� 
 // �������� ���� �������� (angle). � ������, ���� �������� ����� 
 // ������� ����� �������� � ��������� ����� ������ fixation, 
 // �� ����� ���� �������� �� ���������������.
 // ���������� �������� �������������� ���� ��������
 // ��� ������ ���������� 0

function mapSetupTurn(aMap:HMap;aAngle,aFixation:double):double; // 23/09/2009
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ������� �� ������� ?
 // hmap - ������������� �������� ������
 // ���������� (1 - �������; 0 - ���)

function  mapTurnIsActive(aMap : HMap) : integer; // 23/09/2009
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ���� ��������
 // hmap - ������������� �������� ������
 // ���������� �������� �� -Pi �� Pi

function mapGetTurnAngle(aMap:HMap):double;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}; 
  external sGisAcces;

  //*********************************************************
  //*                             *
  //*    ������� ���������� ������������ ����������� �����   *
  //*                             *
  //*    ��� ��������� �������� ����������� �������������    *
  //*       ���������� �������� ����������� � �������� !     *
  //*                             *
  //*********************************************************/

  // ���������� ������� ����������� (����������� ��������)
  //  ���������:
  //          hmap - ������������� �������� ������
  //          x; y - ���������� ��������������� "������ �����������"
  //                 (����� ����� ��������) � ���� � ������� ��������
  //         scale - �������� ������� �����������; ������� ������ ��������
  //  ����������:   0 - ������� �� ���������;  1 - ������� ���������
  //          x; y - ���������� ��������������� "������ �����������"
  //                 � ���� ������������ ���� ��������
  //                 � ����� �������� �����������

function mapSetViewScale(Map:HMap;var X:integer;var Y:integer;
                         scale:single):integer; // 30/09/2009
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

  // �������� ������� ����������� ������������ ��������
  //  ���������:
  //          hmap - ������������� �������� ������
  //          x; y - ���������� ��������������� "������ �����������"
  //                 (����� ����� ��������) � ���� � ������� ��������
  //        change - ����������� ��������� �������� �������� �����������
  //                 0 < change < 1 ; ��� ������
  //                 1 < change < N ; ��� ����������
  //  ����������:   0 - ������� �� ���������;  1 - ������� ���������
  //          x; y - ���������� ��������������� "������ �����������"
  //                 � ���� ������������ ���� ��������
  //                 � ����� �������� �����������

function mapChangeViewScale(Map:HMap;var X:integer;var Y:integer;
                            change:single):integer; // 30/09/2009
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

  // ��������� ����������� ������� ����������� �����
  // hmap - ������������� �������� ������
  // ���������� �������� ����������� ��������

function  mapGetShowScale(Map : HMap) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������ ������� ����������� �����
  // hmap - ������������� �������� ������
  // ���������� �������� ����������� ��������

function  mapGetRealShowScale(Map : HMap):double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ������ ������� ����������� �����          // 09/08/07
  // ���������� �������� ����������� ��������

function  mapSetRealShowScale(Map : HMap; scale : double) :double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������� ����������� ��������������� �����
  // ��������: 5 - ��������� � 5 ��� ������������ �������� ��������;
  //         0.1 - ����� � 10 ���.

function  mapGetDrawScale(Map : HMap) :double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� "�����������" �������; ��������� � ��������� (scale)

function  mapScaleToRoundScale(scale : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� "�����������" �������, ��������� � ��������� (scale)
  // � ������ ������� ����� (��� �������� ������ WMST ����� ����
  // ������ ����������� ��������)
  // hmap - ������������� �������� ������
  // ���������� ����� �������� ����������� ��������

function  mapScaleToRoundScaleEx(map : HMAP; scale : double) : integer;  // 20130127 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces; 

// ��������� ���������� "�����������" ���������        // 24/03/08    

function  mapScaleTableCount: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������� ������� "�����������" ��������� �� ����������� // 24/03/08
 // ������ � �������(� 1)

function  mapScaleTableItem(number: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ���������� ������ ��������������� �������� ����� ��� �����������
  // method - ������ ���������������
  // (0 - ���������������� "� ������������� ����������";
  //  1 - ���������)
  // ���������� ����� ������������� ��������

function  mapSetScaleMethod(method : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapGetScaleMethod: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ����������� ����� �������� ������ �����          // 26/06/09
  // hmap - ������������� �������� ������
  // ��� ������ ���������� ����

function mapSetRegion(aMap : HMAP): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������ ������������ ��������
  // hmap - ������������� �������� ������
  // select - ������������� ��������� ������/�����������
  // � ������� ����� �������� ������� ������� �����������
  // ��. mapCreateMapSelectContext(...)
  // ��� ������ ���������� ����

function  mapGetViewSelect(Map : HMap;Select : HSelect) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ������ ������������ ��������
  // hmap - ������������� �������� ������
  // select - ������������� ���������������� ��������� ������/�����������

procedure mapSetViewSelect(Map : HMap;Select : HSelect);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ���������/���������� ���� ���� ������������ �����
  // hmap - ������������� �������� ������
  // color - ���� ����
  // ��� ��������� ����� ���� ���������� ������������ ���� �����
  // ��� ������ ���������� 0x0FFFFFF (�����)

function  mapGetBackColor(Map : HMap): ColorRef;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapSetBackColor(Map : HMap;Color : ColorRef): ColorRef;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������/���������� ���� ���� ������������ �����           
  // ��� ������ � ���������� ������ �� �����
  // ��� ��������� ����� ���� ���������� ������������ ���� �����
  // ��� ������ ���������� 0x0FFFFFF (�����)

function  mapGetBackPrintColor(map : HMAP) : COLORREF;                 // 20130127 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapSetBackPrintColor(map : HMAP; color : COLORREF) : COLORREF;  // 20130127 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������/���������� ������ ����������� �����
  // hmap - ������������� �������� ������
  // ��� ��������� ������ ������� ����������� ������������
  // ���������� ��������
  // (��. Maptype.h : VT_SCREEN(1); VT_PRINT(3); VT_PRINTRST(6);...)
  // ��� ������ ���������� ����

function  mapGetViewType(Map : HMap) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapSetViewType(Map:HMap;Typ:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

  // ��������� ������� ����� (�� -16 �� +16)
  // hmap - ������������� �������� ������

function  mapGetBright(Map : HMap) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ������� ����� (�� -16 �� +16)
  // hmap - ������������� �������� ������

function  mapSetBright(Map : HMap; bright: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������������� (�� -16 �� +16)
  // hmap - ������������� �������� ������

function  mapGetContrast(Map : HMap) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ������������� (�� -16 �� +16)
  // hmap - ������������� �������� ������

function  mapSetContrast(Map : HMap; contrast : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������������� ������� ��������� ���       // 21/07/06
  // ����������� ����������� (�� 0 �� 100)
  // hmap - ������������� �������� ������

function  mapGetIntensity(Map : HMap) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ������������� ������� ��������� ���
  // ����������� ����������� (�� 0 �� 100)
  // hmap - ������������� �������� ������

function  mapSetIntensity(Map : HMap; intensity : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������ ��� ����������� �����          
  // 1 - � ������, 0 - ��� �����

function mapGetNodeView(aMap : HMAP) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ����������� ����� �� �����
  // 0 - �� ����������, 1 - ����������

procedure mapSetNodeView(aMap : HMAP; mode : integer);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  //**********************************************************
  //*                             *
  //*          ������������ ������ ���� �����                *
  //*                             *
  //**********************************************************

  // ������� ����� ����� � ������ ��� ���������� �������
  // ������������ �� ����� ��������
  // width  - ������ ���������� ����� ���� ����� � ������;
  // height - ������ ���������� ����� ���� ����� � ������.
  // ��������� ������ ����� ������; ������ ��������� ��� ������ ������
  // ������� ����������� ������� � ����� (Draw) -  ��� ������������
  // ���������� ������� ��� ����������� ����� � ��� ������������ ��������
  // ������ ������ =  (width * height * 4)
  // ����� ����� ���� ����������� ������� �� 256 ������� ������� ������������
  // ����� ����������� � ���� � �������� mapChangeImageSizeEx
  // ��� �������� ���������� ���������� ������������� ������ ������
  // ��� ������ ���������� ����

function  mapCreateImageEx(width, height: integer): HImage;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // �������� ������ ����
  // hwnd - ������������� ����; ��� ����������� �������� ���������� �������
  // ����� ����������� � ���� � �������� mapChangeImageSize
  // ��� ������ ���������� ����

function  mapCreateImage(Wnd : HWnd) : HImage;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ����� ����
  // himage - ������������� ������ ����

procedure mapCloseImage(image : HIMAGE);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� �������� ������ ����
  // himage - ������������� ������ ����
  // ��� ������ ���������� ����

function  mapGetImageDC(image : HIMAGE):HDC;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� �������� ������ ���� ��� ����������� ��������
  // himage - ������������� ������ ����
  // ��� ������ ���������� ����

function  mapGetImageObjectDC(image : HImage) :HDC;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ���������� ������ ���� � �������� ��������
  // himage - ������������� ������ ����
  // hdc - �������� ������� ����������� (����);
  // rect - ���������� ������� ����������� � ������ � ���������
  // ��� ������ ���������� ����

function  mapViewImage(image : HImage; DC : HDC; var Rect : TRect) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ���������� ������ ���� � �������� ��������       // 19/12/07
  // himage - ������������� ������ ����
  // hwnd - ������������� ����; � ������� ��������� �����������;
  // rect - ���������� ������� ����������� � ������ � ���������
  // ��� ������ ���������� ����

function  mapViewImageEx(image : HImage; Wnd : HWnd; var Rect : TRect) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ���������� ������ ���� � �������� �������� �� ���������    // 28/07/08
  // himage - ������������� ������ ����
  // hwnd - ������������� ����; � ������� ��������� �����������;
  // rect - ���������� ������� ����������� � ������ � ���������
  // offset - �������� ����������� � ����
  // ��� ������ ���������� ����

function  mapViewImageExOffset(image : HImage; Wnd : HWnd; var Rect : TRect;var offset : TPoint) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // �������� ����������� ����� �� ��������������� ������
  // himage - ������������� ������ ����
  // offset - �������� ����������� � ����
  // ��� ������ ���������� ����

function  mapViewImageOffset(image : HImage; var offset : TPoint) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // �������� ������� ������ ����
  // himage - ������������� ������ ����
  // erase  - ������� ������� ����; ���� ����� 0 - ���������� �����������
  // width  - ����� ������ ������
  // height - ����� ������ ������ 
  // ��� ������ ���������� ����

function  mapChangeImageSizeEx(image : HImage; erase, width, height: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // �������� ������� ������ ����
  // himage - ������������� ������ ����
  // ����������� ��������� ������; ���� ����� ������ ����� mapCreateImage(Wnd : HWnd);
  // ���������� ������ �������� !
  // ��� ������ ���������� ����

function  mapChangeImageSize(image : HImage) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������ ����                         // 26/12/07
  // himage - ������������� ������ ����
  // dx     - �������� �������� ���� �� ����������� (> 0 - ����� �������;
  //                      < 0 - ������ ������)
  // dy     - �������� �������� ���� �� ���������   (> 0 - ������ ����;
  //                      < 0 - ����� �����)
  // onlymap - ������� ���������� ������ ������ ����� (����������� �������� ���������
  //           ��� ���������� ������ mapClearImageObjects)
  // ��� ������ ���������� ����

function  mapScrollImageEx(image : HImage; dx,dy,onlymap : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapScrollImage(image : HImage; dx,dy : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // �������� ����������� ��������� ��������� ����� � ������ ������
  // ����� ���������� ����� ����������� ������������ �������� ���������
  // � �������� ��������� ��������� (�� ��� �������� �������� �������
  // ����� ����� ���������� ������� mapClearImageObjects).
  // himage  - ������������� ������ ����
  // onlymap - ������� ���������� ������ ������ ����� (����������� �������� ���������
  //           ��� ���������� ������ mapClearImageObjects)
  // ��� ������ ���������� ����

function  mapDrawImageMapEx(Image : HImage; Map : HMap; var Rect : TRect;
             var Position : TPoint; onlymap:integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


function  mapDrawImageMap(image : HImage; Map : HMap; var Rect : TRect;
                             var Position : TPoint): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // �������� ����������� ��������� ��������� ����� � ������ ������      // 29/05/09
  // ����� ���������� ����� ����������� ������������ �������� ���������
  // � �������� ���������; ��������� ���������� �������
  // himage  - ������������� ������ ����
  // obj     - ������������� �������; � �������� ��������� �������� ����� ��������� �����
  // onlymap - ������� ���������� ������ ������ ����� (����������� �������� ���������
  //           ��� ���������� ������ mapClearImageObjects)
  // ��� ������ ���������� ����

function  mapReDrawImageMapExUnderObject(Image : HImage; Map : HMap;
              Obj : HOBJ; onlymap:integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������ ������ ����� ��������� � ������ ������
  // himage  - ������������� ������ ����
  // hMap - ������������� �������� ������
  // parm - ��������� ����������� ������� ����� � ������ ������ (������ �����)
  // object - ������������� �������� ������� � ������
  // ��� ������ ���������� ����

function  mapDrawImageMapObject(image : HImage; Map : HMap;
                                  var parm : TPAINTPARM; Obj : HOBJ) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ������ ������ ����� ��������� � ������ ������ c ������
  // ��������� ������ �������
  // himage  - ������������� ������ ����
  // hMap - ������������� �������� ������
  // offset - �������� �������� ����������� ������� � ������ �� ��� ��������
  //          ��������� � ������ � ������� ������� ��������� ���������
  // parm - ��������� ����������� ������� ����� � ������ ������ (������ �����)
  // object - ������������� �������� ������� � ������
  // ��� ������ ���������� ����

function  mapDrawImageOffsetMapObject(image : HImage; Map : HMap; var offset : TDoublePoint ;
                                      var parm : TPAINTPARM; Obj : HOBJ) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ������ ������ ����� ��������� � ������ ������
  // himage  - ������������� ������ ����
  // hMap - ������������� �������� ������
  // parm - ��������� ����������� ������� ����� � ������ ������ (������ �����)
  // data - ������ ��������� ������� � ������� ���������, �������� ���������� place
  // place - ��� ������� ��������� (� ������ ������ - PP_PICTURE, � ������ �
  //         ������� ��������� ��������� - PP_PLANE, � �������� �� ����������
  //         ��������� - PP_GEO)
  // ��� ������ ���������� ����

function  mapDrawImageUserObject(image : HImage; Map : HMap;
                                  var parm : TPAINTPARM;
                                  var data : TPLACEDATA;
                                  place: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ������ ������ ����� ��������� � ������ ������ c ������
  // ��������� ������ �������
  // himage  - ������������� ������ ����
  // hMap - ������������� �������� ������
  // offset - �������� �������� ����������� ������� � ������ �� ��� ��������
  //          ��������� � ������� ���������, �������� ���������� place
  // parm - ��������� ����������� ������� ����� � ������ ������ (������ �����)
  // data - ������ ��������� ������� � ������� ���������, �������� ���������� place
  // place - ��� ������� ��������� (� ������ ������ - PP_PICTURE, � ������ �
  //         ������� ��������� ��������� - PP_PLANE, � �������� �� ����������
  //         ��������� - PP_GEO)
  // ��� ������ ���������� ����

function  mapDrawImageOffsetUserObject(image : HImage; Map : HMap; var offset : TDoublePoint;
                                       var parm : TPAINTPARM;  var data : TPLACEDATA;
                                       place : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ����������� ������                    // 11/07/08
  // hScreen  - ������������� ������ ������;
  // points   - ���������� � ��������
  // count    - ����� ���������
  // image    - ��� ������������ ��������� (��. mapgdi.h)
  // parm     - ��������� ������������ ���������

function  mapDrawImageGraphics(image : HImage; Map : HMap;
                               var points  : TDRAWPOINT; count : integer;
                               datatype : integer; parm : GTKPCHAR) : integer; // 30/09/2009
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ��������� ������ (Arial)
  // image    - ������������� ������ ������;
  // points   - ���������� � ��������
  // count    - ����� ���������
  // text     - ����� �������
  // height   - ������ ������� � ���
  // color    - ���� ������� RGB
  // align    - ������ ������������ ������ (��� -1 - FA_BASELINE|FA_LEFT)
  // ��� ������ ���������� ����

function  mapDrawImageText(image : HImage; Map : HMap; var points : TDRAWPOINT; count : integer;
 	const text : GTKPChar; Height, color, align : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapDrawImageTextUn(image : HImage; Map : HMap; var points : TDRAWPOINT; count : integer;
 	const text : PWChar; Height, color, align : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� BMP � ����� ������              
  // image     - ������������� ������ ������;
  // point     - ���������� � ��������;
  // bmpmemory - ����� ������� ����; ����������� ����� BMP-�����
  // ��� ������ ���������� ����

function  mapDrawImageBitMap(image : HImage; Map : HMap; var points : TDRAWPOINT;
                              const bmpmemory : GTKPCHAR; transparent: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ����������� �������� �� �������� ��������� ������
  // ������ (������������ ����� ���� ����� � ��������� ������)
  // ��� ������ ���������� ����

procedure mapClearImageObjects(image : HImage; var Rect : TRect);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


// 11/12/13 {$IFDEF WIN32}
{$IFNDEF LINUXAPI}            // 11/12/13
  // ������� BITMAP � ������� ������� CreateDIBSection
  // ������ �������� - 4 �����
  // hdc    - ������������ ��������
  // width  - ������ �����������
  // height - ������ �����������
  // memory - ����� ����������� � ������
  // ��� ������ ���������� ����

function  mapCreateBitmap(DC : HDC; width, height: integer; memory : Pointer): HBitMap;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapCreateBitmapEx(width, height: integer) : HBitMap;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� BITMAP; ��������� mapCreateBitmap

procedure mapCloseBitmap(Bitmap : HBitmap);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ����� � Bitmap
  // ������ ����������� �� ������ �������� ������
  // ��� ���������� �������� ��������������� �����������
  // (�� �������� ��������) ����� ������� mapSetScreenImageSize()
  // erase - ������� �������� ���� ����� �������;
  //        (0 - ��� �� �������; !=0 - �������� �������� ������ ����;
  //        ��� ��������� ������� ������ (VT_SCREEN) ������ �������
  //        ������ ����; ����� �������� -2 (����� 2))
  // rect - ���������� ��������� ����� (Draw) � ����������� (Picture)
  // ��� ������ � ���������� ���������� 0

function  mapPaintToBitmap(Map : HMap; Bitmap : HBitmap; erase : integer;
                                   var Rect : TRect) : integer;  // 15/10/07
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ����������� � ��������� ��������� � ���� BMP
 // ������ ������� ����������� ������ ��� ��������� Windows !
 // hdc     - �������� ����������
 // rect    - ������ ����������� �������
 // bmpname - ��� ������������ ����� BMP, 32 ���� �� ������
 // ��� ������ � ���������� ���������� ����

function  mapSaveImageToBMP(dc : HDC; var Rect : TRect; const bmpname : GTKPCHAR) : integer;  // 20130127 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapSaveImageToBMPUn(dc : HDC; var Rect : TRect; const bmpname : PWCHAR) : integer;  // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������� ����������� �� ����� BMP                             
 // rect    - ������ ��������� �������
 // bmpname - ��� ������������ ����� BMP, 32 ���� �� ������
 // ��� ������ � ���������� ���������� ����

function  mapLoadFromBMP(var Rect : TRect; const bmpname : GTKPCHAR) : HBitmap;               // 20130127 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapLoadFromBMPUn(var Rect : TRect; const bmpname : PWCHAR) : HBitmap;             // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


{$endif}  // LINUXAPI   // 11/12/13

  {*********************************************************
  *                                                        *
  *          ����������� ����� �� ����������� �����        *
  *                                                        *
  *********************************************************}

  // ��������� ������� ����������� �����
  // hmap - ������������� �������� ������
  // ���� ����������� ����� �������� - ���������� ��������� ��������
  // 1 - ����������� ��� ������, -1 - ����������� ��� ������
  // ���� ����� �� ������������ - ���������� ����

function mapIsGridActive(aMap : HMAP) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ������� ����������� �����
  // hmap - ������������� �������� ������
  // 1 - ����������� ��� ������, -1 - ����������� ��� ������
  // 0 - �� ���������� �����
  // ���� ����� �� ������������ - ���������� ����

function mapSetGridActive(aMap : HMAP; active : integer) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������� ��� �����
  // hmap - ������������� �������� ������
  // ������� �������� ����� ���������� � ������ ��������
  // ����������� ����������� ���������
  // (��������� ������ ������ 8 ��������, ������ �������������)

function mapGetGridStep(aMap : HMAP) : double;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ��������� �����
  // hmap - ������������� �������� ������
  // parm - ��������� �� ������� ���������� ����������
  // ��� ������ ���������� ����

function mapGetGridParm(aMap : HMAP; parm : PGRIDPARM) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapGetGridParmEx(aMap : HMAP; parm : PGRIDPARM) : integer;          // 04/03/2014
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ���������� ��������� �����
  // hmap - ������������� �������� ������
  // parm - ����� �������� ���������� ����������� �����
  // ��� ������ ���������� ����

function mapSetGridParm(aMap : HMAP; parm : PGRIDPARM) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapSetGridParmEx(aMap : HMAP; parm : PGRIDPARM) : integer;           // 04/03/2014
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  //**********************************************************
  //*                             *
  //*       ������� ������� ���������� � ��������� �����     *
  //*                             *
  //**********************************************************

  // ��������� ������� ������� �����
  // hmap - ������������� �������� ������
  // ��� ������ ���������� ����

function  mapGetMapScale(Map : HMap) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� �������� �����
  // hmap - ������������� �������� ������
  // name - ����� ������ ��� ���������� �������
  // size - ������ ������
  // ��� ������ ���������� ������ ������

function  mapGetMapName(Map : HMap) : GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapGetMapNameEx(Map : HMap; name : GTKPChar; size: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� �������� ����� � ������� UNICODE              // 07/12/06
  // name - ������ � ��������� UNICODE (2 ����� �� ������)
  // size - ������ ������ � ������
  // ��� ������ ���������� ������ ������

function  mapGetMapNameUn(Map : HMap; name : PWCHAR; size : integer) : integer; // 30/09/2009
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������ ���� � �������� ������� �����
  // (������� �������� mapGetMainMapName)
  // hmap - ������������� �������� ������
  // ��� ������ ���������� ������ ������

function  mapGetMapPath(Map : HMap) : GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ��� �����
  // hmap - ������������� �������� ������
  // ���� ���� - ��. mapcreat.h; MAPTYPE
  // ��� ������ ���������� ����

function  mapGetMapType(Map : HMap) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� - �������� �� ����� ����������������
  // ���������������� ����� ������� �� ������ ����� ����������
  // �������� ��� �����
  // � ��������� MAPREGISTEREX ���������������� ����� � ����
  // FlagRealPlace ����� ��������� ��������
  // ���� ����� ���������������� - ������������ ��������� ��������

function  mapIsMapSite(Map : HMap) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������������� ���������� ��������� �������� ��������� �����
  // � ������ (������� ��������� PLANE)
  // X - ����� �����, Y - ����� �������
  // �.1 - ������ ����� ����,
  // �.2 - ������� ������
  // hmap - ������������� �������� ������
  // ��� ������ ���������� ���� (��������, ���� ������ ������ �����)

function mapGetMapX1(Map:HMap):double;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function mapGetMapY1(Map:HMap):double;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function mapGetMapX2(Map:HMap):double;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function mapGetMapY2(Map:HMap):double;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

  // ��������� ���������� ������ ��������� �����
  // ��������� MAPREGISTER � LISTREGISTER ������� � mapcreat.h
  // hmap - ������������� �������� ������
  // sheetnumber - ����� ����� ����� ���
  // �������� ������������� ���������� ������
  // ��� ������ ���������� ����

function  mapGetMapInfo(Map : HMap; sheetnumber: integer; 
  	var MapReg : TMAPREGISTER;
                         var Sheet:  TLISTREGISTER) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapGetMapInfoEx(Map : HMap; sheetnumber: integer;   //15/09/03
                           var MapReg : TMAPREGISTEREX;
                           var Sheet:  TLISTREGISTER) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ���������� ������ ��������� �����
  // �� ����� ����� - �������� ����� (MAP;SIT) (name)
  // ��������� MAPREGISTER � LISTREGISTER ������� � mapcreat.h
  // sheetnumber - ����� ����� ����� ���
  // �������� ������������� ���������� ������
  // ��� ������ ���������� ����, 
  // ����� - ����� �������� �� ����� (���� �� ����� ��� ��������, ���������� -1)  // 09/05/10

function  mapGetMapInfoByName(const name : GTKPChar; sheetnumber: integer;
  	var MapReg : TMAPREGISTER;
                         var Sheet:  TLISTREGISTER) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetMapInfoByNameUn(const name : PWChar; sheetnumber: integer;    // 03/04/2014
  	var MapReg : TMAPREGISTER;
                         var Sheet:  TLISTREGISTER) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetMapInfoByNameEx(const name : GTKPChar; sheetnumber: integer;   //15/09/03
                           var MapReg : TMAPREGISTEREX;
                           var Sheet:  TLISTREGISTER) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetMapInfoByNameExUn(const name : PWChar; sheetnumber: integer;   // 03/04/2014
                           var MapReg : TMAPREGISTEREX;
                           var Sheet:  TLISTREGISTER) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ��� ������������� ����� �� ����� �����   21/02/06
  // name    - ��� ����� �������� ����� (MAP;SIT)
  // rscname - ��� ����� RSC
  // size    - ������ ������ ��� ���������� ����� RSC
  // ��� ������ ���������� ����

function  mapGetRscByName(const name, rscname : GTKPChar; size: integer) : integer; //30/09/2009
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetRscByNameUn(const name, rscname : PWChar; size: integer) : integer; // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // �������� ������������ �����
  // nomenclature - ������ � �������������
  // length - ����� ������
  // mtype - ��� ����� (�� MAPTYPE)
  // scale - �������  (1000000;500000;200000 � �.�.
  //                   ��������������� ���� �����)

function  mapCheckNomenclature(const nomenclature : GTKPChar; length, mtype, scale : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapCheckNomenclatureUn(const nomenclature : PWChar; mtype, scale : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������������ ����� ����� �� ������������ (������� �����; �������; -)
  // filename - ����� ��� ��� �����
  // filelength - ����� ������ ��� ��� �����
  // nomenclature - ������������ �����
  // ��� ������ ���������� 0
  // 04/09/02
function  mapSetFileNameFromNomenclature(filename : GTKPChar; filelength : integer;
              nomenclature : GTKPChar) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ������������ ����� ����� �� ������������ (������� �����, �������, -)
  // filename - ����� ��� ����� �����
  // namesize - ������ ������ � ������
  // nomenclature - ������������ �����
  // ��� ������ ���������� 0

function  mapSetFileNameFromNomenclatureUn(filename : PWChar; namesize : integer;
              nomenclature : PWChar) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������ ������ �� ���� ��������������� �����
  // ������������� �������� ������-�������
  // hmap - ������������� �������� ������
  // ��������� MAPREGISTER � LISTREGISTER ������� � mapcreat.h
  // ������� ������   ���������  ���� � mapreg:
  //                - ��� �����
  //                - �������
  //                - �������� ��������� ���������
  //                  ���������  ���� � sheet:
  //                - ������������
  // �������� ������  ���������  ���� � mapreg:
  //                 - ������ �������� (��� �������� �����( hMap = 0))
  //                  ���������  ���� � sheet:
  //                 - ������������� ����������;
  //                 - ������������� ����������;
  //                 - ��������� ����������
  // ���� mapreg � sheet ����������� ��� �������� ����� (����� ������ ����)
  //  - hmap = 0
  // ���� mapreg ����������� ��� ���������� ����� � �����
  //  - hmap != 0

function  mapCalcTopographicSheet(Map : HMap; var MapReg : TMAPREGISTER;
                         var Sheet:  TLISTREGISTER) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapCalcTopographicSheetEx(aMap : HMAP; mapreg : PMAPREGISTEREX;
                                   sheet : PLISTREGISTER) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ������� ���������� �������� �������� ���������
  // ���������� ����� �������� ����� (Map) ��� ����� (Plane)
  // ��� ��������� ������������ �������� �������� ���������
  // hmap -  ������������� �������� ������
  // ����� ������� ����� ������; �������� �������� - ������������
  // ���������� �������� �������� ������� �� �������� - ��� 100 000 - ��
  // ���� 0;5 �����; ��� 10 000 - �� ���� 0;05 �����
  // ��� ���������������� �����; ����� � ����� ���������
  // ��������� 5 000 � ������� - ������ ���������� ��������
  // ������� �������� ��������������� ��� �������� ����� � ���� DeviceCapability
  // ��������� MAPREGISTEREX ������������� ��������� (��. mapcreat.h);
  // � ���� ������ ����� mapSetMapPrecision �� �����

procedure mapSetMapPrecision(Map : HMap);              // 26/12/06
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������� ���������� �������� �������� ���������
  // hmap -  ������������� �������� ������
  // ���������� ��������:
  // 1 - ������������ �������� ��������;
  // 2 - � ��������� 2 ����� (����������);
  // 3 - � ��������� 3 ����� (����������)
  // ��� ������ ��� ���������� �������� �������� ��������� ���������� ����

function  mapGetMapPrecision(Map : HMap) : integer;              // 26/12/06
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

{  /*********************************************************
  *                                                        *
  *     ������� ������� ������������� ���������� �����     *         // 27/04/10
  *                                                        *
  *********************************************************/
 }

// ��������� ���������� ����������� � ������
function mapGetEllipsoidCount : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� �������� ���������� �� ����
  // ���������  code  - ��� ����������
  //            name  - ����� ������ ��� ���������� �������� ����������
  //            size  - ����� ���������� ������� ��� ������ � ������
  //
  // ��� ������ ���������� 0
  // name �������� �������� "�� �����������"                       // 19/05/10

function mapGetEllipsoidNameByCode(code : integer; name : GTKPCHAR;
                                   size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� �������� ���������� � ��������� UNICODE �� ����
  // ���������  code  - ��� ����������
  //            name  - ����� ������ ��� ���������� �������� ����������
  //            size  - ����� ���������� ������� ��� ������    � ������
  //
  // ��� ������ ���������� 0
  // name �������� �������� "�� �����������"                       // 19/05/10

function mapGetEllipsoidNameByCodeUn(code : integer; name: PWCHAR;
                                     size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������ ���� � �������� ���������� �� ������ � �������
  // ���������  number  - ����� ������ ������� ����������� (����� ���������� � 1)
  //            code    - ��� ����������
  //            name    - ����� ������ ��� ���������� �������� ����������
  //            size    - ����� ���������� ������� ��� ������ � ������
  // ��� ������ ���������� 0

function mapGetEllipsoidByNumber(number : integer; var code  : integer;
                                 name : GTKPCHAR; size: integer) :integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapGetEllipsoidByNumberUn(number : integer; var code  : integer;     // 03/04/2014
                                 name : PWCHAR; size: integer) :integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

//_MAPIMP long int _MAPAPI mapGetEllipsoidByNumber(long int number,long int &code,
//                                                 char* name,long int size);

 // ��������� �� ���� EPSG ����� ���������� (��. MAPCREAT.H)     // 28/03/11
 // ��� ���������� ��������� MAPREGISTEREX
 // code - ��� EPSG
 // ��� ������ ���������� ����

function mapGetEllipsoidByEPSGCode(code : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ��� EPSG ���������� �� ��� ���� (��. MAPCREAT.H)     // 16/02/11
 // ellipsoid - ����� ���������� (��. MAPCREAT.H, ELLIPSOIDKIND)
 // ��� ������ ���������� ����

function mapGetEllipsoidEPSGCode(code : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������ ���������� ����� ���� � ������
  
function mapGetMapTypeCount : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������ �������� ���� ����� �� ����
  // ���������  code  - ��� ���� �����
  //            name  - ����� ������ ��� ���������� �������� ���� �����
  //            size  - ����� ���������� ������� ��� ������  � ������
  //                    (������������ ����� ������ 256 ����)
  // ��� ������ ���������� 0
  // name �������� �������� "�� �����������"                       // 19/05/10

function mapGetMapTypeByCode(code : integer; name : GTKPCHAR;
                               size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������ �������� ���� ����� � ��������� UNICODE �� ����
  // ���������  code  - ��� ���� �����
  //            name  - ����� ������ ��� ���������� �������� ���� �����
  //            size  - ����� ���������� ������� ��� ������ � ������
  //                    (������������ ����� ������ 256 ������)
  // ��� ������ ���������� 0
  // name �������� �������� "�� �����������"                       // 19/05/10

function mapGetMapTypeByCodeUn(code : integer; name : PWCHAR;
                               size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������ ���� � �������� ���� ����� �� ������ � �������
  // ���������  number  - ����� ������ ������� ���� ����� (����� ���������� � 1)
  //            code    - ��� ���� �����
  //            name    - ����� ������ ��� ���������� �������� ���� �����
  //            size    - ����� ���������� ������� ��� ������ � ������
  // ��� ������ ���������� 0

function mapGetMapTypeByNumber(number : integer; var code : integer;
                               name : GTKPCHAR; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


function mapGetMapTypeByNumberUn(number : integer; var code : integer;
                               name : PWCHAR; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������ ���������� �������� � ������

function mapGetProjectionCount : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������ �������� �������� �� ����
  // ���������  code  - ��� ��������
  //            name  - ����� ������ ��� ���������� �������� ��������
  //            size  - ����� ���������� ������� ��� ������ � ������
  // ��� ������ ���������� 0
  // name �������� �������� "�� �����������"                       // 19/05/10

function mapGetProjectionNameByCode(code : integer; name : GTKPCHAR;
                               size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������ �������� �������� � ��������� UNICODE �� ����
  // ���������  code  - ��� ��������
  //            name  - ����� ������ ��� ���������� �������� ��������
  //            size  - ����� ���������� ������� ��� ������ � ������
  // ��� ������ ���������� 0
  // name �������� �������� "�� �����������"                       // 19/05/10

function mapGetProjectionNameByCodeUn(code : integer; name : PWCHAR;
                               size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������ ���� � �������� �������� �� ������ � �������
  // ���������  number  - ����� ������ ������� �������� (����� ���������� � 1)
  //            code    - ��� ��������
  //            name    - ����� ������ ��� ���������� �������� ��������
  //            size    - ����� ���������� ������� ��� ������
  //                      (������������ ����� ������ 256 ����)
  // ��� ������ ���������� 0

function mapGetProjectionByNumber(number : integer; var code : integer;
                               var name : GTKCHAR; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapGetProjectionByNumberUn(number : integer; var code : integer;
                               var name : PWCHAR; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  //**********************************************************
  //*                             *
  //*       ������� ������� ���������� � ����� �����         *
  //*                             *
  //**********************************************************

 // ��������� ����� ����� �� �����
 // hmap - ������������� �������� ������
 // ��� ������ ���������� ����

function  mapGetLayerCount(Map : HMap) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� �������� ���� �� ��� ������ (number)
 // hmap - ������������� �������� ������
 // name - ����� ������ ��� ���������� �������
 // size - ������ ������  � ������
 // ����� ������� ���� 0
 // ��� ������ ���������� ����

function  mapGetLayerName(Map : HMap; number: integer) : GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 function  mapGetLayerNameEx(Map : HMap;number : integer;
           name: GTKPChar; size: integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� �������� ���� � ��������� UNICODE �� ��� ������ (number)
 // hmap - ������������� �������� ������
 // name - ����� ������ ��� ���������� �������
 // size - ������ ������
 // ����� ������� ���� 0
 // ��� ������ ���������� ����

function  mapGetLayerNameUn(Map : HMap;number : integer;
                            name : PWCHAR; size: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  //**********************************************************
  //*                             *
  //*       ������� ������� ���������� � ������ �����        *
  //*                             *
  //**********************************************************

 // ���������� ����������� ����� ����� �� �������� ����������� (x;y).
 // ������� ��������� ������ ���������� place.
 // ���� ���� �� ������ - ���������� ����.
 // ���� � ����� ����� ��������� ������ :
 // hmap - ������������� �������� ������
 // number - ���������� ����� ����� � ���������� (������ � 1).
 // ����� ������ ���� ���������� ������� ������
 // ��� ������ ���������� ����

function mapWhatListNumber(Map:HMap;X:double;Y:double;number:integer;
                           place:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ��������� ������������ ����� �� �������� ����������� (x,y).
 // hmap - ������������� �������� ������
 // name - ����� ������ ��� ���������� �������
 // size - ������ ������
 // ������� ��������� ������ ���������� place.
 // ���� ���� �� ������ - ���������� ����

function mapWhatListName(Map:HMap;X:double;Y:double;number:integer;
                         place:integer):GTKPChar;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 

function  mapWhatListNameEx(Map : HMap; x,y:double; Number, place : integer;
                            name: GTKPChar; size: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapWhatListNameUn(map : HMAP; x, y : double; number, place : integer;  // 20130127 Korolev
                            name : PWCHAR; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;							
									

 // ��������� ��� ����� (ANSI) �� ��� ������ (number)               // 18/05/10
 // hmap - ������������� �������� ������
 // name - ����� ������ ��� ���������� �������
 // size - ������ ������  � ������
 // ��� ������ ���������� ����

function mapGetSheetName(aMAP : HMAP; number : integer) : GTKPCHAR;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapGetSheetNameEx(aMAp : HMAP; number : integer;
                           name : GTKPCHAR; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapGetSheetNameUn(aMAp : HMAP; number : integer;
                           name : PWCHAR; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������������ ����� (ANSI) �� ��� ������ (number)
 // hmap - ������������� �������� ������
 // name - ����� ������ ��� ���������� �������
 // size - ������ ������
 // ��� ������ ���������� ����


function mapGetListName(Map:HMap;number:integer):GTKPChar;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function  mapGetListNameEx(Map : HMap;number : integer; Name : GTKPChar;size : integer) : integer; // 30/09/2009
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������������ ����� � ��������� UNICODE �� ��� ������ (number)
 // hmap - ������������� �������� ������
 // name - ����� ������ ��� ���������� �������
 // size - ������ ������
 // ��� ������ ���������� ����

function  mapGetListNameUn(Map : HMap; number : integer; name : PWCHAR; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ����� ����� ������ � ������
 // hmap - ������������� �������� ������
 // ��� ������ ���������� ����

function  mapGetListCount(Map : HMap) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ����� ����� �������� � �����
 // hmap - ������������� �������� ������
 // number - ����� �����
 // ��� ������ ���������� ����

function mapGetObjectCount(Map:HMap;number:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ��������� ����� ����� �������� � �����, �������� ���������           // 03/04/2014
 // number - ����� �����
 // ��� ������ ���������� ����

function mapGetRealObjectCount(Map:HMap;number:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


 // ��������� ����� ����� �� ��� ������������                 // 27/05/09
 // hmap - ������������� �������� ������
 // name - ��� �����
 // ��� ������ ���������� ����

function  mapGetListNumberByName(Map : HMap; const name : GTKPChar) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetListNumberByNameUn(Map : HMap; const name : PWChar) : integer;       // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� �� ������������ ����� ��� �������������� �����
 // hmap - ������������� �������� ������
 // listname - ��� ����� (������������)
 // ���������� ����� ����� � ������� ����;������� �����������
 // ���� �� ����� listname
 // (0-������� �����; 1-������ ���������������� ����� � �.�.)
 // ��� ������ ���������� "-1"

function mapWhatListLayoutIs(Maph : HMAP; listname : GTKPChar) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function mapWhatListLayoutIsUn(Maph : HMAP; listname : PWChar) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


 // ��������� ������ "����� �����"
 // hmap - ������������� �������� ������
 // list - ����� ����� (c 1)
 // info - ������������� ������� ����� � ������
 // ��� ������ ���������� ����

function  mapGetListFrameObject(Map : HMap; list: integer; Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� �������� ������� "����� �����" (���� ����� ��� -
 // ����������� �� ��������� �� ��������)
 // hmap  - ������������� �������� ������
 // list  - ����� �����
 // frame - ��������� �� �������� ����� � ������
 // ��� ������ ���������� ����

function  mapGetListFrame(Map : HMap; list: integer; var frame : TDFRAME) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������� ������ "����� �����"
 // hmap - ������������� �������� ������
 // list - ���������������� ����� ����� ����� (c 1)
 // info - ������������� ������� ����� � ������
 // HOBJ ������ ���� ������ ������� mapCreateObject
 // ��� �������� ���������� HOBJ ����� ��������� ���������
 // ��� ������������ ����� �����
 // ��� ���������������� ����� ����� ���������; �� �� ������������
 // ��� ����� ��������� ��������� ����� ����������� �� �����
 // ��� ������ ���������� ����

function  mapCreateListFrameObject(Map : HMap; list: integer; Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  //*********************************************************
  //*                             *
  //* ������� ������� ���������� �� �������������� ��'�����  *
  //*                             *
  //*********************************************************/

 // ��������� ����� �������� ��������� � ��������������
 // hmap - ������������� �������� ������
 // ��� ������ ���������� ����

function  mapRscObjectCount(Map : HMap): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ����� �������� ��������� � ��������������
 // � �������� ����
 // hmap - ������������� �������� ������
 // layer - ����� ���� � ��������������
 // ��� ������ ���������� ����

function mapRscObjectCountInLayer(Map:HMap;layer:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ��������� �������� ������� �� ����������� ������ (number)
 // � �������� ����
 // hmap - ������������� �������� ������
 // layer - ����� ���� � ��������������
 // ��� ������ ���������� ���� ��� ������ ������

function mapRscObjectNameInLayer(Map:HMap;layer:integer;number:integer):GTKPChar;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ��������� �������� ������� �� ����������� ������
 // � �������� ����
 // name - ����� ������ ��� ���������� ����������
// size - ������ ������ � ������
 // ��� ������ ���������� ����

function mapRscObjectNameInLayerEx(Map : HMap; layer, number : integer; // 08/05/07
                                    Name : GTKPChar; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapRscObjectNameInLayerUn(Map : HMap; layer, number : integer; // 08/05/07
                                    Name : PWChar; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ����������������� ��� �������
 // �� ����������� ������ � �������� ���� (number)
 // hmap - ������������� �������� ������
 // layer - ����� ���� � ��������������
 // ��� ������ ���������� ����

function mapRscObjectExcodeInLayer(Map:HMap;layer:integer;number:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ��������� ��� ����������� �������
 // �� ����������� ������ � �������� ���� (number)
 // hmap - ������������� �������� ������
 // layer - ����� ���� � ��������������
 // ��� ������ ���������� ���� (���� ��������)

function mapRscObjectLocalInLayer(Map:HMap;layer:integer;
                                  number:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


 // ��������� ���������� ��� (������) �������
 // �� ����������� ������ � �������� ���� (number)
 // hmap - ������������� �������� ������
 // layer - ����� ���� � ��������������
 // ��� ������ ���������� ����

function mapRscObjectCodeInLayer(Map:HMap;layer:integer;
                                 number:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 

 // ��������� ���������� ��� (������) �������
 // �� �������� ���� (excode) � ����������� (local)
 // hmap - ������������� �������� ������
 // ��� ������ ���������� ����

function mapRscObjectCode(Map:HMap;excode:integer;
                          local:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


 // ��������� ����� ������������� ������������� � ��������������
 // hmap - ������������� �������� ������
 // ��� ������ ���������� ����

function  mapRscSemanticCount(Map : HMap) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ��� ������������� �������������� �������
 // hmap - ������������� �������� ������
 // number - ���������������� ����� �������������� � �������������� (c 1)
 // ��� ������ ���������� ����

function mapRscSemanticCode(Map:HMAP;number:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ��������� ��� ������������� �������������� �������
 // �� ��������� ����� ���������               // 09/02/05
 // hmap - ������������� �������� ������
 // key  - �������� ��� ��������� (����) � ��������������
 // ��� ������ ���������� ����

function  mapRscSemanticCodeByKey(Map : HMap; key : GTKPChar) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapRscSemanticCodeByKeyUn(Map : HMap; key : PWChar) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� �������� ������������� �������������� �������
 // hmap - ������������� �������� ������
 // code    - ��� ��������������
 // ��� ������ ���������� ���� ��� ������ ������

function mapRscSemanticName(Map:HMAP;code:integer):GTKPChar;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function mapRscSemanticNameUn(map : HMAP; code : integer; semname : PWCHAR; size : integer) : integer; // 20130127 Korolev
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
external sGisAcces;

  //**********************************************************
  //*                             *
  //*           �������������� ��������� �����               *
  //*                             *
  //**********************************************************

 // ��������� �������� ������ (���� ����� ����)
 // hmap - ������������� �������� ������
 // dframe - ��������� �� ����������� ���������
 // ������������� ���������� ����� ������ � ������ ��� �������� �� ���������
 // � ���������������� ������� ��� � �������� ������������ �������� ������ ����
 // ������
 // place  - ������������� ������� ��������� (PP_PICTURE, PP_PLANE, PP_GEO)
 // ��� ������ ���������� ����

function  mapGetTotalBorder(Map : HMap;var Frame : TDFRAME; place: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // ��������� �������� ������ (���� ����� ����) � ������� ���������,
 // �������� ����� EPSG, � ������ � �������� �� ���������
 // hmap - ������������� �������� ������
 // dframeplane - ��������� �� ����������� ��������� � ������
 // dframegeo   - ��������� �� ����������� ��������� � ��������
 // epsgcode    - ��� ������� ��������� (3395, 3857, 4326 � �.�.)
 // ��� ������������� ������ ��������� ���������� 2,
 // ��� ������� ������������� ���������� 1.
 // ��� ������ ���������� ����

 function  mapGetTotalBorderByEPSG(Map : HMap;var dframeplane,dframegeo : TDFRAME;
                    epsgcode: integer) : integer;        // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // �������������� �� ��������� �� ����� (������ �����)
 // � ������� �� �����������
 // hmap - ������������� �������� ������
 // x;y  - ������������� ����������
 // �� ����� ��������; �� ������ - �������.
 // ���������� :
 // xpix = xdis; ypix = ydis;
 // mapMapToPicture(xpix;ypix);

procedure mapMapToPicture(Map:HMap;var X:double;var Y:double);
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // �������������� �� ��������� �� ����� (������ �����)
 // � ����� �� ���������
 // �� ����� ��������; �� ������ - �����.

procedure mapMapToPlane(Map:HMap;var X:double;var Y:double);
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // �������������� �� �������� � ����������� � ����������
 // ����� (������ �����) � ���������
 // hmap - ������������� �������� ������
 // x;y  - ������������� ����������

procedure mapPictureToMap(Map:HMap;var X:double;var Y:double);
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // �������������� �� �������� � ����������� � ����������
 // �� ��������� � ������
 // ���������� :
 // xmet = xdis; ymet = ydis;
 // mapPictureToPlane(xmet;ymet);
 // hmap - ������������� �������� ������
 // x;y  - ������������� ����������

procedure mapPictureToPlane(Map:HMap;var X:double;var Y:double);
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // �������������� �� ������ �� ��������� � ��������
 // �� ����� (������ �����)
 // hmap - ������������� �������� ������
 // x;y  - ������������� ����������

procedure mapPlaneToMap(Map:HMap;var X:double;var Y:double);
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 

 // �������������� �� ������ �� ��������� � ������� ��
 // �����������
 // hmap - ������������� �������� ������
 // x;y  - ������������� ����������

procedure mapPlaneToPicture(Map:HMap;var X:double;var Y:double);
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // �������������� �� ������ �� ��������� � �������������
 // ���������� � �������� � ������������ � ��������� �����
 // (������������� �� ��� ���� ���� !)
 // ���������� :
 // if (mapIsGeoSupported())   |  ��� :
 //   {                        |  if (mapIsGeoSupported())
 //     B = Xmet; L = Ymet;    |    {
 //     mapPlan2Geo(B;L);      |      mapPlan2Geo(B=Xmet;L=Ymet);
//   }                        |    }
 // hmap - ������������� �������� ������
 // Bx;Ly  - ������������� ����������
 // �� ����� �����; �� ������ - �������
 // ��� ������ ���������� 0

function mapPlaneToGeo(Map:HMap; var BX, LY : double) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // �������������� �� ������������� ��������� � ��������
 // � ����� �� ��������� � ������������ � ��������� �����
 // (�������������� �� ��� ���� ���� !)
 // hmap - ������������� �������� ������
 // Bx;Ly  - ������������� ����������
 // �� ����� �������; �� ������ - �����
 // ��� ������ ���������� 0

function mapGeoToPlane(Map : HMap; var BX , LY : double) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // �������������� ��������� � ������ �� ��������� �� ����� ���� � ������
 // source - ����� �������� ���� ������� 1942�
 // target - ������ ����
 // x;y  - ������������� ����������
 // �� ����� ����� � ����� ���� 42�.;�� ������ - ������.
 // ��� ������ ���������� 0

function  mapPlaneToPlaneByZone(source, target : integer; var x,y : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // �������������� ��������� � ������ �� ��������� �� �������� ����
 // � ������������� ���������� � ������� 42�.
 // zone - ����� �������� ���� ������� 1942�
 // x;y  - ������������� ����������
 // �� ����� ����� � ����� ���� 42�.;�� ������ - �������.
 // ��� ������ ���������� 0

function mapPlane42ToGeo42ByZone(zone : integer; var x, y : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapPlaneToGeo42ByZone(zone : integer; var x,y : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF} external sGisAcces;

 // �������������� ��������� � ������ �� ��������� �� �������� ���� UTM
 // � ������������� ���������� � ������� WGS-84.         // 03/07/06
 // zone - ����� �������� ���� ������� UTM
 // x;y  - ������������� ����������
 // �� ����� ����� � ����� ���� UTM; �� ������ - ������� WGS-84.
 // ��� ������ ���������� 0

function  mapPlaneUTMToGeoWGS84ByZone(zone : integer; var x,y : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� ��������� � ������ �� ��������� �� ����� ���� � ������  // 04/09/09
 // source - ����� �������� ���� ������� UTM
 // target - ������ ���� ������� UTM
 // x,y  - ������������� ����������
 // �� ����� ����� � ����� ���� UTM, �� ������ - ����� � ������ ���� UTM.
 // ��� ������ ���������� 0
function mapPlaneUTMToPlaneUTMByZone(source : integer; target: integer;
                                        var x, y : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������ - �������������� �� �������� � �������������
 // ����������� �� ������� ������������� � �������
 // hmap - ������������� �������� ������
 // ���� ��� - ���������� ����

function  mapIsGeoSupported(Map : HMap) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� �� ������ �� ��������� (�������� �����)
 // � ������������� ���������� � �������� (���������� ��������� WGS84)
 // (�������������� �� ��� ���� ���� !)
 // ������� ������ �������� �������� ��������;
 // ������� mapPlaneToGeoWGS84() ��������
 // ���������� ������ �� �������
 // ���������� :
 // if (mapIsGeoSupported())
 //   {
 //     B = Xmet; L = Ymet;
 //     mapPlaneToGeoWGS84(hMap;B;L);
//   }
 // hmap  - ������������� �������� ������
 // Bx;Ly - ������������� ����������
 // �� ����� �����; �� ������ - �������
 // H     - ������ � ����� (�����)
 // ��� ������ ���������� 0

function mapPlaneToGeoWGS84(Map:HMap; var Bx, Ly : double) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;
function mapPlaneToGeoWGS843D(Map:HMap; var Bx, Ly, H : double) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


 // �������������� �� ������ �� ��������� (�������� �����)
 // � ������������� ���������� � �������� (��������� �����������; ��-42)
 // (�������������� �� ��� ���� ���� !)
 // hmap  - ������������� �������� ������
 // Bx;Ly - ������������� ����������
 // �� ����� �����; �� ������ - �������
 // H     - ������ � ����� (�����)
 // ��� ������ ���������� ����

function mapPlaneToGeo42(Map:HMap; var Bx, Ly) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function mapPlaneToGeo423D(Map:HMap; var Bx, Ly, H : double) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;



 // �������������� �� ������ �� ��������� (�������� �����)
 // � ������������� ���������� � �������� (��������� �����������; ��-95)
 // (�������������� �� ��� ���� ���� !)
 // ���� �������� ����� � ��-42; �� ������������� ���������� �������� � ��-42
 // ��� �������������� 
 // hmap  - ������������� �������� ������
 // Bx;Ly - ������������� ����������
 // �� ����� �����; �� ������ - �������
 // H     - ������ � ����� (�����)
 // ��� ������ ���������� ����

function  mapPlaneToGeo953D(Map : HMap;var Bx, Ly, H : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� ��������� �� ������������� ������� ��������� ����� �    // 29/12/10
 // � ������������� ���������� � �������� (���������� ��������� WGS84)
 // (�������������� �� ��� ���� ���� !)
 // ��� ������ ���������� ����

function  mapGeoToGeoWGS843D(Map : HMap;var Bx, Ly, H : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� ��������� �� ������������� ������� ��������� � ��������   // 29/12/10
 // (���������� ��������� WGS84) � ������������� ������� ��������� �����
 // (�������������� �� ��� ���� ���� !)
 // ��� ������ ���������� ����

function  mapGeoWGS84ToGeo3D(Map : HMap;var Bx, Ly, H : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

                                           
 //  �������������� �� ������������� ��������� � ��������
 // (���������� ��������� WGS84)
 //  � ����� �� ��������� � ������� 1942� (��������� �����������)
 // (�������������� �� ��� ���� ���� !)
 // ���������� :
 // if (mapIsGeoSupported())
 //   {
 //     B = Xmet; L = Ymet;
 //     mapGeo84ToPlane42(hMap;B;L);
//   }
 // hmap - ������������� �������� ������
 // Bx;Ly  - ������������� ����������
 // �� ����� �������; �� ������ - �����
 // ��� ������ ���������� ����

function mapGeoWGS84ToPlane42(Map:HMap; var Bx, Ly : double) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 //  �������������� �� ������������� ��������� � ��������
 // (���������� ��������� WGS84)
 //  � ����� �� ��������� � �������� �����
 // (�������������� �� ��� ���� ���� !)
 // hmap - ������������� �������� ������
 // Bx;Ly;H  - ������������� ����������
 // �� ����� �������; �� ������ - �����
 // ��� ������ ���������� ����

function mapGeoWGS84ToPlane3D(Map:HMap; var Bx, Ly, H : double) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 //  �������������� �� ������������� ��������� � ��������
 // (��������� �����������; ��-42)
 //  � ����� �� ��������� � �������� �����
 // (�������������� �� ��� ���� ���� !)
 // hmap - ������������� �������� ������
 // Bx;Ly;H  - ������������� ����������
 // �� ����� �������; �� ������ - �����
 // ��� ������ ���������� ����

function mapGeo42ToPlane3D(Map:HMap; var Bx, Ly, H : double) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 //  �������������� �� ������������� ��������� � ��������
 // (��������� �����������; ��-95)
 //  � ����� �� ��������� � �������� �����
 // (�������������� �� ��� ���� ���� !)
 // hmap - ������������� �������� ������
 // Bx;Ly;H  - ������������� ����������
 // �� ����� �������; �� ������ - �����
 // ��� ������ ���������� ����

function  mapGeo95ToPlane3D(Map : HMap; var Bx, Ly, H : double) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 //  �������������� �� ������������� ��������� � ��������
 // (���������� ��������� ��-90)
 //  � ����� �� ��������� � �������� �����
 // (�������������� �� ��� ���� ���� !)
 // hmap - ������������� �������� ������
 // Bx;Ly;H  - ������������� ����������
 // �� ����� �������; �� ������ - �����
 // ��� ������ ���������� ����

function  mapGeoEP90ToPlane3D(Map : HMap; var Bx, Ly, H : double) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� ������������� ���������� � �������� �� ������� 1942�
 // (��������� �����������) � ������������� ���������� � ��������
 // (���������� ��������� WGS84) (�������������� �� ��� ���� ���� !)
 // hmap - ������������� �������� ������
 // Bx;Ly  - ������������� ����������
 // �� ����� ������� � 42�.; �� ������ - ������� � WGS84
 // H     - ������ � ����� (�����)
 // �������� hmap ����� ���� ����� ����

procedure mapGeo42ToGeoWGS84Ex(var Bx, Ly,H : double);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function mapGeo42ToGeoWGS84(Map:HMap; var Bx, Ly : double) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function mapGeo42ToGeoWGS843D(Map:HMap; var Bx, Ly, H : double) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // �������������� ������������� ��������� � �������� �� ������� NAHRWAN
 // (��������� ������ 1880�) � ������������� ���������� � ��������
 // ������� WGS84 (���������� ��������� WGS-84)         // 24/01/05
 // �� ����� ������� � NAHRWAN; �� ������ - ������� � WGS84
 // H     - ������ � ����� (�����)
 // ��� ������ ���������� ����

function mapGeoNahrwanToGeoWGS843D(var B, L, H : double) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;



 // �������������� ������������� ���������� � �������� �� ������� WGS 84
 // (���������� ��������� WGS84) (�������������� �� ��� ���� ���� !)
 // � ������������� ���������� � �������� � ������� 1942 � (��������� �����������)
 // hmap - ������������� �������� ������
 // Bx;Ly  - ������������� ����������
 // �� ����� ������� � WGS84; �� ������ - ������� � 42�.
 // H     - ������ � ����� (�����)
 // �������� hmap ����� ���� ����� ����

function  mapGeoWGS84ToGeo42Ex(var Bx, Ly, H : double) : integer;     // 03/05/05
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function mapGeoWGS84ToGeo42(Map:HMap; var Bx, Ly : double) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


function mapGeoWGS84ToGeo423D(Map:HMap; var Bx, Ly, H : double) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // �������������� ������������� ��������� � �������� �� ������� ��-90
 // (���������� ��������� ��-90) � ������������� ���������� � ��������
 // ������� 1942 � (��������� �����������)
 // �������� hmap ����� ���� ����� ����

procedure mapGeoEP90ToGeo42Ex(var Bx, Ly, H : double);      // 03/05/05
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapGeoEP90ToGeo42(Map : HMap; var Bx, Ly : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapGeoEP90ToGeo423D(Map : HMap; var Bx, Ly, H : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� ������������� ��������� � �������� �� ������� ��-90
 // (���������� ��������� ��-90) � ������������� ���������� � ��������
 // ������� 1995 � (��������� �����������)

procedure mapGeoEP90ToGeo95Ex(var Bx, Ly, H: double);     // 03/05/07
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� ������������� ��������� � �������� �� ������� 1995 �
 // (��������� �����������) � ������������� ���������� � ��������
 // ������� ��-90(���������� ��������� ��-90)

procedure mapGeo95ToGeoEP90Ex(var Bx, Ly, H: double);     // 03/05/07
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� ������������� ��������� � �������� �� ������� 1942 �
 // (��������� �����������) � ������������� ���������� � ��������
 // ������� ��-90(���������� ��������� ��-90)
 // �������� hmap ����� ���� ����� ����

procedure mapGeo42ToGeoEP90Ex(var Bx, Ly, H: double);       // 03/05/05
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapGeo42ToGeoEP90(Map : HMap; var Bx, Ly : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGeo42ToGeoEP903D(Map : HMap; var Bx, Ly, H : double) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� ������������� ��������� � �������� �� ������� ��-90
 // (���������� ��������� ��-90) � ������������� ���������� � ��������
 // � ������� WGS-84 (���������� ��������� WGS84)

procedure mapGeoEP90ToGeoWGS843D(var Bx : double; var Ly : double; var H : double);
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� ������������� ��������� � ������ �� ������� ��������� �����
 // � ������������� ���������� � �������� ������� ��-90(���������� ��������� ��-90)
 // ��� ������ ���������� ����

function mapPlaneToGeoEP903D(Map : HMap; var Bx, Ly, H : double) : integer;   // 16/05/07
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� ������������� ��������� � �������� �� ������� 1942 �
 // (��������� �����������) � ������������� � ������ � ������� NAHRWAN �
 // �������� UTM (��������� ������ 1880�.) - NAD27

procedure mapGeo42ToPlaneUTMEx(var Bx, Ly, H : double);       // 03/05/05
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapGeo42ToPlaneUTM(Map : HMap; var Bx, Ly : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapGeo42ToPlaneUTM3D(Map : HMap; var Bx, Ly, H : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // �������������� ������������� ��������� � �������� �� ������� 1942 �
 // (��������� �����������) � ������������� � ������ � ������ ���������
 // ���� � ����������� �����                            // 02/10/02
 // Bx;Ly - ������������� ����������

procedure mapGeo42ToPlaneByOwnZone(var Bx, Ly : double);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // �������������� ������ ����� �� ����� �������
 // ��������� � ������
 // hmap  - ������������� �������� ������
 // Src; targ - ��������� �� ������� ���������� �����;
 // ����� ��������� �� ���� � �� �� ������� ������;
 // source;target - ���� ������� � �������� ������� (PP_MAP;PP_PLANE ...);
// count - ����� ������������� �����.
 // �������� ��������� � �������������� ������������
 // ����� ����������� ������;���� IsGeoSupported() != 0.

procedure mapTransformPoints(Map:HMap;var scr:array of TDOUBLEPOINT;
                             source:integer;
                             var tag:array of TDOUBLEPOINT;target:integer;
                             count:integer);
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 

 // �������������� ��������� �� �������� � �������
 // (��� �������������� ��������)
 // degree - ���������; ���������� ���������� � ��������; �������;
 // ��������. ������� � maptype.h
 // radian - �������� � ��������

procedure mapDegreeToRadian(var degree:TGEODEGREE;var radian:double);
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // �������������� ��������� �� ������ � �������
 // (��� �������������� ��������)
 // radian - �������� � ��������
 // degree - ���������; ���������� ���������� � ��������; �������;
 // ��������. ������� � maptype.h
procedure mapRadianToDegree(var radian:double;var degree:TGEODEGREE);
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // �������������� ��������� �� �������� � ������� � ������ �����
 // degree - ���������; ���������� ���������� � ��������; �������;
 // ��������. ������� � maptype.h
 // radian - �������� � ��������

procedure mapSignDegreeToRadian(var degree : TSIGNDEGREE; var radian : double);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� ��������� �� ������ � ������� �� ������
 // radian - �������� � ��������
 // degree - ���������; ���������� ���������� � ��������; �������;
 // ��������. ������� � maptype.h

procedure mapRadianToSignDegree(var radian : double; var degree : TSIGNDEGREE);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������� ��������� �� ������ ���� ���
 // �������� ������� 42 ����
 // zone - ����� ���� ������� 42 ����
 // ��� ������ ���������� ����

function  mapGetAxisMeridianByZone(zone : integer):double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������ ���� �� ������������� ������� � ��������
 // (���������) ��� �������� ������� 42 ����
 // meridian - �������� ��������� � ��������
 // ��� ������ ���������� ����

function  mapGetZoneByMeridian(meridian:double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������� ��������� �� ������������� ������� �
 // �������� ��� ��������������� ����
 // hmap - ������������� �������� ������
 // meridian - �������� ��������� � ��������
 // �� ������������� ��������� ��� ���� ��� ���������� �������
 // ��� ������ ���������� ����

function  mapSetAxisMeridianByMeridian(Map : HMap; meridian : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������� ��������� �� ���������� Y ���
 // ��������������� ���� ������� 42 ����
 // �� ������������� ��������� ��� ���� ��� ���������� �������
 // hmap  - ������������� �������� ������
 // y     - ���������� Y � ������ ������������ �����;
 //         ���������� �� �������� ����
 // ��� ������ ���������� ����

function  mapSetAxisMeridianByPlaneY(Map : HMap; y : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  //**********************************************************
  //*                                                        *
  //*     �������� ��������� � ������� ������� ���������     *
  //*                                                        *
  //**********************************************************

 // ��������� ��������� ���������� �� ��� ������ (��. MAPCREAT.H)     // 04/03/10
 // ellipsoid - ����� ���������� (��. MAPCREAT.H, ELLIPSOIDKIND)
 // parm      - ��������� ��������� ����������
 // ��� ������ ���������� ����

function mapGetEllipsoidParameters(ellipsoid : integer; parm : PELLIPSOIDPARAM) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������� ��������� ������� ������� ���������
 // hMap   - ������������� �������� �������� �����
 // parm   - ��������� ������� ������� ���������
 // datum     - ��������� ��������� � ���������� ������� ������� ���������
 //             � WGS-84 (datum ����� ���� 0)                         // 04/03/10
 // ellipsoid - ��������� ����������������� ���������� ��� ������� 
 //             ������� ���������, ����� ���� EllipsoideKind �
 //             MAPREGISTEREX ����� USERELLIPSOID (ellipsoid ����� ���� 0)
 // ��� ������ ���������� ����

function mapSetWorkSystemParametersEx(aMAP : HMAP; parm : PMAPREGISTEREX;
                                      datum : PDATUMPARAM;
                                      ellipsoid : PELLIPSOIDPARAM) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


function mapSetWorkSystemParameters(amap: HMAP; parm : PMAPREGISTEREX) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������� ��������� ������� ������� ���������
 // hMap   - ������������� �������� �������� �����
 // parm   - ��������� ������� ������� ���������
 // datum     - ��������� ��������� � ���������� ������� ������� ���������
 //             � WGS-84
 // ellipsoid - ��������� ����������������� ���������� ��� �������
 //             ������� ���������
 // ��� ������ ���������� ����

function mapGetWorkSystemParametersEx(aMAP : HMAP;
                                      parm : PMAPREGISTEREX;
                                      datum : PDATUMPARAM;
                                      ellipsoid : PELLIPSOIDPARAM) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


function mapGetWorkSystemParameters(amap: HMAP; parm : PMAPREGISTEREX) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� - ����������� �� ��������� ������� ������� ���������
 // (���� mapSetWorkSystemParameters �� ���������� - ���������� 0)
 // hMap   - ������������� �������� �������� �����
 // ��� ������ ���������� ����
function mapIsWorkSystemParameters(amap : HMAp) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� �� ������� ������������� ��������� � ������ �� ���������� �����
 // � ����� �� ��������� � ������������ � ����������� ������� ������� ���������
 // hmap   - ������������� �������� ������
 // X,Y,H  - ������������� ����������
 // �� ����� �����, �� ������ - �����
 // ��� ������ ���������� 0
function mapPlaneToWorkSystemPlane(aMap : HMAp; var x, y, h : double): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� �� ������� ������������� ��������� � ������ �� ���������� �����
 // � ������������� ����������  � ������������ � ����������� ������� ������� ���������
 // hmap   - ������������� �������� ������
 // X,Y,H  - ������������� ����������
 // �� ����� �����, �� ������ - �������
 // ��� ������ ���������� 0

function mapPlaneToWorkSystemGeo(aMap : HMAp; var x, y, h : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� �� ������������� ��������� � �������� �� ���������� �����
 // � ����� �� ��������� � ������������ � ����������� ������� ������� ���������
 // hmap     - ������������� �������� ������
 // Bx,Ly,H  - ������������� ����������
 // �� ����� �������, �� ������ - �����
 // ��� ������ ���������� 0

function mapGeoToWorkSystemPlane(aMap : HMAp; var Bx, Ly, h : double): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� � ������������� ���������� � �������� �� ���������� �����
 // �� ������ �� ��������� � ������� ������� ��������� � ������������ � �� �����������
 // hmap    - ������������� �������� ������
 // Bx,Ly,H - ������������� ����������
 // �� ����� �������, �� ������ - �����
 // ��� ������ ���������� 0
function mapWorkSystemPlaneToGeo(aMap : HMAp; var Bx, Ly, h : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

{  /**********************************************************
  *                                                         *
  * �������� ��������� � ���������������� ������� ��������� *         // 07/10/10
  *                                                         *
  **********************************************************/}

 // ���������� ������� ��������� ���������������� ������� ���������
 // parm      - ��������� ������� ������� ��������� (��. MAPCREAT.H)
 // datum     - ��������� ��������� � ���������� ������� ������� ���������
 //             � WGS-84 (datum ����� ���� 0)                         // 04/03/10
 // ellipsoid - ��������� ����������������� ���������� ��� �������
 //             ������� ���������, ������ ����� ���� EllipsoideKind �
 //             MAPREGISTEREX ����� USERELLIPSOID (ellipsoid ����� ���� 0)
 // ���������� ������������� ���������������� ������� ���������
 // �� ���������� ������������� ���������� ������� mapDeleteUserSystemParameters
 // ��� ������ ���������� ����

function mapCreateUserSystemParameters(parm : PMAPREGISTEREX;
                                       datum : PDATUMPARAM;
                                       ellipsoid : PELLIPSOIDPARAM): HHANDLE;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� �� ������������� ��������� (�������) � ���������������� ��������
 // � ������������� ���������� � �������� (���������� ��������� WGS84)
 // huser - ������������� ���������������� ������� ���������
 // Bx,Ly - ������������� ����������
 // �� ����� �������, �� ������ - �������
 // H     - ������ � ����� (�����)
 // ��� ������ ���������� 0

function mapUserGeoToGeoWGS84(huser : HHANDLE; var Bx : double; var Ly : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapUserGeoToGeoWGS843D(huser : HHANDLE; var Bx : double; var Ly : double;
                                 var H : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� �� ������������� ��������� (�������) � ���������������� ��������  // 10/05/11
 // � ����� � ���������������� ��������
 // huser - ������������� ���������������� ������� ���������
 // Bx,Ly - ������������� ����������
 // �� ����� �������, �� ������ - �����
 // ��� ������ ���������� 0

function mapUserGeoToUserPlane(huser : HHANDLE; var Bx : double;
                           var Ly : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapUserGeoToPlane(huser : HHANDLE; var Bx : double;
                           var Ly : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� �� ������ �� ��������� � ���������������� ��������
 // � ������������� ���������� � �������� (���������� ��������� WGS84)
 // ������� ������ �������� �������� ��������
 // huser - ������������� ���������������� ������� ���������
 // Bx,Ly - ������������� ����������
 // �� ����� �����, �� ������ - �������
 // H     - ������ � ����� (�����)
 // ��� ������ ���������� 0

function mapUserPlaneToGeoWGS84(huser : HHANDLE; var Bx : double; var Ly : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapUserPlaneToGeoWGS843D(huser : HHANDLE; var Bx : double; var Ly : double;
                                 var H : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� �� ������ �� ��������� � ���������������� ��������            // 10/05/11
 // � ������������� ���������� � �������� �� ���������� � ���������������� ��������
 // ������� ������ �������� �������� ��������
 // huser - ������������� ���������������� ������� ���������
 // Bx,Ly - ������������� ����������
 // �� ����� �����, �� ������ - �������
 // ��� ������ ���������� 0

function mapUserPlaneToUserGeo(huser : HHANDLE; var Bx : double; var Ly : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapUserPlaneToGeo(huser : HHANDLE; var Bx : double; var Ly : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� �� ������������� ��������� � ��������
 // (���������� ��������� WGS84)
 // � ������������� ���������� � �������� �� ���������� � ���������������� ��������
 // huser    - ������������� ���������������� ������� ���������
 // Bx,Ly,H  - ������������� ����������
 // �� ����� �������, �� ������ - �������
 // ��� ������ ���������� ����

function mapGeoWGS84ToUserGeo(huser : HHANDLE; var Bx : double; var Ly : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapGeoWGS84ToUserGeo3D(huser : HHANDLE; var Bx : double; var Ly : double;
                                 var H : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // �������������� �� ������������� ��������� � ��������
 // (���������� ��������� WGS84)
 // � ����� �� ��������� � ���������������� ��������
 // huser    - ������������� ���������������� ������� ���������
 // Bx,Ly,H  - ������������� ����������
 // �� ����� �������, �� ������ - �����
 // ��� ������ ���������� ����

function mapGeoWGS84ToUserPlane(huser : HHANDLE; var Bx : double; var Ly : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapGeoWGS84ToUserPlane3D(huser : HHANDLE; var Bx : double; var Ly : double;
                                 var H : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� �� ������������� ��������� (�������) � �������� ���������     // 26/08/11
 // � ������������� ���������� � �������� �� ���������� � ���������������� ��������
 // hmap    - ������������� �������� ������
 // huser    - ������������� ���������������� ������� ���������
 // Bx,Ly,H  - ������������� ����������
 // �� ����� �������, �� ������ - �������
 // ��� ������ ���������� ����

function mapGeoToUserGeo(aMap : HMAP; huser : HHANDLE; var Bx : double; var Ly : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapGeoToUserGeo3D(aMap : HMAP; huser : HHANDLE; var Bx : double; var Ly : double;
                              var H : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
                                                                                    
 // �������������� �� ������������� ��������� (�������) �� ����������               // 15/09/11
 // � ���������������� �������� � ������������� ���������� (�������) � �������� ���������
 // hmap    - ������������� �������� ������
 // huser    - ������������� ���������������� ������� ���������
 // Bx,Ly,H  - ������������� ����������
 // �� ����� �������, �� ������ - �������
 // ��� ������ ���������� ����

function mapUserGeoToGeo(aMap : HMAP; huser : HHANDLE; var Bx : double; var Ly : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapUserGeoToGeo3D(aMap : HMAP; huser : HHANDLE; var Bx : double; var Ly : double;
                              var H : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ���������� ������� ���������������� ������� ���������

procedure mapDeleteUserSystemParameters(huser : HHANDLE);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� ��������� ���� ������ ���������                   // 22/01/14
 // parm1      - ��������� ������ ������� ��������� (��. MAPCREAT.H)
 // datum1     - ��������� ��������� � ���������� ������ ������� ���������
 //              � WGS-84 (datum ����� ���� 0)
 // ellipsoid1 - ��������� ����������������� ���������� ��� ������
 //              ������� ���������, ������ ����� ���� EllipsoideKind �
 //              MAPREGISTEREX ����� USERELLIPSOID (ellipsoid ����� ���� 0)
 // parm2      - ��������� ������ ������� ��������� (��. MAPCREAT.H)
 // datum2     - ��������� ��������� � ���������� ������ ������� ���������
 //              � WGS-84 (datum ����� ���� 0)
 // ellipsoid2 - ��������� ����������������� ���������� ��� ������
 //              ������� ���������
 // ��� ������������ �����-���� �������� ���������� ���������� ��������� ��������
 // ��������� ������������� ��������� ����� ��������� �����������
 // (��������, ��������������� ����� UTM � �������-�������������� ����� UTM)

function mapCompareSystemParameters(parm1:PMAPREGISTEREX ;
                                    datum1:PDATUMPARAM;
                                    ellipsoid1:PELLIPSOIDPARAM ;
                                    parm2:PMAPREGISTEREX ;
                                    datum2:PDATUMPARAM;
                                    ellipsoid2:PELLIPSOIDPARAM):Integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;



  {*********************************************************
  *                                                        *
  *     ����������� ��������� � �������� �������           *
  *                                                        *
  *********************************************************/}

 // ���������� ������ ����������� ������� ��������� �������   // 09/06/10
 // hmap    - ������������� �������� ������
 // format -  ����� ������� ����������� ��������� (��. maptype.h - CURRENTPOINTFORMAT)
 // ��� ������ ���������� ����, ����� - ������������� ��������

function mapSetCurrentPointFormat(aMap : HMAP; format : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������ ����������� ������� ��������� �������
 // hmap    - ������������� �������� ������
 // ���������� ����� ������� ����������� ��������� (��. maptype.h - CURRENTPOINTFORMAT)
 // ��� ������ ���������� ����

function mapGetCurrentPointFormat(aMap : HMAP) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ����������� �������� ��������� �� ������� ������������� ��������� ��������� (�����)
 // � �������, ������������ �������� ����������� ������� ���������
 // hmap    - ������������� �������� ������
 // x, y    - ���������� ����� � �������� � ������������ � �������� �����������
 //           �������� (mapGetDocProjection) - ��������������� � ����� ��������
 // h       - ������ ����� (��������� ����� ���� ����� ����), ���� ������ �������� -
 //           ��������������� � ����� ��������
 // maptype - ��� �����, ��������������� �����������, ���� �� ����� ����, 
 //           �� ����������� ������ � ������������ ������� ���������: "(��42)", "(CR95)"...
 // ��� ������ ���������� ����

function mapPlaneToPointFormat(aMap : HMAP;
                               var x: double; var y : double; var h : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
  
 // ����������� �������� ��������� �� ������� ������������� ��������� ��������� (�����)
 // � �������, ������������ �������� ����������� ������� ���������
 // � ������������ ������
 // hmap    - ������������� �������� ������
 // x, y    - ���������� ����� � �������� � ������������ � �������� �����������
 //           �������� (mapGetDocProjection)
 // h       - ������ ����� (��������� ����� ���� ����� ����)
 // place   - ����� ������ ��� ������ ����������
 // size    - ������ ��������� ������ (�� ����� 256 ����)
 // ������ ������:
 // B= -73� 27' 04.53"  L= 175� 51' 21.07"  H= 109.51 m (WGS84)
 // X= 6 309 212.12 �   Y= 7 412 249.25 � (��42)
 // ��� ������ ���������� ����

function mapPlaneToPointFormatString(aMap : HMAP; X, Y, H : PDouble;        // 09/06/2012
                                     place : GTKPCHAR; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapPlaneToPointFormatStringUn(aMap : HMAP; X, Y, H: PDouble;        // 09/06/2012
                                       place : PWCHAR; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ����� ��������� ����� � ������
 // x,y   - ������� ������������� ���������� ����� � ������
 // h     - ������ � ������ ��� ������� ���������
 // place - ����� ������ ��� ���������� ����������
 // size  - ������ ������ (�� ����� 80 ����)
 // maptype - ��� �����, ���� �� ����� ����, �� ����������� ������
 //           � ������������ ������� ���������: "   (��42)", "   (C�95)",...
 // ������ ����������:
 // "X=  438 145.27 m  Y= 6 230 513.03 m  H=  54.12 m"

{$IFDEF WINCE} // 01/12/2010
procedure mapPlaneToString(X, Y, H : PDouble;                  // 09/06/2012
                           place : PWCHAR; size : integer;
                           maptype : integer);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
{$ELSE}

procedure mapPlaneToString(X, Y, H: PDouble;                   // 09/06/2012
                           place : GTKPCHAR; size : integer;
                           maptype : integer);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
{$endif}  // !WINCE

function mapPlaneToStringUn(X, Y, H : PDouble;                  // 09/06/2012
                           place : PWCHAR; size : integer;
                           maptype : integer):Integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������ ����� � ��������� ������ � ���������� ���� �� �������� ����������� ��������
 // (���������� �� ������ �������� �� ����� ������ � ������)
 // ��������: 7 390 621.458                                    // 01/02/12
 // value  - �������� �����, ������������� � ������
 // str - ����� ������ ��� ���������� ����������
 // size   - ����� ������ � ������ (�� ����� 16 !)
 // precision - ����� ������ ����� �������
 // ��� ������ ���������� ����

function mapDoubleToString(value : double; str : GTKPCHAR; size, precision : integer) : integer;  // 20130127 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapDoubleToStringUn(value : double; str : PWCHAR; size, precision : integer) : integer;  // 20130127 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ������ ������ ����� � ���������� ���� �� �������� ����������� ��������
  // (���������� �� ������ �������� �� ����� ������ � ������)
  // size - ����� ������ � ������ (�� ����� 16 !)
  // ��� ������ ���������� ����
{$IFDEF WINCE}  // 01/12/2010
function mapLongToString(number : integer; str : PWCHAR; size: integer): integer; // 09/06/2012
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
{$ELSE}
function mapLongToString(number : integer; str : GTKPCHAR; size: integer): integer;  // 09/06/2012
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
{$endif}  // !WINCE

function mapLongToStringUn(number : integer; str : PWCHAR; size : integer) : integer;   // 20130127 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

// 11/12/13 {$IFDEF WIN32}
{$IFNDEF LINUXAPI}          // 11/12/13
  // ������ ������ ����� ���� __int64 � ���������� ����
  // �� �������� ����������� ��������
  // (���������� �� ������ �������� �� ����� ������ � ������)
  // size - ����� ������ � ������ (�� ����� 32 !)
  // ��� ������ ���������� ����

{$IFDEF WINCE} // 01/12/2010
function mapInt64ToString(number: Int64; str: PWCHAR;   // 09/06/2012
                            size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
{$ELSE}

function mapInt64ToString(number: Int64; str: GTKPCHAR;  // 09/06/2012
                            size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
{$endif}  // !WINCE
{$endif}  // LINUXAPI}  // 11/12/13 // !WIN32API

 function mapInt64ToStringUn(number: Int64; str: PWCHAR;   // 09/06/2012
                            size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
  // ��������� ������� ����� ����� �� ��������� ����� ������
  // ����� ����� �������� ��� ���������
  // value - �������� ��������
  // count - ����� ������ ����� ������� (�� 0 �� 9)

function mapRoundDouble(value : double; count : integer) : double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������ �������� � ���������� ���� �� �������� ����������� ��������
  // (��������: "1 : 50 000","2 : 1" - ���� scale < 1)
  // (���������� �� ������ �������� �� ����� ������ � ������)
  // size - ����� ������ � ������ (�� ����� 20 !)
  // ��� ������ ���������� ����

{$IFDEF WINCE} // 01/12/2010
function mapScaleToString(scale : double; str : PWCHAR; size : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
{$ELSE}
function mapScaleToString(scale : double; str : GTKPCHAR; size : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
{$endif}  // !WINCE

function mapScaleToStringUn(scale : double; str : PWCHAR; size : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

{*************************************************************************
*                                                                        *
*             ������� ������� � ���� ������ EPSG                         *
*  ��� ���������� ���������� ������� � ����� ���������� ������           *
*  EPSG.CSG, EPSG.CSP, EPSG.CSU, ������� ������ � ������ ��� ����� 2011. *
*  �������� ����� ����� ��������� ��������� EPSGReader, �������� �       *
*  ��� ����� 2011.                                                       *
*************************************************************************}

  // ��������� ��������� �������� � ������� ��������� �� ���� EPSG      // 14/07/11
  // ���� ��� EPSG ������ ������������� ������� ���������,
  // �� ��������������� �������� ������\������� � ���������������
  // ��������� ���������� � �����
  // ���� ��� EPSG ������ ������� ������������� ������� ���������,
  // �� ��� ��������� ��������������� �� ���� EPSG
  // parm      - ��������� ������� ��������� � ��������
  // datum     - ��������� ��������� � ���������� ������� ������� ���������
  //             � WGS-84
  // ellipsoid - ��������� ����������������� ���������� ��� �������
  //             ������� ���������
  // ��� ������������� ������ ��������� ���������� 2,
  // ��� ������� ������������� ���������� 1.
  // ��� ������ ���������� 0

function mapGetParametersForEPSG(epsgcode : integer;
                                 mapreg : PMAPREGISTEREX;
                                 datum : PDATUMPARAM;
                                 ellipsoid : PELLIPSOIDPARAM) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ���� ������ EPSG            // 24/03/11
  // ��� �������� ���������� ���������� ������������� �������� ���� ������ EPSG
  // ��� ������ ���������� 0

function mapOpenEPSGDatabase : THandle;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ���� ������ EPSG
  // epsgdata - ������������� �������� ���� ������ EPSG

procedure mapCloseEPSGDatabase(epsgdata : THANDLE);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ������ �� ������� ������������� ������� ��������� � ���� ������ EPSG       // 24/03/11
  // ����� ������ ������� ������� ���������� ��������� ������� ��������� � ���� ������ EPSG
  // epsgdata  - ������������� �������� ���� ������ EPSG
  // mapreg    - ��������� ��������
  // ellipsoid - ���������
  // datum     - �����
  // rectsys   - ��������� ������������� ������� ���������
  // geodsys   - ��������� ������� ������������� ������� ���������
  // unit      - ������� ���������
  // ��� ������ ��� ��� ������ �� ������� ������ ������ ���������� 0

function mapReadEPSGProjectedSystem(epsgdata : THandle; mapreg : PMAPREGISTEREX;
                                    ellipsoid : PELLIPSOIDPARAM; datum : PDATUMPARAM;
                                    rectsys : PEPSGRECTSYS; geodsys : PEPSGGEODSYS;
                                    aunit : PEPSGMEASUNIT) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ������ �� ������������� ������� ��������� �� ������ ������ � ���� ������ EPSG   // 24/03/11
  // ����� ������ ������� ������� ���������� ��������� ������� ��������� � ���� ������ EPSG
  // epsgdata    - ������������� �������� ���� ������ EPSG
  // coordsysnum - ����� ������ � ������ ������������� ������ ��������� (�� 1)
  // mapreg      - ��������� ��������
  // ellipsoid   - ���������
  // datum       - �����
  // rectsys     - ��������� ������������� ������� ���������
  // geodsys     - ��������� ������� ������������� ������� ���������
  // unit        - ������� ���������
  // ��� ������ ��� ��� ������ �� ������� ������ ������ ���������� 0

function mapReadEPSGProjectedSystemByNumber(epsgdata : THandle; coordsysnum : integer;
                                    mapreg : PMAPREGISTEREX; ellipsoid : PELLIPSOIDPARAM;
                                    datum : PDATUMPARAM; rectsys : PEPSGRECTSYS;
                                    geodsys : PEPSGGEODSYS; aunit : PEPSGMEASUNIT) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ��������� ������������� ������� ��������� �� ���� � ���� ������ EPSG
  // epsgcode  - ��� ������������� ������� ��������� � ���� ������ EPSG
  // ellipsoid - ��������� ����������
  // datum     - ��������� ������
  // geodsys   - ��������� ������������� ��
  // ��� ������ ���������� 0

function mapGetEPSGGeodeticSystem(epsgcode : integer; ellipsoid : PELLIPSOIDPARAM;
                                    datum : PDATUMPARAM;
                                    geodsys : PEPSGGEODSYS ) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ��������� ��������� ������������� ������� ��������� �� ���� � ���� ������ EPSG
  // epsgcode - ��� ������������� ������� ��������� � ���� ������ EPSG
  // mapreg   - ��������� ��������
  // rectsys  - ��������� ������������� ��
  // ��� ������ ���������� 0

function mapGetEPSGProjectedSystem(epsgcode : integer; mapreg : PMAPREGISTEREX;
                                    rectsys : PEPSGRECTSYS) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ��������� ������� ��������� �� ���� � ���� ������ EPSG
  // epsgcode - ��� ������� ��������� � ���� ������ EPSG
  // unit     - ��������� ������� ���������
  // ��� ������ ���������� 0

function mapGetEPSGUnit(epsgcode : integer; aunit : PEPSGMEASUNIT) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� WKT ������ �� MAPREGISTEREX, ELLIPSOIDPARAM, DATUMPARAM
  // mapreg    - ��������� ��������
  // ellipsoid - ��������� ����������
  // datum     - ��������� ������
  // wktstr    - ����������� ������ � ��������� ������� ���������
  // wktstrsize - ����������������� ������ ������ (4 ������ ����������)
  // ��� ������ ���������� 0

function mapSetWKTString (mapreg:PMAPREGISTEREX;
                          ellipsoid: PELLIPSOIDPARAM;
                          datum: PDATUMPARAM;
                          wktstr: PChar;
                          wktstrsize:Integer) : integer;         //03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;




  {***************************************************************
  *                                                              *
  *  ������� ������ ���������� ������ ������� �� ������ � �����  *
  *                                                              *
  ****************************************************************
  * <?xml version="1.0" encoding="UTF-8"?>                       *
  * <ProjectList Version="1.0">                                  *
  * <Project Name="ITM"                                          *
  * Comment="Israel Transverse Mercator"                         *
  * EPSG="2039">                                                 *
  * <Projection Type="Transverse Mercator"                       *
  * EPSG="9807"                                                  *
  * CentralMeridian="31 44 3.816999999992845"                    *
  * LatitudeOfOrigin="35 12 16.260999999993260"                  *
  * ScaleFactor="1.0000067"                                      *
  * FalseEasting="219529.584"                                    *
  * FalseNothing="626907.3899999999"/>                           *
  * <Spheroid Type="GRS 1980" Parm="6378137.0 298.257222101"/>   *
  * </Project>                                                   *
  * </ProjectList>                                               *
  ****************************************************************}

 // ������� ������ ���������� ������ �������                      // 22/02/11
 // name - ��� ����� ������ ����������
 // ��� �������� ���������� ���������� ������������� ������ � ������
 // ��� ������ ���������� ����

function  mapOpenMapRegisterList(const name : GTKPCHAR) : HMAPREG;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapOpenMapRegisterListUn(const name : PWCHAR) : HMAPREG;    // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������� ������ ���������� ������ �������
 // name - ��� ����� ������ ����������
 // ��� �������� ���������� ���������� ������������� ������ � ������
 // ��� ������ ���������� ����

function  mapCreateMapRegisterList(const name : GTKPCHAR) : HMAPREG;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapCreateMapRegisterListUn(const name : PWCHAR) : HMAPREG;    // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������� ������ ���������� ������ �������
 // hmapreg - ������������� ������ ���������� ������ �������

procedure  mapCloseMapRegisterList(mapreg : HMAPREG);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ����� ������ �������, ���������� � ������           // 01/02/11
 // � ���� ����� XML (<ProjectList Version="1.0">
 // ����� ������� ������������� ���� ��� "Project"
 // hmapreg - ������������� ������ ���������� ������ �������
 // ��� �������� ���������� ���������� ����� ������� ����������
 // ��� ������ ���������� ����

function  mapMapRegisterListCount(mapreg : HMAPREG) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� �������� ������� ������� �� ��������� ����������� ������
 // � ������ (������� � 1)
 // hmapreg - ������������� ������ ���������� ������ �������
 // number  - ���������� ����� ������ ����������
 // name    - ����� ������ ��� ���������� ����������
 // size    - ������ ���������� ������ � ������
 // ��� ������ ���������� ����

function  mapMapRegisterListName(mapreg : HMAPREG;
                                 number : integer;
                                 name : GTKPCHAR;
                                 size: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapMapRegisterListNameUn(mapreg : HMAPREG;
                                 number : integer;
                                 name : PWCHAR;
                                 size: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ����������� ��� ������� ������� �� ��������� ����������� ������
 // � ������ (������� � 1)
 // hmapreg - ������������� ������ ���������� ������ �������
 // number  - ���������� ����� ������ ����������
 // name    - ����� ������ ��� ���������� ����������
 // size    - ����� ���������� ������ ��� ���������� ����������
 // ��� ������ ���������� ����

function  mapMapRegisterListComment(mapreg : HMAPREG;
                                 number : integer;
                                 name : GTKPCHAR;
                                 size: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapMapRegisterListCommentUn(mapreg : HMAPREG;
                                 number : integer;
                                 name : PWCHAR;
                                 size: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ��� EPSG ��� ������� ������� �� ��������� ����������� ������
 // � ������ (������� � 1)
 // hmapreg - ������������� ������ ���������� ������ �������
 // number  - ���������� ����� ������ ����������
 // ���� ��� �� ����� - ���������� "-1"
 // ��� ������ ���������� ����

function  mapMapRegisterListEPSG(mapreg : HMAPREG;
                                 number : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������������� ��� ������� �������                        
 // �� ��������� ����������� ������ � ������ (������� � 1)
 // hmapreg - ������������� ������ ���������� ������ �������
 // number  - ���������� ����� ������ ����������
 // ident   - ����� ������ ��� ���������� ��������������
 // size    - ����� ���������� ������ ��� ���������� ��������������
 // ��� ������ ���������� ����

function  mapMapRegisterListCrsIdent(mapreg : HMAPREG; number : integer;             // 20130127 Korolev
                                     ident : GTKPCHAR; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapMapRegisterListCrsIdentUn(mapreg : HMAPREG; number : integer;             // 03/04/2014
                                     ident : PWCHAR; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ��������� ������� ������� �� ��������� ����������� ������
 // � ������ (������� � 1)
 // mapreg - ������������� ������ ���������� ������ �������
 // number  - ���������� ����� ������ ����������
 // amapreg  - ��������� �������� <Projection ...>
 // datum   - ��������� ������ <Datum ...>
 // ellparm - ��������� ���������� <Spheroid ...>
 // ��� ������ ���������� ����

function  mapMapRegisterListParameters(mapreg : HMAPREG;
                                 number : integer;
                                 amapreg: PMAPREGISTEREX;
                                 datum: PDATUMPARAM;
                                 ellparm : PELLIPSOIDPARAM) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� ������ ���������� ������� �������
 // mapreg - ������������� ������ ���������� ������ �������
 // name     - ��������� �������� ������� ������� (�����������)
 // comment  - ����������� ��� ������� ������� (��� ����)
 // epsgcode - ��� EPSG (��� ����)
  // ident   - ������������� ������� ������� (��� ����)
 // amapreg   - �������� ���������� ������� ������� (�����������)(��. mapcreat.h)
 // datum    - �������� ���������� ������ (��� ����) (��. mapcreat.h)
 // ellparam - �������� ���������� ���������� (��� ����) (��. mapcreat.h)
 // ��� ������ ���������� ����

function  mapAppendMapRegisterListParameters(mapreg : HMAPREG;
                                 const name : GTKPCHAR;
                                 const comment : GTKPCHAR;
                                 epsgcode : integer;
                                 amapreg: PMAPREGISTEREX;
                                 datum: PDATUMPARAM;
                                 ellparm : PELLIPSOIDPARAM) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


function  mapAppendMapRegisterListParametersEx(amapreg : HMAPREG;                     // 20130128 Korolev
                                               const name : GTKPCHAR;
					       const comment : GTKPCHAR;
                                               epsgcode : integer;
					       const ident : GTKPCHAR;
                                               mapreg : PMAPREGISTEREX;
                                               datum : PDATUMPARAM;
                                               ellparm : PELLIPSOIDPARAM) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapAppendMapRegisterListParametersUn(amapreg : HMAPREG;                     // 03/04/2014
                                               const name : PWCHAR;
					       const comment : PWCHAR;
                                               epsgcode : integer;
					       const ident : PWCHAR;
                                               mapreg : PMAPREGISTEREX;
                                               datum : PDATUMPARAM;
                                               ellparm : PELLIPSOIDPARAM) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������� ������ ���������� ������ ������� �� ����������� ������
 // � ������ (������� � 1)
 // hmapreg - ������������� ������ ���������� ������ �������
 // number  - ���������� ����� ������ ����������
 // ��� ������������ ��������� ������ � ����� ����� �������
 // ������� mapCommitMapRegisterList
 // ��� ������ ���������� ����

function  mapDeleteMapRegisterListParameters(mapreg : HMAPREG;
                                 number : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� ��������, ����������� � ��� ������� ������� �� ��������� ����������� ������
 // � ������ (������� � 1)
 // hmapreg - ������������� ������ ���������� ������ �������
 // number  - ���������� ����� ������ ����������
 // name    - �������� ������� ������� ��� 0 (�� ������)
 // comment - ����������� � ������� ������� ��� 0 (�� ������)
 // code    - ��� EPSG ��� 0 (�� ������)
 // ident   - ������������� ������� ������� ��� 0 (�� ������)
 // ��� ������ ���������� ����

function  mapUpdateMapRegisterListName(mapreg : HMAPREG;
                                       number : integer;
                                       const name : GTKPCHAR;
                                       const comment : GTKPCHAR;
                                       code : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


function  mapUpdateMapRegisterListNameEx(mapreg : HMAPREG; number : integer;         // 20130128 Korolev
                                         const name : GTKPCHAR;
                                         const comment : GTKPCHAR;
                                         code : integer;
                                         const ident : GTKPCHAR) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapUpdateMapRegisterListNameUn(mapreg : HMAPREG; number : integer;         // 03/04/2014
                                         const name : PWCHAR;
                                         const comment : PWCHAR;
                                         code : integer;
                                         const ident : PWCHAR) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;



 // ��������� ��������� ������ ���������� ������ ������� � �����
 // hmapreg - ������������� ������ ���������� ������ �������
 // ��� ������ ���������� ����

function  mapCommitMapRegisterList(mapreg : HMAPREG) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� ��������� ������ ���������� ������ ������� � ������
 // ������ ��������� ����� ���� ��������� �� ������ mapCommitMapRegisterList
 // hmapreg - ������������� ������ ���������� ������ �������
 // ��� ������ ���������� ����

function  mapUndoMapRegisterList(mapreg : HMAPREG) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;



  //**********************************************************
  //*                             *
  //*     ������� ������� ���������� �� ��'���� �����        *
  //*                             *
  //**********************************************************

 // ��������� �������� ����� �� ������� ���������� ������
 // info - ������������� ������� ����� � ������
 // name - ����� ������ ��� ���������� �������
 // size - ������ ������  � ������
 // ��� ������ ���������� ���� !

function  mapListName(Obj : HObj) : GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapListNameEx(Obj : HObj; Name : GTKPChar; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapListNameUn(Obj : HObj; name : PWCHAR; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������������� �������������� �����; ���������� ������             //19/06/06
 // info - ������������� ������� ����� � ������
 // ��� ������ ���������� ����

function  mapGetRscIdentByObject(Obj : HObj) : HRsc;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ���������� ����� �������
 // info - ������������� ������� ����� � ������
 // ��� ������ ���������� 0 

function  mapObjectKey(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ���������� ����� �������
 // info - ������������� ������� ����� � ������
 // number - ���������� ����� ������� � �����
 // ���������; ���������� ������ ������� ������ ����������
 // ������������ ������� � ����� !
 // ��� ������ ���������� 0

function  mapSetObjectKey(Obj : HObj; number : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ����������������� ��� �������
 // info - ������������� ������� ����� � ������
 // ��� ������ ���������� 0 (���� �������� ��� ������ �������)

function  mapObjectExcode(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� �������� ����������� �������
 // info - ������������� ������� ����� � ������
 // ��� ������ ���������� 0  (���� ��������)

function  mapObjectLocal(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� �������� �������� �������
 // info - ������������� ������� ����� � ������
 // name - ����� ������ ��� ���������� �������
 // size - ������ ������
 // ��� ������ ���������� ����

function  mapObjectName(Obj : HObj) : GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapObjectNameEx(Obj : HObj; Name : GTKPChar; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� �������� �������� ������� � ��������� UNICODE
 // Obj - ������������� ������� ����� � ������
 // name - ����� ������ ��� ���������� �������
 // size - ������ ������
 // ��� ������ ���������� ����

//function  mapObjectNameUn(Obj : HObj; var name : PWCHAR; size : integer) : integer;  // 23/06/2011
function  mapObjectNameUn(Obj : HObj; name : PWCHAR; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������������ ������ ��������� �������� �������
 // obj - ������������� ������� ����� � ������
 // ��� ������ ���������� ����

function  mapObjectNameSize(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ����������� ���������� �������
 // (OD_RIGHT;OD_LEFT;... - ��. Maptype.h)
 // Obj - ������������� ������� ����� � ������
 // ����������:                                       
 //  OD_UNDEFINED (1) - �� ���������� (����������� ������ ��� ������,
 //                     ����������� � ����� ��� ������, ������� "�����")
 //  0D_RIGHT     (2) - ������ ������ (�������� ������ ���������� �������
 //                     �� ������� �������)
 //  0D_LEFT      (4) - ������ ����� (�������� ������ ���������� �������
 //                     ������ ������� �������)
 // (��. OBJECT_DIRECT � Maptype.h)
 // ��� ������ ���������� ����

function  mapObjectDirect(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ����������� ���������� �������
 //  info    - ������������� ������� ����� � ������
 //  subject - ����� ���������� (��� ������� - ����� ����)
 // ����������:                                         
 //  OD_UNDEFINED (1) - �� ���������� (����������� ������ ��� ������,
 //                     ����������� � ����� ��� ������, ������� "�����")
 //  0D_RIGHT     (2) - ������ ������ (��������� ������ �������
 //                     �� ������� �������)
 //  0D_LEFT      (4) - ������ ����� (��������� ������ �������
 //                     ������ ������� �������)
 // (��. OBJECT_DIRECT � Maptype.h)
 // ��� ������ ���������� ����
                                     
function  mapSubjectDirect(Obj : HObj; subject: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // ��������� ����� ���� ������� ("Layer" = "Segment")
 // ������ ����� ���������� � ���� !
 // Obj - ������������� ������� ����� � ������
 // ��� ������ ���������� ����

function  mapSegmentNumber(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� �������� ���� ������� ("Layer" = "Segment")
 // Obj - ������������� ������� ����� � ������
 // name - ����� ������ ��� ���������� �������
 // size - ������ ������
 // ��� ������ ���������� ����

function  mapSegmentName(Obj : HObj) : GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapSegmentNameEx(Obj : HObj; Name : GTKPChar;size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapSegmentNameUn(Obj : HObj; name : PWCHAR;size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������������ ������ ������� ���� �������
 // Obj - ������������� ������� ����� � ������
 // ��� ������ ���������� ����

function  mapSegmentNameSize(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������ (���������� ���) �������
 // Obj - ������������� ������� ����� � ������
 // ��� ������ ���������� 0 (���� �������� ��� ������ �������)

function  mapObjectCode(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� �������� ������ ��������� �� ��������������
 // ��������
 // Obj - ������������� ������� ����� � ������

procedure mapClearBotTop(Obj : HObj);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������/���������� �������� ��������� ��������� �������
 // scale - ������� ����������� �� 1:1 �� 1:40 ���.
 // Obj - ������������� ������� ����� � ������

function  mapObjectTopScale(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function mapSetObjectTopScale(Obj:HObj;scale:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 

function mapObjectBotScale(Obj:HObj):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


function mapSetObjectBotScale(Obj:HObj;scale:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ��������� - �������� �� �������� ������ ��������� � �������
 // ����������� (�� ���� �������������� �� �� ��������������;
 // � ������������� ��� ������� ����������)
 // ���� ������� ��������� �������� �� �������������� - ���������� 0

function  mapObjectBotTopUniqueness(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

{$ifndef HIDEGDX}     // 30/07/01
 // ������ �������� ���������� ������� (���������� �������)
 // Obj - ������������� ������� ����� � ������
 // ���� ������ ���������; ���������� 1; ����� 0

function  mapObjectIsGroup(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������ ������� ������� � ������ (���������� �������)
 // info - �������� ������
 // infofirst - ������ ������ � ������
 // ��� ������ ���������� 0

function mapFirstObjectInGroup(infofirst:HObj; info:HObj):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ������ ���������� ������� � ������ (���������� �������)
 // info - �������� ������
 // infonext - ��������� �� �������� ������ � ������
 // ��� ������ ���������� 0

function mapNextObjectInGroup(infonext:HObj; info:HObj):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ����������� ��������� ������ ��������� �������� (���������� �������)
 // Obj - ������������� ������� ����� � ������
 // limit - ������ ��� ����������� (� ������)
 // ��� ������ ���������� 0

function mapUnionGroupObject(Obj:HObj;limit:double):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

{$endif} // HIDEGDX

 // ������������ �������� ������ ������� �� �������� ���� � �����������
 // ��� �������� ��� ������������� ������� �� �����
 // info - ������������� ������� ����� � ������
 // excode - ������� ��� ������� (��������),
 // local  - ����������� (LOCAL_LINE, LOCAL_POINT...)
 // ������ ���������� ����� mapCreateObject(...) � ���������� ��������� 
 // (���� ��� ����)
 // ��� ������ ���������� ����

function mapRegisterObject(Obj:HObj;excode:integer;local:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ������������ �������� ������ ������� �� ��������� ����� ������� 
 // (�����) ��� �������� ��� ������������� ������� �� �����
 // info - ������������� ������� ����� � ������
 // name - ���������� ��� ������� � �������������� (�� 31 �������)
 // ������ ���������� ����� mapCreateObject(...)
 // ��� ������ ���������� ����

function  mapRegisterObjectByKey(Obj : HObj; Name : GTKPChar) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������������ �������� ������ �������
 // �� ����������� ���� ������� (��. mapRscObjectCode() � �.�.)
 // ��� �������� ��� ������������� ������� �� �����
 // info - ������������� ������� ����� � ������
 // ������ ���������� ����� mapCreateObject(...) � ����������
 // ��������� (���� ��� ����)
 // ��� ������ ���������� ����

function mapDescribeObject(Obj:HObj;code:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


 // ������������ �������� ������ ������������ �������
 // �� ������ ���� (�� �������������� �����) � �����������
 // info - ������������� ������� ����� � ������
 // layer - ���������� ����� ���� � ��������������
 // local  - ����������� (LOCAL_LINE; LOCAL_POINT...; ��. maptype.h)
 // ���������� ����� mapCreateObject(...)
 // ��� ������������ ��������� ����� ����������
 // ������������ ������� mapAppendDraw(...)
 // ��� ������ ���������� ����

function  mapRegisterDrawObject(Obj : HObj; layer, local : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ����� ����� ��� ������ �������
 // Obj - ������������� ������� ����� � ������
 // number - ���������������� ����� ����� (� 1)
 // �������� ���������������� � ���������� ������ �������
 // ��� ������ ���������� ����

function mapSetObjectListNumber(Obj:HObj;number:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ��������� ����� ����� ��� �������
 // Obj - ������������� ������� ����� � ������
 // ��� ������ ���������� ����

function  mapGetListNumber(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 //  ��������� ������ �������� ������� (IDSHORT2;...;IDDOUBLE3)
 //  ��� ������ ���������� ����; ����� - ��� ������� �������� �������

function  mapGetObjectKind(Obj : HObj) : integer;      //26/01/05
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ��� � ����������� ������� �������
 // Obj - ������������� ������� ����� � ������
 // kind - ��� �������; ��. maptype.h
 // (�������� : KM_IDFLOAT2;IDDOUBLE3; � �.�.;
 // ������� ���������������� ����� � ����� ����� ������ ����� ��� DOUBLE)
 // �������� ����������� � ����������� ������������ ���������
 // ��� ������ ���������� ����

function mapSetObjectKind(Obj:HObj;kind:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ��������� �������� ������� � ���� ������
 // info - ������������� ������� ����� � ������
 // buffer - ����� ������ ��� ���������� ����������;
 // size   - ������ ���������� ������ ��� ��������.
 // ����� ����������� ��� �������� ������� �� ������ �����
 // ��� �� �������� (!) (����������� ������ ������)
 // �������� ������� ����� ����������� ����� ����������
 // ��������; ����������; ������������
 // �� ��������������� ����������.
 // ��� ������ ���������� ����

function mapGetObjectRecord(info : HOBJ;buf1 : GTKPChar; size : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ��������� ����� �������� ������� � ���� ������
 // info - ������������� ������� ����� � ������
 // ��� ������ ���������� ����

function  mapGetObjectRecordLength(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������� ������ �� ��������� ����� �� ������ �������
 // hmap  - ������������� �������� ������
 // hsite - ������������� �������� ���������������� �����
 // mode  - ����� ��������
 //  0 - ��������;��� �����;
 //  1 - �������� ������ ��� ���������� Key();
 //  4 - ������� � ������;��� �����;
 //  5 - �������� ������ ��� ���������� Key() � ������;
 // ��� ������� 4 � 5 ��������� �����������
 // ����� mapCommitObject()
 // ��� ������ ���������� ����; ����� - ������������� ���������� �������
 // ���� ������ �� �����; ����� ���������� ������� �������� mapFreeObject !

function mapPutObjectRecord(hmap : HMAP;hSite : HSITE;buf1 : GTKPChar;
                            size,mode : integer)  : HOBJ;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
   external sGisAcces;

 // ��������� ���������� ����� ������� � �����
 // Obj  - ������������� ������� ����� � ������
 // ���� ������ ������ ������ � ����� mapCommit... �� ��������� -
 // ���������� ����
 // ��� ������ ���������� ����

function  mapGetObjectNumber(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ���������� ������������� ���� ������� � ��������������
 // (�������� ��� - ������ ������ �� 31 �������)
 // Obj  - ������������� ������� ����� � ������
 // ��� ������ ���������� ����

function  mapObjectRscKey(Obj : HObj) : GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ���������� ������������� ���� ������� � �������������� � ��������� UNICODE
 // info  - ������������� ������� ����� � ������
 // key  - ����� ������ ��� ������ ����������
 // size - ����� ������
 // ��� ������ ���������� ����

function  mapObjectRscKeyUn(info : HOBJ; key : PWCHAR; size : integer) : integer;   // 20130128 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ����������/��������/��������� ���������������� �������
 // scale = 1 ��� ��������� ���������������� ��� ���������� �����
 //         0 ��� ������ �������� ����������������
 // ��� ������ ���������� ����

function  mapSetObjectScale(Obj : HObj; scale : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapGetObjectScale(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ����������/��������/��������� ������� "�� �������" �������
 // press = 1 ��� ��������� �������� "�� �������" ��� ������ �����
 //           �������������� �������� �������� �����
 //         0 ��� ������ ��������
 // ��� ������ ���������� ����

function  mapSetObjectPress(Obj : HObj; press: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetObjectPress(Obj : HObj) : integer ;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ����������/��������� ������ ����������� ������� ������� � ����
 // ������������� �������
 // splinetype - ��� ������� (SPLINETYPE_SMOOTH; SPLINETYPE_POINTS)
 // ��� ������ ��� ������ ��������� ������� ���������� ����

function  mapSetObjectSpline(Obj : HObj; splinetype : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapGetObjectSpline(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // ************************************************************
 //                                 *
 //         ������ ��������� (���������) ��'����               *
 //                                 *
 // ************************************************************

 // ��������� ����� ������������� ������������� � �������
 // Obj  - ������������� ������� ����� � ������
 // ��� ������ ���������� ����

function  mapSemanticAmount(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� �������� ������������� �������������� �������
 // �������� ������������� � ���������� ��� ��� ��������������
 // Obj   - ������������� ������� ����� � ������
 // number - ���������������� ����� �������������� (c 1);
 // value  - ����� ���������� ������;
 // maxsize   - ������������ ����� ������
 // ��� ������ ���������� ����

function mapSemanticValue(Obj:HObj;number:integer;value:GTKPChar;
                          maxsize:integer):integer; // 15/10/2009
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ��������� - �� �������� �� ��������� ������� UNICODE?       // 24/03/11
 // info   - ������������� ������� ����� � ������
 // number - ���������������� ����� �������������� (c 1)
 // ���� ��������� � ��������� Unicode - ���������� ��������� ��������


function  IsSemanticUnicode(Obj : HObj; number : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapIsSemanticUnicode(Obj : HObj; number : integer) : integer;      // 20130128 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // ��������� �������� ������������� �������������� ������� � UNICODE
 // �������� ������������� � ���������� ��� ��� ��������������
 // Obj   - ������������� ������� ����� � ������
 // number - ���������������� ����� �������������� (c 1);
 // value  - ����� ���������� ������;
 // maxsize   - ������������ ����� ������ � ������
 // ��� ������ ���������� ����

function  mapSemanticValueUn(Obj : HObj; number : integer; value : PWCHAR; maxsize : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapSemanticValueUnicode(Obj : HObj; number : integer; value : PWCHAR; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� �������� ������������� �������������� �������
 // � ���� ����� � ��������� ������ ������� ��������
 // Obj    - ������������� ������� ����� � ������
 // number  - ���������������� ����� �������������� (c 1)
 // ���� �������� ��������� �� ����� ���� �������������
 // � ��������� ���� ��� �� ������� - ���������� ����

function mapSemanticDoubleValue(Obj:HObj;number:integer):double;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;
  
 // ��������� �������� ������������� �������������� �������   
 // � ���� ������ �����
 // number  - ���������������� ����� ��������������
 // ���� �������� ��������� �� ����� ���� �������������
 // � ��������� ���� ��� - ���������� ����

function mapSemanticLongValue(info : HOBJ; number : integer) : double;       // 20130128 Korolev
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


 // ��������� �������� ������������� �������������� �������
 // � ���������� ��������������� ����
 // ��������: ��� ��������� "���������" �������� "5"
 // ��������� �� "�����"
 // info    - ������������� ������� ����� � ������
 // number  - ���������������� ����� �������������� (c 1);
 // place   - ����� ���������� ������;
 // maxsize - ������������ ����� ������ � ������
 // ��� ������ ���������� ����

function mapSemanticValueName(Obj:HObj;number:integer;place:GTKPChar;
                              maxsize:integer):integer; // 15/10/2009
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function mapSemanticValueNameUnicode(Obj: HObj;number: integer;place: PWCHAR;
                                     maxsize: integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function mapSemanticValueNameUn(Obj: HObj;number: integer;place: PWCHAR;          // 03/04/2014
                                     maxsize: integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;



 // ��������� �������� ������������� �������������� �������
 // � ���������� ��������������� ���� � ����������� �������
 // ��������� � ���������� ����
 // ��������: ��� ��������� "���������" �������� "5"
 // ��������� �� "�����";
 // �� ��������� "������" �������� "205;5" ���������
 // �� "205;5 �"
 // Obj    - ������������� ������� ����� � ������
 // number  - ���������������� ����� �������������� (c 1);
 // place   - ����� ���������� ������;
 // maxsize - ������������ ����� ������
 // ��� ������ ���������� ����

function mapSemanticValueFullName(Obj:HObj;number:integer;place:GTKPChar;
                                  maxsize:integer):integer; // 15/10/2009
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 

function  mapSemanticValueFullNameUn(Obj : HObj; number : integer; 
 	Place : PWCHAR; MaxSize :integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� �������� ������������� �������������� �������
 // �������� ������������� � ���������� ���
 // Obj    - ������������� ������� ����� � ������
 // code    - ��� ��������������;��� ������� ������ ��������;
 // place   - ����� ���������� ������;
 // maxsize - ������������ ����� ������
 // number  - ���������������� ����� ���������� ��������;
 //  �� ����� ����������������� ������ �������������� !
 //  �������� : ��� code ����� 3-� � 6-� ��������������;
 //             �������������� ��� ��� number = 1 � 2;
 //             � ��� number = 3  - ��� �������� ����� ����.
 // ��� ������ ���������� ����;
 // ��� �������� ���������� - ���������������� �����
 // ��������� ��������������

function mapSemanticCodeValue(Obj:HObj;code:integer;place:GTKPChar;
                              maxsize:integer;number:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function mapSemanticCodeValueUn(Obj:HObj;code:integer;place:PWCHAR;
                              maxsize:integer;number:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


 // ��������� �������� ������������� �������������� �������
 // � ���� ����� � ��������� ������ ������� ��������
 // Obj    - ������������� ������� ����� � ������
 // code    - ��� ��������������;��� ������� ������ ��������;
 // number  - ���������������� ����� ���������� ��������;
 //  �� ����� ����������������� ������ �������������� !
 //  �������� : ��� code ����� 3-� � 6-� ��������������;
 //             �������������� ��� ��� number = 1 � 2;
 //             � ��� number = 3  - ��� �������� ����� ����.
 // ���� �������� ��������� �� ����� ���� �������������
 // � ��������� ���� ��� �� ������� - ���������� ����

function  mapSemanticCodeDoubleValue(Obj : HObj; code, number: integer):double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� �������� ������������� �������������� �������
 // � ���������� ��������������� ����
 // Obj    - ������������� ������� ����� � ������
 // code    - ��� ��������������;��� ������� ������ ��������;
 // place   - ����� ���������� ������;
 // maxsize - ������������ ����� ������
 // number  - ���������������� ����� ���������� ��������;
 //  �� ����� ����������������� ������ �������������� !
 //  �������� : ��� code ����� 3-� � 6-� ��������������;
 //             �������������� ��� ��� number = 1 � 2;
 //             � ��� number = 3  - ��� �������� ����� ����.
 // ��� ������ ���������� ����;
 // ��� �������� ���������� - ���������������� �����
 // ��������� ��������������

function  mapSemanticCodeValueName(Obj : HObj; code: integer; place : GTKPChar; maxsize, number : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


function  mapSemanticCodeValueNameUn(info : HOBJ; code : integer;
                                     place : PWCHAR; maxsize, number : integer) : integer;    // 20130128 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;									

 // ��������� �������� ������������� �������������� �������
 // Obj    - ������������� ������� ����� � ������
 // number  - ���������������� ����� �������������� (c 1)
 // name - ����� ������ ��� ���������� �������
 // size - ������ ������ � ������ 
 // ��� ������ ���������� ����


function mapSemanticName(Obj:HObj;number:integer):GTKPChar;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;
function  mapSemanticNameEx(Obj : HObj;number : integer; Name : GTKPChar;size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapSemanticNameUn(Obj : HObj;number : integer; Name : PWCHAR;size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ��� ������������� �������������� �������
 // Obj     - ������������� ������� ����� � ������
 // number  - ���������������� ����� �������������� (c 1)
 // ��� ������ ���������� ����

function mapSemanticCode(Obj : HObj; number : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ��������� ���������������� ����� ���� �������������
 // �������������� ������� (c 1)                                   // 20/07/06
 // Obj     - ������������� ������� ����� � ������
 // code    - ��� ������������� �������������� � ��������������
 // ��� ������ ���������� ����

function  mapSemanticNumber(Obj : HObj; code : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ����� ������ ������ ��� ���������� ��������� � ������ �������
 // ���� ��������� � ���������� ���������������                      // 27/02/09
 // ��� ������ ������ ������ ����� 255 �������� ��� ������� �� ���������
 // �������������; ���������� ����� ������ ����� 255 ��������
 // ������ �������������� ����� ���� ������ � ����������� �����������
 // �������������� (������� ��������� �������� ��� ������ �������;
 // ��. � RSCSEMANTICEX ���� Reply)
 // Obj     - ������������� ������� ����� � ������
 // number  - ���������������� ����� �������������� (c 1)
 // ���� ������ ����� ��������� � UNICODE, �� ������ ������ ����� �������� �� 2
 // ��� ������ ���������� ����

function  mapSemanticStringLength(Obj : HObj;number : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ����� ������ ��� ���������� ��������� � ������ �������
 // ���� ��������� � ���������� ���������������                      // 27/02/09
 // ��� ������ ������ (mapAppendSemantic) ������ ����� 255 ��������
 // ��� ������� �� ��������� �������������; ���������� ����� ������
 // ����� 255 ��������.
 // ���� ��� �������������� ��������� ��������� ������ ������ ����� 255 ��������;
 // �� ������ ������� ����� ������ ����� mapAppendSemantic (��� ���������������
 // �������� ������� ������ ����� ������������� ���������).
 // ������ �������������� ����� ���� ������ � ����������� �����������
 // �������������� (������� ��������� �������� ��� ������ �������;
 // ��. � RSCSEMANTICEX ���� Reply)
 // info    - ������������� ������� ����� � ������
 // number  - ���������������� ����� �������������� (c 1)
 // value   - ����� ������; ���� ����� �������� ������ � ��������� ANSI
 // length  - ������������ ������ �������� ������ ��� ��������
 // ��� ������ ���������� ����; ����� - ����� ���������� ������

function  mapSemanticString(Obj : HObj;number : integer; value : GTKPChar;
                length: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapSemanticStringUnicode(Obj : HObj;number : integer; value : PWChar;
                                  length: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ************************************************************
 //                                 *
 //      �������������� ��������� (���������) ��'����          *
 //                                 *
 // ************************************************************

 // ��������� ������� �� ���������, ������� ��� ����� ����      // 27/10/11
 // ��������� ��� ������� �������
 // ��������� ������� ���������� � �������� �������������� ��������� ������� !
 // (��������� �������������� ����� ������������� ������ ���� ���)
 // ��� ������ ���������� ����

function mapIsAvailableSemantic(Obj : HObj) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ��������� ���������� ����� ��������; ������� ��� ����� ����
 // ��������� ��� ������� �������
 // ���������� � �������� �������������� ��������� ������� !
 // (��������� �������������� ����� ������������� ������ ���� ���)
 // Obj     - ������������� ������� ����� � ������
 // ��� ������ ���������� ����

function mapAvailableSemanticCount(Obj : HObj) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


 // ��������� ������� ��� ��������� ��������� �� ������
 // �� ����������������� ������ ��������� ��������
 // ���������� � �������� �������������� ��������� ������� !
 // Obj    - ������������� ������� ����� � ������
 // number - ���������������� ����� ��������� �������� (1;2;3...)
 // ��� ������ ���������� ����

function mapAvailableSemanticCode(Obj : HObj; number : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ��������� ������ ������� ����� ��������� �������� �� ������        // 27/10/11
 // ���������� � �������� �������������� ��������� ������� !
 // info    - ������������� ������� ����� � ������
 // buffer  - ��������� �� ������� ������ ��� ���������� ������ ����� ��������
 // count   - ������������ ����� ��������� � ������ (������ ������ �������� �� 4)
 // ��� ������ ���������� ����

function mapAvailableSemanticList(Obj : HObj; var buffer : integer; count : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


 // �������� ����� �������������� � ��������� �������
 // Obj     - ������������� ������� ����� � ������
 // excode  - ������� ��� ��������������
 // place   - ����� ������;���������� ����� ��������
 //           � ���������� ����; ����� � ��������� ������ ����� �����
 //           ������������ ������ ������ ����� "."
 // maxsize - ������������ ����� ������
 // (�������� �������� ����� ����������������� � �������� ���)
 // ���� ����� ��������� ���� � ��� �� ����������� - �������� ����������  // 10/07/06
 // ��� ������ ���������� ����;
 // ��� �������� ���������� - ���������������� �����
 // ��������� ��������������
 // ��� ������ ���������� ����

function mapAppendSemantic(Obj : HObj; excode : integer; place : GTKPChar;
                           maxsize : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 

 // �������� ����� �������������� � ��������� ������� � ���� UNICODE-������  // 24/03/11
 // info    - ������������� ������� ����� � ������
 // code    - ������� ��� ��������������
 // value   - ����� ������,���������� ����� ��������
 //           � ���������� ���� UNICODE UTF-16,
 // size    - ������������ ����� ������
 // ���� ����� �������� ��������� ������� �� 0x0001 �� 0x007E
 // ��� ��������� (0x0400 - 0x045F) � �� ����������
 // ����������� ������� Windows (OEM 866 ��� 1251), �� �����
 // ������������� ��������� � ANSI,
 // ����� ����� �������� ���������� � UTF-16
 // �������� �������� ����� ����������������� � �������� ���
 // ���� ����� ��������� ���� � ��� �� ����������� - �������� ����������
 // ��� ������ ���������� ����,
 // ��� �������� ���������� - ���������������� ����� ��������� ��������������
 // ��� ������ ���������� ����

function  mapAppendSemanticUnicode(Obj : HObj; code: integer; const value : PWCHAR; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������� - �������� mapAppendSemanticUnicode

function  mapAppendSemanticUn(Obj : HObj; code: integer; const value : PWCHAR; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� ����� �������������� � ��������� �������;   // 12/02/06
 // Obj     - ������������� ������� ����� � ������
 // code    - ������� ��� ��������������
 // value   - �������� � ���� ����� ������� ��������
 // ��� ������ ���������� ����;
 // ��� �������� ���������� - ���������������� �����
 // ��������� ��������������

function  mapAppendSemanticDouble(Obj : HObj; code : integer; value : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� ����� �������������� � ��������� �������,   // 12/12/12
 // info    - ������������� ������� ����� � ������
 // code    - ������� ��� ��������������
 // value   - �������� � ���� ������ �����
 // ��� ������ ���������� ����,
 // ��� �������� ���������� - ���������������� �����
 // ��������� ��������������

function  mapAppendSemanticLong(info : HOBJ; code, value : integer) : integer;     // 20130128 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������� ������������� �������������� �������
 // Obj     - ������������� ������� ����� � ������
 // number  - ���������������� ����� ��������������,
 //           ���� ����� ����� "-1", ��������� ��� ��������������
 // ��� ������ ���������� ����

function mapDeleteSemantic(Obj : HObj; number : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;




 // �������� �������� ���� ������������� �������������� �������
 // Obj     - ������������� ������� ����� � ������
 // number  - ���������������� ����� ��������������
 // excode  - ������� ��� ��������������
 // ��� ������ ���������� ����;
 // ����� - ���������� ��� ���������

function mapSetSemanticCode(Obj:HObj;number:integer;
                            excode:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // �������� �������� ������������� �������������� �������
 // Obj     - ������������� ������� ����� � ������
 // number  - ���������������� ����� ��������������;
 // place   - ����� ������; ���������� ����� ��������
 //           � ���������� ����; ��� ��������� ���� "�������������"
 //           ���������� ��� �������� � ���� ����� � ���������� ����;
 //           �� �� - ��� ���� "������ �� ������".
 // maxsize - ����� ������������ ������ (��� ��������)
 // ��� ������ ���������� ����

function mapSetSemanticValue(Obj:HObj;number:integer;place:GTKPChar;
                             maxsize:integer):GTKPChar;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


 // �������� �������� ������������� �������������� ������� � ��������� UNICODE
 // Obj     - ������������� ������� ����� � ������
 // number  - ���������������� ����� ��������������;
 // place   - ����� ������; ���������� ����� ��������
 //           � UNICODE. ��� ��������� ���� "�������������"
 //           ���������� ��� �������� � ���� ����� � ���������� ����;
 //           �� �� - ��� ���� "������ �� ������".
 // maxsize - ����� ������������ ������ (��� ��������)
 // ���� ����� �������� ��������� ������� �� 0x0001 �� 0x007E
 // ��� ��������� (0x0400 - 0x045F) � �� ����������
 // ����������� ������� Windows (OEM 866 ��� 1251), �� �����
 // ������������� ��������� � ANSI,
 // ����� ����� �������� ���������� � UTF-16
 // �������� �������� ����� ����������������� � �������� ���
 // ��� ������ ���������� ����

function  mapSetSemanticValueUn(Obj : HObj;number : integer; place : PWCHAR; maxsize: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapSetSemanticValueUnicode(Obj : HObj;number : integer; const place : PWCHAR; maxsize: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� �������� ������� ��� ��������� �������������
 // �������������
 // Obj    - ������������� ������� ����� � ������
 // ���� ��� ������� �� ��������� ���������� 0

function  mapRedefineObject(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // ��������� ��������� �������� ������������� ��������������
 // �� ���� ���������
 // Obj     - ������������� ������� ����� � ������
 // code    - ������� ��� ���������
 // ��� ������ ���������� ����


function mapSemanticDescribeEx(Obj : HObj; var semtype : TRSCSEMANTICEX;
                               code : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ��������� ���������� ������� � ��������������
 // ��������� �� ���� ���������
 // Obj  - ������������� ������� ����� � ������
 // code - ��� ��������� (��������� ���� "TCODE")
 // ��� ������ ���������� ����

function mapSemanticClassificatorCount(Obj : HObj; code : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


 // ��������� �������� �������� �������������� ��
 // �������������� ��������� �� ���� �
 // ����������������� ������ � ��������������
 // Obj    - ������������� ������� ����� � ������
 // number - ���������������� ����� � ��������������(1;2;3...)
 // code - ��� ���������
 // name - ����� ������ ��� ���������� �������
 // size - ������ ������
 // ��� ������ ���������� ����� ������ ������;
 // ��� �������� ���������� - ����� ������

function mapSemanticClassificatorName(Obj:HObj;code:integer;
                                      number:integer):GTKPChar;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function  mapSemanticClassificatorNameEx(Obj : HObj;code, number : integer;
                  Name : GTKPChar;size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapSemanticClassificatorNameUn(Obj : HObj;code, number : integer;
                  Name : PWChar;size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ��� �������� �������������� ��
 // �������������� ��������� �� ���� �
 // ����������������� ������ � ��������������
 // Obj    - ������������� ������� ����� � ������
 // number - ���������������� ����� � ��������������(1;2;3...)
 // code   - ������� ��� ���������
 // ��� ������ ���������� ����

function mapSemanticClassificatorCode(Obj : HObj; code : integer;
                                      number : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


 // ***********************************************************************
 //      �������������� ��������� (���������) ��'���� -                   *
 //  �������� ���������� ������ �� ��������������� ������                 *
 //      � ������ �������� ��������� �������                              *
 // ������ ������: ����� %# ���� ����� ���������; �� ��� � []- ��������;  *
 // ������� ����� ��������� � ������ ��� ���������� ��������� ���������   *
 // ��������� ����� � ������������ �����.                                 *
 // ������:������� ������ - "��� N %#45[���] ����. %#3[�� ���������]"     *
 //     ��������� �� ��������� ��������� ��� ����������� �������          *
 //     "��� N 5 ����. �� ���������" ��� "��� N 7-a ����. �����"          *
 //      ���  "��� N ��� ����. �� ���������"                              *
 // ***********************************************************************

 // ������ ���������������  ������ �� �����
 // value - ������� ������;
 // ������� ������ �� ����� 256.
 // ��� �������� �������������� ��� ������ - ���������� 0;
 // ����� ������������� ������� � ������ (HFORMULA)
 // ��� ������� ����������� � ������ �� �������������
 // �������������� HFORMULA ��������� ����� ������� mapFreeFormula()
 // 19/01/07

function  mapTakeStringToPieces(value : GTKPChar) : HFORMULA;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapTakeStringToPiecesUn(value : PWChar) : HFORMULA;      // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������� ���������� ������ �� �������������e HFORMULA
 // � ������ �������� �������
 // Obj - ������������� ������� ����� � ������
 // str - ������ ��� ������ ����������
 // size   - ������ ������
 // ��� ������ ���������� 0
 // 19/01/07

function  mapBuildFormulaString(formula : HFORMULA; Obj : HObj;
   str : GTKPChar;size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapBuildFormulaStringUn(formula : HFORMULA; Obj : HObj;
   str : PWChar;size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ����������  ������������� HFORMULA
procedure mapFreeFormula(formula : HFORMULA);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ************************************************************
 //                                 *
 //         ������ ������� (���������) ��'����                 *
 //                                 *
 // ************************************************************

 // ��������� ����������� �������/����������
 // info   - ������������� ������� ����� � ������
 // number - ����� ���������� (��� ������� - ����� ����)
 // ����������:  0 - �� �������; �� 0 - �������

function  mapGetExclusiveSubject(Obj : HObj; number : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������ ��������� ������� (� ������)
 // info   - ������������� ������� ����� � ������
 // dframe - �������� ������� ������� � ������
 // ��� ������ ���������� ����

function mapObjectFrame(Obj:HObj;var dframe:TMAPDFRAME):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ������/�������� ��������� ����������� ����� ������� (� ������)
 // info   - ������������� ������� ����� � ������
 // frame - �������� ����������� ������� � ������
 // force  - ������� ��������������� ��������� ��������� (���������� ����������;
 //          ���� ������ ��������������; �� �� ������� �� �����)
 // ��� ������ ���������� ����

function mapObjectViewFrameEx(Obj:HObj;var frame:TMAPDFRAME; force:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 

function mapObjectViewFrame(Obj:HObj;var dframe:TMAPDFRAME):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ������ ����� ����� ������� �������/����������
 // Obj     - ������������� ������� ����� � ������
 // subject - ����� ���������� (���� = 0; �������������� ������)
 // ��� ������ ���������� ����


function mapPointCount(Obj:HObj;subject:integer):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ������ ����� ��������� ������ (����������� + 1)
 // Obj     - ������������� ������� ����� � ������
 // ���� ����������� ��� - ���������� 1 (������ ������)
 // ��� ������ ���������� ����

function  mapPolyCount(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ���������� �����
 // number - ����� ����� (���������� � 1)
 // subject - ����� ���������� (���� = 0, �������������� ������)
 // ��� ������ ���������� ����

function mapGetPlanePoint(info : HOBJ; point : PDOUBLEPOINT; // 01/12/2010
                           number : integer; subject : integer): integer; 
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������ ���������� ����� ������� � ������������� �������
 // � ������ �� ���������
 // obj    - ������������� ������� ����� � ������
 // number - ����� ����� (���������� � 1)
 // subject - ����� ���������� (���� = 0; �������������� ������)
 // ������� ����������� ����� ����� mapGetPlanePoint()
 // ��� ������ ���������� ����

function mapXPlane(Obj:HObj;number:integer= 1;subject:integer= 0):double;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;
function  mapXPlaneEx(Obj : HObj; number,subject : integer;var X : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapYPlane(Obj:HObj;number:integer= 1;subject:integer= 0):double;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 
function  mapYPlaneEx(Obj : HObj; number,subject : integer;var Y : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapHPlane(Obj:HObj;number:integer= 1;subject:integer= 0):double;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 
function  mapHPlaneEx(Obj : HObj; number, subject : integer; var H : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������������� ���������� ����� � �������� � ������� ���������
 // number - ����� ����� (���������� � 1)
 // subject - ����� ���������� (���� = 0, �������������� ������)
 // ��� ������ ���������� ����
function  mapGetGeoPoint(info : HOBJ; var point : TDOUBLEPOINT; // 15/03/2013
                          number : integer; subject : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������������� ���������� ����� � �������� �� ���������� WGS-84
 // number - ����� ����� (���������� � 1)
 // subject - ����� ���������� (���� = 0, �������������� ������)
 // ��� ������ ���������� ����

function  mapGetGeoPointWGS84(info : HOBJ; var point : TDOUBLEPOINT; // 15/03/2013
                          number : integer; subject : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetGeoPointWGS843D(info : HOBJ; var point : TDOUBLEPOINT;
                          var heigth : double;
                          number : integer; subject : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������ ������������� ��������� ����� �������      // 11/01/10
 // � �������� (BGeo - ������, LGeo - �������)
 // number - ����� ����� (���������� � 1)
 // subject - ����� ���������� (���� = 0, �������������� ������)
 // ������� ����������� ����� ����� mapGetGeoPoint()
 // ��� ������ ���������� ����

function mapBGeo(info : HOBJ; number : integer = 1; subject : integer = 0): double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapLGeo(info : HOBJ; number : integer = 1; subject : integer = 0): double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // ������ - ����� �� ������ 3-�������� �������
 // Obj    - ������������� ������� ����� � ������
 // ���� ��; ���������� ��������� ��������

function  mapIsObject3D(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ��� ������ � ������� ����������
  // ������� ������ ����� ���� � �� ������
  // (0 - ����������; ����� - �������������)
  
function  mapGetHeightType(Obj : HObj) : integer;   // 27/01/06
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ��� ������ � ������� ����������
  // �������� ������ ����� ���� ����������� �������
  // (0 - ����������; ����� - �������������)
  // ������� � ������������� ������� �� ������ �� ���������� ������� �����
  // �������� - ������������ �������� � ���������

function  mapSetHeightType(Obj : HObj; htype : integer) : integer;    // 27/01/06

{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ************************************************************
 //                                 *
 //      �������������� ������� (���������) ��'����            *
 //                                 *
 // ************************************************************

 // �������� � ����� ������� ������� �����
 // info    - ������������� ������� ����� � ������
 // x;y     - ���������� ����� � ������
 // subject - ����� ���������� (���� = 0; �������������� ������)
 // �������� ��������� ������ � ������ �� ���������
 // ��� ��������� ���������� � ���������� �����
 // ��������� ������� SetHPlane(...)
 // ��� ������ ���������� ����

function mapAppendPointPlane(Obj : HObj; X : double; Y : double;
                             Subject : integer= 0) : integer;

  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // �������� � ����� ������� ������� �����
 // info    - ������������� ������� ����� � ������
 // x,y     - ���������� ����� � ������
 // h       - ������ (���������� ��� �������������) � ������
 // subject - ����� ���������� (���� = 0, �������������� ������)
 // ��� ������ ������������ ��������� mapGetHeightType � mapSetHeightType
 // ��� ������ ���������� ����

function mapAppendPointPlane3D(Obj : HObj; X : double; Y : double; H: double;
                             Subject : integer= 0) : integer;

  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ������� �������� ����� �������
 // Obj     - ������������� ������� ����� � ������
 // number  - ����� ����� (� 1)
 // subject - ����� ���������� (���� = 0; �������������� ������)
 // ��� ������ ���������� ����

function mapDeletePointPlane(Obj : HObj; number : integer;
                             subject : integer= 0) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function mapDeletePoint(Obj : HObj; number : integer;         // 03/04/2014
                             subject : integer= 0) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


 // �������� �� ������� ���������� �����                             // 22/07/10
 // precision - �������� ����������� �������� ��������� � ������ �� ���������
 // height    - ������� ����� ���������� ������� (� ���� ������ ��� ����������
 //             ����� � ������ ������� ��������� �������)
 // ��� ������ ���������� 0

function mapDeleteEqualPoint(info : HOBJ; precision : double; height : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ������� �� �������/���������� ������� � ����� number1 �� ����� number2  //18/12/09
 // subject - ����� ���������� (���� = 0, �������������� ������)
 // ������� ���������� ����� ��������� �� ����������
 // ��� �������� ���� ����� �������/���������� ��������� ������ �������,
 // ������/��������� �� ���������

function mapDeletePartObject(info : HOBJ; number1 : integer;
                             number2 : integer; subject : integer = 0) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // �������� � ������� ������� �����
 // Obj     - ������������� ������� ����� � ������
 // x;y     - ���������� ����� � ������
 // number  - ����� ����� �� ������� ����� ��������� ����� �����
 // subject - ����� ���������� (���� = 0; �������������� ������)
 // �������� ��������� ������ � ������ �� ���������
 // ��� �������� ���������� � ���������� �����
 // ��������� ������� mapSetHPlane(...)
 // ��� ������ ���������� ����

function mapInsertPointPlane(Obj : HObj; X : double; Y : double; number : integer;
                             subject : integer= 0) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // �������� ���������� ����� �������
 // �������� ��������� ������ � ������ �� ���������
 // Obj     - ������������� ������� ����� � ������
 // x;y     - ���������� ����� � ������
 // number  - ����� ����� (c 1)
 // subject - ����� ���������� (���� = 0; �������������� ������)
 // ��� ������ ���������� ����

function mapUpdatePointPlane(Obj : HObj; X : double; Y : double; number : integer;
                             subject : integer= 0) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function mapUpdatePointPlane3D(Obj : HObj; X : double; Y : double; H : double; number : integer;
                             subject : integer= 0) : integer;        // 03/04/2014
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;



 // �������� � ����� ������� ������� �����                           // 11/05/07
 // Obj     - ������������� ������� ����� � ������
 // b;l     - ���������� ����� � ��������
 // subject - ����� ���������� (���� = 0; �������������� ������)
 // �������� ��������� ������ ��������������� ������� ���������;
 // �������� � ���������� �����
 // ��� ������ ���������� ����

function  mapAppendPointGeo(Obj : HObj; B,L : double; subject : integer = 0) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� � ����� ������� ������� �����                           // 19/05/10
 // subject - ����� ���������� (���� = 0, �������������� ������)
 // �������� ��������� ������ � �������� �� ���������� WGS-84
 // ��� ������ ���������� ����


function  mapAppendPointGeoWGS84(Obj : HObj; B,L : double; subject : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapAppendPointGeoWGS843D(Obj : HObj; B,L, H : double; subject : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� � ������� ������� �����                                 // 11/05/07
 // Obj     - ������������� ������� ����� � ������
 // b;l     - ���������� ����� � ��������
 // number  - ����� ����� �� ������� ����� ��������� ����� �����
 // subject - ����� ���������� (���� = 0; �������������� ������)
 // �������� ��������� ������ ��������������� ������� ���������;
 // �������� � ���������� �����
 // ��� ��������� ���������� � ���������� �����
 // ��������� ������� HPlane(...)
 // ��� ������ ���������� ����

function  mapInsertPointGeo(Obj : HObj; B,L : double;
 number : integer; subject : integer = 0) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� ���������� ����� �������                                // 11/05/07
 // Obj     - ������������� ������� ����� � ������
 // b;l     - ���������� ����� � ��������
 // number  - ����� ����������� �����
 // subject - ����� ���������� (���� = 0; �������������� ������)
 // �������� ��������� ������ ��������������� ������� ���������;
 // �������� � ���������� �����
 // ��� ������ ���������� ����

function  mapUpdatePointGeo(Obj : HObj; B,L : double; 
 number : integer; subject : integer = 0) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapUpdatePointGeo3D(Obj : HObj; B,L,H: double; 
  number : integer; subject: integer = 0) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� ���������� ����� ����� ������� � ������� ������� �
 // � ���� �������� �����; ������� ����� �����
 // ��������� ����������� ����� ������ mapCommitObject()
 // ��� mapCommitWithPlace()
 // �������� ��������� ������ � ������ �� ���������
 // Obj     - ������������� ������� ����� � ������
 // x;y;h   - ���������� ����� � ������
 // number  - ����� ����� (c 1)
 // subject - ����� ���������� (���� = 0; �������������� ������)
 // ��� ������ ���������� ����

function  mapUpdatePointPlaneInMap(Obj : HObj; X,Y : double;
  number : integer; const subject: integer = 0) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapUpdatePointPlane3DInMap(Obj : HObj; X,Y,H : double;
  number : integer; subject: integer =0): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� ���������� ����� ����� ������� � ������� ������� �
 // � ���� �������� ������ ����;  ������� ����� �����
 // ��������� ����������� ����� ������ mapCommitObject()
 // ��� mapCommitWithPlace()
 // �������� ��������� ������ � ������ �� ���������
 // Obj     - ������������� ������� ����� � ������
 // x;y;h   - ���������� ����� � ������
 // number  - ����� �����
 // subject - ����� ���������� (���� = 0; �������������� ������)
 // ��� ������ ���������� ����

function  mapUpdatePointPlaneInLayer(Obj : HObj; X,Y : double;
  number : integer; subject: integer = 0) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapUpdatePointPlane3DInLayer(Obj : HObj; X,Y,H : double;
  number : integer; subject: integer =0) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� ���������� ����� �������/����������
 // � ������������� ������� � ������ �� ���������
 // Obj     - ������������� ������� ����� � ������
 // x;y;h   - ���������� ����� � ������
 // number  - ����� ����� (c 1)
 // subject - ����� ���������� (���� = 0; �������������� ������)
 // ��� ������ ���������� ����


function mapSetXPlane(Obj : HObj; X : double; number : integer;
                      subject : integer= 0) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


function mapSetYPlane(Obj : HObj; Y : double; number : integer;
                      subject : integer= 0) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


function mapSetHPlane(Obj : HObj; H : double; number : integer;
                      subject : integer= 0) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ������� ���������� ���������� � ������ �������
 // � ����� ������ ����������� ���������� ����������
 // ����������� �������� �� ����� ������ � ����� ������
 // ����� ����� = 0; ����� ���������� = ������������ ����� + 1
 // (��� �������; ������������ ���������� ����� ����������)
 // Obj     - ������������� ������� ����� � ������
 // ��� ������ ���������� ����

function mapCreateSubject(Obj:HObj):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


 // ������� ��������� � ������ �������
 // Obj    - ������������� ������� ����� � ������
 // number - ����� ���������� ���������� (� 0);
 // ���� ����� (-1); �� ��������� ��� ������� ������� !
 // ������� ���������� ������ ����� �������
 // ��� ������ ���������� ����

function mapDeleteSubject(Obj : HObj; number : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 

 // �������� ��� ���������� ������� ������� �� ��������
 // �������� (delta) � ������
 // Obj    - ������������� ������� ����� � ������
 // ��� ������ ���������� 0

function mapRelocateObjectPlane(Obj:HOBJ; delta: Pointer): integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // �������� ��� ���������� ������� ������� �� ��������
 // �������� (delta) � ������������ � ���������� place
 // hmap  - ������������� �������� ������
 // info  - ������������� ������� ����� � ������
 // place - ����������� ������� ��������� (PP_MAP) // 15/02/2010
 // ��� ������ ���������� 0

function  mapRelocateObject(Map : HMap; Obj : HObj; delta : PDoublePoint;
    place: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� ����������� ���������� ����������
 // Obj    - ������������� ������� ����� � ������
 // number - ����� ���������� (� 0);
 // ��� ������ ���������� ����; ����� - ����� ��������
 // (OD_RIGHT;OD_LEFT;... - ��. Maptype.h)

function  mapChangeSubjectDirect(Obj : HObj; number : integer) : integer;  // 31/01/06
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� ����������� ���������� �������
 // Obj   - ������������� ������� ����� � ������
 // ��� ������ ���������� ����; ����� - ����� ��������
 // (OD_RIGHT;OD_LEFT;... - ��. Maptype.h)

function  mapChangeObjectDirect(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������������� ������ (���������); ��������� ������ �������� �����
 // number - ����� �����
 // subject - ����� ���������� (���� = 0; �������������� ������)
 // ��� ������ ���������� ����

function  mapSetFirstPoint(Obj : HObj; number, subject : integer) : integer;     //31/01/05
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� ���������� �������
 // Obj  - ������������� ������� ����� � ������
 // precision - �������� � ������
 // �������: 1. ������� ����� �������;
 //          2. ����������� ���������� < 2 �����;
 //          3. ��������� ���������� < 4 �����;
 //          4. ����� �������; ������� � �������� ������� ������
 //             �� ���������� precision �� ������.
 // ������ �� ������� ������� !!!
 // ���������� ����� ����� ����� �������
 // ��� ������� ����������:
 //    0 - ������ ���������
 //   -1 - ������ ������� �� ����� �����
 //   -2 - ������ ������� �� ���� ���������� �����
 //   -3 - ����� ����� ���������� ������� ������� ����� 3
 //  -10 - ����� ����� ������� ��������� ����� ������ �������

function  mapLinearFilter(Obj : HObj; precision: double) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������� � ������ �������������� ������ � ���������
 // ��������� ����� �����; �������� ����������� ������
 // (����������� � �������� �������; ������� ����� �����;
 //  �������� ����� ����� �� �����������)
 // hMap      - ������������� �������� ������
 // Obj       - ����������� ������
 // precision - �������� � ������ �� ���������
 //             (����������� ���������� �� ����� �� ������;
 //              ����������� ���������� � ��������� �����)
 // ��� ������ ���������� 0

function  mapGeneralFilter(Map : HMap; Obj : HObj; precision : double): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ���� �������� ������ ����� ����� � ������
 // �������������� ������ � ��������� ��������� ���� �� �����
 // ��� �� ����� (����������� � �������� �������; ������� ����� �����;
 // �������� ����� ����� �� �����������)
 // Map      - ������������� �������� ������
 // Site     - ������������� �������������� �����
 // list      - ����� �����
 // precision - �������� � ������ �� ���������
 //             (����������� ���������� �� ����� �� ������;
 //              ����������� ���������� � ��������� �����)
 // wnd       - ������������� ����;������� ����� ����������
 //             (��� ������ ��������� ���������� ������������� � ����)
 // ������� �������� ��������� 0x590:
 // wparm : ������� ���������;
 // lparm : ���������� ������������ ��������.
 //   ����� ���������� = mapGetObjectCount(hMap; list)     - ��� �������� �����
 //   ����� ���������� = mapGetSiteObjectCount(hMap; hSit) - ��� ����������������
 // ��� ���������� �������� ������� ����� 0x590
 // ��� ������ ���������� 0

function  mapGeneralFilterInMap(Map : HMap; Site : HSite; list : integer; 
    	precision : double; Wnd : HWnd) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� ������� - ����������� ������� � ���� ��� �����������
 // ��� ������; ������� �������� ������
 // ����� ������ � ��������� ����� �������(����������) � ��� ��
 // ���������� (���������) ���� �������; ����������� ����� �������
 // (������� ��������� �������/����������).
 // Obj       - �������� ������� �������; �� �������� �������� ������
 // cashion   - �������� ������� ����������
 //             ����� ������� ����� ������� (1<= cashion <= 50)
 //             (������� ��������� �������/����������).
 //             ��� ������ cashion; ��� ������ ����������� ����
 // smooth    - ��������� ������ �������
 //             (����� ����� ����� ������ ������� smooth >= 3).
 //             ��� ������ smooth;��� ����� ��������� �����
 // precision - ����� (��������) ��� ���������� �����; ��� ���������������
 //             ����������� �������� ���������� �������� "-1".
 // ���� �������� ������ ���� 3-� ���������� (������);�� � �������
 // ����� ���� ������ (������������ ��� ����� �����).
 // ��� ������ ���������� ����

function  mapCashionSpline(Obj : HObj; cashion, smooth : integer; precision : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� ������� - ����������� ����������
 // ��� ������; ������� �������� ������
 // ����� ������ � ��������� ����� ���������� � ��� ��
 // ���������� (���������) ���� �������; ����������� ����� �������
 // (������� ��������� ����������).
 // Obj       - �������� ������� �������; �� ���������� �������� �������� ������
 // subject   - ����� ��������������� ���������� (0;1;2...)
 // cashion   - �������� ������� ����������
 //             ����� ������� ����� ������� (1<= cashion <= 50)
 //             (������� ��������� ����������).
 //             ��� ������ cashion; ��� ������ ����������� ����
 // smooth    - ��������� ������ �������
 //             (����� ����� ����� ������ ���������� smooth >= 3).
 //             ��� ������ smooth;��� ����� ��������� �����
 // precision - ����� (��������) ��� ���������� �����; ��� ���������������
 //             ����������� �������� ���������� �������� "-1".
 // ���� �������� ��������� ���� 3-� ���������� (������);�� � �������
 // ����� ���� ������

function  mapCashionSplineSubject(Obj : HObj; subject, cashion, smooth : integer;
     precision : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� ������� - �������� ������� � ���� ��� �����������
 // ��� ������; ������� �������� ����� ��� ����� ��������� �������
 // (������� ��������� �������) � ������� ���. �������� ������
 // ��� �� ������ � ������.
 // Obj       - �������� ������� �������; �� �������� �������� ������
 // press     - ������������ ���������
 //             ������ ������ ������� �� �������
 //             � ��������� �� ����� ������� ( >= 5 ).
 //             ��� ������ press; ��� ����� ������ �����
 //             ��������� �� ������� ������� (������� ���������
 //             �������/����������).
 // smooth    - ��������� ������ �������
 //             (����� ����� ����� ������ ������� smooth >= 3).
 //             ��� ������ smooth;��� ����� ��������� �����
 // precision - ����� (��������) ��� ���������� �����; ��� ���������������
 //             ����������� �������� ���������� �������� "-1".
 // ���� �������� ������ ���� 3-� ���������� (������);�� � �������
 // ����� ���� ������ (������������ ��� ����� �����).
 // ��� ������ ���������� ����

function  mapBendSpline(Obj : HObj; press, smooth : integer;
                         precision : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� ������� - �������� ����������
 // ��� ������; ������� ��������
 // ����� ��� ����� ��������� ���������� � ������� ���.
 // �������� ������ ��� �� ������ � ������.
 // Obj       - �������� ������� �������; �� �������� �������� ������
 // subject   - ����� ��������������� ���������� (0;1;2...)
 // press     - ������������ ���������
 //             ������ ������ ������� �� �������
 //             � ��������� �� ����� ������� ( >= 5 ).
 //             ��� ������ press; ��� ����� ������ �����
 //             ��������� �� ������� ������� (������� ���������
 //             �������/����������).
 // smooth    - ��������� ������ �������
 //             (����� ����� ����� ������ ������� smooth >= 3).
 //             ��� ������ smooth;��� ����� ��������� �����
 // precision - ����� (��������) ��� ���������� �����
 // ���� �������� ��������� ���� 3-� ���������� (������);�� � �������
 // ����� ���� ������

function  mapBendSplineSubject(Obj : HObj;subject, press, smooth : integer;
                               precision : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������������ ������
 // points - ������ ����� (���� �� ���������)
 // count  - ���������� �����
 // smooth - ������� ����������� (0..1; 0 - ������ �����; 1 - ���������� ������)
 // ��� ������ ���������� ����
function  mapSmoothingSpline1(var points : double; count : integer; smooth : double) : integer;   // 04/09/07
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������������ ������
 // points - ������ ����� (x; y)
 // count  - ���������� �����
 // smooth - ������� ����������� (0..1; 0 - ������ �����; 1 - ���������� ������)
 // ��� ������ ���������� ����
 function  mapSmoothingSpline2(points : PDoublePoint;
    count : integer; smooth : double): integer;    // 04/09/07
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������������ ������
 // points - ������ ����� (x; y; h)
 // count  - ���������� �����
 // smooth - ������� ����������� (0..1; 0 - ������ �����; 1 - ���������� ������)
 // ��� ������ ���������� ����
function  mapSmoothingSpline3(points : PXYHDouble;
   count : integer; smooth : double): integer;      // 04/09/07
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // C����������� ������ (2-� ��� 3-� ������ � ����������� �� ������� ������)
 // Obj     - ������������ ������
 // subject - ������������ ���������
 // smooth  - ������� ����������� (0..1; 0 - ������ �����; 1 - ���������� ������)
 // ��� ������ ���������� ����
function  mapSmoothingSplineSubject(Obj : HObj; subject: integer; smooth : double): integer;  // 04/09/07
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // C����������� ������ (2-� ��� 3-� ������ � ����������� �� ������� ������)
 // Obj    - ������������ ������
 // smooth - ������� ����������� (0..1; 0 - ������ �����; 1 - ���������� ������)
 // ��� ������ ���������� ����
function  mapSmoothingSplineObject(Obj : HObj; smooth : double): integer;              // 04/09/07
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ���� ��������� ������� � �������
 // � ����� point2 (� ������ �� ���������)
 // ����� point1 � point3 �������� ��� �����������
 // ����������� (� ������ �� ���������)
 // Map    - ������������� �������� ������
 // Obj    - ������������� ������� ����� � ������
 // radius - � ������ �� ���������
 // ������� �������� � info
 // ��� ������ ���������� 0

function  mapCreateArc(Map : HMap;Obj : HObj; point1,point2,point3 : PDoublePoint; radius:double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

// ���������� ���� ������ ������� ������� ��� ����������
// radius - ������ ����������� ���� (� ������ �� ���������) 
// Obj    - ������������� ����� �������; �� ������� �������� �������� ����.          
// � ���� ������ ����� �������� ������� ����������� ����; ������� 
// ����� ����� ��������� ��� ����� ��������� ������ 
 // subject - ����� ����������, ������ �������� ������ ����
 //           0 - ���� �������� ��� ��������� ������� ������� (��� ����� �����������)
// form - ����� ���� ���� 
// ��� ����������� �������� :
// = 0 - ���� ����� 
// = 1 - ������������� ���� 
// ��� ��������� �������� :                
// = 0 - "���������" ���� 
// = 1 - ������ ���� 
// ��� ������ ���������� 0 

function mapZoneObject(radius : double; Obj : HOBJ; subject, form : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ���������� ���� ������ ����������
 // radius  - ������ ����������� ���� (� ������ �� ���������)
 // info    - ������������� ����� �������, �� ������� �������� �������� ����.
 //           � ���� ������ ����� �������� ������� ����������� ����.
 // subject - ����� ����������, ������ �������� �������� ����
 // form    - ��� ���� 0 - ������, 1 - ������������
 // arcdist - ���������� ����� ������� �� ���� (� ������ �� ���������)
 //           ������������� radius / 15
 // cornerfactor - ����������� ��� ������� ������������ ����� ���� (������������� 3)
 // ���� ����� ������ ��� ����, �� ������� ���� ���������� �� ���������� �� ���� ��
 // ������� radius*cornerfactor ��� ���������� ������� �����
 // side    - ����������� ���������� ���� (1-������, 2-�����, 3-� ����� ������)
 // ��� ������ ���������� ����

function mapZoneObjectEx(radius : double; Obj : HOBJ; subject, form : integer;
                         arcdist : double; cornerfactor : double;
                         side : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ���������� ���� �������/������ �������/����������
 // ��� �������� � ����������� ����� ������ 1
 // (�� ����������� ����������; ���������� ����� �������� ������� �����������;
 //  �������� �� ����� �����������)
 // radius    - ������ ����������� ����
 //    ������������� �������� - ������ �������
 //    ������������� �������� - ������� �������
 // Obj       - ������� �������; ���� �������� ������
 // subject   - ����� ����������; ������ �������� ������ ����
 // ��� ������ ���������� ����                                      // 07/03/06

function mapInsideZoneObject(radius : double; Obj : HOBJ; subject : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ���������� ���� �������/������ ����������
 // radius  - ������ ����������� ���� (� ������ �� ���������)
 //           ������������� �������� - ������ �������
 //           ������������� �������� - ������� �������
 // info    - ������������� ����� �������, �� ������� �������� �������� ����.
 //           � ���� ������ ����� �������� ������� ����������� ����.
 // subject - ����� ����������, ������ �������� �������� ����
 // form    - ��� ���� 0 - ������, 1 - ������������
 // arcdist - ���������� ����� ������� �� ���� (� ������ �� ���������)
 //           ������������� radius / 15
 // cornerfactor - ����������� ��� ������� ������������ ����� ���� (������������� 3)
 // ���� ��� ���� ������, �� ������� ���� ���������� �� ���������� �� ���� ��
 // ������� radius*cornerfactor ��� ���������� ������� �����
 // ��� ������ ���������� ����

 function  mapInsideZoneObjectEx(radius : double; Obj : HOBJ; subject, form : integer;
                         arcdist : double; cornerfactor : double) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ���������� �������� ���� ������ ������� / ����������
 // (������ �� ������� �� ����������� ����������)
 // radius  - ������ ����������� ���� (� ������ �� ���������)
 // Obj     - ������� �������; �� �������� �������� ����
 // subject - ����� ����������; ������ �������� ������ ����
 // ��� ������ ���������� ����


function mapHalfZoneObject(radius : double; Obj : HOBJ; subject : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

// ���������� ���� ������ ��������� ������� ��������� ������������ 
// ������� ���� "������" (��� ����� �����������)                 // 28/06/12
// radius - ������ ����������� ���� (� ������ �� ���������)
// info   - ������� �������, �� �������� �������� ����.
//          �������� ������ ������� �� �����, ������ ������ ����� �������� ����.
//          ��������� ���� ������������ � ����
// � info ������������ ��������� ������ - ����
// ��� ������ ���������� 0

function mapZoneLineObject(map : HMAP; Obj : HOBJ; radius : double) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


 // ��������  ������� ������ � ��� ��� ���������� ���
 // ���������� ��� ��������� �������
 // Obj   - ������������� ������� ����� � ������
 // delta - ����� ��������� � �� �� �����
 // ���� ���������� ����� ������ � ��������� ������ ������ delta; ��
 // ������ ��������� ����� ����� ������
 // ���� ���������� ����� ������ � ��������� ������ ������ delta; ��
 // ����� ��������� ����� ��������� ������
 // ������� - 1 - ���������� ����������
 // ��� ������ ���������� 0

function mapAbrige(Obj : HObj; delta: double): integer;     //01/03/01
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // ���������� ����������
 // first - ���������� ������ ����� � ��������
 // second - ���������� ������ ����� � ��������
 // pArr - ����� ������� ��������� ����������� ����������;
 //         ������ ������� ����� count
 // count - ���������� ����� ��� ���������� ����������
 // ��� ������ ���������� ����                            // 10/03/04

function  mapOrthodrome(First, Second, pArr : PDoublePoint; count: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ���������� (���� �� ����������� �����, �������� ���������� ����������)   // 03/04/12
 // ����� ��������� �������
 // info   - ������������� ������� ����� � ������
 // first  - ���������� ������ ����� � �������� �� ������� ���������� ��������� (SetDocProjection)
 // second - ���������� ������ ����� � ��������
 // ��� ������� ����������� ����� ���� ����������� � ����� �� ����� 0,5 �������,
 // ��� ����� ���������� - �� ���� 10 ����������, ��� ������������ �����������
 // ���� � ����� � ��������� ������������ 1 ������
 // ��� ������ ���������� ����

function  mapOrthodromeObject(info : HOBJ; first, second : PDOUBLEPOINT) : integer;      // 20130128 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� �����������
 // first  - ���������� ������ ����� � ��������
 // second - ���������� ������ ����� � ��������
 // pArr   - ����� ������� ��������� ����������� �����������;
 //         ������ ������� ����� count
 // count  - ���������� ����� ��� ���������� �����������
 // ��� ������ ���������� ����
 // 10/03/04

function  mapLoxodrome(First, Second, pArr : PDoublePoint; count: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ���� ��������� �� ������� ����� � ���� ���������� �����������
 // hmap - ������������� �������� ��������� �����
 // RstName - ������ ��� ������
 // Zone - ��������� ���������� ���� (��.maptype.h)
 // ���������� ������������ ��� ������� �������� ������� �����
 // ��������� ������������ � ���� namerst
 // ���������� ����� ������ � �������
 // ��� ������ ���������� ����

//function  mapVisibilityZone(Map : HMap; RstName : GTKPChar; Zone : TBUILDZONEVISIBILITY) : integer;    // 20/04/2011
function  mapVisibilityZone(Map : HMap; RstName : GTKPChar; var Zone : TBUILDZONEVISIBILITY) : integer;    // 15/10/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

//function  mapVisibilityZoneUn(Map : HMap; RstName : PWChar; Zone : TBUILDZONEVISIBILITY) : integer;    // 03/04/2014
function  mapVisibilityZoneUn(Map : HMap; RstName : PWChar; var Zone : TBUILDZONEVISIBILITY) : integer;    // 15/10/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // ����������� ��������� �� ����� point1 (���������� � ������ �� ���������) ����� point2
 // deltaheight  - ������ ���������� (� ������);
 // ����������� � ������ � ����� point1
 // ���������� ������������ ��� ������� �������� ������� �����
 // ���������� 0 - point2 �� ����� �� point1                     // 12/04/06
 //            1 - point2 ����� �� point1

function  mapVisibilityFromPoint(Map : HMap; point1,point2 : PDoublePoint; deltaheight : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������� ����� � �������
 // ��� ������ ���������� ����    // 08/02/06

function  mapDeleteLoop(Obj : HObj; precision : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������ ������ �������� � ������������� ������� �����   // 15/01/07
 // �� �������� ����
 // info   - ������������� ������� ����� � ������
 // center - ���������� �����; ������ ������� �������������� ������ (�����)
 // angle  - ���� �������� �� ������� ������� (�������; �� -PI �� +PI)
 // ��� ������ ���������� 0

function mapRotateObject(aObj : HObj; var acenter : TDOUBLEPOINT; var aAngle : double):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ������������� ��������, ��������������� �������� ����������� ������,
 // �� ���� �������� ��������� ������
 // hmap - ������������� ������� �����
 // hobj - ������ � ������� ������������ ������������ ������
 // precision - ������ ������������ �������� (������ ���� >= DELTANULL)
 // ��� ������ ���������� 0

function mapContourTotalSeekObjects(aMap: HMAP ;aObj : HObj; precision : double):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;



 // ************************************************************
 //                                 *
 //      �������������� ������ �������                         *
 //                                 *
 // ************************************************************

  // ��������� ���������� ��������� ������
  // info    - ������������� ������� ����� � ������
  // text    - ����� ��� ���������� ������ (ANSI)
  // size    - ����� ���������� ������� ��� ������ � ������
  // subject - ����� ����������
  // ��� ������ ���������� ����

function mapGetText(Obj:HOBJ;text:GTKPChar;length:integer;subject:integer):integer; // 09/10/2009
//function mapGetText(Obj:HOBJ;text:GTKPChar;length:integer;subject:integer):GTKPChar;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 


function  mapGetTextUnicode(Obj : HObj; text : PWCHAR; size, subject : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetTextUn(Obj : HObj; text : PWCHAR; size, subject : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ����� ���������� ��������� ������
  // Obj    - ������������� ������� ����� � ������
  // text   - ����� ����� ������ (ANSI)
  // subject - ����� ����������
  // ��� ������ ���������� ����

function mapPutText(Obj : HOBJ; text : GTKPChar; subject : integer) : GTKPChar;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 

  // ���������� ����� ���������� ��������� ������
  // info   - ������������� ������� ����� � ������
  // text   - ����� ����� ������ UNICODE UTF-16
  // ���� ����� �������� ��������� ������� �� 0x0001 �� 0x007E
  // ��� ��������� (0x0400 - 0x045F) � �� ����������
  // ����������� ������� Windows (OEM 866 ��� 1251), �� �����
  // ������������� ��������� � ANSI,
  // ����� ����� �������� ���������� � UTF-16
  // subject - ����� ����������
  // ��� ������ ���������� ����
function  mapPutTextUnicode(Obj : HObj; const Text: PWCHAR; subject: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ������� - �������� mapPutTextUnicode

function  mapPutTextUn(Obj : HObj; const Text: PWCHAR; subject: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� - �������� �� ����� � ��������� UNICODE
  // Obj    - ������������� ������� ����� � ������
  // ���� ����� � UNICODE - ���������� ��������� ��������
  // ��� ������ ���������� ����

function  mapIsTextUnicode(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ����� ������ � �������� �� �����
  // Obj     - ������������� ������� ����� � ������
  // subject - ����� ����������
  // ��� �������������� ������� ���������� 0
  // ��� ������ ���������� 0

function mapGetTextLengthMkm(info:HOBJ;subject:integer):integer; // 23/09/2009
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 

  // ��������� ������ ������ ������ ��� �������� ���� �������
  // � �������� �� �����
  // info    - ������������� ������� ����� � ������
  // ��� �������������� ������� ���������� 0
  // ��� ������ ���������� 0

function  mapGetTextHeightMkm(Obj : HObj) : integer;      // 12/11/08
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ����� ������� � �������� ��� ������� ������� �����������   // 12/01/13
  // hMap    - ������������� ���������
  // hdc     - ������������� ���������, �� ������� �������������� ������ �����
  //           (����� ���� ����� ����)
  // rect    - ��������� ������� �����������, ������������ ������� ��������� �����,
  //           � �������� �� ���������
  // info    - ������������� ������� ���� �������, ��������� ����������� ������ ���� ���� IMG_TEXT
  // box     - ���������� 4-�� ����� ��������� ����� ������������ �������� ������
  //           ���� ������� rect
  // ���������� ������� �� ����������� ��� ������� �����
  // ��� ������ ���������� 0

function  mapGetPaintTextBorder(Map : HMAP; dc : HDC; var rect : TRECT;        // 20130128 Korolev
                                info : HOBJ; box : PDRAWBOX) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;								

  // ��������� ������ ������������ ������ �� �����������
  // Obj     - ������������� ������� ����� � ������
  // subject - ����� ����������
  // (FA_LEFT;FA_RIGHT;FA_CENTER - ��. mapgdi.h)

function  mapGetTextHorizontalAlign(Obj : HObj; subject: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������ ������������ ������ �� ���������
  // info    - ������������� ������� ����� � ������
  // subject - ����� ����������
  // (FA_BOTTOM;FA_TOP;FA_BASELINE;FA_MIDDLE)

function  mapGetTextVerticalAlign(Obj : HObj; subject: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ������ ������������ ������ �� �����������
  // (FA_LEFT;FA_RIGHT;FA_CENTER)
  // Obj     - ������������� ������� ����� � ������
  // subject - ����� ���������� (-1 - ���������� ����)
  // �� ��������� ����� �������� FA_LEFT
  // ��� �������� ���������� ���������� ������������� ��������

function  mapPutTextHorizontalAlign(Obj : HObj; align, subject: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ������ ������������ ������ �� ���������
  // (FA_BOTTOM;FA_TOP;FA_BASELINE;FA_MIDDLE)
  // Obj     - ������������� ������� ����� � ������
  // subject - ����� ���������� (-1 - ���������� ����)
  // �� ��������� ����� �������� FA_BASELINE
  // ��� �������� ���������� ���������� ������������� ��������

function  mapPutTextVerticalAlign(Obj : HObj; align, subject: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ************************************************************
 //                                 *
 //     �������������� ������������ �������� ��'����           *
 //                                 *
 //  (����������� �������� �������; ��� �������; � ��������    *
 //   ���������������� �����; �� ��������� � ���������������)  *
 //                                 *
 // ************************************************************


  // ��������� - ����� �� ������ ����������� ��������
  // Obj     - ������������� ������� ����� � ������

function  mapIsDrawObject(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ���������� ��������� ������������ ��������
  // Obj     - ������������� ������� ����� � ������
  // ��� ������ ���������� 0

function  mapDrawCount(Obj : HObj) : integer;  
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ��� �������� ������������ ��������
  // �� ��� ������ (number -  �� 1 �� DrawCount())
  // Obj     - ������������� ������� ����� � ������
  // ���������� ����� ������� ���� IMG_XXXXXXX (��. MAPGDI.H)
  // ��� ������ ���������� 0

function  mapDrawImage(Obj : HObj; number : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ����� ���������� �������� ������������ ��������
  // �� ��� ������ (number -  �� 1 �� DrawCount())
  // Obj     - ������������� ������� ����� � ������
  // ���������� ����� ��������� ���� IMGXXXXXX; � ������������
  // � ����� �������� (��. MAPGDI.H)
  // ��� ������� � 0 ������� ���������� ����� ����������
  // ������������ �������� ������� ��������� IMGDRAW
  // ��� ������ ���������� 0

//function  mapDrawParameters(Obj : HObj; number : integer) : GTKPChar;       // 23/10/2014
function  mapDrawParameters(Obj : HObj; number : integer) : Pointer;       // 23/10/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ����� ���������� �������� ������������ ��������
  // �� ��� ������ ( �� 1 �� DrawCount())
  // Obj     - ������������� ������� ����� � ������
  // ��� ������� � 0 ������� ���������� ����� ����������
  // ������������ �������� �������
  // ��� ������ ���������� 0

function  mapDrawLength(Obj : HObj; number : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // �������� ������� ������������ �������� ��������
  // Obj  - ������������� ������� ����� � ������
  // image - ����� ������� ���� IMG_XXXXXXX (��. MAPGDI.H)
  // parm  - ����� ��������� ���� IMGXXXXXX
  // ��� ������ ���������� ����;����� - ����� ��������� � ������

//function  mapAppendDraw(Obj : HObj; image : integer; const parm : GTKPCHAR) : integer;  // 23/10/2014
function  mapAppendDraw(Obj : HObj; image : integer; const parm : Pointer) : integer;  // 23/10/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ��� �������� ������������ �������� �������
  // Obj   - ������������� ������� ����� � ������

function  mapClearDraw(Obj : HObj) : integer;  
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ������� ������������ �������� �������
  // Obj    - ������������� ������� ����� � ������
  // number - ����� �������� (������� � 1)
  // ��� ������ ���������� ����

function  mapDeleteDraw(Obj : HObj; number : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // ************************************************************
 //                                 *
 //         ���������/������������ ������ ��'����              *
 //                                 *
 // ************************************************************


 // ��������� - ���������� �� �����-���� ������ ������� � ������:
 // �������, ���������, �������
 // ��� ��������� ������, ������� �� ���� ��������� �������� ����
 // mapCommitObject, ���������� ��������� ��������
 // ��������� ���� �������, ������ ��������� � ������ �������
 // ��� ������� �� ���������
 // ��� ������� ��������� ���������� ���� ��������� (2 - �������,
 // 4 - ���������, 8 - �������).
 // ��� ������ ���������� ����

function  mapIsDirtyObject(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������ �� ������� � �����
 // Obj   - ������������� ������� ����� � ������
 // ����� ����� � ������ ������ ���� ����������
 // ���������� ��������� ������� ����������� � ���������
 // ������ � ����� ���� �������������
 // ��� ������ ���������� ����

function  mapCommit(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapCommitObject(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������ �� ������� � �����
 // Obj  - ������������� ������� ����� � ������
 // ������� ����� ������������ � ������� ������ � ����
 // ����������� ��� ����������� ���������� ��������
 // (��������; �������� ����� ���� ��� ���������)
 // ���������� ��������� ������� ����������� � ���������
 // ������ � ����� ���� �������������
 // ��� ������ ���������� ����

function  mapCommitObjectByOrder(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������ �� ������� � �����
 // Obj   - ������������� ������� ����� � ������
 // ���� ������ ����� - ����������� ������� mapCommitObject();
 // ���� ����� ������ ��� ���; �� ����������� �����
 // ������� � ����� ���������� ������� (��������������;
 // ��� �������������� �������� ���������� � �.�.)
 // ����� ����� � ������ ������ ���� ����������
 // ��������� �������� �������� ����� ���������� ��������
 // (������ ������ - mapCopyObjectAsNew()).
 // ��� ������ ���������� ����

function  mapCommitObjectAsNew(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������ �� ������� � ����� � ������� �����
 // ��� �������� ������� �� ������ (��� �������������)
 // Obj   - ������������� ������� ����� � ������
 // ��� �������� ���������������� ���� (����������)
 // ���������� mapCommitObject() - ��� ���� ���� � ��� ������.
 // ���������� ��������� ������� ����������� � ���������
 // ������ � ����� ���� �������������
 // ��� ������ ���������� ����

function  mapCommitWithPlace(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������ �� ������� � ����� � ������� �����
 // ��� �������� ������� �� ������ (��� �������������)
 // ������ ����� ��������; ��� �����; � �����������
 // ������ ����������� ������
 // Obj   - ������������� ������� ����� � ������
 // ��� �������� ���������������� ���� (����������)
 // ���������� mapCommitObject() - ��� ���� ���� � ��� ������.
 // ���������� ��������� ������� ����������� � ���������
 // ������ � ����� ���� �������������
 // ��� ������ ���������� ����

function  mapCommitWithPlaceAsNew(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������ �� ������� � ����� �
 // ���������� ������� �� �������� ��������� �����
 // Obj  - ������������� ������� ����� � ������
 // list - ����� �����( > 0 ); �� ����� �������� ���������������
 // ����������� ������ �������
 // ��� �������� ���������������� ���� (����������)
 // ���������� mapCommitObject() - ��� ���� ���� � ��� ������.
 // ��� ������ ���������� ����

function  mapCommitWithPlaceForList(Obj : HObj; list: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������/���������� ��������� ��������� ������� �������� � ��������� ��������,
 // ������������ ��� ���������� �������� � �������� �� ����� � ��������:
 // mapCommitWithPlace, mapCommitWithPlaceAsNew, mapCommitWithPlaceForList
 // parm  - ���������, ������������ ��� ��������������� �������� ����� ��������,
 //         ����� �������� � ����������� ��������� ��������, ������� ����������
 //         ��� ���������� �������� �� ������ ������ �����
 // ��� ������ ���������� ����

function  mapGetCommitObjectParm(aMap : HMAP; parm : PCOMMITOBJECTPARM) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapSetCommitObjectParm(aMap : HMAP; parm : PCOMMITOBJECTPARM) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // ������� ������ �����
 // ���������� ��������� ������� ����������� � ���������
 // ������ � ����� ���� �������������
 // Obj  - ������������� ������� ����� � ������
 // ������� �������� ������������ � ������ � � �����
 // ��� ������ ���������� ����

function  mapDeleteObject(Obj : HObj): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;



 // ������� ������ ����� �� ��� ������ (number)
 // Map   - ������������� �������� ������
 // list   - ���������������� ����� ����� (� 1)
 // number - ���������������� ����� ������� � �����
 // ����� � ������� - ��. mapReadObjectByNumber;mapGetObjectNumber
 // ����� �������� ������� �� ������ ����� �ommit �� ������ �����������
 // ��� ����� �������
 // ��� ������ ���������� ����

function  mapDeleteObjectByNumber(Map : HMap; list, number : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� �������� ������� �����
 // Obj   - ������������� ������� ����� � ������
 // ������� �������� ��������� � ������ � � �����
 // ��� ������ ���������� ����

function  mapUndeleteObject(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ����������� ������ � ������� � ����� (�������� ��� �����)      // 19/02/14
 // ������� ������������� ������� "���� ����"
 // info  - ������������� ������� ����� � ������
 // ���������� ����� ���������������� ����� ������� �� ����� - mapGetObjectNumber()
 // ��� ������ ���������� ����

function  mapUpdateObjectUp(info : HObj) : integer;          // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ����������� ������ � ������� � ������ (�������� ��� �����)     // 19/02/14
 // ������� ������������� ������� "���� ����"
 // info  - ������������� ������� ����� � ������
 // ���������� ����� ���������������� ����� ������� �� ����� - mapGetObjectNumber()
 // ��� ������ ���������� ����

function  mapUpdateObjectDown(info : HObj) : integer;          // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // �������� �������� "���� ����" � "���� ����" � �������          // 19/02/14
 // ��������� ������� � ������������ � ��� ����� � ������������
 // ��������� ����� ���������� �����
 // info  - ������������� ������� ����� � ������
 // ��� ������ ���������� ����

function  mapUpdateObjectNormal(info : HObj) : integer;          // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // �������� �������� ������� ����� �� ��� ������ (number)
 // Map   - ������������� �������� ������
 // Site  - ������������� ���������������� �����
 //          (��� ������ ����� ����� hMap ��� 0)
 // list   - ���������������� ����� ����� (� 1)
 // number - ���������������� ����� ������� � �����
 // ����� � ������� - ��. mapReadObjectByNumber;mapGetObjectNumber
 // ������� �������� ��������� � �����
 // ��� ������ ���������� ����

function  mapUndeleteObjectByNumber(Map : HMap; Site : HSite; list, number : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������������ (� ������) ������ �� ������� �� �����
 // Obj   - ������������� ������� ����� � ������
 // ����� ����� � ������ � ����� ������� ������ ���� �����������
 // ��� ������ ���������� ����

function  mapRevert(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapRevertObject(Obj : HObj) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ����� �������, ������� ����� ����������      // 03/08/09
  // ��� �������������� ������� �����.
  // ��� ������ ���������� � �������������� ���������� �������
  // ������ ������� � �������� � ���������� call � parm ������� ��������
  // ���������� ������� �� ������ ���� ������������� ����� !
  // hmap   - ������������� �������� ������
  // hSite  - ������������� ���������������� �����
  //          (��� ������� ����� ����� hMap ��� 0)
  // call - ����� ���������� ������� (��. maptype.h),
  // parm - ��������, ������� ����� ������� ���������� ������� ������,
  // ������ ���������� ����� ����� ��������� CHANGEINFO (��. maptype.h).
  // ��� ������ ���������� ����

function mapSetChangeCallAndParm(aMap : HMAP; aSite : HSite; call: TCHANGECALL;
                                         parm: Pointer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // *************************************************************
 //                                  *
 //     ��������� ��������� ������ ...\LOG\... ^DA;^SE;^HD      *
 //      (�������� ����� ���� ����������� �� ����� �����)       *
 //                                  *
 // *************************************************************

  // ���������������� ������� ����������������� ����� ������� info
  // Obj  - ������������� ������� ����� � ������
  // copynumber - ���������������� ����� ����� ������� ������� (1; 2; ...)
  // � ������� �� ��������� �������� �������������� � ����������
  // ���������� � ���������������� ����������� copynumber;
  // ���� �� ����� ������� ������ ����� ��� ����� ����������
  // ����� ������� ��������� ����� ������� ���������� ������� �� �����;
  // ����� ������� ������ ����� ������� ��������
  // mapDeleteObjectCopyToNumber; � ����� ������� ������� mapCommit.
  // ��� ������ ���������� ����

function  mapReadObjectCopyByNumber(Obj : HObj; copynumber: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ����������������� ����� ������� info
  // Obj  - ������������� ������� ����� � ������
  // ������� ����� ������� �� ������ �� copynumber �� ������ ������
  // ����� copynumber+1 ���������� ������
  // ���� ���������� ����� ���������� �������������� ����� �������
  // �� ����� (mapCommit - ������ ����������� �������� ��������������);
  // �� ����� ��������� ����� ������ ���� �� 1 ������; ��� �����
  // ����������������� �����. (�.�. mapCommit ������� ��� ���� �����
  // ������� � ��� ����������� ������).
  // ��� ������ ���������� ����

function  mapDeleteObjectCopyToNumber(Obj : HObj; copynumber: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // ************************************************************
 //                                 *
 //         ������ ������������� ��'����                       *
 //                                 *
 // ************************************************************

 // ���������� ����� ������� ������� (�������) �� ���������, 
 // ������� � ��������� �����
 // ��� ��������� ����� ��������� ��������� �� ������ �����
 // Obj     - ������������� ������� ����� � ������
 // number  - ����� �����; ������� � 1
 // subject - ����� ���������� (���� = 0; �������������� ������)
 // ��� ������ ���������� 0 (��� ���������� ����� �����)

function mapSideLength(Obj : HObj; number : integer; subject : integer = 0) : double;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function  mapSideLengthEx(Obj : HObj; number, subject : integer;var length : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ����� ������� ������� (�������) � �������� ���������       // 24/05/10
 // ��� �� ���������, � ����������� �� ��������
 // ������� ���������� ���������� �� ����� -
 // mapSetCalculationConventional
 // info  - ������������� ������� ����� � ������
 // number - ����� �����, ������� � 1
 // subject - ����� ���������� (���� = 0, �������������� ������)
 // ��� ������ ���������� 0

function mapConventionalSideLength(Obj : HObj; number : integer; subject : integer = 0) : double;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ���������� ������� ������� ������� (�������)
 // ���������� �������� ���� � ��������
 // ���� �������� �� ��������������; �� ����������� ������������ ����
 // ��� ��������� ����� ��������� ����������� �� ������ �����
 // � ��������� �������� ������ � ��������� ����� ���������
 // Obj     - ������������� ������� ����� � ������
 // number  - ����� �����; ������� � 1
 // subject - ����� ���������� (���� = 0; �������������� ������)
 // ��� ������ ���������� 0 (��� ���������� ����� �����)

function  mapSideAzimuth(Obj : HObj;number, subject : integer): double;      //16/01/01
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapSideAzimuthEx(Obj : HObj;number, subject : integer; var azimuth : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������������� ���� ������� ������� (�������)
 // ���������� �������� ���� � ��������
 // ��� ��������� ����� �������� ����������� �� ������ �����
 // � ��������� �������� ����� � ������� ����� ���������
 // Obj     - ������������� ������� ����� � ������
 // number  - ����� �����; ������� � 1
 // subject - ����� ���������� (���� = 0; �������������� ������)
 // ��� ������ ���������� 0 (��� ���������� ����� �����)

function mapSideDirection(Obj:HObj;number:integer;
                          subject:integer):double;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 

 // ���������� ���������� ������� ������� �� ���������
 // ��� ���������� ������� ������� ��� ���������� ��������������
 // � �������� ��������������� ����� ��������� ����!
 // Obj   - ������������� ������� ����� � ������
 // ��� ������ ���������� 0

function  mapSquare(Obj : HObj): double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapSquareEx(Obj : HObj;var square : double) : integer;  // 20/05/09
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������� ������� � �������� �����       // 24/05/10
 // info  - ������������� ������� ����� � ������
 // ��� ������ ���������� 0

function mapSquareInMap(info : HOBJ) : double;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������� ������� � �������� �����       // 24/05/10
 // ��� �� ���������, � ����������� �� ��������
 // ������� ���������� ���������� �� ����� -
 // mapSetCalculationConventional
 // info  - ������������� ������� ����� � ������
 // ��� ������ ���������� 0

function mapConventionalSquare(info : HOBJ) : double;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������� ���������� ���������� � �������� ���������      // 27/05/10
 // ��� �� ���������, � ����������� �� ��������
 // ������� ���������� ���������� �� ����� -
 // mapSetCalculationConventional
 // info    - ������������� ������� ����� � ������
 // subject - ����� ���������� �� 0 �� mapPolyCount,
 //           ���� ����� ����, �� ����������� ������� �������� �������
 //           ��� ��������� ������� �����������
 // ��� ������ ���������� 0

function mapConventionalSubjectSquare(info : HOBJ; subject : integer) : double;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������� ���������� � �������� ���������       // 27/05/10
 // info  - ������������� ������� ����� � ������
 // subject - ����� ���������� �� 0 �� mapPolyCount,
 //           ���� ����� ����, �� ����������� ������� �������� �������
 //           ��� ��������� ������� �����������
 // ��� ������ ���������� 0

function mapSubjectSquareInMap(info : HOBJ; subject : integer) : double;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ������� ��� ������� ���������� � ��������   // 24/05/10
 // �� �����
 // hmap   - ������������� �������� ������
 // flag   - ������� ���������� ��������:
 //          0 - ��������� ���������� �������� �������� �
 //          ���������� �� ���������� ������� ���������;
 //          1 - ��������� ������� � ���������� � ��������
 //          �����, ������� ����������� ������
 // ���������� ���������� �������� �������

function mapSetCalculationConventional(aMap : HMAP; flag : integer) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������� ��� ������� ���������� � ��������    // 24/05/10
 // �� �����
 // hmap   - ������������� �������� ������
 // ���������� ������� ���������� ��������: 
 //          0 - ��������� ���������� �������� �������� � 
 //          ���������� �� ���������� ������� ���������;
 //          1 - ��������� ������� � ���������� � ��������
 //          �����, ������� ����������� ������

function mapGetCalculationConventional(aMap : HMAP) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // ���������� ������� ������� c ������ �������
 // ��� ���������� �������(������� �����;�����;Tin-�������) ���������� ������� �������
 // ��� ������ ���������� 0

function  mapSquareWithHeight(Map : HMap; Obj : HObj): double;   //26/05/05
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ��������� �������
 // Obj   - ������������� ������� ����� � ������
 // ��� ������ ���������� 0

function  mapPerimeter(Obj : HObj): double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ����� �������
 // ��� ���������� ����� ������� ��� ���������� ��������������
 // � �������� ��������������� ����� �� ������� ������� ��������
 // � ���������� ������� ��������� � ������ �������
 // Obj  - ������������� ������� ����� � ������
 // ��� ����������� ��������� ��������� �����
 // ��� ������ ���������� 0

function  mapLength(Obj : HObj): double;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapLengthEx(Obj : HObj; var length: double) : integer;   // 20/05/09
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ����� ������� �� �����
 // ���������� ������� �� ���������������; ���������� �����
 // ����� ����������� ���������� �� �������� ����� ������� �� ���������
 // Obj   - ������������� ������� ����� � ������
 // ��� ����������� ��������� ��������� �����
 // ��� ������ ���������� 0

function  mapLengthInMap(Obj : HObj): double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ����� ������� �� ������ �� �������� �����
 // Obj   - ������������� ������� ����� � ������
 // point - ���������� �����; ������������� �����(������) �������
 // ���� ����� �� �� ������� - ������ ��������� ����� �� �������
 // ���������� ����� �����������!
 // ��� ������ ���������� 0

function  mapLengthToPoint(Obj : HObj; Point : PDoublePoint): double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ����� ����������/�������
 // info    - ������������� ������� ����� � ������
 // subject - ����� ���������� (���� = 0, �������������� ������;
 //           ���� ����� -1, ����������� ����� ������� � �����������)
 // ��� ������ ���������� 0

function  mapSubjectLength(Obj : HObj; subject : integer): double;      //20/01/05
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ����� �������\���������� � �������� ���������              // 27/05/10
 // info    - ������������� ������� ����� � ������
 // subject - ����� ���������� (���� = 0, �������������� ������;
 //           ���� ����� -1, ����������� ����� ������� � �����������)
 // ��� ������ ���������� 0

function  mapSubjectLengthInMap(Obj : HObj; subject : integer): double;      //20/01/05
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ����� �������\���������� � �������� ���������       // 24/05/10
 // ��� �� ���������, � ����������� �� ��������
 // ������� ���������� ���������� �� ����� -
 // mapSetCalculationConventional
 // info    - ������������� ������� ����� � ������
 // subject - ����� ���������� (���� = 0, �������������� ������;
 //           ���� ����� -1, ����������� ����� ������� � �����������)
 // ��� ������ ���������� 0

function  mapConventionalSubjectLength(Obj : HObj; subject : integer = -1): double;      //20/01/05
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ����� ������� c ������ �������
 // ��� ���������� �������(������� �����;�����;Tin-�������) ���������� ����� �������
 // ��� ������ ���������� 0

function  mapLengthWithHeight(Map : HMap; Obj : HObj) : double;      
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ����������� ����������� ������� ����������
 // Obj   - ������������� ������� ����� � ������
 // subject - ����� �������� ���������� (0 - �������)
 // ����������: 1 - ������ �������; ����� 0

function mapCircuitousSubject(Obg     : HOBJ; //Osipoff 29.01.02
                              subject : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


 // ����������� ����������� ���������� �� ����� �� �������
 // hmap   - ������������� �������� ������
 // Obj    - ������������� ������� ����� � ������
 // subject - ����� �������� ���������� (0-�������)
 // ���������� ����� point ������ � �������������
 // ������� ��������� ; � ������ �� ���������
 // ���������� ����������� ���������� � ������
 // ��� 0 � ������ ������

function  mapDistancePointSubject(Map : HMap; Obj : HObj; subject: integer;
  Point : PDoublePoint ): double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ����������� ����������� ���������� �� ����� �� ������� (�������
 // ����������)
 // hmap   - ������������� �������� ������
 // Obj    - ������������� ������� ����� � ������
 // ���������� ����� point ������ � �������������
 // ������� ��������� ; � ������ �� ���������
 // ���������� ����������� ����������  � ������
 // ��� 0 � ������ ������

function  mapDistancePointObject(Map : HMap; Obj : HObj; var Point : TDoublePoint): double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ����������� ����������� ���������� ����� ���������    //28/06/02
 // Obj1  - ������������� 1-�� ������� ����� � ������
 // Obj2  - ������������� 2-�� ������� ����� � ������
 // ���������� ����������� ����������  � ������
 // ��� 0 � ������ ������

function  mapDistanceObject(Obj1, Obj2 : HObj): double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ����������� ����������� ���������� ����� ��������� � ���������     // 21/04/10
 // ����� �� �������� ��������
 // hobj1  - ������������� ������� ������� ����� � ������
 // hobj2  - ������������� ������� ������� ����� � ������
 // point1 - ���������� ������ ����� ����� ����������� ����������
 //          ����� ��������� (�� ������� hobj1)
 // point2 - ���������� ������ ����� ����� ����������� ����������
 //          ����� ��������� (�� ������� hobj2)
 // ���������� ����������� ����������  � ������
 // ��� ������� �������� (>= 100000000) � ������ ������

function  mapDistanceObjectEx(Obj1, Obj2 : HObj; var point1, point2 : TDOUBLEPOINT) : double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ������ ������������� ������ �� ����������                       // 03/04/12
 // ��� ���������� �� ����� 250 �� ���������� ������������ � ������� �� 0,0001",
 // � �������� ������ - �� 0,001", ��� ������������� ������������ 1 ������
 // ������ ��������������� ����� �� ������ �����������
 // ����� ������������ ��� ���������� ������ ������� �����
 // ���������� ����������� �� ������� ����������, �������������
 // � ��������� - mapSetDocProjection
 // hmap     - ������������� �������� ������
 // b1,l1    - ������������� ���������� �������� �����
 // angle1   - ������ �� ������ �����
 // distance - ���������� �� ������ �����
 // b2,l2    - ������������ ���������� ������ �����
 // angle2   - ������������ ������ �� ������ ����� �� ������
 //            (���� angle2, �� �������� ������ �� �����������)
 // ��� ������ � ���������� ���������� 0

function  mapDirectPositionComputation(map : HMAP; b1, l1, angle1, distance : double; b2, l2, angle2 : pdouble) : integer;   // 20130128 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������� ������������� ������ �� ����������                     // 03/04/12
 // ��� ���������� �� ����� 180 �������� �� ������
 // ����������� ���������� ���������� �������� mapOrthodromeObject
 // � ������ ����� ������� � ������� ������� �������
 // �������� ������� �������� ������������ 1 ������
 // ���������� ����������� �� ������� ����������, �������������
 // � ��������� - mapSetDocProjection
 // hmap     - ������������� �������� ������
 // b1,l1    - ������������� ���������� ������ �����
 // b2,l2    - ������������� ���������� ������ �����
 // angle    - ������������ ������ � ������ ����� �� ������
 // ���������� ���������� ����� ��������� ������� �� ������� ����������
 // ��� ������ � ���������� ���������� 0

function  mapInversePositionComputation(map : HMAP; b1, l1, b2, l2 : double; angle : pdouble) : double;  // 20130128 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 //**********************************************************
 //*                             *
 //*                �������������� �������                  *
 //*                             *
 //**********************************************************

 // ���������� ���������� ����� ����� ������� �� ���������
 // point1, point2 - ���������� ����� � ������
 // ��� ������ ���������� 0

function  mapDistance(Point1, Point2 : PDoublePoint): double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ���������� ����� ����� ������� �� ���������
 // hmap   - ������������� �������� ������
 // point1, point2 - ���������� ����� � ������ �� ���������
 // ��� ���������� ���������� ���������� ��������������
 // � �������� ��������������� ����� � ���������� ������� 
 // ��������� � ������ �������
 // ��� ������ ���������� 0

function  mapRealDistance(Map : HMap; Point1, Point2 : PDoublePoint): double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ���������� ���������� ����� ����� ������� � �������� �����       // 25/05/10
 // ��� �� ���������, � ����������� �� ��������
 // ������� ���������� ���������� �� ����� -
 // mapSetCalculationConventional
 // hmap   - ������������� �������� ������
 // point1, point2 - ���������� ����� � ������ �� ���������
 // ��� ������ ���������� 0


function  mapConventionalDistance(Map : HMap; Point1, Point2 : PDoublePoint): double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 // ����������� ���� ������� ����������� ����; ���������
 // ������� p1;p2;p3 � �������� � ����� p2
 // ����������� ���� ������� ����������� ����; ���������
 // ������� p1;p2;p3 � �������� � ����� p2
 // ������������ ���� ����� ������������ ��� X; ��� �������������
 // ����������� ������������� �������������� ����������� ��� Y

function  mapBisectorAngle(P1,P2,P3 : PDoublePoint): double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 
  {*********************************************************
  *                                                        *
  *          ������� ������ ������ ����                    *   // 03/04/2014
  *                                                        *
  *  ������ - ����������� ��������� ����� ����� ������     *
  *           ������ ��� �����������. �������� �������     *
  *           �� �� ����� � ������ �������� � �������,     *
  *           � ����� ������� ������ ������ (MPT)          *
  *                                                        *
  *********************************************************}

  // �������� ������ �� �����
  // Map - ������������� �������� ������
  // name - ��� ����� (�������, ������) ��� ����������� ������
  // ���������� ������� ����� ������ � ������
  // ��� ������ ���������� ����

function  mapAppendInset(Map : HMap; name: PWChar): Integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // �������� ������ �� �����
  // hMap - ������������� �������� ������
  // item - ��������� �� �������� ����������� ������ (�����, �������, ������)
  // ���������� ������� ����� ������ � ������
  // ��� ������ ���������� ����

function  mapAppendInsetItem(Map : HMap; item: PINSETDESC): Integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ����� ������ �� �����
  // hMap - ������������� �������� ������
  // ��� ������ ���������� ����

function  mapInsetCount(Map : HMap): Integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� �������� ������ �� ������ � 1
  // hMap   - ������������� �������� ������
  // number - ���������� ����� ������ �� ����� � 1
  // item   - ��������� �� �������� ������,
  //          ������� ����� ���������
  // ��� ������ ���������� ����

function  mapGetInset(Map : HMap;number:Integer; item: PINSETDESC): Integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� �������� ������ �� ��������������
  // hMap   - ������������� �������� ������
  // ident  - ������������� ������ �� �����
  // item   - ��������� �� �������� ������,
  //          ������� ����� ���������
  // ��� ������ ���������� ����, ����� - ����� ������ � ������

function  mapGetInsetByIdent(Map : HMap;ident:Integer; item: PINSETDESC): Integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ��������� �������� ������ �� ������ � 1
  // hMap   - ������������� �������� ������
  // number - ���������� ����� ������ �� �����
  // name   - ����� ��� ������ �����
  // size   - ����� ������ � ������
  // ��� ������ ���������� ����

function  mapGetInsetName(Map : HMap;number:Integer; name: PWChar; size:Integer): Integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������������� ������ �� ������ � 1
  // hMap   - ������������� �������� ������
  // number - ���������� ����� ������ �� �����
  // ��� ������ ���������� ����

function  mapGetInsetIdent(Map : HMap;number:Integer): Integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������������� �������� ������ ������ �� ������ � 1
  // hMap   - ������������� �������� ������
  // number - ���������� ����� ������ �� �����
  // ����������� ������������� �� ������ ����������� (����� ����� mapCloseData)
  // ��� ������ ���������� ����

function  mapGetInsetMapHandle(Map : HMap;number:Integer): Integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ������� �������� ������ �� ������ � 1
  // hMap   - ������������� �������� ������
  // number - ���������� ����� ������ �� �����
  // ��� ������ ���������� ����

function  mapDeleteInset(Map : HMap;number:Integer): Integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // �������� �������� ������ �� ������ � 1
  // hMap   - ������������� �������� ������
  // number - ���������� ����� ������ �� �����
  // item   - ��������� �� ����� �������� ������
  // ��� ������ ���������� ����

function  mapUpdateInset(Map : HMap;number:Integer; item: PINSETDESC): Integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ���������� ������ ������� ������������ �����������
  // hMap   - ������������� �������� ������
  // number - ����� ������, ��� ������� ��������������� ����,
  //          ���� ����� -1, �� ��������������� ��� ���� ������
  // flag   - ������� ������������ ����������� (0 ��� 1)
  // ��� ������ ���������� ����

function  mapSetInsetSchemeFlag(Map : HMap; number, flag:Integer): Integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ��������� ������� ����������� ������
  // hMap   - ������������� �������� ������
  // number - ����� ������ � 1, ��� ������� ������������� ����
  // ��� ������ ���������� ����

function  mapGetInsetViewFlag(Map : HMap; number:Integer): Integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ���������� ������ ������� �����������
  // hMap   - ������������� �������� ������
  // number - ����� ������, ��� ������� ��������������� ����,
  //          ���� ����� -1, �� ��������������� ��� ���� ������
  // flag   - ������� ����������� (0 ��� 1)
  // ��� ������ ���������� ����

function  mapSetInsetViewFlag(Map : HMap; number, flag:Integer): Integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ������ � ���������
  // hMap     - ������������� �������� ������
  // number   - ����� ������������ ������
  //            ���� ����� -1, �� ������������ ��� ������
  // hdc      - ������������� ��������� ���������� ������,
  // rect     - ���������� ������������� ��������� ����� (Draw) � ����������� (Picture)
  // position - ��������� �������� ������ ���� ������������ ������� � ���� �����������
  //            (���� �������� �������������� ������ ����� SetViewportOrgEx, �� �������� ����� 0)
  // printscale - ������������ ������� ������� ����������� ���������� � ������� ������
  //            (��� ������, ��� ������ �� ����� �������� ����� 0)
  // ��� ������ ���������� ����

function  mapPaintInset(Map : HMap; number:Integer; ahdc: HDC; rect:Prect;
                              position: PPOINT; printscale:PDouble): Integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;



 //*********************************************************
 //*                             *
 //*                 ��������� �������                      *
 //*                             *
 //*********************************************************/

 // ���������� ������� ���� ����� ��������� ����������

function mapCloseMapAccess:integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

  // ���������� ��� ����� �� ��� �����
  // name - ��� ������������ ����� (������ ����)
  // ������������� ������ 4 �����; ���������� ������������� ������.
  // ��� ������ ���������� ����; ����� - ������������� �����
  // (��. maptype.h : FILE_SXF; FILE_MAP; FILE_MTW;...)

function mapCheckFile(const FileName: GTKPChar):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

  // ���������� ��� ����� �� ��� �����
  // name - ��� ������������ ����� (������ ����)
  // ������������� ������ 4 �����; ���������� ������������� ������.
  // ��� ������ ���������� ����; ����� - ������������� �����
  // (��. maptype.h : FILE_SXF; FILE_MAP; FILE_MTW;...)
  // ������������� ��������� MAP (FILE_MAP) � SIT (FILE_MAPSIT)
  // ��� ����� ���� � ���� ALIAS#XXXX ��� ���� �� ��� �������

function mapCheckFileEx(const  name : GTKPChar): integer;
       {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;
  
function mapCheckFileExUn(const name : PWCHAR) : integer;    // 12/09/12      // 20130128 Korolev
       {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

  // ��������� ������� ����� � ������� "HH':'mm':'ss"
  // buffer - ����� ������ ��� ���������� ���������� �������
  // size   - ������ ���������� ������ ��� ��������
  // ��� ������ ���������� ����

function  mapGetTheTime(buffer : GTKPChar; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetTheTimeUn(buffer : PWChar; size : integer) : integer;    // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ���������� DLL
  // �� ������ ����� mapLoadLibrary ������ ����������� ����� mapFreeLibrary
  // ��� ������ DLL ����������� � ���������� ����������
  //  void (WINAPI * myfunction)(int param);
  // (FARPROC)myfunction = mapLoadLibrary("ABC.DLL";&instance;"MyFunction");
  //  if (myfunction)
  //      { (*myfunction)(123); ::FreeLibrary(*instance); }
  // ��� ������ ���������� ���� � ������ ��������� �� �����

//function  mapLoadLibrary(const dllname : GTKPChar; libinst : HINST; const Funcname : GTKPChar) : Pointer; // 01/10/2009
function  mapLoadLibrary(const dllname : GTKPChar; var libinst : HINST; const Funcname : GTKPChar) : Pointer; // 07/09/2010
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapLoadLibraryUn(const dllname : PWChar; var libinst : HINST; const Funcname : PWChar) : Pointer; // 03/04/2014
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ���������� DLL

function  mapFreeLibrary(libinst: HINST) : integer;  // 01/02/02 // 01/10/2009
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  //  ��������� ������� ���������� DLL

function  mapGetProcAddress(libinst: HINST; const funcname: GTKPChar) : Pointer;  // 01/02/02
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ����� ���� ������� ������� �� ����� *.CHM                                     // 03/04/14
  // helpName - ��� ����� �������
  // pageName - ��� �������� ������� (� �����������), ����� ���� ����� 0
  // ������ ������ �������: HelpExec("map3d.chm", "Setup.htm");

Procedure  HelpExec( helpName, pageName : PChar);
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ���������� ��������� ����� ���������                          // 07/01/12
  //  mapname - ����������� �����
  //  flags   - ������ ��������� ����� :
  //   0 - ����������� ��� �����,
  //   1 - ������ ���������������,
  //   2 - ��������� ����� ������,                  08/10/00
  //   4 - �������� �������� ��������,              01/08/01
  //   8 - ���������� �������� �������� (FLOAT).    10/02/05
  //  16 - �������� �������� ��������, ������ - ��  26/12/06
  //  32 - �������� �������� ��������, ������ - ��  26/12/06
  //  64 - �������� �������� ��������, ������ - ������� 23/04/10
  // hEvent - ����� ������� ��������� ������ ��� ����������� � ��������
  // eventparam - ��������� ������� ��������� ������
  // outpath - ����� ������ ��� ������ ������ ���� � ��������������� �����,
  //           ���� ����� ������ �� �����, �� ����� ����������� �� �����
  // size    - ������ ������ ��� ������ ����
  // ��� ������ ���������� ����

function  MapSortingWithEvent(const mapname : GTKPCHAR;                            // 20130128 Korolev
                              flags : integer;
                              hEvent : TEVENTCALL; 
                              eventparam : integer;
                              outpath : GTKPCHAR;
                              size : integer) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ���������� ��������� ����� ���������                          // 15/03/13
  // mapname - ����������� �����
  // flags   - ������ ��������� ����� :
  //   0 - ����������� ��� �����,
  //   1 - ������ ���������������,
  //   2 - ��������� ����� ������,
  //   4 - �������� �������� ��������,
  //   8 - ���������� �������� �������� (FLOAT).
  //  16 - �������� �������� ��������, ������ - ��
  //  32 - �������� �������� ��������, ������ - ��
  //  64 - �������� �������� ��������, ������ - �������
  // handle     - ������������� ����, �������� ���������� ��������� WM_LIST � WM_OBJECT,
  //              ���� �� ����� �������� hEvent
  // hEvent     - ����� ������� ��������� ������ ��� ����������� � �������� ����������,
  //              ���� �������� �� �����, �� ���������� ��������� WM_LIST � WM_OBJECT
  // eventparam - ��������, ������������ ������� ��������� ������
  // outpath    - ����� ��� ������ ���� � �����, ���� ��������� ��������������� �����,
  //              ���� ����� ���� ��������, �� �������� ����� �� �����������,
  //              ����� ��������� ������������� � ��������� ����������
  // size       - ����� ������ � ������
  // format - ���������� �������� ����� :                    // 13/06/13
  //  0 -  �� ������,
  //  1 -  ���������� ������ SITX (�� ����� ����� ���� SIT ��� MAP � ����� ������),
  // -1 -  ���������� ������ SIT (�� ����� ����� ���� SITX ��� MAP � ����� ������),
  // code - ���������� ����������� ����� :
  //  0 - �� ������,
  //  1 - ��������� ������ � ������� ������ �� ��������� password (������ SITX),
  // -1 - ����� ���������� ������
  // password - ������ ��� ���������� ������, ����� code = 1, ��� 0
  // ��� ������ ���������� ����

function  MapSortingWithEventPro(const mapname : PWCHAR; flags : integer;
                                 handle : HWND; hEvent : TEVENTCALL; eventparam : Pointer;
                                 outpath : PWCHAR; size : integer;
                                 format : integer; code : integer;
                                 const password : PWCHAR) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  MapSortingSitePro(aMap : HMAP; aSite : HSITE; flags : integer;
                            handle : HWND; format : integer; code : integer;
                            const password : PWCHAR) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  MapSortingWithEventUn(const mapname : PWCHAR; flags : integer;
                                handle : HWND; hEvent : TEVENTCALL; eventparam : Pointer;
                                outpath : PWCHAR; size : integer) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ����������� c���������� �������� ������
  // in     - ��� �������� �����
  // out    - ��� ��������� �����
  // mode   - ������ ������� � ��������� ����� (GENERIC_READ; GENERIC_WRITE);
  //          ���� ������������� GENERIC_READ;
  // access - ������ �������� ��������� ����� (FILE_SHARE_READ;FILE_SHARE_WRITE)
  //          ���� ������������� FILE_SHARE_READ|FILE_SHARE_WRITE
  // ��� ������ ���������� ����

function  mapCopyFile(const InFile, OutFile : GTKPChar;const access : integer = 0;
   const mode : integer = 0) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapCopyFileUn(const InFile, OutFile : PWChar;const access : integer = 0;   // 03/04/2014
   const mode : integer = 0) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;



  // ��������� ����� �����                                  // 26/02/07
  // name - ������ ���� � �����
  // ��� ������ ���������� ����

function  mapGetFileLength(const name : GTKPChar): double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetFileLengthUn(const name : PWChar): double;         // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ���������/��������� ������ ��������� �� �����
  // (��������� ����� ������)
  // enable = 0  - ������ ������ ���������;
  // ���������� ���������� �������� �����

function  mapMessageEnable(enable : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapIsMessageEnable: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������ ��������� �� ������ (�� �����)
  // code - ��� ������ (��. MAPERR.RH)

procedure ErrorMessageEx(WND:HWND;code:integer; // 09/10/2009
                        const filename:GTKPChar);

{function ErrorMessageEx(WND:HWND;code:integer;
                        const filename:GTKPChar): GTKPChar;}
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;
procedure mapErrorMessage(code:integer;const filename:GTKPChar); // 09/10/2009
//function mapErrorMessage(code:integer;const filename:GTKPChar) : GTKPChar;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;
  
procedure mapErrorMessageUn(code : integer; filename : PWCHAR);   // 20130128 Korolev
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

  // ������ ��������� � �������� �����
  // HMAP � HSITE ���������� �����; � �������� �������
  // ������������ ���������
  // (��� ����� ��������� HSITE = HMAP; ��. sitapi.h)
  // amessage - ����� ���������
  // msgtype - ��� ��������� (��. maptype.h : MT_INFO;MT_ERROR;...)

procedure mapMessageToLog(Map : HMap;Site : HSite; const amessage : GTKPChar; msgtype: integer);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

procedure mapMessageToLogUn(Map : HMAP; Site : HSITE; const amessage : PWCHAR; msgtype : integer);        // 28/12/12       // 20130128 Korolev                        
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;							

  // ������ ��������� �� �����

function  mapMessageBox(Wnd : HWnd; const message, title : GTKPChar; flag : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapMessageBoxUn(Wnd : HWnd; const message, title : PWChar; flag : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������ ������ MapAccess.Dll
  // ���� ���������� ������ �� ����� �������� MAPACCESSVERSION
  // � ������ ��������� ����� ��������� ����

function  GetMapAccessVersion: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapGetMapAccessVersion: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapGetMapApiVersion: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ����������/��������� ���� ���������
  // 1 - ����������; 2 - �������; ... (��. Maptype.h)
  // (��-��������� - ����������)

procedure SetMapAccessLanguage(code:integer);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  GetMapAccessLanguage:integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
procedure mapSetMapAccessLanguage(code:integer);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapGetMapAccessLanguage: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ������������� ���� ��� ������ ���������
  // �� "��������" ��������� (������������� ��� ��������
  // �����; ������� �������� �� �������� �������;...)
  // ��� ��������� - 0x590;
  // wparm : -1 ����� ��������;-2 ����������;0-100 ������� ����������
  // lparm : ����� ���������
  // ��� ������ ��������� - ���������� ������������� � ����
  // (������������� ��������� ���� ����� �������� � ���� � �������)
  // ��� ���������� �������� ������� ����� 0x590
  // ���������� ���������� �������� ��������������

function mapSetHandleForMessage(aHwnd : HWND) : HWND; // 10/07/2008
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function mapGetHandleForMessage : HWND;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 

// 11/12/13 {$IFDEF WIN32}
{$IFNDEF LINUXAPI}          // 11/12/13
  // ���������� ������������� ���� ��� ������ ��������� � �������� �����
  // hmap   - ������������� �������� ������
  // hwnd   - ������������� ����
  // event - ���� ����� ������� (1 - ����������� �����)
  // ��� ��������� WM_MAPEVENT (0x591)
  // wparm : ������������� �����; � ������� ��������� ������� (HMAP)
  // lparm : �������� �������
  // ��� ������ ��������� - ���������� ������������� � ����
  // (������������� ��������� ���� ����� �������� � ���� � �������)
  // ��� ������ ���������� ����

function  mapSetHandleForEvent(Map : HMap; Wnd : HWnd; event:integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������� �������� �������������� ���� ��� ������ ���������

function  mapGetHandleForEvent(Map : HMap):HWnd;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
{$endif}

  // �������� ��� ������ ��� ����� ��������
  // ���������� : (1-CMLESS, 2-CMEQUAL, 3-CMMORE - ��. Maptype.h)
  // ��� ������ ���������� ����

function mapCompareString(stroka : GTKPChar; filter : GTKPChar) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;
  
function mapCompareStringUn(const value, temp : PWCHAR) : integer;    // 20130128 Korolev
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

  // ������������� ���� �� ������ date � ����� ��������
  // ������ ����� ����� ��� ��/��/���� ��� ��.��.����
  // ��� �������� ��� ����/��/��
  // ��� ������ ���������� ����; ����� - �������� ����

function  mapDateToLong(const sDate : GTKPChar) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapDateToLongUn(const sDate : PWChar) : integer;                  // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������������� ���� �� �� ����� �������� � ������
  // ��/��/����
  // number - �������� �������� ����;
  // date   - ����� ������ ��� ����������
  // size   -  ������ ������ � ������
  // ��� ������ ���������� ����; ����� - ����� ������� ������

function  mapLongToDate(number : integer; sDate : GTKPChar; size:integer):GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapLongToDateUn(number : integer; sDate : PWChar; size:integer):PWChar;       // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������������� ����� �� ������ time � ����� ������
  // ������ ����� ����� ��� ��:��:��
  // ��� ������ ���������� ����; ����� - �������� ����

function  mapTimeToLong(const stime : GTKPChar) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapTimeToLongUn(const stime : PWChar) : integer;                    // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������������� ����� �� �� ����� ������ � ������
  // ��:��:��
  // number - �������� �������� �������;
  // stime  - ����� ������ ��� ����������
  // size   - ������ ������ ��� ��������
  // ��� ������ ���������� ����; ����� - ����� ������� ������

function  mapLongToTime(number : integer; stime : GTKPChar; size:integer) : GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapLongToTimeUn(number : integer; stime : PWChar; size:integer) : PWChar; // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������������� ������� �������� � �������� �� ������
  // � �������� �������� � ��������
  // ������ ����� ����� ��� ��ð��'CC.CC" ��� ���.�������ð
  // ��� Linux ������ ������� � (\xB0) �.�. ^
  // ��� ������ ���������� ����; ����� - �������� � ��������

function  mapAngleToRadian(angle : GTKPChar): double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapAngleToRadianUn(angle : PWChar): double;     // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������������� �������� �������� �� ������ � ������ ����
  // ��ð��'CC.CC"
  // ��� Linux ������ ������� � (\xB0) �.�. ^
  // angle  - ����� ������ ��� ����������
  // size   - ������ ������ ��� �������� (����������� ��� ����������)
  // ��� ������ ���������� ����; ����� - ����� ������� ������

function  mapRadianToAngle(radian:double; angle : GTKPChar; size:integer):GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapRadianToAngleUn(radian:double; angle : PWChar; size:integer):PWChar;   // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ������ ������ ������� � ���� ������ XXXXX/XXXXX ��� �����������
  // (��������; 16777339 ������������� ������ 256/123)
  // format - ������ ������: 1 - XXXXX/XXXXX; 2 - XXXXXXXXXXX;
  // 0 - ����� �������� �� ��������� (mapGetObjectKeyFormat).
  // ����������� ����� �������� ������ - 12 ����
  // ��� ������ ���������� ����

function  mapObjectKeyToString(key : integer; str : GTKPChar; size, format: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapObjectKeyToStringUn(key : integer; str : PWChar; size, format: integer) : integer; // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // �������������� ������ ������� �� ������ XXXXX/XXXXX ��� �����������
 // � ����� (��������; 16777339 ������������� ������ 256/123)
 // string - ������� ������
 // long   - ���������
 // ��� ������ ���������� ����

function  mapStringToObjectKey(str : GTKPChar;var key : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapStringToObjectKeyUn(str : PWCHAR; key : PINTEGER) : integer;        // 18/12/12 // 20130128 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ����������/��������� ������ ������ �������
  // 1 - XXXXX/XXXXX; 2 - XXXXXXXXXXX

function  mapGetObjectKeyFormat: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapSetObjectKeyFormat(format : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ����������/��������� ���� � ���������� ����������, ���
  // ������������� ��������������� ����� ��� ���������������� 
  // ���-���� (���������� �������� ������ *.iml, ����� ���� ������ EPSG,
  // wmslist.xml, xml-����� � ��.)

procedure mapSetPathShell(const path:GTKPChar);
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 
  
procedure mapSetPathShellUn(const path : PWCHAR);   // 14/01/13   // 20130128 Korolev
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 
  

function  mapGetPathShell: GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetPathShellUnicode: PWCHAR;       // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

procedure mapGetPathShellUn(path : PWCHAR; size : integer);    // 12/09/12  // 20130128 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

procedure mapGetPathShellEx(Path : GTKPChar; size : integer);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ����� ��� INI-����� ����������

procedure mapSetIniPath(const inipath : GTKPChar);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
procedure mapSetIniPathUn(const inipath : PWCHAR);      // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ����� ��� INI-����� ����������
  // ��� ������ ���������� ����

function  mapGetIniPath: GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapGetIniPathUn: PWCHAR;                    // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


{$IFDEF MULTIPLATFORM }                           // 10/08/07
  //  ��������� ���� � ������ ��������� Sparc
  //  ����������� ��� ������ ������ Windows - Sparc
function  mapGetTurnPath: GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
procedure mapGetTurnPathEx(Path : GTKPChar; size : integer);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
{$ENDIF}

  // ����������/��������� ���� � ���������� ������
  // path - ����� ������ � ��������� UTF-16 c ����� � ����� ��� ���������� ������
  // size - ����� ������ (������) � ������

procedure mapSetDataPathUn(const Path : PWCHAR);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}        // 03/04/2014
external sGisAcces;

procedure mapGetDataPathUn(Path : PWCHAR; size : integer);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}              // 03/04/2014
external sGisAcces;


  // ���������� ����� ��� INI-����� ���������
  // ��� ������ ���������� ����

function  mapSetMapIniName(Map : HMap; const mapininame : GTKPChar): GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapSetMapIniNameUn(Map : HMap; const mapininame : PWCHAR): PWCHAR;  // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ��� INI-����� ���������
  // ��� ������ ���������� ������ ������

function  mapGetMapIniName(Map : HMap) : GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetMapIniNameEx(Map : HMap) : PWCHAR;  // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ��� INI-����� ��������� � ��������� UNICODE
  // name - ����� ������ ��� ���������� ����������
  // size - ������ ������ � ������
  // ��� ������ ���������� 0


function  mapGetMapIniNameUn(Map : HMap; name : PWCHAR; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������������ ��� INI-����� �� ����� ����������� ������         // 17/09/12
  // name - ���� � ������������ ����� ������ ��� ����� ������ � ���-������� ���
  // URL ����������
  // mapininame - ����� ������ ��� ���������� ����������
  // size - ������ ������, ����������������� ��� ���������� ���������� (�� ����� 260 ����)
  // ��� ������ ���������� ����

function  mapBuildIniName(const name : GTKPCHAR; mapininame : GTKPCHAR; size : integer) : integer;  // 20130128 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapBuildIniNameUn(const name : PWCHAR; mapininame : PWCHAR; size : integer) : integer;  // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ���� � ����� ������ ��������������� (RSC)
  // ��� ������ ���������� ����

function  mapSetCommonRscPath(const rscpath : GTKPChar) : GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapSetCommonRscPathUn(const rscpath : PWCHAR) : integer;         // 20130128 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ���� � ����� ������ ��������������� (RSC)
  // ��� ������ ���������� ������ ������

function  mapGetCommonRscPath: GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetCommonRscPathUn: PWCHAR;                    // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������������ ������������ ����� ����� �� ������� �����
  // source - �������� ����;
  // target - ���������� ������ (c:\abc..\klm.ext);
  // size   - ���������� ������ �������� ������ � �������� !!!
  // ��� ������ ���������� ����

function mapPathToShort(const aSource : GTKPChar;  aTarget : GTKPChar; aSize : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
external sGisAcces;
   
function mapPathToShortUn(const source : PWCHAR; target : PWCHAR; size : integer) : integer;         // 20130128 Korolev
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
external sGisAcces;
                                        


  // ��������� "��������" ��� �����
  // templ - ���� � ����������� ���������� (mapGetPathShell)
  // name - ������ ��� �����
  // ���������� ��������� �� "��������" ��� �����
  // ��� ������ ���������� ����

function  mapBuildShortName(const templ, name : GTKPChar) : GTKPChar; // 09/05/05
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapBuildShortNameUn(const templ, name : PWCHAR) : PWCHAR;    // 20130128 Korolev
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� "��������" ��� �����
  // � �������� ���������� ���� ����������� ���� � ����������� ���������� (mapGetPathShell)
  // name  - ������ ��� ����� � UNICODE
  // ���������� ��������� �� "��������" ��� �����
  // ��� ������ ���������� ����

function  mapBuildShellShortNameUnicode(const name : PWCHAR) : PWCHAR; // 09/05/05
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� "�������" ��� �����
  // templ - ���� � ����������� ���������� (mapGetPathShell)
  // name - ��� �����
  // path - "�������" ��� ����� (��������� �� ������ ��� ���������� ����������; MAX_PATH)
  // ��� ������ ���������� ����

function  mapBuildLongName(const templ : GTKPChar; const Name : GTKPCHAR; Path : GTKPChar): GTKPChar; // 28/05/01
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapBuildLongNameUn(const templ, name : PWCHAR; path : PWCHAR) : integer;  // 20130128 Korolev
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ��������� ��������� ���� � ����� (������������ ������)
  // templ  - ���� � ������ (��������; mapGetMapPath(Map : HMap))
  // name   - ������ ��� ����� (c:\abc\def...)
  // target - ����� ������ ��� ��������� ���������� ���� (��� �������; ���� ����� �����)
  // size   - ������ ������
  // ���������� ��������� �� ��������� ���� � ����� (.\def...)
  // ��� ������ ���������� ����

function  mapCutPath(const templ : GTKPChar; const Name : GTKPChar; target : GTKPChar;      // 08/10/04
                                  size:integer) : GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapCutPathUn(const templ : PWChar; const Name : PWChar; target : PWChar;      // 03/04/2014
                                  size:integer) : PWChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������ ��� ����� �� ���������� ����
  // templ  - ���� � ������ (��������; mapGetMapPath(Map : HMap))
  // name   - ��������� ���� (���������� � .\) ��� ������ ����
  // target - ����� ������ ��� ���������� ������� ����� �����
  // size   - ������ ������
  // ���������� ��������� �� ������ ���� (�:\abc\def... ��� \\NAME\abc\...)
  // ��� ������ ���������� ����

function  mapUnionPath (const templ : GTKPChar; const Name: GTKPChar; target : GTKPChar;      // 08/10/04
                                  size:integer) : GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 function  mapUnionPathUn (const templ : PWChar; const Name: PWChar; target : PWChar;      // 03/04/2014
                                  size:integer) : PWChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ������������ �������� ������ ��������� �����
  //  size     - ������ �����: size = 16 (16x16); size = 32 (32x32)
  //  bits     - ������� ������� �����: 32 ����� (16x16); 128 ���� (32x32)
  //  length   - ����� ������� data (10000 ���� - �������)
  // ��������� - �������; ���������� � data
  // ��� ������ ���������� 0

function  mapMarkVectorization(size:integer; bits : GTKPChar; 
   length : integer; var data : TPOLYDATA) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������������� ��������� ������ �� ��������� OEM (DOS) � ANSI (WINDOWS)
  // ��� ������ ���������� 0

function  mapDos2Win(Str : GTKPChar; size : integer): GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������������� ��������� ������ �� ��������� ANSI (WINDOWS) � OEM (DOS)
  // ��� ������ ���������� 0

function  mapWin2Dos(Str : GTKPChar; size : integer): GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

{$IFNDEF LINUXAPI}
  // ������������� ��������� ������ �� ��������� OEM (DOS) � KOI8 (UNIX)
  // ��� ������ ���������� 0

  function  mapDos2Koi8(Str : GTKPChar; size : integer) : GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������������� ��������� ������ �� ��������� KOI8 (UNIX) � OEM (DOS)
  // ��� ������ ���������� 0

  function  mapKoi82Dos(Str : GTKPChar; size : integer) : GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������������� ��������� ������ �� ��������� ANSI (WINDOWS) � KOI8 (UNIX)
  // ��� ������ ���������� 0

function  mapWin2Koi8(Str : GTKPChar; size : integer): GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������������� ��������� ������ �� ��������� KOI8 (UNIX) � ANSI (WINDOWS)  // 07/10/08
  // ��� ������ ���������� 0
function  mapKoi82Win(Str : GTKPChar; size : integer): GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
{$ENDIF}     // LINUXAPI     // 11/12/13

  // ��������� ������� Dib
  // ���������� ���������� ���; ���������� �� ���� ����� ����������� �����
function  mapGetMapScreenDepth: integer ;   // 21/03/03
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 {*********************************************************
 *                                                        *
 *   ������� ������������� ����� �� ����������� �����     *     // 03/04/2014
 *                                                        *
 *   ��������� ������ ����� ������ ���������� �������,    *
 * � ������ ����� �������� ����������� � ����� ���������  *
 *                                                        *
 *********************************************************}

  // ���������� ������� ���������� ������� ��� �����
  // hmap     - ������������� �������� ����� (���������)
  // position - ���������� ������� � ������� ������� ��������� ���������
  // ���� position ����� ����, �� ���������� ��������� �� ��������������


procedure  mapSetMarkerPosition(aMap:HMAP; position: PDOUBLEPOINT);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������� ���������� ������� ��� �����
  // hmap     - ������������� �������� ����� (���������)
  // position - ���������� ������� � ������� ������� ��������� ���������
  // ���� ���������� �� �����������, ��� ��������� ���������, �� ���������� ����

procedure  mapGetMarkerPosition(aMap:HMAP; position: PDOUBLEPOINT);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ���������� ����� �������, ������� ����� ������������
  // ���������� ��� ����� ��������� ������� (������ mapSetMarkerPosition)
  // ��� ������� ��������� ���������� ������� mapGetMarkerPosition
  // call - ����� ���������� ������� (��. maptype.h),
  // parm - ��������, ������� ����� ������� ���������� �������
  // ��� ���������� ������, ������������ ����� ������� ����������,
  // ���������� ��� ��� ������� ������� � �������� �����������
  // call � parm.

procedure  mapSetMarkerPositionCallAndParm(aMap:HMAP; call: TBREAKCALL; parm:Pointer);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  //*********************************************************
  //*                             *
  //*              ������� ���������� �����������            *
  //*                             *
  //*********************************************************/

  // ���������� ���������� ������� ������ �����������       
  // ������� ������ ���������� �� �������� ������
  // ����� �������� ������ ��� ������ ��� ��������� ��������������� ��������
  // ������ ���� ����� 0
  // ���� ����� ����������, �� ������� ����������� ���������, ����� �������
  // �������, �� ������������� �������� ����� ������������� ��������� ��
  // �������� ������
  // ��� ������ ���������� 0

function  mapSetScreenImageSize(width, height : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  SetScreenImageSize(width, height : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������������ ������ ����������� ����� � ������
 // ��������� ������ �������� - mapSetScreenImageSize       // 22/01/08

function  mapGetScreenWidth: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� ������������ ������ ����������� ����� � ������
 // ��������� ������ �������� - mapSetScreenImageSize       // 03/12/09

function  mapGetScreenHeight: integer;                    // 22/03/2010
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������/���������� ������ ��������� �������� �����������
  // ������ � ����������� (50 - 4000). ��� ��������� ����������
  // ������ ��������
  // hdc - �������� �������� ���� ��� ������� ������� ��������
  // ������ � ��������
  // ��� ������ ���������� 0

function  mapGetScreenSize: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapSetScreenSize(size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function mapSetScreenSizeEx(aSize : integer; aHdc : HDC) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 

  // ���������/���������� ����������� ��������������� �����������
  // ������ � ��������� (100 - 2000). ��� ��������� ����������
  // ������ �������� � ������������� �������� �������� ������ ������
  // ��� ������ ���������� 0

function  mapGetScreenScale: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapSetScreenScale( scale : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������/���������� �������� �������� ������ ������ � ������
  // �� ���� (1000 - 100000). ��� ��������� ���������� ������
  // �������� � ������������� ����������� ��������������� ������
  // ��� ������ ���������� 0

function  mapGetScreenPrecision: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapGetHorizontalScreenPrecision() : double;       // 20130128 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function mapGetVerticalScreenPrecision() : double;         // 20130128 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapSetScreenPrecision(value : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

procedure  mapSetScreenPrecisionEx(valueHor: double; valueVer: double); // 09/10/2009
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

//function mapSetScreenPrecisionEx(aSize : integer; aHdc : HDC) : integer;
// _MAPIMP void _MAPAPI mapSetScreenPrecisionEx(double valueHor, double valueVer);


  // �������� ����� ������� ��������� ������

  // ���������� ������ � ��������, ���� ����� ����������� �� ������ ����� ������� ��������� ������
function  mapScreenMeter2Pixel(metric : double) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  //���������� ����� ����������� �� ������, ���� ������ � �������� ����� ������� ��������� ������
function  mapScreenPixel2Meter(pixel : integer) : double;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 //*********************************************************
 //*                             *
 //*           ������� ���������� � ������������            *
 //*                             *
 //*********************************************************/

  // ���������� ������ �� ��������� ���� LZW
  //  datain  - ������ �������� ������
  //  dataout - ������ �������� (������) ������
  //  sizein  - ������ ������� �������� ������
  //  sizeout - ������ ������� �������� (������) ������
  // ���������� ������ ������ ������ � �������� ������
  // ��� ������ ��� ��� ������ ����� ��� �� 20%
  // �������� ������ �� ����������� � ������� ���������� 0.

function  mapCompressLZW(const datain  : GTKPCHAR; sizein: integer;
                         dataout : GTKPCHAR; sizeout: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������������ ������ �� ��������� ���� LZW
  //  datain  - ������ �������� (������) ������
  //  dataout - ������ �������� ������
  //  sizein  - ������ ������� �������� (������) ������
  //  sizeout - ������ ������� �������� ������
  // ��� ������ ���������� 0

function  mapDecompressLZW(const datain  : GTKPCHAR; sizein: integer;
                           dataout : GTKPCHAR; sizeout: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ����������� �� ��������� JPEG     // 11/12/12
  // din       - ������ �������� ������
  // width    - ������ ����������� (��������)
  // height   - ������ ����������� (��������)
  // bit      - ���������� ��� �� ������� (����������� ������ ����������� � ����������� ��� �� �������, ������ 24)
  // compressionValue - ������� ������ �������� (1-100, 1-������������ ������,
  // 100-������ ��� ������ ��������), ������������� �������� 60.
  // dout     - ������ �������� ������
  // sizeout - ������ ������� �������� ������
  // ��� ������ ���������� 0

function  mapCompressJPEG(const din : GTKPCHAR; width, height, bit, compressionValue : integer;   // 20130128 Korolev
                          dout : GTKPCHAR; sizeout : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ������������ ����������� �� ��������� JPEG     // 11/12/12
  // din       - ������ �������� (������) ������
  // sizein   - ������ ������� �������� (������) ������
  // width    - ������ ����������� (��������)
  // height   - ������ ����������� (��������)
  // bit      - ���������� ��� �� ������� (����������� ������ ����������� � ����������� ��� �� �������, ������ 24)
  // dout     - ������ �������� ������
  //  sizeout - ������ ������� �������� ������
  // ��� ������ ���������� 0

function  mapDecompressJPEG(const din : GTKPCHAR; sizein, width, height, bit : integer;          // 20130128 Korolev
                            dout : GTKPCHAR; sizeout : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;							

 //**********************************************************
 //*                             *
 //*                   �������� �������                     *
 //*                             *
 //**********************************************************

// 11/12/13 {$IFDEF WIN32}
{$IFNDEF LINUXAPI}          // 11/12/13


  // ��������� ����� ����� ������

function  GetKeyNumber: Cardinal;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
{$endif}


//*********************************************************
// *                                                        *
// *           ������� ������� � ���-�������                *
// *                                                        *
// *     ��� �������� ����� �� ���-������� � �������        *
// *      ������� �������� ���� ���������� ������ ����      *
// *         "HOST#����#����#ALIAS#��������_���_�����"      *
// *    (��������: "HOST#233.47.123.99#2047#ALIAS#Noginsk") *
// *                                                        *
// *  �������� ��� ����� �� �������� ����� � �������� ����� *
// *     ������ "HOST#" � "ALIAS#" �������� ���������       *
// *           ������� ��� ��������� � ���-�������          *
// *                                                        *
// *********************************************************/

  // ��������� �������� �� ��� ��������������� ������ �� �������
  // ���� ��, �� ���������� ��������� �������� (1 - ����������
  // ������ ��� ����� �������, 2 - �������� ��� �������)

function  mapIsAliasName(const name : GTKPCHAR): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapIsAliasNameUn(const name : PWCHAR): integer;        // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ��������� ��� ������ ������ �� ������ ������ �����, ���������� ��� �����
  // ���������� ��������� �� ��� ������ (������ ������ ����� ALIAS#) ��� 0

function  mapGetDataNameFromAlias(const name : GTKPCHAR): GTKPCHAR;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetDataNameFromAliasUn(const name : PWCHAR): PWCHAR;   // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������������ ��� ������� �� �������                    // 03/10/11
  // � ������� "HOST#����#����#ALIAS#��������_���_�����"
  // host  - ��� �����
  // port  - ����� �����
  // alias - ��� ������� (�������� ��� �����)
  // name  - ��� ������ ��� ���������� ���������
  // size  - ������������ ������ � ������ ������
  // ��� ������ � ���������� ���������� ����

function  mapBuildAliasName(const host : GTKPCHAR;
                            port : integer;
                            const alias : GTKPCHAR;
                            name : GTKPCHAR;
                            size : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapBuildAliasNameUn(const host : PWCHAR;           // 03/04/2014
                            port : integer;
                            const alias : PWCHAR;
                            name : PWCHAR;
                            size : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ����� ����������� � ��� ��������
  // ��� ���������� ����������� ���������� ����

function  mapActiveServerCount: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ��������� ����������� � �������
  // number - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // ��� ��������� ��������\���������� ������ � ��� ������� �������������
  // ����� ������ ������ �������� ������ ��������� ��������� �����������
  // � ��� ������ �������� ��������� ���������
  // ���� ����� ������ �������� ������ � ������ "HOST#..." ��� "ALIAS#..."
  // ����������� �� �����������, �� ����� ��������, ��� ������ ������� �
  // ������� ���������� ��������� ����������
  // ���� ����������� � ������� ����������� - ���������� ��������� ��������

function  mapIsServerActiveEx(number : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

    // ��������� ������ � ��������� ����������� ��������� �������
  // ���� ���������� �������� - ���������� ������� ��������

function  mapIsServerMonitoringEnable(number : integer): integer;    // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ��������� ������ ��� ������� �� ������ ����������
  // number - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // ���������� ����������������� ����� ������ ��� ������,
  // ��������: 0x00040503 ������������� ������ 4.5.3
  // ��� ������ ���������� ����

function  mapGetServerVersion(number : integer): integer;            // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ������� ���������� � ��������� �������� ���������� (����������)
  // versin  - ����� ��� ���������� ������ � ������ � ������� ��� �������
  // size    - ������ ������ (�� ����� 80 ����)
  // state   - ��������� ��� �������, ���������� � ���������� �������,
  //           ���� ��������� �� ����������, �� ������������ ����������� �����
  // ����� ���������� ��������� ������ ���������� ���������� ������� �����
  // ������ mapFreeServerState � ����������, ���������� � mapGetServerState
  // ��� ������ ���������� ����

function  mapGetServerState(number : integer; version: PWChar; size,state:Integer): PGSMONITOR;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}                   // 03/04/2014
external sGisAcces;



  // ���������� ������� ����� ��������� ������ ����������� ��������� �������

procedure  mapFreeServerState(buffer:PGSMONITOR);         // 03/04/2014

  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


  // ��������� ��������� �� ����������� � ����������� ������������
  // ��� ��������� ����� ������ ������
  // name - ����� � ������� "HOST#����:����#ALIAS#��������_���_�����",
  //        ��� "HOST#����" ��� "HOST#����:����"
  // ��� �������� �������� ���������� ����� �����������
  // ��� ������ ���������� ����

function  mapCheckConnectForAlias(const name : GTKPCHAR): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapCheckConnectForAliasUn(const name : PWCHAR): integer;  // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ����� ���������� � ���-��������
  // name - ��� ����� (�� 256 ��������),
  //        ��� ������ ������ "XXX.XXX.XXX.XXX"
  // ���� �������� ����� ���� - ������ ������ �� ��������� ����� "localhost".
  // port - ����� ����� �� 1024 �� 65536, �� ��������� - 2047 (���� port = 0)
  // � ������ ������ ������������ ����������� ���������� ��� ���������� �����
  // ��� ������ ���������� ����

function  mapOpenConnect(const name : GTKPCHAR; port : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapOpenConnectUn(const name : PWCHAR; port : integer): integer;   // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ����� ���������� � ���-��������
  // name - ��� ����� (�� 256 ��������),
  //        ��� ������ ������ "XXX.XXX.XXX.XXX"
  // ���� �������� ����� ���� - ������ ������ �� ��������� ����� "localhost".
  // port - ����� ����� �� 1024 �� 65536, �� ��������� - 2047 (���� port = 0)
  // cansleep - ���������� �� �������� ������������ (�������) ����������,
  //            ��� ���������� ����������� ������� � �������
  // ��� ������ ���������� ����

function  mapOpenConnectEx(const name : GTKPCHAR; port, cansleep : integer) : integer;  // 20130128 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapOpenConnectExUn(const name : PWCHAR; port, cansleep : integer) : integer;   // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� - ����� �� ������� ����������
  // ��� ������ (���������� �� �������) ���������� ����
  // ��� ��������� ���������� ���������� "-1"
  // ���� ���������� ����� ���� �������, �� ���������� ������������� ��������

function  mapCanCloseConnect(number : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ���������� � ���-��������
  // number - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // ��� ������ (���������� �� �������) ���������� ����
  // ��� ��������� ���������� ���������� "-1"
  // ��� �������� ���������� ���������� ������������� ��������

function  mapCloseConnect(number : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // �������� ��������� ���������� � ���-��������
  // ���������� �� �������� ���� �� �������
  // number - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // name - ��� ����� (�� 256 ��������),
  //        ��� ������ ������ "XXX.XXX.XXX.XXX"
  // ���� �������� ����� ���� - ������ ������ �� ��������� ����� "localhost".
  // port - ����� ����� �� 1024 �� 65536, �� ��������� - 2047 (���� port = 0)
  // ��� ������ ���������� ����

function  mapSetConnectParametersEx(number : integer;
                       const name : GTKPCHAR; port : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapSetConnectParametersExUn(number : integer;                     // 03/04/2014
                       const name : PWCHAR; port : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ����� ����� ��� ����� � ���-��������
  // ����� ����� �� 1024 �� 65536, �� ��������� - 2047
  // number - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()


function  mapGetConnectPortEx(number : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ���\����� �����
  // ���� ���� ���������� ����� ����� - ������������ �������� 1,
  // ���� ��� ����� - ������������ �������� 2.
  // number - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // ���� ����������� ��� �������� - ������������ ����� �����
  // name - ����� ������ ��� ���������� ����������
  // size - ������ ������ (��� ����� ����� �� ����� 256)
  // ��� ������ ���������� ����

function  mapGetConnectHostEx(number : integer;
                              name : GTKPCHAR; size : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetConnectHostExUn(number : integer;
                              name : PWCHAR; size : integer): integer;     // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������������� ������������
  // number - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // ���� ���������� � �������� �� ���� ����������� -
  // �������� ����������� � �������������� ����� �����������
  // ������ ������ ������������ � ������������� ���� �� ��������� MD5 (� ���� ����)
  // ��� ��������� ���� ������ ������� ������������ ������� svStringToHash 
  // (������� � gisdlgs.h)  
  // ��� ������ ���������� ����

function  mapRegisterUserEx(number : integer;
                              parm : PMCUSERPARM): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������������� �������� ������������ �� ��� ������������ ��� �������
  // number - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // ���� ���������� � �������� �� ���� ����������� -
  // �������� ����������� � �������������� ����� �����������
  // ��� ������ ���������� ����

function  mapRegisterSystemUserEx(number : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� � ������ ��������� ����������� ������������
  // number - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // ����� �������� ���������� ��������� �� ������� ����������
  // ����������� � ��� ������������ �������� ����� ����� ��������
  // ��������� mapRegisterUser()

procedure  mapUnRegisterUserEx(number : integer);
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ��� ����������� ������������
  // number - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // ���� ����������� ������������ ����������� ����� ������� mapRegisterSystemUserEx,
  // �� ������������ ������������� ��������

function  mapGetRegisterUserType(number : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� �� ��� ������ ����� ������ �������������� ����
  // number - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // type   - ��� ������ (0 - ����� ������, 1 - ������ ������������)
  // buffer - ����� ������ � ������ (XML)
  // size   - ����� ������ ������
  // ��� ������ ���������� ����

function  mapSendMapsEx(number : integer; atype : integer;
                        const buffer : GTKPCHAR; size : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������ ��������� ������������ ���� �� ���-�������
  // number - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // buffer - ����� ������ ��� ���������� ������ ����,
  //          ��������� TMCMAPLIST ������� � maptype.h
  // ���� buffer ����� ����, ���������� ������ ����������� ������� ������
  // length - ����� ���������� ������� ������
  // ���������� ����� ������ ��������� ������ ��� 0

function  mapGetMapListforUserEx(number : integer;
                                 buffer : PTMCMAPLIST; 
                                 length : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������ ��������� ������������ ������� �� ���-�������
  // number - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // buffer - ����� ������ ��� ���������� ������ �������,
  //          ��������� TMCMAPLIST ������� � maptype.h
  // length - ����� ���������� ������� ������
  // ���������� ����� ������ ��������� ������ ��� 0

function  mapGetAlsListforUserEx(number : integer;
                                 buffer : PTMCMAPLIST; 
                                 length : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������ ��������� ������������ ������ �� ���-�������
  // number - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // buffer - ����� ������ ��� ���������� ������ ������,
  //          ��������� TMCMAPLIST ������� � maptype.h
  // length - ����� ���������� ������� ������
  // ���� buffer ����� ����, ���������� ������ ����������� ������� ������
  // ���������� ����� ������ ��������� ������ ��� 0

function  mapGetMtwListforUserEx(number : integer;
                                 buffer : PTMCMAPLIST;
                                 length : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������ ��������� ������������ ������� �� ���-�������
  // number - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // buffer - ����� ������ ��� ���������� ������ �������,
  //          ��������� TMCMAPLIST ������� � maptype.h
  // length - ����� ���������� ������� ������
  // ���� buffer ����� ����, ���������� ������ ����������� ������� ������
  // ���������� ����� ������ ��������� ������ ��� 0

function  mapGetRswListforUserEx(number : integer;
                                 buffer : PTMCMAPLIST;
                                 length : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ��� ������������, ��������������� � ���-�������
  // number - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // ��� ������ ���������� ������ ������

function  mapGetCurrentUserNameEx(number : integer): GTKPCHAR;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ������� ��� ������������ ������
  // number - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // ����� - 1, ������ - 2, ������� - 3, ������  - 4
  // ��� ������ ���������� ����

function  mapSetActiveDataType(number : integer;
                                 atype : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������� ��� ������������ ������
  // number - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // ����� - 1, ������ - 2, ������� - 3, ������  - 4
  // ��� ������ ���������� ����

function  mapGetActiveDataType(number : integer): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ���� � ����� ��� �������� ���������� ������ � ��� ������ 2011

function  mapGetCachePath: GTKPCHAR;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetCachePathUn: PWCHAR;                   // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������� ���� � ����� ��� �������� ���������� ������ � ��� ������
  // ���� ���������� �� ���������� ���� � ����� ��� ����������� ������, 
  // �� ��� ������������� ����� ��������� ������ ��������� ����� Temp 
  // � ����� Panorama.Cache.
  // ��� ������ ���������� ����

function  mapSetCachePath(const path : GTKPCHAR): integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapSetCachePathUn(const path : PWCHAR) : integer;            // 20130128 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // �������� ��� ������ ��� ���� ���� �������� ���������,
  // �������� � ��� �������
  // Map  -  ������������� �������� ������
  // ��� ������������� ��������� ��� ���������� ����� �� ��� �������
  // ��� ����������� �������� ����� ����������� ����������,
  // � ������� ����������� ��������� � ������
  // ����� ��� ����������� (�������������) � ������������
  // � �������� ���������� �� ��� �������

procedure mapClearDocCache(Map : HMAP);                                // 20130128 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // �������� ��� ������ ��� �������� ���������������� �����,
  // �������� � ��� �������
  // Map  -  ������������� �������� ������
  // Site - ������������� �������� ���������������� �����
  // ��� ������������� ��������� ��� ���������� ����� �� ��� �������
  // ��� ����������� �������� ����� ����������� ����������,
  // � ������� ����������� ��������� � ������
  // ����� ��� ����������� (�������������) � ������������
  // � �������� ���������� �� ��� �������

procedure mapClearSiteCache(Map : HMAP; Site : HSITE);                 // 20130128 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������� �� ����� �� ������� ��� ��������
  // hmap -  ������������� �������� ������
  // hSite - ������������� �������� ���������������� �����
  // (��� ������� (��������) ����� hSite = hMap)
  // ���� ����� ������� �� ������� ���������� ��������� ��������

function  mapIsMapFromServer(Map : HMAP; Site : HSITE):integer;                 // 03/04/2014
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


 //**********************************************************
 //*                                                        *
 //*  ���������� �������. ���������� � ������� ����������.  *
 //*                                                        *
 //*      ���������� ������ ����� ������� �� ��� ������� -  *
 //*           "ALIAS#��������_���_�����"                   *
 //*                                                        *
 //**********************************************************

  // ���������� ��������� ���������� � ���-��������
  // ���������� �� �������� ���� �� �������
  // HostName - ��� ����� (�� 256 ��������); ���� ishost �� ����� 0
  //        ��� ������ ������ "XXX.XXX.XXX.XXX"
  // ���� �������� ����� ���� - ������ ������ �� ��������� ����� "localhost".
  // port - ����� ����� �� 1024 �� 65536; �� ��������� - 2047 (���� port = 0)
  // ��� ������ ���������� ����

function mapSetConnectParameters(aname : GTKPChar; aport, aishost : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


  // ��������� ����� ����� ��� ����� � ���-��������
  // ����� ����� �� 1024 �� 65536; �� ��������� - 2047

function  mapGetConnectPort: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ���\����� �����
  // ���� ���� ���������� ����� ����� - ������������ �������� 1;
  // ���� ��� ����� - ������������ �������� 2.
  // ���� ����������� ��� �������� - ������������ ����� �����
  // name - ����� ������ ��� ���������� ����������
  // size - ������ ������ (��� ����� ����� �� ����� 256)
  // ��� ������ ���������� ����

function mapGetConnectHost(aname : GTKPChar; asize : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 

  // ���������������� ������������
  // ���� ���������� � �������� �� ���� ����������� -
  // �������� ����������� � �������������� ����� �����������
  // ��������� USERPARM ������� � maptype.h
  // ��� ������ ���������� ����

function mapRegisterUser(var aparm : TMCUSERPARM) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

 // ���������������� ������������ ������������ ������� �� ��� �������
  // ���� ���������� � �������� �� ���� ����������� -
  // �������� ����������� � �������������� ����� �����������
  // ��� ������ ���������� ����

function mapRegisterSystemUser : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

  // ������� � ������ ��������� ����������� ������������
  // ����� �������� ���������� ��������� �� ������� ����������
  // ����������� � ��� ������������ �������� ����� ����� ��������
  // ��������� mapRegisterUser() ��� mapRegisterSystemUser()

procedure mapUnRegisterUser;           // 05/11/08
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ��� ������������; ��������������� � ���-�������
  // ��� ������ ���������� ������ ������

function  mapGetCurrentUserName: GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ��������� ������ ��������� ������������ ���� �� ���-�������
  // �� ������ ������� ���������� ���������������� ������������ 
  // �������� mapRegisterUser.
  // buffer - ����� ������ ��� ���������� ������ ����;
  //          ��������� TMCMAPLIST ������� � maptype.h
  // length - ����� ���������� ������� ������
  // ��� �������� ��������� ����� ��������� MAPAPI � ��
  // ����� ������ ���� �������� ������� "ALIAS#"
  // ���������� ����� ������ ��������� ������ ��� 0

function mapGetMapListforUser(abuffer : PTMCMAPLIST; alength : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


  // ��������� ������ ��������� ������������ ������ �� ���-�������
  // buffer - ����� ������ ��� ���������� ������ ����;
  //          ��������� TMCMAPLIST ������� � maptype.h
  // length - ����� ���������� ������� ������
  // ��� �������� ��������� ������� ��������� MAPAPI � ��
  // ����� ������ ���� �������� ������� "ALIAS#"
  // ���������� ����� ������ ��������� ������ ��� 0

function mapGetMtwListforUser(abuffer : PTMCMAPLIST; alength : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

  // ��������� ������ ��������� ������������ ������� �� ���-�������
  // buffer - ����� ������ ��� ���������� ������ ����;
  //          ��������� TMCMAPLIST ������� � maptype.h
  // length - ����� ���������� ������� ������
  // ��� �������� ���������� ������ ��������� MAPAPI � ��
  // ����� ������ ���� �������� ������� "ALIAS#"
  // ���������� ����� ������ ��������� ������ ��� 0

function mapGetRswListforUser(abuffer : PTMCMAPLIST; alength : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

  // ��������� ������ ��������� ������������ ������� �� ���-�������    // 06/12/10
  // buffer - ����� ������ ��� ���������� ������ �������,
  //          ��������� TMCMAPLIST ������� � maptype.h
  // length - ����� ���������� ������� ������
  // ���������� ����� ������ ��������� ������ ��� 0

function mapGetAlsListforUser(buffer : PTMCMAPLIST; length : integer): integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


  // ��������� ������� �� ����� �� ������� ��� ��������
  // hmap -  ������������� �������� ������
  // hSite - ������������� �������� ���������������� �����
  // (��� ������� (��������) ����� hSite = hMap)
  // ���� ����� ������� �� ������� ���������� ��������� ��������

//function mapIsMapFromServer(ahmap : HMAP; ahsite : HSITE) : integer;
  //{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  //external sGisAcces;

  // ��������� ��������� ����������� � �������
  // hmap -  ������������� �������� ������ ��� 0
  // ��� ��������� ��������\���������� ������ � ��� ������� �������������
  // ����� ������ ������ �������� ������ ��������� ��������� �����������
  // � ��� ������ �������� ��������� ���������
  // ���� ����� ������ �������� ������ � ������ "ALIAS#..."
  // ����������� �� �����������, �� ����� ��������, ��� ������ ������� �
  // ������� ���������� ��������� ����������
  // ���� ����������� � ������� ����������� - ���������� ��������� ��������

function mapIsServerActive(ahmap : HMAP) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
external sGisAcces;


  // ��������� ������ ����� �� �������, ��������� ��� ������ ������            // 02/07/14
  // anumber - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // afolder - ���� � �����, � ������� ������������� ������ ������ � ����� ��� 0
  // ��������, "Data\\Maps"
  // ���� folder ����� 0, �� ������������� ������ ������� ���� ��������� �����
  // allfiles - ������� ������� ���� ������ � ����� folder, ���� �� ����������,
  // �� ����� ����� ������ ���������� ����� � ������ MAP,SIT,SITX,RSC,MTW,MTQ,RSW
  // aparm   - ����� ������ ��� ���������� ������ ����������� ������ ��� 0
  // ���� parm ����� 0, �� ������������� ������ ������, ��������� ��� ���������� ������
  // asize - ������ ������ ��� ���������� ������
  // ������ ������ ����������� ������ ��� ������ � �����, ��������������� �������������
  // � �������� ����� ��� ��������
  // ��� �������� ���������� ���������� ������ ��������������� ������
  // ���� ������ ������ ��������� ������ ������, �� ������ ������� �� ���������.
  // ����� ����� �������� ������� ����� � ��������� ������ ��������
  // ��� ������ ���������� ����                         

function mapGetFolderList(anumber : integer; const afolder : PWCHAR; allfiles : integer;
                                          aparm : PTMCDATALIST; asize : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
external sGisAcces;


  // ������� ����� �� ������� ������������ ������ ��������� �����            // 26/06/14
  // anumber - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // afolder - ���� � ����������� �����
  // ��������, "Data\\Maps" ��� "Data/Maps",
  // ��� "Data" - �������� ����� � ���������� ��� �������, "Maps" - ����������� �����
  // ��� ������ ���������� ����

function mapCreateFolderOnServer(anumber : integer; const afolder : PWCHAR) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
external sGisAcces;


  // ������� ����� �� ������� ������������ ������ ��������� �����            // 27/06/14
  // anumber - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // afolder - ���� � ��������� �����
  // ��������, ��� �������� ����� "Maps": "Data\\Maps" ��� "Data/Maps",
  // ��� "Data" - �������� ����� � ���������� ��� �������, "Maps" - ��������� �����
  // adeletefiles - ������� ��� ����� � ����� (���� ������ ��������� ��������)
  // adeletefolders - ������� ��� �������� � ������� � ��� (���� ������ ��������� ��������)
  // ��� ������ ���������� ����

function mapDeleteFolderOnServer(anumber : integer; const  afolder : PWCHAR;
                                                 adeletefiles : integer; adeletefolders : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
external sGisAcces;


  // ������������� (�����������) ���� ��� ����� �� �������
  // anumber - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // asource - ���� � ����������� ����� ��� �����
  // ��������, "Data\\Maps\\image.sitx",
  // ��� "Data" - �������� ����� � ���������� ��� �������, "image.sitx" - ������������ ����
  // atarget - ����� ���� � ����� ��� �����
  // ��������, "Storage\\Roads\\road_M4.sitx",
  // ��� ������ ���������� ����

function mapRenameFileOnServer(anumber : integer; const asource : PWCHAR; const atarget : PWCHAR) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
external sGisAcces;


  // ��������� ���� �� �������                                               // 26/06/14
  // anumber - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // afolder - ���� ��� ���������� �����, ���� ������ ���������� � ������ �����,
  //          ��������� ��� ������ ������
  // ��������, "Data\\Maps" ��� "Data/Maps",
  // ��� "Data" - �������� ����� � ���������� ��� �������
  // afile   - ���� � �����, ������� ����� ������� � ����� �� ������� (���� ������
  //          ��������� ������, � �� ����������� ���).
  // ��� �����, ���������� � �������� ������/������ �����������.
  // ��� ������ ���������� ����

function mapSaveFileOnServer(anumber : integer; const afolder : PWCHAR;
                                             const afile : PWCHAR) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
external sGisAcces;


  // ��������� ����� �� �������                                               // 02/07/14
  // anumber - ����� ��������� ����������� � ��� ������� �� 1 �� mapActiveServerCount()
  // afolder - ���� ��� ���������� �����, ���� ������ ���������� � ������ �����,
  //          ��������� ��� ������ ������
  // ��������, "Data\\Maps" ��� "Data/Maps",
  // ��� "Data" - �������� ����� � ���������� ��� �������
  // ahMap -  ������������� �������� ������
  // ahSite - ������������� �������� ���������������� �����
  // (��� ������� (��������) ����� hSite = hMap)
  // arscsave � ������� ������������� ���������� ����� RSC �� ������
  // ��� ������ ���������� ����

function mapSaveMapOnServer(anumber : integer; const afolder : PWCHAR;
                                            ahmap : HMAP;  ahSite : HSITE; arscsave : integer) : integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
external sGisAcces;





 //**********************************************************
 //*                                                        *                          
 //*    ������� �����������/������������� ������            *
 //*                                                        *
 //**********************************************************

   // �������� ������������� ��� ������� �����������           // 17/10/12
   // key  - ������, ���������� �������� ���� ��� ����������� ������
   //        ���� ������ ����� ��������� ����������� ����������,
   //        ��������, ������� �������������� ������ ������������ � ��������
   //        ������� �� ��������� MD5
   // size - ����� ������ (������ 16 ����)
   // ����� ����������� �����������/�������������� ����� ���������� �������
   // �������� mapDeleteCoder
   // ��� ������ ���������� ����

function mapCreateCoder(const key : GTKPCHAR; size : integer) : HHANDLE;            // 20130128 Korolev
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
external sGisAcces;

   // ������������ ������� ������ �������� ������ (����� ������� ������ 16)
   // hcoder - ������������� ��� ������� �����������
   // memory - ����� ������� ������, ������� ����� ������������
   // size   - ������ ������� ������ ��� �����������, ������� 16 ������
   // ��� ����������� ����������� �������� XOR � ������������ ������ ������
   // ��������� ����� �������� ��� ����������� ��� ����������� ���������� ������
   // ��� ������ ���������� ����

function mapCoderOn(coder : HHANDLE; memory : GTKPCHAR; size : integer) : integer;  // 20130128 Korolev
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
external sGisAcces;

   // ������������� ������� ������ �������� ������ (����� ������� ������ 16)
   // hcoder - ������������� ��� ������� �����������
   // memory - ����� ������� ������, ������� ����� ������������
   // size   - ������ ������� ������ ��� �����������, ������� 16 ������
   // ��� ����������� ����������� �������� XOR � ������������ ������ ������
   // ��������� ����� �������� ��� ����������� ��� ����������� ���������� ������
   // ��� ������ ���������� ����

function mapCoderOff(coder : HHANDLE; memory : GTKPCHAR; size : integer) : integer; // 20130128 Korolev
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
external sGisAcces;

// ���������� ������� ����� ����������� �����������/��������������

procedure mapDeleteCoder(coder : HHANDLE);                                          // 20130128 Korolev
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
external sGisAcces;


 { /*********************************************************
 *                                                        *                            // 03/04/2014
 *                ���������� �������                      *
 *                                                        *
 *********************************************************/   }


  // ������� ������ � �������������� ������������ �� ����     // 06/10/08
  // (���������;���������;���������...)
  // name - ��� ������������ ����� (MAP; SIT; MTW; RSW; MPT)
  // mode - ����� ������/������ (GENERIC_READ; GENERIC_WRITE ��� 0)
  // GENERIC_READ - ��� ������ ������ �� ������; ��� ���� �� �����������
  // ����� \Log\name.log � \Log\name.tac - �������� ������ � ������ ����������
  // error - ����� ���������� ������� ���������� �������� ��� ������
  //        (����� HMAP ����� 0) ��� 0; ���� ������ ��������� � maperr.rh
  // ��� ������ ���������� ����

function  mapOpenDataEx(const name : GTKPChar; mode: integer; var error: integer):HMap; // 30/09/2009
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ������ � �������������� ������������ �� ����
  // (���������;���������;���������...)
  // name - ��� ������������ ����� (MAP; SIT; MTW; RSW; MPT)
  // mode - ����� ������/������ (GENERIC_READ; GENERIC_WRITE ��� 0)
  // GENERIC_READ - ��� ������ ������ �� ������; ��� ���� �� �����������
  // ����� \Log\name.log � \Log\name.tac - �������� ������ � ������ ����������
  // ��� ������ ���������� ����

function mapOpenData(const mapname:GTKPChar;mode:integer=0):HMap;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

  // ������� ��������� ������ (���������� mapOpenData)
  // ���������� ������������� �������� ��������� ����� (TMapAccess*)
  // mapname - ��� ����� MAP;
  // mode - ����� ������/������ (GENERIC_READ; GENERIC_WRITE ��� 0)
  // GENERIC_READ - ��� ������ ������ �� ������; ��� ���� �� �����������
  // ����� \Log\name.log � \Log\name.tac - �������� ������ � ������ ����������
  // ��� ������ ���������� ����

function mapOpenMap(const mapname:GTKPChar;mode:integer=0):HMap; //22/09/2009 // 30/09/2009
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


  // �������� ������������ ���������� ������ �, ���� ����, �� ����������
  // ��������� �� �������� ���������� 0 - ������ ������������� ���������
  //                                  1 - �������������
  // ��������� MAPREGISTER, LISTREGISTER ������� � mapcreat.h
  // ��� ������ ���������� ����

function  CheckAndUpdate(var mapreg : TMAPREGISTER; var listreg : TLISTREGISTER; priority: integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


   // ������� ����� ����� (����� ��������� �����)
  // mapname - ������ ��� ����� �����
  // rscname - ������ ��� ����� ��������
  // ���������� ������������� �������� ��������� �����
  // ��������� MAPREGISTER � LISTREGISTER ������� � mapcreat.h
  // ��� ������ ���������� ����
function  mapCreateMap(const mapname, rscname: GTKPChar; var mapreg : TMAPREGISTER; var Sheet : TLISTREGISTER):HMap;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ������� ���������������� �����
  // mapname - ������ ��� ����� �����
  // rscname - ������ ��� ����� ��������
  // ���������� ������������� �������� ��������� �����
  // ��������� CREATESITE ������� � maptype.h
  // ��� ������ ���������� ����
function mapCreateSite(const mapname:GTKPChar;const rscname:GTKPChar;
                       var createsite : TCREATESITE):HMap;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function mapCreateSiteEx(const mapname : GTKPChar; const rscname : GTKPChar; // 26/06/2007
                       var createsiteex : TCREATESITEEX) : HMap;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


  // ������� ���������������� ����� �� �������� �����
  // hmap -  ������������� �������� ������
  // mapname - ������ ��� ����� �����
  // rscname - ������ ��� ����� ��������
  // ��������� CREATESITE ������� � maptype.h
  // ���������� ������������� �������� ���������������� �����
  // ��� ������ ���������� ����

function mapCreateAndAppendSite(map:HMAP;
                                const mapname:GTKPChar;const rscname:GTKPChar; // 22/09/2009
                                var createsite:TCREATESITE):HSite; // 30/09/2009
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;


 // ���������� ��������� ����� DIB � ����             // 06/02/06
 // flag - ���������� ���� �� ��������� �������� (1) ��� ����������� ���������� (0)
 // ��-��������� ��� ���������� ���� ����� ��������������� ���������
 // ��� ������������ ��������. ��� ������� ���� ��� ������� ������ �����������
 // (����� ������ - ��������). ��������� ������� ����� ����� ��������� ���;
 // ����� � ��� ����������� ������ �������.
 // �� ��� ��������� ��������� ���������� ����� ������ ����� ��������� ��������;
 // ������� ��� ����� ���������.
 // ������������ ������ ����� ������ ��������������� ����� ������������; � �����
 // ��������������� ���������� ��������.
 // ���������� ���������� ��������

function  mapSetPaintStep(Map : HMap; flag : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

 // ��������� �������� ���������� ������ DIB � ����
 // ��� ������ ���������� ����

function  mapGetPaintStep(map : HMAP) : integer;                       // 20130127 Korolev
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;


{$IFNDEF LINUXAPI}
  // ��������� ������ � ��������� ��������
  // ���� ������ �������� - ��������� ������� �� ���������
  // � �� ��������������

function  GetGroupObjectsAccess: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  SetGroupObjectsAccess(access : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

{$endif}



//**********************************************************
 //*                             *
 //*          ������� �������/��������� �������   ����������         *
 //*                             *
 //**********************************************************

// 11/12/13 {$IFDEF WIN32}
{$IFNDEF LINUXAPI}          // 11/12/13
  // ���������/���������� ����� �����
  // ������� ���������� ��� �� Windows
  // technology - ��� ���������� (DT_RASDISPLAY; DT_PLOTTER)
  //              ������������ �������� GetDeviceCaps(...)
  // index      - ����� ������ (0-4)
  // fontname   - ��������� �� ��� ������; �������� 32 �������;
  //              ������� ����� ������ 0x00

function mapGetMapFont(technology:integer;index:integer= 0):GTKPChar;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;
function mapSetMapFont(technology:integer;index:integer;
                       fontname:GTKPChar):GTKPChar;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 
function GetMapFont(technology : integer; index : integer = 0) : GTKPChar;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function SetMapFont(technology:integer;index:integer;
                    fontname:GTKPChar):GTKPChar;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function  mapGetMapFontName(technology,index : integer; Name : GTKPChar;
 size : integer) : integer;
{$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapSetMapFontName(technology, index : integer; Name : GTKPChar; size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������/���������� ��������� ������ �����
  // technology - ��� ���������� (DT_RASDISPLAY; DT_PLOTTER)
  //              ������������ �������� GetDeviceCaps(...)
  // index      - ����� ������ (0-4)
  // code       - (RUSSIAN_CHARSET; DEFAULT_CHARSET; ANSI_CHARSET)

function  mapGetMapFontCharSet(technology, index : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapSetMapFontCharSet(technology,index,code : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

// ��������� ����� ������ (0-4)

function mapGetMapFontIndex(technology:integer;fontname:GTKPChar):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces;

function GetMapFontIndex(technology:integer;fontname:GTKPChar):integer;
  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};
  external sGisAcces; 

  // ���������/���������� ����� ����� ��������

function  mapGetDialogStringFont: GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapSetDialogStringFont(fontname : GTKPChar) : GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  GetDialogStringFont: GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  SetDialogStringFont(fontname : GTKPChar): GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������/���������� ����� ������ ��������

function  mapGetDialogTableFont :GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  mapSetDialogTableFont(fontname : GTKPChar): GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  GetDialogTableFont: GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;
function  SetDialogTableFont(fontname : GTKPChar): GTKPChar;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������/���������� ������ ������ ����� � ������ ��������

function  mapGetDialogStringFontSize: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetDialogTableFontSize: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapSetDialogStringFontSize(size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapSetDialogTableFontSize(size : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

  // ���������/���������� ������� �������� ������ ����� � ������ ��������   // 20/04/06
function  mapGetDialogStringFontCharSet: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapGetDialogTableFontCharSet: integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapSetDialogStringFontCharSet(charset : integer) : integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

function  mapSetDialogTableFontCharSet(charset : integer):integer;
 {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
external sGisAcces;

{$endif}  // LINUXAPI  // 11/12/13 // WIN32API





implementation

end.
