//*******************************  MapType.pas *******************************
//*                           G I S T O O L K I T
//****************************************************************************
//                : ���������� ����� � ����������
//
// Edentical for 11 and 12 version GTK
//****************************************************************************
// 03/12/2017 ** ����������   ** mapgdi
// 07/09/2017 ** ����������   ** SHEETFRAMEINCODE
// 06/04/2017 ** ������� �.�. ** ������ ��� TPartInfoOld, ��������� ����� TPartInfo � TDOCINFOMATION
// 12/03/2017 ** ��������     **     HSTATISTIC , sGisRSCTOOLS
// 17/01/2017 ** �������� �.  ** BUILDMTW, ml_last =7
// 12/10/2016 ** ��������     ** TMAPPRINTPARM, TMAPPRINTPARMEX ��������� ������ FileName
// 13/09/2016 ** �������� �.  ** TMAPLISTPARM
// 23/06/2016 ** ��������     **   (inc = h)
// 21/06/2016 ** ��������     **   (inc = h)
// 16/06/2016 ** ��������     **   (inc = h)
// 07/06/2016 ** ��������     **   (inc = h)
// 25/05/2016 ** �����������  ** vcl.Graphics
// 04/04/2016 ** ����� �      ** ������� sGisDBMapDll
// 28/01/2016 ** ���������� � ** ������� PP_GEOWGS84
// 22/02/2015 ** ���������� � ** �������� ������� 12-version
// 18/02/2015 ** �������      ** �������� �������
// 05/12/2014 ** ���������� � ** MSG_AW_OPENDOCUN
// 08/05/2014 ** ���������� � ** ������ ���������� Gisrscex Gisdelone
// 04/04/2014 ** �������� ** ������� �������������� ������   (inc = h)
// 26/03/2014 ** �������� ** ���������� ovl_OBJECTOVERLAP
// 26/03/2014 ** ���������� � ** �������� TaskParm
// 25/03/2014 ** �������� ** ���������� ��������� �������� TIFF �������
// 25/12/2013 ** ������� �.   ** ������������ �������� TMAPPRINTPARM, TMAPPRINTPARMEX, TFRAMEPRINTPARAM
// 24/12/2013 ** ���������� �.�. ** ����� ���������� � win32
// 13/12/2013 ** ������� �.   ** ��������������� �������� � ���������
// 11/12/2013 ** ������� �.   ** ���� ������ ��� x64
// 05/07/2013 ** ����� �      ** ������� �������������� ������
// 21/06/2013 ** ������� �.   ** �������� ��������� ������ ������� �� ������ GTKUN
// 21/06/2013 ** ����������   ** �������� ���������� ����� ������� ��������� ������
// 23/04/2013 ** ������       ** ������ � ������������ ��������� TBUILDSURFACE
// 01/04/2013 ** ������� �.   ** �������� ������ ���������� ����� ViewAngle � RotationAngle ��������� TMTR3DVIEWEX. ���� �������� 2 ����� � ������ ����� ��� WORD.
// 22/03/2013 ** ������� �.   ** ������� �������� ��������� ��� ������ GIS_CDGXE3
// 11/03/2013 ** ������� �.   ** ������� �������� �������� ��������� ��� ������ GIS_CDGXE3
// 03/03/2013 ** ������� �.   ** ������� �������� ���������� sS57TOMAP
// 06/02/2012 ** ������ �    ** inc = h
// 11/12/2012 ** �������� �   ** �������� �������� TCREATETASK3D
// 30/11/2012 ** ����� �      ** ������� ��������� ��� GISServer
// 13/07/2012 ** ����� �      ** print, gisformse, ����� giswms
// 12/06/2012 ** ����� �      ** ���� ������ Language ��� ���� (����� ��������)
// 09/06/2012 ** ����� �      ** TCURRENTPOINTFORMAT
// 29/03/2012 ** ����� �      ** Language + �� ������
// 21/05/2012 ** ����� �      ** ������
// 17/05/2012 ** ����� �      ** ����� ���������� WCHAR
// 04/05/2012 ** ����� �      ** ������ OLE-���������� 
// 30/04/2012 ** ����������  � ** HWMS ����� (�� ������ ���� ����� ����)
// 10/04/2012 ** ����� �      ** ������� ��������� ��� OLE-���������� 
// 06/02/2012 ** ����������� � ** inc = h
// 17/10/2011 ** ���������� � ** ��������� TMAPOLEPAINT ��� ���������� ����������� OLE �������� � DC
// 29/07/2011 ** ������� �.�. ** �������� �������� ��������� GRADINRAD  
// 06/06/2011 ** ����������   ** �������� ������������ ����������
// 31/05/2011 ** ����������   ** ������� ����� ��������� MapWMS
// 21/06/2011 ** ����������� �** inc = h
// 25/05/2011 ** �������      ** ������� MSG_AW_OPENATLAS
// 11/05/2011 ** �������      ** ���������� ��������� ����������� OLE ��������� TPartInfo
// 12/04/2011 ** �����������  ** ��������� Generic
// 02/03/2011 ** �����������  ** ��������� cGeneric
// 25/02/2011 ** �����������  ** ������������ � crossapi.h
// 22/02/2011 ** �����������  ** TOBJECT_DIRECT
// 17/12/2010 ** �������     ** ��� ������ � ������� ���������� ������� THeightType
// 22/11/2010 ** �����������  ** �������� ������ dll
// 17/11/2010 ** �����������  ** inc = h

unit maptype;

interface
{$I mapvers.inc}

uses

 {$IFDEF GTK12} // 25/05/2016
   vcl.Graphics,
  {$ELSE}
   Graphics,
 {$ENDIF}
  Windows, Types;

{$I gtktype.inc}

const
// ��������� GISACCES.DLL ��������� � ������ gtklink

{$IFDEF CPUX64}                                       // 11/12/2013
  sGISPICEX   = 'GISU64PICEX.DLL';
  sGISMTREX   = 'GISU64MTREX.DLL';
  sGISVECEX   = 'GISU64VECEX.DLL';
  sGISOBJ     = 'GISU64FORMS.DLL';
  sGISFORMS   = 'GISU64FORMS.DLL';
  sGISFORMSE  = 'GISU64FORMSE.DLL';
  sGISRSC     = 'GISU64ACCES.DLL'; //08/05/2014
  sGISSCRN    = 'GISU64SCRN.DLL';
  sGISOBNET   = 'GISU64OBNET.DLL';
  sS57TOMAP   = 'GISU64S57.DLL';
  sGIS3D      = 'GISU643D.DLL';
  sGISSELEC   = 'GISU64SELEC.DLL';
  sGISDelon   = 'GISU64MATH.DLL'; //08/05/2014
  sGisMath    = 'GISU64MATH.DLL';
  sGisGps     = 'GISU64GPS.DLL';
  sGISDLGS    = 'GISU64DLGS.DLL';
  sGISPICTR   = 'GISU64FORMS.DLL';
  sGISTHEM    = 'GISU64them.dll';
  sGISPASP    = 'GISU64pasp.dll';
  sGISVECTR   = 'GISU64vectr.dll';
  sGISSUPP    = 'GISU64SUPP.DLL';
  sGISRSWG    = 'GISU64RSWG.DLL';
  sgisjpg2rsw = 'GISU64jpg2rsw.dll';
  sGISPRINT   = 'GISU64PRINT.DLL';
  sGISKMLTOMAP= 'GISU64KMLTOMAP.DLL';
  sGISSHPTOMAP= 'GISU64SHPTOMAP.DLL';         // 04/04/2014
  sGISADDRESS = 'GISU64ADDRESS.DLL';  // 22/02/2015
  sGisDBMapDll = 'GISU64SQLMAP.DLL';       // 04/04/2016
  sGisRSCTOOLS= 'GISU64RSCTOOLS.dll';      // 12/03/2017

{$ELSE}

{$IFDEF GTKUN}                    // 26/06/2013 // Sh.D.
  sGISPICEX   = 'GISUPICEX.DLL';  // 22/03/2013
  sGISMTREX   = 'GISUMTREX.DLL';
  sGISVECEX   = 'GISUVECEX.DLL';
  sGISOBJ     = 'GISUFORMS.DLL';
  sGISFORMS   = 'GISUFORMS.DLL';
  sGISFORMSE  = 'GISUFORMSE.DLL';
  sGISRSC     = 'GISUACCES.DLL'; //08/05/2014
  sGISSCRN    = 'GISUSCRN.DLL';
  sGISOBNET   = 'GISUOBNET.DLL';
  sS57TOMAP   = 'GISUS57.DLL';
  sGIS3D      = 'GISU3D.DLL';
  sGISSELEC   = 'GISUSELEC.DLL';
  sGISDelon   = 'GISUMATH.DLL'; //08/05/2014
  sGisMath    = 'GISUMATH.DLL';
  sGisGps     = 'GISUGPS.DLL';
  sGISDLGS    = 'GISUDLGS.DLL';
  sGISPICTR   = 'GISUFORMS.DLL';
  sGISTHEM    = 'GISUthem.dll';
  sGISPASP    = 'GISUpasp.dll';
  sGISVECTR   = 'GISUvectr.dll';
  sGISSUPP    = 'GISUSUPP.DLL';
  sGISSEMDB   = 'GISUSEMDB.DLL';
  sGISRSWG    = 'GISURSWG.DLL';
  sgisjpg2rsw = 'GISUjpg2rsw.dll';
  sGISPRINT   = 'GISUPRINT.DLL';
  sGISKMLTOMAP= 'GISUKMLTOMAP.DLL';                      // 11/12/2013
  sGISSHPTOMAP= 'GISUSHPTOMAP.DLL';             // 04/04/2014
  sGISADDRESS = 'GISUADDRESS.DLL';  // 22/02/2015
  sGisDBMapDll = 'GISUSQLMAP.DLL';    // 04/04/2016
  sGisRSCTOOLS= 'GISURSCTOOLS.dll';      // 12/03/2017
{$ELSE}
  sGISPICEX   = 'GISPICEX.DLL';
  sGISMTREX   = 'GISMTREX.DLL';
  sGISVECEX   = 'GISVECEX.DLL';
  sGISOBJ     = 'GISFORMS.DLL';
  sGISFORMS   = 'GISFORMS.DLL';
  sGISFORMSE  = 'GISFORMSE.DLL';
  sGISRSC     = 'GISACCES.DLL'; //08/05/2014
  sGISSCRN    = 'GISSCRN.DLL';
  sGISOBNET   = 'GISOBNET.DLL';
  sS57TOMAP   = 'GISS57.DLL';  // 03/03/2013
  sGIS3D      = 'GIS3D.DLL';
  sGISSELEC   = 'GISSELEC.DLL';
  sGISDelon   = 'GISMATH.DLL';  //08/05/2014
  sGisMath    = 'GISMATH.DLL'; // 01/03/2010
  sGisGps     = 'GISGPS.DLL';
  sGISDLGS    = 'GISDLGS.DLL';
  sGISPICTR   = 'GISFORMS.DLL'; // 04/03/2010
  sGISTHEM    = 'gisthem.dll'; // 04/06/2010
  sGISPASP    = 'gispasp.dll'; // 27/07/2010
  sGISVECTR   = 'gisvectr.dll'; // 27/07/2010
  sGISSUPP    = 'GISSUPP.DLL'; // 22/11/2010
  sGISSEMDB   = 'GISSEMDB.DLL'; // 23/11/2010
  sGISRSWG    = 'GISRSWG.DLL'; // 23/11/2010
  sgisjpg2rsw = 'gisjpg2rsw.dll'; // 23/11/2010
  sGISPRINT   = 'GISPRINT.DLL';  // 13/07/2012
  sGISKMLTOMAP= 'GISKMLTOMAP.DLL';                       // 11/12/2013
  sGISSHPTOMAP= 'GISSHPTOMAP.DLL'; // 04/04/2014
  sGISADDRESS = 'GISADDRESS.DLL';  // 18/02/2015
  sGisDBMapDll= 'GISSQLMAP.DLL';   // 04/04/2016
  sGisRSCTOOLS= 'GISRSCTOOLS.dll';      // 12/03/2017
{$ENDIF}
{$ENDIF}
const
  MAX_PATH_LONG  = 1024;  // 23/06/2016
var
  arrWChar : array[0..MAX_PATH_LONG] of  WideChar;
  Value    : array[0..MAX_PATH_LONG] of char;

// --------------------------------------------------------------
//  �������������� ������ ��������� ����������� �����
// --------------------------------------------------------------
const
//  MAPAPIVERSION =  $100200;

  IDS_OK              =     1 ; // ��� ������                       // 26/03/2014
  IDS_MEM_ALLOC       = 60001 ;  // ������ ��������� ������
  IDS_MEMORY          = 60001 ;  // ������ ��������� ������
  IDS_DISK_ALLOC      = 60002 ;  // ��� ����� �� �����
  IDS_READ            = 60003 ;  // ������ ������ �����
  IDS_WRITE           = 60004 ;  // ������ ��� ������ �� ����
  IDS_STRUCT          = 60005 ;  // ������ ��������� ������
  IDS_OPEN            = 60006 ;  // ������ �������� �����
  IDS_FILE_NOT_FOUND  = 60007 ;  // ���� �� ������
  IDS_PATH_NOT_FOUND  = 60008 ;  // ���� �� ������
  IDS_FILE_EXIST      = 60009 ;  // ���� ��� ����������
  IDS_CLOSE           = 60010 ;  // ������ �������� �����
  IDS_CREATE          = 60011 ;  // ������ �������� �����
  IDS_CREATEOLD       = 600011; // ������ �������� �����
  IDS_LOCK            = 60012 ;  // ���� ����� ������� �����
  IDS_WAIT            = 60013 ; // �������� �� ����� ���� ��������� � ������ ������       // 26/03/2014
  IDS_CREATEDIALOG    = 60014 ; // ������ ��� ������ �������                         // 26/03/2014
  IDS_DOERROR         = 60015 ; // ������ ��� �����������                             // 26/03/2014
  IDS_RSCOPEN         = 60016 ; // ������ �������� ����� RSC                          // 21/06/2016

  IDS_FILE_MANY       = 60020 ;  // ������� ����� �������� ������
  IDS_OLD_VERSION     = 60030 ;  // ���������� ������ ������
  IDS_MODULE          = 60031 ;  // ������������ ������ MAPACCES.DLL
  IDS_SERVERVERSION   = 60032 ; // ���������� ������ �������, ������� �� ��������������  // 21/06/2016
  IDS_PARM            = 60040 ;  // ������ ������� ����������
  IDS_ACCESS          = 60050 ;  // ��� ���� �������
  IDS_COPYRIGHT       = 60060 ;  // ��� ���� �����������

  IDS_PROJECTTYPE     = 60100 ;  // ��������� ��� ��������
  IDS_PROJECTPARM     = 60101 ;  // ��������� ������ � ��������

  IDS_OUTSIDE         = 60102 ;  // ��'��� �� ��������� ������
  IDS_APPENDOBJECT    = 60103 ;  // ������ ��� ���������� ��'����
  IDS_RSCSTYLE        = 60104 ;  // ������ ��������� ��������������
  IDS_NOTFIND         = 60105 ;  // ���/�������� �� �������
  IDS_CANCEL          = 60106 ;  // �������������� ���������� ������
  IDS_LOADLIBRARY     = 60107 ;  // ������ �������� ����������
  IDS_ENTRYLIBRARY    = 60108 ;  // ������ ������ ����� �����
  IDS_POINTLIMIT      = 60109 ;  // ����� ����� ������ �����������

  IDS_DATANOCROSS     = 60110 ;  // ������� �������� �� ������������  10/10/01
  IDS_DATANOADJUST    = 60111 ;  // ������� ��������� ������� ����������
  IDS_DATARESULTLIMIT = 60112 ;  // ����� ����� �������-���������� ������ �����������
  IDS_DATARESULTERROR = 60113 ;  // ������� ��������������� ������� ��������

// -------------------------------------------------------------------
//  �������������� ������ ��������� �������������� ����������� �����
//              � 60200  �� 60299
// --------------------------------------------------------------------
  IDS_RSCAPPENDEXISTOBJECT = 60200 ;  // ������� �������� ��������� ��'���
  IDS_RSCAPPENDOBJECTERROR = 60201 ;  // ������ ��� ���������� ��'����
  IDS_RSCSTRUCTOBJECTERROR = 60202 ;  // ������ ��������� ��'����
  IDS_RSCEXITSEGMENTERROR  = 60203 ;  // ������� �������� ��������� ��� ����
  IDS_RSCEXITSEMANTICERROR = 60204 ;  // ������� �������� ��������� ��� ���������
  IDS_RSCSEMANTICCODEERROR = 60205 ;  // ������� �������� ��������� ��� ����
  IDS_RSCSEMANTICCODECHANGE =  60206 ; // ������ �������� ��� ���������,��� ���-
                                       // �������� ��� ��'����� ���� ������ ��
                                       // ���
  IDS_RSCSEMANTICCODEREPLY = 60207 ;  // ��������� ��� ���������
  IDS_RSCSEMANTICTYPECHANGE = 60208 ;  // ������ �������� ��� - ��������� ������
                                       // �� ��� ��'�����
  IDS_RSCSEMANTICREPLYCHANGE = 60209;  // ������ �������� ������������� - ���������
                                      // ������ �� ��� ��'�����
  IDS_RSCSEMANTICDEFCHANGE  = 60210;  // �� ������������� ���������

// ������ ��� ���������� ������ ;  // 10/07/06 
  IDS_FORMULESYNTAX         = 60220;  // ��������� ���������
  IDS_FORMULESEMERROR       = 60221;  // ������ ������ ��������� 
  IDS_FORMULEBRACKETERROR   = 60222;  // �������������� ������
  IDS_FORMULESIZEERROR      = 60223;  // ������� ������� �������
  IDS_FORMULECALCERROR      = 60224;  // ������ ��� �����������
  IDS_FORMULENAMEREPLY      = 60225;  // ��������� ��� �������           // 26/03/2014

  IDS_SENDERROR             = 60301;  // ������ �������� ������ ����� ������
  IDS_RECVERROR             = 60302;  // ������ ������ ������ ����� ������

{
/******************************************************************
********************  ������� ��������� ***************************
*******************************************************************

    X� X�
  ^
  | (0,0) (������ ���������� "Picture")
  +---------------------------------------------------> X�
  |                                                 ^
  |        (���������� ���� "Client")               |
  |   (0,0)                      X�                 |
  |     ----------------------->                    |
  |     |               |                           |
  |     |        x..... x  ���� ������������        |
  |     |        ....A. |                           |
  |     |        ...+.. |                           |
  |     |        ...... |                         ��������
  |     ---------x------x                    ������ ("Region")
  |     |          ^                                |
  |     |          |                                |
  |     |         ������� �������� ������           |
  |     V                  ("Draw")                 |
  |       Y�                                        |
  |                                                 V
  +----------------------------------------------------> Y� Y�
  |
  |  0,0 (������ ������ "Map" � ���������
  |       � "��������")
  |
  V        ���������� �.A � ������ X�,Y�; � ����������� X�,Y�
    Y�

******************************************************************/

}


const

 MaxNumberOBject = $7FFFFFFF; // ������������ ���������� ����� ������� ����� � �����

 gtkMaxPath = 260;


//++++++++++++++++++++++++++++++++++++++++++++++++++++
//     ��� ��������� �����
//++++++++++++++++++++++++++++++++++++++++++++++++++++
  MT_BADMAPTYPE   = 0;  // ��������� ��� �����

  MT_TOPOGRAPHIC  = 1;  // ��������������� (��42)
  MT_CK_42        = 1;  // ������� ��������� 42 ����
  MT_GEOGRAPHIC   = 2;  // �������-�������������� (��� � ��������� ������� �� ����������)
  MT_GLOBE        = 3;  // ������������������ (������, �������������� �� ���������� �����������)
  MT_CITYPLAN     = 4;  // ��������������� ���� ������ (��42 � ������������� ��������������)
  MT_LARGESCALE   = 5;  // ���������������� ���� ���������
  MT_AERONAUTIC   = 6;  // �����������������
  MT_SEANAUTIC    = 7;  // ������� �������������
                        // (�������������� ������������� ��������� �� ���������� WGS84)
  MT_AVIATION     = 8;  // ����������� (�������������� �� ���������� �����������)
  MT_BLANK        = 9;  // ��������� (�������������� �� ���������� �����������)
  MT_UTMNAD27     = 10; // UTM �� North American Datum 1927
  MT_UTMWGS84     = 11; // UTM �� WGS84
  MT_UTMTYPE      = 12; // UTM   04/11/02
  MT_CK_63        = 13; // ������� ��������� 63 ����  // 09/09/03
  MT_CK_95        = 14; // ������� ��������� 95 ����  // 02/05/07
  MT_TOPOLOCAL    = 15; // ��������������� � ������������ ������� ������  // 24/12/09  (Transverse Mercator)
  MT_MAPSPHERE    = 16; // �������-�������������� ������/������� �� ����  // 15/04/10
  MT_WORLDMAP     = 17; // ����� ���� (�������������� �������)            // 22/12/10
  MT_MCK_CK63     = 18; // ������� ������� ��������� �� ���� ��-63        // 28/02/11
  MT_MERCATOR     = 19; // �������������� ��������� �� ���� "World Mercator" EPSG:3395
  MT_SEANAUTIC_2SP= 20; // ������� ������������� (Mercator_2SP), ����� ������� ���������     // 13/12/2013
  MT_GCK_2011     = 21; // ������� ��������� ���-2011, ������� ������ ��������               // 26/03/2014
  MT_MAPTYPELIMIT = 21; // ������� �������� ���� �����                                       // 13/12/2013  // 26/03/2014

// ��� ����� ��������� ����������
  TR_ABSOLUTE    = 0; // ���������� ������
  TR_AMOUNT      = 1; // ��������� ������

//++++++++++++++++++++++++++++++++++++++++++++++++++++
//    ��������� ��� ��������� ����������� �����

// ����� ��������� � ������� ���������
  CSC_COUNTTOPO     = 11;        // ���������������                 
  CSC_COUNTGEOG     = 5;        // �������-��������������           
  CSC_COUNTAERO     = 2;        // �����������������                
  CSC_COUNTCITYPLAN = 3;        // ��������������� ���� ������
  COUNTLARGE        = 7;        // ���������������� ����
  COUNT63           = 8;        // ��������������� 63 ����
  COUNTSCALENONE    = 16;        // ��� ���� �� ����������

// ���� ����� ������������ ��������
  PR_NOADEQUACY = 0;   // ������ �� ������������� ��������
  PR_ADEQUACY   = 1;   // ������ ������������� ��������

// ���� ����� "�������� ��������� ���������"
  MP_GAUSSCONFORMAL              =  1; // ������������� ������-�������
  MP_CONICALORTHOMORPHIC         =  2; // ���������� ������������� (������ 22)
  MP_CYLINDRICALSPECIAL          =  3; // �������������� �����������
  MP_LAMBERT                     =  4; // ������������ ������������ ���������� ��������
                                       // (�� ��, ��� � 30) // 25/01/11
  MP_STEREOGRAPHIC               =  5; // �����������������
  MP_POSTEL                      =  6; // �������
  MP_AZIMUTHALOBLIQUE            =  7; // ������������ ������������ ����� ��������
                                       // (�� ��, ��� � 30) // 25/01/11
  MP_MERCATORMAP                 =  8; // �������������� ������
                                       // ������������� ���������
  MP_URMAEV                      =  9; // ������ �������������������� c ���������� ����������� ��������
                                       // �������������� �������� �������
                                       // (�� ��, ��� � 25) // 25/01/11
  MP_POLYCONICAL                 = 10; // �������������� ������� ��������������
                                       // �������� (�������������)
                                       // (�� ��, ��� � 29) // 25/01/11
  MP_SIMPLEPOLYCONICAL           = 11; // ������� �������������� ��������������
                                       // (�� ��, ��� � 29) // 25/01/11
  MP_PSEUDOCONICAL               = 12; // ���������������� ������������
  MP_STEREOGRAPHICPOLAR          = 13; // ����������������� ��������
  MP_CHEBISHEV                   = 14; // ������������� ��������
  MP_GNOMONIC                    = 15; // �������������
  MP_CYLINDRICALSPECIALBLANK     = 16; // �������������� �����������
                                       // ��� ��������� �����
  MP_UTM                         = 17; // Transverse Mercator
  MP_KAVRAJSKY                   = 18; // �������������������� ������������
                                       // �������������� �������� �����������
  MP_MOLLWEIDE                   = 19; // �������������������� ������������
                                       // ������������� �������� ���������
  MP_CONICALEQUIDISTANT          = 20; // (������) ������������������ ���������� ��������
  MP_CONICALEQUALAREA            = 21; // (������) ������������ ���������� ��������
  MP_CONICALDIRECTORTHOMORPHIC   = 22; // (������) ������������� ���������� �������� ��������
  MP_AZIMUTHALORTHOMORPHICPOLAR  = 23; // ������������ ����������������� (�������������)
                                       // �������� ��������            // 23/11/2010
  MP_LAMBERTAZIMUTHALEQUALAREA   = 24; // (����������) ������������
                                       // ������������ �������� �������� (�� ��, ��� � 30) // 25/01/11
  MP_URMAEVSINUSOIDAL            = 25; // ������ �������������������� c ���������� ����������� ��������
                                       // �������������� �������� �������
                                       // ��� ���� �������(������ � ����������)
  MP_AITOFF                      = 26; // ����������� ������������
                                       // �������� ������-������
  MP_CYLINDRICALEQUALSPACED      = 27; // ������������������ �������������� ��������
  MP_LAMBERTCYLINDRICALEQUALAREA = 28; // ������������ ��������������
                                       // �������� ��������
  MP_MODIFIEDPOLYCONICAL         = 29; // �������������� ������� ��������������
                                       // �������� (�������������)
  MP_LAMBERTOBLIQUEAZIMUTHAL     = 30; // ������������ (�����, ����������, ����������) ������������
                                       // �������� �������� (������� �� ��������� ������)
  MP_TRANSVERSECYLINDRICAL       = 31; // ������������� ���������-��������������
                                       // ��������
  MP_GAUSSCONFORMAL_SYSTEM_63    = 32; // ������� ��������� 63 ����
  MP_LATITUDELONGITUDE           = 33; // ������/������� �������������� �� ����  // 23/11/2010
  MP_MILLERCYLINDRICAL           = 34; // �������������� ������� �� ���� ESRI:54003               // 22/12/10
  MP_WORLDMERCATOR               = 35; // �������������� ������ ������������� ��������� EPSG:3395 // 31/03/11
  MP_MERCATOR_2SP                = 36; // �������������� ������ ������������� ���������           // 13/12/2013
  MP_MODIFIEDAZIMUTALEQUIDISTANT = 37; // ���������������� ������������ ������������������ (EPSG:9832)    // 21/06/2016
  MP_AZIMUTALEQUIDISTANTGUAM     = 38; // ������������ ������������������ �������� ���� - Guam Projection (EPSG:9831)    // 21/06/2016
   
  MP_LASTPROJECTIONNUMBER        = 38; // ������� ����� ��������                                  // 13/12/2013

  // �������� �������� �������� � XML
  //   "Pulkovo42"                        // 1    ��-42
  //   "Pulkovo63"                        // 32   ��-63
  //   "Pulkovo95"                        // 1    ��-95
  //   "UTM"                              // 17   UTM NAD83
  //   "Transverse Mercator"              // 17
  //   "Postel Equidistant Azimuthal"     // 6
  //   "Mercator Cylindrical"             // 8
  //   "Kavrajsky Cylindrical Equal Area" // 18
  //   "Mollweide Cylindrical Equal Area" // 19
  //   "Equidistant Conic"                // 20
  //   "Equal Area Conic"                 // 21
  //   "Lambert Conic Conformal"          // 22
  //   "Polar Stereographic"              // 23
  //   "Urmaev Pseudo Cylindrical"        // 25
  //   "Aitoff Azimuthal Equal Area"      // 26
  //   "Equidistant Cylindrical"          // 27
  //   "Lambert Cylindrical Equal Area"   // 28
  //   "Modified Polyconical"             // 29
  //   "Lambert Azimuthal Equal Area"     // 30
  //   "LatitudeLongitude"                // 33
  //   "Miller Cylindrical"               // 34 - EPSG:54003
  //   "World Mercator"                   // 35 - EPSG:3395


 // ���� ����� "��� ����������"
  EK_UNDEFINED   = -1;       // �� �����������
  EK_KRASOVSKY42 =  1;       // ����������� 1942�. (6378245.0, 298.3)
  EK_WGS_72      =  2;       // ������������� 1972�.
  EK_HEFORD      =  3;       // �������� 1910�. (6378388.0, 297.0)
  EK_CLARKE_80   =  4;       // ������ 1880�.
  EK_CLARKE_66   =  5;       // ������ 1866�.
  EK_EVEREST_56  =  6;       // �������� 1856�. (6377301.243, 300.8017)
  EK_BESSEL      =  7;       // ������� 1841�.
  EK_AIRY        =  8;       // ��� 1830�.
  EK_WGS_84      =  9;       // ������������� 1984�.
  EK_SGS_85        = 10;     // ��-90.02    (6378136.0, 298.2578393)
  EK_GRS_80        = 11;     // GRS80   (6378137.0, 298.257222101)
  EK_IERS_96       = 12;     // IERS 1996�.   (6378136.49,  298.25645)
  EK_INTER_1924    = 13;     // ������������� 1924�. (6378388.0, 297.00)
  EK_SOUTHAM_69    = 14;     // ����-������������ 1969�. (6378160.0, 298.25)
  EK_INDONESIAN_74 = 15;     // ������������� 1974�. (6378160.0,  298.247)
  EK_HELMERT       = 16;     // ��������� 1906�. (6378200.0,  298.3)
  EK_FISCHER_60M   = 17;     // ������ 1960�. Modified (6378155.0,  298.3)
  EK_FISCHER_68    = 18;     // ������ 1968�. (6378150.0,  298.3)
  EK_HOUGH         = 19;     // ��� 1960�. (6378270.0,  297.0)
  EK_EVEREST_30    = 20;     // �������� 1830�. (6377276.345,  300.8017)
  EK_AUSTRALIAN    = 21;     // ������������� ������������ (6378160.0,  298.25)
  EK_CGCS2000       = 22;    // CGCS2000
  EK_AIRYM          = 23;    // Airy Modified
  EK_BESSELM        = 24;    // Bessel Modified
  EK_BESSELNAMIBIA  = 25;    // Bessel Namibia
  EK_BESSELGLM      = 26;    // Bessel Namibia (GLM)
  EK_CLARKE1880ARC  = 27;    // Clarke 1880 (Arc)
  EK_CLARKE1880SGA22 = 28;   // Clarke 1880 (SGA 1922)
  EK_EVEREST1830D67 = 29;    // Everest 1830 (1967 Definition)
  EK_EVEREST1830M   = 30;    // Everest 1830 Modified
  EK_EVEREST1830RSO69 = 31;  // Everest 1830 (RSO 1969)
  EK_EVEREST1830D75 = 32;    // Everest 1830 (1975 Definition)
  EK_NWL9D          = 33;    // NWL 9D
  EK_PLESSIS1817    = 34;    // Plessis 1817
  EK_STRUVE1860     = 35;    // Struve 1860
  EK_WAROFFICE      = 36;    // War Office
  EK_GEM10C         = 37;    // GEM 10C
  EK_OSU86F         = 38;    // OSU86F
  EK_OSU91A         = 39;    // OSU91A
  EK_GRS_67         = 40;    // GRS 1967
  EK_ATS_77         = 41;    // Average Terrestrial System 1977
  EK_IAG_75         = 42;    // IAG 1975
  EK_GRS_67M        = 43;    // GRS 1967 Modified
  EK_DANISH1876     = 44;    // Danish 1876
  EK_SPHERE_WGS_84  = 45;    // ��� �� WGS-84                                   // 13/12/2013
  EK_GCK_2011_EE    = 46;    // ���-2011 ���������� ��������� (6378136.5, 298.2564151)   // 26/03/2014
  EK_SGS_85_2011    = 47;    // ��-90.11      (6378136.0, 298.2578393)                   // 26/03/2014
  EK_ELLIPSOIDCOUNT = 47;                                                       // 26/03/2014


  EK_USERELLIPSOID  = 1000;  // ������������ (����������������) ���������

  EK_KRASOVSKY42_95 = 1001;  // ����������� 1942�. ��-95  (don,t use)
  EK_EC90TEMP       = 1002;  // ��-90                     (don,t use)

 // ���� ����� "������� �����"
  HS_BALTIC      =  1;       // ���������� ������� �����
  HS_AUSTRALIA   =  2;       // ������������� ������� ����� 1971�.              // 13/12/2013
  HS_ADRIATIC    =  3;       // ������� ������� �������������� ���� � ������� (�������, ���������)       // 13/12/2013
  HS_NORTHSEA    =  4;       // ������� ������� ��������� ���� � ������� "����-�������"                  // 13/12/2013
  HS_OSTEND      =  5;       // ������� ������� ������ ��� ��������� ���� � ������� -   "���� ���� �� �����" ( �������)  // 13/12/2013
  HS_LAMANSH     =  6;       // ������� ������� ���� � ������� ������
  HS_BELFAST     =  7;       // ������� ������� ����������� ���� � �������� (��������   ��������)        // 13/12/2013
  HS_MALIKXED    =  8;       // ������� ������� �������������� ������ � �����-X�� (��������)             // 13/12/2013
  HS_DUBLIN      =  9;       // ������� ������ ���� � ���������� ������ (��������)                       // 13/12/2013
  HS_AEGEAN      = 10;       // ������� ������� ��������� ���� � ����� ����� (������)                    // 13/12/2013
  HS_DENMARK     = 11;       // ������� ������� ���� � �������� ��������� (�����)                        // 13/12/2013
  HS_ICELAND     = 12;       // ������� ������� ������ ���������� � ���������� (��������)                // 13/12/2013
  HS_ALICANTE    = 13;       // ������� ������� ������������ ���� � �������� (�������                    // 13/12/2013
  HS_CANARY      = 14;       // ������� ������� �������������� ������ (��� ��������� ��������)           // 13/12/2013
  HS_GENOA       = 15;       // ������� ������� ������������ ���� � ����� (������)                       // 13/12/2013
  HS_NORMALNULL  = 16;       // ������� ������� C�������� ���
  HS_OSLO        = 17;       // ������� ������� ���� � ���� - "���������� ���������� ����" (�����  ��������) // 13/12/2013
  HS_NARVIK      = 18;       // ������� ������� ���� � ����� ������ (�������� ��������)                  // 13/12/2013
  HS_CASCAIS     = 19;       // ������� ������� �������������� ������ � ������� (����������)             // 13/12/2013
  HS_HELSINKI    = 20;       // ������� ������� ����������� ���� � X�������� (���������)                 // 13/12/2013
  HS_SWEDEN      = 21;       // ������� ������� ���� � �������� ������� (������)                         // 13/12/2013
  HS_MARSEL      = 22;       // ������� ������� ������������ ���� � �������
  HS_TYRKEY      = 23;       // ������� ������� �����, ��������� ������ (������)                         // 13/12/2013
  HS_USAKANADA   = 24;       // ������� ������� ����� � �������,��������� ��� � ������
  HS_BALTIC77    = 25;       // ���������� ������� 1977 �.
  HS_OKHOTSK     = 26;       // ������� ������� ��������� ���� � ������ ������
  HS_PEACEOCEAN  = 27;       // ������� ������� �������� ������

 // ���� ����� "������� ���������"
  CS_ORTHOGONAL          = 1; // ������� ��������� 42 ����
  CS_UNIVERSALMERCATOR   = 2; // ������� �������� ���������
  CS_NATIONALGRID        = 3; // ������������ ������������� ����� ��������������
  CS_AREAORTHOGONAL      = 4; // ������������� ������� ������� ���������
  CS_SYSTEM_63           = 5; // ������� ��������� 63 ����
  CS_CONDITION           = 6; // ������������� �������� ��� �������� ����
  CS_GEOCOORDINATE       = 7; // ������������� ����������
  CS_GEOCOORDINATEGRADUS = 8; // ������������� ���������� � ��������
  CS_SYSTEM_95           = 9; // ������� ��������� 95 ����
  CS_GCK_2011_CRS        = 10; // ������� ��������� ���-2011

   // ���� ����� "������� ���������"
  MU_KILOMETRE     =  5;    // ���������
  MU_METRE05       =  4;    // 0.5 �
  MU_METRE         =  0;    // �����
  MU_DECIMETRE     =  1;    // ���������
  MU_CENTIMETRE    =  2;    // ����������
  MU_MILLIMETRE    =  3;    // ����������
  MU_SECOND01      = 66;    // 0.1 ���
  MU_RADIAN        = 64;    // �������
  MU_RADIAN8       = 67;    // 10e-8 ���
  MU_DEGREE        = 65;    // �������
  MU_FOOT          = 16;    // ����

 // ���� ����� "��� �����" FRAMEKIND
  FK_TRAPEZE      = 1;      // �������������� ��� ����� ������
  FK_TRAPEZECURVE = 2;      // �������������� � ������� ������
  FK_RECTANGULAR  = 3;      // �������������
  FK_CIRCLE       = 4;      // ��������

  MIK_MAP     = 1;           // ����������������
  MIK_PHOTO   = 2;           // ��������
  MIK_IMAGE   = 3;           // ����������
  MIK_GRAM    = 4;           // �������������������

  MIT_MAPRUN        = 1;        // �������� ������
  MIT_FINAL         = 2;        // ������������ ��������
  MIT_MANUSCRIPT    = 3;        // ��������������� ��������
  MIT_UPDATE        = 4;        // �������� ���������
  MIT_SPECIAL       = 5;        // ����������� ��������
  MIT_CONSTANT      = 6;        // ���������� ����������� ��������
  MIT_FGM           = 7;        // ������������������� ��������
  MIT_FGMMAPRUN     = 8;        // ��� � �������� ������
  MIT_FGMMANUSCRIPT = 9;        // ��� � ��������������� ��������
  MIT_FGMFINAL      = 10;       // ��� � ������������ ��������
  MIT_FGMCONSTANT   = 11;       // ��� � ���������� ����������� ��������
  MIT_FGMSPECIAL    = 12;       // ��� � ����������� ��������

  PIK_SPACE     = 64;         // �����������
  PIK_AERO      = 65;         // ����������
  PIK_PHOTOGRAM = 66;         // ��������������� ������

  // ���� ��������� ��� ������������� �������� ������-�������
  SG_FGKSCALEMLN = 1;
  SG_FGKSCALE500 = 2;
  SG_FGKSCALE200 = 3;
  SG_FGKSCALE100 = 4;
  SG_FGKSCALE50  = 5;
  SG_FGKSCALE25  = 6;
  SG_FGKSCALE10  = 7;
  SG_FGKSCALE5   = 8;
  SG_FGKSCALE2   = 9;

  // ���� ��������� ��� �������-�������������� ����
  SO_FGEOSCALE10MLN = 1;
  SO_FGEOSCALE5MLN  = 2;
  SO_FGEOSCALE2MLN  = 3;
  SO_FGEOSCALEMLN   = 4;
  SO_FGEOSCALE500   = 5;

  // ���� ��������� ����������������� ����
  SA_FARSCALE4MLN = 1;
  SA_FARSCALE2MLN = 2;

  // ���� ��������� ��� ���� ��������������� ���� ������
  SP_FCPSCALE25 = 1;
  SP_FCPSCALE10 = 2;

  // ���������� ����� ��� ������ ������������
  TVT_COUNTMAPTYPE     = 21;             // ��� �����                      
  TVT_COUNTUNIT        = 12;             // ������� ���������
  TVT_COUNTUNITHEIGHT  = 8;              // ������� ��������� �� ������
  TVT_COUNTELL         = EK_ELLIPSOIDCOUNT+1; // ��� ����������
  TVT_COUNTHEIGHT      =  9;             // ������� �����                  
  TVT_COUNTCOORD       =  8;             // ������� ���������
  TVT_COUNTPROJ        = 34;             // ��������
  TVT_COUNTMAPINITKIND =  5;             // ��� ��������� ���������        
  TVT_COUNTMAPINITTYPE = 7;             // ��� ���
  TVT_COUNTPHOTO       =  10;             // ��� �������
  TVT_COUNTFRAME       =  5;             // ��� �����
  TVT_COUNTGEOGRAPHICCODE = 9;        // ���� ��� �������-�������������� ����

// ���� �������� �������
  KM_IDSHORT2  = $7FFF7FFF; // ������������ �������������
  KM_IDLONG2   = $7FFE7FFE; // ��������������� �������������
  KM_IDFLOAT2  = $7FFD7FFD; // � ��������� �������
  KM_IDDOUBLE2 = $7FFC7FFC; // � ��������� ������� ������� ���������
  KM_IDSHORT3  = $7FFB7FFB; // ������������ ������������� ����������
  KM_IDLONG3   = $7FFA7FFA; // ��������������� ������������� ����������
  KM_IDFLOAT3  = $7FF97FF9; // � ��������� ������� ����������
  KM_IDDOUBLE3 = $7FF87FF8; // � ��������� ������� ������� ��������� ����������
  KM_IDBAD     = $7FF87FF7; // ����������� ���

// �������������� ������ (Intel)
    FILE_SXF = $00465853;   // �������� SXF ����               
    FILE_TXT = $4658532E;   // ��������� SXF ���� (.SXF)       
    FILE_TXFMAP = $4658532E; // ��������� SXF ���� (TXF � ������ .SXF - MAP)
    FILE_DIR = $524944;     // ��������� DIR ����              
    FILE_PCX = $10;         // ���� PCX (������������ �� �����)
    FILE_BMP = $4D42;       // ���� BMP
    FILE_TIFF= $4949;       // ���� TIFF
    FILE_BIGTIFF = $49492B; // ���� bigTIFF                                     // 13/12/2013
    FILE_JPEG= $D8FF;       // ���� JPEG
    FILE_MRSID= $6469736D;  // ���� MrSid (SID,JPEG2000,NITF)
    FILE_MAP = $00505350;   // ������� ������ ����� MAP        
    FILE_MAPSIT = $00544953;// ������� ������ ����� SIT
    FILE_SITX = $58544953;   // ���� SITX                       // 12/03/13
// 13/12/2013    FILE_RST = $00545352;   // ���� RST
    FILE_RSW = $00575352;   // ���� RSW
    FILE_RSWTIFF = $54575352;//(RSWT)RSW+TIFF  // 24/05/11
// 13/12/2013    FILE_MTR = $0052544D;   // ���� MTR
    FILE_MTW = $0057544D;   // ���� MTW
    FILE_MTL = $004C544D;   // ���� MTL
    FILE_MTD  = $0044544D;  // ���� MTD  28/11/07
    FILE_SIT = $5449532E;   // ��������� SXF(.SIT) ����
    FILE_TXFSIT = $5449532E; // ��������� SXF ���� (TXF � ������ .SIT - SIT)
    FILE_DXF = $00465844;   // ���� DXF
    FILE_MIF = $0046494D;   // ���� MAPINFO
    FILE_S57 = $00003000;   // ���� S57(�������)
    FILE_DGN = $004E4744;   // ���� Macrostation(DGN)
    FILE_MPT = $0054504D;   // ���� MPT(������)
    FILE_RSC = $00435352;   // ���� RSC
    FILE_MTQ = $0051544D;   // ���� MTQ
    FILE_PLN = $004E4C50;   // ���� PLN (Talka)
    FILE_SHP = $00504853;   // ���� SHP (ArcView)
    FILE_PLS  = $00534C50;   // ���� PLS (������ �������)          // 05/07/04
    FILE_TEXT = $00545854;   // ���� TXT(������������� �������)    // 23/11/04
    FILE_GPS  = $47504724;   // ���� GPS/NMEA ($GPG)               // 07/12/04
    FILE_GRD  = $00445247;   // ���� GRD (������� ������� FOTOMOD) // 10/12/04
    FILE_DBF  = $00464244;   // ���� DBF (���� ������)             // 16/12/04
    FILE_TIN  = $004E4954;   // ���� TIN                           // 31/03/05
    FILE_XLS  = $0000CFD0;   // ���� EXCEL                         // 22/07/09
    FILE_WMS  = $00534D57;   // URL ��� ����������� � WMS-�������  // 16/07/11
    FILE_IMG  = $00494D47;   // ���� IMG                           // 15/09/12
    FILE_PNG  = $00504E47;   // ���� PNG                           // 15/09/12
    FILE_GIF  = $00474946;   // ���� GIF                           // 15/09/12
    FILE_WFS  = $00221497;   // URL ��� ����������� � WFS-�������  // 29/03/13
    FILE_WCS  = $00331924;   // URL ��� ����������� � WCS-�������  // 17/04/13
    FILE_KML  = $25FF5621;   // ���� KML                           // 06/05/13
    FILE_GML  = $25FF5622;   // ���� GML                           // 06/05/13
    FILE_MP   = $25FF5623;   // ���� MP                            // 06/05/13
    FILE_OZF2 = $7778;       // ���� OZF2 (������� ��� OziExplorer)             // 13/12/2013
    FILE_OZFX3= $7780;       // ���� OZFX3(������� ��� OziExplorer)             // 13/12/2013
    FILE_EMF  = $00464D45;   // ���� EMF                                        // 23/06/2016

                              // �������������� ������ (Sparc, Mips)

    FILE_MAP_TURN = $50535000; // ������� ������ �����
    FILE_MTW_TURN = $4D545700; // ���� MTW
    FILE_SXF_TURN = $53584600; // �������� SXF ����    //03/11/03
    FILE_DIR_TURN = $44495200; // ��������� DIR ����   //05/12/03
    FILE_RSW_TURN = $52535700; // ���� RSW  // 20/01/04
    FILE_RSC_TURN = $52534300; // ���� RSC
    FILE_SIT_TURN = $2E534954; // ��������� SXF(.SIT) ���� // 30/06/08
    FILE_MTR_TURN = $4D545200; // ���� MTR
    FILE_MTQ_TURN = $4D545100; // ���� MTQ
    FILE_RST_TURN = $52535400; // ���� RST

                           // �������������� ������ ����� �����
    LABEL_HDR      = $00524448;  // HDR0
    LABEL_DAT      = $00544144;  // DAT0
    LABEL_SEM      = $004D4553;  // SEM0
    LABEL_DRW      = $00575244;  // DRW0
    LABEL_VEC      = $00434556;  // VEC0
    LABEL_HDR_TURN = $48445200;  // 0RDH
    LABEL_DAT_TURN = $44415400;  // 0TAD
    LABEL_SEM_TURN = $53454D00;  // 0MES
    LABEL_DRW_TURN = $44525700;  // 0WRD
    LABEL_VEC_TURN = $56454300;  // 0CEV

// �������� ����������� �������� ���������� ��� ������� ��������s
  OL_LINE      = 0;         // ��������
  OL_SQUARE    = 1;         // ���������
  OL_MARK      = 2;         // ��������
  OL_TEXT      = 3;         // �������
  OL_VECTOR    = 4;         // ���������
  OL_PATTERN   = 5;         // ������
  MAXLOCALS    = 5;
type
  TLOCAL  = (L_LINE, L_SQUARE, L_MARK, L_TEXT, L_VECTOR, L_PATTERN);
  TLOCALS = set of TLOCAL;
const
  LocalsAll = [L_LINE, L_SQUARE, L_MARK , L_TEXT, L_VECTOR, L_PATTERN];

type
  // �������� � �������
  TActionType = (AT_OPENFILE, AT_CLOSEFILE);

  // ���� ������
  TFileType = (MT_MAP, MT_SIT, MT_RSW, MT_RST, MT_MTW, MT_MTL, MT_MTQ, MT_MTR);


//+++++++++++++++++++++++++++++++++++++++++++++++++++

const
  cERRORHEIGHT:double = -111111.0;  // ��� ������ �����

{ 01/06/2008 ����������� � mapviewa
  // ���� ������ ���������� TMapView
  acNoAction     = $0000;     // ������ ��������
  acClose        = $0001;     // �������� �����
  acOpen         = $0002;     // �������� �����
  acBeforePaint  = $0003;     // �� ����������� �����
  acAfterPaint   = $0004;     // ����� ����������� �����
  acAppendSite   = $0005;     // ��������� ����� ����������
  acDeleteSite   = $0006;     // ��������� ����������
}
  // ���� �������� ���������

  cTUNDEFINED = -1;  // �������� �� �����������
  cTSTRING    = 0;   // ���������� ������
  cTNUMBER    = 1;   // �������� ��������
  cTANYFILE   = 9;   // ��� ����� ������������������� ����
  cTBMPFILE   = 10;  // ��� ����� BMP
  cTOLEFILE   = 11;  // ��� �����,��������������� OLE-��������
  cTREFER     = 12;  // ������ �� ������������ ������ �����
                     // (���������� ����� �������)
  cTMAPFILE   = 13;  // ��� �����-�������� ������
  cTTXTFILE   = 14;  // ��� ���������� ����� (OEM)
  cTPCXFILE   = 15;  // ��� ����� PCX
  cTCODE      = 16;  // �������� � ���� ��������� ����
                     // �� �������������� ��������
  cDATE       = 17;  // �������� ���� � �������� ���� (��������)
  cTANGLE     = 18;  // ������� �������� � ��������
  cTTIME      = 19;  // �������� ������� � �������� ���� (������)
  cTFONT      = 20;    // ��� ������ ("Arial", "Courier"...)
  cTCOLOR     = 21;    // �������� �������� ����� � RGB
  cTFDIGITAL  = 22;    // ��������� ���� ������� �������� (� ������������ ����������)   // 18/02/13
  cTFSTRING   = 23;    // ��������� ���� ������� ���������� (� ������������ ����������) // 18/02/13
  cTLAST      = 23;     // ������� ������� ������ �����

  cTDOCREFMIN = 9;     // ������ ������� ���� ��������� - ������ �� �������� (����������� ����, �����)   // 23/06/2016
  cTDOCREFMAX = 15;    // ������ ������� ���� ��������� - ������ �� �������� (����������� ����, �����)   // 23/06/2016

  // ����������� ������� ��������� ��� ��������� � �������� MapApi

  cPP_MAP     = 1;   // ���������� ����� � ������� ����� � ���������
  cPP_PICTURE = 2;   // ���������� ����� � ������� ����������� � ��������
  cPP_PLANE   = 3;   // ���������� ����� � ������� ������������� �������
                     // �� ��������� � ������
  cPP_GEO     = 4;   // ���������� ����� � ������������� �����������
                     // � �������� � ������� ���������
  cPP_GEOWGS84 = 8;  // ���������� ����� � ������������� �����������
                     // � �������� � ������� WGS-84

  // CURRENTPOINTFORMAT  // ������ ����������� �����  // 09/06/10

   cpf_PLANEPOINT      = 1;  // � ������ �� ��������� � ������� ���������� �������� ("DocProjection")
                             // ��������� ����������� 1942�. ��-42 ��� ��-95,
                             // ���� ������� ����� � ��-95
   cpf_MAPPOINT        = 2;  // � �������� �������� ����� (���������)
   cpf_PICTUREPOINT    = 3;  // � �������� �������� ������� �����������

   cpf_GEORAD          = 4;  // � ��������
   cpf_GEOGRAD         = 5;  // � ��������
   cpf_GEOGRADMIN      = 6;  // � ��������, �������, ��������

                             // ���������� ��������� WGS84
   cpf_GEORADWGS84     = 7;  // � ��������
   cpf_GEOGRADWGS84    = 8;  // � ��������
   cpf_GEOGRADMINWGS84 = 9;  // � ��������, �������, ��������

   cpf_PLANE42POINT    = 10; // � ������ �� ��������� �� ��������� ���� � ��-42

                             // ���������� ��������� ��-90.02 (SGS-85)
   cpf_GEORADPZ90      = 11; // � ��������
   cpf_GEOGRADPZ90     = 12; // � ��������
   cpf_GEOGRADMINPZ90  = 13; // � ��������, �������, ��������

   cpf_PLANEWORKSYS      = 14; // � ������ �� ��������� � ������� ������� � ������������ � ����������� ("WorkSystem")
   cpf_GEORADWORKSYS     = 15; // � �������� � ������� �������
   cpf_GEOGRADWORKSYS    = 16; // � �������� � ������� �������
   cpf_GEOGRADMINWORKSYS = 17; // � �������� �������, �������� � ������� �������
   cpf_PLANE95POINT      = 18; // � ������ �� ��������� �� ��������� ���� � ��-95          // 15/11/10
   cpf_LASTPOINTFORMAT   = 18; // ���������� ��������

  // ��� ����������� �����  // 08/06/2004
// �������� �����
  cVT_SCREEN        = 1;  // �������� (����� DIB)
  cVT_SCREENCONTOUR = 2;  // �������� ���������
// ���������� ��������� �����
  cVT_PRINT           = 3; // ���������� ��������� (����� WIN API)
  cVT_PRINTGLASS      = 4; // ���������� ��� ������� ���������
  cVT_PRINTCONTOUR    = 5; // ���������� ���������, ��� �������� ������
// ���������� �������������� �����
  cVT_PRINTRST        = 6; // ���������� �������������� (����� WIN API)
  cVT_PRINTGLASSRST   = 7; // ���������� ��� ������� ���������
  cVT_PRINTCONTOURRST = 8; // ���������� ���������, ��� �������� ������
// ���������� �������������� (�����������) �����        // 13/02/03
  cVT_PRINTRSTSQUARE  = 9; // ���������� (������� ���������, ������� � �����,
                           //             ������, �������)
  cVT_PRINTRSTLINE    =10; // ���������� (�����, ��������, ���������,
                           //             ������� � �����, ����������������)
  cVT_PRINTRSTTEXT    =11; // ���������� (�������, �������)
   // ���������� ��������� (�����������) �����. ������������ ��� ��������
   // ������� (POSTSCRIPT, WMF, EMF)                       // 16/12/04
  cVT_PRINTEX         =15; // ���������� ��������� (����� WIN API)


  // ���� ������ ��������� � ������� ����

  cML_ENGLISH   = 1;  // ����������
  cML_RUSSIAN   = 2;  // �������
  cML_FRENCH    = 3;  // �����������
  cML_SPANISH   = 4;  // ���������
  cML_UKRAINIAN = 5;  // ����������
  cML_KAZAKH    = 6;  // ���������
  cML_VIETNAM   = 7;  // �����������
  cML_LAST      = 7;  // ��������� ���  // 23/10/15

 // ������, ������������ ������� ������ ��������
 // ������ � �������,���������,��������� �� �������� �����,����������

  cWO_FIRST  = 0;
  cWO_LAST   = 2;
  cWO_NEXT   = 4;
  cWO_BACK   = 8;
  cWO_CANCEL = 16;
  cWO_INMAP  = 32;
  cWO_VISUAL = 64;        // ����� ������ ����� ������� ��������                // 13/12/2013
  cWO_VISUALIGNORE = 128; // ����� ����� ���� �������� ��� ����� ���������      // 13/12/2013
  cWO_SKIPTEXT = 256;     // ����� ��� ������ �������� ���������


  MSG_BUILDMTR     = $0581;
  MSG_WM_OBJECT    = $0585;       // ����� �������
  MSG_WM_LIST      = $0586;       // ����� �����
  MSG_WM_INFOLIST  = $0584;       // ���������� � �����
  MSG_WM_ERRORCOORD= $0583;       // ���������� � ������� ��������
  MSG_WM_ERROR     = $0587;       // ���������� �� �������
  MSG_WM_MAP       = $0588;       // ����� ������� �����
  MSG_WM_PROGRESSBAR  = $0590;    // ��������� � ��������� �������� (07/09/2004)
  MSG_WM_MAPEVENT     = $0591;    // ��������� � �������� �����          // 04/04/2014
  MSG_WM_PROGRESSICON = $0592;    // ��������� � ��c������ ��������      // 04/04/2014
                                  // ��� ����������� � ������ ���������
                                  // ��� ��������� ������� ����
  MSG_WM_PROGRESSBARW = $0593;    // ��������� � ��c������ �������� � UNICODE    // 04/04/2014


  CIA_PAINT        = $0001;       // ������ � ������� �� ����������� �������
  SIA_PAINT        = $0001;       // ����� ������� �� �����������
  SIA_PAINT2       = $0002;

  // ���� ���������� ������� ������/����������� �� ���������
  SC_OR  = 16;  // ����������� ���� �� ����
  SC_AND = 32;  // ����������� ���

  SCREENLIMIT  = 32 ;    // ���������� ����� ����������� ������� ������
type

{$IFDEF CPUX64}        // 11/12/2013

  HMAP      = int64;
  HOBJ      = int64;
  HRSC      = int64;
  HSELECT   = int64;
  HMTR3D    = int64;
  HSITE     = int64;
  HCROSS    = int64;
  HSCREEN   = int64;    // ������������� ������� ���������� ������� ������
  HGRAPH    = int64;    // ������������� ����� ���� (��������� �� TGraph)
  HPrinter  = int64;
  HALS      = int64;    // ������������� ������ ������� �����
  HCHOICEOBJECT = int64;// ������������� ��������� ������ ������ �������
                        // (��������� �� TChoiceObject)
  HTFCR     = int64;    // ������������� �������� �������� TIFF
  HTRIANG   = int64;
  HPARAM    = int64;    // �������� ������� ��������� �������
  HCUT      = int64;    // ������������� �������
  HPOINT    = int64;    // ��������� �� ��������� CROSSPOINT
  HBUILDPROFIL = int64;
  
  HSTATOBJ  = int64;  // ������������� ��������� ������� ������� �� �������         // 07/06/2016
  HSEMANT   = int64;  // ������������� ��������� ������� �������������� ���������
  HMCLCALC  = int64;  // ������������� ��������� ������� �����������
  
  HSEMDB    = int64;
  HRSCDB    = int64;
  HMAPDOC   = int64;
  HEQUATIONS= int64;    // ������������� ������� ���������� ��������� (��������� �� TEquations)

{$ELSE}

  HMAP      = integer;
  HOBJ      = integer;
  HRSC      = integer;
  HSELECT   = integer;
  HMTR3D    = integer;
  HSITE     = integer;
  HCROSS    = integer;
  HSCREEN   = integer; // ������������� ������� ���������� ������� ������
  HGRAPH    = integer; // ������������� ����� ���� (��������� �� TGraph)
  HPRINTER  = integer;
  HALS      = Cardinal;      // ������������� ������ ������� �����
  HCHOICEOBJECT = Cardinal;   // ������������� ��������� ������ ������ �������
                              // (��������� �� TChoiceObject)
  HTFCR    = Cardinal;          // ������������� �������� �������� TIFF
  HTRIANG  = Cardinal;
  HPARAM   = integer ;  // �������� ������� ��������� ������� // 07/10/2009
  HCUT      = integer;   // ������������� ������� // 02/10/2009
  HPOINT    = integer; // ��������� �� ��������� CROSSPOINT // 06/10/2009
  HBUILDPROFIL = integer; // 18/04/2010
  
  HSTATOBJ  = integer;  // ������������� ��������� ������� ������� �� �������         // 07/06/2016
  HSEMANT   = integer;  // ������������� ��������� ������� �������������� ���������
  HMCLCALC  = integer;  // ������������� ��������� ������� �����������  
  
  
  HSEMDB  = integer;      // 23/11/2010
  HRSCDB  = integer;      // 23/11/2010
  HMAPDOC = int64;        // 23/11/2010
  HEQUATIONS = integer;   // 17/11/2010 // ������������� ������� ���������� ��������� (��������� �� TEquations)

{$ENDIF}

  HOVL = Pointer;         // // ������������� ������� ���������� ��������  // 25/02/2011

// 17/10/2011
  // ��������� ��� ���������� ����������� OLE �������� � DC 
  TMAPOLEPAINT = packed record
    dc:HDC;
    rect:TRect;
  end;

  TMAPFRAME = packed record 
    X1:longint;
    Y1:longint;
    X2:longint;
    Y2:longint;
  end;

  TMAPDFRAME = packed record
    X1:double;
    Y1:double;
    X2:double;
    Y2:double;
  end;
  PMAPDFRAME = ^TMAPDFRAME;  // 29/09/2009


  TSEMANTICTYPE = packed record     // �������� �������������
    // ���������� ���������         // �������������� �� ����
    fCode:longint;                  // ������� ���
    fType:longint;                  // ��� ��������
    fReply:longint;                 // �������: ����������� ��� ���
    fMist:longint;                  // �������: ����������� ��� ���
    fService:longint;               // �������: ��������� ��� ���
    fName:array [1..32] of GtkChar;    // �������� ��������������
    fUnit:array [1..8] of GtkChar;     // ������� ���������
    fMinimum:double;                // ����������� ��������
    fDefault:double;                // ������������ ��������
    fMaximum:double;                // ������������ ��������
  end;

  TRSCSEMANTICEX = packed record     // �������� �������������
                                     // �������������� �� ����
    Code    : longint;               // ������� ���
    TypeSem : longint;               // ��� �������� ��. cTSTRING ...
    Reply   : longint;               // �������: ����������� (1) ��� ���
    Enable  : longint;               // �������: ����������(1),������������(2)
    Service : longint;               // �������: ���������,��������� ��� ���� �������� (1)
    Reserv  : longint;               // ������ (������ ���� ����� 0)
    Name    : array [1..32] of GtkChar; // �������� ��������������
    UnitSem : array [1..8] of GtkChar;  // ������� ���������
    Minimum : double;                // ����������� ��������
    Default : double;                // ������������ ��������
    Maximum : double;                // ������������ ��������
    Size    : longint;               // ����� ������ ���� �������� ���������
    Decimal : longint;               // �������� ���� �������� ���������
    Key     : array [1..16] of GtkChar; // ���������� ��� ��������� (����� ������ ��������� ���� � ���� ������)
  end;
 PRSCSEMANTICEX = ^TRSCSEMANTICEX; // 07/10/2009

  // �������� �������           // 07/03/13
 TRSCFORMULE  = packed record    // �������� �������

  Code:Longword;          // ���
  Semantic:Longword;      // ��� ����������� ��������� ��� 0
  Length:Longword;        // ����� ������ �������
  aType:Integer;          // ��� ��������������  1 - �������� �������,
                          // 2 - ����������, 4 - ��������������� �������,
                          // 8 - ������� �� ����� LUA
  Round:Integer;          // 0 - ��� ����������
                          // 1 - ���������� �� ������
                          // 2 - ���������� � �����������
                          // 3 - ���������� � �����������
  Precision:Integer;      // 3 - �� �����
                          // 2 - �� �����
                          // 1 - �� �������
                          // 0 - �� �����
                          // -1 - �� ������ ����� ����� �������
                          // -2 - �� ���� ������ ����� �������
                          // -3 - �� ���� ������ ����� �������
							  
  Index:Integer;          // ����� ���������� � ������ ��� Flag=4 ��� 0    // 23/06/2016
  Number:Integer;         // ����� ������� � ���������� ��� Flag=4 ��� 0   // 23/06/2016
							  
   Name: array [0..127] of WCHAR;     // �������� �������
end;
   PRSCFORMULE = ^TRSCFORMULE;

const SEMLIBIDENT =  $7E7E7E7E;


type

TSEMLIBLISTstruct = packed record  
    aType,                               // ��� ���� �������
    Zero    : integer;                   // ������������ �� 8 
    Name    : array [0..3] of PWCHAR;    // �������� �������� (����������) �������
                                         // Name[0]-ML_ENGLISH, Name[1]-ML_RUSSIAN...
end;

 TSEMLIBLIST = packed record  
  Ident   : integer;      // ������������� ������ 0x7E7E7E7E
  Count   : integer;      // ����� ����� �������� � ��������
  Element  : array [0..31] of TSEMLIBLISTstruct;        // �������� �������� ����������� ������� ������ � ���� Count
end;
   PSEMLIBLIST = ^TSEMLIBLIST;


//07/09/2004
// ��������� ��� �������� ���������� � ����������� ������ ������� �� ����������
  TDATAINFORMATION = packed record
       InputFileLength : Cardinal; // ����� ����� ������������ �����������
       Width           : integer;  // ������ �����������
       Height          : integer;  // ������ �����������
       BitPerPixel     : integer;  // ���-�� ��� �� ������
       Precision       : double;   // ����������� ����������� ����������� (�\�)
       PaletteType     : array[0..3] of GTKChar;  // ������ // 24/11/2010
       BlockCount      : integer;  // ���-�� ������ � ������
       RswFileLength   : double;   // ������������� ����� ����� *.rsw
       CompressImage   : array [0..15] of GtkChar;//������ �����������  //
  end;
  PDATAINFORMATION = ^TDATAINFORMATION;


  THeightArray=array [1..1] of double;
  PHeightArray=^THeightArray;

  TMTRBUILDPARM = packed record
    BeginX : double;         // ������������� ����������
    BeginY : double;         // ������ (���-��������� ����)
                             // �������  � ������
    Width  : double;         // ������ ������� � ������
    Height : double;         // ������ ������� � ������
    ElemSizeMeters : double; // ������ ������� ������-
                             // ������� ������� � ������
    ReliefType:integer;      // ��� ������� (0 - ���������� ������,
                             //  1 - ���������� + �������������)
                             //  2 - ������������� ������
    Filter   : integer;      // ������� ������������� ����� MTRCREA.TXT
                             // 0 - �� ������������, 1 - ������������
    UserType : integer;      // ��� ���������������� ������
  end;

 TBUILDMTW = packed record  //  ����������� ��������� �������� ������� �����
  StructSize : integer;    // ������ ������ ��������� :
                           //  sizeof (BUILDMTW)
  NotCheckDiskFreeSpace: integer;  // ���� - �� ��������� ������� ���������� ����� �� �����  // 13/12/2013

  BeginX     : double;     // ������������� ���������� ������
  BeginY     : double;     // (���-��������� ����) ������� � ������

  Width      : double;     // ������ ������� � ������
  Height     : double;     // ������ ������� � ������

  ElemSizeMeters : double; // ������ ������� ������������� �������
                           // � ������ �� ���������

  ElemSizeBytes : integer;  // P����� �������� ������� � ������
                           // (���������� �������� : 1,2,4,8)
                           // ��� �������� ������� ����� MTW �� ��������� ����� ��������
                           // mapBuildMtw �������� ������� ���� ������ ���� ����� 4
                           // �������� 1 ������������� ���� "unsigned char" (��� ���������� ������ = 0)
                           // �������� 2 ������������� ���� "short int" (��� ���������� ������ = -32767)
                           // �������� 4 ������������� ���� "long int" (��� ���������� ������ = -32767000)
                           // �������� 8 ������������� ���� "double" (��� ���������� ������ = -32767000)

  UnitH         : integer; // E������ ��������� ������
                           // (0 - �����, 1 - ���������,
                           //  2 - ����������, 3 - ���������� )

  ReliefType    : integer; // ��� ������� (0 - ���������� ������,
                           //    1 - ���������� + �������������
                           //    2 - �������������) 12/03/2008

  UserType      : integer; // ������������ �����, �����������
                           // � ����������� ��������

  Scale         : integer; // ����������� �������� �����������
                           // ��������� ����� (��� �������� �������
                           // �� ��������� ����� �������� �������
                           // ���� ����� �������� �� �����������
                           // �������� ��������� �����)

  HeightSuper   : integer; // ������ ��� ��������� � �������
                           // ����� ������ ������� � ����������
                           // ������� (0 - �������,
                           // 1 - ������������
                           // 2 - �����������  12/03/2008

  FastBuilding  : integer; // !!!���������� ����!!! ����� �������� ������� :
                           // ������ ���� ���� 14/03/2008

  Method        : integer; // ����� ���������� ����������� : // 12/03/2008
                           //  1 - ���������������� ������������ �� 16 ������������
                           //  2 - ���������������� ������������ �� 8 ������������
                           //  3 - ���������������� ������������ �� 16 ������������,
                           //      ����������� �����������
                           //  8 - �������� ������������ �� ����� �������������
                           //     (� ������ ������������ ������ �������� �������)
                           //  16 - ������� ����������� � ������� �������
                           //       ��� ReliefType = 2

 Extremum       :integer;  // ���� ������������ ��������� �����������
                           //     (��� Method = 1,2,3) :
                           //  0 - ��������� ���������� �� �����������
                           //  1 - ��������� ���������� �����������


{$IFDEF CPUX64}
    Reserv : integer;            //������������  15/09/2016
{$ENDIF}

 Border         : HOBJ;    // ������������� ���������� ������� �����,
                           // �� �������� ��������������� ����� �����������
                           // ������� (���� ������ ���������� ��� ���������
                           // �������, ������������ ���������� �����
                           // BeginX, BeginY, Width, Height ������
                           // ���������, �� ����� �� ���������������)

 LimitMatrixFrame : integer;
                           // ���� ����������� ��������� �������
                           // ��� Method = 1,2,3,8 (�������� �������
                           // ������������ ���������� ����� BeginX,
                           // BeginY, Width, Height ������ ���������) :
                           //  0 - ����������� �� �����������
                           //  1 - �������� ������� ��������������
                           //      ���������� ������
                           //  2 - �������� ������� ��������������
                           //      ���������� ������� ������������
                           //      �������� � ���������� �������

  NotUse3DMetric : integer;
                           // ���� - �� ������������ ���������� �������
                           // �������� :
                           //  0 - ���������� ������� ������������
                           //  1 - ���������� ������� �� ������������

  SurfaceSquare3DObject : integer;
                           // ���� - ������� ����������� ������ ����������
                           // ������� �� ��� ���������� ������� :
                           //  0 - �� ������� �����������
                           //  1 - ������� �����������
                           // ���� �������� NotUse3DMetric ����� 1,
                           // �� ����������� �� ��������

  AltitudeMarksNet : integer;
                           // ���� �������������� ��������� �������� ����� ���
                           // ���������������� ������������ (Method = 1,2,3) :
                           //  0 - ��� ������ �������� ����� ����������� ���������� �
                           //      ��������� � ������� ����� ������� ������ (��� �������
                           //      ������ - 3D-�������,��������� �� ����� �� ������ ��
                           //      16 ���������� �����������; ������ ��������� �������
                           //      ������������ ������� ����� � �������, ��������� ���
                           //      ������������ ������� �� ����� �� ������� �����������.
                           //      ���� ������� ������ ������������ ������� ���� �����
                           //      � ���������� ������� ��� ������ �������� ����� � ��������
                           //      ���������� ������������ ��������� �������.
                           //  1 - �� ������ �������� ����� �������� ������������,
                           //      ���� ������������ (��� ����� ����) ��������� � �������
                           //      � ���� 3D-�������� (���� ����� �� ���������� �������
                           //      � ���������� �������, �� ��� ��������� �������,
                           //      ���� ����������, �� ��������� ����� �����, ������������
                           //      ��� �������� � ������ ����������� � ��������);
                           //  2 - �������������� ��������� �������� ����� �� �����������,
                           //      ������ ����� ��������� � ���� ������� �������,
                           //      ������ ����������� ��� ���������� �����������
                           //      �� ������ ������� �����

  LimitMatrixByFramesOfSheets : integer;
                           // ���� ����������� ������� ������� ������
                           // (��� Method = 1,2,3,8) :
                           //  0 - ����������� ������� �� �����������,
                           //      ������������� �������� ����� �������������
                           //      ��� ����� ������
                           //  1 - ����������� ������� �����������,
                           //      ������������� �������� �������������
                           //      ������ ������ ��������� ����� ������

  ElemSizeMetersX: double; // ������ ������� ������������� �������             // 17/01/2017
                            // � ������ �� ��������� �� ��������� (X) ��� 0
  Reserve : array [0 .. 11] of GtkChar;  // ������ ���� ����

 end;
 PBUILDMTW = ^TBUILDMTW; // 02/10/2009

  TMTRDESCRIBE = packed record      // �������� �������� �������
    fName : array [1..256] of GtkChar; // ��� �����
    fMinHeightValue : double;       // ����������� �������� �������� � �������
    fMaxHeightValue : double;       // ������������ �������� �������� � �������
    fElementInPlane : double;       // ������ �������� � ������ �� ���������
    fFrameMeters : TMAPDFRAME;      // �������� ������� (�����)
    fReliefType  : integer;         // ��� ������� (0 - ���������� ������,
                                    //  1 - ���������� + �������������)
                                    //  2 - ������������� ������
                                    // (��� ���������������� ������)
    fUserType : integer;            // ��� ���������������� ������
    fView : integer;                // ������� ��������������
  end;
  PMTRDESCRIBE = ^TMTRDESCRIBE; // 02/10/2009

 const  MTRTILEIDENT  =    $7F7F7F7F;
 
 type
 
 TMTRTILE   = packed record    // �������� ��������� ������� �� ������� ���������

    Ident,             // ������������� ������ 0x7F7F7F7F
    Length,            // ����� ������ � ������ � ���������� (262208 ����)
    Width,             // ����� �������� (256)
    Height,            // ����� �����    (256)
    Epsg,              // ��� EPSG �������� ������� ��������� (��������, 3857)
    Level,             // ������� ������� � �������� ������ (���������� �������� ������)
    WidthNumber,       // ����� ������� ������� � �������� ������� ��������� (� ������������ �� -180 �� +180 ��������)
    HeightNumber,      // ����� ������ ������� � �������� ������� ��������� (� ������������ �� -90 �� +90 ��������)
    aUnit,              // ������� ��������� ������ (0 - �����, 1 - ���������, 2 - ����������, 3 - ����������)
    UnitPlane,         // ������� ��������� � ����� (0 - �����, 1 - ���������, 2 - ����������, 3 - ����������)
    ItemWidth,         // ������ �������� � �������� �������� ��������� � ����� (���������)
    ItemHeight,        // ������ �������� � �������� �������� ��������� � ����� (���������)
    MinValue,          // ����������� �������� � �������
    MaxValue    : integer;          // ������������ �������� � �������
    Reserve     : array[0..1] of integer;        // ������, �������� ����

    Value       : array[0..255, 0..255] of integer;    // �������� ����� � �������� �������� ��������� ������
  end;
  PMTRTILE = ^TMTRTILE; 


TMTRTILE2   = packed record   // �������� ��������� ������� �� ������� ���������


    Ident,             // ������������� ������ 0x7F7F7F7F
    Length,            // ����� ������ � ������ � ���������� (131136 ����)
    Width,             // ����� �������� (256)
    Height,            // ����� �����    (256)
    Epsg,              // ��� EPSG �������� ������� ��������� (��������, 3857)
    Level,             // ������� ������� � �������� ������ (���������� �������� ������)
    WidthNumber,       // ����� ������� ������� � �������� ������� ��������� (� ������������ �� -180 �� +180 ��������)
    HeightNumber,      // ����� ������ ������� � �������� ������� ��������� (� ������������ �� -90 �� +90 ��������)
    aUnit,              // ������� ��������� ������ (0 - �����, 1 - ���������, 2 - ����������, 3 - ����������)
    UnitPlane,         // ������� ��������� � ����� (0 - �����, 1 - ���������, 2 - ����������, 3 - ����������)
    ItemWidth,         // ������ �������� � �������� �������� ��������� � ����� (���������)
    ItemHeight,        // ������ �������� � �������� �������� ��������� � ����� (���������)
    MinValue,          // ����������� �������� � �������
    MaxValue    : integer;          // ������������ �������� � �������
    Reserve     : array[0..1] of integer;        // ������, �������� ����

    Value       : array[0..255, 0..255] of Word;    // �������� ����� � �������� �������� ��������� ������
  end;
  PMTRTILE2 = ^TMTRTILE2;



  TMTRDESCRIBEUN = packed record      // �������� �������� �������
    fName : array [0..1023] of WChar; // ��� �����
    fMinHeightValue : double;       // ����������� �������� �������� � �������
    fMaxHeightValue : double;       // ������������ �������� �������� � �������
    fElementInPlane : double;       // ������ �������� � ������ �� ���������
    fFrameMeters : TMAPDFRAME;      // �������� ������� (�����)
    fReliefType  : integer;         // ��� ������� (0 - ���������� ������,
                                    //  1 - ���������� + �������������)
                                    //  2 - ������������� ������
                                    // (��� ���������������� ������)
    fUserType : integer;            // ��� ���������������� ������
    fView : integer;                // ������� ��������������
    Zero  : integer;                // ������
  end;
  PMTRDESCRIBEUN = ^TMTRDESCRIBEUN; // 04/04/2014

  TGEODEGREE = packed record   // ���������� ����� � ��������
    Degree:longint;     // �������
    Minute:longint;     // ������
    Second:single;      // �������
  end;

  TTASKPARM = packed record       // ��������� ���������� ������
    Language:longint;      // ��� ����� �������� (1 -ENGLISH,
                           // 2 - RUSSIAN, ...)

{$IFDEF CPUX64}                                                   // 11/12/2013
    TaskparmZero:longint;  // ������������
//    Resource:Int64;        // ������ �������� ����������         // ������ 8 ���� 26/03/2014
//{$ELSE}
//    Resource:longint;      // ������ �������� ����������
{$ENDIF}
    Resource : HINST;         // ������ �������� ����������         // 26/03/2014
    HelpName : GtkPChar;      // ������ ��� ����� ".hlp"
    IniName  : GtkPChar;      // ������ ��� ����� ".ini" ����������
    PathShell : GtkPChar;     // ������� ���������� (exe,dll,...)
    ApplicationName : GtkPChar; // ��� ����������
    Handle : HWND;            // ������������� ���� ����������
  end;
    PTASKPARM = ^TTASKPARM; // 07/10/2009
// ���� � TTASKPARM ����������� ������ ��� ����� ".hlp",
// �� ���� ���� ������ ��������� ����� � ������� 6700,
// ������� ����� ���������� ��� ������� ������� "������"

type
  TTASKPARMEX = packed record       // ��������� ���������� ������
    Language:longint;      // ��� ����� �������� (1 -ENGLISH,
                           // 2 - RUSSIAN, ...)
{$IFDEF CPUX64}                                                   // 11/12/2013
    TaskparmZero:longint;  // ������������
//    Resource:Int64;        // ������ �������� ����������         // ������ 8 ���� 26/03/2014
//{$ELSE}
//    Resource:longint;      // ������ �������� ����������
{$ENDIF}
    Resource : HINST;         // ������ �������� ����������         // 26/03/2014
    HelpName : GtkPChar;      // ������ ��� ����� ".hlp"
    IniName  : GtkPChar;      // ������ ��� ����� ".ini" ����������
    PathShell: GtkPChar;      // ������� ���������� (exe,dll,...)
    ApplicationName : GtkPChar; // ��� ����������
    Handle : HWND;            // ������������� �������� ���� ����������
    DocHandle : HWND;         // ������������� ���� �����
    StayOnTop : longint;      // ������� ��������� � ����� �������� StayOnTop // 26/10/2006
  end;
 PTASKPARMEX = ^TTASKPARMEX; // 02/10/2009

{$IFDEF LINUXAPI}
const
 R2_COPYPEN    = 3;
 R2_NOT        = 10;
 R2_XORPEN     = 6;
 R2_NOTXORPEN  = 9;
{$ENDIF}

type
{$IFDEF CPUX64}                                                          // 11/12/2013
 TPAINTPARM = packed record
    Image : integer;    // ��� ������� �����������                              // 4
    Mode  : integer;    // ������� ����������� � ���������������                // 4



                        //   R2_COPYPEN   - ����������� ������
                        //   R2_NOT       - ����������� ��������� ���� (���� ������������)
                        //   IM_SCALE, IM_DONTSCALE, ... - �������� ����������������
                        //    (������������ ��� Image ������ IMG_OBJECT)
                        // ������ �������������: Mode = R2_NOT | IM_SCALE

    Parm  : pointer;    // ��������� ����������� ��� ��������������� �������.   // 8
                        // ������������ ��� Image ������ IMG_OBJECT (���������
                        // � ����� ������� ��������������� �� ��������������)
  end;
{$ELSE}
 TPAINTPARM = packed record
    Image : integer;    // ��� ������� �����������
    Parm  : pointer;    // ��������� ����������� ��� ��������������� �������.
                        // ������������ ��� Image ������ IMG_OBJECT (���������
                        // � ����� ������� ��������������� �� ��������������)
    Mode  : integer;    // ������� ����������� � ���������������
                        //   R2_COPYPEN   - ����������� ������
                        //   R2_NOT       - ����������� ��������� ���� (���� ������������)
                        //   IM_SCALE, IM_DONTSCALE, ... - �������� ����������������
                        //    (������������ ��� Image ������ IMG_OBJECT)
                        // ������ �������������: Mode = R2_NOT | IM_SCALE
  end;
{$ENDIF}
  PPAINTPARM = ^TPAINTPARM; // 30/09/2009

  TDOUBLEPOINT = packed record
    X:double;
    Y:double;
  end;
  PDoublePoint = ^TDoublePoint;
  PPDoublePoint = ^PDoublePoint;

  PTPoint = ^TPoint;
  PPoints = ^TPoints;
  TPoints = array [1..1] of TDOUBLEPOINT;
  PIntegers = ^TIntegers;
  TIntegers = array [1..1] of integer;

  TTEXTRECORD =  packed record
    Length:integer;
    Text:array [0..255] of GtkChar;
  end;
  
//-----------------------------------------------------------------
// �������� ��������� ������������ ��'����
//-----------------------------------------------------------------
// ������� ��������� � ��� ������� ���������
// �������� � ���������� ������� ������������
//-----------------------------------------------------------------


  TPLACEDATA = packed record
    Points : PPoints;         // ������ ��������� ��������� ��������
    PolyCounts : PIntegers;   // ������ ���������� ����� ��������
    Count : integer;          // ���������� �������� ��� �������� TEXTRECORD
                              // ��� ������� ���� IMG_TEXT :
{$IFDEF CPUX64}  
    Reserv : integer;        // ������ ���� ����� 0
{$ENDIF}  
  
    PolyText : ^TTEXTRECORD;  // ������� ��������������� �������������
                              // �������� TEXTRECORD,
                              // ����� �������� ����� Count

  end;

  TMTR3DVIEW = packed record
    AreaCenterX:double;       // ����� ������������ ������� X,Y
    AreaCenterY:double;       //   (� ������ �� ���������)
    ViewPointX:double;        // ����� ���������� X,Y,H
    ViewPointY:double;        //   (� ������ �� ���������)
    ViewPointH:double;        //
    ShowScale:integer;        // ����������� ��������
    Style:byte;               // C���� (0 - �����, 1 - �������)
    ShowGrid:byte;            // ������� ����� (0 - ���, 1 - ����)
    GridStep:byte;            // ��� ����� � �������� (0 - ����� ���)
    ModelHeight:byte;         // ������ ������ � �������� (2 - 127)
  end;

  TCOORDINATES = packed record
    Coord:array [1..4] of double;
  end;

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++ ��������� ��� �������� ������, ���� � ++++++++++++++++++
//++++++++ ���������������� ����                 ++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  TAPPENDPLANELIST = packed record
   Length        : integer;                // ����� ������ ��������� APPENDLIST
   Nomenclature  : array [1..32] of GtkChar;  // ������������ �����
   ListName      : array [1..32] of GtkChar;  // �������� �������� �����
   FileName      : array [1..256] of GtkChar; // ��� ����� �� �������� � ������ �����
                                           // ������ ���� � ������ ������ � ������������
                                           // HDR,DAT,SEM ...
   Reserv        : integer;                // ������ ��� ������������, ������ ���� 0
                                           // 19.10.2001

   // ������������� ���������� ����� � ������
   // (��� ���������������� ����� (SITE) ������ ���� 0)
   XSouthWest : double;  // X �-�
   YSouthWest : double;  // Y �-�
   XNorthWest : double;  // X �-�
   YNorthWest : double;  // Y �-�
   XNorthEast : double;  // X �-�
   YNorthEast : double;  // Y �-�
   XSouthEast : double;  // X �-�
   YSouthEast : double;  // Y �-�
  end;


 TCREATEPLANE = packed record
   Length  : integer;               // ����� ������ ��������� CREATEPLANE
   MapName : array [1..32] of GtkChar; // ��� ������
   Scale   : integer;               // ����������� �������� �����
   List    : TAPPENDPLANELIST;      // �������� ����� �����
  end;

 TCREATESITE = packed record
   Length  : integer;            // ����� ������ ��������� CREATESITE
   MapName : array [1..32] of GtkChar; // ��� ������
   MapType : integer;            // ���������� ��� �����
   MaterialProjection : integer; // �������� ���. ���������
   Scale : integer;              // ����������� �������� �����
   // � ��������
   FirstMainParallel  : double;  // ������ ������� ���������  StandardParallel1
   SecondMainParallel : double;  // ������ ������� ���������  StandardParallel2
   AxisMeridian       : double;  // ������ �������� (������� ������ ��������) CentralMeridian
   MainPointParallel  : double;  // ��������� ������� ����� (������ ������ ��������) LatitudeOfOrigin
  end;

 TCREATESITEEX = packed record   // 26/06/2007
   Length  : integer;            // ����� ������ ��������� CREATESITE
   MapName : array [0..255] of GtkChar; // ��� ������ � ��������� ANSI (UNICODE - ��� ������� "Un") 13/08/2007
   MapType : integer;            // ���������� ��� �����
   MaterialProjection : integer; // �������� ���. ���������
   EllipsoideKind : integer;     // ��� ���������� (1 - �����������, 9 - WGS84,...) //22/02/2009
   Scale : integer;              // ����������� �������� �����
   Reserve : integer;            // ������ (������ ���� 0)
   // � ��������
   FirstMainParallel : double;   // ������ ������� ���������
   SecondMainParallel : double;  // ������ ������� ���������
   AxisMeridian : double;         // ������ ��������
   MainPointParallel : double;    // ��������� ������� �����
   PoleLatitude : double;         // ������ ������ ��������
   PoleLongitude : double;        // ������� ������ ��������
  end;


  TCREATESITEUN = packed record   // 26/06/2007
   Length  : integer;            // ����� ������ ��������� CREATESITE
   MapName : array [0..127] of WideChar; // ��� ������ � ��������� ANSI (UNICODE - ��� ������� "Un") 13/08/2007
   MapType : integer;            // ���������� ��� �����
   MaterialProjection : integer; // �������� ���. ���������
   EllipsoideKind : integer;     // ��� ���������� (1 - �����������, 9 - WGS84,...) //22/02/2009
   Scale : integer;              // ����������� �������� �����
   Reserve : integer;            // ������ (������ ���� 0)
   FirstMainParallel : double;   // ������ ������� ���������
   SecondMainParallel : double;  // ������ ������� ���������
   AxisMeridian : double;         // ������ ��������
   MainPointParallel : double;    // ��������� ������� �����
   PoleLatitude : double;         // ������ ������ ��������
   PoleLongitude : double;        // ������� ������ ��������
  end;


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  TMAPADJACENTSECTION = packed record     // �������� �������� �������
    number:integer;                // ����� �������
    first:integer;                 // ������ ����� �������
    last:integer;                  // ��������� ����� �������
  end;
  PMAPADJACENTSECTION = ^TMAPADJACENTSECTION; // 06/10/2009
//  TMAPADJACENTLIST = packed record
//    ListName:array [0..24] of GtkChar;     // ��� �����
//    Key:integer;                        // ����� �������
//    Number:integer;                     // ����� �������
//    First:integer;                      // ������ ����� �������
//    Last:integer;                       // ��������� ����� �������
//  end;
//  TARRMAPADJACENTLIST = array [1..1] of TMAPADJACENTLIST;
//  PARRMAPADJACENTLIST = ^TARRMAPADJACENTLIST;

  PMAPADJACENTLISTEX = ^TMAPADJACENTLISTEX;
  TMAPADJACENTLISTEX = packed record
    ListName  : array [0..31] of GtkChar;   // ��� �����
    List      : integer;                 // ����� ����� � ������
    ObjectKey : integer;                 // ����� �������
    ObjectNum : integer;                 // ���������������� ����� ������� �� �����
    Excode    : integer;                 // ����������������� ��� �������
    First     : integer;                 // ������ ����� �������
    Last      : integer;                 // ��������� ����� �������
  end;
  PTMAPADJACENTLISTEX = ^TMAPADJACENTLISTEX;


// �������� �������� ������� // 04/04/2014

  
  TCONNECTPATH = packed record

  IsForward     : integer;       // ����������� ������� �� ������� ����������
                      // (����������� �� �������� ���������� ������ ���������
                      // � ������������ ����������)
  FirstPoint    : integer;     // ������ ����� ������� �� ������� ����������
  LastPoint     : integer;      // ��������� ����� ������� �� ������� ����������
  NearSubject   : integer;    // ����� ��������� ����������
  NearFirstPoint: integer; // ������ ����� ������� �� �������� ����������
  NearLastPoint : integer;  // ��������� ����� ������� �� �������� ����������
 end;
  PCONNECTPATH = ^TCONNECTPATH;



// �������� �� ����� � �������������� �����
type
 TRSCOBJECT  = packed record
   Length  : integer  ; // ������ ���������
   Code    : integer  ; // ����������������� ���
   Local   : integer  ; // �������� �����������  (OBJECT_LOCAL)
   Segment : integer  ; // �0��� ���� ( �� ����� 255)
   Scale   : integer  ; // 1 - �� ��� ��������������
                        // 0 - �� ��� �� ��������������
   Direct  : integer  ; // ����������� ���������� �� ���� (OBJECT_DIRECT)
   Bot     : integer  ; // ������ ������� ��������� (����������� ��������)
   Top     : integer  ; // ������� ������� ��������� (����������� ��������)
   Name    : array [0..31] of GtkChar; // �������� ( �� ����� 30)
 end;
 PRSCOBJECT = ^TRSCOBJECT;  // 07/10/2009

 TRSCOBJECTDESC  = packed record //(� Code �� Name ������� ��������� TRSCOBJECT)        // ������� ��� !!!
   Code    : integer  ; // ����������������� ���
   Local   : integer  ; // �������� �����������  (OBJECT_LOCAL)
   Segment : integer  ; // �0��� ���� ( �� ����� 255)
   Scale   : integer  ; // 1 - �� ��� ��������������
                        // 0 - �� ��� �� ��������������
   Direct  : integer  ; // ����������� ���������� �� ���� (OBJECT_DIRECT)
   Bot     : integer  ; // ������ ������� ��������� (����������� ��������)
   Top     : integer  ; // ������� ������� ��������� (����������� ��������)
   Name    : array [0..31] of GtkChar; // �������� ( �� ����� 30)
   Key     : array [0..31] of GtkChar; // ���������� �������� - ���� ( �� ����� 30)
 end;


// 05/01/2005 -----------------------------------------------------
// C�������� �������� � ������� ����������� ��������
// ��� ������� ���� � ���. (��. MAPGDI.H)
// ��� ������� � �������� - ������� ����� �����
// ���� ������ "���������" - TwoPoint = 1, ����� - 0;
// ��� "���������������" ������� - ������� 0;
// ----------------------------------------------------------------
 TIMAGESIZE = packed record
   Number : Cardinal;          // ����� ������� �����������
   Base   : Cardinal;          // ������ ���� �������
   DeltaH : integer;           // ���������� �� ������ ��������
   DeltaV : integer;           // ���� ��������� �� ����(�����
                               // �������) �������
   HorizontalSize : Cardinal;  // ������� �� �����������
   VerticalSize   : Cardinal;  // ������� �� ���������
   Parametrs : Cardinal;
        {  Horizontal     : 1 ���;   // ���������� �������������
           Vertical       : 1 ���;   // ���������� �����������
           TwoPoint       : 1 ���;   // ������� �� 2 �����
           AlignV         : 2 ����;  // ������������ �� ���������
           AlignH         : 2 ����;  // ������������ �� �����������
           Wide           : 2 ����;  // ������ ������
           FontType       : 8 ���;   // ��� ������ (0-4)
           Italic         : 1;       // ������ ��������
           Rezerv         : 14 ���;  // ������}
 end;//  IMAGESIZE;
 PIMAGESIZE = ^TIMAGESIZE; // 17/11/2010


// �������� ��������� � �������������� ����������� �����
 type
  TRSCSEMANTIC = packed record
  // ���������� !!!
    fCode    : integer;               // �����������������  ��� ���������
    fType    : integer;               // ��� ��������� (SEMT_)
    fReply   : integer;               // ����������� ����������
    fService : integer;               // 1 - ��������� ���������
    fMetric  : array [0..7] of GtkChar;  // �������� ������ ���������
    fName    : array [0..31] of GtkChar; // �������� ( �� ����� 30)
 end;

  TPPLACE = (            // ����������� ������� ���������
    PP_MAP,              // ���������� ����� � ������� ����� � ���������
    PP_PICTURE,          // ���������� ����� � ������� ����������� � ��������
    PP_PLANE,            // ���������� ����� � ������� ������������� �������
                         // �� ��������� � ������
    PP_GEO,              // ���������� ����� � ������������� �����������
                         // � �������� � ������� ���������
    PP_GEOWGS84          // ���������� ����� � ������������� ����������� //28/01/2016
                         // � �������� � ������� WGS-84
  );


  TCURRENTPOINTFORMAT = ( // ������ ����������� �����  // 09/06/2012
   PLANEPOINT      = cpf_PLANEPOINT,  // � ������ �� ��������� � ������� ���������� �������� ("DocProjection")
                         // ��������� ����������� 1942�. ��-42 ��� ��-95,
                         // ���� ������� ����� � ��-95
   MAPPOINT        = cpf_MAPPOINT,  // � �������� �������� ����� (���������)
   PICTUREPOINT    = cpf_PICTUREPOINT,  // � �������� �������� ������� �����������

   GEORAD          = cpf_GEORAD,  // � ��������
   GEOGRAD         = cpf_GEOGRAD,  // � ��������
   GEOGRADMIN      = cpf_GEOGRADMIN,  // � ��������, �������, ��������

                         // ���������� ��������� WGS84
   GEORADWGS84     = cpf_GEORADWGS84,  // � ��������
   GEOGRADWGS84    = cpf_GEOGRADWGS84,  // � ��������
   GEOGRADMINWGS84 = cpf_GEOGRADMINWGS84,  // � ��������, �������, ��������

   PLANE42POINT    = cpf_PLANE42POINT, // � ������ �� ��������� �� ��������� ���� � ��-42

                         // ���������� ��������� ��-90.02 (SGS-85)
   GEORADPZ90      = cpf_GEORADPZ90, // � ��������
   GEOGRADPZ90     = cpf_GEOGRADPZ90, // � ��������
   GEOGRADMINPZ90  = cpf_GEOGRADMINPZ90, // � ��������, �������, ��������

   PLANEWORKSYS      = cpf_PLANEWORKSYS, // � ������ �� ��������� � ������� ������� � ������������ � ����������� ("WorkSystem")
   GEORADWORKSYS     = cpf_GEORADWORKSYS, // � �������� � ������� �������
   GEOGRADWORKSYS    = cpf_GEOGRADWORKSYS, // � �������� � ������� �������
   GEOGRADMINWORKSYS = cpf_GEOGRADMINWORKSYS, // � �������� �������, �������� � ������� �������

   PLANE95POINT      = cpf_PLANE95POINT, // � ������ �� ��������� �� ��������� ���� � ��-95          // 15/11/10

   LASTPOINTFORMAT   = cpf_LASTPOINTFORMAT  // ���������� ��������
);


 type
  TMAPAPILANGUAGE = (     // ���� ������ ��������� � ������� ����
    ML_ENGLISH   = cML_ENGLISH,           // ����������           // 29/03/2012 - ������� ���������, ����� � Builder ���������� � ����
    ML_RUSSIAN   = cML_RUSSIAN            // �������
  );

 const
  // ���������� � ��������� ��� ������� ����� // 31/07/2012
  cGISFormsForLang : array[TMAPAPILANGUAGE] of string =
  ( sGISFORMSE,
    sGISFORMS
   );

  // ����� ��������� ������ - ��� �������� // 31/07/2012
  cLANGUAGES_AVAILABLE : set of TMAPAPILANGUAGE = [ML_ENGLISH,  ML_RUSSIAN];
  cLANGCODES_AVAILABLE = [cML_ENGLISH, cML_RUSSIAN];


(*
// !!!!!!!! ���� ��������� � �����������, ��� ������� ����, ����� � Builder ���������� � ����
// ����� ��������� � Builder-e, ���� ������� ��� ����  // 12/06/2012
  TMAPAPILANGUAGE = (     // ���� ������ ��������� � ������� ����
    ML_ENGLISH,           // ����������
    ML_RUSSIAN            // �������
  );
*)


 type
  TObjectStyle = (   // ������ ������������ ������� ����� � ���������� TMapObj
    OS_NOVIEW,       // ������ ����������� �� ������������
    OS_NORMAL,       // ����������� OS_NOVIEW
    OS_SELECT        // ������ ���������� �������� TObjectStyleSelect ��������
  );

  TObjectStyleSelect = ( // ��� ������������ (�����������) ����������� ������� �����
    SEL_USERFUNC,    // ���������������� ��� ��������� (������� �� �����������
                     // ������������� ���������� Image, Mode, Parm
    SEL_COLOR,       // ��������� ����� ��������� ������� �������� ������
    SEL_LINE,        // ��������� ������. ��������� Image, Mode, Parm ������������
                     // �������� ���������� ������ � 2 ������� (������� IMGLINE)
    SEL_BLINK1,      // ��� ��������� ������ ����� ������������ ������� ������
                     // ColorImage, ����� ����� ����� Interval ������
                     // ColorImageUp. ��� ���� ����������� ����������� ���
                     // ����������� ������� ����� (�� ����������� �����).
    SEL_BLINK2,      // ��� ��������� ������ ����� ������������ ������� ������
                     // ColorImage, ����� ����� ����� Interval ������
                     // ColorImageUp. ��������� ������� ���������� ��������������
                     // ������ (������� IMGSQUARE), � �������� � ��������� �������
                     // �������� ���������� ������ � 2 ������� (������� IMGLINE)
    SEL_OBJUP,       // ������ �������������� ������ ����� ����������� ��������
    SEL_OBJUPMODE,   // ������ �������������� ������ ����� ����������� �������� �����������,
                     // �� � �������������� ������ �����������, ���������� � Mode
    SEL_BLINK3       // ��� ��������� ������ ����� ������������ ����������� �������� �������
                     // � ������ ����������� R2_COPYPEN, ����� ����� ����� Interval
                     // � ������ ����������� R2_NOT

  );

   TSemanticStyle = ( // ������ ������ �������� ��������� �������� �����
    SS_NODECODE,     // �������� �� �������������
    SS_NORMALDECODE, // �������� �������������
    SS_FULLDECODE    // �������� ������������� � ����������� ������� ���������
  );

  TColorStyle = (    // ����� ����������� ���������� �������
    CS_GRAYSCALE,    // � ��������� ������
    CS_COLOR         // � �����
  );

  TSemanticCondition = (   // ������� ������/����������� �� ���������
    SC_BAD,       // ������ � ������
    SC_LESS,      // ������
    SC_EQUAL,     // �����
    SC_LESSEQ,    // ������ ��� �����
    SC_MORE,      // ������
    SC_NOTEQ,     // �� �����
    SC_MOREEQ,    // ������ ��� �����
    SC_ANY       // ����� ��������
    );

const
    SC_CMOR  = 16; // ����������� ���� �� ���� ������� ������
    SC_CMAND = 32; // ����������� ���

type

  TConvertorTypeSource = (    // ���� �������������� ������ ����������
    TS_VIEW,
    TS_MAP,
    TS_DIR,
    TS_SXF,
    TS_TXT,
    TS_S57
  );

  TConvertorTypeDest = (    // ���� �������������� ������ ����������
    TD_VIEW,
    TD_MAP,
    TD_DIRSXFWIN,
    TD_DIRSXFDOS,
    TD_DIRTXTXY,
    TD_DIRTXTBL,
    TD_SXFWIN,
    TD_SXFDOS,
    TD_TXTXY,
    TD_TXTBL
  );

// ���� �������� ���������
const
  SEMT_TSTRING    = 0;     // ���������� ������
  SEMT_TNUMBER    = 1;     // �������� ��������
  SEMT_TBMPFILE   = 10;    // ��� ����� BMP
  SEMT_TOLEFILE   = 11;    // ��� �����,��������������� OLE-��������
  SEMT_TREFER     = 12;    // ������ �� ������������ ������ �����
                           // (���������� ����� �������)
  SEMT_TMAPFILE   = 13;    // ��� �����-�������� ������
  SEMT_TTXTFILE   = 14;    // ��� ���������� �����
  SEMT_TPCXFILE   = 15;    // ��� ����� PCX
  SEMT_TCODE      = 16;    // �������� � ���� ��������� ����
                           // �� �������������� ��������

// ��������� �������� ���������
type
  TMETAFILEBUILDPARM = packed record
  CenterX    : integer;     // ���������� ������ ��������� �������
  CenterY    : integer;     // (� ������ �� ���������)
  Width      : integer;     // ������ ��������� (� �����������)
  Height     : integer;     // ������ ��������� (� �����������)
  Scale      : integer;     // ������� �����������
  VisualType : integer;     // ��� ������������
                            //  1 - �������� (BITMAP) �������� BMP
                            //  2 - ����������� (BITMAP) �������� BMP
                            //  3 - ����������
                            //  4 - ���������� ���������� (��� ������� ���������)
                            //  5 - ��������� ����������
 end;

// ����������� ��������� �������� ���������
type
 TMETAFILEBUILDPARMEX = packed record
  Frame      : TMAPDFRAME;  // �������� ����������� � ������ �� ���������, X1Y1 - ����� ������  X2Y2 - ������ ������� ����
  Scale      : integer;     // ������� �����������
  VisualType : byte;        // ��� ������������
                            //  1 - �������� (BITMAP) �������� BMP
                            //  2 - ����������� (BITMAP) �������� BMP
                            //  3 - ����������
                            //  4 - ���������� ���������� (��� ������� ���������)
                            //  5 - ��������� ����������
  Border     : byte;        // ���� ��������� �����
  Intensity  : byte;        // ������������� ������� % (0-100)
  Black      : byte;        // ���� �����-������ �����������(1 - ����, 0 - ���)
  DontClip   : byte;        // 0 - �������� ������� �� �������� ����������� 1 - �� ��������
  Reserve    : array [0..6] of byte;  // ������ (������ ���� �������)
 end;


// ���� ������ ������ ������ ������� � ������
const RMF_COMPR_LZW  =  1;             // �������� ������ LZW ��� �������                // 03/12/11
      RMF_COMPR_JPEG =  2;             // �������� ������ JPEG ��� 24 ������ �������     // 03/12/11
      RMF_COMPR_32   =  32;            // �������� ������ ��� ������

//******************************************************************
//   ��������� ������ ������� �  ��������� ���������� ������������         *
//  (����� ������ ���������� � ����� mapgdi.h
//******************************************************************
//      ��� ������� (�������, �����, ���������� ...) � ��������    *
//                 (� �������� ����� ����������)                   *
//******************************************************************

// �������� ��������� �������� � �������
const MKMINPIX = 260;  //  1000 * 25.4 / 96 = 264.583333

// 28/09/2009 - ���������� � MAPGDI.INC
{const
 IMG_LINE    = 128; // �����
 IMG_DOT     = 129; // ���������� �����
 IMG_OBJECT  = 255; // ������� ����������� �� ���� �� ���������
 IMG_SQUARE  = 135; // ��������� ��'���

type
 TIMGLINE = packed record     // (128) �����
   Color : TColor;        // ���� ����� (COLORREF)
   Thick : integer;       // ������� �����
end;

type
 TIMGDOT = packed record     // (129) �������
   Color : TColor;        // ���� ����� (COLORREF)
   Thick : integer;       // ������� ������
   Dash  : integer;       // ����� ������
   Blank : integer;       // ����� �������
end;

type
 TIMGSQUARE = packed record  // (135) ��������� ��'���
  Color : TColor;         // ���� �������
end;
}


const
 IMGC_NOMARK = $0FFFFFFFF;  // �� �������� (��� IMG_OBJECT)

// 28/09/2009 - ���������� � MAPGDI.INC
{
type
 TIMGOBJECT = packed record  // (255) ����������/�������� ��'���
                             //  � �������� ������ ��������������
  Color  : TColor;           // ���� ��������� ������� �� �����
                             //  (IMGC_NOMARK - �� ��������)
  Incode : integer;          // ���������� ��� �������
                             //  (0 - ��������������� �� Excode � Local)
  Excode : integer;          // ������� ��� �������
  Local  : integer;          // ����������� ������� (IMGL_LINE, ...)
end;
}

type
// ��������� ����������� ���������� �� �� ���� � ��������
  TOBJECTFORM = packed record
    Window     : HWND ;             // ������������� ���� �����
    Position   : TPoint;            // ��������� ������� � ����� ������ �
                                    // �������� � ����������� ������
    LeftTopPosition: TPoint;        // ���������� ������ �������� ���� �����
                                    // � �������� � ����������� ������
    PlaceSystem: Integer;           // ������� ������� ����������� ���������
                               // 1 - � ������ �� ���������
                               // 2 - � �������� �������� ����� (���������)
                               // 3 - � �������� �������� ������� �����������
                               //     ��������� ����������� 1942�.
                               // 4 - � �������� � ������������ � ���������
                               // 5 - � �������� ...
                               // 6 - � ��������, �������, �������� ...
                               // 7 - � �������� (���������� ��������� WGS84)                    //28/01/05
                               // 8 - � �������� (���������� ��������� WGS84)
                               // 9 - � ��������, �������, �������� (���������� ��������� WGS84)
  FormMode        : smallint;  // ��� ���� ����������� �������
                               // 0 - ������ ����� �������� �� �������
                               // 1 - ����� �������� � ��������� �������
                               // 2 - ����� �������� � ������� �������
                               // 3 - ����� �������� � ������� ����������� �������
                               // 4 - ����� �������� � ������� ��� �������
                               // 5 - ����� �������� � ���������� ��� �������
                               // 6 - ����� �������� � ����������� ��� �������
                               // -1 - -6 ��������������� ���������� � ������������� ��������
  MarkPointMode  : smallint;   // ��� ���� ����������� �����
                               // ���������� ��������������� �������
                               // ������������ ��������� ������� � ������� ��������
                               // ��������� ����������� ��������� ������� ����� �������
                               // 0 - �� ����������� ��������� ������� ����� �������
                               // 1 - ����������� ��������� ������� ����� �������
                               // ������ ������������ ����
                               // 0 - ������������ ������ �����, ���������� ������
                               // 1 - ������������ ��� �����
  Map            : HMap;       // ������������� �������� ��������� �����
                               // (��������� �� TMapAccess)
 end;

  // ��� ������� ����������� ���������� �� ������� �����
  const
   dlMinCommon   =  0;  // ����� �������� �� �������
   dlMinSemantic =  1;  // ������� �������� ��������� �������
   dlMinMetric   =  2;  // ������� �������� ������� �������
   dlMinScale    =  3;  // ������� �������� ������� ����������� �������
   dlMinImage    =  4;  // ������� �������� ������� ��� �������
   dlMinPrint    =  5;  // ������� �������� ���������� ��� �������
   dlMinGrafic   =  6;  // ������� �������� ����������� ��� �������
   dlMaxSemantic = -1;  // ������������� ��� � ������� �������� ��������� �������
   dlMaxMetric   = -2;  // ������������� ��� � ������� �������� ������� �������
   dlMaxScale    = -3;  // ������������� ��� � ������� �������� ������� ����������� �������
   dlMaxImage    = -4;  // ������������� ��� � ������� �������� ������� ��� �������
   dlMaxPrint    = -5;  // ������������� ��� � ������� �������� ���������� ��� �������
   dlMaxGrafic   = -6;  // ������������� ��� � ������� �������� ����������� ��� �������

// ����� ������� �����
type
 TTypeSelectRect = (slNewRect, slChangeRect);
      // slNewRect    - ������� ������
      // slChangeRect - �������� �� ��� � Rect

type
 // ��� ����������� �����     // 13/09/00
  TViewMapType = (
       VT_NONE,           // 0 - ��� ����������
       // �������� �����
       VT_SCREEN,         // 1 - �������� (����� DIB)
       VT_SCREENCONTOUR,  // 2 - �������� ���������
       // ���������� ��������� �����
       VT_PRINT,          // 3 - ���������� ��������� (����� WIN API)
       VT_PRINTGLASS,     // 4 - ���������� ��� ������� ���������
       VT_PRINTCONTOUR,   // 5 - ���������� ���������, ��� �������� ������
       // ���������� �������������� �����
       VT_PRINTRST,       // 6 - ���������� �������������� (����� WIN API)
       VT_PRINTGLASSRST,  // 7 - ���������� ��� ������� ���������
       VT_PRINTCONTOURRST // 8 - ���������� ���������, ��� �������� ������
                 );

 // ��� ������ �����
  TPrintMapType = (
       // ���������� ��������� �����
       PT_PRINT,          // ���������� ��������� (����� WIN API)
       PT_PRINTGLASS,     // ���������� ��� ������� ���������
       PT_PRINTCONTOUR,   // ���������� ���������, ��� �������� ������
       // ���������� �������������� �����
       PT_PRINTRST,       // ���������� �������������� (����� WIN API)
       PT_PRINTGLASSRST,  // ���������� ��� ������� ���������
       PT_PRINTCONTOURRST // ���������� ���������, ��� �������� ������
                 );
type
 // ���� ���������
  TTYPECOORD  = (Metre,   // � ������
                 Degree,  // � ��������
                 Radian); // � ��������


type
 // ������� ��������� ������
  TRstViewType = 0..5;
  // = 0 - �� �����
  // = 1 - ������ ���������
  // = 2 - ����������
  // = 3 - ��������������
  // = 4 - �������
  // = 5 - ����������
  const                                 // 29/05/2012
    cRstViewTypeText : array[TMAPAPILANGUAGE, TRstViewType] of AnsiString =
    (('None',     'Fill',             'Deep',       'Translucent',    'Middle',  'Transparent'),
     ('�� �����', '������ ���������', '����������', '��������������', '�������', '����������'));

type    
 // �������� ������
  TRstContrast = -16..16;

 // ������� ������
  TRstBright = -16..16;

 // �������������� ������� ������
  TRstGamma = -32..32;

 //��������� �� ������� �������
  PCOLORREF = ^COLORREF;

 //������������ ������ ��������� �������
  TArrayCOLORREF = array of TColorRef;

 //������� ��������� � ����������� �� ��������
  TViewScaleRange = packed record
     TopScale    : integer;
     BottomScale : integer;
     end;

// 14/12/2006
// ���������� ����������� OLE ����������
type
  TDocFileInfo = packed record              // 64 �����
    StructSize : longint;                   // ������ ��������� sizeof(DOCINFOMATION)
    PartCount  : longint;                   // ����� ����������� ����������
    Reserv     : array [1 .. 14] of longint;
  end;
  PTDocFileInfo = ^TDocFileInfo;

// ��������� ����������� OLE ���������
type
  TPartInfo = packed record                  // 256 ����
    StructSize    : integer;                 // ������ ��������� sizeof(DOCINFOMATION)
    PartRect      : TRect;                   // ���������� ������������ ��������� (16)
    Name          : array [1 .. 52] of GtkChar; // ��� ������������ ���������
    Color         : integer;                 // ���� ����
    IsColorActive : byte;                    // ������� �������������� ����� ����  0/1
    Reserve       : array[1 .. 3] of byte;   // ������
    DataType      : longint;                 // ������ ����� � ����������
    View          : byte;                    // ���� ����������� OLE-��������� (0/1)
    SizeMode      : array [1 .. 10] of GtkChar; // ����� ���������� OLE-��������� � ���� OLE-���������
    PlaceType     : byte;                    // ��� �������� ��������� ��������
    FirstPlace    : TDOUBLEPOINT;            // ���������� �������� ������ �������� ����
    SecondPlace   : TDOUBLEPOINT;            // ���������� �������� ������� ������� ����
    ParentId      : longint;                 // ������ ����� � ����������                // 11/05/2011
    MultiPage     : longint;                 // ������ ����� � ����������                 // 11/05/2011
    TopScale      : longint;                 // ����������� �������� ������� ������� ���������  // 10/04/2012
    BottomScale   : longint;                 // ����������� �������� ������ ������� ���������   // 10/04/2012
    SchemeFlag    : longint;                 // ��� ����������� ���������    // 26/09/12 *** KIA 06.04.17
    Reserv        : array[1 .. 108] of GtkChar;                                                 // 10/04/2012
  end;
  PTPartInfo = ^TPartInfo;


// ��������� ����������� OLE ���������     // 11/05/2011

const
  DISIZEMODE_diClip     = 1;           // 18/01/2017
  DISIZEMODE_diCenter   = 2;
  DISIZEMODE_diScale    = 3;
  DISIZEMODE_diStretch  = 4;
  DISIZEMODE_diAutoSize = 5;


type
  TDOCINFOMATION = packed record  // 636 bytes          // 18/01/2017
  StructSize   :Integer;     // ������ ��������� sizeof(DOCINFOMATION)
  PlaceType    :Integer;      // ��� �������� ��������� �������� (PP_GEOWGS84, PP_PLANE)
  FirstPlace   :TDOUBLEPOINT;     // ���������� �������� ������ �������� ����
  SecondPlace  :TDOUBLEPOINT;    // ���������� �������� ������� ������� ����
  Name         :array [1 .. 256] of WCHAR;      // ��� ������������ ���������

  Color        :TCOLORREF;          // ���� ����
  IsColorActive,  // ������� �������������� ����� ����  0/1
  DataType,       // ������ ����� � ���������� (0 - OLE, FILE_EMF - EMF, ...)
  View,           // ���� ����������� OLE-��������� (0/1)

  SizeMode,       // ����� ���������� OLE-��������� � ���� OLE-��������� (DISIZEMODE)
  DocIdent,       // ������������� ���������������� ��������� (� ���������������� ��������� ��� ����� � ����� ���������������)
  MultiPage,      // ������� �������� �������� ���������������� ��������� (� �������� - 0)
  TopScale,       // ����������� �������� ������� ������� ���������

  BottomScale,    // ����������� �������� ������ ������� ���������
  SchemeFlag   :Integer;     // ������� ������������ ����������� ��������� (����� � ���������)
  Date         :Cardinal;           // ���� ���������� ��������� ��������� (YYYYMMDD)
  Time         :Cardinal;           // ����� ���������� ��������� ��������� (����� ������� �� 00:00)

  Reserve:array [1 .. 9] of Integer;     // ������
  end;
  PTDOCINFOMATION = ^TDOCINFOMATION;


// ���������� ���� ����������� OLE ����������
type
  TBaseSchemeInfo = packed record
     StructSize    : longint;            // ������ ��������� sizeof(SCHEMEINFOMATION)
     CountScheme   : longint;            // ���������� ���� ����������� ���������� ����������
     CurrentScheme : longint;            // ������� ����� ����������� ���������� ����������(������� � 1)
     Reserv        : array[1 .. 13] of longint;
  end;
  PTBaseSchemeInfo = ^TBaseSchemeInfo;

// ����������� ���� OLE ����������
type
  TSchemeInfo = packed record
     StructSize   : longint;                 // ������ ��������� sizeof(SCHEMEINFOMATION)
     NumberScheme : longint;                 // ���������� ����� ����� ����������� ���������� ����������
     NameScheme   : array[1 .. 64]  of GtkChar; // ��� ����� ����������� ���������� ����������
     IndexView    : array[1 .. 128] of GtkChar; // ������ ������ ����������� ���������� ����������(0/1)
     Reserv       : array[1 .. 56]  of GtkChar; // ������
  end;
  PTSchemeInfo = ^TSchemeInfo;

// ��� �������� MapTop, MapLeft      // 06/02/2007
type
  TMAPPOS = packed record
    MapTop  : integer;
    MapLeft : integer;
  end;

const
// --------------------------------------------------------------
// ��������� �� ��� ����, ����� 200..., ��� �������
// --------------------------------------------------------------
  MSG_AW_OPENDOCUN        = $623; //  ������� �����     05/12/2014
  MSG_AW_OPENDOC          = $655; //  ������� �����
  MSG_AW_GETCURRENTDOC    = $673; //  ������� ������������� �������� ������
  MSG_MT_CHANGEDATA       = $65D; // ��������� ������
  MSG_AW_OPENATLAS        = $0680+373; //  ������� �����     // 25/05/2011


// --------------------------------------------------------------

// --------------------------------------------------------------
//  ��������� ��� �������������� � ���-��������
// --------------------------------------------------------------
const
 HOSTALIAS      = 'HOST#';
 HOSTSIZE       = 5;

 MAPALIAS       = 'ALIAS#';
 ALIASSIZE      = 6;

type
  TMCUSERPARM = packed record // ��������� ������������
     Name         : array[0 .. 31]  of GtkChar; // ��� (��������� ANSI)
     Password     : array[0 .. 63]  of GtkChar; // ������
   end;
   PMCUSERPARM = ^TMCUSERPARM;


  PMCMAPLISTITEM  = ^TMCMAPLISTITEM;
  TMCMAPLISTITEM = packed record // �������� ������ �������
     Level        : Cardinal;     // ������� ����������� �������� � 1 �� MaxLevel
     Flags        : Cardinal;     // ���� �������������� 0 - ��� ���� �������������
     TypeLevel    : Cardinal;     // ��� ����� MAPFILES, ���� ��� 0 - �����������
     Reserve      : Cardinal;     //
     Name         : array[0 .. 255]  of GtkChar; // ��� �������� (�����)
   end;

  TMCMAPLIST  = packed record  // ������ ��������� ����
     //TMCMAPLIST() { Init(); }
     //void Init() {  Ident = 0x7F7F7F7F; Length = sizeof(TMCMAPLIST);
     //            Count = 0; MaxLevel = 1; }

     /// !!! ��� ������������� ���������� ��������� ��������:
     // Ident = 0x7F7F7F7F;
     // Length = sizeof(TMCMAPLIST);
     // Count = 0;
     // MaxLevel = 1;

     Ident        : longint;     // �������������� ������ 0x7F7F7F7F
     Length       : longint;     // ����� ����� ������
     Count        : longint;     // ����� ��������� � ������
     MaxLevel     : longint;     // ������������ ������� ����������� ��������� (������)
     Item         : array [0..1] of TMCMAPLISTITEM; // ������ ��������� ������ ��������� ���� (� ���������� Count) // 20/06/2011
   end;
  PTMCMAPLIST = ^TMCMAPLIST;


type
  TMCDATALISTITEM = packed record   // 29/10/20
  Ident     : Cardinal;              // 0x7FFF7FFF
  Length    : Cardinal;              // ������ ������ ������
  Level     : Cardinal;              // ������� ����������� �������� � 1 �� MaxLevel
  Flags     : Cardinal;              // ���� ��������������, 0 - ��� ���� �������������
  TypeLevel : Cardinal;              // ��� ����� MAPFILES ��� -1 - ����� ��� 0 - �����������
  Size      : Cardinal;              // ������ ������ �����
  Date      : Cardinal;              // ���� ���������� � ������� YYYYMMDD
  Time      : Cardinal;              // ����� ���������� - ����� ������ �� 00:00:00
  Name      : array [0..1] of WCHAR; // ��� �������� (����� ��� �����) ���������� �����
end;
  PTMCDATALISTITEM  = ^TMCDATALISTITEM;


// ������ ������ � �����
TMCDATALIST = packed record     // 29/10/2014
  Ident    : longint;                            // ������������� ������ 0x7F7F7F7F
  Length   : longint;                            // ����� ����� ������
  Count    : longint;                            // ����� ��������� � ������
  MaxLevel : longint;                            // ������������ ������� ����������� ��������� (������)
  Item     : array [0..1] of TMCDATALISTITEM;    // ������ ��������� ������ ��������� ���� (� ���������� Count)
end;
PTMCDATALIST = ^TMCDATALIST;



// ��������� ������ � ��������� ����������� � ��� ������
TGSMONITOR  = packed record

  Ident         : longint;          // 0x7F7F7F7F                           // 26/03/12
  Length        : longint;         // ����� ����� ������ � ������
  Version       : longint;        // ������ ��� ������� 0x0T112233 11.22.33 (T=1,x64)
  Count         : longint;          // ����� ������������ ��������
  State         : longint;          // ��������� ������������ ��������
  BeginTime : array[0..31] of GtkChar; // ����� ������ ��� �������
  ParmName  : array[0..255] of GtkChar;// ��� ����� ����������
  MapEdit       : longint;        // ����� ����� ����, �������� �� ��������������
  MapRead: longint;        // ����� ����� ����, �������� �� ������
  MapCopy: longint;        // ����� ����� ����, ����������� � �����������
  RswEdit: longint;        // ����� ����� �������, �������� �� ��������������
  RswRead: longint;        // ����� ����� �������, �������� �� ������
  RswCopy: longint;        // ����� ����� �������, ����������� � �����������
  MtwEdit: longint;        // ����� ����� ������, �������� �� ��������������
  MtwRead: longint;        // ����� ����� ������, �������� �� ������
  MtwCopy: longint;        // ����� ����� ������, ����������� � �����������
  MapNotPrint: longint;    // ����� ����� ����, �� ����������� � ������
  RswNotPrint: longint;    // ����� ����� �������, �� ����������� � ������
  MtwNotPrint: longint;    // ����� ����� ������, �� ����������� � ������        // 27/09/12

//���� ����� ������������� > 32 - ���������� GSMUSERSHORT
//GSMUSEREX    User[1];    // ������ ������ � ����������� �������������
//GSMUSERSHORT  User[1];   // ������ ������ � ����������� �������������
 end;
  PGSMONITOR = ^TGSMONITOR;

Const
   iINP_TOTAL   = 0;  // ���������� ��� ������ � �������� ����
   iINP_FRAME   = 1;  // ���������� ��������� �������� � �������� ��������

type
 TINSETDESC = packed record   // �������� �������� ������ ������
{
       /// !!! ��� ������������� ���������� ��������� ��������:
    Ident       = 0;
    Path[0]     = 0;
    Name[0]     = 0;
    Scale       = 100000;
    TopScale    = 40000000;
    BottomScale = 1;
    View        = 1;
    BackColor   = 0x07F7F7F;
    BorderColor = 0;
    Transparence= 0;
    BorderThick = 260;
    BorderView  = 1;
    PlaceFlag   = INP_TOTAL;
    FirstPlace  = 0;
    SecondPlace = 0;
    PlaceType   = PP_PLANE;
    Scheme      = 0; 
  }

  Length: Integer;       // ������ ������ ���������
  Ident: Integer ;       // ������������� ��������
  Path: array [0..1023] of WCHAR ;   // ������ ���� � ����� ������
  Name: array [0..127] of WCHAR ;    // �������� �������� ������ (������������� �� ������)

  // ���������� � ������� ��������� ��������� (�����)
  FirstPlace:TDOUBLEPOINT ;   // ���������� �������� ������ �������� ����
  SecondPlace:TDOUBLEPOINT ;  // ���������� �������� ������� ������� ����
  // ���������� � ������� ������ ������ ��� INP_FRAME (�����)
  FramePlace:TDOUBLEPOINT ;   // ���������� ����� �������� (����� ��������� � ������� ������)
  PlaceFlag:Integer;    // ������ �������� (INP_TOTAL, INP_FRAME)
  PlaceType:Integer ;    // ������� ��������� ��������
  // (PP_PLANE - �������������, PP_GEO - �������������� WGS84)

  Scale:Integer ;      // ������� ����������� (������������� �� ������ ��� INP_FRAME)
  TopScale:Integer ;     // ����������� �������� ������� ������� ���������
  BottomScale: Integer ;  // ����������� �������� ������ ������� ���������
  View: Integer;         // ������� ��������� ������

  Scheme: Integer ;       // ������� ����������� ������ � ���� �����
  BackColor:Integer ;    // ���� ���� ���� ������
  Transparence:Integer ; // ������� ������������ ����
  BorderColor:Integer;  // ���� ����� ����
  BorderThick:Integer ;  // ������� �����
  BorderView:Integer ;   // ������� ����������� �����
end;
  PINSETDESC = ^TINSETDESC;



// ----------------------------------------------------------------
// ������� ��������� ������
// ----------------------------------------------------------------
type
{$IFDEF CPUX64}                                           // 11/12/2013
 TEVENTSTATE  = function(Parm : Pointer; percent : integer): integer; stdcall; // 17/06/2016
 TBreakCall   = function(Parm : Pointer): integer; stdcall;
 TBreakCallEx = function(Parm : Pointer; isrepaint : longint): integer; stdcall;
 TBeforePaint = function(Parm : Pointer; DC :HDC; varRect : TRect): integer; stdcall;
 TMaskCall    = function(Parm : Pointer): integer; stdcall;
 TEVENTCALL   = function(Parm : Pointer; value1, value2, value3 : HPARAM) : integer; stdcall;
 TMESSAGEBOXCALL= function(Parm : Pointer; awnd : HWND; const amessage, title: PWCHAR; flag : integer) : integer; stdcall;  // 17/06/2016
{$ELSE}
 TEVENTSTATE  = function(Parm : integer; percent : integer): integer; stdcall; // 17/06/2016
 TBreakCall   = function(Parm : integer): integer; stdcall; // 21/06/2013
 TBreakCallEx = function(Parm : integer; isrepaint : longint): integer; stdcall;  // 21/06/2013
 TBeforePaint = function(Parm : integer; DC :HDC; varRect : TRect): integer; stdcall;  // 21/06/2013
 TMaskCall    = function(Parm : integer): integer; stdcall;  // 21/06/2013
 TEVENTCALL   = function(Parm : integer; value1, value2, value3 : HPARAM) : integer; stdcall;  // 21/06/2013
 TMESSAGEBOXCALL= function(Parm : integer; awnd : HWND; const amessage, title: PWCHAR; flag : integer) : integer; stdcall;  // 17/06/2016
{$ENDIF}



 type
//  HINSTANCEMTYPE  = cardinal; //01/10/2009
{$IFDEF CPUX64}                                           // 11/12/2013
  HFORMULA   = int64;
  HImage     = int64;
  HMrt3d     = int64;
  HObjSet    = int64;
  HDraw      = int64;
  TDRAWPOINT = Types.TPoint;                              // 11/12/2013
{$ELSE}
  HFORMULA   = cardinal;
  HImage     = cardinal;
  HMrt3d     = cardinal;
  HObjSet    = cardinal;
  HDraw      = Cardinal;
  TDRAWPOINT = Types.TPoint;
{$ENDIF}
// ���� �������� ��� ���������� ������ ����� ��������� ����:
{ const
   SEMNETRIB       = 32812;  // ����� �������-����� (��� ����)
   SEMNETKNOTFIRST = 32813;  // ����� �������-���� ������ ����� (��� �����)
   SEMNETKNOTLAST  = 32814;  // ����� �������-���� ��������� ����� (��� �����)
   SEMNETNUMBER    = 32815;  // ����� ����
   SEMNETTOOBJECT  = 32816;  // ������ �� ������ �����
   SEMNETVDIRECT   = 32817;  // �������� ������ (��� �����)
   SEMNETVUNDIRECT = 32818;  // �������� �������� (��� �����)
   SEMNETCOSTRIB   = 32819;  // ��������� �����
   SEMNETRANKRIB   = 32820;  // ���� �����}
// ���� �������� ��� ���������� � ������ � �����
// 5557   // ����� ���� ������������
// 5558   // ���� ����
// 5559   // �������
// 5560   // ��������� �����
// 5561   // ����� ������� �����
// 5562   // ����� ���� �������������
type
 TDFRAME  = packed record
   X1,Y1,
   X2,Y2 : double;
 end;
  PDFRAME = ^TDFRAME;  // 07/10/2009

//------------------------------------------------------------------
//  ��������� �������� ������� ����� (���������� ���������)
//------------------------------------------------------------------
type
 TMTLBUILDPARM = packed record  // ������ ��������� 128 ����
  StructSize : cardinal; // ������ ������ ��������� sizeof (MTRLAYERBUILDPARM)
  BeginX : double; // ������������� ���������� ������
  BeginY: double; // (���-��������� ����) ������� � ������
  EndX        : double; // ������������� ���������� ���������
  EndY        : double; // (������ - ���������� ����) ������� � ������
  ElemSizeMeters : double; // ������ ������� ������������� ������� � ������ �� ���������
  layerCount : integer; // ���������� �����
  layerForm : integer; // ����� ������������� ���-��� � �����
                 // ���������� �������� :
                 // 0 - �������� �����
                 // 1 - ��������� ������
  HeightSizeBytes : integer; // ������ ������ ������� � ������
                 // ���������� �������� :
                 // 0 - ������ �����������;
                 // 2 - ������ "short int"
                 // 4 - ������ "long int"
  layerSizeBytes : integer; // ������ �������� ���� � ������
                 // ���������� �������� :
                 // 1 - �������� "unsigned char"
                 // 2 - �������� "unsigned short int"
                 // 4 - �������� "unsigned long int"
  HeightMeasure  : integer; // E������ ��������� ������
                                // ���������� �������� :
                 // 0 - �����
                 // 1 - ���������
                 // 2 - ����������
                 // 3 - ����������
  layerMeasure : integer; // E������ ��������� �������� ����
                 // ���������� ��������
                 // - �� ��;��� ��� HeightMeasure
  UserType : integer; // ������������ �����; �����������
                 // � ����������� ��������
  Scale : integer; // �������
  Reserve : array[0..51] of GTKchar; // ������ ���� ����
end;


TBUILDMTL = packed record      // ������ ��������� 160 ����
 StructSize : cardinal; // ������ ������ ��������� sizeof (BUILDMTL)
 sFree         : integer; // ������ ���� ����
 BeginX : double; // ������������� ���������� ������
 BeginY        : double; // (���-��������� ����) ������� � ������
 EndX        : double; // ������������� ���������� ���������
 EndY        : double; // (������ - ���������� ����) ������� � ������
 ElemSizeMeters : double; // ������ ������� ������������� ������� � ������ �� ���������
 layerCount : integer; // ���������� �����
 layerForm : integer; // ����� ������������� ���-��� � �����
                 // ���������� �������� :
                 // 0 - �������� �����
                 // 1 - ��������� ������
 HeightSizeBytes : integer; // ������ ������ ������� � ������
                 // ���������� �������� :
                 // 0 - ������ �����������;
                 // 2 - ������ "short int"
                 // 4 - ������ "long int"
 layerSizeBytes: integer; // ������ �������� ���� � ������
                 // ���������� �������� :
                 // 1 - �������� "unsigned char"
                 // 2 - �������� "unsigned short int"
                 // 4 - �������� "unsigned long int"
 HeightMeasure : integer; // E������ ��������� ������
                                // ���������� �������� :
                 // 0 - �����
                 // 1 - ���������
                 // 2 - ����������
                 // 3 - ����������
 layerMeasure : integer; // E������ ��������� �������� ����
                 // ���������� ��������
                 // - �� ��;��� ��� HeightMeasure
 UserType : integer; // ������������ �����; �����������
                 // � ����������� ��������
 Scale         : integer; // �������
 BlockSide : integer; // ������ ������� ����� ������� � ���������
                 // ���������� �������� : 0 - 64
                 // (���� ����� 0; ������������ �������� 64)
 codeCount : integer; // ���������� ����� ����� 10/06/08
 MtdPointFormat: integer;  // ������ ����� MTD-������ :                         // 11/12/2013
                           //   0 - ��������� ����� MTDXYHC (16 ����)
                           //   1 - ��������� ����� MTDXYHCLAS (24 �����)
 Reserve : array[0..67] of GTKchar; // ������ ���� ����
End;
PBUILDMTL = ^TBUILDMTL;



//------------------------------------------------------------------
//  �������� �������� ������� ����� (������ ��������� 1024 �����)
//------------------------------------------------------------------
TMTLDESCRIBE  = packed record
 Name : array[0..259] of GTKchar;             // ��� ����� �������
 MaterialFileName: array[0..259] of GTKchar;  // ��� ����� �������� ����������
 LayerCount: integer;                         // ���-�� �����
 MaterialCount: integer;                      // ���-�� ����������
 ElementInPlane: double;                      // ������ �������� � ������ �� ���������
 FrameMeters: TDFRAME;                        // �������� ������� � ����� (�����)
 MinHeightValue: double;                      // ����������� �������� ������ �������� � ������� (�����)
 MaxHeightValue: double;                      // ������������ �������� ������ �������� � ������� (�����)
 BotLevelHeight: double;                      // ������ �������� ������� ������� ������ (�����) // 24/10/00
 UserType: integer;                           // ��� ���������������� ������
 View        : integer;                       // ������� ��������������
 UserLabel: integer;                          // ���������������� �������������
 ReliefPresence: integer;                     // ������� ������� ������� 17/01/01
 MaxSummaryPower: double;                     // ������������ ��������� �������� ����� (�����) // 17/05/01
 Reserve : array[0..407] of GTKchar;          // ������
end;
PMTLDESCRIBE = ^TMTLDESCRIBE; // 02/10/2009


//------------------------------------------------------------------
//  �������� �������� ������� �����                03/04/14   // 23/06/2016
//------------------------------------------------------------------
TMTLDESCRIBEUN = packed record
 Name : array[0..MAX_PATH_LONG-1] of WCHAR; // ��� ����� �������
 MaterialFileName: array[0..MAX_PATH_LONG-1] of WCHAR;// ��� ����� �������� ����������
 FrameMeters: TDFRAME;    // �������� ������� � ����� (�����)

 ElementInPlane: double;   // ������ �������� � ������ �� ���������

 MinHeightValue: double; // ����������� �������� ������ �������� � ������� (�����)
 MaxHeightValue: double; // ������������ �������� ������ �������� � ������� (�����)

 BotLevelHeight: double; // ������ �������� ������� ������� ������ (�����) 

 MaxSummaryPower: double; // ������������ ��������� �������� ����� (�����) 

 LayerCount: integer; // ���-�� �����
 MaterialCount: integer; // ���-�� ����������

 UserType: integer; // ��� ���������������� ������
 View        : integer; // ������� ��������������

 UserLabel: integer; // ���������������� �������������
 ReliefPresence: integer; // ������� ������� �������

 Reserve : array[0..63] of GTKchar; // ������ 
end;
PMTLDESCRIBEUN = ^TMTLDESCRIBEUN; 


 type
  PCrossPoint = ^TCrossPoint;
  TCrossPoint = packed record
    XY       : TDoublePoint; // ���������� ����� �����������
    H        : double;       // ������ ����� �����������  (�� �������)
    Obj1     : HObj;
    Number1  : integer;      // �� ����� ������ ������� Info1 ��������� ����� �����������
    Subject1 : integer;      // ����� �������/���������� ������� info1
    Obj2     : HObj;
    Number2  : integer;      // �� ����� ������ ������� Info2 ��������� ����� �����������
    Subject2 : integer;      // ����� �������/���������� ������� info2
  end;

  TNumberPoint = packed record
    Point   : TDoublePoint;   // ���������� �����
    Number  : integer;     // ����� ����� �������, �� ������� ������� point
    Update  : integer;     // ����������� ������ ����� �������
    Equal   : integer;      // ����� ����� ���������� � �������
    Reserve : integer;
  end;
  PNumberPoint = ^TNumberPoint; // 06/10/2009
 const
  cDELTANULL =  1e-3;
  cDOUBLENULL=  1e-6; 

//-----------------------------------------------------------------
// ��������� ������������ ���������� ����������� ����� ��� ������
// (��� ������ ���������� ������������ ���������� ����������� �
//  ���������� ����� ������ �������� � �������� ������� RGB � CMYK)
//-----------------------------------------------------------------

Type
 HMessage = HWnd;
 MSGHANDLER = HMESSAGE;  // 02/10/2009

TSAVEASPICTRPARM_FOR_PRINT = packed record
 Length : integer;       // ����� ��������� SAVEASPICTRPARM_FOR_PRINT
 Handle : HMessage;      // ������������� ���� ������� (������������ ���
                         // ����������� ������������� ��������)

 Map    : HMap;          // ������������� �����
 Regime : integer;       // ����� ������ ������������� �������
                         // (AREA_ALL; AREA_SELECT_FRAME; ... - ��. maptype.h)

 PlaneFrame : TDFRAME;   // �������� ��������� ������� � ������

 DPI        : double;    // ���������� ����������� (����� �� ����)
 LPI        : double;    // ����� ������� �� ���� (��� Method = 0 ������������).
                         // ����� ������� �� ����� ����� Shape

 FactorVer  : double;    // ������������ ���������� ����������� ��
 FactorHor  : double;    // ������������ � �������������� ����
 Rect       : TRect;     // ������� ����������� ������� ����� � ��������
                          // (��� �������� � RGB �� �����������)     //24/04/08
 Scale      : integer;    // ������� (�����������)

 BitCount: integer;  // ���������� ��� �� ������ ���������� �����������
                  // ��� ColorModel == 0:             // 20/04/08
                  //  8;16;24;32 ��� �� ������ (RGB)
                  // ��� ColorModel == 1:
                  //  1;8 ��� �� ������ (CMYK) - ������ ����� BMP ��� PCX
                  //  32 ��� �� ������ (CMYK) - ���� ���� TIF
 Intensity: integer; // ������������� ������� ��������� �������� (0-100)
 ColorModel: integer; // ���� �������� ������:
                  //  0 - RGB (����������� ���� ����)
                  //  1 - CMYK BMP ��� CMYK PCX (������ 8-������
                  //    ��� 1-������ �����; ������������ ����������
                  //    �����������; � ����������� � ��������
                  //    ����� ��������� "-C";"-M";"-Y";"-K");
                  //    ��� CMYK TIF (���� ����)

 Method: integer;           // ����� ��������������� 8-������ �����������
                  // ����������� CMYK � 1-������:
                  //  0 - �� ��������� (��������� 8-������ �����������);
                  //  1 - ���������� ���������� ������� ����� Shape
                  //    � ����� DPI/LPI
 Shape: integer;           // ����� ����� (���������) ���������� �������
                  // (��� Method = 0 ������������):
                  //  0 - ���������� ���������� �������� �������
                  // ���� ������� ����� ������� (��� Method = 0 ������������)
 AngleC : double;  // 15 ��������
 AngleM : double;  // 75 ��������
 AngleY : double;  //  0 ��������
 AngleK : double;  // 45 ��������

 _Reserve: array[0..95] of GTKchar;  // ������ (���������� �� 512 ����)

 Cross        : byte;   // ���� ��������� ����������� ������� (��� ColorModel > 0)
          // (1 - ����; 0 - ���); (���� �� �����������)
 Negative: byte;     // ���� ���������� ����������� ����������� (��� BitCount = 1)
          // (1 - ����; 0 - ���); (���� �� �����������)
 Mirror : byte;      // ���� ���������� ����������� ����������� (��� BitCount = 1)
          // (1 - ����; 0 - ���); (���� �� �����������)
 Turn        : byte;       // ���� �������� ����������� �� 90 �������� (��� BitCount = 1)
          // (1 - ����; 0 - ���); (���� �� �����������)

 FileName       : array[0..259] of GTKchar;  // ������� ��� ������������ ����� (BMP; PCX; TIF)
           // ��� �������� 1.BMP (��� BitCount = 1) �����������
           // ������ �����: 1-�.BMP; 1-M.BMP; 1-Y.BMP; 1-K.BMP

           // ��� �������� 1.PCX (��� BitCount = 1) �����������
           // ������ �����: 1-�.PCX; 1-M.PCX; 1-Y.PCX; 1-K.PCX
           // � ��������� ������� ����������� ���� ����
end;
//TSAVEASPICTRPARM = TSAVEASPICTRPARM_FOR_PRINT; // 23/11/2010

// ------------------------------------------
 TMAPREGISTER   = packed record
   Length    : cardinal;      // ������ ������ ���������
   mName     : array[0..31] of GTKChar;    // ��� ������
   Scale,                     // ����������� ��������
   EPSGCode,                  // ��� EPSG ������� ���������     // 12/06/15  // 21/06/2016
   EllipsoideKind,            // ��� ����������
   HeightSystem,              // ������� �����
   MaterialProjection,        // �������� ���. ���������
   CoordinateSystem,          // ������� ���������
   PlaneUnit,                 // ������� ��������� � �����
   HeightUnit,                // ������� ��������� �� ������
   FrameKind,                 // ��� �����
   MapType,                   // ���������� ��� �����
   DeviceCapability,          // ����������� ����������� �������
   DataProjection,            // ������� ������ � ��������
   OrderViewSheetFlag,        // ������� ����������� ������
   FlagRealPlace,             // ������� �������� ���������
   ZoneNumber   : cardinal;   // ����������� �������� ��� �������
                              // ��������� - ����� ���� ��������� � ��������
   FirstMainParallel,         // ������ ������� ���������
   SecondMainParallel,        // ������ ������� ���������
   AxisMeridian,              // ������ ��������
   MainPointParallel: double; // ��������� ������� �����
  end;
   PMAPREGISTER = ^TMAPREGISTER;  // 07/10/2009
    
  TMAPREGISTEREX  = packed record
   Length    : integer;      // ������ ������ ���������  //06/11/2010
   Name      : array[0..31] of GTKChar;    // ��� ������
   Scale,                     // ����������� ��������
   EPSGCode,                  // ��� EPSG ������� ���������     // 12/06/15  // 21/06/2016
   EllipsoideKind,            // ��� ����������
   HeightSystem,              // ������� �����
   MaterialProjection,        // �������� ���. ���������
   CoordinateSystem,          // ������� ���������
   PlaneUnit,                 // ������� ��������� � �����
   HeightUnit,                // ������� ��������� �� ������
   FrameKind,                 // ��� �����
   MapType,                   // ���������� ��� �����
   DeviceCapability,          // ����������� ����������� �������
                                      // ������ ����� 20 000
                                     // ��� ���� ���������� ��������:  // 26/12/06
                                     // -1 - ������������ ��������
                                     // -2 - ������� ���������� � �����������
                                     // -3 - ������� ���������� � �����������
                                     // -7 - ������� ���������� � ��������     // 17/03/10
   DataProjection,            // ������� ������ � �������� (0/1)
   OrderViewSheetFlag,        // ZoneIdent; // ������������� ������ (��� ��� 63: A-X ��� 0)      // 11/12/2013
   FlagRealPlace,              // ��� �����: 0:MAP (������������� � �������),
                               // 1:SIT (������������ ����), 2:SITX (���� ����), -1:SIT c ������, -2:SITX � ������
   ZoneNumber   : integer;    // ����������� �������� ��� �������   //06/11/2010
                              // ��������� - ����� ���� ��������� � ��������
   FirstMainParallel,         // ������ ������� ���������  StandardParallel1
   SecondMainParallel,        // ������ ������� ���������  StandardParallel2
   AxisMeridian,              // ������ ��������   (������� ������ ��������) CentralMeridian
   MainPointParallel,         // ��������� ������� ����� (������ ������ ��������) LatitudeOfOrigin
   PoleLatitude,              // ������ ������ ��������  (Latitude of false origin, etc)
   PoleLongitude,             // ������� ������ �������� (Longitude of false origin, etc)
   FalseEasting,              // �������� ��������� �� ��� Y 06/11/2010
   FalseNorthing,             // �������� ��������� �� ��� X
   ScaleFactor,               // ���������� ����������� �� ������ ��������� (1.0 +\- ...)
   TurnAngle:  double;        // ���� ��������� ���� ��� ��������� ������ (���)
   Reserv2 : array[0..3] of double; // ������ = 0
  end;
   PMAPREGISTEREX = ^TMAPREGISTEREX; // 07/10/2009



 TLISTREGISTER = packed record
    Length       : cardinal;      // ������ ������ ���������
    Nomenclature : array[0..31] of GTKChar;      // ������������ �����
    ListName     : array[0..31] of GTKChar;      // �������� �����
    FileName     : array[0..MAX_PATH-1] of GTKChar;     // ��� ����� �� �������� � ������ �����
                                              // ������ ���� � ������ ������
                                              // � ������������ HDR, DAT, SEM, DRW
// ������������� ���������� ����� � ������
    XSouthWest,                    // X �-�
    YSouthWest,                    // Y �-�
    XNorthWest,                    // X �-�
    YNorthWest,                    // Y �-�
    XNorthEast,                    // X �-�
    YNorthEast,                    // Y �-�
    XSouthEast,                    // X �-�
    YSouthEast,                    // Y �-�
// ������������� ���������� ����� � ��������
    BSouthWestCoordinate,          // B �-�
    LSouthWestCoordinate,          // L �-�
    BNorthWestCoordinate,          // B �-�
    LNorthWestCoordinate,          // L �-�
    BNorthEastCoordinate,          // B �-�
    LNorthEastCoordinate,          // L �-�
    BSouthEastCoordinate,          // B �-�
    LSouthEastCoordinate : double; // L �-�
    MaterialKind,                  // ��� ��������� ���������
    MaterialType,                  // ��� ���
    ReliefHeight : cardinal;       // ������ ������� �������
    sDate  : array[0..11] of GTKchar;  // ���� ������ "��������"
    MagneticAngle,                 // ��������� ���������
    YearMagneticAngle,             // ������� ��������� ���������
    MeridianAngle :  double;       // ������� ��������� ����������
    DateAngle   : array[0..11] of GTKchar; // ���� ��������� "��������" 06/11/2010
    Reserve     : array [0..27] of GTKCHAR; // = 0
    //Reserv2     :array[0..2]of cardinal;      // = 0
    //BorderSW    : TDOUBLEPOINT;        // ��������� ���� (������ 0 !)
  end;
    PLISTREGISTER = ^TLISTREGISTER;  // 07/10/2009

  //----------------------------------------------------------------------------
  TMTRCOLORDESCEX = packed record  // �������� ��������� ����� �������
    MinHeight  : double;           // ����������� ������ ��������
    MaxHeight  : double;           // ������������ ������ ��������
    Color      : TColorRef;        // ����,��������������� ��������� �����
  end;
  PMTRCOLORDESCEX = ^TMTRCOLORDESCEX; // 02/10/2009

  TXIMAGEDESC    = packed record    // ��������� ������� ������� XImage
                                    // (��� ���������� � XWindow)    // 25/09/00
    Point       : Pointer;          // ����� ������ ������� ��������
    Width       : integer;          // ������ ������ � ��������
    Height      : integer;          // ����� �����
    Depth       : integer;          // ������ �������� � ����� (8,15,16,24,32)
    CellSize    : integer;          // ������ ��������(�������) � ������
    RowSize     : integer;          // ������ ������ � ������
  end;
   PXIMAGEDESC = ^TXIMAGEDESC;    // 16/06/2016
  

 // ��������� ��� �������� ��������������
  TRSCCREATE   = packed record
    rscName  : array[0..31] of GTKChar;          // �������� �������������� (�� ����� 19 ��������)
    rscType  : array[0..31] of GTKChar;          // ��� �������������� (�� ����� 25 ��������)
    rscCode  : array[0..7] of GTKchar;   // ��� �������������� (�� ����� 4 ��������)
    Scale    : Cardinal;              // ����������� ��������
    Language : Cardinal;              // ���� (MAPAPILANGUAGE)
  end;
  PRSCCREATE = ^TRSCCREATE;

  TRSCSEGMENT  = packed record
    Order    : Cardinal;              // ������� ������ ���� �� ������
    rscSegName  :array[0..31] of GTKChar;        // �������� ����(�� ����� 30 ��������)
  end;
  PRSCSEGMENT = ^TRSCSEGMENT; // 05/10/2009

  // ��������� �������� ���������,��������������� ������������ ����� ��������
 TSERIALIMIT = packed record
  Code     : integer;                   // ��� ���������
  Count    : integer;                   // ���������� ������������� (>=2)
  Value    : array [0..255] of double;  // �������� �������������
 end;
 PSERIALIMIT = ^TSERIALIMIT; // 02/10/2009

 // ���������� ������ � �����
 TSERIATYPE  = packed record
    Excode      : cardinal;     // ����������������� ��� ��'����� �����
    Local       ,               // �������� �����������  ��'����� �����
    Count       ,               // ���������� �������� �����
    FirstCode   ,               // ��� ������ ���������
    FirstCount  ,               // ���������� ������������� �� 1 ���������
    SecondCode  ,               // ��� ������ ���������
    SecondCount :  integer;     // ���������� ������������� �� 2 ���������
{$IFDEF CPUX64}
    SeriaTypeZero:  integer;    // ������������                                // 13/12/2013
{$ENDIF}
 end;

 // ����� ������� � �����
 TSERIAPLACE = packed record
   FirstCode   ,                // ��� ���������
   FirstNumber ,                // ����� ������������ (� 1)
   SecondCode  ,                // ��� ��������� ,���� ����� �� ����� ���������,�� ����� ����
   SecondNumber : integer;      // ����� ������������ (� 1)
 end;
 PSERIAPLACE = ^TSERIAPLACE; // 02/10/2009

 TElemTree = packed record
   Ident : integer;           // ������������� ������� (��� 0, � ����)
   Depth : integer;           // ������� ����������� � ������
   LayerNumber : integer;     // ����� ����
   ParentNumber : integer;    // ����� �������� � ����� ������� ���������
   PictureIndex : integer;    // ������ � ������� �����������
   Code : array [0..27] of GTKchar;   // ��������������� ������������� ����(��� �����������)
                             // � ��������� �������������� � ����� � �����������
  end;
 PElemTree = ^TElemTree; // 02/10/2009

 TDRAWSIZE = Types.TSize;  // ������ TSize

 TDRAWLINE = packed record     // ���������� �����
  First  : TPoint;
  Second : TPoint;
 end;

 PDRAWBOX = ^TDRAWBOX;        // ���������� ���������� ��������������
 TDRAWBOX = packed record     // 20130206 Korolev
  Point1 : TPoint;
  Point2 : TPoint;
  Point3 : TPoint;
  Point4 : TPoint;
 end;

  HVIEW = integer;


  //------------------------------------
  TPaletteElement = packed record
   case byte of
    0: (R,G,B,Alpha : byte);
    1: (Value : Cardinal);
    2: (Color : TColor);
  end;

  TPALETTE256  = packed record
   palVersion    : word;
   palNumEntries : word;
   palPalEntry   : array[0..255] of TPaletteElement;
  end;
  PPALETTE256 = ^TPALETTE256;

  TIMAGEFRAME    = packed record
   LeftTop      : TDoublePoint;      // ���������� ������ ����� � ���
   RightTop     : TDoublePoint;      // ���������� ������ ����� � ���
   RightBottom  : TDoublePoint;      // ���������� ������� ����� � ���
   LeftBottom   : TDoublePoint;      // ���������� ��������� ����� � ���
  end;

  // �������� ������� �� ���� �� �������� ��������� ("����� ��'����")
  TRSCRELATION  = packed record                         // 01/11/01
    ObjectCode   : Cardinal;            // ���������� ��� ������� ���������
    SemanticCode : Cardinal;            // ��� ������������� ���������
    Prefix       : array[0..6] of byte; // ���������� ������� (�����) ��� �������
    Decimal      : byte;                // ���������� ������ ����� �������
  end;


  // �������� ������������� ��������� ��� ��'�����
  TAPPLYSEMANTIC  = packed record // ���������� ��'�����,������������ ���������
    Possible : integer;   // ��� ������������
    Must     : integer;    // ��� ���������
    Image    : integer;    // ��������� ������������ ��� ��������� ����
  end;
  PAPPLYSEMANTIC = ^TAPPLYSEMANTIC; // 05/10/2009

  TRscFont  = packed record             // �������� ������
   Font     : array[0..31] of GTKChar;             // ��������
   Name     : array[0..31] of GTKChar;            // �������� ��������
   CharSet  : integer;                  // ������� ��������
  end;

  PSIGNDEGREE = ^TSignDegree;     // 20130206 Korolev
  TSignDegree  = packed record    // ���������� ����� � ��������
   Degree  : integer;    // �������
   Minute  : integer;    // ������
// 11/12/2013   Second  : double;     // �������    // 8 ����, � � maptype.h - 4 �����
   Second  : single;     // �������                   // 11/12/2013
   Sign    : integer;    // ���� ����� (+1 ��� -1)
  end;

  TXYHDouble  = packed record
   X,Y,H : double;
  end;
  TXYHPoint =  TXYHDouble;
  PXYHDouble = ^TXYHDouble;


 // ��������� ���������� ���� ���������
 TBUILDZONEVISIBILITY  = packed record                  // 18/05/05
   PointCenter       : TDoublePoint;      // �������� �����
   RadiusMeter       : double;  // ���������� (� ������ �� ���������)
   Azimuth           : double;  // ����������� (� ��������)
   Angle             : double;  // ���� ������ (� ��������)
                                // ��� Azimuth = 0, Angle = 2*M_PI (360 ��������) 
                                // �������� �������� ����
   DeltaHight        : double;  // ������ ���������� (� ������)
   DeltaObservation  : double;  // ���������� ����������� ����� (� ������)
   VisionRst         : integer; // ��������� ��� �����������
                                // 0-�� ���� �����, 1-���� �� � �����
   StyleRst          : integer; // ������� ��������� ������
                                // 0-����������, 1-��������������
                                // 2-�����������
   ColorRst          : integer; // ���� ������
   Inversion         : integer; // 0 - ����������� �����������, 1 - �������� ���������
 end;
 PBUILDZONEVISIBILITY = ^TBUILDZONEVISIBILITY;

 TMTRPROJECTIONDATA  = packed record //  ��������� �������� ������� �����
                                     //   (������ � ��������)
   StructSize     : integer;    // ������ ������ ��������� : 128 ����
                                //  sizeof (MTRPROJECTIONDATA)
   pFree          : integer;    // ������ ���� ����
   MapType        : integer;    // ��� ����� (�������� � ������������
                                //   � MAPTYPE, ���� MAPCREAT.H)
   ProjectionType : integer;    // ��� �������� (�������� � ������������
                                //   � MAPPROJECTION, ���� MAPCREAT.H)
                                // long  MaterialProjection; // �������� ���. ���������
                                // � ��������
   FirstMainParallel,         // ������ ������� ���������
   SecondMainParallel,        // ������ ������� ���������
   AxisMeridian,              // ������ ��������
   MainPointParallel,         // ��������� ������� �����
   PoleLatitude,              // ������ ������ ��������        // 27/06/05
   PoleLongitude  : double;   // ������� ������ ��������       // 27/06/05

   EllipsoideKind,            // ��� ����������        // 01/07/05
   HeightSystem,              // ������� �����         // 01/07/05
   CoordinateSystem,          // ������� ���������     // 01/07/05
   ZoneNumber      : integer; // ����� ���� ���������   // 28/11/07
   FalseEasting,                // �������� ��������� �� ��� Y            // 05/07/10
   FalseNorthing,               // �������� ��������� �� ��� X            // 05/07/10
   ScaleFactor,                  // ���������� �����������                 // 05/07/10
   TurnAngle       : double;   // ���� ��������� ���� ��� ��������� ������ (���)   // 05/07/10
   ZoneIdent       : integer;   // ������������� ������ (��� ��� 63: A-X ��� 0)     // 26/04/11

   Reserve : array[0..11] of byte;        // ������ ���� ����
 end;
 PMTRPROJECTIONDATA = ^TMTRPROJECTIONDATA; // 02/10/2009

 TMTR3DVIEWEX   = packed record // ��������� 3D-����������� ������ (MTW � MTL)
    AreaCenterX,                // ����� ������������ ������� X,Y
    AreaCenterY,                //   (� ������ �� ���������)
    CutX,                       // ���������� ������� �������/������ X,Y,Z
    CutY,                       //   (� ������ �� ���������)      - ��� MTL
    CutZ,
    CutH           : double;    // ������ ��������������� ����� (� ������)
    ShowScale,                  // ����������� �������� ����������� ���������
    ModelHeight,                // ������ ������ � �������� (2 - MTL3D_MAXMODEL)
    GridStep,                   // ��� ����� � �������� (2 - 100)
    CutShape       : integer;   // ����� ������� (��. CUTSHAPE)   - ��� MTL
    ViewAngle      : word;      // ���� ����������/������� (� ��������)         // 01/04/13
    RotationAngle  : word;      // ���� �������� ������������ ������ AreaCenter // 01/04/13
                                //   (� ��������)
    Style,                      // C���� (0 - �����, 1 - �������)
    ShowGrid,                   // ������� ����� (0 - ���, 1 - ����)
    Shadow,                     // ������� ���� (0 - ���, 1 - ����)
    ScaleType,                  // ��� ����� (��. SCALETYPE)      - ��� MTL
    CoverMatrix,                // ������� ������� �����   (0 - ���, 1 - ����)
    CoverMap,                   // ������� �����           (0 - ���, 1 - ����)
    CoverRaster,                // ������� ������          (0 - ���, 1 - ����)
    AccordScale,                // ������������ ���������  (0 - ���, 1 - ����)
    CoverMtq,                   // ������� ������� ��������(0 - ���, 1 - ����)  // 06/10/04
    IsUpdate       : byte;      // ����������� ����������� (0 - ���, 1 - ��)    // 21/10/05
    Reserve        : array[0..25] of byte;
    CursorX        : double;    // ���������� ��������� X,Y
    CursorY        : double;    //   (� ������ �� ���������)
    Width          : integer;   // ������ �����������
    Height         : integer;    // ������ �����������
    Name           : array[0..255] of GTKChar; // ��� ����� ������������ �������
                                   //   (0 - ��� ������� �����)    - ��� MTL
end;

 TBUILDSURFACE   = packed record     // 29/05/07
  StructSize : Cardinal;        // ������ ������ ��������� : sizeof (BUILDSURFACE) = 320 ����
  FileMtw    : integer;         // ���� ���������� ������������ ����� :
                                //   0 - �������� ����� ������� ������� (*.mtq)
                                //   1 - �������� ����� ������� ����� (*.mtw)
  BeginX,                       // ������������� ���������� ������
  BeginY,                       // (���-��������� ����) ������� � ������
  Width,                        // ������ ������� � ������
  Height,                       // ������ ������� � ������
  MinValue,                     // �������� �������� �������������� �������� ����������� �������,
  MaxValue,                     // ���� MinValue >= MaxValue � ������� ���������
                                // ����������� �������� ��������
  ElemSizeMeters: double;       // ������ ������� ������������� �������
                                // � ������ �� ��������� (������� �������)
  UserType : integer;           // ������������ �����, ����������� � ����������� ��������
                                //  (���, �������������� �������)
  SearchSectorCount     : integer;      // ���������� �������� ��� ������ �������� ����� (��� Method = 9,11) // 09/06/11
                                        // 1 - �� �����
                                        // 4 - �� 4 ��������
                                        // 8 - �� 8 ��������
  UserName : array[0..31] of GTKChar;      // �������� ��� ������� (�������� �������������� ��������)

  Border : HObj;  // ������������� ���������� ������� �����, ��������������� �������
                  // ����������� ��������� ������� (���� ����� ����, �� �� ������������)
                  // ���� ������ ���������� ��� ��������� �������, ������������ ����������
                  // ����� BeginX, BeginY, Width, Height ������ ���������, �� �� ������������

  Handle : HWnd;  // ������������� ���� �������, �������� ����������
                  // ��������� 0x0581 � �������� ����������� ����� (� WPARAM),
                  // ���� ������� ������ ���� ������������� ��������, � �����
                  // ������ ��������� �������� 0x0581.
                  // ���� Handle ����� ���� - ��������� �� ����������.

  Palette : PColorRef;     // ����� ������� ����������� ������� �������(*.mtq),
                           // ���� ����� ���� - ������������ ������� �� ���������
  PaletteCount : integer;  // K��������� ������ � ������� (�� 1 �� 256)

  Method : integer;       // ����� ���������� ����������� :
                         //  8 - �������� ������������ �� ����� �������������
                         //      ( ������������ ����� ������� PointArray )
                         //  9 - ��������������� ������������ �� ��������� �����������
                         //       ������ ������������� ��������
                         //      ( ������������ ����� ������� �������� ����� )
                         // 10 - �������� ������������ �� ����� �������������
                         //      ( ������������ ����� ������� �������� ����� )
                         // 11 - �������
                         // 12 - ��������� // 19/11/12

  PointArray : PXYHDouble;  // ����� ������� �������� �������������� �������� (��� Method = 8)
  PointCount,    // ����� ����� � ������� PointArray (��� Method = 8)=
  SemanticCode,  // ��� ��������� ������������ �������������� (��� Method = 9)
  LocalSurfacePointCount,  // K��������� ����� ��� ���������� ��������� �����������
                                   // ������ ������������� �������� (��� Method = 9,11)
                                   // ���� SearchSectorCount = 4, �� LocalSurfacePointCount ������ ���� ������ 4
                                   // ���� SearchSectorCount = 8, �� LocalSurfacePointCount ������ ���� ������ 8
  LocalSurfaceRebuildPointCount : integer; // K��������� ����������� ����� ��� ��������
                                           // � ���������� ������������� ��������,
                                           // ��� ������� ��������������� ���������
                                           // ����������� (��� Method = 9)
  MaxMetricCutLength : double;  // ��� ���������� ����� �� ������� �������� � ��������� �������� (��� Method = 9,11)
                             // ������������ ���� IsMetricCutLength = 1
  Use3DMetric  : integer;    // ���� ������������� ����� �� ���������� ������� �������� :  // 06/11/07
                             //  0 - ������ �� ���������� ������� �� ������������
                             //  1 - ������ �� ���������� ������� ������������


 SemanticCode2 : integer;  // ��� ��������� �������������� �������������� ��� ���������� (��� Method = 12) // 13/02/13

 FillBorderType : integer;   // ��� ���������� ������� ������ ��������������� ������� Border
                             // 0 - ����������� ���� ������
                             // 1 - ����������� ������ ��� �����������
                             // 2 - ����������� ������ � ����������
                             // 3 - ����������� ������ ����������


  IsMetricCutLength     : integer; // ��������� ����� �� ������� �������� � ��������� �������� (��� Method = 9,11) // 09/06/11
                     // ����� ����������� � ����� MaxMetricCutLength

  IsAddPointsInEmptyRegion : integer; // ��������� ����� � ������ �������� (��� Method = 9,11) // 09/06/11

  IsLimitHeight         : integer; // ���� ��������, �� ������ �������� ������� �� ����� ���� ������ ��� ������ ����� (��� Method = 9,11) // 09/06/11
                         // �������� �����, �� ������� �������� �����������, �� LimitOffset

  DistBeforePointsInEmptyRegion: double; // ���������� ����� ������� � ������ �������� (��� Method = 9,11) // 09/06/11
                                  // �������� ������ (��������������) � ����
                                  // ������ ������������ ������� ������������ �� 8 ������������.
                                  // ��� ����� ����������� ��� ����, ����� � ������ �������� ��������� �� ���������
                                  // ��� � �� � ������� ����� �� ������� ������ �������� ���� ������������ ���������.
  LimitOffset           : double; // ���� �������� IsLimitHeight �� ����������� ���������� ������������  (��� Method = 9,11) // 09/06/11
                       // �������� ������� �� ������������, ������������� �������� ����� �������� �����


  Reserve : array[0..127] of byte;  // ������ ���� ����
 end;
 PBUILDSURFACE = ^TBUILDSURFACE;


 // ��������� ��� �������� ���������� � GeoTIFF-������
 TGEOTIFFINFORMATION  = packed record   // 20/03/03
   StructSize,                  // ����� ���������
   TypeCS,                      // ��� ������������ �������
   Spheroid,                    // ���������
   Datum,                       // ������������� ����
   PrimeMeridian,               // ������ ��������
   Projection,                  // ��������
   Zone,                        // ����� ����
   Units            : Cardinal; // ������� ��������� ����� (�����, ������� ...)
   FrameImage       : TDFrame;  // �������� ����������� � ������
   UnitsInElement_X,            // ������ �������� � �������� ��������� �� ��� X
   UnitsInElement_Y,            // ������ �������� � �������� ��������� �� ��� Y
   UnitsInElement_Z : double;   // ������ �������� � �������� ��������� �� ��� Z
   Information      : array[0..255] of GTKChar;// ���������� � ��������, ����� ��������� � �.�.
   PrecisionInch    : double;    // ����������� ����������� ����������� (�\�) // 01/11/05
   PrecisionMet     : double;   // ����������� ����������� ����������� (�\�) // 01/11/05
   UnitsAngular     : Cardinal; // ������� ��������� ����� (�������, ������� ...)  // 28/03/06
   RswWidth         : Cardinal;    // ������ ������������ ����� (��� ������������� ������� �����������������)  // 28/05/10
   RswHeight        : Cardinal;   // ������ ������������ ����� (��� ������������� ������� �����������������)  // 28/05/10
{$IFDEF CPUX64}                                           // 05/12/13  // 23/06/2016
   GeoTiffInformationZero   :  integer ;       // ������������
{$ENDIF}
   Reserv           : array[0..235] of GTKchar; // 24/11/2010
 end;
  PGEOTIFFINFORMATION = ^TGEOTIFFINFORMATION;



TTAG_SORTINT  = packed record                    // 20/01/11 // 23/06/2016

    Value : Smallint ;     // �������� ����                                       // 2
    Flag : Smallint ;      // ���� ������� ���� � �����                           // 2
{$IFDEF CPUX64}         // 05/12/13
    TagSortZero : integer  ;       // ������������
{$ENDIF}
 end;                                                                  //---4
PTAG_SORTINT = ^TTAG_SORTINT;


TTAG_DOUBLE  = packed record                       // 20/01/11   // 23/06/2016

  ValueDouble : double      ;// �������� ����                                      // 8      // 22/01/13
  Flag : Smallint ;      // ���� ������� ���� � �����                           // 2
{$IFDEF CPUX64}         // 05/12/13
  TagDoubleZero : array[0..2] of smallint;       // ������������
{$ENDIF}
 end;                                                                  //---10
PTAG_DOUBLE = ^TTAG_DOUBLE;

TGEOTIFFPARAM    = packed record                 // 19/01/11             // 23/06/2016

    // ������ ������ ���������
    Length                                    : Integer;                                                 // 4
{$IFDEF CPUX64}        // 05/12/13
    GeoTiffParamZero                          : Integer;        // ������������
{$ENDIF}
    GTModelTypeGeoKey                         : TTAG_SORTINT ;                                              // 4
    GTRasterTypeGeoKey                        : TTAG_SORTINT ;                                             // 4

// Geographic CS Parameter GeoKeys
    GeographicTypeGeoKey                      : TTAG_SORTINT ;                                           // 4
    GeogGeodeticDatumGeoKey                   : TTAG_SORTINT ;                                        // 4
    GeogPrimeMeridianGeoKey                   : TTAG_SORTINT ;                                        // 4
    GeogLinearUnitsGeoKey                     : TTAG_SORTINT ;                                          // 4
    GeogLinearUnitSizeGeoKey                  : TTAG_DOUBLE  ;  // Units: meters                     // 10
    GeogAngularUnitsGeoKey                    : TTAG_SORTINT ;                                         // 4
    GeogAngularUnitSizeGeoKey                 : TTAG_DOUBLE  ; // Units: radians                    // 10
    GeogEllipsoidGeoKey                       : TTAG_SORTINT ;                                            // 4
    GeogSemiMajorAxisGeoKey                   : TTAG_DOUBLE  ;   // Units: Geocentric CS Linear Units // 10
    GeogSemiMinorAxisGeoKey                   : TTAG_DOUBLE  ;   // Units: Geocentric CS Linear Units // 10
    GeogInvFlatteningGeoKey                   : TTAG_DOUBLE  ;   // Units: none                       // 10
    GeogAzimuthUnitsGeoKey                    : TTAG_SORTINT ;                                         // 4
    GeogPrimeMeridianLongGeoKey               : TTAG_DOUBLE  ; // Units =  GeogAngularUnits       // 10

// Projected CS Parameter GeoKeys
    ProjectedCSTypeGeoKey                     : TTAG_SORTINT ;                                          // 4

// Projection Definition GeoKeys
    ProjectionGeoKey                          : TTAG_SORTINT ;                                               // 4
    ProjCoordTransGeoKey                      : TTAG_SORTINT ;                                           // 4
    ProjLinearUnitsGeoKey                     : TTAG_SORTINT ;                                          // 4
    ProjLinearUnitSizeGeoKey                  : TTAG_DOUBLE  ;  // Units: meters                     // 10
    ProjStdParallelGeoKey                     : TTAG_DOUBLE  ;     // Units: GeogAngularUnit            // 10
    ProjStdParallel2GeoKey                    : TTAG_DOUBLE  ;    // Units: GeogAngularUnit            // 10
    ProjOriginLongGeoKey                      : TTAG_DOUBLE  ;      // Units: GeogAngularUnit            // 10
    ProjOriginLatGeoKey                       : TTAG_DOUBLE  ;       // Units: GeogAngularUnit            // 10
    ProjFalseEastingGeoKey                    : TTAG_DOUBLE  ;    // Units: ProjLinearUnit             // 10
    ProjFalseNorthingGeoKey                   : TTAG_DOUBLE  ;   // Units: ProjLinearUnit             // 10
    ProjFalseOriginLongGeoKey                 : TTAG_DOUBLE  ; // Units: GeogAngularUnit            // 10
    ProjFalseOriginLatGeoKey                  : TTAG_DOUBLE  ;  // Units: GeogAngularUnit            // 10
    ProjFalseOriginEastingGeoKey              : TTAG_DOUBLE  ; // Units: ProjLinearUnit          // 10
    ProjFalseOriginNorthingGeoKey             : TTAG_DOUBLE  ;// Units: ProjLinearUnit          // 10
    ProjCenterLongGeoKey                      : TTAG_DOUBLE  ;      // Units: GeogAngularUnit            // 10
    ProjCenterLatGeoKey                       : TTAG_DOUBLE  ;       // Units: GeogAngularUnit            // 10
    ProjCenterEastingGeoKey                   : TTAG_DOUBLE  ;   // Units: ProjLinearUnit             // 10
    ProjCenterNorthingGeoKey                  : TTAG_DOUBLE  ;  // Units: ProjLinearUnit             // 10
    ProjScaleAtOriginGeoKey                   : TTAG_DOUBLE  ;   // Units: none                       // 10
    ProjScaleAtCenterGeoKey                   : TTAG_DOUBLE  ;   // Units: none                       // 10
    ProjAzimuthAngleGeoKey                    : TTAG_DOUBLE  ;    // Units: GeogAzimuthUnit            // 10
    ProjStraightVertPoleLongGeoKey            : TTAG_DOUBLE  ; // Units: GeogAngularUnit       // 10

// Vertical CS Parameter Keys
    VerticalCSTypeGeoKey                      : TTAG_SORTINT ;                                           // 4
    VerticalDatumGeoKey                       : TTAG_SORTINT ;                                            // 4
    VerticalUnitsGeoKey                       : TTAG_SORTINT ;                                            // 4

    // TAG_MODELPIXELSCALE 33550 // ������ ����� ������ �� X,Y,Z                // 02/04/11
    // ModelPixelScaleTag = (ScaleX, ScaleY, ScaleZ)
    ModelPixelScaleTag                        : array[0..2] of double;                                               // 24
    Flag_ModelPixelScaleTag                   : Smallint;      // ���� ������� ���� � �����        // 2
{$IFDEF CPUX64}       // 05/12/13 
    GeoTiffParam1Zero                         : array[0..2] of Smallint;       // ������������
{$ENDIF}
    // TAG_MODELTIEPOINT   33922 // �������� ����� ������                       // 02/04/11
    // ModelTiepointTag = (...,I,J,K, X,Y,Z...), ������ �������� (I,J,K, X,Y,Z) 6 ��������
    ModelTiepointTag                          : array[0..1023] of double;                                  // 8192
    Flag_ModelTiepointTag                     : Smallint;        // ���� ������� ���� � �����        // 2
                                            // ���� ��� TAG_MODELTIEPOINT � ����� �����������, �� Flag_ModelTiepointTag = 0
                                            // ���� ��� TAG_MODELTIEPOINT � ����� ����, �� � Flag_ModelTiepointTag ������������ ���������� �������� ���� double
{$IFDEF CPUX64}        // 05/12/13
    GeoTiffParam2Zero                         : array[0..2] of Smallint;       // ������������
{$ENDIF}
    // TAG_MODELTRANSFORMATION       33920  // ������� ����������������� ������ // 02/04/11
    // TAG_MODELTRANSFORMATION_34264 34264  // ������� ����������������� ������
    ModelTransformationTag                     : array[0..15] of double;                                          // 128
    Flag_ModelTransformationTag                : Smallint;  // ���� ������� ������� ����������������� � �����        // 2
                                            // � Flag_ModelTransformationTag ����� ������������ ��������� ��������:
                                            // 0 - ������� ����������������� � ����� �����������
                                            // 1 - ������ ModelTransformationTag �������� ������� ����������������� �� ���� 33920(TAG_MODELTRANSFORMATION)        // 16/08/13
                                            // 2 - ������ ModelTransformationTag �������� ������� ����������������� �� ���� 34264(TAG_MODELTRANSFORMATION_34264)  // 16/08/13

   // /** Do we have any definition at all?  0 if no geokeys found */
    Flag                                       : Smallint;  // ���� ���������� ���� �� ����� ���������               // 2
{$IFDEF CPUX64}  // 05/12/13
    GeoTiffParam3Zero                          : integer ;            // ������������
{$ENDIF}

    // 05/03/12 �������� ����� (��������� �������� � WGS-84)7��.
    // KeyID = 2062; Type = 3/7 * DOUBLE; Values = dX, dY, dZ, Rx, Ry, Rz, dS
    GeogTOWGS84GeoKey                          : array[0..6] of double;                                                // 56       // 05/03/11
    Flag_GeogTOWGS84GeoKey                     : Smallint;       // ���� ������� ���� � �����        // 2        // 05/03/11
                                            // ���� ��� GeogTOWGS84GeoKey � ����� �����������, �� Flag_GeogTOWGS84GeoKey = 0
                                            // ���� ��� GeogTOWGS84GeoKey � ����� ����, �� � Flag_GeogTOWGS84GeoKey ������������ ���������� �������� ���� double (3 ��� 7)

{$IFDEF CPUX64}  // 05/12/13
    GeoTiffParam4Zero                          : array[0..5] of GTKchar;       // ������������
{$ENDIF}
    Reserv                                     : array[0..7] of GTKchar;    // 05/03/12


 end;  
 PGEOTIFFPARAM = ^TGEOTIFFPARAM;  
 
 // ��������� ��� �������� ���������� � ����� JPEG � ������ EXIF     // 02/05/12  // 23/06/2016
TEXIFPARAM   = packed record     

  Length                     : integer  ; // ������ ������ ���������               // 4

  // Indicates the version of GPSInfoIFD. The version is given as 2.2.0.0. This tag is mandatory when GPSInfo tag is
  // present. Note that the GPSVersionID tag is written as a different byte than the Exif Version tag.
  GPSVersionID               : array [0..MAX_PATH-1] of GtkChar; // 0 - GPSVersionID                              // 260
  Flag_GPSVersionID          : smallint;                                                  // 2

  // Indicates the latitude.
  GPSLatitude                :TSIGNDEGREE;                                                       // 16
  Flag_GPSLatitude           : smallint;                                                   // 2

  // Indicates the longitude.
  GPSLongitude   :TSIGNDEGREE;                                                      // 16
  Flag_GPSLongitude          : smallint;                                                  // 2

  GPSAltitude                :TTAG_DOUBLE;   // 6 - GPSAltitude                                  // 10

  // Indicates the time as UTC (Coordinated Universal Time). TimeStamp is expressed as three RATIONAL values
  // giving the hour, minute, and second.
  GPSTimeStamp_Hour          :TTAG_DOUBLE;   // 7 - GPSTimeStamp                           // 10
  GPSTimeStamp_Minute        :TTAG_DOUBLE; // 7 - GPSTimeStamp                           // 10
  GPSTimeStamp_Second        :TTAG_DOUBLE; // 7 - GPSTimeStamp                           // 10

  // Indicates the GPS satellites used for measurements. This tag can be used to describe the number of satellites,
  // their ID number, angle of elevation, azimuth, SNR and other information in ASCII notation. The format is not
  // specified. If the GPS receiver is incapable of taking measurements, value of the tag shall be set to NULL.
  GPSSatellites              : array [0..MAX_PATH-1] of GtkChar; // 8 - GPSSatellites                            // 260
  Flag_GPSSatellites         : smallint;                                                 // 2

  // Indicates the status of the GPS receiver when the image is recorded. 'A' means measurement is in progress, and
  // 'V' means the measurement is Interoperability.
  GPSStatus                  : array [0..3] of Byte; // 9 - GPSStatus                                  // 4
  Flag_GPSStatus             : smallint;                                                     // 2

  // Indicates the GPS measurement mode. '2' means two-dimensional measurement and '3' means three-dimensional
  // measurement is in progress.
  GPSMeasureMode              : array [0..3] of Byte; // 10 - GPSMeasureMode                       // 4
  Flag_GPSMeasureMode         : smallint;                                                // 2

  // Indicates the GPS DOP (data degree of precision). An HDOP value is written during two-dimensional measurement,
  // and PDOP during three-dimensional measurement.
  GPSDOP                      :TTAG_DOUBLE; // 11 - GPSDOP                                             // 10

  // Indicates the unit used to express the GPS receiver speed of movement. 'K' 'M' and 'N' represents kilometers per
  // hour, miles per hour, and knots.
  // 'K' = Kilometers per hour
  // 'M' = Miles per hour
  // 'N' = Knots
  GPSSpeedRef                 : array [0..3] of Byte; // 12                                           // 4
  Flag_GPSSpeedRe             : smallint;                                                   // 2

  // Indicates the speed of GPS receiver movement.
  GPSSpeed                     :TTAG_DOUBLE; // 13                                                    // 10

  // Indicates the reference for giving the direction of GPS receiver movement. 'T' denotes true direction and 'M' is
  // magnetic direction.
  // 'T' = True direction
  // 'M' = Magnetic direction
  GPSTrackRef                  : array [0..3] of Byte; // 14                                           // 4
  Flag_GPSTrackRef             : smallint;                                                   // 2

  // Indicates the direction of GPS receiver movement. The range of values is from 0.00 to 359.99.
  GPSTrack                     :TTAG_DOUBLE; // 15                                                    // 10

  // Indicates the reference for giving the direction of the image when it is captured. 'T' denotes true direction and 'M' is
  // magnetic direction.
  // 'T' = True direction
  // 'M' = Magnetic direction
  GPSImgDirectionRef           : array [0..3] of Byte; // 16                                    // 4
  Flag_GPSImgDirectionRef      : smallint;                                            // 2

  // Indicates the direction of the image when it was captured. The range of values is from 0.00 to 359.99.
  GPSImgDirection              :TTAG_DOUBLE; // 17                                             // 10

  // Indicates the geodetic survey data used by the GPS receiver. If the survey data is restricted to Japan, the value of
  // this tag is 'TOKYO' or 'WGS-84'. If a GPS Info tag is recorded, it is strongly recommended that this tag be recorded.

  GPSMapDatum                  : array [0..MAX_PATH-1] of Byte; // 18                                    // 260
  Flag_GPSMapDatum             : smallint;                                                   // 2

  // GPSDestLatitudeRef 19 13 GPS Info extension - GPSDestLatitudeRef
  // Indicates whether the latitude of the destination point is north or south latitude.

  // Indicates the latitude of the destination point. The latitude is expressed as three RATIONAL values giving the
  // degrees, minutes, and seconds, respectively. If latitude is expressed as degrees, minutes and seconds, a typical
  // format would be dd/1,mm/1,ss/1. When degrees and minutes are used and, for example, fractions of minutes are
  // given up to two decimal places, the format would be dd/1,mmmm/100,0/1.
  GPSDestLatitude               : TSIGNDEGREE; // 20                                             // 16
  Flag_GPSDestLatitude          : smallint;                                               // 2

  //GPSDestLongitudeRef 21 15 GPS Info extension - GPSDestLongitudeRef

  // Indicates the longitude of the destination point.
  GPSDestLongitude              : TSIGNDEGREE; // 22                                            // 16
  Flag_GPSDestLongitude         : smallint;                                              // 2

  // Indicates the reference used for giving the bearing to the destination point. 'T' denotes true direction and 'M' is
  // magnetic direction.
  // 'T' = True direction
  // 'M' = Magnetic direction
  GPSDestBearingRef             : array [0..3] of Byte; // 23                                     // 4
  Flag_GPSDestBearingRef        : smallint;                                             // 2

  // Indicates the bearing to the destination point. The range of values is from 0.00 to 359.99.
  GPSDestBearing                : TTAG_DOUBLE; // 24                                              // 10

  // Indicates the unit used to express the distance to the destination point. 'K', 'M' and 'N' represent kilometers, miles
  // and knots.
  // 'K' = Kilometers
  // 'M' = Miles
  // 'N' = Knots
  GPSDestDistanceRef           : array [0..3] of Byte; // 25                                    // 4
  Flag_GPSDestDistanceRef      : smallint;                                            // 2

  // Indicates the distance to the destination point.
  GPSDestDistance              : TTAG_DOUBLE; // 26                                             // 10


  // A character string recording the name of the method used for location finding. The first byte indicates the character
  // code used (Table 6�Table 7), and this is followed by the name of the method. Since the Type is not ASCII, NULL
  // termination is not necessary.
  // GPSProcessingMethod  // 27 !!!

  // A character string recording the name of the GPS area. The first byte indicates the character code used (Table 6�
  // Table 7), and this is followed by the name of the GPS area. Since the Type is not ASCII, NULL termination is not
  // necessary.
  // GPSAreaInformation  // 28 !!!

  // A character string recording date and time information relative to UTC (Coordinated Universal Time). The
  // format is "YYYY:MM:DD." The length of the string is 11 bytes including NULL.
  GPSDateStamp                 : array [0..MAX_PATH-1] of Byte;  // 29                                  // 260
  Flag_GPSDateStamp            : smallint;                                                  // 2

  // Indicates whether differential correction is applied to the GPS receiver.
  // 0 = Measurement without differential correction
  // 1 = Differential correction applied
  GPSDifferential              : TTAG_SORTINT;                                                  // 4

  // The image orientation viewed in terms of rows and columns.
  // 1 = The 0th row is at the visual top of the image, and the 0th column is the visual left-hand side.
  // 2 = The 0th row is at the visual top of the image, and the 0th column is the visual right-hand side.
  // 3 = The 0th row is at the visual bottom of the image, and the 0th column is the visual right-hand side.
  // 4 = The 0th row is at the visual bottom of the image, and the 0th column is the visual left-hand side.
  // 5 = The 0th row is the visual left-hand side of the image, and the 0th column is the visual top.
  // 6 = The 0th row is the visual right-hand side of the image, and the 0th column is the visual top.
  // 7 = The 0th row is the visual right-hand side of the image, and the 0th column is the visual bottom.
  // 8 = The 0th row is the visual left-hand side of the image, and the 0th column is the visual bottom.
  Orientation                  : TTAG_SORTINT; // 274                                               // 4

  // The number of pixels per ResolutionUnit in the ImageWidth direction. When the image resolution is unknown, 72
  // [dpi] is designated.
  XResolution                  : TTAG_DOUBLE; // 282                                                // 10

  // Image resolution in height  direction
  YResolution                  : TTAG_DOUBLE; // 283                                                // 10

  // The unit for measuring XResolution and YResolution. The same unit is used for both XResolution and YResolution.
  // If the image resolution in unknown, 2 (inches) is designated.
  // 2 = inches
  // 3 = centimeters
  ResolutionUnit               : TTAG_SORTINT; // 296                                            // 4

  // The date and time of image creation. In this standard it is the date and time the file was changed. The format is
  // "YYYY:MM:DD HH:MM:SS" with time shown in 24-hour format, and the date and time separated by one blank
  // character [20.H]. When the date and time are unknown, all the character spaces except colons (":") may be filled
  // with blank characters, or else the Interoperability field may be filled with blank characters. The character string
  // length is 20 bytes including NULL for termination. When the field is left blank, it is treated as unknown.
  DateTime                     : array [0..MAX_PATH-1] of Byte;  // 306                                     // 260
  Flag_DateTime                : smallint;                                                      // 2

  // A character string giving the title of the image. It may be a comment such as "1988 company picnic" or the like.
  // Two-byte character codes cannot be used. When a 2-byte code is necessary, the Exif Private tag UserComment is
  // to be used.
  ImageDescription              : array [0..MAX_PATH-1] of Byte;  // 270                             // 260
  Flag_ImageDescription         : smallint;                                              // 2

  // The manufacturer of the recording equipment. This is the manufacturer of the DSC, scanner, video digitizer or other
  // equipment that generated the image. When the field is left blank, it is treated as unknown.
  Make                          : array [0..MAX_PATH-1] of Byte;  // 271                                         // 260
  Flag_Make                     : smallint;                                                          // 2

  // The model name or model number of the equipment. This is the model name of number of the DSC, scanner, video
  // digitizer or other equipment that generated the image. When the field is left blank, it is treated as unknown.
  Model                         : array [0..MAX_PATH-1] of Byte;  // 272                                        // 260
  Flag_Model                    : smallint;                                                         // 2

  // This tag records the name and version of the software or firmware of the camera or image input device used to
  // generate the image. The detailed format is not specified, but it is recommended that the example shown below be
  // followed. When the field is left blank, it is treated as unknown.
  // Ex.) "Exif Software Version 1.00a"
  Software                      : array [0..MAX_PATH-1] of Byte;  // 305                                     // 260
  Flag_Software                 : smallint;                                                      // 2

  // This tag records the name of the camera owner, photographer or image creator. The detailed format is not specified,
  // but it is recommended that the information be written as in the example below for ease of Interoperability. When the
  // field is left blank, it is treated as unknown.
  Artist                        : array [0..MAX_PATH-1] of Byte;  // 315                                       // 260
  Flag_Artist                   : smallint;                                                        // 2
 
  // Copyright information. In this standard the tag is used to indicate both the photographer and editor copyrights. It is
  // the copyright notice of the person or organization claiming rights to the image. The Interoperability copyright
  // statement including date and rights should be written in this field; e.g., "Copyright, John Smith, 19xx. All rights
  // reserved." In this standard the field records both the photographer and editor copyrights, with each recorded in a
  // separate part of the statement. When there is a clear distinction between the photographer and editor copyrights,
  // these are to be written in the order of photographer followed by editor copyright, separated by NULL (in this case,
  // since the statement also ends with a NULL, there are two NULL codes) (see example 1). When only the
  // photographer copyright is given, it is terminated by one NULL code (see example 2). When only the editor
  // copyright is given, the photographer copyright part consists of one space followed by a terminating NULL code,
  // then the editor copyright is given (see example 3). When the field is left blank, it is treated as unknown.
  // Ex. 1) When both the photographer copyright and editor copyright are given.
  // Photographer copyright + NULL[00.H] + editor copyright + NULL[00.H]
  // Ex. 2) When only the photographer copyright is given.
  // Photographer copyright + NULL[00.H]
  // Ex. 3) When only the editor copyright is given.
  // Space[20.H]+ NULL[00.H] + editor copyright + NULL[00.H]
  Copyright                      : array [0..MAX_PATH-1] of Byte;  //  33432                                 // 260
  Flag_Copyright                 : smallint;                                                     // 2

                                                                                // �����: 3114

  Reserve                        : array [0..65] of GtkChar;                                                             // 66

  Flag                           : smallint;  // ���� ���������� ���� �� ����� ���������               // 2

  end;
   PEXIFPARAM = ^TEXIFPARAM; 
                                                                   // 3200



 
 


  TMAPLISTPARM       = packed record  // ��������� �������� ������� "������ ������"
   Handle        : HWnd;         // ������������� ���� ���������
   {$IFDEF CPUX64}
    Reserv : integer;            //������������  13/09/2016
   {$ENDIF}
   PageNumber    : integer;      //  ����� �������� �������� ��� �������� ������� "������ ������"
                                 //  0 - �������������� �������� "������ �������"
                                 //  1 - �������������� �������� "������ ������"
                                 //  2 - �������������� �������� "������ ��"
                                 //  3 - �������������� �������� "������ TIN-�������"   // 24/10/11
                                 //  4 - �������������� �������� "������ WMS"           // 24/10/11
   SaveMtrPath,                  // �����������������
   SaveRstPath,                  // �����������
   SaveSitPath,                  // ������
   SaveTinPath   : array[0..MAX_PATH-1] of GTKChar;  // 31/03/05
   RstPageNumber,                //  � �������� "������ �������" ����������
                                 //  ���(���������) ������  - 0(1);
   MtrPageNumber,                //  � �������� "������ ������" ����������
                                 //  ���(���������) �������  - 0(1);
   SitPageNumber,                //  � �������� "������ ��" ����������
                                 //  ���(���������) ��  - 0(1);
   TinPageNumber,                // 31/03/05
   RstNumber,                    //  ����� ��������� ������ � �������� "������ �������"
   MtrNumber,                    //  ����� �������� ������� � �������� "������ ������"
   SitNumber,                    //  ����� �������� �� � �������� "������ ��"
   TinNumber     : integer;      // 31/03/05
   CentrePosition: TDoublePoint; // ���������� ������ ���� � ������ � ������
  end;

  TMAPLISTPARMUN       = packed record    // ��������� �������� ������� "������ ������"

   Handle        : HWnd;        // ������������� ���� ���������
   {$IFDEF CPUX64}
    Reserv : integer;           // ������������  13/09/2016
   {$ENDIF}
   PageNumber    : integer;     //  ����� �������� �������� ��� �������� ������� "������ ������"
                                //  0 - �������������� �������� "������ �������"
                                //  1 - �������������� �������� "������ ������"
                                //  2 - �������������� �������� "������ ��"
                                //  3 - �������������� �������� "������ TIN-�������"
                                //  4 - �������������� �������� "������ WMS"
  RstPageNumber : integer;      //  � �������� "������ �������" ����������
                                //  ���(���������) ������  - 0(1);
  MtrPageNumber : integer;      //  � �������� "������ ������" ����������
                                //  ���(���������) �������  - 0(1);
  SitPageNumber : integer;      //  � �������� "������ ��" ����������
                                //  ���(���������) ��  - 0(1);
  TinPageNumber : integer;

  RstNumber: integer;               //  ����� ��������� ������ � �������� "������ �������"
  MtrNumber: integer;               //  ����� �������� ������� � �������� "������ ������"
  SitNumber: integer;               //  ����� �������� �� � �������� "������ ��"
  TinNumber: integer;               //  ����� �������� TIN � �������� "������ ������"
  WmsNumber: integer;               //  ����� �������� WMS � �������� "������ ������"

  Reserv2  : array[0..6] of integer;               // ������ (������ ���� �������)

  CentrePosition : TDoublePoint;  // ���������� ������ ���� � ������ � ������

  SaveMtrPath : array[0..MAX_PATH_LONG-1] of GTKChar;  // �����������������
  SaveRstPath : array[0..MAX_PATH_LONG-1] of GTKChar;  // �����������
  SaveSitPath : array[0..MAX_PATH_LONG-1] of GTKChar;  // ������
  SaveTinPath : array[0..MAX_PATH_LONG-1] of GTKChar;
 end;


  TPALETTECNTPARM    = packed record  // ��������� �������� ������� "���������� ��������"
   Handle                 : HWnd;        // ������������� ���� ���������
   ColorOfSelectObject,                  // ���� ��������� ��������
   ColorOfSelectedObjects,               // ���� ���������� ��������
   ColorOfSetObjects      : TColorRef;   // ���� ������ ��������  //04/05/00
   ThickOfSelectObject    : integer;     // ������� ����� ��������� ��������
  end;
   PPALETTECNTPARM = ^TPALETTECNTPARM;  // 07/10/2009

  TLINEOFLIST   = packed record       // 27/02/02
   OldNumber : integer;
   Name      : array[0..MAX_PATH-1] of GTKChar;
  end;

 TCHOICEOBJECTPARM  = packed record
   Select    : hSelect;         // ������
   MapSelect : integer;         // ������ ��� ������ �� ������� ����
                                //  = 0  - ������ �� ���� ������ ���������
                                //  = 1  - ������ �� ����� ����� (����� �����
                                //         ��������� �� �������������� �������)
   MapEditSelect: integer;      // ������ ��� ������ �� ������� ���� (�� �������� ��������������)
                                //  = 0  - ������ �� ���� ������ ���������
                                //  = 1  - ������ �� ����, ���������� ��������������
  end;
   PCHOICEOBJECTPARM = ^TCHOICEOBJECTPARM; // 07/10/2009

  POBJFROMRSC = ^TOBJFROMRSC;
  TOBJFROMRSC = packed record
   Select : HSELECT;       // ������ (��� ��������� ������ ��������� �����������)
   MapSelect : longint;    // ������ ��� ������ �� ������� ����
                           //  = 0 - ������ �� ���� ������ ���������
                           //  = 1 - ������ ������������� �����
                           //  = 2 - ������ �� ����� ����� (������ ��, �� �������
                           //        ��������������� ������)
   Regime : longint;       // ����� �������� �� MC_POLYLINE ��  MC_THREEPOINT
                           // = -1 - �� ����������
                           // ���� ���������� �����, �� ��������������� �����������
                           // ����� ���������� MC_POLYLINE
   Rpt : longint;          // ����� "������ ���������"
                           // = 0 ���������, 1 �������� -1 �� ����������
                           // ����� ����� ��� ���������������� �������� ���������� ��������
                           // ����� ������ ���� �� ��������������
                           // ���� ����� ��������, ����� ����� �������� ������� �������
                           // ������� ������ ���������� ��������� � ��� ������ ��������
                           // ������ ���������� ��������� �� ����������
                           // ���� ����� ��������� - ��� ������ ������� ������������ �������
                           // �������� ������ ���������� ���������
   end;

  TMEDRSCPARM = packed record
    Regime: integer;    /// ����� ��������
                      // �� MED_KEY1 �� MED_KEY14
                      // == -1 - ������ �������� ���������� � �������!
    fRepeat: integer; // ������������� ���������
                      // == 1 - �����������
                      // == 0 - �������������
                      // == -1 - ���������� � �������
    FlagKey: integer; // ���� ��������� ������� ��������
                      // ���� == 0, �� ����������� �������
                      //            ��������������� �������������,
                      //            ����� � ������������ � ���
    Rezerv  : Integer;
    NameDlg: array[1..128]of GTKChar; // �������� �������
    Key: array[1..32]of GTKChar; // ����������� ������� ��������:
                               //  0 - ����������, 1 - ��������
  end;
  PMEDRSCPARM = ^TMEDRSCPARM;


//-----------------------------------------------------------------
// ��������� ������� ������ �����
//-----------------------------------------------------------------

 TMAPPRINTPARM      = packed record
   Length : Cardinal;   // ����� ��������� MAPPRINTPARM
{$IFDEF CPUX64}                                                                 // 25/12/2013
   MapPrintZero : integer; // ������������                                      // 25/12/2013
{$ENDIF}
   Handle: HWND;        // ������������� ���� �����
                                                           // 0x008 (8)
   Scale,                // ������� ������ (�����������)
   ScaleSave : integer;  // ������� ������ (����������� ��� FitToPage = 1)
                                                           // 0x010 (16)
   RectMetr : TRect;     // ������������� ������ � ������ (� ������)
                                                           // 0x020 (32)
   ShiftLTmm,            // �������� (� �����������)
   ShiftUPmm,            //
                                                           // 0x028 (40)
   FieldLTmm,            // ���� �������� (� �����������)
   FieldUPmm,            //
   FieldRTmm,            //
   FieldDNmm,            //
                                                           // 0x038 (56)
   Intensity,            // ������������� ������� ��������� (0-100)
   Copies : integer;     // ���������� �����
                                                           // 0x040 (64)
   Regime,               // ����� ������ ������ (��. MAPPRINT_REGIME)
   Preview,              // ����� ������ ���� (��. MAPPRINT_PREVIEW)
   TypePrint,            // ��� ������ (��. MAPPRINT_TYPE)
   TypeOutput,           // ��� ������ (��. MAPPRINT_OUTPUT)                                                          // 0x044 (68)
   Orientation,          // ���������� (��. MAPPRINT_ORIENT)
   FileFlag,             // ���� ���������� ������ (1 - ����, 0 - �������)
   Border,               // ���� ������ ����� (1 - ����, 0 - ���)
   FitToPage,            // ������������ � �������� ��������
                         //   (��. MAPPRINT_FITTING)
   Black,                // ���� �����-����� ������ (1 - ����, 0 - ���)
   Calibration,          // ���� ����� ������������� ����������
                         //   (1 - ���������, 0 - ���)
   Mirror   : byte;      // ���� ����������� ������ (1 - ����, 0 - ���)

   Reserve  : byte;       // ������ (������ ���� �������)
                                                           // 0x04C (76)
   FileName : array[0..259] of GTKChar;// ��� ����� ������   // 12/10/2016
                                                           // 0x150 (336)
  end;
   PMAPPRINTPARM = ^TMAPPRINTPARM;  // 07/10/2009

 TMAPPRINTPARMEX    = packed record
   Length : Cardinal;   // ����� ��������� MAPPRINTPARM
{$IFDEF CPUX64}                                                                 // 25/12/2013
   MapPrintZero : integer; // ������������                                      // 25/12/2013
{$ENDIF}
   Handle: HWND;        // ������������� ���� �����
                                                           // 0x008 (8)
   Scale,                // ������� ������ (�����������)
   ScaleSave : integer;  // ������� ������ (����������� ��� FitToPage = 1)
                                                           // 0x010 (16)
   RectMetr : TRect;     // ������������� ������ � ������ (� ������)
                                                           // 0x020 (32)
   ShiftLTmm,            // �������� (� �����������)
   ShiftUPmm,            //
                                                           // 0x028 (40)
   FieldLTmm,            // ���� �������� (� �����������)
   FieldUPmm,            //
   FieldRTmm,            //
   FieldDNmm,            //
                                                           // 0x038 (56)
   Intensity,            // ������������� ������� ��������� (0-100)
   Copies : integer;     // ���������� �����
                                                           // 0x040 (64)
   Regime,               // ����� ������ ������ (��. MAPPRINT_REGIME)
   Preview,              // ����� ������ ���� (��. MAPPRINT_PREVIEW)
   TypePrint,            // ��� ������ (��. MAPPRINT_TYPE)
   TypeOutput,           // ��� ������ (��. MAPPRINT_OUTPUT)                                                          // 0x044 (68)
   Orientation,          // ���������� (��. MAPPRINT_ORIENT)
   FileFlag,             // ���� ���������� ������ (1 - ����, 0 - �������)
   Border,               // ���� ������ ����� (1 - ����, 0 - ���)
   FitToPage,            // ������������ � �������� ��������
                         //   (��. MAPPRINT_FITTING)
   Black,                // ���� �����-����� ������ (1 - ����, 0 - ���)
   Calibration,          // ���� ����� ������������� ����������
                         //   (1 - ���������, 0 - ���)
   Mirror,               // ���� ����������� ������ (1 - ����, 0 - ���)
   CutLine : byte;       // ���� ������ ����� ������� (1 - ����, 0 - ���)
                         //   (��� OverlapLT, OverlapUP,
                         //    OverlapRT ��� OverlapDN > 0) // 0x04C (76)
   FileName : array[0..259] of GTKChar;// ��� ����� ������   // 12/10/2016
                                                           // 0x150 (336)
   OverlapLTmm,          // ���� ���������� ����������� ��������
   OverlapUPmm,          //       ������� (� �����������)
   OverlapRTmm,          //
   OverlapDNmm : integer;//
                                                           // 0x160 (352)
   PageWidth,            // ������� ���������� ������� �������� �� �������
   PageHeight,           // ����� ���������� (� �����������)
                                                           // 0x170 (368)
   Angle  : double;       // ���� �������� ����� (� ��������)
                                                           // 0x178 (376)
   PlaneFrame : TDFRAME;  // �������� ��������� ������� � ������
                          // (���� ������� - ������������� �� ��������� ������).
                          //  X1,Y1 - 1 ����� ������������� �������
                          //  X2,Y2 - 3 ����� ������������� �������
                          // ��� TurnFrame = 1, ������� 1 � 3 ����� ���������
                          // ���������� ��������������, �� ������������ �����
                                                           // 0x198 (408)
   SiteDecor : HSITE;     // ���������� ���������� (������ ���� �������)
                                                           // 0x19C (412)
   FrameKey : integer;    // ����� ������� �����, �� �������� ���������������
                          // �������� ������� ������ PlaneFrame
                          // (���� ����� 0 ��� ������ �����������,
                          //  �� �������� ������������)
                                                           // 0x1A0 (416)

{$IFDEF CPUX64}                                                                 // 25/12/2013
   MapPrintAddZero : integer; // ������������                                   // 25/12/2013
{$ENDIF}

   FrameList : array[0..31] of GTKChar;   // ��� ����� (������������) ������� FrameKey
                                                           // 0x1C0 (448)
   PaperWidth,            // ������ ����� (� �����������)
   PaperHeight : integer; //
                                                           // 0x1C8 (456)
   ReserveEx : array [0..47] of GTKchar;   // ������ (������ ���� �������)
   
   PageCountHor,    // ����� ������� �� �����������                         // 15/05/14  // 23/06/2016
                        // (������������ ��� FitToPage = MPF_FITBYHOR)
   PageCountVer,    // ����� ������� �� ���������
                        // (������������ ��� FitToPage = MPF_FITBYVER)

   FileCount,             // ����� ����������� ������ (������������ ��� PostScript = 1)
                          //   (1 - ���� �������� ��� ������������ ����� C,M,Y,K ��� R,G,B
                          //    4 - ������ �� ������� ������ �������� ���� ��
                          //        ������������ ����� C,M,Y,K)
                                                             // 0x1FB (507)
   ColorModel,            // �������� ������ ������ � PostScript
                          //   (0 - RGB, 1 - CMYK)
   PostScript,            // ���� PostScript-������
   Restore,               // ���� ������������� �������������� ����������
                          //   �� INI-����� ����� (1 - ����, 0 - ���)
   TurnFrame,             // ���� �������� ������� ������ (1 - ����, 0 - ���)
   Decoration : byte;     // ���� ����������� ���������� (������ ���� �������)
 end;
  PMAPPRINTPARMEX = ^TMAPPRINTPARMEX;

 TRESTOREMODE       = packed record
   Note,                    // ���������� ��� ��������������/����������� ��������� �������
   VisualSeek,              // ���� ������ ����� ������� �� ����� ��������.
   MapSeek,                 // ���� ������ �� ������.
   RestoreSelect : integer; // ���� �������������� ��������� ������.
  end;
   PRESTOREMODE = ^TRESTOREMODE;  // 07/10/2009
// ���������        ��������

// Note             ����� �������� �������� ������� ������ ��� ������ ���������
// VisualSeek
//                  "-1" - ���������� ������������ ��������;
//                  "0"  - ���������� ����� ��� ����� ��������� ��������;
//                  "1"  - ���������� ����� � ������ ��������� ��������.
// MapSeek
//                  "-1" - ���������� ������������ ��������;
//                  "-2" - ���������� ����� �� ���� ������;
//                  "0...n" - ���������� ����� � ����� � ��������� �������.
// RestoreSelect
//                  "-1" - ���������� ������������ �������� ������;
//                  "0"  - ������������ ������� ��������� ��������� ������.


// �������� ��������� ���������� ��� ������� ������ �������� �� �������.

 TAREASEEKPARM     = packed record
    Size,          // ������ ���������.
    RestoreSelect, // ���� �������������� ��������� ������ �� �������.
    ToSelectArea,  // ���� ������ ������� ������
    VisualSeek,    // ���� ������ ����� ������� �� ����� ��������.
    MapSeek,       // ���� ������ �� ������.
    AreaType,      // ��� ������� ������: ������/�������� �����.
    InsideFlag,    // ���� ������ ������.
    FilterFlag,    // ���� ������������� ������� ��������.
    AreaAction,    // ������� ������ �������� �����.
    InversionFlag : integer; // ���� ���������� ��������� ��������       26/02/02
    Distance : double;      // ���������� ������ (� ������).
 end;
 PAREASEEKPARM = ^TAREASEEKPARM;

 //  ����� �������� ����� �� ��������

 TARRAYNAME = packed record
     Code  : integer;         // ��� ��������� ��� ������
     Title : integer;         // ������� ����� ��������
     Count : integer;         // ���������� ��������� ��������
     // ������ ��������� �������� ��� ������ �� �����
     Names : array[0..15] of array[0..255] of GTKChar;
 end;
  PARRAYNAME = ^TARRAYNAME;  // 07/10/2009

  TNAMESARRAY = packed record
     Size  : integer;         // ��� ��������� ��� ������
     Count : integer;         // ������� ����� ��������
     Lists : array[0..MAX_PATH-1] of GTKChar;     // ������
 end;
   PNAMESARRAY = ^TNAMESARRAY;  // 07/10/2009

type INFOCOORD = packed record
  CoordPsp      : double;
  CoordCalc     : double;
  Delta         : double;
  Number        : integer;
  Reserve       : integer;
end;

type
 TINFOLIST  = packed record
  Nomenclature  : array[0..31] of GTKChar;  // ������������
  Scale         : integer;       // �������
  CountObject   : integer;       // ���������� ��������
  MapType       : integer;       // ��� �����
 end;



 TINFOSXF  = packed record
   SheetName    : array[0..31] of GTKChar;    // ��������
   Nomenclature : array[0..31] of GTKChar;    // ������������
   RealCoord    : integer;         // ������� �������� ���������
   Scale        : integer;         // ����������� �������
   RecordCount  : integer;         // ���-�� �������
   MapType      : integer;         // ��� �����
 end;
  PINFOSXF = ^TINFOSXF;  // 07/10/2009

 TINFODIR  = packed record
  SheetName     : array[0..31] of GTKChar;    // ����
  NameRsc       : array[0..MAX_PATH-1] of GTKChar;     // �������������
  CountList     : integer;         // ���������� ������
 end;

 const
   AM_HEAD      = 1;        // �������� ���������� ������
   AM_METRIC    = 2;        // �������� ������� ��'����
   AM_SEMANTIC  = 4;        // �������� ���������
   AM_DRAW      = 8;        // �������� ����������� ��������
   AM_HEADREF   = 16;       // ���������� ������ �������� ������ � ����� ������� ������

 type
(* 01/10/2010
 TACTIONRECORD  = packed record// �������� ��������� ��������
                        // (��� AT_OPEN - ��� ������ 16 ����)
  Key     : integer;    // ���������� ����� ������� � �����
  Number  : integer;    // ���������������� ����� � ������ ���������
  Back    : integer;    // ����� ������ � ����� ������ (���������)
  OpType  : integer;    // ��� ��������
  Mask    : byte;       // ����� ������������ ��������� �������
  List    : shortint;   // ����� �����
 end;
*)
 TACTIONRECORD  = packed record// �������� ��������� �������� //01/10/2010
                        // (��� AT_OPEN - ��� ������ 16 ����)
  Key     : integer;    // ���������� ����� ������� � �����
  Number  : integer;    // ���������������� ����� � ������ ���������
  Back    : integer;    // ����� ������ � ����� ������ (���������)
  OpType  : byte;       // ��� ��������
  Mask    : byte;       // ����� ������������ ��������� �������
  List    : word;       // ����� �����
 end;
  PACTIONRECORD = ^TACTIONRECORD;  // 07/10/2009

 (* 01/10/2010
 TACTIONHEAD  = packed record   // ��������� �������� ����������
  Ident    : word;      // 0x7FFF
  Task     : shortint;  // ������������� ������,����������� ����������
  Task     : smint;  // ������������� ������,����������� ����������
  Count    : word;      // ����� �������� (Length = 16+Count*16)
  trType   : word;      // ��� ���������� (��� ������)
  trDate   : integer;   // ���� ���������� ����������
  trTime   : integer;   // ����� ���������� ����������
 end;
 *)
 // 01/10/2010
 TACTIONHEAD  = packed record   // ��������� �������� ����������
  Ident    : smallint;   // 0x7FFF
  Task     : word;      // ������������� ������,����������� ����������
  Count    : word;      // ����� �������� (Length = 16+Count*16)
  trType   : word;      // ��� ���������� (��� ������)
  trDate   : longword;   // ���� ���������� ����������
  trTime   : longword;   // ����� ���������� ����������
 end;


  PACTIONHEAD = ^TACTIONHEAD;  // 07/10/2009
 
//---------------------------------------------------------------------------
// �������� ������ � ini-����� ����� ��� ������ "3D-���������"
//---------------------------------------------------------------------------
 const
  IniFileSection  = 'MAP3D';

 type
{$IFDEF CPUX64}                                           // 11/12/2013
  HTASK3D = Int64;       // ������������� ������ ����������� �����������
                              // ������(�����, ������, �������)
{$ELSE}
  HTASK3D = Cardinal;    // ������������� ������ ����������� �����������
                              // ������(�����, ������, �������)
{$ENDIF}

//---------------------------------------------------------------------------
// ��������� �������� �������� ������ ��� ���������� 3D-������
//---------------------------------------------------------------------------
 TCREATETASK3D  = packed record

  SelectFrame : TMapDFrame;   // �������� ���������� ��� ���������� ������
                              // ������� ������ (� ������ �� ���������)
  Area        : integer;      // ������� ���������� ������:
                              //   0 - ���� �����
                              //   1 - ����� ������� ������ (SelectFrame)
                              //   2 - �� ������� ���� �����
  MoveAdjust  : integer;      // ������������� ����������� ����- � ���������� ����
                              //   0 - �� ����������������
                              //   1 - ���������������� �����������
                              //   2 - ���������������� ���������������
                              //   3 - ���������������� ����������� �
                              //       ���������������
                              // �������� ���������� wParam (notification code)
                              // ��������� ���� WM_COMMAND, ���������� �� ���� 3D-������
                              // ���� ���������, ������� ���������� ������ ���
                              // ������������� ������ 3D-������ � ����������� �������
                              // (��� ����, ���� ������������� �� �����)
  CmStop3d,                   // ��������� ������� � ����������� ��������
  CmMove3d,                   // ��������� � ����������� ������ ��������
                              // 3D-������ ��� �������� ������ �������������
                              // �����������
  CmSelectFrame3d,            // ��������� � ������������� ������ �����
                              // ������ ��� ���������� �� ��� ����� �����
                              // 3D-������
  CmSelectObject3d,           // ��������� � ������ ��� ������ ������ �������
                              // �� ���������� ������
  CmScale3d,                  // ��������� � ��������������� ���������� ������
  Update : integer;           // ���� ������������� ���������� ����������� ������ ������   // 11/12/12
                              // 0 - �� ���������,
                              // 1 - ������� ����������� ������ � ����������� ������
  MapWindowFrame : TMapDFrame;   // �������� ���c���, ������������� � ���� ���������� �����
                                 // (� ������ �� ���������)

  Reserve :array[0..215] of GTKchar;                                                      // 11/12/12
 end;
  PCreateTask3D = ^TCreateTask3D;  // 07/10/2009


 type
//------------------------------------------------------------------
//  ��������� �������� TIN-������
//------------------------------------------------------------------
  TTINBUILD  = packed record    // ������ ��������� 320 ����
    Length,             // ������ ������ ���������: sizeof(TINBUILD)
    UserLabel,          // ���������������� ������������� -
                        // ������������ �����, �����������
                        // � ����������� �������
    ModelType,          // ��� ������
                        //  ��������: 1 - ������ ������� ���������
                        //            2 - ������ ������
    Method : integer;   // ����� ���������� ������
                        //  ��������: 1 - � ��������������
                        //                ������������ ������
    FrameMeters : TDFrame;    // �������� ������ � ����� (�����)

    ModelRegister : TMAPREGISTEREX ;    // ���������� ������ ������
                      // ��������a MAPREGISTEREX ������a � MAPCREAT.H
                      // ��� �������� TIN-������ ������������
                      // �������� ��������� ����� :
                      //  Name - �������� ��� ������,
                      //  Scale - ����������� ��������,
                      //  EllipsoideKind - ��� ���������� (��������
                      //    � ������������ � ELLIPSOIDKIND, ���� MAPCREAT.H),
                      //  HeightSystem - ������� ����� (��������
                      //    � ������������ � HEIGHTSYSTEM, ���� MAPCREAT.H),
                      //  MaterialProjection - �������� ��������� ���������
                      //    (�������� � ������������ � MAPPROJECTION, ���� MAPCREAT.H),
                      //  CoordinateSystem - ������� ��������� (��������
                      //    � ������������ � COORDINATESYSTEM, ���� MAPCREAT.H),
                      //  MapType - ���������� ��� ����� (��������
                      //    � ������������ � MAPTYPE, ���� MAPCREAT.H),
                      //  FirstMainParallel - ������ ������� ���������,
                      //  SecondMainParallel - ������ ������� ���������,
                      //  AxisMeridian - ������ ��������,
                      //  MainPointParallel - ��������� ������� �����.

    Border : HObj;    // ������������� ���������� ������� �����,
                      // �� �������� ��������������� ����� �����������
                      // TIN-������ (���� ������ ���������� ��� ���������
                      // ������, ������������ ���������� ���� FrameMeters
                      // ������ ���������, �� ����� �� ���������������)

    Reserve : array[0..59] of GTKchar;    // ������ ���� ����
  end;
    PTINBUILD = ^TTINBUILD;  // 07/10/2009

//------------------------------------------------------------------
//  �������� �������� TIN-������
//------------------------------------------------------------------
  TTINDESCRIBE  = packed record      // ������ ��������� 1024 �����
   Name : array [0..259] of GTKchar;     // ��� ����� ������
   Length,              // ����� �����
   UserLabel,           // ���������������� �������������
   ModelType,           // ��� ������
   Method,              // ����� ���������� ������
   VertexCount,         // ���������� ������
   TriangleCount,       // ���������� �������������
   View    : integer;   // ������� ��������������

   FrameMeters : TDFrame;    // �������� ������ � ����� (�����)

   MinHeightValue : double;    // ����������� �������� ������ ����������� (�����)
   MaxHeightValue  : double;     // ������������ �������� ������ ����������� (�����)

   ModelRegister : TMAPREGISTEREX ;  // ���������� ������ ������
                      // ��������a MAPREGISTEREX ������a � MAPCREAT.H
                      // �������� TIN-������ �������� ��������
                      // ��������� ����� :
                      //  Name - �������� ��� ������,
                      //  Scale - ����������� ��������,
                      //  EllipsoideKind - ��� ����������,
                      //  HeightSystem - ������� �����,
                      //  MaterialProjection - �������� ��������� ���������,
                      //  CoordinateSystem - ������� ���������,
                      //  MapType - ���������� ��� �����,
                      //  FirstMainParallel - ������ ������� ���������,
                      //  SecondMainParallel - ������ ������� ���������,
                      //  AxisMeridian - ������ ��������,
                      //  MainPointParallel - ��������� ������� �����.

   ModelSquare : double;       // ������� ������ (��.�����)

   Reserve : array [0..471] of GTKchar;      // ������

end;
  PTINDESCRIBE = ^TTINDESCRIBE;  // 07/10/2009

TTINDESCRIBEUN  = packed record    // 03/04/14

  Name              : array [0..MAX_PATH_LONG] of PWCHAR; // ��� ����� ������

  FrameMeters       : TDFRAME  ;         // �������� ������ � ����� (�����)

  ModelRegister    : TMAPREGISTEREX;  // ���������� ������ ������
                      // ��������a MAPREGISTEREX ������a � MAPCREAT.H
                      // �������� TIN-������ �������� ��������
                      // ��������� ����� :
                      //  Name - �������� ��� ������,
                      //  Scale - ����������� ��������,
                      //  EllipsoideKind - ��� ����������,
                      //  HeightSystem - ������� �����,
                      //  MaterialProjection - �������� ��������� ���������,
                      //  CoordinateSystem - ������� ���������,
                      //  MapType - ���������� ��� �����,
                      //  FirstMainParallel - ������ ������� ���������,
                      //  SecondMainParallel - ������ ������� ���������,
                      //  AxisMeridian - ������ ��������,
                      //  MainPointParallel - ��������� ������� �����.

  MinHeightValue,   // ����������� �������� ������ ����������� (�����)
  MaxHeightValue,   // ������������ �������� ������ ����������� (�����)
  ModelSquare :double;      // ������� ������ (��.�����)

  Length,            // ����� �����
  UserLabel,         // ���������������� �������������
  ModelType,         // ��� ������
  Method,            // ����� ���������� ������
  VertexCount,       // ���������� ������
  TriangleCount,     // ���������� �������������
  View : integer;              // ������� ��������������

  Reserve    : array [0..127] of GtkChar;     // ������

end;
  PTINDESCRIBEUN = TTINDESCRIBEUN;



//------------------------------------------------------------------
// ��������� ������ ������� TIN-������
//------------------------------------------------------------------
 TTINVERTEXFLAG = packed record      // ������ ��������� 8 ����
   Cancel,              // ���� ��������
   PolygonBorder,       // ���� �������������� ���������
                        // ����� �������� ������������
   StructLine ,         // ���� �������������� ����������� �����
   FaultLine,           // ���� �������������� ����� �������
   FillingSquare : byte;// ���� �������������� ������� ������� ����������
   Reserve : array [0..2] of GTKchar;
 end;
   PTINVERTEXFLAG = ^TTINVERTEXFLAG;  // 07/10/2009

//------------------------------------------------------------------
// ��������� ������ ������� ������ TIN-������
//------------------------------------------------------------------
 TTINVERTEX = packed record     // ������ ��������� 40 ����
   X,                           // ���������� ������� � �������������
   Y,                           // ������� � �������� �������� (�����)
   H :double;
                                // ��� ������, �������� � ������ ����� �������
   UpperVertexNumber,           // ����� ������� �������� ������
   UnderVertexNumber : integer; // ����� ������� ������� ������

   Flag : TTINVERTEXFLAG;       // ����� �������
end;
  PTINVERTEX = ^TTINVERTEX;  // 07/10/2009

//------------------------------------------------------------------
// ��������� ������ ������������ TIN-������
//------------------------------------------------------------------
 TTINTRIANGLEFLAG = packed record     // ������ ��������� 16 ����
   Cancel    : byte;     // ���� ��������

   Location  : byte;     // ���� ��������� ������������ � ����
                         // (0 - ���������� ����������� ����,
                         //  1 - ������� ����������� ����)

   FillingSquare : byte; // ���� �������������� ������� ������������
                         // ������� ����������

   AB,                   // ���� ����� "AB"
   BC,                   // ���� ����� "BC"
   CA : byte;            // ���� ����� "CA"
                         // (0 - ����� �� ������ � ������
                         //      ����������� �����,
                         //  1 - ����� ������ � ������ �����������
                         //      ����� �������� ��������,
                         //  2 - ����� ������ � ������ �����������
                         //      ����� ���������� ��������,
                         //  3 - ����� ������ � ������ �����������
                         //      ����� �������,
                         //  4 - ����� ������ � ������ �������
                         //      �������� ���������� ������������)
  Reserve : array [0..9] of GTKchar;

end;
  PTINTRIANGLEFLAG = ^TTINTRIANGLEFLAG;  // 07/10/2009
  
//------------------------------------------------------------------
// ��������� ������ ������� ������������� TIN-������
//------------------------------------------------------------------
 TTINTRIANGLE = packed record    // ������ ��������� 40 ����
  A,            // ����� ������� "A" � ������� ������
  B,            // ����� ������� "B" � ������� ������
  C,            // ����� ������� "C" � ������� ������

  AB,           // ����� ��������� ������������ �� ����� "AB"
                // � ������� �������������
  BC,           // ����� ��������� ������������ �� ����� "BC"
                // � ������� �������������
  CA : integer; // ����� ��������� ������������ �� ����� "CA"
                // � ������� �������������
                // (���� ��������� ������������ ���, ����� ����� -1)
  Flag : TTINTRIANGLEFLAG ;  // ����� ������������
end;
  PTINTRIANGLE = ^TTINTRIANGLE;  // 07/10/2009

 // � GISMRTEX api
 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++  ��������������� ���������  ++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 // ���������� ��������������� ������ �������������
 // ����������� �������


  TPRIORMTRPARM = packed record
   Length : integer;               // ������ ������ ��������� :
                                   //  sizeof (PRIORMTRPARM) = 64
   Free   : integer;               // ������ ���� ����
   AbsHeightDifference : double;   // ������ ��������� ����� - ����������
                                   // �������� �������� ����� ��������
                                   // ���������� � �������� ������� (�����)
   Reserve : array [0..5] of double;          // ������ ���� ����
  end;


type
{ TALSITEM = packed record                // �������� �������� ������ ������� �����
  Ident         : longint;             // ������������� ��������
  Path          : array[0..259]of GTKchar; // ������ ���� � �������� ������
  Name          : array[0..31]of GTKchar;  // ��� ������ (������� �� ��������)
  Scale         : longint;              // ������� ������ (������� �� ��������)
  Minimum       : longint;              // ������ ������� �������� � ������ (��������, 100 000)
  Maximum       : longint;              // ������� ������� �������� � ������ (��������, 500 000)
  Priority      : longint;              // ��������� ������ ��� ���������� ���������
 end;
 PALSITEM = ^TALSITEM;  // 07/10/2009}

 TALSITEM = packed record  // �������� �������� ������ ������� �����
    Ident    : longint;        // ������������� ��������
    Path     : array[1..MAX_PATH]of GTKchar; // ������ ���� � �������� ������ [260]
    Name     : array[1..32]of GTKchar;       // ��� ������ (������� �� ��������)
    Scale    : longint;        // ������� ������ (������� �� ��������)
    Minimum  : longint;        // ������ ������� �������� � ������ (��������, 100 000)
    Maximum  : longint;        // ������� ������� �������� � ������ (��������, 500 000)
    Priority : longint;        // ��������� ������ ��� ���������� ���������
   end;

 PALSITEM = ^TALSITEM;  // 15/10/2009

 TALSITEMEX = packed record   // �������� �������� ������ ������� �����                                                                   // 21/11/06
  Ident         : cardinal;             // ������������� ��������         // 21/11/06
  Scale         : integer;              // ������� ������ (������� �� ��������)
  Minimum       : integer;              // ������ ������� �������� � ������ (��������, 100 000)
  Maximum       : integer;              // ������� ������� �������� � ������ (��������, 500 000)
  Priority      : integer;              // ��������� ������ ��� ���������� ���������
  Reserve       : integer;              // ������ = 0
 end;
  PALSITEMEX = ^TALSITEMEX;   // 07/10/2009

const
  WM_RSTROTATE     =  $0500;       // ������� ������
  RSTTRANS_DIAL    =  $0501;       // ����� ������� ������-� ������
  STTRANS_NEWPARM  =  $0502;       // ���������� ���������� ���.������
  RSTTRANS_END     =  $0503;       // ���������� ������

// ������ ��������� ��������, ����������� ����� ����� (������� ������)

  SEMALSPATH   = 7701;  // ���� � �����
  SEMALSNAME   = 7702;  // ��� ������ �����
  SEMALSSCALE  = 7703;  // ������� ������ �����
  SEMALSMIN    = 7704;  // ������ ������� ��������
  SEMALSMAX    = 7705;  // ������� ������� ��������
  SEMALSMODE   = 7706;  // ����� ��������� ������
  SEMALSPRIOR  = 7707;  // ��������� ������
  SEMALSGROUP  = 7708;  // ��� ������ ������ ������     // 28/10/09
  SEMALSNOTE   = 7709;  // ����������                   // 27/11/09
  SEMALSORDER  = 7710;  // ������� ����������                   // 05/04/10
  SEMALSPATHTILE = 7711;  // ���� � �������� �������������� ����� // 05/04/10
  SEMALSCOUNT  = 7712;  // ����� ��������� � ������ (SEMGROUPLEADER + SEMGROUPSLAVE) // 31/10/11
  SEMALSSHEET  = 7713;  // ���/������������ �����       // 31/10/11


  ALS_UP       =  1;  // ����� ����� ������� ��������
  ALS_DOWN     =  2;  // ����� ����� �������� ��������
  ALS_ANY      =  0;  // ����� ����� �����
  ALS_BACK     =  4;  // ��������� � �����
 
 // ����� ������ ������ "���������� �����"

  SAP_SAVEONLY    = -2;  // ���������� ����� ��� ������ �������
                        //
  SAP_INITPARM    = -1; // ����� ������������� ���������� ����������
                        //  (������ "�������" � "��������" ���������)
  SAP_SAVEALL     =  0;  // ���������� ����� � �������������� �����������
                        //  ������� ��� �������� ������� ���������� �����
  SAP_SAVE        =  1;  // ���������� ����� � ������������� �����������
                        //  �������� (RectMetr)
  SAP_SELECTFRAME =  2; // ����� ������ ��������� ����������� �������
                        //
  SAP_CHANGEFRAME =  3; // ����� ��������� ��������� �����������
                        //  �������
  SAP_MOVEFRAME   =  4; // ����� ������ ��������� ����������� �������     // 23/08/01
  SAP_SELECTBYOBJECT = 5;// ����� ������������ ��������� ����������� ������� ��
                        //  ���������� �������
  SAP_SELECTBYRASTERS= 6;// ����� ������������ ��������� ����������� ������� ��
                        //  ��������� �������� �������               // 08/04/14   // 23/06/2016

  // ��������, ����������� ����� Regime ��� ������� �� ������:
  // "���������" - SAP_SAVE,
  // "�������"   - SAP_SELECTFRAME,
  // "��������"  - SAP_CHANGEFRAME,
  // "���������" - SAP_MOVEFRAME,
  // "�� �������"- SAP_SELECTBYOBJECT
  // ����� ������, ��������� ��� ����������� ������� �����
  // ������� ������ �������� �� ������� �������� ���� Regime.

  SAP_NORMALRASTER= 1;  // ����������(���������)
  SAP_TRANSPARENTRASTER= 2;  // ����������(���������)

  SAP_NORMAL      = 3;  // ����������
  SAP_TRANSPARENT = 4;  // ����������
  SAP_CONTOUR     = 5;  // ���������

 
// ������ ������� �������� ������������
// �� ��������� ����������� �������� �����
const
// ��������� � �������� ����������� �����
  IDC_PERCENT_DELONE      = $3000;
  TR_ONLY          = 0;
  TR_THICKEN       = 1;
  TR_FACTORIZATION = 2;

// ����������� ������ ������� ������
 // VERTEXCOUNT     = 50000;  // 29/12/05
 VERTEXCOUNT     = 1000000;  // 04/04/2014

// ����������� ������ ������� �������������
// TRIANGLECOUNT  = 150000;  // 29/12/05
  TRIANGLECOUNT = 3000000;  // 04/04/2014
// ��������� ������� �������������   (56 ����)
 type


 TTRIANGLE  = packed record
  A,B,C                 : integer;  // ������ ������ ������������
  EdgeAB,EdgeBC,EdgeCA  : integer;  // ������ ���� ������������
  NearAB,NearBC,NearCA  : integer;  // ������ �������� �������������
  ParentAB,             // �������������� ������� ������� ������������ (1-��, 0-���)
  ParentBC,
  ParentCA              : integer;
  Cancel                : integer;   // ���� ��������
  Reserve               : integer;
 end;
  PTRIANGLE = ^TTRIANGLE;  // 07/10/2009


// ��������� ������� �����   (32 ����a)

 TEDGE  = packed record
  A,B        : integer;    // ������ ������ �����
  LTri,                    // ����� ������ ������������
  RTri       : integer;    // ����� ������� ������������
  Live       : integer;    // ���� - "�����" ����� (�� ��� �������� ���� �������)
  Cancel     : integer;    // ���� ��������
  Flag       : integer;    // ���� :
                    //  0 - ����� �� ������ � ������
                    //      ����������� �����,
                    //  1 - ����� ������ � ������ �����������
                    //      ����� �������� ��������,
                    //  2 - ����� ������ � ������ �����������
                    //      ����� ���������� ��������,
                    //  3 - ����� ������ � ������ �����������
                    //      ����� �������,
                    //  4 - ����� ������ � ������ �������
                    //      �������� ���������� ������������.
  Reserve : integer;
 end;
  PEDGE = ^TEDGE;  // 07/10/2009

// ��������� ������� ������   (32 ����a)

 TVertex = packed record
  X,Y,H    : double;
  Cancel   : integer;   // ���� ��������
  Flag     : integer;   // ���� : 0 - ������� ������� (�������� ������)
                        //        1 - ������� ����������� ����������� �����
                        //        2 - ������� ����������� ������� ������� ����������
 end;
  PVertex = ^TVertex;  // 07/10/2009

 // �������� ����� �������� � �������������
{$INCLUDE Mapgdi.inc}


 // ---------------- ��� GisToolKit  --------------------
 const
   NAME_LoadImageToRstEx =  'LoadImageToRstEx';
   NAME_LoadRstMirrorDialog =  'LoadRstMirrorDialog';
   NAME_LoadRstCompressDialog =  'LoadRstCompressDialog';
 type
   Dll_LoadImageToRstEx = function (aMap : HMAP; lpszsource,lpsztarget : GTKPChar;
    var Parm : TTASKPARM) : integer;
      {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}; //14/09/2009

   DLL_LoadRstMirrorDialog  =  function(aMap : HMAP; Name : GTKPChar;
    var Parm : TTASKPARM) : integer;
      {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}; //14/09/2009

   DLL_LoadRstCompressDialog  = function (aMap : HMAP; Handle : HWnd; Name : GTKPChar;
    var Parm : TTASKPARM; var reDraw : integer) : integer;
      {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}; //14/09/2009

  DLL_formCheckAndSelectObject = function (Obj : HObj; var Frame : TMAPDFRAME;
   Place : integer; var ObjForm : TOBJECTFORM; var Parm : TTASKPARM) : integer;
      {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}; //14/09/2009

  DLL_formSelectAssignObject =  function (Obj : HObj; var ObjForm : TOBJECTFORM;
   var Parm : TTASKPARM) : integer;
      {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}; //14/09/2009

  Dll_formSeekObject  =  function (Obj : HObj; Flag : integer; Select : HSelect;
    var ObjForm : TOBJECTFORM; var Parm : TTASKPARM) : integer;
      {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}; //14/09/2009

  Dll_LoadS57ToMapForRsc = function(Source,target,rscname : GTKPChar;
     var parm : TTaskParm) : integer;
      {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}; //14/09/2009

  Dll_rscCreate  = function (Name : GTKPChar; var Parm : TTASKPARM) : integer;
      {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}; //14/09/2009

  Dll_rscEditForObject = function (map : HMap; var Parm : TTASKPARM;
   mapnumber,layernumber,objectnumber : integer) : integer;
      {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}; //14/09/2009

  DLL_rscOpen   = function (Name : GTKPChar; var Parm : TTASKPARM) : integer;
      {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}; //14/09/2009


// ������� �������      // 22/09/2009
type
  TScaleRange = packed record
    bottom : longint;
    top    : longint;
    end;

type
  TLPTSTR = GTKPChar; // 02/10/2009 20/10/2009


// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++ ���������� � ��������� �� ���� �����  ++++++++++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 TCHANGECALL = function(parm : Pointer; value: Pointer) : integer of Object;

type
 TCHANGEINFO = packed record
    aSite    : HSITE;     // ������������� ����� � ���������
// 11/12/2013    List     : integer;     // ����� ����� �����      // � maptype.h - 2 ����� (short int)
    List     : smallint;     // ����� ����� �����                 // 11/12/2013
    aType    : GTKChar; // ��� �������� (OBJECT_OPERATION)
    aMask    : GTKChar; // ����� ������������ ��������� ������� (ACTION_MASK)
    Key      : integer;      // ���������� ����� ������� � �����
    aObject  : integer;   // ���������������� ����� ������� � �����
 end;

// ��������� ���������� �������    // 18/04/2010

type
 PROFBUILDPARM = packed record
    Point              : TDOUBLEPOINT; // ���������� ������� ����� �� �������
    ObjectMap          : HOBJ;         // ������ ��� ���������� �������
    ProfStepVertical   : integer;      // ��� ����� �� ��������� (�)
    ProfStepHorizontal : integer;      // ��� ����� �� ����������� (�)
 end;
  PPROFBUILDPARM = ^PROFBUILDPARM;

 // ��������� ���������� �������    // 23/04/10
 PROFBUILDPARMEX = packed record
    Point              : TDOUBLEPOINT; // ���������� ������� ����� �� �������
    ObjectMap          : HOBJ;         // ������ ��� ���������� �������
    ProfStepVertical   : integer;      // ��� ����� �� ��������� (�)
    ProfStepHorizontal : integer;      // ��� ����� �� ����������� (�)
    DeltaRight         : double;       // ���������� � �������� �����
    DeltaCurrent       : double;       // ���������� � ������� �����
    DeltaLeft          : double;       // ���������� � ��������� �����
    ColorProf          : integer;      // ���� �������
    ColorLine          : integer;      // ���� �����
    IsCurvatureEarth   : integer;      // ���� ����� ��������  �����  0 - �� ���������,
                                       //                             1 - ���������;
    IsMiddleHeight     : integer;      // ���� ����� ���������� ����� 0 - �� ���������,
                                       //                             1 - ���������;
    IsLineFL           : integer;      // ���� ����������� ����� (������ - ��������� �����)
                                       // 0 - ����������,   1 - �� ����������;
    IsLineFCL          : integer;      // ���� ����������� ����� (������ - �������- ��������� �����)
                                       // 0 - ����������,   1 - �� ����������;
    IsLineCross        : integer;      // ���� ����������� ����� (����������� �� ������� �����)
                                       // 0 - ����������,   1 - �� ����������;
    IsLineNet          : integer;      // ���� ����������� ����� �������
                                       // 0 - ����������,   1 - �� ����������;
    IsLineRelief       : integer;      // ���� ����������� �������
                                       // 0 - �������,   1 - ������;
 end;
  PPROFBUILDPARMEX = ^PROFBUILDPARMEX;

// ��������� ���������� ���� ����������    // 27/08/2010
TBUILDZONEFLOODPARM = packed record
   Length       : longint;      // ����� ������ ��������� TBUILDZONEFLOODPARM (144)
{$IFDEF CPUX64}                                                                 // 23/06/2016
   Free         : integer;      // ������ ���� ����// 23/06/2016
{$ENDIF}   
   
   Handle       : HWND;         // ������������� ���� �������, �������� ����������
                                // ��������� 0x0590 (WM_PROGRESSBAR) � �������� ����������� ����� (� WPARAM),
                                // ���� ������� ������ ���� ������������� ��������, � �����
                                // ������ ��������� �������� 0x0590.
                                // ���� Handle ����� ���� - ��������� �� ����������.
   Point1       : TDoublePoint; // ���������� ������ �����, � ������� ������ ������� ������� ����
   Point2       : TDoublePoint; // ���������� ������ �����, � ������� ������ ������� ������� ����
                                // ���������� �������� � ������ � ������� ��������� ��������� �����
   Height1      : double;       // ������� ������� ���� ������ �����
   Height2      : double;       // ������� ������� ���� ������ �����
   Width        : double;       // ������ ���� ����������
   ElementSize  : double;       // ������ ������� �������� ������� � ������ �� ���������
   hObject      : HOBJ;         // ������ (����), �� ������� ����������� ����� ������� ���� // 02/09/2010
   Active       : longint;      // 0 - �� ��������� ������� ���� ����������
                                // 1 - ���������, ������ ������������
                                // 2 - ���������, �� ������ ������������
{$IFDEF CPUX64}                                                                 // 23/06/2016
   Free2        : integer;      // ������ ���� ����// 23/06/2016
{$ENDIF}

   Reserve      : array[0..63] of GTKchar;  // ������ ���� ����
end;
PBUILDZONEFLOODPARM = ^TBUILDZONEFLOODPARM;

//********************************************************************
// ���������� ������� ��������� (������� ������� �������� ���������� �����,    // 23/06/2016
// �������� � �������� ������)
//********************************************************************

// ��������� ������ ������ ���������� ������� ���������

TBUILDDENSITY = packed record

      Radius,       // ������ ������ ����� � ������ ��� ���������� �������� ��������
      ElemSize : double;     // ������ �������� ����������� ������� � ������
      Palette  : array [0..255] of TCOLORREF ; // ������� �������
      PaletteCount : integer; // ���������� ������ � ������� (���� = 0, �� ��������������� �����������)
      Reserved : array [0..62] of integer;
end;
PBUILDDENSITY = ^TBUILDDENSITY;

//********************************************************************
// ���������� ������� ��� ��������� (������� ������� ����� �������� 1, � ������   // 23/06/2016
// ���� � ���� ����� ���� �� ���� �������� �����, � ��������� ������ 0)
//********************************************************************

// ��������� ������ ������ ���������� ������� ��� ���������

TBUILDVISIBLE  = packed record
      Radius,       // ���������� ������ ���������
      ElemSize : double;// ������ �������� ����������� ������� � ������
      Color   : TCOLORREF;    // ����, ������� ������������ ��������, � ������� ����� �����
      Reserved : array [0..62] of integer;
end;
PBUILDVISIBLE = ^TBUILDVISIBLE;



// ----------------------------------------------------------------
//  �������������� ������ (WM_COMMAND)
// ----------------------------------------------------------------
const
  WM_ERRORCOORD   = $583;    // ���������� � ������� ��������
  WM_INFOLIST     = $584;    // ���������� � �����
  WM_OBJECT       = $585;    // ����� �������
  WM_LIST         = $586;    // ����� �����
  WM_ERROR        = $587;    // ���������� �� ������� ��� ������ � ���-����
  WM_MAP          = $588;    // ����� ������� �����
  WM_ERRORSXF     = $589;    // ���������� �� ������� ��������� SXF
  
// ----------------------------------------------------------------
//  �������������� ���������
// ----------------------------------------------------------------  
  WM_PROGRESSBAR  = $590;    // ��������� � ��c������ ��������
                                 // wparam - (-1 - �����), (-2 - ����), % ���������� 
                                 // lparam - ����������� (����� � ��������� ���������� ANSI \ UNICODE)
                                 // ���� ����������� � % ���������� �� �����, �� ������� ESC �� �������� � ������� � ���������� ��������
                                 // ��� ���������� ���������� WM_PROGRESSBAR

  WM_MAPEVENT     = $591;    // ��������� � �������� �����
  WM_PROGRESSICON = $592;    // ��������� � ��c������ ��������
                             // ��� ����������� � ������ ���������
                             // ��� ��������� ������� ����
  WM_PROGRESSBARUN= $593;     // ��������� � ��c������ �������� 
                                 // wparam - (-1 - �����), (-2 - ����), % ���������� 
                                 // lparam - ����������� (����� � ��������� UNICODE)
                                 // ���� ����������� � % ���������� �� �����, �� ������� ESC �� �������� � ������� � ���������� ��������
                                 // ��� ���������� ���������� WM_PROGRESSBAR
 

// ----------------------------------------------------------------
//  �������������� ��������� (MESSAGE)
// ----------------------------------------------------------------

   WM_LEFTTOPPOINT = $600;    // C�������� ���� ����� �� ������
                              // ������ ��������� �������� ������
                              // ���� ���� ����� � �������� �����������
                              // wparam : (POINT * point)         //28/11/02
                              // lparam : 0
                              // result : 0x600

   WM_MOVEDOC      = $601;    // C�������� ���� ����� �� ������
                              // �������� �� �������������
                              // ����������� ����������� ����� � ��������
                              // �����, ���������� �������� ������ ���� ����
                              // ����� � �������� �����������
                              // wparam : (POINT * point)         //28/11/02
                              // lparam : 0 ��� (POINT * marker)  //28/11/02
                              // ���������� �������,������������ �� �����������
                              // result : 0x601

   WM_OPENDOC      = $602;    // C�������� ���� ���������� ��� ����� �� ������
                              // �������� �� �������������
                              // ������� ����� �� ����� ����� (MAP,MTW,RSW,...)
                              // wparam : (const char * name)
                              // lparam : 0 ��� (DOUBLEPOINT * point),
                              // ���������� � ������ �����, ������� �����
                              // �������� � ������ ���� �����
                              // result : 0x602

   AW_CLOSEDIALOGNOTIFY = $610; // C�������� �������� ���� � �������� �������
                                // �������� �� ������������� ������� �������
                                // �������� ������� �� ���������
                                // wparam : 0                        //21/04/10
                                // lparam : ��������� �������
                                // result : 0x610

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ��������� ����������                                      // 26/02/10
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
type
TELLIPSOIDPARAM = packed record
  SemiMajorAxis         : double;  // ����� ������� ������� ����������
  InverseFlattening     : double;  // �������� ������ ����������
end;

PELLIPSOIDPARAM = ^TELLIPSOIDPARAM;

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ������������ ����������������� ������������� ���������    // 26/02/10
// ��� �������� �� ��������� ���������� � ���������� WGS-84
// ���� ������ 3 ���������, �� ����������� ��������������
// ������������ (Standard Molodensky Transformations),
// ���� (6+1) - �������������� �� ���� � 51794 - 2007
// (�������� �������������� ���������, ��� Coordinate Frame Rotation;
// EPSG dataset coordinate operation method code 1032)
// ������ �������� ����� � DATUMPARAM:
// 23.92; -141.27; -80.9; 0; -0.35; -0.82; -0.12E-6; 7; 0
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

TDATUMPARAM = packed record
  DX     : double;      // ������ �� ���� � ������
  DY     : double;
  DZ     : double;
  RX     : double;      // ������� �������� � ��������
  RY     : double;
  RZ     : double;
  M      : double;      // �������� ��������
  Count  : integer;         // 3 ��� 7  (14 - ������� ��������� ����� ��-90.02 ��� ��42\95)
  Reserve: integer;         // ����� 0
end;
PDATUMPARAM = ^TDATUMPARAM;

// ��-90.02  ->  WGS-84
const

 DX_SGS85_WGS84   = '-0.36L';
 DY_SGS85_WGS84   = '+0.08L';
 DZ_SGS85_WGS84 = '+0.18L';
 RX_SGS85_WGS84 = '0.0L';
 RY_SGS85_WGS84 = '0.0L';
 RZ_SGS85_WGS84 = '0.0L';
 M_SGS85_WGS84 = '0';


// ��-90.11  ->  WGS-84
 DX_SGS85_11_WGS84  =  '0.013';
 DY_SGS85_11_WGS84  = '-0.106';
 DZ_SGS85_11_WGS84  = '-0.022';
 RX_SGS85_11_WGS84  =  '0.0023';
 RY_SGS85_11_WGS84  = '-0.00354';
 RZ_SGS85_11_WGS84  =  '0.00421';
 M_SGS85_11_WGS84   = '0.000000008';

// ���-2011 ->  WGS-84
 DX_GCK2011_WGS84   =  '0.013';
 DY_GCK2011_WGS84   = '-0.92';
 DZ_GCK2011_WGS84   = '-0.03';
 RX_GCK2011_WGS84   =  '0.001738';
 RY_GCK2011_WGS84   = '-0.003559';
 RZ_GCK2011_WGS84   =  '0.004263';
 M_GCK2011_WGS84    = '0.0000000074';

// ��-95  ->  ��-90.02  (Count = 14)
 DX_S95_SGS85 = '24.83l';
 DY_S95_SGS85 = '-130.97l';
 DZ_S95_SGS85 = '-81.74l';
 RX_S95_SGS85 = '0.0';
 RY_S95_SGS85 = '0.0';
 RZ_S95_SGS85 = '-0.13';
 M_S95_SGS85 = '-0.00000022L';

// ��-42  ->  ��-90.02  (Count = 14)
 DX_S42_SGS85 = '23.93L';
 DY_S42_SGS85 = '-141.03L';
 DZ_S42_SGS85 = '-79.98L';
 RX_S42_SGS85 = '0.0L';
 RY_S42_SGS85 = '-0.35L';
 RZ_S42_SGS85 = '-0.79L';
 M_S42_SGS85 = '-0.00000022L';

// ��-95  ->  WGS-84 (Count = 7)
 DX_S95_WGS84      =  '24.47l';
 DY_S95_WGS84      ='-130.89l';
 DZ_S95_WGS84      = '-81.56l';
 RX_S95_WGS84      =   '0.0';
 RY_S95_WGS84      =   '0.0';
 RZ_S95_WGS84      =  '-0.13';
 M_S95_WGS84       = '-0.00000022L';

// ��-42  -> WGS-84  (Count = 7)
 DX_S42_WGS84      =  '23.57';
 DY_S42_WGS84      ='-140.95';
 DZ_S42_WGS84      = '-79.8';
 RX_S42_WGS84      =   '0.0';
 RY_S42_WGS84      =  '-0.35';
 RZ_S42_WGS84      =  '-0.79';
  M_S42_WGS84      =  '-0.00000022L';

// ��������� ���������� �������� ������� ������� ����������
type
  TSPREADPARM = packed record
    Length        : integer;                         // ����� ������ ��������� SPREADPARM
{$IFDEF CPUX64}                                                                 // 23/06/2016
    Free          : integer;                         // ������ ���� ����// 23/06/2016
{$ENDIF}

    WndHandle     : HWND;                            // ������������� ���� �������, �������� ����������
                                                     // ��������� 0x0581 � �������� ����������� ����� (� WPARAM),
                                                     // ���� ������� ������ ���� ������������� ��������, � �����
                                                     // ������ ��������� �������� 0x0581.
                                                     // ���� Handle ����� ���� - ��������� �� ����������.
    IterCount     : integer;                         // ���������� �������� (��� ������ ��������, ��� ������ ����������� ���)
    Palette       : array [0..255] of  TCOLORREF ;   // ������� ����������� ������� �������
    PaletteCount  : integer;                         // ���������� ������ � �������
    IsIterMtq     : integer;                         // ������� �������� ������ �� ������ ��������
    IsAllEject    : integer;                         // ������� �������������� ������� (���� 0, �� ���� ����������� �� ����� ��������)
    EjectSize     : double;                          // ����� ������� �������� � ������ �����.
    AbsorbHeight  : double;                          // ������� ������, ����������� � ����� � ������
    ElemSize      : double;                          // ������ �������� ����������� ������� � ������
    WaveHeight    : double;                          // ������ �����
    Point         : TDoublePoint;                    // ����� �������
    Reserve       : array [0..63] of GTKCHAR;           // ������ ���� ����
  end;
  PSPREADPARM = ^TSPREADPARM;


// ��������� ������ ������ �������� ������� �����-��������� ���������    // 22/05/09
TWZONEPARM = packed record
   Length       : integer;      // ����� ������ ��������� WZONEPARM
{$IFDEF CPUX64}                                                                 // 23/06/2016
   Free         : integer;      // ������ ���� ����// 23/06/2016
{$ENDIF}
   Wnd          : HMESSAGE;     // ������������� ���� �������, �������� ����������
                                // ��������� 0x0581 � �������� ����������� ����� (� WPARAM),
                                // ���� ������� ������ ���� ������������� ��������, � �����
                                // ������ ��������� �������� 0x0581.
                                // ���� Handle ����� ���� - ��������� �� ����������.
   Map          : HMAP;         // ������������� �������� �������� �����
   Select       : HSELECT;      // ����� ��������, ��� ������� ����������� ����������
   Frame        : TDFRAME;      // ������� ����������
   ElemSize     : double;       // ������ �������� � ������
   dH           : double;       // ������ �� ������ ��� ���������� ������������� ������� �����
   Reserved     : array [0..63] of GTKCHAR;
end;

PWZONEPARM = ^TWZONEPARM;

// ��������� ���������� ������� ������ (mtq)
TDEPTHMTQPARM = packed record
  StructSize      : cardinal;   // ������ ������ ��������� : sizeof (DEPTHMTQPARM) = 256 ����
  Free            : integer;
  MatrixType      : integer;    // ��� �������� ������� �������:
                                //  1 - ������� ����� (mtw)
                                //  2 - ������� ������� (mtq)
  MatrixNumber    : integer;    // ����� ������� � �������
  BeginX          :double;      // ������������� ���������� ������
  BeginY          :double;      // (���-��������� ����) ������� � ������
  Width           :double;      // ������ ������� � ������
  Height          :double;      // ������ ������� � ������
  MinValue        :double;      // �������� �������� ������ ����������� �������,
  MaxValue        :double;      // ���� MinValue >= MaxValue � ������� ���������
                                // ����������� �������� ��������
  Level           :double;      // ���������� ������ �������������� �����������
                                // ��� ���������� ������, ��������� � �������

  ElemSizeMeters  :double;      // ������ ������� �������� ������� ������
                                // � ������ �� ��������� (������� �������)

  UserLabel       : integer;    // ������������ �����, ����������� � ����������� ��������
                                //  (���, �������������� �������)
{$IFDEF CPUX64}                                                                 // 23/06/2016
  Free1           : integer;    // ������ ���� ����// 23/06/2016
{$ENDIF}

  Handle          :HWND;        // ������������� ���� �������, �������� ����������
                                // ��������� 0x0581 � �������� ����������� ����� (� WPARAM),
                                // ���� ������� ������ ���� ������������� ��������, � �����
                                // ������ ��������� �������� 0x0581.
                                // ���� Handle ����� ���� - ��������� �� ����������.

  Border          :HOBJ;        // ������������� ���������� ������� �����, ��������������� �������
                                // ����������� ��������� ������� (���� ����� ����, �� �� ������������)
                                // ���� ������ ���������� ��� ��������� �������, ������������ ����������
                                // ����� BeginX, BeginY, Width, Height ������ ���������, �� �� ������������

{$IFDEF CPUX64}                                                                 // 23/06/2016
  Free2           : integer;    // ������ ���� ����// 23/06/2016
{$ENDIF}

  Reserve         : array [0..127] of GTKCHAR;

  UserName        : array [0..31] of GTKCHAR;  // �������� ��� ������� (�������� �������������� ��������)

end;
 PDEPTHMTQPARM = ^TDEPTHMTQPARM;


//------------------------------------------------------------------------------
// ��������� ���������� ���������� �� �������
//------------------------------------------------------------------------------
TCALCMATRIXPARM = packed record
 StructSize     : cardinal;         // ������ ������ ��������� : sizeof (CALCMATRIXPARM) = 264 ����
 Free           : integer;

 MatrixType     : integer;      // ��� �������� �������:
                                //  1 - ������� ����� (mtw)
                                //  2 - ������� ������� (mtq)

 MatrixNumber   : integer;      // ����� ������� � �������

 BeginX         :double ;       // ������������� ���������� ������
 BeginY         :double ;       // (���-��������� ����) ������� ���������� � ������

 Width          :double ;       // ������ ������� ���������� � ������
 Height         :double ;       // ������ ������� ���������� � ������ ( ���� Width ����� ����
                                // ��� Height ����� ����, �� ������� ���������� ������������
                                // ���������� �������� �������
 MinValue       :double ;       // �������� �������� ��������� �������, ����������� � �����������.
 MaxValue       :double ;       // ���� MinValue >= MaxValue � ����������� ��������� ��� ��������

 ElemSizeMeters :double ;       // ������� ���������� � ������
                                // (���� ����� ����, �� ������������ ������� �������)

 Level          :double ;       // ������ ������ �������������� ���������(���������) ��� ���������� ������
                                //  (������������ ��� CalcVolume == 1)

 WndHandle      : HWND;         // ������������� ���� �������, �������� ����������
                                // ��������� 0x0581 � �������� ����������� ����� (� WPARAM),
                                // ���� ������� ������ ���� ������������� ��������, � �����
                                // ������ ��������� �������� 0x0581.
                                // ���� Handle ����� ���� - ��������� �� ����������.

 Border         :HOBJ;          // ������������� ���������� ������� �����, ��������������� �������
                                // ���������� ��� ���������� ��������� ������� (���� ����� ����,
                                // �� �� ������������). ���� ������ ���������� ��� ��������� �������
                                // ����������, ������������ ���������� �����
                                // BeginX, BeginY, Width, Height ������ ���������, �� �� ������������.

 // ���� (������) ����������:
 //---------------------------------------------
 // ���������� �������
 CalcSquare     : byte;

 // ���������� �����
 CalcVolume     : byte;

 // ���������� ����������� ��������               02/06/10
 CalcMinimun    : byte;

 // ���������� ������������ ��������
 CalcMaximun    : byte;

 // ���������� �������������������� ��������
 CalcAverage    : byte;

 // ���������� ������� ������� ������� (�������������� ���������      30/07/10
 // � ���������� ������� LevelWater) � ������� ���������� - SquareShallowWater
 CalcSquareWater: byte;

 // ���������� ����� ���� ����� �������� LayerTop (�������) � LayerBottom (������)  30/07/10
 CalcVolumeLayer: byte;

 // ������ ��� ����� ����������
 CalcTypeReserve: byte;
 //---------------------------------------------


 // ���������� ����������:
 //-----------------------------------------------------------
  Square         : double;      // �������
  Volume         : double;      // �����

  Minimun        : double;      // ����������� ��������                        02/06/10
  Maximun        : double;      // ������������ ��������
  Average        : double;      // �������������������� ��������

  SquareWater    : double;      // ������� ������� ������� ������ LevelWater   30/07/10

                                // ������� ���������� ������ LevelWater        30/07/10
                                // ����������:   2 � >= ������� >= 1 ��
  SquareShallowWater : double;

  VolumeLayer    : double;      // ����� ���� ����� �������� LayerTop � LayerBottom  30/07/10
 //-----------------------------------------------------------

 LevelWater     :double ;       // ������ ������ �������������� ��������� ������� �������
                                //  (������������ ��� CalcSquareWater == 1)    30/07/10

 LayerTop       :double ;       // ������ �������� ������ ����
                                //  (������������ ��� CalcVolumeLayer == 1)    30/07/10

 LayerBottom    :double ;       // ������ ������� ������ ����
                                //  (������������ ��� CalcVolumeLayer == 1)    30/07/10

 Reserve        : array [0..79] of GTKCHAR;
end;

PCALCMATRIXPARM = ^TCALCMATRIXPARM;


// ��������� ��� ������ ���������� �� ���������� ��������    // 05/05/09
TSELECTOBJLIST = packed record
  Caption       : GTKPCHAR;           // ��������� �������
  PluralFlag    : longint;          // 1 - ������������� �����
  Left          : longint;                // �����
  Top           : longint;                 // ������� ���� �������
  Height        : longint;              // ������
  Width         : longint;               // ������ ������� (���� ������ ������ �������� - �� �����������)
  FirstWidth    : longint;          // ������ ������ �������,���������� �������� ������� ��-��������������
                       // (�������) ������ = 0 - ������� �� �������, ������ = -1 �����������
  SemCount      : longint;            // ���������� ��������� �������� (�� ����� 10)
  SemCode       : plongint;           // ��������� �� ������ ����� ��������
  ColWidth      : plongint;          // ��������� �� ������ ����� ��������
                            // (�������) ������ = 0 - �� �������, ������ = -1 �����������
                            // ���� ColWidth == 0, ������ ������� ��������������
end;

PSELECTOBJLIST = ^TSELECTOBJLIST;

// ��������� � ������ �������� �����
TSELECTOBJINFO = packed record
  aMap          : HMAP;         // ������������� �������� �����
  aSITE         : HSite;        // ������������� �������� ���������������� �����
  ListNumber    : longint;      // ����� ����� (���� ����� - site 0)
  Number        : longint;      // ����� ������� � �����
  Flag          : longint;      // ��� ����� 0, ��� ������ 1, ���� ������ ������
  Reserve       : longint;      // ������                            // 22/06/09
end;
PSELECTOBINFO = ^TSELECTOBJINFO;

const
ALS_NO_CHOICE_ATLAS = 1;  // 21/04/10
ALS_NO_SEND_MESSAGE = 2;  // 21/04/10

const
// ������ ���������� MapAccess
// (���� ����������� "mapacces.h" � �.�.)
MAPACCESSVERSION = 20160531;

// ������ ���������� MAPAPI � ���� ����������
MAPAPIVERSION = $0111304;
type
  HHANDLE = integer;


// enum SXFFORMATFLAG
const
 DATAMETERS       = 0;  // ������� � ������
 DATARADIANS      = 4;  // ������� � ��������
 DATADEGREES      = 8;  // ������� � ��������

type
// �������� ��������� ��� �������������� ����������� ������
TCHANGEPOINT = packed record
  PointIn1      : TDOUBLEPOINT; // ���������� ������ ����� �������� �����
  PointIn2      : TDOUBLEPOINT; // ���������� ������ ����� �������� �����
  PointOut1     : TDOUBLEPOINT; // ���������� ������ ����� �������� �����
  PointOut2     : TDOUBLEPOINT; // ���������� ������ ����� �������� �����

  Scale         : double     ; // ���������� �����������  ��������������
  Angle         : double     ; // ���� �������� ��������������
  Error         : longint    ; // ����������� �������� ������ ��������������
end;
PCHANGEPOINT = ^TCHANGEPOINT;

// �������� ��������� ��� ��������� ����� �������
TDECORATEOBJECTPARM = packed record
  ObjectCode            :longint; // ��� �������, �������������� �� ������ (0 - �� �����������)
  TitleCode             :longint; // ��� ������� "������� ���������"       (0 - �� �����������)

  FirstTitleNumber      :longint; // �������� ������ �������              (>=1)
  Subject               :longint; // ����� ����������                     (>=0)
                                  // (-1 - ���������� �� ������� � ���� �����������,
                                  //       BeginPoint,EndPoint - ������������)
  BeginPoint            :longint; // ��������� � �������� ����� ��������� (>=1)
  EndPoint              :longint; //  (0 - ���������� �� ����� �������)

  ComplexTitle          :longint; // ������ �������� �������� ���������
                                  //  (0 - ������� �� ������ �����)
                                  //  (1 - ������� � ������������)
  Reserve               :longint; // ������

  Distance              : double; // ���������� �� ����� �� �������       (>=1)
end;                              //  (��������� � ������ ������ - � �������� ��������)

PDECORATEOBJECTPARM = ^TDECORATEOBJECTPARM;

//-----------------------------------------------------------------
// ��������� ��� �������� ��������� � ������������ ����� �������
//-----------------------------------------------------------------
// �������� ���������� ����� � �������� ������� :
//
//  10               // ����� �����
// 6345.35  6456.46  // ���������� ������� � ������
//-----------------------------------------------------------------
//-----------------------------------------------------------------
TOBJECTSCHEME = packed record
  NameTxt       : GTKPCHAR; // ��� �������� �������� ����� � �������� �������
  NameWmf       : GTKPCHAR; // ��� ��������� ���������
  Width         : longint;  // ������ �������� � ��
  Height        : longint;  // ������ �������� � ��
  Scale         : longint;  // ����������� �������� ����������� �������
                            // (����������� � gsGetScaleObjectMetricTxtToWmfGetScalePictObject)
  Border        : longint;  // ���� ������� ����� (1 - ����, 0 - ���)
  Image         : longint;  // ��� ��������� ������ (0 - �� ��������,
end;                        //  1 - �����,  2 - ���������� � ������)

POBJECTSCHEME = ^TOBJECTSCHEME;

TCELLFORMAT = packed record   // �������� ���� ������ ��
  Length        : integer;                  // ������ ��������� sizeof(CELLFORMAT)
  Code          : integer;                  // ��� ���������
  Name          : array [0..31] of GTKCHAR; // ��� ����
  aType         : integer;                  // ��� ����
                                            //  - ����������� ���� (������������
                                            // ������ ��� ������ �������
                                            // sdbAppendRecordForObject)
                                            // ('N'- ��������,
                                            //  'C'- ����������,
                                            //  'E'- ������� ��� ������� ����� = N 10.0,
                                            //  'O'- ���������� ����� �������  = N 10.0,
                                            //  'S'- �������� ����� �������    = C 24,
                                            //  'U'- ������� ����� ������      = N 5.0
                                            //  'D'- ���� �������� ������������� �������������� 
                                            //  ������� ������������ � ���������� ��������������� ����)
                                            //  'A'- ����������� ��� ����(��� ���������� �������
                                            //       ������ ������� � ������)
  Size          : integer;                  // ������ ����
  Form          : integer;                  // ������ ����� (�������� �������������)
end;
PCELLFORMAT = ^TCELLFORMAT;

TSEMLAYCREATE = packed record                         // C�������� ���������� �������� �������
  Length        : integer;                            // ������ ���������
  Layer         : integer;                            // ����� ����
  Local         : integer;                            // ��� ����������� ��� -1,���� ����� �����������
  EmptyRec      : integer;                            // =0/1 - ������ ������ �� ���������/���������
  Decode        : integer;                            // =0/1 - ��������� �� �������������/�������������
  TableName     : array [0..255] of gtkchar;          // ������ ��� �������
  FieldExcode   : array [0..31] of gtkchar;           // ��� ���� �������� ���� ��� /0
  FieldObject   : array [0..31] of gtkchar;           // ��� ���� ������ ������� ��� /0
  FieldSheet    : array [0..31] of gtkchar;           // ��� ���� ����� ����� ��� /0
  FieldRecN     : array [0..31] of gtkchar;           // ��� ���� ������ ������� ������ ��� /0
end;
PSEMLAYCREATE = ^TSEMLAYCREATE;

TSTLOCATE = packed record      // �������� ������� ��� ��������� �� locate
  NameField     : array [0..31] of gtkchar;  // ��� ����
  CodSem        : integer;                   // ��� ���������(���� ���� ��� ���������, �� ��� ����
                                             // ������������ � ������������ �� CodSem)
  ValueField    :array [0..255] of gtkchar;   // �������� 
end;
PSTLOCATE = ^TSTLOCATE;

TSTNAMEFIELD = packed record     // �������� ������� ���� � ����� �����
  NumbField     : integer;                  // ����� ����
  NameField     : array [0..31] of gtkchar; // ��� ����
  TypeField     : integer;                  // ��� ���� ('N'- ��������,
                                            //           'C'- ����������,
                                            //           'A'- ����������� ��� ����)
end;
PSTNAMEFIELD = ^TSTNAMEFIELD;

const
  DB_REPEATCODESEM    = 2;    // � ������� ����������� ��� ���������,
  DB_REPEATNAMEFIELD  = 3;    // � ������� ����������� ��� ����,
  DB_IMPOSTYPEFIELD   = 4;    // � ������� ������������ ��� ����,
  DB_IMPOSPRESFIELD   = 5;    // � ������� ������������ �������� ����
  DB_FILEABSENT       = 6;    // �������� ���� �� ����������
  DB_ERSIZEARRAY      = 7;    // ������ ������� �� ������������� ���������,
  DB_ERSIZESTRCELLF   = 8;    // �� ��������� ������ ��������� sizeof(CELLFORMAT),
  DB_VALUEIDENTZERO   = 9;    // �������� ��������������  =0
  DB_ABSENTOPENTABLE  = 10;   // ��� �������� �������
  DB_COMERRORSEM      = 11;   // ��� ��������� ������� = 0
  DB_ERREADSEM        = 12;   // ������ ������ ��������� �������
  DB_ERSAVENUMREC     = 13;   // ������ ���������� ������ ������� ������
  DB_IMPOSCODSEM      = 14;   // ���������� ���������� ��� ��������� � �������
  DB_IMPOSOPENTABLE   = 15;   // ���������� ������� �������
  DB_ERNAMETABLE      = 16;   // ��� ���� ������� ������ �� �����
  DB_IMPOSTYPFIELD    = 17;   // ������������ ��� ����
  DB_ABSENTVALUEFIELD = 18;   // ��� �������� � ����
  DB_ERSHORTNAMESEM   = 19;   // �������� �������� �������� ��������� 10 ��������
  DB_ERRECORDCOUNT    = 20;   // ���-�� ������� � ������� =0
  DB_ERRFIRSTSIMBOL   = 21;   // ������������ ������ ������ � ����� ����
  DB_ERRFIELDNAME     = 22;   // ��� ���� ������ �������� ������
  DB_IDENTMAPZERO     = 30;   // ������������� ����� =0
  DB_IDENTRSCNOTFOUND = 31;   // ������������� Rsc �� ������
  DB_REPEATOPENTABL   = 32;   // ����� ��� ������������������ � ������� �������
  DB_ABSENTVALSEMLAYER= 33;   // ��� ��������� ��� ������� ���� (=0)
  DB_ERCOUNTSEMLAYRSC = 34;   // ������ � ���������� �������� ���� � RSC
  DB_ERTYPESEMRSC     = 35;   // ������ ��� ����������� ���� ��������� �� RSC
  DB_ERSERVISABSENT   = 36;   // ��� ������� � ������� ��-�� ���������� ������
  DB_IMPOSDELOLDTABLE = 37;   // ���������� ������� ������ ���� �������
  DB_ERSIZENUMLAYERLOC= 38;   // ����� ���� ��� ����������� ������ �����������
  DB_ABSREGLAYERLOC   = 40;   // ��� ���������� ������������������� ���� � �����������
  DB_NOUNREPREGISTAB  = 41;   // ����� � ��������� ����������� �������
  DB_NOTNAMETABLEFIELD= 42;   // �� ������ ��� ���� �������
  DB_UNDERSINPUTPARAM = 98;   // ��������� ������� ���������
  DB_SYSTEMERROR      = 99;   // ��������� ������(������ � �.�.)

//-----------------------------------------------------------------
// �������������� ���������� ������
//-----------------------------------------------------------------

type
TPRINTERINFO = packed record                             // 24/12/09
 DeviceName     : array [0..255] of GTKCHAR;    // ��� ���������� ������
 Reserve        : array [0..511] of GTKCHAR;    // ������

 PaperWidth     : double;                       // ������� ������ � ������ ���������� ��������
 PaperHeight    : double;                       // ���������� ������ � �����������

 PixDeviceInMeterX: double;                     // ������� ���������� ���������� ������
 PixDeviceInMeterY: double;                     // �� ����������� � ��������� � ������ �� ����
end;
PPRINTERINFO = ^TPRINTERINFO;

//-----------------------------------------------------------------
// �������� ����� ��������� ������� ������ �����
//-----------------------------------------------------------------
// ����� ������ ������ "������ �����"
const
  MPR_PRINTONLY       = -2;  // ������ ������ ����� ��� ������ �������

  MPR_INITPARM        = -1;  // �������� ������� ������ ��� ���������
                             //  �������� � ���������� ������ (������
                             //  "�������" � "��������" ���������)

  MPR_PRINTALL        =  0;  // �������� ������� ��� ��������� ��������,
                             //  ���������� ������ � ������� ������ �����
                             //  � �������������� ���������� ������� ��
                             //  ��������� ������ (RectMetr - ������������)

  // ��� ����� ������������ ��� �������� ������� ��������� ��������
  // ���������� ������ � ������� ������ ����� � ��������������
  // ���������� ������� �� RectMetr
  MPR_PRINT           =  1;  // ������
  MPR_SELECTFRAME     =  2;  // ����� ������������� �����
  MPR_CHANGEFRAME     =  3;  // ��������� � ����������� ������������� �����
  MPR_MOVEFRAME       =  4;  // ������ ����������� ������������� �����
  MPR_SELECTBYOBJECT  =  5;  // ����� ������� � ��������� ������������� �����
                             // �� ��������� �������

  MPR_SELECTTURNFRAME = 10;  // ���� ������ ����������� ���������

  // ��������, ����������� ����� Regime ��� ������� �� ������:
  // "������"     - MPR_PRINT;
  // "�������"    - MPR_SELECTFRAME ��� MPR_SELECTTURNFRAME;
  // "��������"   - MPR_CHANGEFRAME ���
  //                MPR_MOVEFRAME (��� FitToPage = MPF_ACCORDTOPAGE);
  // "�� �������" - MPR_SELECTBYOBJECT.
  // ����� ������, ��������� ��� ����������� ������� ������ �����
  // ������� ������ �������� �� ������� �������� ���� Regime.
//-----------------------------------------------------------------

// ����� ������ ���� ���������. ���������
  MPV_SCHEME      = 0;  // ���������� ������� �� ���� �����
  MPV_MAP         = 1;  // ���������� ������� �� ���� �����
  MPV_INFO        = 2;  // ���������� �� ���������� ������
//-----------------------------------------------------------------

// ��� ������
  MPT_NORMAL      = 3;  // ����������
  MPT_TRANSPARENT = 4;  // ����������
  MPT_CONTOUR     = 5;  // ���������
//-----------------------------------------------------------------

// ��� ������ MAPPRINT_OUTPUT
  // ������������ ���������� ������� WIN API (��� ������)

  MPO_VECTORIAL   = 0;  // ��������� (����� ������ ��������� ����)
  MPO_RASTERIAL   = 1;  // ��������� (����� ����, ������� � ������)

  // ������������ ������������ WIN API (��� �������� � PostScript-����)

  MPO_VECTORIALEX = 4;  // ��������� �����������
//-----------------------------------------------------------------

// ���������� MAPPRINT_ORIENT
  MPN_DEFAULT     = 0;  // ����� �� ������������ ������� ��������
  MPN_PORTRAIT    = 1;  // �������
  MPN_LANDSCAPE   = 2;  // ���������

//-----------------------------------------------------------------
// ������������ MAPPRINT_FITTING

  MPF_SPLITTOPAGES= 0;  // ������� �����������
  MPF_FITTOPAGE   = 1;  // ��������� �� ��������
  MPF_ACCORDTOPAGE= 2;  // �� ������� ��������
  MPF_FITBYHOR    = 3;  // ��������� �� �����������                  // 15/05/14   // 23/06/2016
                        // (�� �������� ���������� �������)
  MPF_FITBYVER    = 4;  // ��������� �� ���������
//-----------------------------------------------------------------
//-----------------------------------------------------------------
// ��������� ������ �����  ��� ������� Print (mappaint.h, mapacces.h)
//-----------------------------------------------------------------
type
TPRINTPARM = packed record
  Length        : longint;
  Scale         : longint;

  TypePrint     : GTKCHAR;
  TypeOutput    : GTKCHAR;
  Mirror        : GTKCHAR;
  ColorModel    : GTKCHAR;
  Technology    : GTKCHAR;




  Reserve       : array [0..17] of GTKCHAR;
  DontClip      : GTKCHAR;        // ������� �������                                  // 08/04/11

  HorPixInInch  : double;
  VerPixInInch  : double;
end;
PPRINTPARM = ^TPRINTPARM;


//-----------------------------------------------------------------
// ��������� ��������� ���������� ��� ������� ���������� ����������� ����������
//-----------------------------------------------------------------
type
TFRAMEPRINTPARAM = packed record
  DeleteSite    : integer;      // ������� �������� Site ����� ������
  TurnFrame     : integer;      // ��� ����� (0 - ������, 1 - ���������)
  PlaneFrame    : PDFRAME;      // �������� ����� ����������
  ExpandFrame   : PDFRAME;      // ����������� ���������� �������� �����
  Angle         : double;       // ���� ������� �����
  SiteDecor     : HSITE;        // ���������� ����������
  EnableSite    : integer;      // ������/���������� �������������� Sit
{$IFDEF CPUX64}                                                                 // 25/12/2013
  FramePrintZero : integer;     // ������������                                 // 25/12/2013
{$ENDIF}
end;
PFRAMEPRINTPARAM = ^TFRAMEPRINTPARAM;


//-----------------------------------------------------------------
// ��������� ������� ���������� �����
//-----------------------------------------------------------------
type
TSAVEASPICTRPARM = packed record
  Length        : longint;                      // ����� ��������� SAVEASPICTRPARM(������������ ��������)
{$IFDEF CPUX64}                                                              // 23/06/2016
  Reserve1      : longint;                                      // 23/06/2016
{$ENDIF}  
  Handle        : HWND;                         // ������������� ���� �����(������������ ��������)

  Scale         : longint;                      // ������� (�����������)(��� ������������ ��. �����)
  Resolution    : longint;                      // ����������� ����������� (����� �� ����)(��� ������������ ��. �����)
  BitCount      : longint;                      // ���������� ��� �� ������ (��� ������������ ��. �����)
{$IFDEF CPUX64}                                                              // 23/06/2016
  Reserve2      : longint;                                      // 23/06/2016
{$ENDIF}  
  Intensity     : GTKCHAR;                      // ������������� ������� ��������� (0-100)(��� ������������ EMF)  13/01/03
  ResolvColor   : GTKCHAR;                      // ������� ���������� CMYK � ������ ����� (��� ������������ EPS 0 - � ������  ����� 1 - � ����) 13/01/03
  IntergraphTIFF: GTKCHAR;                      // ���� ������ ������� ����������������� ��� ����������� TIFF ��� ������������� � Intergraph // 28/09/05
  CompressTIFF  : GTKCHAR;                      // ���������� ������ ������ PackBit   // 12/10/05
{$IFDEF CPUX64}                                                              // 23/06/2016
  Reserve3      : array [0..3] of GTKCHAR;                                      // 23/06/2016
{$ENDIF} 
// 11/12/2013  PlaneFrame    : PDFRAME;         // ������������� ����������� � ������ (� ������)
  PlaneFrame    : TDFRAME;   // 11/12/2013      // ������������� ����������� � ������ (� ������)

  Regime        : GTKCHAR;                      // ����� ������ ������ (��. SAVEASPICTR_REGIME)
  TypeView      : GTKCHAR;                      // ��� ����������� (��. SAVEASPICTR_TYPE)(��� ������������ EMF)
  Black         : GTKCHAR;                      // ���� �����-������ ����������� (1 - ����, 0 - ���)(��� ������������ EMF)
  Border        : GTKCHAR;                      // ���� ������� ����� (1 - ����, 0 - ���)(��� ������������ EMF)

  FileOfParam   : GTKCHAR;                      // ���� �������� ����� ���������� (*.ini) (��� ������������ ��. �����)
  FormatCorel9  : GTKCHAR;                      // ������ EMF, ��������������  (��� ������������ EMF)   // 31/01/01
  ColorModel    : GTKCHAR;                      // ���� �������� ������ (0 - RGB, 1 - CMYK)(��� ������� 8-������ BMP)
  PolyMark      : GTKCHAR;                      // ���� ��������������� ����� (1 - ����, 0 - ���)(��� ColorModel = CMYK)

  FileName      : array [0..259] of GTKCHAR;    // ��� �����(������������ ��������)
{$IFDEF CPUX64}                                                              // 23/06/2016
  Reserve4      : array [0..3] of GTKCHAR;                                      // 23/06/2016
{$ENDIF} 
end;
PSAVEASPICTRPARM = ^TSAVEASPICTRPARM;

// �������� ��������� ���������� ��������������� ������
const
  WM_PCX2RST              = $590;       // ���������  PCX � RST
  WM_PCX2RST_PROGRESS     = $591;
  WM_PCX2RST_ERROR        = $592;

  WM_RSTCOMPRESS          = $593;       // ������ ������
  WM_RSTCOMPRESS_ERROR    = $594;


 // ����� MTD-������
type
TXYZC = packed record  // ������ ��������� 32 �����
  X     :double;   // ���������� �����
  Y     :double;
  Z     :double;   // ������ �����
  C     :integer;  // ��� �����
  Free  :integer;
end;
PXYZC = ^TXYZC;

 // ����� MTD-������ � ������� �� LAS-�����            27/08/09
type
TXYZCLAS = packed record             // ������ ��������� 80 ����
  X             : double;
  Y             : double;
  Z             : double;

  C             : integer;
  ReturnNumber  : integer;

  NumberOfReturns       : integer;
  ScanDirectionFlag     : integer;

  EdgeOfFlightLineFlag  : integer;
  Classification        : integer;

  ScanAngleRank         : integer;
  FileMarker            : integer;

  Intensity             : integer;
  UserBits              : integer;

  Free1                 : integer;
  Free2                 : integer;

  Free3                 : integer;
  Free4                 : integer;
end;
PXYZCLAS = ^TXYZCLAS;



//-----------------------------------------------------------
// ��������� ����� (���������� ������������� � ����� �����)            // 04/04/2014
// ��� �������� MTD �� TXT-����� (16 ����)
//-----------------------------------------------------------
TMTDXYHC   = packed record

   X:Cardinal;  // ���������� �����           // 12/05/11  "unsigned"
   Y:Cardinal;
   H:Cardinal;
   C:Cardinal;  // ��� �����
 end;
PMTDXYHC = ^TMTDXYHC;


//-----------------------------------
//  ������� ���� LAS-����� (4 �����)                                // 04/04/2014
//-----------------------------------
TLASINFO  = packed record

  RNSE : Byte;                      // ��������� ������� ������� ����
   {
 0-2 ���� unsigned ReturnNumber :3;           // The return number - ����� ��������
 3-5 ���� unsigned NumberOfReturns :3;       // The number of returns for that shot - ���-�� ��������� ������� ��������
   6 ���  unsigned ScanDirectionFlag :1;     // The scan direction - ���� ����������� ������������
   7 ���  unsigned EdgeOfFlightLineFlag :1;  // Edge of scan flag - ���� "���� ����� �����"
}
  Classification : Byte;             // �������������
  ScanAngleRank  : Shortint;         // ���� ���� ������������ (-90 to +90) - Left side
  FileMarker     : Byte;             // ������ ����� - ��� ����������� � ������ ���� LAS ������

end;
PLASINFO = ^TLASINFO;




//-----------------------------------------------------------
// ��������� ����� (���������� ������������� � ����� �����)                  // 04/04/2014
// ��� �������� MTD �� LAS-����� (24 �����)
//-----------------------------------------------------------


TMTDXYHCLAS = packed record

  X:Integer;  // ���������� �����
  Y:Integer;
  H:Integer;
  C:Integer;  // ��� �����

  LasInfo:TLASINFO;  // ������� ���� ����� (4 �����)

  Intensity:Word;  // ������������� ��������
  UserBits:Word;   // ���� ����� ������������

end;
PMTDXYHCLAS = ^TMTDXYHCLAS;


// WORKTYPE
const
 MCK_CK42     = 215;     // ������� ������� ��������� �� ���� ��-42        // 21/09/09
 MCK_CK95     = 216;     // ������� ������� ��������� �� ���� ��-95
 MCK_UTM      = 218;     // ������� ������� ��������� �� ���� UTM\WGS84    // 21/09/09

// ���� ��������� ��� �������-�������������� ���� GEOGRAPHICCODE
  GC_EUROPE           = 1;    // ���� ������
  GC_ASIA             = 2;    // ���� ����
  GC_PACIFICOCEAN     = 3;    // ����� �����
  GC_AMERICANORTH     = 4;    // �������� �������
  GC_ATLANTICOCEAN    = 5;    // ������������� �����
  GC_NORTHPOLARSYSTEM = 6;    // �������� �������� ����������
  GC_SOUTHPOLARSYSTEM = 7;    // ����� �������� ����������
  GC_EQUATORIALSYSTEM = 8;    // �������������� ����������

  // ����� ���������� ���������� �������� (mapGetProjectionParameters)
  type PROJECTIONPARAMETERS =
  (
    EPP_AXISMERIDIAN       = 1,  // ������ �������� (������� ������ ��������)
    EPP_MAINPOINTPARALLEL  = 2,  // ��������� ������� ����� (������ ������ ��������)
    EPP_FIRSTMAINPARALLEL  = 4,  // ������ ������� ���������
    EPP_SECONDMAINPARALLEL = 8,  // ������ ������� ���������
    EPP_FALSEEASTING       = 16, // �������� ��������� �� ��� Y
    EPP_FALSENORTHING      = 32, // �������� ��������� �� ��� X
    EPP_SCALEFACTOR        = 64 // ���������� ����������� �� ������ ���������
  );

// CROSS-����, ������������ �������� ovlGetCrossPoint
const
  ovl_CROSS_0           =     0;    // ����������� ���
  ovl_CROSS_1           =     1;    // 1 �� (���� ����� �����������/���������)
  ovl_CROSS_2           =     2;    // 2 �� (��� ����� �����������/���������)implementation
  ovl_CROSS_A1A2_EQUAL  =     4;    // ����� ������� A ����� (������� ������� �����)
  ovl_CROSS_B1B2_EQUAL  =     8;    // ����� ������� B ����� (������� ������� �����)end.
  ovl_CROSS_A1B1_EQUAL  =    16;    // ����� A1 � B1 �����
  ovl_CROSS_A2B2_EQUAL  =    32;    // ����� A2 � B2 �����
  ovl_CROSS_A1B2_EQUAL  =    64;    // ����� A1 � B2 �����
  ovl_CROSS_A2B1_EQUAL  =   128;    // ����� A2 � B1 �����


// ����������� CROSS-����, ������������ �������� ovlGetCrossPoint
  ovl_CROSS_A1A2_EQUAL_1   =  5;  // ����� ������� A ����� + 1 �� (1+4)
  ovl_CROSS_B1B2_EQUAL_1   =  9;  // ����� ������� B ����� + 1 �� (1+8)

  ovl_CROSS_AB_NULL        = 12;  // O������ A � B ������� ����� (4+8)

  ovl_CROSS_A1B1_EQUAL_1   = 17;  // ����� A1 � B1 ����� + 1 �� (1+16)
  ovl_CROSS_A2B2_EQUAL_1   = 33;  // ����� A2 � B2 ����� + 1 �� (1+32)
  ovl_CROSS_A1B2_EQUAL_1   = 65;  // ����� A1 � B2 ����� + 1 �� (1+64)
  ovl_CROSS_A2B1_EQUAL_1   =129;  // ����� A2 � B1 ����� + 1 �� (1+128)

  ovl_CROSS_A1B1_EQUAL_2   = 18;  // ����� A1 � B1 ����� + 2 �� (2+16)
  ovl_CROSS_A2B2_EQUAL_2   = 34;  // ����� A2 � B2 ����� + 2 �� (2+32)
  ovl_CROSS_A1B2_EQUAL_2   = 66;  // ����� A1 � B2 ����� + 2 �� (2+64)
  ovl_CROSS_A2B1_EQUAL_2   =130;  // ����� A2 � B1 ����� + 2 �� (2+128)

  ovl_CROSS_A1A2B1_EQUAL_1 =149;  // ����� A1,A2,B1 ����� + 1 �� (1+4+16+128)
  ovl_CROSS_A1A2B2_EQUAL_1 =101;  // ����� A1,A2,B2 ����� + 1 �� (1+4+32+64)
  ovl_CROSS_B1B2A1_EQUAL_1 = 89;  // ����� B1,B2,A1 ����� + 1 �� (1+8+16+64)
  ovl_CROSS_B1B2A2_EQUAL_1 =169;  // ����� B1,B2,A2 ����� + 1 �� (1+8+32+128)

  ovl_CROSS_AB_EQUAL_2     = 50;  // ������� ��������� + 2 �� (2+16+32)
                            // (� ������ �����������)

  ovl_CROSS_BA_EQUAL_2     =194;  // ������� ��������� + 2 �� (2+64+128)
                            // (� �������� �����������)

  ovl_CROSS_ALL_EQUAL_1    =253;  // ��� ����� ����� + 1 �� (1+4+8+16+32+64+128)

  ovl_CROSS_EQUAL_1        =240;  // ���� ����������� ������� ����� ����� (16+32+64+128)


// �������� CROSS-���� (������������ ����� ������ ovlGetCrossPoint)
  ovl_CROSS_GETCOUNT       = 3;  // ��� ����������� ����� ����� �����������


// ����� ������������ ������ � ������ ���������� �������
// (������������ � ovlSetObjectCross)

  ovl_FLAG3D_NONE     =0; // ��������� �� �������� ���������� �������
  ovl_FLAG3D_TEMPLET  =2; // ������ ���������� ���������� �� �������
                          // �������
  ovl_FLAG3D_MATRIX   =8; // ������ ���������� ���������� �� �����
                          // (�� �������� ������ �������� ������� �����)
  ovl_FLAG3D_LINE    =32; // ������ ���������� ����������� �� �������
                          // ������ ������� (������� �������� ������������)
  ovl_FLAG3D_ALL     =42; // ���������� ������������� ���� ������
                          // FLAG3D_TEMPLET|FLAG3D_MATRIX|FLAG3D_LINE

// 25/02/2011
// ����� ���� �������������� �������� - ����� ������ ���������
// (������������ � ovlSetObjectCross)
  ovl_METHOD_LINE    = 0;  // ��������� � ����������� ������� �������� ��������
  ovl_METHOD_SQUARE  = 1;  // ��������� ������� (����� object) ��������� ��������
  ovl_METHOD_FAST    = 16; // ������� ������ ��������� - ������������ ������
                           // ��� ��������, ������� �� �������� ��������������� !!!


// ����� ���������� �������������� �������� (������������ � ovlSetObjectCross)

  ovl_ANYOBJECT          = $000; // ����� ���� ��������
  ovl_ANYOBJECT2         = $360; // ����� ���� ��������, ������� �������
                                 // ������� ������ �������� �������

  ovl_OBJECTINSIDE       = $020; // ����� �������� ������ �������
  ovl_OBJECTINSIDE2      = $120; // ����� �������� ������ �������, �������
                                 // ������� ������� ������ �������� �������

  ovl_OBJECTOUTSIDE      = $040; // ����� �������� ��� �������
  ovl_OBJECTOUTSIDE2     = $240; // ����� �������� ��� �������, �������
                                 // ������� ������� ������ �������� �������
  ovl_OBJECTOVERLAP      = $100;  // ����� ����������� �������� ��������  // 26/03/2014


// ���� ������ ����������� ��������, ������������ �������� ovlGetErrorCode

  OVL_ERR_NONE                  =   0; // "������ ���",
  OVL_ERR_UNKNOWN               =   1; // "����������� ������",
  OVL_ERR_PARAM                 =   2; // "������ ������� ����������",
  OVL_ERR_LOCAL                 =   3; // "������ ����������� �������",
  OVL_ERR_STRUCT                =   4; // "������ ��������� �������",
  OVL_ERR_LESSCOUNT             =   5; // "����� ����� ������� ������� ������ �����������",
  OVL_ERR_LESSCOUNT2            =   6; // "����� ������������� ����� ������� ������� ������ �����������",
  OVL_ERR_OVERCOUNT             =   7; // "����� ����� ����� ������� ������ �����������",
  OVL_ERR_UNLOCKED              =   8; // "������ ���������� ������� ���������",
  OVL_ERR_METHOD                =   9; // "������ ������� ���������. ������ ��������� ���� ���������",
  OVL_ERR_MEMORY                =  10; // "������ ��� ��������� ������",
  OVL_ERR_OVERCROSS             =  11; // "����� ����������� ������ ����������� (����� ���)",
  OVL_ERR_ADDSUBJECT            =  12; // "������ ���������� ����������",
  OVL_ERR_ADDPOINT              =  13; // "������ ���������� �����",
  OVL_ERR_DELPOINT              =  14; // "������ �������� �����",
  OVL_ERR_SELFCROSSING          =  15; // "���������� ��������������� ������� �������",
  OVL_ERR_SELFCROSSING2         =  16; // "���������� ����������� �������� �������",
  OVL_ERR_NOCROSS_OBJECTINSIDE  =  17; // "����������� ���. ������ ��������� ������ �������",
  OVL_ERR_NOCROSS_OBJECTOUTSIDE =  18; // "����������� ���. ������ ��������� ��� �������",
  OVL_ERR_TEMPLETOWNED          =  19; // "������ ������� ����������� ������� �������",
  OVL_ERR_NOCROSS               =  20; // "������ ���������. ����������� ���",
  OVL_ERR_SUBJECT               =  21; // "������� ��������� �������� ��������� ����� ���������� �������",
  OVL_ERR_SQUARE_LESSCOUNT      =  22; // "��������� ����� ���������. ����� �������� ����� ������� ������� ����� 4",
  OVL_ERR_SQUARE_UNLOCKEDTEMPLET=  23; // "��������� ����� ���������. ������ ������� ���������",
  OVL_ERR_SQUARE_UNLOCKEDOBJECT  = 24; // "��������� ����� ���������. ������ ������� ��� ���������� ���������",
  OVL_ERR_INSIDE_LESSCOUNT       = 25; // "����� ������ �������. ����� �������� ����� ������� ������� ����� 4",
  OVL_ERR_INSIDE_UNLOCKEDTEMPLET = 26; // "����� ������ �������. ������ ������� ���������",
  OVL_ERR_OUTSIDE_LESSCOUNT      = 27; // "����� ��� �������. ����� �������� ����� ������� ������� ����� 4",
  OVL_ERR_OUTSIDE_UNLOCKEDTEMPLET= 28; // "����� ��� �������. ������ ������� ���������",
  OVL_ERR_GETSIDE_LESSCOUNT      = 29; // "����������� ���������. ����� �������� ����� ������� ������� ����� 4",
  OVL_ERR_GETSIDE_UNLOCKEDTEMPLET= 30; // "����������� ���������. ������ ������� ���������",
  OVL_ERR_SUBJECTNUMBER          = 31; // "������ ������� �������. ����� ��������� ����� ����������",
  OVL_ERR_NOTEMPLET              = 32; // "������ �� ����������. ������������ ������� ������ �������",
  OVL_ERR_NOTEMPLET2             = 33; // "������ �� ����������. ��� �������� ������� ���������� ������",
  OVL_ERR_NOOBJECT               = 34; // "�������������� ������ �� ����������. ������������ ������� ������ �������",
  OVL_ERR_NOOBJECT2              = 35; // "�������������� ������ �� ����������. ��� �������� ������� ���������� ������",
  OVL_ERR_SELFCROSSING_OFF       = 36; // "������ ���������. ��������� �������� �� ���������������",
  OVL_ERR_OVERCROSS_TEMPLET      = 37; // "������ ���������. ��������� ������������ ����� ����� �������",
  OVL_ERR_OVERCROSS_OBJECT       = 38; // "������ ���������. ��������� ������������ ����� ����� �������",
  OVL_ERR_OVERCROSS_RESULT       = 39; // "������ ���������. ��������� ������������ ����� ����� ������������ �������",
  OVL_ERR_EXCEEDS_MAXIMUM        = 40; // "����� ������ ��������� ������������",
  OVL_ERR_SELFCROSSING_OVERLAY   = 41; // "���������� ����������� ������� ������� �������",
  OVL_ERR_SELFCROSSING_OVERLAY2  = 42; // "���������� ����������� ������� �������� �������",
  OVL_ERR_SELFCROSSING_ADJACENT  = 43; // "���������� ����������� ������� ������� �������",
  OVL_ERR_SELFCROSSING_ADJACENT2 = 44; // "���������� ����������� ������� �������� �������",
  OVL_ERR_CHECKING_NEARPOINT     = 45; // "����� ������� ������� ������",
  OVL_ERR_CHECKING_NEARPOINT2    = 46; // "����� �������� ������� ������",
  OVL_ERR_CHECKING_PEAK          = 47; // "������ � ����� �������",
  OVL_ERR_CHECKING_DOUBLEPOINT   = 48; // "���������� ������� �����",
  OVL_ERR_CHECKING_DOUBLEPOINT2  = 49; // "���������� ������� ����� � �������",
  OVL_ERR_END                    = 49; // ��������� ��� ������� ������


// OUTPUTAREA            // ������ ������ ������� ������
  AREA_ALL            = 0; // �������� : ������� ������ - ���� �����
                           // �������� : ������� ������ �� ���������

  AREA_FRAME          = 1; // �������� : ������� ������ - �� RectMetr
                           // �������� : ������� ������ -> � RectMetr

  AREA_SELECT_FRAME   = 2; // �������� : ������� ������ - �� RectMetr
                           // �������� : ��� ������ �������������� ���������

  AREA_CHANGE_FRAME   = 3; // �������� : ������� ������ - �� RectMetr
                           // �������� : ��� ��������� ���������

  AREA_MOVE_FRAME     = 4; // �������� : ������� ������ - �� RectMetr
                           // �������� : ��� ����������� ���������

  AREA_SELECT_OBJECT  = 5; // �������� : ������� ������ - �� RectMetr
                           // �������� : ��� ������ ������� ������ �� �������

  AREA_SELECT_CONTOUR = 6; // �������� : ������� ������ - �� RectMetr
                           // �������� :  ��� ������ ������� ������ �� �������

  AREA_OBJECT         = 7; // ���������� ������ �� ������  // 04/04/2014
                           // ������� ����������� ����� �� �������

  AREA_CONTOUR        = 8; // ���������� ������ �� ������       // 04/04/2014
                           // ������� ����������� ����� �� �������

  AREA_OBJECTS        = 9; // ��������� ����������� �� ���������� ��������   // 04/04/2014

  AREA_SELECT_TURN_FRAME = 10; // ���� ������ ����������� ���������


// CHARCODE         // �������� ��������� �������
  CC_ASCIIZ  = 0  ;   // C����� �������� (ASCIIZ), ������������ �����
  CC_KOI8    = 125;   // C����� (KOI8), ������������ �����
  CC_ANSI    = 126;   // C����� (ANSI, WINDOWS), ������������ �����
  CC_UNICODE = 127;   // C����� �������� (UNICODE), ������������ �����

// SPLINETYPE           // ���� ��������
  SPLINETYPE_NONE   = 0;  // ������� �����
  SPLINETYPE_SMOOTH = 2;  // ������������ ������ (������� ����)
  SPLINETYPE_POINTS = 4;  // ��������� ������ (�������� ����� �����)

//enum PRECISIONFLAG              // �������� �������� ��������� �� �����
  PF_FLOAT  = 0;                // ���������� �������� � ���������
  PF_DOUBLE = 1;                // ���������� �������� � ���������
  PF_METRE  = 2;                // ���������� �������� � ������
  PF_GEO    = 4;                 // ���������� �������� � ��������     // 17/03/10


//enum RST_STATE                   // ��������� ����� ������                      // 18/04/16  // 23/06/2016

  RSTST_NONE              =   0;    // ��� ������; ��� �������� �� � ������ �� �����������
  RSTST_DUPL_JPEG         =   1;    // �������� ���� ������� ��, ������ JPEG ������
  RSTST_DUPL_LZW          =   2;    // �������� ���� ������� ��, ������ LZW ������

  RSTST_DUPL              =   4;    // �������� ���� ������� ��



//enum MTW_STATE                   // ��������� ����� �������                     // 18/04/16   // 23/06/2016

  MTWST_NONE              =   0;    // ��� ������; �������� �� � ������ �� �����������
  MTWST_DUPL_COMPR32      =   3;    // �������� ���� ������� ��, ������ RMF_COMPR_32 �������

  MTWST_DUPL              =   4;    // �������� ���� ������� ��
  

// ����������������� ���� ������������� �������������,
// �������� �� ��� �����                                     // 04/04/2014
// ---------------------------------------------------
const
 EXTSEMANTIC       = 31001;  // ������ ��������� ��������� ��������
 SEMIMAGESCALE     = 31001;  // ������� ����������� ����� � ���������
 SEMIMAGECOLOR     = 31002;  // ���� ����������� ����� RGB
 SEMIMAGEHIGHT     = 31003;  // ������ ������
 SEMIMAGEFONT      = 31004;  // �������� ������
 SEMIMAGETHICK     = 31005;  // ������� ����� � ��
 SEMIMAGETHICK2    = 31006;  // ������� ��������� ����� � ��
 SEMCOLORWEIGHT    = 31007;  // ��� ����� ������� � ���������
 SEMLINETHICK      = 31008;  // ������� ����� � �������
 SEMLINECOLOR      = 31009;  // ���� �����
                                  // �������� �������� ����� RGB
 SEMLINEPATTERN    = 31010;  // ����� ������� ������� ����� � MAPINFOW.PEN
                                  //  (1-255)
 SEMMARKSIZE       = 31011;  // ������ ������� ����� � ������� (1-48)

 SEMMARKCOLOR      = 31012;  // ���� �����, �������
                                  // �������� �������� ����� RGB
 SEMFONCOLOR       = 31013;  // ���� ����
                                  // �������� �������� ����� RGB
 SEMMARKSHAPE      = 31014;  // ����� ������� ����� � ���������������
                                  // TRUETYPE ������ (������ 31),
                                  // ��������� � ��������� SEMIMAGEFONT
 SEMSQUAREPATTERN  = 31015;  // ����� ������� ������� ������� � MAPINFOW.PEN
                                  // (1-255)
 SEMFONTSTYLE      = 31016;  // ����� ������ (����� ���������:
                                  // 0 - �������, 1 - T������, 16 - � �����,
                                  // 32 - � �����, 256 - ����� �����)
 EXTSEMANTICEND    = 31016;  // ����� ���������

// ����������������� ���� ������������� �������������,
// ��� ������                                                 // 04/04/2014
// ---------------------------------------------------
 ADRSEMANTIC       = 31201;  // ������ ��������� �������� ��������
 SEMADRSTREET      = 31201;  // �������� �����
 SEMADRHOUSE       = 31202;  // ����� ����
 SEMADRTRUNK       = 31203;  // ����� �������
 SEMADRBUILD       = 31204;  // ����� ��������
 SEMADRESTATE      = 31205;  // ����� ��������
 SEMADRSTREETCODE  = 31206;  // ��� �����
 SEMADRTOWN        = 31207;  // �������� ������ (����������� ������)
 SEMADRTOWNCODE    = 31208;  // ��� ������ (����������� ������) �� �����
 SEMADRPOSTCODE    = 31209;  // �������� ������
 ADRSEMANTICEND    = 31209;  // ����� ���������


// ����������������� ���� ������������� �������������
// ---------------------------------------------------
const
 OBJEXCODEINSEMANTIC =32800;  // ����������������� ��� ��'����
 SEMOBJEXCODE        =32800;  // ����������������� ��� ��'����

 GROUPLEADER         =32801;  // ������� ��'����, �������� ������
 SEMGROUPLEADER      =32801;  // �� ����������� ��'���

 GROUPSLAVE          =32802;  // ������� ��'����, �� �������
 SEMGROUPSLAVE       =32802;  // ������� ������ �� �������� ��'����;

 GROUPPARTNER        =32803;  // ������� ��'����, ���������
 SEMGROUPPARTNER     =32803;  // � ������ ������������ ��'�����

 SEMOBJECTTOTEXT     =32804;  // ������ �� ������� �� ��'����
 SEMOBJECTFROMTEXT   =32805;  // ������ �� ��'��� �� �������

 SEMOBJECTTOTEXTFAR  =32806;  // ������ �� ������� �� �������               // 31/05/16   // 23/06/2016
                                   // (���� �������-������� �� ������� �����)
 SEMOBJECTFROMTEXTFAR=32807;  // ������ �� ������ �� �������                // 31/05/16
                                   // (���� ��������� ������� �� ������� �����)

 SEMRSCNAME          =32809;  // ��� ����� �������������� RSC   // 16/02/10
 SEMLAYERSHORTNAME   =32810;  // �������� ��� ���� ��'����
 SEMOBJECTSHORTNAME  =32811;  // �������� ��� ��'���� (����)
 SEMOBJECTIDENT      =32812;  // ������������� �������     // 13/01/12
 SEMOBJECTBASESCALE  =32814;  // ������� ������� ��������� ��������� ����� �������   // 23/06/2016
// ��������� ��������� �� ������������� !
 SEMHIDEFIRST        =32800;  // ������ ��������� ��������� ��������
 SEMHIDELAST         =32800;  // �����  ��������� ��������� ��������

 SEMLONWGS84         =32201;  // ������������� ���������� �� ������� � �������� �� WGS-84
 SEMLATWGS84         =32202;  // ������������� ���������� �� ������ � �������� �� WGS-84

 SEMOBJECTDATE       =32850;  // ���� �������� ��'����
 SEMOBJECTTIME       =32851;  // ����� �������� ��'����
 SEMOBJECTAUTHOR     =32852;  // ��� ���������
 SEMOBJECTREDATE     =32853;  // ���� ���������� ��'����        // 17/09/09
 SEMOBJECTRETIME     =32854;  // ����� ���������� ��'����
 SEMOBJECTREAUTHOR   =32855;  // ��� ��������� ����������

// DATUM PARAMETERS
 SEMDATUMDX          =32871;  // ����� �� X
 SEMDATUMDY          =32872;  // ����� �� Y
 SEMDATUMDZ          =32873;  // ����� �� Z
 SEMDATUMRX          =32874;  // ������� �� X
 SEMDATUMRY          =32875;  // ������� �� Y
 SEMDATUMRZ          =32876;  // ������� �� Z
 SEMDATUMM           =32877;  // ���������� �������
 SEMDATUMTYPE        =32878;  // ��� �������������� (0, 3, 7, 14)
 SEMSCALEFACTOR      =32879;  // ������� �� ������ ���������             // 13/05/15                           
// USER ELLIPSOID PARAMETERS
 SEMUSELLAXIS        =32880;  // SEMIMAJORAXIS
 SEMUSELLINFL        =32881;  // INVERSEFLATTENING

 SEMCRSIDENT         =32882;  // CRS Ident (String)
 SEMSHEETNAME        =32883;  // ��� ����� �����
 SEMNOMENCLATURE     =32884;  // ������������ ����� �����  // 16/10/14
 SEMMAINNAME         =32885;  // �������� ������           // 16/10/14
 SEMDATAIDENT        =32886;  // GUID ������ ������        // 11/01/16 

// ����������������� ���� ������������� �������������,
// ��� ������                                                 // 23/04/08
// ---------------------------------------------------
const
 SEMNETRIB           =32812;  // ����� �������-����� (��� ����)
 SEMNETKNOTFIRST     =32813;  // ����� �������-���� ������ ����� (��� �����)
 SEMNETKNOTLAST      =32814;  // ����� �������-���� ��������� ����� (��� �����)
 SEMNETNUMBER        =32815;  // ����� ����
 SEMNETTOOBJECT      =32816;  // ������ �� ������ �����
 SEMNETVDIRECT       =32817;  // �������� ������ (��� �����)
 SEMNETVUNDIRECT     =32818;  // �������� �������� (��� �����)
 SEMNETCOSTRIB       =32819;  // ��������� �����
 SEMNETRANKRIB       =32820;  // ���� �����
 SEMNETBAN           =32821;  // ����� �����, �� ������� �������� ������� (���� ����� ��������� � ����� ����)
 SEMNETTURN          =32822;  // ������� ������� ��������� (1-� ������ �����,2-� �����,3-� ������ � � �����)
 SEMSELFID           =32823;  // ����������� ������������� ������ GDF
 SEMNETCOSTRIBBACK   =32824;  // ��������� ����� ��������
 SEMNETRANKRIBBACK   =32825;  // ���� ����� ��������
                           
// ������ �������� ��� ������ �����, ����� ����������� � ���� ����� - � ������ � � �����         // 18/10/10
 SEMNETBANBEGIN      =32826;  // ����� �����, �� ������� �������� ������� �� ������ �����
 SEMNETBANEND        =32827;  // ����� �����, �� ������� �������� ������� �� ����� �����
 SEMNETINLIST        =32828;  // ������ �� ����� �������� �����
 SEMNETNOMECLATURE   =32829;  // ������������ �����

 SEMSERVICEFIRST     =32800;  // ������ ��������� ����������� ��������
 SEMSERVICELAST      =33000;  // �����  ��������� ����������� ��������

 SHEETFRAMEINCODE    = longint(15); // ���������� ��� ����� �����  05/02/02
 SHEETFRAMEEXCODE    = longint(91000000); // ������� ��� ����� �����

 type
TTASKBUTTONINFO  = packed record        // �������� LPARAM ��������� AW_INSERTTASKBUTTON     // 05/06/10
  aTask         : HINST;           // ������������� ���������� ������, ������� ����� ��������� ������� ��� 0
                                        // ���� ����� ������������� ������, �� Command �����
                                        // ������������ �� ��������� ���������� ������
  Command       : integer;              // ������������� ������,
                                        // ���� Command = (-1) - ����������� �����������
  Sibling       : integer;              // ������������� ������ � ������ ������������,
                                        // ����� ������� ����������� ����������� ������.
                                        // ���� Sibling ����� 0, �� ��� �������� ������ ����������� � ����� ������
  aBitmap       : HBITMAP  ;            // ������������� �������� �� ������ ��� 0
  Background    : integer;              // ������� ���������� ���� ������ (���� �� ����� 0),
                                        // ����������� ���� hBitmap �� ����
  State         : integer;              // ��������� ������ (0 - ������, 1 - ������, -1 - �� ��������)
  Enable        : integer;              // ����������� ������ (0 - ���������� ��� �������, 1 - ����� ����������)

// 11/12/2013  Hint          : array [0..255] of PWCHAR;     // �������� ��������� � ������ � UNICODE ��� 0
  Hint          : array [0..255] of WCHAR;     // �������� ��������� � ������ � UNICODE ��� 0                  // 11/12/2013

// 11/12/2013  Comment       : array [0..255] of PWCHAR;    // ������� ��������� � ������ � UNICODE ��� 0
  Comment       : array [0..255] of WCHAR;    // ������� ��������� � ������ � UNICODE ��� 0                    // 11/12/2013
end;

PTASKBUTTONINFO = ^TTASKBUTTONINFO;

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// // ��������� �������� Ole-�������
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 29/06/10

type
TOLELOADPARM = packed record
 PosX           : double   ;            // ���������� x � ������ �� ���������
 PosY           : double   ;            // ���������� y � ������ �� ���������

 Height         : integer;              // ������ Ole-������� � �� �� ����� � ��������� ��������
 Width          : integer;              // ������ Ole-������� � �� �� ����� � ��������� ��������

 PosRight       : GTKCHAR;               // 0 - ����� ����, 1 - ������ ����
 PosBottom      : GTKCHAR;               // 0 - ������� ����, 1 - ������ ����
 Reserve        : array [0..49] of GTKCHAR;  // ������������ �� 8

 Path           : array [0..259] of GTKCHAR; // ������ ���� � ������
end;
POLELOADPARM = ^TOLELOADPARM;


// 17/12/2010
// ��� ������ � ������� ���������� �������
 type
   THeightType = (
                   HT_ALTITUDE,  // ���������� ������
                   HT_RELATIVE   // ������������� ������
                  );

// 21/02/2011
// ����������� ���������� �������
type
 TOBJECT_DIRECT =
   (
   OD_UNDEFINED = 1,     // ����� ������� ��� �����������
   OD_RIGHT     = 2,     // ������ ������ �� ������� 
   OD_LEFT      = 4,     // ������ ����� �� �������
   OD_CLOCKWISE = 5,     // ��� ����� ������� ������� ��������� �� ������� �������  //23/01/12
   OD_ANTICLOCKWISE = 6  // ��� ����� ������� ������� ��������� ������ ������� �������  
   );


{ const  // 12/04/2011 ���������� generic ��������� � ������ system.pas
 mapapi_GENERIC_READ =  $100000;    // ���� ������ ������ �� ������
 mapapi_GENERIC_WRITE = $000001;    // O_WRONLY // ���� ������ �� ������
}
 const
   DOUBLENULL  = 1e-6;   // 04/03/2011

// 20/06/2011
type TGRIDPARM = packed record     // ��������� ���������� ������������� �����
  //  GRIDPARM()  { Step = 1000; Type = GRT_LINE;
  //                Image.Color = 0x040404; Image.Thick = 250;
  //                Size = 10*250; Shadow = 0; Under = 0; }
  // !!! ��� ������������� ���������� ��������� ��������:

  // Step = 1000;
  // grdType = GRT_LINE;
  // Image.Color = 0x040404;
  // Image.Thick = 250;
  // Size = 10*250;
  // Shadow = 0;
  // Under = 0;

  Step          : double ;      // ��� ����� � ������ �� ���������
  grdType       : integer;      // ��� �������� (�����, �����, ������)
  Image : packed record         // ���� � ������� ����� (RGB, ���)
    Color       : integer;
    Thick       : integer;
  end;
  Size          : integer;      // ������ �������� ���� "�����" � ��� (1:250)
  Shadow        : integer;      // ������� ��������������� �����������
  Under         : integer;      // ������� ����������� ��� ������
end;
 PGRIDPARM = ^TGRIDPARM;

 TGRIDPARMEX  = packed record     // ��������� ���������� ������������� �����
  //  GRIDPARM()  { Step = 1000; Type = GRT_LINE;
  //                Image.Color = 0x040404; Image.Thick = 250;
  //                Size = 10*250; Shadow = 0; Under = 0; }
  // !!! ��� ������������� ���������� ��������� ��������:

  // Step = 1000;
  // grdType = GRT_LINE;
  // Image.Color = 0x040404;
  // Image.Thick = 250;
  // Size = 10*250;
  // Shadow = 0;
  // Under = 0;

  Step          : double ;      // ��� ����� � ������ �� ���������
  grdType       : integer;      // ��� �������� (�����, �����, ������)
  Image : packed record         // ���� � ������� ����� (RGB, ���)
    Color       : integer;
    Thick       : integer;
  end;
  Size          : integer;      // ������ �������� ���� "�����" � ��� (1:250)
  Shadow        : integer;      // ������� ��������������� �����������
  Under         : integer;      // ������� ����������� ��� ������

  BeginX        : Double;         // ������ ��������� �����
  BeginY        : Double;
end;
 PGRIDPARMEX  = ^TGRIDPARMEX ;

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ��������� ������������� ������� ��������� � ���� ������ EPSG    // 11/02/11
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ����������� ��� ���������
type TAXISDIR =
(
   NORTH     = 1,  // �����
   SOUTH     = 2,  // ��
   WEST      = 3,  // �����
   EAST      = 4  // ������
);

type
 TEPSGRECTSYS = packed record

   RectSysCode  : integer;              // ��� ������������� ������� ��������� � ���� ������ EPSG
   GeodSysCode  : integer;              // ��� ������� ������������� ������� ��������� � ���� ������ EPSG
   UnitCode     : integer;              // ��� ������� ��������� � ���� ������ EPSG
   ProjectionCode: integer;             // ��� �������� � ���� ������ EPSG (��. EPSGTRANSVERSEMERCATOR � �.�.)
                                        // � �������� EPSG - ��� ������ �������������� ���������
                                        // (���� COORD_OP_METHOD_CODE � ������� Coordinate_Operation Method)
   XDirection   : integer;             // ����������� ��� � (������ ����������)    // 21/06/2016
   YDirection   : integer;             // ����������� ��� Y (������ ����������)    // 21/06/2016
   RectSysName: array [0..255] of GTKCHAR;   // �������� ������������� ������� ��������� � ���� ������ EPSG
end;
  PEPSGRECTSYS = ^TEPSGRECTSYS;

  
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ������ �������� ��� ������ �����
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
type
 TSHEETNAMES = packed record
                                  // 14/10/14      // 20/01/16  // 16/06/2016

  Nomenclature: array [0..231] of WCHAR ;   // ������������ ����� � UTF-16 � ���������� �����
  Reserve1: array [0..23] of WCHAR ;        // ������, ����� ����
  ListName: array [0..231] of WCHAR ;       // ��� ����� ����� � UTF-16 � ���������� �����
  Reserve2: array [0..23] of WCHAR ;        // ������, ����� ����
  FileName: array [0..231] of WCHAR ;       // ��� ������ ����� ����� � UTF-16 � ���������� �����
  Ident: array [0..33] of GTKCHAR;         // ������������� ����� ����� GUID (32 ����������������� �������)
  Reserve3: array [0..13] of GTKCHAR;        // ������, ����� ����
 
end;
  PSHEETNAMES = ^TSHEETNAMES;

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ��������� ������������� ������� ��������� � ���� ������ EPSG      // 11/02/11
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

type
  TEPSGGEODSYS = packed record

   GeodSysCode  : integer;                      // ��� ������������� ������� ��������� � ���� ������ EPSG
   UnitCode     : integer;                      // ��� ������� ��������� � ���� ������ EPSG
   EllipsoidCode: integer;                      // ��� ���������� � ���� ������ EPSG
   DatumCode    : integer;                      // ��� ������ � ���� ������ EPSG
                                                // � �������� EPSG - ��� �������������� ���������
                                                // (���� COORD_OP_CODE � ������� Coordinate_Operation,
                                                // ���� ������� ���������, �� ��������� ���������)
   GeodSysName  : array [0..255]of GTKchar;     // �������� �������� � ���� ������ EPSG
   EllipsoidName: array [0..255]of GTKchar;     // �������� ���������� � ���� ������ EPSG
   DatumName    : array [0..255]of GTKchar;     // �������� ������ � ���� ������ EPSG
end;
  PEPSGGEODSYS = ^TEPSGGEODSYS;

// ��� ������� ���������
type TEPSGUNITTYPE =
(
  LINEAR = 0,  // �������� (����������� ������� - �����)
  ANGLE  = 1,  // �������  (����������� ������� - �������)
  SCALE  = 2  // ���������� �����������
);

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ������� ��������� � ���� ������ EPSG                        // 11/02/11
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// ������� ��������� (�� ����������� ��������, ��� � ������� ����� �������� ������������ ������ � �������)
type
  TEPSGMEASUNIT = packed record

   UnitCode     : integer;              // ��� ������� ���������
   UnitType     : integer;              // ���     // 21/06/2016
   Factor       : double;               // ����������� ��� ���������� � ����������� ������� ���������
                                        // (�����, �������, �����������)
   Name         : array [0..127]of GTKCHAR; // �������� ������� ���������
end;
  PEPSGMEASUNIT = ^TEPSGMEASUNIT;

  HMAPREG = POINTER;        // ������������� ������ ���������� ������ �������
  HPAINT  = POINTER;        // ������������� TPaintControl   // 16/06/2016
  

//  type HWMS = HHANDLE;           // ������������� WMS-������� 30/04/2012 �� ������ ���� ������ ����
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//    ��������� ��������� �������� ��������, ������������
//        ��� ���������� �������� � �������� �� �����
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
type TCOMMITOBJECTPARM = packed record                         // 11/04/11

  //  COMMITOBJECTPARM () { memset(this, 0, sizeof(*this)); }

  MinCutLength  : double;          // ����������� ����� ������� ������� �������
                                // (� ������ �� ���������)

  // ������������ ��� �������������� ���������� ����� ������� ��� ����������
  // �������� � ��������� �������� � �������� �� ������ � ��������:
  // mapCommitWithPlace(), mapCommitWithPlaceAsNew().
  // ��� ����������� �������, ����� �������� ����� MinLineLength,
  // ��������� ��� ������ �����.

  MinCutLengthOnBorder  : double;  // ����������� ����� ������� ������� �������,
                                   // ���������� �� ����� (� ������ �� ���������)

  // ������������ ��� ��������������� �������� �������� ��������
  // � ����������� ��������� ��������, ����������� ��� �������
  // �������� �� ����� � ��������:
  // mapCommitWithPlace(), mapCommitWithPlaceAsNew().
  // ��� ������������ ���������� ��������� ���������� �������:
  // 1) �����������, ��������� ����� ����� ����� �����,
  //    ����� ����� �� ������ �������� ����� MinCutLengthOnBorder;
  // 2) ���������������, ��������� ����� ����� ����� �����,
  //    ����� ���� �������� �������� ����� MinCutLengthOnBorder.

  Reserve : array [0..47] of GTKCHAR;             // ������
end;
  PCOMMITOBJECTPARM = ^TCOMMITOBJECTPARM;

const
 //GRADINRAD = 57.29577951308232l;  // ���������� �������� � ����� �������  // 29/07/2011
 GRADINRAD = 57.29577951308232;  // ���������� �������� � ����� �������

type TSEEKINAREA =               // 26/04/11
(
   SO_SEEKBYDISTANCE     = 0,  // ����� �� ����������
   SO_SEEKINSIDEANDCROSS = 1,  // ����� ������ �������, ������� ����������� ������� �������
   SO_SEEKINSIDEONLY     = 2,  // ����� ������ �������, �������� ����������� �������
   SO_SEEKOUTSIDEONLY    = 4  // ����� ��� �������, �������� ����������� �������
);

type TMCMODE =               // ������ ������� � ������                 // 21/09/12
(
  TMM_READ         = 1,    // ��������� ������ ������
  TMM_WRITE        = 2,    // ��������� �������������� ������
  TMM_COPY         = 4,    // ��������� ����������� ������ �� �������
  TMM_NOTPRINT     = 8,    // �������� ����� �� ������ �� �������
  TMM_CACHECODING  = 16,   // ��������� ���
  TMM_STREAMCODING = 32,   // ��������� ��������
  TMM_TEMP         = 64,   // ������� ��������� ����� (������� ��� ��������)
  TMM_HIDEPASP     = 128,  // �������� ����� �������� �����           // 16/11/12
  TMM_RSCEDIT      = 256,  // ��������� �������������� �������������� // 27/12/12
  TMM_DOCSTORAGE   = 512,  // ��������� �������� ���������� � ������  // 09/02/13
  TMM_ADMIN        = 1024, // ��������� ����������������� ������
  TMM_MONITOR      = 2048,  // �������� ���������� ��������� �������
  
  TMM_FOLDEREDIT   = 4096, // ��������� �������������� ������� ������ � �����  // 18/07/14  // 23/06/2016
  TMM_GEODB        = 8192 // ��������� ����������� � ����� ������ �� � ���    // 14/08/15  // 23/06/2016
);

type OBM_TYPE =             // ����� ������� ��������   // 23/06/2016
(
  OBM_RECTANGLE    = 1,    // ������� ����� ����� ��������������� (�������������) ��������������
  OBM_CIRCLE       = 2    // ������� ����� ����� ����������
);

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// // ��������� ���������
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 31/01/08
type TMEDPROPERTY = packed record
  FindRadius    : double;       // ������ ������
  Step          : double;       // ��� ����������
  Filter        : double;       // ����� ����������
  ZoneWide      : double;       // ������ ����
  AdjustValue   : double;       // ����� ������������
  SplineValue   : double;       // ����� �����������
  TakeRadius    : double;       // ������ ������� ����� � ����� //23/03/06

  IsSetup       : integer;      // ��������� ������ ��������� ?
  IsDevide      : integer;      // ��������� �� ������   ?
  IsFrameCut    : integer;      // �������� �� �����   ?
  IsEditFrame   : integer;      // ������������� ����� ?

  IsSemantic    : integer;      // ����������� ��������� ?
  IsClearSelect : integer;      // ���������� ��������� ?
  Is3dData      : integer;      // ���������� ������� ?
  IsCursor      : integer;      // ���� ������� ?

  IsNext        : integer;      // ������ �����������?
  IsSeek        : integer;      // ������ �����������?
  IsCross       : integer;      // ������� ����� �����������?
  FilterType    : integer;      // ��� ����������

  IsGroup       : integer;      // ������������ ������?
  FindSector    : integer;      // ������ ������
  Spy           : integer;      // ������� �� ��������������?
  Speed         : integer;      // �������� ������������ (%)

  ViewSemanticCode: integer;    // ��������������� ���������
  IsStatistic   : integer;      // ���������� ����������
  PointInSpline : integer;      // ����� � �������
  AutoCode      : integer;      // ��� ����� ���������� ���������

  IsTrace       : integer;      // �����������
  IsPanel       : integer;      // ��������������� ������  // 03/10/06

  MapSplineType : integer;      // ��� ������� ��� MAP   //11/08/03
  SitSplineType : integer;      // ��� ������� ��� SIT   //11/08/03

  Modelcount    : integer;      // ���������� �������    //03/12/04
  MeasureUnit   : integer;      // ������� ���������     // 29/09/05

  BigButton     : integer;      // ������ ������         //17/01/08
  IsDateTime    : integer;      // ������ ����� � ���� ��������/�������������� //21/09/2009

  // ��������� ������

  Exc_Step      :double;        // ���������� ����� �������� (��)
  Exc_Lenght    :double;        // ����� ���. ���. � (��)
  Exc_MaxWidth  :double;        // ����. ������ ��� ����.������� � (��)

  Exc_Proport   : integer;      // ���������� ���������������� �����?
  Exc_Numerator : integer;      // ��������� ���������                     //06/05/11
  Exc_Denominator: integer;     // ����������� ���������
  Exc_Pitching  : integer;      // �����������?


  MoveMaxCount  : integer;      // ���������� ���������������� ��� �����������
  MoveViewType  : integer;      // 0 - ���, 1- ������, 2 - ��������
end;
  PMEDPROPERTY = ^TMEDPROPERTY;

//******************************************************************
// �������� ����������������� ������������� ���������
//******************************************************************

// ��� ��������� �����������������
type TLINEARTRANSFTYPE =
(
   LT_OFFSET            = 0, // �����
   LT_OFFSETROTATE      = 1, // �����, �������
   LT_OFFSETROTATESCALE = 2, // �����, �������, �������
   LT_AFFINE            = 3 // �������� �����������������
);

// ��������� ��������� �����������������
type TLINEARTRANSFPARM = packed record
   a_orig,
   b_orig,
   c_orig,
   d_orig,
   e_orig,
   f_orig : double; // �� �������� � �������� X = a + bx + cy; Y = d + ex + fy
   A_out,
   B_out,
   C_out,
   D_out,
   E_out,
   F_out : double; // �� �������� � �������� x = A + BX + CY; y = D + EX + FY
   Scale,
   Rotate    : double;    // ����������� ��������������� (= ���������� � ���.�� / ���������� � ���.��)
                           // � ���� �������� � ��������
                           // (��� �������� - ������� ������� � �������, �.�. ��� ��������
                           // ��� ������ ��� � � Y)
end;
 PLINEARTRANSFPARM = ^TLINEARTRANSFPARM;
{$IFDEF CPUX64}                                           // 11/12/2013
 type HTRANSF = Int64;   // ������������� ��������� ����������������� ���������
{$ELSE}
 type HTRANSF = integer; // ������������� ��������� ����������������� ���������
{$ENDIF}
//******************************************************************
// ���������� ����������������� ������������� ���������
//******************************************************************
// ��� ����������� �����������������
type TNONLINEARTRANSFTYPE =                                         // 06/09/10
(
   NT_LINEARSHEET    = 0,     // �������� ��������� ����
   NT_NONLINEARSHEET = 1,     // ���������� ��������� ����
   NT_POLYNOM4       = 4,     // ������� 4  ������������
   NT_POLYNOM5       = 5,     // ������� 5  �������������
   NT_POLYNOM6       = 6,     // ������� 6  �������������
   NT_POLYNOM7       = 7,     // ������� 7  �������������
   NT_POLYNOM8       = 8,     // ������� 8  �������������
   NT_POLYNOM9       = 9,     // ������� 9  �������������
   NT_POLYNOM10      = 10,    // ������� 10 �������������
   NT_POLYNOM11      = 11,    // ������� 11 �������������
   NT_POLYNOM12      = 12,    // ������� 12 �������������
   NT_POLYNOM13      = 13,    // ������� 13 �������������
   NT_POLYNOM14      = 14,    // ������� 14 �������������
   NT_POLYNOM15      = 15,    // ������� 15 �������������
   NT_POLYNOM16      = 16,    // ������� 16 �������������
   NT_POLYNOM17      = 17,    // ������� 17 �������������
   NT_POLYNOM18      = 18,    // ������� 18 �������������
   NT_POLYNOM19      = 19,    // ������� 19 �������������
   NT_POLYNOM20      = 20,    // ������� 20 �������������
   NT_POLYNOM21      = 21,    // ������� 21 �������������
   NT_POLYNOMRANK2   = NT_POLYNOM6,  // ������� 2 �������
   NT_POLYNOMRANK3   = NT_POLYNOM10, // ������� 3 �������
   NT_POLYNOMRANK4   = NT_POLYNOM15, // ������� 4 �������
   NT_POLYNOMRANK5   = NT_POLYNOM21  // ������� 5 �������
);

type HNONLINETRANSF = integer; // ������������� ����������� ����������������� ���������

 type SEEKTYPE =        // ������� ������ ��'�����
 (
    WO_FIRST  = 0,         // ������ � �������
    WO_LAST   = 2,         // ��������� � �������
    WO_NEXT   = 4,         // ��������� �� ��������� �����
    WO_BACK   = 8,         // ���������� �� ����� ����������
    WO_CANCEL = 16,        // ������� ��������� �������
    WO_INMAP  = 32,        // ������ �� ����� ����� (��������������� HSELECT)
    WO_VISUAL = 64,        // ����� ������ ����� ������� ��������
    WO_VISUALIGNORE = 128 // ����� ����� ���� �������� ��� ����� ���������
 );

type
 OGCSERVICETYPE =
  (
  OST_GML  = 0,           // ����� ������ GML          // 16/06/2016
  OST_WFS  = 1,           // GML ��� �������� �� WFS
  OST_JSON = 2            // JSON  
  );
  
  
  type OGCSERVICEFLAG =      // ����� ����������� ���������� �� �������     // 16/06/2016
(
  OSF_METRIC      =   1,  // ����� �������  � �������� �������
  OSF_SEMANTIC    =   2,  // ����� ��������� � �������� �������
  OSF_MEASURE     =   4,  // ����� ������� � ����� (���������) �������
  OSF_MEASURE3D   =   8,  // ����� ������� � ����� (���������) ������� � ������ ������� ���������
  OSF_CENTER      =  16,  // ����� ������ �������
  OSF_SCALEBORDER =  32,  // ����� ������� � ������ ������� ��������� ������� (�������� �����������)
  OSF_POINT       =  64,  // ����� ������ ����� �������
  OSF_SEMANTICNAME=  128, // ����� ����� ��������� ��� ���������� ����� ���������
  OSF_CUTMETRIC   =  256, // ���������� ������� �������� �� ������� ������ �������� �����
  OSF_BBOX        =  512  // ����� ��������� �������
);


// ������������� GML ������
type
{$IFDEF CPUX64}                                           // 11/12/2013
 HGML = Int64;
{$ELSE}
 HGML = cardinal;
{$ENDIF}

// ������������� GML ������
type
{$IFDEF CPUX64}                                           // 16/06/2016
 HGMLCLASS = Int64;
{$ELSE}
 HGMLCLASS = cardinal;
{$ENDIF}

const
// ������� � ����� ����� ��� WMS
 WMAPWMS = 'L"WMS#"';
 MAPWMS  = 'WMS#';
 WMSSIZE = 4;

 // ������� � ����� ����� ��� WMTS         // 04/04/2014
 WMAPWMTS = 'L"WMTS#"';
 MAPWMTS  = 'WMTS#';
 WMTSSIZE = 5;

// ������� � ����� ����� ��� WFS  // 04/04/2014
 WMAPWFS  = 'L"WFS#"';
 MAPWFS   = 'WFS#';
 WFSSIZE  = 4;

// ������� � ����� ����� ��� WCS  // 04/04/2014
 WMAPWCS  = 'L"WCS#"';
 MAPWCS   = 'WCS#';
 WCSSIZE  = 4;



const
// ������������� �������  // 31/08/11
 VIFIRST = 1;
 NDVI    = 1;
 RVI     = 2;
 IPVI    = 3;
 DVI     = 4;
 SAVI    = 5;
 MSAVI2  = 6;
 VILAST  = 6;

type
 TRGBQUAD = packed record
   rgbBlue     :BYTE;
    rgbGreen    :BYTE;
    rgbRed      :BYTE;
    rgbReserved :BYTE;
end;
// ������, ����������� ��� ����������� �������������� ������� // 31/08/11
type TVEGINDEX = packed record

     Index       : integer;         // ������������ ������ ��������� (NDVI, RVI � �.�.)
     ColorCount  : integer;    // ���������� ������ � ������� ��������� �������������� �������
     Palette     : array [0..255] of TRGBQUAD;  // ������� ��������� �������������� �������
     RedBandNum  : integer;    // ����� �������� ������
     NirBandNum  : integer;    // ����� ������������� ������
     L           : double;             // ����������� ��� SAVI (0..1)
     Reserv      : array [0..31] of double;
end;
PVEGINDEX =^TVEGINDEX;

// ��������� ������ ������ ���������� ���������� �������� Transverse Mercator  // 12/07/11
type TCALCTRANSMERCATORPARM =  packed record
   IsCalcM        : integer;  // ����� �� ��������� ������� (���� ���, �� M ������ ���� ����������)
   IsSetM         : integer;   // ����������� �� ��������������� �������� �������� (������������ ���� ���� ���������)
   M              : double ;        // �� ����� - ��������������� �������, �� ������ �����������
   IsCalcLo       : integer; // ����� �� ��������� ������� ������� ��������� (���� ���, �� L0 ������ ���� ����������)
   IsSetLo        : integer;  // ����������� �� ��������������� �������� ������� ������� ��������� (������������ ���� ���� ���������)
   Lo             : double;       // �� ����� - ��������������� ������� ������� ���������, �� ������ �����������
   IsCalcBo       : integer; // ����� �� ��������� ������ ����� ������� (���� ���, �� �0 ������ ���� ����������)
   IsSetBo        : integer;  // ����������� �� ��������������� �������� ������ ����� ������� (������������ ���� ���� ���������)
   Bo             : double;       // �� ����� - ��������������� ������ ����� �������, �� ������ �����������
   IsCalcDx       : integer; // ����� �� ��������� ����� �� � (���� ���, �� Dx ������ ���� ����������)
   IsSetDx        : integer;  // ����������� �� ��������������� �������� ������ �� � (������������ ���� ���� ���������)
   Dx             : double ;       // �� ����� - ��������������� �������� ������ �� �, �� ������ �����������
   IsCalcDy       : integer; // ����� �� ��������� ����� �� Y (���� ���, �� Dy ������ ���� ����������)
   IsSetDy        : integer;  // ����������� �� ��������������� �������� ������ �� Y (������������ ���� ���� ���������)
   Dy             : double ;       // �� ����� - ��������������� �������� ������ �� Y, �� ������ �����������
   IsCalcAn       : integer; // ����� �� ��������� ���� �������� ���(���� ���, �� An ������ ���� ����������)
   IsSetAn        : integer;  // ����������� �� ��������������� �������� ���� �������� ��� (������������ ���� ���� ���������)
   An             : double ;       // �� ����� - ��������������� �������� ���� �������� ���, �� ������ �����������
end;
PCALCTRANSMERCATORPARM = ^TCALCTRANSMERCATORPARM;

// ����������� �����  // 07/09/11
type TCOMPLEX = packed record
 re    : double ; // �������������� �����
  im    : double ; // ������ �����
//  inline void operator = (const COMPLEX &y) {re = y.re; im = y.im;} // ???
end;
PCOMPLEX = ^TCOMPLEX;



type  // ��������� ��� ������ OLE (������ ������ ������)  // 04/05/2012
  TOLE_PRINTPARM = packed record
    Rect      : TRECT;            // ������� ����������� - ������� � ������
    Shift     : TPOINT;           // �������� �� ������ ����� � ������
    Scale     : double;           // ����� �������� ���������� � ������� ������
    ViewScale : double;           // ����������� ��������������� ������������ �������� ��������
  end;
  POLE_PRINTPARM = ^TOLE_PRINTPARM;

  PREFERENCESYSTEM = ^TREFERENCESYSTEM;   // 20130206 Korolev
  TREFERENCESYSTEM = packed record
    NameSystem : array [0..511] of GTKCHAR;
    Comment : array [0..511] of GTKCHAR;
    Ident : array [0..63] of GTKCHAR;
    CodeEpsg : longint;
  end;

TREFERENCESYSTEMUN   = packed record             // 01/04/14   // 23/06/2016
    NameSystem : array [0..511] of WCHAR;
    Comment    : array [0..511] of WCHAR;
    Ident      : array [0..63]  of WCHAR;
    CodeEpsg   : longint;
    Reserve    : longint;

  end;
PREFERENCESYSTEMUN = ^TREFERENCESYSTEMUN;
 
  

  PORGANIZATION = ^TORGANIZATION;    // 20130206 Korolev
  TORGANIZATION = packed record
    Name : array [0..255] of GTKCHAR;                // �������� �����������
    Phone : array [0..31] of GTKCHAR;                // �������
    Facsimile : array [0..31] of GTKCHAR;            // ����
    City : array [0..31] of GTKCHAR;                 // ���������� �����
    Adminarea : array [0..31] of GTKCHAR;            // ���������������� �����
    Postalcode : array [0..31] of GTKCHAR;           // �������� ������
    Country : array [0..31] of GTKCHAR;              // ������
    Email : array [0..63] of GTKCHAR;                // ����������� �����
    Reserve : array [0..255] of GTKCHAR;
  end;

  PAGENT = ^TAGENT;                         // 20130206 Korolev
  TAGENT = packed record
    Fio : array [0..255] of GTKCHAR;        // �������  ���  ��������
    NameOrg : array [0..255] of GTKCHAR;    // �������� �����������
    Phone : array [0..31] of GTKCHAR;       // �������
    Facsimile : array [0..31] of GTKCHAR;   // ����
    Email :array [0..63] of GTKCHAR;        // ����������� �����
  end;

// �������� � ����������, ������� �������� �������� ������ ������ � ���� ����������
TAGENTEX = packed record             // 31/07/15   // 23/06/2016
    Fio : array [0..255] of WCHAR;        // �������  ���  ��������
    NameOrg : array [0..255] of WCHAR;    // �������� �����������
Position : array [0..255] of WCHAR;   // ���������
    Phone : array [0..31] of WCHAR;       // �������
    Facsimile : array [0..31] of WCHAR;   // ����
    Email :array [0..63] of WCHAR;        // ����������� �����
Reserve : array [0..255] of WCHAR;    // 

  end;  
PAGENTEX = ^TAGENTEX;

// �������� �� �����������-������������� ������ ������   // 23/06/2016
TORGANIZATIONEX = packed record 

    Name : array [0..255] of WCHAR;                // �������� �����������
    Phone : array [0..31] of WCHAR;                // �������
    Facsimile : array [0..31] of WCHAR;            // ����
    City : array [0..31] of WCHAR;                 // ���������� �����
    Adminarea : array [0..31] of WCHAR;            // ���������������� �����
    Postalcode : array [0..31] of WCHAR;           // �������� ������
    Country : array [0..31] of WCHAR;              // ������
    Email : array [0..63] of WCHAR;                // ����������� �����
    Reserve : array [0..255] of WCHAR;

  end;  
PORGANIZATIONEX = ^TORGANIZATIONEX;  

// ���������� ������ � �������
TRMF_METADATA  =  packed record 

     Totalsize : double;              // ����� ������ ������ � K������ (��������� �������� mapGetDataSize(...))
     Ident : array[0..63] of WCHAR;            // ������������� ������ � ������� GUID
     WestLongitude: array[0..31] of WCHAR;      // ������� ���-��������� ���� ��������� � �������� WGS-84
     EastLongitude: array[0..31] of WCHAR;      // ������ ���-��������� ���� ��������� � �������� WGS-84
     SouthLatitude: array[0..31] of WCHAR;     // ������� ������-���������� ���� ��������� � �������� WGS-84
     NorthLatitude: array[0..31] of WCHAR;      // ������ ������-���������� ���� ��������� � �������� WGS-84

     Scale: array[0..31] of WCHAR;           // ����� ������ � ����� ������� (������)
// ����� ������ � ����� �������� �������
     Nomenclature: array[0..255] of WCHAR;      // �������� �������� �������
     Createdate: array[0..31] of WCHAR;         // ���� ��������/���������� ������ (�������) - YYYY-MM-DD
     Format: array[0..15] of WCHAR;            // ������������� ������� ������ (MTW, RSW ...)
     Filename: array[0..1023] of WCHAR;         // ������ �� �������� ���� ������   (noginsk.mtw)
     Comment: array[0..255] of WCHAR;           // �����������, ������� ��������
     Lineage: array[0..511] of WCHAR;           // ����� �������� �� �������� ������, �� ������� ������� ����� ��� ����, � � ���������� �� ���������
     Areadate: array[0..15] of WCHAR;           // ��� ��������� ���������

     Security: array[0..127] of WCHAR;           // ���� ����������� (����������)
     Datatype: array[0..127] of WCHAR;           // ��� ������ - ����������� ���������� ������, ��������������, ������������������ ������ (101-106)
// ��� ������ - ���������� ������� �����, TIN-������, MTD-������ (������ �����) (201-203)
// ��� ������ ���

     SatName: array[0..63] of WCHAR;            // �������� ��� �������� ����������� ��������
     CloudState: array[0..31] of WCHAR;         // ������� ������, �������� �����������
     SunAngle: array[0..31] of WCHAR;           // ������ ������ (�������)
     ScanAngle: array[0..31] of WCHAR;          // ���� ������ (�������)

     Epsgcode           : integer;               // ��� EPSG ������� ��������� �����
     Reserve: array[0..251] of WCHAR;
  end;  
PRMF_METADATA = ^TRMF_METADATA;  


// ��� ������, ����������� ��� ���������� ������������ �����������
const
 KM_FIRST       = 0;
 KM_LAST        = 7;
 KM_SPHEROIDAL  = 0;  // �����������
 KM_CIRCULAR    = 1;  // ��������
 KM_TETRASPHER  = 2;  // ����������������
 KM_PENTASPHER  = 3;  // ����������������
 KM_EXPONENT    = 4;  // ����������������
 KM_GAUSS       = 5;  // ��������
 KM_RATSQUARE   = 6;  // ������������ ������������
 KM_HOLE        = 7;  // ������� ����

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ��������� �������� TIFF �������                             // 25/03/2014
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// ��� ������� (����� ������ ���������� ���� ������� �� ������ ������������ ����������,
// �������� ������ ������� � ����� �������� ���������� CREATETIFPARM.BitInBand)
const
 PT_MIN      = 0;
 PT_UNKNOWN  = 0;
 PT_BYTE     = 1;    // 8-�� ��������� ����� ����� ��� �����
 PT_UINT16   = 2;    // 16-�� ��������� ����� ����� ��� �����
 PT_INT16    = 3;    // 16-�� ��������� ����� ����� �� ������
 PT_UINT32   = 4;    // 32-� ��������� ����� ����� ��� �����
 PT_INT32    = 5;    // 32-� ��������� ����� ����� �� ������
 PT_FLOAT32  = 6;    // 32-� ��������� ����� c ��������� �������
 PT_FLOAT64  = 7;    // 64-� ��������� ����� � ��������� �������
 PT_CINT16   = 8;    // 16-�� ��������� ����������� �����
 PT_CINT32   = 9;    // 32-� ��������� ����������� �����
 PT_CFLOAT32 = 10;    // 32-� ��������� ����������� ����� c ��������� �������
 PT_CFLOAT64 = 11;    // 64-� ��������� ����������� ����� � ��������� �������
 PT_MAX      = 11;

// ������������� ������� ��� 3 (CMYK - 4) ��������� �������
 CS_RGB      = 2;
 CS_CMYK     = 5;
 CS_YCBCR    = 6;
 CS_CIELAB   = 8;
 CS_ICCLAB   = 9;
 CS_ITULAB   = 10;

// ������� �������� BIGTIFF
 BT_NO       = 0;  // �� ���������
 BT_YES      = 1;  // ���������
 BT_IFNEEDED = 2;  // ���������, ���� ������ ������ 4 �� � ��� ������
 BT_IFSAFER  = 3;  // ���������, ���� ������ ������ 2 �� � ������ �� �����������

// ���� ���������� � TIF ������
 NOTCOMPRESS    = 1;
 LZW            = 5;
 CCITT          = 2;
 CCITTFAX3      = 3;
 CCITTFAX4      = 4;
 JPEG           = 6;
 DEFLATE        = 8;
 PACKBITS       = 32773;

// ��������� �������� �����������������
type
  PAFFINCOEF = ^TAFFINCOEF;
  TAFFINCOEF = packed record
  A0, A1, A2, B0, B1, B2:Double;
  end;
// ��������� ����������� TIF �����
  PCREATETIFPARM = ^TCREATETIFPARM;
  TCREATETIFPARM = packed record
       BandCount: Integer;    // ���������� �������
       BitInBand: Integer;    // ��� �� �����
       PixelType: Integer;    // ��� ������� (PT_BYTE � �.�.)
       Compress: Integer;     // ��� ���������� (��. NOTCOMPRESS � �.�.)
                        // JPEG - �������� ������ ��� 3 � 1 ��������� 8, 16 ������ �������
                        //      - ��� 3 ��������� �������� ColorSpace = CS_RGB ��� CS_YCBCR (������ ������, �� ���������)
                        //      - ��� CS_YCBCR � ����� ����� ������������ RGB
       IsPredictor: Integer;  // ��� LZW � DEFLATE ���������� - ������� ������ �����������
                        // 0 - ��������� ���� �����
                        // 1 - ��������� ����������� ����� ��������� ��������� � ������
       JPEGQuality: Integer;  // �������� JPEG ������ (1-100) (�� ��������� 75)
       ColorSpace: Integer;   // ������������� ������� ��� 3 ��������� ������� (CMYK ��� 4 ���������) ��. CS_RGB � �.�.
       DeflateLevel: Integer; // ������� DEFLATE ������ (1-9) (�� ��������� 6)
       TileWidth: Integer;    // ������ ����� � ��������
       TileHeight: Integer;   // ������ ����� � ��������
       BigTIFF: Integer;      // ������� �������� BIGTIFF (��. BT_NO � �.�.)
       IsPlane: Integer;      // ������� �������� ����������� �� ���������� (0 - RGB RGB ..., 1 - RR...BB...GG...)
                        // ��� JPEG ������ � �������� ������ YCBCR ������ ���� = 0
       IsAlpha: Integer;      // ������� ������������� ����� ������
       Palette : array[0..255]of RGBQUAD; // ������� 1,4,8 ������ �������
       Reserved: array[0..63] of Integer;
  end;
  // ��������� ����������� TIF �����
  PCREATETIFPARMEX = ^TCREATETIFPARMEX;
  TCREATETIFPARMEX = packed record
       BandCount: Integer;    // ���������� �������
       BitInBand: Integer;    // ��� �� �����
       PixelType: Integer;    // ��� ������� (PT_BYTE � �.�.)
       Compress: Integer;     // ��� ���������� (��. NOTCOMPRESS � �.�.)
                        // JPEG - �������� ������ ��� 3 � 1 ��������� 8, 16 ������ �������
                        //      - ��� 3 ��������� �������� ColorSpace = CS_RGB ��� CS_YCBCR (������ ������, �� ���������)
                        //      - ��� CS_YCBCR � ����� ����� ������������ RGB
       IsPredictor: Integer;  // ��� LZW � DEFLATE ���������� - ������� ������ �����������
                        // 0 - ��������� ���� �����
                        // 1 - ��������� ����������� ����� ��������� ��������� � ������
       JPEGQuality: Integer;  // �������� JPEG ������ (1-100) (�� ��������� 75)
       ColorSpace: Integer;   // ������������� ������� ��� 3 ��������� ������� (CMYK ��� 4 ���������) ��. CS_RGB � �.�.
       DeflateLevel: Integer; // ������� DEFLATE ������ (1-9) (�� ��������� 6)
       TileWidth: Integer;    // ������ ����� � ��������
       TileHeight: Integer;   // ������ ����� � ��������
       BigTIFF: Integer;      // ������� �������� BIGTIFF (��. BT_NO � �.�.)
       IsPlane: Integer;      // ������� �������� ����������� �� ���������� (0 - RGB RGB ..., 1 - RR...BB...GG...)
                        // ��� JPEG ������ � �������� ������ YCBCR ������ ���� = 0

       Palette : array[0..255]of RGBQUAD; // ������� 1,4,8 ������ �������
       IsAlpha: Integer;      // ������� ������������� ����� ������
       Reserved: array[0..64] of Integer;
  end;

const
//********************************************************************
// ��������� // 13/02/13
//********************************************************************

// ������� (������ �������� ��� ����� ��������) ����������� ���������� � ����������
 COVFEATUREMIN = 1;
 COVFEATUREMAX = 3;
 COVFEATURE1   = 1;
 COVFEATURE2   = 2;
 COVFEATURE12  = 3;

//********************************************************************
// ������������� ������ � �������  // 07/04/2014
//********************************************************************

// ����� ���������� �������� � �������

type
TMTRCLASS  = packed record

  Min:Double ;            // �������� �������� ��������� �������, ������������� ������ (������� Min, �������� Max)
  Max:Double ;            // ��� ������� �����      - �������� �����
                         // ��� ������� �������    - �������� ��������
                         // ��� ����������� ������ - �������� ��������
                         // ��� RGB ������         - ������� - ������� �� R,G,B
  Excode:Integer;         // ��� ������������ �������
  Color:Integer;          // ���� ����������� ���������
  SemanticNumber:Integer; // ��� �������� ��������� � ������� ������������ ����� ������ (���� 0, �� �� �������)
                         // (� ��������� ��������� ����� ������ ������� � 1)
  SemanticMin:Integer;    // ��� �������� ��������� ��� ������ Min ������ (���� 0, �� �� �������)
  SemanticMax:Integer;    // ��� �������� ��������� ��� ������ Max ������ (���� 0, �� �� �������)
  SemanticColor:Integer;  // ��� �������� ��������� ��� ������ ����� ����������� ��������� ������ (���� 0, �� �� �������)

  end;
PMTRCLASS = ^TMTRCLASS;

//********************************************************************
// ���������� ���� ������ �������   // 07/06/2016
//********************************************************************

type
TBUILDZONEPARMEX  = packed record

  Radius:Double ;
  FlagDial:Integer;
  Check:Integer;
  Size:Integer;            // 1 - ������ � �
                       // 1000 - ������ � ��
                       // 2 - �� �� �����
  TypeZone:Integer;        // ��� ���� ��� ��������� ��������
                       // 0 - �������
                       // 1 - ����������
                       // 2 - ������� � ����������
  EnableType:Integer;      // 0 - Info �� �������
                       // 1 - Info �������
  FormZone:Integer;        // ��� ���� ����
                       // 1 - ������������
                       // 0 - ���� �����
  end;
PBUILDZONEPARMEX = ^TBUILDZONEPARMEX;


implementation

end.
