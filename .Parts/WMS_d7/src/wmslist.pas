
{*******************************************************}
{                                                       }
{                   XML Data Binding                    }
{                                                       }
{         Generated on: 29.07.2016 16:25:32             }
{       Generated from: W:\WMS\tests\data\wmslist.xml   }
{   Settings stored in: W:\WMS\tests\data\wmslist.xdb   }
{                                                       }
{*******************************************************}

unit wmslist;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLGeoportalType = interface;
  IXMLInternetConnectType = interface;
  IXMLDefinesType = interface;
  IXMLAlgorithmType = interface;
  IXMLUTMSType = interface;
  IXMLPortalStadndartType = interface;
  IXMLTileMatrixSetType = interface;
  IXMLTileMatrixType = interface;
  IXMLBoundingBoxType = interface;
  IXMLWGS84BoundingBoxType = interface;
  IXMLMatrixBoundType = interface;
  IXMLZoomType = interface;
  IXMLWMS_WMTSType = interface;
  IXMLServerType = interface;
  IXMLWFSType = interface;
  IXMLServerType2222222 = interface;
  IXMLWCSType = interface;
  IXMLServerType222222222 = interface;
  IXMLDialogType = interface;
  IXMLPortalType = interface;
  IXMLParamsType = interface;
  IXMLProxyserverType = interface;
  IXMLTopicType = interface;
  IXMLTopicTypeList = interface;
  IXMLLayerType = interface;
  IXMLLayerTypeList = interface;
  IXMLConnectedStingType = interface;
  IXMLIDType = interface;
  IXMLIDTypeList = interface;
  IXMLNameType = interface;
  IXMLNameTypeList = interface;
  IXMLMatrixType = interface;
  IXMLProjectionType = interface;
  IXMLPointType = interface;
  IXMLServerType2 = interface;
  IXMLServerType22 = interface;
  IXMLServerType222 = interface;
  IXMLServerType2222 = interface;
  IXMLServerType22222 = interface;
  IXMLServerType222222 = interface;
  IXMLServerType22222222 = interface;
  IXMLIntegerList = interface;

{ IXMLGeoportalType }

  IXMLGeoportalType = interface(IXMLNode)
    ['{BED68838-226D-47C0-B714-2DC56436394B}']
    { Property Accessors }
    function Get_InternetConnect: IXMLInternetConnectType;
    function Get_Defines: IXMLDefinesType;
    function Get_WMS_WMTS: IXMLWMS_WMTSType;
    function Get_WFS: IXMLWFSType;
    function Get_WCS: IXMLWCSType;
    function Get_Dialog: IXMLDialogType;
    function Get_Portal: IXMLPortalType;
    { Methods & Properties }
    property InternetConnect: IXMLInternetConnectType read Get_InternetConnect;
    property Defines: IXMLDefinesType read Get_Defines;
    property WMS_WMTS: IXMLWMS_WMTSType read Get_WMS_WMTS;
    property WFS: IXMLWFSType read Get_WFS;
    property WCS: IXMLWCSType read Get_WCS;
    property Dialog: IXMLDialogType read Get_Dialog;
    property Portal: IXMLPortalType read Get_Portal;
  end;

{ IXMLInternetConnectType }

  IXMLInternetConnectType = interface(IXMLNode)
    ['{9DC9146F-34AC-4D34-9224-017ECDD41D0E}']
    { Property Accessors }
    function Get_Url1: String;
    function Get_Url2: String;
    function Get_Url3: String;
    procedure Set_Url1(Value: String);
    procedure Set_Url2(Value: String);
    procedure Set_Url3(Value: String);
    { Methods & Properties }
    property Url1: String read Get_Url1 write Set_Url1;
    property Url2: String read Get_Url2 write Set_Url2;
    property Url3: String read Get_Url3 write Set_Url3;
  end;

{ IXMLDefinesType }

  IXMLDefinesType = interface(IXMLNode)
    ['{13039C7E-0334-4BC2-993D-6AE99F4F7ABE}']
    { Property Accessors }
    function Get_Algorithm: IXMLAlgorithmType;
    function Get_PortalStadndart: IXMLPortalStadndartType;
    function Get_TileMatrixSet: IXMLTileMatrixSetType;
    { Methods & Properties }
    property Algorithm: IXMLAlgorithmType read Get_Algorithm;
    property PortalStadndart: IXMLPortalStadndartType read Get_PortalStadndart;
    property TileMatrixSet: IXMLTileMatrixSetType read Get_TileMatrixSet;
  end;

{ IXMLAlgorithmType }

  IXMLAlgorithmType = interface(IXMLNodeCollection)
    ['{D122DEEC-CA2F-419D-90BC-52569C107ACA}']
    { Property Accessors }
    function Get_UTMSType(Index: Integer): IXMLUTMSType;
    { Methods & Properties }
    function Add: IXMLUTMSType;
    function Insert(const Index: Integer): IXMLUTMSType;
    property UTMSType[Index: Integer]: IXMLUTMSType read Get_UTMSType; default;
  end;

{ IXMLUTMSType }

  IXMLUTMSType = interface(IXMLNode)
    ['{8B10E042-BB95-4C06-AD6C-63A1593D9C8E}']
    { Property Accessors }
    function Get_Name: Integer;
    function Get_Value: Integer;
    procedure Set_Name(Value: Integer);
    procedure Set_Value(Value: Integer);
    { Methods & Properties }
    property Name: Integer read Get_Name write Set_Name;
    property Value: Integer read Get_Value write Set_Value;
  end;

{ IXMLPortalStadndartType }

  IXMLPortalStadndartType = interface(IXMLNode)
    ['{EFDD7146-0633-4FF1-9202-99CEB9CC2B2D}']
    { Property Accessors }
    function Get_Type_: IXMLIntegerList;
    function Get_SubType: IXMLIntegerList;
    { Methods & Properties }
    property Type_: IXMLIntegerList read Get_Type_;
    property SubType: IXMLIntegerList read Get_SubType;
  end;

{ IXMLTileMatrixSetType }

  IXMLTileMatrixSetType = interface(IXMLNodeCollection)
    ['{99F04FFD-9B1B-4C0A-B178-BB4B8C218AFA}']
    { Property Accessors }
    function Get_TileMatrix(Index: Integer): IXMLTileMatrixType;
    { Methods & Properties }
    function Add: IXMLTileMatrixType;
    function Insert(const Index: Integer): IXMLTileMatrixType;
    property TileMatrix[Index: Integer]: IXMLTileMatrixType read Get_TileMatrix; default;
  end;

{ IXMLTileMatrixType }

  IXMLTileMatrixType = interface(IXMLNode)
    ['{234FCAF5-90C7-4367-8FEA-D7511C983002}']
    { Property Accessors }
    function Get_Identifier: string;
    function Get_BoundingBox: IXMLBoundingBoxType;
    function Get_WGS84BoundingBox: IXMLWGS84BoundingBoxType;
    function Get_CRS: string;
    function Get_TopLeftCorner: Integer;
    function Get_TileWidth: Integer;
    function Get_TileHeight: Integer;
    function Get_MatrixBound: IXMLMatrixBoundType;
    procedure Set_Identifier(Value: string);
    procedure Set_CRS(Value: string);
    procedure Set_TopLeftCorner(Value: Integer);
    procedure Set_TileWidth(Value: Integer);
    procedure Set_TileHeight(Value: Integer);
    { Methods & Properties }
    property Identifier: string read Get_Identifier write Set_Identifier;
    property BoundingBox: IXMLBoundingBoxType read Get_BoundingBox;
    property WGS84BoundingBox: IXMLWGS84BoundingBoxType read Get_WGS84BoundingBox;
    property CRS: string read Get_CRS write Set_CRS;
    property TopLeftCorner: Integer read Get_TopLeftCorner write Set_TopLeftCorner;
    property TileWidth: Integer read Get_TileWidth write Set_TileWidth;
    property TileHeight: Integer read Get_TileHeight write Set_TileHeight;
    property MatrixBound: IXMLMatrixBoundType read Get_MatrixBound;
  end;

{ IXMLBoundingBoxType }

  IXMLBoundingBoxType = interface(IXMLNode)
    ['{3209ED19-F71F-4517-92BF-69FECAAF113D}']
    { Property Accessors }
    function Get_LowerCorner: Integer;
    function Get_UpperCorner: Integer;
    procedure Set_LowerCorner(Value: Integer);
    procedure Set_UpperCorner(Value: Integer);
    { Methods & Properties }
    property LowerCorner: Integer read Get_LowerCorner write Set_LowerCorner;
    property UpperCorner: Integer read Get_UpperCorner write Set_UpperCorner;
  end;

{ IXMLWGS84BoundingBoxType }

  IXMLWGS84BoundingBoxType = interface(IXMLNode)
    ['{252B318D-A1AB-488E-AEC5-C6DC3719A374}']
    { Property Accessors }
    function Get_LowerCorner: Integer;
    function Get_UpperCorner: Integer;
    procedure Set_LowerCorner(Value: Integer);
    procedure Set_UpperCorner(Value: Integer);
    { Methods & Properties }
    property LowerCorner: Integer read Get_LowerCorner write Set_LowerCorner;
    property UpperCorner: Integer read Get_UpperCorner write Set_UpperCorner;
  end;

{ IXMLMatrixBoundType }

  IXMLMatrixBoundType = interface(IXMLNodeCollection)
    ['{C7ADBC79-8BF4-4B78-B358-ECF29CE53CAF}']
    { Property Accessors }
    function Get_Zoom(Index: Integer): IXMLZoomType;
    { Methods & Properties }
    function Add: IXMLZoomType;
    function Insert(const Index: Integer): IXMLZoomType;
    property Zoom[Index: Integer]: IXMLZoomType read Get_Zoom; default;
  end;

{ IXMLZoomType }

  IXMLZoomType = interface(IXMLNode)
    ['{60572E9C-3050-42D6-97B5-F10C27AED143}']
    { Property Accessors }
    function Get_Value: Integer;
    function Get_Width: Integer;
    function Get_Height: Integer;
    function Get_Denominator: Integer;
    procedure Set_Value(Value: Integer);
    procedure Set_Width(Value: Integer);
    procedure Set_Height(Value: Integer);
    procedure Set_Denominator(Value: Integer);
    { Methods & Properties }
    property Value: Integer read Get_Value write Set_Value;
    property Width: Integer read Get_Width write Set_Width;
    property Height: Integer read Get_Height write Set_Height;
    property Denominator: Integer read Get_Denominator write Set_Denominator;
  end;

{ IXMLWMS_WMTSType }

  IXMLWMS_WMTSType = interface(IXMLNodeCollection)
    ['{99336B0E-A9D8-44A4-A49C-1141700D272B}']
    { Property Accessors }
    function Get_Server(Index: Integer): IXMLServerType;
    { Methods & Properties }
    function Add: IXMLServerType;
    function Insert(const Index: Integer): IXMLServerType;
    property Server[Index: Integer]: IXMLServerType read Get_Server; default;
  end;

{ IXMLServerType }

  IXMLServerType = interface(IXMLNode)
    ['{B4416A81-9ED1-4A5D-BD25-28B69FF7F419}']
    { Property Accessors }
    function Get_Description: Integer;
    procedure Set_Description(Value: Integer);
    { Methods & Properties }
    property Description: Integer read Get_Description write Set_Description;
  end;

{ IXMLWFSType }

  IXMLWFSType = interface(IXMLNodeCollection)
    ['{CEEAC987-B694-4DAC-8D79-E6821689996E}']
    { Property Accessors }
    function Get_Server(Index: Integer): IXMLServerType2222222;
    { Methods & Properties }
    function Add: IXMLServerType2222222;
    function Insert(const Index: Integer): IXMLServerType2222222;
    property Server[Index: Integer]: IXMLServerType2222222 read Get_Server; default;
  end;

{ IXMLServerType2222222 }

  IXMLServerType2222222 = interface(IXMLNode)
    ['{076EC737-6908-467A-BE0A-41C47E50AE19}']
    { Property Accessors }
    function Get_Description: Integer;
    procedure Set_Description(Value: Integer);
    { Methods & Properties }
    property Description: Integer read Get_Description write Set_Description;
  end;

{ IXMLWCSType }

  IXMLWCSType = interface(IXMLNode)
    ['{9FF340AB-BE6E-4E32-AA98-F1B8CB5A4911}']
    { Property Accessors }
    function Get_Server: IXMLServerType222222222;
    { Methods & Properties }
    property Server: IXMLServerType222222222 read Get_Server;
  end;

{ IXMLServerType222222222 }

  IXMLServerType222222222 = interface(IXMLNode)
    ['{DBE916E3-A3F6-40D1-BDC4-C13EB22E800F}']
    { Property Accessors }
    function Get_Description: Integer;
    procedure Set_Description(Value: Integer);
    { Methods & Properties }
    property Description: Integer read Get_Description write Set_Description;
  end;

{ IXMLDialogType }

  IXMLDialogType = interface(IXMLNode)
    ['{3C02328A-9AA4-47CA-A166-4A4C68440768}']
    { Property Accessors }
    function Get_Height: Integer;
    function Get_WfsHeight: Integer;
    procedure Set_Height(Value: Integer);
    procedure Set_WfsHeight(Value: Integer);
    { Methods & Properties }
    property Height: Integer read Get_Height write Set_Height;
    property WfsHeight: Integer read Get_WfsHeight write Set_WfsHeight;
  end;

{ IXMLPortalType }

  IXMLPortalType = interface(IXMLNode)
    ['{24B03376-08D7-4E21-8C9C-B6B0FA513F30}']
    { Property Accessors }
    function Get_Params: IXMLParamsType;
    function Get_Topic: IXMLTopicTypeList;
    { Methods & Properties }
    property Params: IXMLParamsType read Get_Params;
    property Topic: IXMLTopicTypeList read Get_Topic;
  end;

{ IXMLParamsType }

  IXMLParamsType = interface(IXMLNode)
    ['{13966BED-F4B0-49C7-87F4-EEF9A5CF6D5B}']
    { Property Accessors }
    function Get_UpdateFile: String;
    function Get_UseProxy: Integer;
    function Get_Proxyserver: IXMLProxyserverType;
    function Get_Port: Integer;
    function Get_User: Integer;
    function Get_Password: Integer;
    function Get_ConnectTimeout: Integer;
    function Get_ReadTimeout: Integer;
    function Get_UpdateFlag: Integer;
    function Get_FeatureCount: Integer;
    procedure Set_UpdateFile(Value: String);
    procedure Set_UseProxy(Value: Integer);
    procedure Set_Port(Value: Integer);
    procedure Set_User(Value: Integer);
    procedure Set_Password(Value: Integer);
    procedure Set_ConnectTimeout(Value: Integer);
    procedure Set_ReadTimeout(Value: Integer);
    procedure Set_UpdateFlag(Value: Integer);
    procedure Set_FeatureCount(Value: Integer);
    { Methods & Properties }
    property UpdateFile: String read Get_UpdateFile write Set_UpdateFile;
    property UseProxy: Integer read Get_UseProxy write Set_UseProxy;
    property Proxyserver: IXMLProxyserverType read Get_Proxyserver;
    property Port: Integer read Get_Port write Set_Port;
    property User: Integer read Get_User write Set_User;
    property Password: Integer read Get_Password write Set_Password;
    property ConnectTimeout: Integer read Get_ConnectTimeout write Set_ConnectTimeout;
    property ReadTimeout: Integer read Get_ReadTimeout write Set_ReadTimeout;
    property UpdateFlag: Integer read Get_UpdateFlag write Set_UpdateFlag;
    property FeatureCount: Integer read Get_FeatureCount write Set_FeatureCount;
  end;

{ IXMLProxyserverType }

  IXMLProxyserverType = interface(IXMLNode)
    ['{290D9569-DFFB-402E-9201-AB1CB068E9B2}']
    { Property Accessors }
    function Get_Url: String;
    procedure Set_Url(Value: String);
    { Methods & Properties }
    property Url: String read Get_Url write Set_Url;
  end;

{ IXMLTopicType }

  IXMLTopicType = interface(IXMLNode)
    ['{AF5DDBB2-3ED1-42B0-A21E-254A451B5AFF}']
    { Property Accessors }
    function Get_Link: string;
    function Get_Name: string;
    function Get_Img: string;
    
    function Get_Layer: IXMLLayerTypeList;
    function Get_Topic: IXMLTopicTypeList;
    procedure Set_Link(Value: string);
    procedure Set_Name(Value: string);
    { Methods & Properties }
    property Link: string read Get_Link write Set_Link;
    property Name: string read Get_Name write Set_Name;

    property Img: string read Get_Img;
    
    property Layer: IXMLLayerTypeList read Get_Layer;
    property Topic: IXMLTopicTypeList read Get_Topic;
  end;

{ IXMLTopicTypeList }

  IXMLTopicTypeList = interface(IXMLNodeCollection)
    ['{F1A41D63-F1E9-41AA-B9F7-5C082601636A}']
    { Methods & Properties }
    function Add: IXMLTopicType;
    function Insert(const Index: Integer): IXMLTopicType;

    function Get_Item(Index: Integer): IXMLTopicType;
    property Items[Index: Integer]: IXMLTopicType read Get_Item; default;
  end;

{ IXMLLayerType }

  IXMLLayerType = interface(IXMLNode)
    ['{03C42EBF-4812-4424-98FC-E2DA9FB71105}']
    { Property Accessors }
    function Get_Name: string;
    function Get_UnicId: string;
    function Get_ConnectedSting: IXMLConnectedStingType;
    function Get_PortalStadndart: Integer;
    function Get_Alghoritm: Integer;
    function Get_Transparent: Integer;
    function Get_Password: Integer;
    function Get_EditVersion: Integer;
    function Get_SubType: Integer;
    function Get_UserName: Integer;
    function Get_UserNameCon: Integer;
    function Get_PasswordCon: Integer;
    function Get_UrlLogin: String;
    function Get_AdditiaonalParLog: String;
    function Get_UseAut: Integer;
    function Get_ShowScale: Integer;
    function Get_Point: IXMLPointType;
    function Get_MinZoom: Integer;
    function Get_MaxZoom: Integer;
    function Get_ApiKey: Integer;
    procedure Set_Name(Value: string);
    procedure Set_UnicId(Value: string);
    procedure Set_PortalStadndart(Value: Integer);
    procedure Set_Alghoritm(Value: Integer);
    procedure Set_Transparent(Value: Integer);
    procedure Set_Password(Value: Integer);
    procedure Set_EditVersion(Value: Integer);
    procedure Set_SubType(Value: Integer);
    procedure Set_UserName(Value: Integer);
    procedure Set_UserNameCon(Value: Integer);
    procedure Set_PasswordCon(Value: Integer);
    procedure Set_UrlLogin(Value: String);
    procedure Set_AdditiaonalParLog(Value: String);
    procedure Set_UseAut(Value: Integer);
    procedure Set_ShowScale(Value: Integer);
    procedure Set_MinZoom(Value: Integer);
    procedure Set_MaxZoom(Value: Integer);
    procedure Set_ApiKey(Value: Integer);
    { Methods & Properties }
    property Name: string read Get_Name write Set_Name;
    property UnicId: string read Get_UnicId write Set_UnicId;
    property ConnectedSting: IXMLConnectedStingType read Get_ConnectedSting;
    property PortalStadndart: Integer read Get_PortalStadndart write Set_PortalStadndart;
    property Alghoritm: Integer read Get_Alghoritm write Set_Alghoritm;
    property Transparent: Integer read Get_Transparent write Set_Transparent;
    property Password: Integer read Get_Password write Set_Password;
    property EditVersion: Integer read Get_EditVersion write Set_EditVersion;
    property SubType: Integer read Get_SubType write Set_SubType;
    property UserName: Integer read Get_UserName write Set_UserName;
    property UserNameCon: Integer read Get_UserNameCon write Set_UserNameCon;
    property PasswordCon: Integer read Get_PasswordCon write Set_PasswordCon;
    property UrlLogin: String read Get_UrlLogin write Set_UrlLogin;
    property AdditiaonalParLog: String read Get_AdditiaonalParLog write Set_AdditiaonalParLog;
    property UseAut: Integer read Get_UseAut write Set_UseAut;
    property ShowScale: Integer read Get_ShowScale write Set_ShowScale;
    property Point: IXMLPointType read Get_Point;
    property MinZoom: Integer read Get_MinZoom write Set_MinZoom;
    property MaxZoom: Integer read Get_MaxZoom write Set_MaxZoom;
    property ApiKey: Integer read Get_ApiKey write Set_ApiKey;
  end;

{ IXMLLayerTypeList }

  IXMLLayerTypeList = interface(IXMLNodeCollection)
    ['{82B308A5-FD1C-4E47-A332-BD169546477F}']
    { Methods & Properties }
    function Add: IXMLLayerType;
    function Insert(const Index: Integer): IXMLLayerType;

    function Get_Item(Index: Integer): IXMLLayerType;
    property Items[Index: Integer]: IXMLLayerType read Get_Item; default;
  end;

{ IXMLConnectedStingType }

  IXMLConnectedStingType = interface(IXMLNode)
    ['{3BA0B333-2B7C-4F08-88C5-88A216704A09}']
    { Property Accessors }
    function Get_URL: String;
    function Get_ID: IXMLIDTypeList;
    function Get_Name: IXMLNameTypeList;
    function Get_Matrix: IXMLMatrixType;
    function Get_Projection: IXMLProjectionType;
    procedure Set_URL(Value: String);
    { Methods & Properties }
    property URL: String read Get_URL write Set_URL;
    property ID: IXMLIDTypeList read Get_ID;
    property Name: IXMLNameTypeList read Get_Name;
    property Matrix: IXMLMatrixType read Get_Matrix;
    property Projection: IXMLProjectionType read Get_Projection;
  end;

{ IXMLIDType }

  IXMLIDType = interface(IXMLNode)
    ['{C3C2E311-EFDC-4061-9EF2-4FEA3A3AC6BA}']
    { Property Accessors }
    function Get_Number: Integer;
    function Get_Value: Integer;
    procedure Set_Number(Value: Integer);
    procedure Set_Value(Value: Integer);
    { Methods & Properties }
    property Number: Integer read Get_Number write Set_Number;
    property Value: Integer read Get_Value write Set_Value;
  end;

{ IXMLIDTypeList }

  IXMLIDTypeList = interface(IXMLNodeCollection)
    ['{D971D19D-E098-45DE-91B4-9446C0C7A44D}']
    { Methods & Properties }
    function Add: IXMLIDType;
    function Insert(const Index: Integer): IXMLIDType;

    function Get_Item(Index: Integer): IXMLIDType;
    property Items[Index: Integer]: IXMLIDType read Get_Item; default;
  end;

{ IXMLNameType }

  IXMLNameType = interface(IXMLNode)
    ['{77295551-F326-4773-ACA1-63A1D7569DD3}']
    { Property Accessors }
    function Get_Number: Integer;
    function Get_Value: Integer;
    procedure Set_Number(Value: Integer);
    procedure Set_Value(Value: Integer);
    { Methods & Properties }
    property Number: Integer read Get_Number write Set_Number;
    property Value: Integer read Get_Value write Set_Value;
  end;

{ IXMLNameTypeList }

  IXMLNameTypeList = interface(IXMLNodeCollection)
    ['{C484722D-6773-4700-A491-9208C3E0C34D}']
    { Methods & Properties }
    function Add: IXMLNameType;
    function Insert(const Index: Integer): IXMLNameType;

    function Get_Item(Index: Integer): IXMLNameType;
    property Items[Index: Integer]: IXMLNameType read Get_Item; default;
  end;

{ IXMLMatrixType }

  IXMLMatrixType = interface(IXMLNode)
    ['{F8E4745E-D041-4DBE-9181-52FCEF3B8409}']
    { Property Accessors }
    function Get_Name: string;
    procedure Set_Name(Value: string);
    { Methods & Properties }
    property Name: string read Get_Name write Set_Name;
  end;

{ IXMLProjectionType }

  IXMLProjectionType = interface(IXMLNode)
    ['{40B963D3-DFEC-4D6E-968D-DE49751A7B71}']
    { Property Accessors }
    function Get_CRS: string;
    procedure Set_CRS(Value: string);
    { Methods & Properties }
    property CRS: string read Get_CRS write Set_CRS;
  end;

{ IXMLPointType }

  IXMLPointType = interface(IXMLNode)
    ['{310D0A89-5239-416D-B78B-0C0217F03675}']
    { Property Accessors }
    function Get_X: Integer;
    function Get_Y: Integer;
    procedure Set_X(Value: Integer);
    procedure Set_Y(Value: Integer);
    { Methods & Properties }
    property X: Integer read Get_X write Set_X;
    property Y: Integer read Get_Y write Set_Y;
  end;

{ IXMLServerType2 }

  IXMLServerType2 = interface(IXMLNode)
    ['{BAA14788-B18B-415F-9D9F-FA818FBFFAB7}']
    { Property Accessors }
    function Get_Description: Integer;
    function Get_ApiKey: Integer;
    procedure Set_Description(Value: Integer);
    procedure Set_ApiKey(Value: Integer);
    { Methods & Properties }
    property Description: Integer read Get_Description write Set_Description;
    property ApiKey: Integer read Get_ApiKey write Set_ApiKey;
  end;

{ IXMLServerType22 }

  IXMLServerType22 = interface(IXMLNode)
    ['{DEFB0C76-9AE8-456B-95A2-C109D1D56D40}']
    { Property Accessors }
    function Get_Description: Integer;
    procedure Set_Description(Value: Integer);
    { Methods & Properties }
    property Description: Integer read Get_Description write Set_Description;
  end;

{ IXMLServerType222 }

  IXMLServerType222 = interface(IXMLNode)
    ['{CA72848F-1250-4C21-B769-354D4DA868B3}']
    { Property Accessors }
    function Get_Description: Integer;
    procedure Set_Description(Value: Integer);
    { Methods & Properties }
    property Description: Integer read Get_Description write Set_Description;
  end;

{ IXMLServerType2222 }

  IXMLServerType2222 = interface(IXMLNode)
    ['{6A3591ED-7692-4EF5-8CA0-6C486F6DECC5}']
    { Property Accessors }
    function Get_Description: Integer;
    procedure Set_Description(Value: Integer);
    { Methods & Properties }
    property Description: Integer read Get_Description write Set_Description;
  end;

{ IXMLServerType22222 }

  IXMLServerType22222 = interface(IXMLNode)
    ['{09C8210F-30C3-4B1E-8567-7728E16C9433}']
    { Property Accessors }
    function Get_Description: Integer;
    procedure Set_Description(Value: Integer);
    { Methods & Properties }
    property Description: Integer read Get_Description write Set_Description;
  end;

{ IXMLServerType222222 }

  IXMLServerType222222 = interface(IXMLNode)
    ['{9BB77B0A-A934-41DD-9E3B-0BFE62594BC7}']
    { Property Accessors }
    function Get_Description: Integer;
    procedure Set_Description(Value: Integer);
    { Methods & Properties }
    property Description: Integer read Get_Description write Set_Description;
  end;

{ IXMLServerType22222222 }

  IXMLServerType22222222 = interface(IXMLNode)
    ['{FCEB40FF-3C64-4432-98B4-556E813DBC00}']
  end;

{ IXMLIntegerList }

  IXMLIntegerList = interface(IXMLNodeCollection)
    ['{402481C0-A784-4151-A9D3-D7AAF6FA367F}']
    { Methods & Properties }
    function Add(const Value: Integer): IXMLNode;
    function Insert(const Index: Integer; const Value: Integer): IXMLNode;

    function Get_Item(Index: Integer): Integer;
    property Items[Index: Integer]: Integer read Get_Item; default;
  end;

{ Forward Decls }

  TXMLGeoportalType = class;
  TXMLInternetConnectType = class;
  TXMLDefinesType = class;
  TXMLAlgorithmType = class;
  TXMLUTMSType = class;
  TXMLPortalStadndartType = class;
  TXMLTileMatrixSetType = class;
  TXMLTileMatrixType = class;
  TXMLBoundingBoxType = class;
  TXMLWGS84BoundingBoxType = class;
  TXMLMatrixBoundType = class;
  TXMLZoomType = class;
  TXMLWMS_WMTSType = class;
  TXMLServerType = class;
  TXMLWFSType = class;
  TXMLServerType2222222 = class;
  TXMLWCSType = class;
  TXMLServerType222222222 = class;
  TXMLDialogType = class;
  TXMLPortalType = class;
  TXMLParamsType = class;
  TXMLProxyserverType = class;
  TXMLTopicType = class;
  TXMLTopicTypeList = class;
  TXMLLayerType = class;
  TXMLLayerTypeList = class;
  TXMLConnectedStingType = class;
  TXMLIDType = class;
  TXMLIDTypeList = class;
  TXMLNameType = class;
  TXMLNameTypeList = class;
  TXMLMatrixType = class;
  TXMLProjectionType = class;
  TXMLPointType = class;
  TXMLServerType2 = class;
  TXMLServerType22 = class;
  TXMLServerType222 = class;
  TXMLServerType2222 = class;
  TXMLServerType22222 = class;
  TXMLServerType222222 = class;
  TXMLServerType22222222 = class;
  TXMLIntegerList = class;

{ TXMLGeoportalType }

  TXMLGeoportalType = class(TXMLNode, IXMLGeoportalType)
  protected
    { IXMLGeoportalType }
    function Get_InternetConnect: IXMLInternetConnectType;
    function Get_Defines: IXMLDefinesType;
    function Get_WMS_WMTS: IXMLWMS_WMTSType;
    function Get_WFS: IXMLWFSType;
    function Get_WCS: IXMLWCSType;
    function Get_Dialog: IXMLDialogType;
    function Get_Portal: IXMLPortalType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInternetConnectType }

  TXMLInternetConnectType = class(TXMLNode, IXMLInternetConnectType)
  protected
    { IXMLInternetConnectType }
    function Get_Url1: String;
    function Get_Url2: String;
    function Get_Url3: String;
    procedure Set_Url1(Value: String);
    procedure Set_Url2(Value: String);
    procedure Set_Url3(Value: String);
  end;

{ TXMLDefinesType }

  TXMLDefinesType = class(TXMLNode, IXMLDefinesType)
  protected
    { IXMLDefinesType }
    function Get_Algorithm: IXMLAlgorithmType;
    function Get_PortalStadndart: IXMLPortalStadndartType;
    function Get_TileMatrixSet: IXMLTileMatrixSetType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLAlgorithmType }

  TXMLAlgorithmType = class(TXMLNodeCollection, IXMLAlgorithmType)
  protected
    { IXMLAlgorithmType }
    function Get_UTMSType(Index: Integer): IXMLUTMSType;
    function Add: IXMLUTMSType;
    function Insert(const Index: Integer): IXMLUTMSType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLUTMSType }

  TXMLUTMSType = class(TXMLNode, IXMLUTMSType)
  protected
    { IXMLUTMSType }
    function Get_Name: Integer;
    function Get_Value: Integer;
    procedure Set_Name(Value: Integer);
    procedure Set_Value(Value: Integer);
  end;

{ TXMLPortalStadndartType }

  TXMLPortalStadndartType = class(TXMLNode, IXMLPortalStadndartType)
  private
    FType_: IXMLIntegerList;
    FSubType: IXMLIntegerList;
  protected
    { IXMLPortalStadndartType }
    function Get_Type_: IXMLIntegerList;
    function Get_SubType: IXMLIntegerList;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTileMatrixSetType }

  TXMLTileMatrixSetType = class(TXMLNodeCollection, IXMLTileMatrixSetType)
  protected
    { IXMLTileMatrixSetType }
    function Get_TileMatrix(Index: Integer): IXMLTileMatrixType;
    function Add: IXMLTileMatrixType;
    function Insert(const Index: Integer): IXMLTileMatrixType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTileMatrixType }

  TXMLTileMatrixType = class(TXMLNode, IXMLTileMatrixType)
  protected
    { IXMLTileMatrixType }
    function Get_Identifier: string;
    function Get_BoundingBox: IXMLBoundingBoxType;
    function Get_WGS84BoundingBox: IXMLWGS84BoundingBoxType;
    function Get_CRS: string;
    function Get_TopLeftCorner: Integer;
    function Get_TileWidth: Integer;
    function Get_TileHeight: Integer;
    function Get_MatrixBound: IXMLMatrixBoundType;
    procedure Set_Identifier(Value: string);
    procedure Set_CRS(Value: string);
    procedure Set_TopLeftCorner(Value: Integer);
    procedure Set_TileWidth(Value: Integer);
    procedure Set_TileHeight(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLBoundingBoxType }

  TXMLBoundingBoxType = class(TXMLNode, IXMLBoundingBoxType)
  protected
    { IXMLBoundingBoxType }
    function Get_LowerCorner: Integer;
    function Get_UpperCorner: Integer;
    procedure Set_LowerCorner(Value: Integer);
    procedure Set_UpperCorner(Value: Integer);
  end;

{ TXMLWGS84BoundingBoxType }

  TXMLWGS84BoundingBoxType = class(TXMLNode, IXMLWGS84BoundingBoxType)
  protected
    { IXMLWGS84BoundingBoxType }
    function Get_LowerCorner: Integer;
    function Get_UpperCorner: Integer;
    procedure Set_LowerCorner(Value: Integer);
    procedure Set_UpperCorner(Value: Integer);
  end;

{ TXMLMatrixBoundType }

  TXMLMatrixBoundType = class(TXMLNodeCollection, IXMLMatrixBoundType)
  protected
    { IXMLMatrixBoundType }
    function Get_Zoom(Index: Integer): IXMLZoomType;
    function Add: IXMLZoomType;
    function Insert(const Index: Integer): IXMLZoomType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLZoomType }

  TXMLZoomType = class(TXMLNode, IXMLZoomType)
  protected
    { IXMLZoomType }
    function Get_Value: Integer;
    function Get_Width: Integer;
    function Get_Height: Integer;
    function Get_Denominator: Integer;
    procedure Set_Value(Value: Integer);
    procedure Set_Width(Value: Integer);
    procedure Set_Height(Value: Integer);
    procedure Set_Denominator(Value: Integer);
  end;

{ TXMLWMS_WMTSType }

  TXMLWMS_WMTSType = class(TXMLNodeCollection, IXMLWMS_WMTSType)
  protected
    { IXMLWMS_WMTSType }
    function Get_Server(Index: Integer): IXMLServerType;
    function Add: IXMLServerType;
    function Insert(const Index: Integer): IXMLServerType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLServerType }

  TXMLServerType = class(TXMLNode, IXMLServerType)
  protected
    { IXMLServerType }
    function Get_Description: Integer;
    procedure Set_Description(Value: Integer);
  end;

{ TXMLWFSType }

  TXMLWFSType = class(TXMLNodeCollection, IXMLWFSType)
  protected
    { IXMLWFSType }
    function Get_Server(Index: Integer): IXMLServerType2222222;
    function Add: IXMLServerType2222222;
    function Insert(const Index: Integer): IXMLServerType2222222;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLServerType2222222 }

  TXMLServerType2222222 = class(TXMLNode, IXMLServerType2222222)
  protected
    { IXMLServerType2222222 }
    function Get_Description: Integer;
    procedure Set_Description(Value: Integer);
  end;

{ TXMLWCSType }

  TXMLWCSType = class(TXMLNode, IXMLWCSType)
  protected
    { IXMLWCSType }
    function Get_Server: IXMLServerType222222222;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLServerType222222222 }

  TXMLServerType222222222 = class(TXMLNode, IXMLServerType222222222)
  protected
    { IXMLServerType222222222 }
    function Get_Description: Integer;
    procedure Set_Description(Value: Integer);
  end;

{ TXMLDialogType }

  TXMLDialogType = class(TXMLNode, IXMLDialogType)
  protected
    { IXMLDialogType }
    function Get_Height: Integer;
    function Get_WfsHeight: Integer;
    procedure Set_Height(Value: Integer);
    procedure Set_WfsHeight(Value: Integer);
  end;

{ TXMLPortalType }

  TXMLPortalType = class(TXMLNode, IXMLPortalType)
  private
    FTopic: IXMLTopicTypeList;
  protected
    { IXMLPortalType }
    function Get_Params: IXMLParamsType;
    function Get_Topic: IXMLTopicTypeList;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLParamsType }

  TXMLParamsType = class(TXMLNode, IXMLParamsType)
  protected
    { IXMLParamsType }
    function Get_UpdateFile: String;
    function Get_UseProxy: Integer;
    function Get_Proxyserver: IXMLProxyserverType;
    function Get_Port: Integer;
    function Get_User: Integer;
    function Get_Password: Integer;
    function Get_ConnectTimeout: Integer;
    function Get_ReadTimeout: Integer;
    function Get_UpdateFlag: Integer;
    function Get_FeatureCount: Integer;
    procedure Set_UpdateFile(Value: String);
    procedure Set_UseProxy(Value: Integer);
    procedure Set_Port(Value: Integer);
    procedure Set_User(Value: Integer);
    procedure Set_Password(Value: Integer);
    procedure Set_ConnectTimeout(Value: Integer);
    procedure Set_ReadTimeout(Value: Integer);
    procedure Set_UpdateFlag(Value: Integer);
    procedure Set_FeatureCount(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLProxyserverType }

  TXMLProxyserverType = class(TXMLNode, IXMLProxyserverType)
  protected
    { IXMLProxyserverType }
    function Get_Url: String;
    procedure Set_Url(Value: String);
  end;

{ TXMLTopicType }

  TXMLTopicType = class(TXMLNode, IXMLTopicType)
  private
    FLayer: IXMLLayerTypeList;
    FTopic: IXMLTopicTypeList;
  protected
    { IXMLTopicType }
    function Get_Link: string;
    function Get_Name: string;
    function Get_Img: string;
    
    function Get_Layer: IXMLLayerTypeList;
    function Get_Topic: IXMLTopicTypeList;
    
    procedure Set_Link(Value: string);
    procedure Set_Name(Value: string);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTopicTypeList }

  TXMLTopicTypeList = class(TXMLNodeCollection, IXMLTopicTypeList)
  protected
    { IXMLTopicTypeList }
    function Add: IXMLTopicType;
    function Insert(const Index: Integer): IXMLTopicType;

    function Get_Item(Index: Integer): IXMLTopicType;
  end;

{ TXMLLayerType }

  TXMLLayerType = class(TXMLNode, IXMLLayerType)
  protected
    { IXMLLayerType }
    function Get_Name: string;
    function Get_UnicId: string;
    function Get_ConnectedSting: IXMLConnectedStingType;
    function Get_PortalStadndart: Integer;
    function Get_Alghoritm: Integer;
    function Get_Transparent: Integer;
    function Get_Password: Integer;
    function Get_EditVersion: Integer;
    function Get_SubType: Integer;
    function Get_UserName: Integer;
    function Get_UserNameCon: Integer;
    function Get_PasswordCon: Integer;
    function Get_UrlLogin: String;
    function Get_AdditiaonalParLog: String;
    function Get_UseAut: Integer;
    function Get_ShowScale: Integer;
    function Get_Point: IXMLPointType;
    function Get_MinZoom: Integer;
    function Get_MaxZoom: Integer;
    function Get_ApiKey: Integer;
    procedure Set_Name(Value: string);
    procedure Set_UnicId(Value: string);
    procedure Set_PortalStadndart(Value: Integer);
    procedure Set_Alghoritm(Value: Integer);
    procedure Set_Transparent(Value: Integer);
    procedure Set_Password(Value: Integer);
    procedure Set_EditVersion(Value: Integer);
    procedure Set_SubType(Value: Integer);
    procedure Set_UserName(Value: Integer);
    procedure Set_UserNameCon(Value: Integer);
    procedure Set_PasswordCon(Value: Integer);
    procedure Set_UrlLogin(Value: String);
    procedure Set_AdditiaonalParLog(Value: String);
    procedure Set_UseAut(Value: Integer);
    procedure Set_ShowScale(Value: Integer);
    procedure Set_MinZoom(Value: Integer);
    procedure Set_MaxZoom(Value: Integer);
    procedure Set_ApiKey(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLLayerTypeList }

  TXMLLayerTypeList = class(TXMLNodeCollection, IXMLLayerTypeList)
  protected
    { IXMLLayerTypeList }
    function Add: IXMLLayerType;
    function Insert(const Index: Integer): IXMLLayerType;

    function Get_Item(Index: Integer): IXMLLayerType;
  end;

{ TXMLConnectedStingType }

  TXMLConnectedStingType = class(TXMLNode, IXMLConnectedStingType)
  private
    FID: IXMLIDTypeList;
    FName: IXMLNameTypeList;
  protected
    { IXMLConnectedStingType }
    function Get_URL: String;
    function Get_ID: IXMLIDTypeList;
    function Get_Name: IXMLNameTypeList;
    function Get_Matrix: IXMLMatrixType;
    function Get_Projection: IXMLProjectionType;
    procedure Set_URL(Value: String);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLIDType }

  TXMLIDType = class(TXMLNode, IXMLIDType)
  protected
    { IXMLIDType }
    function Get_Number: Integer;
    function Get_Value: Integer;
    procedure Set_Number(Value: Integer);
    procedure Set_Value(Value: Integer);
  end;

{ TXMLIDTypeList }

  TXMLIDTypeList = class(TXMLNodeCollection, IXMLIDTypeList)
  protected
    { IXMLIDTypeList }
    function Add: IXMLIDType;
    function Insert(const Index: Integer): IXMLIDType;

    function Get_Item(Index: Integer): IXMLIDType;
  end;

{ TXMLNameType }

  TXMLNameType = class(TXMLNode, IXMLNameType)
  protected
    { IXMLNameType }
    function Get_Number: Integer;
    function Get_Value: Integer;
    procedure Set_Number(Value: Integer);
    procedure Set_Value(Value: Integer);
  end;

{ TXMLNameTypeList }

  TXMLNameTypeList = class(TXMLNodeCollection, IXMLNameTypeList)
  protected
    { IXMLNameTypeList }
    function Add: IXMLNameType;
    function Insert(const Index: Integer): IXMLNameType;

    function Get_Item(Index: Integer): IXMLNameType;
  end;

{ TXMLMatrixType }

  TXMLMatrixType = class(TXMLNode, IXMLMatrixType)
  protected
    { IXMLMatrixType }
    function Get_Name: string;
    procedure Set_Name(Value: string);
  end;

{ TXMLProjectionType }

  TXMLProjectionType = class(TXMLNode, IXMLProjectionType)
  protected
    { IXMLProjectionType }
    function Get_CRS: string;
    procedure Set_CRS(Value: string);
  end;

{ TXMLPointType }

  TXMLPointType = class(TXMLNode, IXMLPointType)
  protected
    { IXMLPointType }
    function Get_X: Integer;
    function Get_Y: Integer;
    procedure Set_X(Value: Integer);
    procedure Set_Y(Value: Integer);
  end;

{ TXMLServerType2 }

  TXMLServerType2 = class(TXMLNode, IXMLServerType2)
  protected
    { IXMLServerType2 }
    function Get_Description: Integer;
    function Get_ApiKey: Integer;
    procedure Set_Description(Value: Integer);
    procedure Set_ApiKey(Value: Integer);
  end;

{ TXMLServerType22 }

  TXMLServerType22 = class(TXMLNode, IXMLServerType22)
  protected
    { IXMLServerType22 }
    function Get_Description: Integer;
    procedure Set_Description(Value: Integer);
  end;

{ TXMLServerType222 }

  TXMLServerType222 = class(TXMLNode, IXMLServerType222)
  protected
    { IXMLServerType222 }
    function Get_Description: Integer;
    procedure Set_Description(Value: Integer);
  end;

{ TXMLServerType2222 }

  TXMLServerType2222 = class(TXMLNode, IXMLServerType2222)
  protected
    { IXMLServerType2222 }
    function Get_Description: Integer;
    procedure Set_Description(Value: Integer);
  end;

{ TXMLServerType22222 }

  TXMLServerType22222 = class(TXMLNode, IXMLServerType22222)
  protected
    { IXMLServerType22222 }
    function Get_Description: Integer;
    procedure Set_Description(Value: Integer);
  end;

{ TXMLServerType222222 }

  TXMLServerType222222 = class(TXMLNode, IXMLServerType222222)
  protected
    { IXMLServerType222222 }
    function Get_Description: Integer;
    procedure Set_Description(Value: Integer);
  end;

{ TXMLServerType22222222 }

  TXMLServerType22222222 = class(TXMLNode, IXMLServerType22222222)
  protected
    { IXMLServerType22222222 }
  end;

{ TXMLIntegerList }

  TXMLIntegerList = class(TXMLNodeCollection, IXMLIntegerList)
  protected
    { IXMLIntegerList }
    function Add(const Value: Integer): IXMLNode;
    function Insert(const Index: Integer; const Value: Integer): IXMLNode;

    function Get_Item(Index: Integer): Integer;
  end;

{ Global Functions }

function GetGeoportal(Doc: IXMLDocument): IXMLGeoportalType;
function LoadGeoportal(const FileName: string): IXMLGeoportalType;
function NewGeoportal: IXMLGeoportalType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetGeoportal(Doc: IXMLDocument): IXMLGeoportalType;
begin
  Result := Doc.GetDocBinding('Geoportal', TXMLGeoportalType, TargetNamespace) as IXMLGeoportalType;
end;

function LoadGeoportal(const FileName: string): IXMLGeoportalType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Geoportal', TXMLGeoportalType, TargetNamespace) as IXMLGeoportalType;
end;

function NewGeoportal: IXMLGeoportalType;
begin
  Result := NewXMLDocument.GetDocBinding('Geoportal', TXMLGeoportalType, TargetNamespace) as IXMLGeoportalType;
end;

{ TXMLGeoportalType }

procedure TXMLGeoportalType.AfterConstruction;
begin
  RegisterChildNode('InternetConnect', TXMLInternetConnectType);
  RegisterChildNode('Defines', TXMLDefinesType);
  RegisterChildNode('WMS_WMTS', TXMLWMS_WMTSType);
  RegisterChildNode('WFS', TXMLWFSType);
  RegisterChildNode('WCS', TXMLWCSType);
  RegisterChildNode('Dialog', TXMLDialogType);
  RegisterChildNode('Portal', TXMLPortalType);
  inherited;
end;

function TXMLGeoportalType.Get_InternetConnect: IXMLInternetConnectType;
begin
  Result := ChildNodes['InternetConnect'] as IXMLInternetConnectType;
end;

function TXMLGeoportalType.Get_Defines: IXMLDefinesType;
begin
  Result := ChildNodes['Defines'] as IXMLDefinesType;
end;

function TXMLGeoportalType.Get_WMS_WMTS: IXMLWMS_WMTSType;
begin
  Result := ChildNodes['WMS_WMTS'] as IXMLWMS_WMTSType;
end;

function TXMLGeoportalType.Get_WFS: IXMLWFSType;
begin
  Result := ChildNodes['WFS'] as IXMLWFSType;
end;

function TXMLGeoportalType.Get_WCS: IXMLWCSType;
begin
  Result := ChildNodes['WCS'] as IXMLWCSType;
end;

function TXMLGeoportalType.Get_Dialog: IXMLDialogType;
begin
  Result := ChildNodes['Dialog'] as IXMLDialogType;
end;

function TXMLGeoportalType.Get_Portal: IXMLPortalType;
begin
  Result := ChildNodes['Portal'] as IXMLPortalType;
end;

{ TXMLInternetConnectType }

function TXMLInternetConnectType.Get_Url1: String;
begin
  Result := ChildNodes['Url1'].Text;
end;

procedure TXMLInternetConnectType.Set_Url1(Value: String);
begin
  ChildNodes['Url1'].NodeValue := Value;
end;

function TXMLInternetConnectType.Get_Url2: String;
begin
  Result := ChildNodes['Url2'].Text;
end;

procedure TXMLInternetConnectType.Set_Url2(Value: String);
begin
  ChildNodes['Url2'].NodeValue := Value;
end;

function TXMLInternetConnectType.Get_Url3: String;
begin
  Result := ChildNodes['Url3'].Text;
end;

procedure TXMLInternetConnectType.Set_Url3(Value: String);
begin
  ChildNodes['Url3'].NodeValue := Value;
end;

{ TXMLDefinesType }

procedure TXMLDefinesType.AfterConstruction;
begin
  RegisterChildNode('Algorithm', TXMLAlgorithmType);
  RegisterChildNode('PortalStadndart', TXMLPortalStadndartType);
  RegisterChildNode('TileMatrixSet', TXMLTileMatrixSetType);
  inherited;
end;

function TXMLDefinesType.Get_Algorithm: IXMLAlgorithmType;
begin
  Result := ChildNodes['Algorithm'] as IXMLAlgorithmType;
end;

function TXMLDefinesType.Get_PortalStadndart: IXMLPortalStadndartType;
begin
  Result := ChildNodes['PortalStadndart'] as IXMLPortalStadndartType;
end;

function TXMLDefinesType.Get_TileMatrixSet: IXMLTileMatrixSetType;
begin
  Result := ChildNodes['TileMatrixSet'] as IXMLTileMatrixSetType;
end;

{ TXMLAlgorithmType }

procedure TXMLAlgorithmType.AfterConstruction;
begin
  RegisterChildNode('UTMSType', TXMLUTMSType);
  ItemTag := 'UTMSType';
  ItemInterface := IXMLUTMSType;
  inherited;
end;

function TXMLAlgorithmType.Get_UTMSType(Index: Integer): IXMLUTMSType;
begin
  Result := List[Index] as IXMLUTMSType;
end;

function TXMLAlgorithmType.Add: IXMLUTMSType;
begin
  Result := AddItem(-1) as IXMLUTMSType;
end;

function TXMLAlgorithmType.Insert(const Index: Integer): IXMLUTMSType;
begin
  Result := AddItem(Index) as IXMLUTMSType;
end;

{ TXMLUTMSType }

function TXMLUTMSType.Get_Name: Integer;
begin
  Result := AttributeNodes['Name'].NodeValue;
end;

procedure TXMLUTMSType.Set_Name(Value: Integer);
begin
  SetAttribute('Name', Value);
end;

function TXMLUTMSType.Get_Value: Integer;
begin
  Result := AttributeNodes['Value'].NodeValue;
end;

procedure TXMLUTMSType.Set_Value(Value: Integer);
begin
  SetAttribute('Value', Value);
end;

{ TXMLPortalStadndartType }

procedure TXMLPortalStadndartType.AfterConstruction;
begin
  FType_ := CreateCollection(TXMLIntegerList, IXMLNode, 'Type') as IXMLIntegerList;
  FSubType := CreateCollection(TXMLIntegerList, IXMLNode, 'SubType') as IXMLIntegerList;
  inherited;
end;

function TXMLPortalStadndartType.Get_Type_: IXMLIntegerList;
begin
  Result := FType_;
end;

function TXMLPortalStadndartType.Get_SubType: IXMLIntegerList;
begin
  Result := FSubType;
end;

{ TXMLTileMatrixSetType }

procedure TXMLTileMatrixSetType.AfterConstruction;
begin
  RegisterChildNode('TileMatrix', TXMLTileMatrixType);
  ItemTag := 'TileMatrix';
  ItemInterface := IXMLTileMatrixType;
  inherited;
end;

function TXMLTileMatrixSetType.Get_TileMatrix(Index: Integer): IXMLTileMatrixType;
begin
  Result := List[Index] as IXMLTileMatrixType;
end;

function TXMLTileMatrixSetType.Add: IXMLTileMatrixType;
begin
  Result := AddItem(-1) as IXMLTileMatrixType;
end;

function TXMLTileMatrixSetType.Insert(const Index: Integer): IXMLTileMatrixType;
begin
  Result := AddItem(Index) as IXMLTileMatrixType;
end;

{ TXMLTileMatrixType }

procedure TXMLTileMatrixType.AfterConstruction;
begin
  RegisterChildNode('BoundingBox', TXMLBoundingBoxType);
  RegisterChildNode('WGS84BoundingBox', TXMLWGS84BoundingBoxType);
  RegisterChildNode('MatrixBound', TXMLMatrixBoundType);
  inherited;
end;

function TXMLTileMatrixType.Get_Identifier: string;
begin
  Result := ChildNodes['Identifier'].Text;
end;

procedure TXMLTileMatrixType.Set_Identifier(Value: string);
begin
  ChildNodes['Identifier'].NodeValue := Value;
end;

function TXMLTileMatrixType.Get_BoundingBox: IXMLBoundingBoxType;
begin
  Result := ChildNodes['BoundingBox'] as IXMLBoundingBoxType;
end;

function TXMLTileMatrixType.Get_WGS84BoundingBox: IXMLWGS84BoundingBoxType;
begin
  Result := ChildNodes['WGS84BoundingBox'] as IXMLWGS84BoundingBoxType;
end;

function TXMLTileMatrixType.Get_CRS: string;
begin
  Result := ChildNodes['CRS'].Text;
end;

procedure TXMLTileMatrixType.Set_CRS(Value: string);
begin
  ChildNodes['CRS'].NodeValue := Value;
end;

function TXMLTileMatrixType.Get_TopLeftCorner: Integer;
begin
  Result := ChildNodes['TopLeftCorner'].NodeValue;
end;

procedure TXMLTileMatrixType.Set_TopLeftCorner(Value: Integer);
begin
  ChildNodes['TopLeftCorner'].NodeValue := Value;
end;

function TXMLTileMatrixType.Get_TileWidth: Integer;
begin
  Result := ChildNodes['TileWidth'].NodeValue;
end;

procedure TXMLTileMatrixType.Set_TileWidth(Value: Integer);
begin
  ChildNodes['TileWidth'].NodeValue := Value;
end;

function TXMLTileMatrixType.Get_TileHeight: Integer;
begin
  Result := ChildNodes['TileHeight'].NodeValue;
end;

procedure TXMLTileMatrixType.Set_TileHeight(Value: Integer);
begin
  ChildNodes['TileHeight'].NodeValue := Value;
end;

function TXMLTileMatrixType.Get_MatrixBound: IXMLMatrixBoundType;
begin
  Result := ChildNodes['MatrixBound'] as IXMLMatrixBoundType;
end;

{ TXMLBoundingBoxType }

function TXMLBoundingBoxType.Get_LowerCorner: Integer;
begin
  Result := ChildNodes['LowerCorner'].NodeValue;
end;

procedure TXMLBoundingBoxType.Set_LowerCorner(Value: Integer);
begin
  ChildNodes['LowerCorner'].NodeValue := Value;
end;

function TXMLBoundingBoxType.Get_UpperCorner: Integer;
begin
  Result := ChildNodes['UpperCorner'].NodeValue;
end;

procedure TXMLBoundingBoxType.Set_UpperCorner(Value: Integer);
begin
  ChildNodes['UpperCorner'].NodeValue := Value;
end;

{ TXMLWGS84BoundingBoxType }

function TXMLWGS84BoundingBoxType.Get_LowerCorner: Integer;
begin
  Result := ChildNodes['LowerCorner'].NodeValue;
end;

procedure TXMLWGS84BoundingBoxType.Set_LowerCorner(Value: Integer);
begin
  ChildNodes['LowerCorner'].NodeValue := Value;
end;

function TXMLWGS84BoundingBoxType.Get_UpperCorner: Integer;
begin
  Result := ChildNodes['UpperCorner'].NodeValue;
end;

procedure TXMLWGS84BoundingBoxType.Set_UpperCorner(Value: Integer);
begin
  ChildNodes['UpperCorner'].NodeValue := Value;
end;

{ TXMLMatrixBoundType }

procedure TXMLMatrixBoundType.AfterConstruction;
begin
  RegisterChildNode('Zoom', TXMLZoomType);
  ItemTag := 'Zoom';
  ItemInterface := IXMLZoomType;
  inherited;
end;

function TXMLMatrixBoundType.Get_Zoom(Index: Integer): IXMLZoomType;
begin
  Result := List[Index] as IXMLZoomType;
end;

function TXMLMatrixBoundType.Add: IXMLZoomType;
begin
  Result := AddItem(-1) as IXMLZoomType;
end;

function TXMLMatrixBoundType.Insert(const Index: Integer): IXMLZoomType;
begin
  Result := AddItem(Index) as IXMLZoomType;
end;

{ TXMLZoomType }

function TXMLZoomType.Get_Value: Integer;
begin
  Result := AttributeNodes['Value'].NodeValue;
end;

procedure TXMLZoomType.Set_Value(Value: Integer);
begin
  SetAttribute('Value', Value);
end;

function TXMLZoomType.Get_Width: Integer;
begin
  Result := ChildNodes['Width'].NodeValue;
end;

procedure TXMLZoomType.Set_Width(Value: Integer);
begin
  ChildNodes['Width'].NodeValue := Value;
end;

function TXMLZoomType.Get_Height: Integer;
begin
  Result := ChildNodes['Height'].NodeValue;
end;

procedure TXMLZoomType.Set_Height(Value: Integer);
begin
  ChildNodes['Height'].NodeValue := Value;
end;

function TXMLZoomType.Get_Denominator: Integer;
begin
  Result := ChildNodes['Denominator'].NodeValue;
end;

procedure TXMLZoomType.Set_Denominator(Value: Integer);
begin
  ChildNodes['Denominator'].NodeValue := Value;
end;

{ TXMLWMS_WMTSType }

procedure TXMLWMS_WMTSType.AfterConstruction;
begin
  RegisterChildNode('Server', TXMLServerType);
  ItemTag := 'Server';
  ItemInterface := IXMLServerType;
  inherited;
end;

function TXMLWMS_WMTSType.Get_Server(Index: Integer): IXMLServerType;
begin
  Result := List[Index] as IXMLServerType;
end;

function TXMLWMS_WMTSType.Add: IXMLServerType;
begin
  Result := AddItem(-1) as IXMLServerType;
end;

function TXMLWMS_WMTSType.Insert(const Index: Integer): IXMLServerType;
begin
  Result := AddItem(Index) as IXMLServerType;
end;

{ TXMLServerType }

function TXMLServerType.Get_Description: Integer;
begin
  Result := AttributeNodes['Description'].NodeValue;
end;

procedure TXMLServerType.Set_Description(Value: Integer);
begin
  SetAttribute('Description', Value);
end;

{ TXMLWFSType }

procedure TXMLWFSType.AfterConstruction;
begin
  RegisterChildNode('Server', TXMLServerType2222222);
  ItemTag := 'Server';
  ItemInterface := IXMLServerType2222222;
  inherited;
end;

function TXMLWFSType.Get_Server(Index: Integer): IXMLServerType2222222;
begin
  Result := List[Index] as IXMLServerType2222222;
end;

function TXMLWFSType.Add: IXMLServerType2222222;
begin
  Result := AddItem(-1) as IXMLServerType2222222;
end;

function TXMLWFSType.Insert(const Index: Integer): IXMLServerType2222222;
begin
  Result := AddItem(Index) as IXMLServerType2222222;
end;

{ TXMLServerType2222222 }

function TXMLServerType2222222.Get_Description: Integer;
begin
  Result := AttributeNodes['Description'].NodeValue;
end;

procedure TXMLServerType2222222.Set_Description(Value: Integer);
begin
  SetAttribute('Description', Value);
end;

{ TXMLWCSType }

procedure TXMLWCSType.AfterConstruction;
begin
  RegisterChildNode('Server', TXMLServerType222222222);
  inherited;
end;

function TXMLWCSType.Get_Server: IXMLServerType222222222;
begin
  Result := ChildNodes['Server'] as IXMLServerType222222222;
end;

{ TXMLServerType222222222 }

function TXMLServerType222222222.Get_Description: Integer;
begin
  Result := AttributeNodes['Description'].NodeValue;
end;

procedure TXMLServerType222222222.Set_Description(Value: Integer);
begin
  SetAttribute('Description', Value);
end;

{ TXMLDialogType }

function TXMLDialogType.Get_Height: Integer;
begin
  Result := ChildNodes['Height'].NodeValue;
end;

procedure TXMLDialogType.Set_Height(Value: Integer);
begin
  ChildNodes['Height'].NodeValue := Value;
end;

function TXMLDialogType.Get_WfsHeight: Integer;
begin
  Result := ChildNodes['WfsHeight'].NodeValue;
end;

procedure TXMLDialogType.Set_WfsHeight(Value: Integer);
begin
  ChildNodes['WfsHeight'].NodeValue := Value;
end;

{ TXMLPortalType }

procedure TXMLPortalType.AfterConstruction;
begin
  RegisterChildNode('Params', TXMLParamsType);
  RegisterChildNode('Topic', TXMLTopicType);
  FTopic := CreateCollection(TXMLTopicTypeList, IXMLTopicType, 'Topic') as IXMLTopicTypeList;
  inherited;
end;

function TXMLPortalType.Get_Params: IXMLParamsType;
begin
  Result := ChildNodes['Params'] as IXMLParamsType;
end;

function TXMLPortalType.Get_Topic: IXMLTopicTypeList;
begin
  Result := FTopic;
end;

{ TXMLParamsType }

procedure TXMLParamsType.AfterConstruction;
begin
  RegisterChildNode('Proxyserver', TXMLProxyserverType);
  inherited;
end;

function TXMLParamsType.Get_UpdateFile: String;
begin
  Result := ChildNodes['UpdateFile'].Text;
end;

procedure TXMLParamsType.Set_UpdateFile(Value: String);
begin
  ChildNodes['UpdateFile'].NodeValue := Value;
end;

function TXMLParamsType.Get_UseProxy: Integer;
begin
  Result := ChildNodes['UseProxy'].NodeValue;
end;

procedure TXMLParamsType.Set_UseProxy(Value: Integer);
begin
  ChildNodes['UseProxy'].NodeValue := Value;
end;

function TXMLParamsType.Get_Proxyserver: IXMLProxyserverType;
begin
  Result := ChildNodes['Proxyserver'] as IXMLProxyserverType;
end;

function TXMLParamsType.Get_Port: Integer;
begin
  Result := ChildNodes['Port'].NodeValue;
end;

procedure TXMLParamsType.Set_Port(Value: Integer);
begin
  ChildNodes['Port'].NodeValue := Value;
end;

function TXMLParamsType.Get_User: Integer;
begin
  Result := ChildNodes['User'].NodeValue;
end;

procedure TXMLParamsType.Set_User(Value: Integer);
begin
  ChildNodes['User'].NodeValue := Value;
end;

function TXMLParamsType.Get_Password: Integer;
begin
  Result := ChildNodes['Password'].NodeValue;
end;

procedure TXMLParamsType.Set_Password(Value: Integer);
begin
  ChildNodes['Password'].NodeValue := Value;
end;

function TXMLParamsType.Get_ConnectTimeout: Integer;
begin
  Result := ChildNodes['ConnectTimeout'].NodeValue;
end;

procedure TXMLParamsType.Set_ConnectTimeout(Value: Integer);
begin
  ChildNodes['ConnectTimeout'].NodeValue := Value;
end;

function TXMLParamsType.Get_ReadTimeout: Integer;
begin
  Result := ChildNodes['ReadTimeout'].NodeValue;
end;

procedure TXMLParamsType.Set_ReadTimeout(Value: Integer);
begin
  ChildNodes['ReadTimeout'].NodeValue := Value;
end;

function TXMLParamsType.Get_UpdateFlag: Integer;
begin
  Result := ChildNodes['UpdateFlag'].NodeValue;
end;

procedure TXMLParamsType.Set_UpdateFlag(Value: Integer);
begin
  ChildNodes['UpdateFlag'].NodeValue := Value;
end;

function TXMLParamsType.Get_FeatureCount: Integer;
begin
  Result := ChildNodes['FeatureCount'].NodeValue;
end;

procedure TXMLParamsType.Set_FeatureCount(Value: Integer);
begin
  ChildNodes['FeatureCount'].NodeValue := Value;
end;

{ TXMLProxyserverType }

function TXMLProxyserverType.Get_Url: String;
begin
  Result := AttributeNodes['Url'].Text;
end;

procedure TXMLProxyserverType.Set_Url(Value: String);
begin
  SetAttribute('Url', Value);
end;

{ TXMLTopicType }

procedure TXMLTopicType.AfterConstruction;
begin
  RegisterChildNode('Layer', TXMLLayerType);
  RegisterChildNode('Topic', TXMLTopicType);
  FLayer := CreateCollection(TXMLLayerTypeList, IXMLLayerType, 'Layer') as IXMLLayerTypeList;
  FTopic := CreateCollection(TXMLTopicTypeList, IXMLTopicType, 'Topic') as IXMLTopicTypeList;
  inherited;
end;

function TXMLTopicType.Get_Img: string;
begin
  Result := AttributeNodes['Img'].Text;
end;

function TXMLTopicType.Get_Link: string;
begin
  Result := AttributeNodes['Link'].Text;
end;

procedure TXMLTopicType.Set_Link(Value: string);
begin
  SetAttribute('Link', Value);
end;

function TXMLTopicType.Get_Name: string;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLTopicType.Set_Name(Value: string);
begin
  SetAttribute('Name', Value);
end;

function TXMLTopicType.Get_Layer: IXMLLayerTypeList;
begin
  Result := FLayer;
end;

function TXMLTopicType.Get_Topic: IXMLTopicTypeList;
begin
  Result := FTopic;
end;

{ TXMLTopicTypeList }

function TXMLTopicTypeList.Add: IXMLTopicType;
begin
  Result := AddItem(-1) as IXMLTopicType;
end;

function TXMLTopicTypeList.Insert(const Index: Integer): IXMLTopicType;
begin
  Result := AddItem(Index) as IXMLTopicType;
end;

function TXMLTopicTypeList.Get_Item(Index: Integer): IXMLTopicType;
begin
  Result := List[Index] as IXMLTopicType;
end;

{ TXMLLayerType }

procedure TXMLLayerType.AfterConstruction;
begin
  RegisterChildNode('ConnectedSting', TXMLConnectedStingType);
  RegisterChildNode('Point', TXMLPointType);
  inherited;
end;

function TXMLLayerType.Get_Name: string;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLLayerType.Set_Name(Value: string);
begin
  SetAttribute('Name', Value);
end;

function TXMLLayerType.Get_UnicId: string;
begin
  Result := AttributeNodes['UnicId'].Text;
end;

procedure TXMLLayerType.Set_UnicId(Value: string);
begin
  SetAttribute('UnicId', Value);
end;

function TXMLLayerType.Get_ConnectedSting: IXMLConnectedStingType;
begin
  Result := ChildNodes['ConnectedSting'] as IXMLConnectedStingType;
end;

function TXMLLayerType.Get_PortalStadndart: Integer;
begin
  Result := ChildNodes['PortalStadndart'].NodeValue;
end;

procedure TXMLLayerType.Set_PortalStadndart(Value: Integer);
begin
  ChildNodes['PortalStadndart'].NodeValue := Value;
end;

function TXMLLayerType.Get_Alghoritm: Integer;
begin
  Result := ChildNodes['Alghoritm'].NodeValue;
end;

procedure TXMLLayerType.Set_Alghoritm(Value: Integer);
begin
  ChildNodes['Alghoritm'].NodeValue := Value;
end;

function TXMLLayerType.Get_Transparent: Integer;
begin
  Result := ChildNodes['Transparent'].NodeValue;
end;

procedure TXMLLayerType.Set_Transparent(Value: Integer);
begin
  ChildNodes['Transparent'].NodeValue := Value;
end;

function TXMLLayerType.Get_Password: Integer;
begin
  Result := ChildNodes['Password'].NodeValue;
end;

procedure TXMLLayerType.Set_Password(Value: Integer);
begin
  ChildNodes['Password'].NodeValue := Value;
end;

function TXMLLayerType.Get_EditVersion: Integer;
begin
  Result := ChildNodes['EditVersion'].NodeValue;
end;

procedure TXMLLayerType.Set_EditVersion(Value: Integer);
begin
  ChildNodes['EditVersion'].NodeValue := Value;
end;

function TXMLLayerType.Get_SubType: Integer;
begin
  Result := ChildNodes['SubType'].NodeValue;
end;

procedure TXMLLayerType.Set_SubType(Value: Integer);
begin
  ChildNodes['SubType'].NodeValue := Value;
end;

function TXMLLayerType.Get_UserName: Integer;
begin
  Result := ChildNodes['UserName'].NodeValue;
end;

procedure TXMLLayerType.Set_UserName(Value: Integer);
begin
  ChildNodes['UserName'].NodeValue := Value;
end;

function TXMLLayerType.Get_UserNameCon: Integer;
begin
  Result := ChildNodes['UserNameCon'].NodeValue;
end;

procedure TXMLLayerType.Set_UserNameCon(Value: Integer);
begin
  ChildNodes['UserNameCon'].NodeValue := Value;
end;

function TXMLLayerType.Get_PasswordCon: Integer;
begin
  Result := ChildNodes['PasswordCon'].NodeValue;
end;

procedure TXMLLayerType.Set_PasswordCon(Value: Integer);
begin
  ChildNodes['PasswordCon'].NodeValue := Value;
end;

function TXMLLayerType.Get_UrlLogin: String;
begin
  Result := ChildNodes['UrlLogin'].Text;
end;

procedure TXMLLayerType.Set_UrlLogin(Value: String);
begin
  ChildNodes['UrlLogin'].NodeValue := Value;
end;

function TXMLLayerType.Get_AdditiaonalParLog: String;
begin
  Result := ChildNodes['AdditiaonalParLog'].Text;
end;

procedure TXMLLayerType.Set_AdditiaonalParLog(Value: String);
begin
  ChildNodes['AdditiaonalParLog'].NodeValue := Value;
end;

function TXMLLayerType.Get_UseAut: Integer;
begin
  Result := ChildNodes['UseAut'].NodeValue;
end;

procedure TXMLLayerType.Set_UseAut(Value: Integer);
begin
  ChildNodes['UseAut'].NodeValue := Value;
end;

function TXMLLayerType.Get_ShowScale: Integer;
begin
  Result := ChildNodes['ShowScale'].NodeValue;
end;

procedure TXMLLayerType.Set_ShowScale(Value: Integer);
begin
  ChildNodes['ShowScale'].NodeValue := Value;
end;

function TXMLLayerType.Get_Point: IXMLPointType;
begin
  Result := ChildNodes['Point'] as IXMLPointType;
end;

function TXMLLayerType.Get_MinZoom: Integer;
begin
  Result := ChildNodes['MinZoom'].NodeValue;
end;

procedure TXMLLayerType.Set_MinZoom(Value: Integer);
begin
  ChildNodes['MinZoom'].NodeValue := Value;
end;

function TXMLLayerType.Get_MaxZoom: Integer;
begin
  Result := ChildNodes['MaxZoom'].NodeValue;
end;

procedure TXMLLayerType.Set_MaxZoom(Value: Integer);
begin
  ChildNodes['MaxZoom'].NodeValue := Value;
end;

function TXMLLayerType.Get_ApiKey: Integer;
begin
  Result := ChildNodes['ApiKey'].NodeValue;
end;

procedure TXMLLayerType.Set_ApiKey(Value: Integer);
begin
  ChildNodes['ApiKey'].NodeValue := Value;
end;

{ TXMLLayerTypeList }

function TXMLLayerTypeList.Add: IXMLLayerType;
begin
  Result := AddItem(-1) as IXMLLayerType;
end;

function TXMLLayerTypeList.Insert(const Index: Integer): IXMLLayerType;
begin
  Result := AddItem(Index) as IXMLLayerType;
end;

function TXMLLayerTypeList.Get_Item(Index: Integer): IXMLLayerType;
begin
  Result := List[Index] as IXMLLayerType;
end;

{ TXMLConnectedStingType }

procedure TXMLConnectedStingType.AfterConstruction;
begin
  RegisterChildNode('ID', TXMLIDType);
  RegisterChildNode('Name', TXMLNameType);
  RegisterChildNode('Matrix', TXMLMatrixType);
  RegisterChildNode('Projection', TXMLProjectionType);
  FID := CreateCollection(TXMLIDTypeList, IXMLIDType, 'ID') as IXMLIDTypeList;
  FName := CreateCollection(TXMLNameTypeList, IXMLNameType, 'Name') as IXMLNameTypeList;
  inherited;
end;

function TXMLConnectedStingType.Get_URL: String;
begin
  Result := ChildNodes['URL'].Text;
end;

procedure TXMLConnectedStingType.Set_URL(Value: String);
begin
  ChildNodes['URL'].NodeValue := Value;
end;

function TXMLConnectedStingType.Get_ID: IXMLIDTypeList;
begin
  Result := FID;
end;

function TXMLConnectedStingType.Get_Name: IXMLNameTypeList;
begin
  Result := FName;
end;

function TXMLConnectedStingType.Get_Matrix: IXMLMatrixType;
begin
  Result := ChildNodes['Matrix'] as IXMLMatrixType;
end;

function TXMLConnectedStingType.Get_Projection: IXMLProjectionType;
begin
  Result := ChildNodes['Projection'] as IXMLProjectionType;
end;

{ TXMLIDType }

function TXMLIDType.Get_Number: Integer;
begin
  Result := AttributeNodes['Number'].NodeValue;
end;

procedure TXMLIDType.Set_Number(Value: Integer);
begin
  SetAttribute('Number', Value);
end;

function TXMLIDType.Get_Value: Integer;
begin
  Result := AttributeNodes['Value'].NodeValue;
end;

procedure TXMLIDType.Set_Value(Value: Integer);
begin
  SetAttribute('Value', Value);
end;

{ TXMLIDTypeList }

function TXMLIDTypeList.Add: IXMLIDType;
begin
  Result := AddItem(-1) as IXMLIDType;
end;

function TXMLIDTypeList.Insert(const Index: Integer): IXMLIDType;
begin
  Result := AddItem(Index) as IXMLIDType;
end;

function TXMLIDTypeList.Get_Item(Index: Integer): IXMLIDType;
begin
  Result := List[Index] as IXMLIDType;
end;

{ TXMLNameType }

function TXMLNameType.Get_Number: Integer;
begin
  Result := AttributeNodes['Number'].NodeValue;
end;

procedure TXMLNameType.Set_Number(Value: Integer);
begin
  SetAttribute('Number', Value);
end;

function TXMLNameType.Get_Value: Integer;
begin
  Result := AttributeNodes['Value'].NodeValue;
end;

procedure TXMLNameType.Set_Value(Value: Integer);
begin
  SetAttribute('Value', Value);
end;

{ TXMLNameTypeList }

function TXMLNameTypeList.Add: IXMLNameType;
begin
  Result := AddItem(-1) as IXMLNameType;
end;

function TXMLNameTypeList.Insert(const Index: Integer): IXMLNameType;
begin
  Result := AddItem(Index) as IXMLNameType;
end;

function TXMLNameTypeList.Get_Item(Index: Integer): IXMLNameType;
begin
  Result := List[Index] as IXMLNameType;
end;

{ TXMLMatrixType }

function TXMLMatrixType.Get_Name: string;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLMatrixType.Set_Name(Value: string);
begin
  SetAttribute('Name', Value);
end;

{ TXMLProjectionType }

function TXMLProjectionType.Get_CRS: string;
begin
  Result := AttributeNodes['CRS'].Text;
end;

procedure TXMLProjectionType.Set_CRS(Value: string);
begin
  SetAttribute('CRS', Value);
end;

{ TXMLPointType }

function TXMLPointType.Get_X: Integer;
begin
  Result := AttributeNodes[WideString('X')].NodeValue;
end;

procedure TXMLPointType.Set_X(Value: Integer);
begin
  SetAttribute(WideString('X'), Value);
end;

function TXMLPointType.Get_Y: Integer;
begin
  Result := AttributeNodes[WideString('Y')].NodeValue;
end;

procedure TXMLPointType.Set_Y(Value: Integer);
begin
  SetAttribute(WideString('Y'), Value);
end;

{ TXMLServerType2 }

function TXMLServerType2.Get_Description: Integer;
begin
  Result := AttributeNodes['Description'].NodeValue;
end;

procedure TXMLServerType2.Set_Description(Value: Integer);
begin
  SetAttribute('Description', Value);
end;

function TXMLServerType2.Get_ApiKey: Integer;
begin
  Result := AttributeNodes['ApiKey'].NodeValue;
end;

procedure TXMLServerType2.Set_ApiKey(Value: Integer);
begin
  SetAttribute('ApiKey', Value);
end;

{ TXMLServerType22 }

function TXMLServerType22.Get_Description: Integer;
begin
  Result := AttributeNodes['Description'].NodeValue;
end;

procedure TXMLServerType22.Set_Description(Value: Integer);
begin
  SetAttribute('Description', Value);
end;

{ TXMLServerType222 }

function TXMLServerType222.Get_Description: Integer;
begin
  Result := AttributeNodes['Description'].NodeValue;
end;

procedure TXMLServerType222.Set_Description(Value: Integer);
begin
  SetAttribute('Description', Value);
end;

{ TXMLServerType2222 }

function TXMLServerType2222.Get_Description: Integer;
begin
  Result := AttributeNodes['Description'].NodeValue;
end;

procedure TXMLServerType2222.Set_Description(Value: Integer);
begin
  SetAttribute('Description', Value);
end;

{ TXMLServerType22222 }

function TXMLServerType22222.Get_Description: Integer;
begin
  Result := AttributeNodes['Description'].NodeValue;
end;

procedure TXMLServerType22222.Set_Description(Value: Integer);
begin
  SetAttribute('Description', Value);
end;

{ TXMLServerType222222 }

function TXMLServerType222222.Get_Description: Integer;
begin
  Result := AttributeNodes['Description'].NodeValue;
end;

procedure TXMLServerType222222.Set_Description(Value: Integer);
begin
  SetAttribute('Description', Value);
end;

{ TXMLServerType22222222 }

{ TXMLIntegerList }

function TXMLIntegerList.Add(const Value: Integer): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := Value;
end;

function TXMLIntegerList.Insert(const Index: Integer; const Value: Integer): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := Value;
end;

function TXMLIntegerList.Get_Item(Index: Integer): Integer;
begin
  Result := List[Index].NodeValue;
end;

end.