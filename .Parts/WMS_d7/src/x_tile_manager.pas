unit x_Tile_Manager;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Forms, ActiveX, Classes, ComObj, ComServ, SysUtils,  //CodeSiteLogging,
//  wms_TLB,
  u_debug,

  MapXLib_TLB,

  //d_WMS_Set_Center,
  d_WMS_log,
  u_Tile_Loader,

//  u_Tile_Loader,

  u_com,

  u_MapX,
  u_MapX_func,

  StdVcl;

//  u_WMS_file;


type

  {$IFDEF dll}
//  TTileManagerX = class(TTypedComObject,  ITileManagerX)
  {$ELSE}
//  TTileManagerX = class(TInterfacedObject, ITileManagerX)
  {$ENDIF}

  TTileManagerX = class //(TInterfacedObject, ITileManagerX)

  private
    FCMapX : TMap;
//    FCMapX : CMapX;

    procedure Log(aMsg: string);
  protected

    FTileLoader: TTile_Loader;
//..    WmsFile: TWmsFile;

  public
    destructor Destroy; override;

    function Init(const aMapX: TMap): HResult; stdcall;
    function Clear: HResult; stdcall;

    function Dlg_Log: HResult; stdcall;

    function Set_Z(Value: Integer): HResult; stdcall;
    function Get_Z: Integer; stdcall;

    function Set_Center(aLat, aLon: Double; aZ: byte): HResult; stdcall;
    function Set_Dir(const Value: WideString): HResult; stdcall;

    function Set_WMS_layer(aName: WideString; aURL: string; aEPSG: word; aZ_max: byte): HResult; stdcall;

    function Load_CMap: HResult; stdcall;

  end;
           

const
  DEF_2gis =  'http://tile1.maps.2gis.com/tiles?v=1';



implementation



destructor TTileManagerX.Destroy;
begin
  inherited;

  FreeAndNil(FTileLoader);
end;

// ---------------------------------------------------------------
function TTileManagerX.Init(const aMapX: TMap): HResult;
// ---------------------------------------------------------------
begin
  debug ('TTileManagerX.Init(const aMapX: TMap): HResult;');

  FTileLoader:=TTile_Loader.Create();
//  FTileLoader:=TTile_Loader.Create;


  debug ('WmsFile:=TWmsFile.Create; ');


//  Set_WMS_layer('YandexSat');

  FCMapX:= aMapX;
//  FCMapX:= aMapX as CMapX;

  debug ('TTileManagerX.Init(const aMapX: TMap): HResult; done');

end;



function TTileManagerX.Clear: HResult;
begin
   Assert(FCMapX<>nil, 'FCMapX=nil');

   mapx_TilesClear(FCMapX); //aMapX as CMapX);

   mapx_SetMapDefaultProjection_NumericCoordSys_KRASOVKSY42_simple ( FCMapX);
end;


function TTileManagerX.Get_Z: Integer;
begin
  Result:=FTileLoader.Params.Result_Z;
end;


// ---------------------------------------------------------------
function TTileManagerX.Load_CMap: HResult;
begin
//  CodeSite.Send('TTileManagerX.Load_CMap ...');

  Assert(FCMapX<>nil, 'FCMapX=nil');

  FTileLoader.Load_CMap(FCMapX); //aMapX as CMapX);

  mapx_SetMapDefaultProjection_NumericCoordSys_KRASOVKSY42_WMS ( FCMapX);
  
end;


// ---------------------------------------------------------------
function TTileManagerX.Set_Center(aLat, aLon: Double; aZ: byte): HResult;
// ---------------------------------------------------------------
var
  x: double;
  y: double;
begin
  FTileLoader.Params.Z:=aZ;

  Assert (Assigned (FCMapX ), 'Assigned (FCMapX )');
                   
//  x:=FCMapX.CenterX;
//  y:=FCMapX.CenterY;   
  
  
//  CodeSite.Send( Format('CenterX:%1.6f  CenterY:%1.6f',[FCMapX.CenterX, FCMapX.CenterY]));
  
  FCMapX.AutoRedraw :=False; 

  if aLon < -90 then  // !!!!!!!!!!!!!! 
    aLon := -90;
        
  
  FCMapX.ZoomTo (FCMapX.Zoom, aLon, aLat);   // X: Double; Y: Double);

  FTileLoader.Load_CMap(FCMapX);                  
  FTileLoader.Params.Z:=0;

end;


function TTileManagerX.Set_Dir(const Value: WideString): HResult;
begin
  Assert(Value<>'');
  FTileLoader.Params.Dir:=Value;  
  ForceDirectories(Value);
      
end;


// ---------------------------------------------------------------
function TTileManagerX.Set_WMS_layer(aName: WideString; aURL: string; aEPSG:
    word; aZ_max: byte): HResult;
// ---------------------------------------------------------------
//var
//  iIndex: Integer;
begin
  Result:=S_OK;

//  CodeSite.Send('Set_WMS_Layer - ' + aName);

//  Assert(WmsFile.Count>0);

//..  iIndex:=WmsFile.IndexOf(aName);
  //iIndex:=WmsFile.Layers.IndexOf(aName);

//  if iIndex<0 then
//    Exit;

//  Assert(iIndex>=0);


//  Set_WMS_Layer_Index (iIndex);

  FTileLoader.Params.URL      :=aURL;
  FTileLoader.Params.LayerName:=aName; //UnicId_name;
  FTileLoader.Params.EPSG     :=aEPSG;
  FTileLoader.Params.Z_max   :=aZ_max;

//..  CodeSite.Send('Set_WMS_Layer - Done');

//  Result:=S_OK;
end;


function TTileManagerX.Set_Z(Value: Integer): HResult;
begin
  FTileLoader.Params.Z:=Value;

end;


function TTileManagerX.Dlg_Log: HResult;
begin
  Tdlg_WMS_log.ShowForm;
end;
       

  {
function TTileManagerX.Get_Layers_text(var aResult: WideString): HResult;
begin
  aResult:=WmsFile.GetText;
end;
}

procedure TTileManagerX.Log(aMsg: string);
begin

end;


initialization

{$IFDEF dll}
  TTypedComObjectFactory.Create(ComServer, TTileManagerX, Class_TileManagerX,
    ciMultiInstance, tmApartment);

{$ENDIF}    
end.



 {

 function TTileManagerX.GetClassGUID: TGUID;
begin
  Result:=IID_ITileManagerX;
end;


function TTileManagerX.Dlg_Set_center: HResult;
begin
  Tdlg_WMS_set_center.ExecDlg(FCMapX);
end;

function TTileManagerX.Exec(aCmd: Integer): HResult;
begin
  case aCmd of
    DEF_DLG_LOG : Tdlg_WMS_log.ShowForm;
  end;

//  if aCmd=Dlg_Log then
//     Tdlg_WMS_log.ShowForm;

end;



procedure TTileManagerX.Load_Tile_111111111(aX,aY,aZ: integer);
begin
  FTileLoader.Load_Tile_111111111(aX,aY,aZ);  
end;



 {
// ---------------------------------------------------------------
function TTileManagerX.Set_WMS_Layer_Index(aIndex: integer): HResult;
// ---------------------------------------------------------------
var
  oLayer: TWmsLayer_;    
begin
  Assert(WmsFile.Count>0);

  if aIndex>=0 then
  begin
    oLayer:=WmsFile[aIndex];

    Assert(oLayer.Layer_UnicId<>'');
    Assert(oLayer.ConnectedSting.URL<>'');

    FTileLoader.Params.URL      :=oLayer.ConnectedSting.URL;
    FTileLoader.Params.LayerName:=oLayer.Layer_UnicId; //UnicId_name;
    FTileLoader.Params.EPSG     :=oLayer.Get_EPSG();


        
  end;

 // Result:=aIndex>=0;
  
end;

 }
