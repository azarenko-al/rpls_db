unit u_Tile_loader;

interface

//{.$DEFINE use_debug}

//{$DEFINE grid}

uses
  //u_GDAL_classes,
  u_run,
  u_func,
  u_web,
  u_debug,

  d_WMS_log,

  u_com,

  MapXLib_TLB,

//  CodeSiteLogging,

  u_MapX_func,

  u_mapX,

  u_panorama_coord_convert,

  Math,

  u_geo,

  u_files,

//  IOUtils,

  ExtActns,
  Dialogs, JPEG, PNGImage, ExtCtrls, Graphics,  SysUtils, Classes, Forms, Variants,
  StdVcl,  StrUtils, URLMon, ShellApi, IniFiles, DateUtils
  ;


type
  TXYBounds = record
    x_min: Double;
    x_max: Double;
    y_min: Double;
    y_max: Double;
    
  end;

  TOffserRec= record
    Lat: Double;
    lon: Double;
  end;

  
  
  TTileBounds = packed record
                   block_X_min : longword;
                   block_X_max : longword;

                   block_Y_min : longword;
                   block_Y_max : longword;

                   Max: longword;

                   Z : integer;
                end;



 TLoad_params = record
    Lat: Double;
    Lon: Double;

    Width: Integer;
    Height: Integer;
  end;


  TTile_Loader = class(TObject)
  private
    FMapX: TMap;

//    FOldData : record
//      BLRect: TBLRect;
//      LayerName: string;
//    end  ;

//    FGDAL_path: string;
//    FGDAL_bat: TGDAL_bat;

    //----------------------------------------------------
    FBlockX_col_count: word;
    FBlockY_row_count: word;

    FTileBounds: TTileBounds;

    FTileArray: array of
                array of record
                  block_x,block_y: LongWord ;

                  LeftTop: record
                    X, Y: double;
                  end;
                end;

    //----------------------------------------------------

  private

//    procedure CheckBlock(var aBlock: LongWord; aZ: byte);
    procedure GetBlockByXY(aX, aY: double; aZ: byte; var aBlock_X, aBlock_Y:
        LongWord);
    function GetFileDir(aZ: byte): string;
    function GetFileName_bounds: string;
    function GetFileName_TAB(aX, aY: LongWord; aZ: word): string;
    function GetRangeTileBounds_MinMax: TXYBounds;
    function GetTileBounds_mercatorXY(aBlockX, aBlockY: LongWord; aZ: byte):
        TXYBounds;
    function GetTileBounds_view(aLat, aLon: Double; aZ: byte; aWidth, aHeight:
        Integer): TXYBounds;
    function GetZ(aBoundsX: TXYBounds; aWidth: Integer): Integer;
    function Load(aParams: TLoad_params; var aFileName: WideString; var aBounds:
        TXYBounds): HResult;
    function LoadByRect(var aFileName: WideString): Boolean;
    function LoadByRect_new(var aFileName: WideString): Boolean;
    function Load_HTTP(aFileName: string; aX, aY: LongWord; aZ: byte): boolean;
//    procedure Load_GDAL_Ini;

    procedure Load_Ini;
    procedure Log(aMsg: string);
//    function RasterGK_Exists(var aZone: Integer): Boolean;
//    procedure SaveTileFile_WGS_EPSG_3395__(aFileName: string; aX, aY, aZ: Integer; aImgFileName: string; aEPSG: Integer);

    procedure SaveTileFile_WGS_EPSG_3395_bounds(aFileName, aImgFileName: string;
        aEPSG: word; aBounds: TXYBounds; aImage_width, aImage_height: word; aZ:
        byte);

    function Z_Max: byte;
  public
    constructor Create;
    
    function Load_CMap(const aMapX: TMap): HResult;
    procedure Load_Tile(aX, aY: LongWord; aZ: byte);

  public

    Params: record

                Options: record
                  IsShowGrid : boolean;
                  IsUseCache : boolean;
                end;

                URL: string;
                Dir: string;

                LayerName: string;
                EPSG: word;
                Z: byte;

                Z_max: byte;

               //out
                Result_Z: integer;
              end;

  end;



implementation

const
// epsg 3857
  DEF_MAX_20037508 =  20037508.34279000;
//  DEF_MAX_Z = 19; //18;
//  DEF_MAX_Z = 20; //18;


//----------------------------------------------------------------
constructor TTile_Loader.Create;
//----------------------------------------------------------------
var
  r: TXYBounds;

begin
//  r:=GetTileBounds_mercatorXY (0, 0, 0);
//  r:=GetTileBounds_mercatorXY (0, 0, 1);

//  Params.Options.IsShowGrid:=True;
  Params.Options.IsUseCache:=False;

//  Params.Options.IsUseCache:=True;


//  Params.LayerName:='YandexMap';
  Params.Dir:= GetTempFileDir() ;

  Load_Ini;
//
//  Load_GDAL_Ini;

//  FGDAL_bat:=TGDAL_bat.Create;
         
end;    


{
// ---------------------------------------------------------------
procedure TTile_Loader.CheckBlock(var aBlock: LongWord; aZ: byte);
// ---------------------------------------------------------------
var
  iMax: longint;
begin
  iMax:= Trunc(Math.Power(2, aZ))-1;

  if aBlock<0 then
    aBlock:=0 else

  if aBlock>=iMax then
    aBlock:=iMax;
end;
}

// ---------------------------------------------------------------
procedure TTile_Loader.GetBlockByXY(aX, aY: double; aZ: byte; var aBlock_X,
    aBlock_Y: LongWord);
// ---------------------------------------------------------------
var
  eStep: Double;
  iMax: longint;

  iBlock_X: integer;
  iBlock_Y: integer;
 // r: TXYBounds;

begin
  iMax:= Trunc(Math.Power(2,aZ));

  eStep:=(2*DEF_MAX_20037508)/iMax;

  
  iBlock_Y:= Trunc( (DEF_MAX_20037508-aX) / eStep) ;
  iBlock_X:= Trunc( (aY - (- DEF_MAX_20037508)) / eStep) ;

  if aZ<3 then
  begin
    if iBlock_X>=iMax then iBlock_X:=iMax-1;
    if iBlock_X<0     then iBlock_X:=0;
  end;

  if iBlock_Y>=iMax then iBlock_Y:=iMax-1;
  if iBlock_Y<0     then iBlock_Y:=0;

  aBlock_Y:=iBlock_Y;
  aBlock_X:=iBlock_X;

end;

// ---------------------------------------------------------------
function TTile_Loader.GetFileDir(aZ: byte): string;
// ---------------------------------------------------------------
begin
  Assert(Params.Dir<>'', 'Params.Dir');
  Assert(Params.LayerName<>'', 'Params.LayerName');  
         
 
  Result:=IncludeTrailingBackslash(Trim(Params.Dir)) +  Format('%s\z%d\',[ Params.LayerName,   aZ]);  
 
end;

// ---------------------------------------------------------------
function TTile_Loader.GetFileName_bounds: string;
// ---------------------------------------------------------------
var
  iZone: Integer;
begin
  with FTileBounds do 
    Result:=GetFileDir(Z) +  Format('wms_tile_z%d_x%d_%d_y%d_%d.tab',
       [Z, 
        block_X_min, block_X_max, 
        block_Y_min, block_Y_max]);

{
  if RasterGK_Exists(iZone) then
  begin
    Result:= ChangeFileExt(Result, Format('_gk_%d.tab',[iZone]));

    Log('TdmTile_Loader.GetFileName_bounds - '+Result);

  end;
 }
  
  //  IsUseCache         
end;   

// ---------------------------------------------------------------
function TTile_Loader.GetFileName_TAB(aX, aY: LongWord; aZ: word): string;
// ---------------------------------------------------------------
begin
  Result:=GetFileDir(aZ) + Format('x%d\0\',[aX]) + Format('y%d.tab',[aY]);
  
end;   

// ---------------------------------------------------------------
function TTile_Loader.GetRangeTileBounds_MinMax: TXYBounds;
// ---------------------------------------------------------------
var       
  oBounds_min : TXYBounds;  
  oBounds_max : TXYBounds;
begin
  with FTileBounds do 
  begin
    oBounds_min:=GetTileBounds_mercatorXY(block_X_min, block_Y_min, Z);
    oBounds_max:=GetTileBounds_mercatorXY(block_X_max, block_Y_max, Z);
  
    Result.Y_Min:=oBounds_min.y_min;
    Result.Y_Max:=oBounds_max.y_max;  

    Result.X_Min:=oBounds_max.x_min;
    Result.X_Max:=oBounds_min.x_max;  
        
  end;

end;       

// ---------------------------------------------------------------
function TTile_Loader.GetTileBounds_mercatorXY(aBlockX, aBlockY: LongWord; aZ:
    byte): TXYBounds;
// ---------------------------------------------------------------
var
  eStep: Double;
  iMax: Longint ;
  
begin      
  
  iMax:= Trunc(Math.Power(2,aZ));

    
  eStep:=(2*DEF_MAX_20037508)/iMax;
  //= 256

  Result.X_Max:=  DEF_MAX_20037508 - eStep * aBlockY ;
  Result.X_Min:=  DEF_MAX_20037508 - eStep *(aBlockY+1) ;
  if Abs(Result.X_Min) < 1 then
    Result.X_Min:=0;
                         
  
  Result.Y_Min:= -DEF_MAX_20037508 + eStep * aBlockX ;
  Result.Y_Max:= -DEF_MAX_20037508 + eStep *(aBlockX+1) ;

  if Abs(Result.Y_Min) < 0.001 then
    Result.Y_Min:=0;

  if Abs(Result.Y_Max) < 0.001 then
    Result.Y_Max:=0;

  
end;

// ---------------------------------------------------------------
function TTile_Loader.GetTileBounds_view(aLat, aLon: Double; aZ: byte; aWidth,
    aHeight: Integer): TXYBounds;
// ---------------------------------------------------------------
var
  wgs_Lat, wgs_Lon: Double;
  wgs_Lat1, wgs_Lon1: Double;

  iBlock_Y1: LongWord;
  iBlock_Y2: LongWord;
  iBlock_X1: LongWord;
  iBlock_X2: LongWord;
  e1: Double;
  eLat1: Double;
  eLat2: Double;
  eLon1: Double;
  eLon2: Double;
  eStep: Double;
  eScale: double;
  
  iMax: longint;
  iX: word;
  iY: word;
  X, Y: double;            
   
  r: TXYBounds;
  
begin
  Log('TdmTile_Loader.GetTileBounds_view');


  FillChar(FTileBounds,SizeOf(FTileBounds),0);
  FillChar(Result,SizeOf(Result),0);          
  
  iMax:= Trunc(Math.Power(2,aZ));
  Assert(iMax>0);
  
  eStep:=(2*DEF_MAX_20037508)/iMax;

  eScale:=eStep / 256;           
  Assert(eScale>0);
    
  //-----------------------
  // центральная точка   
  //-----------------------
  Geo_EPSG_To_EPSG_plane(DEF_WGS_EPSG_4326, Params.EPSG, aLat, aLon, X, Y);

  
  Log ( Format('центральная точка - Geo_EPSG_To_EPSG_plane(DEF_WGS_EPSG_4326 - x:%1.5f, y:%1.5f',[X, Y]));
                     
  r.X_Min:= x - (aHeight/2) * eScale;  
  r.X_Max:= x + (aHeight/2) * eScale;

  r.Y_Min:= y - (aWidth/2) * eScale;  
  r.Y_Max:= y + (aWidth/2) * eScale;
                                    
                            
  //-------------------------------------
  // 3395 -> 4326
  //-------------------------------------
  Plane_EPSG_To_EPSG_geo(Params.EPSG, DEF_WGS_EPSG_4326, r.X_Max, r.Y_Min, eLat1, eLon1);
  Plane_EPSG_To_EPSG_geo(Params.EPSG, DEF_WGS_EPSG_4326, r.X_Min, r.Y_Max, eLat2, eLon2);

 

  Result.X_Max:=eLat1;
  Result.Y_Min:=eLon1;  

  Result.X_Min:=eLat2;
  Result.Y_Max:=eLon2;  

  // ---------------------------------------------------------------
  // top left
  GetBlockByXY (r.X_Max, r.Y_Min,  aZ, iBlock_X1, iBlock_Y1) ;
  //bottom right
  GetBlockByXY (r.X_Min, r.Y_Max,  aZ, iBlock_X2, iBlock_Y2) ;
           
  // ---------------------------------------------------------------

  FTileBounds.block_X_min:=iBlock_X1;
  FTileBounds.block_Y_min:=iBlock_Y1;

  FTileBounds.block_X_max:=iBlock_X2;// mod iMax;
  FTileBounds.block_Y_max:=iBlock_Y2;// mod iMax;

  FTileBounds.Max:=iMax;
  FTileBounds.Z:=aZ;

  Log ( Format('FTileBounds.block_X_min %d',[FTileBounds.block_X_min]));
  Log ( Format('FTileBounds.block_Y_max %d',[FTileBounds.block_Y_max]));

  Log ( Format('FTileBounds.block_Y_min %d',[FTileBounds.block_Y_min]));
  Log ( Format('FTileBounds.block_X_max %d',[FTileBounds.block_X_max]));
  
  Log ( Format('FTileBounds.Z %d',          [FTileBounds.Z]));

  
  
  // ---------------------------------------------------------------
  FBlockY_row_count:=iBlock_Y2-iBlock_Y1+1;  

  FBlockX_col_count:=iBlock_X2-iBlock_X1+1;
      

  Log (Format('FBlockY_row_count %d', [FBlockY_row_count]));
  Log (Format('FBlockX_col_count %d', [FBlockX_col_count]));
   
  SetLength (FTileArray, FBlockY_row_count, FBlockX_col_count); 

  for iY := 0 to FBlockY_row_count-1 do
    for iX := 0 to FBlockX_col_count-1 do
    begin
    
      FTileArray [iY,iX].block_x:=(iBlock_X1 + iX) mod iMax;
      FTileArray [iY,iX].block_y:=(iBlock_Y1 + iY) mod iMax;

      try
        r:=GetTileBounds_mercatorXY (FTileArray [iY,iX].block_x, FTileArray [iY,iX].block_y, aZ);
      except
      end;   

      FTileArray [iY,iX].LeftTop.X:=r.x_max;
      FTileArray [iY,iX].LeftTop.y:=r.y_min;           
    end;
    
  
end;  

// ---------------------------------------------------------------
function TTile_Loader.GetZ(aBoundsX: TXYBounds; aWidth: Integer): Integer;
// ---------------------------------------------------------------
var
  eStep: Double;
  iMax: longint;
  iZ: Integer;
  r: TXYBounds;

  e: Double;
  eCols: double;
  
begin     
   if aBoundsX.x_max - aBoundsX.y_min = 0 then
   begin
     Result:=1;
     Exit;
//     Exit(1);
   end;

  
  Geo_EPSG_To_EPSG_plane(DEF_WGS_EPSG_4326, Params.EPSG, aBoundsX.x_max, aBoundsX.y_min,   r.x_max, r.y_min);
  Geo_EPSG_To_EPSG_plane(DEF_WGS_EPSG_4326, Params.EPSG, aBoundsX.x_min, aBoundsX.y_max,   r.x_min, r.y_max);


  eCols:=0;

  for iZ := 0 to Z_max() do
//  for iZ := 0 to DEF_MAX_Z do
  begin
    iMax:= Trunc(Math.Power(2,iZ));
    eStep:=(2*DEF_MAX_20037508)/iMax;

    eCols:= (r.y_max - r.y_min)/eStep;

    e:=eCols*225;

    if e > aWidth then
    begin
      Result:=iZ-1;
      Exit;

//      Exit (iZ-1);
    end;
 
  end;

  Result:=Z_Max();
//  Result:=DEF_MAX_Z;

end;

// ---------------------------------------------------------------
function TTile_Loader.Load(aParams: TLoad_params; var aFileName: WideString;
    var aBounds: TXYBounds): HResult;
// ---------------------------------------------------------------
var
  iZ: Integer;

begin    
  Log('TdmTile_Loader.Load');
            
  if Params.Z>0 then
    iZ:=Params.Z
  else  
    iZ:=GetZ (aBounds,aParams.Width);
                            
  Params.Result_Z:= iZ;      

  Assert(aParams.Width>0);
  Assert(aParams.Height>0);  

 
  aBounds:=GetTileBounds_view(aParams.Lat, aParams.Lon, iZ, aParams.Width, aParams.Height);
                          
  if LoadByRect( aFileName) then
    Result:= S_OK
  else
    Result:= S_FALSE;
                  
end;

// ---------------------------------------------------------------
function TTile_Loader.LoadByRect(var aFileName: WideString): Boolean;
// ---------------------------------------------------------------
var
  //sFile: string;
 
  x: Integer;
  y: Integer;

//  dtStart: TDateTime;
  iCol: Integer;
  iRow: Integer;

begin   
  Result:=False;
    

  aFileName:=GetFileName_bounds ();

//  CodeSite.Send('5');
//  CodeSite.Send(aFileName);
  
  if Params.Options.IsUseCache then  
    if FileExists(aFileName) then
    begin
      Result:=true;
      Exit;
    end;

//      Exit(True);
      

  Log('LoadByRect begin: ' + DateTimeToStr(Now()));


  for iRow := 0 to FBlockY_row_count-1 do
  for iCol := 0 to FBlockX_col_count-1 do
    with FTileArray[iRow,iCol] do
    begin
      Load_Tile (block_x, block_y, FTileBounds.z);
    end;
                      

//  FTestWorkspace.SaveToFile(GetFileDir(aRect.Z) + 'workspace.wor');


  //SecondsBetwe
 

  Log('LoadByRect Done: ' + DateTimeToStr(Now()));
          

 // ShowMessage('LoadByRect Done');
      

  //aRect.Z   //aFileName: WideString; aZ: Integer
                       
       
  result:=LoadByRect_new( aFileName);  //aRect,

        
 // end;

//  s:=IdHTTP1.ResponseText;
  
end;

// ---------------------------------------------------------------
function TTile_Loader.LoadByRect_new(var aFileName: WideString): Boolean;
// ---------------------------------------------------------------
var
  iH: Integer;
  iW: Integer;
  iX: Integer;
  iY: Integer;
 
  s: string;
  sDir: string;
  sFile: string;

  oJPEGImage: TJPEGImage;
  oPNGGImage: TPngObject;

  oImage: TBitmap;
  oImage_file: TImage;
  sImageFile: string;
  sImageFile_jpg: string;
  sImageFile_png: string;

  boundsX: TXYBounds;
  k: Integer;
 
  sFile_bmp: string; 
  sFile_png: string;

begin      

//  CodeSite.Send('TdmTile_Loader.LoadByRect_new');

  Log('make image...');
  SaveTime;


  result:=False;

  try
    oImage:=TBitmap.Create();
    oImage.PixelFormat  :=pf24bit;

    oImage_file:=TImage.Create(nil);
  except
    on E: Exception do
    begin
      ShowMessage(E.Message);
      exit;
    end;    // try/finally
  end;

  iH:=FBlockY_row_count * 256;
  iW:=FBlockX_col_count * 256;


  try
    oImage.Width :=iW;
    oImage.Height:=iH;

  except
    on E: Exception do
    begin
      //ErrorDialog(E.Message, E.HelpContext);
      s:=E.Message + Format('  Width %d  Height %d', [iW, iH]);
//      s:=E.Message + Format('  Width %d  Height %d', [iW, iH]);

      ShowMessage(s);

      FreeAndNil(oImage);
      FreeAndNil(oImage_file);

      exit;
    end;    // try/finally
  end;


  for iY := 0 to FBlockY_row_count-1 do
  for iX := 0 to FBlockX_col_count-1 do
    with FTileArray [iY,iX] do
    begin

      sFile:=GetFileName_TAB(block_x, block_y, FTileBounds.Z);
//      if not FileExists(sFile) then
//        sFile:=GetFileName_TAB(block_x, block_y, FTileBounds.Z, 1);


      assert (sFile<>'');


      sImageFile_jpg:= ChangeFileExt(sFile,'.jpg');
      sImageFile_png:= ChangeFileExt(sFile,'.png');

   //   sExt:=

      //---------------------------------------------
      if FileExists(sImageFile_jpg) then
      //---------------------------------------------
      begin
        Assert (FileExists(sImageFile_jpg), sImageFile_jpg);

        try
          debug (sImageFile_jpg);

          oJPEGImage:=TJPEGImage.Create();
          oJPEGImage.LoadFromFile(sImageFile_jpg);
          oImage_file.Picture.LoadFromFile(sImageFile_jpg);
          FreeAndNil(oJPEGImage);
        except
          on E: Exception do
          begin
            ShowMessage('oJPEGImage:=TJPEGImage.Create(); ' + E.Message);
            exit;
          end;    // try/finally
        end;

      end

      else
      //---------------------------------------------
      if FileExists(sImageFile_png) then
      //---------------------------------------------
        try
          oImage_file.Picture.LoadFromFile(sImageFile_png);
        except
          on E: Exception do
          begin
            ShowMessage(E.Message);
            exit;
          end;    // try/finally
        end

      else

      begin
        Log('ERROR: file not found - '+ sFile);

        Continue;
      end;
      

      oImage.Canvas.Draw(256*iX, 256*iY, oImage_file.Picture.Graphic);
    

    end;

 // end;

  // ---------------------------------------------------------------
  // if Params.Options.IsShowGrid then 
  // ---------------------------------------------------------------
  if Params.Options.IsShowGrid then 
  begin     
    oImage.Canvas.Pen.Color:=clWhite;
    oImage.Canvas.Pen.Width:=1;  
                    

    for iX := 0 to FBlockX_col_count-1 do     
    begin   
      oImage.Canvas.MoveTo(iX*256, 0);
      oImage.Canvas.LineTo(iX*256, oImage.Height);          
    end;       

  
    for iY := 0 to FBlockY_row_count-1 do
    begin             
      oImage.Canvas.MoveTo(0, iY*256);
      oImage.Canvas.LineTo(oImage.Width, iY*256);            
    end;

    
    oImage.Canvas.MoveTo(0,0);
    oImage.Canvas.LineTo(0,oImage.Height-1);
    oImage.Canvas.LineTo(oImage.Width,oImage.Height-1);
    oImage.Canvas.LineTo(oImage.Width-1,0);  
    
  end;  
  // ---------------------------------------------------------------
 


  boundsX:=GetRangeTileBounds_MinMax();

  
  sFile:=GetFileName_bounds(); // (aRect);
                                 

  sFile_bmp:= ChangeFileExt(sFile,'.bmp');
  oImage.SaveToFile(sFile_bmp);

  sFile_png:= ChangeFileExt(sFile,'.png');


  oPNGGImage:=TPngObject.Create();
  oPNGGImage.Assign(oImage);


  oPNGGImage.SaveToFile(sFile_png);
  FreeAndNil(oPNGGImage);
                        

  SaveTileFile_WGS_EPSG_3395_bounds(sFile, sFile_png, Params.EPSG,  boundsX, iW, iH, FTileBounds.Z);
                     
  aFileName:=sFile;


  FreeAndNil(oImage);
  FreeAndNil(oImage_file);


  SysUtils.DeleteFile(sFile_bmp);

  
  result:=True;

  Log('make image done - '+ GetTimeDiff());
          
end;

// ---------------------------------------------------------------
function TTile_Loader.Load_CMap(const aMapX: TMap): HResult;
// ---------------------------------------------------------------
var
  k: Integer;

  blRect: TBLRect;
  e: Double;

  iResult: HResult;               

  sFileName: WideString; 

  r: TLoad_params;

  rBounds: TXYBounds;
  s: string;
              

  v:CMapXLayer;

  rTileBounds: TTileBounds;
  
label
  label_exit  ;
  
//const
//  def_offset = 0;  // 0.00001;

begin
  Assert(Params.Dir>'');
  Assert(Params.LayerName>'', 'Params.LayerName = ...');


  FMapX:=aMapX;

//  Load_Ini;

//  k:=aMapX.NumericCoordSys.Datum.Ellipsoid;

 if aMapX.NumericCoordSys.Datum.Ellipsoid<>28 then
 begin   
   aMapX.NumericCoordSys.Set_ (miLongLat, DATUM_WGS84,   //28;
        EmptyParam, EmptyParam, EmptyParam, EmptyParam,
        EmptyParam, EmptyParam, EmptyParam, EmptyParam,
        EmptyParam, EmptyParam, EmptyParam, EmptyParam);

   Log('aMapX.NumericCoordSys.Set_ (miLongLat, DATUM_WGS84,');
 end;


     

  CursorHourGlass;

//  aMapX. OnMapViewChanged;
  
//  aMapX.OnMapViewChanged:=nil; // procedure Tfrm_Map.Map1MapViewChanged(Sender: TObject);

                
  FillChar(r,SizeOf(r),0);

Log('aMapX.CenterY : ' + FloatToStr(aMapX.CenterY));
Log('aMapX.CenterX : ' + FloatToStr(aMapX.CenterX));
Log('aMapX.Zoom :  '   + FloatToStr(aMapX.Zoom));

//  if aMapX.CenterY < -1 then

  if aMapX.CenterY < -90 then     
    aMapX.CenterY := -90;      
          
//    aMapX.ZoomTo (3000, 57, 59);
  
      
  r.Lat:=aMapX.CenterY;
  r.Lon:=aMapX.CenterX; 
  
                           
  
  r.Width :=Round(aMapX.MapScreenWidth);
  r.Height:=Round(aMapX.MapScreenHeight);  


  blRect:=mapx_XRectangleToBLRect(aMapX.Bounds);


//  
//  if not geo_CompareBLRects(blRect, FOldData.BLRect) or (Params.LayerName<>FOldData.LayerName)
//  then begin
// // ShowMessage('if not geo_CompareBLRects(blRect, FOldData.BLRect) or (Params.LayerName<>FOldData.LayerName)');
//
//    FOldData.BLRect:=blRect;
//    FOldData.LayerName:=Params.LayerName;  
//  end else
//    exit;

  
//  blRect:=mapx_GetBounds(Map1);

  FillChar(rBounds,SizeOf(rBounds),0);

  if Abs(blRect.TopLeft.b - blRect.BottomRight.b) > 0 then
  begin     
    rBounds.x_max:=blRect.TopLeft.B;// + def_offset;
    rBounds.y_min:=blRect.TopLeft.L;// - def_offset;
    rBounds.x_min:=blRect.BottomRight.B;// - def_offset;
    rBounds.y_max:=blRect.BottomRight.L;// + def_offset;

  end;
        
      
  iResult:=Load(r, sFileName, rBounds);

//CodeSite.Send(sFileName);
  
  
  Assert(sFileName<>'', 'sFileName = '+ sFileName);

  if mapx_FileExists(aMapX, sFileName) then
    goto label_exit;

//  if sFileName then
                           
  blRect.TopLeft.B:=rBounds.x_max;
  blRect.TopLeft.L:=rBounds.y_min;  
  blRect.BottomRight.B:=rBounds.x_min;
  blRect.BottomRight.L:=rBounds.y_max;  
                                                  

  //!!!!!!!!!!!
  aMapX.AutoRedraw :=False; 


  mapx_TilesClear (aMapX);

  aMapX.Layers.Add (sFileName, aMapX.Layers.Count+1) ;

  
//--  map_MapAdd_Ex_CMapX(aMapX, sFileName);

  mapx_SetBounds(aMapX, blRect);

  
  //!!!!!
  aMapX.AutoRedraw :=True;    


label_exit:


//  aMapX.         

//  aMapX.OnMapViewChanged:=FOnMapViewChanged; // procedure Tfrm_Map.Map1MapViewChanged(Sender: TObject);
           
   
  CursorDefault;
 
end;

// ---------------------------------------------------------------
function TTile_Loader.Load_HTTP(aFileName: string; aX, aY: LongWord; aZ: byte):
    boolean;
// ---------------------------------------------------------------

    // ---------------------------------------------------------------
    function DoGetImageFormat(aFileName: string): string;
    // ---------------------------------------------------------------
    var
      oFileStream: TFileStream;
      buf: array[0..3] of AnsiChar ;
      iRead: Integer;
      k: Integer;

      sComp: AnsiString;
    begin

      oFileStream:=TFileStream.Create (aFileName, fmShareDenyNone);
      oFileStream.Position:=6;
      iRead:=oFileStream.Read (buf, SizeOf(buf));

      SetString(sComp, PAnsiChar(@buf[0]), Length(buf));

      
      if sComp= 'JFIF' then 
        result:='.jpg'
      else
        result:='.png';      
                   
      
      
      FreeAndNil(oFileStream);
    end;




var
  b: Boolean;
  iSize: Int64;
  iSize1: Int64;
  k: Integer;
  oFileStream: TFileStream;
  s: string;
  sContentType: string;
  sExt: string;
  sFile_bin: string;
  sFile_img: string;
  sURL: string;
begin
//  CodeSite.Send('TTileManagerX.Load_HTTP ...');


  Assert(Params.URL<>'');
  Assert(aZ<30);


  if FileExists(ChangeFileExt(aFileName,'.png')) or
     FileExists(ChangeFileExt(aFileName,'.jpg'))
  then
  begin
    Result:=True;
    Exit;
//    Exit(True);
  end;




  s:=ExtractFileDir(aFileName);
  ForceDirectories(s);


  if Pos('#z',Params.URL)>0 then
  begin
    sURL:= Params.URL;

    sURL:= ReplaceStr (sURL, '#z', IntToStr(aZ) );
    sURL:= ReplaceStr (sURL, '#y', IntToStr(aY) );
    sURL:= ReplaceStr (sURL, '#x', IntToStr(aX) );

//    if RightStr(sURL,4)='.png' then
//      sFile_img:= ChangeFileExt(aFileName,'.png');

  end else
    sURL:=Params.URL + Format('&y=%d&x=%d&z=%d',[ay,ax,az]) ;//+ '&format=image/png';
//    sURL:=Params.URL + Format('&y=%d&x=%d&z=%d',[ay,ax,az]);


// sURL:=sURL + '&format=image/png';

  Log ('Loading... ' + sURL)   ;

//   function IsStrInStrArray(aStr: string; aStrArray: array of string): boolean;

  s:=Params.LayerName;


//  sFile_img:=ChangeFileExt(aFileName, sExt);
  sFile_bin:=ChangeFileExt(aFileName, '.bin');

//Log('Load: ' + sURL );
  if Pos('https',sURL)>0 then
    b:=DownloadFile_HTTPS (sURL, sFile_bin)
  else
    b:=DownloadFile (sURL, sFile_bin, Params.Z);

  if not b then
    Exit;

  sExt:=DoGetImageFormat (sFile_bin);
  sFile_img:=ChangeFileExt(aFileName, sExt);

  MoveFile(sFile_bin, sFile_img);

//  TFile.Move(sFile_bin, sFile_img);

    //content-type:image/png

  Result:=True;


  
  if not Result then
  begin  
//    ShowMessage(sFile_img + ' iSize=0  ');
  
//    Log (sFile_img + ' iSize=0  ');
//    SysUtils.DeleteFile(sFile_img);
//    SysUtils.DeleteFile(sFile_bin);

  end ;
//    TFile.FileS


  Log('TTileManagerX.Load_HTTP ... Done');

end;


// ---------------------------------------------------------------
procedure TTile_Loader.Load_Tile(aX, aY: LongWord; aZ: byte);
// ---------------------------------------------------------------
var
  sFile: string;    
begin
  Log(Format('TdmTile_Loader.Load_Tile - x:%d, y:%d, z:%d', [aX,aY,aZ]));


  sFile:=GetFileName_TAB(ax,ay,az);
//  if not FileExists(sFile) then
//    sFile:=GetFileName_TAB(ax,ay,az, 1);

  
 if not FileExists(sFile) then
   Load_HTTP(sFile,ax,ay,az)
    
end;

procedure TTile_Loader.Log(aMsg: string);
begin
  Tdlg_WMS_log.Add(aMsg);

//  CodeSite.Send(aMsg);

end;


// ---------------------------------------------------------------
procedure TTile_Loader.SaveTileFile_WGS_EPSG_3395_bounds(aFileName,
    aImgFileName: string; aEPSG: word; aBounds: TXYBounds; aImage_width,
    aImage_height: word; aZ: byte);
// ---------------------------------------------------------------
const
  DEF_FILE_meter_10_104 = 
  '!table'+ CRLF +
  '!version 300'+ CRLF +
  '!charset WindowsCyrillic'+ CRLF +
  ''+ CRLF +
  'Definition Table'+  CRLF +
  '  File ":file"'+ CRLF +
  '  Type "RASTER"'+  CRLF +
  
  //left top
  '  (:point3_lon,:point3_lat) (0,0) Label "point 1",'+ CRLF +
  //right bottom
  '  (:point1_lon,:point1_lat) (:img_width,:img_height) Label "point 2",'+ CRLF +
  //left bottom  
  '  (:point2_lon,:point2_lat) (0,:img_height) Label "point 3",'+CRLF +
  //rigth top
  '  (:point4_lon,:point4_lat) (:img_width,0) Label "point 4"'+ CRLF +


  '  CoordSys Earth Projection 10, 104, "m", 0'+ CRLF +
  '  Units "m"'+ CRLF +
//  '  Units "degree"'+ CRLF +  
  '  RasterStyle 4 1'+ CRLF +
  '  RasterStyle 7 0'; //+ CRLF +

var       
  s: string;

  points: array[0..3] of TLatLon;

  I: Integer;
  iZone_gk: Integer;

  sFile: string;

//  rGDAL_json: TGDAL_json;
//  sFile_bat: string;
//  sJsonFile: string;

const  
  DEF_EPSG_3395 = 3395;
              

begin           
  
  aBounds.X_Max:=aBounds.X_Max ;
  aBounds.X_Min:=aBounds.X_Min ;
  
  aBounds.Y_Min:=aBounds.Y_Min ;
  aBounds.Y_Max:=aBounds.Y_Max ;
  

  Assert(aEPSG>0);
            
//
  //right bottom
  Plane_EPSG_To_EPSG_plane(aEPSG, DEF_EPSG_3395, aBounds.X_Max, aBounds.Y_Min, points[2].Lat  , points[2].Lon  );    
  //left top
  Plane_EPSG_To_EPSG_plane(aEPSG, DEF_EPSG_3395, aBounds.X_Min, aBounds.Y_Max, points[0].Lat  , points[0].Lon  );
  Plane_EPSG_To_EPSG_plane(aEPSG, DEF_EPSG_3395, aBounds.X_Min, aBounds.Y_Min, points[1].Lat  , points[1].Lon  );
  Plane_EPSG_To_EPSG_plane(aEPSG, DEF_EPSG_3395, aBounds.X_Max, aBounds.Y_Max, points[3].Lat  , points[3].Lon  );
  

  

// ---------------------------------------------------------------    
// meter
// ---------------------------------------------------------------    

  s:=DEF_FILE_meter_10_104;

  sFile:=ExtractFileName(aImgFileName);
  
  s:=ReplaceStr(s, ':file', sFile);

  s:=ReplaceStr(s, ':img_width',  IntToStr(aImage_width));
  s:=ReplaceStr(s, ':img_height', IntToStr(aImage_height));  

  for I := 0 to 3 do
  begin
    s:=ReplaceStr(s, Format(':point%d_lon',[i+1]), FloatToStr(Trunc(points[i].Lon)));
    s:=ReplaceStr(s, Format(':point%d_lat',[i+1]), FloatToStr(Trunc(points[i].Lat )));
  end;

  StrToTextFile (aFileName, s );
         
end;


function TTile_Loader.Z_Max: byte;
begin
  if Params.Z_max > 0 then
//  if Eq(Params.LayerName,'OpenStreetMap') or Eq(Params.LayerName,'2gis')  then
    Result := Params.Z_max
  else
    Result := 18;
end;



// ---------------------------------------------------------------
procedure TTile_Loader.Load_Ini;
// ---------------------------------------------------------------
 {
    // ---------------------------------------------------------------
    function GetModuleFileName: string;
    // ---------------------------------------------------------------
    //var
    //  szFileName: array[0..MAX_PATH] of Char;
    begin
    //  FillChar(szFileName, SizeOf(szFileName), #0);
      //unit SysInit;
      Result := GetModuleName(hInstance); //, szFileName, MAX_PATH);
    //  Result := szFileName;
    end;
  }
var
  ini: TIniFile;
  s, sFile: string;
//  sModule: string;

begin

//  sFile :=SysUtils.GetModuleName(HInstance);

 // s:=ExtractFileExt(sFile);

//  sModule:=GetModuleFileName();

 // s:=ExtractFileExt(sModule);

 // if s='.exe' then
  //  sFile:= 'C:\ONEGA\RPLS_DB\bin\WMS\wms.ini'
//  else

//  if GetModuleFileName()<>'' then
    sFile:= ExtractFilePath(Application.ExeName) + 'wms.ini';
    if not FileExists (sFile) then
      sFile:= ExtractFilePath(Application.ExeName) + 'WMS\wms.ini';

 // else
  //  sFile:= 'C:\ONEGA\RPLS_DB\bin\WMS\wms.ini';

//   sFile:= ChangeFileExt( GetModuleFileName(), '.ini');

  Log(sFile);


 // Exit;

//  sFile:=''

  if FileExists(sFile) then
  begin
    ini:=TIniFile.Create(sFile);
    s:=ini.ReadString('main','dir','');

    if s<>'' then
       Params.Dir:= IncludeTrailingBackslash(s);

//    s:=ini.ReadString('main','gdal','');
//
//    if s<>'' then
//       Params.Dir:= IncludeTrailingBackslash(s);



    FreeAndNil(ini);
  end;


//  FreeAndNil(oSList);

  //135

end;


end.



{
try
  ...
except
  on E: Exception do ErrorDialog(E.Message, E.HelpContext);
end;


