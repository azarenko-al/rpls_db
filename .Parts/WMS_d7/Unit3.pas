unit Unit3;

interface

uses
  x_Tile_Manager,
  d_WMS_log,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, rxToolEdit, OleCtrls, MapXLib_TLB, rxPlacemnt,
  ExtCtrls;

type
  TForm3 = class(TForm)
    Map1: TMap;
    FilenameEdit1: TFilenameEdit;
    Button13: TButton;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Panel1: TPanel;
    Edit1: TEdit;
    FormStorage1: TFormStorage;
    procedure Button13Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FITileManagerX: TTileManagerX;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

procedure TForm3.Button13Click(Sender: TObject);
begin
  Map1.Layers.Add(FilenameEdit1.FileName, 0);
end;

procedure TForm3.Button1Click(Sender: TObject);
begin
  Map1.PropertyPage;
end;

procedure TForm3.Button2Click(Sender: TObject);
begin
  FITileManagerX.Set_Center(59.8243, 30.16, 10);
end;

procedure TForm3.Button3Click(Sender: TObject);
begin
  FITileManagerX.Load_CMap;

end;


procedure TForm3.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FITileManagerX);
end;

procedure TForm3.FormCreate(Sender: TObject);
begin
  FITileManagerX:=TTileManagerX.Create;
  FITileManagerX.Init(Map1);

//  FITileManagerX.Set_WMS_layer('GoogleMap');
//  FITileManagerX.Set_WMS_layer('YandexMap_https');

  FITileManagerX.Set_WMS_layer('YandexMap', 'http://tile1.maps.2gis.com/tiles?v=1', 3857, 18);

  //http://tile1.maps.2gis.com/tiles?v=1
  //3857
  //18

  FITileManagerX.Set_Dir('d:\wms_111');

  Tdlg_WMS_log.ShowForm;

//  FITileManagerX.Set_Center(0, 0, 3);

//  FITileManagerX.Set_Center(60, 179, 4);

//  FITileManagerX.Set_Center(0, 0, 3);
//  FITileManagerX.Set_Center(34, 175, 4);

//  FITileManagerX.Set_Center(34, 0, 4);

//  FITileManagerX.Set_Center(0, -180, 3);

//   FITileManagerX.Set_Center(59.8243, 30.16, 10);

end;

{
// ---------------------------------------------------------------
procedure Tdlg_MIF_export1.Insert_wms_map_ActiveX;
// ---------------------------------------------------------------

var
  vTileManager: ITileManagerX;

//  oWMS: TWmsComClient;

begin
  if not Params.WMS.Checked then
    exit;


  vTileManager:=WMS_Init_ITileManagerX();

//  oWMS:=TWmsComClient.Create;

//  vTileManager:=oWMS.Intf;

//  vTileManager:=CoTileManagerX.Create;
  vTileManager.Init(Map1.DefaultInterface);


//  DbugIntf.Se

  //FMapEngine.


 // oTileManager:=TTileManager.Create;

  assert(Params.WMS.Dir<>'', 'Params.WMS.Dir null');


  vTileManager.Set_Dir(  Params.WMS.Dir);
//  FTileManagerX.Set_Dir(IncludeTrailingBackslash(Trim(DirectoryEdit_WMS.Text)));

//  WMS.LayerName:='GoogleMap';

  assert(Params.WMS.LayerName<>'', 'Params.WMS.LayerName null');


//  if Params.WMS.LayerName='' then
  //  Params.WMS.LayerName:='YandexSat';

  vTileManager.Set_WMS_Layer (Params.WMS.LayerName);

  if Params.WMS.Z>0 then
    vTileManager.Set_Z(Params.WMS.Z);


// ---------------------------------------------------------------  if Params.WMS.Z>0 then
//    vTileManager.Set_Z (Params.WMS.Z);

  vTileManager.Load_CMap();

//  vTileManager:=nil;

//  _WMS_Update;

//  FreeAndNil (oTileManager);

//  FreeAndNil (oWMS);


 // DbugIntf.
end;
}



end.
