program test_WMS;

uses
  Forms,
  Unit3 in 'Unit3.pas' {Form3},
  d_WMS_log in 'src\d_WMS_log.pas' {dlg_WMS_log},
  d_WMS_tools in 'src\d_WMS_tools.pas' {dlg_WMS_tools},
  u_WMS_file in 'src\u_WMS_file.pas',
  wms_TLB in 'src\WMS_TLB.pas',
  wmslist in 'src\wmslist.pas',
  x_Tile_Manager in 'src\x_Tile_Manager.pas',
  u_panorama_coord_convert in 'src\u_panorama_coord_convert.pas',
  u_Tile_loader in 'src\u_Tile_loader.pas',
  u_web in '..\..\..\common7\Package_Common\u_web.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm3, Form3);
  Application.Run;
end.
