inherited dmAct_AntType: TdmAct_AntType
  OldCreateOrder = True
  Left = 1234
  Top = 479
  Height = 303
  Width = 317
  inherited ActionList1: TActionList
    Left = 36
    Top = 20
    inherited act_Object_ShowOnMap: TAction
      OnExecute = act_Object_ShowOnMapExecute
    end
    object act_Import: TAction
      Caption = 'act_Import'
    end
    object act_LoadFromFile: TAction
      Category = 'AntType'
      Caption = 'act_LoadFromFile'
    end
    object act_Repair_TiltType: TAction
      Category = 'AntType'
      Caption = #1048#1089#1087#1088#1072#1074#1080#1090#1100' '#1090#1080#1087' '#1085#1072#1082#1083#1086#1085#1072
    end
  end
  object ActionList2: TActionList
    Left = 33
    Top = 88
    object act_Copy: TAction
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
    end
    object act_Update_Band: TAction
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1076#1080#1072#1087#1072#1079#1086#1085
    end
    object act_Export_MDB: TAction
      Caption = 'Export_MDB'
    end
    object act_Import_MDB: TAction
      Caption = 'Import_MDB'
    end
    object act_Audit1: TAction
      Caption = 'act_Audit1'
    end
    object Action1: TAction
      Caption = 'Action1'
    end
    object act_Restore_bands: TAction
      Caption = 'act_Restore_bands'
    end
    object act_Set_Band: TAction
      Caption = 'act_Set_Band'
    end
    object act_Audit: TAction
      Caption = 'act_Audit'
    end
    object act_restore_polarization_ratio: TAction
      Caption = #1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1079#1072#1097#1080#1090#1085#1086#1077' '#1086#1090#1085#1086#1096#1077#1085#1080#1077' H/V [dB]'
    end
    object act_MSI: TAction
      Caption = 'act_MSI'
    end
  end
  object SaveDialog_MSI: TSaveDialog
    Filter = '*.msi|*.msi'
    Left = 168
    Top = 16
  end
end
