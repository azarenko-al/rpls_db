unit dm_act_AntType;

interface
{$I ver.inc}

uses
//  d_Audit,

  Classes, Forms, Dialogs, Menus, ActnList,  SysUtils,


  i_Audit,

  dm_User_security,
  dm_Folder,


  u_DataExport_run,

  dm_Onega_DB_data,

  I_Object,
  
  u_Shell_new,

  dm_act_Base,
  dm_AntType,

  u_dlg,

  u_func,
  u_run,

  u_types,
  u_const_db,
  u_const,
  u_const_str,

  fr_AntType_view,
  d_AntType_add

  ;

type
  TdmAct_AntType = class(TdmAct_Base)
    act_Import: TAction;
    act_LoadFromFile: TAction;
    act_Repair_TiltType: TAction;
    ActionList2: TActionList;
    act_Copy: TAction;
    act_Update_Band: TAction;
    act_Export_MDB: TAction;
    act_Import_MDB: TAction;
    act_Audit1: TAction;
    Action1: TAction;
    act_Restore_bands: TAction;
    act_Set_Band: TAction;
    act_Audit: TAction;
    act_restore_polarization_ratio: TAction;
    act_MSI: TAction;
    SaveDialog_MSI: TSaveDialog;

    procedure act_Object_ShowOnMapExecute(Sender: TObject);
//    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    function CheckActionsEnable: Boolean;
//    procedure CheckActionsEnable;
    procedure Copy;
  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm (aOwnerForm: TForm): TForm; override;

    procedure DoAction (Sender: TObject);

    function ItemDel(aID: integer; aName: string=''): boolean; override;

    function ItemAdd (aFolderID: integer): integer; override;

  public
    procedure ImportData (aFolderID: integer; aGUID: string='');

    function Dlg_Add(aFolderID: integer =0): integer;

    procedure Dlg_Mask_to_MSI(aID: integer);

    class procedure Init;
  end;

var
  dmAct_AntType: TdmAct_AntType;


//==================================================================
//implementation
//==================================================================
implementation

uses dm_Main; {$R *.dfm}


procedure TdmAct_AntType.act_Object_ShowOnMapExecute(Sender: TObject);
begin
  inherited;
end;

//--------------------------------------------------------------------
class procedure TdmAct_AntType.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_AntType) then
    Application.CreateForm(TdmAct_AntType, dmAct_AntType);

end;


//--------------------------------------------------------------------
procedure TdmAct_AntType.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_ANTENNA_TYPE;

  act_Copy.Caption  := STR_ACT_COPY;

  act_Repair_TiltType.Visible:= false;
                       //STR_IMPORT;
  act_Import.Caption:=STR_IMPORT;
//  act_Export_MDB.Caption:=STR_IMPORT;

   //'������';
 // act_LoadFromFile.Caption:=STR_IMPORT;
//  act_Import_ADF.Caption:= '������ ADF';

  act_Add.Caption :='������� ������ �������';
//  act_Audit.Caption :='������� ������ �������';

   act_Audit.Caption:=STR_Audit;


  act_Export_MDB.Caption:=DEF_STR_Export_MDB;
  act_Import_MDB.Caption:=DEF_STR_Import_MDB;


  act_Restore_bands.Caption:='������������ ���������';

  act_Set_Band.Caption:='���������� ���������';

  act_msi.Caption:='��������� � MSI';

{


  act_Audit.Caption:=STR_Audit;
  act_Audit.Enabled:=False;

}
  SetActionsExecuteProc ([
                         // act_Add_Item,
                          act_Audit,

                          act_Import,
                          act_Copy,
                          act_Export_MDB,
                          act_Import_MDB,

                          act_LoadFromFile,
                          act_Update_Band,

                          act_Restore_bands,
                          act_Set_Band,

                          act_restore_polarization_ratio,
                          act_msi

                          ], DoAction);


  Assert (Assigned (dmUser_Security));

  {

  SetActionsEnabled1( [
                          act_Copy,
                          act_Export_MDB,
                        //  act_Import_MDB,

                          act_LoadFromFile,
                          act_Update_Band,

                          act_Restore_bands,
                          act_Set_Band
                      ],
   dmUser_Security.Is_Lib_Edit_allow );
  }

  {
TdmUser_Security = class(TDataModule)
    sp_Projects: TADOStoredProc;
    ADOConnection1: TADOConnection;
    sp_GeoRegions: TADOStoredProc;
    procedure DataModuleCreate(Sender: TObject);
  private
    function Project_is_ReadOnly11111: Boolean;

  public
    ProjectIsReadOnly: Boolean;

    Is_Lib_Edit_allow: boolean;

}

 // OpenDialog_ADF.Filter:= 'ADF(.adf)|*.adf';
end;




// ---------------------------------------------------------------
procedure TdmAct_AntType.Copy;
// ---------------------------------------------------------------
var
  iFolder_ID: Integer;
  iID: integer;
  sFolderGUID: string;
  sNewName: string;
begin

  sNewName:=FFocusedName +' (�����)';

  if InputQuery ('����� ������','������� ����� ��������', sNewName) then
  begin
    iID:=dmOnega_DB_data.AntennaType_Copy(FFocusedID, sNewName);
//      dmAntType.Copy (FFocusedID, sNewName);

//      sGUIDg_Obj.ItemByName[OBJ_ANTENNA_TYPE].RootFolderGUID;

  //  g_Shell.UpdateNodeChildren_ByGUID(GUID_ANTENNA_TYPE);
  //  g_Shell.Shell_UpdateNodeChildren_ByGUID(GUID_ANTENNA_TYPE);




     iFolder_ID:=dmAntType.GetFolderID(iID);

   //  if aFolderID=0 then
     if iFolder_ID>0 then
       sFolderGUID:=dmFolder.GetGUIDByID(iFolder_ID)
     else
       sFolderGUID:=g_Obj.ItemByName[OBJ_ANTENNA_TYPE].RootFolderGUID;
    // else
     //  sFolderGUID:=dmFolder.GetGUIDByID(aFolderID);

     g_Shell.UpdateNodeChildren_ByGUID (sFolderGUID);

   //  g_ShellEvents.Shell_EXPAND_BY_GUID(sFolderGUID);
     g_Shell.EXPAND_BY_GUID(sFolderGUID);

     g_Shell.FocuseNodeByObjName (OBJ_ANTENNA_TYPE, iID);

  end;
end;

//--------------------------------------------------------------------
procedure TdmAct_AntType.DoAction (Sender: TObject);
//--------------------------------------------------------------------
var
  iID: integer;
  s: string;
  sNewName: string;
  sSQL: string;
  sValue: string;

begin
// dmOnega_DB_data.AntennaType_restore_from_mask(FAntTypeID);



  if Sender=act_MSI then
    Dlg_Mask_to_MSI(FFocusedID) else


  if Sender=act_restore_polarization_ratio then
    dmOnega_DB_data.AntennaType_restore_from_mask(FFocusedID) else


  if Sender=act_Audit then
//    Dlg_Audit(TBL_ANTENNATYPE, FFocusedID) else
     Init_IAuditX.ExecDlg( dmMain.GetConnectionString, TBL_ANTENNATYPE, FFocusedID) else


     //

  if Sender=act_Set_Band then
  begin
    if inputQuery ('��������� ������','��������', sValue) then
    begin
      s:=FSelectedIDList.ToString;
      sSQL:='update antennaType set band=:band where id in ('+ s +')';

      dmOnega_DB_data.ExecCommand_(sSQL, [FLD_BAND, sValue]);
    end;

  end
//    TDataExport_run.ExportToRplsDb_selected (TBL_ANTENNATYPE, FSelectedIDList)
  else



  if Sender=act_Restore_bands then
     dmAntType.RestoreBands
//    TDataExport_run.ExportToRplsDb_selected (TBL_ANTENNATYPE, FSelectedIDList)
  else



  if Sender=act_Export_MDB then
    TDataExport_run.ExportToRplsDb_selected (TBL_ANTENNATYPE, FSelectedIDList)
  else

  if Sender=act_Import_MDB then
    TDataExport_run.ImportFromRplsDbGuides
  else


  if Sender=act_Update_Band then
  begin
    if InputQuery ('���������� ��������','������� ��������', sNewName) then
      dmAntType.UpdateBand (FSelectedIDList, sNewName);

  end else

  // ---------------------------------------------------------------
  if Sender=act_Copy then
  // ---------------------------------------------------------------
  begin
    Copy();
  end else

  //----------------
  if Sender=act_Import then
  begin
    ImportData (FFocusedID, FFocusedGUID);
  //  SH_PostUpdateNodeChildren (FFocusedGUID);
  end

  else
    raise Exception.Create('');



  //----------------
{  if Sender=act_Repair_TiltType then
    dmAntType.RepairTiltType (SelectedIDList);
}

end;


function TdmAct_AntType.ItemAdd (aFolderID: integer): integer;
begin
  Result:=Dlg_Add (aFolderID);
//  Result:=dmAntType.Dlg_Add (aFolderID);

//  Result:=dmAntType_.Dlg_AntType_add (aFolderID);
end;

function TdmAct_AntType.ItemDel(aID: integer; aName: string=''): boolean;
begin
  Result:=dmAntType.Del (aID);
end;

//--------------------------------------------------------------------
procedure TdmAct_AntType.ImportData (aFolderID: integer; aGUID: string);
//--------------------------------------------------------------------
begin
  if RunApp (GetApplicationDir() + EXE_ANT_TYPE_IMPORT, AsString(aFolderID)) then
//  if Tdlg_AntennaType_import.ExecDlg (aFolderID) then
    g_Shell.UpdateNodeChildren_ByGUID (aGUID);
end;


function TdmAct_AntType.Dlg_Add(aFolderID: integer =0): integer;
begin
  Result := Tdlg_AntType_add.ExecDlg(aFolderID);
end;



//--------------------------------------------------------------------
function TdmAct_AntType.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_AntType_View.Create(aOwnerForm);
end;

// ---------------------------------------------------------------
function TdmAct_AntType.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
  Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];

  SetActionsEnabled1( [
          act_msi,
          act_Del_,
          act_Del_list,
          act_Copy,
        //  act_Export_MDB,
          act_Import_MDB,
          act_Import,

          act_LoadFromFile,
          act_Update_Band,

          act_Restore_bands,
          act_Set_Band,

          act_restore_polarization_ratio

      ],

   Result );


  //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );


end;

// ---------------------------------------------------------------
procedure TdmAct_AntType.Dlg_Mask_to_MSI(aID: integer);
// ---------------------------------------------------------------
var
  s: string;
begin
  if SaveDialog_MSI.Execute then
  begin
    dmOnega_DB_data.OpenStoredProc(dmOnega_DB_data.ADOStoredProc1, 'lib.sp_AntennaType_MSI_SEL',[FLD_ID,aID]);

    s:=dmOnega_DB_data.ADOStoredProc1.Fields[0].AsString;

    StrToFile(s, ChangeFileExt(SaveDialog_MSI.FileName, '.msi'));
  end;
end;


//--------------------------------------------------------------------
procedure TdmAct_AntType.GetPopupMenu;
//--------------------------------------------------------------------
var
  bEnable: Boolean;
begin
  bEnable:=CheckActionsEnable();


  case aMenuType of
    mtFolder: begin
               // AddMenuItem (aPopupMenu, act_Add_Item);
                  AddFolderMenu_Create (aPopupMenu, bEnable);

               // AddMenuItem (aPopupMenu, act_Add);
                AddMenuItem (aPopupMenu, act_Import);
                AddMenuItem (aPopupMenu, act_Export_MDB);

              //  AddMenuItem (aPopupMenu, act_Import_ADF);
             //   AddMenuItem (aPopupMenu, act_Repair_TiltType);
                AddFolderPopupMenu (aPopupMenu, bEnable);

                AddMenuItem (aPopupMenu, act_Restore_bands);

              end;

    mtItem:   begin
                AddMenuItem (aPopupMenu, act_Del_);
                AddMenuItem (aPopupMenu, act_Copy);

                AddMenuDelimiter (aPopupMenu);
             //   AddMenuItem (aPopupMenu, act_LoadFromFile );
              //  dlg_AddMenuItem (aPopupMenu, act_Folder_MoveTo);
                AddMenuItem (aPopupMenu, act_Repair_TiltType);

//                if bEnable then
                AddFolderMenu_Tools (aPopupMenu);

                AddMenuDelimiter (aPopupMenu);

                AddMenuItem (aPopupMenu, act_Export_MDB);
                AddMenuItem (aPopupMenu, act_Import_MDB);
            //    AddMenuItem (aPopupMenu, act_Audit);
                AddMenuItem (aPopupMenu, act_Set_BAND);

                AddMenuItem (aPopupMenu, act_restore_polarization_ratio);

                AddMenuItem (aPopupMenu, nil);

                AddMenuItem (aPopupMenu, act_Audit);
                AddMenuItem (aPopupMenu, act_msi);


              end;
    mtList:   begin
              // dlg_AddMenuItem (aPopupMenu, act_Folder_MoveTo);
                AddMenuItem (aPopupMenu, act_Del_list);
                AddMenuDelimiter (aPopupMenu);
                AddMenuItem (aPopupMenu, act_Update_Band);
                AddMenuDelimiter (aPopupMenu);
                AddMenuItem (aPopupMenu, act_Export_MDB);

                if bEnable then
                  AddFolderMenu_Tools (aPopupMenu);

                AddMenuItem (aPopupMenu, act_Set_BAND);

              end;
  end;
end;


begin

end.


{

end;


procedure TdmAct_LinkEnd.DataModuleDestroy(Sender: TObject);
begin
  ILinkEnd := nil;

  inherited;


}

{  if Tdlg_AntennaType_import.ExecDlg (aFolderID) then
    SH_PostUpdateNodeChildren (aGUID);
end;
}d