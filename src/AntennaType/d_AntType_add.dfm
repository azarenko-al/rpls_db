inherited dlg_AntType_add: Tdlg_AntType_add
  Left = 569
  Top = 250
  Width = 524
  Height = 448
  Caption = 'dlg_AntType_add'
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 386
    Width = 516
    inherited Bevel1: TBevel
      Width = 516
    end
    inherited Panel3: TPanel
      Left = 320
      inherited btn_Ok: TButton
        Left = 353
      end
      inherited btn_Cancel: TButton
        Left = 437
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 516
    inherited Bevel2: TBevel
      Width = 516
    end
    inherited pn_Header: TPanel
      Width = 516
      Height = 55
    end
  end
  inherited Panel1: TPanel
    Width = 516
    inherited ed_Name_: TEdit
      Width = 505
    end
  end
  inherited Panel2: TPanel
    Width = 516
    Height = 259
    inherited PageControl1: TPageControl
      Width = 506
      Height = 249
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 498
          Height = 218
          BorderStyle = cxcbsNone
          Align = alClient
          LookAndFeel.Kind = lfStandard
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 151
          OptionsView.ValueWidth = 49
          TabOrder = 0
          Version = 1
          object row_Band: TcxEditorRow
            Properties.Caption = #1044#1080#1072#1087#1072#1079#1086#1085
            Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object row_Gain_dBi: TcxEditorRow
            Expanded = False
            Properties.Caption = #1059#1089#1080#1083#1077#1085#1080#1077' [dBi]'
            Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
            Properties.EditProperties.UseCtrlIncrement = True
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = ''
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
          object row_Freq_MHz: TcxEditorRow
            Expanded = False
            Properties.Caption = #1063#1072#1089#1090#1086#1090#1072' ['#1052#1075#1094']'
            Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
            Properties.EditProperties.UseCtrlIncrement = True
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 2
            ParentID = -1
            Index = 2
            Version = 1
          end
          object cxVerticalGrid1EditorRow4: TcxEditorRow
            Properties.Caption = 'DNA'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            Visible = False
            ID = 3
            ParentID = -1
            Index = 3
            Version = 1
          end
          object row_Diameter: TcxEditorRow
            Properties.Caption = 'Diameter'
            Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
            Properties.EditProperties.UseCtrlIncrement = True
            Properties.EditProperties.ValueType = vtFloat
            Properties.DataBinding.ValueType = 'Float'
            Properties.Value = Null
            ID = 4
            ParentID = -1
            Index = 4
            Version = 1
          end
          object row_BeamWidth: TcxEditorRow
            Properties.Caption = 'BeamWidth'
            Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
            Properties.EditProperties.UseCtrlIncrement = True
            Properties.DataBinding.ValueType = 'Float'
            Properties.Value = Null
            ID = 5
            ParentID = -1
            Index = 5
            Version = 1
          end
          object cxVerticalGrid1CategoryRow1: TcxCategoryRow
            Expanded = False
            ID = 6
            ParentID = -1
            Index = 6
            Version = 1
          end
          object row_Comment1: TcxEditorRow
            Expanded = False
            Properties.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 7
            ParentID = -1
            Index = 7
            Version = 1
          end
        end
      end
    end
  end
  inherited ActionList1: TActionList
    Left = 352
  end
  inherited FormStorage1: TFormStorage
    Left = 324
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 382
    Top = 2
  end
end
