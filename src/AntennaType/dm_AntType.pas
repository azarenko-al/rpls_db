unit dm_AntType;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms, Windows,  Variants, ComObj,

  dm_Onega_DB_data,

  dm_Main,

  u_types,

  u_log,
  u_classes,
  u_Func_arrays,  
  u_func,
  u_db,
  u_radio,

  u_const_db,
  u_const,

  u_antenna_mask,

  dm_Object_base
  ;


//used in fr_Antenna_mask
const
  DEF_MASK_HOR = 0;
  DEF_MASK_VER = 1;

type

  TdmAntTypeMaskArray  =  array of record
                           Angle: double;
                           Loss: double;
                         end;



  //-------------------------------------------------------------------
  TdmAntTypeInfoRec = record
  //-------------------------------------------------------------------
    Gain_dB           :  double;
    Diameter          :  double;

    Polarization_str  : string;

    Vert_Width        :  double;
    Horz_Width        :  double;
    Freq_MHz          :  double;

    Polarization_ratio: double;

    VertMask          : string;
    HorzMask          : string;

    HorzMaskArr,
    VertMaskArr: TdmAntTypeMaskArray;

  end;


  //-------------------------------------------------------------------
  TdmAntType = class(TdmObject_base)
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
  //  function Add (aRec: TdmAntTypeAddRec): integer;

    function GetGain(aID: integer): double;

  public
    procedure Mask_StrToArr      (aValue: string; var aMask: TdmAntTypeMaskArray);
    function  Mask_GetLossByAngle(var aMask: TdmAntTypeMaskArray; aAngle: double): double;
    function  Mask_GetLossesStr  (var aMask: TdmAntTypeMaskArray): string;

    procedure UpdateBand(aIDList: TIDList; aBand: string);
    procedure RestoreBands;

    function GetInfoRec11(aID: integer; var aRec: TdmAntTypeInfoRec): Boolean;
    function GetBand(aID: integer): string;
//    function Copy(aSourceID: integer; aName: string): integer;
    function GetRaznos_MHZ(aID: Integer): Double;


 //   class procedure Init;
  end;


function dmAntType: TdmAntType;


//================================================================
implementation {$R *.dfm}
//================================================================

var
  FdmAntType: TdmAntType;


{
// ---------------------------------------------------------------
class procedure TdmAntType.Init;
// ---------------------------------------------------------------
begin
  if not Assigned(dmAntType) then
    FdmAntType := TdmAntType.Create(Application);
end;
}

// ---------------------------------------------------------------
function dmAntType: TdmAntType;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmAntType) then
    FdmAntType := TdmAntType.Create(Application);

  Result:=FdmAntType;
end;


procedure TdmAntType.DataModuleCreate(Sender: TObject);
begin
  inherited;
  TableName:=TBL_ANTENNATYPE;
  ObjectName:=OBJ_ANTENNA_TYPE;
end;



//-------------------------------------------------------------------
function TdmAntType.GetInfoRec11(aID: integer; var aRec: TdmAntTypeInfoRec):
    Boolean;
//-------------------------------------------------------------------
//const
 // FLD_FREQ = 'FREQ';
begin
  FillChar (aRec, SizeOf(aRec), 0);

  db_OpenTableByID (qry_Temp, TBL_AntennaType, aID);

  with qry_Temp do
    if not IsEmpty then
  begin
    aRec.Gain_dB           :=FieldByName(FLD_GAIN).AsFloat;

    // OLD !!!!
    aRec.Freq_MHz          :=FieldByName(FLD_FREQ).AsFloat;

//    aRec.Freq_MHz          :=FieldByName(FLD_FREQ_MHZ).AsFloat;

    aRec.Diameter          :=FieldByName(FLD_Diameter).AsFloat;
  //  aRec.Polarization      :=FieldByName(FLD_POLARIZATION).AsInteger;

    aRec.Polarization_str  :=FieldByName(FLD_POLARIZATION_str).Asstring;
    aRec.POLARIZATION_RATIO:=FieldByName(FLD_POLARIZATION_RATIO).AsFloat;

    aRec.Vert_Width        :=FieldByName(FLD_Vert_Width).AsFloat;
    aRec.Horz_Width        :=FieldByName(FLD_Horz_Width).AsFloat;

    aRec.VertMask          :=FieldByName(FLD_vert_mask).AsString;
    aRec.HorzMask          :=FieldByName(FLD_horz_mask).AsString;

    Mask_StrToArr (aRec.HorzMask, aRec.HorzMaskArr);
    Mask_StrToArr (aRec.VertMask, aRec.VertMaskArr);

    Result:=true;
  end else begin
    g_Log.Add ('db_OpenQueryByID (qry_Temp2, TBL_ANTENNA_TYPE, aID);-qry_Temp2.IsEmpty');
    Result:=False;

  end;
end;

function TdmAntType.GetGain(aID: integer): double;
begin
  Result:=GetIntFieldValue(aID, FLD_GAIN);
end;


// ---------------------------------------------------------------
function TdmAntType.Mask_GetLossByAngle(var aMask: TdmAntTypeMaskArray; aAngle:
    double): double;
// ---------------------------------------------------------------
var i: Integer;
begin
  Result := 0;

  for i := 0 to High(aMask) do
    if (aAngle <= aMask[i].Angle) then
    begin
      Result := aMask[i].Loss;
      Exit;
    end;
end;


// ---------------------------------------------------------------
procedure TdmAntType.Mask_StrToArr(aValue: string; var aMask: TdmAntTypeMaskArray);
// ---------------------------------------------------------------
var
  strArr1,strArr2: TStrArray;
  i: Integer;
begin
  strArr1:=StringToStrArray (aValue,';');

  SetLength (aMask, Length(strArr1));

  for i := 0 to High(strArr1) do
  begin
    strArr2:=StringToStrArray (strArr1[i],'=');

    if Length(strArr2)=2 then
    begin
      aMask[i].Angle:=AsFloat(strArr2[0]);
      aMask[i].Loss :=AsFloat(strArr2[1]);
    end;
  end;

 // i:=0;
end;



function TdmAntType.GetBand(aID: integer): string;
begin
  Result := GetStringFieldValue (aID, FLD_BAND);
end;


// ---------------------------------------------------------------
function TdmAntType.GetRaznos_MHZ(aID: Integer): Double;
// ---------------------------------------------------------------
var
  eFreq_MHz: double;
begin
  eFreq_MHz:=GetDoubleFieldValue (aID, FLD_FREQ);
//  eFreq_MHz:=GetDoubleFieldValue (aID, FLD_FREQ_MHZ);
  if eFreq_MHz = 0 then
    Result :=0
  else
    Result := 300/eFreq_MHz*200;

end;

// ---------------------------------------------------------------
function TdmAntType.Mask_GetLossesStr(var aMask: TdmAntTypeMaskArray): string;
// ---------------------------------------------------------------
var i: Integer;
begin
  Result := '';

  for i := 0 to High(aMask) do
    Result := Result + Format('%1.1f=%1.1f', [aMask[i].Angle,aMask[i].Loss])
              + IIF(i< High(aMask),';','');
end;

// ---------------------------------------------------------------
procedure TdmAntType.RestoreBands;
// ---------------------------------------------------------------
begin
  dmOnega_DB_data.ExecStoredProc_('sp_Antennatype_restore_bands',[]);

end;


procedure TdmAntType.UpdateBand(aIDList: TIDList; aBand: string);
var
  I: Integer;
begin
  for I := 0 to aIDList.Count - 1 do
    gl_DB.UpdateRecord(TBL_AntennaType, aIDList[i].ID, [db_Par(FLD_BAND, aBand)]);

end;


//begin
end.







//------------------------------------------------------
//function TdmAntType.Copy(aSourceID: integer; aName: string): integer;
//------------------------------------------------------
//begin
//  Result:=dmOnega_DB_data.AntennaType_Copy(aSourceID, aName);
//end;