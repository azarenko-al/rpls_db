object frame_Antenna_mask: Tframe_Antenna_mask
  Left = 630
  Top = 211
  Width = 806
  Height = 652
  Caption = 'frame_Antenna_mask'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 790
    Height = 61
    Caption = 'ToolBar1'
    TabOrder = 1
    Visible = False
  end
  object pn_Type: TPanel
    Left = 0
    Top = 61
    Width = 790
    Height = 20
    Align = alTop
    Alignment = taLeftJustify
    BevelOuter = bvLowered
    Caption = 'pn_Type'
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object pn_Grid: TPanel
    Left = 655
    Top = 81
    Width = 135
    Height = 455
    Align = alRight
    BevelOuter = bvNone
    Caption = 'pn_Grid'
    TabOrder = 2
    object cxGrid1: TcxGrid
      Left = 0
      Top = 29
      Width = 135
      Height = 246
      Align = alTop
      TabOrder = 0
      LookAndFeel.Kind = lfFlat
      object cxGrid1DBTableView1: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        NavigatorButtons.First.Visible = False
        NavigatorButtons.PriorPage.Visible = False
        NavigatorButtons.Prior.Visible = False
        NavigatorButtons.Next.Visible = False
        NavigatorButtons.NextPage.Visible = False
        NavigatorButtons.Last.Visible = False
        NavigatorButtons.Edit.Visible = False
        NavigatorButtons.Refresh.Visible = False
        NavigatorButtons.SaveBookmark.Visible = False
        NavigatorButtons.GotoBookmark.Visible = False
        NavigatorButtons.Filter.Visible = False
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = ds_Data
        DataController.Filter.MaxValueListCount = 1000
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnPopup.MaxDropDownItemCount = 12
        OptionsBehavior.ImmediateEditor = False
        OptionsCustomize.ColumnSorting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsView.Navigator = True
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        OptionsView.Indicator = True
        Preview.AutoHeight = False
        Preview.MaxLineCount = 2
        object col_Angle1: TcxGridDBColumn
          DataBinding.FieldName = 'angle'
          PropertiesClassName = 'TcxSpinEditProperties'
          Properties.ValueType = vtFloat
          Options.Filtering = False
          Options.SortByDisplayText = isbtOn
          Options.Sorting = False
          Width = 59
        end
        object col_Loss1: TcxGridDBColumn
          DataBinding.FieldName = 'loss'
          PropertiesClassName = 'TcxSpinEditProperties'
          Properties.ValueType = vtFloat
          Options.Filtering = False
          Options.Sorting = False
          Width = 51
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
    object ActionMainMenuBar1: TActionMainMenuBar
      Left = 0
      Top = 0
      Width = 135
      Height = 29
      ActionManager = ActionManager1
      Caption = 'ActionMainMenuBar1'
      ColorMap.HighlightColor = clWhite
      ColorMap.BtnSelectedColor = clBtnFace
      ColorMap.UnusedColor = clWhite
      EdgeBorders = [ebLeft, ebTop, ebRight, ebBottom]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMenuText
      Font.Height = -12
      Font.Name = 'Segoe UI'
      Font.Style = []
      Spacing = 0
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 536
    Width = 790
    Height = 77
    Align = alBottom
    BevelOuter = bvNone
    BorderWidth = 4
    TabOrder = 3
    Visible = False
    object RadioGroup_Polar_or_Decart: TRadioGroup
      Left = 4
      Top = 4
      Width = 113
      Height = 69
      Align = alLeft
      Caption = #1057#1050
      ItemIndex = 0
      Items.Strings = (
        #1055#1086#1083#1103#1088#1085#1072#1103
        #1044#1077#1082#1072#1088#1090#1086#1074#1072)
      TabOrder = 0
    end
    object GroupBox1: TGroupBox
      Left = 249
      Top = 4
      Width = 176
      Height = 69
      Align = alLeft
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100
      TabOrder = 1
      object cb_Show_tables: TCheckBox
        Left = 16
        Top = 16
        Width = 105
        Height = 17
        Caption = #1058#1072#1073#1083#1080#1094#1072
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object cb_Show_6_db: TCheckBox
        Left = 16
        Top = 39
        Width = 153
        Height = 17
        Caption = #1059#1088#1086#1074#1077#1085#1100' '#1087#1086#1090#1077#1088#1100' 6 dB'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
    end
    object rg_Scale: TRadioGroup
      Left = 117
      Top = 4
      Width = 132
      Height = 69
      Align = alLeft
      Caption = #1043#1088#1072#1092#1080#1082' '#1044#1053#1040
      ItemIndex = 1
      Items.Strings = (
        #1083#1086#1075#1072#1088#1080#1092#1084#1080#1095#1077#1089#1082#1080#1081
        #1083#1080#1085#1077#1081#1085#1099#1081)
      TabOrder = 2
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 81
    Width = 489
    Height = 455
    ActivePage = TabSheet_polar
    Align = alLeft
    MultiLine = True
    Style = tsFlatButtons
    TabOrder = 4
    object TabSheet_polar: TTabSheet
      Caption = #1055#1086#1083#1103#1088#1085#1072#1103
      ImageIndex = 1
      object Chart_polar: TChart
        Left = 0
        Top = 0
        Width = 384
        Height = 424
        BackWall.Transparent = False
        Foot.Font.Color = clBlue
        Foot.Font.Name = 'Verdana'
        Gradient.Direction = gdBottomTop
        Gradient.EndColor = clWhite
        Gradient.MidColor = 15395562
        Gradient.StartColor = 15395562
        LeftWall.Color = 14745599
        LeftWall.Transparent = True
        LeftWall.Visible = False
        Legend.Font.Name = 'Verdana'
        Legend.Visible = False
        RightWall.Color = 14745599
        Title.Font.Name = 'Verdana'
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.Automatic = False
        BottomAxis.AutomaticMinimum = False
        BottomAxis.Axis.Color = 4210752
        BottomAxis.Grid.Color = 11119017
        BottomAxis.Increment = 30.000000000000000000
        BottomAxis.Labels = False
        BottomAxis.LabelsSeparation = 100
        BottomAxis.LabelStyle = talValue
        BottomAxis.MinorTickCount = 1
        BottomAxis.MinorTickLength = 1
        BottomAxis.TickLength = 1
        BottomAxis.TicksInner.Color = 11119017
        BottomAxis.Title.Font.Name = 'Verdana'
        DepthAxis.Axis.Color = 4210752
        DepthAxis.Grid.Color = 11119017
        DepthAxis.LabelStyle = talValue
        DepthAxis.MinorTickCount = 1
        DepthAxis.TicksInner.Color = 11119017
        DepthAxis.Title.Font.Name = 'Verdana'
        DepthTopAxis.Axis.Color = 4210752
        DepthTopAxis.Grid.Color = 11119017
        DepthTopAxis.TicksInner.Color = 11119017
        DepthTopAxis.Title.Font.Name = 'Verdana'
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.Axis.Color = 4210752
        LeftAxis.Grid.Color = 11119017
        LeftAxis.Grid.Visible = False
        LeftAxis.Labels = False
        LeftAxis.LabelsSeparation = 100
        LeftAxis.LabelStyle = talValue
        LeftAxis.MinorTickCount = 1
        LeftAxis.MinorTickLength = 1
        LeftAxis.TickLength = 1
        LeftAxis.TicksInner.Color = 11119017
        LeftAxis.Title.Font.Name = 'Verdana'
        RightAxis.Automatic = False
        RightAxis.AutomaticMinimum = False
        RightAxis.Axis.Color = 4210752
        RightAxis.Grid.Color = 11119017
        RightAxis.Labels = False
        RightAxis.LabelsSeparation = 100
        RightAxis.LabelStyle = talValue
        RightAxis.MinorTickCount = 1
        RightAxis.MinorTickLength = 1
        RightAxis.TickInnerLength = 1
        RightAxis.TickLength = 1
        RightAxis.TicksInner.Color = 11119017
        RightAxis.Title.Font.Name = 'Verdana'
        TopAxis.Automatic = False
        TopAxis.AutomaticMinimum = False
        TopAxis.Axis.Color = 4210752
        TopAxis.Grid.Color = 11119017
        TopAxis.Labels = False
        TopAxis.LabelsSeparation = 100
        TopAxis.LabelStyle = talValue
        TopAxis.MinorTickCount = 1
        TopAxis.MinorTickLength = 1
        TopAxis.TickInnerLength = 1
        TopAxis.TickLength = 1
        TopAxis.TicksInner.Color = 11119017
        TopAxis.Title.Font.Name = 'Verdana'
        View3D = False
        View3DWalls = False
        Zoom.Pen.Mode = pmNotXor
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        ColorPaletteIndex = 8
        object Series_ant: TPolarSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Shadow.Color = 8487297
          Marks.Visible = False
          SeriesColor = clRed
          Circled = True
          Shadow.Visible = False
          AngleIncrement = 30.000000000000000000
          AngleValues.Name = 'AngleValues'
          AngleValues.Order = loAscending
          Brush.Color = clWhite
          Brush.Style = bsClear
          CircleBackColor = clWhite
          CircleLabels = True
          ClockWiseLabels = True
          Pen.Color = clRed
          Pen.Width = 2
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
        end
        object Series_Radius: TPolarSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          Circled = True
          Shadow.Visible = False
          AngleIncrement = 30.000000000000000000
          AngleValues.Name = 'AngleValues'
          AngleValues.Order = loAscending
          Brush.Color = clWhite
          Brush.Style = bsClear
          ClockWiseLabels = True
          Pen.Color = 238
          Pen.Style = psDash
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
        end
        object Series_3: TPolarSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          SeriesColor = 16744703
          Circled = True
          Shadow.Visible = False
          AngleIncrement = 30.000000000000000000
          AngleValues.Name = 'AngleValues'
          AngleValues.Order = loAscending
          Brush.Color = clWhite
          Brush.Style = bsClear
          Pen.Color = 16744703
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
        end
        object Series_6: TPolarSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          SeriesColor = 16744703
          Circled = True
          Shadow.Visible = False
          AngleIncrement = 30.000000000000000000
          AngleValues.Name = 'Angle'
          AngleValues.Order = loAscending
          Brush.Color = clWhite
          Brush.Style = bsClear
          Pen.Color = 16744703
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
        end
        object Series_15: TPolarSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          SeriesColor = 16744703
          Circled = True
          Shadow.Visible = False
          AngleIncrement = 30.000000000000000000
          AngleValues.Name = 'Angle'
          AngleValues.Order = loAscending
          Brush.Color = clWhite
          Brush.Style = bsClear
          Pen.Color = 16744703
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
        end
        object Series_30: TPolarSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          SeriesColor = 16744703
          Circled = True
          Shadow.Visible = False
          AngleIncrement = 30.000000000000000000
          AngleValues.Name = 'Angle'
          AngleValues.Order = loAscending
          Brush.Color = clWhite
          Brush.Style = bsClear
          Pen.Color = 16744703
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
        end
        object Series_labels: TPolarSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Callout.ArrowHeadSize = 15
          Marks.Callout.Distance = -4
          Marks.Callout.Length = -21
          Marks.Margins.Left = 14
          Marks.Margins.Top = 13
          Marks.Margins.Right = 30
          Marks.Shadow.Color = 8487297
          Marks.ShapeStyle = fosRoundRectangle
          Marks.Transparent = True
          Marks.Visible = True
          Circled = True
          AngleIncrement = 30.000000000000000000
          AngleValues.Name = 'Angle'
          AngleValues.Order = loAscending
          Brush.Color = clWhite
          Brush.Style = bsClear
          Pen.Color = 195
          Pen.Visible = False
          Pointer.InflateMargins = True
          Pointer.Style = psCircle
          Pointer.Visible = True
        end
        object Series_45: TPolarSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          SeriesColor = 16744703
          Circled = True
          Shadow.Visible = False
          AngleIncrement = 30.000000000000000000
          AngleValues.Name = 'Angle'
          AngleValues.Order = loAscending
          Brush.Color = clWhite
          Brush.Style = bsClear
          Pen.Color = 16744703
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
        end
      end
    end
    object TabSheet_decart: TTabSheet
      Caption = #1044#1077#1082#1072#1088#1090#1086#1074#1072
      object Chart_decart: TChart
        Left = 0
        Top = 0
        Width = 361
        Height = 424
        BackWall.Transparent = False
        Foot.Font.Color = clBlue
        Foot.Font.Name = 'Verdana'
        Gradient.Direction = gdBottomTop
        Gradient.EndColor = clWhite
        Gradient.MidColor = 15395562
        Gradient.StartColor = 15395562
        LeftWall.Color = 14745599
        LeftWall.Visible = False
        Legend.Font.Name = 'Verdana'
        Legend.Visible = False
        RightWall.Color = 14745599
        Title.Font.Name = 'Verdana'
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.Automatic = False
        BottomAxis.AutomaticMaximum = False
        BottomAxis.AutomaticMinimum = False
        BottomAxis.Axis.Color = 4210752
        BottomAxis.Grid.Color = 11119017
        BottomAxis.Increment = 30.000000000000000000
        BottomAxis.Labels = False
        BottomAxis.LabelsSeparation = 50
        BottomAxis.LabelStyle = talMark
        BottomAxis.Maximum = 181.000000000000000000
        BottomAxis.Minimum = -181.000000000000000000
        BottomAxis.MinorTickCount = 4
        BottomAxis.TicksInner.Color = 11119017
        BottomAxis.Title.Font.Name = 'Verdana'
        DepthAxis.Axis.Color = 4210752
        DepthAxis.Grid.Color = 11119017
        DepthAxis.Labels = False
        DepthAxis.TicksInner.Color = 11119017
        DepthAxis.Title.Font.Name = 'Verdana'
        DepthTopAxis.Axis.Color = 4210752
        DepthTopAxis.Grid.Color = 11119017
        DepthTopAxis.Labels = False
        DepthTopAxis.TicksInner.Color = 11119017
        DepthTopAxis.Title.Font.Name = 'Verdana'
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.Axis.Color = 4210752
        LeftAxis.Grid.Color = 11119017
        LeftAxis.LabelsSeparation = 100
        LeftAxis.LabelStyle = talValue
        LeftAxis.PositionPercent = 50.000000000000000000
        LeftAxis.TicksInner.Color = 11119017
        LeftAxis.Title.Font.Name = 'Verdana'
        LeftAxis.Title.Visible = False
        RightAxis.Axis.Color = 4210752
        RightAxis.Grid.Color = 11119017
        RightAxis.TicksInner.Color = 11119017
        RightAxis.Title.Font.Name = 'Verdana'
        Shadow.Visible = False
        TopAxis.Axis.Color = 4210752
        TopAxis.Grid.Color = 11119017
        TopAxis.TicksInner.Color = 11119017
        TopAxis.Title.Font.Name = 'Verdana'
        View3D = False
        View3DWalls = False
        Zoom.Pen.Mode = pmNotXor
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        ColorPaletteIndex = 13
        object Series_decart: TLineSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.Name = 'X'
          XValues.Order = loNone
          YValues.Name = 'Y'
          YValues.Order = loNone
          Data = {
            001900000000000000006C8B4000000000008888400000000000E88740000000
            0000A0894000000000007889400000000000A485400000000000388340000000
            0000407F400000000000307B400000000000F07E400000000000C07740000000
            0000D873400000000000E075400000000000E06F400000000000606840000000
            00007072400000000000B073400000000000E06F400000000000306B40000000
            0000206C4000000000008873400000000000C067400000000000307140000000
            00004872400000000000F07940}
        end
        object Series_decart_3: TLineSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          SeriesColor = clGray
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series_decart_15: TLineSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          SeriesColor = clGray
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series_decart_labels: TLineSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Callout.Distance = 2
          Marks.Callout.Length = -22
          Marks.Margins.Left = 21
          Marks.Margins.Top = 9
          Marks.Margins.Right = 35
          Marks.ShapeStyle = fosRoundRectangle
          Marks.Transparent = True
          Marks.Visible = True
          Pointer.InflateMargins = True
          Pointer.Pen.Visible = False
          Pointer.Style = psCircle
          Pointer.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
          Data = {
            010300000000000000000000000000000080627D400000000000000000000000
            00008F7B4000000000000000000000000000004C40}
        end
        object Series_decart_radius: TLineSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          LinePen.Color = clRed
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series_decart_30: TLineSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          SeriesColor = clGray
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series_decart_45: TLineSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          SeriesColor = clGray
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series_decart_6: TLineSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          SeriesColor = clGray
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
    end
  end
  object ds_Data: TDataSource
    DataSet = dxMemData1
    Left = 64
    Top = 5
  end
  object PopupMenu1: TPopupMenu
    Left = 400
    Top = 8
    object N1: TMenuItem
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    end
  end
  object ActionManager1: TActionManager
    ActionBars = <
      item
        Items = <
          item
            Action = act_SaveToDB_
          end>
        ActionBar = ActionMainMenuBar1
      end>
    OnUpdate = ActionManager1Update
    Left = 235
    Top = 7
    StyleName = 'XP Style'
    object act_SaveToDB_: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      OnExecute = act_SaveToDB_Execute
    end
  end
  object dxMemData1: TdxMemData
    Indexes = <>
    SortOptions = []
    AfterPost = mem_Items111AfterPost
    Left = 24
    Top = 8
    object dxMemData1angle: TFloatField
      FieldName = 'angle'
    end
    object dxMemData1loss: TFloatField
      FieldName = 'loss'
    end
  end
end
