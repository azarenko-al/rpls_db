unit fr_AntType_masks;

interface

uses
  Windows, Messages, StdCtrls, Controls, ComCtrls, ToolWin, Classes, ExtCtrls,  Forms,

  dm_Main,   

  dm_AntType,

  fr_Antenna_mask,

  u_const_db,

  u_vars,

  u_reg,
  u_db,
  u_func,
  u_const
  ;

type
  Tframe_AntType_masks = class(TForm )
    pn_Horz: TPanel;
    pn_Vert: TPanel;
    pn_Type: TPanel;
    Panel2: TPanel;
    RadioGroup_Polar_or_Decart: TRadioGroup;
    GroupBox3: TGroupBox;
    cb_Show_tables: TCheckBox;
    cb_Show_6_db: TCheckBox;
    rg_Scale: TRadioGroup;
    procedure cb_ShowGridsClick(Sender: TObject);
    procedure cb_Show_3_dbClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    FID: integer;
    FRegPath: string;

    Ffrm_Mask_ver: Tframe_Antenna_mask;
    Ffrm_Mask_hor: Tframe_Antenna_mask;

    procedure ShowGrid(aValue: boolean);
    procedure UpdateForm(aForm: Tframe_Antenna_mask);
  public
    // : Boolean;

    procedure SetReadOnly(Value: Boolean);

    procedure View (aID: integer);

  end;


//====================================================================
// implementation
//====================================================================
implementation  {$R *.DFM}


//-------------------------------------------------------------------
procedure Tframe_AntType_masks.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  FRegPath:=vars_Form_GetRegPath(ClassName);

  FRegPath:=REGISTRY_COMMON_FORMS+ClassName+'\';

  pn_Vert.Align:=alCLient;

  CreateChildForm(Tframe_Antenna_mask, Ffrm_Mask_ver, pn_Vert);
  CreateChildForm(Tframe_Antenna_mask, Ffrm_Mask_hor, pn_horz);



//  cb_ShowGrids.Checked:=reg_ReadBool(FRegPath, cb_ShowGrids.Name, False);

  cb_ShowGridsClick(nil);
//  ShowGrid(False);
end;


procedure Tframe_AntType_masks.FormDestroy(Sender: TObject);
begin
 // reg_WriteBool(FRegPath, cb_ShowGrids.Name, cb_ShowGrids.Checked);

  inherited;
end;

procedure Tframe_AntType_masks.cb_ShowGridsClick(Sender: TObject);
begin
//  ShowGrid(cb_ShowGrids.Checked);
end;


//Tframe_Antenna_mask;

// ---------------------------------------------------------------
procedure Tframe_AntType_masks.UpdateForm(aForm: Tframe_Antenna_mask);
// ---------------------------------------------------------------
begin
//
  aForm.rg_Scale.ItemIndex:=rg_Scale.ItemIndex;
  aForm.RadioGroup_Polar_or_Decart.ItemIndex:=RadioGroup_Polar_or_Decart.ItemIndex;

  aForm.cb_Show_tables.Checked:=cb_Show_tables.Checked;
  aForm.cb_Show_6_db.Checked:=cb_Show_6_db.Checked;


  // aForm.Chart_polar.Axes.Left.Logarithmic:=cb_Show_Log.Checked;
  // aForm.Chart_polar.Axes.Top.Logarithmic:=cb_Show_Log.Checked;   
  // aForm.Chart_polar.Axes.Right.Logarithmic:=cb_Show_Log.Checked;
  // aForm.Chart_polar.Axes.Bottom.Logarithmic:=cb_Show_Log.Checked;

  aForm.pn_Grid.Visible:=cb_Show_tables.Checked;;

 // aForm.Series_Radius.Visible:=cb_Show_radius.Checked;
//                     asdas asdasdas

//  if RadioGroup_Polar_or_Decart.ItemIndex=0 then
  aForm.PageControl1.ActivePageIndex:=RadioGroup_Polar_or_Decart.ItemIndex;

  aForm.PaintDiagram2;

end;




procedure Tframe_AntType_masks.cb_Show_3_dbClick(Sender: TObject);
begin
  UpdateForm (Ffrm_Mask_hor);
  UpdateForm (Ffrm_Mask_ver);

end;


procedure Tframe_AntType_masks.FormResize(Sender: TObject);
begin
  pn_Horz.Width:=ClientWidth div 2;
end;

procedure Tframe_AntType_masks.SetReadOnly(Value: Boolean);
begin
  Ffrm_Mask_ver.SetReadOnly(Value);
  Ffrm_Mask_hor.SetReadOnly(Value);

end;


//--------------------------------------------------------------------
procedure Tframe_AntType_masks.View (aID: integer);
//--------------------------------------------------------------------
var  rec: TdmAntTypeInfoRec;
begin
  FID:=aID;

  Ffrm_Mask_ver.View (FID, DEF_MASK_VER);
  Ffrm_Mask_hor.View (FID, DEF_MASK_HOR);


  if dmAntType.GetInfoRec11 (aID, rec) then
  begin
    Ffrm_Mask_hor.LoadMaskFromString(rec.HorzMask, rec.Gain_dB);
    Ffrm_Mask_ver.LoadMaskFromString(rec.VertMask, rec.Gain_dB);

  end else
  begin
    Ffrm_Mask_hor.Clear;
    Ffrm_Mask_ver.Clear;

  end;

end;


procedure Tframe_AntType_masks.ShowGrid(aValue: boolean);
begin
  Ffrm_Mask_ver.ShowGrid(aValue);
  Ffrm_Mask_hor.ShowGrid(aValue);

end;


end.


