unit dm_AntType_add;

interface

uses
  SysUtils, Forms, Classes, DB, ADODB,

  dm_Onega_DB_data,

  u_const_db,

  u_func,
  u_DB;


type

  //-------------------------------------------------------------------
  TdmAntTypeAddRec = record
  //-------------------------------------------------------------------
    NewName           : string;
    Folder_ID         : integer;

    Band              : string;

    Gain_dB           : double;
    Freq_MHz          : double;
    Diameter          : double;

    TiltType          : integer;
    ElectricalTilt    : double;

//    FrontToBackRatio  : double;

    Polarization_str  : string;  //   0: Result:= 'H'; 1: Result:= 'V';  2: Result:= 'X';

  //  Polarization_ratio: double;

    Horz_Width         : double;
    Vert_Width         : double;

    Vert_Mask          : string;
    Horz_Mask          : string;

    FileName : string;

    Comment           : string;

  end;


  TdmAntType_add = class(TDataModule)
  private
  public
    function Add(aRec: TdmAntTypeAddRec): Integer;

    procedure GetFromDataset(aDataset: TDataSet; aRec: TdmAntTypeAddRec);

  end;

function dmAntType_add: TdmAntType_add;


implementation
{$R *.dfm}

var
  FdmAntType_add: TdmAntType_add;
        

// ---------------------------------------------------------------
function dmAntType_add: TdmAntType_add;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmAntType_add) then
    FdmAntType_add := TdmAntType_add.Create(Application);

  Result:=FdmAntType_add;
end;


//--------------------------------------------------------------------
function TdmAntType_add.Add(aRec: TdmAntTypeAddRec): Integer;
//--------------------------------------------------------------------
begin        
  with aRec do
    Result:=dmOnega_DB_data.ExecStoredProc_ (sp_ANTENNATYPE_add,
        [FLD_NAME,             NewName,
         FLD_FOLDER_ID,        IIF_NULL(Folder_ID),

         FLD_BAND,             IIF_NULL(BAND),

         FLD_GAIN,             Gain_dB,
         FLD_FREQ_MHZ,         Freq_MHz,
         FLD_DIAMETER,         TruncFloat(Diameter,2),

         FLD_Tilt_Type,        TiltType,
         FLD_Electrical_Tilt,  ElectricalTilt,

         FLD_Polarization_str, Polarization_str,

         FLD_VERT_WIDTH,       TruncFloat(Vert_Width,2),
         FLD_HORZ_WIDTH,       TruncFloat(Horz_Width,2),

         FLD_vert_mask,        IIF_NULL(Vert_Mask),
         FLD_horz_mask,        IIF_NULL(Horz_Mask),

         FLD_Comment,          IIF_NULL(Comment)

        ] );

end;



//--------------------------------------------------------------------
procedure TdmAntType_add.GetFromDataset(aDataset: TDataSet; aRec:
    TdmAntTypeAddRec);
//--------------------------------------------------------------------
begin

  with aRec, aDataset do
  begin
    NewName          := FieldBYName(FLD_NAME).AsString;

    BAND             := FieldBYName(FLD_BAND).AsString;

    Gain_dB          := FieldBYName(FLD_GAIN).AsFloat;
    Freq_MHz         := FieldBYName(FLD_FREQ_MHZ).AsFloat;
    Diameter         := FieldBYName(FLD_DIAMETER).AsFloat;

    TiltType         := FieldBYName(FLD_Tilt_Type).AsInteger;
    ElectricalTilt   := FieldBYName(FLD_Electrical_Tilt).AsFloat;

    Polarization_str := FieldBYName(FLD_Polarization_str).AsString;

    Vert_Width       := FieldBYName(FLD_VERT_WIDTH).AsFloat;
    Horz_Width       := FieldBYName(FLD_HORZ_WIDTH).AsFloat;

    Vert_Mask        := FieldBYName(FLD_vert_mask).AsString;
    Horz_Mask        := FieldBYName(FLD_horz_mask).AsString;

    Comment          :=FieldBYName(FLD_Comment).AsString;

  end;


end;



end.


(*
  with aRec,aDataset do
//    Result:=dmOnega_DB_data.ExecStoredProc (sp_ANTENNATYPE_add,
        [

                       db_Par(FLD_NAME,             NewName),
                       db_Par(FLD_FOLDER_ID,        IIF_NULL(Folder_ID)),

                       db_Par(FLD_BAND,             IIF_NULL(BAND)),

                       db_Par(FLD_GAIN,             Gain_dB),
                       db_Par(FLD_FREQ_MHZ,         Freq_MHz),
                       db_Par(FLD_DIAMETER,         TruncFloat(Diameter,2)),

                       db_Par(FLD_Tilt_Type,        TiltType),
                       db_Par(FLD_Electrical_Tilt,  ElectricalTilt),

                       db_Par(FLD_Polarization_str, Polarization_str),

                       db_Par(FLD_VERT_WIDTH,       TruncFloat(Vert_Width,2)),
                       db_Par(FLD_HORZ_WIDTH,       TruncFloat(Horz_Width,2)),

                       db_Par(FLD_vert_mask,        IIF_NULL(Vert_Mask)),
                       db_Par(FLD_horz_mask,        IIF_NULL(Horz_Mask)),

                       db_Par(FLD_Comment,          IIF_NULL(Comment))
                       

*)

