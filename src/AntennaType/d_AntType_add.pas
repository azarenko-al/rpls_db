unit d_AntType_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rxPlacemnt, ActnList,     StdCtrls, ExtCtrls, 
  cxPropertiesStore, cxVGrid, cxControls,  cxInplaceContainer, cxDropDownEdit,

  d_Wizard_add_with_Inspector,

  dm_Library,

  u_func,
  //  
  u_cx_vgrid,

  u_const,
  u_const_db,
  u_const_str,

  dm_AntType_add,
  dm_AntType,

  u_cx_VGrid_manager, ComCtrls;


type
  Tdlg_AntType_add = class(Tdlg_Wizard_add_with_Inspector)
    cxVerticalGrid1: TcxVerticalGrid;
    row_Band: TcxEditorRow;
    row_Gain_dBi: TcxEditorRow;
    row_Freq_MHz: TcxEditorRow;
    cxVerticalGrid1EditorRow4: TcxEditorRow;
    row_Diameter: TcxEditorRow;
    row_BeamWidth: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    row_Comment1: TcxEditorRow;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
//    procedure row_FileNameButtonClick(Sender: TObject;
  //    AbsoluteIndex: Integer);
  private
    FID: integer;

    FRec: TdmAntTypeAddRec;

    row_Freq_MHz1: TcxEditorRow;
    row_Gain_dBi1: TcxEditorRow;
    row_Band1: TcxEditorRow;


    FVGridManager: TcxVGridManager;

    procedure Append;

  public
  //  destructor Destroy; override;
    class function ExecDlg (aFolderID: integer =0): integer;

  end;


implementation
{$R *.DFM}


//--------------------------------------------------------------------
class function Tdlg_AntType_add.ExecDlg;
//--------------------------------------------------------------------
begin
  with Tdlg_AntType_add.Create(Application) do
  begin                                         
    FRec.Folder_ID:=aFolderID;
    ed_Name_.Text:=dmAntType.GetNewName ();

    ShowModal;
    Result:=FID;

    Free;
  end;
end;


//--------------------------------------------------------------------
procedure Tdlg_AntType_add.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
 // Assert(Assigned(dmAntType));


  inherited;
  Caption:=DLG_CAPTION_OBJECT_CREATE;
                       
//  lb_File.Caption:=STR_IMPORT_FILENAME;

  SetActionName(STR_DLG_ADD_ANTENNA_TYPE);


  row_Gain_dBi.Properties.Caption:=STR_GAIN;
  row_Freq_MHz.Properties.Caption:=STR_FREQ_MHz;

  row_BeamWidth.Properties.Caption:='������ ��� (3 dB Beam width)';
  row_Diameter.Properties.Caption:='������� [m]';

//  row_Comment.Caption:=STR_COMMENT;

  cx_InitVerticalGrid (cxVerticalGrid1);

  SetDefaultSize();
                                      
  TcxComboBoxProperties(
    row_Band.Properties.EditProperties).Items.Assign (dmLIbrary.Bands);


  FVGridManager := TcxVGridManager.Create();
  FVGridManager.Init (cxVerticalGrid1);


end;


procedure Tdlg_AntType_add.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FVGridManager);

  inherited;
end;


//--------------------------------------------------------------------
procedure Tdlg_AntType_add.act_OkExecute(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
  Append;
end;


//--------------------------------------------------------------------
procedure Tdlg_AntType_add.Append;
//--------------------------------------------------------------------
begin
  FRec.NewName:=ed_Name_.Text;
  FRec.Gain_dB:=AsFloat(row_Gain_dBi.Properties.Value);
  FRec.Freq_MHz:=AsFloat(row_Freq_MHz.Properties.Value);
  FRec.Comment :=AsString(row_Comment1.Properties.Value);
  FRec.Band    :=AsString(row_Band.Properties.Value);
  FRec.Diameter:=AsFloat(row_Diameter.Properties.Value);


  FRec.Horz_Width:=AsFloat(row_BeamWidth.Properties.Value);
  FRec.Vert_Width:=AsFloat(row_BeamWidth.Properties.Value);



//  FRec.FolderID:=FFolderID;

  { TODO : dmAntType }
  FID:=dmAntType_add.Add (FRec);

 // dmAntType_Import.LoadFromFile (FID, row_FileName.Text);
end;



end.


{
procedure Tdlg_AntType_add.row_FileNameButtonClick(Sender: TObject; AbsoluteIndex: Integer);
begin
 // dx_BrowseFile (Sender, FILTER_MSI);
end;