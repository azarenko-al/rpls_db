unit a_Unit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,

  fr_Browser,

  d_AntType_add,

  dm_Act_Explorer,
  fr_Explorer,
  dm_Explorer,

  dm_Main,
  dm_Main,

//  dm_Act_Project,
  u_func,
//  u_func_app,
  u_db,
  u_reg,

  f_Log,


  u_const,
  u_const_msg,
  u_const_db,
  u_dlg,
  u_Types,

  fr_AntType_view,

//  f_Explorer,

  ComCtrls, rxPlacemnt, ToolWin, ExtCtrls;

type
  Tfrm_test_AntType = class(TForm)
    Panel1: TPanel;
    Splitter1: TSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    pn_Browser: TPanel;
    FormStorage1: TFormStorage;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    Fframe_Explorer : Tframe_Explorer;

    Fframe_Browser : Tframe_Browser;
  public
    { Public declarations }
  end;

var
  frm_test_AntType: Tfrm_test_AntType;

//===================================================
implementation  {$R *.DFM}
//===================================================

//------------------------------------------------------------------
procedure Tfrm_test_AntType.FormCreate(Sender: TObject);
//------------------------------------------------------------------
begin
 // gl_reg:=TRegStore_Create(REGISTRY_ROOT);

 // Tfrm_Log.CreateForm();

  TdmMain.Init;
  dmMain.OpenDlg;


{
  TdmAct_Explorer.Init;

  CreateChildForm(Tframe_Explorer, Fframe_Explorer, TabSheet1);
//  Fframe_Explorer.SetViewObjects([otAntennaType]);
  Fframe_Explorer.SetViewObjects([otAntennaType]);
  Fframe_Explorer.RegPath:='ant';
  Fframe_Explorer.Load;

  CreateChildForm(Tframe_Browser, Fframe_Browser, pn_Browser);
  Fframe_Explorer.OnNodeFocused:=Fframe_Browser.ViewObjectByPIDL;
}


   Tdlg_AntType_add.ExecDlg;


//  dmAct_Project.LoadLastProject;

//  PostMsg (WM_SHOW_FORM_MAP);
end;


procedure Tfrm_test_AntType.FormDestroy(Sender: TObject);
begin
 // Fframe_Explorer.Free;
//  Fframe_Browser.Free;

  inherited;
end;


end.

{

  CreateChildForm (Tframe_Explorer_Container, Fframe_Explorer, TabSheet1);
//  Fframe_Explorer := Tframe_Explorer_Container.CreateChildForm (TabSheet1);
  Fframe_Explorer.SetViewObjects([otProperty, otLink, otLinkEnd, otGeoRegion]);
  Fframe_Explorer.RegPath:='Property';
  Fframe_Explorer.Load;


  CreateChildForm(Tframe_Browser,Fframe_Browser,pn_Browser);
 // Fframe_Browser := Tframe_Browser.CreateChildForm(pn_Browser);
  Fframe_Explorer.OnNodeFocused:=Fframe_Browser.ViewObjectByPIDL;

  dmAct_Project.LoadLastProject;


}
