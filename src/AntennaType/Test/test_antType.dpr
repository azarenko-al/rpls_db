program test_antType;

uses
  Forms,
  a_Unit in 'a_Unit.pas' {frm_test_AntType},
  d_AntType_add in '..\d_AntType_add.pas' {dlg_AntType_add},
  dm_act_AntType in '..\dm_Act_AntType.pas' {dmAct_AntType: TDataModule},
  fr_AntType_view in '..\fr_AntType_view.pas' {frame_AntType_View},
  fr_Antenna_mask in '..\fr_Antenna_mask.pas' {frame_Antenna_mask},
  fr_AntType_masks in '..\fr_AntType_masks.pas' {frame_AntType_masks};

{$R *.RES}
 

begin
  Application.Initialize;
  Application.CreateForm(Tfrm_test_AntType, frm_test_AntType);
  Application.Run;
end.
