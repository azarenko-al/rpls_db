unit u_antenna;

interface
uses Classes,SysUtils,ADODB,
  u_func,
  u_files
  ;


type
  TMask  = array of record
                   Angle: double;
                   Loss: double;
                 end;

  //--------------------------------------------------------------------
  TAntenna = class // �������
  //--------------------------------------------------------------------
  private
    function Find_Angle(aAngle: double; aDiagram: TMask): boolean;

//    function Find_Angle(aAngle: double; aDiagram: TMask): boolean;
       // procedure DefineDNAWidth();
  public
    ModelName      : string; // �������� ������
    Producer       : string; // �������� �������������
    Gain           : double; // �������� []
    TiltElectrical : string; // ������ ������������� � ����
    FrontToBackRatio: double;// �������� ���������

    Freq           : double; // �������
    Comment        : string; // ����������

    DiagramHorz,
    DiagramVert: TMask;

    DNAHorWidth,
    DNAVertWidth   : integer; // ������ ���,���� ��������� ��������������

    function IsOMNI : boolean; // ��������������� �������

    // ���������� ������ ��� � ���� ��������� ��������������
    function GetHorzWidth: integer;
    function GetVertWidth: integer;

    function GetHorzLoss(aAngle: integer): double;
    function GetVertLoss(aAngle: integer): double;

    function LoadFromMSIFile (aFileName: string): boolean;
    function SaveToMSIFile (aFileName: string): boolean;

  end;



//==========================================================
implementation
//==========================================================


//---------------------------------------------------------
// Load from MSI file
//---------------------------------------------------------
function TAntenna.LoadFromMSIFile (aFileName: string): boolean;
//---------------------------------------------------------
var strList: TStringList;
    strArr: TStrArray;
    str,str1,str2,str3: string;
    angle,value: double;
    i,total: integer;
    k,m: integer;
    dAngle: double;
    state: (HEADER_PART,HORIZONTAL_PART,VERTICAL_PART);

begin
  Result:=false;

  SetLength(DiagramVert,0);
  SetLength(DiagramHorz,0);

  if not FileExists(aFileName) then Exit;

  strList:=TStringList.Create;
  Result:=false;

  try
    strList.LoadFromFile (AFileName);
  except
  end;

  state:=HEADER_PART;

  // �� ��������� ��������� ��� ������� ������ ������ �����
  if strList.Count>0 then
    Modelname:=strList[0];


  k:=0; m:=0;

  for i:=0 to strList.Count-1 do
  begin
    // �������� ���������� �� �������
    strList[i]:=ReplaceStr (strList[i], #9, ' ');

    strArr:=StringToStrArray (strList[i],' ');
    total:=High(strArr)+1; str1:=''; str2:=''; str3:='';
    if total>=1 then str1:=strArr[0];
    if total>=2 then str2:=strArr[1];
    if total>=3 then str3:=strArr[2];

    if Eq(str1,'NAME')       then Modelname:=str2 + str3     else
    if Eq(str1,'FREQUENCY')  then Freq:=AsFloat(str2) else
    if Eq(str1,'GAIN')       then begin
                                 Gain:=AsFloat(str2);
                                 //���� ������� ��������� dBd -> ��������� 3.5
                                 if Pos('dbd',LowerCase(strList[i]))>0 then
                                   Gain:=Gain + 2.15;
                             end else
    if Eq(str1,'TILT')       then TiltElectrical:=str2    else
    if Eq(str1,'COMMENT')    then Comment:=str2+str3 else
    if Eq(str1,'HORIZONTAL') then state:=HORIZONTAL_PART         else
    if Eq(str1,'VERTICAL')   then state:=VERTICAL_PART
    else

    if state in [HORIZONTAL_PART,VERTICAL_PART] then
    begin
      dAngle:=AsFloat(str1);
      value :=AsFloat(str2);
      if (dAngle>=0) and (dAngle<360) then
        if state=HORIZONTAL_PART then
        begin
          SetLength(DiagramHorz, Length(DiagramHorz)+1);

          DiagramHorz[k].Loss:=value;
          DiagramHorz[k].Angle:=dAngle;
          k:=k+1;
        end else

        if state=VERTICAL_PART then
        begin
          SetLength(DiagramVert, Length(DiagramVert)+1);

          DiagramVert[m].Loss:=value;
          DiagramVert[m].Angle:=dAngle;
          m:=m+1;
        end;
    end;
  end;

//  DefineDNAWidth(); // ������ ��� � ������������ ������ ��� �������

  Result:=true; // Everything's OK

  strList.Free;

end;


//---------------------------------------------------------
function TAntenna.SaveToMSIFile (aFileName: string): boolean;
//---------------------------------------------------------
var oStrList: TStringList;
    i: integer;
begin
  oStrList:=TStringList.Create;
  Result:=false;

  with oStrList do begin
    Add ( Format('NAME %s',      [Modelname]));
    Add ( Format('FREQUENCY %1.0f', [Freq]));
    Add ( Format('GAIN %1.0f',      [Gain]));
    Add ( Format('TILT %s',      [TiltElectrical]));
    Add ( Format('COMMENT %s',   [Comment]));

    Add ( Format('HORIZONTAL %d',   [Length(DiagramHorz)]));
    for i:=0 to High(DiagramHorz) do
      Add(Format('%1.1f %f', [DiagramHorz[i].Angle, DiagramHorz[i].Loss]));

    Add ( Format('VERTICAL %d',   [Length(DiagramVert)]));
    for i:=0 to High(DiagramVert) do
      Add(Format('%1.1f %f', [DiagramVert[i].Angle, DiagramVert[i].Loss]));
  end;

  try
    oStrList.SaveToFile (aFileName);
    Result:=true; // Everything's OK
  finally
    oStrList.Free;
  end;
end;


//--------------------------------------------------------------------
function TAntenna.IsOMNI(): boolean;
//--------------------------------------------------------------------
var i:integer;
begin
  Result:= True;

  for i:=0 to High(DiagramHorz) do
    if DiagramHorz[i].Loss>3 then begin Result:= False; Break; end;
end;


//--------------------------------------------------------------------
function TAntenna.GetHorzWidth: integer;
//--------------------------------------------------------------------
var i,ind,iNulAngleInd:integer;
   isOMNI: boolean;
begin
  Result:=0;

  iNulAngleInd:=-1;
  for i:=0 to High(DiagramHorz) do     
    if DiagramHorz[i].Loss=0 then begin iNulAngleInd:=i; Break; end;

  if iNulAngleInd<0 then Exit;

  //def OMNI
  isOMNI:=True;
  for i:=0 to High(DiagramHorz) do
    if (DiagramHorz[i].Loss > 3) then begin isOMNI:=False; Break; end;
  if isOMNI then begin
    Result:=360;
    Exit;
  end;


  for i:=0 to High(DiagramHorz) do begin
    ind:=(iNulAngleInd + i) mod Length(DiagramHorz);
    if (DiagramHorz[ind].Loss < 3) then Inc(Result) else Break;
  end;

  for i:=0 to High(DiagramHorz) do begin
    ind:=((iNulAngleInd - i) + Length(DiagramHorz)) mod Length(DiagramHorz);
    if (DiagramHorz[ind].Loss < 3) then Inc(Result) else Break;
  end;

end;

//--------------------------------------------------------------------
function TAntenna.GetVertWidth: integer;
//--------------------------------------------------------------------
var i,ind,iNulAngleInd:integer;
   isOMNI: boolean;
begin
  Result:=0;

  //def OMNI
  isOMNI:=True;
  for i:=0 to High(DiagramVert) do
    if (DiagramVert[i].Loss > 3) then begin isOMNI:=False; Break; end;
  if isOMNI then begin
    Result:=360;
    Exit;
  end;


  iNulAngleInd:=-1;
  for i:=0 to High(DiagramVert) do     
    if DiagramVert[i].Loss=0 then begin iNulAngleInd:=i; Break; end;

  if iNulAngleInd<0 then Exit;

  for i:=0 to High(DiagramVert) do begin
    ind:=(iNulAngleInd + i) mod Length(DiagramVert);
    if (DiagramVert[ind].Loss < 3) then Inc(Result) else Break;
  end;

  for i:=0 to High(DiagramVert) do begin
    ind:=((iNulAngleInd - i) + Length(DiagramVert)) mod Length(DiagramVert);
    if (DiagramVert[ind].Loss < 3) then Inc(Result) else Break;
  end;


end;

//--------------------------------------------------------------------
function TAntenna.GetHorzLoss(aAngle: integer): double;
//--------------------------------------------------------------------
var i:integer;
begin
  Result:=0;

  for i:=0 to High(DiagramHorz) do
    if (DiagramHorz[i].Angle=aAngle) then begin Result:=DiagramHorz[i].Loss; Exit; end;

  for i:=0 to High(DiagramHorz)-1 do
    if ((DiagramHorz[i].Angle>aAngle) and (DiagramHorz[i+1].Angle<aAngle)) then
      begin Result:=Trunc(DiagramHorz[i].Angle); Exit; end;
end;

// -------------------------------------------------------------------
function TAntenna.GetVertLoss(aAngle: integer): double;
// -------------------------------------------------------------------
var i:integer;
begin
  Result:=0;

  for i:=0 to High(DiagramVert) do
    if (DiagramVert[i].Angle=aAngle) then begin Result:=DiagramVert[i].Loss; Exit; end;

  for i:=0 to High(DiagramHorz)-1 do
    if ((DiagramVert[i].Angle>aAngle) and (DiagramVert[i+1].Angle<aAngle)) then
      begin Result:=Trunc(DiagramVert[i].Angle); Exit; end;
end;



//--------------------------------------------------------------------
function TAntenna.Get_Polarization_Ratio: double;
//--------------------------------------------------------------------
var i:integer; dLoss0: double;
begin
  Result:= 0;

  for i:=0 to High(DiagramHorz_H) do
    if DiagramHorz_H[i].Angle = 0 then
      begin dLoss0:= DiagramHorz_H[i].Loss; Break; end;

  for i:=0 to High(DiagramVert_H) do
    if DiagramVert_H[i].Angle = 0 then
      begin Result:= Abs(DiagramVert_H[i].Loss-dLoss0); Break; end;
end;


//---------------------------------------------------------
// Load from ADF file
//---------------------------------------------------------
function TAntenna.LoadFromADFFile (aFileName: string): boolean;
//---------------------------------------------------------
var strList: TStringList;
    strArr: TStrArray;
    str,str1,str2,str3, sFileName: string;
    dAngle,value: double;
    iAngle,i,total: integer;
    k,m,n,p: integer;
    state: (HEADER_PART,HORIZONTAL_PART_H,VERTICAL_PART_H,
                        HORIZONTAL_PART_V,VERTICAL_PART_V);
//    DiagramHorz_H, DiagramVert_H,
//    DiagramHorz_V, DiagramVert_V: TMask;
begin
  SetLength(DiagramVert_H, 0);
  SetLength(DiagramHorz_H, 0);
  SetLength(DiagramVert_V, 0);
  SetLength(DiagramHorz_V, 0);

  Result:=false;

  if not FileExists(aFileName) then Exit;

  strList:=TStringList.Create;
  Result:=false;

  try
    strList.LoadFromFile (AFileName);
  except
  end;

  state:=HEADER_PART;

  k:=0; m:=0; n:=0; p:=0;

  for i:=0 to strList.Count-1 do
  begin
    // �������� ���������� �� �������
    strList[i]:=ReplaceStr (strList[i], #9, ' ');
    strList[i]:=ReplaceStr (strList[i], ',', ' ');

    strArr:=StringToStrArray (strList[i],' ');
    total:=High(strArr)+1; str1:=''; str2:=''; str3:='';
    if total>=1 then str1:=strArr[0];
    if total>=2 then str2:=strArr[1];
    if total>=3 then str3:=strArr[2];

    if Eq(str1,'MODNUM:')     then ModelName := str2 + str3     else
    if Eq(str1,'LOWFRQ:')     then Freq := AsFloat(str2) else
    if Eq(str1,'HGHFRQ:')     then Freq := (AsFloat(str2)+Freq)/2 else
    if Eq(str1,'MDGAIN:')     then Gain := AsFloat(str2) else
    if Eq(str1,'ELTILT:')     then TiltElectrical  :=str2    else
    if Eq(str1,'FRTOBA:')     then FrontToBackRatio:= AsFloat(str2) else
    if Eq(str1,'AZWIDT:')     then DNAHorzWidth := AsFloat(str2) else
    if Eq(str1,'ELWIDT:')     then DNAVertWidth := AsFloat(str2) else

    if (Eq(str1,'POLARI:')) and (Eq(str2,'H/H'))
                             then state:=HORIZONTAL_PART_H   else
    if (Eq(str1,'POLARI:')) and (Eq(str2,'H/V'))
                             then state:=VERTICAL_PART_H     else
    if (Eq(str1,'POLARI:')) and (Eq(str2,'V/H'))
                             then state:=HORIZONTAL_PART_V   else
    if (Eq(str1,'POLARI:')) and (Eq(str2,'V/V'))
                             then state:=VERTICAL_PART_V     else

    if Eq(str1,'PATCUT:')     then Continue  else
    if Eq(str1,'NUPOIN:')     then Continue  else
    if Eq(str1,'ENDFIL:')     then Break  else

    if state in [HORIZONTAL_PART_H,VERTICAL_PART_H,
                 HORIZONTAL_PART_V,VERTICAL_PART_V] then
    begin
      dAngle:=AsFloat(str1);
      if (dAngle<0) and (dAngle>=-180) then dAngle:= 360 + dAngle;
      value :=Abs(AsFloat(str2));
      if (dAngle>=0) and (dAngle<360) then
        if state=HORIZONTAL_PART_H then
          if Find_Angle(dAngle, DiagramHorz_H) then Continue else
        begin
          SetLength(DiagramHorz_H, Length(DiagramHorz_H)+1);
          DiagramHorz_H[k].Loss:=value;
          DiagramHorz_H[k].Angle:=dAngle;
          k:=k+1;
        end else

        if state=VERTICAL_PART_H then
          if Find_Angle(dAngle, DiagramVert_H) then Continue else
        begin
          SetLength(DiagramVert_H, Length(DiagramVert_H)+1);
          DiagramVert_H[m].Loss:=value;
          DiagramVert_H[m].Angle:=dAngle;
          m:=m+1;
        end;

        if state=HORIZONTAL_PART_V then
          if Find_Angle(dAngle, DiagramHorz_V) then Continue else
        begin
          SetLength(DiagramHorz_V, Length(DiagramHorz_V)+1);
          DiagramHorz_V[n].Loss:=value;
          DiagramHorz_V[n].Angle:=dAngle;
          n:=n+1;
        end else

        if state=VERTICAL_PART_V then
          if Find_Angle(dAngle, DiagramVert_V) then Continue else
        begin
          SetLength(DiagramVert_V, Length(DiagramVert_V)+1);
          DiagramVert_V[p].Loss:=value;
          DiagramVert_V[p].Angle:=dAngle;
          p:=p+1;
        end;
    end;
  end;

  Result:=true; // Everything's OK

  strList.Free;
end;


//--------------------------------------------------------------------------
function TAntenna.Find_Angle (aAngle: double; aDiagram: TMask): boolean;
//--------------------------------------------------------------------------
var  I: Integer;
begin
  Result:= False;
  for i:=0 to High(aDiagram) do
    if aDiagram[i].Angle = aAngle then
    begin
      Result:= True;
      Exit;
    end;
end;    //


{ TAntImport }

var
  obj: TAntenna;
  i: integer;
begin
{
  obj:=TAntenna.Create;

  obj.LoadFromMSIFile ('u:\727727X2.MSI');
  obj.SaveToMSIFile ('u:\727727X2_.MSI');

  i:=obj.GetVertWidth;
  i:=obj.GetHorzWidth;

  i:=0;
 }


end.

