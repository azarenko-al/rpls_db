unit u_AntType_classes;

interface
uses
  SysUtils,

    u_Func_arrays,
  u_func
  ;


type
  TAntennaMask = class
  private
    FMask: array of record
             Angle: double;
             Loss: double;
           end;
  public
    function GetLossByAngle(aAngle: double): double;
    function GetLossesStr: string;
    procedure StrToArr(aValue: string);

  end;

implementation

// ---------------------------------------------------------------
function TAntennaMask.GetLossByAngle(aAngle: double): double;
// ---------------------------------------------------------------
var i: Integer;
begin
  Result := 0;

  for i := 0 to High(FMask) do
    if (aAngle <= FMask[i].Angle) then
    begin
      Result := FMask[i].Loss;
      Exit;
    end;
end;

// ---------------------------------------------------------------
function TAntennaMask.GetLossesStr: string;
// ---------------------------------------------------------------
var i: Integer;
begin
  Result := '';

  for i := 0 to High(FMask) do
  begin
    Result := Result + Format('%1.1f=%1.1f', [FMask[i].Angle, FMask[i].Loss]);
             // + IIF(i< High(FMask),';','');

    if i< High(FMask) then
      Result := Result + ';';
      
//   ,';','');

  end;

end;

// ---------------------------------------------------------------
procedure TAntennaMask.StrToArr(aValue: string);
// ---------------------------------------------------------------
var
  strArr1,strArr2: TStrArray;
  i: Integer;
begin
  strArr1:=StringToStrArray (aValue,';');

  SetLength (FMask, Length(strArr1));

  for i := 0 to High(strArr1) do
  begin
    strArr2:=StringToStrArray (strArr1[i],'=');

    if Length(strArr2)=2 then
    begin
      FMask[i].Angle:=AsFloat(strArr2[0]);
      FMask[i].Loss :=AsFloat(strArr2[1]);
    end;
  end;

 // i:=0;
end;

end.
