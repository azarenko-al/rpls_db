object frame_AntType_masks: Tframe_AntType_masks
  Left = 828
  Top = 350
  Width = 866
  Height = 497
  Align = alClient
  Caption = 'frame_AntType_masks'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object pn_Horz: TPanel
    Left = 0
    Top = 0
    Width = 201
    Height = 393
    Align = alLeft
    BevelOuter = bvLowered
    Caption = 'pn_Horz'
    TabOrder = 0
  end
  object pn_Vert: TPanel
    Left = 647
    Top = 0
    Width = 211
    Height = 393
    Align = alRight
    BevelOuter = bvLowered
    Caption = 'pn_Vert'
    TabOrder = 1
  end
  object pn_Type: TPanel
    Left = 201
    Top = 0
    Width = 2
    Height = 393
    Align = alLeft
    Alignment = taLeftJustify
    BevelOuter = bvLowered
    Color = clAppWorkSpace
    TabOrder = 2
  end
  object Panel2: TPanel
    Left = 0
    Top = 393
    Width = 858
    Height = 77
    Align = alBottom
    BevelOuter = bvNone
    BorderWidth = 4
    TabOrder = 3
    object RadioGroup_Polar_or_Decart: TRadioGroup
      Left = 4
      Top = 4
      Width = 113
      Height = 69
      Align = alLeft
      Caption = #1057#1050
      ItemIndex = 0
      Items.Strings = (
        #1055#1086#1083#1103#1088#1085#1072#1103
        #1044#1077#1082#1072#1088#1090#1086#1074#1072)
      TabOrder = 0
      OnClick = cb_Show_3_dbClick
    end
    object GroupBox3: TGroupBox
      Left = 249
      Top = 4
      Width = 176
      Height = 69
      Align = alLeft
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100
      TabOrder = 1
      object cb_Show_tables: TCheckBox
        Left = 16
        Top = 16
        Width = 105
        Height = 17
        Caption = #1058#1072#1073#1083#1080#1094#1099
        Checked = True
        State = cbChecked
        TabOrder = 0
        OnClick = cb_Show_3_dbClick
      end
      object cb_Show_6_db: TCheckBox
        Left = 16
        Top = 39
        Width = 153
        Height = 17
        Caption = #1059#1088#1086#1074#1077#1085#1100' '#1087#1086#1090#1077#1088#1100' 6 dB'
        TabOrder = 1
        OnClick = cb_Show_3_dbClick
      end
    end
    object rg_Scale: TRadioGroup
      Left = 117
      Top = 4
      Width = 132
      Height = 69
      Align = alLeft
      Caption = #1043#1088#1072#1092#1080#1082' '#1044#1053#1040
      ItemIndex = 0
      Items.Strings = (
        #1083#1080#1085#1077#1081#1085#1099#1081
        #1083#1086#1075#1072#1088#1080#1092#1084#1080#1095#1077#1089#1082#1080#1081)
      TabOrder = 2
      OnClick = cb_Show_3_dbClick
    end
  end
end
