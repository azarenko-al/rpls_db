unit fr_AntType_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, cxControls, cxSplitter,
  Dialogs, ActnList, Menus, DBCtrls, StdCtrls,  
  cxPropertiesStore, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,

  ComCtrls, ExtCtrls, rxPlacemnt,  ImgList,

  dm_User_Security,

  u_func_msg,

  u_types,
  u_const_db,

  u_func,
  u_db,
  u_reg,

  fr_View_base,

  fr_AntType_masks,
  fr_DBInspector_Container,

  dxSkinsCore, dxSkinsDefaultPainters, dxBar, cxBarEditItem, cxClasses
  ;

type
  Tframe_AntType_View = class(Tframe_View_Base, IMessageHandler)
    act_Import: TAction;
    cxSplitter1: TcxSplitter;
    pn_Inspector: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    procedure FormCreate(Sender: TObject);
//    procedure pn_MainClick(Sender: TObject);

  private
    FID: Integer;

    Ffrm_DBInspector: Tframe_DBInspector_Container;
    Fframe_AntType_masks: Tframe_AntType_masks;

    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList; var aHandled:
        boolean);
  public
    procedure View (aID: integer; aGUID: string); override;
  end;


implementation {$R *.DFM}



procedure Tframe_AntType_View.GetMessage(aMsg: TEventType; aParams:
    TEventParamList; var aHandled: boolean);
begin
  Assert(ID>0);

  if aMsg = et_RefreshData then
    if Eq(aParams.objname , OBJ_ANTENNA_TYPE) then
      if aParams.id = ID then
        Ffrm_DBInspector.View (ID);

{
    g_EventManager.PostEvent_(etRefreshData,
      ['objname', OBJ_ANTENNA_TYPE,
       'id',      FAntTypeID
       ]);
 }

 // ShowMessage('');
end;


//--------------------------------------------------------------------
procedure Tframe_AntType_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
var
  oForm: TForm;

begin
  inherited;
  ObjectName:=OBJ_ANTENNA_TYPE;

  TableName:=TBL_ANTENNATYPE;

  pn_Inspector.Align:=alClient;

  CreateChildForm(Tframe_DBInspector_Container, Ffrm_DBInspector, pn_Inspector);
  Ffrm_DBInspector.PrepareViewForObject(OBJ_ANTENNA_TYPE);

  CreateChildForm(Tframe_AntType_masks, Fframe_AntType_masks, TabSheet1);


  AddComponentProp(PageControl1, PROP_HEIGHT);
  cxPropertiesStore.RestoreFrom;

  pn_Inspector.Constraints.MinHeight:=20;

  cxSplitter1.Realign;

end;



//--------------------------------------------------------------------
procedure Tframe_AntType_View.View (aID: integer; aGUID: string);
//--------------------------------------------------------------------
var
  bReadOnly: Boolean;
begin
  inherited;

  if not RecordExists (aID) then
    Exit;


//  FID:=aID;

  Ffrm_DBInspector.View (aID);
  Fframe_AntType_masks.View (aID);

  bReadOnly:=dmUser_Security.Lib_Edit_ReadOnly();

  Ffrm_DBInspector.SetReadOnly(bReadOnly);
  Fframe_AntType_masks.SetReadOnly(bReadOnly);


   {
 SetActionsEnabled1(
       act_SaveAs,
    //   act_Load,
       act_Export_MDB

   dmUser_Security.Is_Lib_Edit_allow );
   }

end;




end.

(*
{
  IMessageHandler_ = interface
    ['{DB6CC915-A762-4C09-B12A-111D3D2E765C}']
    procedure GetMessage_ (aMsg: integer; aParams: Variant; var aHandled: Boolean);
  end;
