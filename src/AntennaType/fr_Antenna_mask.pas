unit fr_Antenna_mask;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, DB,
  Dialogs, Math, RxMemDS, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  ExtCtrls, ToolWin, ComCtrls, ActnMan, ActnCtrls, ActnMenus, XPStyleActnCtrls,
   TeEngine, TeeTools, TeePageNumTool, Series, TeePolar, TeeProcs, Chart,
  cxControls, cxGridCustomView, cxClasses, cxGridLevel, cxGrid, ActnList, Menus,

  dm_Onega_DB_data,
  dm_AntType,

  u_func,
  u_db,

  u_types,

  u_const_db,
  u_const_str,

  u_func_msg,


  StdCtrls, dxmdaset, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxSpinEdit, dxSkinsCore, dxSkinsDefaultPainters

  ;


type
  TLossItem = class;
  TLossItemList = class;


  Tframe_Antenna_mask = class(TForm)
    pn_Type: TPanel;
    ds_Data: TDataSource;
    ToolBar1: TToolBar;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    pn_Grid: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    col_Angle1: TcxGridDBColumn;
    col_Loss1: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    ActionManager1: TActionManager;
    act_SaveToDB_: TAction;
    ActionMainMenuBar1: TActionMainMenuBar;
    Panel1: TPanel;
    RadioGroup_Polar_or_Decart: TRadioGroup;
    GroupBox1: TGroupBox;
    cb_Show_tables: TCheckBox;
    PageControl1: TPageControl;
    TabSheet_decart: TTabSheet;
    TabSheet_polar: TTabSheet;
    Chart_decart: TChart;
    Series_decart: TLineSeries;
    Series_decart_3: TLineSeries;
    Series_decart_15: TLineSeries;
    Series_decart_labels: TLineSeries;
    Series_decart_radius: TLineSeries;
    Series_decart_30: TLineSeries;
    Chart_polar: TChart;
    Series_ant: TPolarSeries;
    Series_Radius: TPolarSeries;
    Series_3: TPolarSeries;
    Series_6: TPolarSeries;
    Series_15: TPolarSeries;
    Series_30: TPolarSeries;
    Series_labels: TPolarSeries;
    Series_decart_45: TLineSeries;
    Series_45: TPolarSeries;
    rg_Scale: TRadioGroup;
    cb_Show_6_db: TCheckBox;
    Series_decart_6: TLineSeries;
    dxMemData1: TdxMemData;
    dxMemData1angle: TFloatField;
    dxMemData1loss: TFloatField;
    procedure ActionManager1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_SaveToDB_Execute(Sender: TObject);
//    procedure cb_Show_3_dbClick(Sender: TObject);
//    procedure Chart1Resize(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(Sender:
        TcxCustomGridTableView; APrevFocusedRecord, AFocusedRecord:
        TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
//    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure mem_Items111AfterPost(DataSet: TDataSet);

  private
    FDelta : double;
    Losses : TLossItemList;

    FOldMask: string;

    FIsPosted: Boolean;
    FAntTypeID: Integer;

    FMaskKind: Integer;

    FGain   : double; //усиление

    procedure SaveDataSet ();

    procedure Prepare;

    procedure LoadFromArray (var aMaskArr: TdmAntTypeMaskArray; aGain: double);
    procedure Mask_StrToArr(aValue: string; var aMask: TdmAntTypeMaskArray);

    procedure SaveIntoDB;

    procedure ShowLogScale;

    procedure ShowRadius(aAngle: Double);
 
  public
    procedure PaintDiagram2;


    procedure LoadMaskFromString(aMask: string; aGain: double);
    procedure Clear;
    procedure SetReadOnly(Value: Boolean);

    procedure View(aID, aMaskKind: integer);

    procedure ShowGrid (aValue: boolean);

    procedure SetMax (aValue: double);


  end;


  TLossItem = class
    Angle: double;
    Loss : double;
  end;

  TLossItemList = class(TList)
    function GetItem (Index: integer): TLossItem;
    function IndexByAngle (aAngle: double): integer;

    procedure AddItem (aAngle, aLoss: double);
    property Items[Index: integer]: TLossItem read GetItem; default;
  private
  public
    procedure SortData;
  end;


//=============================================================
// implementation
//=============================================================
implementation {$R *.DFM}

const
  BORDER_WIDTH = 10;


//-------------------------------------------------------------
procedure Tframe_Antenna_mask.FormCreate(Sender: TObject);
//-------------------------------------------------------------
begin
//  Chart1.Align:=alClient;
  Chart_polar.Align:=alClient;
  Chart_decart.Align:=alClient;

//  TabSheet_decart.TabVisible:=False;
//  TabSheet_polar.TabVisible:=False;


  PageControl1.Align:=alClient;


  PageControl1.ActivePageIndex:=0;

(*  GSPages1.Align:=alClient;
  GSPages1.TabHidden:=true;
*)
 // GSPages1.ActivePageIndex :=1;


  Losses:=TLossItemList.Create;

 // Panel1.Align:=alCLient;
  cxGrid1.Align:=alCLient;

 // Image1.Align:=alCLient;

  col_Angle1.Caption:=STR_DNA_ANGLE;
  col_Loss1.Caption :=STR_DNA_LOSS;


//  Chart1.Align:=alCLient;


  pn_Type.Height:=20;



  // ---------------------------------------------------------------
  Chart_polar.Axes.Right.Labels:=False;
  Chart_polar.Axes.Top.Labels:=False;
  Chart_polar.Axes.Bottom.Labels:=False;

  Chart_polar.LeftAxis.AutomaticMaximum:=true;
  Chart_polar.LeftAxis.AutomaticMinimum:=False;
  Chart_polar.LeftAxis.Minimum:=0;



  Series_3.Pen.Color:=clGray;
  Series_6.Pen.Color:=clGray;
  Series_15.Pen.Color:=clGray;
  Series_30.Pen.Color:=clGray;
  Series_45.Pen.Color:=clGray;

  Series_radius.Pen.Color:=clRed;

  // ---------------------------------------------------------------

  Chart_decart.Axes.Left.Labels:=False;
  Chart_decart.LeftAxis.AutomaticMaximum:=true;

  Chart_decart.LeftAxis.AutomaticMinimum:=False;
  Chart_decart.LeftAxis.Minimum:=0;



  Series_decart_3.Pen.Color:=clGray;
  Series_decart_6.Pen.Color:=clGray;
  Series_decart_15.Pen.Color:=clGray;
  Series_decart_30.Pen.Color:=clGray;
  Series_decart_45.Pen.Color:=clGray;

  Series_decart_radius.Pen.Color:=clRed;

  // ---------------------------------------------------------------
  Series_radius.Visible:=True;
  Series_decart_radius.Visible:=True;

  Series_3.Visible:=True;
  Series_decart_3.Visible:=True;


end;


//-------------------------------------------------------------
procedure Tframe_Antenna_mask.SetMax (aValue: double);
//-------------------------------------------------------------
begin
  Chart_polar.Axes.Left.AutomaticMaximum:=False;
  Chart_polar.Axes.Left.Maximum:=aValue;

  Chart_polar.Axes.Right.AutomaticMaximum:=False;
  Chart_polar.Axes.Right.Maximum:=aValue;

  Chart_polar.Axes.Top.AutomaticMaximum:=False;
  Chart_polar.Axes.Top.Maximum:=aValue;

  Chart_polar.Axes.Bottom.AutomaticMaximum:=False;
  Chart_polar.Axes.Bottom.Maximum:=aValue;

  // ---------------------------------------------------------------

  Chart_decart.Axes.Left.AutomaticMaximum:=False;
  Chart_decart.Axes.Left.Maximum:=aValue;

end;



//---------------------------------------------------------------
procedure Tframe_Antenna_mask.Prepare;
//---------------------------------------------------------------
var i:integer; max: double;
begin
  max:=0;
  for i:=0 to Losses.Count-1 do
    if max < Losses[i].Loss then max:=Losses[i].Loss;

  if max > FGain then FDelta:=max //-FGain
                 else FDelta:=FGain;

  FDelta:=FDelta + 5;


//  if cb_Show_Log.Checked then
//    FDelta:=100;


  SetMax(FDelta);


end;
                 
 

//---------------------------------------------------------------
procedure Tframe_Antenna_mask.PaintDiagram2;
//---------------------------------------------------------------

    //-------------------------------------------------
    function DoLossToChart(aLoss: Double): double;
    //-------------------------------------------------
    const
      CONST_EXP = 4;
      DEF_OFFSET = 5;
    var
      dShare: double;
   //   dDelta: double;
    begin
    //  dDelta:=FDelta + 5;


      if rg_Scale.ItemIndex<>1 then
      begin
        dShare:= (FDelta  - aLoss) / FDelta;
        Result:= FDelta*(Exp(dShare*CONST_EXP)-1)/(Exp(CONST_EXP)-1);

      end else
        Result:= FDelta - aLoss;

    //  Result:= Result * dScale;
    end;





   //-------------------------------------------------
   procedure DoPaintDiagram;
   //-------------------------------------------------
   var i: integer;

       e,fLoss,fAngle: Double;
       bUseNull: boolean;
     s: string;
     sAngle: string;
   begin
     bUseNull:=false;



     if (FGain>0)  and (FDelta>0) then
     begin  // draw antenna diagram

       for i:=0 to Losses.Count-1 do
       begin
         fAngle:=Losses[i].Angle;
         fLoss :=Losses[i].Loss;

         // --------------------------------------

         if Round(fAngle) mod 30 = 0 then
           sAngle:= FloatToStr(Round(fAngle))
         else
           sAngle:= '';

         // --------------------------------------

         if fAngle<=180 then
         begin
         //  if sAngle<>'' then
             Series_decart.AddXY (fAngle, DoLossToChart(fLoss)  )
         //  else
         //    Series_decart.AddXY (fAngle, DoLossToChart(fLoss) )

         end
         else
         begin
           if not bUseNull then
           begin
             Series_decart.AddNull();
             bUseNull:=True;
           end;

        //   Series_decart.AddXY (fAngle-360, DoLossToChart(fLoss), sAngle)

       //   if sAngle<>'' then
             Series_decart.AddXY (fAngle-360, DoLossToChart(fLoss)  )
       //    else
        //     Series_decart.AddXY (fAngle-360, DoLossToChart(fLoss) )


         end;
           
       end;



       for i:=0 to Losses.Count-1 do
       begin
         fAngle:=Losses[i].Angle;
         fLoss :=Losses[i].Loss;

        // e:=FDelta-fLoss;

         Series_ant.AddPolar(-fAngle, DoLossToChart(fLoss));
       end;



       for i:=0 to 360-1 do
       begin
         Series_3.AddPolar(i, DoLossToChart(3));
         Series_6.AddPolar(i, DoLossToChart(6));
         Series_15.AddPolar(i, DoLossToChart(15));
         Series_30.AddPolar(i, DoLossToChart(30));
   //      Series_45.AddPolar(i, DoLossToChart(45));

       end;



     end;
   end;
   //-------------------------------------------------

begin
  Series_ant.Clear;


  Series_6.Visible       :=cb_Show_6_db.Checked;
  Series_decart_6.Visible:=cb_Show_6_db.Checked;


  Series_labels.Clear;

  Series_ant.Clear;
  Series_3.Clear;
  Series_6.Clear;
  Series_15.Clear;
  Series_30.Clear;
  Series_45.Clear;
  Series_Radius.Clear;

  // -------------------------

  Series_decart.Clear;
  Series_decart_3.Clear;
  Series_decart_6.Clear;
  Series_decart_15.Clear;
  Series_decart_30.Clear;
  Series_decart_45.Clear;

  Series_decart_labels.Clear;
  Series_decart_radius.Clear;



  Prepare();
  DoPaintDiagram;

  // ---------------------------------------------
  // Series_decart
  // ---------------------------------------------

  Series_decart_3.AddXY(-180, DoLossToChart(3));
  Series_decart_3.AddXY( 180, DoLossToChart(3));

  Series_decart_6.AddXY(-180, DoLossToChart(6));
  Series_decart_6.AddXY( 180, DoLossToChart(6));

  Series_decart_15.AddXY(-180, DoLossToChart(15));
  Series_decart_15.AddXY( 180, DoLossToChart(15));

  Series_decart_30.AddXY(-180, DoLossToChart(30));
  Series_decart_30.AddXY( 180, DoLossToChart(30));

//  Series_decart_45.AddXY(-180, DoLossToChart(45));
//  Series_decart_45.AddXY( 180, DoLossToChart(45));

  // ---------------------------------------------
  // Series_labels
  // ---------------------------------------------
  Series_labels.AddPolar(0, DoLossToChart(3),  '3');
  Series_labels.AddPolar(0, DoLossToChart(6),  '6');
  Series_labels.AddPolar(0, DoLossToChart(15), '15');
  Series_labels.AddPolar(0, DoLossToChart(30), '30');
//  Series_labels.AddPolar(0, DoLossToChart(45), '45');

{
  Series_labels.AddPolar(90, DoLossToChart(3),  '3');
  Series_labels.AddPolar(90, DoLossToChart(6),  '6');
  Series_labels.AddPolar(90, DoLossToChart(15), '15');
  Series_labels.AddPolar(90, DoLossToChart(30), '30');
  Series_labels.AddPolar(90, DoLossToChart(45), '45');
}


  Series_decart_labels.AddXY(0, DoLossToChart(3),  '3');
  Series_decart_labels.AddXY(0, DoLossToChart(6),  '6');
  Series_decart_labels.AddXY(0, DoLossToChart(15), '15');
  Series_decart_labels.AddXY(0, DoLossToChart(30), '30');
 // Series_decart_labels.AddXY(0, DoLossToChart(45), '45');


//  if rg_Scale.ItemIndex=0 then
//    ShowLogScale

end;


//---------------------------------------------------------------
procedure Tframe_Antenna_mask.ShowRadius(aAngle: Double);
//---------------------------------------------------------------
begin
  Series_Radius.Clear;

  Series_Radius.AddPolar(0,0);
  Series_Radius.AddPolar(-aAngle, FDelta);

  // --------------------------------------
  Series_decart_radius.Clear;

  if aAngle>180 then
    aAngle:=aAngle - 360;

  Series_decart_radius.AddXY(aAngle,0);
  Series_decart_radius.AddXY(aAngle, FDelta);


end;


//-------------------------------------------------------------------
procedure Tframe_Antenna_mask.LoadFromArray(var aMaskArr: TdmAntTypeMaskArray; aGain: double);
//-------------------------------------------------------------------   
var
  i: Integer;
begin

  dxMemData1.AfterPost:=nil;

  dxMemData1.Close;
  dxMemData1.Open;
  dxMemData1.DisableControls;


//  Losses.SortData;
  Losses.Clear;

  for i := 0 to High(aMaskArr) do
    Losses.AddItem (aMaskArr[i].Angle, aMaskArr[i].Loss);

  Losses.SortData;

  // ---------------------------------------------------------------

  for i := 0 to Losses.Count-1 do
 // begin
//    Losses.AddItem (aMaskArr[i].Angle, aMaskArr[i].Loss);

    db_AddRecord_ (dxMemData1, [FLD_ANGLE, Losses[i].Angle,
                                FLD_Loss,  Losses[i].Loss
                              ]);
//  end;



  dxMemData1.First;
  dxMemData1.EnableControls;

  dxMemData1.AfterPost:=mem_Items111AfterPost;


  FGain:=aGain;


  Paint;
end;



//--------------------------------------------------------------------
procedure TLossItemList.AddItem(aAngle, aLoss: double);
//--------------------------------------------------------------------
var obj: TLossItem;
  ind: integer;
begin
  ind:=IndexByAngle (aAngle);

  if ind<0 then
  begin
    obj:=TLossItem.Create;
    Add (obj);
    obj.Angle:=aAngle;
    obj.Loss:=aLoss;

//    Sort(SortCompareByAngle);
  end else
    Items[ind].Loss:=aLoss;
end;

//--------------------------------------------------------------------
function TLossItemList.GetItem(Index: integer): TLossItem;
//--------------------------------------------------------------------
begin
  Result:=TLossItem(inherited Items[Index]);
end;


function TLossItemList.IndexByAngle(aAngle: double): integer;
var
  I: Integer;
begin
  for I := 0 to Count-1 do
    if Items[i].Angle=aAngle then begin Result := i; Exit; end;

  Result := -1;
end;

// ---------------------------------------------------------------
function Compare_Loss(Item1, Item2: Pointer): Integer;
// ---------------------------------------------------------------
begin
  // сортировка по Bandwidth, Bitrate

  Result:=0;

  if TLossItem(Item1).Angle > TLossItem(Item2).Angle then
    Result := 1
  else

  if TLossItem(Item1).Angle < TLossItem(Item2).Angle then
    Result := -1


end;


procedure TLossItemList.SortData;
begin
  Sort(Compare_Loss);
end;



procedure Tframe_Antenna_mask.ActionManager1Update(Action: TBasicAction; var Handled: Boolean);
begin
  act_SaveToDB_.Enabled:=FIsPosted;
end;


procedure Tframe_Antenna_mask.Clear;
begin
  Losses.Clear;
//  Paint;

//  PaintDiagram;
  PaintDiagram2;

end;



procedure Tframe_Antenna_mask.LoadMaskFromString(aMask: string; aGain: double);
var
  arMask: TdmAntTypeMaskArray;
begin
  Mask_StrToArr(aMask,arMask);
  LoadFromArray (arMask, aGain);


//  PaintDiagram;
  PaintDiagram2;


  FOldMask:=aMask;
end;


procedure Tframe_Antenna_mask.ShowGrid (aValue: boolean);
begin
  pn_Grid.Visible:=aValue;
//  cxGrid1.Visible:=aValue;
end;


// ---------------------------------------------------------------
procedure Tframe_Antenna_mask.Mask_StrToArr(aValue: string; var aMask: TdmAntTypeMaskArray);
// ---------------------------------------------------------------
begin
  dmAntType.Mask_StrToArr(aValue, aMask);
end;


//-------------------------------------------------------------------
procedure Tframe_Antenna_mask.SaveIntoDB;
//-------------------------------------------------------------------
var
  arMask: TdmAntTypeMaskArray;

var
  sNewMask: string;
  i: Integer;
var
  arrMask: TdmAntTypeMaskArray;
  sField: string;
begin
  if not FIsPosted then
    Exit;


  SetLength (arMask, Losses.Count);

  for i := 0 to Losses.Count-1 do
  begin
    arMask[i].Angle:=Losses[i].Angle;
    arMask[i].Loss :=Losses[i].Loss;
  end;


  sNewMask:=dmAntType.Mask_GetLossesStr(arMask);


  FIsPosted:=False;



  case FMaskKind of
    DEF_MASK_HOR: sField:=FLD_HORZ_MASK;//  dmAntType_.Update(FAntTypeID, [db_Par(FLD_HORZ_MASK, sNewMask)]);
    DEF_MASK_VER: sField:=FLD_VERT_MASK;//  dmAntType_.Update(FAntTypeID, [db_Par(FLD_VERT_MASK, sNewMask)]);
  end;

  dmAntType.Update_(FAntTypeID, [db_Par(sField, sNewMask)]);
 
  dmOnega_DB_data.AntennaType_restore_from_mask(FAntTypeID);

  g_EventManager.PostEvent_(et_RefreshData,
      ['objname', OBJ_ANTENNA_TYPE,
       'id',      FAntTypeID
       ]);
  //


  //FAntTypeID

 // str_DeleteLastChar(sNewMask, ';');
  //  ShowMessage('')


{  if not Eq(sNewMask, FOldMask) then begin
  end;
  }
end;


procedure Tframe_Antenna_mask.act_SaveToDB_Execute(Sender: TObject);
begin
  SaveIntoDB();
end;



procedure Tframe_Antenna_mask.cxGrid1DBTableView1FocusedRecordChanged(Sender:
    TcxCustomGridTableView; APrevFocusedRecord, AFocusedRecord:
    TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
var
  e: Double;
begin
// AFocusedItem.

  if Assigned(AFocusedRecord) then
  begin
    e:=AFocusedRecord.Values[col_Angle1.Index];

  //  AFocusedItem.

  //  e:=mem_Items.FieldByName(FLD_ANGLE).AsFloat;

    ShowRadius(e);
  end
end;



procedure Tframe_Antenna_mask.mem_Items111AfterPost(DataSet: TDataSet);
begin                             
  FIsPosted:=True;
  SaveDataSet();
end;


//--------------------------------------------------------------------
procedure Tframe_Antenna_mask.SaveDataSet();
//--------------------------------------------------------------------
var
  fAngle,fLoss: double;
  bm: TBookmark;
begin
  Losses.Clear;

  with dxMemData1 do
  begin
    DisableControls;


    bm:=GetBookmark;
    First;

    while not EOF do
    begin
      fAngle:=FieldBYName(FLD_ANGLE).AsFloat;
      fLoss :=FieldBYName(FLD_LOSS).AsFloat;

      Losses.AddItem (fAngle, fLoss);

      Next;
    end;

    EnableControls();

    GotoBookmark(bm);
  end;

//  Paint;
end;


procedure Tframe_Antenna_mask.SetReadOnly(Value: Boolean);
begin
  cxGrid1DBTableView1.OptionsData.Editing := not Value;
end;


//-------------------------------------------------------------
procedure Tframe_Antenna_mask.View(aID, aMaskKind: integer);
//-------------------------------------------------------------
begin
  FAntTypeID:=aID;
  FMaskKind:=aMaskKind;

  case aMaskKind of
    DEF_MASK_VER: pn_Type.Caption:='вертикальная';
    DEF_MASK_HOR: pn_Type.Caption:='горизонтальная';
  end;

//  PaintDiagram;
  PaintDiagram2;

end;


// ---------------------------------------------------------------
procedure Tframe_Antenna_mask.ShowLogScale;
// ---------------------------------------------------------------
const
  DEF_PART_LEN = 50;

(*
   function DoGetX (aValue : double) : double;
   begin
       // e:=50 * Log10 ( 30+10);

{     if aValue < Power(10,1) then
       Result:= 1+  DEF_PART_LEN * Log10 ( aValue * Power(10,0))
     else

     if aValue < Power(10,2) then
       Result:= 10+ DEF_PART_LEN * Log10 ( (aValue - 10) * Power(10,1));
}

   end;
*)
   
  //    Series_decart_labels: TLineSeries;



var
  e: double;

  oSeries: TLineSeries;
  oPolar: TPolarSeries;

begin
  SetMax (100);

  oSeries:=Series_decart_labels;

  oSeries.Clear;
{

  oSeries.AddXY(0, 100 - 1, '1');
  oSeries.AddXY(0, 100 - 50, '50');
  oSeries.AddXY(0, 100 - 100, '100');
  }


 // Exit;

  e:=100 - 50 * Log10 ( 2+1);
  oSeries.AddXY(0, e,'2');

  e:=100 - 50 * Log10 ( 6+1);
  oSeries.AddXY(0, e,'6');

  e:=100 - 50 * Log10 ( 5 + 1);
  oSeries.AddXY(0, e,'5');

  e:=100 - 50 * Log10 ( 15+10);
  oSeries.AddXY(0, e,'15');

  e:=100 - 50 * Log10 ( 30+10);
  oSeries.AddXY(0, e,'30');

  e:=100 - 50 * Log10 ( 45+10);
  oSeries.AddXY(0, e,'45');


  // ---------------------------------------------------------------
  oPolar:=Series_labels;

  oPolar.Clear;

{
  oPolar.AddPolar(90, 100 - 1, '1');
  oPolar.AddPolar(90, 100 - 50, '50');
  oPolar.AddPolar(90, 100 - 100, '100');
 }

//  Exit;

  e:=100 - 50 * Log10 ( 3+1);
  oPolar.AddPolar(90, e,'3');

  e:=100 - 50 * Log10 ( 6+1);
  oPolar.AddPolar(90, e,'6');

//  e:=100 - 50 * Log10 ( 5 + 1);
//  oPolar.AddPolar(90, e,'5');

  e:=100 - 50 * Log10 ( 15+10);
  oPolar.AddPolar(90, e,'15');

  e:=100 - 50 * Log10 ( 30+10);
  oPolar.AddPolar(90, e,'30');

  e:=100 - 50 * Log10 ( 45+10);
  oPolar.AddPolar(90, e,'45');




end;



end.
