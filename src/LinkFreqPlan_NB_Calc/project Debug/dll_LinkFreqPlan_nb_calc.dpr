program dll_LinkFreqPlan_nb_calc;

uses
  Forms,
  dm_Main_app__NB_calc in '..\src\dm_Main_app__NB_calc.pas' {dmMain_app__NB_calc: TDataModule},
  dm_LinkFreqPlan_Neighbors in '..\src\dm_LinkFreqPlan_Neighbors.pas' {dmLinkFreqPlan_Neighbors: TDataModule},
  dm_LinkFreqPlan_Neighbors_calc in '..\src\dm_LinkFreqPlan_Neighbors_calc.pas' {dmLinkFreqPlan_Neighbors_calc: TDataModule},
  dm_Rel_Engine in '..\..\RelEngine\dm_Rel_Engine.pas' {dmRel_Engine: TDataModule},
  u_LinkFreqPlan_classes in '..\src\u_LinkFreqPlan_classes.pas',
  u_antenna_mask in '..\..\Common classes\u_antenna_mask.pas',
  d_LinkFreqPlan_calc_data_test in '..\src\d_LinkFreqPlan_calc_data_test.pas' {dlg_LinkFreqPlan_NB_calc_data_test};

{$R *.RES}



begin
  Application.Initialize;
  Application.CreateForm(TdmMain_app__NB_calc, dmMain_app__NB_calc);
  Application.Run;
end.
