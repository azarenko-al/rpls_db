program dll_LinkFreqPlan_nb_calc;

uses
  dm_LinkFreqPlan_Neighbors in '..\src\dm_LinkFreqPlan_Neighbors.pas' {dmLinkFreqPlan_Neighbors: TDataModule},
  dm_LinkFreqPlan_Neighbors_calc in '..\src\dm_LinkFreqPlan_Neighbors_calc.pas' {dmLinkFreqPlan_Neighbors_calc: TDataModule},
  dm_Main_app__NB_calc in '..\src\dm_Main_app__NB_calc.pas' {dmMain_app__NB_calc: TDataModule},
  dm_Rel_Engine in '..\..\RelEngine\dm_Rel_Engine.pas' {dmRel_Engine: TDataModule},
  Forms,
  u_ini_LinkFreqPlan_nb_calc in '..\src shared\u_ini_LinkFreqPlan_nb_calc.pas',
  u_rrl_param_rec_new in '..\..\Link_Calc\src_shared\u_rrl_param_rec_new.pas',
  u_LinkFreqPlan_classes in '..\..\LinkFreqPlan_common_classes\u_LinkFreqPlan_classes.pas',
  u_link_calc in '..\..\LinkFreqPlan_calc\src_shared\u_link_calc.pas',
  u_LinkFreqPlan_antenna_classes in '..\..\LinkFreqPlan_common_classes\u_LinkFreqPlan_antenna_classes.pas';

{$R *.RES}



begin
  Application.Initialize;
  Application.CreateForm(TdmMain_app__NB_calc, dmMain_app__NB_calc);
  Application.Run;
end.
