unit dm_LinkFreqPlan_Neighbors_calc_new;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms, Math, Dialogs, Controls, Variants,

   XMLDoc, XMLIntf,

   u_Ini_rpls_rrl_params,


   u_LinkCalcResult_new,


   u_run,

   u_link_model_layout,

   u_xml_document,

  dm_LinkEnd_ex,


  dm_Onega_DB_data,

  //u_XML,

  u_vars,

  u_rrl_param_rec_new,

  u_antenna_mask,

  u_const,

  u_const_db,
  u_Link_const,

  u_LinkFreqPlan_const,


  u_db,

  u_files,
  u_func,

  u_Geo,

  u_rel_Profile,

  dm_Main,
  dm_Progress,

 // dm_AntType,
  dm_LinkEnd,

  dm_Library,

//  dm_act_Rel_Engine,
  dm_Rel_Engine,

 // u_antenna_classes,

  u_LinkEnd_classes,

  u_LinkFreqPlan_classes;

type

  TCalc_Freq_SpacingRec = record
    CI_treb: Double;     // aCI_t - ��������� CI, ��
    CI_0: Double;     // aCI_0 - �������� CI (��� ���������� ������), ��
    PowerNoise_dBm: Double;  // aPowerNoise  - �������� ����, dBm
    PowerNeighbour_dBm: Double;
    Degradation_treb_dB: Double;
    Degradation_calc: Double;
    Freq1_spacing_MHz: Double;
    Freq2_spacing_MHz: Double;

    A1, A2, B1, B2: Double;

    Band_Width_MHz: double;  //

  //-------�������� ������--------
  // aCI_t - ��������� CI, ��
  // aCI_0 - �������� CI (��� ���������� ������), ��
  // aPowerNoise  - �������� ����, dBm
  // aPowerNeighbour - �������� ������ ��� ���������� ������, dBm
  // aDegradation_treb - ���������� �������� ���������� ����������������, dB
  // aDegradation_calc - ���������� �������� ���������� ���������������� ��� ���������� �������, dB
  // aFreq1_spacing, aFreq2_spacing - ������ (���) ������, ���
  // A1, A2 - ���. ���������� ����� ��� ������� �� ������ 10 �����, ��
  // B1, B2 - ���������� ��������� ������, ��
  //(��� � � �: 1-��� ���1; 2-��� ���2)
  // aBand_Width - ������ ���������� ��������� (����. ��� 13 ���-500��� (12750...13250))
// ---------------------------------------------------------------

  end;



  TdmLinkFreqPlan_Neighbors_calc_new111 = class(TdmProgress)
    procedure DataModuleCreate(Sender: TObject);
//    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
 

    FVector:         TBLVector;
    FrelProfile:     TrelProfile;
    FClutterModelID: integer;

//    FPROPERTY_ID : Integer;
(*
    FDBLinkend1: TnbLinkFreqPlan_Linkend;
    FDBLinkend2: TnbLinkFreqPlan_Linkend;

    FDBAntennas1: TnbAntennaList;
    FDBAntennas2: TnbAntennaList;
*)

{    FDBLinkend1: TDBLinkend;
    FDBLinkend2: TDBLinkend;
}

    FIsPropertyOne:  boolean; //���� ��������
    FIsLink:         boolean; //�������� ��������
//    FIsRange:        boolean; //���� ��������
    FDiagram_level:  double;
//    FFreq_GHz:       double;
    FDistanse_m:       double;



    function Calc_Diagram_new(aBLPoint1, aBLPoint2: TBLPoint; aLinkEnd1, aLinkEnd2:
        TnbLinkFreqPlan_Linkend): double;

    function SaveProfileToXMLNode(aCalcParam: TrrlCalcParam; aStep: integer):     boolean;

    procedure LoadCalcResultsFromXML1(aID1, aID2: integer; aDBNeighbor:
        TnbNeighbor; aFileName: string);
    function Run_rpls_rrl(aFile1, aFile2, aFile_layout: string): Boolean;

    function SaveToXML(aFileName: string; aLinkEnd1,aLinkEnd2: TnbLinkFreqPlan_Linkend):
        boolean;

  public
    Params: record
              Calc_Distance_m: double;
              LinkFreqPlan_ID: integer;
              PolarizationLevel: Double;
            end;


(*      e:= Abs(gl_DB.GetDoubleFieldValue(TBL_LINKFREQPLAN,
                FLD_PolarizationLevel,
                [db_par(FLD_ID, Params.LinkFreqPlan_ID)]));
*)

    function Calc_Freq_Spacing_MHz(aRec: TCalc_Freq_SpacingRec): double;

{    aCI_t, aCI_0,
       aPowerNoise, aPowerNeighbour, aDegradation_treb, aDegradation_calc,
       aFreq1_spacing, aFreq2_spacing, A1, A2, B1, B2, aBand_Width: double): double;
}


    function Execute(aLinkEnd_ID1, aLinkEnd_ID2: integer;
         aLinkEnd1,aLinkEnd2: TnbLinkFreqPlan_Linkend;
          aDBNeighbor: TnbNeighbor;

         var aTerminated: boolean): boolean;

    class procedure Init;

    function OpenData_new(aLinkEnd1, aLinkEnd2: TnbLinkFreqPlan_Linkend; var
        aTerminated: boolean): boolean;

  end;

var
  dmLinkFreqPlan_Neighbors_calc_new111: TdmLinkFreqPlan_Neighbors_calc_new111;


//  function dmLinkFreqPlan_Neighbors_calc: TdmLinkFreqPlan_Neighbors_calc;



//==================================================================
implementation

uses I_rel_Matrix1; {$R *.dfm}
//==================================================================


class procedure TdmLinkFreqPlan_Neighbors_calc_new111.Init;
begin

//  if not Assigned(dmLinkFreqPlan_Neighbors_calc_new111) then
//    dmLinkFreqPlan_Neighbors_calc_new := TdmLinkFreqPlan_Neighbors_calc_new.Create(Application);

end;


// ---------------------------------------------------------------
procedure TdmLinkFreqPlan_Neighbors_calc_new111.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;

  db_SetComponentADOConnection(Self, dmMain.ADOConnection);

  FrelProfile:=TrelProfile.Create();


//  Flog:=TStingList.Create();


(*
  FDBLinkend1 := TnbLinkFreqPlan_Linkend.Create(nil);
  FDBLinkend2 := TnbLinkFreqPlan_Linkend.Create(nil);
  FDBAntennas1 := TnbAntennaList.Create();
  FDBAntennas2 := TnbAntennaList.Create();
*)
end;

//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_Neighbors_calc_new111.DataModuleDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
(*  FreeAndNil(FDBAntennas2);
  FreeAndNil(FDBAntennas1);
  FreeAndNil(FDBLinkend2);
  FreeAndNil(FDBLinkend1);
  *)

  FreeAndNil(FrelProfile);

//  FreeAndNil(FLog);

  inherited;
end;


//--------------------------------------------------------------------
function TdmLinkFreqPlan_Neighbors_calc_new111.Calc_Freq_Spacing_MHz(aRec:
    TCalc_Freq_SpacingRec): double;
//--------------------------------------------------------------------
  //-------�������� ������--------
  // aCI_t - ��������� CI, ��
  // aCI_0 - �������� CI (��� ���������� ������), ��
  // aPowerNoise  - �������� ����, dBm
  // aPowerNeighbour - �������� ������ ��� ���������� ������, dBm
  // aDegradation_treb - ���������� �������� ���������� ����������������, dB
  // aDegradation_calc - ���������� �������� ���������� ���������������� ��� ���������� �������, dB
  // aFreq1_spacing, aFreq2_spacing - ������ (���) ������, ���
  // A1, A2 - ���. ���������� ����� ��� ������� �� ������ 10 �����, ��
  // B1, B2 - ���������� ��������� ������, ��
  //(��� � � �: 1-��� ���1; 2-��� ���2)
  // aBand_Width - ������ ���������� ��������� (����. ��� 13 ���-500��� (12750...13250))
// ---------------------------------------------------------------
const
  STEP_F = 0.125;  //��� ��������
var
  dCI_prev: double;
  f: double; //������� ��������� ������
  dY_f       : double; //���������� �������� �����
  dFreq_delta: double; //
  dQ_0       : double; //�����. ���������� �� ��������� ������, � �����
  dQ_1       : double; //�����. ���������� �� �������� ������ ���1, � �����
  dQ_2       : double; //�����. ���������� �� �������� ������ ���2, � �����
  dCI_calc    : double; //�������� CI ��� ���. ��������� �������
  dPowerNeighbour_calc: double; //�������� ������  ��� ���. ��������� �������, dBm
  dThresholdDegradation_calc: double; //���������� ���������������� ��� ���. ��������� �������, dB

begin
  try
    Result:= aRec.Band_Width_MHz;

    if (aRec.Freq1_spacing_MHz=0) or (aRec.Freq2_spacing_MHz=0) then
      Exit;

    if (aRec.CI_treb < aRec.CI_0) and (aRec.Degradation_treb_dB > aRec.Degradation_calc) then
    begin
      Result := 0;
      Exit;
    end;

    dThresholdDegradation_calc:= aRec.Degradation_calc;

    f:= STEP_F;
    while f < aRec.Band_Width_MHz do
    begin
      dY_f:= 0.5*(aRec.Freq1_spacing_MHz + aRec.Freq2_spacing_MHz) - f;

      if dY_f > 0 then
        dFreq_delta:= IIF(dY_f < Min(aRec.Freq1_spacing_MHz, aRec.Freq2_spacing_MHz), dY_f,
                                 Min(aRec.Freq1_spacing_MHz, aRec.Freq2_spacing_MHz))
      else
        dFreq_delta:= 0;

      dQ_0 := dFreq_delta / aRec.Freq1_spacing_MHz;
      if f=0 then
      begin
        dQ_1:=0;
        dQ_2:=0;
      end
      else
      begin
        dQ_1 := IIF(f<0.5*aRec.Freq1_spacing_MHz, 0, Power(10, 0.1*(-aRec.B1-aRec.A1*log10(f/aRec.Freq1_spacing_MHz))));
        dQ_2 := IIF(f<0.5*aRec.Freq2_spacing_MHz, 0, Power(10, 0.1*(-aRec.B2-aRec.A2*log10(f/aRec.Freq2_spacing_MHz))));
      end;

      dCI_calc:= aRec.CI_0 - 10*log10(dQ_0 + dQ_1 + dQ_2);

      if aRec.PowerNoise_dBm<>255 then
      begin
        dPowerNeighbour_calc      := aRec.PowerNeighbour_dBm + 10*log10(dQ_0 + dQ_1 + dQ_2);
        dThresholdDegradation_calc:= TruncFloat(10*log10(Power(10, 0.1*aRec.PowerNoise_dBm) +
                                      Power(10, 0.1*dPowerNeighbour_calc)) - aRec.PowerNoise_dBm);
      end;

      if (dCI_calc > aRec.CI_treb) and
         (dThresholdDegradation_calc < aRec.Degradation_treb_dB) then
      begin
        Result:= f;

//        Flog.Add (

        Exit;
      end;

      f:= f + STEP_F;
    end;
  except

  end;

end;


//--------------------------------------------------------------------
function TdmLinkFreqPlan_Neighbors_calc_new111.Calc_Diagram_new(aBLPoint1, aBLPoint2:
    TBLPoint; aLinkEnd1, aLinkEnd2: TnbLinkFreqPlan_Linkend): double;
//--------------------------------------------------------------------
var
  dTiltAngle: double; //���� ����� ����� ������������� �� �������� � ��������� ���

    // ---------------------------------------------------------------
    //------ ������� ����������� ���������� ��� � �������� �����������
    // ---------------------------------------------------------------
    function DoGetLoss_Antenna (aBLPoint1, aBLPoint2: TBLPoint;
                                aTypeMask: string;
                               // aTypeMask: integer;

                               // aAntQuery: TADOQuery;
                               // aAzimuth : Double;
                               // var aAntType: TdmAntTypeInfoRec;

                                aAntenna: TnbAntenna
                              //  var aMask: TdmAntTypeMask
                                ): double;
    // ---------------------------------------------------------------
{
    0: Result:= 'H';
    1: Result:= 'V';
    2: Result:= 'X';
}
    // ---------------------------------------------------------------
   { const
      TBL_Antenna_Mask = 'AntennaMask';

      SQL_SELECT_ANTENNA_MASK =
         'SELECT * FROM '+ TBL_Antenna_Mask +
         ' WHERE (antennaType_id=:antennaType_id) and (type=:type) ' +
         ' ORDER BY angle';
}
    var
      dAzimuth, dAngle: double;
      d: Double;
      v: Variant;

    begin
      Result := 0;

///      Assert(aAntQuery.RecordCount>0, 'aAntQuery.RecordCount>0');


      if geo_ComparePoints(aBLPoint1, aBLPoint2) then
        dAzimuth:= 90
      else
      begin
       // v:=aAntQuery.FieldValues[FLD_AZIMUTH];

      //  d:=aAntQuery.FieldByName(FLD_AZIMUTH).AsFloat;

        d:=aAntenna.Azimuth;


        dAzimuth:= TruncFloat(geo_Azimuth(aBLPoint1, aBLPoint2));
        dAzimuth:= dAzimuth - aAntenna.Azimuth;


//        dAzimuth:= TruncFloat(geo_Azimuth(aBLPoint1, aBLPoint2));
 //       dAzimuth:= dAzimuth - aAntQuery.FieldByName(FLD_AZIMUTH).AsFloat;

      end;

      if dAzimuth < 0 then
        dAzimuth:= dAzimuth + 360;

      dTiltAngle :=0;

      if (dAzimuth = 90) or (dTiltAngle = 0) then
        dAngle:= dAzimuth
      else
        dAngle:= TruncFloat((180/Pi) * arccos(cos(dAzimuth*Pi/180) * cos(dTiltAngle*Pi/180)));



      if Eq(aTypeMask,'h') then
        Result:=aAntenna.HorzMask.GetLossByAngle(dAngle) else
      if Eq(aTypeMask,'v') then
        Result:=aAntenna.VertMask.GetLossByAngle(dAngle)
     //   Result:=dmAntType.Mask_GetLossByAngle (aAntType.VertMaskArr, dAngle)
      else
        Result:=0;
(*

      if Eq(aTypeMask,'h') then
        Result:=dmAntType.Mask_GetLossByAngle (aAntType.HorzMaskArr, dAngle);
      if Eq(aTypeMask,'v') then
        Result:=dmAntType.Mask_GetLossByAngle (aAntType.VertMaskArr, dAngle)
      else
        Result:=0;
*)

(*      case aTypeMask of
        0: Result:=dmAntType.Mask_GetLossByAngle (aAntType.HorzMaskArr, dAngle);
        1: Result:=dmAntType.Mask_GetLossByAngle (aAntType.VertMaskArr, dAngle);
      else
        Result:=0;
      end;
*)
{
      db_OpenQuery(qry_Mask, SQL_SELECT_ANTENNA_MASK,
          [db_par(FLD_ANTENNATYPE_ID, aAntQuery.FieldValues[FLD_ANTENNATYPE_ID]),
           db_par(FLD_TYPE,           aTypeMask)]);

//      if dAngle > 0 then
        if qry_Mask.Locate(FLD_ANGLE, dAngle, []) then
          Result:= qry_Mask.FieldByName(FLD_LOSS).AsFloat
        else

        begin
          qry_Mask.First;
          with qry_Mask do while not Eof do
             if dAngle < FieldByName(FLD_ANGLE).AsFloat then
          begin
            Result:= FieldByName(FLD_LOSS).AsFloat;
            Break;
          end
          else
            Next;

            if Result = 0 then
              Result:= qry_Mask.FieldByName(FLD_LOSS).AsFloat;

        end;
}
    end;

    // ---------------------------------------------------------------
    //------ ������� ����������� ������� �� ��������������� ����������� -----
    // ---------------------------------------------------------------
    function DoIsPolarization_Other_Diagramm () : boolean;
    //var aAntType1,aAntType2: TdmAntTypeInfoRec
    // ---------------------------------------------------------------
   { const                   //(var aMask: TdmAntTypeMask)


      SQL_SELECT_ANTENNA_SUM_LOSS =
           ' SELECT SUM(loss) AS SUM  FROM '+ TBL_Antenna_Mask +
           ' WHERE (antennaType_id=:antennaType_id) AND (angle = 0) ';
}

    var
      dSum,dVLoss,dHLoss,dPolarizationRatio : double;

      oAntenna1: TnbAntenna;
      oAntenna2: TnbAntenna;

    begin
      Result := False;

      oAntenna1 :=aLInkEnd1.Antennas[0];

      dVLoss:=oAntenna1.VertMask.GetLossByAngle(0);
      dHLoss:=oAntenna1.HorzMask.GetLossByAngle(0);

(*      dVLoss:=dmAntType.Mask_GetLossByAngle (aAntType1.VertMaskArr , 0);
      dHLoss:=dmAntType.Mask_GetLossByAngle (aAntType1.HorzMaskArr , 0);
*)
      dSum:=dVLoss + dHLoss;

{
      db_OpenQuery(qry_Temp, SQL_SELECT_ANTENNA_SUM_LOSS,
          [db_par(FLD_ANTENNATYPE_ID, qry_Antennas1.FieldValues[FLD_ANTENNATYPE_ID])]);

      dPolarizationRatio:= gl_DB.GetDoubleFieldValue(TBL_ANTENNA_TYPE, FLD_POLARIZATION_RATIO,
          [db_par(FLD_ID, qry_Antennas1.FieldValues[FLD_ANTENNATYPE_ID])]);
}
   //   Result := (qry_Temp.FieldByName('SUM').AsFloat = dPolarizationRatio);

      Result := (dSum = oAntenna1.Polarization_ratio);

    //  Result := (dSum = aAntType1.POLARIZATION_RATIO);

      if not Result then
        Exit;

      oAntenna2 :=aLInkEnd2.Antennas[0];

      dVLoss:=oAntenna2.VertMask.GetLossByAngle(0);
      dHLoss:=oAntenna2.HorzMask.GetLossByAngle(0);

(*
      dVLoss:=dmAntType.Mask_GetLossByAngle (aAntType2.VertMaskArr , 0);
      dHLoss:=dmAntType.Mask_GetLossByAngle (aAntType2.HorzMaskArr , 0);
*)
      dSum:=dVLoss + dHLoss;

{      db_OpenQuery(qry_Temp, SQL_SELECT_ANTENNA_SUM_LOSS,
          [db_par(FLD_ANTENNATYPE_ID, qry_Antennas2.FieldValues[FLD_ANTENNATYPE_ID])]);

      dPolarizationRatio:= gl_DB.GetDoubleFieldValue(TBL_ANTENNA_TYPE, FLD_POLARIZATION_RATIO,
          [db_par(FLD_ID, qry_Antennas2.FieldValues[FLD_ANTENNATYPE_ID])]);

      Result := (qry_Temp.FieldByName('SUM').AsFloat = dPolarizationRatio);
    }

      Result := (dSum = oAntenna2.Polarization_ratio);

   //   Result := (dSum = aAntType2.Polarization_Ratio);

    end;
//
//    // ---------------------------------------------------------------
//    //------ ������� ����������� ���� ����� -----
//    // ---------------------------------------------------------------
//    function DoGetTiltAngle(): double;
//    // ---------------------------------------------------------------
//    var
//      dTiltAngle1, dTiltAngle2, dRelHeight11, dRelHeight12, dRelHeight2: double;
//      dAntHeight11, dAntHeight12, dAntHeight2: double;
//      dHeight11, dHeight12, dHeight2 : double;
//      blPoint11, blPoint12, blPoint2: TBLPoint;
//      e: Double;
//      iLinkEndID11, iLinkEndID12, iLinkEndID2: integer;
//
//      oAntenna1: TnbAntenna;
//      oAntenna2: TnbAntenna;
//
//    begin
//      Result := 0;
//
//      oAntenna1 := aLinkEnd1.AntennaList[0];
//      oAntenna2 := aLinkEnd2.AntennaList[0];
//
//      blPoint11 := oAntenna1.BLPoint;
//      blPoint2  := oAntenna2.BLPoint;
//
//
//      blPoint11.B:= qry_Antennas1.FieldByName(FLD_LAT).AsFloat;
//      blPoint11.L:= qry_Antennas1.FieldByName(FLD_LON).AsFloat;
//      blPoint2.b := qry_Antennas2.FieldByName(FLD_LAT).AsFloat;
//      blPoint2.L := qry_Antennas2.FieldByName(FLD_LON).AsFloat;
//
//      if geo_ComparePoints(blPoint11, blPoint2) then
//        Exit;
//
//      dAntHeight11:= oAntenna1.Height;
//      dAntHeight2 := oAntenna2.Height;
//
//
//      dAntHeight11:= qry_Antennas1.FieldByName(FLD_HEIGHT).AsFloat;
//      dAntHeight2 := qry_Antennas2.FieldByName(FLD_HEIGHT).AsFloat;
//
//      iLinkEndID2:=qry_Antennas2.FieldByName(FLD_LINKEND_ID).AsInteger;
//      iLinkEndID11:=qry_Antennas1.FieldByName(FLD_LINKEND_ID).AsInteger;
//
//      iLinkEndID12:= gl_DB.GetIntFieldValue(TBL_LINK, FLD_LINKEND2_ID,
//                                       [db_par(FLD_LINKEND1_ID, iLinkEndID11)]);
//
//      if iLinkEndID12=0 then
//        iLinkEndID12:= gl_DB.GetIntFieldValue(TBL_LINK, FLD_LINKEND1_ID,
//                                       [db_par(FLD_LINKEND2_ID, iLinkEndID11)]);
//
//      blPoint12:= dmLinkEnd.GetPropertyPos(iLinkEndID12);
//      dAntHeight12:= dmLinkEnd_ex.GetMaxAntennaHeight(iLinkEndID12);
//
//      e:=aLinkEnd1.MaxAntennaHeight;
//      e:=aLinkEnd2.MaxAntennaHeight;
//
//      e:=aLinkEnd1.PROPERTY_GROUND_HEIGHT;
//      e:=aLinkEnd2.PROPERTY_GROUND_HEIGHT;
//
//
//      dRelHeight11:=gl_DB.GetDoubleFieldValue(TBL_PROPERTY, FLD_GROUND_HEIGHT,
//                       [db_par(FLD_ID, dmLinkEnd.GetPropertyID(iLinkEndID11))]);
//      dRelHeight12:=gl_DB.GetDoubleFieldValue(TBL_PROPERTY, FLD_GROUND_HEIGHT,
//                       [db_par(FLD_ID, dmLinkEnd.GetPropertyID(iLinkEndID12))]);
//      dRelHeight2 :=gl_DB.GetDoubleFieldValue(TBL_PROPERTY, FLD_GROUND_HEIGHT,
//                       [db_par(FLD_ID, dmLinkEnd.GetPropertyID(iLinkEndID2))]);
//
//      dHeight11:= dAntHeight11 + dRelHeight11;
//      dHeight12:= dAntHeight12 + dRelHeight12;
//      dHeight2 := dAntHeight2 + dRelHeight2;
//
//      dTiltAngle1:= geo_TiltAngle (blPoint11, blPoint12, dHeight11, dHeight12);
//      dTiltAngle2:= geo_TiltAngle (blPoint11, blPoint2, dHeight11, dHeight2);
//
//      Result := TruncFloat(Abs(dTiltAngle1 - dTiltAngle2));
//    end;    //
//    // ---------------------------------------------------------------


var
//  bDifferentPolarization: boolean; // ������ �����������
  bEqualPolarization: Boolean;
  bPolarization_Other_Diagramm : boolean; //������� �� ��������������� �����������
  dGain1, dGain2, dGain1_Different, dGain2_Different: double;
  e: Double;

//  rVMask,rHMask: TdmAntTypeMask;

 // rAntType1,rAntType2: TdmAntTypeInfoRec;

  //  function TdmAntType.GetInfoRec (aID: integer; var aRec: TdmAntTypeInfoRec): boolean;

{        procedure Mask_StrToArr(aValue: string; var aMask: TdmAntTypeMask);
    function Mask_GetLossByAngle(var aMask: TdmAntTypeMask; aAngle: double): double;
}

  oAntenna1: TnbAntenna;
  oAntenna2: TnbAntenna;

begin
(*
  if FIsPropertyOne then
    dTiltAngle:= 0
  else
    dTiltAngle:= DoGetTiltAngle;

*)
(*  if FIsPropertyOne then dTiltAngle:= DoGetTiltAngle
                    else dTiltAngle:= 0;
*)


  oAntenna1 := aLinkEnd1.Antennas[0];
  oAntenna2 := aLinkEnd2.Antennas[0];

(*
  bDifferentPolarization:=
//    oAntenna1.Polarization_str <> oAntenna2.Polarization_str;
    oAntenna1.Polarization <> oAntenna2.Polarization;
*)

  bEqualPolarization:=
//    oAntenna1.Polarization_str <> oAntenna2.Polarization_str;
    oAntenna1.Polarization = oAntenna2.Polarization;



//  bDifferentPolarization:=
//    aLinkEnd1.AntennaList[0].Polarization <>
//                          aLinkEnd2.AntennaList[0].Polarization;
//

//    aLinkEnd1.AntennaList[0].Polarization_str <>
  //                        aLinkEnd2.AntennaList[0].Polarization_str;

(*
  bDifferentPolarization:=

    // (qry_Antennas1.FieldByName(FLD_POLARIZATION_STR).Asstring<>null) and
    // (qry_Antennas2.FieldByName(FLD_POLARIZATION_STR).Asstring<>null) and

     (qry_Antennas1.FieldByName(FLD_POLARIZATION_STR).AsString <>
      qry_Antennas2.FieldByName(FLD_POLARIZATION_STR).AsString);
*)

(*
  dmAntType.GetInfoRec1
     (qry_Antennas1.FieldByName(FLD_ANTENNATYPE_ID).AsInteger, rAntType1);

  dmAntType.GetInfoRec1
     (qry_Antennas2.FieldByName(FLD_ANTENNATYPE_ID).AsInteger, rAntType2);

*)

  bPolarization_Other_Diagramm:= DoIsPolarization_Other_Diagramm();
 // (rAntType1,rAntType2);


//  if (not bDifferentPoarization) or (not bPolarization_Other_Diagramm) then
  if (bEqualPolarization) or (not bPolarization_Other_Diagramm) then
  begin
    //������ TX Antenna Loss
    Result:= DoGetLoss_Antenna(aBLPoint1, aBLPoint2,
             oAntenna1.Polarization_str,
             //qry_Antennas1.FieldByName(FLD_POLARIZATION_STR).asString,
      //       qry_Antennas1,
       //      qry_Antennas1.FieldByName(FLD_AZIMUTH).AsFloat,
         //    rAntType1,
             oAntenna1
             //aLinkEnd1.AntennaRefList[0]
             );

  //������ RX Antenna Loss
    e:= DoGetLoss_Antenna(aBLPoint2, aBLPoint1,
             oAntenna2.Polarization_str,
            // qry_Antennas2.FieldByName(FLD_POLARIZATION_STR).asString,
           //  qry_Antennas2,
          //   qry_Antennas2.FieldByName(FLD_AZIMUTH).AsFloat,
            // rAntType2,
             oAntenna2
             //aLinkEnd2.AntennaRefList[0]
             );
    Result:= Result + e;

  //  if (not bPolarization_Other_Diagramm) and (bDifferentPolarization) then
    if (not bPolarization_Other_Diagramm) and (not bEqualPolarization) then
    begin
      e:=Params.PolarizationLevel;

      e:= Abs(gl_DB.GetDoubleFieldValue(TBL_LINKFREQPLAN,
                FLD_PolarizationLevel,
                [db_par(FLD_ID, Params.LinkFreqPlan_ID)]));

      Result:= Result + e;
    end;

  end
  else
  begin
    dGain1:= DoGetLoss_Antenna(aBLPoint1,aBLPoint2,
           oAntenna1.Polarization_str,
       //    qry_Antennas1.FieldByName(FLD_POLARIZATION_STR).asString,
         //  qry_Antennas1,
          // qry_Antennas1.FieldByName(FLD_AZIMUTH).AsFloat,
         //  rAntType1,
           oAntenna1 //aLinkEnd1.AntennaRefList[0]
           );

    dGain2:= DoGetLoss_Antenna(aBLPoint2,aBLPoint1,
           oAntenna2.Polarization_str,
        //   qry_Antennas2.FieldByName(FLD_POLARIZATION_STR).asString,
         //  qry_Antennas2,
          // qry_Antennas2.FieldByName(FLD_AZIMUTH).AsFloat,
         //  rAntType2,
           oAntenna2 //aLinkEnd2.AntennaRefList[0]
           );

    dGain1_Different:= DoGetLoss_Antenna(aBLPoint1,aBLPoint2,
           oAntenna2.Polarization_str,
        //   qry_Antennas2.FieldByName(FLD_POLARIZATION_STR).asString,
         //  qry_Antennas1,
        //   qry_Antennas1.FieldByName(FLD_AZIMUTH).AsFloat,
         //  rAntType1,
           oAntenna1 //aLinkEnd1.AntennaRefList[0]
           );

    dGain2_Different:= DoGetLoss_Antenna(aBLPoint2,aBLPoint1,
           oAntenna1.Polarization_str,
         //  qry_Antennas1.FieldByName(FLD_POLARIZATION_STR).asString,
         //  qry_Antennas2,
         //  qry_Antennas2.FieldByName(FLD_AZIMUTH).AsFloat,
         //  rAntType2,
           oAntenna2 //aLinkEnd2.AntennaRefList[0]
           );

    Result:= Min(dGain1 + dGain2_Different,
                 dGain2 + dGain1_Different);
  end;

end;



//--------------------------------------------------------------------
function TdmLinkFreqPlan_Neighbors_calc_new111.OpenData_new(aLinkEnd1, aLinkEnd2:
    TnbLinkFreqPlan_Linkend; var aTerminated: boolean): boolean;
//--------------------------------------------------------------------
//const
 // SQL_SELECT_LINKENDTYPE =
  //   'SELECT * FROM '+ TBL_LinkEndType +' WHERE id=:id';

(*
const
  SQL_SELECT_LINKEND_ANTENNA =
//     'SELECT * FROM '+ TBL_LINKEND_ANTENNA +' WHERE linkend_id=:linkend_id';

     'SELECT * FROM '+ VIEW_LINKEND_ANTENNAS +' WHERE linkend_id=:linkend_id';
*)

var iLinkEndType,iPropID1,iPropID2: integer;
    blPos1, blPos2: TBLPoint;
  eFreq_Mhz: Double;
    f1,f2: double;
  s: string;
begin
//  raise Exception.Create('');
  Result:= False;

(*  Assert(aLinkEnd1.AntennaList.Count>0, 'Value <=0');
  Assert(aLinkEnd2.AntennaList.Count>0, 'Value <=0');
*)
(*

  db_OpenQuery (qry_Antennas1, SQL_SELECT_LINKEND_ANTENNA,
                [db_Par(FLD_LINKEND_ID, aLinkEnd_ID1)]);

  db_OpenQuery (qry_Antennas2, SQL_SELECT_LINKEND_ANTENNA,
                [db_Par(FLD_LINKEND_ID, aLinkEnd_ID2)]);

  FDBAntennas1.LoadFromDataset(qry_Antennas1);
  FDBAntennas2.LoadFromDataset(qry_Antennas2);


  qry_Antennas1.First;
  qry_Antennas2.First;*)


  FClutterModelID:=aLInkEnd1.ClutterModel_ID;

(*  FClutterModelID:= gl_DB.GetIntFieldValue(TBL_LINK, FLD_CLUTTER_MODEL_ID,
                                      [db_par(FLD_LINKEND1_ID, aLinkEndID2)]);
  if FClutterModelID=0 then
    FClutterModelID:= gl_DB.GetIntFieldValue(TBL_LINK, FLD_CLUTTER_MODEL_ID,
                                      [db_par(FLD_LINKEND2_ID, aLinkEndID2)]);

  blPos1 := aLinkEnd1.AntennaList[0].BLPoint;
  blPos2 := aLinkEnd2.AntennaList[0].BLPoint;
*)



  FDistanse_m:= geo_Distance_m(aLinkEnd1.PROPERTY_BLPoint, aLinkEnd2.PROPERTY_BLPoint);

//  FDistanse_m:= geo_Distance_m(blPos1, blPos2);


(*  blPos1.B:= qry_Antennas1.FieldByName(FLD_LAT).AsFloat;
  blPos1.L:= qry_Antennas1.FieldByName(FLD_LON).AsFloat;
  blPos2.B:= qry_Antennas2.FieldByName(FLD_LAT).AsFloat;
  blPos2.L:= qry_Antennas2.FieldByName(FLD_LON).AsFloat;
  FDistanse_m:= geo_Distance_m(blPos1, blPos2);
*)
  Assert(Params.Calc_Distance_m>0);

  if FDistanse_m > Params.Calc_Distance_m then
    Exit;

  FIsLink:=aLinkEnd1.Link_id = aLinkEnd2.Link_id;

(*
  FIsLink:=aLinkEnd1.Next_Linkend_id = aLinkEnd2.Linkend_id;


  if (gl_DB.GetIntFieldValue(TBL_LINK, FLD_ID,
              [db_par(FLD_LINKEND1_ID, aLinkEndID1),
               db_par(FLD_LINKEND2_ID, aLinkEndID2)])>0)
       or
     (gl_DB.GetIntFieldValue(TBL_LINK, FLD_ID,
              [db_par(FLD_LINKEND1_ID, aLinkEndID2),
               db_par(FLD_LINKEND2_ID, aLinkEndID1)])>0)
  then FIsLink:= True
  else FIsLink:= False;
*)

  blPos1 := aLinkEnd1.PROPERTY_BLPoint;
  blPos2 := aLinkEnd2.PROPERTY_BLPoint;



  FVector:= MakeBLVector (aLinkEnd1.PROPERTY_BLPoint, aLinkEnd2.PROPERTY_BLPoint);

//    FDistanse_m:= geo_Distance_m(aLinkEnd1.PROPERTY_BLPoint, aLinkEnd2.PROPERTY_BLPoint);


//  db_OpenQueryByID (qry_LinkType, TBL_LINK_TYPE, iLinkType);
(*  db_OpenTableByID (qry_LinkEnd1, VIEW_LINKEND,  aLinkEndID1);
  db_OpenTableByID (qry_LinkEnd2, VIEW_LINKEND,  aLinkEndID2);


  FDBLinkend1.LoadFromDataset(qry_LinkEnd1);
  FDBLinkend2.LoadFromDataset(qry_LinkEnd2);
*)

(*  iPropID1:= FDBLinkend1.PROPERTY_ID;
  iPropID2:= FDBLinkend2.PROPERTY_ID;


  iPropID1:= aLinkEnd1.PROPERTY_ID;
  iPropID2:= aLinkEnd2.PROPERTY_ID;
*)

  FIsPropertyOne:=(aLinkEnd1.PROPERTY_ID=aLinkEnd2.PROPERTY_ID);


(*
  if iPropID1=iPropID2 then FIsPropertyOne:= True
                       else FIsPropertyOne:= False;


 // iPropID1:= qry_LinkEnd1.FieldValues[FLD_PROPERTY_ID];
 // iPropID2:= qry_LinkEnd2.FieldValues[FLD_PROPERTY_ID];

  if iPropID1=iPropID2 then FIsPropertyOne:= True
                       else FIsPropertyOne:= False;

*)

{  if qry_LinkEnd1.FieldValues[FLD_RANGE] <>     //linkend ������ ����������
     qry_LinkEnd2.FieldValues[FLD_RANGE] then
}

{  if qry_LinkEnd1.FieldValues[FLD_BAND] <>     //linkend ������ ����������
     qry_LinkEnd2.FieldValues[FLD_BAND] then
}

  if aLinkEnd1.BAND <>     //linkend ������ ����������
     aLinkEnd2.BAND then

//  if FDBLinkend1.BAND <>     //linkend ������ ����������
 //    FDBLinkend2.BAND then
  begin
//    FIsRange:= False;

    if FIsLink then
    begin
      s:='�� ��������� '+ aLinkEnd1.Link_name +
                // GetNameLink_ByLinkEnd(aLinkEndID1, aLinkEndID2)+
               ' ��� ������ ����������!'+#13#10+'���������� ������?';

      aTerminated:=(MessageDlg (s, mtError, [mbYes,mbNo],0)=mrYes);
    end;

    Exit;
  end else
  ;
   // FIsRange:= True;


  //get first linkend types
//  iLinkEndType:=FDBLinkend1.LINKENDTYPE_ID;

//  iLinkEndType:=aLinkEnd1.LINKENDTYPE_ID;

 // iLinkEndType:=qry_LinkEnd1.FieldByName(FLD_LINKENDTYPE_ID).AsInteger;

  if aLinkEnd1.LINKENDTYPE_ID = 0 then
  begin
    aTerminated:=(MessageDlg ('�� ��� '''+
              aLinkEnd1.Name +
              //qry_LinkEnd1.FieldByName(FLD_NAME).AsString+
             ''' ��������� ������������!'+#13#10+'���������� ������?',
             mtError, [mbYes,mbNo],0)=mrYes);
    Exit;
  end;

{  db_OpenQuery (qry_LinkEndType, SQL_SELECT_LINKENDTYPE,
                [db_Par(FLD_ID, iLinkEndType)]);
}

  //������� ����������� ���������� ��� � �������� �����������
  FDiagram_level:= Calc_Diagram_new(blPos1, blPos2, aLinkEnd1, aLinkEnd2);


//  with qry_LinkEnd1 do //������� ����������� !!!
////  FFreq_GHz:=TruncFloat(aLinkend1.TX_FREQ_MHz / 1000, 2);


 // with qry_LinkEnd1 do //������� ����������� !!!
//    FFreq_GHz:=TruncFloat(FieldByName(FLD_TX_FREQ_MHz).AsFloat/ 1000, 2);

//  if FFreq_Ghz=0 then
//  begin
// //   db_OpenQuery (qry_LinkEndType, SQL_SELECT_LINKENDTYPE,
//   //               [db_Par(FLD_ID, iLinkEndType)]);
//
////    with qry_LinkEndType do
//  //  begin
//      //������� ������� ���������
//
//(*
//      eFreq_Mhz:=dmLibrary.GetBandAveFreq_MHZ(FDBLinkend1.BAND);
//
////      FFreq_Ghz:=(FieldByName(FLD_FREQ_MIN).AsFloat + FieldByName(FLD_FREQ_MAX).AsFloat) / 2;
//
//      FFreq_Ghz:=TruncFloat(eFreq_Mhz / 1000, 2);
//*)
//   // end;
//  end;



  Result:=True;
end;



//--------------------------------------------------------------------
function TdmLinkFreqPlan_Neighbors_calc_new111.SaveToXML(aFileName: string; aLinkEnd1,aLinkEnd2:
    TnbLinkFreqPlan_Linkend): boolean;
//--------------------------------------------------------------------
(*
  //--------------------------------------------------------------------
  procedure DoAdd (aNode: IXMLNode; aParamID: integer; aValue: Variant; aComment: string='');
  //--------------------------------------------------------------------
  begin
    xml_AddNode (aNode, TAG_PARAM, [xml_Att(ATT_id, aParamID),
                                    xml_Att(ATT_value, aValue),
                                    xml_Att(ATT_COMMENT, aComment ) ]);
  end;*)

  //--------------------------------------------------------------------
  procedure DoAddLinkType(aCalcParam: TrrlCalcParam);
  //--------------------------------------------------------------------
 // var vGroup: IXMLNode;
  var
    eLength_KM: Double;
  begin
    //RRV: array[1..11] of double; //�������������� ����� �� �������� ��������������� ���������
     {---1=-8.000     ;������� �������� ��������� ����.�������������,10^-8 1/�
      2= 7.000     ;����������� ���������� ���������,10^-8 1/�
      3= 1.000     ;��� ������������ �������.(1...3,0->����.�����=����.���.)
      ---4= 7.500     ;���������� ��������� �������� ����, �/���.�
      5= 1.000     ;Q-������ ������ �����������
      6=41.000     ;������������� ������ K��, 10^-6
      7= 1.500     ;�������� ������������ b ��� ������� ������
      8= 0.500     ;�������� ������������ c ��� ������� ������
      9= 2.000     ;�������� ������������ d ��� ������� ������
      10=20.000    ;������������� ����� � ������� 0.01% �������, ��/���
      11= 1.335    ;�����. ��������� (��� ��.���������=0, ����� �.�.=0)  }

  (*    vGroup:=aRoot.AddChild('RRV');
      DoAdd (vGroup, 1,  -8,  '������� �������� ��������� ����.�������������,10^-8 1/�' );
      DoAdd (vGroup, 2,  7,   '����������� ���������� ���������,10^-8 1/�' );
      DoAdd (vGroup, 3,  1,   '��� ������������ �������.(1...3,0->����.�����=����.���.)' );
      DoAdd (vGroup, 4,  7.5, '���������� ��������� �������� ����, �/���.�');
      DoAdd (vGroup, 5,  1,   'Q-������ ������ �����������');
      DoAdd (vGroup, 6,  41,  '������������� ������ K��, 10^-6');
      DoAdd (vGroup, 7,  1.5, '�������� ������������ b ��� ������� ������');
      DoAdd (vGroup, 8,  0.5, '�������� ������������ c ��� ������� ������');
      DoAdd (vGroup, 9,  2,   '�������� ������������ d ��� ������� ������');
      DoAdd (vGroup, 10, 20,  '������������� ����� � ������� 0.01% �������, ��/���');
      DoAdd (vGroup, 11, -1,  '�����. ��������� (��� ��.���������=0, ����� �.�.=0)'); // "-1" -������� ������������ �������
*)

      aCalcParam.RRV.gradient_diel_1           :=-10; //'������� �������� ��������� ����.�������������,10^-8 1/�' );
      aCalcParam.RRV.gradient_deviation_2      :=8;  //'����������� ���������� ���������,10^-8 1/�' );
      aCalcParam.RRV.terrain_type_14           :=0;  //'��� ���������(0...2) 1-���������� 2-���������� (��������) 3-���������� (������)' );

      aCalcParam.RRV.underlying_terrain_type_3 :=2;  //'��� ������������ �������.(1...3,0->����.�����=����.���.)' );
      aCalcParam.RRV.steam_wet_4               :=7.5;//'���������� ��������� �������� ����, �/���.�');

      aCalcParam.RRV.Air_temperature_12    :=15;
      aCalcParam.RRV.Atmosphere_pressure_13:=1013;

      aCalcParam.RRV.Q_factor_5                :=1;  //'Q-������ ������ �����������');
      aCalcParam.RRV.climate_factor_6          :=410; //'������������� ������ K��, 10^-6');
      aCalcParam.RRV.factor_B_7                :=1.5;//'�������� ������������ b ��� ������� ������');
      aCalcParam.RRV.factor_C_8                :=2;//'�������� ������������ c ��� ������� ������');
      aCalcParam.RRV.FACTOR_D_9                :=2;  //'�������� ������������ d ��� ������� ������');
      aCalcParam.RRV.RAIN_intensity_10         :=24.68; //'������������� ����� � ������� 0.01% �������, ��/���');


//      aCalcParam.RRV.gradient_diel_1           :=-8; //'������� �������� ��������� ����.�������������,10^-8 1/�' );
//      aCalcParam.RRV.gradient_deviation_2      :=7;  //'����������� ���������� ���������,10^-8 1/�' );
//      aCalcParam.RRV.underlying_terrain_type_3 :=1;  //'��� ������������ �������.(1...3,0->����.�����=����.���.)' );
//      aCalcParam.RRV.steam_wet_4               :=7.5;//'���������� ��������� �������� ����, �/���.�');
//      aCalcParam.RRV.Q_factor_5                :=1;  //'Q-������ ������ �����������');
//      aCalcParam.RRV.climate_factor_6          :=41; //'������������� ������ K��, 10^-6');
//      aCalcParam.RRV.factor_B_7                :=1.5;//'�������� ������������ b ��� ������� ������');
//      aCalcParam.RRV.factor_C_8                :=0.5;//'�������� ������������ c ��� ������� ������');
//      aCalcParam.RRV.FACTOR_D_9                :=2;  //'�������� ������������ d ��� ������� ������');
//      aCalcParam.RRV.RAIN_intensity_10         :=20; //'������������� ����� � ������� 0.01% �������, ��/���');
//      aCalcParam.RRV.REFRACTION_11             :=-1; //'�����. ��������� (��� ��.���������=0, ����� �.�.=0)'); // "-1" -������� ������������ �������'�����. ��������� (��� ��.���������=0, ����� �.�.=0)'); // "-1" -������� ������������ �������


(*      gradient_diel : double;   // '������� �������� ��������� ����.�������������,10^-8 1/�' );
      gradient      : double;   // '����������� ���������� ���������,10^-8 1/�' );

      underlying_terrain_type: Double; //'��� ������������ �������.(1...3,0->����.�����=����.���.)' );
      terrain_type_14           : Double;   //'��� ���������.(1...3,0->����.�����=����.���.)' );

      steam_wet     : double;   // '���������� ��������� �������� ����, �/���.�');
      Q_factor      : double;   // 'Q-������ ������ �����������');
      climate_factor: double;   //'������������� ������ K��, 10^-6');
      factor_B      : double;   // '�������� ������������ b ��� ������� ������');
      factor_C      : double;   // '�������� ������������ c ��� ������� ������');
      FACTOR_D      : double;   // '�������� ������������ d ��� ������� ������');
      RAIN_RATE     : double;   // '������������� ����� � ������� 0.01% �������, ��/���');
      REFRACTION: double;       //'�����. ��������� (��� ��.���������=0, ����� �.�.=0)');
*)


    //DLT: array[1..4] of double;  //���������� � �������� ��������
     {1= 0.100     ;��� ��������� �������, �� ( 0 - ��������� ����)
      2= 1.000     ;�������� ������ ������� V����(g)=V���.�, ��
      3= 1.000     ;�������� ������ ������� g(V����)=g(V���.�), 10^-8
      4=80.000     ;������������ �������� ��� ������� ������������, 10^-8}

(*      vGroup:=xml_AddNodeTag (aRoot, 'DLT');

      DoAdd (vGroup,  1, 0,   '��� ��������� �������, �� ( 0 - ��������� ����)');
      DoAdd (vGroup,  2, 1,   '�������� ������ ������� V����(g)=V���.�, ��');
      DoAdd (vGroup,  3, 1,   '�������� ������ ������� g(V����)=g(V���.�), 10^-8');
      DoAdd (vGroup,  4, 80,  '������������ �������� ��� ������� ������������, 10^-8');
*)
      aCalcParam.DLT.Profile_Step_KM_1          :=0.025;
      aCalcParam.DLT.precision_V_diffraction_2  :=1;
      aCalcParam.DLT.precision_g_V_diffraction_3:=1;
      aCalcParam.DLT.max_gradient_for_subrefr_4 :=80;


//      aCalcParam.DLT. gradient_diel_1 :=-8;


    //TRB: array[1..4] of double;  //���������� � ����������� �������� ���
     {1=0.012      ;����������� �������� SESR ��� ����, %
      2=0.05       ;����������� �������� ���  ��� ����, %
      3=600        ;����� ����, ��
      4=0.200      ;���������� ����� ���, ������������� �������������}
(*
      vGroup:=xml_AddNodeTag (aRoot, 'TRB');
      DoAdd (vGroup,  1, 0.012,  '����������� �������� SESR ��� ����, %');
      DoAdd (vGroup,  2, 0.05,   '����������� �������� ���  ��� ����, %');
      DoAdd (vGroup,  3, 600,    '����� ����, ��');
      DoAdd (vGroup,  4, 0.2,    '���������� ����� ���, ������������� �������������}');
*)
    aCalcParam.TRB.GST_SESR_1     :=0.006; //����������� �������� SESR ��� ����, %'
     aCalcParam.TRB.GST_Kng_2      :=0.0125;  //����������� �������� ���  ��� ����, %
//      aCalcParam.TRB.GST_equivalent_length_km_3:=600;   //����� ����, ��


     eLength_KM:=  geo_Distance_km(FVector);


      aCalcParam.TRB.GST_Length_KM_3_:=eLength_KM;

//      aCalcParam.TRB.GST_Length_KM_3:=600;   //����� ����, ��
      aCalcParam.TRB.KNG_dop_4      :=0.2;   //���������� ����� ���, ������������� �������������}

      aCalcParam.TRB.SPACE_LIMIT_5 :=0;   //���������� ������������� ������� p(a%)
      aCalcParam.TRB.SPACE_LIMIT_PROBABILITY_6 :=20;   //���������� ����������� a% ������������� ������������ �������� p(a%) [%]

//
//      aCalcParam.TRB.GST_SESR_1     :=0.012; //����������� �������� SESR ��� ����, %'
//      aCalcParam.TRB.GST_Kng_2      :=0.05;  //����������� �������� ���  ��� ����, %
////      aCalcParam.TRB.GST_equivalent_length_km_3:=600;   //����� ����, ��
//      aCalcParam.TRB.GST_Length_KM_3:=600;   //����� ����, ��
//      aCalcParam.TRB.KNG_dop_4      :=0.2;   //���������� ����� ���, ������������� �������������}

  end;
  //--------------------------------------------------------------------

var
 // vRoot,vGroup,vGroup1: IXMLNode;

  iValue: integer;
 // dTHRESHOLD_dBm: double;
 // oXMLDoc: TXMLDoc;
 // e: double;

  oCalcParam: TrrlCalcParam;

  oDBAntenna1: TnbAntenna;
  oDBAntenna2: TnbAntenna;

  s: string;
  iPOLARIZATION: integer;
  e: double;

begin
//  oXMLDoc:=TXMLDoc.Create;

  oCalcParam:=TrrlCalcParam.Create;

//  vRoot:=xml_AddNodeTag (oXMLDoc.DocumentElement, 'RRL_PARAMS');

  DoAddLinkType (oCalcParam);

  // ��� �����������, 0-��������������,1-������������
  // P������ �������, ���
  // �������� ��������, ��/�
  // ������ ���������, ���
  // ������ ���������, ��
  // ��� ������� ���������, 1-���, 2-��, 3-���
  // ����� ������� ���������
  // �������, �������� ������������, ��
  // ��������� ���������� �� �������, 1...8
  // ��������� ���������� �� ������������, 1...2
  // ��������� ������, ���
  //vGroup:=xml_AddNodeTag (vRoot, 'TTX');


 // e:=FDBLinkEnd1.Power_dBm;

  oDBAntenna1:=aLinkEnd1.Antennas[0];

 // Assert(Assigned(oDBAntenna), 'Value not assigned');

  e:=aLinkEnd1.Power_dBm;

(*
  assert(qry_LinkEnd1.Active);
  assert(qry_LinkEnd2.Active);

  db_View(qry_LinkEnd1);
*)



//  DoAdd (vGroup,  1, qry_LinkEnd1.FieldByName(FLD_POWER_dBm).AsFloat, 'site1-��������');
  //DoAdd (vGroup,  1, aLinkEnd1.Power_dBm, 'site1-��������');

//  with qry_Antennas1 do
  begin
    oCalcParam.TTX.Site1_Power_dBm := aLinkEnd1.Power_dBm;
    oCalcParam.TTX.Site1_DIAMETER  := oDBAntenna1.Diameter;
    oCalcParam.TTX.Site1_GAIN      := oDBAntenna1.Gain;

    oCalcParam.TTX.Site1_LOSS := aLinkEnd1.LOSS + oDBAntenna1.Loss;
     // FieldByName(FLD_LOSS_dB).AsFloat + qry_LinkEnd1.FieldByName(FLD_LOSS).AsFloat;

    oCalcParam.TTX.Site1_VERT_WIDTH:= oDBAntenna1.Vert_width;// FieldByName(FLD_VERT_WIDTH).AsFloat;
    oCalcParam.TTX.Site1_HEIGHT    := oDBAntenna1.Height; // FieldByName(FLD_HEIGHT).AsFloat;

//    oCalcParam.TTX.site
    // -------------------------
(*
    DoAdd (vGroup,  2,  FieldByName(FLD_DIAMETER).AsFloat,   'site1-�������'); //Site1.Antenna_Diameter]);
    DoAdd (vGroup,  3,  FieldByName(FLD_GAIN).AsFloat,       'site1-��������'); // Site1.Antenna_Gain]);
    DoAdd (vGroup,  4,  FieldByName(FLD_VERT_WIDTH).AsFloat, 'site1-������ ���');//  Site1.Antenna_Width]);
    DoAdd (vGroup,  5,  FieldByName(FLD_LOSS_dB).AsFloat+
           qry_LinkEnd1.FieldByName(FLD_LOSS).AsFloat, 'site1-������ � ���');{Site1.Feeder_Losses}

    DoAdd (vGroup,  6,  FieldValues[FLD_HEIGHT],     'site1-������'); // Site1.Antenna_Height]);
*)

  end;

  { xml_AddNode (vGroup, 'param', ['id','value'], [24, Site1.Extra.Antenna_Diameter]);
    xml_AddNode (vGroup, 'param', ['id','value'], [25, Site1.Extra.Antenna_Gain]);
    xml_AddNode (vGroup, 'param', ['id','value'], [26, Site1.Extra.Antenna_Width]);
    xml_AddNode (vGroup, 'param', ['id','value'], [27, Site1.Extra.Feeder_Losses]);
    xml_AddNode (vGroup, 'param', ['id','value'], [28, Site1.Extra.Antenna_Height]);  }

{ TODO : ���������������� ���� ��� BER 10^-3 �.�. qry_Link �����������... �� �������... }

   //  dTHRESHOLD_dBm:=FDBLinkEnd2.THRESHOLD_BER_3;
  //   dTHRESHOLD_dBm:=qry_LinkEnd2.FieldByName(FLD_THRESHOLD_BER_3).AsInteger;
//    1: dTHRESHOLD_dBm:=qry_LinkEnd2.FieldByName(FLD_THRESHOLD_BER_6).AsInteger;

 // oCalcParam.TTX.Site2_Threshold_dBM := aLinkEnd2.THRESHOLD_BER_3;

//  DoAdd (vGroup,  7,  dTHRESHOLD_dBm,  'site2-������');

  oDBAntenna2:=aLinkEnd2.Antennas[0];

//  Assert(Assigned(oDBAntenna), 'Value not assigned');



 // with qry_Antennas2 do
  begin
    oCalcParam.TTX.Site2_Threshold_dBM := aLinkEnd2.THRESHOLD_BER_3;;
    oCalcParam.TTX.Site2_DIAMETER  := oDBAntenna2.Diameter;
    oCalcParam.TTX.Site2_GAIN      := oDBAntenna2.Gain;
    oCalcParam.TTX.Site2_LOSS      := oDBAntenna2.Loss + aLinkEnd2.LOSS;
//                                      qry_LinkEnd2.FieldByName(FLD_LOSS).AsFloat;
    oCalcParam.TTX.Site2_VERT_WIDTH:= oDBAntenna2.Vert_width;
    oCalcParam.TTX.Site2_HEIGHT    := oDBAntenna2.Height;

(*
    DoAdd (vGroup,  8,  FieldByName(FLD_DIAMETER).AsFloat,  'site2-�������'); //Site1.Antenna_Diameter]);
    DoAdd (vGroup,  9,  FieldByName(FLD_GAIN).AsFloat,      'site2-��������'); // Site1.Antenna_Gain]);
    DoAdd (vGroup,  10, FieldByName(FLD_VERT_WIDTH).AsFloat,'site2-������ ���');//  Site1.Antenna_Width]);
    DoAdd (vGroup,  11, FieldByName(FLD_LOSS_dB).AsFloat+
           qry_LinkEnd2.FieldByName(FLD_LOSS).AsFloat, 'site2-������ � ���');{Site1.Feeder_Losses}

    DoAdd (vGroup,  12, FieldByName(FLD_HEIGHT).AsFloat,    'site2-������'); // Site1.Antenna_Height]);
*)
  end;

{   xml_AddNode (vGroup, 'param', ['id','value'], [7,  Site2.Sensitivity]);
    xml_AddNode (vGroup, 'param', ['id','value'], [8,  Site2.Antenna_Diameter]);
    xml_AddNode (vGroup, 'param', ['id','value'], [9,  Site2.Antenna_Gain]);
    xml_AddNode (vGroup, 'param', ['id','value'], [10, Site2.Antenna_Width]);
    xml_AddNode (vGroup, 'param', ['id','value'], [11, Site2.Feeder_Losses]);
    xml_AddNode (vGroup, 'param', ['id','value'], [12, Site2.Antenna_Height]); }

  case oDBAntenna1.Polarization of
    ptH: iPOLARIZATION :=0;
    ptV: iPOLARIZATION :=1;
    ptX: iPOLARIZATION :=2;
  end;

  case oDBAntenna1.Polarization of
    ptH: oCalcParam.TTX.POLARIZATION_type :=ptH_;
    ptV: oCalcParam.TTX.POLARIZATION_type :=ptV_;
    ptX: oCalcParam.TTX.POLARIZATION_type :=ptX_;
  end;
          


(*
  s := qry_Antennas1.FieldByName(FLD_POLARIZATION_sTR).AsString;
  if Eq(s,'h') then
    iPOLARIZATION :=0 else
  if Eq(s,'v') then
    iPOLARIZATION :=1
  else
    iPOLARIZATION :=2;
*)

  oCalcParam.TTX.POLARIZATION :=iPOLARIZATION;



//  oCalcParam.TTX.POLARIZATION := qry_Antennas1.FieldByName(FLD_POLARIZATION).AsInteger;

//  FFreq_GHz:=TruncFloat(aLinkend1.TX_FREQ_MHz / 1000, 2);
//  oCalcParam.TTX.Freq_GHz := FFreq_GHz;

/////  oCalcParam.TTX.Freq_GHz := FFreq_GHz;
 
  oCalcParam.TTX.Freq_GHz:=TruncFloat(aLinkend1.TX_FREQ_MHz / 1000, 2);


////  DoAdd (vGroup, 13, iPOLARIZATION, '��� �����������, 0-��������������,1-������������,2-�����');
 // DoAdd (vGroup, 14, FFreq_GHz,  'P������ �������, ���');


//  with qry_LinkEnd1 do
  begin
    oCalcParam.TTX.SIGNATURE_WIDTH_Mhz := aLinkEnd1.SIGNATURE_WIDTH_MHz; //  FieldByName(FLD_SIGNATURE_WIDTH).AsFloat;
    oCalcParam.TTX.SIGNATURE_HEIGHT_dB := aLinkEnd1.SIGNATURE_HEIGHT_dB;    // FieldByName(FLD_SIGNATURE_HEIGHT).AsFloat;

    oCalcParam.TTX.MODULATION_Level_COUNT  :=1;
   // oCalcParam.TTX.MODULATION_COUNT  :=1;

 //   oCalcParam.TTX.EQUALISER_PROFIT  :=FieldByName(FLD_EQUALISER_PROFIT).AsFloat;

    oCalcParam.TTX.KRATNOST_BY_FREQ :=  aLinkEnd1.KRATNOST_BY_FREQ;  // FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger + 1;
    oCalcParam.TTX.Freq_Spacing_MHz :=  aLinkEnd1.Freq_Spacing_MHz;  // FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger + 1;
//    oCalcParam.TTX.

(*
    DoAdd (vGroup, 16, FieldByName(FLD_SIGNATURE_WIDTH).AsFloat,  '������ ���������, ���');
    DoAdd (vGroup, 17, FieldByName(FLD_SIGNATURE_HEIGHT).AsFloat, '������ ���������, ��');
    DoAdd (vGroup, 19, 1,                                         '����� ������� ���������');
//    DoAdd (vGroup, 20, FieldByName(FLD_EQUALISER_PROFIT).AsFloat, '�������, �������� ������������, ��');
    DoAdd (vGroup, 20, 0, '�������, �������� ������������, ��');
    DoAdd (vGroup, 21, FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger + 1, '��������� ���������� �� �������, 1...8');  //�� ���

    DoAdd (vGroup, 23, FieldValues[FLD_Freq_Spacing], '��������� ������, ���');
*)
  end;

 // with qry_LinkEnd2 do
  begin
//    iValue:=aLinkEnd2.KRATNOST_BY_SPACE ;
 //   iValue:=iValue+1;

    oCalcParam.TTX.KRATNOST_BY_SPACE := aLinkEnd2.KRATNOST_BY_SPACE + 1;

  //  oCalcParam.TTX.KRATNOST_BY_SPACE := iValue;    // FieldByName(FLD_SIGNATURE_HEIGHT).AsFloat;

(*    iValue:=FieldByName(FLD_KRATNOST_BY_SPACE).AsInteger;
    iValue:=iValue+1;
    DoAdd (vGroup, 22, iValue,  '��������� ���������� �� ������������, 1...2'); //�� ���
*)
  end;

//  oDBAntenna:=FDBAntennas1[0];

  if aLinkEnd1.Antennas.Count>1 then
//  with qry_Antennas1 do
  begin
    oDBAntenna1:=aLinkEnd1.Antennas[1];

    oCalcParam.TTX.Dop_Site1.DIAMETER  := oDBAntenna1.Diameter;
    oCalcParam.TTX.Dop_Site1.GAIN      := oDBAntenna1.Gain;

   // oCalcParam.TTX.Site2_LOSS      := oDBAntenna2.Loss + aLinkEnd2.Loss;
//                                      qry_LinkEnd2.FieldByName(FLD_LOSS).AsFloat;
    oCalcParam.TTX.Dop_Site1.VERT_WIDTH:= oDBAntenna1.Vert_width;
    oCalcParam.TTX.Dop_Site1.LOSS1     := oDBAntenna1.Loss;

    oCalcParam.TTX.Dop_Site1.Height    := oDBAntenna1.Height;


(*
   Next;

    DoAdd (vGroup,  24,  FieldValues[FLD_DIAMETER],   'dop-site1-�������');   //Site1.Antenna_Diameter]);
    DoAdd (vGroup,  25,  FieldValues[FLD_GAIN],       'dop-site1-��������');  // Site1.Antenna_Gain]);
    DoAdd (vGroup,  26,  FieldValues[FLD_VERT_WIDTH], 'dop-site1-������ ���');//  Site1.Antenna_Width]);
    DoAdd (vGroup,  27,  FieldValues[FLD_LOSS_dB],    'dop-site1-������ � ���');{Site1.Feeder_Losses}
    DoAdd (vGroup,  28,  FieldValues[FLD_HEIGHT],     'dop-site1-������');    // Site1.Antenna_Height]);
*)

  end;


 if aLinkEnd2.Antennas.Count>1 then
//  with qry_Antennas1 do
  begin
    oDBAntenna2:=aLinkEnd2.Antennas[1];

    oCalcParam.TTX.Dop_Site2.DIAMETER  := oDBAntenna2.Diameter;
    oCalcParam.TTX.Dop_Site2.GAIN      := oDBAntenna2.Gain;

   // oCalcParam.TTX.Site2_LOSS      := oDBAntenna2.Loss + aLinkEnd2.Loss;
//                                      qry_LinkEnd2.FieldByName(FLD_LOSS).AsFloat;
    oCalcParam.TTX.Dop_Site2.VERT_WIDTH:= oDBAntenna2.Vert_width;
    oCalcParam.TTX.Dop_Site2.LOSS1    := oDBAntenna2.Loss;

    oCalcParam.TTX.Dop_Site2.Height  := oDBAntenna2.Height;
 end;

(*  with qry_Antennas2 do
  begin
    Next;

    DoAdd (vGroup,  29,  FieldValues[FLD_DIAMETER],   'dop-site2-�������'); //Site1.Antenna_Diameter]);
    DoAdd (vGroup,  30,  FieldValues[FLD_GAIN],       'dop-site2-��������'); // Site1.Antenna_Gain]);
    DoAdd (vGroup,  31,  FieldValues[FLD_VERT_WIDTH], 'dop-site2-������ ���');//  Site1.Antenna_Width]);
    DoAdd (vGroup,  32,  FieldValues[FLD_LOSS_dB],     'dop-site2-������ � ���');{Site1.Feeder_Losses}
    DoAdd (vGroup,  33,  FieldValues[FLD_HEIGHT],     'dop-site2-������'); // Site1.Antenna_Height]);

  end;*)


//  oCalcParam.

(*
  DoAdd (vGroup, 34, qry_Antennas1.FieldValues[FLD_FrontToBackRatio],  'ant1 - ����.���.�������� (������/�����) ���-�������, ��');
  DoAdd (vGroup, 35, qry_Antennas2.FieldValues[FLD_FrontToBackRatio],  'ant2 - ����.���.�������� (������/�����) ���-�������, ��');

  DoAdd (vGroup, 36, qry_Antennas1.FieldBYName(FLD_FrontToBackRatio).AsFloat,  'ant1 - ����.���.�������� (������/�����) ���-�������, ��');
  DoAdd (vGroup, 37, qry_Antennas2.FieldBYName(FLD_FrontToBackRatio).AsFloat,  'ant2 - ����.���.�������� (������/�����) ���-�������, ��');
*)

  oCalcParam.TTX.Site1_KNG := aLinkEnd1.KNG;
  oCalcParam.TTX.Site2_KNG := aLinkEnd2.KNG;

(*
  DoAdd (vGroup, 38, qry_LinkEnd1.FieldBYName(FLD_KNG).AsFloat,  '��� ������������ (%)1');
  DoAdd (vGroup, 39, qry_LinkEnd2.FieldBYName(FLD_KNG).AsFloat,  '��� ������������ (%)2');
*)

  if not SaveProfileToXMLNode (oCalcParam, 50) then  { TODO : ��� ���� ����� � ������� �������. �.�.}
    Exit;

 // oXMLDoc.SaveToFile(aFileName);

 // oXMLDoc.Free;


  oCalcParam.SaveToXML(aFileName);

  FreeAndNil(oCalcParam);

  Result:= True;


end;

//--------------------------------------------------------------------
function TdmLinkFreqPlan_Neighbors_calc_new111.SaveProfileToXMLNode(aCalcParam:
    TrrlCalcParam; aStep: integer): boolean;
//--------------------------------------------------------------------
var i: integer;
//  vGroup: IXMLNode;
begin
  Result:= False;

  if FIsPropertyOne then
  begin
    raise Exception.Create('');
  //  aCalcParam.Relief.RecordCount :=2;

//    aCalcParam.
    SetLength (aCalcParam.Relief.Items, 2);

//    e:=IIF(FDistanse_m<1, 1, FDistanse_m);

//    aCalcParam.Relief.Items[1].Distance_KM := 0.001;
    aCalcParam.Relief.Items[1].Distance_KM := 1;


(*    vGroup:=xml_AddNode (vRoot, 'relief', [xml_Att('count', 2) ]);

    xml_AddNode (vGroup, 'item',
                       [xml_ATT ('dist',       0),
                        xml_ATT ('rel_h',      0),
                        xml_ATT ('local_h',    0),
                        xml_ATT ('local_code', 0) ]);

    xml_AddNode (vGroup, 'item',
                       [xml_ATT ('dist',       IIF(FDistanse_m<1, 1, FDistanse_m)),
                        xml_ATT ('rel_h',      0),
                        xml_ATT ('local_h',    0),
                        xml_ATT ('local_code', 0) ]);
*)
    Result:= True;
    Exit;
  end;

  dmRel_Engine.AssignClutterModel (FClutterModelID, FrelProfile.Clutters );
  dmRel_Engine.BuildProfile1 (FrelProfile, FVector, aStep); //, FClutterModelID);

  if FrelProfile.Count=0 then
    Exit;

  //  aCalcParam.Relief.RecordCount :=2;

//    aCalcParam.
    SetLength (aCalcParam.Relief.Items, FrelProfile.Count);


(*  vGroup:=xml_AddNode (vRoot, 'relief',
                       [xml_Att('count', FrelProfile.Count) ]);
*)

  aCalcParam.Relief.Length_KM:= FrelProfile.Data.Distance_KM;


  with FrelProfile.Data do
    for i:=0 to Count-1 do
  begin
    aCalcParam.Relief.Items[i].Distance_KM  := Items[i].Distance_KM;
    aCalcParam.Relief.Items[i].rel_h        := Items[i].Rel_H;
    aCalcParam.Relief.Items[i].Clutter_h    := Items[i].Clutter_H;
    aCalcParam.Relief.Items[i].Clutter_code := Items[i].Clutter_Code;


(*    xml_AddNode (vGroup, 'item',
       [xml_ATT ('dist',       TruncFloat(Items[i].Distance_KM, 3)),
        xml_ATT ('rel_h',      Items[i].Rel_H ),
        xml_ATT ('local_h',    Items[i].Clutter_H ),
        xml_ATT ('local_code', Items[i].Clutter_Code)
       ]);
       *)

  end;

  Result:= True;
end;



//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_Neighbors_calc_new111.LoadCalcResultsFromXML1(aID1,
    aID2: integer; aDBNeighbor: TnbNeighbor; aFileName: string);
//--------------------------------------------------------------------
var// iValue,i,j,iID: integer;
 // vNode,vGroup,vRoot,vRoot1: IXMLNode;
   eRx_Level_dBm,dSESR,dKng: double;
 //  dPolarization_Ratio: double;
 // eValue: Double;
  k: Integer;

 // oXMLDoc: TXMLDoc;//('');

//const
//  DEF_RX_LEVEL_DBM = 101; //��������� ������� ������� �� ����� ��������� (���)
//  DEF_SESR  = 17;
//  DEF_KNG   = 19;

var
  oLinkCalcResult: TLinkCalcResult;


begin
  oLinkCalcResult:=TLinkCalcResult.create;
  oLinkCalcResult.LoadFromXmlFile_RRL_Result(aFileName);
//  oLinkCalcResult.

//  oLinkCalcResult.

  eRx_Level_dBm:=oLinkCalcResult.Data.Rx_Level_dBm;
  dSESR:=oLinkCalcResult.Data.SESR;
  dKng:=oLinkCalcResult.Data.Kng;

//eRx_Level_dBm := eValue;
//dSESR := eValue;
//dKng  := eValue;
//

  FreeAndNil(oLinkCalcResult);

  //////////////////////////////////////////////////////

  {

  oXMLDoc:=TXMLDoc.Create;

  oXMLDoc.LoadFromFile(aFileName);

  vRoot:=oXMLDoc.DocumentElement;

  if vRoot.ChildNodes.Count>0 then
  begin
//    vRoot := oXMLDoc.DocumentElement;
    vRoot1:= vRoot.childNodes[0];

//    iID:= DoAdd_Neighbors(aID1, aID2);

    for i:=0 to vRoot1.ChildNodes.Count-1 do
    begin
      vGroup:=vRoot1.ChildNodes[i];

      for j:=0 to vGroup.ChildNodes.Count-1 do
      begin
        vNode:=vGroup.ChildNodes[j];

        iID   :=xml_GetIntAttr(vNode, 'id');
        eValue:=xml_GetFloatAttr(vNode, 'value');

        case iID of
          DEF_RX_LEVEL_DBM: eRx_Level_dBm := eValue;
          DEF_SESR        : dSESR := eValue;
          DEF_KNG         : dKng  := eValue;
        end;

//        if iID = DEF_RX_LEVEL_DBM then
  //        eRx_Level_dBm := (xml_GetFloatAttr(vNode, 'value')) else

//        if iID = DEF_SESR then
//          dSESR := (xml_GetFloatAttr(vNode, 'value')) else
//
//        if iID = DEF_KNG then
//          dKng := (xml_GetFloatAttr(vNode, 'value'));

      end;
    end;

 // oXMLDoc.Free;

  }

   {
    eRx_Level_dBm:=Round(eRx_Level_dBm);
    dSESR :=TruncFloat(dSESR, 5);
    dKng  :=TruncFloat(dKng,  5);
    }


  aDBNeighbor.SESR:=dSESR;
  aDBNeighbor.Kng:=dKng;
  aDBNeighbor.Rx_Level_dBm:=eRx_Level_dBm;

  aDBNeighbor.IsPropertyOne:=FIsPropertyOne;
  aDBNeighbor.IsLink       :=FIsLink;

  aDBNeighbor.Distanse_m   :=FDistanse_m;
  aDBNeighbor.Diagram_level:=FDiagram_level;



(*   iID:= DoAdd_Neighbors(aID1, aID2);

    gl_DB.UpdateRecord(TBL_LINKFREQPLAN_NEIGHBORS, iID,
                  [db_Par(FLD_RX_LEVEL_DBM,  eRx_Level_dBm), //POWER_dBm
                   db_Par(FLD_SESR,          dSESR),
                   db_Par(FLD_Kng,           dKng),

                   db_Par(FLD_IsPropertyOne, FIsPropertyOne),
                   db_Par(FLD_IsLink,        FIsLink),

                   db_Par(FLD_Distance_M,    FDistanse_m), // geo_Distance(FVector)),
                   db_Par(FLD_diagram_level, FDiagram_level)
                  ]);
*)
   k:=dmOnega_DB_data.ExecStoredProc(SP_LinkFreqPlan_Neighbors_add,
                  [
                   db_Par (FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID),

                 //  db_Par (FLD_INDEX, 2),


                   db_Par (FLD_TX_LINKEND_ID, aID1),
                   db_Par (FLD_RX_LINKEND_ID, aID2),
               //    db_Par (FLD_TX_LINK_ID, iID22),

                   db_Par(FLD_RX_LEVEL_DBM,  eRx_Level_dBm), //POWER_dBm
                   db_Par(FLD_SESR,          dSESR),
                   db_Par(FLD_Kng,           dKng),

                   db_Par(FLD_IsPropertyOne, FIsPropertyOne),
                   db_Par(FLD_IsLink,        FIsLink),

                   db_Par(FLD_Distance_M,    FDistanse_m), // geo_Distance(FVector)),
                   db_Par(FLD_diagram_level, FDiagram_level)
                  ]);
 // end;

end;



//--------------------------------------------------------------------
function TdmLinkFreqPlan_Neighbors_calc_new111.Execute(aLinkEnd_ID1, aLinkEnd_ID2: integer;
    aLinkEnd1,aLinkEnd2: TnbLinkFreqPlan_Linkend;   aDBNeighbor: TnbNeighbor; var aTerminated: boolean): boolean;
//--------------------------------------------------------------------
const

  TEMP_IN_FILENAME = 'rrl_neighb_in.xml';
  TEMP_OUT_FILENAME = 'rrl_neighb_res.xml';

  TEMP_IN_layout  = 'rrl_layout.txt';



var sFile1,sFile2: string;
    id_parent: integer;
  sFile_layout: string;
begin
  Result:=False;

  { TODO : clutters }
 // dmRel_Engine.OpenAll;

  if not OpenData_new (aLinkEnd1,aLinkEnd2, aTerminated) then
    Exit;


//  if not OpenData (aLinkEnd_ID1, aLinkEnd_ID2, aLinkEnd1,aLinkEnd2, aTerminated) then
 //   Exit;

(*  if FIsPropertyOne then
  begin
    if not FIsRange then FIsPropertyOne:= False; //��������� �� Prop �����������...

    id_parent:= DoAdd_Neighbors(aLinkEnd_ID1, aLinkEnd_ID2);
    gl_DB.UpdateRecord(TBL_LINK_FREQPLAN_NEIGHBORS, id_parent,
                [db_Par(FLD_IsPropertyOne, FIsPropertyOne),
                 db_Par(FLD_IsLink,        FIsLink),
                 db_Par(FLD_Distance,      0) ]);
  end
  else
  begin*)



(*    sFile1:=GetTempFileDir + TEMP_IN_FILENAME;
    sFile2:=GetTempFileDir + TEMP_OUT_FILENAME;
*)


  //  sIniFile := g_ApplicationDataDir + TEMP_INI_FILE;
                                            
    sFile1:=g_ApplicationDataDir + TEMP_IN_FILENAME;
    sFile2:=g_ApplicationDataDir + TEMP_OUT_FILENAME;
    sFile_layout :=g_ApplicationDataDir + TEMP_IN_layout;



    if not SaveToXML (sFile1, aLinkEnd1,aLinkEnd2) then
      Exit;



  CalcMethod_LoadFromXMLFile(
      GetApplicationDir + 'link_calc_model_setup\RRL_ParamSys.xml',
      0, //oCalcParams.Calc_method,
      sFile_layout
      );


    Run_rpls_rrl  (sFile1, sFile2, sFile_layout);



/////////    ShellExec_Notepad(sFile1);


{
    if RunApp (GetApplicationDir() + EXE_RPLS_RRL,
                  DoubleQuotedStr(sFile1) +' '+
                  DoubleQuotedStr(sFile2))
    then
 }

 
    LoadCalcResultsFromXML1 (aLinkEnd_ID1, aLinkEnd_ID2, aDBNeighbor,  sFile2);

///  end;
  Result:=True;
end;




// ---------------------------------------------------------------
function TdmLinkFreqPlan_Neighbors_calc_new111.Run_rpls_rrl(aFile1, aFile2,
    aFile_layout: string): Boolean;
// ---------------------------------------------------------------
var
  sRunPath: string;

  oIni_rpls_rrl_params: TIni_rpls_rrl_params;
  sFile: string;

begin
  Assert(aFile1<>'');
  Assert(aFile2<>'');
  Assert(aFile_layout<>'');


  oIni_rpls_rrl_params:=TIni_rpls_rrl_params.Create;


  sRunPath := GetApplicationDir() + EXE_RPLS_RRL;

  Result := True;

  {$DEFINE use_dll111}

  {$IFDEF use_dll}
   {
    if Load_Irpls_rrl() then
    begin
    //  Irpls_rrl.InitADO(dmMain.ADOConnection1.ConnectionObject);
   //   ILink_graph.InitAppHandle(Application.Handle);
      Irpls_rrl.Execute(sFile);

      UnLoad_Irpls_rrl();
    end;
    }
  //  Exit;

  {$ELSE}

    oIni_rpls_rrl_params.In_FileName := aFile1;
    oIni_rpls_rrl_params.out_FileName := aFile2;
    oIni_rpls_rrl_params.OUTREZ_FileName := aFile_layout;


    sFile :=g_ApplicationDataDir + 'rrl_nb.ini';

    oIni_rpls_rrl_params.SaveToFile(sFile);

    Result := RunAppEx (sRunPath,  [sFile]);

 //   Result := RunAppEx (sRunPath,  [aFile1, aFile2, '', aFile_layout]);

//                      DoubleQuotedStr(aFile1) +' '+
  //                    DoubleQuotedStr(aFile2)) ;

//    Result := RunApp (sRunPath,
//                      DoubleQuotedStr(aFile1) +' '+
//                      DoubleQuotedStr(aFile2)) ;


  //  if RunApp (GetApplicationDir() + EXE_RPLS_RRL, sFile1 +' '+ aFile2) then


  {$ENDIF}

  FreeAndNil(oIni_rpls_rrl_params);

end;



begin

end.

