unit dm_Main_app__NB_calc;

interface

uses
  SysUtils, Classes, Dialogs, 

  u_vars,

  u_ini_LinkFreqPlan_nb_calc,

  dm_LinkFreqPlan_Neighbors_calc,
 // dm_LinkFreqPlan_Neighbors_calc_NEW,
  dm_LinkFreqPlan_Neighbors,

  dm_Main
  ;

type
  TdmMain_app__NB_calc = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);

  end;


var
  dmMain_app__NB_calc: TdmMain_app__NB_calc;

//===================================================================
implementation {$R *.DFM}
//===================================================================


//-------------------------------------------------------------------
procedure TdmMain_app__NB_calc.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
const
  DEF_TEMP_FILENAME = 'LinkFreqPlan_NB_Calc.ini';


var
 // oInifile: TIniFile;

  sIniFileName: string;

(*
  iProjectID: Integer;
  iLinkFreqPlanID: integer;
  bIsFullCalc: boolean;
*)

  oParams: TIni_LinkFreqPlan_nb_calc_Params;

begin
  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
    sIniFileName:=g_ApplicationDataDir + DEF_TEMP_FILENAME;


  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;


  oParams:=TIni_LinkFreqPlan_nb_calc_Params.Create;
  oParams.LoadFromFile(sIniFileName);


(*
  oInifile:=TIniFile.Create (sIniFileName);

  iLinkFreqPlanID  :=oInifile.ReadInteger('main', 'LinkFreqPlanID',  0);
  bIsFullCalc      :=oInifile.ReadBool   ('main', 'IsFullCalc',  False);
  iProjectID       :=oInifile.ReadInteger('main', 'ProjectID',  0);

  oInifile.Free;
*)

 // Assert (iProjectID<>0);
 // Assert (iLinkFreqPlanID<>0);


  TdmMain.Init;

// assert (oParams.ConnectionString <> '');

//  dmMain.Open_ADOConnection (oParams.ConnectionString);


//  dmMain.Open_ADOConnection(oParams.)

  if not dmMain.OpenDB_reg  then
    Exit;

  dmMain.ProjectID:=oParams.ProjectID;

  TdmLinkFreqPlan_Neighbors_calc.Init;
//  TdmLinkFreqPlan_Neighbors_calc_new.Init;


  with dmLinkFreqPlan_Neighbors do
  begin
    Params.LinkFreqPlan_ID:= oParams.LinkFreqPlanID;
    Params.IsTotalCalc    := oParams.IsFullCalc;

    ExecuteDlg ('��� ���');
//    ExecuteProc;
  end;


  FreeAndNil(oParams);

end;


end.
