unit f_Data;

interface

uses
  Classes, Controls, Forms,
  DBGrids, ComCtrls, Grids;

type
  Tfrm_Data = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
//    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class procedure ExecDlg;

  end;

var
  frm_Data: Tfrm_Data;

implementation



{$R *.dfm}

class procedure Tfrm_Data.ExecDlg;
begin
  with Tfrm_Data.Create(Application) do
  begin

    ShowModal;

    Free;
  end;

end;



end.
