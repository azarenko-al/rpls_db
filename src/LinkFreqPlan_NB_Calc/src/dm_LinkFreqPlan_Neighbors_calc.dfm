inherited dmLinkFreqPlan_Neighbors_calc: TdmLinkFreqPlan_Neighbors_calc
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 1152
  Top = 309
  Height = 272
  Width = 448
  object qry_LinkEnd1: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT    *'
      'FROM      Property'
      'ORDER BY name')
    Left = 28
    Top = 84
  end
  object qry_Antennas1: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT     dbo.LinkEndAntenna.*, dbo.AntennaType.*'
      'FROM         dbo.AntennaType RIGHT OUTER JOIN'
      
        '                      dbo.LinkEndAntenna ON dbo.AntennaType.id =' +
        ' dbo.LinkEndAntenna.antennaType_id')
    Left = 28
    Top = 152
  end
  object qry_LinkEnd2: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT    *'
      'FROM      Property'
      'ORDER BY name')
    Left = 108
    Top = 84
  end
  object qry_Antennas2: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT     dbo.LinkEndAntenna.*, dbo.AntennaType.*'
      'FROM         dbo.AntennaType RIGHT OUTER JOIN'
      
        '                      dbo.LinkEndAntenna ON dbo.AntennaType.id =' +
        ' dbo.LinkEndAntenna.antennaType_id')
    Left = 112
    Top = 152
  end
  object qry_LinkType: TADOQuery
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'id'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 5
      end>
    SQL.Strings = (
      'select * from link where id=:id')
    Left = 28
    Top = 16
  end
  object qry_LinkEndType: TADOQuery
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'id'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 5
      end>
    SQL.Strings = (
      'select * from link where id=:id')
    Left = 112
    Top = 12
  end
  object qry_Temp: TADOQuery
    Parameters = <>
    Left = 224
    Top = 16
  end
  object qry_Mask: TADOQuery
    Parameters = <>
    Left = 308
    Top = 20
  end
end
