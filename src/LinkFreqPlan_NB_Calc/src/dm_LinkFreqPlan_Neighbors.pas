unit dm_LinkFreqPlan_Neighbors;

interface

{$DEFINE db}

uses
  SysUtils, Classes, Forms, Db, ADODB, math, Variants, Dialogs,

  dm_Progress,


  dm_Rel_Engine,

  

  

/////////////  dm_LinkFreqPlan_Neighbors_calc_new,

  dm_Onega_DB_data,

  u_LinkFreqPlan_classes,

 // u_dev_tools,

  dm_Main,

  dm_LinkFreqPlan_Neighbors_calc,
  dm_LinkEndType,
//  dm_LinkFreqPlan_Tools,

  u_const_db,
//  u_const,
//  u_const_msg,

  u_LinkFreqPlan_const,


  

  u_log,
  u_func,

//  u_func_msg,

  u_db
  ;

type
  TGet_Freq_Spacing_MHz_rec=  record
    CI_treb,
    CI_0,
    Degradation_treb,
    Degradation_calc,
    PowerNeighbors,
    PowerNoise: double;

    LinkEnd1_ID,
    LinkEnd2_ID: Integer;


    Tx_LinkEnd_ID,
    Rx_LinkEnd_ID: Integer;

    LinkEnd1, aLinkEnd2: TnbLinkFreqPlan_Linkend;

  end;



  TdmLinkFreqPlan_Neighbors = class(TdmProgress)
    qry_FreqPlan_LinkEnd: TADOQuery;
    qry_Neighbors: TADOQuery;
    qry_LinkEnd1: TADOQuery;
    qry_LinkEnd2: TADOQuery;
    qry_Mode1: TADOQuery;
    qry_Mode2: TADOQuery;
    qry_Band1: TADOQuery;
    qry_LinkFreqPlan: TADOQuery;
    qry_Antennas: TADOQuery;
    ds_FreqPlan_LinkEnd: TDataSource;
    ds_Antennas: TDataSource;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
 //   Flog: TStingList;

//    FLinkFreqPlan: TLinkFreqPlan;
//    FNeighbors: TnbNeighborList;

    Fdata: TnbData;


//    FAntennaList: TnbAntennaList;
//    FLinkFreqPlan_Linkends: TnbLinkFreqPlan_Linkend_List;

//    FLinkFreqPlan_ID: integer;

    procedure Calc_Degradation;

//    procedure Open;
    function Neighbors_Calc (): boolean;

    function Get_Freq_Spacing_MHz(
        aRec: TGet_Freq_Spacing_MHz_rec;

        aCI_treb, aCI_0,
        aDegradation_treb,
        aDegradation_calc,
        aPowerNeighbors,
        aPowerNoise: double;

        aLinkEnd1_ID, aLinkEnd2_ID: Integer;

        aLinkEnd1, aLinkEnd2:  TnbLinkFreqPlan_Linkend
        ): double;

    procedure Log(aValue: string);

    function OpenData: Boolean;


  public
    Params: record
      LinkFreqPlan_ID: integer;
      IsTotalCalc: boolean;
    end;

    procedure ExecuteProc; override;
    procedure IsNeighbors(aLinkFreqPlan_ID: integer);


  end;


function dmLinkFreqPlan_Neighbors: TdmLinkFreqPlan_Neighbors;


//==============================================================
//==============================================================
implementation


{$R *.DFM}

var
  FdmLinkFreqPlan_Neighbors: TdmLinkFreqPlan_Neighbors;

(*const
  FLD_level_tx_a = 'level_tx_a';
  FLD_level_rx_a = 'level_rx_a';

*)

// ---------------------------------------------------------------
function dmLinkFreqPlan_Neighbors: TdmLinkFreqPlan_Neighbors;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLinkFreqPlan_Neighbors) then
    FdmLinkFreqPlan_Neighbors := TdmLinkFreqPlan_Neighbors.Create(Application);

   Result := FdmLinkFreqPlan_Neighbors;
end;


//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_Neighbors.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
  db_SetComponentADOConnection (Self, dmMain.ADOConnection);

//  db_CreateField (mem_linkend,  [db_Field(FLD_ID, ftInteger)]);

//  FLinkFreqPlan := TLinkFreqPlan.Create();
//  FNeighbors := TnbNeighborList.Create();
//  FLinkFreqPlan_Linkends := TnbLinkFreqPlan_Linkend_List.Create();
//  FAntennaList := TnbAntennaList.Create();


  FData:=TnbData.Create;


//  Flog:=TStingList.Create();

end;


procedure TdmLinkFreqPlan_Neighbors.DataModuleDestroy(Sender: TObject);
begin
//  FreeAndNil(FAntennaList);
// .. FreeAndNil(FLinkFreqPlan_Linkends);
//  FreeAndNil(FNeighbors);
//  FreeAndNil(FLinkFreqPlan);

  FreeAndNil(FData);
              

//  FreeAndNil(Flog);

end;


//--------------------------------------------------------------------
function TdmLinkFreqPlan_Neighbors.Neighbors_Calc (): boolean;
//--------------------------------------------------------------------
var i, iID, iOldID: integer;
 // I: Integer;
    sFileName: string;
  oLinkend: TnbLinkFreqPlan_Linkend;
  j: integer;
  iID1: integer;
  iID2: integer;

  oDBNeighbor: TnbNeighbor;

begin
  dmLinkFreqPlan_Neighbors_calc.Params.LinkFreqPlan_ID:= Params.LinkFreqPlan_ID;
///////  dmLinkFreqPlan_Neighbors_calc_new.Params.LinkFreqPlan_ID:= Params.LinkFreqPlan_ID;

  dmLinkFreqPlan_Neighbors_calc.Params.Calc_Distance_m:=
//     1000 * FLinkFreqPlan.MaxCalcDistance_km;
     1000 * FData.LinkFreqPlan.MaxCalcDistance_km;


//  dmLinkFreqPlan_Neighbors_calc_new.Params.Calc_Distance_m:=
//     1000 * FData.LinkFreqPlan.MaxCalcDistance_km;


  Result:= False;
  Terminated:=False;

  DoProgressMsg2('������ �������������� �������������');

  // { TODO 1 : ->SP }
//  gl_DB.DeleteRecordsByFieldValue
 //    (TBL_LINKFREQPLAN_NEIGHBORS, FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID);

 // dmLinkFreqPlan_Tools.Clear_Interference(FLinkFreqPlan_ID);

  dmOnega_DB_data.LinkFreqPlan_Tools(Params.LinkFreqPlan_ID, DEF_Clear_NEIGHBORS1);
  dmOnega_DB_data.LinkFreqPlan_Tools(Params.LinkFreqPlan_ID, DEF_Clear_Interference);


//
  dmRel_Engine.Open;//All;


  // i - ���, ���� �����
  // j - ���, ��� �����

  for I := 0 to FData.Linkends.Count - 1 do
//  for I := 50 to FLinkFreqPlan_Linkends.Count - 1 do
    if not Terminated then
  begin
    DoProgress(i, FData.Linkends.Count);

    for j := 0 to FData.Linkends.Count - 1 do
      if (i<>j) and (not Terminated) then
    begin
      DoProgress(j, FData.Linkends.Count);

      iID1:=FData.Linkends[i].Linkend_id;
      iID2:=FData.Linkends[j].Linkend_id;

      Assert(iID1<>iID2);

      oDBNeighbor:=FData.Neighbors.AddItem ();
//      oDBNeighbor.LINKEND_ID := iID1;
//      oDBNeighbor.LINKEND_ID := iID2;

      oDBNeighbor.LINKEND1_ID := iID1;
      oDBNeighbor.LINKEND2_ID := iID2;


//      if (iID1=3378) and (iID2=5119) then
//        iID1:=3378;


      oDBNeighbor.LINKEND1 := FData.Linkends[i];
      oDBNeighbor.LINKEND2 := FData.Linkends[j];



(*    for j := 0 to FNeighbors.Count - 1 do
      if FNeighbors[j].RX_LINK_ID = oDBLinkFreqPlan_Linkend.LINKEND_ID  then
    begin
      oDBNeighbor:=FNeighbors[j];
*)

     {

      dmLinkFreqPlan_Neighbors_calc.Execute(
           iID1, iID2,
           FData.Linkends[i],
           FData.Linkends[j],

           oDBNeighbor,

           Terminated);
         }
//      if iID1 <> iID2 then

    //  if FData.Linkends[i].PROPERTY_ID <> FData.Linkends[j].PROPERTY_ID
     // then
//      begin
        dmLinkFreqPlan_Neighbors_calc.Execute(
           iID1, iID2,
           FData.Linkends[i],
           FData.Linkends[j],

           oDBNeighbor,

           Fdata,


           Terminated);


//      if oDBNeighbor.IsPropertyOne  then
//        Assert ( 1=1 );


{      if oDBNeighbor.IsLink then
      begin
        Assert ( FData.Linkends[i].Link_id = FData.Linkends[j].Link_id );
        assert (oDBNeighbor.Distanse_m>1);

      end;
}

//      if oDBNeighbor.Distanse_m_ < 1 then
//       oDBNeighbor.Distanse_m_ := 1;


//       oDBNeighbor.di


//      if oDBNeighbor.IsLink then

//       if oDBNeighbor.Distanse_m_  =0 then
//         ShowMessage('');

//      assert (oDBNeighbor.Distanse_m_>0);

{      if (iID1=3378) and (iID2=5119) then
        assert (oDBNeighbor.Distanse_m>0);
//        iID1:=3378;
}



   dmOnega_DB_data.ExecStoredProc(SP_LinkFreqPlan_Neighbors_add,
                  [
                   db_Par (FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID),

                   db_Par (FLD_LINKEND1_ID, oDBNeighbor.LINKEND1_ID),
                   db_Par (FLD_LINKEND2_ID, oDBNeighbor.LINKEND2_ID),

             //      db_Par (FLD_TX_LINKEND_ID, oDBNeighbor.LINKEND1_ID),
             //      db_Par (FLD_RX_LINKEND_ID, oDBNeighbor.LINKEND2_ID),

               //    db_Par (FLD_TX_LINK_ID, iID22),

                   db_Par(FLD_RX_LEVEL_DBM,  IIF(oDBNeighbor.IsPropertyOne, null, oDBNeighbor.Rx_Level_dBm)), //POWER_dBm
            //       db_Par(FLD_SESR,          IIF(oDBNeighbor.IsPropertyOne, null, oDBNeighbor.SESR)),
             //      db_Par(FLD_Kng,           IIF(oDBNeighbor.IsPropertyOne, null, oDBNeighbor.Kng)),

                   db_Par(FLD_IsPropertyOne, oDBNeighbor.IsPropertyOne),
                   db_Par(FLD_IsLink,        oDBNeighbor.IsLink),

                   db_Par(FLD_Distance_M,    round(oDBNeighbor.Distanse_m)), // geo_Distance(FVector)),
                   db_Par(FLD_diagram_level, IIF(oDBNeighbor.IsPropertyOne, null, oDBNeighbor.Diagram_level))
                  ]);


  //    end ;
      {
       else

        dmOnega_DB_data.ExecStoredProc(SP_LinkFreqPlan_Neighbors_add,
                  [
                   db_Par (FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID),

                   db_Par (FLD_TX_LINKEND_ID, oDBNeighbor.TX_LINKEND_ID),
                   db_Par (FLD_RX_LINKEND_ID, oDBNeighbor.RX_LINKEND_ID),
               //    db_Par (FLD_TX_LINK_ID, iID22),

                //   db_Par(FLD_RX_LEVEL_DBM,  eRx_Level_dBm), //POWER_dBm
                //   db_Par(FLD_SESR,          dSESR),
                 //  db_Par(FLD_Kng,           dKng),

                   db_Par(FLD_IsPropertyOne, True)
                 //  db_Par(FLD_IsLink,        FIsLink),

                  // db_Par(FLD_Distance_M,    FDistanse_m), // geo_Distance(FVector)),
                   //db_Par(FLD_diagram_level, FDiagram_level)
                  ]);
      }


     {
      dmLinkFreqPlan_Neighbors_calc_new.Execute(
         iID1, iID2,
         FLinkFreqPlan_Linkends[i],
         FLinkFreqPlan_Linkends[j],

         Terminated);
      }

    end;
  end;

  //-------------------------------------------------------------------

  {

  qry_FreqPlan_LinkEnd.Filtered:= False;

  with mem_linkend do
     if RecordCount>0 then
   for i:=0 to RecordCount-1 do
      if not Terminated then
   begin
      DoProgress(i+1, RecordCount);

      RecNo:= i;
      iID:= FieldValues[FLD_ID];

      qry_FreqPlan_LinkEnd.First;
      while not qry_FreqPlan_LinkEnd.Eof do
        if not Terminated then
      begin
        oLinkend:=TDBLinkFreqPlan_Linkend.Create(nil);
        oLinkend.LoadFromDataset(qry_FreqPlan_LinkEnd);
        oLinkend.Free;


        DoProgress2(qry_FreqPlan_LinkEnd.RecNo, qry_FreqPlan_LinkEnd.RecordCount);

        if iID<>qry_FreqPlan_LinkEnd.FieldValues[FLD_LINKEND_ID] then
          dmLinkFreqPlan_Neighbors_calc.Execute(iID, qry_FreqPlan_LinkEnd[FLD_LINKEND_ID], Terminated);

        qry_FreqPlan_LinkEnd.Next;
      end
       else Break;
    end;

  }

  Result:= not Terminated;

  if not Result then
{
  if not Terminated then
    Result:= True
  else
}
 // gl_DB.DeleteRecordsByFieldValue(TBL_LINKFREQPLAN_NEIGHBORS,
  //                        FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID);
end;

//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_Neighbors.IsNeighbors(aLinkFreqPlan_ID: integer);
//--------------------------------------------------------------------
const
{
  SQL_SELECT11111 =
   'SELECT LinkEndType_Mode.bandwidth AS bandwidth,                           '+
   '       LinkEndType.coeff_noise AS coeff_noise, LinkFreqPlan_Neighbors.*   '+
   ' FROM LinkEnd LEFT OUTER JOIN                                             '+
   '      LinkEndType_Mode LEFT OUTER JOIN                                    '+
   '      LinkEndType ON LinkEndType_Mode.LinkEndType_id = LinkEndType.id ON '+
   '      LinkEnd.linkendtype_id = LinkEndType_Mode.LinkEndType_id AND      '+
   '      LinkEnd.mode = LinkEndType_Mode.mode RIGHT OUTER JOIN               '+
   '      LinkFreqPlan_Neighbors ON LinkEnd.id = LinkFreqPlan_Neighbors.rx_linkend_id '+
   ' WHERE LinkFreqPlan_id=:LinkFreqPlan_id                                 ';
   }


  view_LINKFREQPLAN_Calc_NEIGHBORS = 'view_LINKFREQPLAN_Calc_NEIGHBORS';

  SQL_SELECT_LINKFREQPLAN_NEIGHBORS =
    'SELECT * FROM  '+  view_LINKFREQPLAN_Calc_NEIGHBORS +
    ' WHERE LinkFreqPlan_id=:LinkFreqPlan_id';

//
//  SQL_SELECT111 =
//'SELECT     dbo.LinkFreqPlan_Neighbors.*, dbo.LinkEndType_Mode.bandwidth, dbo.LinkEndType.coeff_noise '+
//'FROM         dbo.LinkEnd LEFT OUTER JOIN '+
//'             dbo.LinkEndType_Mode ON dbo.LinkEnd.LinkEndType_mode_id = dbo.LinkEndType_Mode.id LEFT OUTER JOIN '+
//'             dbo.LinkEndType ON dbo.LinkEnd.linkendtype_id = dbo.LinkEndType.id RIGHT OUTER JOIN '+
//'             dbo.LinkFreqPlan_Neighbors ON dbo.LinkEnd.id = dbo.LinkFreqPlan_Neighbors.rx_linkend_id '+
//' WHERE LinkFreqPlan_id=:LinkFreqPlan_id                                 ';
//


var
  b: Boolean;
  dPower_Neighbors111: Double;
  I: Integer;
  dRX_LEVEL_dBm, dCI_treb, dCI_calc, dd, dFreqSpacing_MHZ, dDegdadation_treb: double;
  dPower_Noise, dPower_Neighbors, dDegradation_calc : double;

  isNeigh: boolean;
  iSiteFreqSpacing: integer;

  oLinkEnd_tx,oLinkEnd_rx: TnbLinkFreqPlan_Linkend;

  obNeighbor: TnbNeighbor;
  obNeighbor1: TnbNeighbor;

  e: Double;
  e1: Double;
  e2: Double;
  e3: Double;
  e_: Double;
  i1: integer;
  i2: integer;
  iID: Integer;
  iRes: Integer;
  iRX_LINKEND_ID: Integer;
  k: Integer;

  rec: TGet_Freq_Spacing_MHz_rec;

  //oDBNeighbor: TDBNeighbor;

  oLINKEND1: TnbLinkFreqPlan_Linkend;
  oLINKEND2: TnbLinkFreqPlan_Linkend;

  oNext_for_LINKEND1: TnbLinkFreqPlan_Linkend;

begin
  if Terminated then
    Exit;

  DoProgressMsg2('������ ��������� �� CI');


  dCI_treb         :=FData.LinkFreqPlan.MIN_CI_dB;
  iSiteFreqSpacing :=FData.LinkFreqPlan.SiteFreqSpacing;
  dDegdadation_treb:=FData.LinkFreqPlan.Threshold_degradation_db;

{
  dCI_treb         := gl_DB.GetDoubleFieldValue(TBL_LINK_FREQPLAN, FLD_CI,
                                [db_par(FLD_ID, aLinkFreqPlan_ID)]);
  iSiteFreqSpacing := gl_DB.GetIntFieldValue(TBL_LINK_FREQPLAN, FLD_SiteFreqSpacing,
                                [db_par(FLD_ID, aLinkFreqPlan_ID)]);
  dDegdadation_treb:= gl_DB.GetDoubleFieldValue(TBL_LINK_FREQPLAN, FLD_THRESHOLD_DEGRADATION,
                                [db_par(FLD_ID, aLinkFreqPlan_ID)]);
}


//  'SELECT * FROM '
  dmOnega_DB_data.OpenQuery(qry_Neighbors, SQL_SELECT_LINKFREQPLAN_NEIGHBORS,
                              [FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID]);

  if qry_Neighbors.RecordCount=0 then
  begin
    ShowMessage('������ �� ����������.');
    exit;
  end;



  FData.Neighbors.LoadFromDataset(qry_Neighbors);



  //-------------------------------------------------------------------

  qry_Neighbors.First;

 // db_ViewDataSet(qry_Neighbors);

  Assert(FData.Neighbors.Count  > 0 );


/////////////  Assert(FData.Neighbors.Count = ( Sqr( FData.Linkends.Count)  - FData.Linkends.Count));




  with qry_Neighbors do
    while (not Eof) and (not Terminated) do
  begin
    DoProgress2(RecNo, RecordCount);

    iID:=FieldValues[FLD_ID];


    obNeighbor := FData.Neighbors.FindIByID(iID);
    Assert(Assigned(obNeighbor));

    if obNeighbor.IsPropertyOne then
    begin
      Next;
      Continue;
    end;


   oLINKEND1:=FData.Linkends.FindByLinkendID(obNeighbor.LINKEND1_ID);
   oLINKEND2:=FData.Linkends.FindByLinkendID(obNeighbor.LINKEND2_ID);

   if (oLinkEnd1.Name='1') and (oLinkEnd2.Name='4') then
       k:=0;



   oNext_for_LINKEND1:=FData.Linkends.FindByLinkendID(oLINKEND1.Next_Linkend_id);

//    obNeighbor.LINKEND2:=FData.Linkends.FindByLinkendID(obNeighbor.LINKEND2_ID);


    b:=obNeighbor.IsLink;


    if (not FieldByName(FLD_IsLink).AsBoolean) then
    // -------------------------------------
    // �� ������ �� ���������
    // -------------------------------------
    begin
      iRX_LINKEND_ID:=obNeighbor.LINKEND2_ID;

      iRX_LINKEND_ID:=FieldValues[FLD_RX_LINKEND_ID];

  //    obNeighbor1 := FData.Neighbors.FindItemByLinkendID_IsLink(iRX_LINKEND_ID);




//      if (obNeighbor1.LinkEnd1_Name='2') and (obNeighbor1.LinkEnd2_Name='1') then
  //       k:=0;


   //   if not Assigned(obNeighbor1) then
    //    Assert(Assigned(obNeighbor1), 'RX_LINKEND_ID: '+ IntTostr(iRX_LINKEND_ID));

        //{$DEFINE db}

      e_:=obNeighbor.RX_LEVEL_DBM;



      dRX_LEVEL_dBm:= gl_DB.GetDoubleFieldValue(TBL_LINKFREQPLAN_NEIGHBORS, FLD_RX_LEVEL_dBm, //FLD_POWER_dBm,
                     [db_par(FLD_RX_LINKEND_ID,     oNext_for_LINKEND1.Linkend_id),
                      db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID),
                      db_par(FLD_IsLink,           True) ]);



      if dRX_LEVEL_dBm<>0 then
      begin
        e_:=obNeighbor.RX_LEVEL_DBM;

        e:=FieldByName(FLD_RX_LEVEL_dBm).AsFloat; //test

        dCI_calc:= dRX_LEVEL_dBm -
                  (obNeighbor.RX_LEVEL_DBM - obNeighbor.DIAGRAM_LEVEL + obNeighbor.EXTRA_LOSS);


//        dCI_calc:= dRX_LEVEL_dBm - (FieldByName(FLD_POWER_dBm).AsFloat -
        dCI_calc:= dRX_LEVEL_dBm - (FieldByName(FLD_RX_LEVEL_dBm).AsFloat -
                             FieldByName(FLD_DIAGRAM_LEVEL).AsFloat +
                             FieldByName(FLD_EXTRA_LOSS).AsFloat);
        isNeigh:= dCI_treb > dCI_calc;

      end else
        isNeigh:= False;


      dPower_Neighbors111:= obNeighbor.RX_LEVEL_DBM -
                         obNeighbor.DIAGRAM_LEVEL +
                         obNeighbor.EXTRA_LOSS;


      e1:=FieldByName(FLD_RX_LEVEL_dBm).AsFloat ;
                         //FieldByName(FLD_POWER_dBm).AsFloat -
      e2:=FieldByName(FLD_DIAGRAM_LEVEL).AsFloat ;
      e3:=FieldByName(FLD_EXTRA_LOSS).AsFloat;


      dPower_Neighbors:=FieldByName(FLD_RX_LEVEL_dBm).AsFloat -
                         //FieldByName(FLD_POWER_dBm).AsFloat -
                         FieldByName(FLD_DIAGRAM_LEVEL).AsFloat +
                         FieldByName(FLD_EXTRA_LOSS).AsFloat;

//      Assert (dPower_Neighbors111 = dPower_Neighbors);

      Assert(FieldByName(FLD_BANDWIDTH).AsFloat <> 0);

      
        dPower_Noise:= obNeighbor.Rx_LinkEndType__coeff_noise  +
          10*log10(1.38*Power(10, -14)*300* obNeighbor.Rx_Mode__bandwidth);


        dPower_Noise:= TruncFloat(FieldByName(FLD_COEFF_NOISE).AsFloat +
          10*log10(1.38*Power(10, -14)*300* FieldByName(FLD_BANDWIDTH).AsFloat));

        dDegradation_calc:= TruncFloat(10*log10(Power(10, 0.1*dPower_Noise) +
                        Power(10, 0.1*dPower_Neighbors))-dPower_Noise);
   //   end;


      if (FieldByName(FLD_IsPropertyOne).AsBoolean) and
         (iSiteFreqSpacing > 0)
      then
        isNeigh:= True
      else
        isNeigh:= (isNeigh or (dDegdadation_treb < dDegradation_calc));


      i1:=qry_Neighbors[FLD_TX_LINKEND_ID];
      i2:=qry_Neighbors[FLD_RX_LINKEND_ID];

      oLinkEnd_tx:=FData.Linkends.FindByLinkendID(qry_Neighbors[FLD_TX_LINKEND_ID]);
      oLinkEnd_rx:=FData.Linkends.FindByLinkendID(qry_Neighbors[FLD_RX_LINKEND_ID]);

      Assert(Assigned(oLinkEnd_tx), 'Value not assigned');
      Assert(Assigned(oLinkEnd_rx), 'Value not assigned');

(*    function Get_Freq_Spacing_MHz(aRec: TGet_Freq_Spacing_MHz_rec;
        aCI_treb, aCI_0,
        aDegradation_treb,
        aDegradation_calc,
        aPowerNeighbors,
        aPowerNoise: double;
*)


      FillChar(rec, sizeOf(rec), 0);

      rec.CI_treb := dCI_treb;
      rec.CI_0 := dCI_calc;
      rec.Degradation_treb := dDegdadation_treb;
      rec.Degradation_calc := dDegradation_calc;
      rec.PowerNeighbors := dPower_Neighbors;
      rec.PowerNoise := dPower_Noise;

      rec.LINKEND1_ID := obNeighbor.LINKEND1_ID;
      rec.LINKEND2_ID := obNeighbor.LINKEND2_ID;


      rec.TX_LINKEND_ID := qry_Neighbors[FLD_TX_LINKEND_ID];
      rec.RX_LINKEND_ID := qry_Neighbors[FLD_RX_LINKEND_ID];


      dFreqSpacing_MHZ:= Get_Freq_Spacing_MHz(rec,

                        dCI_treb, dCI_calc, dDegdadation_treb,
                        dDegradation_calc, dPower_Neighbors, dPower_Noise,
                        qry_Neighbors[FLD_TX_LINKEND_ID],
                        qry_Neighbors[FLD_RX_LINKEND_ID],

                        oLinkEnd_tx,
                        oLinkEnd_rx
                      );
    end
    else
      begin
        // ��������� �� ���������

        isNeigh := True;
        dCI_calc:= 0;
        dDegradation_calc:=0;
        dPower_Noise     := 255;
        dFreqSpacing_MHZ := 0;
      end;

 (*   Assert(qry_Neighbors.recNo>0, 'Value <=0');

    oDBNeighbor:=FNeighbors[qry_Neighbors.recNo-1];


    Assert(isNeigh = oDBNeighbor.IsNeigh);
  *)


   obNeighbor.IsNeigh1         := isNeigh;
   obNeighbor.POWER_NOISE     := dPower_Noise;
   obNeighbor.FREQ_SPACING_MHz:= dFreqSpacing_MHZ;
   obNeighbor.CI              := dCI_calc;



   iRes :=dmOnega_DB_data.ExecStoredProc(SP_LinkFreqPlan_Neighbors_update,
                  [
                    db_par(FLD_ID,                 qry_Neighbors[FLD_ID]),

                    db_par(FLD_IsNeigh,            isNeigh),
                    db_par(FLD_CI,                 dCI_calc),
                    db_par(FLD_Threshold_degradation, dDegradation_calc),
                    db_par(FLD_POWER_NOISE,        IIF(dPower_Noise=255, null, dPower_Noise)),
                    db_par(FLD_FREQ_SPACING_MHZ,   dFreqSpacing_MHZ)
                  ]);


    Next;
  end;
end;

//--------------------------------------------------------------------
function TdmLinkFreqPlan_Neighbors.Get_Freq_Spacing_MHz(aRec:
    TGet_Freq_Spacing_MHz_rec; aCI_treb, aCI_0, aDegradation_treb,
    aDegradation_calc, aPowerNeighbors, aPowerNoise: double;

    aLinkEnd1_ID, aLinkEnd2_ID: Integer;

    aLinkEnd1, aLinkEnd2: TnbLinkFreqPlan_Linkend):   double;
//--------------------------------------------------------------------

   // ---------------------------------------------------------------
   procedure Do_Calc_Param_A_B(var aA1, aA2, aB1, aB2: double);
   // ---------------------------------------------------------------
   const
     DEF_MIN = 1E-10;

   var iLevel_TX_a, iLevel_RX_a: integer;
       dValue_BANDWIDTH_30, dValue_BANDWIDTH_3, dValue_BANDWIDTH_A, dDeltaF : double;
       e: Double;
   begin
     aA1:=0; aA2:=0; aB1:=0; aB2:=0;

     // -------------------------
     //for TX
     // -------------------------
//     dDeltaF_ := aLinkEnd1.BANDWIDTH;
     dDeltaF := qry_Mode1.FieldByName(FLD_BANDWIDTH).AsFloat;

 //    assert (dDeltaF_=dDeltaF);

//     iLevel_TX_a_ := Abs(aLinkEnd1.LinkEndType.level_tx_a);

     iLevel_TX_a:= Abs(dmLinkEndType.GetIntFieldValue(
                     qry_LinkEnd1.FieldByName(FLD_LINKENDTYPE_ID).AsInteger,
                               FLD_level_tx_a));

//     assert (iLevel_TX_a_=iLevel_TX_a);


//     dValue_BANDWIDTH_A_ := aLinkEnd1.BANDWIDTH_TX_A;
     dValue_BANDWIDTH_A := qry_Mode1.FieldByName(FLD_BANDWIDTH_TX_A).AsFloat;

  //   Assert (dValue_BANDWIDTH_A_ = dValue_BANDWIDTH_A);


//     if (iLevel_TX_a <> 0) and (dValue_BANDWIDTH_A <> 0) then
     if (iLevel_TX_a > 0) and (dValue_BANDWIDTH_A > 0) then
     begin
       dValue_BANDWIDTH_30 := aLinkEnd1.BANDWIDTH_TX_30;
       dValue_BANDWIDTH_30:= qry_Mode1.FieldByName(FLD_BANDWIDTH_TX_30).AsFloat;

       if dValue_BANDWIDTH_30 > 0 then
       begin
         if Abs(dValue_BANDWIDTH_A-dValue_BANDWIDTH_30) < 1E-10  then
           e:=1E-10 //log10(dValue_BANDWIDTH_A/dValue_BANDWIDTH_30)
         else
           e:=log10(dValue_BANDWIDTH_A/dValue_BANDWIDTH_30);


        // aA1:= (iLevel_TX_a-30)/log10(dValue_BANDWIDTH_A/dValue_BANDWIDTH_30);
         aA1:= (iLevel_TX_a-30) / e;

         aB1:= (30*log10(dValue_BANDWIDTH_A/(2*dDeltaF))-iLevel_TX_a*log10(dValue_BANDWIDTH_30/(2*dDeltaF)))/
                                    e;
                                   // log10(dValue_BANDWIDTH_A/dValue_BANDWIDTH_30);

       end;
     end
     else
     begin
       dValue_BANDWIDTH_30 := aLinkEnd1.BANDWIDTH_TX_30;
       dValue_BANDWIDTH_3  := aLinkEnd1.BANDWIDTH_TX_3;


       dValue_BANDWIDTH_30:= qry_Mode1.FieldByName(FLD_BANDWIDTH_TX_30).AsFloat;
       dValue_BANDWIDTH_3 := qry_Mode1.FieldByName(FLD_BANDWIDTH_TX_3).AsFloat;

       if (dValue_BANDWIDTH_3 > 0) and (dValue_BANDWIDTH_30 > 0) then
       begin
         if Abs(dValue_BANDWIDTH_30-dValue_BANDWIDTH_3) < 1E-10  then
           e:=1E-10 //log10(dValue_BANDWIDTH_30/dValue_BANDWIDTH_3)
         else
           e:=log10(dValue_BANDWIDTH_30/dValue_BANDWIDTH_3);


       //  try


//           e:=log10(dValue_BANDWIDTH_30/dValue_BANDWIDTH_3);

           aA1:= (30-3) / e;
          // aA1:= (30-3)/log10(dValue_BANDWIDTH_30/dValue_BANDWIDTH_3);

           aB1:= (3*log10(dValue_BANDWIDTH_30/(2*dDeltaF))-30*log10(dValue_BANDWIDTH_3/(2*dDeltaF)))/
                                                        e;
                                                       // log10(dValue_BANDWIDTH_30/dValue_BANDWIDTH_3);

       //  except
        //   on E: Exception do ShowMessage(E.Message) ;
        // end;

       end;
     end;

     // -------------------------
     //for RX
     // -------------------------
//     dDeltaF_ := aLinkEnd2.BANDWIDTH;
     dDeltaF := qry_Mode2.FieldByName(FLD_BANDWIDTH).AsFloat;

     iLevel_RX_a := Abs(aLinkEnd2.LinkEndType.level_rx_a);
     iLevel_RX_a := Abs(dmLinkEndType.GetIntFieldValue(
                       qry_LinkEnd2.FieldByName(FLD_LINKENDTYPE_ID).AsInteger, FLD_level_rx_a));

//     dValue_BANDWIDTH_A_ := aLinkEnd2.BANDWIDTH_RX_A;
     dValue_BANDWIDTH_A := qry_Mode2.FieldByName(FLD_BANDWIDTH_RX_A).AsFloat;

//     Assert (dValue_BANDWIDTH_A_ = dValue_BANDWIDTH_A);


     if (iLevel_RX_a > 0) and (dValue_BANDWIDTH_A > 0) then
     begin
//       dValue_BANDWIDTH_30_ :=aLinkEnd2.BANDWIDTH_RX_30;
       dValue_BANDWIDTH_30:= qry_Mode2.FieldByName(FLD_BANDWIDTH_RX_30).AsFloat;

  //     Assert (dValue_BANDWIDTH_30_ = dValue_BANDWIDTH_30);


       if dValue_BANDWIDTH_30 > 0 then
       begin
         if Abs(dValue_BANDWIDTH_A-dValue_BANDWIDTH_30) < 1E-10  then
           e:=1E-10 //log10(dValue_BANDWIDTH_A/dValue_BANDWIDTH_30)
         else
           e:=log10(dValue_BANDWIDTH_A/dValue_BANDWIDTH_30);


        // e :=log10(dValue_BANDWIDTH_A/dValue_BANDWIDTH_30);


       //  aA2:= (iLevel_RX_a-30)/log10(dValue_BANDWIDTH_A/dValue_BANDWIDTH_30);
         aA2:= (iLevel_RX_a-30)/ e;

         aB2:= (30*log10(dValue_BANDWIDTH_A/(2*dDeltaF))-iLevel_RX_a*log10(dValue_BANDWIDTH_30/(2*dDeltaF)))/
                                                      e;
                                                 //     log10(dValue_BANDWIDTH_A/dValue_BANDWIDTH_30);
       end;
     end
     else
     begin
       dValue_BANDWIDTH_30 := aLinkEnd2.BANDWIDTH_RX_30;
       dValue_BANDWIDTH_3  := aLinkEnd2.BANDWIDTH_RX_3;


       dValue_BANDWIDTH_30:= qry_Mode2.FieldByName(FLD_BANDWIDTH_RX_30).AsFloat;
       dValue_BANDWIDTH_3 := qry_Mode2.FieldByName(FLD_BANDWIDTH_RX_3).AsFloat;

       if (dValue_BANDWIDTH_3 > 0) and (dValue_BANDWIDTH_30 > 0) then
       begin
         if Abs(dValue_BANDWIDTH_30-dValue_BANDWIDTH_3) < 1E-10  then
           e:=1E-10 //log10(dValue_BANDWIDTH_30/dValue_BANDWIDTH_3)
         else
           e:=log10(dValue_BANDWIDTH_30/dValue_BANDWIDTH_3);


       //  e:=log10(dValue_BANDWIDTH_30/dValue_BANDWIDTH_3);

//         aA2:= (30-3)/log10(dValue_BANDWIDTH_30/dValue_BANDWIDTH_3);
         aA2:= (30-3) / e; //log10(dValue_BANDWIDTH_30/dValue_BANDWIDTH_3);

         aB2:= (3*log10(dValue_BANDWIDTH_30/(2*dDeltaF))
                - 30*log10(dValue_BANDWIDTH_3/(2*dDeltaF))) / e;
                 //log10(dValue_BANDWIDTH_30/dValue_BANDWIDTH_3);
       end;
     end;
   end;    //

const
  SQL_LINKEND =
    'SELECT linkendtype_id, LINKENDTYPE_BAND_id, LINKENDTYPE_mode_id  FROM '+ TBL_LINKEND +
//    'SELECT mode,linkendtype_id,band_name FROM '+ TBL_LINKEND +
    ' WHERE id=:id';

  SQL_LINKENDTYPE_MODE =
    'SELECT * FROM '+ TBL_LINKENDTYPE_MODE +
    ' WHERE (id=:id)';
 //   ' WHERE (LinkEndType_id=:LinkEndType_id) and (mode=:mode)';

  SQL_LINKENDTYPE__BAND =
    'SELECT * FROM '+ TBL_LINKENDTYPE_BAND + ' WHERE (id=:id) ';

var iBandID: integer;
    dBand_Width_MHz, dA1, dA2, dB1, dB2: double;

    rec: dm_LinkFreqPlan_Neighbors_calc.TCalc_Freq_SpacingRec;

begin
  dA1:=0;
  dA2:=0;
  dB1:=0;
  dB2:=0;


  dmOnega_DB_data.OpenQuery(qry_LinkEnd1, SQL_LINKEND, [FLD_ID, aLinkEnd1_ID]);
  dmOnega_DB_data.OpenQuery(qry_LinkEnd2, SQL_LINKEND, [FLD_ID, aLinkEnd2_ID]);

  dmOnega_DB_data.OpenQuery(qry_Mode1, SQL_LINKENDTYPE_MODE,
      [FLD_ID, qry_LinkEnd1.FieldValues[FLD_LINKENDTYPE_mode_ID]]);

//       db_par(FLD_MODE,            qry_LinkEnd1.FieldValues[FLD_MODE])]);

  Assert(qry_Mode1.RecordCount=1, Format('RecordCount: %d', [qry_Mode1.RecordCount]));

  dmOnega_DB_data.OpenQuery(qry_Mode2, SQL_LINKENDTYPE_MODE,
      [FLD_ID, qry_LinkEnd2.FieldValues[FLD_LINKENDTYPE_mode_ID]]);

//      [db_par(FLD_LINKENDTYPE_ID, qry_LinkEnd2.FieldValues[FLD_LINKENDTYPE_ID]),
 //      db_par(FLD_MODE,           qry_LinkEnd2.FieldValues[FLD_MODE])]);

  Assert(qry_Mode2.RecordCount=1);

  iBandID:= qry_LinkEnd1.FieldByName(FLD_LINKENDTYPE_BAND_ID).AsInteger;


(*  iBandID:= dmLinkEndType.GetBandID_by_Name
                (qry_LinkEnd1.FieldByName(FLD_SUBBAND).AsString,
                 qry_LinkEnd1.FieldByName(FLD_LINKENDTYPE_ID).AsInteger);
*)

  if iBandID = 0 then
  begin
    Log ('LinkEnd_ID='+AsString(aLinkEnd1_ID)+'->BandID=0');
//    g_Log.Add ( 'LinkEnd_ID='+AsString(aLinkEnd1_ID)+'->BandID=0');
    Result := 0;
    Exit;
  end;

//  aLinkEnd1.Fr


   dBand_Width_MHz:= aLinkEnd1.LinkEndType_band.FREQ_MAX_HIGH -
                     aLinkEnd1.LinkEndType_band.FREQ_MIN_LOW;

 //                     FieldByName(FLD_FREQ_MIN_LOW).AsFloat;


  dmOnega_DB_data.OpenQuery(qry_Band1, SQL_LINKENDTYPE__BAND, [FLD_ID, iBandID]);
  with qry_Band1 do
    dBand_Width_MHz:= FieldByName(FLD_FREQ_MAX_HIGH).AsFloat -
                      FieldByName(FLD_FREQ_MIN_LOW).AsFloat;

  Do_Calc_Param_A_B(dA1, dA2, dB1, dB2);
  { DONE : ��� ���� freqspacing � linkendtype_mode. ?! ��� ���� ������������!}

  try
    FillChar(rec, SizeOf(rec), 0);

    rec.CI_treb             :=aCI_treb;
    rec.CI_0                :=aCI_0;
    rec.PowerNoise_dBm      :=aPowerNoise;
    rec.PowerNeighbour_dBm  :=aPowerNeighbors;
    rec.Degradation_treb_dB :=aDegradation_treb;
    rec.Degradation_calc    :=aDegradation_calc;


    rec.Freq1_spacing_MHz   :=aLinkEnd1.BANDWIDTH;
    rec.Freq2_spacing_MHz   :=aLinkEnd2.BANDWIDTH;

    rec.Freq1_spacing_MHz   :=qry_Mode1.FieldByName(FLD_BANDWIDTH).AsFloat;
    rec.Freq2_spacing_MHz   :=qry_Mode2.FieldByName(FLD_BANDWIDTH).AsFloat;
    rec.A1                  :=dA1;
    rec.A2                  :=dA2;
    rec.B1                  :=dB1;
    rec.B2                  :=dB2;
    rec.Band_Width_MHz      :=dBand_Width_MHz;

    Result:= dmLinkFreqPlan_Neighbors_calc.Calc_Freq_Spacing_MHz (rec);
  except

  end;

{
  Result:= dmLinkFreqPlan_Neighbors_calc.Calc_Freq_Spacing (aCI_treb, aCI_0,
                          aPowerNoise, aPowerNeighbors, aDegradation_treb, aDegradation_calc,
                          qry_Mode1.FieldByName(FLD_BANDWIDTH).AsFloat,
                          qry_Mode2.FieldByName(FLD_BANDWIDTH).AsFloat,
                          dA1, dA2, dB1, dB2, dBand_Width);}

end;

//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_Neighbors.Calc_Degradation;
//--------------------------------------------------------------------
var ePower_Neigh_Sum, dPower_Neigh, dPower_Noise: double;
  dPower_Neigh1: Double;
  e: Double;
  I: Integer;
  eDegradation: double;
  iID: Integer;
  iLINKEND_ID: Integer;
  oLinkFreqPlan_Linkend: TnbLinkFreqPlan_Linkend;
  oDBNeighbor: TnbNeighbor;
  j: integer;
  obNeighbor2: TnbNeighbor;
  s: string;

  oLinkend: TnbLinkFreqPlan_Linkend;

begin

  if Terminated or (qry_Neighbors.RecordCount = 0) then
    Exit;

//  qry_LinkEnd.Filter:= 'Enabled=1';
//  qry_LinkEnd.Filtered:= True;
  qry_FreqPlan_LinkEnd.First;

 // db_ViewDataSet(qry_FreqPlan_LinkEnd);


///////////  FData.Linkends.LoadFromDataset(qry_FreqPlan_LinkEnd);

(*

  for I := 0 to FLinkFreqPlan_Linkends.Count - 1 do    // Iterate
  begin
    oDBLinkFreqPlan_Linkend:=FLinkFreqPlan_Linkends[i];

    DoProgress(i, FLinkFreqPlan_Linkends.Count);

    ePower_Neigh_Sum:= 0;


    for j := 0 to FNeighbors.Count - 1 do
      if FNeighbors[j].RX_LINK_ID = oDBLinkFreqPlan_Linkend.LINKEND_ID  then
    begin
      oDBNeighbor:=FNeighbors[j];

      DoProgress2(j, FNeighbors.Count);


  //  end;


  //  with qry_Neighbors do
{    begin
      Filtered:= False;
      Filter  := 'rx_link_id=' + qry_FreqPlan_LinkEnd.FieldByName(FLD_LINKEND_ID).AsString;
      Filtered:= True;
      First;}

{
      if oDBNeighbor.BANDWIDTH = 0 then
      begin
        Log (IntToStr(oDBLinkFreqPlan_Linkend.LINKEND_ID) + ': bandwidth=0');
        //g_Log.Add ( qry_LinkEnd.FieldByName(FLD_LINKEND_ID).AsString+': bandwidth=0');
        qry_FreqPlan_LinkEnd.Next;
        Continue;
      end;
}

   Assert(oDBNeighbor.BANDWIDTH>0);

      dPower_Noise:= TruncFloat(oDBNeighbor.COEFF_NOISE +
                     10*log10(1.38*Power(10, -14)*300 * oDBNeighbor.BANDWIDTH));
{
      while (not Eof) and (not Terminated) do
      begin
        DoProgress2(RecNo, RecordCount);

        if oDBNeighbor.IsLink then
          begin Next; Continue; end;
}

        if not oDBNeighbor.IsLink then
        begin

          dPower_Neigh:= oDBNeighbor.POWER -
                         oDBNeighbor.DIAGRAM_LEVEL +
                         oDBNeighbor.EXTRA_LOSS;
          ePower_Neigh_Sum:= Power(10, 0.1*dPower_Neigh) + ePower_Neigh_Sum;

        end;
     //   Next;
     // end;
    end;

//    Assert(ePower_Neigh_Sum=0);
    //ePower_Neigh_Sum:=10*log10(ePower_Neigh_Sum);

    if ePower_Neigh_Sum>0 then
      ePower_Neigh_Sum:=10*log10(ePower_Neigh_Sum)
    else
      ePower_Neigh_Sum:=0;

    eDegradation := TruncFloat(10*log10(Power(10, 0.1*dPower_Noise) + Power(10, 0.1*ePower_Neigh_Sum)) - dPower_Noise);

    db_UpdateRecord(qry_FreqPlan_LinkEnd,
       [db_par(FLD_THRESHOLD_DEGRADATION, eDegradation)]);

    qry_FreqPlan_LinkEnd.Next;
  end;


*)

  //-------------------------------------------------------------------
  //-------------------------------------------------------------------

////////  db_View(qry_Neighbors);


  qry_FreqPlan_LinkEnd.First;

  while not qry_FreqPlan_LinkEnd.Eof do
  begin
{    oLinkFreqPlan_Linkend:=TDBLinkFreqPlan_Linkend.Create(nil);
    oLinkFreqPlan_Linkend.LoadFromDataset(qry_FreqPlan_LinkEnd);
    oLinkFreqPlan_Linkend.Free;
}

    DoProgress(qry_FreqPlan_LinkEnd.RecNo, qry_FreqPlan_LinkEnd.RecordCount);


//    qry_FreqPlan_LinkEnd.FieldByName(FLD_LINKEND_ID).AsString

    ePower_Neigh_Sum:= 0;

    iLINKEND_ID:=qry_FreqPlan_LinkEnd.FieldByName(FLD_LINKEND_ID).AsInteger;


    oLinkend:= Fdata.Linkends.FindByLinkendID(iLINKEND_ID);
    Assert(Assigned(oLinkend));
    // SelectByRxLinkendID_and_not_IsLink(iLINKEND_ID);


    Fdata.Neighbors.SelectByRxLinkendID_and_not_IsLink(iLINKEND_ID);

  //  Fdata.Neighbors.Selection.Show;


   // db_View(qry_Neighbors);

    with qry_Neighbors do
    begin

//db_View(qry_Neighbors);


  //    Filtered:= False;

//      s:='rx_link_id=' + qry_FreqPlan_LinkEnd.FieldByName(FLD_LINKEND_ID).AsString;
      s:='rx_linkend_id=' + qry_FreqPlan_LinkEnd.FieldByName(FLD_LINKEND_ID).AsString;

      Filter  := s; //'rx_link_id=' + qry_FreqPlan_LinkEnd.FieldByName(FLD_LINKEND_ID).AsString;
      Filtered:= True;
      First;

      Assert(FieldByName(FLD_BANDWIDTH).AsFloat <> 0);

      {
      if FieldByName(FLD_BANDWIDTH).AsFloat = 0 then
      begin
        Log (qry_FreqPlan_LinkEnd.FieldByName(FLD_LINKEND_ID).AsString+': bandwidth=0');
        //g_Log.Add ( qry_LinkEnd.FieldByName(FLD_LINKEND_ID).AsString+': bandwidth=0');
        qry_FreqPlan_LinkEnd.Next;
        Continue;
      end;
       }

//      dPower_Noise:=oLinkend.Lin


      dPower_Noise:= TruncFloat(FieldByName(FLD_COEFF_NOISE).AsFloat +
                     10*log10(1.38*Power(10, -14)*300*FieldByName(FLD_BANDWIDTH).AsFloat));

      ePower_Neigh_Sum:=0;

      while (not Eof) and (not Terminated) do
      begin
        DoProgress2(RecNo, RecordCount);

        if FieldByName(FLD_IsLink).AsBoolean then
          begin
            Next;
            Continue;
          end;

        iID:=FieldByName(FLD_ID).AsInteger;


        obNeighbor2:= Fdata.Neighbors.Selection.FindIByID(iID);

        Assert(Assigned(obNeighbor2));


        dPower_Neigh1:= obNeighbor2.RX_LEVEL_dBm -
                       obNeighbor2.DIAGRAM_LEVEL +
                       obNeighbor2.EXTRA_LOSS;



//        dPower_Neigh:= FieldByName(FLD_POWER_dBm).AsFloat -
        dPower_Neigh:= FieldByName(FLD_RX_LEVEL_dBm).AsFloat -
                       FieldByName(FLD_DIAGRAM_LEVEL).AsFloat +
                       FieldByName(FLD_EXTRA_LOSS).AsFloat;

      Assert (dPower_Neigh1 = dPower_Neigh);



        ePower_Neigh_Sum:= Power(10, 0.1*dPower_Neigh) + ePower_Neigh_Sum;

        Next;
      end;
    end;

//    Assert(ePower_Neigh_Sum=0);
    //ePower_Neigh_Sum:=10*log10(ePower_Neigh_Sum);

    if ePower_Neigh_Sum>0 then
      ePower_Neigh_Sum:=10*log10(ePower_Neigh_Sum)
    else
      ePower_Neigh_Sum:=0;

      try
       e:=Power(10, 0.1*dPower_Noise);

       eDegradation := TruncFloat( 10*log10(Power(10, 0.1*dPower_Noise) + Power(10, 0.1*ePower_Neigh_Sum) )  - dPower_Noise );
      except
      end;



    db_UpdateRecord(qry_FreqPlan_LinkEnd,
       [db_par(FLD_THRESHOLD_DEGRADATION, eDegradation)]);

    qry_FreqPlan_LinkEnd.Next;
  end;


end;



procedure TdmLinkFreqPlan_Neighbors.Log(aValue: string);
begin
  g_Log.Add ( aValue);
end;


//--------------------------------------------------------------------
function TdmLinkFreqPlan_Neighbors.OpenData: Boolean;
//--------------------------------------------------------------------
const
(*
  SQL_SELECT_LinkFreqPlan_LINKEND =
      'SELECT  id, LinkEnd_id, threshold_degradation '+  //Enabled,
      ' FROM ' + TBL_LinkFreqPlan_LinkEnd +
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id)';


  view_LinkFreqPlan_LinkEnd_for_calc_NB =
       'view_LinkFreqPlan_LinkEnd_for_calc_NB';

*)

  SQL_SELECT_LinkFreqPlan =
      'SELECT  * FROM ' + TBL_LinkFreqPlan +
      ' WHERE (id=:id)';





  SQL_SELECT_LinkEnds =
//      'SELECT * FROM  view_LinkFreqPlan_LinkEnd_for_calc_NB ' +
//      'SELECT * FROM ' + 'view_LinkFreqPlan_LinkEnd' +
      'SELECT * FROM ' + '[LinkFreqPlan].[view_LinkFreqPlan_LinkEnd]' + // !!!!!!!!!!!
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) order by linkend_name';

{
  SQL_SELECT_LinkEnds =
  //    'SELECT * FROM ' + view_LinkFreqPlan_LinkEnd_for_calc_NB +
      'SELECT * FROM ' + view_LinkFreqPlan_LinkEnd +
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id)';
}


// -------------------------
//  view_LinkFreqPlan_LinkEnd_antennas = 'view_LinkFreqPlan_LinkEnd_antennas_for_calc_NB';
  view_LinkFreqPlan_LinkEnd_antennas = 'LinkFreqPlan.view_LinkFreqPlan_LinkEnd_antennas';  //!!!!!!!!!!!!!!!




  SQL_SELECT_LinkFreqPlan_LinkEnd_antennas =
      'SELECT * FROM ' + view_LinkFreqPlan_LinkEnd_antennas +
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id)';


var
  b: Boolean;
  oErrorList: TStringList;
begin
  Result:=False;

// ---------------------------------------------------------------
  dmOnega_DB_data.OpenQuery (qry_FreqPlan_LinkEnd,
                SQL_SELECT_LinkEnds,
                [FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID]);

  dmOnega_DB_data.OpenQuery (qry_Antennas,
                SQL_SELECT_LinkFreqPlan_LinkEnd_antennas,
                [FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID]);

//  FAntennaList.LoadFromDataset(qry_Antennas);

//  db_View (qry_Antennas);


 /// Tfrm_Data.ExecDlg;


  Assert (qry_FreqPlan_LinkEnd.RecordCount>0);
  Assert (qry_Antennas.RecordCount>0);

  FData.Linkends.LoadFromDataset(qry_FreqPlan_LinkEnd);
  FData.Linkends.LoadFromDataset_Antennas(qry_Antennas);

//  if not b then
 //   Exit;


// ---------------------------------------------------------------

  dmOnega_db_data.OpenQuery (qry_LinkFreqPlan,
                SQL_SELECT_LinkFreqPlan,
                [FLD_ID, Params.LinkFreqPlan_ID]);

  FData.LinkFreqPlan.LoadFromDataset(qry_LinkFreqPlan);


  // -------------------------
  Result:=FData.Linkends.Validate();

//
//  oErrorList:=TStringList.Create;
//  FLinkFreqPlan.Linkends.Validate(oErrorList);
//
//  if oErrorList.Count>0 then
//  begin
//    if ConfirmDlg('���������� ������ ������. �������� ������?') then
//      ShellExec_Notepad_temp(oErrorList.Text);
//
//    if not ConfirmDlg('���������� ������?') then
//      Exit;
//
//  end;
//
//  FreeAndNil(oErrorList);
//  // -------------------------


//  Result:=True;

end;



//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_Neighbors.ExecuteProc;
//--------------------------------------------------------------------

(*const
  SQL_SELECT_LinkFreqPlan_LINKEND =
      'SELECT  id, LinkEnd_id, threshold_degradation '+  //Enabled,
      ' FROM ' + TBL_LinkFreqPlan_LinkEnd +
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id)';

  SQL_SELECT_LinkFreqPlan =
      'SELECT  * FROM ' + TBL_LinkFreqPlan +
      ' WHERE (id=:id)';

  view_LinkFreqPlan_LinkEnd_for_calc_NB =
       'view_LinkFreqPlan_LinkEnd_for_calc_NB';

  SQL_SELECT_LinkEnds =
      'SELECT * FROM ' + view_LinkFreqPlan_LinkEnd_for_calc_NB +
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id)';


// -------------------------
  view_LinkFreqPlan_LinkEnd_antennas_for_calc_NB = 'view_LinkFreqPlan_LinkEnd_antennas_for_calc_NB';

  SQL_SELECT_LinkEnd_antennas =
      'SELECT * FROM ' + view_LinkFreqPlan_LinkEnd_antennas_for_calc_NB +
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id)';
*)

begin
  if not OpenData() then
    Exit;

(*// ---------------------------------------------------------------
  db_Op enQuery (qry_Antennas,
                SQL_SELECT_LinkEnd_antennas,
                [db_Par (FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID)]);

  FAntennaList.LoadFromDataset(qry_Antennas);

  db_OpenQuery (qry_FreqPlan_LinkEnd,
                SQL_SELECT_LinkEnds,
                [db_Par (FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID)]);

  FLinkFreqPlan_Linkends.LoadFromDataset(qry_FreqPlan_LinkEnd);
// ---------------------------------------------------------------
*)

(*
  db_OpenQu ery (qry_FreqPlan_LinkEnd,
                SQL_SELECT_LinkFreqPlan_LINKEND,
                [db_Par (FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID)]);

  FLinkFreqPlan_Linkends.LoadFromDataset(qry_FreqPlan_LinkEnd);
*)

  (*db_OpenQ uery (qry_LinkFreqPlan,
                SQL_SELECT_LinkFreqPlan,
                [db_Par (FLD_ID, Params.LinkFreqPlan_ID)]);

  FLinkFreqPlan.LoadFromDataset(qry_LinkFreqPlan);
*)

//   tools_DatasetToClass(qry_LinkFreqPlan, 'd:\fr.pas');
//   tools_DatasetToClass(qry_LinkFreqPlan, 'd:\fr.pas');


//  db_ViewDataSet(qry_FreqPlan_LinkEnd);


  if Params.IsTotalCalc then
  begin
(*    FLinkFreqPlan_ID:= aLinkFreqPlan_ID;

    dmLinkFreqPlan_Neighbors_calc.Params.LinkFreqPlan_ID:= aLinkFreqPlan_ID;

    dmLinkFreqPlan_Neighbors_calc.Params.Calc_Distance_m:=
       1000 * FLinkFreqPlan.MaxCalcDistance_km;
*)

   // Open(Params.LinkFreqPlan_ID);
    Neighbors_Calc;
  end;

  //������ ��������� �� CI
  IsNeighbors (Params.LinkFreqPlan_ID);
 /// Calc_Degradation();
end;




begin
end.




{
  with qry_Neighbors do
    while (not Eof) and (not Terminated) do
  begin
    DoProgress2(RecNo, RecordCount);

    if (not FieldByName(FLD_IsLink).AsBoolean) then
    begin
      dPower:= gl_DB.GetDoubleFieldValue(TBL_LINK_FREQPLAN_NEIGHBORS, FLD_POWER,
                     [db_par(FLD_RX_LINK_ID,       FieldValues[FLD_RX_LINK_ID]),
                      db_par(FLD_LINK_FREQPLAN_ID, aLink_FreqPlan_ID),
                      db_par(FLD_IsLink,           True) ]);
      if dPower=0 then
        dPower:= gl_DB.GetDoubleFieldValue(TBL_LINK_FREQPLAN_NEIGHBORS, FLD_POWER,
                     [db_par(FLD_TX_ID,           FieldValues[FLD_RX_LINK_ID]),
                      db_par(FLD_LINK_FREQPLAN_ID, aLink_FreqPlan_ID),
                      db_par(FLD_IsLink,          True) ]);

      if dPower<>0 then
        begin
          dCI_calc:= dPower - (FieldByName(FLD_POWER).AsFloat -
                               FieldByName(FLD_DIAGRAM_LEVEL).AsFloat +
                               FieldByName(FLD_EXTRA_LOSS).AsFloat);
          isNeigh:= dCI_treb > dCI_calc;
        end
      else
        isNeigh:= False;

      dPower_Neighbors:= FieldByName(FLD_POWER).AsFloat -
                         FieldByName(FLD_DIAGRAM_LEVEL).AsFloat +
                         FieldByName(FLD_EXTRA_LOSS).AsFloat;

      if FieldByName(FLD_BANDWIDTH).AsFloat = 0 then
      begin
        Log (FieldByName(FLD_RX_LINK_ID).AsString+': bandwidth=0');
//        g_Log.Add ( FieldByName(FLD_RX_LINK_ID).AsString+': bandwidth=0');
        dPower_Noise:= 255;
        dDegradation_calc:= 0;
      end
      else
      begin
        dPower_Noise:= TruncFloat(FieldByName(FLD_COEFF_NOISE).AsFloat +
          10*log10(1.38*Power(10, -14)*300*FieldByName(FLD_BANDWIDTH).AsFloat));

          dDegradation_calc:= TruncFloat(10*log10(Power(10, 0.1*dPower_Noise)+
                                  Power(10, 0.1*dPower_Neighbors))-dPower_Noise);
      end;
}



//--------------------------------------------------------------------
//procedure TdmLinkFreqPlan_Neighbors.Open;
//--------------------------------------------------------------------
//begin
//  mem_linkend.Close;
//  mem_linkend.Open;
//
//  FLinkFreqPlan_ID:= aLinkFreqPlan_ID;
//(*
//  dmLinkFreqPlan_Neighbors_calc.Params.LinkFreqPlan_ID:= Params.LinkFreqPlan_ID;
//
//  dmLinkFreqPlan_Neighbors_calc.Params.Calc_Distance_m:=
//     1000 * FDBLinkFreqPlan.MaxCalcDistance_km;
//*)
//{
//  dmLinkFreqPlan_Neighbors_calc.Params.Calc_Distance:=
//     1000*gl_DB.GetDoubleFieldValue (TBL_LINK_FREQPLAN, FLD_MaxCalcDistance,
//                                         [db_par(FLD_ID, aLinkFreqPlan_ID)]);
//}
//  qry_LinkEnd.Filter:= 'Enabled=1';
//  qry_LinkEnd.Filtered:= True;
//
//
//(*  qry_FreqPlan_LinkEnd.First;
//  with qry_FreqPlan_LinkEnd do
//     while not Eof do
//  begin
//    if not mem_linkend.Locate(FLD_ID, FieldValues[FLD_LINKEND_ID], []) then
//      db_AddRecord(mem_linkend, [db_par(FLD_ID, FieldValues[FLD_LINKEND_ID])]);
//
//    Next;
//  end;
//*)
//
//end;

{
  FLinkFreqPlan_ID:= aLinkFreqPlan_ID;

}



