unit dm_LinkFreqPlan_Neighbors_calc;

interface

{$DEFINE db}


uses
  SysUtils, Classes, DB, ADODB, Forms, Math, Variants,

   XMLIntf,
   u_xml_document,

  // u_Ini_rpls_rrl_params,

   u_link_model_layout,

   u_LinkCalcResult_new,

   dm_LinkEnd_ex,

  u_run,

  //u_XML,

  u_vars,

  u_rrl_param_rec_new,

  u_antenna_mask,

  u_const,

  u_const_db,
  u_Link_const,

  u_LinkFreqPlan_const,


  u_db,

  
  u_func,
  
  u_Geo,

  u_rel_Profile,

  dm_Main,
  dm_Progress,

  dm_AntType,
  dm_LinkEnd,

  

//  dm_act_Rel_Engine,
  dm_Rel_Engine,

  u_LinkFreqPlan_antenna_classes,
//  u_antenna_classes,

  u_LinkEnd_classes,

  u_LinkFreqPlan_classes;

type

  TCalc_Freq_SpacingRec = record
    CI_treb: Double;     // aCI_t - ��������� CI, ��
    CI_0: Double;     // aCI_0 - �������� CI (��� ���������� ������), ��
    PowerNoise_dBm: Double;  // aPowerNoise  - �������� ����, dBm
    PowerNeighbour_dBm: Double;
    Degradation_treb_dB: Double;
    Degradation_calc: Double;
    Freq1_spacing_MHz: Double;
    Freq2_spacing_MHz: Double;

    A1, A2, B1, B2: Double;

    Band_Width_MHz: double;  //

  //-------�������� ������--------
  // aCI_t - ��������� CI, ��
  // aCI_0 - �������� CI (��� ���������� ������), ��
  // aPowerNoise  - �������� ����, dBm
  // aPowerNeighbour - �������� ������ ��� ���������� ������, dBm
  // aDegradation_treb - ���������� �������� ���������� ����������������, dB
  // aDegradation_calc - ���������� �������� ���������� ���������������� ��� ���������� �������, dB
  // aFreq1_spacing, aFreq2_spacing - ������ (���) ������, ���
  // A1, A2 - ���. ���������� ����� ��� ������� �� ������ 10 �����, ��
  // B1, B2 - ���������� ��������� ������, ��
  //(��� � � �: 1-��� ���1; 2-��� ���2)
  // aBand_Width - ������ ���������� ��������� (����. ��� 13 ���-500��� (12750...13250))
// ---------------------------------------------------------------

  end;



  TdmLinkFreqPlan_Neighbors_calc = class(TdmProgress)
    qry_LinkEnd1: TADOQuery;
    qry_Antennas1: TADOQuery;
    qry_LinkEnd2: TADOQuery;
    qry_Antennas2: TADOQuery;
    qry_LinkType: TADOQuery;
    qry_LinkEndType: TADOQuery;
    qry_Temp: TADOQuery;
    qry_Mask: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
//    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
  //  Flog: TStingList;



    FVector:         TBLVector;
    FrelProfile:     TrelProfile;
    FClutterModelID: integer;

//    FPROPERTY_ID : Integer;

    FDBLinkend1: TDBLinkend;
    FDBLinkend2: TDBLinkend;

    FDBAntennas1: TDBAntennaList_;
    FDBAntennas2: TDBAntennaList_;


{    FDBLinkend1: TDBLinkend;
    FDBLinkend2: TDBLinkend;
}

    FIsPropertyOne:  boolean; //���� ��������
    FIsLink:         boolean; //�������� ��������
//    FIsRange:        boolean; //���� ��������
    FDiagram_level:  double;
    FFreq_GHz:       double;
    FDistanse_m:       double;



//    function GetNameLink_ByLinkEnd(aLinkEnd1_ID, aLinkEnd2_ID: integer): string;

//  procedure Get_AntennasGain(aBLPoint1, aBLPoint2: TBLPoint;
//                                  var aGain1, aGain2: double);

  //  function DoAdd_Neighbors(aID1,aID2: integer): integer;

    function Calc_Diagram(aBLPoint1, aBLPoint2: TBLPoint; aLinkEnd1,aLinkEnd2:
        TnbLinkFreqPlan_Linkend): double;

    function SaveProfileToXMLNode(vRoot: IXMLNode; aStep_M: integer; aCalcParam:
        TrrlCalcParam): boolean;

    procedure LoadCalcResultsFromXML( //aID1, aID2: integer;
            aDBNeighbor: TnbNeighbor;
                aFileName: string);

    function OpenData(aLinkEndID1, aLinkEndID2: integer;

           aLinkEnd1,aLinkEnd2: TnbLinkFreqPlan_Linkend;

           aData: TnbData

//           var aTerminated: boolean
): boolean;

    function Run_rpls_rrl(aFile_XML: string): Boolean;

    function SaveToXML(aFileName, aResult_FileName, aLayout_FileName: string;
        aLinkEnd1,aLinkEnd2: TnbLinkFreqPlan_Linkend): boolean;

//    vRoot.Attributes['Result_FileName'] := aResult_FileName;
//    vRoot.Attributes['Layout_FileName'] := aLayout_FileName;



    function SaveToXML_new(aFileName: string;
        aLinkEnd1,aLinkEnd2: TnbLinkFreqPlan_Linkend): boolean;

//    function Validate: Integer;

  public
    Params: record
              Calc_Distance_m: double;
              LinkFreqPlan_ID: integer;
            end;

    function Calc_Freq_Spacing_MHz(aRec: TCalc_Freq_SpacingRec): double;

{    aCI_t, aCI_0,
       aPowerNoise, aPowerNeighbour, aDegradation_treb, aDegradation_calc,
       aFreq1_spacing, aFreq2_spacing, A1, A2, B1, B2, aBand_Width: double): double;
}


    function Execute(aLinkEnd_ID1, aLinkEnd_ID2: integer;
       aLinkEnd1,aLinkEnd2:  TnbLinkFreqPlan_Linkend;

        aDBNeighbor: TnbNeighbor;

        aData: TnbData;

        var aTerminated:  boolean): boolean;

    class procedure Init;
  end;

var
  dmLinkFreqPlan_Neighbors_calc: TdmLinkFreqPlan_Neighbors_calc;


//  function dmLinkFreqPlan_Neighbors_calc: TdmLinkFreqPlan_Neighbors_calc;



//==================================================================
implementation {$R *.dfm}
//==================================================================


class procedure TdmLinkFreqPlan_Neighbors_calc.Init;
begin
  if not Assigned(dmLinkFreqPlan_Neighbors_calc) then
    dmLinkFreqPlan_Neighbors_calc := TdmLinkFreqPlan_Neighbors_calc.Create(Application);

end;


// ---------------------------------------------------------------
procedure TdmLinkFreqPlan_Neighbors_calc.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;

  db_SetComponentADOConnection(Self, dmMain.ADOConnection);

  FrelProfile:=TrelProfile.Create();

  FDBLinkend1 := TDBLinkend.Create();
  FDBLinkend2 := TDBLinkend.Create();
  FDBAntennas1 := TDBAntennaList_.Create();
  FDBAntennas2 := TDBAntennaList_.Create();

 // Flog:=TStingList.Create();


end;

//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_Neighbors_calc.DataModuleDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  FreeAndNil(FDBAntennas2);
  FreeAndNil(FDBAntennas1);
  FreeAndNil(FDBLinkend2);
  FreeAndNil(FDBLinkend1);
  FreeAndNil(FrelProfile);
                         
 // FreeAndNil(Flog);

  inherited;
end;


//--------------------------------------------------------------------
function TdmLinkFreqPlan_Neighbors_calc.Calc_Freq_Spacing_MHz(aRec:
    TCalc_Freq_SpacingRec): double;
//--------------------------------------------------------------------
  //-------�������� ������--------
  // aCI_t - ��������� CI, ��
  // aCI_0 - �������� CI (��� ���������� ������), ��
  // aPowerNoise  - �������� ����, dBm
  // aPowerNeighbour - �������� ������ ��� ���������� ������, dBm
  // aDegradation_treb - ���������� �������� ���������� ����������������, dB
  // aDegradation_calc - ���������� �������� ���������� ���������������� ��� ���������� �������, dB
  // aFreq1_spacing, aFreq2_spacing - ������ (���) ������, ���
  // A1, A2 - ���. ���������� ����� ��� ������� �� ������ 10 �����, ��
  // B1, B2 - ���������� ��������� ������, ��
  //(��� � � �: 1-��� ���1; 2-��� ���2)
  // aBand_Width - ������ ���������� ��������� (����. ��� 13 ���-500��� (12750...13250))
// ---------------------------------------------------------------
const
  STEP_F = 0.125;  //��� ��������
var
  dCI_prev: double;
  f: double; //������� ��������� ������
  dY_f       : double; //���������� �������� �����
  dFreq_delta: double; //
  dQ_0       : double; //�����. ���������� �� ��������� ������, � �����
  dQ_1       : double; //�����. ���������� �� �������� ������ ���1, � �����
  dQ_2       : double; //�����. ���������� �� �������� ������ ���2, � �����
  dCI_calc    : double; //�������� CI ��� ���. ��������� �������
  dPowerNeighbour_calc: double; //�������� ������  ��� ���. ��������� �������, dBm
  dThresholdDegradation_calc: double; //���������� ���������������� ��� ���. ��������� �������, dB

  e: Extended;
  e1: Extended;
  e11: Extended;
  e2: Extended;
  e22: Extended;
  eSum: Double;

begin
  try
    Result:= aRec.Band_Width_MHz;

    if (aRec.Freq1_spacing_MHz=0) or (aRec.Freq2_spacing_MHz=0) then
      Exit;

    if (aRec.CI_treb < aRec.CI_0) and (aRec.Degradation_treb_dB > aRec.Degradation_calc) then
    begin
      Result := 0;
      Exit;
    end;

    dThresholdDegradation_calc:= aRec.Degradation_calc;

    f:= STEP_F;
    while f < aRec.Band_Width_MHz do
    begin
      dY_f:= 0.5*(aRec.Freq1_spacing_MHz + aRec.Freq2_spacing_MHz) - f;

   //   if dY_f > 0 then
      if dY_f >= 0 then
        dFreq_delta:= IIF(dY_f < Min(aRec.Freq1_spacing_MHz, aRec.Freq2_spacing_MHz), dY_f,
                                 Min(aRec.Freq1_spacing_MHz, aRec.Freq2_spacing_MHz))
      else
        dFreq_delta:= 0;

      dQ_0 := dFreq_delta / aRec.Freq1_spacing_MHz;
      if f=0 then
      begin
        dQ_1:=0;
        dQ_2:=0;
      end
      else
      begin
        e:=log10(f/aRec.Freq1_spacing_MHz);

        e:=aRec.A1*log10(f/aRec.Freq1_spacing_MHz);


 //       dQ_1 := IIF(f<0.5*aRec.Freq1_spacing_MHz, 0, Power(10, 0.1*(-aRec.B1-aRec.A1*log10(f/aRec.Freq1_spacing_MHz))));
   //     dQ_2 := IIF(f<0.5*aRec.Freq2_spacing_MHz, 0, Power(10, 0.1*(-aRec.B2-aRec.A2*log10(f/aRec.Freq2_spacing_MHz))));

        if (f<0.5*aRec.Freq1_spacing_MHz) then
           dQ_1 :=0
        else
        begin
          e11:=Power(10, 0.1*(-aRec.B1));

          e1:=Power(10, 0.1*(-aRec.B1-aRec.A1*log10(f/aRec.Freq1_spacing_MHz)));

          dQ_1 := Power(10, 0.1*(-aRec.B1-aRec.A1*log10(f/aRec.Freq1_spacing_MHz)));
        end;

        if (f<0.5*aRec.Freq2_spacing_MHz) then
          dQ_2 := 0
        else
        begin
          e22:=Power(10, 0.1*(-aRec.B2));

          e2:=Power(10, 0.1*(-aRec.B2-aRec.A2*log10(f/aRec.Freq2_spacing_MHz)));

          dQ_2 := Power(10, 0.1*(-aRec.B2-aRec.A2*log10(f/aRec.Freq2_spacing_MHz)));
        end;

      end;

      try
        eSum:=dQ_0 + dQ_1 + dQ_2;
        if eSum<1E-10 then
          eSum := 1E-10;

          

       // assert(dQ_0 + dQ_1 + dQ_2 <> 0);

//        dCI_calc:= aRec.CI_0 - 10*log10(dQ_0 + dQ_1 + dQ_2);
        dCI_calc:= aRec.CI_0 - 10*log10(eSum);

      except
       // on E: Exception do: ;
      end;

      if aRec.PowerNoise_dBm<>255 then
      begin
       //  assert(dQ_0 + dQ_1 + dQ_2 <> 0);

//        dPowerNeighbour_calc      := aRec.PowerNeighbour_dBm + 10*log10(dQ_0 + dQ_1 + dQ_2);
        dPowerNeighbour_calc      := aRec.PowerNeighbour_dBm + 10*log10(eSum);

        dThresholdDegradation_calc:= TruncFloat(10*log10(Power(10, 0.1*aRec.PowerNoise_dBm) +
                                      Power(10, 0.1*dPowerNeighbour_calc)) - aRec.PowerNoise_dBm);
      end;

      if (dCI_calc > aRec.CI_treb) and
         (dThresholdDegradation_calc < aRec.Degradation_treb_dB) then
      begin
        Result:= f;
        Exit;
      end;

      f:= f + STEP_F;
    end;
  except

  end;

end;

//--------------------------------------------------------------------
function TdmLinkFreqPlan_Neighbors_calc.Calc_Diagram(aBLPoint1, aBLPoint2: TBLPoint;
    aLinkEnd1,aLinkEnd2: TnbLinkFreqPlan_Linkend): double;
//--------------------------------------------------------------------
var
//ffffffffffffff
  dTiltAngle: double; //���� ����� ����� ������������� �� �������� � ��������� ���

    // ---------------------------------------------------------------
    //------ ������� ����������� ���������� ��� � �������� �����������
    // ---------------------------------------------------------------
    function DoGetLoss_Antenna (aBLPoint1, aBLPoint2: TBLPoint;
                                aTypeMask: string;
                               // aTypeMask: integer;

                                aAntQuery: TADOQuery;
                                aAzimuth : Double;
                                var aAntType: TdmAntTypeInfoRec;

                                aAntenna: TnbAntenna
                              //  var aMask: TdmAntTypeMask
                                ): double;
    // ---------------------------------------------------------------
{
    0: Result:= 'H';
    1: Result:= 'V';
    2: Result:= 'X';
}
    // ---------------------------------------------------------------
   { const
      TBL_Antenna_Mask = 'AntennaMask';

      SQL_SELECT_ANTENNA_MASK =
         'SELECT * FROM '+ TBL_Antenna_Mask +
         ' WHERE (antennaType_id=:antennaType_id) and (type=:type) ' +
         ' ORDER BY angle';
}
    var
      dAzimuth, dAngle: double;
      d: Double;
    //  v: Variant;

    begin
      Result := 0;

      Assert(aAntQuery.RecordCount>0, 'aAntQuery.RecordCount>0');


      if geo_ComparePoints(aBLPoint1, aBLPoint2) then
        dAzimuth:= 90
      else
      begin
//        v:=aAntQuery.FieldValues[FLD_AZIMUTH];

        d:=aAntQuery.FieldByName(FLD_AZIMUTH).AsFloat;

        dAzimuth:= TruncFloat(geo_Azimuth(aBLPoint1, aBLPoint2));
        dAzimuth:= dAzimuth - aAntQuery.FieldByName(FLD_AZIMUTH).AsFloat;
      end;

      if dAzimuth < 0 then
        dAzimuth:= dAzimuth + 360;

      if (dAzimuth = 90) or (dTiltAngle = 0) then
        dAngle:= dAzimuth
      else
        dAngle:= TruncFloat((180/Pi)*arccos(cos(dAzimuth*Pi/180)*cos(dTiltAngle*Pi/180)));



      if Eq(aTypeMask,'h') then
        Result:=aAntenna.HorzMask.GetLossByAngle(dAngle) else
      if Eq(aTypeMask,'v') then
        Result:=aAntenna.VertMask.GetLossByAngle(dAngle)
     //   Result:=dmAntType.Mask_GetLossByAngle (aAntType.VertMaskArr, dAngle)
      else
        Result:=0;


      if Eq(aTypeMask,'h') then
        Result:=dmAntType.Mask_GetLossByAngle (aAntType.HorzMaskArr, dAngle) else
      if Eq(aTypeMask,'v') then
        Result:=dmAntType.Mask_GetLossByAngle (aAntType.VertMaskArr, dAngle)
      else
        Result:=0;


(*      case aTypeMask of
        0: Result:=dmAntType.Mask_GetLossByAngle (aAntType.HorzMaskArr, dAngle);
        1: Result:=dmAntType.Mask_GetLossByAngle (aAntType.VertMaskArr, dAngle);
      else
        Result:=0;
      end;
*)
{
      db_Op enQuery(qry_Mask, SQL_SELECT_ANTENNA_MASK,
          [db_par(FLD_ANTENNATYPE_ID, aAntQuery.FieldValues[FLD_ANTENNATYPE_ID]),
           db_par(FLD_TYPE,           aTypeMask)]);

//      if dAngle > 0 then
        if qry_Mask.Locate(FLD_ANGLE, dAngle, []) then
          Result:= qry_Mask.FieldByName(FLD_LOSS).AsFloat
        else

        begin
          qry_Mask.First;
          with qry_Mask do while not Eof do
             if dAngle < FieldByName(FLD_ANGLE).AsFloat then
          begin
            Result:= FieldByName(FLD_LOSS).AsFloat;
            Break;
          end
          else
            Next;

            if Result = 0 then
              Result:= qry_Mask.FieldByName(FLD_LOSS).AsFloat;

        end;
}
    end;

    // ---------------------------------------------------------------
    //------ ������� ����������� ������� �� ��������������� ����������� -----
    // ---------------------------------------------------------------
    function DoIsPolarization_Other_Diagramm (var aAntType1,aAntType2: TdmAntTypeInfoRec) : boolean;
    // ---------------------------------------------------------------
   { const                   //(var aMask: TdmAntTypeMask)


      SQL_SELECT_ANTENNA_SUM_LOSS =
           ' SELECT SUM(loss) AS SUM  FROM '+ TBL_Antenna_Mask +
           ' WHERE (antennaType_id=:antennaType_id) AND (angle = 0) ';
}

    var
      dSum,dVLoss,dHLoss,dPolarizationRatio : double;

      oAntenna1: TnbAntenna;
      oAntenna2: TnbAntenna;

    begin
      Result := False;

      oAntenna1 :=aLInkEnd1.Antennas[0];

      dVLoss:=oAntenna1.VertMask.GetLossByAngle(0);
      dHLoss:=oAntenna1.HorzMask.GetLossByAngle(0);

      dVLoss:=dmAntType.Mask_GetLossByAngle (aAntType1.VertMaskArr , 0);
      dHLoss:=dmAntType.Mask_GetLossByAngle (aAntType1.HorzMaskArr , 0);
      dSum:=dVLoss + dHLoss;

{
      db_Open Query(qry_Temp, SQL_SELECT_ANTENNA_SUM_LOSS,
          [db_par(FLD_ANTENNATYPE_ID, qry_Antennas1.FieldValues[FLD_ANTENNATYPE_ID])]);

      dPolarizationRatio:= gl_DB.GetDoubleFieldValue(TBL_ANTENNA_TYPE, FLD_POLARIZATION_RATIO,
          [db_par(FLD_ID, qry_Antennas1.FieldValues[FLD_ANTENNATYPE_ID])]);
}
   //   Result := (qry_Temp.FieldByName('SUM').AsFloat = dPolarizationRatio);

      Result := (dSum = oAntenna1.Polarization_ratio);

      Result := (dSum = aAntType1.POLARIZATION_RATIO);

      if not Result then
        Exit;

      oAntenna2 :=aLInkEnd2.Antennas[0];

      dVLoss:=oAntenna2.VertMask.GetLossByAngle(0);
      dHLoss:=oAntenna2.HorzMask.GetLossByAngle(0);


      dVLoss:=dmAntType.Mask_GetLossByAngle (aAntType2.VertMaskArr , 0);
      dHLoss:=dmAntType.Mask_GetLossByAngle (aAntType2.HorzMaskArr , 0);
      dSum:=dVLoss + dHLoss;

{      db_OpenQ uery(qry_Temp, SQL_SELECT_ANTENNA_SUM_LOSS,
          [db_par(FLD_ANTENNATYPE_ID, qry_Antennas2.FieldValues[FLD_ANTENNATYPE_ID])]);

      dPolarizationRatio:= gl_DB.GetDoubleFieldValue(TBL_ANTENNA_TYPE, FLD_POLARIZATION_RATIO,
          [db_par(FLD_ID, qry_Antennas2.FieldValues[FLD_ANTENNATYPE_ID])]);

      Result := (qry_Temp.FieldByName('SUM').AsFloat = dPolarizationRatio);
    }

      Result := (dSum = oAntenna2.Polarization_ratio);

      Result := (dSum = aAntType2.Polarization_Ratio);

    end;


    // ---------------------------------------------------------------
    //------ ������� ����������� ���� ����� -----
    // ---------------------------------------------------------------
    function DoGetTiltAngle(): double;
    // ---------------------------------------------------------------
    var
      dTiltAngle1, dTiltAngle2, dRelHeight11, dRelHeight12, dRelHeight2: double;
      dAntHeight11, dAntHeight12, dAntHeight2: double;
      dHeight11, dHeight12, dHeight2 : double;
      blPoint11, blPoint12, blPoint2: TBLPoint;
      e: Double;
      iLinkEndID11, iLinkEndID12, iLinkEndID2: integer;

      oAntenna1: TnbAntenna;
      oAntenna2: TnbAntenna;

    begin
      Result := 0;

      oAntenna1 := aLinkEnd1.Antennas[0];
      oAntenna2 := aLinkEnd2.Antennas[0];

      blPoint11 := oAntenna1.BLPoint;
      blPoint2  := oAntenna2.BLPoint;


      blPoint11.B:= qry_Antennas1.FieldByName(FLD_LAT).AsFloat;
      blPoint11.L:= qry_Antennas1.FieldByName(FLD_LON).AsFloat;
      blPoint2.b := qry_Antennas2.FieldByName(FLD_LAT).AsFloat;
      blPoint2.L := qry_Antennas2.FieldByName(FLD_LON).AsFloat;


   //   if geo_ComparePoints(blPoint11, blPoint2) then
    //    Exit;

      dAntHeight11:= oAntenna1.Height;
      dAntHeight2 := oAntenna2.Height;


//      oAntenna1.

      dAntHeight11:= qry_Antennas1.FieldByName(FLD_HEIGHT).AsFloat;
      dAntHeight2 := qry_Antennas2.FieldByName(FLD_HEIGHT).AsFloat;

      iLinkEndID2 :=qry_Antennas2.FieldByName(FLD_LINKEND_ID).AsInteger;
      iLinkEndID11:=qry_Antennas1.FieldByName(FLD_LINKEND_ID).AsInteger;

      iLinkEndID12:= gl_DB.GetIntFieldValue(TBL_LINK, FLD_LINKEND2_ID,
                                       [db_par(FLD_LINKEND1_ID, iLinkEndID11)]);

      if iLinkEndID12=0 then
        iLinkEndID12:= gl_DB.GetIntFieldValue(TBL_LINK, FLD_LINKEND1_ID,
                                       [db_par(FLD_LINKEND2_ID, iLinkEndID11)]);

      blPoint12   := dmLinkEnd.GetPropertyPos(iLinkEndID12);
      dAntHeight12:= dmLinkEnd_ex.GetMaxAntennaHeight(iLinkEndID12);

//      e:=aLinkEnd1.get MaxAntennaHeight;
//      e:=aLinkEnd2.MaxAntennaHeight;

      e:=aLinkEnd1.PROPERTY_GROUND_HEIGHT;
      e:=aLinkEnd2.PROPERTY_GROUND_HEIGHT;


      dRelHeight11:=gl_DB.GetDoubleFieldValue(TBL_PROPERTY, FLD_GROUND_HEIGHT,
                       [db_par(FLD_ID, dmLinkEnd.GetPropertyID(iLinkEndID11))]);
      dRelHeight12:=gl_DB.GetDoubleFieldValue(TBL_PROPERTY, FLD_GROUND_HEIGHT,
                       [db_par(FLD_ID, dmLinkEnd.GetPropertyID(iLinkEndID12))]);
      dRelHeight2 :=gl_DB.GetDoubleFieldValue(TBL_PROPERTY, FLD_GROUND_HEIGHT,
                       [db_par(FLD_ID, dmLinkEnd.GetPropertyID(iLinkEndID2))]);

      dHeight11:= dAntHeight11 + dRelHeight11;
      dHeight12:= dAntHeight12 + dRelHeight12;
      dHeight2 := dAntHeight2 + dRelHeight2;

      dTiltAngle1:= geo_TiltAngle (blPoint11, blPoint12, dHeight11, dHeight12);
      dTiltAngle2:= geo_TiltAngle (blPoint11, blPoint2,  dHeight11, dHeight2);

      Result := Abs(dTiltAngle1 - dTiltAngle2);
//      Result := TruncFloat(Abs(dTiltAngle1 - dTiltAngle2));
    end;    //
    // ---------------------------------------------------------------



var
  bDifferentPolarization: boolean; // ������ �����������
  bPolarization_Other_Diagramm : boolean; //������� �� ��������������� �����������
  dGain1, dGain2, dGain1_Different, dGain2_Different: double;
  e: Double;
  e_test: Double;

//  rVMask,rHMask: TdmAntTypeMask;

  rAntType1,rAntType2: TdmAntTypeInfoRec;

  //  function TdmAntType.GetInfoRec (aID: integer; var aRec: TdmAntTypeInfoRec): boolean;

{        procedure Mask_StrToArr(aValue: string; var aMask: TdmAntTypeMask);
    function Mask_GetLossByAngle(var aMask: TdmAntTypeMask; aAngle: double): double;
}

  oAntenna1: TnbAntenna;
  oAntenna2: TnbAntenna;

begin
  dTiltAngle:= 0;



 {
  if FIsPropertyOne then
    dTiltAngle:= 0
  else
    dTiltAngle:= DoGetTiltAngle;
  }

  if FIsPropertyOne then
    dTiltAngle:= DoGetTiltAngle
  else
    dTiltAngle:= 0;


  Assert (aLinkEnd1.Antennas.Count>0);
  Assert (aLinkEnd2.Antennas.Count>0);


  oAntenna1 := aLinkEnd1.Antennas[0];
  oAntenna2 := aLinkEnd2.Antennas[0];


  bDifferentPolarization:=
//    oAntenna1.Polarization_str <> oAntenna2.Polarization_str;
    oAntenna1.Polarization <> oAntenna2.Polarization;


  bDifferentPolarization:=
    aLinkEnd1.Antennas[0].Polarization <>
                          aLinkEnd2.Antennas[0].Polarization;

//    aLinkEnd1.AntennaList[0].Polarization_str <>
  //                        aLinkEnd2.AntennaList[0].Polarization_str;


  bDifferentPolarization:=

    // (qry_Antennas1.FieldByName(FLD_POLARIZATION_STR).Asstring<>null) and
    // (qry_Antennas2.FieldByName(FLD_POLARIZATION_STR).Asstring<>null) and

     (qry_Antennas1.FieldByName(FLD_POLARIZATION_STR).AsString <>
      qry_Antennas2.FieldByName(FLD_POLARIZATION_STR).AsString);


  dmAntType.GetInfoRec11
     (qry_Antennas1.FieldByName(FLD_ANTENNATYPE_ID).AsInteger, rAntType1);

  dmAntType.GetInfoRec11
     (qry_Antennas2.FieldByName(FLD_ANTENNATYPE_ID).AsInteger, rAntType2);



  bPolarization_Other_Diagramm:= DoIsPolarization_Other_Diagramm(rAntType1,rAntType2);


  if (not bDifferentPolarization) or (not bPolarization_Other_Diagramm) then
  begin
    //������ TX Antenna Loss
    Result:= DoGetLoss_Antenna(aBLPoint1,aBLPoint2,
             qry_Antennas1.FieldByName(FLD_POLARIZATION_STR).asString,
             qry_Antennas1,
             qry_Antennas1.FieldByName(FLD_AZIMUTH).AsFloat,
             rAntType1,
             oAntenna1
             //aLinkEnd1.AntennaRefList[0]
             );




e_test:=DoGetLoss_Antenna(aBLPoint2,aBLPoint1,
             qry_Antennas2.FieldByName(FLD_POLARIZATION_STR).asString,
             qry_Antennas2,
             qry_Antennas2.FieldByName(FLD_AZIMUTH).AsFloat,
             rAntType2,
             oAntenna2
             //aLinkEnd2.AntennaRefList[0]
             );


  //������ RX Antenna Loss
    Result:= Result + DoGetLoss_Antenna(aBLPoint2,aBLPoint1,
             qry_Antennas2.FieldByName(FLD_POLARIZATION_STR).asString,
             qry_Antennas2,
             qry_Antennas2.FieldByName(FLD_AZIMUTH).AsFloat,
             rAntType2,
             oAntenna2
             //aLinkEnd2.AntennaRefList[0]
             );

    if (not bPolarization_Other_Diagramm) and (bDifferentPolarization) then
    begin


      e:=Abs(gl_DB.GetDoubleFieldValue(TBL_LINKFREQPLAN,
                FLD_PolarizationLevel,
                [db_par(FLD_ID, Params.LinkFreqPlan_ID)]));

      Result:= Result + Abs(gl_DB.GetDoubleFieldValue(TBL_LINKFREQPLAN,
                FLD_PolarizationLevel,
                [db_par(FLD_ID, Params.LinkFreqPlan_ID)]));
    end;

  end
  else
  begin
    dGain1:= DoGetLoss_Antenna(aBLPoint1,aBLPoint2,
           qry_Antennas1.FieldByName(FLD_POLARIZATION_STR).asString,
           qry_Antennas1,
           qry_Antennas1.FieldByName(FLD_AZIMUTH).AsFloat,
           rAntType1,
           oAntenna1 //aLinkEnd1.AntennaRefList[0]
           );

    dGain2:= DoGetLoss_Antenna(aBLPoint2,aBLPoint1,
           qry_Antennas2.FieldByName(FLD_POLARIZATION_STR).asString,
           qry_Antennas2,
           qry_Antennas2.FieldByName(FLD_AZIMUTH).AsFloat,
           rAntType2,
           oAntenna2 //aLinkEnd2.AntennaRefList[0]
           );

    dGain1_Different:= DoGetLoss_Antenna(aBLPoint1,aBLPoint2,
           qry_Antennas2.FieldByName(FLD_POLARIZATION_STR).asString,
           qry_Antennas1,
           qry_Antennas1.FieldByName(FLD_AZIMUTH).AsFloat,
           rAntType1,
           oAntenna1 //aLinkEnd1.AntennaRefList[0]
           );

    dGain2_Different:= DoGetLoss_Antenna(aBLPoint2,aBLPoint1,
           qry_Antennas1.FieldByName(FLD_POLARIZATION_STR).asString,
           qry_Antennas2,
           qry_Antennas2.FieldByName(FLD_AZIMUTH).AsFloat,
           rAntType2,
           oAntenna2 //aLinkEnd2.AntennaRefList[0]
           );

    Result:= Min(dGain1 + dGain2_Different, dGain2 + dGain1_Different);
  end;

end;






//--------------------------------------------------------------------
function TdmLinkFreqPlan_Neighbors_calc.OpenData(
    aLinkEndID1, aLinkEndID2: integer;

    aLinkEnd1,aLinkEnd2: TnbLinkFreqPlan_Linkend;

    aData: TnbData

   // var aTerminated: boolean
    ): boolean;
//--------------------------------------------------------------------
//const
 // SQL_SELECT_LINKENDTYPE =
  //   'SELECT * FROM '+ TBL_LinkEndType +' WHERE id=:id';

const
  SQL_SELECT_LINKEND_ANTENNA =
//     'SELECT * FROM '+ TBL_LINKEND_ANTENNA +' WHERE linkend_id=:linkend_id';

     'SELECT * FROM '+ VIEW_LINKEND_ANTENNAS +' WHERE linkend_id=:linkend_id';


var
  b: boolean;
  iLinkEndType,iPropID1,iPropID2: integer;
  blPos1, blPos2: TBLPoint;
  eFreq_Mhz: Double;
  f1,f2: double;
  k: Integer;

 // iNext_Linkend_id: Integer;

//  oLinkEnd3: TnbLinkFreqPlan_Linkend;

 // eDistanse_m_: Double;
  s: string;
begin
//  raise Exception.Create('');
  Result:= False;


  s:= aLinkEnd1.PROPERTY_name;
  s:= aLinkEnd2.PROPERTY_name;


  iPropID1:= aLinkEnd1.PROPERTY_ID;
  iPropID2:= aLinkEnd2.PROPERTY_ID;

  FIsPropertyOne:=(iPropID1=iPropID2);
  if FIsPropertyOne then
    exit;

  //-------------------------------

  FIsLink:=aLinkEnd1.Link_id = aLinkEnd2.Link_id;

  {

  if not FIsLink then
  begin
    assert (aLinkEnd1.Next_Linkend_id>0);

    aLinkEndID1:=aLinkEnd1.Next_Linkend_id;

    aLinkEnd1:=aData.Linkends.FindByLinkendID(aLinkEnd1.Next_Linkend_id);

  end;
  }

//  Assert(aLinkEnd1.Antennas.Count>0, 'aLinkEnd1.Antennas.Count Value <=0');
//  Assert(aLinkEnd2.Antennas.Count>0, 'aLinkEnd2.Antennas.Count Value <=0');



  dmOnega_db_data.OpenQuery (qry_Antennas1, SQL_SELECT_LINKEND_ANTENNA,
                [FLD_LINKEND_ID, aLinkEndID1]);

  dmOnega_db_data.OpenQuery (qry_Antennas2, SQL_SELECT_LINKEND_ANTENNA,
                [FLD_LINKEND_ID, aLinkEndID2]);

//  Assert (aLinkEnd1.Antennas.Count>0);


  FDBAntennas1.LoadFromDataset(qry_Antennas1);
  FDBAntennas2.LoadFromDataset(qry_Antennas2);


  qry_Antennas1.First;
  qry_Antennas2.First;

  FClutterModelID:=aLinkEnd2.ClutterModel_ID;

{
  FClutterModelID:= gl_DB.GetIntFieldValue(TBL_LINK, FLD_CLUTTER_MODEL_ID,
                                      [db_par(FLD_LINKEND1_ID, aLinkEndID2)]);
  if FClutterModelID=0 then
    FClutterModelID:= gl_DB.GetIntFieldValue(TBL_LINK, FLD_CLUTTER_MODEL_ID,
                                      [db_par(FLD_LINKEND2_ID, aLinkEndID2)]);

}


{
  blPos1 := aLinkEnd1.PROPERTY_BLPoint;
  blPos2 := aLinkEnd2.PROPERTY_BLPoint;


  blPos1 := aLinkEnd1.Antennas[0].BLPoint;
  blPos2 := aLinkEnd2.Antennas[0].BLPoint;

  FDistanse_m:= geo_Distance_m(blPos1, blPos2);

}

  {
  blPos1.B:= qry_Antennas1.FieldByName(FLD_LAT).AsFloat;
  blPos1.L:= qry_Antennas1.FieldByName(FLD_LON).AsFloat;
  blPos2.B:= qry_Antennas2.FieldByName(FLD_LAT).AsFloat;
  blPos2.L:= qry_Antennas2.FieldByName(FLD_LON).AsFloat;
  FDistanse_m:= geo_Distance_m(blPos1, blPos2);
  }

//  Assert (aLinkEnd1.Antennas.Count>0);
//  Assert (aLinkEnd2.Antennas.Count>0);

//  aLinkEnd1.PROPERTY_BLPoint:=aLinkEnd1.Antennas[0].BLPoint;
//  aLinkEnd2.PROPERTY_BLPoint:=aLinkEnd2.Antennas[0].BLPoint;


//  eDistanse_m_:= geo_Distance_m(aLinkEnd1.Antennas[0].BLPoint, aLinkEnd2.Antennas[0].BLPoint);


//  FVector:= MakeBLVector (aLinkEnd1.Antennas[0].BLPoint, aLinkEnd2.Antennas[0].BLPoint);




   FDistanse_m:= geo_Distance_m(aLinkEnd2.BLPoint, aLinkEnd1.BLPoint);


  //assert (abs (FDistanse_m - eDistanse_m_) < 100);

{
  FDistanse_m:= geo_Distance_m(aLinkEnd1.Antennas[0].BLPoint, aLinkEnd2.Antennas[0].BLPoint);
  if FDistanse_m < 10 then
    FDistanse_m :=1;

}

//    if (aLinkEndID1=3378) and (aLinkEndID2=5119) then
//      assert (FDistanse_m>0);



  if FDistanse_m > Params.Calc_Distance_m then
    Exit;




  {
  FIsLink:=aLinkEnd1.Next_Linkend_id = aLinkEnd2.Linkend_id;


  if (gl_DB.GetIntFieldValue(TBL_LINK, FLD_ID,
              [db_par(FLD_LINKEND1_ID, aLinkEndID1),
               db_par(FLD_LINKEND2_ID, aLinkEndID2)])>0)
       or
     (gl_DB.GetIntFieldValue(TBL_LINK, FLD_ID,
              [db_par(FLD_LINKEND1_ID, aLinkEndID2),
               db_par(FLD_LINKEND2_ID, aLinkEndID1)])>0)
  then FIsLink:= True
  else FIsLink:= False;

  }


  FVector:= MakeBLVector (aLinkEnd1.BLPoint, aLinkEnd2.BLPoint);

//  FVector:= MakeBLVector (blPos1, blPos2);


//  db_OpenQu eryByID (qry_LinkType, TBL_LINK_TYPE, iLinkType);
  db_OpenTableByID (qry_LinkEnd1, VIEW_LINKEND,  aLinkEndID1);
  db_OpenTableByID (qry_LinkEnd2, VIEW_LINKEND,  aLinkEndID2);

  Assert ( qry_LinkEnd1.RecordCount>0);
  Assert ( qry_LinkEnd2.RecordCount>0);


//  db_View (qry_LinkEnd1);
//  db_View (qry_LinkEnd2);


  FDBLinkend1.LoadFromDataset(qry_LinkEnd1);
  FDBLinkend2.LoadFromDataset(qry_LinkEnd2);


  iPropID1:= FDBLinkend1.PROPERTY_ID;
  iPropID2:= FDBLinkend2.PROPERTY_ID;



(*
  if iPropID1=iPropID2 then FIsPropertyOne:= True
                       else FIsPropertyOne:= False;


 // iPropID1:= qry_LinkEnd1.FieldValues[FLD_PROPERTY_ID];
 // iPropID2:= qry_LinkEnd2.FieldValues[FLD_PROPERTY_ID];

  if iPropID1=iPropID2 then FIsPropertyOne:= True
                       else FIsPropertyOne:= False;

*)

{  if qry_LinkEnd1.FieldValues[FLD_RANGE] <>     //linkend ������ ����������
     qry_LinkEnd2.FieldValues[FLD_RANGE] then
}

{  if qry_LinkEnd1.FieldValues[FLD_BAND] <>     //linkend ������ ����������
     qry_LinkEnd2.FieldValues[FLD_BAND] then
}

{
  if aLinkEnd1.BAND <>     //linkend ������ ����������
     aLinkEnd2.BAND then

//  if FDBLinkend1.BAND <>     //linkend ������ ����������
 //    FDBLinkend2.BAND then
  begin
//    FIsRange:= False;

    if FIsLink then
    begin
      s:='�� ��������� '+ aLinkEnd1.Link_name +
                // GetNameLink_ByLinkEnd(aLinkEndID1, aLinkEndID2)+
               ' ��� ������ ����������!'+#13#10+'���������� ������?';

      aTerminated:=(MessageDlg (s, mtError, [mbYes,mbNo],0)=mrYes);
    end;

    Exit;
  end else
  ;
 } 
   // FIsRange:= True;


  //get first linkend types
  iLinkEndType:=FDBLinkend1.LINKENDTYPE_ID;

  iLinkEndType:=aLinkEnd2.LINKENDTYPE_ID;

  iLinkEndType:=qry_LinkEnd2.FieldByName(FLD_LINKENDTYPE_ID).AsInteger;

{
  if iLinkEndType = 0 then
  begin
    aTerminated:=(MessageDlg ('�� ��� '''+
              qry_LinkEnd1.FieldByName(FLD_NAME).AsString+
             ''' ��������� ������������!'+#13#10+'���������� ������?',
             mtError, [mbYes,mbNo],0)=mrYes);
    Exit;
  end;
 }

{  db_OpenQuery (qry_LinkEndType, SQL_SELECT_LINKENDTYPE,
                [db_Par(FLD_ID, iLinkEndType)]);
}

  //������� ����������� ���������� ��� � �������� �����������
  FDiagram_level:= Calc_Diagram(FVector.Point1, FVector.Point2, aLinkEnd1, aLinkEnd2);

  b:=FIsLink;

 // FDiagram_level:= Calc_Diagram(blPos1, blPos2, aLinkEnd1, aLinkEnd2);

//  FVector

//  with qry_LinkEnd1 do //������� ����������� !!!
  FFreq_GHz:=TruncFloat(FDBLinkend1.TX_FREQ_MHz / 1000, 2);

  Assert (FFreq_Ghz>0);


//  with qry_LinkEnd1 do //������� ����������� !!!
//    FFreq_GHz:=TruncFloat(FieldByName(FLD_TX_FREQ_MHz).AsFloat/ 1000, 2);

{
  if FFreq_Ghz=0 then
  begin
 //   db_Ope nQuery (qry_LinkEndType, SQL_SELECT_LINKENDTYPE,
   //               [db_Par(FLD_ID, iLinkEndType)]);

//    with qry_LinkEndType do
  //  begin
      //������� ������� ���������

      eFreq_Mhz:=dmLibrary.GetBandAveFreq_MHZ(FDBLinkend1.BAND);

//      FFreq_Ghz:=(FieldByName(FLD_FREQ_MIN).AsFloat + FieldByName(FLD_FREQ_MAX).AsFloat) / 2;

      FFreq_Ghz:=TruncFloat(eFreq_Mhz / 1000, 2);
   // end;
  end;

  Assert (FFreq_Ghz <> 0);
 }

  Result:=True;
end;




//--------------------------------------------------------------------
function TdmLinkFreqPlan_Neighbors_calc.SaveToXML(aFileName, aResult_FileName,
    aLayout_FileName: string; aLinkEnd1,aLinkEnd2: TnbLinkFreqPlan_Linkend):
    boolean;

//--------------------------------------------------------------------

  //--------------------------------------------------------------------
  procedure DoAdd (aNode: IXMLNode; aParamID: integer; aValue: double; aComment: string='');
  //--------------------------------------------------------------------
  begin
    xml_AddNode (aNode, TAG_PARAM, [xml_Att(ATT_id, aParamID),
                                    xml_Att(ATT_value, aValue),
                                    xml_Att(ATT_COMMENT, aComment )
                                    ]);
  end;

  //--------------------------------------------------------------------
  procedure DoAddLinkType (aRoot: IXMLNode; aCalcParam: TrrlCalcParam);
  //--------------------------------------------------------------------
  var vGroup: IXMLNode;
    eLength_KM: double;
  begin
    //RRV: array[1..11] of double; //�������������� ����� �� �������� ��������������� ���������
     {---1=-8.000     ;������� �������� ��������� ����.�������������,10^-8 1/�
      2= 7.000     ;����������� ���������� ���������,10^-8 1/�
      3= 1.000     ;��� ������������ �������.(1...3,0->����.�����=����.���.)
      ---4= 7.500     ;���������� ��������� �������� ����, �/���.�
      5= 1.000     ;Q-������ ������ �����������
      6=41.000     ;������������� ������ K��, 10^-6
      7= 1.500     ;�������� ������������ b ��� ������� ������
      8= 0.500     ;�������� ������������ c ��� ������� ������
      9= 2.000     ;�������� ������������ d ��� ������� ������
      10=20.000    ;������������� ����� � ������� 0.01% �������, ��/���
      11= 1.335    ;�����. ��������� (��� ��.���������=0, ����� �.�.=0)  }
      vGroup:=aRoot.AddChild('RRV');
      vGroup.Attributes['comment']:='������� ��������������� ���������';

//      [xml_ATT ('dist',       0),

      //    <RRV comment="������� ��������������� ���������">

  {
      DoAdd (vGroup, 1,  -8,  '������� �������� ��������� ����.�������������,10^-8 1/�' );
      DoAdd (vGroup, 2,  7,   '����������� ���������� ���������,10^-8 1/�' );

      DoAdd (vGroup, 14,  0,   '��� ���������(0...2) 0-���������� 1-���������� (��������) 2-���������� (������)' );

      DoAdd (vGroup, 3,  1,   '��� ������������ �������.(1...3,0->����.�����=����.���.)' );
      DoAdd (vGroup, 4,  7.5, '���������� ��������� �������� ����, �/���.�');

      DoAdd (vGroup, 12, 15,   '����������� ������� [��.�] (�� ��������� 15)'); // "-1" -������� ������������ �������
      DoAdd (vGroup, 13, 1013, '����������� �������� [����] (�� ��������� 1013)'); // "-1" -������� ������������ �������

      DoAdd (vGroup, 5,  1,   'Q-������ ������ �����������');
      DoAdd (vGroup, 6,  41,  '������������� ������ K��, 10^-6');
      DoAdd (vGroup, 7,  1.5, '�������� ������������ b ��� ������� ������');
      DoAdd (vGroup, 8,  0.5, '�������� ������������ c ��� ������� ������');
      DoAdd (vGroup, 9,  2,   '�������� ������������ d ��� ������� ������');
      DoAdd (vGroup, 10, 20,  '������������� ����� � ������� 0.01% �������, ��/���');
  }



      DoAdd (vGroup, 1,  -10,  '������� �������� ��������� ����.�������������,10^-8 1/�' );
      DoAdd (vGroup, 2,  8,   '����������� ���������� ���������,10^-8 1/�' );


      DoAdd (vGroup, 3,  2,   '��� ������������ �������.(1...3,0->����.�����=����.���.)' );
      DoAdd (vGroup, 4,  7.5, '���������� ��������� �������� ����, �/���.�');


      DoAdd (vGroup, 5,  1,   'Q-������ ������ �����������');
      DoAdd (vGroup, 6,  410, '������������� ������ K��, 10^-6');
      DoAdd (vGroup, 7,  1.5, '�������� ������������ b ��� ������� ������');
      DoAdd (vGroup, 8,  2,   '�������� ������������ c ��� ������� ������');
      DoAdd (vGroup, 9,  2,   '�������� ������������ d ��� ������� ������');
      DoAdd (vGroup, 10, 24.68,  '������������� ����� � ������� 0.01% �������, ��/���');

      DoAdd (vGroup, 12, 15,   '����������� ������� [��.�] (�� ��������� 15)'); // "-1" -������� ������������ �������
      DoAdd (vGroup, 13, 1013, '����������� �������� [����] (�� ��������� 1013)'); // "-1" -������� ������������ �������
      DoAdd (vGroup, 14,  0,   '��� ���������(0...2) 0-���������� 1-���������� (��������) 2-���������� (������)' );

  //    DoAdd (vGroup, 11, -1,  '�����. ��������� (��� ��.���������=0, ����� �.�.=0)'); // "-1" -������� ������������ �������



//          DoAdd (vGroup, 12, RRV.Air_temperature_12,     '����������� ������� [��.�] (�� ��������� 15)');
  //        DoAdd (vGroup, 13, RRV.Atmosphere_pressure_13, '����������� �������� [����] (�� ��������� 1013)');



      aCalcParam.RRV.gradient_diel_1           :=-10; //'������� �������� ��������� ����.�������������,10^-8 1/�' );
      aCalcParam.RRV.gradient_deviation_2      :=8;  //'����������� ���������� ���������,10^-8 1/�' );
      aCalcParam.RRV.terrain_type_14           :=0;  //'��� ���������(0...2) 1-���������� 2-���������� (��������) 3-���������� (������)' );

      aCalcParam.RRV.underlying_terrain_type_3 :=2;  //'��� ������������ �������.(1...3,0->����.�����=����.���.)' );
      aCalcParam.RRV.steam_wet_4               :=7.5;//'���������� ��������� �������� ����, �/���.�');

      aCalcParam.RRV.Air_temperature_12    :=15;
      aCalcParam.RRV.Atmosphere_pressure_13:=1013;

      aCalcParam.RRV.Q_factor_5                :=1;  //'Q-������ ������ �����������');
      aCalcParam.RRV.climate_factor_6          :=410; //'������������� ������ K��, 10^-6');
      aCalcParam.RRV.factor_B_7                :=1.5;//'�������� ������������ b ��� ������� ������');
      aCalcParam.RRV.factor_C_8                :=2;//'�������� ������������ c ��� ������� ������');
      aCalcParam.RRV.FACTOR_D_9                :=2;  //'�������� ������������ d ��� ������� ������');
      aCalcParam.RRV.RAIN_intensity_10         :=24.68; //'������������� ����� � ������� 0.01% �������, ��/���');

 //     aCalcParam.RRV.REFRACTION_11             :=-1; //'�����. ��������� (��� ��.���������=0, ����� �.�.=0)'); // "-1" -������� ������������ �������'�����. ��������� (��� ��.���������=0, ����� �.�.=0)'); // "-1" -������� ������������ �������

//       <param id="12" value="15" comment="����������� ������� [��.�] (�� ��������� 15)"/>
//      <param id="13" value="1013" comment="����������� �������� [����] (�� ��������� 1013)"/>
//      <param id="14" value="0" comment="��� ���������(0...2) 0-���������� 1-���������� (��������) 2-���������� (������)"/>




(*      gradient_diel : double;   // '������� �������� ��������� ����.�������������,10^-8 1/�' );
      gradient      : double;   // '����������� ���������� ���������,10^-8 1/�' );

      underlying_terrain_type: Double; //'��� ������������ �������.(1...3,0->����.�����=����.���.)' );
      terrain_type_14           : Double;   //'��� ���������.(1...3,0->����.�����=����.���.)' );

      steam_wet     : double;   // '���������� ��������� �������� ����, �/���.�');
      Q_factor      : double;   // 'Q-������ ������ �����������');
      climate_factor: double;   //'������������� ������ K��, 10^-6');
      factor_B      : double;   // '�������� ������������ b ��� ������� ������');
      factor_C      : double;   // '�������� ������������ c ��� ������� ������');
      FACTOR_D      : double;   // '�������� ������������ d ��� ������� ������');
      RAIN_RATE     : double;   // '������������� ����� � ������� 0.01% �������, ��/���');
      REFRACTION: double;       //'�����. ��������� (��� ��.���������=0, ����� �.�.=0)');
*)


    //DLT: array[1..4] of double;  //���������� � �������� ��������
     {1= 0.100     ;��� ��������� �������, �� ( 0 - ��������� ����)
      2= 1.000     ;�������� ������ ������� V����(g)=V���.�, ��
      3= 1.000     ;�������� ������ ������� g(V����)=g(V���.�), 10^-8
      4=80.000     ;������������ �������� ��� ������� ������������, 10^-8}
      vGroup:=xml_AddNodeTag (aRoot, 'DLT');
  //    vGroup.Attributes['comment']:='������� ��������������� ���������';

      DoAdd (vGroup,  1, 0.025, '��� ��������� �������, �� ( 0 - ��������� ����)');
      DoAdd (vGroup,  2, 1,     '�������� ������ ������� V����(g)=V���.�, ��');
      DoAdd (vGroup,  3, 1,     '�������� ������ ������� g(V����)=g(V���.�), 10^-8');
      DoAdd (vGroup,  4, 80,    '������������ �������� ��� ������� ������������, 10^-8');
      DoAdd (vGroup,  12, 0,    '0-ITU-R � 16 ���� �� (�� ���������); 1-���� � 53363 - 2009; 2-����-1998; 3-E-�������� (ITU); 4-��� (16�����); ');



      aCalcParam.DLT.Profile_Step_KM_1          :=0.025;
      aCalcParam.DLT.precision_V_diffraction_2  :=1;
      aCalcParam.DLT.precision_g_V_diffraction_3:=1;
      aCalcParam.DLT.max_gradient_for_subrefr_4 :=80;
//      aCalcParam.DLT. max_gradient_for_subrefr_4 :=80;


      aCalcParam.Calc_method:=0;

//      aCalcParam.DLT. gradient_diel_1 :=-8;


    //TRB: array[1..4] of double;  //���������� � ����������� �������� ���
     {1=0.012      ;����������� �������� SESR ��� ����, %
      2=0.05       ;����������� �������� ���  ��� ����, %
      3=600        ;����� ����, ��
      4=0.200      ;���������� ����� ���, ������������� �������������}

      vGroup:=xml_AddNodeTag (aRoot, 'TRB');
      DoAdd (vGroup,  1, 0.006,  '����������� �������� SESR ��� ����, %');
      DoAdd (vGroup,  2, 0.0125,   '����������� �������� ���  ��� ����, %');
      ///!!!!!!!!
    //  aCalcParam.


     eLength_KM:=  geo_Distance_km(FVector);
     if eLength_KM=0 then
       eLength_KM:=0.001;
   //  Assert(eLength_KM>0);


//      DoAdd (vGroup,  3, 600,    '����� ����, ��');
      DoAdd (vGroup,  3, eLength_KM,  '����� ����, ��');

      DoAdd (vGroup,  4, 0.2,    '���������� ����� ���, ������������� �������������}');

      DoAdd (vGroup,  5, 0,      '���������� ������������� ������� p(a%)');
      DoAdd (vGroup,  6, 20,      '���������� ����������� a% ������������� ������������ �������� p(a%) [%]');


//      <param id="5" value="0" comment="���������� ������������� ������� p(a%)"/>
//      <param id="6" value="0" comment="���������� ����������� a% ������������� ������������ �������� p(a%) [%]"/>


      aCalcParam.TRB.GST_SESR_1     :=0.006; //����������� �������� SESR ��� ����, %'
      aCalcParam.TRB.GST_Kng_2      :=0.0125;  //����������� �������� ���  ��� ����, %
//      aCalcParam.TRB.GST_equivalent_length_km_3:=600;   //����� ����, ��


      aCalcParam.TRB.GST_Length_KM_3_:=eLength_KM;

//      aCalcParam.TRB.GST_Length_KM_3:=600;   //����� ����, ��
      aCalcParam.TRB.KNG_dop_4      :=0.2;   //���������� ����� ���, ������������� �������������}

      aCalcParam.TRB.SPACE_LIMIT_5 :=0;   //���������� ������������� ������� p(a%)
      aCalcParam.TRB.SPACE_LIMIT_PROBABILITY_6 :=20;   //���������� ����������� a% ������������� ������������ �������� p(a%) [%]


  end;
  //--------------------------------------------------------------------

var
  vRoot,vGroup,vGroup1: IXMLNode;

  iValue: integer;
  dTHRESHOLD_dBm: double;
  oXMLDoc: TXMLDoc;
 // e: double;

  oCalcParam: TrrlCalcParam;

 // oDBAntenna: TnbAntenna;
  s: string;
  iPOLARIZATION: integer;
  e: double;

begin
  oXMLDoc:=TXMLDoc.Create;

  oCalcParam:=TrrlCalcParam.Create;

  vRoot:=xml_AddNodeTag (oXMLDoc.DocumentElement, 'RRL_PARAMS');

  vRoot.Attributes['IsShowWindow'] := False;

    vRoot.Attributes['Result_FileName'] := ExtractFileName (aResult_FileName);
    vRoot.Attributes['Layout_FileName'] := ExtractFileName (aLayout_FileName);

  oCalcParam.Result_FileName:=aResult_FileName;
  oCalcParam.Layout_FileName:=aLayout_FileName;


  DoAddLinkType (vRoot, oCalcParam);

  // ��� �����������, 0-��������������,1-������������
  // P������ �������, ���
  // �������� ��������, ��/�
  // ������ ���������, ���
  // ������ ���������, ��
  // ��� ������� ���������, 1-���, 2-��, 3-���
  // ����� ������� ���������
  // �������, �������� ������������, ��
  // ��������� ���������� �� �������, 1...8
  // ��������� ���������� �� ������������, 1...2
  // ��������� ������, ���
  vGroup:=xml_AddNodeTag (vRoot, 'TTX');


 // e:=FDBLinkEnd1.Power_dBm;

//  oDBAntenna:=FDBAntennas1[0];

 // Assert(Assigned(oDBAntenna), 'Value not assigned');

  e:=aLinkEnd1.Power_dBm;

  DoAdd (vGroup,  1, qry_LinkEnd1.FieldByName(FLD_POWER_dBm).AsFloat, 'site1-��������');
  with qry_Antennas1 do
  begin
    oCalcParam.TTX.Site1_Power_dBm := qry_LinkEnd1.FieldByName(FLD_POWER_dBm).AsFloat;
    oCalcParam.TTX.Site1_DIAMETER  := FieldByName(FLD_DIAMETER).AsFloat;
    oCalcParam.TTX.Site1_GAIN      := FieldByName(FLD_GAIN).AsFloat;

    oCalcParam.TTX.Site1_LOSS :=
      FieldByName(FLD_LOSS).AsFloat + qry_LinkEnd1.FieldByName(FLD_LOSS).AsFloat;

    oCalcParam.TTX.Site1_VERT_WIDTH:= FieldByName(FLD_VERT_WIDTH).AsFloat;
    oCalcParam.TTX.Site1_HEIGHT    := FieldByName(FLD_HEIGHT).AsFloat;


 //   aLinkEnd1.Power_dBm;


    // ---------------------------------------------------------------

    
    DoAdd (vGroup,  2,  FieldByName(FLD_DIAMETER).AsFloat,   'site1-�������'); //Site1.Antenna_Diameter]);
    DoAdd (vGroup,  3,  FieldByName(FLD_GAIN).AsFloat,       'site1-��������'); // Site1.Antenna_Gain]);
    DoAdd (vGroup,  4,  FieldByName(FLD_VERT_WIDTH).AsFloat, 'site1-������ ���');//  Site1.Antenna_Width]);
    DoAdd (vGroup,  5,  FieldByName(FLD_LOSS).AsFloat+
           qry_LinkEnd1.FieldByName(FLD_LOSS).AsFloat, 'site1-������ � ���');{Site1.Feeder_Losses}

    DoAdd (vGroup,  6,  FieldValues[FLD_HEIGHT],     'site1-������'); // Site1.Antenna_Height]);

  end;

  { xml_AddNode (vGroup, 'param', ['id','value'], [24, Site1.Extra.Antenna_Diameter]);
    xml_AddNode (vGroup, 'param', ['id','value'], [25, Site1.Extra.Antenna_Gain]);
    xml_AddNode (vGroup, 'param', ['id','value'], [26, Site1.Extra.Antenna_Width]);
    xml_AddNode (vGroup, 'param', ['id','value'], [27, Site1.Extra.Feeder_Losses]);
    xml_AddNode (vGroup, 'param', ['id','value'], [28, Site1.Extra.Antenna_Height]);  }

{ TODO : ���������������� ���� ��� BER 10^-3 �.�. qry_Link �����������... �� �������... }


   dTHRESHOLD_dBm:=FDBLinkEnd2.THRESHOLD_BER_3;
   dTHRESHOLD_dBm:=qry_LinkEnd2.FieldByName(FLD_THRESHOLD_BER_3).AsInteger;

//    1: dTHRESHOLD_dBm:=qry_LinkEnd2.FieldByName(FLD_THRESHOLD_BER_6).AsInteger;

  DoAdd (vGroup,  7,  dTHRESHOLD_dBm,  'site2-������');

  //oDBAntenna:=FDBAntennas2[0];

//  Assert(Assigned(oDBAntenna), 'Value not assigned');



  with qry_Antennas2 do
  begin
    oCalcParam.TTX.Site2_Threshold_dBM := dTHRESHOLD_dBm;
    oCalcParam.TTX.Site2_DIAMETER  := FieldByName(FLD_DIAMETER).AsFloat;
    oCalcParam.TTX.Site2_GAIN      := FieldByName(FLD_GAIN).AsFloat;
    oCalcParam.TTX.Site2_LOSS      := FieldByName(FLD_LOSS).AsFloat+
                                      qry_LinkEnd2.FieldByName(FLD_LOSS).AsFloat;
    oCalcParam.TTX.Site2_VERT_WIDTH:= FieldByName(FLD_VERT_WIDTH).AsFloat;
    oCalcParam.TTX.Site2_HEIGHT    := FieldByName(FLD_HEIGHT).AsFloat;

    // ---------------------------------------------------------------

    DoAdd (vGroup,  8,  FieldByName(FLD_DIAMETER).AsFloat,  'site2-�������'); //Site1.Antenna_Diameter]);
    DoAdd (vGroup,  9,  FieldByName(FLD_GAIN).AsFloat,      'site2-��������'); // Site1.Antenna_Gain]);
    DoAdd (vGroup,  10, FieldByName(FLD_VERT_WIDTH).AsFloat,'site2-������ ���');//  Site1.Antenna_Width]);
    DoAdd (vGroup,  11, FieldByName(FLD_LOSS).AsFloat+
           qry_LinkEnd2.FieldByName(FLD_LOSS).AsFloat, 'site2-������ � ���');{Site1.Feeder_Losses}

    DoAdd (vGroup,  12, FieldByName(FLD_HEIGHT).AsFloat,    'site2-������'); // Site1.Antenna_Height]);

  end;

{   xml_AddNode (vGroup, 'param', ['id','value'], [7,  Site2.Sensitivity]);
    xml_AddNode (vGroup, 'param', ['id','value'], [8,  Site2.Antenna_Diameter]);
    xml_AddNode (vGroup, 'param', ['id','value'], [9,  Site2.Antenna_Gain]);
    xml_AddNode (vGroup, 'param', ['id','value'], [10, Site2.Antenna_Width]);
    xml_AddNode (vGroup, 'param', ['id','value'], [11, Site2.Feeder_Losses]);
    xml_AddNode (vGroup, 'param', ['id','value'], [12, Site2.Antenna_Height]); }

  s := qry_Antennas1.FieldByName(FLD_POLARIZATION_sTR).AsString;

  if Eq(s,'h') then begin
    iPOLARIZATION :=0;
    oCalcParam.TTX.POLARIZATION_type :=ptH_;
  end else
  if Eq(s,'v') then begin
    iPOLARIZATION :=1;
    oCalcParam.TTX.POLARIZATION_type :=ptV_;
  end else begin
    iPOLARIZATION :=2;
    oCalcParam.TTX.POLARIZATION_type :=ptX_;
  end;


  oCalcParam.TTX.POLARIZATION :=iPOLARIZATION;

//  oCalcParam.TTX.POLARIZATION := qry_Antennas1.FieldByName(FLD_POLARIZATION).AsInteger;

  oCalcParam.TTX.Freq_GHz := FFreq_GHz;

  DoAdd (vGroup, 13, iPOLARIZATION, '��� �����������, 0-��������������,1-������������,2-�����');
  DoAdd (vGroup, 14, FFreq_GHz,     'P������ �������, ���');


  with qry_LinkEnd1 do
  begin
    oCalcParam.TTX.SIGNATURE_WIDTH_Mhz  :=FieldByName(FLD_SIGNATURE_WIDTH).AsFloat;
    oCalcParam.TTX.SIGNATURE_HEIGHT_dB :=FieldByName(FLD_SIGNATURE_HEIGHT).AsFloat;

//    oCalcParam.TTX.MODULATION_COUNT  :=1;
    oCalcParam.TTX.MODULATION_Level_COUNT  :=1;

 //   oCalcParam.TTX.EQUALISER_PROFIT  :=FieldByName(FLD_EQUALISER_PROFIT).AsFloat;

    oCalcParam.TTX.KRATNOST_BY_FREQ :=FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger + 1;


    oCalcParam.TTX.BitRate_Mbps :=FieldByName(FLD_BitRate_Mbps).AsFloat;


    DoAdd (vGroup, 15, FieldByName(FLD_BitRate_Mbps).AsFloat,  '�������� ��������, ��/�');



    DoAdd (vGroup, 16, FieldByName(FLD_SIGNATURE_WIDTH).AsFloat,  '������ ���������, ���');
    DoAdd (vGroup, 17, FieldByName(FLD_SIGNATURE_HEIGHT).AsFloat, '������ ���������, ��');

    DoAdd (vGroup, 18, 0, '��� ���������, 0-PSK (QPSK), 1-QAM (TCM)');

//          <param id="18" value="0" comment="��� ���������, 0-PSK (QPSK), 1-QAM (TCM)"/>


    DoAdd (vGroup, 19, 1,                                         '����� ������� ���������');
//    DoAdd (vGroup, 20, FieldByName(FLD_EQUALISER_PROFIT).AsFloat, '�������, �������� ������������, ��');
    DoAdd (vGroup, 20, 0, '�������, �������� ������������, ��');
    DoAdd (vGroup, 21, FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger + 1, '��������� ���������� �� �������, 1...8');  //�� ���

//    DoAdd (vGroup, 23, FieldValues[FLD_Freq_Spacing], '��������� ������, ���');

  end;

  with qry_LinkEnd2 do
  begin
    iValue:=FDBLinkEnd2.KRATNOST_BY_SPACE;

    iValue:=FieldByName(FLD_KRATNOST_BY_SPACE).AsInteger;
    iValue:=iValue+1;
    DoAdd (vGroup, 22, iValue,  '��������� ���������� �� ������������, 1...2'); //�� ���
  end;


  with qry_LinkEnd1 do
  begin
    oCalcParam.TTX.Freq_Spacing_MHz :=FieldByName(FLD_Freq_Spacing).AsFloat;

    DoAdd (vGroup, 23, FieldByName(FLD_Freq_Spacing).AsFloat, '��������� ������, ���');

  end;


//  oDBAntenna:=FDBAntennas1[0];


  if qry_Antennas1.RecordCount>1 then
    with qry_Antennas1 do
    begin
      Next;

      DoAdd (vGroup,  24,  FieldByName(FLD_DIAMETER).AsFloat,   'dop-site1-�������');   //Site1.Antenna_Diameter]);
      DoAdd (vGroup,  25,  FieldByName(FLD_GAIN).AsFloat,       'dop-site1-��������');  // Site1.Antenna_Gain]);
      DoAdd (vGroup,  26,  FieldByName(FLD_VERT_WIDTH).AsFloat, 'dop-site1-������ ���');//  Site1.Antenna_Width]);
      DoAdd (vGroup,  27,  FieldByName(FLD_LOSS).AsFloat,       'dop-site1-������ � ���');{Site1.Feeder_Losses}
      DoAdd (vGroup,  28,  FieldByName(FLD_HEIGHT).AsFloat,     'dop-site1-������');    // Site1.Antenna_Height]);

      if aLinkEnd1.Antennas.Count>1 then
      begin
  //      oDBAntenna:=aLinkEnd1.Antennas[1];

      //  oCalcParam.TTX

    //    FblVectorForOffset.Point2:=oDBAntenna.BLPoint;

  //      aObj.TTX.Dop_Site2.DIAMETER  :=   oDBAntenna.DIAMETER;
  //      aObj.TTX.Dop_Site2.GAIN      :=   oDBAntenna.GAIN;
  //      aObj.TTX.Dop_Site2.VERT_WIDTH:=   oDBAntenna.VERT_WIDTH;
  //      aObj.TTX.Dop_Site2.LOSS      :=   oDBAntenna.Loss_dB;
  //      aObj.TTX.Dop_Site2.HEIGHT    :=   oDBAntenna.HEIGHT;

      end;



    end;


  if qry_Antennas2.RecordCount>1 then
    with qry_Antennas2 do
    begin
      Next;

      DoAdd (vGroup,  29,  FieldByName(FLD_DIAMETER).AsFloat,   'dop-site2-�������'); //Site1.Antenna_Diameter]);
      DoAdd (vGroup,  30,  FieldByName(FLD_GAIN).AsFloat,       'dop-site2-��������'); // Site1.Antenna_Gain]);
      DoAdd (vGroup,  31,  FieldByName(FLD_VERT_WIDTH).AsFloat, 'dop-site2-������ ���');//  Site1.Antenna_Width]);
      DoAdd (vGroup,  32,  FieldByName(FLD_LOSS).AsFloat,    'dop-site2-������ � ���');{Site1.Feeder_Losses}
      DoAdd (vGroup,  33,  FieldByName(FLD_HEIGHT).AsFloat,     'dop-site2-������'); // Site1.Antenna_Height]);

    end;

//  oCalcParam. TTX. BitRate_Mbps :=FieldByName(FLD_BitRate_Mbps).AsFloat;


 {
  DoAdd (vGroup, 34, qry_Antennas1.FieldValues[FLD_FrontToBackRatio],  'ant1 - ����.���.�������� (������/�����) ���-�������, ��');
  DoAdd (vGroup, 35, qry_Antennas2.FieldValues[FLD_FrontToBackRatio],  'ant2 - ����.���.�������� (������/�����) ���-�������, ��');

  DoAdd (vGroup, 36, qry_Antennas1.FieldValues[FLD_FrontToBackRatio],  'ant1 - ����.���.�������� (������/�����) ���-�������, ��');
  DoAdd (vGroup, 37, qry_Antennas2.FieldValues[FLD_FrontToBackRatio],  'ant2 - ����.���.�������� (������/�����) ���-�������, ��');
 }

  oCalcParam.TTX.Site1_KNG:=qry_LinkEnd1.FieldBYName(FLD_KNG).AsFloat;
  oCalcParam.TTX.Site2_KNG:=qry_LinkEnd2.FieldBYName(FLD_KNG).AsFloat;


//  DoAdd (vGroup, 38, TTX.Site1_KNG1,  '��� ������������ (%)1');
//  DoAdd (vGroup, 39, TTX.Site2_KNG1,  '��� ������������ (%)2');


  DoAdd (vGroup, 38, qry_LinkEnd1.FieldBYName(FLD_KNG).AsFloat,  '��� ������������ (%)1');
  DoAdd (vGroup, 39, qry_LinkEnd2.FieldBYName(FLD_KNG).AsFloat,  '��� ������������ (%)2');

  if not SaveProfileToXMLNode (vRoot, 25, oCalcParam) then  { TODO : ��� ���� ����� � ������� �������. �.�.}
    Exit;

    {
  oXMLDoc.SaveToFile(aFileName);

  oCalcParam.SaveToXML( ChangeFileExt(aFileName, '_.xml') );
}

  oCalcParam.SaveToXML( ChangeFileExt(aFileName, '.xml') );

  oXMLDoc.Free;

  Result:= True;

  FreeAndNil(oCalcParam);

end;



//--------------------------------------------------------------------
function TdmLinkFreqPlan_Neighbors_calc.SaveToXML_new(aFileName: string;
    aLinkEnd1,aLinkEnd2: TnbLinkFreqPlan_Linkend): boolean;
//--------------------------------------------------------------------

  //--------------------------------------------------------------------
  procedure DoAdd (aNode: IXMLNode; aParamID: integer; aValue: double; aComment: string='');
  //--------------------------------------------------------------------
  begin
    xml_AddNode (aNode, TAG_PARAM, [xml_Att(ATT_id, aParamID),
                                    xml_Att(ATT_value, aValue),
                                    xml_Att(ATT_COMMENT, aComment )
                                    ]);
  end;

  //--------------------------------------------------------------------
  procedure DoAddLinkType (aRoot: IXMLNode; aCalcParam: TrrlCalcParam);
  //--------------------------------------------------------------------
  var vGroup: IXMLNode;
    eLength_KM: double;
  begin
    //RRV: array[1..11] of double; //�������������� ����� �� �������� ��������������� ���������
     {---1=-8.000     ;������� �������� ��������� ����.�������������,10^-8 1/�
      2= 7.000     ;����������� ���������� ���������,10^-8 1/�
      3= 1.000     ;��� ������������ �������.(1...3,0->����.�����=����.���.)
      ---4= 7.500     ;���������� ��������� �������� ����, �/���.�
      5= 1.000     ;Q-������ ������ �����������
      6=41.000     ;������������� ������ K��, 10^-6
      7= 1.500     ;�������� ������������ b ��� ������� ������
      8= 0.500     ;�������� ������������ c ��� ������� ������
      9= 2.000     ;�������� ������������ d ��� ������� ������
      10=20.000    ;������������� ����� � ������� 0.01% �������, ��/���
      11= 1.335    ;�����. ��������� (��� ��.���������=0, ����� �.�.=0)  }
      vGroup:=aRoot.AddChild('RRV');
      vGroup.Attributes['comment']:='������� ��������������� ���������';

//      [xml_ATT ('dist',       0),

      //    <RRV comment="������� ��������������� ���������">

  {
      DoAdd (vGroup, 1,  -8,  '������� �������� ��������� ����.�������������,10^-8 1/�' );
      DoAdd (vGroup, 2,  7,   '����������� ���������� ���������,10^-8 1/�' );

      DoAdd (vGroup, 14,  0,   '��� ���������(0...2) 0-���������� 1-���������� (��������) 2-���������� (������)' );

      DoAdd (vGroup, 3,  1,   '��� ������������ �������.(1...3,0->����.�����=����.���.)' );
      DoAdd (vGroup, 4,  7.5, '���������� ��������� �������� ����, �/���.�');

      DoAdd (vGroup, 12, 15,   '����������� ������� [��.�] (�� ��������� 15)'); // "-1" -������� ������������ �������
      DoAdd (vGroup, 13, 1013, '����������� �������� [����] (�� ��������� 1013)'); // "-1" -������� ������������ �������

      DoAdd (vGroup, 5,  1,   'Q-������ ������ �����������');
      DoAdd (vGroup, 6,  41,  '������������� ������ K��, 10^-6');
      DoAdd (vGroup, 7,  1.5, '�������� ������������ b ��� ������� ������');
      DoAdd (vGroup, 8,  0.5, '�������� ������������ c ��� ������� ������');
      DoAdd (vGroup, 9,  2,   '�������� ������������ d ��� ������� ������');
      DoAdd (vGroup, 10, 20,  '������������� ����� � ������� 0.01% �������, ��/���');
  }



      DoAdd (vGroup, 1,  -10,  '������� �������� ��������� ����.�������������,10^-8 1/�' );
      DoAdd (vGroup, 2,  8,   '����������� ���������� ���������,10^-8 1/�' );

      DoAdd (vGroup, 14,  0,   '��� ���������(0...2) 0-���������� 1-���������� (��������) 2-���������� (������)' );

      DoAdd (vGroup, 3,  2,   '��� ������������ �������.(1...3,0->����.�����=����.���.)' );
      DoAdd (vGroup, 4,  7.5, '���������� ��������� �������� ����, �/���.�');

      DoAdd (vGroup, 12, 15,   '����������� ������� [��.�] (�� ��������� 15)'); // "-1" -������� ������������ �������
      DoAdd (vGroup, 13, 1013, '����������� �������� [����] (�� ��������� 1013)'); // "-1" -������� ������������ �������

      DoAdd (vGroup, 5,  1,   'Q-������ ������ �����������');
      DoAdd (vGroup, 6,  410, '������������� ������ K��, 10^-6');
      DoAdd (vGroup, 7,  1.5, '�������� ������������ b ��� ������� ������');
      DoAdd (vGroup, 8,  2,   '�������� ������������ c ��� ������� ������');
      DoAdd (vGroup, 9,  2,   '�������� ������������ d ��� ������� ������');
      DoAdd (vGroup, 10, 24.68,  '������������� ����� � ������� 0.01% �������, ��/���');
  //    DoAdd (vGroup, 11, -1,  '�����. ��������� (��� ��.���������=0, ����� �.�.=0)'); // "-1" -������� ������������ �������



//          DoAdd (vGroup, 12, RRV.Air_temperature_12,     '����������� ������� [��.�] (�� ��������� 15)');
  //        DoAdd (vGroup, 13, RRV.Atmosphere_pressure_13, '����������� �������� [����] (�� ��������� 1013)');



      aCalcParam.RRV.gradient_diel_1           :=-10; //'������� �������� ��������� ����.�������������,10^-8 1/�' );
      aCalcParam.RRV.gradient_deviation_2      :=8;  //'����������� ���������� ���������,10^-8 1/�' );
      aCalcParam.RRV.terrain_type_14           :=0;  //'��� ���������(0...2) 1-���������� 2-���������� (��������) 3-���������� (������)' );

      aCalcParam.RRV.underlying_terrain_type_3 :=2;  //'��� ������������ �������.(1...3,0->����.�����=����.���.)' );
      aCalcParam.RRV.steam_wet_4               :=7.5;//'���������� ��������� �������� ����, �/���.�');

      aCalcParam.RRV.Air_temperature_12    :=15;
      aCalcParam.RRV.Atmosphere_pressure_13:=1013;

      aCalcParam.RRV.Q_factor_5                :=1;  //'Q-������ ������ �����������');
      aCalcParam.RRV.climate_factor_6          :=410; //'������������� ������ K��, 10^-6');
      aCalcParam.RRV.factor_B_7                :=1.5;//'�������� ������������ b ��� ������� ������');
      aCalcParam.RRV.factor_C_8                :=2;//'�������� ������������ c ��� ������� ������');
      aCalcParam.RRV.FACTOR_D_9                :=2;  //'�������� ������������ d ��� ������� ������');
      aCalcParam.RRV.RAIN_intensity_10         :=24.68; //'������������� ����� � ������� 0.01% �������, ��/���');

 //     aCalcParam.RRV.REFRACTION_11             :=-1; //'�����. ��������� (��� ��.���������=0, ����� �.�.=0)'); // "-1" -������� ������������ �������'�����. ��������� (��� ��.���������=0, ����� �.�.=0)'); // "-1" -������� ������������ �������

//       <param id="12" value="15" comment="����������� ������� [��.�] (�� ��������� 15)"/>
//      <param id="13" value="1013" comment="����������� �������� [����] (�� ��������� 1013)"/>
//      <param id="14" value="0" comment="��� ���������(0...2) 0-���������� 1-���������� (��������) 2-���������� (������)"/>




(*      gradient_diel : double;   // '������� �������� ��������� ����.�������������,10^-8 1/�' );
      gradient      : double;   // '����������� ���������� ���������,10^-8 1/�' );

      underlying_terrain_type: Double; //'��� ������������ �������.(1...3,0->����.�����=����.���.)' );
      terrain_type_14           : Double;   //'��� ���������.(1...3,0->����.�����=����.���.)' );

      steam_wet     : double;   // '���������� ��������� �������� ����, �/���.�');
      Q_factor      : double;   // 'Q-������ ������ �����������');
      climate_factor: double;   //'������������� ������ K��, 10^-6');
      factor_B      : double;   // '�������� ������������ b ��� ������� ������');
      factor_C      : double;   // '�������� ������������ c ��� ������� ������');
      FACTOR_D      : double;   // '�������� ������������ d ��� ������� ������');
      RAIN_RATE     : double;   // '������������� ����� � ������� 0.01% �������, ��/���');
      REFRACTION: double;       //'�����. ��������� (��� ��.���������=0, ����� �.�.=0)');
*)


    //DLT: array[1..4] of double;  //���������� � �������� ��������
     {1= 0.100     ;��� ��������� �������, �� ( 0 - ��������� ����)
      2= 1.000     ;�������� ������ ������� V����(g)=V���.�, ��
      3= 1.000     ;�������� ������ ������� g(V����)=g(V���.�), 10^-8
      4=80.000     ;������������ �������� ��� ������� ������������, 10^-8}
      vGroup:=xml_AddNodeTag (aRoot, 'DLT');
  //    vGroup.Attributes['comment']:='������� ��������������� ���������';

      DoAdd (vGroup,  1, 0.025, '��� ��������� �������, �� ( 0 - ��������� ����)');
      DoAdd (vGroup,  2, 1,     '�������� ������ ������� V����(g)=V���.�, ��');
      DoAdd (vGroup,  3, 1,     '�������� ������ ������� g(V����)=g(V���.�), 10^-8');
      DoAdd (vGroup,  4, 80,    '������������ �������� ��� ������� ������������, 10^-8');
      DoAdd (vGroup,  12, 0,    '0-ITU-R � 16 ���� �� (�� ���������); 1-���� � 53363 - 2009; 2-����-1998; 3-E-�������� (ITU); 4-��� (16�����); ');



      aCalcParam.DLT.Profile_Step_KM_1          :=0.025;
      aCalcParam.DLT.precision_V_diffraction_2  :=1;
      aCalcParam.DLT.precision_g_V_diffraction_3:=1;
      aCalcParam.DLT.max_gradient_for_subrefr_4 :=80;
//      aCalcParam.DLT. max_gradient_for_subrefr_4 :=80;


      aCalcParam.Calc_method:=0;

//      aCalcParam.DLT. gradient_diel_1 :=-8;


    //TRB: array[1..4] of double;  //���������� � ����������� �������� ���
     {1=0.012      ;����������� �������� SESR ��� ����, %
      2=0.05       ;����������� �������� ���  ��� ����, %
      3=600        ;����� ����, ��
      4=0.200      ;���������� ����� ���, ������������� �������������}

      vGroup:=xml_AddNodeTag (aRoot, 'TRB');
      DoAdd (vGroup,  1, 0.006,  '����������� �������� SESR ��� ����, %');
      DoAdd (vGroup,  2, 0.0125,   '����������� �������� ���  ��� ����, %');
      ///!!!!!!!!
    //  aCalcParam.


     eLength_KM:=  geo_Distance_km(FVector);

     Assert(eLength_KM>0);


//      DoAdd (vGroup,  3, 600,    '����� ����, ��');
      DoAdd (vGroup,  3, eLength_KM,  '����� ����, ��');

      DoAdd (vGroup,  4, 0.2,    '���������� ����� ���, ������������� �������������}');

      DoAdd (vGroup,  5, 0,      '���������� ������������� ������� p(a%)');
      DoAdd (vGroup,  6, 20,      '���������� ����������� a% ������������� ������������ �������� p(a%) [%]');


//      <param id="5" value="0" comment="���������� ������������� ������� p(a%)"/>
//      <param id="6" value="0" comment="���������� ����������� a% ������������� ������������ �������� p(a%) [%]"/>


      aCalcParam.TRB.GST_SESR_1     :=0.006; //����������� �������� SESR ��� ����, %'
      aCalcParam.TRB.GST_Kng_2      :=0.0125;  //����������� �������� ���  ��� ����, %
//      aCalcParam.TRB.GST_equivalent_length_km_3:=600;   //����� ����, ��


      aCalcParam.TRB.GST_Length_KM_3_:=eLength_KM;

//      aCalcParam.TRB.GST_Length_KM_3:=600;   //����� ����, ��
      aCalcParam.TRB.KNG_dop_4      :=0.2;   //���������� ����� ���, ������������� �������������}

      aCalcParam.TRB.SPACE_LIMIT_5 :=0;   //���������� ������������� ������� p(a%)
      aCalcParam.TRB.SPACE_LIMIT_PROBABILITY_6 :=20;   //���������� ����������� a% ������������� ������������ �������� p(a%) [%]


  end;
  //--------------------------------------------------------------------

var
  vRoot,vGroup,vGroup1: IXMLNode;

  iValue: integer;
  dTHRESHOLD_dBm: double;
  oXMLDoc: TXMLDoc;
 // e: double;

  oCalcParam: TrrlCalcParam;

 // oDBAntenna: TnbAntenna;
  s: string;
  iPOLARIZATION: integer;
  e: double;

begin
  oXMLDoc:=TXMLDoc.Create;

  oCalcParam:=TrrlCalcParam.Create;

  vRoot:=xml_AddNodeTag (oXMLDoc.DocumentElement, 'RRL_PARAMS');

  vRoot.Attributes['IsShowWindow'] := False;

  DoAddLinkType (vRoot, oCalcParam);

  // ��� �����������, 0-��������������,1-������������
  // P������ �������, ���
  // �������� ��������, ��/�
  // ������ ���������, ���
  // ������ ���������, ��
  // ��� ������� ���������, 1-���, 2-��, 3-���
  // ����� ������� ���������
  // �������, �������� ������������, ��
  // ��������� ���������� �� �������, 1...8
  // ��������� ���������� �� ������������, 1...2
  // ��������� ������, ���
  vGroup:=xml_AddNodeTag (vRoot, 'TTX');


 // e:=FDBLinkEnd1.Power_dBm;

//  oDBAntenna:=FDBAntennas1[0];

 // Assert(Assigned(oDBAntenna), 'Value not assigned');

  e:=aLinkEnd1.Power_dBm;

//  DoAdd (vGroup,  1, qry_LinkEnd1.FieldByName(FLD_POWER_dBm).AsFloat, 'site1-��������');
  DoAdd (vGroup,  1, qry_LinkEnd1.FieldByName(FLD_POWER_dBm).AsFloat, 'site1-��������');

  with qry_Antennas1 do
  begin


     {
    oCalcParam.TTX.Site1_Power_dBm :=aLinkEnd1.Power_dBm;
    oCalcParam.TTX.Site1_DIAMETER  :=aLinkEnd1.an;
    oCalcParam.TTX.Site1_GAIN      :=
    }

    oCalcParam.TTX.Site1_Power_dBm := qry_LinkEnd1.FieldByName(FLD_POWER_dBm).AsFloat;
    oCalcParam.TTX.Site1_DIAMETER  := FieldByName(FLD_DIAMETER).AsFloat;
    oCalcParam.TTX.Site1_GAIN      := FieldByName(FLD_GAIN).AsFloat;

    oCalcParam.TTX.Site1_LOSS :=
      FieldByName(FLD_LOSS).AsFloat + qry_LinkEnd1.FieldByName(FLD_LOSS).AsFloat;

    oCalcParam.TTX.Site1_VERT_WIDTH:= FieldByName(FLD_VERT_WIDTH).AsFloat;
    oCalcParam.TTX.Site1_HEIGHT    := FieldByName(FLD_HEIGHT).AsFloat;


 //   aLinkEnd1.Power_dBm;


    // ---------------------------------------------------------------

    
    DoAdd (vGroup,  2,  FieldByName(FLD_DIAMETER).AsFloat,   'site1-�������'); //Site1.Antenna_Diameter]);
    DoAdd (vGroup,  3,  FieldByName(FLD_GAIN).AsFloat,       'site1-��������'); // Site1.Antenna_Gain]);
    DoAdd (vGroup,  4,  FieldByName(FLD_VERT_WIDTH).AsFloat, 'site1-������ ���');//  Site1.Antenna_Width]);
    DoAdd (vGroup,  5,  FieldByName(FLD_LOSS).AsFloat+
           qry_LinkEnd1.FieldByName(FLD_LOSS).AsFloat, 'site1-������ � ���');{Site1.Feeder_Losses}

    DoAdd (vGroup,  6,  FieldValues[FLD_HEIGHT],     'site1-������'); // Site1.Antenna_Height]);

  end;

  { xml_AddNode (vGroup, 'param', ['id','value'], [24, Site1.Extra.Antenna_Diameter]);
    xml_AddNode (vGroup, 'param', ['id','value'], [25, Site1.Extra.Antenna_Gain]);
    xml_AddNode (vGroup, 'param', ['id','value'], [26, Site1.Extra.Antenna_Width]);
    xml_AddNode (vGroup, 'param', ['id','value'], [27, Site1.Extra.Feeder_Losses]);
    xml_AddNode (vGroup, 'param', ['id','value'], [28, Site1.Extra.Antenna_Height]);  }

{ TODO : ���������������� ���� ��� BER 10^-3 �.�. qry_Link �����������... �� �������... }


   dTHRESHOLD_dBm:=FDBLinkEnd2.THRESHOLD_BER_3;
   dTHRESHOLD_dBm:=qry_LinkEnd2.FieldByName(FLD_THRESHOLD_BER_3).AsInteger;

//    1: dTHRESHOLD_dBm:=qry_LinkEnd2.FieldByName(FLD_THRESHOLD_BER_6).AsInteger;

  DoAdd (vGroup,  7,  dTHRESHOLD_dBm,  'site2-������');

  //oDBAntenna:=FDBAntennas2[0];

//  Assert(Assigned(oDBAntenna), 'Value not assigned');



  with qry_Antennas2 do
  begin
    oCalcParam.TTX.Site2_Threshold_dBM := dTHRESHOLD_dBm;
    oCalcParam.TTX.Site2_DIAMETER  := FieldByName(FLD_DIAMETER).AsFloat;
    oCalcParam.TTX.Site2_GAIN      := FieldByName(FLD_GAIN).AsFloat;
    oCalcParam.TTX.Site2_LOSS      := FieldByName(FLD_LOSS).AsFloat + qry_LinkEnd2.FieldByName(FLD_LOSS).AsFloat;
    oCalcParam.TTX.Site2_VERT_WIDTH:= FieldByName(FLD_VERT_WIDTH).AsFloat;
    oCalcParam.TTX.Site2_HEIGHT    := FieldByName(FLD_HEIGHT).AsFloat;

    // ---------------------------------------------------------------

    DoAdd (vGroup,  8,  FieldByName(FLD_DIAMETER).AsFloat,  'site2-�������'); //Site1.Antenna_Diameter]);
    DoAdd (vGroup,  9,  FieldByName(FLD_GAIN).AsFloat,      'site2-��������'); // Site1.Antenna_Gain]);
    DoAdd (vGroup,  10, FieldByName(FLD_VERT_WIDTH).AsFloat,'site2-������ ���');//  Site1.Antenna_Width]);
    DoAdd (vGroup,  11, FieldByName(FLD_LOSS).AsFloat+
           qry_LinkEnd2.FieldByName(FLD_LOSS).AsFloat, 'site2-������ � ���');{Site1.Feeder_Losses}

    DoAdd (vGroup,  12, FieldByName(FLD_HEIGHT).AsFloat,    'site2-������'); // Site1.Antenna_Height]);

  end;

{   xml_AddNode (vGroup, 'param', ['id','value'], [7,  Site2.Sensitivity]);
    xml_AddNode (vGroup, 'param', ['id','value'], [8,  Site2.Antenna_Diameter]);
    xml_AddNode (vGroup, 'param', ['id','value'], [9,  Site2.Antenna_Gain]);
    xml_AddNode (vGroup, 'param', ['id','value'], [10, Site2.Antenna_Width]);
    xml_AddNode (vGroup, 'param', ['id','value'], [11, Site2.Feeder_Losses]);
    xml_AddNode (vGroup, 'param', ['id','value'], [12, Site2.Antenna_Height]); }

  s := qry_Antennas1.FieldByName(FLD_POLARIZATION_sTR).AsString;

  if Eq(s,'h') then begin
    iPOLARIZATION :=0;
    oCalcParam.TTX.POLARIZATION_type :=ptH_;
  end else
  if Eq(s,'v') then begin
    iPOLARIZATION :=1;
    oCalcParam.TTX.POLARIZATION_type :=ptV_;
  end else begin
    iPOLARIZATION :=2;
    oCalcParam.TTX.POLARIZATION_type :=ptX_;
  end;


  oCalcParam.TTX.POLARIZATION :=iPOLARIZATION;

//  oCalcParam.TTX.POLARIZATION := qry_Antennas1.FieldByName(FLD_POLARIZATION).AsInteger;

  oCalcParam.TTX.Freq_GHz := FFreq_GHz;

  DoAdd (vGroup, 13, iPOLARIZATION, '��� �����������, 0-��������������,1-������������,2-�����');
  DoAdd (vGroup, 14, FFreq_GHz,     'P������ �������, ���');


  with qry_LinkEnd1 do
  begin
    oCalcParam.TTX.SIGNATURE_WIDTH_Mhz  :=FieldByName(FLD_SIGNATURE_WIDTH).AsFloat;
    oCalcParam.TTX.SIGNATURE_HEIGHT_dB :=FieldByName(FLD_SIGNATURE_HEIGHT).AsFloat;

//    oCalcParam.TTX.MODULATION_COUNT  :=1;
    oCalcParam.TTX.MODULATION_Level_COUNT  :=1;

 //   oCalcParam.TTX.EQUALISER_PROFIT  :=FieldByName(FLD_EQUALISER_PROFIT).AsFloat;

    oCalcParam.TTX.KRATNOST_BY_FREQ :=FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger + 1;


    oCalcParam.TTX.BitRate_Mbps :=FieldByName(FLD_BitRate_Mbps).AsFloat;


    DoAdd (vGroup, 15, FieldByName(FLD_BitRate_Mbps).AsFloat,  '�������� ��������, ��/�');



    DoAdd (vGroup, 16, FieldByName(FLD_SIGNATURE_WIDTH).AsFloat,  '������ ���������, ���');
    DoAdd (vGroup, 17, FieldByName(FLD_SIGNATURE_HEIGHT).AsFloat, '������ ���������, ��');

    DoAdd (vGroup, 18, 0, '��� ���������, 0-PSK (QPSK), 1-QAM (TCM)');

//          <param id="18" value="0" comment="��� ���������, 0-PSK (QPSK), 1-QAM (TCM)"/>


    DoAdd (vGroup, 19, 1,                                         '����� ������� ���������');
//    DoAdd (vGroup, 20, FieldByName(FLD_EQUALISER_PROFIT).AsFloat, '�������, �������� ������������, ��');
    DoAdd (vGroup, 20, 0, '�������, �������� ������������, ��');
    DoAdd (vGroup, 21, FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger + 1, '��������� ���������� �� �������, 1...8');  //�� ���

//    DoAdd (vGroup, 23, FieldValues[FLD_Freq_Spacing], '��������� ������, ���');

  end;

  with qry_LinkEnd2 do
  begin
    iValue:=FDBLinkEnd2.KRATNOST_BY_SPACE;

    iValue:=FieldByName(FLD_KRATNOST_BY_SPACE).AsInteger;
    iValue:=iValue+1;
    DoAdd (vGroup, 22, iValue,  '��������� ���������� �� ������������, 1...2'); //�� ���
  end;


  with qry_LinkEnd1 do
  begin
    oCalcParam.TTX.Freq_Spacing_MHz :=FieldByName(FLD_Freq_Spacing).AsFloat;

    DoAdd (vGroup, 23, FieldByName(FLD_Freq_Spacing).AsFloat, '��������� ������, ���');

  end;


//  oDBAntenna:=FDBAntennas1[0];


  if qry_Antennas1.RecordCount>1 then
    with qry_Antennas1 do
    begin
      Next;

      DoAdd (vGroup,  24,  FieldByName(FLD_DIAMETER).AsFloat,   'dop-site1-�������');   //Site1.Antenna_Diameter]);
      DoAdd (vGroup,  25,  FieldByName(FLD_GAIN).AsFloat,       'dop-site1-��������');  // Site1.Antenna_Gain]);
      DoAdd (vGroup,  26,  FieldByName(FLD_VERT_WIDTH).AsFloat, 'dop-site1-������ ���');//  Site1.Antenna_Width]);
      DoAdd (vGroup,  27,  FieldByName(FLD_LOSS).AsFloat,    'dop-site1-������ � ���');{Site1.Feeder_Losses}
      DoAdd (vGroup,  28,  FieldByName(FLD_HEIGHT).AsFloat,     'dop-site1-������');    // Site1.Antenna_Height]);

      if aLinkEnd1.Antennas.Count>1 then
      begin
  //      oDBAntenna:=aLinkEnd1.Antennas[1];

      //  oCalcParam.TTX

    //    FblVectorForOffset.Point2:=oDBAntenna.BLPoint;

  //      aObj.TTX.Dop_Site2.DIAMETER  :=   oDBAntenna.DIAMETER;
  //      aObj.TTX.Dop_Site2.GAIN      :=   oDBAntenna.GAIN;
  //      aObj.TTX.Dop_Site2.VERT_WIDTH:=   oDBAntenna.VERT_WIDTH;
  //      aObj.TTX.Dop_Site2.LOSS      :=   oDBAntenna.Loss_dB;
  //      aObj.TTX.Dop_Site2.HEIGHT    :=   oDBAntenna.HEIGHT;

      end;



    end;


  if qry_Antennas2.RecordCount>1 then
    with qry_Antennas2 do
    begin
      Next;

      DoAdd (vGroup,  29,  FieldByName(FLD_DIAMETER).AsFloat,   'dop-site2-�������'); //Site1.Antenna_Diameter]);
      DoAdd (vGroup,  30,  FieldByName(FLD_GAIN).AsFloat,       'dop-site2-��������'); // Site1.Antenna_Gain]);
      DoAdd (vGroup,  31,  FieldByName(FLD_VERT_WIDTH).AsFloat, 'dop-site2-������ ���');//  Site1.Antenna_Width]);
      DoAdd (vGroup,  32,  FieldByName(FLD_LOSS).AsFloat,    'dop-site2-������ � ���');{Site1.Feeder_Losses}
      DoAdd (vGroup,  33,  FieldByName(FLD_HEIGHT).AsFloat,     'dop-site2-������'); // Site1.Antenna_Height]);

    end;

//  oCalcParam. TTX. BitRate_Mbps :=FieldByName(FLD_BitRate_Mbps).AsFloat;


 {
  DoAdd (vGroup, 34, qry_Antennas1.FieldValues[FLD_FrontToBackRatio],  'ant1 - ����.���.�������� (������/�����) ���-�������, ��');
  DoAdd (vGroup, 35, qry_Antennas2.FieldValues[FLD_FrontToBackRatio],  'ant2 - ����.���.�������� (������/�����) ���-�������, ��');

  DoAdd (vGroup, 36, qry_Antennas1.FieldValues[FLD_FrontToBackRatio],  'ant1 - ����.���.�������� (������/�����) ���-�������, ��');
  DoAdd (vGroup, 37, qry_Antennas2.FieldValues[FLD_FrontToBackRatio],  'ant2 - ����.���.�������� (������/�����) ���-�������, ��');
 }

  oCalcParam.TTX.Site1_KNG:=qry_LinkEnd1.FieldBYName(FLD_KNG).AsFloat;
  oCalcParam.TTX.Site2_KNG:=qry_LinkEnd2.FieldBYName(FLD_KNG).AsFloat;


//  DoAdd (vGroup, 38, TTX.Site1_KNG1,  '��� ������������ (%)1');
//  DoAdd (vGroup, 39, TTX.Site2_KNG1,  '��� ������������ (%)2');


  DoAdd (vGroup, 38, qry_LinkEnd1.FieldBYName(FLD_KNG).AsFloat,  '��� ������������ (%)1');
  DoAdd (vGroup, 39, qry_LinkEnd2.FieldBYName(FLD_KNG).AsFloat,  '��� ������������ (%)2');

  if not SaveProfileToXMLNode (vRoot, 25, oCalcParam) then  { TODO : ��� ���� ����� � ������� �������. �.�.}
    Exit;

  oXMLDoc.SaveToFile(aFileName);


  oCalcParam.SaveToXML( ChangeFileExt(aFileName, '_.xml') );


  oXMLDoc.Free;

  Result:= True;

  FreeAndNil(oCalcParam);

end;



//--------------------------------------------------------------------
function TdmLinkFreqPlan_Neighbors_calc.SaveProfileToXMLNode(vRoot: IXMLNode;
    aStep_M: integer; aCalcParam: TrrlCalcParam): boolean;
//--------------------------------------------------------------------
var i: integer;
  vGroup: IXMLNode;
begin
  Result:= False;

  aCalcParam.Relief.Length_KM:=  geo_Distance_km(FVector);

  if FIsPropertyOne then
  begin
 //   if FDistanse_m>10 then
  //    raise Exception.Create('');


    vGroup:=xml_AddNode (vRoot, 'relief', [xml_Att('count', 2) ]);

  //  FDistanse_m :=50;
    FDistanse_m :=0;  //!!!!!!!!!!

    SetLength( aCalcParam.Relief.Items, 2);


    aCalcParam.Relief.Items[0].Distance_KM:=0;
    aCalcParam.Relief.Items[1].Distance_KM:=0.001;


    xml_AddNode (vGroup, 'item',
                 [xml_ATT ('dist',       0),
                  xml_ATT ('rel_h',      0),
                  xml_ATT ('local_h',    0),
                  xml_ATT ('local_code', 0) ]);

    xml_AddNode (vGroup, 'item',
 //                [xml_ATT ('dist',       IIF(FDistanse_m<1, 1, FDistanse_m)),
                 [xml_ATT ('dist',       0.001),
                  xml_ATT ('rel_h',      0),
                  xml_ATT ('local_h',    0),
                  xml_ATT ('local_code', 0) ]);

    Result:= True;

    Exit;
  end;

  dmRel_Engine.AssignClutterModel (FClutterModelID, FrelProfile.Clutters );
  dmRel_Engine.BuildProfile1 (FrelProfile, FVector, aStep_M); //, FClutterModelID);

  if FrelProfile.Count=0 then
    Exit;


//       <relief count="281" Length="13.959" Length_km="13.959">



  vGroup:=xml_AddNode (vRoot, 'relief',
                       [
                        xml_Att('count',     FrelProfile.Count),
                        xml_Att('Length',    FrelProfile.Data.Distance_KM),
                        xml_Att('Length_km', FrelProfile.Data.Distance_KM)
                       ]);


  SetLength( aCalcParam.Relief.Items, FrelProfile.Data.Count);



  with FrelProfile.Data do
    for i:=0 to Count-1 do
  begin
    aCalcParam.Relief.Items[i].Distance_KM :=TruncFloat(Items[i].Distance_KM, 3);
    aCalcParam.Relief.Items[i].rel_h       :=Items[i].Rel_H;
    aCalcParam.Relief.Items[i].Clutter_h   :=Items[i].Clutter_H;
    aCalcParam.Relief.Items[i].Clutter_Code:=Items[i].Clutter_Code;


//    xml_AddNode (vGroup, 'item',
    xml_AddNode (vGroup, 'relief',    

       [xml_ATT ('dist',       TruncFloat(Items[i].Distance_KM, 3)),
        xml_ATT ('rel_h',      Items[i].Rel_H ),
        xml_ATT ('local_h',    Items[i].Clutter_H ),
        xml_ATT ('local_code', Items[i].Clutter_Code)
       ]);

  end;

  Result:= True;
end;



//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_Neighbors_calc.LoadCalcResultsFromXML(
//aID1, aID2: integer;

aDBNeighbor: TnbNeighbor; aFileName: string);

//--------------------------------------------------------------------
var // iValue,i,j,iID: integer;
 //  vNode,vGroup,vRoot,vRoot1: IXMLNode;
   eRx_Level_dBm,dSESR,dKng: double;
 //  dPolarization_Ratio: double;
 // eValue: Double;

 // oXMLDoc: TXMLDoc;//('');

  oLinkCalcResult: TLinkCalcResult;
//
//const
//  DEF_RX_LEVEL_DBM = 101; //��������� ������� ������� �� ����� ��������� (���)
//  DEF_SESR  = 17;
//  DEF_KNG   = 19;


begin
  Assert(FileExists(aFileName), aFileName);
  //-----------------------------

  oLinkCalcResult:=TLinkCalcResult.create;
  oLinkCalcResult.LoadFromXmlFile_RRL_Result(aFileName);
//  oLinkCalcResult.

//  oLinkCalcResult.

  eRx_Level_dBm:=oLinkCalcResult.Data.Rx_Level_dBm;
  dSESR:=oLinkCalcResult.Data.SESR;
  dKng:=oLinkCalcResult.Data.Kng;

//eRx_Level_dBm := eValue;
//dSESR := eValue;
//dKng  := eValue;
//

  FreeAndNil(oLinkCalcResult);

  ////////////////////////////

  {
  oXMLDoc:=TXMLDoc.Create;

  oXMLDoc.LoadFromFile(aFileName);

  vRoot:=oXMLDoc.DocumentElement;

  if vRoot.ChildNodes.Count>0 then
  begin
//    vRoot := oXMLDoc.DocumentElement;
    vRoot1:= vRoot.childNodes[0];

//    iID:= DoAdd_Neighbors(aID1, aID2);

    for i:=0 to vRoot1.ChildNodes.Count-1 do
    begin
      vGroup:=vRoot1.ChildNodes[i];

      for j:=0 to vGroup.ChildNodes.Count-1 do
      begin
        vNode:=vGroup.ChildNodes[j];

        iID   :=xml_GetIntAttr(vNode, 'id');
        eValue:=xml_GetFloatAttr(vNode, 'value');

        case iID of
          DEF_RX_LEVEL_DBM: eRx_Level_dBm := eValue;
          DEF_SESR        : dSESR := eValue;
          DEF_KNG         : dKng  := eValue;
        end;

//        if iID = DEF_RX_LEVEL_DBM then
  //        eRx_Level_dBm := (xml_GetFloatAttr(vNode, 'value')) else

//        if iID = DEF_SESR then
//          dSESR := (xml_GetFloatAttr(vNode, 'value')) else
//
//        if iID = DEF_KNG then
//          dKng := (xml_GetFloatAttr(vNode, 'value'));

      end;
    end;
  }

  aDBNeighbor.SESR:=dSESR;
  aDBNeighbor.Kng:=dKng;
  aDBNeighbor.Rx_Level_dBm:=eRx_Level_dBm;

  aDBNeighbor.IsPropertyOne:=FIsPropertyOne;
  aDBNeighbor.IsLink       :=FIsLink;


//    iID1:=3378;



//  aDBNeighbor.Distanse_m  :=FDistanse_m;
  aDBNeighbor.Diagram_level:=FDiagram_level;




 //   eRx_Level_dBm:=Round(eRx_Level_dBm);
 //   dSESR :=TruncFloat(dSESR, 5);
 //   dKng  :=TruncFloat(dKng,  5);

(*   iID:= DoAdd_Neighbors(aID1, aID2);

    gl_DB.UpdateRecord(TBL_LINKFREQPLAN_NEIGHBORS, iID,
                  [db_Par(FLD_RX_LEVEL_DBM,  eRx_Level_dBm), //POWER_dBm
                   db_Par(FLD_SESR,          dSESR),
                   db_Par(FLD_Kng,           dKng),

                   db_Par(FLD_IsPropertyOne, FIsPropertyOne),
                   db_Par(FLD_IsLink,        FIsLink),

                   db_Par(FLD_Distance_M,    FDistanse_m), // geo_Distance(FVector)),
                   db_Par(FLD_diagram_level, FDiagram_level)
                  ]);
*)

{
   dmOnega_DB_data.ExecStoredProc(SP_LinkFreqPlan_Neighbors_add,
                  [
                   db_Par (FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID),

                   db_Par (FLD_TX_LINKEND_ID, aID1),
                   db_Par (FLD_RX_LINKEND_ID, aID2),
               //    db_Par (FLD_TX_LINK_ID, iID22),

                   db_Par(FLD_RX_LEVEL_DBM,  eRx_Level_dBm), //POWER_dBm
                   db_Par(FLD_SESR,          dSESR),
                   db_Par(FLD_Kng,           dKng),

                   db_Par(FLD_IsPropertyOne, FIsPropertyOne),
                   db_Par(FLD_IsLink,        FIsLink),

                   db_Par(FLD_Distance_M,    FDistanse_m), // geo_Distance(FVector)),
                   db_Par(FLD_diagram_level, FDiagram_level)
                  ]);
}
//  end;

//  oXMLDoc.Free;
end;



//--------------------------------------------------------------------
function TdmLinkFreqPlan_Neighbors_calc.Execute(aLinkEnd_ID1, aLinkEnd_ID2:
    integer; aLinkEnd1,aLinkEnd2: TnbLinkFreqPlan_Linkend;

    aDBNeighbor: TnbNeighbor;

    aData: TnbData;

    var aTerminated: boolean): boolean;
//--------------------------------------------------------------------
const

//  TEMP_IN_FILENAME = 'rrl_neighb_in.xml';
//  TEMP_OUT_FILENAME = 'rrl_neighb_res.xml';

  TEMP_IN_FILENAME = 'rrl_neighb_in_%d_%d.xml';
  TEMP_OUT_FILENAME = 'rrl_neighb_res_%d_%d.xml';

  TEMP_IN_layout_XML  = 'rrl_layout.xml';



var sFile1,sFile2: string;
  eDistanse_m: Double;
    id_parent: integer;
  k: Integer;
  s: string;
  s1: string;
  sFile_layout: string;
begin
  Result:=False;


//  if (aLinkEnd1.Name='1') and (aLinkEnd2.Name='2') then
//     k:=0;

  { TODO : clutters }
//  dmRel_Engine.OpenAll;
   {
   if aLinkEnd1.PROPERTY_ID =aLinkEnd2.PROPERTY_ID then
   begin
    FIsPropertyOne:=true;

    aDBNeighbor.IsPropertyOne:=true;
    exit;
   end;
   }

//  FIsLink:=aLinkEnd1.Link_id = aLinkEnd2.Link_id;

  FIsLink:=aLinkEnd1.Link_id = aLinkEnd2.Link_id;

  aDBNeighbor.IsLink:=aLinkEnd1.Link_id = aLinkEnd2.Link_id;

  if aLinkEnd1.Link_id <> aLinkEnd2.Link_id then
  begin
    assert (aLinkEnd1.Next_Linkend_id>0);

    aLinkEnd_ID1:=aLinkEnd1.Next_Linkend_id;

    aLinkEnd1:=aData.Linkends.FindByLinkendID(aLinkEnd1.Next_Linkend_id);

    aDBNeighbor.RX_for_LINKEND2:=aLinkEnd1;


  end;

  aDBNeighbor.Distanse_m:= geo_Distance_m(aLinkEnd1.BLPoint, aLinkEnd2.BLPoint);


  //--------------------------------------------

   if aLinkEnd1.PROPERTY_ID =aLinkEnd2.PROPERTY_ID then
   begin
    FIsPropertyOne:=true;

    aDBNeighbor.IsPropertyOne:=true;
//    aDBNeighbor.Distanse_m:=0;
    exit;
   end;



  eDistanse_m:= geo_Distance_m(aLinkEnd1.BLPoint, aLinkEnd2.BLPoint);

//  if FDistanse_m > Params.Calc_Distance_m then
 //   Exit;




  OpenData (aLinkEnd_ID1, aLinkEnd_ID2, aLinkEnd1,aLinkEnd2, aData) ;


//  if not OpenData (aLinkEnd_ID1, aLinkEnd_ID2, aLinkEnd1,aLinkEnd2, aData, aTerminated) then
//    Exit;
  {

   if FIsPropertyOne then
   begin
    aDBNeighbor.IsPropertyOne:=FIsPropertyOne;
    exit;

  //  aDBNeighbor.IsLink       :=FIsLink;

   end;
   }




(*  if FIsPropertyOne then
  begin
    if not FIsRange then FIsPropertyOne:= False; //��������� �� Prop �����������...

    id_parent:= DoAdd_Neighbors(aLinkEnd_ID1, aLinkEnd_ID2);
    gl_DB.UpdateRecord(TBL_LINK_FREQPLAN_NEIGHBORS, id_parent,
                [db_Par(FLD_IsPropertyOne, FIsPropertyOne),
                 db_Par(FLD_IsLink,        FIsLink),
                 db_Par(FLD_Distance,      0) ]);
  end
  else
  begin*)



(*    sFile1:=GetTempFileDir + TEMP_IN_FILENAME;
    sFile2:=GetTempFileDir + TEMP_OUT_FILENAME;
*)


  //  sIniFile := g_ApplicationDataDir + TEMP_INI_FILE;

//  TEMP_IN_FILENAME = 'rrl_neighb_in.xml';
//  TEMP_OUT_FILENAME = 'rrl_neighb_res.xml';


(*

s:=Format('linkend_%d_%d__property_%d_%d.xml', [
     aLinkEnd1.ID, aLinkEnd2.ID,
     aLinkEnd1.PROPERTY_ID, aLinkEnd2.PROPERTY_ID
     ]);// TEMP_IN_FILENAME;

    sFile1:=g_ApplicationDataDir + Format('rrl_neighb_in_%s.xml', [s]);// TEMP_IN_FILENAME;
    sFile2:=g_ApplicationDataDir + Format('rrl_neighb_res_%s.xml', [s]);// TEMP_OUT_FILENAME;
*)

//    sFile1:=g_ApplicationDataDir + TEMP_IN_FILENAME;
//    sFile2:=g_ApplicationDataDir + TEMP_OUT_FILENAME;



    sFile1       :=g_ApplicationDataDir + Format(TEMP_IN_FILENAME,  [aLinkEnd_ID1, aLinkEnd_ID2]);
    sFile2       :=g_ApplicationDataDir + Format(TEMP_OUT_FILENAME, [aLinkEnd_ID1, aLinkEnd_ID2]);
    sFile_layout :=g_ApplicationDataDir + TEMP_IN_layout_XML;



 ////////   ShellExec_Notepad(sFile1);


  if not FileExists(sFile_layout) then
    CalcMethod_LoadFromXMLFile(
        GetApplicationDir + 'link_calc_model_setup\RRL_ParamSys.xml',
        0, //oCalcParams.Calc_method,
        sFile_layout
        );

    if not SaveToXML (sFile1, sFile2, sFile_layout,  aLinkEnd1, aLinkEnd2) then
      Exit;


    Run_rpls_rrl  (sFile1); //, sFile2, sFile_layout);


   {
    if RunApp (GetApplicationDir() + EXE_RPLS_RRL,
                  DoubleQuotedStr(sFile1) +' '+
                  DoubleQuotedStr(sFile2))
    then
   }

  LoadCalcResultsFromXML (aDBNeighbor, sFile2); //..aLinkEnd_ID1, aLinkEnd_ID2, 

///  end;
  Result:=True;
end;


// ---------------------------------------------------------------
function TdmLinkFreqPlan_Neighbors_calc.Run_rpls_rrl(aFile_XML: string):
    Boolean;
// ---------------------------------------------------------------
var
  sRunPath: string;

//  oIni_rpls_rrl_params: TIni_rpls_rrl_params;
 // sFile: string;

begin
  Assert(aFile_XML<>'');
//  Assert(aFile2<>'');
//  Assert(aFile_layout<>'');


//  oIni_rpls_rrl_params:=TIni_rpls_rrl_params.Create;


  sRunPath := GetApplicationDir() + EXE_RPLS_RRL;

  Result := True;

  {$DEFINE use_dll111}

  {$IFDEF use_dll}
   {
    if Load_Irpls_rrl() then
    begin
    //  Irpls_rrl.InitADO(dmMain.ADOConnection1.ConnectionObject);
   //   ILink_graph.InitAppHandle(Application.Handle);
      Irpls_rrl.Execute(sFile);

      UnLoad_Irpls_rrl();
    end;
    }
  //  Exit;

  {$ELSE}

  //  oIni_rpls_rrl_params.In_FileName := aFile_XML;
//    oIni_rpls_rrl_params.out_FileName := aFile2;
//    oIni_rpls_rrl_params.OUTREZ_FileName := aFile_layout;


  //  sFile :=g_ApplicationDataDir + 'rrl_nb.ini';

  //  oIni_rpls_rrl_params.SaveToFile(sFile);

    Result := RunAppEx (sRunPath,  [aFile_XML]);

 //   Result := RunAppEx (sRunPath,  [aFile_XML, aFile2, '', aFile_layout]);

//                      DoubleQuotedStr(aFile_XML) +' '+
  //                    DoubleQuotedStr(aFile2)) ;

//    Result := RunApp (sRunPath,
//                      DoubleQuotedStr(aFile_XML) +' '+
//                      DoubleQuotedStr(aFile2)) ;


  //  if RunApp (GetApplicationDir() + EXE_RPLS_RRL, sFile1 +' '+ aFile2) then


  {$ENDIF}

//  FreeAndNil(oIni_rpls_rrl_params);

end;


begin

end.



//--------------------------------------------------------------------
function TdmLinkFreqPlan_Neighbors_calc.Validate: Integer;
//--------------------------------------------------------------------
begin

(*

 for I := 0 to FLinkFreqPlan_Linkends.Count - 1 do
    if not Terminated then
  begin
    DoProgress(i, FLinkFreqPlan_Linkends.Count);

    for j := 0 to FLinkFreqPlan_Linkends.Count - 1 do
      if (i<>j) and (not Terminated) then
    begin
      DoProgress(j, FLinkFreqPlan_Linkends.Count);

      iID1:=FLinkFreqPlan_Linkends[i].Linkend_id;
      iID2:=FLinkFreqPlan_Linkends[j].Linkend_id;



  if aLinkEnd1.BAND <>     //linkend ������ ����������
     aLinkEnd2.BAND then

//  if FDBLinkend1.BAND <>     //linkend ������ ����������
 //    FDBLinkend2.BAND then
  begin
//    FIsRange:= False;

    if FIsLink then
    begin
      s:='�� ��������� '+ aLinkEnd1.Link_name +
                // GetNameLink_ByLinkEnd(aLinkEndID1, aLinkEndID2)+
               ' ��� ������ ����������!'+#13#10+'���������� ������?';

      aTerminated:=(MessageDlg (s, mtError, [mbYes,mbNo],0)=mrYes);
    end;

    Exit;
  end else
  ;
   // FIsRange:= True;


  //get first linkend types
  iLinkEndType:=FDBLinkend1.LINKENDTYPE_ID;

  iLinkEndType:=aLinkEnd1.LINKENDTYPE_ID;

  iLinkEndType:=qry_LinkEnd1.FieldByName(FLD_LINKENDTYPE_ID).AsInteger;

  if iLinkEndType = 0 then
  begin
    aTerminated:=(MessageDlg ('�� ��� '''+
              qry_LinkEnd1.FieldByName(FLD_NAME).AsString+
             ''' ��������� ������������!'+#13#10+'���������� ������?',
             mtError, [mbYes,mbNo],0)=mrYes);
    Exit;
  end;

*)


end;


