unit d_LinkFreqPlan_calc_data_test;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls,

  dm_LinkFreqPlan_Neighbors, Grids, DBGrids, DB, rxPlacemnt
  ;

type
  Tdlg_LinkFreqPlan_NB_calc_data_test = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    ds_LinkFreqPlan: TDataSource;
    ds_LinkFreqPlan_LinkEnd: TDataSource;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    TabSheet3: TTabSheet;
    FormStorage1: TFormStorage;
    ds_Neighbors: TDataSource;
    DBGrid3: TDBGrid;
  private
    { Private declarations }
  public
    class procedure ExecDlg;

  end;

(*var
  dlg_LinkFreqPlan_calc_data_test: Tdlg_LinkFreqPlan_calc_data_test;
*)
implementation

{$R *.dfm}

class procedure Tdlg_LinkFreqPlan_NB_calc_data_test.ExecDlg;
begin
  with Tdlg_LinkFreqPlan_NB_calc_data_test.Create(Application) do
  begin
    ShowModal;

    Free;
  end;
  
end;

end.
