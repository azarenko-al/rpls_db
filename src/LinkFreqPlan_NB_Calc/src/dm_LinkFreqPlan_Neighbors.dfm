inherited dmLinkFreqPlan_Neighbors: TdmLinkFreqPlan_Neighbors
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 1024
  Top = 458
  Height = 366
  Width = 576
  object qry_FreqPlan_LinkEnd: TADOQuery
    Parameters = <>
    Left = 456
    Top = 16
  end
  object qry_Neighbors: TADOQuery
    Parameters = <>
    Left = 256
    Top = 16
  end
  object qry_LinkEnd1: TADOQuery
    Parameters = <>
    Left = 32
    Top = 160
  end
  object qry_LinkEnd2: TADOQuery
    Parameters = <>
    Left = 32
    Top = 212
  end
  object qry_Mode1: TADOQuery
    Parameters = <>
    Left = 140
    Top = 156
  end
  object qry_Mode2: TADOQuery
    Parameters = <>
    Left = 140
    Top = 212
  end
  object qry_Band1: TADOQuery
    Parameters = <>
    Left = 240
    Top = 160
  end
  object qry_LinkFreqPlan: TADOQuery
    Parameters = <>
    Left = 348
    Top = 128
  end
  object qry_Antennas: TADOQuery
    Parameters = <>
    Left = 464
    Top = 192
  end
  object ds_FreqPlan_LinkEnd: TDataSource
    DataSet = qry_FreqPlan_LinkEnd
    Left = 456
    Top = 72
  end
  object ds_Antennas: TDataSource
    DataSet = qry_Antennas
    Left = 464
    Top = 248
  end
end
