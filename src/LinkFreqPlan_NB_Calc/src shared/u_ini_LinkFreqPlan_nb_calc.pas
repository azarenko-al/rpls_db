unit u_ini_LinkFreqPlan_nb_calc;

interface
uses
   IniFiles, SysUtils, Dialogs,

   u_assert
   ;

type
  TIni_LinkFreqPlan_nb_calc_Params = class(TObject)
  public
    ConnectionString : string;

    ProjectID      : Integer;
    LinkFreqPlanID : integer;
    IsFullCalc     : boolean;

    procedure LoadFromFile(aFileName: string);
    procedure SaveToFile(aFileName: string);

    function Validate: boolean;
  end;


implementation


// ---------------------------------------------------------------
procedure TIni_LinkFreqPlan_nb_calc_Params.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
var
  oIniFile: TIniFile;
begin
  AssertFileExists(aFileName);



  oIniFile := TIniFile.Create(aFileName);

  with oIniFile do
  begin
    LinkFreqPlanID :=ReadInteger('main', 'LinkFreqPlanID',  0);
    IsFullCalc     :=ReadBool   ('main', 'IsFullCalc',  False);
    ProjectID      :=ReadInteger('main', 'ProjectID',  0);

   ConnectionString     :=ReadString   ('main', 'ConnectionString',  '');



  end;


  assert(ProjectID>0);


  FreeAndNil(oIniFile);
end;

// ---------------------------------------------------------------
procedure TIni_LinkFreqPlan_nb_calc_Params.SaveToFile(aFileName: string);
// ---------------------------------------------------------------
var
  oIniFile: TIniFile;
begin
//  assert(ProjectID>0);


  oIniFile := TIniFile.Create(aFileName);

  with oIniFile do
  begin
    WriteInteger('main', 'LinkFreqPlanID',  LinkFreqPlanID);
    WriteBool   ('main', 'IsFullCalc',  IsFullCalc);

    WriteString ('main', 'ConnectionString', ConnectionString);

    WriteInteger('main', 'ProjectID', ProjectID );
  end;

  FreeAndNil(oIniFile);
end;


// ---------------------------------------------------------------
function TIni_LinkFreqPlan_nb_calc_Params.Validate: boolean;
// ---------------------------------------------------------------
begin
  Result := (LinkFreqPlanID>0); // and (ProjectID>0);

  if not Result then
    ShowMessage('(LinkFreqPlanID=0) ');  //or (ProjectID=0)
end;


end.

(*



  oParams:=TIni_LinkFreqPlan_nb_calc_Params.Create;
  oParams.LoadFromFile(sIniFileName);
  FreeAndNil(oParams);

*)
