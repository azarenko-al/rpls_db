unit d_SelectItemByList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,

  u_Storage,

  dm_Onega_DB_data,

  u_const_db,

  u_db,
  u_func,

    DB, ADODB, rxPlacemnt, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, StdCtrls, ExtCtrls;


type
  Tdlg_SelectItemByList_rec = record
    Object_field_id : Integer;
    Caption        : string;

    Result : record
              ID    : Integer;
              Value : string;
             end;
  end;


type
  Tdlg_SelectItemByList = class(TForm)
    pn_Header: TPanel;
    pn_Buttons: TPanel;
    FormStorage1: TFormStorage;
    Bevel1: TBevel;
    ds_Items: TDataSource;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxGrid1DBTableView1value: TcxGridDBColumn;
    ADOStoredProc1: TADOStoredProc;
    Panel1: TPanel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    procedure FormDestroy(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FRegPath : string;

  public
    class function ExecDlg(var aRec: Tdlg_SelectItemByList_rec): Boolean;
  end;


//==============================================================================
//==============================================================================
implementation {$R *.DFM}



//------------------------------------------------------------------------------
class function Tdlg_SelectItemByList.ExecDlg(var aRec:
    Tdlg_SelectItemByList_rec): Boolean;
//------------------------------------------------------------------------------
begin
  with Tdlg_SelectItemByList.Create(Application) do
  try

    if dmOnega_DB_data.GroupAssign_Select_subitems(ADOStoredProc1,  aRec.Object_field_id) > 0 then
      ADOStoredProc1.Locate(FLD_ID, aRec.Result.id, []);

    Result := ShowModal=mrOk;

    if Result then
    begin
      aRec.Result.ID    := ADOStoredProc1.FieldByName(FLD_ID).AsInteger;
      aRec.Result.Value := ADOStoredProc1.FieldByName(FLD_NAME).AsString;
    end;


  finally
    Free;
  end;
end;



//------------------------------------------------------------------------------
procedure Tdlg_SelectItemByList.FormCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  Caption:='����� ���������';
//  dxDBGrid.Align:= alClient;
  cxGrid1.Align:= alClient;

 // db_SetComponentADOConnection(Self, dmMain.ADOConnection);


 // FRegPath:=REGISTRY_COMMON_FORMS + ClassName;

  g_Storage.RestoreFromRegistry(cxGrid1DBTableView1, ClassName);

//  cxGridDBTableView1.RestoreFromRegistry(FRegPath + '\'+ cxGridDBTableView1.Name);
 // cxGridDBTableView2.RestoreFromRegistry(FRegPath + '\'+ cxGridDBTableView2.Name);

end;


procedure Tdlg_SelectItemByList.FormDestroy(Sender: TObject);
begin
  g_Storage.StoreToRegistry(cxGrid1DBTableView1, ClassName);

//  cxGridDBTableView1.StoreToRegistry(FRegPath + '\'+ cxGridDBTableView1.Name, True,  [gsoUseFilter]);
//  cxGridDBTableView2.StoreToRegistry(FRegPath + '\'+ cxGridDBTableView2.Name, True,  [gsoUseFilter]);

end;



procedure Tdlg_SelectItemByList.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  btn_Ok.Click;
end;


end.