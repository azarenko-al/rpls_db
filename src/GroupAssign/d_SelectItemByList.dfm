object dlg_SelectItemByList: Tdlg_SelectItemByList
  Left = 680
  Top = 339
  Width = 604
  Height = 413
  Caption = 'dlg_SelectItemByList'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object pn_Header: TPanel
    Left = 0
    Top = 0
    Width = 596
    Height = 57
    Align = alTop
    Color = clAppWorkSpace
    TabOrder = 0
    Visible = False
  end
  object pn_Buttons: TPanel
    Left = 0
    Top = 345
    Width = 596
    Height = 34
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 596
      Height = 2
      Align = alTop
    end
    object Panel1: TPanel
      Left = 424
      Top = 2
      Width = 172
      Height = 32
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btn_Ok: TButton
        Left = 6
        Top = 5
        Width = 75
        Height = 23
        Caption = 'Ok'
        Default = True
        ModalResult = 1
        TabOrder = 0
      end
      object btn_Cancel: TButton
        Left = 88
        Top = 5
        Width = 75
        Height = 23
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        ModalResult = 2
        TabOrder = 1
      end
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 191
    Width = 596
    Height = 154
    Align = alBottom
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelInner = bvNone
    TabOrder = 2
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnDblClick = cxGrid1DBTableView1DblClick
      DataController.DataSource = ds_Items
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      Filtering.MaxDropDownCount = 12
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Editing = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GridLineColor = clGray
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.Indicator = True
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object cxGrid1DBTableView1id: TcxGridDBColumn
        Caption = #8470
        DataBinding.FieldName = 'id'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        HeaderAlignmentHorz = taCenter
        MinWidth = 16
        Options.Filtering = False
        Width = 33
      end
      object cxGrid1DBTableView1value: TcxGridDBColumn
        Caption = #1047#1085#1072#1095#1077#1085#1080#1077' '#1087#1072#1088#1072#1084#1077#1090#1088#1072
        DataBinding.FieldName = 'name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taCenter
        SortIndex = 0
        SortOrder = soAscending
        Width = 431
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 224
    Top = 8
  end
  object ds_Items: TDataSource
    DataSet = ADOStoredProc1
    Left = 40
    Top = 112
  end
  object ADOStoredProc1: TADOStoredProc
    Parameters = <>
    Left = 40
    Top = 64
  end
end
