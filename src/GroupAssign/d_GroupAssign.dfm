object dlg_GroupAssign: Tdlg_GroupAssign
  Left = 679
  Top = 368
  Width = 689
  Height = 456
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'dlg_GroupAssign'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object pn_Header: TPanel
    Left = 0
    Top = 0
    Width = 681
    Height = 113
    Align = alTop
    Color = clAppWorkSpace
    TabOrder = 0
    Visible = False
  end
  object pn_Buttons: TPanel
    Left = 0
    Top = 394
    Width = 681
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 681
      Height = 2
      Align = alTop
    end
    object Panel1: TPanel
      Left = 503
      Top = 2
      Width = 178
      Height = 33
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btn_Ok: TButton
        Left = 7
        Top = 5
        Width = 75
        Height = 23
        Caption = 'Ok'
        Default = True
        ModalResult = 1
        TabOrder = 0
      end
      object btn_Cancel: TButton
        Left = 95
        Top = 5
        Width = 75
        Height = 23
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        ModalResult = 2
        TabOrder = 1
      end
    end
  end
  object cxDBTreeList1: TcxDBTreeList
    Left = 0
    Top = 113
    Width = 681
    Height = 120
    Align = alTop
    Bands = <
      item
      end>
    DataController.DataSource = DataSource1
    DataController.ParentField = 'parent_id'
    DataController.KeyField = 'object_field_id'
    DefaultRowHeight = 17
    LookAndFeel.Kind = lfFlat
    OptionsCustomizing.BandMoving = False
    OptionsCustomizing.BandVertSizing = False
    OptionsCustomizing.ColumnVertSizing = False
    OptionsData.Deleting = False
    OptionsView.ShowEditButtons = ecsbFocused
    OptionsView.Indicator = True
    Preview.LeftIndent = 1
    Preview.MaxLineCount = 1
    RootValue = -1
    TabOrder = 2
    object col_caption: TcxDBTreeListColumn
      Caption.Text = #1053#1072#1079#1074#1072#1085#1080#1077
      DataBinding.FieldName = 'field_caption'
      Options.Editing = False
      Width = 205
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      SortOrder = soAscending
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_checked: TcxDBTreeListColumn
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.NullStyle = nssUnchecked
      Caption.Text = #1054#1090#1084#1077#1090#1080#1090#1100
      DataBinding.FieldName = 'checked'
      Options.Sorting = False
      Width = 63
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_display_value: TcxDBTreeListColumn
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end
        item
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF0000008400FFFFFF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF000000840000008400FFFFFF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF000000840000008400FFFFFF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF0000008400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF000000840000008400FFFFFF00FF00FF00FF00FF00FF00
            FF000000840000008400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00000084000000840000008400FFFFFF00FF00FF000000
            840000008400FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00000084000000840000008400000084000000
            8400FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00000084000000840000008400FFFF
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00000084000000840000008400000084000000
            8400FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00000084000000840000008400FFFFFF00FF00FF000000
            8400FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00000084000000840000008400FFFFFF00FF00FF00FF00FF00FF00
            FF000000840000008400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00000084000000840000008400FFFFFF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF0000008400FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF000000840000008400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF0000008400FFFFFF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
          Kind = bkGlyph
        end>
      Properties.ReadOnly = False
      Properties.OnButtonClick = cxDBTreeList2cxDBTreeListColumn6PropertiesButtonClick
      Caption.Text = #1047#1085#1072#1095#1077#1085#1080#1077
      DataBinding.FieldName = 'value'
      Options.Sorting = False
      Width = 263
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_ID_VALUE: TcxDBTreeListColumn
      Visible = False
      Caption.Text = 'col_ID_VALUE'
      DataBinding.FieldName = 'ID_VALUE'
      Width = 117
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Object_field_id: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'Object_field_id'
      Options.Hidden = True
      Width = 115
      Position.ColIndex = 4
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object FormStorage: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 456
    Top = 8
  end
  object ActionList: TActionList
    Left = 384
    Top = 8
    object act_CheckedAll: TAction
      Caption = #1091' '#1074#1089#1077#1093' '#1087#1072#1088#1072#1084#1077#1090#1088#1086#1074
    end
    object act_UnCheckedAll: TAction
      Caption = #1057#1085#1103#1090#1100' '#1074#1089#1077' '#1095#1077#1082#1077#1090#1099
    end
    object act_CheckedEdited: TAction
      Caption = #1091' '#1087#1072#1088#1072#1084#1077#1090#1088#1086#1074' '#1089' '#1079#1072#1087#1086#1083#1085#1077#1085#1085#1099#1084#1080' '#1079#1085#1072#1095#1077#1085#1080#1103#1084#1080
    end
  end
  object PopupMenu: TPopupMenu
    Left = 328
    Top = 8
    object N2: TMenuItem
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1095#1077#1082#1077#1090#1099
      object N4: TMenuItem
        Action = act_CheckedEdited
      end
      object N3: TMenuItem
        Action = act_CheckedAll
      end
    end
    object actUnCheckedAll1: TMenuItem
      Action = act_UnCheckedAll
    end
  end
  object ADOStoredProc1_Select: TADOStoredProc
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega_1'
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    AfterPost = ADOStoredProc1_SelectAfterPost
    ProcedureName = 'sp_GroupAssign_select;1'
    Parameters = <>
    Left = 56
    Top = 8
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc1_Select
    Left = 56
    Top = 56
  end
  object ADOStoredProc1_update: TADOStoredProc
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega_1'
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    AfterPost = ADOStoredProc1_SelectAfterPost
    ProcedureName = 'sp_GroupAssign;1'
    Parameters = <>
    Left = 216
    Top = 8
  end
  object ADOStoredProc1: TADOStoredProc
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega_1'
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    AfterPost = ADOStoredProc1_SelectAfterPost
    ProcedureName = 'sp_GroupAssign;1'
    Parameters = <>
    Left = 96
    Top = 280
  end
end
