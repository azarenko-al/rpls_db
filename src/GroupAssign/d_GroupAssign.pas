unit d_GroupAssign;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   rxPlacemnt, cxStyles, cxInplaceContainer, cxTL, cxDBTL, cxControls, Variants,

  u_Storage,


//  dm_Main,

  dm_Onega_DB_data,

  dm_MapEngine_store,

 // d_expl_Object_Select,
  d_SelectItemByList,

  u_types,
  u_const_db,

  u_const_msg,

  u_dlg,

  u_classes,
  u_db,
  u_img,
  u_func,

  cxTLData, Grids, DBGrids, DB, ADODB, Menus, ActnList, StdCtrls, ExtCtrls;


//type
//  TAssignParamType = (aptDelimeter, aptOther, aptText, aptInt,
//                      aptFloat, aptBool,
//                      aptDate, aptXref, aptList, aptHeader);


type
  Tdlg_GroupAssign = class(TForm)
    pn_Header: TPanel;
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    FormStorage: TFormStorage;
    ActionList: TActionList;
    act_CheckedAll: TAction;
    act_UnCheckedAll: TAction;
    PopupMenu: TPopupMenu;
    actUnCheckedAll1: TMenuItem;
    act_CheckedEdited: TAction;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    ADOStoredProc1_Select: TADOStoredProc;
    DataSource1: TDataSource;
    cxDBTreeList1: TcxDBTreeList;
    col_caption: TcxDBTreeListColumn;
    col_checked: TcxDBTreeListColumn;
    col_display_value: TcxDBTreeListColumn;
    ADOStoredProc1_update: TADOStoredProc;
    col_ID_VALUE: TcxDBTreeListColumn;
    col_Object_field_id: TcxDBTreeListColumn;
    Panel1: TPanel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    ADOStoredProc1: TADOStoredProc;
    procedure FormDestroy(Sender: TObject);
    procedure ADOStoredProc1_SelectAfterPost(DataSet: TDataSet);
//    procedure Button3Click(Sender: TObject);
//    procedure btn_UpdateClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxDBTreeList2cxDBTreeListColumn6PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBTreeList1Editing(Sender: TObject; AColumn: TcxTreeListColumn; var
        Allow: Boolean);
  private
    FDataset : TDataSet;
    FIDList_ref  : TIDList;
    FObjName : string;

    procedure ClearRecord;
    procedure OkExec;

    procedure SelectItems;
    procedure UpdateRec;
  public
    class function ExecDlg(aObjName: string; aIDList: TIDList): boolean;
  end;

//==============================================================================
implementation  {$R *.DFM}
//==============================================================================
const

  FLD_ID_VALUE = 'ID_VALUE';
  FLD_field_type = 'field_type';
  FLD_VALUE = 'VALUE';
  FLD_WHERE = 'WHERE';




//------------------------------------------------------------------------------
class function Tdlg_GroupAssign.ExecDlg(aObjName: string; aIDList: TIDList): boolean;
//------------------------------------------------------------------------------
//var
 // iRes: Integer;
 // iResult: Integer;
 // k: Integer;
 // sWhere: string;
var
  iRes: Integer;
begin
  if aIDList.Count=0 then
  begin
    ShowMessage('�� ������� ������.');
    Result := False;
    Exit;
  end;



  // -------------------------
  with Tdlg_GroupAssign.Create(Application) do
  // -------------------------
  try
    FIDList_ref:=aIDList;
    FObjName:=aObjName;

    iRes:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc1_Select,
        SP_GroupAssign_select,
        [
         FLD_OBJNAME,  aObjName
        ]);

    ADOStoredProc1_Select.Locate (FLD_CHECKED, True, []);

 //  db_View( ADOStoredProc1_Select);


    cxDBTreeList1.FullExpand;


    Result:=(ShowModal=mrOk);

    if Result then
      OkExec();

  finally
    Free;
  end;
end;


//------------------------------------------------------------------------------
procedure Tdlg_GroupAssign.FormCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  Caption:= '��������� ������������';

  FDataset:=ADOStoredProc1_Select;

  cxDBTreeList1.Align:=alClient;
  cxDBTreeList1.OptionsView.ShowEditButtons :=ecsbFocused;


  g_Storage.RestoreFromRegistry(cxDBTreeList1, className);


  if ADOStoredProc1_Select.Active then
    ShowMessage('ADOStoredProc1_Select.Active');

end;


procedure Tdlg_GroupAssign.FormDestroy(Sender: TObject);
begin
  g_Storage.StoreToRegistry(cxDBTreeList1, className);

end;


// ---------------------------------------------------------------
procedure Tdlg_GroupAssign.SelectItems;
// ---------------------------------------------------------------
var
  rec: Tdlg_SelectItemByList_rec;
begin
//  rec.FieldType      := FDataset.FieldBYName(FLD_field_type).AsString;
  rec.Object_field_id:= FDataset.FieldBYName(FLD_Object_field_id).AsInteger;
//  rec.XREF_TableName := FDataset.FieldBYName(FLD_XREF_TableName).AsString;
//  rec.StatusName     := FDataset.FieldBYName(FLD_Status_Name).AsString;

  rec.Result.ID  := FDataset.FieldBYName(FLD_ID_VALUE).AsInteger;

  if Tdlg_SelectItemByList.ExecDlg(rec) then
    db_UpdateRecord(FDataSet,
      [ db_Par(FLD_ID_VALUE,    rec.Result.ID),
        db_Par(FLD_VALUE,       rec.Result.Value),
        db_Par(FLD_CHECKED,     True)
      ]);
end;

// ---------------------------------------------------------------
procedure Tdlg_GroupAssign.UpdateRec;
// ---------------------------------------------------------------

var
  i: Integer;
begin
  i:=dmOnega_DB_data.ExecStoredProc_( //ADOStoredProc1_update,
      SP_GroupAssign_update_record,
    [
     FLD_object_field_id,  FDataset[FLD_object_field_id],
     FLD_CHECKED,          FDataset[FLD_Checked],
     FLD_VALUE,            FDataset[FLD_VALUE],
     FLD_ID_VALUE,         FDataset[FLD_ID_VALUE]
    ]);

end;

// ---------------------------------------------------------------
procedure Tdlg_GroupAssign.ClearRecord;
// ---------------------------------------------------------------
begin
  db_UpdateRecord(FDataSet,
    [ db_Par(FLD_ID_VALUE, NULL),
      db_Par(FLD_VALUE,    NULL)
    ]);
end;


procedure Tdlg_GroupAssign.ADOStoredProc1_SelectAfterPost(DataSet: TDataSet);
begin
  UpdateRec();
end;

procedure Tdlg_GroupAssign.cxDBTreeList2cxDBTreeListColumn6PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  case AButtonIndex of
     0: SelectItems;
     1: ClearRecord;
  end;

end;


procedure Tdlg_GroupAssign.cxDBTreeList1Editing(Sender: TObject; AColumn:
    TcxTreeListColumn; var Allow: Boolean);
var
  bIsFolder: Boolean;
  sFieldType: string;
begin
  bIsFolder  := FDataset.FieldBYName(FLD_IS_FOLDER).AsBoolean;
  sFieldType := FDataset.FieldBYName(FLD_field_type).AsString;

  if bIsFolder then
    cxDBTreeList1.OptionsView.ShowEditButtons :=ecsbNever;

  Allow:=(not bIsFolder) ;//and
//         (not Eq(sFieldType,'xref'  )) and
 //        (not Eq(sFieldType,'status'));

end;

// ---------------------------------------------------------------
procedure Tdlg_GroupAssign.OkExec;
// ---------------------------------------------------------------
var
  iResult: Integer;
  k: Integer;
  sWhere: string;
begin

  db_PostDataset(ADOStoredProc1_Select);


//.. if Assigned(aIDList) then
//  sWhere:=FIDList_ref.ValuesToString(',');
  sWhere:=FIDList_ref.MakeWhereString;

  k:=Length(sWhere);

// �� else
 //�   sWhere:='';

//  iResult:=dmOnega_DB_data.ExecStoredProc(sp_GroupAssign_Exec_sql,
  iResult:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc1,
                 sp_GroupAssign_Exec_sql,

                [
                 FLD_OBJNAME, FObjName,
               //  db_Par(FLD_WHERE,   sWhere),
                 FLD_ID_STR,  sWhere
                ]);

  //db_View(ADOStoredProc1);

  if iResult>0 then
  begin
    ShowMessage( Format('��������� ��� %d �������', [iResult]));

    dmMapEngine_store.Update_Data(FObjName);

//    PostMessage (Application.Handle, WM_DATA_CHANGED ,0,0);
  end;

//  else
 //   ErrorDlg('������ ���������� ��������� ������.');
   // ShowMessage( '������!.');

end;

end.

