unit d_Geo_Pos;

interface

uses
  Classes, Controls, Forms, 
  StdCtrls, 

  u_func,


  d_Wizard,

//  I_Act_Profile,

  fr_geo_Coordinates,

//  dm_Options,
  
  


  u_geo, cxLookAndFeels, cxPropertiesStore, rxPlacemnt, ActnList, ExtCtrls;

type
    Tdlg_Geo_Pos = class(Tdlg_Wizard)
    GroupBox1: TGroupBox;
    ComboBox1: TComboBox;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
//    procedure act_OkExecute(Sender: TObject);
//    procedure ActionList1Update(Action: TBasicAction;
 //     var Handled: Boolean);
  private
//    FAngle1_2, FAngle2_2, FAngle1, FAngle2: TAngle;

    FBLPoint: TBLPoint;

    Fframe_geo_Coordinates1: Tframe_geo_Coordinates;

  public
    class function ExecDlg(var aBLPoint: TBLPoint): boolean;

  end;



//==============================================================
//  implementation
//==============================================================
implementation

 {$R *.DFM}



//--------------------------------------------------------------------
class function Tdlg_Geo_Pos.ExecDlg(var aBLPoint: TBLPoint):
    boolean;
//--------------------------------------------------------------------
begin
  with Tdlg_Geo_Pos.Create(Application) do
  begin
    FBLPoint:=aBLPoint;

    Fframe_geo_Coordinates1.Set_BLPoint_and_CoordSys(aBLPoint, EK_CK_42);

//    Fframe_geo_Coordinates1.BLPoint_Pulkovo:=aBLPoint;

//    Fframe_geo_Coordinates1.DisplayCoordSys:=IOptions.Get_CoordSys;


//    ed_Coords1.Text:=geo_FormatBLPoint(FBLVector.Point1);
  //  ed_Coords2.Text:=geo_FormatBLPoint(FBLVector.Point2);

   // if aProfileType = 'BETWEEN_BS_AND_POINT' then
  //    ed_Coords1.OnButtonClick:=nil;

    result := ShowModal=mrOk;

    if result then
    begin
      aBLPoint:=Fframe_geo_Coordinates1.GetBLPoint_Pulkovo;
    end;
  end;
end;


procedure Tdlg_Geo_Pos.FormCreate(Sender: TObject);
begin
  inherited;

  CreateChildForm(Tframe_geo_Coordinates, Fframe_geo_Coordinates1, GroupBox1);

  SetDefaultWidth;

//  Fframe_geo_Coordinates1:=Tframe_geo_Coordinates.CreateChildForm (GroupBox1);
//  Fframe_geo_Coordinates2:=Tframe_geo_Coordinates.CreateChildForm (GroupBox2);
end;


procedure Tdlg_Geo_Pos.FormDestroy(Sender: TObject);
begin
//  Fframe_geo_Coordinates1.Free;
//  Fframe_geo_Coordinates2.Free;

  inherited;
end;


end.
