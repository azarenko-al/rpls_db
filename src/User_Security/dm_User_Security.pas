unit dm_User_Security;

interface

uses
  Classes, ADODB,  Forms, Dialogs,

  dm_Main,

  dm_Onega_DB_data,

  u_log,

  u_const_db,


  u_db, DB
  ;


type
  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );

  TdmUser_Security = class(TDataModule)
    ADOStoredProc1: TADOStoredProc;
    procedure DataModuleCreate(Sender: TObject);
  private
    Is_Lib_Edit_allow: boolean;


    function GeoRegion_is_ReadOnly(aGeoRegion_ID: Integer): Boolean;

    function Project_is_ReadOnly11111: Boolean;

  public
  //  ProjectIsReadOnly: Boolean;

    UserName: string;


    Modify_lib     : TModify_lib ;//(mtAllow, mtAllowMy, mtForbidden );
    Modify_project : TModify_lib ;//(mtAllow, mtAllowMy, mtForbidden );

    //    TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );


//    ProjectIsReadOnly : Boolean;
 //   Project_EnableEditing : Boolean;


    class procedure Init;
    procedure Open;

    function Lib_Edit_ReadOnly: Boolean;
    function Object_Edit_ReadOnly: Boolean;

  end;

var
  dmUser_Security: TdmUser_Security;

implementation

uses Variants;


{$R *.dfm}



class procedure TdmUser_Security.Init;
begin
  if not Assigned(dmUser_Security) then
    dmUser_Security := TdmUser_Security.Create(Application);
end;

// ---------------------------------------------------------------
procedure TdmUser_Security.Open;
// ---------------------------------------------------------------
//const
//  DEF_SP_User_Security = 'sp_User_Security';
var
  k: Integer;
begin
  Assert (Assigned (dmUser_Security));


//  Exit;

  g_Log.SysMsg('procedure TdmUser_Security.Open;');


   k:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc1,  'Security.sp_User_access_SEL', []);

   UserName:=ADOStoredProc1.FieldByName('User_Name').AsString;

//   sp_Projects_enabled['is_modify_Project'];
   Modify_lib := TModify_lib ( ADOStoredProc1.FieldByName('is_modify_lib').AsInteger );

   Modify_project := TModify_lib ( ADOStoredProc1.FieldByName('is_modify_project').AsInteger );


   // (mtAllow, mtAllowMy, mtForbidden );


(*
  i:=dmOnega_DB_data.OpenStoredProc(sp_Projects,  DEF_SP_User_Security,
        [
         db_Par(FLD_ACTION, 'select_projects_enabled')
        //,
        //  db_Par(FLD_User_ID, aUser_id)
        // db_Par(FLD_ID, aID),
         ]);
*)

{
  i:=dmOnega_DB_data.OpenStoredProc(sp_GeoRegions,  DEF_SP_User_Security,
        [
         FLD_ACTION, 'select_GeoRegions_enabled'
         //,
         // db_Par(FLD_User_ID, aUser_id)
         // db_Par(FLD_ID, aID),
         ]);
 }


end;

// ---------------------------------------------------------------
function TdmUser_Security.Project_is_ReadOnly11111: Boolean;
// ---------------------------------------------------------------
begin
  Result := True;

//  Result := sp_Projects_enabled.Look;

end;

// ---------------------------------------------------------------
function TdmUser_Security.GeoRegion_is_ReadOnly(aGeoRegion_ID: Integer):
    Boolean;
// ---------------------------------------------------------------
var
  v: Variant;
begin
  Assert (Assigned (dmUser_Security));


{

Result := False;
Exit;



  if ProjectIsReadOnly then
  begin
    Result := True;
    Exit;
  end;
 }


(*
Result := False;
  Exit;

*)

(*
  Result := aGeoRegion_ID=95;

  Exit;
*)

  if aGeoRegion_ID<=0 then
  begin
    Result := False;
    Exit;
  end;

(*

  Result := True;

  if ProjectIsReadOnly or (aGeoRegion_ID=0) then
    Exit;
*)

//  Assert(sp_GeoRegions.Active);

//  v:=sp_GeoRegions.Lookup(FLD_GeoRegion_ID, aGeoRegion_ID, FLD_GeoRegion_ID);

//  Result := VarIsNull(v);

end;


// ---------------------------------------------------------------
procedure TdmUser_Security.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  db_SetComponentADOConnection(Self, dmMain.ADOConnection1);

  Is_Lib_Edit_allow:=False;


//  ProjectIsReadOnly := True;

//  ProjectIsReadOnly := False;

end;


// ---------------------------------------------------------------
function TdmUser_Security.Lib_Edit_ReadOnly: Boolean;
// ---------------------------------------------------------------
begin
//  Assert (aTableName<>'');

  Assert (Assigned (dmUser_Security));


  Result := not ( Modify_lib in [mtAllow, mtAllowMy] );

//  Result := False;
end;

// ---------------------------------------------------------------
function TdmUser_Security.Object_Edit_ReadOnly: Boolean;
// ---------------------------------------------------------------
begin
//  Assert (aTableName<>'');
  Assert (Assigned (dmUser_Security));

  Result := not ( Modify_project in [mtAllow, mtAllowMy] );


//  Result := False;
end;



end.