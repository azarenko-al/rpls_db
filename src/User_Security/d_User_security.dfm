object dlg_User_security: Tdlg_User_security
  Left = 1863
  Top = 354
  Width = 552
  Height = 561
  BorderWidth = 5
  Caption = 'dlg_User_security'
  Color = clBtnFace
  DefaultMonitor = dmDesktop
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 534
    Height = 281
    ActivePage = TabSheet_GeoRegions
    Align = alTop
    TabOrder = 0
    object TabSheet_GeoRegions: TTabSheet
      Caption = 'Geo Regions'
      ImageIndex = 1
      object cxGrid3: TcxGrid
        Left = 0
        Top = 0
        Width = 526
        Height = 145
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView2: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_GeoRegions
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsSelection.CellSelect = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object col_ENABLED1: TcxGridDBColumn
            DataBinding.FieldName = 'ENABLED'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssUnchecked
            Visible = False
            Width = 86
          end
          object col_GeoRegion_ID: TcxGridDBColumn
            DataBinding.FieldName = 'GeoRegion_ID'
            Visible = False
            Width = 75
          end
          object col_Geo_Region_name: TcxGridDBColumn
            DataBinding.FieldName = 'GeoRegion_Name'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 339
          end
          object cxGridDBColumn3: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
            Width = 49
          end
          object cxGridDBColumn4: TcxGridDBColumn
            DataBinding.FieldName = 'User_ID'
            Visible = False
          end
          object cxGridDBColumn5: TcxGridDBColumn
            DataBinding.FieldName = 'Project_ID'
            Visible = False
          end
          object col_GeoRegion_ReadOnly: TcxGridDBColumn
            DataBinding.FieldName = 'ReadOnly'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssUnchecked
            Width = 98
          end
        end
        object cxGridLevel2: TcxGridLevel
          GridView = cxGridDBTableView2
        end
      end
    end
    object TabSheet_Projects: TTabSheet
      Caption = 'Projects'
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 526
        Height = 209
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView1: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_Projects
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsSelection.CellSelect = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object col_Enabled: TcxGridDBColumn
            DataBinding.FieldName = 'ENABLED'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssUnchecked
            Visible = False
            Width = 78
          end
          object col_Project_name: TcxGridDBColumn
            DataBinding.FieldName = 'Project_name'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 379
          end
          object cxGridDBTableView1id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
            Width = 49
          end
          object cxGridDBTableView1User_ID: TcxGridDBColumn
            DataBinding.FieldName = 'User_ID'
            Visible = False
          end
          object cxGridDBTableView1Project_ID: TcxGridDBColumn
            DataBinding.FieldName = 'Project_ID'
            Visible = False
          end
          object col_Project_ReadOnly: TcxGridDBColumn
            DataBinding.FieldName = 'ReadOnly'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssUnchecked
            Width = 116
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 492
    Width = 534
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Panel2: TPanel
      Left = 406
      Top = 0
      Width = 128
      Height = 31
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object Button2: TButton
        Left = 40
        Top = 5
        Width = 75
        Height = 25
        Cancel = True
        Caption = #1047#1072#1082#1088#1099#1090#1100
        ModalResult = 2
        TabOrder = 0
      end
    end
  end
  object sp_Projects: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'sp_User_Security'
    Parameters = <>
    Left = 144
    Top = 320
  end
  object sp_GeoRegions: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'sp_User_Security'
    Parameters = <>
    Left = 256
    Top = 320
  end
  object ds_Projects: TDataSource
    DataSet = sp_Projects
    Left = 144
    Top = 376
  end
  object ds_GeoRegions: TDataSource
    DataSet = sp_GeoRegions
    Left = 256
    Top = 376
  end
  object ADOConnection1: TADOConnection
    CommandTimeout = 60
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega_1'
    ConnectionTimeout = 30
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 40
    Top = 320
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredProps.Strings = (
      'PageControl1.ActivePage')
    StoredValues = <>
    Left = 408
    Top = 328
  end
end
