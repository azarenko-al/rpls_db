object dmUser_Security: TdmUser_Security
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 1183
  Top = 448
  Height = 281
  Width = 417
  object ADOStoredProc1: TADOStoredProc
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'sp_User_Projects;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 134
      end
      item
        Name = '@User_ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MODE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 184
    Top = 24
  end
end
