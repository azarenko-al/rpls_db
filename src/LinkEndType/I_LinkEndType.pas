unit I_LinkEndType;

interface

uses
  dm_LinkEndType;


type
  TLinkEndType_Get_rec =  record
    LinkEndTypeID: integer;

    ModeID: Integer;
    LinkEndType_Mode_ID : Integer;
  end;


  ILinkEndTypeX__ = interface(IInterface)
  ['{07DBA571-D8E3-48ED-8BF3-AC321FBC2823}']

    function Dlg_GetMode(aLinkEndTypeID: integer;
      //var aModeID, aLinkEndType_Mode_ID: Integer;
        aMode: Integer;
        var aRec: TdmLinkEndTypeModeInfoRec): boolean;


  //  function Dlg_GetMode (aLinkEndTypeID: integer): integer;

(*    function Dlg_GetBandID (aLinkEndTypeID: integer;
                            aTxFreq, aRxFreq: double;
                            var aChannel_Type: string;
                            var aBandID: Integer;
                            var aLinkEndType_Band_ID : Integer
                            ): boolean;

*)
 end;

(*
var
  ILinkEndType: ILinkEndTypeX__;
  *)

implementation

end.
