unit u_LinkEndType_str;

interface


const
  CAPTION_NAME   = '������������';

  CAPTION_High = '������������ ������������ High';
  CAPTION_Low  = '������������ ������������ Low';

  CAPTION_FREQ_MIN          = 'Min ������� ������� %s [MHz]';
  CAPTION_FREQ_MAX          = 'Max ������� ������� %s [MHz]';

  CAPTION_CHANNEL_WIDTH     = '������ ������� ������ [MHz]'; //'������ ���������� ������ [MHz]';

  CAPTION_DUPLEX_FREQ_DELTA = '���������� ������ ������ ���/��� [MHz]';


//- ��� ����� ������ [MHz]  = ������ ������� ������ [MHz]
//- ������ ������ ���/��� [MHz] = ���������� ������ ������ ���/��� [MHz]


  CAPTION_CH_COUNT          = '���-�� ������';

  CAPTION_CHANNEL            = '�����';

  CAPTION_CH_LOW            = '����� Low';
  CAPTION_CH_HIGH           = '����� High';
  CAPTION_LOW_TX_FREQ       = '������� ��� [MHz]';
  CAPTION_LOW_RX_FREQ       = '������� ��� [MHz]';

  CAPTION_HIGH_TX_FREQ       = '������� ��� [MHz]';
  CAPTION_HIGH_RX_FREQ       = '������� ��� [MHz]';


  CAPTION_SPEED            = '�������� �������� [Mbit/s]';
  CAPTION_CH_SPACING       = '������ ������� [MHz]';
  CAPTION_MODULATION_TYPE  = '��� ���������';
  CAPTION_MODE             = '�����';
//  CAPTION_BANDWIDTH        = '������ ������ [MHz]';


  STR_RX_FREQ_LOW          = '������� ������ Low [MHz]';
  STR_TX_FREQ_LOW          = '������� �������� Low [MHz]';

  STR_RX_FREQ_HIGH         = '������� ������ High [MHz]';
  STR_TX_FREQ_HIGH         = '������� �������� High [MHz]';

  {
  STR_RX_FREQ_LOW          = 'Rx low [MHz]';
  STR_TX_FREQ_LOW          = 'Tx low [MHz]';

  STR_RX_FREQ_HIGH         = 'Rx high [MHz]';
  STR_TX_FREQ_HIGH         = 'Tx high [MHz]';
   }



implementation

end.
