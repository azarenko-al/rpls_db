inherited dmLinkEndType_Mode: TdmLinkEndType_Mode
  OldCreateOrder = True
  Left = 498
  Top = 497
  Height = 233
  Width = 464
  object qry_Modes: TADOQuery
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=SQL_ONEGA'
    CursorType = ctStatic
    BeforeEdit = qry_ModesBeforeEdit
    BeforePost = qry_ModesBeforePost
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM    LINKENDTYPE_MODE')
    Left = 44
    Top = 20
  end
  object ds_Mode: TDataSource
    DataSet = qry_Modes
    Left = 44
    Top = 68
  end
  object ds_tab_Lib_Modulations: TDataSource
    DataSet = tab_Lib_Modulations
    Left = 146
    Top = 68
  end
  object tab_Lib_Modulations: TADOTable
    CursorType = ctStatic
    TableName = 'Lib_Modulations'
    Left = 144
    Top = 20
    object Lib_ModulationsNAME: TStringField
      FieldName = 'NAME'
      Size = 50
    end
  end
  object tab_lib_Radiation_class: TADOTable
    TableName = 'lib_Radiation_class'
    Left = 284
    Top = 20
    object tab_lib_Radiation_classname2: TStringField
      FieldName = 'name'
      Size = 10
    end
  end
  object ds_tab_lib_Radiation_class: TDataSource
    DataSet = tab_lib_Radiation_class
    Left = 282
    Top = 68
  end
end
