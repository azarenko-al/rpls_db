object frame_LinkEndType_Band: Tframe_LinkEndType_Band
  Left = 683
  Top = 280
  Width = 1093
  Height = 605
  Caption = 'frame_LinkEndType_Band'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 1077
    Height = 313
    ActivePage = TabSheet_Bands
    Align = alTop
    Style = tsFlatButtons
    TabOrder = 0
    OnChange = PageControl1Change
    object TabSheet_Bands: TTabSheet
      Caption = 'Bands'
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 1069
        Height = 169
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGrid1DBTableView_bands: TcxGridDBTableView
          PopupMenu = PopupMenu_Bands
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_Bands
          DataController.Filter.MaxValueListCount = 1000
          DataController.KeyFieldNames = 'id'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnPopup.MaxDropDownItemCount = 12
          OptionsBehavior.ImmediateEditor = False
          OptionsCustomize.ColumnFiltering = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsView.Navigator = True
          OptionsView.GroupByBox = False
          OptionsView.GroupFooters = gfVisibleWhenExpanded
          OptionsView.HeaderAutoHeight = True
          OptionsView.Indicator = True
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          Styles.OnGetContentStyle = cxGrid1DBTableView_bandsStylesGetContentStyle
          object col_id: TcxGridDBColumn
            Tag = -1
            DataBinding.FieldName = 'id'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = False
            Visible = False
            Options.Filtering = False
            Width = 25
          end
          object col_name1: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Filtering = False
            Width = 76
          end
          object col_name_vendor: TcxGridDBColumn
            DataBinding.FieldName = 'name_vendor'
            Width = 103
          end
          object col__High: TcxGridDBColumn
            DataBinding.FieldName = 'High'
            Options.Filtering = False
            Width = 55
          end
          object col__Low: TcxGridDBColumn
            DataBinding.FieldName = 'Low'
            Options.Filtering = False
            Width = 66
          end
          object col__TxRx_Shift: TcxGridDBColumn
            DataBinding.FieldName = 'TxRx_Shift'
            Options.Filtering = False
            Styles.OnGetContentStyle = col__TxRx_ShiftStylesGetContentStyle
            Width = 69
          end
          object col__freq_min_low: TcxGridDBColumn
            DataBinding.FieldName = 'freq_min_low'
            Options.Filtering = False
            Width = 77
          end
          object col__freq_max_low: TcxGridDBColumn
            DataBinding.FieldName = 'freq_max_low'
            HeaderAlignmentHorz = taCenter
            Options.Filtering = False
            Width = 84
          end
          object col__freq_min_high: TcxGridDBColumn
            DataBinding.FieldName = 'freq_min_high'
            Options.Filtering = False
            Width = 72
          end
          object col__freq_max_high: TcxGridDBColumn
            DataBinding.FieldName = 'freq_max_high'
            Options.Filtering = False
            Width = 75
          end
          object col__Channel_width: TcxGridDBColumn
            Caption = 'ch width'
            DataBinding.FieldName = 'Channel_width'
            Options.Filtering = False
            Width = 69
          end
          object col_bandwidth: TcxGridDBColumn
            DataBinding.FieldName = 'bandwidth'
          end
          object col__Channel_count: TcxGridDBColumn
            Caption = 'N '#1082#1072#1085#1072#1083#1086#1074
            DataBinding.FieldName = 'Channel_count'
            Options.Filtering = False
            Width = 78
          end
          object cxGrid1DBTableView_bandsColumn1: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1087#1088#1086#1076#1091#1082#1090#1072' High'
            DataBinding.FieldName = 'item_code_high'
            Visible = False
            Options.Filtering = False
            Width = 85
          end
          object cxGrid1DBTableView_bandsColumn2: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1087#1088#1086#1076#1091#1082#1090#1072' Low'
            DataBinding.FieldName = 'item_code_low'
            Visible = False
            Options.Filtering = False
            Width = 88
          end
          object cxGrid1DBTableView_bandsColumn3: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1087#1088#1086#1076#1091#1082#1090#1072' High '#1082#1086#1088#1087#1086#1088#1072#1090#1080#1074#1085#1099#1081
            DataBinding.FieldName = 'item_code_high_internal'
            Visible = False
            Options.Filtering = False
            Width = 107
          end
          object cxGrid1DBTableView_bandsColumn4: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1087#1088#1086#1076#1091#1082#1090#1072' Low '#1082#1086#1088#1087#1086#1088#1072#1090#1080#1074#1085#1099#1081
            DataBinding.FieldName = 'item_code_low_internal'
            Visible = False
            Options.Filtering = False
            Width = 111
          end
          object col_Forbidden_freq_str: TcxGridDBColumn
            DataBinding.FieldName = 'Forbidden_freq_str'
          end
          object col_Allowed_freq_str: TcxGridDBColumn
            DataBinding.FieldName = 'Allowed_freq_str'
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView_bands
        end
      end
    end
    object TabSheet_Channels: TTabSheet
      Caption = 'Channels'
      ImageIndex = 1
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 1069
        Height = 266
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = False
        LookAndFeel.SkinName = ''
        object cxGrid2DBBandedTableView_channels: TcxGridDBBandedTableView
          PopupMenu = PopupMenu_channels
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_Channels
          DataController.Filter.MaxValueListCount = 1000
          DataController.KeyFieldNames = 'id'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnPopup.MaxDropDownItemCount = 12
          OptionsBehavior.ColumnHeaderHints = False
          OptionsCustomize.ColumnFiltering = False
          OptionsCustomize.ColumnHiding = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.GroupBySorting = True
          OptionsCustomize.GroupRowSizing = True
          OptionsCustomize.BandHiding = True
          OptionsCustomize.BandsQuickCustomization = True
          OptionsCustomize.NestedBands = False
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsSelection.MultiSelect = True
          OptionsView.CellAutoHeight = True
          OptionsView.HeaderAutoHeight = True
          OptionsView.Indicator = True
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          Styles.OnGetContentStyle = cxGrid1DBTableView_bandsStylesGetContentStyle
          Bands = <
            item
              Width = 293
            end
            item
              Caption = 'Low'
            end
            item
              Caption = 'High'
            end
            item
              Width = 83
            end>
          object col__band_name: TcxGridDBBandedColumn
            DataBinding.FieldName = 'HIGH'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = True
            Visible = False
            Options.Filtering = False
            Width = 107
            Position.BandIndex = 0
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object col__channel_low: TcxGridDBBandedColumn
            DataBinding.FieldName = 'LOW'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = True
            Visible = False
            Options.Editing = False
            Options.Filtering = False
            Width = 76
            Position.BandIndex = 0
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object col__TX_freq_low: TcxGridDBBandedColumn
            Caption = 'TX freq low'
            DataBinding.FieldName = 'TX_Freq_LOW'
            Options.Editing = False
            Options.Filtering = False
            Width = 79
            Position.BandIndex = 1
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object col__RX_freq_low: TcxGridDBBandedColumn
            Caption = 'RX freq low'
            DataBinding.FieldName = 'TX_Freq_HIGH'
            Options.Editing = False
            Options.Filtering = False
            Width = 68
            Position.BandIndex = 1
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object col__tx_freq_high: TcxGridDBBandedColumn
            Caption = 'tx freq high'
            DataBinding.FieldName = 'RX_Freq_LOW'
            Options.Editing = False
            Options.Filtering = False
            Width = 70
            Position.BandIndex = 2
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object col__rx_freq_high: TcxGridDBBandedColumn
            Caption = 'rx freq high'
            DataBinding.FieldName = 'RX_Freq_HIGH'
            Options.Editing = False
            Options.Filtering = False
            Width = 73
            Position.BandIndex = 2
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object col_Channel: TcxGridDBBandedColumn
            DataBinding.FieldName = 'Channel_number'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Width = 43
            Position.BandIndex = 0
            Position.ColIndex = 3
            Position.RowIndex = 0
          end
          object col_name: TcxGridDBBandedColumn
            DataBinding.FieldName = 'name'
            GroupIndex = 0
            Options.Editing = False
            Options.Filtering = False
            Width = 132
            Position.BandIndex = 0
            Position.ColIndex = 4
            Position.RowIndex = 0
          end
          object col_checked: TcxGridDBBandedColumn
            Caption = #1047#1072#1087#1088#1077#1090' MO'
            DataBinding.FieldName = 'checked'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.ImmediatePost = True
            Properties.NullStyle = nssUnchecked
            Width = 70
            Position.BandIndex = 3
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object col_id_: TcxGridDBBandedColumn
            DataBinding.FieldName = 'id'
            Visible = False
            Width = 34
            Position.BandIndex = 0
            Position.ColIndex = 2
            Position.RowIndex = 0
          end
          object col_Bandwidth_: TcxGridDBBandedColumn
            DataBinding.FieldName = 'Bandwidth'
            Options.Editing = False
            Options.Filtering = False
            Width = 101
            Position.BandIndex = 0
            Position.ColIndex = 5
            Position.RowIndex = 0
          end
          object col_LinkEndType_band_id: TcxGridDBBandedColumn
            DataBinding.FieldName = 'LinkEndType_band_id'
            Visible = False
            Position.BandIndex = 0
            Position.ColIndex = 6
            Position.RowIndex = 0
          end
        end
        object cxGrid2Level1: TcxGridLevel
          GridView = cxGrid2DBBandedTableView_channels
        end
      end
    end
  end
  object ds_Bands: TDataSource
    DataSet = qry_Bands
    Left = 32
    Top = 396
  end
  object qry_Bands: TADOQuery
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=SERVER1'
    CursorType = ctStatic
    BeforeEdit = qry_BandsBeforeEdit
    BeforePost = qry_BandsBeforePost
    OnNewRecord = qry_BandsNewRecord
    Parameters = <>
    SQL.Strings = (
      'select top 100 * from LinkendType_band')
    Left = 32
    Top = 348
  end
  object ds_Channels: TDataSource
    DataSet = sp_Band_Channels
    Left = 124
    Top = 396
  end
  object PopupMenu_Bands: TPopupMenu
    Left = 234
    Top = 459
    object actCopyrecord1: TMenuItem
      Action = act_Copy_record
      Visible = False
    end
    object actCopytoLinkendType1: TMenuItem
      Action = act_Copy_to_LinkendType
    end
    object N2: TMenuItem
      Action = act_duplicate
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object N4: TMenuItem
      Action = act_Load_auto
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object actRestoredefault1: TMenuItem
      Action = act_Restore_default
    end
    object actBanddel1: TMenuItem
      Action = act_Band_del
    end
  end
  object ActionList222: TActionList
    OnUpdate = ActionList222Update
    Left = 436
    Top = 361
    object act_Copy_record: TAction
      Caption = 'act_Copy_record'
      OnExecute = act_Copy_recordExecute
    end
    object act_Restore_default: TAction
      Caption = 'act_Restore_default'
      OnExecute = act_Copy_recordExecute
    end
    object act_Copy_to_LinkendType: TAction
      Caption = 'act_Copy_to_LinkendType'
      OnExecute = act_Copy_recordExecute
    end
    object act_duplicate: TAction
      Caption = #1044#1091#1073#1083#1080#1088#1086#1074#1072#1090#1100
      OnExecute = act_duplicateExecute
    end
    object act_channels_forbidden_check: TAction
      Category = 'channels'
      Caption = 'act_channels_forbidden_check'
      OnExecute = act_channels_forbidden_checkExecute
    end
    object act_channels_forbidden_uncheck: TAction
      Category = 'channels'
      Caption = 'act_channels_forbidden_uncheck'
      OnExecute = act_channels_forbidden_checkExecute
    end
    object act_Load_auto: TAction
      Caption = #1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1089#1077#1090#1082#1091' '#1095#1072#1089#1090#1086#1090' '#1072#1074#1090#1086#1084#1072#1090#1080#1095#1077#1089#1082#1080
      OnExecute = act_Load_autoExecute
    end
    object act_Band_del: TAction
      Category = 'band'
      Caption = 'act_Band_del'
      OnExecute = act_Band_delExecute
    end
  end
  object FormStorage1: TFormStorage
    Active = False
    IniFileName = 'Software\Onega\'
    Options = [fpPosition]
    StoredProps.Strings = (
      'PageControl1.ActivePage')
    StoredValues = <>
    Left = 260
    Top = 356
  end
  object sp_Band_Channels: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    AfterPost = sp_Band_ChannelsAfterPost
    ProcedureName = 'LinkEndType.sp_Band_Channel_SEL'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 10621
      end>
    Left = 124
    Top = 348
  end
  object PopupMenu_channels: TPopupMenu
    Left = 424
    Top = 432
    object MenuItem2: TMenuItem
      Action = act_Restore_default
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object actchannelsforbiddencheck1: TMenuItem
      Action = act_channels_forbidden_check
    end
    object actchannelsforbiddenuncheck1: TMenuItem
      Action = act_channels_forbidden_uncheck
    end
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=server1'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 624
    Top = 360
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 616
    Top = 440
    PixelsPerInch = 96
    object cxStyle_clRed: TcxStyle
      AssignedValues = [svColor]
      Color = clRed
    end
  end
  object cxStyleRepository2: TcxStyleRepository
    Left = 744
    Top = 440
    PixelsPerInch = 96
    object cxStyle_Edit: TcxStyle
    end
    object cxStyle_ReadOnly: TcxStyle
      AssignedValues = [svColor]
      Color = clInactiveBorder
    end
  end
  object cxStyleRepository_row: TcxStyleRepository
    Left = 896
    Top = 416
    PixelsPerInch = 96
    object cxStyle_row: TcxStyle
      AssignedValues = [svColor]
      Color = clInfoBk
    end
  end
end
