unit dm_LinkEndType_Mode;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms, Math, Variants,

  u_dlg,
  u_func,
  u_db,
  u_const_db,
  dm_Custom,

  u_LinkEndType_const,

  dm_LinkEndType

  ;

type
  TdmLinkEndType_Mode = class(TdmCustom)
    qry_Modes: TADOQuery;
    ds_Mode: TDataSource;
    ds_tab_Lib_Modulations: TDataSource;
    tab_Lib_Modulations: TADOTable;
    Lib_ModulationsNAME: TStringField;
    tab_lib_Radiation_class: TADOTable;
    tab_lib_Radiation_classname2: TStringField;
    ds_tab_lib_Radiation_class: TDataSource;


      procedure qry_ModesNewRecord (DataSet: TDataSet);
      procedure qry_ModesBeforePost(DataSet: TDataSet);
    procedure qry_ModesBeforeEdit(DataSet: TDataSet);

  private
    FID: integer;
    FBandwidth_tx_a,
    FBandwidth_tx_30,
    FBandwidth_tx_3: double;

    FBandwidth_rx_a,
    FBandwidth_rx_30,
    FBandwidth_rx_3: double;

    FPower_Noise : double;

  public
    class procedure Init;

    procedure Open1;
    procedure View(aID: integer; aMode: integer);
  end;

var
  dmLinkEndType_Mode: TdmLinkEndType_Mode;

implementation

{$R *.dfm}


//--------------------------------------------------------------------
class procedure TdmLinkEndType_Mode.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmLinkEndType_Mode) then
   Application.CreateForm(TdmLinkEndType_Mode, dmLinkEndType_Mode);

end;


procedure TdmLinkEndType_Mode.Open1;
begin
  tab_Lib_Modulations.Open1;
  tab_lib_Radiation_class.Open1;

end;


//--------------------------------------------------------------
procedure TdmLinkEndType_Mode.View(aID: integer; aMode: integer);
//--------------------------------------------------------------
begin
  FID:=aID;

  dmOnega_DB_data.OpenQuery (qry_Modes,
               'SELECT * FROM ' + TBL_LINKENDTYPE_MODE +
               ' WHERE LinkEndType_id=:LinkEndType_id',
               [FLD_LINKENDType_ID, aID]);

  qry_Modes.Locate(FLD_MODE,aMode,[]);
end;


//----------------------------------------------------------------------
procedure TdmLinkEndType_Mode.qry_ModesNewRecord (DataSet: TDataSet);
//----------------------------------------------------------------------
begin
//  DataSet[FLD_ID]:=-1;

  DataSet[FLD_LINKENDTYPE_ID]:=FID;
end;


//----------------------------------------------------------------------
procedure TdmLinkEndType_Mode.qry_ModesBeforePost(DataSet: TDataSet);
//----------------------------------------------------------------------
const
  MSG_ERROR_NEGATIVE_TX = '������ ������ ��������� �� ����� ����� ������������� ��������!';
  MSG_ERROR_NEGATIVE_RX = '������ ����������� ��� �� ����� ����� ������������� ��������!';

var
  dBandwidth,dCoeff_Noise: double;
begin  
  inherited;

  with DataSet do
  begin
    if (FieldByName(FLD_BANDWIDTH).AsFloat>0) and
      ((FieldByName(FLD_POWER_NOISE).AsFloat=0) or
       (FieldByName(FLD_POWER_NOISE).AsFloat<>FPower_Noise)) then
    begin
      dCoeff_Noise:= dmLinkEndType.GetDoubleFieldValue(FID, FLD_COEFF_NOISE);
      dBandwidth := FieldByName(FLD_BANDWIDTH).AsFloat;

      FieldValues[FLD_POWER_NOISE]:= TruncFloat(dCoeff_Noise +
                                     10*log10(1.38*Power(10, -14)*300*dBandwidth));
    end;

//?/  with DataSet do
    if (FieldByName(FLD_BANDWIDTH).AsFloat>0) and (FieldByName(FLD_CH_SPACING).AsFloat=0) then
      FieldValues[FLD_CH_SPACING]:= FieldValues[FLD_BANDWIDTH];

  //tx bandwidths -------------------------
//  with DataSet do
 // begin
    if (FieldValues[FLD_BANDWIDTH_TX_3]<0) and (FieldValues[FLD_BANDWIDTH_TX_3]<>null) then
    begin
      ErrDlg(MSG_ERROR_NEGATIVE_TX);
      FieldValues[FLD_BANDWIDTH_TX_3]:= FBandwidth_tx_3;
    end;
    if (FieldValues[FLD_BANDWIDTH_TX_30]<0) and (FieldValues[FLD_BANDWIDTH_TX_30]<>null) then
    begin
      ErrDlg(MSG_ERROR_NEGATIVE_TX);
      FieldValues[FLD_BANDWIDTH_TX_30]:= FBandwidth_tx_30;
    end;
    if (FieldValues[FLD_BANDWIDTH_TX_A]<0) and (FieldValues[FLD_BANDWIDTH_TX_A]<>null) then
    begin
      ErrDlg(MSG_ERROR_NEGATIVE_TX);
      FieldValues[FLD_BANDWIDTH_TX_A]:= FBandwidth_tx_a;
    end;

    if (FieldByName(FLD_BANDWIDTH_TX_A).AsFloat<>0) and
       (FieldByName(FLD_BANDWIDTH_TX_30).AsFloat<>0) and
       (FieldValues[FLD_BANDWIDTH_TX_A] < FieldValues[FLD_BANDWIDTH_TX_30]) then
    begin
      ErrDlg('������ ������ ��������� �� ������ ''A'' '+#13#10+
             '������ ���� ������, ��� �� ������ ''-30 [dB]'' !');
      FieldValues[FLD_BANDWIDTH_TX_30]:= FBandwidth_tx_30;
      FieldValues[FLD_BANDWIDTH_TX_A]:= FBandwidth_tx_a;
    end;

    if (FieldByName(FLD_BANDWIDTH_TX_30).AsFloat<>0) and
       (FieldByName(FLD_BANDWIDTH_TX_3).AsFloat<>0) and
       (FieldValues[FLD_BANDWIDTH_TX_30] < FieldValues[FLD_BANDWIDTH_TX_3]) then
    begin
      ErrDlg('������ ������ ��������� �� ������ ''-30 [dB]'' '+#13#10+
             '������ ���� ������, ��� �� ������ ''-3 [dB]'' !');
      FieldValues[FLD_BANDWIDTH_TX_30]:= FBandwidth_tx_30;
      FieldValues[FLD_BANDWIDTH_TX_3]:= FBandwidth_tx_3;
    end;

    if (FieldByName(FLD_BANDWIDTH_TX_30).AsFloat=0) then
    begin
      FieldValues[FLD_BANDWIDTH_TX_30]:= null;
      FieldValues[FLD_BANDWIDTH_TX_A]:= null;
    end;

    if (FieldByName(FLD_BANDWIDTH_TX_3).AsFloat=0) then
    begin
      FieldValues[FLD_BANDWIDTH_TX_3]:= null;
      FieldValues[FLD_BANDWIDTH_TX_30]:= null;
      FieldValues[FLD_BANDWIDTH_TX_A]:= null;
    end;
  end;

  //rx bandwidths -------------------------
  with DataSet do
  begin
    if (FieldValues[FLD_BANDWIDTH_RX_3]<0) and (FieldValues[FLD_BANDWIDTH_RX_3]<>null) then
    begin
      ErrDlg(MSG_ERROR_NEGATIVE_RX);
      FieldValues[FLD_BANDWIDTH_RX_3]:= FBandwidth_RX_3;
    end;
    if (FieldValues[FLD_BANDWIDTH_RX_30]<0) and (FieldValues[FLD_BANDWIDTH_RX_30]<>null) then
    begin
      ErrDlg(MSG_ERROR_NEGATIVE_RX);
      FieldValues[FLD_BANDWIDTH_RX_30]:= FBandwidth_RX_30;
    end;
    if (FieldValues[FLD_BANDWIDTH_RX_A]<0) and (FieldValues[FLD_BANDWIDTH_RX_A]<>null) then
    begin
      ErrDlg(MSG_ERROR_NEGATIVE_RX);
      FieldValues[FLD_BANDWIDTH_RX_A]:= FBandwidth_RX_a;
    end;

    if (FieldByName(FLD_BANDWIDTH_RX_A).AsFloat<>0) and
       (FieldByName(FLD_BANDWIDTH_RX_30).AsFloat<>0) and
       (FieldValues[FLD_BANDWIDTH_RX_A] < FieldValues[FLD_BANDWIDTH_RX_30]) then
    begin
      ErrDlg('������ ����������� ��� �� ������ ''a [dB]'' '+#13#10+
             '������ ���� ������, ��� �� ������ ''-30 [dB]'' !');
      FieldValues[FLD_BANDWIDTH_RX_30]:= FBandwidth_RX_30;
      FieldValues[FLD_BANDWIDTH_RX_A]:= FBandwidth_RX_a;
    end;

    if (FieldByName(FLD_BANDWIDTH_RX_30).AsFloat<>0) and
       (FieldByName(FLD_BANDWIDTH_RX_3).AsFloat<>0) and
       (FieldValues[FLD_BANDWIDTH_RX_30] < FieldValues[FLD_BANDWIDTH_RX_3]) then
    begin
      ErrDlg('������ ����������� ��� �� ������ ''-30 [dB]'' '+#13#10+
             '������ ���� ������, ��� �� ������ ''-3 [dB]'' !');
      FieldValues[FLD_BANDWIDTH_RX_30]:= FBandwidth_RX_30;
      FieldValues[FLD_BANDWIDTH_RX_3]:= FBandwidth_RX_3;
    end;

    if (FieldByName(FLD_BANDWIDTH_RX_30).AsFloat=0) then
    begin
      FieldValues[FLD_BANDWIDTH_RX_30]:= null;
      FieldValues[FLD_BANDWIDTH_RX_A]:= null;
    end;
    if (FieldByName(FLD_BANDWIDTH_RX_3).AsFloat=0) then
    begin
      FieldValues[FLD_BANDWIDTH_RX_3]:= null;
      FieldValues[FLD_BANDWIDTH_RX_30]:= null;
      FieldValues[FLD_BANDWIDTH_RX_A]:= null;
    end;

  end;

end;

//----------------------------------------------------------------------
procedure TdmLinkEndType_Mode.qry_ModesBeforeEdit(DataSet: TDataSet);
//----------------------------------------------------------------------
begin
  inherited;

  FBandwidth_tx_a := DataSet.FieldByName(FLD_BANDWIDTH_TX_A).AsFloat;
  FBandwidth_tx_30:= DataSet.FieldByName(FLD_BANDWIDTH_TX_30).AsFloat;
  FBandwidth_tx_3 := DataSet.FieldByName(FLD_BANDWIDTH_TX_3).AsFloat;

  FBandwidth_rx_a := DataSet.FieldByName(FLD_BANDWIDTH_RX_A).AsFloat;
  FBandwidth_RX_30:= DataSet.FieldByName(FLD_BANDWIDTH_RX_30).AsFloat;
  FBandwidth_RX_3 := DataSet.FieldByName(FLD_BANDWIDTH_RX_3).AsFloat;

  FPower_Noise    := DataSet.FieldByName(FLD_POWER_NOISE).AsFloat;
end;



end.
