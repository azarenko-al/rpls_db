unit dm_LinkEndType;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Variants,
  Db, ADODB, Math,

  dm_Onega_DB_data,


  dm_Main,
  dm_Object_base,

 // u_const_str,
  u_const_db,

  u_link_const,
  u_types,

  u_geo,
  u_db,
  u_radio,
  u_func

  ;


type
  //-------------------------------------------------------------------
  TdmLinkEndTypeAddRec = record
  //-------------------------------------------------------------------
    Name            : string;
    FolderID        : integer;

    Band            : string; //old - range

    Power_max_dBm    : double; // ����������� ��������
    Power_loss_dBm   : double; // ���������� ��������

    FreqMin_MHz         : double;
    FreqMax_MHz         : double;

//    Equaliser_Profit: double; // �������� �����������

    KNG             : double;

    failure_period  : double; // ����� ��������� �� �����

    Vendor_id1           : Integer;
    Vendor_Equipment_id1 : Integer;

  end;

  //-------------------------------------------------------------------
  TdmLinkEndTypeModeInfoRec = record
  //-------------------------------------------------------------------
    ID : Integer;

    Mode : Integer;

    Bitrate_Mbps_ : double; //was Integer

    Modulation_type :  string;

    Signature_height:  double;
    Signature_width :  double;
    Threshold_ber_3 :  double;
    Threshold_ber_6 :  double;
    Power_max_dBm   :  double;

    bandwidth_MHz   :  Double;

    Modulation_level_count : Integer;
    GOST_Modulation : string;
  end;

  //-------------------------------------------------------------------
  TdmLinkEndTypeInfoRec = record
  //-------------------------------------------------------------------
    BAND            : string; //old - range

    Power_Max        : double;
    KNG              : double;
    Failure_period   : Double;

    Threshold_ber_3 :  double;
    Threshold_ber_6 :  double;

    freq_channel_count: integer;
    KRATNOST_BY_FREQ : integer;

    KRATNOST_BY_SPACE: integer;

    Freq_Min : double;
    Freq_Max : double;

    Freq_Ave_MHz: Double;

  //  Equaliser_profit : double;

  end;

  //-------------------------------------------------------------------
  TdmLinkEndType = class(TdmObject_base)
    qry_LinkEndType_Band: TADOQuery;
    sp_Copy: TADOStoredProc;
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);

  private
  public
    function Add (aRec: TdmLinkEndTypeAddRec): integer;

//    function Copy_Band(aBand_ID, aLinkEndType_ID: integer): integer;
    function Copy_Mode(aMode_ID, aLinkEndType_ID: integer): integer;


    function GetInfoRec     (aID: integer; var aRec: TdmLinkEndTypeInfoRec): boolean;

    function GetBandID_by_Name (aBandName: string; aLinkEndType_ID: integer): integer;


  end;



function dmLinkEndType: TdmLinkEndType;


//================================================================
implementation {$R *.DFM}
//================================================================


var
  FdmLinkEndType: TdmLinkEndType;


// ---------------------------------------------------------------
function dmLinkEndType: TdmLinkEndType;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLinkEndType) then
    FdmLinkEndType := TdmLinkEndType.Create(Application);

  Result := FdmLinkEndType;
end;


//--------------------------------------------------------------------
procedure TdmLinkEndType.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  TableName:=TBL_LINKENDTYPE;
  ObjectName:=OBJ_LINKEND_TYPE;
end;


//--------------------------------------------------------------------
function TdmLinkEndType.Add(aRec: TdmLinkEndTypeAddRec): integer;
//--------------------------------------------------------------------
begin
 // Assert(aRec.Band<>'', 'aRec.Band<>''''');

  with aRec do
    Result:=gl_DB.AddRecordID (TableName,
             [db_Par(FLD_NAME,           Name),
              db_Par(FLD_FOLDER_ID,      IIF_NULL(FolderID)),

              db_Par(FLD_BAND,           IIF_NULL(Band)),

              db_Par(FLD_POWER_MAX,      IIF_NULL(Power_max_dBm)),
              db_Par(FLD_POWER_LOSS,     IIF_NULL(Power_loss_dBm)),

              db_Par(FLD_FAILURE_PERIOD, IIF_NULL(FAILURE_PERIOD)),

              db_Par(FLD_FREQ_MIN,       IIF_NULL(FreqMin_MHz)),
              db_Par(FLD_FREQ_MAX,       IIF_NULL(FreqMax_MHz)),

              db_Par(FLD_Vendor_ID,           IIF_NULL(aRec.Vendor_ID1)),
              db_Par(FLD_Vendor_Equipment_ID, IIF_NULL(aRec.Vendor_Equipment_ID1))

              ]);
end;

{
function TdmLinkEndType.Copy_Band(aBand_ID, aLinkEndType_ID: integer): integer;
begin
  Result:=dmOnega_DB_data.ExecStoredProc_('sp_LinkEndType_func',
     [
       FLD_ACTION, 'copy_band',
       FLD_ID, aBand_ID,
       FLD_LinkEndType_ID, aLinkEndType_id
     ]);

end;
}

function TdmLinkEndType.Copy_Mode(aMode_ID, aLinkEndType_ID: integer): integer;
begin
  Result:=dmOnega_DB_data.ExecStoredProc_('sp_LinkEndType_func',
     [
       FLD_ACTION, 'copy_mode',
       FLD_ID, aMode_ID,
       FLD_LinkEndType_ID, aLinkEndType_id
     ]);

end;




//--------------------------------------------------------------------
function TdmLinkEndType.GetBandID_by_Name(aBandName: string; aLinkEndType_ID: integer): integer;
//--------------------------------------------------------------------
const
  SQL_SELECT_LINKEND_TYPE_BAND =
     'SELECT id,low,high FROM ' + TBL_LINKENDTYPE_BAND +
     ' WHERE (LinkEndType_id=:LinkEndType_id) and'+
     '       ((low=:name) or (high=:name))';
begin
  if (aBandName='') and (aLinkEndType_ID=0) then
    begin Result:=0; Exit; end;

  dmOnega_DB_data.OpenQuery (qry_LinkEndType_Band, SQL_SELECT_LINKEND_TYPE_BAND,
               [FLD_LINKENDType_ID, aLinkEndType_ID,
                FLD_NAME,            aBandName ]);

  if not qry_LinkEndType_Band.IsEmpty then
    Result:= qry_LinkEndType_Band.FieldByName(FLD_ID).AsInteger
  else
    Result:=0;

end;

//-------------------------------------------------------------------
function TdmLinkEndType.GetInfoRec (aID: integer; var aRec: TdmLinkEndTypeInfoRec): boolean;
//-------------------------------------------------------------------
var
  i: integer;
begin
  FillChar (aRec, SizeOf(aRec), 0);

  db_OpenTableByID (qry_Temp, TBL_LinkEndType, aID);
  Result:=not qry_Temp.IsEmpty;

//  db_ViewDataSet(qry_Temp);
  if Result then

    with qry_Temp  do
    begin
  //     RANGE             := FieldByName(FLD_RANGE).AsInteger;
       aRec.Band              := FieldByName(FLD_band).AsString;
       aRec.Power_Max         := FieldByName(FLD_POWER_MAX).AsFloat;
       aRec.KNG               := FieldByName(FLD_KNG).AsFloat;

       aRec.failure_period    := FieldByName(FLD_FAILURE_PERIOD).AsFloat;

       aRec.freq_channel_count:= FieldByName(FLD_freq_channel_count).AsInteger;

       if aRec.freq_channel_count < 1 then
          aRec.freq_channel_count := 1;

  //     Assert (aRec.freq_channel_count >=1);

       aRec.Freq_Min := FieldByName(FLD_Freq_Min).AsFloat;
       aRec.Freq_Max := FieldByName(FLD_Freq_Max).AsFloat;

//       aRec.Freq_Ave_MHz:=(aRec.Freq_Max - aRec.Freq_Min) / 2;
       aRec.Freq_Ave_MHz:=(aRec.Freq_Max + aRec.Freq_Min) / 2;


       aRec.KRATNOST_BY_FREQ  := FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger;
       aRec.KRATNOST_BY_SPACE := FieldByName(FLD_KRATNOST_bY_SPACE).AsInteger;
   //    EQUALISER_PROFIT  := FieldByName(FLD_EQUALISER_PROFIT).AsFloat;

      // Threshold_ber_3:=FieldByName(FLD_Threshold_ber_3).AsFloat;
     //  Threshold_ber_6:=FieldByName(FLD_Threshold_ber_6).AsFloat;
             
    end;

end;



begin
end.


