unit fr_LinkEndType_Mode;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, ToolWin, ComCtrls,  StdCtrls,
  Math, rxPlacemnt,  Variants,   cxGridLevel, cxGrid, cxPropertiesStore, Provider,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxControls, cxGridCustomView, cxClasses,


  dm_Act_Explorer,

//  d_Audit_new,

  u_storage,

  u_db_ini,

  dm_Onega_DB_data,

//  f_Custom,

  u_types,
  u_const,
  u_const_db,
  u_const_str,

  u_Func_arrays,
  u_func,
  u_dlg,
  u_files,
  u_db,
//
  u_cx,

  u_Link_const,

//  dm_Main,
  u_func_msg,

  dm_Library,
  dm_LinkEndType,
 // dm_LinkEndType_Mode,

  u_LinkEndType_const,

  ADOInt, ExtCtrls, DB, ADODB, Menus, cxStyles, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxDBLookupComboBox;


type
  Tframe_LinkEndType_Mode = class(TForm, IMessageHandler)
    ds_Mode: TDataSource;
    qry_Modes: TADOQuery;

    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBBandedTableView1: TcxGridDBBandedTableView;

    col__Mode: TcxGridDBBandedColumn;
    col_Bitrate_Mbps: TcxGridDBBandedColumn;
    col__bandwidth: TcxGridDBBandedColumn;
    col__Modulation_type: TcxGridDBBandedColumn;
    col__traffic_type: TcxGridDBBandedColumn;
    col__radiation_class: TcxGridDBBandedColumn;
    col__Power_max: TcxGridDBBandedColumn;
    col__power_noise: TcxGridDBBandedColumn;
    col__bandwidth_tx_3: TcxGridDBBandedColumn;
    col__bandwidth_tx_30: TcxGridDBBandedColumn;
    col__bandwidth_tx_a: TcxGridDBBandedColumn;
    col__threshold_BER_3: TcxGridDBBandedColumn;
    col__threshold_BER_6: TcxGridDBBandedColumn;
    col__bandwidth_rx_3: TcxGridDBBandedColumn;
    col__bandwidth_rx_30: TcxGridDBBandedColumn;
    col__bandwidth_rx_a: TcxGridDBBandedColumn;
    col__ch_spacing: TcxGridDBBandedColumn;
    col__signature_width: TcxGridDBBandedColumn;
    col__signature_height: TcxGridDBBandedColumn;
    col_ID: TcxGridDBBandedColumn;
    ds_tab_Lib_Modulations: TDataSource;
    ds_tab_lib_Radiation_class: TDataSource;
    Panel1: TPanel;
    Button1: TButton;
    q_Lib_Modulations: TADOQuery;
    q_lib_Radiation_class: TADOQuery;
    PopupMenu1: TPopupMenu;
    ActionList1: TActionList;
    act_Grid_setup1: TAction;
    act_Copy_record: TAction;
    act_Restore_default: TAction;
    actRestoredefault1: TMenuItem;
    act_Audit1: TAction;
    ADOConnection1: TADOConnection;
    qry_Modesid: TAutoIncField;
    qry_ModesLinkEndType_id: TIntegerField;
    qry_Modesmode: TIntegerField;
    qry_Modesradiation_class: TStringField;
    qry_Modesbitrate_Mbps: TFloatField;
    qry_Modesmodulation_type: TStringField;
    qry_Modesthreshold_BER_3: TFloatField;
    qry_Modesthreshold_BER_6: TFloatField;
    qry_Modespower_max: TFloatField;
    qry_Modestraffic_type: TStringField;
    qry_Modessignature_width: TFloatField;
    qry_Modessignature_height: TFloatField;
    qry_Modesch_spacing: TFloatField;
    qry_Modesbandwidth: TFloatField;
    qry_Modesbandwidth_tx_3: TFloatField;
    qry_Modesbandwidth_tx_30: TFloatField;
    qry_Modesbandwidth_tx_a: TFloatField;
    qry_Modesbandwidth_rx_3: TFloatField;
    qry_Modesbandwidth_rx_30: TFloatField;
    qry_Modesbandwidth_rx_a: TFloatField;
    qry_Modespower_noise: TFloatField;
    act_Del: TAction;
    act_Copy_to_LinkendType: TAction;
    actCopytoLinkendType1: TMenuItem;
    N1: TMenuItem;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_Edit: TcxStyle;
    cxStyle_ReadOnly: TcxStyle;
    cxStyle_row_2: TcxStyle;
    cxGrid1DBBandedTableView1Column1: TcxGridDBBandedColumn;
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_Copy_recordExecute(Sender: TObject);
  //  procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
//    procedure qry_ModesAfterInsert(DataSet: TDataSet);

    procedure qry_ModesNewRecord(DataSet: TDataSet);

    procedure qry_ModesBeforePost(DataSet: TDataSet);
    procedure qry_ModesBeforeEdit(DataSet: TDataSet);
    procedure cxGrid1DBBandedTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
//    procedure ToolButton1Click(Sender: TObject);
//    procedure ToolButton2Click(Sender: TObject);
  private
    FReadOnly: boolean;

    FErrorList: TStringList;

    FID: integer;
    FBandwidth_tx_a,
    FBandwidth_tx_30,
    FBandwidth_tx_3: double;

    FBandwidth_rx_a,
    FBandwidth_rx_30,
    FBandwidth_rx_3: double;

    FPower_Noise : double;
    procedure Copy_record;
    procedure Dlg_Select_LinkendType;

    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList; var aHandled:
        boolean);

//    function GetMode(var aRec: TdmLinkEndTypeModeInfoRec): Integer;

  public
    procedure View(aID: integer; aMode: integer = 0);

    procedure SetReadOnly(aValue: boolean);

  end;


//=================================================================
implementation {$R *.DFM}
//=================================================================



//--------------------------------------------------------------
procedure Tframe_LinkEndType_Mode.FormCreate(Sender: TObject);
//--------------------------------------------------------------
begin
  inherited;


  Assert(not ADOConnection1.Connected);


//  col_Freq_spacing_MHz.Caption := '��������� ������, MHz';


  assert(not qry_Modes.Active);


//  FRegPath:=FRegPath+'3\';

//  Caption:='����� ������';

  cxGrid1.Align:= alClient;

//  act_Audit.Caption:=STR_Audit;
 // act_Audit.Enabled:=False;


  cxGrid1DBBandedTableView1.OptionsSelection.MultiSelect := True;


  cxGrid1DBBandedTableView1.Styles.BandBackground:=cxStyle_Edit;


//  ToolBar2.Visible:=True;

  // -------------------------------------------------------------------
 // Assert(FRegPath<>'');

//  cxGrid1DBBandedTableView1.RestoreFromRegistry(FRegPath + cxGrid1DBBandedTableView1.Name);

  g_Storage.StoreToRegistry (cxGrid1DBBandedTableView1, className+'_default');

//  g_Storage.RestoreFromRegistry(cxGrid1DBBandedTableView1, className);


  cxGrid1DBBandedTableView1.Bands[1].Caption:= CAPTION_BAND1;
  cxGrid1DBBandedTableView1.Bands[2].Caption:= CAPTION_BAND2;
  cxGrid1DBBandedTableView1.Bands[3].Caption:= CAPTION_BAND3;


  col_Bitrate_Mbps.Caption     := CAPTION_BITRATE_Mbps;
  col__Modulation_type.Caption := CAPTION_MODULATION_TYPE;
  col__threshold_BER_3.Caption := CAPTION_THRESHOLD_BER_3;
  col__threshold_BER_6.Caption := CAPTION_THRESHOLD_BER_6;
  col__Power_max.Caption       := CAPTION_POWER_MAX_dBm;
  col__bandwidth.Caption       := CAPTION_BANDWIDTH;
  col__ch_spacing.Caption      := CAPTION_CH_SPACING;
  col__power_noise.Caption     := CAPTION_POWER_NOISE;
  col__signature_width.Caption := CAPTION_SIGNATURE_WIDTH;
  col__signature_height.Caption:= CAPTION_SIGNATURE_HEIGHT;


  act_Copy_record.Caption  := STR_Copy_record;
  act_Restore_default.Caption:=STR_GRID_Restore_default;  // '������������ �������';


  act_Copy_to_LinkendType.Caption:='���������� � ������ ������������';
  

 // col_Name.Caption := CAPTION_NAME;


//  tab_li

 // db_SetComponentADOConnection(Self, dmMain.ADOConnection1);

 // db_Open

  dmOnega_DB_data.OpenQuery(q_Lib_Modulations,     'SELECT * FROM '+ TBL_Lib_Modulations);
  dmOnega_DB_data.OpenQuery(q_lib_Radiation_class, 'SELECT * FROM '+ TBL_lib_Radiation_class);

  Assert(q_Lib_Modulations.RecordCount>0);
  Assert(q_lib_Radiation_class.RecordCount>0);


{
  db_View(q_Lib_Modulations);
  db_View(q_lib_Radiation_class);
}

{
  db_O penQuery(q_Lib_Modulations, 'SELECT * FROM '+ TBL_Lib_Modulations);
  db_O penQuery(q_lib_Radiation_class, 'SELECT * FROM '+ TBL_lib_Radiation_class);
}

(*
  db_TableOpen1 (tab_Lib_Modulations, TBL_Lib_Modulations);
  db_TableOpen1 (tab_lib_Radiation_class, TBL_lib_Radiation_class);


  db_TableOpen1 (tab_Lib_Modulations, TBL_Lib_Modulations);
  db_TableOpen1 (tab_lib_Radiation_class, TBL_lib_Radiation_class);

  *)

 // tab_Lib_Modulations.Open;
//  tab_lib_Radiation_class.Open;


(*  TdmLinkEndType_Mode.Init;
  dmLinkEndType_Mode.Open;
*)


//  db_OpenQuery (qry_Modulation, 'SELECT * FROM ' + TBL_LIB_MODULATIONS);


  cx_SetColumnCaptions(cxGrid1DBBandedTableView1,
     [
      FLD_Mode,             '�����',

      FLD_BITRATE_MBPS,     CAPTION_BITRATE_Mbps,
      FLD_Modulation_type,  CAPTION_MODULATION_TYPE,
      FLD_threshold_BER_3,  CAPTION_THRESHOLD_BER_3,
      FLD_threshold_BER_6,  CAPTION_THRESHOLD_BER_6,

      FLD_traffic_type,     '��� �������',
      FLD_radiation_class,  '����� ���������',

      FLD_Power_max,        CAPTION_POWER_MAX_dBm,
      FLD_bandwidth,        CAPTION_BANDWIDTH,

      FLD_ch_spacing,       CAPTION_CH_SPACING,
      FLD_power_noise,      CAPTION_POWER_NOISE,

      FLD_signature_width,  CAPTION_SIGNATURE_WIDTH,
      FLD_signature_height, CAPTION_SIGNATURE_HEIGHT
    ]);



  FErrorList := TStringList.Create();

  g_Storage.StoreToRegistry(cxGrid1DBBandedTableView1, className+ '_default');


  g_Storage.RestoreFromRegistry(cxGrid1DBBandedTableView1, className);





  Assert (Assigned (OnDestroy));

  //  cxGrid1DBBandedTableView1.SaveToRegistry
//     (FRegPath  + cxGrid1DBBandedTableView1.Name + '_default');

end;

//--------------------------------------------------------------
procedure Tframe_LinkEndType_Mode.FormDestroy(Sender: TObject);
//--------------------------------------------------------------
begin
  FreeAndNil(FErrorList);

  g_Storage.StoreToRegistry(cxGrid1DBBandedTableView1, className);

 // cxGrid1DBBandedTableView1.StoreToRegistry(FRegPath + cxGrid1DBBandedTableView1.Name);

  inherited;
end;


//-------------------------------------------------------------------
procedure Tframe_LinkEndType_Mode.Dlg_Select_LinkendType;
//-------------------------------------------------------------------
var
  I: Integer;
  iLinkEndTypeID: Integer;
  sLinkEndTypeName: Widestring;

  oList: TList;
begin
  oList:=TList.Create;

  cx_GetSelectedList_(cxGrid1DBBandedTableView1, oList);

  iLinkEndTypeID:=0;


  if dmAct_Explorer.Dlg_Select_Object (otLinkEndType, iLinkEndTypeID, sLinkEndTypeName) then
  begin
    for I := 0 to oList.Count - 1 do    // Iterate
      dmLinkEndType.Copy_Mode( Integer (oList[i]), iLinkEndTypeID );




{    procedure cx_GetCheckedItemsToStrList(aGrid: TcxGridDBBandedTableView;
      aOutStrList: TStrings; aKeyFieldName: string = 'id'; aCheckFieldName:
      string = 'checked'); overload;
}

//    Assert(iLinkEndID>0, 'Value <=0');

{    dmOnega_DB_data.Object_update_LinkEndType
      (sObjName, iLinkEndID, iLinkEndTypeID, mode_rec.ID);

}
 end;

end;


//--------------------------------------------------------------
procedure Tframe_LinkEndType_Mode.View(aID: integer; aMode: integer = 0);
//--------------------------------------------------------------
begin
  FID:=aID;

  dmOnega_DB_data.OpenQuery(qry_Modes,

//  db_OpenQuery (qry_Modes,
               'SELECT * FROM ' + TBL_LINKENDTYPE_MODE +
//               'SELECT * FROM ' + view_LINKENDTYPE_MODE +
               ' WHERE LinkEndType_id=:LinkEndType_id',
               [FLD_LINKENDType_ID, aID]);

  // db_View(qry_Modes);

  qry_Modes.Locate(FLD_MODE,aMode,[]);





//  dmLinkEndType_Mode.View(aID,aMode);
end;


//----------------------------------------------------------------------
procedure Tframe_LinkEndType_Mode.qry_ModesNewRecord (DataSet: TDataSet);
//----------------------------------------------------------------------
begin
//  DataSet[FLD_ID]:=-1;

  DataSet[FLD_LINKENDTYPE_ID]:=FID;
end;


//----------------------------------------------------------------------
procedure Tframe_LinkEndType_Mode.qry_ModesBeforePost(DataSet: TDataSet);
//----------------------------------------------------------------------
const
  MSG_ERROR_NEGATIVE_TX = '������ ������ ��������� �� ����� ����� ������������� ��������!';
  MSG_ERROR_NEGATIVE_RX = '������ ����������� ��� �� ����� ����� ������������� ��������!';

var
  dBandwidth_MHz,dCoeff_Noise: double;
begin
  inherited;

  FErrorList.Clear;

  with DataSet do
  begin
    if (FieldByName(FLD_BANDWIDTH).AsFloat>0) and
      ((FieldByName(FLD_POWER_NOISE).AsFloat=0) or
       (FieldByName(FLD_POWER_NOISE).AsFloat<>FPower_Noise)) then
    begin
      dCoeff_Noise:= dmLinkEndType.GetDoubleFieldValue(FID, FLD_COEFF_NOISE);

      dBandwidth_MHz := FieldByName(FLD_BANDWIDTH).AsFloat;

      FieldValues[FLD_POWER_NOISE]:= TruncFloat(dCoeff_Noise +
                                     10*log10(1.38*Power(10, -14)*300*dBandwidth_MHz));
    end;

//?/  with DataSet do
    if (FieldByName(FLD_BANDWIDTH).AsFloat>0) and
       (FieldByName(FLD_CH_SPACING).AsFloat=0)
    then
      FieldValues[FLD_CH_SPACING]:= FieldValues[FLD_BANDWIDTH];

  //tx bandwidths -------------------------
//  with DataSet do
 // begin
    if (FieldValues[FLD_BANDWIDTH_TX_3]<0) and
       (FieldValues[FLD_BANDWIDTH_TX_3]<>null) then
    begin
      FErrorList.Add(MSG_ERROR_NEGATIVE_TX);

 //     ErrDlg(MSG_ERROR_NEGATIVE_TX);
      FieldValues[FLD_BANDWIDTH_TX_3]:= FBandwidth_tx_3;
    end;

    if (FieldValues[FLD_BANDWIDTH_TX_30]<0) and
       (FieldValues[FLD_BANDWIDTH_TX_30]<>null) then
    begin
      FErrorList.Add(MSG_ERROR_NEGATIVE_TX);
   //   ErrDlg(MSG_ERROR_NEGATIVE_TX);
      FieldValues[FLD_BANDWIDTH_TX_30]:= FBandwidth_tx_30;
    end;

    if (FieldValues[FLD_BANDWIDTH_TX_A]<0) and
       (FieldValues[FLD_BANDWIDTH_TX_A]<>null) then
    begin
      FErrorList.Add(MSG_ERROR_NEGATIVE_TX);
//      ErrDlg(MSG_ERROR_NEGATIVE_TX);
      FieldValues[FLD_BANDWIDTH_TX_A]:= FBandwidth_tx_a;
    end;

    if (FieldByName(FLD_BANDWIDTH_TX_A).AsFloat<>0) and
       (FieldByName(FLD_BANDWIDTH_TX_30).AsFloat<>0) and
       (FieldValues[FLD_BANDWIDTH_TX_A] < FieldValues[FLD_BANDWIDTH_TX_30]) then
    begin
      FErrorList.Add('������ ������ ��������� �� ������ ''A'' ������ ���� ������, ��� �� ������ ''-30 [dB]''.');

//      ErrDlg('������ ������ ��������� �� ������ ''A'' ������ ���� ������, ��� �� ������ ''-30 [dB]'' !');
      FieldValues[FLD_BANDWIDTH_TX_30]:= FBandwidth_tx_30;
      FieldValues[FLD_BANDWIDTH_TX_A] := FBandwidth_tx_a;
    end;

    if (FieldByName(FLD_BANDWIDTH_TX_30).AsFloat<>0) and
       (FieldByName(FLD_BANDWIDTH_TX_3).AsFloat<>0) and
       (FieldValues[FLD_BANDWIDTH_TX_30] < FieldValues[FLD_BANDWIDTH_TX_3]) then
    begin
      FErrorList.Add('������ ������ ��������� �� ������ ''''-30 [dB]'''' ������ ���� ������, ��� �� ������ ''''-3 [dB]'''' !');

//      ErrDlg('������ ������ ��������� �� ������ ''-30 [dB]'' ������ ���� ������, ��� �� ������ ''-3 [dB]'' !');
      FieldValues[FLD_BANDWIDTH_TX_30]:= FBandwidth_tx_30;
      FieldValues[FLD_BANDWIDTH_TX_3]:= FBandwidth_tx_3;
    end;

    if (FieldByName(FLD_BANDWIDTH_TX_30).AsFloat=0) then
    begin
      FieldValues[FLD_BANDWIDTH_TX_30]:= null;
      FieldValues[FLD_BANDWIDTH_TX_A]:= null;
    end;

    if (FieldByName(FLD_BANDWIDTH_TX_3).AsFloat=0) then
    begin
      FieldValues[FLD_BANDWIDTH_TX_3]:= null;
      FieldValues[FLD_BANDWIDTH_TX_30]:= null;
      FieldValues[FLD_BANDWIDTH_TX_A]:= null;
    end;
  end;

  //rx bandwidths -------------------------
  with DataSet do
  begin
    if (FieldValues[FLD_BANDWIDTH_RX_3]<0) and
       (FieldValues[FLD_BANDWIDTH_RX_3]<>null) then
    begin
      FErrorList.Add(MSG_ERROR_NEGATIVE_RX);
      FieldValues[FLD_BANDWIDTH_RX_3]:= FBandwidth_RX_3;
    end;

    if (FieldValues[FLD_BANDWIDTH_RX_30]<0) and
       (FieldValues[FLD_BANDWIDTH_RX_30]<>null) then
    begin
      FErrorList.Add(MSG_ERROR_NEGATIVE_RX);
      FieldValues[FLD_BANDWIDTH_RX_30]:= FBandwidth_RX_30;
    end;

    if (FieldValues[FLD_BANDWIDTH_RX_A]<0) and
       (FieldValues[FLD_BANDWIDTH_RX_A]<>null) then
    begin
      FErrorList.Add(MSG_ERROR_NEGATIVE_RX);
      FieldValues[FLD_BANDWIDTH_RX_A]:= FBandwidth_RX_a;
    end;

    if (FieldByName(FLD_BANDWIDTH_RX_A).AsFloat<>0) and
       (FieldByName(FLD_BANDWIDTH_RX_30).AsFloat<>0) and
       (FieldValues[FLD_BANDWIDTH_RX_A] < FieldValues[FLD_BANDWIDTH_RX_30]) then
    begin
      FErrorList.Add('������ ����������� ��� �� ������ ''a [dB]'' ������ ���� ������, ��� �� ������ ''-30 [dB]'' !');

//      ErrDlg('������ ����������� ��� �� ������ ''a [dB]'' ������ ���� ������, ��� �� ������ ''-30 [dB]'' !');
      FieldValues[FLD_BANDWIDTH_RX_30]:= FBandwidth_RX_30;
      FieldValues[FLD_BANDWIDTH_RX_A] := FBandwidth_RX_a;
    end;

    if (FieldByName(FLD_BANDWIDTH_RX_30).AsFloat<>0) and
       (FieldByName(FLD_BANDWIDTH_RX_3).AsFloat<>0) and
       (FieldValues[FLD_BANDWIDTH_RX_30] < FieldValues[FLD_BANDWIDTH_RX_3]) then
    begin
      FErrorList.Add('������ ����������� ��� �� ������ ''-30 [dB]'' ������ ���� ������, ��� �� ������ ''-3 [dB]'' !');

//      ErrDlg('������ ����������� ��� �� ������ ''-30 [dB]'' ������ ���� ������, ��� �� ������ ''-3 [dB]'' !');
      FieldValues[FLD_BANDWIDTH_RX_30]:= FBandwidth_RX_30;
      FieldValues[FLD_BANDWIDTH_RX_3] := FBandwidth_RX_3;
    end;

    if (FieldByName(FLD_BANDWIDTH_RX_30).AsFloat=0) then
    begin
      FieldValues[FLD_BANDWIDTH_RX_30]:= null;
      FieldValues[FLD_BANDWIDTH_RX_A]:= null;
    end;

    if (FieldByName(FLD_BANDWIDTH_RX_3).AsFloat=0) then
    begin
      FieldValues[FLD_BANDWIDTH_RX_3]:= null;
      FieldValues[FLD_BANDWIDTH_RX_30]:= null;
      FieldValues[FLD_BANDWIDTH_RX_A]:= null;
    end;

  end;

  if FErrorList.Count>0 then
    ShowMessage(FErrorList.Text);

end;

//----------------------------------------------------------------------
procedure Tframe_LinkEndType_Mode.qry_ModesBeforeEdit(DataSet: TDataSet);
//----------------------------------------------------------------------
begin
  inherited;

  FBandwidth_tx_a := DataSet.FieldByName(FLD_BANDWIDTH_TX_A).AsFloat;
  FBandwidth_tx_30:= DataSet.FieldByName(FLD_BANDWIDTH_TX_30).AsFloat;
  FBandwidth_tx_3 := DataSet.FieldByName(FLD_BANDWIDTH_TX_3).AsFloat;

  FBandwidth_rx_a := DataSet.FieldByName(FLD_BANDWIDTH_RX_A).AsFloat;
  FBandwidth_RX_30:= DataSet.FieldByName(FLD_BANDWIDTH_RX_30).AsFloat;
  FBandwidth_RX_3 := DataSet.FieldByName(FLD_BANDWIDTH_RX_3).AsFloat;

  FPower_Noise    := DataSet.FieldByName(FLD_POWER_NOISE).AsFloat;
end;



procedure Tframe_LinkEndType_Mode.ActionList1Update(Action: TBasicAction; var
    Handled: Boolean);
begin
  act_Copy_record.Enabled := (not FReadOnly)  and  qry_Modes.Active and
                            (qry_Modes.RecordCount>0);

  act_Copy_to_LinkendType.Enabled:=(not FReadOnly)  and  (cxGrid1DBBandedTableView1.Controller.SelectedRowCount>0);
               //    for i := 0 to SelectionCount - 1 do


end;

// ---------------------------------------------------------------
procedure Tframe_LinkEndType_Mode.act_Copy_recordExecute(Sender: TObject);
// ---------------------------------------------------------------

begin

{  // -------------------------
  if Sender = act_Audit then
  // -------------------------
  begin

  sp_Link_Update_AntennaType



    iID:=qry_Modes.FieldByName(FLD_ID).AsInteger;
    if iID>0 then
      Tdlg_Audit_new.ExecDlg (TBL_LinkEndType_Mode, iID);

  end else
}


  // -------------------------
  if Sender = act_Restore_default then
  // -------------------------
  begin
    g_Storage.RestoreFromRegistry(cxGrid1DBBandedTableView1, className+ '_default');
  end else

  // -------------------------
  if Sender=act_Copy_to_LinkendType then  
  // -------------------------
  begin
    Dlg_Select_LinkendType();

  end else


  // -------------------------
  if Sender=act_Copy_record then
  // -------------------------
  begin
    Copy_record();

  end else
    raise Exception.Create('');
    
end;


// ---------------------------------------------------------------
procedure Tframe_LinkEndType_Mode.Copy_record;
// ---------------------------------------------------------------
var
  iID: Integer;
  iMode: Integer;
  sMode: string;
begin
  sMode:='';

    if InputQuery('����� ������ ������','',sMode) then
    begin
      iID:=qry_Modes.FieldByName(FLD_ID).AsInteger;

      iMode:=StrToInt(sMode);

      dmOnega_DB_data.ExecStoredProc_(sp_LinkEndType_mode_Copy,
             [FLD_ID,   iID,
              FLD_MODE, iMode
             ]);

      qry_Modes.Requery();

    end;
end;


// ---------------------------------------------------------------
procedure Tframe_LinkEndType_Mode.SetReadOnly(aValue: boolean);
// ---------------------------------------------------------------
begin
  FReadOnly:= aValue;

  { check : !!! }
  cx_SetGridReadOnly (cxGrid1DBBandedTableView1, aValue);


  if aValue then
    cxGrid1DBBandedTableView1.Styles.Background:=cxStyle_ReadOnly
  else
    cxGrid1DBBandedTableView1.Styles.Background:=cxStyle_Edit

end;


procedure Tframe_LinkEndType_Mode.cxGrid1DBBandedTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  if ARecord.RecordIndex mod 2 = 0 then
    AStyle:=cxStyle_row_2;
end;

procedure Tframe_LinkEndType_Mode.GetMessage(aMsg: TEventType; aParams:
    TEventParamList; var aHandled: boolean);
begin
  if aMsg= et_Refresh_Modes then
    if qry_Modes.Active then
      qry_Modes.Requery([]);
end;


end.

