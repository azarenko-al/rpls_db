object dmLinkEndType_mode: TdmLinkEndType_mode
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 1145
  Top = 635
  Height = 219
  Width = 287
  object qry_Modes: TADOQuery
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=SQL_ONEGA'
    CursorType = ctStatic
    BeforeEdit = qry_ModesBeforeEdit
    BeforePost = qry_ModesBeforePost
    OnNewRecord = qry_ModesNewRecord
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM    LINKENDTYPE_MODE')
    Left = 36
    Top = 12
  end
  object ds_Modulation: TDataSource
    DataSet = tab_Lib_Modulations
    Left = 141
    Top = 68
  end
  object ds_Mode: TDataSource
    DataSet = qry_Modes
    Left = 36
    Top = 68
  end
  object tab_Lib_Modulations: TADOTable
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega'
    TableName = 'Lib_Modulations'
    Left = 144
    Top = 13
  end
end
