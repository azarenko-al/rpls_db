unit d_LinkEndType_Select_Vendor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBCtrls,StdCtrls, ExtCtrls, Grids, DBGrids, DB, ADODB,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  rxPlacemnt, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,

  dm_Main,

  u_const_db,

  u_db,

  cxGrid

  , u_classes, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData;

type
  TLinkEndType_Select_Vendor_rec = record

    Vendor_ID: Integer;
    Vendor_Equipment_ID: Integer;

    Vendor_name : string;
    Vendor_Equipment_name : string;
  end;




  Tdlg_LinkEndType_Select_Vendor = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    Bevel1: TBevel;
    t_lib_Vendor: TADOTable;
    t_lib_Vendorid: TAutoIncField;
    t_lib_Vendorname: TStringField;
    ds_lib_Vendor: TDataSource;
    t_lib_Vendor_Equipment: TADOTable;
    t_lib_Vendor_Equipmentid: TAutoIncField;
    t_lib_Vendor_Equipmentmanufacturer_id: TIntegerField;
    t_lib_Vendor_Equipmentname: TStringField;
    ds_lib_Vendor_Equipment: TDataSource;
    ADOConnection111: TADOConnection;
    Splitter1: TSplitter;
    FormStorage1: TFormStorage;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1DBColumn1: TcxGridDBColumn;
    cxGrid_Modes: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridLevel2: TcxGridLevel;
    procedure FormCreate(Sender: TObject);
  private
    procedure OpenDB;

  public
    class function ExecDlg(var aRec: TLinkEndType_Select_Vendor_rec): Boolean; overload;

    class function ExecDlg(var aVendor_ID, aVendor_Equipment_ID: Integer): Boolean; overload;

  end;


implementation

{$R *.dfm}

// ---------------------------------------------------------------
class function Tdlg_LinkEndType_Select_Vendor.ExecDlg(var aVendor_ID,
    aVendor_Equipment_ID: Integer): Boolean;
// ---------------------------------------------------------------
var
  rec: TLinkEndType_Select_Vendor_rec;
begin
  Rec.Vendor_ID            := aVendor_ID;
  Rec.Vendor_Equipment_ID  := aVendor_Equipment_ID;

  Result := Tdlg_LinkEndType_Select_Vendor.ExecDlg(rec);


  if Result then
  begin
  //  Assert (aRec.Vendor_ID > 0);

    aVendor_ID            := rec.Vendor_ID;
    aVendor_Equipment_ID  := rec.Vendor_Equipment_ID;

   end;
end;

// ---------------------------------------------------------------
class function Tdlg_LinkEndType_Select_Vendor.ExecDlg(var aRec:
    TLinkEndType_Select_Vendor_rec): Boolean;
// ---------------------------------------------------------------
begin
  with Tdlg_LinkEndType_Select_Vendor.Create(Application) do
  begin
    db_TableOpen1(t_lib_Vendor_Equipment);
    db_TableOpen1(t_lib_Vendor);


    if aRec.Vendor_ID>0 then
      t_lib_Vendor.Locate(FLD_ID, aRec.Vendor_ID, []);

    if aRec.Vendor_Equipment_ID>0 then
      t_lib_Vendor_Equipment.Locate(FLD_ID, aRec.Vendor_Equipment_ID, []);


    ShowModal;


    Result := ModalResult=mrOk;

    if ModalResult=mrOk then
    begin
      aRec.Vendor_ID            := t_lib_Vendor.FieldByName(FLD_ID).AsInteger;
      Assert (aRec.Vendor_ID > 0);

      aRec.Vendor_Equipment_ID  := t_lib_Vendor_Equipment.FieldByName(FLD_ID).AsInteger;

      aRec.Vendor_name           := t_lib_Vendor.FieldByName(FLD_NAME).AsString;
      aRec.Vendor_Equipment_name := t_lib_Vendor_Equipment.FieldByName(FLD_NAME).AsString;
    end;

    Free;
  end;

end;


procedure Tdlg_LinkEndType_Select_Vendor.FormCreate(Sender: TObject);
begin
  cxGrid1.Align:=alClient;
  db_SetComponentADOConnection(Self, dmMain.ADOConnection1);
end;


procedure Tdlg_LinkEndType_Select_Vendor.OpenDB;
begin
end;



end.
 
