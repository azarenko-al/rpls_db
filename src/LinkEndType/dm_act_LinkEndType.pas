unit dm_act_LinkEndType;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, ActnList,

  dm_Folder, 

  u_func_msg,

  dm_Main,

  i_Audit,

dm_User_Security,

  fr_LinkEndType_View,

  u_DataExport_run,

  dm_Onega_DB_data,


 // I_Act_LinkEndType,

  d_LinkEndType_Select_Vendor,
  d_LinkEndType_Mode,

  dm_act_Base,

  u_shell_new,

  I_Shell,

  I_Object,


  u_dlg,
  u_func,

  u_const_str,
  u_const,
  u_const_msg,
  u_const_db,

  u_types,

  dm_LinkEndType,
  d_LinkEndType_add,

  u_classes, DB, ADODB;

type                                    //, IAct_LinkEndType_X
  TdmAct_LinkEndType = class(TdmAct_Base)  //ILinkEndTypeX
    act_Copy: TAction;
    ActionList2: TActionList;
    act_Change_Vendor: TAction;
    act_Change_Vendor_Equipment: TAction;
    act_Export_MDB: TAction;
    act_Import_MDB: TAction;
    act_Audit1111111111: TAction;
    act_Update_Field: TAction;
    act_Mode_Generate_mask: TAction;
    act_Audit: TAction;
    act_Replace_bands: TAction;
    ADOStoredProc1: TADOStoredProc;
    procedure act_AuditExecute(Sender: TObject);
//    procedure act_Export_MDBExecute(Sender: TObject);
  //  procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    function CheckActionsEnable: Boolean;
    procedure Copy;
    procedure Dlg_Replace_Bands(aFocusedID: integer);

    procedure DoAction (Sender: TObject);
    procedure Update_Field(aIDList: TIDList);
  protected
    procedure Dlg_Change_Vendor(Sender: TObject; aIDList: TIDList; aUseEquipment: boolean =
        false);

    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm (aOwnerForm: TForm): TForm; override;

    function ItemDel(aID: integer; aName: string=''): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;


    procedure Mode_Generate_Mask(aIDList: TIDList);

  public
//    function Dlg_GetBandID(aLinkEndTypeID: integer; aTxFreq, aRxFreq: double; var
  //      aChannel_Type: string; var aBandID, aLinkEndType_Band_ID: integer): Boolean;

    function Dlg_GetMode(aLinkEndTypeID, aMode_ID: integer; var aRec:
        TdmLinkEndTypeModeInfoRec): boolean;

    //var aMode, aLinkEndType_Mode_ID: Integer;

    class procedure Init;
  end;

var
  dmAct_LinkEndType: TdmAct_LinkEndType;


//====================================================================
// implementation
//====================================================================
implementation

uses
  dm_Act_Explorer
  ; {$R *.dfm}



procedure TdmAct_LinkEndType.act_AuditExecute(Sender: TObject);
begin
//  Tdlg_Audit.execDlg ;
end;

//--------------------------------------------------------------------
class procedure TdmAct_LinkEndType.Init;
//--------------------------------------------------------------------
begin
  Assert(not Assigned(dmAct_LinkEndType));
//  if not Assigned(dmAct_LinkEndType) then

  dmAct_LinkEndType:=TdmAct_LinkEndType.Create(Application);
end;


//--------------------------------------------------------------------
procedure TdmAct_LinkEndType.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
var b: Boolean;

begin
  inherited;

  ObjectName:=OBJ_LINKEND_TYPE;

//  act_Copy.Caption:='����������';
  act_Copy.Caption         := STR_ACT_COPY;

  act_Add.Caption :='������� �� ������������';
  act_Export_MDB.Caption :='Export_MDB';

  act_Update_Field.Caption :='��������� ������';

 //        if dmAct_Explorer.Dlg_Select_Object (otLinkEndType, iLinkEndType_ID, sName) then

  act_Replace_bands.Caption:='������������� �������';


  act_Mode_Generate_mask.Caption :='��������� ��������� � ����� �������';

//  act_Change_Vendor.Caption :='������� �� ������������';
  act_Export_MDB.Caption:=DEF_STR_Export_MDB;
  act_Import_MDB.Caption:=DEF_STR_Import_MDB;

  act_Audit.Caption:=STR_Audit;


{  act_Audit.Caption:=STR_Audit;
  act_Audit.Enabled:=False;
}

  SetActionsExecuteProc ([
                           act_Export_MDB,
                           act_Audit,
                           
//                           act_Import_MDB,

                           act_Copy,
                           act_Change_Vendor,
                           act_Change_Vendor_Equipment,

                           act_Update_Field,
                           act_Replace_bands,

                           act_Mode_Generate_mask


                         //  act_Audit

                         ], DoAction);

  {
 SetActionsEnabled1)( [
                        //   act_Export_MDB,
//                           act_Import_MDB,

                           act_Copy,
                           act_Change_Vendor,
                           act_Change_Vendor_Equipment,

                           act_Update_Field,

                           act_Mode_copy_tx_to_RX

                      ],
   dmUser_Security.Is_Lib_Edit_allow );
   }

end;


//..       if dmAct_Explorer.Dlg_Select_Object (otLinkEndType, iLinkEndType_ID, sName) then

//--------------------------------------------------------------------
procedure TdmAct_LinkEndType.Dlg_Replace_Bands(aFocusedID: integer);
//--------------------------------------------------------------------
var
  i: Integer;
  iLinkEndType_ID : Integer;
  k: Integer;
  sName: WideString;
begin
//  iVendor_ID:=0;
//  iVendor_Equipment_ID:=0;

  if dmAct_Explorer.Dlg_Select_Object (otLinkEndType, iLinkEndType_ID, sName) then
  //  for i := 0 to aIDList.Count-1 do
    begin
   //   Assert ( iVendor_ID > 0);

      k:=dmOnega_DB_data.ExecStoredProc_('lib.sp_LinkEndType_replace_bands',
        [
          'DEST_LinkEndType_ID', aFocusedID,
          'SRC_LinkEndType_ID',  iLinkEndType_ID
        ] );


        g_EventManager.PostEvent_ (et_Refresh_Modes, []);
  //      g_ShellEvents.Shell_UpdateNodeChildren_ByObjName(OBJ_LinkEnd_Type, aFocusedID);

    end;

//      dmLinkEnd.UpdateLinkEndType (aIDList[i].ID, iLinkEndTypeID, iMode);

end;

//--------------------------------------------------------------------
procedure TdmAct_LinkEndType.Dlg_Change_Vendor(Sender: TObject; aIDList: TIDList;
    aUseEquipment: boolean = false);
//--------------------------------------------------------------------
var
  i: Integer;
  iVendor_ID, iVendor_Equipment_ID: Integer;
  k: Integer;
begin
  iVendor_ID:=0;
  iVendor_Equipment_ID:=0;

  if Tdlg_LinkEndType_Select_Vendor.ExecDlg(iVendor_ID, iVendor_Equipment_ID) then
    for i := 0 to aIDList.Count-1 do
    begin
      Assert ( iVendor_ID > 0);

      k:=dmOnega_DB_data.LinkEndType_update_vendor
        (aIDList[i].ID, iVendor_ID, IIF(aUseEquipment, iVendor_Equipment_ID, 0));

    end;

//      dmLinkEnd.UpdateLinkEndType (aIDList[i].ID, iLinkEndTypeID, iMode);

end;

//--------------------------------------------------------------------
function TdmAct_LinkEndType.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_LinkEndType_add.ExecDlg (aFolderID);
end;

//--------------------------------------------------------------------
function TdmAct_LinkEndType.ItemDel(aID: integer; aName: string=''): boolean;
//--------------------------------------------------------------------
begin
  Result:=dmLinkEndType.Del (aID);
end;


// ---------------------------------------------------------------
function TdmAct_LinkEndType.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
  Result:= dmUser_Security. Modify_lib in [mtAllow, mtAllowMy];

  SetActionsEnabled1( [

 act_Export_MDB,
//                           act_Import_MDB,

  act_Copy,
  act_Change_Vendor,
  act_Change_Vendor_Equipment,

  act_Update_Field,

  act_Mode_Generate_mask,
  act_Del_,
  act_Del_list

      ],

   Result );

   //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );


end;

// ---------------------------------------------------------------
procedure TdmAct_LinkEndType.Copy;
// ---------------------------------------------------------------
var
  iFolder_ID: Integer;
  iID: integer;
  sFolderGUID: string;
  sNewName: string;
begin
   sNewName:=FFocusedName +' (�����)';

   if InputQuery ('����� ������','������� ����� ��������', sNewName) then
   begin
     iID:=dmOnega_DB_data.LinkEndType_Copy(FFocusedID, sNewName);

   //  iID:=dmLinkEndType.Copy (FFocusedID, sNewName);
     if iID>0 then
       g_Shell.UpdateNodeChildren_ByGUID(GUID_LinkEnd_Type);


     iFolder_ID:=dmLinkEndType.GetFolderID(iID);

   //  if aFolderID=0 then
     if iFolder_ID>0 then
       sFolderGUID:=dmFolder.GetGUIDByID(iFolder_ID)
     else
       sFolderGUID:=g_Obj.ItemByName[OBJ_LinkEnd_Type].RootFolderGUID;
    // else
     //  sFolderGUID:=dmFolder.GetGUIDByID(aFolderID);

     g_Shell.UpdateNodeChildren_ByGUID (sFolderGUID);

   //  g_ShellEvents.Shell_EXPAND_BY_GUID(sFolderGUID);
     g_Shell.EXPAND_BY_GUID(sFolderGUID);

     g_Shell.FocuseNodeByObjName (OBJ_LinkEnd_Type, iID);

  end;
end;



//--------------------------------------------------------------------
procedure TdmAct_LinkEndType.GetPopupMenu;
//--------------------------------------------------------------------
var
  bEnable: Boolean;
begin
  bEnable:=CheckActionsEnable();


  case aMenuType of
    mtFolder: begin
                AddFolderMenu_Create (aPopupMenu, bEnable);
                AddFolderPopupMenu(aPopupMenu, bEnable);

                AddMenuItem (aPopupMenu, act_Export_MDB);

              end;

    mtItem:   begin
                AddMenuItem (aPopupMenu, act_Del_);
                AddMenuItem (aPopupMenu, act_Copy);
                AddMenuItem (aPopupMenu, nil);

                AddFolderMenu_Tools (aPopupMenu);

                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Export_MDB);
                AddMenuItem (aPopupMenu, act_Update_Field);

                AddMenuItem (aPopupMenu, act_Mode_Generate_mask);
                AddMenuItem (aPopupMenu, act_Audit);
                AddMenuItem (aPopupMenu, nil);

                AddMenuItem (aPopupMenu, act_Replace_bands);


             //   AddMenuItem (aPopupMenu, act_Import_MDB);

             //   AddMenuItem (aPopupMenu, act_Audit);
              end;
    mtList:   begin
                AddMenuItem (aPopupMenu, act_Del_list);
                AddFolderMenu_Tools (aPopupMenu);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Export_MDB);
              //  AddMenuItem (aPopupMenu, act_Import_MDB);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Change_Vendor);
                AddMenuItem (aPopupMenu, act_Change_Vendor_Equipment);

                AddMenuItem (aPopupMenu, act_Update_Field);

                AddMenuItem (aPopupMenu, act_Mode_Generate_mask);

              end;
  end;
end;




//--------------------------------------------------------------------
procedure TdmAct_LinkEndType.Mode_Generate_Mask(aIDList: TIDList);
//--------------------------------------------------------------------
var
  k,i: Integer;

begin

  if ConfirmDlg('��������� ?') then
    for i := 0 to aIDList.Count-1 do
      k:=dmOnega_DB_data.ExecStoredProc_ ('sp_LinkEndType_Generate_Mask',
        [FLD_LinkEndType_ID, aIDList[i].ID  ]);

//        sp_LinkEndType_Generate_tx_RX


end;


//--------------------------------------------------------------------
procedure TdmAct_LinkEndType.Update_Field(aIDList: TIDList);
//--------------------------------------------------------------------
var
  s: string;
  sValue: string;
begin
  s:=aIDList.ToString();

  if not InputQuery('��������� ������', '��������� - ������(MHz)', sValue) then
    Exit;



//    function InputQuery(const ACaption, APrompt: string;
//  var Value: string): Boolean;


 {
  iVendor_ID:=0;
  iVendor_Equipment_ID:=0;

  if Tdlg_LinkEndType_Select_Vendor.ExecDlg(iVendor_ID, iVendor_Equipment_ID) then
    for i := 0 to aIDList.Count-1 do
    begin
      k:=dmOnega_DB_data.LinkEndType_update_vendor
        (aIDList[i].ID, iVendor_ID, IIF(aUseEquipment, iVendor_Equipment_ID, 0));

    end;
  }

//      dmLinkEnd.UpdateLinkEndType (aIDList[i].ID, iLinkEndTypeID, iMode);

end;

//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------

// -------------------------------------------------------------------
procedure TdmAct_LinkEndType.DoAction (Sender: TObject);
// -------------------------------------------------------------------
var
  iID: integer;
  sFolderGUID,sNewName: string;

  i: Integer;
  iVendor_ID, iVendor_Equipment_ID: Integer;
begin


  if Sender=act_Replace_bands then
    Dlg_Replace_Bands (FFocusedID) else

  if Sender=act_Audit then
    Init_IAuditX.ExecDlg( dmMain.GetConnectionString,  TBL_LINKENDTYPE, FFocusedID) else
//     Tdlg_Audit.ExecDlg (TBL_ANTENNATYPE, FFocusedID);



  if Sender=act_Mode_Generate_mask then
    Mode_Generate_Mask ( FSelectedIDList)
  else


  if Sender=act_Update_Field then
    Update_Field ( FSelectedIDList)
  else


  if Sender=act_Export_MDB then
//    TDataExport_run.ExportToRplsDbGuides1 (OBJ_LINKEND_TYPE);
    TDataExport_run.ExportToRplsDb_selected (TBL_LINKENDTYPE, FSelectedIDList)
  else


(*
     sNewName:=FFocusedName +' (�����)';

    if InputQuery ('����� ������','������� ����� ��������', sNewName) then
    begin
      iID:=dmOnega_DB_data.AntType_Copy(FFocusedID, sNewName);

*)



  if Sender=act_Copy then
  begin
   (*TODO: extracted code
    sNewName:=FFocusedName +' (�����)';

    if InputQuery ('����� ������','������� ����� ��������', sNewName) then
    begin
      iID:=dmOnega_DB_data.LinkEndType_Copy(FFocusedID, sNewName);

    //  iID:=dmLinkEndType.Copy (FFocusedID, sNewName);
      if iID>0 then
        g_Shell.UpdateNodeChildren_ByGUID(GUID_LinkEnd_Type);


     iFolder_ID:=dmLink.GetFolderID(iID);

    //  if aFolderID=0 then
      if Frec.Folder_ID>0 then
        sFolderGUID:=dmFolder.GetGUIDByID(iFolder_ID)
      else
        sFolderGUID:=g_Obj.ItemByName[OBJ_PROPERTY].RootFolderGUID;
     // else
      //  sFolderGUID:=dmFolder.GetGUIDByID(aFolderID);

      g_Shell.UpdateNodeChildren_ByGUID (sFolderGUID);

    //  g_ShellEvents.Shell_EXPAND_BY_GUID(sFolderGUID);
      g_Shell.EXPAND_BY_GUID(sFolderGUID);

      g_Shell.FocuseNodeByObjName (OBJ_LINK, iID);

   end;
   *)
   Copy;
 end else



  if Sender=act_Change_Vendor then
    Dlg_Change_Vendor (Sender, FSelectedIDList, False)  else

  if Sender=act_Change_Vendor_Equipment then
    Dlg_Change_Vendor (Sender, FSelectedIDList, True)

  else
    raise Exception.Create('');

end;


function TdmAct_LinkEndType.GetViewForm(aOwnerForm: TForm): TForm;
begin
  Result:=Tframe_LinkEndType_View.Create(aOwnerForm);
end;


function TdmAct_LinkEndType.Dlg_GetMode(aLinkEndTypeID, aMode_ID: integer; var
    aRec: TdmLinkEndTypeModeInfoRec): boolean;
var
  iID: integer;
begin
  iID:= Tdlg_LinkEndType_Mode.Dlg_GetMode1 (aLinkEndTypeID, aMode_ID, aRec);
  //aMode_ID,

  Result := iID>0;

end;


begin
end.



{
       if dmAct_Explorer.Dlg_Select_Object (otLinkEndType, iLinkEndType_ID, sName) then
