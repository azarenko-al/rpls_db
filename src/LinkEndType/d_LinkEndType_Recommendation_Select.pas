unit d_LinkEndType_Recommendation_Select;

interface

uses

  dm_Main,

  u_const_db,
  dm_Onega_DB_data,

//  u_LinkEndType_str,

  u_db,
  u_classes,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBCtrls,StdCtrls, ExtCtrls, Grids, DBGrids, DB, ADODB,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  rxPlacemnt, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  
  cxGrid,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  dxSkinsCore, dxSkinsDefaultPainters, ActnList, cxCurrencyEdit, cxCheckBox;

type

  Tdlg_LinkEndType_Recommendation_Select_rec = record
    Band: string;
    Recommendation: string;

    freq_min_MHz: double;
    freq_max_MHz: double;
  end;


  Tdlg_LinkEndType_Recommendation_Select = class(TForm)
    Panel1: TPanel;
    btn_Ok: TButton;
    Button2: TButton;
    Bevel1: TBevel;
    ds_Recommendation: TDataSource;
    ADOConnection1: TADOConnection;
    FormStorage1: TFormStorage;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    col_name: TcxGridDBColumn;
    ActionList1: TActionList;
    col_freq_min_MHz: TcxGridDBColumn;
    col_freq_max_MHz: TcxGridDBColumn;
    col_band: TcxGridDBColumn;
    ADOStoredProc1: TADOStoredProc;
    ds_bandwidth: TDataSource;
    t_bandwidth: TADOTable;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1id: TcxGridDBColumn;
    cxGridDBTableView1band: TcxGridDBColumn;
    col_bandwidth: TcxGridDBColumn;
    col_checked: TcxGridDBColumn;
    Splitter1: TSplitter;
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    procedure OpenDB;

  public
    class function ExecDlg(var aRec: Tdlg_LinkEndType_Recommendation_Select_rec): Boolean; overload;

  end;


implementation
{$R *.dfm}

procedure Tdlg_LinkEndType_Recommendation_Select.ActionList1Update(Action:
    TBasicAction; var Handled: Boolean);
begin
  btn_Ok.Enabled:=ADOStoredProc1.RecordCount>0;
end;


// ---------------------------------------------------------------
class function Tdlg_LinkEndType_Recommendation_Select.ExecDlg(var aRec:
    Tdlg_LinkEndType_Recommendation_Select_rec): Boolean;
// ---------------------------------------------------------------
begin
  with Tdlg_LinkEndType_Recommendation_Select.Create(Application) do
  begin
//    dmOnega_DB_data.OpenQuery_(ADOQuery1, 'select * from status where band is not null', []);
    dmOnega_DB_data.OpenStoredProc(ADOStoredProc1, 'lib.sp_Band_SEL', []);

    t_bandwidth.Connection:=dmOnega_DB_data.ADOConnection;
    t_bandwidth.Open;

//    dmOnega_DB_data. OpenStoredProc(ADOStoredProc1, 'lib.sp_Band_SEL', []);


//    dmOnega_DB_data.OpenQuery_(ADOQuery1, 'select * from status where band=:band', ['band',aRec.Band]);

//    db_TableOpen1(t_Recommendation);
//    db_TableOpen1(t_lib_Vendor);

//     t_Recommendation.Filter:='band='+ aRec.Band;
//     t_Recommendation.Filtered:=true;

//    if aRec.Vendor_ID>0 then
//     if t_Recommendation.RecordCount>0 then
     ADOStoredProc1.Locate(FLD_BAND, aRec.Band, []);

//    if aRec.Vendor_Equipment_ID>0 then
//      t_lib_Vendor_Equipment.Locate(FLD_ID, aRec.Vendor_Equipment_ID, []);


    ShowModal;

    Result := ModalResult=mrOk;

    if ModalResult=mrOk then
    begin
      aRec.Band           := ADOStoredProc1.FieldByName(FLD_Band).AsString;
      aRec.Recommendation := ADOStoredProc1.FieldByName(FLD_name).AsString;

      aRec.freq_min_MHz := ADOStoredProc1.FieldByName(FLD_freq_min_MHz).AsFloat;
      aRec.freq_max_MHz := ADOStoredProc1.FieldByName(FLD_freq_max_MHz).AsFloat;
    end;

    Free;
  end;

end;

//----------------------------------------------------------------
procedure Tdlg_LinkEndType_Recommendation_Select.FormCreate(Sender: TObject);
//----------------------------------------------------------------
begin
  cxGrid1.Align:=alClient;

  Caption:='����� ������������ ���-R...';

  if ADOConnection1.Connected then
    ShowMessage ('ADOConnection1.Connected');


  col_Band.Caption        :='�������� ������';
  col_FREQ_Min_MHZ.Caption:='������ ������� ��������� [MHz]';
  col_FREQ_Max_MHZ.Caption:='������� ������� ��������� [MHz]';

  col_checked.Caption  :='���';
  col_bandwidth.Caption:='������ ������ �������';



//  db_SetComponentADOConnection(Self, dmMain.ADOConnection1);
end;


procedure Tdlg_LinkEndType_Recommendation_Select.OpenDB;
begin
end;



end.

