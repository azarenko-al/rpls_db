object dlg_LinkEndType_Select_Vendor: Tdlg_LinkEndType_Select_Vendor
  Left = 1314
  Top = 222
  Width = 394
  Height = 586
  BorderIcons = [biSystemMenu, biMinimize]
  BorderWidth = 5
  Caption = #1042#1099#1073#1086#1088' '#1084#1086#1076#1077#1083#1080' '#1056#1056#1057
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 320
    Width = 368
    Height = 3
    Cursor = crVSplit
    Align = alBottom
  end
  object Panel1: TPanel
    Left = 0
    Top = 504
    Width = 368
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      368
      33)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 368
      Height = 3
      Align = alTop
      Shape = bsTopLine
    end
    object Button1: TButton
      Left = 220
      Top = 10
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Caption = 'Ok'
      ModalResult = 1
      TabOrder = 0
    end
    object Button2: TButton
      Left = 300
      Top = 10
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ModalResult = 2
      TabOrder = 1
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 368
    Height = 136
    Align = alTop
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = ds_lib_Vendor
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsView.Navigator = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGrid1DBTableView1DBColumn1: TcxGridDBColumn
        Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
        DataBinding.FieldName = 'name'
        Width = 162
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxGrid_Modes: TcxGrid
    Left = 0
    Top = 323
    Width = 368
    Height = 181
    Align = alBottom
    TabOrder = 2
    LookAndFeel.Kind = lfFlat
    object cxGridDBTableView2: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = ds_lib_Vendor_Equipment
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsView.Navigator = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGridDBColumn2: TcxGridDBColumn
        Caption = #1051#1080#1085#1077#1081#1082#1072' '#1086#1073#1086#1088#1091#1076#1086#1074#1072#1085#1080#1103
        DataBinding.FieldName = 'name'
        Width = 162
      end
    end
    object cxGridLevel2: TcxGridLevel
      GridView = cxGridDBTableView2
    end
  end
  object t_lib_Vendor: TADOTable
    Connection = ADOConnection111
    CursorType = ctStatic
    TableName = 'lib_Vendor'
    Left = 156
    Top = 147
    object t_lib_Vendorid: TAutoIncField
      FieldName = 'id'
      ReadOnly = True
    end
    object t_lib_Vendorname: TStringField
      FieldName = 'name'
      Size = 50
    end
  end
  object ds_lib_Vendor: TDataSource
    DataSet = t_lib_Vendor
    Left = 156
    Top = 195
  end
  object t_lib_Vendor_Equipment: TADOTable
    Connection = ADOConnection111
    CursorType = ctStatic
    IndexFieldNames = 'Vendor_id'
    MasterFields = 'id'
    MasterSource = ds_lib_Vendor
    TableName = 'lib_Vendor_Equipment'
    Left = 270
    Top = 146
    object t_lib_Vendor_Equipmentid: TAutoIncField
      FieldName = 'id'
      ReadOnly = True
    end
    object t_lib_Vendor_Equipmentmanufacturer_id: TIntegerField
      FieldName = 'Vendor_id'
    end
    object t_lib_Vendor_Equipmentname: TStringField
      FieldName = 'name'
      Size = 50
    end
  end
  object ds_lib_Vendor_Equipment: TDataSource
    DataSet = t_lib_Vendor_Equipment
    Left = 268
    Top = 195
  end
  object ADOConnection111: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega1'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 32
    Top = 152
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 32
    Top = 208
  end
end
