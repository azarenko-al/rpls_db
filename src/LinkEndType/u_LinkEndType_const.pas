unit u_LinkEndType_const;

interface

const
  CAPTION_MODE = '�����';

  CAPTION_BANDWIDTH = '������ ������ ������� [MHz]';


  CAPTION_BAND1            = '������ ������ ��������� [MHz] �� ������:';
  CAPTION_BAND2            = '���������';
  CAPTION_BAND3            = '������ ����������� ��� [MHz] �� ������:';

  CAPTION_name = '��������';


  CAPTION_BITRATE_Mbps     = '�������� �������� [Mbit/s]';
  CAPTION_MODULATION_TYPE  = '��� ���������';

  CAPTION_fade_margin_dB = '����� �� ��������� [dB]';

  CAPTION_modulation_level_count  = '���-�� �������  ���������';
  CAPTION_GOST_53363_modulation   = '���������';

  CAPTION_THRESHOLD_BER_3  = '���������������� ��� BER 10^-3 [dBm]';
  CAPTION_THRESHOLD_BER_6  = '���������������� ��� BER 10^-6 [dBm]';
  CAPTION_POWER_MAX_dBm    = '����������� �������� ��� [dBm]';
//  CAPTION_BANDWIDTH        = '������ ������ [MHz]';

  CAPTION_CH_SPACING       = '������ �������� ������� [MHz]';
  CAPTION_POWER_NOISE      = '�������� ���� [dBm]';


  CAPTION_SIGNATURE_WIDTH  = '������ [MHz]';
  CAPTION_SIGNATURE_HEIGHT = '������ [dB]';

  FLD_low   ='low';
  FLD_high  ='high';


  FLD_TX_Freq_LOW = 'TX_Freq_LOW';
  FLD_RX_Freq_LOW = 'RX_Freq_LOW';

  FLD_TX_Freq_HIGH = 'TX_Freq_HIGH';
  FLD_RX_Freq_HIGH = 'RX_Freq_HIGH';


implementation

end.
