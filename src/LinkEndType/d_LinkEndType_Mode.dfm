inherited dlg_LinkEndType_Mode: Tdlg_LinkEndType_Mode
  Left = 719
  Top = 326
  Width = 817
  Height = 368
  Caption = 'dlg_LinkEndType_Mode'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    Width = 801
    Height = 69
  end
  object pn_Buttons: TPanel [1]
    Left = 0
    Top = 294
    Width = 801
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 1
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 801
      Height = 2
      Align = alTop
      Shape = bsTopLine
      Visible = False
    end
    object Panel1: TPanel
      Left = 608
      Top = 2
      Width = 193
      Height = 33
      Align = alRight
      TabOrder = 0
      object btn_Ok: TButton
        Left = 14
        Top = 5
        Width = 75
        Height = 23
        Caption = 'Ok'
        Default = True
        ModalResult = 1
        TabOrder = 0
      end
      object btn_Cancel: TButton
        Left = 102
        Top = 5
        Width = 75
        Height = 23
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        ModalResult = 2
        TabOrder = 1
      end
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 69
    Width = 801
    Height = 156
    Align = alTop
    TabOrder = 2
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBBandedTableView1: TcxGridDBBandedTableView
      PopupMenu = PopupMenu1
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = ds_Mode
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.HeaderAutoHeight = True
      OptionsView.Indicator = True
      OptionsView.FixedBandSeparatorWidth = 1
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      Bands = <
        item
        end
        item
          Caption = #1064#1080#1088#1080#1085#1072' '#1087#1086#1083#1086#1089#1099' '#1080#1079#1083#1091#1095#1077#1085#1080#1103' [MHz] '#1085#1072' '#1091#1088#1086#1074#1085#1077':'
        end
        item
          Caption = #1057#1080#1075#1085#1072#1090#1091#1088#1072
        end
        item
          Caption = #1055#1086#1083#1086#1089#1072' '#1087#1088#1086#1087#1091#1089#1082#1072#1085#1080#1103' '#1059#1042#1063' [MHz] '#1085#1072' '#1091#1088#1086#1074#1085#1077':'
        end
        item
          Caption = #1043#1054#1057#1058
        end>
      object col__ID: TcxGridDBBandedColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Options.Filtering = False
        Width = 41
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object col__Mode: TcxGridDBBandedColumn
        Caption = #1056#1077#1078#1080#1084
        DataBinding.FieldName = 'Mode'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 50
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object col_Bitrate_Mbps: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Bitrate_Mbps'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 60
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object col__bandwidth: TcxGridDBBandedColumn
        DataBinding.FieldName = 'bandwidth'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 107
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object col__Modulation_type: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Modulation_type'
        Options.Filtering = False
        Width = 85
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object col__traffic_type: TcxGridDBBandedColumn
        Caption = #1058#1080#1087' '#1090#1088#1072#1092#1080#1082#1072
        DataBinding.FieldName = 'traffic_type'
        Options.Filtering = False
        Width = 86
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object col__radiation_class: TcxGridDBBandedColumn
        Caption = #1050#1083#1072#1089#1089' '#1080#1079#1083#1091#1095#1077#1085#1080#1103
        DataBinding.FieldName = 'radiation_class'
        Options.Filtering = False
        Width = 95
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object col__Power_max: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Power_max'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 88
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object col__power_noise: TcxGridDBBandedColumn
        DataBinding.FieldName = 'power_noise'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 66
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object col__bandwidth_tx_3: TcxGridDBBandedColumn
        Caption = '-3 [dB]'
        DataBinding.FieldName = 'bandwidth_tx_3'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 48
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object col__bandwidth_tx_30: TcxGridDBBandedColumn
        Caption = '-30 [dB]'
        DataBinding.FieldName = 'bandwidth_tx_30'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 44
        Position.BandIndex = 1
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object col__bandwidth_tx_a: TcxGridDBBandedColumn
        Caption = 'a [dB]'
        DataBinding.FieldName = 'bandwidth_tx_a'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 35
        Position.BandIndex = 1
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object col__threshold_BER_3: TcxGridDBBandedColumn
        DataBinding.FieldName = 'threshold_BER_3'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 101
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
      object col__threshold_BER_6: TcxGridDBBandedColumn
        DataBinding.FieldName = 'threshold_BER_6'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 126
        Position.BandIndex = 0
        Position.ColIndex = 11
        Position.RowIndex = 0
      end
      object col__bandwidth_rx_3: TcxGridDBBandedColumn
        Caption = '-3 [dB]'
        DataBinding.FieldName = 'bandwidth_rx_3'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 37
        Position.BandIndex = 3
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object col__bandwidth_rx_30: TcxGridDBBandedColumn
        Caption = '-30 [dB]'
        DataBinding.FieldName = 'bandwidth_rx_30'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 43
        Position.BandIndex = 3
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object col__bandwidth_rx_a: TcxGridDBBandedColumn
        Caption = 'a [dB]'
        DataBinding.FieldName = 'bandwidth_rx_a'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 34
        Position.BandIndex = 3
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object col__ch_spacing: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ch_spacing'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 61
        Position.BandIndex = 0
        Position.ColIndex = 12
        Position.RowIndex = 0
      end
      object col__signature_width: TcxGridDBBandedColumn
        DataBinding.FieldName = 'signature_width'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 80
        Position.BandIndex = 2
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object col__signature_height: TcxGridDBBandedColumn
        DataBinding.FieldName = 'signature_height'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 84
        Position.BandIndex = 2
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object col_GOST_modulation_level_count: TcxGridDBBandedColumn
        DataBinding.FieldName = 'modulation_level_count'
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object col_GOST_modulation: TcxGridDBBandedColumn
        DataBinding.FieldName = 'GOST_53363_modulation'
        Options.Filtering = False
        Position.BandIndex = 4
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBBandedTableView1
    end
  end
  inherited ActionList: TActionList
    Left = 104
    object act_Grid_Restore_default: TAction
      Caption = 'act_Grid_Restore_default'
      OnExecute = act_Grid_Restore_defaultExecute
    end
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 172
  end
  object FormPlacement1: TFormPlacement
    IniFileName = 'Software\Onega\Common\Forms\Tframe_LinkEndType_Mode'
    Left = 41
    Top = 65535
  end
  object qry_Modes: TADOQuery
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM    view_LINKENDTYPE_MODE')
    Left = 28
    Top = 236
  end
  object ds_Mode: TDataSource
    DataSet = qry_Modes
    Left = 84
    Top = 236
  end
  object PopupMenu1: TPopupMenu
    Left = 200
    Top = 240
    object MenuItem2: TMenuItem
      Action = act_Grid_Restore_default
    end
  end
end
