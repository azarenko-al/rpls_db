inherited dlg_LinkEndType_add: Tdlg_LinkEndType_add
  Left = 593
  Top = 245
  Width = 602
  Height = 527
  Caption = 'dlg_LinkEndType_add'
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 453
    Width = 586
    inherited Bevel1: TBevel
      Width = 586
    end
    inherited Panel3: TPanel
      Left = 390
      Width = 196
      inherited btn_Ok: TButton
        Left = 8
      end
      inherited btn_Cancel: TButton
        Left = 92
      end
    end
    object Button1: TButton
      Left = 4
      Top = 7
      Width = 149
      Height = 23
      Caption = #1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1091#1084#1086#1083#1095#1072#1085#1080#1103
      TabOrder = 2
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 192
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Button2'
      TabOrder = 1
      Visible = False
      OnClick = Button2Click
    end
  end
  inherited pn_Top_: TPanel
    Width = 586
    inherited Bevel2: TBevel
      Width = 586
    end
    inherited pn_Header: TPanel
      Width = 586
      Height = 55
    end
  end
  inherited Panel1: TPanel
    Width = 586
    inherited ed_Name_: TEdit
      Width = 582
    end
  end
  inherited Panel2: TPanel
    Width = 586
    Height = 318
    inherited PageControl1: TPageControl
      Width = 576
      Height = 217
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 568
          Height = 186
          Align = alClient
          LookAndFeel.Kind = lfFlat
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 277
          OptionsView.ValueWidth = 40
          TabOrder = 0
          Version = 1
          object Row_Folder: TcxEditorRow
            Properties.Caption = 'Folder'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object cxVerticalGrid1CategoryRow2: TcxCategoryRow
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
          object row_Band: TcxEditorRow
            Expanded = False
            Properties.Caption = 'Band'
            Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 2
            ParentID = -1
            Index = 2
            Version = 1
          end
          object r_Freq_Min: TcxEditorRow
            Expanded = False
            Properties.Caption = 'Freq_Min'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = '12750'
            ID = 3
            ParentID = -1
            Index = 3
            Version = 1
          end
          object r_Freq_Max: TcxEditorRow
            Expanded = False
            Properties.Caption = 'Freq_Max'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = '13250'
            ID = 4
            ParentID = -1
            Index = 4
            Version = 1
          end
          object cxVerticalGrid1EditorRow4: TcxEditorRow
            Expanded = False
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 5
            ParentID = -1
            Index = 5
            Version = 1
          end
          object r_Power_max: TcxEditorRow
            Expanded = False
            Properties.Caption = 'Power_max'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 6
            ParentID = -1
            Index = 6
            Version = 1
          end
          object r_power_loss: TcxEditorRow
            Expanded = False
            Properties.Caption = 'power_loss'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 7
            ParentID = -1
            Index = 7
            Version = 1
          end
          object cxVerticalGrid1CategoryRow1: TcxCategoryRow
            Expanded = False
            ID = 8
            ParentID = -1
            Index = 8
            Version = 1
          end
          object r_failure_period: TcxEditorRow
            Expanded = False
            Properties.Caption = 'row_failure_period'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 9
            ParentID = -1
            Index = 9
            Version = 1
          end
        end
      end
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 284
  end
  inherited FormStorage1: TFormStorage
    Left = 252
  end
end
