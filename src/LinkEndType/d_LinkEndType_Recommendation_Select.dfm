object dlg_LinkEndType_Recommendation_Select: Tdlg_LinkEndType_Recommendation_Select
  Left = 845
  Top = 260
  Width = 598
  Height = 586
  BorderIcons = [biSystemMenu, biMinimize]
  BorderWidth = 5
  Caption = 'dlg_LinkEndType_Recommendation_Select'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 421
    Width = 572
    Height = 3
    Cursor = crVSplit
    Align = alBottom
  end
  object Panel1: TPanel
    Left = 0
    Top = 504
    Width = 572
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      572
      33)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 572
      Height = 3
      Align = alTop
      Shape = bsTopLine
    end
    object btn_Ok: TButton
      Left = 413
      Top = 10
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object Button2: TButton
      Left = 493
      Top = 10
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ModalResult = 2
      TabOrder = 1
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 572
    Height = 209
    Align = alTop
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = ds_Recommendation
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnFiltering = False
      OptionsSelection.CellSelect = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object col_band: TcxGridDBColumn
        DataBinding.FieldName = 'band'
        Options.Sorting = False
      end
      object col_name: TcxGridDBColumn
        Caption = #1056#1077#1082#1086#1084#1077#1085#1076#1072#1094#1080#1103' '#1052#1057#1069'-R'
        DataBinding.FieldName = 'name'
        Options.Editing = False
        Options.Filtering = False
        Width = 162
      end
      object col_freq_min_MHz: TcxGridDBColumn
        DataBinding.FieldName = 'freq_min_MHz'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0'
      end
      object col_freq_max_MHz: TcxGridDBColumn
        DataBinding.FieldName = 'freq_max_MHz'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxGrid2: TcxGrid
    Left = 0
    Top = 424
    Width = 572
    Height = 80
    Align = alBottom
    TabOrder = 2
    LookAndFeel.Kind = lfFlat
    object cxGridDBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = ds_bandwidth
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnFiltering = False
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object col_checked: TcxGridDBColumn
        DataBinding.FieldName = 'checked'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.ImmediatePost = True
        Width = 76
      end
      object cxGridDBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Width = 27
      end
      object cxGridDBTableView1band: TcxGridDBColumn
        DataBinding.FieldName = 'band'
        Visible = False
        Width = 77
      end
      object col_bandwidth: TcxGridDBColumn
        DataBinding.FieldName = 'bandwidth'
        Width = 100
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object ds_Recommendation: TDataSource
    DataSet = ADOStoredProc1
    Left = 156
    Top = 275
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=server1'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 32
    Top = 224
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 32
    Top = 280
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 416
    Top = 232
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'lib.sp_Band_SEL'
    Parameters = <>
    Left = 152
    Top = 224
  end
  object t_bandwidth: TADOTable
    Connection = ADOConnection1
    CursorType = ctStatic
    IndexFieldNames = 'band'
    MasterFields = 'band'
    MasterSource = ds_Recommendation
    TableName = 'lib.band_bandwidth'
    Left = 280
    Top = 224
  end
  object ds_bandwidth: TDataSource
    DataSet = t_bandwidth
    Left = 276
    Top = 275
  end
end
