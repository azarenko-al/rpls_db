unit d_LinkEndType_Mode;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,

  cxGridBandedTableView, cxGridDBBandedTableView, cxClasses, cxControls,
  Math, rxPlacemnt,  Variants, cxPropertiesStore,

  u_Storage,

  dm_Onega_DB_data,

  f_Custom,


  u_const_str,

  u_Func_arrays,
  u_func,
  u_db,

  u_LinkEndType_const,

  u_const_db,

//  u_const

  u_Link_const,

  dm_LinkEndType,

  cxGridCustomView, cxGrid, Menus, DB, ADODB, ActnList, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, StdCtrls, ExtCtrls, ToolWin,
  ComCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxTextEdit;


type
  Tdlg_LinkEndType_Mode = class(Tfrm_Custom)
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    FormPlacement1: TFormPlacement;
    qry_Modes: TADOQuery;
    ds_Mode: TDataSource;
    cxGrid1: TcxGrid;
    cxGrid1DBBandedTableView1: TcxGridDBBandedTableView;
    col__ID: TcxGridDBBandedColumn;
    col__Mode: TcxGridDBBandedColumn;
    col_Bitrate_Mbps: TcxGridDBBandedColumn;
    col__bandwidth: TcxGridDBBandedColumn;
    col__Modulation_type: TcxGridDBBandedColumn;
    col__traffic_type: TcxGridDBBandedColumn;
    col__radiation_class: TcxGridDBBandedColumn;
    col__Power_max: TcxGridDBBandedColumn;
    col__power_noise: TcxGridDBBandedColumn;
    col__bandwidth_tx_3: TcxGridDBBandedColumn;
    col__bandwidth_tx_30: TcxGridDBBandedColumn;
    col__bandwidth_tx_a: TcxGridDBBandedColumn;
    col__threshold_BER_3: TcxGridDBBandedColumn;
    col__threshold_BER_6: TcxGridDBBandedColumn;
    col__bandwidth_rx_3: TcxGridDBBandedColumn;
    col__bandwidth_rx_30: TcxGridDBBandedColumn;
    col__bandwidth_rx_a: TcxGridDBBandedColumn;
    col__ch_spacing: TcxGridDBBandedColumn;
    col__signature_width: TcxGridDBBandedColumn;
    col__signature_height: TcxGridDBBandedColumn;
    cxGrid1Level1: TcxGridLevel;
    col_GOST_modulation_level_count: TcxGridDBBandedColumn;
    col_GOST_modulation: TcxGridDBBandedColumn;
    PopupMenu1: TPopupMenu;
    MenuItem2: TMenuItem;
    act_Grid_Restore_default: TAction;
    Panel1: TPanel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    procedure FormDestroy(Sender: TObject);
    procedure act_Grid_Restore_defaultExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FRegPath: string;

//    Fframe_LinkEndType_Mode: Tframe_LinkEndType_Mode;
    function GetMode2(var aRec: TdmLinkEndTypeModeInfoRec): Integer;

    procedure View(aID: integer; aMode_ID: integer = 0);

  public
    class function Dlg_GetMode1(aLinkEndTypeID, aMode_ID: integer; var aRec:
        TdmLinkEndTypeModeInfoRec): Integer;


  end;


//=============================================================
// implementation
//=============================================================

implementation {$R *.DFM}



procedure Tdlg_LinkEndType_Mode.act_Grid_Restore_defaultExecute(Sender:
    TObject);
begin
  // ---------------------------------------------------------------
  if Sender=act_Grid_Restore_default then
  // ---------------------------------------------------------------
  begin
    g_Storage.RestoreFromRegistry(cxGrid1DBBandedTableView1, className+'_default')

   // dxDBTree.LoadFromRegistry (FRegPath +  dxDBTree.Name + '_default');
  end

end;

//-------------------------------------------------------------
class function Tdlg_LinkEndType_Mode.Dlg_GetMode1(aLinkEndTypeID, aMode_ID: integer; var
    aRec: TdmLinkEndTypeModeInfoRec): Integer;
//-------------------------------------------------------------
begin
  with Tdlg_LinkEndType_Mode.Create(Application) do
  try
    View (aLinkEndTypeID, aMode_ID);

    if ShowModal=mrOk then
      Result:=GetMode2(aRec)
    else
      Result:=-1;

  finally
    Free;
  end;
end;

//--------------------------------------------------------------
procedure Tdlg_LinkEndType_Mode.FormCreate(Sender: TObject);
//--------------------------------------------------------------
begin
  inherited;

  Caption:='����� ������';

  cxGrid1.Align:=alClient;

  act_GRid_Restore_default.Caption:=STR_GRID_Restore_default;  // '������������ �������';


  FRegPath := g_Storage.GetPathByClass(ClassName);

//  FRegPath :=FRegPath+ '2';

 // cxGrid1DBTableView11.restoreFromRegistry (FRegPath+ cxGrid1DBTableView11.Name);
 // cxGrid2DBBandedTableView11.restoreFromRegistry (FRegPath+ cxGrid2DBBandedTableView11.Name);

  g_Storage.StoreToRegistry(cxGrid1DBBandedTableView1, className+'_default');

  g_Storage.RestoreFromRegistry(cxGrid1DBBandedTableView1, className);
end;


procedure Tdlg_LinkEndType_Mode.FormDestroy(Sender: TObject);
begin
  g_Storage.StoreToRegistry(cxGrid1DBBandedTableView1, className);
end;


//-------------------------------------------------------------
function Tdlg_LinkEndType_Mode.GetMode2(var aRec: TdmLinkEndTypeModeInfoRec): Integer;
//-------------------------------------------------------------
begin
  Result:=0;

  with qry_Modes do
    if not IsEmpty then
  begin
    Result:=FieldByName(FLD_ID).AsInteger;
    // ---------------------------------------------------------------

    aRec.ID               :=FieldByName(FLD_ID).AsInteger;

    aRec.Mode             :=FieldByName(FLD_Mode).AsInteger;
    aRec.Bitrate_Mbps_     :=FieldByName(FLD_Bitrate_Mbps).AsFloat;

    aRec.Modulation_type  :=FieldByName(FLD_Modulation_type).AsString;

//     aRec.Modulation_Count :=qry_Modes.FieldByName(FLD_Modulation_type).AsString;

    aRec.SIGNATURE_HEIGHT    :=FieldByName(FLD_SIGNATURE_HEIGHT).AsFloat;
    aRec.SIGNATURE_WIDTH     :=FieldByName(FLD_SIGNATURE_WIDTH).AsFloat;
    aRec.THRESHOLD_BER_3     :=FieldByName(FLD_THRESHOLD_BER_3).AsFloat;
    aRec.THRESHOLD_BER_6     :=FieldByName(FLD_THRESHOLD_BER_6).AsFloat;

    aRec.Power_max_dBm       :=FieldByName(FLD_Power_max).AsFloat;

    // ������ �������� ������� MHz  
    aRec.bandwidth_MHz   :=FieldByName(FLD_bandwidth).AsFloat;


    aRec.Modulation_level_count :=  FieldByName(FLD_Modulation_LEVEL_COUNT).AsInteger;
    aRec.GOST_Modulation        :=  FieldByName(FLD_GOST_53363_modulation).AsString;

    // -------------------------
//    aRec.Modulation_Count := AsInteger(tab_Lib_Modulations.Lookup(FLD_));

 //   if  then

  end;
end;

//--------------------------------------------------------------
procedure Tdlg_LinkEndType_Mode.View(aID: integer; aMode_ID: integer = 0);
//--------------------------------------------------------------
begin
// zz FID:=aID;

  dmOnega_DB_data.OpenQuery (qry_Modes,
               'SELECT * FROM ' + VIEW_LINKENDTYPE_MODE +
               ' WHERE LinkEndType_id=:LinkEndType_id',
               [FLD_LINKENDType_ID, aID]);

//  qry_Modes.Locate(FLD_MODE,aMode_ID,[]);
  qry_Modes.Locate(FLD_ID,aMode_ID,[]);


  db_SetFieldCaptions(qry_Modes,
     [
     FLD_Mode,             '�����',

     FLD_BITRATE_MBPS,     CAPTION_BITRATE_Mbps,
     FLD_Modulation_type,  CAPTION_MODULATION_TYPE,

      //����
      FLD_modulation_level_count, CAPTION_modulation_level_count,
      FLD_GOST_53363_modulation,  CAPTION_GOST_53363_modulation,

      FLD_threshold_BER_3,  CAPTION_THRESHOLD_BER_3,
      FLD_threshold_BER_6,  CAPTION_THRESHOLD_BER_6,

      FLD_traffic_type,     '��� �������',
      FLD_radiation_class,  '����� ���������',

      FLD_Power_max,        CAPTION_POWER_MAX_dBm,
      FLD_bandwidth,        CAPTION_BANDWIDTH,

      FLD_ch_spacing,       CAPTION_CH_SPACING,
      FLD_power_noise,      CAPTION_POWER_NOISE,

      FLD_signature_width,  CAPTION_SIGNATURE_WIDTH,
      FLD_signature_height, CAPTION_SIGNATURE_HEIGHT
    ]);


end;



end.