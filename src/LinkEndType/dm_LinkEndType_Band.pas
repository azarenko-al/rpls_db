unit dm_LinkEndType_Band;

interface

uses
  SysUtils, Classes, DB, ADODB,

  dm_Main,

  u_const_db

  ;

type
  TdmLinkEndType_Band = class(TDataModule)
    qry_Bands: TADOQuery;
    ds_Bands: TDataSource;
    procedure qry_BandsBeforeEdit(DataSet: TDataSet);
    procedure qry_BandsBeforePost(DataSet: TDataSet);
    procedure qry_BandsNewRecord(DataSet: TDataSet);
  private

  public
    procedure Open(aID: integer);

  end;


var
  dmLinkEndType_Band: TdmLinkEndType_Band;

implementation  {$R *.DFM}


class procedure TdmLinkEndType_Band.Init;
begin
  if not Assigned(dmLinkEndType_Band) then
    dmLinkEndType_Band := TdmLinkEndType_Band.Create(Application);
end;



procedure TdmLinkEndType_Band.Open(aID: integer);
begin
  dmOnega_DB_data.OpenQuery (aADOQuery,
               'SELECT * FROM ' + TBL_LINKENDTYPE_BAND +
               ' WHERE LinkEndType_id=:LinkEndType_id',
               [FLD_LINKENDType_ID, aID]);
end;


procedure TdmLinkEndType_Band.qry_BandsBeforeEdit(DataSet: TDataSet);
begin
  with DataSet do
  begin
    FChCount     := FieldByName(FLD_CHANNEL_COUNT).AsInteger;
    FTxRxShift   := FieldByName(FLD_TXRX_SHIFT).AsFloat;
    FChWidth     := FieldByName(FLD_CHANNEL_WIDTH).AsFloat;
    FFreqMin_low := FieldByName(FLD_FREQ_MIN_LOW).AsFloat;
    FFreqMax_low := FieldByName(FLD_FREQ_MAX_LOW).AsFloat;
    FFreqMin_high:= FieldByName(FLD_FREQ_MIN_HIGH).AsFloat;
    FFreqMax_high:= FieldByName(FLD_FREQ_MAX_HIGH).AsFloat;
  end;

end;

procedure TdmLinkEndType_Band.qry_BandsBeforePost(DataSet: TDataSet);
begin
var dChannelWidth, dTxRxShift : double;
    dFreqMin_low, dFreqMax_low, dFreqMin_high, dFreqMax_high : double;
    iChannelCount: integer;
begin
  inherited;

  with DataSet do
  begin
    dTxRxShift    := FieldByName(FLD_TXRX_SHIFT).AsFloat;
    iChannelCount := FieldByName(FLD_CHANNEL_COUNT).AsInteger;
    dChannelWidth := FieldByName(FLD_CHANNEL_WIDTH).AsFloat;
    dFreqMin_low  := FieldByName(FLD_FREQ_MIN_LOW).AsFloat;
    dFreqMax_low  := FieldByName(FLD_FREQ_MAX_LOW).AsFloat;
    dFreqMin_high := FieldByName(FLD_FREQ_MIN_HIGH).AsFloat;
    dFreqMax_high := FieldByName(FLD_FREQ_MAX_HIGH).AsFloat;

    // ���������, ���� ��������� ���� �� ���������
    if (dFreqMin_low > 0) and (dFreqMax_low > 0) and (dTxRxShift > 0) and
       (dFreqMin_high = 0) and (dFreqMax_high = 0) then
    begin
      dFreqMin_high:= dFreqMin_low + dTxRxShift;
      dFreqMax_high:= dFreqMax_low + dTxRxShift;

{
      FieldValues[FLD_FREQ_MIN_HIGH]:= dFreqMin_low + dTxRxShift;
      FieldValues[FLD_FREQ_MAX_HIGH]:= dFreqMax_low + dTxRxShift;
}
    end
    else

    if (dFreqMin_high > 0) and (dFreqMax_high > 0) and (dTxRxShift > 0) and
       (dFreqMin_low = 0) and (dFreqMax_low = 0) then
    begin
      dFreqMin_low:= dFreqMin_high - dTxRxShift;
      dFreqMax_low:= dFreqMax_high - dTxRxShift;

{
      FieldValues[FLD_FREQ_MIN_LOW]:= dFreqMin_high - dTxRxShift;
      FieldValues[FLD_FREQ_MAX_LOW]:= dFreqMax_high - dTxRxShift;
}
    end;

    if (dChannelWidth = 0) and (iChannelCount > 0) and (dFreqMin_low > 0) and (dFreqMax_low > 0) then
      dChannelWidth:= (dFreqMax_low-dFreqMin_low)/(iChannelCount-1)
//      FieldValues[FLD_CHANNEL_WIDTH]:= (dFreqMax_low-dFreqMin_low)/(iChannelCount-1)
    else

    if (dFreqMax_low = 0) and (iChannelCount > 0) and (dFreqMin_low > 0) and (dChannelWidth > 0) then
      dFreqMax_low:=  dFreqMin_low+((iChannelCount-1)*dChannelWidth)

//      FieldValues[FLD_FREQ_MAX_LOW]:=  dFreqMin_low+((iChannelCount-1)*dChannelWidth)
    else

    if (dFreqMin_low = 0) and (iChannelCount > 0) and (dChannelWidth > 0) and (dFreqMax_low > 0) then
      FieldValues[FLD_FREQ_MIN_LOW]:= dFreqMax_low-((iChannelCount-1)*dChannelWidth)
    else
    if (iChannelCount = 0) and (dChannelWidth > 0) and (dFreqMin_low > 0) and (dFreqMax_low > 0) then
        iChannelCount:= (Floor((dFreqMax_low-dFreqMin_low)/dChannelWidth))+1 ;
//      FieldValues[FLD_CHANNEL_COUNT]:= (Floor((dFreqMax_low-dFreqMin_low)/dChannelWidth))+1 ;

    // ���������, ���� ��������� ���� ����������
    if (FTxRxShift <> dTxRxShift) and (dFreqMin_low > 0) and (dFreqMax_low > 0) then
    begin
      dFreqMin_high:= dFreqMin_low + dTxRxShift;
      dFreqMax_high:= dFreqMax_low + dTxRxShift;

{
      FieldValues[FLD_FREQ_MIN_HIGH]:= dFreqMin_low + dTxRxShift;
      FieldValues[FLD_FREQ_MAX_HIGH]:= dFreqMax_low + dTxRxShift;
}
    end
    else

    if ((FFreqMin_low <> dFreqMin_low) and (dFreqMax_low > 0) and (dChannelWidth > 0)) or
       ((FFreqMax_low <> dFreqMax_low) and (dFreqMin_low > 0) and (dChannelWidth > 0)) then
    begin
      iChannelCount:= (Floor((dFreqMax_low-dFreqMin_low)/dChannelWidth))+1 ;

//      FieldValues[FLD_CHANNEL_COUNT]:= (Floor((dFreqMax_low-dFreqMin_low)/dChannelWidth))+1 ;

      if dTxRxShift > 0 then
      begin
        dFreqMin_high:= dFreqMin_low + dTxRxShift;
        dFreqMax_high:= dFreqMax_low + dTxRxShift;

 //       FieldValues[FLD_FREQ_MIN_HIGH]:= dFreqMin_low + dTxRxShift;
  //      FieldValues[FLD_FREQ_MAX_HIGH]:= dFreqMax_low + dTxRxShift;

      end;
    end
    else

    if (FChWidth <> dChannelWidth) and (dChannelWidth > 0) and (dFreqMin_low > 0) and (dFreqMax_low > 0) then
    begin
      iChannelCount:= (Floor((dFreqMax_low-dFreqMin_low)/dChannelWidth))+1 ;
//      FieldValues[FLD_CHANNEL_COUNT]:= (Floor((dFreqMax_low-dFreqMin_low)/dChannelWidth))+1 ;
    end
    else

    if (FChCount <> iChannelCount) and (iChannelCount <> 0) and (dFreqMin_low > 0) and (dChannelWidth > 0) then
    begin
      dFreqMax_low:=  dFreqMin_low+((iChannelCount-1)*dChannelWidth);
      dFreqMin_high:= dFreqMin_low + dTxRxShift;
      dFreqMax_high:= dFreqMin_low+((iChannelCount-1)*dChannelWidth) + dTxRxShift;

{      FieldValues[FLD_FREQ_MAX_LOW]:=  dFreqMin_low+((iChannelCount-1)*dChannelWidth);
      FieldValues[FLD_FREQ_MIN_HIGH]:= dFreqMin_low + dTxRxShift;
      FieldValues[FLD_FREQ_MAX_HIGH]:= dFreqMin_low+((iChannelCount-1)*dChannelWidth) + dTxRxShift;

}    end;


    FieldValues[FLD_CHANNEL_COUNT]:=iChannelCount;
    FieldValues[FLD_FREQ_MIN_LOW] :=dFreqMin_low;
    FieldValues[FLD_FREQ_MAX_LOW] :=dFreqMax_low;
    FieldValues[FLD_FREQ_MIN_HIGH]:=dFreqMin_High;
    FieldValues[FLD_FREQ_MAX_HIGH]:=dFreqMax_High;


  end;    // with

end;


procedure TdmLinkEndType_Band.qry_BandsNewRecord(DataSet: TDataSet);
begin
  DataSet[FLD_LINKENDType_ID]:=FID;

end;



end.
