unit d_LinkEndType_Field_update;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,

  cxGridBandedTableView, cxGridDBBandedTableView, cxClasses, cxControls,
  Math, rxPlacemnt,  Variants, cxPropertiesStore,

  u_Storage,

  dm_Onega_DB_data,

  f_Custom,


  u_const_str,

  u_Func_arrays,
  u_func,
  u_db,

  u_LinkEndType_const,

  u_const_db,

//  u_const

  u_Link_const,

  dm_LinkEndType,

  cxGridCustomView, cxGrid, Menus, DB, ADODB, ActnList, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, StdCtrls, ExtCtrls, ToolWin,
  ComCtrls, dxmdaset;


type
  Tdlg_LinkEndType_Field_update = class(Tfrm_Custom)
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    FormPlacement1: TFormPlacement;
    act_Grid_Restore_default: TAction;
    dxMemData1: TdxMemData;
    DataSource1: TDataSource;
    dxMemData1name: TStringField;
    dxMemData1caption: TStringField;
    cxGrid1: TcxGrid;
    cxGrid1DBBandedTableView1: TcxGridDBBandedTableView;
    col__ID: TcxGridDBBandedColumn;
    col__Mode: TcxGridDBBandedColumn;
    col_Bitrate_Mbps: TcxGridDBBandedColumn;
    col__bandwidth: TcxGridDBBandedColumn;
    col__Modulation_type: TcxGridDBBandedColumn;
    col__traffic_type: TcxGridDBBandedColumn;
    col__radiation_class: TcxGridDBBandedColumn;
    col__Power_max: TcxGridDBBandedColumn;
    col__power_noise: TcxGridDBBandedColumn;
    col__bandwidth_tx_3: TcxGridDBBandedColumn;
    col__bandwidth_tx_30: TcxGridDBBandedColumn;
    col__bandwidth_tx_a: TcxGridDBBandedColumn;
    col__threshold_BER_3: TcxGridDBBandedColumn;
    col__threshold_BER_6: TcxGridDBBandedColumn;
    col__bandwidth_rx_3: TcxGridDBBandedColumn;
    col__bandwidth_rx_30: TcxGridDBBandedColumn;
    col__bandwidth_rx_a: TcxGridDBBandedColumn;
    col__ch_spacing: TcxGridDBBandedColumn;
    col__signature_width: TcxGridDBBandedColumn;
    col__signature_height: TcxGridDBBandedColumn;
    col_GOST_modulation_level_count: TcxGridDBBandedColumn;
    col_GOST_modulation: TcxGridDBBandedColumn;
    cxGrid1Level1: TcxGridLevel;
    Panel1: TPanel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    procedure FormDestroy(Sender: TObject);
    procedure act_Grid_Restore_defaultExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    procedure View(aID: integer; aMode_ID: integer = 0);

  public
    class procedure ExecDlg(aLinkEndTypeID: Integer);


  end;


//=============================================================
// implementation
//=============================================================

implementation {$R *.DFM}



procedure Tdlg_LinkEndType_Field_update.act_Grid_Restore_defaultExecute(Sender:
    TObject);
begin


end;

//-------------------------------------------------------------
class procedure Tdlg_LinkEndType_Field_update.ExecDlg(
    aLinkEndTypeID: Integer);
//-------------------------------------------------------------
begin
  with Tdlg_LinkEndType_Mode.Create(Application) do
  try
  //  View (aLinkEndTypeID, aMode_ID);

    ShowModal;

  finally
    Free;
  end;
end;

//--------------------------------------------------------------
procedure Tdlg_LinkEndType_Field_update.FormCreate(Sender: TObject);
//--------------------------------------------------------------
begin
  inherited;

  Caption:='����� ������';



end;





end.