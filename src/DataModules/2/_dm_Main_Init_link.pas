unit _dm_Main_Init_link;

interface
{$I ver.inc}


uses Forms, Classes, GSDock, Dialogs,

  

  dm_Main,
//  dm_Main_res,
  dm_Custom,

  u_const,

  u_reg,

  dm_Act_Antenna,
  dm_Act_AntType,
  dm_act_CalcModel,
  dm_Act_Cell_Layer,
  dm_Act_CellTemplate,
  dm_act_Clutter,
  dm_act_ColorSchema,
  dm_Act_Combiner,
  dm_act_Explorer,
  dm_Act_Filter,

  dm_Act_Forms,
  dm_act_GeoRegion,


  dm_act_Map_Engine,
  dm_Act_MapDesktop,
  dm_Act_MapFile,
  dm_Act_Profile,
  dm_Act_Project,
  dm_Act_Property,
  dm_act_Rel_Engine,
  dm_Act_RelFile,
  dm_Act_Report,
  dm_Act_SiteTemplate,
  dm_Act_Terminal,
  dm_act_Trx,


  {$IFDEF Link}
  dm_act_Link,
  dm_Act_LinkEnd,
  dm_act_LinkEndType,
  dm_Act_LinkFreqPlan,
  dm_act_LinkLine,
  dm_act_LinkLine_Link,
  dm_Act_LinkNet,
  dm_act_LinkNet_Link_Ref,
  dm_act_LinkNet_LinkLine_Ref,
  dm_act_LinkType,
  {$ENDIF Link}


  {$IFDEF Pmp}
  dm_Act_Pmp_Sector,
  dm_Act_Pmp_Site,
  dm_Act_PmpTerminal,
  dm_Act_PMP_CalcRegion_Site,

  {$ENDIF Pmp}

  {$IFDEF GSM}
  dm_Act_BSC,
  dm_Act_CalcRegion_Site,
  dm_Act_Cell,
  dm_Act_Drive,
  dm_Act_FreqPlan,
  dm_Act_MSC,
  dm_Act_Site,
  dm_act_TransferTech,
  dm_Act_Traffic,

  {$ENDIF}

  {$IFDEF 3G}

  dm_3G_Act_CalcRegion,
  dm_3G_Act_CalcRegion_Site,
  dm_3G_Act_Cell,
  dm_3G_Act_Traffic_Type,
  dm_3G_Act_ConfigTemplate,
  dm_3G_Act_TrafficModel,
  dm_3G_Act_NetConfig,
  dm_3G_Act_Site,

  {$ENDIF}

  dm_UserAuth,
  dm_Objects,
  dm_Options,


  f_Main_MDI
  ;


type
  TdmMain_Init_link = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
  end;

var
  dmMain_Init_link: TdmMain_Init_link;


//====================================================================
implementation {$R *.dfm}
//====================================================================

// -------------------------------------------------------------------
procedure TdmMain_Init_link.DataModuleCreate(Sender: TObject);
// -------------------------------------------------------------------
begin
//  dm_Custom.init_UpdateADOConnections;

  gl_Reg:=TRegStore_Create(REGISTRY_ROOT);

  TdmMain.Init;
  IShellFactory_Init (dmMain);

  if not dmMain.OpenDlg() then
    Exit;

  dmUserAuth.Load;

  dmObjects.Open;

  dmOptions.SetADOConnection (dmMain.ADOConnection);
  dmOptions.LoadDefault;// (dmMain.ADOConnection);
  dmOptions.LoadFromReg;
//  dmOptions.SyncXrefFieldsWithOptions (otSite, dmMain.ADOConnection);


 // gl_Reg.
//  InitRegistry(REGISTRY_ROOT);


//  if not dmMain.LoginDlg(True) then
 //   Exit;

  InitGSDockManager;

  Application.CreateForm(Tfrm_Main_MDI, frm_Main_MDI);
  Application.ProcessMessages;

  dmAct_Project.LoadLastProject;


end;


end.






