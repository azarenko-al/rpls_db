unit fr_Login;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, StdCtrls,     ExtCtrls, Db, ADODB  ,

  u_dlg,
  u_func,
  //u_func_sql,
  u_db

  ;

type
  Tframe_Login = class(TForm)
    ActionList1: TActionList;
    act_Check: TAction;
    act_Database_Refresh: TAction;
    ADOConnection1: TADOConnection;
    gb_ConnectToSQL: TGroupBox;
    dxInspector2: TdxInspector;
    row_login: TdxInspectorTextRow;
    row_Password: TdxInspectorTextRow;
    row_SQL_auth: TdxInspectorTextPickRow;
    row_Server: TdxInspectorTextButtonRow;
    row_Database: TdxInspectorTextPickRow;
    dxInspector2Row7: TdxInspectorTextRow;
    Panel1: TPanel;
    Button1: TButton;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure act_CheckExecute(Sender: TObject);
    procedure dxInspector2ChangeNode(Sender: TObject; OldNode, Node:
        TdxInspectorNode);
    procedure row_DatabaseCloseUp(Sender: TObject; var Value: Variant; var Accept:
        Boolean);
    procedure row_DatabaseEditButtonClick(Sender: TObject);
    procedure row_DatabaseValidate(Sender: TObject; var ErrorText: string; var
        Accept: Boolean);
    procedure row_PasswordChange(Sender: TObject);
    procedure row_ServerButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure row_ServerChange(Sender: TObject);
  private
    FRegPath: string;
    FLoginRec: TdbLoginRec;

    procedure SaveEditsToRec;
    procedure SaveToReg;

  public
    IsAdmin: boolean;

    procedure LoadFromReg (aRegPath: string);

    function GetLoginRec (): TdbLoginRec;

    class function CreateChildForm (aDest: TWinControl) : Tframe_Login;
  end;


implementation

{$R *.DFM}


procedure Tframe_Login.FormDestroy(Sender: TObject);
begin
  SaveToReg();
  inherited;
end;

// -------------------------------------------------------------------
class function Tframe_Login.CreateChildForm ( aDest: TWinControl) : Tframe_Login;
// -------------------------------------------------------------------
begin
  Result:=Tframe_Login.Create(nil);
  CopyControls (Result, aDest);

  aDest.Height:=170;
end;


procedure Tframe_Login.FormCreate(Sender: TObject);
begin
end;


//--------------------------------------------------------------------
function Tframe_Login.GetLoginRec (): TdbLoginRec;
//--------------------------------------------------------------------
begin
  SaveEditsToRec();

  Result:=FLoginRec;
end;


// ---------------------------------------------------------------
procedure Tframe_Login.LoadFromReg(aRegPath: string);
// ---------------------------------------------------------------
begin
  FRegPath:=aRegPath;

  db_LoadConnectRecFromReg (FRegPath, FLoginRec);

  with FLoginRec do
  begin
    //default values
    if Server=''    then Server:='(local)';
    if User=''      then User:='';
    if Database=''  then Database:='';


    row_Server.Text    := Server;
    row_Login.Text     := User;
    row_Password.Text  := Password;
    row_Database.Text  := Database;
    row_SQL_auth.Text  := row_SQL_auth.Items [IIF(IsUseWinAuth,1,0)];

  end;
end;

// ---------------------------------------------------------------
procedure Tframe_Login.SaveToReg;
// ---------------------------------------------------------------
begin
  if FRegPath='' then
    Exit;

  SaveEditsToRec();

  db_SaveConnectRecToReg (FRegPath, FLoginRec);

end;



//--------------------------------------------------------------------
procedure Tframe_Login.SaveEditsToRec;
//--------------------------------------------------------------------
begin
  with FLoginRec do
  begin
    Server      :=row_Server.Text;
    User        :=row_Login.Text;
    Password    :=row_Password.Text;
    Database    :=row_Database.Text;

    IsUseWinAuth:=row_SQL_auth.Items.IndexOf (row_SQL_auth.Text) = 1;
  end;
end;


// ---------------------------------------------------------------
procedure Tframe_Login.act_CheckExecute(Sender: TObject);
// ---------------------------------------------------------------
begin
  // ---------------------------------------------------------------
  if Sender=act_Database_Refresh then
  // ---------------------------------------------------------------
  begin
    SaveEditsToRec();

    db_GetDatabaseList (FLoginRec, row_Database.Items);

    if (row_Database.Items.IndexOf(row_Database.Text)=-1) then
      row_Database.Text:= '';

    if (row_Database.Text= '') and (row_Database.Items.Count>0) then
      row_Database.Text:= row_Database.Items[0];
  end;

  // ---------------------------------------------------------------
  if Sender=act_Check then
  // ---------------------------------------------------------------
  begin
    SaveEditsToRec();

    if IsAdmin then
      if db_ADO_CheckAdminRights (FLoginRec) then
        ShowMessage('���������� � �� �����������. ');

    if not IsAdmin then
      if db_ADO_CheckConnection (FLoginRec) then
        ErrorDlg('���������� � �� �� �����������! ');

{    if db_ADO_CheckConnection (FLoginRec) then
      if dmMain.OpenLoginRec (FLoginRec) then
      begin
        db_SaveConnectRecToReg (REGISTRY_LOGIN, FLoginRec);

        ModalResult:=mrOK;
      end;
}
  end;


end;

procedure Tframe_Login.dxInspector2ChangeNode(Sender: TObject; OldNode, Node:
    TdxInspectorNode);
begin
  if Node=row_Database.Node then
    if row_Database.Items.Count=0 then
       act_Database_Refresh.Execute;

end;

procedure Tframe_Login.row_DatabaseCloseUp(Sender: TObject; var Value: Variant;
    var Accept: Boolean);
begin
 // ShowMessage('row_DatabaseCloseUp(Sender: TObject; var Value: Variant;  var Accept: Boolean);');
end;


procedure Tframe_Login.row_DatabaseEditButtonClick(Sender: TObject);
begin
//  Label1.Caption:='11111111111';

 // row_Database.Items.Add('11111111');

{    SaveEditsToRec();

    db_GetDatabaseList (FLoginRec, row_Database.Items);
}

 // if row_Database.Items.Count=0 then
//  act_Database_Refresh.Execute;

//  row_Database.
//  ShowMessage('row_DatabaseEditButtonClick');
end;

procedure Tframe_Login.row_DatabaseValidate(Sender: TObject; var ErrorText:
    string; var Accept: Boolean);
begin
 // ShowMessage('row_DatabaseValidate');
end;

procedure Tframe_Login.row_PasswordChange(Sender: TObject);
begin

end;


//----------------------------------------------------------------------
procedure Tframe_Login.row_ServerButtonClick(Sender: TObject; AbsoluteIndex: Integer);
//----------------------------------------------------------------------
var sStr : string;
begin
  sStr := db_ShowServerDialog(Self.Handle);
  if sStr='' then
    Exit;

  row_Server.Text := sStr;

 // act_Database_Refresh.Execute;
end;


procedure Tframe_Login.row_ServerChange(Sender: TObject);
begin
  row_Database.Items.Clear;
 //  act_Database_Refresh.Execute;
end;



end.

{



//----------------------------------------------------------------------
procedure Tdlg_Login.ed_ServerButtonClick(Sender: TObject; AbsoluteIndex: Integer);
//----------------------------------------------------------------------
var  sStr : string;
begin
  sStr := ShowServerDialog(Self.Handle);
  if sStr='' then
    Exit;

  ed_Server.Text := sStr;

  act_Database_Refresh.Execute;   
end;




//----------------------------------------------------------------------
procedure Tdlg_Login.act_OkExecute(Sender: TObject);
//----------------------------------------------------------------------
begin
  SaveEditsToRec();

  if db_ADO_CheckConnection (FLoginRec) then
    if dmMain.OpenLoginRec (FLoginRec) then
    begin
      db_SaveConnectRecToReg (REGISTRY_LOGIN, FLoginRec);

      ModalResult:=mrOK;
    end;
end;



//-----------------------------------------------------------------
procedure Tdlg_Login.FormCreate(Sender: TObject);
//-----------------------------------------------------------------
begin
  inherited;

  db_LoadConnectRecFromReg (REGISTRY_LOGIN, FLoginRec);

  with FLoginRec do
  begin
    //default values
    if Server=''    then Server:='(local)';
    if User=''      then User:='sa';
    if Database=''  then Database:='onega';


    ed_Server.Text    := Server;
    ed_User.Text      := User;
    ed_Password.Text  := Password;
    ed_Database.Text  := Database;
    cb_Auth.ItemIndex := IIF(IsUseWinAuth,1,0);
  end;
//   LoadFromReg();

  cb_ShowOnStart.Checked:=reg_GetBoolValue (REGISTRY_LOGIN, 'IsShowDialog', True);
end;


}


{
//----------------------------------------------------------------------
procedure Tframe_Login.row_ServerButtonClick(Sender: TObject; AbsoluteIndex:  Integer);
//----------------------------------------------------------------------
var  sStr : string;
begin
  sStr := ShowServerDialog(Self.Handle);
  if sStr='' then
    Exit;

  ed_Server.Text := sStr;

  act_Database_Refresh.Execute;
end;}