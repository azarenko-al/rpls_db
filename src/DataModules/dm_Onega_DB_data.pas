unit dm_Onega_db_data;

interface

uses
  SysUtils, Classes, ADODB, DB, Forms, Variants,

  u_debug,
  u_log,

  u_func,
  u_db,
  u_const_db

  ;

type
  TObject_GetNewName_Params = record
                                ObjName     : string;
                                Name        : string;
                                Project_ID  : Integer;
                                Property_ID : Integer;
                                Parent_ID   : Integer;
                              end;


type
  TdmOnega_DB_data = class(TDataModule)
    ADOQuery1: TADOQuery;
    ADOStoredProc1: TADOStoredProc;
    ADOCommand1: TADOCommand;
//    procedure DataModuleCreate(Sender: TObject);
  private
    FADOConnectionRef :  TADOConnection;

//    function LinkEnd_update_antenna_type(aID,aAntennaType_ID: Integer): integer;
    procedure Log(aMsg: string);
    procedure SetADOConnection(const Value: TADOConnection);
    procedure sp_LinkLine_Add_link(aLinkLineID, aLinkID: Integer);
//    procedure sp_LinkLine_remove_link(aLinkLineID: integer);
  public
    function OpenStoredProc(aADOStoredProc: TADOStoredProc; aProcName: string;  aParams: array of Variant): integer; overload;
    function ExecStoredProc(aADOStoredProc: TADOStoredProc; aProcName: string; aParams: array of Variant): integer;  overload;


    function ExecCommand(aSQL: string; aParamList: TdbParamList = nil): boolean;

    function ExecStoredProc(aProcName: string; aParams: array of TdbParamRec):    Integer; overload;

//    function ExecStoredProc11(aADOStoredProc: TAdoStoredProc; aProcName: string;
//        aParams: array of TdbParamRec): Integer;

  public
    class procedure Init;

    procedure Activity_Log(aName: string; aProject_ID: Integer);

    function AntennaType_Copy(aID: Integer; aName: string): integer;


    function CalcModel_Copy(aID: Integer; aName: string): integer;
//    procedure CalcRegion_Check(aID: Integer);


    function ClutterModel_restore(aiD: Integer): Integer;

    function ColorSchema_Copy(aID: Integer; aName: string): Integer;

    // -------------------------
    // Library
    // -------------------------



    function Explorer_Select_Object(aAdoStoredProc: TAdoStoredProc;aObjName:
        string; aProject_ID: Integer = 0): Integer;

    // -------------------------
    // CalcMap
    // -------------------------

     function CalcMap_Del(aID: Integer): Integer;

    // -------------------------
    // GroupAssign
    // -------------------------

    function GroupAssign_Select_subitems(aADOStoredProc: TAdoStoredProc; aID:
        Integer): Integer;


    // -------------------------
    // Folder_
    // -------------------------

   function Folder_Has_Children(aObjectName: string; aID: integer; aProjectID:
       integer = 0): Boolean;

   function Folder_GetFullPath(aID: integer): string;
//    function Folder_GetParents(aADOStoredProc: TADOStoredProc; aID: Integer):
//        Integer;

    // -------------------------
    // LinkEndType
    // -------------------------
    function LinkEndType_Copy(aID: Integer; aName: string): Integer;
    function LinkEndType_Band_Channel_Select(aADOStoredProc: TADOStoredProc; aID:
        Integer): Integer;

    function LinkEndType_update_vendor(aID, aVendor_ID, aVendor_equipment_id:
        Integer): Integer;

    // -------------------------
    // Linkend_Antenna
    // -------------------------

    function Linkend_Antenna_Copy(aID: Integer; aName: string): integer;
    function Linkend_Antenna_Del(aID: Integer): Integer;

    function Linkend_Antenna_GetNewName(aObjName: string; aOwnerID: integer):
        string;
//    function Linkend_Antenna_SelectByOwner(aADOStoredProc: TAdoStoredProc;
 //       aObjName: string; aOwnerID: integer): Integer;

    function Linkend_Antenna_Update_AntennaType(aID,aAntennaType_ID: Integer):
        integer;

//    function Linkend_Antenna_update_Azimuth11_1111111(aObjName: string; aID:
//        Integer; aAzimuth: double): Integer;

    // -------------------------
    // Linkend
    // -------------------------

    function LinkEnd_Copy(aID: Integer; aName: string): integer;
    function LinkEnd_del(aID: Integer): Integer;
    function LinkEnd_Dependency(aADOStoredProc: TADOStoredProc; aID: Integer):
        Integer;
    function LinkEnd_GetSubItems(aADOStoredProc: TADOStoredProc; aID: Integer):
        Integer;
    function LinkEnd_Get_Next_LinkEnd_ID(aID: integer): integer;
    function LinkEnd_UpdateNext(aID: Integer): Integer;
//    function LinkEnd_update_antenna_type(aLinkEnd_ID, aAntennaType_ID: Integer):
 //       Integer;

    // -------------------------
    // LinkFreqPlan
    // -------------------------
    function LinkFreqPlan_Init(aID: Integer): Integer;
    function LinkFreqPlan_Linkend1(aADOStoredProc: TADOStoredProc; aID: Integer):
        Integer;
    function LinkFreqPlan_Restore_Fixed(aID: Integer): Integer;
    function LinkFreqPlan_Select_Linkend_NEIGHBORS(aADOStoredProc: TADOStoredProc;
        aLinkFreqPlanID, aLinkEndID: Integer): Integer;
    function LinkFreqPlan_Select_NB_Linkends(aADOStoredProc: TADOStoredProc; aID:
        Integer): Integer;
    function LinkFreqPlan_set_fixed(aID: Integer): Integer;
    function LinkFreqPlan_tools(aID: Integer; aAction: string; aValue: Integer =
        0): Integer;


    // -------------------------
    // LINKLINE
    // -------------------------
    function LINKLINE_ADD(aProject_ID: Integer; aName: string): Integer;

    function LinkLine_Add_link(aLinkLineID, aLinkID: Integer): Integer;
    function LinkLine_GetMinBitRate_Mbps(aID: integer): integer;
    function LinkLine_get_new_name(aProject_ID: integer; aName: string): string;
    function LinkLine_Remove_link(aLinkLineID, aLinkID: Integer): Integer;

    function LinkLine_Select_Neighbors_Links(aADOStoredProc: TADOStoredProc;
        aLink_ID, aProperty_ID: Integer): Integer;

    function LinkLine_Update_Summary(aID: Integer): Integer;

    // -------------------------
    // LinkNet
    // -------------------------

    function LinkNet_Add_Item(aID, aLinkLineID, aLinkID: Integer): Integer;
    function LinkNet_links(aADOStoredProc: TADOStoredProc; aID: Integer): Integer;
    function LinkNet_SELECT_items(aADOStoredProc: TADOStoredProc; aID: Integer):
        Integer;

    function LinkType_Copy(aID: Integer; aName: string): integer;

    // -------------------------
    // Link
    // -------------------------

    function Link_ClearCalc(aID: Integer): Integer;

    function Link_Copy(aID: Integer; aName: string): integer;
    function Link_Del(aID: Integer): Integer;
    function Link_Del_with_LinkEnds(aADOStoredProc: TADOStoredProc; aID: Integer):
        Integer;
    function Link_GetSubItems(aADOStoredProc: TADOStoredProc; aID: Integer):
        Integer;
    function Link_Rename1(aID: Integer; aADOStoredProc: TADOStoredProc=nil):
        Integer;

    function Link_Summary(aADOStoredProc: TADOStoredProc; aID: Integer): Integer;
    function Link_Update_profile_XML(aID: Integer; aProfile_XML: string): integer;

    function Link_Select(aADOStoredProc: TADOStoredProc; aID: Integer): Integer;
    function Link_Select_for_calc(aADOStoredProc: TADOStoredProc; aID: Integer):  Integer;


    function Link_select_item(aADOStoredProc: TADOStoredProc; aID, aLinkEnd_ID:
        Integer
        //aPMP_TERMINAL_ID: Integer = 0; aPMP_SECTOR_ID: Integer = 0
        ):
        Integer;

    function Link_Update_Length_and_Azimuth(aID: Integer): integer;

    // -------------------------
    // LOS
    // -------------------------

    function LOS_property_select(aADOStoredProc: TADOStoredProc; aProject_ID:
        Integer; aLat_min: Double = 0; aLat_max: double = 0; aLon_min: Double = 0;
        aLon_max: double = 0): Integer;

    // -------------------------
    // Link_Repeater
    // -------------------------

    function Link_Repeater_Antenna_Update_AntennaType(aID,aAntennaType_ID:
        Integer): integer;


    function MSC_del(aID: Integer): Integer;


    // -------------------------
    // Pmp_CalcRegion
    // -------------------------
    function PMP_CalcRegion_add_point(aID: Integer; aLat, aLon: double): Integer;
    
    function PMP_CalcRegion_del(aID: Integer): Integer;
    function PMP_CalcRegion_Init(aID: Integer): Integer;
    function PMP_CalcRegion_pmp_site_Add(aCALC_REGION_ID, aPMP_SITE_ID: Integer):
        Integer;
    function PMP_CalcRegion_pmp_site_Del(aCALC_REGION_ID, aPMP_SITE_ID: Integer):
        Integer;


    function Pmp_CalcRegion_Copy(aID: Integer; aName: string): integer;
    function PMP_CalcRegion_property_select(aADOStoredProc: TADOStoredProc;
        aProject_ID: Integer; aLat_min: Double = 0; aLat_max: double = 0; aLon_min:
        Double = 0; aLon_max: double = 0): Integer;
    function pmp_CalcRegion_Select_AntennaTypes(aADOStoredProc: TADOStoredProc;
        aID: Integer): Integer;
//    procedure pmp_CalcRegion_Select_CalcModels(aID: Integer);

    function pmp_CalcRegion_Select_CalcModels(aADOStoredProc: TADOStoredProc; aID:
        Integer): Integer;

    // -------------------------
    // Pmp_Sector
    // -------------------------
    function Pmp_Sector_del(aID: Integer): Integer;
    function Pmp_Sector_update_calc_model(aID, aCalc_model_ID: Integer): Integer;

    function Pmp_site_Add(aProperty_ID: Integer; aName: string; aCalcRadius_km:
        double; aTemplate_pmp_Site_ID: Integer): integer;
    function Pmp_Site_del(aID: Integer): Integer;
    function Pmp_Site_GetSubItemList(aADOStoredProc: TADOStoredProc; aID: Integer):
        Integer;
    function Pmp_Site_Select_Item(aADOStoredProc: TADOStoredProc; aID: Integer):
        Integer;

    // -------------------------
    // PMP_TERMINAL
    // -------------------------

    function PMP_TERMINAL_PLUG(aADOStoredProc: TAdoStoredProc; aID, aNewSectorID:
        integer): Integer;

    function PMP_Terminal_Select_PMP_Sectors_for_Plug_(aADOStoredProc:
        TAdoStoredProc; aPmpTerminalID, aPROJECT_ID: Integer): Integer;

    function PmpTERMINAL_DEL(aID: Integer): Integer;

    // -------------------------
    // Project
    // -------------------------

    function Project_Clear_(aID: Integer): Integer;
    function Project_GetTables(aADOStoredProc: TADOStoredProc; aID: Integer):
        Integer;
// TODO: Project_Init
//  function Project_Init(aID: Integer): Integer;

    function PROJECT_SUMMARY(aADOStoredProc: TADOStoredProc): Integer;

    // -------------------------
    // Property
    // -------------------------

    function Property_Del(aID: Integer): Integer;
    function PROPERTY_GetConnectedLinks(aADOStoredProc: TADOStoredProc; aID:
        Integer): Integer;
    function Property_GetItemsList(aADOStoredProc: TADOStoredProc; aID: Integer):
        Integer;
//    procedure LINK_UPDATE_LENGTH_and_Azimuth(aID: Integer; aLENGTH_M, aAzimuth1, aAzimuth2: double);

//    function PROPERTY_MOVE(aID: Integer; aLAT, aLON, aLAT_WGS, aLON_WGS, aLAT_CK95,
//        aLON_CK95: double): Integer;

//    procedure Link_select_item(aADOQuery: TADOQuery; aID: Integer);
    function Property_Select_Item(aADOStoredProc: TADOStoredProc; aID: Integer):
        Integer;
    function PROPERTY_Update_GeoRegion(aID: Integer; aGeoRegion_ID: Integer):
        Integer;
    function PROPERTY_Update_Ground(aID: Integer; aGROUND_HEIGHT,aCLUTTER_HEIGHT:
        Integer; aCLUTTER_NAME : string): Integer;
//    procedure Object_Update_AntType(aObjName: string; aID, aAntennaType_ID: Integer);
  //  procedure Object_Update_AntType(aObjName: string; aID, aAntennaType_ID: Integer);
//    procedure Object_Update_AntType(aObjName: string; aID, aAntennaType_ID: Integer);
                    


    // -------------------------
    // MapDesktop
    // -------------------------

    function MapDesktop_Select(aADOStoredProc: TADOStoredProc; aProjectID:
        Integer): Integer;
    function MapDesktop_Select_CalcMaps(aADOStoredProc: TADOStoredProc; aID:
        Integer; aChecked: boolean = False): Integer;
    function MapDesktop_Select_GeoMaps(aADOStoredProc: TADOStoredProc; aID:
        Integer; aChecked: boolean = False): Integer;
    function MapDesktop_Select_ObjectMaps(aADOStoredProc: TADOStoredProc; aID:
        Integer; aChecked: boolean = False): Integer;

    function MapDesktop_tools__1111111111111(aID: Integer; aAction: string; aName:
        string = ''): Integer;

    function MapDesktop_Add(aProject_ID: Integer; aName: string): Integer;
    function MapDesktop_Move_Maps(aID: Integer; aDirection_str: string): Integer;


    // -------------------------
    // Object_
    // -------------------------
    function Object_Dependency(aADOStoredProc: TADOStoredProc; aObjName: string;
        aID: Integer): Integer;


    function Object_Has_children(aObjName: string; aID: integer): Boolean;
    function Object_Update_AntType(aObjName: string; aID, aAntennaType_ID:
        Integer): Integer;
//    procedure Object_Update_AntType(aObjName : string; aID, aAntTypeID: Integer);
    function Object_Update_CalcModel(aObjName: string; aID, aCalcModel_ID:
        Integer): Integer;
//    procedure Object_update_calc_model(aObjName : string; aID, aCalc_model_ID: Integer);

    function Object_update_LinkEndType(aObjName: string; aID, aLinkEndTypeID,
        aLinkEndType_Mode_ID: Integer): Integer;


    function Object_GetNewName_select( //aRec: TObject_GetNewName_Params;
        aObjName: string; aProject_ID: Integer = 0; aName: string = ''): string;

    function Object_GetNewName_new(aRec: TObject_GetNewName_Params; aObjName:
        string; aProject_ID: Integer = 0; aProperty_ID: Integer = 0; aParent_ID:
        integer = 0): string;
    function Object_Update_Combiner(aObjName: string; aID, aCombiner_ID:
        Integer): Integer;

    // -------------------------
    // Map
    // -------------------------

    function Map_Del(aID: Integer): Integer;
    function Map_XREF_set_default_object_params(aID: Integer): Integer;




//    function sp_password(aUserLogin,aNewPwd : string): Integer;

    // -------------------------
    // Relief
    // -------------------------

    function Relief_Select(aADOStoredProc: TADOStoredProc; aProject_ID: Integer;
        aChecked: boolean = False): Integer;

    function Status_Select_Values(aADOStoredProc: TAdoStoredProc; aName: string):
        Integer;

    // -------------------------
    // Template_Pmp_Site
    // -------------------------
    function Template_Pmp_Site_Del(aID: Integer): Integer;
    function Template_Pmp_Site_Select_Item(aADOStoredProc: TADOStoredProc; aID:
        Integer): Integer;
// TODO: Template_Site_select_item
//  procedure Template_Site_select_item(aADOStoredProc: TADOStoredProc; aID: Integer);

//    function TransferTech_Copy(aID: Integer; aName: string): integer;


    function DeleteRecordByID(aTableName : string; aID: integer): boolean;
    function ExecCommand_(aSQL: string; aParams: array of Variant): boolean;

    function ExecStoredProc_(aProcName: string; aParams: array of variant; aOutParamList: TdbParamList = nil): Integer;

    function GetFieldValue(aTableName: string; aID: Integer; aFieldName: string):
        Variant;

    function GetNameByID(aTableName: string; aID: integer): string;
    function LinkEnd_update_antenna_type(aLinkEnd_ID, aAntennaType_ID: Integer):
        Integer;
    function LinkFreqPlan_Del(aID: Integer): Integer;
    function LinkFreqPlan_Neighbors(aADOStoredProc: TADOStoredProc; aID: Integer):
        Integer;
    function LinkFreqPlan_SELECT(aADOStoredProc: TADOStoredProc; aID: Integer):
        Integer;
    function LinkNet_add(aProjectID: integer; aName: string): integer;
    function LinkNet_Add_Link(aID, aLinkID: Integer): Integer;
    function LinkNet_Clear(aID: integer): integer;
    function LinkNet_Del(aID: integer): integer;

    function Map_XREF_Update(aID: Integer; aChecked : Boolean): Integer;

    procedure OpenQuery(aADOQuery: TADOQuery; aSQL : string); overload;

//    procedure OpenQuery(aQuery : TADOQuery; aSQL : string; aParams : array of  TDBParamRec); overload;
    procedure OpenQuery(aQuery : TADOQuery; aSQL : string; aParams : array of
        Variant); overload;



    function Project_Del(aID: Integer): Integer;
    function Property_GetMaxHeight(aID: Integer): Double;

    procedure AntennaType_restore_from_mask(aID: Integer);
//    procedure CalcModel_Copy(aID: Integer; aName: string);
    procedure Linkend_restore_XPIC_params(aLinkend_Antenna_ID: Integer);

//    function Link_del_WITH_PROPERTY(aADOStoredProc: TADOStoredProc; aID: Integer):
//        Integer;



    function Relief_XREF_Update1(aRelief_ID: Integer; aChecked: Boolean): Integer;
    function Link_Update_AntennaType(aID, aAntennaType_ID: Integer): Integer;
    function Project_Add(aName: string): Integer;
//    function PROPERTY_MOVE_new(aID: Integer; aLAT, aLON: double): Integer;

    function RecordExists(aTableName : string; aID: integer): boolean;
    procedure TableOpen(aADOTable: TADOTable; aTableName : string);
    function Template_Linkend_del(aID: Integer): Integer;
    function Template_PMP_Site_Copy_to_PMP_Site(aPmp_site_id, aID: Integer):
        Integer;
//    function LinkEnd_update_antenna_type(aID,aAntennaType_ID: Integer): integer;

    function UpdateRecordByID(aTableName : string; aID : integer; aParams: array of
        TDBParamRec): boolean; overload;
    function UpdateRecordByID_list(aTableName : string; aID : integer; aParamList:
        TdbParamList): boolean;


//    function UpdateRecordByID(aTableName : string; aID : integer; aParamList:
 //       TdbParamList): boolean; overload;

    property ADOConnection: TADOConnection read FADOConnectionRef write
        SetADOConnection;

  end;


var
  dmOnega_DB_data: TdmOnega_DB_data;



const
  FLD_template_ID = 'template_ID';

  sp_PMP_CalcRegion_pmp_site_Del = 'sp_PMP_CalcRegion_pmp_site_Del';


  SP_Link_Repeater_Update = 'SP_Link_Repeater_Update';

  DEF_STATUS_Link_Status        = 'Link_Status';
  DEF_STATUS_Property_placement = 'Property_placement';

  //�������������, ������������, ...
  DEF_STATUS_property_property_status = 'property_property_status';


  sp_Property_Move_to_GeoRegion_folder = 'sp_Property_Move_to_GeoRegion_folder';
  sp_Link_Move_to_GeoRegion_folder     = 'sp_Link_Move_to_GeoRegion_folder';

  sp_Linkend_Antenna_GetNewName    = 'sp_Linkend_Antenna_GetNewName';
  //sp_Linkend_Antenna_SelectByOwner = 'sp_Linkend_Antenna_SelectByOwner';

//  sp_Relief_Add = 'sp_Relief_Add';


  sp_GroupAssign_update_record = 'sp_GroupAssign_update_record';
  sp_GroupAssign_select        = 'sp_GroupAssign_select';
  sp_GroupAssign_Exec_sql      = 'sp_GroupAssign_Exec_sql';



//  dmOnega_DB_data.Status_Select_Values(sp_Link_State, 'Link_Status');
//  dmOnega_DB_data.Status_Select_Values(sp_Property_placement, 'Property_placement');
//  dmOnega_DB_data.Status_Select_Values(sp_Property_state, 'Property_state');



//  sp_Status_Select_Values = 'sp_Status_Select_Values';

  SP_LINKEND_ANTENNA_ADD1 = 'SP_LINKEND_ANTENNA_ADD';

  //-------------------------------------------------------
  // PROPERTY
  //-------------------------------------------------------
  SP_PROPERTY_ADD = 'sp_Property_add';

  //-------------------------------------------------------
  // LinkEnd
  //-------------------------------------------------------
  SP_LinkEnd_ADD = 'SP_LinkEnd_ADD';


  //-------------------------------------------------------
  // Link
  //-------------------------------------------------------
  SP_LINK_ADD = 'SP_LINK_ADD';


  sp_LinkEndType_mode_Copy = 'sp_LinkEndType_mode_Copy';
  sp_LinkEndType_Band_Copy = 'sp_LinkEndType_Band_Copy';

  sp_Pmp_site_Add = 'sp_Pmp_site_Add';

  sp_ANTENNATYPE_add = 'sp_ANTENNATYPE_add';

  sp_LINK_UPDATE = 'sp_LINK_UPDATE';

  SP_PMP_TERMINAL_PLUG = 'sp_PMP_Terminal_Plug';

  SP_LINKFREQPLAN_ADD ='SP_LINKFREQPLAN_ADD';
  sp_LinkFreqPlan_Update_Item ='sp_LinkFreqPlan_Update_Item';

  sp_Map_XREF_Update ='sp_Map_XREF_Update';
 // sp_MapDesktop_Map_Update ='sp_MapDesktop_Map_Update';


  sp_PMP_CalcRegion_add ='sp_PMP_CalcRegion_add';
  SP_PMP_CALCREGION_SELECT_ITEM ='sp_PMP_CalcRegion_select_item';
  sp_PMP_CalcRegion_Update_Item = 'sp_PMP_CalcRegion_Update_Item';
  sp_PMP_Site_update_item ='sp_PMP_Site_update_item';

  SP_PROJECT_ADD ='SP_PROJECT_ADD';

  sp_Relief_Add = 'sp_Relief_Add';
  sp_Relief_XREF_Update ='sp_Relief_XREF_Update';
  SP_TEMPLATE_LINKEND_ADD='SP_TEMPLATE_LINKEND_ADD';
  SP_TEMPLATE_LINKEND_ANTENNA_ADD = 'SP_TEMPLATE_LINKEND_ANTENNA_ADD';
  sp_Template_PMP_Site_update_item ='sp_Template_PMP_Site_update_item';

  SP_TrxType_ADD = 'SP_TrxType_ADD';

  sp_LinkFreqPlan_Neighbors = 'sp_LinkFreqPlan_Neighbors';

//  sp_LinkFreqPlan_Neighbors_new = 'sp_LinkFreqPlan_Neighbors_new';

  sp_PMP_CalcRegion_PMP_site_select_new = 'sp_PMP_CalcRegion_PMP_site_select_new';


  sp_LinkFreqPlan_Neighbors_add    = 'sp_LinkFreqPlan_Neighbors_add';
  SP_LinkFreqPlan_Neighbors_update = 'SP_LinkFreqPlan_Neighbors_update';

//  sp_Link_Update_Calc_Results = 'sp_Link_Update_Calc_Results';
//  sp_Link_Update = 'sp_Link_Update';

  sp_PMP_Terminal_Select_PMP_Sectors_for_Plug1 = 'sp_PMP_Terminal_Select_PMP_Sectors_for_Plug';



  DEF_Clear_Interference      = 'Clear_Interference';
  DEF_Clear_Distribute        = 'Clear_Distribute';
  DEF_Clear_NEIGHBORS1         = 'Clear_NEIGHBORS';
  DEF_Set_Common_requirements = 'Set_Common_requirements';
  DEF_Set_Fixed               = 'Set_Fixed';
  DEF_Restore_Fixed           = 'Restore_Fixed';
  DEF_Clear_Fixed             = 'Clear_Fixed';
  DEF_Clear_Extra_Loss        = 'Clear_Extra_Loss';




implementation
{$R *.dfm}

const
  FLD_PMP_CALC_REGION_ID = 'PMP_CALC_REGION_ID';



class procedure TdmOnega_DB_data.Init;
begin
  Assert(not Assigned(dmOnega_DB_data));

  Application.CreateForm(TdmOnega_DB_data, dmOnega_DB_data);
end;



procedure TdmOnega_DB_data.Activity_Log(aName: string; aProject_ID: Integer);
begin
  ExecStoredProc_('sp_Activity_Log',
      [FLD_NAME, aName,
       FLD_Project_ID, aProject_ID
      ]);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.AntennaType_Copy(aID: Integer; aName: string): integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_('sp_AntennaType_Copy',
                        [FLD_ID, aID,
                         FLD_Name, aName
                         ]);

  Assert(Result>0);

end;


// ---------------------------------------------------------------
procedure TdmOnega_DB_data.AntennaType_restore_from_mask(aID: Integer);
// ---------------------------------------------------------------
begin
   ExecStoredProc_('lib.ft_AntennaType_restore_from_mask',  [FLD_ID, aID ]);
end;




// ---------------------------------------------------------------
function TdmOnega_DB_data.Property_GetMaxHeight(aID: Integer): Double;
// ---------------------------------------------------------------
begin
  OpenQuery(AdoQuery1, Format('SELECT %s (%d)', ['dbo.fn_Property_GetMaxHeight', aID]) );
  Result := AdoQuery1.Fields[0].AsFloat;

end;



// ---------------------------------------------------------------
function TdmOnega_DB_data.CalcMap_Del(aID: Integer): Integer;
// ---------------------------------------------------------------
begin
  Result :=ExecStoredProc_('sp_CalcMap_Del_',  [FLD_ID, aID]);
  Assert(Result>0);

end;


function TdmOnega_DB_data.CalcModel_Copy(aID: Integer; aName: string): integer;
begin
  Result:=ExecStoredProc_('sp_CalcModel_Copy_',
            [ FLD_ID,    aID,
              FLD_Name,  aName]);

  Assert(Result>0);

end;


procedure TdmOnega_DB_data.Linkend_restore_XPIC_params(aLinkend_Antenna_ID:
    Integer);
begin
  ExecStoredProc_('link.sp_Linkend_restore_XPIC_params',  [ FLD_ID,  aLinkend_Antenna_ID ]);
end;


// ---------------------------------------------------------------
function TdmOnega_DB_data.PMP_CalcRegion_add_point(aID: Integer; aLat, aLon:
    double): Integer;
// ---------------------------------------------------------------
begin
  Result :=ExecStoredProc_('sp_PMP_CalcRegion_add_point',
                 [FLD_ID,  aID,
                  FLD_LAT, aLat,
                  FLD_LON, aLon
                  ]);
 Assert(Result>0);
end;


function TdmOnega_DB_data.PMP_CalcRegion_del(aID: Integer): Integer;
begin
  Result := ExecStoredProc_('sp_PMP_CalcRegion_del', [FLD_ID, aID]);
  Assert(Result>0);

end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.PMP_CalcRegion_Init(aID: Integer): Integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_('sp_PMP_CalcRegion_Init', [FLD_ID, aID]);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.PMP_CalcRegion_pmp_site_Add(aCALC_REGION_ID,
    aPMP_SITE_ID: Integer): Integer;
// ---------------------------------------------------------------

begin
  Result := ExecStoredProc_ ('sp_PMP_CalcRegion_pmp_site_Add',
          [FLD_PMP_CALC_REGION_ID, aCALC_REGION_ID,
           FLD_PMP_SITE_ID,    aPMP_SITE_ID ]);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.PMP_CalcRegion_pmp_site_Del(aCALC_REGION_ID,
    aPMP_SITE_ID: Integer): Integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_ ('sp_PMP_CalcRegion_pmp_site_Del',
          [FLD_PMP_CALC_REGION_ID, aCALC_REGION_ID,
           FLD_PMP_SITE_ID,    aPMP_SITE_ID ]);
end;

function TdmOnega_DB_data.ClutterModel_restore(aiD: Integer): Integer;
begin
  Result :=ExecStoredProc_('sp_ClutterModel_restore_', [FLD_ID, aID]);
end;


function TdmOnega_DB_data.ColorSchema_Copy(aID: Integer; aName: string): Integer;
begin
  Result := ExecStoredProc_('sp_ColorSchema_Copy_',
              [FLD_ID, aID,
               FLD_Name, aName ]);
               
  Assert(Result>0);
end;


//--------------------------------------------------------------------
function TdmOnega_DB_data.DeleteRecordByID(aTableName : string; aID: integer):
    boolean;
//--------------------------------------------------------------------
begin
  FADOConnectionRef.Execute(
    Format('DELETE FROM %s WHERE id=%d', [aTableName,aID]) );
end;


//--------------------------------------------------------------------
function TdmOnega_DB_data.RecordExists(aTableName : string; aID: integer):
    boolean;
//--------------------------------------------------------------------
begin
  OpenQuery(adoQuery1, Format('SELECT Count(*) FROM %s WHERE id=%d', [aTableName, aID]) );
  Result := adoQuery1.Fields[0].AsInteger > 0;


//  FADOConnectionRef.Execute(
 //   Format('DELETE FROM %s WHERE id=%d', [aTableName,aID]) );
end;


//--------------------------------------------------------------------
function TdmOnega_DB_data.ExecCommand(aSQL: string; aParamList: TdbParamList =
    nil): boolean;
//--------------------------------------------------------------------
var i: integer;
  oParameter: TParameter;
begin
  Assert(aSQL<>'');

  ADOCommand1.CommandText:=aSQL;

  if Assigned(aParamList) then
    for i:=0 to aParamList.Count-1 do
    begin
      oParameter:=ADOCommand1.Parameters.FindParam (aParamList[i].FieldName);
      if oParameter<>nil then
        try
          oParameter.Value :=aParamList[i].FieldValue;
        except
        end;
    end;

  try
    ADOCommand1.Execute;
    Result:=True;
  except
    Result:=False;
  end;

end;


//--------------------------------------------------------------------
function TdmOnega_DB_data.ExecCommand_(aSQL: string; aParams: array of
    Variant): boolean;
//--------------------------------------------------------------------
var i: integer;
  oParameter: TParameter;
  sName: string;
  vValue: Variant;
begin
  Assert(aSQL<>'');

  ADOCommand1.CommandText:=aSQL;

//  if Assigned(aParamList) then
    for i:=0 to (Length(aParams) div 2)-1 do
    begin
      sName  :=aParams[i*2];
      vValue :=aParams[i*2+1];

      oParameter:=ADOCommand1.Parameters.FindParam (sName);
      if oParameter<>nil then
        try
          oParameter.Value :=vValue;
        except
        end;
    end;

  try
    ADOCommand1.Execute;
    Result:=True;
  except
    Result:=False;
  end;

end;


//--------------------------------------------------------------------
function TdmOnega_DB_data.GetFieldValue(aTableName: string; aID: Integer;
    aFieldName: string): Variant;
//--------------------------------------------------------------------
var
  sSQL: string;
begin
  sSQL:= Format('SELECT %s FROM %s WHERE id=%d', [aFieldName, aTableName,  aID]);

  db_OpenQuery( ADOQuery1, sSQL);

  Assert(ADOQuery1.RecordCount = 1);

  result := ADOQuery1.Fields[0].AsVariant;

end;
                                                          

// ---------------------------------------------------------------
function TdmOnega_DB_data.ExecStoredProc(aProcName: string; aParams: array of
    TdbParamRec): Integer;
begin
  Assert(Assigned(FADOConnectionRef), 'Value not assigned');

  Result := db_ExecStoredProc(FADOConnectionRef, aProcName, aParams);

//  Result := db_ExecStoredProc(FADOConnectionRef, aProcName, aParams);

end;



// ---------------------------------------------------------------
function TdmOnega_DB_data.ExecStoredProc_(aProcName: string; aParams: array of
    variant; aOutParamList: TdbParamList = nil): Integer;
begin
  Assert(Assigned(FADOConnectionRef), 'Value not assigned');

  Result := db_ExecStoredProc_(FADOConnectionRef, aProcName, aParams, aOutParamList);

//  Result := db_ExecStoredProc(FADOConnectionRef, aProcName, aParams);

end;


//



{
// ---------------------------------------------------------------
function TdmOnega_DB_data.ExecStoredProc11(aADOStoredProc: TAdoStoredProc;
    aProcName: string; aParams: array of TdbParamRec): Integer;
begin
  aADOStoredProc.Connection := FADOConnectionRef;

  Result := db_ExecStoredProc__(aADOStoredProc, aProcName, aParams);
end;
}

// ---------------------------------------------------------------
function TdmOnega_DB_data.Explorer_Select_Object(aAdoStoredProc:
    TAdoStoredProc;aObjName: string; aProject_ID: Integer = 0): Integer;
// ---------------------------------------------------------------
begin
//  aAdoStoredProc.Connection := FADOConnectionRef;

  Result :=OpenStoredProc(aAdoStoredProc, 'sp_Explorer_Select_Object',
          [FLD_ObjName,    aObjName,
           FLD_Project_ID, aProject_ID
           ]);
end;



//-------------------------------------------------------------
function TdmOnega_DB_data.Folder_GetFullPath(aID: integer): string;
//-------------------------------------------------------------
begin
  OpenQuery(AdoQuery1, Format('SELECT %s (%d)', ['dbo.fn_Folder_FullPath', aID]) );
  Result := AdoQuery1.Fields[0].AsString;
end;


{
// ---------------------------------------------------------------
function TdmOnega_DB_data.Folder_GetParents(aADOStoredProc: TADOStoredProc; aID:
    Integer): Integer;
// ---------------------------------------------------------------
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_Folder_GetParents_',
     [FLD_ID, aID] );

end;}


//-------------------------------------------------------
function TdmOnega_DB_data.LinkNet_Del(aID: integer): integer;
//-------------------------------------------------------
begin
  Result:=ExecStoredProc_ ('sp_LinkNet_Del',  [FLD_ID, aID ]);

//  Assert(Result>0);
end;


//-------------------------------------------------------
function TdmOnega_DB_data.LinkNet_Clear(aID: integer): integer;
//-------------------------------------------------------
begin
  Result:=ExecStoredProc_ ('sp_LinkNet_clear',  [FLD_ID, aID ]);

 // Assert(Result<>0);

 // Result:=Abs(Result);

end;



function TdmOnega_DB_data.LinkEndType_Band_Channel_Select(aADOStoredProc:
    TADOStoredProc; aID: Integer): Integer;
begin
 // Result := OpenStoredProc(aADOStoredProc, 'sp_LinkEndType_Band_Channel_Select', [FLD_ID, aID] );
  Result := OpenStoredProc(aADOStoredProc, 'LinkEndType.sp_Band_Channel_SEL', [FLD_ID, aID] );

  //
  //  db_View(aADOStoredProc);

end;


function TdmOnega_DB_data.LinkEndType_Copy(aID: Integer; aName: string): Integer;
begin
  Result:=ExecStoredProc_('sp_LinkEndType_Copy',
                            [ FLD_ID,   aID,
                              FLD_Name, aName
                            ] );
 Assert(Result>0);
end;


// ---------------------------------------------------------------
function TdmOnega_DB_data.LinkEndType_update_vendor(aID, aVendor_ID,
    aVendor_equipment_id: Integer): Integer;
// ---------------------------------------------------------------
begin
  assert (aVendor_ID > 0);

  Result := ExecStoredProc_ ('sp_LinkEndType_update_vendor',
                [FLD_ID, aID,
                 FLD_vendor_ID,  IIF_NULL(aVendor_ID),
                 FLD_vendor_equipment_id,  IIF_NULL(aVendor_equipment_id)
                 ]);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.Linkend_Antenna_Copy(aID: Integer; aName: string): integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_('sp_Linkend_Antenna_Copy',
                        [FLD_ID, aID,
                         FLD_Name, aName]);
// Assert(Result>0);


{  if Result>0 then
  begin

  end;
}

end;


function TdmOnega_DB_data.Linkend_Antenna_Del(aID: Integer): Integer;
begin
  Result := ExecStoredProc_('sp_Linkend_Antenna_Del', [FLD_ID, aID]);

 // Assert(Result>0);
 // Assert(Result>0);
end;

//---------------------------------------------------------------------------
function TdmOnega_DB_data.Link_Update_AntennaType(aID, aAntennaType_ID:
    Integer): Integer;
//---------------------------------------------------------------------------
begin                         //sp_Link_Update_AntennaType
  Result := ExecStoredProc_('sp_Link_Update_AntennaType',
                [
                 FLD_ID, aID,
                // FLD_LINK_ID, aID,
                 FLD_AntennaType_ID, aAntennaType_ID
                ]);
 // Assert(Result>0);
end;


//---------------------------------------------------------------------------
function TdmOnega_DB_data.Template_PMP_Site_Copy_to_PMP_Site(aPmp_site_id, aID:
    Integer): Integer;
//---------------------------------------------------------------------------
begin                         //sp_Link_Update_AntennaType
  Result := ExecStoredProc_('sp_Template_PMP_Site_Copy_to_PMP_Site',
                [FLD_ID, aID,
                 FLD_Pmp_site_id, aPmp_site_id
                ]);
 // Assert(Result>0);
end;

//
//if @TEMPLATE_PMP_SITE_ID>0
//   exec dbo.sp_Template_PMP_Site_Copy_to_PMP_Site
//   			@ID = @TEMPLATE_PMP_SITE_ID,
//            @Pmp_site_id = @new_id




//--------------------------------------------------------------------
function TdmOnega_DB_data.Linkend_Antenna_GetNewName(aObjName: string; aOwnerID:
    integer): string;
//--------------------------------------------------------------------
//var iCount: integer;
begin
 // dmOnega_DB_data.
  OpenStoredProc(ADOStoredProc1, sp_Linkend_Antenna_GetNewName,
    [
     FLD_ObjName, aObjName,
     FLD_ID,      aOwnerID
     ]);

  Result :=ADOStoredProc1.Fields[0].AsString;

//  iCount:= GetOwnerAntennaCount(aOwnerID, aObjName);
//  Result:= DisplayName + AsString(iCount+1);

end;

{
//-------------------------------------------------------------------
function TdmOnega_DB_data.Linkend_Antenna_SelectByOwner(aADOStoredProc:
    TAdoStoredProc; aObjName: string; aOwnerID: integer): Integer;
//-------------------------------------------------------------------
begin
  Result := OpenStoredProc(aADOStoredProc, sp_Linkend_Antenna_SelectByOwner,
      [FLD_ObjName, aObjName,
       FLD_ID,      aOwnerID  ]);

end;
}

// ---------------------------------------------------------------
function TdmOnega_DB_data.Link_Repeater_Antenna_Update_AntennaType(aID,
    aAntennaType_ID: Integer): integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_('sp_Link_Repeater_Antenna_Update_AntennaType',
                        [FLD_ID, aID,
                         FLD_AntennaType_ID, aAntennaType_ID
                         ]);
end;



// ---------------------------------------------------------------
function TdmOnega_DB_data.Linkend_Antenna_Update_AntennaType(aID,aAntennaType_ID:
    Integer): integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_ ('sp_Linkend_Antenna_Update_AntennaType',
                        [FLD_ID, aID,
                         FLD_AntennaType_ID, aAntennaType_ID]);

  Assert(Result>0);                       
end;
 
{

// ---------------------------------------------------------------
function TdmOnega_DB_data.Linkend_Antenna_update_Azimuth11_1111111(aObjName:
    string; aID: Integer; aAzimuth: double): Integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_('sp_Linkend_Antenna_update_Azimuth',
         [FLD_ID,        aID,
          FLD_ObjName,   aObjName,
          FLD_Azimuth,   aAzimuth
          ]);

end;
}


// ---------------------------------------------------------------
function TdmOnega_DB_data.LinkEnd_Copy(aID: Integer; aName: string): integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_ ('sp_LinkEnd_Copy',
                        [FLD_ID, aID,
                         FLD_Name, aName
                         ]);
 Assert(Result>0);
end;

function TdmOnega_DB_data.LinkEnd_del(aID: Integer): Integer;
begin
  Result := ExecStoredProc_('sp_LinkEnd_del', [FLD_id, aID]);

  Assert(Result>0);
end;

function TdmOnega_DB_data.LinkEnd_Dependency(aADOStoredProc: TADOStoredProc; aID:
    Integer): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_LinkEnd_Dependency',
    [FLD_ID, aID]);

end;

function TdmOnega_DB_data.LinkEnd_GetSubItems(aADOStoredProc: TADOStoredProc; aID:
    Integer): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_LinkEnd_GetSubItems',
    [FLD_ID, aID]);  
end;

//  dmOnega_DB_data.LinkEnd_Dependency(ADOStoredProc1, aID);


//-------------------------------------------------------------
function TdmOnega_DB_data.LinkEnd_Get_Next_LinkEnd_ID(aID: integer): integer;
//-------------------------------------------------------------
begin
  OpenQuery(adoQuery1,
    Format('SELECT %s (%d)', ['dbo.fn_LinkEnd_Get_Next_LinkEnd_ID', aID]) );

  Result := adoQuery1.Fields[0].AsInteger;
end;

function TdmOnega_DB_data.LinkEnd_UpdateNext(aID: Integer): Integer;
begin
  Log (Format('��������� ��������� ��� ��� ���: %d', [aID]));

  Result := ExecStoredProc_('sp_LinkEnd_UpdateNext', [FLD_ID, aID]);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.LinkEnd_update_antenna_type(aLinkEnd_ID,
    aAntennaType_ID: Integer): Integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_('sp_LinkEnd_update_antenna_type',
         [FLD_LinkEnd_ID,     aLinkEnd_ID,
          FLD_AntennaType_ID, aAntennaType_ID ]);
end;

function TdmOnega_DB_data.LinkFreqPlan_Init(aID: Integer): Integer;
begin
  Result := ExecStoredProc_('sp_LinkFreqPlan_Init', [FLD_LinkFreqPlan_ID, aID]);
end;

function TdmOnega_DB_data.LinkFreqPlan_Linkend1(aADOStoredProc: TADOStoredProc;
    aID: Integer): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_LinkFreqPlan_Linkend',
       [FLD_LinkFreqPlan_ID, aID ]);
end;

function TdmOnega_DB_data.LinkFreqPlan_Restore_Fixed(aID: Integer): Integer;
begin
  Result := ExecStoredProc_('sp_LinkFreqPlan_Restore_Fixed', [FLD_ID, aID]);
end;


function TdmOnega_DB_data.Template_Linkend_del(aID: Integer): Integer;
begin
  Result := ExecStoredProc_('sp_Template_Linkend_del', [FLD_ID, aID]);
end;


function TdmOnega_DB_data.LinkFreqPlan_SELECT(aADOStoredProc: TADOStoredProc;
    aID: Integer): Integer;
begin

  Result := OpenStoredProc(aADOStoredProc, 'sp_LinkFreqPlan_SELECT',
       [FLD_ID, aID ]);

  Assert(aADOStoredProc.Active );

end;


function TdmOnega_DB_data.LinkFreqPlan_Select_Linkend_NEIGHBORS(aADOStoredProc:
    TADOStoredProc; aLinkFreqPlanID, aLinkEndID: Integer): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc,
     'sp_LinkFreqPlan_Select_Linkend_NEIGHBORS',
       [FLD_LinkFreqPlan_ID, aLinkFreqPlanID,
        FLD_LinkEnd_ID, aLinkEndID
        ]);

end;


function TdmOnega_DB_data.LinkFreqPlan_Neighbors(aADOStoredProc:
    TADOStoredProc; aID: Integer): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_LinkFreqPlan_Neighbors', [FLD_ID, aID ]);
end;


function TdmOnega_DB_data.LinkFreqPlan_Select_NB_Linkends(aADOStoredProc: TADOStoredProc; aID: Integer): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc,
     'sp_LinkFreqPlan_Select_NB_Linkends',
       [FLD_LinkFreqPlan_ID, aID ]);

end;


function TdmOnega_DB_data.LinkFreqPlan_set_fixed(aID: Integer): Integer;
begin
  Result := ExecStoredProc_('sp_LinkFreqPlan_set_fixed', [FLD_ID, aID]);
end;


function TdmOnega_DB_data.LinkFreqPlan_tools(aID: Integer; aAction: string; aValue:
    Integer = 0): Integer;
begin
  Assert(aID>0);

  Result := ExecStoredProc_('sp_LinkFreqPlan_tools',
              [FLD_ID, aID,
               FLD_Action, aAction,
               FLD_Value,  aValue ]);

 // Assert(Result>0, 'function TdmOnega_DB_data.LinkFreqPlan_tools(');


end;



// ---------------------------------------------------------------
function TdmOnega_DB_data.LINKLINE_ADD(aProject_ID: Integer; aName: string):
    Integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_('sp_LINKLINE_ADD',
                 [FLD_PROJECT_ID,  aProject_ID,
                  FLD_NAME,        aName
                  ]);

  Assert(Result>0);

end;

function TdmOnega_DB_data.LinkLine_Add_link(aLinkLineID, aLinkID: Integer):
    Integer;
begin
  Result := ExecStoredProc_('sp_LinkLine_Add_link',
         [FLD_LinkLine_ID, aLinkLineID,
          FLD_Link_ID,     aLinkID
          ]);

 // Assert(Result>0);
end;

//-------------------------------------------------------------
function TdmOnega_DB_data.LinkLine_GetMinBitRate_Mbps(aID: integer): integer;
//-------------------------------------------------------------
begin
  OpenQuery(adoQuery1, Format('SELECT %s (%d)', ['dbo.fn_LinkLine_GetMinBitrate_Mbps', aID]) );
  Result := adoQuery1.Fields[0].AsInteger;
end;

//-------------------------------------------------------------
function TdmOnega_DB_data.LinkLine_get_new_name(aProject_ID: integer; aName:
    string): string;
//-------------------------------------------------------------
begin
  OpenQuery(adoQuery1,  Format('SELECT %s (%d, ''%s'')',
    ['dbo.FN_LinkLine_get_new_name', aProject_ID, aName]) );

  Result := adoQuery1.Fields[0].Asstring;
end;

function TdmOnega_DB_data.LinkLine_Remove_link(aLinkLineID, aLinkID: Integer):
    Integer;
begin
  Result :=ExecStoredProc_('sp_LinkLine_Remove_link',
         [FLD_LinkLine_ID, aLinkLineID,
          FLD_Link_ID,     aLinkID  ]);

end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.LinkLine_Select_Neighbors_Links(aADOStoredProc:
    TADOStoredProc; aLink_ID, aProperty_ID: Integer): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_LinkLine_Select_Neighbors_Links',
         [FLD_Property_ID, aProperty_ID,
          FLD_Link_ID,     aLink_ID
          ]);

end;


function TdmOnega_DB_data.LinkLine_Update_Summary(aID: Integer): Integer;
begin
  Result :=ExecStoredProc_('sp_LinkLine_Update_Summary', [FLD_ID, aID]);
end;



//-------------------------------------------------------
function TdmOnega_DB_data.LinkNet_add(aProjectID: integer; aName: string):
    integer;
//-------------------------------------------------------
//var i: Integer;
begin
  Result:=dmOnega_DB_data.ExecStoredProc_ ('sp_LinkNet_add',
                [FLD_PROJECT_ID, aProjectID,
                 FLD_NAME,       aName
                 ]  );

  Result:=Abs(Result);
end;


function TdmOnega_DB_data.LinkNet_Add_Link(aID, aLinkID: Integer): Integer;
begin
  Assert(aLinkID>0);

  Result :=ExecStoredProc_('sp_LinkNet_Add_Item',
         [FLD_ID,          aID,
//          FLD_LinkLine_ID, IIF_NULL(aLinkLineID),
          FLD_Link_ID,     aLinkID
          ]);
end;


function TdmOnega_DB_data.LinkNet_Add_Item(aID, aLinkLineID, aLinkID: Integer):
    Integer;
begin
  Result :=ExecStoredProc_('sp_LinkNet_Add_Item',
         [FLD_ID,          aID,
          FLD_LinkLine_ID, IIF_NULL(aLinkLineID),
          FLD_Link_ID,     IIF_NULL(aLinkID)
          ]);
end;

function TdmOnega_DB_data.LinkNet_links(aADOStoredProc: TADOStoredProc; aID:
    Integer): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_LinkNet_links',  [FLD_ID, aID]);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.LinkNet_SELECT_items(aADOStoredProc: TADOStoredProc;
    aID: Integer): Integer;
// ---------------------------------------------------------------
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_LinkNet_SELECT_items',  [FLD_ID,  aID ]);

//  OpenQuery(aADOQuery, Format('exec %s %d',['sp_LinkNet_SELECT_items', aID]));
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.LinkType_Copy(aID: Integer; aName: string): integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_('sp_LinkType_Copy',
                        [FLD_ID, aID,
                         FLD_Name, aName]);

  Assert(Result>0);
end;


function TdmOnega_DB_data.Link_ClearCalc(aID: Integer): Integer;
begin
  Result := ExecStoredProc_('sp_Link_ClearCalc', [FLD_ID, aID]);
end;


// ---------------------------------------------------------------
function TdmOnega_DB_data.Link_Copy(aID: Integer; aName: string): integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_('sp_Link_Copy',
                        [FLD_ID, aID,
                         FLD_Name, aName]);
  Assert(Result>0);
end;



function TdmOnega_DB_data.Link_Del(aID: Integer): Integer;
begin
  Result := ExecStoredProc_('dbo.sp_Link_Del', [FLD_id, aID]);
//  Assert(Result>0);
end;





function TdmOnega_DB_data.Link_Del_with_LinkEnds(aADOStoredProc:
    TADOStoredProc; aID: Integer): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_Link_Del_with_LinkEnds', [FLD_id, aID]);
 // Assert(Result>0);
end;

function TdmOnega_DB_data.Link_GetSubItems(aADOStoredProc: TADOStoredProc; aID:
    Integer): Integer;
begin


  Result := OpenStoredProc (aADOStoredProc, 'sp_Link_GetSubItems', [FLD_ID, aID ]);


//  OpenQuery(aADOQuery,
 //   Format('exec %s %d',['sp_Link_GetSubItems', aID]));

end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.Link_Rename1(aID: Integer; aADOStoredProc:
    TADOStoredProc=nil): Integer;
// ---------------------------------------------------------------
begin

  if Assigned(aADOStoredProc) then
    Result := OpenStoredProc(aADOStoredProc, 'sp_Link_Rename',
         [FLD_ID, aID ])

  else
    Result := OpenStoredProc(ADOStoredProc1, 'sp_Link_Rename',
         [FLD_ID, aID ]);

end;


function TdmOnega_DB_data.Link_Select(aADOStoredProc: TADOStoredProc; aID: Integer):   Integer;
begin

  Result := OpenStoredProc(aADOStoredProc, 'sp_Link_select',
         [FLD_ID, aID ]);

  g_Log.Add ('Result>0 - function TdmOnega_DB_data.Link_Select(aADOStoredProc: TADOStoredProc; aID: Integer):');

//  Assert (Result>0, 'function TdmOnega_DB_data.Link_Select(aADOStoredProc: TADOStoredProc; aID: Integer):');

end;


function TdmOnega_DB_data.Link_Select_for_calc(aADOStoredProc: TADOStoredProc; aID: Integer):  Integer;
begin

  Result := OpenStoredProc(aADOStoredProc, 'link_calc.sp_Link_select_for_calc',  [FLD_ID, aID ]);

 // g_Log.Add ('Result>0 - function TdmOnega_DB_data.Link_Select(aADOStoredProc: TADOStoredProc; aID: Integer):');

//  Assert (Result>0, 'function TdmOnega_DB_data.Link_Select(aADOStoredProc: TADOStoredProc; aID: Integer):');

end;


// ---------------------------------------------------------------
function TdmOnega_DB_data.Link_select_item(aADOStoredProc: TADOStoredProc; aID,
    aLinkEnd_ID: Integer
//     aPMP_TERMINAL_ID: Integer = 0; aPMP_SECTOR_ID:  Integer = 0
    ): Integer;
// ---------------------------------------------------------------
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_Link_select_item',
         [FLD_ID,          aID,
          FLD_LinkEnd_ID,  aLinkEnd_ID

   //       FLD_PMP_SECTOR_ID,    aPMP_SECTOR_ID,
    //      FLD_PMP_TERMINAL_ID,  aPMP_TERMINAL_ID
         ]);
end;

function TdmOnega_DB_data.Link_Summary(aADOStoredProc: TADOStoredProc; aID:
    Integer): Integer;
begin
 // Assert(aID>0, 'Value <=0');

  Result := OpenStoredProc(aADOStoredProc, 'Link.sp_Link_Summary',  [FLD_ID,  aID ]);
//  Result := OpenStoredProc(aADOStoredProc, 'sp_Link_Summary',  [FLD_ID,  aID ]);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.Link_Update_profile_XML(aID: Integer; aProfile_XML:
    string): integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_('sp_Link_Update_profile_XML',
                        [FLD_ID, aID,
                         FLD_Profile_XML, aProfile_XML
                         ]);
end;

procedure TdmOnega_DB_data.Log(aMsg: string);
begin
  g_Log.SysMsg (aMsg);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.LOS_property_select(aADOStoredProc: TADOStoredProc;
    aProject_ID: Integer; aLat_min: Double = 0; aLat_max: double = 0; aLon_min:
    Double = 0; aLon_max: double = 0): Integer;
// ---------------------------------------------------------------
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_LOS_property_select',
      [FLD_Project_ID, aProject_ID,
       FLD_LAT_MIN, aLat_min,
       FLD_LAT_MAX, aLat_max,
       FLD_LON_MIN, aLon_min,
       FLD_LON_MAX, aLon_max
      ]);
end;

function TdmOnega_DB_data.MapDesktop_Add(aProject_ID: Integer; aName: string):
    integer;
begin
  Result := ExecStoredProc_ ('sp_MapDesktop_Add',
                [FLD_Project_ID, aProject_ID,
                 FLD_NAME,       aName ]);
  Assert(Result>0);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.MapDesktop_Move_Maps(aID: Integer; aDirection_str:
    string): Integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_ ('sp_MapDesktop_Move_Maps',
        [FLD_ID, aID,
         'Direction_str',   aDirection_str ]);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.Map_Del(aID: Integer): Integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_ ('sp_Map_Del', [FLD_ID, aID]);

  Assert(Result>0, 'function TdmOnega_DB_data.Map_Del(aID: Integer): Integer;');

end;

function TdmOnega_DB_data.MSC_del(aID: Integer): Integer;
begin
  Result := ExecStoredProc_('sp_MSC_del_', [FLD_ID, aID]);

//  Assert(Result>0);

end;

//-------------------------------------------------------------
function TdmOnega_DB_data.Object_Has_children(aObjName: string; aID: integer):
    Boolean;
//-------------------------------------------------------------
begin
// db_Par(FLD_ObjName, aObjName),

  OpenQuery(adoQuery1,
    Format('SELECT %s (''%s'',%d)',
      ['dbo.fn_Object_Has_children', aObjName, aID]) );

  Result := adoQuery1.Fields[0].AsBoolean;
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.Object_Update_AntType(aObjName: string; aID,
    aAntennaType_ID: Integer): Integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_ ('sp_Object_Update_AntType',
                [FLD_ObjName,    aObjName,
                 FLD_ID,         aID,
                 FLD_ANTENNATYPE_ID, IIF_NULL(aAntennaType_ID)
                 ]);
end;

function TdmOnega_DB_data.Object_Update_CalcModel(aObjName: string; aID,
    aCalcModel_ID: Integer): Integer;
begin
  Result := ExecStoredProc_('sp_Object_Update_CalcModel',
            [FLD_ObjName, aObjName,
             FLD_ID, aID,
             FLD_Calc_Model_ID, IIF_NULL(aCalcModel_ID)
            ]);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.Object_update_LinkEndType(aObjName: string; aID,
    aLinkEndTypeID, aLinkEndType_Mode_ID: Integer): Integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_('sp_Object_update_LinkEndType',
         [FLD_ObjName,         aObjName,

          FLD_ID,              aID,
          FLD_LinkEndType_ID,  aLinkEndTypeID,
          FLD_LinkEndType_Mode_ID, aLinkEndType_Mode_ID
          ]);
end;

// ---------------------------------------------------------------
procedure TdmOnega_DB_data.OpenQuery(aADOQuery: TADOQuery; aSQL : string);
// ---------------------------------------------------------------
begin
  aADOQuery.Connection := FADOConnectionRef;
  db_OpenQuery(aADOQuery, aSQL);
end;

// ---------------------------------------------------------------
procedure TdmOnega_DB_data.TableOpen(aADOTable: TADOTable; aTableName : string);
// ---------------------------------------------------------------
begin
  aADOTable.Connection := FADOConnectionRef;
  db_TableOpen1(aADOTable, aTableName);

end;

{

procedure TdmOnega_DB_data.OpenQuery(aQuery : TADOQuery; aSQL : string; aParams
    : array of TDBParamRec);
begin
  aQuery.Connection := FADOConnectionRef;
  db_OpenQuery(aQuery, aSQL, aParams);
end;
  }

function TdmOnega_DB_data.OpenStoredProc(aADOStoredProc: TADOStoredProc;
    aProcName: string; aParams: array of Variant): integer;
begin
  Assert(Assigned(ADOStoredProc1), 'Value not assigned');

  aADOStoredProc.Connection := FADOConnectionRef;

  Result := db_ExecStoredProc__new(aADOStoredProc, aProcName, aParams, [flOPenQuery]);
end;


function TdmOnega_DB_data.ExecStoredProc(aADOStoredProc: TADOStoredProc;   aProcName: string; aParams: array of Variant): integer;
begin
  Assert(Assigned(ADOStoredProc1), 'Value not assigned');

  aADOStoredProc.Connection := FADOConnectionRef;

  Result := db_ExecStoredProc__new(aADOStoredProc, aProcName, aParams, []);
end;


function TdmOnega_DB_data.PmpTERMINAL_DEL(aID: Integer): Integer;
begin
  Result := ExecStoredProc_('sp_PMP_TERMINAL_DEL', [FLD_ID, aID]);
  Assert(Result>0);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.Pmp_CalcRegion_Copy(aID: Integer; aName: string): integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_('sp_Pmp_CalcRegion_Copy',
                        [FLD_ID, aID,
                         FLD_Name, aName]);
  Assert(Result>0);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.PMP_CalcRegion_property_select(aADOStoredProc:
    TADOStoredProc; aProject_ID: Integer; aLat_min: Double = 0; aLat_max:
    double = 0; aLon_min: Double = 0; aLon_max: double = 0): Integer;
// ---------------------------------------------------------------
begin
//sp_PMP_CalcRegion_property_select
  Result := OpenStoredProc(aADOStoredProc, 'sp_PMP_CalcRegion_property_select',
      [FLD_Project_ID, aProject_ID,
       FLD_LAT_MIN, aLat_min,
       FLD_LAT_MAX, aLat_max,
       FLD_LON_MIN, aLon_min,
       FLD_LON_MAX, aLon_max
      ]);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.pmp_CalcRegion_Select_AntennaTypes(aADOStoredProc:
    TADOStoredProc; aID: Integer): Integer;
// ---------------------------------------------------------------
begin
  Result := OpenStoredProc(aADOStoredProc,
            'sp_pmp_CalcRegion_Select_AntennaTypes',
             [FLD_ID, aID]);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.pmp_CalcRegion_Select_CalcModels(aADOStoredProc:
    TADOStoredProc; aID: Integer): Integer;
// ---------------------------------------------------------------
begin
  Result := OpenStoredProc(aADOStoredProc,'sp_pmp_CalcRegion_Select_CalcModels', [FLD_ID, aID]);
end;

//  Result:=gl_DB.ExecSP (SP_PMPTERMINAL_DEL, [db_Par(FLD_ID, aID)]);


function TdmOnega_DB_data.Pmp_Sector_del(aID: Integer): Integer;
begin
  Result := ExecStoredProc_('sp_Pmp_Sector_del', [FLD_ID, aID]);
 Assert(Result>0, 'dmOnega_DB_data.Pmp_Sector_del(aID: Integer): Integer');
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.Pmp_Sector_update_calc_model(aID, aCalc_model_ID:
    Integer): Integer;
// ---------------------------------------------------------------
begin
  Result :=  ExecStoredProc_ ('sp_Pmp_Sector_update_calc_model',
          [FLD_ID, aID,
           FLD_calc_model_ID, aCalc_model_ID
           ]);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.Pmp_site_Add(aProperty_ID: Integer; aName: string;
    aCalcRadius_km: double; aTemplate_pmp_Site_ID: Integer): integer;
// ---------------------------------------------------------------
//aADOStoredProc: TADOStoredProc;
begin
  Result := ExecStoredProc_ ('sp_Pmp_site_Add',
//  Result := ExecStoredProc (aADOStoredProc, 'sp_Pmp_site_Add',
        [FLD_Property_ID,      aProperty_ID,
         FLD_Name,             aName,
         FLD_CALC_RADIUS_KM,   aCalcRadius_km,

         FLD_Template_pmp_Site_ID, aTemplate_pmp_Site_ID

         ]);
  Assert(Result>0);
end;

function TdmOnega_DB_data.Pmp_Site_del(aID: Integer): Integer;
begin
  Result := ExecStoredProc_('sp_Pmp_Site_del', [FLD_ID, aID]);
  Assert(Result>0);
end;

function TdmOnega_DB_data.Pmp_Site_GetSubItemList(aADOStoredProc: TADOStoredProc;
    aID: Integer): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_Pmp_Site_GetSubItemList',
              [FLD_ID, aID]);

 // OpenQuery(aADOQuery,
  //z  Format('exec %s %d',['sp_PmpSite_GetSubItemList', aID]));
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.Pmp_Site_Select_Item(aADOStoredProc: TADOStoredProc; aID:
    Integer): Integer;
// ---------------------------------------------------------------
begin
//  OpenStoredProc('sp_Pmp_Site_Select_Item', [db_Par(FLD_ID, aID) ]);

  Result := OpenStoredProc(aADOStoredProc, 'sp_Pmp_Site_Select_Item',
     [FLD_ID, aID ]);
end;


function TdmOnega_DB_data.Project_Clear_(aID: Integer): Integer;
begin
  Result := ExecStoredProc_('sp_Project_Clear_', [FLD_ID, aID ]);

  Assert(Result>0, 'function TdmOnega_DB_data.Project_Clear_(aID: Integer): Integer;  '+ IntToStr(aID));

end;

function TdmOnega_DB_data.Project_Del(aID: Integer): Integer;
begin
  Result := ExecStoredProc_('sp_Project_Del', [FLD_ID, aID ]);
 // Assert(Result>0);
end;

//---------------------------------------------------------------------------
function TdmOnega_DB_data.Project_Add(aName: string): Integer;
//---------------------------------------------------------------------------
begin
  Result := ExecStoredProc_('sp_Project_Add', [FLD_NAME, aName ]);
//  Assert(Result>0);


end;



// ---------------------------------------------------------------
function TdmOnega_DB_data.Project_GetTables(aADOStoredProc: TADOStoredProc; aID:
    Integer): Integer;
// ---------------------------------------------------------------
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_Project_GetTables',
    [FLD_ID, aID]);

//  Assert(Result>0);

//  OpenQuery(aADOQuery,Format('exec %s %d ', ['sp_Project_GetTables',aID]));
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.Property_Del(aID: Integer): Integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_('sp_Property_Del_', [FLD_id, aID ]);

//  Assert(Result>0);

//  ExecStoredProc('sp_Property_Del', [db_Par(FLD_Property_id, aID ) ]);
end;



// ---------------------------------------------------------------
function TdmOnega_DB_data.LinkFreqPlan_Del(aID: Integer): Integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_('sp_LinkFreqPlan_Del', [FLD_id, aID ]);

//  Assert(Result>0);

//  ExecStoredProc('sp_Property_Del', [db_Par(FLD_Property_id, aID ) ]);
end;



function TdmOnega_DB_data.PROPERTY_GetConnectedLinks(aADOStoredProc: TADOStoredProc;
    aID: Integer): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc, 'SP_PROPERTY_GetConnectedLinks',
    [FLD_ID, aID]);


 // OpenQuery(aADOQuery,
//    Format('exec %s %d', ['SP_PROPERTY_GetConnectedLinks', aID]));
end;

function TdmOnega_DB_data.Property_GetItemsList(aADOStoredProc: TADOStoredProc;
    aID: Integer): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_Property_GetItemsList_',
    [FLD_ID, aID]);

//  OpenQuery(aADOQuery,
 //   Format('exec %s %d',['sp_Property_GetItemsList', aID]));
end;

{
// ---------------------------------------------------------------
function TdmOnega_DB_data.PROPERTY_MOVE(aID: Integer; aLAT, aLON, aLAT_WGS,
    aLON_WGS, aLAT_CK95, aLON_CK95: double): Integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_('sp_Property_MOVE',
              [FLD_Property_id, aID ,

               FLD_LAT, aLAT ,
               FLD_LON, aLON ,

               FLD_LAT_WGS, aLAT_WGS,
               FLD_LON_WGS, aLON_WGS,

               FLD_LAT_CK95, aLAT_CK95,
               FLD_LON_CK95, aLON_CK95

              ]);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.PROPERTY_MOVE_new(aID: Integer; aLAT, aLON: double):
    Integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_('sp_Property_MOVE',
              [FLD_Property_id, aID ,

               FLD_LAT, aLAT ,
               FLD_LON, aLON

            //   FLD_LAT_WGS, aLAT_WGS,
           //    FLD_LON_WGS, aLON_WGS,

            //   FLD_LAT_CK95, aLAT_CK95,
            //   FLD_LON_CK95, aLON_CK95

              ]);
end;

 }

// ---------------------------------------------------------------
function TdmOnega_DB_data.Property_Select_Item(aADOStoredProc: TADOStoredProc;
    aID: Integer): Integer;
// ---------------------------------------------------------------
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_Property_Select_Item',
        [ FLD_ID, aID]);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.PROPERTY_Update_GeoRegion(aID: Integer;
    aGeoRegion_ID: Integer): Integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_('sp_PROPERTY_Update',
        [
         FLD_ID, aID,
         FLD_GeoRegion_ID,  aGeoRegion_ID
        ]);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.PROPERTY_Update_Ground(aID: Integer; aGROUND_HEIGHT,
    aCLUTTER_HEIGHT: Integer; aCLUTTER_NAME : string): Integer;
// ---------------------------------------------------------------
begin
  Result :=ExecStoredProc_('sp_PROPERTY_Update',
        [
         FLD_ID, aID,

         FLD_GROUND_HEIGHT,  aGROUND_HEIGHT,
         FLD_CLUTTER_HEIGHT, aCLUTTER_HEIGHT,
         FLD_CLUTTER_NAME,   aCLUTTER_NAME
        ]);


end;

procedure TdmOnega_DB_data.SetADOConnection(const Value: TADOConnection);
begin
  FADOConnectionRef := Value;

  ADOQuery1.Connection      :=FADOConnectionRef;
  ADOStoredProc1.Connection :=FADOConnectionRef;
  ADOCommand1.Connection    :=FADOConnectionRef;

end;


function TdmOnega_DB_data.Folder_Has_Children(aObjectName: string; aID: integer;
    aProjectID: integer = 0): Boolean;
var
  k: Integer;
begin
  k:=ExecStoredProc_('sp_Folder_Has_Children', // [db_Par(FLD_ID, aID)]);
              [ FLD_ObjName,     aObjectName,
                FLD_ID,          aID,
                FLD_Project_ID,  IIF_NULL(aProjectID) //>0,aProjectID,0))
              ]);
  Result:=k > 0;
end;


function TdmOnega_DB_data.GroupAssign_Select_subitems(aADOStoredProc:
    TAdoStoredProc; aID: Integer): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_GroupAssign_Select_subitems',
         [FLD_OBJECT_FIELD_ID, aID]);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.Link_Update_Length_and_Azimuth(aID: Integer): integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_('sp_Link_Update_Length_and_Azimuth',
                        [FLD_ID, aID]);
end;

function TdmOnega_DB_data.MapDesktop_Select(aADOStoredProc: TADOStoredProc;
    aProjectID: Integer): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_MapDesktop_Select',
        [FLD_PROJECT_ID, aProjectID]);
end;


function TdmOnega_DB_data.MapDesktop_Select_CalcMaps(aADOStoredProc:
    TADOStoredProc; aID: Integer; aChecked: boolean = False): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_MapDesktop_Select_CalcMaps',
        [FLD_ID,      aID,
         FLD_Checked, IIF(aChecked, aChecked, NULL)
        ]);
end;


function TdmOnega_DB_data.MapDesktop_Select_GeoMaps(aADOStoredProc:
    TADOStoredProc; aID: Integer; aChecked: boolean = False): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc,
     'sp_MapDesktop_select_GeoMaps',
        [FLD_ID, aID,
         FLD_Checked, IIF(aChecked, aChecked, NULL)
        ]);
end;


function TdmOnega_DB_data.MapDesktop_Select_ObjectMaps(aADOStoredProc:
    TADOStoredProc; aID: Integer; aChecked: boolean = False): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_MapDesktop_select_ObjectMaps',
        [FLD_ID,       aID,
         FLD_Checked,  IIF(aChecked, aChecked, NULL)
        ]);
end;

function TdmOnega_DB_data.MapDesktop_tools__1111111111111(aID: Integer;
    aAction: string; aName: string = ''): Integer;
begin
  Result := ExecStoredProc_( 'sp_MapDesktop_tools',
        [FLD_ID,     aID,
         FLD_Action, aAction,
         FLD_NAME,   aName
        ]);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.Map_XREF_set_default_object_params(aID: Integer):
    Integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_ ('sp_Map_XREF_set_default_object_params', [FLD_ID, aID]);
end;

function TdmOnega_DB_data.Object_Dependency(aADOStoredProc: TADOStoredProc;
    aObjName: string; aID: Integer): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_Object_Dependency',
        [FLD_ObjName, aObjName,
         FLD_ID, aID ]);
end;



function TdmOnega_DB_data.GetNameByID(aTableName: string; aID: integer): string;
begin
  OpenQuery(AdOQuery1, Format('SELECT name FROM %s WHERE id=%d', [aTableName, aID]));
  Result := AdOQuery1.FieldByName(FLD_NAME).AsString;
end;


// ---------------------------------------------------------------
function TdmOnega_DB_data.Object_GetNewName_select(
//--aRec:
//    TObject_GetNewName_Params;

    aObjName: string; aProject_ID: Integer = 0;
    aName: string = ''): string;
// ---------------------------------------------------------------
var
  k: Integer;
begin
  assert ( aObjName <> '');

  k:=OpenStoredProc(ADOStoredProc1,'sp_Object_GetNewName_select', // [db_Par(FLD_ID, aID)]);
            [ FLD_ObjName,     aObjName,
              FLD_Name,        aName,
              FLD_Project_ID,  IIF_NULL(aProject_ID) //>0,aProjectID,0))
            ]);

  Result := ADOStoredProc1.Fields[0].AsString;
end;


// ---------------------------------------------------------------
function TdmOnega_DB_data.Object_GetNewName_new(aRec:
    TObject_GetNewName_Params; aObjName: string; aProject_ID: Integer = 0;
    aProperty_ID: Integer = 0; aParent_ID: integer = 0): string;
// ---------------------------------------------------------------
const
  FLD_RESULT = 'RESULT';
var
  k: Integer;
begin

  k:=OpenStoredProc(ADOStoredProc1,'sp_Object_GetNewName_new', // [db_Par(FLD_ID, aID)]);
            [ FLD_ObjName,     aObjName,
             // db_Par(FLD_Name,        aName),
              FLD_Project_ID,  IIF_NULL(aProject_ID), //>0,aProjectID,0))
              FLD_Property_ID, IIF_NULL(aProperty_ID), //>0,aProjectID,0))

              FLD_Parent_ID, IIF_NULL(aParent_ID), //>0,aProjectID,0))

              FLD_MODE, 'SELECT', //>0,aProjectID,0))

              FLD_RESULT, '' //>0,aProjectID,0))
            ]);


  Result := ADOStoredProc1.Fields[0].AsString;

end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.Object_Update_Combiner(aObjName: string; aID,
    aCombiner_ID: Integer): Integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_('sp_Object_Update_Combiner',
          [
           FLD_ObjName, aObjName,
           FLD_ID, aID,
           FLD_Combiner_ID, aCombiner_ID
           ]);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.PROJECT_SUMMARY(aADOStoredProc: TADOStoredProc):
    Integer;
// ---------------------------------------------------------------
begin
  Result := OpenStoredProc(aADOStoredProc,'sp_PROJECT_SUMMARY',[]);
end;

function TdmOnega_DB_data.Relief_Select(aADOStoredProc: TADOStoredProc;
    aProject_ID: Integer; aChecked: boolean = False): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_Relief_Select',
      [FLD_Project_ID, aProject_ID,
       FLD_Checked, IIF(aChecked, aChecked, null)
      ]);
end;

function TdmOnega_DB_data.Status_Select_Values(aADOStoredProc: TAdoStoredProc;
    aName: string): Integer;
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_Status_Select_Values',
         [FLD_name, aName]);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.Template_Pmp_Site_Del(aID: Integer): Integer;
// ---------------------------------------------------------------
begin
  Result := ExecStoredProc_ ('sp_Template_PMP_Site_Del', [FLD_ID, aID]);
  Assert(Result>0);
end;

// ---------------------------------------------------------------
function TdmOnega_DB_data.Template_Pmp_Site_Select_Item(aADOStoredProc:
    TADOStoredProc; aID: Integer): Integer;
// ---------------------------------------------------------------
begin
  Result := OpenStoredProc(aADOStoredProc, 'sp_Template_PMP_Site_Select_Item',
      [FLD_ID, aID]);
end;


//----------------------------------------------------------------------------
function TdmOnega_DB_data.UpdateRecordByID_list(aTableName : string; aID :
    integer; aParamList: TdbParamList): boolean;
//----------------------------------------------------------------------------
var
  s: string;
begin
  Assert (aTableName<>'', 'function TDBManager.UpdateRecordByID - aTableName=''''');

  s:=aParamList.MakeUpdateString(aTableName, aID);
  s:=db_MakeUpdateString(aTableName, aID, aParamList);

  Result := ExecCommand (s, aParamList);

end;


//----------------------------------------------------------------------------
function TdmOnega_DB_data.UpdateRecordByID(aTableName : string; aID : integer;
    aParams: array of TDBParamRec): boolean;
//----------------------------------------------------------------------------
var
  s: string;
  oParamList: TdbParamList;
begin
  Assert (aTableName<>'', 'function TDBManager.UpdateRecordByID - aTableName=''''');


  oParamList:= TdbParamList.Create;
  oParamList.LoadFromArr(aParams);

  s:=oParamList.MakeUpdateString(aTableName, aID);
  s:=db_MakeUpdateString(aTableName, aID, oParamList);

  Result := ExecCommand (s, oParamList);

  FreeAndNil(oParamList);

end;



//-------------------------------------------------------------------
function TdmOnega_DB_data.PMP_Terminal_Select_PMP_Sectors_for_Plug_(
    aADOStoredProc: TAdoStoredProc; aPmpTerminalID, aPROJECT_ID: Integer):
    Integer;
//-------------------------------------------------------------------
begin
  Result := OpenStoredProc(aADOStoredProc,
     sp_PMP_Terminal_Select_PMP_Sectors_for_Plug1,
       [
         FLD_ID, aPmpTerminalID,
         FLD_PROJECT_ID, aPROJECT_ID
        ]);

end;



//-------------------------------------------------------------------
function TdmOnega_DB_data.Relief_XREF_Update1(aRelief_ID: Integer; aChecked:
    Boolean): Integer;
//-------------------------------------------------------------------
begin
  result:=ExecStoredProc_ ( sp_Relief_XREF_Update,
        [
          FLD_Relief_ID, aRelief_ID,
    //     db_Par (FLD_ID,        aRelief_ID),
          FLD_CHECKED,   aChecked
        ]);
end;

//-------------------------------------------------------------------
function TdmOnega_DB_data.Map_XREF_Update(aID: Integer; aChecked : Boolean):
    Integer;
//-------------------------------------------------------------------
begin
  Result:=ExecStoredProc_ ( sp_Map_XREF_Update,
        [
         FLD_ID,        aID,
         FLD_CHECKED,   aChecked
        ]);
end;

procedure TdmOnega_DB_data.OpenQuery(aQuery : TADOQuery; aSQL : string; aParams
    : array of Variant);
begin
  aQuery.Connection := FADOConnectionRef;
  db_OpenQuery_(aQuery, aSQL, aParams);


end;



//-------------------------------------------------------------------
function TdmOnega_DB_data.PMP_TERMINAL_PLUG(aADOStoredProc: TAdoStoredProc;
    aID, aNewSectorID: integer): Integer;
//-------------------------------------------------------------------
begin
  Result := OpenStoredProc(aADOStoredProc,
     SP_PMP_TERMINAL_PLUG,
               [FLD_ID, aID,
                 FLD_PMP_SECTOR_ID, aNewSectorID
                 ]);

end;


//-------------------------------------------------------------------
procedure TdmOnega_DB_data.sp_LinkLine_Add_link(aLinkLineID, aLinkID: Integer);
//-------------------------------------------------------------------
var
  k: Integer;
begin
//  db_OpenQuery (qry_Temp,
  //              'SELECT LINK_ID FROM '+ VIEW_LINKLINE_LINKS + ' WHERE (linkline_id=:id)',
        //       [db_Par(FLD_ID, aLinkLineID)]);

  k:=ExecStoredProc_('sp_LinkLine_Add_link',
             [FLD_LINKLINE_ID, aLinkLineID,
              FLD_LINK_ID,     aLinkID ]);
 // end;

 //   if not qry_Temp.Locate (FLD_LINK_ID, aIDList[i].ID, []) then
   //   Add (aLinkLineID, aIDList[i].ID);

end;




end.


