unit dm_Main_res;

interface

uses
  SysUtils, Dialogs, Classes, ActnList, ImgList, Controls, Windows, Graphics, Menus, Forms,

  u_const,
  u_types;

type
  TdmMain_res = class(TDataModule)
    imgs_Common: TImageList;
    img_MIF_LineStyle: TImageList;
    img_MIF_RegionStyle: TImageList;
    img_Tools: TImageList;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure LoadImages();
    { Private declarations }
  public
    function GetImageIndexByName (aName: string): integer;
    function GetImageIndexByObject (aObjectName: string): integer;

    class procedure Init;

  end;


const

  ICON_ANTENNA = 'ANTENNA';
  ICON_BSC     = 'BSC';
  ICON_CELL    = 'CELL';
  ICON_CALC_REGION = 'CALC_REGION';
  ICON_LINK    = 'LINK';
  ICON_LINKEND = 'LINKEND';
  ICON_LINKLINE= 'LINKLINE';
  ICON_LINKFREQ= 'LINKFREQ';
  ICON_LINKNET = 'LINKNET';
  ICON_MSC     = 'MSC';
  ICON_PROJECT = 'PROJECT';
  ICON_PROPERTY= 'PROPERTY';
  ICON_SITE    = 'SITE';
  ICON_PMP_SITE        = 'PMP_SITE';
  ICON_PMP_TERMINAL    = 'PMP_TERMINAL';

  ICON_FOLDER  = 'FOLDER';
  ICON_FOLDER_OPEN  = 'FOLDER_OPEN';

const
  IMAGE_NAMES : array[0..16] of string =
   (
    ICON_FOLDER,
    ICON_FOLDER_OPEN,

    ICON_ANTENNA,
    ICON_BSC,
    ICON_CELL,
    ICON_SITE,
    ICON_PMP_SITE,
    ICON_PMP_TERMINAL,

    ICON_CALC_REGION,

    ICON_MSC,
    ICON_PROJECT,
    ICON_PROPERTY,
    ICON_LINK,
    ICON_LINKEND,
    ICON_LINKLINE,
    ICON_LINKNET,
    ICON_LINKFREQ
    );


var
  dmMain_res: TdmMain_res;


//===============================================================
// implementation
//===============================================================

implementation {$R *.dfm}

{$R w:\rpls_db\images.RES}



class procedure TdmMain_res.Init;
begin
  if not Assigned(dmMain_res) then
    dmMain_res:=TdmMain_res.Create(Application)
//  else
  //  raise Exception.Create('');
end;


//--------------------------------------------------------------------
function TdmMain_res.GetImageIndexByObject (aObjectName: string): integer;
//--------------------------------------------------------------------
var ot: TrpObjectType;
begin
  if not Assigned(g_Obj.ItemByName[aObjectName]) then
  begin
    ShowMessage('Value not assigned: '+aObjectName);
    Exit;
  end;

//  Assert(Assigned(g_Obj.ItemByName[aObjectName]), 'Value not assigned: '+aObjectName);


  ot:=g_Obj.ItemByName[aObjectName].Type_;
  case ot of
    otProject:  Result:=GetImageIndexByName (ICON_PROJECT);
    otProperty: Result:=GetImageIndexByName (ICON_PROPERTY);


//    otTemplate_Site_mast,
    otSite,
    otPmpSite:     Result:=GetImageIndexByName (ICON_PMP_SITE);

    otPmpTerminal: Result:=GetImageIndexByName (ICON_PMP_TERMINAL);

    otCell,//, otCell_3G,
    otPMPSector:  Result:=GetImageIndexByName (ICON_CELL);

    otLink,
    otLinkFreqPlan_Link,
    otLinkLine_Link,
    otLinkNet_Link_Ref
          :     Result:=GetImageIndexByName (ICON_LINK);

    otLinkLine,
    otLinkNet_LinkLine_Ref
            :   Result:=GetImageIndexByName (ICON_LINKLINE);
    otLinkNet:  Result:=GetImageIndexByName (ICON_LINKNET);

    otTemplate_Site_Linkend,
    otLinkEnd:  Result:=GetImageIndexByName (ICON_LINKEND);
    
    otLinkFreqPlan: Result:=GetImageIndexByName (ICON_LINKFREQ);

    otMSC:      Result:=GetImageIndexByName (ICON_MSC);
    otBSC:      Result:=GetImageIndexByName (ICON_BSC);

    otAntennaType,
  //  otLinkendAntenna,

    otTemplate_Site_Linkend_ant,
    otCellAnt,
    otLinkEnd_Antenna: Result:=GetImageIndexByName (ICON_ANTENNA);

    otPmpCalcRegion: Result:=GetImageIndexByName (ICON_CALC_REGION);

  //  otCalcRegion_Site: Result:=GetImageIndexByName (ICON_SITE);
    otPMPCalcRegion_Site: Result:=GetImageIndexByName (ICON_PMP_SITE);

//    otAntenna:    Result:=GetImageIndexByName (ICON_ANTENNA);
  else
    Result:=-1;
  end;
end;



procedure TdmMain_res.DataModuleCreate(Sender: TObject);
begin
  LoadImages();
end;


function TdmMain_res.GetImageIndexByName(aName: string): integer;
var
  i: integer;
begin
  for i:=0 to High(IMAGE_NAMES) do
    if IMAGE_NAMES[i]=aName then begin
      Result:=i; Exit;
    end;
  Result:=-1;
end;

//============================================================================//
procedure TdmMain_res.LoadImages();
//============================================================================//
//  �������� ������ �� �������� � TImageList-�
//
//============================================================================//
var bmp : Graphics.TBitmap;
  i: integer;
//  sName: string;
begin
//  sName:=ExtractFileName(Application.ExeName);

  bmp := Graphics.TBitmap.Create;
  try
    imgs_Common.Clear;

    for i:=0 to High(IMAGE_NAMES) do begin
      bmp.LoadFromResourceName (HInstance, IMAGE_NAMES[i]);

      try
        imgs_Common.AddMasked (bmp, bmp.TransparentColor);
      except end;
    end;

  except end;

  FreeAndNil(bmp);
end;


begin
 
end.
