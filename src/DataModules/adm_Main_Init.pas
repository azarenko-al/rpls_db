unit adm_Main_Init;

interface
{$I ver.inc}

{.$DEFINE hasp}


uses Forms, Classes, SysUtils,

  u_log,
  u_files,

 // GSDock,
  {$IFDEF hasp}
  i_dll_Hasp,

//  u_Hasp,
  {$ENDIF}

  

  dm_Act_Project,

 // dm_Object_Manager,

  

  dm_Main,
  dm_Main_res,

  dm_Act_ColorSchema ,

  dm_Act_Folder,

  dm_Act_Company,
  dm_Act_Terminal,
  dm_Act_Trx,
  dm_Act_TransferTech,
  dm_act_AntType,
  dm_Act_Template_Linkend,
  dm_Act_Template_PMP_Site,
  dm_act_GeoRegion,
  dm_Act_MSC,
  dm_Act_BSC,
  dm_Act_Cell_Layer,
  dm_act_CalcModel,
  dm_act_Clutter,

  dm_Act_Combiner,
  dm_Act_Filter,

  dm_Act_MapFile,

  dm_Act_Profile,
  dm_Act_Explorer,

  dm_Act_Map,
  dm_act_Map_Engine,
  dm_Act_MapDesktop,

  dm_Act_Antenna,

  dm_Act_Status,

  dm_act_Rel_Engine,
  dm_Act_RelFile,
  dm_Act_Report,


  dm_act_Link,
  dm_Act_LinkEnd,
  dm_act_LinkEndType,
  dm_act_LinkFreqPlan,
  dm_Act_LinkFreqPlan_Link,
  dm_act_LinkLine,
  dm_act_LinkLine_Link,
  dm_act_LinkNet,
  dm_act_LinkNet_Link_Ref,
  dm_act_LinkNet_LinkLine_Ref,
  dm_act_LinkType,



  dm_Act_Pmp_Sector,
  dm_Act_Pmp_Site,
  dm_Act_PmpTerminal,
  dm_act_PMP_CalcRegion,
 // dm_Act_PMP_CalcRegion_Site,

  dm_Act_Property,

  I_Options,

  f_Main_MDI
  ;


type
  TdmMain_Init_link = class(TDataModule)

    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  public
    procedure InitDataModules;
  end;

var
  dmMain_Init_link: TdmMain_Init_link;


implementation
 {$R *.dfm}


// -------------------------------------------------------------------
procedure TdmMain_Init_link.DataModuleCreate(Sender: TObject);
// -------------------------------------------------------------------
(*var
  b: boolean;*)
begin
  g_Log.SetFileName(
    IncludeTrailingBackslash(GetTempFileDir())+'_rpls_log.txt');


//  g_Log:=TLog.Create(
 //   IncludeTrailingBackslash(GetTempFileDir())+'_rpls_log.txt');

//  g_EventManager:=TEventManager.Create;
//  g_ShellEvents:=TShellEventManager_.Create;

//  g_Obj:=TObjectEngine.Create;

  {$IFDEF hasp}
(*
  if not
       (hasp_CheckProg(PROG_HASP4_TIME_ONEPLAN_RPLS_DB_LINK) or
        hasp_CheckProg(PROG_HASP4_TIME_ONEPLAN_RPLS_LINK) or
        hasp_CheckProg(PROG_HASP_UNLIMITED_ONEPLAN_RPLS_DB_LINK) or
        hasp_CheckProg(PROG_HASP_UNLIMITED_ONEPLAN_RPLS_LINK)
        )
  then begin
    hasp_ErrorMsg;
    exit;
  end;*)

  {$ENDIF}



//  dm_Custom.init_UpdateADOConnections;
 // dx_Init (Application, Mouse);
//  ShowMessage('1');


//  gl_Reg:=TRegStore_Create(REGISTRY_ROOT);


//  IShellFactory_Init (dmMain);

//  dmMain: TdmMain;

//   Exit;


  TdmMain.Init;

  if not dmMain.OpenDB_ then
    Exit;


//  dmUserAuth.Load;



  InitDataModules();


//Exit;



  dmAct_Map_Engine.DebugMode:=False;


 //zzzzzzz dmObjects.Open;

  IOptionsX_Load;


//  dmOptions.SyncXrefFieldsWithOptions (otSite, dmMain.ADOConnection);


 // gl_Reg.
//  InitRegistry(REGISTRY_ROOT);


//  if not dmMain.LoginDlg(True) then
 //   Exit;


 ///////////////////////
//  InitGSDockManager;



  Application.CreateForm(Tfrm_Main_MDI, frm_Main_MDI);
  Application.ProcessMessages;

  dmAct_Project.LoadLastProject;

end;



procedure TdmMain_Init_link.InitDataModules;
var
  b: boolean;
begin

  TdmMain_res.Init;

  TdmObject_Manager.Init;


(*  TdmAct_Folder.Init;
  TdmAct_Project1.Init;
  TdmAct_Forms.Init;

*)

//  TdmAct_Company.Init;
  TdmAct_ColorSchema.Init;

  TdmAct_Combiner.Init;
  TdmAct_Company.Init;

  TdmAct_MSC.Init;

 // IMsc := nil;

 //  Exit;


  TdmAct_AntType.Init;

 // b:=dmAct_MSC.GetInterface(IMscX, IMsc);

  //

 // IMsc := nil;



  TdmAct_Terminal.Init;
  TdmAct_Trx.Init;
  TdmAct_TransferTech.Init;
  TdmAct_CalcModel.Init;

  TdmAct_Terminal.Init;
  TdmAct_Cell_Layer.Init;
  TdmAct_Clutter.Init;
  TdmAct_CalcModel.Init;

  TdmAct_Antenna.Init;
  TdmAct_Report.Init;

  TdmAct_RelFile.Init;

  TdmAct_Status.Init;

  TdmAct_Map.Init;
  TdmAct_MapDesktop.Init;

  TdmAct_Profile.Init;

  TdmAct_BSC.Init;


//  Tdmact_CalcModel.Init;
//  TdmAct_ColorSchema.Init;
//  TdmAct_AntType.Init;


  {$IFDEF GSM}
(*
  TdmAct_Site.Init;
  TdmAct_Cell.Init;

  TdmAct_CalcRegion.Init;

  TdmCalcRegion_calc_View.Init;
  TdmAct_CalcRegion_Site.Init;
*)


  {$ENDIF}

  //shell
  TdmAct_Explorer.Init;
  TdmAct_Filter.Init;
  TdmAct_Folder.Init;

  TdmAct_LinkEndType.Init;

  TdmAct_Link.Init;
  TdmAct_LinkEnd.Init;
  TdmAct_LinkFreqPlan.Init;
  TdmAct_LinkFreqPlan_Link.Init;
  TdmAct_LinkLine.Init;
  TdmAct_LinkLine_Link.Init;
  TdmAct_LinkNet.Init;
  TdmAct_LinkNet_Link_Ref.Init;
  TdmAct_LinkNet_LinkLine_Ref.Init;
  TdmAct_LinkType.Init;



 // TdmAct_DigitalPath.Init;

  TdmAct_PMP_Sector.Init;
  TdmAct_PMP_site.Init;
  TdmAct_PmpTerminal.Init;
  TdmAct_Pmp_CalcRegion.Init;
//  TdmAct_Pmp_CalcRegion_Site.Init;

//  TdmRel_Engine.Init;

  TdmAct_Rel_Engine.Init;
  TdmAct_Property.Init;

  TdmAct_LinkNet.Init;
  TdmAct_GeoRegion.Init;
  TdmAct_MapFile.Init;

  TdmAct_Template_Linkend.Init;
  TdmAct_Template_PMP_Site.Init;


  TdmAct_Project.Init;

end;


procedure TdmMain_Init_link.DataModuleDestroy(Sender: TObject);
//
//var
//  I: integer;
//  s: string;
//  s4: string;
//  s3: string;
//  s2: string;
//  s1: string;
begin

  IOptionsX_UnLoad();


//  Show_Application_DataModules;


  //I := Application.ComponentCount;

//
//  for I := Application.ComponentCount - 1  downto 0 do
//  //  if Application.Components[i] is TDataModule then
//    begin
//    //  ShowMessage(Application.Components[i].ClassName +'  '+ Application.Components[i].Name);
//
//      s1:=Application.Components[i].Name;
//      s2:=Application.Components[i].ClassName;
//
//      s3:=Application.Components[i].Owner.Name;
//      s4:=Application.Components[i].Owner.className;
//
//   //   (Application.Components[i]).Free;
//
//    //     ShowMessage(Application.Components[i].ClassName +'  '+ Application.Components[i].Name);
//
//    end;


(*  IOptions

  IOptions :=Get_IOptionsX();

  IOptions := nil;
*)

{
  for i:=0 to Application.ComponentCount-1 do
    s:=s+ Application.Components[i].ClassName + CRLF;


  ShowMessage(s);


   for i:=Application.ComponentCount-1 downto 0 do
     if Application.Components[i] <> Self then
     if Application.Components[i] is TDataModule then
     try
       s:=Application.Components[i].Name;
       Application.Components[i].Free;

     //  j:=Application.ComponentCount;

     except
     //  ShowMessage(Components[i].ClassName);
     //  Raise Exception.Create(Components[i].Name);
     end;}


//  FreeAndNil(g_ShellEvents);


//  FreeAndNil(g_Log);
//  FreeAndNil(g_EventManager);
//  FreeAndNil(g_Obj);
end;



end.

//
//
//(*
//
//
//
//procedure TdmMain_Init_link.TestFree;
//(*var
//  i : Integer;
//  s: string;*)
//begin
//{
//  for i:=0 to Application.ComponentCount-1 do
//    s:=s+ Application.Components[i].ClassName + CRLF;
//
//
//  ShowMessage(s);
//
//}
//{
//
//  while Application.ComponentCount>0  do
//  begin
//   // if Application.Components[0] is TDataModule then
//    s:=Application.Components[0].ClassName;
//    s:=Application.Components[0].Name;
//
//    Application.Components[0].Free;
//  end;
//
//}
//
//{
//   for i:=Application.ComponentCount-1 downto 0 do
//     if Application.Components[i] <> Self then
//       if Application.Components[i] is TDataModule then
//     try
//       s:=Application.Components[i].ClassName;
//       Application.Components[i].Free;
//     except
//       ShowMessage(Components[i].ClassName);
//     //  Raise Exception.Create(Components[i].Name);
//     end;
//}
//end;
//
//
//