unit dm_Main_Init;

interface

uses Forms, Classes,

  GSDock,

  u_const_db,
  u_const_msg,
  u_const,

  u_reg,
  u_func_msg,

  dm_Main,

  dm_Act_Project,

  dm_act_Cell,
  dm_act_Site,
  dm_act_Property,

  dm_Act_CalcMap,

  dm_act_Map_Engine,

  dm_Act_FreqPlan,

  dm_act_XConn,

  dm_act_AntType,
  dm_Act_Cell_Layer,

  dm_Act_ColorSchema,

  dm_Act_MSC,
  dm_Act_BSC,

  dm_act_LinkType,
  dm_act_LinkEndType,
  dm_Act_Terminal,

  dm_act_LinkLine,

  dm_act_Link,
  dm_Act_LinkEnd,

  //dm_act_Property,
 // dm_act_Site,
 // dm_act_CEll,

  dm_Act_MapFile,
  dm_Act_RelFile,

  dm_act_Rel_Engine,
  dm_Act_MapDesktop,

  dm_act_Explorer,
  dm_Act_Filter,
  dm_Act_Profile,

  dm_Act_CalcMOdel,

  dm_act_TrxType,
  dm_act_PMP_CalcRegion,

  dm_act_Traffic,
  dm_act_Clutter,

  dm_Act_Forms,

  f_Main_MDI
  ;


type
  TdmMain_Init = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);

  public
    { Public declarations }
  end;

var
  dmMain_Init: TTdmMain_Init;


//==================================================================
implementation {$R *.dfm}
//==================================================================

procedure TdmMain_Init.DataModuleCreate(Sender: TObject);
begin

  InitRegistry(REGISTRY_ROOT);

  dmMain.Open;

  InitGSDockManager;

  Application.CreateForm(Tfrm_Main_MDI, frm_Main_MDI);

  Application.ProcessMessages;

  dmAct_Project.LoadLastProject;
end;


procedure TdmMain_Init.DataModuleDestroy(Sender: TObject);
begin
  dmAct_Project.SaveLastProject;
end;                            


end.
