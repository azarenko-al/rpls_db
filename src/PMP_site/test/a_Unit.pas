unit a_Unit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,

  
  dm_Main,
  dm_Main,

  d_PMP_site_add,

  u_func, 

  dm_Act_Folder,
  dm_Act_Explorer,

  dm_Act_CellTemplate,
  dm_Act_SiteTemplate,

  u_Geo,
  u_db,
  u_reg,
  u_func_msg,
  u_dlg,

  u_types,
  u_const,
  u_const_msg,
  u_const_DB,
  u_const_str,


  fr_Explorer_Container,

  //fr_Explorer,


  dm_Act_Antenna,
  dm_Act_PMP_site,
  dm_Act_PMP_Sector,
  dm_Act_PMPTerminal,
  dm_Act_LinkEndType,

  dm_Act_Link,

  dm_Act_AntType, 
  dm_Act_Project,

  fr_Browser,
  f_Log,

  
  StdCtrls, rxPlacemnt, ComCtrls, ExtCtrls, ToolWin;

type
  TForm9 = class(TForm)
    FormPlacement1: TFormPlacement;
    PageControl1: TPageControl;
    ts_LinkLine: TTabSheet;
    pn_Browser: TPanel;
    ToolBar1: TToolBar;
    Splitter1: TSplitter;
    Button1: TButton;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    iID: integer;

    Fframe_Explorer : Tframe_Explorer_Container;

    Fframe_Browser : Tframe_Browser;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form9: TForm9;


implementation  {$R *.DFM}

uses dm_Act_Map_Engine;



procedure TForm9.FormCreate(Sender: TObject);
begin 
  gl_Reg:=TRegStore_Create(REGISTRY_ROOT);

 
  // -------------------------------------------------------------------
  TdmMain.Init;
//  IShellFactory_Init;// (dmMain);
  // -------------------------------------------------------------------



  dmMain.OpenDlg;

  TdmAct_Explorer.Init;
  TdmAct_Folder.Init;
  

 { InitRegistry1(REGISTRY_ROOT);

  Tfrm_Log.CreateForm();}

  dmAct_Map_Engine.Enabled:= False; 


  TdmAct_PMP_site.Init;
  TdmAct_PMP_Sector.Init;
  TdmAct_PMPTerminal.Init;

  TdmAct_LinkEndType.Init;
  TdmAct_Antenna.Init;



  TdmAct_CellTemplate.Init;
  TdmAct_SiteTemplate.Init;

  TdmAct_Project.Init;


//  dmMain.OpenDefault ;

  CreateChildForm(Tframe_Explorer_Container, Fframe_Explorer, ts_LinkLine);
 // Fframe_Explorer := Tframe_Explorer_Container.CreateChildForm ( ts_LinkLine);
  Fframe_Explorer.SetViewObjects([otPmpSite,otSiteTemplate]);
  Fframe_Explorer.RegPath:='LinkLine';


  CreateChildForm(Tframe_Browser, Fframe_Browser, pn_Browser);

//  Fframe_Browser := Tframe_Browser.CreateChildForm( pn_Browser);

  Fframe_Explorer.OnNodeFocused:=Fframe_Browser.ViewObjectByPIDL;

////////////////////

  dmAct_Project.LoadLastProject;

  pn_Browser.Align:=alClient;


  Tdlg_PMP_site_add.ExecDlg (0,0,NULL_POINT);


end;


procedure TForm9.FormDestroy(Sender: TObject);
begin

//  Fframe_Explorer.Free;
//  Fframe_Browser.Free;

end;

procedure TForm9.Button1Click(Sender: TObject);
begin
//  dmAct_Project.Browse;

end;

procedure TForm9.Button2Click(Sender: TObject);
begin
//////  dmPMP_export_calc.SaveToXMLFile (aPmpID: integer; aFileName: string);

end;



end.
