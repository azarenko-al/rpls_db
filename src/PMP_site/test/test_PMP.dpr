program test_PMP;

{%File '..\..\ver.inc'}

uses
  ShareMem,
  Forms,
  a_Unit in 'a_Unit.pas' {Form9},
  d_Antenna_add in '..\..\Antenna\d_Antenna_add.pas' {dlg_Antenna_add},
  d_LinkEnd_add in '..\..\LinkEnd\d_LinkEnd_add.pas' {dlg_LinkEnd_add},
  d_PMP_site_add in '..\d_PMP_site_add.pas' {dlg_PMP_site_add},
  d_PmpLink_add in '..\..\Link\d_PmpLink_add.pas' {dlg_PmpLink_add},
  dm_act_Antenna in '..\..\Antenna\dm_act_Antenna.pas' {dmAct_Antenna: TDataModule},
  dm_act_PMP_site in '..\dm_act_PMP_site.pas' {dmAct_PMP_site: TDataModule},
  dm_BSC in '..\..\BSC\dm_BSC.pas' {dmBSC: TDataModule},
  dm_CalcRegion in '..\..\CalcRegion\dm_CalcRegion.pas' {dmCalcRegion_pmp: TDataModule},
  dm_Cell in '..\..\Cell\dm_Cell.pas' {dmCell: TDataModule},
  dm_Custom in '..\..\.common\dm_Custom.pas' {dmCustom: TDataModule},
  dm_Folder in '..\..\Folder\dm_Folder.pas' {dmFolder: TDataModule},
  dm_Link_tools in '..\..\Link\dm_Link_tools.pas' {dmLink_tools: TDataModule},
  dm_MSC in '..\..\MSC\dm_MSC.pas' {dmMSC: TDataModule},
  dm_Object_base in '..\..\Common\dm_Object_base.pas' {dmObject_base: TDataModule},
  dm_PMP_site in '..\dm_PMP_site.pas' {dmPmpSite: TDataModule},
  dm_PMP_site_View in '..\dm_PMP_site_View.pas' {dmPMP_site_View: TDataModule},
  fr_Antenna_inspector in '..\..\Antenna\fr_Antenna_inspector.pas' {frame_Antenna_inspector},
  fr_LinkEnd_inspector in '..\..\LinkEnd\fr_LinkEnd_inspector.pas' {frame_LinkEnd_inspector},
  fr_PMP_site_view in '..\fr_PMP_site_view.pas' {frame_PMP_site_View},
  fr_PMP_inspector in '..\fr_PMP_inspector.pas' {frame_PMP_inspector},
  fr_PMP_Site_Sectors in '..\fr_PMP_Site_Sectors.pas' {frame_PMP_Site_Sector},
  I_Main in '..\..\DataModules\I_Main.pas',
  I_LinkEndType in '..\..\LinkEndType\I_LinkEndType.pas',
  dm_Template_PMP_Site in '..\..\SiteTemplate\dm_Template_PMP_Site.pas' {dmTemplate_PMP_Site: TDataModule};

{$R *.RES}




begin
  Application.Initialize;
  Application.CreateForm(TForm9, Form9);
  Application.Run;
end.
