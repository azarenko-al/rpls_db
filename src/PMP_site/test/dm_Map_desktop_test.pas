unit dm_Map_desktop_test;

interface

uses
  SysUtils, Classes, Forms,

//  dm_Pmp_site,

  dm_Onega_DB_data,

  dm_Map_Desktop,
//  dm_ColorSchema,

  u_db,
  u_const_db,

  u_func,

  dm_Project,

  I_db_login,


  dm_Main, DB, ADODB, dm_Base
  ;
          //    TOnProgressEvent = procedure (aProgress, aMax: Integer) of object


type
  TdmMap_desktop_test = class(TdmBase)
    ADOStoredProc_MapDesktops: TADOStoredProc;
  private
    { Private declarations }
  public
    class procedure Init;
    procedure Test;

  //  property OnProgress: TOnProgressEvent read FOnProgress write FOnProgress;

  end;

var
  dmMap_desktop_test: TdmMap_desktop_test;

implementation

{$R *.dfm}

// ---------------------------------------------------------------
procedure TdmMap_desktop_test.Test;
// ---------------------------------------------------------------
var
  iID: Integer;
  iID_new: Integer;
  iPROJECT_ID: Integer;
  k: Integer;

  //rec: TdmPMP_Site_Add_rec;

begin
  k:=dmOnega_DB_data.MapDesktop_Select(ADOStoredProc_MapDesktops, dmMain.ProjectID);

//  dmOnega_DB_data.OpenQuery(ADOQuery1,
//      'select  * from '+ view_Map_Desktop );
//

  with ADOStoredProc_MapDesktops do
    while not EOF do
  begin
    iID := FieldByName(FLD_ID).AsInteger;
    iPROJECT_ID := FieldByName(FLD_PROJECT_ID).AsInteger;

    ShowProgress(RecNo, ADOQuery1.RecordCount);


    dmOnega_DB_data.MapDesktop_Select(ADOStoredProc1, iPROJECT_ID);

    dmOnega_DB_data.MapDesktop_Select_CalcMaps(ADOStoredProc1, iID);
    dmOnega_DB_data.MapDesktop_Select_GeoMaps(ADOStoredProc1, iID);

    k:=dmOnega_DB_data.MapDesktop_Select_ObjectMaps(ADOStoredProc1, iID);

   // FillChar(rec,SizeOf(rec),0);

//
//
//    rec.NewName:='test_' + IntToStr(Random(1000000));
//    rec.Property_ID:=  FieldByName(FLD_Property_ID).AsInteger;
//    rec.CalcRadius_km:=  32;
////    rec.:=  32;
//

//
//    iID_new:=dmOnega_DB_data.Pmp_site_Add(ADOStoredProc1,
//      rec.Property_ID,
//      rec.NewName,
//      rec.CalcRadius_km
//      );
//
//
//    dmOnega_DB_data.Pmp_Site_Select_Item(ADOStoredProc1, iID);
//    dmOnega_DB_data.Pmp_Site_GetSubItemList(ADOStoredProc1, iID);
//
//    dmOnega_DB_data.Pmp_Site_Select_Item(ADOStoredProc1, iID_new);
//    dmOnega_DB_data.Pmp_Site_GetSubItemList(ADOStoredProc1, iID_new);
//
//
//    dmOnega_DB_data.Pmp_Site_del(iID_new);

    Next;
  end;
end;


// ---------------------------------------------------------------
class procedure TdmMap_desktop_test.Init;
// ---------------------------------------------------------------
begin
  Application.CreateForm(TdmMap_desktop_test, dmMap_desktop_test);

end;


begin
  TdmMap_desktop_test.Init;

end.


