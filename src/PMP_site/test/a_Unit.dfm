object Form9: TForm9
  Left = 500
  Top = 316
  Width = 667
  Height = 347
  Caption = 'Form9'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 197
    Top = 53
    Height = 267
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 53
    Width = 197
    Height = 267
    ActivePage = ts_LinkLine
    Align = alLeft
    TabOrder = 0
    object ts_LinkLine: TTabSheet
      Caption = 'ts_LinkLine'
    end
  end
  object pn_Browser: TPanel
    Left = 264
    Top = 53
    Width = 395
    Height = 267
    Align = alRight
    Caption = 'pn_Browser'
    TabOrder = 1
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 659
    Height = 53
    Caption = 'ToolBar1'
    TabOrder = 2
    object Button1: TButton
      Left = 0
      Top = 2
      Width = 75
      Height = 22
      Caption = 'Button1'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 75
      Top = 2
      Width = 75
      Height = 22
      Caption = 'Button2'
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object FormPlacement1: TFormPlacement
    IniFileName = 'Software\Onega\'
    Left = 324
  end
end
