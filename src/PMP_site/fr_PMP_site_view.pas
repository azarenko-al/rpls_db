unit fr_PMP_site_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, rxPlacemnt, ActnList, Menus,  cxControls, cxSplitter,
  ToolWin, Grids, DBGrids,     
  ComCtrls, ExtCtrls,  ImgList,

  
  dm_User_Security,    

  fr_PMP_inspector,
  fr_PMP_Site_Sectors,

  //dm_CalcRegion_site,
  dm_CalcRegion_pmp_site,

  u_const_db,


  u_func,
  u_reg,

  u_types,

  fr_View_base,


  cxPropertiesStore, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinsDefaultPainters, dxBar, cxBarEditItem, cxClasses
  ;


type
  Tframe_PMP_site_View = class(Tframe_View_Base)
    pn_Inspector: TPanel;
    pn_Cells: TPanel;
    cxSplitter1: TcxSplitter;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    Fframe_inspector: Tframe_PMP_inspector;
    Ffrm_PMP_Site_Sector: Tframe_PMP_Site_Sector;

  public
    ViewMode: (vmSite,vmPmpCalcRegionSite);

    procedure View (aID: integer; aGUID: string); override;
  end;


//====================================================================
//====================================================================

implementation

{$R *.dfm}


//--------------------------------------------------------------------
procedure Tframe_PMP_site_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_PMP_SITE;

  TableName:=TBL_PMP_SITE;

  pn_Inspector.Align:=alClient;

  CreateChildForm(Tframe_PMP_inspector,   Fframe_inspector,     pn_Inspector);
  CreateChildForm(Tframe_PMP_Site_Sector, Ffrm_PMP_Site_Sector, pn_Cells);

  AddComponentProp(pn_Cells, PROP_HEIGHT);
  cxPropertiesStore.RestoreFrom;


end;


procedure Tframe_PMP_site_View.FormDestroy(Sender: TObject);
begin
  FreeAndNil(Fframe_inspector);
  FreeAndNil(Ffrm_PMP_Site_Sector);
  inherited;
end;


//--------------------------------------------------------------------
procedure Tframe_PMP_site_View.View(aID: integer; aGUID: string);
//--------------------------------------------------------------------
//var
//  bReadOnly: Boolean;
 // iGeoRegion_ID: Integer;
begin
//  inherited View(aID);

  if ViewMode=vmPmpCalcRegionSite then
    aID:=dmCalcRegion_pmp_site.GetSiteID (aID); //, OBJ_PMP_SITE);
//    aID:=dmCalcRegion_site.GetSiteID (aID); //, OBJ_PMP_SITE);

  inherited;


  if not RecordExists (aID) then
    Exit;



  Fframe_inspector.View(aID);

(*
  iGeoRegion_ID := Fframe_inspector.GetIntFieldValue(FLD_GeoRegion_ID);

  ShowMessage(IntToStr(iGeoRegion_ID));
*)

  // -------------------------
//  FGeoRegion_ID := Fframe_inspector.GetDataset().FieldByName(FLD_GeoRegion_ID).AsInteger;// GetIntFieldValue(FLD_GeoRegion_ID);
//  bReadOnly:=dmUser_Security.GeoRegion_is_ReadOnly(FGeoRegion_ID);

//  Fframe_Inspector.SetReadOnly(bReadOnly, IntToStr(FGeoRegion_ID));
//  Ffrm_PMP_Site_Sector.SetReadOnly(bReadOnly);
  // -------------------------


  Ffrm_PMP_Site_Sector.View(aID);
end;


end.