inherited dlg_PMP_site_add: Tdlg_PMP_site_add
  Left = 914
  Top = 303
  Width = 526
  Height = 543
  Caption = 'dlg_PMP_site_add'
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 480
    Width = 518
    inherited Bevel1: TBevel
      Width = 518
    end
    inherited Panel3: TPanel
      Left = 334
      inherited btn_Ok: TButton
        Left = 355
      end
      inherited btn_Cancel: TButton
        Left = 439
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 518
    inherited Bevel2: TBevel
      Width = 518
    end
    inherited pn_Header: TPanel
      Width = 518
      Height = 55
    end
  end
  inherited Panel1: TPanel
    Width = 518
    inherited ed_Name_: TEdit
      Width = 507
    end
  end
  inherited Panel2: TPanel
    Width = 518
    Height = 290
    inherited PageControl1: TPageControl
      Width = 508
      Height = 244
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 500
          Height = 213
          Align = alClient
          LookAndFeel.Kind = lfUltraFlat
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 215
          OptionsView.ValueWidth = 50
          TabOrder = 0
          Version = 1
          object row_CalcRadius_km: TcxEditorRow
            Expanded = False
            Properties.Caption = 'CalcRadius km'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'Float'
            Properties.Value = Null
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object cxVerticalGrid1CategoryRow2: TcxCategoryRow
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
          object row_Property_type: TcxEditorRow
            Properties.Caption = #1055#1083#1086#1097#1072#1076#1082#1072' '#1085#1086#1074#1072#1103'/'#1089#1091#1097#1077#1089#1090#1074#1091#1102#1097#1072#1103
            Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
            Properties.EditProperties.DropDownListStyle = lsFixedList
            Properties.EditProperties.ImmediatePost = True
            Properties.EditProperties.Items.Strings = (
              #1053#1086#1074#1072#1103
              'C'#1091#1097#1077#1089#1090#1074#1091#1102#1097#1072#1103)
            Properties.EditProperties.OnCloseUp = row_Property_typeEditPropertiesCloseUp
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 2
            ParentID = -1
            Index = 2
            Version = 1
          end
          object row_Property_: TcxEditorRow
            Properties.Caption = #1053#1072#1079#1074#1072#1085#1080#1077
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = row_PropertyEditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 3
            ParentID = 2
            Index = 0
            Version = 1
          end
          object cxVerticalGrid1CategoryRow1: TcxCategoryRow
            ID = 4
            ParentID = -1
            Index = 3
            Version = 1
          end
          object row_is_template: TcxEditorRow
            Properties.Caption = 'row_is_template'
            Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
            Properties.EditProperties.Alignment = taLeftJustify
            Properties.EditProperties.NullStyle = nssUnchecked
            Properties.EditProperties.ReadOnly = False
            Properties.EditProperties.ValueChecked = 'True'
            Properties.EditProperties.ValueGrayed = ''
            Properties.EditProperties.ValueUnchecked = 'False'
            Properties.DataBinding.ValueType = 'Boolean'
            Properties.Value = Null
            ID = 5
            ParentID = -1
            Index = 4
            Version = 1
          end
          object row_site_template: TcxEditorRow
            Properties.Caption = 'row_site_template'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = row_PropertyEditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 6
            ParentID = 5
            Index = 0
            Version = 1
          end
        end
      end
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList2Update
    inherited act_Ok: TAction
      OnExecute = _ActionMenu
    end
    inherited act_Cancel: TAction
      OnExecute = _ActionMenu
    end
  end
end
