unit dm_PMP_site;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Variants,
  Db, ADODB, dxmdaset,

  dm_Property,


  dm_Onega_DB_data,

  dm_Object_base,

  dm_Main,

  dm_Antenna,
  dm_Pmp_Sector,


  u_types,
  u_const_str,
  u_const_db,

  u_Geo,
  u_db,
  u_func,
  u_classes
 , Mxarrays;


type
  //-------------------------------------------------------------------
  TdmPMP_Site_Add_rec = record
  //-------------------------------------------------------------------
    NewName         : string;

   	Property_ID     : Integer;
    CalcRadius_km   : double;

  //  IsAddProperty : Boolean;
    PropertyName    : string;
    Point           : TBLPoint;

    Template_Pmp_Site_ID: integer;

  end;



  TdmPmpSite = class(TdmObject_base)
    qry_PMP_Sector: TADOQuery;
    qry_Antennas: TADOQuery;
    qry_Links: TADOQuery;
    qry_Temp: TADOQuery;
    ADOStoredProc_Add: TADOStoredProc;
    ADOStoredProc2: TADOStoredProc;
    procedure DataModuleCreate(Sender: TObject);
  private
//    function GetPmpSectorCount11111111111(aID: integer): integer;
  //  procedure GetSectorIDList1111111111(aID: integer; aSectorIDList: TIDList);
// TODO: GetMaxHeight
//  function GetMaxHeight(aSiteID: integer): double;
  public
    function Add (aRec: TdmPMP_Site_Add_rec): integer;

    function Del (aID: integer): boolean; override;
//    function GetAntIDArray1(aID: integer): TIntArray;

    procedure GetSubItemList(aID: integer; aItemList: TIDList);

    function GetNearestID(aPropID: integer): integer;

    function GetMax_CalcRadius_M(aPMPID: integer): integer;

    procedure GetBLRect_for_PMP(aPmpSiteIDList: TIDList; var aBLPoints: TBLPointArrayF);


  end;

function dmPmpSite: TdmPmpSite;

//====================================================================
// implementation
//====================================================================
implementation  {$R *.dfm}



var
  FdmPmpSite: TdmPmpSite;


// ---------------------------------------------------------------
function dmPmpSite: TdmPmpSite;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmPmpSite) then
      FdmPmpSite := TdmPmpSite.Create(Application);

  Result := FdmPmpSite;
end;

//-------------------------------------------------------------------
procedure TdmPmpSite.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  TableName:=  TBL_PMP_SITE;
 // DisplayName:=STR_PMP_SITE;
  ObjectName:= OBJ_PMP_SITE;

end;

//-------------------------------------------------------------------
function TdmPmpSite.Del (aID: integer): boolean;
//-------------------------------------------------------------------
var
  i : Integer;
begin
  i:= dmOnega_DB_data.Pmp_Site_del(aID);
  Result:=i > 0;
//  gl_DB.ExecSP (sp_PmpSite_del, [db_Par(FLD_ID, aID)]);

end;



//-------------------------------------------------------------------
function TdmPmpSite.GetNearestID(aPropID: integer): integer;
//-------------------------------------------------------------------
{
const
  SQL_SELECT_NEAREST_PROPERTY =
    'SELECT  id,name,lat,lon, SQRT((lat-:lat)*(lat-:lat)+(lon-:lon)*(lon-:lon)) AS dist '+
    ' FROM '+ TBL_PROPERTY +
    ' WHERE project_id=:project_id '+
    ' ORDER BY dist';
var
  oBlPoint: TBlPoint;
  iNearestPmpSite: integer;
}
begin
  //sp_PmP_site_GetNearest

  Result := dmOnega_DB_data.ExecStoredProc_('sp_PmP_site_GetNearest',
              [
              // db_Par(FLD_ProJECT_id, dmMain.ProjectID ),
               FLD_PROPERTY_ID, aPropID 

            //   db_Par(FLD_LAT, aLAT ),
             //  db_Par(FLD_LON, aLON )
              ]);


{

  end;
}

end;


//-------------------------------------------------------------------
procedure TdmPmpSite.GetBLRect_for_PMP(aPmpSiteIDList: TIDList; var aBLPoints:
    TBLPointArrayF);
//-------------------------------------------------------------------
var
  blRect: TBLRect;
  blRects: TBLRectArray;
  blPoint: TBLPoint;
  i, j, iCalcPMP_Count: integer;
begin
  j:=0;
  iCalcPMP_Count:=0;




  for i:=0 to aPmpSiteIDList.Count-1 do
 //   if gl_db.RecordExists(TBL_PMP_SECTOR, [db_par(FLD_PMP_SITE_ID, aPmpSiteIDList[i].ID)]) then
      Inc(iCalcPMP_Count);

  if iCalcPMP_Count=0 then
    Exit;

  SetLength(blRects, iCalcPMP_Count);

  for i:=0 to aPmpSiteIDList.Count-1 do
 //   if gl_db.RecordExists (TBL_PMP_SECTOR, [db_par(FLD_PMP_SITE_ID, aPmpSiteIDList[i].ID)]) then
  begin
    blPoint:= GetPropertyPos (aPmpSiteIDList[i].ID);
    blRect := geo_GetSquareAroundPoint (blPoint, GetMax_CalcRadius_M (aPmpSiteIDList[i].ID));
    blRects[j]:= blRect;
    Inc(j);
  end;

  geo_BLRectToBLPointsF (geo_GetRoundBLRect (blRects), aBLPoints);


end;

//--------------------------------------------------------------------
function TdmPmpSite.GetMax_CalcRadius_M(aPMPID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=0;


  db_OpenQuery (qry_Temp,
       'SELECT * FROM '+ view_PMP_SECTOR +' WHERE (pmp_site_id = :pmp_site_id)',
       [db_Par(FLD_PMP_SITE_ID, aPmpID)]);

  with qry_Temp do
    while not EOF do
  begin
    if FieldByName(FLD_CALC_RADIUS_KM).AsInteger>Result then
      Result:= FieldByName(FLD_CALC_RADIUS_KM).AsInteger;
    Next;
  end;


  if Result=0 then Result:= 35000
              else Result:= Result*1000;

end;

//--------------------------------------------------------------------
function TdmPmpSite.Add (aRec: TdmPMP_Site_Add_rec): integer;
//--------------------------------------------------------------------
begin

  Result := dmOnega_DB_data.ExecStoredProc_ ('sp_Pmp_site_Add',
          [
           FLD_PROJECT_ID,       dmMain.ProjectID,

           FLD_Property_ID,      aRec.Property_ID,
           FLD_Name,             aRec.NewName,
           FLD_CALC_RADIUS_KM,   aRec.CalcRadius_km,
           FLD_Template_pmp_Site_ID, aRec.Template_pmp_Site_ID,

//           'Is_new_', aRec.IsAddProperty,
           'Property_name', IIF_NULL(aRec.PropertyName) ,

           'lat', aRec.Point.B,
           'lon', aRec.Point.L

//    IsAddProperty : Boolean;
///    Point         : TBLPoint;


           ]);
      {
  @PROJECT_ID 	    int=null,   
  @PROPERTY_NAME 	varchar(100)=null,
  @LAT 				float=null,
  @LON 				float=null,
  @LAT_WGS			float=null,
  @LON_WGS			float=null,
  }

end;

//-------------------------------------------------------------------
procedure TdmPmpSite.GetSubItemList(aID: integer; aItemList: TIDList);
//-------------------------------------------------------------------
begin
  dmOnega_DB_data.Pmp_Site_GetSubItemList(ADOStoredProc2, aID);

//  db_OpenQuery (qry_Temp, 'Exec '+ sp_PmpSite_GetSubItemList + Format(' %d', [aID]));

  with ADOStoredProc2 do
    while not Eof do
    begin
      aItemList.AddObjNameAndGUID1 (
        FieldValues[FLD_ID], FieldValues[FLD_OBJNAME], FieldValues[FLD_GUID]);

      Next;
    end;

end;


begin

end.
