inherited frame_PMP_site_View: Tframe_PMP_site_View
  Left = 980
  Top = 301
  Width = 474
  Height = 552
  Caption = 'frame_PMP_site_View'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 466
    Height = 56
  end
  inherited pn_Caption: TPanel
    Top = 81
    Width = 466
  end
  inherited pn_Main: TPanel
    Top = 98
    Width = 466
    Height = 279
    object pn_Inspector: TPanel
      Left = 1
      Top = 1
      Width = 464
      Height = 136
      Align = alTop
      BevelOuter = bvLowered
      Caption = 'pn_Inspector'
      TabOrder = 0
    end
    object pn_Cells: TPanel
      Left = 1
      Top = 194
      Width = 464
      Height = 84
      Align = alBottom
      BevelOuter = bvLowered
      Caption = 'pn_Cells'
      TabOrder = 1
    end
    object cxSplitter1: TcxSplitter
      Left = 1
      Top = 186
      Width = 464
      Height = 8
      HotZoneClassName = 'TcxSimpleStyle'
      AlignSplitter = salBottom
      Control = pn_Cells
    end
  end
  inherited MainActionList: TActionList
    Left = 20
    Top = 4
  end
  inherited ImageList1: TImageList
    Left = 52
    Top = 4
  end
  inherited PopupMenu1: TPopupMenu
    Left = 84
    Top = 4
  end
  inherited cxLookAndFeelController1: TcxLookAndFeelController
    Top = 472
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    Top = 424
    DockControlHeights = (
      0
      0
      25
      0)
  end
end
