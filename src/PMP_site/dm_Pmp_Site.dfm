inherited dmPmpSite: TdmPmpSite
  Left = 814
  Top = 471
  Height = 275
  Width = 515
  inherited ADOStoredProc1: TADOStoredProc
    Left = 304
    Top = 48
  end
  object qry_PMP_Sector: TADOQuery
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'id'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 5
      end>
    SQL.Strings = (
      'select   id,name'
      ' from property'
      'where id=:id')
    Left = 40
    Top = 160
  end
  object qry_Antennas: TADOQuery
    MarshalOptions = moMarshalModifiedOnly
    EnableBCD = False
    Parameters = <>
    Left = 124
    Top = 160
  end
  object qry_Links: TADOQuery
    MarshalOptions = moMarshalModifiedOnly
    EnableBCD = False
    Parameters = <>
    Left = 208
    Top = 156
  end
  object qry_Temp: TADOQuery
    LockType = ltReadOnly
    Parameters = <>
    Left = 311
    Top = 156
  end
  object ADOStoredProc_Add: TADOStoredProc
    Parameters = <>
    Left = 120
    Top = 40
  end
  object ADOStoredProc2: TADOStoredProc
    Parameters = <>
    Left = 432
    Top = 104
  end
end
