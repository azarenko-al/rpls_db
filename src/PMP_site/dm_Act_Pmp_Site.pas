unit dm_act_PMP_site;

interface

uses
  Classes, Menus, Forms, ActnList, 

  i_Audit,

  u_shell_var,

  dm_MapEngine_store,

  dm_Act_Explorer,

  dm_act_Base,

//  d_Audit,

  I_Object,

 // I_Act_Pmp_Sector, //dm_Act_Pmp_Sector,
 // I_Act_Pmp_Site,   //dm_Act_Pmp_Site,
               

  dm_Folder,
  
  u_GEO,

  u_func,
  u_types,
  u_const_str,
  u_const_db,
   

  dm_PMP_site,

  d_Object_dependence,

  fr_PMP_site_view,
  d_PMP_site_add

  ;

type
  TdmAct_PMP_site = class(TdmAct_Base) 
    act_PMPSector_Add: TAction;
    act_Object_dependence: TAction;
    ActionList2: TActionList;
    act_Audit: TAction;
    act_Template_apply: TAction;
 //   procedure DataModuleDestroy(Sender: TObject);
//    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  //  procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);

  private
    FBLPoint: TBLPoint;
    FPropertyID: integer;

    procedure DoAction (Sender: TObject);
//    procedure DoAfterItemDel(aID: integer);

    procedure Add (aPropertyID: integer);
    procedure ShowOnMap (aID: integer);

  protected
    function GetExplorerPath(aID: integer; aGUID: string): string;

    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm  (aOwnerForm: TForm): TForm; override;
    function ViewInProjectTree(aID: integer): TStrArray; override;
//    procedure DoAction (Sender: TObject);

    function ItemDel(aID: integer ; aName: string = ''): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;

  public
    procedure AddByPos(aBLPoint: TBLPoint);

    procedure AddByProperty(aProperty_ID: integer);
    class procedure Init;
  end;


var
  dmAct_PMP_site: TdmAct_PMP_site;

//==================================================================
// implementation
//==================================================================

implementation
{$R *.dfm}
uses
  dm_Act_Pmp_Sector, dm_Onega_db_data, dm_Main;




//--------------------------------------------------------------------
class procedure TdmAct_PMP_site.Init;
//--------------------------------------------------------------------
begin
  Assert(not Assigned(dmAct_PMP_site));

  Application.CreateForm(TdmAct_PMP_site, dmAct_PMP_site);

//  dmAct_PMP_site.GetInterface(IAct_PMP_site_X, IAct_PMP_site);
//  Assert(Assigned(IAct_PMP_site));

end;

//
//procedure TdmAct_PMP_site.DataModuleDestroy(Sender: TObject);
//begin
////  IAct_PMP_site := nil;
////  dmAct_PMP_site:= nil;
//
//  inherited;
//end;


//--------------------------------------------------------------------
procedure TdmAct_PMP_site.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;


  ObjectName:=OBJ_PMP_site;

//  act_Move.Caption:=STR_MOVE;
  act_PMPSector_Add.Caption:=STR_ADD_CELL;

  act_Object_dependence.Caption:=S_OBJECT_DEPENDENCE;

  act_Add.Caption:='������� ��� �������';

 // act_Audit.Caption:=STR_Audit;
//  act_Audit.Enabled:=False;
  

  act_Template_apply.Caption:='��������� ������';

  act_Audit.Caption:=STR_Audit;


  SetActionsExecuteProc ([
                          act_Object_ShowOnMap,{ act_Edit,}
                          act_PMPSector_Add,
                          act_Object_dependence,

                          act_Template_apply,

                          act_Audit

                          ],
                          DoAction);


  SetCheckedActionsArr([
        act_PMPSector_Add
     ]);

end;




//--------------------------------------------------------------------
procedure TdmAct_PMP_site.ShowOnMap (aID: integer);
//--------------------------------------------------------------------
var
  bl: TBLPoint;
begin
  bl:=dmPmpSite.GetPropertyPos (aID);
  ShowPointOnMap (bl, aID);
end;


//--------------------------------------------------------------------
procedure TdmAct_PMP_site.AddByPos(aBLPoint: TBLPoint);
//--------------------------------------------------------------------
begin
  FBLPoint:=aBLPoint;
  FPropertyID:=0;
  inherited Add (0);
end;


//--------------------------------------------------------------------
procedure TdmAct_PMP_site.AddByProperty(aProperty_ID: integer);
//--------------------------------------------------------------------
begin
  FBLPoint:=NULL_BLPOINT;
  FPropertyID:=aProperty_ID;
  inherited Add (0);
end;



//--------------------------------------------------------------------
procedure TdmAct_PMP_site.Add (aPropertyID: integer);
//--------------------------------------------------------------------
begin
  FBLPoint:=NULL_POINT;
  FPropertyID:=aPropertyID;
  inherited Add (aPropertyID);
end;

//--------------------------------------------------------------------
function TdmAct_PMP_site.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_PMP_site_add.ExecDlg (FPropertyID, FBLPoint) ;

end;

//--------------------------------------------------------------------
function TdmAct_PMP_site.ItemDel(aID: integer ; aName: string = ''): boolean;
//--------------------------------------------------------------------
var
  i: integer;
begin
//  oIDList:=TIDList.Create;

 // FDeletedIDList.AddObjName(aID, OBJ_PMP_SITE);

  dmPmpSite.GetSubItemList(aID, FDeletedIDList);//  oIDList);
  Result:= dmPmpSite.Del (aID);

  if Result then
  begin
    dmMapEngine_store.Feature_Del (OBJ_Pmp_Site, aID);

///    PostEvent (WE_MAP_ENGINE_DEL_OBJECT, Integer(otCalcRegion), aID);

  //  dmAct_Map.MapRefresh;
  end;


//  intArr:=oIDList.MakeIntArray;
 // intArr: TIntArray;
//  oIDList: TIDList;


 // FDeletedIDList.DlgShow;


{
  if Result then
  begin

    dmMapEngine1.DelList(oIDList);

  	dmAct_Map.MapRefresh;

  end;

  oIDList.Free;

}
end;



//--------------------------------------------------------------------
function TdmAct_PMP_site.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_PMP_site_View.Create(aOwnerForm);
end;

//--------------------------------------------------------------------
procedure TdmAct_PMP_site.GetPopupMenu;
//--------------------------------------------------------------------
begin
  //CheckActionsEnable();

  case aMenuType of
    mtFolder: begin
                AddFolderMenu_Create (aPopupMenu);
                AddFolderPopupMenu (aPopupMenu);
              end;
    mtItem:   begin
                AddMenuItem (aPopupMenu, act_Object_ShowOnMap);
                AddMenuItem (aPopupMenu, nil);

                AddMenuItem (aPopupMenu, act_PMPSector_Add);
                AddMenuItem (aPopupMenu, act_Object_dependence);
                AddMenuItem (aPopupMenu, nil);

                AddMenuItem (aPopupMenu, act_Del_);
                AddFolderMenu_Tools (aPopupMenu);



                AddMenuItem (aPopupMenu, act_Template_apply);
                AddMenuItem (aPopupMenu, act_Audit);


              end;
    mtList:   begin
                AddMenuItem (aPopupMenu, act_Del_list);
                AddFolderMenu_Tools (aPopupMenu);

                AddMenuItem (aPopupMenu, act_Template_apply);

              end;
  end;
end;


//--------------------------------------------------------------------
procedure TdmAct_PMP_site.DoAction (Sender: TObject);
//--------------------------------------------------------------------
var
  i: Integer;
  iTemplateID: Integer;
   sName: WideString;

begin
  if Sender=act_Audit then
    Init_IAuditX.ExecDlg( dmMain.GetConnectionString, TBL_PMP_Site, FFocusedID) else



{  if Sender=act_Audit then
    Dlg_Audit (TBL_PMP_site, FFocusedID)
  else
}
  if Sender = act_Object_dependence then
    Tdlg_Object_dependence.ExecDlg(OBJ_PMP_SITE, FFocusedName, FFocusedID)
  else

  //---------------------------------------
  if Sender=act_Template_apply then begin
  //---------------------------------------

    if dmAct_Explorer.Dlg_Select_Object(otTemplatePmpSite, iTemplateID, sName) then
      for i:=0 to FSelectedIDList.Count-1 do
        dmOnega_DB_data.Template_PMP_Site_Copy_to_PMP_Site(FSelectedIDList[i].ID, iTemplateID);


//   for i:=0 to FSelectedIDList.Count-1 do
 //     Calc (FSelectedIDList[i].ID );

//    dmAct_LinkLine.Dlg_Add (FSelectedIDList);
  end else



//            if dmAct_Explorer.Dlg_Select_Object (otCalcMOdel, iCalcMOdelID, sName) then
//            begin
//              dmOnega_DB_data.Object_Update_CalcModel(OBJ_Template_LinkEnd, iID, iCalcMOdelID);



  if Sender=act_Object_ShowOnMap then
    ShowOnMap (FFocusedID) else

  if Sender=act_PMPSector_Add  then
   // if Assigned(IPmpSector) then
    dmAct_Pmp_Sector.Dlg_Add(FFocusedID);

//    PostWMsg (WM_PMPSECTOR_ADD, FFocusedID);

end;

//--------------------------------------------------------------------
function TdmAct_PMP_site.GetExplorerPath(aID: integer; aGUID: string): string;
//--------------------------------------------------------------------
var sFolderGUID: string;
begin
  sFolderGUID:=dmFolder.GetGUIDByID (dmPmpSite.GetFolderID(aID));

  Result := GUID_PROJECT  + CRLF +
            GUID_PMP_Site + CRLF +
            sFolderGUID  + CRLF +
            aGUID;
end;

//--------------------------------------------------------------------
function TdmAct_PMP_site.ViewInProjectTree(aID: integer): TStrArray;
//--------------------------------------------------------------------
var
  iFolderID: Integer;
  sFolderGUID: string;

begin
  if Assigned(IMainProjectExplorer) then
  begin
    IMainProjectExplorer.ExpandByGUID(GUID_PROJECT);
    IMainProjectExplorer.ExpandByGUID(GUID_PMP_GROUP);
    IMainProjectExplorer.ExpandByGUID(GUID_PMP_Site);

    iFolderID := dmPmpSite.GetFolderID(aID);
//    sGUID:=dmFolder.GetGUIDByID (iFolderID);

    if iFolderID=0 then
      sFolderGUID:=g_Obj.ItemByName[ObjectName].RootFolderGUID
    else
      sFolderGUID:=dmFolder.GetGUIDByID(iFolderID);


     Result := StrArray([GUID_PROJECT,
                         GUID_PMP_GROUP,
                         GUID_PMP_Site,
                         sFolderGUID]);


    IMainProjectExplorer.ExpandByGUID(sFolderGUID);
//    IMainProjectExplorer.FocuseNodeByGUID (aGUID);

    IMainProjectExplorer.FocuseNodeByObjName(ObjectName, aID);

  end;

end;

 
end.


//
//
//{
//
////--------------------------------------------------------------------
//procedure TdmAct_PMP_site.DoAfterItemDel(aID: integer);
////--------------------------------------------------------------------
//var
//  i: integer;
//  intArr: TIntArray;
//begin
//
//{  Assert(Assigned(IMapEngine));
//  Assert(Assigned(IMapAct));
//
//
//  intArr:=dmPmpSite.GetAntIDArray(aID);
//
//
//  dmMapEngine1.DelObject (otPmpSite, aID);
//
//  for i := 0 to High(intArr) do
//    dmMapEngine1.DelObject (otPmpSectorAnt, intArr[i]);
//
//  dmAct_Map.MapRefresh;
//}
//
//end;
//
//
//
//procedure TdmAct_PMP_site.DataModuleDestroy(Sender: TObject);
//begin
//  inherited;
//end;
//
//
//
//                 //--------------------------------------------------------------------
//function TdmAct_Antenna.Dlg_Delete(aAntennaID: integer): Boolean;
////--------------------------------------------------------------------
////var
// // iID: Integer;
//var
//  sGUID: string;
//begin
//  Result:=False;
//
//  if ConfirmDlg('������� ?') then
//  begin
//   {
//    iID:=gl_DB.GetIntFieldValue(TBL_PMPSECTOR_ANT_XREF,
//                                FLD_ANTENNA_ID,
//                                [db_Par(FLD_PMPSECTOR_ID, FID)  ]);
//   }
//
//    //if Eq(FType,'ANTENNA') then
////    PostWMsg (WM_CELL_ANTENNA_DEL, iID); // else
//
//
////    iID:=gl_DB.GetIntFieldValue(TBL_PMPSECTOR_ANT_XREF,
////                                FLD_ANTENNA_ID,
// //                               [db_Par(FLD_PMPSECTOR_ID, FID)  ]);
//   ////   ItemDel(aAntennaID);
//
//    sGUID := dmAntenna.GetGUIDByID(aAntennaID);
//
//    if ItemDel(aAntennaID) then
//    begin
//
//      Assert(sGUID<>'');
//
//      g_ShellEvents.Shell_PostDelNode(sGUID);
//
//   //   PostEvent (WE_EXPLORER_DELETE_NODE_BY_GUID,  [app_Par(PAR_GUID, sGUID) ]);
////      SH_PostDelNode (sGUID);
//    end;
//
//    Result:=True;
//  end;
//
//end;
