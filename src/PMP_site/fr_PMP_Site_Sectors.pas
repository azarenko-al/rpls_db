unit fr_PMP_Site_Sectors;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  Variants,    cxTextEdit,
  cxTL, cxDBTL,
  ADODB, DB, Menus, ActnList, 

  u_Storage,

  //dm_User_Security,

 // I_Act_Explorer, //dm_Act_Explorer,
  dm_Act_Pmp_Sector, //dm_Act_Pmp_Sector,
//  dm_Act_Antenna,  // dm_Act_Antenna,

  dm_Onega_DB_data,


 // I_CalcModel,
 // I_LinkEndType,

  dm_Act_LinkEndType,

//  I_MapEngine,
//  dm_MapEngine,

 // I_PMP_Sector,


  u_const_str,
  u_const_db,

  u_cx,
  u_db,

  u_img,
  u_func,
  u_radio,


  u_func_msg,
  

  u_types,


  dm_LinkEndType,

  dxBar, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxCustomData, cxStyles, cxButtonEdit, cxMaskEdit, cxColorComboBox,
  cxTLdxBarBuiltInMenu, cxClasses, cxInplaceContainer, cxTLData,
  dxSkinsCore, dxSkinsDefaultPainters;



 type
  Tframe_PMP_Site_Sector = class(TForm, IMessageHandler)
    ActionList1: TActionList;
    act_Edit: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    act_Add_cell: TAction;
    act_Add_Antenna: TAction;
    act_Del: TAction;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    act_Setup_Dlg: TAction;
    act_Antenna_Update: TAction;
    actAntennaUpdate1: TMenuItem;
    DataSource2: TDataSource;
    cxDBTreeList1: TcxDBTreeList;
    col__name: TcxDBTreeListColumn;
    col_objname: TcxDBTreeListColumn;
    col__id: TcxDBTreeListColumn;
    col__cell_layer_name: TcxDBTreeListColumn;
    col__linkendtype_name: TcxDBTreeListColumn;
    col_THRESHOLD_BER_6: TcxDBTreeListColumn;
    col__trx_power_dBm: TcxDBTreeListColumn;
    col__trx_Power_W: TcxDBTreeListColumn;
    col__trx_freq: TcxDBTreeListColumn;
    col_antennatype_name: TcxDBTreeListColumn;
    col__height: TcxDBTreeListColumn;
    col__azimuth: TcxDBTreeListColumn;
    col__antenna_loss: TcxDBTreeListColumn;
    col__tilt: TcxDBTreeListColumn;
    col__calc_model_name: TcxDBTreeListColumn;
    col__k0: TcxDBTreeListColumn;
    col__k0_open: TcxDBTreeListColumn;
    col__k0_closed: TcxDBTreeListColumn;
    col__color: TcxDBTreeListColumn;
    col__calc_Radius: TcxDBTreeListColumn;
    ADOStoredProc1: TADOStoredProc;
    col_band: TcxDBTreeListColumn;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarSubItem1: TdxBarSubItem;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    procedure FormCreate(Sender: TObject);
    procedure act_EditExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
    procedure FormDestroy(Sender: TObject);


    procedure cxDBTreeList1DblClick11(Sender: TObject);
    procedure cxDBTreeList1Editing1(Sender: TcxCustomTreeList; AColumn: TcxTreeListColumn;
        var Allow: Boolean);
    procedure cxDBTreeList1KeyDown1(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ADOQuery1AfterPost(DataSet: TDataSet);


    procedure col__cell_layer_namePropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure col__trx_Power_WPropertiesEditValueChanged(Sender: TObject);
    procedure col__azimuthPropertiesEditValueChanged(Sender: TObject);
  private


//    FAzimuthChanged : Boolean;

    FDataSet : TDataSet;

    FSiteID: integer;

    FRecNo: integer;

    FReadOnly : Boolean;

    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList; var aHandled:
        boolean);

//    FUpdated:  boolean;
  protected
    procedure RefreshData;
  public

    procedure SetReadOnly(Value: Boolean);

    procedure View(aID: Integer);
  end;


//==================================================================
// implementation
//==================================================================
implementation
 {$R *.dfm}

uses dm_Act_Explorer,
     dm_act_Antenna;

 
//-------------------------------------------------------------------
procedure Tframe_PMP_Site_Sector.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
var
  i: integer;
begin
  inherited;

  cxDBTreeList1.OnEditing  := cxDBTreeList1Editing1;
  cxDBTreeList1.OnKeyDown  := cxDBTreeList1KeyDown1;
  cxDBTreeList1.OnDblClick := cxDBTreeList1DblClick11;


 // Assert(Assigned(ILinkEndType));
//  Assert(Assigned(IPmpSector));
 // Assert(Assigned(IAntenna));
//  Assert(Assigned(IMapEngine));

  act_Setup_Dlg.Caption:= STR_SETUP_TABLE;

  act_Antenna_Update.Caption:='�������� �� �����';

  cxDBTreeList1.Align := alClient;

//  dxDBTree.Align := alClient;
  //cxPageControl1.Align := alClient;


(*  dxDBTree.Bands[2].Caption := STR_TRX;
  dxDBTree.Bands[3].Caption := STR_ANTENNA;
*)
(*
  col_Name.Caption           := STR_NAME;
  col_Trx_Sense.Caption      := STR_TRANS_SENSE_dBm;
  col_Trx_Power_wt.Caption   := STR_TRANS_POWER_W;
  col_Trx_Power_dbm.Caption  := STR_TRANS_POWER_DBM;
  col_Trx_Freq.Caption       := STR_TRANS_FREQ_MHz;
  col_LinkEndType.Caption    := STR_TYPE;
  col_Calc_Radius_km.Caption := STR_CALC_RADIUS_km;
  col_Cell_Calc_Model.Caption:= STR_CALC_MODEL;
  col_k0.Caption             := STR_K0;
  col_k0_Open.Caption        := STR_K0_OPEN;
  col_k0_closed.Caption      := STR_K0_CLOSED;
  col_Color.Caption          := '';
  col_Antenna_Type.Caption   := STR_TYPE;
  col_Antenna_Height.Caption := STR_HEIGHT;
  col_Antenna_Azimuth.Caption:= STR_AZIMUTH;
  col_Antenna_tilt.Caption   := STR_TILT;
  col_Antenna_pos.Caption    := STR_COORDS;
  col_Ant_Loss.Caption       := STR_LOSS_dB;


  *)

  col__Color.Caption.Text            := '����';
        

  col__Name.Caption.Text            := STR_NAME;
  col_THRESHOLD_BER_6.Caption.Text  := STR_TRANS_SENSE_dBm;

  col__Trx_Power_W.Caption.Text       := STR_TRANS_POWER_W;
  col__Trx_Power_dbm.Caption.Text   := STR_TRANS_POWER_DBM;
  col__Trx_Freq.Caption.Text        := STR_TRANS_FREQ_MHz;
  col__LinkEndType_name.Caption.Text:= STR_TYPE;
  col__Calc_Radius.Caption.Text     := STR_CALC_RADIUS_km;
  col__calc_model_name.Caption.Text := STR_CALC_MODEL;
  col__k0.Caption.Text              := STR_K0;
  col__k0_Open.Caption.Text         := STR_K0_OPEN;
  col__k0_closed.Caption.Text       := STR_K0_CLOSED;

  //col__Color.Caption.Text           := '';

  col_antennatype_name.Caption.Text:= STR_TYPE;
  col__Height.Caption.Text          := STR_HEIGHT;
  col__Azimuth.Caption.Text         := STR_AZIMUTH;
  col__tilt.Caption.Text            := STR_TILT;
//  col__pos.Caption.Text             := STR_COORDS;
  col__Antenna_Loss.Caption.Text    := STR_LOSS;

      //  col_THRESHOLD_BER_6
 // col_Cell_ID.FieldName:=FLD_CELLID;

 // dmPMP_Site_View.InitDB (mem_Data);


//  for i := 0 to mem_Data.Fields.Count - 1 do
 //   mem_Data.Fields[i].OnChange:=dmPMP_site_View.FieldChange;


 // dxDBTree.LoadFromRegistry (FRegPath + dxDBTree.Name);
 // dx_CheckColumnSizes_DBTreeList(dxDBTree);

(*  col_Type.Visible := False;
  col_Type.Tag:=1; // not visible
*)


  cxDBTreeList1.OptionsBehavior.ExpandOnDblClick := False;

  g_Storage.RestoreFromRegistry(cxDBTreeList1, ClassName);


  FDataset:=ADOStoredProc1;

 // cxPageControl1.HideTabs := True;

end;

//-------------------------------------------------------------------
procedure Tframe_PMP_Site_Sector.FormDestroy(Sender: TObject);
//-------------------------------------------------------------------
begin
  g_Storage.StoreToRegistry(cxDBTreeList1, ClassName);     

 // dxDBTree.SaveToRegistry (FRegPath + dxDBTree.Name);
  inherited;
end;


// ---------------------------------------------------------------
procedure Tframe_PMP_Site_Sector.SetReadOnly(Value: Boolean);
// ---------------------------------------------------------------
begin
//  cxGrid1DBTableView1.OptionsData.Editing := not Value;
  FReadOnly := Value;

  ADOStoredProc1.Close;
  ADOStoredProc1.LockType := IIF(Value, ltReadOnly, ltBatchOptimistic);
  SetFormActionsEnabled(Self, not Value);

//  ADOStoredProc1.LockType := IIF(dmUser_Security.ProjectIsReadOnly, ltReadOnly, ltBatchOptimistic);
//  SetFormActionsEnabled(Self, not dmUser_Security.ProjectIsReadOnly);

end;

//-------------------------------------------------------------------
procedure Tframe_PMP_Site_Sector.View(aID: Integer);
//-------------------------------------------------------------------
var
  k: Integer;
begin
  FSiteID := aID;

(*
  FUpdated:=true;
  dmPMP_site_View.OpenDB(aID, mem_Data);
  FUpdated:=false;
*)

(*  ADOStoredProc1.LockType := IIF(dmUser_Security.ProjectIsReadOnly, ltReadOnly, ltBatchOptimistic);
  SetFormActionsEnabled(Self, not dmUser_Security.ProjectIsReadOnly);

*)
  k:=dmOnega_DB_data.Pmp_Site_Select_Item(ADOStoredProc1, aID);


//  dx_CheckColumnSizes_DBTreeList(dxDBTree);

//  dxDBTree.FullExpand;

  cxDBTreeList1.FullExpand;

end;

//-------------------------------------------------------------------
procedure Tframe_PMP_Site_Sector.act_EditExecute(Sender: TObject);
//-------------------------------------------------------------------
 var
  bRefreshData: Boolean;
  iID: Integer;
 // sType,
  sObjName, sGUID1, sGUID2: string;
  sGUID: string;

begin
  if FReadOnly then
    Exit;


  iID :=0;

  bRefreshData := False;

//  sObjName := mem_Data[FLD_TYPE];
  if FDataset.RecordCount>0 then
  begin
    iID       := FDataset.FieldByName(FLD_ID).AsInteger;
    sObjName  := FDataset.FieldByName(FLD_OBJNAME).AsString;
    sGUID     := FDataset.FieldByName(FLD_GUID).AsString;
  end;

  //---------------------------------
  if Sender = act_Edit then begin
  //---------------------------------
   //  iID := FDataset[FLD_ID];

     if Eq(sObjName, OBJ_LINKEND_ANTENNA) or
        Eq(sObjName, 'ANTENNA')
     then
        if dmAct_Antenna.Dlg_Edit (iID) then;
          ;

     if Eq(sObjName, OBJ_PMP_SECTOR) then
        if dmAct_Pmp_Sector.Dlg_Edit (iID) then;

     bRefreshData:=True;
//     RefreshData();
  end else


//  act_Setup_Dlg.Caption:= STR_SETUP_TABLE;

  //---------------------------------
  if Sender = act_Setup_Dlg then begin
  //---------------------------------
//    dx_Dlg_Customize_DBTreeList (dxDBTree);
  end else

  //---------------------------------
  if Sender = act_Add_cell then
  //---------------------------------
  begin
    iID := dmAct_Pmp_Sector.Dlg_Add(FSiteID);
    if iID > 0 then
      bRefreshData:=True;
//      RefreshData();

  end else

  //---------------------------------
  if Sender = act_Add_Antenna then begin
  //---------------------------------
  //  iID := FDataset[FLD_ID];


    bRefreshData:=dmAct_Antenna.Dlg_Add_LinkEnd_Antenna(iID)>0 ;
 //   if IAct_Pmp_Sector.AntennaAdd (iID) then
//     bRefreshData:=True;

    //  RefreshData();
  end else


  //---------------------------------
  if Sender = act_Antenna_Update then begin
  //---------------------------------
  //  iID := FDataset[FLD_ID];
   // dmMapEngine.ReCreateObject (otPmpSectorAnt, FDataset[FLD_ID]);
  end else

  //---------------------------------
  if Sender = act_Del then begin
  //---------------------------------
  //  sType := mem_Data[FLD_TYPE];
  //  iID   := FdataSet[FLD_ID];


    if Eq(sObjName, OBJ_PMP_SECTOR) then
    begin
      bRefreshData:=dmAct_Pmp_Sector.Dlg_Del(iID);   //dmPmp_Sector.GetGUIDByID(iID))

//        RefreshData();

    end
    else

    if Eq(sObjName, OBJ_LINKEND_ANTENNA) or
       Eq(sObjName, 'ANTENNA')
    then
//      if IAct_Antenna.Dlg_Delete (iID) then

      bRefreshData:=dmAct_Antenna.Dlg_Delete (iID)
 //       RefreshData();

  //    IAct_Antenna.Del(iID) //dmAntenna.GetGUIDByID(iID))
    else
      raise Exception.Create('');


  end;


  if bRefreshData then
    RefreshData();

end;

//-------------------------------------------------------------------
procedure Tframe_PMP_Site_Sector.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//-------------------------------------------------------------------
var sObjName: string;
  bAnt: boolean;
  bRecords: boolean;
  bSector: boolean;
begin
  if FReadOnly then
    Exit;



  bRecords := FDataset.RecordCount>0;

  sObjName  := FDataset.FieldByName(FLD_OBJNAME).AsString;

  act_Del.Enabled  := bRecords;

//  sObjName := FDataset.FieldByName(FLD_TYPE).AsString;

//  act_Edit.Enabled          := (sObjName <> '');
//  act_Del.Enabled           := act_Edit.Enabled;

  bAnt:=bRecords and Eq(sObjName, OBJ_LINKEND_ANTENNA);
 // bAnt:=bRecords and Eq(sObjName, OBJ_LINKEND_ANTENNA);

  bSector :=bRecords and  Eq(sObjName, OBJ_PMP_SECTOR);

 // act_Add_cell.Visible      := bRecords and (not Eq(sObjName, OBJ_LINKEND_ANTENNA));

  act_Add_Antenna.Enabled   := bSector;
  act_Antenna_Update.Enabled:= bAnt;

end;



procedure Tframe_PMP_Site_Sector.cxDBTreeList1DblClick11(Sender: TObject);
var
  iColor: integer;
  sObjName: string;
begin
  if FReadOnly then
    Exit;


  if not cx_IsCursorInGrid(Sender) then
    Exit;


   {

Node := cxInvoices.FocusedNode;
 cxInvoices.HitTest.ReCalculate(GetMouseCursorPos);
 if Node <> cxInvoices.HitTest.HitNode then exit;

   }



 // cxDBTreeList1.FocusedColumn.f

//  sObjName := FDataSet.FieldByName(FLD_TYPE).AsString;

  sObjName  := FDataset.FieldByName(FLD_OBJNAME).AsString;


  if cxDBTreeList1.FocusedColumn = col__color then
    if Eq(sObjName, OBJ_PMP_SECTOR) then
//    if Eq (cxDBTreeList1.FocusedColumn. DataBinding. FieldName, FLD_COLOR) then
    begin
      iColor:=FDataset.FieldByName(FLD_COLOR).AsInteger;
      iColor:=dlg_SelectColor(iColor);

      if iColor<>0 then
      begin
      //  gl_DB.UpdateRecord(TBL_PMP_SECTOR, FDataset[FLD_ID],
        //                  [db_Par(FLD_COLOR, iColor)]);
       //
        db_UpdateRecord(FDataset, [db_Par(FLD_COLOR, iColor)]);
      end;
    end;

end;

// ---------------------------------------------------------------
procedure Tframe_PMP_Site_Sector.cxDBTreeList1Editing1(Sender: TcxCustomTreeList;
    AColumn: TcxTreeListColumn; var Allow: Boolean);
// ---------------------------------------------------------------
var
  sObjName: string;
  s: string;
  bAnt: Boolean;
begin
  if FReadOnly then
  begin
    Allow:=False;
    Exit;
  end;


  sObjName := FDataSet.FieldByName(FLD_OBJNAME).AsString;
//  sObjName := FDataSet.FieldByName(FLD_TYPE).AsString;

//  s:=AColumn.DataBinding.FieldName;

  bAnt:=Eq(sObjName,OBJ_LINKEND_ANTENNA) or
     Eq(sObjName,'ANTENNA');

  Allow:=(Eq(sObjName, OBJ_PMP_SECTOR) and
          (
            (AColumn=col__linkendtype_name) or
            (AColumn=col__cell_layer_name) or
            (AColumn=col_THRESHOLD_BER_6) or
            (AColumn=col__Trx_Power_W) or
            (AColumn=col__Trx_Power_dbm) or
          //  col_Trx_Power.Options.Editing       := bAnt;
            (AColumn=col__Trx_Freq) or

            (AColumn=col__Calc_Radius) or
            (AColumn=col__calc_model_name) or
            (AColumn=col__K0) or
            (AColumn=col__K0_Open) or
            (AColumn=col__K0_Closed)
           )
         )

         or

         (bAnt and
         (
          (AColumn=col_antennatype_name) or
          (AColumn=col__Height) or
          (AColumn=col__Azimuth) or
          (AColumn=col__tilt) or
          (AColumn=col__antenna_Loss)
         ));
end;

// ---------------------------------------------------------------
procedure Tframe_PMP_Site_Sector.cxDBTreeList1KeyDown1(Sender: TObject; var Key: Word; Shift:
    TShiftState);
// ---------------------------------------------------------------
var
  sObjName: string;
begin
//  sObjName := FDataSet.FieldByName(FLD_TYPE).AsString;
  sObjName  := FDataset.FieldByName(FLD_OBJNAME).AsString;


  if ((Key = VK_DELETE) or (Key = VK_BACK)) and
     Eq(sObjName,OBJ_PMP_SECTOR) then
  begin
    if (cxDBTreeList1.FocusedColumn = col__calc_model_name) then
      if (cxDBTreeList1.FocusedNode.Values[col__calc_model_name.ItemIndex] <> '') then
      begin
//        FRecNo:= FdataSet.RecNo;

     //   FDataset[FLD_CALC_MODEL_ID] := null;

        db_UpdateRecord(FDataSet,
              [

               db_par(FLD_CALC_MODEL_name, NULL),
               db_par(FLD_CALC_MODEL_ID, NULL),
               db_par(FLD_K0,            NULL),
               db_par(FLD_K0_OPEN,       NULL),
               db_par(FLD_K0_CLOSED,     NULL)
              ]);


(*        dmPmp_Sector.Update(FDataSet[FLD_ID], [db_par(FLD_CALC_MODEL_ID, NULL),
                                               db_par(FLD_K0,            NULL),
                                               db_par(FLD_K0_OPEN,       NULL),
                                               db_par(FLD_K0_CLOSED,     NULL) ]);
*)
//        RefreshData();

    //    FdataSet.RecNo:= FRecNo;
      end;
  end;
end;


//-------------------------------------------------------------------
procedure Tframe_PMP_Site_Sector.ADOQuery1AfterPost(DataSet: TDataSet);
//-------------------------------------------------------------------
var
  e: Double;
  iAntType: Integer;
  iAntID: Integer;
  dAzimuth, dRadius_km, powerDbm: double;
  sObjName: string;
  bAnt: boolean;
  i: Integer;
  iID: Integer;
  k: Integer;
begin
//  if FUpdated then
//    Exit;

//  sObjName := FdataSet.FieldByName(FLD_TYPE).AsString;
  sObjName  := FDataset.FieldByName(FLD_OBJNAME).AsString;
  iID:=DataSet[FLD_ID];

  i:=dmOnega_DB_data.ExecStoredProc (sp_PMP_Site_update_item,
        [
          db_Par(FLD_ID,      DataSet[FLD_ID]),
          db_Par(FLD_OBJNAME, DataSet[FLD_OBJNAME]),

          //cector
          db_Par(FLD_COLOR,     DataSet[FLD_COLOR]),
          db_Par(FLD_POWER_DBM, DataSet[FLD_POWER_DBM]),
          db_Par(FLD_POWER_W,   DataSet[FLD_POWER_W]),

          db_Par(FLD_TX_FREQ_MHz,   DataSet[FLD_TX_FREQ_MHz]),
                                            

 //          db_UpdateRecord (FdataSet, FLD_CELL_LAYER_ID, iCellLayerID);


          db_Par(FLD_CELL_LAYER_ID, DataSet[FLD_CELL_LAYER_ID]),

              //    FDataset[FLD_CALC_MODEL_ID] := null;

          db_Par(FLD_CALC_RADIUS_KM, DataSet[FLD_CALC_RADIUS_KM]),

          db_Par(FLD_CALC_MODEL_ID,  DataSet[FLD_CALC_MODEL_ID]),
          db_Par(FLD_K0_OPEN,        DataSet[FLD_K0_OPEN]),
          db_Par(FLD_K0_CLOSED,      DataSet[FLD_K0_CLOSED]),
          db_Par(FLD_K0,             DataSet[FLD_K0]),

          db_Par(FLD_THRESHOLD_BER_3,    DataSet[FLD_THRESHOLD_BER_3]),
          db_Par(FLD_THRESHOLD_BER_6,    DataSet[FLD_THRESHOLD_BER_6]),


          //antenna
//

          db_Par(FLD_AntennaType_ID,  DataSet[FLD_AntennaType_ID]),

          db_Par(FLD_HEIGHT,  DataSet[FLD_HEIGHT]),
          db_Par(FLD_AZIMUTH, DataSet[FLD_AZIMUTH]),
          db_Par(FLD_TILT,    DataSet[FLD_TILT]),
          db_Par(FLD_LOSS,    DataSet[FLD_ANTENNA_LOSS])

         ]);


(*
k:=dmOnega_DB_data.ExecStoredProc('sp_Linkend_Antenna_Update',
      [
       db_Par(FLD_ID,     DataSet[FLD_ID]),

       db_Par(FLD_NAME,   DataSet[FLD_NAME]),
       db_Par(FLD_AZIMUTH, DataSet[FLD_AZIMUTH]),
       db_Par(FLD_TILT,    DataSet[FLD_TILT]),

       db_Par(FLD_HEIGHT, DataSet[FLD_HEIGHT])
       ]);
*)


  i:=DataSet[FLD_ID];


  bAnt:= Eq(sObjName, OBJ_LINKEND_ANTENNA) or
         Eq(sObjName, 'ANTENNA');

(*
  if bAnt then
    k:=dmOnega_DB_data.ExecStoredProc('sp_Linkend_Antenna_Update',
      [
       db_Par(FLD_ID,      DataSet[FLD_ID]),

       db_Par(FLD_NAME,    DataSet[FLD_NAME]),
       db_Par(FLD_AZIMUTH, DataSet[FLD_AZIMUTH]),
       db_Par(FLD_TILT,    DataSet[FLD_TILT]),

       db_Par(FLD_HEIGHT, DataSet[FLD_HEIGHT])
       ]);
*)


//  if bAnt and (FAzimuthChanged)then
//    dmMapEngine.ReCreateObject(otPmpSectorAnt, FDataset[FLD_ID]);

//  FAzimuthChanged := False;
end;


// ---------------------------------------------------------------
procedure Tframe_PMP_Site_Sector.RefreshData;
// ---------------------------------------------------------------
begin
  FRecNo:= FdataSet.RecNo;

  View(FSiteID);

  if FRecNo>0 then
    FdataSet.RecNo:= FRecNo;
end;


//-------------------------------------------------------------------
procedure Tframe_PMP_Site_Sector.col__cell_layer_namePropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
//-------------------------------------------------------------------
var
  iPmpSectorID, iCalcModelID, iLinkEndType_ID, iCellLayerID: integer;
  iID: Integer;
  iMode: Integer;
  sObjName: string;
  iAntID, iAntTypeID: integer;
  sName: Widestring;
  K0, k0_Open, k0_Closed: double;
  iLinkEndType_Mode_ID: integer;
  rec: TdmLinkEndTypeModeInfoRec;

begin
  inherited;
 // FRecNo:= FdataSet.RecNo;

//  sObjName  :=dxDBTree.FocusedNode.Values[col_Type.Index];
 // iID       :=dxDBTree.FocusedNode.Values[col_ID.Index];

//  sObjName  :=FdataSet.FieldByName(FLD_type).AsString;

  iID       := FdataSet.FieldByName(FLD_ID).AsInteger;
  sObjName  := FDataset.FieldByName(FLD_OBJNAME).AsString;


  //--------------------------------
  if cxDBTreeList1.FocusedColumn=col_antennatype_name then begin
  //--------------------------------
    if Eq(sObjName, OBJ_LINKEND_ANTENNA) or Eq(sObjName, 'ANTENNA') then
    begin
    //  iAntID          := iID;
      iAntTypeID := FdataSet.FieldByName(FLD_ANTENNATYPE_ID).AsInteger;
      //dmAntenna.GetIntFieldValue(iAntID, FLD_ANTENNATYPE_ID);

      if  dmAct_Explorer.Dlg_Select_Object (otAntennaType, iAntTypeID, sName) then
      begin
 {
        gl_DB.UpdateRecord(TBL_ANTENNA, iAntID, [db_par(FLD_ANTENNATYPE_ID, iAntTypeID_new)]);
        dmAntenna.UpdateAntTypeInfo(iAntID);
 }

        dmOnega_DB_data.Object_update_AntType(OBJ_LINKEND_ANTENNA, iID, iAntTypeID);

       // dmAntenna.ChangeAntType (iAntID, iAntTypeID);

        RefreshData();
      end;
    end;

  end else

  //--------------------------------
  if cxDBTreeList1.FocusedColumn=col__cell_layer_name then begin
  //--------------------------------
    if Eq(sObjName, OBJ_PMP_SECTOR) then
    begin
    //  iPmpSectorID := iID;

      iCellLayerID := FdataSet.FieldByName(FLD_CELL_LAYER_ID).AsInteger;

  //    iCellLayerID:=0;
      if dmAct_Explorer.Dlg_Select_Object (otCellLayer, iCellLayerID, sName) then
      begin
        db_UpdateRecord (FdataSet, FLD_CELL_LAYER_ID, iCellLayerID);

     //   dmPmp_Sector.Update(iID, [db_par(FLD_CELL_LAYER_ID, iCellLayerID)]);

  ////////      dmLinkEnd.UpdateLinkEndInfo(iPmpSectorID, iCellLayerID_new, OBJ_PMP_SECTOR);
        RefreshData();
      end;
    end;
  end else

  //--------------------------------
  if cxDBTreeList1.FocusedColumn=col__calc_model_name then  begin
  //--------------------------------
    if Eq(sObjName, OBJ_PMP_SECTOR) then
    begin
      case AButtonIndex of
      {  1:  begin
              if ConfirmDlg('�������� ������ ��������?') then
              begin
                dmPmp_Sector.Update(dxDBTree.FocusedNode.Values[col_ID.Index],
                                 [db_par(FLD_CALC_MODEL_ID, NULL),
                                  db_par(FLD_K0,            NULL),
                                  db_par(FLD_K0_OPEN,       NULL),
                                  db_par(FLD_K0_CLOSED,     NULL)  ]);
                View(FSiteID);
              end;
            end;
                    }
        0:  begin
            //  iCalcModelID:=0;

              iCalcModelID := FdataSet.FieldByName(FLD_CALC_MODEL_ID).AsInteger;

              if dmAct_Explorer.Dlg_Select_Object (otCalcModel, iCalcModelID, sName) then
              begin
                dmOnega_DB_data.Object_update_calcmodel (OBJ_Pmp_Sector, iID, iCalcModelID);

               // dmOnega_DB_data.Pmp_Sector_update_calc_model(iID, iCalcModelID);


(*                dmCalcModel.GetK0_open_close(iCalcModelID, K0, k0_Open, k0_Closed);

                db_UpdateRecord (FdataSet,
                       [db_Par(FLD_CALC_MODEL_ID, iCalcModelID),
                        db_par(FLD_K0,            K0),
                        db_par(FLD_K0_OPEN,       k0_Open),
                        db_par(FLD_K0_CLOSED,     k0_Closed)  ]);
*)

             {    dmPmp_Sector.Update(iID, // dxDBTree.FocusedNode.Values[col_ID.Index],

                                  [db_Par(FLD_CALC_MODEL_ID, iCalcModelID),
                                   db_par(FLD_K0,            K0),
                                   db_par(FLD_K0_OPEN,       k0_Open),
                                   db_par(FLD_K0_CLOSED,     k0_Closed)  ]);
}
             //   RefreshData();

                RefreshData();
              end;
            end;
      end; // case
    end;

  end else

  //--------------------------------
  if cxDBTreeList1.FocusedColumn=col__linkendtype_name then begin
  //--------------------------------
    if Eq(sObjName, OBJ_PMP_SECTOR) then
    begin
    //  iPmpSectorID     := iID;
   //   iLinkEndType_ID := dmLinkEnd.GetIntFieldValue_(iPmpSectorID, FLD_LINKEND_Type_ID, OBJ_PMP_SECTOR);
    //  iLinkEndType_ID      := dmPmp_Sector.GetIntFieldValue (iPmpSectorID, FLD_LINKENDTYPE_ID);

      iLinkEndType_ID     := FdataSet.FieldByName(FLD_LINKENDType_ID).AsInteger;
      iLinkendType_Mode_ID:= FDataset.FieldByName(FLD_LINKENDTYPE_Mode_ID).AsInteger ;

      if dmAct_Explorer.Dlg_Select_Object (otLinkEndType, iLinkEndType_ID, sName) then
        if dmAct_LinkEndType.Dlg_GetMode (iLinkEndType_ID, iLinkendType_Mode_ID,rec) then
      begin
          dmOnega_DB_data.Object_update_LinkEndType
              (OBJ_PMP_SECTOR, iID, iLinkEndType_ID, rec.ID); //rec.Mode,


(*        gl_DB.UpdateRecord(TBL_PMP_SECTOR, iPmpSectorID,
                           [db_par(FLD_LINKEND_Type_ID, iLinkEndType_ID)]);

        dmLinkEnd.UpdateLinkEndInfo1 (iPmpSectorID, OBJ_PMP_SECTOR, iLinkEndType_ID);

        if dmAct_LinkEndType.Dlg_GetMode (iLinkEndType_ID, iMode) then
          dmLinkEnd.UpdateModeInfo (iPmpSectorID, OBJ_PMP_SECTOR, iMode);

        RefreshData();
*)
       RefreshData();
      end;
    end;
  end;

 // FdataSet.RecNo:= FRecNo;
end;


procedure Tframe_PMP_Site_Sector.col__trx_Power_WPropertiesEditValueChanged(
  Sender: TObject);
var
  s: string;
  oEdit: TcxTextEdit;
  e,e1: double;
begin
  oEdit:= Sender as TcxTextEdit;
  oEdit.PostEditValue;

  s:=Sender.className;
  s:='';

  e:=AsFloat(oEdit.Text);

  if cxDBTreeList1.FocusedColumn=col__trx_power_dBm then
  begin
    e1 :=dbm_to_wt(e);
    cxDBTreeList1.FocusedNode.Values[col__trx_power_W.ItemIndex] := e1;
  end else

  if cxDBTreeList1.FocusedColumn=col__trx_power_W then
  begin
    e1:=wt_to_dbm(e);
    cxDBTreeList1.FocusedNode.Values[col__trx_power_dBm.ItemIndex] := e1;
  end;

//  try
//    if Eq(dxDBTree.FocusedField.FieldName, FLD_POWER_DBM) then
//    begin
//      e:=AsFloat(Node.Values[col_Trx_Power_dbm.Index]);
//
//      Node.Values[col_Trx_Power_WT.Index]:=wt_to_dbm(e);
//    end;
//
//    if Eq(dxDBTree.FocusedField.FieldName, FLD_POWER_WT) then
//    begin
//      e:=AsFloat(Node.Values[col_Trx_Power_WT.Index]);
//
//      Node.Values[col_Trx_Power_dbm.Index]:=dbm_to_wt(e);
//    end;
//  except




end;

procedure Tframe_PMP_Site_Sector.col__azimuthPropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;

//  FAzimuthChanged := True;

end;

procedure Tframe_PMP_Site_Sector.GetMessage(aMsg: TEventType; aParams:
    TEventParamList; var aHandled: boolean);
begin
   case aMsg of
    WM_REFRESH_SITE: RefreshData();
  end;

end;

end.

{


//-------------------------------------------------------------------
procedure Tframe_PMP_Site_Sector.ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
//-------------------------------------------------------------------
begin
  inherited;

  case Msg.message of
    WM_REFRESH_SITE: RefreshData();
  end;

end;


// ---------------------------------------------------------------
procedure Tframe_SiteTemplate_Cells.cxDBTreeList1KeyDown(Sender: TObject; var
    Key: Word; Shift: TShiftState);
// ---------------------------------------------------------------
var
  sObjName: string;
  iID: integer;
  k: Integer;
begin
//  sObjName:=mem_Data.FieldBYName(FLD_type).AsString;

  sObjName:=FDataSet.FieldBYName(FLD_ObjName).AsString;
  iID:=FDataSet.FieldBYName(FLD_ID).AsInteger;


  if ((Key = VK_DELETE) or (Key = VK_BACK)) and
     Eq(sObjName, OBJ_PMP_SECTOR) then
  begin
    if (cxDBTreeList1.FocusedColumn = col_Calc_model_Name) then
      if (cxDBTreeList1.FocusedNode.Values[col_Calc_model_Name.ItemIndex] <> '') then
      begin
        FRecNo:= FDataSet.RecNo;

        k:=dmOnega_DB_data.Object_Update_CalcModel(OBJ_Template_LinkEnd, iID, 0);

        View();

        FDataSet.RecNo:= FRecNo;
      end;


    if (cxDBTreeList1.FocusedColumn = col_Combiner_Name) then
      if (cxDBTreeList1.FocusedNode.Values[col_Combiner_Name.ItemIndex] <> '') then
      begin
        FRecNo:= FDataSet.RecNo;

        dmTemplate_Linkend.Update_(iID,
             [db_par(FLD_COMBINER_ID,   NULL),
              db_par(FLD_COMBINER_LOSS, NULL) ]);

        View();

        FDataSet.RecNo:= FRecNo;
      end;
  end;


end;

