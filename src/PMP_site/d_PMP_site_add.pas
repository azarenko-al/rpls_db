unit d_PMP_site_add;

interface
{$I ver.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  
  cxVGrid, cxControls, cxInplaceContainer, Registry, cxButtonEdit,

  ComCtrls, cxPropertiesStore, rxPlacemnt,

  dm_User_Security,

  d_Wizard_Add_with_params,

  u_Storage,

  I_Shell,
  u_Shell_new,

  u_cx_VGrid,

  dm_Main,

  dm_MapEngine_store,

  dm_Property,

  dm_PMP_site,

  u_SiteTemplate_types,

  u_GEO,
  u_db,
  u_func,
  u_classes,
  u_dlg,

  u_types,

  u_const_str,
  u_const_db,


   ActnList, StdCtrls, ExtCtrls, cxLookAndFeels
  ;

type
  Tdlg_PMP_site_add = class(Tdlg_Wizard_add_with_params)
    cxVerticalGrid1: TcxVerticalGrid;
    row_Property_: TcxEditorRow;
    row_CalcRadius_km: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    row_is_template: TcxEditorRow;
    row_site_template: TcxEditorRow;
    row_Property_type: TcxEditorRow;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;

    procedure _ActionMenu(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
//    procedure DoOnButtonClick(Sender: TObject;  AbsoluteIndex: Integer);
    procedure ActionList2Update(Action: TBasicAction;
      var Handled: Boolean);

    procedure row_PropertyEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure row_Property_typeEditPropertiesCloseUp(Sender: TObject);

  private
    FBLPoint: TBLPoint;

    FRec: TdmPMP_site_Add_Rec;
    FID: integer;

    FTemplate_pmp_Site_ID: integer;

    procedure Append;
    procedure DoOnProperty_TypeChange;

  public

    class function ExecDlg(aPropertyID: integer; aBLPoint: TBLPoint): integer;
  end;

//===================================================================
implementation

uses dm_Object_base;  {$R *.DFM}
//===================================================================

const
  DEF_NEW     = 0;
  DEF_EXISTED = 1;

  //�����
//C�����������



//-------------------------------------------------------------------
class function Tdlg_PMP_site_add.ExecDlg(aPropertyID: integer; aBLPoint:
    TBLPoint): integer;
//-------------------------------------------------------------------
var
  s: string;
begin
{
  // ---------------------------------------------------------------
  if dmUser_Security.ProjectIsReadOnly then
  begin
    ShowMessage('���������� �������� ������. ������ ������ ��� ������.');
    Result:=0;
    Exit;
  end;
  // ---------------------------------------------------------------
}


  with Tdlg_PMP_site_add.Create(Application) do
  try
    FBLPoint :=aBLPoint;

  //  FRec.FolderID:=aFolderID;
    ed_Name_.Text:=dmPmpSite.GetNewName;
//    ed_Name_.Text   :=dmPmpSite.GetNewName;

    if (aPropertyID = 0) then
      aPropertyID:=dmProperty.GetNearestID(aBLPoint);

 //   FRec.FolderID:=aFolderID;


    FRec.Property_ID:=aPropertyID;

   // row_Folder.Text  :=dmFolder.GetPath (FRec.FolderID);

//�����
//C�����������
//  DEF_NEW     = 0;
//  DEF_EXISTED = 1;


    if aPropertyID>0 then
    begin
      cx_SetRowItemIndex(row_Property_type, DEF_EXISTED);

      s:=dmProperty.GetNameByID (FRec.Property_ID);
      row_Property_.Properties.vALUE:=s;
    end else
      cx_SetRowItemIndex(row_Property_type, DEF_NEW);


    if ShowModal=mrOk then
      Result:=FID
    else
      Result:=0;

  finally
    Free;
  end;
end;


//--------------------------------------------------------------------
procedure Tdlg_PMP_site_add.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
var
  s: string;
begin
  inherited;

  SetActionName (STR_DLG_ADD_PMP);

  row_Property_.Properties.Caption     := STR_PROPERTY;
 // row_Folder.Caption       := STR_FOLDER;
  row_CalcRadius_km.Properties.Caption   := STR_CALC_RADIUS_km;
  row_is_template.Properties.Caption  := '������������ ������ ��';
  row_site_template.Properties.Caption:= STR_TEMPLATE_PMP_SITE;


  row_CalcRadius_km.Properties.Value:=30;


  cx_SetRowItemValues (row_Property_type, ['�����','C�����������']);

 // row_Property_new_name.Visible := False;


  //hide !!!!!!!!
 // row_Property_new_name



  
  with TRegIniFile.Create(FRegPath) do
  begin
//    row_CalcRadius_km.Properties.Value := STR_CALC_RADIUS_km;

    s:=row_is_template.Name;


    row_is_template.Properties.Value    := ReadBool   ('', row_is_template.Name, False);
    FTemplate_pmp_Site_ID:= ReadInteger('', FLD_TEMPLATE_PMP_SITE_ID, 0);

//    row_is_template.Properties.Value  := ReadInteger('', FLD_TEMPLATE_SITE_ID, 0);

    Free;
  end;

//  row_is_template.Properties.Value  := FIsTemplate;

  s:=gl_DB.GetNameByID(TBL_TEMPLATE_PMP_SITE, FTemplate_pmp_Site_ID);
  row_site_template.Properties.Value:=s;


 // row_Folder.OnButtonClick:=DoOnButtonClick;

  SetDefaultWidth();

  cx_InitVerticalGrid (cxVerticalGrid1);

end;

//-------------------------------------------------------------------
procedure Tdlg_PMP_site_add.DoOnProperty_TypeChange;
//-------------------------------------------------------------------
var
  iInd: integer;
begin
  iInd:=cx_GetRowItemIndex(row_Property_type);

  row_Property_.Visible          :=iInd=DEF_EXISTED;
  //row_Property_new_name.Visible :=iInd=1;

end;


//----------------------------------------------------------------------
procedure Tdlg_PMP_site_add.FormDestroy(Sender: TObject);
//----------------------------------------------------------------------
begin
  with TRegIniFile.Create(FRegPath) do
  begin

    //    WriteInteger(Name, FLD_PROPERTY_ID, FPropertyID);
    WriteBool   ('', row_is_template.name, row_is_template.Properties.Value);
    WriteInteger('', FLD_TEMPLATE_pmp_SITE_ID, FTemplate_pmp_Site_ID);
  //  WriteInteger(Name, FLD_FOLDER_ID,  FFolderID);

    Free;
  end;

  inherited;
end;

//-------------------------------------------------------------------
procedure Tdlg_PMP_site_add._ActionMenu(Sender: TObject);
//-------------------------------------------------------------------
begin
  if Sender=act_Ok then
    Append ;
end;


// -------------------------------------------------------------------
procedure Tdlg_PMP_site_add.ActionList2Update(Action: TBasicAction; var Handled: Boolean);
// -------------------------------------------------------------------
begin
  inherited;
  act_Ok.Enabled:=(FRec.Property_ID>0);
end;



//--------------------------------------------------------------------
procedure Tdlg_PMP_site_add.Append;
//--------------------------------------------------------------------
var
  bIsTemplate: Boolean;
  blPoint: TBLPoint;
  iProperty_type: Integer;
  oIDList: TIDList;
  sFolderGUID: string;

begin
  FRec.NewName       :=ed_Name_.Text;
  FRec.CalcRadius_km := AsFloat(row_CalcRadius_km.Properties.Value);

  bIsTemplate:=row_is_template.Properties.Value;

  iProperty_type:= cx_GetRowItemIndex( row_Property_type );

  if iProperty_type=DEF_NEW then
  begin
    FRec.Point:=FBLPoint;
    FRec.PropertyName:=dmProperty.GetNewName();
    FRec.Property_id:=-1;

  end;

  if (bIsTemplate) and (FTemplate_pmp_Site_ID > 0) then
    FRec.Template_Pmp_Site_ID:=FTemplate_pmp_Site_ID;

//  if (bIsTemplate) and (FTemplate_pmp_Site_ID > 0) then
///    FRec.Template_pmp_Site_ID:=FTemplate_pmp_Site_ID;

  //FID:=dmPmp_Site.Add (FRec);


//  if (FIsTemplate = True) and (FTemplate_pmp_Site_ID > 0) then
//    FID:=dmPmp_Site.Add_by_Template (rec)
 // else

////procedure cx_SetRowItemIndex(aEditorRow: TcxEditorRow; aItemIndex: Integer);




  FID:=dmPmpSite.Add (FRec);



(*
  if (FIsTemplate = True) and (FTemplate_pmp_Site_ID > 0) then
  begin
  //  dmPmp_Sector.Add_by_Template (Frec.SiteTemplateID, FID);
    blPoint:=dmProperty.GetPos (FRec.PropertyID);

    dmSiteTemplate.PMP_Sector_Add_by_Template (Frec.SiteTemplateID, FID, blPoint);
  end;
 //   integer);


*)
 //   FID:=dmSite.Add (rec);

{
  if Result > 0 then
  begin
    dmMapEngine.Sync(otPmpSectorAnt);
    dmAct_Map.MapRefresh;
  end;
}

  if FID>0 then
  begin
    g_ShellEvents.Shell_UpdateNodeChildren_ByObjName (OBJ_PROPERTY, FRec.Property_ID);



    sFolderGUID:=g_Obj.ItemByName[OBJ_PMP_SITE].RootFolderGUID;
    g_Shell.UpdateNodeChildren_ByGUID(sFolderGUID);
  //  g_ShellEvents.She UpdateNodeChildren_ByGUID (sFolderGUID);


//    if Assigned(IMapEngine) then


    dmMapEngine_store.Feature_Add(OBJ_PMP_SITE, FID);

    if not dmMapEngine_store.Is_Object_Map_Exists(OBJ_PMP_SITE) then
       dmMapEngine_store.Open_Object_Map(OBJ_PMP_SITE);

    if not dmMapEngine_store.Is_Object_Map_Exists(OBJ_PROPERTY) then
       dmMapEngine_store.Open_Object_Map(OBJ_PROPERTY);

//       dmMapEngine_store.RefreshObjectThemas_All;


(*
    oIDList:=TIDList.Create;

    dmPmpSite.GetSubItemList(FID, oIDList);

    oIDList.AddObjName(FID, OBJ_PMP_SITE);

   // dmMapEngine.CreateObjectsByList(oIDList);
    FreeAndNil(oIDList);
*)

  //  if Assigned(IMapAct) then
   //////  	dmAct_Map.MapRefresh;

  ////////  {$ENDIF}

{    dmMapEngine1.CreateObject (otPmpSite, FID);
    dmMapEngine1.Sync(otPmpSectorAnt);
}



  end;

end;



procedure Tdlg_PMP_site_add.row_PropertyEditPropertiesButtonClick( Sender: TObject; AButtonIndex: Integer);
begin
  if cxVerticalGrid1.FocusedRow =row_Property_ then
  begin
    Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otProperty, FRec.Property_ID);
    row_Property_.Tag := FRec.Property_ID;
  end
  else


  if cxVerticalGrid1.FocusedRow =row_site_template then
  begin
    Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otTemplatePmpSite, FTemplate_pmp_Site_ID);
    row_site_template.Tag :=FTemplate_pmp_Site_ID;
  end;
end;


procedure Tdlg_PMP_site_add.row_Property_typeEditPropertiesCloseUp(
  Sender: TObject);
begin
  DoOnProperty_TypeChange;

end;

end.


 (*

//--------------------------------------------------------------
procedure Tdlg_LinkEnd_add.cxVerticalGrid1DrawValue(Sender: TObject; ACanvas:
     TcxCanvas; APainter: TcxvgPainter; AValueInfo: TcxRowValueInfo; var Done: Boolean);
//--------------------------------------------------------------
var



  if not (AValueInfo.Row is TcxEditorRow) then
    Exit;

  oRow:=TcxEditorRow(AValueInfo.Row);
  s:=oRow.Name;

  cx_ColorRow(oRow, [row_Antenna1_Type,
                     row_Antenna1_Height,
                     row_Property,
                     row_Cell_Layer,
                     row_PmpSite], ACanvas,  true, clSilver);

  b:=Eq(AsString(row_equipment_Type.Properties.Value), STR_TYPED);

  if b then
    cx_ColorRow(oRow, [row_LinkEndType],  ACanvas,  true, clSilver)
  else
    cx_ColorRow(oRow, [row_Power_dBm,
                       row_Freq_GHz,
                       row_Sens_ber_3,
                       row_Sens_ber_6,
                       row_signature_height,
                       row_signature_width],  ACanvas,  true, clSilver);

  if (FObjectName = OBJ_PMP_TERMINAL) then
    if (row_AssociateWithPmpSite.Properties.Value=true)
  then
    cx_ColorRow(oRow, [row_PmpSector],  ACanvas,  true, clSilver);
*)