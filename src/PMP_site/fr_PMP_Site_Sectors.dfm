object frame_PMP_Site_Sector: Tframe_PMP_Site_Sector
  Left = 295
  Top = 545
  Width = 1500
  Height = 363
  Caption = 'frame_PMP_Site_Sector'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object cxDBTreeList1: TcxDBTreeList
    Left = 0
    Top = 27
    Width = 1484
    Height = 111
    Align = alTop
    Bands = <
      item
        Caption.AlignHorz = taCenter
        FixedKind = tlbfLeft
      end
      item
        Caption.AlignHorz = taCenter
        Caption.Text = #1057#1077#1082#1090#1086#1088
        Width = 140
      end
      item
        Caption.AlignHorz = taCenter
        Caption.Text = #1055#1088#1080#1077#1084#1086#1087#1077#1088#1077#1076#1072#1090#1095#1080#1082
      end
      item
        Caption.AlignHorz = taCenter
        Caption.Text = #1040#1085#1090#1077#1085#1085#1072
      end
      item
        Caption.AlignHorz = taCenter
        Caption.Text = #1050#1072#1083#1080#1073#1088#1086#1074#1082#1072
      end>
    DataController.DataSource = DataSource2
    DataController.ParentField = 'parent_guid'
    DataController.KeyField = 'guid'
    LookAndFeel.Kind = lfFlat
    OptionsBehavior.CellHints = True
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsBehavior.AutoDragCopy = True
    OptionsBehavior.DragCollapse = False
    OptionsBehavior.ExpandOnDblClick = False
    OptionsBehavior.ExpandOnIncSearch = True
    OptionsBehavior.ShowHourGlass = False
    OptionsCustomizing.BandCustomizing = False
    OptionsCustomizing.BandVertSizing = False
    OptionsCustomizing.ColumnVertSizing = False
    OptionsData.Deleting = False
    OptionsSelection.HideFocusRect = False
    OptionsSelection.InvertSelect = False
    OptionsView.CellTextMaxLineCount = -1
    OptionsView.Bands = True
    OptionsView.Indicator = True
    ParentColor = False
    PopupMenu = PopupMenu1
    Preview.AutoHeight = False
    Preview.MaxLineCount = 2
    RootValue = -1
    TabOrder = 0
    OnDblClick = cxDBTreeList1DblClick11
    OnKeyDown = cxDBTreeList1KeyDown1
    object col__name: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'name'
      Width = 141
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_objname: TcxDBTreeListColumn
      Tag = 1
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      DataBinding.FieldName = 'objname'
      Width = 39
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__id: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      Caption.Text = 'ID'
      DataBinding.FieldName = 'id'
      Width = 29
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__cell_layer_name: TcxDBTreeListColumn
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.ReadOnly = True
      Properties.OnButtonClick = col__cell_layer_namePropertiesButtonClick
      Caption.Text = #1057#1090#1072#1085#1076#1072#1088#1090' '#1089#1077#1090#1080
      DataBinding.FieldName = 'cell_layer_name'
      Width = 115
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_band: TcxDBTreeListColumn
      DataBinding.FieldName = 'band'
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__linkendtype_name: TcxDBTreeListColumn
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.ReadOnly = True
      Properties.OnButtonClick = col__cell_layer_namePropertiesButtonClick
      DataBinding.FieldName = 'LinkEndType_Name'
      Width = 105
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_THRESHOLD_BER_6: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = False
      Properties.OnEditValueChanged = col__trx_Power_WPropertiesEditValueChanged
      DataBinding.FieldName = 'THRESHOLD_BER_6'
      Width = 115
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__trx_power_dBm: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = False
      Properties.OnEditValueChanged = col__trx_Power_WPropertiesEditValueChanged
      DataBinding.FieldName = 'POWER_dBm'
      Width = 76
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__trx_Power_W: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = False
      Properties.OnEditValueChanged = col__trx_Power_WPropertiesEditValueChanged
      DataBinding.FieldName = 'POWER_W'
      Width = 78
      Position.ColIndex = 4
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__trx_freq: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = False
      DataBinding.FieldName = 'TX_FREQ_MHz'
      Width = 40
      Position.ColIndex = 5
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_antennatype_name: TcxDBTreeListColumn
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.ReadOnly = True
      Properties.OnButtonClick = col__cell_layer_namePropertiesButtonClick
      DataBinding.FieldName = 'AntennaType_Name'
      Width = 98
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__height: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = False
      DataBinding.FieldName = 'height'
      Width = 69
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__azimuth: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = False
      Properties.OnEditValueChanged = col__azimuthPropertiesEditValueChanged
      DataBinding.FieldName = 'azimuth'
      Width = 68
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__antenna_loss: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = False
      Caption.Text = 'antenna_loss'
      DataBinding.FieldName = 'antenna_loss_dB'
      Width = 28
      Position.ColIndex = 4
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__tilt: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = False
      DataBinding.FieldName = 'tilt'
      Width = 52
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__calc_model_name: TcxDBTreeListColumn
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.ReadOnly = False
      Properties.OnButtonClick = col__cell_layer_namePropertiesButtonClick
      DataBinding.FieldName = 'calc_model_name'
      Width = 83
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 4
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__k0: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = False
      DataBinding.FieldName = 'k0'
      Width = 36
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 4
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__k0_open: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = False
      DataBinding.FieldName = 'k0_open'
      Width = 53
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 4
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__k0_closed: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = False
      DataBinding.FieldName = 'k0_closed'
      Width = 57
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 4
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__color: TcxDBTreeListColumn
      PropertiesClassName = 'TcxColorComboBoxProperties'
      Properties.CustomColors = <>
      Properties.ShowDescriptions = False
      DataBinding.FieldName = 'color'
      Width = 50
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__calc_Radius: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = False
      Caption.Text = 'calc_Radius'
      DataBinding.FieldName = 'calc_radius_km'
      Width = 90
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 255
    Top = 188
    object act_Add_cell: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1077#1082#1090#1086#1088
      OnExecute = act_EditExecute
    end
    object act_Add_Antenna: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1072#1085#1090#1077#1085#1085#1091
      OnExecute = act_EditExecute
    end
    object act_Edit: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      OnExecute = act_EditExecute
    end
    object act_Del: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = act_EditExecute
    end
    object act_Setup_Dlg: TAction
      Caption = 'act_Setup_Dlg'
      OnExecute = act_EditExecute
    end
    object act_Antenna_Update: TAction
      Caption = 'act_Antenna_Update'
      OnExecute = act_EditExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 156
    Top = 188
    object N2: TMenuItem
      Action = act_Add_cell
    end
    object N5: TMenuItem
      Action = act_Add_Antenna
    end
    object actAntennaUpdate1: TMenuItem
      Action = act_Antenna_Update
    end
    object N1: TMenuItem
      Action = act_Edit
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N3: TMenuItem
      Action = act_Del
    end
  end
  object DataSource2: TDataSource
    DataSet = ADOStoredProc1
    Left = 32
    Top = 228
  end
  object ADOStoredProc1: TADOStoredProc
    AfterPost = ADOQuery1AfterPost
    Parameters = <>
    Left = 32
    Top = 184
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 160
    Top = 248
    DockControlHeights = (
      0
      0
      27
      0)
    object dxBarManager1Bar1: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 1599
      FloatTop = 937
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1054#1087#1077#1088#1072#1094#1080#1080
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton4'
        end>
    end
    object dxBarButton1: TdxBarButton
      Action = act_Add_cell
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = act_Edit
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = act_Add_Antenna
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = act_Del
      Category = 0
    end
  end
end
