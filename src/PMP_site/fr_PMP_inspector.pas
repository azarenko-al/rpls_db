 unit fr_PMP_inspector;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, rxPlacemnt, StdCtrls, ExtCtrls, Math,
               
  fr_DBInspector_Container,
  
 // fr_Object_inspector,

  dm_Terminal,

  u_func,
  u_db,
  u_dlg,

  u_const_db,
  u_types

  ;

type
  Tframe_PMP_inspector = class(Tframe_DBInspector_Container)

//  Tframe_PMP_inspector = class(Tframe_Object_inspector)
    procedure FormCreate(Sender: TObject);
  private
    procedure DoOnFieldChanged (Sender: TObject; aFieldName: string; aNewValue: Variant); override;
  public
    class function ExecDlg (aID: integer): boolean;

  end;


//==================================================================
implementation {$R *.dfm}
//==================================================================


//--------------------------------------------------------------------
class function Tframe_PMP_inspector.ExecDlg (aID: integer): boolean;
//--------------------------------------------------------------------
begin
  with Tframe_PMP_inspector.Create(Application) do
  try

(*    View (aID);
    //Ffrm_DBInspector.Options.AutoCommit:=False;
    pn_Buttons.Visible:=True;

    Result:=(ShowModal=mrOK);
*)
  finally
    Free;
  end;

end;


//-------------------------------------------------------------------
procedure Tframe_PMP_inspector.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;
  PrepareViewForObject (OBJ_PMP_SITE);

end;


//-------------------------------------------------------------------
procedure Tframe_PMP_inspector.DoOnFieldChanged (Sender: TObject; aFieldName: string;
                                                aNewValue: Variant);
//-------------------------------------------------------------------
var rec: TdmTerminalTypeAddRec;
begin
  //------------------------------
  if Eq(aFieldName, FLD_TERMINAL_ID) then
  //------------------------------
    if dmTerminalType.GetInfo (aNewValue, rec) then
      begin
        SetFieldValue (FLD_Terminal_Power_W,      rec.Power_W);
        SetFieldValue (FLD_Terminal_Sensitivity,  rec.SENSITIVITY);
        SetFieldValue (FLD_TERMINAL_HEIGHT, rec.Height);
        SetFieldValue (FLD_Terminal_Loss,   rec.Loss);
      end;
end;


end.
