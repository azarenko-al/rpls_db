program dll_Link_Calc;

uses
  dm_Link_calc in '..\src\dm_Link_calc.pas' {dmLink_calc: TDataModule},
  dm_Main_app in '..\src\dm_Main_app.pas' {dmMain_link_calc: TDataModule},
  Forms,
  u_ini_LinkCalcParams in '..\src_shared\u_ini_LinkCalcParams.pas',
  u_link_calc_classes_main in '..\src\u_link_calc_classes_main.pas',
  u_link_calc_result in '..\src\u_link_calc_result.pas',
  u_Link_CalcRains in '..\..\Link\u_Link_CalcRains.pas',
  u_rrl_param_rec_new in '..\src_shared\u_rrl_param_rec_new.pas',
  u_XML_bind_rrl_result in '..\src\u_XML_bind_rrl_result.pas',
  u_xml_document in '..\..\..\..\common\Package_Common\u_xml_document.pas',
  xml_rrl in '..\src\xml_rrl.pas',
  u_link_calc_classes_export in '..\src\u_link_calc_classes_export.pas';

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(TdmMain_link_calc, dmMain_link_calc);
  Application.Run;
end.
