unit u_ini_LinkCalcParams;

interface
uses
   IniFiles, SysUtils, Dialogs,

   u_classes,

   u_files,
   u_func
   ;

type
  TLink_Calc_Params_Mode = (mtNone, mtBuildGraph, mtOptimize_equip, mtAdaptiveModulation);

//  DEF_ARR: array[TLink_Calc_Params_Mode] of string = ( '', 'BuildGraph', 'Optimize_equip', 'AdaptiveModulation' );




  TIni_Link_Calc_Params_Result_Rec = record

      Enabled: boolean;


       MODE            : Integer;
       THRESHOLD_BER_3 : Double;
       THRESHOLD_BER_6 : Double;
       Power_dBm       : Double;
       BitRate_Mbps    : Double;

       Modulation_type : String;

       KNG             : Double;
       SESR            : Double;
       Rx_Level_dBm    : Double;
       FADE_MARGIN_DB  : Double;
   //    KNG_required    : Double;
    //   SESR_required   : Double;

       Is_Working      : Boolean;


       LinkEndType_name : string;
       antennaType_name : string;

       LinkEndType_id : Integer;
       LinkEndType_mode_id : Integer;

       antennaType_id : Integer;
       diameter  : Double;


  end;





  TIni_Link_Calc_Params = class(TObject)
  private
    FIniFileName: string;


  public
    IDList: TIDList;

  public
    Auto: record
      LinkEndType_IDList : TIDList;
      AntType_IDList     : TIDList;

    end;




    Handle : Integer;


    Mode: TLink_Calc_Params_Mode; // (mtNone, mtBuildGraph, mtOptimize_equip, mtAdaptiveModulation);

    IsDebug_ShowXML : Boolean;
    IsCalcBothSides : Boolean;

    ProjectID       : Integer;
    LinkID          : Integer;

    IsRunBothSides : Boolean;
 //     Params.IsUsePassiveElements  :=True;


//    IsUsePassiveElements       : boolean;
//    IsCalcWithAdditionalRain : Boolean;

 //   IsSaveReportToDB         : Boolean;

 //   IsUseAdaptiveModulation : Boolean;

    BuildGraph: record
                  Active : Boolean;

                  ArgGroup : Integer;
                  NArg : Integer;

                  NFunc1 : Integer;
                  NFunc2 : Integer;

               //   BLVector: TBLVector;
                end;

    Optimize: record
       Enabled : Boolean;

       Site1_HEIGHT  : double;
       Site2_HEIGHT  : double;

       Dop_Site2_HEIGHT : double;
    end;


    Bitrate_min : Integer;


    Results: TIni_Link_Calc_Params_Result_Rec;


{
    record

           MODE            : Integer;
           THRESHOLD_BER_3 : Double;
           THRESHOLD_BER_6 : Double;
           Power_dBm       : Double;
           BitRate_Mbps    : Double;

           Modulation_type : String;

           KNG             : Double;
           SESR            : Double;
           Rx_Level_dBm    : Double;
           FADE_MARGIN_DB  : Double;
       //    KNG_required    : Double;
        //   SESR_required   : Double;

           Is_Working      : Boolean;

    end;
}


    IsShowXMLFiles : boolean;

{

  if AsBoolean(GetGridValue(TAG_ROW_TX_ANTENNA, col_Check.Index)) then
    FrrlCalcParam.TTX.Site1_HEIGHT:=- AsInteger(GetGridValue(TAG_ROW_TX_ANTENNA, col_H_max.Index));

  if AsBoolean(GetGridValue(TAG_ROW_RX_ANTENNA, col_Check.Index)) then
    FrrlCalcParam.TTX.Site2_HEIGHT:=- AsInteger(GetGridValue(TAG_ROW_RX_ANTENNA, col_H_max.Index));

  if AsBoolean(GetGridValue(TAG_ROW_RX_ANTENNA_DOP, col_Check.Index)) then
    FrrlCalcParam.TTX.Dop_Site2.HEIGHT:=- AsInteger(GetGridValue(TAG_ROW_RX_ANTENNA_DOP, col_H_max.Index));

}


    constructor Create;
    destructor Destroy; override;


    procedure Results_SaveToFile_______(aFileName: string);

    procedure LoadFromFile(aFileName: string);
    procedure SaveToFile(aFileName: string);

    function Validate: boolean;
  end;




implementation

const
  DEF_RESULTS = 'Results';




const
  DEF_ARR: array[TLink_Calc_Params_Mode] of string =
  //  Mode: TLink_Calc_Params_Mode;
//  end =
  (
    '',
    'BuildGraph',
    'Optimize_equip',
    'AdaptiveModulation'
  );

  
{
 case Mode of
      mtNone:               sMode :='';
      mtBuildGraph:         sMode :='BuildGraph';
      mtmtOptimize_equip:   sMode :='mtOptimize_equip';
      mtAdaptiveModulation: sMode :='AdaptiveModulation';
    end;}



constructor TIni_Link_Calc_Params.Create;
begin
  inherited;

  IDList := TIDList.Create;


  Auto.LinkEndType_IDList := TIDList.Create;
  Auto.AntType_IDList     := TIDList.Create;


end;

// ---------------------------------------------------------------
destructor TIni_Link_Calc_Params.Destroy;
// ---------------------------------------------------------------
begin
  inherited;

  FreeAndNil(IDList);

  FreeAndNil(Auto.LinkEndType_IDList);
  FreeAndNil(Auto.AntType_IDList);

end;

// ---------------------------------------------------------------
procedure TIni_Link_Calc_Params.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
const
  DEF_main = 'main';

  DEF_Repeater = 'Repeater';
  DEF_BuildGraph = 'BuildGraph';
var
  oIniFile: TIniFile;
  sMode: string;

begin
  FIniFileName:=aFileName;

  Assert(FileExists(aFileName), aFileName);


  oIniFile := TIniFile.Create(aFileName);

  with oIniFile do
  begin
    sMode := ReadString ('main', 'Mode', '');

    if Eq(sMode,'BuildGraph')           then Mode :=mtBuildGraph else
    if Eq(sMode,'AdaptiveModulation')   then Mode :=mtAdaptiveModulation else
    if Eq(sMode,'Optimize_equip')        then Mode :=mtOptimize_equip;


    Handle :=ReadInteger (DEF_main, 'Handle', 0);



    IsDebug_ShowXML := ReadBool ('main', 'IsDebug_ShowXML', False);

    ProjectID                :=ReadInteger (DEF_main, 'ProjectID', 0);
    LinkID                   :=ReadInteger (DEF_main, 'LinkID',    0);

//    IsSaveReportToDB         :=ReadBool    ('main', 'IsSaveReportToDB',  True);

//    isUsePassiveElements     :=ReadBool    ('main', 'UsePassiveElements',  false);
//    IsCalcWithAdditionalRain :=ReadBool    ('main', 'IsCalcWithAdditionalRain',  false);

    IsRunBothSides :=ReadBool    (DEF_main, 'IsRunBothSides',  false);


//    IsUseAdaptiveModulation :=ReadBool    ('main', 'IsUseAdaptiveModulation',  false);


    BuildGraph.Active   :=ReadBool    (DEF_BuildGraph, 'Active', False);
    BuildGraph.ArgGroup :=ReadInteger (DEF_BuildGraph, 'ArgGroup', 0);
    BuildGraph.NArg     :=ReadInteger (DEF_BuildGraph, 'NArg',   0);
    BuildGraph.NFunc1   :=ReadInteger (DEF_BuildGraph, 'NFunc1', 0);
    BuildGraph.NFunc2   :=ReadInteger (DEF_BuildGraph, 'NFunc2', 0);


    Optimize.Enabled:=ReadBool    ('Optimize', 'Enabled', False);
    Optimize.Site1_HEIGHT:=ReadFloat   ('Optimize', 'Site1_HEIGHT',  0);
    Optimize.Site2_HEIGHT:=ReadFloat   ('Optimize', 'Site2_HEIGHT',  0);
    Optimize.Dop_Site2_HEIGHT:=ReadFloat   ('Optimize', 'Dop_Site2_HEIGHT',  0);


    Bitrate_min:=ReadInteger   ('main', 'Bitrate_min', 0 );


    IsShowXMLFiles :=ReadBool    ('main', 'IsShowXMLFiles', False);


  end;


  Auto.LinkEndType_IDList.LoadIDsFromIniSection(oIniFile, 'LinkEndType');
  Auto.AntType_IDList.LoadIDsFromIniSection(oIniFile, 'AntType');


  with oIniFile do
  begin

    Results.MODE            :=ReadInteger   (DEF_RESULTS, 'MODE', 0 );
    Results.THRESHOLD_BER_3 :=ReadFloat   (DEF_RESULTS, 'THRESHOLD_BER_3', 0 );
    Results.THRESHOLD_BER_6 :=ReadFloat   (DEF_RESULTS, 'THRESHOLD_BER_6', 0 );
    Results.Power_dBm       :=ReadFloat   (DEF_RESULTS, 'Power_dBm', 0 );
    Results.BitRate_Mbps    :=ReadFloat   (DEF_RESULTS, 'BitRate_Mbps', 0 );
    Results.Modulation_type :=ReadString    (DEF_RESULTS, 'Modulation_type', '' );
    Results.KNG             :=ReadFloat   (DEF_RESULTS, 'KNG', 0 );
    Results.SESR            :=ReadFloat   (DEF_RESULTS, 'SESR', 0 );

    Results.Rx_Level_dBm    :=ReadFloat   (DEF_RESULTS, 'Rx_Level_dBm', 0 );
    Results.FADE_MARGIN_DB  :=ReadFloat   (DEF_RESULTS, 'FADE_MARGIN_DB', 0 );

    Results.Is_Working      :=ReadBool  (DEF_RESULTS, 'Is_Working', False);

    Results.LinkEndType_name :=ReadString   (DEF_RESULTS, 'LinkEndType_name', '' );
    Results.antennaType_name :=ReadString   (DEF_RESULTS, 'antennaType_name', '' );
    Results.Rx_Level_dBm     :=ReadFloat  (DEF_RESULTS, 'Rx_Level_dBm', 0 );

    Results.LinkEndType_name := ReadString  (DEF_RESULTS, 'LinkEndType_name', '');
    Results.antennaType_name := ReadString  (DEF_RESULTS, 'antennaType_name', '');

    Results.LinkEndType_id := ReadInteger  (DEF_RESULTS, 'LinkEndType_id', 0);
    Results.antennaType_id := ReadInteger  (DEF_RESULTS, 'antennaType_id', 0);
    Results.LinkEndType_mode_id := ReadInteger  (DEF_RESULTS, 'LinkEndType_mode_id', 0);

    Results.diameter     :=ReadFloat  (DEF_RESULTS, 'diameter', 0 );



  end;




  FreeAndNil(oIniFile);


  IDList.LoadIDsFromIniFile(aFileName, 'items');
end;

// ---------------------------------------------------------------
procedure TIni_Link_Calc_Params.SaveToFile(aFileName: string);
// ---------------------------------------------------------------
const
  DEF_main = 'main';


  DEF_Repeater = 'Repeater';
  DEF_BuildGraph = 'BuildGraph';
var
  oIniFile: TIniFile;
  sFileName: string;
  sMode: string;

begin
//  FIniFileName

  if aFileName<>'' then
    FIniFileName:=aFileName;


  sFileName:= ChangeFileExt(FIniFileName, '.ini');

  ForceDirByFileName(sFileName);

  DeleteFile(sFileName);


  oIniFile := TIniFile.Create(sFileName);

  with oIniFile do
  begin
    case Mode of
      mtNone:               sMode :='';
      mtBuildGraph:         sMode :='BuildGraph';
      mtOptimize_equip:     sMode :='Optimize_equip';
      mtAdaptiveModulation: sMode :='AdaptiveModulation';
    else
      raise Exception.Create('');
    end;


    
    WriteInteger (DEF_main, 'Handle', Handle);



    WriteBool ('main', 'IsDebug_ShowXML', IsDebug_ShowXML);

    WriteString ('main', 'Mode', sMode);

    WriteInteger ('main', 'ProjectID', ProjectID);
    WriteInteger ('main', 'LinkID',    LinkID);

//    WriteBool    ('main', 'IsSaveReportToDB',  IsSaveReportToDB);

//    WriteBool    ('main', 'UsePassiveElements',  isUsePassiveElements);
//    WriteBool    ('main', 'IsCalcWithAdditionalRain',  IsCalcWithAdditionalRain);

    WriteBool    ('main', 'IsRunBothSides',  IsRunBothSides);

   // WriteBool    ('main', 'IsUseAdaptiveModulation',  IsUseAdaptiveModulation);


    WriteBool    ('Optimize', 'Enabled',  Optimize.Enabled);

    WriteFloat   ('Optimize', 'Site1_HEIGHT',  Optimize.Site1_HEIGHT);
    WriteFloat   ('Optimize', 'Site2_HEIGHT',  Optimize.Site2_HEIGHT);
    WriteFloat   ('Optimize', 'Dop_Site2_HEIGHT',  Optimize.Dop_Site2_HEIGHT);

    WriteFloat   ('main', 'Bitrate_min',  Bitrate_min);

    WriteBool    ('main', 'IsShowXMLFiles', IsShowXMLFiles);
         

{ Optimize: record
       Enabled : Boolean;

       Site1_HEIGHT  : double;
       Site2_HEIGHT  : double;

       Dop_Site2_HEIGHT : double;
    end;

}

  end;



  with oIniFile do
    if Results.Enabled then
  begin

    WriteInteger (DEF_RESULTS, 'MODE',  Results.MODE);
    WriteFloat   (DEF_RESULTS, 'THRESHOLD_BER_3',  Results.THRESHOLD_BER_3);
    WriteFloat   (DEF_RESULTS, 'THRESHOLD_BER_6',  Results.THRESHOLD_BER_6);
    WriteFloat   (DEF_RESULTS, 'Power_dBm',  Results.Power_dBm);
    WriteFloat   (DEF_RESULTS, 'BitRate_Mbps',  Results.BitRate_Mbps);

    WriteString  (DEF_RESULTS, 'Modulation_type',  Results.Modulation_type);

    WriteFloat   (DEF_RESULTS, 'KNG',  Results.KNG);
    WriteFloat   (DEF_RESULTS, 'SESR',  Results.SESR);
    WriteFloat   (DEF_RESULTS, 'Rx_Level_dBm',  Results.Rx_Level_dBm);

    WriteFloat   (DEF_RESULTS, 'FADE_MARGIN_DB',  Results.FADE_MARGIN_DB);
    WriteBool    (DEF_RESULTS, 'Is_Working',  Results.Is_Working);


    WriteString  (DEF_RESULTS, 'LinkEndType_name',  Results.LinkEndType_name);
    WriteString  (DEF_RESULTS, 'antennaType_name',  Results.antennaType_name);

    WriteInteger (DEF_RESULTS, 'LinkEndType_id',  Results.LinkEndType_id);
    WriteInteger (DEF_RESULTS, 'antennaType_id',  Results.antennaType_id);
    WriteInteger (DEF_RESULTS, 'LinkEndType_mode_id',  Results.LinkEndType_mode_id);

    WriteFloat   (DEF_RESULTS, 'diameter',  Results.diameter);


  end;

  Auto.LinkEndType_IDList.SaveIDsToIniFile1(oIniFile, 'LinkEndType');
  Auto.AntType_IDList.SaveIDsToIniFile1(oIniFile, 'AntType');


  FreeAndNil(oIniFile);


  IDList.SaveIDsToIniFile(sFileName, 'items');

end;


// ---------------------------------------------------------------
procedure TIni_Link_Calc_Params.Results_SaveToFile_______(aFileName: string);
// ---------------------------------------------------------------
const
  DEF_Repeater = 'Repeater';
  DEF_BuildGraph = 'BuildGraph';
var
  oIniFile: TIniFile;
  sMode: string;

begin
  aFileName:= ChangeFileExt(aFileName, '.ini');

  ForceDirByFileName(aFileName);


  oIniFile := TIniFile.Create(aFileName);

  with oIniFile do
  begin



    WriteFloat   ('Optimize', 'Site1_HEIGHT',  Optimize.Site1_HEIGHT);
    WriteFloat   ('Optimize', 'Site2_HEIGHT',  Optimize.Site2_HEIGHT);
    WriteFloat   ('Optimize', 'Dop_Site2_HEIGHT',  Optimize.Dop_Site2_HEIGHT);


{ Optimize: record
       Enabled : Boolean;

       Site1_HEIGHT  : double;
       Site2_HEIGHT  : double;

       Dop_Site2_HEIGHT : double;
    end;

}

  end;


  FreeAndNil(oIniFile);




end;




function TIni_Link_Calc_Params.Validate: boolean;
begin
//  Result := (LinkID>0) and (ProjectID>0);
  Result := (ProjectID>0);

  if not Result then
    ShowMessage('(iProjectID=0)');
    
end;


end.





