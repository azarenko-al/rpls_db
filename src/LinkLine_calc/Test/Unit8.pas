unit Unit8;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,

  I_Link_calc,

  u_files,
  u_db,

  u_vars,

  dm_Main, StdCtrls
  ;

type
  TForm8 = class(TForm)
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form8: TForm8;

implementation

{$R *.dfm}

procedure TForm8.FormCreate(Sender: TObject);
begin

  TdmMain.Init;
  if not dmMain.OpenDB_reg then
//  if not dmMain.OpenFromReg then
    Exit;


end;

procedure TForm8.Button1Click(Sender: TObject);

const
  DEF_SOFTWARE_NAME= 'RPLS_DB_LINK';
  DEF_TEMP_FILENAME = 'link_calc.ini';

var
  sIniFileName: string;
//  oParams: TIni_Link_Calc_Params;

begin
  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
//    sIniFileName:=TEMP_INI_FILE;
    sIniFileName:=GetCommonApplicationDataDir(DEF_SOFTWARE_NAME)+
                  DEF_TEMP_FILENAME;


//  sIniFileName:=GetCommonApplicationDataDir(DEF_SOFTWARE_NAME)+
 //                 DEF_TEMP_FILENAME;


 // sIniFileName:=ParamStr(1);
//  if sIniFileName='' then
    sIniFileName:=g_ApplicationDataDir+ DEF_TEMP_FILENAME;


  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;

  //---------------------------------------------------------------------

(*
  g_Ini_LOS_Params:=TIni_LOS_Params.Create;
  if not g_Ini_LOS_Params.LoadFromFile(sIniFileName) then
    Exit;
*)

(*
  TdmMain.Init;
  if not dmMain.OpenDB_reg then
//  if not dmMain.OpenFromReg then
    Exit;

*)


  if Load_ILink_calc() then
  begin
    Assert(Assigned(dmMain), 'Value not assigned');

    ILink_calc.InitADO( dmMain.ADOConnection1.ConnectionObject);

  //  ILink_calc.Execute(sIniFileName);


    UnLoad_ILink_calc();
  end;




//  FreeAndNil(dmMain);




end;



end.

