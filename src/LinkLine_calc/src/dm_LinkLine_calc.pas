unit dm_LinkLine_calc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, Variants,

  dm_Onega_DB_data,

 // I_Act_Link,  //  uses dm_Act_Link;


  dm_Main,

  u_classes,
  u_dlg,
  u_func,
  u_db,

  dm_Link,
  dm_Link_Calc_SESR,
  dm_LinkLine,

  u_types,

  u_linkLine_const,
  u_const_db,
  u_link_const,

  dm_Progress

  ;

type
  TdmLinkLine_calc = class(TdmProgress)
    qry_Links: TADOQuery;
    qry_Summary: TADOQuery;
    qry_LinkLine: TADOQuery;
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);

  private
    FLinkLineID: integer;
    FIsReFreshCalcs: boolean;
//    function GetStatus_(aID: integer): integer;

 protected

    procedure ExecuteProc; override;

  public
    procedure UpdateSummary (aID: integer);
    procedure Dlg_Exec(aID: Integer);
  end;


function dmLinkLine_calc: TdmLinkLine_calc;

//==================================================================
implementation
 {$R *.DFM}

uses
  dm_Act_Link;


var
  FdmLinkLine_calc: TdmLinkLine_calc;



function dmLinkLine_calc: TdmLinkLine_calc;
begin
  if not Assigned(FdmLinkLine_calc) then
    FdmLinkLine_calc:=TdmLinkLine_calc.Create (Application);

  Result:= FdmLinkLine_calc;
end;


//--------------------------------------------------------------
procedure TdmLinkLine_calc.Dlg_Exec(aID: Integer);
//--------------------------------------------------------------
begin
  case MessageBox(0, '���������� ��� ��������� ������?',
                    '������ �����', MB_ICONWARNING or MB_YESNOCANCEL) of
    IDYES: FIsReFreshCalcs:=True;
    IDNO:  FIsReFreshCalcs:=False;
  else
   // IDCANCEL:
    Exit;
  end;



 // FIsReFreshCalcs:=ConfirmDlg('���������� ��� ��������� ������?');
//      dmLinkLine_Calc.FIsReFreshCalcs:=True;

  FLinkLineID:= aID;
  ExecuteDlg ('������ �����');

end;

//--------------------------------------------------------------------
procedure TdmLinkLine_calc.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
  db_SetComponentADOConnection (Self, dmMain.ADOConnection);

end;

//--------------------------------------------------------------------
procedure TdmLinkLine_calc.ExecuteProc;
//--------------------------------------------------------------------
const
  SQL_SELECT_LINKLINE_LINKS =
     'SELECT id,name FROM '+ view_LinkLine_links + ' WHERE (linkline_id=:id)';

//  SQL_SELECT_LINK =
 //    'SELECT SESR,kng,length FROM '+ VIEW_link + ' WHERE (id=:id)';

var
  I: Integer;
 // oNotCalcedList: TIDList;

  oIDList: TIDList;

begin
  if FIsReFreshCalcs then
  begin

     // oNotCalcedList:= TIDList.Create;
      oIDList:= TIDList.Create;

      dmOnega_DB_data.OpenQuery (qry_Links, SQL_SELECT_LINKLINE_LINKS, [FLD_ID, FLinkLineID]);

      with qry_Links do
        while not EOF do
      begin
        oIDList.AddName(qry_Links[FLD_ID], qry_Links[FLD_NAME]);

    {
        if FIsReFreshCalcs then
          oIDList.AddName(qry_Links[FLD_ID], qry_Links[FLD_NAME])
        else
        begin
          db_OpenQuery (qry_Temp, SQL_SELECT_LINK, [db_Par(FLD_ID, qry_Links[FLD_ID])]);

          if VarIsNull(qry_Temp[FLD_SESR]) or
             VarIsNull(qry_Temp[FLD_KNG]) or
             VarIsNull(qry_Temp[FLD_LENGTH])
          then
            oIDList.AddName(qry_Links[FLD_ID], qry_Links[FLD_NAME]);
        end;
    }

        Next;
      end;


      dmAct_Link.Calc_list (oIDList);


      FreeAndNil (oIDList);
  end;
    



{

//  if oNotCalcedList.Count = 0 then
 //   DoProgress(1, 1);

  for I := 0 to oNotCalcedList.Count-1 do
  begin
    oIDList.Ad
    DoProgress(i, oNotCalcedList.Count-1);
    DoProgressMsg('������ ���������: '+oNotCalcedList[i].Name);

   // Assert(Assigned(ILink));

    dmAct_Link.Calc (oNotCalcedList[i].ID);
  end;    // for

  FreeAndNil(oNotCalcedList);
}


  UpdateSummary(FLinkLineID);
end;


//--------------------------------------------------------------------
procedure TdmLinkLine_calc.UpdateSummary (aID: integer);
//--------------------------------------------------------------------
const
  SQL_LinkLine_links =
     'SELECT SUM(SESR) AS SESR_, SUM(Kng) AS Kng_, SUM(length) AS Length_ '+
     ' FROM '+ view_LinkLine_links +
     ' WHERE (linkline_id=:id)';

  SQL_UPDATE_LINKLINE =
     'UPDATE '+ TBL_LINKLINE +
     ' SET SESR=:SESR, Kng=:Kng, Length=:Length '+
     ' WHERE id=:id';

var
  iGST_Type, iStatus: integer;
  dKng_norm, dLength_norm_Km, dSesr_norm,
  dKng_req, dSesr_req,
  dSesr, dKng,
  dReal_length_km: double;

  eBitrate_Mbps : double;
           

  rec: TCalcEsrBberParamRec;

  eSummary_KNG: Double;
  eSummary_SESR: Double;
  eSummary_LENGTH_M: Double;
begin
  dSesr_req:=0;
  dKng_req :=0;

  dmOnega_DB_data.LinkLine_Update_Summary(aID);

  dmOnega_DB_data.OpenQuery (qry_Summary, SQL_LinkLine_links, [FLD_ID, aID]);
                           

  eSummary_KNG      := qry_Summary.FieldByName(FLD_KNG+'_').AsFloat;
  eSummary_SESR     := qry_Summary.FieldByName(FLD_SESR+'_').AsFloat;
  eSummary_LENGTH_M := qry_Summary.FieldByName(FLD_LENGTH+'_').AsFloat;


  with qry_Summary do
    gl_DB.ExecCommand (SQL_UPDATE_LINKLINE,
                      [
                       db_Par(FLD_ID, aID),

                       db_Par(FLD_KNG,    eSummary_KNG),
                       db_Par(FLD_SESR,   eSummary_SESR),

                       db_Par(FLD_LENGTH, eSummary_LENGTH_M)
                       ]);


  db_OpenTableByID (qry_LinkLine, TBL_LINKLINE, aID);
  if qry_LinkLine.IsEmpty then
    Exit;

  with qry_LinkLine do
  begin
    //��� �����
    iGST_Type       :=FieldByName(FLD_GST_TYPE).AsInteger;

    dReal_length_km :=eSummary_LENGTH_M / 1000 ;

    dSesr           :=FieldByName(FLD_SESR).AsFloat;
    dKng            :=FieldByName(FLD_KNG).AsFloat;

    dLength_norm_Km :=FieldByName(FLD_GST_LENGTH).AsFloat;
    dSesr_norm      :=FieldByName(FLD_GST_SESR).AsFloat;
    dKng_norm       :=FieldByName(FLD_GST_KNG).AsFloat;
  end;

  if dLength_norm_Km=0 then
    Exit;

  if (dReal_length_km > dLength_norm_Km) then
    dReal_length_km:= dLength_norm_Km;


//  if aGSTType > 0 then
//  begin
//    aEsrNorm :=100*(d_A_ESR* LINKLINE_NORM_ARR[aGSTType].Param_C);
//    TruncFloatVar(aEsrNorm, 6);
//
//    aBberNorm:=100*(d_A_BBER*LINKLINE_NORM_ARR[aGSTType].Param_C);
//    TruncFloatVar(aBberNorm, 6);
//
//    CalcRequiredEsr(//aRec,
//                    aGSTType, aLengthKM, aEsrNorm, aBberNorm,
//                    aEsrReq, aBberReq);
//  end
//

//procedure TdmLink_calc_SESR.CalcRequiredESR(
//        //  var aRec: TdmLink_calc_SESR_rec;
//         aGSTType: integer;
//
//         aLength_KM: double;
//         aEsrNorm, aBberNorm: double;
//
//         var aEsrReq, aBberReq: double);



  if iGST_Type > 0 then
    dmLink_Calc_SESR.CalcRequiredEsr(//rec,
                  iGST_Type, dReal_length_km,
                  dSesr_norm, dKng_norm,
                  dSesr_req, dKng_req)
  else
  begin
    dSesr_req:= dSesr_norm * dReal_length_km / dLength_norm_Km;
    dKng_req := dKng_norm  * dReal_length_km / dLength_norm_Km;
  end;

  eBitrate_Mbps:=qry_LinkLine.FieldByName(FLD_BitRate_Mbps).AsFloat;

//  iBitrate_Mbps:=gl_DB.GetFieldValueByID(TBL_LINKLINE, FLD_SPEED, aID,'');
 // if iBitrate_Mbps = 0 then
  //  iBitrate_Mbps:=dmLinkLine.GetMinBitRate_Mbps(aID);

  //------------------------------------------------------
  FillChar(rec, SizeOf(rec), 0);

  rec.ID      :=aID;
  rec.ObjName :=OBJ_LINKLINE;
  rec.Length_KM:=dReal_length_km;
  rec.GST_Type :=iGST_Type;
  rec.LinkLineSpeed_Mbps := eBitrate_Mbps;



  dmLink_Calc_SESR.CalcEsrBber11(rec

(*              aID, OBJ_LINKLINE,
              dReal_length_km, iGST_Type,
             dEsrNorm, dEsrReq, dBberNorm, dBberReq, iBitrate_Mbps
*)
             );

  //------------------------------------------------------

  if (dKng  > 100)  then dKng  := 99.99;
  if (dSesr > 100)  then dSesr := 99.99;

//  iStatus:=qry_LinkLine.FieldByName(FLD_STATUS).AsInteger;



//  iStatus:=GetStatus_(aID);

//  if iStatus = 1 then  // �����
  iStatus:=IIF((dKng_req >= dKng) and (dSesr_req >= dSesr), 1, 2);

//  dKng_year:=TruncFloat(0.01 * dKng * 365 * 24 * 60, 1);

  dmLinkLine.Update_ (aID,
     [
      db_Par(FLD_STATUS,        iStatus),

      db_Par(FLD_ESR_NORM,      rec.output.Esr_Norm),
      db_Par(FLD_BBER_NORM,     rec.output.Bber_Norm),
      db_Par(FLD_ESR_REQUIRED,  rec.output.Esr_Required),
      db_Par(FLD_BBER_REQUIRED, rec.output.Bber_Required),

      db_Par(FLD_KNG_REQUIRED,  TruncFloat(dKNG_req,  6)),
      db_Par(FLD_SESR_REQUIRED, TruncFloat(dSESR_req, 6)),


      db_Par(FLD_KNG,           dKng),
      db_Par(FLD_SESR,          dSesr)
      ]);

end;


end.



