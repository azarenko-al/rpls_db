unit u_const_sql;

interface
 

implementation

end.


  (*
{
uses u_const_str;

const
  SQL_VIEW_CELL_NB =
    'SELECT  TOP 100 PERCENT Cell.id, Site.number + '' | '' + Site.name + '' | '' +   '+
    '                                 Property.address AS site_name, Cell.CELLID,     '+
    '        Cell.name AS cell_name, Cell.BCC, Cell.BCCH, Cell.LAC, Cell.BSIC,        '+
    '        Site.project_id, CAST(Site.number AS int)                                '+
    '        AS number_int, Antenna.lat, Antenna.lon                                  '+
    ' FROM   Property INNER JOIN                                                      '+
    '        Site ON Property.id = Site.property_id RIGHT OUTER JOIN                  '+
    '        AntennaXREF INNER JOIN                                                   '+
    '        Cell ON AntennaXREF.cell_id = Cell.id INNER JOIN                         '+
    '        Antenna ON AntennaXREF.antenna_id = Antenna.id ON Site.id = Cell.site_id ';

{  SQL_VIEW_CELL_NB2 =
    'SELECT  TOP 100 PERCENT Cell_1.id AS cell_id, Cell_1.name AS cell_name,          '+
    '        Cell_2.name AS ncell_name, dbo.Cell_Neighbour.project_id,                '+
    '        dbo.Cell_Neighbour.margin, dbo.Cell_Neighbour.id,                        '+
    '        dbo.Cell_Neighbour.ncell_id, Cell_2.LAC AS nLAC, Cell_2.BSIC AS nBSiC,   '+
    '        Cell_2.CELLID AS nCellID, Cell_1.cell_layer_id,                          '+
    '        Cell_2.cell_layer_id AS ncell_layer_id, dbo.Antenna.lat AS nLat,         '+
    '        dbo.Antenna.lon AS nLon                                                  '+
    ' FROM   dbo.Site RIGHT OUTER JOIN                                                '+
    '        dbo.Cell Cell_2 ON dbo.Site.id = Cell_2.site_id LEFT OUTER JOIN          '+
    '        dbo.AntennaXREF LEFT OUTER JOIN dbo.Antenna ON                           '+
    '        dbo.AntennaXREF.antenna_id = dbo.Antenna.id ON                           '+
    '        Cell_2.id = dbo.AntennaXREF.cell_id RIGHT OUTER JOIN                     '+
    '        dbo.Cell_Neighbour LEFT OUTER JOIN                                       '+
    '        dbo.Cell Cell_1 ON dbo.Cell_Neighbour.cell_id = Cell_1.id ON             '+
    '        Cell_2.id = dbo.Cell_Neighbour.ncell_id                                  ';
}

  SQL_SELECT_COCHANNEL_FREQS_BY_SECTOR_FREQS =
    'SELECT  DISTINCT Antenna.id                                                                                 '+
    ' FROM   AntennaXREF INNER JOIN FreqPlan_Carriers ON                                                '+
    '        AntennaXREF.cell_id = FreqPlan_Carriers.cell_id RIGHT OUTER JOIN                           '+
    '        Antenna ON AntennaXREF.antenna_id = Antenna.id                                             '+
    ' WHERE  (FreqPlan_Carriers.freqplan_id = :freqplan_id) AND (FreqPlan_Carriers.number IN            '+
    '          (SELECT     number                                                                       '+
    '           FROM       FreqPlan_Carriers                                                            '+
    '           WHERE      (freqplan_id = :freqplan_id) AND (cell_id = :cell_id) AND (state = :state))) '+
    '         AND (FreqPlan_Carriers.state = :state) AND (FreqPlan_Carriers.cell_id <> :cell_id)        ';

(*    'SELECT  FreqPlan_Carriers.cell_id, FreqPlan_Carriers.number,                                       '+
    '        FreqPlan_Carriers.state, Antenna.id, Antenna.lat,                                          '+
    '        Antenna.lon, Antenna.name, Antenna.azimuth, Cell.CELLID, CellLayer.freq_center as freq     '+
    ' FROM   CellLayer RIGHT OUTER JOIN                                                                 '+
    '        Cell ON CellLayer.id = Cell.cell_layer_id RIGHT OUTER JOIN                                 '+
    '        AntennaXREF INNER JOIN                                                                     '+
    '        FreqPlan_Carriers ON AntennaXREF.cell_id = FreqPlan_Carriers.cell_id                       '+
    '        ON Cell.id = AntennaXREF.cell_id RIGHT OUTER JOIN                                          '+
    '        Antenna ON AntennaXREF.antenna_id = Antenna.id                                             '+
    ' WHERE  (FreqPlan_Carriers.freqplan_id = :freqplan_id) AND (FreqPlan_Carriers.number IN            '+
    '           (SELECT     number                                                                      '+
    '           FROM          FreqPlan_Carriers                                                         '+
    '           WHERE      (freqplan_id = :freqplan_id) AND (cell_id = :cell_id) AND (state = :state))) '+
    '         AND (FreqPlan_Carriers.state = :state) AND (FreqPlan_Carriers.cell_id <> :cell_id)        ';*)

  SQL_SELECT_NEIGHBOR_FREQS_BY_SECTOR_FREQS =
(*    'SELECT  FreqPlan_Carriers.cell_id, FreqPlan_Carriers.number,                                                 '+
    '        FreqPlan_Carriers.state, Antenna.id, Antenna.lat,                                                    '+
    '        Antenna.lon, Antenna.name, Antenna.azimuth, Cell.CELLID, CellLayer.freq_center as freq               '+
    ' FROM   CellLayer RIGHT OUTER JOIN                                                                           '+
    '        Cell ON CellLayer.id = Cell.cell_layer_id RIGHT OUTER JOIN AntennaXREF INNER JOIN                    '+
    '        FreqPlan_Carriers ON AntennaXREF.cell_id = FreqPlan_Carriers.cell_id ON Cell.id = AntennaXREF.cell_id'+
    '        RIGHT OUTER JOIN Antenna ON AntennaXREF.antenna_id = Antenna.id                                      '+*)

    'SELECT  DISTINCT Antenna.id                                                                                           '+
    ' FROM   Antenna LEFT OUTER JOIN AntennaXREF INNER JOIN                                                       '+
    '        FreqPlan_Carriers ON AntennaXREF.cell_id = FreqPlan_Carriers.cell_id                                 '+
    '        ON Antenna.id = AntennaXREF.antenna_id                                                               '+
    ' WHERE  (FreqPlan_Carriers.freqplan_id = :freqplan_id) AND (FreqPlan_Carriers.state = :state) AND            '+
    '        ((FreqPlan_Carriers.number - 1) IN                                                                   '+
    '           (SELECT     number                                                                                '+
    '            FROM       FreqPlan_Carriers                                                                     '+
    '            WHERE      (freqplan_id = :freqplan_id) AND (cell_id = :cell_id) AND (state = :state)) OR        '+
    '        (FreqPlan_Carriers.number + 1) IN                                                                    '+
    '           (SELECT     number                                                                                '+
    '            FROM       FreqPlan_Carriers                                                                     '+
    '            WHERE      (freqplan_id = :freqplan_id) AND (cell_id = :cell_id) AND (state = :state)))          ';




implementation

end.





const
{
  SQL_TOOL_EMP_SELECT_GSM_CELL_INFO =
    'SELECT DISTINCT'+
    '  Site.name AS site_name, Property.Address, Property.lat, Property.lon, Cell.id AS cell_id, Cell.CellID,'+
    '  Cell.name AS cell_name, Cell.color AS cell_color, CalcRegionCells.calc_region_id'+
    ' FROM  CalcRegionCells LEFT OUTER JOIN'+
    '       Site ON CalcRegionCells.site_id = Site.id LEFT OUTER JOIN'+
    '       Property ON Site.property_id = Property.id RIGHT OUTER JOIN'+
    '       Cell ON CalcRegionCells.cell_id = Cell.id'+
    ' WHERE     (CalcRegionCells.calc_region_id = :calc_region_id)';

  SQL_TOOL_EMP_SELECT_PMP_CELL_INFO =
    ' SELECT DISTINCT'                                               +
    '   CalcRegionCells.calc_region_id, Property.lat, Property.lon, Property.Address, '  +
    '   PMP_Sector.id AS cell_id, PMP_Sector.id AS CellID, PMP_Sector.name AS cell_name,'       +
    '   PMP_Sector.color AS cell_color, PMP_site.name AS site_name'     +
    ' FROM  PMP_Sector LEFT OUTER JOIN'                                  +
    '       CalcRegionCells LEFT OUTER JOIN'                              +
    '       Property RIGHT OUTER JOIN'                                     +
    '       PMP_site ON Property.id = PMP_site.property_id ON CalcRegionCells.pmp_site_id = PMP_site.id ON'+
    '       PMP_Sector.id = CalcRegionCells.pmp_sector_id'                 +
    ' WHERE     (CalcRegionCells.calc_region_id = :calc_region_id)';
}

{

  SQL_VIEW_SH_CALCREGION_SITE =
    'SELECT DISTINCT CalcRegionCells.id, CalcRegionCells.guid, Site.name, Site.number, '+
    '  CAST(Site.number AS int) AS number_int, CalcRegionCells.calc_region_id,'+
    '               (SELECT  COUNT(*)  FROM Cell WHERE Cell.site_id = Site.id) AS child_count'+
    ' FROM         Site RIGHT OUTER JOIN'                                                        +
    '                      CalcRegionCells ON Site.id = CalcRegionCells.site_id'                  +
    ' WHERE     (CalcRegionCells.cell_id IS NULL) AND (CalcRegionCells.site_id IS NOT NULL) AND (calc_region_id=%d)' +
    ' ORDER BY number_int, Site.name' ;

  SQL_VIEW_CELL_ANTENNAS =
    'SELECT  AntennaXREF.cell_id, Cell.name AS cell_name, Cell.cell_layer_id,'+
    '        Cell.calc_radius, Site.id AS site_id, Antenna.*,'+
    '        AntennaType.name AS AntennaType_name, Site.property_id AS property_id'+
    ' FROM   Cell LEFT OUTER JOIN Site ON Cell.site_id = Site.id RIGHT OUTER JOIN AntennaXREF'+
    '        ON Cell.id = AntennaXREF.cell_id LEFT OUTER JOIN AntennaType RIGHT OUTER JOIN Antenna'+
    '        ON AntennaType.id = Antenna.antennaType_id ON AntennaXREF.antenna_id = Antenna.id'+
    ' WHERE     (AntennaXREF.cell_id IS NOT NULL)';

}


  SQL_SEL_SITE_INFO =
    'SELECT  TOP 100 PERCENT '+
    '        Site.*, '+
    '        Site.name                  AS site_name ,   '+       //  '''+STR_BS_NAME+''','+
//    '        Site.number               , '+       // AS    '''+STR_BS_NUMBER+''','+
//    '        Site.calc_radius          , '+       // AS    '''+STR_BS_CALC_RADIUS+''','+
//    '        Site.calc_radius_noise    , '+       // AS    '''+STR_BS_CALC_RADIUS_NOISE+''','+
    '        Site.Status                AS site_status    , '+       // AS    '''+STR_BS_STATUS+''','+
//    '        Site.Band                 , '+       // AS    '''+STR_BAND+''','+
//    '        Site.Band_900_Status      , '+       // AS    '''+STR_BAND_900_STATUS+''','+
//    '        Site.Band_1800_Status     , '+       // AS    '''+STR_BAND_1800_STATUS+''','+
//    '        Site.LAC,'+
//    '        Site.Comment              , '+       // AS    '''+STR_COMMENT+''','+
    '        CellLayer.name             AS CellLayer_name    , '+       // AS    '''+STR_TRX_TYPE+''','+
    '        Passive_Componenet.name    AS combiner_name    , '+       // AS    '''+STR_TRX_TYPE+''','+
    '        Passive_Componenet.loss    AS combiner_loss    , '+       // AS    '''+STR_TRX_TYPE+''','+
    '        TrxType.name               AS trx_name    , '+       // AS    '''+STR_TRX_TYPE+''','+
    '        Bsc.name                   AS bsc_name    , '+       // AS    '''+STR_BSC+''','+
    '        Cell.name                  AS cell_name    , '+       // AS    ''��� �������'','+
    '        Cell.*,'+
//    '        Cell.CELLID,'+
//    '        Cell.freq_forbidden       , '+       // AS    '''+STR_FREQ_FORBIDDEN+''','+
//    '        Cell.freq_fixed           , '+       // AS    '''+STR_FREQ_FIXED+''','+
//    '        Cell.freq_allow           , '+       // AS    '''+STR_FREQ_ALLOW+''', '+
//    '        Cell.Hysteresis           , '+       // AS    '''+STR_HYSTERESIS+''','+
//    '        Cell.power                , '+       // AS    '''+STR_POWER+''','+
//    '        Cell.sense                , '+       // AS    '''+STR_SENSE+''','+
//    '        Cell.freq                 , '+       // AS    '''+STR_FREQ+''','+
//    '        Property.Address          , '+       // AS    '''+STR_ADDRESS+''','+
    '        Property.Lat               as property_lat    , '+       // AS    '''+STR_LAT+' (��������)'+''','+
    '        Property.Lon               as property_lon    , '+       // AS    '''+STR_LON+' (��������)'+''','+
    '        Property.Name              as property_name   , '+       // AS    '''+STR_LAT+' (��������)'+''','+
    '        Property.address1          as area            , '+       // AS    '''+STR_LAT+' (��������)'+''','+
    '        Property.address2          as region          , '+       // AS    '''+STR_LAT+' (��������)'+''','+
    '        Property.town                                 , '+       // AS    '''+STR_LAT+' (��������)'+''','+
    '        Property.address                              , '+       // AS    '''+STR_LAT+' (��������)'+''','+
    '        Property.status            as property_status   , '+       // AS    '''+STR_LON+' (��������)'+''','+
    '        Property.Comment           as property_comment   , '+       // AS    '''+STR_LON+' (��������)'+''','+
    '        AntennaType.name           as ant_type        , '+       // AS    '''+'������� - '+STR_ANTENNA_TYPE+''','+
    '        Antenna.* ,'+
//    '        Antenna.height            , '+       // AS    '''+STR_HEIGHT+''','+
    '        Antenna.lat                as antenna_lat   , '+       // AS    '''+STR_LAT+''','+
    '        Antenna.lon                as antenna_lon    '+       // AS    '''+STR_LON+''','+
//    '        Antenna.azimuth           , '+       // AS    '''+STR_AZIMUTH+''','+
//    '        Antenna.tilt              , '+       // AS    '''+STR_TILT+''','+
//    '        Antenna.loss              , '+       // AS    '''+STR_ANTENNA_LOSS_dB+''','+
//    '        Antenna.heel              , '+       // AS    '''+STR_ANTENNA_HEEL+''','+
//    '        Antenna.service_distance  , '+       // AS    '''+STR_ANTENNA_SERVING_DISTANCE+''','+
//    '        Antenna.freq              , '+       // AS    '''+'������� - '+STR_FREQ+''','+
//    '        Antenna.gain              , '+       // AS    '''+STR_GAIN+''','+
//    '        Antenna.diameter          , '+       // AS    '''+STR_DIAMETER+''','+
//    '        Antenna.vert_width        , '+       // AS    '''+STR_VERT_WIDTH+''','+
//    '        Antenna.horz_width        , '+       // AS    '''+STR_HORZ_WIDTH+''','+
  //  '        Antenna.polarization       '+       // AS    '''+STR_POLARIZATION+''''+
    ' FROM   Site LEFT OUTER JOIN                                               '+
    '        BSC ON Site.bsc_id = BSC.id LEFT OUTER JOIN                        '+
    '        Property ON Site.property_id = Property.id LEFT OUTER JOIN         '+
    '        TrxType RIGHT OUTER JOIN                                           '+
    '        Cell ON TrxType.id = Cell.trx_id LEFT OUTER JOIN                   '+
    '        AntennaXREF LEFT OUTER JOIN                                        '+
    '        AntennaType RIGHT OUTER JOIN                                       '+
    '        Antenna ON AntennaType.id = Antenna.antennaType_id ON              '+
    '        AntennaXREF.antenna_id = Antenna.id ON                             '+
    '        Cell.id = AntennaXREF.cell_id ON Site.id = Cell.site_id            '+
    ' WHERE  site.id in (%s)';






  SQL_SELECT_COCHANNEL_FREQS_BY_FREQ_NUMBER =
(*    'SELECT  FreqPlan_Carriers.cell_id, FreqPlan_Carriers.number,                                                                       '+
    '        FreqPlan_Carriers.state, Antenna.id, Antenna.lat,                                                                          '+
    '        Antenna.lon, Antenna.name, Antenna.azimuth, Cell.CELLID, CellLayer.freq_center as freq                                     '+
    ' FROM   CellLayer RIGHT OUTER JOIN                                                                                                 '+
    '        Cell ON CellLayer.id = Cell.cell_layer_id RIGHT OUTER JOIN                                                                 '+
    '        AntennaXREF INNER JOIN                                                                                                     '+
    '        FreqPlan_Carriers ON AntennaXREF.cell_id = FreqPlan_Carriers.cell_id ON Cell.id = AntennaXREF.cell_id RIGHT OUTER JOIN     '+
    '        Antenna ON AntennaXREF.antenna_id = Antenna.id                                                                             '+*)

    ' SELECT  DISTINCT Antenna.id                                                        '+
    ' FROM    AntennaXREF INNER JOIN FreqPlan_Carriers ON                       '+
    '         AntennaXREF.cell_id = FreqPlan_Carriers.cell_id RIGHT OUTER JOIN  '+
    '         Antenna ON AntennaXREF.antenna_id = Antenna.id                    '+
    ' WHERE  (FreqPlan_Carriers.freqplan_id = :freqplan_id) AND                 '+
    '        (FreqPlan_Carriers.number = :number)                               '+
    '         AND (FreqPlan_Carriers.state = :state)                            ';

  SQL_SELECT_NEIGHBOR_FREQS_BY_FREQ_NUMBER =
(*    'SELECT  FreqPlan_Carriers.cell_id, FreqPlan_Carriers.number,                                                 '+
    '        FreqPlan_Carriers.state, Antenna.id, Antenna.lat,                                                    '+
    '        Antenna.lon, Antenna.name, Antenna.azimuth, Cell.CELLID, CellLayer.freq_center as freq               '+
    'FROM    CellLayer RIGHT OUTER JOIN                                                                           '+
    '        Cell ON CellLayer.id = Cell.cell_layer_id RIGHT OUTER JOIN AntennaXREF INNER JOIN                    '+
    '        FreqPlan_Carriers ON AntennaXREF.cell_id = FreqPlan_Carriers.cell_id ON Cell.id = AntennaXREF.cell_id'+
    '        RIGHT OUTER JOIN Antenna ON AntennaXREF.antenna_id = Antenna.id                                      '+*)

    ' SELECT  DISTINCT Antenna.id                                               '+
    ' FROM    AntennaXREF INNER JOIN FreqPlan_Carriers ON                       '+
    '         AntennaXREF.cell_id = FreqPlan_Carriers.cell_id RIGHT OUTER JOIN  '+
    '         Antenna ON AntennaXREF.antenna_id = Antenna.id                    '+
    'WHERE   (FreqPlan_Carriers.freqplan_id = :freqplan_id) AND                 '+
    '        (FreqPlan_Carriers.state = :state) AND                             '+
    '        ( (ABS(FreqPlan_Carriers.number - :number) <= :freq_spacing)       '+
    '           AND (FreqPlan_Carriers.number <> :number))                      ';
























  SQL_SEL_SITE_INFO =
    'SELECT  TOP 100 PERCENT Site.name AS  site_name , Site.number   ,Site.calc_radius          , '+       // AS    '''+STR_BS_CALC_RADIUS+''','+
    '        Site.calc_radius_noise    , '+       // AS    '''+STR_BS_CALC_RADIUS_NOISE+''','+
    '        Site.Status                AS site_status    , '+       // AS    '''+STR_BS_STATUS+''','+
    '        Site.Band                 , '+       // AS    '''+STR_BAND+''','+
    '        Site.Band_900_Status      , '+       // AS    '''+STR_BAND_900_STATUS+''','+
    '        Site.Band_1800_Status     , '+       // AS    '''+STR_BAND_1800_STATUS+''','+
    '        Site.LAC,'+
    '        Site.Comment              , '+       // AS    '''+STR_COMMENT+''','+
    '        TrxType.name               AS trx_name    , '+       // AS    '''+STR_TRX_TYPE+''','+
    '        Bsc.name                   AS bsc_name    , '+       // AS    '''+STR_BSC+''','+
    '        Cell.name                  AS   cell_name    , '+       // AS    ''��� �������'','+
    '        Cell.CELLID,'+
    '        Cell.freq_forbidden       , '+       // AS    '''+STR_FREQ_FORBIDDEN+''','+
    '        Cell.freq_fixed           , '+       // AS    '''+STR_FREQ_FIXED+''','+
    '        Cell.freq_allow           , '+       // AS    '''+STR_FREQ_ALLOW+''', '+
    '        Cell.Hysteresis           , '+       // AS    '''+STR_HYSTERESIS+''','+
    '        Cell.power                , '+       // AS    '''+STR_POWER+''','+
    '        Cell.sense                , '+       // AS    '''+STR_SENSE+''','+
    '        Cell.freq                 , '+       // AS    '''+STR_FREQ+''','+
    '        Property.Address          , '+       // AS    '''+STR_ADDRESS+''','+
    '        Property.Lat               as property_lat   , '+       // AS    '''+STR_LAT+' (��������)'+''','+
    '        Property.Lon               as         property_lon   , '+       // AS    '''+STR_LON+' (��������)'+''','+
    '        Property.status            as property_status   , '+       // AS    '''+STR_LON+' (��������)'+''','+
    '        AntennaType.name           AS ant_type  , '+       // AS    '''+'������� - '+STR_ANTENNA_TYPE+''','+
    '        Antenna.height            , '+       // AS    '''+STR_HEIGHT+''','+
    '        Antenna.lat                as antenna_lat   , '+       // AS    '''+STR_LAT+''','+
    '        Antenna.lon                as antenna_lon   , '+       // AS    '''+STR_LON+''','+
    '        Antenna.azimuth           , '+       // AS    '''+STR_AZIMUTH+''','+
    '        Antenna.tilt              , '+       // AS    '''+STR_TILT+''','+
    '        Antenna.loss              , '+       // AS    '''+STR_ANTENNA_LOSS_dB+''','+
    '        Antenna.heel              , '+       // AS    '''+STR_ANTENNA_HEEL+''','+
    '        Antenna.service_distance  , '+       // AS    '''+STR_ANTENNA_SERVING_DISTANCE+''','+
    '        Antenna.freq              , '+       // AS    '''+'������� - '+STR_FREQ+''','+
    '        Antenna.gain              , '+       // AS    '''+STR_GAIN+''','+
    '        Antenna.diameter          , '+       // AS    '''+STR_DIAMETER+''','+
    '        Antenna.vert_width        , '+       // AS    '''+STR_VERT_WIDTH+''','+
    '        Antenna.horz_width        , '+       // AS    '''+STR_HORZ_WIDTH+''','+
    '        Antenna.polarization       '+       // AS    '''+STR_POLARIZATION+''''+
    ' FROM   Site LEFT OUTER JOIN                                                                         '+
    '        BSC ON Site.bsc_id = BSC.id LEFT OUTER JOIN                                                  '+
    '        Property ON Site.property_id = Property.id LEFT OUTER JOIN                                   '+
    '        TrxType RIGHT OUTER JOIN                                                                     '+
    '        Cell ON TrxType.id = Cell.trx_id LEFT OUTER JOIN                                             '+
    '        AntennaXREF LEFT OUTER JOIN                                                                  '+
    '        AntennaType RIGHT OUTER JOIN                                                                 '+
    '        Antenna ON AntennaType.id = Antenna.antennaType_id ON AntennaXREF.antenna_id = Antenna.id ON '+
    '        Cell.id = AntennaXREF.cell_id ON Site.id = Cell.site_id                                      '+
    ' WHERE  site.id in (%s)';