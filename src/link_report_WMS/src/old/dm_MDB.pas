unit dm_MDB;

interface

uses
  ADODB, Db,  Forms, Classes, SysUtils, Dialogs,

  u_const_db,

  u_db,
  

  u_db_mdb, DBClient, dxmdaset

  ;

type
  TdmMDB = class(TDataModule)
    ADOConnection_MDB: TADOConnection;
    t_Link: TADOTable;
    t_Property1: TADOTable;
    t_LinkEnd1: TADOTable;
    t_Property2: TADOTable;
    t_LinkEnd2: TADOTable;
    ds_Property2: TDataSource;
    ds_Property1: TDataSource;
    ds_LinkEnd1: TDataSource;
    ds_LinkEnd2: TDataSource;
    ds_Link: TDataSource;
    t_Antennas1: TADOTable;
    t_Antennas2: TADOTable;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    t_LInk_repeater: TADOTable;
    t_LInk_repeater_ant: TADOTable;
    ds_LInk_repeater: TDataSource;
    t_LInk_repeater_property: TADOTable;
    t_Profile_items: TADOTable;
    t_Items: TADOTable;
    t_link_img11111: TADOTable;
    t_groups: TADOTable;
    ds_groups: TDataSource;
    ClientDataSet_Link_img: TClientDataSet;
    ClientDataSet_Link_imglink_id: TIntegerField;
    ClientDataSet_Link_imgimg1: TGraphicField;
    ClientDataSet_Link_imgimg2: TGraphicField;
    ClientDataSet_Link_imgimg3: TGraphicField;
    ClientDataSet_Link_imgprofile_img: TGraphicField;
    ClientDataSet_Link_imgmap_img: TGraphicField;
    ClientDataSet_Link_imgimg1_filename: TStringField;
    ClientDataSet_Link_imgimg2_filename: TStringField;
    ClientDataSet_Link_imgimg3_filename: TStringField;
    ClientDataSet_Link_imgprofile_img_filename: TStringField;
    ClientDataSet_Link_imgmap_filename: TStringField;
    qry_LinkLine: TADOQuery;
    qry_LinkLine_links: TADOQuery;
    t_LinkLine: TADOTable;
    t_LinkLine_links: TADOTable;
    ClientDataSet_linkline_img: TClientDataSet;
    ClientDataSet_linkline_imgmap_img: TGraphicField;
    dxMemData_Link: TdxMemData;
    dxMemData_Property1: TdxMemData;
    dxMemData_Property2: TdxMemData;
    dxMemData_LinkEnd1: TdxMemData;
    dxMemData_LinkEnd2: TdxMemData;
    dxMemData_Antennas1: TdxMemData;
    dxMemData_Antennas2: TdxMemData;
    dxMemData_LInk_repeater: TdxMemData;
    dxMemDataLInk_repeater_ant: TdxMemData;
    dxMemDatat_groups: TdxMemData;
    dxMemData2: TdxMemData;
//    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FtempFileName : string;




//    function OpenMDB(aMDBFileName: string): Boolean;

    function CloseMDB: Integer;

    procedure Open_data;

  public


    procedure Clear;
// TODO: BeginTransaction
//  procedure BeginTransaction;

    function CreateMDB1(aFileName: string): Boolean;

    procedure DeleteMDB;

    procedure CreateTableFromDataset(aTblName: string; aDataSet_src: TDataSet;
        aTable: TADOTable);
//    TableNameList: TStringList;

    class procedure Init;



  end;

var
  dmMDB: TdmMDB;


const
  FLD_ground_h      = 'ground_h';
  FLD_clutter_h     = 'clutter_h';
  FLD_clutter_code  = 'clutter_code';
  FLD_clutter_color = 'clutter_color';

  FLD_earth_h       = 'earth_h';

  FLD_img1_filename ='img1_filename';
  FLD_img2_filename ='img2_filename';
  FLD_img3_filename ='img3_filename';
  FLD_map_filename  ='map_filename';
  FLD_profile_img_filename  ='profile_img_filename';




implementation
  {$R *.DFM}


function TdmMDB.CloseMDB: Integer;
begin
  ADOConnection_MDB.Close;

end;




//------------------------------------------------------------------------------
class procedure TdmMDB.Init;
//------------------------------------------------------------------------------
begin
  if not Assigned(dmMDB) then
    Application.CreateForm(TdmMDB, dmMDB);
end;


procedure TdmMDB.DataModuleCreate(Sender: TObject);
begin
  if ADOConnection_MDB.Connected then
    ShowMessage('ADOConnection_MDB.Connected');


//  Assert(not ADOConnection_MDB.Connected);

end;



//------------------------------------------------------------------------------
function TdmMDB.CreateMDB1(aFileName: string): Boolean;
//------------------------------------------------------------------------------

var
  s: string;
begin
  Assert(aFileName<>'', 'Value <=0');

  aFileName:=ChangeFileExt(aFileName, '.mdb');

  FtempFileName := aFileName;


  DeleteFile (aFileName);

  Assert(not FileExists(aFileName), 'FileExists -' + aFileName);



  Result := mdb_CreateFileAndOpen(aFileName, ADOConnection_MDB);

{

  "CREATE UNIQUE INDEX CustID " _
        & "ON Customers (CustomerID) " _
        & "WITH DISALLOW NULL;"
}


  mdb_CreateTableFromArr(ADOConnection_MDB, 'Profile_items',
    [
      db_Field(FLD_link_id, ftInteger),

      db_Field(FLD_INDEX, ftInteger),

      db_Field(FLD_distance_km, ftFloat),

      db_Field(FLD_ground_h, ftFloat),
      db_Field(FLD_clutter_h, ftInteger),
      db_Field(FLD_clutter_code, ftInteger),

      db_Field(FLD_clutter_name, ftString, 200),
      db_Field(FLD_clutter_color, ftInteger),

      db_Field(FLD_earth_h, ftFloat)

    ]);

//   s:= 'CREATE UNIQUE INDEX Profile_items_index ON Profile_items(link_id,distance_km) WITH DISALLOW NULL';
//   ADOConnection_MDB.Execute(s);



 // s:= Format('"CREATE UNIQUE INDEX CustID ON Profile_items(FLD_link_id, ) ', []);



 mdb_CreateTableFromArr(ADOConnection_MDB, 'link_img',
    [
      db_Field(FLD_link_id, ftInteger),


      db_Field(FLD_distance_km, ftFloat),

      db_Field(FLD_img1_filename, ftString, 200),
      db_Field(FLD_img2_filename, ftString, 200),
      db_Field(FLD_img3_filename, ftString, 200),
      db_Field(FLD_map_filename, ftString, 200),
      db_Field(FLD_profile_img_filename, ftString, 200)

    ]);

   s:= 'CREATE UNIQUE INDEX link_img_index ON link_img(link_id) WITH DISALLOW NULL';
   ADOConnection_MDB.Execute(s);

{
      "CREATE UNIQUE INDEX CustID " _
        & "ON Customers (CustomerID) " _
        & "WITH DISALLOW NULL;"

}



  mdb_CreateTableFromArr(ADOConnection_MDB, 'groups',
    [
      db_Field(FLD_id, ftAutoInc),

      db_Field(FLD_caption, ftString, 200),
      db_Field(FLD_link_id, ftInteger)

   //   db_Field(FLD_name, ftString)

    ]);

//   s:= 'CREATE UNIQUE INDEX groups_index ON groups(link_id) WITH DISALLOW NULL';
//   ADOConnection_MDB.Execute(s);



  mdb_CreateTableFromArr(ADOConnection_MDB, 'Items',
    [
      db_Field(FLD_id, ftAutoInc),

      db_Field(FLD_caption,   ftString, 200),
      db_Field(FLD_parent_id, ftInteger),
    //  FLD_link_id,  ftInteger,

      db_Field(FLD_CODE,  ftInteger),

//      db_Field(FLD_id,     ftInteger),
      db_Field(FLD_name,   ftString, 200),

      db_Field(FLD_value,  ftFloat),
      db_Field(FLD_value2, ftFloat)

    ]);

//   s:= 'CREATE UNIQUE INDEX Items_index ON Items(parent_id) WITH DISALLOW NULL';
//   ADOConnection_MDB.Execute(s);




  mdb_CreateTableFromArr(ADOConnection_MDB, 'Antennas1',
    [
      db_Field(FLD_link_id,  ftInteger),

      db_Field(FLD_polarization, ftString, 4),
      db_Field(FLD_height,   ftString),
      db_Field(FLD_diameter, ftString),
      db_Field(FLD_tilt    , ftString),
      db_Field(FLD_gain    , ftString)

    ]);

//   s:= 'CREATE UNIQUE INDEX Antennas1_index ON Antennas1(link_id) WITH DISALLOW NULL';
//   ADOConnection_MDB.Execute(s);



  mdb_CreateTableFromArr(ADOConnection_MDB, 'Antennas2',
    [
      db_Field(FLD_link_id,  ftInteger),

      db_Field(FLD_polarization,  ftString, 4),
      db_Field(FLD_height,   ftString),
      db_Field(FLD_diameter, ftString),
      db_Field(FLD_tilt    , ftString),
      db_Field(FLD_gain    , ftString)

    ]);

//   s:= 'CREATE UNIQUE INDEX Antennas1_index ON Antennas2(link_id) WITH DISALLOW NULL';
//   ADOConnection_MDB.Execute(s);



 // FtempFileName := ChangeFileExt(aMDBFileName, '.mdb');

  //Result := mdb_OpenConnection(ADOConnection_MDB, FtempFileName);


  Open_data;  

end;



// ---------------------------------------------------------------
procedure TdmMDB.Clear;
// ---------------------------------------------------------------
begin
  ADOConnection_MDB.Execute('delete from link');
  ADOConnection_MDB.Execute('delete from Profile_items');
  ADOConnection_MDB.Execute('delete from Antennas1');
  ADOConnection_MDB.Execute('delete from Antennas2');

  ADOConnection_MDB.Execute('delete from groups');
  ADOConnection_MDB.Execute('delete from items');


end;



// ---------------------------------------------------------------
procedure TdmMDB.CreateTableFromDataset(aTblName: string; aDataSet_src:
    TDataSet; aTable: TADOTable);
// ---------------------------------------------------------------
var
  s: string;
begin

  s:=mdb_MakeCreateSql_From_Dataset(aDataSet_src, aTblName);

  ADOConnection_MDB.Execute(s);

  aTable.TableName:=aTblName;
  aTable.Open;

//  Result := ;
end;

// ---------------------------------------------------------------
procedure TdmMDB.DeleteMDB;
// ---------------------------------------------------------------
begin
  ADOConnection_MDB.Close;

  DeleteFile(FtempFileName);

  Assert(not FileExists(FtempFileName), 'FileExists -' + FtempFileName);

end;


// ---------------------------------------------------------------
procedure TdmMDB.Open_data;
// ---------------------------------------------------------------
begin

{  t_Link.Open;


  t_Property1.Open;
  t_Property2.Open;

  t_LinkEnd1.Open;
  t_LinkEnd2.Open;
}
//  t_link_img.Open;


  t_groups.Open;
  t_Items.Open;

  t_Antennas1.Open;
  t_Antennas2.Open;

  t_Profile_items.Open;

//  t_link_img.Open;



{  db_CopyRecords (qry_Property1, dmMDB.t_Property1 );
  db_CopyRecords (qry_Property2, dmMDB.t_Property2 );

  db_CopyRecords (qry_LinkEnd1, dmMDB.t_LinkEnd1 );
  db_CopyRecords (qry_LinkEnd2, dmMDB.t_LinkEnd2 );

  db_CopyRecords (qry_Antennas1, dmMDB.t_Antennas1 );
  db_CopyRecords (qry_Antennas2, dmMDB.t_Antennas2 );
}

  // TODO -cMM: TdmMDB.Open_data default body inserted
end;



procedure test;
begin
 // TdmMDB.Init;
//  dmMDB.CreateMDB('g:\d.mdb');

end;    //



begin
//  test;


end.




{




//------------------------------------------------------------------------------
function TdmMDB.OpenMDB(aMDBFileName: string): Boolean;
//------------------------------------------------------------------------------
var
  I: Integer;

  s: string;
begin
//  TableNameList.Clear;

  FtempFileName := ChangeFileExt(aMDBFileName, '.mdb');

  Result := mdb_OpenConnection(ADOConnection_MDB, FtempFileName);

{
  if not Result then
    Exit;

    ADOTable1.Close;
  end;
}

//  FreeAndNil(oTableFieldNameList);


end;

