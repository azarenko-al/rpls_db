unit dm_App_link_report_WMS;

interface

uses
  Classes, Dialogs,  SysUtils,

//  x_Options,

  u_ini_Link_report_params_WMS,
  d_MIF_export,

//  dm_Link_SDB_report,
//  dm_Link_report_xml,
//  dm_Link_report_data,

 // dm_Link_report,
//  dm_LinkLine_report,
//  dm_Link_map,

  u_vars
  
//  dm_Main

  ;
  //dm_LinkLine_Complex_Report11,

type
  TdmMain_app__report_WMS = class(TDataModule)
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FLink_Report_Param_WMS: TIni_Link_Report_Param_WMS;

   // FfrxReport: TfrxReport;
    
//    FReportSelect: TIni_Link_Report_Param;
  public

  //  procedure UpdateReports;
  end;

var
  dmMain_app__report_WMS: TdmMain_app__report_WMS;


implementation

//uses dm_Test1;

 {$R *.DFM}

//--------------------------------------------------------------
procedure TdmMain_app__report_WMS.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const
//C:\Users\Alexey\AppData\Local\RPLS_DB_LINK\
  DEF_TEMP_FILENAME = 'link_report_wms.ini';

  REGISTRY_ROOT = 'Software\Onega\RPLS_DB\';

var
  k: Integer;
  sIniFileName: string;
//  sMDB_FileName: string;
//  sReportFileName: string;
//  sTemplateFileName: string;


begin

(*
  TdmTest1.Init;
  dmTest1.frxReport.DesignReport(true);

  Exit;
*)
 // ---------------------------------------------------------------




  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
    sIniFileName:=g_ApplicationDataDir+ DEF_TEMP_FILENAME;



  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;

  // -------------------------------------------------------------------

//    dmMain.Open_ADOConnection (oIni.ConnectionString);
  

//  TdmMain.Init;

//  if not dmMain.OpenDB_reg then    // True
//    Exit;




  {$DEFINE use_dll111}

  {$IFDEF use_dll}

{
  if Load_ILink_report() then
  begin
    ILink_report.InitADO(dmMain.ADOConnection1.ConnectionObject);
 //   ILink_graph.InitAppHandle(Application.Handle);
    ILink_report.Execute(sFile);

    UnLoad_ILink_report();
  end;

  Exit;
 }
  {$ENDIF}



  FLink_Report_Param_WMS:=TIni_Link_Report_Param_WMS.Create;
  FLink_Report_Param_WMS.LoadFromINI(sIniFileName);



//  WMS_Init;


//  dmLink_report.frxReport.DesignReport();

//  TdmTest1.Init;
 // dmTest1.frxReport.DesignReport(true);

 // Exit;



//  if iScale>0 then
 //   dmLink_map.Scale:=iScale;

  Tdlg_Report_Select_new.ExecDlg();

//  if not Tdlg_Report_Select_new.ExecDlg() then
//    Exit;



//   dmMDB.DeleteMDB;


end;



// ---------------------------------------------------------------
procedure TdmMain_app__report_WMS.DataModuleDestroy(Sender: TObject);
// ---------------------------------------------------------------
var
  I: integer;
  s: string;
  s4: string;
  s3: string;
  s2: string;
  s1: string;

begin
//  IOptionsX_UnLoad();

  FreeAndNil(g_Link_Report_Param);

 // FreeAndNil(FReportSelect);
        

(*
  FreeAndNil(dmLink_report_data);
  FreeAndNil(dmLink_report);
  FreeAndNil(dmLinkLine_report);
  FreeAndNil(dmLink_map);
  FreeAndNil(dmLink_report_xml);
  FreeAndNil(dmRel_Engine);

  FreeAndNil(dmMain);

  inherited;*)

end;


end.

