unit dm_MDB_v1;

interface

uses
  ADODB, Db,  Forms, Classes, SysUtils, Dialogs,

  u_const_db,

  u_db,
  

  u_db_mdb, DBClient, dxmdaset

  ;

type
  TdmMDB_v1 = class(TDataModule)
    ds_Property2: TDataSource;
    ds_Property1: TDataSource;
    ds_LinkEnd1: TDataSource;
    ds_LinkEnd2: TDataSource;
    ds_Link: TDataSource;
    t_LInk_repeater: TADOTable;
    t_LInk_repeater_ant: TADOTable;
    ds_LInk_repeater: TDataSource;
    t_LInk_repeater_property: TADOTable;
    t_Profile_items: TADOTable;
    t_Items: TADOTable;
    t_link_img11111: TADOTable;
    t_groups: TADOTable;
    ds_groups: TDataSource;
    ClientDataSet_Link_img: TClientDataSet;
    ClientDataSet_Link_imglink_id: TIntegerField;
    ClientDataSet_Link_imgimg1: TGraphicField;
    ClientDataSet_Link_imgimg2: TGraphicField;
    ClientDataSet_Link_imgimg3: TGraphicField;
    ClientDataSet_Link_imgprofile_img: TGraphicField;
    ClientDataSet_Link_imgmap_img: TGraphicField;
    ClientDataSet_Link_imgimg1_filename: TStringField;
    ClientDataSet_Link_imgimg2_filename: TStringField;
    ClientDataSet_Link_imgimg3_filename: TStringField;
    ClientDataSet_Link_imgprofile_img_filename: TStringField;
    ClientDataSet_Link_imgmap_filename: TStringField;
    qry_LinkLine: TADOQuery;
    qry_LinkLine_links: TADOQuery;
    t_LinkLine: TADOTable;
    t_LinkLine_links: TADOTable;
    ClientDataSet_linkline_img: TClientDataSet;
    ClientDataSet_linkline_imgmap_img: TGraphicField;
    t_link: TClientDataSet;
    t_Property2: TClientDataSet;
    t_Property1: TClientDataSet;
    t_LinkEnd1: TClientDataSet;
    t_LinkEnd2: TClientDataSet;
    t_Antennas1: TClientDataSet;
    t_Antennas2: TClientDataSet;
    t_LinkLine_: TClientDataSet;
    ClientDataSet2: TClientDataSet;
    ClientDataSet1: TClientDataSet;
    t_groups_: TClientDataSet;
    t_Items_: TClientDataSet;
    t_Profile_items_: TClientDataSet;
//    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FtempFileName : string;




//    function OpenMDB(aMDBFileName: string): Boolean;

    function CloseMDB: Integer;

    procedure Open_data;

  public


    procedure Clear;
// TODO: BeginTransaction
//  procedure BeginTransaction;

    function CreateMDB1(aFileName: string): Boolean;

    procedure DeleteMDB;

    procedure CreateTableFromDataset(aTblName: string; aDataSet_src: TDataSet;
        aTable: TADOTable);
//    TableNameList: TStringList;

    class procedure Init;



  end;

var
  dmMDB_v1: TdmMDB_v1;


const
  FLD_ground_h      = 'ground_h';
  FLD_clutter_h     = 'clutter_h';
  FLD_clutter_code  = 'clutter_code';
  FLD_clutter_color = 'clutter_color';

  FLD_earth_h       = 'earth_h';

  FLD_img1_filename ='img1_filename';
  FLD_img2_filename ='img2_filename';
  FLD_img3_filename ='img3_filename';
  FLD_map_filename  ='map_filename';
  FLD_profile_img_filename  ='profile_img_filename';




implementation
  {$R *.DFM}


function TdmMDB_v1.CloseMDB: Integer;
begin
  ADOConnection_MDB.Close;

end;




//------------------------------------------------------------------------------
class procedure TdmMDB_v1.Init;
//------------------------------------------------------------------------------
begin
  if not Assigned(dmMDB) then
    Application.CreateForm(TdmMDB, dmMDB);
end;


procedure TdmMDB_v1.DataModuleCreate(Sender: TObject);
begin
//  if ADOConnection_MDB.Connected then
//    ShowMessage('ADOConnection_MDB.Connected');


//  Assert(not ADOConnection_MDB.Connected);

end;



//------------------------------------------------------------------------------
function TdmMDB_v1.CreateMDB1(aFileName: string): Boolean;
//------------------------------------------------------------------------------

var
  s: string;
begin
{
  Assert(aFileName<>'', 'Value <=0');

  aFileName:=ChangeFileExt(aFileName, '.mdb');

  FtempFileName := aFileName;


  DeleteFile (aFileName);

  Assert(not FileExists(aFileName), 'FileExists -' + aFileName);



  Result := mdb_CreateFileAndOpen(aFileName, ADOConnection_MDB);
}
{

  "CREATE UNIQUE INDEX CustID " _
        & "ON Customers (CustomerID) " _
        & "WITH DISALLOW NULL;"
}

  db_CreateField (dxMemData_Profile_items,

//  mdb_CreateTableFromArr(ADOConnection_MDB, 'Profile_items',
    [
      db_Field(FLD_link_id, ftInteger),

      db_Field(FLD_INDEX, ftInteger),

      db_Field(FLD_distance_km, ftFloat),

      db_Field(FLD_ground_h, ftFloat),
      db_Field(FLD_clutter_h, ftInteger),
      db_Field(FLD_clutter_code, ftInteger),

      db_Field(FLD_clutter_name, ftString, 200),
      db_Field(FLD_clutter_color, ftInteger),

      db_Field(FLD_earth_h, ftFloat)

    ]);

//   s:= 'CREATE UNIQUE INDEX Profile_items_index ON Profile_items(link_id,distance_km) WITH DISALLOW NULL';
//   ADOConnection_MDB.Execute(s);



 // s:= Format('"CREATE UNIQUE INDEX CustID ON Profile_items(FLD_link_id, ) ', []);


  db_CreateField (dxMemData_link_img,

// mdb_CreateTableFromArr(ADOConnection_MDB, 'link_img',
    [
      db_Field(FLD_link_id, ftInteger),


      db_Field(FLD_distance_km, ftFloat),

      db_Field(FLD_img1_filename, ftString, 200),
      db_Field(FLD_img2_filename, ftString, 200),
      db_Field(FLD_img3_filename, ftString, 200),
      db_Field(FLD_map_filename, ftString, 200),
      db_Field(FLD_profile_img_filename, ftString, 200)

    ]);

//   s:= 'CREATE UNIQUE INDEX link_img_index ON link_img(link_id) WITH DISALLOW NULL';
//   ADOConnection_MDB.Execute(s);

{
      "CREATE UNIQUE INDEX CustID " _
        & "ON Customers (CustomerID) " _
        & "WITH DISALLOW NULL;"

}


  db_CreateField (dxMemData_groups,

//  mdb_CreateTableFromArr(ADOConnection_MDB, 'groups',
    [
      db_Field(FLD_id, ftAutoInc),

      db_Field(FLD_caption, ftString, 200),
      db_Field(FLD_link_id, ftInteger)

   //   db_Field(FLD_name, ftString)

    ]);

//   s:= 'CREATE UNIQUE INDEX groups_index ON groups(link_id) WITH DISALLOW NULL';
//   ADOConnection_MDB.Execute(s);

   db_CreateField (dxMemData_Items,


//  mdb_CreateTableFromArr(ADOConnection_MDB, 'Items',
    [
      db_Field(FLD_id, ftAutoInc),

      db_Field(FLD_caption,   ftString, 200),
      db_Field(FLD_parent_id, ftInteger),
    //  FLD_link_id,  ftInteger,

      db_Field(FLD_CODE,  ftInteger),

//      db_Field(FLD_id,     ftInteger),
      db_Field(FLD_name,   ftString, 200),

      db_Field(FLD_value,  ftFloat),
      db_Field(FLD_value2, ftFloat)

    ]);

//   s:= 'CREATE UNIQUE INDEX Items_index ON Items(parent_id) WITH DISALLOW NULL';
//   ADOConnection_MDB.Execute(s);


   db_CreateField (dxMemData_Antennas1,


//  mdb_CreateTableFromArr(ADOConnection_MDB, 'Antennas1',
    [
      db_Field(FLD_link_id,  ftInteger),

      db_Field(FLD_polarization, ftString, 4),
      db_Field(FLD_height,   ftString),
      db_Field(FLD_diameter, ftString),
      db_Field(FLD_tilt    , ftString),
      db_Field(FLD_gain    , ftString)

    ]);

//   s:= 'CREATE UNIQUE INDEX Antennas1_index ON Antennas1(link_id) WITH DISALLOW NULL';
//   ADOConnection_MDB.Execute(s);

   db_CreateField (dxMemData_Antennas2,


//  mdb_CreateTableFromArr(ADOConnection_MDB, 'Antennas2',
    [
      db_Field(FLD_link_id,  ftInteger),

      db_Field(FLD_polarization,  ftString, 4),
      db_Field(FLD_height,   ftString),
      db_Field(FLD_diameter, ftString),
      db_Field(FLD_tilt    , ftString),
      db_Field(FLD_gain    , ftString)

    ]);

//   s:= 'CREATE UNIQUE INDEX Antennas1_index ON Antennas2(link_id) WITH DISALLOW NULL';
//   ADOConnection_MDB.Execute(s);



 // FtempFileName := ChangeFileExt(aMDBFileName, '.mdb');

  //Result := mdb_OpenConnection(ADOConnection_MDB, FtempFileName);


  Open_data;  

end;



// ---------------------------------------------------------------
procedure TdmMDB_v1.Clear;
// ---------------------------------------------------------------
begin
  ADOConnection_MDB.Execute('delete from link');
  ADOConnection_MDB.Execute('delete from Profile_items');
  ADOConnection_MDB.Execute('delete from Antennas1');
  ADOConnection_MDB.Execute('delete from Antennas2');

  ADOConnection_MDB.Execute('delete from groups');
  ADOConnection_MDB.Execute('delete from items');


end;



// ---------------------------------------------------------------
procedure TdmMDB_v1.CreateTableFromDataset(aTblName: string; aDataSet_src:
    TDataSet; aTable: TADOTable);
// ---------------------------------------------------------------
var
  s: string;
begin

  s:=mdb_MakeCreateSql_From_Dataset(aDataSet_src, aTblName);

//  ADOConnection_MDB.Execute(s);

  aTable.TableName:=aTblName;
  aTable.Open;

//  Result := ;
end;




// ---------------------------------------------------------------
procedure TdmMDB_v1.Open_data;
// ---------------------------------------------------------------
begin

  t_groups.Open;
  t_Items.Open;

  t_Antennas1.Open;
  t_Antennas2.Open;

  t_Profile_items.Open;

//  t_link_img.Open;



  // TODO -cMM: TdmMDB.Open_data default body inserted
end;



begin
//  test;


end.




{




//------------------------------------------------------------------------------
function TdmMDB.OpenMDB(aMDBFileName: string): Boolean;
//------------------------------------------------------------------------------
var
  I: Integer;

  s: string;
begin
//  TableNameList.Clear;

  FtempFileName := ChangeFileExt(aMDBFileName, '.mdb');

  Result := mdb_OpenConnection(ADOConnection_MDB, FtempFileName);

{
  if not Result then
    Exit;

    ADOTable1.Close;
  end;
}

//  FreeAndNil(oTableFieldNameList);


end;

