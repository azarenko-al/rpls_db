unit u_ini_Link_report_params_WMS;

interface
uses
  IniFiles, SysUtils,

  u_geo,
  
  u_files
  ;

type
  // ---------------------------------------------------------------
  TIni_Link_Report_Param_WMS = class
  // ---------------------------------------------------------------

  public

//  TExportMapRec = record
    GeosetFileName: string;

    ImgFileName: string;
    Scale: integer;

//    Mode: (mtNone,mtVEctor);//,mtPoint); //,mtRect);

//    BLPoint1: TblPoint;

    BLVector: TblVector;
 //   BLRect: TBLRect;

    Width_cm: double;
    Height_cm: double;

 //   IsDebug : Boolean;

//    MapFileList : TStringList;
//  end;



//    constructor Create;
//    destructor Destroy; override;

//    function GetTempReportFile: string;

    procedure LoadFromINI(aFileName: string);
    procedure SaveToINI(aFileName: string);

  //  function Validate: boolean;

  end;


//var
//  g_Link_Report_Param_WMS: TIni_Link_Report_Param_WMS;


implementation


//--------------------------------------------------------------
procedure TIni_Link_Report_Param_WMS.LoadFromINI(aFileName: string);
//--------------------------------------------------------------
var
  oIniFile: TIniFile;
 // sCategory: string;
  sScale: string;

begin
  if not FileExists(aFileName) then
    Exit;

  oIniFile := TIniFile.Create(aFileName);

 // LinkLineIDList.LoadIDsFromIniSection (oIniFile, 'LinkLines');

  with oIniFile do
  begin
    GeosetFileName:=ReadString('main', 'GeosetFileName', GeosetFileName);

    ImgFileName:=ReadString('main', 'ImgFileName', ImgFileName);
    Scale:=ReadInteger('main', 'Scale', Scale);

//    Mode: (mtNone,mtVEctor);//,mtPoint); //,mtRect);

//    BLPoint1: TblPoint;
    BLVector.Point1.B:= ReadFloat('main', 'BLVector_Point1_B', 0);
    BLVector.Point1.L:= ReadFloat('main', 'BLVector_Point1_L', 0);

    BLVector.Point2.B:= ReadFloat('main', 'BLVector_Point2_B', 0);
    BLVector.Point2.L:= ReadFloat('main', 'BLVector_Point2_L', 0);

 //   BLRect: TBLRect;
    WriteFloat('main', 'Width_cm', Width_cm);
    WriteFloat('main', 'Height_cm', Height_cm);


  end;

  FreeAndNil(oIniFile);
//  oIniFile.Free;

(*
  if Scale=0 then
  begin
    sScale:='1 000 000';
    if InputQuery('����� ��������', '�������:', sScale) then
      Scale:= AsInteger( ReplaceStr (sScale, ' ','') )
  end;
*)

end;

// ---------------------------------------------------------------
procedure TIni_Link_Report_Param_WMS.SaveToINI(aFileName: string);
// ---------------------------------------------------------------
var
  oInifile: TIniFile;
  sCategory: string;
begin
  //Assert(MapDesktopID>0, 'Value <=0');

  DeleteFile(aFileName);


  ForceDirByFileName(aFileName);

//  ForceDirectories (ExtractFileDir (aFileName));

  oInifile:=TIniFile.Create (aFileName);

  with oInifile do
  begin

    WriteString('main', 'GeosetFileName', GeosetFileName);

    WriteString('main', 'ImgFileName', ImgFileName);
    WriteInteger('main', 'Scale', Scale) ;

//    Mode: (mtNone,mtVEctor);//,mtPoint); //,mtRect);

//    BLPoint1: TblPoint;


    WriteFloat('main', 'BLVector_Point1_B', BLVector.Point1.B);
    WriteFloat('main', 'BLVector_Point1_L', BLVector.Point1.L);

    WriteFloat('main', 'BLVector_Point2_B', BLVector.Point2.B);
    WriteFloat('main', 'BLVector_Point2_L', BLVector.Point2.L);





 //   BLRect: TBLRect;
    WriteFloat('main', 'Width_cm', Width_cm);
    WriteFloat('main', 'Height_cm', Height_cm);

//    Width_cm: double;
//    Height_cm: double;


  end;

  oInifile.Free;


end;



end.


