program dll_Link_report_WMS;



uses
  d_MIF_export in '..\..\..\..\common7\Package_Common\Map\d_MIF_export.pas' {dlg_MIF_export1},
  dm_App_link_report_WMS in '..\src\dm_App_link_report_WMS.pas' {dmMain_app__report_WMS: TDataModule},
  Forms,
  i_WMS in '..\..\ActiveX\i_WMS.pas',
  MidasLib,
  u_debug in '..\..\..\..\common7\Package_Common\u_debug.pas',
  u_ini_Link_report_params_WMS in '..\src_shared\u_ini_Link_report_params_WMS.pas',
  WMS_TLB in '..\..\ActiveX\WMS_TLB.pas';

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(TdmMain_app__report_WMS, dmMain_app__report_WMS);
  Application.Run;
end.
