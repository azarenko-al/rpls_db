object dmLibrary: TdmLibrary
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  Left = 803
  Top = 440
  Height = 321
  Width = 536
  object tab_Bands: TADOTable
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    TableName = 'lib_Bands'
    Left = 40
    Top = 36
  end
  object Lib_Modulations: TADOTable
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    TableName = 'Lib_Modulations'
    Left = 132
    Top = 36
  end
  object ADOTable111111: TADOTable
    CursorType = ctStatic
    LockType = ltReadOnly
    TableName = 'lib_Radiation_class'
    Left = 228
    Top = 36
    object StringField1: TStringField
      FieldName = 'NAME'
      Size = 50
    end
  end
  object tab_Lib_Modulations: TADOTable
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    TableName = 'Lib_Modulations'
    Left = 56
    Top = 124
    object Lib_ModulationsNAME: TStringField
      FieldName = 'NAME'
      Size = 50
    end
  end
  object ds_tab_Lib_Modulations: TDataSource
    DataSet = tab_Lib_Modulations
    Left = 58
    Top = 172
  end
  object ds_tab_lib_Radiation_class: TDataSource
    DataSet = tab_lib_Radiation_class
    Left = 202
    Top = 172
  end
  object tab_lib_Radiation_class: TADOTable
    LockType = ltBatchOptimistic
    TableName = 'lib_Radiation_class'
    Left = 204
    Top = 124
    object tab_lib_Radiation_classname2: TStringField
      FieldName = 'name'
      Size = 10
    end
  end
end
