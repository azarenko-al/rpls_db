unit dm_Library;

interface

uses
  Windows, Messages, Db, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ADODB, Variants,

  dm_Main,

  u_db,
  u_const_db,

  dm_Custom
  ;

type

  TdmLibrary = class(TDataModule)
    tab_Bands: TADOTable;
    Lib_Modulations: TADOTable;
    ADOTable111111: TADOTable;
    StringField1: TStringField;
    tab_Lib_Modulations: TADOTable;
    Lib_ModulationsNAME: TStringField;
    ds_tab_Lib_Modulations: TDataSource;
    ds_tab_lib_Radiation_class: TDataSource;
    tab_lib_Radiation_class: TADOTable;
    tab_lib_Radiation_classname2: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FFreq_MHZ: Double;
    FBandStr : string;

    procedure ForEach_AddBand(aDataset: TDataset; var aTerminated: boolean);
    procedure ForEach_AddModulation(aDataset: TDataSet; var aTerminated: Boolean);

//    function GetBandByFreq_MHZ___________(aFreq_MHZ: Double): String;
//    procedure ForEach_GetFreq(aDataset: TDataset; var aTerminated: Boolean);
  public
    Modulations: TStringList;
    Bands: TStringList;

    function GetBandAveFreq_MHZ(aBand: string): integer;
  end;


function dmLibrary: TdmLibrary;


//================================================================
//implementation
//================================================================
implementation  {$R *.dfm}

var
  FdmLibrary: TdmLibrary;



// ---------------------------------------------------------------
function dmLibrary: TdmLibrary;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLibrary) then
    Application.CreateForm(TdmLibrary, FdmLibrary);

  Result := FdmLibrary;
end;

//-------------------------------------------------------------------
procedure TdmLibrary.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  Bands := TStringList.Create();
  Modulations := tstringList.Create();

  db_SetComponentADOConnection(Self, dmMain.ADOConnection1);

  db_TableOpen1(tab_Bands, TBL_LIB_BANDS);
  db_TableOpen1(Lib_Modulations, TBL_LIB_MODULATIONS);

  db_TableOpen1(tab_Lib_Modulations, TBL_Lib_Modulations);
  db_TableOpen1(tab_lib_Radiation_class, TBL_lib_Radiation_class);


 // tab_Bands.Open;
 // Lib_Modulations.Open;

  db_ForEach(tab_Bands, ForEach_AddBand);
  db_ForEach(Lib_Modulations, ForEach_AddModulation);
end;


procedure TdmLibrary.ForEach_AddBand(aDataset: TDataset; var aTerminated:   boolean);
begin
  Bands.Add(aDataset.FieldByName(FLD_NAME).AsString);
end;

procedure TdmLibrary.ForEach_AddModulation(aDataset: TDataSet; var aTerminated: Boolean);
begin
  Modulations.Add(aDataset.FieldByName(FLD_NAME).AsString);
end;


procedure TdmLibrary.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(Modulations);
  FreeAndNil(Bands);
end;

// ---------------------------------------------------------------
function TdmLibrary.GetBandAveFreq_MHZ(aBand: string): integer;
// ---------------------------------------------------------------
begin
  tab_Bands.Active := True;

  Assert(tab_Bands.Active, 'function TdmLibrary.GetBandAveFreq_MHZ(aBand: string): Double; - tab_Bands.Active');

  with tab_Bands do
    if Locate(FLD_NAME, aBand,[]) then
    begin
      Result:=FieldByName(FLD_FREQ_AVE_MHZ).Asinteger;

    //  if Result=0 then
     //   Result := round ((FieldByName(FLD_FREQ_MAX_MHZ).Asinteger +
      //                    FieldByName(FLD_FREQ_MAX_MHZ).Asinteger) / 2)
    end
    else
      Result :=0;

end;
       {

// ---------------------------------------------------------------
function TdmLibrary.GetBandByFreq_MHZ___________(aFreq_MHZ: Double): String;
// ---------------------------------------------------------------
var
  eMin: integer;
  eMax: Integer;
begin
  FFreq_MHZ:=aFreq_MHZ;
  FBandStr := '';

//  db_ForEach(tab_Bands, ForEach_GetFreq);

  with tab_Bands do
    while not EOF do
  begin
    eMin:=FieldByName('FREQ_MIN_MHZ').Asinteger;
    eMax:=FieldByName('FREQ_MAX_MHZ').Asinteger;

    if (eMin <= FFreq_MHZ) and (FFreq_MHZ <= eMax )
    then
      FBandStr:=FieldByName(FLD_NAME).AsString;

    Next;
  end;


  Result := FBandStr;
end;

 }

(*
initialization
  if Assigned(FdmLibrary) then
    FreeAndNil(FdmLibrary);
*)


//    FdmLibrary.Free;

end.




(*
// ---------------------------------------------------------------
procedure TdmLibrary.ForEach_GetFreq(aDataset: TDataset; var aTerminated:   Boolean);
// ---------------------------------------------------------------
var
  eMin: integer;
  eMax: integer;
begin
  eMin:=aDataset.FieldByName('FREQ_MIN_MHZ').Asinteger;
  eMax:=aDataset.FieldByName('FREQ_MAX_MHZ').Asinteger;

  if (eMin <= FFreq_MHZ) and (FFreq_MHZ <= eMax )
  then
    FBandStr:=aDataset.FieldByName(FLD_NAME).AsString;
end;
*)


