unit dm_CalcMap;

interface

uses
  Classes, Forms, Variants, SysUtils, 

  dm_Onega_DB_data,

  dm_Main,

  u_const_db,
  

  u_db,
  u_func
  ;


type
  TdmCalcMapAddRec1 = record
     Section : string;

     Name: string;
     TabFileName: string;
     Matrix_FileName: string;

     Checked: boolean;

     PmpCalcRegionID: integer;
     CellLayer_ID: Integer;

     LinkFreqPlanID : Integer;


     Zone : Integer;
     x_min : Double;
     y_min : Double;
     x_max : double;
     y_max : double;

  end;


  TdmCalcMap = class(TDataModule)
  public
    function Delete(aID: integer): Boolean;
    function RegisterCalcMap_(aRec: TdmCalcMapAddRec1): integer;
  end;


function dmCalcMap: TdmCalcMap;


//==================================================================
// implementation
//==================================================================
implementation {$R *.DFM}

var
  FdmCalcMap: TdmCalcMap;


// ---------------------------------------------------------------
function dmCalcMap: TdmCalcMap;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmCalcMap) then
      FdmCalcMap := TdmCalcMap.Create(Application);

  Result := FdmCalcMap;
end;


//------------------------------------------------------------------------------
function TdmCalcMap.Delete(aID: integer): Boolean;
//------------------------------------------------------------------------------
begin
  Result := dmOnega_DB_data.CalcMap_Del(aID) > 0;
end;


//---------------------------------------------------------------
function TdmCalcMap.RegisterCalcMap_(aRec: TdmCalcMapAddRec1): integer;
//---------------------------------------------------------------
begin
  Assert(dmMain.ProjectID>0);
  Assert(aRec.TabFileName<>'', 'Value <=0');

  Assert(FileExists(aRec.TabFileName), aRec.TabFileName);

  Result :=dmOnega_DB_data.ExecStoredProc_ ('sp_CalcMap_Add',
       [
        FLD_PROJECT_ID,         dmMain.ProjectID,

        FLD_SECTION,            aRec.SECTION,

        FLD_NAME,               aRec.Name,
        FLD_FILENAME,           aRec.TabFileName,
        FLD_MATRIX_FILENAME,    aRec.matrix_FileName,

        FLD_CHECKED,            aRec.Checked,

        FLD_PMP_CALC_REGION_ID, aRec.PmpCalcRegionID,
        FLD_CELL_LAYER_ID,      aRec.CellLayer_ID,

        FLD_LinkFreqPlan_ID,    aRec.LinkFreqPlanID,

        FLD_zone,   aRec.zone,

        FLD_x_min,  IIF(aRec.x_min>0, Round(aRec.x_min), null),
        FLD_y_min,  IIF(aRec.y_min>0, Round(aRec.y_min), null),
        FLD_x_max,  IIF(aRec.x_max>0, Round(aRec.x_max), null),
        FLD_y_max,  IIF(aRec.y_max>0, Round(aRec.y_max), null)

       ]);

 // Assert(Result > 0, 'function TdmCalcMap.RegisterCalcMap_(aRec: TdmCalcMapAddRec1): integer; - sp_CalcMap_Add');

end;



begin

end.


