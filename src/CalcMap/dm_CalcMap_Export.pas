unit dm_CalcMap_Export;

interface

uses
  SysUtils, Classes, Forms, IniFiles, Graphics, DB, ADODB,

  u_const_db,
//  u_const_msg,
  u_const,

  u_classes,
  u_func,
  
  u_DB,
 // u_func_msg,
 // u_func_reg,

  u_files,
  u_dlg,

  //------------------------
  u_mapX,         // ExportVectorMapToGoogle
  mapXLib_TLB,    // ExportVectorMapToGoogle
  //------------------------

//  u_geo_types,
  u_geo,
 // u_types,
  u_xml,
  u_kml,

//  u_color_schema,
  u_progress_simple,

//  dm_ColorSchema,
//  dm_CalcRegion,
  dm_CalcMap,

//  dm_ColorSchema_import,
//  dm_ColorSchema_export,

  dm_Main

  ;


type
  TLocalMapType = (lmtKmz, lmtKml, lmtTabRaster, lmtTabVector);

  TdmCalcMap_Export = class(TDataModule)
    qry_CalcMaps: TADOQuery;
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FErrLogFileName: string;

  //  FRlfList: TStringList;

    procedure ExportVectorMapToGoogle(aMapFileName, aOutFileName: string;
                                      aTransparence: Integer);

//    procedure DoAddToRlfRegionList(aKeyName: string);

//    procedure ExportRlfList(aNode: variant);

    function  DoGetCaption(aIDList: TIDList; aInitFileNameOrPath: string;
        aQry: TAdoQuery; aMapType: TLocalMapType; aMapInd: Integer): string;

    procedure DoExportMap(aIDList: TIDList;
                          aFileNameOrPath: string;
                          aEnabledCluList: TIDList;
                          aMapType: TLocalMapType;
                          aTransparence: Integer;
                          aPrecKm: Integer;
                          aPattern: Integer;
                          aFieldsIndex: Integer
                         );

  public
    procedure ExportToKml (aIDList: TIDList;
                           aFileName: string; aTransparence: Integer;
                           aEnabledCluList: TIDList);

    procedure ExportToKmz (aIDList: TIDList;
                           aFileName: string; aTransparence: Integer;
                           aEnabledCluList: TIDList;
                           aPrecKm: Integer);

    procedure ExportToTabVector(aIDList: TIDList;
                                aFileName: string;
                                aPattern: Integer;
                                aFieldsIndex: Integer;
                                aEnabledCluList: TIDList);

    procedure ExportToTabRaster (aIDList: TIDList;
                                 aFileName: string; aTransparence: Integer;
                                 aEnabledCluList: TIDList);

  end;

var
  FdmCalcMap_Export: TdmCalcMap_Export;

function dmCalcMap_Export: TdmCalcMap_Export;


//==============================================================================
implementation {$R *.dfm}
//==============================================================================

const
  ERR_MSG_MO_MATRIX_F =
    '��������� ������� %s �� ����������. ����������� �� ��������';
  ERR_MSG_DRIVES =
    '������� ���� ��������� �������� ������ � ������ KML';
  ERR_MSG_NO_LEGEND =
    '�� ������� ������� ��������� �����. ��� �������� �������������� ��������� �����';



//------------------------------------------------------------------------------
function dmCalcMap_Export: TdmCalcMap_Export;
//------------------------------------------------------------------------------
begin
  if not Assigned(FdmCalcMap_Export) then
    FdmCalcMap_Export:= TdmCalcMap_Export.Create(Application);
  Result:= FdmCalcMap_Export;
end;

//------------------------------------------------------------------------------
procedure TdmCalcMap_Export.DataModuleCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  db_SetComponentsADOConn([qry_CalcMaps, qry_Temp], dmMain.ADOConnection);

  FErrLogFileName:= GetTempFileDir + 'bin_to_mif_err_log.txt';
 // FRlfList:= TStringList.Create;
end;

//------------------------------------------------------------------------------
procedure TdmCalcMap_Export.DataModuleDestroy(Sender: TObject);
//------------------------------------------------------------------------------
begin
 // FRlfList.Free;
end;

{
//------------------------------------------------------------------------------
procedure TdmCalcMap_Export.DoAddToRlfRegionList(aKeyName: string);
//------------------------------------------------------------------------------
var
  oRec: TKeyNameRec;
  oStrList: TStringList;
  i, iID: Integer;
begin
  dmCalcMap.ParseKeyName(aKeyName, oRec);
  iID:= IIF(oRec.RegionID>0, oRec.RegionID, oRec.RegionID_3G);
  if iID <= 0 then exit;

  oStrList:= TStringList.Create;
  dmCalcRegion.GetRlfList(iID, oStrList);

  for i := 0 to oStrList.Count - 1 do
    if FRlfList.IndexOf(oStrList[i])<0 then
      FRlfList.Add(oStrList[i]);

  oStrList.Free;
end;

//------------------------------------------------------------------------------
procedure TdmCalcMap_Export.ExportRlfList(aNode: variant);
//------------------------------------------------------------------------------
var i: Integer;
begin
  for i := 0 to FRlfList.Count - 1 do
    xml_AddNode (aNode, 'item', [xml_Par('filename', FRlfList[i]) ]);
end;

}

//------------------------------------------------------------------------------
function  TdmCalcMap_Export.DoGetCaption(aIDList: TIDList; aInitFileNameOrPath: string;
        aQry: TAdoQuery; aMapType: TLocalMapType; aMapInd: Integer): string;
//------------------------------------------------------------------------------
var
  sExt: string;
begin
  if aIDList.Count = 1 then
    Result:= aInitFileNameOrPath
  else begin
    Result:= IncludeTrailingBackslash(aInitFileNameOrPath) +
                 AsString(aMapInd) +'_'+
                 aQry.FieldByName(FLD_NAME).AsString + '.';

    case aMapType of
      lmtKmz      : Result:= Result + 'kmz';
      lmtKml      : Result:= Result + 'kml';
      lmtTabRaster: Result:= Result + 'tab';
      lmtTabVector: Result:= Result + 'tab';
    end;    
  end;
end;

//------------------------------------------------------------------------------
procedure TdmCalcMap_Export.ExportToKmz(aIDList: TIDList;
                                 aFileName: string; aTransparence: Integer;
                                 aEnabledCluList: TIDList;
                                 aPrecKm: Integer);
//------------------------------------------------------------------------------
begin
  DoExportMap(aIDList, aFileName, aEnabledCluList, lmtKmz, aTransparence, aPrecKm, 0, 0);
end;



//------------------------------------------------------------------------------
procedure TdmCalcMap_Export.ExportToKml(aIDList: TIDList;
            aFileName: string; aTransparence: Integer;
            aEnabledCluList: TIDList);
//------------------------------------------------------------------------------
begin
  DoExportMap(aIDList, aFileName, aEnabledCluList, lmtKml, aTransparence, 0, 0, 0);
end;

//------------------------------------------------------------------------------
procedure TdmCalcMap_Export.ExportToTabRaster (aIDList: TIDList;
                                    aFileName: string; aTransparence: Integer;
                                    aEnabledCluList: TIDList);
//------------------------------------------------------------------------------
begin
  DoExportMap(aIDList, aFileName, aEnabledCluList, lmtTabRaster, aTransparence, 0, 0, 0);
end;

//------------------------------------------------------------------------------
procedure TdmCalcMap_Export.ExportToTabVector(aIDList: TIDList; aFileName: string;
        aPattern: Integer; aFieldsIndex: Integer; aEnabledCluList: TIDList);
//------------------------------------------------------------------------------
begin
  DoExportMap(aIDList, aFileName, aEnabledCluList, lmtTabVector, 0, 0, aPattern, aFieldsIndex);
end;


//------------------------------------------------------------------------------
procedure TdmCalcMap_Export.DoExportMap(aIDList: TIDList;
    aFileNameOrPath: string; aEnabledCluList: TIDList;
    aMapType: TLocalMapType; aTransparence: Integer;
    aPrecKm: Integer; aPattern: Integer; aFieldsIndex: Integer );
//------------------------------------------------------------------------------
var
  sKey, sMatrix, sKgrMemTxtFile, sFileName, s, sCaption: string;
  vRoot, vClu, vMatr, vRLF, vPar, vColorSchema: variant;
  oColorSchema: TColorSchema;
  i, iID, iCSID: Integer;
  oRec: TKeyNameRec;
begin
  FRlfList.Clear;

  gl_XMLDoc.Clear;
  vRoot:= gl_XMLDoc.DocumentElement;

  vPar:= xml_AddNode(vRoot, 'PARAMS');
    xml_AddNode(vPar, 'ErrorLog_FileName'     , [xml_Par(ATT_VALUE, FErrLogFileName) ]);
    xml_AddNode(vPar, 'IsTransparent'         , [xml_Par(ATT_VALUE, '-1') ]);

    case aMapType of
      lmtTabVector: begin
        xml_AddNode(vPar, 'IsBuildVectorMaps'     , [xml_Par(ATT_VALUE, true) ]);
        xml_AddNode(vPar, 'VectorTypes'           , [xml_Par(ATT_VALUE, 'TAB') ]);
        xml_AddNode(vPar, 'MapCreateMethod'       , [xml_Par(ATT_VALUE, 'MapX') ]);
        xml_AddNode(vPar, 'TransparentVectorStyle', [xml_Par(ATT_VALUE, aPattern) ]);
      end;

      lmtTabRaster: begin
        xml_AddNode(vPar, 'MapCreateMethod'         , [xml_Par(ATT_VALUE, 'MapX') ]);
        xml_AddNode(vPar, 'TransparentRasterPercent', [xml_Par(ATT_VALUE, Trunc(255*aTransparence/100)) ]);
        xml_AddNode(vPar, 'RasterMapsFormat'        , [xml_Par(ATT_VALUE, 'png') ]);
      end;

      lmtKml: begin
        xml_AddNode(vPar, 'MapType', [xml_Par(ATT_VALUE, 'Google') ]);
        xml_AddNode(vPar, 'GoogleMapTransparence', [xml_Par(ATT_VALUE, aTransparence) ]);
      end;

      lmtKMZ: begin
        xml_AddNode(vPar, 'MapType'               , [xml_Par(ATT_VALUE, 'KMZ') ]);
        xml_AddNode(vPar, 'GoogleMapTransparence' , [xml_Par(ATT_VALUE, aTransparence) ]);
        xml_AddNode(vPar, 'MaxDistForKmzKm'       , [xml_Par(ATT_VALUE, aPrecKm) ]);
      end;
    end;


  vClu:= xml_AddNode(vRoot, 'ENABLED_CLU');
    for i := 0 to aEnabledCluList.Count - 1 do
      xml_AddNode (vClu, 'item', [xml_Par('code'   , aEnabledCluList[i].ID),
                                    xml_Par('enabled', aEnabledCluList[i].Tag2)  ]);


  vMatr := xml_AddNode(vRoot, 'MATRIXES');
  vRLF := xml_AddNode(vRoot, 'RLF');
  vColorSchema:= xml_AddNode (vRoot, TAG_COLOR_SCHEMAS);

  
  iCSID:= 0;
  for i := 0 to aIDList.Count - 1 do
  begin
    db_OpenTableByID(qry_CalcMaps, dmCalcMap.TableName, aIDList[i].ID);
    if qry_CalcMaps.IsEmpty then continue;

    sKey     := qry_CalcMaps.FieldByName(FLD_KEY_NAME).AsString;
    sFileName:= qry_CalcMaps.FieldByName(FLD_FILENAME).AsString;
    sMatrix  := qry_CalcMaps.FieldByName(FLD_calc_matrix_name).AsString;
    sCaption := qry_CalcMaps.FieldByName(FLD_NAME).AsString;

    if not MatrixExists(sMatrix) then
    begin
      if aIDList.Count = 1 then
        ErrDlg( Format(ERR_MSG_MO_MATRIX_F, [sMatrix]) )
      else
        PostEvent(WE_LOG, '', Format(ERR_MSG_MO_MATRIX_F, [sMatrix]));
      continue;
    end;

    oColorSchema:= dmCalcMap.LoadColorSchemaByMapFileName(sFileName);
    if oColorSchema = nil then begin
      if aIDList.Count = 1 then
        ErrDlg(ERR_MSG_NO_LEGEND)
      else
        PostEvent(WE_LOG, '', ERR_MSG_NO_LEGEND);
      continue;
    end;

    dmCalcMap.ParseKeyName(sKey, oRec);
    
    if aMapType = lmtTabVector then
      if oColorSchema.Par.Type_ = 'KGR' then
      begin
        iID:= IIF(oRec.RegionID>0, oRec.RegionID, oRec.RegionID_3G);

        sKgrMemTxtFile:= GetTempFileDir + Format('mem_fields_%d.txt', [iID]);
        dmCalcRegion.ExportKgrFields(iID, aFieldsIndex, sKgrMemTxtFile);

        xml_AddNode(vPar, 'MemFields_Kgr'         , [xml_Par(ATT_VALUE, sKgrMemTxtFile) ]);
        xml_AddNode(vPar, 'MemFldsIndex'          , [xml_Par(ATT_VALUE, aFieldsIndex) ]);
      end;

    s:= DoGetCaption(aIDList, aFileNameOrPath, qry_CalcMaps, aMapType, i);

    if aMapType in [lmtKmz, lmtKml] then
      if oRec.DriveID>0 then begin
        ExportVectorMapToGoogle(sFileName, s, aTransparence);
        continue;
      end;

    Inc(iCSID);
    oColorSchema.Par.ID:= iCSID;
    dmColorSchema_export.ExportItemToXMLNode (oColorSchema, vColorSchema);

    DoAddToRlfRegionList(sKey);

    xml_AddNode (vMatr, 'item',
        [xml_Par('filename', sMatrix),
         xml_Par('map',      s),
         xml_Par('caption',  sCaption ),
         xml_par('schema',   oColorSchema.Par.id)
        ]);
  end;

  ExportRlfList(vRlf);

  s:= GetTempFileDir + 'bin_to_mif_export.xml';
  gl_XMLDoc.SaveToFile (s);
  RunApp (EXE_BIN_TO_MIF, s);

  MsgDlg('���������');


end;




//------------------------------------------------------------------------------
procedure TdmCalcMap_Export.ExportVectorMapToGoogle(aMapFileName, aOutFileName: string;
                                      aTransparence: Integer);
//------------------------------------------------------------------------------
var
  oMiMap: TMiMap;

    function DoGetFeatureColor(aFeature: CMapXFeature): Integer;
    begin
      Result:= clWhite;

      case oMiMap.Layer_.PredominantFeatureType of
        miFeatureTypeRegion: Result:= aFeature.Style.RegionColor;
        miFeatureTypeSymbol: Result:= aFeature.Style.SymbolFontColor;
      end;
    end;

var
  oKML: TKML;
  sPoly_color: string;
  vFeatures: CMapXFeatures;
  oBlArr: TBLPointArray; oBLRect: TBLRect; oBLPos: TBLPoint;
  i, iTransp, iColor: Integer;
begin
  oMiMap:= TMiMap.Create;

  if not oMiMap.OpenFile(aMapFileName) then begin
    MsgDlg('�� ������� ������� ���� '+ aMapFileName);
    oMiMap.Free;
    exit;
  end;

  if oMiMap.Layer_.type_ = miLayerTypeNormal then
  begin
    oKML := TKML.Create();
    iTransp:= Trunc(255*aTransparence/100);

    with oKML do
    begin
      AddHeader;

      gl_simple_Progress.Show;

      oMiMap.Layer_.BeginAccess(miAccessRead);
      vFeatures:= oMiMap.Layer_.AllFeatures;

      gl_IDList.Clear;
      gl_simple_Progress.Max:= vFeatures.Count;

      for i := 1 to vFeatures.Count do begin
        gl_IDList.AddID( DoGetFeatureColor(vFeatures[i]) );
        gl_simple_Progress.Current:= i+1;
      end;

      for i := 0 to gl_IDList.Count - 1 do
      begin
        sPoly_color := IntToHex(iTransp, 2) + IntToHex(gl_IDList[i].ID, 6);
        oKML.AddStyle(sPoly_color, IntToStr(gl_IDList[i].ID));
        oKML.AddStyleMap(IntToStr(gl_IDList[i].ID));
      end;
    end;

    gl_simple_Progress.Max:= vFeatures.Count;

    for i := 1 to vFeatures.Count do
    begin
      case vFeatures[i].Type_ of
        miFeatureTypeRegion: mapx_GetBLPointArray(vFeatures[i], oBlArr);

        miFeatureTypeSymbol: begin
          oBLPos.B:= vFeatures[i].CenterY;
          oBLPos.L:= vFeatures[i].CenterX;
          oBLRect:= geo_GetSquareAroundPoint(oBLPos, 20);
          SetLength(oBlArr, 4);
          oBlArr[0]:= oBLRect.TopLeft;
          oBlArr[1].B:= oBLRect.TopLeft.B;
          oBlArr[1].L:= oBLRect.BottomRight.L;
          oBlArr[2]:= oBLRect.BottomRight;
          oBlArr[3].B:= oBLRect.BottomRight.B;
          oBlArr[3].L:= oBLRect.TopLeft.L;
        end;
      end;

      iColor:= DoGetFeatureColor(vFeatures[i]);

      oKML.AddPlacemark(oBlArr, iColor);
      if i mod 50 = 0 then
        gl_simple_Progress.Current:= i+1;
    end;

    oMiMap.Layer_.EndAccess(miAccessEnd);

    vFeatures:= nil;
    gl_simple_Progress.Close;

    with oKML do begin
      AddFooter;
      SaveToFile(aOutFileName);
      Free;
    end;
  end;

  oMiMap.CloseFile;
  oMiMap.Free;

  vFeatures:= nil;
end;



end.
















































(*

  db_OpenTableByID(qry_CalcMaps, dmCalcMap.TableName, aID);

  sFileName:= qry_CalcMaps.FieldByName(FLD_FILENAME).AsString;
  if sFileName='' then exit;

  sKey:= qry_CalcMaps.FieldByName(FLD_KEY_NAME).AsString;

  dmCalcMap.ParseKeyName(sKey, oRec);

  if oRec.DriveID>0 then begin
    MsgDlg(ERR_MSG_DRIVES); exit;
  end;


  //------------------------------------------------------------------
  if (oRec.RegionID>0) or (oRec.RegionID_3g>0) then
  begin
    oColorSchema:= dmCalcMap.LoadColorSchemaByMapFileName(sFileName);
    if oColorSchema = nil then begin
      ErrDlg(ERR_MSG_NO_LEGEND);
      exit;
    end;

    sMatrix:= qry_CalcMaps.FieldByName(FLD_calc_matrix_name).AsString;
//    sMatrix:= dmCalcMap.GetMatrixNameByMapName(oRec, sFileName);

    if not MatrixExists(sMatrix) then begin
      MsgDlg('����� '+ sMatrix+ ' �� ����������. ��� �������� ����������� ��������� ����� (��������� � ������� ������� ������)');
      exit;
    end;

    gl_XMLDoc.Clear;
    vRoot:= gl_XMLDoc.DocumentElement;

    vGroup:=xml_AddNode(vRoot, 'PARAMS');
      xml_AddNode(vGroup, 'MapType', [xml_Par(ATT_VALUE, 'KMZ') ]);
      xml_AddNode(vGroup, 'GoogleMapTransparence', [xml_Par(ATT_VALUE, aTransparence) ]);
      xml_AddNode(vGroup, 'MaxDistForKmzKm', [xml_Par(ATT_VALUE, aPrecKm) ]);

    vGroup:= xml_AddNode(vRoot,'ENABLED_CLU');
    for i := 0 to aEnabledCluList.Count - 1 do
      xml_AddNode (vGroup, 'item', [xml_Par('code'   , aEnabledCluList[i].ID),
                                    xml_Par('enabled', aEnabledCluList[i].Tag2)
                                   ]);

    iID:= IIF(oRec.RegionID>0, oRec.RegionID, oRec.RegionID_3g);

    vGroup := xml_AddNode(vRoot,'MATRIXES');
      xml_AddNode (vGroup, 'item',
                  [xml_Par('filename', sMatrix),
                   xml_Par('map',      aFileName),
                   xml_Par('caption',  dmCalcRegion.GetNameByID(iID) ),
                   xml_par('schema',   oColorSchema.Par.id)
                  ]);

    oStrList:= TStringList.Create;
    dmCalcRegion.GetRlfList(iID, oStrList);

    vGroup := xml_AddNode(vRoot,'RLF');
    for i := 0 to oStrList.Count - 1 do
      xml_AddNode (vGroup, 'item', [xml_Par('filename', oStrList[i]) ]);

    oStrList.Free;

    vGroup:= xml_AddNode (vRoot, TAG_COLOR_SCHEMAS);
    dmColorSchema_export.ExportItemToXMLNode (oColorSchema, vGroup);

    s:= GetTempFileDir + 'bin_to_mif_google.xml';
    gl_XMLDoc.SaveToFile (s);
    if RunApp (EXE_BIN_TO_MIF, s) then
      MsgDlg('���������');
  end else
  begin
    MsgDlg('����������� ������ ����� ��� ��������'); exit;
  end;

*)


(*
    sCSFileName:= sFileName + '.xml';
    if FileExists(sCSFileName) then
    begin
      dmColorSchema_import.LoadFromXML(sCSFileName, oColorSchema);
      dmColorSchema_export.ExportItemToXMLNode (oColorSchema, vGroup);
      iColorSchemaID:= oColorSchema.Par.id;
      oColorSchema.Free;
    end else begin
      if oRec.FreqPlanID>0 then begin
        db_OpenTableByID(qry_Temp, TBL_FREQPLAN, oRec.FreqPlanID);

        with qry_Temp do begin
          dmColorSchema_export.ExportItemToXMLNode (FieldByName(FLD_ci_color_schema_id).AsInteger, vGroup);
          dmColorSchema_export.ExportItemToXMLNode (FieldByName(FLD_ca_color_schema_id).AsInteger, vGroup);
          dmColorSchema_export.ExportItemToXMLNode (FieldByName(FLD_cia_color_schema_id).AsInteger, vGroup);
        end;
      end else
      begin
        db_OpenTableByID(qry_Temp, TBL_CALCREGION, oRec.RegionID);

        with qry_Temp do begin
          dmColorSchema_export.ExportItemToXMLNode (FieldByName(FLD_KUP_ID).AsInteger, vGroup);
          dmColorSchema_export.ExportItemToXMLNode (FieldByName(FLD_KMO_ID).AsInteger, vGroup);
          dmColorSchema_export.ExportItemToXMLNode (FieldByName(FLD_HANDOVER_ID).AsInteger, vGroup);
          dmColorSchema_export.ExportItemToXMLNode (FieldByName(FLD_color_schema_id_LOS).AsInteger, vGroup);
          dmColorSchema_export.ExportItemToXMLNode (FieldByName(FLD_color_schema_id_connection_prb).AsInteger, vGroup);
        end;
      end;

      iColorSchemaID:= 0;
      if Pos('kgr', sFileName)>0 then
        iColorSchemaID:= -1 else
      if Pos('kup', sFileName)>0 then
        iColorSchemaID:= qry_Temp.FieldByName(FLD_KUP_ID).AsInteger else
      if Pos('kmo', sFileName)>0 then
        iColorSchemaID:= qry_Temp.FieldByName(FLD_KMO_ID).AsInteger else
      if Pos('hoff', sFileName)>0 then
        iColorSchemaID:= qry_Temp.FieldByName(FLD_HANDOVER_ID).AsInteger else

      if Pos('ci', sFileName)>0 then
        iColorSchemaID:= qry_Temp.FieldByName(FLD_ci_color_schema_id).AsInteger else
      if Pos('ca', sFileName)>0 then
        iColorSchemaID:= qry_Temp.FieldByName(FLD_ca_color_schema_id).AsInteger else
      if Pos('cia', sFileName)>0 then
        iColorSchemaID:= qry_Temp.FieldByName(FLD_cia_color_schema_id).AsInteger
      ;
    end;
*)


(*//------------------------------------------------------------------------------
procedure TdmCalcMap_Export.ExportToKmz(aID: Integer;
                                    aFileName: string; aTransparence: Integer;
                                    aEnabledCluList: TIDList;
                                    aPrecKm: Integer);
//------------------------------------------------------------------------------
var
  oIDList: TIDLIst;
begin
  oIDList:= TIDLIst.Create;
  oIDList.AddID(aID);
  ExportToKmz(oIDList, aFileName, aTransparence, aEnabledCluList, aPrecKm);
  oIDList.Free;
end;
*)
