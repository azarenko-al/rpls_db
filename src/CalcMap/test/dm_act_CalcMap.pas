unit dm_act_CalcMap;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ActnList,

  dm_act_Base,

  u_func_msg,
  u_dlg,
  u_func,
  u_Types,

  u_const_str,
  u_const_msg,

  dm_CalcMap,
  dm_CalcMap_View

  ;

type
  TdmAct_CalcMap = class(TdmAct_Base)
    act_AddMap: TAction;
    act_AddSection: TAction;
    act_AddRegion: TAction;
  private
    procedure DoAction (Sender: TObject);
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;

    function Add (aParentID, aRegionID: integer; aName: string): integer;
    procedure Del (aGUID: string); //aID: integer;

  public

    class procedure Init;
  end;


var
  dmAct_CalcMap: TdmAct_CalcMap;

//==================================================================
implementation {$R *.dfm}
//==================================================================


//--------------------------------------------------------------------
class procedure TdmAct_CalcMap.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_CalcMap) then
    dmAct_CalcMap:=TdmAct_CalcMap.Create(Application);
end;


//-------------------------------------------------------------------
procedure TdmAct_CalcMap.DoAction (Sender: TObject);
//-------------------------------------------------------------------
begin
//  if Sender=act_AddSection then dmCalcMaps.RegisterSection (FFocusedID,0,'section') else
//  if Sender=act_AddMap     then dmCalcMaps.RegisterMap (FFocusedID,0, 'map','eee') else
  if Sender=act_Del_       then Del (FFocusedGUID) else
end;

//--------------------------------------------------------------------
procedure TdmAct_CalcMap.Del (aGUID: string);  //aID: integer;
//--------------------------------------------------------------------
begin

 if ConfirmDlg (STR_ASK_DEL) then
   if dmCalcMap.DelByGUID (aGUID) then
   begin
//     PostDelNode (aID, ntItem);
   end;

end;


//--------------------------------------------------------------------
function TdmAct_CalcMap.Add (aParentID, aRegionID: integer; aName: string): integer;
//--------------------------------------------------------------------
begin
//  Result:= dmCalcMaps.RegisterSection(aParentID, aRegionID, aName);
end;


//--------------------------------------------------------------------
procedure TdmAct_CalcMap.GetPopupMenu;
//--------------------------------------------------------------------
begin
  case aMenuType of
    mtFolder: begin
      AddMenuItem (aPopupMenu, act_AddSection);
      AddMenuItem (aPopupMenu, act_AddMap);
      AddMenuItem (aPopupMenu, nil);
      AddMenuItem (aPopupMenu, act_Del_);

//      act_FolderDel.Enabled:=(FFocusedID > 0);
    end;

    mtItem:
      AddMenuItem (aPopupMenu, act_Del_);

    mtList:
      AddMenuItem (aPopupMenu, act_Del_);
  end;
end;



end.