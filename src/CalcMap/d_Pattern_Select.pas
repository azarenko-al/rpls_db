unit d_Pattern_Select;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms;

type
  TPatternSelectDialog = class(TForm)
    procedure FormPaint(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
  private
    FModal: Boolean;
    FImageList: TImageList;
    FPattern: Integer;
    FCols,Frows: Integer;
    FPainted: Boolean;

    procedure Paint; override;
    procedure SetImageList(const Value: TImageList);
  public
    OnPatternSelect: TNotifyEvent;

    function  Execute: Boolean;

    property Pattern: Integer read FPattern;
    property ImageList: TImageList read FImageList write SetImageList;

  end;

implementation {$R *.DFM}


//------------------------------------------------------------------------------
procedure TPatternSelectDialog.FormPaint(Sender: TObject);
//------------------------------------------------------------------------------
begin
  Paint;
end;

//------------------------------------------------------------------------------
procedure TPatternSelectDialog.FormMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
//------------------------------------------------------------------------------
var
  P: Integer;
begin
  if FImageList<>nil then
  begin
    P:=((Y-1) div (FImageList.Height+1))*Fcols+((X-1) div (FImageList.Width+1));
    if P>=FImageList.Count then
      P:=FImageList.Count-1;
    FPattern:=P;
    if FModal then
      ModalResult:=mrOK else begin Hide; OnPatternSelect(Self); end;
  end;
end;

//------------------------------------------------------------------------------
procedure TPatternSelectDialog.Paint;
//------------------------------------------------------------------------------
var
  X,Y,I: Integer;
begin
  if FImageList=nil then
    Exit;
  if FImageList.Count>0 then
  begin
    Canvas.Rectangle(Canvas.ClipRect);

    X:=0;Y:=0;

    for I:=0 to FImageList.Count-1 do
    begin
      FImageList.Draw(Canvas,X*(FImageList.Width+1)+1,Y*(FImageList.Height+1)+1,I,True);
      inc(X);
      if X>=Fcols then begin X:=0;inc(Y);end;
    end;
  end;
end;

//------------------------------------------------------------------------------
procedure TPatternSelectDialog.FormClose(Sender: TObject;
  var Action: TCloseAction);
//------------------------------------------------------------------------------
begin
  FModal:=False;
end;

//------------------------------------------------------------------------------
function TPatternSelectDialog.Execute: Boolean;
//------------------------------------------------------------------------------
begin
  Left:= Mouse.CursorPos.X; // (Screen.Width -Width) div 2;
  Top := Mouse.CursorPos.Y; // (Screen.Height-Height) div 2;
  FModal:=True;
  Result:=(ShowModal=mrOK);
end;

//------------------------------------------------------------------------------
procedure TPatternSelectDialog.FormCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  Caption:='����� �������';
  FCols:=8;//Width div FImageList.Width;
end;

//------------------------------------------------------------------------------
procedure TPatternSelectDialog.FormDeactivate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  Hide;
end;

//------------------------------------------------------------------------------
procedure TPatternSelectDialog.SetImageList(const Value: TImageList);
//------------------------------------------------------------------------------
begin
  if Value<>nil then
  begin
    FImageList := Value;
    FCols:=8;
    FRows:=FImageList.Count div Fcols;
    if FImageList.Count mod Fcols<>0 then
      inc(Frows);

    Width :=(FImageList.Width +1)*FCols+4;
    Height:=(FImageList.Height+1)*FRows+4;
  end;
end;

//------------------------------------------------------------------------------
procedure TPatternSelectDialog.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
//------------------------------------------------------------------------------
begin
  if Key=VK_ESCAPE then
  begin
    if FModal then ModalResult:=mrCancel else Hide;
  end;
end;

//------------------------------------------------------------------------------
procedure TPatternSelectDialog.FormActivate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  Show;
end;


end.
