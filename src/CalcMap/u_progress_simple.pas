unit u_progress_simple;

interface

uses u_func, SysUtils, Forms;

type
  TOnSimpleProgressSetVisible = procedure (aVal: boolean) of object;
  TOnSimpleProgressCurrent    = procedure (aVal: double ) of object;

  TSimpleProgress = class
  private
    FCurrent, FMax: double;
    FOldPercent: double;
    FOldTime: TDateTime;

    FOnVisible  : TOnSimpleProgressSetVisible ;
    FOnCurrent  : TOnSimpleProgressCurrent    ;

    procedure SetCurrent(aVal: double);
    procedure SetMax    (aVal: double);
  public
    Terminated: boolean;

    property  Current: double write SetCurrent;
    property  Max    : double read FMax write SetMax;

    property  OnVisible: TOnSimpleProgressSetVisible  read FOnVisible       write FOnVisible;
    property  OnCurrent: TOnSimpleProgressCurrent     read FOnCurrent       write FOnCurrent;

    procedure Show;
    procedure Hide;
    procedure Close;
  end;

var
  Fgl_SimpleProgress: TSimpleProgress;

function gl_simple_Progress: TSimpleProgress;

implementation

uses DateUtils;

function gl_simple_Progress: TSimpleProgress;
begin
  if not Assigned(Fgl_SimpleProgress) then
    Fgl_SimpleProgress:= TSimpleProgress.Create;

  Result:= Fgl_SimpleProgress;
end;

procedure TSimpleProgress.SetCurrent(aVal: double);
begin
  FCurrent:= aVal;

  if not Assigned(FOnCurrent) then
    exit;

  if (FMax<=0) then begin
    FOnCurrent(0); exit;
  end;

  try

(*
  if TruncFloat(aVal/FMax*100, 1) <> FOldPercent then
  begin
    FOldPercent:= TruncFloat(aVal/FMax*100, 1);
    FOnCurrent(FOldPercent)
  end;
*)
  if (MilliSecondsBetween(Now, FOldTime) > 300) or (FCurrent = FMax) then
  begin
    FOnCurrent(TruncFloat(aVal/FMax*100, 1));
//    Application.ProcessMessages;
    FOldTime:= Now;
  end;

  except
  end;
end;

procedure TSimpleProgress.SetMax(aVal: double);
begin
  Terminated:= false;
  FMax:= aVal;
//  if Assigned(FOnMax) then
//    FOnMax(aVal);
end;

procedure TSimpleProgress.Show;
begin
  Terminated:= false;
  Current:= 0;
  if Assigned(FOnVisible) then
    FOnVisible(true);
end;

procedure TSimpleProgress.Hide;
begin
  Current:= 0;
  if Assigned(FOnVisible) then
    FOnVisible(false);
  if Assigned(FOnCurrent) then
    FOnCurrent(0);
end;

procedure TSimpleProgress.Close;
begin
  Hide;
end;


end.
