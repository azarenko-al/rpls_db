inherited dlg_LinkLine_add: Tdlg_LinkLine_add
  Left = 466
  Top = 135
  Width = 794
  Height = 594
  Caption = 'dlg_LinkLine_add'
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 531
    Width = 786
    inherited Bevel1: TBevel
      Width = 786
    end
    inherited Panel3: TPanel
      Left = 583
      Width = 203
      inherited btn_Ok: TButton
        Left = 7
      end
      inherited btn_Cancel: TButton
        Left = 91
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 786
    inherited Bevel2: TBevel
      Width = 786
    end
    inherited pn_Header: TPanel
      Width = 786
      Height = 55
    end
  end
  inherited Panel1: TPanel
    Width = 786
    inherited ed_Name_: TEdit
      Width = 774
    end
  end
  inherited Panel2: TPanel
    Width = 786
    Height = 359
    inherited PageControl1: TPageControl
      Width = 776
      Height = 349
      inherited TabSheet1: TTabSheet
        object cxGrid_L: TcxGrid
          Left = 0
          Top = 0
          Width = 239
          Height = 283
          Align = alLeft
          TabOrder = 0
          LookAndFeel.Kind = lfFlat
          object cx_Left: TcxGridDBBandedTableView
            OnDblClick = cx_LeftDblClick
            NavigatorButtons.ConfirmDelete = False
            DataController.Filter.MaxValueListCount = 1000
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086': 0'
                Kind = skCount
                Column = cx_LeftNAME
              end>
            DataController.Summary.SummaryGroups = <>
            Filtering.ColumnPopup.MaxDropDownItemCount = 12
            OptionsCustomize.ColumnMoving = False
            OptionsCustomize.ColumnSorting = False
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Editing = False
            OptionsSelection.HideFocusRectOnExit = False
            OptionsSelection.InvertSelect = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.GroupFooters = gfAlwaysVisible
            OptionsView.BandHeaders = False
            Preview.AutoHeight = False
            Preview.MaxLineCount = 2
            Bands = <
              item
                Caption = #1057#1086#1089#1077#1076#1085#1080#1077' '#1087#1088#1086#1083#1105#1090#1099
              end>
            object cx_LeftNAME: TcxGridDBBandedColumn
              Tag = 1
              Caption = #1057#1086#1089#1077#1076#1085#1080#1077' '#1080#1085#1090#1077#1088#1074#1072#1083#1099
              DataBinding.FieldName = 'NAME'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.MaxLength = 0
              Properties.ReadOnly = True
              Options.Filtering = False
              SortIndex = 0
              SortOrder = soDescending
              Width = 223
              Position.BandIndex = 0
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object cx_LeftID: TcxGridDBBandedColumn
              DataBinding.FieldName = 'ID'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.MaxLength = 0
              Properties.ReadOnly = True
              Visible = False
              Options.Filtering = False
              VisibleForCustomization = False
              Width = 88
              Position.BandIndex = 0
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object cx_LeftDBBandedColumn1: TcxGridDBBandedColumn
              DataBinding.FieldName = 'property1_id'
              Visible = False
              Options.Filtering = False
              VisibleForCustomization = False
              Position.BandIndex = 0
              Position.ColIndex = 2
              Position.RowIndex = 0
            end
            object cx_LeftDBBandedColumn2: TcxGridDBBandedColumn
              DataBinding.FieldName = 'property2_id'
              Visible = False
              Options.Filtering = False
              VisibleForCustomization = False
              Position.BandIndex = 0
              Position.ColIndex = 3
              Position.RowIndex = 0
            end
            object cx_LeftDBBandedColumn3: TcxGridDBBandedColumn
              DataBinding.FieldName = 'property_id'
              Visible = False
              VisibleForCustomization = False
              Position.BandIndex = 0
              Position.ColIndex = 4
              Position.RowIndex = 0
            end
          end
          object cxGrid_Left: TcxGridLevel
            GridView = cx_Left
          end
        end
        object Panel4: TPanel
          Left = 0
          Top = 283
          Width = 768
          Height = 35
          Align = alBottom
          BevelOuter = bvNone
          Constraints.MaxHeight = 35
          Constraints.MinHeight = 35
          TabOrder = 1
          object Button7: TButton
            Left = 1
            Top = 9
            Width = 155
            Height = 23
            Action = act_Select_first_link
            TabOrder = 0
          end
          object Button8: TButton
            Left = 166
            Top = 9
            Width = 75
            Height = 23
            Caption = #1054#1095#1080#1089#1090#1080#1090#1100
            TabOrder = 1
            Visible = False
            OnClick = _ActionMenu
          end
        end
        object cxGrid_C: TcxGrid
          Left = 268
          Top = 0
          Width = 271
          Height = 283
          Align = alLeft
          TabOrder = 2
          LookAndFeel.Kind = lfFlat
          object cx_LInks: TcxGridDBBandedTableView
            NavigatorButtons.ConfirmDelete = False
            DataController.Filter.MaxValueListCount = 1000
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086': 0'
                Kind = skCount
                Column = cx_LInksname
              end>
            DataController.Summary.SummaryGroups = <>
            Filtering.ColumnPopup.MaxDropDownItemCount = 12
            OptionsCustomize.ColumnMoving = False
            OptionsCustomize.ColumnSorting = False
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.BandMoving = False
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsSelection.CellSelect = False
            OptionsSelection.HideFocusRectOnExit = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsView.ColumnAutoWidth = True
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.GroupFooters = gfVisibleWhenExpanded
            OptionsView.BandHeaders = False
            Preview.AutoHeight = False
            Preview.MaxLineCount = 2
            Preview.Visible = True
            Bands = <
              item
                Caption = #1056#1056' '#1080#1085#1090#1077#1088#1074#1072#1083#1099' '#1074' '#1083#1080#1085#1080#1080
                Width = 185
              end>
            object cx_LInksname: TcxGridDBBandedColumn
              Tag = 1
              Caption = #1056#1056' '#1080#1085#1090#1077#1088#1074#1072#1083#1099' '#1074' '#1083#1080#1085#1080#1080
              DataBinding.FieldName = 'name'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.MaxLength = 0
              Properties.ReadOnly = False
              Options.Filtering = False
              Width = 134
              Position.BandIndex = 0
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object cx_LInksid: TcxGridDBBandedColumn
              DataBinding.FieldName = 'id'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.MaxLength = 0
              Properties.ReadOnly = False
              Visible = False
              Options.Filtering = False
              VisibleForCustomization = False
              Width = 30
              Position.BandIndex = 0
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object cx_main_property1_id: TcxGridDBBandedColumn
              DataBinding.FieldName = 'property1_id'
              Visible = False
              Options.Filtering = False
              VisibleForCustomization = False
              Position.BandIndex = 0
              Position.ColIndex = 2
              Position.RowIndex = 0
            end
            object cx_main_property2_id: TcxGridDBBandedColumn
              DataBinding.FieldName = 'property2_id'
              Visible = False
              Options.Filtering = False
              VisibleForCustomization = False
              Position.BandIndex = 0
              Position.ColIndex = 3
              Position.RowIndex = 0
            end
            object cx_LInksDBBandedColumn1: TcxGridDBBandedColumn
              DataBinding.FieldName = 'property_id'
              Visible = False
              VisibleForCustomization = False
              Position.BandIndex = 0
              Position.ColIndex = 4
              Position.RowIndex = 0
            end
          end
          object cxGrid_CLevel1: TcxGridLevel
            GridView = cx_LInks
          end
        end
        object pn_Buttons_right: TPanel
          Left = 584
          Top = 0
          Width = 29
          Height = 283
          Align = alRight
          Alignment = taLeftJustify
          BevelOuter = bvNone
          TabOrder = 4
          object Button5: TButton
            Left = 3
            Top = 6
            Width = 24
            Height = 24
            Action = act_add_right_neighbour
            TabOrder = 0
          end
          object Button6: TButton
            Left = 3
            Top = 35
            Width = 24
            Height = 24
            Action = act_del_right_neighbour
            TabOrder = 1
          end
        end
        object cxGrid_R: TcxGrid
          Left = 613
          Top = 0
          Width = 155
          Height = 283
          Align = alRight
          TabOrder = 3
          LookAndFeel.Kind = lfFlat
          object cx_Right: TcxGridDBBandedTableView
            OnDblClick = cx_RightDblClick
            NavigatorButtons.ConfirmDelete = False
            DataController.Filter.MaxValueListCount = 1000
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086': 0'
                Kind = skCount
                Column = cx_RightNAME
              end>
            DataController.Summary.SummaryGroups = <>
            Filtering.ColumnPopup.MaxDropDownItemCount = 12
            OptionsCustomize.ColumnMoving = False
            OptionsCustomize.ColumnSorting = False
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.BandMoving = False
            OptionsData.Editing = False
            OptionsSelection.HideFocusRectOnExit = False
            OptionsSelection.InvertSelect = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.GroupFooters = gfVisibleWhenExpanded
            OptionsView.BandHeaders = False
            Preview.AutoHeight = False
            Preview.MaxLineCount = 2
            Bands = <
              item
                Caption = #1057#1086#1089#1077#1076#1085#1080#1077' '#1087#1088#1086#1083#1105#1090#1099
              end>
            object cx_RightNAME: TcxGridDBBandedColumn
              Tag = 1
              Caption = #1057#1086#1089#1077#1076#1085#1080#1077' '#1080#1085#1090#1077#1088#1074#1072#1083#1099
              DataBinding.FieldName = 'NAME'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.MaxLength = 0
              Properties.ReadOnly = True
              Options.Filtering = False
              SortIndex = 0
              SortOrder = soDescending
              Width = 223
              Position.BandIndex = 0
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object cx_RightID: TcxGridDBBandedColumn
              DataBinding.FieldName = 'ID'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.MaxLength = 0
              Properties.ReadOnly = True
              Visible = False
              Options.Filtering = False
              VisibleForCustomization = False
              Width = 88
              Position.BandIndex = 0
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object cx_RightDBBandedColumn2_property1_id: TcxGridDBBandedColumn
              DataBinding.FieldName = 'property1_id'
              Visible = False
              Options.Filtering = False
              VisibleForCustomization = False
              Position.BandIndex = 0
              Position.ColIndex = 2
              Position.RowIndex = 0
            end
            object cx_RightDBBandedColumn3_property2_id: TcxGridDBBandedColumn
              DataBinding.FieldName = 'property2_id'
              Visible = False
              Options.Filtering = False
              VisibleForCustomization = False
              Position.BandIndex = 0
              Position.ColIndex = 3
              Position.RowIndex = 0
            end
            object cx_RightDBBandedColumn1: TcxGridDBBandedColumn
              DataBinding.FieldName = 'property_id'
              Visible = False
              VisibleForCustomization = False
              Position.BandIndex = 0
              Position.ColIndex = 4
              Position.RowIndex = 0
            end
          end
          object cxGrid_Right: TcxGridLevel
            GridView = cx_Right
          end
        end
        object pn_Buttons_Left: TPanel
          Left = 239
          Top = 0
          Width = 29
          Height = 283
          Align = alLeft
          Alignment = taLeftJustify
          BevelOuter = bvNone
          TabOrder = 5
          object Button1: TButton
            Left = 3
            Top = 6
            Width = 24
            Height = 24
            Action = act_add_left_neighbour
            TabOrder = 0
          end
          object Button2: TButton
            Left = 3
            Top = 35
            Width = 24
            Height = 24
            Action = act_del_left_neighbour
            TabOrder = 1
          end
        end
      end
    end
  end
  object ToolBar1: TToolBar [4]
    Left = 0
    Top = 502
    Width = 786
    Height = 29
    Align = alBottom
    Caption = 'ToolBar1'
    TabOrder = 4
    Visible = False
  end
  object StatusBar1: TStatusBar [5]
    Left = 0
    Top = 483
    Width = 786
    Height = 19
    Panels = <>
    SimplePanel = True
    Visible = False
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 424
    Top = 3
    inherited act_Ok: TAction
      OnExecute = _ActionMenu
    end
    inherited act_Cancel: TAction
      OnExecute = _ActionMenu
    end
  end
  inherited FormStorage1: TFormStorage
    Left = 383
    Top = 2
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 350
    Top = 3
  end
  object Actions: TActionList
    OnUpdate = ActionsUpdate
    Left = 273
    Top = 4
    object act_Select_first_link: TAction
      Caption = #1042#1099#1073#1088#1072#1090#1100' '#1087#1077#1088#1074#1099#1081' '#1080#1085#1090#1077#1088#1074#1072#1083
      OnExecute = _ActionMenu
    end
    object act_add_right_neighbour: TAction
      Caption = '<'
      OnExecute = _ActionMenu
    end
    object act_del_right_neighbour: TAction
      Caption = '>'
      OnExecute = _ActionMenu
    end
    object act_add_left_neighbour: TAction
      Caption = '>'
      OnExecute = _ActionMenu
    end
    object act_del_left_neighbour: TAction
      Caption = '<'
      OnExecute = _ActionMenu
    end
  end
end
