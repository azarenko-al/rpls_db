unit dm_LinkLine_Add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  dxmdaset, Db, ADODB, RxMemDS,

  d_LinkLine_Get_First_Link,

  dm_Onega_DB_data,

  dm_Main,
  u_db,
  u_func,
  u_dlg,
  u_classes,

  u_const_str,
  u_const_db,

  u_types,

  u_linkLine_const,

  u_LinkLine_Add_classes;

type
  TNeighbourType = (ntLeft, ntRight);


  TdmLinkLine_Add = class(TDataModule)
    ds_Items: TDataSource;
    mem_Items: TdxMemData;
    qry_Links_all: TADOQuery;
    mem_Left: TdxMemData;
    mem_Right: TdxMemData;
    ds_Left: TDataSource;
    ds_Right: TDataSource;
    ds_All: TDataSource;

    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FLinkList_All: TLinkLine_Link_List;

    FLinkList_Left: TLinkLine_Link_List;
    FLinkList_Right:    TLinkLine_Link_List;
    FLinkList_Selected: TLinkLine_Link_List;

    procedure UpdateLeftMemItems;
    procedure UpdateRightMemItems;

  public
  //  class procedure Init;


    procedure del_neighbour(aIndex: TNeighbourType);
    procedure add_neighbour(aIndex: TNeighbourType);
    procedure Clear;

    function Dlg_AddFirstInterval1(): Boolean;

//    procedure Edit(aIDList: TIDList);

    procedure InitItemsFromIDList(aIDList: TIDList);

    procedure OpenDB;


  end;


function  dmLinkLine_Add: TdmLinkLine_Add;
procedure dmLinkLine_Add_Free;

//
//var
//  dmLinkLine_Add: TdmLinkLine_Add;


implementation
{$R *.DFM}

var
  FdmLinkLine_Add: TdmLinkLine_Add;


function dmLinkLine_Add: TdmLinkLine_Add;
begin
  if not Assigned(FdmLinkLine_Add) then
    FdmLinkLine_Add := TdmLinkLine_Add.Create(Application);

  Result := FdmLinkLine_Add;
end;


procedure dmLinkLine_Add_Free;
begin
  if Assigned(FdmLinkLine_Add) then
    FreeAndNil(FdmLinkLine_Add);
end;


//procedure TdmLinkLine_Add.Init;
//begin
//  if not Assigned(dmLinkLine_Add) then
//    dmLinkLine_Add := TdmLinkLine_Add.Create(Application);
//end;
//


// ---------------------------------------------------------------
procedure TdmLinkLine_Add.DataModuleDestroy(Sender: TObject);
// ---------------------------------------------------------------
begin
  FreeAndNil(FLinkList_Right);
  FreeAndNil(FLinkList_Selected);
  FreeAndNil(FLinkList_Left);
  FreeAndNil(FLinkList_All);
end;


//-------------------------------------------------------------------
procedure TdmLinkLine_Add.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  db_CreateField (mem_Items,
             [db_Field(FLD_ID,          ftInteger),
              db_Field(FLD_NAME,        ftString, 100 ),

              db_Field(FLD_property1_ID, ftInteger),
              db_Field(FLD_property2_ID, ftInteger)
              ]);

  mem_Items.Open;
  mem_Right.CreateFieldsFromDataSet(mem_Items);
  mem_Left.CreateFieldsFromDataSet(mem_Items);


  FLinkList_All      := TLinkLine_Link_List.Create();
  FLinkList_Selected := TLinkLine_Link_List.Create();
  FLinkList_Left     := TLinkLine_Link_List.Create();
  FLinkList_Right    := TLinkLine_Link_List.Create();
end;


//----------------------------------------------------------------------
procedure TdmLinkLine_add.UpdateLeftMemItems;
//----------------------------------------------------------------------
var
  oLinkLine_Link: TLinkLine_Link;
begin
  if FLinkList_Selected.Count=0 then
    Exit;

  oLinkLine_Link :=FLinkList_Selected.Items[0];

  FLinkList_All.SelectByProperty_ID (oLinkLine_Link.Property1_ID,
    FLinkList_Left, FLinkList_Selected);

  FLinkList_Left.SaveToDataset(mem_Left);
end;



//----------------------------------------------------------------------
procedure TdmLinkLine_add.UpdateRightMemItems;
//----------------------------------------------------------------------
var
  oLinkLine_Link: TLinkLine_Link;
begin
  if FLinkList_Selected.Count=0 then
    Exit;

  oLinkLine_Link :=FLinkList_Selected.Items[FLinkList_Selected.Count-1];

    FLinkList_All.SelectByProperty_ID (oLinkLine_Link.Property2_ID,
    FLinkList_Right, FLinkList_Selected);

  FLinkList_Right.SaveToDataset(mem_Right);

end;

//----------------------------------------------------------------------
procedure TdmLinkLine_Add.add_neighbour(aIndex: TNeighbourType);
//----------------------------------------------------------------------
var
  iID: Integer;
  oSrcDataset: TDataset;

  oTempLink: TLinkLine_Link;
  
  oLinkLine_Link: TLinkLine_Link;
  oLinkLine_Link_new: TLinkLine_Link;
//  sName: string;

begin
  case aIndex of
    ntLeft:  oSrcDataset:= mem_Left;
    ntRight: oSrcDataset:= mem_right;
  end;

  if oSrcDataset.RecordCount=0 then
    Exit;

  iID  :=oSrcDataset.FieldByName(FLD_ID).AsInteger;
 // sName:=oSrcDataset.FieldByName(FLD_Name).AsString;

  oLinkLine_Link := FLinkList_All.FindBYID(iID);

  oTempLink :=FLinkList_Selected.FindBYID(iID);
                             
//  oTempLink :=FindByID(aID);

  if Assigned(oTempLink) then
  begin
    ShowMessage('�������� ��� ��������: '+ oTempLink.Name);
    exit;
  end;


  Assert(not Assigned(oTempLink), 'not Assigned(FindByID(aID)');



//    Assert(not Assigned(FindByID(aID)), 'Value not assigned');



  case aIndex of
    ntLeft:  oLinkLine_Link_new := FLinkList_Selected.InsertItem(iID);
    ntRight: oLinkLine_Link_new := FLinkList_Selected.AddItem(iID);
  end;

  oLinkLine_Link_new.Assign(oLinkLine_Link);

  FLinkList_Selected.SaveToDataset(mem_Items);

  case aIndex of
    ntLeft:  UpdateLeftMemItems();
    ntRight: UpdateRightMemItems();
  end;
end;

//----------------------------------------------------------------------
procedure TdmLinkLine_Add.del_neighbour(aIndex: TNeighbourType);
//----------------------------------------------------------------------
begin
  case aIndex of
    ntLeft:    FLinkList_Selected.Delete(0);
    ntRight:   FLinkList_Selected.Delete(FLinkList_Selected.Count-1);
  end;

  UpdateLeftMemItems();
  UpdateRightMemItems();

  FLinkList_Selected.SaveToDataset(mem_Items);
end;

// ---------------------------------------------------------------
procedure TdmLinkLine_Add.Clear;
// ---------------------------------------------------------------
begin
  db_Clear(mem_Items);
  db_Clear(mem_Left);
  db_Clear(mem_Right);

  FLinkList_Left.Clear;
  FLinkList_Right.Clear;
  FLinkList_Selected.Clear;

end;


//----------------------------------------------------------------------
function TdmLinkLine_Add.Dlg_AddFirstInterval1(): boolean;
//----------------------------------------------------------------------
var
  rec: Tdlg_LinkLine_Get_First_Link_rec;
  oLinkLine_Link: TLinkLine_Link;

begin
//  Result:= False;

//  if not mem_Items.IsEmpty then
  //  Exit;

  Result:= Tdlg_LinkLine_Get_First_Link.ExecDlg(rec);

  if Result then
  begin
    oLinkLine_Link :=FLinkList_All.FindBYID(rec.ID);

    Assert(Assigned(oLinkLine_Link), 'Value not assigned');

    FLinkList_Selected.Clear;
    FLinkList_Selected.AddItem(rec.ID).Assign(oLinkLine_Link);
    FLinkList_Selected.SaveToDataset(mem_Items);

    UpdateLeftMemItems;
    UpdateRightMemItems;

  //  Result:= True;
  end;
end;


//-------------------------------------------------------------------
procedure TdmLinkLine_Add.InitItemsFromIDList(aIDList: TIDList);
//-------------------------------------------------------------------
var
  i: integer;
 // oLinkLine_Link_prior,
  oLinkLine_Link: TLinkLine_Link;
begin
//  OpenDB();
  FLinkList_Selected.Clear;


  for i:=0 to aIDList.Count-1 do
  begin
    oLinkLine_Link :=FLinkList_All.FindBYID(aIDList[i].ID);

    Assert(Assigned(oLinkLine_Link));

//    if i>0 then
//      oLinkLine_Link_prior:=FLinkList_All[i-1];

// TLinkLine_Link

    FLinkList_Selected.AddItem(aIDList[i].ID).Assign(oLinkLine_Link);
  end;

  FLinkList_Selected.SaveToDataset(mem_Items);

  UpdateLeftMemItems;
  UpdateRightMemItems;

 ////////// FLinkList_All.SaveToDataset(mem_All);

end;

// ---------------------------------------------------------------
procedure TdmLinkLine_Add.OpenDB;
// ---------------------------------------------------------------
begin
  dmOnega_DB_data.OpenQuery(qry_LInks_all,
      Format('SELECT id,name,property1_id,property2_id '+
//             ' FROM %s WHERE project_id=%d', [VIEW_Link, dmMain.ProjectID]));
             ' FROM %s WHERE project_id=%d', [view_Link_base, dmMain.ProjectID]));
//             ' FROM %s WHERE project_id=%d', [TBL_Link, dmMain.ProjectID]));

  FLinkList_All.LoadFromDataset(qry_LInks_all);

end;


begin
end.
