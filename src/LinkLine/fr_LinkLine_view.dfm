inherited frame_LinkLine_View: Tframe_LinkLine_View
  Left = 1451
  Top = 530
  Width = 589
  Height = 526
  Caption = 'frame_LinkLine_View'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 581
    Height = 56
  end
  inherited pn_Caption: TPanel
    Top = 81
    Width = 581
  end
  inherited pn_Main: TPanel
    Top = 98
    Width = 581
    Height = 175
    Anchors = [akLeft, akTop, akRight, akBottom]
    object PageControl1: TPageControl
      Left = 1
      Top = 6
      Width = 579
      Height = 163
      ActivePage = TabSheet_Main
      Align = alTop
      Style = tsFlatButtons
      TabOrder = 0
      OnChange = PageControl1Change
      object TabSheet_Main: TTabSheet
        Caption = #1057#1074#1086#1081#1089#1090#1074#1072
      end
      object TabSheet_Links: TTabSheet
        Caption = #1048#1085#1090#1077#1088#1074#1072#1083#1099
        ImageIndex = 1
      end
      object TabSheet_Reports: TTabSheet
        Caption = #1054#1090#1095#1077#1090#1099
        ImageIndex = 2
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 579
      Height = 5
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
    end
  end
  inherited MainActionList: TActionList
    Left = 28
    Top = 4
  end
  inherited ImageList1: TImageList
    Left = 60
    Top = 4
  end
  inherited PopupMenu1: TPopupMenu
    Left = 92
    Top = 4
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 198
    Top = 6
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      25
      0)
  end
  object ActionList1: TActionList
    Left = 318
    Top = 7
    object act_Calc: TAction
      Caption = #1056#1072#1089#1095#1077#1090
      OnExecute = act_CalcExecute
    end
    object act_Update: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      OnExecute = act_CalcExecute
    end
    object act_Report: TAction
      Caption = #1054#1090#1095#1077#1090
      OnExecute = act_CalcExecute
    end
  end
end
