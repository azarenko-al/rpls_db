unit u_LinkLine_Add_classes;

interface

uses
  Classes, DB, SysUtils, Dialogs,

  u_func
  ;

type
  TLinkLine_Link = class(TCollectionItem)
  public
    ID    : Integer;
    Name  : string;

    PROPERTY1_ID: Integer;
    PROPERTY2_ID: Integer;

    procedure Assign(aValue : TLinkLine_Link);
  end;


  TLinkLine_Link_List = class(TCollection)
  private
    function GetItems(Index: Integer): TLinkLine_Link;
  public
//    Selection: TList;

    constructor Create;
//    destructor Destroy; override;

    procedure Dlg_Show;


    function AddItem(aID: Integer): TLinkLine_Link;
    function InsertItem(aID: Integer): TLinkLine_Link;

    function FindByID(aID : Integer): TLinkLine_Link;

    procedure SelectByProperty_ID(aProperty_ID: Integer; aDestList:
        TLinkLine_Link_List; aExceptList: TLinkLine_Link_List);

    procedure LoadFromDataset(aDataset: TDataSet);
    procedure SaveToDataset(aDataset: TDataSet);


    property Items[Index: Integer]: TLinkLine_Link read GetItems; default;
  end;


implementation


constructor TLinkLine_Link_List.Create;
begin
  inherited Create (TLinkLine_Link);
end;

// ---------------------------------------------------------------
function TLinkLine_Link_List.AddItem(aID: Integer): TLinkLine_Link;
// ---------------------------------------------------------------
var
  oTempLink: TLinkLine_Link;
begin
  oTempLink :=FindByID(aID);

  Assert(not Assigned(FindByID(aID)), 'Value not assigned');

  Result := TLinkLine_Link(inherited Add);
  Result.ID := aID;
//  Result.Name := aName;

end;

// ---------------------------------------------------------------
function TLinkLine_Link_List.InsertItem(aID: Integer): TLinkLine_Link;
// ---------------------------------------------------------------
var
  oTempLink: TLinkLine_Link;
begin
  oTempLink :=FindByID(aID);

  Assert(not Assigned(FindByID(aID)), 'not Assigned(FindByID(aID)');

  Result := TLinkLine_Link(inherited Insert(0));
  Result.ID := aID;
 // Result.Name := aName;
end;


function TLinkLine_Link_List.GetItems(Index: Integer): TLinkLine_Link;
begin
  Result := TLinkLine_Link(inherited Items[Index]);
end;

// ---------------------------------------------------------------
procedure TLinkLine_Link_List.LoadFromDataset(aDataset: TDataSet);
// ---------------------------------------------------------------
var
  oTableInfo: TLinkLine_Link;
begin
  Clear;

  with aDataset do
    while not EOF do
    begin
      oTableInfo := AddItem(FieldByName('ID').AsInteger);

//      oTableInfo.ID   :=FieldByName('ID').AsInteger;
      oTableInfo.Name :=FieldByName('Name').AsString;

      oTableInfo.PROPERTY1_ID:=FieldByName('PROPERTY1_ID').AsInteger;
      oTableInfo.PROPERTY2_ID:=FieldByName('PROPERTY2_ID').AsInteger;

      Next;
    end;
end;

// ---------------------------------------------------------------
procedure TLinkLine_Link_List.SaveToDataset(aDataset: TDataSet);
// ---------------------------------------------------------------
var
  I: Integer;
  oTableInfo: TLinkLine_Link;
begin
  with aDataset do
  begin
    DisableControls;

    Open;

    while not EOF do
      Delete;

    for I := 0 to Count - 1 do
    begin
      oTableInfo := Items[i];

      Append;

      FieldByName('ID').AsInteger:=  oTableInfo.ID;
      FieldByName('Name').AsString:= oTableInfo.Name;

      FieldByName('PROPERTY1_ID').AsInteger:=oTableInfo.PROPERTY1_ID;
      FieldByName('PROPERTY2_ID').AsInteger:=oTableInfo.PROPERTY2_ID;

      Post;
    end;

    EnableControls;
  end;
end;

// ---------------------------------------------------------------
procedure TLinkLine_Link_List.SelectByProperty_ID(aProperty_ID: Integer;
    aDestList: TLinkLine_Link_List; aExceptList: TLinkLine_Link_List);
// ---------------------------------------------------------------
var
  I: Integer;
  oLinkLine_Link: TLinkLine_Link;
begin
  aDestList.Clear;

  for I := 0 to Count - 1 do
  begin
    oLinkLine_Link :=aExceptList.FindByID(Items[i].ID);

  //  if Assigned(oLinkLine_Link) then
  //    ShowMessage('');

    if not Assigned(oLinkLine_Link) then
      if (Items[i].PROPERTY1_ID = aProperty_ID) or
         (Items[i].PROPERTY2_ID = aProperty_ID)
      then
        aDestList.AddItem(Items[i].ID).Assign(Items[i]);
  end;
end;


function TLinkLine_Link_List.FindByID(aID : Integer): TLinkLine_Link;
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    if Items[i].ID = aID then
    begin
      Result := Items[i];
      Exit;
    end;

  Result :=nil;  
end;

// ---------------------------------------------------------------
procedure TLinkLine_Link_List.Dlg_Show;
// ---------------------------------------------------------------
var
  I: Integer;
  S: string;
begin
  for I := 0 to Count - 1 do
    s:=s + Format('id:%d; name:%s; p1_id:%d; p2_id:%d;' + CRLF  ,
             [Items[i].ID, Items[i].Name,
              Items[i].PROPERTY1_ID, Items[i].PROPERTY2_ID  ]);

  ShowMessage(s);
end;



procedure TLinkLine_Link.Assign(aValue : TLinkLine_Link);
begin
  Assert (Assigned (aValue));

  ID    :=aValue.ID;
  Name  :=aValue.Name;
  PROPERTY1_ID:=aValue.PROPERTY1_ID;
  PROPERTY2_ID:=aValue.PROPERTY2_ID;
end;


end.


