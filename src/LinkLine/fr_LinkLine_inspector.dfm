object frame_LinkLine_inspector: Tframe_LinkLine_inspector
  Left = 864
  Top = 334
  Width = 590
  Height = 408
  Caption = 'frame_LinkLine_inspector'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 120
  TextHeight = 16
  object pn_Buttons: TPanel
    Left = 0
    Top = 340
    Width = 582
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 0
    DesignSize = (
      582
      35)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 582
      Height = 2
      Align = alTop
      Shape = bsTopLine
    end
    object btn_Ok: TButton
      Left = 377
      Top = 6
      Width = 92
      Height = 28
      Action = act_Ok
      Anchors = [akTop, akRight]
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object btn_Cancel: TButton
      Left = 480
      Top = 6
      Width = 92
      Height = 28
      Action = act_Cancel
      Anchors = [akTop, akRight]
      Cancel = True
      TabOrder = 1
    end
  end
  object pn_Header: TPanel
    Left = 0
    Top = 0
    Width = 582
    Height = 60
    Align = alTop
    BevelOuter = bvLowered
    BorderWidth = 6
    Color = clInfoBk
    Constraints.MaxHeight = 60
    Constraints.MinHeight = 60
    TabOrder = 1
    Visible = False
  end
  object FormPlacement1: TFormPlacement
    Active = False
    IniFileName = 'Software\Onega\'
    Left = 36
    Top = 4
  end
  object ActionList1: TActionList
    Left = 72
    Top = 4
    object act_Ok: TAction
      Category = 'Main'
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    end
    object act_Cancel: TAction
      Category = 'Main'
      Caption = #1054#1090#1084#1077#1085#1072
    end
  end
  object qry_Temp: TADOQuery
    Parameters = <>
    Left = 156
    Top = 4
  end
end
