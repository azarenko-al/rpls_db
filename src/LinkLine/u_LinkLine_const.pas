unit u_LinkLine_const;

interface

uses
  u_const_db;

type
  TLinkLineNormRec = record
    Name_    : string;      // ������� ��� ������
    Max_Length_KM: integer;     // ����� ������, ��
    SESR     : double;      // ���TSESR.��, %
    KNG      : double;      // ������.��, %
    Comment  : string;      // ������������� ����������� �������� ��� �������� �����
    NamePr92 : integer;
    Param_C  : double;
  end;



const
  LINKLINE_NORM_ARR1: array [0..7] of TLinkLineNormRec =
    (
     //0
     (Name_:    '-���������-';
      Max_Length_KM:  600;
      SESR:     0.025;
      KNG:     0.05;
      Comment: '��������������� R ���  R >= 600 �� '+
               '���������� �� ����� ��� 200 < R < 600 ��';
      NamePr92: 2;
      Param_C: 0.075
     ),

     //1
     (Name_:    '������������� ������� (�� 12.500 km)';
      Max_Length_KM:  12500;
      SESR:     0.07;
      KNG:     1.5;
      Comment: 'R >= 2500 �����.�� ��������������� RTSESR.��=0.05+0.02* R / 12500';
      NamePr92: 1;
      Param_C: 0.2
     ),

     //2
     (Name_:    '������������� ���� (�� 2500 km)';
      Max_Length_KM:  2500;
      SESR:     0.054;
      KNG:     0.3;
      Comment: '��������������� R ���  R >= 50 ��';
      NamePr92: 1;
      Param_C: 0.04
     ),

     //3
     (Name_:    '������������� ���� (�� 600 km)';
      Max_Length_KM:  600;
      SESR:     0.025;
      KNG:     0.05;
      Comment: '��������������� R ���  R >= 600 �� '+
               '���������� �� ����� ��� 200 < R < 600 ��';
      NamePr92: 2;
      Param_C: 0.075
     ),

     //4
     (Name_:    '������������� ���� (�� 200 km)';
      Max_Length_KM:  200;
      SESR:     0.025;
      KNG:     0.05;
      Comment: '��������������� R ���  50 < R < 200 ��';
      NamePr92: 2;
      Param_C: 0.025
     ),

     //5
     (Name_:    '������������� ���� (�� 50 km)';
      Max_Length_KM:  50;
      SESR:     0.006;
      KNG:     0.0125;
      Comment: '���������� �� ����� ��� R < 50 ��';
      NamePr92: 2;
      Param_C: 0.00625
     ),

     //6
     (Name_:    '������� ���� (�� 100 km)';
      Max_Length_KM:  100;
      SESR:     0.015;
      KNG:     0.05;
      Comment: '���������� �� ����� ��� R < 100 ��';
      NamePr92: 3;
      Param_C: 0.075
     ),

     //7
     (Name_:    '���� �������';
      Max_Length_KM:  50;
      SESR:     0.015;
      KNG:     0.05;
      Comment: '���������� �� �����';
      NamePr92: 4;
      Param_C: 0.15
     )

    );

implementation



end.

(*
type

  TLinkCSTTypeRec = record
    Bitrate_Mbps : double;   //speed
    Param_A_ESR  : double;
    Param_A_BBER : double;
  end;
*)

(*
  LINK_CST_TYPE_ARRAY: array [0..4] of TLinkCSTTypeRec =
    (
     (Bitrate_Mbps: 0.064;   Param_A_ESR:  0.08;    Param_A_BBER: 0
     ),
     (Bitrate_Mbps: 2.048;    Param_A_ESR:  0.04;    Param_A_BBER: 0.0003
     ),
     (Bitrate_Mbps: 8.448;    Param_A_ESR:  0.05;   Param_A_BBER: 0.0002
     ),
     (Bitrate_Mbps: 34.368;   Param_A_ESR:  0.075;   Param_A_BBER: 0.0002
     ),
     (Bitrate_Mbps: 139.264;  Param_A_ESR:  0.16;   Param_A_BBER: 0.0002
     )
    );

*)

