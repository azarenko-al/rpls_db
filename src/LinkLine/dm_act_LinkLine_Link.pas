unit dm_act_LinkLine_Link;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Dialogs,  Menus,
  Controls, Forms, ActnList,

  I_Object,
  
  dm_act_Base,


  u_GEO,
  u_classes,
  u_types,

  u_func,
  u_dlg,

  u_const_str,
  u_const_db,

  fr_Link_view,

  dm_LinkLine,
  dm_Link,
  dm_LinkLine_link
  
  ;

type
  TdmAct_LinkLine_Link = class(TdmAct_Base)
    procedure DataModuleCreate(Sender: TObject);

  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm  (aOwnerForm: TForm): TForm; override;

    procedure DoAction (Sender: TObject);

// TODO: ItemDel
//  function ItemDel (aID: integer): boolean; override;

    procedure ShowOnMap (aID: integer);
  public

    class procedure Init;
  end;


var
  dmAct_LinkLine_Link: TdmAct_LinkLine_Link;

//==================================================================
implementation {$R *.dfm}
//==================================================================



//--------------------------------------------------------------------
class procedure TdmAct_LinkLine_Link.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_LinkLine_Link) then
    dmAct_LinkLine_Link:=TdmAct_LinkLine_Link.Create(Application);
end;

//--------------------------------------------------------------------
procedure TdmAct_LinkLine_Link.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_LINKLINE_LINK;

//  act_Move.Caption:=STR_MOVE;

  SetActionsExecuteProc ([act_Object_ShowOnMap], DoAction);
end;

//--------------------------------------------------------------------
procedure TdmAct_LinkLine_Link.ShowOnMap (aID: integer);
//--------------------------------------------------------------------
var iLinkID: integer;
  BLVector: TBLVector;
begin
  iLinkID:= dmLinkLine_link.GetLinkID (aID);

  if dmLink.GetBLVector (iLinkID, BLVector) then
    ShowVectorOnMap (BLVector, aID);

//  ShowRectOnMap (dmLink.GetBLRect (iLinkID), aID);
end;

//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_LinkLine_Link.DoAction(Sender: TObject);
begin
  if Sender=act_Object_ShowOnMap then
    ShowOnMap (FFocusedID);
end;

//--------------------------------------------------------------------
function TdmAct_LinkLine_Link.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_Link_View.Create(aOwnerForm);
  (Result as Tframe_Link_View).ViewMode:=vmLinkLine_Link;
end;

//--------------------------------------------------------------------
procedure TdmAct_LinkLine_Link.GetPopupMenu;
//--------------------------------------------------------------------
begin
  case aMenuType of
    mtItem:   begin
                AddMenuItem (aPopupMenu, act_Object_ShowOnMap);
               // AddMenuItem (aPopupMenu, nil);
               // AddMenuItem (aPopupMenu, act_Del_);
              end;
//    mtList:
 //               AddMenuItem (aPopupMenu, act_Del_list);
  end;
end;



begin
 
end.





// TODO: ItemDel
////--------------------------------------------------------------------
//function TdmAct_LinkLine_Link.ItemDel (aID: integer): boolean;
////--------------------------------------------------------------------
//begin
//Result:=dmLinkLine_link.Del (aID);
//end;

