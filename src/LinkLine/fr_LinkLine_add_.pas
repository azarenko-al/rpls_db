unit fr_LinkLine_add_;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, StdCtrls, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView,
  cxClasses, cxControls, cxGridCustomView, cxGrid, ExtCtrls,

  dm_LinkLine_Add
  ;

type
  Tframe_LinkLine_add_ = class(TForm)
    Actions: TActionList;
    act_Select_first_link: TAction;
    act_add_right_neighbour: TAction;
    act_del_right_neighbour: TAction;
    act_add_left_neighbour: TAction;
    act_del_left_neighbour: TAction;
    Panel1: TPanel;
    cxGrid_L: TcxGrid;
    cx_Left: TcxGridDBBandedTableView;
    cx_LeftNAME: TcxGridDBBandedColumn;
    cx_LeftID: TcxGridDBBandedColumn;
    col_property1_id_: TcxGridDBBandedColumn;
    col_property2_id_: TcxGridDBBandedColumn;
    cx_LeftDBBandedColumn3: TcxGridDBBandedColumn;
    cxGrid_Left: TcxGridLevel;
    pn_Buttons_Left: TPanel;
    Button1: TButton;
    Button2: TButton;
    cxGrid_C: TcxGrid;
    cx_LInks: TcxGridDBBandedTableView;
    cx_LInksname: TcxGridDBBandedColumn;
    cx_LInksid: TcxGridDBBandedColumn;
    cx_main_property1_id: TcxGridDBBandedColumn;
    cx_main_property2_id: TcxGridDBBandedColumn;
    cx_LInksDBBandedColumn1: TcxGridDBBandedColumn;
    cxGrid_CLevel1: TcxGridLevel;
    Panel4: TPanel;
    Button7: TButton;
    Button8: TButton;
    pn_Buttons_right: TPanel;
    Button5: TButton;
    Button6: TButton;
    cxGrid_R: TcxGrid;
    cx_Right: TcxGridDBBandedTableView;
    cx_RightNAME: TcxGridDBBandedColumn;
    cx_RightID: TcxGridDBBandedColumn;
    col_property1_id__: TcxGridDBBandedColumn;
    col_property2_id__: TcxGridDBBandedColumn;
    cx_RightDBBandedColumn1: TcxGridDBBandedColumn;
    cxGrid_Right: TcxGridLevel;
    procedure ActionsUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Panel1Click(Sender: TObject);
    procedure _ActionMenu(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

(*
var
  frame_LinkLine_add_: Tframe_LinkLine_add_;
*)

implementation

{$R *.dfm}

// ---------------------------------------------------------------
procedure Tframe_LinkLine_add_.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  cxGrid_C.Align:=alClient;

  Panel1.Align:=alClient;


 // TdmLinkLine_Add.Init;
  dmLinkLine_Add.OpenDB;

  cx_LInks.DataController.DataSource := dmLinkLine_Add.ds_Items;
  cx_Left.DataController.DataSource  := dmLinkLine_Add.ds_left;
  cx_Right.DataController.DataSource := dmLinkLine_Add.ds_right;

end;


procedure Tframe_LinkLine_add_.FormDestroy(Sender: TObject);
begin
  dmLinkLine_Add_Free();
end;


//------------------------------------------------------
procedure Tframe_LinkLine_add_.ActionsUpdate(Action: TBasicAction; var Handled:
    Boolean);
//------------------------------------------------------
begin
  act_del_left_neighbour.Enabled:=  cx_LInks.DataController.RecordCount>1;
  act_del_right_neighbour.Enabled:= cx_LInks.DataController.RecordCount>1;

  act_add_left_neighbour.Enabled:=  cx_Left.DataController.RecordCount>0;
  act_add_right_neighbour.Enabled:= cx_Right.DataController.RecordCount>0;

end;

procedure Tframe_LinkLine_add_.FormResize(Sender: TObject);
var iW: integer;
begin
  iW:=Trunc((Width- 30*2)/3);

  cxGrid_L.Width:= iW;
  cxGrid_R.Width:= iW;
end;



procedure Tframe_LinkLine_add_.Panel1Click(Sender: TObject);
begin

end;


//-------------------------------------------------------------------
procedure Tframe_LinkLine_add_._ActionMenu(Sender: TObject);
//-------------------------------------------------------------------
begin

  //------------------------------------------------------
  if Sender=act_Select_first_link then
  //------------------------------------------------------
  begin
    if not dmLinkLine_Add.Dlg_AddFirstInterval1() then
      Exit;

  end else

  if sender=act_add_right_neighbour then
    dmLinkLine_Add.add_neighbour(ntRight) else

  if sender=act_del_right_neighbour then
    dmLinkLine_Add.del_neighbour(ntRight) else

  if sender=act_add_left_neighbour then
    dmLinkLine_Add.add_neighbour(ntLeft) else

  if sender=act_del_left_neighbour then
    dmLinkLine_Add.del_neighbour(ntLeft);

end;


end.


 {

procedure Tdlg_LinkLine_add.cx_LeftDblClick(Sender: TObject);
begin
  act_add_left_neighbour.Execute;
end;


procedure Tdlg_LinkLine_add.cx_RightDblClick(Sender: TObject);
begin
  act_add_right_neighbour.Execute;
end;

