unit fr_LinkLine_Links;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  AppEvnts, Menus, DB, ADODB, ActnList, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, cxVGrid, cxDBVGrid,
  cxSplitter,


  dm_User_Security,

  dm_Main,

  u_Storage,

  dm_Onega_DB_data,

  u_Log,

  u_vars,

//  f_Custom,

  I_LinkLine,

  dm_LinkLine,
  dm_LinkLine_Calc,

  d_LinkLine_Add,

  u_Func_arrays,
  u_func,
  u_db,
    
  u_cx,
  u_cx_VGrid,
  u_dlg,

  u_link_const,

  u_const,
  u_const_DB,
  u_const_str,

  cxInplaceContainer, ExtCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxEdit, cxTextEdit, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxDBData, cxCurrencyEdit

  ;


type
  Tframe_LinkLine_Links = class(Tform)
    MainActionList: TActionList;
    Panel1: TPanel;
    act_Calc: TAction;
    act_Summary: TAction;
    act_Update: TAction;
    act_Report: TAction;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    col__id: TcxGridDBColumn;
    col__name: TcxGridDBColumn;
    col__status: TcxGridDBColumn;
    col__Power: TcxGridDBColumn;
    col__sesr_required: TcxGridDBColumn;
    col__sesr_calc: TcxGridDBColumn;
    col__kng_req_: TcxGridDBColumn;
    col__kng_calc: TcxGridDBColumn;
    col__length: TcxGridDBColumn;
    col__fade_reserve: TcxGridDBColumn;
    cxSplitter1: TcxSplitter;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxCategoryRow1: TcxCategoryRow;
    row_SESR_required: TcxDBEditorRow;
    row_sesr: TcxDBEditorRow;
    row_kng_required: TcxDBEditorRow;
    row_Kng: TcxDBEditorRow;
    row_status_str: TcxDBEditorRow;
    col_length: TcxDBEditorRow;
    qry_LinkLine: TADOQuery;
    ds_LinkLine: TDataSource;
    ds_Links: TDataSource;
    qry_Links: TADOQuery;
    col_band: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    Export1: TMenuItem;
    act_Excel: TAction;
    col_GST_len: TcxGridDBColumn;
    ADOStoredProc1: TADOStoredProc;
    procedure act_AddExecute(Sender: TObject);
//    procedure ApplicationEvents1ActionExecute(Action: TBasicAction; var Handled:
//        Boolean);
//    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  //  procedure Button1Click(Sender: TObject);

  private
    FID: integer;

    FRegPath: string;

  public
    procedure Open(aID: integer);
    procedure SetReadOnly(Value: Boolean);
    procedure View (aID: integer);
  end;


//==================================================================
implementation {$R *.DFM}

uses
  dm_act_LinkLine;



const
  REGISTRY_COMMON_FORMS = 'Software\Onega\Common\Forms\';


//const
 // VER = '_1.0';


//--------------------------------------------------------------------
procedure Tframe_LinkLine_Links.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

//  FRegPath := REGISTRY_COMMON_FORMS + ClassName +'\';

  FRegPath := vars_Form_GetRegPath(ClassName);



 // dxDBGrid1.Align:=alClient;

  cxGrid1.Align:=alClient;


  // -------------------------------------------------------------------
//  cxGrid1DBTableView1.RestoreFromRegistry(FRegPath + cxGrid1DBTableView1.Name );

  col__Name.Caption        :=STR_NAME;
  col__sesr_calc.Caption        :='SESR ���� [%]';
  //col__Sesr_req.Caption    :='SESR ���� [%]';
  col__sesr_required.Caption   :='SESR ���� [%]'; //'SESR ���� [%]'

  col__KNG_calc.Caption         :='��� ���� [%]';
  ///col__KNG_req.Caption     :='��� ���� [%]';

  col__Length.Caption      :='����� [m]';
  col__Power.Caption       :='������� [dBm]';
  col__status.Caption      :='�����������';
  col__Fade_Reserve.Caption:='����� �� ��������� [dB]';

  col_band.Caption:='��������';



  // -------------------------------------------------------------------

  act_Excel.Caption:=STR_GRID_EXPORT;


//  Assert(Assigned(dmLinkLine_view));



{  dxDBInspector1.DataSource:=ds_LinkLine;
  dxDBGrid1.DataSource:=ds_Links;
}

  // -------------------------------------------------------------------
{
  row_SESR.Caption        :='SESR ���� [%]' ;
  row_KNG.Caption         :='��� ���� [%]';
  row_Len.Caption         :='����� [m]';

  col_Name.Caption        :=STR_NAME;
  col_SESR.Caption        :='SESR ���� [%]';
  col_KNG.Caption         :='��� ���� [%]';
  col_Length.Caption      :='����� [m]';
  col_Power.Caption       :='������� [dBm]';
  col_status.Caption      :='�����������';
  col_Fade_Reserve.Caption:='����� �� ��������� [dB]';
  col_Sesr_req.Caption    :='SESR ���� [%]';
  col_KNG_req.Caption     :='��� ���� [%]';

  col_status.FieldName      :='�����������';
}

{  dxDBGrid1.LoadFromRegistry (FRegPath+ dxDBGrid1.Name);
  dx_CheckRegistry(dxDBGrid1, FRegPath+ dxDBGrid1.Name);
}


//zzzzzzzzzzzzzz

  cx_InitDBVerticalGrid (cxDBVerticalGrid1);

  col_GST_len.DataBinding.FieldName:=FLD_GST_LENGTH;


//  TdmLinkLine_view.Init;

(*  cxGrid1DBTableView1.DataController.DataSource := dmLinkLine_view.ds_Links;
  cxDBVerticalGrid1.DataController.DataSource := dmLinkLine_view.ds_LinkLine;
*)
  cxGrid1DBTableView1.DataController.DataSource := ds_Links;
  cxDBVerticalGrid1.DataController.DataSource := ds_LinkLine;


  g_Storage.RestoreFromRegistry(cxGrid1DBTableView1, className);



  cx_SetColumnCaptions(cxGrid1DBTableView1,
     [
      FLD_NAME,      '��������',
    //  SArr(FLD_sesr,      'SESR ���� [%]'),

      FLD_status_str, '�����������',
      FLD_rx_Level_dBm,  '������� [dBm]',

      FLD_GST_LENGTH,  '����� ���� [km]',


      FLD_sesr,       'SESR ���� [%]',
      FLD_sesr_norm,  'SESR ���� [%]',

      FLD_kng,        '��� ���� [%]',
      FLD_kng_norm,   '��� ���� [%]',

      FLD_length,     '����� [m]',
      FLD_FADE_MARGIN_DB, '����� �� ��������� [dB]'
     ]);

  cxGrid_RemoveUnusedColumns_DBTableView(cxGrid1DBTableView1);

end;


//----------------------------------------------------------
procedure Tframe_LinkLine_Links.FormDestroy(Sender: TObject);
//----------------------------------------------------------
begin
//  cxGrid1DBTableView1.StoreToRegistry(FRegPath + cxGrid1DBTableView1.Name );

  g_Storage.StoreToRegistry(cxGrid1DBTableView1, className);


  inherited;
end;

//--------------------------------------------------------------------
procedure Tframe_LinkLine_Links.View (aID: integer);
//--------------------------------------------------------------------
begin
  FID:=aID;

//  dmLinkLine_view.Open(aID);
  Open(aID);

end;



//--------------------------------------------------------------------
procedure Tframe_LinkLine_Links.act_AddExecute(Sender: TObject);
//--------------------------------------------------------------------
begin
  if Sender=act_Excel then
    cx_ExportToExcel(cxGrid1) else


  //-------------------------------------------------------------------
  if Sender=act_Update then begin
  //-------------------------------------------------------------------
    Tdlg_LinkLine_Add.UpdateDlg(FID);

    act_Summary.Execute;
  end else

  //-------------------------------------------------------------------
  if Sender=act_Calc then begin
  //-------------------------------------------------------------------
//

{
    dmLinkLine_Calc.IsReFreshCalcs:=ConfirmDlg('���������� ��� ��������� ������?');
//      dmLinkLine_Calc.IsReFreshCalcs:=True;

    dmLinkLine_Calc.LinkLineID:= FID;
    dmLinkLine_Calc.ExecuteDlg ('������ �����');
}
    dmLinkLine_Calc.Dlg_Exec(FID);

    View (FID);
  end else

  //-------------------------------------------------------------------
  if Sender=act_Report then begin
  //-------------------------------------------------------------------
    dmAct_LinkLine.MakeReport(FID);
  end else

  //-------------------------------------------------------------------
  if Sender=act_Summary then begin
  //-------------------------------------------------------------------
    dmLinkLine_Calc.UpdateSummary (FID);
    View (FID);
  end;

end;


// ---------------------------------------------------------------
procedure Tframe_LinkLine_Links.SetReadOnly(Value: Boolean);
// ---------------------------------------------------------------
begin

  qry_LinkLine.LockType := IIF(Value, ltReadOnly, ltBatchOptimistic);
  qry_Links.LockType    := IIF(Value, ltReadOnly, ltBatchOptimistic);

  SetFormActionsEnabled(Self, not Value);

  cxGrid1DBTableView1.OptionsData.Editing := not Value;

end;


// ---------------------------------------------------------------
procedure Tframe_LinkLine_Links.Open(aID: integer);
// ---------------------------------------------------------------

begin
  FID:=aID;

(*
  qry_LinkLine.LockType := IIF(dmUser_Security.ProjectIsReadOnly, ltReadOnly, ltBatchOptimistic);
  qry_Links.LockType    := IIF(dmUser_Security.ProjectIsReadOnly, ltReadOnly, ltBatchOptimistic);
  SetFormActionsEnabled(Self, not dmUser_Security.ProjectIsReadOnly);


  cxGrid1DBTableView1.OptionsData.Editing := not dmUser_Security.ProjectIsReadOnly;

*)


  dmOnega_DB_data.OpenQuery( qry_LinkLine,
               'SELECT * FROM ' + view_LINKLINE + ' WHERE (id=:id)',
               [FLD_ID, aID]);

  if qry_LinkLine.IsEmpty then
  begin
    g_Log.Error('procedure Tframe_LinkLine_Links.Open(aID: integer); qry_LinkLine.IsEmpty');
    Exit;
  end;
    

// dmOnega_DB_data.OpenStoredProc(ADOStoredProc1,


  dmOnega_DB_data.OpenQuery(qry_Links,
               'SELECT id, name, sesr, sesr_norm, rx_Level_dBm, '+
               '       kng, kng_norm, length, fade_margin_db,   '+
               '       band,  '+
               '  GST_length, '+
               '       status_str  '+
               ' FROM ' + view_LinkLine_links +
               ' WHERE (linkline_id=:id)',
               [FLD_ID, aID]);


(*
  db_SetFieldCaptions(qry_Links,
     [
      SArr(FLD_NAME,      '��������'),
    //  SArr(FLD_sesr,      'SESR ���� [%]'),

      SArr(FLD_status_str, '�����������'),
      SArr(FLD_rx_Level_dBm,  '������� [dBm]'),

      SArr(FLD_sesr,       'SESR ���� [%]'),
      SArr(FLD_sesr_norm,  'SESR ���� [%]'),

      SArr(FLD_kng,        '��� ���� [%]'),
      SArr(FLD_kng_norm,   '��� ���� [%]'),

      SArr(FLD_length,     '����� [m]'),
      SArr(FLD_FADE_MARGIN_DB, '����� �� ��������� [dB]')
     ]);
*)


 // db_ViewDataSet(qry_LinkLine);

 // db_SetFieldCaption (qry_LinkLine, FLD_kng_norm, '��� ���� [%]');



 // db_ViewDataSet(qry_LinkLine);

(*
  cxGrid1DBTableView1.RestoreFromRegistry(FRegPath + cxGrid1DBTableView1.Name + VER);

  col__Name.Caption        :=STR_NAME;
  col__sesr_calc.Caption    :='SESR ���� [%]';
  //col__Sesr_req.Caption    :='SESR ���� [%]';
  col__sesr_required.Caption   :='SESR ���� [%]'; //'SESR ���� [%]'

  col__KNG_calc.Caption         :='��� ���� [%]';
  ///col__KNG_req.Caption     :='��� ���� [%]';

  col__Length.Caption      :='����� [m]';
  col__Power.Caption       :='������� [dBm]';
  col__status.Caption      :='�����������';
  col__Fade_Reserve.Caption:='����� �� ��������� [dB]';

*)

//  aQuery.Connection:=dmMain.ADOConnection;
{
  db_Ope nQuery (aQuery,
               'SELECT * FROM '+ TBL_LINKLINE + ' WHERE (id=:id)',
               [db_Par(FLD_ID, aID)]);}
end;


end.