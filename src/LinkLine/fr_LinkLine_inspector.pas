unit fr_LinkLine_inspector;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rxPlacemnt, ActnList, StdCtrls, ExtCtrls, Db, ADODB,

  dm_Onega_DB_data,

  dm_Main,

  fr_DBInspector_Container,

  dm_Link,
  dm_LinkLine,
  dm_LinkEnd,
  dm_Link_calc_SESR,

  u_LinkLine_const,
  u_Link_const,
                    
  u_GEO,
  u_radio,
  u_func,
  u_db,
  u_dlg,

  u_const_db,
  u_types
  ;


type
  Tframe_LinkLine_inspector = class(TForm)
    FormPlacement1: TFormPlacement;
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    ActionList1: TActionList;
    act_Ok: TAction;
    act_Cancel: TAction;
    pn_Header: TPanel;
    qry_Temp: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
//    procedure act_OkExecute(Sender: TObject);

  private
    FID: integer;
    Ffrm_DBInspector_Container: Tframe_DBInspector_Container;

    procedure DoOnFieldChanged (Sender: TObject; aFieldName: string; aNewValue: Variant);
  public
    procedure View (aID: integer);
    procedure Post;

    class function ExecDlg (aID: integer): boolean;
    procedure SetReadOnly(Value: Boolean);

 //   class function CreateChildForm ( aDest: TWinControl): Tframe_LinkLine_inspector;

  end;

 // function FoundInSpeedRow(aSpeed: double) :boolean;


//====================================================================
implementation {$R *.DFM}
//====================================================================


//--------------------------------------------------------------------
class function Tframe_LinkLine_inspector.ExecDlg (aID: integer): boolean;
//--------------------------------------------------------------------
begin
  with Tframe_LinkLine_inspector.Create(Application) do
  try
    FormPlacement1.Active:=True;
    FormPlacement1.RestoreFormPlacement;

    pn_Buttons.Visible:=True;

    View (aID);
    //Ffrm_DBInspector_Container.Options.AutoCommit:=False;
    pn_Buttons.Visible:=True;

    Result:=(ShowModal=mrOK);
//    if Result then
//      Ffrm_DBInspector_Container.Post;

  finally
    Free;
  end;

end;


//--------------------------------------------------------------------
procedure Tframe_LinkLine_inspector.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  CreateChildForm (Tframe_DBInspector_Container, Ffrm_DBInspector_Container, Self);
  Ffrm_DBInspector_Container.PrepareViewForObject (OBJ_LINKLINE);
//  Ffrm_DBInspector_Container.OnButtonClick:=DoOnButtonFieldClick;
  Ffrm_DBInspector_Container.OnFieldChanged:=DoOnFieldChanged;

//  db_SetADOConn (qry_Temp, dmMain.ADOConnection);

  pn_Buttons.Visible:=False;
end;



procedure Tframe_LinkLine_inspector.FormDestroy(Sender: TObject);
begin
  inherited;
end;

//--------------------------------------------------------------------
procedure Tframe_LinkLine_inspector.Post;
//--------------------------------------------------------------------
begin
  try
  Ffrm_DBInspector_Container.Post;
  except end;
end;



//--------------------------------------------------------------------
procedure Tframe_LinkLine_inspector.View (aID: integer);
//--------------------------------------------------------------------
begin
  FID:=aID;

  Ffrm_DBInspector_Container.PrepareViewForObject (OBJ_LINKLINE);
  Ffrm_DBInspector_Container.View (aID);
end;


//-------------------------------------------------------------------
procedure Tframe_LinkLine_inspector.DoOnFieldChanged (Sender: TObject; aFieldName: string;
                                                aNewValue: Variant);
//-------------------------------------------------------------------
var i,id,ind: integer;
  iBitRate_Mbps: Integer;
  dLength_KM, dOldSpeed: double;
  dEsrNorm, dEsrReq, dBberNorm, dBberReq: double;
  rec: TCalcEsrBberParamRec;
  iValue: Integer;

begin
  with Ffrm_DBInspector_Container do
  begin


  //------------------------------------------------------
  if Eq(aFieldName, FLD_GST_TYPE) then
  //------------------------------------------------------
  begin
    ind:=aNewValue;

    SetFieldValue (FLD_GST_LENGTH,   LINKLINE_NORM_ARR1[ind].Max_Length_km );
    SetFieldValue (FLD_GST_SESR,     LINKLINE_NORM_ARR1[ind].SESR );
    SetFieldValue (FLD_GST_KNG,      LINKLINE_NORM_ARR1[ind].KNG );

    dLength_KM     :=GetFloatFieldValue (FLD_Length);
    iBitRate_Mbps :=GetIntFieldValue   (FLD_BitRate_Mbps);

//    dLength_KM:=dmLinkLine.GetLengthKm(FID);

//    iBitRate_Mbps:=gl_DB.GetIntFieldValueByID(TBL_LINKLINE, FLD_BitRate_Mbps, FID);


//    dBitRate_Mbps:=gl_DB.GetFieldValueByID(TBL_LINKLINE, FLD_SPEED, FID, '');

  ////  if iBitRate_Mbps = 0 then
   ////   iBitRate_Mbps:=dmLinkLine.GetMinBitRate_Mbps(FID);

    if (dLength_KM > 0) then
    begin
      rec.ID       :=FID;
      rec.ObjName  :=OBJ_LINKLINE;
      rec.Length_KM:=dLength_KM;
      rec.GST_Type  :=ind;
      rec.LinkLineSpeed_Mbps:=iBitRate_Mbps;


      dmLink_calc_SESR.CalcEsrBber11(rec

(*      ,
                        FID, OBJ_LINKLINE, dLength_KM, ind,
                         dEsrNorm, dEsrReq, dBberNorm, dBberReq,
                         iBitRate_Mbps
*)
                         );

      SetFieldValue(FLD_ESR_NORM,      rec.Output.Esr_Norm);
      SetFieldValue(FLD_BBER_NORM,     rec.Output.Bber_Norm);
      SetFieldValue(FLD_ESR_REQUIRED,  rec.Output.Esr_required);
      SetFieldValue(FLD_BBER_REQUIRED, rec.Output.Bber_Required);
    end;

  end;

  //---------------------------------------
  if Eq(aFieldName, FLD_GST_LENGTH) or
     Eq(aFieldName, FLD_GST_SESR) or
     Eq(aFieldName, FLD_GST_KNG) or
     Eq(aFieldName, FLD_ESR_NORM) or
     Eq(aFieldName, FLD_BBER_NORM)
  then
    SetFieldValue (FLD_GST_TYPE, 0);


  end; // with
end;



procedure Tframe_LinkLine_inspector.SetReadOnly(Value: Boolean);
begin
  Ffrm_DBInspector_Container.setReadOnly(Value);


end;

end.





(*


procedure Tframe_LinkLine_inspector.act_OkExecute(Sender: TObject);
begin
//  if Sender=act_Cancel then

//.
end;


//--------------------------------------------------------------------
function FoundInSpeedRow(aSpeed: double): boolean;
//--------------------------------------------------------------------
const
  SpeedRow: array[0..10] of double = (2, 4, 8, 14, 16, 17, 32, 34, 64, 80, 155);
var
  i: integer;
begin
  Result := False;
  for i:=0 to 10 do
  begin
    if aSpeed = SpeedRow[i] then
      Result := True;
  end;
end;*)


(*
   // -------------------------------------------------------------------
    if Eq(aFieldName, FLD_GST_TYPE) then
   // -------------------------------------------------------------------
    begin
      ind:=aNewValue;

      if ind = 0 then
        Exit;

      dLengthKM:=GetFloatFieldValue (FLD_Length)/1000;


      if Tdlg_Link_Get_Network_Type.ExecDlg(ID, dLengthKM, ind, rNetwork_Type_rec) then
      begin

        SetFieldValue(FLD_GST_TYPE,      rNetwork_Type_rec.Index );

        SetFieldValue(FLD_GST_LENGTH,    rNetwork_Type_rec.Length_km );
        SetFieldValue(FLD_GST_SESR,      rNetwork_Type_rec.SESR );
        SetFieldValue(FLD_GST_KNG,       rNetwork_Type_rec.KNG );

        SetFieldValue(FLD_ESR_NORM,      rNetwork_Type_rec.Esr_Norm);
        SetFieldValue(FLD_BBER_NORM,     rNetwork_Type_rec.Bber_Norm);
        SetFieldValue(FLD_ESR_REQUIRED,  rNetwork_Type_rec.Esr_Req);
        SetFieldValue(FLD_BBER_REQUIRED, rNetwork_Type_rec.Bber_Req);
      end;
   end;
*)

(*  //---------------------------------------
  if Eq(aFieldName, FLD_BitRate_Mbps) then
  //---------------------------------------
  begin
    dOldSpeed:=gl_DB.GetFieldValueByID(TBL_LINKLINE, FLD_BitRate_Mbps, FID, '');
    iBitRate_Mbps:=GetIntFieldValue(FLD_BitRate_Mbps);

    if (iBitRate_Mbps <> 0) then
    begin
      iValue := dmOnega_DB_data.LinkLine_GetMinBitRate_Mbps(FID);


      if FoundInSpeedRow(iBitRate_Mbps) then
        if iBitRate_Mbps <= iValue then
//          dmLinkLine.GetMinBitRate_Mbps(FID) then

         // SetFieldValue(FLD_SPEED_E1, SpeedToE1(dBitRate_Mbps))

        else
        begin
          ErrorDlg('�������� �������� ����� �� ����� ���� ������ '+
                   '�������� �������� �������� � ��� ����� ����������');
          SetFieldValue(FLD_BitRate_Mbps, dOldSpeed);
        end
      else
      begin
        ErrorDlg('�������� �������� ����� ����� ��������: '+
                 '2, 4, 8, 14, 16, 17, 32, 34, 64, 80, 155');

        SetFieldValue(FLD_BitRate_Mbps, dOldSpeed);
      end;
    end;
  end;
*)


  //SetSpeed();
  //SetEsrBber();
//  SetKNGYear();