unit dm_LinkLine;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Db, ADODB, dxmdaset,
  Variants,

  dm_Onega_DB_data,

  dm_Main,
  dm_Object_base,

  dm_Link_Calc_SESR,

  u_types,

  u_const_str,
  u_const_db,
  u_LinkLine_const,
  u_link_const,

  u_classes,
  u_db,
  u_func,
  u_Geo,
  u_MapX
 // u_Mitab

  ;


type
  //-------------------------------------------------------------------
  TdmLinkLineAddRec = record
  //-------------------------------------------------------------------
    Name:  string;
  end;



  //-------------------------------------------------------------------
  TdmLinkLine = class(TdmObject_base)
    DataSource111: TDataSource;
    qry_all_neighbors_links111: TADOQuery;
    qry_Links: TADOQuery;
    DataSource211: TDataSource;
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    function GetLengthKm(aID: integer): double;

  public
    function Add (aRec: TdmLinkLineAddRec; aIDList: TIDList): integer;

    function  GetBLRect (aID: integer): TBLRect;

    procedure GetLinkListByLinkLineID(aID: integer; aIDList: TIDList);

  end;

function dmLinkLine: TdmLinkLine;


//====================================================================
implementation  {$R *.dfm}
//====================================================================

var
  FdmLinkLine: TdmLinkLine;


// ---------------------------------------------------------------
function dmLinkLine: TdmLinkLine;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLinkLine) then
      FdmLinkLine := TdmLinkLine.Create(Application);

  Result := FdmLinkLine;
end;


//-------------------------------------------------------------------
procedure TdmLinkLine.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  TableName:=TBL_LINKLINE;
  ObjectName:=OBJ_LINKLINE;
end;


//-------------------------------------------------------------------
function TdmLinkLine.GetLengthKm(aID: integer): double;
//-------------------------------------------------------------------
begin
//  Result:=GetDoubleFieldValue(aID, FLD_LENGTH) / 1000;
  Result:=GetDoubleFieldValue(aID, FLD_LENGTH_KM); // / 1000;
end;


//-------------------------------------------------------------------
procedure TdmLinkLine.GetLinkListByLinkLineID(aID: integer; aIDList: TIDList);
//-------------------------------------------------------------------
var
 //  i, k: integer;
  oItem: TIDListItem;
begin
  Assert(Assigned(aIDList), 'Value not assigned');


  dmOnega_DB_data.OpenQuery(qry_LInks,
//     'SELECT * FROM '+ TBL_LINKLINE_LINK_XREF +' WHERE linkline_id = :linkline_id',
//     'SELECT LINK_ID,name,status FROM '+ VIEW_LINKLINE_LINKS +' WHERE linkline_id = :linkline_id  ORDER BY name ',
     'SELECT LINK_ID,name FROM '+ VIEW_LINKLINE_LINKS +' WHERE linkline_id = :linkline_id',
     [FLD_LINKLINE_ID, aID]);


  aIDList.Clear;

  with qry_LInks do
    while not Eof do
  begin
    oItem:=aIDList.AddID (FieldValues[FLD_LINK_ID]);
    oItem.Name:=FieldByName(FLD_NAME).AsString;
    Next;
  end;
end;


//-------------------------------------------------------------------
function TdmLinkLine.GetBLRect (aID: integer): TBLRect;
//-------------------------------------------------------------------
var blVector: TblVector;
begin
  Result:=NULL_BLRECT();

  db_OpenQuery (qry_Temp,
                'SELECT lat1,lon1,lat2,lon2 FROM '+ view_LinkLine_links + ' WHERE (linkline_id=:id)',
                [db_Par(FLD_ID, aID)]);

  with qry_Temp do
    while not EOF do
    begin
      blVector:=db_ExtractBLVector(qry_Temp);

      Assert(blVector.Point1.B<>0);
      Assert(blVector.Point1.L<>0);

      Assert(blVector.Point2.B<>0);
      Assert(blVector.Point2.L<>0);


      geo_RoundedBLRect (blVector, Result);

      Next;
    end;
end;


//-------------------------------------------------------
function TdmLinkLine.Add (aRec: TdmLinkLineAddRec; aIDList: TIDList): integer;
//-------------------------------------------------------
var
  ind,i: integer;
  dEsrNorm, dEsrReq, dBberNorm, dBberReq: Double;
  k: Integer;
  rec: TCalcEsrBberParamRec;

begin
  Result:=dmOnega_DB_data.LINKLINE_ADD
           (dmMain.ProjectID, aRec.Name);
           //, aRec.Link1_id, aRec.Link2_id);


  if Assigned(aIDList) then
   for i:= 0 to aIDList.Count-1 do
     k:=dmOnega_DB_data.LinkLine_Add_link(Result, aIDList.Items[i].ID);
//     dmLinkLine_link.Add (Result, aIDList.Items[i].ID);


  ind:=3;  //������������� ���� (�� 600)

  //------------------------------------------------------
  FillChar(rec, SizeOf(rec), 0);

  rec.ID      :=Result;
  rec.ObjName :=OBJ_LINKLINE;
  rec.Length_KM:=GetLengthKm(Result);
  rec.GST_Type :=ind;

{  rec.dEsrNorm,
  rec.dEsrReq,
  rec.dBberNorm,
  rec.dBberReq
}
  dmLink_Calc_SESR.CalcEsrBber11(rec
//              Result, OBJ_LINKLINE,
//            // dmLinkLine.
//              GetLengthKm(Result),
//              ind,
//              dEsrNorm, dEsrReq, dBberNorm, dBberReq
              );
  //------------------------------------------------------

  Update_ (Result,
         [db_Par(FLD_GST_TYPE,   ind ),
          db_Par(FLD_GST_LENGTH, LINKLINE_NORM_ARR1[ind].Max_Length_KM ),
          db_Par(FLD_GST_SESR,   LINKLINE_NORM_ARR1[ind].SESR ),
          db_Par(FLD_GST_KNG,    LINKLINE_NORM_ARR1[ind].KNG ),

          db_Par(FLD_ESR_NORM,      rec.Output.Esr_Norm),
          db_Par(FLD_BBER_NORM,     rec.Output.Bber_Norm),

          db_Par(FLD_ESR_REQUIRED,  rec.Output.Esr_required),
          db_Par(FLD_BBER_REQUIRED, rec.Output.Bber_Required)
         ]);

end;



begin

end.

