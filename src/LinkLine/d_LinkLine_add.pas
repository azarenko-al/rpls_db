unit d_LinkLine_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,     StdCtrls, ExtCtrls,
  rxPlacemnt, ActnList,    cxGridCustomView,
  ADODB, ComCtrls,   cxPropertiesStore, cxClasses, cxControls,
   ToolWin, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGrid, cxGridDBBandedTableView,

  d_Wizard_add_with_Inspector,

  fr_LinkLine_add_,

  dm_User_Security,


  dm_Main,

  I_Shell,

  dm_LinkLine,
  dm_LinkLine_Link,
  dm_LinkLine_calc,

  dm_LinkLine_Add,

  u_func,
  u_classes,
  u_db,

  u_types,
  u_const_str,

  cxGridDBTableView
  ;

type
  Tdlg_LinkLine_add = class(Tdlg_Wizard_add_with_Inspector)
    Actions: TActionList;
    act_Select_first_link: TAction;
    act_add_right_neighbour: TAction;
    act_del_right_neighbour: TAction;
    act_add_left_neighbour : TAction;
    act_del_left_neighbour : TAction;
    ToolBar1: TToolBar;
    StatusBar1: TStatusBar;
    cxGrid_L: TcxGrid;
    cx_Left: TcxGridDBBandedTableView;
    cx_LeftNAME: TcxGridDBBandedColumn;
    cx_LeftID: TcxGridDBBandedColumn;
    cx_LeftDBBandedColumn1: TcxGridDBBandedColumn;
    cx_LeftDBBandedColumn2: TcxGridDBBandedColumn;
    cx_LeftDBBandedColumn3: TcxGridDBBandedColumn;
    cxGrid_Left: TcxGridLevel;
    Panel4: TPanel;
    Button7: TButton;
    Button8: TButton;
    cxGrid_C: TcxGrid;
    cx_LInks: TcxGridDBBandedTableView;
    cx_LInksname: TcxGridDBBandedColumn;
    cx_LInksid: TcxGridDBBandedColumn;
    cx_main_property1_id: TcxGridDBBandedColumn;
    cx_main_property2_id: TcxGridDBBandedColumn;
    cx_LInksDBBandedColumn1: TcxGridDBBandedColumn;
    cxGrid_CLevel1: TcxGridLevel;
    pn_Buttons_right: TPanel;
    Button5: TButton;
    Button6: TButton;
    cxGrid_R: TcxGrid;
    cx_Right: TcxGridDBBandedTableView;
    cx_RightNAME: TcxGridDBBandedColumn;
    cx_RightID: TcxGridDBBandedColumn;
    cx_RightDBBandedColumn2_property1_id: TcxGridDBBandedColumn;
    cx_RightDBBandedColumn3_property2_id: TcxGridDBBandedColumn;
    cx_RightDBBandedColumn1: TcxGridDBBandedColumn;
    cxGrid_Right: TcxGridLevel;
    pn_Buttons_Left: TPanel;
    Button1: TButton;
    Button2: TButton;

    procedure _ActionMenu(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure ActionsUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean); 
    procedure cx_LeftDblClick(Sender: TObject);
    procedure cx_RightDblClick(Sender: TObject);

  private
    FID: integer;
    FIDList: TIDList;

    Fframe_LinkLine_add_: Tframe_LinkLine_add_;

  //  FEditState: (esEdit,esInsert);

    procedure Append;
  public
    class function UpdateDlg (aID: integer): boolean;

    class function ExecDlg(aIDList: TIDList=nil): integer;
  end;


//==================================================================
implementation  {$R *.DFM}
//==================================================================

//--------------------------------------------------------------------
class function Tdlg_LinkLine_add.UpdateDlg (aID: integer): boolean;
//--------------------------------------------------------------------
begin


  with Tdlg_LinkLine_add.Create(Application) do
  try
    FID:=aID;


    ed_Name_.Text:=dmLinkLine.GetNameByID (aID);

    dmLinkLine.GetLinkListByLinkLineID (aID, FIDList);
    dmLinkLine_Add.InitItemsFromIDList (FIDList);

//    dmLinkLine_Add.Edit (FIDList);

//    ShowPassive (False);

    Result:=(ShowModal=mrOk);

  finally
    Free;
  end;
end;


//--------------------------------------------------------------------
class function Tdlg_LinkLine_add.ExecDlg(aIDList: TIDList=nil): integer;
//--------------------------------------------------------------------
var
  i: Integer;

  s: string;
begin
{
  if dmUser_Security.ProjectIsReadOnly then
  begin
    ShowMessage('���������� �������� ������. ������ ������ ��� ������.');
    Result:=0;
    Exit;
  end;
}

 // Screen.Cursor:=crHourGlass;

  with Tdlg_LinkLine_add.Create(Application) do
  try
 //   FFolderID:=aFolderID;
    ed_Name_.Text:=dmLinkLine.GetNewName;

//    FEditState:=esInsert;

    //restore names
    if Assigned(aIDList) then
      dmLinkLine_Add.InitItemsFromIDList(aIDList);


    if ShowModal=mrOk then
      Result:=FID
    else
      Result:=0;
  finally
    Free;
  end;
end;


//--------------------------------------------------------------------
procedure Tdlg_LinkLine_add.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
var
  I: Integer;
begin
  inherited;
  SetActionName (STR_DLG_ADD_LINKLINE);

  lb_Action.Caption:= '�������� �����';

  TabSheet1.Caption:='�� ���������';

// .. ts_Params.TabVisible := False;

//  TdmLinkLine_Add.Init;

  FIDList:=TIDList.Create;

 // cxGrid_passive2.Align :=alclient;
  cxGrid_C.Align:=alClient;


 // TdmLinkLine_Add.Init;
  dmLinkLine_Add.OpenDB;

  cx_LInks.DataController.DataSource := dmLinkLine_Add.ds_Items;
  cx_Left.DataController.DataSource  := dmLinkLine_Add.ds_left;
  cx_Right.DataController.DataSource := dmLinkLine_Add.ds_right;

 // CreateChildForm(Tframe_LinkLine_add, frame_LinkLine_add, GSPage1);

 // CreateChildForm(Tframe_LinkLine_add_, Fframe_LinkLine_add_, TabSheet2);




end;

//----------------------------------------------------------------------
procedure Tdlg_LinkLine_add.FormDestroy(Sender: TObject);
//----------------------------------------------------------------------
begin
  FreeAndNil(FIDList);
  dmLinkLine_Add_Free();

  FreeAndNil(Fframe_LinkLine_add_);


 // FreeAndNil(dmLinkLine_Add);

  inherited;
end;

//----------------------------------------------------------------------
procedure Tdlg_LinkLine_add.FormResize(Sender: TObject);
//----------------------------------------------------------------------
var iW: integer;
begin
  iW:=Trunc((TabSheet1.Width- 30*2)/3);

  cxGrid_L.Width:= iW;
  cxGrid_R.Width:= iW;
end;

//--------------------------------------------------------------------
procedure Tdlg_LinkLine_add.Append;
//--------------------------------------------------------------------
var rec: TdmLinkLineAddRec;
begin
  FillChar(rec, SizeOf(rec), 0);

  rec.Name :=ed_Name_.Text;

  //------------------------------------------------------
  FIDList.LoadIDsFromDataSet (dmLinkLine_Add.mem_Items);

  //------------------------------------------------------

  if FID > 0 then
  begin
    dmLinkLine_Link.LinkListRemoveAll (FID);
    dmLinkLine_Link.LinkListAdd (FID, FIDList);

  end else
    FID:=dmLinkLine.Add (rec, FIDList);


  dmLinkLine_calc.UpdateSummary (FID);


//
//  I_Shell,
////  if aFolderID=0 then
//    sFolderGUID:=g_Obj.ItemByName[OBJ_PROPERTY].RootFolderGUID;
// // else
//  //  sFolderGUID:=dmFolder.GetGUIDByID(aFolderID);
//
//  g_ShellEvents.Shell_UpdateNodeChildren_ByGUID (sFolderGUID);

end;


//-------------------------------------------------------------------
procedure Tdlg_LinkLine_add._ActionMenu(Sender: TObject);
//-------------------------------------------------------------------
begin
  if Sender=act_Ok then begin
    Append;
  end else

  //------------------------------------------------------
  if Sender=act_Select_first_link then
  //------------------------------------------------------
  begin
    if not dmLinkLine_Add.Dlg_AddFirstInterval1() then
      Exit;

  end else

  if sender=act_add_right_neighbour then
    dmLinkLine_Add.add_neighbour(ntRight) else

  if sender=act_del_right_neighbour then
    dmLinkLine_Add.del_neighbour(ntRight) else

  if sender=act_add_left_neighbour then
    dmLinkLine_Add.add_neighbour(ntLeft) else

  if sender=act_del_left_neighbour then
    dmLinkLine_Add.del_neighbour(ntLeft);

end;

//------------------------------------------------------
procedure Tdlg_LinkLine_add.ActionsUpdate(Action: TBasicAction; var Handled: Boolean);
//------------------------------------------------------
begin
  act_del_left_neighbour.Enabled:=  cx_LInks.DataController.RecordCount>1;
  act_del_right_neighbour.Enabled:= cx_LInks.DataController.RecordCount>1;

  act_add_left_neighbour.Enabled:=  cx_Left.DataController.RecordCount>0;
  act_add_right_neighbour.Enabled:= cx_Right.DataController.RecordCount>0;

end;

//------------------------------------------------------
procedure Tdlg_LinkLine_add.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//------------------------------------------------------
begin
  act_OK.Enabled := cx_Links.DataController.RecordCount>=2;
end;


procedure Tdlg_LinkLine_add.cx_LeftDblClick(Sender: TObject);
begin
  act_add_left_neighbour.Execute;
end;


procedure Tdlg_LinkLine_add.cx_RightDblClick(Sender: TObject);
begin
  act_add_right_neighbour.Execute;
end;



end.