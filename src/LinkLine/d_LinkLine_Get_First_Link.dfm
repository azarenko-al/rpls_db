object dlg_LinkLine_Get_First_Link: Tdlg_LinkLine_Get_First_Link
  Left = 485
  Top = 309
  Width = 498
  Height = 486
  BorderWidth = 3
  Caption = 'dlg_LinkLine_Get_First_Link'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 476
    Height = 105
    Caption = 'ToolBar1'
    Color = clAppWorkSpace
    EdgeBorders = []
    ParentColor = False
    TabOrder = 0
    Visible = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 406
    Width = 476
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 1
    DesignSize = (
      476
      35)
    object btn_Ok: TButton
      Left = 321
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object Button2: TButton
      Left = 403
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ModalResult = 2
      TabOrder = 1
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 105
    Width = 476
    Height = 192
    Align = alTop
    TabOrder = 2
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnDblClick = cxGrid1DBTableView1DblClick
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
      end
      object cxGrid1DBTableView1name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        SortIndex = 0
        SortOrder = soAscending
        Width = 285
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qry_Links: TADOQuery
    AutoCalcFields = False
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega'
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'SELECT id,name,pmp_site_id,pmp_site_name'
      ''
      'FROM view_pmp_sectors')
    Left = 36
    Top = 4
  end
  object DataSource1: TDataSource
    DataSet = qry_Links
    Left = 36
    Top = 52
  end
end
