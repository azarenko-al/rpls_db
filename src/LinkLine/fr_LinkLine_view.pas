unit fr_LinkLine_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, rxPlacemnt, ActnList, Menus, cxPropertiesStore,  cxPC,
  ToolWin, Grids, DBGrids,      DBCtrls,
  ComCtrls, ExtCtrls,  ImgList,

  fr_View_base,

  fr_Link_items,

  //fr_Link_Passive_profile,
  fr_LinkLine_inspector,
  fr_LinkLine_Links,

  f_Documents,

  I_Shell,

  I_LinkLine,


  dm_LinkNet_Item_Ref,
  dm_LinkLine_calc,
  dm_LinkLine,

  u_func,
  u_dlg,

  u_const_db,
  u_const_str,
  u_types,

  cxControls, dxBar, cxClasses, cxBarEditItem, cxLookAndFeels;
  

type
  Tframe_LinkLine_View = class(Tframe_View_Base)
    ActionList1: TActionList;
    act_Calc: TAction;
    act_Update: TAction;
    act_Report: TAction;
    PageControl1: TPageControl;
    TabSheet_Main: TTabSheet;
    TabSheet_Links: TTabSheet;
    TabSheet_Reports: TTabSheet;
    Panel2: TPanel;
    procedure act_CalcExecute(Sender: TObject);
//    procedure cxPageControl1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
  //  procedure FormDestroy(Sender: TObject);
 //   procedure GSPages1Click(Sender: TObject);
  private
    Fframe_inspector: Tframe_LinkLine_inspector;
    Ffrm_Links: Tframe_LinkLine_Links;

  //  Fframe_Link_passive_profile: Tframe_Link_passive_profile;

(*    Ffrm_Link_items1: Tframe_Link_items;
    Ffrm_Link_items2: Tframe_Link_items;
*)
    Ffrm_Documents: Tfrm_Documents;

    FActivePage: integer;

// TODO: View_passive
////    FHasRepeater: Boolean;
//  procedure View_passive(aLinkLineID: integer);

//    FProcIndex: integer;
 //   Ffrm_Map: Tframe_LinkLine_Map;
  //  FID,
 //   procedure GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean);

  protected
    procedure GetShellMessage(aMsg: integer; aRec: TShellEventRec); override;
  public
    ViewMode: (vmLinkLine, vmRR_Net_LinkLine_Ref);

    procedure View(aID: integer; aGUID: string); override;
  end;



//==================================================================
//implementation
//==================================================================
implementation {$R *.dfm}

uses
  dm_Act_LinkLine, dm_User_Security;


//--------------------------------------------------------------------
procedure Tframe_LinkLine_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
  FActivePage:=-1;

  ObjectName:=OBJ_LINKLINE;

  TableName:=TBL_LINKLINE;


  PageControl1.Align:=alClient;

//  cxPageControl1.ActivePage:=p_Main;

//  GSPages1.Align:=alClient;

  Caption:=STR_LINKLINE;

  CreateChildForm(Tframe_LinkLine_inspector,   Fframe_inspector, TabSheet_Main);
  CreateChildForm(Tframe_LinkLine_Links,       Ffrm_Links,       TabSheet_Links);
  CreateChildForm(Tfrm_Documents,              Ffrm_Documents,   TabSheet_Reports);


(*
  CreateChildForm(Tframe_Link_items,  Ffrm_Link_items1,  pn_Repeater_top);
  CreateChildForm(Tframe_Link_items,  Ffrm_Link_items2,  pn_Repeater_bottom);
*)
 // pn_repeater_top.Height := 300;


 // pn_Repeater_bottom.Align:=alClient;


  AddComponentProp(PageControl1, 'ActivePage');

  Assert(Assigned(PageControl1.OnChange));

 // cxPropertiesStore.RestoreFrom;
end;


//--------------------------------------------------------------------
procedure Tframe_LinkLine_View.View(aID: integer; aGUID: string);
//--------------------------------------------------------------------
var
  bReadOnly: Boolean;
  i: Integer;
begin
  inherited;

  FActivePage:=-1;

  case ViewMode of
    vmRR_Net_LinkLine_Ref: ID:=dmLinkNet_Item_Ref.GetLinkLineID (aID);
    vmLinkLine:            ID:=aID;
  end;


  if not RecordExists (ID) then
    Exit;


   bReadOnly:=dmUser_Security.Object_Edit_ReadOnly();


  Fframe_inspector.SetReadOnly (bReadOnly);
  Ffrm_Links.SetReadOnly (bReadOnly);



//  bReadOnly := dmUser_Security.ProjectIsReadOnly;
//  SetFormActionsEnabled(Self, not bReadOnly);

//  Fframe_inspector.SetReadOnly (bReadOnly);
//  Ffrm_Links.SetReadOnly (bReadOnly);


//  Fframe_CalcRegion_Params.SetReadOnly (bReadOnly);
 // Fframe_CalcRegion_Cells.SetReadOnly (bReadOnly);



(*
 // i:=dmLinkLine.GetIntFieldValue(ID, FLD_Has_Repeater);
  FHasRepeater:=(0<>dmLinkLine.GetIntFieldValue(ID, FLD_Has_Repeater));

  if FHasRepeater then
  begin
    if (not Assigned(Fframe_Link_passive_profile)) then
      CreateChildForm(Tframe_Link_passive_profile, Fframe_Link_passive_profile, TabSheet_Repeater);

    View_passive(ID);

  end;
  *)



//  TabSheet_Repeater.TabVisible:=FHasRepeater;
  //TabSheet_Repeater_items.TabVisible:=FHasRepeater;


  PageControl1Change(nil);
end;




// TODO: View_passive
////--------------------------------------------------------------------
//procedure Tframe_LinkLine_View.View_passive(aLinkLineID: integer);
////--------------------------------------------------------------------
//var
//FLinkLinePassiveRec: TdmLinkLinePassiveInfoRec;
//
//begin
//
//if dmLinkLine.GetPassiveInfo(aLinkLineID, FLinkLinePassiveRec) then
//begin
//  Ffrm_Link_items1.View(FLinkLinePassiveRec.Link1.id);
//  Ffrm_Link_items2.View(FLinkLinePassiveRec.Link2.id);
//
//end;
//
//
//end;

//--------------------------------------------------------------
procedure Tframe_LinkLine_View.act_CalcExecute(Sender: TObject);
//--------------------------------------------------------------
begin
  //-------------------------------------------------------------------
  if Sender=act_Calc then begin
  //-------------------------------------------------------------------

{
    dmLinkLine_Calc.IsReFreshCalcs:=ConfirmDlg('���������� ��� ��������� ������?');
//      dmLinkLine_Calc.IsReFreshCalcs:=True;

    dmLinkLine_Calc.LinkLineID:= FID;
    dmLinkLine_Calc.ExecuteDlg ('������ �����');
}
    dmLinkLine_Calc.Dlg_Exec(ID);

    View (ID, FGUID);
  end else

  //-------------------------------------------------------------------
  if Sender=act_Report then begin
  //-------------------------------------------------------------------
  //  Assert(Assigned(ILinkLine));

    dmAct_LinkLine.MakeReport(ID);
  end else
end;


//--------------------------------------------------------------
procedure Tframe_LinkLine_View.GetShellMessage(aMsg: integer; aRec: TShellEventRec);
//--------------------------------------------------------------
begin
  case aMSG of
    WE__EXPLORER_UPDATE_NODE_CHILDREN_BY_OBJNAME:
      if PageControl1.ActivePageIndex in [0,1] then
        if Eq(aRec.ObjName, OBJ_LINKLINE) and (aRec.ID=ID) then
          Ffrm_Links.View (ID);

  end;
end;


procedure Tframe_LinkLine_View.PageControl1Change(Sender: TObject);
begin
  if ID=0 then
    exit;


  FActivePage:= PageControl1.ActivePageIndex;

  Fframe_inspector.Post;

  with PageControl1 do
    if ActivePage=TabSheet_Main then Fframe_inspector.View (ID) else
    if ActivePage=TabSheet_Links then Ffrm_Links.View (ID) else
    if ActivePage=TabSheet_Reports then Ffrm_Documents.View_LinkLine (ID) else

(*
    if ActivePage=TabSheet_Repeater then
      if FHasRepeater then
        if (Assigned(Fframe_Link_passive_profile)) then
           Fframe_Link_passive_profile.View (ID) ;
*)


//        else
//
//
//  case cxPageControl1.ActivePageIndex of
//    0: Fframe_inspector.View (ID);
//    1: Ffrm_Links.View (ID);
//    2: Ffrm_Documents.View_LinkLine (ID);
//
//    3: if FHasRepeater and (Assigned(Fframe_Link_passive_profile)) then
//         Fframe_Link_passive_profile.View(ID);
//
// //  3: //if dmLinkLine.GetIntFieldValue(ID, FLD_Is_Passive_Relay)=1 then
//  //     Fframe_Link_passive_profile.View(ID);
//
//  end;

end;


end.



{


//-------------------------------------------------------------------
procedure Tframe_LinkLine_View.GSPages1Click(Sender: TObject);
//-------------------------------------------------------------------
begin
 //

end;



procedure Tframe_LinkLine_View.FormDestroy(Sender: TObject);
begin
 // GSPages1.OnClick:=nil;

  inherited;
end;}




{  mnu_Calc.Action:=dmAct_LinkLine.act_
  mnu_Report.Action:=dmAct_LinkLine.act_Report;

}

//  CreateChildForm(Tframe_Link_passive_profile, Fframe_Link_passive_profile, p_Repeater);



(*
//--------------------------------------------------------------------
procedure Tframe_LinkLine_View.cxPageControl1Change(Sender: TObject);
//--------------------------------------------------------------------
begin
 //.. if FActivePage=cxPageControl1.ActivePageIndex then
// ..   Exit;

end;*)