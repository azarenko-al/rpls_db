unit dm_act_LinkLine;

interface

uses
  Classes, Menus, Forms, ActnList, 


  dm_User_Security,

 // d_Audit,


 // I_act_LinkLine,

//  I_Act_Report, //dm_Act_Report;

  u_DataExport_run,
                    

  dm_act_Base,

  I_Object,
  I_Shell,
  u_Shell_new,

  u_const_db,
  

  u_types,

  u_classes,

  u_func,

  u_Link_Report_Lib,

  fr_LinkLine_view,
  
  dm_LinkLine_Calc,
  dm_LinkLine,

 // d_LinkLine_passive_add,
  d_LinkLine_add

  ;

type
  TdmAct_LinkLine = class(TdmAct_Base)// IAct_LinkLine_X)
    act_Edit: TAction;
    act_Calc: TAction;
    act_Report: TAction;
    act_Add_Item: TAction;
    act_Add_Item_passive111111: TAction;
    ActionList2: TActionList;
    act_Export_MDB: TAction;

  //  procedure DataModuleDestroy(Sender: TObject);
  //  procedure act_Add_ItemExecute(Sender: TObject);
//    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

  private
    function CheckActionsEnable: Boolean;
// TODO: Links_Add11
//  procedure Links_Add11(aLinkLineID : integer);
//

  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm  (aOwnerForm: TForm): TForm; override;

    procedure DoAction (Sender: TObject);

    function ItemDel(aID: integer; aName: string = ''): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;

    procedure ShowOnMap (aID: integer);
  public
    procedure MakeReport(aID: integer);

    function Dlg_Add (aIDList: TIDList): integer;

    class procedure Init;
  end;


var
  dmAct_LinkLine: TdmAct_LinkLine;

//==================================================================
// implementation
//==================================================================
implementation
 {$R *.dfm}





//--------------------------------------------------------------------
class procedure TdmAct_LinkLine.Init;
//--------------------------------------------------------------------
begin
  Assert(not Assigned(dmAct_LinkLine));

//  if not Assigned(dmAct_LinkLine) then
//  begin
    dmAct_LinkLine:=TdmAct_LinkLine.Create(Application);

  //  dmAct_LinkLine.GetInterface(IAct_LinkLine_X, IAct_LinkLine);
  //  Assert(Assigned(IAct_LinkLine));

 // end;
end;

//
//
//procedure TdmAct_LinkLine.DataModuleDestroy(Sender: TObject);
//begin
////  IAct_LinkLine := nil;
// // dmAct_LinkLine := nil;
//
//  inherited;
//end;


//--------------------------------------------------------------------
procedure TdmAct_LinkLine.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
var b: Boolean;

begin
  inherited;

  ObjectName:=OBJ_LINKLINE;

  
 // act_Add_Item.Caption :='�������� �� �����';
 // act_Add_Item_passive.Caption :='������� �� ����� c ��������� ��������������';
  act_Add.Caption:='������� �� �����'; // STR_ADD;

{  act_Audit.Caption:=STR_Audit;
  act_Audit.Enabled:=False;
}


   act_Export_MDB.Caption:=DEF_STR_Export_MDB;



 // act_Report.Enabled:=False;


//  act_ExtendedReport.Caption:= '����������� �����';
//  act_Move.Caption:=STR_MOVE;

  SetActionsExecuteProc ([
                        //  act_Add_Item,
                       //   act_Add_Item_passive,

                          act_Object_ShowOnMap,

                          act_Export_MDB,


                          act_Edit,
                          act_Calc,
                          act_Report

//                          act_Audit

                          ],
                        DoAction);

  SetCheckedActionsArr([
        act_Add,
        act_Del_,
        act_Del_list,

      //  act_Add_Item_passive,
        act_Edit,
        act_Calc
    ]);
                        
//  act_Add.Enabled := not dmUser_Security.ProjectIsReadOnly;

end;




//--------------------------------------------------------------------
function TdmAct_LinkLine.Dlg_Add (aIDList: TIDList): integer;
//--------------------------------------------------------------------
begin
  Result:= Tdlg_LinkLine_add.ExecDlg (aIDList);
  if Result <> 0 then
    g_Shell.UpdateNodeChildren_ByGUID(g_Obj.ItemByType[otLinkLine].RootFolderGUID); //dmLinkLine.GetGUIDByID(Result));
end;


//--------------------------------------------------------------------
procedure TdmAct_LinkLine.ShowOnMap (aID: integer);
//--------------------------------------------------------------------
begin
  ShowRectOnMap (dmLinkLine.GetBLRect (aID), aID);
end;

//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_LinkLine.DoAction(Sender: TObject);
//var
 // sXMLFileName: string;
 // sGUID: string;
begin

{  if Sender=act_Audit then
     Dlg_Audit (TBL_LinkLine, FFocusedID) else
}


  if Sender=act_Export_MDB then
    TDataExport_run.ExportToRplsDb_selected (TBL_LINKLINE, FSelectedIDList)
  else


  if Sender=act_Object_ShowOnMap then
    ShowOnMap (FFocusedID) else

(*
  if Sender=act_Add_Item_passive then
  begin
     if Tdlg_LinkLine_Passive_add.ExecDlg()>0 then
        g_ShellEvents.Shell_UpdateNodeChildren_ByGUID (GUID_LINKLINE);

  end else
*)


  //-------------------------------------------------------------------
  if Sender=act_Edit then
  //-------------------------------------------------------------------
  begin
    if Tdlg_LinkLine_add.UpdateDlg (FFocusedID) then
    begin
    //  sGUID := dmLinkLine.GetGUIDByID(FFocusedID);

      g_ShellEvents.Shell_UpdateNodeChildren_ByObjName(OBJ_LinkLine, FFocusedID);
    //  g_ShellEvents.Shell_UpdateObjectView(OBJ_LinkLine, ID)
    end;
  //  begin
//      if Assigned(frame_LinkLine_View) then
//        frame_LinkLine_View.RefreshInfo(FFocusedID);
 //   end
  end else

  // ---------------------------------------------------------------
  if Sender=act_Calc then
  // ---------------------------------------------------------------
  begin

{    dmLinkLine_Calc.IsReFreshCalcs:=
      MessageDlg('���������� ��� ��������� ������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes;

    dmLinkLine_Calc.LinkLineID:= FFocusedID;
    dmLinkLine_Calc.ExecuteDlg ('������ �����');
}
    dmLinkLine_Calc.Dlg_Exec(FFocusedID);

    { TODO : PostEvent(WE_REFRESH_LINKLINE_PARAMS, [app_Par(PAR_ID, FFocusedID)]); }
   // PostEvent(WE_REFRESH_LINKLINE_PARAMS, [app_Par(PAR_ID, FFocusedID)]);

  end else


  if Sender=act_Report then
    MakeReport(FFocusedID) else

{  if Sender=act_UnDelete then
    Tdlg_Restore_Objects.ExecDlg(otLinkLine, FFocusedID) else
}

//////        sh_PostUpdateNodeChildren(gl_DB.GetGUIDByID(g_Obj.ItemByType[ObjType].TableName, iID));


  //if Sender=act_Print then
   // dmLinkLine_print. PrintLine(FFocusedID) else

 { // -------------------------------------------------------------------
  if Sender=act_ExtendedReport then
  // -------------------------------------------------------------------
  begin
    ExtendedReport();
}
    { TODO :  if Sender=act_ExtendedReport then }

  //  ShowMessage('');

{
    sXMLFileName:=GetTempXmlFileName();

    LinkReport_Execute_LinkLine_complex (FFocusedID, sXMLFileName);

//    RunApp (GetApplicationDir() + EXE_LinkLine_report,  Format('%d %s',[FFocusedID,sXMLFileName]) );

 //   dmLinkLine_Report.Execute(FFocusedID, sXMLFileName, '');

    dmAct_Report.ExecDlg (sXMLFileName, CATEGORY_EXTENDED_LINKLINE);

}
    //Tfrm_LinkLine_Report.CreateForm(FFocusedID) else
 // end    else


end;

//--------------------------------------------------------------------
procedure TdmAct_LinkLine.MakeReport(aID: integer);
//--------------------------------------------------------------------
//var
//  rReportSelect: TdmReportSelectRec;
 // oReportSelect: TIni_Link_Report_Param;
begin
//  Assert(Assigned(IReport));

 // oReportSelect:=TIni_Link_Report_Param.Create;


//  if dmAct_Report.Dlg_Select ('linkline', oReportSelect) then //, sRepName); //aType: string);
  begin
//    rReportSelect.XMLFileName:=GetTempXmlFileName();

    u_Link_Report_lib.LinkReport_Execute_LinkLine ( aID);

  //  dmReport.MakeReport (rReportSelect);

  end;

//  FreeAndNil(oReportSelect);

end;


//--------------------------------------------------------------------
function TdmAct_LinkLine.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:= Tdlg_LinkLine_add.ExecDlg ();
end;


//�������� �����, � ����������; ����������� ���������� �� ��������� �
//����������� ��������� ����
//--------------------------------------------------------------------
function TdmAct_LinkLine.ItemDel(aID: integer; aName: string = ''): boolean;
//--------------------------------------------------------------------
begin
  //����� ��������� ����� �������� ��� � ����� � �����������
////////  AddTableToBackup(TBL_LINKLINE_LINK_XREF,db_Par(FLD_LINKLINE_ID, aID));

  Result:= dmLinkLine.Del (aID);
end;

//--------------------------------------------------------------------
function TdmAct_LinkLine.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_LinkLine_View.Create(aOwnerForm);
  (Result as Tframe_LinkLine_View).ViewMode:=vmLinkLine;
end;



// ---------------------------------------------------------------
function TdmAct_LinkLine.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
  Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];

  SetActionsEnabled1( [


  act_Edit,
  act_Calc,

// act_Export_MDB,
//                           act_Import_MDB,

//  act_Copy,
  act_Del_,
  act_Del_list

      ],

   Result );

   //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );


end;





//--------------------------------------------------------------------
procedure TdmAct_LinkLine.GetPopupMenu;
//--------------------------------------------------------------------
var
  bEnable: Boolean;
begin
  bEnable:=CheckActionsEnable();
  
//  CheckActionsEnable();

  case aMenuType of
    mtFolder: begin

         //       AddMenuItem (aPopupMenu, act_Add_Item);
//  dlg_AddMenuItem (oMenuItem, dmAct_Folder.act_Folder_Add);

//  act_Add.Tag:=integer(FSelectedPIDLs);

                AddFolderMenu_Create (aPopupMenu);
               // AddMenuItem (aPopupMenu, act_Add_Item_passive);
                AddFolderPopupMenu (aPopupMenu);
               
              end;
    mtItem:   begin
                AddMenuItem (aPopupMenu, act_Edit);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Del_);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Calc);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Report);
                AddMenuItem (aPopupMenu, act_Export_MDB);

             ////////   AddMenuItem (aPopupMenu, act_Audit);
                                 

            //    AddMenuItem (aPopupMenu, act_ExtendedReport);
            //    AddMenuItem (aPopupMenu, act_Print);

                AddFolderMenu_Tools (aPopupMenu);
              end;
    mtList:   begin
                AddMenuItem (aPopupMenu, act_Del_list);
                AddFolderMenu_Tools (aPopupMenu);
               // AddMenuItem (oMenuItem, act_Export_MDB);
              end;
  end;
end;


end.
