object frame_LinkLine_add: Tframe_LinkLine_add
  Left = 345
  Top = 384
  Width = 917
  Height = 403
  Caption = 'frame_LinkLine_add'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel4: TPanel
    Left = 0
    Top = 334
    Width = 909
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 0
    object Button7: TButton
      Left = 1
      Top = 9
      Width = 155
      Height = 23
      Action = act_Select_first_link
      TabOrder = 0
    end
  end
  object cxGrid_C: TcxGrid
    Left = 0
    Top = 0
    Width = 197
    Height = 334
    Align = alLeft
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    object cx_LInks: TcxGridDBBandedTableView
      DataController.Filter.MaxValueListCount = 1000
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086': 0'
          Kind = skCount
          Column = cx_LInksname
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      Filtering.MaxDropDownCount = 12
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.BandMoving = False
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.BandHeaders = False
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      Preview.Visible = True
      Bands = <
        item
          Caption = #1056#1056' '#1080#1085#1090#1077#1088#1074#1072#1083#1099' '#1074' '#1083#1080#1085#1080#1080
          Width = 185
        end>
      object cx_LInksname: TcxGridDBBandedColumn
        Tag = 1
        Caption = #1056#1056' '#1080#1085#1090#1077#1088#1074#1072#1083#1099' '#1074' '#1083#1080#1085#1080#1080
        DataBinding.FieldName = 'name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 134
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object cx_LInksid: TcxGridDBBandedColumn
        DataBinding.FieldName = 'id'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Visible = False
        Hidden = True
        Options.Filtering = False
        Width = 30
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object cx_main_property1_id: TcxGridDBBandedColumn
        DataBinding.FieldName = 'property1_id'
        Visible = False
        Hidden = True
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object cx_main_property2_id: TcxGridDBBandedColumn
        DataBinding.FieldName = 'property2_id'
        Visible = False
        Hidden = True
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object cx_LInksDBBandedColumn1: TcxGridDBBandedColumn
        DataBinding.FieldName = 'property_id'
        Visible = False
        Hidden = True
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
    end
    object cxGrid_CLevel1: TcxGridLevel
      GridView = cx_LInks
    end
  end
  object DBGrid1: TDBGrid
    Left = 584
    Top = 0
    Width = 325
    Height = 334
    Align = alRight
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Visible = False
  end
  object pn_Right: TPanel
    Left = 295
    Top = 0
    Width = 289
    Height = 334
    Align = alRight
    Caption = 'pn_Right'
    TabOrder = 3
    object pn_Top: TPanel
      Left = 1
      Top = 1
      Width = 287
      Height = 128
      Align = alTop
      BevelOuter = bvNone
      BorderWidth = 5
      Caption = 'pn_Top'
      TabOrder = 0
      object cxGrid_L: TcxGrid
        Left = 34
        Top = 5
        Width = 228
        Height = 118
        Align = alLeft
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cx_Left: TcxGridDBBandedTableView
          OnDblClick = cx_LeftDblClick
          DataController.Filter.MaxValueListCount = 1000
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086': 0'
              Kind = skCount
              Column = cx_LeftNAME
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          Filtering.MaxDropDownCount = 12
          OptionsCustomize.ColumnMoving = False
          OptionsCustomize.ColumnSorting = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Editing = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          OptionsView.GroupFooters = gfAlwaysVisible
          OptionsView.BandHeaders = False
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          Bands = <
            item
              Caption = #1057#1086#1089#1077#1076#1085#1080#1077' '#1087#1088#1086#1083#1105#1090#1099' '#1089#1083#1077#1074#1072
            end>
          object cx_LeftNAME: TcxGridDBBandedColumn
            Tag = 1
            Caption = #1057#1086#1089#1077#1076#1085#1080#1077' '#1080#1085#1090#1077#1088#1074#1072#1083#1099' '#1089#1083#1077#1074#1072
            DataBinding.FieldName = 'NAME'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = True
            Options.Filtering = False
            Width = 223
            Position.BandIndex = 0
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object cx_LeftID: TcxGridDBBandedColumn
            DataBinding.FieldName = 'ID'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = True
            Visible = False
            Hidden = True
            Options.Filtering = False
            Width = 88
            Position.BandIndex = 0
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object cx_LeftDBBandedColumn1: TcxGridDBBandedColumn
            DataBinding.FieldName = 'property1_id'
            Visible = False
            Hidden = True
            Options.Filtering = False
            Position.BandIndex = 0
            Position.ColIndex = 2
            Position.RowIndex = 0
          end
          object cx_LeftDBBandedColumn2: TcxGridDBBandedColumn
            DataBinding.FieldName = 'property2_id'
            Visible = False
            Hidden = True
            Options.Filtering = False
            Position.BandIndex = 0
            Position.ColIndex = 3
            Position.RowIndex = 0
          end
          object cx_LeftDBBandedColumn3: TcxGridDBBandedColumn
            DataBinding.FieldName = 'property_id'
            Visible = False
            Hidden = True
            Position.BandIndex = 0
            Position.ColIndex = 4
            Position.RowIndex = 0
          end
        end
        object cxGrid_Left: TcxGridLevel
          GridView = cx_Left
        end
      end
      object Panel3: TPanel
        Left = 5
        Top = 5
        Width = 29
        Height = 118
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvNone
        TabOrder = 1
        object Button3: TButton
          Left = 0
          Top = 29
          Width = 24
          Height = 24
          Action = act_del_left_neighbour
          TabOrder = 0
        end
        object Button4: TButton
          Left = 0
          Top = 0
          Width = 24
          Height = 24
          Action = act_add_left_neighbour
          TabOrder = 1
        end
      end
    end
    object pn_Bottom: TPanel
      Left = 1
      Top = 184
      Width = 287
      Height = 149
      Align = alBottom
      BevelOuter = bvNone
      BorderWidth = 5
      Caption = 'pn_Bottom'
      TabOrder = 1
      object Panel6: TPanel
        Left = 5
        Top = 5
        Width = 29
        Height = 139
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvNone
        TabOrder = 0
        object Button5: TButton
          Left = 0
          Top = 1
          Width = 24
          Height = 24
          Action = act_add_right_neighbour
          TabOrder = 0
        end
        object Button6: TButton
          Left = 0
          Top = 30
          Width = 24
          Height = 24
          Action = act_del_right_neighbour
          TabOrder = 1
        end
      end
      object cxGrid_R: TcxGrid
        Left = 34
        Top = 5
        Width = 236
        Height = 139
        Align = alLeft
        TabOrder = 1
        LookAndFeel.Kind = lfFlat
        object cx_Right: TcxGridDBBandedTableView
          OnDblClick = cx_RightDblClick
          DataController.Filter.MaxValueListCount = 1000
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086': 0'
              Kind = skCount
              Column = cx_RightNAME
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          Filtering.MaxDropDownCount = 12
          OptionsCustomize.ColumnMoving = False
          OptionsCustomize.ColumnSorting = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.BandMoving = False
          OptionsData.Editing = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          OptionsView.GroupFooters = gfVisibleWhenExpanded
          OptionsView.BandHeaders = False
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          Bands = <
            item
              Caption = #1057#1086#1089#1077#1076#1085#1080#1077' '#1087#1088#1086#1083#1105#1090#1099
            end>
          object cx_RightNAME: TcxGridDBBandedColumn
            Tag = 1
            Caption = #1057#1086#1089#1077#1076#1085#1080#1077' '#1080#1085#1090#1077#1088#1074#1072#1083#1099' '#1089#1087#1088#1072#1074#1072
            DataBinding.FieldName = 'NAME'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = True
            Options.Filtering = False
            Width = 223
            Position.BandIndex = 0
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object cx_RightID: TcxGridDBBandedColumn
            DataBinding.FieldName = 'ID'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = True
            Visible = False
            Hidden = True
            Options.Filtering = False
            Width = 88
            Position.BandIndex = 0
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object cx_RightDBBandedColumn2_property1_id: TcxGridDBBandedColumn
            DataBinding.FieldName = 'property1_id'
            Visible = False
            Hidden = True
            Options.Filtering = False
            Position.BandIndex = 0
            Position.ColIndex = 2
            Position.RowIndex = 0
          end
          object cx_RightDBBandedColumn3_property2_id: TcxGridDBBandedColumn
            DataBinding.FieldName = 'property2_id'
            Visible = False
            Hidden = True
            Options.Filtering = False
            Position.BandIndex = 0
            Position.ColIndex = 3
            Position.RowIndex = 0
          end
          object cx_RightDBBandedColumn1: TcxGridDBBandedColumn
            DataBinding.FieldName = 'property_id'
            Visible = False
            Hidden = True
            Position.BandIndex = 0
            Position.ColIndex = 4
            Position.RowIndex = 0
          end
        end
        object cxGrid_Right: TcxGridLevel
          GridView = cx_Right
        end
      end
    end
  end
  object Actions: TActionList
    OnUpdate = ActionsUpdate
    Left = 225
    Top = 16
    object act_Select_first_link: TAction
      Caption = #1042#1099#1073#1088#1072#1090#1100' '#1087#1077#1088#1074#1099#1081' '#1080#1085#1090#1077#1088#1074#1072#1083
      OnExecute = _ActionMenu
    end
    object act_add_right_neighbour: TAction
      Caption = '<'
      OnExecute = _ActionMenu
    end
    object act_del_right_neighbour: TAction
      Caption = '>'
      OnExecute = _ActionMenu
    end
    object act_add_left_neighbour: TAction
      Caption = '<'
      OnExecute = _ActionMenu
    end
    object act_del_left_neighbour: TAction
      Caption = '>'
      OnExecute = _ActionMenu
    end
  end
end
