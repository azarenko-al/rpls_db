unit dm_LinkLine_link;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Db, ADODB,

  dm_Onega_DB_data,

  dm_Main,
  dm_Object_base,

  u_Geo,
  u_db,
  u_func,
  u_classes,

  u_types,

  u_const_str,
  u_const_db
  ;


type

  //-------------------------------------------------------------------
  TdmLinkLine_link = class(TdmObject_base)
    qry_Temp: TADOQuery;
  //  procedure DataModuleCreate(Sender: TObject);
  private

  public
    function GetLinkID(aID: integer): integer;

    procedure LinkListRemoveAll(aLinkLineID: integer); overload;
    procedure LinkListAdd(aLinkLineID: integer; aIDList: TIDList);

  end;

function dmLinkLine_link: TdmLinkLine_link;


//====================================================================
implementation {$R *.dfm}
//====================================================================

var
  FdmLinkLine_link: TdmLinkLine_link;


// ---------------------------------------------------------------
function dmLinkLine_link: TdmLinkLine_link;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLinkLine_link) then
    FdmLinkLine_link := TdmLinkLine_link.Create(Application);

  Result := FdmLinkLine_link;
end;


{
procedure TdmLinkLine_link.DataModuleCreate(Sender: TObject);
begin
  inherited;
///  TableName:=TBL_LinkLineLinkXREF;
end;
}

// -------------------------------------------------------------------
function TdmLinkLine_link.GetLinkID(aID: integer): integer;
// -------------------------------------------------------------------
begin
  Result:=gl_DB.GetIntFieldValue (TBL_LinkLineLinkXREF, FLD_LINK_ID, [db_Par(FLD_ID, aID)]);
end;


//-------------------------------------------------------------------
procedure TdmLinkLine_link.LinkListAdd(aLinkLineID: integer; aIDList: TIDList);
//-------------------------------------------------------------------
var i: integer;
  iLinkID: Integer;
begin
//  db_OpenQuery (qry_Temp,
  //              'SELECT LINK_ID FROM '+ VIEW_LINKLINE_LINKS + ' WHERE (linkline_id=:id)',
        //       [db_Par(FLD_ID, aLinkLineID)]);

  for i:=0 to aIDList.Count-1 do
  begin
    iLinkID  := aIDList[i].ID;

    dmOnega_DB_data.ExecStoredProc_('sp_LinkLine_Add_link',
             [FLD_LINKLINE_ID, aLinkLineID,
              FLD_LINK_ID,     iLinkID ]);
  end;

 //   if not qry_Temp.Locate (FLD_LINK_ID, aIDList[i].ID, []) then
   //   Add (aLinkLineID, aIDList[i].ID);

end;


//-------------------------------------------------------------------
procedure TdmLinkLine_link.LinkListRemoveAll(aLinkLineID: integer);
//-------------------------------------------------------------------
{
CREATE procedure [dbo].sp_LinkLine_remove_link
(
  @LINKLINE_ID 	int, 
  @LINK_ID 		int = NULL 
)

AS
BEGIN
  IF ISNULL(@LINK_ID,0)=0
    DELETE FROM LinkLineLinkXREF
       WHERE (LinkLine_ID=@LinkLine_ID)  
  ELSE
    DELETE FROM LinkLineLinkXREF
       WHERE (LinkLine_ID=@LinkLine_ID) and (Link_ID=@Link_ID); 
END
}


var
  k: integer;
begin
  k:=dmOnega_DB_data.ExecStoredProc('sp_LinkLine_remove_link',
     [db_Par(FLD_LINKLINE_ID, aLinkLineID) ]);

//  gl_DB.DeleteRecords(TBL_LinkLineLinkXREF,
  //                   [db_Par(FLD_LINKLINE_ID, aLinkLineID) ] );
end;


begin

end.




// TODO: LinkListRemove
////-------------------------------------------------------------------
//procedure TdmLinkLine_link.LinkListRemove (aLinkLineID: integer; aIDList: TIDList);
////-------------------------------------------------------------------
//var i: integer;
//begin
//for i:=0 to aIDList.Count-1 do
//  Remove (aLinkLineID, aIDList[i].ID);
//end;



// TODO: Add
//// TODO: Remove
////procedure TdmLinkLine_link.Remove (aLinkLineID, aLinkID: integer);
////begin
////dmOnega_DB_data.LinkLine_Remove_link(aLinkLineID, aLinkID);
////end;
//
//
//procedure TdmLinkLine_link.Add(aLinkLineID,aLinkID: integer);
//begin
//dmOnega_DB_data.LinkLine_Add_link(aLinkLineID, aLinkID);
//end;