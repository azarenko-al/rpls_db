object frame_LinkLine_add_: Tframe_LinkLine_add_
  Left = 463
  Top = 304
  Width = 770
  Height = 391
  Caption = 'frame_LinkLine_add_'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 762
    Height = 241
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    OnClick = Panel1Click
    object cxGrid_L: TcxGrid
      Left = 0
      Top = 0
      Width = 225
      Height = 206
      Align = alLeft
      TabOrder = 0
      LookAndFeel.Kind = lfFlat
      object cx_Left: TcxGridDBBandedTableView
        DataController.Filter.MaxValueListCount = 1000
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086': 0'
            Kind = skCount
            Column = cx_LeftNAME
          end>
        DataController.Summary.SummaryGroups = <>
        NavigatorButtons.ConfirmDelete = False
        Filtering.MaxDropDownCount = 12
        OptionsCustomize.ColumnMoving = False
        OptionsCustomize.ColumnSorting = False
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Editing = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        OptionsView.GroupFooters = gfAlwaysVisible
        OptionsView.BandHeaders = False
        Preview.AutoHeight = False
        Preview.MaxLineCount = 2
        Bands = <
          item
            Caption = #1057#1086#1089#1077#1076#1085#1080#1077' '#1087#1088#1086#1083#1105#1090#1099
          end>
        object cx_LeftNAME: TcxGridDBBandedColumn
          Tag = 1
          Caption = #1057#1086#1089#1077#1076#1085#1080#1077' '#1080#1085#1090#1077#1088#1074#1072#1083#1099
          DataBinding.FieldName = 'NAME'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Filtering = False
          SortIndex = 0
          SortOrder = soDescending
          Width = 223
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object cx_LeftID: TcxGridDBBandedColumn
          DataBinding.FieldName = 'ID'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Hidden = True
          Options.Filtering = False
          Width = 88
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object col_property1_id_: TcxGridDBBandedColumn
          DataBinding.FieldName = 'property1_id'
          Hidden = True
          Options.Filtering = False
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object col_property2_id_: TcxGridDBBandedColumn
          DataBinding.FieldName = 'property2_id'
          Hidden = True
          Options.Filtering = False
          Position.BandIndex = 0
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object cx_LeftDBBandedColumn3: TcxGridDBBandedColumn
          DataBinding.FieldName = 'property_id'
          Hidden = True
          Position.BandIndex = 0
          Position.ColIndex = 4
          Position.RowIndex = 0
        end
      end
      object cxGrid_Left: TcxGridLevel
        GridView = cx_Left
      end
    end
    object pn_Buttons_Left: TPanel
      Left = 225
      Top = 0
      Width = 29
      Height = 206
      Align = alLeft
      Alignment = taLeftJustify
      BevelOuter = bvNone
      TabOrder = 1
      object Button1: TButton
        Left = 3
        Top = 6
        Width = 24
        Height = 24
        Action = act_add_left_neighbour
        TabOrder = 0
      end
      object Button2: TButton
        Left = 3
        Top = 35
        Width = 24
        Height = 24
        Action = act_del_left_neighbour
        TabOrder = 1
      end
    end
    object cxGrid_C: TcxGrid
      Left = 254
      Top = 0
      Width = 184
      Height = 206
      Align = alLeft
      TabOrder = 2
      LookAndFeel.Kind = lfFlat
      object cx_LInks: TcxGridDBBandedTableView
        DataController.Filter.MaxValueListCount = 1000
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086': 0'
            Kind = skCount
            Column = cx_LInksname
          end>
        DataController.Summary.SummaryGroups = <>
        NavigatorButtons.ConfirmDelete = False
        Filtering.MaxDropDownCount = 12
        OptionsCustomize.ColumnMoving = False
        OptionsCustomize.ColumnSorting = False
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.BandMoving = False
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsSelection.CellSelect = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsSelection.MultiSelect = True
        OptionsView.ColumnAutoWidth = True
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        OptionsView.BandHeaders = False
        Preview.AutoHeight = False
        Preview.MaxLineCount = 2
        Preview.Visible = True
        Bands = <
          item
            Caption = #1056#1056' '#1080#1085#1090#1077#1088#1074#1072#1083#1099' '#1074' '#1083#1080#1085#1080#1080
            Width = 185
          end>
        object cx_LInksname: TcxGridDBBandedColumn
          Tag = 1
          Caption = #1056#1056' '#1080#1085#1090#1077#1088#1074#1072#1083#1099' '#1074' '#1083#1080#1085#1080#1080
          DataBinding.FieldName = 'name'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Options.Filtering = False
          Width = 134
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object cx_LInksid: TcxGridDBBandedColumn
          DataBinding.FieldName = 'id'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Hidden = True
          Options.Filtering = False
          Width = 30
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object cx_main_property1_id: TcxGridDBBandedColumn
          DataBinding.FieldName = 'property1_id'
          Hidden = True
          Options.Filtering = False
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object cx_main_property2_id: TcxGridDBBandedColumn
          DataBinding.FieldName = 'property2_id'
          Hidden = True
          Options.Filtering = False
          Position.BandIndex = 0
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object cx_LInksDBBandedColumn1: TcxGridDBBandedColumn
          DataBinding.FieldName = 'property_id'
          Hidden = True
          Position.BandIndex = 0
          Position.ColIndex = 4
          Position.RowIndex = 0
        end
      end
      object cxGrid_CLevel1: TcxGridLevel
        GridView = cx_LInks
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 206
      Width = 762
      Height = 35
      Align = alBottom
      BevelOuter = bvNone
      Constraints.MaxHeight = 35
      Constraints.MinHeight = 35
      TabOrder = 3
      object Button7: TButton
        Left = 1
        Top = 9
        Width = 155
        Height = 23
        Action = act_Select_first_link
        TabOrder = 0
      end
      object Button8: TButton
        Left = 166
        Top = 9
        Width = 75
        Height = 23
        Caption = #1054#1095#1080#1089#1090#1080#1090#1100
        TabOrder = 1
        Visible = False
        OnClick = _ActionMenu
      end
    end
    object pn_Buttons_right: TPanel
      Left = 578
      Top = 0
      Width = 29
      Height = 206
      Align = alRight
      Alignment = taLeftJustify
      BevelOuter = bvNone
      TabOrder = 4
      object Button5: TButton
        Left = 3
        Top = 6
        Width = 24
        Height = 24
        Action = act_add_right_neighbour
        TabOrder = 0
      end
      object Button6: TButton
        Left = 3
        Top = 35
        Width = 24
        Height = 24
        Action = act_del_right_neighbour
        TabOrder = 1
      end
    end
    object cxGrid_R: TcxGrid
      Left = 607
      Top = 0
      Width = 155
      Height = 206
      Align = alRight
      TabOrder = 5
      LookAndFeel.Kind = lfFlat
      object cx_Right: TcxGridDBBandedTableView
        DataController.Filter.MaxValueListCount = 1000
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086': 0'
            Kind = skCount
            Column = cx_RightNAME
          end>
        DataController.Summary.SummaryGroups = <>
        NavigatorButtons.ConfirmDelete = False
        Filtering.MaxDropDownCount = 12
        OptionsCustomize.ColumnMoving = False
        OptionsCustomize.ColumnSorting = False
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.BandMoving = False
        OptionsData.Editing = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        OptionsView.BandHeaders = False
        Preview.AutoHeight = False
        Preview.MaxLineCount = 2
        Bands = <
          item
            Caption = #1057#1086#1089#1077#1076#1085#1080#1077' '#1087#1088#1086#1083#1105#1090#1099
          end>
        object cx_RightNAME: TcxGridDBBandedColumn
          Tag = 1
          Caption = #1057#1086#1089#1077#1076#1085#1080#1077' '#1080#1085#1090#1077#1088#1074#1072#1083#1099
          DataBinding.FieldName = 'NAME'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Filtering = False
          SortIndex = 0
          SortOrder = soDescending
          Width = 223
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object cx_RightID: TcxGridDBBandedColumn
          DataBinding.FieldName = 'ID'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Hidden = True
          Options.Filtering = False
          Width = 88
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object col_property1_id__: TcxGridDBBandedColumn
          DataBinding.FieldName = 'property1_id'
          Hidden = True
          Options.Filtering = False
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object col_property2_id__: TcxGridDBBandedColumn
          DataBinding.FieldName = 'property2_id'
          Hidden = True
          Options.Filtering = False
          Position.BandIndex = 0
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object cx_RightDBBandedColumn1: TcxGridDBBandedColumn
          DataBinding.FieldName = 'property_id'
          Hidden = True
          Position.BandIndex = 0
          Position.ColIndex = 4
          Position.RowIndex = 0
        end
      end
      object cxGrid_Right: TcxGridLevel
        GridView = cx_Right
      end
    end
  end
  object Actions: TActionList
    OnUpdate = ActionsUpdate
    Left = 25
    Top = 260
    object act_Select_first_link: TAction
      Caption = #1042#1099#1073#1088#1072#1090#1100' '#1087#1077#1088#1074#1099#1081' '#1080#1085#1090#1077#1088#1074#1072#1083
      OnExecute = _ActionMenu
    end
    object act_add_right_neighbour: TAction
      Caption = '<'
      OnExecute = _ActionMenu
    end
    object act_del_right_neighbour: TAction
      Caption = '>'
      OnExecute = _ActionMenu
    end
    object act_add_left_neighbour: TAction
      Caption = '>'
      OnExecute = _ActionMenu
    end
    object act_del_left_neighbour: TAction
      Caption = '<'
      OnExecute = _ActionMenu
    end
  end
end
