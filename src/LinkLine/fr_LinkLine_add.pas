unit fr_LinkLine_add;

interface

uses
  SysUtils, Classes, Controls, Forms,
  DBGrids, Grids, cxGridCustomTableView, cxGridTableView,
  cxGridLevel, cxGridBandedTableView, cxClasses, cxControls,
  cxGridDBBandedTableView,
  cxGrid, StdCtrls, ExtCtrls, ActnList,

  dm_LinkLine,
  dm_LinkLine_Add,


  u_classes,
  cxGridCustomView;

type
  Tframe_LinkLine_add = class(TForm)
    Actions: TActionList;
    act_Select_first_link: TAction;
    act_add_right_neighbour: TAction;
    act_del_right_neighbour: TAction;
    act_add_left_neighbour: TAction;
    act_del_left_neighbour: TAction;
    Panel4: TPanel;
    Button7: TButton;
    cxGrid_C: TcxGrid;
    cx_LInks: TcxGridDBBandedTableView;
    cx_LInksname: TcxGridDBBandedColumn;
    cx_LInksid: TcxGridDBBandedColumn;
    cx_main_property1_id: TcxGridDBBandedColumn;
    cx_main_property2_id: TcxGridDBBandedColumn;
    cx_LInksDBBandedColumn1: TcxGridDBBandedColumn;
    cxGrid_CLevel1: TcxGridLevel;
    DBGrid1: TDBGrid;
    pn_Right: TPanel;
    pn_Top: TPanel;
    cxGrid_L: TcxGrid;
    cx_Left: TcxGridDBBandedTableView;
    cx_LeftNAME: TcxGridDBBandedColumn;
    cx_LeftID: TcxGridDBBandedColumn;
    cx_LeftDBBandedColumn1: TcxGridDBBandedColumn;
    cx_LeftDBBandedColumn2: TcxGridDBBandedColumn;
    cx_LeftDBBandedColumn3: TcxGridDBBandedColumn;
    cxGrid_Left: TcxGridLevel;
    Panel3: TPanel;
    Button3: TButton;
    Button4: TButton;
    pn_Bottom: TPanel;
    Panel6: TPanel;
    Button5: TButton;
    Button6: TButton;
    cxGrid_R: TcxGrid;
    cx_Right: TcxGridDBBandedTableView;
    cx_RightNAME: TcxGridDBBandedColumn;
    cx_RightID: TcxGridDBBandedColumn;
    cx_RightDBBandedColumn2_property1_id: TcxGridDBBandedColumn;
    cx_RightDBBandedColumn3_property2_id: TcxGridDBBandedColumn;
    cx_RightDBBandedColumn1: TcxGridDBBandedColumn;
    cxGrid_Right: TcxGridLevel;
    procedure FormDestroy(Sender: TObject);
    procedure ActionsUpdate(Action: TBasicAction; var Handled: Boolean);
//    procedure Button1Click(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
    procedure cx_LeftDblClick(Sender: TObject);
    procedure cx_RightDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure _ActionMenu(Sender: TObject);
  private
    FIDList: TIDList;

  public
    procedure Edit(aID: Integer);

  end;

var
  frame_LinkLine_add: Tframe_LinkLine_add;

implementation

{$R *.dfm}

procedure Tframe_LinkLine_add.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FIDList);
end;

procedure Tframe_LinkLine_add.ActionsUpdate(Action: TBasicAction; var Handled:
    Boolean);
begin
  act_del_left_neighbour.Enabled:=  cx_LInks.DataController.RecordCount>1;
  act_del_right_neighbour.Enabled:= cx_LInks.DataController.RecordCount>1;

  act_add_left_neighbour.Enabled:=  cx_Left.DataController.RecordCount>0;
  act_add_right_neighbour.Enabled:= cx_Right.DataController.RecordCount>0;

 // act_Select_first_link.Enabled:= cx_Links.DataController.RecordCount=0;

end;


procedure Tframe_LinkLine_add.cx_LeftDblClick(Sender: TObject);
begin
  act_add_left_neighbour.Execute;
end;

procedure Tframe_LinkLine_add.cx_RightDblClick(Sender: TObject);
begin
  act_add_right_neighbour.Execute;
end;



procedure Tframe_LinkLine_add.FormCreate(Sender: TObject);
begin
  FIDList:=TIDList.Create;

  cxGrid_C.Align:=alClient;
  cxGrid_L.Align:=alClient;
  cxGrid_R.Align:=alClient;

  pn_Bottom.Align:=alClient;
  pn_Right.Align:=alClient;

  TdmLinkLine_Add.Init;
  dmLinkLine_Add.OpenDB;

  cx_LInks.DataController.DataSource := dmLinkLine_Add.ds_Items;
  cx_Left.DataController.DataSource  := dmLinkLine_Add.ds_left;
  cx_Right.DataController.DataSource := dmLinkLine_Add.ds_right;

  DBGrid1.DataSource := dmLinkLine_Add.ds_All;

end;


procedure Tframe_LinkLine_add.FormResize(Sender: TObject);
var
  iW: integer;
begin
  iW:=Trunc((Width- 30*2)/3);

//  cxGrid_L.Width:= iW;
 // cxGrid_R.Width:= iW;

  pn_Top.Height:=Trunc(Height/2);

  cxGrid_C.Width:=Trunc(Width/2);

end;


procedure Tframe_LinkLine_add.Edit(aID: Integer);
begin
  dmLinkLine.GetLinkListByLinkLineID (aID, FIDList);
  dmLinkLine_Add.InitItemsFromIDList (FIDList);

end;

//-------------------------------------------------------------------
procedure Tframe_LinkLine_add._ActionMenu(Sender: TObject);
//-------------------------------------------------------------------
begin
  if Sender=act_Select_first_link then
    dmLinkLine_Add.Dlg_AddFirstInterval1() else

  if sender=act_add_right_neighbour then
    dmLinkLine_Add.add_neighbour(ntRight) else

  if sender=act_del_right_neighbour then
    dmLinkLine_Add.del_neighbour(ntRight) else

  if sender=act_add_left_neighbour then
    dmLinkLine_Add.add_neighbour(ntLeft) else

  if sender=act_del_left_neighbour then
    dmLinkLine_Add.del_neighbour(ntLeft);

end;



end.

