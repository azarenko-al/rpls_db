object frame_LinkLine_Links: Tframe_LinkLine_Links
  Left = 962
  Top = 483
  Width = 965
  Height = 507
  Caption = 'frame_LinkLine_Links'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 326
    Width = 949
    Height = 142
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object cxDBVerticalGrid1: TcxDBVerticalGrid
      Left = 0
      Top = 0
      Width = 321
      Height = 142
      BorderStyle = cxcbsNone
      Align = alLeft
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = True
      OptionsView.CellTextMaxLineCount = 3
      OptionsView.AutoScaleBands = False
      OptionsView.PaintStyle = psDelphi
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.RowHeaderMinWidth = 30
      OptionsView.RowHeaderWidth = 142
      OptionsView.ValueWidth = 98
      OptionsBehavior.BandSizing = False
      OptionsData.Editing = False
      OptionsData.Deleting = False
      TabOrder = 0
      DataController.DataSource = ds_LinkLine
      Version = 1
      object cxCategoryRow1: TcxCategoryRow
        Properties.Caption = #1050#1072#1095#1077#1089#1090#1074#1086' '#1089#1074#1103#1079#1080
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object row_SESR_required: TcxDBEditorRow
        Properties.Caption = 'SESR '#1090#1088#1077#1073' [%]'
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = False
        Properties.DataBinding.FieldName = 'SESR_required'
        ID = 1
        ParentID = 0
        Index = 0
        Version = 1
      end
      object row_sesr: TcxDBEditorRow
        Expanded = False
        Properties.Caption = 'SESR '#1088#1072#1089#1095' [%]'
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = True
        Properties.DataBinding.FieldName = 'sesr'
        ID = 2
        ParentID = 1
        Index = 0
        Version = 1
      end
      object row_kng_required: TcxDBEditorRow
        Properties.Caption = #1050#1085#1075' '#1090#1088#1077#1073' [%]'
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = False
        Properties.DataBinding.FieldName = 'kng_required'
        ID = 3
        ParentID = 0
        Index = 1
        Version = 1
      end
      object row_Kng: TcxDBEditorRow
        Expanded = False
        Properties.Caption = #1050#1085#1075' '#1088#1072#1089#1095' [%]'
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = True
        Properties.DataBinding.FieldName = 'Kng'
        ID = 4
        ParentID = 3
        Index = 0
        Version = 1
      end
      object row_status_str: TcxDBEditorRow
        Properties.Caption = #1055#1088#1080#1075#1086#1076#1085#1086#1089#1090#1100
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = False
        Properties.DataBinding.FieldName = 'status_str'
        ID = 5
        ParentID = -1
        Index = 1
        Version = 1
      end
      object col_length: TcxDBEditorRow
        Expanded = False
        Properties.Caption = #1044#1083#1080#1085#1072' ['#1084'] '
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = True
        Properties.DataBinding.FieldName = 'length'
        ID = 6
        ParentID = -1
        Index = 2
        Version = 1
      end
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 949
    Height = 177
    Align = alTop
    TabOrder = 2
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      PopupMenu = PopupMenu1
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = ds_Links
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086': 0'
          Kind = skCount
          Column = col__name
        end>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Editing = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.HeaderAutoHeight = True
      OptionsView.Indicator = True
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object col__name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Options.Filtering = False
        Options.Sorting = False
        Width = 161
      end
      object col__status: TcxGridDBColumn
        Caption = #1055#1088#1080#1075#1086#1076#1085#1086#1089#1090#1100
        DataBinding.FieldName = 'status_str'
        Options.Filtering = False
        Width = 79
      end
      object col__Power: TcxGridDBColumn
        Caption = #1059#1088#1086#1074#1077#1085#1100' [dBm]'
        DataBinding.FieldName = 'rx_level_dBm'
        Options.Filtering = False
        Width = 56
      end
      object col__sesr_required: TcxGridDBColumn
        Caption = 'SESR '#1090#1088#1077#1073' [%]'
        DataBinding.FieldName = 'sesr_norm'
        Options.Filtering = False
        Width = 72
      end
      object col__sesr_calc: TcxGridDBColumn
        Caption = 'SESR '#1088#1072#1089#1095' [%]'
        DataBinding.FieldName = 'sesr'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0.00000'
        Options.Filtering = False
        Width = 55
      end
      object col__kng_req_: TcxGridDBColumn
        Caption = #1050#1085#1075' '#1090#1088#1077#1073' [%]'
        DataBinding.FieldName = 'kng_norm'
        Options.Filtering = False
        Width = 60
      end
      object col__kng_calc: TcxGridDBColumn
        Caption = #1050#1085#1075' '#1088#1072#1089#1095' [%]'
        DataBinding.FieldName = 'kng'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0.00000'
        Options.Filtering = False
        Width = 50
      end
      object col__length: TcxGridDBColumn
        DataBinding.FieldName = 'length'
        Options.Filtering = False
        Width = 57
      end
      object col__fade_reserve: TcxGridDBColumn
        Caption = #1047#1072#1087#1072#1089' '#1085#1072' '#1079#1072#1084#1080#1088#1072#1085#1080#1103' [dB]'
        DataBinding.FieldName = 'FADE_MARGIN_DB'
        Options.Filtering = False
        Width = 92
      end
      object col__id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 31
      end
      object col_band: TcxGridDBColumn
        Caption = #1044#1080#1072#1087#1072#1079#1086#1085
        DataBinding.FieldName = 'band'
      end
      object col_GST_len: TcxGridDBColumn
        DataBinding.FieldName = 'GST_LENGTH'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 0
    Top = 318
    Width = 949
    Height = 8
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salBottom
    Control = Panel1
    Visible = False
  end
  object MainActionList: TActionList
    Left = 208
    Top = 196
    object act_Calc: TAction
      Caption = #1056#1072#1089#1095#1077#1090
      OnExecute = act_AddExecute
    end
    object act_Summary: TAction
      Caption = #1057#1090#1072#1090#1080#1089#1090#1080#1082#1072
      OnExecute = act_AddExecute
    end
    object act_Export: TAction
      Caption = 'Export'
      OnExecute = act_AddExecute
    end
    object act_Report: TAction
      Caption = #1054#1090#1095#1077#1090
      OnExecute = act_AddExecute
    end
    object act_Excel: TAction
      Caption = 'act_Excel'
      OnExecute = act_AddExecute
    end
    object act_Update: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      OnExecute = act_AddExecute
    end
  end
  object qry_LinkLine: TADOQuery
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega'
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select top 1 * from view_LinkLine1')
    Left = 24
    Top = 192
  end
  object ds_LinkLine: TDataSource
    DataSet = qry_LinkLine
    Left = 24
    Top = 240
  end
  object ds_Links: TDataSource
    DataSet = qry_Links
    Left = 105
    Top = 241
  end
  object qry_Links: TADOQuery
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=ONEGA'
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'SELECT top 10 *  FROM  view_LinkLine_links ')
    Left = 105
    Top = 193
  end
  object PopupMenu1: TPopupMenu
    Left = 304
    Top = 192
    object Export1: TMenuItem
      Action = act_Excel
    end
  end
  object ADOStoredProc1: TADOStoredProc
    Parameters = <>
    Left = 432
    Top = 192
  end
end
