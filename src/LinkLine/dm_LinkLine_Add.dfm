object dmLinkLine_Add: TdmLinkLine_Add
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 866
  Top = 521
  Height = 230
  Width = 473
  object ds_Items: TDataSource
    DataSet = mem_Items
    Left = 31
    Top = 77
  end
  object mem_Items: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 30
    Top = 19
  end
  object qry_Links_all: TADOQuery
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 296
    Top = 20
  end
  object mem_Left: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 118
    Top = 19
  end
  object mem_Right: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 190
    Top = 19
  end
  object ds_Left: TDataSource
    DataSet = mem_Left
    Left = 119
    Top = 77
  end
  object ds_Right: TDataSource
    DataSet = mem_Right
    Left = 191
    Top = 77
  end
  object ds_All: TDataSource
    DataSet = qry_Links_all
    Left = 295
    Top = 77
  end
end
