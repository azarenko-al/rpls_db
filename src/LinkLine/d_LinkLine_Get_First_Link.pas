unit d_LinkLine_Get_First_Link;

interface

uses
  Classes, Controls, Forms, ADODB, cxGridLevel,
  cxGridDBTableView, cxGrid, DB, StdCtrls, ExtCtrls,
  cxGridCustomTableView, cxGridTableView, cxClasses,
  ComCtrls, cxControls, cxGridCustomView,

  dm_Main,

  dm_Onega_DB_data,

  u_Func_arrays,  
  u_func,
  u_db,

  u_const_db,

  ToolWin, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData;


type
  Tdlg_LinkLine_Get_First_Link_rec = record
    ID: Integer;
    Name : string;

    Property1_ID: integer;
    Property2_ID: Integer;
  end;


  Tdlg_LinkLine_Get_First_Link = class(TForm)
    ToolBar1: TToolBar;
    Panel1: TPanel;
    btn_Ok: TButton;
    Button2: TButton;
    qry_Links: TADOQuery;
    DataSource1: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    procedure OpenDB;
    { Private declarations }
  public
    class function ExecDlg(var aRec: Tdlg_LinkLine_Get_First_Link_rec): boolean;

  end;


implementation

{$R *.dfm}


procedure Tdlg_LinkLine_Get_First_Link.cxGrid1DBTableView1DblClick(Sender:
    TObject);
begin
  btn_Ok.Click;
end;


//--------------------------------------------------------------------
class function Tdlg_LinkLine_Get_First_Link.ExecDlg(var aRec:
    Tdlg_LinkLine_Get_First_Link_rec): boolean;
//--------------------------------------------------------------------
begin
  with Tdlg_LinkLine_Get_First_Link.Create(Application) do
  begin
   // OpenDB;

  //  if aRec.ID>0 then
    //  ADOQuery1.Locate (FLD_ID, aRec.ID, 0);


    Result:=ShowModal=mrOK;

    if Result then
      with qry_Links do
      begin
        aRec.ID  := FieldByName(FLD_ID).AsInteger;
        aRec.name:= FieldByName(FLD_name).AsString;

        aRec.Property1_ID:=FieldByName(FLD_PROPERTY1_ID).AsInteger;
        aRec.Property2_ID:=FieldByName(FLD_PROPERTY2_ID).AsInteger;

  //      aRec.PmpSite_ID  := ADOQuery1.FieldByName(FLD_Pmp_Site_ID).AsInteger;
    //    aRec.PmpSite_name:= ADOQuery1.FieldByName(FLD_Pmp_Site_name).AsString;
      end;

    Free;
  end;
end;


procedure Tdlg_LinkLine_Get_First_Link.FormCreate(Sender: TObject);
begin
  Caption:='����� ���������';

  cxGrid1.Align:=alClient;

  OpenDB();
end;


procedure Tdlg_LinkLine_Get_First_Link.OpenDB;
begin
  dmOnega_DB_data.OpenQuery(qry_Links,
               'SELECT id,name,PROPERTY1_ID,PROPERTY2_ID FROM '
                + VIEW_LINK +
               ' WHERE project_id=:project_id '+
               ' ORDER BY name',
               [
                 FLD_project_id, dmMain.ProjectID
                 ]);



  db_SetFieldCaptions(qry_Links,
     [
       FLD_name,  '��������'

     ] );

end;

end.