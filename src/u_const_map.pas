unit u_const_map;

interface

const
  //-----------------------------------------
  // ����� ����
  //-----------------------------------------
  MAP_FILEDIR = 'MAPS';

  MAP_PROPERTY          = 'property.tab';
  MAP_LINK              = 'link.tab';
//  MAP_CALCREGION        = 'calc_region.tab';
  MAP_LINKEND           = 'linkend.tab';
  MAP_LINKEND_antenna   = 'linkend_antenna.tab';
  MAP_BSC               = 'BSC.tab';
  MAP_MSC               = 'msc.tab';
  MAP_SITE              = 'site.tab';
  MAP_CELL_ANT          = 'cell_ant.tab';
  MAP_X_CONN            = 'xconn.tab';
  MAP_PMP               = 'pmp_site.tab';
  MAP_PMP_SECTOR_ANT    = 'pmp_sector_ant.tab';
  MAP_PMP_TERMINAL      = 'pmp_terminal.tab';
  MAP_PMP_TERMINAL_ANT  = 'pmp_terminal_ant.tab';

  //-----------------------------------------
  // 3G
  //-----------------------------------------
  MAP_PMP_CALCREGION= 'pmp_calc_region.tab';

type
  TMapType = (mtGeoMap, mtCalcMap, mtObjectsMap, mtOther);

{
const

  LAYER_AntNeighbor     = 'LAYER_AntNeighbor';
  LAYER_CurAntNeighbor  = 'LAYER_CurAntNeighbor';
  LAYER_CurAnt          = 'LAYER_CurAnt';

  NEIGHBOR_LAYER               = 'Handover_Neighbors_Layer';

  LAYER_HighLightCellAntCI     = 'HighLightCellAntCI';
  LAYER_HighLightCellAntCA     = 'HighLightCellAntCA';
  LAYER_HighLightCellAntSector = 'HighLightCellAntSector';



  TEMP_LAYER = 'TEMP_LAYER';

}
  
implementation

end.
       //