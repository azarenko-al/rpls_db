unit dm_Custom;

interface

uses
  ADODB, Classes,  

  dm_Main,

//  u_class_manager,
  u_db
  ;

type

  TdmCustom = class(TDataModule) //, IADOHandler
//    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

  private
    //IADOHandler
    procedure SetADOConnection(aADOConnection: TADOConnection);
  public
  end;


implementation {$R *.DFM}


// -------------------------------------------------------------------
procedure TdmCustom.DataModuleCreate (Sender: TObject);
// -------------------------------------------------------------------
begin
  inherited;

  Assert(Assigned(dmMain), 'dmMain=nil: '+ClassName);

  SetADOConnection(dmMain.ADOConnection);

//  g_ClassManager.RegisterDataModule (Self);
end;


procedure TdmCustom.SetADOConnection(aADOConnection: TADOConnection);
begin
  db_SetComponentADOConnection (Self, aADOConnection);
end;


end.



(*

procedure TdmCustom.DataModuleDestroy(Sender: TObject);
begin
//  g_ClassManager.UnRegisterDataModule (Self);
end;

*)
