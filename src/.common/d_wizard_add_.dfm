object dlg_Wizard_add_: Tdlg_Wizard_add_
  Left = 811
  Top = 411
  Width = 513
  Height = 407
  ActiveControl = ed_Name_
  Caption = 'dlg_Wizard_add_'
  Color = clBtnFace
  Constraints.MinHeight = 371
  Constraints.MinWidth = 446
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 65
    Width = 505
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Constraints.MaxHeight = 50
    Constraints.MinHeight = 50
    TabOrder = 0
    DesignSize = (
      505
      50)
    object lb_Name: TLabel
      Left = 5
      Top = 3
      Width = 31
      Height = 13
      Caption = 'Name:'
    end
    object ed_Name_: TEdit
      Left = 5
      Top = 22
      Width = 495
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
    end
  end
  object pn_Buttons: TPanel
    Left = 0
    Top = 338
    Width = 505
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 1
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 505
      Height = 2
      Align = alTop
      Shape = bsTopLine
    end
    object Panel3: TPanel
      Left = 320
      Top = 2
      Width = 185
      Height = 33
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btn_Ok: TButton
        Left = 6
        Top = 5
        Width = 75
        Height = 23
        Action = act_Ok
        Default = True
        ModalResult = 1
        TabOrder = 0
      end
      object btn_Cancel: TButton
        Left = 94
        Top = 5
        Width = 75
        Height = 23
        Action = act_Cancel
        Cancel = True
        ModalResult = 2
        TabOrder = 1
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 505
    Height = 65
    Align = alTop
    BevelOuter = bvLowered
    Color = clActiveBorder
    TabOrder = 2
    Visible = False
  end
  object ActionList1: TActionList
    Left = 33
    Top = 12
    object act_Ok: TAction
      Category = 'Main'
      Caption = 'Ok'
    end
    object act_Cancel: TAction
      Category = 'Main'
      Caption = #1054#1090#1084#1077#1085#1072
    end
  end
  object FormStorage1: TFormStorage
    Active = False
    IniFileName = 'Software\Onega\'
    Options = [fpPosition]
    StoredValues = <>
    Left = 72
    Top = 12
  end
end
