unit d_wizard_add_;

interface

uses
  Classes, Controls, Forms, StdCtrls, ExtCtrls,

    ActnList, rxPlacemnt
  ;

type
  Tdlg_Wizard_add_ = class(TFOrm)
    Panel1: TPanel;
    lb_Name: TLabel;
    ed_Name_: TEdit;
    ActionList1: TActionList;
    act_Ok: TAction;
    act_Cancel: TAction;
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    Panel3: TPanel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    Panel2: TPanel;
    FormStorage1: TFormStorage;
    procedure FormCreate(Sender: TObject);
  protected
    FRegPath: string;

    procedure SetDefaultSize;
    procedure SetDefaultWidth;
  public
  end;


//==================================================================
implementation {$R *.DFM}
//==================================================================

const
  REGISTRY_COMMON_FORMS  = 'Software\Onega\Common\Forms\';

const
  DLG_CAPTION_OBJECT_CREATE = '�������� �������';


procedure Tdlg_Wizard_add_.FormCreate(Sender: TObject);
begin
  inherited;
  Caption:=DLG_CAPTION_OBJECT_CREATE;

  lb_Name.Caption:='��������';


  FRegPath:= REGISTRY_COMMON_FORMS + ClassName +'\';

  Assert(not FormStorage1.Active);

  FormStorage1.IniFileName:=FRegPath + FormStorage1.name;
  FormStorage1.IniSection:='Window';
  FormStorage1.Active:=True;

end;


procedure Tdlg_Wizard_add_.SetDefaultSize;
begin
  with Constraints do begin
    MaxHeight:=385;
    MinHeight:=MaxHeight;
  end;

  SetDefaultWidth();
end;


procedure Tdlg_Wizard_add_.SetDefaultWidth;
begin
  with Constraints do begin
    MaxWidth :=505;
    MinWidth :=MaxWidth;
  end;
end;


end.
