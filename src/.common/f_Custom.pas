
unit f_Custom;

interface

uses
  Classes, Controls, Forms, ActnList, ComCtrls,cxPropertiesStore,

  dm_Main,

  u_func_msg,
  u_db, ToolWin
  ;

type
  Tfrm_Custom = class(TForm)
    ToolBar1: TToolBar;
    ActionList: TActionList;
    cxPropertiesStore: TcxPropertiesStore;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
  //  FLocalRegPath: string;

//    FProcIndex: integer;
  protected
    FRegPath: string;

    procedure AddComponentProp (aComponent: TComponent; aField: string);

   procedure GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean); virtual;
  public
  end;


//====================================================================
// implementation
//====================================================================

implementation
{$R *.DFM}


const
  REGISTRY_COMMON_FORMS = 'Software\Onega\Common\Forms\';


// -------------------------------------------------------------------
procedure Tfrm_Custom.FormCreate(Sender: TObject);
// -------------------------------------------------------------------
begin
  FRegPath := REGISTRY_COMMON_FORMS + ClassName +'\';

  Assert(Assigned(dmMain));

  db_SetComponentADOConnection (Self, dmMain.ADOConnection);


//  FProcIndex:=g_EventManager.RegisterProc (GetMessage, Name);

  Assert(not cxPropertiesStore.Active);

  cxPropertiesStore.StorageName:=FRegPath+'cxPropertiesStore';
  cxPropertiesStore.Active := True;

end;


procedure Tfrm_Custom.FormDestroy(Sender: TObject);
begin
//  g_EventManager.UnRegisterProc (FProcIndex, GetMessage);

  inherited;
end;


procedure Tfrm_Custom.AddComponentProp(aComponent: TComponent; aField: string);
var
  oStoreComponent: TcxPropertiesStoreComponent;
begin
  oStoreComponent := TcxPropertiesStoreComponent(cxPropertiesStore.Components.Add);

  oStoreComponent.Component:=aComponent;
  oStoreComponent.Properties.Add(aField);

end;


procedure Tfrm_Custom.GetMessage(aMsg: integer; aParams: TEventParamList; var aHandled: Boolean);
begin
//!!!!!!!!!
end;


procedure Tfrm_Custom.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;


end.


   {



