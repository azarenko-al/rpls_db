unit d_Wizard_Add_with_params;

interface

uses
  Classes, Controls,
  Forms, ExtCtrls,  cxButtonEdit,

  d_Wizard_add,

//  I_Act_Explorer, //dm_Act_Explorer;

  u_types,

  u_const_str,

  ComCtrls, cxLookAndFeels, cxPropertiesStore, rxPlacemnt, ActnList,
  StdCtrls, dxSkinsCore, dxSkinsDefaultPainters;

type

  Tdlg_Wizard_add_with_params = class(Tdlg_Wizard_add)
    Panel2: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;

    procedure FormCreate(Sender: TObject);

  private
  protected
    function Dlg_SelectObject_cxButtonEdit(Sender: TcxButtonEdit; aType:
        TrpObjectType; var aID: integer): Boolean;
  end;
                              
//==================================================================
implementation
{$R *.DFM}

uses
  dm_Act_Explorer;


//-------------------------------------------------------------------
procedure Tdlg_Wizard_add_with_params.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;


// ��  ts_Params.Caption:='���������';
  TabSheet1.Caption:='���������';

  PageControl1.Align:=alClient;

//GSPages1.Left:=5;
//GSPages1.Width:=iWidth;

  Panel2.Align:=alClient;

  Caption:=DLG_CAPTION_OBJECT_CREATE;
(*
  with GSPages1 do
    if PageCount = 1 then
      BackColor:=Color;
*)
end;



//-------------------------------------------------------------------
function Tdlg_Wizard_add_with_params.Dlg_SelectObject_cxButtonEdit(Sender:
    TcxButtonEdit; aType: TrpObjectType; var aID: integer): Boolean;
//-------------------------------------------------------------------
begin
  Assert(Sender is TcxButtonEdit);

  Result:=dmAct_Explorer.Dlg_SelectObject_cxButtonEdit(Sender as TcxButtonEdit, aType, aID);
end;

end.

