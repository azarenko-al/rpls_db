inherited DlgSaveMapToFile: TDlgSaveMapToFile
  Left = 1023
  Top = 494
  VertScrollBar.Range = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'DlgSaveMapToFile'
  ClientHeight = 326
  ClientWidth = 573
  FormStyle = fsStayOnTop
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 291
    Width = 573
    inherited Bevel1: TBevel
      Width = 573
    end
    inherited Panel3: TPanel
      Left = 394
    end
    object btn_Rect: TButton
      Left = 8
      Top = 8
      Width = 113
      Height = 23
      Caption = #1042#1099#1073#1088#1072#1090#1100' '#1092#1088#1072#1075#1084#1077#1085#1090
      TabOrder = 1
      OnClick = btn_RectClick
    end
  end
  inherited pn_Top_: TPanel
    Width = 573
    Height = 54
    Constraints.MaxHeight = 0
    Constraints.MinHeight = 0
    Visible = True
    inherited Bevel2: TBevel
      Top = 51
      Width = 573
    end
    inherited pn_Header: TPanel
      Width = 573
      Height = 51
      inherited lb_Action: TLabel
        Top = 4
        Width = 368
        Height = 39
        Caption = 
          #1041#1091#1076#1077#1090' '#1074#1099#1087#1086#1083#1085#1077#1085#1086' '#1089#1086#1093#1088#1072#1085#1077#1085#1080#1077' '#1074#1099#1073#1088#1072#1085#1085#1086#1075#1086' '#1092#1088#1072#1075#1084#1077#1085#1090#1072' '#1082#1072#1088#1090#1099' '#1074' '#1092#1072#1081#1083' bmp' +
          #13#10#1055#1088#1080#1084#1077#1095#1072#1085#1080#1077': '#1076#1083#1103' '#1093#1088#1072#1085#1077#1085#1080#1103' '#1074#1088#1077#1084#1077#1085#1085#1099#1093' '#1092#1072#1081#1083#1086#1074' '#1087#1086#1085#1072#1076#1086#1073#1080#1090#1089#1103' '#1089#1074#1086#1073#1086#1076#1085#1086 +
          #1075#1086#13#10#1084#1077#1089#1090#1072' '#1074#1086' '#1074#1088#1077#1084#1077#1085#1085#1086#1081' '#1087#1072#1087#1082#1077' '#1074#1076#1074#1086#1077' '#1073#1086#1083#1100#1096#1077' '#1088#1072#1089#1089#1095#1080#1090#1072#1085#1085#1086#1075#1086
      end
    end
  end
  object rg_Scale: TRadioGroup [2]
    Left = 8
    Top = 108
    Width = 249
    Height = 73
    Caption = #1052#1072#1089#1096#1090#1072#1073' '#1101#1082#1089#1087#1086#1088#1090#1072' (1 : ... ) '#1089#1084
    Items.Strings = (
      #1042#1079#1103#1090#1100' '#1089' '#1082#1072#1088#1090#1099' ('#1101#1082#1089#1087#1086#1088#1090' '#1082#1072#1088#1090#1099' "'#1082#1072#1082' '#1077#1089#1090#1100'")'
      #1059#1082#1072#1079#1072#1090#1100' '#1089#1074#1086#1081)
    TabOrder = 2
    OnClick = rg_ScaleClick
  end
  object GroupBox1: TGroupBox [3]
    Left = 8
    Top = 56
    Width = 449
    Height = 49
    Caption = #1060#1072#1081#1083':'
    TabOrder = 3
    DesignSize = (
      449
      49)
    object ed_FileName: TFilenameEdit
      Left = 9
      Top = 18
      Width = 429
      Height = 22
      DialogKind = dkSave
      DefaultExt = 'bmp'
      Filter = 'BMP image (*.bmp)|*.bmp'
      DialogOptions = [ofHideReadOnly, ofExtensionDifferent, ofNoLongNames, ofEnableSizing]
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 0
    end
  end
  object GroupBox2: TGroupBox [4]
    Left = 264
    Top = 108
    Width = 193
    Height = 95
    Caption = #1042#1099#1093#1086#1076#1085#1086#1081' '#1092#1072#1081#1083
    TabOrder = 4
    object Label1: TLabel
      Left = 16
      Top = 21
      Width = 76
      Height = 13
      Caption = #1064#1080#1088#1080#1085#1072' ('#1090#1086#1095#1082#1080')'
    end
    object Label2: TLabel
      Left = 16
      Top = 43
      Width = 75
      Height = 13
      Caption = #1042#1099#1089#1086#1090#1072' ('#1090#1086#1095#1082#1080')'
    end
    object Label3: TLabel
      Left = 16
      Top = 66
      Width = 63
      Height = 13
      Caption = #1056#1072#1079#1084#1077#1088' ('#1052#1073')'
    end
    object ed_Width: TEdit
      Left = 112
      Top = 18
      Width = 66
      Height = 21
      TabOrder = 0
      OnChange = ed_HeightChange
    end
    object ed_Height: TEdit
      Left = 112
      Top = 41
      Width = 66
      Height = 21
      TabOrder = 1
      OnChange = ed_HeightChange
    end
    object ed_Size: TEdit
      Left = 112
      Top = 64
      Width = 66
      Height = 21
      Enabled = False
      ReadOnly = True
      TabOrder = 2
    end
  end
  object pn_Main: TScrollBox [5]
    Left = 504
    Top = 132
    Width = 177
    Height = 177
    BevelOuter = bvRaised
    BorderStyle = bsNone
    TabOrder = 5
    Visible = False
  end
  object ed_Scale: TComboBox [6]
    Left = 115
    Top = 151
    Width = 132
    Height = 21
    DropDownCount = 20
    ItemHeight = 13
    TabOrder = 6
    OnChange = ed_HeightChange
    OnCloseUp = ed_ScaleCloseUp
    Items.Strings = (
      '500'
      '1000'
      '2000'
      '5000'
      '10000'
      '25000'
      '50000'
      '100000'
      '200000'
      '500000'
      '1000000')
  end
  object pb: TProgressBar [7]
    Left = 0
    Top = 275
    Width = 573
    Height = 16
    Align = alBottom
    TabOrder = 7
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
  end
  inherited FormStorage1: TFormStorage
    StoredProps.Strings = (
      'ed_FileName.FileName')
  end
end
