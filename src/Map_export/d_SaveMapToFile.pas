unit d_SaveMapToFile;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, d_Wizard, rxPlacemnt, ActnList, StdCtrls, ExtCtrls, Mask, rxToolEdit,
  IniFiles, OleCtrls, Math,

  rxFileUtil,

  MapXLib_TLB,
  u_MapX,

  u_types,
  u_mapViewClasses,

  u_geo,
  u_geo_types,

  dm_Main,

  u_const,
  u_const_msg,
  u_const_map,

  u_func_msg,
  u_func_dlg,
  u_func_files,
  u_func_reg,
  u_func, cxPropertiesStore, ComCtrls
  ;

type
  TDlgSaveMapToFile = class(Tdlg_Wizard)
    rg_Scale: TRadioGroup;
    GroupBox1: TGroupBox;
    ed_FileName: TFilenameEdit;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    ed_Width: TEdit;
    ed_Height: TEdit;
    Label3: TLabel;
    ed_Size: TEdit;
    btn_Rect: TButton;
    pn_Main: TScrollBox;
    Map1_: TMap;
    Map_Preview_: TMap;
    ed_Scale: TComboBox;
    pb: TProgressBar;
    procedure rg_ScaleClick(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ed_HeightChange(Sender: TObject);
    procedure ed_ScaleCloseUp(Sender: TObject);
    procedure btn_RectClick(Sender: TObject);
  private
    FNotUpdate,
    FTestMode: boolean;

    FblRect, FInitBLRect: TBLRect;

    FDir: string;

    FWidth ,
    FHeight: Integer;

    FProcIndex: Integer;

    FInitielPanel: TPanel;
    FInitialMap: TMap;
    FInitialKoef: double;

    FDrawed: boolean;

    procedure DrawRegionOnMapUserLayer();

    function  GetScale: integer;

    procedure UpdateWidth;

  //  procedure GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean);

    //---------------------------------------------
    procedure UpdateZoom();
    function  MIF_ExportMap: boolean;
    function  JoinFiles(aImgFileName: string; aPieceCount: Integer): boolean;
    function  ExportMap(aPieceCount: Integer): boolean;
    //---------------------------------------------

  public
    class function ExecDlg(aPanel: TPanel; aScale: Integer;
        aMap: TMap; aGstFileName: string): boolean;
  end;


//==========================================================
implementation {$R *.dfm}
//==========================================================

const
  MAP_OBJ_TYPE  = 'Dlg_Save_Map';


//----------------------------------------------------------
class function TDlgSaveMapToFile.ExecDlg;
//----------------------------------------------------------
begin
  with TDlgSaveMapToFile.Create(Application) do
  begin
    FInitialMap:= aMap;
    FInitialKoef:= aMap.Width/aMap.Height;
    FInitielPanel:= aPanel;

    FInitBLRect:= mapX_XRectangleToBLRect(FInitialMap.Bounds);
    FBLRect:= FInitBLRect;

    FWidth := aMap.Width;
    FHeight:= aMap.Height;

    ed_Scale.Text:= AsString(aScale);

    Show;

    UpdateWidth;

    DrawRegionOnMapUserLayer();
    FDrawed:= true;
  end;
end;

//----------------------------------------------------------
procedure TDlgSaveMapToFile.FormCreate(Sender: TObject);
//----------------------------------------------------------
begin
  inherited;
  FDrawed:= false;

//  FProcIndex:= RegisterProc (GetMessage, Name);

  rg_Scale.ItemIndex:=
    gl_Reg.RegIni.ReadInteger(TDlgSaveMapToFile.ClassName, 'rg_Scale.ItemIndex', 0);

//  FormStyle:= IIF(dmMain.DEBUG, fsNormal, fsStayOnTop);

  Caption:= '������� ����� � ����';
end;

//----------------------------------------------------------
procedure TDlgSaveMapToFile.FormDestroy(Sender: TObject);
//----------------------------------------------------------
begin
  inherited;

  UL_Del_Object_And_Draw(USER_LAYER_DIFF, -1, MAP_OBJ_TYPE);

  UnRegisterProc (FProcIndex);

  FInitialMap:= nil;
  gl_Reg.RegIni.WriteInteger(TDlgSaveMapToFile.ClassName, 'rg_Scale.ItemIndex', rg_Scale.ItemIndex);
end;

//--------------------------------------------------------------------
procedure TDlgSaveMapToFile.DrawRegionOnMapUserLayer();
//--------------------------------------------------------------------
var oStyle: TCustStyle;
begin
  oStyle.Pen.Width:= 3;
  oStyle.Pen.Color:= clBlue;
  oStyle.Brush.Style:= bsClear;

  UL_Del_Object(USER_LAYER_DIFF, -1, MAP_OBJ_TYPE);
  UL_Add_Rect(USER_LAYER_DIFF, -1, MAP_OBJ_TYPE, FBLRect, oStyle, '');
  UL_Draw(USER_LAYER_DIFF, NULL_RECT);
end;

//----------------------------------------------------------
procedure TDlgSaveMapToFile.rg_ScaleClick(Sender: TObject);
//----------------------------------------------------------
begin
  inherited;
  UpdateWidth;

  if Assigned(FInitialMap) then
    if rg_Scale.ItemIndex=0 then
      ed_Scale.Text:= AsString(Round(mapx_getScale(FInitialMap)));
end;

//----------------------------------------------------------
function  TDlgSaveMapToFile.GetScale: integer;
//----------------------------------------------------------
begin
  if rg_Scale.ItemIndex=1 then
    Result:= AsInteger(ed_Scale.Text)
  else
    Result:= Round(mapx_getScale(FInitialMap));
end;


//----------------------------------------------------------
procedure TDlgSaveMapToFile.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
//----------------------------------------------------------
begin
  inherited;
  act_Ok.Enabled:= (Trim(ed_FileName.FileName)<>'');

  if rg_Scale.ItemIndex=1 then
    act_Ok.Enabled:= (AsInteger(ed_Scale.Text)>0);

  ed_Width .Enabled:= (rg_Scale.ItemIndex=1);
  ed_Height.Enabled:= (rg_Scale.ItemIndex=1);
end;

//----------------------------------------------------------
procedure TDlgSaveMapToFile.UpdateWidth;
//----------------------------------------------------------
var
  iScale: Integer;  // iWidth, iHeight,
begin
  if Assigned(FInitialMap) then begin
    iScale:= GetScale();
    if iScale=0 then
      exit;

    FWidth := Round( FInitialMap.Width * (mapx_GetScale(FInitialMap)/iScale) );
    FHeight:= Round(FWidth/FInitialKoef);

    FNotUpdate:= true;
    ed_Width .Text:= AsString(FWidth);
    ed_Height.Text:= AsString(FHeight);
    FNotUpdate:= false;
  end;
end;

//----------------------------------------------------------
procedure TDlgSaveMapToFile.ed_HeightChange(Sender: TObject);
//----------------------------------------------------------
begin
  ed_Size.Text:= AsString(TruncFloat(
                          (AsInteger(ed_Height.Text)/1024)*
                          (AsInteger(ed_Width.Text )/1024)*
                          3));

  if FNotUpdate then
    exit;

  if Sender = ed_Width then begin
    FWidth:= AsInteger(ed_Width.Text);
    if FWidth=0 then
      exit;

    FNotUpdate:= true;
    FHeight:= Round(FWidth/FInitialKoef);
    ed_Height.Text:= AsString(FHeight);

    ed_scale.Text:= AsString( Round( FInitialMap.Width / FWidth * mapx_GetScale(FInitialMap) ) );
    FNotUpdate:= false;
  end else

  if Sender = ed_Height then begin
    FHeight:= AsInteger(ed_Height.Text);
    if FHeight=0 then
      exit;

    FNotUpdate:= true;
    FWidth:= Round(FHeight*FInitialKoef);
    ed_Width.Text:= AsString(FWidth);

    ed_scale.Text:= AsString( Round( FInitialMap.Width / FWidth *  mapx_GetScale(FInitialMap) ) );
    FNotUpdate:= false;
  end else

  if Sender = ed_Scale then begin
    UpdateWidth;
  end;
end;

//----------------------------------------------------------
procedure TDlgSaveMapToFile.ed_ScaleCloseUp(Sender: TObject);
//----------------------------------------------------------
begin
  inherited;
  UpdateWidth;

  if AsInteger(ed_Scale.Text) <> mapx_GetScale(FInitialMap) then
    rg_Scale.ItemIndex:= 1;
end;


//-------------------------------------------------------------------
procedure TDlgSaveMapToFile.btn_RectClick(Sender: TObject);
//-------------------------------------------------------------------
begin
  WindowState:= wsMinimized;
  PostEvent(WE_MAP_GET_PRINT_RECT);
end;

(*//-------------------------------------------------------------------
procedure TDlgSaveMapToFile.GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean);
//-------------------------------------------------------------------
var
  fScreenP1X, fScreenP1Y, fScreenP2X, fScreenP2Y: single;
begin
  inherited;

  case aMsg of
    WE_MAP_RECT_DRAWN: begin
      FblRect:= aParams.ExtractBLRect();

      FInitialMap.ConvertCoord(fScreenP1X, fScreenP1Y, FblRect.TopLeft.L, FblRect.TopLeft.B, miMapToScreen);
      FInitialMap.ConvertCoord(fScreenP2X, fScreenP2Y, FblRect.BottomRight.L, FblRect.BottomRight.B, miMapToScreen);

      FWidth := Round(fScreenP2X-fScreenP1X);
      FHeight:= Round(fScreenP2Y-fScreenP1Y);

      FInitialKoef:= FWidth/FHeight;

      WindowState:= wsNormal;

      DrawRegionOnMapUserLayer();
      FDrawed:= true;

      UpdateWidth;
    end;
  end;
end;
*)

//----------------------------------------------------------
procedure TDlgSaveMapToFile.act_OkExecute(Sender: TObject);
//----------------------------------------------------------
begin
  FDir:= IncludeTrailingBackslash(GetTempFileDir) +'PARTS';
  ForceDirectories(FDir);
  ClearDir(FDir);

  if not DirectoryExists(FDir) then begin
    ErrDlg('�� ������ ������� �������: '+FDir);
    ModalResult:= mrNone;
    exit;
  end;

  if FileExists(ed_FileName.text) then begin
    if not ConfirmDlg('��������� ���� ��� ����������, ������������?') then begin
      ModalResult:= mrNone;
      exit;
    end;

    if not DeleteFile(ed_FileName.text) then begin
      ErrDlg('�� ������ ������������ ��������� ����, �������� �� ������������ ������ ����������');
      ModalResult:= mrNone;
      exit;
    end;
  end;


  if MIF_ExportMap then begin
    if ConfirmDlg('������� ��������. �������?') then
      ShellExec(ed_FileName.FileName, '');
    Close;
  end else
    MsgDlg('�������������� ���� �� �������');
end;


// -------------------------------------------------------------------
function TDlgSaveMapToFile.MIF_ExportMap;
// -------------------------------------------------------------------
const
  MAX_MAPX_LENGTH = 2000;
var
  i, iWidth, iHeight: Integer;
  dkoef: double;
begin
  i:= 1;

  Result:= false;

  if Max(FWidth, FHeight)>MAX_MAPX_LENGTH then begin
    i:= Max(FWidth, FHeight) div MAX_MAPX_LENGTH;
    Inc(i);
  end;

  while not Result do
  begin
    Result:= ExportMap (i);

    Inc(i);

    if i>=100 then begin
      ErrDlg('�� ������� �������������� �����');
      break;
    end;
  end;

  Dec(i);

  if Result then
    Result:= JoinFiles(ed_FileName.text, i);


  FInitielPanel.Align:= alClient;
  FInitialMap.Align:= alClient;
  mapx_SetBounds(FInitialMap, FInitBLRect);
end;


// -------------------------------------------------------------------
function  TDlgSaveMapToFile.ExportMap;
// -------------------------------------------------------------------

  // ------------------------------------
  function GetBLBottomRight(): TBLPoint;
  // ------------------------------------
  var
    oXYPointRes, oXYPointTL, oXYPointBR: TXYPoint;
    iZone: Integer;
  begin
    iZone:= geo_Get6ZoneBL(FBLRect.TopLeft);

    oXYPointTL:= geo_BL_to_XY (FBLRect.TopLeft, 1, iZone);
    oXYPointBR:= geo_BL_to_XY (FBLRect.BottomRight, 1, iZone);

    oXYPointRes:= oXYPointTL;

    oXYPointRes.X:= oXYPointRes.X - Abs((oXYPointBR.X-oXYPointTL.X))/aPieceCount;
    oXYPointRes.Y:= oXYPointRes.Y + Abs((oXYPointBR.Y-oXYPointTL.Y))/aPieceCount;

    Result:= geo_XY_to_BL (oXYPointRes, 1, iZone);
  end;


  // ------------------------------------
  function DoExport(ai, aj, aWidth, aHeight: Integer): boolean;
  // ------------------------------------
  var
    sImgFileName: string;
    iOldWidth, iOldHeight: Integer;
    dSize: double;
  begin
    Result:= false;

    if aPieceCount=1 then begin
      sImgFileName:= FDir + '\all_map.bmp'; // ed_FileName.Text;
    end else
      sImgFileName:= FDir + '\map'+ Format('%d--%d.bmp', [aI, aJ]);

    UpdateZoom;

    try
      ForceDirectories(ExtractFileDir(sImgFileName));
      FInitialMap.ExportMap (sImgFileName, miFormatBmp);
      Result:= true;
      if Result and (aPieceCount=1) then
        MoveFile(sImgFileName, ReplaceStr(ed_FileName.Text, '"', ''));
    except
      Result:= false;
    end;
  end;

var
  i, j: Integer;
  iShiftWidth, iShiftHeight, iWidth, iHeight: Integer;
  blBottomRight: TBLPoint;
  rect: TBLRect;

  label cicle_exit;
begin
  Result:= true;

  if geo_Eq(FblRect, NULL_RECT) then
    FblRect := FInitBLRect;

  CursorHourGlass;


  //-------------------------
  if aPieceCount>1 then begin
    //-------------------------
    // ������������� ��������� ������ ����� ��� � ��������� ����������.
    FInitielPanel.Align:= alNone;
    FInitielPanel.Width := 30000;
    FInitielPanel.Height:= 30000;

    FInitialMap.Align:= alNone;
    FInitialMap.Top := 0;
    FInitialMap.Left:= 0;
    //-------------------------

    FInitialMap.Width  := Round(FWidth/aPieceCount);
    FInitialMap.Height := Round(FHeight/aPieceCount);
    FInitialMap.Refresh;

    rect:= FblRect;
    blBottomRight:= GetBLBottomRight();
    rect.BottomRight:= blBottomRight;
  end else
    rect:= FblRect;

  mapx_SetBounds (FInitialMap, rect);
  FInitialMap.Refresh;

  UpdateZoom;


  PB.Position:= 0; PB.Max:= Sqr(aPieceCount);
  for i := 0 to aPieceCount - 1 do
    for j := 0 to aPieceCount - 1 do
    begin
      FInitialMap.Pan(j*FInitialMap.Width, -i*FInitialMap.Height);
//      FInitialMap.Refresh;

      Result:= DoExport(i, j, iWidth, iHeight);

      if not Result then
        goto cicle_exit;

      FInitialMap.Pan(-j*FInitialMap.Width, i*FInitialMap.Height);
//      FInitialMap.Refresh;
      PB.Position:= PB.Position + 1;
    end;

cicle_exit:


  CursorDefault;
end;



function TDlgSaveMapToFile.JoinFiles (aImgFileName: string; aPieceCount: Integer): boolean;
var
  i, j: Integer;
  sBatFileName, sCommand, sCommandOem, sTmp, sImgFileName, sResTmpFileName: string;
  s1, s2: string;
begin
  if aPieceCount=1 then begin
    Result:= true;
    exit;
  end;

  CursorHourGlass;

  Result:= false;

//  sCommand:= Format('cd %s', [FDir]) ;
//  sCommand:= sCommand + CRLF;

  sResTmpFileName:= FDir +'\'+ ExtractFileName(GetTempFileName_('bmp'));

  sCommand:= sCommand + '"'+IncludeTrailingBackslash(GetApplicationDir)+'bmp_join\bmp_join.exe"';
  sTmp:= '';

  for i := 0 to aPieceCount - 1 do
  begin
    for j := 0 to aPieceCount - 1 do
    begin
      sImgFileName:= Format(' "%s\map%d--%d.bmp" ', [FDir, I, J]);
      sTmp:= sTmp + sImgFileName;
    end;

    sCommand:= sCommand+' ( '+sTmp+'+append -monitor -regard-warnings ) ';
    sTmp:= '';
  end;

  sCommand:= sCommand+ ' -append -monitor -regard-warnings "' + sResTmpFileName+ '"';

//  sCommand:= sCommand+ CRLF+ 'pause';

//  s1:= ConvertAnsiToOem(sCommand);

  tmp_StrList.Text:= sCommand;
  sBatFileName:= FDir+'\run.bat';
  tmp_StrList.SaveToFile(sBatFileName);

  DeleteFile(sResTmpFileName);
  RunApp(sBatFileName, '');

  Sleep(5000);
  Result:= FileExists(sResTmpFileName);

//  ShowMessage( sDir + sResTmpFileName + '  -  ' + AsString(Result) );

  if Result then
    MoveFile(sResTmpFileName, ReplaceStr(aImgFileName, '"', ''));

  Result:= FileExists(ReplaceStr(aImgFileName, '"', ''));

//  ShowMessage( aImgFileName + '  -  ' + AsString(Result) );

  if Result then begin
    ClearDir(FDir);
    RemoveDir(FDir);
//    DeleteFile(sBatFileName);
//    DeleteFiles(FDir+'\map*.bmp');
  end;

  CursorDefault;
end;



// -------------------------------------------------------------------
procedure  TDlgSaveMapToFile.UpdateZoom();
// -------------------------------------------------------------------
begin
  lb_Action.Caption:= Format('1 �� = %f ��', [mapx_GetScale(FInitialMap)]);
end;


end.
