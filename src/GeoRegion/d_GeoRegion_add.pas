unit d_GeoRegion_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,

 u_db,
  u_func,
  u_func_msg,
  u_dlg,

  u_types,
  u_classes,

  u_const_str,
  u_const_db,

  dm_GeoRegion,

  d_Wizard_add_with_Inspector, cxPropertiesStore, ComCtrls
  ;

type
  Tdlg_GeoRegion_add = class(Tdlg_Wizard_add_with_Inspector)

    ActionList2: TActionList;

    act_Add: TAction;
    act_Del: TAction;

    procedure _ActionMenu(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

  private
    FID,
    FFolderID
    //FPropertyID,
    //FFolderID
    : integer;


    procedure Append;

  public
    class function ExecDlg (aFolderID: integer=0): integer;
  end;

//==================================================================
implementation  {$R *.DFM}
//==================================================================

//-------------------------------------------------------------------
class function Tdlg_GeoRegion_add.ExecDlg;
//-------------------------------------------------------------------
begin
 // Screen.Cursor:=crHourGlass;

  with Tdlg_GeoRegion_add.Create(Application) do
  try
    FFolderID:=aFolderID;
    ed_Name_.Text:=dmGeoRegion.GetNewName;

 //   if (aPropertyID = 0) then
//      aPropertyID:=dmProperty.GetNearestID(aBLPoint);

  //  ed_Name_.Text := dmPMP.GetNewName;

  //  FFolderID  :=aFolderID;

//    row_Property.Text:=dmProperty.GetNameByID (aPropertyID);
  //  row_Folder.Text  :=dmFolder.GetPath (FFolderID);


    if ShowModal=mrOk then
      Result:=FID
    else
      Result:=0;

  finally
    Free;
  end;
end;


//--------------------------------------------------------------------
procedure Tdlg_GeoRegion_add.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

//////  SetActionName (STR_DLG_ADD_PMP);

  //lb_Action.Caption:= '�������� �����';

  SetDefaultSize();

end;


//----------------------------------------------------------------------
procedure Tdlg_GeoRegion_add.FormDestroy(Sender: TObject);
//----------------------------------------------------------------------
begin

  inherited;
end;

//--------------------------------------------------------------------
procedure Tdlg_GeoRegion_add.Append;
//--------------------------------------------------------------------
var rec: TdmGeoRegionAddRec;
begin
  FillChar(rec, SizeOf(rec), 0);

  rec.Name:=ed_Name_.Text;
  rec.FolderID:=FFolderID; 

  FID:=dmGeoRegion.Add (rec);

end;

//-------------------------------------------------------------------
procedure Tdlg_GeoRegion_add._ActionMenu(Sender: TObject);
//-------------------------------------------------------------------
//var LinkEnd1_ID, LinkEnd2_ID: integer;
begin
  if Sender=act_Ok then
    Append ;

end;


end.
