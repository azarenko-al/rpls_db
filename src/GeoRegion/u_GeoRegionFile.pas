unit u_GeoRegionFile;

interface
{$I Mapx.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Db,
  ADODB, Variants,

  u_assert,


  MapXLib_TLB,
  u_geo, 
  u_dlg,
  u_db,
  u_files,
  u_func,

  u_MapX,
  u_MapX_lib
;



type
  TGeoRegionFile = class(TObject)
  private
    FGeoRegionFileName: string;
    FMap: TmiMap;

    function Map: TmiMap;

    procedure SetFileName(aFileName: string = '');
  public
    constructor Create;
    destructor Destroy; override;

    function GetPolyPointsFromMap(aGeoRegionName: string; var aBlPointArr:
        TBLPointArrayF): boolean;

    function GetRegionCodeByMap(aBLPoint: TBLPoint; var aCountry: string; var
        aRegionCode: integer): boolean;
  end;


implementation


constructor TGeoRegionFile.Create;
begin
  inherited;

  FGeoRegionFileName:=GetApplicationDir() + 'data\region.tab';

  if not FileExists(FGeoRegionFileName) then
    ShowMessage('not FileExist: '+ FGeoRegionFileName);
end;


procedure TGeoRegionFile.SetFileName(aFileName: string = '');
begin
  if aFileName='' then
    aFileName := GetApplicationDir() + 'data\region.tab';

//  if not FileExists(aFileName) then
 //   ShowMessage('not FileExist: '+ FGeoRegionFileName);
end;



function TGeoRegionFile.Map: TmiMap;
begin
  if not Assigned(FMap) then
    FMap:= TmiMap.Create;

  Result := FMap;
end;


destructor TGeoRegionFile.Destroy;
begin
  if Assigned(FMap) then
    FreeAndNil(FMap);

  inherited;
end;


//-------------------------------------------------------------------
function TGeoRegionFile.GetPolyPointsFromMap(aGeoRegionName: string; var
    aBlPointArr: TBLPointArrayF): boolean;
//-------------------------------------------------------------------
var
 // oMap: TMiMap;
  vFeatures: CMapXFeatures;
  vFeature: CMapXFeature;
  vParts: CMapxParts;
  iCount, i: Integer;
begin
//  oMap:= TMiMap.Create;
 // SetLength(aBLPointArr, 0);
  Result:= false;

  try
    if not Map().OpenFile(FGeoRegionFileName) then
      Exit;

    vFeatures:= Map().VLayer.Search(Format('��������="%s"', [aGeoRegionName]), EmptyParam);
    iCount:= vFeatures.Count;
    if iCount<1 then
      exit;


      vFeature:= vFeatures.Item[1];
      vParts:= vFeature.Parts;

      iCount:= vParts.Item[1].Count;   


    aBLPointArr.Count:=iCount;
//    SetLength(aBLPointArr, iCount);

    for I := 1 to iCount do
    begin

        aBLPointArr.Items[i-1].B:= vParts.Item[1].Item[i].Y;
        aBLPointArr.Items[i-1].L:= vParts.Item[1].Item[i].X;

    end;

  except
   // exit;
  end;

  Result:= true;

 // oMap.CloseFile;
 // oMap.Free;
end;

//-----------------------------------------------------------------------
function TGeoRegionFile.GetRegionCodeByMap(aBLPoint: TBLPoint; var aCountry:
    string; var aRegionCode: integer): boolean;
//-----------------------------------------------------------------------
var
  vFeature  : CMapXFeature;
  vFeatures : CMapXFeatures;
  vSelection: CMapXSelection;

  vRowValues: CMapXRowValues;
  vPt       : CMapXPoint;

  iCount    : integer;
  v1,v2     : variant;
begin
  Result:=False;

  if not Map().Active then
    if not Map().OpenFile (FGeoRegionFileName) then
    begin
      ErrDlg_FileNotExists (FGeoRegionFileName);
      ErrDlg ('�� ������ ������� ����� ��������. ��������� �� ������������� � ���������� "BIN\DATA" ���������. - '+FGeoRegionFileName);

     // FMap.Free;
    //  FMap:=Nil;

      Exit;
    end;

  {
  if not Assigned(FMap) then
  begin
    FMap:= TmiMap.Create;

   // sFileName:=GetApplicationDir() + 'data\region.tab';

    if not FMap.OpenFile (FGeoRegionFileName) then
    begin
      ErrDlg_FileNotExists (FGeoRegionFileName);
      ErrDlg ('�� ������ ������� ����� ��������. ��������� �� ������������� � ���������� "BIN\DATA" ���������. - '+FGeoRegionFileName);

     // FMap.Free;
    //  FMap:=Nil;

      Exit;
    end;
  end;
  }

  vPt := CoPoint.Create;
  vPt.Set_(aBLPoint.L, aBLPoint.B);
                  

    vFeatures:= FMap.VLayer.SearchAtPoint(vPt, EmptyParam);



  iCount:= vFeatures.Count;

  if iCount>0 then

    try

        vFeature:=vFeatures.Item[1];
        vRowValues:=Map().VDataset.RowValues[vFeature];

        aCountry    := AsString (vRowValues.Item['Country'].Value);
        aRegionCode := AsInteger(vRowValues.Item['Code'].Value);


      Result:=True;
    except
      Result:=False;
    end;

end;

end.

