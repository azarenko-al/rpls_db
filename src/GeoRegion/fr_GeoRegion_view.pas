unit fr_GeoRegion_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, rxPlacemnt, ActnList, Menus,
  ComCtrls, ExtCtrls,

  dm_User_Security,

  fr_DBInspector_Container,

  u_func,
  u_reg,
  u_dlg,

  u_const_db,

  u_const_str,
  u_types,

  fr_View_base, cxPropertiesStore, dxBar, cxBarEditItem, cxClasses,
  cxLookAndFeels, dxSkinsCore, dxSkinsDefaultPainters, dxSkinsdxBarPainter,
  cxCheckBox, cxButtonEdit, ImgList

  ;

type
  Tframe_GeoRegion_View = class(Tframe_View_Base)
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    Ffrm_DBInspector: Tframe_DBInspector_Container;

//    FID: integer;

  public
    procedure View (aID: integer; aGUID: string); override;
    
  end;


//==================================================================
implementation {$R *.dfm}
//==================================================================

//--------------------------------------------------------------------
procedure Tframe_GeoRegion_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_GEO_REGION;
  TableName:=TBL_GEOREGION;

  pn_Main.Align:=alClient;

  CreateChildForm(Tframe_DBInspector_Container, Ffrm_DBInspector, pn_Main);

 // Ffrm_DBInspector:=Tframe_DBInspector.CreateChildForm(pn_Main);
  Ffrm_DBInspector.PrepareViewForObject (OBJ_GEO_REGION);
end;

//--------------------------------------------------------------------
procedure Tframe_GeoRegion_View.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
//  Ffrm_DBInspector.Free;

  inherited;
end;

//--------------------------------------------------------------------
procedure Tframe_GeoRegion_View.View(aID: integer; aGUID: string);
//--------------------------------------------------------------------
var
  bReadOnly: Boolean;
begin
  inherited; // View(aID);

  Ffrm_DBInspector.View(aID);


  bReadOnly:=dmUser_Security.Lib_Edit_ReadOnly();

  Ffrm_DBInspector.SetReadOnly(bReadOnly);

end;



end.

