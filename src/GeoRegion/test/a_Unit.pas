unit a_Unit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,

  u_Geo,
  u_db,
  u_reg,
  //u_func_app,
  u_dlg,

  u_types,
  u_const,
  u_const_msg,
  u_const_DB,

//  fr_Explorer,

  //dm_Act_Region,

  dm_georegion,

 // fr_Browser,
  f_Log,

 // dm_Main_res,
  dm_Main,
  dm_Main,


   StdCtrls, rxPlacemnt, ComCtrls, ExtCtrls, ToolWin;

type
  TForm988 = class(TForm)
    FormPlacement1: TFormPlacement;
    PageControl1: TPageControl;
    ts_LinkLine: TTabSheet;
    pn_Browser: TPanel;
    ToolBar1: TToolBar;
    Splitter1: TSplitter;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    iID: integer;

   { Fframe_Explorer : Tframe_Explorer;

    Fframe_Browser : Tframe_Browser;
}
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form988: TForm988;


implementation  {$R *.DFM}

//uses dm_Act_Map_Engine;



procedure TForm988.FormCreate(Sender: TObject);
begin
 // InitRegistry(REGISTRY_ROOT);

//  Tfrm_Log.CreateForm();

 pn_Browser.Align:=alClient;

   gl_Reg:=TRegStore_Create(REGISTRY_ROOT);

  Tfrm_Log.CreateForm();

  // -------------------------------------------------------------------
  TdmMain.Init;
 // IShellFactory_Init (dmMain);
  // -------------------------------------------------------------------

  dmMain.OpenDlg;


  dmGeoregion.UpdateBounds;


{
  Fframe_Explorer := Tframe_Explorer.CreateChildForm ( ts_LinkLine);
  Fframe_Explorer.SetViewObjects([otRegion]);
  Fframe_Explorer.RegPath:='LinkLine';
  Fframe_Explorer.Load;

  Fframe_Browser := Tframe_Browser.CreateChildForm( pn_Browser);

  Fframe_Explorer.OnNodeFocused:=Fframe_Browser.ViewObjectByPIDL;

 // dmAct_Map_Engine.Enabled:=False;

//  dmAct_Project.LoadLastProject;

  pn_Browser.Align:=alClient;}

end;


procedure TForm988.FormDestroy(Sender: TObject);
begin
{
  Fframe_Explorer.Free;
  Fframe_Browser.Free;}

end;

end.
