unit dm_act_GeoRegion;

interface

uses
  Classes, Menus,  Forms, ActnList,

  dm_User_Security,

  u_func,

  I_Object,

  dm_act_Base,
                     
  u_types,

  fr_GeoRegion_view,

  dm_GeoRegion

  ;

type
  TdmAct_GeoRegion = class(TdmAct_Base)
    act_Edit: TAction;
    act_MoveToCountryDirs: TAction;
    procedure DataModuleCreate(Sender: TObject);

  private
    function CheckActionsEnable: Boolean;
   // FBLPoint: TBLPoint;

//    procedure AddByPos (aPropertyID: integer; aBLPoint: TBLPoint);
  //  procedure Add (aParentID: integer);

  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm  (aOwnerForm: TForm): TForm; override;

  //  procedure DoAction (Sender: TObject);

    function ItemDel(aID: integer; aName: string): boolean; override;
//    function ItemAdd (aFolderID: integer): integer; override;
  public
    class procedure Init;
  end;


var
  dmAct_GeoRegion: TdmAct_GeoRegion;

//==================================================================
implementation  {$R *.dfm}
//==================================================================


//--------------------------------------------------------------------
class procedure TdmAct_GeoRegion.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_GeoRegion) then
    dmAct_GeoRegion:= TdmAct_GeoRegion.Create(Application);
end;

//--------------------------------------------------------------------
procedure TdmAct_GeoRegion.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_GEO_REGION;

//  act_Move.Caption:=STR_MOVE;
 /// act_MoveToCountryDirs.Caption:= '������������ ������� �� ������� (������� �����)';

 // SetActionsExecuteProc ([act_MoveToCountryDirs], DoAction);
end;


//--------------------------------------------------------------------
function TdmAct_GeoRegion.ItemDel(aID: integer; aName: string): boolean;
//--------------------------------------------------------------------
begin
  Result:= dmGeoRegion.Del (aID);
end;

//--------------------------------------------------------------------
function TdmAct_GeoRegion.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:= Tframe_GeoRegion_View.Create (aOwnerForm);
end;


// ---------------------------------------------------------------
function TdmAct_GeoRegion.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
  Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];

  SetActionsEnabled1( [
          act_Del_,
          act_Del_list
//          act_Copy,

       //   act_Cell_Add
        //  act_Export_MDB,
      ],

   Result );
  //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );
         
end;


//--------------------------------------------------------------------
procedure TdmAct_GeoRegion.GetPopupMenu;
//--------------------------------------------------------------------
var
  bEnable: Boolean;
begin
  bEnable:=CheckActionsEnable();


  case aMenuType of
    mtFolder: begin
              //  AddFolderMenu_Create (aPopupMenu);
              //  AddFolderPopupMenu   (aPopupMenu);
            //    AddMenuItem (aPopupMenu, act_MoveToCountryDirs);
              end;
    mtItem:   begin
              //  AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Del_);
              end;
    mtList:
                AddMenuItem (aPopupMenu, act_Del_list);
  end;


end;



begin

end.

  (*



//--------------------------------------------------------------------
function TdmAct_GeoRegion.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
{
  Result:=Tdlg_PMP_add.ExecDlg (aFolderID, FBLPoint) ;
  if Result>0 then begin
    dmMapEngine.CreateObject (otPMP, Result);
//    PostEvent (WE_MAP_ENGINE_CREATE_OBJECT, Integer(otSite), Result);
    dmAct_Map.MapRefresh;
  end;
  }
end;


//--------------------------------------------------------------------
procedure TdmAct_GeoRegion.DoAction (Sender: TObject);
//--------------------------------------------------------------------
begin
 // if Sender = act_MoveToCountryDirs then
 //   dmGeoRegion.DistributeByFolders();
end;

//--------------------------------------------------------------------
procedure TdmAct_GeoRegion.Add (aParentID: integer);
//--------------------------------------------------------------------
begin
{
  FBLPoint:=NULL_POINT;
  inherited Add (aPropertyID);
  }
end;

