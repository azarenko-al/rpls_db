unit dm_GeoRegion;

interface
{$I ver.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Db,
  ADODB, Variants,

  MapXLib_TLB,

  dm_Main,

  u_GeoRegionFile,

  dm_Object_base,

  u_geo, 
  u_dlg,
  u_db,
  u_files,
  u_func,

  u_MapX,
  u_MapX_lib,

  u_types,
  u_const_str,
  u_const_db

  ;


type
  //-------------------------------------------------------------------
  TdmGeoRegionAddRec = record
  //-------------------------------------------------------------------
    Name:      string;
    FolderID:  integer;
  end;

  //-------------------------------------------------------------------
  TdmGeoRegion = class(TdmObject_base)
    qry_Temp: TADOQuery;
    qry_Temp2: TADOQuery;
    q_GeoRegions: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private                                                            

    FGeoRegionFile: TGeoRegionFile;

  public
    function Add (aRec: TdmGeoRegionAddRec): integer;
    function GetID_ByBLPoint(aBLPoint: TBLPoint): integer;

    function Find(aCountry: string; aRegionCode: Integer): integer;

    function GetPolyPointsFromMap(aGeoRegionName: string; var aBlPointArr: TBLPointArrayF): boolean;

  end;


function dmGeoRegion: TdmGeoRegion;


//====================================================================
implementation

uses dm_Onega_db_data;  {$R *.dfm}
//====================================================================

var
  FdmGeoRegion: TdmGeoRegion;


// ------------------------------------------------------------------
function dmGeoRegion: TdmGeoRegion;
// ------------------------------------------------------------------
begin
  if not Assigned(FdmGeoRegion) then
    Application.CreateForm(TdmGeoRegion, FdmGeoRegion);

//    FdmGeoRegion := TdmGeoRegion.Create(Application);

  Result := FdmGeoRegion;
end;

//-------------------------------------------------------------------
procedure TdmGeoRegion.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  TableName:=TBL_GEORegion;
  ObjectName:=OBJ_GEO_Region;
//  DisplayName:='������';

  FGeoRegionFile := TGeoRegionFile.Create();

end;


procedure TdmGeoRegion.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FGeoRegionFile);

  inherited;
end;


//-------------------------------------------------------------------
function TdmGeoRegion.GetPolyPointsFromMap(aGeoRegionName: string; var aBlPointArr:
    TBLPointArrayF): boolean;
//-------------------------------------------------------------------
begin
  Result := FGeoRegionFile.GetPolyPointsFromMap(aGeoRegionName, aBlPointArr);
end;

//-------------------------------------------------------
function  TdmGeoRegion.Add (aRec: TdmGeoRegionAddRec): integer;
//-------------------------------------------------------
begin
  with aRec do
    Result:= gl_DB.AddRecordID (TBL_GEORegion,
                  [
                   db_Par(FLD_FOLDER_ID, IIF(FolderID>0,FolderID,NULL)),
                   db_Par(FLD_NAME,      Name)
                   ] );
end;


//-----------------------------------------------------------------------
function TdmGeoRegion.GetID_ByBLPoint(aBLPoint: TBLPoint): integer;
//-----------------------------------------------------------------------
var sCountry: string;       //,sFileName
    iRegionCode: integer;
begin
  try
    if FGeoRegionFile.GetRegionCodeByMap (aBLPoint, sCountry, iRegionCode) then
      Result := Find(sCountry, iRegionCode)
    else
      Result := -1;
  finally

  end;

end;

//-----------------------------------------------------------------------
function TdmGeoRegion.Find(aCountry: string; aRegionCode: Integer): integer;
//-----------------------------------------------------------------------
var
  v: Variant;
begin
  if not q_GeoRegions.Active then
    dmOnega_DB_data.OpenQuery (q_GeoRegions, 'SELECT * FROM '+ TBL_GEOREGION);

  Assert(q_GeoRegions.RecordCount>0, 'q_GeoRegions.RecordCount>0');


  v := q_GeoRegions.Lookup(FLD_COUNTRY+';'+FLD_CODE,
      VarArrayOf([aCountry,aRegionCode]), FLD_ID);

  Result := AsInteger(v);
end;


begin

end.


