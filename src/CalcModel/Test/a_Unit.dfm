object frm_Test_CalcModel: Tfrm_Test_CalcModel
  Left = 230
  Top = 224
  Width = 622
  Height = 331
  Caption = 'frm_Test_CalcModel'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 614
    Height = 280
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 185
      Top = 5
      Height = 270
    end
    object pc_Main: TPageControl
      Left = 5
      Top = 5
      Width = 180
      Height = 270
      ActivePage = ts_Explorer
      Align = alLeft
      TabOrder = 0
      object ts_Explorer: TTabSheet
        Caption = 'ts_Explorer'
      end
    end
    object pn_Browser: TPanel
      Left = 188
      Top = 5
      Width = 421
      Height = 270
      Align = alClient
      Caption = 'pn_Browser'
      TabOrder = 1
    end
  end
  object FormPlacement1: TFormPlacement
    Left = 84
    Top = 6
  end
end
