program test_CalcModel_Setup;

uses
  Forms,
  d_CalcModel in '..\src\CalcModel\d_CalcModel.pas' {dlg_CalcModel_Setup},
  d_CalcModel_add in '..\src\CalcModel\d_CalcModel_add.pas' {dlg_CalcModel_add},
  dm_Act_CalcModel in '..\src\CalcModel\dm_Act_CalcModel.pas' {dmAct_CalcModel: TDataModule},
  dm_CalcModel in '..\src\CalcModel\dm_CalcModel.pas' {dmCalcModel: TDataModule},
  fr_CalcModel_models in '..\src\CalcModel\fr_CalcModel_models.pas' {frame_CalcModel_models},
  u_Formula_Editor in '..\src\CalcModel\u_Formula_Editor.pas',
  fr_CalcModel_model_base in '..\src\CalcModel\fr_CalcModel_model_base.pas' {frame_CalcModel_model_base},
  fr_CalcModel_Cost231 in '..\src\CalcModel\fr_CalcModel_Cost231.pas' {frame_CalcModel_Cost231},
  fr_CalcModel_HATA567 in '..\src\CalcModel\fr_CalcModel_HATA567.pas' {frame_CalcModel_HATA567},
  fr_CalcModel_Det in '..\src\CalcModel\fr_CalcModel_Det.pas' {frame_CalcModel_Det},
  a_Unit in 'calc_model\a_Unit.pas' {Form7},
  dm_Main in '..\src\DataModules\dm_Main.pas' {dmMain: TDataModule},
  u_CalcModel_det_const in '..\src\CalcModel\u_CalcModel_det_const.pas',
  fr_DBInspector in '..\src\Explorer\fr_DBInspector.pas' {frame_DBInspector},
  fr_CalcModel_view in '..\src\CalcModel\fr_CalcModel_view.pas' {frame_CalcModel_View},
  dm_act_Base in '..\src\Common\dm_act_Base.pas' {dmAct_Base: TDataModule};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TdmMain, dmMain);
  Application.CreateForm(TForm7, Form7);
  Application.CreateForm(Tframe_CalcModel_View, frame_CalcModel_View);
  Application.Run;
end.
