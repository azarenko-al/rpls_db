unit test_CalcModel_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 22.01.2008 20:12:15 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\RPLS_DB\src\CalcModel\Test\test_CalcModel.tlb (1)
// LIBID: {E340209C-6D27-494A-964F-241294AA3081}
// LCID: 0
// Helpfile: 
// HelpString: test_CalcModel Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  test_CalcModelMajorVersion = 1;
  test_CalcModelMinorVersion = 0;

  LIBID_test_CalcModel: TGUID = '{E340209C-6D27-494A-964F-241294AA3081}';

  IID_ICalcModel: TGUID = '{EE10A25E-4FB1-4186-A118-46AABDAB8658}';
  CLASS_CalcModel: TGUID = '{3F439664-843F-4ED7-92D7-5872E543A276}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ICalcModel = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  CalcModel = ICalcModel;


// *********************************************************************//
// Interface: ICalcModel
// Flags:     (256) OleAutomation
// GUID:      {EE10A25E-4FB1-4186-A118-46AABDAB8658}
// *********************************************************************//
  ICalcModel = interface(IUnknown)
    ['{EE10A25E-4FB1-4186-A118-46AABDAB8658}']
  end;

// *********************************************************************//
// The Class CoCalcModel provides a Create and CreateRemote method to          
// create instances of the default interface ICalcModel exposed by              
// the CoClass CalcModel. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCalcModel = class
    class function Create: ICalcModel;
    class function CreateRemote(const MachineName: string): ICalcModel;
  end;

implementation

uses ComObj;

class function CoCalcModel.Create: ICalcModel;
begin
  Result := CreateComObject(CLASS_CalcModel) as ICalcModel;
end;

class function CoCalcModel.CreateRemote(const MachineName: string): ICalcModel;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CalcModel) as ICalcModel;
end;

end.
