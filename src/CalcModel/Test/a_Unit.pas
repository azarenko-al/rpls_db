unit a_Unit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,


  dm_Main,

  fr_CalcModel_View,

  fr_Explorer,
  fr_Browser,

  u_func,
  u_db,
  u_const_msg,
  u_const_db,
  u_dlg,
  u_reg,

  u_types,
  u_const,

  StdCtrls, ComCtrls, rxPlacemnt, ExtCtrls;

type
  Tfrm_Test_CalcModel = class(TForm)
    FormPlacement1: TFormPlacement;
    Panel1: TPanel;
    Splitter1: TSplitter;
    pc_Main: TPageControl;
    ts_Explorer: TTabSheet;
    pn_Browser: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    id: integer;
    Fframe_Browser : Tframe_Browser;

    { Private declarations }
  public
    { Public declarations }
    Fframe_CalcModel : Tframe_Explorer;
  end;

var
  frm_Test_CalcModel: Tfrm_Test_CalcModel;


implementation
{$R *.DFM}



procedure Tfrm_Test_CalcModel.FormCreate(Sender: TObject);
begin
  gl_Reg:=TRegStore_Create(REGISTRY_ROOT);

  TdmMain.Init;
  dmMain.OpenFromReg;


{  Fframe_Browser:=Tframe_Browser.CreateChildForm( pn_Browser);

  Fframe_CalcModel:=Tframe_Explorer.CreateChildForm( ts_Explorer);
  Fframe_CalcModel.SetViewObjects([otCalcModel]);
  Fframe_CalcModel.RegPath:='CalcModel';
  Fframe_CalcModel.Load;

  Fframe_CalcModel.OnNodeFocused:=Fframe_Browser.ViewObjectByPIDL;
}

end;


procedure Tfrm_Test_CalcModel.FormDestroy(Sender: TObject);
begin

{  Fframe_Browser.Free;
  Fframe_CalcModel.Free;
}
end;


end.