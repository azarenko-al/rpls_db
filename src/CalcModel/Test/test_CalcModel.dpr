program test_CalcModel;

uses
  ShareMem,

  Forms,
  dm_Act_CalcModel in '..\dm_act_CalcModel.pas' {dmAct_CalcModel: TDataModule},
  fr_CalcModel_view in '..\fr_CalcModel_view.pas' {frame_CalcModel_View},
  a_Unit in 'a_Unit.pas' {frm_Test_CalcModel},
  dm_Object_base in '..\..\Common\dm_Object_base.pas' {dmObject_base: TDataModule};

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(Tfrm_Test_CalcModel, frm_Test_CalcModel);
  Application.Run;
end.
