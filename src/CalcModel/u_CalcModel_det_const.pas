unit u_CalcModel_det_const;

interface


(*const
  CRLF = #13+#10;
*)

const

//
//  MODEL_DET_TEMPLATE_XML11 =
//   '<?xml version="1.0" encoding="windows-1251"?> '+
////   '<?xml:stylesheet type="text/xsl" ?> '+   //href=""
//   '<Document> '+  
//   '  <GROUP name="�������� ���������"> '+
//
//   '      <PARAM id="02" value="1" name="HefBS=" > '+
//   '        <ITEM value="1" name="HefBS=H_BS;"/> '+
//   '        <ITEM value="2" name="HefBS=(H_BS+H�����BS)-Hcp"/> '+
//   '      </PARAM> '+
//
//   '      <PARAM id="01" value="1" name="����������� ����������� ������ ������� ��"> '+
//   '        <ITEM value="1" name="����������� HefBS �� ������ ��������� ������� 1..10 �� �� BS"/> '+
//   '        <ITEM value="2" name="����������� HefBS � ����� �� ���������� �� ������ ����� ������"/> '+
//   '      </PARAM> '+
//
//   '      <PARAM id="11" value="2" name="��������� ������ ��� �� ��" comment="��� ��������� ������ ��� �� �������� ����������, �������������� ��������� ������ ��� � ����������� �� �������� �������� �� �� �� ��������� ������ �� ������� �., �� "> '+
//   '        <ITEM value="1" name="�������"/> '+
//   '        <ITEM value="2" name="�� ���()"/> '+
//   '      </PARAM> '+
//
//   '      <PARAM id="12" value="2" name="����������� ��������� ������ ��� �� ��"/> '+
////   '      <PARAM/> '+
//   '  </GROUP> '+
//   '</Document>';
//


  MODEL_DET_TEMPLATE_XML=
   '<?xml version="1.0" encoding="windows-1251"?> '+
//   '<?xml:stylesheet type="text/xsl" ?> '+   //href=""
   '<Document> '+  
   '  <GROUP name="�������� ���������"> '+

   '      <PARAM id="02" value="1" name="HefBS=" > '+
   '        <ITEM value="1" name="HefBS=H_BS;"/> '+
   '        <ITEM value="2" name="HefBS=(H_BS+H�����BS)-Hcp"/> '+
   '      </PARAM> '+

   '      <PARAM id="01" value="1" name="����������� ����������� ������ ������� ��"> '+
   '        <ITEM value="1" name="����������� HefBS �� ������ ��������� ������� 1..10 �� �� BS"/> '+
   '        <ITEM value="2" name="����������� HefBS � ����� �� ���������� �� ������ ����� ������"/> '+
   '      </PARAM> '+
   '      <PARAM id="11" value="2" name="��������� ������ ��� �� ��" comment="��� ��������� ������ ��� �� �������� ����������, �������������� ��������� ������ ��� � ����������� �� �������� �������� �� �� �� ��������� ������ �� ������� �., �� "> '+
   '        <ITEM value="1" name="�������"/> '+
   '        <ITEM value="2" name="�� ���()"/> '+
   '      </PARAM> '+

   '      <PARAM id="12" value="2" name="����������� ��������� ������ ��� �� ��"/> '+
//   '      <PARAM/> '+
   '  </GROUP> '+
   '  <GROUP name="���������� �������� ��������� � �� [dB]"> '+
   '      <PARAM id="13_1" value="67" name="�����"/> '+
   '      <PARAM id="13_2" value="27" name="� ����"/> '+
   '      <PARAM id="13_3" value="37" name="� ������"/> '+
   '      <PARAM id="13_4" value="27" name="� ��"/> '+
   '  </GROUP> '+
   '  <GROUP name="��� �������� ����������"> '+
   '      <PARAM id="03" value="3" name="������ ��������� ��� ������� ������ W��=W��" > '+
   '        <ITEM value="1" name="W�� �� ���� ��� �������� ���������" /> '+
   '        <ITEM value="2" name="W��=(122+k0) + (20+k1)Lg(R[��]) - 20Lg(Dv[c�])" /> '+
   '        <ITEM value="3" name="W��=W�� �� ����������� )" /> '+
   '      </PARAM> '+
   '      <GROUP name="���: W��=(122+k0) + (20+k1)Lg(R[��]) - 20Lg(Dv[c�])"> '+
      '      <PARAM id="closed_k0" value="0" name="k0=-40...40" /> '+
      '      <PARAM id="closed_k1" value="0" name="k1=-10...10" /> '+
   '      </GROUP> '+
   '      <PARAM id="14" value="2" name="��� �������� ���������� ��� ������� W�� �� ���� ��� �������� ��������� �������������� �������� Wp ���������������������� R" '+
   '                              comment="��� �������� ���������� ��� ������� W�� �� ���� ��� �������� ��������� �������������� �������� Wp ���������������������� R"> '+
   '        <ITEM value="1" name="������� 1/R"/> '+
   '        <ITEM value="2" name="�� ���(1/R)"/> '+
   '      </PARAM> '+
   '      <PARAM id="15" value="2" name="����������� �������� Wp ���������������������� R" '+
   '                               comment="��� �������� ���������� ��� ������� W�� �� ���� ��� �������� ��������� �������������� �������� Wp ���������������������� R:"/> '+
   '  </GROUP> '+
   '  <GROUP name="��� �������� ����������"> '+
   '      <PARAM id="04" value="1" name="������ ��������� ��� ������� ������ W��=W��" > '+
   '        <ITEM value="1" name="W�� �� ���� ��� �������� ���������"/> '+
   '        <ITEM value="2" name="W��=(122+k0) + (20+k1)Lg(R[��]) - 20Lg(Dv[c�])"/> '+
   '        <ITEM value="3" name="W��=W�� �� ����������� "/> '+
   '      </PARAM> '+
   '      <GROUP name="���: W��=(122+k0) + (20+k1)Lg(R[��]) - 20Lg(Dv[c�])"> '+
      '      <PARAM id="open_k0" value="0" name="k0=-40...40" /> '+
      '      <PARAM id="open_k1" value="0" name="k1=-10...10" /> '+
   '      </GROUP> '+

   '      <PARAM id="06"  value="69.55" name="������ ����������� ���� ��� ���������� ����������"/> '+
   '      <PARAM id="07"  value="2"     name="����� ���� �������"/> '+
   '      <PARAM id="05"  value="1" name="������ Wp"> '+
   '        <ITEM value="1" name="������ Wp �� ���� �����������, �������� � ��"/> '+
   '        <ITEM value="2" name="������ Wp �� ���������� ������� ����� ��"/> '+
   '      </PARAM> '+
   '  </GROUP> '+
   '  <GROUP name="��� ���� ����������"> '+
   '      <PARAM id="08"  value="2" name="������ ������ � ����"> '+
   '        <ITEM value="1" name="��������� ������ ���� ��� ����"/> '+
   '        <ITEM value="2" name="�� ��������� ������ ����, � ��������� �������� ��������� � ����"/> '+
   '        <ITEM value="3" name="�� ��������� ������ ����, � ������ ������ � ���� �� ���������"/> '+
   '      </PARAM> '+
   '      <PARAM id="09"  value="1" name="������ ������ � ������"> '+
   '        <ITEM value="1" name="��������� ������ ������ ��� ����"/> '+
   '        <ITEM value="2" name="�� ��������� ������ ������, � ��������� �������� ��������� � ������"/> '+
   '        <ITEM value="3" name="�� ��������� ������ ������, � ������ ������ � ������ �� ���������"/> '+
   '      </PARAM> '+
   '      <PARAM id="10" value="3" name="������ ������ � �� - ������� ���������� ����" > '+
   '        <ITEM value="1" name="��������� ������ �� ��� ����"/> '+
   '        <ITEM value="2" name="�� ��������� ������ ��, � ��������� �������� ��������� � ��"/> '+
   '        <ITEM value="3" name="�� ��������� ������ ��, � ������ ������ � �� �� ���������"/> '+
   '      </PARAM> '+
   '  </GROUP> '+
   '  <GROUP name="��� ������ ���������"> '+
   '      <PARAM id="16" value="50"  name="������� ������ ����� � ������ [m]"/> '+
   '      <PARAM id="19" value="50"  name="������� ������ ����� � �� [m]"/> '+
   '      <PARAM id="22" value="25"  name="������� ���������� ����� ��������� � ���� [m]"/> '+
   '      <GROUP name="�������� ����� [dB]"> '+
   '          <PARAM id="18" value="23"  name="� ������=0...40"' +
   '          comment="[0�40] �������� ����� ��� ������� ������ � ������ �� ������� ���������, ��. "/> '+
   '          <PARAM id="24" value="0"   name="� ����=0...20" '+
   '          comment="[0�20] �������� ����� ��� ������� ������ � ���� �� ������� ���������, ��. "/>'+

   '          <PARAM id="21" value="5"   name="� ��=0...20"' +
   '          comment="[0�20] �������� ����� ��� ������� ������ � �� �� ������� ���������, ��. "/> '+
   '      </GROUP> '+
   '      <GROUP name="����������, �� ������� ������������... [km]"> '+
   '         <PARAM id="17" value="35" name="������� ������ ����� � ������ "  comment="0-������ ������ ����� ���������� �� ����� ��������� ���� � �����"/> '+
   '         <PARAM id="20" value="1"  name="������� ������ � ��"            comment="0-������ ������ ����� ���������� �� ����� ��������� ���� � ��"/> '+
   '         <PARAM id="23" value="35" name="������� ���������� ����� ��������� � ����"   comment="0- ������ ���������� ����� ��������� ���������� �� ����� ��������� ���� � ���"/> '+
   '      </GROUP> '+
   '  </GROUP> '+
   '  <GROUP name="��� �����"> '+
   '      <PARAM id="25"  value="8"  name="����� ����������� �������"  type="int" /> '+
   '      <PARAM id="26"  value="2"  name="���������� ���������� ���������� �� �������, ����� ������� [dB]"/> '+
   '      <PARAM id="Kwp" value="1"  name="�����.������������� ����������� ���������� (0..1)"/> '+

   '      <PARAM id="27" value="0.2" name="������������������ ���������� [km]"/> '+
   '      <PARAM id="28" value="0" name="����� ������� �������� ������"> '+
   '        <ITEM value="0" name="������� �����"/> '+
   '        <ITEM value="1" name="������������ �����"/> '+
   '      </PARAM> '+
   '      <PARAM id="281" value="45" name="���������� �������� W� [db] = 0...75"/> '+    //MaxWp
   '      <GROUP name="���������� r2 ��� �����"> '+
   '          <PARAM id="29" value="0.05"  name="���� ������� � ���� [km]"/> '+
   '          <PARAM id="30" value="0.025" name="���� ������� � ������ [km]"/> '+
   '          <PARAM id="31" value="0.05"  name="���� ������� � �� [km]"/> '+
   '      </GROUP> '+
   '      </GROUP> '+
   '</Document>';


implementation

end.



