unit fr_CalcModel_model_base;

interface

uses

  f_Custom,

  u_classes,

  u_const_db,
  u_db,

  dm_CalcModel,
//  dm_CalcModel,

  u_calcparams,

  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList,  ToolWin, ComCtrls,
  
  cxPropertiesStore, DB, ADODB, dxBar, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxTextEdit, dxSkinsdxBarPainter, cxControls, cxClasses,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridCustomView, cxGrid

  ;

type
  Tframe_CalcModel_model_base = class(Tfrm_Custom)
    qry_Params: TADOQuery;
    ds_Params: TDataSource;
    ActionList1: TActionList;
    act_Save: TAction;
    PageControl1: TPageControl;
    TabSheet_Setup: TTabSheet;
    TabSheet_Params: TTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    cxGrid1DBTableView1value_: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarButton1: TdxBarButton;
    procedure FormCreate(Sender: TObject);
    procedure act_SaveExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);

  protected
    FID: integer;
    FParams: TcmParamList;

    procedure OpenParams ();

  public
    function Modified: boolean; virtual; abstract;

    procedure LoadFromParams (aParams: TcmParamList); virtual; abstract;
    procedure SaveToParams   (aParams: TcmParamList); virtual; abstract;

    procedure View (aID: integer);

  end;


//==================================================================
implementation

uses dm_Onega_db_data; {$R *.DFM}
//==================================================================


procedure Tframe_CalcModel_model_base.FormCreate(Sender: TObject);
begin
  inherited;
  PageControl1.Align:=alClient;
  PageControl1.ActivePageIndex:=0;



  FParams:=TcmParamList.Create;

////////  Fframe_ClutterModel_Items:=Tframe_ClutterModel_Items.CreateChildForm ( ts_Clutters);
end;


procedure Tframe_CalcModel_model_base.FormDestroy(Sender: TObject);
begin
//  Fframe_ClutterModel_Items.Free;
  FParams.Free;

  inherited;
end;



//-------------------------------------------------------
procedure Tframe_CalcModel_model_base.View (aID: integer);
//-------------------------------------------------------
begin
  FID:=aID;

  dmCalcModel.LoadParams1 (FID, FParams);
  OpenParams ();

  LoadFromParams (FParams);
end;

//------------------------------------------------------
procedure Tframe_CalcModel_model_base.OpenParams ();
//------------------------------------------------------
begin
  dmOnega_DB_data.OpenQuery(qry_Params,
               'SELECT * FROM ' + TBL_CalcModelParams +
               ' WHERE calc_model_id=:id  ORDER BY name',
               [FLD_ID, FID ]);
end;



procedure Tframe_CalcModel_model_base.act_SaveExecute(Sender: TObject);
begin
  if Sender=act_Save then
  begin
    SaveToParams (FParams);
    dmCalcModel.SaveParams (FID, FParams);

    OpenParams ();
  end;

end;



procedure Tframe_CalcModel_model_base.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
begin
  act_Save.Enabled:=Modified;
end;


end.
