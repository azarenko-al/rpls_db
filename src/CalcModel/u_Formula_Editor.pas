unit u_Formula_Editor;

interface
uses Windows, Messages, extctrls, SysUtils, Classes, Graphics, Controls, ADODB,
    Variants, cxPropertiesStore, cxControls, cxContainer, cxEdit, cxTextEdit,

    cxCurrencyEdit,


    u_calcparams,

    //  u_XML,
    u_const_db,
    u_classes,

    u_Func_arrays,
    u_func,
    u_db

     ;

type
  TFormulaEditorGroup = class;

  //--------------------------------------------------------
  TFormulaEditor = class(TList)
  //--------------------------------------------------------
  private
    FFormulaEditorGroup: TFormulaEditorGroup;

    FParent : TWinControl;
    FCanvas : TCanvas;     // ����. ��� ����������� ������� �������
    FParamCount : integer; // ���-�� ������������� (����������)
    FStartLeft: integer;   // ����������� �� ������ ����

    function GetItem      (Index: integer): TControl;
    function GetControlWidth (Index: integer): integer;

    procedure DoOnEditEnter  (Sender: TObject);
    procedure DoOnEditChange (Sender: TObject);
    procedure AlignControls;
    procedure SetFormulaText (Value: string);
    function  GetParams: TDoubleArray;
    procedure SetParams (Values: TDoubleArray);
    function  GetParamStr: string;
    procedure SetParamStr (Value: string);
  public
    Enabled : boolean;

    constructor Create (AParent: TWinControl; ACanvas: TCanvas; AFormulaEditorGroup: TFormulaEditorGroup);

    procedure Clear; override;

//    procedure SaveValuesToIniFile (aFileName: string; aSectionName: string);

    property Controls [Index:integer]: TControl read GetItem;
    property FormulaText: string write SetFormulaText;
    property Params: TDoubleArray read GetParams write SetParams;
    property ParamCount: integer read FParamCount; // ���-�� ������������� (����������)
      // �������� ���������� � �������: 32;55;22;66;
    property ParamStr: string read GetParamStr write SetParamStr;
     // ������������ �������
     // format: <34.22> log 10 fsdfs <123.322>
  end;

  //-------------------------------------------------------
  TFormulaEditorGroup = class(TList)
  //-------------------------------------------------------
  private
    function GetItem(Index:integer): TFormulaEditor;
  public
    Modified : boolean;

    ActiveGroupIndex  : integer; // ����� ��������� ������
    ActiveFormulaIndex: integer; // ����� ��������� �������

    constructor Create;

    procedure Clear; override;

    procedure SaveToParams (aParamList: TcmParamList);
    procedure LoadFromParams (aParamList: TcmParamList);

    property  Items [Index:integer]: TFormulaEditor read GetItem; default;
  end;


const
  FORMULA_PARAM_COLOR = clRed; // ���� �����.
  TAG_PARAM  = 0;
  TAG_CONST  = 1;
  TAG_HIDDEN = 2; // ������� ��������

  // HATA 567 -----------------------------------------
  HATA_URBAN_LU            = '<69.55> + <26.16> * log(f) - <13.82> * log(Hb) - a(Hm) + (<44.9> - <6.55> * log(Hb)) * log(d)';
  HATA_URBAN_SMALL_aHm     = '(<1.1> log(f) - <0.7>) Hm - (<1.56> * log(f) - <0.8>)';
  HATA_URBAN_LARGE_aHm_200 = '<8.29> * (log(<1.54>* Hm))^2 - <1.1>  [200]';  // for f<=200
  HATA_URBAN_LARGE_aHm_400 = '<3.2> * (log(<11.75>* Hm))^2 - <4.97> [400]'; // for f>=400

  HATA_SUBURBAN_Lsu     = 'Lu - <2> * (log(f/<28>))^2 - <5.4>';

  HATA_RURAL_Quasi_open = 'Lu - <4.78> * (log(f))^2 + <18.33> * log(f) - <35.94>';
  HATA_RURAL_Open_area  = 'Lu - <4.78> * (log(f))^2 + <18.33> * log(f) - <40.94>';
  //---------------------------------------------------

  // COST 231 -----------------------------------------
  COST231_LU   = '<46.3> + <33.9> * lof(f) - <13.82> * log(Hb) - a(Hm) + '+
                '(<44.9> - <6.55> * log(Hb)) * log(d) + Cm';
  COST231_aHm  = '(<1.1> * log(f) - <0.7>) * Hm - (<1.56> * log(f) - <0.8>)';
  COST231_Cm0  = '<0>';
  COST231_Cm3  = '<3>';
  //---------------------------------------------------


  STR_FORMAT = '%.2d';


implementation

//--------------------------------------------------------
// TFormulaEditor
//--------------------------------------------------------
constructor TFormulaEditor.Create;
begin
  inherited Create;
  FParent:=AParent;
  FCanvas:=ACanvas;
  FStartLeft:=50;
  FFormulaEditorGroup := AFormulaEditorGroup;
  Enabled:=true;
end;


function TFormulaEditor.GetItem (Index: integer): TControl;
begin
  Result:=TControl (inherited Items[Index]);
end;

procedure TFormulaEditor.DoOnEditEnter (Sender: TObject);
begin // �������� ��� ������
  PostMessage ((Sender as TWinControl).Handle, EM_SETSEL, 0, 100);
end;

procedure TFormulaEditor.DoOnEditChange (Sender: TObject);
begin
  FFormulaEditorGroup.Modified := True;
  AlignControls; // setup new control width
end;

procedure TFormulaEditor.Clear;
var i:integer;
begin
  for i:=0 to Count-1 do Controls[i].Destroy; inherited;
end;

//-----------------------------------------------------------
function TFormulaEditor.GetControlWidth (Index: integer): integer;
//-----------------------------------------------------------
var str: string; tag:integer;
begin
  if (Controls[Index] is TcxCurrencyEdit) then str:=(Controls[Index] as TcxCurrencyEdit).Text else
//  if (Controls[Index] is TdxEdit)         then str:=(Controls[Index] as TdxEdit).Text  else
  if (Controls[Index] is TPanel)          then str:=(Controls[Index] as TPanel).Caption;

  tag:=Controls[Index].Tag;
  if tag=TAG_HIDDEN
    then Result:=0
    else Result:=FCanvas.TextWidth (str + IIF(tag=TAG_PARAM,'W',''))
end;

//-----------------------------------------------------------
procedure TFormulaEditor.SetFormulaText (Value: string);
//-----------------------------------------------------------

    //-----------------------------------------------------
    function MakeParamField (AText: string; AIsVisible: boolean): TcxCurrencyEdit;
    //-----------------------------------------------------
    begin
        Result:=TcxCurrencyEdit.Create(FParent);
        Result.Text:=AText;

        Result.Properties.DisplayFormat:=',0.00';

//        Result.Alignment:=taCenter;
        Result.ParentColor:=true;
     ///////////   Result.Style.BorderStyle:=xbsNone;

      //  xbsNone

        Result.Properties.OnChange:=DoOnEditChange;
        Result.OnEnter:=DoOnEditEnter;
      //  Result.Font.Color:=FORMULA_PARAM_COLOR;

        Result.Visible:=IIF(AIsVisible, true, false);
        Result.Tag    :=IIF(AIsVisible, TAG_PARAM, TAG_HIDDEN);
        Inc(FParamCount);
    end;

    //-----------------------------------------------------
    function MakeConstField (AText: string): TPanel; //TdxEdit;
    //-----------------------------------------------------
    begin
        Result:=TPanel.Create(FParent);
        Result.Caption:=AText;
        Result.BevelOuter:=bvNone;
        Result.ParentColor:=true;
        Result.Top:=1;
        Result.Height:=FParent.ClientHeight-2;
        Result.Tag:=TAG_CONST;
    end;
    //-----------------------------------------------------

var control: TControl;
    i:integer;
    str : string;
    parts: TStrArray;
    isOpenParam,isHiddenParam: boolean; // �����������
begin // format: <34.22>;log 10 fsdfs;<123.322>
  Clear; FParamCount:=0;

  // include ';' before and after '<' and '>'
  Value:=StringReplace (Value,'<',';<',[rfReplaceAll]);
  Value:=StringReplace (Value,'>','>;',[rfReplaceAll]);
  Value:=StringReplace (Value,'[',';[',[rfReplaceAll]); // hidden parameters
  Value:=StringReplace (Value,']','];',[rfReplaceAll]);

  parts:=StringToStrArray (Value,';');
  for i:=0 to High(parts) do parts[i]:=Trim(parts[i]);

  for i:=0 to High(parts) do
  begin
    str:=parts[i]; if str='' then Continue;
    isOpenParam  :=(Length(str)>1) and (str[1]='<');
    isHiddenParam:=(Length(str)>1) and (str[1]='[');
    if (Length(str)>1) and (str[1] in ['<','[']) then str:=Copy(str,2,Length(str)-2);

    if isOpenParam   then Add(MakeParamField (str, true)) else
    if isHiddenParam then Add(MakeParamField (str, false))
                     else Add(MakeConstField (str));
  end;

  AlignControls();
end;

//-----------------------------------------------------------
procedure TFormulaEditor.AlignControls;
//-----------------------------------------------------------
var i,len:integer;
begin
  len:=FStartLeft;

  for i:=0 to Count-1 do
  with Controls[i] do begin
    Parent:=FParent;

    Left:=len;  Width:=GetControlWidth(i);
    Inc(len,Width);
  end;
end;

//-----------------------------------------------------------
function TFormulaEditor.GetParams: TDoubleArray;
//-----------------------------------------------------------
var i,j: integer;
begin
  SetLength (Result,FParamCount); j:=0;
  for i:=0 to Count-1 do
  if Controls[i].Tag in [TAG_PARAM,TAG_HIDDEN] then begin
    Result[j]:=(Controls[i] as TcxCurrencyEdit).Value;  Inc(j);
  end;
end;

//-----------------------------------------------------------
procedure TFormulaEditor.SetParams (Values: TDoubleArray);
//-----------------------------------------------------------
var i,j: integer;
begin
  j:=0;

  if High(Values)+1 = ParamCount then
  begin
    for i:=0 to Count-1 do
    if Controls[i].Tag in [TAG_PARAM,TAG_HIDDEN] then
      begin (Controls[i] as TcxCurrencyEdit).Value:=Values[j]; Inc(j); end;
  end;

  AlignControls;
end;

//-------------------------------------------------------
function TFormulaEditor.GetParamStr: string;
//-------------------------------------------------------
begin
  Result:=DoubleArrayToString (Params,';');
end;

procedure TFormulaEditor.SetParamStr (Value: string);
begin
  Params:=StringToDoubleArray(Value,';');
end;

//-------------------------------------------------------
//  TFormulaEditorGroup
//-------------------------------------------------------
constructor TFormulaEditorGroup.Create;
begin
  inherited;
  Modified := False;
end;

function TFormulaEditorGroup.GetItem(Index:integer): TFormulaEditor;
begin
  Result:=(inherited Items[Index]);
end;

procedure TFormulaEditorGroup.Clear;
var i:integer;
begin
  for i:=0 to Count-1 do Items[i].Destroy;
end;

{
//---------------------------------------------------------
procedure TFormulaEditorGroup.LoadFromDB (aModelID: integer);
//---------------------------------------------------------
begin
    dmCalcModel.LoadParams (aModelID);
    LoadFromParams (dmCalcModel.ModelParams);

end;

//---------------------------------------------------------
procedure TFormulaEditorGroup.SaveToDB (aModelID: integer);
//---------------------------------------------------------
begin
  SaveToParams (dmCalcModel.ModelParams);
  dmCalcModel.SaveParams (aModelID);
end;
}
//---------------------------------------------------------
procedure TFormulaEditorGroup.LoadFromParams(aParamList: TcmParamList);
//---------------------------------------------------------
var i, j, k: integer;
    str: string;
begin
  ActiveGroupIndex   := Round(aParamList.ValueByName ('01'));
  ActiveFormulaIndex := Round(aParamList.ValueByName ('02'));

  k:=3;
  for i := 0 to Count - 1 do
  begin
    str := '';
    for j := 1 to Items[i].ParamCount do
    begin
      str := str + FloatToStr (aParamList.ValueByName (Format('%.2d', [k]) )) + ';';
      Inc(k);
    end;
    system.delete(str, length(str), 1);
    Items[i].ParamStr:=str;
  end;


  Modified := False;

end;


//---------------------------------------------------------
procedure TFormulaEditorGroup.SaveToParams(aParamList: TcmParamList);
//---------------------------------------------------------
var i, j, iPos: integer;
  arr: array of double;
  str: string;
begin
  aParamList.Clear;

  SetLength (arr, 1000);
  arr[0]:=ActiveGroupIndex;
  arr[1]:=ActiveFormulaIndex;

  begin
    aParamList.AddItem ('01', ActiveGroupIndex);
    aParamList.AddItem ('02', ActiveFormulaIndex);
    iPos:=2;

    for i := 0 to Count - 1 do
      for j := 0 to Items[i].ParamCount - 1 do
      begin
        Inc(iPos);
        aParamList.AddItem (Format('%.2d',[iPos]),
                         Items[i].Params[j] );
      end;
  end;
end;


end.