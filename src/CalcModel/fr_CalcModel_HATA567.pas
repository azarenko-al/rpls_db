unit fr_CalcModel_HATA567;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms, 
  
  
  StdCtrls, ExtCtrls,   
  

  u_calcparams,

  u_Formula_Editor,
  u_func,
  

  fr_CalcModel_model_base;


type
  Tframe_CalcModel_HATA567 = class(Tframe_CalcModel_model_base)
    ScrollBox1: TScrollBox;
    pn_Model: TPanel;
    Panel2: TPanel;
    rb_Large_city_400: TRadioButton;
    rb_Medium_city: TRadioButton;
    rb_Large_city_200: TRadioButton;
    Panel25: TPanel;
    Panel14: TPanel;
    pn_Hata_Urban_small: TPanel;
    Panel16: TPanel;
    Panel17: TPanel;
    pn_Hata_Urban_large_400: TPanel;
    Panel19: TPanel;
    Panel20: TPanel;
    pn_Hata_Urban_large_200: TPanel;
    Panel22: TPanel;
    Panel6: TPanel;
    Panel44: TPanel;
    pn_Hata_suburban: TPanel;
    Panel13: TPanel;
    Panel4: TPanel;
    Panel47: TPanel;
    Bevel3: TBevel;
    Bevel4: TBevel;
    rb_Rural: TRadioButton;
    Panel45: TPanel;
    Bevel2: TBevel;
    rb_Suburban: TRadioButton;
    Panel43: TPanel;
    Bevel1: TBevel;
    rb_Urban: TRadioButton;
    Panel46: TPanel;
    Panel50: TPanel;
    rb_Quasi_open: TRadioButton;
    rb_Open_Area: TRadioButton;
    Panel51: TPanel;
    pn_Hata_Rural_quasi_open: TPanel;
    Panel53: TPanel;
    Panel55: TPanel;
    pn_Hata_Rural_open_area: TPanel;
    Panel57: TPanel;
    Panel58: TPanel;
    pn_HATA_Urban_lu: TPanel;
    Panel24: TPanel;
    Memo1: TMemo;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FRadioModified: boolean;
    FBeginUpdate: boolean;
    FHataEditorList: TFormulaEditorGroup;

    procedure DrawFormulaEditors();
    procedure InitFormulas;

    procedure ChangeFormulaGroup (Sender: TObject);
    procedure ChangeFormula (Sender: TObject);

  public
    function Modified: boolean; override;

    procedure LoadFromParams (aParams: TcmParamList); override;
    procedure SaveToParams   (aParams: TcmParamList); override;

  end;

//===========================================================
implementation {$R *.DFM}
//===========================================================

const
  // INDEX использовать для сохранения 0
  INDEX_HATA_URBAN_LU            = 0;
  INDEX_HATA_URBAN_SMALL_aHm     = 1;
  INDEX_HATA_URBAN_LARGE_aHm_200 = 2;
  INDEX_HATA_URBAN_LARGE_aHm_400 = 3;

  INDEX_HATA_SUBURBAN_Lsu     = 4;

  INDEX_HATA_RURAL_Quasi_open = 5;
  INDEX_HATA_RURAL_Open_area  = 6;
//  STR_MODEL_NAME = 'hata567';



  procedure CheckButton (Sender: TRadioButton; AButtons: array of TRadioButton); forward;


//------------------------------------------------------
procedure Tframe_CalcModel_HATA567.FormCreate(Sender: TObject);
//------------------------------------------------------
begin
  inherited;

  FHataEditorList:=TFormulaEditorGroup.Create;
  InitFormulas();

  rb_Urban.OnClick:=ChangeFormulaGroup;
  rb_Suburban.OnClick:=ChangeFormulaGroup;
  rb_Rural.OnClick:=ChangeFormulaGroup;

  rb_Medium_city.OnClick:=ChangeFormula;
  rb_Large_city_200.OnClick:=ChangeFormula;
  rb_Large_city_400.OnClick:=ChangeFormula;

  rb_Quasi_open.OnClick:=ChangeFormula;
  rb_Open_Area.OnClick:=ChangeFormula;

  pn_Model.Align:=alCLient;
  ScrollBox1.Align:=alCLient;

  PageControl1.Align:=alCLient;

end;


procedure Tframe_CalcModel_HATA567.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FHataEditorList);

  inherited;
end;


//------------------------------------------------------
procedure Tframe_CalcModel_HATA567.InitFormulas;
//------------------------------------------------------
var ed: TFormulaEditor;
begin
  // URBAN
  ed:=TFormulaEditor.Create (pn_HATA_Urban_lu, Canvas, FHataEditorList);
  ed.FormulaText:=HATA_URBAN_LU;
  FHataEditorList.Add(ed);

  ed:=TFormulaEditor.Create (pn_Hata_Urban_small, Canvas, FHataEditorList);
  ed.FormulaText:=HATA_URBAN_SMALL_aHm;
  FHataEditorList.Add(ed);

  ed:=TFormulaEditor.Create (pn_Hata_Urban_large_200, Canvas, FHataEditorList);
  ed.FormulaText:=HATA_URBAN_LARGE_aHm_200;
  FHataEditorList.Add(ed);

  ed:=TFormulaEditor.Create (pn_Hata_Urban_large_400, Canvas, FHataEditorList);
  ed.FormulaText:=HATA_URBAN_LARGE_aHm_400;
  FHataEditorList.Add(ed);

  // SUBURBAN
  ed:=TFormulaEditor.Create (pn_Hata_suburban, Canvas, FHataEditorList);
  ed.FormulaText:=HATA_SUBURBAN_Lsu;
  FHataEditorList.Add(ed);

  // RURAL
  ed:=TFormulaEditor.Create (pn_Hata_Rural_quasi_open, Canvas, FHataEditorList);
  ed.FormulaText:=HATA_RURAL_Quasi_open;
  FHataEditorList.Add(ed);

  ed:=TFormulaEditor.Create (pn_Hata_Rural_open_area, Canvas, FHataEditorList);
  ed.FormulaText:=HATA_RURAL_Open_area;
  FHataEditorList.Add(ed);

  // INDEX использовать для сохранения 0
  FHataEditorList [INDEX_HATA_URBAN_LU].Enabled:=true;
  FHataEditorList [INDEX_HATA_URBAN_LARGE_aHm_400].Enabled:=false;
  DrawFormulaEditors;
end;



//--------------------------------------------
procedure Tframe_CalcModel_HATA567.LoadFromParams(aParams: TcmParamList);
//--------------------------------------------
begin
  FHataEditorList.LoadFromParams(aParams);

  FBeginUpdate:=True;
  FRadioModified:=False;

//////////////  rb_Medium_city.Checked:=true;
/////////////  rb_Quasi_open.Checked:=true;

  // установить CheckBoxes
  with FHataEditorList do
  begin
    case ActiveGroupIndex of
      0: ChangeFormulaGroup (rb_Urban);
      1: ChangeFormulaGroup (rb_Suburban);
      2: ChangeFormulaGroup (rb_Rural);
    end;

    case ActiveGroupIndex of
      0: case ActiveFormulaIndex of
           0: rb_Medium_city.Checked:=true;
           1: rb_Large_city_200.Checked:=true;
           2: rb_Large_city_400.Checked:=true;
         end;
      2: case ActiveFormulaIndex of
             0: rb_Quasi_open.Checked:=true;
             1: rb_Open_Area.Checked:=true;
         end;
    end;
  end;

  FBeginUpdate:=False;
end;

//------------------------------------------------------
procedure Tframe_CalcModel_HATA567.SaveToParams(aParams: TcmParamList);
//------------------------------------------------------
begin
  FHataEditorList.SaveToParams(aParams);
  FHataEditorList.Modified:=False; //True;
  FRadioModified:=False;
end;

//------------------------------------------------------
procedure Tframe_CalcModel_HATA567.ChangeFormula(Sender: TObject);
//------------------------------------------------------
begin
  if not FBeginUpdate then
    FRadioModified:=True;

  if Sender=rb_Medium_city    then FHataEditorList.ActiveFormulaIndex:=0;
  if Sender=rb_Large_city_200 then FHataEditorList.ActiveFormulaIndex:=1;
  if Sender=rb_Large_city_400 then FHataEditorList.ActiveFormulaIndex:=2;

  if Sender=rb_Quasi_open     then FHataEditorList.ActiveFormulaIndex:=0;
  if Sender=rb_Open_Area      then FHataEditorList.ActiveFormulaIndex:=1;

  DrawFormulaEditors();
end;

//------------------------------------------------------
procedure Tframe_CalcModel_HATA567.ChangeFormulaGroup(Sender: TObject);
//------------------------------------------------------
begin
  if not FBeginUpdate then
    FRadioModified:=True;

  if (Sender=rb_Urban) or (Sender=rb_Suburban)
   or (Sender=rb_Rural)
    then CheckButton ((Sender as TRadioButton), [rb_Urban,rb_Suburban,rb_Rural]);

  if Sender=rb_Urban    then FHataEditorList.ActiveGroupIndex:=0;
  if Sender=rb_Suburban then FHataEditorList.ActiveGroupIndex:=1;
  if Sender=rb_Rural    then FHataEditorList.ActiveGroupIndex:=2;

  DrawFormulaEditors();
end;


//------------------------------------------------------
procedure Tframe_CalcModel_HATA567.DrawFormulaEditors();
//------------------------------------------------------
var i: integer;
begin
  // set NOT ENABLED all except  first
  for i:=1 to FHataEditorList.Count-1 do FHataEditorList[i].Enabled:=false;

  if rb_Urban.Checked then begin
     FHataEditorList[INDEX_HATA_URBAN_SMALL_aHm].Enabled     := rb_Medium_city.Checked;
     FHataEditorList[INDEX_HATA_URBAN_LARGE_aHm_200].Enabled := rb_Large_city_200.Checked;
     FHataEditorList[INDEX_HATA_URBAN_LARGE_aHm_400].Enabled := rb_Large_city_400.Checked;
  end;
  pn_Hata_Urban_small.Color    := IIF(rb_Urban.Checked and rb_Medium_city.Checked,    clWindow,clBtnFace);
  pn_Hata_Urban_large_200.Color:= IIF(rb_Urban.Checked and rb_Large_city_200.Checked, clWindow,clBtnFace);
  pn_Hata_Urban_large_400.Color:= IIF(rb_Urban.Checked and rb_Large_city_400.Checked, clWindow,clBtnFace);

  //------------------------------------------------
  if rb_SubUrban.Checked then begin
    FHataEditorList[INDEX_HATA_SUBURBAN_Lsu].Enabled:=true;
  end;
  pn_Hata_suburban.Color:=IIF(rb_SubUrban.Checked, clWindow,clBtnFace);

  //------------------------------------------------
  if rb_Rural.Checked then begin
    FHataEditorList[INDEX_HATA_RURAL_Quasi_open].Enabled := rb_Quasi_open.Checked;
    FHataEditorList[INDEX_HATA_RURAL_Open_Area].Enabled  := rb_Open_Area.Checked;
  end;

  pn_Hata_Rural_quasi_open.Color:=IIF (rb_Rural.Checked and rb_Quasi_open.Checked, clWindow, clBtnFace);
  pn_Hata_Rural_open_area.Color :=IIF (rb_Rural.Checked and rb_Open_Area.Checked, clWindow, clBtnFace);

end;


function Tframe_CalcModel_HATA567.Modified: boolean;
begin
  Result:=FHataEditorList.Modified or FRadioModified;
end;




//--------------------------------------------
procedure CheckButton (Sender: TRadioButton; AButtons: array of TRadioButton);
//--------------------------------------------
var i: integer;
begin
   for i:=0 to High(AButtons) do
     if Sender<>AButtons[i] then
     begin AButtons[i].Checked:=false; AButtons[i].Font.Style:=[] end;

   Sender.Checked:=true;
   Sender.Font.Style:=[fsBold];
end;

end.