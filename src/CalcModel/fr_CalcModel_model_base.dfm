inherited frame_CalcModel_model_base: Tframe_CalcModel_model_base
  Left = 1082
  Top = 391
  Width = 609
  Height = 492
  Caption = 'frame_CalcModel_model_base'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    Width = 593
    Height = 69
  end
  object PageControl1: TPageControl [1]
    Left = 0
    Top = 97
    Width = 593
    Height = 176
    ActivePage = TabSheet_Setup
    Align = alTop
    Style = tsFlatButtons
    TabOrder = 1
    object TabSheet_Setup: TTabSheet
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
    end
    object TabSheet_Params: TTabSheet
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099
      ImageIndex = 1
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 200
        Height = 145
        Align = alLeft
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGrid1DBTableView1: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_Params
          DataController.Filter.MaxValueListCount = 1000
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnPopup.MaxDropDownItemCount = 12
          OptionsSelection.CellSelect = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsView.GroupByBox = False
          OptionsView.GroupFooters = gfVisibleWhenExpanded
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          object cxGrid1DBTableView1name: TcxGridDBColumn
            Caption = #1055#1072#1088#1072#1084#1077#1090#1088
            DataBinding.FieldName = 'name'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = False
            Options.Filtering = False
            Width = 77
          end
          object cxGrid1DBTableView1value_: TcxGridDBColumn
            Caption = #1047#1085#1072#1095#1077#1085#1080#1077
            DataBinding.FieldName = 'value_'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = False
            Options.Filtering = False
            Width = 63
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
  end
  inherited ActionList: TActionList
    Left = 48
  end
  object qry_Params: TADOQuery
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'id'
        DataType = ftString
        Size = 1
        Value = '5'
      end>
    SQL.Strings = (
      'select * from linkline where id=:id')
    Left = 188
    Top = 4
  end
  object ds_Params: TDataSource
    DataSet = qry_Params
    Left = 228
    Top = 4
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 12
    Top = 4
    object act_Save: TAction
      Caption = '&'#1057#1086#1093#1088#1072#1085#1080#1090#1100
      OnExecute = act_SaveExecute
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    NotDocking = [dsNone]
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 32
    Top = 304
    DockControlHeights = (
      0
      0
      28
      0)
    object dxBarManager1Bar1: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 2
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 733
      FloatTop = 876
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarButton1: TdxBarButton
      Action = act_Save
      Category = 0
    end
  end
end
