object frame_Inspector_base: Tframe_Inspector_base
  Left = 837
  Top = 540
  Width = 526
  Height = 235
  Caption = 'frame_Inspector_base'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object cxVerticalGrid1: TcxVerticalGrid
    Left = 0
    Top = 0
    Width = 518
    Height = 120
    Align = alTop
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = True
    OptionsView.RowHeaderWidth = 333
    TabOrder = 0
    OnEditValueChanged = cxVerticalGrid1EditValueChanged
    object cxVerticalGrid1CategoryRow1: TcxCategoryRow
    end
    object cxVerticalGrid1CategoryRow2: TcxCategoryRow
    end
    object cxVerticalGrid1EditorRow1: TcxEditorRow
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
    end
    object cxVerticalGrid1EditorRow2: TcxEditorRow
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
    end
  end
end
