unit dm_CalcModel;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms, Windows,  IniFiles, Dialogs,Variants,

  dm_Onega_DB_data,

  u_calcparams,

  dm_Main,
  dm_Object_base,

  I_CalcModel,

  u_db,
  u_func,
  u_classes,

  u_types,
  u_const_db,
  u_const
  ;

type


  //-------------------------------------------------------------------
  TdmCalcModel = class(TdmObject_base)
    ADOQuery1: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

  private
    FTempParams: TcmParamList;
//    function Copy__(aSourceID: integer; aName: string): integer;
  public
    function Add (aRec: TdmCalcModelAddRec): integer;

    function GetType (aID: integer): integer;
    function Del(aID: integer): integer;

    function GetK0_open_close (aID: integer; var aK0,aK0_open,aK0_closed: double): boolean;

    procedure SaveParams (aID: integer; aParams: TcmParamList);
    procedure AddDefaultParams (aID: integer; aType: integer);
    procedure LoadParams1(aID: integer; aParams: TcmParamList);

  end;


function dmCalcModel: TdmCalcModel;

{
var
  dmCalcModel_: TdmCalcModel_;

}
//================================================================
implementation   {$R *.DFM}
//================================================================

(*const
  SQL_SELECT_CALC_MODEL_PARAMS=
       'SELECT * FROM ' + TBL_CalcModelParams +
       ' WHERE calc_model_id=:id ORDER BY name';
*)
var
  FdmCalcModel: TdmCalcModel;



// ---------------------------------------------------------------
function dmCalcModel: TdmCalcModel;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmCalcModel) then
    Application.CreateForm(TdmCalcModel, FdmCalcModel);
//    FdmCalcModel := TdmCalcModel_.Create(Application);

  Result := FdmCalcModel;
end;



// ---------------------------------------------------------------
procedure TdmCalcModel.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;
  TableName := TBL_CALCMODEL;
  ObjectName  := OBJ_CALC_MODEL;

  FTempParams:=TcmParamList.Create;
end;


procedure TdmCalcModel.DataModuleDestroy(Sender: TObject);
begin
  FTempParams.Free;
  inherited;
end;

//------------------------------------------------------
procedure TdmCalcModel.SaveParams (aID: integer; aParams: TcmParamList);
//------------------------------------------------------
var i: integer;
begin

  gl_DB.ExecCommand ('DELETE FROM ' + TBL_CalcModelParams +
                     ' WHERE calc_model_id = :id',
                      [db_Par(FLD_ID, aID)]);

  with aParams do
  for i:=0 to Count-1 do
    gl_DB.AddRecord (TBL_CalcModelParams,
                    [db_Par(FLD_CALC_MODEL_ID, aID),
                     db_Par(FLD_NAME,   Items[i].Name),
                     db_Par(FLD_VALUE_,  Items[i].Value)
                    ]);
end;

//------------------------------------------------------
procedure TdmCalcModel.LoadParams1(aID: integer; aParams: TcmParamList);
//------------------------------------------------------
const
  SQL_SELECT_CALC_MODEL_PARAMS=
       'SELECT * FROM ' + TBL_CalcModelParams +
       ' WHERE calc_model_id=:id ORDER BY name';

var i: integer;
begin
  aParams.Clear;

  dmOnega_DB_data.OpenQuery(ADOQuery1,
                SQL_SELECT_CALC_MODEL_PARAMS,
                [FLD_ID, aID] );

 // db_View(ADOQuery1);


(*  db_OpenQuery (gl_DB.Query,
                SQL_SELECT_CALC_MODEL_PARAMS,
                [db_Par(FLD_ID, aID)] );
*)


  with ADOQuery1 do
    while not EOF do
    begin
      aParams.AddItem (FieldValues[FLD_NAME], FieldValues[FLD_VALUE_] );
      Next;
    end;
end;


function TdmCalcModel.GetType(aID: integer): integer;
begin
  Result:=GetIntFieldValue (aID, FLD_TYPE);
end;


const
  HATA567_PARAM_COUNT = 28;
  HATA567 : array[1..HATA567_PARAM_COUNT] of double =
      (0, 2,
       69.55, 26.16, 13.82, 44.9, 6.55,
       1.1, 0.7, 1.56, 0.8,
       8.29, 1.54, 1.1, 0, //200,
       3.2, 11.75, 4.97, 0, //400,
       2, 28, 5.4,
       4.78, 18.33, 35.94,
       4.78, 18.33, 40.94);

  COST231_PARAM_COUNT=13;
  COST231 : array[1..COST231_PARAM_COUNT] of double =
      (0, 0,
       46.3, 33.9, 13.82, 44.9, 6.55,
       1.1, 0.7, 1.56, 0.8,
       0,
       3);


  DET: array [0..54] of record
       Name: string; Value: double;
       Comment : string;
     end =
    (
      //;1 - (1-����������� HefBS �� ������ ��������� ������� 1..10 �� �� BS;
      //     2-����������� HefBS � ����� �� ���������� �� ������ ����� ������)
      //Param1=1
      (Name:'01'; Value:1; Comment:'1 - (1-����������� HefBS �� ������ ��������� ������� 1..10 �� �� BS;'),

     // ;===����������� ����������� ������ �������===
     // ;1 - (1- HefBS=H_BS;  2- HefBS=(H_BS+H�����BS)-Hcp)
     // Param2=1
      (Name:'02'; Value:1;),


     // ;===������ ��������� ��� ������� ������===
     // ;3 - (��� �������� ���������� ������ ��������� ��� ������� ������ W��=W��:
     //     1- W�� �� ���� ��� �������� ���������;
     //     2- W��=122+20lg(R[��]/Dv[c�];
     //     3- W��=W�� �� ����������� )
     // ;1
     // Param3=2
      (Name:'03'; Value: 2;),

     // ;1 - (��� �������� ���������� ������ ��������� ��� ������� ������ W��=W��:
     //      1- W�� �� ���� ��� �������� ���������;
     //      2- W��=122+20lg(R[��]/Dv[c�];
     //      3- W��=W�� �� ����������� )
     // ;1
     // Param4=2
      (Name:'04'; Value: 2;),


//      ;1 - (��� �������� ����������: 1-������ Wp �� ���� �����������, �������� � �� ;
       // 2-������ Wp �� ���������� ������� ����� ��
//      Param5=1
      (Name:'05'; Value:1;),

  //    ;69.55 - ������ ����������� ���� ��� ���������� ����������(�� ��������� 69.55)
   //   ;
     // Param6=69.55
      (Name:'06'; Value:69.55;),

     // ;2 - (��� �������� ����������: ����� ���� �������)
     // Param7=2
      (Name:'07'; Value:2;),

     // ;2 - (��� ���� ����������: 1- ��������� ������ ���� ��� ����;
     //    2- �� ��������� ������ ����, � ��������� �������� ��������� � ����; 3- �� ��������� ������ ����, � ������ ������ � ���� �� ���������)
     // Param8=2
      (Name:'08'; Value:1;),

     // ;3 - (��� ���� ����������: 1- ��������� ������ ������ ��� ����;2- �� ��������� ������ ������, � ��������� �������� ��������� � ������; 3- �� ��������� ������ ������, � ������ ������ � ������ �� ���������)
     // Param9=1
      (Name:'09'; Value:0;),

     // ;3 - (��� ���� ����������: 1- ��������� ������ ��� ��� ����;   2- �� ��������� ������ ���, � ��������� �������� ��������� � ���; 3- �� ��������� ������ ���, � ������ ������ � ��� �� ���������)
     // Param10=1
      (Name:'10'; Value:0;),

//      ;1 - (��������� ������ ��� �� ��: 1-�������; 2-�� ���()
 //     Param11=2
      (Name:'11'; Value:2;),

//      ;0.01 - (����������� ��������� ������ ��� �� ��)
 //     ;��� �. ��� ������� ����������
  //    Param12=0.5
      (Name:'12'; Value:0.3;),


   //   ;67.00 27.00 37.00 27.00 37.00 10.00 - (���������� �������� ��������� � ��, � ����, � ������, � ���,  � �����, � ���)
    //  Param13_1=67.00
  //    Param13_2=30.00
   //   Param13_3=37.00
 //     Param13_4=27.00
  //    Param13_5=37.00
   //   Param13_6=10.00
      (Name:'13_1'; Value:67;),
      (Name:'13_2'; Value:30;),
      (Name:'13_3'; Value:37;),
      (Name:'13_4'; Value:27;),
      (Name:'13_5'; Value:37;),
      (Name:'13_6'; Value:10;),

    //  ;2 - (1-������� 1/R; 2-�� ���(1/R):
    //    ��� �������� ���������� ��� ������� W�� �� ���� ��� �������� ��������� �������������� �������� Wp ���������������������� R
    //     Param14=2
      (Name:'14'; Value:2;),

   //   ;0.05 - (����������� �������� Wp ���������������������� R: ��� �������� ���������� ��� ������� W�� �� ���� ��� �������� ��������� �������������� �������� Wp ���������������������� R:
  //    ;����������� ���������
  //    Param15=0.05
      (Name:'15'; Value:0.05;),

 //     ;50.0 - ��� ������ ��������� ������� ������ ����� (� ������) � ������
 //     Param16=50.0
      (Name:'16'; Value:50;),

 //     ;35.00 - ��� ������ ��������� ����������(��), �� ������� ������������ ������� ������ ����� � ������; 0-������ ������ ����� ���������� �� ����� ��������� ���� � �����
 //     Param17=0
      (Name:'17'; Value:0;),

  //    ;23.0 - (�������� ����� ��� ������ ��������� � ������, ��)
   //   Param18=20.0
      (Name:'18'; Value:20;),

//      ;50.0 - ��� ������ ��������� ������� ������ ����� (� ������) � ���
 //     Param19=50.0
      (Name:'19'; Value:50;),

  //    ;1.00 - ��� ������ ��������� ����������(��), �� ������� ������������ ������� ������ � ���; 0-������ ������ ����� ���������� �� ����� ��������� ���� � ���
   //   Param20=0
      (Name:'20'; Value:0;),

   //   ;5.0 - (�������� ����� ��� ������ ��������� � ���, ��)
  //    Param21=5.0
      (Name:'21'; Value:5;),

   //   ;25.0 - ��� ������ ��������� ������� ���������� ����� ��������� (� ������) � ����
    //  Param22=25.0
      (Name:'22'; Value:25;),

//      ;35.00 - ��� ������ ��������� ����������(��), �� ������� ������������ ������� ���������� ����� ��������� � ����; 0- ������ ���������� ����� ��������� ���������� �� ����� ��������� ���� � ���
 //     Param23=35.00
      (Name:'23'; Value:35;),

  //    ;0.0 - (�������� ����� ��� ������ ��������� � ����, ��)
   //   Param24=0.0
      (Name:'24'; Value:0;),

    //  ;8 - ����� ����������� �������
 //     Param25=10
      (Name:'25'; Value:10;),

   //   ;2.0 - (db) ���������� ���������� ���������� �� �������, ����� �������
  //    Param26=2.0
      (Name:'26'; Value:2;),

      // k����.������������� ����������� ���������� (0..1)
      (Name:'Kwp'; Value:1;),

 //     ;0.20 - ��������������� ����������, ��
//      Param27=0.20
      (Name:'27'; Value:0.3;),

 //     ;1 - ����� ������� �������� ������ (0 - ������� ����� 1- ������������ �����)
 //     Param28=0
      (Name:'28'; Value:1;),

 //     ;0.050 - (��) ���������� r2 ��� �����, ���� �� � ����
  //    Param29=0.050
      (Name:'29'; Value:0.05;),

//      ;0.025 - (��) ���������� r2 ��� �����, ���� �� � ������
 //     Param30=0.025
      (Name:'30'; Value:0.025;),

//      ;0.050 - (��) ���������� r2 ��� �����, ���� �� � ���
 //     Param31=0.050
      (Name:'31'; Value:0.050;),

//      ;1 - (1-����������� HefAT �� ������ ��������� ������� 1..10 �� �� AT; 2-����������� HefAT � ����� �� ���������� �� ������ ����� ������)
//      Param32=1
      (Name:'32'; Value:1;),

//      ;1 - (1- HefAT=H_AT;  2- HefAT=(H_AT+H�����AT)-Hcp)
//      Param33=1
      (Name:'33'; Value:1;),

//      ;10.0 - //��, ��������� �������� ��������� ��������� � ������ ��� ����� (� ��), ��
//      Param34=15.0
      (Name:'34'; Value:15;),

//      ;10.0 - //��, ���������� ��������� ��������� � ������  ��� ����� (Param34=0 � ���� Param35, ��
//      Param35=15.0
      (Name:'35'; Value:15;),

//      ;15.0 - //��, ��������� �������� ��������� ��������� � ������ ��� ���������, ��
//      Param36=0.1
      (Name:'36'; Value:0.1;),

//      ;5.0 - //��, ���������� ��������� ��������� � ������  ��� ���������, ��
//      Param37=0.1
      (Name:'37'; Value:0.1;),

//      ;0.5 - //��, ���������� ��������� ���������� � ������� ���� ��� �������� ��������� �� ������� (�� ���� ������� ��������, ����������� � ������� ����)
//      Param38=0.5
      (Name:'38'; Value:0.5;),

//      ;40.0 - //��, ���������� ��������� �������������� ��������� �� �������  ��� �����, ��
//      Param39=40.0
      (Name:'39'; Value:40;),

//      ;50.0 - ��� ������ ��������� ������� ������ ����� (� ������) � ���.�����
//      Param40=50.0
      (Name:'40'; Value:50;),

//      ;35.00 - ��� ������ ��������� ����������(��), �� ������� ������������ ������� ������ � ���.�����; 0-������ ������ ����� ���������� �� ����� ��������� ���� � ���.����
//      Param41=35.00
      (Name:'41'; Value:35;),

//      ;23.0 - (�������� ����� ��� ������ ��������� � ���.�����73, ��)
//      ;���� �������
//      Param42=18.0
      (Name:'42'; Value:18;),

//      ;0.025 - (��) ���������� r2 ��� �����, ���� �� � ���.�����73
//      Param43=0.05
      (Name:'43'; Value:0.05;),

//      ;3 - (��� ���� ����������: 1- ��������� ������ ���.����73 ��� ����; 2- �� ��������� ������ ���.����, � ��������� �������� ��������� � ���; 3- �� ��������� ������ ���.����, � ������ ������ � ��� �� ���������)
//      Param44=3
      (Name:'44'; Value:3;),

//      ;50.0 - ��� ������ ��������� ������� ������ ����� (� ������) � ���
//      Param45=50.0
      (Name:'45'; Value:50;),

//      ;2.00 - ��� ������ ��������� ����������(��), �� ������� ������������ ������� ������ � ���; 0-������ ������ ����� ���������� �� ����� ��������� ���� � ���
 //     Param46=2.00
      (Name:'46'; Value:2;),

//      ;2.0 - (�������� ����� ��� ������ ��������� � ���, ��)
 //     Param47=2.0
      (Name:'47'; Value:2;),

  //    ;0.050 - (��) ���������� r2 ��� �����, ���� �� � ���
//      Param48=0.050
      (Name:'48'; Value:0.050;),

//      ;3 - (��� ���� ����������: 1- ��������� ������ ��� ��� ����;   2- �� ��������� ������ ���, � ��������� �������� ��������� � ���; 3- �� ��������� ������ ���, � ������ ������ � ��� �� ���������)
 //     Param49=1
      (Name:'49'; Value:1;)
     );



//------------------------------------------------------
function TdmCalcModel.Add (aRec: TdmCalcModelAddRec): integer;
//------------------------------------------------------
begin
  Result:=gl_DB.AddRecordID (TableName,
                 [
                  db_Par(FLD_NAME,      aRec.Name),
                  db_Par(FLD_FOLDER_ID, IIF_NULL(aRec.FolderID)),
                  db_Par (FLD_TYPE,     aRec.Type_)
                  ]);

  AddDefaultParams (Result, aRec.Type_);   
end;

//------------------------------------------------------
procedure TdmCalcModel.AddDefaultParams (aID: integer; aType: integer);
//------------------------------------------------------
  procedure DoAddParam (aIndex: integer; aValue: double);
  begin
    FTempParams.AddItem (Format('%.2d', [aIndex]), aValue);
  end;

var i: integer;
begin
  FTempParams.Clear;

  case aType of
    MODEL_DET,
    MODEL_BERTONI,
    MODEL_METOD_B,
    MODEL_INDOOR//,
   // MODEL_LOS
    :     for i:=0 to High(DET) do
                     FTempParams.AddItem (DET[i].Name, DET[i].Value);
//    MODEL_B
    MODEL_HATA567: for i:=1 to HATA567_PARAM_COUNT do
                     DoAddParam (i, HATA567[i]);

    MODEL_COST231: for i:=1 to COST231_PARAM_COUNT do
                     DoAddParam (i, COST231[i]);
  end;

  SaveParams (aID, FTempParams);
end;

function TdmCalcModel.Del(aID: integer): integer;
begin
  Result  :=dmOnega_DB_data.ExecStoredProc ('lib.sp_CalcModel_del', [db_Par('ID', aID)]);

end;


//------------------------------------------------------
function TdmCalcModel.GetK0_open_close (aID: integer; var aK0,aK0_open,aK0_closed: double): boolean;
//------------------------------------------------------
begin
  aK0       :=gl_DB.GetDoubleFieldValue (TBL_CalcModelParams, FLD_VALUE_,
               [db_Par(FLD_CALC_MODEL_ID, aID),
                db_Par(FLD_NAME, 'k0') ]);

  aK0_open  :=gl_DB.GetDoubleFieldValue (TBL_CalcModelParams, FLD_VALUE_,
               [db_Par(FLD_CALC_MODEL_ID, aID),
                db_Par(FLD_NAME, 'open_k0') ]);

  aK0_closed:=gl_DB.GetDoubleFieldValue (TBL_CalcModelParams, FLD_VALUE_,
               [db_Par(FLD_CALC_MODEL_ID, aID),
                db_Par(FLD_NAME, 'closed_k0') ]);
  result:=True;
end;


begin

end.



//------------------------------------------------------
//function TdmCalcModel_.Copy__(aSourceID: integer; aName: string): integer;
//------------------------------------------------------
//begin
//  Result:=dmOnega_DB_data.CalcModel_Copy(aSourceID, aName);
//
//
//const
//  sp_CalcModel_Copy = 'sp_CalcModel_Copy';
//
//
//  Result:=gl_DB.ExecSP_Result (sp_CalcModel_Copy, [ db_Par(FLD_ID, aSourceID),
 //                                           db_Par(FLD_Name, aName)]);
//
//
//{  ADOStoredProc1.Close;
//  ADOStoredProc1.ProcedureName:=sp_CalcModel_Copy;
//
//  with ADOStoredProc1.Parameters do
//  begin
//    Refresh;
//    ParamByName('@aID')   .Value := aSourceID;
//    ParamByName('@aName') .Value := aName;
//    ParamByName('@RESULT').Value := 0;
//  end;
//
//  ADOStoredProc1.ExecProc;
//
//  Result:= ADOStoredProc1.Parameters.ParamByName('@RESULT').Value;
//}
//end;