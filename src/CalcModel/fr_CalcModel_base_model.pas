unit fr_CalcModel_base_model;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  Tframe_CalcModel_base_model = class(TForm)
  protected
    FModelID: integer;                         

  public
    Modified: boolean;

    procedure LoadFromDB (aModelID: integer); virtual; abstract;
    procedure SaveToDB   (aModelID: integer); virtual; abstract;
  end;

var
  frame_CalcModel_base_model: Tframe_CalcModel_base_model;

implementation

{$R *.DFM}


end.
