inherited frame_CalcModel_View: Tframe_CalcModel_View
  Left = 1527
  Top = 402
  Width = 594
  Height = 483
  Caption = 'frame_CalcModel_View'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 586
  end
  inherited pn_Caption: TPanel
    Width = 586
  end
  inherited pn_Main: TPanel
    Width = 586
    Height = 299
    object pn_Inspector: TPanel
      Left = 1
      Top = 1
      Width = 584
      Height = 100
      Align = alTop
      BevelOuter = bvLowered
      Caption = 'pn_Inspector'
      Constraints.MinHeight = 100
      TabOrder = 0
    end
    object cxSplitter1: TcxSplitter
      Left = 1
      Top = 168
      Width = 584
      Height = 8
      HotZoneClassName = 'TcxSimpleStyle'
      AlignSplitter = salBottom
      Control = pn_Bottom
    end
    object pn_Bottom: TPanel
      Left = 1
      Top = 176
      Width = 584
      Height = 122
      Align = alBottom
      BevelOuter = bvLowered
      Caption = 'pn_Bottom'
      DockSite = True
      DragKind = dkDock
      TabOrder = 2
    end
  end
  inherited MainActionList: TActionList
    object act_SaveToXML: TAction
      Caption = 'Export to XML'
    end
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      25
      0)
  end
end
