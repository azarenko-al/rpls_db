inherited dmAct_CalcModel: TdmAct_CalcModel
  OldCreateOrder = True
  Left = 1196
  Top = 246
  Height = 411
  Width = 418
  inherited ActionList1: TActionList
    Left = 120
    object act_Copy: TAction
      Caption = 'act_Copy'
    end
    object act_ExportToXML: TAction
      Caption = 'act_ExportToXML'
    end
    object act_ImportFromXML: TAction
      Caption = 'act_ImportFromXML'
    end
    object act_ExportAllToXML: TAction
      Caption = 'act_ExportAllToXML'
    end
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.rlf'
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 132
    Top = 162
  end
  object SaveDialog_XML: TSaveDialog
    DefaultExt = 'xml'
    Filter = 'XML (*.xml)|*.xml'
    Left = 132
    Top = 108
  end
  object ActionList2: TActionList
    Left = 256
    Top = 120
    object act_Export_MDB: TAction
      Caption = 'act_Export_MDB'
    end
    object act_Audit: TAction
      Caption = 'act_Audit'
    end
  end
end
