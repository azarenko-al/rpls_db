unit dm_CalcModel_export;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Db, ADODB ,

  XMLDoc, XMLIntf,                                   
  u_xml_document,

  dm_Main,
 // dm_Custom,


  u_db,
  u_func,
  u_classes,

  u_const_db
  ;
    

type
  TdmCalcModel_export = class(TDataModule)
    qry_Models: TADOQuery;
    qry_Params: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private

  public
    procedure SaveToXMLFile (aID: integer; aFileName: string);

    procedure SaveToXMLNode(aIDList: TIDList; aParentNode: IXMLNode);
    procedure SaveToXMLNodeByID(aID: integer; aParentNode: IXMLNode);
  end;


function dmCalcModel_export: TdmCalcModel_export;


//==================================================================
implementation

uses dm_Onega_db_data; {$R *.DFM}
//==================================================================
var
  FdmCalcModel_export: TdmCalcModel_export;


//--------------------------------------------------------------------
function dmCalcModel_export: TdmCalcModel_export;
//--------------------------------------------------------------------
begin
  if not Assigned(FdmCalcModel_export) then
    FdmCalcModel_export:=TdmCalcModel_export.Create(Application);

  Result:= FdmCalcModel_export;
end;


procedure TdmCalcModel_export.DataModuleCreate(Sender: TObject);
begin
  db_SetComponentADOConnection (Self, dmMain.ADOConnection);
end;


//----------------------------------------------------
procedure TdmCalcModel_export.SaveToXMLNode(aIDList: TIDList; aParentNode:
    IXMLNode);
//----------------------------------------------------
var i: integer;
begin
  for i:=0 to aIDList.Count-1 do
    SaveToXMLNodeByID (aIDList[i].ID, aParentNode);
end;


//----------------------------------------------------
procedure TdmCalcModel_export.SaveToXMLNodeByID(aID: integer; aParentNode:
    IXMLNode);
//----------------------------------------------------
var vNode: IXMLNode;
begin
  if aID>=0 then
    db_OpenTableByID (qry_Models, TBL_CALCMODEL, aID)
  else
    dmOnega_db_data.OpenQuery (qry_Models, 'SELECT * FROM '+ TBL_CALCMODEL);


  with qry_Models do
    while not EOF do
  begin
    vNode:=xml_AddNode (aParentNode, TAG_ITEM,
          [xml_Par (ATT_NAME, FieldValues[FLD_NAME]),
           xml_Par (ATT_TYPE, FieldValues[FLD_TYPE]),
           xml_Par (ATT_ID,   FieldValues[FLD_ID])
           ]);

    db_OpenQuery (qry_Params,
                 'SELECT * FROM ' + TBL_CalcModelParams + ' WHERE calc_model_id=:id ORDER BY name',
                 [db_Par(FLD_ID, FieldValues[FLD_ID])] );

    with qry_Params do
      while not EOF do begin
        xml_AddNode (vNode, 'PARAM',
              [xml_Par (ATT_NAME,  FieldValues[FLD_NAME]),
               xml_Par (ATT_value, FieldValues[FLD_VALUE_])  ]);

        qry_Params.Next;
      end;

    Next;
  end;


end;


//----------------------------------------------------
procedure TdmCalcModel_export.SaveToXMLFile (aID: integer; aFileName: string);
//----------------------------------------------------
var
  oXMLDoc: TXmlDocumentEx;//('');
begin
  oXMLDoc:=TXmlDocumentEx.Create;
//  gl_XMLDoc.Clear;
  SaveToXMLNodeByID (aID, oXMLDoc.DocumentElement);
  oXMLDoc.SaveToFile (aFileName);
  oXMLDoc.Free;
end;


end.
