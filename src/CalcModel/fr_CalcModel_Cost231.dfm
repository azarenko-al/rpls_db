inherited frame_CalcModel_Cost231: Tframe_CalcModel_Cost231
  Left = 667
  Top = 199
  Width = 635
  Height = 561
  Caption = 'frame_CalcModel_Cost231'
  PixelsPerInch = 96
  TextHeight = 13
  object Memo2: TMemo [0]
    Left = 0
    Top = 423
    Width = 627
    Height = 110
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Lines.Strings = (
      'Frequency f:                    1500 - 2000 Mhz'
      'Base station height Hb:   30   - 200 m'
      'Mobile height Hm:            1    - 10 m'
      'Distance d:                      1    - 20 km'
      
        'Large and small cells (i.e. base station antenna heights above r' +
        'oot-top levels of buildings adjacent '
      'to the '
      'base station')
    ParentColor = True
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
  end
  inherited ToolBar1: TToolBar
    Width = 627
    TabOrder = 2
  end
  inherited PageControl1: TPageControl
    Width = 627
    Height = 300
    inherited TabSheet_Setup: TTabSheet
      object ScrollBox1: TScrollBox
        Left = 0
        Top = 0
        Width = 619
        Height = 217
        Align = alTop
        TabOrder = 0
        object pn_Main: TPanel
          Left = 0
          Top = 0
          Width = 615
          Height = 184
          Align = alTop
          BevelOuter = bvNone
          BorderWidth = 1
          TabOrder = 0
          object Panel1: TPanel
            Left = 1
            Top = 1
            Width = 613
            Height = 20
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Bevel5: TBevel
              Left = 0
              Top = 13
              Width = 613
              Height = 0
              Align = alBottom
              Shape = bsTopLine
            end
            object Bevel6: TBevel
              Left = 0
              Top = 13
              Width = 613
              Height = 7
              Align = alBottom
              Shape = bsTopLine
            end
            object rb_Cost_Urban: TRadioButton
              Left = 0
              Top = 4
              Width = 85
              Height = 17
              Caption = ' Urban'
              Checked = True
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              TabStop = True
            end
          end
          object Panel3: TPanel
            Left = 1
            Top = 21
            Width = 613
            Height = 124
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object rb_Cost_Centers: TRadioButton
              Left = 10
              Top = 92
              Width = 141
              Height = 17
              Caption = 'Metropolitan centers'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 4
            end
            object rb_Cost_City: TRadioButton
              Left = 10
              Top = 68
              Width = 147
              Height = 17
              Caption = 'Medium city and suburban'
              Checked = True
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
              TabStop = True
            end
            object Panel41: TPanel
              Left = 0
              Top = 0
              Width = 613
              Height = 18
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvLowered
              Caption = '     Los = Lu - a(Hm) + Cm'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
            object pn_Cost_Urban_Lu: TPanel
              Left = 0
              Top = 18
              Width = 613
              Height = 20
              Align = alTop
              Alignment = taRightJustify
              BevelOuter = bvLowered
              Color = clWindow
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              object Panel40: TPanel
                Left = 1
                Top = 1
                Width = 50
                Height = 18
                Align = alLeft
                BevelOuter = bvNone
                Caption = 'Lu ='
                TabOrder = 0
              end
            end
            object Panel29: TPanel
              Left = 261
              Top = 38
              Width = 352
              Height = 86
              Align = alRight
              BevelOuter = bvNone
              BorderWidth = 4
              TabOrder = 2
              object pn_Cost_urban_cm0: TPanel
                Left = 4
                Top = 32
                Width = 344
                Height = 20
                Align = alTop
                Alignment = taRightJustify
                BevelOuter = bvLowered
                Color = clWindow
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                object Panel31: TPanel
                  Left = 1
                  Top = 1
                  Width = 50
                  Height = 18
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = 'Cm ='
                  TabOrder = 0
                end
              end
              object Panel32: TPanel
                Left = 4
                Top = 4
                Width = 344
                Height = 4
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
              end
              object pn_Cost_aHm: TPanel
                Left = 4
                Top = 8
                Width = 344
                Height = 20
                Align = alTop
                Alignment = taRightJustify
                BevelOuter = bvLowered
                Color = clWindow
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
                object Panel34: TPanel
                  Left = 1
                  Top = 1
                  Width = 50
                  Height = 18
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = 'a(Hm) ='
                  TabOrder = 0
                end
              end
              object Panel35: TPanel
                Left = 4
                Top = 52
                Width = 344
                Height = 4
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 3
              end
              object pn_Cost_urban_cm3: TPanel
                Left = 4
                Top = 56
                Width = 344
                Height = 20
                Align = alTop
                Alignment = taRightJustify
                BevelOuter = bvLowered
                Color = clWindow
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 4
                object Panel37: TPanel
                  Left = 1
                  Top = 1
                  Width = 50
                  Height = 18
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = 'Cm ='
                  TabOrder = 0
                end
              end
              object Panel38: TPanel
                Left = 4
                Top = 28
                Width = 344
                Height = 4
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 5
              end
            end
          end
        end
      end
    end
    inherited TabSheet_Params: TTabSheet
      inherited cxGrid1: TcxGrid
        Height = 269
      end
    end
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    Left = 88
    Top = 392
    DockControlHeights = (
      0
      0
      28
      0)
  end
end
