unit u_calcparams;

interface
uses Classes, SysUtils, DB, IniFiles,
     u_func
     ;

type
  TcmParam = class
    Name: string;
    Value: double;
  end;

  TcmParamList = class(TList)
    function GetItem(Index: integer): TcmParam;
    procedure AddItem (aName: string; aValue: double);
    function FindByName (aName: string): TcmParam;
    function ValueByName (aName: string): double;

    property Items[Index: integer]: TcmParam read GetItem; default;
  end;




//==================================================================
implementation
//==================================================================



//-------------------------------------------------
// TcmParamList
//-------------------------------------------------
function TcmParamList.FindByName (aName: string): TcmParam;
var i: integer;
begin
  for i:=0 to Count-1 do
    if Items[i].Name=aName then begin Result:=Items[i]; Exit; end;
  Result:=nil;
end;

function TcmParamList.ValueByName (aName: string): double;
var i: integer;
begin
  for i:=0 to Count-1 do
    if Items[i].Name=aName then begin Result:=Items[i].Value; Exit; end;
  Result:=0;
end;


function TcmParamList.GetItem(Index: integer): TcmParam;
begin
  Result:=TcmParam (inherited Items[Index]);
end;


procedure TcmParamList.AddItem (aName: string; aValue: double);
var obj: TcmParam;
begin
  obj:=TcmParam.Create;
  obj.Name:=aName;
  obj.Value:=aValue;
  Add (obj)
end;


begin

end.