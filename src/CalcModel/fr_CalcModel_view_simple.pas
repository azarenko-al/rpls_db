unit fr_CalcModel_view_simple;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,

  DBCtrls, Grids, DBGrids, rxPlacemnt,
  StdCtrls, ToolWin, ComCtrls, ExtCtrls,

  fr_DBInspector,
  fr_CalcModel_models,
  dm_CalcModel,

  u_func,
  u_const_DB,
  u_reg,
  u_dlg,
  u_const,
  u_db,

  fr_base_View,
  fr_View_base, 

   Db, ADODB, ImgList,Dialogs,
  cxPropertiesStore, Menus, ActnList, dxBar, cxBarEditItem, cxClasses,
  cxLookAndFeels
  ;

type
  Tframe_CalcModel_View_simple = class(Tframe_View_Base)
    act_SaveToXML: TAction;
    OpenDialog1: TOpenDialog;
    FormStorage1: TFormStorage;
    SaveDialog1: TSaveDialog;
    qry_Params: TADOQuery;
    ds_Params: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure GSPages1Changing(Sender: TObject; NewIndex: Integer;
      var AllowChange: Boolean);

  private
    Ffrm_DBInspector: Tframe_DBInspector;
    Ffrm_CalcModel_models: Tframe_CalcModel_models;
    FADOQuery: TADOQuery;

    FType: integer;
//    procedure CheckEditState();

  public
    procedure View (aID: integer); override;
  end;

var
  frame_CalcModel_View_simple: Tframe_CalcModel_View_simple;


implementation {$R *.dfm}


//--------------------------------------------------------------------
procedure Tframe_CalcModel_View_simple.View (aID: integer);
//--------------------------------------------------------------------
begin
//  CheckEditState();

  inherited View(aID);

  dmCalcModel.Open (aID, FADOQuery);
  dmCalcModel.OpenParams (ID, qry_Params);


//  Ffrm_DBInspector.ViewSimple (ID, 'CalcModel');
  Ffrm_DBInspector.DoAfterView();
  Ffrm_DBInspector.IsRenamed:=False;

  FType:=FADOQuery.FieldByName(FLD_TYPE).AsInteger;

  Ffrm_CalcModel_models.View (FType, ID);
end;


//--------------------------------------------------------------------
procedure Tframe_CalcModel_View_simple.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  Caption:=STR_CALC_MODEL;

  pn_Main.Align:=alClient;
  pn_Inspector.Align:=alTop;
  pn_Inspector.Height:=80;

  GSPages1.Align:=alClient;

  Ffrm_DBInspector:=Tframe_DBInspector.Create(Self);
  CopyControls(Ffrm_DBInspector, pn_Inspector );

  FADOQuery:=Ffrm_DBInspector.ADOQuery1;


  Ffrm_CalcModel_models:=Tframe_CalcModel_models.Create(Self);
  CopyControls(Ffrm_CalcModel_models, ts_Setup);


  Ffrm_DBInspector.AddSimpleRow (nil, 'name', '�������� ������');

  with gl_Reg do begin
    BeginGroup(Self, Name);
    AddControl(GSPages1, [PROP_HEIGHT]);
    AddControl(GSPages1, [PROP_ACTIVE_PAGE_INDEX]);
  end;
end;


procedure Tframe_CalcModel_View_simple.FormDestroy(Sender: TObject);
begin
//  CheckEditState;

  gl_Reg.SaveAndClearGroup(Self, Name);

  Ffrm_DBInspector.Free;
  Ffrm_CalcModel_models.Free;
end;


procedure Tframe_CalcModel_View_simple.GSPages1Changing(Sender: TObject;
  NewIndex: Integer; var AllowChange: Boolean);
begin
  inherited;

  if NewIndex=1 then
    dmCalcModel.OpenParams (ID, qry_Params);

end;



end.