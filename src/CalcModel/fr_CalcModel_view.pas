unit fr_CalcModel_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, Menus,
 
 StdCtrls, ToolWin, ComCtrls, ExtCtrls,
   ImgList, RXSplit, cxControls, cxSplitter,

  fr_View_base,

  fr_DBInspector_Container,
  fr_CalcModel_models,

  u_const_DB,
  u_const_str,
  u_types,

  u_reg,
  u_dlg,
  u_func, cxPropertiesStore, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters, dxBar,
  cxBarEditItem, cxClasses
  ;

type
  Tframe_CalcModel_View = class(Tframe_View_Base)
    act_SaveToXML: TAction;
    pn_Inspector: TPanel;
    cxSplitter1: TcxSplitter;
    pn_Bottom: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    Ffrm_DBInspector: Tframe_DBInspector_Container;
    Ffrm_CalcModel_models: Tframe_CalcModel_models;
  public
    procedure View (aID: integer; aGUID: string); override;
  end;

  
//==================================================================
implementation {$R *.dfm}
//==================================================================

//--------------------------------------------------------------------
procedure Tframe_CalcModel_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
  ObjectName:=OBJ_CALC_MODEL;

  TableName:=TBL_CALCMODEL;



  Caption:=STR_CALC_MODEL;

  pn_Inspector.Align:=alClient;


  CreateChildForm(Tframe_DBInspector_Container, Ffrm_DBInspector, pn_Inspector);
  Ffrm_DBInspector.PrepareViewForObject(OBJ_CALC_MODEL);
//  Ffrm_DBInspector:=Tframe_DBInspector.CreateChildForm(pn_Inspector, OBJ_CALC_MODEL);


  CreateChildForm (Tframe_CalcModel_models, Ffrm_CalcModel_models, pn_Bottom);
//  :=.Create(Self);
  //CopyControls(Ffrm_CalcModel_models, pn_Bottom);

 // AddControl(pn_Bottom, [PROP_HEIGHT]);

  AddComponentProp(pn_Bottom, PROP_HEIGHT);
  cxPropertiesStore.RestoreFrom;

end;

//--------------------------------------------------------------------
procedure Tframe_CalcModel_View.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
//  Ffrm_DBInspector.Free;
//  Ffrm_CalcModel_models.Free;

  inherited;
end;


//--------------------------------------------------------------------
procedure Tframe_CalcModel_View.View (aID: integer; aGUID: string);
//--------------------------------------------------------------------
begin
  inherited;

  if not RecordExists (aID) then
    Exit;


  Ffrm_DBInspector.View (aID);
  Ffrm_CalcModel_models.View (aID);
end;




end.