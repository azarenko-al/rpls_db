inherited dlg_CalcModel_add: Tdlg_CalcModel_add
  Left = 557
  Top = 392
  Height = 394
  Caption = 'dlg_CalcModel_add'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 325
  end
  inherited Panel1: TPanel
    inherited lb_Name: TLabel
      Top = 6
    end
    inherited ed_Name_: TEdit
      Width = 547
    end
  end
  inherited Panel2: TPanel
    Height = 205
    inherited PageControl1: TPageControl
      Height = 128
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 542
          Height = 97
          Align = alClient
          LookAndFeel.Kind = lfStandard
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 129
          OptionsView.ValueWidth = 71
          TabOrder = 0
          object row_Model: TcxEditorRow
            Expanded = False
            Properties.Caption = #1052#1086#1076#1077#1083#1100' '#1088#1072#1089#1095#1077#1090#1072
            Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.DropDownListStyle = lsFixedList
            Properties.EditProperties.DropDownRows = 10
            Properties.EditProperties.ImmediateDropDown = False
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.EditProperties.Revertable = True
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
          end
        end
      end
    end
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 488
    Top = 5
  end
  object OpenDialog1: TOpenDialog
    Left = 232
  end
end
