unit fr_CalcModel_Cost231;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, DB, cxPropertiesStore, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  ADODB, ActnList,  ToolWin, ComCtrls,

  u_Formula_Editor,
  u_func,
  u_Classes,

  u_calcparams,

  fr_CalcModel_model_base,

  cxControls, cxGridCustomView, cxGrid, dxBar
  ;

type
  Tframe_CalcModel_Cost231 = class(Tframe_CalcModel_model_base)
    Memo2: TMemo;
    ScrollBox1: TScrollBox;
    pn_Main: TPanel;
    Panel1: TPanel;
    Bevel5: TBevel;
    Bevel6: TBevel;
    rb_Cost_Urban: TRadioButton;
    Panel3: TPanel;
    rb_Cost_Centers: TRadioButton;
    rb_Cost_City: TRadioButton;
    Panel41: TPanel;
    pn_Cost_Urban_Lu: TPanel;
    Panel40: TPanel;
    Panel29: TPanel;
    pn_Cost_urban_cm0: TPanel;
    Panel31: TPanel;
    Panel32: TPanel;
    pn_Cost_aHm: TPanel;
    Panel34: TPanel;
    Panel35: TPanel;
    pn_Cost_urban_cm3: TPanel;
    Panel37: TPanel;
    Panel38: TPanel;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FCostEditorList : TFormulaEditorGroup;
    
    FBeginUpdate: boolean;
    FRadioModified: boolean;

    procedure SelectFormula(Sender: TObject);
    procedure DrawFormulaEditors();
    procedure InitFormulas();

  public
    function Modified: boolean; override;

    procedure LoadFromParams (aParams: TcmParamList); override;
    procedure SaveToParams   (aParams: TcmParamList); override;
  end;


const
  INDEX_COST231_LU   = 0;
  INDEX_COST231_aHm  = 1;
  INDEX_COST231_Cm0  = 2;
  INDEX_COST231_Cm3  = 3;

//=========================================================================
//=========================================================================
implementation  {$R *.DFM}



procedure Tframe_CalcModel_Cost231.FormCreate(Sender: TObject);
begin
  inherited;

  pn_Main.Align:=alClient;

  FCostEditorList:=TFormulaEditorGroup.Create;
  InitFormulas();

  rb_Cost_City.OnClick:=SelectFormula;
  rb_Cost_Centers.OnClick:=SelectFormula;

  pn_Main.Align:=alCLient;
  ScrollBox1.Align:=alCLient;

end;



procedure Tframe_CalcModel_Cost231.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FCostEditorList);

  inherited;
end;



procedure Tframe_CalcModel_Cost231.SelectFormula(Sender: TObject);
begin
  if not FBeginUpdate then
    FRadioModified:=True;

  if Sender=rb_Cost_City    then FCostEditorList.ActiveFormulaIndex:=0;
  if Sender=rb_Cost_Centers then FCostEditorList.ActiveFormulaIndex:=1;

  DrawFormulaEditors();
end;


//------------------------------------------------------
procedure Tframe_CalcModel_Cost231.InitFormulas;
//------------------------------------------------------
var ed: TFormulaEditor;
begin

  // COST URBAN
  ed:=TFormulaEditor.Create (pn_Cost_Urban_Lu, Canvas, FCostEditorList);
  ed.FormulaText:=COST231_LU;
  FCostEditorList.Add(ed);

  ed:=TFormulaEditor.Create (pn_Cost_aHm, Canvas, FCostEditorList);
  ed.FormulaText:=COST231_aHm;
  FCostEditorList.Add(ed);

  ed:=TFormulaEditor.Create (pn_Cost_urban_cm0, Canvas, FCostEditorList);
  ed.FormulaText:=COST231_Cm0;
  FCostEditorList.Add(ed);

  ed:=TFormulaEditor.Create (pn_Cost_urban_cm3, Canvas, FCostEditorList);
  ed.FormulaText:=COST231_Cm3;
  FCostEditorList.Add(ed);
end;



// -------------------------------------------------------------------
procedure Tframe_CalcModel_Cost231.DrawFormulaEditors();
// -------------------------------------------------------------------
var i: integer;
begin
  // set NOT ENABLED all except  first
  for i:=1 to FCostEditorList.Count-1 do FCostEditorList[i].Enabled:=false;

  // COST --------------------------------
  if rb_Cost_Urban.Checked then begin
    FCostEditorList[INDEX_COST231_Cm0].Enabled := rb_Cost_City.Checked;
    FCostEditorList[INDEX_COST231_Cm0].Enabled := rb_Cost_Centers.Checked;
  end;
  pn_Cost_urban_cm0.Color := IIF(rb_Cost_City.Checked, clWindow, clBtnFace);
  pn_Cost_urban_cm3.Color := IIF(rb_Cost_Centers.Checked, clWindow, clBtnFace);

end;

// -------------------------------------------------------------------
procedure Tframe_CalcModel_Cost231.LoadFromParams(aParams: TcmParamList);
// -------------------------------------------------------------------
begin
  FBeginUpdate:=True;
  FRadioModified:=False;

  FCostEditorList.LoadFromParams (aParams);

  with FCostEditorList do
  case ActiveFormulaIndex of
    0: rb_Cost_City.Checked:=true;    // SelectFormula (rb_Medium_city);
    1: rb_Cost_Centers.Checked:=true; // SelectFormula (rb_Large_city_200);
  end;

  FBeginUpdate:=False;
end;

// -------------------------------------------------------------------
procedure Tframe_CalcModel_Cost231.SaveToParams(aParams: TcmParamList);
// -------------------------------------------------------------------
begin
  FCostEditorList.SaveToParams(aParams);
  FCostEditorList.Modified:=False;
  FRadioModified:=False;
end;


function Tframe_CalcModel_Cost231.Modified: boolean;
begin
  Result:=FCostEditorList.Modified or FRadioModified ;
end;



end.
