unit fr_Inspector_Base;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Variants, ExtCtrls, StdCtrls, Menus,  cxDBVGrid,  cxDropDownEdit, cxVGrid, cxControls,


  u_cx_DBVGrid_manager,


  XMLDoc, XMLIntf,
     u_xml_document,

  u_func, cxInplaceContainer
 // u_XML
  ;

type

  //-------------------------------------------------
  Tframe_Inspector_base = class(TForm)
    cxVerticalGrid1: TcxVerticalGrid;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;
    cxVerticalGrid1EditorRow1: TcxEditorRow;
    cxVerticalGrid1EditorRow2: TcxEditorRow;
    procedure cxVerticalGrid1EditValueChanged(Sender: TObject; ARowProperties:
        TcxCustomEditorRowProperties);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);

  private

    procedure SetReadOnly;

    procedure ClearNodes;

 //   procedure SaveParamValuesToXML   (vParentNode: IXMLNode);
  //  procedure LoadParamValuesFromXML (vParentNode: IXMLNode);

   // function  FindRowByFieldName (aFieldName: string): TdxInspectorRow;
   // procedure AddRowListItem (aRow: TdxInspectorRow; aCaption: string; aID: integer);

  public
    Modified: boolean; // �����-�� ������ ���� ��������

    GridManager: TcxVerticalGridManager;


  end;


//========================================================
implementation  {$R *.DFM}
//========================================================

//
//
//function VarToString(Value : Variant): string;
//begin
//  case VarType(Value) of
//    varString  : Result:=Trim(Value);
//    varInteger : Result:=IntToStr(Value);
//    varDouble  : Result:=FloatToStr(Value);
//    varBoolean :
//       Result:=IIF(Value=true,1,0);
//    varDate    : Result:=DateTimeToStr (Value);
//  end;
//end;
//


procedure Tframe_Inspector_base.FormCreate(Sender: TObject);
begin
 // Align:=alClient;

 // dxInspector.Align:=alClient;
//  dxInspector.OnEdited:=dxInspectorEdited;

  cxverticalGrid1.Align:=alClient;


 // cxverticalGrid1.ClearRows;


  GridManager:=TcxVerticalGridManager.Create(cxVerticalGrid1);

end;


procedure Tframe_Inspector_base.FormDestroy(Sender: TObject);
begin
  FreeAndNil(GridManager);
end;


procedure Tframe_Inspector_base.SetReadOnly;
begin
//  with dxInspector do
  //  Options:=Options - [ioEditing];
end;




procedure Tframe_Inspector_base.ClearNodes;
begin
 // dxInspector.ClearNodes;
end;

procedure Tframe_Inspector_base.cxVerticalGrid1EditValueChanged(Sender:
    TObject; ARowProperties: TcxCustomEditorRowProperties);
begin
  Modified := True;
 // ShowMessage('cxVerticalGrid1EditValueChanged');
end;

end.




//    {
//
////-----------------------------------------------------------------
//procedure Tframe_Inspector_base.dxInspectorRowButtonClick (Sender: TObject; AbsoluteIndex: Integer);
////-----------------------------------------------------------------
//var
//  row: TdxInspectorTextButtonRow;
//  bNeedCallback: boolean;
//  oField: TInspectorField;
//
//begin
//  row:=TdxInspectorTextButtonRow(Sender);
//  oField:=TInspectorField(row.Tag);
//  oField.FieldValue:=row.Text;
//
//  bNeedCallback:=true;
//
//
//  if Assigned(FOnButtonClick) then begin
//    FOnButtonClick (oField.FieldName);
//
//    if Assigned(FOnRefreshValues) then
//      FOnRefreshValues(nil);
//
//    dxInspector.HideEditor;  ///!!!!!!!!!!!!!!!!!!!!!
//
//  end;
//                           {
//  if bNeedCallback and Assigned(FOnButtonClick) then begin
//    FOnButtonClick (Self, FClass, prop.FieldName, prop );
//    RefreshFieldValues();
//
//    dxInspector.HideEditor;  ///!!!!!!!!!!!!!!!!!!!!!
//
//  end;
//  }
//
//end;
//
//
//
//
//
//
//
////--------------------------------------------------------
//// DoAddRows
////--------------------------------------------------------
//function Tframe_Inspector_base.AddRow (aParentRow: TdxInspectorRow;
//                                       aFieldType: TInspFieldType;
//                                       aCaption,aFieldName: string;
//                                       aListItems: TStringList = nil): TdxInspectorRow;
////--------------------------------------------------------
//var oField: TInspectorField;   j: integer;
//begin
//  case aFieldType of
//
//    ftNone,ftString,ftFloat: Result:=dxInspector.CreateRow(TdxInspectorTextRow);
//
//    ftInteger:               Result:=dxInspector.CreateRow(TdxInspectorTextSpinRow);
//
////    ftButton,ftFile:         Result:=dxInspector.CreateRow(TdxInspectorTextButtonRow);
//
//    ftBoolean,
//    ftFixedList: begin
//                    Result:=dxInspector.CreateRow(TdxInspectorTextPickRow);
//                    with (Result as TdxInspectorTextPickRow) do
//                    begin
//                      PopupBorder:=pbSingle;
//                      DropDownListStyle:=true;
//                    end;
//                 end;
//  end;
//
//  if Assigned(aParentRow) then Result.Node.MoveTo (aParentRow.Node,inaAddChild);
//
//  oField:=TInspectorField.Create;
//  oField.FieldName:=aFieldName;
//  oField.FieldType:=aFieldType;
//  Result.Tag:=Integer(oField);
//
//  Result.Caption:=aCaption;
//  if aFieldName='' then Result.ReadOnly:=True;
//
//  if (aCaption<>'') and (aFieldName='') then Result.IsCategory:=True;
//
//
//
//  case aFieldType of
////      ftFile,ftButton
////                : with (Result as TdxInspectorTextButtonRow) do
////                    OnButtonClick:=dxInspectorRowButtonClick;
//
//      ftFixedList
//                : with (Result as TdxInspectorTextPickRow) do
//                    OnCloseUp:=dxInspectorRowCloseUp;
//  end;
//
//  if (aFieldType in [ftFixedList,ftBoolean]) and (Assigned(aListItems)) then
//    for j:=0 to aListItems.Count-1 do
//      AddRowListItem (Result, aListItems[j], Integer(aListItems.Objects[j]) );
//
//end;
//
//
//
////-----------------------------------------------------------------
//procedure Tframe_Inspector_base.AddRowListItem (aRow: TdxInspectorRow; aCaption: string; aID: integer);
////-----------------------------------------------------------------
//begin
//  with (aRow as TdxInspectorTextPickRow) do
//    Items.AddObject (aCaption, Pointer(aID));
//end;
//
//procedure Tframe_Inspector_base.cxVerticalGrid1Edited(Sender: TObject;
//    ARowProperties: TcxCustomEditorRowProperties);
//begin
//
//end;
//
//
////--------------------------------------------------------
//function Tframe_Inspector_base.FindRowByFieldName (aFieldName: string): TdxInspectorRow;
////--------------------------------------------------------
//var i: integer;   obj: TInspectorField;
//begin
//  Result:=nil;
//  if aFieldName='' then Exit;
//
//  with dxInspector do
//    for i:=0 to TotalRowCount-1 do
//  begin
//    obj:=TInspectorField(Rows[i].Tag);
//
//    if Eq (obj.FieldName, AFieldName) then
//    begin
//      Result:=Rows[i]; Exit;
//    end;
//  end;
//end;
//
////------------------------------------------------------------
//procedure Tframe_Inspector_base.dxInspectorEdited (Sender: TObject; Node: TdxInspectorNode; Row: TdxInspectorRow);
////------------------------------------------------------------
//var ind,iValue: integer;   sValue: string;
//    oField: TInspectorField;
//    oRow  : TdxInspectorTextPickRow;
//begin // ��� ����� List ��������� ������ ������
//  if row.EditText = FOldValue then Exit;
//  Modified:=True;
//
//  oField:=TInspectorField(row.Tag);
//
//  if oField.FieldType = ftFixedList
//  then begin
//    oRow :=(row as TdxInspectorTextPickRow);
//    ind:=oRow.Items.IndexOf (row.EditText);
//    if ind>=0 then begin
//      iValue:=Integer(oRow.Items.Objects[ind]);
//      sValue:=IntToStr(iValue);
//    end;
//
//  end else
//    sValue:=row.EditText;
//
//  //--------------------------------------
//  if Assigned (FOnFieldEdited) then
//    FOnFieldEdited (oField.FieldName, sValue);
//end;
//
////------------------------------------------------------------
//procedure Tframe_Inspector_base.dxInspectorChangeNode (Sender: TObject; OldNode, Node: TdxInspectorNode);
////------------------------------------------------------------
//begin
//  if Assigned(FOnChangeNode) then
//    FOnChangeNode(GetFocusedField());
//
//  with dxInspector do
//    if FocusedNumber>=0 then
//      FOldValue:=Rows[FocusedNumber].EditText;
//end;
//
////------------------------------------------------------------
//procedure Tframe_Inspector_base.dxInspectorRowCloseUp (Sender: TObject; var Value: Variant; var Accept: Boolean);
////------------------------------------------------------------
//var row: TdxInspectorTextPickRow;
//    oField: TInspectorField;
//    ind,iValue: integer;
//begin
//  if Value = FOldValue then Exit;
//
//  row :=(Sender as TdxInspectorTextPickRow);
//  oField:=TInspectorField(row.Tag);
//
//  ind:=row.Items.IndexOf (Value);
//  if ind>=0 then begin
//    iValue:=Integer(row.Items.Objects[ind]);
//    Modified:=True;
//
//    if Assigned (FOnFieldEdited) then
//       FOnFieldEdited (oField.FieldName, IntToStr(iValue));
//
//    if Assigned (FOnCloseUp) then
//       FOnCloseUp (oField.FieldName, IntToStr(iValue));
//
//  end;
//end;
//
//
////--------------------------------------------------------
//function Tframe_Inspector_base.GetFieldValue (aFieldName: string): Variant;
////--------------------------------------------------------
//var oRow: TdxInspectorRow;
//    oField: TInspectorField;
//    ind: integer;
//begin
//  oRow:=FindRowByFieldName(aFieldName);
//  if not Assigned(oRow) then
//    raise Exception.Create('Tframe_Inspector: Not found field - ' + aFieldName);
//
//  oField:=TInspectorField(oRow.Tag);
//
//  case oField.FieldType of
//    ftFixedList,ftBoolean
//                :  with (oRow as TdxInspectorTextPickRow) do begin
//                    ind:=Items.IndexOf(EditText);
//
//                    if oField.FieldType=ftFixedList then
//                      if ind>=0 then Result:=Integer(Items.Objects[ind])
//                                else Result:=-1;
//
//                    if oField.FieldType=ftBoolean then
//                      Result:=(ind=0);
//                  end;
//
//    ftFloat    :  Result:=AsFloat(oRow.EditText);
//    ftInteger  :  Result:=AsInteger(oRow.EditText);
//  else
//      Result:=oRow.EditText;
//  end;
//
//
//end;
//
////--------------------------------------------------------
//procedure Tframe_Inspector_base.SetFieldValue (aFieldName: string; aValue: Variant);
////--------------------------------------------------------
//var oRow: TdxInspectorRow;
//    oField: TInspectorField;
//    ind,iValue: integer;
//    oStrList: TStrings;
//begin
//  oRow:=FindRowByFieldName(AFieldName);
//  if not Assigned(oRow) then
//    Exit;
////    raise Exception.Create('Tframe_Inspector: Not found field - ' + aFieldName);
//
//  if VarIsNull(aValue) then Exit;
//
//  oField:=TInspectorField(oRow.Tag);
//
//  case oField.FieldType of
//    ftFixedList:  begin
//                    iValue:= Integer(aValue);
//                    oStrList:=(oRow as TdxInspectorTextPickRow).Items;
//                    ind:=oStrList.IndexOfObject (Pointer(iValue));
//
//                    if ind>=0
//                       then oRow.EditText := oStrList [ind]
//                       else oRow.EditText := '';
//                  end;
////    ftFloat    :  oRow.EditText:=AsFloat(VarToString(aValue));
//
//    ftBoolean  :  with (oRow as TdxInspectorTextPickRow) do
//                   if Items.Count=2 then
//                    if aValue=false then oRow.EditText := Items [0]
//                                    else oRow.EditText := Items [1];
//
//  else
//      oRow.EditText := VarToString(aValue);
//  end;
//
//end;
//
////--------------------------------------------------------
//procedure Tframe_Inspector_base.RestoreDefaultValues();
////--------------------------------------------------------
//var i: integer;  oField: TInspectorField;
//begin
//  with dxInspector do begin
//    HideEditor;
//
//    for i:=0 to TotalRowCount-1 do begin
//      oField:=TInspectorField(Rows[i].Tag);
//      if oField.FieldName<>'' then
//        SetFieldValue (oField.FieldName, oField.DefaultValue);
//    end;
//
//    Refresh;
//  end;
//end;
//
//
//procedure Tframe_Inspector_base.GotoTop();
//begin
//  dxInspector.TopIndex:=0;
//end;
//
////--------------------------------------------------------
//function Tframe_Inspector_base.GetFocusedField(): TInspectorField;
////--------------------------------------------------------
//begin
//  with dxInspector do
//    if FocusedNumber>0 then Result:=TInspectorField (Rows[FocusedNumber].Tag)
//                       else Result:=nil;
//end;



//--------------------------------------------------------
procedure Tframe_Inspector_base.SaveParamValuesToXML (vParentNode: IXMLNode);
//--------------------------------------------------------
(*
var i,j: integer;
    vNode: IXMLNode;
    oField: TInspectorField;
    vValue: Variant;
    sCaption: string;

   function GetRootCaption (aRow: TdxInspectorRow): string;
   var k: integer;
   begin
      Result:=aRow.Caption;
      if Assigned(aRow.Node.Parent) then
      with dxInspector do
        for k:=0 to TotalRowCount-1 do
          if Rows[k].Node = aRow.Node.Parent then begin
             Result := GetRootCaption (Rows[k]) + ': ' + Result;
             Break;
          end;
     // if Assigned(aRow.Node.Parent) then
   end;
*)
begin
 (* with dxInspector do
    for i:=0 to TotalRowCount-1 do
    begin
      oField:=TInspectorField (Rows[i].Tag);
      if oField.FieldName='' then Continue;

      vValue:=GetFieldValue (oField.FieldName);

      sCaption:=GetRootCaption (Rows[i]);

      vNode:=xml_AddNode (vParentNode, 'param',
          [xml_Att ('id',       oField.FieldName),
           xml_Att ('value',    vValue),
           xml_Att ('caption',  sCaption)
           ]);

    end;*)
end;

//--------------------------------------------------------
procedure Tframe_Inspector_base.LoadParamValuesFromXML (vParentNode: IXMLNode);
//--------------------------------------------------------
(*var i,j: integer;
   vNode: IXMLNode;
   oField: TInspectorField;
   vValue: Variant;
   sFieldName,sValue: string;
*)
begin
 (* dxInspector.HideEditor;

  if not VarIsNull(vParentNode) then
  for i:=0 to vParentNode.ChildNodes.Count-1 do
  begin
    vNode:=vParentNode.ChildNodes[i];
    if not Eq(vNode.NodeName , 'param') then Continue;

    sFieldName:=xml_GetAttr (vNode, 'id');
    sValue:=xml_GetAttr (vNode, 'value');

    SetFieldValue (sFieldName, sValue);
  end;

  dxInspector.Refresh;
  Modified:=True;*)
end;
//  TInspectorField = class;
//
//  TInspFieldType = (ftNone, ftString, ftInteger,ftFloat,
//  //ftButton,
//  ftBoolean,
//  //ftFile,
//                   ftFixedList);
//
//  TOnFieldEditedEvent       = procedure (aFieldName: string; aNewValue: string) of object;
//  TOnInspectorCloseUpEvent  = TOnFieldEditedEvent;
//  TOnInspectorEditedEvent   = TOnFieldEditedEvent;
//  TOnInspectorChangeNode    = procedure (aInspectorField: TInspectorField) of object;
//  TOnInspectorButtonClickEvent  = procedure (aFieldName: string) of object;


 // -------------------------------------------------
//  TInspectorField = class
 // -------------------------------------------------
//    FieldName  : string;
//    FieldType  : TInspFieldType;
//    FieldValue : string;
//    DefaultValue: Variant;
//    Comment    : string; // �������� ����
//
//(*    Dialog: record // Dialog options
//              FileExt   : string;
//              FileFilter: string;
//            end;
//*)
//  end;

//    function  AddRow (aParentRow: TdxInspectorRow;
//                      aFieldType: TInspFieldType;
//                      aCaption,aFieldName: string;
//                      aListItems: TStringList = nil): TdxInspectorRow; overload;
//
//    function  AddRow (aParentRow: TdxInspectorRow;
//                      aCaption,aValue: string): TdxInspectorRow; overload;


 //   procedure SetFieldValue (aFieldName: string; aValue: Variant);
 //   function  GetFieldValue (aFieldName: string): Variant;

    //function  GetFocusedField(): TInspectorField;
   // procedure RestoreDefaultValues();
   // procedure GotoTop();


//    property OnCloseUp:       TOnInspectorCloseUpEvent read FOnCloseUp write FOnCloseUp;
//    property OnFieldEdited:   TOnFieldEditedEvent read FOnFieldEdited write FOnFieldEdited;
//  //  property OnButtonClick:   TOnInspectorButtonClickEvent read FOnButtonClick write FOnButtonClick;
//    property OnRefreshValues: TNotifyEvent read FOnRefreshValues write FOnRefreshValues;
//
//    property OnChangeNode: TOnInspectorChangeNode read FOnChangeNode write FOnChangeNode;


//
//function Tframe_Inspector_base.AddRow (aParentRow: TdxInspectorRow; aCaption,aValue: string): TdxInspectorRow;
//begin
//  Result:=AddRow (aParentRow, ftNone, aCaption, '', nil);
//  Result.EditText:=aValue;
//
//  Result.IsCategory:=False;
//end;


