unit d_CalcModel_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList,     ExtCtrls, 
  StdCtrls,  cxPropertiesStore, rxPlacemnt, cxDropDownEdit,

  u_cx_vgrid,

 // cxComboBo,

  d_Wizard_add_with_Inspector,

  I_CalcModel,

  u_reg,
  u_db,
  u_func,
  u_dlg,

  u_const_db,
  u_const_str,

  dm_CalcModel, cxVGrid, cxControls, cxInplaceContainer, ComCtrls
  ;

type
  Tdlg_CalcModel_add = class(Tdlg_Wizard_add_with_Inspector)
    OpenDialog1: TOpenDialog;
    cxVerticalGrid1: TcxVerticalGrid;
    row_Model: TcxEditorRow;
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
  private
    FID,FSourceID,FFolderID: integer;
 //   FAction: (acCreate,acCopy);

    rec: TdmCalcModelAddRec;

    procedure Append;
  public
    class function ExecDlg (aFolderID: integer): integer;
  //  class function CopyDlg (aID: integer; var aFolderID: integer): integer;
  end;


//====================================================================
implementation {$R *.dfm}
//====================================================================

//--------------------------------------------------------------------
class function Tdlg_CalcModel_add.ExecDlg;                
//-------------------------------------------------------     -------------
begin            
  with Tdlg_CalcModel_add.Create(Application) do
  begin
    FFolderID:=aFolderID;
 //   FAction:=acCreate;

    ed_Name_.Text:=dmCalcModel.GetNewName();

    ShowModal;
    Result:=FID;

    Free;
  end;
end;


//--------------------------------------------------------------------
procedure Tdlg_CalcModel_add.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
var
  i: integer;
  oStrings: TStrings;

begin
  inherited;

  SetActionName (STR_DLG_ADD_CALC_MODEL);

  ed_Name_.Text:=STR_CALC_MODEL;


  oStrings := TcxComboBoxProperties(row_Model.Properties.EditProperties).Items;

  for i:=0 to High(CALC_MODELS) do
    oStrings.AddObject (CALC_MODELS[i].Name,  Pointer(CALC_MODELS[i].ID));

  row_Model.Properties.Value := oStrings[0];

  //row_Model.Text:=CALC_MODELS[0].Name;



  cx_InitVerticalGrid (cxVerticalGrid1);

  SetDefaultSize();

end;



procedure Tdlg_CalcModel_add.act_OkExecute(Sender: TObject);
begin
  Append;
end;


//--------------------------------------------------------------------
procedure Tdlg_CalcModel_add.Append;
//--------------------------------------------------------------------
var ind,iType: integer;
  v: Variant;
//  sName: string;
  oStrings: TStrings;
begin
//  sName:=ed_Name_.Text;

  oStrings := TcxComboBoxProperties(row_Model.Properties.EditProperties).Items;

  ind:=oStrings.IndexOf(row_Model.Properties.Value);

  if ind>=0 then
    iType:=Integer(oStrings.Objects [ind])
  else
    iType:=0;


 // case FAction of
  //  acCreate:
   //   begin
 // v:=row_Model.Properties.Value;

 (*
  ind:=row_Model.Items.IndexOf(row_Model.Text);
  if ind<0 then  Exit;

  iType:=Integer(row_Model.Items.Objects [ind]);
*)
 // iType:=0;


  rec.Name    :=ed_Name_.Text;
  rec.FolderID:=FFolderID;
  rec.Type_   :=iType;
//   rec.FileName_ConfigHef:=row_File_ConfigHEF.Text;

  FID:=dmCalcModel.Add (rec);
//    end;

 //   acCopy:
  //    FID:=dmCalcModel_.Copy (FSourceID, ed_Name_.Text, FFolderID);
 // end;
end;


end.