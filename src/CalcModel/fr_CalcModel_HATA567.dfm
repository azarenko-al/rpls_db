inherited frame_CalcModel_HATA567: Tframe_CalcModel_HATA567
  Left = 530
  Top = 154
  Width = 749
  Height = 624
  Caption = 'frame_CalcModel_HATA567'
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    Width = 716
    TabOrder = 1
  end
  inherited PageControl1: TPageControl
    Width = 716
    Height = 492
    TabOrder = 0
    inherited TabSheet_Setup: TTabSheet
      object ScrollBox1: TScrollBox
        Left = 52
        Top = 0
        Width = 656
        Height = 351
        Align = alRight
        TabOrder = 1
        object pn_Model: TPanel
          Left = 56
          Top = 0
          Width = 596
          Height = 347
          Align = alRight
          BevelOuter = bvNone
          BorderWidth = 5
          TabOrder = 0
          object Panel2: TPanel
            Left = 5
            Top = 53
            Width = 586
            Height = 96
            Align = alTop
            BevelOuter = bvNone
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            object rb_Large_city_400: TRadioButton
              Left = 10
              Top = 72
              Width = 141
              Height = 17
              Caption = 'Large city (f >= 400 Mhz)'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 4
            end
            object rb_Medium_city: TRadioButton
              Left = 10
              Top = 25
              Width = 105
              Height = 17
              Caption = 'Medium-small city'
              Checked = True
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              TabStop = True
            end
            object rb_Large_city_200: TRadioButton
              Left = 10
              Top = 48
              Width = 141
              Height = 17
              Caption = 'Large city (f <= 200 Mhz)'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
            end
            object Panel25: TPanel
              Left = 0
              Top = 0
              Width = 586
              Height = 17
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvLowered
              Caption = '     Los = Lu - a(Hm)'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
            object Panel14: TPanel
              Left = 216
              Top = 17
              Width = 370
              Height = 79
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
              object pn_Hata_Urban_small: TPanel
                Left = 0
                Top = 4
                Width = 370
                Height = 20
                Align = alTop
                Alignment = taRightJustify
                BevelOuter = bvLowered
                Color = clWindow
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                object Panel16: TPanel
                  Left = 1
                  Top = 1
                  Width = 50
                  Height = 18
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = 'a(Hm) ='
                  TabOrder = 0
                end
              end
              object Panel17: TPanel
                Left = 0
                Top = 48
                Width = 370
                Height = 4
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
              end
              object pn_Hata_Urban_large_400: TPanel
                Left = 0
                Top = 52
                Width = 370
                Height = 20
                Align = alTop
                Alignment = taRightJustify
                BevelOuter = bvLowered
                Color = clWindow
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
                object Panel19: TPanel
                  Left = 1
                  Top = 1
                  Width = 50
                  Height = 18
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = 'a(Hm) ='
                  TabOrder = 0
                end
              end
              object Panel20: TPanel
                Left = 0
                Top = 0
                Width = 370
                Height = 4
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 3
              end
              object pn_Hata_Urban_large_200: TPanel
                Left = 0
                Top = 28
                Width = 370
                Height = 20
                Align = alTop
                Alignment = taRightJustify
                BevelOuter = bvLowered
                Color = clWindow
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 4
                object Panel22: TPanel
                  Left = 1
                  Top = 1
                  Width = 50
                  Height = 18
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = 'a(Hm) ='
                  TabOrder = 0
                end
              end
              object Panel6: TPanel
                Left = 0
                Top = 24
                Width = 370
                Height = 4
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 5
              end
            end
          end
          object Panel44: TPanel
            Left = 5
            Top = 169
            Width = 586
            Height = 44
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object pn_Hata_suburban: TPanel
              Left = 0
              Top = 17
              Width = 586
              Height = 20
              Align = alTop
              Alignment = taRightJustify
              BevelOuter = bvLowered
              Color = clWindow
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              object Panel13: TPanel
                Left = 1
                Top = 1
                Width = 50
                Height = 18
                Align = alLeft
                BevelOuter = bvNone
                Caption = 'Lsu ='
                TabOrder = 0
              end
            end
            object Panel4: TPanel
              Left = 0
              Top = 0
              Width = 586
              Height = 17
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvLowered
              Caption = '    Los = Lu - Lsu'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
            end
          end
          object Panel47: TPanel
            Left = 5
            Top = 213
            Width = 586
            Height = 20
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 2
            object Bevel3: TBevel
              Left = 0
              Top = 13
              Width = 586
              Height = 0
              Align = alBottom
              Shape = bsTopLine
            end
            object Bevel4: TBevel
              Left = 0
              Top = 13
              Width = 586
              Height = 7
              Align = alBottom
              Shape = bsTopLine
            end
            object rb_Rural: TRadioButton
              Left = 0
              Top = 4
              Width = 85
              Height = 17
              Caption = ' Rural '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
          end
          object Panel45: TPanel
            Left = 5
            Top = 149
            Width = 586
            Height = 20
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 3
            object Bevel2: TBevel
              Left = 0
              Top = 13
              Width = 586
              Height = 7
              Align = alBottom
              Shape = bsTopLine
            end
            object rb_Suburban: TRadioButton
              Left = 0
              Top = 3
              Width = 85
              Height = 17
              Caption = ' Suburban'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
          end
          object Panel43: TPanel
            Left = 5
            Top = 25
            Width = 586
            Height = 28
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 4
            object Bevel1: TBevel
              Left = 0
              Top = 21
              Width = 586
              Height = 7
              Align = alBottom
              Shape = bsTopLine
            end
            object rb_Urban: TRadioButton
              Left = 0
              Top = 10
              Width = 85
              Height = 17
              Caption = 'Urban'
              Checked = True
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              TabStop = True
            end
          end
          object Panel46: TPanel
            Left = 5
            Top = 233
            Width = 586
            Height = 72
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 5
            object Panel50: TPanel
              Left = 0
              Top = 0
              Width = 586
              Height = 17
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvLowered
              Caption = '     Los = Lu - Lr'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
            object rb_Quasi_open: TRadioButton
              Left = 10
              Top = 25
              Width = 82
              Height = 17
              Caption = 'Quasi-open'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
            end
            object rb_Open_Area: TRadioButton
              Left = 10
              Top = 48
              Width = 78
              Height = 17
              Caption = 'Open Area'
              Checked = True
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              TabStop = True
            end
            object Panel51: TPanel
              Left = 216
              Top = 17
              Width = 370
              Height = 55
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 3
              object pn_Hata_Rural_quasi_open: TPanel
                Left = 0
                Top = 4
                Width = 370
                Height = 20
                Align = alTop
                Alignment = taRightJustify
                BevelOuter = bvLowered
                Color = clWindow
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                object Panel53: TPanel
                  Left = 1
                  Top = 1
                  Width = 50
                  Height = 18
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = 'Lrqo ='
                  TabOrder = 0
                end
              end
              object Panel55: TPanel
                Left = 0
                Top = 24
                Width = 370
                Height = 4
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
              end
              object pn_Hata_Rural_open_area: TPanel
                Left = 0
                Top = 28
                Width = 370
                Height = 20
                Align = alTop
                Alignment = taRightJustify
                BevelOuter = bvLowered
                Color = clWindow
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
                object Panel57: TPanel
                  Left = 1
                  Top = 1
                  Width = 50
                  Height = 18
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = 'Lro ='
                  TabOrder = 0
                end
              end
              object Panel58: TPanel
                Left = 0
                Top = 0
                Width = 370
                Height = 4
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 3
              end
            end
          end
          object pn_HATA_Urban_lu: TPanel
            Left = 5
            Top = 5
            Width = 586
            Height = 20
            Align = alTop
            Alignment = taRightJustify
            BevelOuter = bvLowered
            Color = clWindow
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 6
            object Panel24: TPanel
              Left = 1
              Top = 1
              Width = 52
              Height = 18
              Align = alLeft
              BevelOuter = bvNone
              Caption = 'Lu ='
              TabOrder = 0
            end
          end
        end
      end
      object Memo1: TMemo
        Left = 0
        Top = 351
        Width = 708
        Height = 110
        Align = alBottom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Lines.Strings = (
          'Frequency f:                    150 - 1000 Mhz'
          'Base station height Hb:   30   - 200 m'
          'Mobile height Hm:            1    - 10 m'
          'Distance d:                      1    - 20 km'
          
            'Large and small cells (i.e. base station antenna heights above r' +
            'oot-top levels of buildings adjacent to the '
          'base station')
        ParentColor = True
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    inherited TabSheet_Params: TTabSheet
      inherited cxGrid1: TcxGrid
        Width = 305
        Height = 461
        inherited cxGrid1DBTableView1: TcxGridDBTableView
          inherited cxGrid1DBTableView1name: TcxGridDBColumn
            Width = 142
          end
          inherited cxGrid1DBTableView1value_: TcxGridDBColumn
            Width = 101
          end
        end
      end
    end
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      28
      0)
  end
end
