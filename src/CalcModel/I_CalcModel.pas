unit I_CalcModel;

interface
{$DEFINE dll__}

uses
  u_classes,
  u_calcparams
  ;

const
                                             
  MODEL_DET      = 0;
  MODEL_HATA567  = 1;
  MODEL_COST231  = 2;
  MODEL_DEGO     = 3;
  MODEL_BERTONI  = 4;
  MODEL_METOD_B  = 5;
  MODEL_INDOOR   = 6;
 // MODEL_LOS      = 7;

var
  CALC_MODELS : array [0..6] of record
    Name: string; ID: integer;
   end =
  (
   (Name: '0. Детерминированная';      ID: MODEL_DET),
   (Name: '1. Rec.567 (Okumura-Hata)'; ID: MODEL_HATA567),
   (Name: '2. Cost. 231';              ID: MODEL_COST231),
   (Name: '3. Dego';                   ID: MODEL_DEGO),
   (Name: '4. Ksj-Bertoni';            ID: MODEL_BERTONI),
   (Name: '5. Metod B';                ID: MODEL_METOD_B),
   (Name: '6. Indoor';                 ID: MODEL_INDOOR)//,
//   (Name: '7. LOS';                    ID: MODEL_LOS)
   );


type
  //-------------------------------------------------------------------
  TdmCalcModelAddRec = packed record
  //-------------------------------------------------------------------
    Name     : string;
    FolderID : integer;
    Type_    : integer;

    K0,K0_open,K0_closed: double;

  end;

(*
  ICalcModelX = interface(IInterface)

  //  function GetNameByID (aID: integer): string;
 //   function Add_ExecDlg (aFolderID: integer): integer;
//    function Copy_ExecDlg (aID: integer; var aFolderID: integer): integer;

    function Add (aRec: TdmCalcModelAddRec): integer;
    function Del (aID: integer): boolean;

    function GetType (aID: integer): integer;

    function GetK0_open_close (aID: integer; var aK0,aK0_open,aK0_closed: double): boolean;

    function GetIDByName (aName: string): integer;
    function GetNameByID (aID: integer): string;

    procedure SaveToXMLFile(aID: integer; aFileName: string);
    procedure SaveToXMLNode(aIDList: TIDList; aParentNode: Variant);
    procedure SaveToXMLNodeByID(aID: integer; aParentNode: Variant);

    procedure LoadParams (aID: integer; aParams: TcmParamList);
    procedure SaveParams (aID: integer; aParams: TcmParamList);

  end;


  function dmCalcModel: ICalcModelX;

*)

// ==================================================================
implementation
// ==================================================================

(*uses
  x_CalcModel;
*)

(*var
  FdmCalcModel: ICalcModelX;
*)
(*
// ------------------------------------------------------------------
function dmCalcModel: ICalcModelX;
// ------------------------------------------------------------------
begin
  if not Assigned(FdmCalcModel) then
    FdmCalcModel := TCalcModelX.Create();

  Result := FdmCalcModel;
end;


begin
  FdmCalcModel := nil;
*)
end.


