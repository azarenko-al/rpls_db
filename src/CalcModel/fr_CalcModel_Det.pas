unit fr_CalcModel_Det;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,  DB, ExtCtrls, cxPropertiesStore, cxGridLevel,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls, cxGridCustomTableView,
  ADODB, ActnList,  ToolWin, ComCtrls,  Variants,

  cxDBVGrid,  cxDropDownEdit, cxVGrid,


  XMLDoc, XMLIntf, u_xml_document,

  u_func,
  u_dlg,

 // u_XML,

  u_db,
  u_const_db,                                   

  u_calcparams,

  u_Classes,

  u_CalcModel_det_const,

  fr_CalcModel_model_base,

  fr_Inspector_Base,

  cxGridCustomView, cxGrid, dxBar
  ;

type
  Tframe_CalcModel_Det = class(Tframe_CalcModel_model_base)
    Memo1: TMemo;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

  private
    Ffrm_Inspector_Base: Tframe_Inspector_Base;

    FXMLDoc: TXMLDoc;

  //  procedure DoOnChangeNode (aInspectorField: TInspectorField);
    procedure LoadRowsFromXML (aParentNode: IXMLNode);
  public
    function Modified: boolean; override;

    procedure LoadFromParams (aParams: TcmParamList); override;
    procedure SaveToParams   (aParams: TcmParamList); override;

  end;


//=====================================================
implementation {$R *.DFM}

uses
  u_cx_DBVGrid_manager;

//=====================================================

//----------------------------------------------------
procedure Tframe_CalcModel_Det.FormCreate(Sender: TObject);
//----------------------------------------------------
begin
  inherited;

//  CreateChildForm(Tframe_Inspector_Base, Ffrm_Inspector_Base, Self);
  CreateChildForm(Tframe_Inspector_Base, Ffrm_Inspector_Base, TabSheet_Setup);
//  Ffrm_Inspector_Base:=Tframe_Inspector_Base.Create(Self);
  //CopyControls (Ffrm_Inspector_Base, ts_Setup);

//  Ffrm_Inspector_Base.OnChangeNode:=DoOnChangeNode;

  FXMLDoc:=TXMLDoc.Create;
  FXMLDoc.LoadFromText (MODEL_DET_TEMPLATE_XML);

  LoadRowsFromXML (FXMLDoc.DocumentElement);
end;


procedure Tframe_CalcModel_Det.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FXMLDoc);


  inherited;
end;



//----------------------------------------------------
procedure Tframe_CalcModel_Det.LoadRowsFromXML (aParentNode: IXMLNode);
//----------------------------------------------------
var
  vRoot,vNode: IXMLNode;
  i: integer;


  //----------------------------------------------------
//  procedure DoProcessNodes (vParentNode: IXMLNode; aParentRow: TdxInspectorRow);
  procedure DoProcessNodes (vParentNode: IXMLNode; aParentRow: TcxCustomRow);
  //----------------------------------------------------
  var iID,i,j: integer;
      vNode,vNode1: IXMLNode;
      sField,sValue,sCaption,sComment: string;
      sTag,sCaption1: string;
      iValue: integer;
    //  oRow: TdxInspectorRow;
    //  oInspectorField: TInspectorField;
      oStrList: TStringList;

      oCustomRow: TcxCustomRow;
    oRowData: TRowData;

  begin
    oStrList:=TStringList.Create;

    if not VarIsEmpty(vParentNode) then
    for i:=0 to vParentNode.ChildNodes.Count-1 do
    begin
      vNode:=vParentNode.ChildNodes[i];
//      vNode.no
      if not xml_IsElement(vNode) then
        Continue;

      oStrList.Clear;

      sTag:=vNode.NodeName; // nodeTypeString;
      if sTag='#comment' then Continue;


      sField  :=xml_GetAttr (vNode, 'id');
      sValue  :=xml_GetAttr (vNode, 'value');
      sCaption:=xml_GetAttr (vNode, 'name');
      sComment:=xml_GetAttr (vNode, 'comment');

      //test
      iID:=xml_GetIntAttr (vNode, 'id');
      if iID>0 then
        sCaption:=sCaption + Format(' [%d]', [iID]);


      //---------------------------------------------------
      if Eq(sTag,'GROUP') then begin
      //---------------------------------------------------
        oCustomRow := Ffrm_Inspector_Base.GridManager.AddCategory(aParentRow,sCaption);

       // oCustomRow.

//        TcxCustomRow


      //  oRow:=Ffrm_Inspector_Base.AddRow (aParentRow, ftNone, sCaption, '');
      //  TInspectorField(oRow.Tag).Comment:=sComment;

        DoProcessNodes (vNode,oCustomRow);
      //  DoProcessNodes (vNode,oRow);
      end else

      //---------------------------------------------------
      if Eq(sTag,'PARAM') then
      //---------------------------------------------------
      begin
        for j:=0 to vNode.ChildNodes.Count-1 do
        begin
          vNode1:=vNode.ChildNodes[j];
          if not xml_IsElement(vNode) then Continue;

          iValue := StrToIntDef (xml_GetAttr (vNode1, 'value'), 0);
          sCaption1:=xml_GetAttr (vNode1, 'name');

          oStrList.AddObject (sCaption1, Pointer(iValue));
        end;


        if oStrList.Count>0 then
          oCustomRow := Ffrm_Inspector_Base.GridManager.AddComboBox(aParentRow,sCaption, oStrList)
        else
          oCustomRow := Ffrm_Inspector_Base.GridManager.AddRow_Float(aParentRow,sCaption);


        oRowData:=Ffrm_Inspector_Base.GridManager.GetRowData(oCustomRow);

        oRowData.FieldName := sField;

        Ffrm_Inspector_Base.GridManager.SetFieldValue(sField, sValue);


         // oCustomRow

   //            oCustomRow := Ffrm_Inspector_Base.DBVGridManager.AddCategory(aParentRow,sCaption);

//           oCustomRow.DefaultValue := sValue;


//
//          oRow:=Ffrm_Inspector_Base.AddRow (aParentRow, ftFixedList, sCaption, sField, oStrList)
//        else
//          oRow:=Ffrm_Inspector_Base.AddRow (aParentRow, ftFloat, sCaption, sField);
//
//
//        oInspectorField:=TInspectorField(oRow.Tag);
//        oInspectorField.Comment:=sComment;
//
//        oInspectorField.DefaultValue := sValue;


      end;
    end;

    oStrList.Free;
  end;
  //----------------------------------------------------

begin
  Ffrm_Inspector_Base.GridManager.ClearRows;

//  TcxCustomRow


 // Ffrm_Inspector_Base.ClearNodes;

  if not VarIsNull(aParentNode) then
    DoProcessNodes (aParentNode, nil);


//  Ffrm_Inspector_Base.GotoTop;
//  Ffrm_Inspector_Base.RestoreDefaultValues;


end;


//----------------------------------------------------
procedure Tframe_CalcModel_Det.LoadFromParams(aParams: TcmParamList);
//----------------------------------------------------
var i: integer;
begin

  with aParams do
    for i:=0 to Count-1 do
      Ffrm_Inspector_Base.GridManager.SetFieldValue (Items[i].Name, Items[i].Value);



//  with aParams do
//    for i:=0 to Count-1 do
//      Ffrm_Inspector_Base.SetFieldValue (Items[i].Name, Items[i].Value);



 ////// Ffrm_Inspector_Base.dxInspector.Refresh;
  
end;

//----------------------------------------------------
procedure Tframe_CalcModel_Det.SaveToParams(aParams: TcmParamList);
//----------------------------------------------------
var i: integer;
    v: Variant;

    oStrList: TStringList;

  //  oField: TInspectorField;
begin
  oStrList:=TStringList.Create;


 (* aParams.Clear;

  with Ffrm_Inspector_Base.dxInspector do
  begin
    HideEditor;

    for i:=0 to TotalRowCount-1 do
    begin
      oField:=TInspectorField(Rows[i].Tag);

      if oField.FieldName<>'' then
      begin
        v := Ffrm_Inspector_Base.GetFieldValue(oField.FieldName);

        aParams.AddItem (oField.FieldName, v);
      end;
    end;
  end;

  Ffrm_Inspector_Base.Modified:=False;

*)

  aParams.Clear;

  Ffrm_Inspector_Base.GridManager.GetFieldNames (oStrList);


  for i:=0 to oStrList.Count-1 do
  begin
    v := Ffrm_Inspector_Base.GridManager.GetFieldValue(oStrList[i]);

    aParams.AddItem (oStrList[i], v);

  end;


  FreeAndNil(oStrList);


//
//
// with Ffrm_Inspector_Base do
//  begin
// //   HideEditor;
//
//    for i:=0 to TotalRowCount-1 do
//    begin
//      oField:=TInspectorField(Rows[i].Tag);
//
//      if oField.FieldName<>'' then
//      begin
//        v := Ffrm_Inspector_Base.GetFieldValue(oField.FieldName);
//
//        aParams.AddItem (oField.FieldName, v);
//      end;
//    end;
//  end;

  Ffrm_Inspector_Base.Modified:=False;


end;


function Tframe_CalcModel_Det.Modified: boolean;
begin
  Result:=Ffrm_Inspector_Base.Modified;
end;



end.

//
//
//procedure Tframe_CalcModel_Det.DoOnChangeNode (aInspectorField: TInspectorField);
//begin
//
//(*  if Assigned(aInspectorField) then
//    Memo1.Text:=Format('debug - [fld:%s] ',[aInspectorField.FieldName]) + CRLF
//                + 'comment: ' + aInspectorField.Comment;
//*)
//end;
