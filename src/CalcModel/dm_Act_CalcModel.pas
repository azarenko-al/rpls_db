unit dm_Act_CalcModel;

interface

uses
  Windows, Messages, SysUtils, Classes, ADODB, ActnList, Menus,
  Graphics, Controls, Forms, Dialogs,


    i_Audit,


 // d_Audit,

  dm_User_Security,

  u_DataExport_run,


  dm_Onega_DB_data,

  I_Object,
  I_CalcModel,
  dm_CalcModel,
  dm_CalcModel_export,

  I_Shell,
  u_Shell_new,

  u_types,
  u_classes,

  u_const_db,
  u_const,
  u_const_str,
  u_const_msg,

  u_func,
  u_func_msg,
  u_dlg,

  fr_CalcModel_view,
  d_CalcModel_add,


  dm_act_Base
  ;


type
  TdmAct_CalcModel = class(TdmAct_Base)
    act_Copy: TAction;
    act_ExportToXML: TAction;
    act_ImportFromXML: TAction;
    OpenDialog1: TOpenDialog;
    SaveDialog_XML: TSaveDialog;
    act_ExportAllToXML: TAction;
    ActionList2: TActionList;
    act_Export_MDB: TAction;
    act_Audit: TAction;
    procedure DataModuleCreate(Sender: TObject);
  private
    function CheckActionsEnable: Boolean;
    function  GetViewForm  (aOwnerForm: TForm): TForm; override;
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;

    procedure DoAction (Sender: TObject);

    function ItemDel(aID: integer; aName: string): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;

  public
    procedure ExportToXML (aID: integer);
//    procedure ImportFromXML (aFolderID: integer);

    procedure Copy (aID: integer);

    class procedure Init;
  end;

var
  dmAct_CalcModel: TdmAct_CalcModel;


//====================================================================
implementation

uses dm_Main; {$R *.dfm}
//====================================================================

//--------------------------------------------------------------------
class procedure TdmAct_CalcModel.Init;
//--------------------------------------------------------------------
begin
//  Assert(not Assigned(dmAct_CalcModel));

  if not Assigned(dmAct_CalcModel) then
    dmAct_CalcModel:=TdmAct_CalcModel.Create(Application);
end;

//--------------------------------------------------------------------
procedure TdmAct_CalcModel.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_CALC_MODEL;

  act_Copy.Caption         := STR_ACT_COPY;
  act_ExportToxml.Caption  := STR_EXPORT_TO_XML;
  act_ImportFromxml.Caption:= STR_IMPORT_FROM_XML;

  act_Export_MDB.Caption:=DEF_STR_Export_MDB;

  act_Audit.Caption:=STR_Audit;


{
  act_Audit.Caption:=STR_Audit;
  act_Audit.Enabled:=False;
}

  SetActionsExecuteProc
     ([
        act_Export_MDB,
       act_Copy,
       act_ExportToXML,
       act_ExportAllToXML,
       act_ImportFromXML,

       act_Audit
       ],
      DoAction);
end;


//--------------------------------------------------------------------
function TdmAct_CalcModel.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_CalcModel_add.ExecDlg (aFolderID);
end;

//--------------------------------------------------------------------
procedure TdmAct_CalcModel.Copy (aID: integer);
//--------------------------------------------------------------------
var iID,iFolderID: integer;
  sNewName: string;
begin
  sNewName:=FFocusedName +' (�����)';

//xx  if Sender=act_Copy then begin
  if InputQuery ('����� ������','������� ����� ��������', sNewName) then
  begin
//    iID:=dmCalcModel_.Copy (FFocusedID, sNewName);
    iID:=dmOnega_DB_data.CalcModel_Copy(FFocusedID, sNewName);

    g_Shell.UpdateNodeChildren_ByGUID(GUID_CALC_MODEL);
 end;

// end;

{  iID:=Tdlg_CalcModel_add.CopyDlg (aID, iFolderID);

//  iID:=dmCalcModel.Copy_ExecDlg (aID, iFolderID);
  if iID>0 then begin
    PostUpdateFolderAndFocusNode (iFolderID, iID);
  end;
}

end;

{
function TCalcModelX.Copy_ExecDlg(aID: integer;  var aFolderID: integer): integer;
begin
   Result := Tdlg_CalcModel_add.CopyDlg (aID, aFolderID);
end;
}

//--------------------------------------------------------------------
function TdmAct_CalcModel.ItemDel(aID: integer; aName: string): boolean;
//--------------------------------------------------------------------
begin
  Result:=dmCalcModel.Del (aID) > 0;
end;

//--------------------------------------------------------------------
procedure TdmAct_CalcModel.ExportToXML (aID: integer);
//--------------------------------------------------------------------
begin
  SaveDialog_xml.FileName:='CalcModel';
  if SaveDialog_xml.Execute then
    dmCalcModel_export.SaveToXMLFile (aID, SaveDialog_xml.FileName);
end;
//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_CalcModel.DoAction (Sender: TObject);
begin

  if Sender=act_Audit then
     Init_IAuditX.ExecDlg( dmMain.GetConnectionString, TBL_CalcModel, FFocusedID) else

{
  if Sender=act_Audit then
     dlg_Audit(TBL_CalcModel, FFocusedID) else
}

  if Sender=act_Export_MDB then
    TDataExport_run.ExportToRplsDb_selected (TBL_CalcModel, FSelectedIDList);


  if Sender=act_Copy then Copy (FFocusedID) else

  if Sender=act_ExportToXML then ExportToXML (FFocusedID) else
  if Sender=act_ExportAllToXML then ExportToXML (-1) else

//  if Sender=act_ImportFromXML then ImportFromXML (FFocusedID) else

end;


//--------------------------------------------------------------------
function TdmAct_CalcModel.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_CalcModel_View.Create(aOwnerForm);
end;

// ---------------------------------------------------------------
function TdmAct_CalcModel.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
  Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];

  SetActionsEnabled1( [
          act_Del_,
          act_Del_list,
          act_Copy
      ],

   Result );


  //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );


end;


//--------------------------------------------------------------------
procedure TdmAct_CalcModel.GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType);
//--------------------------------------------------------------------
var
  bEnable: Boolean;
begin
  bEnable:=CheckActionsEnable();


  case aMenuType of
    mtFolder: begin
                AddFolderMenu_Create (aPopupMenu, bEnable);
              //   AddMenuItem (aPopupMenu, act_ImportFromXML);
                AddFolderPopupMenu (aPopupMenu, bEnable);
              end;
    mtItem:   begin
                AddMenuItem (aPopupMenu, act_Del_);
                AddMenuItem (aPopupMenu, act_Copy);
             //   AddMenuItem (aPopupMenu, act_ExportToXML);

                AddFolderMenu_Tools (aPopupMenu);
                AddMenuItem (aPopupMenu, act_Export_MDB);

                AddMenuItem (aPopupMenu, act_Audit);

              end;
    mtList:   begin
                AddMenuItem (aPopupMenu, act_Del_list);
                AddMenuDelimiter (aPopupMenu);
                AddFolderMenu_Tools (aPopupMenu);
                AddMenuItem (aPopupMenu, act_Export_MDB);

              end;
  end;
end;


begin
 
end.
