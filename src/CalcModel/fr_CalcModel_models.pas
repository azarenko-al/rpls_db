unit fr_CalcModel_models;

interface
uses
  SysUtils, Classes, Forms, StdCtrls, ExtCtrls, Graphics, ComCtrls, Controls,
  ActnList, ToolWin, Dialogs,

  dm_CalcModel,
  I_CalcModel,

  u_Classes,

  u_func,

  fr_CalcModel_model_base,
  fr_CalcModel_Det,
  fr_CalcModel_Cost231,
  fr_CalcModel_HATA567;


type
  Tframe_CalcModel_models = class(TForm)
    pn_Main: TPanel;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FType: integer;

    Ffrm_View: Tframe_CalcModel_model_base;

    procedure SelectModelView (aType: integer);
  public
    procedure View (aID: integer);
  end;


//=========================================================================
implementation {$R *.DFM}
//=========================================================================


procedure Tframe_CalcModel_models.FormCreate(Sender: TObject);
begin
  inherited;
  pn_Main.Align:=alClient;
  FType:=-1;
end;


procedure Tframe_CalcModel_models.FormDestroy(Sender: TObject);
begin
  SelectMOdelView (-1);
  inherited;
end;


//-------------------------------------------------------
procedure Tframe_CalcModel_models.View (aID: integer);
//-------------------------------------------------------
var iType: integer;
begin
  iType:=dmCalcModel.GetType (aID);
  SelectModelView (iType);

  if Assigned(Ffrm_View) then
    Ffrm_View.View (aID);
end;



//-------------------------------------------------------
procedure Tframe_CalcModel_models.SelectModelView (aType: integer);
//-------------------------------------------------------
begin
//  ShowMessage('1');

  if FType=aType then Exit
                 else FType:=aType;

  if Assigned(Ffrm_View) then begin
    FreeAndNil(Ffrm_View);
  end;

//  ShowMessage('2');

  case FType of
    MODEL_DET,
    MODEL_BERTONI,
    MODEL_METOD_B,
    MODEL_INDOOR//,
    //MODEL_LOS
                   : Ffrm_View:=Tframe_CalcModel_Det.Create(Self);

//    , MODEL_B
    MODEL_COST231  : Ffrm_View:=Tframe_CalcModel_Cost231.Create(Self);
    MODEL_HATA567  : Ffrm_View:=Tframe_CalcModel_HATA567.Create(Self);
  else
    Exit;
  end;

  CopyDockingEx(Ffrm_View, pn_Main);
//  CopyControls (Ffrm_View, pn_Main);

  {
  Ffrm_View.ManualDock (pn_Main);
  Ffrm_View.Align:= alClient;
  pn_Main.DockSite:=False;
  Ffrm_View.Show;
  }


//  CopyControls (Ffrm_View, GSPage1);

 // ShowMessage('3');

end;



end.


