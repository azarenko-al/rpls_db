inherited frame_CalcModel_View_simple: Tframe_CalcModel_View_simple
  Left = 199
  Top = 242
  Width = 501
  Height = 413
  Caption = 'frame_CalcModel_View_simple'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 493
    Height = 68
  end
  inherited pn_Caption: TPanel
    Top = 93
    Width = 493
  end
  inherited pn_Main: TPanel
    Top = 110
    Width = 493
  end
  inherited MainActionList: TActionList
    object act_SaveToXML: TAction
      Caption = 'Export to XML'
    end
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      25
      0)
  end
  object OpenDialog1: TOpenDialog
    Left = 136
    Top = 8
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredProps.Strings = (
      'OpenDialog1.FileName'
      'OpenDialog1.InitialDir')
    StoredValues = <>
    Left = 164
    Top = 8
  end
  object SaveDialog1: TSaveDialog
    Left = 224
    Top = 9
  end
  object qry_Params: TADOQuery
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'id'
        DataType = ftString
        Size = 1
        Value = '5'
      end>
    SQL.Strings = (
      'select * from linkline where id=:id')
    Left = 324
    Top = 4
  end
  object ds_Params: TDataSource
    DataSet = qry_Params
    Left = 356
    Top = 4
  end
end
