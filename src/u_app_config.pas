unit u_app_config;

interface
uses
  IniFiles, SysUtils;


type
  TApp_config = class(TObject)
  public
    Login_IsAuto : boolean;

    procedure LoadFromFile(aFileName: string);
  end;

var
  g_App_config: TApp_config;


implementation


// ---------------------------------------------------------------
procedure TApp_config.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
var
  oIniFile: TIniFile;

begin
  Assert (FileExists(aFileName), 'FileExists: '+ aFileName);

//  if not FileExists(aFileName) then
//    Exit;

  oIniFile := TIniFile.Create(aFileName);

//  IDList.LoadIDsFromIniSection(oIniFile, 'items');
 // LinkLineIDList.LoadIDsFromIniSection (oIniFile, 'LinkLines');

  with oIniFile do
  begin
    Login_IsAuto := 'auto'= LowerCase(ReadString ('login', 'login', ''));

  end;

  FreeAndNil(oIniFile);


  // TODO -cMM: TApp_config.LoadFromFile default body inserted
end;

begin
  g_App_config:=TApp_config.Create;


end.

{
[idle]
;�����
time_minutes=1
msg=������ ��������� ��������� ��-�� ������������: %s

[login]
login=auto


//--------------------------------------------------------------
procedure TIni_Link_Report_Param.LoadFromINI(aFileName: string);
//--------------------------------------------------------------
var
  oIniFile: TIniFile;
 // sCategory: string;
  sScale: string;

begin
  if not FileExists(aFileName) then
    Exit;

  oIniFile := TIniFile.Create(aFileName);

  IDList.LoadIDsFromIniSection(oIniFile, 'items');
 // LinkLineIDList.LoadIDsFromIniSection (oIniFile, 'LinkLines');

  with oIniFile do
  begin
    IsTest   :=ReadBool   ('main', 'IsTest', False);

    ConnectionString :=ReadString ('main', 'ConnectionString', '');


    Category  :=ReadString ('main', 'Category', 'link');

    Category_Type := ctLink;

    if Eq(Category,'link')     then Category_Type := ctLink else
    if Eq(Category,'linkline') then Category_Type := ctLinkLIne;


  //  Category_Type  : (ctLink,ctLinkLIne);


    ProjectID        :=ReadInteger('main', 'ProjectID',   0);

  //  FileDir          :=ReadString ('main', 'FileDir', '');
 //   FileDir          :=IncludeTrailingBackslash(FileDir);

//    ID               :=ReadInteger('main', 'ID', 0);
  //  ObjName          :=ReadString ('main', 'ObjName', '');

    Report_ID         :=ReadInteger('main', 'ReportID',    0);
//    ReportFileName   :=ReadString ('main', 'ReportFileName', '');
  //  Action           :=ReadString ('main', 'Action',      '');
  //  LinkLineID       :=ReadInteger('main', 'LinkLineID',  0);

