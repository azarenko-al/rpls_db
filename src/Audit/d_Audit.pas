unit d_Audit;

interface

uses
  u_db,

  dm_Onega_DB_data,

  forms,

  cxGridDBTableView, cxClasses, cxControls, rxPlacemnt, cxLookAndFeels, DB,
  ADODB, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridCustomView, Classes, Controls, cxGrid, cxInplaceContainer, cxTL,
  cxDBTL, cxTLData;

type
  Tdlg_Audit_ = class(TForm)
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    ADOConnection1: TADOConnection;
    ds_Data: TDataSource;
    ADOStoredProc1: TADOStoredProc;
    cxGridDBTableView1Row_num: TcxGridDBColumn;
    cxGridDBTableView1id: TcxGridDBColumn;
    cxGridDBTableView1action: TcxGridDBColumn;
    cxGridDBTableView1table_name: TcxGridDBColumn;
    cxGridDBTableView1record_id: TcxGridDBColumn;
    cxGridDBTableView1project_id: TcxGridDBColumn;
    cxGridDBTableView1property_id: TcxGridDBColumn;
    cxGridDBTableView1xml_data: TcxGridDBColumn;
    cxGridDBTableView1user_created: TcxGridDBColumn;
    cxGridDBTableView1date_created: TcxGridDBColumn;
    cxGridDBTableView1name: TcxGridDBColumn;
    col_old_value: TcxGridDBColumn;
    cxGridDBTableView1new_value: TcxGridDBColumn;
    cxLookAndFeelController1: TcxLookAndFeelController;
    FormPlacement1: TFormPlacement;
    cxDBTreeList1: TcxDBTreeList;
    cxDBTreeList1Row_Num: TcxDBTreeListColumn;
    cxDBTreeList1parent_id: TcxDBTreeListColumn;
    cxDBTreeList1id: TcxDBTreeListColumn;
    cxDBTreeList1action: TcxDBTreeListColumn;
    cxDBTreeList1table_name: TcxDBTreeListColumn;
    cxDBTreeList1record_id: TcxDBTreeListColumn;
    cxDBTreeList1project_id1111111: TcxDBTreeListColumn;
    cxDBTreeList1property_id111111111: TcxDBTreeListColumn;
    cxDBTreeList1user_created: TcxDBTreeListColumn;
    cxDBTreeList1date_created: TcxDBTreeListColumn;
    cxDBTreeList1record_parent_id11111111111: TcxDBTreeListColumn;
    cxDBTreeList1action_str: TcxDBTreeListColumn;
    cxDBTreeList1column_name: TcxDBTreeListColumn;
    cxDBTreeList1old_value: TcxDBTreeListColumn;
    cxDBTreeList1new_value: TcxDBTreeListColumn;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class procedure ExecDlg(aTable_Name: string; aID: Integer);

  end;

//var
//  dlg_Audit: Tdlg_Audit;

  
implementation

{$R *.dfm}

procedure Tdlg_Audit_.FormCreate(Sender: TObject);
begin
  Caption:='�����';
  cxGrid2.Align:=alClient;

  db_SetFieldCaptions(ADOStoredProc1,
  [
    'column_name', '����'

  ]);

end;



//-----------------------------------------------------------------
class procedure Tdlg_Audit_.ExecDlg(aTable_Name: string; aID: Integer);
//-----------------------------------------------------------------
begin
  with Tdlg_Audit.Create(Application) do
  begin

    dmOnega_DB_data.OpenStoredProc ( ADOStoredProc1, 'history.sp_Record_History_SEL',
        [
          'table_name', aTable_Name,
          'record_id', aID
        ] );


     ShowModal;

    Free;
  end;
end;

end.


{



procedure Tdlg_Audit.cxGridDBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
//  ARecord.Index;
end;

}
