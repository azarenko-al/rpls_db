object dlg_Audit_: Tdlg_Audit_
  Left = 1028
  Top = 327
  Width = 731
  Height = 531
  Caption = 'dlg_Audit_'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid2: TcxGrid
    Left = 0
    Top = 336
    Width = 723
    Height = 167
    Align = alBottom
    TabOrder = 0
    object cxGridDBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = ds_Data
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object cxGridDBTableView1Row_num: TcxGridDBColumn
        DataBinding.FieldName = 'Row_num'
        Width = 54
      end
      object cxGridDBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
      end
      object cxGridDBTableView1action: TcxGridDBColumn
        DataBinding.FieldName = 'action'
      end
      object cxGridDBTableView1table_name: TcxGridDBColumn
        DataBinding.FieldName = 'table_name'
        Width = 91
      end
      object cxGridDBTableView1record_id: TcxGridDBColumn
        DataBinding.FieldName = 'record_id'
      end
      object cxGridDBTableView1project_id: TcxGridDBColumn
        DataBinding.FieldName = 'project_id'
      end
      object cxGridDBTableView1property_id: TcxGridDBColumn
        DataBinding.FieldName = 'property_id'
      end
      object cxGridDBTableView1xml_data: TcxGridDBColumn
        DataBinding.FieldName = 'xml_data'
      end
      object cxGridDBTableView1user_created: TcxGridDBColumn
        DataBinding.FieldName = 'user_created'
        Width = 81
      end
      object cxGridDBTableView1date_created: TcxGridDBColumn
        DataBinding.FieldName = 'date_created'
      end
      object cxGridDBTableView1name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Width = 91
      end
      object col_old_value: TcxGridDBColumn
        DataBinding.FieldName = 'old_value'
        Width = 134
      end
      object cxGridDBTableView1new_value: TcxGridDBColumn
        DataBinding.FieldName = 'new_value'
        Width = 122
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object cxDBTreeList1: TcxDBTreeList
    Left = 0
    Top = 160
    Width = 723
    Height = 176
    Align = alBottom
    Bands = <
      item
      end>
    DataController.DataSource = ds_Data
    DataController.ParentField = 'parent_id'
    DataController.KeyField = 'id'
    RootValue = -1
    TabOrder = 1
    object cxDBTreeList1Row_Num: TcxDBTreeListColumn
      DataBinding.FieldName = 'Row_Num'
      Width = 65
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1parent_id: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'parent_id'
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1id: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'id'
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1action: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'action'
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1table_name: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'table_name'
      Width = 100
      Position.ColIndex = 5
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1record_id: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'record_id'
      Position.ColIndex = 6
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1project_id1111111: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'project_id1111111'
      Position.ColIndex = 7
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1property_id111111111: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'property_id111111111'
      Position.ColIndex = 8
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1user_created: TcxDBTreeListColumn
      DataBinding.FieldName = 'user_created'
      Width = 100
      Position.ColIndex = 9
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1date_created: TcxDBTreeListColumn
      DataBinding.FieldName = 'date_created'
      Width = 100
      Position.ColIndex = 10
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1record_parent_id11111111111: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'record_parent_id11111111111'
      Position.ColIndex = 11
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1action_str: TcxDBTreeListColumn
      DataBinding.FieldName = 'action_str'
      Width = 79
      Position.ColIndex = 4
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1column_name: TcxDBTreeListColumn
      DataBinding.FieldName = 'column_name'
      Width = 100
      Position.ColIndex = 12
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1old_value: TcxDBTreeListColumn
      DataBinding.FieldName = 'old_value'
      Width = 100
      Position.ColIndex = 13
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList1new_value: TcxDBTreeListColumn
      DataBinding.FieldName = 'new_value'
      Width = 100
      Position.ColIndex = 14
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=SERVER1;Use Procedu' +
      're for Prepare=1;Auto Translate=True;Packet Size=4096;Workstatio' +
      'n ID=ALEX;Use Encryption for Data=False;Tag with column collatio' +
      'n when possible=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 56
  end
  object ds_Data: TDataSource
    DataSet = ADOStoredProc1
    Left = 265
    Top = 56
  end
  object ADOStoredProc1: TADOStoredProc
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'history.sp_Record_History_SEL'
    Parameters = <>
    Left = 272
  end
  object cxLookAndFeelController1: TcxLookAndFeelController
    Kind = lfFlat
    NativeStyle = True
    SkinName = 'Office2013LightGray'
    Left = 401
  end
  object FormPlacement1: TFormPlacement
    IniFileName = 'Software\Onega\'
    Left = 576
  end
end
