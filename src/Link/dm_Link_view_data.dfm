object dmLink_view_data: TdmLink_view_data
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 1277
  Top = 381
  Height = 511
  Width = 643
  object ADOStoredProc_Link: TADOStoredProc
    Parameters = <>
    Left = 72
    Top = 16
  end
  object ds_LInk: TDataSource
    DataSet = ADOStoredProc_Link
    OnDataChange = ds_LInkDataChange
    Left = 72
    Top = 80
  end
  object ADOStoredProc_Left: TADOStoredProc
    Parameters = <>
    Left = 272
    Top = 16
  end
  object ADOStoredProc_Right: TADOStoredProc
    Parameters = <>
    Left = 272
    Top = 80
  end
  object q_LInk_repeater: TADOQuery
    Parameters = <>
    Left = 408
    Top = 16
  end
  object ds_LInk_repeater: TDataSource
    DataSet = q_LInk_repeater
    OnDataChange = ds_LInkDataChange
    Left = 408
    Top = 72
  end
  object q_LInk_repeater_ant: TADOQuery
    DataSource = ds_LInk_repeater
    Parameters = <>
    Left = 520
    Top = 16
  end
  object ds_LInk_repeater_ant: TDataSource
    DataSet = q_LInk_repeater_ant
    OnDataChange = ds_LInkDataChange
    Left = 520
    Top = 72
  end
  object ds_reflection_POINTS: TDataSource
    DataSet = qry_reflection_POINTS_
    Left = 272
    Top = 232
  end
  object qry_reflection_POINTS_: TADOQuery
    LockType = ltReadOnly
    Parameters = <>
    Left = 272
    Top = 172
  end
  object qry_LinkEnd1: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT    *'
      'FROM      Property'
      'ORDER BY name')
    Left = 412
    Top = 164
  end
  object qry_LinkEnd2: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT    *'
      'FROM      Property'
      'ORDER BY name')
    Left = 516
    Top = 164
  end
  object ds_LinkEnd2: TDataSource
    DataSet = qry_LinkEnd2
    OnDataChange = ds_LInkDataChange
    Left = 520
    Top = 232
  end
  object ds_LinkEnd1: TDataSource
    DataSet = qry_LinkEnd1
    OnDataChange = ds_LInkDataChange
    Left = 416
    Top = 232
  end
  object qry_Antennas1: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT     dbo.LinkEndAntenna.*, dbo.AntennaType.*'
      'FROM         dbo.AntennaType RIGHT OUTER JOIN'
      
        '                      dbo.LinkEndAntenna ON dbo.AntennaType.id =' +
        ' dbo.LinkEndAntenna.antennaType_id')
    Left = 76
    Top = 168
  end
  object qry_Antennas2: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT     dbo.LinkEndAntenna.*, dbo.AntennaType.*'
      'FROM         dbo.AntennaType RIGHT OUTER JOIN'
      
        '                      dbo.LinkEndAntenna ON dbo.AntennaType.id =' +
        ' dbo.LinkEndAntenna.antennaType_id')
    Left = 80
    Top = 224
  end
  object ds_TN_Links: TDataSource
    DataSet = mem_TN_links
    Left = 287
    Top = 391
  end
  object mem_TN_links: TdxMemData
    Indexes = <>
    SortOptions = []
    SortedField = 'code'
    Left = 287
    Top = 334
  end
  object ds_Results: TDataSource
    DataSet = qry_Results
    Left = 384
    Top = 388
  end
  object qry_Results: TADOQuery
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select * from LinkCalcResults')
    Left = 388
    Top = 340
  end
end
