unit fr_Link_calc_results;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  cxStyles, cxInplaceContainer, cxTL, cxDBTL, cxControls, ComCtrls, Registry,
  cxTLData, cxPropertiesStore, cxGraphics, DB,   ActnList, ADODB, ToolWin,

  u_Vars,


  u_const_msg,

  u_link_classes1,

  dm_Link_view_data,


  u_cx_TreeList,

  u_LinkCalcResult_new,

  u_Storage,

  dm_Onega_DB_data,

  dm_Main,

  Variants,

  u_log,

 // f_Custom,

  u_func,
  u_db,

  u_const_db,
  u_const,
  u_const_str,

   dxmdaset, Menus, Grids, DBGrids, AppEvnts, ExtCtrls, StdCtrls, dxBar,
  dxBarExtDBItems, cxClasses
  ;

type
  Tframe_Link_calc_results = class(TForm)
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    PopupMenu1: TPopupMenu;
    ActionList1: TActionList;
    act_Collapse: TAction;
    act_Expand: TAction;
    Action11: TMenuItem;
    Action12: TMenuItem;
    ADOConnection1: TADOConnection;
    cxDBTreeList1: TcxDBTreeList;
    col_Caption1: TcxDBTreeListColumn;
    col_Param1: TcxDBTreeListColumn;
    col_Value11: TcxDBTreeListColumn;
    cxDBTreeListColumn4: TcxDBTreeListColumn;
    cxDBTreeListColumn5: TcxDBTreeListColumn;
    cxDBTreeListColumn6: TcxDBTreeListColumn;
    col_Value211: TcxDBTreeListColumn;
    cxDBTreeList2cxDBTreeListColumn1: TcxDBTreeListColumn;
    ADOStoredProc1: TADOStoredProc;
    DataSource1: TDataSource;
    procedure act_CollapseExecute(Sender: TObject);
//    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
    procedure cxDBTreeList1CustomDrawCell(Sender: TObject; ACanvas: TcxCanvas; AViewInfo:
        TcxTreeListEditCellViewInfo; var ADone: Boolean);
//    procedure cxDBTreeList1Expanded(Sender: TObject; ANode: TcxTreeListNode);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

    procedure cxDBTreeList1Expanded(Sender: TcxCustomTreeList; ANode:
        TcxTreeListNode);
    procedure cxDBTreeList2StylesGetContentStyle(Sender: TcxCustomTreeList;
      AColumn: TcxTreeListColumn; ANode: TcxTreeListNode;
      var AStyle: TcxStyle);
  
  private

    FStateFileName: string;

    FIsUpdated: Boolean;



  //  FRegPath: string;

   // procedure LoadFromRegCX(aRegPath: string);

  //  procedure SaveToRegCX(aRegPath: string);
  public
    procedure View ;//(aID1: integer);

  end;


//============================================================
implementation

 {$R *.dfm}


procedure Tframe_Link_calc_results.act_CollapseExecute(Sender: TObject);
begin

  if Sender=act_Collapse then
  begin
    cxDBTreeList1.FullCollapse;
  end
  else

  if Sender=act_Expand then
  begin
    cxDBTreeList1.FullExpand;
  end;


end;


procedure Tframe_Link_calc_results.cxDBTreeList1CustomDrawCell(Sender: TObject; ACanvas: TcxCanvas;
    AViewInfo: TcxTreeListEditCellViewInfo; var ADone: Boolean);
begin
//  AViewInfo.
//  AViewInfo.Node.Values

 // if AViewInfo.Node.Values[col_Is_Folder.ItemIndex]=True then
  //  ACanvas.Font.Style:=ACanvas.Font.Style + [fsBold];

end;

//-------------------------------------------------------------------
procedure Tframe_Link_calc_results.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  if ADOConnection1.Connected then
    ShowMessage('ADOConnection1');

//  cxDBTreeList1.Align:=alClient;

 // cxDBTreeList1.Visible:=False;


//  DataSource1.DataSet:=dxMemData1;

  cxDBTreeList1.Align:=alClient;

//  cxDBTreeList1.DataController.KeyField    := FLD_RecID;
//  cxDBTreeList1.DataController.ParentField := FLD_PARENT_ID;


  act_Collapse.Caption      := '�������� ���';
  act_Expand.Caption        := '�������� ���';

  // -------------------------------------------------------------------
 // cxDBTreeList1.RestoreFromRegistry(FRegPath + cxDBTreeList1.Name);

 {
  col_Caption.Caption.Text  :=STR_NAME;
  col_ID.Caption.Text       :=STR_PARAM;
  col_Value1.Caption.Text   :=STR_VALUE + ' 1->2';
  col_Value2.Caption.Text   :=STR_VALUE + ' 2->1';
  }


  col_Caption1.Caption.Text :=STR_NAME;
  col_Param1.Caption.Text    :=STR_PARAM;
  col_Value11.Caption.Text  :=STR_VALUE + ' 1->2';
  col_Value211.Caption.Text :=STR_VALUE + ' 2->1';



  FStateFileName := g_Application_FormStateDir + 'link_calc_results.txt';
                                        

//  col_Name.Visible:=False;

 // FRegPath := g_Storage.GetPathByClass (ClassName);

  g_Storage.RestoreFromRegistry(cxDBTreeList1, ClassName);


//  DataSource1.DataSet:=dxMemData1;

(*
  db_CreateField(mem_Link,
        [db_Field(FLD_ID,    ftInteger),
         db_Field(FLD_NAME,  ftString, 100) ]);

*)

end;

// ---------------------------------------------------------------
procedure Tframe_Link_calc_results.FormDestroy(Sender: TObject);
// ---------------------------------------------------------------
begin

 // cxDBTreeList1.StoreToRegistry(FRegPath + cxDBTreeList1.Name);

  g_Storage.StoreToRegistry(cxDBTreeList1, ClassName);

  inherited;
end;


// -------------------------------------------------------------------
procedure Tframe_Link_calc_results.View;
//(aID1: integer);
// -------------------------------------------------------------------
var
  k: Integer;
begin
  k:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc1, 'link_calc.sp_Link_calc_results_SEL', [FLD_ID, dmLink_view_data.DBLink.ID ]);

end;
      

procedure Tframe_Link_calc_results.cxDBTreeList1Expanded(Sender:
    TcxCustomTreeList; ANode: TcxTreeListNode);
begin
  if not FIsUpdated then
    cx_SaveToReg_Expanded(cxDBTreeList1, col_Caption1,   FStateFileName);

//  if not FIsUpdated then
//    SaveToRegCx (FRegPath);
    
end;


// ---------------------------------------------------------------
procedure Tframe_Link_calc_results.cxDBTreeList2StylesGetContentStyle(
  Sender: TcxCustomTreeList; AColumn: TcxTreeListColumn;
  ANode: TcxTreeListNode; var AStyle: TcxStyle);
// ---------------------------------------------------------------

var
  v1,v2: Variant;
  e1,e2 : Double;
begin
  inherited;

  if not col_Value211.Visible then
    exit;


  v1:=ANode.Values[col_Value11.ItemIndex];
  v2:=ANode.Values[col_Value211.ItemIndex];

//  v1 := 44.111;
 // v2:= ''; //null;


  try
    e1:=AsFloat(v1);
    e2:=AsFloat(v2);

    if e1 <> e2 then
      AStyle := cxStyle1;
  except
//    g_log.Error('procedure Tframe_Link_calc_results.cxDBTreeList1StylesGetContentStyle(Sender, AItem: TObject; ANode: TcxTreeListNode; var AStyle: TcxStyle);');
  end;

end;


end.




{
// ---------------------------------------------------------------
procedure Tframe_Link_calc_results.ApplicationEvents1Message(var Msg: tagMSG;
    var Handled: Boolean);
// ---------------------------------------------------------------
begin
 case Msg.message of
    WM_USER + Integer (  WM_LINK_REFRESH1_NEW):
    begin
       dmLink_view_data.OpenData(dmLink_view_data.Get_ID);

       View;// (FID);
    end;
  end;

end;

