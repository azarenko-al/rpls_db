unit fr_Link_profile_summary;

interface

uses

  IniFiles,

  u_Config,

  u_log,
  u_dxbar,

  u_Storage,

  u_Link_run,

  u_ini_Link_Optimize_params,

  u_vars,

  dm_Onega_DB_data,

  dm_Main,


  u_db,
  u_dlg,
  u_files,
  u_func,
  u_Geo,
  u_reg,
  u_cx_VGrid,

  dm_Link,
  dm_Link_Summary,

  u_link_const,

  u_func_msg,
  u_const_msg,

  u_const_db,
  u_const,
  u_const_str,

  Windows,  ADODB, AppEvnts, Messages, SysUtils, Classes, Graphics, Controls, Forms, cxGraphics,
  Db, dxmdaset,   Dialogs,    DBConsts,  Registry, ToolWin, ComCtrls,
    ExtCtrls, ActnList, StdCtrls, cxVGrid, cxDBVGrid, cxControls, cxInplaceContainer,
  dxBar, TB2Item, TB2Dock, TB2Toolbar, cxStyles, cxClasses, Grids, DBGrids,
  WSDLBind, DBCtrls, cxPropertiesStore, cxDBLookupComboBox, cxDBEdit,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,  
  cxLookupEdit, cxDBLookupEdit, cxLookAndFeels, cxBarEditItem,
  dxBarExtItems, dxBarExtDBItems, cxLookAndFeelPainters, cxCurrencyEdit,
  cxCheckBox, dxSkinsCore, dxSkinsDefaultPainters;

const
  WM_REFRESH_DATA = WM_USER + 600;


type
//  TOnGetUsePassiveElementEvent = procedure (var aValue : boolean) of object;


  Tframe_Link_profile_summary = class(TForm, IMessageHandler)
    ActionList1: TActionList;
    act_Calc: TAction;
    act_Opti_height: TAction;
    act_calc_params: TAction;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    TBItem3: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    TBSeparatorItem2: TTBSeparatorItem;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1CategoryRow1: TcxCategoryRow;
    row__Rx_Level_dBm: TcxDBEditorRow;
    row_SESR_norm: TcxDBEditorRow;
    row_sesr: TcxDBEditorRow;
    row_kng_norm: TcxDBEditorRow;
    cxDBVerticalGrid1kng: TcxDBEditorRow;
    cxDBVerticalGrid1kng_year: TcxDBEditorRow;
    row__BER_required: TcxDBEditorRow;
    row__fade_margin_dB: TcxDBEditorRow;
    row__Status: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow2: TcxCategoryRow;
    cxDBVerticalGrid1DBMultiEditorRow1: TcxDBMultiEditorRow;
    row_Band: TcxDBEditorRow;
    cxDBVerticalGrid1freq_min: TcxDBEditorRow;
    cxDBVerticalGrid1freq_max: TcxDBEditorRow;
    row_freq_MHz: TcxDBEditorRow;
    cxDBVerticalGrid1_bitrate_Mbps: TcxDBEditorRow;
    cxDBVerticalGrid1E1: TcxDBEditorRow;
    CategoryRow_Rains: TcxCategoryRow;
    row__Rain_Intensity: TcxDBEditorRow;
    row__IsCalcLength: TcxDBEditorRow;
    cxDBVerticalGrid1polarization: TcxDBEditorRow;
    cxDBVerticalGrid1Signal_Quality: TcxDBEditorRow;
    cxDBVerticalGrid1Rain_Length: TcxDBEditorRow;
    cxDBVerticalGrid1Weakening_vert: TcxDBEditorRow;
    cxDBVerticalGrid1Weakening_horz: TcxDBEditorRow;
    cxDBVerticalGrid1Allowable_Intense_vert: TcxDBEditorRow;
    cxDBVerticalGrid1Allowable_Intense_horz: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow4: TcxCategoryRow;
    row_Space_spread: TcxDBEditorRow;
    row_Frenel_max: TcxDBEditorRow;
    DataSource1: TDataSource;
    Panel1: TPanel;
    Button1: TButton;
    ADOStoredProc1: TADOStoredProc;
    act_Calc_Adaptive: TAction;
    TBItem4: TTBItem;
    TBSeparatorItem3: TTBSeparatorItem;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_Red: TcxStyle;
    cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow;
    Button2: TButton;
    cxStyle_ReadOnly: TcxStyle;
    TBControlItem1: TTBControlItem;
    DBCheckBox_Is_calc_with_passive: TDBCheckBox;
    ds_Link1: TDataSource;
    qry_Link1: TADOQuery;
    dxMemData_Calc_method: TdxMemData;
    dxMemData_Calc_methodname: TStringField;
    dxMemData_Calc_methodid: TIntegerField;
    ds_dxMemData_Calc_method: TDataSource;
    TBSeparatorItem4: TTBSeparatorItem;
    row_Is_CalcWithAdditionalRain: TcxDBEditorRow;
    cxPropertiesStore: TcxPropertiesStore;
    act_opt_equipment: TAction;
    TBItem5: TTBItem;
    TBControlItem4: TTBControlItem;
    cxDBLookupComboBox1: TcxDBLookupComboBox;
    row_Reliability: TcxDBEditorRow;
    row_Status_back: TcxDBEditorRow;
    dxBarManager1: TdxBarManager;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarManager1Bar2: TdxBar;
    cxLookAndFeelController1: TcxLookAndFeelController;
    dxBarManager1Bar1: TdxBar;
    cxBarEditItem2: TcxBarEditItem;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    cxBarEditItem1: TcxBarEditItem;
    cxBarEditItem3: TcxBarEditItem;
    dxBarControlContainerItem1: TdxBarControlContainerItem;
    ADOConnection1: TADOConnection;
    row_K_gotovnost: TcxDBEditorRow;
    dxBarManager1Bar3: TdxBar;
    row_Modulation: TcxDBEditorRow;
    row_Bandwidth: TcxDBEditorRow;
    cxStyle_yellow: TcxStyle;
    cxStyle_Green: TcxStyle;
    cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow3: TcxDBEditorRow;
    cxBarEditItem4: TcxBarEditItem;
    dxBarContainerItem1: TdxBarContainerItem;
    dxBarControlContainerItem2: TdxBarControlContainerItem;
    cxBarEditItem5: TcxBarEditItem;
    procedure act_CalcExecute(Sender: TObject);
//    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
    procedure Button2Click(Sender: TObject);
  //  procedure cxDBVerticalGrid1DrawValue(Sender: TObject; ACanvas: TcxCanvas; APainter: TcxvgPainter;
  //     AValueInfo: TcxRowValueInfo; var Done: Boolean);
    procedure cxDBVerticalGrid1Edited(Sender: TObject; ARowProperties: TcxCustomEditorRowProperties);

    procedure FormCreate(Sender: TObject);
//    procedure dxDBInspector1111DrawValue(Sender: TdxInspectorRow;
 //     ACanvas: TCanvas; ARect: TRect; var AText: String; AFont: TFont;
   //   var AColor: TColor; var ADone: Boolean);
//    procedure dxDBInspector1111ChangeNode(Sender: TObject; OldNode, Node: TdxInspectorNode);
    procedure FormDestroy(Sender: TObject);
 //   procedure mem_SummaryCalcFields(DataSet: TDataSet);
//    procedure row_IsCalcLengthToggleClick(Sender: TObject; const Text: string;
//        State: TdxCheckBoxState);
    procedure row__IntensityEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
 //   procedure row__IsCalcLengthEditPropertiesChange(Sender: TObject);
//    procedure row__BER_requiredEditPropertiesCloseUp(Sender: TObject);
    procedure cxDBVerticalGrid1StylesGetContentStyle(Sender: TObject;
      AEditProp: TcxCustomEditorRowProperties; AFocused: Boolean;
      ARecordIndex: Integer; var AStyle: TcxStyle);
    procedure DBCheckBox_Is_calc_rainsMouseUp(Sender: TObject; Button:
        TMouseButton; Shift: TShiftState; X, Y: Integer);
//    procedure DBLookupComboBox1111CloseUp(Sender: TObject);
    procedure row_Is_CalcWithAdditionalRainEditPropertiesEditValueChanged(
      Sender: TObject);
    procedure cxDBLookupComboBox2PropertiesCloseUp(Sender: TObject);
//    procedure qry_Link1CalcFields(DataSet: TDataSet);

  private
//    FOnGetUsePassiveElement: TOnGetUsePassiveElementEvent;

    FReadOnly : Boolean;

    FDataset: TDataSet;

    FID: integer;
    FLastCol, FLastRow: Integer;

    FRegPath: string;
    FOnCalcButton: TNotifyEvent;
    FOnDataChanged: TNotifyEvent;

    FIsCalcWithAdditionalRain: boolean;

//    procedure Init;
    procedure Change_IsCalcLength1(aNewValue: boolean);

    procedure Run_Link_Optimize(aID: Integer);

    procedure SetIsCalcWithAdditionalRain(const Value: boolean);
    procedure UpdateLinkData;
    procedure UpdateVisible;
    procedure UpdateVisibleRows;

    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList ; var aHandled:
        boolean);


  protected
    procedure WM_REFRESH_DATA_proc(var Message: TMessage); message WM_REFRESH_DATA;
  public
    property IsCalcWithAdditionalRain: boolean read FIsCalcWithAdditionalRain write
        SetIsCalcWithAdditionalRain;

    procedure HideHeader;
    procedure RunLinkGraph(aID: Integer);
    procedure SetReadOnly(aValue: Boolean);

    procedure View(aID: integer; aSrcProc: TADOStoredProc=nil);

    property OnDataChanged: TNotifyEvent read FOnDataChanged write FOnDataChanged;
//    class function CreateChildForm ( aDest: TWinControl): Tframe_Link_profile_summary;
    property OnCalcButton: TNotifyEvent read FOnCalcButton write FOnCalcButton;

//    property OnGetUsePassiveElement: TOnGetUsePassiveElementEvent read FOnGetUsePassiveElement write FOnGetUsePassiveElement;

  end;


//====================================================================
//====================================================================
implementation

uses dm_Act_Link; {$R *.dfm}



//---------------------------------------------------------------------
procedure Tframe_Link_profile_summary.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
var
  I: Integer;
begin
  inherited;

  if ADOConnection1.Connected then
    ShowMessage ('procedure Tframe_Link_profile_summary.FormCreate(Sender: TObject);');
    

  FRegPath :=g_Storage.GetPathByClass(ClassName);

  bar_Init (dxBarManager1);


  cxDBVerticalGrid1.Align:=alClient;
  cxDBVerticalGrid1.OptionsView.RowHeaderWidth:=reg_ReadInteger (FRegPath, 'RowHeaderWidth', 100);
  cxDBVerticalGrid1.DataController.DataSource :=DataSource1;

  cx_InitDBVerticalGrid (cxDBVerticalGrid1);
 
  row__Rain_Intensity.Properties.DataBinding.FieldName := FLD_rain_INTENSITY_extra;


 // AddComponentProp(cb_SaveToDB, 'Checked');


//  FDataset := mem_Summary;
  FDataset := ADOStoredProc1;


  cx_DBVGrid_check(cxDBVerticalGrid1);



//  Toolbar2.Align:=alBottom;
  //Panel1.Align:=alClient;

 { row_Power.Caption:='������� [dBm]';
  row_Fade_Reserv.Caption:='����� �� ��������� [dB]';
}


  row__Rx_Level_dBm.Properties.Caption:='������� [dBm]';
  row__fade_margin_dB.Properties.Caption:='����� �� ��������� [dB]';

  row_Space_spread.Properties.Caption:='��������.������.���������� [m]';
  row_Frenel_max.Properties.Caption:='1-� ���� ������� � �������� ��������� [m]';



 // row_Is_CalcWithAdditionalRain.Properties.Caption:='���� ������� ���. ������� �� ����� �� ������';


  cxDBVerticalGrid1.OptionsView.RowHeaderWidth :=
    reg_ReadInteger(FRegPath, 'RowHeaderWidth', cxDBVerticalGrid1.OptionsView.RowHeaderWidth);


//  Init();

  for I := 0 to High(LINK_CALC_METHODS_ARR) do
    db_AddRecord(dxMemData_Calc_method,
       [
         db_Par(FLD_NAME, LINK_CALC_METHODS_ARR[i].Name),
         db_Par(FLD_ID, LINK_CALC_METHODS_ARR[i].ID)
       ]);



  cx_InitDBVerticalGrid(cxDBVerticalGrid1);

  act_opt_equipment.Caption:='����������� ������������';


//  row_Reliability.Visible  :=False;


  row_Reliability.Properties.DataBinding.FieldName:= FLD_Reliability;


  //���������� ����� �%"
  //reliability

 // dxDBInspector1.DividerPos:=185
end;


// ---------------------------------------------------------------
procedure Tframe_Link_profile_summary.UpdateLinkData;
// ---------------------------------------------------------------
var
//  bUKV: Boolean;
  k: Integer;
begin
 // db_PostDataset(qry_Link);

// DBLookupComboBox1.

  k:=qry_Link1.FieldByName(FLD_CALC_METHOD).AsInteger;

  db_UpdateRecord__(qry_Link1,
    [
      FLD_is_UsePassiveElements,     DBCheckBox_Is_calc_with_passive.Checked,
  //    FLD_Is_CalcWithAdditionalRain, DBCheckBox_Is_calc_rains.Checked,
      FLD_CALC_METHOD,               qry_Link1.FieldByName(FLD_CALC_METHOD).AsInteger
    ]);

{
  bUKV:= k=DEF_Calc_Method_UKV;

  row_SESR_norm.Visible     :=not bUKV;
  row_sesr.Visible          :=not bUKV;
  row__BER_required.Visible :=not bUKV;}


{
 LINK_CALC_METHODS_ARR : array[0..4] of
      record
        ID   : Integer;
        Name : string;
      end  =

  (
  (id : DEF_Calc_Method_ITU_R;      Name: '���������� (ITU, ����, 16�����)'),
  (id : DEF_Calc_Method_GOST;       Name: '���� � 53363-2009�.'),
  (id : DEF_Calc_Method_NIIR_1998;  Name: '����-98�.'),
  (id : DEF_Calc_Method_E_BAND_ITU; Name: 'E-�������� (ITU)'),
  (id : DEF_Calc_Method_UKV;        Name: '��� (16�����)')
  );}


//  g_log.Add('UpdateLinkData done.');

end;

// ---------------------------------------------------------------
procedure Tframe_Link_profile_summary.UpdateVisibleRows;
// ---------------------------------------------------------------
var
  bUKV: Boolean;
  k: Integer;
begin
 // db_PostDataset(qry_Link);

// DBLookupComboBox1.

  k:=qry_Link1.FieldByName(FLD_CALC_METHOD).AsInteger;



  bUKV:= k=DEF_Calc_Method_UKV;

  row_SESR_norm.Visible     :=not bUKV;
  row_sesr.Visible          :=not bUKV;
  row__BER_required.Visible :=not bUKV;

  CategoryRow_Rains.Visible :=not bUKV;
  row_Space_spread.Visible  :=not bUKV;

  row_Reliability.Visible  :=bUKV;
  row_K_gotovnost.Visible  :=not bUKV;

////  row_Reliability.Visible  :=not bUKV;


{
 LINK_CALC_METHODS_ARR : array[0..4] of
      record
        ID   : Integer;
        Name : string;
      end  =

  (
  (id : DEF_Calc_Method_ITU_R;      Name: '���������� (ITU, ����, 16�����)'),
  (id : DEF_Calc_Method_GOST;       Name: '���� � 53363-2009�.'),
  (id : DEF_Calc_Method_NIIR_1998;  Name: '����-98�.'),
  (id : DEF_Calc_Method_E_BAND_ITU; Name: 'E-�������� (ITU)'),
  (id : DEF_Calc_Method_UKV;        Name: '��� (16�����)')
  );}


//  g_log.Add('UpdateLinkData done.');

end;




//---------------------------------------------------------------------
procedure Tframe_Link_profile_summary.FormDestroy(Sender: TObject);
//---------------------------------------------------------------------
begin
  reg_WriteInteger (FRegPath, 'RowHeaderWidth', cxDBVerticalGrid1.OptionsView.RowHeaderWidth);

  inherited;
end;

//--------------------------------------------------------------------
procedure Tframe_Link_profile_summary.View(aID: integer; aSrcProc:
    TADOStoredProc=nil);
//--------------------------------------------------------------------
var
  k: Integer;
begin
  UpdateVisible;


  //; aSrcProc: TADOStoredProc=nil

  Assert(aID>0, 'procedure Tframe_Link_profile_summary.View(aID: integer);   - Value : '+ IntToStr(aID));

  FID:=aID;

  {
 function TdmOnega_DB_data.Link_Summary(aADOStoredProc: TADOStoredProc; aID:
    Integer): Integer;
begin
 // Assert(aID>0, 'Value <=0');

  Result := OpenStoredProc(aADOStoredProc, 'Link.sp_Link_Summary',  [FLD_ID,  aID ]);
//  Result := OpenStoredProc(aADOStoredProc, 'sp_Link_Summary',  [FLD_ID,  aID ]);
end;

  }

 // g_Profiler.Start('Tframe_Link_profile_summary.View');

 {
  if ADOStoredProc1.FieldCount=0 then
  begin
    k:=dmOnega_DB_data.Link_Summary(ADOStoredProc1, aID);

 //   k:=dmOnega_DB_data.Link_Summary(ADOStoredProc1, 1);
                                                                         )
    db_CreateFieldsForDataset(ADOStoredProc1);

    db_CreateCalculatedField(ADOStoredProc1, FLD_Reliability, ftFloat);

  end;
  }
  if not Assigned(aSrcProc) then
    k:=dmOnega_DB_data.Link_Summary(ADOStoredProc1, aID)
  else
    ADOStoredProc1.Clone(aSrcProc);

//  db_View (ADOStoredProc1);


  Assert (Assigned ( ADOStoredProc1.FindField (FLD_status_name)  ));



 // g_Profiler.Stop('Tframe_Link_profile_summary.View');


 dmOnega_DB_data.OpenQuery(qry_Link1,
    Format('SELECT calc_method, Is_CalcWithAdditionalRain, '+
       'is_UsePassiveElements FROM %s WHERE id=%d', [TBL_LINK, aID]));


  UpdateVisibleRows();

end;


procedure Tframe_Link_profile_summary.HideHeader;
begin
  TBDock1.Hide;
end;


procedure Tframe_Link_profile_summary.SetIsCalcWithAdditionalRain(const Value:
    boolean);
begin
  FIsCalcWithAdditionalRain := Value;
end;


// ---------------------------------------------------------------
procedure Tframe_Link_profile_summary.row__IntensityEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
// ---------------------------------------------------------------


    // -------------------------------------------------------------------
    procedure DoChange_Rain_Intensity(aNewValue: double);
    // -------------------------------------------------------------------
    begin
      if (aNewValue<0) or (aNewValue>1000) then
      begin
        Error:=True;
     //   ErrorText:='������������� ������� ������ ���� � �������� �� 0 �� 1000 ��/���';

        ShowMessage('������������� ������� ������ ���� � �������� �� 0 �� 1000 ��/���');

      //  db_UpdateRecord( mem_Summary, [db_Par(FLD_INTENSITY,0)]);

        exit;
      end;

    //  iCurrentINTENSITY:= dmLink.GetIntFieldValue(FID, FLD_INTENSITY);

     // iCurrentINTENSITY:= mem_Summary[FLD_INTENSITY]


      if (aNewValue <> FDataset[FLD_rain_INTENSITY_extra]) then
//      if (aNewValue <> mem_Summary[FLD_rain_INTENSITY]) then
      begin
        dmLink.Update (FID, FLD_rain_INTENSITY_extra, aNewValue);

        dmLink_Summary.CalcRainDop1  (FID, FIsCalcWithAdditionalRain);

        View(FID);
      end;
    end;


begin

  if cxDBVerticalGrid1.FocusedRow=row__Rain_Intensity then
  begin
    DoChange_Rain_Intensity(DisplayValue);
  end else

(*  if cxDBVerticalGrid1.FocusedRow=row__BER_required then
    DoChange_BER_required(DisplayValue);
*)
end;



// -------------------------------------------------------------------
procedure Tframe_Link_profile_summary.Change_IsCalcLength1(aNewValue: boolean);
// -------------------------------------------------------------------
var
  bCurrentIsCalcLength: Boolean;
begin
//  bCurrentIsCalcLength:= FDataset.FieldByName(FLD_RAIN_ISCALCLENGTH).AsBoolean;

//    iCurrentIsCalcLength:= dmLink.GetIntFieldValue(FID, FLD_ISCALCLENGTH);

//    if (Boolean(iCurrentIsCalcLength)
//       <> mem_Summary[FLD_ISCALCLENGTH]) then

  //if (bCurrentIsCalcLength <> aNewValue) then
  begin
  // .. if mem_Summary[FLD_ISCALCLENGTH] = true then
    dmLink.Update (FID, FLD_RAIN_IsCALCLENGTH, aNewValue);
  //..  else
  //    dmLink.Update (FID, [db_Par(FLD_ISCALCLENGTH, False)  ]);

    dmLink_Summary.CalcRainDop1(FID, aNewValue);//, mem_Summary[FLD_ISCALCLENGTH]);
//    dmLink_Summary.CalcRainDop1(FID, FIsCalcWithAdditionalRain);//, mem_Summary[FLD_ISCALCLENGTH]);

    View(FID);
  end;
end;


procedure Tframe_Link_profile_summary.cxDBVerticalGrid1Edited(Sender: TObject; ARowProperties: TcxCustomEditorRowProperties);
var b: boolean;
  v: variant;
begin
  if cxDBVerticalGrid1.FocusedRow=row__IsCalcLength then
  begin
    try
      v :=row__IsCalcLength.Properties.Value;
      b:=AsBoolean(row__IsCalcLength.Properties.Value);
    except
   //   on E: Exception do: ;
    end;

    Change_IsCalcLength1(b);
  end;
end;



// ---------------------------------------------------------------
procedure Tframe_Link_profile_summary.RunLinkGraph(aID: Integer);
// ---------------------------------------------------------------
begin
  TLink_run.RunLinkGraph(aID);

end;



//-------------------------------------------------------------------
procedure Tframe_Link_profile_summary.Run_Link_Optimize(aID: Integer);
//-------------------------------------------------------------------
//var
//  b : Boolean;
begin
//  b := False;
//  if Assigned(FOnGetUsePassiveElement) then
//    FOnGetUsePassiveElement (b);

//  TLink_run.Run_Link_Optimize (aID, Handle, b);
  if TLink_run.Run_Link_Optimize (aID) then
  begin

    if Assigned(FOnDataChanged) then
      FOnDataChanged(Self);
  end;

//  WM_REFRESH_DATA
//    PostUserMessage(WM_LINK_REFRESH);

end;




procedure Tframe_Link_profile_summary.WM_REFRESH_DATA_proc(var Message:
    TMessage);
begin
  if Assigned(FOnDataChanged) then
    FOnDataChanged(Self);

end;



procedure Tframe_Link_profile_summary.cxDBVerticalGrid1StylesGetContentStyle(
  Sender: TObject; AEditProp: TcxCustomEditorRowProperties;
  AFocused: Boolean; ARecordIndex: Integer; var AStyle: TcxStyle);

var
  iValue: integer;
  iValue_back: Integer;
begin
  if FReadOnly then
  begin
    AStyle := cxStyle_ReadOnly;
    Exit;
  end;


  if  AEditProp.Row=row__Status then
    if FDataSet.Active and (FDataSet.RecordCount>0) then
    begin
      iValue     :=FDataSet.FieldByName(FLD_STATUS).AsInteger;
      iValue_back:=FDataSet.FieldByName(FLD_STATUS_BACK).AsInteger;

//      if (iValue=0) then
//        Exit;

      if (iValue=1) and (iValue_back=1) then
        AStyle  := cxStyle_Green  else

      if (iValue=2) and (iValue_back=2) then
        AStyle  := cxStyle_Red  else

      if iValue <> iValue_back then
        AStyle  := cxStyle_Yellow
   //   else
    //    AStyle  := cxStyle_Red
    end;
end;

procedure Tframe_Link_profile_summary.DBCheckBox_Is_calc_rainsMouseUp(Sender:  TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  UpdateLinkData();
end;




procedure Tframe_Link_profile_summary.row_Is_CalcWithAdditionalRainEditPropertiesEditValueChanged(
  Sender: TObject);
var
  b: Boolean;
  v: Variant;
begin
//  ShowMessage('row_Is_CalcWithAdditionalRainEditPropertiesEditValueChanged');

//  v:=row_Is_CalcWithAdditionalRain.Properties.Value;


 // b:= FDataset.FieldByName(FLD_Is_CalcWithAdditionalRain).AsBoolean;

  dmLink.Update (FID, FLD_Is_CalcWithAdditionalRain, row_Is_CalcWithAdditionalRain.Properties.Value);


 // b:=b;


//      dmLink.Update (FID, FLD_RAIN_IsCALCLENGTH, aNewValue);

end;


procedure Tframe_Link_profile_summary.cxDBLookupComboBox2PropertiesCloseUp(
  Sender: TObject);
begin
  UpdateLinkData();
  UpdateVisibleRows();
end;


procedure Tframe_Link_profile_summary.SetReadOnly(aValue: Boolean);
begin
  FReadOnly := aValue;

  SetFormActionsEnabled(Self, not aValue);

  cxDBVerticalGrid1.OptionsData.Editing := not aValue;

  ADOStoredProc1.LockType := IIF(aValue, ltReadOnly, ltBatchOptimistic);

end;



// ---------------------------------------------------------------
procedure Tframe_Link_profile_summary.act_CalcExecute(Sender: TObject);
// ---------------------------------------------------------------
begin
  if FReadOnly then
    Exit;


  //------------------------------------------------------
  if Sender=act_Calc_adaptive then
  begin
    dmAct_Link.CalcAdaptive(FID);

//    if Assigned(FOnCalcButton) then
 //     FOnCalcButton(Self);
  end else


  //------------------------------------------------------
  if Sender=act_Calc then
  begin
  //  PostUserMessage(WM_LINK_REFRESH, FID);

    if Assigned(FOnCalcButton) then
      FOnCalcButton(Self);
  end else


  //------------------------------------------------------
  if Sender=act_opt_equipment then
  begin
    TLink_run.CalcOpti_equip (FID);

    g_EventManager.PostEvent_ID(et_LINK_REFRESH, FID);

//    PostUserMessage(WM_LINK_REFRESH, FID);
  end else


  //------------------------------------------------------
  if Sender=act_Opti_height then
  begin
    Run_Link_Optimize(FID);

    g_EventManager.PostEvent_ID(et_LINK_REFRESH, FID);

  //  PostUserMessage(WM_LINK_REFRESH, FID);


  end else

  //------------------------------------------------------
  if Sender=act_calc_params then
    RunLinkGraph (FID);
//    Tfrm_Main_link_graph.ExecDlg(FID);

end;


procedure Tframe_Link_profile_summary.Button2Click(Sender: TObject);
begin
  cxDBVerticalGrid1.OptionsData.Editing :=False;

end;

procedure Tframe_Link_profile_summary.GetMessage(aMsg: TEventType; aParams:
    TEventParamList ; var aHandled: boolean);
begin

  case aMsg of
    et_INTERFACE_NORMAL,
    et_INTERFACE_SIMPLE:
  //  begin


     // ShowMessage('Tframe_Link_View.ApplicationEvents1Message');

      UpdateVisible;

  //  end;
  end;
end;


// ---------------------------------------------------------------
procedure Tframe_Link_profile_summary.UpdateVisible;
// ---------------------------------------------------------------
var
  b: Boolean;
begin
  b:=not g_Config.IsInterfaceSimple;

  CategoryRow_Rains.Visible:=b;

end;





end.



{



procedure Tframe_Link_View.ApplicationEvents1Message(var Msg: tagMSG; var
    Handled: Boolean);
begin


  case Msg.message of
    Integer (WM_INTERFACE_NORMAL),
    Integer (WM_INTERFACE_SIMPLE):
    begin


     // ShowMessage('Tframe_Link_View.ApplicationEvents1Message');

      UpdateVisible;

    end;
  end;



 case Msg.message of
    WM_USER + Integer (  WM_LINK_REFRESH):

    begin
 //     Msg.

      View (FID, '');

    end;

  end;

//  inherited;
end;




//--------------------------------------------------------------------
procedure Tframe_Link_View.UpdateVisible;
//--------------------------------------------------------------------
var
  b: Boolean;
begin
  b:=not g_Config.IsInterfaceSimple;

//  TabSheet_Profile.Visible:=b;

  TabSheet_Inspector.TabVisible:=b;
  TabSheet_CalcRes.TabVisible:=b;
  TabSheet_Refl_Points.TabVisible:=b;
  TabSheet_Reports.TabVisible:=b;
  TabSheet_Repeater.TabVisible:=b;
end;

procedure Tframe_Link_profile_summary.qry_Link1CalcFields(DataSet: TDataSet);
begin
 // DataSet[FLD_Reliability]:= 100 - DataSet.FieldByName(FLD_KNG).AsFloat ;

end;




procedure Tframe_Link_profile_summary.ApplicationEvents1Message(var Msg:  tagMSG; var Handled: Boolean);
begin
  {
  case Msg.message of
    WM_USER + Integer (  WM_LINK_REFRESH1):
         View (FID);
  end;
  }

 case Msg.message of
    Integer (WM_INTERFACE_NORMAL),
    Integer (WM_INTERFACE_SIMPLE):
    begin


     // ShowMessage('Tframe_Link_View.ApplicationEvents1Message');

      UpdateVisible;

    end;
  end;



end;

{
bar = new Bar();
bar.Name = "Toolbar3";  
bar.UseWholeRow = DefaultBoolean.True;  
bar.IsMultiLine = true;
