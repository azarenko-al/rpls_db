unit u_Link_run;

interface

//{$DEFINE use_dll111}
{$DEFINE use_dll111}

uses
  Classes, Forms, Menus, ActnList, IniFiles, SysUtils, Dialogs, u_assert,
  u_files,
  u_const,
  u_func,
  dm_Main,
  u_classes,
  u_reg,
  u_Vars,

  //  ..  I_Google,
  //..  I_Link_ACAD,
  //  I_link_calc,
  //  I_Link_Graph,
  //  I_link_optimize,

  u_Ini_Google_Params, //shared
  u_ini_Link_freq_report_params,
  u_ini_Link_optimize_params,  //shared
  u_ini_Link_graph_params,  //shared

  u_ini_LinkCalcParams;  //shared



type
  TLink_run = class(TObject)
  private
//    class procedure CalcAdaptive_new(aID: Integer);
  public
    class procedure Export_Google(aSelectedIDList: TIDList);

//    class procedure Calc(aID: integer);
    class procedure Calc_list(aIDList: TIDList);

//    aIDList: TIDList);

//    class procedure Calc(aID: integer; aUsePassiveElements: Boolean=False;
//        aIsCalcWithAdditionalRain: Boolean=False; aIsSaveReportToDB: Boolean=True);

    class procedure CalcAdaptive(aID: Integer; aParams: TIni_Link_Calc_Params =
        nil; aBitrate_min: Integer = 0);

    class procedure CalcAdaptive_new(aID: Integer; aFileName: string);

    class function CalcOpti_Equip(aID: integer; aParams: TIni_Link_Calc_Params =
        nil; aBitrate_min: Integer = 0): Integer;

    class procedure Calc_for_graph(aID: integer);
    class procedure Export_ACAD(aID: integer);
    class procedure Freq_Report(aSelectedIDList: TIDList);

//    class procedure RunLinkGraph(aID: Integer; aUse_Passive_Elements: boolean);
    class procedure RunLinkGraph(aID: Integer);

//    class procedure Run_Link_Optimize(aID, aHandle: Integer; aUse_Passive_Elements : boolean);
    class function Run_Link_Optimize(aID: Integer): Boolean;

//    class procedure Run_Link_Opt(aID: Integer);

    class function Run_Link_Optimize_freq_diversity(aID: Integer): boolean;
  end;

implementation

// ---------------------------------------------------------------
class procedure TLink_run.Export_Google(aSelectedIDList: TIDList);
// ---------------------------------------------------------------
const
  TEMP_INI_FILE = 'project_google.ini';
var
  i: Integer;
  iCode: Integer;
  oParams: TIni_Google_Params;
  sFile: string;

 // oItem: TIDListItem;
begin
  if aSelectedIDList.Count = 0 then
  begin
    ShowMessage('�������� ������');
    Exit;
  end;

  sFile := g_ApplicationDataDir + TEMP_INI_FILE;

  oParams := TIni_Google_Params.Create;

  oParams.ProjectID := dmMain.ProjectID;
  oParams.Mode := mtLink;
      

//  oItem:=aSelectedIDList[0];
 // i:=aSelectedIDList.Count;
  oParams.Link_IDList.Assign(aSelectedIDList);

  oParams.SaveToFile(sFile);

  FreeAndNil(oParams);

  // -----------------------------
  {$IFDEF use_dll}
 {
  if Load_IGoogle() then
  begin
    IGoogle.InitADO(dmMain.ADOConnection1.ConnectionObject);
    IGoogle.Execute(sFile);

    UnLoad_IGoogle();
  end;
  }
  {$ELSE}
  AssertFileExists(GetApplicationDir() + PROG_Google);

  RunApp(GetApplicationDir() + PROG_Google, DoubleQuotedStr(sFile), iCode);
  {$ENDIF}
end;


//--------------------------------------------------------------------
class procedure TLink_run.Export_ACAD(aID: integer);
//--------------------------------------------------------------------
const
  TEMP_INI_FILE = 'link_ACAD.ini';
var
  iCode: Integer;
  oIni: TIniFile;
  sFile: string;
begin

  sFile := g_ApplicationDataDir + TEMP_INI_FILE;

  oIni := TIniFile.Create(sFile);
  //oIni.ReadInteger ('main', 'project_id', dmMain.ProjectID);
//  oIni.WriteInteger ('main', 'ProjectID',          dmMain.ProjectID);
  oIni.WriteInteger('main', 'link_ID', aID);

  FreeAndNil(oIni);



  {$IFDEF use_dll}
 {
  if Load_ILink_ACAD() then
  begin
    ILink_ACAD.InitADO(dmMain.ADOConnection1.ConnectionObject);
 //   ILink_graph.InitAppHandle(Application.Handle);
    ILink_ACAD.Execute(sFile);

    UnLoad_ILink_ACAD();
  end;
  }
  {$ELSE}
//  CursorHourGlass;

  AssertFileExists(GetApplicationDir() + EXE_LINK_ACAD);

  RunApp(GetApplicationDir() + EXE_LINK_ACAD, DoubleQuotedStr(sFile), iCode);

//  CursorDefault;
  {$ENDIF}


end;



//--------------------------------------------------------------------
class procedure TLink_run.CalcAdaptive(aID: Integer; aParams:
    TIni_Link_Calc_Params = nil; aBitrate_min: Integer = 0);
//--------------------------------------------------------------------
const
  TEMP_INI_FILE = 'link_calc.ini';
var
  iCode: Integer;
  k: Integer;
  sFile: string;
  oObj: TIni_Link_Calc_Params;
begin
  sFile := g_ApplicationDataDir + TEMP_INI_FILE;

  oObj := TIni_Link_Calc_Params.Create;

 // k:= IIF(aUsePassiveElements, 1, 0);

  oObj.ConnectionString:=dmMain.GetConnectionString();

//  oObj.ProjectID := aProjectID;
  oObj.ProjectID := dmMain.ProjectID;
  oObj.LinkID    := aID;

  oObj.Mode := mtAdaptiveModulation;

  oObj.Bitrate_min:=aBitrate_min;

//  oObj.IsUseAdaptiveModulation  :=true;

//  oObj.IsSaveReportToDB         :=aIsSaveReportToDB;

 // oObj.IsUsePassiveElements     :=True;
 // oObj.IsCalcWithAdditionalRain :=True;

  oObj.SaveToFile(sFile);

  FreeAndNil(oObj);

  // ---------------------------------------------------------------


  AssertFileExists(GetApplicationDir() + EXE_LINK_CALC);

  RunApp(GetApplicationDir() + EXE_LINK_CALC, DoubleQuotedStr(sFile), iCode);

//  CursorDefault;


  if iCode>0 then
    if assigned(aParams) then
      aParams.LoadFromFile (sFile);


end;



//--------------------------------------------------------------------
class procedure TLink_run.CalcAdaptive_new(aID: Integer; aFileName: string);
//--------------------------------------------------------------------
const
  TEMP_INI_FILE = 'link_calc.ini';
var
  iCode: Integer;
  k: Integer;
  sFile: string;
  oObj: TIni_Link_Calc_Params;
begin
  sFile := g_ApplicationDataDir + TEMP_INI_FILE;

  oObj := TIni_Link_Calc_Params.Create;

 // k:= IIF(aUsePassiveElements, 1, 0);


//  oObj.ProjectID := aProjectID;
  oObj.ProjectID := dmMain.ProjectID;
  oObj.LinkID    := aID;

  oObj.Mode := mtAdaptiveModulation;

  oObj.AM_FileName:=aFileName;

  oObj.SaveToFile(sFile);

  FreeAndNil(oObj);

  // ---------------------------------------------------------------

  AssertFileExists(GetApplicationDir() + EXE_LINK_CALC);

  RunApp(GetApplicationDir() + EXE_LINK_CALC, DoubleQuotedStr(sFile), iCode);

//  CursorDefault;



end;





//--------------------------------------------------------------------
class function TLink_run.CalcOpti_Equip(aID: integer; aParams:
    TIni_Link_Calc_Params = nil; aBitrate_min: Integer = 0): Integer;
//--------------------------------------------------------------------
const
  TEMP_INI_FILE = 'link_calc.ini';
var
  iCode: Integer;
  sFile: string;
  oObj: TIni_Link_Calc_Params;
begin
  sFile := g_ApplicationDataDir + TEMP_INI_FILE;

  oObj := TIni_Link_Calc_Params.Create;

 // k:= IIF(aUsePassiveElements, 1, 0);


  oObj.ProjectID := dmMain.ProjectID;
  oObj.LinkID := aID;
  oObj.ConnectionString:=dmMain.GetConnectionString();



  oObj.Mode := mtOptimize_equip;

  oObj.Bitrate_min:=aBitrate_min;


//  oObj.IsSaveReportToDB         :=aIsSaveReportToDB;

 // oObj.IsUsePassiveElements     :=True;
 // oObj.IsCalcWithAdditionalRain :=True;

  oObj.SaveToFile(sFile);

  FreeAndNil(oObj);

  // ---------------------------------------------------------------


  AssertFileExists(GetApplicationDir() + EXE_LINK_CALC);

 // result:=
  RunApp(GetApplicationDir() + EXE_LINK_CALC, DoubleQuotedStr(sFile), iCode);

//  CursorDefault;


  if iCode>0 then
    if assigned(aParams) then
      aParams.LoadFromFile (sFile);


end;



// ---------------------------------------------------------------
class procedure TLink_run.RunLinkGraph(aID: Integer);
// ---------------------------------------------------------------
//; aUseAM: boolean = false
const
  TEMP_INI_FILE = 'link_calc.ini';
var
  iCode: Integer;
 // k: Integer;
  oInifile: TIniFile;
  oParams: TIni_Link_Graph_Params;
  sFile: string;
begin
 // k:= IIF(aUsePassiveElements, 1, 0);
  sFile := g_ApplicationDataDir + TEMP_INI_FILE;

  // -------------------------
  oParams := TIni_Link_Graph_Params.Create;
//  oParams.Handle    := aHandle;
  oParams.ProjectID := dmMain.ProjectID;

//  oParams.ConnectionString:= dmMain.ADOConnection.ConnectionString;
  oParams.ConnectionString:= dmMain.GetConnectionString;

  oParams.LinkID := aID;
 ///// oParams.UsePassiveElements  :=aUse_Passive_Elements;
//  oParams.UseAM  := aUseAM;
//  oParams.mode      := 'Optimize_antennas';
  oParams.SaveToFile(sFile);

  FreeAndNil(oParams);
                         
  // -----------------------------


  AssertFileExists(GetApplicationDir() + EXE_LINK_GRAPH);

  RunApp(GetApplicationDir() + EXE_LINK_GRAPH, DoubleQuotedStr(sFile), iCode);



end;


// ---------------------------------------------------------------
class procedure TLink_run.Freq_Report(aSelectedIDList: TIDList);
// ---------------------------------------------------------------
//; aUseAM: boolean = false
const
  TEMP_INI_FILE = 'link_Report.ini';
var
  iCode: Integer;
 // k: Integer;
  oInifile: TIniFile;
  oParams: TIni_Link_freq_Report_Param;
  sFile: string;
begin
 // k:= IIF(aUsePassiveElements, 1, 0);
  sFile := g_ApplicationDataDir + TEMP_INI_FILE;

  // -------------------------
  oParams := TIni_Link_freq_Report_Param.Create;
  oParams.IDList.Assign(aSelectedIDList);

//  oParams.ConnectionString:= dmMain.ADOConnection.ConnectionString;
  oParams.ConnectionString:= dmMain.GetConnectionString;

  oParams.SaveToINI(sFile);
  FreeAndNil(oParams);

  // -----------------------------


  AssertFileExists(GetApplicationDir() + EXE_DLL_LINK_FREQ_report);

  RunApp(GetApplicationDir() + EXE_DLL_LINK_FREQ_report, DoubleQuotedStr(sFile), iCode);


end;




//-------------------------------------------------------------------
class function TLink_run.Run_Link_Optimize(aID: Integer): Boolean;
//-------------------------------------------------------------------
const
  DEF_TEMP_FILENAME  = 'Link_Optimize.ini';
  PROG_Link_Optimize = 'Link_Optimize.exe';
var
  iCode: Integer;
  oInifile: TIniFile;
  oParams: TIni_Link_Optimize_Params;
  sFile: string;
begin
  sFile := g_ApplicationDataDir + DEF_TEMP_FILENAME;


   // -------------------------
  oParams := TIni_Link_Optimize_Params.Create;
//  oParams.Handle := aHandle;

  oParams.ProjectID := dmMain.ProjectID;
  oParams.LinkID := aID;
  oParams.mode := 'Optimize_antennas';


  oParams.ConnectionString:= dmMain.GetConnectionString;
//  oParams.ConnectionString:= dmMain.ADOConnection.ConnectionString;

 // oParams.IsSaveReportToDB     :=aIsSaveReportToDB;
//  oParams.UsePassiveElements  :=aUse_Passive_Elements;

(*    dmAct_Link.Calc (FID,
              cb_Use_Passive_Elements.Checked,
              cb_Additional_Rain_Accounting.Checked,
              cb_SaveToDB.Checked);
*)

//  oObj.IsCalcWithAdditionalRain :=aIsCalcWithAdditionalRain;


  oParams.SaveToFile(sFile);
 // oParams.Validate;
  FreeAndNil(oParams);



  {$IFDEF use_dll}
  {
  // -----------------------------
  if Load_ILink_optimize() then
  begin
    ILink_optimize.InitADO(dmMain.ADOConnection1.ConnectionObject);
    ILink_optimize.Execute(sFile);

    UnLoad_ILink_optimize();
  end;
   }
  {$ELSE}
  AssertFileExists(GetApplicationDir() + PROG_Link_Optimize);

 // ShellExec(GetApplicationDir() + PROG_Link_Optimize, DoubleQuotedStr(sFile));

  RunApp(GetApplicationDir() + PROG_Link_Optimize, DoubleQuotedStr(sFile), iCode);

  result:=iCode=1;

//  ShowMessage( IntToStr(iCode) );


  {$ENDIF}
end;





// ---------------------------------------------------------------
class function TLink_run.Run_Link_Optimize_freq_diversity(aID: Integer):
    boolean;
//-------------------------------------------------------------------
const
  DEF_TEMP_FILENAME = 'Link_Optimize.ini';
  PROG_Link_Optimize = 'Link_Optimize.exe';
var
  oParams: TIni_Link_Optimize_Params;
  sFile: string;

  iCode: integer;
begin
 // TLink_run.
  sFile := g_ApplicationDataDir + DEF_TEMP_FILENAME;

  oParams := TIni_Link_Optimize_Params.Create;
  oParams.ProjectID := dmMain.ProjectID;
  //oParams.Handle := aHandle;
  oParams.linkID := aID;
  oParams.mode := 'Optimize_freq_diversity';

 // oParams.IsSaveReportToDB     :=aIsSaveReportToDB;
 // oParams.UsePassiveElements  :=aUse_Passive_Elements;

(*    dmAct_Link.Calc (FID,
              cb_Use_Passive_Elements.Checked,
              cb_Additional_Rain_Accounting.Checked,
              cb_SaveToDB.Checked);
*)

//  oObj.IsCalcWithAdditionalRain :=aIsCalcWithAdditionalRain;


  oParams.Savetofile(sFile);
 // oParams.Validate;
  FreeAndNil(oParams);


  {$IFDEF use_dll}
  {
  // -----------------------------
  if Load_ILink_optimize() then
  begin
    ILink_optimize.InitADO(dmMain.ADOConnection1.ConnectionObject);
    ILink_optimize.Execute(sFile);

    UnLoad_ILink_optimize();
  end;
   }
  {$ELSE}

  AssertFileExists(GetApplicationDir() + PROG_Link_Optimize);

///////  ShellExec(GetApplicationDir() + PROG_Link_Optimize, DoubleQuotedStr(sFile));

  RunApp(GetApplicationDir() + PROG_Link_Optimize, DoubleQuotedStr(sFile), iCode);

  result:=iCode=1;


  {$ENDIF}


  (*
  with TIniFile.Create (sFile) do
  begin
    WriteInteger ('main', 'ProjectID', dmMain.ProjectID);
    WriteInteger ('main', 'Handle',    Handle);
    WriteInteger ('main', 'ID',        aID);
    WriteString  ('main', 'mode',      'Optimize_freq_diversity');
    Free;
  end;
*)
end;



//--------------------------------------------------------------------
class procedure TLink_run.Calc_for_graph(aID: integer);
//--------------------------------------------------------------------
const
  TEMP_INI_FILE = 'link_calc.ini';
var
  iCode: Integer;
  sFile: string;
  oObj: TIni_Link_Calc_Params;
begin
  sFile := g_ApplicationDataDir + TEMP_INI_FILE;

  oObj := TIni_Link_Calc_Params.Create;


  oObj.ConnectionString:=dmMain.GetConnectionString;
 // k:= IIF(aUsePassiveElements, 1, 0);


  oObj.ProjectID := dmMain.ProjectID;
  oObj.LinkID := aID;
 // oObj.IsSaveReportToDB         :=aIsSaveReportToDB;
 // oObj.IsUsePassiveElements     :=aUsePassiveElements;
//  oObj.IsCalcWithAdditionalRain :=aIsCalcWithAdditionalRain;
  oObj.SaveToFile(sFile);

  FreeAndNil(oObj);

  // ---------------------------------------------------------------

  {$IFDEF use_dll}
  {
  if Load_ILink_calc() then
  begin
    ILink_calc.InitADO(dmMain.ADOConnection1.ConnectionObject);
 //   ILink_graph.InitAppHandle(Application.Handle);
    ILink_calc.Execute(sFile);

    UnLoad_ILink_calc();
  end;
   }

  {$ELSE}
//  CursorHourGlass;

  AssertFileExists(GetApplicationDir() + EXE_LINK_CALC);

  RunApp(GetApplicationDir() + EXE_LINK_CALC, DoubleQuotedStr(sFile), iCode);

//  CursorDefault;
  {$ENDIF}


end;

//---------------------------------------------------------------------------
class procedure TLink_run.Calc_list(aIDList: TIDList);
//---------------------------------------------------------------------------
const
  TEMP_INI_FILE = 'link_calc.ini';
var
  iCode: Integer;
  sFile: string;
  oParams: TIni_Link_Calc_Params;
begin
//  aIsSaveReportToDB:=True;
  sFile := g_ApplicationDataDir + TEMP_INI_FILE;

  oParams := TIni_Link_Calc_Params.Create;

 // k:= IIF(aUsePassiveElements, 1, 0);

  oParams.IDList.assign(aIDList);

  oParams.ProjectID := dmMain.ProjectID;

  oParams.ConnectionString:=dmMain.GetConnectionString;

//  oParams.LinkID                   :=aID;
//  oParams.IsSaveReportToDB         :=aIsSaveReportToDB;
 // oParams.IsUsePassiveElements     :=aUsePassiveElements;
//  oParams.IsCalcWithAdditionalRain :=aIsCalcWithAdditionalRain;


  oParams.SaveToFile(sFile);

  FreeAndNil(oParams);


//  ShellExec_Notepad(sFile);

//  exit;

  // ---------------------------------------------------------------

  {$IFDEF use_dll}
  {
  if Load_ILink_calc() then
  begin
    ILink_calc.InitADO(dmMain.ADOConnection1.ConnectionObject);
 //   ILink_graph.InitAppHandle(Application.Handle);
    ILink_calc.Execute(sFile);

    UnLoad_ILink_calc();
  end;

  }

  {$ELSE}
//  CursorHourGlass;

  AssertFileExists(GetApplicationDir() + EXE_LINK_CALC);

  RunApp(GetApplicationDir() + EXE_LINK_CALC, DoubleQuotedStr(sFile), iCode);

//  CursorDefault;
  {$ENDIF}


end;

end.


