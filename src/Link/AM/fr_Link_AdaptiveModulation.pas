unit fr_Link_AdaptiveModulation;

interface

uses

XMLDoc, XMLIntf,

  Variants, Classes, Controls, Forms, SysUtils,
  Menus, cxGridTableView, cxGridBandedTableView,
  cxClasses, cxControls, cxGridCustomView, cxGridLevel,
  Graphics, Dialogs, ActnList, StdActns, StdCtrls, ExtCtrls, DB,

  cxGridCustomTableView, cxGridDBBandedTableView, cxGrid, RxMemDS,
  rxPlacemnt, cxInplaceContainer, cxVGrid, frxClass, frxDBSet, frxExportXLS,
  dxmdaset, cxGridDBTableView, ADODB, cxStyles, cxGraphics, Grids,

  u_classes_Adaptive,

  u_xml_document,

  //  cxExportGrid4Link,

//  dm_Link_calc,


  u_Link_run,

  dm_Link,

  u_cx,
  u_cx_vgrid,
  //u_cx_vgrid,

//  u_db,
  dm_Onega_DB_data,

  u_SDB_classes,

  u_Storage,

  u_LinkEndType_const,
  u_Link_const,

  u_func,
  u_files,

  u_vars,

  u_const_db,

  u_db,


  DBGrids, dxBar, cxLookAndFeels, cxLookAndFeelPainters, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxCurrencyEdit,
  cxCheckBox, cxDropDownEdit, frxExportBaseDialog;

type
  Tframe_Link_AdaptiveModulation = class(TForm)
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    cxGrid1: TcxGrid;
    cxGrid1DBBandedTableView1: TcxGridDBBandedTableView;
    col_ID: TcxGridDBBandedColumn;
    col__Mode: TcxGridDBBandedColumn;
    col_Bitrate_Mbps: TcxGridDBBandedColumn;
    col__Power_max: TcxGridDBBandedColumn;
    col__threshold_BER_3: TcxGridDBBandedColumn;
    col__threshold_BER_6: TcxGridDBBandedColumn;
    cxGrid1Level1: TcxGridLevel;
    DataSource: TDataSource;
    col_sesr: TcxGridDBBandedColumn;
    col_kng: TcxGridDBBandedColumn;
    col_rx_level: TcxGridDBBandedColumn;
    FormStorage1: TFormStorage;
    col_IsWorking: TcxGridDBBandedColumn;
    col_Modulation_type: TcxGridDBBandedColumn;
    ActionList1: TActionList;
    FileSaveAs1_excel: TFileSaveAs;
    PopupMenu1: TPopupMenu;
    SaveAs1: TMenuItem;
    col_fade_margin_dB: TcxGridDBBandedColumn;
    col_kng_req: TcxGridDBBandedColumn;
    col_sesr_req: TcxGridDBBandedColumn;
    col_bandwidth: TcxGridDBBandedColumn;
    act_Save: TAction;
    Button2: TButton;
    Panel1: TPanel;
    cxVerticalGrid1: TcxVerticalGrid;
    row_BitRate: TcxEditorRow;
    row_Sum: TcxEditorRow;
    dxMemData1: TdxMemData;
    dxMemData1THRESHOLD_BER_3: TFloatField;
    dxMemData1THRESHOLD_BER_6: TFloatField;
    dxMemData1Power_dBm: TFloatField;
    dxMemData1BitRate_Mbps: TFloatField;
    dxMemData1Modulation_type: TStringField;
    dxMemData1KNG: TFloatField;
    dxMemData1KNG_year: TFloatField;
    dxMemData1SESR: TFloatField;
    dxMemData1Rx_Level_dBm: TFloatField;
    dxMemData1FADE_MARGIN_DB: TFloatField;
    dxMemData1KNG_required: TFloatField;
    dxMemData1SESR_required: TFloatField;
    dxMemData1MODE: TIntegerField;
    dxMemData1Is_Working: TBooleanField;
    dxMemData1bandwidth: TFloatField;
    dxMemData1id: TIntegerField;
    act_Set_Priority_Bitrate_mas_0: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    act_Set_Priority_Bitrate_eq_0: TAction;
    N01: TMenuItem;
    row_B: TcxEditorRow;
    dxMemData1K: TFloatField;
    cxGrid1DBBandedTableView1K: TcxGridDBBandedColumn;
    b_Calc: TButton;
    b_Save_SDB: TButton;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_selected: TcxStyle;
    cxStyle1: TcxStyle;
    cxGrid1DBBandedTableView1Column1: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Column2: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Column3: TcxGridDBBandedColumn;
    dxMemData1T_neg_hours: TFloatField;
    dxMemData1T_work_hours: TFloatField;
    dxMemData1T_work_percent: TFloatField;
    dxMemData1sum: TFloatField;
    cxGrid1DBBandedTableView1Column4: TcxGridDBBandedColumn;
    dxMemData1Working_str: TStringField;
    dxMemData1LinkEndType_ID: TIntegerField;
    col_K_gotov: TcxGridDBBandedColumn;
    qry_Modes: TADOQuery;
    ds_Modes: TDataSource;
    Button3: TButton;
    col_Modulation_level_count: TcxGridDBBandedColumn;
    col_SIGNATURE_WIDTH: TcxGridDBBandedColumn;
    col_SIGNATURE_HEIGHT: TcxGridDBBandedColumn;
    dxMemData1SIGNATURE_WIDTH: TFloatField;
    dxMemData1SIGNATURE_height: TFloatField;
    col_kng_year: TcxGridDBBandedColumn;
    Timer1_AutoCalc: TTimer;
    col_Checked: TcxGridDBBandedColumn;
    dxMemData1checked: TBooleanField;
    row_Config: TcxEditorRow;
    act_row_check: TAction;
    act_row_uncheck: TAction;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    Button1: TButton;
    act_row_check_in_band: TAction;
    N6: TMenuItem;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxMemData_summary: TdxMemData;
    dxMemData_summaryrow_BitRate: TFloatField;
    dxMemData_summaryrow_Sum: TFloatField;
    dxMemData_summaryrow_B: TFloatField;
    dxMemData_summaryrow_Conffig: TStringField;
    frxReport1: TfrxReport;
    frxXLSExport1: TfrxXLSExport;
    frxDBDataset_mode: TfrxDBDataset;
    frxDBDataset_linkend: TfrxDBDataset;
    frxDBDataset1_summary: TfrxDBDataset;
    q_Link: TADOQuery;
    ds_Link: TDataSource;
    Button4: TButton;
    dxMemData1power_noise: TFloatField;
    cxGrid1DBBandedTableView1Column5: TcxGridDBBandedColumn;
    act_ExecCalc: TAction;
    Button7: TButton;
    Button8: TButton;
    procedure act_ExecCalcExecute(Sender: TObject);
    procedure act_row_uncheckExecute(Sender: TObject);
    procedure act_SaveExecute(Sender: TObject);
    procedure act_Set_Priority_Bitrate_eq_0Execute(Sender: TObject);
    procedure act_Set_Priority_Bitrate_mas_0Execute(Sender: TObject);
//    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
//    procedure Button5Click(Sender: TObject);
//    procedure Button6Click(Sender: TObject);
//    procedure Button7Click(Sender: TObject);
//    procedure Button8Click(Sender: TObject);
//    procedure Button5Click(Sender: TObject);
    procedure b_CalcClick(Sender: TObject);
    procedure b_Save_SDBClick(Sender: TObject);
    procedure cxGrid1DBBandedTableView1CustomDrawCell(Sender:
        TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo:
        TcxGridTableDataCellViewInfo; var ADone: Boolean);
        procedure FormDestroy(Sender: TObject);
//    procedure b_Save_SDBClick(Sender: TObject);
//    procedure b_Save_SDB__Click(Sender: TObject);
    procedure dxMemData1CalcFields(DataSet: TDataSet);
    procedure FileSaveAs1_excelAccept(Sender: TObject);
    procedure FileSaveAs1_excelBeforeExecute(Sender: TObject);
//    procedure FileSaveAs1_excelBeforeExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1_AutoCalcTimer(Sender: TObject);
  private
    FSelected_ID: Integer;

    FAdaptiveModeList: TAdaptiveModeList;

    FBandwidth: Integer;

    FFolder: string;

    FDataset: TDataset;
    FLink_ID: Integer;

    FLink_name: string;

    FXPIC: boolean;

    FLinkEndType_id: Integer;

    procedure Calc_AM;
    procedure ExecCalc;
//    procedure DoOnLoadChecked(aDataset: TDataSet; var aTerminated: boolean);
//    procedure Exec11111111111111111;

    procedure OpenData_Modes;

    procedure Save_bitrates;
    procedure Save_Results;
    procedure Update_Checked_for_Bandwidth(aDataset: TDataset; var aTerminated:
        boolean);

    procedure PrepareData;
//    procedure SaveToXML;

//    procedure SaveToDB;

    function LoadFromDB: Boolean;

    procedure OpenData_Link;


  public
//    class procedure ExecDlg(aID: Integer);
    procedure View(aID: Integer);

    //aArr: array of TAdaptiveModeRec;
  end;



implementation

{$R *.dfm}


const
  FLD_Is_Working = 'Is_Working';




procedure Tframe_Link_AdaptiveModulation.act_ExecCalcExecute(Sender: TObject);
begin
  ExecCalc
end;

//  CAPTION_BITRATE_Mbps = '�������� [Mbps]';


// ---------------------------------------------------------------
procedure Tframe_Link_AdaptiveModulation.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  Caption:='������ � ���������� ����������.';

  cxGrid1.Align:=alClient;

  FFolder:=g_ApplicationDataDir + 'AM\';


  FDataset:=dxMemData1 ; //RxMemoryData1;

  dxMemData1.Open;


//  while dxMemData1.RecordCount>0 do
//    dxMemData1.delete;



//  dlg_SetDefaultSize(Self);

  db_CreateField (FDataset,
           [
            db_Field(FLD_ID,   ftInteger),
            db_Field(FLD_MODE, ftInteger),

            db_Field(FLD_THRESHOLD_BER_3, ftFloat),
            db_Field(FLD_THRESHOLD_BER_6, ftFloat),
            db_Field(FLD_Power_dBm,       ftFloat),
            db_Field(FLD_BitRate_Mbps,    ftFloat),

            db_Field(FLD_Modulation_type, ftString, 10),
            db_Field(FLD_Modulation_level_count, ftInteger),

//          FLD_SIGNATURE_WIDTH,        FieldValues[FLD_SIGNATURE_WIDTH],
  //        FLD_SIGNATURE_HEIGHT,       FieldValues[FLD_SIGNATURE_HEIGHT],


            db_Field(FLD_KNG,  ftFloat),
            db_Field(FLD_KNG_year,  ftFloat),

            db_Field(FLD_SESR, ftFloat),
            db_Field(FLD_Rx_Level_dBm,   ftFloat),
            db_Field(FLD_FADE_MARGIN_DB, ftFloat),
            db_Field(FLD_KNG_required,   ftFloat),
            db_Field(FLD_SESR_required,  ftFloat),

            db_Field(FLD_Is_Working, ftBoolean),

            db_Field(FLD_bandwidth, ftFloat)
           ]);

  db_CreateCalculatedField(FDataset, 'Working_str', ftString);

  FDataset.Open;



  col_IsWorking.Caption:='�����������';

 // FileSaveAs1.Dialog.Filter:=




  cx_SetColumnCaptions(cxGrid1DBBandedTableView1,
     [
      FLD_Mode,             '�����',
  //    FLD_checked,          '���',

      FLD_BITRATE_MBPS,     CAPTION_BITRATE_Mbps,
      FLD_threshold_BER_3,  CAPTION_THRESHOLD_BER_3,
      FLD_threshold_BER_6,  CAPTION_THRESHOLD_BER_6,

      FLD_Modulation_type,  CAPTION_MODULATION_TYPE,

      FLD_MODE,             CAPTION_MODE,
      FLD_Power_dBm,        CAPTION_Power_max_dBm,
      FLD_Rx_Level_dBm,     CAPTION_Rx_Level_dBm,

      FLD_Modulation_type,  CAPTION_Modulation_type,
      FLD_Modulation_level_count, '������ ���������',

      FLD_SIGNATURE_WIDTH,   '������ ���������',
      FLD_SIGNATURE_HEIGHT,  '������ ���������',


      FLD_fade_margin_dB,  CAPTION_fade_margin_dB,

      FLD_KNG,        'KNG [%]',
      FLD_KNG_year,   'KNG ��� � ���',
      FLD_SESR,       'SESR [%]',

      FLD_KNG_required,   'KNG ���� [%]',
      FLD_SESR_required,  'SESR ���� [%]',

      FLD_Is_Working,     '�����������',

      FLD_bandwidth,      '������ ������ [MHz]'


    ]);


 // cxGrid1DBBandedTableView1.ApplyBestFit();
//  cx_

  g_Storage.RestoreFromRegistry(cxGrid1DBBandedTableView1, ClassName+'5');

  FAdaptiveModeList := TAdaptiveModeList.Create (TAdaptiveMode);
end;

// ---------------------------------------------------------------
procedure Tframe_Link_AdaptiveModulation.View(aID: Integer);
// ---------------------------------------------------------------
begin
  FLink_ID:=aID;
        
  OpenData_Link;
  if not LoadFromDB then
    OpenData_Modes;


  cxGrid1DBBandedTableView1.ViewData.Expand(True);

 // OpenData_Modes;

end;



procedure Tframe_Link_AdaptiveModulation.act_SaveExecute(Sender: TObject);
begin
 //row_BitRate

   Save_Results();
end;

procedure Tframe_Link_AdaptiveModulation.act_Set_Priority_Bitrate_eq_0Execute(Sender:
    TObject);
begin
//  if FDataset.RecordCount>0 then
    row_BitRate.Properties.Value:=0;

//
end;


procedure Tframe_Link_AdaptiveModulation.Button3Click(Sender: TObject);
begin
//  frxXLSExport1.FileName:= FileSaveAs1.Dialog.fIleName;
  frxXLSExport1.ShowDialog:= False;

  frxReport1.PrepareReport();

  frxReport1.ShowReport;

//  frxReport1.Export(frxXLSExport1);



//  cxGrid1.Repaint;
end;


procedure Tframe_Link_AdaptiveModulation.Button4Click(Sender: TObject);
begin
  PrepareData();
  frxReport1.ShowReport;
  
//  OpenData_Modes;
end;


// ---------------------------------------------------------------
procedure Tframe_Link_AdaptiveModulation.OpenData_Link;
// ---------------------------------------------------------------
const
  SQL_SELECT_LINKEND =
    'select Link.AM_data_xml, Link.name as Link_name, LinkEnd.kratnostBySpace, LinkEnd.LinkEndType_id '+
    '  from Link '+
    '    left join LinkEnd on LinkEnd.id = Link.linkend1_id '+
    ' where Link.id=:id ';



begin
  Assert(FLink_ID>0);

//  dmOnega_DB_data.Link_Select(ADOStoredProc_Link, aID);
//  Assert(ADOStoredProc_Link.recordCount=1, 'ADOStoredProc_Link.recordCount=1');

//  iLinkEnd1_ID :=ADOStoredProc_Link.FieldByName(FLD_LinkEnd1_ID).AsInteger;

  dmOnega_DB_data.OpenQuery(q_Link, SQL_SELECT_LINKEND,
     [
       FLD_ID, FLink_ID
     ]);


  FLink_name:=q_Link['Link_name'];
//  FileSaveAs1.Dialog.FileName:=  ExtractFilePath( FileSaveAs1.Dialog.FileName ) + ReplaceInvalidFileNameChars (FLink_name) + '.xls';


  FLinkEndType_id:=q_Link.FieldByName('LinkEndType_id').AsInteger;

  FXPIC:=q_Link['kratnostBySpace'] >=2 ;
//  FXPIC:=True;

  cx_SetRowItemIndex( row_Config,   q_Link['kratnostBySpace']);

end;
// ---------------------------------------------------------------
procedure Tframe_Link_AdaptiveModulation.OpenData_Modes;
// ---------------------------------------------------------------
const
  SQL_SELECT_LINKENDType_MODES =
    'select * '+
    '  from view_LinkEndType_mode m '+
    '    left join LinkEnd on LinkEnd.LinkEndType_id = m.LinkEndType_id '+
    '    left join Link on Link.linkend1_id=LinkEnd.id '+
    ' where Link.id=:id ';



begin
  Assert(FLink_ID>0);

//  dmOnega_DB_data.Link_Select(ADOStoredProc_Link, aID);
//  Assert(ADOStoredProc_Link.recordCount=1, 'ADOStoredProc_Link.recordCount=1');

//  iLinkEnd1_ID :=ADOStoredProc_Link.FieldByName(FLD_LinkEnd1_ID).AsInteger;




  dmOnega_DB_data.OpenQuery(qry_Modes, SQL_SELECT_LINKENDType_MODES,
     [
       FLD_ID, FLink_ID
     ]);

  FDataset.Close;
  FDataset.Open;

    //   TForEachProc = procedure(aDataset: TDataSet; var aTerminated: boolean) of object;

//  function db_ForEach(aDataset: TDataset; aProc: TForEachProc): boolean;

  with qry_Modes do
    while not EOF do
    begin
      db_AddRecord_(FDataset,
        [
          FLD_LinkEndType_ID, FieldValues[FLD_LinkEndType_ID],
          FLD_ID,             FieldValues[FLD_ID],

          FLD_MODE,           FieldValues[FLD_MODE],
          FLD_THRESHOLD_BER_3,FieldValues[FLD_THRESHOLD_BER_3],
          FLD_THRESHOLD_BER_6,FieldValues[FLD_THRESHOLD_BER_6],

          FLD_Power_dBm,      FieldValues[FLD_Power_dBm],

          FLD_BitRate_Mbps,   IIF(FXPIC, 2 * FieldByName(FLD_BitRate_Mbps).AsFloat, FieldByName(FLD_BitRate_Mbps).AsFloat),

          FLD_Modulation_type,        FieldValues[FLD_Modulation_type],
          FLD_Modulation_level_count, FieldValues[FLD_Modulation_level_count],

          FLD_SIGNATURE_WIDTH,        FieldValues[FLD_SIGNATURE_WIDTH],
          FLD_SIGNATURE_HEIGHT,       FieldValues[FLD_SIGNATURE_HEIGHT],


          FLD_Power_noise,      FieldValues[FLD_Power_noise],

          FLD_bandwidth,      FieldValues[FLD_bandwidth],
          FLD_checked,      True

//                FAdaptiveModeArr[i].bandwidth       :=qry_Modes.FieldByName(FLD_bandwidth).AsFloat;

          //checked

        ] );

      Next;
    end;


end;


// ---------------------------------------------------------------
procedure Tframe_Link_AdaptiveModulation.Calc_AM;
// ---------------------------------------------------------------
var
  oList: TBitrateList;
  oItem: TBitrateItem;
begin
  oList:=TBitrateList.Create;

  dxMemData1.DisableControls;
  dxMemData1.First;

  while not dxMemData1.EOF do
  begin
    oItem:= oList.AddItem_();

    oItem.ID        := dxMemData1.FieldValues[FLD_ID];

    oItem.Bitrate  := dxMemData1.FieldValues[FLD_Bitrate_MBps];
    oItem.Bandwidth:= dxMemData1.FieldValues[FLD_Bandwidth];

    oItem.K := dxMemData1.FieldByName ('K').AsFloat;

    dxMemData1.Next;
  end;

  oList.Calc;

//  ---------------------

  dxMemData1.First;

  while not dxMemData1.EOF do
  begin
    oItem:= oList.FindByID(dxMemData1.FieldValues[FLD_ID]);

    Assert (oItem.T_work_percent >=0);

    db_UpdateRecord__(dxMemData1,

    [
      'T_work_percent', oItem.T_work_percent,
      'sum',            oItem.Sum

    ]);


    dxMemData1.Next;
  end;


  dxMemData1.First;
  dxMemData1.EnableControls;


end;

// ---------------------------------------------------------------
procedure Tframe_Link_AdaptiveModulation.Save_bitrates;
// ---------------------------------------------------------------
const
  FLD_SDB_bitrate_priority    = 'SDB_bitrate_priority';
  FLD_SDB_bitrate_no_priority = 'SDB_bitrate_no_priority';

var
  k: Integer;
begin
  k:=dmOnega_DB_data.ExecStoredProc_('dbo.sp_Link_Update',
    [
      FLD_ID,  FLink_ID,

      FLD_SDB_bitrate_priority,    row_BitRate.Properties.Value,
      FLD_SDB_bitrate_no_priority, row_Sum.Properties.Value
    ]);


//  @SDB_bitrate_priority float = null,
//  @SDB_bitrate_no_priority float = null

end;


// ---------------------------------------------------------------
procedure Tframe_Link_AdaptiveModulation.b_CalcClick(Sender: TObject);
// ---------------------------------------------------------------
var
  eBandwidth: Double;
  eBitRate: Double;
  eK: Double;
  oList: TBitrateList;
begin
  if row_B.Properties.Value = null then
  begin
    ShowMessage('�������� �������� ������������� ������, V��, Mbit/s');

    Exit;
  end;


  Assert(row_B.Properties.Value <> null);

  oList:=TBitrateList.Create;

 // oList.AddItem(0, 0);
  oList.AddItem(99999, 100);


  dxMemData1.First;

  with dxMemData1 do
    while not EOF do
    begin
      if FieldByName(FLD_CHECKED).AsBoolean then
      begin
        eBitRate  :=FieldByName(FLD_BitRate_Mbps).AsFloat;
        eBandwidth:=FieldByName(FLD_bandwidth).AsFloat;
        eK        :=FieldByName('k').AsFloat;

        if row_B.Properties.Value = eBandwidth then
          oList.AddItem(eBitRate, eK);

      end;
        
   //   row_B.Properties.Value;

      Next;
    end;


  oList.Sort_;

  row_Sum.Properties.Value:=oList.Calc_Sum(row_BitRate.Properties.Value);


{
  oList.AddItem(100, 234234);
  oList.AddItem(10, 234234);
  oList.AddItem(120, 234234);

  oList.Sort_;
 }

end;

procedure Tframe_Link_AdaptiveModulation.b_Save_SDBClick(Sender: TObject);
begin
  Save_bitrates;
end;

// ---------------------------------------------------------------
procedure Tframe_Link_AdaptiveModulation.Save_Results;
// ---------------------------------------------------------------
begin

  dmOnega_DB_data.ExecStoredProc_ ('sp_Link_Update_LinkEndType',
     [
      'LINK_ID' , FLink_ID,

      'LinkEndType_ID',       FDataset.FieldByName(FLD_LinkEndType_ID).AsInteger,
      'LinkEndType_Mode_ID' , FDataset.FieldByName(FLD_ID).AsInteger

     ]);

end;


procedure Tframe_Link_AdaptiveModulation.FormDestroy(Sender: TObject);
begin
  g_Storage.StoreToRegistry(cxGrid1DBBandedTableView1, ClassName+'4');

  FreeAndNil(FAdaptiveModeList);

end;

//--------------------------------------------------------------
procedure Tframe_Link_AdaptiveModulation.FileSaveAs1_excelAccept(Sender: TObject);
//--------------------------------------------------------------
begin
//  db_View(FDataset);


  frxXLSExport1.FileName:= FileSaveAs1_excel.Dialog.fIleName;
  frxXLSExport1.ShowDialog:= False;

  frxReport1.PrepareReport();

//  frxReport1.Preview;

  frxReport1.Export(frxXLSExport1);


//  cx_ExportGridToExcel(FileSaveAs1.Dialog.fIleName, cxGrid1);

//  ExportGrid4ToExcel(FileSaveAs1.Dialog.fIleName, cxGrid1);

  ShellExec(FileSaveAs1_excel.Dialog.fIleName);

end;



// ---------------------------------------------------------------
procedure Tframe_Link_AdaptiveModulation.dxMemData1CalcFields(DataSet: TDataSet);
// ---------------------------------------------------------------
var
  e: Double;
  eK: Double;
  eKNG: Double;
  eSESR: Double;
  eT_neg_hours: Double;
  eWork_hours: Double;
begin
  if not Assigned(FDataset) then
    exit;

  eSESR:=FDataset.FieldByName(FLD_SESR).AsFloat;
  eKNG :=FDataset.FieldByName(FLD_KNG).AsFloat;

  if eSESR = 0 then
    exit;



  eK := (eKNG + eSESR - 0.01 * eKNG * eSESR);

  eT_neg_hours:=8760 * eK * 0.01;

//  eWork_hours:=8760 * eK * 0.01;

//  8760

  FDataset['K']:=eK;
  FDataset['T_neg_hours']:=eT_neg_hours;


  e :=FDataset.FieldByName('T_work_percent').AsFloat;

  if e<>0 then
    FDataset['T_work_hours']:=e * 8760 * 0.01;


  if FDataset.FieldByName(FLD_Is_Working).AsBoolean = true then
    FDataset['Working_str'] := '��������';


  //    db_Field(FLD_Is_Working, ftBoolean),


//    db_CreateCalculatedField(FDataset, 'Working_str', ftString);



   {
  FDataset['K']:=0.001 * (FDataset.FieldByName(FLD_KNG).AsFloat +
         FDataset.FieldByName(FLD_SESR).AsFloat
          - 0.01 * FDataset.FieldByName(FLD_KNG).AsFloat * FDataset.FieldByName(FLD_SESR).AsFloat);
  }


//    row_BitRate.Properties.Value:=FDataset.FieldByName(FLD_BitRate_Mbps).AsFloat;
//    row_B.Properties.Value:=FDataset.FieldByName(FLD_BANDWIDTH).AsFloat;


end;


procedure Tframe_Link_AdaptiveModulation.act_Set_Priority_Bitrate_mas_0Execute(Sender: TObject);
begin
  if FDataset.RecordCount>0 then
  begin
    FSelected_ID:=FDataset.FieldByName(FLD_ID).AsInteger;

//    Edit1.Text:= IntToStr (FSelected_ID);

    cxGrid1.Invalidate();


    row_BitRate.Properties.Value:=FDataset.FieldByName(FLD_BitRate_Mbps).AsFloat;
    row_B.Properties.Value:=FDataset.FieldByName(FLD_BANDWIDTH).AsFloat;

  //  cxGrid1.Repaint;

  end;
end;


// ---------------------------------------------------------------
procedure Tframe_Link_AdaptiveModulation.ExecCalc;
// ---------------------------------------------------------------
var
  sFile_xml: string;
  
begin
//  if dmLink_Calc.Params.Validate then
 //   dmLink_Calc.Execute (dmLink_Calc.Params.LinkID, nil);
//    dmLink_Calc.ExecuteList1 (dmLink_Calc.Params.LinkID, nil);

//  dmLink_calc.FAdaptiveModeList.LoadFromDB(dxMemData1);

//  dmLink_Calc.ExecuteList1 (dmLink_Calc.Params.IDList);
//  dmLink_calc.FAdaptiveModeList.SaveResultsToDB(dxMemData1);

  sFile_xml:=GetTempXmlFileName;

  TLink_run.CalcAdaptive_new(FLink_ID, sFile_xml);

  FAdaptiveModeList.LoadFromXML(sFile_xml);
  FAdaptiveModeList.SaveResultsToDB(dxMemData1);

  Calc_AM;

end;



procedure Tframe_Link_AdaptiveModulation.cxGrid1DBBandedTableView1CustomDrawCell(
    Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo:
    TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  v: Variant;

begin
  v:= AViewInfo.GridRecord.Values[col_ID.Index];

  if v = FSelected_ID then
    ACanvas.Brush.Color:=cxStyle_selected.Color;

end;


procedure Tframe_Link_AdaptiveModulation.Timer1_AutoCalcTimer(Sender: TObject);
begin
  Timer1_AutoCalc.Enabled:=False;
  ExecCalc;
end;


// ---------------------------------------------------------------
procedure Tframe_Link_AdaptiveModulation.Update_Checked_for_Bandwidth(aDataset:
    TDataset; var aTerminated: boolean);
// ---------------------------------------------------------------
var
  iBandwidth: Integer;
begin
  iBandwidth:=aDataset.FieldByName(FLD_bandwidth).AsInteger;

  if iBandwidth = FBandwidth then
    db_UpdateRecord__(aDataset, [FLD_CHECKED, True]);


end;

// ---------------------------------------------------------------
procedure Tframe_Link_AdaptiveModulation.act_row_uncheckExecute(Sender: TObject);
// ---------------------------------------------------------------

begin

  if Sender = act_row_uncheck then
    cx_UpdateSelectedItems_BandedTable (cxGrid1DBBandedTableView1, FLD_checked, False);

  if Sender = act_row_check then
    cx_UpdateSelectedItems_BandedTable (cxGrid1DBBandedTableView1, FLD_checked, True);

  if Sender = act_row_check_in_band then
  begin
    FBandwidth:=FDataset.FieldByName(FLD_bandwidth).AsInteger;

    db_ForEach (FDataset, Update_Checked_for_Bandwidth);

 //   dxMemData1.Filtered:=True;
  //  dxMemData1.Filter:='bandwidth=14';

  end;
//    cx_UpdateSelectedItems_BandedTable (cxGrid1DBBandedTableView1, FLD_checked, True);


//..    bandwidth

    //  procedure cx_UpdateSelectedItems_BandedTable(aGrid: TcxGridDBBandedTableView; aFieldName:   string; aValue: Variant);

//      procedure cx_GetCheckedItemsToStrList(aGrid: TcxGridDBBandedTableView;
  //    aOutStrList: TStrings; aKeyFieldName: string = 'id'; aCheckFieldName:   string = 'checked'); overload;

 //

end;


procedure Tframe_Link_AdaptiveModulation.Button5Click(Sender: TObject);
var
  sFile: string;
begin
  sFile:=GetTempXmlFileName;

  TLink_run.CalcAdaptive_new(FLink_ID, sFile);

  FAdaptiveModeList.LoadFromXML(sFile);
  FAdaptiveModeList.SaveResultsToDB(dxMemData1);
end;


procedure Tframe_Link_AdaptiveModulation.Button7Click(Sender: TObject);
begin
//  SaveToXML
end;

procedure Tframe_Link_AdaptiveModulation.FileSaveAs1_excelBeforeExecute(Sender:
    TObject);
begin
   with TFileSaveAs(Sender).Dialog do
     FileName:=  ExtractFilePath( FileName ) + ReplaceInvalidFileNameChars (FLink_name) + '.xls';

end;




// ---------------------------------------------------------------
function Tframe_Link_AdaptiveModulation.LoadFromDB: Boolean;
// ---------------------------------------------------------------
{
const
  SQL_SELECT_LINKEND =
    'select Link.AM_data_xml, Link.name as Link_name, LinkEnd.kratnostBySpace '+
    '  from Link '+
    '    left join LinkEnd on LinkEnd.id = Link.linkend1_id '+
    ' where Link.id=:id ';
 }

var
//  iLinkEndType_id: Integer;
//  oXML: TXmlDocumentEx;
//  oStream: TStringStream;
//  s: string;

  sFile_bin: string;
//  sFile_xml: string;
//  sXML: string;
//  vXMLNode: IXMLNode;

begin
  result:=False;

  sFile_bin:= g_ApplicationDataDir + 'am_dxMemData1.bin';

 // sFile_txt:=GetTempFileNameWithExt('txt');
//  sFile_xml:=GetTempXmlFileName;

{
  dmOnega_DB_data.OpenQuery_(q_Link, SQL_SELECT_LINKEND,
     [
       FLD_ID, FLink_ID
     ]);

  sXML:=q_Link.FieldByName('AM_data_xml').AsString;
  if sXML='' then
    Exit;
 }

//  StrToTxtFile( sXML, 'd:\1.xml') ;


  if dmLink.SaveToBinFile(FLink_ID, 'AM_data_bin', sFile_bin) then
    dxMemData1.LoadFromBinaryFile(sFile_bin);

{

  oXML:=TXmlDocumentEx.Create;
  oXML.LoadFromText(sXML);

  //FLinkEndType_id

  vXMLNode:=oXML.FindNodeByPath(oXML.DocumentElement, ['params']);
  iLinkEndType_id :=oXML.GetIntAttr(vXMLNode,'LinkEndType_id');

  Assert(iLinkEndType_id>0);

  if iLinkEndType_id <> FLinkEndType_id then
  begin
    ShowMessage('�������� ������ ������������');
    Exit;
  end;

  vXMLNode:=oXML.FindNodeByPath(oXML.DocumentElement, ['DATA']);

  StrToTxtFile(vXMLNode.Text, sFile_txt ) ;

  dxMemData1.LoadFromTextFile(sFile_txt);

  FreeAndNil(oXML);
  }



  result:=True;

end;


procedure Tframe_Link_AdaptiveModulation.PrepareData;
begin
  dxMemData_summary.Active:=true;

  db_UpdateRecord__(dxMemData_summary,
    [
    'row_BitRate', row_BitRate.Properties.Value,
    'row_Sum',     row_Sum.Properties.Value,
    'row_B',       row_B.Properties.Value,
    'row_Config',  row_Config.Properties.Value

    ]);
end;


end.



{

// ---------------------------------------------------------------
procedure Tframe_Link_AdaptiveModulation.SaveToDB;
// ---------------------------------------------------------------
var
  oXML: TXmlDocumentEx;
//  oStream: TStringStream;
  s: string;
  sFile_txt: string;
//  sFile_xml: string;
  sXML: string;

//  sFile: string;
begin

  sFile_txt:=GetTempFileNameWithExt('txt');
//  sFile_xml:=GetTempXmlFileName;


 // sFile:=GetTempXmlFileName;
//  sFile:='d:\111.txt';

//  oStream:=TStringStream.create('');

  dxMemData1.SaveToTextFile(sFile_txt);


  TextFileToStr(sFile_txt,s) ;
//  dxMemData1.SaveToTextFile('d:\111.txt');

  oXML:=TXmlDocumentEx.Create;

//  oXML.DocumentElement.Attributes['Selected_ID']:=FSelected_ID;

  with oXML.DocumentElement.AddChild('PARAMS') do
  begin

    Attributes['Selected_ID']   :=FSelected_ID;
    Attributes['LinkEndType_id']:=q_Link[FLD_LinkEndType_id];
  end;


//

  oXML.DocumentElement.AddChild('DATA').Text:=s; // '������ ��������� ��������'; //oStream.DataString;
//  oXML.SaveToFile( 'd:\111.xml');
  sXML:=oXML.SaveToText;


  FreeAndNil(oXML);


  dmOnega_DB_data.ExecCommand_( //dmOnega_DB_data.ADOQuery1,
     'update link set AM_data_xml=:AM_data_xml where id=:id',
     [
      'id',           FLink_ID,
      'AM_data_xml',  sXML

     ]);

end;





//----------------------------------------------------------------------------
procedure TdmReport.LoadFromFile (aID: integer; aFileName: string);
//----------------------------------------------------------------------------
var
  rec: TdbBlobRec;
begin
  rec.TableName:=TableName;
  rec.ID:=aID;
  rec.FieldName:=FLD_CONTENT_BIN;
  rec.FileName:=aFileName;

  db_Blob_LoadFromFile (dmMain.ADOConnection, rec, false);

  Update_ (aID, [db_Par(FLD_FILENAME, ExtractFileName(aFileName))] );

end;

//----------------------------------------------------------------------------
function TdmReport.SaveToFile(aID: integer; aFileName: string): boolean;
//----------------------------------------------------------------------------
var
  rec: TdbBlobRec;
begin
  rec.TableName:=TableName;
  rec.ID:=aID;
  rec.FieldName:=FLD_CONTENT_BIN;
  rec.FileName:=aFileName;


  Result:=db_Blob_SaveToFile (dmMain.ADOConnection, rec, false);
end;
