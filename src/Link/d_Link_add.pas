
unit d_Link_add;

interface

uses
  cxInplaceContainer, cxVGrid, DB, ADODB, ComCtrls, ExtCtrls, cxControls,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Math,
  Registry, cxButtonEdit, cxDBLookupComboBox,   Variants,
  StdCtrls, cxPropertiesStore, rxPlacemnt, ActnList, Menus, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  dxSkinsDefaultPainters, cxStyles, cxEdit, cxCheckBox, cxDropDownEdit,

  u_log,
  dm_Folder,

  d_Wizard_Add_with_params,

  dm_Link_add_data,

  dm_User_Security,

  dm_Act_Map,

  dm_MapEngine_store,

  I_Shell,
  u_Shell_new,

  dm_Onega_DB_data,

  dm_Main,

  u_Geo,
  u_func,

  u_cx,
  u_cx_VGrid,
  u_db,
  u_radio,
  u_classes,
  u_dlg,

  u_link_const,
  u_const_str,
  u_const_db,
  u_types,

  dm_ClutterModel,
 // dm_Antenna,

  dm_Property,
  dm_LinkEnd,
  dm_LinkEndType,
  dm_Link,

  fr_dlg_Link_add,

  cxRadioGroup, ImgList

  ;

type
  Tdlg_Link_add = class(Tdlg_Wizard_add_with_params)
    Button1: TButton;
    act_MakeLinkName: TAction;
    StatusBar1: TStatusBar;
    StatusBar2: TStatusBar;
    qry_Temp1: TADOQuery;
    TabSheet_LinkEnds: TTabSheet;
    cxVerticalGrid1: TcxVerticalGrid;
    row_ClutterModel: TcxEditorRow;
    pn_Left: TPanel;
    Splitter1: TSplitter;
    pn_Right: TPanel;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    cxVerticalGrid1EditorRow1: TcxEditorRow;
    cxVerticalGrid1EditorRow2: TcxEditorRow;
    row_SESR: TcxEditorRow;
    row_KNG: TcxEditorRow;
    row_calc_method: TcxEditorRow;
    row_Space_limit: TcxEditorRow;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;
    PopupMenu1: TPopupMenu;
    ActionList2: TActionList;
    act_Restore_default: TAction;
    Action11: TMenuItem;
    row_RRV_model: TcxEditorRow;
    row_RRV_source: TcxEditorRow;
    cxVerticalGrid1CategoryRow3: TcxCategoryRow;
    row_Use_fixed_values: TcxEditorRow;
    row_Template_site: TcxEditorRow;
    ImageList1: TImageList;
    row_use_template: TcxEditorRow;
    cb_Name_Site1_Site2: TCheckBox;


    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//    procedure ActionList2Update(Action: TBasicAction; var Handled: Boolean);
    procedure row_LinkTypeButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure FormResize(Sender: TObject);
    procedure act_MakeLinkNameExecute(Sender: TObject);
    procedure act_Restore_defaultExecute(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
//    procedure Panel2Click(Sender: TObject);
    procedure row_RRV_modelEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure row_TemplateEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure row_use_templateEditPropertiesEditValueChanged(
      Sender: TObject);


  private
    Fframe_Link_add1: Tframe_Link_add;
    Fframe_Link_add2: Tframe_Link_add;

    FLinkEnd1: integer;
    FLinkEnd2: integer;

    FID,FFolderID,FClutterMOdelID: integer;

//    FRepeater_ID: integer;

    FTemplate_LINK_ID: integer;

    procedure DoOnLinkEndChanged(Sender: TObject);
    //; aLinkend_ID: Integer);

//
 //   procedure UpdateDistanceStatus();
    procedure DoOnChange(Sender: TObject);
  private
    FVector: TBLVector;

    procedure CalcReserve;

    procedure Append;
    procedure SaveToClass;
  public
    class function ExecDlg (aFolderID: integer; aVector: TBLVector): integer;
  end;


//====================================================================
implementation {$R *.dfm}
//====================================================================


//--------------------------------------------------------------------
class function Tdlg_Link_add.ExecDlg (aFolderID: integer; aVector: TBLVector): integer;
//--------------------------------------------------------------------
begin
{
  if dmUser_Security. then
  begin
    ShowMessage('���������� �������� ������. ������ ������ ��� ������.');
    Result:=0;
    Exit;
  end;

}

  with Tdlg_Link_add.Create(Application) do
  begin
//    dmMain.ADOConnection.BeginTrans;

    FVector:=aVector;
    FFolderID:=aFolderID;

    Fframe_Link_add1.Params.Property_BLPos:=aVector.Point1;
    Fframe_Link_add2.Params.Property_BLPos:=aVector.Point2;

//    Fframe_Link_add1.OnChange := DoOnLinkEndChanged;
//    Fframe_Link_add2.OnChange := DoOnLinkEndChanged;


    Fframe_Link_add1.Init;
    Fframe_Link_add2.Init;

    CalcReserve;

    act_MakeLinkName.Execute;

    if (ShowModal=mrOk) and (FID>0) then
    begin
      Result:=FID;
  //    dmMain.ADOConnection.CommitTrans;
    end else begin
      Result:=0;
   //   dmMain.ADOConnection.RollbackTrans;
    end;

    Free;
  end;
end;

//--------------------------------------------------------------------
procedure Tdlg_Link_add.FormCreate(Sender: TObject);
//--------------------------------------------------------------------

var
  I: Integer;
  k: Integer;
  oLookupComboBoxProperties: TcxLookupComboBoxProperties;
  oRegIniFile: TRegIniFile;
begin
  inherited;

  FRegPath:=FRegPath + '3\';

  SetActionName (STR_DLG_ADD_LINK);

  StatusBar1.Panels[0].Text:= '������ ��� ��������� ���������:';
  StatusBar1.Panels[1].Text:= '����� ��������� (km): ';
  StatusBar2.Panels[0].Text:= '�������� �� ����� �������� (dBm): ';
  StatusBar2.Panels[1].Text:= '����� �� ��������� (dB): ';


  row_Template_site.Properties.Caption:='������';


  pn_Right.Align:=alClient;
  lb_Name.Caption:=STR_NAME;
 // row_NetID.Properties.Caption:= STR_NET_CODE;

  ed_Name_.Text:=dmLink.GetNewName();


  TdmLink_add_data.Init;
//  dmLink_add_data.Open();


  CreateChildForm(Tframe_Link_add,  Fframe_Link_add1, pn_Left);
  CreateChildForm(Tframe_Link_add,  Fframe_Link_add2, pn_Right);

  Fframe_Link_add1.Update_PROPERTY;

//  LockWindowUpdate(Fframe_Link_add2.FComboBoxAutoComplete.Handle);
//  Fframe_Link_add2.FComboBoxAutoComplete.Items.Assign (Fframe_Link_add1.FComboBoxAutoComplete.Items);
//  Fframe_Link_add2.FComboBoxAutoComplete.StoredItems.Assign (Fframe_Link_add1.FComboBoxAutoComplete.StoredItems);

  //  FComboBoxAutoComplete.StoredItems.Assign(FComboBoxAutoComplete.Items);

//  LockWindowUpdate(0);



  Fframe_Link_add1.Index:=1;
  Fframe_Link_add2.Index:=2;

  Fframe_Link_add1.OnChange:=DoOnChange;
  Fframe_Link_add2.OnChange:=DoOnChange;

  AddComponentProp(PageControl1, 'ActivePage');


 // AddComponentProp(GSPages1, 'ActivePage');

{  AddComponentProp(row_Repeater, DEF_PropertiesValue);
  AddComponentProp(row_Antenna1_H, DEF_PropertiesValue);
  AddComponentProp(row_Antenna2_H, DEF_PropertiesValue);
}

  RestoreFrom();




  oLookupComboBoxProperties:=TcxLookupComboBoxProperties(row_calc_method.Properties.EditProperties);
  oLookupComboBoxProperties.Items.Clear;

  for I := 0 to High(LINK_CALC_METHODS_ARR) do
     oLookupComboBoxProperties.Items.AddObject(
       LINK_CALC_METHODS_ARR[i].Name,
       Pointer(LINK_CALC_METHODS_ARR[i].id)
     );

//  FRegPath


  try

    row_SESR.Properties.Value        := 0.0125;
    row_KNG.Properties.Value         := 0.003;
    row_Space_limit.Properties.Value := 1;

    row_calc_method.Properties.Value := 0;
    row_RRV_source.Properties.Value  := 0;
  except
  end;

 /// cx_VerticalGrid_LoadFromReg_(cxVerticalGrid1, FRegPath);


  try

  //  oRegIniFile := TRegIniFile.Create(FRegPath);

//    with oRegIniFile do
    with TRegIniFile.Create(FRegPath) do
    begin
      FTemplate_LINK_ID:=ReadInteger('', FLD_Template_LINK_ID, 0);


      FClutterModelID:=ReadInteger('', FLD_CLUTTER_MODEL_ID, 0);
     // := ReadInteger('', row_NamesGenerateMethod.Name, 0);


      row_Use_fixed_values.Properties.Value := ReadBool('', row_Use_fixed_values.Name, False);
      row_use_template.Properties.Value     := ReadBool('', row_use_template.Name, False);

    ///  oRegIniFile.read
//    if RegIniFile.ValueExists('MyFloat') then // ???? ?????? ???????? ??????????, ??
//2
//    Edit2.Text:=FloatToStr(RegIniFile.ReadFloat('MyFloat')); // ????????? ?

      if ValueExists(row_SESR.Name) then
        row_SESR.Properties.Value        := ReadFloat(row_SESR.Name);

      if ValueExists(row_KNG.Name) then
        row_KNG.Properties.Value         := ReadFloat(row_KNG.Name);

      row_Space_limit.Properties.Value := ReadInteger('', row_Space_limit.Name, 1);

{
      row_SESR.Properties.Value        := ReadFloat('', row_SESR.Name, 0.0125);
      row_KNG.Properties.Value         := ReadFloat('', row_KNG.Name,  0.003);
      row_Space_limit.Properties.Value := ReadInteger('', row_Space_limit.Name, 1);
}

  //    row_calc_method.Properties.Value := ReadString('', row_calc_method.Name, LINK_CALC_METHODS_ARR[0].Name);

      k := ReadInteger('', row_calc_method.Name, 0);

      cx_SetRowItemIndex(row_calc_method, k);


      row_RRV_source.Properties.Value := ReadInteger('', row_RRV_source.Name, 0);

      row_RRV_model.Properties.Value:=ReadString ('', row_RRV_model.Name, '');
      row_RRV_model.Tag             :=ReadInteger('', row_RRV_model.Name+ '_tag', 0);

      if row_RRV_model.Tag=0 then
        row_RRV_model.Properties.Value:='';


      Free;
    end;

  except
  end;



  if FClutterMOdelID=0 then
    FClutterMOdelID:=dmClutterModel.GetDefaultID();


  row_ClutterModel.Properties.Value :=gl_DB.GetNameByID(TBL_CLUTTERMODEL, FClutterModelID);
  row_Template_site.Properties.Value:=gl_DB.GetNameByID(TBL_Template_LINK, FTemplate_LINK_ID);

//  row_NamesGenerateMethod.Text:=row_NamesGenerateMethod.Items[Fnames_generate_method];

  FLinkEnd1:= 0;
  FLinkEnd2:= 0;


  cxVerticalGrid1.Align:=alClient;

 // row_Repeater.Properties.Value:=False;
 // row_Repeater.Visible:=False;


  cx_InitVerticalGrid (cxVerticalGrid1);

 // SetDefaultWidth();

  FormResize(nil);
end;

//--------------------------------------------------------------------
procedure Tdlg_Link_add.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
var
  k: Integer;
begin
  try

  with TRegIniFile.Create(FRegPath) do
  begin
    WriteInteger('', FLD_CLUTTER_MODEL_ID, FClutterModelID);
    WriteInteger('', FLD_Template_LINK_ID, FTemplate_LINK_ID);

    WriteBool('', row_Use_fixed_values.Name, row_Use_fixed_values.Properties.Value);
    WriteBool('', row_use_template.Name,     row_use_template.Properties.Value);

    WriteInteger('', row_ClutterModel.Name+ '_tag', row_ClutterModel.Tag);

//  row_ClutterModel.Properties.Value:=gl_DB.GetNameByID(TBL_CLUTTERMODEL, FClutterModelID);

    WriteFloat(row_SESR.Name, row_SESR.Properties.Value);
    WriteFloat(row_KNG.Name,  row_KNG.Properties.Value);

 //   WriteFloat('', row_SESR.Name, row_SESR.Properties.Value);
 //   WriteFloat('', row_KNG.Name,  row_KNG.Properties.Value);
    WriteInteger('', row_Space_limit.Name, row_Space_limit.Properties.Value);


    k:=cx_GetRowItemIndex(row_calc_method);

    WriteInteger('', row_calc_method.Name, k);

    WriteInteger('', row_RRV_source.Name, row_RRV_source.Properties.Value);

    WriteString ('', row_RRV_model.Name,         row_RRV_model.Properties.Value);
    WriteInteger('', row_RRV_model.Name+ '_tag', row_RRV_model.Tag);


   // WriteInteger ('', row_NamesGenerateMethod.Name,
   //     row_NamesGenerateMethod.Items.IndexOf(row_NamesGenerateMethod.Text));

    Free;
  end;

  except
   // g_Lo
  end;

  inherited;
end;


procedure Tdlg_Link_add.act_OkExecute(Sender: TObject);
begin
  inherited;
  Append;
end;



//--------------------------------------------------------------------
procedure Tdlg_Link_add.SaveToClass;
//--------------------------------------------------------------------
var
  rec: record
    IsUse_fixed_values : boolean;

    SESR : double;
    kng  : double;
    Space_limit : double;

  end;

begin
  rec.IsUse_fixed_values:=row_Use_fixed_values.Properties.Value ;

  if rec.IsUse_fixed_values then
  begin
    rec.SESR        :=AsFloat(row_SESR.Properties.Value);
    rec.kng         :=AsFloat(row_KNG.Properties.Value);
    rec.Space_limit :=AsInteger(row_Space_limit.Properties.Value);

  end;     

end;



//--------------------------------------------------------------------
procedure Tdlg_Link_add.Append;
//--------------------------------------------------------------------
var
  rec: TdmLinkAddRec;
  sLinkEnd1, sLinkEnd2: string;
  i, iLinkEndID1, iLinkEndID2: integer;
  k: Integer;
  oAntIdList: TIDList;
  sFolderGUID: string;
  sGUID: string;
begin
  Assert (Fframe_Link_add1.PropertyID <> Fframe_Link_add2.PropertyID, 'Fframe_Link_add1.PropertyID <> Fframe_Link_add2.PropertyID');

  if cb_Name_Site1_Site2.Checked then
    act_MakeLinkNameExecute(nil);



  FillChar (rec, SizeOf(rec), 0);

  rec.NewName:= ed_Name_.Text;

{
  case row_NamesGenerateMethod.Items.IndexOf(row_NamesGenerateMethod.Text) of    //
    0: rec.Name:= ed_Name_.Text;
    1: rec.Name:= row_NetID.Text;
    2: rec.Name:= ed_Name_.Text   +' - '+ row_NetID.Text;
    3: rec.Name:= row_NetID.Text +' - '+ ed_Name_.Text;
  end;    // case
  if row_NetID.Text = '' then
  rec.Name:=ed_Name_.Text;
}


///////////////////  rec.Name:=ed_Name_.Text;




  rec.FolderID:=FFolderID;
  rec.ClutterModelID:=FClutterMOdelID;
{  if Assigned(row_NetID) then
    rec.Beenet:=row_NetID.Text;}

//  rec.Project_ID :=dmMain.ProjectID;

//    LinkEnd1_ID:=Fframe_Link_add1.GetLinkEndID();

  if row_use_template.Properties.Value=True then
  begin
    Assert (FTemplate_Link_ID >0, 'FTemplate_Link_ID >0');

    rec.Template_Link_ID:=FTemplate_Link_ID;

    rec.Property1_ID:=Fframe_Link_add1.PropertyID;
    rec.Property2_ID:=Fframe_Link_add2.PropertyID;


  end else

//  if row_use_template.Properties.Value=False then
  begin
    rec.LinkEnd1_ID:=Fframe_Link_add1.GetLinkEndID();
    rec.LinkEnd2_ID:=Fframe_Link_add2.GetLinkEndID();

    //-------------------------------------------

    Assert(rec.LinkEnd1_ID>0, 'aRec.LinkEnd1_ID>0');
    Assert(rec.LinkEnd2_ID>0, 'aRec.LinkEnd2_ID>0');

    Assert (rec.LinkEnd1_ID <> rec.LinkEnd2_ID, 'rec.LinkEnd1_ID <> rec.LinkEnd2_ID');

  end;



//  rec.Link_Repeater_ID  := FRepeater_ID;


  rec.IsUse_fixed_values:=row_Use_fixed_values.Properties.Value ;


  if rec.IsUse_fixed_values then
  begin
    rec.SESR        :=AsFloat(row_SESR.Properties.Value);
    rec.kng         :=AsFloat(row_KNG.Properties.Value);
    rec.Space_limit :=AsInteger(row_Space_limit.Properties.Value);

  end;



  k:=cx_GetRowItemIndex(row_calc_method);

  if k>=0 then
    rec.calc_method :=LINK_CALC_METHODS_ARR[k].ID;


  if row_RRV_source.Properties.Value = 1 then
    rec.LinkType_ID := row_RRV_model.Tag;


//  rec.calc_method :=AsInteger(row_calc_method.Properties.Value);



  sLinkEnd1:=Fframe_Link_add1.GetPropertyName +' -> '+ Fframe_Link_add2.GetPropertyName;
  sLinkEnd2:=Fframe_Link_add2.GetPropertyName +' -> '+ Fframe_Link_add1.GetPropertyName;


  FID:= dmLink.Add (rec);
  if FID<=0 then
    Exit;


//  if cb_Rename_RRS.checked then
 //   dmOnega_DB_data.Link_Rename1(FID);


  //update LOW,HIGH
 ///// dmOnega_DB_data.LinkEnd_UpdateNext(rec.LinkEnd1_ID);

 /////////// dmLinkEnd.UpdateNextLinkEnd (rec.LinkEndID1);


  if FID > 0 then
  begin
   // {$IFDEF test}
      dmMapEngine_store.Feature_Add(OBJ_LINK, FID);

      if not dmMapEngine_store.Is_Object_Map_Exists(OBJ_LINK) then
         dmMapEngine_store.Open_Object_Map(OBJ_LINK);



//    {$ELSE}
//      dmMapEngine.CreateObject (otLink, FID);

  (*    dmLink.GetLinkEndID(FID, iLinkEndID1, iLinkEndID2);

      oAntIdList:= TIDList.Create;

      dmLinkEnd.GetAntennaIDList (iLinkEndID1, oAntIdList);
      dmLinkEnd.GetAntennaIDList (iLinkEndID2, oAntIdList);

      for I := 0 to oAntIdList.Count-1 do
        dmMapEngine.ReCreateObject(otLinkEnd_Antenna, oAntIdList[i].ID);


      FreeAndNil(oAntIdList);*)

//      dmAct_Map.MapRefresh;

 //   {$ENDIF}


  end;

  //FFolderID
{

  if FFolderID>0 then
    sFolderGUID:=dmFolder.GetGUIDByID(FFolderID)
  else
//  if aFolderID=0 then
    sFolderGUID:=g_Obj.ItemByName[OBJ_LINK].RootFolderGUID;


  g_Shell.UpdateNodeChildren_ByGUID (sFolderGUID);
  g_Shell.EXPAND_BY_GUID(sFolderGUID);

  g_Shell.FocuseNodeByObjName (OBJ_LINK, FID);
  }

end;


//--------------------------------------------------------------------
procedure Tdlg_Link_add.CalcReserve;
//--------------------------------------------------------------------
var
//  b1: Boolean;
//  b2: Boolean;
//  eFade_margin_dB: Double;
//  ePout: double;
//  iLinkEnd1, iLinkEnd2: integer;

//  rLinkEnd1, rLinkEnd2: TLinkEnd_info_rec;

//  oVector: TBLVector;

  rec: TdmLinkCalcReserve_Rec;
begin

(*
function Tframe_Link_add.GetLinkEnd_info_rec(var aRec: TLinkEnd_info_rec):
    Boolean;
*)


  FLinkEnd1:= Fframe_Link_add1.GetLinkEndID();
  FLinkEnd2:= Fframe_Link_add2.GetLinkEndID();

  {
  b1:=Fframe_Link_add1.GetLinkEnd_info_rec(rLinkEnd1);
  b2:=Fframe_Link_add2.GetLinkEnd_info_rec(rLinkEnd2);

  if not (b1 and b2) then
    Exit;
      }

//  if (iLinkEnd1 = 0) or (iLinkEnd2 = 0) then
//    exit;

//  oVector:=MakeBLVector(rLinkEnd2.Property_BLPoint,
//                        rLinkEnd1.Property_BLPoint);


//  oVector:=MakeBLVector(Fframe_Link_add1.GetPropertyPos,
//                        Fframe_Link_add2.GetPropertyPos);
//
// aRec: TdmLinkCalcReserve_Rec;

 // Result:=
  FillChar(rec, SizeOf(rec), 0);

  if row_use_template.Properties.Value=True then
  begin
    rec.Template_Link_ID:=FTemplate_Link_ID;
    rec.Property1_ID:=Fframe_Link_add1.PropertyID;
    rec.Property2_ID:=Fframe_Link_add2.PropertyID;
  end else
  begin
    rec.LinkEndID1:=Fframe_Link_add1.GetLinkEndID();
    rec.LinkEndID2:=Fframe_Link_add2.GetLinkEndID();

  end;

  if ( (rec.Template_Link_ID>0) and (rec.Property1_ID>0) and (rec.Property2_ID>0)  and (rec.Property1_ID<>rec.Property2_ID) )
     or
    ( (rec.Template_Link_ID=0) and (rec.LinkEndID1>0) and (rec.LinkEndID2>0) )
  then
    dmLink.CalcReserve(rec) //, iLinkEnd1,iLinkEnd2, oVector, ePout, eFade_margin_dB);
  else
    exit;


  if rec.Result.Fade_margin_dB<0 then
    StatusBar2.Color:=clRed
  else
    StatusBar2.Color:=clLime;

//  UpdateDistanceStatus();

  StatusBar1.Color:=clYellow;


  StatusBar2.Panels[0].Text:= Format('�������� �� ����� �������� (dBm): %1.2f', [rec.Result.RxLevel_dBm]);
  StatusBar2.Panels[1].Text:= Format('����� �� ��������� (dB): %1.2f', [rec.Result.Fade_margin_dB]);
  StatusBar1.Panels[1].Text:= Format('����� ��������� (km): %1.2f', [rec.Result.Length_km]);


{
  if (FLinkEnd1 <> iLinkEnd1) or (FLinkEnd2 <> iLinkEnd2) then
    if dmLinkEnd.GetIntFieldValue__(iLinkEnd1, OBJ_LINKEND, FLD_LINKENDType_ID) <>
       dmLinkEnd.GetIntFieldValue__(iLinkEnd2, OBJ_LINKEND, FLD_LINKENDType_ID)
    then
      MsgDlg('��������! ������������ ��� ������ �����!');
}

//  FLinkEnd1:= iLinkEnd1;
//  FLinkEnd2:= iLinkEnd2;

end;

{
//------------------------------------------------------------------
procedure Tdlg_Link_add.UpdateDistanceStatus();
//------------------------------------------------------------------
var
  oVector: TBLVector;
begin
  if (Fframe_Link_add1.PropertyID>0) and
     (Fframe_Link_add2.PropertyID>0) then
  begin
    oVector:=MakeBLVector(Fframe_Link_add1.GetPropertyPos, Fframe_Link_add2.GetPropertyPos);

    StatusBar1.Panels[1].Text:= '����� ��������� (km): '+AsString(geo_Distance_m (oVector)/1000);
  end;
end;
 }


procedure Tdlg_Link_add.DoOnChange(Sender: TObject);
begin
  CalcReserve;

  if cb_Name_Site1_Site2.Checked then
    act_MakeLinkName.Execute
//    ed_Name.Text:= Fframe_Link_add1.GetPropertyName +' - '+ Fframe_Link_add2.GetPropertyName;

//  sLinkEnd2:=Fframe_Link_add2.GetPropertyName +' -> '+ Fframe_Link_add1.GetPropertyName;


//  UpdateDistanceStatus();
end;


//------------------------------------------------------------------
procedure Tdlg_Link_add.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//------------------------------------------------------------------
var
  iProperty1_ID: Integer;
  iProperty2_ID: Integer;
//  k1: Integer;
//  k2: Integer;
//  v: Variant;
begin
  if row_use_template.Properties.Value=True then
  begin
//    rec.Template_Link_ID:=FTemplate_Link_ID;
    iProperty1_ID:=Fframe_Link_add1.PropertyID;
    iProperty2_ID:=Fframe_Link_add2.PropertyID;


    act_Ok.Enabled:=
       (FTemplate_Link_ID > 0) and
       (iProperty1_ID > 0) and
       (iProperty2_ID > 0) and
       (iProperty1_ID <> iProperty2_ID) and
       (FClutterModelID > 0);

  end else
  begin
    act_Ok.Enabled:=
       Fframe_Link_add1.IsAllowAppend and
       Fframe_Link_add2.IsAllowAppend and
       (FClutterModelID > 0);

//    iLinkEndID1:=Fframe_Link_add1.GetLinkEndID();
//    iLinkEndID2:=Fframe_Link_add2.GetLinkEndID();

  end;

  {
  k1:=Fframe_Link_add1.propertyID;
  k2:=Fframe_Link_add2.propertyID;

  act_Ok.Enabled:=true;
  Exit;
  }


   {
  if Fframe_Link_add1.IsAllowAppend and
     Fframe_Link_add2.IsAllowAppend
  then
   ;///// CalcReserve;
    }
   {

   Exit;
   /////////////////////////////////////////////

//  TcxEditorRow.Properties.Options.Editing


//  row_RRV_model.Expanded//
  row_RRV_model.Properties.EditProperties.ReadOnly:= True;

//
  v:=row_RRV_source.Properties.Value;// := 1;
//
//  row_RRV_model.Properties.EditProperties.ReadOnly:= row_RRV_source.Properties.Value= 1;
  row_RRV_model.Properties.Options.Editing:= row_RRV_source.Properties.Value= 1;
  }

end;



//------------------------------------------------------------------
procedure Tdlg_Link_add.row_LinkTypeButtonClick(Sender: TObject; AbsoluteIndex: Integer);
//------------------------------------------------------------------
begin
  if cxVerticalGrid1.FocusedRow=row_ClutterModel then
    if Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otClutterModel, FClutterModelID) then
      row_ClutterModel.Tag:=FClutterModelID;
//    Dlg_SelectObject_cx (row_ClutterModel, otClutterModel, FClutterModelID);


{
  if cxVerticalGrid1.FocusedRow=row_Repeater  then
    if Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otLinkRepeater, FRepeater_ID) then
      row_Repeater.Tag:=FRepeater_ID;
}

//    Dlg_SelectObject_cx (row_Repeater_propperty, otProperty, FRepeater_propperty_ID);


end;


//---------------------------------------------------------------
procedure Tdlg_Link_add.FormResize(Sender: TObject);
//---------------------------------------------------------------
begin
  pn_Left.Width:=TabSheet_LinkEnds.Width div 2;

  StatusBar1.Panels[0].Width:=AsInteger(Width/2);
  StatusBar2.Panels[0].Width:=AsInteger(Width/2);
end;


//---------------------------------------------------------------
procedure Tdlg_Link_add.act_MakeLinkNameExecute(Sender: TObject);
//---------------------------------------------------------------
var
  sProp1, sProp2: string;
begin
  sProp1:=Fframe_Link_add1.GetPropertyName();
  sProp2:=Fframe_Link_add2.GetPropertyName();

  if (sProp1<>'') and (sProp2<>'') then
    ed_Name_.Text:= dmLink.MakeLinkName1(sProp1, sProp2);
end;



procedure Tdlg_Link_add.act_Restore_defaultExecute(Sender: TObject);
begin
  if Sender=act_Restore_default then
  begin
    row_SESR.Properties.Value        := 0.0125;
    row_KNG.Properties.Value         := 0.003;
    row_Space_limit.Properties.Value := 1;

  end;

end;



procedure Tdlg_Link_add.DoOnLinkEndChanged(Sender: TObject);
begin
  CalcReserve;
end;

// ---------------------------------------------------------------
procedure Tdlg_Link_add.row_RRV_modelEditPropertiesButtonClick( Sender: TObject; AButtonIndex: Integer);
// ---------------------------------------------------------------
var
  iLinkType_ID : Integer;
begin
  inherited;

  iLinkType_ID:=row_RRV_model.Tag;

  if cxVerticalGrid1.FocusedRow=row_RRV_model then
    if Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otLinkType, iLinkType_ID) then
      row_RRV_model.Tag:=iLinkType_ID;


end;

// ---------------------------------------------------------------
procedure Tdlg_Link_add.row_TemplateEditPropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
// ---------------------------------------------------------------
begin
  if AButtonIndex=0 then
    if cxVerticalGrid1.FocusedRow=row_Template_site then
      Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otTemplate_LINK, FTemplate_LINK_ID);

//    Dlg_Apply_template;

  if AButtonIndex=1 then
  begin
    FTemplate_LINK_ID:=0;
    row_Template_site.Properties.Value:='';
  end;


 //
end;

procedure Tdlg_Link_add.row_use_templateEditPropertiesEditValueChanged(
  Sender: TObject);
begin
  CalcReserve;

end;

end.


