unit u_ComboBox_AutoComplete;

interface
uses
  System.Classes, Winapi.Messages, Vcl.Controls, Vcl.StdCtrls, StrUtils;


type
  TComboBoxAutoComplete = class(TComboBox)
  private
    FStoredItems: TStringList;
//    procedure FilterItems_old;
    procedure StoredItemsChange(Sender: TObject);
    procedure SetStoredItems(const Value: TStringList);
    procedure CNCommand(var AMessage: TWMCommand); message CN_COMMAND;
    procedure FilterItems;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property StoredItems: TStringList read FStoredItems write SetStoredItems;
  end;


implementation

uses
  Winapi.Windows;


//------------------------------------------------------------------------
constructor TComboBoxAutoComplete.Create(AOwner: TComponent);
//------------------------------------------------------------------------
begin
  inherited;
  AutoComplete := False;
  FStoredItems := TStringList.Create;
  FStoredItems.OnChange := StoredItemsChange;

  AutoDropDown:=True;

  DropDownCount := 24;


  Left := 10;
  Top := 10;
  Text := 'Br';

  {
 

  // here's how to fill the StoredItems
  StoredItems.BeginUpdate;
  try
    StoredItems.Add('Mr John Brown');
    StoredItems.Add('Mrs Amanda Brown');
    StoredItems.Add('Mr Brian Jones');
    StoredItems.Add('Mrs Samantha Smith');
  finally
    StoredItems.EndUpdate;
  end;
  }

end;


destructor TComboBoxAutoComplete.Destroy;
begin
  FStoredItems.Free;
  inherited;
end;


procedure TComboBoxAutoComplete.CNCommand(var AMessage: TWMCommand);
begin
  // we have to process everything from our ancestor
  inherited;
  // if we received the CBN_EDITUPDATE notification
  if AMessage.NotifyCode = CBN_EDITUPDATE then
    // fill the items with the matches
    FilterItems;
end;


//------------------------------------------------------------------------
procedure TComboBoxAutoComplete.FilterItems;
//------------------------------------------------------------------------

type
  TSelection = record
    StartPos, EndPos: Integer;
  end;
var
  I: Integer;
  Selection: TSelection;
  xText: string;
begin
  // store the current combo edit selection
  SendMessage(Handle, CB_GETEDITSEL, WPARAM(@Selection.StartPos), LPARAM(@Selection.EndPos));

  // begin with the items update
  Items.BeginUpdate;
  try
    // if the combo edit is not empty, then clear the items
    // and search through the FStoredItems
    if Text <> '' then begin
      // clear all items
      Items.Clear;
      // iterate through all of them
      for I := 0 to FStoredItems.Count - 1 do begin
        // check if the current one contains the text in edit
//      if ContainsText(FStoredItems[I], Text) then
        if Pos( Text, FStoredItems[I])>0 then begin
          // and if so, then add it to the items
          Items.Add(FStoredItems[I]);
        end;
      end;
    end else begin
      // else the combo edit is empty
      // so then we'll use all what we have in the FStoredItems
      Items.Assign(FStoredItems)
    end;
  finally
    // finish the items update
    Items.EndUpdate;
  end;

  // and restore the last combo edit selection
  xText := Text;
  SendMessage(Handle, CB_SHOWDROPDOWN, Integer(True), 0);
  if (Items<>nil) and (Items.Count>0) then begin
    ItemIndex := 0;
  end else begin
    ItemIndex := -1;
  end;
  Text := xText;
  SendMessage(Handle, CB_SETEDITSEL, 0, MakeLParam(Selection.StartPos, Selection.EndPos));

end;



procedure TComboBoxAutoComplete.StoredItemsChange(Sender: TObject);
begin
  if Assigned(FStoredItems) then
    FilterItems;
end;

procedure TComboBoxAutoComplete.SetStoredItems(const Value: TStringList);
begin
  if Assigned(FStoredItems) then
    FStoredItems.Assign(Value)
  else
    FStoredItems := Value;
end;

end.



        //
//procedure TForm1.FormCreate(Sender: TObject);
//var
//  ComboBox: TComboBox;
//begin
//  // here's one combo created dynamically
//  ComboBox := TComboBox.Create(Self);
//  ComboBox.Parent := Self;
//  ComboBox.Left := 10;
//  ComboBox.Top := 10;
//  ComboBox.Text := 'Br';
//
//  // here's how to fill the StoredItems
//  ComboBox.StoredItems.BeginUpdate;
//  try
//    ComboBox.StoredItems.Add('Mr John Brown');
//    ComboBox.StoredItems.Add('Mrs Amanda Brown');
//    ComboBox.StoredItems.Add('Mr Brian Jones');
//    ComboBox.StoredItems.Add('Mrs Samantha Smith');
//  finally
//    ComboBox.StoredItems.EndUpdate;
//  end;
//
//  // and here's how to assign the Items of the combo box from the form
//  // to the StoredItems; note that if you'll use this, you have to do
//  // it before you type something into the combo's edit, because typing
//  // may filter the Items, so they would get modified
//  ComboBox1.StoredItems.Assign(ComboBox1.Items);
//end;





//------------------------------------------------------------------------
procedure TComboBoxAutoComplete.FilterItems_old;
//------------------------------------------------------------------------
var
  I: Integer;
  Selection: TSelection;
begin
  // store the current combo edit selection
  SendMessage(Handle, CB_GETEDITSEL, WPARAM(@Selection.StartPos),
    LPARAM(@Selection.EndPos));

  // DroppedDown:=True;

  // begin with the items update
  Items.BeginUpdate;
  try
    // if the combo edit is not empty, then clear the items
    // and search through the FStoredItems
    if Text <> '' then
    begin
      // clear all items
      Items.Clear;
      // iterate through all of them
      for I := 0 to FStoredItems.Count - 1 do
        // check if the current one contains the text in edit
        if ContainsText(FStoredItems[I], Text) then
          // and if so, then add it to the items
          Items.Add(FStoredItems[I]);

    //  DroppedDown:=True;

    end
    // else the combo edit is empty
    else
      // so then we'll use all what we have in the FStoredItems
      Items.Assign(FStoredItems)
  finally
    // finish the items update
    Items.EndUpdate;
  end;
  // and restore the last combo edit selection
  SendMessage(Handle, CB_SETEDITSEL, 0, MakeLParam(Selection.StartPos,
    Selection.EndPos));


 // DroppedDown:=True;
end;

