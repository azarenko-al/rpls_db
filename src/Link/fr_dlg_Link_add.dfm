object frame_Link_add: Tframe_Link_add
  Left = 985
  Top = 264
  Width = 560
  Height = 586
  Caption = 'frame_Link_add'
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -10
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 544
    Height = 121
    Caption = 'ToolBar1'
    Color = clAppWorkSpace
    ParentColor = False
    TabOrder = 0
    Visible = False
  end
  object ToolBar2: TToolBar
    Left = 0
    Top = 518
    Width = 544
    Height = 29
    Align = alBottom
    ButtonHeight = 21
    ButtonWidth = 81
    Caption = 'ToolBar2'
    ShowCaptions = True
    TabOrder = 1
    object ToolButton1: TToolButton
      Left = 0
      Top = 2
      Action = act_Add_LinkEnd
    end
  end
  object GroupBox_Prop: TGroupBox
    Left = 0
    Top = 121
    Width = 544
    Height = 96
    Align = alTop
    Caption = #1055#1083#1086#1097#1072#1076#1082#1072
    TabOrder = 2
    DesignSize = (
      544
      96)
    object b_Select: TButton
      Left = 489
      Top = 14
      Width = 49
      Height = 24
      Action = act_Select_Property
      Anchors = [akTop, akRight]
      TabOrder = 0
    end
    object Button1: TButton
      Left = 416
      Top = 13
      Width = 68
      Height = 25
      Action = act_refresh
      Anchors = [akTop, akRight]
      Default = True
      TabOrder = 1
    end
    object ComboBox1: TComboBox
      Left = 8
      Top = 16
      Width = 393
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      DropDownCount = 20
      ItemHeight = 13
      TabOrder = 2
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 499
    Width = 544
    Height = 19
    Panels = <
      item
        Width = 150
      end
      item
        Width = 50
      end>
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 217
    Width = 544
    Height = 104
    Align = alTop
    TabOrder = 4
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      PopupMenu = PopupMenu1
      NavigatorButtons.ConfirmDelete = False
      OnCustomDrawCell = cxGrid1DBTableView1CustomDrawCell
      OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
      DataController.DataSource = ds_LinkEnds
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Appending = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsSelection.CellSelect = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object col_Name: TcxGridDBColumn
        Caption = 'PPC'
        DataBinding.FieldName = 'name'
        SortIndex = 0
        SortOrder = soAscending
        Width = 160
      end
      object col_ID: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
      end
      object col_link_count: TcxGridDBColumn
        DataBinding.FieldName = 'link_count'
        Visible = False
      end
      object col_link_ID: TcxGridDBColumn
        DataBinding.FieldName = 'link_ID'
        Visible = False
        Width = 67
      end
      object col_LinkEndType_name: TcxGridDBColumn
        Caption = 'LinkEndType'
        DataBinding.FieldName = 'LinkEndType_name'
        Visible = False
        Width = 122
      end
      object col_LinkEndType_mode: TcxGridDBColumn
        Caption = 'mode'
        DataBinding.FieldName = 'LinkEndType_mode'
        Visible = False
      end
      object col_property_id: TcxGridDBColumn
        DataBinding.FieldName = 'property_id'
        Visible = False
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object DBGrid1_test: TDBGrid
    Left = 0
    Top = 415
    Width = 544
    Height = 84
    Align = alBottom
    DataSource = ds__Lookup
    TabOrder = 5
    TitleFont.Charset = RUSSIAN_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -10
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Visible = False
  end
  object ApplicationEvents1: TApplicationEvents
    OnActionUpdate = ApplicationEvents1ActionUpdate
    Left = 36
    Top = 6
  end
  object ds_LinkEnds: TDataSource
    DataSet = qry_LinkEnds
    Left = 192
    Top = 5
  end
  object ActionList1: TActionList
    OnUpdate = ApplicationEvents1ActionUpdate
    Left = 68
    Top = 6
    object act_Add_LinkEnd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1056#1056#1057
      OnExecute = act_Add_LinkEndExecute
    end
    object act_Reload: TAction
      Caption = 'act_Reload'
      OnExecute = act_Add_LinkEndExecute
    end
    object act_Select_Property: TAction
      Caption = #1042#1099#1073#1086#1088
      OnExecute = act_Add_LinkEndExecute
    end
    object act_Open_Property: TAction
      Caption = 'act_Open_Property'
    end
    object act_refresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      OnExecute = act_refreshExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 116
    Top = 6
    object actAddLinkEnd1: TMenuItem
      Action = act_Add_LinkEnd
    end
  end
  object qry_LinkEnds: TADOQuery
    LockType = ltReadOnly
    Parameters = <>
    Left = 192
    Top = 56
  end
  object qry_Prop: TADOQuery
    LockType = ltReadOnly
    Parameters = <>
    Left = 288
    Top = 8
  end
  object ds_Prop: TDataSource
    DataSet = qry_Prop
    Left = 288
    Top = 61
  end
  object ds__Lookup: TDataSource
    DataSet = ADOStoredProc_Lookup
    Left = 384
    Top = 64
  end
  object ADOStoredProc_Lookup: TADOStoredProc
    CursorType = ctStatic
    ProcedureName = '_test.__sp_property_search_top_10_SEL'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@project_id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 4
      end
      item
        Name = '@SEARCH_TEXT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 384
    Top = 8
  end
end
