unit dm_Link_tools;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms, Math,  Dialogs,


  dm_Rel_Engine,
  u_Rel_Profile,


  u_Shell_new,

  dm_Onega_DB_data,
  dm_MapEngine_store,

  dm_Main,

  I_Shell,

  u_const_str,
  u_const_db,

  u_const_msg,
  u_func_msg,

  u_Log,


  u_db,
  u_dlg,
  u_func,
  u_classes,

  u_types,

  u_Link_const,
  u_LinkLine_const,

  dm_Link

  ;

type

  TdmLink_tools = class(TDataModule)
    qry_Links: TADOQuery;
    qry_LinkEndType1: TADOQuery;
    qry_LinkEnd: TADOQuery;
    ADOStoredProc1: TADOStoredProc;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FIDList: TIDList;
    procedure UpdateShellAndMaps;

  public
  //  constructor Create(AOwner: TComponent); override;
  //  destructor Destroy; override;
    procedure Del_with_Property (aID: integer);
    procedure Del_with_LinkEnds (aID: integer);

    function DlgDelLinkLines(aID: integer; aName: string): boolean;

    procedure GetSubItemList(aID: integer; aItemList: TIDList);
    procedure Move_By_GeoRegions(aIDList: TIDList);
    function Reload_Profile(aID: integer): Integer;

  end;


function dmLink_tools: TdmLink_tools;


//===================================================================
implementation{$R *.dfm}
//==================================================================

var
  FdmLink_tools: TdmLink_tools;


// ---------------------------------------------------------------
function dmLink_tools: TdmLink_tools;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLink_tools) then
   FdmLink_tools := TdmLink_tools.Create(Application);

  Result := FdmLink_tools;
end;



procedure TdmLink_tools.DataModuleCreate(Sender: TObject);
begin
  db_SetComponentADOConnection (Self, dmMain.ADOConnection);

  FIDList := TIDList.Create();
end;


procedure TdmLink_tools.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FIDList);
end;



//--------------------------------------------------------------------
procedure TdmLink_tools.Move_By_GeoRegions(aIDList: TIDList);
//--------------------------------------------------------------------
var
  i: Integer;
  k: Integer;
begin
  CursorSQL;

  try

    for i := 0 to aIDList.Count-1 do
  //  begin

      k:=dmOnega_DB_data.ExecStoredProc_ (sp_Link_Move_to_GeoRegion_folder,
               [FLD_ID, aIDList[i].ID] );


  finally
 //
  end;

  CursorDefault;

end;


//-------------------------------------------------------------------
procedure TdmLink_tools.GetSubItemList(aID: integer; aItemList: TIDList);
//-------------------------------------------------------------------
begin
  dmOnega_DB_data.Link_GetSubItems(ADOStoredProc1, aID) ;
//  dmOnega_DB_data.Link_GetSubItems(ADOStoredProc1, aID) ;



//  db_OpenQuery (qry_Temp, 'Exec '+ sp_Link_GetSubItems + Format(' %d', [aID]));

  with ADOStoredProc1 do
    while not Eof do
    begin
      aItemList.AddObjNameAndGUID1 (fIELDvALUES[FLD_ID], fIELDvALUES[FLD_OBJNAME], fIELDvALUES[FLD_GUID]);

      Next;
    end;

end;

//--------------------------------------------------------------------
function TdmLink_tools.DlgDelLinkLines(aID: integer; aName: string): boolean;
//--------------------------------------------------------------------
(*const
  MSG  = '�� �������� "%s" ������������ � �� ������: "%s". '+
         '��� �������� ��� ����� �������. �������������?';
  MSG1 = '�� �������� "%s" ������������ � �� �����: "%s" .'+
         '��� �������� ��� ����� �������. �������������?';*)
var
  i: Integer;
  iRes: Integer;
  aIDList: TIDList;
  s: string;

  aSList: TStringList;



begin
  iRes:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc1, 'sp_Link_Dependency',
          [FLD_ID, aID]);

   if iRes=0 then
   begin
     Result := True;
     Exit;
   end;


  aSList:=TStringList.Create;


   aIDList:=TIDList.Create;

   with ADOStoredProc1 do
     while not EOF do
   begin
     s:=FieldByName(FLD_NAME).AsString;

     aSList.Add(s);

    // aIDList.Add(s);

     aIDList.AddObjName(FieldValues[FLD_ID], FieldValues[FLD_OBJNAME]);
     Next;
   end;


 //  s:=aSList.Text;

//   s:=aIDList.Text;

   if aIDList.Count>0 then
     ShowMessage(Format('��� [%s] ������������ � [%s]. �������� ����������.', [ aName,  aSList.Text] ));


   // ----------------------------
   Result := (aIDList.Count=0) ;


   FreeAndNil(aIDList);

   FreeAndNil(aSList);

 // aSList:=TStringList.Create;


//
//
//  oLineIDs:=TIDList.Create;
//  Result:= true;
//
// // dmLink.GetSubItemList (oLineIDs);
//
//  if dmLink.GetLinkLineList1 (aID, oLineIDs) then
//  begin
//    for i:=0 to oLineIDs.Count-1 do
//     // if oLineIDs[i]. then
//    begin
//      AppendStr (sLineNames, oLineIDs[i].Name);
//      if i<>oLineIDs.Count-1 then
//        AppendStr (sLineNames, ', ');
//    end;
//
//    if oLineIDs.Count = 1 then
//      sMsg:= MSG1
//    else
//      sMsg:= MSG;
//
//    Result:=ConfirmDlg(Format(sMsg, [dmLink.GetNameByID(aID), sLineNames]));
////    begin

{
      for I := 0 to oLineIDs.Count-1 do
        dmLinkLine.Del(oLineIDs[i].ID);
}
 //   end else
   // begin
//      Result:= false;
//     // oLineIDs.Free;
//     // exit;
//  //  end;
//  end;
//
//  oLineIDs.Free;
//

end;


//--------------------------------------------------------------------
procedure TdmLink_tools.UpdateShellAndMaps;
//--------------------------------------------------------------------
var
  I: Integer;
  iID: Integer;
  k: Integer;
  sGUID: string;
  sObjName: string;
begin
  if not ADOStoredProc1.Active then
    exit;
    


  FIDList.Clear;

  k:=ADOStoredProc1.RecordCount;


  with ADOStoredProc1 do
    while not EOF do
  begin
     iID      := FieldByName(FLD_ID).AsInteger;
     sObjName := FieldByName(FLD_ObjName).AsString;
     sGUID := FieldByName(FLD_GUID).AsString;

     g_Log.Add(Format('ID: %d; ObjName: %s; GUID: %s', [iID, sObjName, sGUID ]) );


     FIDList.AddObjNameAndGUID1 (iID, sObjName, sGUID);

    Next;
  end;

//  g_ShellEvents.sh


  g_ShellEvents.Shell_PostDelNodeByList (FIDList);
  g_Shell.PostDelNodeByList (FIDList);


  for I := 0 to FIDList.Count - 1 do
    dmMapEngine_store.Feature_Del (FIDList[i].ObjName, FIDList[i].ID);



//  dmMapEngine_store.Reload_Object_Layers();


 // dmMapEngine.DelList (FIDList);

 // dmAct_Map.MapRefresh;
end;


//--------------------------------------------------------------------
procedure TdmLink_tools.Del_with_Property (aID: integer);
//--------------------------------------------------------------------
var
  iRes: Integer;
begin


//  iRes:=dmOnega_DB_data.Link_del_WITH_PROPERTY (ADOStoredProc1, aID);

//  UpdateShellAndMaps();

end;


//--------------------------------------------------------------------
procedure TdmLink_tools.Del_with_LinkEnds (aID: integer);
//--------------------------------------------------------------------
var
  iRes: Integer;
begin
  iRes:=dmOnega_DB_data.Link_Del_with_LinkEnds (ADOStoredProc1, aID);
  if iRes>0 then
    UpdateShellAndMaps();
end;


//-------------------------------------------------------------------
function TdmLink_tools.Reload_Profile(aID: integer): Integer;
//-------------------------------------------------------------------
var
  b: Boolean;
  e: Double;
  s: string;
  sXML: string;

  FLinkParamRec: TdmLinkInfoRec;
  FRelProfile: TrelProfile;


begin
//  dmRel_Engine.Open ();


{
  Assert( FLinkParamRec.BLVector.Point1.b<>0,
    'Tframe_Link_profile.Reload_Profile: Boolean: FLinkParamRec.BLVector.Point1.b<>0');

  Assert( FLinkParamRec.BLVector.Point1.L<>0,
    'Tframe_Link_profile.Reload_Profile: Boolean: FLinkParamRec.BLVector.Point1.L>0');


  s:=geo_FormatBLPoint(FLinkParamRec.BLVector.Point1) + ' - ' +
     geo_FormatBLPoint(FLinkParamRec.BLVector.Point2);



  g_Log.Msg('function Tframe_Link_profile.Reload_Profile: '+ s );
 }

  if not dmLink.GetInfo1(aID, FLinkParamRec) then
    Exit;

  FrelProfile:=TrelProfile.Create;



//  e:=geo_Distance_km(FLinkParamRec.BLVector);

//  g_Log.Msg('geo_Distance_km: '+ FloatToStr(e)) ;


//  dmRel_Engine.AssignClutterModel (FLinkParamRec.ClutterMOdelID, FrelProfile.Clutters);
 // b :=
 // FrelProfile.isDirectionReversed :=False;

  b := dmRel_Engine.BuildProfile1 (FrelProfile,
       FLinkParamRec.BLVector, FLinkParamRec.PROFILE_Step_M);//, FClutterModelID);



  if b then
  begin
  //  FrelProfile.isDirectionReversed := FLinkParamRec.is_profile_reversed;
//    FrelProfile.SetDirection(FLinkParamRec.CalcDirection);

  //  FrelProfile.Property1_id:=FLinkParamRec.PropertyID1;
//    FrelProfile.Property2_id:=FLinkParamRec.PropertyID2;

  //  g_Log.Msg('FrelProfile.Count: ' + IntToStr(FrelProfile.Count)) ;


    sXML:=FrelProfile.GetXML();


////////    ShellExec_Notepad_temp (sXML  );

    dmLink.Update_profile_XML(aID, sXML);

   // g_Log.Msg(sXML) ;


//    g_Log.Msg('������� �������� � ��.') ;

//    Fframe_Profile_Rel.RelProfile_Ref:=FrelProfile;

//    Fframe_Profile_Rel.IsDirectionReversed := FLinkParamRec.is_profile_reversed;


  end else
    g_Log.Error('�� ������� ��������� �������! ��������� ������� ������ �����.') ;

//    ShowMessage('�� ������� ��������� �������!') ;


//  dmRel_Engine.Close1;

  FreeAndNil(FrelProfile);

end;



begin

end.

