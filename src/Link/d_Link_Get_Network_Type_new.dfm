object dlg_Link_Get_Network_Type_new: Tdlg_Link_Get_Network_Type_new
  Left = 1325
  Top = 631
  Width = 780
  Height = 573
  BorderWidth = 5
  Caption = 'dlg_Link_Get_Network_Type_new'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pn_Buttons: TPanel
    Left = 0
    Top = 502
    Width = 762
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 33
    Constraints.MinHeight = 33
    TabOrder = 0
    DesignSize = (
      762
      33)
    object btn_Ok: TButton
      Left = 463
      Top = 8
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object btn_Cancel: TButton
      Left = 547
      Top = 8
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
  object cxGrid2: TcxGrid
    Left = 0
    Top = 25
    Width = 762
    Height = 264
    Align = alTop
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    object cxGridDBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
    end
    object cxGridDBTableView2: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = DataSource_Main
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsView.GroupByBox = False
      object col_document_name: TcxGridDBColumn
        DataBinding.FieldName = 'document_name'
        SortIndex = 0
        SortOrder = soAscending
        Width = 253
      end
      object col_network_name: TcxGridDBColumn
        DataBinding.FieldName = 'network_name'
        SortIndex = 1
        SortOrder = soAscending
        Width = 163
      end
      object col_max_length_km: TcxGridDBColumn
        DataBinding.FieldName = 'max_length_km'
        SortIndex = 2
        SortOrder = soAscending
        Width = 97
      end
      object cxGridDBTableView2SESR: TcxGridDBColumn
        DataBinding.FieldName = 'SESR'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0.00000000'
        Width = 86
      end
      object cxGridDBTableView2Kng: TcxGridDBColumn
        DataBinding.FieldName = 'Kng'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0.00000000'
        Width = 93
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView2
    end
  end
  object rb_Not_Typed: TRadioButton
    Left = 21
    Top = 336
    Width = 113
    Height = 17
    Caption = #1053#1077#1090#1080#1087#1086#1074#1086#1077
    Checked = True
    TabOrder = 2
    TabStop = True
  end
  object GroupBox2: TGroupBox
    Left = 24
    Top = 360
    Width = 185
    Height = 89
    Caption = #1053#1077#1090#1080#1087#1086#1074#1086#1077
    TabOrder = 3
    object Label2: TLabel
      Left = 16
      Top = 52
      Width = 18
      Height = 13
      Caption = #1050#1085#1075
    end
    object Label1: TLabel
      Left = 16
      Top = 24
      Width = 21
      Height = 13
      Caption = 'Sesr'
    end
    object ed_SESR: TCurrencyEdit
      Left = 58
      Top = 23
      Width = 100
      Height = 21
      DisplayFormat = ',0.0000000'
      TabOrder = 0
      Value = 0.333000000000000000
    end
    object ed_KNG: TCurrencyEdit
      Left = 58
      Top = 52
      Width = 100
      Height = 21
      DisplayFormat = ',0.0000000'
      TabOrder = 1
      Value = 0.335530000000000000
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 762
    Height = 25
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object rb_Typed: TRadioButton
      Left = 5
      Top = 3
      Width = 113
      Height = 17
      Caption = #1090#1080#1087#1086#1074#1086#1077
      TabOrder = 0
    end
  end
  object FormStorage1: TFormStorage
    Active = False
    IniFileName = 'Software\Onega\'
    Options = []
    StoredValues = <>
    Left = 520
    Top = 348
  end
  object DataSource_Main: TDataSource
    DataSet = ADOStoredProc_Main
    Left = 352
    Top = 400
  end
  object ADOStoredProc_Main: TADOStoredProc
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega_1'
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'sp_Link_norms'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@LENGTH_KM'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 33
      end
      item
        Name = '@MODE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 352
    Top = 344
  end
end
