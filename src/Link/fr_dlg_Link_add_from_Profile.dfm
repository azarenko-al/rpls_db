object frame_Link_add_from_Profile: Tframe_Link_add_from_Profile
  Left = 1045
  Top = 119
  Width = 395
  Height = 737
  Caption = 'frame_Link_add_from_Profile'
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -10
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object cxVerticalGrid1: TcxVerticalGrid
    Left = 0
    Top = 0
    Width = 379
    Height = 496
    BorderStyle = cxcbsNone
    Align = alTop
    LookAndFeel.Kind = lfOffice11
    OptionsView.CellTextMaxLineCount = 3
    OptionsView.AutoScaleBands = False
    OptionsView.PaintStyle = psDelphi
    OptionsView.GridLineColor = clBtnShadow
    OptionsView.RowHeaderMinWidth = 30
    OptionsView.RowHeaderWidth = 180
    OptionsView.ValueWidth = 40
    Styles.OnGetContentStyle = cxVerticalGrid1StylesGetContentStyle
    TabOrder = 0
    Version = 1
    object row_Property: TcxEditorRow
      Properties.Caption = #1055#1083#1086#1097#1072#1076#1082#1072
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.ReadOnly = True
      Properties.EditProperties.OnButtonClick = row_PropEditPropertiesButtonClick
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object row_Property_pos: TcxEditorRow
      Properties.Caption = 'Property_pos'
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.ReadOnly = True
      Properties.EditProperties.OnButtonClick = row_Property_posEditPropertiesButtonClick
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object row_Property_address: TcxEditorRow
      Properties.Caption = 'Property_address'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 2
      ParentID = 0
      Index = 1
      Version = 1
    end
    object cxVerticalGrid1CategoryRow1: TcxCategoryRow
      ID = 3
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxVerticalGrid1CategoryRow2: TcxCategoryRow
      Properties.Caption = #1040#1085#1090#1077#1085#1085#1072' '#1086#1089#1085#1086#1074#1085#1072#1103
      ID = 4
      ParentID = -1
      Index = 2
      Version = 1
    end
    object row_Antenna1_Type: TcxEditorRow
      Expanded = False
      Properties.Caption = #1058#1080#1087
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.ReadOnly = True
      Properties.EditProperties.OnButtonClick = row_LinkEndType1EditPropertiesButtonClick
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 5
      ParentID = 4
      Index = 0
      Version = 1
    end
    object row_Antenna_Height: TcxEditorRow
      Expanded = False
      Properties.Caption = #1042#1099#1089#1086#1090#1072
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.EditProperties.ValueType = vtFloat
      Properties.DataBinding.ValueType = 'Float'
      Properties.Value = Null
      ID = 6
      ParentID = 4
      Index = 1
      Version = 1
    end
    object cxVerticalGrid1CategoryRow3: TcxCategoryRow
      Properties.Caption = #1055#1072#1089#1089#1080#1074#1085#1099#1081' '#1101#1083#1077#1084#1077#1085#1090
      ID = 7
      ParentID = -1
      Index = 3
      Version = 1
    end
    object row_PassiveElement_type: TcxEditorRow
      Expanded = False
      Properties.Caption = #1058#1080#1087
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end
        item
        end>
      Properties.EditProperties.ReadOnly = True
      Properties.EditProperties.OnButtonClick = row_LinkEndType1EditPropertiesButtonClick
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 8
      ParentID = 7
      Index = 0
      Version = 1
    end
    object row_LinkEnd: TcxCategoryRow
      Properties.Caption = #1056#1056#1057' ('#1087#1086#1083#1091#1082#1086#1084#1087#1083#1077#1082#1090')'
      ID = 9
      ParentID = -1
      Index = 4
      Version = 1
    end
    object row_LinkEnd_Name: TcxEditorRow
      Options.CanResized = False
      Properties.Caption = #1048#1084#1103' '#1056#1056#1057
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 10
      ParentID = 9
      Index = 0
      Version = 1
    end
    object row_Equipment_Typed: TcxEditorRow
      Properties.Caption = #1058#1080#1087#1086#1074#1086#1077
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.OnEditValueChanged = row_Equip_TypeEditPropertiesEditValueChanged
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = 'False'
      ID = 11
      ParentID = 9
      Index = 1
      Version = 1
    end
    object row_not_Typed_equipment: TcxCategoryRow
      Properties.Caption = #1053#1077#1090#1080#1087#1086#1074#1086#1077' '#1086#1073#1086#1088#1091#1076#1086#1074#1072#1085#1080#1077
      ID = 12
      ParentID = -1
      Index = 5
      Version = 1
    end
    object row_Freq_GHz: TcxEditorRow
      Expanded = False
      Properties.Caption = 'row_Freq'
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.EditProperties.ValueType = vtFloat
      Properties.DataBinding.ValueType = 'Float'
      Properties.Value = Null
      ID = 13
      ParentID = 12
      Index = 0
      Version = 1
    end
    object row_Power_dBm: TcxEditorRow
      Expanded = False
      Properties.Caption = 'row_Power'
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.EditProperties.ValueType = vtFloat
      Properties.DataBinding.ValueType = 'Float'
      Properties.Value = Null
      ID = 14
      ParentID = 12
      Index = 1
      Version = 1
    end
    object row__Loss: TcxEditorRow
      Expanded = False
      Properties.Caption = 'row_Loss'
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.EditProperties.ValueType = vtFloat
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 15
      ParentID = 12
      Index = 2
      Version = 1
    end
    object row__Raznos: TcxEditorRow
      Expanded = False
      Properties.Caption = 'row_Raznos'
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.EditProperties.ValueType = vtFloat
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 16
      ParentID = 12
      Index = 3
      Version = 1
    end
    object row__Signature_Width: TcxEditorRow
      Expanded = False
      Properties.Caption = 'row_Signature_Width'
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.EditProperties.ValueType = vtFloat
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 17
      ParentID = 12
      Index = 4
      Version = 1
    end
    object row__Signature_Height: TcxEditorRow
      Expanded = False
      Properties.Caption = 'row_Signature_Height'
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.EditProperties.ValueType = vtFloat
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 18
      ParentID = 12
      Index = 5
      Version = 1
    end
    object row_THRESHOLD_BER_3: TcxEditorRow
      Properties.Caption = 'row_THRESHOLD_BER_3'
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.EditProperties.ValueType = vtFloat
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 19
      ParentID = 12
      Index = 6
      Version = 1
    end
    object row_THRESHOLD_BER_6: TcxEditorRow
      Expanded = False
      Properties.Caption = 'row_THRESHOLD_BER_6'
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.EditProperties.ValueType = vtFloat
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 20
      ParentID = 12
      Index = 7
      Version = 1
    end
    object row_Band: TcxEditorRow
      Properties.Caption = #1044#1080#1072#1087#1072#1079#1086#1085
      Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 21
      ParentID = 12
      Index = 8
      Version = 1
    end
    object row_Typed_equipment: TcxCategoryRow
      Properties.Caption = #1058#1080#1087#1086#1074#1086#1077' '#1086#1073#1086#1088#1091#1076#1086#1074#1072#1085#1080#1077
      ID = 22
      ParentID = -1
      Index = 6
      Version = 1
    end
    object row_LinkEndType: TcxEditorRow
      Expanded = False
      Properties.Caption = #1058#1080#1087
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.ReadOnly = True
      Properties.EditProperties.OnButtonClick = row_LinkEndType1EditPropertiesButtonClick
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 23
      ParentID = 22
      Index = 0
      Version = 1
    end
    object row_mode: TcxEditorRow
      Expanded = False
      Properties.Caption = #1056#1077#1078#1080#1084
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.ReadOnly = True
      Properties.EditProperties.OnButtonClick = row_LinkEndType1EditPropertiesButtonClick
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 24
      ParentID = 22
      Index = 1
      Version = 1
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnActionUpdate = ApplicationEvents1ActionUpdate
    Left = 31
    Top = 534
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 128
    Top = 536
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clActiveBorder
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
    end
  end
end
