object frame_Link_profile_ant: Tframe_Link_profile_ant
  Left = 949
  Top = 331
  Width = 818
  Height = 540
  Caption = 'frame_Link_profile_ant'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object cxDBTreeList11: TcxDBTreeList
    Left = 0
    Top = 29
    Width = 802
    Height = 92
    Align = alTop
    Bands = <
      item
        Caption.AlignHorz = taCenter
      end>
    DataController.DataSource = DataSource1
    DataController.ParentField = 'parent_guid'
    DataController.KeyField = 'guid'
    LookAndFeel.Kind = lfFlat
    OptionsBehavior.CellHints = True
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsBehavior.AutoDragCopy = True
    OptionsBehavior.DragCollapse = False
    OptionsBehavior.ExpandOnIncSearch = True
    OptionsBehavior.ShowHourGlass = False
    OptionsCustomizing.BandCustomizing = False
    OptionsCustomizing.BandVertSizing = False
    OptionsCustomizing.ColumnVertSizing = False
    OptionsSelection.HideSelection = True
    OptionsSelection.InvertSelect = False
    OptionsView.CellTextMaxLineCount = -1
    OptionsView.ShowEditButtons = ecsbFocused
    OptionsView.Indicator = True
    OptionsView.SimpleCustomizeBox = True
    ParentColor = False
    PopupMenu = PopupMenu1
    Preview.AutoHeight = False
    Preview.MaxLineCount = 1
    RootValue = -1
    TabOrder = 1
    OnEditing = cxDBTreeList11Editing
    OnExit = cxDBTreeList11Exit
    object col__name: TcxDBTreeListColumn
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.ReadOnly = True
      Properties.ViewStyle = vsHideCursor
      Properties.OnButtonClick = col_Model_NameButtonClick
      DataBinding.FieldName = 'name'
      Options.ShowEditButtons = eisbNever
      Width = 91
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      SortOrder = soAscending
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__Model_Name: TcxDBTreeListColumn
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.ReadOnly = True
      Properties.OnButtonClick = col_Model_NameButtonClick
      Caption.Text = #1052#1086#1076#1077#1083#1100
      DataBinding.FieldName = 'EquipmentType_Name'
      Width = 103
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__azimuth: TcxDBTreeListColumn
      Tag = -1
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      Caption.AlignHorz = taCenter
      DataBinding.FieldName = 'azimuth'
      Options.Editing = False
      Options.Sorting = False
      Width = 50
      Position.ColIndex = 15
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_height: TcxDBTreeListColumn
      PropertiesClassName = 'TcxSpinEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.AssignedValues.MaxValue = True
      Properties.AssignedValues.MinValue = True
      Properties.AutoSelect = False
      Properties.ReadOnly = False
      Properties.UseCtrlIncrement = True
      Properties.ValueType = vtFloat
      Properties.OnChange = col___heightPropertiesChange
      Properties.OnEditValueChanged = col_heightPropertiesEditValueChanged
      Caption.AlignHorz = taCenter
      Caption.Text = 'H [m]'
      DataBinding.FieldName = 'height'
      Options.ShowEditButtons = eisbNever
      Options.Sorting = False
      Width = 50
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__diameter: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Caption.AlignHorz = taCenter
      Caption.Text = 'D [m]'
      DataBinding.FieldName = 'diameter'
      Options.Editing = False
      Options.ShowEditButtons = eisbNever
      Options.Sorting = False
      Width = 50
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__gain: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Caption.Text = #1050#1059' [dB]'
      DataBinding.FieldName = 'gain'
      Options.Editing = False
      Options.Sorting = False
      Width = 50
      Position.ColIndex = 4
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__tx_freq: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Caption.AlignHorz = taCenter
      Caption.Text = 'tx_freq'
      DataBinding.FieldName = 'tx_freq_MHz'
      Options.Editing = False
      Options.Sorting = False
      Width = 42
      Position.ColIndex = 5
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__rx_freq: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Caption.AlignHorz = taCenter
      Caption.Text = 'rx_freq'
      DataBinding.FieldName = 'rx_freq_MHz'
      Options.Editing = False
      Options.Sorting = False
      Width = 42
      Position.ColIndex = 6
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList2height_old: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      Caption.Text = 'H '#1085#1072#1095#1072#1083#1100#1085#1072#1103
      DataBinding.FieldName = 'height_old'
      Options.Editing = False
      Options.Sorting = False
      Width = 34
      Position.ColIndex = 14
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__type: TcxDBTreeListColumn
      Tag = -1
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Visible = False
      Caption.Text = 'Type'
      DataBinding.FieldName = 'type'
      Options.Sorting = False
      Width = 50
      Position.ColIndex = 13
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__Tilt: TcxDBTreeListColumn
      Tag = -1
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      DataBinding.FieldName = 'Tilt'
      Options.Editing = False
      Options.Sorting = False
      Width = 21
      Position.ColIndex = 12
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__IsMaster: TcxDBTreeListColumn
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.NullStyle = nssUnchecked
      Properties.ValueGrayed = ''
      Properties.OnChange = col__IsMasterPropertiesChange
      DataBinding.FieldName = 'IS_MASTER'
      Options.Sorting = False
      Width = 47
      Position.ColIndex = 7
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_ANTENNATYPE_ID: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'AntennaType_ID'
      Options.Sorting = False
      Position.ColIndex = 10
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Polarization: TcxDBTreeListColumn
      Caption.Text = 'Polarization'
      DataBinding.FieldName = 'Polarization_str'
      Options.Sorting = False
      Width = 100
      Position.ColIndex = 11
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__Power_dBm: TcxDBTreeListColumn
      DataBinding.FieldName = 'Power_dBm'
      Options.Sorting = False
      Width = 40
      Position.ColIndex = 8
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_THRESHOLD_BER_3: TcxDBTreeListColumn
      DataBinding.FieldName = 'THRESHOLD_BER_3'
      Options.Sorting = False
      Width = 50
      Position.ColIndex = 9
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Loss: TcxDBTreeListColumn
      DataBinding.FieldName = 'antenna_Loss'
      Width = 100
      Position.ColIndex = 16
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object StatusBar2111: TStatusBar
    Left = 0
    Top = 482
    Width = 802
    Height = 19
    Panels = <>
    SimplePanel = True
    Visible = False
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 802
    Height = 29
    Caption = 'ToolBar1'
    TabOrder = 2
    Visible = False
    object Button1: TButton
      Left = 0
      Top = 2
      Width = 160
      Height = 22
      Action = act_Linkend_GetBand
      TabOrder = 0
    end
  end
  object PopupMenu1: TPopupMenu
    OnPopup = PopupMenu1Popup
    Left = 204
    Top = 176
    object N1: TMenuItem
      Action = act_Refresh
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object mnu_make_antenna1: TMenuItem
      Action = act_Add_Antenna
    end
    object mnu_Antenna_Del1: TMenuItem
      Action = act_Del_Antenna
    end
    object mnu_Copy_Antenna1: TMenuItem
      Action = act_Copy_Antenna
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N5: TMenuItem
      Action = act_Grid_Setup
      Visible = False
    end
    object N3: TMenuItem
      Caption = '-'
      Visible = False
    end
    object actSelectLinkendType1: TMenuItem
      Action = act_Select_LinkendType
    end
    object actSelectLinkendTypeMode1: TMenuItem
      Action = act_Select_LinkendType_Mode
    end
    object actLinkendGetBand1: TMenuItem
      Action = act_Linkend_GetBand
    end
  end
  object ActionList1: TActionList
    Left = 272
    Top = 176
    object act_Refresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      OnExecute = act_RefreshExecute
    end
    object act_Add_Antenna: TAction
      Category = 'Antenna'
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1072#1085#1090#1077#1085#1085#1091
      OnExecute = act_RefreshExecute
    end
    object act_Del_Antenna: TAction
      Category = 'Antenna'
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1072#1085#1090#1077#1085#1085#1091
      OnExecute = act_RefreshExecute
    end
    object act_Copy_Antenna: TAction
      Category = 'Antenna'
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1085#1072' '#1074#1089#1090#1088#1077#1095#1085#1091#1102' '#1056#1056#1057
      OnExecute = act_RefreshExecute
    end
    object act_Grid_Setup: TAction
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
      OnExecute = act_RefreshExecute
    end
    object act_Select_LinkendType: TAction
      Caption = 'act_Select_LinkendType'
      OnExecute = act_RefreshExecute
    end
    object act_Select_LinkendType_Mode: TAction
      Caption = 'act_Select_LinkendType_Mode'
      OnExecute = act_RefreshExecute
    end
    object act_Linkend_GetBand: TAction
      Caption = 'act_Linkend_GetBand'
      OnExecute = act_Linkend_GetBandExecute
    end
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc1
    Left = 48
    Top = 228
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = ADOConnection1
    ProcedureName = 'sp_Link_select_item'
    Parameters = <>
    Left = 48
    Top = 176
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 368
    Top = 176
    PixelsPerInch = 96
    object cxStyle_ReadOnly: TcxStyle
      AssignedValues = [svColor]
      Color = clBtnFace
    end
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=server1'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 560
    Top = 192
  end
  object ADOStoredProc_UPD: TADOStoredProc
    Connection = ADOConnection1
    ProcedureName = 'sp_Link_select_item'
    Parameters = <>
    Left = 208
    Top = 304
  end
end
