unit fr_Link_items;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Grids, Menus,
  Dialogs,   DB, dxmdaset,   RxMemDS, cxStyles, cxInplaceContainer, cxTL, cxDBTL, cxControls,
  cxTLData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,


  dm_Main,

  dm_Onega_DB_data,

 // I_Act_Explorer, //dm_Act_Explorer,

  u_Storage,

  u_log,                                                

  f_Custom,

  I_Shell,

  I_LinkEndType,
  dm_Act_LinkEndType,
  dm_Act_Antenna,

  dm_LinkEndType,

//  dm_Antenna,
//  dm_LinkEnd,
//  dm_Link,

//  dm_Link_Items_View,

  u_cx,

  u_dlg,
  u_db,
  u_func,
 //   

  u_types,

  u_link_const,
  u_const,
  u_const_db,
  u_const_str,


   cxPropertiesStore, cxEdit, ADODB, ActnList, cxGraphics, dxBar,
  cxLookAndFeels, cxLookAndFeelPainters, cxCustomData, cxTextEdit,
  cxButtonEdit, cxCurrencyEdit, cxCheckBox, cxMaskEdit,
  cxTLdxBarBuiltInMenu, dxSkinsCore, dxSkinsDefaultPainters;

type
  Tframe_Link_items = class(TForm)
    ActionList1: TActionList;
    act_Edit: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    act_Antenna_Move: TAction;
    N2: TMenuItem;
    act_Antenna_New: TAction;
    N3: TMenuItem;
    act_Grid_Setup: TAction;
    N4: TMenuItem;
    N5: TMenuItem;
    cxDBTreeList11: TcxDBTreeList;
    col__cxDBTreeListColumn1: TcxDBTreeListColumn;
    col_name: TcxDBTreeListColumn;
    col_linkendtype_name: TcxDBTreeListColumn;
    col_power_dBm: TcxDBTreeListColumn;
    col_THRESHOLD_BER_3: TcxDBTreeListColumn;
    col_THRESHOLD_BER_6: TcxDBTreeListColumn;
    col_rx_freq: TcxDBTreeListColumn;
    col_tx_freq: TcxDBTreeListColumn;
    col__id: TcxDBTreeListColumn;
    col_address: TcxDBTreeListColumn;
    col_Pos: TcxDBTreeListColumn;
    col_LinkEnd_loss: TcxDBTreeListColumn;
    col_polarization: TcxDBTreeListColumn;
    col_Antenna_Loss: TcxDBTreeListColumn;
    col_gain: TcxDBTreeListColumn;
    col_height: TcxDBTreeListColumn;
    col_azimuth: TcxDBTreeListColumn;
    col_antennatype_name: TcxDBTreeListColumn;
    col_diameter: TcxDBTreeListColumn;
    col__tree_id: TcxDBTreeListColumn;
    col__tree_parent_id: TcxDBTreeListColumn;
    col_code: TcxDBTreeListColumn;
    col_type: TcxDBTreeListColumn;
    col_Antenna_Location_type: TcxDBTreeListColumn;
    col__IsMaster: TcxDBTreeListColumn;
    col_Band: TcxDBTreeListColumn;
    DataSource1: TDataSource;
    col_BitRate_MBps: TcxDBTreeListColumn;
    act_Select_LinkendType_Mode: TAction;
    act_Select_LinkendType: TAction;
    actSelectLinkendType1: TMenuItem;
    actSelectLinkendTypeMode1: TMenuItem;
    N6: TMenuItem;
    ADOStoredProc1: TADOStoredProc;
    act_Restore_default: TAction;
    Restoredefault1: TMenuItem;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    procedure FormCreate(Sender: TObject);
    procedure act_EditExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure ADOQuery1AfterPost(DataSet: TDataSet);
    procedure col_LinkEnd_TypeButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure cxDBTreeList11Editing(Sender: TcxCustomTreeList; AColumn: TcxTreeListColumn; var Allow: Boolean);
    procedure cxDBTreeList11Exit(Sender: TObject);
 //   procedure cxDBTreeList1InitEdit(Sender, AItem: TObject; AEdit: TcxCustomEdit);
//    procedure ds_mem_DataDataChange(Sender: TObject; Field: TField);
   // procedure dxDBTreeEditing(Sender: TObject; Node: TdxTreeListNode; var Allow: Boolean);
    procedure dxDBTreeExit(Sender: TObject);
//    procedure dxDBTreeHotTrackNode(Sender: TObject; AHotTrackInfo: TdxTreeListHotTrackInfo; var ACursor: TCursor);
    procedure col__linkendtype_namePropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
   // procedure cxDBTreeList1Exit(Sender: TObject);
  //  procedure TBItem2Click(Sender: TObject);

  private
    FDataset : TDataSet;

    FLinkID: integer;

    FRegPath : string;


    FReadOnly : Boolean;

    procedure Dlg_Select_Antenna;
    procedure Dlg_Select_LinkEndType;
    procedure Dlg_Select_LinkEndType_Mode;
    procedure UpdateEditing;
  public
    procedure SetReadOnly(Value: Boolean);
    procedure View(aID: integer = -1);
  end;


implementation
 {$R *.dfm}
 
uses
  dm_Act_Explorer;

               

//--------------------------------------------------------------------
procedure Tframe_Link_items.View(aID: integer = -1);
//--------------------------------------------------------------------
var
  i: Integer;
begin


  if aID>0 then
    FLinkID:=aID;

(*
  ADOStoredProc1.LockType := IIF(dmUser_Security.ProjectIsReadOnly, ltReadOnly, ltBatchOptimistic);
  SetFormActionsEnabled(Self, not dmUser_Security.ProjectIsReadOnly);
*)


(*  dmLink_Items_View.OpenDB (aID, mem_Data);
  dxDBTree.FullExpand;
*)

//  dmOnega_DB_data.Link_select_item1(ADOQuery1, aID, 0);
  i:=dmOnega_DB_data.Link_select_item(ADOStoredProc1, FLinkID, 0);

 // db_View(ADOStoredProc1);


//////  db_OpenQuery(ADOQuery1, Format('exec %s %d', [sp_Link_select_item, aID]));
  cxDBTreeList11.FullExpand;

 // dxDBTree.DataSource:=DataSource1;

//  cxDBTreeList1.FullExpand;

  //g_Profiler.Stop('Tframe_Link_items.View');

end;


//--------------------------------------------------------------------
procedure Tframe_Link_items.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;


  cxDBTreeList11.OnExit   :=cxDBTreeList11Exit;
  cxDBTreeList11.OnEditing:=cxDBTreeList11Editing;


//  FRegPath:=FRegPath+ '1';

  FRegPath:=g_Storage.GetPathByClass(ClassName);


  act_Restore_default.Caption:=STR_GRID_Restore_default;// '������������ �������';


 // Assert(Assigned(ILinkEndType));
//  Assert(Assigned(IAntenna));
 // Assert(Assigned(IShell));

 //zzz cxDBTreeList1.Align:=alClient;

  cxDBTreeList11.Align:=alClient;

  act_Antenna_Move.Caption:='�������� ���������� �������';
  act_Antenna_New.Caption :='�������� �������';


  act_Select_LinkendType.Caption     :='������� ������ ���';
  act_Select_LinkendType_Mode.Caption:='������� ����� ������ ���';



  // -------------------------------------------------------------------
(*
  col_Ant_POLARIZATION.FieldName:=FLD_POLARIZATION_STR;

  col_ant_Azimuth.Caption :=STR_AZIMUTH;
  col_ant_Height.Caption  :=STR_ANT_HEIGHT;
  col_ant_Diameter.Caption:=STR_ANT_DIAMETER;

  col_BitRate_MBps.Caption:='BitRate Mbps';


  col_THRESHOLD_BER_3.Caption      := 'P�� BER 10^-3 [dBm]';
  col_THRESHOLD_BER_6.Caption      := 'P�� BER 10^-6 [dBm]';
  col_THRESHOLD_BER_3.FieldName    :=FLD_THRESHOLD_BER_3;
  col_THRESHOLD_BER_6.FieldName    :=FLD_THRESHOLD_BER_6;

  col_Ant_Type.Caption    := '��� �������';
  col_LinkEnd_Type.Caption:= '��� ���';

  col_Power_dBm.Caption       := 'P��� [dBm]';

  col_rx_freq.Caption     :=STR_RX_FREQ_MHz;
  col_Tx_freq.Caption     :=STR_TX_FREQ_MHz;

  col_Name.Caption        :=STR_NAME;
  col_Pos.Caption         :=STR_COORDS;
  col_Address.Caption     :=STR_ADDRESS;

//  col_Ant_Type.Caption:= STR_ANTENNA_TYPE;
//  col_LinkEnd_Type.Caption:= STR_LINKEND_TYPE;

  col_LinkEnd_Loss.Caption:= '������ ��� [dB]';
  col_Antenna_Loss.Caption:= '������ ��� [dB]';

*)

 // col_LinkEnd_Loss.Caption:= '������ ��� [dB]';
  col_Antenna_Loss.Caption.Text := '������ ��� [dB]';
  col_Gain.Caption.Text     :='�� [dB]';


 // -------------------------------------------------------------------
//  cxDBTreeList1.RestoreFromRegistry(FRegPath + cxDBTreeList1.Name);

  col_Azimuth.Caption.Text  :=STR_AZIMUTH;
  col_Height.Caption.Text  :=STR_ANT_HEIGHT;
//  col_Height1.Caption.Text  :=STR_ANT_HEIGHT;
  col_Diameter.Caption.Text:=STR_ANT_DIAMETER;
  col_BitRate_MBps.Caption.Text:='BitRate Mbps';

  col_Antenna_Loss.DataBinding.FieldName :=FLD_Antenna_Loss;


(*  col_Lat.Caption:='������';
  col_Lon.Caption:='�������';
*)

  col_THRESHOLD_BER_3.Caption.Text          := 'P�� BER 10^-3 [dBm]';
  col_THRESHOLD_BER_6.Caption.Text          := 'P�� BER 10^-6 [dBm]';
  col_THRESHOLD_BER_3.DataBinding.FieldName :=FLD_THRESHOLD_BER_3;
  col_THRESHOLD_BER_6.DataBinding.FieldName :=FLD_THRESHOLD_BER_6;

  col_antennatype_name.Caption.Text  := '��� �������';
  col_linkendtype_name.Caption.Text  := '��� ���';

  col_power_dBm.Caption.Text       := 'P��� [dBm]';

  col_rx_freq.Caption.Text     :=STR_RX_FREQ_MHz;
  col_Tx_freq.Caption.Text     :=STR_TX_FREQ_MHz;

  col_Name.Caption.Text        :=STR_NAME;
  col_Pos.Caption.Text         :=STR_COORDS;
  col_Address.Caption.Text     :=STR_ADDRESS;

 // col__Antenna_Location_type.Caption.Text :=STR_ANTENNA_Location_type;
 // col_Antenna_Location_type.Caption       :=STR_ANTENNA_Location_type;

  //col_IsMaster1.Caption :='������� ��� ������� �� ���������';

  col_LinkEnd_Loss.Caption.Text:= '������ ��� [dB]';

  col_BitRate_MBps.Caption.Text:='BitRate Mbps';



//  col_Ant_Type.Caption:= STR_ANTENNA_TYPE;
//  col_LinkEnd_Type.Caption:= STR_LINKEND_TYPE;

{  col_LinkEnd_Loss.Caption:= '������ ��� [dB]';
  col_Antenna_Loss.Caption:= '������ ��� [dB]';


  col_Ant_Gain.Caption:='�� [dB]';
}
 // -------------------------------------------------------------------

(*
  with dxDBTree do
  begin
    Bands[1].Caption:=STR_LINKEND;
    Bands[2].Caption:=STR_ANTENNA;
//    Bands[3].Caption:=STR_PROPERTY;
    //test
    Bands[Bands.Count-1].Visible:=False;
  end;
*)

(*
  col_Pos.Width:=170;
  col_Pos.Visible:=True;
 // col_ObjName.Visible:=True;
 // col_Type.Visible:=False;*)

////////////  dmLink_Items_View.InitDB (mem_Data);

  FDataset:=ADOStoredProc1;

(*  dxDBTree.DataSource := DataSource1;


  dxDBTree.SaveToRegistry (FRegPath  + dxDBTree.Name + '_default');

  dxDBTree.LoadFromRegistry (FRegPath + dxDBTree.Name);
*)
  g_Storage.RestoreFromRegistry(cxDBTreeList11, ClassName);


//  g_Storage.RestoreFromRegistry();

 // dx_CheckColumnSizes_DBTreeList (dxDBTree);


end;


//--------------------------------------------------------------------
procedure Tframe_Link_items.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
 // dxDBTree.SaveToRegistry (FRegPath + dxDBTree.Name);
//  cxDBTreeList1.StoreToRegistry(FRegPath + cxDBTreeList1.Name);

  g_Storage.StoreToRegistry(cxDBTreeList11, ClassName);


  inherited;
end;


//--------------------------------------------------------------------
procedure Tframe_Link_items.act_EditExecute(Sender: TObject);
//--------------------------------------------------------------------
var iID: integer;
    sObjName: string;
begin
  iID:=FDataset.FieldValues[FLD_ID];


  if Sender = act_Restore_default then
  begin
  //////  dxDBTree.LoadFromRegistry (FRegPath +  dxDBTree.Name + '_default');
  end else

  //---------------------------------
  if Sender=act_Edit then begin
  //---------------------------------
    sObjName:=FDataset.FieldValues[FLD_OBJNAME];

    if Eq(sObjName, OBJ_LINKEND_ANTENNA) then
    begin
      if dmAct_Explorer.Dlg_EditObject (OBJ_LINKEND_ANTENNA, iID) then
        View ();
    end else

     //  if dmAct_Antenna.EditDlg (iID) then  View (FLinkID) else

    if Eq(sObjName, OBJ_LINKEND) then
    begin
      if dmAct_Explorer.Dlg_EditObject (OBJ_LINKEND, iID) then
        View ();
    end;
  //    if dmAct_LINKEND.EditDlg (iID) then  View (FLinkID);

  end else


  //---------------------------------
  if Sender=act_Antenna_Move then begin
  //---------------------------------
    dmAct_Antenna.Dlg_Move (iID);
  end else


  //---------------------------------
  if Sender=act_Antenna_New then begin
  //---------------------------------
   if dmAct_Antenna.Dlg_Add_LinkEnd_Antenna (iID) > 0 then
     View();
  end else

  // -------------------------------------------------------------------
  if Sender=act_Grid_Setup then
  // -------------------------------------------------------------------
  begin
   ///////// dx_Dlg_Customize_DBTreeList (dxDBTree );
  end else

  if Sender=act_Select_LinkendType then
    Dlg_Select_LinkendType
  else

  if Sender=act_Select_LinkendType_Mode then
    Dlg_Select_LinkendType_Mode
  else
    raise Exception.Create('procedure Tframe_Link_items.act_EditExecute(Sender: TObject);');


end;

//--------------------------------------------------------------------
procedure Tframe_Link_items.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//--------------------------------------------------------------------
var // b: boolean;
  sObjName: string;
  bIsLinkend: boolean;
  bAnt: Boolean;
begin
  if FReadOnly then
    Exit;                 

(*
  if not Assigned(dxDBTree.FocusedNode) then
    Exit;

  sObjName:=FDataset.FieldByName(FLD_OBJNAME).AsString;

  bIsLinkend := Eq(sObjName,OBJ_LINKEND) or
                Eq(sObjName,OBJ_PMP_SECTOR) or
                Eq(sObjName,OBJ_PMP_TERMINAL);

  act_Select_LinkendType.Enabled := bIsLinkend;
  act_Select_LinkendType_Mode.Enabled := bIsLinkend;


  bAnt :=Eq(sObjName,OBJ_LINKEND_ANTENNA) or Eq(sObjName,'ANTENNA');

  act_Edit.Enabled := bAnt or bIsLinkend;

  act_Antenna_Move.Enabled := bAnt;
  act_Antenna_New.Enabled  := bIsLinkend;


  UpdateEditing();

  *)
end;

// ---------------------------------------------------------------
procedure Tframe_Link_items.ADOQuery1AfterPost(DataSet: TDataSet);
// ---------------------------------------------------------------
begin

 (* dmOnega_DB_data.ExecProc (sp_PMP_Site_update_item,
        [
          db_Par(FLD_ID,      DataSet[FLD_ID]),
          db_Par(FLD_OBJNAME, DataSet[FLD_OBJNAME]),

          //cector
          db_Par(FLD_COLOR,     DataSet[FLD_COLOR]),
          db_Par(FLD_POWER_DBM, DataSet[FLD_POWER_DBM]),

          db_Par(FLD_CALC_RADIUS_KM, DataSet[FLD_CALC_RADIUS_KM]),
          db_Par(FLD_K0_OPEN,        DataSet[FLD_K0_OPEN]),
          db_Par(FLD_K0_CLOSED,      DataSet[FLD_K0_CLOSED]),
          db_Par(FLD_K0,             DataSet[FLD_K0]),


          //antenna
          db_Par(FLD_HEIGHT,  DataSet[FLD_HEIGHT]),
          db_Par(FLD_AZIMUTH, DataSet[FLD_AZIMUTH]),
          db_Par(FLD_TILT,    DataSet[FLD_TILT]),
          db_Par(FLD_LOSS,    DataSet[FLD_ANTENNA_LOSS])
         ]);
*)
end;


// -------------------------------------------------------------------
procedure Tframe_Link_items.col_LinkEnd_TypeButtonClick(Sender: TObject; AbsoluteIndex: Integer);
// -------------------------------------------------------------------
{var iMode,iLinkEnd1,iLinkend,iChannelType,iAntID: integer;
    iAntTypeID,iLinkendTypeID: integer;
    iCellID, iTrxID, iCellLayerID: integer;

    sObjName,sTableName, sName, sName1: string;
    }

var
  S: string;
  sObjName: string;
  bLinkEnd: boolean;
begin
  //    if dmAct_Explorer.EditObjectDlg (OBJ_ANTENNA, mem_Items[FLD_ID]) then
   //     DoChangeData();

  s:=Sender.ClassName;

  sObjName:=FDataset[FLD_OBJNAME];

  bLinkEnd:= Eq(sObjName, OBJ_LINKEND) or
             Eq(sObjName, OBJ_PMP_SECTOR) or
             Eq(sObjName, OBJ_PMP_TERMINAL);



  //--------------------------------
  if //(Sender=col_Ant_Type) or
     ((cxDBTreeList11.FocusedColumn=col_antennatype_name)) then
  //--------------------------------
    if Eq(sObjName, OBJ_LINKEND_ANTENNA) then
  begin
    Dlg_Select_Antenna();
  end;


  //--------------------------------
  if //(Sender=col_LinkEnd_Type) or
     ((cxDBTreeList11.FocusedColumn=col_linkendtype_name))then
  //--------------------------------
    if bLinkEnd then
  begin
    Dlg_Select_LinkEndType();
   // end;
  end;
end;

// ---------------------------------------------------------------
procedure Tframe_Link_items.cxDBTreeList11Editing(Sender: TcxCustomTreeList; AColumn: TcxTreeListColumn; var
    Allow: Boolean);
// ---------------------------------------------------------------
var
  sObjName: string;
  bAnt: Boolean;
  bLinkend: Boolean;
begin
  Exit;

  sObjName:=FDataset[FLD_OBJNAME];

  bLinkend:= (Eq(sObjName,OBJ_LINKEND) or
            Eq(sObjName,OBJ_PMP_SECTOR) or
            Eq(sObjName,OBJ_PMP_TERMINAL));


  col_linkendtype_name.Properties.ReadOnly:=True;
  col_antennatype_name.Properties.ReadOnly:=True;


 cx_SetColumnReadOnly (
    [
      col_linkendtype_name,
      col_antennatype_name

     ], True);



  ////////////////////////


  //col__linkendtype_name.Editing:=bLinkend;

  if bLinkend then
    col_linkendtype_name.Options.ShowEditButtons:=eisbNever
  else
    col_linkendtype_name.Options.ShowEditButtons:=eisbDefault;


(*
  col__linkendtype_name.DisableEditor:=not (Eq(sObjName,OBJ_LINKEND) or
                                       Eq(sObjName,OBJ_PMP_SECTOR) or
                                       Eq(sObjName,OBJ_PMP_TERMINAL));
*)

  bAnt :=Eq(sObjName,OBJ_LINKEND_ANTENNA);

 // col__antennatype_name.Editing  :=bAnt;

  if bAnt then
    col_antennatype_name.Options.ShowEditButtons:=eisbNever
  else
    col_antennatype_name.Options.ShowEditButtons:=eisbDefault;


 // col_IsMaster1.DisableEditor    :=not bAnt;

end;

// ---------------------------------------------------------------
procedure Tframe_Link_items.UpdateEditing;
// ---------------------------------------------------------------
var
  sObjName: string;
  bAnt: Boolean;
  bLinkend: Boolean;
begin
 Exit;

  sObjName:=FDataset[FLD_OBJNAME];

  bLinkend:= (Eq(sObjName,OBJ_LINKEND) or
              Eq(sObjName,OBJ_PMP_SECTOR) or
              Eq(sObjName,OBJ_PMP_TERMINAL));


  col_linkendtype_name.Editing:=bLinkend;

  if bLinkend then
    col_linkendtype_name.Options.ShowEditButtons:=eisbDefault
  else
    col_linkendtype_name.Options.ShowEditButtons:=eisbNever;


(*
  col__linkendtype_name.DisableEditor:=not (Eq(sObjName,OBJ_LINKEND) or
                                       Eq(sObjName,OBJ_PMP_SECTOR) or
                                       Eq(sObjName,OBJ_PMP_TERMINAL));
*)

  bAnt :=Eq(sObjName,OBJ_LINKEND_ANTENNA);

  col_antennatype_name.Editing  :=bAnt;

  if bAnt then
    col_antennatype_name.Options.ShowEditButtons:=eisbDefault
  else
    col_antennatype_name.Options.ShowEditButtons:=eisbNever;


 // col_IsMaster1.DisableEditor    :=not bAnt;

end;




procedure Tframe_Link_items.cxDBTreeList11Exit(Sender: TObject);
begin
  db_PostDataset (FDataset);
end;


// ---------------------------------------------------------------
procedure Tframe_Link_items.SetReadOnly(Value: Boolean);
// ---------------------------------------------------------------
begin
  FReadOnly := Value;

  cxDBTreeList11.OptionsData.Editing := not FReadOnly;

(*  if Value then
    dxDBTree.OptionsBehavior := dxDBTree.OptionsBehavior - [etoEditing]
  else
    dxDBTree.OptionsBehavior := dxDBTree.OptionsBehavior + [etoEditing];

*)
  ADOStoredProc1.Close;
  ADOStoredProc1.LockType := IIF(FReadOnly, ltReadOnly, ltBatchOptimistic);
  SetFormActionsEnabled(Self, not FReadOnly);

 // cxGrid1DBTableView1.OptionsData.Editing := not FReadOnly;


end;
            



//-------------------------------------------------------------------
procedure Tframe_Link_items.Dlg_Select_Antenna;
//-------------------------------------------------------------------
var
  i: Integer;
  iAntTypeID: Integer;
  sName: Widestring;
begin
  iAntTypeID:= FDataset.FieldBYName(FLD_ANTENNATYPE_ID).AsInteger ;

  if dmAct_Explorer.Dlg_Select_Object (otAntennaType, iAntTypeID, sName) then
  begin
 {   dmAntenna.Update(mem_Data[FLD_ID], [db_par(FLD_ANTENNATYPE_ID, iAntTypeID)]);
    dmAntenna.UpdateAntTypeInfo(mem_Data[FLD_ID]);
}
//    dmAntenna.ChangeAntType (FDataset[FLD_ID], iAntTypeID);


    i:=dmOnega_DB_data.Object_UPDATE_AntType(OBJ_LINKEND_ANTENNA, FDataset[FLD_ID], iAntTypeID);

    View();
  end;

end;



//-------------------------------------------------------------------
procedure Tframe_Link_items.Dlg_Select_LinkEndType;
//-------------------------------------------------------------------
var
  bLinkend: Boolean;
 // iLinkEnd1,iLinkend: integer;
  iAntTypeID,iLinkendTypeID: Integer;
  sObjName : string;
  sName: Widestring ;

  mode_rec: TdmLinkEndTypeModeInfoRec;
  iLinkendType_Mode_ID: integer;
  iLinkend: integer;
  k: Integer;
begin
  sObjName:= FDataset[FLD_OBJNAME];

  bLinkend:= (Eq(sObjName,OBJ_LINKEND) or
              Eq(sObjName,OBJ_PMP_SECTOR) or
              Eq(sObjName,OBJ_PMP_TERMINAL));


  if not bLinkend then
    exit;

  iLinkend:= FDataset[FLD_ID];

  iLinkendTypeID      := FDataset.FieldByName(FLD_LINKENDTYPE_ID).AsInteger ;
  iLinkendType_Mode_ID:= FDataset.FieldByName(FLD_LINKENDTYPE_Mode_ID).AsInteger ;


  if dmAct_Explorer.Dlg_Select_Object (otLinkendType, iLinkendTypeID, sName) then
    if dmAct_LinkEndType.Dlg_GetMode (iLinkendTypeID, iLinkendType_Mode_ID, mode_rec) then //iMode, iLinkEndType_Mode_ID,
    begin
      k:=dmOnega_DB_data.Object_update_LinkEndType
         (sObjName,
          iLinkend, iLinkendTypeID, mode_rec.ID); //mode_rec.Mode,

      if k<=0 then
      begin
         g_log.Error('dmOnega_DB_data.Object_update_LinkEndType: '+ sObjName);
         Exit;
      end;


      if Eq(sObjName,OBJ_LINKEND) then
        if ConfirmDlg('����������� ��������� ������������ ��� ��������� ���?') then
          dmOnega_DB_data.LinkEnd_UpdateNext(iLinkEnd);

      View();
    end;
end;



//-------------------------------------------------------------------
procedure Tframe_Link_items.Dlg_Select_LinkEndType_Mode;
//-------------------------------------------------------------------
var
  i: Integer;

  iLinkendTypeID: Integer;
  sName,  sObjName : string;
  mode_rec: TdmLinkEndTypeModeInfoRec;
  iLinkendType_Mode_ID: integer;
  iLinkend: integer;
begin
  iLinkendTypeID      := FDataset.FieldByName(FLD_LINKENDTYPE_ID).AsInteger ;
  iLinkendType_Mode_ID:= FDataset.FieldByName(FLD_LINKENDTYPE_Mode_ID).AsInteger ;

  iLinkend      := FDataset[FLD_ID];
  sObjName      := FDataset[FLD_OBJNAME];

// if dmAct_Explorer.Dlg_Select_Object (otLinkendType, iLinkendTypeID, sName) then
  if dmAct_LinkEndType.Dlg_GetMode (iLinkendTypeID, iLinkendType_Mode_ID, mode_rec) then //iMode, iLinkEndType_Mode_ID,
  begin
    i:=dmOnega_DB_data.Object_update_LinkEndType
           (sObjName,
            iLinkend, iLinkendTypeID, mode_rec.ID); //mode_rec.Mode,

    if Eq(sObjName,OBJ_LINKEND) then
      if ConfirmDlg('����������� ��������� ������������ ��� ��������� ���?') then
        i:=dmOnega_DB_data.LinkEnd_UpdateNext(iLinkEnd);

    View();
  end;

end;



procedure Tframe_Link_items.dxDBTreeExit(Sender: TObject);
begin
  db_PostDataset (FDataset);
end;



procedure Tframe_Link_items.col__linkendtype_namePropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  Dlg_Select_LinkEndType();
//  ShowMessage('procedure Tframe_Link_items.col__linkendtype_namePropertiesButtonClick Sender: TObject; AButtonIndex: Integer);');
end;

end.



