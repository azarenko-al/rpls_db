unit dm_Link_Summary;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Variants,
  Db, ADODB,

  dm_Main,


  u_const,
  u_const_db,
  u_const_str,
  u_link_const,

  u_link_classes,

  u_db,
  u_radio,
  u_Geo,
  u_func,

  u_types,

  u_Link_CalcRains,

  dm_LinkEnd,
  dm_Link,

  dxmdaset
  ;

type
  TdmLink_Summary = class(TDataModule)
    qry_Link: TADOQuery;
    ADOStoredProc1: TADOStoredProc;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FDBLink: TDBLink;
  public
    procedure CalcRainDop1(aLinkID: integer; aIsCalcWithAdditionalRain: boolean);
  end;

function dmLink_Summary: TdmLink_Summary;

//==================================================================
implementation {$R *.DFM}
//==================================================================


var
  FdmLink_Summary: TdmLink_Summary;


// ---------------------------------------------------------------
function dmLink_Summary: TdmLink_Summary;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLink_Summary) then
    FdmLink_Summary := TdmLink_Summary.Create(Application);

  Result := FdmLink_Summary;
end;



procedure TdmLink_Summary.DataModuleCreate(Sender: TObject);
begin
  inherited;
  db_SetComponentADOConnection (Self, dmMain.ADOConnection);

  FDBLink := TDBLink.Create();
end;


procedure TdmLink_Summary.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FDBLink);
end;


//------------------------------------------------------------------------
procedure TdmLink_Summary.CalcRainDop1(aLinkID: integer;
    aIsCalcWithAdditionalRain: boolean);
//------------------------------------------------------------------------
var
  iPolarization: Integer;
  rec_RainDop: TLinkCalcRainDop;
  rec_DopResults: TLinkCalcRainDopResults;

  dLength: double;
  iLinkEndID1, iLinkEndID2: integer;
  iStatus, iOldStatus: integer;
  sPolarization: string;

begin


  db_OpenQuery(qry_Link,
         'SELECT * FROM '+ VIEW_LINK + ' WHERE id= ' + IntToStr(aLinkID) );

  Assert(not qry_Link.IsEmpty);

  Assert(Assigned(FDBLink), 'Value not assigned');

  FDBLink.LoadFromDataset(qry_Link);


{

 Ir:=      aRainDop.rain_intense;     //������������� ������� (��/���)
 Pr:=      aRainDop.polarization;     //����������� (0-�����., 1- ����.)
 L :=      aRainDop.weakness;         //���������� ���������� (��)
 R :=      aRainDop.interval_length;  //����� ��������� (��)
 F :=      aRainDop.work_freq_GHz;        //������� ������� (���)
 U :=      aRainDop.tilt;             //������ ����� ������ ���������(����)
 bIsReffD:= aRainDop.IsCalcLength;
}


{
    //��������� ������� ������� �� ����� ��������� (���)
   // dPower:=qry_Temp.Lookup(FLD_PARAM, DEF_POWER, FLD_VALUE_);

    with qry_Temp do
    begin

      dPower      :=AsFloat (Lookup(FLD_PARAM, DEF_POWER,    FLD_VALUE_));
}

  // ------------------------------------------------------------
  // �������������� ���� �������
  // ------------------------------------------------------------
  with rec_RainDop do
  begin
    Rain_INTENSITY:= FDBLink.Rains.rain_intensity_extra;
    if (Rain_INTENSITY = 0)  then
      Rain_INTENSITY:= FDBLink.RRV.RAIN_intensity;

    Rain_INTENSITY:= qry_Link.FieldByName(FLD_RAIN_INTENSITY_extra).AsFloat;
    if (Rain_INTENSITY = 0)  then
      Rain_INTENSITY:= qry_Link.FieldByName(FLD_rain_intensity).AsFloat;

    //  rain_intense:= gl_DB.GetFieldValueByID(TBL_LINK, FLD_RAIN_RATE, aID);
    iLinkEndID1:=FDBLink.LinkEndID1;
    iLinkEndID2:=FDBLink.LinkEndID2;


    dmLink.GetLinkEndID(aLinkID, iLinkEndID1, iLinkEndID2);

//    iPolarization:=gl_DB.GetIntFieldValueByID (VIEW_LINKEND_ANTENNAS, FLD_POLARIZATION, iLinkEndID1);
    sPolarization:=gl_DB.GetStringFieldValueByID (VIEW_LINKEND_ANTENNAS, FLD_POLARIZATION_STR, iLinkEndID1);

    polarization:=IIF (Eq(sPolarization,'h'), ptH, ptV);
//    polarization:=IIF (iPolarization=0, ptH, ptV);

   // dmLink.GetLinkEndID(aLinkID, iLinkEndID1, iLinkEndID2);





    Work_freq_GHz   := FDBLink.Tx_Freq_Ghz;
    IsCalcRainLength:= FDBLink.Rains.ISCALCLENGTH;


    Work_freq_GHz    := qry_Link.FieldByName(FLD_tx_FREQ_MHZ).AsFloat/ 1000;
    IsCalcRainLength := qry_Link.FieldByName(FLD_rain_ISCALCLENGTH).AsBoolean;
  end;

//  rec_RainDop.weakness           :=FDBLink.weakness;
  rec_RainDop.rain_signal_depression_dB   :=FDBLink.rain_signal_depression_dB;

  rec_RainDop.direct_line_tilt   :=FDBLink.TILT;
  rec_RainDop.interval_length_KM :=FDBLink.Length_m / 1000;


  rec_RainDop.rain_signal_depression_dB        :=qry_Link.FieldByName(FLD_rain_signal_depression_dB).AsFloat;
//  rec_RainDop.weakness         :=qry_Link.FieldByName(FLD_weakness).AsFloat;
  rec_RainDop.direct_line_tilt   :=qry_Link.FieldByName(FLD_TILT).AsFloat;

  rec_RainDop.interval_length_KM :=qry_Link.FieldByName(FLD_length).AsFloat / 1000;


  if rec_RainDop.interval_length_KM > 0 then
    Link_CalcRains1 (rec_RainDop,
                    aIsCalcWithAdditionalRain,
                    rec_DopResults)
  else
    exit;

  //---------------------------------------------------------

  if  aIsCalcWithAdditionalRain then
  begin
    iOldStatus:=qry_Link.FieldByName(FLD_STATUS).AsInteger;

    if iOldStatus=1 then
    begin
      if (not rec_DopResults.Rain_Enabled) then
//      if (rec_DopResults.rain_fitness_str = DEF_LINK_CALCRAIN_NOT_NORM) then
        iStatus:=2
      else
        iStatus:=1;

      dmLink.Update_(aLinkID, [db_Par(FLD_STATUS, iStatus) ]);
    end;
  end;


  with rec_DopResults do
    dmLink.Update_(aLinkID,
         [
          { TODO : ! }
       //   db_Par(FLD_POLARIZATION,         IIF (iPolarization=0, 'H', 'V')),

        //  db_Par(FLD_weakness,               weakness),

          db_Par(FLD_RAIN_SIGNAL_QUALITY,         rain_fitness_str),
          db_Par(FLD_RAIN_LENGTH_km,              rain_length_km),
          db_Par(FLD_RAIN_WEAKENING_VERT,         weakness_on_vert_polarization),
          db_Par(FLD_RAIN_WEAKENING_HORZ,         weakness_on_horz_polarization),
          db_Par(FLD_RAIN_ALLOWABLE_INTENSE_VERT, allowable_intens_on_vert_polarization),
          db_Par(FLD_RAIN_ALLOWABLE_INTENSE_HORZ, allowable_intens_on_horz_polarization)
         ]);

end;


begin

end.


