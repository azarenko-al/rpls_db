object frame_Link_calc_results: Tframe_Link_calc_results
  Left = 937
  Top = 421
  Width = 769
  Height = 492
  Caption = 'frame_Link_calc_results'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object cxDBTreeList1: TcxDBTreeList
    Left = 0
    Top = 266
    Width = 761
    Height = 198
    Align = alBottom
    Bands = <
      item
        Caption.AlignHorz = taCenter
      end>
    DataController.DataSource = DataSource1
    DataController.ParentField = 'tree_parent_id'
    DataController.KeyField = 'tree_id'
    LookAndFeel.Kind = lfFlat
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsBehavior.AutoDragCopy = True
    OptionsBehavior.DragCollapse = False
    OptionsBehavior.ExpandOnIncSearch = True
    OptionsBehavior.ShowHourGlass = False
    OptionsCustomizing.BandCustomizing = False
    OptionsCustomizing.BandVertSizing = False
    OptionsCustomizing.ColumnVertSizing = False
    OptionsData.Editing = False
    OptionsSelection.CellSelect = False
    OptionsSelection.HideFocusRect = False
    OptionsSelection.InvertSelect = False
    OptionsView.CellTextMaxLineCount = -1
    OptionsView.ShowEditButtons = ecsbFocused
    ParentColor = False
    PopupMenu = PopupMenu1
    Preview.AutoHeight = False
    Preview.MaxLineCount = 2
    RootValue = -1
    Styles.OnGetContentStyle = cxDBTreeList2StylesGetContentStyle
    TabOrder = 0
    OnCollapsed = cxDBTreeList1Expanded
    OnExpanded = cxDBTreeList1Expanded
    object col_Caption1: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'caption'
      Width = 403
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Param1: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'id'
      Width = 100
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Value11: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'value'
      Width = 100
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeListColumn4: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      DataBinding.FieldName = 'is_folder'
      Position.ColIndex = 6
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeListColumn5: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      DataBinding.FieldName = 'id'
      Position.ColIndex = 5
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeListColumn6: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      DataBinding.FieldName = 'name'
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Value211: TcxDBTreeListColumn
      DataBinding.FieldName = 'value2'
      Width = 100
      Position.ColIndex = 4
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList2cxDBTreeListColumn1: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'RecId'
      Position.ColIndex = 7
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 240
    Top = 24
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clYellow
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 32
    Top = 24
    object Action11: TMenuItem
      Action = act_Collapse
    end
    object Action12: TMenuItem
      Action = act_Expand
    end
  end
  object ActionList1: TActionList
    Left = 248
    Top = 88
    object act_Collapse: TAction
      Caption = 'Action1'
      OnExecute = act_CollapseExecute
    end
    object act_Expand: TAction
      Caption = 'Action2'
      OnExecute = act_CollapseExecute
    end
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=SERVER1'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 128
    Top = 24
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'link_calc.sp_Link_calc_results_SEL'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 175
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1685
      end>
    Left = 136
    Top = 104
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc1
    Left = 136
    Top = 156
  end
end
