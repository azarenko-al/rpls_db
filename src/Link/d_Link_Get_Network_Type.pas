unit d_Link_Get_Network_Type;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rxPlacemnt, StdCtrls, ExtCtrls,

  u_Link_const,
  u_LinkLine_const,

  dm_Link_calc_SESR
  ;

type
  // -------------------------
  Tdlg_Link_Get_Network_Type_rec = record
  // -------------------------
    Index : byte;

    Length_km : Double;

    SESR      : double;
    Kng       : double;

    Esr_Norm  : double;
    Bber_Norm : Double;
    Esr_Req   : Double;
    Bber_Req  : Double;

  end;


type
  Tdlg_Link_Get_Network_Type = class(TForm)
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    FormStorage1: TFormStorage;
    ListBox1: TListBox;
    procedure FormCreate(Sender: TObject);
  private
    //function ExecDlg(aLength_KM: Double; aIndex: Integer; var aRec:
     //   Tdlg_Link_Get_Network_Type_rec): Boolean;

  public
    class function ExecDlg(aID: Integer; aLength_KM: Double; aIndex: Integer; var
        aRec: Tdlg_Link_Get_Network_Type_rec): Boolean;

  end;


implementation

{$R *.dfm}

procedure Tdlg_Link_Get_Network_Type.FormCreate(Sender: TObject);
begin
  Caption  := '��� ����';

  ListBox1.Align:=alClient;
end;

// ---------------------------------------------------------------
class function Tdlg_Link_Get_Network_Type.ExecDlg(aID: Integer; aLength_KM:
    Double; aIndex: Integer; var aRec: Tdlg_Link_Get_Network_Type_rec): Boolean;
// ---------------------------------------------------------------
var
 // ind: Integer;

  rec: TCalcEsrBberParamRec;

begin
  FillChar(aRec ,SizeOf(aRec),0);

  aRec.Index:=aIndex;

  aRec.Length_km := LINKLINE_NORM_ARR1[aIndex].Max_Length_km;
  aRec.SESR      := LINKLINE_NORM_ARR1[aIndex].SESR;
  aRec.KNG       := LINKLINE_NORM_ARR1[aIndex].KNG;


  if (aLength_KM > 0) then
  begin
     rec.ID:=aID;
     rec.ObjName  :='LINK';
     rec.Length_KM:=aLength_KM;
     rec.GST_Type :=aIndex;

     dmLink_calc_SESR.CalcEsrBber11(rec);


     aRec.Esr_Norm  := rec.Output.Esr_Norm;
     aRec.Bber_Norm := rec.Output.Bber_Norm;
     aRec.Esr_Req   := rec.Output.Esr_Required;
     aRec.Bber_Req  := rec.Output.Bber_Required;

  end;


  Result := True;

(*

   with Tdlg_Link_Get_Network_Type.Create(Application) do
   begin
     if aIndex<ListBox1.Items.Count then
       ListBox1.Selected[aIndex] := True;

     Result := ShowModal=mrOk;

     // -------------------------
     if Result then
     // -------------------------
     begin
       ind:=ListBox1.ItemIndex;

       FillChar(aRec ,SizeOf(aRec),0);

       aRec.Index:=ind;

       aRec.Length_km := LINKLINE_NORM_ARR1[ind].Length_km;
       aRec.SESR      := LINKLINE_NORM_ARR1[ind].SESR;
       aRec.KNG       := LINKLINE_NORM_ARR1[ind].KNG;


       if (aLength_KM > 0) then
       begin
         rec.ID:=aID;
         rec.ObjName:='LINK';
         rec.Length_KM:=aLength_KM;
         rec.GST_Type:=ind;

         dmLink_calc_SESR.CalcEsrBber(rec);


         aRec.Esr_Norm  := rec.Output.Esr_Norm;
         aRec.Bber_Norm := rec.Output.Bber_Norm;
         aRec.Esr_Req   := rec.Output.Esr_Required;
         aRec.Bber_Req  := rec.Output.Bber_Required;

       end;

     end;

     Free;
   end;
*)
end;

end.


(*
rec: TCalcEsrBberParamRec;


  //-------------------------------------------------------------------
  if Eq(aFieldName, FLD_GST_TYPE) then
  //-------------------------------------------------------------------
  begin
    ind:=aNewValue;

    if ind = 0 then
      Exit;

    SetFieldValue (FLD_GST_LENGTH, LINKLINE_NORM_ARR1[ind].Length_km );
    SetFieldValue (FLD_GST_SESR,   LINKLINE_NORM_ARR1[ind].SESR );
    SetFieldValue (FLD_GST_KNG,    LINKLINE_NORM_ARR1[ind].KNG );

    { TODO : optimize }
    dLengthKM:=GetFloatFieldValue (FLD_Length)/1000;

    Assert(ID>0);
 //   dLengthKM:=dmLink.GetDistance_KM(ID); ///1000;

    if (dLengthKM > 0) then
    begin
      rec.ID:=ID;
      rec.ObjName:=OBJ_LINK;
      rec.Length_KM:=dLengthKM;
      rec.GSTType:=ind;

      dmLink_calc_SESR.CalcEsrBber(rec
      //, ID, OBJ_LINK, dLengthKM, ind,
//                                  dEsrNorm, dEsrReq, dBberNorm, dBberReq
                                  );
    end;

    SetFieldValue(FLD_ESR_NORM,      rec.Output.Esr_Norm);
    SetFieldValue(FLD_BBER_NORM,     rec.Output.Bber_Norm);
    SetFieldValue(FLD_ESR_REQUIRED,  rec.Output.Esr_Req);
    SetFieldValue(FLD_BBER_REQUIRED, rec.Output.Bber_Req);
  end;

*)
