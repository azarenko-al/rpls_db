unit d_Link_climate;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, cxPropertiesStore, Mask, rxToolEdit, ExtCtrls, rxPlacemnt,
  ActnList,

  I_Inspector,

  d_Wizard,

  d_expl_Object_Select,

  //i_core,

  dm_Main,
//  I_Shell,

  u_db,
 //   
  u_reg,
  u_GEO,

  u_const_db,
  u_types,

  I_Link_climate,

  dm_Link_Climate,


  ComCtrls, Grids, ValEdit, ToolWin, cxLookAndFeels

  ;

type
  Tdlg_Link_Climate = class(Tdlg_Wizard)
    act_RowButtonClick: TAction;
    Panel1: TPanel;
    rb_BD: TRadioButton;
    rb_Map: TRadioButton;
    ComboEdit1: TComboEdit;
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure ComboEdit1ButtonClick(Sender: TObject);
//    procedure ToolButton2Click(Sender: TObject);
//    procedure row_LinkTypeButtonClick(Sender: TObject;  AbsoluteIndex: Integer);
  private
    FLinkTypeID: Integer;
    FCalc_Method_ID: Integer;
    FLinkTypeName : WideString;

    FBLPoint: TBLPoint;

    FClimateRec: TLinkClimateDataRec;

  public
    class function ExecDlg(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec;
        aLinkTypeID, aCalc_Method_ID: Integer; var aSource_from_DB: boolean):
        boolean;

// TODO: Get_GOST_data
//  procedure Get_GOST_data;

  end;

//====================================================================
implementation  {$R *.DFM}


// -------------------------------------------------------------------
class function Tdlg_Link_Climate.ExecDlg(aBLPoint: TBLPoint; var aRec:
    TLinkClimateDataRec; aLinkTypeID, aCalc_Method_ID: Integer; var
    aSource_from_DB: boolean): boolean;
// -------------------------------------------------------------------
begin
  with Tdlg_link_climate.Create(Application) do
  try
    FBLPoint    := aBLPoint;
    FLinkTypeID := aLinkTypeID;

    FCalc_Method_ID :=aCalc_Method_ID;

    if FLinkTypeID>0 then
      FLinkTypeName :=gl_DB.GetNameByID(TBL_LinkTYPE, FLinkTypeID);


   // if aCalc_Method_ID<PageControl1.PageCount then
    //  PageControl1.ActivePageIndex :=aCalc_Method_ID;


    Result:=(ShowModal=mrOk);

    if Result then
      aRec:=FClimateRec;


    rb_BD.Checked:=FLinkTypeID>0;

    ComboEdit1.Text:=FLinkTypeName;

    aSource_from_DB:=rb_BD.Checked;

//     aSource:=

  finally
    Free;
  end;
end;

//--------------------------------------------------------------------
procedure Tdlg_Link_Climate.act_OkExecute(Sender: TObject);
//--------------------------------------------------------------------
var
  rParam: TdmLinkClimateParamRec;

begin
  FillChar(rParam,SizeOf(rParam),0);


  case rb_BD.Checked of
    True:  begin
          //   rParam.DataSource:=dtFromDict;

             rParam.LinkType_ID:=FLinkTypeID;
             rParam.LinkType_Name:=FLinkTypeName;

            // dmLink_Climate.Open;
             dmLink_Climate.GetFromDict (rParam, FClimateRec);
            // dmLink_Climate.Close;


           end;

    False: begin
           //  rParam.DataSource:=dtFromMap;
             rParam.BLPoint:=FBLPoint;
             rParam.Calc_Method_Code := FCalc_Method_ID;
                                                      
             dmLink_Climate.OpenFiles;
             dmLink_Climate.GetFromMaps (rParam, FClimateRec);
             dmLink_Climate.CloseFiles;

           end;
  end;


(*  dmLink_Climate.Open;
  dmLink_Climate.Get1 (rParam, FClimateRec);
  dmLink_Climate.Close;
*)
end;


procedure Tdlg_Link_Climate.ComboEdit1ButtonClick(Sender: TObject);
begin
  if Tdlg_expl_Object_Select.ExecDlg1 (otLinkType, FLinkTypeID, FLinkTypeName) then
    ComboEdit1.Text := FLinkTypeName;
end;

//--------------------------------------------------------------------
procedure Tdlg_Link_Climate.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  Caption  := '����� ������� ���������������';
  SetActionName ('����� ������� ���������������');

  HideHeader;

//  FLinkTypeID := reg_ReadInteger(FRegPath, FLD_LinkType_ID, 0);
 // row_LinkType.Text := reg_ReadString(FRegPath, FLD_LinkType_ID, 0);

//  ComboEdit1.Text:=gl_DB.GetNameByID(TBL_LinkTYPE, FLinkTypeID);

  SetDefaultWidth;

 // PageControl1.Align:=alClient;

(*
  ValueListEditor_GOST.Strings.Clear;
  ValueListEditor_NIIR.Strings.Clear;

  ValueListEditor_GOST.Align:=alClient;
  ValueListEditor_NIIR.Align:=alClient;
*)
  //insp_Map.Enabled:=false;
end;


procedure Tdlg_Link_Climate.ActionList1Update(Action: TBasicAction; var
    Handled: Boolean);
begin
  if rb_BD.Checked then
    ComboEdit1.Color  :=clWindow
  else
    ComboEdit1.ParentColor:=True;

  act_Ok.enabled :=(not rb_BD.Checked) or
                   (rb_BD.Checked and (FLinkTypeID>0));

end;

end.


(*

procedure Tdlg_Link_Climate.ToolButton2Click(Sender: TObject);
begin
 /// Get_GOST_data();
end;*)

// TODO: Get_GOST_data
//// ---------------------------------------------------------------
//procedure Tdlg_Link_Climate.Get_GOST_data;
//// ---------------------------------------------------------------
//
//var
//b: Boolean;
//rParam: TdmLinkClimateParamRec;
//
//begin
//ValueListEditor_NIIR.Strings.Clear;
//
//FillChar(rParam,SizeOf(rParam),0);
//
//rParam.Calc_Method_Code:=2;
//rParam.BLPoint:=FBLPoint;
//
//dmLink_Climate.OpenFiles;
//b:=dmLink_Climate.GetFromMaps (rParam, FClimateRec);
//dmLink_Climate.CloseFiles;
//                             
//if b then
//begin
//// ---------------------------------------------------------------
//
//   ValueListEditor_NIIR.InsertRow(
//      '����� ������ �� ����� ���� (���.8.11) ��� ������������� ������',
//       IntToStr(FClimateRec.NIIR_1998.rain_8_11_intensity.region_number), True);
//
//   ValueListEditor_NIIR.InsertRow(
//      '����� ������ �� ����� ���� (���.8.15) ��� ��������� Q�',
//       IntToStr(FClimateRec.NIIR_1998.rain_8_15_Qd.region_number), True);
//
//// ---------------------------------------------------------------
//
//(*     ValueListEditor_NIIR.InsertRow(
//      '���������� ��������� �� ����� ���� (���.2.12 � ����.2.1)',
//       FloatToStr(FClimateRec.GOST.humidity.absolute_humidity),True);
//*)
//
////   ValueListEditor_NIIR.InsertRow('radioklimat.TAB', '',True);
//
// //  ValueListEditor_NIIR.InsertRow('', IntToStr(FClimateRec.GOST.radioklimat.region_number), True);
////   ValueListEditor_NIIR.InsertRow('', r.GOST.radioklimat.name, True);
//
//(*         ValueListEditor_NIIR.InsertRow('', FloatToStr(r.GOST.radioklimat.gradient_summer), True);
//   ValueListEditor_NIIR.InsertRow('', FloatToStr(r.GOST.radioklimat.deviation_summer), True);
//   ValueListEditor_NIIR.InsertRow('', FloatToStr(r.GOST.radioklimat.gradient_winter), True);
//   ValueListEditor_NIIR.InsertRow('', FloatToStr(r.GOST.radioklimat.deviation_winter), True);
//*)
//
//
//(*
//
//   ValueListEditor_NIIR.InsertRow(
//      '12 ������� �������� ��������� ��������������� ��������-����� �� ����� ���� (���.2.3 � ����.2.1)',
//       FloatToStr(FClimateRec.GOST.radioklimat.gradient), True);
//
//   ValueListEditor_NIIR.InsertRow(
//      '����������� ���������� ���-������ �� ����� ���� (���.2.3 � ����.2.1)',
//       FloatToStr(FClimateRec.GOST.radioklimat.gradient_deviation), True);
//*)
//
//
////       ValueListEditor_NIIR.InsertRow('������', r.GOST.radioklimat.months, True);
//
//
////   ValueListEditor_NIIR.InsertRow('', '',True);
////        ValueListEditor1.InsertRow('rain_8_11.TAB', '',True);
//
//
//(*         ValueListEditor_NIIR.InsertRow('Aq', FloatToStr(r.GOST.rain_8_15.A), True);
//   ValueListEditor_NIIR.InsertRow('Bq', FloatToStr(r.GOST.rain_8_15.B), True);
//   ValueListEditor_NIIR.InsertRow('Cq', FloatToStr(r.GOST.rain_8_15.C), True);
//*)
//
// end;
//
//end;

//---------------------------------------------------------------------



{


       ValueListEditor_NIIR.Strings.Clear;

       if v.GetByBLPoint_GOST(bl, r) then
       begin
       //  s := r.rain_8_15.Name;

         ValueListEditor_NIIR.InsertRow('', '',True);
//         ValueListEditor1.InsertRow('rain_8_15.TAB', '',True);
         ValueListEditor_NIIR.InsertRow('����� ������ �� ����� ���� (���.8.15) ��� ��������� Q�',
                                    IntToStr(r.GOST.rain_8_15_Qd.region_number), True);
//         ValueListEditor_NIIR.InsertRow('�������� ������', r.GOST.rain_8_15.Name, True);


         ValueListEditor_NIIR.InsertRow('humidity.TAB   ���������','',True);
         ValueListEditor_NIIR.InsertRow('-', FloatToStr(r.GOST.humidity.absolute_humidity),True);


         ValueListEditor_NIIR.InsertRow('radioklimat.TAB', '',True);

         ValueListEditor_NIIR.InsertRow('', IntToStr(r.GOST.radioklimat.region_number), True);
      //   ValueListEditor_NIIR.InsertRow('', r.GOST.radioklimat.name, True);

(*         ValueListEditor_NIIR.InsertRow('', FloatToStr(r.GOST.radioklimat.gradient_summer), True);
         ValueListEditor_NIIR.InsertRow('', FloatToStr(r.GOST.radioklimat.deviation_summer), True);
         ValueListEditor_NIIR.InsertRow('', FloatToStr(r.GOST.radioklimat.gradient_winter), True);
         ValueListEditor_NIIR.InsertRow('', FloatToStr(r.GOST.radioklimat.deviation_winter), True);
*)
         ValueListEditor_NIIR.InsertRow('', FloatToStr(r.GOST.radioklimat.gradient), True);
         ValueListEditor_NIIR.InsertRow('', FloatToStr(r.GOST.radioklimat.deviation), True);

  //       ValueListEditor_NIIR.InsertRow('������', r.GOST.radioklimat.months, True);


         ValueListEditor_NIIR.InsertRow('', '',True);
 //        ValueListEditor1.InsertRow('rain_8_11.TAB', '',True);
         ValueListEditor_NIIR.InsertRow('����� ������ �� ����� ���� (���.8.11) ��� ������������� ������',
                                    IntToStr(r.GOST.rain_8_11_intensity.region_number), True);



(*         ValueListEditor_NIIR.InsertRow('Aq', FloatToStr(r.GOST.rain_8_15.A), True);
         ValueListEditor_NIIR.InsertRow('Bq', FloatToStr(r.GOST.rain_8_15.B), True);
         ValueListEditor_NIIR.InsertRow('Cq', FloatToStr(r.GOST.rain_8_15.C), True);
*)

       end;