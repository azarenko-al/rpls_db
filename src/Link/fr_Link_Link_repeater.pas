unit fr_Link_Link_repeater;

interface

uses
  DB, ADODB, ActnList, DBCtrls, ExtCtrls, DBGrids, cxControls,
  Classes, Controls, Forms, Menus, StdCtrls,
  ComCtrls, cxVGrid, cxDBVGrid, cxStyles,  Dialogs,

  dm_Onega_DB_data,


  dm_Repeater,

  dm_MapEngine_store,

  dm_Link_view_data,

//  I_Act_Explorer,

  u_types,

  u_db,

 // f_Custom,

  u_func,

  u_dlg,
  
  u_const_db,
  u_const_str,

  cxInplaceContainer, Grids, Mask, ToolWin, dxmdaset, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, cxEdit, cxCurrencyEdit,
  cxButtonEdit, cxTextEdit ;




type
  Tframe_Link_Link_repeater = class(TForm)
    ToolBar1: TToolBar;
    Button1: TButton;
    ActionList1: TActionList;
    act_AntennaType_Edit: TAction;
    PopupMenu1: TPopupMenu;
    actAntennaTypeEdit1: TMenuItem;
    cxStyleRepository1: TcxStyleRepository;
    Button2: TButton;
    act_Link_repeater_Del: TAction;
    act_Link_repeater_Select: TAction;
    DBEdit1: TDBEdit;
    Panel1: TPanel;
    cxDBVerticalGrid_Antennas: TcxDBVerticalGrid;
    cxDBVerticalGrid_Antennasname: TcxDBEditorRow;
    row_AntennaType_name: TcxDBEditorRow;
    cxDBVerticalGrid_Antennaslat: TcxDBEditorRow;
    cxDBVerticalGrid_Antennaslon: TcxDBEditorRow;
    row_height: TcxDBEditorRow;
    row_azimuth: TcxDBEditorRow;
    cxDBVerticalGrid_Antennastilt: TcxDBEditorRow;
    row_loss: TcxDBEditorRow;
    cxDBVerticalGrid_Antennasband: TcxDBEditorRow;
    row_freq_MHz: TcxDBEditorRow;
    row_gain: TcxDBEditorRow;
    row_diameter: TcxDBEditorRow;
    row_polarization_str: TcxDBEditorRow;
    cxDBVerticalGrid_Antennasid: TcxDBEditorRow;
    cxDBVerticalGrid_AntennasLink_Repeater_id: TcxDBEditorRow;
    cxDBVerticalGrid_AntennasantennaType_id: TcxDBEditorRow;
    ToolBar2: TToolBar;
    DBNavigator2: TDBNavigator;
    cxStyle_Header: TcxStyle;
    q_Repeater: TADOQuery;
    ds_Repeater: TDataSource;
    row_Link_Repeater: TcxDBEditorRow;
    procedure act_Link_repeater_DelExecute(Sender: TObject);
    procedure act_Link_repeater_SelectExecute(Sender: TObject);
    procedure DBRadioGroup1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
//    procedure ComboEdit1ButtonClick(Sender: TObject);
  private
    procedure Open_Data(aID: Integer);
    { Private declarations }
  public

    procedure View;
  end;


implementation
{$R *.dfm}

uses
  dm_Act_Explorer;


// ---------------------------------------------------------------
procedure Tframe_Link_Link_repeater.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
//  if ADOConnection1.Connected then
//    ShowMessage('ADOConnection1.Connected  -   procedure Tframe_Link_Link_repeater.FormCreate(Sender: TObject);');


//  Assert(Assigned(dmLink_view_data), 'Value not assigned');

  cxDBVerticalGrid_Antennas.Align:=alClient;

  TdmLink_view_data.Init;

  DBNavigator2.DataSource := dmLink_view_data.ds_LInk_repeater_ant;;

  cxDBVerticalGrid_Antennas.DataController.DataSource:= dmLink_view_data.ds_LInk_repeater_ant;


  row_AntennaType_name.Properties.Caption := STR_ANTENNA_TYPE;
  row_Height.Properties.Caption           := STR_HEIGHT;
  row_Diameter.Properties.Caption         := STR_DIAMETER;
  row_gain.Properties.Caption             := STR_GAIN;
  row_Polarization_str.Properties.Caption := STR_POLARIZATION;

  row_azimuth.Properties.Caption := STR_azimuth;


  row_Loss.Properties.Caption := STR_LOSS;

//-------------------------
  DBEdit1.DataSource:= dmLink_view_data.ds_LInk_repeater;


end;

//---------------------------------------------------------------------------
procedure Tframe_Link_Link_repeater.act_Link_repeater_DelExecute(Sender:  TObject);
//---------------------------------------------------------------------------
begin
  if ConfirmDlg('������� ?') then
  begin
    dmLink_view_data.Set_Link_repeater(0);

    Open_Data (0);

    if dmMapEngine_store.Is_Object_Map_Exists(OBJ_LINK) then
      dmMapEngine_store.Feature_Update(OBJ_LINK, dmLink_view_data.Get_ID());

  end;

end;

// ---------------------------------------------------------------
procedure Tframe_Link_Link_repeater.act_Link_repeater_SelectExecute(Sender:
    TObject);
// ---------------------------------------------------------------
var
  iID: Integer;
  iLink_ID: Integer;
  r: Integer;
  sName: WideString;
begin
  iLink_ID:=dmLink_view_data.DBLink.id;


  if dmAct_Explorer.Dlg_Select_Object (otLinkRepeater, iID, sName) then
  begin
   // r:=dmRepeater.Check_for_Link(iID, iLink_ID);

    Open_Data (iID);

    if dmLink_view_data.Set_Link_repeater(iID) then
    begin
      if dmMapEngine_store.Is_Object_Map_Exists(OBJ_LINK) then
        dmMapEngine_store.Feature_Update(OBJ_LINK, iLink_ID);


//      dmLink_view_data.DBLink.LinkRepeater.ID := iID;
 //     dmLink_view_data.OpenData_Link_repeater(iID);
    end else
      ErrorDlg('������������ ������ �����������.');


//dmMapEngine_store.Update_Data

//        RefreshObjectThemas (vLayer, sObjName);

  end;
end;

procedure Tframe_Link_Link_repeater.DBRadioGroup1Click(Sender: TObject);
begin
  ShowMessage ('procedure Tframe_Link_Link_repeater.DBRadioGroup1Click(Sender: TObject);');
  q_Repeater.Post;
end;


// ---------------------------------------------------------------
procedure Tframe_Link_Link_repeater.View;
// ---------------------------------------------------------------
var
  iID: Integer;
begin
  iID:=dmLink_view_data.DBLink.LinkRepeater.ID;

  dmLink_view_data.OpenData_Link_repeater1(iID);

  Open_Data (iID);

//  Load_Link_repeater_FromClass();

end;


// ---------------------------------------------------------------
procedure Tframe_Link_Link_repeater.Open_Data(aID: Integer);
// ---------------------------------------------------------------
begin

  dmOnega_DB_data.OpenQuery (q_Repeater, 'select * from link_repeater where id=:id',
             [FLD_ID, aID ]);


//   db_View (q_Repeater);

  row_Link_Repeater.Visible:= q_Repeater.FieldByName(FLD_IS_ACTIVE).AsInteger=1;

end;


end.



{

procedure Tframe_Link_Link_repeater.ComboEdit1ButtonClick(Sender: TObject);
begin
//  ShowMessage('Repeater');
end;

  k:=dmOnega_DB_data.ExecStoredProc_('sp_Link_Set_link_repeater',
             [FLD_ID, DBLink.ID ,
             FLD_link_repeater_ID, aID
             ]);


