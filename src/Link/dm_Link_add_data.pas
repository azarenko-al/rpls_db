unit dm_Link_add_data;

interface

uses
  SysUtils, Classes, DB, ADODB,  Forms, Variants,


  dm_Main,

  u_db,

  u_types,

  u_const_db,
  u_const,

  dm_Onega_DB_data
  ;

type
  TdmLink_add_data = class(TDataModule)
    qry_Prop: TADOQuery;
    ds_Prop: TDataSource;
    procedure DataModuleCreate(Sender: TObject);

  private
    FItems: TStringList;
    FCHECKSUM_AGG: Integer;
 //   procedure Open;
    procedure Update_PROPERTY(aItems: TStrings);
  public
    class procedure Init;

  end;

var
  dmLink_add_data: TdmLink_add_data;

implementation


{$R *.dfm}


procedure TdmLink_add_data.DataModuleCreate(Sender: TObject);
begin
  FItems:=TStringList.Create;
end;

class procedure TdmLink_add_data.Init;
begin
  if not Assigned(dmLink_add_data) then
    Application.CreateForm(TdmLink_add_data, dmLink_add_data);
end;

{

//------------------------------------------------------------------------------
procedure TdmLink_add_data.Open;
//------------------------------------------------------------------------------
const
//  view_LinkEnd_for_Link_add = 'view_LinkEnd_for_Link_add';

  SQL_SELECT =  'SELECT id,name FROM '+ tbl_PROPERTY + ' WHERE project_id=%d';
  SQL_SELECT_CHECKSUM =  'SELECT CHECKSUM_AGG(id,name) FROM '+ tbl_PROPERTY + ' WHERE project_id=%d';

begin
  dmOnega_DB_data.OpenQuery(qry_Prop, Format(SQL_SELECT_CHECKSUM, [dmMain.ProjectID]));


  dmOnega_DB_data.OpenQuery(qry_Prop, Format(SQL_SELECT, [dmMain.ProjectID]));

//--Get the checksum value before the column value is changed.

//SELECT CHECKSUM_AGG(CAST(Quantity AS INT))
//FROM Production.ProductInventory;


end;
}

//------------------------------------------------------------------------------
procedure TdmLink_add_data.Update_PROPERTY(aItems: TStrings);
//------------------------------------------------------------------------------

var
  i,iID: Integer;
  iCheck: Integer;
  sName: string;
  V: Variant;

const
//  view_LinkEnd_for_Link_add = 'view_LinkEnd_for_Link_add';

  SQL_SELECT =  'SELECT id,name FROM '+ tbl_PROPERTY + ' WHERE project_id=%d';
  SQL_SELECT_CHECKSUM =  'SELECT CHECKSUM_AGG(CHECKSUM(id,name)) FROM '+ tbl_PROPERTY + ' WHERE project_id=%d';

//SELECT CHECKSUM_AGG (CHECKSUM (i_id, s_name))
//FROM [map_poi_item]
//--WHERE CHECKSUM(N'Bearing Ball') = cs_Pname

begin
  dmOnega_DB_data.OpenQuery(qry_Prop, Format(SQL_SELECT_CHECKSUM, [dmMain.ProjectID]));
  iCheck:=qry_Prop.Fields[0].AsInteger;

  if iCheck<>FCHECKSUM_AGG then
  begin
    FCHECKSUM_AGG:=iCheck;

    dmOnega_DB_data.OpenQuery(qry_Prop, Format(SQL_SELECT, [dmMain.ProjectID]));

   // qry_Prop.First;
    V := qry_Prop.Recordset.GetRows(-1, EmptyParam, VarArrayOf(['id','Name']));

    FItems.BeginUpdate;
    FItems.Clear;

    for I:= VarArrayLowBound(V, 2) to VarArrayHighBound(V, 2) do
    begin
      iID  :=V[0, I];
      sName:=V[1, I];

      FItems.AddObject(sName, Pointer(iID));

    end;

    FItems.EndUpdate;
  end;


  aItems.BeginUpdate;
  aItems.Assign(FItems);
  aItems.EndUpdate;




  {
  qry_Prop.First;


  with qry_Prop do
    while not EOF do
  begin
    iID  := FieldByName(FLD_ID).AsInteger;
    sName:= FieldByName(FLD_NAME).AsString;

    aItems.AddObject(sName, Pointer(iID));

    Next;
  end;

  aItems.EndUpdate;
  }

end;




end.


     {



        // ---------------------------------------------------------------
procedure Tframe_Link_add.Update_PROPERTY;
// ---------------------------------------------------------------
const
//  view_LinkEnd_for_Link_add = 'view_LinkEnd_for_Link_add';

  SQL_SELECT =
    'SELECT * FROM '+ tbl_PROPERTY + ' WHERE project_id=%d';

var
  iID: Integer;
  sName: string;
begin
  db_OpenQuery (qry_Prop, Format(SQL_SELECT, [dmMain.ProjectID]));


 // comboBox1.Beg

  LockWindowUpdate(comboBox1.Handle);

  with qry_Prop do
    while not EOF do
  begin
    iID  := FieldByName(FLD_ID).AsInteger;
    sName:= FieldByName(FLD_NAME).AsString;

    FComboBoxAutoComplete.Items.AddObject(sName, Pointer(iID));

    Next;
  end;


  LockWindowUpdate(0);


  FComboBoxAutoComplete.StoredItems.Assign(FComboBoxAutoComplete.Items);


end;


ab.Button6Click(Sender: TObject);
var
  V,v1: Variant;
  I: Integer;

  a: array of Variant;
  
  ar: Variant;
  //array = VarArrayCreate(bounds, 1, varInteger);

begin
  ar:= VarArrayCreate([0,1], varVariant);

  ar[0]:='id';
  ar[1]:='name';

  
//  arr[0]
  {
  ADOQuery1.First;

//  SetLength(a,2);
  
  v1:=VarArrayOf(a);
 // VarArrayPut(v1, 'name',[]);

  V := ADOQuery1.Recordset.GetRows(adGetRowsRest, EmptyParam, ar);
  
//  V := ADOQuery1.Recordset.GetRows(adGetRowsRest, EmptyParam, VarArrayOf(['id','Name']));

  for I:= VarArrayLowBound(V, 2) to VarArrayHighBound(V, 2) do
  begin
    Memo1.Lines.Add(V[0, I]);
    Memo1.Lines.Add(V[1, I]);

  end;
  }
    
end;


