object dlg_Link_Get_Network_Type: Tdlg_Link_Get_Network_Type
  Left = 608
  Top = 343
  BorderStyle = bsDialog
  BorderWidth = 5
  Caption = 'dlg_Link_Get_Network_Type'
  ClientHeight = 355
  ClientWidth = 343
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pn_Buttons: TPanel
    Left = 0
    Top = 322
    Width = 343
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 33
    Constraints.MinHeight = 33
    TabOrder = 0
    DesignSize = (
      343
      33)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 343
      Height = 2
      Align = alTop
      Shape = bsTopLine
    end
    object btn_Ok: TButton
      Left = 180
      Top = 8
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object btn_Cancel: TButton
      Left = 264
      Top = 8
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
  object ListBox1: TListBox
    Left = 0
    Top = 0
    Width = 343
    Height = 257
    Align = alTop
    ItemHeight = 13
    Items.Strings = (
      '- '#1053#1077#1090#1080#1087#1086#1074#1086#1081' -'
      #1052#1077#1078#1076#1091#1085#1072#1088#1086#1076#1085#1099#1081' '#1091#1095#1072#1089#1090#1086#1082' ('#1076#1086' 12.500 '#1082#1084')'
      #1052#1072#1075#1080#1089#1090#1088#1072#1083#1100#1085#1072#1103' '#1089#1077#1090#1100' ('#1076#1086' 2500 '#1082#1084')'
      #1042#1085#1091#1090#1088#1080#1079#1086#1085#1086#1074#1072#1103' '#1089#1077#1090#1100' ('#1076#1086' 600 '#1082#1084')'
      #1042#1085#1091#1090#1088#1080#1079#1086#1085#1086#1074#1072#1103' '#1089#1077#1090#1100' ('#1076#1086' 200 '#1082#1084')'
      #1042#1085#1091#1090#1088#1080#1079#1086#1085#1086#1074#1072#1103' '#1089#1077#1090#1100' ('#1076#1086' 50 '#1082#1084')'
      #1052#1077#1089#1090#1085#1072#1103' '#1089#1077#1090#1100' ('#1076#1086' 100)'
      #1057#1077#1090#1100' '#1076#1086#1089#1090#1091#1087#1072)
    TabOrder = 1
  end
  object FormStorage1: TFormStorage
    Active = False
    IniFileName = 'Software\Onega\'
    Options = []
    StoredValues = <>
    Left = 264
    Top = 12
  end
end
