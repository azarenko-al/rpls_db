unit fr_Link_params;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, rxPlacemnt, ToolWin,   RXSplit, cxControls, cxSplitter,
  Db, ADODB, cxPropertiesStore, cxPC, StdCtrls,  ActnList,

//  u_Profiler,

  u_Storage,

  dm_Main,

  dm_Link,
 // dm_ClutterModel,

  u_const_db,

  u_func,
  u_db,

  fr_Link_inspector,
  fr_ClutterModel_Items,
  fr_Link_items,

  f_Custom

  ;

type
  Tframe_Link_Params = class(Tfrm_Custom)
    pn_Inspector: TPanel;
    cxSplitter1: TcxSplitter;
    ActionList1: TActionList;
    act_Copy: TAction;
    PageControl1: TPageControl;
    ts_Antennas: TTabSheet;
    ts_Clutters: TTabSheet;
    procedure PageControl1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);

  private
    FID: integer;

    Ffrm_Link_items: Tframe_Link_items;
    Ffrm_ClutterModel_Items: Tframe_ClutterModel_Items;
    Fframe_Link_inspector: Tframe_Link_inspector;

  public
    procedure View (aID: integer);


    procedure SetReadOnly(Value: Boolean);
//    procedure SetAllowed(Value: Boolean);

    procedure DoOnClutter(Sender: TObject; aID: Integer);
    procedure Post;
  end;



implementation {$R *.DFM}



//--------------------------------------------------------------------
procedure Tframe_Link_Params.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
  pn_Inspector.Align:=alClient;

  CreateChildForm(Tframe_Link_inspector,  Fframe_Link_inspector, pn_Inspector);
  Fframe_Link_inspector.OnClutterModelChange:=DoOnClutter;

  CreateChildForm(Tframe_ClutterModel_Items,  Ffrm_ClutterModel_Items, ts_Clutters);
  Ffrm_ClutterModel_Items.SetReadOnly(True);

  CreateChildForm(Tframe_Link_items,  Ffrm_Link_items, ts_Antennas);


//  FRegPath := g_Storage

//  AddControl (GSPages1, [PROP_HEIGHT, PROP_ACTIVE_PAGE_INDEX]);
  AddComponentProp(PageControl1, 'Height');
  AddComponentProp(PageControl1, 'ActivePage');
  cxPropertiesStore.RestoreFrom;

end;

// ---------------------------------------------------------------
procedure Tframe_Link_Params.SetReadOnly(Value: Boolean);
// ---------------------------------------------------------------
begin
  Fframe_Link_inspector.SetReadOnly(Value);
  Ffrm_Link_items.SetReadOnly(Value);


//  cxGrid1DBTableView1.OptionsData.Editing := not Value;
//  FReadOnly := Value;
(*
  ADOStoredProc1.LockType := IIF(Value, ltReadOnly, ltBatchOptimistic);
  SetFormActionsEnabled(Self, not Value);
*)
//  ADOStoredProc1.LockType := IIF(dmUser_Security.ProjectIsReadOnly, ltReadOnly, ltBatchOptimistic);
//  SetFormActionsEnabled(Self, not dmUser_Security.ProjectIsReadOnly);

end;

//--------------------------------------------------------------------
procedure Tframe_Link_Params.View(aID: integer);
//--------------------------------------------------------------------
begin
  FID:=aID;

//  g_Profiler.Start('Tframe_Link_Params.View');


  Fframe_Link_inspector.View (FID);

  Assert(Assigned(PageControl1.OnChange));

//  g_Profiler.Stop('Tframe_Link_Params.View');

  PageControl1Change(nil);
end;





procedure Tframe_Link_Params.PageControl1Change(Sender: TObject);
begin
  if FID=0 then
    Exit;

  case PageControl1.ActivePageIndex of
    0: Ffrm_Link_items.View (FID);
    1: Ffrm_ClutterModel_Items.View (dmLink.GetClutterModelID(FID));
  end;

end;


procedure Tframe_Link_Params.Post;
begin
  Fframe_Link_inspector.Post;

end;



procedure Tframe_Link_Params.DoOnClutter(Sender: TObject; aID: integer);
begin
  Ffrm_ClutterModel_Items.View (aID);
end;



end.



//
//
//procedure Tframe_Link_Params.GSPages1Click(Sender: TObject);
//begin
// (* if FID=0 then
//    Exit;
//
//  case PageControl1.ActivePageIndex of
//    0: Ffrm_Link_items.View (FID);
//    1: Ffrm_ClutterModel_Items.View (dmLink.GetClutterModelID(FID));
//  end;
//*)
//end;

