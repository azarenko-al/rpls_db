object Form6: TForm6
  Left = 549
  Top = 350
  Width = 631
  Height = 425
  Caption = 'Form6'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 265
    Top = 161
    Height = 230
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 161
    Width = 265
    Height = 230
    ActivePage = p_Main
    Align = alLeft
    MultiLine = True
    TabOrder = 0
    object p_Main: TTabSheet
      Caption = 'p_Main'
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 623
    Height = 161
    Align = alTop
    Caption = 'Panel1'
    TabOrder = 1
    object Button1: TButton
      Left = 8
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 96
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Button2'
      TabOrder = 1
    end
    object Button3: TButton
      Left = 244
      Top = 4
      Width = 73
      Height = 37
      Caption = 'Button3'
      TabOrder = 2
      OnClick = Button3Click
    end
    object btnPick: TButton
      Left = 360
      Top = 8
      Width = 75
      Height = 25
      Caption = 'btnPick'
      TabOrder = 3
      OnClick = btnPickClick
    end
  end
  object pn_Browser: TPanel
    Left = 268
    Top = 161
    Width = 289
    Height = 230
    Align = alLeft
    Caption = 'pn_Browser'
    TabOrder = 2
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 520
    Top = 4
  end
end
