unit fr_Link_inspector111111;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rxPlacemnt, ActnList, StdCtrls, ExtCtrls, Db, ADODB,

  dm_Main,
  dm_Link,

  u_Link_const,
  u_LinkLine_const,

  dm_Link_Climate,
  d_Link_climate,

  fr_DBInspector,

  u_GEO,
  u_func,
  u_db,
  u_dlg,

  u_const_db,
  u_types
  ;


type
  Tframe_Link_inspector = class(TForm)
    FormPlacement1: TFormPlacement;
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    ActionList1: TActionList;
    act_Ok: TAction;
    act_Cancel: TAction;
    pn_Header: TPanel;
    qry_Temp: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);

  private
    FID: integer;
    Ffrm_DBInspector: Tframe_DBInspector;

    procedure DoOnFieldChanged (Sender: TObject; aFieldName: string; aNewValue: Variant);
    procedure DoOnButtonFieldClick (Sender: TObject; aFieldName: string; aNewValue: Variant);
  public
    procedure View (aID: integer);

    class function ExecDlg (aID: integer): boolean;

    class function CreateChildForm ( aDest: TWinControl): Tframe_Link_inspector;

  end;


//====================================================================
implementation {$R *.DFM}
//====================================================================


class function Tframe_Link_inspector.CreateChildForm ( aDest: TWinControl): Tframe_Link_inspector;
begin
  Result:=Tframe_Link_inspector.Create(nil);
  CopyControls (Result, aDest);
end;

//--------------------------------------------------------------------
class function Tframe_Link_inspector.ExecDlg (aID: integer): boolean;
//--------------------------------------------------------------------
begin
  with Tframe_Link_inspector.Create(Application) do
  try
    pn_Buttons.Visible:=True;

    View (aID);
    //Ffrm_DBInspector.Options.AutoCommit:=False;

    Result:=(ShowModal=mrOK);
    if Result then
      Ffrm_DBInspector.Post;

  finally
    Free;
  end;

end;


//--------------------------------------------------------------------
procedure Tframe_Link_inspector.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  Ffrm_DBInspector:=Tframe_DBInspector.CreateChildForm( Self);
  Ffrm_DBInspector.PrepareView (OBJ_LINK);
  Ffrm_DBInspector.OnButtonClick:=DoOnButtonFieldClick;
  Ffrm_DBInspector.OnFieldChanged:=DoOnFieldChanged;

  db_SetADOConn (qry_Temp, dmMain.ADOConnection);

  pn_Buttons.Visible:=False;
end;


procedure Tframe_Link_inspector.FormDestroy(Sender: TObject);
begin
  Ffrm_DBInspector.Free;
end;


//--------------------------------------------------------------------
procedure Tframe_Link_inspector.View (aID: integer);
//--------------------------------------------------------------------
begin
  FID:=aID;
  Ffrm_DBInspector.View (aID);
end;


//-------------------------------------------------------------------
procedure Tframe_Link_inspector.DoOnButtonFieldClick (Sender: TObject; aFieldName: string;
                                                aNewValue: Variant);
//-------------------------------------------------------------------
var i,id,ind: integer;
  rec: TdmLinkClimateConditionsRec;
  blPoint: TBLPoint;
begin
  if Eq(aFieldName, 'RRV_NAME') then
  begin
    blPoint:=dmLink.GetBLCenter (FID);

    if Tdlg_Link_climate.ExecDlg (blPoint, rec) then
      with Ffrm_DBInspector do
    begin
      SetFieldValue ('RRV_NAME',          Rec.Comment);

      SetFieldValue (FLD_Q_FACTOR,        Rec.Q_factor);
      SetFieldValue (FLD_TERRAIN_TYPE,    Rec.terrain_type);

      SetFieldValue (FLD_Climate_factor,  Rec.Climate_factor);
      SetFieldValue (FLD_Factor_B,        Rec.Factor_B);
      SetFieldValue (FLD_Factor_C,        Rec.Factor_C);
      SetFieldValue (FLD_Factor_D,        Rec.Factor_D);

      SetFieldValue (FLD_gradient_diel,   Rec.gradient_diel);
      SetFieldValue (FLD_gradient,        Rec.gradient);

      SetFieldValue (FLD_rain_rate,       Rec.rain_rate);

    end;
  end;

end;


//-------------------------------------------------------------------
procedure Tframe_Link_inspector.DoOnFieldChanged (Sender: TObject; aFieldName: string;
                                                aNewValue: Variant);
//-------------------------------------------------------------------
var i,id,ind: integer;
begin
  //-------------------------------------------------------------------
  for i:=0 to High(LINK_CONDITIONS_ARR) do
    if Eq(LINK_CONDITIONS_ARR[i], aFieldName) then begin
      Ffrm_DBInspector.SetFieldValue ('RRV_NAME', 'нетиповой');
      Exit;
    end;

  //-------------------------------------------------------------------
  if Eq(aFieldName, FLD_LinkType_ID) then begin
  //-------------------------------------------------------------------
    id:=aNewValue;
    db_OpenQuery (qry_Temp, TBL_LINK_TYPE, FLD_ID, id);

    if not qry_Temp.IsEmpty then
      for i:=0 to High(LINK_CONDITIONS_ARR) do
        Ffrm_DBInspector.SetFieldValue (LINK_CONDITIONS_ARR[i],  qry_Temp[LINK_CONDITIONS_ARR[i]]);
  end;

  //-------------------------------------------------------------------
  if Eq(aFieldName, FLD_GST_TYPE) then
  //-------------------------------------------------------------------
    with Ffrm_DBInspector do
    begin
      ind:=aNewValue;

      SetFieldValue (FLD_GST_LENGTH, LINKLINE_NORM_ARR[ind].Length );
      SetFieldValue (FLD_GST_SESR,   LINKLINE_NORM_ARR[ind].SESR );
      SetFieldValue (FLD_GST_KNG,    LINKLINE_NORM_ARR[ind].KNG );
    end;


  if Eq(aFieldName, FLD_GST_LENGTH) or
     Eq(aFieldName, FLD_GST_SESR) or
     Eq(aFieldName, FLD_GST_KNG)
  then
    Ffrm_DBInspector.SetFieldValue (FLD_GST_TYPE, 0);

end;
            

procedure Tframe_Link_inspector.act_OkExecute(Sender: TObject);
begin
//.
end;

end.

