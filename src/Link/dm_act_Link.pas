unit dm_Act_Link;

interface


uses
  Classes, Forms, Menus, ActnList,  SysUtils,

  i_Audit,

  d_progress,

  dm_User_Security,

  dm_Link_climate,

//  dm_Localization,


  u_shell_var,

  dm_Onega_DB_data,

  dm_Link_export,

  dm_Act_LinkEndType,

 // I_Act_Explorer, // dm_Act_Explorer


  dm_LinkEndType,

  dm_MapEngine_store,

  u_DataExport_run,
  u_DataExport_RPLS_XML_run,


  u_Link_run,

 // I_Act_Link,     //  dm_Act_LinkNet,

 // I_Act_LinkNet,  //  dm_Act_LinkNet,
//  I_Act_Report,   //  dm_Act_Report,
//  I_act_LinkLine, //  dm_act_LinkLine;

  I_Act_Profile,

  fr_Link_view,

  d_GroupAssign,

  u_ini_Link_report_params,

   //shared

  dm_act_Base,

  I_Object,
  I_Shell,
  u_Shell_new,

  u_func,

  u_dlg,

  

  u_Geo,
  u_classes,

  u_types,
  u_const_db,
  u_const_str,

  u_func_msg,

  d_Link_add,
  d_Link_add_from_Profile,

  d_Object_dependence,

  u_Link_report_lib,

  dm_Folder,
  dm_Link,
//  dm_Link_repot,

  dm_Link_tools,


  Dialogs

  ;

type
  TdmAct_Link = class(TdmAct_Base) //, IAct_Link_X)
    act_Add_LinkLine: TAction;
    act_Change_LinkEndType: TAction;
    act_Del_with_LinkEnds: TAction;
    act_UpdateAntennaAzimuth: TAction;
    act_Report: TAction;
    act_Create_Net: TAction;
    act_Calc: TAction;
    act_Update_Climate: TAction;
    act_Del_with_Property: TAction;
    Action2: TAction;
    act_Update_Name_by_linkends: TAction;
    act_Export_Google: TAction;
    act_Link_profile_Save_XML: TAction;
    act_Object_dependence: TAction;
    act_Export_RPLS_XML: TAction;
    act_Export_RPLS_XML_folder: TAction;
    act_Eprasys: TAction;
    Action1: TAction;
    act_Export_ACAD: TAction;
    act_Link_profile_Load_XML: TAction;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    act_Calc_Adaptive: TAction;
    act_Link_profile_Save_to_CSV: TAction;
    act_Link_profile_Load_from_CSV: TAction;
    ActionList2: TActionList;
    act_Export_MDB: TAction;
    act_Copy: TAction;
    act_Add_Repeater: TAction;
    act_Move_By_GeoRegions: TAction;
    act_Report_FreqPlan: TAction;
    act_Repeater: TAction;
    act_Audit: TAction;
    act_AntennaType_change: TAction;
    act_Update_profile: TAction;
    procedure DataModuleCreate(Sender: TObject);
  private
    FTempVector: TBLVector;
    FBLVector: TBLVector;

    procedure Action_Update_climate;

    procedure Action_Update_Name_by_linkends;
    procedure Action_Update_profile;
    function CheckActionsEnable: Boolean;
    procedure Copy;
    procedure DoAction(Sender: TObject);


    procedure Del_with_LinkEnds (aID: integer);
    procedure Del_with_Property(aID: integer);
//    procedure Dlg_Apply_template(aIDList: TIDList);


    procedure Dlg_Change_LinkEndType(Sender: TObject; aIDList: TIDList);
    procedure Dlg_Set_Repeater(aEnabled: Boolean = True);
    procedure DoOnDeleteList(Sender : TObject);
    function ViewInProjectTree11(aID: integer): TStrArray;

 //   procedure Export_ACAD(aID: integer);

 //   procedure Link_profile_Save_to_XML(aID: integer);

  protected
    function  GetViewForm  (aOwnerForm: TForm): TForm; override;
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;

    function ItemDel(aID: integer; aName: string): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;


    function ViewInProjectTree(aID: integer): TStrArray; override;

    procedure ReportSelectedItems;

    function GetExplorerPath(aID: integer; aGUID: string): string;

  public
//    procedure Calc(aID: integer; aIsSaveReportToDB: Boolean = True);
    procedure Calc(aID: integer);

    procedure Dlg_Report(aID: integer);  //; aObjectName: WideString
//    procedure Dlg_Report (aIDList: TIDList);  overload;

    procedure Add (aFolderID: integer); override;

    function AddByProfile(aVector: TBLVector; aAntenna1, aAntenna2, aFreq_GHz:
        Double; aProfileType: TProfileBuildType): Integer;

// TODO: AddByPos
////        aProfileType: string): integer;
//
//  procedure AddByPos(aBLVector: TBLVector);

    procedure AddByVector(aBLVector: TBLVector);
    procedure CalcAdaptive(aID: integer);
    procedure Calc_list(aIDList: TIDList);

    procedure ShowOnMap (aID: integer);

    class procedure Init;
//    function IsObjectSupported(aObjectName: string): Boolean; override;
  end;

var
  dmAct_Link: TdmAct_Link;

//====================================================================
// implementation
//====================================================================
implementation
 {$R *.dfm}

uses
  dm_Act_LinkNet,
  dm_Act_Explorer,
  dm_act_LinkLine,
  dm_Rel_Engine,
  dm_Main;


//--------------------------------------------------------------------
class procedure TdmAct_Link.Init;
//--------------------------------------------------------------------
begin
  Assert(not Assigned(dmAct_Link));

  Application.CreateForm(TdmAct_Link, dmAct_Link);


end;



//--------------------------------------------------------------------
procedure TdmAct_Link.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
var b: Boolean;
begin
  inherited;

  ObjectName:=OBJ_LINK;


  act_Add_LinkLine.Caption     :='������� �����';
  act_Del_with_LinkEnds.Caption:='������� � ���';
  act_Del_with_Property.Caption:='������� � ����������';

  act_Export_ACAD.Caption:='������� � ACAD';


  act_Update_profile.Caption:='�������� �������';

  act_Update_Climate.Caption:='�������� ������� ��� �� �����';

  act_Object_dependence.Caption:=S_OBJECT_DEPENDENCE;//  '�������� �����������';


  act_UpdateAntennaAzimuth.Caption:='������ ������� ������';
//  act_Change_LinkEndType.Caption:='�������� ������������ ���';

  act_Update_Name_by_linkends.Caption:='�������� �������� �� ���������';

  act_Export_RPLS_XML.Caption:='������� � RPLS XML';
  act_Export_RPLS_XML_folder.Caption:='������� � RPLS XML';

  act_Link_profile_Load_XML.Caption:='������ �� XML';
  act_Link_profile_Save_XML.Caption:='������� � XML';

  act_Link_profile_Load_from_CSV.Caption:='������ �� CSV';
  act_Link_profile_Save_to_CSV.Caption  :='������� � CSV';

  //act_Test_Report.Caption:='Test_Report: e:\link.xml';
  act_Report.Caption:='�����';
  //act_Report_Separate.Caption:='���������� �����';
  act_Calc.Caption:='������';
  act_Calc_Adaptive.Caption:='������ c ���������� ����������';

  act_Copy.Caption:= '�����������';

  act_Create_Net.Caption:= '������� �� ����';
 // OnAfterItemDel:=DoAfterItemDel;
 // OnMapRefresh:=DoOnMapRefresh;

  act_AntennaType_change.Caption:='�������� ������ �������'; // STR_ADD;

//  act_Apply_template.Caption:='act_Apply_template'; // STR_ADD;

//  act_Apply_template.Caption:='������';


  act_Add.Caption:='������� �� ��������'; // STR_ADD;

  act_Export_MDB.Caption:=DEF_STR_Export_MDB;


  act_Move_By_GeoRegions.Caption :='������������ �� �����������';

  act_Report_FreqPlan.Caption :='�����: ��������� ������';

  act_Change_LinkEndType.Caption:='������� ������������';

  act_Repeater.Caption:='���������� ��������� ������������';

  //  act_Add_Item.Caption :='�������� �� ��������';

   act_Audit.Caption:=STR_Audit;

 // act_Audit.Enabled := False;

   OnDeleteList:=DoOnDeleteList;



  SetActionsExecuteProc
      ([
   //     act_Apply_template,

        act_Calc,
        act_Calc_Adaptive,
        act_Del_with_LinkEnds,

        act_Update_Climate,
        act_Update_profile,

        act_Export_RPLS_XML,
        act_Export_MDB,
        act_Export_RPLS_XML_folder,
        act_Export_Google,
        act_Export_ACAD,

        act_Copy,

        act_Del_with_Property,
        act_Add_LinkLine,
        act_Object_ShowOnMap,

        act_UpdateAntennaAzimuth,
        act_Update_Name_by_linkends,

        act_Report,
        act_Create_Net,

        act_Link_profile_Load_XML,
        act_Link_profile_Save_XML,

        act_Link_profile_Load_from_CSV,
        act_Link_profile_Save_to_CSV,

        act_AntennaType_change,

      // act_Report_Separate,

        act_Object_dependence,

        act_Audit,

        act_Change_LinkEndType,

        act_GroupAssign,

        act_Report_FreqPlan,

        act_Move_By_GeoRegions,

        act_Repeater

       ], DoAction);


  SetCheckedActionsArr([
        act_Add,
        act_Del_,
        act_Del_list,

        act_UpdateAntennaAzimuth,

        act_Copy,
        act_Update_Name_by_linkends,

        act_Calc,
        act_Calc_Adaptive,
        act_Del_with_LinkEnds,

        act_Change_LinkEndType,

        act_Del_with_Property,
        act_Add_LinkLine,
        act_Create_Net,

        act_Link_profile_Load_XML,
        act_Link_profile_Load_from_CSV,

        act_Move_By_GeoRegions,

        act_GroupAssign,

        act_Repeater
     ]);



//  dmLocalization.Load_DataModule (Self);

end;



 // ---------------------------------------------------------------
procedure TdmAct_Link.Action_Update_profile;
// ---------------------------------------------------------------
var
  i: Integer;
begin
  CursorHourGlass;

  dmRel_Engine.Open ();

  for i:=0 to FSelectedIDList.Count-1 do
    dmLink_tools.Reload_Profile (FSelectedIDList[i].ID);


  dmRel_Engine.Close1 ();

  CursorDefault;


  //(aID: integer): Integer;

{
  dmLink_climate.OpenFiles;

  for i:=0 to FSelectedIDList.Count-1 do
    dmLink_climate.UpdateByMap (FSelectedIDList[i].ID);

  dmLink_climate.CloseFiles;
 }
end;



 // ---------------------------------------------------------------
procedure TdmAct_Link.Action_Update_climate;
// ---------------------------------------------------------------
var
  bTerminated: Boolean;
  i: Integer;
begin
  Progress_Show('���������....');


  dmLink_climate.OpenFiles;

  for i:=0 to FSelectedIDList.Count-1 do
  begin
    dmLink_climate.UpdateByMap (FSelectedIDList[i].ID);

    Progress_SetProgress1(i, FSelectedIDList.Count, bTerminated);
  end;

  dmLink_climate.CloseFiles;

  Progress_Free;

end;

// ---------------------------------------------------------------
procedure TdmAct_Link.Action_Update_Name_by_linkends;
// ---------------------------------------------------------------
var
  oIDList: TIDList;
  i: integer;
begin
  oIDList := TIDList.Create();


  for i:=0 to FSelectedIDList.Count-1 do
  begin
    dmLink.UpdateName_Property1_to_Property2_ (FSelectedIDList[i].ID, oIDList);

  //  g_ShellEvents.Shell_RENAME_ITEM(oIDList[i].GUID, oIDList[i].Name);
  end;


  for I := 0 to oIDList.Count - 1 do
  begin
    g_ShellEvents.Shell_RENAME_ITEM(oIDList[i].GUID, oIDList[i].Name);
//    g_Shell.      Shell_RENAME_ITEM(oIDList[i].GUID, oIDList[i].Name);

  end;


{
      PostEvent (WE_EXPLORER_RENAME_ITEM,
         [app_Par(PAR_GUID, oIDList[i].GUID),
          app_Par(PAR_NAME, oIDList[i].Name) ]);
}

{
    PostEvent (WE_EXPLORER_RENAME_ITEM,
       [app_Par(PAR_GUID, sQUID1),
        app_Par(PAR_NAME, sLinkEndID1) ]);

    PostEvent (WE_EXPLORER_RENAME_ITEM,
       [app_Par(PAR_GUID, sQUID2),
        app_Par(PAR_NAME, sLinkEndID2) ]);

    PostEvent (WE_EXPLORER_RENAME_ITEM,
       [app_Par(PAR_GUID, sQUID3),
        app_Par(PAR_NAME, sLinkName) ]);
}

  oIDList.Free;

end;
        

//--------------------------------------------------------------------
procedure TdmAct_Link.Del_with_Property(aID: integer);
//--------------------------------------------------------------------
begin
  if ConfirmDlg (STR_ASK_DEL) then
    dmLink_tools.Del_with_Property (aID);
end;


// ---------------------------------------------------------------
procedure TdmAct_Link.Copy;
// ---------------------------------------------------------------
var
  iFolder_ID: Integer;
  iID: integer;
  sFolderGUID: string;
  sNewName: string;
begin
   if dmLink.HasLinkRepeater (FFocusedID) then
   begin
     ErrorDlg ('��� � ��������� �������������� �� �����������.');
     exit;
   end;


  sNewName:=FFocusedName +' (�����)';

  if InputQuery ('����� ������','������� ����� ��������', sNewName) then
  begin
    iID:=dmOnega_DB_data.Link_Copy(FFocusedID, sNewName);

    if iID>0 then
    begin
      dmMapEngine_store.Feature_Add(OBJ_LINK, iID);

      g_Shell.UpdateNodeChildren_ByGUID(GUID_Link);
    end;


    iFolder_ID:=dmLink.GetFolderID(iID);

  //  if aFolderID=0 then
    if iFolder_ID>0 then
      sFolderGUID:=dmFolder.GetGUIDByID(iFolder_ID)
    else
      sFolderGUID:=g_Obj.ItemByName[OBJ_LINK].RootFolderGUID;
   // else
    //  sFolderGUID:=dmFolder.GetGUIDByID(aFolderID);

    g_Shell.UpdateNodeChildren_ByGUID (sFolderGUID);

  //  g_ShellEvents.Shell_EXPAND_BY_GUID(sFolderGUID);
    g_Shell.EXPAND_BY_GUID(sFolderGUID);

    g_Shell.FocuseNodeByObjName (OBJ_LINK, iID);



//      dmAntType.Copy (FFocusedID, sNewName);

//      sGUIDg_Obj.ItemByName[OBJ_ANTENNA_TYPE].RootFolderGUID;


  end;
end;


//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_Link.DoAction (Sender: TObject);
//--------------------------------------------------------------------
var
  i: integer;
  iAntennaTypeID: Integer;
  sTempFile: string;
  s: string;
  sFile: string;
  sName: WideString;

begin
  //---------------------------------------
  // MAP
  //---------------------------------------
  if Sender=act_Update_Climate then
  //---------------------------------------
    Action_Update_climate()
  else

  if sender=act_Update_profile then
    Action_Update_profile()
  else

// Action_Update_climate


  if Sender=act_Audit then
    Init_IAuditX.ExecDlg( dmMain.GetConnectionString, TBL_LINK, FFocusedID) else


  //---------------------------------------
  if Sender=act_Object_ShowOnMap then
  //---------------------------------------
    ShowOnMap (FFocusedID)
  else


  if Sender=act_Change_LinkEndType then
    Dlg_Change_LinkEndType(Sender, FSelectedIDList) else

  if Sender=act_Repeater then
   Dlg_Set_Repeater else

{  if Sender=act_Audit then
    dlg_Audit (TBL_Link, FFocusedID)
  else
}
  // -------------------------
  if Sender=act_Move_By_GeoRegions then
  // -------------------------
  begin
    dmLink_tools.Move_By_GeoRegions(FSelectedIDList);

    if Assigned(IMainProjectExplorer) then
    begin
      IMainProjectExplorer.UPDATE_NODE_CHILDREN(GUID_LINK);
      IMainProjectExplorer.ExpandByGUID(GUID_LINK);
    end;
  end else

  if Sender=act_Report_FreqPlan then
  begin
    TLink_run.Freq_Report(FSelectedIDList);
  end else


  // ---------------------------------------------------------------
  if Sender=act_Copy then
  // ---------------------------------------------------------------
  begin
    Copy();
  end else

  //------------------------------------------------------
  if Sender=act_Export_RPLS_XML then
//    dmProperty_export.ExportToRPLS_XML ('');//, FSelectedIDList);
    TDataExport_RPLS_XML_run.Export_Link_selected(FSelectedIDList) else

  //------------------------------ ------------------------
  if Sender=act_Export_RPLS_XML_folder then
//    dmProperty_export.ExportToRPLS_XML ('');//, FSelectedIDList);
    TDataExport_RPLS_XML_run.Export_Link_selected (nil, FFocusedID) else
//    dmLink_export.Dlg_ExportFolderToRPLS_XML (FFocusedID) else


(*
   dmLink_export.Dlg_ExportToRPLS_XML (FSelectedIDList);

  //------------------------------------------------------
  if Sender=act_Export_RPLS_XML then
//    dmProperty_export.ExportToRPLS_XML ('');//, FSelectedIDList);
    TDataExport_RPLS_XML_run.Export_Property_selected(FSelectedIDList) else
//    dmProperty_export.Dlg_ExportToRPLS_XML (FSelectedIDList);

*)

  if Sender=act_Export_MDB then
    TDataExport_run.ExportToRplsDb_selected (TBL_LINK, FSelectedIDList)
  else


  if Sender = act_Object_dependence then
    Tdlg_Object_dependence.ExecDlg(OBJ_LINK, FFocusedName, FFocusedID)
  else

 //---------------------------------------
  if Sender=act_Export_Google then
    TLink_run.Export_Google(FSelectedIDList)
  else

//    Action_Export_Google() ELSE
  //---------------------------------------

  if Sender=act_Export_ACAD then
    TLink_run.export_ACAD(FFocusedID)  else
     //Export_ACAD(FFocusedID);


  if Sender=act_GroupAssign then
    Tdlg_GroupAssign.ExecDlg (OBJ_Link, FSelectedIDList) else

 //---------------------------------------
  if Sender=act_Update_Name_by_linkends then
  //---------------------------------------
    Action_Update_Name_by_linkends() else

  //---------------------------------------
  if Sender=act_Del_with_LinkEnds then
    Del_with_LinkEnds (FFocusedID) else

  //---------------------------------------
  if Sender=act_Del_with_Property then
    Del_with_Property (FFocusedID) else





{  if Sender=act_Change_LinkEndType then
    ShowOnMap (FFocusedID);
}

 // .Caption:='�������: ������ �� CSV';
  //.Caption  :='�������: ��������� � CSV';

  if Sender=act_Link_profile_Save_to_CSV then
  begin
    dmLink_export.Link_profile_Save_to_CSV(FFocusedID);
  end else

  if Sender=act_Link_profile_Load_from_CSV then
  begin
    dmLink_export.Link_profile_Load_from_CSV(FFocusedID);
  end else

  //---------------------------------------
  if Sender=act_Link_profile_Load_XML then
  //---------------------------------------
  begin
    dmLink_export.Link_profile_Load_from_XML(FFocusedID);
 //   Link_Load_profile_XML(FFocusedID);
  end else

  //---------------------------------------
  if Sender=act_Link_profile_Save_XML then
  //---------------------------------------
  begin
    dmLink_export.Link_profile_Save_to_XML(FFocusedID);

  //  Link_profile_Save_to_XML(FFocusedID);

(*    sTempFile:=GetTempFileName_('xml');
    s:=dmLink.GetStringFieldValue(FFocusedID, FLD_profile_XML);
    StrToTxtFile(s, sTempFile);

    ShellExec('notepad', sTempFile);
*)
  end else

{
 //---------------------------------------
  if Sender=act_Apply_template then
  //---------------------------------------
  begin
    Dlg_Apply_template (FSelectedIDList);

   // ShowMessage('');

    //s:=FSelectedIDList.;

 //   for i:=0 to FSelectedIDList.Count-1 do
  //    dmOnega_DB_data.Link_Update_Length_and_Azimuth(FSelectedIDList[i].ID);


//   UpdateAntennasAzimuthAndLen_list
  end else
}

  //---------------------------------------
  if Sender=act_UpdateAntennaAzimuth then
  //---------------------------------------
  begin
    //s:=FSelectedIDList.;

    for i:=0 to FSelectedIDList.Count-1 do
      dmOnega_DB_data.Link_Update_Length_and_Azimuth(FSelectedIDList[i].ID);


//   UpdateAntennasAzimuthAndLen_list
  end else

  //---------------------------------------
  if Sender=act_Calc then begin
  //---------------------------------------
    Calc_list (FSelectedIDList);

//    for i:=0 to FSelectedIDList.Count-1 do
  //    Calc_list (FSelectedIDList[i].ID );
  end else


  //---------------------------------------
  if Sender=act_Calc_Adaptive then begin
  //---------------------------------------
      CalcAdaptive (FFocusedID);
  end else

  //---------------------------------------
  if Sender=act_Add_LinkLine then begin
  //---------------------------------------
    dmAct_LinkLine.Dlg_Add (FSelectedIDList);
  end else

  //---------------------------------------
  if Sender=act_AntennaType_change then begin
  //---------------------------------------

    if dmAct_Explorer.Dlg_Select_Object(otAntennaType, iAntennaTypeID, sName) then
      for i:=0 to FSelectedIDList.Count-1 do
        dmOnega_DB_data.Link_Update_AntennaType(FSelectedIDList[i].ID, iAntennaTypeID);

//   for i:=0 to FSelectedIDList.Count-1 do
 //     Calc (FSelectedIDList[i].ID );

//    dmAct_LinkLine.Dlg_Add (FSelectedIDList);
  end else


//---------------------------------------
  if Sender=act_Report then begin
  //---------------------------------------
    ReportSelectedItems();

  end else

  //---------------------------------------
  if Sender=act_Create_Net then begin
  //---------------------------------------
  //  dmAct_LinkNet.Add_by_LinkList(FSelectedIDList);
    dmAct_LinkNet.Add_by_LinkList(FSelectedIDList);

  end else
    raise Exception.Create('');

end;
                                                   

//--------------------------------------------------------------------
function TdmAct_Link.ItemAdd(aFolderID: integer): integer;
//--------------------------------------------------------------------
//var
 // I: Integer;
//  iLinkEnd1, iLinkEnd2: integer;
 // oAntArray: u_func.TIntArray;

 // oAntIdList: TIDList;
begin
  Result:=Tdlg_Link_add.ExecDlg (aFolderID, FBLVector);
 // if Result>0 then
 // begin
  //  Assert(Assigned(IMapEngine));
   // Assert(Assigned(IMapAct));


////////    dmMapEngine.CreateObject (otLink, Result);

    //������� ������� �� �����
    //PostEvent(WE_MAP_ENGINE_CREATE_OBJECT, Integer(otLink), Result);
//    dmMapEngine.Sync (otProperty);
//    dmMapEngine.Sync (otLinkEnd);



/////    dmMapEngine.Sync (otLinkEndAnt);

 //   dmAct_Map.MapRefresh;

 // end
end;

           
//--------------------------------------------------------------------
procedure TdmAct_Link.Del_with_LinkEnds (aID: integer);
//--------------------------------------------------------------------
begin
  if ConfirmDlg (STR_ASK_DEL) then
    dmLink_tools.Del_with_LinkEnds (aID);
end;

//--------------------------------------------------------------------
function TdmAct_Link.ItemDel(aID: integer; aName: string): boolean;
//--------------------------------------------------------------------
begin
//      SH_PostDelNode (sGUID2);
  Result:=False;

  if not dmLink_tools.DlgDelLinkLines (aID, aName) then
    exit;
//  if not DlgDelLinkLines(aID) then
 //

// ShowMessage('test');

  Result:= dmLink.Del (aID);

  g_Shell.DelNodeByID(aID, OBJ_LINK);


  {
  if Result then
  begin
    dmMapEngine_store.Feature_Del (OBJ_Link, aID);

///    PostEvent (WE_MAP_ENGINE_DEL_OBJECT, Integer(otCalcRegion), aID);

  //  dmAct_Map.MapRefresh;
  end;
   }
end;

procedure TdmAct_Link.DoOnDeleteList(Sender : TObject);
begin
  dmMapEngine_store.Reload_Object (OBJ_Link );

//  dmMapEngine_store.Reload_Object_Layers;


{  for I := 0 to FDeletedIDList.Count - 1 do    // Iterate
    dmMapEngine_store.Feature_Del (OBJ_Link, FDeletedIDList[i] );
           //    aMap.AutoRedraw := True;
}

//  PostUserMessage (WM__DEL_MAP, Integer(FDeletedIDList));

//   ShowMessage('DoOnDeleteList');
end;



//--------------------------------------------------------------------
procedure TdmAct_Link.Add (aFolderID: integer);
//--------------------------------------------------------------------
begin
  FBLVector:=NULL_VECTOR;
  inherited Add (aFolderID);
end;

//--------------------------------------------------------------------
function TdmAct_Link.AddByProfile(aVector: TBLVector; aAntenna1, aAntenna2,
    aFreq_GHz: Double; aProfileType:TProfileBuildType): Integer;
//--------------------------------------------------------------------
var
  iLinkEndID1, iLinkEndID2: integer;
  Rec: TDlgLinkAddRec;

  oIDList: TIDList;
begin
 // Assert(Assigned(IMapEngine));
 // Assert(Assigned(IMapAct));

  //---------------------------------------------------------

  FillChar(rec, SizeOf(rec), 0);

  rec.BLVector:=aVector;
  rec.Height1:=aAntenna1;
  rec.Height2:=aAntenna2;

  rec.Freq_GHz:=aFreq_GHz;
  rec.ProfileType:=aProfileType;


  Result:= Tdlg_Link_add_from_Profile.ExecDlg (rec);

//  if Result>0 then
   //  begin
//    if Assigned(IMapEngine) then
 //////////   oIDList:=TIDList.Create;

////////////    dmMapEngine.CreateObject (otLink, Result);

////    dmMapEngine.Sync (otProperty);
////    dmMapEngine.Sync (otLinkEnd);

//    dmMapEngine1.Sync (otLinkEndAnt);

 //////   oIDList.Free;

   // dmAct_Map.MapRefresh;

 //   dmLink.GetLinkEndID (Result, iLinkEndID1, iLinkEndID2);
         

 // end;
end;


procedure TdmAct_Link.AddByVector(aBLVector: TBLVector);
begin
  FBLVector:=aBLVector;
  inherited Add (0);
end;

//--------------------------------------------------------------------
procedure TdmAct_Link.ShowOnMap (aID: integer);
//--------------------------------------------------------------------
var
  blVector: TBLVector;
begin
  if dmLink.GetBLVector(aID, blVector) then
    ShowVectorOnMap (blVector, aID);
end;

//--------------------------------------------------------------------
function TdmAct_Link.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_Link_View.Create(aOwnerForm);
  (Result as Tframe_Link_View).ViewMode:=vmLink;
end;




// ---------------------------------------------------------------
function TdmAct_Link.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
  Result:= dmUser_Security.Modify_project in [mtAllow, mtAllowMy];

  SetActionsEnabled1( [
          act_Del_,
          act_Del_list,
          act_Copy
        //  act_Export_MDB,
      ],

   Result );


  //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );


end;




//--------------------------------------------------------------------
procedure TdmAct_Link.GetPopupMenu;
//--------------------------------------------------------------------
var
 // bReadOnly: Boolean;
//  lt: TdmLinkType;

  oMenuItem: TMenuItem;

  bEnable: Boolean;


begin
//  lt:=dmLink.GetType(FFocusedID);


  bEnable:=CheckActionsEnable();


(*
  CheckActionsEnable();


*)

//  bReadOnly:=dmUser_Security.ProjectIsReadOnly;



  case aMenuType of
    mtFolder: begin
//                AddMenuItem (aPopupMenu, act_Add_Item);

                AddFolderMenu_Create (aPopupMenu, bEnable);
                AddMenuItem (aPopupMenu, nil);
                AddFolderPopupMenu (aPopupMenu, bEnable);
      /////          AddMenuItem (aPopupMenu, act_Import);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Export_RPLS_XML_folder);
           //     AddMenuItem (aPopupMenu, act_Undelete);
              end;

    mtItem:   begin

//                AddMenuItem (aPopupMenu, act_Apply_template);



               // AddMenuItem (aPopupMenu, act_Print);
               //   AddMenuItem (aPopupMenu, nil);

              //  if lt = ltLink then
                  AddMenuItem (aPopupMenu, act_Add_LinkLine);

                AddMenuItem (aPopupMenu, act_Object_ShowOnMap);
                AddMenuItem (aPopupMenu, act_UpdateAntennaAzimuth);
                AddMenuItem (aPopupMenu, nil);

                AddMenuItem (aPopupMenu, act_Report);
                AddMenuItem (aPopupMenu, act_Calc);
                AddMenuItem (aPopupMenu, act_Calc_Adaptive);


                AddFolderMenu_Tools (aPopupMenu);
                AddMenuItem (aPopupMenu, act_Object_dependence);

                AddMenuItem (aPopupMenu, nil);

                oMenuItem := dlg_AddMenuItem (aPopupMenu, nil, '�������');
                  AddMenuItem (oMenuItem, act_Link_profile_Save_XML);
                  AddMenuItem (oMenuItem, act_Link_profile_Load_XML);
                  AddMenuItem (oMenuItem, nil);
                  AddMenuItem (oMenuItem, act_Link_profile_Save_to_CSV);
                  AddMenuItem (oMenuItem, act_Link_profile_Load_from_CSV);


              //  AddMenuItem (aPopupMenu, act_Change_LinkEndType);
            //    if lt = ltLink then
                begin
                  AddMenuItem (aPopupMenu, nil);
                  AddMenuItem (aPopupMenu, act_Update_Name_by_linkends);

                  AddMenuItem (aPopupMenu, act_Del_with_LinkEnds);
                  AddMenuItem (aPopupMenu, act_Del_with_Property);
                  AddMenuItem (aPopupMenu, nil);
//                  AddMenuItem (aPopupMenu, act_Export_RPLS_XML);
                end;
               // AddMenuItem (aPopupMenu, act_Update_Climate);


                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Del_ );
              (*  AddMenuItem (aPopupMenu, nil);

                AddMenuItem (aPopupMenu, act_Export_Google);
                AddMenuItem (aPopupMenu, act_Export_ACAD);
                AddMenuItem (aPopupMenu, act_Export_MDB);
                AddMenuItem (aPopupMenu, act_Export_RPLS_XML);
*)
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_GroupAssign);
                AddMenuItem (aPopupMenu, act_AntennaType_change);


                oMenuItem := dlg_AddMenuItem (aPopupMenu, nil, '�������');
                  AddMenuItem (oMenuItem, act_Export_Google);
                  AddMenuItem (oMenuItem, act_Export_MDB);
                  AddMenuItem (oMenuItem, act_Export_RPLS_XML);
                  AddMenuItem (oMenuItem, act_Export_ACAD);

                AddMenuItem (aPopupMenu, act_Copy);
          /////////      AddMenuItem (aPopupMenu, act_Audit);


                AddMenuItem (aPopupMenu, act_Move_By_GeoRegions);
                AddMenuItem (aPopupMenu, act_Report_FreqPlan);


                 AddMenuItem (aPopupMenu, nil);
                 AddMenuItem (aPopupMenu, act_Update_Climate);
                 AddMenuItem (aPopupMenu, act_Update_profile);


                 AddMenuItem (aPopupMenu, act_Audit);
                // d_Audit,

//                AddMenuItem (aPopupMenu, act_Repeater);


           /////     AddMenuItem (aPopupMenu, act_Change_LinkEndType);


  //              AddMenuItem (aPopupMenu, nil);
//                AddMenuItem (aPopupMenu, act_Eprasys );

              end;
    mtList:   begin
//              AddMenuItem (aPopupMenu, act_Apply_template);


              //  if lt = ltLink then
                begin
                  AddMenuItem (aPopupMenu, act_Create_Net);
                  AddMenuItem (aPopupMenu, nil);
                end;
                AddMenuItem (aPopupMenu, act_Report);
             //   AddMenuItem (aPopupMenu, act_Report_Separate);
                AddMenuItem (aPopupMenu, act_Calc);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Update_Name_by_linkends);
//                AddMenuItem (aPopupMenu, nil);
  //              AddMenuItem (aPopupMenu, act_Export_Google);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Del_list);

                AddFolderMenu_Tools (aPopupMenu);

                AddMenuItem (aPopupMenu, act_UpdateAntennaAzimuth);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_GroupAssign);
                AddMenuItem (aPopupMenu, act_AntennaType_change);

                oMenuItem := dlg_AddMenuItem (aPopupMenu, nil, '�������');
                  AddMenuItem (oMenuItem, act_Export_Google);
                  AddMenuItem (oMenuItem, act_Export_MDB);
                  AddMenuItem (oMenuItem, act_Export_RPLS_XML);

                AddMenuItem (aPopupMenu, act_Move_By_GeoRegions);

                AddMenuItem (aPopupMenu, act_Report_FreqPlan);

                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Update_Climate);
                AddMenuItem (aPopupMenu, act_Update_profile);



              ///////  AddMenuItem (aPopupMenu, act_Change_LinkEndType);

              end;
  end;
end;


//--------------------------------------------------------------------
procedure TdmAct_Link.Dlg_Report(aID: integer);  //; aObjectName: WideString
//--------------------------------------------------------------------
//var
//  oReportSelect: TIni_Link_Report_Param ;
begin

 // oReportSelect:=TIni_Link_Report_Param.Create;


//  oReportSelect.ID:=aID;
//  oReportSelect.ObjName:=aObjectName;

//  if dmAct_Report.Dlg_Select('link', oReportSelect) then //, sRepName); //aType: string);
 // begin

    u_Link_Report_lib.LinkReport_Execute_Link( aID);

   // ShellExec(sOutFname,'');

  //  dmReport.MakeReport (rReportSelect);

//  end;

 // FreeAndNil(oReportSelect);



{  sXMLFileName:=GetTempXmlFileName();
  u_Link_Report_lib.LinkReport_Execute_Link(aID, sXMLFileName);

  dmAct_Report.ExecDlg (sXMLFileName, 'link');//,  dmLink.GetNameByID (aID)); //aType: string);
}
end;


//--------------------------------------------------------------------
procedure TdmAct_Link.ReportSelectedItems;
//--------------------------------------------------------------------
var
  oReportSelect: TIni_Link_Report_Param;
begin
 // Assert(Assigned(IReport));

  oReportSelect:=TIni_Link_Report_Param.Create;


//  if dmAct_Report.Dlg_Select ('link', oReportSelect) then //, sRepName); //aType: string);
//  begin

    u_Link_Report_lib.LinkReport_Execute_LinkList ( FSelectedIDList);

//    u_Link_Report_lib.LinkReport_Execute_Link(aID, rReportSelect.XMLFileName);


//  end;


  oReportSelect.Free;


  //  sXMLFileName:=GetTempXmlFileName();
//    sXMLFileName:='t:\aaaaaaaaaaaaaaaa.xml';

  //  u_Link_Report_lib.LinkReport_Execute_LinkList (SelectedIDList, sXMLFileName);

{
    if SelectedIDList.Count>1 then
      sRepName:='����� �� ����������'

    else

    if SelectedIDList.Count=1 then
      sRepName:=dmLink.GetNameByID ( SelectedIDList[0].ID)
    else
      Exit;}


//    dmAct_Report.ExecDlg (sXMLFileName, 'link'); //, sRepName); //aType: string);
  {  if dmAct_Report.ExecDlg ('link', rReportSelect) then //, sRepName); //aType: string);
      dmReport.MakeReport (rReportSelect);}

    //  TReportSelectRec


end;


//--------------------------------------------------------------------
function TdmAct_Link.GetExplorerPath(aID: integer; aGUID: string): string;
//--------------------------------------------------------------------
var sFolderGUID: string;
begin
  sFolderGUID:=dmFolder.GetGUIDByID (dmLINK.GetFolderID(aID));

  Result := GUID_PROJECT + CRLF +
            GUID_LINK    + CRLF +
            sFolderGUID  + CRLF +
            aGUID;
end;

//--------------------------------------------------------------------
function TdmAct_Link.ViewInProjectTree(aID: integer): TStrArray;
//--------------------------------------------------------------------
var sFolderGUID: string;
  iFolderID: Integer;
begin
//  Assert(Assigned(IMainProjectExplorer));
//  if Assigned(IMainProjectExplorer) then
  begin
//    IMainProjectExplorer.ExpandByGUID(GUID_PROJECT);
//    IMainProjectExplorer.ExpandByGUID(GUID_LINK_GROUP);
//    IMainProjectExplorer.ExpandByGUID(GUID_LINK);

(*    IMainProjectExplorer.ExpandByGUID(GUID_PROJECT);
    IMainProjectExplorer.ExpandByGUID(GUID_PROPERTY);
*)


    iFolderID := dmLINK.GetFolderID(aID);

    if iFolderID=0 then
      sFolderGUID:=g_Obj.ItemByName[ObjectName].RootFolderGUID
    else
      sFolderGUID:=dmFolder.GetGUIDByID(iFolderID);


     Result := StrArray([GUID_PROJECT,
                         GUID_LINK_GROUP,
                         GUID_Link,
                         sFolderGUID]);


  //  sFolderGUID:=dmFolder.GetGUIDByID (iFolderID);

  //  IMainProjectExplorer.ExpandByGUID(sFolderGUID);
//    IMainProjectExplorer.FocuseNodeByGUID (aGUID);

 //   IMainProjectExplorer.FocuseNodeByObjName(ObjectName, aID);

  end;
end;


//--------------------------------------------------------------------
function TdmAct_Link.ViewInProjectTree11(aID: integer): TStrArray;
//--------------------------------------------------------------------
var sFolderGUID: string;
  iFolderID: Integer;
begin
//  Assert(Assigned(IMainProjectExplorer));
  if Assigned(IMainProjectExplorer) then
  begin
    IMainProjectExplorer.ExpandByGUID(GUID_PROJECT);
    IMainProjectExplorer.ExpandByGUID(GUID_LINK_GROUP);
    IMainProjectExplorer.ExpandByGUID(GUID_LINK);

(*    IMainProjectExplorer.ExpandByGUID(GUID_PROJECT);
    IMainProjectExplorer.ExpandByGUID(GUID_PROPERTY);
*)


    iFolderID := dmLINK.GetFolderID(aID);

    if iFolderID=0 then
      sFolderGUID:=g_Obj.ItemByName[ObjectName].RootFolderGUID
    else
      sFolderGUID:=dmFolder.GetGUIDByID(iFolderID);


     Result := StrArray([GUID_PROJECT,
                         GUID_LINK_GROUP,
                         GUID_Link,
                         sFolderGUID]);


  //  sFolderGUID:=dmFolder.GetGUIDByID (iFolderID);

    IMainProjectExplorer.ExpandByGUID(sFolderGUID);
//    IMainProjectExplorer.FocuseNodeByGUID (aGUID);

    IMainProjectExplorer.FocuseNodeByObjName(ObjectName, aID);

  end;
end;


//--------------------------------------------------------------------
procedure TdmAct_Link.Calc(aID: integer);
//--------------------------------------------------------------------
var
  oIDList: TIDList;
begin
  oIDList:=TIDList.Create;

  oIDList.AddID(aID);

//  TLink_run.Calc(aID,  aIsSaveReportToDB);
  TLink_run.Calc_list(oIDList);

  freeAndNil(oIDList);

//  PostUserMessage(WM_LINK_REFRESH_NEW, aID);

  g_EventManager.PostEvent_ID(et_LINK_REFRESH, aID);

 // WM_LINK_REFRESH_new

//aIDList
  //aIDList
end;

//--------------------------------------------------------------------
procedure TdmAct_Link.Calc_list(aIDList: TIDList);
//--------------------------------------------------------------------
begin
//  TLink_run.Calc(aID,  aIsSaveReportToDB);
  TLink_run.Calc_list(aIDList);

  g_EventManager.PostEvent_ID(et_LINK_REFRESH, aIDList[0].ID);

//aIDList
  //aIDList
end;




//--------------------------------------------------------------------
procedure TdmAct_Link.CalcAdaptive(aID: integer);
//--------------------------------------------------------------------
begin
  TLink_run.CalcAdaptive(aID);
end;



//--------------------------------------------------------------------
procedure TdmAct_Link.Dlg_Change_LinkEndType(Sender: TObject; aIDList: TIDList);
//--------------------------------------------------------------------
var
  i: Integer;
  iMode,iLinkEndTypeID: integer;
//  LinkEndTypeID: integer;
  sName: Widestring;

  mode_rec: TdmLinkEndTypeModeInfoRec;

  iLinkEndType_Mode_ID: integer;
  s: string;
begin
  //Assert(Assigned(IShell));
//  Assert(Assigned(ILinkEndType));



  if dmAct_Explorer.Dlg_Select_Object(otLinkEndType, iLinkEndTypeID, sName) then
    if dmAct_LinkEndType.Dlg_GetMode (iLinkEndTypeID, 0, mode_rec) then
    begin
      s:=aIDList.MakeWhereString();// ValuesToString(',');

      dmOnega_DB_data.ExecStoredProc_ ('sp_Link_Update_LinkEndType',
          [
           FLD_ID, aIDList[i].ID,

          // db_Par(FLD_ID_STR, s),
           FLD_LinkEndType_ID, iLinkEndTypeID,
           FLD_LinkEndType_MODE_ID, mode_rec.ID
          ]);

(*
      for i := 0 to aIDList.Count-1 do
        dmOnega_DB_data.Object_update_LinkEndType
          (OBJ_LinkEnd,
           aIDList[i].ID, iLinkEndTypeID, mode_rec.ID);
*)
    end;

end;




//--------------------------------------------------------------------
procedure TdmAct_Link.Dlg_Set_Repeater(aEnabled: Boolean = True);
//--------------------------------------------------------------------
var
  iLinkRepeater: Integer;
  sName: Widestring;
begin
  //Assert(Assigned(IShell));
//  Assert(Assigned(ILinkEndType));

  if dmAct_Explorer.Dlg_Select_Object(otLinkRepeater, iLinkRepeater, sName) then
  begin
    dmOnega_DB_data.ExecStoredProc_('sp_Link_Repeater_Attach_to_link',
         [
           FLD_ID,      iLinkRepeater,
           FLD_Link_ID, FFocusedID
         ]);
  end;



(*
  if IAct_Explorer.Dlg_Select_Object1(otRep, iLinkEndTypeID, sName) then
    if dmAct_LinkEndType.Dlg_GetMode (iLinkEndTypeID, 0, mode_rec) then
    begin
      for i := 0 to aIDList.Count-1 do
        dmOnega_DB_data.Object_update_LinkEndType
          (OBJ_LinkEnd,
           aIDList[i].ID, iLinkEndTypeID, mode_rec.ID);

    end;

    *)

end;
 
end.


