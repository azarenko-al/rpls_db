unit fr_Link_view;

interface
{$I ver.inc}

uses
  Dialogs,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  ActnList, Menus, ToolWin,     StdCtrls, ADODB, DBCtrls,
  rxPlacemnt, cxPC, cxControls,  dxBar,
    ComCtrls, ExtCtrls,  ImgList,

fr_Link_AdaptiveModulation,

    u_COnfig,

dm_User_Security,

  fr_View_base,

  fr_Link_Link_repeater,


 // I_Act_Explorer, //dm_Act_Explorer


  dm_Link_view_data,

  u_const_msg,
  u_func_msg,

  u_const_str,

  u_types,

  u_func,
  u_dlg,
  u_reg,

  u_const_db,


  f_Documents,

  fr_Link_params,
  fr_Link_profile,
  fr_Link_calc_results,
  fr_Link_reflection_points,
 // fr_Link_Link_repeater,

  dm_LinkLine_link,
  dm_LinkFreqPlan_link,
  dm_LinkNet_Item_Ref,
  dm_Link,

  cxPropertiesStore, Mask, rxToolEdit, AppEvnts, cxClasses, cxBarEditItem,
  cxLookAndFeels
  ;

type
  Tframe_Link_View = class(Tframe_View_Base, IMessageHandler)
    FormPlacement1: TFormPlacement;
    PageControl1: TPageControl;
    TabSheet_Inspector: TTabSheet;
    TabSheet_Profile: TTabSheet;
    TabSheet_CalcRes: TTabSheet;
    TabSheet_Refl_Points: TTabSheet;
    TabSheet_Reports: TTabSheet;
    Panel2: TPanel;
    TabSheet_Repeater: TTabSheet;
    TabSheet_AM: TTabSheet;
 //   procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
    procedure ComboEdit1ButtonClick(Sender: TObject);
//    procedure cxPageControl1PageChanging(Sender: TObject; NewPage: TcxTabSheet; var AllowChange:
     //   Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
  private
    FID : Integer;

    FParamsChanged: boolean;

    Ffrm_Link_Params: Tframe_Link_Params;
    Ffrm_Link_profile: Tframe_Link_profile;

    Ffrm_calc_results: Tframe_Link_calc_results;
    Fframe_Link_reflection_points: Tframe_Link_reflection_points;

    Ffrm_Link_Link_repeater: Tframe_Link_Link_repeater;
    Ffrm_Link_AdaptiveModulation: Tframe_Link_AdaptiveModulation;

    Ffrm_Documents: Tfrm_Documents;

    procedure DoOnUpdateAfterCalc (Sender: TObject);
    procedure UpdateVisible;

    procedure ViewPage (aPageIndex: integer);
    
    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList; var aHandled:
        boolean);

  public
    ViewMode: (vmLink,
               vmLinkLine_Link,
               vmLinkFreqPlan_Link,
               vmRR_Net_Link_Ref
               );

    procedure View(aID: integer; aGUID: string); override;
  end;

//====================================================================

//====================================================================
implementation


{$R *.dfm}


procedure Tframe_Link_View.ComboEdit1ButtonClick(Sender: TObject);
begin

end;


//--------------------------------------------------------------------
procedure Tframe_Link_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_LINK;
  SetCaption (STR_LINK);

  TableName:=TBL_LINK;


  PageControl1.Align:=alClient;


//
  CreateChildForm(Tframe_Link_Params,            Ffrm_Link_Params,  TabSheet_Inspector);

  CreateChildForm(Tframe_Link_calc_results,      Ffrm_Calc_results,  TabSheet_CalcRes);

  CreateChildForm(Tframe_Link_reflection_points, Fframe_Link_reflection_points,  TabSheet_Refl_Points);
  CreateChildForm(Tframe_Link_profile,           Ffrm_Link_profile,  TabSheet_Profile);
  CreateChildForm(Tfrm_Documents,                Ffrm_Documents,  TabSheet_Reports);

  CreateChildForm(Tframe_Link_Link_repeater,     Ffrm_Link_Link_repeater,  TabSheet_Repeater);

  CreateChildForm(Tframe_Link_AdaptiveModulation,  Ffrm_Link_AdaptiveModulation,  TabSheet_AM);


  //
  Ffrm_Link_profile.OnUpdateAfterCalc:=DoOnUpdateAfterCalc;


//  Ffrm_Map:=Tframe_Link_map.CreateChildForm( ts_Map);


 /// Ffrm_LinkEndType:=Tframe_DBInspector.CreateChildForm( ts_LinkEndType);
  //Ffrm_LinkEndType.PrepareView (OBJ_LINKEND_TYPE);

//  cxPageControl1.ActivePageIndex:=0;

  PageControl1.ActivePageIndex:=0;

//  AddControl(cxPageControl1, [PROP_ACTIVE_PAGE_INDEX]);



  AddComponentProp(PageControl1, PROP_ACTIVE_PAGE);
  cxPropertiesStore.RestoreFrom;


  TabSheet_Repeater.Caption :='�������������';


  TdmLink_view_data.Init;

  Assert(Assigned(PageControl1.OnChange));


  UpdateVisible;



 // TabSheet_Repeater.TabVisible := False;

end;


//--------------------------------------------------------------------
procedure Tframe_Link_View.UpdateVisible;
//--------------------------------------------------------------------
var
  b: Boolean;
begin
  b:=not g_Config.IsInterfaceSimple;

//  TabSheet_Profile.Visible:=b;

  TabSheet_Inspector.TabVisible:=b;
  TabSheet_CalcRes.TabVisible:=b;
  TabSheet_Refl_Points.TabVisible:=b;
  TabSheet_Reports.TabVisible:=b;
  TabSheet_Repeater.TabVisible:=b;
end;


//--------------------------------------------------------------------
procedure Tframe_Link_View.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  FreeAndNil(Ffrm_Link_Params);
  FreeAndNil(Ffrm_Link_profile);
  FreeAndNil(Ffrm_Calc_results);
  FreeAndNil(Fframe_Link_reflection_points);
  FreeAndNil(Ffrm_Documents);
  FreeAndNil(Ffrm_Link_Link_repeater);
  FreeAndNil(Ffrm_Link_AdaptiveModulation);

  inherited;
end;

//--------------------------------------------------------------------
procedure Tframe_Link_View.View(aID: integer; aGUID: string);
//--------------------------------------------------------------------
//var
//  bReadOnly: Boolean;
 // iGeoRegion1_ID: Integer;
 // iGeoRegion2_ID: Integer;
var
  bReadOnly: Boolean;
begin
  if FID = aID then
    Exit;

  FID:=aID;

//  g_Profiler.Start('procedure Tframe_Link_View.View');

//  exit;
//  dmLink_view_data.Init;


  case ViewMode of
    vmLinkLine_Link:     aID:=dmLinkLine_link.GetLinkID (aID);
    vmLinkFreqPlan_Link: aID:=dmLinkFreqPlan_Link.GetLinkID (aID);
    vmRR_Net_Link_Ref:   aID:=dmLinkNet_Item_Ref.GetLinkID (aID);
  end;

  inherited;


  if not RecordExists (aID) then
  begin
    ErrorDlg('������ �� ����������');

    Exit;
  end;


  TdmLink_view_data.Init;
//  dmLink_view_data.OpenData(aID);



  bReadOnly:=dmUser_Security.Object_Edit_ReadOnly();

  Ffrm_Link_Params.SetReadOnly(bReadOnly);



  // -------------------------
//  FGeoRegion1_ID := dmLink_view_data.DBLink.GeoRegion1_ID;
//  FGeoRegion2_ID := dmLink_view_data.DBLink.GeoRegion2_ID;

//  bReadOnly:=dmUser_Security.GeoRegion_is_ReadOnly(FGeoRegion1_ID) and
//             dmUser_Security.GeoRegion_is_ReadOnly(FGeoRegion2_ID) ;
                            
//  Ffrm_Link_Params.SetReadOnly(bReadOnly);
//  Ffrm_Link_profile.SetReadOnly(bReadOnly);

  // -------------------------


//
//  iGeoRegion1_ID := GetIntFieldValue(FLD_GeoRegion1_ID);
//  iGeoRegion2_ID := GetIntFieldValue(FLD_GeoRegion2_ID);
//
//  b:=dmUser_Security.GeoRegion_is_ReadOnly(iGeoRegion1_ID);
//
//  SetReadOnly(b, 'GeoRegion:'+IntToStr(iGeoRegion1_ID));



  ViewPage (PageControl1.ActivePageIndex);

 // g_Profiler.Stop('procedure Tframe_Link_View.View');

end;


procedure Tframe_Link_View.DoOnUpdateAfterCalc(Sender: TObject);
begin
  FParamsChanged:=True;
end;

//--------------------------------------------------------------------
procedure Tframe_Link_View.ViewPage (aPageIndex: integer);
//--------------------------------------------------------------------
begin
  if ID=0 then Exit;

  //save data
  Ffrm_Link_Params.Post;


  with PageControl1 do
    if ActivePage=TabSheet_Inspector   then Ffrm_Link_Params.View  (ID) else
    if ActivePage=TabSheet_Profile     then Ffrm_Link_profile.View (ID) else
    if ActivePage=TabSheet_CalcRes     then Ffrm_Calc_results.View () else
//    if ActivePage=TabSheet_CalcRes     then Ffrm_Calc_results.View (ID) else
    if ActivePage=TabSheet_refl_Points then Fframe_Link_reflection_points.View (ID) else
    if ActivePage=TabSheet_Reports     then Ffrm_Documents.View_Link (ID) else
    if ActivePage=TabSheet_Repeater    then Ffrm_Link_Link_repeater.View  else
    if ActivePage=TabSheet_AM          then Ffrm_Link_AdaptiveModulation.View(ID)  else;

end;

procedure Tframe_Link_View.PageControl1Change(Sender: TObject);
begin
   ViewPage (PageControl1.ActivePageIndex);
end;




procedure Tframe_Link_View.GetMessage(aMsg: TEventType; aParams:
    TEventParamList; var aHandled: boolean);
begin
//  inherited;

  case aMsg of
    et_LINK_REFRESH :  ViewPage (PageControl1.ActivePageIndex);

    et_INTERFACE_NORMAL,
    et_INTERFACE_SIMPLE:   UpdateVisible;

  end;  

   //   View (FID, '');

end;


end.

(*

  IMessageHandler_ = interface
    ['{DB6CC915-A762-4C09-B12A-111D3D2E765C}']
    procedure GetMessage_(aMsg: TEventType; aParams: TEventParamList; var aHandled:   Boolean);
  end;


  
procedure Tframe_Link_View.ApplicationEvents1Message(var Msg: tagMSG; var
    Handled: Boolean);
begin


 {

 case Msg.message of
    WM_USER + Integer (  WM_LINK_REFRESH1):

    begin
 //     Msg.

      View (FID, '');

    end;

  end;
  }

//  inherited;
end;

