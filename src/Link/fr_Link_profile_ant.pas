unit fr_Link_profile_ant;

interface
{$I ver.inc}

uses

  d_LinkEnd_Get_Band,

  u_const_msg,

//  u_Profiler,

//  I_Act_Explorer, //; dm_Act_Explorer;

//  I_act_Antenna, //dm_act_Antenna,
//     dm_Act_Explorer,
//  I_Act_LinkEnd, //   dm_Act_LinkEnd;

  dm_Onega_DB_data,

  u_Storage,

  u_lists,

  dm_Act_LinkEndType,

  dm_LinkEndType,

  dm_Main,
  I_Shell,

  dm_Antenna,
  dm_Link,
  dm_LinkEnd,
  dm_Property,

  dm_Pmp_Site,
  dm_Pmp_Sector,
  dm_PmpTerminal,

  u_types,

  u_const,
  u_const_db,
  u_const_str,
//  u_const_msg,

  u_func_msg,

  u_func,
  u_db,

  u_cx,
  u_dlg,


  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  dxmdaset,  cxStyles, cxTL, cxDBTL, cxTLData, Grids, DBGrids,
  cxVGrid, cxPropertiesStore, cxDBVGrid, cxControls, cxInplaceContainer,  cxSpinEdit,  Variants,
  DB, ADODB, ActnList, Menus, ExtCtrls, ComCtrls, StdCtrls, ToolWin,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, cxCustomData,
  cxButtonEdit, cxTextEdit, cxCheckBox, cxMaskEdit, cxTLdxBarBuiltInMenu,
  dxSkinsCore, dxSkinsDefaultPainters, AppEvnts
  ;


type
  Tframe_Link_profile_ant = class(TForm) //, IMessageHandler_)
    PopupMenu1: TPopupMenu;
    ActionList1: TActionList;
    act_Refresh: TAction;
    N1: TMenuItem;
    act_Add_Antenna: TAction;
    act_Del_Antenna: TAction;
    mnu_Antenna_Del1: TMenuItem;
    mnu_make_antenna1: TMenuItem;
    N2: TMenuItem;
    act_Copy_Antenna: TAction;
    mnu_Copy_Antenna1: TMenuItem;
    N4: TMenuItem;
    act_Grid_Setup: TAction;
    N5: TMenuItem;
    cxDBTreeList11: TcxDBTreeList;
    col__name: TcxDBTreeListColumn;
    col__azimuth: TcxDBTreeListColumn;
    col_height: TcxDBTreeListColumn;
    col__diameter: TcxDBTreeListColumn;
    col__gain: TcxDBTreeListColumn;
    col__tx_freq: TcxDBTreeListColumn;
    col__rx_freq: TcxDBTreeListColumn;
    col__Tilt: TcxDBTreeListColumn;
    col__Model_Name: TcxDBTreeListColumn;
    col__type: TcxDBTreeListColumn;
    StatusBar2111: TStatusBar;
    col__IsMaster: TcxDBTreeListColumn;
    DataSource1: TDataSource;
    col_ANTENNATYPE_ID: TcxDBTreeListColumn;
    col_Polarization: TcxDBTreeListColumn;
    act_Select_LinkendType_Mode: TAction;
    act_Select_LinkendType: TAction;
    N3: TMenuItem;
    actSelectLinkendType1: TMenuItem;
    actSelectLinkendTypeMode1: TMenuItem;
    col__Power_dBm: TcxDBTreeListColumn;
    col_THRESHOLD_BER_3: TcxDBTreeListColumn;
    ADOStoredProc1: TADOStoredProc;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_ReadOnly: TcxStyle;
    col_Loss: TcxDBTreeListColumn;
    act_Linkend_GetBand: TAction;
    actLinkendGetBand1: TMenuItem;
    ToolBar1: TToolBar;
    Button1: TButton;
    ADOConnection1: TADOConnection;
    ADOStoredProc_UPD: TADOStoredProc;
 
    procedure act_Linkend_GetBandExecute(Sender: TObject);
//    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure mem_ItemsAfterPost(DataSet: TDataSet);
    procedure FormDestroy(Sender: TObject);
    procedure mem_ItemsBeforeEdit(DataSet: TDataSet);
  //  procedure dxDBTreeExit(Sender: TObject);

    procedure col_Model_NameButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure act_RefreshExecute(Sender: TObject);
//    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);

//    procedure CheckBox1Click(Sender: TObject);
    procedure cxDBTreeList11Exit(Sender: TObject);
    procedure cxDBTreeList11Editing(Sender: TcxCustomTreeList; AColumn: TcxTreeListColumn; var Allow: Boolean);
    procedure PopupMenu1Popup(Sender: TObject);

    procedure col___heightPropertiesChange(Sender: TObject);
//    procedure col__IsMasterPropertiesEditValueChanged(Sender: TObject);
    procedure col__IsMasterPropertiesChange(Sender: TObject);
    procedure cxDBTreeList1StylesGetContentStyle(Sender, AItem: TObject;
      ANode: TcxTreeListNode; var AStyle: TcxStyle);
    procedure col_heightPropertiesEditValueChanged(Sender: TObject);
 //   procedure col_height11PropertiesValidate(Sender: TObject;
  //    var DisplayValue: Variant; var ErrorText: TCaption;
   //   var Error: Boolean);
//    procedure col_height11PropertiesEditValueChanged(Sender: TObject);
  private
 //   FOnGetNextLinkEnd: TIntNotifyEvent;
    FDataset : TDataSet;

    FOnChange: TNotifyEvent;
    FOnChangeHeight: TNotifyEvent;

    FReadOnly : Boolean;

    FIsUpdated: boolean;
    FRegPath: string;

    FLink_ID : Integer;

    FID: integer;
    FObjName: string;

    procedure Antenna_Add;
    procedure ChangeAntennaMasterChecked;
    procedure _ChangeHeight(aHeight: Double);
    procedure Antenna_Copy(aAntennaTypeID: Integer = 0);
    procedure Antenna_Del;
    procedure Linked_Dlg_GetBand;

    procedure Dlg_Select_AntennaType;
    procedure Dlg_Select_LinkendType;
    procedure Dlg_Select_LinkendType_Mode;
    procedure Test;
//    procedure OpenAntennas;




  protected
    FAntennaHeightList: TDoubleList;

    procedure DoChangeData;
//    procedure GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean); override;

  public
    IsModified: boolean;
    NextLinkEndID: integer;


    procedure DoUpdateSiteProfile;
    function GetAntennaHeightArr: u_func.TDoubleArray;
    procedure SaveAntennaHeightsToDB ();

    procedure View1(aLink_ID, aID: Integer);

    procedure RefreshData;
    procedure SetReadOnly(aValue: Boolean);

    property OnChange: TNotifyEvent read FOnChange write FOnChange;

    property OnChangeHeight: TNotifyEvent read FOnChangeHeight write FOnChangeHeight;
  end;


//===================================================================
implementation
{$R *.DFM}

uses dm_act_Antenna,
  dm_Act_Explorer;   



procedure Tframe_Link_profile_ant.SetReadOnly(aValue: Boolean);
begin
  //
  FReadOnly := aValue;



  cxDBTreeList11.OptionsData.Editing := not aValue;


  ADOStoredProc1.LockType := IIF(aValue, ltReadOnly, ltBatchOptimistic);

//  SetFormActionsEnabled(Self, not dmUser_Security.ProjectIsReadOnly);
  SetFormActionsEnabled(Self, not aValue);


 // cxGrid1DBTableView1.OptionsData.Editing := not dmUser_Security.ProjectIsReadOnly;



//  b:=dmUser_Security.GeoRegion_is_ReadOnly(iGeoRegion_ID);


end;


//-------------------------------------------------------------------
procedure Tframe_Link_profile_ant.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
//var
 // oSpinEditProperties: TcxSpinEditProperties;

begin
  inherited;

  cxDBTreeList11.OnEditing:=cxDBTreeList11Editing;
  cxDBTreeList11.OnExit   :=cxDBTreeList11Exit;



//  FRegPath := REGISTRY_COMMON_FORMS + ClassName +'\';


  FRegPath := g_Storage.GetPathByClass(ClassName)+ '1';

//////////  FRegPath:=FRegPath + '1';


//  Assert(Assigned(IShell));
//  Assert(Assigned(ILinkEndType));
//  Assert(Assigned(ILinkEnd));


 // ed_Address.Text:='';
 // ed_CODE.Text:='';



  cxDBTreeList11.Align:=alClient;

  act_Select_LinkendType.Caption :='������� ������ ���';
  act_Select_LinkendType_Mode.Caption :='������� ����� ������ ���';

  act_Linkend_GetBand.Caption :='������� ������� ���';


//  dxDBTree.Align:=alClient;

  col__name.Caption.Text    :=STR_NAME;

  col__Name.Caption.Text    :=STR_NAME;
  col_Height.Caption.Text  :=STR_HEIGHT;
  col__Diameter.Caption.Text:=STR_DIAMETER;

  col__tx_freq.Caption.Text :=STR_TX_FREQ_MHz;
  col__rx_freq.Caption.Text :=STR_RX_FREQ_MHz;

  col__Power_dBm.Caption.Text :='�������� dBm';

  col_THRESHOLD_BER_3.Caption.Text :=STR_ThRESHOLD_BER_3;

  col__IsMaster.Caption.Text :='������� ��� ������� �� ���������';

  col_Polarization.Caption.Text :='�����������';

  col_Loss.Caption.Text :=STR_LOSS;

  col_Loss.DataBinding.FieldName :=FLD_antenna_Loss;

//  col_Height.



{  col__Height.Caption.Text  :=STR_ANT_HEIGHT;
  col__Diameter.Caption.Text:=STR_ANT_DIAMETER;
}

//  col_Tilt.Visible    :=false;

 // col_Tilt.Caption:=STR_TILT;

  col__Gain.Caption.Text   :=STR_GAIN;


  FDataset:=ADOStoredProc1;

 ////////// dmLink_Items_View.InitDB (mem_Items);

//////////  dxDBTree.LoadFromRegistry (FRegPath + dxDBTree.Name);
//  dx_CheckColumnSizes_DBTreeList (dxDBTree);

  g_Storage.RestoreFromRegistry(cxDBTreeList11, ClassName);

 // col_height.Visible := False;

//  cxDBTreeList1.RestoreFromRegistry(FRegPath + cxDBTreeList1.Name);

 // mnu_make_antenna.Visible:=false;
 // mnu_Antenna_Del.Visible:=false;

//
//  {$IFDEF debug}
//     dxPageControl1_new.Visible:=True;
//
//  {$ELSE}
//     dxPageControl1_new.Visible:=False;
//
//  {$ENDIF}

 //  col__ID.Visible:=True;

  FAntennaHeightList := TDoubleList.Create();



//  col_Height.Properties.e
{

  oSpinEditProperties:=TcxSpinEditProperties(col_Height.Properties.EditProperties);
  oSpinEditProperties.Alignment.Horz:=taLeftJustify;
  oSpinEditProperties.UseMouseWheel:=False;
  oSpinEditProperties.UseCtrlIncrement:=True;
  oSpinEditProperties.SpinButtons.ShowFastButtons:=False;
}

end;


procedure Tframe_Link_profile_ant.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FAntennaHeightList);
//  cxDBTreeList1.storeToRegistry(FRegPath + cxDBTreeList1.Name);

  g_Storage.StoreToRegistry(cxDBTreeList11, ClassName);


// dxDBTree.SaveToRegistry (FRegPath + dxDBTree.Name);
  inherited;
end;

//-------------------------------------------------------------------
procedure Tframe_Link_profile_ant.View1(aLink_ID, aID: Integer);
//-------------------------------------------------------------------
// dmPmp_Sector, dmPmpTerminal, dmLinkEnd
//-------------------------------------------------------------------
var
 // iPmpSiteID,
  iPropertyID: integer;
  k: Integer;
begin
//  g_Profiler.Start('Tframe_Link_profile_ant.View');

  IsModified:=False;

//  FObjName:=aObjName;
  FID:=aID;

  FLink_ID:=aLink_ID;

  FIsUpdated:=True;


//  dmOnega_DB_data.Link_select_item(ADOQuery1, FLink_ID, aID);

  k:=dmOnega_DB_data.Link_select_item(ADOStoredProc1, FLink_ID, aID);

  cxDBTreeList11.FullExpand;

//  g_Profiler.Stop('Tframe_Link_profile_ant.View');


 // db_View( ADOStoredProc1 );

end;

//-------------------------------------------------------------------
function Tframe_Link_profile_ant.GetAntennaHeightArr: u_func.TDoubleArray;
//-------------------------------------------------------------------
var
//  iColumnInd: integer;
  i: integer;
  v: Variant;
begin
//  Result:=dmLink_Items_View.GetAntennaHeightArr1 (FID, FObjName);

  FAntennaHeightList.Clear;

 // iColumnInd:=col_height.ItemIndex;

  // with cxDBTreeList1.DataController do
  
  for i := 0 to cxDBTreeList11.AbsoluteCount - 1 do
  begin
    v:=cxDBTreeList11.AbsoluteItems[i].Values[col_height.ItemIndex];

    if not VarIsNull(v) then
      FAntennaHeightList.AddValue(v);
  end;


(*

   with cxDBTreeList1.DataController do
    for i := 0 to RecordCount - 1 do
    begin
      v:=Values[i, iColumnInd];

      if not VarIsNull(v) then
        FAntennaHeightList.AddValue(v);
    end;
    
*)
///  FAntennaHeightList.

  result :=  FAntennaHeightList.MakeDoubleArray;


end;


//------------------------------------------------------------------------
procedure Tframe_Link_profile_ant.mem_ItemsAfterPost(DataSet: TDataSet);
//------------------------------------------------------------------------
begin
  if FIsUpdated then
    Exit;

  if Assigned(FOnChange) then
    FOnChange(Self);

  IsModified:=True;
end;


//------------------------------------------------------------------------
procedure Tframe_Link_profile_ant.mem_ItemsBeforeEdit(DataSet: TDataSet);
//------------------------------------------------------------------------
var
  sObjName: string;
begin
  if FIsUpdated then
    Exit;

  sObjName:=DataSet[FLD_OBJNAME];

  if not Eq(sObjName, OBJ_LINKEND_ANTENNA) then
    DataSet.Cancel;
end;

{
//------------------------------------------------------------------------
procedure Tframe_Link_profile_ant.dxDBTreeExit(Sender: TObject);
//------------------------------------------------------------------------
begin
  db_PostDataset (FDataset);

end;

}

//------------------------------------------------------------------------
procedure Tframe_Link_profile_ant._ChangeHeight(aHeight: Double);
//------------------------------------------------------------------------
var
 // S: string;
  i: Integer;
 // iAntID: integer;
begin
////////////////////
//  cxDBTreeList1.



 //  Exit;

//  s:=dxDBTree.EditingText;

//  iAntID:=FDataset[FLD_ID];

//  FDataset[FLD_HEIGHT]     :=aHeight;
//  FDataset[FLD_HEIGHT_OLD] :=aHeight;


//
//  if (FDataset[FLD_HEIGHT] <> 0) then
//    dmAntenna.UpdateHeight1 (FDataset[FLD_ID], FDataset[FLD_HEIGHT])
//  else
//    dmAntenna.UpdateHeight1 (FDataset[FLD_ID], 1);
//

//  if (FDataset[FLD_HEIGHT] > 0) then
  if (aHeight > 0) then
    dmAntenna.UpdateHeight1 (FDataset[FLD_ID], FDataset[FLD_HEIGHT]);

//  else
 //   dmAntenna.UpdateHeight1 (FDataset[FLD_ID], 1);


  if Assigned(FOnChangeHeight) then
    FOnChangeHeight(Self);



 // DoChangeData();

 //  Exit;

//  if Assigned(FOnChangeHeight) then
//    FOnChangeHeight(Self);

end;


//------------------------------------------------------------------------
procedure Tframe_Link_profile_ant.Test;
//------------------------------------------------------------------------
var
 // S: string;
  i: Integer;
 // iAntID: integer;
begin
////////////////////
//  cxDBTreeList1.


 // if Assigned(FOnChangeHeight) then
//    FOnChangeHeight(Self);

 //  Exit;

//  s:=dxDBTree.EditingText;

//  iAntID:=FDataset[FLD_ID];

//  FDataset[FLD_HEIGHT]     :=aHeight;
//  FDataset[FLD_HEIGHT_OLD] :=aHeight;


//
//  if (FDataset[FLD_HEIGHT] <> 0) then
//    dmAntenna.UpdateHeight1 (FDataset[FLD_ID], FDataset[FLD_HEIGHT])
//  else
//    dmAntenna.UpdateHeight1 (FDataset[FLD_ID], 1);
//

  if (FDataset[FLD_HEIGHT] > 0) then
    dmAntenna.UpdateHeight1 (FDataset[FLD_ID], FDataset[FLD_HEIGHT]);

//  else
 //   dmAntenna.UpdateHeight1 (FDataset[FLD_ID], 1);



 // DoChangeData();

 //  Exit;

//  if Assigned(FOnChangeHeight) then
//    FOnChangeHeight(Self);

end;



//------------------------------------------------------------------------
procedure Tframe_Link_profile_ant.ChangeAntennaMasterChecked;
//------------------------------------------------------------------------
var
//  iAntID: integer;
  b: boolean;
begin

//  s:=dxDBTree.EditingText;
//  iAntID:=mem_Items[FLD_ID];

  b:=not FDataset.FieldByName(FLD_IS_MASTER).AsBoolean;
//  b:=not FDataset[FLD_IS_MASTER];

  dmAntenna.Update(FDataset[FLD_ID], FLD_IS_MASTER, b);

end;



//------------------------------------------------------------------------
procedure Tframe_Link_profile_ant.col_Model_NameButtonClick(Sender: TObject;
  AbsoluteIndex: Integer);
//------------------------------------------------------------------------
var
  iID: Integer;
  sObjName: string;
  s: string;

begin
 // dxDBTree.HideEditor;

  sObjName:=AsString(FDataset[FLD_OBJNAME]);

 // s:=cxDBTreeList2.FocusedColumn.Name;


 // ShowMessage(Sender.ClassName);

  //------------------------------------------------------------------------
  if (cxDBTreeList11.FocusedColumn=col__Model_Name)then begin
  //------------------------------------------------------------------------
    //------------------------------------------------------------------------
    if Eq(sObjName, OBJ_LINKEND_ANTENNA) then
    //------------------------------------------------------------------------
    begin
      Dlg_Select_AntennaType();
    end else

    if StringIsInArr(sObjName, [OBJ_LINKEND, OBJ_PMP_SECTOR, OBJ_PMP_TERMINAL]) then

    //------------------------------------------------------------------------
//    if Eq(sObjName, OBJ_LINKEND) then
    //------------------------------------------------------------------------
    begin
      Dlg_Select_LinkendType();

    end;


  end else

  //------------------------------------------------------------------------
  if (cxDBTreeList11.FocusedColumn=col__Name) then begin
  //------------------------------------------------------------------------

(*  bLINKEND :=Eq(sObjName,OBJ_LINKEND) or
             Eq(sObjName,OBJ_PMP_SECTOR) or
             Eq(sObjName,OBJ_PMP_TERMINAL);

*)
    //------------------------------------------------------------------------
    if Eq(sObjName, OBJ_LINKEND) or
       Eq(sObjName, OBJ_PMP_SECTOR) or
       Eq(sObjName, OBJ_PMP_TERMINAL)
    then begin
    //------------------------------------------------------------------------
      iID := FDataset[FLD_ID];

      if dmAct_Explorer.Dlg_EditObject (OBJ_LINKEND, FDataset[FLD_ID]) then
        DoChangeData();
      //PostEvent(WE_UPDATE_LINK_PROFILE_ANT);

    end else

    //------------------------------------------------------------------------
    if Eq(sObjName, OBJ_LINKEND_ANTENNA) then begin
    //------------------------------------------------------------------------
      if dmAct_Explorer.Dlg_EditObject (OBJ_LINKEND_ANTENNA, FDataset[FLD_ID]) then
        DoChangeData();
      //xxPostEvent(WE_UPDATE_LINK_PROFILE_ANT);
    end;

  end;

end;



//------------------------------------------------------------------------
procedure Tframe_Link_profile_ant.PopupMenu1Popup(Sender: TObject);
//------------------------------------------------------------------------
var
  bAnt: Boolean;
  sObjName: string;
 // bAnt: Boolean;
  bLINKEND: boolean;
 // bANTENNA: boolean;
begin
  if FReadOnly then
    Exit;
    

  sObjName:=FDataset.FieldByName(FLD_OBJNAME).asstring;


  bAnt:=Eq(sObjName,OBJ_LINKEND_ANTENNA) or
        Eq(sObjName,'ANTENNA');

  bLINKEND :=Eq(sObjName,OBJ_LINKEND) or
             Eq(sObjName,OBJ_PMP_SECTOR) or
             Eq(sObjName,OBJ_PMP_TERMINAL);

//  bANTENNA :=Eq(sObjName,OBJ_LINKEND_ANTENNA);

 // mnu_make_antenna.Enabled:=bLINKEND;
  act_Select_LinkendType.Enabled:=bLINKEND;
  act_Select_LinkendType_Mode.Enabled:=bLINKEND;
  act_Linkend_GetBand.Enabled:=bLINKEND;

 // mnu_Antenna_Del.Enabled :=bAnt;
  act_Del_Antenna.Enabled:=bAnt;
  act_Copy_Antenna.Enabled:=bAnt;
  act_Add_Antenna.Enabled:=bLINKEND;

  act_Linkend_GetBand.Enabled:=bLINKEND;


end;


//------------------------------------------------------------------------
procedure Tframe_Link_profile_ant.Antenna_Copy(aAntennaTypeID: Integer = 0);
//------------------------------------------------------------------------
begin
  if aAntennaTypeID=0 then
    aAntennaTypeID := FDataset.FieldbyName(FLD_ANTENNATYPE_ID).AsInteger;

  if aAntennaTypeID=0 then
  begin
    ErrorDlg('��������� �������.');
    Exit;
  end;

  if ConfirmDlg('���������� ������ ������� �� ��������� ���?') then
  begin
    dmOnega_DB_data.LinkEnd_update_antenna_type(NextLinkEndID, aAntennaTypeID);

//  dmLinkEnd.UpdateAntennaType (NextLinkEndID, aAntennaTypeID);
    DoChangeData;
//      PostEvent(WE_PROFILE_SITES_REFRESH);
  end;
end;

// ---------------------------------------------------------------
procedure Tframe_Link_profile_ant.Antenna_Add;
// ---------------------------------------------------------------
begin
  if dmAct_Antenna.Dlg_Add_LinkEnd_Antenna(FID)>0 then
//     PostEvent(WE_LINKEND_ANTENNA_ADD, [app_Par(PAR_ID, FID) ]);

  begin
    RefreshData();

    DoChangeData;
    DoUpdateSiteProfile();

  end;
     //PostEvent(WE_PROFILE_SITES_REFRESH);
end;

// ---------------------------------------------------------------
procedure Tframe_Link_profile_ant.Antenna_Del;
// ---------------------------------------------------------------
var
  iID: Integer;
begin
   iID:=FDataset.FieldByName(FLD_ID).AsInteger;
 //  iID:=FDataset.FieldByName(FLD_ANTENNA_ID).AsInteger;


   if dmAct_Antenna.Dlg_Delete (iID) then
//     if ConfirmDlg('�������') then
   begin
//     dmAct_Antenna.
   //  PostEvent(WE_LINKEND_ANTENNA_DEL, [app_Par(PAR_ID, mem_Items[FLD_ID])  ]);

     RefreshData();

     DoChangeData;

     DoUpdateSiteProfile();
//       PostEvent(WE_PROFILE_SITES_REFRESH);
   end;
end;


//------------------------------------------------------------------------
procedure Tframe_Link_profile_ant.act_RefreshExecute(Sender: TObject);
//------------------------------------------------------------------------

(*var
  iType: Integer;
*)
begin
  // -------------------------------------------------------------------
  if Sender=act_Copy_Antenna then
  // -------------------------------------------------------------------
 // begin
    Antenna_Copy() else

(*
    iType := FDataset.FieldbyName(FLD_ANTENNATYPE_ID).AsInteger;

    if iType=0 then
    begin
      ErrorDlg('��������� �������.');
      Exit;
    end;

    if ConfirmDlg('���������� �� ��������� ���') then
    begin
      dmLinkEnd.UpdateAntennaType (NextLinkEndID, iType);

      DoChangeData;
//      PostEvent(WE_PROFILE_SITES_REFRESH);
    end;
*)
 // end;

  // -------------------------------------------------------------------
   if Sender=act_Add_Antenna then
  // -------------------------------------------------------------------
   begin
     Antenna_Add ();

(*     if dmAct_Antenna.Dlg_AddLinkEndAntenna(FID)>0 then
//     PostEvent(WE_LINKEND_ANTENNA_ADD, [app_Par(PAR_ID, FID) ]);

     begin
       DoChangeData;
       DoUpdateSiteProfile();

       RefreshData();
     end;
     *)

     //PostEvent(WE_PROFILE_SITES_REFRESH);
   end else


  // -------------------------------------------------------------------
   if Sender=act_Del_Antenna then
  // -------------------------------------------------------------------
   begin
     Antenna_Del();
(*
     if dmAct_Antenna.Dlg_Delete (FDataset[FLD_ID]) then
//     if ConfirmDlg('�������') then
     begin
  //     dmAct_Antenna.
     //  PostEvent(WE_LINKEND_ANTENNA_DEL, [app_Par(PAR_ID, mem_Items[FLD_ID])  ]);
       DoChangeData;
       RefreshData();


       DoUpdateSiteProfile();

//       PostEvent(WE_PROFILE_SITES_REFRESH);
     end;
     *)


   end else

   if Sender=act_Linkend_GetBand then
     Linked_Dlg_GetBand() else


   if Sender=act_Select_LinkendType then
     Dlg_Select_LinkendType else

   if Sender=act_Select_LinkendType_Mode then
     Dlg_Select_LinkendType_Mode else


  // -------------------------------------------------------------------
   if Sender=act_Grid_Setup then
  // -------------------------------------------------------------------
   begin
   //  dx_Dlg_Customize_DBTreeList (cx );
   end;

 //  View1(FID, FObjName);
end;




procedure Tframe_Link_profile_ant.DoChangeData;
begin
  if Assigned(FOnChange) then
    FOnChange(Self);
end;



procedure Tframe_Link_profile_ant.RefreshData;
var
  sGUID: string;
begin
//  View1(FLInkID,FID, FObjName);
  assert (FDataset.Active, 'Tframe_Link_profile_ant.RefreshData; - FDataset.Active');

  assert (Assigned (FDataset.FindField(FLD_GUID)), 'Assigned (FDataset.FindField(FLD_GUID))' );

  sGUID:=FDataset.FieldByName(FLD_GUID).AsString;

  View1(FLink_ID, FID); //, FObjName

  if sGUID<>'' then
    FDataset.Locate(FLD_GUID, sGUID, []);
end;


procedure Tframe_Link_profile_ant.DoUpdateSiteProfile;
begin
  if Assigned(FOnChangeHeight) then
    FOnChangeHeight(Self);

end;



procedure Tframe_Link_profile_ant.cxDBTreeList11Exit(Sender: TObject);
begin
  db_PostDataset (FDataset);
end;


procedure Tframe_Link_profile_ant.cxDBTreeList11Editing(Sender: TcxCustomTreeList; AColumn: TcxTreeListColumn;
    var Allow: Boolean);
var
  sObjName: string;
  bAnt: Boolean;
  bLINKEND: boolean;
begin
  sObjName:=FDataset[FLD_OBJNAME];

  bAnt:=Eq(sObjName,OBJ_LINKEND_ANTENNA) or
        Eq(sObjName,'ANTENNA');

  bLINKEND :=Eq(sObjName,OBJ_LINKEND) or
             Eq(sObjName,OBJ_PMP_SECTOR) or
             Eq(sObjName,OBJ_PMP_TERMINAL);





  Allow:=((cxDBTreeList11.FocusedColumn=col_Height) and
             bAnt)
           or
         ((cxDBTreeList11.FocusedColumn=col__Model_Name) and
            (bAnt or bLINKEND))
           or

         ((cxDBTreeList11.FocusedColumn=col__IsMaster) and
            bAnt )
         or
         ((cxDBTreeList11.FocusedColumn=col__Name) and
            (bAnt or bLINKEND));

  if cxDBTreeList11.FocusedColumn=col_height then
   Allow:=true;

//  col_Name.DisableEditor      :=not ((mem_Items[FLD_OBJNAME]=OBJ_ANTENNA) or
   //                                  (mem_Items[FLD_OBJNAME]=OBJ_LINKEND));
end;


//-------------------------------------------------------------------
procedure Tframe_Link_profile_ant.Dlg_Select_AntennaType;
//-------------------------------------------------------------------
var
 // sObjName: string;
  iMode: Integer;
  sAntTypeName: Widestring;
  sLinkEndTypeName: string;
  iAntTypeID, iLinkEndTypeID, iLinkEndID, iLinkEnd1, ChannelType: integer;
  dFreq, dVert_Width, dHorz_Width, dGain: double;
  iPolariz: integer;

  iAntID: integer;
begin
 // dxDBTree.HideEditor;

  //sObjName:=AsString(mem_Items[FLD_OBJNAME]);


  iAntTypeID:=FDataset.FieldBYName(FLD_AntennaType_ID).AsInteger;

  if dmAct_Explorer.Dlg_Select_Object (otAntennaType, iAntTypeID, sAntTypeName) then
  begin
    iAntID:=FDataset[FLD_ID];

  //  if Eq(FObjName, OBJ_LINKEND) then
   //   if not IAct_LinkEnd.CheckRanges(FID, iAntTypeID) then
    //    Exit;

     //   Object_UPDATE_AntType(OBJ_ANTENNA,


    dmOnega_DB_data.Object_UPDATE_AntType(OBJ_LINKEND_ANTENNA, iAntID, iAntTypeID);


//    dmAntenna.ChangeAntType (iAntID, iAntTypeID);

    {
    dmAntenna.Update (iAntID, [db_Par(FLD_ANTENNATYPE_ID, iAntTypeID)]);
    dmAntenna.UpdateAntTypeInfo (iAntID);
     }

//        View1(FID, FObjName);
    Antenna_Copy(iAntTypeID);

//    act_Copy_Antenna.Execute;

    RefreshData;
  end;
end;

//-------------------------------------------------------------------
procedure Tframe_Link_profile_ant.Dlg_Select_LinkendType;
//-------------------------------------------------------------------
var

  iLinkEndID: Integer;
  iLinkEndTypeID: Integer;
  sLinkEndTypeName: Widestring;

  mode_rec: TdmLinkEndTypeModeInfoRec;
  iLinkendType_Mode_ID: integer;
  sObjName: string;

begin
  iLinkEndID:=FDataset[FLD_ID];
  iLinkEndTypeID:=FDataset.FieldBYName(FLD_LINKENDTYPE_ID).AsInteger;
  iLinkendType_Mode_ID:= FDataset.FieldByName(FLD_LINKENDTYPE_Mode_ID).AsInteger ;

  sObjName:=FDataset.FieldBYName(FLD_OBJNAME).AsString;

 // sLinkEndTypeName:=FDataset.FieldBYName(FLD_EquipmentType_Name).AsString;

  if dmAct_Explorer.Dlg_Select_Object (otLinkEndType, iLinkEndTypeID, sLinkEndTypeName) then
    if dmAct_LinkEndType.Dlg_GetMode (iLinkEndTypeID, iLinkendType_Mode_ID, mode_rec) then //iMode, iLinkEndType_Mode_ID,
  begin
    Assert(iLinkEndID>0, 'Value <=0');

    dmOnega_DB_data.Object_update_LinkEndType
      (sObjName, iLinkEndID, iLinkEndTypeID, mode_rec.ID);

    if ConfirmDlg('����������� ��������� ������������ ��� ��������� ���?') then
      dmOnega_DB_data.LinkEnd_UpdateNext(iLinkEndID);

    DoChangeData();

    RefreshData();
  end;

end;


//-------------------------------------------------------------------
procedure Tframe_Link_profile_ant.Dlg_Select_LinkendType_Mode;
//-------------------------------------------------------------------
var

 // iAntID: integer;
  iLinkEndID: Integer;
  iLinkEndTypeID: Integer;
  sLinkEndTypeName: string;

  mode_rec: TdmLinkEndTypeModeInfoRec;
  iLinkendType_Mode_ID: integer;
  sObjName: string;

begin
  iLinkEndID:=FDataset[FLD_ID];
  iLinkEndTypeID:=FDataset.FieldBYName(FLD_LINKENDTYPE_ID).AsInteger;
  iLinkendType_Mode_ID:= FDataset.FieldByName(FLD_LINKENDTYPE_Mode_ID).AsInteger ;

  sObjName:=FDataset.FieldBYName(FLD_OBJNAME).AsString;


 // sLinkEndTypeName:=FDataset.FieldBYName(FLD_EquipmentType_Name).AsString;

//  if dmAct_Explorer.Dlg_Select_Object(otLinkEndType, iLinkEndTypeID, sLinkEndTypeName) then
  if dmAct_LinkEndType.Dlg_GetMode (iLinkEndTypeID, iLinkendType_Mode_ID, mode_rec) then //iMode, iLinkEndType_Mode_ID,
  begin
    Assert(iLinkEndID>0, 'Value <=0');

    dmOnega_DB_data.Object_update_LinkEndType
      (sObjName,
      iLinkEndID, iLinkEndTypeID, mode_rec.ID); //mode_rec.Mode,

    if ConfirmDlg('����������� ��������� ������������ ��� ��������� ���?') then
      dmOnega_DB_data.LinkEnd_UpdateNext(iLinkEndID);

    DoChangeData();

    RefreshData();
  end;

end;





procedure Tframe_Link_profile_ant.col__IsMasterPropertiesChange(
  Sender: TObject);
begin
  ChangeAntennaMasterChecked ();

end;

procedure Tframe_Link_profile_ant.cxDBTreeList1StylesGetContentStyle(
  Sender, AItem: TObject; ANode: TcxTreeListNode; var AStyle: TcxStyle);
begin
  if FReadOnly then
  begin
    AStyle := cxStyle_ReadOnly;
    Exit;
  end;

end;


//--------------------------------------------------------------------
procedure Tframe_Link_profile_ant.SaveAntennaHeightsToDB ();
//--------------------------------------------------------------------
var
  sObjName: string;
begin
  if not IsModified then
    Exit;

  with FDataset do
//  with mem_Items do
  begin
    First;

    while not EOF do
    begin
      sObjName:=FieldValues[FLD_OBJNAME];

      if Eq(sObjName,OBJ_LINKEND_ANTENNA) then
//        if FieldByName(FLD_HEIGHT).AsFloat <> FieldByName(FLD_HEIGHT_OLD).AsFloat then
          dmAntenna.UpdateHeight1 (FieldValues[FLD_ID], FieldByName(FLD_HEIGHT).AsFloat);
      Next;
    end;

    First;
  end;

(*
      with cxGridDBBandedTableView1.Controller do
      for i := 0 to SelectedRowCount - 1 do
      begin
        iID:=SelectedRows[i].Values[col_id.Index];
     //   iID:=cxGrid1DBTableView1.DataController.Values[SelectedRows[i].RecordIndex, col__id.Index];
     //   oIDList1.AddID(iID);
      end;
*)

  IsModified:=False;
end;


procedure Tframe_Link_profile_ant.col___heightPropertiesChange( Sender: TObject);
var
  eH: Double;
  oEdit: TcxSpinEdit;

begin
  oEdit:=(Sender as TcxSpinEdit);
  oEdit.PostEditValue;

  eH:=(Sender as TcxSpinEdit).Value;
  _ChangeHeight(eH);

 // oEdit.DoEditing;



end;



procedure Tframe_Link_profile_ant.col_heightPropertiesEditValueChanged(
  Sender: TObject);

var
  eH: Double;
  oEdit: TcxSpinEdit;
  v: Variant;

begin

  oEdit:=(Sender as TcxSpinEdit);

  v:= oEdit.EditingText;


  oEdit.PostEditValue;

  eH:=(Sender as TcxSpinEdit).Value;
  eH:=0;

 // _ChangeHeight(eH);

  //
end;

procedure Tframe_Link_profile_ant.act_Linkend_GetBandExecute(Sender: TObject);
begin
  Linked_Dlg_GetBand;

//
end;


//-------------------------------------------------------------------
procedure Tframe_Link_profile_ant.Linked_Dlg_GetBand;
//-------------------------------------------------------------------
var
  rec_band: TLinkEnd_Band_rec;

//..  eFreqSpacing_MHz: Double;
//  iChannelSpacing: Double;

begin
  FillChar(rec_band, SizeOf(rec_band), 0);

  rec_band.LinkEndType_BAND_ID :=FDataset.FieldByName(FLD_LINKENDTYPE_BAND_ID).AsInteger;

  rec_band.LinkEndType_ID :=FDataset.FieldByName(FLD_LINKENDTYPE_ID).AsInteger;
  rec_band.Channel_Type   :=FDataset.FieldByName(FLD_Channel_Type).AsString;
  rec_band.Channel_number :=FDataset.FieldByName(FLD_Channel_number).AsInteger;

  rec_band.TxFreq_MHz     :=FDataset.FieldByName(FLD_Tx_Freq_MHz).AsFloat;
  rec_band.RxFreq_MHz     :=FDataset.FieldByName(FLD_Rx_Freq_MHz).AsFloat;

  rec_band.Channel_width_MHz:=FDataset.FieldByName(FLD_Channel_width_MHz).AsFloat;
  rec_band.SubBand          :=FDataset.FieldByName(FLD_SubBand).AsString;



  if Tdlg_LinkEnd_Get_Band.ExecDlg(rec_band) then
  begin
    case rec_band.InputType of
      itAuto:
          begin
            db_UpdateRecord__(FDataset,
            [
                FLD_LINKENDTYPE_BAND_ID, rec_band.LinkEndType_BAND_ID,

                FLD_CHANNEL_TYPE,       rec_band.Channel_Type,
                FLD_CHANNEL_NUMBER,     rec_band.Channel_number,
                FLD_TX_FREQ_MHz,        rec_band.TxFreq_MHz,
                FLD_RX_FREQ_MHz,        rec_band.RxFreq_MHz,
                FLD_SUBBAND,            rec_band.LinkEndType_BAND_Name,
                FLD_channel_width_MHz,  rec_band.Channel_width_MHz

        //    iChannelSpacing:=GetIntFieldValue(FLD_CHANNEL_SPACING);

       //     eFreqSpacing_MHz:=iChannelSpacing * rec_band.channel_width_MHz;

          //  if (GetFloatFieldValue(FLD_FREQ_SPACING) <> dFreqSpacing) then
           //     FLD_FREQ_SPACING, rec_band.Channel_width_MHz * rec_band.channel_width_MHz

            ]);

            dmOnega_DB_data.ExecStoredProc(ADOStoredProc_UPD, 'link.sp_LinkEnd_UPD',
            [
                FLD_ID,                 FID,

                FLD_LINKENDTYPE_BAND_ID, rec_band.LinkEndType_BAND_ID,

                FLD_CHANNEL_TYPE,       rec_band.Channel_Type,
                FLD_CHANNEL_NUMBER,     rec_band.Channel_number,
                FLD_TX_FREQ_MHz,        rec_band.TxFreq_MHz,
                FLD_RX_FREQ_MHz,        rec_band.RxFreq_MHz,
                FLD_SUBBAND,            rec_band.LinkEndType_BAND_Name,

                FLD_channel_width_MHz,  rec_band.Channel_width_MHz
            ] );

          end;

        itHand:
          begin
            db_UpdateRecord__(FDataset,
            [
                FLD_CHANNEL_TYPE,       rec_band.Channel_Type,

                FLD_TX_FREQ_MHz,        rec_band.TxFreq_MHz ,
                FLD_RX_FREQ_MHz,        rec_band.RxFreq_MHz,

                FLD_SubBand,            rec_band.SubBand,

                FLD_channel_width_MHz,  rec_band.Channel_width_MHz,

                FLD_CHANNEL_NUMBER,  null
            ]);

            dmOnega_DB_data.ExecStoredProc(ADOStoredProc_UPD, 'link.sp_LinkEnd_UPD',
            [

                FLD_ID,                 FID,

                FLD_CHANNEL_TYPE,       rec_band.Channel_Type,

                FLD_TX_FREQ_MHz,        rec_band.TxFreq_MHz ,
                FLD_RX_FREQ_MHz,        rec_band.RxFreq_MHz,

                FLD_SubBand,            rec_band.SubBand,

                FLD_channel_width_MHz,  rec_band.Channel_width_MHz
            ] );

//            SetFieldValue(FLD_CHANNEL_NUMBER,  rec_band.Channel_number);

          //    rec_band.Channel_number := GetIntFieldValue (FLD_CHANNEL_NUMBER);

//            SetFieldValue(FLD_FREQ_SPACING, eFreqSpacing_MHz);


          end;

    end;

   // FIsPostEnabled := True;

//    Exit;

    if Assigned(FOnChange) then
      FOnChange(Self);

  end;

end;


end.


