unit d_Link_Get_Network_Type_new;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, DB, ADODB, cxGridLevel, cxClasses, cxControls,
  Dialogs, rxPlacemnt, StdCtrls, ExtCtrls,
  cxGridCustomView, cxGrid, ComCtrls, Mask, DBCtrls, rxToolEdit,

  u_db,
  dm_Onega_DB_data,

  u_const_db,

  u_Link_const,
  u_LinkLine_const,

  rxCurrEdit
  ;

type
  // -------------------------
  Tdlg_Link_Get_Network_Type_rec_ = record
  // -------------------------
    Index     : byte;
    Length_km : Double;

    SESR      : double;
    Kng       : double;

  end;


type
  Tdlg_Link_Get_Network_Type_new = class(TForm)
    pn_Buttons: TPanel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    FormStorage1: TFormStorage;
    DataSource_Main: TDataSource;
    ADOStoredProc_Main: TADOStoredProc;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    rb_Not_Typed: TRadioButton;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label1: TLabel;
    ed_SESR: TCurrencyEdit;
    ed_KNG: TCurrencyEdit;
    col_document_name: TcxGridDBColumn;
    col_network_name: TcxGridDBColumn;
    col_max_length_km: TcxGridDBColumn;
    cxGridDBTableView2SESR: TcxGridDBColumn;
    cxGridDBTableView2Kng: TcxGridDBColumn;
    Panel1: TPanel;
    rb_Typed: TRadioButton;
    procedure FormCreate(Sender: TObject);
  private
    procedure OpenData(aLength_km: Double);
    //function ExecDlg(aLength_KM: Double; aIndex: Integer; var aRec:
     //   Tdlg_Link_Get_Network_Type_rec_): Boolean;

  public
  //aLength_KM: Double;
    class function ExecDlg(var aRec: Tdlg_Link_Get_Network_Type_rec_): Boolean;

  end;


implementation

{$R *.dfm}

procedure Tdlg_Link_Get_Network_Type_new.FormCreate(Sender: TObject);
begin
  Caption  := '��� ����';


  col_document_name.Caption :='����������� ��������';
  col_network_name.Caption :='����';
  col_max_length_km.Caption :='Max ����� [km]';


  if ADOStoredProc_Main.Active then
    ShowMessage('ADOStoredProc_Main.Active');


 /// cxGrid1.Align:=alClient;
end;

// ---------------------------------------------------------------
class function Tdlg_Link_Get_Network_Type_new.ExecDlg(var aRec:
    Tdlg_Link_Get_Network_Type_rec_): Boolean;
// ---------------------------------------------------------------
var
  ind: Integer;

//  rec: TCalcEsrBberParamRec;

begin
   with Tdlg_Link_Get_Network_Type_new.Create(Application) do
   begin
     OpenData(aRec.Length_KM);


     ed_SESR.Value :=aRec.SESR;
     ed_Kng.Value :=aRec.KNG;



   //  if aIndex<ListBox1.Items.Count then
    //   ListBox1.Selected[aIndex] := True;

 //   dmOnega_DB_data.OpenStoredProc(adostoredProc1, __sp_Link_norm)



     Result := ShowModal=mrOk;

     // -------------------------
     if Result then
     // -------------------------
     begin
     //  FillChar(aRec ,SizeOf(aRec),0);

       aRec.Length_km := ADOStoredProc_Main.FieldByName('max_length_km').AsFloat;

       aRec.SESR := ADOStoredProc_Main.FieldByName('SESR').AsFloat;
       aRec.Kng  := ADOStoredProc_Main.FieldByName('Kng').AsFloat;


     end;

     Free;
   end;

end;

// ---------------------------------------------------------------
procedure Tdlg_Link_Get_Network_Type_new.OpenData(aLength_km: Double);
// ---------------------------------------------------------------
begin

  dmOnega_DB_data.OpenStoredProc(ADOStoredProc_Main, 'sp_Link_norms',
      [FLD_LENGTH_KM, aLength_km] );


end;

end.