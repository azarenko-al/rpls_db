unit fr_dlg_Link_add;

interface

uses
  u_ComboBox_AutoComplete_v1,

  dm_Onega_DB_data,

  u_debug,
  dm_Link_add_data,

//  u_ComboBox_AutoComplete,

  d_LinkEnd_add,


 // I_Act_LinkEnd,  //dm_Act_LinkEnd,
 // I_Act_Explorer, //dm_Act_Explorer,
                 
  I_Shell,
  dm_Main,

  u_types,

  u_const_db,
  u_const,

  u_dlg,
  u_db,
  u_cx_VGrid,
  u_func,
  u_Geo,

  dm_LinkEnd,
  dm_Property,
  dm_Link,

  Variants,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ToolWin,
  StdCtrls, ExtCtrls,  ComCtrls, Menus, rxToolEdit,  Db, RxMemDS, Grids, DBGrids,
   Mask, ActnList, ADODB, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid , cxGraphics, cxVGrid, cxInplaceContainer, cxButtonEdit,

   AppEvnts, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxContainer,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox

  ;

type
//  TOnLinkEndChangedEvent = procedure(Sender: TObject) of object;

//  TOnLinkEndChangedEvent = procedure(Sender: TObject; aLinkend_ID: Integer) of object;

  TLinkEnd_info_rec = record
    LinkEnd_ID : Integer;

    POWER_dBm : double;
    tx_freq_MHz : double;
    rx_freq_MHz : double;
    threshold_ber_6 : double;
    threshold_ber_3 : Double;

    Gain : double;

    LINKENDType_ID : Integer;

    Property_ID : Integer;
    Property_BLPoint : TBLPoint;

  end;




  Tframe_Link_add = class(TForm)
    ApplicationEvents1: TApplicationEvents;
    ds_LinkEnds: TDataSource;
    ToolBar1: TToolBar;

    ActionList1: TActionList;
    act_Add_LinkEnd: TAction;
    PopupMenu1: TPopupMenu;
    actAddLinkEnd1: TMenuItem;
    qry_LinkEnds: TADOQuery;
    ToolBar2: TToolBar;
    ToolButton1: TToolButton;
    GroupBox_Prop: TGroupBox;
    qry_Prop: TADOQuery;
    act_Reload: TAction;
    b_Select: TButton;
    act_Select_Property: TAction;
    act_Open_Property: TAction;
    StatusBar1: TStatusBar;
    ds_Prop: TDataSource;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    col_Name: TcxGridDBColumn;
    col_ID: TcxGridDBColumn;
    col_link_count: TcxGridDBColumn;
    col_link_ID: TcxGridDBColumn;
    col_LinkEndType_name: TcxGridDBColumn;
    col_LinkEndType_mode: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    ds__Lookup: TDataSource;
    ADOStoredProc_Lookup: TADOStoredProc;
    act_refresh: TAction;
    Button1: TButton;
    col_property_id: TcxGridDBColumn;
    DBGrid1_test: TDBGrid;
    ComboBox1: TComboBox;
    procedure FormDestroy(Sender: TObject);
    procedure ApplicationEvents1ActionUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
     procedure act_Add_LinkEndExecute(Sender: TObject);
    procedure act_refreshExecute(Sender: TObject);
//    procedure b_RefreshClick(Sender: TObject);
//    procedure ComboBox1Change(Sender: TObject);
//    procedure b_RefreshClick(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
    procedure cxGrid1DBTableView1CustomDrawCell(Sender: TcxCustomGridTableView;
        ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone:
        Boolean);
    procedure cxGrid1DBTableView1FocusedRecordChanged(Sender: TcxCustomGridTableView;
        APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
        ANewItemRecordFocusingChanged: Boolean);
    procedure cxLookupComboBox1KeyDown(Sender: TObject; var Key: Word; Shift:
        TShiftState);
//    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
//    procedure Timer_enable_OnChangeTimer(Sender: TObject);
//    procedure qry_LinkEndsAfterScroll(DataSet: TDataSet);
//    procedure row_PropEditPropertiesButtonClick(Sender: TObject;
 //     AButtonIndex: Integer);
  private
    FOnChange: TNotifyEvent;
    FDisableControls: boolean;
//    FDisableOnChange: boolean;

    FLinkEndID: integer;
    FOnLinkEndChanged: TNotifyEvent;

    FComboBoxAutoComplete : TComboBoxAutoComplete_v1;

 //   procedure ChangeText;
//    procedure ComboBox1KeyPress(Sender: TObject; var Key: Char);
    procedure Open_PropertyLinkEnds (aPropertyID: integer);// aDataset: TDataset);

    procedure Dlg_SelectProperty;
    procedure Dlg_Select_Property;
    procedure DoOnUpdateItems(Sender: TObject);
    procedure UpdatePanel;
//    procedure ValueChanged;

  public
//    FComboBoxAutoComplete : TComboBoxAutoComplete;

  public
    PropertyID: integer;

    Params: record
      Property_BLPos: TBLPoint;
    end;


    Index : Integer;

    IsAllowAppend: boolean;

    function GetLinkEnd_info_rec(var aRec: TLinkEnd_info_rec): Boolean;

    function GetLinkEndID: integer;
    function GetPropertyPos: TBLPoint;
    function GetPropertyName: string;

    procedure Init;

    procedure Select_property(aPropertyID: integer);
    procedure Update_PROPERTY;

//    property OnLinkEndChanged: TNotifyEvent read FOnLinkEndChanged write    FOnLinkEndChanged;

    property OnChange: TNotifyEvent read FOnChange write FOnChange;

  end;


//===================================================================
implementation
{$R *.dfm}

uses dm_Act_Explorer,
     dm_Act_LinkEnd;

const
  FLD_LINK_COUNT = 'LINK_COUNT';


procedure Tframe_Link_add.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FComboBoxAutoComplete);
end;

//-------------------------------------------------------------------
procedure Tframe_Link_add.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
//  Assert(Assigned(ILinkEnd));
  //Assert(Assigned(IShell));

  inherited;

  GroupBox_Prop.Height:= 50;


  cxGrid1.Align:=alClient;

//  GroupBox1.Align:=alClient;
  cxGrid1.Align:=alClient;

  db_SetComponentADOConnection(Self, dmMain.ADOConnection);

//  cx_InitVerticalGrid (cxVerticalGrid1);


//  FComboBoxAutoComplete := TComboBoxAutoComplete.Create(ComboBox1);

//  FComboBoxAutoComplete.Parent:=GroupBox_Prop;

//  FComboBoxAutoComplete.AssignComboBox (ComboBox1);

//  FComboBoxAutoComplete.OnSelected:=Select_property;
//  ComboBox1KeyPress;

//
//
//procedure TForm5.ComboBox1KeyPress(Sender: TObject; var Key: Char);
//begin
//  if Key= Chr(VK_RETURN)  then
//    ShowMessage('');
//
//end;



(*  FComboBoxAutoComplete.Left:= ComboBox1.Left;
  FComboBoxAutoComplete.Top:= ComboBox1.Top;
  FComboBoxAutoComplete.Width:= ComboBox1.Width;

  FComboBoxAutoComplete.Anchors:= ComboBox1.Anchors;

*)



 // FComboBoxAuto.StoredItems.Assign(ListBox1.Items);


   dmOnega_DB_data.OpenStoredProc(ADOStoredProc_Lookup, 'link.sp_property_search_top_10_SEL',
  [
    'project_id', dmMain.projectID,
    'SEARCH_text', ''

  ]);

  FComboBoxAutoComplete := TComboBoxAutoComplete_v1.Create(Self);
  FComboBoxAutoComplete.Parent:=Self;
  FComboBoxAutoComplete.AssignComboBox (ComboBox1);

  FComboBoxAutoComplete.OnSelected:=Select_property;
//  FComboBoxAutoComplete.OnChange:= ComboBox1Change;
  FComboBoxAutoComplete.OnUpdateItems:= DoOnUpdateItems;

  FComboBoxAutoComplete.LoadItemsFromDataset(ADOStoredProc_Lookup);



// Update_PROPERTY;

// DBGrid1.DataSource:=dmLink_add_data.ds_Prop;


end;


//procedure Tframe_Link_add.DoOnSelectedEvent(Sender: TObject; aID: integer);
//begin
//  //
//end;


procedure Tframe_Link_add.UpdatePanel;
begin
  StatusBar1.Panels[0].Text:= 'Property ID: '+ IntToStr(PropertyID);
end;

// ---------------------------------------------------------------
procedure Tframe_Link_add.Select_property(aPropertyID: integer);
// ---------------------------------------------------------------
begin
  Debug ('Tframe_Link_add.Select_property');

  PropertyID:=aPropertyID;

//  Update_PROPERTY();

  Open_PropertyLinkEnds (aPropertyID);//, mem_LinkEnds);

//  UpdatePanel();
//  StatusBar1.Panels[0].Text:= 'Property ID: '+ IntToStr(PropertyID);

  if Assigned(FOnChange) then
   FOnChange (Self);

  //ShowMessage('Select_property : '+ IntToStr(aID));
end;



//
//procedure Tframe_Link_add.ComboBox1KeyPress(Sender: TObject; var Key: Char);
//begin
//  if Key= Chr(VK_RETURN)  then
//    Update_PROPERTY();
//
//end;
//


//--------------------------------------------------------------------
procedure Tframe_Link_add.Init;
//--------------------------------------------------------------------
var
  blPos: TBLPoint;
  sName : string;

begin
//  blPos:=Params.Property_BLPos;

  PropertyID:=dmProperty.GetNearestIDandPos (Params.Property_BLPos, blPos, sName);

  if (PropertyID>0) then
  begin
    FDisableControls:=True;
    FComboBoxAutoComplete.Text:=dmProperty.GetNameByID (PropertyID);
    FDisableControls:=False;

//    ed_Property_Name.Text:=dmProperty.GetNameByID (PropertyID);
//    row_Prop.Properties.Value:=sName; //dmProperty.GetNameByID (PropertyID);         PropertyID

//    FComboBoxAutoComplete.Text:=sName;


    //ed_Property_Name.Text;

//    row_Pos.Properties.Value:=geo_FormatBLPoint (blPos);
            //geo_FormatBLPoint (Params.Property_BLPos);;

  //  ed_Pos.Text:=geo_FormatBLPoint (Params.Property_BLPos);

   // dmLinkEnd_View.
    Open_PropertyLinkEnds (PropertyID);  //, mem_LinkEnds);
  end;

end;


//--------------------------------------------------------------------
procedure Tframe_Link_add.Open_PropertyLinkEnds (aPropertyID: integer); // aDataset: TDataset);
//--------------------------------------------------------------------
const
 // view_LinkEnd_for_Link_add = 'link.view_Dlg_Link_add__LinkEnds_SEL';

//  view_LinkEnd_for_Link_add = 'view_LinkEnd_for_Link_add';

  SQL_SELECT =
//    'SELECT * FROM '+ view_LinkEnd_for_Link_add + ' WHERE property_id=%d and Is_connected=0';
    'SELECT * FROM  view_LinkEnd_for_Link_add  WHERE property_id=%d';

begin
  dmOnega_DB_data.OpenQuery (qry_LinkEnds, Format(SQL_SELECT, [PropertyID]));

//  if Assigned(FOnChange) then
//    FOnChange (Self);

end;



//--------------------------------------------------------------------
procedure Tframe_Link_add.ApplicationEvents1ActionUpdate(Action: TBasicAction; var Handled: Boolean);
//--------------------------------------------------------------------
begin

  if (not qry_LinkEnds.IsEmpty) and
    // (mem_LinkEnds[FLD_TYPE]=OBJ_LINKEND) and
//     (qry_LinkEnds.FieldByName(FLD_IS_LINK_USED).AsBoolean = False)
     (qry_LinkEnds.FieldByName(FLD_LINK_ID).AsInteger = 0)
//     (qry_LinkEnds.FieldByName(FLD_LINK_COUNT).AsInteger = 0)

  then FLinkEndID:=qry_LinkEnds.FieldByName(FLD_ID).AsInteger
  else FLinkEndID:=0;

  IsAllowAppend:=(FLinkEndID<>0);

  if FLinkEndID>0 then
    StatusBar1.Panels[1].Text:= Format('ID: %d', [FLinkEndID])
  else
   StatusBar1.Panels[1].Text:='';

//  act_Add_LinkEnd.Enabled:=row_Prop.Properties.Value <> '';


  act_Add_LinkEnd.Enabled:= PropertyID > 0;

end;


//-------------------------------------------------------------------
function Tframe_Link_add.GetLinkEndID: integer;
//-------------------------------------------------------------------
begin
//  Result.Property_ID:=PropertyID;
 // Result.Property_BLPos:=dmProperty.GetPos (PropertyID);
//  Result:=FLinkEndID;

  if (not qry_LinkEnds.IsEmpty) and
    // (mem_LinkEnds[FLD_TYPE]=OBJ_LINKEND) and
//     (qry_LinkEnds.FieldByName(FLD_IS_LINK_USED).AsBoolean = False)
     (qry_LinkEnds.FieldByName(FLD_LINK_ID).AsInteger = 0)

  then Result:=qry_LinkEnds.FieldByName(FLD_ID).AsInteger
  else Result:=0;

end;

//-------------------------------------------------------------------
function Tframe_Link_add.GetPropertyPos: TBLPoint;
//-------------------------------------------------------------------
begin
  Result:=dmProperty.GetPos(PropertyID);
end;

//-------------------------------------------------------------------
function Tframe_Link_add.GetPropertyName: string;
//-------------------------------------------------------------------
begin
  Result:=FComboBoxAutoComplete.Text;

//  Result:=dmProperty.GetNameByID (PropertyID);
end;

//-------------------------------------------------------------------
procedure Tframe_Link_add.Dlg_SelectProperty;
//-------------------------------------------------------------------
var sName: Widestring;
   // iID: integer;
begin
  if dmAct_Explorer.Dlg_Select_Object(otProperty, PropertyID, sName) then
    Select_property(PropertyID);

//  begin
 //   FComboBoxAutoComplete.Text:= sName;
//    row_Prop.Properties.Value := sName;

  //  Params.Property_BLPos:=dmProperty.GetPos(PropertyID);
//    ed_Pos.Text:=geo_FormatBLPoint (Params.Property_BLPos);
//  end;

  //dmLinkEnd_View.
//  Open_PropertyLinkEnds (PropertyID);//, mem_LinkEnds);

//  if assigned(FOnChange) then
//    FOnChange(Self);
end;

//-------------------------------------------------------------------
procedure Tframe_Link_add.act_Add_LinkEndExecute(Sender: TObject);
//-------------------------------------------------------------------


    //--------------------------------------------------------------------
    function DoLinkEndAdd: Integer;
    //--------------------------------------------------------------------
    var
      rec: Tdlg_LinkEnd_add_rec;
    begin
      FillChar(rec, SizeOf(rec), 0);

     //  rec.FolderID  :=aFolderID;
     //  rec.BLPoint   :=FBLPoint;
      rec.PropertyID:=PropertyID;

      rec.ChannelType:= IIF (Index=1, 'low', 'high');


      Result:=Tdlg_LinkEnd_add.ExecDlg_LinkEnd(rec);//aFolderID, FBLPoint, FProperty);

      if Result>0 then
      begin
        Open_PropertyLinkEnds (PropertyID);
        qry_LinkEnds.Locate(FLD_ID, Result, []);
      end;

    end;


begin
  if (Sender=act_Add_LinkEnd) and (PropertyID>0) then
  begin
    DoLinkEndAdd;
   // dmLinkEnd_View.
   // Open_PropertyLinkEnds (PropertyID);//, mem_LinkEnds);
  end;


  if (Sender=act_Select_Property) then
  begin
    Dlg_Select_Property();


   // dmAct_LinkEnd.Add (0, PropertyID);

   // dmLinkEnd_View.
  //  Open_PropertyLinkEnds (PropertyID);//, mem_LinkEnds);
  end;
//    dmAct_LinkEnd.Add (0, PropertyID);
   // dmLinkEnd_View.
  //  Open_PropertyLinkEnds (PropertyID);//, mem_LinkEnds);
//  end;


//  act_Add_LinkEndExecute


end;

procedure Tframe_Link_add.act_refreshExecute(Sender: TObject);
begin
  Open_PropertyLinkEnds (PropertyID);
end;


// ---------------------------------------------------------------
procedure Tframe_Link_add.Update_PROPERTY;
// ---------------------------------------------------------------
begin
//  LockWindowUpdate(FComboBoxAutoComplete.Handle);
{
  dmLink_add_data.Update_PROPERTY(FComboBoxAutoComplete.Items);

  FComboBoxAutoComplete.StoredItems.Assign(FComboBoxAutoComplete.Items);

  LockWindowUpdate(0);
 }
{



 // comboBox1.Beg

  LockWindowUpdate(comboBox1.Handle);

  with qry_Prop do
    while not EOF do
  begin
    iID  := FieldByName(FLD_ID).AsInteger;
    sName:= FieldByName(FLD_NAME).AsString;

    FComboBoxAutoComplete.Items.AddObject(sName, Pointer(iID));

    Next;
  end;


  LockWindowUpdate(0);

}
{
  LockWindowUpdate(FComboBoxAutoComplete.Handle);

//  FComboBoxAutoComplete.BeginUpdate;
  FComboBoxAutoComplete.StoredItems.Assign(FComboBoxAutoComplete.Items);
//  FComboBoxAutoComplete.EndUpdate;

  LockWindowUpdate(0);
}
end;

//
//procedure Tframe_Link_add.Button2Click(Sender: TObject);
//begin
////?/Select_Property
//end;


procedure Tframe_Link_add.cxGrid1DBTableView1CustomDrawCell(Sender:
    TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo:
    TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
//  if AViewInfo.GridRecord.Values[col_link_count.Index]>0 then
//  if AViewInfo.rec Values[col_link_id.Index]>0 then
    if AViewInfo.GridRecord.Values[col_link_id.Index]>0 then
      ACanvas.Font.Style :=ACanvas.Font.Style + [fsStrikeout];

end;

procedure Tframe_Link_add.cxGrid1DBTableView1FocusedRecordChanged(Sender:
    TcxCustomGridTableView; APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
    ANewItemRecordFocusingChanged: Boolean);
begin
  if qry_LinkEnds.Active then
    if qry_LinkEnds.recordCount>0 then
      if Assigned(FOnChange) then
        FOnChange (Self);



//  if Assigned(FOnLinkEndChanged) then
//    FOnLinkEndChanged (Self, qry_LinkEnds.FieldByName(FLD_ID).AsInteger);



//  ShowMessage('cxGrid1DBTableView1FocusedRecordChanged');
end;

procedure Tframe_Link_add.cxLookupComboBox1KeyDown(Sender: TObject; var Key:
    Word; Shift: TShiftState);
begin
end;

// ---------------------------------------------------------------
function Tframe_Link_add.GetLinkEnd_info_rec(var aRec: TLinkEnd_info_rec):
    Boolean;
// ---------------------------------------------------------------
begin
  Result := qry_LinkEnds.Active and (qry_LinkEnds.RecordCount>0);

  if Result then
    with qry_LinkEnds do
    begin
      aRec.Property_ID      :=FieldByName(FLD_Property_ID).AsInteger;
      aRec.Property_BLPoint :=db_ExtractBLPoint(qry_LinkEnds);

      aRec.LinkEnd_ID      :=FieldByName(FLD_ID).AsInteger;
      aRec.POWER_dBm       :=FieldByName(FLD_POWER_dBm).AsFloat;
      aRec.tx_freq_MHz     :=FieldByName(FLD_tx_freq_MHz).AsFloat;
      aRec.rx_freq_MHz     :=FieldByName(FLD_rx_freq_MHz).AsFloat;
      aRec.threshold_ber_6 :=FieldByName(FLD_threshold_ber_6).AsFloat;
      aRec.threshold_ber_3 :=FieldByName(FLD_threshold_ber_3).AsFloat;
      aRec.Gain            :=FieldByName(FLD_Gain).AsFloat;

      aRec.LinkEndType_ID  :=FieldByName(FLD_LinkEndType_ID).AsInteger;
    end;

end;


// ---------------------------------------------------------------
procedure Tframe_Link_add.Dlg_Select_Property;
// ---------------------------------------------------------------
var
  sNAme: Widestring;
begin

//    if dmAct_Explorer.Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otProperty, PropertyID) then
    if dmAct_Explorer.Dlg_Select_Object (otProperty, PropertyID, sNAme) then
    begin
   //   Params.Property_BLPos:=dmProperty.GetPos (PropertyID);

    //  row_Pos.Properties.Value:=geo_FormatBLPoint (Params.Property_BLPos);;
  //  ed_Pos.Text:=geo_FormatBLPoint (Params.Property_BLPos);

//      cxLookupComboBox1.
      FDisableControls:=True;
//      cxLookupComboBox1.Properties.OnChange:=nil;
      FComboBoxAutoComplete.Text:=sName;
      FDisableControls:=False;

  //    cxLookupComboBox1.Properties.OnChange:=cxLookupComboBox1PropertiesChange;

//      cxLookupComboBox1.DroppedDown:=true;

   // dmLinkEnd_View.
      Open_PropertyLinkEnds (PropertyID);//, mem_LinkEnds);
    end;


   // xx  row_Prop.Properties.Value:=ed_Property_Name.Text;
    //  row_Pos.Properties.Value:=geo_FormatBLPoint (Params.Property_BLPos);;

  //  end;
end;


// ---------------------------------------------------------------
procedure Tframe_Link_add.Timer2Timer(Sender: TObject);
begin
//  Timer2.Enabled:=false;
//  ChangeText;

end;

{
//----------------------------------------------------------------
procedure Tframe_Link_add.ChangeText;
//----------------------------------------------------------------
var
  sText: string;
begin
  sText:=Trim(FComboBoxAutoComplete.Text);


  dmOnega_DB_data.OpenStoredProc(ADOStoredProc_Lookup, 'link.sp_property_search_top_10_SEL',
  [
    'project_id', dmMain.projectID,
    'SEARCH_text', sText

  ]);

  FComboBoxAutoComplete.LoadItemsFromDataset(ADOStoredProc_Lookup);


  if FComboBoxAutoComplete.Items.Count>1 then
    FComboBoxAutoComplete.DropDown;

end;
}

procedure Tframe_Link_add.DoOnUpdateItems(Sender: TObject);
var
  sText: string;
begin
  sText:=Trim(FComboBoxAutoComplete.Text);

  dmOnega_DB_data.OpenStoredProc(ADOStoredProc_Lookup,  'link.sp_property_search_top_10_SEL',
  [
    'project_id', dmMain.projectID,
    'SEARCH_text', sText

  ]);

  FComboBoxAutoComplete.LoadItemsFromDataset(ADOStoredProc_Lookup);

end;


{
procedure Tframe_Link_add.ComboBox1Change(Sender: TObject);
begin
  if not FDisableControls then
    Timer2.Enabled:=true;
end;
}


end.




(*
// ---------------------------------------------------------------
procedure Tframe_Link_add.Timer1Timer(Sender: TObject);
// ---------------------------------------------------------------
var
//  sName: string;
  sValue: string;
begin
  if FDisableControls then
    Exit;

  Debug ('Tframe_Link_add.Timer1Timer');

  Timer1.Enabled:=False;

  sValue:=VarToStr(cxLookupComboBox1.EditingValue);
  Debug ('sValue='+ sValue);

  if ADOStoredProc_Lookup.RecordCount=1 then
    if ADOStoredProc_Lookup['name']=sValue then
    begin
       Debug ('RecordCount=1');
       Debug ('Select_property (ADOStoredProc_Lookup.FieldByName(FLD_ID).AsInteger);');

       Select_property (ADOStoredProc_Lookup.FieldByName(FLD_ID).AsInteger);

       Exit;
    end;



  dmOnega_DB_data.OpenStoredProc(ADOStoredProc_Lookup, 'link.sp_property_search_top_10_SEL',
  [
    'project_id', dmMain.projectID,
    'SEARCH_text', sValue

  ]);

//  sName:=VarToStr(cxLookupComboBox1.EditingValue);

//  if Eq(ADOStoredProc_Lookup.FieldByName(FLD_NAME).AsString , trim(sNAme) ) then

//  if ADOStoredProc_Lookup.Locate (FLD_NAME, sName, []) then
//    Select_property (ADOStoredProc_Lookup.FieldByName(FLD_ID).AsInteger);

//  if ADOStoredProc_Lookup.RecordCount>0 then
 //   Select_property (ADOStoredProc_Lookup.FieldByName(FLD_ID).AsInteger);

//    PropertyID:=ADOStoredProc_Lookup.FieldByName(FLD_ID).AsInteger;

//  UpdatePanel();



  cxLookupComboBox1.DroppedDown  :=false;

  if ADOStoredProc_Lookup.RecordCount>0 then
    cxLookupComboBox1.DroppedDown  :=true;


end;

{
// ---------------------------------------------------------------
procedure Tframe_Link_add.ValueChanged;
// ---------------------------------------------------------------
var
//  sName: string;
  sValue: string;
begin
  sValue:=VarToStr(cxLookupComboBox1.EditingValue);
  Debug ('Tframe_Link_add.cxLookupComboBox1PropertiesCloseUp Value='+ sValue) ;


  if ADOStoredProc_Lookup.Locate('name',sValue,[]) then
  begin
    Debug ('ID = '+ ADOStoredProc_Lookup.FieldByName(FLD_ID).AsString);

    Select_property (ADOStoredProc_Lookup.FieldByName(FLD_ID).AsInteger);
  end;
//  cxLookupComboBox1.

  FDisableControls:=True;
end;
}

