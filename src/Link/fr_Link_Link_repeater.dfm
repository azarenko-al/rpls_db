object frame_Link_Link_repeater: Tframe_Link_Link_repeater
  Left = 1609
  Top = 394
  Width = 951
  Height = 478
  Caption = 'frame_Link_Link_repeater'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 935
    Height = 29
    ButtonHeight = 21
    Caption = 'ToolBar1'
    TabOrder = 0
    object DBEdit1: TDBEdit
      Left = 0
      Top = 2
      Width = 288
      Height = 21
      DataField = 'name'
      ParentColor = True
      ReadOnly = True
      TabOrder = 2
    end
    object Button1: TButton
      Left = 288
      Top = 2
      Width = 75
      Height = 21
      Action = act_Link_repeater_Select
      TabOrder = 0
    end
    object Button2: TButton
      Left = 363
      Top = 2
      Width = 75
      Height = 21
      Action = act_Link_repeater_Del
      TabOrder = 1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 29
    Width = 449
    Height = 410
    Align = alLeft
    BevelOuter = bvNone
    Caption = 'Panel1'
    TabOrder = 1
    object cxDBVerticalGrid_Antennas: TcxDBVerticalGrid
      Left = 0
      Top = 29
      Width = 401
      Height = 381
      Align = alLeft
      LayoutStyle = lsMultiRecordView
      LookAndFeel.Kind = lfFlat
      OptionsView.RowHeaderWidth = 191
      OptionsView.ValueWidth = 117
      Styles.Header = cxStyle_Header
      TabOrder = 0
      Version = 1
      object cxDBVerticalGrid_Antennasname: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'name'
        Visible = False
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object row_Link_Repeater: TcxDBEditorRow
        Properties.Caption = #1059#1089#1080#1083#1077#1085#1080#1077' '#1088#1077#1090#1088#1072#1085#1089#1083#1103#1090#1086#1088#1072', dB'
        Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.AssignedValues.DisplayFormat = True
        Properties.DataBinding.FieldName = 'repeater_gain'
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object row_AntennaType_name: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.DataBinding.FieldName = 'AntennaType_name'
        Properties.Options.Editing = False
        ID = 2
        ParentID = -1
        Index = 2
        Version = 1
      end
      object cxDBVerticalGrid_Antennaslat: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'lat'
        Visible = False
        ID = 3
        ParentID = -1
        Index = 3
        Version = 1
      end
      object cxDBVerticalGrid_Antennaslon: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'lon'
        Visible = False
        ID = 4
        ParentID = -1
        Index = 4
        Version = 1
      end
      object row_height: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.AssignedValues.DisplayFormat = True
        Properties.DataBinding.FieldName = 'height'
        ID = 5
        ParentID = -1
        Index = 5
        Version = 1
      end
      object row_azimuth: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.AssignedValues.DisplayFormat = True
        Properties.DataBinding.FieldName = 'azimuth'
        Properties.Options.Editing = False
        ID = 6
        ParentID = -1
        Index = 6
        Version = 1
      end
      object cxDBVerticalGrid_Antennastilt: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'tilt'
        Visible = False
        ID = 7
        ParentID = -1
        Index = 7
        Version = 1
      end
      object row_loss: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.AssignedValues.DisplayFormat = True
        Properties.DataBinding.FieldName = 'loss'
        ID = 8
        ParentID = -1
        Index = 8
        Version = 1
      end
      object cxDBVerticalGrid_Antennasband: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'band'
        Visible = False
        ID = 9
        ParentID = -1
        Index = 9
        Version = 1
      end
      object row_freq_MHz: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.DataBinding.FieldName = 'freq_MHz'
        Properties.Options.Editing = False
        Visible = False
        ID = 10
        ParentID = -1
        Index = 10
        Version = 1
      end
      object row_gain: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.AssignedValues.DisplayFormat = True
        Properties.DataBinding.FieldName = 'gain'
        Properties.Options.Editing = False
        ID = 11
        ParentID = -1
        Index = 11
        Version = 1
      end
      object row_diameter: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.AssignedValues.DisplayFormat = True
        Properties.DataBinding.FieldName = 'diameter'
        Properties.Options.Editing = False
        ID = 12
        ParentID = -1
        Index = 12
        Version = 1
      end
      object row_polarization_str: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.DataBinding.FieldName = 'polarization_str'
        Properties.Options.Editing = False
        ID = 13
        ParentID = -1
        Index = 13
        Version = 1
      end
      object cxDBVerticalGrid_Antennasid: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'id'
        Visible = False
        ID = 14
        ParentID = -1
        Index = 14
        Version = 1
      end
      object cxDBVerticalGrid_AntennasLink_Repeater_id: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Link_Repeater_id'
        Visible = False
        ID = 15
        ParentID = -1
        Index = 15
        Version = 1
      end
      object cxDBVerticalGrid_AntennasantennaType_id: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'antennaType_id'
        Visible = False
        ID = 16
        ParentID = -1
        Index = 16
        Version = 1
      end
    end
    object ToolBar2: TToolBar
      Left = 0
      Top = 0
      Width = 449
      Height = 29
      Caption = 'ToolBar2'
      TabOrder = 1
      object DBNavigator2: TDBNavigator
        Left = 0
        Top = 2
        Width = 56
        Height = 22
        VisibleButtons = [nbPost, nbCancel]
        ConfirmDelete = False
        TabOrder = 0
      end
    end
  end
  object ActionList1: TActionList
    Left = 676
    Top = 92
    object act_AntennaType_Edit: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1084#1086#1076#1077#1083#1100' '#1072#1085#1090#1077#1085#1085#1099
    end
    object act_Link_repeater_Del: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = act_Link_repeater_DelExecute
    end
    object act_Link_repeater_Select: TAction
      Caption = #1042#1099#1073#1088#1072#1090#1100
      OnExecute = act_Link_repeater_SelectExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 840
    Top = 92
    object actAntennaTypeEdit1: TMenuItem
      Action = act_AntennaType_Edit
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 760
    Top = 88
    PixelsPerInch = 96
    object cxStyle_Header: TcxStyle
      AssignedValues = [svColor]
      Color = clMoneyGreen
    end
  end
  object q_Repeater: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select top 2 * from '
      '  view_Link_repeater_Antenna')
    Left = 520
    Top = 80
  end
  object ds_Repeater: TDataSource
    DataSet = q_Repeater
    Left = 517
    Top = 132
  end
end
