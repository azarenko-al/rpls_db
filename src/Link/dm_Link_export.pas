unit dm_Link_export;

interface
//{$I ver.inc}

uses
  SysUtils, Classes, DB, ADODB, Forms, Windows, dxmdaset, Controls, Graphics, Variants, Dialogs,
  MapXLib_TLB,
  XMLDoc, XMLIntf, u_xml_document,

  u_rel_Profile,

  dm_Main,

  dm_Link,

  u_Log,


  u_files,
  u_classes,
  u_Geo,
  u_func,

  u_db,

  u_types,
  u_const_db, ActnList
  ;

type

  TdmLink_export = class(TDataModule)
    qry_Links: TADOQuery;
    SaveDialog1: TSaveDialog;
    qry_Temp: TADOQuery;
    OpenDialog1: TOpenDialog;
    ActionList1: TActionList;
    act_Link_profile_Save_XML: TAction;
    act_Link_profile_Load_XML: TAction;
    procedure DataModuleCreate(Sender: TObject);
  private
 
  public
    procedure Link_profile_Load_from_XML(aID: integer);
    procedure Link_profile_Save_to_XML(aID: integer);

    procedure Link_profile_Load_from_CSV(aID: integer);
    procedure Link_profile_Save_to_CSV(aID: integer);

  end;


function dmLink_export: TdmLink_export;


//====================================================================
//
//====================================================================
implementation {$R *.dfm}

var
  FdmLink_export: TdmLink_export;


// -------------------------------------------------------------------
function dmLink_export: TdmLink_export;
// -------------------------------------------------------------------
begin
 if not Assigned(FdmLink_export) then
   FdmLink_export := TdmLink_export.Create(Application);

  Result := FdmLink_export;
end;


procedure TdmLink_export.DataModuleCreate(Sender: TObject);
begin
  inherited;

  db_SetComponentADOConnection(Self, dmMain.AdoConnection);
end;


// ---------------------------------------------------------------
procedure TdmLink_export.Link_profile_Load_from_XML(aID: integer);
// ---------------------------------------------------------------
var
  s: string;
  oProfile : TrelProfile;
begin
  OpenDialog1.DefaultExt := '.xml';
  OpenDialog1.Filter:='XML (*.xml)|*.xml';

  if OpenDialog1.Execute then
  begin
    oProfile:=TrelProfile.Create;
    oProfile.LoadFromXmlFile(OpenDialog1.FileName);


 //   oProfile.SaveToXmlFile('S:\_1\link_.xml');

    s:=oProfile.GetXML();

    FreeAndNil(oProfile);

   // s:=OpenDialog1.FileName;

  //  s:=TxtFileToStr(OpenDialog1.FileName);
//    dmLink.Update (aID, FLD_profile_XML_, s);
    dmLink.Update_profile_XML(aID, s);

  end;
end;

// ---------------------------------------------------------------
procedure TdmLink_export.Link_profile_Save_to_XML(aID: integer);
// ---------------------------------------------------------------
var
  s,sFile: string;
begin
  SaveDialog1.FileName :='link.xml';
  SaveDialog1.DefaultExt := '.xml';
  SaveDialog1.Filter:='XML (*.xml)|*.xml';

  if SaveDialog1.Execute then
  begin
    sFile:=SaveDialog1.FileName;

    s:=dmLink.GetStringFieldValue(aID, FLD_profile_XML);
    StrToTxtFile(s, sFile);

    ShellExec('notepad', sFile);

  end;


end;



// ---------------------------------------------------------------
procedure TdmLink_export.Link_profile_Load_from_CSV(aID: integer);
// ---------------------------------------------------------------
var
  s: string;
  oProfile : TrelProfile;
begin
  OpenDialog1.DefaultExt := '.csv';
  OpenDialog1.Filter:='CSV (*.csv)|*.csv';


  if OpenDialog1.Execute then
  begin
    oProfile:=TrelProfile.Create;
    oProfile.LoadFromCSV(OpenDialog1.FileName);

    s:=oProfile.GetXML();

    FreeAndNil(oProfile);

    dmLink.Update_profile_XML (aID, s);


  end;
end;


// ---------------------------------------------------------------
procedure TdmLink_export.Link_profile_Save_to_CSV(aID: integer);
// ---------------------------------------------------------------
var
  sXML,sTempFile: string;
  oProfile : TrelProfile;

begin
// sTempFile:=GetTempFileName_('xml');
  SaveDialog1.FileName :='link.csv';
  SaveDialog1.DefaultExt := '.csv';
  SaveDialog1.Filter:='CSV (*.csv)|*.csv';


  if SaveDialog1.Execute then
  begin
    sTempFile:=SaveDialog1.FileName;

    sXML:=dmLink.GetStringFieldValue(aID, FLD_profile_XML);

    if sXML='' then
    begin
      g_Log.Error('dmLink.GetStringFieldValue(aID, FLD_profile_XML); - empty XML');
      Exit;
    end;
      

    oProfile:=TrelProfile.Create;
    oProfile.LoadFromXml(sXML,0);
    oProfile.SaveToCSV(sTempFile);

  //  sXML:=oProfile.GetXML();

    FreeAndNil(oProfile);


    ShellExec('notepad', sTempFile);

  end;

end;

begin
end.




