library dll_Options;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  Classes,
  Windows,
  d_ChangeCoordSystem in '..\src\d_ChangeCoordSystem.pas' {dlg_ChangeCoordSystem},
  d_Options in '..\src\d_Options.pas' {dlg_Options},
  d_Symbol_Select in '..\src\d_Symbol_Select.pas' {SymbolSelectDialog},
  fr_object in '..\src\fr_object.pas' {frame_Object},
  I_Options in '..\I_Options.pas',
  SysUtils,
  u_Options in '..\src\u_Options.pas',
  u_Options_classes in '..\src\u_Options_classes.pas',
  x_Options in '..\src\x_Options.pas';

{$R *.res}

begin


end.
