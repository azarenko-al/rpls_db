unit I_Options;

interface
uses ADODB, DB,Windows, Dialogs, SysUtils, Forms,

   // ?/u_dll,


   // x_Options,

    u_types,
    u_mapx
    ;

{$DEFINE test}


type
  TShapeDrawType = (dtAsVector,dtAsPolygon,dtAsSymbol);

  TOptionsObjectStyleRec =  packed record
                                MIStyle:       TmiStyleRec;
                                LineLength_M:  Integer;

                                DrawType: TShapeDrawType;

                                FieldValue: ShortString;

                              end;


type
  IOptionsX1 = interface(IInterface)
     ['{63892042-EE23-4F93-9F48-AFC313BFC906}']

    //---------------------------------------------------------
    function Dlg_ChangeCoordSystem: Boolean;stdcall;

    //---------------------------------------------------------
    function GetGeoCoord: Integer;stdcall;

    function GetDefaultStyle(aObjName: WideString): TOptionsObjectStyleRec; stdcall;

  //  function IsActiveParam(aObjName, aFieldName: WideString): boolean; stdcall;

    function GetParamCount(aObjName: WideString): Integer; stdcall;
    function GetParamByIndex(aObjName: WideString; aIndex: Integer): TOptionsObjectStyleRec;
        stdcall;

    function GetStyle(aObjName: WideString; aRecordset: _Recordset):
      TOptionsObjectStyleRec; stdcall;


    function ExecDlg(var aSet: TrpObjectTypeSet): boolean; stdcall;
    function ExecDlg_new: boolean; stdcall;

    procedure LoadOptionsXML; stdcall;

    procedure LoadFromXML(aFileName: WideString);stdcall;
    procedure SaveToXML(aFileName: WideString); stdcall;

//    procedure InitAppHandle(aAppHandle: Integer);stdcall;

    property GeoCoord: Integer read GetGeoCoord;
  end;

//
//
//{$IFDEF test}
//  function IOptions: TOptionsX;
//
////  IOptions: TOptionsX;
//
//{$ELSE}
//var
//  IOptions: IOptionsX;
//
//{$ENDIF}


//
//function IOptionsX_Load: Boolean;
//procedure IOptionsX_UnLoad;
//

implementation


end.


(*



{$IFDEF test}
uses
  x_Options;


  FOptionsX : TOptionsX;


{$ELSE}
const
  DEF_DLL = 'dll_Options.dll';

  function DllGetInterface(const IID: TGUID; var Obj): HResult; stdcall; external DEF_DLL;

{$ENDIF}

var
  LHandle : Integer;

// ---------------------------------------------------------------
function IOptionsX_Load: Boolean;
// ---------------------------------------------------------------
begin
  aSSERT(Assigned(Application));

{$IFDEF test}
  Result := x_Options.DllGetInterface (IOptionsX, IOptions)=0;
{$ELSE}
  Result := DllGetInterface (IOptionsX, IOptions)=0;

//  Result :=  GetInterface_dll(DEF_DLL, LHandle, IOptionsX, IOptions)=0;
  Assert(Result, 'Result not assigned');

  //IOptions.InitAppHandle(Application.Handle);
{$ENDIF}

end;



procedure IOptionsX_UnLoad;
begin
  IOptions := nil;

{$IFNDEF test}
//  UnloadPackage_dll(LHandle);
{$ENDIF}
//  UnloadPackage_dll(LHandle);
end;


{$IFDEF test}

function IOptions: TOptionsX;
begin
  if not Assigned(FOptionsX) then
    FOptionsX:= TOptionsX.Create;

  Result := FOptionsX;

end;

{$ELSE}
//  IOptions: IOptionsX;

{$ENDIF}


end.

