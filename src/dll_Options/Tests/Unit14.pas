unit Unit14;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,

  u_db,

  u_geo,

  I_Options,

    u_types,


  StdCtrls, Grids, DBGrids, DB, ADODB, ComCtrls, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ImgList
  ;

type
  TForm1411 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    ADOConnection1: TADOConnection;
    DBGrid1: TDBGrid;
    dbgrd1: TDBGrid;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    q_MapObjects: TADOQuery;
    ds_MapObjects: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGrid2: TDBGrid;
    TabSheet2: TTabSheet;
    ds_PickList_Values: TDataSource;
    ads_PickList_Values: TADODataSet;
    DBGrid3: TDBGrid;
    DataSource2: TDataSource;
    q_PickList_Values: TADODataSet;
    TabSheet3: TTabSheet;
    DBGrid4: TDBGrid;
    DataSource3: TDataSource;
    ADODS_Obj_fields: TADODataSet;
    Button6: TButton;
    ds_ADODataSet_Main: TDataSource;
    ADODataSet_Main: TADODataSet;
    Button7: TButton;
    ListBox_Deleted: TListBox;
    Button8: TButton;
    ListBox_updated: TListBox;
    Button9: TButton;
    ADOStoredProc1_Save: TADOStoredProc;
    Button10_Add: TButton;
    ts_Status_Values: TTabSheet;
    DBGrid5: TDBGrid;
    ds_Status_Values: TDataSource;
    q_Status_Values: TADODataSet;
    Button10_Save: TButton;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxGrid1DBTableView1user: TcxGridDBColumn;
    cxGrid1DBTableView1host: TcxGridDBColumn;
    cxGrid1DBTableView1objname: TcxGridDBColumn;
    cxGrid1DBTableView1checked: TcxGridDBColumn;
    cxGrid1DBTableView1mif_feature_type: TcxGridDBColumn;
    cxGrid1DBTableView1_: TcxGridDBColumn;
    cxGrid1DBTableView1mif_line_color: TcxGridDBColumn;
    cxGrid1DBTableView1mif_line_style: TcxGridDBColumn;
    cxGrid1DBTableView1mif_line_width: TcxGridDBColumn;
    cxGrid1DBTableView1__: TcxGridDBColumn;
    cxGrid1DBTableView1mif_font_character: TcxGridDBColumn;
    cxGrid1DBTableView1mif_font_name: TcxGridDBColumn;
    cxGrid1DBTableView1mif_font_color: TcxGridDBColumn;
    cxGrid1DBTableView1mif_font_size: TcxGridDBColumn;
    cxGrid1DBTableView1mif_font_is_halo: TcxGridDBColumn;
    cxGrid1DBTableView1mif_font_is_italic: TcxGridDBColumn;
    cxGrid1DBTableView1___: TcxGridDBColumn;
    cxGrid1DBTableView1mif_Region_Border_color: TcxGridDBColumn;
    cxGrid1DBTableView1mif_Region_Border_size: TcxGridDBColumn;
    cxGrid1DBTableView1mif_Region_Border_width: TcxGridDBColumn;
    cxGrid1DBTableView1mif_Region_Foreground_Color: TcxGridDBColumn;
    cxGrid1DBTableView1mif_Region_Background_Color: TcxGridDBColumn;
    cxGrid1DBTableView1mif_Region_Pattern: TcxGridDBColumn;
    cxGrid1DBTableView1____: TcxGridDBColumn;
    cxGrid1DBTableView1field_name: TcxGridDBColumn;
    cxGrid1DBTableView1field_value: TcxGridDBColumn;
    cxGrid1DBTableView1field_caption: TcxGridDBColumn;
    cxGrid1DBTableView1is_default: TcxGridDBColumn;
    img_MIF_LineStyle: TImageList;
    img_MIF_RegionStyle: TImageList;
    procedure ADODataSet_MainAfterEdit(DataSet: TDataSet);
    procedure Button10_AddClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
  private
    procedure SaveRec;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1411: TForm1411;

implementation
{$R *.dfm}

const
  FLD_symbol_font         = 'symbol_font';
  FLD_symbol_color        = 'symbol_color';
  FLD_symbol_style        = 'symbol_style';
  FLD_symbol_size         = 'symbol_size';
  FLD_symbol_back_enbld   = 'symbol_back_enabled';
  FLD_symbol_back_color   = 'symbol_back_color'  ;


  FLD_vector_color        = 'vector_color';
  FLD_vector_style        = 'vector_style';
  FLD_vector_width        = 'vector_width';
  FLD_vector_length       = 'vector_length';

  FLD_poly_transparent    = 'poly_transparent';
  FLD_poly_border_width   = 'poly_border_width';
  FLD_poly_length         = 'poly_length';
  FLD_poly_back_color     = 'poly_back_color';
  FLD_poly_color          = 'poly_color';
  FLD_poly_back_style     = 'poly_back_style';
  FLD_poly_border_color   = 'poly_border_color';
  FLD_poly_border_style   = 'poly_border_style';





procedure TForm1411.Button2Click(Sender: TObject);
begin
  //
end;

procedure TForm1411.Button3Click(Sender: TObject);
var
  r: TOptionsObjectStyleRec;

begin


end;


procedure TForm1411.SaveRec;
begin


end;


procedure TForm1411.Button6Click(Sender: TObject);
var
  sObjName: string;

begin
  sObjName := 'property';


  adoDs_Obj_fields.Filtered := True;
  adoDs_Obj_fields.Filter := Format('objname=''%s''', [sObjName]);

  ads_PickList_Values.Filtered := True;
  ads_PickList_Values.Filter := Format('table_name=''%s''', [sObjName]);

  ADODataSet_Main.Filtered := True;
  ADODataSet_Main.Filter := Format('objname=''%s''', [sObjName]);


//q_Status_Values

end;


procedure TForm1411.ADODataSet_MainAfterEdit(DataSet: TDataSet);
begin
  if ADODataSet_Main['id'] <> null then
    ListBox_updated.Items.Add(ADODataSet_Main.FieldByName('id').AsString );

//  DataSet['rec_state'] := 'edit';
end;


procedure TForm1411.Button10_AddClick(Sender: TObject);
var
  sObjName: string;

begin
  sObjName := 'property';


  ads_PickList_Values.Filtered := True;
  ads_PickList_Values.Filter := Format('objname=''%s''', [sObjName]);

end;

procedure TForm1411.Button8Click(Sender: TObject);
begin
  if ADODataSet_Main['id'] <> null then
    ListBox_Deleted.Items.Add(ADODataSet_Main.FieldByName('id').AsString );

end;



procedure TForm1411.Button9Click(Sender: TObject);
begin
  ADODataSet_Main.First;

  with ADODataSet_Main do
  begin
    if FieldValues['id'] <> null then
      Exit;


     Next;
  end;  


end;



end.
