inherited dlg_Options: Tdlg_Options
  Left = 1005
  Top = 294
  Width = 729
  Height = 533
  Caption = 'dlg_Options'
  Constraints.MinHeight = 532
  Constraints.MinWidth = 420
  Icon.Data = {
    0000010001002020040000000000E80200001600000028000000200000004000
    0000010004000000000000020000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    0000000000000000000000000000000000000000000007777888888888800000
    00000000000007777888888008F0000000000000000008777888888008F00000
    000000C0000008778888877008F0000000CCCCCCC00007778888777888F00000
    0C0000C0000008788888888888F000000C0000000000088888CCC88888800000
    0C00000000000FFF88CFC999999000000C0000000000088FF8CCC8888F800000
    0C00000000000888FFF987788FF0000000000000000008888FF9877778800000
    000000000000088888F9888778F0000000000000000008888889888777F00000
    00008880000007788889F8877780000000088888000007788889F88777F00000
    08888008F00007788889F8FFFFF0000778888008880000000000000000000777
    7788888888900000000000000000007777888888998F00000000000000000008
    778888998877F000000000000000000088888C888877FF000000000000000000
    08888C98888777F0000000000000000000FFFCF9888777FF0000000000000000
    00088CFF988FFF00000000000000000000008C88F9FF00000000000000000000
    00000C788F000000000000000000000000000C77000000000000000000000000
    00000C0000000000000000000000000000000C00000000000000000000000000
    00000C0000000000000000000000000000000C0000000000000000000000FFFF
    0000FFFF0000FFFF0000FFFF0000FFDF0000FC070000FBDF0000FBFF0000FBFF
    0000FBFF0000FBFF0000FFFF0000FFFF0000FF9F0000FE0F0000F8070000E003
    0000800100000000FFFF80007FFFC0003FFFE0001FFFF0000FFFF80007FFFC00
    0FFFFE003FFFFF00FFFFFF83FFFFFF8FFFFFFFBFFFFFFFBFFFFFFFBFFFFF}
  OldCreateOrder = True
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 471
    Width = 721
    TabOrder = 2
    inherited Bevel1: TBevel
      Width = 721
    end
    inherited Panel3: TPanel
      Left = 542
      inherited btn_Ok: TButton
        Left = 558
      end
      inherited btn_Cancel: TButton
        Left = 642
      end
    end
    object btn_Default: TButton
      Left = 8
      Top = 8
      Width = 87
      Height = 23
      Action = act_SetDefault
      TabOrder = 1
    end
    object Button1: TButton
      Left = 112
      Top = 8
      Width = 137
      Height = 23
      Action = act_LoadFromFile
      TabOrder = 2
    end
  end
  inherited pn_Top_: TPanel
    Width = 721
    inherited Bevel2: TBevel
      Width = 721
    end
    inherited pn_Header: TPanel
      Width = 721
      Visible = False
      inherited lb_Action: TLabel
        Left = 44
        Top = 24
      end
    end
  end
  object pn_Main: TPanel [2]
    Left = 0
    Top = 60
    Width = 721
    Height = 341
    Align = alTop
    BevelOuter = bvNone
    Caption = 'pn_Main'
    TabOrder = 0
    object Splitter: TSplitter
      Left = 169
      Top = 0
      Width = 4
      Height = 341
    end
    object cxTreeList1: TcxTreeList
      Left = 0
      Top = 0
      Width = 169
      Height = 341
      Align = alLeft
      Bands = <
        item
        end>
      LookAndFeel.Kind = lfFlat
      OptionsBehavior.ImmediateEditor = False
      OptionsData.Editing = False
      OptionsData.Deleting = False
      OptionsSelection.CellSelect = False
      OptionsView.ColumnAutoWidth = True
      TabOrder = 0
      OnSelectionChanged = cxTreeList1SelectionChanged
      object col_Name1: TcxTreeListColumn
        DataBinding.ValueType = 'String'
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object col_Page1: TcxTreeListColumn
        Visible = False
        DataBinding.ValueType = 'String'
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object cxPageControl1: TcxPageControl
      Left = 328
      Top = 0
      Width = 393
      Height = 341
      ActivePage = page_Object
      Align = alRight
      LookAndFeel.Kind = lfUltraFlat
      TabOrder = 1
      ClientRectBottom = 341
      ClientRectRight = 393
      ClientRectTop = 24
      object page_Object: TcxTabSheet
        Caption = 'page_Object'
        ImageIndex = 1
      end
      object page_Blank: TcxTabSheet
        Caption = 'page_Blank'
        ImageIndex = 0
      end
    end
  end
  inherited ActionList1: TActionList
    Left = 260
    object act_SaveToFile: TAction
      Category = 'Main'
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1072#1081#1083
    end
    object act_SetDefault: TAction
      Caption = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
      OnExecute = act_SetDefaultExecute
    end
    object act_LoadFromFile: TFileOpen
      Category = 'File'
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1080#1079' '#1092#1072#1081#1083#1072
      Dialog.DefaultExt = '*.xml'
      Dialog.FileName = 'options.xml'
      Dialog.Filter = '*.xml|*.xml'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      BeforeExecute = act_LoadFromFileBeforeExecute
      OnAccept = act_LoadFromFileAccept
    end
  end
  inherited FormStorage1: TFormStorage
    Left = 289
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 321
    Top = 6
  end
end
