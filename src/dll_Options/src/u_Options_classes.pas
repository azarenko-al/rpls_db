unit u_Options_classes;

interface

uses Graphics, ADODB, DB, Classes, Windows, Dialogs, SysUtils, Variants, XMLDoc, XMLIntf,

   u_xml_document,

     I_Options,

     u_func,
     u_MapX,
   //  u_XML,

     ADOInt;
type


  //-------------------------------------------------------------------
  TOptionsParameterItem = class(TCollectionItem)
  //-------------------------------------------------------------------
  protected

  private
  public
    Checked:  boolean;

    FieldCaption:  string;
    FieldName:     string;

    Condition:     string;

    FieldValue:    string;

//    FieldType:    string; //��� ���� : text,date,...

//    IsStatus : integer;

    Style: TOptionsObjectStyleRec;

    //inclute subitems

    procedure Assign(aSource: TOptionsParameterItem);

    function Compare(aDataset: TDataset): Boolean;
    function Caption: string;

    procedure SaveToXmlNode(aNode: IXMLNode);
    procedure LoadFromXmlNode(aNode: IXMLNode);

  end;


  TOptionsParameterList = class(TCollection)
  private
    function GetItems(Index: integer): TOptionsParameterItem;
  public
    function AddRecord(aFieldCaption,aFieldName, aCondition, aFieldValue: string):
        TOptionsParameterItem;


    procedure Assign(aSource: TOptionsParameterList);
    function GetStyle(aDataset: TDataset; var aStyle: TOptionsObjectStyleRec):
        Boolean;

    property Items[Index: integer]: TOptionsParameterItem read GetItems; default;
  end;


  //-------------------------------------------------------------------
  TOptionsObjectItem = class
  //-------------------------------------------------------------------
  public
    ObjectName:   string;

    IsModified: Boolean;

    IsAntenna: boolean;

    DrawType: TShapeDrawType;

    DefaultStyle: TOptionsObjectStyleRec;

    //Styles
    Parameters:   TOptionsParameterList;


    function GetStyle(aRecordset: _Recordset): TOptionsObjectStyleRec;

    procedure SaveToXmlNode(aNode: IXMLNode);
    procedure LoadFromXmlNode(aNode: IXMLNode);

    procedure Assign(aSource: TOptionsObjectItem);

    constructor Create;
  end;


  //-------------------------------------------------------------------
  TOptionsObjectItemList = class(TList)
  //-------------------------------------------------------------------
  private
    function GetItems(Index: Integer): TOptionsObjectItem;
    function GetItemsByName(aObjectName: string): TOptionsObjectItem;

  public
    function IsActiveParam(aObjName, aFieldName: string): boolean;
                         
    property Items[Index: Integer]: TOptionsObjectItem read GetItems; default;
    property ItemsByName[ObjectName: string]: TOptionsObjectItem read GetItemsByName;

  end;


  TOptionsAntenna = class(TOptionsObjectItem)
  public
    constructor Create;
  end;



  TOptionsGsmAntenna = class(TOptionsAntenna)
  public
    constructor Create;
  end;

  TOptionsLinkEndAntenna = class(TOptionsAntenna)
  public
    constructor Create;
  end;

  TOptionsPmpSectorAntenna = class(TOptionsAntenna)
  public
    constructor Create;
  end;


  TOptionsSite = class(TOptionsObjectItem)
    constructor Create;
  end;

  TOptionsLink = class(TOptionsObjectItem)
  public
    constructor Create;
  end;

  TOptionsProperty = class(TOptionsObjectItem)
  public
    constructor Create;
  end;

  TOptionsPmpSite = class(TOptionsObjectItem)
  public
    constructor Create;
  end;

  TOptionsPMPterminal = class(TOptionsObjectItem)
  public
    constructor Create;
  end;

  TOptionsLinkend = class(TOptionsObjectItem)
  public
    constructor Create;
  end;

  TOptionsMSC = class(TOptionsObjectItem)
  public
    constructor Create;
  end;

  TOptionsBSC = class(TOptionsObjectItem)
  public
    constructor Create;
  end;


  TOptionsLinkRepeater = class(TOptionsObjectItem)
  public
    constructor Create;
  end;

  TOptionsPmpCalcRegion = class(TOptionsObjectItem)
    constructor Create;
  end;


  TOptionsLinkPmp = class(TOptionsObjectItem)
  public
    constructor Create;
  end;




  procedure mapx_Style_SaveToXmlNode(aNode: IXMLNode; var aMIStyle: TmiStyleRec);
  procedure mapx_Style_LoadFromXmlNode(aNode: IXMLNode; var aMIStyle: TmiStyleRec);



// ==================================================================
implementation
// ==================================================================


const

  MI_PEN_PATTERN_SOLID = 2;
  MI_PEN_PATTERN_ARROW = 59;

  MI_PEN_NONE  = 0;
  MI_PEN_SOLID = 1;

  SITE_SYMBOL_COLOR    = clRed;
  DEFAULT_SYMBOL_SIZE  = 14;
  DEFAULT_SYMBOL_COLOR = clBlack;

//  MI_DEFAULT_FONT = 'Mapinfo symbols';
  MI_Map_symbols_FONT = 'Map symbols';
  MI_ONEGA_FONT   = 'Onega';

//  MI_FONT_Transportation = 'MapInfo Transportation';
//  MI_FONT_CARTOGRAPHIC   = 'MapInfo Cartographic';

  MI_SYMBOL_SIZE = 14;
  MI_SYMBOL_ANT  = 100;
  //MI_SYMBOL_ANT    = 33;

  MI_SYMBOL_STAR   = 36;
  MI_SYMBOL_CIRCLE = 41;
  MI_SYMBOL_RECT   = 39;

const

  RRL_LINE_COLOR = clGreen;
  RRL_LINE_WIDTH = 2; // in pt


  MI_BRUSH_PATTERN_BLANK = 1;
  MI_BRUSH_PATTERN_SOLID = 2;
  MI_BRUSH_PATTERN_DOTS  = 16;
  MI_BRUSH_PATTERN_RARE_DOTS  = 53;

  DEF_ANTENNA_LENGTH_M = 200;



  OBJ_BSC                     = 'BSC';
  OBJ_MSC                     = 'MSC';

  OBJ_LINK_REPEATER           = 'LINK_REPEATER';

//  OBJ_CALC_REGION             = 'Calc_Region';
  OBJ_PMP_CALC_REGION         = 'PMP_Calc_Region';
  OBJ_LINK                    = 'LINK';
  OBJ_LINK_PMP                = 'LINK_PMP';
  OBJ_LINKEND                 = 'LINKEND';
  OBJ_LINKEND_ANTENNA         = 'LinkEnd_Antenna';
  OBJ_PMP_SECTOR_ANT          = 'PMP_SECTOR_ANT';
  OBJ_PMP_SITE                = 'PMP_SITE';
  OBJ_PMP_TERMINAL            = 'PMP_TERMINAL';
  OBJ_PMP_TERMINAL_ANT        = 'PMP_TERMINAL_ANT';
  OBJ_PROPERTY                = 'PROPERTY';

  OBJ_CELL_ANT                = 'cell_ant';
  OBJ_SITE                    = 'SITE';



procedure SetFont (var aStyle: TmiStyleRec;
                   aCharacter:     Integer;
                   aFontName:      string  = MI_Map_symbols_FONT;
                   aColor:         integer = DEFAULT_SYMBOL_COLOR;
                   aSize:          integer = DEFAULT_SYMBOL_SIZE;
                   aIsFontItalic:  Boolean = False;
                   aIsFontHalo:    Boolean = True
                   );
begin
  aStyle.Character    := aCharacter;
  aStyle.FontColor    := aColor;
  aStyle.FontSize     := aSize;
  aStyle.FontName     := aFontName;
  aStyle.FontIsItalic := aIsFontItalic;
  aStyle.FontIsHalo   := aIsFontHalo;

end;


procedure SetLine (var aStyle: TmiStyleRec;
                   aLineStyle: integer;
                   aLineColor: integer;
                   aLineWidth: integer
                   );
begin
  aStyle.LineColor:=aLineColor;
  aStyle.LineWidth:=aLineWidth;
  aStyle.LineStyle:=aLineStyle;

end;



function TOptionsParameterList.AddRecord(aFieldCaption,aFieldName, aCondition,
    aFieldValue: string): TOptionsParameterItem;
//   aFieldMinValue: string = '';
  //  aFieldMaxValue: string = ''
begin
  Result := Add as TOptionsParameterItem;

  with Result do
  begin
    Checked:=True;

    FieldCaption := aFieldCaption;
    FieldName    := aFieldName;
    Condition    := aCondition;

    FieldValue   := aFieldValue;
//    FieldMinValue:= aFieldMinValue;
//    FieldMaxValue:= aFieldMaxValue;
  end;

end;

procedure TOptionsParameterList.Assign(aSource: TOptionsParameterList);
var
  I: integer;
begin
  Clear;
  for I := 0 to aSource.Count - 1 do
    (Add as TOptionsParameterItem).assign (aSource.Items[i]);
end;

function TOptionsParameterList.GetItems(Index: integer): TOptionsParameterItem;
begin
  Result := TOptionsParameterItem(inherited Items[Index]);
end;


function TOptionsObjectItemList.GetItemsByName(aObjectName: string): TOptionsObjectItem;
var
  I: integer;
begin
  Result:=nil;

  for I := 0 to Count - 1 do
    if Eq(Items[i].ObjectName,aObjectName) then
    begin
      Result := Items[i];
      Exit;
    end;

  raise Exception.Create('function TOptionsParameterList.GetItemsByName(aObjectName: string): TOptionsParameterItem; - '+ aObjectName);

end;



procedure TOptionsObjectItem.Assign(aSource: TOptionsObjectItem);
begin
  DefaultStyle:=aSource.DefaultStyle;

  //Styles
  Parameters.Assign(aSource.Parameters);
end;


constructor TOptionsObjectItem.Create;
begin
  inherited;

  Parameters:=TOptionsParameterList.Create(TOptionsParameterItem);
 // Parameters1:=TOptionsParameterList.Create(TOptionsParameterItem);

  DrawType:=dtAsSymbol;

//  if Assigned(aList) then
//    aList.Add(Self);
end;



{ TOptionsParameterItem }

procedure TOptionsParameterItem.Assign(aSource: TOptionsParameterItem);
begin
  Checked       := aSource.Checked;

  FieldCaption  := aSource.FieldCaption;
  FieldName     := aSource.FieldName;
  Condition     := aSource.Condition;

  FieldValue    := aSource.FieldValue;
  //FieldMinValue := aSource.FieldMinValue;
//  FieldMaxValue := aSource.FieldMaxValue;


  Style:=aSource.Style;
end;


constructor TOptionsGsmAntenna.Create;
begin
  inherited;

  ObjectName:=OBJ_CELL_ANT;

  SetLine (DefaultStyle.MIStyle, MI_PEN_PATTERN_ARROW, clRed, 1 );


end;

// ---------------------------------------------------------------
constructor TOptionsLinkEndAntenna.Create;
// ---------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_LINKEND_ANTENNA;

  SetLine (DefaultStyle.MIStyle, MI_PEN_PATTERN_ARROW, clGreen, 1 );

end;


//-------------------------------------------------------------------
constructor TOptionsPmpSectorAntenna.Create;
//-------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_PMP_SECTOR_ANT;

  SetLine (DefaultStyle.MIStyle, MI_PEN_PATTERN_ARROW, clGreen, 1 );

  DefaultStyle.MIStyle.RegionBorderColor:=clRed;
  DefaultStyle.MIStyle.RegionBorderWidth:=1;
  DefaultStyle.MIStyle.RegionBorderStyle:=MI_PEN_PATTERN_SOLID;
  DefaultStyle.MIStyle.RegionForegroundColor := clBlack;
  DefaultStyle.MIStyle.RegionBackgroundColor := clFuchsia;
//  DefaultStyle.Region Color      :=12615680; //����-���������
  DefaultStyle.MIStyle.RegionPattern    :=MI_BRUSH_PATTERN_DOTS;
  DefaultStyle.MIStyle.RegionIsTransparent:=False;

{
      RegionForegroundColor:= xml_GetIntAttr(aNode,'RegionForegroundColor', RegionForegroundColor);
    RegionBackgroundColor:= xml_GetIntAttr(aNode,'RegionBackgroundColor', RegionBackgroundColor);
}


  
{

  MI_BRUSH_PATTERN_BLANK = 1;
  MI_BRUSH_PATTERN_SOLID = 2;
  MI_BRUSH_PATTERN_DOTS  = 16;
  MI_BRUSH_PATTERN_RARE_DOTS  = 53;

}

  DrawType:=dtAsPolygon;

{
  DefaultStyle.LineLength:=DEF_ANTENNA_LENGTH_M;
  DrawType:=dtAsVector;
  IsAntenna := True;
}
end;


(*
constructor TOptionsCalcRegion.Create;
begin
  inherited;
  ObjectName:=OBJ_CALC_REGION;


  DefaultStyle.MIStyle.RegionBorderColor:=clRed;
  DefaultStyle.MIStyle.RegionBorderWidth:=2;
  DefaultStyle.MIStyle.RegionBorderStyle:=2;
  DefaultStyle.MIStyle.RegionBackgroundColor   := clFuchsia;
//  DefaultStyle.Region Color      :=12615680; //����-���������
  DefaultStyle.MIStyle.RegionPattern    :=0;
  DefaultStyle.MIStyle.RegionIsTransparent:=True;

  DrawType:=dtAsPolygon;

end;*)


constructor TOptionsSite.Create;
begin
  inherited;
  ObjectName:=OBJ_Site;

  SetFont(DefaultStyle.MIStyle, MI_SYMBOL_STAR, MI_Map_symbols_FONT, clRed);

end;


//-------------------------------------------------------------------
constructor TOptionsLink.Create;
//-------------------------------------------------------------------
const
  FLD_STATUS_NAME = 'STATUS_NAME';
var
  oItem: TOptionsParameterItem;
begin
  inherited;
  ObjectName:=OBJ_link;

  SetLine (DefaultStyle.MIStyle, 2, clFuchsia, 1 );

  DrawType:=dtAsVector;  //for GRID

  oItem:=Parameters.AddRecord('������',FLD_STATUS_NAME, '=', '��������');
  SetLine (oItem.Style.MIStyle, 3, clGreen, 1 );

  oItem:=Parameters.AddRecord('������',FLD_STATUS_NAME, '=', '�����������');
  SetLine (oItem.Style.MIStyle, 3, clFuchsia, 1 );

  oItem:=Parameters.AddRecord('������',FLD_STATUS_NAME, '=', '�������������');
  SetLine (oItem.Style.MIStyle, 3, clRed, 1 );

  oItem:=Parameters.AddRecord('������',FLD_STATUS_NAME, '=', '���������');
  SetLine (oItem.Style.MIStyle, 3, clBlue, 1 );

  oItem:=Parameters.AddRecord('������',FLD_STATUS_NAME, '=', '������������');
  SetLine (oItem.Style.MIStyle, 3, clFuchsia, 1 );

  oItem:=Parameters.AddRecord('������',FLD_STATUS_NAME, '=', '�����');
  SetLine (oItem.Style.MIStyle, 3, clYellow, 1 );

  oItem:=Parameters.AddRecord('������',FLD_STATUS_NAME, '=', '��������');
  SetLine (oItem.Style.MIStyle, 3, clNavy, 1 );


end;


//-------------------------------------------------------------------
procedure TOptionsObjectItem.LoadFromXmlNode(aNode: IXMLNode);
//-------------------------------------------------------------------
var
  i: integer;
  aDefStyle: IXMLNode;
  vParameters,vNode: IXmlNode;
  oItem: TOptionsParameterItem;
begin
  if VarIsnull(aNode) then
    Exit;

  vNode := xml_FindNode(aNode, 'DefaultStyle');
  mapx_Style_LoadFromXmlNode (vNode, DefaultStyle.MIStyle);

//  ObjectList[i].LoadXmlNode(xml_GetNodeByPath(aNode, 'DefaultStyle'));

  vParameters := xml_FindNode(aNode, 'Parameters');

  if Assigned(vParameters) then
  begin
    Parameters.Clear;

    for i:=0 to vParameters.ChildNodes.Count-1 do
    begin
      oItem:=Parameters.Add as TOptionsParameterItem;

      vNode:=vParameters.ChildNodes[i];
      oItem.LoadFromXmlNode(vNode);
    end;
  end;
 // mapx_

end;

//-------------------------------------------------------------------
procedure TOptionsObjectItem.SaveToXmlNode(aNode: IXMLNode);
//-------------------------------------------------------------------
var
  I: integer;
  vGroup,vNode: IXMLNode;

begin
  vGroup:=xml_AddNodeTag (aNode, 'DefaultStyle');
  mapx_Style_SaveToXmlNode (vGroup, DefaultStyle.MIStyle);

  if Parameters.Count>0 then
  begin
    vGroup:=xml_AddNodeTag (aNode, 'Parameters');

    for I := 0 to Parameters.Count - 1 do
    begin
      vNode:=xml_AddNodeTag (vGroup, 'item');
      Parameters[I].SaveToXmlNode(vNode);
    end;

  end;

//  xml_AddNode(aNode, [xml_Par(ATT_ObjectName, ObjectName)]);
end;

constructor TOptionsProperty.Create;
begin
  inherited;
  ObjectName:=OBJ_Property;

  SetFont(DefaultStyle.MIStyle, MI_SYMBOL_STAR, MI_Map_symbols_FONT, clRed);

end;

constructor TOptionsPmpSite.Create;
begin
  inherited;
  ObjectName:=OBJ_PMP_SITE;

  SetFont(DefaultStyle.MIStyle, MI_SYMBOL_STAR, MI_Map_symbols_FONT, clRed);
end;

constructor TOptionsLinkend.Create;
begin
  inherited;
  ObjectName:=OBJ_Linkend;

  SetFont(DefaultStyle.MIStyle, MI_SYMBOL_STAR, MI_Map_symbols_FONT, clGreen);

end;

constructor TOptionsPMPterminal.Create;
begin
  inherited;
  ObjectName:=OBJ_PMP_TERMINAL;

  SetFont(DefaultStyle.MIStyle, MI_SYMBOL_STAR, MI_Map_symbols_FONT, clBlue);

end;

constructor TOptionsMSC.Create;
begin
  inherited;
  ObjectName:=OBJ_MSC;

  SetFont(DefaultStyle.MIStyle, MI_SYMBOL_STAR, MI_Map_symbols_FONT, clYellow);

end;


constructor TOptionsBSC.Create;
begin
  inherited;
  ObjectName:=OBJ_BSC;

  SetFont(DefaultStyle.MIStyle, MI_SYMBOL_STAR, MI_Map_symbols_FONT, clLime);

end;

constructor TOptionsLinkRepeater.Create;
begin
  inherited;
  ObjectName:=OBJ_Link_Repeater;

  SetFont(DefaultStyle.MIStyle, MI_SYMBOL_STAR, MI_Map_symbols_FONT, clLime);

end;




function TOptionsParameterItem.Compare(aDataset: TDataset): Boolean;
begin
  if Assigned(aDataset.FindField(FieldName)) then
    Result:=True;
end;


function TOptionsParameterItem.Caption: string;
begin
  Result := FieldCaption+' '+Condition+' '+FieldValue;
  assert( Result<>'');
end;


procedure TOptionsParameterItem.LoadFromXmlNode(aNode: IXMLNode);
begin
  if VarIsNull(aNode) then
    Exit;

  Checked:=AsBoolean(xml_GetStrAttr(aNode, 'Checked'));

  FieldCaption:=xml_GetStrAttr(aNode, 'FieldCaption');
  FieldName   :=xml_GetStrAttr(aNode, 'FieldName');
  Condition   :=xml_GetStrAttr(aNode, 'Condition');

  FieldValue  :=xml_GetStrAttr(aNode,   'FieldValue');
//  FieldMinValue :=xml_GetStringAttr(aNode, 'FieldMinValue');
//  FieldMaxValue :=xml_GetStringAttr(aNode, 'FieldMaxValue');


  mapx_Style_LoadFromXmlNode(aNode, Style.MIStyle);

end;


procedure TOptionsParameterItem.SaveToXmlNode(aNode: IXMLNode);
begin
 // xml_SetAttr(aNode, 'FieldName',  FieldName);
// xml_SetAttr(aNode, 'FieldValue', FieldValue);

    xml_SetAttribs(aNode,
         [
          xml_Att('Checked',        Checked),
          xml_Att('FieldCaption',   FieldCaption),
          xml_Att('FieldName',      FieldName),
          xml_Att('Condition',      Condition),

          xml_Att('FieldValue',     FieldValue)

//          xml_Att('FieldMinValue',  FieldMinValue),
//          xml_Att('FieldMaxValue',  FieldMaxValue)

          ]);

  mapx_Style_SaveToXmlNode(aNode, Style.MIStyle);

end;


//-------------------------------------------------------------------
procedure mapx_Style_SaveToXmlNode(aNode: IXMLNode; var aMIStyle: TmiStyleRec);
//-------------------------------------------------------------------
begin
  if VarIsnull(aNode) then
    Exit;


  with aMIStyle do
  begin

    xml_SetAttribs(aNode,
         [
          xml_Att('FontName',  FontName),
          xml_Att('Character', Character),
          xml_Att('FontColor', FontColor),
          xml_Att('FontSize',  FontSize),

          xml_Att('LineStyle', LineStyle),
          xml_Att('LineColor', LineColor),
          xml_Att('LineWidth', LineWidth),

          xml_Att('RegionBorderWidth', RegionBorderWidth),
          xml_Att('RegionBorderStyle', RegionBorderStyle),
          xml_Att('RegionBorderColor', RegionBorderColor),

          xml_Att('RegionPattern',          RegionPattern),
          xml_Att('RegionForegroundColor',  RegionForegroundColor),
          xml_Att('RegionBackgroundColor',  RegionBackgroundColor),
          xml_Att('RegionIsTransparent',    RegionIsTransparent)

         ]);
  end;
end;


//-------------------------------------------------------------------
procedure mapx_Style_LoadFromXmlNode(aNode: IXMLNode; var aMIStyle: TmiStyleRec);
//-------------------------------------------------------------------
begin
  if VarIsnull(aNode) then
    Exit;


  with aMIStyle do
  begin
    FontName  :=xml_GetAttr   (aNode,'FontName',  FontName );
    Character :=xml_GetIntAttr(aNode,'Character', Character);
    FontColor :=xml_GetIntAttr(aNode,'FontColor', FontColor);
    FontSize  :=xml_GetIntAttr(aNode,'FontSize',  FontSize );

    LineStyle := xml_GetIntAttr(aNode,'LineStyle',LineStyle);
    LineColor := xml_GetIntAttr(aNode,'LineColor',LineColor);
    LineWidth := xml_GetIntAttr(aNode,'LineWidth',LineWidth);

    RegionBorderWidth    := xml_GetIntAttr(aNode,'RegionBorderWidth', RegionBorderWidth);
    RegionBorderStyle    := xml_GetIntAttr(aNode,'RegionBorderStyle', RegionBorderStyle);
    RegionBorderColor    := xml_GetIntAttr(aNode,'RegionBorderColor', RegionBorderColor);

    RegionPattern        := xml_GetIntAttr(aNode,'RegionPattern',         RegionPattern);
    RegionForegroundColor:= xml_GetIntAttr(aNode,'RegionForegroundColor', RegionForegroundColor);
    RegionBackgroundColor:= xml_GetIntAttr(aNode,'RegionBackgroundColor', RegionBackgroundColor);
    RegionIsTransparent  := AsBoolean(xml_GetAttr(aNode,'RegionIsTransparent'));


  end;
end;


function TOptionsObjectItemList.GetItems(Index: Integer): TOptionsObjectItem;
begin
  Result := TOptionsObjectItem(inherited Items[Index]);
end;


function TOptionsObjectItem.GetStyle(aRecordset: _Recordset):
    TOptionsObjectStyleRec;
begin
  result:=DefaultStyle;


{  if Parameters.Count>0 then
    Result:=Parameters[0].Style;
}


//zz    Parameters.GetStyle(aDataset, result);

  Result.DrawType:=DrawType;

end;



//-------------------------------------------------------------------
function TOptionsParameterList.GetStyle(aDataset: TDataset; var aStyle:
    TOptionsObjectStyleRec): Boolean;
//-------------------------------------------------------------------
var
  I: integer;
begin
  Result:=False;

  for I := 0 to Count - 1 do
    if Assigned(aDataset.FindField(Items[i].FieldName)) then
    begin
      if (Items[i].Condition='=') then
      begin
        if Items[i].FieldValue = aDataset.FieldValues[Items[i].FieldName] then
          begin
            aStyle := Items[i].Style;  Exit;
          end;
      end;
    end;


end;



//-------------------------------------------------------------------
function TOptionsObjectItemList.IsActiveParam(aObjName, aFieldName: string): boolean;
//-------------------------------------------------------------------
var
  I: integer;
  oItem: TOptionsObjectItem;
begin
  oItem:=ItemsByName[aObjName];

  Result := False;

  for I := 0 to oItem.Parameters.Count - 1 do
    if Eq(oItem.Parameters[i].FieldName, aFieldName) then
    begin
      Result:=True;
      Exit;
    end;

end;


constructor TOptionsAntenna.Create;
begin
  inherited;

//  SetLine (DefaultStyle.MIStyle, MI_PEN_PATTERN_ARROW, clGreen, 1 );

  DefaultStyle.LineLength_M:=DEF_ANTENNA_LENGTH_M;
  DrawType:=dtAsVector;
//  DrawType:=dtAsSymbol;
  IsAntenna := True;

end;

constructor TOptionsPmpCalcRegion.Create;
begin
  inherited;
  ObjectName:=OBJ_PMP_CALC_REGION;


  DefaultStyle.MIStyle.RegionBorderColor:=clRed;
  DefaultStyle.MIStyle.RegionBorderWidth:=2;
  DefaultStyle.MIStyle.RegionBorderStyle:=2;
  DefaultStyle.MIStyle.RegionBackgroundColor   := clFuchsia;
//  DefaultStyle.Region Color      :=12615680; //����-���������
  DefaultStyle.MIStyle.RegionPattern    :=0;
  DefaultStyle.MIStyle.RegionIsTransparent:=True;

  DrawType:=dtAsPolygon;

end;

//-------------------------------------------------------------------
constructor TOptionsLinkPmp.Create;
//-------------------------------------------------------------------
begin
  inherited;
  ObjectName:=OBJ_link_PMP;

  SetLine (DefaultStyle.MIStyle, 2, clNavy, 1 );

  DrawType:=dtAsVector;  //for GRID
end;



end.




