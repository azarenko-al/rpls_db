unit d_ChangeCoordSystem;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, rxPlacemnt, ActnList, cxPropertiesStore,

  d_Wizard,
  u_Geo
  ;


type
  Tdlg_ChangeCoordSystem = class(Tdlg_Wizard)
    Panel1: TPanel;
    rg_CoordSystem: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);

  public
    class function ExecDlg(var aCoordSys: Integer): Boolean;
  end;


//=================================================================
implementation {$R *.DFM}
//=================================================================


//-----------------------------------------------------------------
class function Tdlg_ChangeCoordSystem.ExecDlg(var aCoordSys: Integer): Boolean;
//-----------------------------------------------------------------
var
  ind: integer;
begin
  with Tdlg_ChangeCoordSystem.Create(Application) do
  begin
//    rg_CoordSystem.Items.IndexOfObject()

    ind := rg_CoordSystem.Items.IndexOfObject(Pointer(aCoordSys));
    if ind>=0 then
      rg_CoordSystem.ItemIndex :=ind;

(*    case aCoordSys of    //
      EK_KRASOVSKY42:  rg_CoordSystem.ItemIndex:= 0;
      EK_CK95       :  rg_CoordSystem.ItemIndex:= 1;
      EK_WGS84:        rg_CoordSystem.ItemIndex:= 2;
    else
      raise Exception.Create('');
    end;
*)
  // z Result :=ShowModal=mrOk;

    Result := ShowModal=mrOk;

    if Result then
      aCoordSys := Integer( rg_CoordSystem.Items.Objects[rg_CoordSystem.ItemIndex]);

(*      case rg_CoordSystem.ItemIndex of    //
        0: aCoordSys := EK_KRASOVSKY42;
        1: aCoordSys := EK_CK95;
        1: aCoordSys := EK_WGS84;
      else
        raise Exception.Create('');
      end ;
*)
    Free;
  end;
end;


//-----------------------------------------------------------------
procedure Tdlg_ChangeCoordSystem.FormCreate(Sender: TObject);
//-----------------------------------------------------------------
begin
  inherited;

  Caption:= '����� ������� ���������';
  lb_Action.Caption:= '����� ������� ���������';

  SetDefaultSize;

//  Height:= 385;
//  Width:=  505;

  rg_CoordSystem.Items.Clear;
  rg_CoordSystem.Items.AddObject(DEF_GEO_STR_CK42,  Pointer(EK_CK_42));
  rg_CoordSystem.Items.AddObject(DEF_GEO_STR_CK95,  Pointer(EK_CK_95));
  rg_CoordSystem.Items.AddObject(DEF_GEO_STR_GCK_2011, Pointer(EK_GCK_2011));
  rg_CoordSystem.Items.AddObject(DEF_GEO_STR_WGS84, Pointer(EK_WGS84));

(*  rg_CoordSystem.Items[0]:=DEF_GEO_STR_KRASOVSKY42;
  rg_CoordSystem.Items[1]:=DEF_GEO_STR_WGS84;
*)
end;


//-----------------------------------------------------------------
procedure Tdlg_ChangeCoordSystem.act_OkExecute(Sender: TObject);
//-----------------------------------------------------------------
begin
  /////////////////
end;


end.

