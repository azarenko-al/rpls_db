unit fr_object;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,DB, dxmdaset, Menus, ActnList, ImgList, Grids, DBGrids,
  cxInplaceContainer, cxTL, cxDBTL, cxTLData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,

  cxContainer, cxGraphics, cxDataStorage, cxDataUtils,
  cxEdit, cxDropDownEdit, cxEditConsts, cxTextEdit, cxButtons, cxImage, cxMemo,


  cxColorComboBox,


    cxGridDBTableView, 



//  cxCanvas,


    d_Symbol_Select,

  I_Options,
  u_Options_classes,

  u_MapX,
  u_func,
  u_db,

  u_const_DB, RxMemDS
 ;

type
  Tframe_Object = class(TForm)
    mem_Criteria_Values1: TdxMemData;
    ds_Criteria_Values: TDataSource;
    img_MIF_LineStyle: TImageList;
    img_MIF_RegionStyle: TImageList;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBBandedTableView1: TcxGridDBBandedTableView;
    col__Tag: TcxGridDBBandedColumn;
    col__caption: TcxGridDBBandedColumn;
    col__symbol_font: TcxGridDBBandedColumn;
    col__symbol_color: TcxGridDBBandedColumn;
    col__symbol_size: TcxGridDBBandedColumn;
    col__symbol_code: TcxGridDBBandedColumn;
    col__vector_color: TcxGridDBBandedColumn;
    col__vector_width: TcxGridDBBandedColumn;
    col__vector_style: TcxGridDBBandedColumn;
    col__vector_length: TcxGridDBBandedColumn;
    col__poly_color: TcxGridDBBandedColumn;
    col__poly_back_style: TcxGridDBBandedColumn;
    col__poly_transparent: TcxGridDBBandedColumn;
    col__poly_back_color: TcxGridDBBandedColumn;
    col__poly_border_color: TcxGridDBBandedColumn;
    col__poly_border_width: TcxGridDBBandedColumn;
    col__poly_border_style: TcxGridDBBandedColumn;
    col__poly_length: TcxGridDBBandedColumn;
    FontDlg: TFontDialog;
    ActionList1: TActionList;
    PopupMenu1: TPopupMenu;
    act_Add: TAction;
    N1: TMenuItem;
    col__symbol_IsFontHalo: TcxGridDBBandedColumn;
    col__symbol_IsFontOpaque: TcxGridDBBandedColumn;
    act_Del: TAction;
    DBGrid1: TDBGrid;
    col__Checked: TcxGridDBBandedColumn;
    procedure act_DelExecute(Sender: TObject);
    procedure cxGrid1DBBandedTableView1CustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo:
        TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure col__symbol_codePropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure col__symbol_fontPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGrid1DBBandedTableView1CellClick(Sender: TcxCustomGridTableView; ACellViewInfo:
        TcxGridTableDataCellViewInfo; AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
    procedure cxGrid1Exit(Sender: TObject);
    procedure mem_Criteria_Values1AfterPost(DataSet: TDataSet);
  private
    FRegPath: string;
    FUpdated: Boolean;

    FDataset: TDataSet;

    FShowType: TShapeDrawType;

    FItem: TOptionsObjectItem;

    FSymbolSelectDlg: TSymbolSelectDialog;

    procedure DoOnChangeSymbol(Sender: TObject);

    procedure SaveRecordToStyle(var aStyle: TOptionsObjectStyleRec);
    procedure AddRecord(aCaption: string; aTag: integer; aParameterItem: TOptionsParameterItem;
                        aStyle: TOptionsObjectStyleRec; aTreeID,aTreeParentID: integer );

    procedure AddDelimiter;

   // procedure Load;

  public
  //  ADOConnection: TADOConnection;

    procedure Reload;
    procedure SetObjectItem(aItem: TOptionsObjectItem);

    procedure SetShowType(aShowType: TShapeDrawType);
  end;

  
var
  frame_Object: Tframe_Object;

implementation
{$R *.dfm}

const
  REGISTRY_COMMON_FORMS  = 'Software\Onega\Common\Forms\';





const
  ACTIVE_FEAT_COLOR = '����';

  FLD_ACTIVE = 'ACTIVE';
//  FLD_SHOWAS = 'SHOWAS';

  FLD_ActiveFeature = 'Criterias';
  FLD_CRITERIAS = FLD_ActiveFeature;

  FEAT_SYMBOL_COLOR       = '����';
  FEAT_SYMBOL_SIZE        = '������';
  FEAT_SYMBOL_CODE       = '������';

  FEAT_VECTOR_COLOR       = FEAT_SYMBOL_COLOR;
  FEAT_VECTOR_WIDTH       = '������ (pix)';
  FEAT_VECTOR_STYLE       = '�����';
  FEAT_VECTOR_LENGTH      = '����� (m)';

  FEAT_POLY_COLOR         = '�������: '+FEAT_SYMBOL_COLOR;
  FEAT_POLY_BACK_STYLE    = '�������: '+FEAT_VECTOR_STYLE;
  FEAT_POLY_BACK_COLOR    = '���: '+FEAT_SYMBOL_COLOR;
  FEAT_POLY_TRANSPARENT   = '������������';
  FEAT_POLY_BORDER_COLOR  = '�������: ����';
  FEAT_POLY_BORDER_STYLE  = '�������: �����';
  FEAT_POLY_BORDER_WIDTH  = '�������: ������ (pix)';
  FEAT_POLY_LENGTH        = FEAT_VECTOR_LENGTH;



  FLD_symbol_font         = 'symbol_font';
  FLD_symbol_color        = 'symbol_color';
  FLD_symbol_CODE         = 'symbol_CODE';
  FLD_symbol_size         = 'symbol_size';
  FLD_symbol_FontIsHalo   = 'symbol_FontIsHalo';
  FLD_symbol_FontIsOpaque = 'symbol_FontIsOpaque';

  FLD_vector_color        = 'vector_color';
  FLD_vector_style        = 'vector_style';
  FLD_vector_width        = 'vector_width';
  FLD_vector_length       = 'vector_length';

  FLD_poly_transparent    = 'poly_transparent';
  FLD_poly_border_width   = 'poly_border_width';
  FLD_poly_length         = 'poly_length';
  FLD_poly_back_color     = 'poly_back_color';
  FLD_poly_color          = 'poly_color';
  FLD_poly_back_style     = 'poly_back_style';
  FLD_poly_border_color   = 'poly_border_color';
  FLD_poly_border_style   = 'poly_border_style';

  FLD_criteria_caption    = 'criteria_caption';

  FLD_OBJECT = 'OBJECT';


  //---------------------------------------------------------.
  TAG_DefaultStyle = 1;
  TAG_SEPARATOR    = 2;
  TAG_ITEM         = 3;



procedure Tframe_Object.act_DelExecute(Sender: TObject);
begin
 //
end;

procedure Tframe_Object.FormDestroy(Sender: TObject);
begin
  cxGrid1DBBandedTableView1.StoreToRegistry(FRegPath+ cxGrid1DBBandedTableView1.Name);
end;


// ---------------------------------------------------------------
procedure Tframe_Object.AddRecord(aCaption: string; aTag: integer;
                                  aParameterItem: TOptionsParameterItem;
                                  aStyle: TOptionsObjectStyleRec;
                                  aTreeID,aTreeParentID: integer
                                  );
// ---------------------------------------------------------------
begin
  FUpdated:=True;

  assert(aCaption<>'');

  with aStyle do
    db_AddRecord_(Fdataset,
      [FLD_CAPTION           , aCaption,
      // db_Par(FLD_NAME              , 'Default'),
       FLD_TAG                 , aTag,
    //   db_Par(FLD_INDEX             , 0),
   //    db_Par(FLD_criteria_caption  , '���'),



       FLD_OBJECT       , Integer(aParameterItem),

       FLD_symbol_font       , MiStyle.FontName,
       FLD_symbol_color      , aStyle.MiStyle.FontColor,

       FLD_symbol_CODE       , Chr(Byte(MiStyle.Character)),
  //     db_Par(FLD_symbol_CODE       , aStyle.Character),

       FLD_symbol_size       ,   MiStyle.FontSize,
       FLD_symbol_FontIsHalo   , MiStyle.FontIsHalo,
       FLD_symbol_FontIsOpaque , MiStyle.FontIsOpaque,

       FLD_vector_color      , MiStyle.LineColor,
       FLD_vector_style      , MiStyle.LineStyle-1,
       FLD_vector_width      , MiStyle.LineWidth,
       FLD_vector_length     , aStyle.LineLength_M,

       FLD_poly_color        , MiStyle.RegionForegroundColor,
       FLD_poly_transparent  , MiStyle.RegionIsTransparent,
       FLD_poly_border_width , MiStyle.RegionBorderWidth,
   //    db_Par(FLD_poly_length       , RegionStyle.Length),
       FLD_poly_back_color   , MiStyle.RegionBackgroundColor,
       FLD_poly_back_style   , MiStyle.RegionPattern-1,
       FLD_poly_border_color , MiStyle.RegionBorderColor,
       FLD_poly_border_style , MiStyle.RegionBorderStyle-1
      ]);

  FUpdated:=False;

end;



procedure Tframe_Object.SaveRecordToStyle(var aStyle: TOptionsObjectStyleRec);
var
  iChar: integer;
  s: string;
begin
  if FUpdated then
    Exit;


  with aStyle do
//    with mem_Criteria_Values do
    with Fdataset do
    begin
    //   Checked:=FieldByName(FLD_vector_Length).AsInteger;

       s:=FieldByName(FLD_symbol_CODE).AsString;
       if s<>'' then iChar:=Byte(s[1]) else iChar:=0;

       LineLength_M:=FieldByName(FLD_vector_Length).AsInteger;


       MIStyle.FontName    := FieldByName(FLD_symbol_font).AsString;
       MIStyle.FontColor   := FieldByName(FLD_symbol_color).AsInteger;
       MIStyle.Character   := iChar;//FieldByName(FLD_symbol_CODE).AsString;
       MIStyle.FontSize    := FieldByName(FLD_symbol_size).AsInteger;
       MIStyle.FontIsHalo  := FieldByName(FLD_symbol_FontIsHalo).AsBoolean;
       MIStyle.FontIsOpaque:= FieldByName(FLD_symbol_FontIsOpaque).AsBoolean;

       MIStyle.LineColor   := FieldByName(FLD_vector_color).AsInteger;
       MIStyle.LineStyle   := FieldByName(FLD_vector_style).AsInteger+1;
       MIStyle.LineWidth   := FieldByName(FLD_vector_width).AsInteger;

       MIStyle.RegionForegroundColor := FieldByName(FLD_poly_color).AsInteger;//   FieldByName(FLD_vector_length     , MIStyle.LineLength; FieldByName(FLD_poly_color).AsInteger;
       MIStyle.RegionIsTransparent   := FieldByName(FLD_poly_transparent).AsBoolean;
       MIStyle.RegionBorderWidth     := FieldByName(FLD_poly_border_width).AsInteger;
       MIStyle.RegionBackgroundColor := FieldByName(FLD_poly_back_color).AsInteger; //    FieldByName(FLD_poly_length       , RegionStyle.Length; FieldByName(FLD_poly_back_color).AsInteger;
       MIStyle.RegionPattern         := FieldByName(FLD_poly_back_style).AsInteger+1;
       MIStyle.RegionBorderColor     := FieldByName(FLD_poly_border_color).AsInteger;
       MIStyle.RegionBorderStyle     := FieldByName(FLD_poly_border_style).AsInteger+1;
    end;

end;


procedure Tframe_Object.AddDelimiter;
begin
//  db_AddRecord(mem_Criteria_Values, [db_Par(FLD_CAPTION, '')   ]);
  db_AddRecord_(Fdataset, [FLD_CAPTION, '' ]);

end;


//--------------------------------------------
procedure Tframe_Object.DoOnChangeSymbol(Sender: TObject);
//--------------------------------------------
begin
{
  db_SetEditState(mem_Criteria_Values);
  mem_Criteria_Values[FLD_symbol_code]:=FDlgSymbolSelect.CharCode;
  mem_Criteria_Values.Post;
 }

//  db_UpdateRecord(mem_Criteria_Values, [db_Par(FLD_symbol_code, FSymbolSelectDlg.CharCode)]);
  db_UpdateRecord(Fdataset, [db_Par(FLD_symbol_code, FSymbolSelectDlg.CharCode)]);


///  db_UpdateRecord(mem_Criteria_Values, [db_Par(FLD_symbol_code, FDlgSymbolSelect.CharCode)]);


{  with grid_Criterias do
  begin
    db_UpdateRecord(mem_Criteria_Values, [db_Par(FLD_symbol_style, FDlgSymbolSelect.CharCode)]);

    FParamValue:= GetCurrentParamValue(FParamIsDefault);

    FParamValue.FontStyle.Character:= ord(FDlgSymbolSelect.CharCode);

    if FParamIsDefault then
      Load;

    RefreshSample; // �������� ������
  end;
}
end;


//-------------------------------------------------------------------
procedure Tframe_Object.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
var
  I: integer;
begin
  col__symbol_font.DataBinding.FieldName  := FLD_symbol_font;
  col__symbol_color.DataBinding.FieldName := FLD_symbol_color;
  col__symbol_code.DataBinding.FieldName  := FLD_symbol_code;
  col__symbol_size.DataBinding.FieldName  := FLD_symbol_size;
  col__symbol_IsFontHalo.DataBinding.FieldName   := FLD_symbol_FontIsHalo;
  col__symbol_IsFontOpaque.DataBinding.FieldName := FLD_symbol_FontIsOpaque;


  col__vector_color.DataBinding.FieldName     := FLD_vector_color;
  col__vector_style.DataBinding.FieldName     := FLD_vector_style;
  col__vector_width.DataBinding.FieldName     := FLD_vector_width;
  col__vector_length.DataBinding.FieldName    := FLD_vector_length;

  col__poly_color.DataBinding.FieldName       := FLD_poly_color;
  col__poly_back_style.DataBinding.FieldName  := FLD_poly_back_style;
  col__poly_length.DataBinding.FieldName      := FLD_poly_length;
  col__poly_transparent.DataBinding.FieldName := FLD_poly_transparent;
  col__poly_back_color.DataBinding.FieldName  := FLD_poly_back_color;
  col__poly_border_width.DataBinding.FieldName:= FLD_poly_border_width ;
  col__poly_border_color.DataBinding.FieldName:= FLD_poly_border_color ;
  col__poly_border_style.DataBinding.FieldName:= FLD_poly_border_style ;

//  col_criteria_caption .FieldName :=  FLD_criteria_caption  ;



  col__symbol_color     .Caption   := FEAT_SYMBOL_COLOR     ;
  col__symbol_code      .Caption   := FEAT_SYMBOL_CODE     ;
  col__symbol_size      .Caption   := FEAT_SYMBOL_SIZE      ;

  col__vector_color     .Caption   := FEAT_VECTOR_COLOR     ;
  col__vector_style     .Caption   := FEAT_VECTOR_STYLE     ;
  col__vector_width     .Caption   := FEAT_VECTOR_WIDTH     ;
  col__vector_length    .Caption   := FEAT_VECTOR_LENGTH    ;

  col__poly_color       .Caption   := FEAT_POLY_COLOR       ;
  col__poly_back_style  .Caption   := FEAT_POLY_BACK_STYLE  ;
  col__poly_transparent .Caption   := FEAT_POLY_TRANSPARENT ;
  col__poly_back_color  .Caption   := FEAT_POLY_BACK_COLOR  ;
  col__poly_border_width.Caption   := FEAT_POLY_BORDER_WIDTH;
  col__poly_border_color.Caption   := FEAT_POLY_BORDER_COLOR;
  col__poly_border_style.Caption   := FEAT_POLY_BORDER_STYLE;
  col__poly_Length      .Caption   := FEAT_POLY_LENGTH      ;



  Fdataset:= mem_Criteria_Values1;
  //Fdataset:= dxMemData1;
 // Fdataset:= RxMemoryData1;


  ds_Criteria_Values.DataSet:=Fdataset;

 // Fdataset.Open;

//  Fdataset.Append;
//  Fdataset.FieldByName (FLD_CAPTION).AsString:= 'dddddda';
//  Fdataset.Post;
//
//

{
 FUpdated := True;

  Fdataset.Append;
  Fdataset.FieldByName (FLD_CAPTION).AsString:= 'dddddda';
  Fdataset.Post;

   FUpdated := False;

   }




//  db_CreateField (mem_Criteria_Values,
  db_CreateField (Fdataset,
        [
         db_Field(FLD_CAPTION           , ftString),
         db_Field(FLD_TAG               , ftInteger),

         db_Field(FLD_OBJECT      ,    ftInteger),

         db_Field(FLD_CHECKED     ,    ftBoolean),


    //     db_Field(FLD_DXTREE_ID      ,    ftInteger),
   //      db_Field(FLD_DXTREE_PARENT_ID ,  ftInteger),

         db_Field(FLD_symbol_font       , ftString),
         db_Field(FLD_symbol_color      , ftInteger),
         db_Field(FLD_symbol_code       , ftString),
         db_Field(FLD_symbol_size       , ftInteger),
         db_Field(FLD_symbol_FontIsHalo , ftBoolean),
         db_Field(FLD_symbol_FontIsOpaque,ftBoolean),


         db_Field(FLD_vector_color      , ftInteger),
         db_Field(FLD_vector_style      , ftInteger),
         db_Field(FLD_vector_width      , ftInteger),
         db_Field(FLD_vector_length     , ftInteger),

         db_Field(FLD_poly_transparent  , ftBoolean),
         db_Field(FLD_poly_border_width , ftInteger),
         db_Field(FLD_poly_length       , ftInteger),
         db_Field(FLD_poly_color        , ftInteger),
         db_Field(FLD_poly_back_color   , ftInteger),
         db_Field(FLD_poly_back_style   , ftInteger),
         db_Field(FLD_poly_border_color , ftInteger),
         db_Field(FLD_poly_border_style , ftInteger)
        ]);

//  mem_Criteria_Values.Open;

  Fdataset.Open;

  FUpdated := True;
  {

  Fdataset.Append;
  Fdataset.FieldByName (FLD_CAPTION).Value:= 'dddddda';
  Fdataset.Post;
   }

  FUpdated := False;


  FRegPath:= REGISTRY_COMMON_FORMS + ClassName +'\1\';

  cxGrid1DBBandedTableView1.RestoreFromRegistry(FRegPath + cxGrid1DBBandedTableView1.Name);
  cxGrid1DBBandedTableView1.OptionsView.GroupByBox:=False;

  col__Tag.Visible := False;
  col__Checked.Visible := False;

  FSymbolSelectDlg:=TSymbolSelectDialog.Create(self);
  FSymbolSelectDlg.OnSymbolSelect:=DoOnChangeSymbol;


  col__vector_length.Visible := False;

 // col__Checked.Visible := False;


  cxGrid1.Align:=alClient;

end;




procedure Tframe_Object.SetObjectItem(aItem: TOptionsObjectItem);
begin
  FItem:=aItem;

  SetShowType (FItem.DrawType);

  Reload();

end;


//-------------------------------------------------------------------
procedure Tframe_Object.Reload;
//-------------------------------------------------------------------
var
  I: integer;
begin
//  db_Clear(mem_Criteria_Values);
  db_Clear(Fdataset);

  AddRecord('�� ���������', TAG_DefaultStyle, nil, FItem.DefaultStyle, 0,0);

  if FItem.Parameters.Count=0 then
    Exit;

//  AddDelimiter();

  with FItem do
    for I := 0 to Parameters.Count - 1 do
    begin
      AddRecord(Parameters[i].Caption, TAG_ITEM, Parameters[i], Parameters[i].Style, 0,0);

    end;

end;


//--------------------------------------------
procedure Tframe_Object.SetShowType(aShowType: TShapeDrawType);
//--------------------------------------------
begin
  FShowType:= aShowType;

  with cxGrid1DBBandedTableView1 do
  begin
    Bands[1].Visible:= false;
    Bands[2].Visible:= false;
    Bands[3].Visible:= false;

    case FShowType of    //
      dtAsSymbol: Bands[1].Visible:= true;
      dtAsVector: Bands[2].Visible:= true;
      dtAsPolygon:Bands[3].Visible:= true;
    end;


    Bands[4].Visible:= FItem.IsAntenna;

  end;

{
  with cxDBTreeList1 do
  begin
    Bands[1].Visible:= false;
    Bands[2].Visible:= false;
    Bands[3].Visible:= false;

    case FShowType of    //
      dtAsSymbol: Bands[1].Visible:= true;
      dtAsVector: Bands[2].Visible:= true;
      dtAsPolygon:Bands[3].Visible:= true;
    end;


    Bands[4].Visible:= FItem.IsAntenna;

  end;
}


{
  with grid_Criterias do
  begin
    Bands[1].Visible:= false;
    Bands[2].Visible:= false;
    Bands[3].Visible:= false;

    case FShowType of    //
      dtAsSymbol: Bands[1].Visible:= true;
      dtAsVector: Bands[2].Visible:= true;
      dtAsPolygon:Bands[3].Visible:= true;
    end;

  end;
}

end;


//-------------------------------------------------------------------
procedure Tframe_Object.col__symbol_codePropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
//-------------------------------------------------------------------
begin
  if Fdataset.FieldByName(fld_TAG).AsInteger=0 then
    Exit;



{  with grid_Criterias do
  begin
    n:= grid_Criterias.FocusedAbsoluteIndex;

    //---------------------------------
    if n=col_symbol_style.Index then
    begin
}

  if FSymbolSelectDlg.Visible then
    FSymbolSelectDlg.Hide else
  begin
    FSymbolSelectDlg.FontName:= Fdataset.FieldByName(fld_symbol_font).AsString;
    FSymbolSelectDlg.CharCode:= Fdataset.FieldByName(fld_symbol_code).AsString[1];
    FSymbolSelectDlg.Show;
  end;

  //  end else

end;


procedure Tframe_Object.cxGrid1DBBandedTableView1CustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
    AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  sFontName: string;
begin
//  if AViewInfo.GridRecord.Values[col__Tag.Index])

//  Exit;

  if (AViewInfo.Item.Index = col__symbol_code.Index) then
  begin
     sFontName:=AsString(AViewInfo.GridRecord.Values[col__symbol_font.Index]);


     ACanvas.Font.Name:=sFontName;

  {  AViewInfo.GridView.
  //  oDefParam:= FParameterList.ParameterByName('default');
 //   if Assigned(oDefParam) then
    begin
      sFontName:= oDefParam.Values[0].FontStyle.FontName;
      AFont.Name:= sFontName; // mem_criteria_values.FieldByName(fld_symbol_font).AsString;
    end;
}

  end;

end;

procedure Tframe_Object.col__symbol_fontPropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
begin
 // s:=FontDlg.Font.Name;
 // db_SetEditState(mem_Criteria_Values);
  db_SetEditState(Fdataset);

  FontDlg.Font.Name := Fdataset.FieldByName(fld_symbol_font).AsString;
  FontDlg.Font.Size := Fdataset.FieldByName(fld_symbol_size).AsInteger;
  FontDlg.Font.Color:= Fdataset.FieldByName(fld_symbol_color).AsInteger;


  if FontDlg.Execute then
  begin
    Fdataset.FieldByName(fld_symbol_font).AsString := FontDlg.Font.Name;
    Fdataset.FieldByName(fld_symbol_size).AsInteger := FontDlg.Font.Size;
    Fdataset.FieldByName(fld_symbol_color).AsInteger := FontDlg.Font.Color;

    Fdataset.Post;

{    db_UpdateRecord (mem_criteria_values,
        [db_Par(fld_symbol_font , FontDlg.Font.Name),
         db_Par(fld_symbol_size , FontDlg.Font.Size),
         db_Par(fld_symbol_color, FontDlg.Font.Color) ]);
}
  end;


end;

procedure Tframe_Object.cxGrid1DBBandedTableView1CellClick(Sender: TcxCustomGridTableView; ACellViewInfo:
    TcxGridTableDataCellViewInfo; AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
//  ACellViewInfo.
end;


procedure Tframe_Object.cxGrid1Exit(Sender: TObject);
begin
  db_PostDataset(Fdataset);
  
end;


procedure Tframe_Object.mem_Criteria_Values1AfterPost(DataSet: TDataSet);
var
  oItem: TOptionsParameterItem;
begin
  if FUpdated then
    Exit;

  FItem.IsModified :=True;

  if DataSet[FLD_TAG]=TAG_DefaultStyle then
    SaveRecordToStyle (FItem.DefaultStyle);

  if DataSet[FLD_TAG]=TAG_ITEM then
  begin
    oItem:=TOptionsParameterItem(DataSet.FieldByName(FLD_OBJECT).AsInteger);
    oItem.Checked:=Fdataset.FieldByName(FLD_CHECKED).AsBoolean;

    SaveRecordToStyle (oItem.Style);
  end;

end;



{
procedure Tframe_Filter.ExtractFilterRec(var aRec: TdmFilterRec);
begin
  with dmFilter_View.qry_FilterFields do
  begin

    arec.FieldName:= FieldByName(FLD_NAME).AsString;
    arec.Value    := FieldByName(FLD_Value1).AsString;
    arec.Value2   := FieldByName(FLD_Value2).AsString;
    arec.Condition:= FieldByName(FLD_Condition).AsString;
  end;

end;
}



end.


{

//--------------------------------------------
procedure Tframe_Object.Load;
//--------------------------------------------

{
  function GetParamCriteriaStr(aParameter: TOptionsParameter): string;
  var
    i: Integer;
  begin
    Result:= '';

    for i := 0 to High(aParameter.Criterias(ShowType)) do
    begin
      Result:= Result + GetParamCriteriaCaption(aParameter.Criterias(ShowType)[i]);
      if i<>High(aParameter.Criterias(ShowType)) then
        Result:= Result + ' | ';
    end;
  end;

}

var
  I  : Integer;
 // oParameter: TOptionsParameter;
begin
{

//  ComboBox1.Clear;

  pn_Top.Visible:= (FParameterList.Count>1);

  FDisableUpdate:= true;

  db_Clear(mem_Criterias);
  db_Clear(mem_Criteria_Values);

  with FParameterList do  begin
    for I:=0 to FParameterList.Count-1 do begin
      oParameter:= Parameters[i];
      if not Eq(oParameter.Caption, 'Default') then
        db_AddRecord(mem_Criterias,
          [db_Par(FLD_NAME,          oParameter.ParameterName),
           db_Par(FLD_CAPTION,       oParameter.Caption),
           db_Par(FLD_ACTIVE,        oParameter.Active),
           db_Par(FLD_ActiveFeature, GetParamCriteriaStr(oParameter))
          ])
      else begin
        with oParameter.Values[i] do
          db_AddRecord(mem_Criteria_Values,
            [db_Par(FLD_CAPTION           , '�� ���������'),
             db_Par(FLD_NAME              , 'Default'),
             db_Par(FLD_INDEX             , 0),
             db_Par(FLD_criteria_caption  , '���'),

             db_Par(FLD_symbol_font       , FontStyle.FontName),
             db_Par(FLD_symbol_color      , FontStyle.Color),
             db_Par(FLD_symbol_style      , Chr(Byte(FontStyle.Character))),
             db_Par(FLD_symbol_size       , FontStyle.Size),

             db_Par(FLD_vector_color      , LineStyle.Color),
             db_Par(FLD_vector_style      , LineStyle.Style),
             db_Par(FLD_vector_width      , LineStyle.Width),
             db_Par(FLD_vector_length     , LineStyle.Length),

             db_Par(FLD_poly_color        , RegionStyle.Color),
             db_Par(FLD_poly_transparent  , RegionStyle.Transparent),
             db_Par(FLD_poly_border_width , RegionStyle.BorderWidth),
             db_Par(FLD_poly_length       , RegionStyle.Length),
             db_Par(FLD_poly_back_color   , RegionStyle.BackColor),
             db_Par(FLD_poly_back_style   , RegionStyle.FPattern),
             db_Par(FLD_poly_border_color , RegionStyle.BorderColor),
             db_Par(FLD_poly_border_style , RegionStyle.BorderStyle)
            ]);
      end;
    end;
  end;

  mem_Criteria_Values.First;

//  db_View(mem_Criteria_Values);

  // �������� ������
  RefreshSample;

  FDisableUpdate:= false;

}

end;