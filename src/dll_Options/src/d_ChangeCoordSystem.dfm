inherited dlg_ChangeCoordSystem: Tdlg_ChangeCoordSystem
  Left = 1017
  Top = 316
  VertScrollBar.Range = 0
  ActiveControl = rg_CoordSystem
  BorderStyle = bsDialog
  Caption = 'dlg_ChangeCoordSystem'
  ClientHeight = 327
  ClientWidth = 613
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 292
    Width = 613
    inherited Bevel1: TBevel
      Width = 613
    end
    inherited Panel3: TPanel
      Left = 389
      Width = 224
      inherited btn_Cancel: TButton
        Left = 90
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 613
    inherited Bevel2: TBevel
      Width = 613
    end
    inherited pn_Header: TPanel
      Width = 613
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 60
    Width = 613
    Height = 133
    Align = alTop
    BevelOuter = bvNone
    BorderWidth = 5
    Caption = 'Panel1'
    TabOrder = 2
    object rg_CoordSystem: TRadioGroup
      Left = 5
      Top = 5
      Width = 603
      Height = 108
      Align = alTop
      Items.Strings = (
        'CK 42'
        'CK 95'
        'GCK_2011'
        'WGS 1984')
      TabOrder = 0
    end
  end
  inherited ActionList1: TActionList
    Left = 136
  end
  inherited FormStorage1: TFormStorage
    Left = 108
  end
end
