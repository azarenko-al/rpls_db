unit d_Symbol_Select;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, ActnList, ExtCtrls,  ComCtrls;

type
  TOnSymbolOverEvent = procedure(Sender: TObject; Symbol: Char) of object;

  TSymbolSelectDialog = class(TForm)
    StringGrid1: TStringGrid;
    StatusBar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);

    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

   
    procedure StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
  private
    FCharCode: Char;
    FRowCount,
    FColCount: Integer;

  //  OnSymbolOver  : TOnSymbolOverEvent;


    procedure SetSelectedSymbol(const Value: Char);
    procedure SetFontName(const Value: String);
  //  procedure Paint; override;

  public
    OnSymbolSelect: TNotifyEvent;



    property FontName: String write SetFontName;
    property CharCode: Char read FCharCode write SetSelectedSymbol;

  end;


//============================================================================//
//============================================================================//
implementation  {$R *.DFM}


procedure TSymbolSelectDialog.FormCreate(Sender: TObject);
begin
  Caption:='����� �������';
  FRowCount:=14;
  FColCount:=16;

  StringGrid1.Align:=alClient;
end;


//-------------------------------------------------------------------
//  ���������������� �� ������������ �������
//-------------------------------------------------------------------
procedure TSymbolSelectDialog.SetSelectedSymbol(const Value: Char);
//-------------------------------------------------------------------
var
  I: Integer;
begin
  FCharCode:=Value;
  if FCharCode<' ' then
    FCharCode:=' ';
    
  with StringGrid1 do
  begin
    I  :=(ord(FCharCode)-32) div FColCount; // ������
    Col:=(ord(FCharCode)-32) mod FColCount; // �������
    if (I>RowCount-1) then Row:=0 else Row:=I;
  end;
end;


procedure TSymbolSelectDialog.FormDeactivate(Sender: TObject);
begin
  Hide;
end;

//-------------------------------------------------------------------
// ���������� ����� ���������
//-------------------------------------------------------------------
procedure TSymbolSelectDialog.SetFontName(const Value: String);
var
  Sym,r,c: Integer;
begin
 // Canvas.Font.Name:=Value;
//  Canvas.Font.Size:=18;

  StringGrid1.Font.Name := Value;
  StringGrid1.Font.Size := 18;
  Sym:=32;

  with StringGrid1 do
    for r := 0 to RowCount - 1 do
      for c := 0 to ColCount - 1 do
      begin
        Cells[c, r]:=Char(Sym);
        inc(Sym);
      end;
end;



procedure TSymbolSelectDialog.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key=VK_ESCAPE then
    Hide else
end;


procedure TSymbolSelectDialog.StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  FCharCode:=Chr(32+aRow*FColCount+aCol);
  Hide;

  if Assigned(OnSymbolSelect) then
    OnSymbolSelect(Self);
end;


end.
