object SymbolSelectDialog: TSymbolSelectDialog
  Left = 768
  Top = 334
  Width = 572
  Height = 423
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'dlg_Font_Select_Char'
  Color = clBtnFace
  Constraints.MinHeight = 100
  Constraints.MinWidth = 100
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = True
  Scaled = False
  OnCreate = FormCreate
  OnDeactivate = FormDeactivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object StringGrid1: TStringGrid
    Left = 0
    Top = 0
    Width = 564
    Height = 267
    Align = alTop
    ColCount = 16
    DefaultColWidth = 32
    DefaultRowHeight = 32
    FixedCols = 0
    RowCount = 14
    FixedRows = 0
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnSelectCell = StringGrid1SelectCell
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 377
    Width = 564
    Height = 19
    Panels = <>
  end
end
