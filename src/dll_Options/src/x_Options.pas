unit x_Options;

interface

uses  ADODB,DB, SysUtils, Dialogs, Forms, ADOInt,

   u_vars,

   I_Options,

   u_Options_classes,
   u_Options,

   d_Options,
   d_ChangeCoordSystem,

   u_types,

   u_files
   ;



type
//  TOptionsX = class(TInterfacedObject, IOptionsX)
  TOptionsX = class
  private
/////////////    FOldHandle: Integer;

//    FAppHandle: Integer;

//    FOldHandle : Integer;
    Options: TOptions11;

//    procedure InitAppHandle(aAppHandle: Integer); stdcall;
    function IsActiveParam(aObjName, aFieldName: WideString): boolean; stdcall;

  public
    constructor Create;
    destructor Destroy; override;

    function GetGeoCoord: Integer; stdcall;


    //aADOConnection: TADOConnection;
    function ExecDlg(var aObjectSet: TrpObjectTypeSet): Boolean; stdcall;

    function ExecDlg_new: boolean; stdcall;


//    function ExecDlg(var aArr: TrpObjectTypeArr): Boolean; stdcall;

    function Dlg_ChangeCoordSystem: Boolean;stdcall;

    procedure LoadOptionsXML; stdcall;
    procedure LoadFromXML(aFileName: WideString); stdcall;
    procedure SaveToXML(aFileName: WideString); stdcall;

    function GetDefaultStyle(aObjName: WideString): TOptionsObjectStyleRec; stdcall;

    function GetParamByIndex(aObjName: WideString; aIndex: Integer):
        TOptionsObjectStyleRec; stdcall;

    function GetParamCount(aObjName: WideString): Integer; stdcall;

// TODO: GetStyle
//  function GetStyle(aObjName: WideString; aDataset: TDataset): TOptionsObjectStyleRec;
//      stdcall;
// TODO: GetStyle

    function GetStyle(aObjName: WideString; aRecordset: _Recordset):
      TOptionsObjectStyleRec; stdcall;

    property GeoCoord: Integer read GetGeoCoord;
  end;



 // function DllGetInterface(const IID: TGUID; var Obj): HResult; stdcall;

function IOptions: TOptionsX;


function IOptionsX_Load: Boolean;
procedure IOptionsX_UnLoad;

(*
exports
  DllGetInterface;*)

(*
var
  dmOptionsX: TOptionsX;
*)

implementation

uses Classes;

var
  FOptionsX: TOptionsX;


    {
function DllGetInterface(const IID: TGUID; var Obj): HResult; stdcall;
begin
  Result:=S_FALSE; Integer(Obj):=0;

  if IsEqualGUID(IID, IOptionsX) then
    with TOptionsX.Create do
      if GetInterface(IID,OBJ) then Result:=S_OK
end;

    }

constructor TOptionsX.Create;
begin
  inherited;
  Options:=TOptions11.Create;

  Options.LoadFromXML('');

//  FOldHandle := Application.Handle;

end;


destructor TOptionsX.Destroy;
begin
  FreeAndNil(Options);

(*
  if FAppHandle>0 then
    Application.Handle := 0;
*)

//    Application.Handle := FAppHandle;

//  Options.Free;
  inherited;
end;

// ---------------------------------------------------------------
function TOptionsX.Dlg_ChangeCoordSystem: Boolean;
// ---------------------------------------------------------------
var i: integer;
begin
////////  Assert(Application.Handle>0, 'Application.Handle =0');

  i:=Options.GeoCoord;

  if TDlg_ChangeCoordSystem.ExecDlg(i) then
  begin
    Options.GeoCoord:=i;
    Options.SaveToXML();
  end;
end;


//var aArr: TrpObjectTypeArr
function TOptionsX.ExecDlg(var aObjectSet: TrpObjectTypeSet): Boolean;
var
  I: Integer;
  aArr: TrpObjectTypeArr;
begin
  aSSERT(Assigned(Application));

///////////  Assert(Application.Handle>0, 'Application.Handle =0');

 // FOldHandle:=Application.Handle;
 // Application.Handle := aAppHandle;


  Result := Tdlg_Options.ExecDlg(Options, aArr);

  aObjectSet :=[];

  for I := 0 to High(aArr) do
    aObjectSet :=aObjectSet + [aArr[i]];

  //nil,
  //aADOConnection,


end;


function TOptionsX.ExecDlg_new: boolean;
var
  I: Integer;
  aArr: TrpObjectTypeArr;
begin
  aSSERT(Assigned(Application));


//////////  Assert(Application.Handle>0, 'Application.Handle =0');

 // FOldHandle:=Application.Handle;
 // Application.Handle := aAppHandle;


  Result := Tdlg_Options.ExecDlg(Options, aArr);

end;




function TOptionsX.GetDefaultStyle(aObjName: WideString): TOptionsObjectStyleRec;
begin
  fillChar(Result, SizeOf(Result), 0);

  if Assigned(Options.Objects.ItemsByName[aObjName]) then
    Result:=Options.Objects.ItemsByName[aObjName].DefaultStyle;
end;


function TOptionsX.GetGeoCoord: Integer;
begin
  Result := Options.GeoCoord;;
end;

(*
procedure TOptionsX.InitAppHandle(aAppHandle: Integer);
begin

//  FAppHandle :=aAppHandle;

/////////  Application.Handle := aAppHandle;

end;
*)

function TOptionsX.IsActiveParam(aObjName, aFieldName: WideString): boolean;
begin
  Result := True ;
end;


procedure TOptionsX.LoadOptionsXML;
var
  sDir: string;
begin
 // g_ApplicationDataDir
//  ApplicationName
 // sDir:=GetCommonApplicationDataDir_(ApplicationName);
  sDir:=g_ApplicationDataDir;

  Options.LoadFromXML(sDir+'options.xml');

end;


procedure TOptionsX.LoadFromXML(aFileName: WideString);
begin
  Options.LoadFromXML(aFileName);
end;

procedure TOptionsX.SaveToXML(aFileName: WideString);
begin
  Options.SaveToXML(aFileName);
end;


 //TODO: GetStyle_111111111111111
// ---------------------------------------------------------------
function TOptionsX.GetStyle(aObjName: WideString; aRecordset: _Recordset):
    TOptionsObjectStyleRec;
// ---------------------------------------------------------------
var
  oObjectItem: TOptionsObjectItem;
begin
  fillChar(Result, SizeOf(Result), 0);

  oObjectItem := Options.Objects.ItemsByName[aObjName];

  if Assigned(oObjectItem) then
    Result := oObjectItem.GetStyle(aRecordset);

end;

// ---------------------------------------------------------------
function TOptionsX.GetParamCount(aObjName: WideString): Integer;
// ---------------------------------------------------------------
var
  oObjectItem: TOptionsObjectItem ;
begin
  oObjectItem:=Options.Objects.ItemsByName[aObjName];

  if Assigned(oObjectItem) then
    Result:=oObjectItem.Parameters.Count
  else
    raise Exception.Create('');
end;



function TOptionsX.GetParamByIndex(aObjName: WideString; aIndex: Integer):
    TOptionsObjectStyleRec;
var
  I: Integer;
  oObjectItem: TOptionsObjectItem ;
begin
  oObjectItem:=Options.Objects.ItemsByName[aObjName];

  if Assigned(oObjectItem) then
  begin
    Result := oObjectItem.Parameters[aIndex].Style;
    Result.FieldValue := oObjectItem.Parameters[aIndex].FieldValue;

//    Result:=oObjectItem.Parameters.Count

  end;

end;


function IOptions: TOptionsX;
begin
  if not Assigned(FOptionsX) then
    FOptionsX:= TOptionsX.Create;

  Result := FOptionsX;

end;


function IOptionsX_Load: Boolean;
begin
  if not Assigned(FOptionsX) then
    FOptionsX:= TOptionsX.Create;

end;


procedure IOptionsX_UnLoad;
begin
  if Assigned(FOptionsX) then
    FreeAndNil(FOptionsX);
//    FOptionsX:= TOptionsX.Create;
end;



end.




// TODO: GetStyle
//function TOptionsX.GetStyle(aObjName: WideString; aDataset: TDataset):
//  TOptionsObjectStyleRec;
//var
//oObjectItem: TOptionsObjectItem;
//begin
//fillChar(Result, SizeOf(Result), 0);
//
//oObjectItem := Options.Objects.ItemsByName[aObjName];
//
//if Assigned(oObjectItem) then
//  Result := oObjectItem.GetStyle(aDataset); //aRecordset
//
////  else
// //   Result :=
//
//end;


