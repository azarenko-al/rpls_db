unit d_Options;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rxPlacemnt, ActnList, StdCtrls, ExtCtrls,

 ADODB, Db,


  d_Wizard,

  fr_object, 
 

  u_options,
  u_options_classes,

  u_types,
  u_const_str,

  u_func,
  u_reg, cxPropertiesStore, cxControls, cxInplaceContainer, cxTL, StdActns,
  cxPC

  ;

type

  Tdlg_Options = class(Tdlg_Wizard)
    pn_Main      : TPanel;
    btn_Default  : TButton;
    Splitter     : TSplitter;
    act_SaveToFile: TAction;
    act_SetDefault: TAction;
    cxTreeList1: TcxTreeList;
    col_Name1: TcxTreeListColumn;
    col_Page1: TcxTreeListColumn;
    Button1: TButton;
    act_LoadFromFile: TFileOpen;
    cxPageControl1: TcxPageControl;
    page_Blank: TcxTabSheet;
    page_Object: TcxTabSheet;

    procedure act_LoadFromFileAccept(Sender: TObject);
    procedure act_LoadFromFileBeforeExecute(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

// TODO: dxTreeList1ChangeNode
//  procedure dxTreeList1ChangeNode(Sender: TObject; OldNode, Node: TdxTreeListNode);
 //   procedure act_OkExecute(Sender: TObject);
    procedure act_SetDefaultExecute(Sender: TObject);
    procedure cxTreeList1SelectionChanged(Sender: TObject);
  //  procedure cxTreeList1Click(Sender: TObject);
  //  procedure cxTreeList1DataChanged(Sender: TObject);

  private
    FADOConnection: TADOConnection;

    FOptions: TOptions11;
    FFocusedNumber: integer;

    FChandedObjectsArr: array of string;

// TODO: AddPage
////  FPageIndex: integer;
//
//  function AddPage(aParentNode: TdxTreeListNode; aCaption: String; aTag: integer): TdxTreeListNode;
    function AddPage_cx(aParentNode: TcxTreeListNode; aCaption: String; aTag:
        integer): TcxTreeListNode;
    procedure ChangeNode(aNode: TcxTreeListNode);
    procedure ShowPage(aPage: Integer);


    procedure SaveChandedObjects(var aArr: TrpObjectTypeArr);
    procedure InitData;
  public
    // aADOConnection: TADOConnection
//    class function ExecDlg(aADOConnection: TADOConnection;
  //                         aOptions: TOptions1): boolean;

    class function ExecDlg(//aADOConnection: TADOConnection;
                           aOptions: TOptions11;
                           var aArr: TrpObjectTypeArr): boolean;


      //; AProjectID: Integer
  end;


//============================================================================//
// implementation
//============================================================================//
implementation {$R *.DFM}


const
  TAG_Site           = 1;
  TAG_PMP_SITE       = 2;
  TAG_MSC            = 3;
  TAG_BSC            = 4;
  TAG_LinkEnd        = 5;
  TAG_PMPterminal    = 6;
  TAG_Property       = 7;
  TAG_Link           = 8;
//  TAG_CalcRegion     = 9;

  TAG_LinkEnd_ANT    = 10;
  TAG_PmpSector_ANT  = 11;

  TAG_PMP_CalcRegion = 12;
  TAG_Link_repeater  = 13;


procedure Tdlg_Options.act_LoadFromFileAccept(Sender: TObject);
begin
  if Sender=act_LoadFromFile then
  begin
    FOptions.LoadFromXML(act_LoadFromFile.Dialog.FileName);
    ChangeNode(nil);
//    cxTreeList1Change (nil);
  end;
end;

// ---------------------------------------------------------------
procedure Tdlg_Options.act_LoadFromFileBeforeExecute(Sender: TObject);
// ---------------------------------------------------------------
begin
  if Sender=act_SetDefault then
  begin
    FOptions.LoadDefault;

    ChangeNode(nil);
//    cxTreeList1Change (nil);
  end;

//
//procedure Tdlg_Options.act_SetDefaultExecute(Sender: TObject);
//begin
//  FOptions.LoadDefault;



end;


procedure Tdlg_Options.act_OkExecute(Sender: TObject);
begin
  inherited;
end;

(*  TAG_CellAntGroup_Freq_900  = 12;
  TAG_CellAntGroup_Freq_1800 = 13;

*)

//-------------------------------------------------------------------
class function Tdlg_Options.ExecDlg (//aADOConnection: TADOConnection;
                                     aOptions: TOptions11;
                                     var aArr: TrpObjectTypeArr): boolean;
//-------------------------------------------------------------------
var
  i: integer;
begin
  with Tdlg_Options.Create(Application) do
  begin
    FOptions.Assign(aOptions);

   // dxTreeList1.FocusedNumber:=FFocusedNumber;

  //  FADOConnection:=aADOConnection;

    Result := ShowModal=mrOk;

    if Result then
    begin
      aOptions.Assign(FOptions);
      aOptions.SaveToXML();

      SaveChandedObjects(aArr);

      Result := Length(aArr)>0;
    end;

    Free;
  end;


end;

//============================================================================//
//
//============================================================================//
procedure Tdlg_Options.FormCreate(Sender: TObject);
var
  I: integer;
 //oNode,oNode1: TdxTreeListNode;
begin
  inherited ;

  Caption:='���������';

  pn_Main.Align := alClient;

//  cxPageControl1.TabHidden :=true;
  cxPageControl1.Align := alClient;

  CreateChildForm(Tframe_Object, frame_Object, page_Object );


  FOptions:=TOptions11.Create;

  AddComponentProp(cxTreeList1, 'width');
  RestoreFrom;

  cxTreeList1.OptionsView.Headers := False;



 // dxTreeList1.FocusedNumber:=reg_ReadInteger(FregPath,'FocusedNumber',0);
  FFocusedNumber:=reg_ReadInteger(FregPath,'FocusedNumber',0);


  page_Object.TabVisible :=False;
  page_Blank.TabVisible :=False;


  InitData;
end;


// ---------------------------------------------------------------
procedure Tdlg_Options.InitData;
// ---------------------------------------------------------------
var
  I: integer;
 oNode,oNode1: TcxTreeListNode;
begin
  cxTreeList1.OnChange:=nil;


  oNode:=AddPage_cx(nil,'��������� �����',0);
    //SubItem
    AddPage_cx(oNode,DN_FOLDER_PROPERTIES,  TAG_Property);
    AddPage_cx(oNode,DN_FOLDER_PMPTERMINAL, TAG_PMPterminal);

    //---------------------------------------------------------
    oNode1:=AddPage_cx(oNode,  DN_FOLDER_PMP, TAG_PMP_SITE);
            AddPage_cx(oNode1, '�������',     TAG_PmpSector_ANT);

    //-------------------------------

    oNode1:=AddPage_cx(oNode,  DN_FOLDER_LINKENDS, TAG_LinkEnd);
            AddPage_cx(oNode1, '�������',          TAG_LinkEnd_ANT);

    AddPage_cx(oNode,DN_FOLDER_LINKS ,TAG_Link);

//    AddPage(oNode,DN_FOLDER_CALC_REGION ,TAG_CalcRegion);
    AddPage_cx(oNode,DN_FOLDER_CALC_REGION ,TAG_PMP_CalcRegion);

  //  AddPage(DN_FOLDER_CALC_REGION_3G ,0);

    AddPage_cx(oNode,DN_FOLDER_MSC,TAG_MSC);
    AddPage_cx(oNode,DN_FOLDER_BSC,TAG_BSC);

    AddPage_cx(oNode,DN_FOLDER_Link_repeater, TAG_Link_repeater);



 // cxTreeList1.OnChangeNode:=dxTreeList1ChangeNode;

  cxTreeList1.FullExpand;

 //

 // cxTreeList1.OnChange:=cxTreeList1Change;


 // FFocusedNumber

//  dxTreeList1.

 // if FFocusedNumber=0 then
 //   FFocusedNumber:=1;
end;



procedure Tdlg_Options.FormDestroy(Sender: TObject);
begin

 // reg_WriteInteger(FRegPath,'FocusedNumber', cxTreeList1.FocusedNumber);

end;




//============================================================================
function Tdlg_Options.AddPage_cx(aParentNode: TcxTreeListNode; aCaption:
    String; aTag: integer): TcxTreeListNode;
//============================================================================
begin
  if aParentNode=nil then
    Result:=cxTreeList1.Add
  else
    Result:=aParentNode.AddChild;

  Result.Values[col_Name1.ItemIndex]:=aCaption;
  Result.Values[col_Page1.ItemIndex]:=aTag;

end;



//-------------------------------------------------------------------
procedure Tdlg_Options.ShowPage(aPage: Integer);
//-------------------------------------------------------------------
var
  I: Integer;
  oItem: TOptionsObjectItem;
begin

  if aPage>0 then
  begin
   // FPageIndex:=aPage;

    case aPage of
      TAG_Site            : oItem:=FOptions.Site;
      TAG_PMP_SITE        : oItem:=FOptions.PMPSite;
      TAG_MSC             : oItem:=FOptions.MSC;
      TAG_BSC             : oItem:=FOptions.BSC;
      TAG_LinkEnd         : oItem:=FOptions.LinkEnd;
      TAG_PMPterminal     : oItem:=FOptions.PMPterminal;
      TAG_Property        : oItem:=FOptions.Property_;
      TAG_Link            : oItem:=FOptions.Link;

      TAG_PMP_CalcRegion      : oItem:=FOptions.PMPCalcRegion;

      TAG_PmpSector_ANT   : oItem:=FOptions.PMPSectorAnt;
      TAG_LinkEnd_ANT     : oItem:=FOptions.LinkEndAnt;

      TAG_Link_repeater   : oItem:=FOptions.LinkRepeater;

    //  TAG_CellAntGroup_Freq_900:  oItem:=FOptions.CellAntGroup_Freq_900;
    //  TAG_CellAntGroup_Freq_1800: oItem:=FOptions.CellAntGroup_Freq_1800;

    else
      raise Exception.Create('');
    end;

    cxPageControl1.ActivePage:=page_Object;
    frame_Object.SetObjectItem(oItem);
  end
  else
    cxPageControl1.ActivePage:=page_Blank;

end;



procedure Tdlg_Options.act_SetDefaultExecute(Sender: TObject);
begin
  FOptions.LoadDefault;

  ChangeNode(nil);

//  cxTreeList1Change (nil);
         
 // ShowPage(FPageIndex);

//  dxTreeList1ChangeNode(dxTreeList1, nil, dxTreeList1.FocusedNode);

//  frame_Object.Reload;
end;



procedure Tdlg_Options.ChangeNode(aNode: TcxTreeListNode);
//var
//  oNode: TcxTreeListNode;
begin
 // oNode := cxTreeList1.FocusedNode;

  if Assigned(aNode) then
    ShowPage(aNode.Values[col_Page1.ItemIndex]);
end;



procedure Tdlg_Options.cxTreeList1SelectionChanged(Sender: TObject);
begin
  ChangeNode (cxTreeList1.FocusedNode);
end;



procedure Tdlg_Options.SaveChandedObjects(var aArr: TrpObjectTypeArr);
var
  I: integer;
begin
  SetLength (aArr, 0);

  for I := 0 to FOptions.Objects.Count - 1 do
    if FOptions.Objects[i].IsModified then
  begin
    SetLength (aArr, Length(aArr)+1);
    aArr[High(aArr)]:=ObjNameToType(FOptions.Objects[i].ObjectName);
  end;

end;

end.



(*

procedure Tdlg_Options.SaveChandedObjects;
var
  I: integer;
begin
  SetLength (FChandedObjectsArr, 0);

  for I := 0 to FOptions.Objects.Count - 1 do
    if FOptions.Objects[i].IsModified then
  begin
    SetLength (FChandedObjectsArr, Length(FChandedObjectsArr)+1);
    FChandedObjectsArr[High(FChandedObjectsArr)]:=FOptions.Objects[i].ObjectName;
  end;

end;

*)



{
procedure Tdlg_Options.LoadPageDefault;
begin

{
  obj:=TOptions.Create;
  obj.LoadDefault(FADOConnection);

  FPages[FCurrentPage].PagePointer.Load(obj);

  obj.Free;}

end;





function Tdlg_Options.GetCurrentPage: Integer;
//var              
//  info: TRegKeyInfo;
begin
//  Result :=reg_ReadInteger(REGISTRY_OPTIONS,'CurrentPage',0);

{  Result := 0;
  with TRegistry.Create(KEY_READ) do
  begin
    if OpenKey(REGISTRY_OPTIONS,False) then
    begin
      if ValueExists('CurrentPage') then
        Result:=ReadInteger('CurrentPage')
      else Result:=0;
      CloseKey;                    end;

    Free;
end;

procedure Tdlg_Options.PutCurrentPage(APage: Integer);
begin
//  reg_WriteInteger(REGISTRY_OPTIONS,'CurrentPage',aPage);

{  with TRegistry.Create(KEY_WRITE) do
  begin
    if OpenKey(REGISTRY_OPTIONS,True) then
    begin
      WriteInteger('CurrentPage', APage);
      CloseKey;
    end;

    Free;
  end;}
end;





//    CellAntGroup_Freq_900, CellAntGroup_Freq_1800

      //SubItem
//      AddPage('BS_GSM','������� 900','Antenna_GSM_900',
 //                         TFrm_Page_Antenna_GSM_900.Create(Self));
  //    AddPage('BS_GSM','������� 1800','Antenna_GSM_1800',
   //                       TFrm_Page_Antenna_GSM_1800.Create(Self));
    //-------------------------------

{    AddPage(DN_FOLDER_SITE_3G, 0);
      //SubItem
      AddPage('������� 3G','Antenna_3G',
                          TFrm_Page_Antenna_3G.Create(Self));
}
      //SubItem
     // AddPage('RRS','�������','Antenna_RRS',
    //                      TFrm_Page_Antenna_RRS.Create(Self));


     {   AddPage(dxTreeList1,'SYMBOLS',DN_FOLDER_X_CONN,'XCONN',
                        TFrm_Page_XCONN.Create(Self));
}


  {
  AddPage(dxTreeList1,'','������','miscellaneous',nil);
    AddPage(dxTreeList1,'miscellaneous','����� ������� ���������','SYSCOORDS',
                           TFrm_Page_SysCoords.Create(Self));
   }


   {
  fr_Page_Property,
  fr_Page_TC_PMP,
  fr_Page_BS_PMP,
  fr_Page_BS_GSM,
  fr_Page_BS_3G,
  fr_Page_LinkEnd,
  fr_Page_Antenna_RRS,
  fr_Page_Antenna_GSM_900,
  fr_Page_Antenna_GSM_1800,
  fr_Page_Antenna_3G,
  fr_Page_Antenna_PMP,
  fr_Page_Link,
  fr_Page_Calc_Region,
  fr_Page_Calc_Region_3G,
  fr_Page_MSC,
  fr_Page_BSC,
 ///  fr_Page_XCONN,
  fr_Page_SysCoords,


  fr_OptionsBaseForm,

 }


 
{    //������ ��� ���������� �������
    FPages      : Array of TPage;
    FPagesCount : Integer;
    FCurrentPage: Integer;
    FADOConnection : TADOConnection;
    FOptions    : TOptions;
    FChangedPagesArr: TrpObjectTypeArr;

    function  AddPage      (aList: TdxTreeList;
                            aParentNodeName,
                            aMenuCaption,
                            aMenuName: String;
                            aItemPointer: TOptionsBaseForm): Pointer;


    procedure SavePages;
    procedure LoadPages;
    procedure LoadPageDefault;
    function  FindItem (aList: TdxTreeList; aItemName: String): TdxTreeListNode;
    function  GetCurrentPage: Integer;
    procedure PutCurrentPage (APage: Integer);
}



// TODO: AddPage
//// TODO: dxTreeList1ChangeNode
////procedure Tdlg_Options.dxTreeList1ChangeNode(Sender: TObject; OldNode, Node: TdxTreeListNode);
////begin
//// // ShowPage(Node.Values[col_Page.Index]);
////
////end;
//
//
////============================================================================
//function  Tdlg_Options.AddPage(aParentNode: TdxTreeListNode; aCaption: String; aTag: integer): TdxTreeListNode;
////============================================================================
//begin
//
//(*  if aParentNode=nil then
//  Result:=dxTreeList1.Add
//else
//  Result:=aParentNode.AddChild;
//
//Result.Values[col_PageName.Index]:=aCaption;
//Result.Values[col_Page.Index]:=aTag;
//*)
//end;


(*
//-------------------------------------------------------------------
class function Tdlg_Options.ExecDlg (aADOConnection: TADOConnection;
                                     aOptions: TOptions1
                                     ): boolean;
//-------------------------------------------------------------------
var
  i: integer;
begin
  with Tdlg_Options.Create(Application) do
  begin
    FOptions.Assign(aOptions);

    dxTreeList1.FocusedNumber:=FFocusedNumber;

  //  FADOConnection:=aADOConnection;

    if ShowModal=mrOk then
    begin
      aOptions.Assign(FOptions);
      aOptions.SaveToXML();

      SaveChandedObjects;//(aArr);

    //  aArr := FChandedObjectsArr;

      Result := Length(FChandedObjectsArr)>0;
    end;

    Free;
  end;


end;
*)



