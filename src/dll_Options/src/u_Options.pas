unit u_Options;

interface

uses Graphics, Classes, SysUtils, ADODB, DB, Variants, Forms,XMLDoc, XMLIntf,

   u_xml_document,

   u_func,
   u_geo,
   u_vars,
//   u_XML,

   u_Options_classes
   ;

const
  CONST_EK_KRASOVSKY42 = '���������� 1942';
  CONST_EK_WGS84       = 'WGS 1984';


type
  TOptions11 = class
  private
    FXmlFileName: string;
//    procedure SyncXrefFieldsWithOptions (aObjectType: TrpObjectType);
    // ��������, ������� ���������
    FGeoCoord: Integer;

    procedure SetGeoCoord(aValue: Integer);
    function  GetGeoCoord: integer;
  public

  {  Project: record
      DefaultDir: string;
    end;}

    //----------------------------------
    // main objects
    //----------------------------------


    Property_:     TOptionsProperty;
    Site:          TOptionsSite;
    MSC:           TOptionsMSC;
    LinkEnd:       TOptionsLinkEnd;
    BSC:           TOptionsBSC;
    PMPSite:       TOptionsPMPSite;
    PMPterminal:   TOptionsPMPterminal;
    PMPSectorAnt:  TOptionsPmpSectorAntenna;
    PMPCalcRegion: TOptionsPMPCalcRegion;
 //   CalcRegion:    TOptionsCalcRegion;
    Link:          TOptionsLink;
    LinkEndAnt:    TOptionsLinkEndAntenna;
    LinkRepeater:   TOptionsLinkRepeater;





//    CellAntGroup_Freq_900 :   TOptionsGsmAntenna;
//    CellAntGroup_Freq_1800:   TOptionsGsmAntenna;

    Objects: TOptionsObjectItemList;


    constructor Create;
    destructor Destroy; override;

    procedure Assign(AOptions: TOptions11);

    function GetGeoCoordStr: string;

    procedure LoadFromXML(aFileName: string = '');
    procedure SaveToXML(aFileName: string='');

    procedure LoadDefault;

    function IsActiveParam(aObjName, aFieldName: string): boolean;

    property GeoCoord: Integer read FGeoCoord write SetGeoCoord;

  end;



// ===============================================================
//implementation
// ===============================================================
implementation

// ---------------------------------------------------------------
constructor TOptions11.Create;
// ---------------------------------------------------------------
begin
  inherited;

  BSC         :=TOptionsBSC.Create;

//  CalcRegion  :=TOptionsCalcRegion.Create();
  PMPCalcRegion:=TOptionsPMPCalcRegion.Create();

  Link        :=TOptionsLink.Create();
  LinkEnd     :=TOptionsLinkEnd.Create;
  LinkEndAnt  :=TOptionsLinkEndAntenna.Create();
  MSC         :=TOptionsMSC.Create;
  PMPSectorAnt:=TOptionsPmpSectorAntenna.Create();
  PMPSite     :=TOptionsPMPSite.Create;
  PMPterminal :=TOptionsPMPterminal.Create;
  Property_   :=TOptionsProperty.Create;
  Site        :=TOptionsSite.Create;

  LinkRepeater:=TOptionsLinkRepeater.Create;


  //---------------------------------------------------------
  Objects := TOptionsObjectItemList.Create();

  Objects.Add(BSC);
//  Objects.Add(CalcRegion);
  Objects.Add(PmpCalcRegion);
{  Objects.Add(CellAntGroup_Freq_1800);
  Objects.Add(CellAntGroup_Freq_900);
}
  Objects.Add(Link);
  Objects.Add(LinkEnd);
  Objects.Add(LinkEndAnt);
  Objects.Add(MSC);
  Objects.Add(PMPSectorAnt);
  Objects.Add(PMPSite);
  Objects.Add(PMPterminal);
  Objects.Add(Property_);
  Objects.Add(Site);
  Objects.Add(LinkRepeater);


  //  Freq_1800.Length:=300;
 //   Freq_3G  :=  TOptionsAntenna.Create;
 // end;


  GeoCoord:=EK_WGS84;
//  GeoCoord:=EK_KRASOVSKY42;


{      EK_KRASOVSKY42:  rg_CoordSystem.ItemIndex:= 0;
      EK_WGS84:        rg_CoordSystem.ItemIndex:= 1;
}


  FXmlFileName:= g_ApplicationDataDir + 'options.xml';

(*  FXmlFileName:= IncludeTrailingBackslash (ExtractFileDir(Application.ExeName))
                + 'options.xml';
*)

end;


// ---------------------------------------------------------------
destructor TOptions11.Destroy;
// ---------------------------------------------------------------
var I: integer;
begin
  for I := 0 to Objects.Count - 1 do
    Objects[i].Free;

  FreeAndNil(Objects);

  inherited;
end;



function TOptions11.GetGeoCoordStr: string;
begin
  result:=geo_GetCoordSysStr (GeoCoord);

  {case GeoCoord of
    EK_CK_42: Result:=CONST_EK_KRASOVSKY42;
    EK_WGS_84     : Result:=CONST_EK_WGS84;
  end;

}
end;


procedure TOptions11.SetGeoCoord(aValue: Integer);
begin
//  if (aValue in [EK_CK_42,EK_WGS_84]) then
    FGeoCoord:=aValue;
end;




procedure TOptions11.Assign(AOptions: TOptions11);
var
  I: integer;
begin
  for I := 0 to Objects.Count - 1 do
    Objects[i].Assign(AOptions.Objects[i]);

  GeoCoord:=AOptions.GeoCoord;
end;



function TOptions11.GetGeoCoord: Integer;
begin
  Result:=GeoCoord;
end;

function TOptions11.IsActiveParam(aObjName, aFieldName: string): boolean;
begin
  Result := Objects.IsActiveParam(aObjName, aFieldName);
end;


//-------------------------------------------------------------------
procedure TOptions11.LoadFromXML(aFileName: string = '');
//-------------------------------------------------------------------
var
  s: string;
  I: integer;

  oXMLDoc: TXMLDoc;

  vRoot: IXMLNode;
  vNode: IXMLNode;
  vObjects: IXMLNode;
begin
  if (aFileName='') then
    aFileName:=FXmlFileName;


  if not FileExists(aFileName) then
    Exit;
      


  oXMLDoc:=TXMLDoc.Create;
  oXMLDoc.LoadFromFile(aFileName);

  vRoot:=oXMLDoc.DocumentElement;

 // vNode:=xml_GetNodeByPath(vRoot, 'main');
 // if not VarIsNull(vNode) then
 //
  s:=xml_FindNodeAttr(vRoot, 'main','GeoCoord');

  SetGeoCoord(AsInteger(s));
 // xml_GetNodeByPath (vRoot, Objects[i].ObjectName)


  vObjects := xml_FindNode(vRoot, 'Objects');

  if Assigned(vObjects) then
    for I := 0 to Objects.Count - 1 do
    begin
      vNode := xml_FindNode(vObjects, Objects[i].ObjectName);

      if Assigned(vNode) then
        Objects[i].LoadFromXmlNode(vNode);
    end;


  FreeAndNil(oXMLDoc);

//  FXmlFileName:=aFileName;
end;


//-------------------------------------------------------------------
procedure TOptions11.SaveToXML(aFileName: string='');
//-------------------------------------------------------------------
var
  I: Integer;
  oXMLDoc: TXMLDoc;
  vObjects, vRoot,vGroup: IXMLNode;
begin
  if (aFileName='') then
    aFileName:=FXmlFileName;


  oXMLDoc:=TXMLDoc.Create;

 // vRoot:=
  xml_AddNode (oXMLDoc.DocumentElement, 'main', [xml_Att('GeoCoord', GeoCoord)]);

  vObjects:=oXMLDoc.DocumentElement.AddChild('objects');


  for I := 0 to Objects.Count - 1 do
  begin
//    vGroup:=xml_AddNodeTag (vRoot, Objects[i].ObjectName);
    vGroup:=vObjects.AddChild(Objects[i].ObjectName);

    Objects[i].SaveToXmlNode(vGroup);
  end;


  oXMLDoc.SaveToFile(aFileName);

  FreeAndNil(oXMLDoc);

end;



procedure TOptions11.LoadDefault;
var
  oOptions: TOptions11;
begin
  oOptions:=TOptions11.Create;
  Assign(oOptions);

  FreeAndNil(oOptions);

//  oOptions.Free;
end;


begin

end.





