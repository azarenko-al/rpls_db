object frm_Profile: Tfrm_Profile
  Left = 920
  Top = 315
  Width = 740
  Height = 443
  Caption = 'frm_Profile'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poDefault
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 724
    Height = 57
    Align = alTop
    BevelOuter = bvLowered
    Color = clAppWorkSpace
    TabOrder = 0
    Visible = False
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 144
    Top = 4
    object act_Show_AntInfo: TAction
      Caption = 'act_Show_AntInfo'
      Visible = False
      OnExecute = DoAction
    end
    object act_LinkCreate: TAction
      Caption = 'act_LinkCreate'
      OnExecute = DoAction
    end
    object act_Select_Coord: TAction
      Caption = 'act_Specify_Coord'
      OnExecute = DoAction
    end
  end
  object timer_RelProfile: TTimer
    Enabled = False
    Interval = 100
    OnTimer = timer_RelProfileTimer
    Left = 224
    Top = 4
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\Common\Forms\frm_Profile'
    Options = [fpPosition]
    StoredProps.Strings = (
      'ed_Step.Text'
      'ed_H1.Text'
      'ed_H2.Text'
      'ed_Freq_GHz.Text')
    StoredValues = <>
    Left = 40
  end
  object dxBarManager1: TdxBarManager
    AlwaysSaveText = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    LookAndFeel.SkinName = ''
    PopupMenuLinks = <>
    Style = bmsXP
    SunkenBorder = True
    UseSystemFont = True
    Left = 48
    Top = 232
    DockControlHeights = (
      0
      0
      56
      0)
    object dxBarManager1Bar1: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 1097
      FloatTop = 326
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 61
          Visible = True
          ItemName = 'ed_Step'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 134
          Visible = True
          ItemName = 'ed_ClutterModel'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton2'
        end>
      NotDocking = [dsLeft, dsTop, dsRight, dsBottom]
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 2'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 28
      DockingStyle = dsTop
      FloatLeft = 1050
      FloatTop = 286
      FloatClientWidth = 128
      FloatClientHeight = 69
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 42
          Visible = True
          ItemName = 'ed_H1'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 44
          Visible = True
          ItemName = 'ed_H2'
        end
        item
          BeginGroup = True
          UserDefine = [udWidth]
          UserWidth = 50
          Visible = True
          ItemName = 'ed_Freq_GHz'
        end>
      NotDocking = [dsLeft, dsTop, dsRight, dsBottom]
      OneOnRow = True
      Row = 1
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object ed_Step: TdxBarCombo
      Caption = #1064#1072#1075' [m]'
      Category = 0
      Hint = #1064#1072#1075' [m]'
      Visible = ivAlways
      ShowCaption = True
      Width = 100
      Text = '25'
      Items.Strings = (
        '0.1'
        '1'
        '5'
        '10'
        '20'
        '50'
        '100')
      ItemIndex = -1
    end
    object ed_ClutterModel: TcxBarEditItem
      Caption = #1058#1080#1087' '#1084#1077#1089#1090#1085#1086#1089#1090#1080
      Category = 0
      Hint = #1058#1080#1087' '#1084#1077#1089#1090#1085#1086#1089#1090#1080
      Visible = ivAlways
      ShowCaption = True
      Width = 100
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = cxBarEditItem_ClutterPropertiesButtonClick
    end
    object dxBarButton1: TdxBarButton
      Action = act_LinkCreate
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = act_Select_Coord
      Category = 0
    end
    object ed_H1: TdxBarSpinEdit
      Caption = #1042#1099#1089#1086#1090#1072' 1 (m)'
      Category = 0
      Hint = #1042#1099#1089#1086#1090#1072' 1 (m)'
      Visible = ivAlways
      OnChange = ed_H1Change
      ShowCaption = True
      Width = 101
      MaxValue = 99999999.000000000000000000
      Value = 30.000000000000000000
      OnButtonClick = ed_H1ButtonClick
    end
    object ed_H2: TdxBarSpinEdit
      Caption = #1042#1099#1089#1086#1090#1072' 2 (m)'
      Category = 0
      Hint = #1042#1099#1089#1086#1090#1072' 2 (m)'
      Visible = ivAlways
      OnChange = ed_H1Change
      ShowCaption = True
      Width = 101
      MaxValue = 99999999.000000000000000000
      Value = 30.000000000000000000
      OnButtonClick = ed_H1ButtonClick
    end
    object ed_Freq_GHz: TdxBarSpinEdit
      Caption = #1063#1072#1089#1090#1086#1090#1072' (GHz)'
      Category = 0
      Hint = #1063#1072#1089#1090#1086#1090#1072' (GHz)'
      Visible = ivAlways
      OnChange = ed_H1Change
      ShowCaption = True
      Width = 100
      MaxValue = 99999999.000000000000000000
      Value = 7.000000000000000000
      OnButtonClick = ed_H1ButtonClick
    end
  end
end
