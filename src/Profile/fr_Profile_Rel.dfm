object frame_profile_Rel: Tframe_profile_Rel
  Left = 626
  Top = 335
  Width = 738
  Height = 560
  Caption = 'frame_profile_Rel'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 722
    Height = 61
    Align = alTop
    BevelOuter = bvLowered
    Color = clAppWorkSpace
    TabOrder = 0
    Visible = False
  end
  object StatusBar2: TStatusBar
    Left = 0
    Top = 444
    Width = 722
    Height = 19
    Panels = <
      item
        Width = 150
      end
      item
        Width = 50
      end
      item
        Alignment = taCenter
        Width = 50
      end
      item
        Width = 50
      end
      item
        Width = 50
      end>
    SizeGrip = False
    OnResize = StatusBar1Resize
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 463
    Width = 722
    Height = 19
    Panels = <
      item
        Width = 150
      end
      item
        Width = 50
      end
      item
        Width = 50
      end
      item
        Width = 50
      end
      item
        Width = 50
      end>
    SizeGrip = False
    OnResize = StatusBar1Resize
  end
  object statbar_RRS: TStatusBar
    Left = 0
    Top = 482
    Width = 722
    Height = 19
    Panels = <
      item
        Width = 150
      end
      item
        Width = 50
      end>
    SizeGrip = False
    OnResize = StatusBar1Resize
  end
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 61
    Width = 722
    Height = 120
    ActivePage = p_Profile
    Align = alTop
    LookAndFeel.Kind = lfUltraFlat
    PopupMenu = PopupMenu1
    TabOrder = 4
    TabPosition = tpBottom
    OnClick = cxPageControl1Click
    ClientRectBottom = 96
    ClientRectLeft = 4
    ClientRectRight = 718
    ClientRectTop = 4
    object p_Profile: TcxTabSheet
      Caption = #1055#1088#1086#1092#1080#1083#1100
      ImageIndex = 0
    end
    object p_Points: TcxTabSheet
      Caption = #1054#1090#1089#1095#1077#1090#1099
      ImageIndex = 1
    end
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'bmp'
    InitialDir = 'c:\'
    Left = 24
    Top = 4
  end
  object PopupMenu1: TPopupMenu
    Left = 52
    Top = 4
    object SaveAs1: TMenuItem
      Action = act_SaveAs
    end
  end
  object ActionList1: TActionList
    Left = 116
    Top = 4
    object act_SaveAs: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1072#1081#1083'...'
      OnExecute = act_SaveAsExecute
    end
    object act_Print: TAction
    end
    object Action1: TAction
      Caption = 'Action1'
    end
  end
  object MainMenu1: TMainMenu
    Left = 216
    Top = 4
    object menu1: TMenuItem
      Caption = 'menu'
      object N1: TMenuItem
        Action = act_SaveAs
      end
    end
  end
  object cxLookAndFeelController1: TcxLookAndFeelController
    Kind = lfFlat
    NativeStyle = True
    SkinName = 'Office2013LightGray'
    Left = 552
    Top = 248
  end
end
