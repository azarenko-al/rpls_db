unit u_profile_classes;

interface
uses
  u_func,

  u_reflection_points
  ;

type

(*  TRelReflectionPointRecArray = array of record
     Distance_KM: Double;
     Length_KM: Double;
     Radius_KM: Double;
     abs_prosvet_m: double;
  end;
*)

 // TSiteGroupList = class;

 // TProfileViewFormParams =  record

  TProfileViewFormParamsObj =  class
  public
//      IsShowSites: boolean;       // �������� BS ��� ���

    IsShowFrenelZoneTop: boolean;
    IsShowFrenelZoneBottomLeft: boolean;
    IsShowFrenelZoneBottomRight: Boolean;
    IsShowFrenelZoneBottom: boolean;

    IsShowReflectionPoints: boolean;

    //��� ���� �������
    Freq_MHz : double; // ������� ������� //���
    NFrenel  : double; // ������ ���� �������

//      IsShowAntennas: boolean;


  //    SiteGroupList: TSiteGroupList;

    SiteGroups: array of record
                  Distance_KM: Double;       //����� �������

                  //Offset_KM: Double;       //����� �������

                  Site1,
                  Site2:
                      record
                        // RelProfileRef: TrelProfile;


                         AntennaHeights: TDoubleArray;
                       //  DistanceKM: double; //�������� ����� [km]
                         Rel_H: double;// integer;

                         //������
                         AntennaMinH: Double;
                         AntennaMaxH: Double;
                       end;

                  ReflectionPointArr: TrelReflectionPointRecArray;

                end;



    procedure SetSiteGroupCount(aCount: Integer);

   //   constructor Create;
   //   destructor Destroy; override;
//      Reflection: Double;
   end;




implementation


procedure TProfileViewFormParamsObj.SetSiteGroupCount(aCount: Integer);
var
  i: Integer;
begin
  SetLength(SiteGroups, aCount);

  for i:=0 to High(SiteGroups) do
  begin
    SetLength(SiteGroups[i].Site1.AntennaHeights, 1);
    SetLength(SiteGroups[i].Site2.AntennaHeights, 1);
  end;


end;


end.

