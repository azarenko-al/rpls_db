object dlg_Profile_Rel_Grid: Tdlg_Profile_Rel_Grid
  Left = 1178
  Top = 275
  Width = 706
  Height = 475
  Caption = 'dlg_Profile_Rel_Grid'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 698
    Height = 113
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'ToolBar1'
    Color = clAppWorkSpace
    ParentColor = False
    TabOrder = 0
    Visible = False
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 141
    Width = 698
    Height = 109
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      PopupMenu = PopupMenu1
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = ds_Data
      DataController.Filter.MaxValueListCount = 1000
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnSorting = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.Indicator = True
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object col_recno1: TcxGridDBColumn
        DataBinding.FieldName = 'RecNo'
        Options.Editing = False
        Options.Filtering = False
        Width = 35
      end
      object col_Clu_Color: TcxGridDBColumn
        Caption = #1062#1074#1077#1090
        DataBinding.FieldName = 'Clu_Color'
        PropertiesClassName = 'TcxColorComboBoxProperties'
        Properties.CustomColors = <>
        Properties.ReadOnly = True
        Properties.ShowDescriptions = False
        Options.Editing = False
        Options.Filtering = False
        Width = 34
      end
      object col_Dist_M: TcxGridDBColumn
        Caption = 'R [m]'
        DataBinding.FieldName = 'dist_m'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DisplayFormat = ',0'
        Properties.Nullable = False
        Properties.ReadOnly = False
        Options.Editing = False
        Options.Filtering = False
        Width = 46
      end
      object col_Rel_H: TcxGridDBColumn
        Caption = 'H '#1088#1077#1083#1100#1077#1092#1072' [m]'
        DataBinding.FieldName = 'rel_h'
        PropertiesClassName = 'TcxSpinEditProperties'
        Properties.AssignedValues.DisplayFormat = True
        Options.Filtering = False
        Width = 82
      end
      object col_earth_h: TcxGridDBColumn
        Caption = 'H '#1082#1088#1080#1074'. '#1079#1077#1084#1083#1080' [m]'
        DataBinding.FieldName = 'earth_h'
        PropertiesClassName = 'TcxSpinEditProperties'
        Properties.DisplayFormat = ',0.00'
        Properties.ValueType = vtFloat
        Options.Editing = False
        Options.Filtering = False
        Width = 79
      end
      object col_clu_h: TcxGridDBColumn
        Caption = 'H '#1084#1087' [m]'
        DataBinding.FieldName = 'clu_h'
        PropertiesClassName = 'TcxSpinEditProperties'
        Options.Filtering = False
        Width = 63
      end
      object col_Clu_Name: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1084#1087
        DataBinding.FieldName = 'Clu_Name'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 15
        Properties.KeyFieldNames = 'code'
        Properties.ListColumns = <
          item
            FieldName = 'name'
          end>
        Properties.ListOptions.ShowHeader = False
        Options.Filtering = False
        Width = 124
      end
      object col_clu_code: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1084#1087
        DataBinding.FieldName = 'clu_code'
        PropertiesClassName = 'TcxSpinEditProperties'
        Properties.ReadOnly = True
        Options.Editing = False
        Options.Filtering = False
        Width = 57
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 385
    Width = 698
    Height = 62
    Align = alBottom
    DataSource = ds_Clutters
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Visible = False
  end
  object ds_Data: TDataSource
    DataSet = mem_Relief
    Left = 96
    Top = 50
  end
  object mem_Clutters1: TdxMemData
    Indexes = <>
    SortOptions = []
    SortedField = 'dist'
    Left = 167
    Top = 2
    object mem_Clutters1id: TIntegerField
      FieldName = 'code'
    end
    object mem_Clutters1name: TStringField
      FieldName = 'name'
    end
  end
  object ds_Clutters: TDataSource
    DataSet = mem_Clutters1
    Left = 169
    Top = 49
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 19
    Top = 7
    object act_Save: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      OnExecute = act_SaveExecute
    end
    object act_Refresh: TAction
      Caption = 'Refresh'
      OnExecute = act_RefreshExecute
    end
    object act_Update_Rel_H: TAction
      Caption = 'act_Update_Rel_H'
      OnExecute = act_Update_Rel_HExecute
    end
    object act_Update_Clu_H: TAction
      Caption = 'act_Update_Clu_H'
      OnExecute = act_Update_Rel_HExecute
    end
    object act_Update_Clu_Code: TAction
      Caption = 'act_Update_Clu_Code'
      OnExecute = act_Update_Rel_HExecute
    end
  end
  object mem_Relief: TdxMemData
    Indexes = <>
    SortOptions = []
    SortedField = 'dist'
    AfterEdit = mem_Relief_AfterEdit
    AfterDelete = mem_Relief_AfterDelete
    Left = 96
    Top = 3
    object mem_ReliefrecNo: TIntegerField
      DisplayLabel = #8470
      FieldName = 'recNo'
    end
    object mem_Reliefdist: TFloatField
      FieldName = 'dist_m'
    end
    object mem_Reliefrel_h: TFloatField
      FieldName = 'rel_h'
    end
    object mem_Reliefearth_h: TFloatField
      FieldKind = fkCalculated
      FieldName = 'earth_h'
      Calculated = True
    end
    object mem_Reliefclu_h: TFloatField
      FieldName = 'clu_h'
    end
    object mem_Reliefclu_code: TIntegerField
      FieldName = 'clu_code'
    end
    object mem_Reliefclu_name: TStringField
      FieldKind = fkLookup
      FieldName = 'clu_name'
      LookupDataSet = mem_Clutters1
      LookupKeyFields = 'code'
      LookupResultField = 'name'
      KeyFields = 'clu_code'
      Lookup = True
    end
    object mem_Reliefclu_color: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'clu_color'
      Calculated = True
    end
    object mem_Reliefis_modify: TBooleanField
      FieldName = 'is_modify'
    end
    object mem_Reliefindex: TIntegerField
      FieldName = 'index'
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 298
    Top = 4
    object N1: TMenuItem
      Action = act_Save
    end
    object N2: TMenuItem
      Caption = '-'
      Enabled = False
    end
    object actUpdateRelH1: TMenuItem
      Action = act_Update_Rel_H
    end
    object actUpdateCluCode1: TMenuItem
      Action = act_Update_Clu_Code
    end
    object actUpdateCluH1: TMenuItem
      Action = act_Update_Clu_H
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    CanCustomize = False
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    NotDocking = [dsLeft, dsTop, dsRight, dsBottom]
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 232
    Top = 304
    DockControlHeights = (
      0
      0
      28
      0)
    object dxBarManager1Bar1: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 749
      FloatTop = 344
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end>
      NotDocking = [dsLeft, dsTop, dsRight, dsBottom]
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarButton1: TdxBarButton
      Action = act_Save
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = act_Refresh
      Category = 0
    end
  end
end
