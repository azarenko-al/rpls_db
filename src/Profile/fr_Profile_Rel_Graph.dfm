object frame_Profile_Rel_Graph: Tframe_Profile_Rel_Graph
  Left = 761
  Top = 257
  Width = 939
  Height = 555
  Anchors = [akLeft, akTop, akBottom]
  Caption = 'frame_Profile_Rel_Graph'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 923
    Height = 57
    Caption = 'ToolBar1'
    Color = clAppWorkSpace
    EdgeBorders = []
    ParentColor = False
    TabOrder = 0
    Visible = False
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 497
    Width = 923
    Height = 19
    Panels = <
      item
        Width = 120
      end
      item
        Width = 120
      end
      item
        Width = 120
      end
      item
        Width = 160
      end>
  end
  object Chart1: TChart
    Left = 0
    Top = 57
    Width = 300
    Height = 440
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Foot.Visible = False
    Legend.Visible = False
    MarginBottom = 2
    MarginLeft = 0
    MarginRight = 0
    MarginTop = 2
    Title.AdjustFrame = False
    Title.Text.Strings = (
      'TChart')
    Title.Visible = False
    BottomAxis.Automatic = False
    BottomAxis.AutomaticMaximum = False
    BottomAxis.Axis.Width = 1
    BottomAxis.AxisValuesFormat = '#,##0.##'
    BottomAxis.ExactDateTime = False
    BottomAxis.Grid.Color = clSilver
    BottomAxis.Grid.Style = psDashDotDot
    BottomAxis.Grid.SmallDots = True
    BottomAxis.Increment = 3.000000000000000000
    BottomAxis.LabelStyle = talValue
    BottomAxis.Maximum = 25.000000000000000000
    BottomAxis.MinorTickCount = 4
    LeftAxis.Axis.Color = clGray
    LeftAxis.Axis.Width = 1
    LeftAxis.ExactDateTime = False
    LeftAxis.Grid.Color = clSilver
    LeftAxis.Grid.Style = psDashDot
    LeftAxis.Grid.SmallDots = True
    LeftAxis.Grid.Visible = False
    LeftAxis.Increment = 10.000000000000000000
    LeftAxis.LabelsOnAxis = False
    LeftAxis.LabelsSeparation = 0
    LeftAxis.LabelsSize = 25
    LeftAxis.MinorTickCount = 4
    LeftAxis.TickInnerLength = 1
    LeftAxis.TickLength = 2
    LeftAxis.Ticks.Color = 4194368
    LeftAxis.Title.Angle = 0
    LeftAxis.TitleSize = 22
    RightAxis.Automatic = False
    RightAxis.AutomaticMaximum = False
    RightAxis.Axis.Color = 4194368
    RightAxis.Axis.Width = 1
    RightAxis.Grid.Visible = False
    RightAxis.Maximum = 992.500000000000000000
    RightAxis.MinorTickCount = 4
    TopAxis.Axis.Width = 1
    TopAxis.MinorTickLength = 0
    View3D = False
    Align = alLeft
    BevelOuter = bvNone
    Color = clWhite
    PopupMenu = PopupMenu1
    TabOrder = 4
    AutoSize = True
    Constraints.MinWidth = 300
    OnDblClick = Chart1DblClick
    OnMouseMove = Chart1MouseMove
    ColorPaletteIndex = 13
    object Label1: TLabel
      Left = 0
      Top = 0
      Width = 32
      Height = 13
      Caption = 'Label1'
      Visible = False
    end
    object ser_Earth_bottom: TFastLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      SeriesColor = clGray
      LinePen.Color = clGray
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
      Left = 4
    end
    object ser_Earth_area_Top: TAreaSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      SeriesColor = clWhite
      VertAxis = aBothVertAxis
      AreaBrush = bsDiagCross
      AreaChartBrush.Style = bsDiagCross
      AreaChartBrush.Image.Data = {
        07544269746D61707E000000424D7E000000000000003E000000280000001000
        0000100000000100010000000000400000000000000000000000020000000200
        000000000000FFFFFF00EEEE0000FFFF0000BBBB0000FFFF0000EEEE0000FFFF
        0000BBBB0000FFFF0000EEEE0000FFFF0000BBBB0000FFFF0000EEEE0000FFFF
        0000BBBB0000FFFF0000}
      AreaLinesPen.Color = clGray
      AreaLinesPen.Visible = False
      DrawArea = True
      Pointer.InflateMargins = False
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
      Left = 4
    end
    object ser_Earth_top: TFastLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      SeriesColor = clGray
      LinePen.Color = clGray
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
      Left = 4
    end
    object ser_Earth_area_bottom: TAreaSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      SeriesColor = clWhite
      ShowInLegend = False
      AreaLinesPen.Visible = False
      DrawArea = True
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
      Left = 4
    end
    object Series_Earth_lines: TFastLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      LinePen.Color = clRed
      LinePen.Style = psDot
      XValues.Name = 'X'
      XValues.Order = loNone
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object Series_Clu: TAreaSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      AreaLinesPen.Visible = False
      Dark3D = False
      DrawArea = True
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loNone
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object ser_Tower: TLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      SeriesColor = clBlue
      LinePen.Width = 3
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loNone
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object ser_Sites: TPointSeries
      Marks.Arrow.SmallDots = True
      Marks.Arrow.Visible = False
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.SmallDots = True
      Marks.Callout.Arrow.Visible = False
      Marks.BackColor = clWhite
      Marks.Color = clWhite
      Marks.Shadow.Color = 8487297
      Marks.Shadow.Visible = False
      Marks.Visible = True
      SeriesColor = clBlue
      ClickableLine = False
      Pointer.Brush.Color = clRed
      Pointer.HorizSize = 5
      Pointer.InflateMargins = True
      Pointer.Style = psCircle
      Pointer.VertSize = 5
      Pointer.Visible = True
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object ser_Direct: TLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      SeriesColor = clBlack
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object ser_Frenel: TLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loNone
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object ser_Refl: TLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loNone
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object Series_Earth_reflection_2: TFastLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      SeriesColor = clGray
      LinePen.Color = clGray
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
  end
  object pn_status_left: TPanel
    Left = 424
    Top = 104
    Width = 65
    Height = 57
    Caption = 'RX'
    TabOrder = 2
  end
  object pn_status_right: TPanel
    Left = 424
    Top = 184
    Width = 65
    Height = 57
    Caption = 'RX'
    TabOrder = 3
  end
  object SaveDialog_XML: TSaveDialog
    DefaultExt = 'xml'
    InitialDir = 'c:\'
    Left = 162
    Top = 5
  end
  object ActionList1: TActionList
    Left = 12
    Top = 4
    object act_SaveImageAs: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1072#1081#1083'...'
      OnExecute = act_SaveImageAsExecute
    end
    object act_Print: TAction
      Caption = 'act_Print'
    end
    object act_SaveProfileAs: TAction
      Caption = 'Save Profile'
      OnExecute = act_SaveProfileAsExecute
    end
    object act_Magnify: TAction
      AutoCheck = True
      Caption = #1051#1080#1085#1079#1072
      OnExecute = act_MagnifyExecute
    end
    object act_Cross: TAction
      AutoCheck = True
      Caption = #1050#1091#1088#1089#1086#1088
      OnExecute = act_CrossExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 47
    Top = 5
    object N1: TMenuItem
      Action = act_SaveImageAs
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object N3: TMenuItem
      Action = act_Magnify
      AutoCheck = True
    end
    object N4: TMenuItem
      Action = act_Cross
      AutoCheck = True
    end
  end
  object SaveDialog_IMG: TSaveDialog
    DefaultExt = 'bmp'
    InitialDir = 'c:\'
    Left = 304
    Top = 2
  end
end
