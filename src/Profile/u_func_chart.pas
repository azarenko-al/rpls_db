unit u_func_chart;

interface
uses
  Windows, SysUtils, Classes, Graphics, Forms, Controls,
  ActnList, Dialogs, ComCtrls, Series, TeEngine, TeeProcs, Chart,  Math, Menus;




  function DrawCross(aChart: TChart; const aCross: TPoint;
                   aVer,aHor: Boolean; clPen: TColor; aSave: Boolean = True): TPoint;


  function GetSeriesValue(aSer: TChartSeries; aScreenX: Integer): Double;


implementation


//---------------------------------------------------------------------------
function DrawCross(aChart: TChart; const aCross: TPoint;
                   aVer,aHor: Boolean; clPen: TColor; aSave: Boolean = True): TPoint;
//---------------------------------------------------------------------------
begin
  with aChart, Canvas do
  begin
    Pen.Color:= clPen;
    Pen.Style:= psSolid;
    Pen.Mode := pmXor;
    Pen.Width:= 1;

    if aVer then // ������������ �������
      Line(aCross.X, ChartRect.Top-Height3D, aCross.X, ChartRect.Bottom-Height3D);

    if aHor then // �������������� �������
      Line(ChartRect.Left-Width3D, aCross.Y, ChartRect.Right-Width3D, aCross.Y);
  end;

  if aSave then
    Result:= aCross
  else
    Result:= Point(-1,-1);

end;




  function GetSeriesValue(aSer: TChartSeries; aScreenX: Integer): Double;
  var
    iInd: Integer;
    iIndex: Integer;
    PtLf,PtRg : TPoint;
    n: Integer;
  begin
    iIndex:= -1;
    Result:= 0;

//    iInd:=aSer.GetCursorValueIndex();

//    Chart1.G

(*
    iInd:=aSer.GetCursorValueIndex();

    if iInd>=0 then
    begin
      Result := aSer.YValue[iInd];
      Exit;
    end;
*)


    for n:= aSer.FirstValueIndex to aSer.LastValueIndex-1 do //ValueIndex
      if not(aSer.IsNull(n) or aSer.IsNull(n+1)) then
        begin
        // ����� ����� � ������ �� �������
        PtLf:= Point(aSer.CalcXPos(n  ), aSer.CalcYPos(n  ));
        PtRg:= Point(aSer.CalcXPos(n+1), aSer.CalcYPos(n+1));

        // �������� ���������� ������� � ��������� ����� �������
        if (PtLf.X <= aScreenX) and (aScreenX <= PtRg.X) then
          begin
          if (aScreenX = PtRg.X) then                       // ������ �� ������ �������
            Result:= aSer.YValue[n+1]                       // �������� �� ������ �������
          else
            begin
            Result:= aSer.YValue[n];                        // �������� �� ����� �������
            if not ((aScreenX = PtLf.X) or                  // ������ �� �� ����� ������� �
               (PtRg.X = PtLf.X) or (PtRg.Y = PtLf.Y)) then // �� ����������� � �����/����� �������
              Result:= Result + (aScreenX - PtLf.X) / (PtRg.X - PtLf.X) *
                        (aSer.YValue[n+1] - Result);
            end;
          iIndex:= n;



          Break;
          end;
{
        end
      else
        begin
        if (aSer.CalcXPos(n) <= aScreenX) and (aScreenX <= aSer.CalcXPos(n+1)) then
          begin
          iIndex:= n;
          Break;
          end;
}
        end;
  end;





end.
