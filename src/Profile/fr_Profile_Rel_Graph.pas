unit fr_Profile_Rel_Graph;

interface

uses
  Windows, SysUtils, Classes, Graphics, Forms, Controls,
  ActnList, Dialogs, ComCtrls, Series, TeEngine, TeeProcs, Chart,  Math, Menus,

  u_dll_img,
  //  u_img_PNG,

  u_math_reflection,   

  u_func_chart,     

  u_reflection_points,


  TeeMagnifyTool,

  I_rel_Matrix1,
 // I_MapView,
 // I_MapAct,

  u_files,


  u_Func_arrays,
  u_func,
  u_Geo,

  u_rel_Profile,
  u_rel_Profile_tools,

  u_profile_classes,

  ToolWin, ExtCtrls, StdCtrls, TeeTools
  ;


type

  Tframe_Profile_Rel_Graph = class(TForm)
    SaveDialog_XML: TSaveDialog;
    ToolBar1: TToolBar;
    ActionList1: TActionList;
    act_SaveImageAs: TAction;
    act_Print: TAction;
    act_SaveProfileAs: TAction;
    PopupMenu1: TPopupMenu;
    SaveDialog_IMG: TSaveDialog;
    N1: TMenuItem;
    StatusBar1: TStatusBar;
    act_Magnify: TAction;
    act_Cross: TAction;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    pn_status_left: TPanel;
    pn_status_right: TPanel;
    Chart1: TChart;
    Label1: TLabel;
    ser_Earth_bottom: TFastLineSeries;
    ser_Earth_area_Top: TAreaSeries;
    ser_Earth_top: TFastLineSeries;
    ser_Earth_area_bottom: TAreaSeries;
    Series_Earth_lines: TFastLineSeries;
    Series_Clu: TAreaSeries;
    ser_Tower: TLineSeries;
    ser_Sites: TPointSeries;
    ser_Direct: TLineSeries;
    ser_Frenel: TLineSeries;
    ser_Refl: TLineSeries;
    Series_Earth_reflection_2: TFastLineSeries;
    procedure act_CrossExecute(Sender: TObject);
    procedure act_MagnifyExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure act_SaveImageAsExecute(Sender: TObject);
    procedure act_SaveProfileAsExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);


      procedure Chart1MouseMove(Sender: TObject;
          Shift: TShiftState; X, Y: Integer);

//..    procedure Chart1MouseMove(Sender: TObject; Shift: TShiftState; X,
//  ..    Y: Integer);
    procedure Chart1DblClick(Sender: TObject);
    procedure FormResize(Sender: TObject);

  private
    FIsShowSites: Boolean;

    procedure DrawFrenelZone(aMinDistance_km, aLength_km, aNFrenel, aFreq_Mhz, aH1,
        aH2: double; aColor: Integer);

    procedure ShowFrenelZone ();


  private
    FMagnify : TMagnifyTool;
    CrossOld: TPoint;


    FOldX, FOldY: integer;

   // FIMapView: IMapViewX;

    // ��� ������ ���� ������
    FMaxLocalH: Double;
    FMinRelH: Double;
    FMaxRelH: Double;

    procedure BeginUpdateAll;
// TODO: AddFastLineSeries
//  function AddFastLineSeries(aTag: Integer; aLinePenStyle: TPenStyle;
//      aSeriesColor: Integer): TFastLineSeries;


    procedure UpdateMinMaxHeights;
    procedure DrawRelief(aRelProfile: TRelProfile);//; aLenOffset: double);

//    procedure DrawClutters(aRelProfile: TRelProfile);// aLenOffset_KM: double);
    procedure DrawClutters(aRelProfile: TRelProfile);
    procedure DrawEarthLines_new(aMinDistance_km, aLength_km, aRefraction: double);
    procedure EndUpdateAll;
// TODO: ShowSites
//  procedure ShowSites ();

    procedure ShowReflectionPoints_new;

    procedure ShowSites_;
    procedure UpdateChartLeftIncrement;

  protected
    procedure UpdateChartBottomIncrement;
  public
    RelProfile_Ref: TrelProfile;

    Params: TProfileViewFormParamsObj;

    Params_Refraction_2: double;


    procedure Clear;

    procedure ShowGraph;

    procedure SetChartSize(aWidth,aHeight : integer);


    procedure SaveToBmpFile (aFileName: string='');
    procedure SaveToPNG(aFileName: string);
    procedure SetStatusPanelColor(aIndex, aStatus: Integer);

    procedure Hide_TxRx;

  end;


//===================================================================
implementation
 {$R *.DFM}
//===================================================================

const
  DEF_OFFSET = 5;
  DEF_SIZE = 20;


const
  REGISTRY_COMMON_FORMS  = 'Software\Onega\Common\Forms\';


const
  FRENEL_STEP_COUNT = 50; // ���-�� �������� ��� ��������� Frenel
 // REFLECT_SERIES = 1;

  DEFAULT_INCREMENT = 10;

  //����� ��� �������� ���������
  DEF_REFLECTION_COLORS: array [0..9] of integer =
      (clOlive, clGreen, clLime, clYellow, clMaroon, clNavy, clRed, clBlue, clOlive, clTeal);

  //����� ��� �������� FRENEL
  FRENEL_COLORS: array [0..3] of integer =
      (clRed,clFuchsia, clLime, clOlive);

  DEF_WATER_OFFSET = 3; //5;


  REL_BOTTOM_OFFSET=0;
//  REL_TOP_MIN_OFFSET=20; // meters
  REL_TOP_MIN_OFFSET=5; // meters

  MIF_LAYER_PROFILE_REL_POINT = 'PROFILE_REL_POINT';


// -------------------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.FormCreate(Sender: TObject);
// -------------------------------------------------------------------
var i: integer;
 // oFastLineSeries: TFastLineSeries;
begin
  inherited;

  Caption:='�������';

 // Params_Refraction_2:=0.5;

  pn_Status_left.Height:=DEF_SIZE;
  pn_Status_left.Width:=DEF_SIZE;

  pn_status_right.Height:=DEF_SIZE;
  pn_status_right.Width:=DEF_SIZE;


 // ChartTool_Magnify1.Visible := False;


  Series_Clu.LinePen.Visible := False;


  ser_Earth_top.XValues.Order:=loNone;
  ser_Earth_area_top.XValues.Order:=loNone;

  Series_Earth_lines.SeriesColor  :=Chart1.BottomAxis.Ticks.Color;
  Series_Earth_lines.LinePen.Style:=psDot;
  Series_Earth_lines.IgnoreNulls :=False;
  Series_Earth_lines.TreatNulls  :=tnDontPaint;

  ser_Refl.XValues.Order:=loNone;
  ser_Refl.TreatNulls  :=tnDontPaint;


  Series_Earth_reflection_2.XValues.Order:=loNone;
  Series_Earth_reflection_2.SeriesColor  :=clRed;
  Series_Earth_reflection_2.LinePen.Style:=psDot;
  Series_Earth_reflection_2.IgnoreNulls :=False;
  Series_Earth_reflection_2.TreatNulls  :=tnDontPaint;


 // Series_Frenel.

  i:=Chart1.SeriesList.IndexOf(Series_Clu);
  Chart1.SeriesList.Move(i,0);

  i:=Chart1.SeriesList.IndexOf(ser_sites);
  Chart1.SeriesList.Move(i,Chart1.SeriesList.Count-1);

  //!!!!!!
  i:=Chart1.SeriesList.IndexOf(Series_Earth_reflection_2);
  Chart1.SeriesList.Move(i,Chart1.SeriesList.Count-1);




  //  i:=Chart1.SeriesList.IndexOf(ser_Sites1);
//  Chart1.SeriesList.Move(i,0);



//  Move(Series1.);


  Params := TProfileViewFormParamsObj.Create();


 // FIMapView:=IActiveMapView;

  //------------------------------------------------------
 // if assigned(FIMapView) then
  //   FIMapView.TempLayer_Create(MIF_LAYER_PROFILE_REL_POINT);
  //------------------------------------------------------


//  FRegPath:= REGISTRY_COMMON_FORMS + ClassName +'\';

//  RelProfile_Ref:=TrelProfile.Create;


  Chart1.Align:=alCLient;
//  panel1.Align:=alCLient;

  with Chart1 do
  begin
  ////////             AllowZoom:=False;

    LeftAxis.LabelsSize:=33;
    RightAxis.LabelsSize:=33;

    LeftAxis.Automatic:=False;
    RightAxis.Automatic:=False;
    BottomAxis.Automatic:=False;

    LeftAxis.Minimum:=0;
    LeftAxis.Maximum:=0;
    RightAxis.Minimum:=0;
    RightAxis.Maximum:=0;

    BottomAxis.Minimum:=0;

  end;


 // ser_Sites.SeriesColor:=clBlue;

//  Params.FIsShowSites:=true;

  FIsShowSites:=True;

  Params.IsShowReflectionPoints:=False;
  Params.IsShowFrenelZoneTop:=True;

end;


//-----------------------------------------------------
procedure Tframe_Profile_Rel_Graph.FormDestroy(Sender: TObject);
//-----------------------------------------------------
begin
  FreeAndNil(Params);

  inherited;
end;


//-----------------------------------------------------
procedure Tframe_Profile_Rel_Graph.UpdateMinMaxHeights;
//-----------------------------------------------------
var
  eFullDistanceKM: Double;
  I: Integer;
begin
  Assert(Assigned(RelProfile_Ref), 'RelProfile_Ref not assigned');

//  RelProfile_Ref.CalcSummary1();
  TRelProfile_tools.CalcSummary1(RelProfile_Ref);

  FMaxRelH := 0;

  for I := 0 to Length(Params.SiteGroups) - 1 do
 // begin
    FMaxRelH:=Max (FMaxRelH,
                   Max(Params.SiteGroups[i].Site1.Rel_H,
                       Params.SiteGroups[i].Site2.Rel_H));
 // end;

  FMinRelH:=RelProfile_Ref.Summary.MinRelH - REL_BOTTOM_OFFSET;
  FMaxRelH:=Max (RelProfile_Ref.Summary.MaxRelH +  RelProfile_Ref.Summary.MaxEarthH +
                 REL_TOP_MIN_OFFSET,

                 FMaxRelH
//                 Max(Params.SiteGroups[0].Site1.Rel_H,
  //                   Params.SiteGroups[0].Site2.Rel_H)
                 )
                ;


  FMaxLocalH:=0;
  eFullDistanceKM:=0;

  for I := 0 to High(Params.SiteGroups) do
    with Params.SiteGroups[i] do
    begin
      Site1.AntennaMinH:=DoubleArray_MinValue (Site1.AntennaHeights);
      Site1.AntennaMaxH:=DoubleArray_MaxValue (Site1.AntennaHeights);


      Site2.AntennaMinH:=DoubleArray_MinValue (Site2.AntennaHeights);
      Site2.AntennaMaxH:=DoubleArray_MaxValue (Site2.AntennaHeights);

//      Assert(Site1.AntennaMaxH < 1000, 'Value <=0');
 //     Assert(Site2.AntennaMaxH < 1000, 'Value <=0');

      Assert(Distance_KM > 0, 'Distance_KM1 = 0');



      eFullDistanceKM:=eFullDistanceKM + Distance_KM;

      if FIsShowSites then
        FMaxLocalH:=Round(Max(Max(Site1.AntennaMaxH, Site2.AntennaMaxH), FMaxLocalH));
    end;


  FMaxLocalH:=Max(FMaxLocalH, RelProfile_Ref.Summary.MaxClutterH);

  FMaxLocalH:=FMaxLocalH + FMaxLocalH / 3;


  with Chart1 do
  begin
    LeftAxis.Minimum :=-10000;
    LeftAxis.Maximum :=+10000;
    RightAxis.Minimum:=-10000;
    RightAxis.Maximum:=+10000;

    LeftAxis.Maximum:=FMaxRelH + FMaxLocalH;
    LeftAxis.Minimum:=FMinRelH - 4;
//    LeftAxis.Minimum:=FMinRelH;// + 5;


//    LeftAxis.Maximum:=LeftAxis.Maximum+ ((LeftAxis.Maximum-LeftAxis.Minimum)/6);
//    LeftAxis.Minimum:=FMinRelH - 5;


    Chart1.RightAxis.Minimum :=Chart1.LeftAxis.Minimum;
    Chart1.RightAxis.Maximum :=Chart1.LeftAxis.Maximum;

    UpdateChartLeftIncrement();

    BottomAxis.Minimum:=0; //-0.01;
    BottomAxis.Maximum:=eFullDistanceKM;
  end;


//  StatusBar1.SimpleText:=Format('Distance-%f;  BottomAxis.Maximum:%f',
//                      [RelProfile.Data.Distance, Chart1.BottomAxis.Maximum ]);

end;



//----------------------------------------------------
// SERVICE functions
// ---------------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.SaveToBmpFile (aFileName: string='');
// ---------------------------------------------------------------
begin
  if aFileName='' then
    if SaveDialog_IMG.Execute then aFileName:=SaveDialog_IMG.FileName
                              else Exit;

  aFileName:=ChangeFileExt(aFileName, '.bmp');

  ForceDirByFileName (aFileName);
  Chart1.SaveToBitmapFile (aFileName);

end;


procedure Tframe_Profile_Rel_Graph.SaveToPNG(aFileName: string);
begin
  SaveToBmpFile(aFileName);
  img_BMP_to_PNG(aFileName, aFileName);
end;



//---------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.DrawEarthLines_new(aMinDistance_km,
    aLength_km, aRefraction: double);
//---------------------------------------------------------

  function DoGetEarthH(aDist: double): Double;
  begin
    Result:=geo_GetEarthHeight (aDist, aLength_km, aRefraction);
//    Result:=geo_GetEarthHeight (aDist, RelProfile.GeoDistance1(), RelProfile.Refraction);
  end;


var i,j,k,iYStep,iX,

  iY: integer;
  d: Double;
  eDist: Double;
  h: Double;
  iCount: Integer;
  iStep: integer;
 //   y,
  min_y,eStep,fHeight: double;

  oSeries: TFastLineSeries;

const

  STEPS_COUNT = 50; // ���-�� �������� ��� ��������� ���

begin

//  Series_Earth_lines.Active:=True;
//  Result.Active:=False;
  oSeries := Series_Earth_lines;


  eStep:=aLength_km / STEPS_COUNT;

  min_y:=Chart1.LeftAxis.Increment;


  iCount := Round(Chart1.LeftAxis.Maximum / Chart1.LeftAxis.Increment);

  Assert(iCount<10000, 'Value <=0');

  min_y:=0;

  for k:=0 to iCount do
  begin
    h:= min_y + (k*Chart1.LeftAxis.Increment);
    if h < Chart1.LeftAxis.Minimum then
      Continue;

    oSeries.AddNull;

    for i:=0 to STEPS_COUNT do
    begin
      eDist:=eStep*i;
      fHeight:=h + DoGetEarthH(eDist);

      oSeries.AddXY (aMinDistance_km + eDist, fHeight);
    end;

  end;


end;


//--------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.ShowFrenelZone();
//--------------------------------------------------------
var h1,h2, rel_h1,rel_h2: double;
  I: Integer;
  eOffset: Double;
begin
  eOffset:=0;

  for I := 0 to High(Params.SiteGroups) do
    with Params.SiteGroups[i] do
  begin

    if (Length(Site1.AntennaHeights)=0) or (Length(Site2.AntennaHeights)=0) then
      exit;


    Assert(Distance_KM > 0, 'Distance_KM1 = 0');


    if Params.IsShowFrenelZoneTop then
      DrawFrenelZone (eOffset, Distance_KM, Params.NFrenel,
                    Params.Freq_MHz,
                    Site1.Rel_H + Site1.AntennaMaxH,
                    Site2.Rel_H + Site2.AntennaMaxH,
                    FRENEL_COLORS[0]
                    );

    if Params.IsShowFrenelZoneBottomRight then
      if Length(Site1.AntennaHeights)> 1 then
        DrawFrenelZone (eOffset, Distance_KM, Params.NFrenel,
                    Params.Freq_MHz,
                    Site1.Rel_H + Site1.AntennaMinH,
                    Site2.Rel_H + Site2.AntennaMaxH,
                    FRENEL_COLORS[1]
                    );

    if Params.IsShowFrenelZoneBottomLeft then
      if Length(Site2.AntennaHeights)> 1 then
        DrawFrenelZone (eOffset, Distance_KM, Params.NFrenel,
                    Params.Freq_MHz,
                    Site1.Rel_H + Site1.AntennaMaxH,
                    Site2.Rel_H + Site2.AntennaMinH,
                    FRENEL_COLORS[2]);

    if Params.IsShowFrenelZoneBottom then
      if (Length(Site1.AntennaHeights)>1) and (Length(Site2.AntennaHeights)>1) then
        DrawFrenelZone (eOffset, Distance_KM, Params.NFrenel,
                    Params.Freq_MHz,
                    Site1.Rel_H + Site1.AntennaMinH,
                    Site2.Rel_H + Site2.AntennaMinH,
                    FRENEL_COLORS[3]);

    eOffset:=eOffset + Distance_KM;
  end;
end;


//--------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.DrawFrenelZone(aMinDistance_km, aLength_km,
    aNFrenel, aFreq_Mhz, aH1, aH2: double; aColor: Integer);

//--------------------------------------------------------
var
  delta,tang,h,h1,h2,dx,dy: double;
  i:integer;
  k: Integer;
  oSeries: TLineSeries;
begin
  if aNFrenel=0 then
    aNFrenel:=0.3;
                       
  oSeries:=ser_Frenel;

  oSeries.AddNull;
  oSeries.AddXY (aMinDistance_km, aH1);
  oSeries.AddXY (aMinDistance_km + aLength_km, aH2, '', clBlack);


  if (aFreq_Mhz < 0.01) or (aLength_km< 0.001) then
    Exit;

  tang :=(aH2 - aH1) / aLength_km; //������ ����� ��.�����.
  delta:=aLength_km / FRENEL_STEP_COUNT;  //  ������ �������


//  aSeries.tr

 // aSeries.XValues.Order := loNone;
//  aSeries.YValues.Order := loNone;

  ////////////////////////////

  for k := 0 to 1 do
  begin
    oSeries.AddNull;

//
  //  if k=1 then
    //  aSeries.AddNull;


    for i:=0 to FRENEL_STEP_COUNT do
    begin
      dx:= i*delta;
      h:= tang*dx + aH1; //������ ����� ��.�����.


      //���
      //dy:=dx* (1 - dx/(Distance)));
      if (i=0) or (i=FRENEL_STEP_COUNT) then
        dy:=0
      else
        dy:=Sqrt(( 300/aFreq_Mhz )*
            aNFrenel * (1000*dx)* (1 - dx/aLength_km));


      if k=0 then
        h1:=h + dy   //top
      else
        h1:=h - dy;  //bottom


    //  aSeries.AddXY (aMinDistance_km + dx, h1, '', aColor);

      oSeries.AddXY (aMinDistance_km + dx, h1, '', aColor);

    end;
  end;

(*  ser_Direct.Clear;
  aSeries.Clear;
*)
end;


//--------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.ShowSites_;
//--------------------------------------------------------
var eDistance,h1,h2: double;
  e: Double;
  h: Double;
    iAntCount1,iAntCount2: integer;
    i,j: integer;
begin
 // ser_Sites.Clear;

  Assert(Length(Params.SiteGroups)>0, 'Tframe_Profile_Rel_Graph.ShowSites_ - Length(Params.SiteGroups)>0');
  eDistance:=0;


  for j := 0 to High(Params.SiteGroups) do
    with Params.SiteGroups[j] do
  begin

    Assert(Distance_KM>0, 'procedure Tframe_Profile_Rel_Graph.ShowSites_; - Distance_KM>0');

    // -------------------------
    for i:=0 to High(Site1.AntennaHeights) do
    // -------------------------
    begin
      ser_Tower.AddNull;

      h:=Site1.Rel_H + Site1.AntennaHeights[i];
      ser_Tower.AddXY (eDistance, 0);
    //  ser_Tower.AddXY (eDistance, Site1.Rel_H);
      ser_Tower.AddXY (eDistance, h);

  (*    ser_Sites.AddXY (eDistance,  -1000, '' );

      ser_Sites.AddXY (eDistance,
                       Site1.Rel_H + Site1.AntennaHeights[i],
                       FloatToStr(Site1.AntennaHeights[i]) );
*)

(*      ser_Sites.AddXY (eDistance,
                       Site1.Rel_H,
                       ' ' );
*)

      ser_Sites.AddXY (eDistance,
                       Site1.Rel_H + Site1.AntennaHeights[i],
                       FloatToStr(Site1.AntennaHeights[i]) );

    end;
//      ser_Sites.AddXY (0+0.0001, FSite1.Rel_H + Params.Site1.Antennas[i],
         //              FloatToStr(Site1.Antennas[i]) );

    // -------------------------
//    for i:=High(Site2.AntennaHeights) downto 0 do
    for i:=0 to High(Site2.AntennaHeights) do
    // -------------------------
    begin
      ser_Tower.AddNull;

      h:=Site2.Rel_H + Site2.AntennaHeights[i];


      Assert(Distance_KM > 0, 'Distance_KM1 = 0');


      ser_Tower.AddXY (eDistance + Distance_KM, 0);
//      ser_Tower.AddXY (eDistance + Distance_KM, Site2.Rel_H);
      ser_Tower.AddXY (eDistance + Distance_KM, h);


  (*    ser_Sites.AddXY (eDistance + Distance_KM,
                       Site2.Rel_H + Site2.AntennaHeights[i],
                       FloatToStr(Site2.AntennaHeights[i]) );
*)

      ser_Sites.AddXY (eDistance + Distance_KM,
                       Site2.Rel_H + Site2.AntennaHeights[i],
                       FloatToStr(Site2.AntennaHeights[i]) );

 //     ser_Sites.AddXY (eDistance + Distance_KM,  -1000, '' );

    end;
//      ser_Sites.AddXY (RelProfile.GeoDistance1()-0.0001, FSite2.Rel_H + Params.Site2.Antennas[i],
   //                    FloatToStr(Params.Site2.Antennas[i]) );


    eDistance:=eDistance + Distance_KM;
  end;
end;



procedure Tframe_Profile_Rel_Graph.act_SaveImageAsExecute(Sender: TObject);
begin
  if Sender=act_SaveImageAs then
    SaveToBmpFile ();
end;


//---------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.ShowReflectionPoints_new;
//---------------------------------------------------------
//var
//  oLog: TStringList;

{
  //---------------------------------------------------------
  function DoLog(x, y: double): double;
  begin
    oLog.Add( Format('x: %1.3f,  y: %1.3f,', [x,y]) );
  end;
}


  //---------------------------------------------------------
  function DoFindRotationAngle(x1, y1, x2, y2: double): double;
  var iAngle: double;
  begin
    iAngle:=(arctan(y1/x1)+arctan(y2/x2))/2;
    Result:=arctan(y1/x1)-iAngle;
  end;

  //---------------------------------------------------------
  function DoTurnPoint(aNullPoint, aPoint: TxyPoint; aRotationAngle: double): TxyPoint;
  //---------------------------------------------------------
  var
    eDeltaX: Double;
    eDeltaY: Double;
  begin
    aPoint.x:=aPoint.x*1000;
    eDeltaX:=(aPoint.x*cos(aRotationAngle) + aPoint.y*sin(aRotationAngle))/1000;

    eDeltaY:=(-aPoint.x*sin(aRotationAngle)) + aPoint.y*cos(aRotationAngle);

    Result.x:=eDeltaX + aNullPoint.x;
    Result.y:=eDeltaY + aNullPoint.y;
  end;

              

  //---------------------------------------------------------
  function DoGetOffset_km(aNullPoint: TxyPoint): double;
  //---------------------------------------------------------
  var a, b: double;
      i: Integer;
  begin
    i:=RelProfile_Ref.GetItemIndexByDistance_KM(aNullPoint.x);
    if i>=0 then
    begin
      a:=RelProfile_Ref.ItemFullHeight(i);
      Result:=a-aNullPoint.y;
    end else
      Result:=0;
  end;


var
  i,k: integer;
  sin_alpha, cos_alpha: double;
  j: Integer;

  eProfileDist_km: Double;

 // e,offset: double;
 // Point3_res, Point4_res : TxyPoint;
  Point1_res, Point2_res : TxyPoint;

 // Point3, Point4: TxyPoint;
  Point1, Point2, NullPoint: TxyPoint;

  rotation_angle: double;
  dProsvet: double;

  oSeries: TLineSeries;

  dDistance_to_center_km,dLength_km,dRelH1,dRelH2, dTopH1,dTopH2: double;
  dYDistance,x,y,x1,y1,xNul,yNul, dY1Distance, dY2Distance: double;
  dx, dy: double;
  dRadius_km: double;
  e: Double;
  iColor: Integer;
  eOffset_km: Double;
 // iKoeff: Integer;
 // iKoeff: Double;
 // iKoeff2: Double;
  offset: double;

 // eDeltaX: Double;
  eDeltaY: Double;

  iCoeff: Integer;



  r: TReflectionRec;


  point0,
  point0_res: array[0..30] of TxyPoint;

begin
  oSeries:=ser_Refl;



//  if RelProfile.Data.Distance=0 then
//  if eProfileDist_km=0 then
//  if RelProfile.Distance()=0 then
   // Exit;

  eOffset_km:=0;


  for k:=0 to High(Params.SiteGroups) do
    with Params.SiteGroups[k] do
  begin

      eProfileDist_km:=Distance_KM;// RelProfile_Ref.GeoDistance_KM;

      e:=Params.SiteGroups[k].Site1.AntennaMaxH;
      e:=Params.SiteGroups[k].Site2.AntennaMaxH;


      dTopH1:=Site1.Rel_H + Site1.AntennaMaxH;
      dTopH2:=Site2.Rel_H + Site2.AntennaMaxH;

      for i:=0 to High(ReflectionPointArr) do
      begin

        if i>High(DEF_REFLECTION_COLORS) then
          Break;

        iColor := DEF_REFLECTION_COLORS[i];

//        dDistance_to_center_km:=ReflectionPointArr[i].Distance_KM;
        dDistance_to_center_km:=ReflectionPointArr[i].DISTANCE_to_center_km;
        dLength_km  :=ReflectionPointArr[i].Length_KM;
        dProsvet    :=ReflectionPointArr[i].abs_prosvet_m;
        dRadius_km  :=ReflectionPointArr[i].Radius_KM;


       // oSeries :=AddFastLineSeries(REFLECT_SERIES, psSolid, DEF_REFLECTION_COLORS[i]);

//        ser_Refl.AddNull();


        if dTopH1-dTopH2 > 0 then
        begin
          dYDistance:=dDistance_to_center_km*(dTopH1-dTopH2)/eProfileDist_km;
          dYDistance:=dTopH1-dYDistance;
        end else

        if dTopH1-dTopH2 = 0 then
          dYDistance:=dTopH1
        else
        begin
          dYDistance:=(eProfileDist_km-dDistance_to_center_km)*(dTopH2-dTopH1) / eProfileDist_km;
          dYDistance:=dTopH2-dYDistance;
        end;

        dYDistance:=dYDistance - dProsvet;





        //-------------------------------------
        if dDistance_to_center_km = 0 then
          exit;

        rotation_angle:=
          DoFindRotationAngle(dDistance_to_center_km*1000, dTopH1-dYDistance,
                         (eProfileDist_km-dDistance_to_center_km)*1000, dTopH2-dYDistance);

   //  oLog.Add('-------rotation_angle---------------');
   //  oLog.Add( Format('%1.2f', [rotation_angle])  );




        NullPoint.X:=dDistance_to_center_km;
        NullPoint.Y:=dYDistance;

        iCoeff:= IIF (dTopH1>dTopH2, -1, +1);
//        iKoeff2:= IIF (dTopH1>dTopH2, -1, +1);


       eDeltaY:=    (dLength_km*1000/2) * Sin (rotation_angle) ;


        Point1_res.x:=NullPoint.X - dLength_km/2;
        Point1_res.y:=NullPoint.y + eDeltaY * iCoeff;


        Point2_res.x:=NullPoint.X + dLength_km/2;
        Point2_res.y:=NullPoint.y + eDeltaY * iCoeff * (-1);


        FillChar(Point0, SizeOf (Point0), 0);


        dx:=dLength_km/30;
        Point0[0].x:=-dLength_km/2;


        for j:=0 to 30 do
        begin
          if sqr(dRadius_km) > sqr(Point0[j].x) then
            Point0[j].y:=sqrt(sqr(dRadius_km)-sqr(Point0[j].x))
          else
            continue;

          Point0[j].y     := -(dRadius_km-Point0[j].y)*1000;
          Point0_res[j]   := DoTurnPoint(NullPoint, Point0[j], rotation_angle);
          Point0_res[j].y := Point0_res[j].y;

          oSeries.AddXY (eOffset_km + Point0_res[j].x,   Point0_res[j].y, '',iColor);

          if j = 30
            then break;

          Point0[j+1].x:=Point0[j].x+dx;
        end;    // for



        //-------------------------------------
        //-------------------------------------
        //-------------------------------------
        FillChar(r, sizeOf(r),0);

        r.Radius:=dLength_km / 2;


        r.B:=MakeXY(0, dTopH1);
        r.C:=MakeXY(eProfileDist_km, dTopH2);

        r.A:=MakeXY(dDistance_to_center_km, dYDistance);

        CalcReflectionPoints(r);
        //-------------------------------------
        //-------------------------------------
        //-------------------------------------


       // oSeries.IgnoreNulls:=False;
        //����
        oSeries.AddNull; //(0,0);
        oSeries.AddXY (eOffset_km + 0, dTopH1);
        oSeries.AddXY (eOffset_km + dDistance_to_center_km, dYDistance, '',iColor);
        oSeries.AddXY (eOffset_km + eProfileDist_km, dTopH2, '',iColor);




        oSeries.AddNull;
        oSeries.AddXY (eOffset_km + r.Result.Line[0].x,   r.Result.Line[0].y);
        oSeries.AddXY (eOffset_km + r.Result.Line[1].x,   r.Result.Line[1].y,  '',iColor);

        oSeries.AddNull;

      end;

    eOffset_km:=eOffset_km + Distance_KM;

  end;

end;


procedure Tframe_Profile_Rel_Graph.act_SaveProfileAsExecute(Sender: TObject);
begin
  if SaveDialog_XML.Execute then
    RelProfile_Ref.SaveToXmlFile(SaveDialog_XML.FileName);
end;

procedure Tframe_Profile_Rel_Graph.Button1Click(Sender: TObject);
begin
  Chart1.Realign;
end;



//------------------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.Clear;
//------------------------------------------------------------------
//var
//  j: integer;
//  k: Integer;
//  i: Integer;

var
  I: Integer;
begin
  for I := 0 to Chart1.SeriesCount - 1 do
    Chart1.Series[i].Clear;


end;


//------------------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.UpdateChartBottomIncrement;
//------------------------------------------------------------------
var
  eDist_km,eStep: Double;
begin
  //in KM
  eDist_km:=RelProfile_Ref.Data.Distance_km;

  if eDist_km < 0.1 then eStep:=0.01  else
  if eDist_km < 1 then eStep:=0.1  else
//  if eDist_km < 2 then eStep:=0.2  else
  if eDist_km < 10 then eStep:=0.5  else
 // if eDist_km < 10 then eStep:=1  else
//  if eDist_km < 20 then eStep:=1 else
  if eDist_km < 30 then eStep:=1 else
  if eDist_km < 40 then eStep:=2
//  if eDist_km < 60 then eStep:=2
  else eStep:=5;

  Chart1.BottomAxis.Increment:=eStep;
end;


//------------------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.UpdateChartLeftIncrement;
//------------------------------------------------------------------
var
  eMaxH_m,eStep: Double;
begin
  eMaxH_m:=FMaxRelH + FMaxLocalH -FMinRelH;

  if eMaxH_m < 10  then eStep:=1  else
  if eMaxH_m < 50  then eStep:=5  else
//  if eMaxH_m < 100 then eStep:=10 else
  if eMaxH_m < 200  then eStep:=10  else
  if eMaxH_m < 500  then eStep:=50  else
  if eMaxH_m < 1000 then eStep:=100 else
  if eMaxH_m < 2000 then eStep:=200
                    else eStep:=200;

  Chart1.LeftAxis.Increment:=eStep;
  Chart1.RightAxis.Increment:=eStep;


end;




//------------------------------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.Chart1DblClick(Sender: TObject);
//------------------------------------------------------------------------------

(*      //---------------------------------------------------------
      procedure DoPaintDirectLIne(aBL1,aBL2: TBLPoint; aColor: TColor; aWidth: integer);
      begin
        FIMapView.MAP_ADD_LAYER_LINE (MIF_LAYER_PROFILE_REL_POINT,
                        MakeBLVector(aBL1,aBL2), aColor, 4, aWidth, '');
      end;
      //---------------------------------------------------------
*)

var
  eCoeff: double;
  bl: TBLPoint;
  blVec: TBLVector;
begin
  if not PtInRect(Chart1.ChartRect,
                  Point(FOldX-Chart1.Width3D, FOldY+Chart1.Height3D))
  then
    exit;

  eCoeff:= ((FOldX-Chart1.ChartRect.Left) / (Chart1.ChartRect.Right-Chart1.ChartRect.Left));

  blVec :=RelProfile_Ref.Data.BLVector;

  with blVec do
  begin
    bl.B:= Point1.B + (Point2.B - Point1.B)*eCoeff;
    bl.L:= Point1.L + (Point2.L - Point1.L)*eCoeff;
  end;



(*
 //------------------------------------------------------
  if Assigned(FIMapView) then
  //------------------------------------------------------
  begin
    FIMapView.TempLayer_Clear(MIF_LAYER_PROFILE_REL_POINT);

    DoPaintDirectLIne (blVec.Point1, blVec.Point2, clRed, 1);
    DoPaintDirectLIne (geo_RotateByAzimuth(bl, 50, 0),  geo_RotateByAzimuth(bl, 50, 180), clBlue, 4);
    DoPaintDirectLIne (geo_RotateByAzimuth(bl, 50, 90), geo_RotateByAzimuth(bl, 50, 270), clBlue, 4);

  end;
*)

end;


//--------------------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.DrawClutters(aRelProfile: TRelProfile);
//--------------------------------------------------------------------
var i,j,iCluCode,iSeries: integer;
//  b: Boolean;

    rel_h,loc_h,
    earth,
    x1,x2,
    h1,h2, rel_h1,rel_h2: double;
    bool: boolean;
    dist_km: double;

    eStep_KM: double;
    e: double;
  iColor: Integer;
  y: Double;

 // eLen: double;
begin
//  Series_Clu.Clear;
 ///// ShowMessage('procedure Tframe_Profile_Rel_Graph.DrawClutters(aRelProfile: TRelProfile);');


  if not assigned(aRelProfile) then
    exit;

 // Series_Clu.BeginUpdate;


//  UpdateClutterSeriesList();

  eStep_KM :=aRelProfile.GetStep_km();


  for i:=0 to aRelProfile.Count-1 do
  begin
    with aRelProfile do
    begin
      rel_h   :=Items[i].Rel_H;
      loc_h   :=Items[i].Clutter_H;
      iCluCode:=Items[i].Clutter_Code;
      earth   :=Items[i].Earth_H;
   //  end;

      dist_km :=Items[i].Distance_km - eStep_KM/2;  // �������� ������������� ��������
   end;

    if iCluCode>0 then
    begin

      iColor := aRelProfile.Clutters.Items[iCluCode].Color;


      x1 :=aRelProfile.Items[i].Distance_km - eStep_KM/2;
      x2 :=aRelProfile.Items[i].Distance_km + eStep_KM/2;


      if iCluCode in [DEF_CLU_BOLOTO,DEF_CLU_WATER,DEF_CLU_ROAD] then
      begin
       // loc_h := 0;
      //  loc_h := 2;
        loc_h := 1;
     //   loc_h := 0;

      end;

      //  DEF_CLU_OPEN_AREA: oAreaSeries.AddXY (dist_km, DEF_CHART_MIN_HEIGHT);


    //  y:=rel_h+loc_h+earth+20;
      y:=rel_h+loc_h+earth;

      Series_Clu.AddXY(x1, y, '', iColor);
      Series_Clu.AddXY(x2, y, '', iColor);

    end else
      Series_Clu.AddNull;


  end;


//  Series_Clu.EndUpdate;

end;



//-------------------------------------
procedure Tframe_Profile_Rel_Graph.DrawRelief(aRelProfile: TRelProfile);
//    aLenOffset: double);
//-------------------------------------
const
  DEF_SMALL_OFFSET = 0; //  0.001;
var
  iOffsetH: Integer;
  i: integer;
  earth_h,min_h,max_h, max_h_2, dist_km: double;

  earth_h_2: double;

  eStep_KM: double;
  e: double;
  iClu_H: double;

  x1: double;
  x2: Double;

begin
  if not assigned(aRelProfile) then
    exit;

  if aRelProfile.Count=0 then
    Exit;


//  ser_Earth_bottom.BeginUpdate;


{
  if (aRelProfile.Count>1) then
    eStep_KM :=aRelProfile.Items[1].Distance_KM- aRelProfile.Items[0].Distance_KM
  else
    eStep_KM :=0;
}

  eStep_KM:=aRelProfile.GetStep_km();

  e:=aRelProfile.Data.Distance_KM;


  with aRelProfile do
    for i:=0 to Count-1 do
  begin
    dist_km := Items[i].Distance_KM;
    earth_h := Items[i].Earth_H;

    if Params_Refraction_2 > 0 then
    begin
      earth_h_2 := geo_GetEarthHeight (dist_km, aRelProfile.Data.Distance_KM, Params_Refraction_2);
      max_h_2   := Items[i].Rel_H  + earth_h_2;
    end;

//      function geo_GetEarthHeight(aOffset_KM, aDistance_KM: double; aRefraction:
 //     double=1.33): double;


//      Params.Refraction_2:=0.5;


//////    earth_h := 0;///////////////////


    if i <= Count then
    begin
      min_h:=FMinRelH + earth_h;;
      max_h:=Items[i].Rel_H  + earth_h;
    end else begin
      min_h:=earth_h;
      max_h:=earth_h;
    end;

    if (Items[i].Clutter_Code in [DEF_CLU_WATER,DEF_CLU_BOLOTO] ) then
//      iOffsetH:=3
      iOffsetH:=DEF_WATER_OFFSET
    else
      iOffsetH:=0;


   ////// iOffsetH := 0;
    //-------------------------------------------------------------------
    x1 := dist_km - eStep_KM/2 + DEF_SMALL_OFFSET;
    x2 := dist_km + eStep_KM/2 - DEF_SMALL_OFFSET;

    ser_Earth_area_top.AddXY (x1, max_h - iOffsetH);
    ser_Earth_area_top.AddXY (x2, max_h - iOffsetH);

    //------------------
    ser_Earth_top.AddXY (x1, max_h - iOffsetH);
    ser_Earth_top.AddXY (x2, max_h - iOffsetH);

    if Params_Refraction_2 > 0 then
    begin
     // max_h_2:=100;
//      Items[i].
      iClu_H:=Items[i].Clutter_H;

//      Series_Earth_reflection_2.AddXY (x1, max_h_2);// - iOffsetH);
//      Series_Earth_reflection_2.AddXY (x2, max_h_2);// - iOffsetH);


      Series_Earth_reflection_2.AddXY (x1, max_h_2);// - iOffsetH);
      Series_Earth_reflection_2.AddXY (x1, max_h_2+iClu_H);// - iOffsetH);

      Series_Earth_reflection_2.AddXY (x2, max_h_2+iClu_H);// - iOffsetH);
      Series_Earth_reflection_2.AddXY (x2, max_h_2);// - iOffsetH);


    end;
//    earth_h_2 := geo_GetEarthHeight (dist_km, aRelProfile.Data.Distance_KM, Refraction_2);
  //  max_h_2   := Items[i].Rel_H  + earth_h_2;



//    ser_Earth_area_bottom.AddXY (dist_km, min_h - REL_BOTTOM_OFFSET - 4);

    ser_Earth_area_bottom.AddXY (x1, min_h - REL_BOTTOM_OFFSET - 4);

 //   ser_Earth_area_bottom.AddXY (dist_km - eStep_KM/2, min_h - REL_BOTTOM_OFFSET - 4);

   ser_Earth_bottom.AddXY (x1, min_h - REL_BOTTOM_OFFSET - 4);
   ser_Earth_bottom.AddXY (x2, min_h - REL_BOTTOM_OFFSET - 4);
//    ser_Earth_bottom.AddXY (dist_km + eStep_KM/2, min_h - REL_BOTTOM_OFFSET - 4);


  end;

  e:=aRelProfile.LastItem.Distance_km + eStep_KM/2;

//  ser_Earth_bottom.AddXY (e, DEF_CHART_MIN_HEIGHT);
  ser_Earth_area_bottom.AddXY (e, min_h - REL_BOTTOM_OFFSET - 4);

end;

procedure Tframe_Profile_Rel_Graph.FormResize(Sender: TObject);
begin
  Chart1.Realign;

  pn_status_Left.Left:=DEF_OFFSET;
  pn_status_Left.Top :=Height - StatusBar1.Height - pn_status_Left.Height -  DEF_OFFSET;

  pn_status_right.Left:=Width - pn_status_right.Width - DEF_OFFSET;
  pn_status_right.Top :=Height - StatusBar1.Height - pn_status_right.Height -  DEF_OFFSET;

end;

procedure Tframe_Profile_Rel_Graph.SetChartSize(aWidth,aHeight : integer);
begin
  Chart1.Align:=alNone;

  Chart1.Width:=aWidth;
  Chart1.Height:=aHeight;
end;


procedure Tframe_Profile_Rel_Graph.BeginUpdateAll;
var
  I: Integer;
begin
  for I := 0 to Chart1.SeriesCount - 1 do
    Chart1.Series[i].BeginUpdate;;
end;


procedure Tframe_Profile_Rel_Graph.EndUpdateAll;
var
  I: Integer;
begin
  for I := 0 to Chart1.SeriesCount - 1 do
    Chart1.Series[i].EndUpdate;;
end;


//-----------------------------------------------------
procedure Tframe_Profile_Rel_Graph.ShowGraph;
//-----------------------------------------------------
var
  eOffset: double;
  i: integer;
begin
  Assert(Assigned(RelProfile_Ref));


  if RelProfile_Ref.Data.Distance_KM=0 then
    exit; 



  BeginUpdateAll;

  Clear;

  UpdateMinMaxHeights();


//    ShowReflectionPoints_new();

  //  Exit;

  DrawClutters (RelProfile_Ref);

  DrawRelief (RelProfile_Ref);


//  exit; //!!!!!!!!!!!!!


  eOffset:=0;
  for i := 0 to High(Params.SiteGroups) do
    with Params.SiteGroups[i] do
    begin
      Assert(Distance_KM > 0, 'Distance_KM1 = 0');


      DrawEarthLines_new (eOffset, Distance_KM, RelProfile_Ref.Refraction);

      eOffset:=eOffset + Distance_KM;
    end;


  if Params.IsShowReflectionPoints then
    ShowReflectionPoints_new();

//(*
//  end
//  else
//  ; //  ClearReflectionPoints();
//*)

  if FIsShowSites then
  begin
    ShowSites_();

    ShowFrenelZone ();
  end;

   // ShowSites() ;


  UpdateChartBottomIncrement();

  EndUpdateAll;

end;

{


//---------------------------------------------------------------------------
function DrawCross11(aChart: TChart; const aCross: TPoint;
                   aVer,aHor: Boolean; clPen: TColor; aSave: Boolean = True): TPoint;
//---------------------------------------------------------------------------
begin
  with aChart, Canvas do
  begin
    Pen.Color:= clPen;
    Pen.Style:= psSolid;
    Pen.Mode := pmXor;
    Pen.Width:= 1;

    if aVer then // ������������ �������
      Line(aCross.X, ChartRect.Top-Height3D, aCross.X, ChartRect.Bottom-Height3D);

    if aHor then // �������������� �������
      Line(ChartRect.Left-Width3D, aCross.Y, ChartRect.Right-Width3D, aCross.Y);
  end;

  if aSave then
    Result:= aCross
  else
    Result:= Point(-1,-1);

end;

}

//---------------------------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.act_CrossExecute(Sender: TObject);
//---------------------------------------------------------------------------
begin
  // ������
  if (CrossOld.X <> -1) then
    CrossOld:= DrawCross(Chart1, CrossOld, not act_Cross.Checked, False, clRed, False);
end;

//---------------------------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.act_MagnifyExecute(Sender: TObject);
//---------------------------------------------------------------------------
begin
//  act_Magnify.Checked:= not act_Magnify.Checked;

//  ChartTool_Magnify.Visible := False;

 //////////

 {
  ChartTool_Magnify1.Visible := act_Magnify.Checked;

  with ChartTool_Magnify1 do
  begin
 //   ParentChart:= Chart1;
    Bounds:= Rect(0,0,250,250);
    FollowMouse       := True;  // �������� � ����
    Smooth            := False; //True;  // �����������
    Circled           := True;  // �������
    Percent           :=  50;   // ����������, %
    Shape.Transparency:=  10;   // ������������, %
    Height            := 250;   // ������, pix
    Width:= Height;

  end
 }

(*
  if act_Magnify.Checked then
    begin
    FMagnify:= TMagnifyTool.Create(Self);
    with FMagnify do
      begin
      ParentChart:= Chart1;
      Bounds:= Rect(0,0,250,250);
      FollowMouse       := True;  // �������� � ����
      Smooth            := False; //True;  // �����������
      Circled           := True;  // �������
      Percent           :=  50;   // ����������, %
      Shape.Transparency:=  10;   // ������������, %
      Height            := 250;   // ������, pix
      Width:= Height;
      end;
    end
  else
    FreeAndNil(FMagnify);
  *)

end;


//------------------------------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.Chart1MouseMove(Sender: TObject;
          Shift: TShiftState; X, Y: Integer);
//------------------------------------------------------------------------------
var
  I: Integer;
  Cross     : TPoint;   // ���������� �������
  Distance  : Double;
  Frenel    : Integer;
  Earth_Top : Integer;
  Clu_Height: Integer;
  Clu_Name  : String;
  iIndex    : Integer;
  LocalObjectH: Integer;
  DeltaH    : Integer;

  function GetClutterName(aCode: Integer): String;
  begin
    case aCode of
     0 : Result:= '�������� ���������';
     1 : Result:= '���';
     2 : Result:= '����';
     3 : Result:= '���������� �����';
     4 : Result:= '������';
     5 : Result:= '�/�';
     6 : Result:= '������������ ������';
     7 : Result:= '�����';
     8 : Result:= '���������� ������';
    31 : Result:= '��������� ��� � �������';
    73 : Result:= '�������� ������� ������';
    else
      Result:= '';
    end;//case
  end;

  {

  function GetSeriesValue111(aSer: TChartSeries; aScreenX: Integer): Double;
  var
    iInd: Integer;
    PtLf,PtRg : TPoint;
    n: Integer;
  begin
    iIndex:= -1;
    Result:= 0;



    for n:= aSer.FirstValueIndex to aSer.LastValueIndex-1 do //ValueIndex
      if not(aSer.IsNull(n) or aSer.IsNull(n+1)) then
        begin
        // ����� ����� � ������ �� �������
        PtLf:= Point(aSer.CalcXPos(n  ), aSer.CalcYPos(n  ));
        PtRg:= Point(aSer.CalcXPos(n+1), aSer.CalcYPos(n+1));

        // �������� ���������� ������� � ��������� ����� �������
        if (PtLf.X <= aScreenX) and (aScreenX <= PtRg.X) then
          begin
          if (aScreenX = PtRg.X) then                       // ������ �� ������ �������
            Result:= aSer.YValue[n+1]                       // �������� �� ������ �������
          else
            begin
            Result:= aSer.YValue[n];                        // �������� �� ����� �������
            if not ((aScreenX = PtLf.X) or                  // ������ �� �� ����� ������� �
               (PtRg.X = PtLf.X) or (PtRg.Y = PtLf.Y)) then // �� ����������� � �����/����� �������
              Result:= Result + (aScreenX - PtLf.X) / (PtRg.X - PtLf.X) *
                        (aSer.YValue[n+1] - Result);
            end;
          iIndex:= n;



          Break;
          end;

        end;
  end;
  }


begin

  // ������� ������
  Cross:= Point(X,Y);
  // "�����" ������� �������
  if (CrossOld.X <> -1) then
    CrossOld:= DrawCross(Chart1, CrossOld, act_Cross.Checked, False, clRed, False);

  // �������������� ����� �������
  if PtInRect(Chart1.ChartRect, Point(X-Chart1.Width3D, Y+Chart1.Height3D)) then
    begin
    // ��������� ������ ������� � ����������� ��� �������
    CrossOld:= DrawCross(Chart1, Cross, act_Cross.Checked, False, clRed);

    Distance  := ser_Earth_bottom.XScreenToValue(Cross.X);
    Frenel    := Round(GetSeriesValue(ser_Frenel, Cross.X));
    Clu_Height:= Round(GetSeriesValue(Series_Clu, Cross.X));
    Earth_Top := Round(GetSeriesValue(ser_Earth_Top, Cross.X));

    LocalObjectH:= IIF((Clu_Height > 0), Clu_Height - Earth_Top, 0);

    DeltaH:= Frenel - (Earth_Top + LocalObjectH);


    // ����
    with StatusBar1 do
    begin
      // ����������

//      Panels[0].Text:= Format('�������� �����: %f ��', [Distance]);
      Panels[0].Text:= Format('��������: %f ��', [Distance]);
      //StatusTextOut (sbInfo, 1, Format('��������: %f ��', [RelProfile_Ref.Items[iIndex].Distance_KM]));
      Panels[1].Text:= Format('�������: %d �', [DeltaH]);
      Panels[2].Text:= Format('������ �����: %d �', [Earth_Top]);

      //����
      if LocalObjectH=3 then
        LocalObjectH:=0;

      if (LocalObjectH > 0) then
        Panels[3].Text:= Format('������ ����.��������: %d �', [LocalObjectH])
      else
        Panels[3].Text:= '';

      //StatusTextOut (sbInfo, 4, Format('%s', [Clu_Name]));

    end;


    end
  else
    begin
      for I := 0 to StatusBar1.Panels.Count - 1 do
        StatusBar1.Panels[i].Text:='';

//    StatusTextClear (sbInfo);
    Label1.Visible:= False;
    end;

end;

procedure Tframe_Profile_Rel_Graph.Hide_TxRx;
begin
  pn_status_left.Visible:=False;
  pn_status_right.Visible:=False;
end;



//---------------------------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.SetStatusPanelColor(aIndex, aStatus:  Integer);
//---------------------------------------------------------------------------
var
  oPanel: TPanel;
  cl: TColor;
begin
  Assert(aIndex in [1,2]);

  if aIndex=1 then
    oPanel:=pn_status_left
  else
    oPanel:=pn_status_Right;

  case aStatus of
    1: cl:=clLime;
    2: cl:=clRed;
  else
    cl := clBtnFace;
  end;

  oPanel.Color:= cl;

  // ������
//  if (CrossOld.X <> -1) then
//    CrossOld:= DrawCross(Chart1, CrossOld, not act_Cross.Checked, False, clRed, False);
end;

//procedure SaveToBmpFile (aFileName: string='');



end.


