
unit fr_Profile_Rel;

interface

uses

//  u_geo_convert_new,
  u_geo_convert_2017,

  I_Options_,
//  X_Options,

  fr_Profile_Rel_Graph,
  fr_profile_Rel_Grid,

  u_files,
  u_dlg,
  u_func,

  u_Geo,
  ///u_geo_convert_new,

  u_rel_Profile,

  u_profile_classes,


  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Menus, Dialogs, ExtCtrls,  cxPC, cxControls, ToolWin, StdCtrls ,
  cxLookAndFeels, cxGraphics, cxLookAndFeelPainters, dxSkinsCore,
  dxSkinsDefaultPainters

  ;

type


  Tframe_profile_Rel = class(TForm)
    Panel1: TPanel;
    SaveDialog1: TSaveDialog;
    PopupMenu1: TPopupMenu;
    ActionList1: TActionList;
    act_SaveAs: TAction;
    act_Print: TAction;
    StatusBar1: TStatusBar;
    StatusBar2: TStatusBar;
    SaveAs1: TMenuItem;
    MainMenu1: TMainMenu;
    menu1: TMenuItem;
    N1: TMenuItem;
    statbar_RRS: TStatusBar;
    Action1: TAction;
    cxPageControl1: TcxPageControl;
    p_Profile: TcxTabSheet;
    p_Points: TcxTabSheet;
    cxLookAndFeelController1: TcxLookAndFeelController;
    procedure FormDestroy(Sender: TObject);
    procedure act_SaveAsExecute(Sender: TObject);
    procedure cxPageControl1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  //  procedure FormDestroy(Sender: TObject);
    procedure StatusBar1Resize(Sender: TObject);
//    procedure cxPageControlOnChange(Sender: TObject);
    procedure GSPages11Click(Sender: TObject);
  private
    FUpdated: Boolean;

    FOnProfileChanged: TNotifyEvent;

    Profile_Rel_Graph_: Tframe_Profile_Rel_Graph;

    Fframe_profile_Rel_Grid: Tframe_profile_Rel_Grid;


    function GetRelProfile_Ref: TrelProfile;
    procedure SetRelProfile_Ref(const Value: TrelProfile);
    procedure StatusBar_ShowSiteAngles(aAngle1,aAngle2: double);

    procedure DoOnProfileSave(Sender: TObject);
    procedure StatusBarRefresh;

  public
    BLVector: TBLVector;

    Repeater: record
      Enabled : boolean;

      BLPoint : TBLPoint;
    end;

    Site1Name, Site2Name: string;

//    IsDirectionReversed : Boolean;

    Params: TProfileViewFormParamsObj;

    Params_Refraction_2: double;// =0.5;


    procedure SetSiteGroupCount(aCount: Integer);
    procedure Refresh_;

    function GetDisplayBLVector: TBLVector;

    procedure ShowGraph;
    procedure ShowProfile; //virtual;

    procedure ShowStatusBars(aValue: boolean);

    procedure SetStatusPanelColor(aIndex, aStatus: Integer);


    property RelProfile_Ref: TrelProfile read GetRelProfile_Ref write SetRelProfile_Ref;

    property OnProfileChanged: TNotifyEvent read FOnProfileChanged write FOnProfileChanged;
  end;


//===========================================================
//===========================================================
implementation

uses Options_TLB; {$R *.DFM}

const
  //width
  DEF_STATUS_DEGREE_WIDTH=110;
  DEF_STATUS_AZIMUTH_WIDTH=80;

  DEF_STATUSBAR_SITE_ANGLE_WIDTH = 80;


  PROFILE_PANEL1_POINT1_B = 0;
  PROFILE_PANEL1_POINT1_L = 1;
  PROFILE_PANEL1_LEN      = 2;
  PROFILE_PANEL1_POINT2_B = 3;
  PROFILE_PANEL1_POINT2_L = 4;

  PROFILE_PANEL2_AZIM1  = 0;
  PROFILE_PANEL2_ANGLE1 = 1;
  PROFILE_PANEL2_AZIM2  = 3;
  PROFILE_PANEL2_ANGLE2 = 4;


  FMT_SITE_AZIM ='���: %s';
  FMT_SITE_ANGLE='���: %3.2f�';
  FMT_LENGTH    ='%1.3f km';// : %d m';




procedure Tframe_profile_Rel.act_SaveAsExecute(Sender: TObject);
begin

end;


//---------------------------------------------------------
procedure Tframe_profile_Rel.FormCreate(Sender: TObject);
//---------------------------------------------------------
begin
  inherited;

  Params_Refraction_2:=0;
//  Params_Refraction_2:=1;


  cxPageControl1.Align:=alClient;
  cxPageControl1.ActivePageIndex:=0;

//  GSPages1.Align:=alClient;
//  GSPages1.ActivePageIndex:=0;



  with StatusBar1 do
  begin
    Panels[PROFILE_PANEL1_POINT1_B].Width :=DEF_STATUS_DEGREE_WIDTH;
    Panels[PROFILE_PANEL1_POINT1_L].Width :=DEF_STATUS_DEGREE_WIDTH;
    Panels[2].Width                       :=0;
    Panels[PROFILE_PANEL1_LEN].Width      :=1;
    Panels[PROFILE_PANEL1_LEN].Alignment  :=taCenter;
    Panels[4].Width                       :=0;
    Panels[PROFILE_PANEL1_POINT2_B].Width :=DEF_STATUS_DEGREE_WIDTH;
    Panels[PROFILE_PANEL1_POINT2_L].Width :=DEF_STATUS_DEGREE_WIDTH;
  end;

  with StatusBar2 do
  begin
    Panels[PROFILE_PANEL2_ANGLE1].Width :=DEF_STATUSBAR_SITE_ANGLE_WIDTH;
    Panels[PROFILE_PANEL2_AZIM1].Width  :=DEF_STATUS_AZIMUTH_WIDTH;
    Panels[2].Width  :=1;
    Panels[2].Alignment                 :=taCenter;
    Panels[PROFILE_PANEL2_AZIM2].Width  :=DEF_STATUS_AZIMUTH_WIDTH;
    Panels[PROFILE_PANEL2_ANGLE2].Width :=DEF_STATUSBAR_SITE_ANGLE_WIDTH;
  end;

  with statbar_RRS do
  begin
    Panels[0].Width :=Trunc(statbar_RRS.Width/2);
    Panels[1].Width :=Trunc(statbar_RRS.Width/2);
    Panels[1].Alignment  :=taRightJustify;
  end;

//  FUpdated:=True;
  CreateChildForm(Tframe_profile_Rel_Grid, Fframe_profile_Rel_Grid, p_Points);
//  CreateChildForm(Tframe_profile_Rel_Grid, Fframe_profile_Rel_Grid, p_Points);
 // FUpdated:=False;

  Fframe_profile_Rel_Grid.OnSave:=DoOnProfileSave;


  CreateChildForm(Tframe_Profile_Rel_Graph, Profile_Rel_Graph_, p_Profile);

//  Assert(Assigned(RelProfile_Ref));

//  Profile_Rel_Graph_.RelProfile_Ref:=RelProfile_Ref;

  Params:=Profile_Rel_Graph_.Params;

  Params.IsShowFrenelZoneTop:=True;
  Params.IsShowFrenelZoneBottomLeft:=True;
  Params.IsShowFrenelZoneBottomRight:=True;
  Params.IsShowFrenelZoneBottom:=True;



//  RelProfile1:=Profile_Rel_Graph_.RelProfile1;

  Params.IsShowFrenelZoneTop:=True;

 // p_Clutters.TabVisible:=False;

end;


procedure Tframe_profile_Rel.FormDestroy(Sender: TObject);
begin
  FreeAndNil(Fframe_profile_Rel_Grid);
  FreeAndNil(Profile_Rel_Graph_);

  inherited;
end;


procedure Tframe_profile_Rel.DoOnProfileSave(Sender: TObject);
begin
 // Profile_Rel_Graph_.ClearReflectionPoints;
  Profile_Rel_Graph_.ShowGraph;

  if Assigned(FOnProfileChanged) then
    FOnProfileChanged(Self);
end;



//-----------------------------------------------------
procedure Tframe_profile_Rel.ShowProfile;
//-----------------------------------------------------
var
  eRel_H1, eRel_H2, max_h1, max_h2, h1, h2: double;
  e1: double;
  e2: double;
  i: Integer;
  blVector_: TblVector;

begin
  i:=Length(Params.SiteGroups);

  if Length(Params.SiteGroups)=0 then
    exit;


  Profile_Rel_Graph_.Params:=Params;
  Profile_Rel_Graph_.Params_Refraction_2:=Params_Refraction_2;



//  Profile_Rel_Graph_.RelProfile_Ref:=RelProfile_Ref;
  Profile_Rel_Graph_.ShowGraph;

  StatusBarRefresh();

  if Length(Profile_Rel_Graph_.Params.SiteGroups)= 1 then
  begin

    with Profile_Rel_Graph_.Params.SiteGroups[0] do
    begin
      h1:=Site1.AntennaMaxH + Site1.Rel_H;
      h2:=Site2.AntennaMaxH + Site2.Rel_H;
    end;

    blVector_ := GetDisplayBLVector();


    e1:=geo_TiltAngle (BLVector_.Point1, BLVector_.Point2, h1, h2);
    e2:=geo_TiltAngle (BLVector_.Point2, BLVector_.Point1, h2, h1);

    StatusBar_ShowSiteAngles (geo_TiltAngle (BLVector_.Point1, BLVector_.Point2, h1, h2),
                              geo_TiltAngle (BLVector_.Point2, BLVector_.Point1, h2, h1));

  end;



  case cxPageControl1.ActivePageIndex of
    1: Fframe_profile_Rel_Grid.View (RelProfile_Ref);
  end;

end;



//---------------------------------------------------------
// MISC
//---------------------------------------------------------

// -------------------------------------------------------------------
procedure Tframe_profile_Rel.StatusBarRefresh;
// -------------------------------------------------------------------
var
  iCoord: Integer;
  dDistance_m: double;
  blPoint1,blPoint2: TBLPoint;
  blVector_: TblVector;
  e1: Double;
  e2: Double;
  s: string;
begin
 // Assert(Assigned(IOptions));

  blVector_ := GetDisplayBLVector();

  //geo_BL_to_BL_2017

  iCoord:=IOptions.Get_CoordSys;
  blPoint1:=geo_BL_to_BL_2017_ (BLVector_.Point1, EK_CK_42, iCoord);
  blPoint2:=geo_BL_to_BL_2017_ (BLVector_.Point2, EK_CK_42, iCoord);

//  blPoint1:=geo_BL_to_BL_new (BLVector_.Point1, EK_CK_42, IOptions.GeoCoord);
//  blPoint2:=geo_BL_to_BL_new (BLVector_.Point2, EK_CK_42, IOptions.GeoCoord);



  ////  StatusBar1.Panels[0].Text:=

  s:=geo_GetCoordSysStr(IOptions.Get_CoordSys);


  with StatusBar1 do
  begin

    Panels[PROFILE_PANEL1_POINT1_B].Text:=geo_FormatDegree_lat(blPoint1.B);
    Panels[PROFILE_PANEL1_POINT1_L].Text:=geo_FormatDegree_lon(blPoint1.L);
    Panels[PROFILE_PANEL1_POINT2_B].Text:=geo_FormatDegree_lat(blPoint2.B);
    Panels[PROFILE_PANEL1_POINT2_L].Text:=geo_FormatDegree_lon(blPoint2.L);

{
    Panels[PROFILE_PANEL1_POINT1_B].Text:=geo_FormatDegree(blPoint1.B, dtLat)+ IIF(iCoord=EK_WGS84,' w','');
    Panels[PROFILE_PANEL1_POINT1_L].Text:=geo_FormatDegree(blPoint1.L, dtLon)+ IIF(iCoord=EK_WGS84,' w','');
    Panels[PROFILE_PANEL1_POINT2_B].Text:=geo_FormatDegree(blPoint2.B, dtLat)+ IIF(iCoord=EK_WGS84,' w','');
    Panels[PROFILE_PANEL1_POINT2_L].Text:=geo_FormatDegree(blPoint2.L, dtLon)+ IIF(iCoord=EK_WGS84,' w','');
}

//    Panels[PROFILE_PANEL1_LEN].Text:=Format(FMT_LENGTH, [Round(geo_Distance (BLVector))]);



    ///////
    if Repeater.Enabled then
    begin
      e1:=geo_Distance_m (BLVector.Point1, Repeater.BLPoint);
      e2:=geo_Distance_m (BLVector.Point2, Repeater.BLPoint);

      dDistance_m:=e1 + e2;

    end else
      dDistance_m:=geo_Distance_m (BLVector);

  end;

  StatusBar2.Panels[PROFILE_PANEL1_LEN].Text:=  Format(FMT_LENGTH, [dDistance_m/1000]);


  StatusBar1.Panels[PROFILE_PANEL1_LEN].Alignment:=taLeftJustify;

  s:=geo_GetCoordSysStr(IOptions.Get_CoordSys);
  StatusBar1.Panels[PROFILE_PANEL1_LEN].Text:=s;
  StatusBar1.Panels[PROFILE_PANEL1_LEN].Alignment:=taCenter;


{
 Repeater: record
      Enabled : boolean;

      BLPoint : TBLPoint;
    end;
}


end;

//===========================================================================//
//  �������� ���� �����
//
//===========================================================================//

procedure Tframe_profile_Rel.StatusBar1Resize(Sender: TObject);
var i,iWidth: integer;
begin
  iWidth:=0;
  for i:=0 to StatusBar1.Panels.Count-1 do
    if i<>PROFILE_PANEL1_LEN then
       Inc(iWidth, StatusBar1.Panels[i].Width);

  with StatusBar1 do
    Panels[PROFILE_PANEL1_LEN].Width :=Width-iWidth;

  with StatusBar2 do
    Panels[2].Width :=Width - 2*Panels[0].Width - 2*Panels[1].Width;

  with statbar_RRS do
  begin
    Panels[0].Width :=Trunc(Width/2);
    Panels[1].Width :=Panels[0].Width;
  end;    // with
end;

//--------------------------------------------------------
procedure Tframe_profile_Rel.StatusBar_ShowSiteAngles(aAngle1,aAngle2: double);
//--------------------------------------------------------
var
  blVector_: TblVector;

  dAzimuth1,dAzimuth2: double;
  s1: string;
  s2: string;

begin

  blVector_ := GetDisplayBLVector();


  with StatusBar2 do
  begin
    s1:= Format('���: %1.2f', [aAngle1]);
    s2:= Format('���: %1.2f', [aAngle2]);

    Panels[PROFILE_PANEL2_ANGLE1].Text:=s1;
    Panels[PROFILE_PANEL2_ANGLE2].Text:=s2;

(*
    Panels[PROFILE_PANEL2_ANGLE1].Text:='���: '+ geo_FormatAngleEx (aAngle1);
    Panels[PROFILE_PANEL2_ANGLE2].Text:='���: '+ geo_FormatAngleEx (aAngle2);
*)

    dAzimuth1:=geo_Azimuth (BLVector_.Point1, BLVector_.Point2);
    dAzimuth2:=geo_Azimuth (BLVector_.Point2, BLVector_.Point1);

    s1:= Format('���: %1.2f', [dAzimuth1]);
    s2:= Format('���: %1.2f', [dAzimuth2]);

    Panels[PROFILE_PANEL2_AZIM1].Text:=s1;
    Panels[PROFILE_PANEL2_AZIM2].Text:=s2;

(*
    Panels[PROFILE_PANEL2_AZIM1].Text:=  '���: '+ geo_FormatAngleEx (dAzimuth1);
    Panels[PROFILE_PANEL2_AZIM2].Text:='���: '+ geo_FormatAngleEx (dAzimuth2);
*)

  end;

  with statbar_RRS do
  begin
    statbar_RRS.Visible:=Site2Name <> '';
    Panels[0].Text:=Site1Name;
    Panels[1].Text:=Site2Name;
  end;    // with
end;


procedure Tframe_profile_Rel.SetSiteGroupCount(aCount: Integer);
begin
 // Profile_Rel_Graph_.SetSiteGroupCount(aCount);

  if Length(Params.SiteGroups)<>aCount then
    SetLength(Params.SiteGroups, aCount);

end;

procedure Tframe_profile_Rel.ShowGraph;
begin
  Profile_Rel_Graph_.ShowGraph;
end;



function Tframe_profile_Rel.GetDisplayBLVector: TBLVector;
begin
//  if IsDirectionReversed then
//    Result := geo_RotateBLVector_(BLVector)
//  else
    Result := BLVector;
end;


function Tframe_profile_Rel.GetRelProfile_Ref: TrelProfile;
begin
  Result := Profile_Rel_Graph_.RelProfile_Ref;
end;

procedure Tframe_profile_Rel.SetRelProfile_Ref(const Value: TrelProfile);
begin
  Profile_Rel_Graph_.RelProfile_Ref:=Value;
end;



procedure Tframe_profile_Rel.GSPages11Click(Sender: TObject);
begin
//  if not FUpdated then

end;

procedure Tframe_profile_Rel.Refresh_;
begin
//  Profile_Rel_Graph_.Repaint;
  Profile_Rel_Graph_.Chart1.Repaint;
end;


procedure Tframe_profile_Rel.ShowStatusBars(aValue: boolean);
begin
  StatusBar1.Visible:=aValue;
  StatusBar2.Visible:=aValue;
  statbar_RRS.Visible:=aValue;

end;


procedure Tframe_profile_Rel.cxPageControl1Click(Sender: TObject);
begin
    case cxPageControl1.ActivePageIndex of
      1: begin
         //  e:=Params.SiteGroups[0].DistanceKM;

           Fframe_profile_Rel_Grid.View (RelProfile_Ref);

         end;
    end;


end;


procedure Tframe_profile_Rel.SetStatusPanelColor(aIndex, aStatus: Integer);
begin
  Profile_Rel_Graph_.SetStatusPanelColor(aIndex, aStatus);
end;




end.

{


//--------------------------------------------------------
procedure Tframe_profile_Rel.cxPageControlOnChange(Sender: TObject);
//--------------------------------------------------------
begin
 // if not FUpdated then
    case cxPageControl1.ActivePageIndex of
      1: begin
         //  e:=Params.SiteGroups[0].DistanceKM;

           Fframe_profile_Rel_Grid.View (RelProfile_Ref);

         end;
  //    2: Fframe_ClutterModel_Items.View (ClutterModelID);
    end;

end;

}
