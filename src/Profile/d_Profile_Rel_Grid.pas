unit d_Profile_Rel_Grid;
 { TODO : remove 2-nd menu bar }


interface

uses
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxControls, cxGridCustomView, cxClasses, cxGridLevel, cxGrid, dxmdaset,
  StdCtrls, Menus, Grids, DBGrids,  ActnList, cxGraphics,
  XPStyleActnCtrls, ActnMan, ActnCtrls, ActnMenus,

  u_rel_Profile,

  u_func,
  u_dlg,
  u_db,

  u_cx,

  dxBar, DB, Classes, Controls, ToolWin, ComCtrls
  ;



type
  Tdlg_Profile_Rel_Grid = class(TForm)
    ToolBar1: TToolBar;
    ds_Data: TDataSource;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    col_recno1: TcxGridDBColumn;
    col_Dist_M: TcxGridDBColumn;
    col_Rel_H: TcxGridDBColumn;
    col_earth_h: TcxGridDBColumn;
    col_clu_h: TcxGridDBColumn;
    col_Clu_Name: TcxGridDBColumn;
    col_Clu_Color: TcxGridDBColumn;
    col_clu_code: TcxGridDBColumn;
    mem_Clutters1: TdxMemData;
    mem_Clutters1id: TIntegerField;
    mem_Clutters1name: TStringField;
    ds_Clutters: TDataSource;
    DBGrid1: TDBGrid;
    ActionList1: TActionList;
    act_Save: TAction;
    act_Refresh: TAction;
    mem_Relief: TdxMemData;
    mem_Reliefdist: TFloatField;
    mem_Reliefrel_h: TFloatField;
    mem_Reliefearth_h: TFloatField;
    mem_Reliefclu_h: TFloatField;
    mem_Reliefclu_code: TIntegerField;
    mem_Reliefclu_name: TStringField;
    mem_Reliefclu_color: TIntegerField;
    mem_ReliefrecNo: TIntegerField;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    mem_Reliefis_modify: TBooleanField;
    mem_Reliefindex: TIntegerField;
    act_Update_Rel_H: TAction;
    act_Update_Clu_H: TAction;
    act_Update_Clu_Code: TAction;
    actUpdateCluCode1: TMenuItem;
    actUpdateCluH1: TMenuItem;
    actUpdateRelH1: TMenuItem;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
  //  mem_Reliefis_modify: TBooleanField;
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
 //   procedure act_Multi_updateExecute(Sender: TObject);
    procedure act_RefreshExecute(Sender: TObject);
    procedure act_SaveExecute(Sender: TObject);
    procedure act_Update_Rel_HExecute(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
//    procedure dfffffff1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure mem_Relief_AfterDelete(DataSet: TDataSet);
    procedure mem_Relief_AfterEdit(DataSet: TDataSet);
//    procedure mem_Relief_CalcFields(DataSet: TDataSet);
    procedure SetReadOnly(aValue: boolean);
    procedure ToolButton1Click(Sender: TObject);
  private
    FIsModify: Boolean;
    FRegPath: string;
    FOnSave: tnotifyEvent;
    FRelProfileRef: TrelProfile;

    procedure SaveToProfileObj;
    procedure Update_Rel_H;
    procedure Update_Clutter_Code;
    procedure Update_Clutter_H;

  public
//    procedure UpdateRecNo;
    procedure View(aProfile: TrelProfile);

    property OnSave: TNotifyEvent read FOnSave write FOnSave;
  end;


//==============================================================
implementation {$R *.DFM}
//==============================================================

const
  REGISTRY_COMMON_FORMS  = 'Software\Onega\Common\Forms\';


const
  FLD_INDEX     = 'INDEX';
  FLD_DIST_M    = 'Dist_m';
  FLD_DIST_KM   = 'Dist_KM';
  FLD_REL_H     = 'Rel_H';
  FLD_CLU_H     = 'Clu_H';
  FLD_CLU_code  = 'Clu_code';
  FLD_CLU_color = 'clu_color';
  FLD_earth_h   = 'earth_h';



//-------------------------------------------------------------------
procedure Tdlg_Profile_Rel_Grid.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  cxGrid1.Align:=alClient;

  FRegPath:=REGISTRY_COMMON_FORMS + ClassName + '\2\';

  cxGrid1DBTableView1.RestoreFromRegistry(FRegPath + cxGrid1DBTableView1.Name);

  mem_Relief.SortedField:=FLD_DIST_M;


 // cxGrid1DBTableView1.DataController.KeyFieldNames :=FLD_INDEX;

//   i:=FieldByName(FLD_INDEX).AsInteger;

  cxGrid1DBTableView1.DataController.KeyFieldNames :=FLD_RECNO;

  col_recno1.DataBinding.FieldName:=FLD_RECNO;

  mem_Clutters1.SortedField:=FLD_DIST_M;


  act_Update_Clu_H.Caption    :='�������� ������ �� [m]';
  act_Update_Clu_Code.Caption :='�������� ��� ��';
  act_Update_Rel_H.Caption    :='�������� ������ ������� [m]';

 // mem_Clutters1.sav

 // col_Dist_.DisplayFormat:=',0;-,0';
end;


procedure Tdlg_Profile_Rel_Grid.FormDestroy(Sender: TObject);
begin
  cxGrid1DBTableView1.StoreToRegistry(FRegPath + cxGrid1DBTableView1.Name);

end;

procedure Tdlg_Profile_Rel_Grid.mem_Relief_AfterDelete(DataSet: TDataSet);
begin
  FIsModify:=True;
//  UpdateRecNo();
end;


//---------------------------------------------------
procedure Tdlg_Profile_Rel_Grid.View(aProfile: TrelProfile);
//---------------------------------------------------
var i,iCluCode:integer;
  e: double;
begin
  Assert(Assigned(aProfile));

  FRelProfileRef:=aProfile;

//  if not Assigned(aProfile) then
  //  Exit;

  db_Clear(mem_Clutters1);


 // Assert( Length(aProfile.Clutters.Items)<30);

  for i:=0 to High(aProfile.Clutters.Items) do
    if aProfile.Clutters.Items[i].IsUsed then
      db_AddRecord (mem_Clutters1,
        [
          db_Par(FLD_code, aProfile.Clutters.Items[i].Code),
          db_Par(FLD_name, aProfile.Clutters.Items[i].Name)
        ]);


  aProfile.SaveToDataset(mem_Relief);

  FIsModify:=False;
end;


//---------------------------------------------------
procedure Tdlg_Profile_Rel_Grid.SaveToProfileObj;
//---------------------------------------------------
begin
  Assert(Assigned(FRelProfileRef));

  FRelProfileRef.LoadFromDataset(mem_Relief);

  if Assigned(FOnSave) then
    FOnSave(Self);

  FIsModify:=False;
end;


procedure Tdlg_Profile_Rel_Grid.SetReadOnly(aValue: boolean);
begin
  cxGrid1DBTableView1.OptionsData.Editing :=not aValue;
  cxGrid1DBTableView1.OptionsSelection.CellSelect:=not aValue;
end;


procedure Tdlg_Profile_Rel_Grid.act_RefreshExecute(Sender: TObject);
begin
  View(FRelProfileRef);
end;

procedure Tdlg_Profile_Rel_Grid.act_SaveExecute(Sender: TObject);
begin
  SaveToProfileObj ();
end;


procedure Tdlg_Profile_Rel_Grid.CheckBox1Click(Sender: TObject);
begin
 // SetReadOnly (CheckBox1.Checked);
end;

procedure Tdlg_Profile_Rel_Grid.mem_Relief_AfterEdit(DataSet: TDataSet);
begin
  FIsModify:=True;
  DataSet['is_modify'] := True;
end;


procedure Tdlg_Profile_Rel_Grid.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
begin
  act_Save.Enabled:=FIsModify;
end;

procedure Tdlg_Profile_Rel_Grid.act_Update_Rel_HExecute(Sender: TObject);
begin
  if Sender=act_Update_Rel_H then
    Update_Rel_H;

  if Sender=act_Update_Clu_H then
    Update_Clutter_H;

  if Sender=act_Update_Clu_code then
    Update_Clutter_code;


end;

procedure Tdlg_Profile_Rel_Grid.ToolButton1Click(Sender: TObject);
begin

end;

// ---------------------------------------------------------------
procedure Tdlg_Profile_Rel_Grid.Update_Rel_H;
// ---------------------------------------------------------------
var
  iH : Integer;
  sValue: string;
begin
  sValue :='0';

  if InputQuery('�������� ������ ������� [m]','��������', sValue) then
  begin
    iH  := AsInteger(sValue);

    cx_UpdateSelectedItems_Table(cxGrid1DBTableView1, FLD_REL_H, iH);
  end;
end;

// ---------------------------------------------------------------
procedure Tdlg_Profile_Rel_Grid.Update_Clutter_H;
// ---------------------------------------------------------------
var
  iH : Integer;
  sValue: string;
begin
  sValue :='0';

  if InputQuery('�������� ������ �� [m]','��������', sValue) then
  begin
    iH  := AsInteger(sValue);

    cx_UpdateSelectedItems_Table(cxGrid1DBTableView1, FLD_CLU_H, iH);
  end;
end;

// ---------------------------------------------------------------
procedure Tdlg_Profile_Rel_Grid.Update_Clutter_Code;
// ---------------------------------------------------------------
var
  iH : Integer;
  sValue: string;
begin
  sValue :='0';

  if InputQuery('�������� ��� ��','��������', sValue) then
  begin
    iH  := AsInteger(sValue);

    cx_UpdateSelectedItems_Table(cxGrid1DBTableView1, FLD_CLU_Code, iH);
  end;
end;



end.
