unit d_Relief_update;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, DBCtrls, DB, cxGridDBTableView,


  u_cx;

type
  Tdlg_Relief_update11 = class(TForm)
    DBLookupListBox1: TDBLookupListBox;
    SpinEdit1: TSpinEdit;
    rb_C: TRadioButton;
    RadioButton2: TRadioButton;
    Button1: TButton;
    GroupBox1: TGroupBox;
    CheckBox1: TCheckBox;
    SpinEdit2: TSpinEdit;
  private
    FGrid: TcxGridDBTableView;
    { Private declarations }
  public
    class procedure ExecDlg(aGrid: TcxGridDBTableView);

    { Public declarations }
  end;

implementation

{$R *.dfm}


class procedure Tdlg_Relief_update11.ExecDlg(aGrid: TcxGridDBTableView);
begin
  with Tdlg_Relief_update11.Create(Application) do
  begin
    FGrid:=aGrid;

    ShowModal;
    Free;
  end;
end;


end.

