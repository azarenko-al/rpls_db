unit I_Act_Profile;

interface

uses
  u_geo;


type
  TProfileBuildType = (
    prtNone,

    DEF_PROFILE_POINTS,
    DEF_PROFILE_BETWEEN_BS,
    DEF_PROFILE_BY_COORD,
    DEF_PROFILE_BY_FIXED_POINT,
    DEF_PROFILE_BETWEEN_BS_AND_POINT
  );



(*
  IAct_Profile_X = interface(IInterface)
  ['{62B526CA-584D-47E4-9F54-808D5B1A861A}']

    procedure BuildByVector(aProfileType: TProfileBuildType; aBLVector: TBLVector);
    procedure ShowForm;// (aFormStyle: TFormStyle=fsNormal);

  end;
*)

(*
var
  IAct_Profile: IAct_Profile_X;
*)

implementation

end.

