unit f_Profile;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, ActnList, Registry,
  Dialogs, ExtCtrls, rxPlacemnt,  StdCtrls, ToolWin, ComCtrls, RxDBComb,
  Db, ADODB, Variants,
  cxPropertiesStore, cxControls, cxContainer, cxEdit, cxTextEdit,

  u_dxbar,
  u_Storage,

  dm_Main,

//  I_Act_Explorer, //dm_Act_Explorer,

 // I_Act_Link, //dm_Act_Link,

  I_Act_Profile,

  dm_Rel_Engine,                                       

  u_rel_Profile,

  I_Shell,
  I_MapAct,
  I_MapView,
//  I_Link,
//  I_Property,
       
  u_Geo,

  u_const,
  u_const_db,
  u_const_str,
  u_types,

  u_func,
  u_dlg,
  u_db,
 // u_reg,

  u_classes,

  dm_ClutterModel,

  d_Profile_AddByExactCoord,

  fr_profile_Rel,



  cxMaskEdit, cxButtonEdit, Mask, rxToolEdit, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters,  dxSkinsDefaultPainters, dxBar,
  cxBarEditItem, cxClasses, dxBarExtItems, dxSkinsCore, dxSkinsdxBarPainter

  ;

type
  Tfrm_Profile = class(TForm)
    Panel1: TPanel;
    ActionList1: TActionList;
    act_Show_AntInfo: TAction;
    act_LinkCreate: TAction;
    act_Select_Coord: TAction;
    timer_RelProfile: TTimer;
    FormStorage1: TFormStorage;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    ed_Step: TdxBarCombo;
    ed_ClutterModel: TcxBarEditItem;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarManager1Bar2: TdxBar;
    ed_H1: TdxBarSpinEdit;
    ed_H2: TdxBarSpinEdit;
    ed_Freq_GHz: TdxBarSpinEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
//    procedure ed_ClutterModel1ButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure timer_RelProfileTimer(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//    procedure ed_ClutterModelClick(Sender: TObject);

    procedure DoAction (Sender: TObject);
//    procedure DoChangeEdits(Sender: TObject);
    procedure ed_H1Change(Sender: TObject);
    procedure ed_ClutterModelPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxBarEditItem_ClutterPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure ed_H1ButtonClick(Sender: TdxBarSpinEdit; Button:
        TdxBarSpinEditButton);

  private
    FOldVector, FTempVector: TBLVector;

    FRegPath: string;

    FIMapView: IMapViewX;

  //  FProfileType: string;
    FProfileType: TProfileBuildType;


    FIsBuildEnabled: boolean;
    FUpdated: boolean;

    FRelProfile: TRelProfile;

    //-----------------------------------------------
    FClutterModelID: integer;

    //-----------------------------------------------

    FBLVector: TBLVector;
    FStep: double;

///////    Ffrm_profile_EMP: Tframe_profile_EMP;
    Fframe_profile_Rel: Tframe_profile_Rel;

 //   procedure ShowProfile();

{    procedure DoLoadEMP();
    procedure ShowStation ();
}
    procedure AddMapUserLine(aBLVector: TBLVector);
  //  procedure GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean);

    procedure SetFirstPoint(aBLPoint: TBLPoint);

    procedure Build1(aIsOpenMatrixes: boolean = true);

    procedure LoadFromReg;
    procedure SaveToReg;

  public
  //  constructor Create(AOwner: TComponent);

    procedure BuildByVector(aProfileType:TProfileBuildType;
    //aProfileType: string;
              aBLVector: TBLVector;
              aSite1Name,aSite2Name: string);

//    class function CreateForm: Tfrm_Profile;
    class function ShowForm: Tfrm_Profile;
  end;


//==================================================================
implementation
   {$R *.dfm}

uses
  dm_Act_Explorer,
  dm_Act_Link;

 
const
  STR_SHOW_ANTENNAS   = '�������� �������';
  STR_REMOVE_ANTENNAS = '������ �������';

(*
const
  REGISTRY_COMMON_FORMS  = 'Software\Onega\Common\Forms\';
*)

//
////------------------------------------------------------------------
//constructor Tfrm_Profile.Create(AOwner: TComponent);
////------------------------------------------------------------------
//begin
//  inherited;
//
////  if Assigned(Application.MainForm) then
// //   if Application.MainForm.FormStyle=fsMDIForm then
//  //    FormStyle:=fsMDIChild;
//end;


//------------------------------------------------------------------
class function Tfrm_Profile.ShowForm: Tfrm_Profile;
//------------------------------------------------------------------
var oForm: TForm;
begin
  oForm:=IsFormExists (Application, Tfrm_Profile.ClassName);

  if not Assigned(oForm) then
    oForm:=Tfrm_Profile.Create(Application);

  oForm.Show;

  Result:=Tfrm_Profile(oForm);
end;


//--------------------------------------------------------------------
procedure Tfrm_Profile.FormCreate (Sender: TObject);
//--------------------------------------------------------------------
var
  i: integer;
begin
  inherited;

  {
   StoredProps.Strings = (
      'ed_Freq_GHz.Text'
      'ed_Height1.Text'
      'ed_Height2.Text'
      'ed_Step1.Text'
      'dxBarCombo_Step.Text'
      'dxBarSpinEdit_Freq.Value'
      'dxBarSpinEdit_H1.Value'
      'dxBarSpinEdit_H2.Value')
   }


  bar_Init(dxBarManager1);


  FIMapView:=IActiveMapView;

  FRelProfile:=TRelProfile.Create;

//  FUpdated1:= false;
  FUpdated:= true;


//  pn_Relief.Align:= alClient;


//  FRegPath:=REGISTRY_COMMON_FORMS + ClassName + '\';


  FRegPath:=g_Storage.GetPathByClass(ClassName);



  
//  cxPropertiesStore1.StorageName:=FRegPath;
{
  FormStorage1.IniFileName:=FRegPath;
  FormStorage1.Active := True;
  FormStorage1.RestoreFormPlacement;
}

  Caption:='�������';

  timer_RelProfile.Interval := 100;

  act_Select_Coord.Caption := '������ ����������';
//  act_Show_AntInfo.Caption  := STR_REMOVE_ANTENNAS;
  act_LinkCreate.Caption    := '������� ��������';

  CreateChildForm(Tframe_profile_Rel,  Fframe_profile_Rel, Self);

  Fframe_profile_Rel.RelProfile_Ref:=FRelProfile;


  SetLength(Fframe_Profile_Rel.Params.SiteGroups,1);
  SetLength(Fframe_Profile_Rel.Params.SiteGroups[0].Site1.AntennaHeights,1);
  SetLength(Fframe_Profile_Rel.Params.SiteGroups[0].Site2.AntennaHeights,1);

////////////////////  Fframe_Profile_Rel.Params.IsShowFrenelZoneTop:=True;

  Fframe_profile_Rel.SetSiteGroupCount(1);

//z  Fframe_profile_Rel:= Tframe_profile_Rel.CreateChildForm ( pn_Relief);
///  Ffrm_profile_EMP:=   Tframe_profile_EMP.CreateChildForm ( pn_EMP);


/////  Ffrm_Profile_EMP.Init (0); ///////// gl_MapMatrixList.GetBest6Zone(0));

  FIsBuildEnabled:= true;

  with TRegIniFile.Create(FRegPath) do
  begin
//    BeginGroup (Self, Name);
 //   AddControl (ed_Step, [PROP_TEXT]);
    ed_Step.Text:=ReadString (FRegPath, 'Step', '50');

//    ed_Step1.Text   :=ReadString (FRegPath, ed_Step1.name, '50');
    FClutterModelID:=ReadInteger(FRegPath, FLD_CLUTTER_MODEL_ID, 0);

    Free;
  end;


  if FClutterModelID<=0 then
    FClutterModelID:=dmClutterModel.GetDefaultID;

//    FClutterModelID:=gl_DB.GetFieldValue (TBL_CLUTTER_MODEL, FLD_ID, []);

    {                          aTableName : string;
                              aFieldName : string;
                              aParams    : array of TDBParamRec): Variant;
}

 // cxBarEditItem_Clutter.EditValue:=gl_DB.GetNameByID (TBL_CLUTTERMODEL, FClutterModelID);

  ed_ClutterModel.EditValue:=gl_DB.GetNameByID (TBL_CLUTTERMODEL, FClutterModelID);
//  Fframe_profile_Rel.ClutterModelID:=FClutterModelID;

  dmRel_Engine.AssignClutterModel (FClutterModelID, FRelProfile.Clutters);

  FProfileType:=prtNone;// 'none';

  SetActionsExecuteProc([ //act_Show_AntInfo,
                          act_LinkCreate,
                          act_Select_Coord], DoAction);


//  FProcIndex:=RegisterProc (GetMessage, Name);
end;

//--------------------------------------------------------------------
procedure Tfrm_Profile.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  FreeAndNil(FRelProfile);


  with TRegIniFile.Create(FRegPath) do
  begin
    WriteInteger (FRegPath, FLD_CLUTTER_MODEL_ID, FClutterModelID);
    WriteString  (FRegPath, 'Step', ed_Step.text);
//    WriteString  (FRegPath, ed_Step1.name, ed_Step1.text);

 //   WriteInteger (FRegPath, 'Left', Left);
  //  WriteInteger (FRegPath, 'Top',  Top);

//    BeginGroup (Self, Name);
 //   AddControl (ed_Step, [PROP_TEXT]);
//    ed_Step.Text   :=ReadInteger (FRegPath, , 50);
  //  FClutterModelID:=ReadInteger (FRegPath, FLD_CLUTTER_MODEL_ID, 0);
    Free;
  end;

 // reg_WriteInteger (FRegPath, FLD_CLUTTER_MODEL_ID, Fframe_profile_Rel.ClutterModelID);
//  gl_Reg.SaveAndClearGroup (Self);

//  Fframe_profile_Rel.Free;

//  UnRegisterProc (FProcIndex);

  if Assigned(FIMapView) then
  begin
    FIMapView.SetDefaultTool;
    FIMapView.UserLayerDestroy;
  end;
//  PostEvent(WE_MAP_SET_DEFAULT_TOOL);

  inherited;
end;


//------------------------------------------------------------------------
procedure Tfrm_Profile.FormClose(Sender: TObject; var Action: TCloseAction);
//------------------------------------------------------------------------
begin
  Action:=caFree;
end;


//--------------------------------------------------------------------
procedure Tfrm_Profile.BuildByVector (
   aProfileType:TProfileBuildType;
//aProfileType: string;
                                      aBLVector: TBLVector;
                                      aSite1Name,aSite2Name: string);
//--------------------------------------------------------------------
var
  bProfileIsByFixedPoint: boolean;
  iSite: integer;

begin

  if FProfileType <> aProfileType then
  begin
    FProfileType:= aProfileType;

    bProfileIsByFixedPoint:= (FProfileType = DEF_PROFILE_BY_FIXED_POINT);

//////////////        pn_Emp.Visible:= bProfileIsByFixedPoint;
//////////////        tb_BS_and_Sectors.Visible:= bProfileIsByFixedPoint;
  end;

  bProfileIsByFixedPoint:=   (FProfileType = DEF_PROFILE_BY_FIXED_POINT);
  Timer_RelProfile.Enabled:=  bProfileIsByFixedPoint;


  FBLVector:=aBLVector;
 // FStep:=AsFloat(ed_Step1.Text);
  FStep:=AsFloat(ed_Step.Text);



 // Fframe_profile_Rel.BLVector:=aBLVector;
//  Fframe_profile_Rel.Step:=AsInteger(ed_Step.Text);

  Fframe_profile_Rel.Site1Name:= aSite1Name;
  Fframe_profile_Rel.Site2Name:= aSite2Name;

//  with Fframe_profile_Rel do
 // begin
  FTempVector:=aBLVector;

{  Site1Name:='';
  Site2Name:='';
}
//      if not VarIsNull(aParams.ValueByName('Name1'))  then

//        if not VarIsNull(aParams.ValueByName('Name2'))  then


  if not geo_ComparePoints(aBLVector.Point1, aBLVector.Point2) then
  begin
  //  FBLVector:=
  //  Build1 (not bProfileIsByFixedPoint);

   // Fframe_profile_Rel.
    Build1 (not bProfileIsByFixedPoint);

    if not bProfileIsByFixedPoint then
      AddMapUserLine (aBLVector);
  end;

 // end;

//  ShowProfile();

//  FUpdated1:= false;
end;


//------------------------------------------------------------------------
procedure Tfrm_Profile.timer_RelProfileTimer(Sender: TObject);
//------------------------------------------------------------------------
begin
  if not geo_Eq(FOldVector, FTempVector) then
    FUpdated:= false;

  if geo_Eq(FOldVector, FTempVector) and (not FUpdated) then
  begin
    AddMapUserLine (FTempVector);
    FUpdated:= true;
  end;

  FOldVector:= FTempVector;
end;



//------------------------------------------------------------------------
procedure Tfrm_Profile.AddMapUserLine(aBLVector: TBLVector);
//------------------------------------------------------------------------
begin
  if Assigned(FIMapView) then
  begin
    FIMapView.MAP_ADD_USER_LINE(clRed, 1, aBLVector.Point1, aBLVector.Point2);
    FIMapView.UserLayerDraw;
  end;

end;

 //------------------------------------------------------------------------
procedure Tfrm_Profile.ActionList1Update(Action: TBasicAction;  var Handled: Boolean);
//------------------------------------------------------------------------
begin
  act_Select_Coord.Enabled:= not (FProfileType = DEF_PROFILE_BETWEEN_BS);
end;


//------------------------------------------------------------------------
procedure Tfrm_Profile.SetFirstPoint(aBLPoint: TBLPoint);
//------------------------------------------------------------------------
begin
  if Assigned(FIMapView) then
    FIMapView.ChangeProfileFirstPointPos (aBLPoint);

//  PostBLPoint (WE_MAP_CHANGE_FIRST_POINT, aBLPoint);

//  PostEvent(WE_MAP_CHANGE_FIRST_POINT,
  //  [app_Par(PAR_BLPOINT, @aBLPoint)
 //   ,
   //  app_Par(PAR_LON, aPoint.L)

 //    ]);
end;





//------------------------------------------------------------------------
procedure Tfrm_Profile.DoAction (Sender: TObject);
//------------------------------------------------------------------------
var
  iID: Integer;
  oBLVector: TBLVector;
begin
  //------------------------------
  if Sender=act_Show_AntInfo then
  //------------------------------
  begin

 //   with Fframe_profile_Rel do
  //  begin
    //  Profile_Rel_Graph.Params.IsShowAntennas:=
     //   IIF(Profile_Rel_Graph.Params.IsShowAntennas=True, False, True);

     // Profile_Rel_Graph.Params.IsShowSites:=Profile_Rel_Graph.Params.IsShowAntennas;

 {     if Profile_Rel_Graph.Params.IsShowAntennas then
        act_Show_AntInfo.Caption := STR_REMOVE_ANTENNAS
      else
        act_Show_AntInfo.Caption := STR_SHOW_ANTENNAS;
}

//      ShowProfile;
 //   end;

  end   else


  //------------------------------
  if Sender=act_LinkCreate then
  //------------------------------
  begin
  //  Assert(Assigned(ILink));

   if not Assigned(Fframe_profile_Rel.RelProfile_Ref) then
     Exit;

//   if Fframe_profile_Rel.RelProfile_Ref.Data.BLVector.Point1.B=0 then
  //   Exit;

//    with Fframe_profile_Rel do
    iID:= dmAct_Link.AddByProfile (Fframe_profile_Rel.RelProfile_Ref.Data.BLVector,
                        Fframe_profile_Rel.Params.SiteGroups[0].Site1.AntennaHeights[0],
                        Fframe_profile_Rel.Params.SiteGroups[0].Site2.AntennaHeights[0],
                        Fframe_profile_Rel.Params.Freq_MHz / 1000,
                        FProfileType);


    if iID>0 then
       dmAct_Explorer.Browser_ViewObjectByID(OBJ_LINK, iID);
//      Tfrm_Browser.ViewObjectByID (OBJ_LINK, iID);
  end   else


  //------------------------------
  if Sender=act_Select_Coord then
  //------------------------------
  begin
    FBLVector:= Tdlg_Profile_AddByExactCoord.ExecDlg(Fframe_profile_Rel.BLVector, FProfileType);

    FStep:= AsFloat(ed_Step.Text);

//    Fframe_profile_Rel.Step:= AsInteger(ed_Step.Text);

    if (FBLVector.Point1.b <> 0) or (FBLVector.Point1.l <> 0) or
       (FBLVector.Point2.l <> 0) or (FBLVector.Point2.b <> 0) then
    begin
      Build1();

//      Fframe_profile_Rel.bLVector:=FBLVector;
  //    Fframe_profile_Rel.RelProfile.Assign(FRelProfile);
    //  Fframe_profile_Rel.Build1;


    //  Fframe_profile_Rel.ShowProfile;
    end;

    AddMapUserLine (FBLVector);
  end;

end;



//----------------------------------------------------
procedure Tfrm_Profile.Build1(aIsOpenMatrixes: boolean = true);
//----------------------------------------------------
var b: boolean;
begin
  Assert(geo_Distance_m(FBLVector)>0, 'geo_Distance_m(FBLVector)>0');


//  RelProfile.Refraction:=Refraction;

  //dmRel_Engine.AssignClutterModel (FClutterModelID, FRelProfile.Clutters);


//  if Profile_XML='' then
 //   RelProfile.LoadFromXml(Profile_XML);


  if aIsOpenMatrixes then
 // begin
//    dmRel_Engine.OpenByVector (FBLVector);     //Self, ClassName,
    dmRel_Engine.Open ();     //Self, ClassName,
 //   b:=dmRel_Engine.BuildProfile (RelProfile, BLVector, Step);//, ClutterModelID);
 //   dmAct_Rel_Engine.UnRegisterSender (Self);
//  end ;
  // else
   // b:=dmRel_Engine.BuildProfile (RelProfile, BLVector, Step);//, ClutterModelID);


  b:=dmRel_Engine.BuildProfile1 (FRelProfile, FBLVector, FStep);//, ClutterModelID);

//  FRelProfile.SetRefraction(Refraction);

//  dmAct_Rel_Engine.ProfileBuild (RelProfile, BLVector,
//                              Step, ClutterModelID);

 // RelProfile.CalcSummary();

   Fframe_profile_Rel.RelProfile_Ref:=FRelProfile;


//  Fframe_profile_Rel.Profile_Rel_Graph.Params.SiteGroups[0].Site1.Rel_H:=

  Assert(Assigned(Fframe_profile_Rel.RelProfile_Ref), 'Fframe_profile_Rel.RelProfile not assigned');


  Fframe_profile_Rel.RelProfile_Ref.Assign(FRelProfile);

//  Fframe_Profile_Rel.Params.IsShowFrenelZoneTop:=True;
//  Fframe_Profile_Rel.Params.Freq_MHz := AsFloat(ed_Freq_GHz.Text)*1000;
  Fframe_Profile_Rel.Params.Freq_MHz := ed_Freq_GHz.Value*1000;
  Fframe_profile_Rel.Params.SiteGroups[0].Site1.AntennaHeights[0]:=ed_H1.Value;
  Fframe_profile_Rel.Params.SiteGroups[0].Site2.AntennaHeights[0]:=ed_H2.Value;
 // Fframe_profile_Rel.Params.Freq:=AsFloat(ed_Freq.Text)*1000;


  Fframe_Profile_Rel.Params.SiteGroups[0].Distance_KM:=geo_Distance_m(FBLVector)/1000;
  Fframe_Profile_Rel.Params.SiteGroups[0].Site1.Rel_H:=FrelProfile.FirstItem().Rel_H;
  Fframe_Profile_Rel.Params.SiteGroups[0].Site2.Rel_H:=FrelProfile.LastItem().Rel_H;



//  Fframe_profile_Rel.ClutterModelID:=FClutterModelID;
  Fframe_profile_Rel.bLVector:=FBLVector;
  Fframe_profile_Rel.ShowProfile();;

//  ShowProfile();

////////
// remove close
//  dmRel_Engine.Close1;
end;



// -------------------------------------------------------------------
procedure Tfrm_Profile.LoadFromReg;
// -------------------------------------------------------------------
begin
  with TRegIniFile.Create(FRegPath) do
  begin
    ed_H1.Text      :=ReadString ('', ed_H1.Name, '30');
    ed_H2.Text      :=ReadString ('', ed_H2.Name, '30');
    ed_Freq_GHz.Text:=ReadString ('', ed_Freq_GHz.Name,    '7');

    Free;
  end;

{  SetLength(Params.SiteGroups,1);
  Params.SiteGroups
}


{  Fframe_profile_Rel.Profile_Rel_Graph.Params.SiteGroups[0].Site1.Antennas[0]:=AsFloat(ed_Height1.Text);
  Fframe_profile_Rel.Profile_Rel_Graph.Params.SiteGroups[0].Site2.Antennas[0]:=AsFloat(ed_Height2.Text);
}

  Fframe_profile_Rel.Params.SiteGroups[0].Site1.AntennaHeights[0]:=ed_H1.Value;
  Fframe_profile_Rel.Params.SiteGroups[0].Site2.AntennaHeights[0]:=ed_H2.Value;


{  Params.Site1.Antennas[0]:=AsFloat(ed_Height1.Text);
  Params.Site2.Antennas[0]:=AsFloat(ed_Height2.Text);
}
  Fframe_profile_Rel.Params.Freq_MHz:=ed_Freq_GHz.Value*1000;

end;



// -------------------------------------------------------------------
procedure Tfrm_Profile.SaveToReg;
// -------------------------------------------------------------------
begin
  with TRegIniFile.Create(FRegPath) do
  begin
    WriteString  ('', ed_H1.Name, ed_H1.Text);
    WriteString  ('', ed_H2.Name, ed_H2.Text);
    WriteString  ('', ed_Freq_GHz.Name,ed_Freq_GHz.Text);

    Free;
  end;
end;


procedure Tfrm_Profile.ed_H1ButtonClick(Sender: TdxBarSpinEdit; Button:   TdxBarSpinEditButton);
begin
  ed_H1Change(Sender);
end;


//-------------------------------------------------------------------
procedure Tfrm_Profile.ed_H1Change(Sender: TObject);
//-------------------------------------------------------------------
begin

  if Sender = ed_H1 then
    Fframe_profile_Rel.Params.SiteGroups[0].Site1.AntennaHeights[0]:= ed_H1.CurValue;


  if Sender = ed_H2 then
    Fframe_profile_Rel.Params.SiteGroups[0].Site2.AntennaHeights[0]:= ed_H2.CurValue;

  if Sender = ed_Freq_GHz then
    Fframe_profile_Rel.Params.Freq_MHz:= ed_Freq_GHz.CurValue * 1000;


//  Fframe_profile_Rel.Profile_Rel_Graph.Params.IsShowFrenelZoneTop:=True;
  Fframe_profile_Rel.Params.IsShowFrenelZoneTop:=True;
  Fframe_profile_Rel.ShowProfile;

{  UpdateMinMaxHeights;
  ShowSites();
}

  SaveToReg;

end;



procedure Tfrm_Profile.ed_ClutterModelPropertiesButtonClick( Sender: TObject; AButtonIndex: Integer);
begin
  if dmAct_Explorer.Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otClutterModel, FClutterModelID) then
    dmRel_Engine.AssignClutterModel (FClutterModelID, FRelProfile.Clutters);

end;

procedure Tfrm_Profile.cxBarEditItem_ClutterPropertiesButtonClick( Sender: TObject; AButtonIndex: Integer);
begin
  if dmAct_Explorer.Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otClutterModel, FClutterModelID) then
    dmRel_Engine.AssignClutterModel (FClutterModelID, FRelProfile.Clutters);

end;


end.






  (*






//-------------------------------------------------------------------
procedure Tfrm_Profile.DoChangeEdits(Sender: TObject);
//-------------------------------------------------------------------
begin

  if Sender = ed_H1 then
  begin
    if ed_H1.Text <> '' then
      //Fframe_profile_Rel.Profile_Rel_Graph.Params.SiteGroups[0].Site1.Antennas[0]:= AsFloat(ed_Height1.Text);
      Fframe_profile_Rel.Params.SiteGroups[0].Site1.AntennaHeights[0]:= ed_H1.Value;
  end  else

  if Sender = ed_H2 then
  begin
    if ed_H2.Text <> '' then
  //    Fframe_profile_Rel.Profile_Rel_Graph.Params.SiteGroups[0].Site2.Antennas[0]:= AsFloat(ed_Height2.Text);
      Fframe_profile_Rel.Params.SiteGroups[0].Site2.AntennaHeights[0]:= ed_H2.Value;
  end  else

  if Sender = ed_Freq_GHz then
  begin
    if ed_Freq_GHz.Text <> '' then
   //   Fframe_profile_Rel.Profile_Rel_Graph.Params.Freq:= AsFloat(ed_Freq.Text)*1000;
      Fframe_profile_Rel.Params.Freq_MHz:= ed_Freq_GHz.Value*1000;
  end;

//  Fframe_profile_Rel.Profile_Rel_Graph.Params.IsShowFrenelZoneTop:=True;
  Fframe_profile_Rel.Params.IsShowFrenelZoneTop:=True;
  Fframe_profile_Rel.ShowProfile;

{  UpdateMinMaxHeights;
  ShowSites();
}

  SaveToReg;

end;

