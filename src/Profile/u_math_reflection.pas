unit u_math_reflection;

interface
uses Math;


type
  TXY = record
    x,y: double;
  end;

function MakeXY(aX,aY: double): TXY;


type
  TReflectionRec = record
     Radius : Double;

     A: TXY;
     B,C: TXY;

     Result: record

       Line: array[0..1] of TXY;
       Arc: array of TXY;
     end;

  end;


procedure CalcReflectionPoints(var aRec: TReflectionRec);


implementation


function GetMovedPos(aXY: TXY; aCenter: TXY; aAngle: double): TXY; forward;

function GetAngleA(A,B,C: TXY): Double; forward;

function GetLineCenter(A,B: TXY): TXY; forward;
function GetLineTilt(A,B: TXY): Double; forward;


//---------------------------------------------------------------------------
function MakeXY(aX,aY: double): TXY;
//---------------------------------------------------------------------------
begin
  Result.x  :=aX;
  Result.y  :=aY;
end;


//---------------------------------------------------------------------------
function GetAngleA(A,B,C: TXY): Double;
//---------------------------------------------------------------------------
// ???? ????? ???????
//---------------------------------------------------------------------------
var
  angle,a1,e: double;
begin

  e:= ( (b.x-a.x)*(c.x-a.x) + (b.y-a.y)*(c.y-a.y) )   /
      (sqrt (sqr(b.x - a.x)  + sqr(b.y-a.y) ) *
       sqrt (sqr(c.x - a.x)  + sqr(c.y-a.y) ) );

  angle:=arccos(e);

  Result:=RadToDeg(angle);

end;


//---------------------------------------------------------------------------
function GetMovedPos(aXY: TXY; aCenter: TXY; aAngle: double): TXY;
//---------------------------------------------------------------------------
//http://mathhelpplanet.com/static.php?p=pryeobrazovaniya-pryamougolnyh-koordinat
var
  eRad,e1,e2: Double;
begin
  eRad:=DegToRad(aAngle);


  e1:=cos(  Pi);
  e1:=cos(  Pi/2);

  e1:=cos( eRad);
  e2:=sin(eRad);

  result.x:= aXY.X * cos(eRad) + aXY.Y * sin(eRad);
  result.Y:=-aXY.X * sin(eRad) + aXY.Y * cos(eRad);

  result.x:=result.x + aCenter.X;
  result.Y:=result.Y + aCenter.Y;

end;

//--------------------------------------------------------------------
procedure CalcReflectionPoints(var aRec: TReflectionRec);
//--------------------------------------------------------------------
const
  DEF_COUNT = 11;
var
  angle: double;
  eDelta: Double;
 // e: Double;
  eTilt: Double;
  eTilt_A_C: Double;
  I: Integer;
  x: Double;
  xy,xy1,xy2: TXY;
  y: Double;
begin
  angle:=GetAngleA (aRec.A, aRec.B, aRec.C);

  xy1:=MakeXY(-aRec.Radius, 0);
  xy2:=MakeXY(aRec.Radius, 0);


  eTilt_A_C:=GetLineTilt (aRec.A, aRec.C);

  eTilt:= 90  - (eTilt_A_C + angle/2);


  xy1:=GetMovedPos(xy1, aRec.A, eTilt);
  xy2:=GetMovedPos(xy2, aRec.A, eTilt);


  aRec.Result.Line[0]:=xy1;
  aRec.Result.Line[1]:=xy2;


  eDelta:=2*aRec.Radius / (DEF_COUNT-1);

  SetLength(aRec.Result.Arc, DEF_COUNT);

  for I := 0 to DEF_COUNT-1 do
  begin
    x:= - aRec.Radius + eDelta*i;
    y:= - sqr(x) ;

    xy:=MakeXY(x,y);

    xy:=GetMovedPos(xy, aRec.A, eTilt);


    aRec.Result.Arc[i]:=xy;
  end;


end;



procedure CalcReflectionPoints_test;
var
  r: TReflectionRec;

begin
  FillChar(r, sizeOf(r),0);

  r.Radius:=5;

  r.A:=MakeXY(0,0);

  r.B:=MakeXY(0,10);
  r.C:=MakeXY(10,0);

//    angle:=GetAngleA (MakeXY(0,10), MakeXY(0,0), MakeXY(10,0));


  CalcReflectionPoints (r);

  // TODO -cMM: CalcReflectionPoints_test default body inserted
end;

function GetLineCenter(A,B: TXY): TXY;
begin
  Result.X := (a.x  + b.x) / 2;
  Result.Y := (a.y  + b.y) / 2;
end;

//--------------------------------------------------------------------
function GetLineTilt(A,B: TXY): Double;
//--------------------------------------------------------------------
var
  e: Double;
begin
  if ABS(a.x-b.x) < 0.001 then
    Result:=90
  else
  begin
    e:= (a.y-b.y) / ( a.x-b.x);
    e:=ArcTan(e);

    Result:=RadToDeg(e);
  end;

//  Result.X := (a.x  + b.x) / 2;
//  Result.Y := (a.y  + b.y) / 2;
end;



end.



    {



procedure TForm12.FormCreate(Sender: TObject);
var
  angle: double;
  xy1,xy2: TXY;

begin
  angle:=GetAngleA (MakeXY(0,10), MakeXY(0,0), MakeXY(10,0));


  xy1:=GetMovedPos (MakeXY(10,0), MakeXY(0,0), 180/2);



end;

end.

}