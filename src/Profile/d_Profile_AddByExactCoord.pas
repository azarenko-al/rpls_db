unit d_Profile_AddByExactCoord;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rxPlacemnt, ActnList, StdCtrls, ExtCtrls,


  d_Wizard,

  I_Act_Profile,

  
  fr_geo_Coordinates,

//  dm_Options,
  I_Options_,
//  X_Options,


  u_geo,
  u_func, cxPropertiesStore, cxLookAndFeels

  //d_Coordinates

  ;

type
  Tdlg_Profile_AddByExactCoord = class(Tdlg_Wizard)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
  private
//    FAngle1_2, FAngle2_2, FAngle1, FAngle2: TAngle;

    FBLPoint,  FBLPoint2: TBLPoint;
    FBLVector: TBLVector;

    Fframe_geo_Coordinates1: Tframe_geo_Coordinates;
    Fframe_geo_Coordinates2: Tframe_geo_Coordinates;

  public
    class function ExecDlg(aBLVector: TBLVector; aProfileType:TProfileBuildType):
        TBLVector;
//    aProfileType: string): TBLVector;
  end;



//==============================================================
//  implementation
//==============================================================
implementation {$R *.DFM}



//--------------------------------------------------------------------
class function Tdlg_Profile_AddByExactCoord.ExecDlg(aBLVector: TBLVector;
    aProfileType:TProfileBuildType): TBLVector;
//--------------------------------------------------------------------
begin
  with Tdlg_Profile_AddByExactCoord.Create(Application) do
  begin
    FBLVector:=aBLVector;

    Fframe_geo_Coordinates1.Set_BLPoint_and_CoordSys (FBLVector.Point1, EK_CK_42);
    Fframe_geo_Coordinates2.Set_BLPoint_and_CoordSys (FBLVector.Point2, EK_CK_42);

{
    Fframe_geo_Coordinates1.BLPoint_Pulkovo:=FBLVector.Point1;
    Fframe_geo_Coordinates2.BLPoint_Pulkovo:=FBLVector.Point2;

    Fframe_geo_Coordinates1.DisplayCoordSys:=IOptions.Get_CoordSys;
    Fframe_geo_Coordinates2.DisplayCoordSys:=IOptions.Get_CoordSys;
 }

//    ed_Coords1.Text:=geo_FormatBLPoint(FBLVector.Point1);
  //  ed_Coords2.Text:=geo_FormatBLPoint(FBLVector.Point2);

   // if aProfileType = 'BETWEEN_BS_AND_POINT' then
  //    ed_Coords1.OnButtonClick:=nil;

    if ShowModal=mrOk then
    begin
      Result.Point1:=Fframe_geo_Coordinates1.GetBLPoint_Pulkovo;
      Result.Point2:=Fframe_geo_Coordinates2.GetBLPoint_Pulkovo;
    end else
    begin
      Result.Point1:=NULL_BLPOINT;
      Result.Point2:=NULL_BLPOINT;
    end;
  end;
end;

// ---------------------------------------------------------------
procedure Tdlg_Profile_AddByExactCoord.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;

  CreateChildForm(Tframe_geo_Coordinates, Fframe_geo_Coordinates1, GroupBox1);
  CreateChildForm(Tframe_geo_Coordinates, Fframe_geo_Coordinates2, GroupBox2);

  GroupBox1.Height:=Fframe_geo_Coordinates2.GetBestHeight;
  GroupBox2.Height:=GroupBox1.Height;

 // GetBestHeight

  SetDefaultSize;

//  Fframe_geo_Coordinates1:=Tframe_geo_Coordinates.CreateChildForm (GroupBox1);
//  Fframe_geo_Coordinates2:=Tframe_geo_Coordinates.CreateChildForm (GroupBox2);
end;


procedure Tdlg_Profile_AddByExactCoord.FormDestroy(Sender: TObject);
begin
//  Fframe_geo_Coordinates1.Free;
//  Fframe_geo_Coordinates2.Free;

  inherited;
end;
//------------------------------------------------------------------
procedure Tdlg_Profile_AddByExactCoord.ActionList1Update(
  Action: TBasicAction; var Handled: Boolean);
//------------------------------------------------------------------
begin
//  act_ok.Enabled:=(ed_Coords1.Text <> '') and (ed_Coords2.Text <> '');
end;


//------------------------------------------------------------------
procedure Tdlg_Profile_AddByExactCoord.act_OkExecute(Sender: TObject);
//------------------------------------------------------------------
begin
  ////
end;




end.
