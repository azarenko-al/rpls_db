unit dm_act_Profile;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs,  Menus, ActnList,

  dm_Property,

 // u_const_msg,
  u_const_str,

  u_geo,
//  u_func_msg,

  I_Act_Profile,

  d_Profile_AddByExactCoord,
  f_Profile

  ;

type

//, IActProfileX
  TdmAct_Profile = class(TDataModule) //, IAct_Profile_X)
  
  //  procedure DataModuleDestroy(Sender: TObject);
//    procedure DataModuleDestroy(Sender: TObject);
  public
    procedure BuildByVector(aProfileType:TProfileBuildType; aBLVector: TBLVector);

    procedure ShowForm;// (aFormStyle: TFormStyle=fsNormal);

    class procedure Init;
  end;

var
  dmAct_Profile: TdmAct_Profile;

//==================================================================
//implementation
//==================================================================
implementation  {$R *.dfm}



//--------------------------------------------------------------------
class procedure TdmAct_Profile.Init;
//--------------------------------------------------------------------
begin
  Assert(not Assigned(dmAct_Profile));

 // if not Assigned(dmAct_Profile) then
  //begin
    Application.CreateForm(TdmAct_Profile, dmAct_Profile);

 //   dmAct_Profile:=TdmAct_Profile.Create(Application);

   // dmAct_Profile.GetInterface(IAct_Profile_X, IAct_Profile);
//    Assert(Assigned(IAct_Profile));
 // end;
end;

//
//procedure TdmAct_Profile.DataModuleDestroy(Sender: TObject);
//begin
////  IAct_Profile :=nil;
////  dmAct_Profile:=nil;
//
//  inherited;
//end;


//--------------------------------------------------------------------
procedure TdmAct_Profile.BuildByVector(aProfileType:TProfileBuildType;
    aBLVector: TBLVector);
//--------------------------------------------------------------------
var
  oBLVector: TBLVector;
  iID1, iID2: integer;
//  dHeight1, dHeight2: double;
  sName1,sName2: string;

  oForm: Tfrm_Profile;
begin
  sName1:='';
  sName2:='';


  if (aProfileType = DEF_PROFILE_POINTS) or
     (aProfileType = DEF_PROFILE_BY_FIXED_POINT)
  then
    //do nothing
  else

  //--------------------------------------------------------------------
  if aProfileType = DEF_PROFILE_BETWEEN_BS then
  //--------------------------------------------------------------------
  begin
    iID1:=dmProperty.GetNearestIDandPos (aBLVector.Point1, aBLVector.Point1, sName1);
  //  dHeight1:=
    iID2:=dmProperty.GetNearestIDandPos (aBLVector.Point2, aBLVector.Point2, sName2);
  //  dHeight2:=

 //   sName1:=dmProperty.GetNameByID(iID1);
  //  sName2:=dmProperty.GetNameByID(iID2);

  end else

  //--------------------------------------------------------------------
  if aProfileType = DEF_PROFILE_BETWEEN_BS_AND_POINT then begin
  //--------------------------------------------------------------------
    iID1  :=dmProperty.GetNearestIDandPos (aBLVector.Point1, aBLVector.Point1, sName1);
   // sName1:=dmProperty.GetNameByID(iID1);

  end else

  //--------------------------------------------------------------------
  if aProfileType = DEF_PROFILE_BY_COORD then
  //--------------------------------------------------------------------
  begin
    oBLVector:=TDlg_Profile_AddByExactCoord.ExecDlg(aBLVector, DEF_PROFILE_BY_COORD);

    if ((oBLVector.Point1.b = 0) or (oBLVector.Point1.l = 0) or
        (oBLVector.Point2.l = 0) or (oBLVector.Point2.b = 0)) then
      exit
    else
      aBLVector:=oBLVector;

    aProfileType:=prtNone;//, '';
  end else
    raise Exception.Create('procedure TdmAct_Profile.BuildByVector(aProfileType: string; aBLVector: TBLVector);');



  oForm:=Tfrm_Profile.ShowForm;

  oForm.BuildByVector(aProfileType, aBLVector, sName1, sName2);

end;



//--------------------------------------------------------------------
procedure TdmAct_Profile.ShowForm;// (aFormStyle: TFormStyle=fsNormal);
//--------------------------------------------------------------------
begin
  Tfrm_Profile.ShowForm;
end;




end.