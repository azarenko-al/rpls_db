inherited frm_Documents: Tfrm_Documents
  Left = 1100
  Top = 584
  Width = 714
  Height = 612
  Caption = 'fr_Documents'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    Width = 698
    Color = clAqua
  end
  object cxGrid1: TcxGrid [1]
    Left = 0
    Top = 85
    Width = 698
    Height = 219
    Align = alTop
    PopupMenu = PopupMenu1
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    object grid: TcxGridDBTableView
      OnDblClick = gridDblClick
      NavigatorButtons.ConfirmDelete = False
      FilterBox.Visible = fvAlways
      DataController.DataSource = ds_Doc
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.CellHints = True
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object col_id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Options.Editing = False
        VisibleForCustomization = False
      end
      object col_Name: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'name'
        Width = 183
      end
      object col_FileName: TcxGridDBColumn
        Caption = #1048#1084#1103' '#1092#1072#1081#1083#1072
        DataBinding.FieldName = 'FileName'
        Options.Editing = False
        Width = 192
      end
      object col_Date: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'date_created'
        PropertiesClassName = 'TcxDateEditProperties'
        Options.Editing = False
        SortIndex = 0
        SortOrder = soAscending
        Width = 141
      end
      object col_User: TcxGridDBColumn
        Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
        DataBinding.FieldName = 'user_created'
        Options.Editing = False
        Width = 97
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = grid
    end
  end
  inherited ActionList: TActionList
    OnUpdate = ActionListUpdate
    object act_View_Doc: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088#1077#1090#1100
      OnExecute = _Action_exec
    end
  end
  object ds_Doc: TDataSource
    DataSet = qry_Doc
    Left = 264
    Top = 8
  end
  object qry_Doc: TADOQuery
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=SQL_ONEGA'
    CursorType = ctStatic
    OnCalcFields = qry_DocCalcFields
    Parameters = <>
    Left = 296
    Top = 8
  end
  object PopupMenu1: TPopupMenu
    Left = 88
    Top = 8
    object N1: TMenuItem
      Action = act_View_Doc
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    NotDocking = [dsNone]
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 160
    Top = 408
    DockControlHeights = (
      0
      0
      28
      0)
    object dxBarManager1Bar1: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 733
      FloatTop = 876
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarButton3: TdxBarButton
      Action = act_View_Doc
      Category = 0
    end
  end
end
