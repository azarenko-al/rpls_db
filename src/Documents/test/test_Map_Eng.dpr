program test_Map_Eng;

uses
  ShareMem,
  Forms,
  a_Unit in 'a_Unit.pas' {Form2},
  dm_Act_Map_Engine in '..\dm_Act_Map_Engine.pas' {dmAct_Map_Engine: TDataModule},
  dm_Custom in '..\..\.common\dm_Custom.pas' {dmCustom: TDataModule},
  dm_MapEngine in '..\dm_MapEngine.pas',
  dm_MapEngine_Filter in '..\dm_MapEngine_Filter.pas' {dmMapEngine_Filter: TDataModule},
  dm_MapEngine_MDB in '..\dm_MapEngine_MDB.pas' {dmMapEngine_MDB: TDataModule},
  dm_MapEngine_MIF in '..\dm_MapEngine_MIF.pas' {dmMapEngine_MIF: TDataModule},
  dm_MapEngine_SQLDB in '..\dm_MapEngine_SQLDB.pas' {dmMapEngine_SQLDB: TDataModule},
  dm_MapEngine_Sync in '..\dm_MapEngine_Sync.pas' {dmMapEngine_Sync: TDataModule},
  u_MapEngine_func in '..\u_MapEngine_func.pas';

{$R *.RES}
 

begin
  Application.Initialize;
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.
