unit f_test_Sync;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,

  f_Log,

  //u_func_app,
  u_const,
  u_const_msg,
  u_db,

  u_types,

  dm_MapEngine,
//  f_test_DataModule,
  dm_MapEngine_Sync,
  dm_Main,


  StdCtrls,
  rxPlacemnt, Db, ADODB, Grids, DBGrids, ComCtrls;


type
  Tfrm_Test = class(TForm)
    Sync: TButton;
    Check_New: TButton;
    Button2: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGrid3: TDBGrid;
    Sync_DB__: TTabSheet;
    TabSheet4: TTabSheet;
    ADOConnection2: TADOConnection;
    tbl_LinkEnd_ant: TADOTable;
    DataSource1: TDataSource;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    PageControl3: TPageControl;
    TabSheet3: TTabSheet;
    DBGrid4: TDBGrid;
    DBGrid5: TDBGrid;
    procedure SyncClick(Sender: TObject);
    procedure Check_NewClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_Test: Tfrm_Test;

implementation


{$R *.DFM}

procedure Tfrm_Test.SyncClick(Sender: TObject);
begin
///////  PostEvent (WE_PROJECT_CHANGED);

//  dmMapEngine.Sync;
end;


procedure Tfrm_Test.Check_NewClick(Sender: TObject);
begin

//  dmMapEngine.CheckNew;
end;


procedure Tfrm_Test.Button2Click(Sender: TObject);
begin
{
  dmMap_Sync.Params.MifFileName:='t:\_a\maps\linkend.TAB';
  dmMap_Sync.Params.ObjectType:=otLinkEnd;
  dmMap_Sync.Params.ProjectID:=dmMain.ProjectID;

  dmMap_Sync.Execute;
 }
end;

procedure Tfrm_Test.FormCreate(Sender: TObject);
begin
  Tfrm_Log.CreateForm;

  SyncClick(nil);
end;

end.
