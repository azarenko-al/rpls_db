object frm_Test: Tfrm_Test
  Left = 338
  Top = 161
  Width = 581
  Height = 528
  Caption = 'frm_Test'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Sync: TButton
    Left = 12
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Sync'
    TabOrder = 0
    OnClick = SyncClick
  end
  object Check_New: TButton
    Left = 12
    Top = 60
    Width = 75
    Height = 25
    Caption = 'Check_New'
    TabOrder = 1
    OnClick = Check_NewClick
  end
  object Button2: TButton
    Left = 12
    Top = 120
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 2
    OnClick = Button2Click
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 177
    Width = 573
    Height = 324
    ActivePage = TabSheet1
    Align = alBottom
    TabOrder = 3
    object TabSheet1: TTabSheet
      Caption = 'tbl_LinkEnd_ant'
      object DBGrid3: TDBGrid
        Left = 0
        Top = 0
        Width = 565
        Height = 236
        Align = alTop
        DataSource = DataSource1
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object Sync_DB__: TTabSheet
      Caption = 'Sync'
      ImageIndex = 1
      object PageControl2: TPageControl
        Left = 0
        Top = 133
        Width = 565
        Height = 145
        ActivePage = TabSheet2
        Align = alTop
        TabOrder = 0
        object TabSheet2: TTabSheet
          Caption = 'Sync_DB'
          object DBGrid4: TDBGrid
            Left = 0
            Top = 0
            Width = 557
            Height = 117
            Align = alClient
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
      end
      object PageControl3: TPageControl
        Left = 0
        Top = 0
        Width = 565
        Height = 133
        ActivePage = TabSheet3
        Align = alTop
        TabOrder = 1
        object TabSheet3: TTabSheet
          Caption = 'Sync_MIF'
          object DBGrid5: TDBGrid
            Left = 0
            Top = 0
            Width = 557
            Height = 105
            Align = alClient
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'TabSheet4'
      ImageIndex = 3
    end
  end
  object ADOConnection2: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=SQL_ONEGA;Extended Properties="DSN=SQL_ONEGA;UID=sa;APP=En' +
      'terprise;WSID=ALEX;DATABASE=ONEGA;Trusted_Connection=Yes"'
    LoginPrompt = False
    Left = 356
    Top = 4
  end
  object tbl_LinkEnd_ant: TADOTable
    Active = True
    Connection = ADOConnection2
    CursorType = ctStatic
    TableName = 'view_LinkEnd_antennas'
    Left = 284
    Top = 4
  end
  object DataSource1: TDataSource
    DataSet = tbl_LinkEnd_ant
    Left = 280
    Top = 48
  end
end
