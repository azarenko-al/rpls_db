unit dm_Document;

interface

uses
  SysUtils, Classes, Forms, Variants,

//  dm_Custom,

  dm_Main,

  u_const_db,

  u_func,
  u_dlg,
  u_db,
  u_files,
  u_zlib
  ;

type
  //---------------------------------------
  TdmDocumentAddRec = record
  //---------------------------------------
    Project_ID : Integer;

    Name: string;

  //  FileList: TStringList;            // �����
    FileName: string;            // ����� ����

    Link_ID: integer;
    LinkLine_ID: integer;

  //  Category: string;
  end;
 //   DirName: string;      // ���������
 //   DirList: TStringList; // ��������

   // project_id: Integer;

  //  object_type: string;



  //--------------------------------------------------------------------
//  TdmDocument = class(TdmCustom)
  TdmDocument = class(TDataModule)
  //--------------------------------------------------------------------
    procedure DataModuleCreate(Sender: TObject);
  private
    FTableName: string;

    //; aFileList: TStringList
    procedure SaveToDB(aID: integer; aFileName: string);
  public
    function Add(aRec: TdmDocumentAddRec): Integer;

    function Add_link(aProject_ID, aLink_ID: Integer; aName, aFileName: string):
        Integer;
    function Add_linkline(aProject_ID, aLinkLine_ID: Integer; aName, aFileName:
        string): Integer;

    function LoadFromDB(aID: integer; aPath: string; var aMainFileName: string): boolean;

    procedure ViewDoc(aID: integer);
  end;


function dmDocument: TdmDocument;

//================================================================
implementation  {$R *.dfm}
//================================================================

var
  FdmDocument: TdmDocument;


// ---------------------------------------------------------------
function dmDocument: TdmDocument;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmDocument) then
    Application.CreateForm(TdmDocument, FdmDocument);

  Result := FdmDocument;
end;



// ---------------------------------------------------------------
procedure TdmDocument.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;
  FTableName:=TBL_DOCUMENTS;
end;

// ---------------------------------------------------------------
function TdmDocument.Add_link(aProject_ID, aLink_ID: Integer; aName, aFileName:
    string): Integer;
// ---------------------------------------------------------------
var
  rec: TdmDocumentAddRec;
begin
  Assert(aProject_ID>0, 'Value <=0');
  Assert(aLink_ID>0, 'Value <=0');


  FillChar(rec,SizeOf(rec),0);

  rec.Project_ID :=aProject_ID;
  rec.Link_ID   :=aLink_ID;
  rec.FileName  :=aFileName;
  rec.Name    :=aName;

  Result := Add(rec);

end;


// ---------------------------------------------------------------
function TdmDocument.Add_linkline(aProject_ID, aLinkLine_ID: Integer; aName,
    aFileName: string): Integer;
// ---------------------------------------------------------------
var
  rec: TdmDocumentAddRec;
begin
  Assert(aProject_ID>0, 'Value <=0');
  Assert(aLinkLine_ID>0, 'Value <=0');


  FillChar(rec,SizeOf(rec),0);


  rec.Project_ID   :=aProject_ID;
  rec.LinkLine_ID :=aLinkLine_ID;
  rec.FileName    :=aFileName;
  rec.Name    :=aName;

  Result := Add(rec);

end;


//----------------------------------------------------------------
function TdmDocument.Add(aRec: TdmDocumentAddRec): Integer;
//----------------------------------------------------------------
var
  i,j: Integer;
begin
  result:= gl_DB.AddRecordID (FTableName,
                  [db_Par(FLD_NAME        , aRec.Name),

                   db_Par(FLD_Link_ID,      IIF_NULL(aRec.Link_ID)),
                   db_Par(FLD_LinkLine_ID,  IIF_NULL(aRec.LinkLine_ID)),


                   db_Par(FLD_PROJECT_ID  , dmMain.ProjectID),
                   db_Par(FLD_FILENAME   , aRec.FileName)
                   ] );


  SaveToDB(Result, aRec.FileName);

end;


//----------------------------------------------------------------
procedure TdmDocument.SaveToDB(aID: integer; aFileName: string);
//----------------------------------------------------------------
var
 /// oStrList, oTempStrList: TStringList;
  sTempFile_zip: string;
  i,j: Integer;
//  sTempFile2: string;
begin
  assert( aFileName<>'');
//  assert( Assigned(aFileList));

  sTempFile_zip:= GetTempFileNameWithExt('z');

 // sTempFile2:=aFileName+'_tmp';
//
 // FileCopy(aFileName, sTempFile2);




 // sTempFile_zip:='d:\aaa.z';

//  zlib_CompressFileList(aFileList, sTempFile_zip);
  zlib_CompressFile(aFileName, sTempFile_zip);

  gl_Db.Blob_LoadFromFile(FTableName, aID, FLD_CONTENT, sTempFile_zip);//, True);


  DeleteFile(sTempFile_zip);

 // DeleteFile(sTempFile2);


end;

//----------------------------------------------------------------
function TdmDocument.LoadFromDB(aID: integer; aPath: string; var aMainFileName: string): boolean;
//----------------------------------------------------------------
var
  sTempFile: string;
begin
  aMainFileName:= '';
  Result:= false;

  sTempFile:= GetTempFileNameWithExt('z');


  //sTempFile:='d:\bbb.z';

  //aPath:='d:\docs\';


  if gl_Db.Blob_SaveToFile(FTableName, aID, FLD_CONTENT, sTempFile) then //, True) then
//  if gl_Db.SaveMemoToFile1(FTableName, aID, FLD_CONTENT, sTempFile) then
  begin
    ForceDirectories(aPath);
    zlib_DecompressFileList(sTempFile, aPath);

    DeleteFile(sTempFile);

    aMainFileName:= gl_Db.GetStringFieldValueByID(FTableName, FLD_FILENAME, aID);

    Result:= aMainFileName<>'';
  end;



  
end;

//--------------------------------------------------------------
procedure TdmDocument.ViewDoc(aID: integer);
//--------------------------------------------------------------
var
  sMainFileName: string;
  sTempDir: string;
begin

  if aID>0 then
  begin
    sTempDir:= GetTempFileDir();
    sTempDir:= sTempDir + Format('rpls\DOCUMENTS\%d\', [aID]);
    DirClear(sTempDir);

    if LoadFromDb(aID, sTempDir, sMainFileName) then
      ShellExec(sTempDir+ExtractFileName(sMainFileName), '')
    else
      MsgDlg('�� ������� ��������� �������� � �������');
  end;

end;



end.

