unit f_Documents;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, ToolWin, ComCtrls,
  cxPropertiesStore, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ADODB, Db, dxmdaset, StdCtrls, Menus,

  dm_Main,

//  u_Oneg

  dm_Document,

  u_func,
  u_db,
  u_cx,
  u_dlg,

  u_const_db,

  f_Custom, dxBar, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxCalendar
  ;

type
  Tfrm_Documents = class(Tfrm_Custom)
    grid: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    ds_Doc: TDataSource;
    qry_Doc: TADOQuery;
    col_Name: TcxGridDBColumn;
    col_id: TcxGridDBColumn;
    col_Date: TcxGridDBColumn;
    col_User: TcxGridDBColumn;
    act_View_Doc: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    col_FileName: TcxGridDBColumn;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarButton3: TdxBarButton;
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure gridDblClick(Sender: TObject);
    procedure _Action_exec(Sender: TObject);
    procedure qry_DocCalcFields(DataSet: TDataSet);
  private
    procedure OpenDb();
  public
    class function CreateSingleForm: boolean;
    class function ExecDlg: Boolean;

    procedure View_Link(aLink_ID: Integer);
    procedure View_LinkLine(aLinkLine_ID: Integer);

  end;


//================================================================
implementation

uses dm_Onega_db_data;  {$R *.dfm}


//----------------------------------------------------------------
class function Tfrm_Documents.CreateSingleForm: boolean;
//----------------------------------------------------------------
begin
  with Tfrm_Documents.Create(Application) do
  begin
    OpenDb();

    Show;
  end;
end;



//----------------------------------------------------------------
class function Tfrm_Documents.ExecDlg: boolean;
//----------------------------------------------------------------
begin
  with Tfrm_Documents.Create(Application) do
  begin
    OpenDb();

    ShowModal;
    Free;
  end;
end;


//----------------------------------------------------------------
procedure Tfrm_Documents.FormCreate(Sender: TObject);
//----------------------------------------------------------------
begin
  inherited;
//  col_obj_name.DataBinding.FieldName:= FLD_OBJECT_NAME;

 // col_category.Caption:='���������';

  FRegPath:=FRegPath + '1';

  Caption:= '���������';

  grid.ApplyBestFit(nil);

  cxGrid1.Align:= alClient;

  grid.RestoreFromRegistry(FRegPath + '1'+ grid.Name);

 
end;

procedure Tfrm_Documents.FormDestroy(Sender: TObject);
begin
  grid.StoreToRegistry(FRegPath + grid.Name);

  inherited;
end;

//----------------------------------------------------------------
procedure Tfrm_Documents.OpenDb();
//----------------------------------------------------------------
begin
  dmOnega_DB_data.OpenQuery(qry_Doc, 'SELECT * FROM '+ VIEW_DOCUMENTS);
//  db_OpenQuery(qry_Doc, 'SELECT id,name,category,date_created,user_created,project_id,object_name,object_type FROM '+ TBL_DOCUMENTS);
end;


procedure Tfrm_Documents.qry_DocCalcFields(DataSet: TDataSet);
begin
end;

//----------------------------------------------------------------
procedure Tfrm_Documents._Action_exec(Sender: TObject);
//----------------------------------------------------------------
var
  iID: Integer;
  //sTempDir, sMainFileName: string;
begin
  if sender = act_View_Doc then
  begin
    iID:= AsInteger(grid.Controller.FocusedRow.Values[col_id.index]);

    dmDocument.ViewDoc (iID);

 {  if iID>0 then
    begin
      sTempDir:= GetTempFileDir();
      sTempDir:= sTempDir + Format('rpls\DOCUMENTS\%d\', [iID]);
      ClearDir(sTempDir);

      if dmDocument.LoadFromDb(iID, sTempDir, sMainFileName) then
        ShellExec(sTempDir+ExtractFileName(sMainFileName), '')
      else
        MsgDlg('�� ������� ��������� �������� � �������');
    end;}

  end else   ;

end;


procedure Tfrm_Documents.View_Link(aLink_ID: Integer);
begin
  db_OpenQuery(qry_Doc,
      'SELECT * FROM '+ VIEW_DOCUMENTS + ' WHERE (link_id=:link_id)',
      [db_Par(FLD_link_id, aLink_ID)]);

end;


procedure Tfrm_Documents.View_LinkLine(aLinkLine_ID: Integer);
begin
  db_OpenQuery(qry_Doc,
      'SELECT * FROM '+ VIEW_DOCUMENTS + ' WHERE (linkLine_id=:linkLine_id)',
      [db_Par(FLD_linkLine_id, aLinkLine_ID)]);

end;


procedure Tfrm_Documents.ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  act_View_Doc.Enabled:=Assigned(grid.Controller.FocusedRow);

end;


procedure Tfrm_Documents.gridDblClick(Sender: TObject);
begin
  if not cx_IsCursorInGrid(Sender) then
    Exit;

  if act_View_Doc.Enabled then
    act_View_Doc.Execute;
end;



end.
