unit u_Property_run;

interface

uses
  Classes, Forms, Menus, ActnList, IniFiles,  SysUtils,

  u_files,
  u_const,
  u_func,

  dm_Main,

  u_classes,

  u_reg,

  u_Vars,

  u_Ini_Google_Params //shared

;


type
  TProperty_run = class(TObject)
  public
    class procedure Export_Google(aSelectedIDList: TIDList);

  end;

implementation

// ---------------------------------------------------------------
class procedure TProperty_run.Export_Google(aSelectedIDList: TIDList);
// ---------------------------------------------------------------
const
  TEMP_INI_FILE = 'project_google.ini';
var
  iCode: Integer;
  oParams: TIni_Google_Params;
  sFile: string;
begin
  sFile := g_ApplicationDataDir + TEMP_INI_FILE;

  oParams:=TIni_Google_Params.Create;

  oParams.Mode      := mtProperty;
  oParams.ProjectID := dmMain.ProjectID;
  oParams.Property_IDList.Assign(aSelectedIDList);

  oParams.ConnectionString:=dmMain.GetConnectionString();

  oParams.SaveToFile(sFile);

  FreeAndNil(oParams);

  RunApp (GetApplicationDir() + PROG_Google,  DoubleQuotedStr(sFile), iCode);

end;



end.
