unit dm_Property;

interface
{$I ver.inc}

uses
  Classes, DB, ADODB, Forms, Variants, 

  dm_Onega_DB_data,

  dm_Main,

  dm_Object_base,

  u_classes,
  
  u_db,
  u_func,
  u_Geo,
  u_geo_convert_new,
//  u_geo_convert,

  u_types,

  u_const_db
  ;

type
  TdmPropertyAddRec1 = record
  ///  Project_ID : Integer;
    Project_ID : Integer;

    //------------------------------------------

    Name:       string;
    Folder_ID:   integer;
    BLPoint_Pulkovo:  TBLPoint;

    Ground_Height: integer;
    CLUTTER_HEIGHT: integer;
    CLUTTER_NAME: string;
    
   // Beenet: string;
    GeoRegionID: Integer;

//    Template_site_id  : Integer;

    //new
//////    GeoRegion_code: Integer; //��� ������� //77,98

(*    Placement_Name : string;
    Status_Name : string;
*)

    Address: string;
    City: string;

 //   Bee_Beenet: string;
//    GeoRegionID: Integer;

    Comments : string;

//    Address: string;

    Placement_id : Integer;
    Status_id    : Integer;

  end;


  TdmProperty = class(TdmObject_base)
    qry_Antennas: TADOQuery;
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
//    function IsEmpty(aID: integer): boolean;
  public
    function Add(aRec: TdmPropertyAddRec1; aIDList: TIDList = nil): Integer;

    function Del(aID: integer): boolean;
    function GetPos (aID: integer): TBLPoint;

    function GetNearestIDandPos(aBLPoint: TBLPoint; var aPropertyPos: TBLPoint; var aName:
        string): integer;
        
    function GetNearestID  (aBLPoint: TBLPoint): integer;
    procedure UpdateAddress(aID: integer; aAddress: string);

  end;


  function dmProperty: TdmProperty;


//================================================================
implementation {$R *.dfm}
//================================================================

uses
  dm_Property_tools;

var
  FdmProperty: TdmProperty;

// ---------------------------------------------------------------
function dmProperty: TdmProperty;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmProperty) then
    FdmProperty:=TdmProperty.Create(Application);

  Result := FdmProperty;
end;

// ---------------------------------------------------------------
procedure TdmProperty.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;

  TableName  :=TBL_PROPERTY;
  ObjectName :=OBJ_PROPERTY;
end;



//--------------------------------------------------------------------
function TdmProperty.Add(aRec: TdmPropertyAddRec1; aIDList: TIDList = nil): integer;
//--------------------------------------------------------------------
var
  blPointWGS: TBLPoint;

 // oPropertyAdd: TPropertyAdd;

begin
  Assert(aRec.Name<>'');

  Assert( aRec.Project_ID>0, ' aRec.ProjectID <=0');

//  Assert( dmMain.ProjectID>0, ' dmMain.ProjectID <=0');
  // Added by Aleksey 01.09.2009 12:08:33
  //geo_ReduceBLPoint (aRec.BLPoint);


(*  oPropertyAdd:=TPropertyAdd.Create;
  oPropertyAdd.PROJECT_ID := dmMain.ProjectID;

*)

//////  aRec.ProjectID:=dmMain.ProjectID;

  blPointWGS:=geo_Pulkovo42_to_WGS84(aRec.BLPoint_Pulkovo);//, EK_CK_42, EK_WGS84);


  Result:=dmOnega_DB_data.ExecStoredProc_(SP_PROPERTY_ADD,

//  Result:=db_ExecStoredProc(dmMain.ADOConnection, SP_PROPERTY_ADD,

                  [
//                    db_Par(FLD_PROJECT_ID,      dmMain.ProjectID),
                    FLD_PROJECT_ID,      aRec.Project_ID,

                    FLD_NAME,            aRec.Name,
                    FLD_FOLDER_ID,       IIF_NULL(aRec.Folder_ID),

                    FLD_GROUND_HEIGHT,   aRec.Ground_Height,
                    FLD_CLUTTER_HEIGHT,  aRec.CLUTTER_HEIGHT,
                    FLD_clutter_name,    aRec.clutter_name,

                    FLD_LAT,             aRec.BLPoint_Pulkovo.B,
                    FLD_LON,             aRec.BLPoint_Pulkovo.L,

                    FLD_LAT_WGS,         blPointWGS.B,
                    FLD_LON_WGS,         blPointWGS.L,

                    FLD_ADDRESS,         aRec.Address,

              //      db_Par(FLD_BEE_BEENET,      aRec.BEE_BEENET,

                    FLD_Comments,        aRec.Comments,

                  ///////  db_Par(FLD_Placement_id,    aRec.Placement_id,
                    FLD_Status_id,       aRec.Status_id,
                    FLD_GEOREGION_ID,    IIF_NULL(aRec.GeoRegionID)

//                    FLD_Template_site_id,  IIF_NULL(aRec.Template_site_id)
                   ] );


  if Assigned(aIDList) then
    aIDList.AddObjName(Result, OBJ_Property);


  dmProperty_tools.Update_LatLonStr (Result, aRec.BLPoint_Pulkovo);


//                    db_Par(FLD_BEE_BEENET,  IIF(beenet<>'',beenet,NULL)),

{
                  db_Par(FLD_ADDRESS1, Area),
                  db_Par(FLD_ADDRESS2, Distinct),
                  db_Par(FLD_TOWN,     Town),
                  db_Par(FLD_ADDRESS,  Address)}

{
                    db_Par(FLD_CLUTTER_HEIGHT,CLUTTER_HEIGHT),
                    db_Par(FLD_CLUTTER_NAME,  CLUTTER_NAME),
}

end;


//-------------------------------------------------------------------
function TdmProperty.GetNearestID (aBLPoint: TBLPoint): integer;
//-------------------------------------------------------------------
var
  aPos: TBLPoint;
  sName: string;
begin
  Result:=GetNearestIDandPos (aBLPoint,aPos, sName);
end;


//-------------------------------------------------------------------
function TdmProperty.Del(aID: integer): boolean;
//-------------------------------------------------------------------
var
  k: Integer;
begin
  k:=dmOnega_DB_data.Property_Del(aID);
  Result:=True;

// 	Result:= gl_db.DeleteRecordByID (TableName, aID);
end;


//-------------------------------------------------------------------
procedure TdmProperty.UpdateAddress(aID: integer; aAddress: string);
//-------------------------------------------------------------------
begin
  Update(aID, FLD_ADDRESS, aAddress);
end;



//-------------------------------------------------------------------
function TdmProperty.GetNearestIDandPos(aBLPoint: TBLPoint; var aPropertyPos: TBLPoint;
    var aName: string): integer;
//-------------------------------------------------------------------
const
  SQL_SELECT_NEAREST_PROPERTY =
    'SELECT top 1  id, name, lat, lon, '+
    '        SQRT((lat-:lat)*(lat-:lat)+(lon-:lon)*(lon-:lon)) AS dist '+
    ' FROM '+ TBL_PROPERTY +
    ' WHERE project_id=:project_id '+
    ' ORDER BY dist';

var
  iID: Integer;
  s: string;
  blPoint2:  TBLPoint;
  dist: double;

begin
 // g_Log.Add( Format('TdmProperty.GetNearestIDandPos - lat: %1.8f, lon: %1.8f', [aBLPoint.B, aBLPoint.L]));


  Assert(dmMain.ProjectID>0, 'Value <=0');


  if aBLPoint.B=0 then
  begin
    Result:=0;  Exit;
  end;

  dmOnega_DB_data.OpenQuery (qry_Temp, SQL_SELECT_NEAREST_PROPERTY,
     [FLD_LAT, aBLPoint.B,
      FLD_LON, aBLPoint.L,
      FLD_PROJECT_ID, dmMain.ProjectID
     ]);


  with qry_Temp do
    if not IsEmpty then
    begin
      Result:=FieldValues[FLD_ID];
      aName:=FieldByName(FLD_NAME).AsString;

      aPropertyPos:=db_ExtractBLPoint (qry_Temp);
    end else
      Result:=0;
end;


//--------------------------------------------------------------------
function TdmProperty.GetPos (aID: integer): TBLPoint;
//--------------------------------------------------------!-----------
begin
   gl_DB.GetPosByID (TBL_PROPERTY, aID, Result.B, Result.L );
end;

begin

end.
