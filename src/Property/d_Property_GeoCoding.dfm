object dlg_Property_GeoCoding: Tdlg_Property_GeoCoding
  Left = 1410
  Top = 516
  Width = 858
  Height = 503
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'dlg_Property_GeoCoding'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 529
    Height = 445
    Align = alLeft
    DataSource = ds_Data
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'id'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'name'
        Title.Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        Width = 220
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'address'
        Title.Caption = #1040#1076#1088#1077#1089
        Width = 379
        Visible = True
      end>
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 445
    Width = 842
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object Panel1: TPanel
    Left = 744
    Top = 0
    Width = 98
    Height = 445
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 2
    object Button1: TButton
      Left = 8
      Top = 40
      Width = 75
      Height = 25
      Action = act_Save
      TabOrder = 0
    end
    object Button2: TButton
      Left = 8
      Top = 104
      Width = 75
      Height = 25
      Cancel = True
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ModalResult = 2
      TabOrder = 1
    end
    object Button3: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Action = act_Run
      TabOrder = 2
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 608
    Top = 336
  end
  object ds_Data: TDataSource
    DataSet = qry_Props
    Left = 606
    Top = 92
  end
  object qry_Props: TADOQuery
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 608
    Top = 152
  end
  object ActionList2: TActionList
    Left = 608
    Top = 252
    object act_Save: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      OnExecute = act_SaveExecute
    end
    object act_Run: TAction
      Caption = #1055#1086#1080#1089#1082
      OnExecute = act_SaveExecute
    end
  end
end
