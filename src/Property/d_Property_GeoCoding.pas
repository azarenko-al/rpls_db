unit d_Property_GeoCoding;

interface

uses

  Forms, SysUtils,

  dm_Main,
  dm_Onega_DB_data,

  u_geo_convert_new,

  i_GeoCoding,

  u_const_db,

  u_classes,

  dm_Property,

  u_geo,

  u_func,
  u_func_arr,


  u_db,

  Controls, Grids, DBGrids, StdCtrls, Buttons, ToolWin, DB, dxmdaset,
  Classes, ActnList, rxPlacemnt, ComCtrls, ADODB, ExtCtrls;

type


  Tdlg_Property_GeoCoding = class(TForm)
    DBGrid1: TDBGrid;
    FormStorage1: TFormStorage;
    ds_Data: TDataSource;
    StatusBar1: TStatusBar;
    qry_Props: TADOQuery;
    Panel1: TPanel;
    Button1: TButton;
    ActionList2: TActionList;
    act_Save: TAction;
    act_Run: TAction;
    Button2: TButton;
    Button3: TButton;
    procedure act_SaveExecute(Sender: TObject);

    procedure FormCreate(Sender: TObject);
  private


    procedure OpenDB(aWhere: string);
    procedure Run;
    procedure Save;
    procedure UpdateStatusBar;

  public
    class procedure ExecDlg(aSelectedIDList: TIDList);

  end;


implementation

{$R *.dfm}

//------------------------------------------------------------------------------
class procedure Tdlg_Property_GeoCoding.ExecDlg(aSelectedIDList: TIDList);
//------------------------------------------------------------------------------
var
  s: string;
  sSQL: string;
begin
  with Tdlg_Property_GeoCoding.Create(Application) do
  begin
    s:=aSelectedIDList.MakeWhereString();

    OpenDB (s);


 //   FProc:=aProc;

  //  if aEvents<>nil then
   //   FEvents:=aEvents;


    UpdateStatusBar;

    ShowModal;

    Free;
  end;

end;

// ---------------------------------------------------------------
procedure Tdlg_Property_GeoCoding.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin

  Caption:= 'Геокодирование';
  DBGrid1.Align:= alClient;


end;


procedure Tdlg_Property_GeoCoding.act_SaveExecute(Sender: TObject);
begin
  if Sender=act_Save then
    Save else

  if Sender=act_Run then
    Run

//
end;




// ---------------------------------------------------------------
procedure Tdlg_Property_GeoCoding.UpdateStatusBar;
// ---------------------------------------------------------------
begin
  StatusBar1.SimpleText:=  Format('%d записей',[qry_Props.RecordCount]);
end;


// ---------------------------------------------------------------
procedure Tdlg_Property_GeoCoding.OpenDB(aWhere: string);
// ---------------------------------------------------------------
const
  SQL_SELECT_PROPERTY =
    'SELECT id, address, name, lat, lon'+
//    'SELECT id, address, name, lat, lon, lat_wgs, lon_wgs'+
    ' FROM '+ TBL_PROPERTY +
    ' WHERE (project_id=:project_id)  and  ';
//    ' ORDER BY name';
begin
  dmOnega_DB_data.OpenQuery(qry_Props,
     SQL_SELECT_PROPERTY + ' (ID in (' + aWhere + '))',
     [
      FLD_PROJECT_ID, dmMain.ProjectID
     ]);



  UpdateStatusBar;

//  StatusBar1.SimpleText:=  Format('%d записей',[mem_Data.RecordCount]);
end;


//------------------------------------------------------------------------------
procedure Tdlg_Property_GeoCoding.Run;
//------------------------------------------------------------------------------
var
//  I: Integer;
//  i: integer;

  bl: TBLPoint;
  BLPoint_WGS: TBLPoint;

  obj: TComClient_GeoCoding;
  //iID: Integer;
  sAddress: Widestring;


begin


  obj:=TComClient_GeoCoding.Create;


  qry_Props.DisableControls;
  qry_Props.First;

  with qry_Props do
    while not EOF  do
    begin
    //  iID:=FieldByName(FLD_ID).AsInteger;
      bl.B:=FieldByName(FLD_LAT).AsFloat;
      bl.L:=FieldByName(FLD_LON).AsFloat;

      BLPoint_WGS:=geo_Pulkovo42_to_WGS84(bl);
      obj.Intf.Get_Address_By_Lat_Lon_WGS(BLPoint_WGS.B, BLPoint_WGS.L, sAddress);

//      obj.Intf.Get_Address_By_Lat_Lon_WGS(bl.B, bl.L, sAddress);

      db_UpdateRecord__(qry_Props,
        [
           FLD_Address,sAddress
        ]);


   //   FGeoObjList.Add_(iID, '', bl.B, bl.L);

      //sAddress:= webgeo_GetAddressByLatLon (bl.B, bl.L);

      Next;
    end;

    {
  FGeoObjList.Refresh_Address_by_LatLon__(False);

  for I := 0 to FGeoObjList.Count - 1 do    // Iterate
    if FGeoObjList[i].Address<>'' then
    begin
      if qry_Props.Locate(FLD_ID, FGeoObjList[i].ID, []) then
         db_UpdateRecord__(qry_Props,
        [
           FLD_Address, FGeoObjList[i].Address
        ]);
    end;
  }

  qry_Props.First;
  qry_Props.EnableControls;


  FreeAndNil(obj);

end;


//------------------------------------------------------------------------------
procedure Tdlg_Property_GeoCoding.Save;
//------------------------------------------------------------------------------
var
  iID: Integer;
  sAddress: string;
begin

 // db_Clear(mem_Data);


  qry_Props.DisableControls;
  qry_Props.First;

  with qry_Props do
    while not EOF  do
    begin
      iID:=FieldByName(FLD_ID).AsInteger;
      sAddress:=FieldByName(FLD_ADDRESS).AsString;


      dmProperty.UpdateAddress(iID, sAddress);

      //sAddress:= webgeo_GetAddressByLatLon (bl.B, bl.L);

      Next;
    end;


  qry_Props.First;
  qry_Props.EnableControls;

  Close;
end;






end.

