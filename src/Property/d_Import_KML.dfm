object dlg_Import_kml: Tdlg_Import_kml
  Left = 622
  Top = 355
  Width = 1450
  Height = 643
  Caption = 'dlg_Import_kml'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 1314
    Top = 97
    Width = 128
    Height = 499
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      128
      499)
    object btn_Close: TButton
      Left = 10
      Top = 124
      Width = 110
      Height = 23
      Cancel = True
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ModalResult = 2
      TabOrder = 0
    end
    object btn_Save: TButton
      Left = 9
      Top = 45
      Width = 110
      Height = 25
      Action = act_Save
      Anchors = [akTop, akRight]
      TabOrder = 1
    end
    object btn_Clear: TButton
      Left = 9
      Top = 85
      Width = 110
      Height = 25
      Action = act_Clear
      Anchors = [akTop, akRight]
      TabOrder = 2
    end
    object btnLoadKML: TButton
      Left = 9
      Top = 14
      Width = 110
      Height = 25
      Action = act_Open
      Anchors = [akTop, akRight]
      Default = True
      TabOrder = 3
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 1442
    Height = 97
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    Visible = False
    object FilenameEdit1: TFilenameEdit
      Left = 8
      Top = 18
      Width = 425
      Height = 21
      NumGlyphs = 1
      TabOrder = 0
      Text = 'W:\RPLS_DB_XE_PARTS\KML\demos\_'#1041#1072#1096#1085#1080'57.kml'
    end
    object Button1: TButton
      Left = 520
      Top = 16
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 1
      OnClick = Button1Click
    end
    object Edit1: TEdit
      Left = 8
      Top = 45
      Width = 361
      Height = 21
      TabOrder = 2
    end
  end
  object DBGrid1: TDBGrid
    Left = 465
    Top = 97
    Width = 737
    Height = 499
    Align = alLeft
    DataSource = DataSource1
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnCellClick = DBGrid1CellClick
    OnDrawColumnCell = DBGrid1DrawColumnCell
    Columns = <
      item
        Expanded = False
        FieldName = 'checked'
        Width = 51
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'name'
        Width = 161
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Description'
        Width = 192
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'lat'
        Width = 122
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'lon'
        Width = 117
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'id'
        Visible = False
      end>
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 596
    Width = 1442
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 97
    Width = 465
    Height = 499
    Align = alLeft
    TabOrder = 4
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1RecId: TcxGridDBColumn
        DataBinding.FieldName = 'RecId'
        Visible = False
      end
      object cxGrid1DBTableView1checked: TcxGridDBColumn
        DataBinding.FieldName = 'checked'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.ImmediatePost = True
        Properties.NullStyle = nssUnchecked
      end
      object cxGrid1DBTableView1name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Width = 102
      end
      object cxGrid1DBTableView1Description: TcxGridDBColumn
        DataBinding.FieldName = 'Description'
        Width = 121
      end
      object cxGrid1DBTableView1lat: TcxGridDBColumn
        DataBinding.FieldName = 'lat'
        Width = 95
      end
      object cxGrid1DBTableView1lon: TcxGridDBColumn
        DataBinding.FieldName = 'lon'
        Width = 89
      end
      object cxGrid1DBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    Options = [fpPosition]
    StoredProps.Strings = (
      'Edit1.Text')
    StoredValues = <>
    Left = 636
    Top = 196
  end
  object ActionList1: TActionList
    Left = 640
    Top = 252
    object act_Save: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      OnExecute = act_ClearExecute
    end
    object act_Clear: TAction
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100
      OnExecute = act_ClearExecute
    end
    object act_Open: TAction
      Caption = #1054#1090#1082#1088#1099#1090#1100' KML'
      OnExecute = act_ClearExecute
    end
  end
  object dxMemData1: TdxMemData
    Active = True
    Indexes = <>
    Persistent.Data = {
      5665728FC2F5285C8FFE3F060000000200000005000800636865636B656400C8
      000000010005006E616D650008000000060004006C6174000800000006000400
      6C6F6E006400000001000C004465736372697074696F6E000400000003000300
      6964000000000104000000747772740100000000000010400100000000000010
      4001020000006764000000000000000001070000007765727477657201000000
      0000001040010000000000001440010500000067736466670000000000}
    SortOptions = []
    Left = 640
    Top = 344
    object dxMemData1checked: TBooleanField
      DefaultExpression = 'True'
      DisplayLabel = #1042#1082#1083
      FieldName = 'checked'
    end
    object dxMemData1name: TStringField
      DisplayLabel = #1053#1072#1079#1074#1072#1085#1080#1077
      FieldName = 'name'
      Size = 200
    end
    object dxMemData1lat: TFloatField
      DisplayLabel = #1064#1080#1088#1086#1090#1072
      FieldName = 'lat'
    end
    object dxMemData1lon: TFloatField
      DisplayLabel = #1044#1086#1083#1075#1086#1090#1072
      FieldName = 'lon'
    end
    object dxMemData1Description: TStringField
      DisplayLabel = #1054#1087#1080#1089#1072#1085#1080#1077
      FieldName = 'Description'
      Size = 100
    end
    object dxMemData1id: TIntegerField
      FieldName = 'id'
    end
  end
  object DataSource1: TDataSource
    DataSet = dxMemData1
    Left = 640
    Top = 400
  end
  object ADOStoredProc1: TADOStoredProc
    Parameters = <>
    Left = 640
    Top = 512
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '.kml'
    FileName = 'W:\RPLS_DB_XE_PARTS\KML\demos\_'#1041#1072#1096#1085#1080'57.kml'
    Filter = '*.kml|*.kml'
    Left = 632
    Top = 112
  end
  object cxLookAndFeelController1: TcxLookAndFeelController
    Kind = lfFlat
    NativeStyle = True
    SkinName = 'Office2013LightGray'
    Left = 1240
    Top = 208
  end
end
