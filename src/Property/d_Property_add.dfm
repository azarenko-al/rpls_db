inherited dlg_Property_add: Tdlg_Property_add
  Left = 1243
  Top = 347
  Width = 619
  Height = 568
  Caption = 'dlg_Property_add'
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 494
    Width = 603
    TabOrder = 2
    inherited Bevel1: TBevel
      Width = 603
    end
    inherited Panel3: TPanel
      Left = 432
      Width = 171
      inherited btn_Ok: TButton
        Left = 0
      end
      inherited btn_Cancel: TButton
        Left = 84
      end
    end
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Caption = 'check'
      TabOrder = 1
      Visible = False
    end
    object Button2: TButton
      Left = 96
      Top = 8
      Width = 75
      Height = 25
      Caption = 'list'
      TabOrder = 2
      Visible = False
    end
  end
  inherited pn_Top_: TPanel
    Width = 603
    TabOrder = 3
    inherited Bevel2: TBevel
      Width = 603
    end
    inherited pn_Header: TPanel
      Width = 603
    end
  end
  inherited Panel1: TPanel
    Width = 603
    TabOrder = 0
    inherited ed_Name_: TEdit
      Left = 4
      Top = 21
      Width = 602
    end
  end
  inherited Panel2: TPanel
    Top = 197
    Width = 603
    Height = 253
    TabOrder = 4
    inherited PageControl1: TPageControl
      Width = 593
      Height = 204
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 585
          Height = 173
          BorderStyle = cxcbsNone
          Align = alClient
          LookAndFeel.Kind = lfUltraFlat
          LookAndFeel.NativeStyle = False
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.ShowEditButtons = ecsbAlways
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 203
          OptionsView.ValueWidth = 40
          TabOrder = 0
          Version = 1
          object row_GeoRegion1: TcxEditorRow
            Expanded = False
            Properties.Caption = #1043#1077#1086#1075#1088#1072#1092#1080#1095#1077#1089#1082#1080#1081' '#1088#1077#1075#1080#1086#1085
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = row_Folder1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object row_City: TcxEditorRow
            Expanded = False
            Properties.Caption = 'row_City'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
          object row_Address: TcxEditorRow
            Expanded = False
            Properties.Caption = 'row_Address'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 2
            ParentID = -1
            Index = 2
            Version = 1
          end
          object cxVerticalGrid1CategoryRow2: TcxCategoryRow
            Expanded = False
            ID = 3
            ParentID = -1
            Index = 3
            Version = 1
          end
          object row_Relief_Height1: TcxEditorRow
            Expanded = False
            Properties.Caption = 'row_Relief_Height'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 4
            ParentID = -1
            Index = 4
            Version = 1
          end
          object row_MP_Type1: TcxEditorRow
            Expanded = False
            Properties.Caption = 'row_MP_Type'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 5
            ParentID = -1
            Index = 5
            Version = 1
          end
          object row_MP_Height1: TcxEditorRow
            Expanded = False
            Properties.Caption = 'row_MP_Height'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 6
            ParentID = -1
            Index = 6
            Version = 1
          end
          object cxVerticalGrid1EditorRow9: TcxEditorRow
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            Visible = False
            ID = 7
            ParentID = -1
            Index = 7
            Version = 1
          end
          object cxVerticalGrid1EditorRow10: TcxEditorRow
            Properties.Caption = #1057#1087#1086#1089#1086#1073' '#1092#1086#1088#1084#1080#1088#1086#1074#1072#1085#1080#1103' '#1080#1084#1105#1085
            Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.DropDownRows = 7
            Properties.EditProperties.Items.Strings = (
              #1048#1084#1103
              'K'#1086#1076' '#1086#1073#1098#1077#1082#1090#1072
              #1048#1084#1103' + K'#1086#1076' '#1086#1073#1098#1077#1082#1090#1072
              'K'#1086#1076' '#1086#1073#1098#1077#1082#1090#1072' + '#1048#1084#1103' ')
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.EditProperties.Revertable = True
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            Visible = False
            ID = 8
            ParentID = -1
            Index = 8
            Version = 1
          end
          object cxVerticalGrid1CategoryRow1: TcxCategoryRow
            ID = 9
            ParentID = -1
            Index = 9
            Version = 1
          end
          object row_Template_site: TcxEditorRow
            Properties.Caption = 'row_Template_site'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end
              item
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  1800000000000003000000000000000000000000000000000000FF00FFFF00FF
                  FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                  FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                  00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FF00FFFF00FFFF00FFFF00FF
                  FF00FF000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                  FFFF00FFFF00FFFF00FFFF00FFFF00FF000000000000000000FF00FFFF00FFFF
                  00FFFF00FFFF00FFFF00FFFF00FF000000FF00FFFF00FFFF00FFFF00FFFF00FF
                  000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FF00
                  FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000000000000000FF00FFFF
                  00FFFF00FFFF00FF000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                  FF00FFFF00FF000000000000000000FF00FFFF00FF000000000000FF00FFFF00
                  FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00000000000000
                  0000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                  FF00FFFF00FFFF00FFFF00FF000000000000000000FF00FFFF00FFFF00FFFF00
                  FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00000000000000
                  0000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                  FF00FFFF00FF000000000000000000FF00FFFF00FF000000FF00FFFF00FFFF00
                  FFFF00FFFF00FFFF00FFFF00FFFF00FF000000000000000000000000FF00FFFF
                  00FFFF00FFFF00FF000000000000FF00FFFF00FFFF00FFFF00FFFF00FF000000
                  000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000000000
                  00FF00FFFF00FFFF00FFFF00FF000000000000FF00FFFF00FFFF00FFFF00FFFF
                  00FFFF00FFFF00FFFF00FFFF00FF000000000000FF00FFFF00FFFF00FFFF00FF
                  FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                  FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                  00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
                Kind = bkGlyph
              end>
            Properties.EditProperties.OnButtonClick = row_TemplateEditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            Visible = False
            ID = 10
            ParentID = -1
            Index = 10
            Version = 1
          end
        end
      end
    end
  end
  object GroupBox1: TGroupBox [4]
    Left = 0
    Top = 107
    Width = 603
    Height = 90
    Align = alTop
    Caption = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1099
    Constraints.MaxHeight = 90
    Constraints.MinHeight = 90
    TabOrder = 1
  end
  inherited ActionList1: TActionList
    Left = 432
    inherited act_Ok: TAction
      OnUpdate = act_OkUpdate
    end
  end
  inherited FormStorage1: TFormStorage
    Left = 372
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 399
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 248
    Top = 3
  end
end
