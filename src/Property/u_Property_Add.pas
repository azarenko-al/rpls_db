unit u_Property_Add;

interface
uses
  Variants, 

  dm_Onega_DB_data,

  u_classes,
  u_db,
  u_func,
  u_Geo,
//
//      bl:=geo_WGS84_to_Pulkovo42(bl) ;
//        bl1:=geo_Pulkovo42_to_WGS84(bl) ;

  u_geo_convert_new,
  //u_geo_convert,

  u_types,

  u_const_db ;


type

  //---------------------------------------------------------
  TPropertyAddToDB = class
  //---------------------------------------------------------

  public
    PROJECT_ID     : Integer;

    NewName         : string;
    Folder_ID       : integer;

    Folder_Name     : string;

    BLPoint_Pulkovo: TBLPoint;
    BLPoint_WGS    : TBLPoint;

    CoordnateKind : (ckPulkovo,ckWGS);
//    CoordnateKind : (ckNone,ckPulkovo,ckWGS);

    Tower_height : Double;


    Ground_Height  : integer;
    CLUTTER_HEIGHT : integer;
    CLUTTER_NAME   : string;

    Comments       : string;

    Address        : string;
    City           : string;

    GeoRegion_ID   : Integer;

    Placement_id   : Integer;
    State_id       : Integer;

    //Beeline
    NAME_SDB : string;
    NAME_ERP : string;

//    Template_site_id : Integer;

  public
    function Add_DB(aIDList: TIDList = nil): integer;
  end;


implementation


//--------------------------------------------------------------------
function TPropertyAddToDB.Add_DB(aIDList: TIDList = nil): integer;
//--------------------------------------------------------------------
var
  bl_CK95: TBLPoint;
begin
  Assert(Assigned(dmOnega_DB_data));


//  bl_CK95 := geo_Pulkovo_BL_to_BL_95(BLPoint_Pulkovo);
//  bl_CK95 := geo_Pulkovo_CK_42_to_CK_95 (BLPoint_Pulkovo);
  Assert(NewName<>'');
  Assert(Project_ID>0, ' ProjectID <=0');
  // Added by Aleksey 01.09.2009 12:08:33
  //geo_ReduceBLPoint (aRec.BLPoint);


  if (BLPoint_WGS.B=0) and (BLPoint_Pulkovo.B<>0) then
//    BLPoint_WGS:=geo_BL_to_BL_new(BLPoint_Pulkovo, EK_CK_42, EK_WGS84);
    BLPoint_WGS:=geo_Pulkovo42_to_WGS84(BLPoint_Pulkovo);

  if (BLPoint_WGS.B<>0) and (BLPoint_Pulkovo.B=0) then
  //  BLPoint_Pulkovo:=geo_BL_to_BL_new(BLPoint_WGS, EK_WGS84, EK_CK_42);
    BLPoint_Pulkovo:=geo_WGS84_to_Pulkovo42(BLPoint_WGS);

//    result := Format('%1.8f N  %1.8f E', [bl.B, bl.L]);


  bl_CK95 := geo_Pulkovo42_to_CK_95 (BLPoint_Pulkovo);



  Result:=dmOnega_DB_data.ExecStoredProc_ (SP_PROPERTY_ADD,
          [
            FLD_PROJECT_ID,      Project_ID,
            FLD_NAME,            NewName,
            FLD_FOLDER_ID,       IIF_NULL(Folder_ID),

            FLD_Tower_height,    IIF_NULL(Tower_height),


            FLD_GROUND_HEIGHT,   IIF_NULL(Ground_Height),
            FLD_CLUTTER_HEIGHT,  IIF_NULL(CLUTTER_HEIGHT),
            FLD_clutter_name,    IIF_NULL(clutter_name),

            FLD_LAT,             BLPoint_Pulkovo.B,
            FLD_LON,             BLPoint_Pulkovo.L,

            FLD_LAT_WGS,         BLPoint_WGS.B,
            FLD_LON_WGS,         BLPoint_WGS.L,

            FLD_LAT_CK95,        bl_CK95.B,
            FLD_LON_CK95,        bl_CK95.L,


            FLD_ADDRESS,         IIF_NULL(Address),
            FLD_CITY,            IIF_NULL(CITY),

            FLD_Comments,        IIF_NULL(Comments),

            FLD_Placement_id,    IIF_NULL(Placement_id),
            FLD_State_id,        IIF_NULL(State_id),

            FLD_GEOREGION_ID,    IIF_NULL(GeoRegion_ID)

//            FLD_Template_site_id,   IIF_NULL(Template_site_id),


//            FLD_NAME_SDB, IIF_NULL(NAME_SDB),
//            FLD_NAME_ERP, IIF_NULL(NAME_ERP)
          ]); //

 // db_ViewDataSet(aADOQuery);

 // Assert(result>0, 'SP_PROPERTY_ADD.result <=0');



  if Assigned(aIDList) then
    aIDList.AddObjName(Result, OBJ_PROPERTY);

 
end;



end.

