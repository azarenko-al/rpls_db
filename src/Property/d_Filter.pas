unit d_Filter;

interface

uses
  dm_XML,
  I_Shell,
  u_shell_new,

  dm_Onega_DB_data,

  dm_MapEngine_store,

  u_const_db,

  u_types,

  u_func_msg,
  u_func,
  u_db,
  u_geo,

  
  SysUtils, Classes, xmldom, XMLIntf, XMLDoc, Forms,
  Windows, Messages, Variants,  
  Dialogs, StdCtrls, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, DB,
  ADODB, rxPlacemnt, dxmdaset, DBCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxCheckBox, cxContainer, cxGroupBox, cxRadioGroup,
  cxDBEdit, Controls, Grids, DBGrids, ExtCtrls;

type
  Tdlg_Filter = class(TForm)
    Button1: TButton;
    Button2: TButton;
    sp_Filter_items_PLACE_DEVICE_TYPE: TADOStoredProc;
    ds_Filter_items_PLACE_DEVICE_TYPE: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    col_Name: TcxGridDBColumn;
    col_Values: TcxGridDBColumn;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    cb_Logical_Status: TCheckBox;
    sp_Filter_items_logical_status_sel: TADOStoredProc;
    ds_sp_Filter_items_logical_status_sel: TDataSource;
    ADOConnection1: TADOConnection;
    Panel1: TPanel;
    cb_PLACE_DEVICE_TYPE: TCheckBox;
    FormStorage1: TFormStorage;
    cxGridDBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGridDBTableView1Column2: TcxGridDBColumn;
    q_Filter: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure sp_Filter_items_PLACE_DEVICE_TYPEAfterPost(DataSet: TDataSet);
    procedure cxGrid1DBTableView1Column1PropertiesEditValueChanged(
      Sender: TObject);
  private
    FFolderGUID: string;
    FDisableControls : boolean;

    procedure Check_Dataset_to_XML(aDoc: TXMLDocument; aFieldName: string;
        aDataset: TDataset; aFieldName_Values: string);

    procedure OpenData;
    procedure Save;
  public
    class function ExecDlg(aFolderGUID: string): boolean;

  end;


implementation
{$R *.dfm}

const
  DEF_Sp_Filter_UPD       = 'ui_explorer.sp_Filter_UPD';
  DEF_Sp_Filter_items_PLACE_DEVICE_TYPE_SEL = 'ui_explorer.sp_Filter_items_PLACE_DEVICE_TYPE_SEL';
  DEF_sp_Filter_items_logical_status_sel    = 'ui_explorer.sp_Filter_items_logical_status_sel';
  
//ui_explorer.sp_Filter_items_Logical_status_SEL
//    dmOnega_DB_data.OpenStoredProc (sp_Filter_items_sel, DEF_Sp_Filter_items_SEL,  [ ]);

 // DEF_Sp_Filter_SEL       = 'ui_explorer.sp_Filter_SEL';



procedure Tdlg_Filter.FormCreate(Sender: TObject);
begin
  if ADOConnection1.Connected then
    ShowMessage ('procedure Tdlg_Tdlg_Settings_WMSLinkFreqPlan_Add_from_map.FormCreate(Sender: TObject);');


  Caption:='���������� ��������...';

  cb_PLACE_DEVICE_TYPE.Caption:='��� ������������';
  cb_Logical_Status.Caption   :='������ ��������';


  Height:=480;
end;


//------------------------------------------------------------------------------
class function Tdlg_Filter.ExecDlg(aFolderGUID: string): boolean;
//------------------------------------------------------------------------------
begin

  with Tdlg_Filter.Create(Application) do
  begin
    FFolderGUID:=aFolderGUID;

    OpenData;

    Result:=ShowModal=mrOk;
    if Result then
      Save;

    Free;
  end;

end;

// ---------------------------------------------------------------
procedure Tdlg_Filter.OpenData;
// ---------------------------------------------------------------
begin
//  dmOnega_DB_data.OpenStoredProc (sp_Filter_sel,       DEF_Sp_Filter_SEL,  [ ]);
  dmOnega_DB_data.OpenStoredProc (sp_Filter_items_PLACE_DEVICE_TYPE,  DEF_Sp_Filter_items_PLACE_DEVICE_TYPE_SEL,  [ ]);
  dmOnega_DB_data.OpenStoredProc (sp_Filter_items_logical_status_sel, DEF_sp_Filter_items_logical_status_sel,  [ ]);

//  sp_Filter_items_sel.Locate('name',sp_Filter_sel['name'], []);

end;


// ---------------------------------------------------------------
procedure Tdlg_Filter.Save;
// ---------------------------------------------------------------
var
  k: Integer;
  sXML: WideString;
  sFilter: string;
  sFolderGUID: string;
  sName: string;

  oDoc: TXMLDocument;
  vNode: IXMLNode;
begin
  oDoc:=TdmXML.GetXMLDocumentRef;

  db_PostDataset(sp_Filter_items_PLACE_DEVICE_TYPE);
  db_PostDataset(sp_Filter_items_logical_status_sel);


  if cb_PLACE_DEVICE_TYPE.Checked then
    Check_Dataset_to_XML (oDoc, 'PLACE_DEVICE_TYPE', sp_Filter_items_PLACE_DEVICE_TYPE, 'values');

  if cb_Logical_Status.Checked then
    Check_Dataset_to_XML (oDoc, 'Logical_Status', sp_Filter_items_logical_status_sel, 'name');


  oDoc.SaveToXML(sXML);
 // oDoc.SaveToFile('d:\111.xml');


  k:=dmOnega_DB_data.ExecStoredProc_(DEF_Sp_Filter_UPD,
  [
 //   'id',  sp_Filter_sel['id'],
//    'name', sp_Filter_items_sel['name'],
    'xml', sXML
  ]);


{$IFDEF test}
EXIT;
{$ENDIF}


//..  sFolderGUID:=g_Obj.ItemByName[OBJ_PROJECT].RootFolderGUID;
//  g_Shell. UpdateNodeChildren_ByGUID (sFolderGUID);
//  g_Shell.EXPAND_BY_GUID(sFolderGUID);

  sFolderGUID:=g_Obj.ItemByName[OBJ_PROPERTY].RootFolderGUID;
  g_Shell.UpdateNodeChildren_ByGUID (sFolderGUID);
  g_Shell.EXPAND_BY_GUID(sFolderGUID);
  g_Shell.FocuseNodeByGUID(FFolderGUID);

  //////////////////////////////////

  dmOnega_DB_data.OpenQuery(q_Filter, 'SELECT ui_explorer.fn_Filter_value_v1()');
  if q_Filter.RecordCount>0 then
  begin
    sFilter:=q_Filter.Fields[0].Value;
    sName  :='��������'+ IIF((sFilter<>''), ' ('+sFilter+')', '');


    g_Shell.RENAME_ITEM_(sFolderGUID, sName);

  end;


//  dmMapEngine_store.Feature_Add(OBJ_PROPERTY, FID);

{
procedure TdmMapEngine_store.Reload_Object(aObjName: string);
begin
  if Is_Object_Map_Exists(aObjName) then
    Open_Object_Map(aObjName);

end;
}

  g_EventManager.PostEvent_(et_MAP_REFRESH_FROM_DESKTOP,[]);
//  g_EventManager.PostEvent_(WE_MAP_REFRESH_OBJECT_Themas,[]);



{
procedure TdmMapEngine_store.GetMessage(aMsg: TEventType; aParams:  TEventParamList ; var aHandled: boolean);
begin
  if not Assigned(FMap) then
    Exit;

  case aMsg of
    WE_MAP_REFRESH_OBJECT_Themas:
         RefreshObjectThemas_All ();

  end;

end;



  if dmMapEngine_store.Is_Object_Map_Exists(OBJ_PROPERTY) then
  begin
    dmMapEngine_store.Delete_Object_Map(OBJ_PROPERTY);
    dmMapEngine_store.Open_Object_Map(OBJ_PROPERTY);

    dmMapEngine_store.RefreshObjectThemas_All;

  end;
   }
end;


//--------------------------------------------------------
procedure Tdlg_Filter.Check_Dataset_to_XML(aDoc: TXMLDocument; aFieldName:
    string; aDataset: TDataset; aFieldName_Values: string);
//--------------------------------------------------------
var
  I: Integer;
  vFolder,vNode: IXMLNode;
  sValues: string;
  v: Variant;
begin

  vFolder:=aDoc.DocumentElement.AddChild('folder');
  vFolder.Attributes['name'] := aFieldName;

  aDataset.First;
  aDataset.DisableControls;
  sValues:='';

  with aDataset do
   while not EOF do
   begin
      if FieldByName('checked').AsBoolean then
      begin
        vNode:=vFolder.AddChild('item');
        vNode.Attributes['id']  := FieldValues['id'];
        
 //       vNode.Attributes['name']:= FieldValues['name'];
      //  vNode.Attributes['values']:= FieldValues['values'];

        v:=FieldValues[aFieldName_Values];;
        if FieldValues[aFieldName_Values] <> null then
          sValues:=sValues + FieldValues[aFieldName_Values] + ';';

//        vNode.Attributes['checked']  := True;

  //      if FieldByName('enabled').AsBoolean then

    //      vNode.Attributes['checked']:=aCheckListBox.Checked[i];

      end;

      Next;

   end;

  vFolder.Attributes['values'] := sValues;

  aDataset.First;
  aDataset.EnableControls;


//  oDoc.SaveToXML(Result);
end;

//----------------------------------------------------------------
procedure Tdlg_Filter.sp_Filter_items_PLACE_DEVICE_TYPEAfterPost(DataSet: TDataSet);
//----------------------------------------------------------------
var
  iID: Integer;
//label
//  exit_;
begin
  if FDisableControls then
    Exit;

  FDisableControls:=true;

  iID:=DataSet.FieldByName('id').AsInteger;
  DataSet.DisableControls;

  //-----------------------------------------
  // ��� �������� ������� - ������ ��������� �������
  //-----------------------------------------
  if DataSet.FieldByName('checked').AsBoolean and (DataSet.FieldByName('values').AsString='') then
  begin
    DataSet.First;

    with DataSet do
      while not EOF do
      begin
        if FieldByName('id').AsInteger<>iID then
          db_UpdateRecord__(DataSet, ['checked', False]);

        Next;
      end;

  end else

  //-----------------------------------------
  //  �������� ������� - ��� �� �������
  //-----------------------------------------
  if DataSet.FieldByName('checked').AsBoolean and (DataSet.FieldByName('values').AsString<>'') then
    if DataSet.Locate('name','���',[]) then
      db_UpdateRecord__(DataSet, ['checked', False]);


//exit_:
  DataSet.Locate('id', iID, []);
  DataSet.EnableControls;

  FDisableControls:=False;

end;


procedure Tdlg_Filter.cxGrid1DBTableView1Column1PropertiesEditValueChanged(
  Sender: TObject);
begin
//  ShowMessage (Sender.className);
  db_PostDataset(sp_Filter_items_PLACE_DEVICE_TYPE);

end;

end.

