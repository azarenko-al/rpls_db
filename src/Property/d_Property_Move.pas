unit d_Property_Move;

interface
{$I ver.Inc}

uses
  I_Options_,

  d_Wizard,

 // {$IFDEF test}
  dm_MapEngine_store,

//  {$ELSE}
 //   I_MapAct,
  //  dm_Act_Map,
  //  I_MapEngine,
//    dm_MapEngine,
//  {$ENDIF}


 // X_Options,


  dm_Property_tools,
  dm_Property,

  fr_geo_Coordinates,
  u_Geo,
  u_func,
  

  u_func_msg,

  Classes, Controls, Forms,  ActnList, StdCtrls,
  cxLookAndFeels, cxPropertiesStore, rxPlacemnt, ExtCtrls;

type
  Tdlg_Property_Move = class(Tdlg_Wizard)
    GroupBox1: TGroupBox;
    procedure FormCreate(Sender: TObject);

    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure act_OkExecute(Sender: TObject);
    
  private
    FBLPointOld,FBLPoint: TBLPoint;
    FPropertyID: integer;

    Fframe_geo_Coordinates: Tframe_geo_Coordinates;

  //  procedure ValidateGeoRegion;

    procedure DoOnPosChange(Sender: TObject);
    procedure Execute();

  public
    class function ExecDlg (aPropertyID: integer): boolean;
    
    procedure Move;
  end;


//======================================================
// implementation
//======================================================
implementation {$R *.DFM}


//--------------------------------------------------------------------
class function Tdlg_Property_Move.ExecDlg (aPropertyID: integer): boolean;
//--------------------------------------------------------------------
begin
  with Tdlg_Property_Move.Create(Application) do
  try
    FBLPoint   :=dmProperty.GetPos (aPropertyID);
    FBLPointOld:=FBLPoint;
    FPropertyID:=aPropertyID;


    Fframe_geo_Coordinates.Set_BLPoint_and_CoordSys(FBLPointOld, IOptions.Get_CoordSys());

//    Fframe_geo_Coordinates.Set_BLPoint_and_CoordSys(FBLPointOld, EK_CK_42);

{
 k:=IOptions.Get_CoordSys();

    if (aBLPoint_CK42.B=0) and (aBLPoint_CK42.B=0) then
//      Fframe_geo_Coordinates.Set_BLPoint_and_CoordSys (MakeBLPoint(0,0), EK_WGS84)
       //dmFolder.GetPath (FRec.FolderID);

      //  dmOnega_DB_data.Folder_GetFullPath(FRec.Folder_ID);

//    row_Pos.Text   :=geo_FormatDegreeWithCoordSys(aBLPoint_CK42, dmOptions.GetGeoCoord);

//    UpdateGroundHeight();
 /////   Assert(Assigned(IOptions));
      Fframe_geo_Coordinates.Set_BLPoint_and_CoordSys (MakeBLPoint(0,0), IOptions.Get_CoordSys() )

    else begin
      bl_WGS:=geo_BL_to_BL_2017_ (aBLPoint_CK42, EK_CK_42, IOptions.Get_CoordSys());
      Fframe_geo_Coordinates.Set_BLPoint_and_CoordSys (bl_WGS, IOptions.Get_CoordSys());

//      bl_WGS:=geo_BL_to_BL_2017_ (aBLPoint_CK42, EK_CK_42, EK_WGS84);
//      Fframe_geo_Coordinates.Set_BLPoint_and_CoordSys (bl_WGS, EK_WGS84);

    end;
 }


//    Fframe_geo_Coordinates.BLPoint_Pulkovo:=FBLPointOld;
//    Fframe_geo_Coordinates.DisplayCoordSys:=IOptions.Get_CoordSys;


//    dxButtonEdit1.Text:=geo_FormatDegreeWithCoordSys (FBLPointOld, dmOptions.GetGeoCoord);

    Result:=(ShowModal=mrOk);

    if Result then
      g_EventManager.postEvent_ (WE_PROPERTY_REFRESH, ['ID', aPropertyID ]);

    //  PostEvent (WE_PROPERTY_REFRESH, [app_Par(PAR_ID, aPropertyID)]);



  finally
    Free;
  end;
end;


//-------------------------------------------------------------------
procedure Tdlg_Property_Move.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;
  Caption:='����������';
  SetActionName ('����� ��������� ��� ��������.');

  CreateChildForm(Tframe_geo_Coordinates, Fframe_geo_Coordinates, GroupBox1);
  Fframe_geo_Coordinates.OnPosChange:=DoOnPosChange;

  GroupBox1.Height := Fframe_geo_Coordinates.GetBestHeight;


  SetDefaultSize();
end;


//--------------------------------------------------------------------
procedure Tdlg_Property_Move.Execute();
//--------------------------------------------------------------------
begin
//  dmProperty.Move (FPropertyID, FBLPoint);
end;


procedure Tdlg_Property_Move.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
begin
  act_Ok.Enabled:=not geo_ComparePoints(FBLPoint, FBLPointOld);
end;


procedure Tdlg_Property_Move.act_OkExecute(Sender: TObject);
begin
  Move();
end;

procedure Tdlg_Property_Move.DoOnPosChange(Sender: TObject);
begin
  if Sender=Fframe_geo_Coordinates then
     FBLPoint:=Fframe_geo_Coordinates.GetBLPoint_Pulkovo;
end;


//-------------------------------------------------------------------
procedure Tdlg_Property_Move.Move;
//-------------------------------------------------------------------
//var
//  I: Integer;
 // oPropItemsList: TIDList;

begin
 // Assert(Assigned(IMapEngine));
 // Assert(Assigned(IMapAct));

  dmProperty_tools.Move (FPropertyID, FBLPoint);
  dmProperty_tools.UpdateGroundHeight (FPropertyID);


  dmMapEngine_store.Reload_Object_Layers();

 // exit;

{
  oPropItemsList:= TIDList.Create;

  dmProperty_tools.GetSubItemList1 (FPropertyID, oPropItemsList);

  g_Log.Add('  ');
  g_Log.Add('Start moving property id='+AsString(FPropertyID)+'...');


  Assert(Assigned(dmMapEngine_store), 'Value not assigned');

  dmMapEngine_store.Feature_Update(OBJ_PROPERTY, FPropertyID);

  for I := 0 to oPropItemsList.Count-1 do
    dmMapEngine_store.Feature_Update (oPropItemsList[i].ObjName,
                                      oPropItemsList[i].ID);

}

 // dmMapEngine_store.
  //Feature_ Add(OBJ_PROPERTY, FPropertyID);


//
//  {$IFDEF test}
//  // dmMapEngine_store.Feature_Add(OBJ_Pmp_Terminal, FPropertyID);
//
//  {$ELSE}
//
//  dmMapEngine.ReCreateObject(otProperty, FPropertyID);
//
//  for I := 0 to oPropItemsList.Count-1 do
//    dmMapEngine.ReCreateObject (ObjNameToType(oPropItemsList[i].ObjName),
//                                oPropItemsList[i].ID);
//
//  FreeAndNil(oPropItemsList);
//  g_Log.Add('Moving property done.');
//
//  dmAct_Map.MapRefresh;
//  {$ENDIF}
//



end;




end.

