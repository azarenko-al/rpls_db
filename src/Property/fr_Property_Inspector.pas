unit fr_Property_Inspector;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, rxPlacemnt, StdCtrls, ExtCtrls, Math,
         

  dm_User_Security,

  fr_DBInspector_Container,

  d_Property_Move,

  u_func,
  u_db,
  u_dlg,

  u_const_db,
  u_const_msg,

  u_func_msg,

  u_types


  , DB;

type
  Tframe_Property_inspector = class(Tframe_DBInspector_Container)
    procedure FormCreate(Sender: TObject);
  //  procedure FormCreate(Sender: TObject);

  private
    FObjName: string;
    FTableName: string;

//    FIsPostEnabled: boolean;

  //  procedure DoOnCalcFields(DataSet: TDataSet);
   // FObjID: integer;

//    procedure DoOnFieldChanged (Sender: TObject; aFieldName: string; aNewValue: Variant); override;

  protected
    procedure DoOnPost(Sender: TObject); override;


    procedure DoOnButtonFieldClick(Sender: TObject; aFieldName: string; aNewValue:
        Variant); override;
  public
    procedure View (aID: integer);

  end;


implementation

uses dm_MapEngine_store;
{$R *.dfm}

procedure Tframe_Property_inspector.FormCreate(Sender: TObject);
begin
  inherited;


  //db_CreateCalculatedField(aMem_Data, 'lat_CK_95',  ftFloat);

end;

(*
procedure Tframe_Property_inspector.FormCreate(Sender: TObject);
begin
  inherited;
//  GetDataSet.OnCalcFields :=  DoOnCalcFields;

end;
*)

//-------------------------------------------------------------------
procedure Tframe_Property_inspector.DoOnButtonFieldClick(Sender: TObject;
    aFieldName: string; aNewValue: Variant);
//-------------------------------------------------------------------
const
  FLD_LAT_LON_KRAS_WGS = 'LAT_LON_KRAS_WGS';

begin
  if Eq(aFieldName, FLD_LAT_LON_KRAS_WGS) then
    if Tdlg_Property_Move.ExecDlg (ID) then
      View(ID);

end;


procedure Tframe_Property_inspector.DoOnPost(Sender: TObject);
begin
  inherited;

//
//var
//  oMsg: TMessageInfo;
//begin
//  inherited;
//
//  oMsg:=TMessageInfo.Create;
////  oMsg.IntValue := FStatusIntValue;
////  oMsg.StrValue := FStatusStrValue;
//  oMsg.StrValue := ObjectName;
//
////  PostMessage (Application.Handle, WE_MAP_UPDATE_LINK_STATUS, ID, Integer(oMsg));
//  PostMessage (Application.Handle, WE_MAP_UPDATE_DATA, ID, Integer(oMsg));


  dmMapEngine_store.Update_Data(OBJ_Property);


  g_EventManager.postEvent_ (WM_DATA_CHANGED, []);

//  PostMessage (Application.Handle, WM_DATA_CHANGED, 0,0);

end;

//-------------------------------------------------------------------
procedure Tframe_Property_inspector.View (aID: integer); //; aObjName: string
//-------------------------------------------------------------------
//var
//  b: Boolean;
//  iGeoRegion_ID: Integer;
begin
  PrepareViewForObject (OBJ_Property);

  inherited View(aID);

//  iGeoRegion_ID := GetIntFieldValue(FLD_GeoRegion_ID);

//  b:=dmUser_Security.GeoRegion_is_ReadOnly(iGeoRegion_ID);

//  SetReadOnly(b, 'GeoRegion:'+IntToStr(iGeoRegion_ID));

 // ShowMessage(IntToStr(iGeoRegion_ID));

end;

end.


(*
//-------------------------------------------------------------------
procedure Tframe_Property_inspector.DoOnFieldChanged (Sender: TObject; aFieldName: string; aNewValue: Variant);
//-------------------------------------------------------------------
begin

end;
*)

(*
procedure Tframe_Property_inspector.DoOnCalcFields(DataSet: TDataSet);
begin
(*  if Assigned(DataSet.FindField(FLD_LAT)) then
  begin
    if Assigned(DataSet.FindField(FLD_LAT)) then
    ;

  end;*)

(*

db_CreateCalculatedField(aMem_Data, 'lat_CK_95',  ftFloat);
  db_CreateCalculatedField(aMem_Data, 'lon_CK_95',  ftFloat);
  db_CreateCalculatedField(aMem_Data, 'lat_WGS_84', ftFloat);
  db_CreateCalculatedField(aMem_Data, 'lon_WGS_84', ftFloat);
*)
  //