object frm_Test: Tfrm_Test
  Left = 627
  Top = 259
  Width = 952
  Height = 509
  Caption = 'frm_Test'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 41
    Width = 944
    Height = 392
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 301
      Top = 5
      Height = 382
    end
    object pc_Main: TPageControl
      Left = 5
      Top = 5
      Width = 296
      Height = 382
      ActivePage = TabSheet1
      Align = alLeft
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'TabSheet1'
      end
    end
    object pn_Browser: TPanel
      Left = 304
      Top = 5
      Width = 635
      Height = 382
      Align = alClient
      Caption = 'pn_Browser'
      TabOrder = 1
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 944
    Height = 41
    ButtonHeight = 25
    Caption = 'ToolBar1'
    TabOrder = 1
    object Add: TButton
      Left = 0
      Top = 2
      Width = 75
      Height = 25
      Caption = 'add'
      TabOrder = 0
      OnClick = AddClick
    end
    object b_Map: TButton
      Left = 75
      Top = 2
      Width = 75
      Height = 25
      Caption = 'b_Map'
      TabOrder = 3
      OnClick = b_MapClick
    end
    object Button2: TButton
      Left = 150
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Button2'
      TabOrder = 4
      OnClick = Button2Click
    end
    object Button1: TButton
      Left = 225
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 2
      OnClick = Button1Click
    end
    object view: TButton
      Left = 300
      Top = 2
      Width = 75
      Height = 25
      Caption = 'view'
      TabOrder = 1
      OnClick = viewClick
    end
    object Button3: TButton
      Left = 375
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Button3'
      TabOrder = 5
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 450
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Button4'
      TabOrder = 6
      OnClick = Button4Click
    end
    object Button5: TButton
      Left = 525
      Top = 2
      Width = 75
      Height = 25
      Caption = 'prop test'
      TabOrder = 7
      OnClick = Button5Click
    end
    object Button6: TButton
      Left = 600
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Button6'
      TabOrder = 8
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 72
    Top = 96
  end
  object PBFolderDialog1: TPBFolderDialog
    Left = 824
    Top = 186
  end
end
