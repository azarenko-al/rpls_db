object frm_Test: Tfrm_Test
  Left = 300
  Top = 252
  Width = 611
  Height = 399
  Caption = 'frm_Test'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 29
    Width = 603
    Height = 321
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 301
      Top = 5
      Height = 311
    end
    object pc_Main: TPageControl
      Left = 5
      Top = 5
      Width = 296
      Height = 311
      ActivePage = TabSheet1
      Align = alLeft
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'TabSheet1'
      end
    end
    object pn_Browser: TPanel
      Left = 304
      Top = 5
      Width = 294
      Height = 311
      Align = alClient
      Caption = 'pn_Browser'
      TabOrder = 1
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 603
    Height = 29
    ButtonHeight = 25
    Caption = 'ToolBar1'
    TabOrder = 1
    object Add: TButton
      Left = 0
      Top = 2
      Width = 75
      Height = 25
      Caption = 'add'
      TabOrder = 0
    end
    object b_Map: TButton
      Left = 75
      Top = 2
      Width = 75
      Height = 25
      Caption = 'b_Map'
      TabOrder = 3
    end
    object Button2: TButton
      Left = 150
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Button2'
      TabOrder = 4
    end
    object Button1: TButton
      Left = 225
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 2
    end
    object view: TButton
      Left = 300
      Top = 2
      Width = 75
      Height = 25
      Caption = 'view'
      TabOrder = 1
    end
    object Button3: TButton
      Left = 375
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Button3'
      TabOrder = 5
    end
    object Button4: TButton
      Left = 450
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Button4'
      TabOrder = 6
    end
    object Button5: TButton
      Left = 525
      Top = 2
      Width = 75
      Height = 25
      Caption = 'prop test'
      TabOrder = 7
    end
  end
  object FormStorage1: TFormStorage
    StoredValues = <>
    Left = 72
    Top = 96
  end
end
