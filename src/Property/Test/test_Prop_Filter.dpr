program test_Prop_Filter;

uses
  Forms,
  Unit5 in 'Unit5.pas' {Form5},
  d_Filter in '..\d_Filter.pas' {dlg_Filter},
  dm_Settings in '..\..\Settings\dm_Settings.pas' {dmSettings: TDataModule},
  d_Objects_Deleted in '..\..\Settings\d_Objects_Deleted.pas' {dlg_Objects_Deleted},
  u_LinkFreqPlan_from_map_run in '..\..\LinkFreqPlan_Megafon\u_LinkFreqPlan_from_map_run.pas',
  u_ini_LinkFreqPlan in '..\..\..\.Parts from src\LinkFreqPlan_XE\src_shared\u_ini_LinkFreqPlan.pas',
  u_GEO in '..\..\..\..\common7\geo\u_Geo.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm5, Form5);
  Application.Run;
end.
