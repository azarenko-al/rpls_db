unit Unit5;

interface

uses
  dm_Onega_DB_data,
  d_Filter,
  dm_Settings,
  d_Settings_WMS,

  d_Objects_Deleted,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, ADODB, Grids, DBGrids, dxBar, dxBarExtDBItems,
  cxClasses;

type
  TForm5 = class(TForm)
    ADOConnection1: TADOConnection;
    Button1: TButton;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarLookupCombo1: TdxBarLookupCombo;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    q_WMS: TADOQuery;
    Button2: TButton;
    Button3: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form5: TForm5;

implementation

{$R *.dfm}

procedure TForm5.Button1Click(Sender: TObject);
begin
  Tdlg_Filter.ExecDlg('');

end;

procedure TForm5.Button2Click(Sender: TObject);
begin
  Tdlg_Settings_WMS.ExecDlg;
end;

procedure TForm5.Button3Click(Sender: TObject);
begin
  Tdlg_Objects_Deleted.ExecDlg;
end;

procedure TForm5.FormCreate(Sender: TObject);
begin
  TdmOnega_DB_data.Init;
  dmOnega_DB_data.ADOConnection:=ADOConnection1;


  TdmSettings.Init;
end;

end.
