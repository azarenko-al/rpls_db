unit a_test_copy_map;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,

  dm_Main,

  dm_Custom,

  dm_Act_Project,

  dm_Act_Map_Engine,

  u_const,
  u_const_msg,
  u_const_db,
  u_types,

  u_reg,
  u_db,
  u_dlg,
  u_func_msg,

  u_Geo,

  f_Log,

  dm_Property,


  StdCtrls, rxPlacemnt, ExtCtrls, ComCtrls, ToolWin
  ;

type
  Tfrm_Test = class(TForm)
    FormStorage1: TFormStorage;
    Panel1: TPanel;
    Splitter1: TSplitter;
    pc_Main: TPageControl;
    TabSheet1: TTabSheet;
    pn_Browser: TPanel;
    ToolBar1: TToolBar;
    Add: TButton;
    view: TButton;
    Button1: TButton;
    b_Map: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton; 
    procedure FormCreate(Sender: TObject); 
  private

    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_Test: Tfrm_Test;
  id: integer;


implementation

uses dm_Property_tools;



 {$R *.DFM}

procedure Tfrm_Test.FormCreate(Sender: TObject);
begin
  gl_Reg.InitRegistry(REGISTRY_ROOT);


  TdmMain.Init;
  if not dmMain.OpenDlg then
    Exit;


  dmMain.ProjectID:=478;

  dmAct_Map_Engine.Enabled:=False;

  dmAct_Project.LoadLastProject;


  dmProperty_tools.SaveToImage_all;

end;

end.

