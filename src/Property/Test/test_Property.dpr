program test_Property;

uses
  a_Unit in 'a_Unit.pas' {frm_Test},
  d_Property_add in '..\d_Property_add.pas' {dlg_Property_add},
  d_Property_Move in '..\d_Property_Move.pas' {dlg_Property_Move},
  dm_Act_Project1 in '..\..\Project\dm_Act_Project1.pas' {dmAct_Project1: TDataModule},
  dm_act_Property in '..\dm_act_Property.pas' {dmAct_Property: TDataModule},
  dm_Antenna_tools in '..\..\Antenna\dm_Antenna_tools.pas' {dmAntenna_tools: TDataModule},
  dm_Antenna_view in '..\..\Antenna\dm_Antenna_view.pas' {dmAntenna_view: TDataModule},
  dm_Folder in '..\..\Folder\dm_Folder.pas' {dmFolder: TDataModule},
  dm_GeoRegion in '..\..\GeoRegion\dm_GeoRegion.pas' {dmGeoRegion: TDataModule},
  dm_Object_base in '..\..\Common\dm_Object_base.pas' {dmObject_base: TDataModule},
  dm_Property in '..\dm_Property.pas' {dmProperty: TDataModule},
  dm_Property_export in '..\dm_Property_export.pas' {dmProperty_export: TDataModule},
  dm_Property_View in '..\dm_Property_View.pas' {dmProperty_View: TDataModule},
  dm_Site in '..\..\Site\dm_Site.pas' {dmSite: TDataModule},
  dm_Site_Tools in '..\..\Site\dm_Site_Tools.pas' {dmSite_Tools: TDataModule},
  dm_UserAuth in '..\..\User\dm_UserAuth.pas' {dmUserAuth: TDataModule},
  dm_UserGeoRegions in '..\..\User\dm_UserGeoRegions.pas' {dmUserGeoRegions: TDataModule},
  Forms,
  fr_Property_Inspector in '..\fr_Property_Inspector.pas' {frame_Property_inspector},
  fr_Property_items in '..\fr_Property_items.pas' {frame_Property_Items},
  fr_Property_view in '..\fr_Property_view.pas' {frame_Property_View},
  I_Main in '..\..\DataModules\I_Main.pas' {dmMainX: TDataModule},

  u_const_str in '..\..\u_const_str.pas',
  u_types in '..\..\u_types.pas';

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(Tfrm_Test, frm_Test);
  Application.Run;
end.

