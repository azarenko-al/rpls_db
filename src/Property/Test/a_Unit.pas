unit a_Unit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,

  dm_Property_Tools,

  I_Options,
  
  d_Property_add,

  dm_Act_Explorer,

  dm_Act_Property,

  dm_Main,
  dm_Main,

  u_func, 

  dm_UserAuth,

  dm_Custom,

  dm_Act_Project,

  dm_Act_Map_Engine,
  dm_Act_Link,

 // d_Property_add,

  u_const,
  u_const_msg,
  u_const_db,
  u_types,

  u_reg,
  u_db,
  u_dlg,
  u_func_msg,

  u_Geo,

  f_Log,
  fr_Property_View,

  dm_Property,

  dm_Explorer,
  fr_Explorer_Container,
//  u_Explorer,

  //d_Property_add,

  fr_Browser,



  StdCtrls, rxPlacemnt, ExtCtrls, ComCtrls, ToolWin, PBFolderDialog
  ;

type
  Tfrm_Test = class(TForm)
    FormStorage1: TFormStorage;
    Panel1: TPanel;
    Splitter1: TSplitter;
    pc_Main: TPageControl;
    TabSheet1: TTabSheet;
    pn_Browser: TPanel;
    ToolBar1: TToolBar;
    Add: TButton;
    view: TButton;
    Button1: TButton;
    b_Map: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    PBFolderDialog1: TPBFolderDialog;
    procedure AddClick(Sender: TObject);
    procedure viewClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure b_MapClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private

    Fframe_Explorer : Tframe_Explorer_Container;
    Fframe_Browser : Tframe_Browser;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_Test: Tfrm_Test;
  id: integer;


implementation



 {$R *.DFM}


procedure Tfrm_Test.AddClick(Sender: TObject);
begin
//  id:=gl_DB.GetMaxID (TBL_PROPERTY_FOLDER);
//  dmAct_Property.Add (0);
end;


procedure Tfrm_Test.viewClick(Sender: TObject);
begin
  //id:=gl_DB.GetMaxID (TBL_PROPERTY);
  //Tframe_Property_View.CreateForm (id);
end;


procedure Tfrm_Test.FormCreate(Sender: TObject);
var
  i: Integer;
begin
 // gl_Reg:=TRegStore_Create(REGISTRY_ROOT);


  // -------------------------------------------------------------------
  TdmMain.Init;
  IOptions_Init;

//  IShellFactory_Init (dmMain);
  // -------------------------------------------------------------------

  TdmAct_Property.Init;

  TdmAct_Explorer.Init;
  TdmAct_Project.Init;

//  gl_Reg.InitRegistry(REGISTRY_ROOT);

//  TdmCustom.InitADOConnections;

//  init_UpdateADOConnections;

 // Tfrm_Log.CreateForm;

 //  TdmMain.Init;
  if not dmMain.OpenDlg then
    Exit;

//  dmUserAuth.Load;


 //  Tdlg_Property_add_test;


//  dmAct_Map_Engine.Enabled:=False;
{

  CreateChildForm (Tframe_Explorer_Container, Fframe_Explorer, TabSheet1);
//  Fframe_Explorer := Tframe_Explorer_Container.CreateChildForm (TabSheet1);
  Fframe_Explorer.SetViewObjects([otProperty, otLink, otLinkEnd, otGeoRegion]);
  Fframe_Explorer.RegPath:='Property';
  Fframe_Explorer.Load;


  CreateChildForm(Tframe_Browser,Fframe_Browser,pn_Browser);
 // Fframe_Browser := .CreateChildForm(pn_Browser);
  Fframe_Explorer.OnNodeFocused:=Fframe_Browser.ViewObjectByPIDL;

  dmAct_Project.LoadLastProject;
}


 dmAct_Project.LoadLastProject;


 Tdlg_Property_add.ExecDlg(0, NULL_BLPOINT);


  i:=gl_DB.GetMaxID1(TBL_Property );


 with Tframe_Property_View.Create(Self) do
  begin
    View(i,'');
//    View(53912,'');
    ShowModal;
    Free;
  end;


//  dmProperty_tools.ExportToRPLS_XML('');
end;

procedure Tfrm_Test.FormDestroy(Sender: TObject);
begin

//  Fframe_Explorer.Free;
//  Fframe_Browser.Free;
 
end;



procedure Tfrm_Test.Button1Click(Sender: TObject);
begin
  dmProperty.GetNearestID (MakeBLPoint(53,46));
end;


procedure Tfrm_Test.Button2Click(Sender: TObject);
var aName: string;
begin
 // Tdlg_expl_Object_Select.ExecDlg (otProperty, 0, aName);

end;


procedure Tfrm_Test.b_MapClick(Sender: TObject);
begin
//  PostMsg(WM_SHOW_FORM_MAP);
end;

procedure Tfrm_Test.Button3Click(Sender: TObject);
var s: string;
begin
//  dmAct_Explorer.Dlg_Select_Object (otAntennaType, 1, s);
end;

procedure Tfrm_Test.Button4Click(Sender: TObject);
begin
  PostEvent (WE_EXPLORER_EXPAND_BY_GUID, [app_Par(PAR_GUID, GUID_PROJECT  )]);
  PostEvent (WE_EXPLORER_EXPAND_BY_GUID, [app_Par(PAR_GUID, GUID_PROPERTY )]);

end;

procedure Tfrm_Test.Button5Click(Sender: TObject);
begin
 // Tdlg_Property_add_test;
end;


end.
