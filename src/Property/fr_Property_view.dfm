inherited frame_Property_View: Tframe_Property_View
  Left = 754
  Top = 338
  Width = 412
  Height = 531
  Caption = 'frame_Property_View'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 404
    TabOrder = 1
  end
  inherited pn_Caption: TPanel
    Width = 404
    TabOrder = 0
  end
  inherited pn_Main: TPanel
    Width = 404
    Height = 251
    object pn_Inspector: TPanel
      Left = 1
      Top = 1
      Width = 402
      Height = 112
      Align = alTop
      BevelOuter = bvLowered
      Caption = 'pn_Inspector'
      Constraints.MinHeight = 100
      TabOrder = 0
    end
    object cxSplitter1: TcxSplitter
      Left = 1
      Top = 113
      Width = 402
      Height = 8
      HotZoneClassName = 'TcxSimpleStyle'
      AlignSplitter = salTop
      Control = pn_Inspector
    end
    object pn_Bottom: TPanel
      Left = 1
      Top = 148
      Width = 402
      Height = 102
      Align = alBottom
      BevelOuter = bvLowered
      Caption = 'pn_Bottom'
      TabOrder = 2
    end
  end
  inherited cxLookAndFeelController1: TcxLookAndFeelController
    Top = 432
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    Top = 376
    DockControlHeights = (
      0
      0
      25
      0)
  end
end
