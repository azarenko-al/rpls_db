unit dm_Property_tools;

interface
//{$I ver.inc}

uses
  SysUtils, Classes, DB, ADODB, Forms, Windows, dxmdaset, Controls, Graphics, Variants,
  MapXLib_TLB,

  u_func_msg,
  u_const_msg,

  u_dll_geo_convert,

  dm_Onega_DB_data,

  dm_Main,

 // I_Shell,
 // I_MapEngine,
  I_rel_Matrix1,

//  d_MIF_export,

  dm_ClutterModel,

//  dm_Link,
//  dm_LinkEnd,

//  dm_PMPterminal,
//  dm_PMP_Site,

//  dm_Antenna,
  dm_GeoRegion,
  dm_Property,
  dm_Rel_Engine,

  u_classes,
  u_Geo,
  u_Geo_classes,

  u_geo_convert_new,

  u_Log,

  u_func,
  u_db,
  u_dlg,
  u_files,
  u_mapx_lib,
  u_mapx,


  u_types,
  u_const_db,


  Dialogs
  ;

type

  TdmProperty_tools = class(TDataModule)
    qry_Antennas: TADOQuery;
    SaveDialog1: TSaveDialog;
    qry_Temp: TADOQuery;
    ADOStoredProc1: TADOStoredProc;
    procedure DataModuleCreate(Sender: TObject);
  private
    function GetCoordStr(aBLPoint: TBLPoint): string;
  public

    procedure UpdateGeoRegionForProperties;

    function Del(aID: integer; aIDList : TIDList): boolean;

    procedure GetSubItemList1(aID: integer; aItemList: TIDList);
    procedure Move (aID: integer; aBLPoint: TBLPoint);

    procedure Move_By_GeoRegions(aIDList: TIDList);

    procedure UpdateGroundHeight_ByFolder(aFolderID: integer);

    procedure UpdateGroundHeight_List (aPropIDList: TIDList; aFolderID: integer);
    function  UpdateGroundHeight (aID: integer): boolean;

    function UpdateGroundHeightForBLPointList(aBLPointList: TBLPointList): boolean;
    procedure Update_LatLonStr(aID: integer; aBLPoint: TBLPoint);

  end;


function dmProperty_tools: TdmProperty_tools;

implementation
{$R *.dfm}

var
  FdmProperty_tools: TdmProperty_tools;


// -------------------------------------------------------------------
function dmProperty_tools: TdmProperty_tools;
// -------------------------------------------------------------------
begin
 if not Assigned(FdmProperty_tools) then
     FdmProperty_tools := TdmProperty_tools.Create(Application);

  Result := FdmProperty_tools;
end;

procedure TdmProperty_tools.DataModuleCreate(Sender: TObject);
begin
  inherited;

  db_SetComponentADOConnection (Self, dmMain.ADOConnection);
end;


//-------------------------------------------------------------------
function TdmProperty_tools.GetCoordStr(aBLPoint: TBLPoint): string;
//-------------------------------------------------------------------
var
  iScale: integer;
//  dMapLengthInCm, dMapLengthInKm, dZoom, dMapX, dMapY: Double;

  bl: TblPoint;
  k: Integer;
  S: string;

 // xyPoint: TxyPoint;

const
  DEF_DELIMITER = '  |  ';

begin
    assert(aBLPoint.B<>0);

    s:='';

    s:=s + geo_FormatBLPoint(aBLPoint) + ' (CK-42)' + DEF_DELIMITER;

    bl:= geo_Pulkovo42_to_CK_95 (aBLPoint);//dmOptions.GetGeoCoord);
    s:=s  + geo_FormatBLPoint(bl)+ ' (CK-95)' + DEF_DELIMITER;

    CK42_to_GSK_2011 (aBLPoint.B, aBLPoint.L, bl.B, bl.L  );
    s:=s  + geo_FormatBLPoint(bl) + ' (�CK-2011)'+ DEF_DELIMITER;

    bl:= geo_Pulkovo42_to_WGS84 (aBLPoint);//dmOptions.GetGeoCoord);
    s:=s  + geo_FormatBLPoint(bl) + ' (WGS-84)';//+ DEF_DELIMITER;

   result:=s;
end;

// ---------------------------------------------------------------
procedure TdmProperty_tools.Update_LatLonStr(aID: integer; aBLPoint: TBLPoint);
// ---------------------------------------------------------------
var
  s: string;
begin
  s:= GetCoordStr (aBLPoint);

  dmOnega_DB_data.UpdateRecordByID(TBL_PROPERTY, aID,
   [
     db_Par( FLD_lat_lon_kras_wgs, s)
   ]

  );
end ;


//--------------------------------------------------------------------
procedure TdmProperty_tools.Move (aID: integer; aBLPoint: TBLPoint);
//--------------------------------------------------------------------
var
//  BLVector: TBLVector;

  //dAzimuth: double;

  wgs_Point: TBLPoint;
 // bl_CK95: TBLPoint;

 // eLen_m: Double;
 // iID: integer;
  //dAzimuth1: Double;
 // dAzimuth2: Double;
  r: Integer;
begin
  wgs_Point:=geo_BL_to_BL_new(aBLPoint, EK_CK_42, EK_WGS84);

//  bl_CK95 := geo_Pulkovo_CK_42_to_CK_95(aBLPoint);
//  bl_CK95 := geo_Pulkovo42_to_CK_95(aBLPoint);
//  bl_CK95 := geo_Pulkovo_BL_to_BL_95(aBLPoint);

//  r:=dmOnega_DB_data.PROPERTY_MOVE_new (aID, aBLPoint.B, aBLPoint.L);

  r := dmOnega_DB_data.ExecStoredProc_('sp_Property_MOVE',
              [FLD_Property_id, aID ,

               FLD_LAT, aBLPoint.B ,
               FLD_LON, aBLPoint.L ,

               FLD_LAT_WGS, wgs_Point.B,
               FLD_LON_WGS, wgs_Point.L

//               FLD_LAT_CK95, aLAT_CK95,
//               FLD_LON_CK95, aLON_CK95

              ]);




{
  r:=dmOnega_DB_data.PROPERTY_MOVE (aID, aBLPoint.B, aBLPoint.L,
                                     wgs_Point.B, wgs_Point.L,
                                     bl_CK95.B,   bl_CK95.L
                                     );
}


  Update_LatLonStr (aID, aBLPoint);

  //lat_lon_kras_wgs


end;


//-------------------------------------------------------------------
procedure TdmProperty_tools.GetSubItemList1(aID: integer; aItemList: TIDList);
//-------------------------------------------------------------------
begin
  dmOnega_DB_data.Property_GetItemsList(ADOStoredProc1, aID);

  with ADOStoredProc1 do
    while not Eof do
  begin
    aItemList.AddObjNameAndGUID1 (
      FieldValues[FLD_ID], FieldValues[FLD_OBJNAME], FieldValues[FLD_GUID]);

    Next;
  end;

end;

//-------------------------------------------------------------------
function TdmProperty_tools.Del(aID: integer; aIDList : TIDList): boolean;
//-------------------------------------------------------------------
begin
 // Assert(Assigned(IMapEngine));

 // oIDList := TIDList.Create();
//  oIDList.Clear;

  GetSubItemList1(aID, aIDList);

  if aIDList.Count>0 then
  begin
    ShowMessage('�� �������� ��������� �������. �������� ����������.');

    Result:=False;
    Exit;
  end;



{  dmMapEngine1.DelList(oIDList);
  g_ShellEvents.Shell_PostDelNodeByList (oIDList);
}



  Result:= dmProperty.Del(aID);

 // oIDList.Free;
end;


//-------------------------------------------------------------------
function TdmProperty_tools.UpdateGroundHeightForBLPointList(aBLPointList: TBLPointList):
    boolean;
//-------------------------------------------------------------------
var
  I,iID: Integer;
  blPoint: TBLPoint;
  oRelRec: Trel_Record;
  sClutterName: string;
begin
 // blPoint:= dmProperty.GetPos(aID);

 // TdmRel_Engine.Init;

 // Assert(Assigned(dmRel_Engine), 'Value not assigned');

  dmRel_Engine.Open ();

  for I := 0 to aBLPointList.Count - 1 do
  begin
  //  g_Log.Add('Restre rel info for property: '+ aBLPointList.Items[i].Name);

    blPoint :=aBLPointList.Items[i].BLPoint;
    iID :=aBLPointList.Items[i].ID;

    if dmRel_Engine.getRelInfo_(blPoint, oRelRec) then
    begin
      sClutterName:= dmRel_Engine.GetClutterNameByCode(oRelRec.Clutter_Code);

    //  sCluName:= gl_DB.GetStringFieldValue(TBL_CLUTTER_TYPE, FLD_NAME, [db_Par(FLD_CODE, oRelRec.Loc_Code)]);


     dmOnega_DB_data.PROPERTY_Update_Ground (iID,
             oRelRec.Rel_H, oRelRec.Clutter_H, sClutterName);

  //    Result := True;
    end;
  end;

 // dmRel_Engine.Close1;
end;



//-------------------------------------------------------------------
function TdmProperty_tools.UpdateGroundHeight(aID: integer): boolean;
//-------------------------------------------------------------------
var
  blPoint: TBLPoint;
  oRelRec: Trel_Record;
  sClutterName: string;
begin
  blPoint:= dmProperty.GetPos(aID);

 // TdmRel_Engine.Init;

 // Assert(Assigned(dmRel_Engine), 'Value not assigned');

  dmRel_Engine.Open_All ();

  if dmRel_Engine.GetRelInfo_(blPoint, oRelRec) then
  begin
    sClutterName:= dmRel_Engine.GetClutterNameByCode(oRelRec.Clutter_Code);

  //  sCluName:= gl_DB.GetStringFieldValue(TBL_CLUTTER_TYPE, FLD_NAME, [db_Par(FLD_CODE, oRelRec.Loc_Code)]);

    dmOnega_DB_data.PROPERTY_Update_Ground (aID,
             oRelRec.Rel_H, oRelRec.Clutter_H, sClutterName);

    Result := True;
  end else
    Result := False;

 // dmRel_Engine.Close1;
end;


//-------------------------------------------------------
procedure TdmProperty_tools.UpdateGeoRegionForProperties;
//-------------------------------------------------------
var
  iGeoRegionID: Integer;
  blPoint: TBLPoint;
begin
  Screen.Cursor := crHourGlass;
  try
  // db_OpenQuery(qry_Temp, 'SELECT id,lat,lon FROM ' +  TBL_PROPERTY );
    dmOnega_DB_data.OpenQuery (qry_Temp, 'SELECT id,lat,lon FROM ' +  TBL_PROPERTY +
                           ' WHERE (project_id=:project_id)', //(georegion_id is NULL) and
                  [FLD_project_id,   dmMain.ProjectID  ]);
                              // db_Par(FLD_project_id,   dmMain.ProjectID)
                           //   ]);

  // db_ViewDataSet (qry_Temp);

    with qry_Temp do
       while not EOF do
     begin
       blPoint:=db_ExtractBLPoint (qry_Temp);

       iGeoRegionID:=dmGeoRegion.GetID_ByBLPoint(blPoint);

       if iGeoRegionID>0 then
         dmOnega_DB_data.PROPERTY_Update_GeoRegion (qry_Temp[FLD_ID], iGeoRegionID);

    //     dmProperty.Update (qry_Temp[FLD_ID],
      //                        [db_Par(FLD_georegion_ID, iGeoRegionID)
        //                      ]);
       Next;
     end;

  finally
  	Screen.Cursor := crDefault;
  end;  // try/finally

  if qry_Temp.IsEmpty then
    ShowMessage('�� ������� �������� ��� �������.');

end;

//--------------------------------------------------------------------
procedure TdmProperty_tools.UpdateGroundHeight_List (aPropIDList: TIDList; aFolderID: integer);
//--------------------------------------------------------------------
var
  sClutterName: string;
  blPoint: TBLPoint;
  rel_rec:  TRel_Record;
  i: Integer;
begin
  CursorHourGlass;

  if aPropIDList.Count=0 then
  begin
    if aFolderID = 0 then
      db_OpenTable1(qry_Temp, TBL_PROPERTY, [db_Par(FLD_PROJECT_ID, dmMain.ProjectID) ])
    else
      db_OpenTable1(qry_Temp, TBL_PROPERTY, [db_Par(FLD_FOLDER_ID,  aFolderID),
                                            db_Par(FLD_PROJECT_ID, dmMain.ProjectID) ]);


    while not qry_Temp.EOF do
    begin
      aPropIDList.AddID(qry_Temp.FieldByName(FLD_ID).AsInteger);

      qry_Temp.Next;
    end;    // while

  end;


  try

  for i := 0 to aPropIDList.Count-1 do
  begin
    if aPropIDList[i].ID = 0 then
      exit;

    blPoint:= dmProperty.GetPos(aPropIDList[i].ID);

    dmRel_Engine.Open();

    if dmRel_Engine.GetRelInfo_ (blPoint, rel_rec) then
    begin
      sClutterName:= dmRel_Engine.GetClutterNameByCode(rel_rec.Clutter_Code);

      dmOnega_DB_data.PROPERTY_Update_Ground (aPropIDList[i].ID,
               rel_rec.Rel_H, rel_rec.Clutter_H, sClutterName);
    end else
      dmOnega_DB_data.PROPERTY_Update_Ground (aPropIDList[i].ID,
               -999, 0, '');


(*
    dmProperty.Update (aPropIDList[i].ID,
                   [db_par(FLD_GROUND_HEIGHT, rel_rec.Rel_H),
                    db_par(FLD_CLUTTER_HEIGHT,rel_rec.Clutter_H),
                    db_par(FLD_CLUTTER_NAME,  sClutterName)
                    ]);
*)
{ TODO : remove }

    g_EventManager.postEvent_ (WE_PROPERTY_REFRESH, ['ID', aPropIDList[i].ID]);
//    PostEvent (WE_PROPERTY_REFRESH, [app_Par(PAR_ID, aPropIDList[i].ID)]);

  //  dmRel_Engine.Close;
  end;



  finally
    CursorDefault;
  end;

end;


//--------------------------------------------------------------------
procedure TdmProperty_tools.Move_By_GeoRegions(aIDList: TIDList);
//--------------------------------------------------------------------
var
  i,iRes: Integer;
begin
  CursorSQL;

  try
    for i := 0 to aIDList.Count-1 do
  //  begin

      iRes:=dmOnega_DB_data.ExecStoredProc_ (sp_Property_Move_to_GeoRegion_folder,
               [FLD_ID, aIDList[i].ID ]);

  finally
  end;

  CursorDefault;


end;

// ---------------------------------------------------------------
procedure TdmProperty_tools.UpdateGroundHeight_ByFolder(aFolderID: integer);
// ---------------------------------------------------------------

var
  sClutterName: string;
  blPoint: TBLPoint;
  rel_rec:  TRel_Record;
  i: Integer;
  oPropIDList: TIDList;
begin
  oPropIDList:=TIDList.create;

  CursorHourGlass;

//  if aPropIDList.Count=0 then
//  begin
    if aFolderID = 0 then
      db_OpenTable1(qry_Temp, TBL_PROPERTY, [db_Par(FLD_PROJECT_ID, dmMain.ProjectID) ])
    else
      db_OpenTable1(qry_Temp, TBL_PROPERTY, [db_Par(FLD_FOLDER_ID,  aFolderID),
                                            db_Par(FLD_PROJECT_ID, dmMain.ProjectID) ]);


    while not qry_Temp.EOF do
    begin
      oPropIDList.AddID(qry_Temp.FieldByName(FLD_ID).AsInteger);

      qry_Temp.Next;
    end;    // while

 // end;


  try

  for i := 0 to oPropIDList.Count-1 do
  begin
    if oPropIDList[i].ID = 0 then
      exit;

    blPoint:= dmProperty.GetPos(oPropIDList[i].ID);

    dmRel_Engine.Open();

    if dmRel_Engine.GetRelInfo_ (blPoint, rel_rec) then
    begin
      sClutterName:= dmRel_Engine.GetClutterNameByCode(rel_rec.Clutter_Code);

      dmOnega_DB_data.PROPERTY_Update_Ground (oPropIDList[i].ID,
               rel_rec.Rel_H, rel_rec.Clutter_H, sClutterName);
    end else
      dmOnega_DB_data.PROPERTY_Update_Ground (oPropIDList[i].ID,
               -999, 0, '');


(*
    dmProperty.Update (aPropIDList[i].ID,
                   [db_par(FLD_GROUND_HEIGHT, rel_rec.Rel_H),
                    db_par(FLD_CLUTTER_HEIGHT,rel_rec.Clutter_H),
                    db_par(FLD_CLUTTER_NAME,  sClutterName)
                    ]);
*)
{ TODO : remove }

    g_EventManager.postEvent_ (WE_PROPERTY_REFRESH, ['ID', oPropIDList[i].ID]);
//    PostEvent (WE_PROPERTY_REFRESH, [app_Par(PAR_ID, aPropIDList[i].ID)]);

  //  dmRel_Engine.Close;
  end;



  finally
    CursorDefault;
  end;


  freeAndNil (oPropIDList);

end;


begin
end.


