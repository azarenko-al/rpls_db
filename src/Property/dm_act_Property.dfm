inherited dmAct_Property: TdmAct_Property
  OldCreateOrder = True
  Left = 2019
  Top = 612
  Width = 325
  inherited ActionList1: TActionList
    Left = 48
    object act_Add_coord: TAction
      Caption = 'act_Add_coord'
    end
    object act_Move: TAction
      Caption = 'act_Move'
    end
    object act_Site_Add1: TAction
      Category = 'site'
      Caption = 'act_Site_Add1'
    end
    object act_LinkEnd_Add: TAction
      Category = 'Add'
      Caption = 'act_LinkEnd_Add'
    end
    object act_Refresh_MP_Height: TAction
      Category = 'prop'
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1074#1099#1089#1086#1090#1091' '#1079#1077#1084#1083#1080
    end
    object act_Refresh_GeoRegion: TAction
      Category = 'prop'
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1075#1077#1086' '#1088#1077#1075#1080#1086#1085#1099
    end
    object act_Make_Maps: TAction
      Caption = #1042#1099#1082#1086#1087#1080#1088#1086#1074#1082#1072
    end
    object act_Export_RPLS_XML: TAction
      Caption = 'act_Export_RPLS_XML'
    end
    object act_Export_RPLS_XML_folder: TAction
      Caption = 'act_Export_RPLS_XML_folder'
    end
    object act_Object_dependence: TAction
      Category = 'prop'
      Caption = 'act_Object_dependence'
    end
    object act_Print_map: TAction
      Caption = 'act_Print_map'
    end
    object act_Import_RPLS_XML: TAction
      Caption = 'act_Import_RPLS_XML'
    end
    object act_pmp_Terminal_Add: TAction
      Category = 'Add'
      Caption = 'act_pmp_Terminal_Add'
    end
    object act_Export_Google: TAction
      Category = 'Export'
      Caption = 'act_Export_Google'
    end
    object act_pmp_Site_Add: TAction
      Category = 'Add'
      Caption = 'act_pmp_Site_Add'
    end
    object act_Import_KML: TAction
      Caption = 'act_Import_KML'
    end
    object act_Geocoding_Address: TAction
      Caption = 'act_Geocoding_Address'
    end
    object act_Refresh_MP_Height_folder: TAction
      Caption = 'act_Refresh_MP_Height_folder'
    end
  end
  inherited ActionList_Folders: TActionList
    Left = 216
  end
  object ActionList2: TActionList
    Left = 48
    Top = 96
    object act_Add_: TAction
      Caption = 'Add'
    end
    object act_Export_MDB: TAction
      Caption = 'act_Export_MDB'
    end
    object act_MSC_Add: TAction
      Caption = 'act_MSC_Add'
    end
    object act_BSC_Add: TAction
      Caption = 'act_BSC_Add'
    end
    object act_Repeater_Add: TAction
      Caption = 'act_Repeater_Add'
    end
    object act_Move_By_GeoRegions: TAction
      Caption = 'act_Move_By_GeoRegions'
    end
    object act_Apply_template: TAction
      Caption = 'act_Apply_template'
    end
    object act_Audit: TAction
      Caption = 'act_Audit'
    end
    object act_Filter: TAction
      Caption = 'act_Filter'
      OnExecute = act_FilterExecute
    end
    object act_Property_LOS: TAction
      Caption = 'act_Property_LOS'
    end
  end
end
