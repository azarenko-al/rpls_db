object dlg_Filter: Tdlg_Filter
  Left = 1223
  Top = 134
  BorderStyle = bsDialog
  BorderWidth = 10
  Caption = 'dlg_Filter'
  ClientHeight = 745
  ClientWidth = 594
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  DesignSize = (
    594
    745)
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 430
    Top = 384
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Ok'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object Button2: TButton
    Left = 518
    Top = 384
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 1
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 24
    Width = 594
    Height = 160
    Align = alTop
    TabOrder = 2
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = ''
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = ds_Filter_items_PLACE_DEVICE_TYPE
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGrid1DBTableView1Column1: TcxGridDBColumn
        Caption = #1042#1082#1083'.'
        DataBinding.FieldName = 'checked'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.ImmediatePost = True
        Properties.NullStyle = nssUnchecked
        Properties.OnEditValueChanged = cxGrid1DBTableView1Column1PropertiesEditValueChanged
        Width = 38
      end
      object col_Name: TcxGridDBColumn
        Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'name'
        Options.Editing = False
        Width = 124
      end
      object col_Values: TcxGridDBColumn
        Caption = #1059#1089#1083#1086#1074#1080#1077
        DataBinding.FieldName = 'values'
        Options.Editing = False
        Width = 165
      end
      object cxGrid1DBTableView1Column2: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxGrid2: TcxGrid
    Left = 0
    Top = 224
    Width = 593
    Height = 153
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 3
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = ''
    object cxGridDBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = ds_sp_Filter_items_logical_status_sel
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGridDBColumn1: TcxGridDBColumn
        Caption = #1042#1082#1083'.'
        DataBinding.FieldName = 'checked'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.ImmediatePost = True
        Properties.NullStyle = nssUnchecked
        Width = 38
      end
      object cxGridDBColumn2: TcxGridDBColumn
        Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'name'
        Options.Editing = False
        Width = 124
      end
      object cxGridDBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
      end
      object cxGridDBTableView1Column2: TcxGridDBColumn
        DataBinding.FieldName = 'comment'
        Width = 122
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object cb_Logical_Status: TCheckBox
    Left = 0
    Top = 200
    Width = 201
    Height = 17
    Caption = 'cb_Logical_Status'
    TabOrder = 4
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 594
    Height = 24
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 5
    object cb_PLACE_DEVICE_TYPE: TCheckBox
      Left = 0
      Top = 0
      Width = 257
      Height = 17
      Caption = 'cb_PLACE_DEVICE_TYPE'
      TabOrder = 0
    end
  end
  object sp_Filter_items_PLACE_DEVICE_TYPE: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    AfterPost = sp_Filter_items_PLACE_DEVICE_TYPEAfterPost
    ProcedureName = 'ui_explorer.sp_Filter_items_SEL'
    Parameters = <>
    Prepared = True
    Left = 192
    Top = 472
  end
  object ds_Filter_items_PLACE_DEVICE_TYPE: TDataSource
    DataSet = sp_Filter_items_PLACE_DEVICE_TYPE
    Left = 192
    Top = 525
  end
  object sp_Filter_items_logical_status_sel: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ui_explorer.sp_Filter_items_Logical_status_SEL'
    Parameters = <>
    Prepared = True
    Left = 192
    Top = 608
  end
  object ds_sp_Filter_items_logical_status_sel: TDataSource
    DataSet = sp_Filter_items_logical_status_sel
    Left = 192
    Top = 661
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=server1'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 384
    Top = 504
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredProps.Strings = (
      'cb_Logical_Status.Checked'
      'cb_PLACE_DEVICE_TYPE.Checked')
    StoredValues = <>
    Left = 400
    Top = 648
  end
  object q_Filter: TADOQuery
    Parameters = <>
    Left = 40
    Top = 472
  end
end
