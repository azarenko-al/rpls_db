unit dm_act_Property;

interface
{$I ver.inc}


uses
  Classes, Forms, Menus, ActnList,  SysUtils,

  u_run,
  u_vars,
  u_Ini_Property_LOS_Params,

  i_Audit,
  d_Filter,

  dm_User_Security,

//  RPLS_search_TLB,
//  i_search_shared,

  d_Import_KML,


  d_Property_GeoCoding,

  dm_Main,


  u_shell_var,

  dm_act_Base,

  dm_MapEngine_store,

 // d_Audit,

 // i_Act_Property,

//  dm_Act_Repeater,  //dm_Act_LinkEnd
//  dm_Act_LinkEnd,  //dm_Act_LinkEnd
//  dm_Act_MSC,   // dm_Act_MSC
//  dm_Act_BSC,   //  dm_Act_BSC ,

//  dm_Act_Pmp_Site,  //  dm_Act_Pmp_Site
//  dm_Act_PmpTerminal,  //dm_Act_PmpTerminal;


  

  u_DataExport_run,
  u_DataExport_RPLS_XML_run,

  dm_Onega_DB_data,

  d_GroupAssign,

  I_Object,
  
//  I_Property,

  u_Property_run,

  

 // I_Site,

  d_Object_dependence,

  u_Geo,

  u_func,
  u_dlg,


  u_const_db,
  u_const_str,

  u_Types,

  d_Property_Move,
  d_Property_add,

  fr_Property_view,

  dm_Folder,
  dm_Property,

 // dm_Property_export,
  dm_Property_tools

  ;

type
//IMapObjectHandlerX,

  TdmAct_Property = class(TdmAct_Base) //,  IAct_PropertyX)
    act_Move: TAction;
    act_Site_Add1: TAction;
    act_LinkEnd_Add: TAction;
    act_Add_coord: TAction;
    act_Refresh_MP_Height: TAction;

    act_Refresh_GeoRegion: TAction;
    act_Make_Maps: TAction;
    act_Export_RPLS_XML: TAction;
    act_Export_RPLS_XML_folder: TAction;
    act_Object_dependence: TAction;
    act_Print_map: TAction;
    act_Import_RPLS_XML: TAction;
    ActionList2: TActionList;
    act_Add_: TAction;
    act_pmp_Terminal_Add: TAction;
    act_Export_Google: TAction;
    act_Export_MDB: TAction;
    act_pmp_Site_Add: TAction;
    act_MSC_Add: TAction;
    act_BSC_Add: TAction;
    act_Repeater_Add: TAction;
    act_Move_By_GeoRegions: TAction;
    act_Import_KML: TAction;
    act_Geocoding_Address: TAction;
    act_Apply_template: TAction;
    act_Audit: TAction;
    act_Refresh_MP_Height_folder: TAction;
    act_Filter: TAction;
    act_Property_LOS: TAction;
 
    procedure act_FilterExecute(Sender: TObject);
  //  procedure DataModuleDestroy(Sender: TObject);
//    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FBLPoint: TBLPoint;

    function CheckActionsEnable: Boolean;


//    procedure Dlg_template(aList: TIDList);

    procedure DoAction (Sender: TObject);
//    procedure DoOnDeleteList(Sender : TObject);
    procedure Import_KML;
    procedure Property_LOS(aID: integer);
//    procedure Import_KML_test;

  protected
    function GetExplorerPath(aID: integer; aGUID: string): string;

    function  GetViewForm  (aOwnerForm: TForm): TForm; override;
    procedure GetPopupMenu(aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;

// TODO: GetMessage
//  procedure GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean); override;

    function ItemDel(aID: integer; aName: string = ''): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;

    function ViewInProjectTree(aID: integer): TStrArray; override;

    procedure ShowOnMap (aID: integer);
  public
    procedure Add (aFolderID: integer);
    function  AddByPos (aFolderID: integer; aBLPoint: TBLPoint): integer;

    procedure Dlg_Move(aID: integer);

    procedure AddFromMap(aBLPoint: TBLPoint);

    class procedure Init;
    procedure LOS_FromMap(aBLPoint: TBLPoint);
  end;

var
  dmAct_Property: TdmAct_Property;


//===============================================================
//implementation
//===============================================================
implementation
{$R *.dfm}

uses
  dm_Act_LinkEnd,
  dm_act_MSC,
  dm_act_BSC,
  dm_Act_Repeater,
  dm_Act_PmpTerminal,
  dm_act_PMP_site;



//--------------------------------------------------------------------
class procedure TdmAct_Property.Init;
//--------------------------------------------------------------------
begin
  Assert(not Assigned(dmAct_Property));

  dmAct_Property:=TdmAct_Property.Create(Application);

//  dmAct_Property.GetInterface(IAct_PropertyX, IAct_Property);
//  Assert(Assigned(IAct_Property));
end;

//-------------------------------------------------------------------
procedure TdmAct_Property.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;


  ObjectName:=OBJ_PROPERTY;

////  act_Undelete.Visible:= false;

  act_Move.Caption:=STR_MOVE;

 // act_Site_Add.Caption:=STR_SITE;
//  act_Move.Caption:=STR_MOVE;

  act_Apply_template.Caption:='��������� ������';


  act_Import_RPLS_XML.Caption:='������ �� RPLS XML';

  act_Export_Google.Caption:='������� � Google';

  act_Export_RPLS_XML.Caption:='������� � RPLS XML';
  act_Export_RPLS_XML_folder.Caption:='������� � RPLS XML';
  act_Object_dependence.Caption:=S_OBJECT_DEPENDENCE;

  act_LinkEnd_Add.Caption:=STR_LINKEND;
  act_Pmp_Terminal_Add.Caption:='PMP Terminal';
  act_Pmp_Site_Add.Caption  :='PMP Site';
  act_repeater_Add.Caption := '������������';

  act_Add.Caption :='C������ ��������';

  act_Add_.Caption :='C������ ��������';


  act_Move_By_GeoRegions.Caption :='������������ �� �����������';

  act_Refresh_MP_Height_folder.Caption :='�������� ������ ����� (� �����)';


  act_Export_MDB.Caption:=DEF_STR_Export_MDB;
 // act_Export_RPLS_XML1.Caption:=DEF_STR_Export_RPLS_XML;

  act_MSC_Add.Caption :='MSC';
  act_BSC_Add.Caption :='BSC';

  act_Import_KML.Caption :='������ �� ������� Google(kml)';

  act_Geocoding_Address.Caption :='��������������';

  act_Audit.Caption :=str_Audit;
  act_Filter.Caption :='������...';

  act_Property_LOS.Caption :='������ ��������� �� ��������';

{
  act_Audit.Enabled:=False;
}

//  OnDeleteList:=DoOnDeleteList;




  SetActionsExecuteProc (
    [
   //  act_Add_mast,
     act_Apply_template,

     act_Geocoding_Address,

     act_Import_KML,

     act_Add_,
     act_Export_MDB,

     act_Object_ShowOnMap,
     act_Move,
     act_Refresh_MP_Height,
     act_Refresh_MP_Height_folder,

//     act_Site_Add,
     act_LinkEnd_Add,
     act_Pmp_Terminal_Add,
     act_Pmp_Site_Add,
     act_MSC_Add,
     act_BSC_Add,
     act_repeater_Add,

     act_GroupAssign,

     act_Print_map,

     act_Export_Google,

     act_Export_RPLS_XML,
     act_Export_RPLS_XML_folder,
//     act_Export_RPLS_XML1,

     act_Import_RPLS_XML,

     act_Refresh_GeoRegion,
     act_Object_dependence,

     act_Audit,
     act_Move_By_GeoRegions ,

     act_Filter,
     act_Property_LOS

     ],
     DoAction);



  SetCheckedActionsArr([
//       act_Add_mast,

       act_Apply_template,

       act_Geocoding_Address,

       act_Add,
       act_Del_,
       act_Del_list,

       act_LinkEnd_Add,
       act_Pmp_Terminal_Add,
       act_Pmp_Site_Add,
       act_MSC_Add,
       act_BSC_Add,
       act_repeater_Add,

       act_Move,
       act_Refresh_MP_Height,

       act_Refresh_GeoRegion,
       act_Import_RPLS_XML,

       act_Move_By_GeoRegions ,

       act_Filter

    //   act_GroupAssign

     ]);


 // act_Add.Enabled := not dmUser_Security.ProjectIsReadOnly;



end;

procedure TdmAct_Property.act_FilterExecute(Sender: TObject);
begin

end;


//--------------------------------------------------------------------
procedure TdmAct_Property.ShowOnMap (aID: integer);
//--------------------------------------------------------------------
begin
  ShowPointOnMap (dmProperty.GetPos (aID), aID);
 // HighLightObjectOnMap (aID);
end;


//--------------------------------------------------------------------
procedure TdmAct_Property.Property_LOS(aID: integer);
//--------------------------------------------------------------------
const
  TEMP_INI_FILE = 'Property_LOS.ini';

var
  oIni: TIni_Property_LOS_Params;
  sFile: string;

begin
  sFile := g_ApplicationDataDir + TEMP_INI_FILE;


  oIni:=TIni_Property_LOS_Params.create;
  oIni.Project_ID:=dmMain.ProjectID;
  oIni.Property_ID:=aID;

  oIni.SaveToFile(sFile);

  FreeAndNil(oIni);

  RunApp (GetApplicationDir() + 'property_LOS.exe',  DoubleQuotedStr(sFile) );

 // HighLightObjectOnMap (aID);
end;


procedure TdmAct_Property.LOS_FromMap(aBLPoint: TBLPoint);
var
  iID: Integer;
  bl: TBLPoint;
  sName: string;
begin
  iID:=dmProperty.GetNearestIDandPos(aBLPoint, bl, sName );
  if iID>0 then
     Property_LOS (iID);

end;
           



//--------------------------------------------------------------------
procedure TdmAct_Property.Import_KML;
//--------------------------------------------------------------------
begin
  Tdlg_Import_KML.ExecDlg(dmMain.ProjectID);

end;

//--------------------------------------------------------------------
procedure TdmAct_Property.DoAction (Sender: TObject);
//--------------------------------------------------------------------
var
    i: Integer;
    S: string;
  sName: string;
begin
  if Sender=act_Filter then
     Tdlg_Filter.ExecDlg (FFocusedGUID)
  else


  if Sender=act_Property_LOS then
     Property_LOS( FFocusedID) else



{
 if Sender=act_Audit then
     dlg_Audit (TBL_Property, FFocusedID) else
}
  {

  //---------------------------------
  if Sender = act_Add_mast then begin
  //---------------------------------

    sName:='�����';

    if InputQuery ('�����','�������� �����',sName) then
    begin

      dmOnega_DB_data.OpenQuery_(dmOnega_DB_data.ADOQuery1, 'INSERT INTO '+ TBL_mast +
              ' (name,Property_id) values (:name,:Property_id) ',
              [ FLD_name,  sName,
                FLD_Property_id, FFocusedID
              ]);
    end;

  end;
  }

  if Sender=act_Audit then
     Init_IAuditX.ExecDlg( dmMain.GetConnectionString, TBL_PROPERTY, FFocusedID) else


//  if Sender = act_Apply_template then
//    Dlg_template(FSelectedIDList) else

//procedure TdmAct_Property.Dlg_template(aProperty_ID: Integer);



  if Sender=act_Geocoding_Address then
  begin
    Tdlg_Property_GeoCoding.ExecDlg (FSelectedIDList);
  end else
//    TDataExport_run.ExportToRplsDb_selected(TBL_Property, FSelectedIDList) else



  if Sender=act_Import_KML then
    Import_KML() else


  if Sender=act_Move_By_GeoRegions then
  begin
    dmProperty_tools.Move_By_GeoRegions(FSelectedIDList);

    if Assigned(IMainProjectExplorer) then
    begin
      IMainProjectExplorer.UPDATE_NODE_CHILDREN(GUID_PROPERTY);
      IMainProjectExplorer.ExpandByGUID(GUID_PROPERTY);
    end;
  end else

   // TDataExport_run.ExportToRplsDb_selected(TBL_Property, FSelectedIDList) else


  //------------------------------------------------------
  if Sender=act_Export_RPLS_XML then
//    dmProperty_export.ExportToRPLS_XML ('');//, FSelectedIDList);
    TDataExport_RPLS_XML_run.Export_Property_selected(FSelectedIDList) else
//    dmProperty_export.Dlg_ExportToRPLS_XML (FSelectedIDList);


  //------------------------------------------------------
  if Sender=act_Export_RPLS_XML_folder then
//    dmProperty_export.ExportToRPLS_XML ('');//, FSelectedIDList);
    TDataExport_RPLS_XML_run.Export_Property_selected (nil, FFocusedID) else


  if Sender=act_Import_RPLS_XML then
    TDataExport_RPLS_XML_run.Import_Property else
//    dmProperty_export.Dlg_ImportFrom_RPLS_SITES_XML else

  if Sender=act_Export_MDB then
    TDataExport_run.ExportToRplsDb_selected(TBL_Property, FSelectedIDList) else


  if Sender = act_Object_dependence then
    Tdlg_Object_dependence.ExecDlg(OBJ_Property, FFocusedName, FFocusedID) else

  //------------------------------------------------------
  if Sender=act_Move then
    Dlg_Move(FFocusedID) else


  if Sender=act_GroupAssign then
    Tdlg_GroupAssign.ExecDlg (OBJ_PROPERTY, FSelectedIDList) else


  if Sender=act_Export_Google then
//    dmProperty_export.ExportToRPLS_XML ('');//, FSelectedIDList);
    TProperty_run.Export_Google (FSelectedIDList) else



  //------------------------------------------------------
  if Sender=act_LinkEnd_Add then
  begin
  //  if Assigned(ILinkEnd) then
    dmAct_LinkEnd.AddBYProperty (FFocusedID);

     // dmAct_LinkEnd.AddBYProperty (FFocusedID)
  end else

  if Sender=act_MSC_Add then
  begin
  //  if Assigned(ILinkEnd) then
     // dmAct_MSC.AddBYProperty (FFocusedID)
    dmAct_MSC.AddBYProperty (FFocusedID);
  end else

  if Sender=act_BSC_Add then
  begin
  //  if Assigned(ILinkEnd) then
    dmAct_BSC.AddBYProperty (FFocusedID);
     // dmAct_BSC.AddBYProperty (FFocusedID)
  end else



  //------------------------------------------------------
  if Sender=act_Pmp_Terminal_Add then
  begin
  //  if Assigned(ILinkEnd) then
     // dmAct_PmpTerminal.AddBYProperty (FFocusedID)
    dmAct_PmpTerminal.AddBYProperty (FFocusedID)
  end else

  //------------------------------------------------------
  if Sender=act_Repeater_Add then
  begin
  //  if Assigned(ILinkEnd) then
     // dmAct_PmpTerminal.AddBYProperty (FFocusedID)
    dmAct_Link_Repeater.AddBYProperty (FFocusedID)
  end else


  //------------------------------------------------------
  if Sender=act_Pmp_Site_Add then
  begin
  //  if Assigned(ILinkEnd) then
//    dmAct_Pmp_Site.AddBYProperty (FFocusedID)
    dmAct_Pmp_Site.AddBYProperty (FFocusedID)
  end else


  //------------------------------------------------------
  if Sender=act_Object_ShowOnMap then
    ShowOnMap (FFocusedID) else





  if Sender=act_Refresh_MP_Height_folder then
    dmProperty_tools.UpdateGroundHeight_ByFolder (FFocusedID)
  else

  //------------------------------------------------------
  if Sender=act_Refresh_MP_Height then
    dmProperty_tools.UpdateGroundHeight_List (FSelectedIDList, FFocusedID)
  else

  if Sender=act_Refresh_GeoRegion then
    dmProperty_tools.UpdateGeoRegionForProperties
  else
    raise Exception.Create('');
  ;
end;

//--------------------------------------------------------------------
function TdmAct_Property.AddByPos (aFolderID: integer; aBLPoint: TBLPoint): integer;
//--------------------------------------------------------------------
begin
 // Result:=Tdlg_Property_add.ExecDlg (aFolderID, aBLPoint);
  FBLPoint:=aBLPoint;
  inherited Add (aFolderID);
end;

//--------------------------------------------------------------------
procedure TdmAct_Property.Add (aFolderID: integer);
//--------------------------------------------------------------------
begin
//  Result:=Tdlg_Property_add.ExecDlg (aFolderID, NULL_POINT);

  FBLPoint:=NULL_BLPOINT;
  inherited Add (aFolderID);
end;

// ---------------------------------------------------------------
procedure TdmAct_Property.AddFromMap(aBLPoint: TBLPoint);
begin
  Tdlg_Property_add.ExecDlg (0, aBLPoint);
end;


//--------------------------------------------------------------------
function TdmAct_Property.ItemDel(aID: integer; aName: string = ''): boolean;
//--------------------------------------------------------------------
var
  bHas_children: boolean;
begin
  Result:= false;

  bHas_children := dmOnega_DB_data.Object_Has_children (OBJ_property, aID);


 // bHas_children := dmProperty.IsEmpty(aID);

  if not bHas_children then
    Result:= dmProperty.Del(aID)

  else

//  if ConfirmDlg('������ � ��������� ����� ������� ��� ����������� �� ��� ��������. ������������� ��������? '+ FFocusedName)
//  then

  Result:= dmProperty_tools.Del(aID, FDeletedIDList);


  if Result then
  begin
    dmMapEngine_store.Feature_Del (OBJ_Property, aID);

///    PostEvent (WE_MAP_ENGINE_DEL_OBJECT, Integer(otCalcRegion), aID);

  //  dmAct_Map.MapRefresh;
  end;

end;

{
procedure TdmAct_Property.DoOnDeleteList(Sender : TObject);
var
  I: Integer;
begin
//  PostUserMessage (WM__DEL_MAP, Integer(FDeletedIDList));

//   ShowMessage('DoOnDeleteList');
end;

   }

//--------------------------------------------------------------------
function TdmAct_Property.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_Property_add.ExecDlg (aFolderID, FBLPoint);

end;


//--------------------------------------------------------------------
function TdmAct_Property.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_Property_View.Create(aOwnerForm);
end;



// ---------------------------------------------------------------
function TdmAct_Property.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
  Result:= dmUser_Security.Modify_project in [mtAllow, mtAllowMy];
//  Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];

  SetActionsEnabled1( [
          act_Del_,
          act_Del_list
//          act_Copy
        //  act_Export_MDB,

      ],

   Result );


  //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );


end;

//--------------------------------------------------------------------
procedure TdmAct_Property.GetPopupMenu(aPopupMenu: TPopupMenu; aMenuType:
    TMenuType);
//--------------------------------------------------------------------
var
 // bReadOnly: Boolean;
  oMenuItem: TMenuItem;

  bEnable: Boolean;

begin
//  bEnable:=CheckActionsEnable();
  bEnable:=True;

 // CheckActionsEnable();

 // bReadOnly := dmUser_Security.ProjectIsReadOnly;

  case aMenuType of
    mtFolder: begin
          //      AddMenuItem (aPopupMenu, act_Add_Item);

                AddFolderMenu_Create (aPopupMenu, bEnable);
                AddFolderPopupMenu  (aPopupMenu, bEnable); //, not bReadOnly);

                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Refresh_MP_Height_folder);
                AddMenuItem (aPopupMenu, act_Refresh_GeoRegion);

                AddMenuItem (aPopupMenu, act_Export_RPLS_XML_folder);
                AddMenuItem (aPopupMenu, act_Import_RPLS_XML);

                AddMenuItem (aPopupMenu, act_Import_KML);

                AddMenuItem (aPopupMenu, nil);

                AddMenuItem (aPopupMenu, act_Filter);


    //            AddMenuItem (aPopupMenu, act_Undelete);
              end;

    mtItem:  begin
                oMenuItem:=dlg_AddMenuItem (aPopupMenu, nil, '�������');

       //         {$IFDEF gsm}
//                dlg_AddMenuItem (oMenuItem, act_Site_Add);
         //       {$ENDIF}

//                dlg_AddMenuItem (oMenuItem, act_LinkEnd_Add);
                dlg_AddMenuItem (oMenuItem, act_LinkEnd_Add);
                dlg_AddMenuItem (oMenuItem, act_pmp_Terminal_Add);
                dlg_AddMenuItem (oMenuItem, act_pmp_Site_Add);

                dlg_AddMenuItem (oMenuItem, act_MSC_Add);
                dlg_AddMenuItem (oMenuItem, act_BSC_Add);
                dlg_AddMenuItem (oMenuItem, act_repeater_Add);


                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Move);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Object_ShowOnMap);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Del_);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Refresh_MP_Height);

                AddMenuItem (aPopupMenu, act_Object_dependence);

                AddMenuItem (aPopupMenu, nil);
                AddFolderMenu_Tools (aPopupMenu); //, not bReadOnly);

                AddMenuItem (aPopupMenu, nil);
                  oMenuItem := dlg_AddMenuItem (aPopupMenu, nil, '�������');
                  AddMenuItem (oMenuItem, act_Export_MDB);
                  AddMenuItem (oMenuItem, act_Export_RPLS_XML);
                  AddMenuItem (oMenuItem, act_Export_Google);


                AddMenuItem (aPopupMenu, act_Move_By_GeoRegions);
                AddMenuItem (aPopupMenu, act_GroupAssign);

                AddMenuItem (aPopupMenu, act_Geocoding_Address);

                AddMenuItem (aPopupMenu, nil);
             //   AddMenuItem (aPopupMenu, act_Apply_template);
                AddMenuItem (aPopupMenu, act_Audit);
                AddMenuItem (aPopupMenu, act_Property_LOS);


//                AddMenuItem (aPopupMenu, act_Add_mast);
                                                 

(*
                AddMenuItem (oMenuItem, act_Export_Google);
                AddMenuItem (oMenuItem, act_Export_MDB);
                AddMenuItem (oMenuItem, act_Export_RPLS_XML);*)
             //   AddMenuItem (aPopupMenu, act_Print_map);

            end;

    mtList: begin
                AddMenuItem (aPopupMenu, act_Del_list);
                AddMenuItem (aPopupMenu, act_GroupAssign);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Refresh_MP_Height);
                AddFolderMenu_Tools (aPopupMenu);
                
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Export_MDB);
                AddMenuItem (aPopupMenu, act_Export_RPLS_XML);
                AddMenuItem (aPopupMenu, act_Export_Google);

                AddMenuItem (aPopupMenu, act_Move_By_GeoRegions);
                AddMenuItem (aPopupMenu, act_Geocoding_Address);

           //     AddMenuItem (aPopupMenu, act_Apply_template);


            end;
  end;
end;


//--------------------------------------------------------------------
procedure TdmAct_Property.Dlg_Move(aID: integer);
//--------------------------------------------------------------------
begin
  Tdlg_Property_Move.ExecDlg (aID);
end;


//--------------------------------------------------------------------
function TdmAct_Property.ViewInProjectTree(aID: integer): TStrArray;
//--------------------------------------------------------------------
var sFolderGUID: string;
  iFolderID: Integer;
begin

  if Assigned(IMainProjectExplorer) then
  begin
(*    IMainProjectExplorer.ExpandByGUID(GUID_PROJECT);
    IMainProjectExplorer.ExpandByGUID(GUID_PROPERTY);
*)

    iFolderID:=dmPROPERTY.GetFolderID(aID);

    if iFolderID=0 then
      sFolderGUID:=g_Obj.ItemByName[ObjectName].RootFolderGUID
    else
      sFolderGUID:=dmFolder.GetGUIDByID(iFolderID);


     Result := StrArray([GUID_PROJECT,
                         GUID_PROPERTY,
                         sFolderGUID]);


//    iFolderID:=dmPROPERTY.GetFolderID(aID);
 //   sFolderGUID:=dmFolder.GetGUIDByID (iFolderID);

  //  IMainProjectExplorer.ExpandByGUID(sFolderGUID);
 //    IMainProjectExplorer.FocuseNodeByGUID (aGUID);

   // IMainProjectExplorer.FocuseNodeByObjName(ObjectName, aID);

  end;

end;


//--------------------------------------------------------------------
function TdmAct_Property.GetExplorerPath(aID: integer; aGUID: string): string;
//--------------------------------------------------------------------
var sFolderGUID: string;
begin
  sFolderGUID:=dmFolder.GetGUIDByID (dmPROPERTY.GetFolderID(aID));

  Result := GUID_PROJECT  + CRLF +
            GUID_PROPERTY + CRLF +
            sFolderGUID   + CRLF +
            aGUID;
end;




end.

{




//--------------------------------------------------------------------
procedure TdmAct_Property.Dlg_template(aList: TIDList);
//--------------------------------------------------------------------
var
  i: Integer;
 // iLinkRepeater: Integer;
  iTemplate_Site_ID: Integer;
  sName: Widestring;
begin
  //Assert(Assigned(IShell));
//  Assert(Assigned(ILinkEndType));
      //FSelectedIDList
  if dmAct_Explorer.Dlg_Select_Object(otTemplate_Site, iTemplate_Site_ID, sName) then

  for i:=0 to aList.Count-1 do
  begin
    dmOnega_DB_data.ExecStoredProc_(sp_Property_Apply_template,
         [
           FLD_Property_ID,      aList[i].ID,
           FLD_template_site_ID, iTemplate_Site_ID
         ]);
  end;

end; 


// ---------------------------------------------------------------
procedure Tdlg_Property_add.row_TemplateEditPropertiesButtonClick( Sender: TObject; AButtonIndex: Integer);
// ---------------------------------------------------------------
begin
  if AButtonIndex=0 then
    if cxVerticalGrid1.FocusedRow=row_Template_site then
      Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otTemplate_Site, Frec.Template_site_id);

//    Dlg_Apply_template;

  if AButtonIndex=1 then
  begin
    Frec.Template_site_id:=0;
    row_Template_site.Properties.Value:='';
  end;

end;



// TODO: GetMessage
////--------------------------------------------------------------------
//procedure TdmAct_Property.GetMessage(aMsg: integer; aParams: TEventParamList; var aHandled: Boolean);
////--------------------------------------------------------------------
//begin
//inherited;
////
////  case aMsg of
////    WE_OBJECT_GET_ALWAYS_ENABLED_ACTIONS:
////    begin
////      if aParams.ValueByName(PAR_OBJECT_NAME) <> OBJ_PROPERTY then
////        exit;
////
////      FAlwaysEnabledActionList:= TStringList (aParams.PointerByName(PAR_NAME));
////
////      AddActionNamesToList([act_Object_ShowOnMap
////                           ], FAlwaysEnabledActionList);
////
////    end;
////  end;
//end;


