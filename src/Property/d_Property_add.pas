unit d_Property_add;

interface
{$I ver.inc}


uses

//  I_Shell_new,


  fr_geo_Coordinates,
  u_geo_convert_2017,

  d_Wizard_Add_with_params,

  i_GeoCoding,

  u_cx_vgrid,
  
  u_geo_convert_new,

  dm_MapEngine_store,

  u_Property_Add,

  dm_Main,

  I_rel_Matrix1,

  I_Options_,

  dm_GeoRegion,
  dm_Property,
  dm_Rel_Engine,

//  dm_UserAuth,

  u_geo,
  u_func,

  u_dlg,

//  u_cx_VGrid,

  u_types,

  u_const_str,

  SysUtils, Classes, Controls, Forms,  StdCtrls, ActnList, ExtCtrls,
  cxVGrid,  cxButtonEdit, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxEdit, cxTextEdit, cxDropDownEdit,
  cxInplaceContainer, cxPropertiesStore, rxPlacemnt, ComCtrls, dxSkinsCore,
  dxSkinsDefaultPainters ;

type
  Tdlg_Property_add = class(Tdlg_Wizard_add_with_params)
    GroupBox1: TGroupBox;
    Timer1: TTimer;
    cxVerticalGrid1: TcxVerticalGrid;
    row_GeoRegion1: TcxEditorRow;
    row_City: TcxEditorRow;
    row_Address: TcxEditorRow;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;
    row_Relief_Height1: TcxEditorRow;
    row_MP_Type1: TcxEditorRow;
    row_MP_Height1: TcxEditorRow;
    cxVerticalGrid1EditorRow9: TcxEditorRow;
    cxVerticalGrid1EditorRow10: TcxEditorRow;
    row_Template_site: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    Button1: TButton;
    Button2: TButton;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure act_OkUpdate(Sender: TObject);
//    procedure Button2Click(Sender: TObject);


    procedure DoOnButtonClick(Sender: TObject; AbsoluteIndex: Integer);
  //  procedure FormActivate(Sender: TObject);
    procedure row_Folder1EditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure Timer1Timer(Sender: TObject);
    procedure row_TemplateEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
//    procedure Button2Click(Sender: TObject);
//    procedure Button1Click(Sender: TObject);

  private
    FRec: TdmPropertyAddRec1;
    FID: integer;
//    FTemplate_Site_ID: integer;

//    FGeoRegionID: integer;
   // FBLPoint: TBLPoint;

    FRelRec:  Trel_Record;


    procedure Append;
    procedure UpdateGroundHeight();
  //  procedure ValidateGeoRegion;

    procedure DoOnPosChange(Sender: TObject);

  protected
    Fframe_geo_Coordinates: Tframe_geo_Coordinates;
    procedure PosChanged;

  public
    class function ExecDlg(aFolderID: integer; aBLPoint_CK42: TBLPoint): integer;

  end;


//==================================================================
implementation

 {$R *.DFM}



//--------------------------------------------------------------------
class function Tdlg_Property_add.ExecDlg(aFolderID: integer; aBLPoint_CK42:
    TBLPoint): integer;
//--------------------------------------------------------------------
var
  bl_WGS  : TBLPoint;
begin
  {
  if dmUser_Security.ProjectIsReadOnly then
  begin
    ShowMessage('���������� �������� ������. ������ ������ ��� ������.');
    Result:=0;
    Exit;
  end;
  }

  with Tdlg_Property_add.Create(Application) do
  begin
    FRec.Folder_ID:=aFolderID;
//    FBLPoint :=aBLPoint_CK42;

    ed_Name_.Text   :=dmProperty.GetNewName();
//    row_Folder.Text:=dmFolder.GetPath (FRec.FolderID);
 //   row_Folder1.Properties.Value:=

    if (aBLPoint_CK42.B=0) and (aBLPoint_CK42.B=0) then
//      Fframe_geo_Coordinates.Set_BLPoint_and_CoordSys (MakeBLPoint(0,0), EK_WGS84)
       //dmFolder.GetPath (FRec.FolderID);

      //  dmOnega_DB_data.Folder_GetFullPath(FRec.Folder_ID);

//    row_Pos.Text   :=geo_FormatDegreeWithCoordSys(aBLPoint_CK42, dmOptions.GetGeoCoord);

//    UpdateGroundHeight();
 /////   Assert(Assigned(IOptions));
      Fframe_geo_Coordinates.Set_BLPoint_and_CoordSys (MakeBLPoint(0,0), IOptions.Get_CoordSys())

    else begin
      bl_WGS:=geo_BL_to_BL_2017_ (aBLPoint_CK42, EK_CK_42, IOptions.Get_CoordSys());
      Fframe_geo_Coordinates.Set_BLPoint_and_CoordSys (bl_WGS, IOptions.Get_CoordSys());

//      bl_WGS:=geo_BL_to_BL_2017_ (aBLPoint_CK42, EK_CK_42, EK_WGS84);
//      Fframe_geo_Coordinates.Set_BLPoint_and_CoordSys (bl_WGS, EK_WGS84);

    end;

//    Fframe_geo_Coordinates.BLPoint_Pulkovo:=aBLPoint_CK42;
//    Fframe_geo_Coordinates.DisplayCoordSys:=IOptions.Get_CoordSys;

    FRec.BLPoint_Pulkovo:=aBLPoint_CK42;

    if not geo_Eq(FRec.BLPoint_Pulkovo, NULL_BLPOINT) then
      PosChanged;
      //Timer1.Enabled:=True;



//    PostMessage (WM);

{ TODO : PostMessage (WM); }
{    if not geo_Eq(aBLPoint_CK42, NULL_POINT) then
    begin
      UpdateGroundHeight ();

      FGeoRegionID      := dmGeoRegion.GetID_ByBLPoint(aBLPoint_CK42);
      row_GeoRegion.Text:= dmGeoRegion.GetNameByID(FGeoRegionID);
    end;
}

 //   Perform(WM_FORM_CREATED, 0,0);

 //   PostMessage (Handle, WM_FORM_CREATED, 0,0);




    if (ShowModal=mrOk) then
      Result:=FID
    else
      Result:=0;

    Free;
  end;
end;

//--------------------------------------------------------------------
procedure Tdlg_Property_add.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
var
  iIndex: integer;

begin
  inherited;

//  Assert(Assigned(IMapEngine));

   GroupBox1.Height := 90;


  Caption:=DLG_CAPTION_OBJECT_CREATE;
  SetActionName(STR_DLG_ADD_PROP);


  row_Template_site.Properties.Caption  :=STR_TEMPLATE_SITE;

  //------------------------------------------------------

 // row_Folder1.Properties.Caption       :=STR_FOLDER;
  row_City.Properties.Caption         :=STR_TOWN;
  row_Address.Properties.Caption      :=STR_ADDRESS;

  row_Relief_Height1.Properties.Caption:=STR_RELIEF_HEIGHT;
  row_MP_height1.Properties.Caption    :=STR_MP_HEIGHT;
  row_MP_type1.Properties.Caption      :=STR_MP_TYPE;

  SetDefaultWidth();


  CreateChildForm(Tframe_geo_Coordinates, Fframe_geo_Coordinates, GroupBox1);
  Fframe_geo_Coordinates.OnPosChange:=DoOnPosChange;

  cx_InitVerticalGrid (cxVerticalGrid1);

  Height:=600;


{
  with TRegIniFile.Create(FRegPath) do
  begin
    //////////
//    FRec.Template_site_id:= ReadInteger('', FLD_Template_site_id, 0);

    Free;
  end;
 }

//  cx_VerticalGrid_LoadFromReg(cxVerticalGrid1, FRegPath);


  //----------------
//  row_Template_site.Properties.Value:= gl_DB.GetNameByID(TBL_Template_site, FRec.Template_site_id);
                       

  dmRel_Engine.Open;

end;


// ---------------------------------------------------------------
procedure Tdlg_Property_add.FormDestroy(Sender: TObject);
// ---------------------------------------------------------------
begin
  FreeAndNil(Fframe_geo_Coordinates);

  {

  with TRegIniFile.Create(FRegPath) do
  begin
    //////////
//     WriteInteger('', FLD_Template_site_id, FRec.Template_site_id);

    Free;
  end;
  }

  inherited;
end;


//--------------------------------------------------------------------
procedure Tdlg_Property_add.act_OkExecute(Sender: TObject);
begin
  Append;
end;


procedure Tdlg_Property_add.act_OkUpdate(Sender: TObject);
begin
  with Fframe_geo_Coordinates.GetBLPoint_Pulkovo do
    act_Ok.Enabled:= not ((B = 0) and
                          (L = 0));
end;


//-------------------------------------------------------------------
procedure Tdlg_Property_add.DoOnButtonClick(Sender: TObject; AbsoluteIndex: Integer);
//-------------------------------------------------------------------
begin

{  // ---------------------------------------------------------------
  if Sender=row_GeoRegion then
  // ---------------------------------------------------------------
  begin
    if dlg_SelectObject_dx (row_GeoRegion, otGeoRegion, FRec.GeoRegionID) then
      if not dmUserAuth.IsAccessAllowed_for_GeoRegion(FRec.GeoRegionID) then
      begin
        ErrDlg (MSG_NOT_ENOUGH_RIGHTS);

        row_GeoRegion.Text:='';
        FRec.GeoRegionID:=0;
      end;
  end  else


  // ---------------------------------------------------------------
  if Sender=row_Folder then
    Dlg_SelectObjectFolder_dx (row_Folder, otProperty, FRec.FolderID);
}
end;

//-------------------------------------------------------------------
procedure Tdlg_Property_add.UpdateGroundHeight();
//-------------------------------------------------------------------
var
  b: Boolean;
  I: Integer;
  iH: integer;
  blPoint: TBLPoint;
  BLPoint_WGS: TBLPoint;
  obj: TComClient_GeoCoding;
  sAddress: Widestring;

  sCluName: string;
begin


//  dmRel_Engine.OpenByPoint (FRec.BLPoint_Pulkovo) ;



  b:=dmRel_Engine.GetRelInfo_(FRec.BLPoint_Pulkovo, FRelRec);



  sCluName:=dmRel_Engine.GetClutterNameByCode(FRelRec.Clutter_Code);

//
//  row_Relief_Height.Text:= AsString(FRelRec.Rel_H);
//  row_MP_height.Text    := AsString(FRelRec.Loc_H);
//  row_MP_type.Text      := sCluName;
//


  row_Relief_Height1.Properties.Value:= FRelRec.Rel_H;
  row_MP_height1.Properties.Value    := FRelRec.Clutter_H;
  row_MP_type1.Properties.Value      := sCluName;



  FRec.Ground_Height :=FRelRec.Rel_H;
  FRec.CLUTTER_HEIGHT:=FRelRec.Clutter_H;
  FRec.CLUTTER_NAME  :=sCluName;




  FRec.GeoRegionID  := dmGeoRegion.GetID_ByBLPoint(FRec.BLPoint_Pulkovo);


/////////  Exit;

 // row_GeoRegion.Text:= dmGeoRegion.GetNameByID(FRec.GeoRegionID);
  row_GeoRegion1.Properties.Value:= dmGeoRegion.GetNameByID(FRec.GeoRegionID);

  // -----------------------------------------
  obj:=TComClient_GeoCoding.Create;

  BLPoint_WGS:=geo_Pulkovo42_to_WGS84(FRec.BLPoint_Pulkovo);
  obj.Intf.Get_Address_By_Lat_Lon_WGS(BLPoint_WGS.B, BLPoint_WGS.L, sAddress);

  row_Address.Properties.Value:=sAddress;
                                                            
  FreeAndNil(obj);


//    gl_DB.GetStringFieldValue(TBL_CLUTTER_TYPE, FLD_NAME,
  //                           [db_Par(FLD_CODE, FRelRec.Loc_Code)  ]);


////  dmClutterModel.GetNameByID(FRelRec.Loc_Code);
//  AsString(FRelRec.Loc_Code);

//  dmRel_Engine.FindPointBL (blPoint, FRelRec);
//  if dmRel_Engine.FindPointBL (blPoint, FRelRec) then
//  begin
//    dmProperty.Update (FID, [db_par(FLD_GROUND_HEIGHT, rec.Rel_H)]);
//    row_MP_height.Text    :=AsString(rec.Loc_H);
//    row_MP_type.Text      :=AsString(rec.Loc_Code);
//  end;

 // dmRel_Engine.Close;
end;



// -------------------------------------------------------------------
procedure Tdlg_Property_add.Append;
// -------------------------------------------------------------------
var
  oPropertyAdd: TPropertyAddToDB;
  sFolderGUID: string;
  sGUID: string;
begin
  ed_Name_.Text:=Trim(ed_Name_.Text);

  if dmProperty.FindByName(ed_Name_.Text)>0 then
  begin
    ErrDlg ('�������� ������ ��� ��� ��������.');

    ModalResult:= mrNone;
    Exit;
  end;

(*
  if not dmUserAuth.IsAccessAllowed_for_GeoRegion(FRec.GeoRegionID) then
  begin
    ErrDlg (MSG_NOT_ENOUGH_RIGHTS);

    ModalResult:= mrNone;
    Exit;
  end;
*)

//  FillChar(rec, SIzeOf(rec), 0);

 { case row_NamesGenerateMethod.Items.IndexOf(row_NamesGenerateMethod.Text) of    //
    0: rec.Name:= ed_Name_.Text;
    1: rec.Name:= row_NetID.Text;
    2: rec.Name:= ed_Name_.Text   +' - '+ row_NetID.Text;
    3: rec.Name:= row_NetID.Text +' - '+ ed_Name_.Text;
  end;    // case

}

//  if row_NetID.Text = '' then

  FRec.Name:=ed_Name_.Text;
  FRec.Project_ID:=dmMain.ProjectID;

  FRec.BLPoint_Pulkovo:=Fframe_geo_Coordinates.GetBLPoint_Pulkovo;// FBLPoint;

{
  //rec.FolderID:=FFolderID;
//  rec.Beenet  :=row_NetID.Text;


  FRec.Ground_Height :=FRelRec.Rel_H;
  FRec.CLUTTER_HEIGHT:=FRelRec.Loc_H;
  FRec.CLUTTER_NAME  :=row_MP_type.Text;
}

////////////  FID:=dmProperty.Add (FRec);

  // ---------------------------------------------------------------

  oPropertyAdd := TPropertyAddToDB.Create();

  oPropertyAdd.Project_ID:=dmMain.ProjectID;

  oPropertyAdd.Folder_ID:=Frec.Folder_ID;
  oPropertyAdd.NewName  :=ed_Name_.Text;

  oPropertyAdd.Address:=AsString(row_Address.Properties.Value);
  oPropertyAdd.City   :=AsString(row_City.Properties.Value);

  oPropertyAdd.CoordnateKind:=ckPulkovo;

  oPropertyAdd.BLPoint_Pulkovo :=FRec.BLPoint_Pulkovo;

  oPropertyAdd.Ground_Height :=FRec.Ground_Height;
  oPropertyAdd.CLUTTER_HEIGHT:=FRec.CLUTTER_HEIGHT;
  oPropertyAdd.CLUTTER_NAME  :=FRec.CLUTTER_NAME;

  oPropertyAdd.GeoRegion_ID  := Frec.GeoRegionID;

//  oPropertyAdd.Template_site_id  := Frec.Template_site_id;


//  oPropertyAdd.Height    := AsFloat(row_Height.Properties.Value);
 // oPropertyAdd.Azimuth   := row_Azimuth.Properties.Value;
//  FAntennaAdd.PropertyID:= FPropertyID;

//  oPropertyAdd.AntennaType_ID :=FRec.AntennaTypeID;

  FID:=oPropertyAdd.Add_DB; //(dmMain.ADOConnection);

  FreeAndNil(oPropertyAdd);

  // ---------------------------------------------------------------

  if FID>0 then
  begin
  //  if assigned(IMapEngine) then

    Assert(Assigned(dmMapEngine_store), 'Value not assigned');


    dmMapEngine_store.Feature_Add(OBJ_PROPERTY, FID);

    if not dmMapEngine_store.Is_Object_Map_Exists(OBJ_PROPERTY) then
       dmMapEngine_store.Open_Object_Map(OBJ_PROPERTY);


    //if assigned(IMapAct) then
  end;

 {
//  if aFolderID=0 then
  if Frec.Folder_ID>0 then
    sFolderGUID:=dmFolder.GetGUIDByID(Frec.Folder_ID)
  else
    sFolderGUID:=g_Obj.ItemByName[OBJ_PROPERTY].RootFolderGUID;
 // else
  //  sFolderGUID:=dmFolder.GetGUIDByID(aFolderID);

  g_Shell.UpdateNodeChildren_ByGUID (sFolderGUID);

//  g_ShellEvents.Shell_EXPAND_BY_GUID(sFolderGUID);
  g_Shell.EXPAND_BY_GUID(sFolderGUID);

  g_Shell.FocuseNodeByObjName (OBJ_PROPERTY, FID);
   }


end;




procedure Tdlg_Property_add.DoOnPosChange(Sender: TObject);
begin
  Frec.BLPoint_Pulkovo:=Fframe_geo_Coordinates.GetBLPoint_Pulkovo;
  PosChanged();
end;



procedure Tdlg_Property_add.PosChanged;
begin
 // if Sender=row_Pos then begin
 //   if geo_Dlg_Coordinates (FBLPoint, dmOptions.GetGeoCoord()) then
  //  begin
  //    row_Pos.Text:=geo_FormatBLPoint(FBLPoint);
   //   row_Pos.Text:=geo_FormatDegreeWithCoordSys(FBLPoint, dmOptions.GetGeoCoord);

//  CursorWait;

  Timer1.Enabled:=True;  //delay - 2 sec

//  UpdateGroundHeight();

{  FRec.GeoRegionID  := dmGeoRegion.GetID_ByBLPoint(FBLPoint);
  row_GeoRegion.Text:= dmGeoRegion.GetNameByID(FRec.GeoRegionID);
  row_GeoRegion1.Properties.Value:= dmGeoRegion.GetNameByID(FRec.GeoRegionID);
}


//  CursorDefault;

  //  end;
 // end else}

end;


procedure Tdlg_Property_add.row_Folder1EditPropertiesButtonClick( Sender: TObject; AButtonIndex: Integer);
begin
  // ---------------------------------------------------------------
  if cxVerticalGrid1.FocusedRow =row_GeoRegion1 then
  // ---------------------------------------------------------------
  begin
    Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otGeoRegion, FRec.GeoRegionID);

(*    if Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otGeoRegion, FRec.GeoRegionID) then
      if not dmUserAuth.IsAccessAllowed_for_GeoRegion(FRec.GeoRegionID) then
      begin
        ErrDlg (MSG_NOT_ENOUGH_RIGHTS);

        row_GeoRegion1.Properties.Value:='';
        FRec.GeoRegionID:=0;
      end;
*)

  end else

 // ---------------------------------------------------------------
 // if cxVerticalGrid1.FocusedRow =row_Folder1 then
  //  Dlg_SelectObjectFolder_cxButtonEdit (Sender as TcxButtonEdit, otProperty, FRec.Folder_ID);

end;

procedure Tdlg_Property_add.Timer1Timer(Sender: TObject);
begin
  UpdateGroundHeight();
  Timer1.Enabled:=False;
end;


// ---------------------------------------------------------------
procedure Tdlg_Property_add.row_TemplateEditPropertiesButtonClick( Sender: TObject; AButtonIndex: Integer);
// ---------------------------------------------------------------
begin
 {
  if AButtonIndex=0 then
    if cxVerticalGrid1.FocusedRow=row_Template_site then
      Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otTemplate_Site, Frec.Template_site_id);


//    Dlg_Apply_template;

  if AButtonIndex=1 then
  begin
    Frec.Template_site_id:=0;
    row_Template_site.Properties.Value:='';

  // cx_

    row_Template_site.VerticalGrid.HideEdit;

//    cx_SetButtonEditText(row_Template_site, '');

  end;
}

end;




end.


(*


   {


  obj:=TComClient_GeoCoding.Create;
   obj.Intf.Get_Address_By_Lat_Lon_WGS(bl.B, bl.L, sAddress);



procedure Tdlg_Property_add.ValidateGeoRegion;
begin{
  if not dmUserAuth.IsAccessAllowed_for_GeoRegion(FGeoRegionID) then begin
    ErrDlg (MSG_NOT_ENOUGH_RIGHTS);

    row_GeoRegion.Text:='';
    FGeoRegionID:=0;
  end;}
end;


{var
  NAMES_GENERATE_METHOD: array [0..3] of string = ('���',
                           '������� ��� �������',
                           '��� + ������� ��� �������',
                           '������� ��� ������� + ���'); }


                           
{  // ---------------------------------------------------------------
  if Sender=row_Pos then begin
    if geo_Dlg_Coordinates (FBLPoint, dmOptions.GetGeoCoord()) then
    begin
  //    row_Pos.Text:=geo_FormatBLPoint(FBLPoint);
      row_Pos.Text:=geo_FormatDegreeWithCoordSys(FBLPoint, dmOptions.GetGeoCoord);
      UpdateGroundHeight();

      FGeoRegionID      := dmGeoRegion.GetID_ByBLPoint(FBLPoint);
      row_GeoRegion.Text:= dmGeoRegion.GetNameByID(FGeoRegionID);

    end;
  end else}

{
  if Sender=row_Pos then begin
    if Tdlg_Coordinates.ExecDlg (FBLPoint) then
    begin
      row_Pos.Text:=geo_FormatBLPoint2(FBLPoint);
      UpdateGroundHeight();
      FGeoRegionID:= dmUserRegion.GetRegionID(FBLPoint);
      row_GeoRegion.Text:= dmUserRegion.GetNameByID(FGeoRegionID);
    end;
  end else

}


{const
  WM_FORM_CREATED = WM_USER + 1000;
}
{
procedure Tdlg_Property_add_test;
var
  blPoint: TBLPoint;

begin
  blPoint.B:=59;
  blPoint.L:=30;

  Tdlg_Property_add.ExecDlg (0, blPoint);
end;
}


(*

  if FID>0 then
  begin
    if aFolderID=0 then
      sFolderGUID:=g_Obj.ItemByName[ObjectName].RootFolderGUID
    else
      sFolderGUID:=dmFolder.GetGUIDByID(aFolderID);
                     g_ShellEvents.Shell_UpdateNodeChildren_ByGUID
    { TODO : add focus }
     (sFolderGUID);

  //  SH_PostUpdateNodeChildren (sFolderGUID);
  end;

*)

procedure Tdlg_Property_add.Button1Click(Sender: TObject);
begin

  (*if not dmUserAuth.IsAccessAllowed_for_GeoRegion(FRec.GeoRegionID) then
  begin
    ErrDlg (MSG_NOT_ENOUGH_RIGHTS);

  end;
*)
{

  FBLPoint:=geo_TestBLPoint;


  row_Pos.Text:=geo_FormatDegreeWithCoordSys(FBLPoint, dmOptions.GetGeoCoord);
  UpdateGroundHeight();

  FGeoRegionID      := dmGeoRegion.GetID(FBLPoint);
  row_GeoRegion.Text:= dmGeoRegion.GetNameByID(FGeoRegionID);

  }


end;

procedure Tdlg_Property_add.Button2Click(Sender: TObject);
begin
  inherited;
end;



procedure Tdlg_Property_add.Button1Click(Sender: TObject);
begin
//  dm

end;


    {

procedure Tdlg_Property_add.Button2Click(Sender: TObject);
var
  bl: TBLPoint;
  k: Integer;
begin
//  FRec.BLPoint_Pulkovo:=Fframe_geo_Coordinates.BLPoint_Pulkovo;// FBLPoint;

  bl:=Fframe_geo_Coordinates.BLPoint_Pulkovo;


  k:=dmOnega_DB_data.OpenStoredProc(dmOnega_DB_data.ADOStoredProc1,
     '[Security].sp_User_Allow_LatLon_in_GeoRegion',
     [
       FLD_LAT,  bl.B,
       FLD_LON,  bl.L
     ]

     );

  //[Security].sp_User_Allow_LatLon_in_GeoRegion

//  db_View (dmOnega_DB_data.ADOStoredProc1);

end;


//
//procedure Tdlg_Property_add.FormActivate(Sender: TObject);
//begin
////  Refresh;
//
// // if not geo_Eq(FRec.BLPoint_Pulkovo, NULL_BLPOINT) then
//  //  Timer1.Enabled:=True;
//
////    PosChanged();
//
//end;



