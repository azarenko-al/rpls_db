unit fr_Property_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, Menus, DBCtrls, ToolWin, ComCtrls, ExtCtrls,
  cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters, cxSplitter, StdCtrls,

  u_func_msg,

  dm_User_Security,

  fr_View_base,

 // fr_DBInspector_Container,

  u_types,

  u_func,
  u_dlg,
  u_reg,
  u_db,

  fr_Property_Inspector,
  fr_Property_items,

  u_const_db,


  u_const_msg,
  //u_func_msg,
    
  cxPropertiesStore, ImgList, dxBar, cxBarEditItem, cxClasses;

type
  Tframe_Property_View = class(Tframe_View_Base, IMessageHandler)
    pn_Inspector: TPanel;
    cxSplitter1: TcxSplitter;
    pn_Bottom: TPanel;
    procedure FormCreate(Sender: TObject);
  private
    Ffrm_Inspector: Tframe_Property_inspector;
    Fframe_Property_Items: Tframe_Property_Items;


    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList ; var aHandled:
        boolean);

   // procedure GetMessage(aMsg: integer; aParams: TEventParamList; var aHandled:   Boolean); override;

  protected
//    procedure GetMessage(aMsg: integer; aParams: TEventParamList; var aHandled:   Boolean); override;
  public
    procedure View(aID: integer; aGUID: string); override;
  end;


//==================================================================
implementation {$R *.dfm}
//==================================================================


//--------------------------------------------------------------------
procedure Tframe_Property_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  pn_Bottom.Align:=alClient;

  ObjectName:=OBJ_PROPERTY;
  
  TableName:=TBL_PROPERTY;


  CreateChildForm(Tframe_Property_inspector,  Ffrm_Inspector,  pn_Inspector);
  CreateChildForm(Tframe_Property_Items,  Fframe_Property_Items,  pn_Bottom);


  AddComponentProp(pn_Inspector, PROP_HEIGHT);
  cxPropertiesStore.RestoreFrom;

end;


procedure Tframe_Property_View.GetMessage(aMsg: TEventType; aParams:
    TEventParamList ; var aHandled: boolean);
begin
  inherited;

  case aMsg of
    WE_PROPERTY_REFRESH: View(ID, FGUID);
  end;  
end;


// -------------------------------------------------------------------
procedure Tframe_Property_View.View(aID: integer; aGUID: string);
// -------------------------------------------------------------------
var
  bReadOnly: Boolean;
 // iGeoRegion_ID: Integer;
begin
  inherited;

  Ffrm_Inspector.View (aID);
  Fframe_Property_Items.View(aID);


  bReadOnly:=dmUser_Security.Object_Edit_ReadOnly();

  Ffrm_Inspector.SetReadOnly(bReadOnly);


 // -------------------------
//  FGeoRegion_ID := Ffrm_Inspector.GetDataset().FieldByName(FLD_GeoRegion_ID).AsInteger;// GetIntFieldValue(FLD_GeoRegion_ID);

 // bReadOnly:=dmUser_Security.GeoRegion_is_ReadOnly(FGeoRegion_ID);

 // Ffrm_Inspector.SetReadOnly(bReadOnly, IntToStr(FGeoRegion_ID));

//  Fframe_Property_Items.SetReadOnly(bReadOnly);

end;




end.

{

// ---------------------------------------------------------------
procedure Tframe_Property_View.GetMessage(aMsg: integer; aParams:  TEventParamList; var aHandled: Boolean);
// ---------------------------------------------------------------
begin
  if aMsg=WE_PROPERTY_REFRESH then
    View(ID, FGUID);
end;
