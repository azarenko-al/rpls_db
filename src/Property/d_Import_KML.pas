unit d_Import_KML;

interface

uses
  Windows,   Variants,
  RxToolEdit, dxmdaset,  SysUtils,


  dm_Main,

  dm_Onega_DB_data,

  u_KML_import,

  u_Property_add,


  u_const_db,

//  dm_Property,
  
  u_func,
  
  u_db,

  Forms,

  ComCtrls, Grids, DBGrids, StdCtrls, Mask, Controls, ExtCtrls, Dialogs,
  ADODB, DB, Classes, ActnList, rxPlacemnt, cxLookAndFeels,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGrid;

type
  Tdlg_Import_kml = class(TForm)
    FormStorage1: TFormStorage;
    ActionList1: TActionList;
    act_Save: TAction;
    act_Clear: TAction;
    Panel1: TPanel;
    btn_Close: TButton;
    btn_Save: TButton;
    btn_Clear: TButton;
    btnLoadKML: TButton;
    Panel2: TPanel;
    FilenameEdit1: TFilenameEdit;
    Button1: TButton;
    dxMemData1: TdxMemData;
    dxMemData1name: TStringField;
    dxMemData1lat: TFloatField;
    dxMemData1lon: TFloatField;
    dxMemData1checked: TBooleanField;
    DataSource1: TDataSource;
    Edit1: TEdit;
    dxMemData1Description: TStringField;
    DBGrid1: TDBGrid;
    StatusBar1: TStatusBar;
    ADOStoredProc1: TADOStoredProc;
    dxMemData1id: TIntegerField;
    OpenDialog1: TOpenDialog;
    act_Open: TAction;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1RecId: TcxGridDBColumn;
    cxGrid1DBTableView1checked: TcxGridDBColumn;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    cxGrid1DBTableView1lat: TcxGridDBColumn;
    cxGrid1DBTableView1lon: TcxGridDBColumn;
    cxGrid1DBTableView1Description: TcxGridDBColumn;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxLookAndFeelController1: TcxLookAndFeelController;
    procedure act_ClearExecute(Sender: TObject);
//    procedure btn_SaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
//    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol:
//        Integer; Column: TColumn; State: TGridDrawState);
    procedure FileOpen_KMLAccept(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    FDataset: TDataset;

    FProject_id: Integer;

    procedure Clear_DB;
    procedure OpenFileKML(aFileName: string);
    procedure Save;
    procedure UpdateStatusBar;

  public
    class function ExecDlg(aProject_id: Integer): boolean;

  end;


implementation

{$R *.dfm}

var
  dlg_Import_kml: Tdlg_Import_kml;



//------------------------------------------------------------------------------
class function Tdlg_Import_kml.ExecDlg(aProject_id: Integer): boolean;
//------------------------------------------------------------------------------
begin
//  TdmMain_search.Init;
//  dmMain_search.ADOConnection1.ConnectionString:=aADOConnectionString;
//  dmMain_search.ADOConnection1.Connected:=True;



  with Tdlg_Import_kml.Create(Application) do
  begin
    FProject_id:=aProject_id;

   // FFolderID:= aFolderID;

    ShowModal;

  ///  Result := FIsPointsLoad;

    Free;
  end;


//  FreeAndNil(dmMain_search);
  
end;

// ---------------------------------------------------------------
procedure Tdlg_Import_kml.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  Caption:='������ �� KML';
                             

  FDataset:=dxMemData1;

  DBGrid1.Align:=alClient;

  SetComponentActionsExecuteProc(Self, act_ClearExecute);

  Clear_DB();

//  db_DeleteRecordsFromDataset(FDataset);

  
  db_SetFieldCaptions(dxMemData1,
    [  
      FLD_CHECKED,     '���',
        
      FLD_NAME,        '��������',        
      FLD_Description, '��������',
        
      FLD_LAT, '������',
      FLD_LON, '�������'
    ]);
  
end;

// ---------------------------------------------------------------
procedure Tdlg_Import_kml.act_ClearExecute(Sender: TObject);
// ---------------------------------------------------------------
begin
  if Sender=act_Save then
    Save else   

  if Sender=act_Open then
  begin
    if OpenDialog1.Execute then
      OpenFileKML (OpenDialog1.FileName);

  end else

  if Sender=act_Clear then
    Clear_DB;
end;

 

// ---------------------------------------------------------------
procedure Tdlg_Import_kml.Save;
// ---------------------------------------------------------------
var
  eLat: Double;
  eLon: Double;
  iID: Integer;
  sDescription: string;
  sName: string;

  oPropertyAddToDB: TPropertyAddToDB;

  iTotal: integer;
  s: string;
begin
 // Assert(dmMain_search <> nil);



  iTotal:=0;

  ADOStoredProc1.Connection:=dmMain.ADOConnection1;

  FDataset.DisableControls;
  FDataset.First;

  with FDataset do
    while not EOF do
    begin
      if FieldByName(FLD_CHECKED).AsBoolean then
      begin
        oPropertyAddToDB:=TPropertyAddToDB.Create;

        oPropertyAddToDB.NewName:=FieldByName(FLD_NAME).AsString;
        oPropertyAddToDB.Address:=FieldByName(FLD_Description).AsString;

        oPropertyAddToDB.BLPoint_WGS.B:=FieldByName(FLD_LAT).AsFloat;
        oPropertyAddToDB.BLPoint_WGS.L:=FieldByName(FLD_LON).AsFloat;

        oPropertyAddToDB.PROJECT_ID:=FProject_ID;

        iID:=oPropertyAddToDB.Add_DB();

        FreeAndNil (oPropertyAddToDB);

        {

        sName       :=FieldByName(FLD_NAME).AsString;
        sDescription:=FieldByName(FLD_Description).AsString;

        eLat:=FieldByName(FLD_LAT).AsFloat;
        eLon:=FieldByName(FLD_LON).AsFloat;

//        dmOnega_DB_data.ExecStoredProc_()

        iID:=dmOnega_DB_data.ExecStoredProc_('SP_PROPERTY_ADD',
          [
           FLD_PROJECT_ID,      FProject_ID,
           FLD_NAME,            sName,
           FLD_ADDRESS,         sDescription,

            FLD_LAT,            eLat,
            FLD_LON,            eLon,

            FLD_LAT_WGS,         eLat,
            FLD_LON_WGS,         eLon,

            FLD_LAT_CK95,        eLat,
            FLD_LON_CK95,        eLon

          ]);

          }

        Inc(iTotal);

        db_UpdateRecord__ (FDataset, [FLD_ID, iID]) ;
      end;



      Next;
    end;


  FDataset.EnableControls;


  s:= Format('��������� ���������� ��� %d ��������.', [iTotal]);

  ShowMessage(s);



//
//  Result:=dmOnega_DB_data.ExecStoredProc_ (SP_PROPERTY_ADD,
//          [
//            FLD_PROJECT_ID,      Project_ID,
//            FLD_NAME,            NewName,
//            FLD_FOLDER_ID,       IIF_NULL(Folder_ID),
//
//            FLD_Tower_height,    IIF_NULL(Tower_height),
//
//
//            FLD_GROUND_HEIGHT,   IIF_NULL(Ground_Height),
//            FLD_CLUTTER_HEIGHT,  IIF_NULL(CLUTTER_HEIGHT),
//            FLD_clutter_name,    IIF_NULL(clutter_name),
//
//            FLD_LAT,             BLPoint_Pulkovo.B,
//            FLD_LON,             BLPoint_Pulkovo.L,
//
  

end;


procedure Tdlg_Import_kml.Button1Click(Sender: TObject);
begin
  OpenFileKML(FilenameEdit1.FileName);

  
end;

procedure Tdlg_Import_kml.Button2Click(Sender: TObject);
begin
  OpenDialog1.Execute;
end;


procedure Tdlg_Import_kml.FileOpen_KMLAccept(Sender: TObject);
begin
//  ShowMessage('FileOpen_KMLAccept');

  OpenFileKML (OpenDialog1.FileName);

end;


procedure Tdlg_Import_kml.Clear_DB;
begin
  db_DeleteRecordsFromDataset (dxMemData1);
end;

// ---------------------------------------------------------------
procedure Tdlg_Import_kml.OpenFileKML(aFileName: string);
// ---------------------------------------------------------------
var
  oKML: TKML;
begin
  oKML:=TKML.Create;
  oKML.LoadFromFile(aFileName);
  
  Clear_DB;
  
  oKML.Placemarks.SaveToDataset(dxMemData1);
  FreeAndNil(oKML);

  UpdateStatusBar  ();
  
end;

// ---------------------------------------------------------------
procedure Tdlg_Import_kml.UpdateStatusBar;
// ---------------------------------------------------------------
begin
  StatusBar1.SimpleText:=  Format('%d �������',[dxMemData1.RecordCount]);
end;
              



procedure Tdlg_Import_kml.DBGrid1CellClick(Column: TColumn);
begin

 if (Column.Field.DataType=ftBoolean) then
  begin      
    Column.Grid.DataSource.DataSet.Edit;
    Column.Field.Value:= not Column.Field.AsBoolean;
    Column.Grid.DataSource.DataSet.Post;   
  end;
end;



// ---------------------------------------------------------------
procedure Tdlg_Import_kml.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
//  Exit;

//  Assert(Column<>nil);
//  Assert(Column.Field<>nil);

//  s:=Sender.Classname;    //DBGrid1

  if Column.Field=nil then //!!!!!
    Exit;

      if (Column.Field.DataType=ftBoolean) then
      begin
        DBGrid1.Canvas.FillRect(Rect) ;

        if (VarIsNull(Column.Field.Value))  or (Column.Field.Value=False)
        then
          DrawFrameControl(DBGrid1.Canvas.Handle,Rect, DFC_BUTTON, DFCS_BUTTONCHECK or DFCS_INACTIVE)
        else
          DrawFrameControl(DBGrid1.Canvas.Handle,Rect, DFC_BUTTON, DFCS_BUTTONCHECK or DFCS_CHECKED);
      end;

  try

  except
    on E: Exception do

  end;

end;



end.



{

unit u_Property_Add;

interface
uses
  Variants, 

  dm_Onega_DB_data,

  u_classes,
  u_db,
  u_func,
  u_Geo,
//
//      bl:=geo_WGS84_to_Pulkovo42(bl) ;
//        bl1:=geo_Pulkovo42_to_WGS84(bl) ;

  u_geo_convert_new,
  

  u_types,

  u_const_db ;


type

  //---------------------------------------------------------
  TPropertyAddToDB = class
  //---------------------------------------------------------

  public
    PROJECT_ID     : Integer;

    NewName         : string;
    Folder_ID       : integer;

    Folder_Name     : string;

    BLPoint_Pulkovo: TBLPoint;
    BLPoint_WGS    : TBLPoint;

    CoordnateKind : (ckPulkovo,ckWGS);
//    CoordnateKind : (ckNone,ckPulkovo,ckWGS);

    Tower_height : Double;


    Ground_Height  : integer;
    CLUTTER_HEIGHT : integer;
    CLUTTER_NAME   : string;

    Comments       : string;

    Address        : string;
    City           : string;

    GeoRegion_ID   : Integer;

    Placement_id   : Integer;
    State_id       : Integer;

    //Beeline
    NAME_SDB : string;
    NAME_ERP : string;

  public
    function Add(aIDList: TIDList = nil): integer;
  end;


implementation


//--------------------------------------------------------------------
function TPropertyAddToDB.Add(aIDList: TIDList = nil): integer;
//--------------------------------------------------------------------
var
  bl_CK95: TBLPoint;
begin
  Assert(Assigned(dmOnega_DB_data));


//  bl_CK95 := geo_Pulkovo_BL_to_BL_95(BLPoint_Pulkovo);
//  bl_CK95 := geo_Pulkovo_CK_42_to_CK_95 (BLPoint_Pulkovo);
  bl_CK95 := geo_Pulkovo42_to_CK_95 (BLPoint_Pulkovo);            
  Assert(NewName<>'');
  Assert(Project_ID>0, ' ProjectID <=0');
  // Added by Aleksey 01.09.2009 12:08:33
  //geo_ReduceBLPoint (aRec.BLPoint);


  if (BLPoint_WGS.B=0) and (BLPoint_Pulkovo.B<>0) then
//    BLPoint_WGS:=geo_BL_to_BL_new(BLPoint_Pulkovo, EK_CK_42, EK_WGS84);
    BLPoint_WGS:=geo_Pulkovo42_to_WGS84(BLPoint_Pulkovo);

  if (BLPoint_WGS.B<>0) and (BLPoint_Pulkovo.B=0) then
  //  BLPoint_Pulkovo:=geo_BL_to_BL_new(BLPoint_WGS, EK_WGS84, EK_CK_42);
    BLPoint_Pulkovo:=geo_WGS84_to_Pulkovo42(BLPoint_WGS);

//    result := Format('%1.8f N  %1.8f E', [bl.B, bl.L]);





  Result:=dmOnega_DB_data.ExecStoredProc_ (SP_PROPERTY_ADD,
          [
            FLD_PROJECT_ID,      Project_ID,
            FLD_NAME,            NewName,
            FLD_FOLDER_ID,       IIF_NULL(Folder_ID),

            FLD_Tower_height,    IIF_NULL(Tower_height),


            FLD_GROUND_HEIGHT,   IIF_NULL(Ground_Height),
            FLD_CLUTTER_HEIGHT,  IIF_NULL(CLUTTER_HEIGHT),
            FLD_clutter_name,    IIF_NULL(clutter_name),

            FLD_LAT,             BLPoint_Pulkovo.B,
            FLD_LON,             BLPoint_Pulkovo.L,

            FLD_LAT_WGS,         BLPoint_WGS.B,
            FLD_LON_WGS,         BLPoint_WGS.L,

            FLD_LAT_CK95,        bl_CK95.B,
            FLD_LON_CK95,        bl_CK95.L,


            FLD_ADDRESS,         IIF_NULL(Address),
            FLD_CITY,            IIF_NULL(CITY),

            FLD_Comments,        IIF_NULL(Comments),

            FLD_Placement_id,    IIF_NULL(Placement_id),
            FLD_State_id,        IIF_NULL(State_id),

            FLD_GEOREGION_ID,    IIF_NULL(GeoRegion_ID),

            FLD_NAME_SDB, IIF_NULL(NAME_SDB),
            FLD_NAME_ERP, IIF_NULL(NAME_ERP)
          ]); //

 // db_ViewDataSet(aADOQuery);

  Assert(result>0, 'SP_PROPERTY_ADD.result <=0');



  if Assigned(aIDList) then
    aIDList.AddObjName(Result, OBJ_PROPERTY);

 
end;



end.


}
