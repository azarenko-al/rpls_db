unit fr_Property_items;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  dm_Onega_DB_data,

  u_storage,

  u_cx,

  //f_Custom, cxControls, cxTLData


 // f_Custom,

  u_const,
  u_const_str,
  u_const_db,

  u_func,

  u_db,
    
  u_types,

  // dm_Property_View,

  cxControls, cxTLData, f_Custom, DB, ADODB, cxInplaceContainer, cxTL,
  cxDBTL
  ;


type
  Tframe_Property_Items = class(TForm)
    DataSource1: TDataSource;
    ADOStoredProc1: TADOStoredProc;
    cxDBTreeList1: TcxDBTreeList;
    col_Name_: TcxDBTreeListColumn;
    cxDBTreeList1LinkEnd_loss: TcxDBTreeListColumn;
    col_POLARIZATION_str_: TcxDBTreeListColumn;
    col_Antenna_Loss: TcxDBTreeListColumn;
    col_Ant_Gain_: TcxDBTreeListColumn;
    col_ant_Height_: TcxDBTreeListColumn;
    col_ant_Azimuth_: TcxDBTreeListColumn;
    col_Ant_Type_: TcxDBTreeListColumn;
    col_ant_diameter_: TcxDBTreeListColumn;
    col_Mast_index: TcxDBTreeListColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  //  procedure act_Grid_SetupExecute(Sender: TObject);
  public
    procedure View (aID: integer);
  end;


implementation {$R *.DFM}


// -------------------------------------------------------------------
procedure Tframe_Property_Items.FormCreate(Sender: TObject);
// -------------------------------------------------------------------
begin
  inherited;


  cxDBTreeList1.Align:=alClient;

//  dxDBTree.Align:=alClient;


 // act_Grid_Setup.Caption:= STR_SETUP_TABLE;


  col_POLARIZATION_str_.Caption.Text:='�����������';

  col_Name_.Caption.Text:=STR_NAME;
  col_ant_Azimuth_.Caption.Text:=STR_AZIMUTH;
  col_ant_Height_.Caption.Text:=STR_HEIGHT;
  col_ant_Diameter_.Caption.Text:=STR_DIAMETER;

  col_Antenna_Loss.Caption.Text:=STR_ANTENNA_LOSS;
  col_Ant_Type_.Caption.Text:=STR_TYPE;
  col_ant_Gain_.Caption.Text:=STR_GAIN; // '�� [dB]';


  col_Antenna_Loss.DataBinding.FieldName:=FLD_Antenna_Loss;

  g_storage.RestoreFromRegistry(cxDBTreeList1, ClassName);



  cx_SetColumnCaptions(cxDBTreeList1,
     [
       FLD_name,     STR_NAME,
       FLD_Height,   STR_HEIGHT,
       FLD_Diameter, STR_DIAMETER,
       FLD_Antenna_Loss,  STR_ANTENNA_LOSS,
       FLD_antennatype_name,  STR_TYPE,
       FLD_Gain,  STR_GAIN,
       FLD_MAST_INDEX,  STR_MAST_INDEX

     ] );






  // sdfgdfg


 // cxDBTreeList1.Customizing.MakeColumnSheetVisible;

//  cxDBTreeList1.StoreToRegistry(FRegPath + cxDBTreeList1.Name);


end;


// -------------------------------------------------------------------
procedure Tframe_Property_Items.FormDestroy(Sender: TObject);
// -------------------------------------------------------------------
begin
 // dxDBTree.SaveToRegistry (FRegPath+ dxDBTree.Name);

 //  cxDBTreeList1.StoreToRegistry(FRegPath + cxDBTreeList1.Name);

  g_storage.StoreToRegistry(cxDBTreeList1, ClassName);

  inherited;
end;


// ---------------------------------------------------------------
procedure Tframe_Property_Items.View (aID: integer);
// ---------------------------------------------------------------
begin
  dmOnega_DB_data.Property_Select_Item(ADOStoredProc1, aID);
     
(*
  db_SetFieldCaptions(ADOStoredProc1,
     [
       SArr(FLD_name,    STR_NAME),
       SArr(FLD_Height,  STR_HEIGHT),
       SArr(FLD_Diameter, STR_DIAMETER),
       SArr(FLD_Antenna_Loss_db,  STR_ANTENNA_LOSS_dB),
       SArr(FLD_antennatype_name,  STR_TYPE),
       SArr(FLD_Gain,  STR_GAIN_dB)

     ] );
*)
  cxDBTreeList1.FullExpand;
    //  col_Name.Caption:=STR_COL_NAME;


//  dxDBTree.FullExpand;
/////  dmProperty_View.OpenDB (aID, mem_Items);

 // dx_CheckColumnSizes_DBTreeList (dxDBTree);  // ����� opendDB  !!!!!

end;



end.


 // col_AntType_Name.FieldName:=FLD_ANTENNATYPE_NAME;
//  col_Polar.Caption:='�����������';


 // dmProperty_View.InitDB (mem_Items);

(*  dx_CheckRegistry (dxDBTree,  FRegPath+ dxDBTree.Name);
  dxDBTree.LoadFromRegistry (FRegPath+ dxDBTree.Name);
*)