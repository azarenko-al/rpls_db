unit dm_ColorSchema_export;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, Db, ADODB,

  u_Params_Bin_to_Map_classes,

  dm_Main,

  XMLDoc, XMLIntf, u_xml_document,

  u_Func_arrays,
  u_func,
  u_db,

  u_const_db
  ;

type
  TdmColorSchema_export = class(TDataModule)
    qry_Schemas: TADOQuery;
    qry_Ranges: TADOQuery;
    qry_Item: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure AddNode (aID: integer; aNode: IXMLNode);
//    procedure ExportToXML_All(aFileName: string);

  public
    procedure SaveToXML_array (aArrID: TIntArray; aFileName: string); overload;
    procedure SaveToXML (aID: integer; aFileName: string); overload;

    procedure ExportToXMLNode(aParentNode: IXMLNode);

    function ExportItemToXMLNode(aID: integer; aParentNode: IXMLNode): boolean;

    procedure ExportItemsToXMLNode(aParentNode: IXMLNode; aIDArr: array of Integer);

    function ExportItemToClass(aID: integer; aColorSchemaList: TColorSchemaList):
        Boolean;


    procedure SetADOConnection(aADOConnection: TADOConnection);


  end;

function dmColorSchema_export: TdmColorSchema_export;


//================================================
implementation

uses dm_Onega_db_data; {$R *.DFM}
//================================================

const
  TAG_COLOR_SCHEMAS = 'COLOR_SCHEMAS';
  ATT_COLOR_SCHEMA = 'COLOR_SCHEMA';

  ATT_MIN_VALUE = 'MIN_VALUE';
  ATT_MAX_VALUE = 'MAX_VALUE';
  ATT_MIN_COLOR = 'MIN_COLOR';
  ATT_MAX_COLOR = 'MAX_COLOR';
  ATT_IS_TRANSPARENT = 'IS_TRANSPARENT';



var
  FdmColorSchema_export: TdmColorSchema_export;


// ---------------------------------------------------------------
function dmColorSchema_export: TdmColorSchema_export;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmColorSchema_export) then
    FdmColorSchema_export:=TdmColorSchema_export.Create(Application);

  Result := FdmColorSchema_export;
end;


procedure TdmColorSchema_export.DataModuleCreate(Sender: TObject);
begin
  db_SetComponentADOConnection (Self, dmMain.ADOConnection);
end;


procedure TdmColorSchema_export.SetADOConnection(aADOConnection: TADOConnection);
begin
  db_SetComponentADOConnection (Self, aADOConnection);
end;



//-------------------------------------------------------------------
procedure TdmColorSchema_export.SaveToXML (aID: integer; aFileName: string);
//-------------------------------------------------------------------
var arrID: TIntArray;
begin
  AddToIntArray(aID, arrID);
  SaveToXML_array (arrID, aFileName);
end;

//-------------------------------------------------------------------
procedure TdmColorSchema_export.SaveToXML_array (aArrID: TIntArray; aFileName: string);
//-------------------------------------------------------------------
var
  oGroup: IXMLNode;
  i: integer;

  oXMLDoc: TXMLDoc;//('');

begin
  oXMLDoc:=TXMLDoc.Create;

  oGroup := oXMLDoc.DocumentElement.AddChild(TAG_COLOR_SCHEMAS);

  for i:=0 to High(aArrID) do
    AddNode(aArrID[i], oGroup);

  oXMLDoc.SaveToFile(aFileName);

  oXMLDoc.Free;

end;

//-------------------------------------------------------------------
procedure TdmColorSchema_export.AddNode (aID: integer; aNode: IXMLNode);
//-------------------------------------------------------------------
var
  vNode,vNode1: IXMLNode;

begin
  dmOnega_DB_data.OpenQuery (qry_Ranges,
          'SELECT * FROM '+ TBL_ColorSchemaRanges +
          ' WHERE color_schema_id=:color_schema_id  ORDER BY min_value ',
         [FLD_color_schema_id, aID]);

  dmOnega_DB_data.OpenQuery (qry_Schemas,
          'SELECT * FROM '+ TBL_COLORSCHEMA +
          ' WHERE id=:id ',
         [FLD_id, aID]);


 // sName:=gl_db.GetNameByID(TBL_COLOR_SCHEMA, aID);
  vNode := xml_AddNode (aNode, ATT_COLOR_SCHEMA,
        [xml_Par(ATT_NAME,    qry_Schemas.FieldByName(FLD_NAME).AsString),
         xml_Par(ATT_COMMENT, qry_Schemas.FieldByName(FLD_COMMENT).AsString)
        ]);

  with qry_Ranges do
    while not Eof do
    begin
       xml_AddNode (vNode, 'range',
         [xml_Par(ATT_COLOR,     FieldBYname(FLD_COLOR).AsInteger),
          xml_Par(ATT_MIN_VALUE, FieldBYname(FLD_MIN_VALUE).AsInteger),
          xml_Par(ATT_MAX_VALUE, FieldBYname(FLD_MAX_VALUE).AsInteger),
          xml_Par(ATT_MIN_COLOR, FieldBYname(FLD_MIN_COLOR).AsInteger),
          xml_Par(ATT_MAX_COLOR, FieldBYname(FLD_MAX_COLOR).AsInteger),
          xml_Par(ATT_TYPE,      FieldBYname(FLD_TYPE).AsInteger),
          xml_Par(ATT_ENABLED,   FieldBYname(FLD_ENABLED).AsBoolean),
          xml_Par(ATT_IS_TRANSPARENT, FieldBYname(FLD_IS_TRANSPARENT).AsBoolean),
          xml_Par(ATT_COMMENT,   FieldBYname(FLD_COMMENT).AsString)

          ]);
      Next;
    end;
end;



//-------------------------------------------------------------------
procedure TdmColorSchema_export.ExportToXMLNode(aParentNode: IXMLNode);
//-------------------------------------------------------------------
var vGroup: IXMLNode;
begin
  vGroup := aParentNode.AddChild( TAG_COLOR_SCHEMAS);

  dmOnega_DB_data.OpenQuery (qry_Schemas, 'SELECT * FROM '+ TBL_COLORSCHEMA);

  with qry_Schemas do
    while not Eof do
    begin
      ExportItemToXMLNode (FieldBYname(FLD_ID).AsInteger, vGroup);
      Next;
    end;
end;

//-------------------------------------------------------------------
procedure TdmColorSchema_export.ExportItemsToXMLNode(aParentNode: IXMLNode;
    aIDArr: array of Integer);
//-------------------------------------------------------------------
var
  oArr: TIntArray;
  sInSql: string;
  vGroup: IXMLNode;
begin
//  IntArrayClear(oArr);
  SetLength(oArr, 0);

  IntArrayAddValues(oArr, aIDArr);
  sInSql:= Format('in (%s)', [IntArrayToStringSimple(oArr)]);
  dmOnega_DB_data.OpenQuery (qry_Schemas, 'SELECT * FROM '+ TBL_COLORSCHEMA+' WHERE id '+sInSql);

  dmOnega_DB_data.OpenQuery (qry_Ranges,
          'SELECT * FROM '+ TBL_COLORSCHEMARANGES +
          ' WHERE (color_schema_id '+sInSql+ ') and (enabled<>0)'+
          ' ORDER BY min_value ');

  qry_Ranges.Filtered:= true;

  with qry_Schemas do
    while not Eof do
  begin
     vGroup := xml_AddNode (aParentNode, 'item',
                          [xml_Par(FLD_NAME, FieldValues[FLD_NAME]),
                           xml_Par(FLD_ID,   FieldBYname(FLD_ID).AsInteger)
                           ]);

     qry_Ranges.Filter:= 'color_schema_id=' + FieldByName(FLD_ID).AsString;

     qry_Ranges.First;

     with qry_Ranges do
       while not Eof do
       begin
           xml_AddNode (vGroup, 'range',
             [xml_Par(FLD_COLOR,   FieldBYname(FLD_COLOR).AsInteger),
              xml_Par('min_value', FieldBYname('min_value').AsInteger),
              xml_Par('max_value', FieldBYname('max_value').AsInteger),
              xml_Par('min_color', FieldBYname('min_color').AsInteger),
              xml_Par('max_color', FieldBYname('max_color').AsInteger),
              xml_Par(FLD_TYPE,    FieldBYname(FLD_TYPE).AsInteger)]);
          Next;
       end;

    Next;
  end;

  qry_Ranges.Filter:= '';
  qry_Ranges.Filtered:= false;
end;


//-------------------------------------------------------------------
function TdmColorSchema_export.ExportItemToXMLNode(aID: integer; aParentNode:
    IXMLNode): boolean;
//-------------------------------------------------------------------
var
  vGroup,vNode: IXMLNode;

//  oColorSchema: TColorSchema;

(*
 TColorSchemaRange = class(TCollectionItem)
  public
    MinValue : double; // �������� � �������
    MaxValue : double; // �������� � �������
    Color    : integer; // ���� �� �����
    MaxColor : integer;
    MinColor : integer;

    Type_    : (rtFixed,rtGradient);
 //  Caption  : string;
  //IsTransparent: boolean;
  end;

  TColorSchema = class(TCollectionItem)
  public
    ID : Integer;
    Index : Integ
*)

begin
  db_OpenQueryByFieldValue (qry_Item, TBL_COLORSCHEMA, FLD_ID, aID);

//  oColorSchema := aColorSchemaList.AddItem;

  with qry_Item do
    while not Eof do
    begin
    ////  oColorSchema.ID := FieldValues[FLD_ID];
    //  oColorSchema.na := FieldValues[FLD_ID];

       vGroup := xml_AddNode (aParentNode, 'item',
                            [xml_Par(FLD_NAME, FieldValues[FLD_NAME]),
                             xml_Par(FLD_ID, FieldBYname(FLD_ID).AsInteger)
                             ]);

       dmOnega_DB_data.OpenQuery (qry_Ranges,
                'SELECT * FROM '+ TBL_COLORSCHEMARANGES +
                ' WHERE (color_schema_id=:color_schema_id) and (enabled<>0) '+
                ' ORDER BY min_value ',
               [FLD_color_schema_id, FieldValues[FLD_ID]] );

       Result:= not qry_Ranges.IsEmpty;

       with qry_Ranges do
         while not Eof do
         begin
             xml_AddNode (vGroup, 'range',
               [xml_Par(FLD_COLOR,   FieldBYname(FLD_COLOR).AsInteger  ),
                xml_Par('min_value', FieldBYname('min_value').AsInteger),
                xml_Par('max_value', FieldBYname('max_value').AsInteger),
                xml_Par('min_color', FieldBYname('min_color').AsInteger),
                xml_Par('max_color', FieldBYname('max_color').AsInteger),
                xml_Par(FLD_TYPE,    FieldBYname(FLD_TYPE).AsInteger)
               ]);
            Next;
         end;

       Next;
    end;
end;


//-------------------------------------------------------------------
function TdmColorSchema_export.ExportItemToClass(aID: integer;
    aColorSchemaList: TColorSchemaList): Boolean;
//-------------------------------------------------------------------
var
  oColorSchema: TColorSchema;
  oRange: TColorSchemaRange;

begin

  db_OpenQueryByFieldValue (qry_Item, TBL_COLORSCHEMA, FLD_ID, aID);

  dmOnega_DB_data.OpenQuery (qry_Ranges,
            'SELECT * FROM '+ TBL_COLORSCHEMARANGES +
            ' WHERE (color_schema_id=:color_schema_id) and (enabled<>0) '+
            ' ORDER BY min_value ',
           [FLD_color_schema_id, aID] );




  with qry_Item do
    while not Eof do
    begin
      oColorSchema := aColorSchemaList.AddItem;

      oColorSchema.ID   := FieldValues[FLD_ID];


       with qry_Ranges do
         while not Eof do
         begin
           oRange := oColorSchema.Ranges.AddItem;

           oRange.Color   := FieldBYname(FLD_COLOR).AsInteger;
           oRange.minvalue:= FieldBYname('min_value').AsInteger;
           oRange.maxvalue:= FieldBYname('max_value').AsInteger;
           oRange.mincolor:= FieldBYname('min_color').AsInteger;
           oRange.maxcolor:= FieldBYname('max_color').AsInteger;

            Next;
         end;

       Next;
    end;

  Result:= not qry_Ranges.IsEmpty;
end;


begin

end.

