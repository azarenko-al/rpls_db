unit d_ColorSchema_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rxPlacemnt, ActnList,      StdCtrls, ExtCtrls,

  dm_Onega_DB_data,


  d_Wizard_add_with_Inspector,

  dm_ColorSchema,
//  dm_Folder,

  u_const,
  u_const_str,
  u_const_db,

  u_Geo,
  u_dlg,
  u_func_msg,
  u_func,


  cxPropertiesStore, ComCtrls
  ;



type
  Tdlg_ColorSchema_add = class(Tdlg_Wizard_add_with_Inspector)
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure row_FolderButtonClick(Sender: TObject;
      AbsoluteIndex: Integer);

  private
    FRec: TdmColorSchemaAddRec;

    FID: integer;

  public
    class function ExecDlg (aFolderID: integer): integer;
  end;


//==================================================
implementation {$R *.dfm}
//==================================================

//-------------------------------------------------
class function Tdlg_ColorSchema_add.ExecDlg (aFolderID: integer): integer;
//-------------------------------------------------
begin                                             
  with Tdlg_ColorSchema_add.Create(Application) do
  try
    ed_Name_.Text:=dmColorSchema.GetNewName();
 //   row_Folder.Text:=
    //dmFolder.GetPath (aFolderID);

     //  dmOnega_DB_data.Folder_GetFullPath(aFolderID);

    FRec.FolderID:=aFolderID;

    if (ShowModal=mrOk) then
    begin
      Result:=FID;
    end else
      Result:=0;

  finally
    Free;
  end;    // with
end;


procedure Tdlg_ColorSchema_add.FormCreate(Sender: TObject);
begin
  inherited;
  SetActionName (STR_DLG_ADD_COLOR_SCHEMA);

  SetDefaultSize();
  
end;


procedure Tdlg_ColorSchema_add.act_OkExecute(Sender: TObject);
//var
begin
  inherited;

  FRec.NewName:=ed_Name_.Text;
//  FRec.FolderID:=FFolderID;

  FID:=dmColorSchema.Add (FRec);
end;


procedure Tdlg_ColorSchema_add.row_FolderButtonClick(Sender: TObject;
  AbsoluteIndex: Integer);
begin
  inherited;

  ////////////
end;

end.