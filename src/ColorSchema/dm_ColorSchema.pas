unit dm_ColorSchema;

interface

uses
  SysUtils, Classes, DB, ADODB, Graphics, Forms, Dialogs,  Variants,

  dm_Onega_DB_data,

  u_func,
  u_db,

  u_const_db,

  u_types,

  dm_Main,
  dm_Object_base;
  

type
  TdmColorSchemaAddRec = record
    NewName:  string;
    FolderID: integer;
    Comment:  string;
  end;

  TdmColorRangeParamRec = record
     SchemaID      : integer;
     Min_value     : double;
     Max_value     : double;
     Color         : integer;
     Min_color     : integer;
     Max_color     : integer;
     Transparent: boolean;
     Type_         : integer;
     Enabled       : boolean;
     Comment       : string;

  end;



  TdmColorSchema = class(TdmObject_base)
    ColorDialog: TColorDialog;
    qry_Ranges: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  public
    function Add (aRec: TdmColorSchemaAddRec): integer;
    function AddRanges (aRec: TdmColorRangeParamRec): integer;

    function Copy(aSourceID: integer; aName: string): integer;
//    procedure OpenData;

  end;


function dmColorSchema: TdmColorSchema;

//==================================================================
implementation {$R *.dfm}
//==================================================================

var
  FdmColorSchema: TdmColorSchema;


// ---------------------------------------------------------------
function dmColorSchema: TdmColorSchema;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmColorSchema) then
    FdmColorSchema := TdmColorSchema.Create(Application);

  Result := FdmColorSchema;
end;


//-----------------------------------------------------------------------
procedure TdmColorSchema.DataModuleCreate(Sender: TObject);
//-----------------------------------------------------------------------
begin
  inherited;
  TableName := TBL_ColorSchema;
  ObjectName :=OBJ_COLOR_SCHEMA;
end;

//-----------------------------------------------------------------------
function TdmColorSchema.Add (aRec: TdmColorSchemaAddRec): integer;
//-----------------------------------------------------------------------
begin
  with aRec do
   Result:=gl_DB.AddRecordID (TBL_ColorSchema,
        [db_Par (FLD_NAME,     NewName),
         db_Par(FLD_FOLDER_ID, IIF_NULL(FolderID)),
         db_Par(FLD_COMMENT,  Comment)
        ]);
end;

//-----------------------------------------------------------------------
function TdmColorSchema.AddRanges (aRec: TdmColorRangeParamRec ): integer;
//-----------------------------------------------------------------------
begin
  with aRec do
    Result:=gl_DB.AddRecordID (TBL_ColorSchemaRanges,
                 [db_Par(FLD_COLOR_SCHEMA_ID, SchemaID),
                  db_Par(FLD_MIN_VALUE,       Min_value),
                  db_Par(FLD_MAX_VALUE,       Max_value),
                  db_Par(FLD_MIN_COLOR,       Min_color),
                  db_Par(FLD_MAX_COLOR,       Max_color),
                  db_Par(FLD_COLOR    ,       Color),
                  db_Par(FLD_TYPE     ,       Type_),
                  db_Par(FLD_ENABLED  ,       Enabled),
                  db_Par(FLD_IS_TRANSPARENT,  Transparent),
                  db_Par(FLD_COMMENT,         Comment)

                 ]);

end;

//------------------------------------------------------
function TdmColorSchema.Copy(aSourceID: integer; aName: string): integer;
//------------------------------------------------------
begin
  Result:=dmOnega_DB_data.ColorSchema_Copy(aSourceID, aName);
end;


begin

end.