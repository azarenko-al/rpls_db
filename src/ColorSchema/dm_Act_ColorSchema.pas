unit dm_Act_ColorSchema;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Dialogs,  Menus, ActnList,
  Controls, Forms,

  dm_User_Security,

  u_DataExport_run,

  i_Audit,



  I_Object,
  I_Shell,
  u_Shell_new,

  dm_act_Base,

  dm_ColorSchema,
  dm_ColorSchema_import,
  dm_ColorSchema_export,

 // u_func_msg,
  u_common_const,
  u_const_filters,


  u_dlg,

  u_Func_arrays,  
  u_func,

  u_Types,
  
  u_const,
  u_const_db,
  u_const_str,
//  u_const_msg,

  d_ColorSchema_add,
  fr_ColorSchema_view ;

type
  TdmAct_ColorSchema = class(TdmAct_Base)
    OpenDialog1: TOpenDialog;
    act_ExportToXML: TAction;
    SaveDialog_XML: TSaveDialog;
    act_ImportFromXML: TAction;
    ActionList2: TActionList;
    act_Copy: TAction;
    act_Export_MDB: TAction;
    act_Audit: TAction;
    procedure DataModuleCreate(Sender: TObject);
  private
    function CheckActionsEnable: Boolean;
    procedure DoAction (Sender: TObject);

  protected
    function  GetViewForm (aOwnerForm: TForm): TForm; override;
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;

    function ItemDel(aID: integer; aName: string): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;

  public
    procedure Dlg_ExportToXML(aArrID: TIntArray);

    procedure Dlg_ImportFromXML(aFolderID: integer);
//    procedure ImportFromXML_ver1205 (aFolderID: integer);

    class procedure Init;
  end;
  

var
  dmAct_ColorSchema: TdmAct_ColorSchema;

//====================================================================
implementation

uses dm_Main; {$R *.dfm}
//====================================================================

//--------------------------------------------------------------------
class procedure TdmAct_ColorSchema.Init;
//--------------------------------------------------------------------
begin
  Assert(not Assigned(dmAct_ColorSchema));

 // if not Assigned(dmAct_ColorSchema) then
  Application.CreateForm(TdmAct_ColorSchema, dmAct_ColorSchema);

//  dmAct_ColorSchema:=TdmAct_ColorSchema.Create(Application);
end;

//--------------------------------------------------------------------
procedure TdmAct_ColorSchema.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_COLOR_SCHEMA;

//  act_Import_ver1205.Caption:='������ �� XML (ver 1205)';
  act_ExportToxml.Caption:= STR_EXPORT_TO_XML;
  act_ImportFromxml.Caption:= STR_IMPORT_FROM_XML;
  act_Copy.Caption:= STR_ACT_COPY;

  act_Add.Caption :='������� �������� �����';

    act_Audit.Caption:=STR_Audit;


  act_Export_MDB.Caption:=DEF_STR_Export_MDB;


  SetActionsExecuteProc
           ([//act_Import_ver1205,
               act_ExportToXML,
               act_Copy,
               act_ImportFromXML,
               act_Export_MDB ,

               act_Audit
             ],
            DoAction);
end;

//--------------------------------------------------------------------
function TdmAct_ColorSchema.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_ColorSchema_add.ExecDlg (aFolderID);
end;

//--------------------------------------------------------------------
function TdmAct_ColorSchema.ItemDel(aID: integer; aName: string): boolean;
//--------------------------------------------------------------------
begin
  Result:=dmColorSchema.Del (aID);
end;

//--------------------------------------------------------------------
function TdmAct_ColorSchema.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_ColorSchema_View.Create(aOwnerForm);
end;


// ---------------------------------------------------------------
function TdmAct_ColorSchema.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
  Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];


  SetActionsEnabled1( [

      act_Copy,
      act_ImportFromXML,
      // act_Export_MDB,
      //                           act_Import_MDB,

      act_Copy,

      act_Del_,
      act_Del_list

   ],

   Result );


   //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );


end;

//--------------------------------------------------------------------
procedure TdmAct_ColorSchema.GetPopupMenu;
//--------------------------------------------------------------------
var
  bEnable: Boolean;
begin
  bEnable:=CheckActionsEnable();
 

  case aMenuType of
    mtFolder: begin
                AddFolderMenu_Create (aPopupMenu, bEnable);
//                AddMenuItem (aPopupMenu, act_Add);
                AddMenuItem (aPopupMenu, nil);
          //      AddMenuItem (aPopupMenu, act_Import_ver1205);
                AddMenuItem (aPopupMenu, act_ImportFromXML);
                AddFolderPopupMenu (aPopupMenu, bEnable);

                AddMenuItem (aPopupMenu, act_Export_MDB);

              end;
    mtItem:   begin
                AddMenuItem (aPopupMenu, act_ExportToXML);
                AddMenuItem (aPopupMenu, act_Copy);
                AddMenuItem (aPopupMenu, act_Del_);
                AddMenuItem (aPopupMenu, nil);
                AddFolderMenu_Tools (aPopupMenu);
                AddMenuItem (aPopupMenu, act_Export_MDB);

                AddMenuItem (aPopupMenu, act_Audit);

              end;
    mtList:   begin
               AddMenuItem (aPopupMenu, act_ExportToXML);
               AddMenuItem (aPopupMenu, act_Del_list);
               AddMenuItem (aPopupMenu, act_Export_MDB);
              end;
  end;

end;


//--------------------------------------------------------------------
procedure TdmAct_ColorSchema.Dlg_ImportFromXML(aFolderID: integer);
//--------------------------------------------------------------------
begin
  OpenDialog1.DefaultExt:='*.xml';
  OpenDialog1.Filter:=FILTER_XML;
  if OpenDialog1.Execute then
  begin
    dmColorSchema_import.LoadFromXML (OpenDialog1.FileName, aFolderID);

//    IDM
 //   SH_PostUpdateNodeChildren(FFocusedGUID);
  end;
end;

//--------------------------------------------------------------------
procedure TdmAct_ColorSchema.Dlg_ExportToXML(aArrID: TIntArray);
//--------------------------------------------------------------------
begin
  SaveDialog_xml.FileName:='ColorSchema';
  if SaveDialog_xml.Execute then
    dmColorSchema_export.SaveToXML_array (aArrID, SaveDialog_xml.FileName);

end;

//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_ColorSchema.DoAction (Sender: TObject);
var arrID: TIntArray;
  iID: integer;
  sNewName: string;
    i: integer;
begin

  if Sender=act_Audit then
     Init_IAuditX.ExecDlg( dmMain.GetConnectionString, TBL_LinkEnd, FFocusedID) else


  if Sender=act_Export_MDB then
    TDataExport_run.ExportToRplsDb_selected (TBL_ColorSchema, FSelectedIDList);


  if Sender=act_Copy then
  begin
    sNewName:=FFocusedName +' (�����)';
(*
//xx  if Sender=act_Copy then begin
  if InputQuery ('����� ������','������� ����� ��������', sNewName) then
  begin
//    iID:=dmCalcModel_.Copy (FFocusedID, sNewName);
    iID:=dmOnega_DB_data.CalcModel_Copy(FFocusedID, sNewName);

    g_ShellEvents.Shell_UpdateNodeChildren_ByGUID(GUID_CALC_MODEL);
 end;
*)
    if InputQuery ('����� ������','������� ����� ��������', sNewName) then
    begin
      iID:=dmColorSchema.Copy (FFocusedID, sNewName);
      g_Shell.UpdateNodeChildren_ByGUID(GUID_Color_Schema);

   end;
 end;

  //---------------------------
//  if Sender=act_Import_ver1205 then
 //   ImportFromXML_ver1205 (FFocusedID) else

  //---------------------------
  if Sender=act_ImportFromXML then
  begin
    Dlg_ImportFromXML (FFocusedID);
    g_Shell.UpdateNodeChildren_ByGUID(GUID_Color_Schema);

  end else

  //---------------------------
  if Sender=act_ExportToXML then
  begin
    with FSelectedPIDLs do
    for i:=0 to Count-1 do
      if Items[i].ObjectName=ObjectName then
        AddToIntArray(Items[i].ID, arrID);

    Dlg_ExportToXML(arrID);
  end;

//    Dlg_ExportToXML (FFocusedID);
end;


begin

end.
