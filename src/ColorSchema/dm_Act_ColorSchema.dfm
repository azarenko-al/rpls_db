inherited dmAct_ColorSchema: TdmAct_ColorSchema
  OldCreateOrder = True
  Left = 1008
  Top = 376
  Height = 409
  Width = 506
  inherited ActionList1: TActionList
    Left = 36
    Top = 192
    object act_ExportToXML: TAction
      Category = 'ColorSchema'
      Caption = 'act_ExportToXML'
    end
    object act_ImportFromXML: TAction
      Category = 'ColorSchema'
      Caption = 'act_ImportFromXML'
    end
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.xml'
    Filter = #1052#1072#1090#1088#1080#1094#1099' '#1074#1099#1089#1086#1090'|*.rlf;*.rlf2;*.rel'
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 40
    Top = 22
  end
  object SaveDialog_XML: TSaveDialog
    DefaultExt = 'xml'
    Filter = 'XML (*.xml)|*.xml'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 40
    Top = 76
  end
  object ActionList2: TActionList
    Left = 176
    Top = 24
    object act_Copy: TAction
      Caption = 'act_Copy'
    end
    object act_Export_MDB: TAction
      Caption = 'act_Export_MDB'
    end
    object act_Audit: TAction
      Caption = 'act_Audit'
    end
  end
end
