unit fr_ColorSchema_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Variants,
  Dialogs, ActnList, Menus, DBCtrls, StdCtrls, rxPlacemnt, ToolWin, 
  ComCtrls, ExtCtrls, Db, ADODB,    ImgList,
  cxControls, cxSplitter, cxGraphics, cxGridCustomTableView, cxGridDBTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView,
  cxGridCustomView, cxClasses, cxGridLevel, cxGrid, cxPropertiesStore,

  dm_User_Security,

  u_cx,

 // dxDBGrid,

  dm_Main,

  fr_View_base,
  dm_ColorSchema,

  fr_DBInspector_Container,

  u_dlg,
  u_db,
  u_img,
  u_func,
  u_reg,

  u_types,
  u_const_db,
  u_const,

  Grids, DBGrids, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinsDefaultPainters, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxCheckBox, cxSpinEdit, cxColorComboBox, cxTextEdit, dxBar, cxBarEditItem,
  cxButtonEdit

  ;


type
  Tframe_ColorSchema_View = class(Tframe_View_Base)
    FormStorage1: TFormStorage;
    qry_Ranges: TADOQuery;
    ds_Ranges: TDataSource;
    act_Add: TAction;
    act_Del: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    act_Check: TAction;
    act_UnCheck: TAction;
    N3: TMenuItem;
    actCheck1: TMenuItem;
    actUnCheck1: TMenuItem;
    pn_Inspector: TPanel;
    cxSplitter1: TcxSplitter;
  //  col_Comment: TdxDBGridColumn;
    cxGrid1: TcxGrid;
    cxGrid1DBBandedTableView1: TcxGridDBBandedTableView;
    cxGrid1DBBandedTableView1enabled: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1min_value: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1max_value: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1color: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1type: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1min_color: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1max_color: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1id: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Comment: TcxGridDBBandedColumn;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1DBColumn1: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBBandedTableView1DBBandedColumn1: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1DBBandedColumn2: TcxGridDBBandedColumn;
    cxStyleRepository2: TcxStyleRepository;
    cxStyle_Edit: TcxStyle;
    cxStyle_ReadOnly: TcxStyle;
    cxGrid1DBBandedTableView1Column1_color: TcxGridDBBandedColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure qry_RangesNewRecord(DataSet: TDataSet);

//    procedure dx_RangesDblClick(Sender: TObject);
    procedure act_CheckExecute(Sender: TObject);


    procedure qry_RangesAfterPost(DataSet: TDataSet);

//    procedure dx_RangesCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
//      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
//      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
//      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
//      var ADone: Boolean);

  private
    FUpdated : boolean;

//    FRegPath: string;
    Ffrm_DBInspector: Tframe_DBInspector_Container;
    procedure SetReadOnly(aValue: boolean);

  protected
(*    procedure Rollback; override;
    procedure Commit; override;
*)
  public
    procedure View(aID: integer; aGUID: string); override;
  end;


implementation

uses dm_Onega_db_data; {$R *.dfm}


//--------------------------------------------------------------------
procedure Tframe_ColorSchema_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

//  dx_Ranges.Align:=alClient;
 // pn_Inspector.Align:=alClient;

  ObjectName:=OBJ_COLOR_SCHEMA;

  TableName:=TBL_COLORSCHEMA;


  act_Check.Caption:='��������';
  act_UnCheck.Caption:='����� �������';


  CreateChildForm(Tframe_DBInspector_Container, Ffrm_DBInspector, pn_Inspector);
  Ffrm_DBInspector.PrepareViewForObject(OBJ_COLOR_SCHEMA);


//  FRegPath:=REGISTRY_FORMS + Name +'\'+ dx_Ranges.Name;

 // AddControl(dx_Ranges, [PROP_HEIGHT]);

  AddComponentProp(pn_Inspector, PROP_HEIGHT);

//  AddComponentProp(dx_Ranges, PROP_HEIGHT);
  cxPropertiesStore.RestoreFrom;

  cxGrid1.Align:=alClient;


  db_SetComponentADOConnection (Self, dmMain.ADOConnection);

//  dx_Ranges.LoadFromRegistry (FRegPath+ dx_Ranges.Name);


  cxGrid1DBBandedTableView1.RestoreFromRegistry(FRegPath+ cxGrid1DBBandedTableView1.Name);

//  col_ID.Visible := False;
end;


//--------------------------------------------------------------------
procedure Tframe_ColorSchema_View.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
 // dx_Ranges.SaveToRegistry (FRegPath+ dx_Ranges.Name);

  cxGrid1DBBandedTableView1.StoreToRegistry(FRegPath+ cxGrid1DBBandedTableView1.Name);

  inherited;
end;


//--------------------------------------------------------------------
procedure Tframe_ColorSchema_View.View(aID: integer; aGUID: string);
//--------------------------------------------------------------------
const
  SQL_SELECT_RANGES  =
    'SELECT * FROM '+ TBL_COLORSCHEMARANGES +
    ' WHERE color_schema_id=:id  ORDER BY min_value DESC';
var
  bReadOnly: Boolean;

begin
  inherited;

  if not RecordExists (aID) then
    Exit;


  bReadOnly:=dmUser_Security.Lib_Edit_ReadOnly();


  Ffrm_DBInspector.View (aID);
  Ffrm_DBInspector.SetReadOnly(bReadOnly);

  SetReadOnly(bReadOnly);

//  cxGrid1DBBandedTableView1.OptionsBe
//  ----------------------------


  qry_Ranges.DisableControls;

  FUpdated := False;
  dmOnega_DB_data.OpenQuery (qry_Ranges, SQL_SELECT_RANGES,  [FLD_ID, aID] );

    with qry_Ranges do
      while not EOF do
    begin
      if FieldValues[FLD_min_color]= 0 then
        db_UpdateRecord__(qry_Ranges,  [FLD_min_color, null]);

      if FieldValues[FLD_max_color]= 0 then
        db_UpdateRecord__(qry_Ranges,  [FLD_max_color, null]);


      Next;
    end;

  qry_Ranges.First;
  qry_Ranges.EnableControls;


end;


//--------------------------------------------------------------------
procedure Tframe_ColorSchema_View.qry_RangesNewRecord(DataSet: TDataSet);
//--------------------------------------------------------------------
begin
  Dataset[FLD_COLOR_SCHEMA_ID]:=ID;
end;


procedure Tframe_ColorSchema_View.qry_RangesAfterPost(DataSet: TDataSet);
begin
  FUpdated := True;
 // qry_Ranges.Refresh;
//  inherited;
end;

// ---------------------------------------------------------------
procedure Tframe_ColorSchema_View.SetReadOnly(aValue: boolean);
// ---------------------------------------------------------------
begin
  { check : !!! }
  cx_SetGridReadOnly (cxGrid1DBBandedTableView1, aValue);


  if aValue then
    cxGrid1DBBandedTableView1.Styles.BandBackground:=cxStyle_ReadOnly
  else
    cxGrid1DBBandedTableView1.Styles.BandBackground:=cxStyle_Edit



{  cxGrid1DBBandedTableView1.OptionsData.Editing:=aValue;
  cxGrid1DBBandedTableView1.OptionsSelection.CellSelect:=not aValue;
}
//  ToolBar2.Visible:=False;


end;


//------------------------------------------------------------------
procedure Tframe_ColorSchema_View.act_CheckExecute(Sender: TObject);
//------------------------------------------------------------------
begin
  //----------------------------
  if Sender=act_Add then
  //----------------------------
  begin
    qry_Ranges.Append;
    qry_Ranges[FLD_COLOR]:=img_GetRandomColor();
  end else

  //----------------------------
  if Sender=act_Del then
  //----------------------------
  begin
    if ConfirmDlg('�������?') then
      qry_Ranges.Delete;
  end;

 (* if Sender=act_Check then
    dx_CheckSelectedItems (dx_Ranges, FLD_ENABLED, True) else

  if Sender=act_UnCheck then
    dx_CheckSelectedItems (dx_Ranges, FLD_ENABLED, False)
*)

//
end;


end.


  {

procedure Tframe_ColorSchema_View.dx_RangesCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
var
  iType: Integer;
   mRect, mRectGrad: TRect;
   iMinColor,iMaxColor,iBackColor,mColor: integer;
begin
  //  exit;

//
//  if ((AColumn=col_color) or
//      (AColumn=col_Gradient_MinColor) or
//      (AColumn=col_Gradient_MaxColor))
//  then
//    if (AText <> '') then
//    begin
//      ACanvas.Brush.Color := clWindow;
//      ACanvas.FillRect(ARect);
//
//      mColor := StringToColor(AText);
//      mRect := ARect;
//      mRect.Top    := mRect.Top + 1;
//      mRect.Bottom := mRect.Bottom - 1;
//      mRect.Left   := mRect.Left + 1;
//      mRect.Right  := mRect.Right - 1;
//
//      mRectGrad := ARect;
//      mRectGrad.Top    := mRectGrad.Top + 2;
//      mRectGrad.Bottom := mRectGrad.Bottom - 2;
//      mRectGrad.Left   := mRectGrad.Left + 3;
//      mRectGrad.Right  := mRectGrad.Right - 3;
//
//
//      if aSelected or aFocused
//         then iBackColor:=clHighlight
//         else iBackColor:=clWindow;
//
//
//    //  iBackColor:=clWindow;
//
//      iType:=AsInteger(ANode.Values[col_type.Index]);
//
//      if AColumn=col_color then
//        with ANode do
//        begin
//          if iType=0 then
//            img_DrawColorRect_OnCanvas (ACanvas,  mRect, iBackColor, mColor, 1)
//          else begin
//            iMinColor:=AsInteger(Values[col_Gradient_MinColor.Index]);
//            iMaxColor:=AsInteger(Values[col_Gradient_MaxColor.Index]);
//
//            img_FillRectGradient_OnCanvas(ACanvas, mRectGrad, iMinColor, iMaxColor);
//          end;
//        end
//
//      else
//        if iType=1 then
//     //   if mColor<>0 then
//          img_DrawColorRect_OnCanvas (ACanvas,  mRect, iBackColor, mColor, 1);
//
//
//      ADone:=True;
//    end;

end;




//------------------------------------------------------------------
procedure Tframe_ColorSchema_View.dx_RangesDblClick(Sender: TObject);
//------------------------------------------------------------------
var
  iColor:integer;
  sFieldName: string;
begin

//  iColor:=dx_Ranges.FocusedColumn;
//
//  if dx_Ranges.FocusedAbsoluteIndex = col_color.Index then
//    sFieldName:=FLD_COLOR else
//  if dx_Ranges.FocusedAbsoluteIndex = col_Gradient_MinColor.Index then
//    sFieldName:=FLD_MIN_COLOR else
//  if dx_Ranges.FocusedAbsoluteIndex = col_Gradient_MaxColor.Index then
//    sFieldName:=FLD_MAX_COLOR
//  else
//    Exit;
//
//
//  with qry_Ranges do
//  begin
//    iColor:=dlg_SelectColor (FieldByName(sFieldName).AsInteger);
//
//    if iColor<>0 then
//      db_UpdateRecord (qry_Ranges,  [db_Par(sFieldName, iColor)]);
//  end;

end;
