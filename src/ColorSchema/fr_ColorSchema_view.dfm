inherited frame_ColorSchema_View: Tframe_ColorSchema_View
  Left = 1158
  Top = 350
  Width = 668
  Height = 692
  Caption = 'frame_ColorSchema_View'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 652
    Height = 61
  end
  inherited pn_Caption: TPanel
    Top = 88
    Width = 652
  end
  inherited pn_Main: TPanel
    Top = 105
    Width = 652
    Height = 329
    object pn_Inspector: TPanel
      Left = 1
      Top = 1
      Width = 650
      Height = 80
      Align = alTop
      BevelOuter = bvLowered
      Caption = 'pn_Inspector'
      TabOrder = 0
    end
    object cxSplitter1: TcxSplitter
      Left = 1
      Top = 81
      Width = 650
      Height = 8
      HotZoneClassName = 'TcxSimpleStyle'
      AlignSplitter = salTop
      Control = pn_Inspector
    end
    object cxGrid1: TcxGrid
      Left = 1
      Top = 183
      Width = 650
      Height = 145
      Align = alBottom
      TabOrder = 2
      LookAndFeel.Kind = lfFlat
      object cxGrid1DBBandedTableView1: TcxGridDBBandedTableView
        PopupMenu = PopupMenu1
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = ds_Ranges
        DataController.Filter.MaxValueListCount = 1000
        DataController.KeyFieldNames = 'id'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnPopup.MaxDropDownItemCount = 12
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsSelection.MultiSelect = True
        OptionsView.GroupByBox = False
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        OptionsView.Indicator = True
        Preview.AutoHeight = False
        Preview.MaxLineCount = 2
        Bands = <
          item
            Width = 239
          end
          item
            Caption = #1043#1088#1072#1076#1080#1077#1085#1090
            Width = 230
          end
          item
            Width = 175
          end>
        object cxGrid1DBBandedTableView1enabled: TcxGridDBBandedColumn
          Caption = #1042#1082#1083'.'
          DataBinding.FieldName = 'enabled'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ReadOnly = False
          MinWidth = 16
          Options.Filtering = False
          Options.Sorting = False
          Width = 35
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object cxGrid1DBBandedTableView1min_value: TcxGridDBBandedColumn
          Caption = '>= '#1052#1080#1085
          DataBinding.FieldName = 'min_value'
          PropertiesClassName = 'TcxSpinEditProperties'
          Options.Filtering = False
          Width = 53
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object cxGrid1DBBandedTableView1max_value: TcxGridDBBandedColumn
          Caption = '< '#1052#1072#1082#1089
          DataBinding.FieldName = 'max_value'
          PropertiesClassName = 'TcxSpinEditProperties'
          Options.Filtering = False
          Options.Sorting = False
          Width = 50
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object cxGrid1DBBandedTableView1color: TcxGridDBBandedColumn
          Caption = #1062#1074#1077#1090
          DataBinding.FieldName = 'color'
          PropertiesClassName = 'TcxColorComboBoxProperties'
          Properties.AllowSelectColor = True
          Properties.CustomColors = <>
          Properties.DropDownRows = 20
          Properties.MaxMRUColors = 20
          Properties.ReadOnly = False
          Properties.ShowDescriptions = False
          Properties.ValidateOnEnter = False
          Options.Filtering = False
          Options.FilteringMRUItemsList = False
          Options.Sorting = False
          Options.VertSizing = False
          Width = 61
          Position.BandIndex = 0
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object cxGrid1DBBandedTableView1type: TcxGridDBBandedColumn
          Caption = #1042#1082#1083'.'
          DataBinding.FieldName = 'type'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.NullStyle = nssUnchecked
          Properties.ReadOnly = False
          Properties.ValueChecked = '1'
          Properties.ValueUnchecked = '0'
          MinWidth = 16
          Options.Filtering = False
          Options.Sorting = False
          Width = 53
          Position.BandIndex = 1
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object cxGrid1DBBandedTableView1min_color: TcxGridDBBandedColumn
          Caption = #1052#1080#1085
          DataBinding.FieldName = 'min_color'
          PropertiesClassName = 'TcxColorComboBoxProperties'
          Properties.AllowSelectColor = True
          Properties.ColorBoxFrameColor = clWindow
          Properties.CustomColors = <>
          Properties.DefaultColorStyle = cxdcClear
          Properties.DropDownRows = 20
          Properties.ImmediateDropDown = False
          Properties.ReadOnly = False
          Properties.ShowDescriptions = False
          Options.Filtering = False
          Options.Grouping = False
          Options.Sorting = False
          Width = 82
          Position.BandIndex = 1
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object cxGrid1DBBandedTableView1max_color: TcxGridDBBandedColumn
          Caption = #1052#1072#1082#1089
          DataBinding.FieldName = 'max_color'
          PropertiesClassName = 'TcxColorComboBoxProperties'
          Properties.AllowSelectColor = True
          Properties.ColorBoxFrameColor = clWindow
          Properties.CustomColors = <>
          Properties.DropDownRows = 20
          Properties.ImmediateDropDown = False
          Properties.MaxMRUColors = 20
          Properties.ReadOnly = False
          Properties.ShowDescriptions = False
          Options.Filtering = False
          Options.Sorting = False
          Width = 95
          Position.BandIndex = 1
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object cxGrid1DBBandedTableView1id: TcxGridDBBandedColumn
          Tag = -1
          DataBinding.FieldName = 'id'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Visible = False
          Options.Filtering = False
          Options.Sorting = False
          Width = 20
          Position.BandIndex = 0
          Position.ColIndex = 4
          Position.RowIndex = 0
        end
        object cxGrid1DBBandedTableView1Comment: TcxGridDBBandedColumn
          Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
          DataBinding.FieldName = 'Comment'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Options.Filtering = False
          Options.Sorting = False
          Width = 69
          Position.BandIndex = 2
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object cxGrid1DBBandedTableView1DBBandedColumn1: TcxGridDBBandedColumn
          DataBinding.FieldName = 'color_schema_id'
          Visible = False
          Position.BandIndex = 0
          Position.ColIndex = 5
          Position.RowIndex = 0
        end
        object cxGrid1DBBandedTableView1DBBandedColumn2: TcxGridDBBandedColumn
          DataBinding.FieldName = 'min_color'
          Visible = False
          Position.BandIndex = 0
          Position.ColIndex = 6
          Position.RowIndex = 0
        end
        object cxGrid1DBBandedTableView1Column1_color: TcxGridDBBandedColumn
          DataBinding.FieldName = 'color'
          Visible = False
          Position.BandIndex = 0
          Position.ColIndex = 7
          Position.RowIndex = 0
        end
      end
      object cxGrid1DBTableView1: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        object cxGrid1DBTableView1DBColumn1: TcxGridDBColumn
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBBandedTableView1
      end
    end
  end
  inherited MainActionList: TActionList
    Left = 8
    Top = 0
    object act_Add: TAction [0]
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      OnExecute = act_CheckExecute
    end
    object act_Del: TAction [1]
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = act_CheckExecute
    end
    object act_Check: TAction [2]
      Caption = 'act_Check'
      OnExecute = act_CheckExecute
    end
    object act_UnCheck: TAction [3]
      Caption = 'act_UnCheck'
      OnExecute = act_CheckExecute
    end
  end
  inherited ImageList1: TImageList
    Left = 36
    Top = 0
  end
  inherited PopupMenu1: TPopupMenu
    Left = 164
    Top = 0
    object N1: TMenuItem
      Action = act_Add
    end
    object N2: TMenuItem
      Action = act_Del
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object actCheck1: TMenuItem
      Action = act_Check
    end
    object actUnCheck1: TMenuItem
      Action = act_UnCheck
    end
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 197
    Top = 0
  end
  inherited cxLookAndFeelController1: TcxLookAndFeelController
    Left = 232
    Top = 536
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    Top = 536
    DockControlHeights = (
      0
      0
      27
      0)
  end
  object cxStyleRepository2: TcxStyleRepository
    Left = 416
    Top = 536
    PixelsPerInch = 96
    object cxStyle_Edit: TcxStyle
    end
    object cxStyle_ReadOnly: TcxStyle
      AssignedValues = [svColor]
      Color = clInactiveBorder
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 92
  end
  object qry_Ranges: TADOQuery
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=SQL_ONEGA_111111'
    CursorType = ctStatic
    AfterPost = qry_RangesAfterPost
    OnNewRecord = qry_RangesNewRecord
    Parameters = <>
    SQL.Strings = (
      'select * from '
      'ColorSchemaRanges')
    Left = 409
    Top = 11
  end
  object ds_Ranges: TDataSource
    DataSet = qry_Ranges
    Left = 468
    Top = 9
  end
end
