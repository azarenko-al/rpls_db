program test_ColorSchema;

uses
  a_Unit in 'a_Unit.pas' {Form2},
  d_ColorSchema_add in '..\d_ColorSchema_add.pas' {dlg_ColorSchema_add},
  dm_Act_ColorSchema in '..\dm_Act_ColorSchema.pas' {dmAct_ColorSchema: TDataModule},
  dm_ColorSchema in '..\dm_ColorSchema.pas' {dmColorSchema: TDataModule},
  dm_ColorSchema_export in '..\dm_ColorSchema_export.pas' {dmColorSchema_export: TDataModule},
  dm_ColorSchema_import in '..\dm_ColorSchema_import.pas' {dmColorSchema_import: TDataModule},
  Forms,
  fr_ColorSchema_view in '..\fr_ColorSchema_view.pas' {frame_ColorSchema_View},
  fr_View_base in '..\..\Explorer\fr_View_base.pas' {frame_View_Base},
  ShareMem;

{$R *.RES}
//{$R ..\..\images.RES}

begin
  Application.Initialize;
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.
