unit dm_ColorSchema_import;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,

  dm_ColorSchema,

  XMLDoc, XMLIntf, u_xml_document,

  fr_ColorSchema_view,

  
  u_func,
  u_db,
  u_const_db
  ;

type
  TdmColorSchema_import = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
  private
  public
  //  procedure ImportFromXML_ver0512 (aFileName: string; aFolderID: integer);
    procedure LoadFromXML (aFileName: string; aFolderID: integer);

  end;

  function dmColorSchema_import: TdmColorSchema_import;

//================================================
implementation {$R *.DFM}
//================================================
var
  FdmColorSchema_import: TdmColorSchema_import;

const
  FLD_COLOR_SCHEME = 'COLOR_SCHEME';
  ATT_COLOR_SCHEMAS ='COLOR_SCHEMAS';

  ATT_MIN_VALUE = 'MIN_VALUE';
  ATT_MAX_VALUE = 'MAX_VALUE';
  ATT_MIN_COLOR = 'MIN_COLOR';
  ATT_MAX_COLOR = 'MAX_COLOR';
  ATT_IS_TRANSPARENT = 'IS_TRANSPARENT';



//-----------------------------------------------------
function dmColorSchema_import: TdmColorSchema_import;
//-----------------------------------------------------
begin
  if not Assigned(FdmColorSchema_import) then
    FdmColorSchema_import:=TdmColorSchema_import.Create(Application);

  Result := FdmColorSchema_import;
end;


procedure TdmColorSchema_import.DataModuleCreate(Sender: TObject);
begin

end;

//-------------------------------------------------------------------
procedure TdmColorSchema_import.LoadFromXML (aFileName: string; aFolderID: integer);
//-------------------------------------------------------------------
var
  vRoot,vNode,vNode1,vNode2 : IXmlNode;
  i,j, iSchemaID: Integer;
  recColorRange: TdmColorRangeParamRec;
  recSchema: TdmColorSchemaAddRec;
  v: Variant;

  oXMLDoc: TXMLDoc;
begin
  oXMLDoc:=TXMLDoc.Create;

  FillChar(recSchema, SizeOf(recSchema), 0);
  recSchema.FolderID:=aFolderID;


  oXMLDoc.LoadFromFile(aFileName);
  vRoot := oXMLDoc.DocumentElement;

  vNode:= xml_FindNode(vRoot, ATT_COLOR_SCHEMAS);
  for i := 0 to vNode.ChildNodes.Count - 1 do
  begin
    vNode1:= vNode.ChildNodes[i];

    if VarExists(vNode1) then
    begin
      recSchema.NewName   :=xml_GetAttr (vNode1, FLD_NAME);
      recSchema.Comment:=xml_GetAttr (vNode1, ATT_Comment);

      iSchemaID:= dmColorSchema.Add(recSchema);

      for j := 0 to vNode1.ChildNodes.Count - 1 do
      begin
        vNode2:= vNode1.ChildNodes[j];

        if VarExists(vNode2) then
          with recColorRange do
        begin
          SchemaID   := iSchemaID;

          v    := xml_GetAttr (vNode2, ATT_ENABLED);
          Enabled    := AsBoolean(v);
          Enabled    := AsBoolean(xml_GetAttr (vNode2, ATT_ENABLED));

          Color      := xml_GetIntAttr (vNode2, ATT_COLOR);
          Min_value  := AsFloat  (xml_GetAttr (vNode2, ATT_MIN_VALUE));
          Max_value  := AsFloat  (xml_GetAttr (vNode2, ATT_MAX_VALUE));
          Min_color  := xml_GetIntAttr (vNode2, ATT_MIN_COLOR);
          Max_color  := xml_GetIntAttr (vNode2, ATT_MAX_COLOR);
          Type_      := xml_GetIntAttr (vNode2, ATT_TYPE);
          Transparent:= AsBoolean(xml_GetAttr (vNode2, ATT_IS_TRANSPARENT));
          Comment    := AsString (xml_GetAttr (vNode2, ATT_Comment));
        end;

        dmColorSchema.AddRanges(recColorRange);
      end;

    end;
  end;

  oXMLDoc.Free;
end;


begin

end.


