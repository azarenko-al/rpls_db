unit fr_DBInspector_Container;

interface

uses
  Windows, Messages, ExtCtrls, Controls, Classes,Forms,menus,
  Dialogs, SysUtils, cxVGrid, DB,

  I_Shell,

  fr_DBInspector,

  u_Inspector_common,

  u_Explorer,

  u_const_db,
  u_Types,


  i_Inspector,

  u_log,
  u_func,
  u_db;

type


  Tframe_DBInspector_Container = class(TForm, IDBInspector)
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FOnButtonClick: TOnFieldChangedEvent;
    FOnFieldChanged: TOnFieldChangedEvent;
    FOnPost: TNotifyEvent;

  private
    FDBInspector: Tframe_DBInspector;
  public
    procedure DoOnPost             (Sender: TObject); virtual;
    procedure DoOnFieldChanged     (Sender: TObject; aFieldName: string; aNewValue: Variant); virtual;
    procedure DoOnButtonFieldClick (Sender: TObject; aFieldName: string; aNewValue: Variant); virtual;
    procedure DoOnRename           (Sender: TObject; aGUID,aNAME: string);
  public
    ID: Integer;
    ObjectName: string;

    function GetDataSet: TDataSet;

    procedure View (aID: integer); virtual;
    procedure Post();

    procedure PrepareViewForObject(aObjectName: string; aObjectName2: string = '');

    procedure SetFieldReadOnly(aFieldName: string; aValue: boolean);


    function  GetFieldValue (aFieldName: string): Variant;
    function  GetIntFieldValue (aFieldName: string): Integer;
    function GetBooleanFieldValue(aFieldName: string): Boolean;
    function  GetStrFieldValue (aFieldName: string): string;
    function  GetFloatFieldValue(aFieldName: string): double;

    function GetInspectorRowByFieldName(aFieldName: string): TcxCustomRow;


    procedure ExpandRowByFieldName(aFieldName: string; aVisible : boolean);
//    procedure CollapseRowByFieldName (aFieldName: string);

 //   procedure CollapseRowByAlias(aAlias: string);
    procedure ExpandRowByAlias(aAlias: string; aVisible : boolean);

    procedure HideEdit;
    procedure SetRowCaptionByFieldName(aFieldName,aCaption: string);

    procedure SetMinMaxValues(aFieldName: string; aMin, aMax: Integer);

    procedure SetRowVisibleByFieldName(aFieldName: string; aVisible : boolean);

    procedure SetFieldValue(aFieldName: string; aValue: Variant; aIsAddToList:
        boolean = true);

 //   procedure SetFieldValue_new(const aFieldName: WideString; aValue: OleVariant);

    procedure SetFieldValue_new(const aFieldName: WideString; aValue: OleVariant);
        safecall;


    procedure SetReadOnly(aValue: boolean; aMsg: string = '');

    procedure SetAllowed(aValue: boolean);
    procedure SetColor_Blank(aFieldName: string);
    procedure SetColor_Yellow(aFieldName: string);


    procedure SetRowVisibleByAlias(aAlias: string; aVisible : boolean);
    procedure SetStrFieldValue(aFieldName: string; aValue: string; aIsAddToList: boolean = true);

    property OnFieldChanged: TOnFieldChangedEvent read FOnFieldChanged write FOnFieldChanged;
    property OnButtonClick: TOnFieldChangedEvent read FOnButtonClick write FOnButtonClick;
    property OnPost: TNotifyEvent read FOnPost write FOnPost;

  end;


implementation
{$R *.DFM}


//--------------------------------------------------------------------
procedure Tframe_DBInspector_Container.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  FDBInspector:=Tframe_DBInspector.Create(Self);

  FDBInspector.ManualDock(Self);
  FDBInspector.Align:=alClient;
  FDBInspector.Show;

  FDBInspector.OnRename:=DoOnRename;
  FDBInspector.OnFieldChanged:=DoOnFieldChanged;
  FDBInspector.OnButtonClick:=DoOnButtonFieldClick;

  FDBInspector.OnPost:=DoOnPost;
end;


//--------------------------------------------------------------------
procedure Tframe_DBInspector_Container.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  FDBInspector.Post;   // !!!!!!!!!

  FreeAndNil(FDBInspector);

  inherited;
end;


procedure Tframe_DBInspector_Container.View(aID: integer);
begin
  ID:=aID;

  FDBInspector.View (aID);

end;


procedure Tframe_DBInspector_Container.Post;
begin
  FDBInspector.Post;
end;


procedure Tframe_DBInspector_Container.PrepareViewForObject(aObjectName:
    string; aObjectName2: string = '');
begin
  ObjectName:=aObjectName;
//  ObjectName2:=aObjectName2;
  FDBInspector.PrepareViewForObject(aObjectName, aObjectName2);
end;


function Tframe_DBInspector_Container.GetFieldValue(aFieldName: string): Variant;
begin
  Result:= FDBInspector.GetFieldValue(aFieldName);
end;


procedure Tframe_DBInspector_Container.SetFieldValue(aFieldName: string;
    aValue: Variant; aIsAddToList: boolean = true);
begin
  FDBInspector.SetFieldValue(aFieldName,aValue,aIsAddToList);

  g_log.Add('SetFieldValue '+ aFieldName );

end;


procedure Tframe_DBInspector_Container.SetFieldValue_new(const aFieldName:
    WideString; aValue: OleVariant);
begin
  FDBInspector.SetFieldValue(aFieldName,aValue);
end;


procedure Tframe_DBInspector_Container.SetStrFieldValue(aFieldName: string;
    aValue: string; aIsAddToList: boolean = true);
begin
  FDBInspector.SetStrFieldValue(aFieldName,aValue,aIsAddToList);
end;


function Tframe_DBInspector_Container.GetFloatFieldValue(aFieldName: string): double;
begin
  Result:= FDBInspector.GetFloatFieldValue(aFieldName);
end;


function Tframe_DBInspector_Container.GetBooleanFieldValue(aFieldName: string):
    Boolean;
begin
  Result := FDBInspector.GetBooleanFieldValue(aFieldName);
end;


function Tframe_DBInspector_Container.GetIntFieldValue(aFieldName: string): Integer;
begin
  Result:= FDBInspector.GetIntFieldValue(aFieldName);
end;

function Tframe_DBInspector_Container.GetStrFieldValue( aFieldName: string): string;
begin
  Result:= FDBInspector.GetStrFieldValue( aFieldName);
end;

(*
procedure Tframe_DBInspector_Container.CollapseRowByFieldName(aFieldName: string);
begin
  FDBInspector.CollapseRowByFieldName(aFieldName);
end;
*)


procedure Tframe_DBInspector_Container.ExpandRowByFieldName(aFieldName: string;
    aVisible : boolean);
begin
  FDBInspector.ExpandRowByFieldName(aFieldName, aVisible);
end;


procedure Tframe_DBInspector_Container.ExpandRowByAlias(aAlias: string;
    aVisible : boolean);
begin
  FDBInspector.ExpandRowByAlias(aAlias, aVisible);
end;


procedure Tframe_DBInspector_Container.SetRowCaptionByFieldName(aFieldName,aCaption: string);
begin
  FDBInspector.SetRowCaptionByFieldName (aFieldName,aCaption);
end;


function Tframe_DBInspector_Container.GetInspectorRowByFieldName(aFieldName:
    string): TcxCustomRow;
begin
  Result := FDBInspector.GetInspectorRowByFieldName(aFieldName);
end;


procedure Tframe_DBInspector_Container.SetRowVisibleByFieldName(aFieldName: string;
    aVisible : boolean);
//--------------------------------------------------------------------
begin
  FDBInspector.SetRowVisibleByFieldName(aFieldName,aVisible);
end;


procedure Tframe_DBInspector_Container.SetRowVisibleByAlias(aAlias: string;
    aVisible : boolean);
begin
  FDBInspector.SetRowVisibleByAlias(aAlias,aVisible);
end;



procedure Tframe_DBInspector_Container.SetFieldReadOnly(aFieldName: string;
    aValue: boolean);
begin
  FDBInspector.SetFieldReadOnly (aFieldName,aValue);
end;


procedure Tframe_DBInspector_Container.SetReadOnly(aValue: boolean; aMsg:
    string = '');
begin
  FDBInspector.SetReadOnly(aValue,aMsg);
end;


procedure Tframe_DBInspector_Container.SetAllowed(aValue: boolean);
begin
  FDBInspector.SetAllowed(aValue);
end;



procedure Tframe_DBInspector_Container.DoOnButtonFieldClick(Sender: TObject; aFieldName: string; aNewValue: Variant);
begin
  if Assigned(FOnButtonClick) then
    FOnButtonClick(Sender,aFieldName,aNewValue);
end;

procedure Tframe_DBInspector_Container.DoOnFieldChanged(Sender: TObject; aFieldName: string; aNewValue: Variant);
begin
  if Assigned(FOnFieldChanged) then
    FOnFieldChanged(Sender,aFieldName,aNewValue);
end;

procedure Tframe_DBInspector_Container.HideEdit;
begin
  FDBInspector.cxDBVerticalGrid1.HideEdit;
end;


procedure Tframe_DBInspector_Container.SetMinMaxValues(aFieldName: string; aMin, aMax: Integer);
begin
  FDBInspector.SetMinMaxValues1(aFieldName, aMin, aMax);
end;


procedure Tframe_DBInspector_Container.SetColor_Yellow(aFieldName: string);
begin
  FDBInspector.SetColor_Yellow(aFieldName);
end;


procedure Tframe_DBInspector_Container.SetColor_Blank(aFieldName: string);
begin
  FDBInspector.SetColor_Blank(aFieldName);
end;


//-------------------------------------------------------------------
//
//-------------------------------------------------------------------
procedure Tframe_DBInspector_Container.DoOnPOst(Sender: TObject);
var
  ot: TrpObjectType;
begin
  ot:= g_Obj.ItemByName[ObjectName].Type_;

(*
  if FoundInMapObjects(ot) then
  begin
    if Assigned(IMapEngine) then
      dmMapEngine1.ReCreateObject(ot, ID);

//    PostEvent(WE_MAP_RECREATE_OBJECT,
 //    [app_Par(PAR_ObjectType, ot),
  //  zz    app_Par(PAR_ID,         ID1) ]);
  //  Assert(Assigned(IMapAct));


 /////////   if Assigned(IMapAct) then
//////      dmAct_Map.MapRefresh;

 //   dmAct_Map.MapRefresh;
  end;
*)

  if Assigned(FOnPost) then
    FOnPost(Sender);

end;


procedure Tframe_DBInspector_Container.DoOnRename(Sender: TObject; aGUID, aNAME: string);
begin
  g_ShellEvents.Shell_RENAME_ITEM (aGUID, aNAME);
end;


function Tframe_DBInspector_Container.GetDataSet: TDataSet;
begin
//  Result := FDBInspector.mem_Data;
  Result := FDBInspector.qry_Data;

end;



end.




// TODO: GetFieldInfoByFieldName1111111
//function Tframe_DBInspector_Container.GetFieldInfoByFieldName1111111(
//  aFieldName: string): TViewEngFieldInfo;
//begin
// ///// Result:=  FDBInspector.GetFieldInfoByFieldName (aFieldName);
//end;
