unit d_expl_Object_Select;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls,  ActnList, StdCtrls,    RXSplit,
  cxPropertiesStore, rxPlacemnt, DB, ADODB, cxInplaceContainer,
  cxTL, cxDBTL, cxTLData, ImgList, cxControls, cxSplitter, Variants,


  dm_Main,

  dm_Onega_DB_data,

  d_Wizard,

 // dm_Explorer,
 // dm_Folder,

  fr_Explorer,
  fr_Browser,

  u_Explorer,
  u_types,
  u_const,
  u_const_db,
  u_const_str,

  u_Log,
  u_func,
  u_reg,
  u_db,
  u_cx,
  u_dlg,

//  u_func_msg

  cxGraphics, Menus, cxStyles, ToolWin, ComCtrls, cxLookAndFeels,
  cxLookAndFeelPainters, cxCustomData, cxMaskEdit, cxTLdxBarBuiltInMenu,
  dxSkinsCore, dxSkinsDefaultPainters, Grids, DBGrids;


type
  Tdlg_expl_Object_Select = class(Tdlg_Wizard)
    pn_Main: TPanel;
    pn_Tree: TPanel;
    pn_Browser: TPanel;
    Button1: TButton;
    act_Hide: TAction;
    cxSplitter1: TcxSplitter;
    DataSource1: TDataSource;
    FolderImageList1: TImageList;
    cxDBTreeList1: TcxDBTreeList;
    col_name: TcxDBTreeListColumn;
    col_is_folder: TcxDBTreeListColumn;
    cxDBTreeList1cxDBTreeListColumn3: TcxDBTreeListColumn;
    col_id: TcxDBTreeListColumn;
    col_guid: TcxDBTreeListColumn;
    col_parent_guid: TcxDBTreeListColumn;
    ActionList2: TActionList;
    act_Customize: TAction;
    ADOStoredProc1: TADOStoredProc;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_Bold: TcxStyle;
    col_property_name: TcxDBTreeListColumn;
    ToolBar1: TToolBar;
    ed_Search: TEdit;
    Button3: TButton;
    Button2: TButton;
    act_Search: TAction;
    act_Search_next: TAction;
    DBGrid1: TDBGrid;
    procedure act_CustomizeExecute(Sender: TObject);
    procedure act_SearchExecute(Sender: TObject);
    procedure act_Search_nextExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
//    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
 //   procedure act_HideExecute(Sender: TObject);
  //  procedure cxDBTreeList1CustomDrawCell(Sender: TObject; ACanvas: TcxCanvas; AViewInfo:
    //    TcxTreeListEditCellViewInfo; var ADone: Boolean);
    procedure cxDBTreeList1SelectionChanged(Sender: TObject);
    procedure cxDBTreeList1StylesGetBandContentStyle(Sender,
      AItem: TObject; ANode: TcxTreeListNode; var AStyle: TcxStyle);
    procedure cxDBTreeList1StylesGetContentStyle(Sender: TcxCustomTreeList;
      AColumn: TcxTreeListColumn; ANode: TcxTreeListNode;
      var AStyle: TcxStyle);
  private
    Fframe_Browser : Tframe_Browser;

    FNodeFound: TcxTreeListNode;


    FObjectName : string;

    FFocused_ID : Integer;
    FFocused_Name : string;

    FId: integer;
    FName: string;

    procedure DoOnObjectFocused (aObjectName: string; aID: integer; aGUID,aName: string);
  public
    class function ExecDlg1 (aType: TrpObjectType;
                            var aID: integer;
                            var aName: WideString): Boolean; overload;
  end;



implementation

uses
  AutoAdap;

{$R *.DFM}

//--------------------------------------------------------------
class function Tdlg_expl_Object_Select.ExecDlg1 (aType: TrpObjectType;
                                                var aID: integer;
                                                var aName: WideString): boolean;
//--------------------------------------------------------------
var sObjectName: string;
  i: Integer;
  b: boolean;
  k: Integer;
begin
  with Tdlg_expl_Object_Select.Create(Application) do
  try
    sObjectName:=g_Obj.ItemByType[aType].Name;

//    FRegPath:=FRegPath + sObjectName + '\';

    FObjectName :=sObjectName;


    ADOStoredProc1.AfterScroll := nil;

    cxDBTreeList1.OnSelectionChanged := nil;

//    AdoQuery1.DisableControls;

    k:=dmOnega_DB_data.Explorer_Select_Object(ADOStoredProc1, sObjectName, dmMain.ProjectID);


 //   db_View(ADOStoredProc1);


    b:= Eq(sObjectName, OBJ_LINK_Repeater);

//    b:=Assigned(ADOStoredProc1.FindField(FLD_PROPERTY_NAME));

    col_property_name.Visible := b;


    cxdbTreeList1.FullCollapse;

    cxDBTreeList1.OnSelectionChanged := cxDBTreeList1SelectionChanged;

    b:=ADOStoredProc1.Locate(FLD_ID+';'+FLD_IS_FOLDER, VarArrayOf([aID,False]), []);

    cxDBTreeList1SelectionChanged(nil);


//    b:=AdoQuery1.Locate(FLD_ID+';'+FLD_IS_FOLDER, VarArrayOf([aID,null]), []);

 //   AdoQuery1.AfterScroll := ADOQuery1AfterScroll;

//    AdoQuery1.EnableControls;


(*    if b then
      ADOQuery1AfterScroll(AdoQuery1);
*)


    Result:= (ShowModal = mrOK);

    if Result then
    begin
      Assert(FFocused_ID>0, 'FFocused_ID <=0');

      aID   := FFocused_ID;
      aName := FFocused_name;
    end;

  finally
    Free;
  end;
end;



//--------------------------------------------------------------
procedure Tdlg_expl_Object_Select.FormCreate(Sender: TObject);
//--------------------------------------------------------------
begin
  inherited;

  cxdbTreeList1.Align:=alClient;

  pn_Main.Align:=alClient;
  pn_Browser.Align:=alClient;

  cxDBTreeList1.Align:=alClient;

  cxDBTreeList1.DataController.KeyField := FLD_GUID;
  cxDBTreeList1.DataController.ParentField := FLD_PARENT_GUID;
  cxDBTreeList1.DataController.ImageIndexField := FLD_FOLDER_IMAGE_INDEX;

  col_name.Caption.text:=STR_NAME;
  col_property_name.Caption.text:=STR_property;

  Caption:='����� �������';

  CreateChildForm(Tframe_Browser, Fframe_Browser, pn_Browser);

  AddComponentProp(pn_Tree, PROP_WIDTH);
  RestoreFrom();

  act_Search.Caption:='�����';
  act_Search_next.Caption:='����';

end;


procedure Tdlg_expl_Object_Select.DoOnObjectFocused(aObjectName: string; aID: integer; aGUID,aName:
    string);
begin
  FID:=aID;
  FName:=aName;
end;



procedure Tdlg_expl_Object_Select.cxDBTreeList1SelectionChanged(Sender: TObject);
var
  b: boolean;
  v: Variant;
  sName: string;
  iID: integer;
  oNode: TcxTreeListNode;
begin
  g_Log.SysMsg ('cxDBTreeList1SelectionChanged');

  if cxDBTreeList1.SelectionCount=0 then
    Exit;

  oNode := cxDBTreeList1.Selections[0];


//  b:=oNode.Values[col_is_folder.ItemIndex];
  v:=oNode.Values[col_is_folder.ItemIndex];

//  sObjectName:=DataSet.FieldByName(FLD_OBJNAME).AsString;
  sName:=oNode.Values[col_Name.ItemIndex];
  iID:=oNode.Values[col_ID.ItemIndex];

  b:=AsBoolean(v);
  if not b then
  begin
    Fframe_Browser.ViewObjectByID (FObjectName, iID);

    FFocused_ID :=iID;
    FFocused_Name :=sName;

  end;

//  ShowMessage('cxDBTreeList1SelectionChanged');
end;


procedure Tdlg_expl_Object_Select.act_CustomizeExecute(Sender: TObject);
begin
 // if Sender=act_Customize then
  //  cx_DBTreeList_Customizing(cxDBTreeList1);

end;

// ---------------------------------------------------------------
procedure Tdlg_expl_Object_Select.act_SearchExecute(Sender: TObject);
// ---------------------------------------------------------------
var
  s: string;
 // oNode: TcxTreeListNode;
begin
  s:=Trim(ed_Search.Text);

  FNodeFound:=cxDBTreeList1.FindNodeByText(s, col_name);
  if Assigned(FNodeFound) then
    FNodeFound.Focused := True;
end;

// ---------------------------------------------------------------
procedure Tdlg_expl_Object_Select.act_Search_nextExecute(Sender: TObject);
// ---------------------------------------------------------------
var
  s: string;
  oNode: TcxTreeListNode;
begin
  if not Assigned(FNodeFound) then
    exit;


  Assert(Assigned(FNodeFound));

  s:=Trim(ed_Search.Text);

//  FNodeFound.Parent
  oNode:=FNodeFound.GetNext;
  if not Assigned(oNode) then
   Exit;


  FNodeFound:=cxDBTreeList1.FindNodeByText(s, col_name, oNode);
  if Assigned(FNodeFound) then
    FNodeFound.Focused := True;

//  cxDBTreeList1.fFindNext(True);
//
//
//  oNode:=cxDBTreeList1.FindNext(True);
//  if Assigned(oNode) then
//    oNode.Focused := True;

end;


procedure Tdlg_expl_Object_Select.cxDBTreeList1StylesGetBandContentStyle(
  Sender, AItem: TObject; ANode: TcxTreeListNode; var AStyle: TcxStyle);
var
  v: Variant;
begin
//  v := ANode.Values[col_Is_Folder.ItemIndex];
//
//  if v=True then
//    AStyle:=cxStyle_Bold;

//    ACanvas.Font.Style:=ACanvas.Font.Style + [fsBold];

end;




procedure Tdlg_expl_Object_Select.cxDBTreeList1StylesGetContentStyle(
  Sender: TcxCustomTreeList; AColumn: TcxTreeListColumn;
  ANode: TcxTreeListNode; var AStyle: TcxStyle);
var
  v: Variant;
begin
  v := ANode.Values[col_Is_Folder.ItemIndex];

  if v=True then
    AStyle:=cxStyle_Bold;

end;

end.

