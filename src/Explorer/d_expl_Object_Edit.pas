unit d_expl_Object_Edit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, rxPlacemnt, ActnList, StdCtrls, ExtCtrls,
//  ADODB,

  u_types,
  d_Wizard,
  u_func,
  u_db,

  fr_DBInspector_Container,


  cxPropertiesStore;


type
  Tdlg_Object_Edit = class(Tdlg_Wizard)
    pn_Main: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
  private
 //   FQuery: TADOQuery;
    Ffrm_DBInspector: Tframe_DBInspector_Container;

  public
    class function ExecDlg (aObjectName: string;
                             aID: integer): boolean;
  end;

//====================================================================
implementation {$R *.dfm}
//====================================================================
 

//--------------------------------------------------------------------
class function Tdlg_Object_Edit.ExecDlg (
                        aObjectName: string;
                        aID        : integer
                        ): boolean;
//--------------------------------------------------------------------
begin
  with Tdlg_Object_Edit.Create(Application) do
  try
    Ffrm_DBInspector.PrepareView (aObjectName);
    Ffrm_DBInspector.View (aID);
//    Ffrm_DBInspector.Options.AutoCommit:=False;

    Result:=(ShowModal=mrOK);
    if Result then
      Ffrm_DBInspector.Post;

  finally
    Free;
  end;

end;

//--------------------------------------------------------------------
procedure Tdlg_Object_Edit.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  Caption:='������';

  pn_Main.Align:=alClient;

  CreateChildForm (Tframe_DBInspector_Container, Ffrm_DBInspector, pn_Main);

 // Ffrm_DBInspector:=Tframe_DBInspector_Container.Create(Self);
 // CopyControls(Ffrm_DBInspector, pn_Main);

end;


procedure Tdlg_Object_Edit.act_OkExecute(Sender: TObject);
begin
  inherited;
  //
end;


end.
