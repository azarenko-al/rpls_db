object frame_Browser: Tframe_Browser
  Left = 543
  Top = 525
  Width = 589
  Height = 289
  Caption = 'frame_Browser'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 120
  TextHeight = 16
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 581
    Height = 113
    ActivePage = p_Main
    Align = alTop
    Style = tsFlatButtons
    TabOrder = 0
    object p_Main: TTabSheet
      Caption = 'p_Main'
      object Panel1: TPanel
        Left = 560
        Top = 29
        Width = 13
        Height = 50
        Align = alRight
        Color = clActiveCaption
        TabOrder = 0
        Visible = False
      end
      object ToolBar1: TToolBar
        Left = 0
        Top = 0
        Width = 573
        Height = 29
        Caption = 'ToolBar1'
        TabOrder = 1
        Visible = False
        object Button1: TButton
          Left = 0
          Top = 2
          Width = 75
          Height = 22
          Caption = 'Button1'
          TabOrder = 0
          OnClick = Button1Click
        end
      end
    end
    object p_Blank: TTabSheet
      Caption = 'p_Blank'
      ImageIndex = 1
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    IniFileName = 'Software\Onega\'
    Left = 44
    Top = 132
  end
end
