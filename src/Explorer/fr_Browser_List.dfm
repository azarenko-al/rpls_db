inherited frame_Browser_List: Tframe_Browser_List
  Left = 898
  Top = 266
  Width = 807
  Height = 569
  Caption = 'frame_Browser_List'
  OldCreateOrder = True
  OnClose = nil
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 791
    Height = 113
  end
  inherited pn_Caption: TPanel
    Top = 141
    Width = 791
  end
  object StatusBar1: TStatusBar [2]
    Left = 0
    Top = 511
    Width = 791
    Height = 19
    Panels = <
      item
        Text = 'Count: '
        Width = 80
      end
      item
        Width = 50
      end>
  end
  inherited pn_Main: TPanel
    Top = 187
    Width = 791
    Height = 181
    Anchors = [akLeft, akTop, akRight, akBottom]
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 789
      Height = 145
      Align = alTop
      TabOrder = 0
      LookAndFeel.Kind = lfFlat
      object cxGrid1DBTableView1: TcxGridDBTableView
        PopupMenu = PopupMenu1
        OnDblClick = cxDBGrid11DblClick
        OnKeyDown = cxDBGrid11KeyDown
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = ds_Data
        DataController.KeyFieldNames = 'id'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.ImmediateEditor = False
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.MultiSelect = True
        OptionsView.Indicator = True
        object col__ID: TcxGridDBColumn
          DataBinding.FieldName = 'id'
          Visible = False
          VisibleForCustomization = False
        end
        object col__guid: TcxGridDBColumn
          DataBinding.FieldName = 'guid'
          Visible = False
          VisibleForCustomization = False
        end
        object col__recNo: TcxGridDBColumn
          DataBinding.FieldName = 'recno'
          OnGetDataText = col__recNoGetDataText
          VisibleForCustomization = False
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object ToolBar1: TToolBar [4]
    Left = 0
    Top = 158
    Width = 791
    Height = 29
    ButtonHeight = 21
    Caption = 'ToolBar1'
    TabOrder = 4
    object ed_Search: TEdit
      Left = 0
      Top = 2
      Width = 241
      Height = 21
      TabOrder = 0
    end
    object ToolButton1: TToolButton
      Left = 241
      Top = 2
      Width = 8
      Caption = 'ToolButton1'
      Style = tbsSeparator
    end
    object Button1: TButton
      Left = 249
      Top = 2
      Width = 64
      Height = 21
      Caption = #1055#1086#1080#1089#1082
      TabOrder = 1
      OnClick = Button1Click
    end
  end
  inherited MainActionList: TActionList
    Left = 8
    Top = 4
    object act_Filter_Open: TAction
      Category = 'Filter'
      Caption = 'setup'
      OnExecute = _Actions
    end
    object act_SaveAsExcel: TAction
      Category = 'Export'
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' Excel'
      OnExecute = _Actions
    end
    object act_Filter_Clear: TAction
      Category = 'Filter'
      Caption = 'Filter_Clear'
      OnExecute = _Actions
    end
    object act_Column_customize: TAction
      Caption = 'Column_customize'
      OnExecute = _Actions
    end
    object act_All_Folders: TAction
      Category = 'Filter'
      Caption = 'All Folders'
      OnExecute = _Actions
    end
    object act_Search: TAction
      Caption = #1055#1086#1080#1089#1082
      OnExecute = _Actions
    end
  end
  inherited ImageList1: TImageList
    Left = 36
    Top = 4
  end
  inherited PopupMenu1: TPopupMenu
    OnPopup = PopupMenu1Popup
    Left = 92
    Top = 4
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Top = 4
  end
  inherited cxLookAndFeelController1: TcxLookAndFeelController
    Left = 432
    Top = 424
  end
  inherited dxBarManager1: TdxBarManager
    AlwaysSaveText = True
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    Left = 64
    Top = 400
    DockControlHeights = (
      0
      0
      28
      0)
    inherited dxBarManager1Bar1: TdxBar
      OneOnRow = False
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 524
      Visible = True
    end
    object dxBarManager1Bar3: TdxBar [2]
      AllowQuickCustomizing = False
      Caption = 'Custom 3'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 88
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 1378
      FloatTop = 666
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 43
          Visible = True
          ItemName = 'cb_show_list'
        end
        item
          BeginGroup = True
          UserDefine = [udWidth]
          UserWidth = 39
          Visible = True
          ItemName = 'cb_Filter_All_Folders_1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    inherited cb_Filter_All_Folders_1: TcxBarEditItem
      OnChange = cb_Filter_All_Folders_1Change
    end
    inherited dxBarButton3: TdxBarButton
      Action = act_SaveAsExcel
    end
    inherited ed_Filter: TcxBarEditItem
      Properties.OnButtonClick = ed_FilterPropertiesButtonClick
    end
    object cb_show_list: TcxBarEditItem
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1089#1086#1076#1077#1088#1078#1080#1084#1086#1077' '#1087#1072#1087#1082#1080
      Category = 0
      Hint = #1055#1086#1082#1072#1079#1072#1090#1100' '#1089#1086#1076#1077#1088#1078#1080#1084#1086#1077' '#1087#1072#1087#1082#1080
      Visible = ivAlways
      OnChange = cb_show_listChange
      ShowCaption = True
      Width = 100
      OnClick = cb_show_listClick
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.ImmediatePost = True
      Properties.NullStyle = nssUnchecked
      InternalEditValue = 'False'
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 560
    Top = 8
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clYellow
    end
  end
  object FormStorage1: TFormStorage
    Active = False
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 208
    Top = 400
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '*.xls'
    Filter = 'xls|*.xls'
    Left = 120
    Top = 4
  end
  object TimerOnDblClick: TTimer
    Enabled = False
    Interval = 100
    OnTimer = TimerOnDblClickTimer
    Left = 464
    Top = 8
  end
  object ds_Data: TDataSource
    DataSet = sp_Data
    Left = 292
    Top = 56
  end
  object sp_Data: TADOStoredProc
    Parameters = <>
    Left = 288
  end
end
