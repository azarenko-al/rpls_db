unit dm_ViewEngine;

interface
//{$I ver.inc}

uses
  SysUtils, Classes, DB, ADODB, Forms,RxMemDS, Controls, dxmdaset, Dialogs, Variants,

//  CK42_to_GSK_2011,

  u_dll_geo_convert,

  dm_Onega_DB_data,

  u_geo_convert_new,
//  u_geo_convert,
//  u_geo_convert_2017,

    //u_geo_convert_new,

  u_ViewEngine_classes,

  dm_Main,

  dm_DBManager,

  u_Inspector_common,

  u_cx,
  u_db,
  u_func,
  u_Geo,
  u_log,

  u_types,

  u_const_db
 ;


type


  TdmViewEngineOpenDatasetRec = record
    ObjectType:   TrpObjectType;
    FolderID:     integer;
  //  FilterWhere111:  string;
    IsAllFolders: boolean;
  //  FieldNames:   TStrings
  end;


  //-------------------------------------------------------------------
  TdmViewEngine = class(TDataModule)

    ADOTable1: TADOTable;
    ADOQuery1: TADOQuery;
    qry_Status: TADOQuery;

    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
   // FViewSQL : string;


    procedure DoOnCalcFields(DataSet: TDataSet);
    function GetCoordStr(aBLPoint: TBLPoint): string;

//aFilterWhere: string;
    function GetWhereSQL(aObjectType: TrpObjectType; aIsAllFolders: boolean;
        aProjectID: integer=0): string;


//    procedure LoadData1111111111;


  public
    ObjectList: TViewEngineObjectList;


    procedure Clear;
    function CreateFieldInfo(aViewField: TViewField): TViewEngFieldInfo;


    function GetTableSQLForFields(aObjectName: string; aViewObject:
        TViewEngineObject; aListFields: TViewEngineFieldRefList; aADOQuery:
        TADOQuery): string;



    function GetCoordFieldValue(aFieldName: string; aBLPoint_CK42: TBLPoint):
        string;

    function PrepareObject1(aObjectName: string): TViewEngineObject;

    procedure OpenDB_list (
                           aSQL : string;
                           aADOQuery: TADOQuery;
                           aRec: TdmViewEngineOpenDatasetRec;
                           aViewObject: TViewEngineObject
                           );

    function LoadMemData(aObjectName: string; aID: integer; aADOQuery: TADOQuery;
        aViewSQL: string): Boolean;



    function LoadMemData_Prepare_ViewFields(aObjectName: string; aADOQuery:
        TADOQuery; aViewObject:TViewEngineObject; var aViewSQL: string): Boolean;

  end;


function dmViewEngine: TdmViewEngine;


//==================================================================
//
//==================================================================
implementation {$R *.DFM}

const
  FLD_ROW = 'ROW';


var
  FdmViewEngine: TdmViewEngine;


// ---------------------------------------------------------------
function dmViewEngine: TdmViewEngine;
// ---------------------------------------------------------------
begin
 if not Assigned(FdmViewEngine) then
     FdmViewEngine := TdmViewEngine.Create(Application);

  Result := FdmViewEngine;
end;

//-------------------------------------------------------------------
procedure TdmViewEngine.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  db_SetComponentADOConnection(Self, dmMain.ADOConnection1);


  ObjectList := TViewEngineObjectList.Create();
end;

procedure TdmViewEngine.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(ObjectList);

  //g_Log.Add('TdmViewEngine.DataModuleDestroy(Sender: TObject);');
  inherited;
end;


procedure TdmViewEngine.Clear;
begin
  ObjectList.Clear;
end;


//-------------------------------------------------------------------
function TdmViewEngine.GetTableSQLForFields(aObjectName: string; aViewObject:
    TViewEngineObject; aListFields: TViewEngineFieldRefList; aADOQuery:
    TADOQuery): string;
//-------------------------------------------------------------------

{
    //-------------------------------------------------------------------
    function DoGetCaseSQL(aListFields: TStrings; aFieldName,aOutFieldName: string): string;
    //-------------------------------------------------------------------
    var I: Integer;
      s : string;
    begin

      if aListFields.Count>0 then
      begin
        s:=Format('CASE m.%s',[aFieldName]);

        for I := 0 to aListFields.Count - 1 do
          s:=s+ Format(' WHEN %d THEN ''%s''', [i, aListFields[i]]);

        s:=s+' END as '+aOutFieldName;
      end else
        s:=''''' as '+aOutFieldName;


      Result:=s;
    end;
    //---------------------------------------------------------
 }

const
  SQL_SELECT =
    ' SELECT %s  FROM %s m   %s ';
//    ' %s  WHERE main_tbl.id=:id ';

var
 // bIsExists_GeoRegion_ID: Boolean;
//  I: Integer;
 // sSql,sFields,sJoins,sTable,sMainTableName,s,sFieldName: string;

  sMainTableName: string;

 // sTblAlias, sFldAlias: string;
 // ot: TViewEngFieldType;
 // oStrList: TStringList;
 // sName: string;

 // oViewField: TViewField;
  //s1: string;

begin
 // Assert  (Assigned (aMem_Fields) );
  sMainTableName:= g_Obj.ItemByName[aObjectName].ViewName;


  if sMainTableName='' then
    sMainTableName:= g_Obj.ItemByName[aObjectName].TableName;


//   Result:= 'select top 1 * from ui.view_' + aObjectName;

   ///////////
//  dmOnega_DB_data.OpenQuery(aADOQuery, Result);

   Result:= 'select * from ui.view_' + aObjectName;
   Exit;

   /////////////////////////////
   /////////////////////////////
   /////////////////////////////
   /////////////////////////////
   /////////////////////////////
   /////////////////////////////
   /////////////////////////////
   /////////////////////////////
   /////////////////////////////
   /////////////////////////////

{

//  db_OpenQuery (qry_Data, 'SELECT TOP 1 * FROM '+sMainTableName+ ' WHERE id=0');
  s:='SELECT TOP 1 * FROM '+sMainTableName+ ' WHERE id=0';

 // aADOQuery.Fields.Clear;

  dmOnega_DB_data.OpenQuery(aADOQuery, s);

//  db_View(aADOQuery);

//  db_OpenQuery (aADOQuery, s);


//  bIsExists_GeoRegion_ID := Assigned(aADOQuery.FindField(FLD_GeoRegion_ID));


//  sFields:= '';// 'id, guid, ';
  sJoins := '';

  sFields:='m.id, ';

//  if bIsExists_GeoRegion_ID then
//    sFields:=sFields + 'm.GeoRegion_ID, ';



  for I := 0 to aListFields.Count - 1 do
//  for I := 0 to aViewObject.ListFields.Count - 1 do
  begin
    oViewField := aListFields[i];


    sFieldName:=oViewField.Field_Name1;

    if (sFieldName<>'') AND (not Assigned(aADOQuery.FindField(sFieldName))) then
    begin
      g_Log.Error('Field not found: '+ sFieldName);
      Continue;
    end;


    if (sFieldName<>'') AND
      Assigned(aADOQuery.FindField(sFieldName))
//       Assigned(aMem_Data.FindField(sFieldName+'_str'))
    then
    begin
      AppendStr(sFields, 'm.'+sFieldName+',');
//      sFields:= + sFieldName;//Format('%s.name AS %s,', [sTblAlias, sFldAlias]);

}

   (*   s1:=oViewField.View_FieldName;

      if s1<>'' then
      begin
        sFldAlias:=sFieldName+'_str';

        sFields   :=sFields + Format('%s AS %s,', [s1, sFldAlias]);

      end else
*)

{
      case TdmViewEngine_StrToFieldType(oViewField.FieldTypeStr) of
        //------------------------------------------------------
        vftList:
        //------------------------------------------------------
        begin

          oStrList:=TStringList.Create;
          oStrList.Text:=oViewField.ListNames.Text;// ItemsText;//I FieldByName(FLD_ITEMS).AsString;

          sFields:=sFields+ DoGetCaseSQL (oStrList, sFieldName, sFieldName+'_str') + ',';
//          AppendStr(sFields, DoGetCaseSQL (oStrList, sFieldName, sFieldName+'_str') + ',');

          FreeAndNil(oStrList);
        end;

        //------------------------------------------------------
        vftXRef:
        //------------------------------------------------------
        begin
       //   s1:=oViewField.View_FieldName;


           (* if oViewField.View_FieldName<>'' then
            begin
              s:=oViewField.View_FieldName;

            end;*)

            s:= oViewField.XREF_ObjectName;//   FieldByName(FLD_XREF_ObjectName).AsString;

            Assert (oViewField.XREF_ObjectName<>'', sFieldName);

            if Assigned(g_Obj.ItemByName[s]) then
            begin

              sTable:= g_Obj.ItemByName[s].TableName;

            //  sTblAlias:= 'tbl'+AsString(RecNo+1);
              sTblAlias:= 'tbl'+AsString(i+1);

              sFldAlias:=  sFieldName+'_str';
              sFields:=    sFields + Format('%s.name AS %s, ', [sTblAlias, sFldAlias]);


              sJoins:=     sJoins + Format('LEFT OUTER JOIN %s  %s ON m.%s   = %s.id      ',
                           [sTable, sTblAlias, sFieldName, sTblAlias]);
            end;
          end;

        //------------------------------------------------------
        vftStatus:
        //------------------------------------------------------
        begin
      //    s1:=oViewField.View_FieldName;


       //   sTblAlias:='tbl'+AsString(RecNo+1);
          sTblAlias:='tbl'+AsString(i+1);

          sFldAlias:=sFieldName+'_str';

          sFields   :=sFields + Format('%s.name AS %s,', [sTblAlias, sFldAlias]);
          sTable    :=TBL_STATUS;
//          sTable    :=TBL_STATUS_Values;
          sJoins    :=sJoins + Format('LEFT OUTER JOIN %s  %s ON m.%s   = %s.id      ',
                       [sTable, sTblAlias, sFieldName, sTblAlias]);


        end;
      end;
    end;

  //  Next;
  end;

  sFields:=sFields + Format(' m.guid, m.%s ,m.%s ,m.%s ,m.%s', [FLD_DATE_CREATED,FLD_DATE_MODIFY,FLD_USER_CREATED,FLD_USER_MODIFY]);

  sSql:= Format(SQL_SELECT, [sFields, sMainTableName, sJoins]);

  Result:= sSql;
  }
end;


//-------------------------------------------------------------------
function TdmViewEngine.LoadMemData_Prepare_ViewFields(aObjectName: string;
    aADOQuery: TADOQuery; aViewObject:TViewEngineObject; var aViewSQL: string):
    Boolean;
// ---------------------------------------------------------------
var
  sSQL: string;
begin
  Assert  (Assigned (aAdOQuery) );

  aADOQuery.Fields.Clear;

 // Assert  (Assigned (aMem_Fields) );
//  Assert  (Assigned (aMem_Data) );

  //--------------------------------------------------------
  sSQL:=GetTableSQLForFields(aObjectName, aViewObject, aViewObject.ViewFields, aADOQuery); //aMem_Fields,

//  sSQL:=GetListSQL (aRec.ObjectType, aRec.FilterWhere, aRec.IsAllFolders);

//  FViewSQL:=sSQL + Format(' WHERE m.id=%d', [aID]);

//  aViewSQL:=sSQL + ' WHERE m.id=%d';
  aViewSQL:=sSQL + ' WHERE id=%d';

end;


//-------------------------------------------------------------------
function TdmViewEngine.LoadMemData(aObjectName: string; aID: integer;
    aADOQuery: TADOQuery; aViewSQL: string): Boolean;

//-------------------------------------------------------------------
//type
//  TAfterPostEvent = procedure (DataSet: TDataSet) of object;


var sTableName,s,sFieldName: string;
  I: integer;
  ot: TViewEngFieldType;
  sObjectName: string;
  sSQL: string;
  oField1: TField;
  oField2: tField;
  i1: integer;
  i2: integer;

  pr: TDataSetAfterPostEvent;

begin
  Result := False;



  Assert  (Assigned (aAdOQuery) );


  sSQL:= Format(aViewSQL, [aID]);


  dmOnega_DB_data.OpenQuery(aAdOQuery, sSQL);
  if aAdOQuery.IsEmpty then
    begin
//      g_log.Add();

    //  ShowMessage('Empty: '+ sSQL);
      Exit;
    end;



 // aAdOQuery.EnableControls;


//  i:=qry_Data.FieldCount;
  i:=aAdOQuery.FieldCount;


 // db_OpenQuery (aAdOQuery, sSQL);

 // db_OpenQuery (qry_Data, sSQL);

  i:=aAdOQuery.FieldCount;



  if Eq(aObjectName, OBJ_PROPERTY) and
    (not Assigned(aAdOQuery.FindField(FLD_lat_lon_CK_42))) then
  begin
   //  db_View(aMem_Data);

  //  aMem_Data.Close;

    db_CreateFieldsForDataset(aAdOQuery);


    db_CreateCalculatedField(aAdOQuery, FLD_lat_lon_CK_42,  ftString, 50);
    db_CreateCalculatedField(aAdOQuery, FLD_lat_lon_CK_42_str,  ftString, 50);

    db_CreateCalculatedField(aAdOQuery, FLD_lat_lon_CK_95,  ftString, 50);
    db_CreateCalculatedField(aAdOQuery, FLD_lat_lon_CK_95_str,  ftString, 50);

    db_CreateCalculatedField(aAdOQuery, FLD_lat_lon_WGS_84, ftString, 50);
    db_CreateCalculatedField(aAdOQuery, FLD_lat_lon_WGS_84_str, ftString, 50);

    db_CreateCalculatedField(aADOQuery, FLD_lat_lon_full_str, ftString, 200);



    aAdOQuery.OnCalcFields :=DoOnCalcFields;

    aAdOQuery.Open;


  end ;
end;


//-------------------------------------------------------------------
function TdmViewEngine.PrepareObject1(aObjectName: string): TViewEngineObject;
//-------------------------------------------------------------------
const
  SQL_OBJECT_FIELDS =
     'SELECT *  FROM ' + view__object_fields_enabled  ;

  view__object_fields_user = 'view__object_fields_user';

  SQL_OBJECT_USER_FIELDS =
     'SELECT *  FROM ' + view__object_fields_user  ;


   //  'SELECT *  FROM ' + TBL_OBJECT_FIELDS +
   //  ' WHERE (enabled <> 0) ORDER BY parent_id, index_ ';
    // ' WHERE (enabled <> 0) ORDER BY parent_id, index_ ';

begin

//////  oObject:=ObjectList.FindByObjectName(aObjectName);

  if ObjectList.Count=0 then
  begin
    ADOTable1.TableName  := TBL__OBJECTS;
    ADOTable1.Open;
    ObjectList.LoadFromDB_Objects(ADOTable1);
    ADOTable1.Close;

    // -------------------------
    dmOnega_DB_data.OpenQuery (ADOQuery1,  SQL_OBJECT_FIELDS);

  //  ADOQuery1.TableName  := TBL_OBJECT_FIELDS;
    ADOQuery1.Open;
    ObjectList.LoadFromDB_ObjectFields(ADOQuery1);
    ADOQuery1.Close;

    // -------------------------
    dmOnega_DB_data.OpenQuery (ADOQuery1,  SQL_OBJECT_USER_FIELDS);

  //  ADOQuery1.TableName  := TBL_OBJECT_FIELDS;
    ADOQuery1.Open;
    ObjectList.LoadFromDB_ObjectFields_user(ADOQuery1);
    ADOQuery1.Close;


    ObjectList.Prepare;

  end;

  Result  :=  ObjectList.FindByObjectName(aObjectName);

  Assert(Assigned(Result), aObjectName + ' --- Assigned(Result)  --  function TdmViewEngine.PrepareObject1(aObjectName: string): TViewEngineObject;');

end;


//------------------------------------------------------------------
procedure TdmViewEngine.OpenDB_list (
                                aSQL : string;
                                aADOQuery: TADOQuery;
                                aRec: TdmViewEngineOpenDatasetRec;
                                aViewObject: TViewEngineObject
                                );
//------------------------------------------------------------------
var
  sSQL: string;
  i: Integer;
  sObjectName: string;
begin
 // sObjectName:=g_Obj.ItemByType[aRec.ObjectType].Name;

 // sSQL:=GetTableSQLForFields(sObjectName,  aViewObject, aViewObject.ListFields, aADOQuery);  //aMem_Fields,

   aSQL:='select * from ('+ aSQL +') M ';

//  sSQL:=sSQL + GetWhereSQL (aRec.ObjectType, aRec.FilterWhere, aRec.IsAllFolders);
  sSQL:=aSQL + GetWhereSQL (aRec.ObjectType, aRec.IsAllFolders);



  //aRec.FilterWhere111,

 // db_View(aADOQuery);
  aADOQuery.DisableControls;


 // dmOnega_DB_data.

  dmOnega_DB_data.OpenQuery(aADOQuery, sSQL,
  
//  db_OpenQuery (aADOQuery, sSQL,
               [FLD_FOLDER_ID,  aRec.FolderID,
                FLD_PROJECT_ID, dmMain.ProjectID
               ]);

//db_View(aADOQuery);

  if aRec.ObjectType = otPROPERTY then
  begin
 // db_View(aADOQuery);

    db_CreateFieldsForDataset(aADOQuery);

    db_CreateCalculatedField(aADOQuery, FLD_lat_lon_CK_42,  ftString, 50);
    db_CreateCalculatedField(aADOQuery, FLD_lat_lon_CK_42_str,  ftString, 50);


    db_CreateCalculatedField(aADOQuery, FLD_lat_lon_CK_95,  ftString, 50);
    db_CreateCalculatedField(aADOQuery, FLD_lat_lon_CK_95_str,  ftString, 50);

    db_CreateCalculatedField(aADOQuery, FLD_lat_lon_WGS_84, ftString, 50);
    db_CreateCalculatedField(aADOQuery, FLD_lat_lon_WGS_84_str, ftString, 50);


    db_CreateCalculatedField(aADOQuery, FLD_lat_lon_full_str, ftString, 200);


    aADOQuery.OnCalcFields :=DoOnCalcFields;

//db_View(aADOQuery);

    aADOQuery.Open;

//db_View(aADOQuery);

   //  db_View(aADOQuery);

  //  db_ViewDataSet(aMem_Data);

  end;


  aADOQuery.EnableControls;

end;



//-------------------------------------------------------------------
function TdmViewEngine.CreateFieldInfo(aViewField: TViewField): TViewEngFieldInfo;
//-------------------------------------------------------------------
begin
  Result:=TViewEngFieldInfo.Create;

  with aViewField do
  begin
    Result.DisplayIndex  :=  aViewField.DisplayIndex;
                                             
    Result.ID        :=  aViewField.ID;
    Result.ParentID  :=  aViewField.PARENT_ID;

//    Result.Name_     :=  aViewField.Name_;
    Result.Alias     :=  aViewField.Alias;
    Result.FieldName:=  aViewField.Field_Name1;

    Result.Caption   :=  aViewField.Caption;
    Result.FieldType :=  TdmViewEngine_StrToFieldType(aViewField.FieldTypeStr);

    Result.IsFolder  :=  aViewField.IsFolder;
    Result.IsReadOnly:=  aViewField.IsReadOnly;

    Result.ListNames.Text :=aViewField.ListNames.Text;

    Result.StatusName:=aViewField.Status_Name;

//    Result.Max_Value:=aViewField.Max_Value;
//    Result.Min_Value:=aViewField.Min_Value;

    Result.XREF_ObjectName:=aViewField.XREF_ObjectName;
//    Result.XREF_FieldName :=aViewField.XREF_FieldName2;
  //  Result.View_FieldName :=aViewField.View_FieldName;

  end;
end;


//--------------------------------------------------------------------
function TdmViewEngine.GetWhereSQL(aObjectType: TrpObjectType; aIsAllFolders:
    boolean; aProjectID: integer=0): string;
//--------------------------------------------------------------------
var// sFROM,
   sWhere,sWhereFolder,sWhereProjectID: string;
   i: integer;
   oItem: TObjectTypeItem;
begin
  oItem:=g_Obj.ItemByType[aObjectType];

  //use Project ID for objects

  if oItem.IsUseProjectID then
    sWhereProjectID:='(project_id=:project_id)'
//    sWhereProjectID:='(m.project_id=:project_id)'
  else
    sWhereProjectID:='(0=0)';


  if (not aIsAllFolders) then
//    sWhereFolder:='(((0=:folder_id) and (m.folder_id IS NULL)) '+
//                  ' or (m.folder_id = :folder_id))'

    sWhereFolder:='(((0=:folder_id) and (folder_id IS NULL)) '+
                  ' or (folder_id = :folder_id))'

  else
    sWhereFolder:='(0=0)';

//  if aFilterWhere='' then
//    aFilterWhere:='(0=0)';

//  sWhere:=aFilterWhere +' and '+ sWhereFolder+' and '+ sWhereProjectID;
  sWhere:=sWhereFolder+' and '+ sWhereProjectID;

  sWhere:=ReplaceStr(sWhere, '(0=0) and ', '');

  if sWhere<>'' then
    Result:=' WHERE '+ sWhere
  else
    Result:='';

//  Format('SELECT * FROM %s WHERE %s ', [sFROM, sWhere]);

//  Result:=ReplaceStr(Result, '(0=0) and ', '');

end;


//-------------------------------------------------------------------
function TdmViewEngine.GetCoordStr(aBLPoint: TBLPoint): string;
//-------------------------------------------------------------------
var
  iScale: integer;
//  dMapLengthInCm, dMapLengthInKm, dZoom, dMapX, dMapY: Double;

  bl: TblPoint;
  k: Integer;
  S: string;

 // xyPoint: TxyPoint;

const
  DEF_DELIMITER = '  |  ';

begin
    assert(aBLPoint.B<>0);

    s:='';

    s:=s + geo_FormatBLPoint(aBLPoint) + ' (CK-42)' + DEF_DELIMITER;

    bl:= geo_Pulkovo42_to_CK_95 (aBLPoint);//dmOptions.GetGeoCoord);
    s:=s  + geo_FormatBLPoint(bl)+ ' (CK-95)' + DEF_DELIMITER;

    CK42_to_GSK_2011 (aBLPoint.B, aBLPoint.L, bl.B, bl.L  );
    s:=s  + geo_FormatBLPoint(bl) + ' (�CK-2011)'+ DEF_DELIMITER;

    bl:= geo_Pulkovo42_to_WGS84 (aBLPoint);//dmOptions.GetGeoCoord);
    s:=s  + geo_FormatBLPoint(bl) + ' (WGS-84)';//+ DEF_DELIMITER;

   result:=s;
end;


// ---------------------------------------------------------------
procedure TdmViewEngine.DoOnCalcFields(DataSet: TDataSet);
// ---------------------------------------------------------------
var
  bl_CK42: TBLPoint;
  bl: TBLPoint;
  i: Integer;
  s: string;
begin
  i:=DataSet.RecordCount;



   if Assigned(DataSet.FindField(FLD_lat_lon_full_str)) then
   begin
     bl_CK42 := db_ExtractBLPoint(DataSet);

 //    assert(bl_CK42.B<>0);

     if bl_CK42.B>0 then
       DataSet[FLD_lat_lon_full_str] := GetCoordStr (bl_CK42);
//     'lat_lon_kras_wgs';


   end;

//      lat_lon_kras_wgs

   // -------------------------



  // ---------------------------------------------------------------
  if Assigned(DataSet.FindField(FLD_LAT))and
     Assigned(DataSet.FindField(FLD_LON)) then
  begin
    // initial coordinate
    bl_CK42 := db_ExtractBLPoint(DataSet);

    if (bl_CK42.B=0) and (bl_CK42.L=0) then
      Exit;


//      lat_lon_kras_wgs

   // -------------------------
   if Assigned(DataSet.FindField(FLD_lat_lon_CK_42)) then
     DataSet[FLD_lat_lon_CK_42] := GetCoordFieldValue(FLD_lat_lon_CK_42, bl_CK42);

   if Assigned(DataSet.FindField(FLD_lat_lon_CK_42_str)) then
     DataSet[FLD_lat_lon_CK_42_str] := GetCoordFieldValue(FLD_lat_lon_CK_42_str, bl_CK42);

   // -------------------------
   if Assigned(DataSet.FindField(FLD_lat_lon_CK_95)) then
     DataSet[FLD_lat_lon_CK_95] := GetCoordFieldValue(FLD_lat_lon_CK_95, bl_CK42);

   if Assigned(DataSet.FindField(FLD_lat_lon_CK_95_str)) then
     DataSet[FLD_lat_lon_CK_95_str] := GetCoordFieldValue(FLD_lat_lon_CK_95_str, bl_CK42);

   // -------------------------
   if Assigned(DataSet.FindField(FLD_lat_lon_WGS_84)) then
     DataSet[FLD_lat_lon_WGS_84] := GetCoordFieldValue(FLD_lat_lon_WGS_84, bl_CK42);

   if Assigned(DataSet.FindField(FLD_lat_lon_WGS_84_str)) then
     DataSet[FLD_lat_lon_WGS_84_str] := GetCoordFieldValue(FLD_lat_lon_WGS_84_str, bl_CK42);
          
  end;

end;


// ---------------------------------------------------------------
function TdmViewEngine.GetCoordFieldValue(aFieldName: string; aBLPoint_CK42:
    TBLPoint): string;
// ---------------------------------------------------------------
var
  bl: TBLPoint;
  s: string;

begin
  Result := '';

  if Eq(aFieldName, FLD_lat_lon_CK_42) then
  begin
    bl := aBLPoint_CK42;
    result := Format('%1.8f N  %1.8f E', [bl.B, bl.L]);
  end else

  if Eq(aFieldName, FLD_lat_lon_CK_42_str) then
  begin
    bl := aBLPoint_CK42;
    result :=geo_FormatDegree_Lat(bl.B) + '  ' + geo_FormatDegree_lon(bl.B);
  end  else

  if Eq(aFieldName, FLD_lat_lon_CK_95) then
  begin
    bl := geo_Pulkovo42_to_CK_95 (aBLPoint_CK42);
//    bl := geo_Pulkovo_CK_42_to_CK_95 (aBLPoint_CK42);
//    bl := geo_Pulkovo_BL_to_BL_95(aBLPoint_CK42);
    result := Format('%1.8f N  %1.8f E', [bl.B, bl.L]);
  end  else

  if Eq(aFieldName, FLD_lat_lon_CK_95_str) then
  begin
//     bl := geo_Pulkovo_CK_42_to_CK_95(aBLPoint_CK42);
     bl := geo_Pulkovo42_to_CK_95(aBLPoint_CK42);

//    bl := geo_Pulkovo_BL_to_BL_95(aBLPoint_CK42);
    result :=geo_FormatDegree_lat(bl.B) + '  ' + geo_FormatDegree_lon(bl.B);
  end else

  if Eq(aFieldName, FLD_lat_lon_WGS_84) then
  begin
    bl := geo_Pulkovo42_to_WGS84(aBLPoint_CK42);
    s:=Format('%1.8f N  %1.8f E', [bl.B, bl.L]);
    result:= s;
  end else

  if Eq(aFieldName, FLD_lat_lon_WGS_84_str) then
  begin
    bl := geo_Pulkovo42_to_WGS84(aBLPoint_CK42);
    result:=geo_FormatDegree_lat(bl.B) + '  ' + geo_FormatDegree_lon(bl.B);
  end;

end;


begin

end.



{




//-------------------------------------------------------------------
procedure TdmViewEngine.LoadData1111111111;
//-------------------------------------------------------------------
const
  SQL_OBJECT_FIELDS =
//     'SELECT *  FROM ' + TBL_OBJECT_FIELDS + ' ORDER BY parent_id, index_ ';

//     'SELECT *  FROM ' + view__object_fields_enabled_ ;

     'SELECT *  FROM ' +   view__object_fields_enabled_ ;

//     ' WHERE (enabled <> 0) ORDER BY parent_id, index_ ';

begin
  ObjectList.Clear;


  ADOTable1.TableName  := TBL__OBJECTS;
  ADOTable1.Open;
  ObjectList.LoadFromDB_Objects(ADOTable1);
  ADOTable1.Close;

  // -------------------------
  db_Ope nQuery (ADOQuery1,  SQL_OBJECT_FIELDS);

//  ADOQuery1.TableName  := TBL_OBJECT_FIELDS;
  ADOQuery1.Open;
  ObjectList.LoadFromDB_ObjectFields(ADOQuery1);
  ADOQuery1.Close;

//  g_Log.Add('Open table: '+ VIEW_Status_Values);

  db_OpenQuery (qry_Status, 'SELECT * FROM '+ VIEW_Status_Values);

end;

