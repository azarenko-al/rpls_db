unit fr_View_base;

interface

uses
  Classes, Controls, Forms,  ExtCtrls, ActnList, Menus, cxPropertiesStore,

  u_dxBar,

  I_object_list,

  dm_Onega_DB_data,

  dm_Main,

  u_explorer,

  u_cx,

  u_const_str,
  u_const,
  u_types,

  u_func_msg,
  
                                   
  I_Shell, cxLookAndFeels, dxBar, cxBarEditItem, cxCheckBox, cxButtonEdit,
  cxClasses, ImgList, dxSkinsCore, dxSkinsDefaultPainters;

type
  Tframe_View_Base = class(TForm)
    Panel1: TPanel;
    MainActionList: TActionList;
    ImageList1: TImageList;
    pn_Caption: TPanel;
    PopupMenu1: TPopupMenu;
    pn_Main: TPanel;
    cxPropertiesStore: TcxPropertiesStore;
    act_Commit: TAction;
    act_Rollback: TAction;
    cxLookAndFeelController1: TcxLookAndFeelController;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    cb_Filter_All_Folders_1: TcxBarEditItem;
    dxBarButton3: TdxBarButton;
    dxBarSubItem_Menu: TdxBarSubItem;
    ed_Filter: TcxBarEditItem;
    procedure dxBarSubItem_MenuPopup(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
//    procedure mnu_ActionsPopup(Sender: TTBCustomItem; FromLink: Boolean);
    procedure FormDestroy(Sender: TObject);
  private
//    procedure AddControl(aControl: TWinControl; aFields: array of string);
    procedure Open;
    procedure Rollback; virtual;
    procedure Commit; virtual;
    procedure DoOnActionsPopup;
  protected
    FRegPath: string;

//    FGeoRegion_ID : Integer;
//    FGeoRegion1_ID : Integer;
//    FGeoRegion2_ID : Integer;


//    FProcIndex: integer;

    FIsEditMode: boolean;

    procedure AddComponentProp(aComponent: TComponent; aField: string); overload;
    procedure AddComponentProps(aComponent: TComponent; aFields: array of string); overload;

//    procedure GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean); virtual;
//    procedure GetMessage_(aMsg: TEventType; aParams: TEventParamList; var aHandled:  Boolean); virtual; abstract;

    procedure GetShellMessage(aMsg: integer; aRec: TShellEventRec); virtual;
  protected
    FGUID: string;

    function GetGUID: string;
    function RecordExists(aID: integer): Boolean;
  public
    ID: integer;
    ObjectName: string;

    TableName : string;

    ReadOnly: Boolean;

    //CanBeModified: boolean;

 
   procedure View(aID: integer; aGUID: string); virtual;

    procedure SetObjectName (aName: string); virtual;
    procedure SetCaption (aCaption: string);
  end;



implementation
{$R *.dfm}


procedure Tframe_View_Base.AddComponentProps(aComponent: TComponent; aFields: array of string);
var I: integer;
begin
  for I := 0 to High(aFields) do
    AddComponentProp(aComponent, aFields[i]);
end;


procedure Tframe_View_Base.AddComponentProp(aComponent: TComponent; aField: string);
begin
  cx_AddComponentProp(cxPropertiesStore, aComponent, aField);
end;



// -------------------------------------------------------------------
procedure Tframe_View_Base.FormCreate(Sender: TObject);
// -------------------------------------------------------------------
begin
  inherited;

//  mnu_Actions.Caption:=STR_ACTIONS;

  dxBarSubItem_Menu.Caption:=STR_ACTIONS;

  Menu:=nil;

  pn_Main.Align:=alClient;


  FRegPath:= REGISTRY_COMMON_FORMS + ClassName +'\';

 // pn_Header.Height:=//pn_Caption.Height +
  //                  CoolBar2.Height;


  Assert(not cxPropertiesStore.Active);

  cxPropertiesStore.StorageName:=FRegPath;
  cxPropertiesStore.Active := True;


//  FProcIndex:=g_EventManager.RegisterProc (GetMessage, Name);

end;


procedure Tframe_View_Base.FormDestroy(Sender: TObject);
begin
//  g_EventManager.UnRegisterProc (FProcIndex, GetMessage);

  inherited;
end;


procedure Tframe_View_Base.Open;
begin
  View (ID, GetGUID);
end;

// ---------------------------------------------------------------
function Tframe_View_Base.RecordExists(aID: integer): Boolean;
// ---------------------------------------------------------------
begin
  Assert(TableName<>'');

  Result:=dmOnega_DB_data.RecordExists(TableName, aID);
end;


// ---------------------------------------------------------------
procedure Tframe_View_Base.View(aID: integer; aGUID: string);
// ---------------------------------------------------------------
begin
  ID:=aID;
  FGUID:=aGUID;

//  result := False;

  //status
  if g_Obj.ItemByName[ObjectName].TableName='' then
    Exit;


  Assert(g_Obj.ItemByName[ObjectName].TableName<>'');

 // if GUID = '' then
 //   GUID:=gl_DB.GetGUIDByID (g_Obj.ItemByName[ObjectName].TableName, aID);


//  GUID:=gl_DB.GetGUIDByID (g_Obj.ItemByName[ObjectName].TableName, aID);

 // CanBeModified:= True;
 ///
   { TODO : dmUser.ObjectCanBeModified }
////////////////   dmUser.ObjectCanBeModified (ObjectName, ID);


end;


procedure Tframe_View_Base.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;


procedure Tframe_View_Base.SetCaption(aCaption: string);
begin
  Caption:=aCaption;
  pn_Caption.Caption:=aCaption;
end;


procedure Tframe_View_Base.SetObjectName(aName: string);
begin
  ObjectName:=aName;

end;



procedure Tframe_View_Base.GetShellMessage(aMsg: integer; aRec: TShellEventRec);
begin

end;



procedure Tframe_View_Base.dxBarSubItem_MenuPopup(Sender: TObject);
begin
  DoOnActionsPopup
end;

  {
//-------------------------------------------------------------------
procedure Tframe_View_Base.mnu_ActionsPopup (Sender: TTBCustomItem; FromLink: Boolean);
//-------------------------------------------------------------------
begin
  DoOnActionsPopup;
end;
   }
   
//-------------------------------------------------------------------
procedure Tframe_View_Base.DoOnActionsPopup;
//-------------------------------------------------------------------
var oPIDL: TrpPIDL;
  i: Integer;

  oPIDLs: TrpPIDL_List;

begin
  { TODO : ->call DM }

  oPIDLs:=TrpPIDL_List.Create;

  oPIDLs.Clear;

  oPIDL:=oPIDLs.AddItem;
  oPIDL.ID  :=ID;
  oPIDL.GUID:=GetGUID;
  oPIDL.IsFolder:=False;
  oPIDL.ObjectName:=ObjectName;

//  oPIDL.GeoRegion_ID :=FGeoRegion_ID;
//  oPIDL.GeoRegion1_ID:=FGeoRegion1_ID;
//  oPIDL.GeoRegion2_ID:=FGeoRegion2_ID;


  { TODO : !!!!!!!!!!! }

////  g_Objects1.GetPopupMenu();
  g_Objects1.GetPopupMenu_list(oPIDLs, PopupMenu1);


  g_EventManager.PostEvent_ (WE_OBJECT_GET_POPUP_MENU,
            ['PIDL_LIST',  Integer(oPIDLs) ,
             'POPUP_MENU', Integer(PopupMenu1)
            ]);

  oPIDLs.Free;

//  dlg_AssignPopupMenu (PopupMenu1, mnu_Actions);

  dlg_AssignPopupMenu_dx (PopupMenu1,  dxBarSubItem_Menu, dxBarManager1);


    // !!USERS!!
{  for i:= 0 to PopupMenu1.Items.Count-1 do
    PopupMenu1.Items[i].Enabled:= dmUser.ObjectCanBeModified (oPIDL.ObjectName, oPIDL.ID);
}

end;



procedure Tframe_View_Base.Rollback;
begin
  //virtual;
end;


procedure Tframe_View_Base.Commit;
begin
  //virtual;
end;



function Tframe_View_Base.GetGUID: string;
begin
  if FGUID = '' then
    FGUID:=gl_DB.GetGUIDByID (g_Obj.ItemByName[ObjectName].TableName, ID);

  Result := FGUID;
end;


end.

{



//-------------------------------------------------------------------
procedure Tframe_View_Base.GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean);
//-------------------------------------------------------------------
begin
  // ShowMessage('');

{   DEMO

    case aMsg of
    WE_REFRESH_LINKLINE_PARAMS: View (aParams[0].Value);
  end;}
end;
