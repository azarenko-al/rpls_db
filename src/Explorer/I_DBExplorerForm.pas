unit I_DBExplorerForm;

interface

uses
  Classes, Controls, SysUtils,Graphics,

  u_explorer,
  u_types,
  u_Inspector_common

  ;
  
type
 // TOnExplorerCustomDrawCellByGUID = procedure (Sender: TObject;
 //                        aPIDL: TrpPIDL; var AColor: TColor; AFont: TFont) of object;


  TOnExplorerItemEvent = procedure (aPIDL: TrpPIDL) of object;


  TdmExplorerStyle = (esProject, esDict, esObject);

  TdmExplorerOption = (otDisablePopup);
  TdmExplorerOptionSet = set of TdmExplorerOption;

  TdmExplorerFlag =(flIsFolder,

                    flGetChildCount,
                    flRoot
                   );

  TExplorerFlags = set of TdmExplorerFlag;


  IDBExplorerFormX = interface(IInterface)
  ['{B4171BD3-0317-4075-920E-2BFBAB0D40B3}']

    procedure RENAME_ITEM(aGUID, aNewName: string);

 //   procedure UPDATE_NODE_CHILDREN (aGUID: string );

    procedure DELETE_NODE_BY_GUID (aGUID: string );

//    function FocuseNodeByObjName(aObjectName: Widestring; aID: integer): Boolean;


    function ExpandByGUID(aGUID: string): Boolean;

    function FocuseNodeByGUID(aGUID: string): Boolean;
    function FocuseNodeByObjName(aObjectName: Widestring; aID: integer): Boolean;

    procedure UPDATE_NODE_CHILDREN_ByGUID (aGUID: string );


 //   procedure SetViewObject(aObjName: string);
  //  procedure SetViewObjects(aValues: array of TrpObjectType);
  end;


implementation

end.

