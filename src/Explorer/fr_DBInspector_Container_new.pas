unit fr_DBInspector_Container_new;

interface

uses
  Windows, Messages, ExtCtrls, Controls, Classes,Forms,menus,
  Dialogs, SysUtils, cxVGrid, DB,

  I_Shell,
 
  fr_DBInspector,

  u_Inspector_common,

  u_Explorer,

  u_const_db,
  u_Types,

  u_log,
  u_func,
  u_db,


  cxStyles, ADODB, ImgList, ActnList, Grids, DBGrids, StdCtrls, Mask,
  DBCtrls, ComCtrls, cxDBVGrid, cxControls, cxInplaceContainer, dxmdaset;

type


  Tframe_DBInspector_Container_new = class(Tframe_DBInspector)
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FOnButtonClick: TOnFieldChangedEvent;
    FOnFieldChanged: TOnFieldChangedEvent;
    FOnPost: TNotifyEvent;

  private
    FDBInspector: Tframe_DBInspector;
  public

    procedure DoOnPost             (Sender: TObject); virtual;
    procedure DoOnFieldChanged     (Sender: TObject; aFieldName: string; aNewValue: Variant); virtual;
    procedure DoOnButtonFieldClick (Sender: TObject; aFieldName: string; aNewValue: Variant); virtual;

    procedure DoOnRename (Sender: TObject; aGUID,aNAME: string);

  public
    ID: Integer;
    ObjectName: string;

    function GetDataSet: TDataSet;

    procedure View (aID: integer); virtual;
    procedure Post();
    procedure PrepareViewForObject(aObjectName: string; aObjectName2: string = '');

// TODO: FreeRowByFieldName
//  procedure FreeRowByFieldName (aFieldName: string);

//    procedure SetFieldEnable (aFieldName: string; aValue: boolean);
    procedure SetFieldReadOnly(aFieldName: string; aValue: boolean);


    function  GetFieldValue (aFieldName: string): Variant;
    function  GetIntFieldValue (aFieldName: string): Integer;
    function GetBooleanFieldValue(aFieldName: string): Boolean;
    function  GetStrFieldValue (aFieldName: string): string;
    function  GetFloatFieldValue(aFieldName: string): double;

    function GetInspectorRowByFieldName(aFieldName: string): TcxCustomRow;

    procedure ExpandRowByFieldName(aFieldName: string; aVisible : boolean);

//    procedure CollapseRowByFieldName (aFieldName: string);

// TODO: GetFieldInfoByFieldName1111111
//  function GetFieldInfoByFieldName1111111(aFieldName: string): TViewEngFieldInfo;
    procedure HideEdit;
    procedure SetRowCaptionByFieldName(aFieldName,aCaption: string);

    procedure SetMinMaxValues1(aFieldName: string; aMin, aMax: Integer);

    procedure SetRowVisibleByFieldName(aFieldName: string; aVisible : boolean);
    procedure SetRowVisibleByAlias(aAlias: string; aVisible : boolean);

    procedure SetFieldValue(aFieldName: string; aValue: Variant; aIsAddToList:
        boolean = true);

    procedure SetReadOnly(aValue: boolean; aMsg: string = '');
    procedure SetStrFieldValue(aFieldName: string; aValue: string; aIsAddToList: boolean = true);

    property OnFieldChanged: TOnFieldChangedEvent read FOnFieldChanged write FOnFieldChanged;
    property OnButtonClick: TOnFieldChangedEvent read FOnButtonClick write FOnButtonClick;
    property OnPost: TNotifyEvent read FOnPost write FOnPost;

  end;


implementation
{$R *.DFM}


//--------------------------------------------------------------------
procedure Tframe_DBInspector_Container_new.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  FDBInspector:=Tframe_DBInspector.Create(Self);

  FDBInspector.ManualDock(Self);
  FDBInspector.Align:=alClient;
  FDBInspector.Show;

  FDBInspector.OnRename:=DoOnRename;
  FDBInspector.OnFieldChanged:=DoOnFieldChanged;
  FDBInspector.OnButtonClick:=DoOnButtonFieldClick;

  FDBInspector.OnPost:=DoOnPost;
end;


//--------------------------------------------------------------------
procedure Tframe_DBInspector_Container_new.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  FDBInspector.Post;   // !!!!!!!!!

  FreeAndNil(FDBInspector);

  inherited;
end;


procedure Tframe_DBInspector_Container_new.View(aID: integer);
begin
  ID:=aID;

  FDBInspector.View (aID);

end;


procedure Tframe_DBInspector_Container_new.Post;
begin
  FDBInspector.Post;
end;

procedure Tframe_DBInspector_Container_new.PrepareViewForObject(aObjectName:
    string; aObjectName2: string = '');
begin
  ObjectName:=aObjectName;
//  ObjectName2:=aObjectName2;
  FDBInspector.PrepareViewForObject(aObjectName, aObjectName2);
end;

function Tframe_DBInspector_Container_new.GetFieldValue(aFieldName: string): Variant;
begin
  Result:= FDBInspector.GetFieldValue(aFieldName);
end;


procedure Tframe_DBInspector_Container_new.SetFieldValue(aFieldName: string;
    aValue: Variant; aIsAddToList: boolean = true);
begin
  FDBInspector.SetFieldValue(aFieldName,aValue,aIsAddToList);
end;

procedure Tframe_DBInspector_Container_new.SetStrFieldValue(aFieldName: string;
    aValue: string; aIsAddToList: boolean = true);
begin
  FDBInspector.SetStrFieldValue(aFieldName,aValue,aIsAddToList);
end;


function Tframe_DBInspector_Container_new.GetFloatFieldValue(aFieldName: string): double;
begin
  Result:= FDBInspector.GetFloatFieldValue(aFieldName);
end;


function Tframe_DBInspector_Container_new.GetBooleanFieldValue(aFieldName: string):
    Boolean;
begin
  Result := FDBInspector.GetBooleanFieldValue(aFieldName);
end;


function Tframe_DBInspector_Container_new.GetIntFieldValue(aFieldName: string): Integer;
begin
  Result:= FDBInspector.GetIntFieldValue(aFieldName);
end;

function Tframe_DBInspector_Container_new.GetStrFieldValue( aFieldName: string): string;
begin
  Result:= FDBInspector.GetStrFieldValue( aFieldName);
end;

(*
procedure Tframe_DBInspector_Container_new.CollapseRowByFieldName(aFieldName: string);
begin
  FDBInspector.CollapseRowByFieldName(aFieldName);
end;
*)


procedure Tframe_DBInspector_Container_new.ExpandRowByFieldName(aFieldName:
    string; aVisible : boolean);
begin
  FDBInspector.ExpandRowByFieldName(aFieldName, aVisible);
end;

// TODO: GetFieldInfoByFieldName1111111
//function Tframe_DBInspector_Container.GetFieldInfoByFieldName1111111(
//  aFieldName: string): TViewEngFieldInfo;
//begin
// ///// Result:=  FDBInspector.GetFieldInfoByFieldName (aFieldName);
//end;

procedure Tframe_DBInspector_Container_new.SetRowCaptionByFieldName(aFieldName,aCaption: string);
begin
  FDBInspector.SetRowCaptionByFieldName (aFieldName,aCaption);
end;


function Tframe_DBInspector_Container_new.GetInspectorRowByFieldName(aFieldName:
    string): TcxCustomRow;
begin
  Result := FDBInspector.GetInspectorRowByFieldName(aFieldName);
end;


procedure Tframe_DBInspector_Container_new.SetRowVisibleByFieldName(aFieldName: string;
    aVisible : boolean);
//--------------------------------------------------------------------
begin
  FDBInspector.SetRowVisibleByFieldName(aFieldName,aVisible);
end;


procedure Tframe_DBInspector_Container_new.SetRowVisibleByAlias(aAlias: string;
    aVisible : boolean);
begin
  FDBInspector.SetRowVisibleByAlias(aAlias,aVisible);
end;



procedure Tframe_DBInspector_Container_new.SetFieldReadOnly(aFieldName: string;
    aValue: boolean);
begin
  FDBInspector.SetFieldReadOnly (aFieldName,aValue);
end;


procedure Tframe_DBInspector_Container_new.SetReadOnly(aValue: boolean; aMsg:
    string = '');
begin
  FDBInspector.SetReadOnly(aValue,aMsg);
end;



procedure Tframe_DBInspector_Container_new.DoOnButtonFieldClick(Sender: TObject; aFieldName: string; aNewValue: Variant);
begin
  if Assigned(FOnButtonClick) then
    FOnButtonClick(Sender,aFieldName,aNewValue);
end;

procedure Tframe_DBInspector_Container_new.DoOnFieldChanged(Sender: TObject; aFieldName: string; aNewValue: Variant);
begin
  if Assigned(FOnFieldChanged) then
    FOnFieldChanged(Sender,aFieldName,aNewValue);
end;

procedure Tframe_DBInspector_Container_new.HideEdit;
begin
  FDBInspector.cxDBVerticalGrid1.HideEdit;
end;


procedure Tframe_DBInspector_Container_new.SetMinMaxValues1(aFieldName: string;
    aMin, aMax: Integer);
begin
  FDBInspector.SetMinMaxValues1(aFieldName, aMin, aMax);
end;


//-------------------------------------------------------------------
//
//-------------------------------------------------------------------
procedure Tframe_DBInspector_Container_new.DoOnPOst(Sender: TObject);
var
  ot: TrpObjectType;
begin
  ot:= g_Obj.ItemByName[ObjectName].Type_;

(*
  if FoundInMapObjects(ot) then
  begin
    if Assigned(IMapEngine) then
      dmMapEngine1.ReCreateObject(ot, ID);

//    PostEvent(WE_MAP_RECREATE_OBJECT,
 //    [app_Par(PAR_ObjectType, ot),
  //  zz    app_Par(PAR_ID,         ID1) ]);
  //  Assert(Assigned(IMapAct));


 /////////   if Assigned(IMapAct) then
//////      dmAct_Map.MapRefresh;

 //   dmAct_Map.MapRefresh;
  end;
*)

  if Assigned(FOnPost) then
    FOnPost(Sender);

end;


procedure Tframe_DBInspector_Container_new.DoOnRename(Sender: TObject; aGUID, aNAME: string);
begin
  g_ShellEvents.Shell_RENAME_ITEM (aGUID, aNAME);
  g_Shell.Shell_RENAME_ITEM (aGUID, aNAME);
end;


function Tframe_DBInspector_Container_new.GetDataSet: TDataSet;
begin
//  Result := FDBInspector.mem_Data;
  Result := FDBInspector.qry_Data;

end;



end.
