object frame_View_Base: Tframe_View_Base
  Left = 812
  Top = 142
  Width = 636
  Height = 485
  Caption = 'frame_View_base'
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 620
    Height = 64
    Align = alTop
    BevelOuter = bvLowered
    Color = clAppWorkSpace
    TabOrder = 0
    Visible = False
  end
  object pn_Caption: TPanel
    Left = 0
    Top = 91
    Width = 620
    Height = 17
    Align = alTop
    Alignment = taLeftJustify
    BevelOuter = bvLowered
    BorderWidth = 2
    Caption = 'object caption'
    Color = clAppWorkSpace
    TabOrder = 1
    Visible = False
  end
  object pn_Main: TPanel
    Left = 0
    Top = 108
    Width = 620
    Height = 126
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 2
  end
  object MainActionList: TActionList
    Images = ImageList1
    Left = 40
    Top = 8
    object act_Commit: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    end
    object act_Rollback: TAction
      Caption = #1054#1090#1084#1077#1085#1072
    end
  end
  object ImageList1: TImageList
    Left = 68
    Top = 8
  end
  object PopupMenu1: TPopupMenu
    Left = 100
    Top = 8
  end
  object cxPropertiesStore: TcxPropertiesStore
    Active = False
    Components = <>
    StorageName = 'cxPropertiesStore'
    StorageType = stRegistry
    Left = 200
    Top = 8
  end
  object cxLookAndFeelController1: TcxLookAndFeelController
    Kind = lfFlat
    NativeStyle = True
    SkinName = 'Office2013LightGray'
    Left = 88
    Top = 280
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    LookAndFeel.Kind = lfFlat
    NotDocking = [dsNone]
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 88
    Top = 336
    DockControlHeights = (
      0
      0
      27
      0)
    object dxBarManager1Bar1: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 801
      FloatTop = 229
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem_Menu'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 2'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 286
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 1172
      FloatTop = 327
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 207
          Visible = True
          ItemName = 'ed_Filter'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object cb_Filter_All_Folders_1: TcxBarEditItem
      Caption = #1042#1089#1077' '#1087#1072#1087#1082#1080
      Category = 0
      Hint = #1042#1089#1077' '#1087#1072#1087#1082#1080
      Visible = ivAlways
      ShowCaption = True
      Width = 100
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.ImmediatePost = True
      Properties.NullStyle = nssUnchecked
      InternalEditValue = False
    end
    object dxBarButton3: TdxBarButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarSubItem_Menu: TdxBarSubItem
      Caption = '&'#1054#1087#1077#1088#1072#1094#1080#1080
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
      OnPopup = dxBarSubItem_MenuPopup
    end
    object ed_Filter: TcxBarEditItem
      Caption = #1060#1080#1083#1100#1090#1088
      Category = 0
      Hint = #1060#1080#1083#1100#1090#1088
      Visible = ivAlways
      ShowCaption = True
      Width = 100
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end
        item
          Caption = #1059#1076#1072#1083#1080#1090#1100
        end>
      Properties.LookupItems.Strings = (
        'asdfaSD'
        'asdASDa'
        'sasdfasdf'
        'asdfasdf')
      Properties.ReadOnly = True
    end
  end
end
