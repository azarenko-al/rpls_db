unit I_Shell;

interface
uses DB, Controls, Contnrs, SysUtils, Classes, Forms,
    Dialogs, cxVGrid,  cxButtonEdit,

  //fr_Explorer_Container,

  I_DBExplorerForm,

  u_func,
  u_classes,
  u_types
  ;



  //dm_act_Explorer

//  IShellX_ = interface(IInterface)
//    ['{151D1659-3EC2-4258-AE04-38ADD0A37D17}']
//
//
////    function Dlg_SelectObject_dxInspectorRow(Sender: TObject; aType: TrpObjectType;
//  //      var aID: Integer): Boolean;
//
//    // dx button edit
//    procedure Dlg_SelectObject_dxButtonEdit (Sender: TObject; aType: TrpObjectType;
//                    var aID: Integer);
//
//    // VertcalGrid
//    function Dlg_SelectObject_cxButtonEdit (Sender: TcxButtonEdit; aType: TrpObjectType;
//                    var aID: Integer): Boolean;
//
//
//    function Dlg_EditObject (aObjectName: string; aID: integer): boolean;
//
//    function  Dlg_Select_Object (aType: TrpObjectType;
//                                  var aID: integer;
//                                  var aName: string): Boolean;
//
//    function Dlg_Object_Move_Enhanced (aObjectType: TrpObjectType;
//                                       aSelectObjectType: TrpObjectType;
//                                       aSelectedID: TIDList;
//                                       aIsSelectOneItem: boolean): Boolean;
//
//
//    function  Dlg_Select_ObjectByIDList (aIDList: TIDList): integer;   overload;
//    function  Dlg_Select_ObjectByIDList (aDataSet: TDataSet): integer; overload;
//////    procedure Dlg_Select_ItemsByIDList  (aIDList: TIDList; var aOutIdList: TIDList);   overload;
//    function Dlg_Select_ItemsByIDList(aDataSet: TDataSet; var aOutIdList: TIDList): Boolean;
//
//
//    function Dlg_Select_Item_ (aObjectType, aSelectObjectType: TrpObjectType;
//                              aSelectedIDList: TIDList): boolean;
//
//    function Dlg_Select_Items   (aObjectType: TrpObjectType;
//                                 aListID: TIDList): boolean;
//
//    function Dlg_Select_Items_on_Filter1 (aObjectType: TrpObjectType;
//                                         aListID: TIDList): boolean;
//    procedure Browser_ViewObjectByID (aObjectName: string; aID: integer);
//
////    function CreateDBExplorer  (aDest: TWinControl): IDBExplorerFormX;
// end;

 
(*
var
  IShell: IShellX;
*)


(*var
  IMainProjectExplorer: Tframe_Explorer_Container;
//  IDBExplorerFormX;
*)


type
  TShellEventRec = packed record
    ID      : Integer;
    ObjName : ShortString;
    Name    : ShortString;
    GUID    : ShortString;

    PIDL    : TObject;

    IDList  : TIDList;
  end;

type
  IShellNotifyX = interface(IInterface)
  ['{A6E004E1-FB43-4E79-B5CB-D9C6B389C8FD}']
    procedure GetShellMessage (aMsg: integer; aRec: TShellEventRec);
  end;



type
  TShellEventManager_ = class(TObjectList)
  private
    FRec: TShellEventRec;



  public
    procedure EXPLORER_ReLoad_Project;

    procedure PostEvent(aMsg: integer; aRec: TShellEventRec); overload;
    procedure PostEvent(aMsg: integer); overload;


    procedure RegisterObject(Sender: TObject);
    procedure UnRegisterObject(Sender: TObject);


    //------------------------------------------------------

//    procedure Shell_UpdateNodeChildren_ByGUID(aGUID: string); overload;
    procedure Shell_UpdateNodeChildren_ByObjName(aObjName: string; aID: integer); overload;

    procedure Shell_PostDelNode (aGUID: string);
    procedure Shell_PostDelNodeByList (aItemList: TIDList);
    procedure Shell_PostDelNodesByIDList(aIDList: TIDList);


    procedure Shell_EXPAND_BY_GUID(aGUID: string);

    procedure Shell_PostFocusNode (aGUID: string);

    procedure Shell_RENAME_ITEM(aGUID,aNewName: string);
    procedure Shell_ReLoad_Project();

    procedure Shell_OPEN_PIDL(aPIDL: TObject);

    procedure ShowCount;
//    procedure PostEvent(aMsg: integer); overload;

  end;

const

  //---------------------------------------------------
  // EXPLORER, shell
  //---------------------------------------------------

  WM__EXPLORER_UPDATE_NODE_BY_PIDL           =  2;
  WE__EXPLORER_UPDATE_NODE_CHILDREN_BY_OBJNAME=  15;
  WE__EXPLORER_UPDATE_NODE_CHILDREN_BY_GUID  =  55;
 // WM__EXPLORER_UPDATE_IF_EXPANDED            =  25;

  WE__EXPLORER_DELETE_NODE_BY_GUID           =  18;
  WE__EXPLORER_DELETE_NODES_BY_IDLIST        =  3;

  WE___EXPLORER_OPEN_PIDL                    = 5;

  WE__EXPLORER_EXPAND_BY_GUID                =  23;
 // WE__EXPLORER_COLLAPSE_BY_GUID              =  11;

//  WM__EXPLORER_REFRESH                       =  12;
  WM__EXPLORER_SELECT_NODE_BY_ID_LIST        =  13;
  WM__EXPLORER_SELECT_NODE_BY_ID_IN_FOLDER   =  14;

  WE__EXPLORER_RENAME_ITEM                   =  19;
  WE__EXPLORER_FOCUS_NODE_BY_GUID            =  17;

  WE__EXPLORER_RELOAD_PROJECT                =  34;
  WE__BROWSER_LIST_REVIEW                    =  95;



  //  WM__EXPLORER_DEL_NODE_BY_PIDL              =  4;
//  WM__EXPLORER_FOCUS_NODE_NO_UPDATE          =  24;
//  WE__EXPLORER_UPDATE_FOLDER_AND_FOCUS_CHILD =  16;
 // WE_PROJECT_CHANGED
 // WE_EXPLORER_UPDATE_NODE_BY_ID             = WM_USER + 22;

var
  g_ShellEvents: TShellEventManager_;

implementation


procedure TShellEventManager_.EXPLORER_ReLoad_Project;
begin

    PostEvent(WE__EXPLORER_ReLoad_Project);


  // TODO -cMM: TShellEventManager_.EXPLORER_ReLoad_Project default body inserted
end;

//--------------------------------------------------------------
procedure TShellEventManager_.RegisterObject(Sender: TObject);
//--------------------------------------------------------------
var v: IShellNotifyX;
begin
  Assert(IndexOf(Sender)<0, Format('classname: %s', [Sender.ClassName]));
  Assert(Sender.GetInterface(IShellNotifyX, v));

  Add(Sender);
end;


//--------------------------------------------------------------
procedure TShellEventManager_.UnRegisterObject(Sender: TObject);
//--------------------------------------------------------------
begin
  Assert(IndexOf(Sender)>=0);
  Extract(Sender);
end;


procedure TShellEventManager_.PostEvent(aMsg: integer);
begin
 // FillChar(Frec, SizeOf(Frec), 0);
  PostEvent (aMsg, FRec);
end;


procedure TShellEventManager_.ShowCount;
var
  I: Integer;
  S: string;
begin
  s:=s+ IntToStr(Count) + CRLF;

  for I := 0 to Count - 1 do
  begin
    s:=s+ Format('ClassName: %s', [Items[i].ClassName]) + CRLF;
  end;

  ShowMessage(s);
end;


procedure TShellEventManager_.Shell_ReLoad_Project;
begin
  PostEvent (WE__EXPLORER_ReLoad_Project);
end;


procedure TShellEventManager_.Shell_PostDelNode(aGUID: string);
begin
  FRec.GUID:=aGUID;
  PostEvent (WE__EXPLORER_DELETE_NODE_BY_GUID);
end;


procedure TShellEventManager_.Shell_EXPAND_BY_GUID(aGUID: string);
begin
  FRec.GUID:=aGUID;
  PostEvent (WE__EXPLORER_EXPAND_BY_GUID);
end;



procedure TShellEventManager_.Shell_PostDelNodeByList(aItemList: TIDList);
var
  i: Integer;
begin
  for I:=0 to aItemList.Count-1 do
    Shell_PostDelNode (aItemList[i].GUID);
end;


procedure TShellEventManager_.Shell_PostDelNodesByIDList(aIDList: TIDList);
begin
  FRec.IDList:=aIDList;
  PostEvent(WE__EXPLORER_DELETE_NODES_BY_IDLIST, FRec);
end;


procedure TShellEventManager_.Shell_PostFocusNode(aGUID: string);
begin
  FRec.GUID:=aGUID;
  PostEvent (WE__EXPLORER_FOCUS_NODE_BY_GUID);
end;



procedure TShellEventManager_.Shell_UpdateNodeChildren_ByObjName(aObjName: string; aID: integer);
begin
  FRec.ID:=aID;
  FRec.ObjName:=aObjName;
  PostEvent (WE__EXPLORER_UPDATE_NODE_CHILDREN_BY_OBJNAME);//, [app_Par(PAR_GUID, aGUID) ]);
end;

{
procedure TShellEventManager_.Shell_UpdateNodeChildren_ByGUID(aGUID: string);
begin
  FRec.GUID:=aGUID;
  PostEvent (WE__EXPLORER_UPDATE_NODE_CHILDREN_BY_GUID);//, [app_Par(PAR_GUID, aGUID) ]);

//  if Assigned(IMainProjectExplorer) then
 //   IMainProjectExplorer.UPDATE_NODE_CHILDREN(aGUID);

//  PostEvent (WE_EXPLORER_UPDATE_NODE_CHILDREN, [app_Par(PAR_GUID, aGUID) ]);
end;
}

procedure TShellEventManager_.Shell_RENAME_ITEM(aGUID,aNewName: string);
begin
  FRec.GUID:=aGUID;
  FRec.Name:=aNewName;

  PostEvent (WE__EXPLORER_RENAME_ITEM);//, [app_Par(PAR_GUID, aGUID) ]);

 // if Assigned(IMainProjectExplorer) then
  //  IMainProjectExplorer.RENAME_ITEM(aGUID, aNewName);

//   PostEvent (WE_EXPLORER_RENAME_ITEM,
 //         [app_Par(PAR_GUID, aGUID),
  //         app_Par(PAR_NAME, aNewName) ]);
end;



//-------------------------------------------------------------------
procedure TShellEventManager_.PostEvent(aMsg: integer; aRec: TShellEventRec);
//-------------------------------------------------------------------
var i: integer;
  v: IShellNotifyX;
begin
  Assert(aMsg>0);

 // try
    //------------------------------------------------------
    //�������� ������� - ��� ���������� ��� �������� ��������
    //------------------------------------------------------
   for i:=Count-1  downto 0 do
//    for i:=0 to Count-1 do
    if Items[i].GetInterface(IShellNotifyX, v) then
    begin
      v.GetShellMessage(aMsg, aRec);
//      v:=nil;

    end;
     // else
    //    raise Exception.Create('TShellEventManager_.PostEvent(aMsg: integer; aRec: TShellEventRec);' + Items[i].ClassName);

  {except
    on E: Exception do ShowMessage(E.Message);
  end;
}
end;


procedure TShellEventManager_.Shell_OPEN_PIDL(aPIDL: TObject);
begin
  FRec.PIDL:=aPIDL;
  PostEvent(WE___EXPLORER_OPEN_PIDL, FRec);
end;



initialization

  g_ShellEvents:=TShellEventManager_.Create;

finalization
  FreeAndNil(g_ShellEvents);

end.

