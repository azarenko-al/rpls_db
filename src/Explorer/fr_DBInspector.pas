unit fr_DBInspector;

interface

///////////////////////////////////////////////
{$DEFINE SHOW_FIELD_NAME_}
///////////////////////////////////////////////

uses
  Windows, Messages, SysUtils, Classes, dxmdaset,  

StrUtils,

  Graphics, Controls, Forms,
  ExtCtrls,  StdCtrls,  ToolWin, Menus, ActnList, DB, Dialogs,
  DBCtrls, Grids,   ComCtrls,  ADODB, RxMemDS, Variants, ImgList, Mask,
  cxVGrid, cxControls, cxInplaceContainer, cxCheckBox,  cxSpinEdit,
   cxPropertiesStore,  cxDropDownEdit, cxButtonEdit, cxTextEdit,
  cxDBLookupComboBox,  cxDBVGrid, DBGrids, cxPC, cxEdit ,


  dm_Onega_DB_data,
  u_debug,

  dm_DBManager,

  u_geo_convert_new,

  u_geo,

  u_Status_classes,

  u_cx_DBVGrid_manager,

  I_Shell,

  u_ViewEngine_classes,

  u_Inspector_common,

  dm_Status,
  dm_ViewEngine,

  u_Log,
  u_func,
  u_cx_vgrid,
  //
  u_db,
  u_files,
  u_dlg,
  u_reg,

  u_Storage,

  u_const,
  u_const_DB,

  u_types,

  cxGraphics, cxStyles, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinsDefaultPainters, cxDBExtLookupComboBox;

const
  TEST_MODE = True;

type
  Tframe_DBInspector = class(TForm)
    Panel1: TPanel;
    PopupMenu1: TPopupMenu;
    ActionList1: TActionList;
    act_DB_Post: TAction;
    act_DB_Cancel: TAction;
    actDBPost1: TMenuItem;
    actDBCancel1: TMenuItem;
    OpenDialog1: TOpenDialog;
    Panel2: TPanel;
    DBNavigator2: TDBNavigator;
    DBNavigator1: TDBNavigator;
    ds_Data: TDataSource;
    StatusBar2: TStatusBar;
    ImageList1: TImageList;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow3: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow1: TcxCategoryRow;
    cxDBVerticalGrid1DBEditorRow4: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow5: TcxDBEditorRow;
    SpinEdit: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow6: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow7: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow8: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow9: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow10: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow11: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow12: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow13: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow14: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow15: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow16: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow17: TcxDBEditorRow;
    ActionList2: TActionList;
    act_SetModeMultyColumn: TAction;
    actSetModeMultyColumn1: TMenuItem;
    PopupMenu2_test: TPopupMenu;
    N11: TMenuItem;
    N1: TMenuItem;
    test1: TMenuItem;
    viewmem1: TMenuItem;
    qry_Data: TADOQuery;
    pn_Read_only: TPanel;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    dxMemData1: TdxMemData;
    DataSource1: TDataSource;
    cxStyleRepository2: TcxStyleRepository;
    cxStyle_Edit: TcxStyle;
    cxStyle_ReadOnly: TcxStyle;
    cxStyle_Yellow: TcxStyle;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

    procedure DoOnXREFSelect(Sender: TObject; AbsoluteIndex: Integer);

    procedure dxDBInspector1Exit(Sender: TObject);
    procedure act_DB_PostExecute(Sender: TObject);
    procedure act_SetModeMultyColumnExecute(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure cxDBVerticalGrid1DrawValue(Sender: TObject; ACanvas: TcxCanvas;
        APainter: TcxvgPainter; AValueInfo: TcxRowValueInfo; var Done: Boolean);
//     procedure dxDBInspector1Click(Sender: TObject);
    procedure mem_Data11AfterPost(DataSet: TDataSet);
    procedure mem_Data_BeforeEdit(DataSet: TDataSet);
    procedure mem_Data11BeforeClose(DataSet: TDataSet);

    procedure N11Click(Sender: TObject);
    procedure viewmem1Click(Sender: TObject);
    procedure ds_DataDataChange(Sender: TObject; Field: TField);
    procedure qry_DataAfterPost(DataSet: TDataSet);
    procedure cxDBVerticalGrid1StylesGetContentStyle(Sender: TObject;
      AEditProp: TcxCustomEditorRowProperties; AFocused: Boolean;
      ARecordIndex: Integer; var AStyle: TcxStyle);

  protected
    FDataSet: TDataSet;

    FObjectName: string;
    FID: integer;

  private
    FObjectName2: string;

    FViewSQL : string;

    FLookupMemTables: TList;


    FDBVertGridManager: TcxDBVGridManager;

    FReadOnly : Boolean;


    FIsPostEditor: boolean;
    FUpdated: boolean;


    FADOQuery: TADOQuery;
    FOnPost: TNotifyEvent;

    FObjectType: TrpObjectType;
//    FOnLog: TOnLogEvent;
//    FFieldNames: TStringList;

    FFieldNamesModified: TStringList;
    FFieldNamesModified_new: TStringList;

    FOnButtonClick: TOnFieldChangedEvent;
    FOnFieldChanged: TOnFieldChangedEvent;

    FOnRename: TOnRenameEvent;


    procedure LoadFieldsFromDB ();

//    function GetStrListByFieldName11_____________(aFieldName: string): TStrings;

    procedure LoadStatusBar;

    function AddRow(aParentRow: TcxCustomRow; aFieldInfo: TViewEngFieldInfo): TcxCustomRow;

    procedure ClearLookupMemTables;
    procedure cxDBVerticalPropertiesGetDisplayText_for_coord(Sender:
        TcxCustomEditorRowProperties; ARecord: Integer; var AText: String);
    procedure DoOnButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure DoOnEditPropertiesChange(Sender: TObject);
    procedure DoOnPickCloseUp_CX(Sender: TObject);
    procedure DoOnValidate_CX(Sender: TObject; var DisplayValue: Variant; var
        ErrorText: TCaption; var Error: Boolean);
    function GetCoordFieldValue(aFieldName: string): string;
//    function GetInspectorRowByAlias_old(aAlias: string): TcxCustomRow;

    procedure PostData;
    function PrepareLookupMemTable(aStatusName: string; aText: string = ''):
        TDataSource;

    procedure SaveExpanedState;
    procedure SaveMemData1(aViewObject: TViewEngineObject);
  private
    FRegPath : string;


    BeforeEditText: Variant;
    FIsRenamed: boolean;

    FViewObject: TViewEngineObject;

    FOptions: record
      AutoCommit: boolean;
    end;

    FFieldList: TViewEngFieldInfoList;
  public

    procedure View (aID: integer);

    procedure SetReadOnly(Value: Boolean; aMsg: string = '');
    procedure SetAllowed(Value: Boolean);

    procedure Post();

    procedure ClearRows;
    procedure PrepareViewForObject(aObjectName: string; aObjectName2: string = '');

    function  GetFieldValue (aFieldName: string): Variant;
    function  GetIntFieldValue (aFieldName: string): Integer;
    function  GetStrFieldValue (aFieldName: string): string;
    function  GetFloatFieldValue(aFieldName: string): double;

    function GetInspectorRowByFieldName(aFieldName: string): TcxCustomRow;

// TODO: GetFieldInfoByFieldName111111
//  function GetFieldInfoByFieldName111111(aFieldName: string): TViewEngFieldInfo;
    procedure SetRowCaptionByFieldName(aFieldName,aCaption: string);

    procedure ExpandRowByFieldName(aFieldName: string; aVisible : boolean);
//    procedure CollapseRowByFieldName (aFieldName: string);

    procedure ExpandRowByAlias(aAlias: string; aExpanded: boolean);

    function GetBooleanFieldValue(aFieldName: string): boolean;
    function GetInspectorRowByAlias(aAlias: string): TcxCustomRow;
    procedure SetColor_Blank(aFieldName: string);
    procedure SetColor_Yellow(aFieldName: string);

    procedure SetFieldReadOnly(aFieldName: string; aValue: boolean);
    procedure SetMinMaxValues1(aFieldName: string; aMin, aMax: Integer);
    procedure SetRowVisibleByFieldName(aFieldName: string; aVisible : boolean);

    procedure SetStrFieldValue(aFieldName: string; aValue: string; aIsAddToList:
        boolean = true);
    procedure SetFieldValue(aFieldName: string; aValue: Variant; aIsAddToList:
        boolean = true);
    procedure SetRowVisibleByAlias(aAlias: string; aVisible : boolean);

    property OnRename: TOnRenameEvent read FOnRename write FOnRename;

    property OnPost:         TNotifyEvent         read FOnPost write FOnPost;
    property OnFieldChanged: TOnFieldChangedEvent read FOnFieldChanged write FOnFieldChanged;
    property OnButtonClick:  TOnFieldChangedEvent read FOnButtonClick write FOnButtonClick;

  end;


//============================================================================//
//============================================================================//
implementation
 {$R *.dfm}

uses
  dm_Act_Explorer; //dm_Act_Explorer,



const
  FLD_TYPE_LIST     = 'list';
  FLD_TYPE_MEMO     = 'comments';
  FLD_TYPE_DEGREE   = 'degree';

  FLD_TYPE_FILENAME  = 'filename';
  FLD_TYPE_FILEDIR   = 'filedir';
  FLD_TYPE_FILE      = 'file';
  FLD_TYPE_XREF      = 'xref';


//  FLD_ROW = 'ROW';


//--------------------------------------------------------------------
procedure Tframe_DBInspector.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
var
  iValue: Integer;
begin
  inherited;

  qry_Data.LockType := ltBatchOptimistic;

  FDataSet :=qry_Data;


 // cxDBVerticalGrid1.RowByName()


{  dxDBInspector1.ClearRows;
  dxDBInspector1.Align:=alClient;
}



//  FFieldNames:=TStringList.Create;

 /////////////////////// dmViewEngine.InitDB_ObjectFields (mem_Fields);

  with FOptions do begin
    AutoCommit:=True;
  end;

  cxDBVerticalGrid1.Align:=alClient;
///  cxDBVerticalGrid1.LookAndFeel.Kind:=

  cxDBVerticalGrid1.ClearRows;
  cxDBVerticalGrid1.OptionsView.RowHeaderWidth:=400;

  FFieldList := TViewEngFieldInfoList.Create;

  cx_InitDBVerticalGrid (cxDBVerticalGrid1);


  FDBVertGridManager := TcxDBVGridManager.Create(cxDBVerticalGrid1);
 // FDBVertGridManager.Init();

  FFieldNamesModified := TStringList.Create();
  FFieldNamesModified.Duplicates := dupIgnore;
  FFieldNamesModified.Sorted := True; ///!!!!!


  FFieldNamesModified_new := TStringList.Create();
  FFieldNamesModified_new.Duplicates := dupIgnore;
  FFieldNamesModified_new.Sorted := True; ///!!!!!


  FLookupMemTables := TList.Create();


  FRegPath := g_Storage.GetPathByClass(ClassName);

  cxDBVerticalGrid1.LayoutStyle := lsSingleRecordView;

(*
  iValue :=reg_ReadInteger(FRegPath, 'RowHeaderWidth', 300);

  cxDBVerticalGrid1.OptionsView.RowHeaderWidth := iValue;
  //  reg_ReadInteger(FRegPath, 'RowHeaderWidth', 300);

*)
  act_SetModeMultyColumn.Checked := reg_ReadBool(FRegPath, act_SetModeMultyColumn.Name, False);
//  act_SetModeMultyColumn.Execute;

  if act_SetModeMultyColumn.Checked then
    cxDBVerticalGrid1.LayoutStyle := lsBandsView;


 //  act_SetModeMultyColumn


end;

// ---------------------------------------------------------------
procedure Tframe_DBInspector.FormDestroy(Sender: TObject);
// ---------------------------------------------------------------
var
  I: Integer;
  oSList: TStringList;
  s: string;

begin


  ClearLookupMemTables();

  FreeAndNil(FLookupMemTables);

  FreeAndNil(FFieldNamesModified);
  FreeAndNil(FFieldNamesModified_new);
  FreeAndNil(FDBVertGridManager);
  Post();


  if act_SetModeMultyColumn.Tag<>0 then
    reg_WriteBool(FRegPath, act_SetModeMultyColumn.Name, act_SetModeMultyColumn.Checked);

 // reg_WriteInteger(FRegPath, 'RowHeaderWidth', cxDBVerticalGrid1.OptionsView.RowHeaderWidth);

   SaveExpanedState();


  FreeAndNil(FFieldList);

  inherited;
end;


//--------------------------------------------------------------------
procedure Tframe_DBInspector.SaveExpanedState;
//--------------------------------------------------------------------
var
  i: Integer;
  oCategory: TcxCategoryRow;
  oItem: TViewEngFieldInfo;
  oSList: TStringList;
  s: string;

begin

  oSList:=TStringList.Create;


  i:=cxDBVerticalGrid1.Rows.Count;

  for I := 0 to cxDBVerticalGrid1.Rows.Count - 1 do
    if cxDBVerticalGrid1.Rows[i] is TcxCategoryRow then
      if cxDBVerticalGrid1.Rows[i].Expanded then
    begin
      s:=cxDBVerticalGrid1.Rows[i].Name;

      oCategory:=cxDBVerticalGrid1.Rows[i] as TcxCategoryRow;
      s:=oCategory.Name;

      oItem:=TViewEngFieldInfo(oCategory.Tag);

      oSList.Add(IntToStr(oItem.ID));

//      if oItem.Name_<>'' then
  //      oSList.Add(oItem.Name_);
    end;



  FreeAndNil(oSList);

end;



//--------------------------------------------------------------------
procedure Tframe_DBInspector.PrepareViewForObject(aObjectName: string;
    aObjectName2: string = '');
//--------------------------------------------------------------------
begin
  if FObjectName=aObjectName then
    Exit;


//  FObjectName:=aObjectName;

  ClearRows;
  ClearLookupMemTables();

  FObjectName:=aObjectName;
  FObjectName2:=aObjectName2;

  if aObjectName='' then
    Exit;


  qry_Data.Fields.Clear;

  Assert(Assigned(g_Obj.ItemByName[aObjectName]));


  FObjectType:=g_Obj.ItemByName[aObjectName].Type_;

  FViewObject := dmViewEngine.PrepareObject1 (aObjectName); //FViewObject


//  FSQL:=dmViewEngine.GetTableSQLForFields(aObjectName,  FViewObject, FViewObject.ListFields, qry_Data);  //aMem_Fields,



  if FObjectName2<>'' then
    dmViewEngine.LoadMemData_Prepare_ViewFields (FObjectName2,  qry_Data, FViewObject, FViewSQL) //mem_Fields,
  else
    dmViewEngine.LoadMemData_Prepare_ViewFields (FObjectName,   qry_Data, FViewObject, FViewSQL); //mem_Fields,


  if Assigned(FViewObject) then
    LoadFieldsFromDB();

  //dxDBInspector1.FocusedNumber:=0;
end;


//--------------------------------------------------------------------
procedure Tframe_DBInspector.LoadFieldsFromDB ();
//--------------------------------------------------------------------

    //--------------------------------------------------------------------
    procedure DoAddListCX(aParentRow: TcxCustomRow; aList: TViewEngFieldInfoList);
    //--------------------------------------------------------------------
    var oRowInfo: TViewEngFieldInfo;
        oRow: TcxCustomRow;
        I: integer;
    begin


      //������� move to !!!!!!!!!!!!!

      if not Assigned(aParentRow) then
        for I := 0 to aList.Count - 1 do
        begin
          oRowInfo:=aList[i];
          oRow :=AddRow (aParentRow, oRowInfo);

          Assert(Assigned(oRow));

          oRow.Tag := Integer(oRowInfo);
       //   oRow.name := IntToStr(oRowInfo.ID);

          DoAddListCX(oRow, oRowInfo.SubFields);
        end

      else
        for I := aList.Count - 1 downto 0 do
        begin
          oRowInfo:=aList[i];
          oRow :=AddRow (aParentRow, oRowInfo);

          Assert(Assigned(oRow));

          oRow.Tag := Integer(oRowInfo);
       //   oRow.Name := IntToStr(oRowInfo.ID);

//          cxdbVerticalGrid1.

          DoAddListCX(oRow, oRowInfo.SubFields);
        end;
    end;

    //--------------------------------------------------------------------
    procedure DoLoadFields();
    //--------------------------------------------------------------------
    var
      I: Integer;
      oRowInfo: TViewEngFieldInfo;
    begin
      FFieldList.Clear;

      for I := 0 to FViewObject.ViewFields.Count - 1 do
        if not FViewObject.ViewFields[i].IsHidden then
      begin
        oRowInfo:=dmViewEngine.CreateFieldInfo (FViewObject.ViewFields[i]);
        FFieldList.Add(oRowInfo);

      end;

      FFieldList.ReOrder;
    end;

begin
  Assert(Assigned(FViewObject), 'Value not assigned');

  cxDBVerticalGrid1.BeginUpdate;

  ClearRows();

  DoLoadFields();

  DoAddListCX(nil, FFieldList);

  cxDBVerticalGrid1.EndUpdate;

end;

//============================================================================//
//  ������� ������ �����
//
//============================================================================//
(*
//--------------------------------------------------------------------
procedure Tframe_DBInspector.CollapseRowByFieldName (aFieldName: string);
//--------------------------------------------------------------------
var oRow: TcxCustomRow;
begin
  oRow:=GetInspectorRowByFieldName (aFieldName);
  if Assigned(oRow) then
    oRow.Collapse(True)
end;
*)

//--------------------------------------------------------------------
procedure Tframe_DBInspector.ExpandRowByFieldName(aFieldName: string; aVisible
    : boolean);
//--------------------------------------------------------------------
var oRow: TcxCustomRow;
begin
  oRow:=GetInspectorRowByFieldName (aFieldName);
(*  if Assigned(oRow) then
    oRow.Expand (True);
*)
  if Assigned(oRow) then
    case aVisible of
      True : oRow.Expand (False);
      False: oRow.Collapse (False);

(*      True : oRow.Expand (True);
      False: oRow.Collapse (True);
*)
    end;

end;

//--------------------------------------------------------------------
procedure Tframe_DBInspector.ExpandRowByAlias(aAlias: string; aExpanded:
    boolean);
//--------------------------------------------------------------------
var oRow: TcxCustomRow;
begin
  oRow:=GetInspectorRowByAlias (aAlias);
  if Assigned(oRow) then
    case aExpanded of
      True : oRow.Expand (False);
      False: oRow.Collapse (False);

(*      True : oRow.Expand (True);
      False: oRow.Collapse (True);
*)
    end;
end;


//--------------------------------------------------------------------
procedure Tframe_DBInspector.SetRowVisibleByFieldName(aFieldName: string; aVisible :
    boolean);
//--------------------------------------------------------------------
var oRow: TcxCustomRow;
begin
  oRow:=GetInspectorRowByFieldName (aFieldName);
  if Assigned(oRow) then
    oRow.Visible :=aVisible;
end;


//--------------------------------------------------------------------
procedure Tframe_DBInspector.SetRowVisibleByAlias(aAlias: string; aVisible :
    boolean);
//--------------------------------------------------------------------
var oRow: TcxCustomRow;
begin
  oRow:=GetInspectorRowByAlias (aAlias);

  Assert(Assigned(oRow), 'Tframe_DBInspector.SetRowVisibleByAlias: alias-'+ aAlias);

  if Assigned(oRow) then
  begin
    oRow.Visible :=aVisible;

  end;

end;



//--------------------------------------------------------------------
function Tframe_DBInspector.GetInspectorRowByFieldName(aFieldName: string): TcxCustomRow;
//--------------------------------------------------------------------
var
  I: integer;
  oItem: TViewEngFieldInfo;
  oRow: TcxDBEditorRow;
  oCategory: TcxCategoryRow;
 // sFieldName: string;
begin
  Result:=nil;

 // ShowMessage(IntToStr(cxDBVerticalGrid1.Rows.Count));

// cxDBVerticalGrid1.RowByName()


  for I := 0 to cxDBVerticalGrid1.Rows.Count - 1 do
  begin
    //---------------------------------------------------------
    if cxDBVerticalGrid1.Rows[i] is TcxDBEditorRow then
    begin
      oRow:=cxDBVerticalGrid1.Rows[i] as TcxDBEditorRow;
      oItem:=TViewEngFieldInfo(oRow.Tag);

//      sFieldName:=oRow.Properties.DataBinding.FieldName;

    end else

    //---------------------------------------------------------
    if cxDBVerticalGrid1.Rows[i] is TcxCategoryRow then
    begin
      oCategory:=cxDBVerticalGrid1.Rows[i] as TcxCategoryRow;
      oItem:=TViewEngFieldInfo(oCategory.Tag);

    end else
      raise Exception.Create('');


    if Eq(oItem.FieldName,aFieldName) then
    begin
      Result:=cxDBVerticalGrid1.Rows[i];
      Exit;
    end;
  end;

 // raise Exception.Create('function Tframe_DBInspector.GetInspectorRowByFieldName (aFieldName: string): TcxDBEditorRow;');
end;


//--------------------------------------------------------------------
function Tframe_DBInspector.GetInspectorRowByAlias(aAlias: string):
    TcxCustomRow;
//--------------------------------------------------------------------
var
  I: integer;
  oItem: TViewEngFieldInfo;
  oRow: TcxDBEditorRow;
  oCategory: TcxCategoryRow;
 // sFieldName: string;
begin

 // ShowMessage(IntToStr(cxDBVerticalGrid1.Rows.Count));

// cxDBVerticalGrid1.RowByName()


  for I := 0 to cxDBVerticalGrid1.Rows.Count - 1 do
  begin
    oItem:=TViewEngFieldInfo(cxDBVerticalGrid1.Rows[i].Tag);

    Assert(Assigned(oItem), 'Value not assigned');

    if Eq(oItem.Alias, aAlias) then
    begin
      Result:=cxDBVerticalGrid1.Rows[i] as TcxCustomRow;
      Exit;
    end;
  end;

  Result:=nil;


  
//  end;

 // raise Exception.Create('function Tframe_DBInspector.GetInspectorRowByFieldName (aFieldName: string): TcxDBEditorRow;');
end;

       

procedure Tframe_DBInspector.ClearRows;
begin
  cxDBVerticalGrid1.ClearRows;
end;


procedure Tframe_DBInspector.act_DB_PostExecute(Sender: TObject);
begin
  if Sender=act_DB_Post then
    db_PostDataset(FDataSet) else
//    db_PostDataset(mem_Data) else

  if Sender=act_DB_Cancel then
    FDataSet.Cancel;
    
//    mem_Data.Cancel;
end;


procedure Tframe_DBInspector.act_SetModeMultyColumnExecute(Sender: TObject);
begin
  act_SetModeMultyColumn.Tag:=1;

  act_SetModeMultyColumn.Checked:=not act_SetModeMultyColumn.Checked;

  cxDBVerticalGrid1.LayoutStyle :=
    IIF(act_SetModeMultyColumn.Checked, lsBandsView, lsSingleRecordView);


(*  if act_SetModeMultyColumn.Checked then
    cxDBVerticalGrid1.LayoutStyle := lsBandsView
  else
    cxDBVerticalGrid1.LayoutStyle := lsSingleRecordView;
*)

end;



//-------------------------------------------------------------------
procedure Tframe_DBInspector.DoOnValidate_CX(Sender: TObject; var DisplayValue:
    Variant; var ErrorText: TCaption; var Error: Boolean);
//-------------------------------------------------------------------
var
  sValue,sFieldName: string;
  oFieldInfo: TViewEngFieldInfo;
begin
  oFieldInfo:=TViewEngFieldInfo(cxDBVerticalGrid1.FocusedRow.Tag);

  sFieldName:=oFieldInfo.FieldName;

  sValue:=DisplayValue;

  if Assigned(FOnFieldChanged) then
  begin
    SetFieldValue (sFieldName, sValue);

    FOnFieldChanged(nil, sFieldName, sValue);
  end;

  if Eq(sFieldName, 'name') then
    FIsRenamed:=True;
end;


//-------------------------------------------------------------------
procedure Tframe_DBInspector.DoOnPickCloseUp_CX(Sender: TObject);
//-------------------------------------------------------------------
var
  i: integer;
  ind: integer;
  iValue: integer;
  sFieldName: string;

 // oRow: TdxInspectorDBPickRow;
  oFieldInfo: TViewEngFieldInfo;
  oComboBox: TcxComboBox;
  oLookupComboBox: TcxLookupComboBox;

  oStrings: TStrings;
  s: string;
begin

  oFieldInfo:=TViewEngFieldInfo(cxDBVerticalGrid1.FocusedRow.Tag);

  sFieldName:=oFieldInfo.FieldName;

//  ShowMessage(oComboBox.Properties.Items.Text);

 // ind:=-1;

 s:=Sender.className;

  case oFieldInfo.FieldType of
    vftStrList: begin
                  FFieldNamesModified.Add(sFieldName);


                  Exit;
                end;

    vftList:
               if Sender is TcxComboBox then
               begin
(*
                 oComboBox:=Sender as TcxComboBox;

                 oStrings :=(Sender as TcxComboBox).Properties.Items;

                 i:=oStrings.IndexOf (oComboBox.Text);
                 ind:=oFieldInfo.ListNames.IndexOf (oComboBox.Text);

                 //�������� ����������
                 if mem_Data.FieldValues[sFieldName] = ind then
                   Exit;

                 db_DatasetEdit (mem_Data);
                 mem_Data.FieldValues[sFieldName+'_str']:=oComboBox.Text;
                 mem_Data.FieldValues[sFieldName]:=ind;

*)               end;

  end;


//  iValue:=mem_Data.FieldByName(sFieldName).AsInteger;
  iValue:=FDataSet.FieldByName(sFieldName).AsInteger;
  if Assigned(FOnFieldChanged) then
    FOnFieldChanged(Self, sFieldName, iValue);


  //if Assigned(FOnFieldChanged) then
 //   FOnFieldChanged(Self, sFieldName, ind);

  FFieldNamesModified.Add(sFieldName);

end;


//-------------------------------------------------------------------
//  ����� ������ �� ������
//-------------------------------------------------------------------
procedure Tframe_DBInspector.DoOnXREFSelect (Sender: TObject; AbsoluteIndex: Integer);
//-------------------------------------------------------------------
var
  sXRef,sFieldName: string;
  iID: integer;

  sName: Widestring;
  sRowFieldName: string;
  ot: TrpObjectType;
  oFieldInfo: TViewEngFieldInfo;
  oXrefField: TField;

  oEditRow: TcxDBEditorRow;
begin
  oFieldInfo:=TViewEngFieldInfo(cxDBVerticalGrid1.FocusedRow.Tag);

  oEditRow:=cxDBVerticalGrid1.FocusedRow as TcxDBEditorRow;

  sRowFieldName:=oEditRow.Properties.DataBinding.FieldName;

  sName:=FDataSet.FieldByName(sRowFieldName).AsString;
  iID:=FDataSet.FieldByName(oFieldInfo.FieldName).AsInteger;

  ot:=g_Obj.ItemByName[oFieldInfo.XREF_ObjectName].Type_;
  if ot=otNone then
    raise Exception.Create('');

  case AbsoluteIndex of
    0: if not dmAct_Explorer.Dlg_Select_Object(ot, iID, sName) then
         Exit;

    1: begin
         iID:=0;
         sName:='';
       end;
  end;

  /////////////////////
  db_SetEditState(FDataSet);
  
//  if not (mem_Data.State in [dsEdit]) then
 //   mem_Data.Edit;

  FDataSet.FieldByName(sRowFieldName).AsString:=sName;

//  mem_Data.FieldValues[oFieldInfo.FieldName] := IIF_NULL(iID);

  SetFieldValue(oFieldInfo.FieldName, IIF_NULL(iID));

  if Assigned(FOnFieldChanged) then
//    FOnFieldChanged(Self, sFieldName, iID);
    FOnFieldChanged(Self, oFieldInfo.FieldName, iID);


  cxDBVerticalGrid1.HideEdit;

end;


//-------------------------------------------------------------------
procedure Tframe_DBInspector.DoOnButtonClick(Sender: TObject; AbsoluteIndex:
    Integer);
//-------------------------------------------------------------------
var
  sFieldName: string;
  sName: string;
//  oRow: TdxInspectorDBButtonRow;
  oFieldInfo: TViewEngFieldInfo;
begin
  oFieldInfo:=TViewEngFieldInfo(cxDBVerticalGrid1.FocusedRow.Tag);

  sFieldName:=oFieldInfo.FieldName;

  if Assigned(FOnButtonClick) then
  begin
    FOnButtonClick(Self, sFieldName, '');
    cxDBVerticalGrid1.HideEdit;
  end;
end;

//-------------------------------------------------------------------
procedure Tframe_DBInspector.View (aID: integer);
//-------------------------------------------------------------------
var sSQL,sTable: string;

begin
  Post();

  if not Assigned(FViewObject) then
    Exit;

  if FObjectType=otNone then

    raise Exception.Create('');

  FID:=aID;

  FUpdated:=True;
  CursorSQL;

  FFieldNamesModified.Clear;


  ds_Data.dataSet  := nil;

  if FObjectName2<>'' then
    dmViewEngine.LoadMemData (FObjectName2, aID, qry_Data, FViewSQL)
  else
    dmViewEngine.LoadMemData (FObjectName, aID,   qry_Data, FViewSQL );

  ds_Data.dataSet  := qry_Data;

  CursorDefault;
  FUpdated:=False;

  LoadStatusBar();
end;


//-------------------------------------------------------------------
procedure Tframe_DBInspector.LoadStatusBar;
//-------------------------------------------------------------------
const
  STR_1 = 'ID: %d | ������: %s %s | �������: %s %s';
var
  sName, sUserCr, sUserMod, sDateCr, sDateMod: string;
begin

//  with dmViewEngine.qry_Data do
  with FDataSet do
  //  if Assigned(FindField(FLD_DATE_CREATED)) then
    begin
      sDateCr :=FieldByName(FLD_DATE_CREATED).AsString;
      sUserCr :=FieldByName(FLD_USER_CREATED).AsString;

//      sDateMod:=FieldByName(FLD_DATE_MODIFY).AsString;
//      sUserMod:=FieldByName(FLD_USER_MODIFY).AsString;
    end;

  StatusBar2.SimpleText:=
    Format(STR_1, [FID, sUserCr, sDateCr, sUserMod, sDateMod]);

end;

// ---------------------------------------------------------------
procedure Tframe_DBInspector.Post;
// ---------------------------------------------------------------
begin    
  try
    if FDataSet.State = dsEdit then
      FDataSet.Post;
  except

  end;  // try/except
end;

// ---------------------------------------------------------------
procedure Tframe_DBInspector.SetFieldValue(aFieldName: string; aValue: Variant;
    aIsAddToList: boolean = true);
// ---------------------------------------------------------------
begin
//  if aIsAddToList then
 //   FFieldNamesModified.Add(aFieldName);

  db_DatasetEdit (FDataSet);


 // db_View(FDataSet);

  if Assigned(FDataSet.FindField(aFieldName)) then
  begin
    FDataSet.FieldByName(aFieldName).AsVariant:=aValue;

    if aIsAddToList then
      FFieldNamesModified.Add(aFieldName);

  end
  else
    begin
      g_Log.Error('Field not found: '+ aFieldName);

     // ShowMessage('Tframe_DBInspector.SetFieldValue: + ' + aFieldName);

     // db_View(FDataSet);

    end;
//    raise Exception.Create('Field not found: '+ aFieldName);

//    g_Log.Error('Field not found: '+ aFieldName);

end;


// ---------------------------------------------------------------
procedure Tframe_DBInspector.SetStrFieldValue(aFieldName: string; aValue:
    string; aIsAddToList: boolean = true);
// ---------------------------------------------------------------
begin
  SetFieldValue (aFieldName, aValue, aIsAddToList);

end;


function Tframe_DBInspector.GetFieldValue(aFieldName: string): Variant;
begin
  if Assigned(FDataSet.Fields.FindField (aFieldName)) then
    Result:=FDataSet[aFieldName]
  else
    Result:=null;
end;


function Tframe_DBInspector.GetFloatFieldValue(aFieldName: string): double;
begin
  Result:=AsFloat(GetFieldValue(aFieldName));
end;

function Tframe_DBInspector.GetBooleanFieldValue(aFieldName: string): boolean;
begin
  Result:=AsBoolean(GetFieldValue(aFieldName));
end;


function Tframe_DBInspector.GetIntFieldValue(aFieldName: string): Integer;
begin
  Result:=AsInteger(GetFieldValue(aFieldName));
end;


function Tframe_DBInspector.GetStrFieldValue(aFieldName: string): string;
begin
  Result:=AsString(GetFieldValue(aFieldName));
end;



  // ---------------------------------------------------------------
procedure Tframe_DBInspector.SetAllowed(Value: Boolean);
// ---------------------------------------------------------------
begin
  SetReadOnly (not Value);
end;

// ---------------------------------------------------------------
procedure Tframe_DBInspector.SetReadOnly(Value: Boolean; aMsg: string = '');
// ---------------------------------------------------------------
begin
  FReadOnly  := Value;

//  with dxDBInspector1 do  Options:=Options - [dioEditing];
  cxDBVerticalGrid1.OptionsData.Editing :=not Value;

  pn_Read_only.Visible :=Value;
//  pn_Read_only.Caption:='������ ��� ������: ' + aMsg;
  pn_Read_only.Caption:='������ ��� ������';


//  with cxDBVerticalGrid1 do
//    OptionsView:=OptionsView - [dioEditing];


end;


procedure Tframe_DBInspector.mem_Data_BeforeEdit(DataSet: TDataSet);
begin
  FIsRenamed:=False;
end;


procedure Tframe_DBInspector.mem_Data11BeforeClose(DataSet: TDataSet);
begin
  g_Log.SysMsg('Tframe_DBInspector.mem_DataBeforeClose(DataSet: TDataSet) - Post();');

  Post();   
end;


//-------------------------------------------------------------------
procedure Tframe_DBInspector.SetMinMaxValues1(aFieldName: string; aMin, aMax:
    Integer);
//-------------------------------------------------------------------
var oRow: TcxCustomRow;
   oEditRow: TcxDBEditorRow;
begin

  oRow:= GetInspectorRowByFieldName(aFieldName);

 // ShowMessage(oRow.ClassName);

  if assigned ( oRow ) then
    if oRow is TcxDBEditorRow then
    begin
      oEditRow:=(oRow as TcxDBEditorRow);

      if oEditRow.Properties.EditPropertiesClass = TcxSpinEditProperties then
      begin
        TcxSpinEditProperties(oEditRow.Properties.EditProperties).MinValue:=aMin;
        TcxSpinEditProperties(oEditRow.Properties.EditProperties).MaxValue:=aMax;
      end;
    end;
end;


procedure Tframe_DBInspector.dxDBInspector1Exit(Sender: TObject);
begin
  if FOptions.AutoCommit then
    Post();
end;

// ---------------------------------------------------------------
procedure Tframe_DBInspector.SetRowCaptionByFieldName(aFieldName,aCaption: string);
// ---------------------------------------------------------------
var  oRow: TcxCustomRow;
begin

/////////  ShowMessage('SetRowCaptionByFieldName ') ;

  oRow:= GetInspectorRowByFieldName(aFieldName);
  if assigned (oRow) then
  begin
//    ShowMessage(oRow.ClassName);

    if oRow is TcxDBEditorRow then
     (oRow as TcxDBEditorRow).Properties.Caption:=aCaption;

  end else
    g_Log.AddError (ClassName, 'SetRowCaptionByFieldName - aFieldName: '+aFieldName);

end;

// ---------------------------------------------------------------
procedure Tframe_DBInspector.SetFieldReadOnly(aFieldName: string; aValue:
    boolean);
// ---------------------------------------------------------------
var
  oRow: TcxCustomRow;
begin
  oRow:=GetInspectorRowByFieldName (aFieldName);
  if assigned(oRow) then
    (oRow as TcxDBEditorRow).Properties.Options.Editing:=aValue;
end;


// -------------------------------------------------------------------
procedure Tframe_DBInspector.mem_Data11AfterPost(DataSet: TDataSet);
// -------------------------------------------------------------------
begin
  PostData();

(*  if FUpdated then
    Exit;

//  dmViewEngine.SaveMemData1 (FObjectName, FID, mem_Data,
  SaveMemData1 (FViewObject);

//  FFieldNamesModified.Clear;

  if Assigned(FOnPost)then
     FOnPost(Self);

  if FIsRenamed then
    if Assigned(FOnRename)then
      FOnRename (Self, mem_Data.FieldByName(FLD_GUID).AsString,
                       mem_Data.FieldByName(FLD_NAME).AsString);

*)
end;



// -------------------------------------------------------------------
procedure Tframe_DBInspector.PostData;
// -------------------------------------------------------------------
begin
  if FUpdated then
    Exit;

//  dmViewEngine.SaveMemData1 (FObjectName, FID, mem_Data,
  SaveMemData1 (FViewObject);

//  FFieldNamesModified.Clear;

  if Assigned(FOnPost)then
     FOnPost(Self);

  if FIsRenamed then
    if Assigned(FOnRename)then
      FOnRename (Self, FDataSet.FieldByName(FLD_GUID).AsString,
                       FDataSet.FieldByName(FLD_NAME).AsString);

end;



//--------------------------------------------------------------------
function Tframe_DBInspector.AddRow(aParentRow: TcxCustomRow; aFieldInfo: TViewEngFieldInfo):
    TcxCustomRow;
//--------------------------------------------------------------------
var
  I: integer;
  sNewFieldName: string;
  bIsCategory: Boolean;
  oEditRow: TcxDBEditorRow;

  oButton: TcxEditButton;
  oStatusItem: TStatusItem;
  s: string;

  oDataSource: TDataSource;

  oLookupComboBoxProperties: TcxLookupComboBoxProperties;
  oComboBoxProperties: TcxComboBoxProperties;
  oButtonEditProperties: TcxButtonEditProperties;


  oCheckBoxProperties: TcxCheckBoxProperties;

  oSpinEditProperties: TcxSpinEditProperties;

  oTextEditProperties: TcxTextEditProperties;

begin
 // if Eq(aFieldInfo.FieldName, FLD_POWER_LOSS) then
 //   I:=0;

  bIsCategory:=aFieldInfo.IsFolder or (aFieldInfo.FieldType=vftDelimiter);


  if bIsCategory then
  begin
    Result := FDBVertGridManager.AddCategory(aParentRow, aFieldInfo.Caption);
    Exit;
  end;

  Result := FDBVertGridManager.AddRow(aParentRow, aFieldInfo.Caption, aFieldInfo.FieldName);

  oEditRow:=Result as TcxDBEditorRow;


//  if Eq(aFieldInfo.FieldName, 'power_max') then
  //  s:='';


  case aFieldInfo.FieldType of

    //vftStrList,
   // vftList

    vftList,
    vftStatus
    : begin
             //  if Eq(aFieldInfo.FieldName, 'address2') then
               begin
                 oEditRow.Properties.EditPropertiesClass:=TcxLookupComboBoxProperties;

                 oLookupComboBoxProperties:=TcxLookupComboBoxProperties(oEditRow.Properties.EditProperties);

               //  oLookupComboBoxProperties.OnCloseUp := DoOnPickCloseUp_CX;

               //  with TcxLookupComboBoxProperties(oEditRow.Properties.EditProperties) do
               //  begin
//                   if aFieldInfo.FieldType<>vftStrList then
                    oLookupComboBoxProperties.OnCloseUp := DoOnPickCloseUp_CX;

                   // if aFieldInfo.FieldType<>vftStrList then
                    //  OnCloseUp := DoOnPickCloseUp_CX;

                    oLookupComboBoxProperties.ImmediatePost := True;
                    oLookupComboBoxProperties.ListOptions.ShowHeader := False;

                    oLookupComboBoxProperties.UseMouseWheel := False;
                    oLookupComboBoxProperties.DropDownRows  :=20;
                    oLookupComboBoxProperties.DropDownListStyle:=lsFixedList;

                    case aFieldInfo.FieldType of
                 //     vftStrList,vftList:
                 //       oDataSource := PrepareLookupMemTable('',aFieldInfo.ListNames.Text);

                      vftList:
                         oDataSource := PrepareLookupMemTable('',aFieldInfo.ListNames.Text);

                      vftStatus:
                      begin
                        Assert(aFieldInfo.StatusName<>'');

                        oDataSource := PrepareLookupMemTable(aFieldInfo.StatusName,'');
                      end;
                    end;

                  //  db_View(oDataSource.Dataset);

                    oLookupComboBoxProperties.KeyFieldNames  :='id';
                    oLookupComboBoxProperties.ListFieldNames :='name';
                    oLookupComboBoxProperties.ListSource     := oDataSource;
               //  end;

               end;
             end;


 //   vftStatus, !!!!!!!!!!!!!
  //////
    vftStrList
   // vftList
    : begin
               oEditRow.Properties.EditPropertiesClass:=TcxComboBoxProperties;

               oComboBoxProperties:=TcxComboBoxProperties(oEditRow.Properties.EditProperties);

             //  with TcxComboBoxProperties(oEditRow.Properties.EditProperties) do
             //  begin
//                  if aFieldInfo.FieldType<>vftStrList then
                 oComboBoxProperties.OnCloseUp := DoOnPickCloseUp_CX;

         //   OnValidate:=DoOnValidate_CX;

                 // OnValidate:=DoOnValidate_CX;

                  oComboBoxProperties.ImmediatePost := True;

                  oComboBoxProperties.UseMouseWheel := False;
                  oComboBoxProperties.DropDownRows  :=20;
                  oComboBoxProperties.DropDownListStyle:=lsFixedList;

                //  ImmediatePost := True;

                  /////////////////////////
                //  case  aFieldInfo.FieldType of
                   // vftStrList: ;

                  //  vftList:
                   //   oDataSource := PrepareLookupMemTable('',aFieldInfo.ListNames.Text);
                 //   vftStatus:
                  //    oDataSource := PrepareLookupMemTable(aFieldInfo.StatusName,'');
                //  end;
                  /////////////////////////


              //    if aFieldInfo.FieldType in [vftStrList,vftList] then
                    oComboBoxProperties.Items.Text:=aFieldInfo.ListNames.Text ;

              // end;
             end;


    vftBoolean: begin
                  oEditRow.Properties.EditPropertiesClass:=TcxCheckBoxProperties;

                  oCheckBoxProperties:=TcxCheckBoxProperties(oEditRow.Properties.EditProperties);

//                  with TcxCheckBoxProperties(oEditRow.Properties.EditProperties) do
  //                begin
                    oCheckBoxProperties.NullStyle := nssUnchecked;

//                    OnValidate:=DoOnValidate_CX;
                //    OnChange:=cxDBVerticalGrid1DBEditorRow5EditPropertiesEditValueChanged;
                    oCheckBoxProperties.OnEditValueChanged:=DoOnEditPropertiesChange;
                    oCheckBoxProperties.UseAlignmentWhenInplace:=True;



                    //OnEditValueChanged:=cxDBVerticalGrid1DBEditorRow5EditPropertiesChange;

              //    end;
                end;

    vftButton: begin
                 oEditRow.Properties.EditPropertiesClass:=TcxButtonEditProperties;
                 oEditRow.Properties.Options.Editing:=True;

                 oButtonEditProperties:=TcxButtonEditProperties(oEditRow.Properties.EditProperties);

                // with TcxButtonEditProperties(oEditRow.Properties.EditProperties) do
                // begin
                   oButtonEditProperties.Alignment.Horz:=taLeftJustify;

                   oButtonEditProperties.OnButtonClick:=DoOnButtonClick;
                 //  Buttons.Add;

                   oButtonEditProperties.ReadOnly:=True;
                // end;
               end;

    vftXRef:
               begin
                oEditRow.Properties.EditPropertiesClass:=TcxButtonEditProperties;

                oButtonEditProperties:=TcxButtonEditProperties(oEditRow.Properties.EditProperties);
               // .Alignment.Horz:=taLeftJustify;

               // with TcxButtonEditProperties(oEditRow.Properties.EditProperties) do
               // begin
                  oButtonEditProperties.Alignment.Horz:=taLeftJustify;
                  oButtonEditProperties.ReadOnly:=True;

                  if not aFieldInfo.IsReadOnly then
                  begin
                      oButtonEditProperties.OnButtonClick:=DoOnXREFSelect;
                    //  ReadOnly:=True;
                      oButtonEditProperties.Buttons.Add;

                      // delete button
                      oButtonEditProperties.Buttons[1].Kind:=bkGlyph;
                     // oButtonEditProperties.
                      ImageList1.GetBitmap(0, oButtonEditProperties.Buttons[1].Glyph);
                  end;
              //  end;
               end;

    vftFloat,
    vftInt:
              begin
                oEditRow.Properties.EditPropertiesClass:=TcxSpinEditProperties;

                oSpinEditProperties:=TcxSpinEditProperties(oEditRow.Properties.EditProperties);

                oSpinEditProperties.OnValidate:=DoOnValidate_CX;
                oSpinEditProperties.Alignment.Horz:=taLeftJustify;
                oSpinEditProperties.UseMouseWheel:=False;
                oSpinEditProperties.UseCtrlIncrement:=True;

                if aFieldInfo.Min_Value<>aFieldInfo.Max_Value then
                begin
                  oSpinEditProperties.MinValue:=aFieldInfo.Min_Value ;
                  oSpinEditProperties.MaxValue:=aFieldInfo.Max_Value ;
                end;

                oSpinEditProperties.SpinButtons.ShowFastButtons:=False;

                 case aFieldInfo.FieldType of
                    vftFloat: oSpinEditProperties.ValueType := vtFloat;
                    vftInt  : oSpinEditProperties.ValueType := vtInt;
                 end;

                 oEditRow.Properties.Options.ShowEditButtons :=eisbNever;

               //  oSpinEditProperties.Options.


(*
                with TcxSpinEditProperties(oEditRow.Properties.EditProperties) do
                begin
                  Alignment.Horz:=taLeftJustify;

                  OnValidate:=DoOnValidate_CX;

                 // OnEditValueChanged:=SpinEditEditPropertiesEditValueChanged;

                  UseMouseWheel := False;
                  UseCtrlIncrement:=True;

                  Min
                end;
*)

             end;


  else  begin
          oEditRow.Properties.EditPropertiesClass:=TcxTextEditProperties;

          oTextEditProperties:=TcxTextEditProperties(oEditRow.Properties.EditProperties);

       //   with TcxTextEditProperties(oEditRow.Properties.EditProperties) do
         // begin
            oTextEditProperties.OnValidate:=DoOnValidate_CX;
            oTextEditProperties.Alignment.Horz:=taLeftJustify;
         // end;
        end;
  end;


  sNewFieldName:='';

  case aFieldInfo.FieldType of
    vftXRef:        sNewFieldName:=aFieldInfo.FieldName + '_str';
    vftList:        sNewFieldName:=aFieldInfo.FieldName;
//    vftList:        sNewFieldName:=aFieldInfo.FieldName + '_str';
    vftStatus:      sNewFieldName:=aFieldInfo.FieldName;

    vftDegree,
    vftDegreeLat,
    vftDegreeLon:  //begin
                     sNewFieldName:=aFieldInfo.FieldName + '_str';
                    // oDBRow.ReadOnly:=True;
                  // end;

  end;

  if sNewFieldName<>'' then
    oEditRow.Properties.DataBinding.FieldName:=sNewFieldName;


  if
     Eq(aFieldInfo.FieldName, FLD_lat_lon_full_str) or

     Eq(aFieldInfo.FieldName, FLD_lat_lon_CK_42) or
     Eq(aFieldInfo.FieldName, FLD_lat_lon_CK_42_str) or
     Eq(aFieldInfo.FieldName, FLD_lat_lon_CK_95) or
     Eq(aFieldInfo.FieldName, FLD_lat_lon_CK_95_str) or
     Eq(aFieldInfo.FieldName, FLD_lat_lon_WGS_84) or
     Eq(aFieldInfo.FieldName, FLD_lat_lon_WGS_84_str)
  then
  begin
//     oEditRow.Properties.OnGetDisplayText := cxDBVerticalPropertiesGetDisplayText_for_coord;

     oEditRow.Properties.EditProperties.ReadOnly:=True;
//     oEditRow.Properties.EditProperties.ReadOnly:=False;
  end;


  if aFieldInfo.IsReadOnly then
    oEditRow.Properties.EditProperties.ReadOnly:=True;

//    oEditRow.Properties.Options.Editing:=False;


(*                with TcxButtonEditProperties(oEditRow.Properties.EditProperties) do
                begin
                  Alignment.Horz:=taLeftJustify;
                  ReadOnly:=True;
*)

//    SetFieldReadOnly();


  {$IFDEF SHOW_FIELD_NAME}
  oEditRow.Properties.Caption:=oEditRow.Properties.Caption +
                               ' / '+ oEditRow.Properties.DataBinding.FieldName;

  {$ENDIF}

end;

procedure Tframe_DBInspector.Button2Click(Sender: TObject);
begin
//  db_View(FDataSet);
end;

procedure Tframe_DBInspector.Button3Click(Sender: TObject);
begin
  cxDBVerticalGrid1.OptionsView.RowHeaderWidth := 600;
end;

procedure Tframe_DBInspector.Button4Click(Sender: TObject);
begin
//  dxMemData1.ass

end;


//--------------------------------------------------------------------
function Tframe_DBInspector.PrepareLookupMemTable(aStatusName: string; aText:
    string = ''): TDataSource;
//--------------------------------------------------------------------
const
  DEF_LookupMemTable=111;
var
  I: Integer;
  oDataSource: TDataSource;
  oMemoryData: TRxMemoryData;
  oStrList: TStringList;
  s: string;
begin
  if (aStatusName='') and (aText='') then
  begin
    g_Log.AddError (ClassName, '(aStatusName=) and (aText=)');
    Exit;
  end;


  oMemoryData:=TRxMemoryData.Create(nil);
  oMemoryData.Tag :=DEF_LookupMemTable;
  InsertComponent(oMemoryData);

  oDataSource:=TDataSource.Create(nil);
  oDataSource.DataSet := oMemoryData;
  oDataSource.Tag:=DEF_LookupMemTable;
  InsertComponent(oDataSource);

  Result := oDataSource;


  db_CreateField(oMemoryData,
        [db_Field(FLD_ID,    ftInteger),
         db_Field(FLD_NAME,  ftString, 100) ]);

  if aStatusName<>'' then
    dmStatus.AssignDataset(oMemoryData, aStatusName)
  else

  if aText<>'' then
  begin
    oStrList:=TStringList.Create;
    oStrList.Text :=aText;

    for I := 0 to oStrList.Count - 1 do
      db_AddRecord_(oMemoryData,
             [FLD_NAME, oStrList[i],
              FLD_ID,   i
              ]);

    oStrList.Free;
  end;

  if oMemoryData.RecordCount=0 then
  begin
    s:='PrepareLookupMemTable - Status=0 for '+ aStatusName;
    g_Log.AddError (ClassName, s);
  //  ShowMessage(ClassName +' ' +s);

  end;

end;


//--------------------------------------------------------------------
procedure Tframe_DBInspector.ClearLookupMemTables;
//--------------------------------------------------------------------
const
  DEF_LookupMemTable=111;
var
  I: Integer;
//  oMemoryData: TRxMemoryData;
begin
  for I := ComponentCount - 1 downto 0 do
    if Components[i].Tag=DEF_LookupMemTable then
    begin
      Components[i].Free;
//      FreeAndNil(Components[i]);

//      RemoveComponent(Components[i]);
    end;
end;

procedure Tframe_DBInspector.cxDBVerticalGrid1DrawValue(Sender: TObject;
    ACanvas: TcxCanvas; APainter: TcxvgPainter; AValueInfo: TcxRowValueInfo;
    var Done: Boolean);
begin

end;


//-------------------------------------------------------------------
procedure Tframe_DBInspector.SaveMemData1(aViewObject: TViewEngineObject);
//-------------------------------------------------------------------
var
  I: Integer;
  sTableName,sFieldName: string;
  ot: TViewEngFieldType;
  oField: TField;

  oParamList: TdbParamList;
  s: string;

  oObjectTypeItem: TObjectTypeItem;

begin
  //FObjectName, FID, FViewObject

  g_Log.Add(FFieldNamesModified.Text);

//  ShowMessage('TdmViewEngine.SaveMemData1 --'+ FFieldNamesModified.Text);
                   
  if FFieldNamesModified.Count=0 then
    Exit;

  oObjectTypeItem :=g_Obj.ItemByName[FObjectName];

  Assert(Assigned(oObjectTypeItem), 'Value not assigned');

  if oObjectTypeItem.TableName_for_update<>'' then
    sTableName:=oObjectTypeItem.TableName_for_update
  else
    sTableName:=oObjectTypeItem.TableName;
                         
//  sTableName:=g_Obj.ItemByName[aObjectName].TableName;

  oParamList:=TdbParamList.Create;

  for I := 0 to FFieldNamesModified.Count - 1 do
  begin
    s:=FFieldNamesModified[i];
    oParamList.AddItem (s, FDataSet.FieldByName(s).AsVariant);
  end;

  s:=db_MakeUpdateString(sTableName, FID, oParamList);
  g_Log.Add(s);


//  dmDBManager.UpdateRecord (sTableName, FID, oParamList);
  dmOnega_DB_data.UpdateRecordByID_list (sTableName, FID, oParamList);

  FreeAndNil(oParamList);

  FFieldNamesModified.Clear;

end;


procedure Tframe_DBInspector.DoOnEditPropertiesChange(Sender: TObject);
var
  oFieldInfo: TViewEngFieldInfo;
begin
  oFieldInfo:=TViewEngFieldInfo(cxDBVerticalGrid1.FocusedRow.Tag);

//  if  then

  FFieldNamesModified.Add(oFieldInfo.FieldName);

end;

procedure Tframe_DBInspector.N11Click(Sender: TObject);
begin
 ///// db_View(FDataSet);

end;

procedure Tframe_DBInspector.viewmem1Click(Sender: TObject);
begin
 //// db_View(FDataSet);
end;


procedure Tframe_DBInspector.cxDBVerticalPropertiesGetDisplayText_for_coord(
    Sender: TcxCustomEditorRowProperties; ARecord: Integer; var AText: String);
var
  s: string;
begin
  if (Sender is TcxDBEditorRowProperties) then
  begin
    s:= TcxDBEditorRowProperties(Sender).DataBinding.FieldName;// = 'Letzte') then

    AText :=  GetCoordFieldValue(s);
  end;

end;



procedure Tframe_DBInspector.ds_DataDataChange(Sender: TObject; Field: TField);
begin
  if not Assigned(Field) then
    Exit;

  if RightStr(Field.FieldName, 4)<>'_str' then
  begin
    FFieldNamesModified_new.Add(Field.FieldName);

    g_Log.SysMsg('FFieldNamesModified_new--'+ Field.FieldName);

  end;

end;

// ---------------------------------------------------------------
function Tframe_DBInspector.GetCoordFieldValue(aFieldName: string): string;
// ---------------------------------------------------------------
var
  bl_CK42: TBLPoint;
  bl: TBLPoint;
  i: Integer;
  s: string;

begin
    {
  i:=FDataSet.RecordCount;


  // ---------------------------------------------------------------
  if Assigned(FDataSet.FindField(FLD_LAT))and
     Assigned(FDataSet.FindField(FLD_LON)) then
  begin
    // initial coordinate
    bl_CK42 := db_ExtractBLPoint(FDataSet);

    if (bl_CK42.B=0) or (bl_CK42.L=0) then
      Exit;


    Result := dmViewEngine.GetCoordFieldValue(aFieldName, bl_CK42);

  end;
  }

end;

procedure Tframe_DBInspector.qry_DataAfterPost(DataSet: TDataSet);
begin
  PostData();
end;



procedure Tframe_DBInspector.cxDBVerticalGrid1StylesGetContentStyle(
  Sender: TObject; AEditProp: TcxCustomEditorRowProperties;
  AFocused: Boolean; ARecordIndex: Integer; var AStyle: TcxStyle);
begin
  if FReadOnly then
    AStyle :=cxStyle1;

end;


procedure Tframe_DBInspector.SetColor_Yellow(aFieldName: string);
var
  oRow: TcxCustomRow;
begin
  oRow:=GetInspectorRowByFieldName(aFieldName);
  if Assigned(oRow) then
    TcxDBEditorRow (oRow).Styles.Content:=cxStyle_Yellow;

end;


procedure Tframe_DBInspector.SetColor_Blank(aFieldName: string);
var
  oRow: TcxCustomRow;
begin
  oRow:=GetInspectorRowByFieldName(aFieldName);
  if Assigned(oRow) then
    TcxDBEditorRow (oRow).Styles.Content:=nil;

end;


end.


(*

procedure Tframe_DBInspector_Container.SetColor_Yellow(aFieldName: string);


    if dmOnega_DB_data.sp_GroupAssign_Select_subitems(ADOStoredProc1,  aRec.Object_field_id) > 0 then
      ADOStoredProc1.Locate(FLD_ID, aRec.Result.id, []);

    Result := ShowModal=mrOk;

    if Result then
    begin
      aRec.Result.ID    := ADOStoredProc1.FieldByName(FLD_ID).AsInteger;
      aRec.Result.Value := ADOStoredProc1.FieldByName(FLD_NAME).AsString;
    end;
*)