unit fr_Browser_simple_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  ToolWin, Dialogs, rxPlacemnt, ExtCtrls, DB, ADODB, ComCtrls, Menus,
  ActnList, DBCtrls, StdCtrls, cxPropertiesStore, ImgList,

  fr_View_base,

  I_Shell,
 // I_DBInspector,

  fr_DBInspector_Container,

  u_func,
  u_dlg, dxBar, cxClasses, cxBarEditItem, cxLookAndFeels
//  u_db,


  ;

type
  Tframe_Browser_Simple_View = class(Tframe_View_Base)
    pn_Inspector: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
//    Ffrm_DBInspector: IDBInspectorFormX;
    Ffrm_DBInspector: Tframe_DBInspector_Container;

//    Tframe_DBInspector;

  public
    procedure SetObjectName (aName: string); override;
    procedure View(aID: integer; aGUID: string); override;
  end;


//==================================================================
// implementation
//==================================================================
implementation {$R *.dfm}



//--------------------------------------------------------------------
procedure Tframe_Browser_Simple_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

//  Assert(Assigned(IShell));
    

//  db_SetComponentADOConn (Self, dmMain.ADOConnection);

  pn_Inspector.Align:=alClient;

 // Ffrm_DBInspector:=dmAct_Explorer.CreateDBInspector (pn_Inspector,'');// TDBInspectorFormX_Create;
//
  CreateChildForm(Tframe_DBInspector_Container, Ffrm_DBInspector, pn_Inspector);

 //zzzzzzz Ffrm_DBInspector:=Tframe_DBInspector.CreateChildForm (pn_Inspector);

//  CopyControls(Ffrm_DBInspector, pn_Inspector);
end;


//--------------------------------------------------------------------
procedure Tframe_Browser_Simple_View.View(aID: integer; aGUID: string);
//--------------------------------------------------------------------
begin
  inherited View(aID,aGUID);

  Ffrm_DBInspector.View (aID);
///////////////  Ffrm_DBInspector.DoAfterView;

end;


procedure Tframe_Browser_Simple_View.FormDestroy(Sender: TObject);
begin
  FreeAndNil(Ffrm_DBInspector);
//  Ffrm_DBInspector.Free;
 // Ffrm_DBInspector:=nil;

  inherited;
end;


procedure Tframe_Browser_Simple_View.SetObjectName(aName: string);
begin
  inherited SetObjectName(aName);

  Ffrm_DBInspector.PrepareViewForObject (aName);
end;



end.
