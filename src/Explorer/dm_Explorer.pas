unit dm_Explorer;

interface
{$I ver.inc}

{$DEFINE LinkFreqPlan}


uses
  SysUtils, Classes, DB, ADODB, Forms,  RxMemDS, Dialogs,

 u_Config,

  dm_Onega_DB_data,

  I_DBExplorerForm,
  dm_Main,

  u_types,

  u_db,
  u_func,
  u_log,

  u_const_db,
  u_const_str,
  //u_types,

  u_Explorer
  ;


type
  TdmExplorerObjectSet = set of TrpObjectType;


  TdmMemItemRec = record

    Checked       : Boolean;

    ref_ID        : integer;
    ref_ObjName   : string;

    ID            : integer;
//    Name          : Widestring;
    Name          : string;
    ObjectName    : string;

    Owner         : string;

    IsFolder      : boolean;

    HasChildren   : boolean;

    Guid_str      : string;

//    GeoRegion_ID_  : integer;
//    GeoRegion1_ID_ : integer;
//    GeoRegion2_ID_  : integer;
  end;


  //-------------------------------------------------------
  TdmExplorer = class(TDataModule)
  //-------------------------------------------------------
    mem_Items: TRxMemoryData;
    qry_Temp: TADOQuery;
    ADOQuery1: TADOQuery;
    q_SubItems: TADOQuery;
    ADOStoredProc1: TADOStoredProc;
    q_Filter: TADOQuery;

    procedure DataModuleCreate(Sender: TObject);
  private
    FViewObjects: TrpObjectTypeArr;

    procedure InsertDataset(aDataset: TDataset);
    procedure InsertFolderItems(aObjectName: string; aFolderID: integer; aFlags:
        TExplorerFlags);

    procedure InsertItems(aSQL, aObjectName: string; aFlags: TExplorerFlags);
    procedure InsertItems2(aObjectName: string; aID: Integer; aFolderID: Integer = 0);
// TODO: InsertItems3
    procedure InsertItems3(aObjectName: string; aFolder_ID: Integer);

    procedure InsertSysObject(aObjectType: TrpObjectType);
    procedure InsertSysObject_new(aObjectType: TrpObjectType);


    procedure InsertMemRecord (aRec: TdmMemItemRec);



    function  MakeFolderListSQL (aTableName  : string;
                                 aFilterField: string;
                                 aFilterValue: integer): string;

    function MakeSelectFoldersSQL (aObjectName  : string;
                                    aParentID   : integer
                                    ): string;
  public
    Options: set of TdmExplorerOption;
    Style: TdmExplorerStyle;
    // ������� ��� �����������

    procedure Build (aPIDL: TrpPIDL);
    function GetDataset: TRxMemoryData;

    procedure SetViewObjects(aValue: TrpObjectTypeArr);

  end;


function dmExplorer: TdmExplorer;


//================================================================
implementation {$R *.dfm}
//================================================================

var
  FdmExplorer: TdmExplorer;

const
  FLD_SORTED_STR = 'SORTED_STR';

 

// ---------------------------------------------------------------
function dmExplorer: TdmExplorer;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmExplorer) then
      FdmExplorer := TdmExplorer.Create(Application);

  Result := FdmExplorer;
end;



//----------------------------------------------------------------
procedure TdmExplorer.DataModuleCreate(Sender: TObject);
//----------------------------------------------------------------
begin
  inherited;


  db_SetComponentADOConnection(Self, dmMain.ADOConnection1);


  db_CreateField (mem_Items,
       [
        db_Field(FLD_CHECKED,      ftBoolean),

        db_Field(FLD_ID,           ftInteger),
        db_Field(FLD_Is_Folder,    ftBoolean),

        db_Field(FLD_NAME,         ftString, 200),
//        db_Field(FLD_NAME,         ftWideString),

//        db_Field(FLD_COMMENT,      ftString),

        db_Field(FLD_OBJECT_NAME,  ftString, 50),
        db_Field(FLD_Has_Children, ftBoolean),

        db_Field(FLD_GUID,         ftString),
        db_Field(FLD_PARENT_GUID,  ftString),

        db_Field(FLD_SORTED_STR,   ftString),

//        db_Field(FLD_GEOREGION_ID,  ftInteger),

//        db_Field(FLD_GEOREGION1_ID,  ftInteger),
//        db_Field(FLD_GEOREGION2_ID,  ftInteger),

        db_Field(FLD_REF_ID,       ftInteger),
        db_Field(FLD_REF_OBJNAME,  ftString, 50),

        db_Field(FLD_Owner,  ftString, 50)
       ]);
end;

//------------------------------------------------------------
procedure TdmExplorer.InsertFolderItems(aObjectName: string; aFolderID:  integer; aFlags: TExplorerFlags);
//------------------------------------------------------------
var sSQL,sFolderSQL: string;
begin    
  // folders
  sFolderSQL:=MakeSelectFoldersSQL (aObjectName,aFolderID);//,aFlags);

  InsertItems (sFolderSQL, aObjectName, [flIsFolder]);

  InsertItems3(aObjectName, aFolderID);

end;

//----------------------------------------------------------------
procedure TdmExplorer.InsertItems(aSQL, aObjectName: string; aFlags: TExplorerFlags);
//----------------------------------------------------------------
var

  bIsFolder, bHasChildren: boolean;
  bFolderFieldExist, bChildCountFieldExist: boolean; //, bCommentFieldExist

//  iGeoRegion1_ID: Integer;
//  iGeoRegion2_ID: Integer;

//  iGeoRegion_ID: Integer;

  rec: TdmMemItemRec;

begin
  dmOnega_DB_data.OpenQuery (qry_Temp, aSQL,
       [FLD_PROJECT_ID, dmMain.ProjectID]);

  if qry_Temp.IsEmpty then
    Exit;


  bFolderFieldExist:=     Assigned(qry_Temp.FindField(FLD_IS_FOLDER));
  bChildCountFieldExist:= Assigned(qry_Temp.FindField('child_count'));

//  bGeoRegionID_Field_Exists:=Assigned(qry_Temp.FindField(FLD_GeoRegion_ID));


  with qry_Temp do
    while not EOF do
  begin
    if FieldbyName(FLD_GUID).AsString='' then
      g_Log.AddError('procedure TdmExplorer.InsertItems(aSQL, aObjectName: string; aFlags: TExplorerFlags);','GUID=''''; ObjectName:'+aObjectName);

 //
    bIsFolder:=(flIsFolder in aFlags);
    bHasChildren:=False;

//    iGeoRegion_ID :=0;
//    iGeoRegion1_ID :=0;
//    iGeoRegion2_ID :=0;

    if bFolderFieldExist then
      bIsFolder:=FieldByName(FLD_IS_FOLDER).AsBoolean;

  {
    if Assigned(qry_Temp.FindField(FLD_GeoRegion_ID)) then
      iGeoRegion_ID:=FieldByName(FLD_GeoRegion_ID).AsInteger ;

    if Assigned(qry_Temp.FindField(FLD_GeoRegion1_ID)) then
      iGeoRegion1_ID:=FieldByName(FLD_GeoRegion1_ID).AsInteger ;

    if Assigned(qry_Temp.FindField(FLD_GeoRegion2_ID)) then
      iGeoRegion2_ID:=FieldByName(FLD_GeoRegion2_ID).AsInteger ;
   }


    bHasChildren:=(flRoot in aFlags) or
                  (
                    bChildCountFieldExist and
                   (FindField('child_count').AsInteger > 0)
                  );

    FillChar(rec, SizeOf(rec), 0);



    rec.ID             :=FieldByName(FLD_ID).AsInteger;
    rec.Name           :=FieldByName(FLD_NAME).AsString;
    rec.ObjectName     :=aObjectName;
    rec.IsFolder       :=bIsFolder;
    rec.HasChildren    :=bHasChildren;
    rec.Guid_str       :=FieldbyName(FLD_GUID).AsString;

//    rec.GeoRegion_ID   :=iGeoRegion_ID;
//    rec.GeoRegion1_ID  :=iGeoRegion1_ID;
//    rec.GeoRegion2_ID  :=iGeoRegion2_ID;
  //  rec.Owner

    if aObjectName=OBJ_PROJECT then
    begin
//      rec.Name:=rec.Name + Format(' [%d]', [FieldByName(FLD_ID).AsInteger]);
      rec.Name:='������: '+ rec.Name;

      rec.Guid_str :=GUID_PROJECT;
    end;

    InsertMemRecord (rec);

    Next;
  end;
end;


//----------------------------------------------------------------
procedure TdmExplorer.InsertItems2(aObjectName: string; aID: Integer; aFolderID: Integer
    = 0);
//----------------------------------------------------------------
var
 
  bIsFolder, bHasChildren: boolean;
  bFolderFieldExist, bChildCountFieldExist: boolean; //, bCommentFieldExist
  rec: TdmMemItemRec;
  
  bHas_Children_FieldExist: boolean;
  k: Integer;

begin


  k:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc1,
         'sp_Explorer_Select_SubItems',
         [FLD_ObjName,    aObjectName,
          FLD_ID,         aID,

          FLD_PROJECT_ID, dmMain.ProjectID

         ]);


  InsertDataset(ADOStoredProc1);// (q_SubItems);


  with q_SubItems do
    while not EOF do
  begin

    FillChar(rec, SizeOf(rec), 0);

    rec.ID             :=FieldByName(FLD_ID).AsInteger;
    rec.Name           :=FieldByName(FLD_NAME).AsString;
    rec.ObjectName     :=FieldByName(FLD_ObjName).AsString;
  //  rec.IsFolder       :=bIsFolder;

    if Assigned(FindField(FLD_USER_CREATED)) then
      rec.Owner :=FieldByName(FLD_USER_CREATED).AsString;


    if Assigned(FindField(FLD_REF_ID)) then
      rec.REF_ID    :=FieldByName(FLD_REF_ID).AsInteger;

    if Assigned(FindField(FLD_REF_OBJNAME)) then
      rec.REF_OBJNAME    :=FieldByName(FLD_REF_OBJNAME).AsString;

    if Assigned(FindField(FLD_Has_Children)) then
      rec.HasChildren    :=FieldByName(FLD_Has_Children).AsBoolean;

    rec.Guid_str       :=FieldbyName(FLD_GUID).AsString;

  //  if aObjectName=OBJ_PROJECT then
  //    rec.Name:=rec.Name + Format(' [%d]', [FieldByName(FLD_ID).AsInteger]);

    if Assigned(qry_Temp.FindField(FLD_Checked)) then
      rec.Checked      :=FieldByName(FLD_Checked).AsBoolean;


    InsertMemRecord (rec);


    Next;
  end;
end;


//----------------------------------------------------------------
procedure TdmExplorer.InsertDataset(aDataset: TDataset);
//----------------------------------------------------------------
var
 // iGeoRegion_ID: Integer;
 // bGeoRegionFieldExist: boolean;
  bIsFolder, bHasChildren: boolean;
//  bFolderFieldExist, bChildCountFieldExist: boolean; //, bCommentFieldExist
  rec: TdmMemItemRec;
//  bHas_Children_FieldExist: boolean;

begin

  with aDataset do
    while not EOF do
  begin
    FillChar(rec, SizeOf(rec), 0);

    rec.ID             :=FieldByName(FLD_ID).AsInteger;
    rec.Name           :=FieldByName(FLD_NAME).AsString;
    rec.ObjectName     :=FieldByName(FLD_ObjName).AsString;

  //  rec.IsFolder       :=bIsFolder;

{
    if Assigned(FindField(FLD_GeoRegion_ID)) then
      rec.GeoRegion_ID :=FieldByName(FLD_GeoRegion_ID).AsInteger;

    if Assigned(FindField(FLD_GeoRegion1_ID)) then
      rec.GeoRegion1_ID :=FieldByName(FLD_GeoRegion1_ID).AsInteger;

    if Assigned(FindField(FLD_GeoRegion2_ID)) then
      rec.GeoRegion2_ID :=FieldByName(FLD_GeoRegion2_ID).AsInteger;
}

    if Assigned(FindField(FLD_USER_CREATED)) then
      rec.Owner :=FieldByName(FLD_USER_CREATED).AsString;


    if Assigned(FindField(FLD_REF_ID)) then
      rec.REF_ID    :=FieldByName(FLD_REF_ID).AsInteger;

    if Assigned(FindField(FLD_REF_OBJNAME)) then
      rec.REF_OBJNAME    :=FieldByName(FLD_REF_OBJNAME).AsString;

    if Assigned(FindField(FLD_Has_Children)) then
      rec.HasChildren    :=FieldByName(FLD_Has_Children).AsBoolean;

    if Assigned(FindField(FLD_CHECKED)) then
      rec.CHECKED    :=FieldByName(FLD_CHECKED).AsBoolean;

    rec.Guid_str       :=FieldbyName(FLD_GUID).AsString;

    InsertMemRecord (rec);

   // Assert (rec.Guid_str<>'', 'aObjectName:'+aObjectName+'; id:'+IntToStr(rec.ID));

    Next;
  end;
end;



//----------------------------------------------------------------
procedure TdmExplorer.InsertSysObject(aObjectType: TrpObjectType);
//----------------------------------------------------------------
var rec: TdmMemItemRec;

    iProjectID: integer;

    oObjTypeItem: TObjectTypeItem;
  sFilter: string;
  sName: string;
begin
  oObjTypeItem:=g_Obj.ItemByType[aObjectType];

  Assert(Assigned(oObjTypeItem), 'Value not assigned');

  if oObjTypeItem.IsUseProjectID
    then iProjectID:=dmMain.ProjectID
    else iProjectID:=0;

  if aObjectType=otProperty then
  begin
//  ShowMessage ('procedure TdmExplorer.InsertSysObject(aObjectType: TrpObjectType); 1111111111111');


    dmOnega_DB_data.OpenQuery(q_Filter, 'SELECT ui_explorer.fn_Filter_value_v1()');
    if q_Filter.RecordCount>0 then
    begin
      sFilter:=q_Filter.Fields[0].Value;
      sName  :=oObjTypeItem.RootFolderName + IIF((sFilter<>''), ' ('+sFilter+')', '');

//      g_Shell.RENAME_ITEM_(sFolderGUID, sName);

    end;

  end;


  {
    dmOnega_DB_data.OpenQuery(q_Filter, 'SELECT ui_explorer.fn_Filter_value()');
  if q_Filter.RecordCount>0 then
  begin
    sFilter:=q_Filter.Fields[0].Value;
    sName  :='��������'+ IIF((sFilter<>''), ' ('+sFilter+')', '');

    g_Shell.RENAME_ITEM_(sFolderGUID, sName);

  end;
  }

  FillChar(rec, SizeOf(rec), 0);


  assert(oObjTypeItem.RootFolderName<>'');

  rec.ID         :=0;
  rec.Name       :=oObjTypeItem.RootFolderName;

  if aObjectType=otProperty then
    rec.Name:=sName;

  rec.ObjectName :=oObjTypeItem.name;
  rec.IsFolder   :=True;
  rec.Guid_str   :=oObjTypeItem.RootFolderGUID;
  rec.HasChildren:=   dmOnega_DB_data.Folder_Has_Children(oObjTypeItem.Name, 0, iProjectID);     //1477

  if aObjectType=otStatus then
     rec.IsFolder := False;

  InsertMemRecord (rec);
end;

//----------------------------------------------------------------
procedure TdmExplorer.InsertSysObject_new(aObjectType: TrpObjectType);
//----------------------------------------------------------------
var rec: TdmMemItemRec;

    oObjTypeItem: TObjectTypeItem;
begin
  oObjTypeItem:=g_Obj.ItemByType[aObjectType];

  Assert(Assigned(oObjTypeItem), 'Value not assigned');

{
  if oObjTypeItem.IsUseProjectID
    then iProjectID:=dmMain.ProjectID
    else iProjectID:=0;}

  FillChar(rec, SizeOf(rec), 0);

  rec.ID         :=0;
  rec.Name       :=oObjTypeItem.RootFolderName;
  rec.ObjectName :=oObjTypeItem.name;
  rec.IsFolder   :=True;
  rec.Guid_str   :=oObjTypeItem.RootFolderGUID;
  rec.HasChildren:=True;

  InsertMemRecord (rec);
end;


function TdmExplorer.GetDataset: TRxMemoryData;
begin
  Result := mem_Items;
end;

//----------------------------------------------------------------
procedure TdmExplorer.InsertMemRecord (aRec: TdmMemItemRec);
//----------------------------------------------------------------
var
  S: string;
begin
//  Assert (aRec.Guid_str<>'', 'ObjectName:'+aRec.Type_+', id:'+IntToStr(aRec.ID));

  if aRec.Guid_str='' then
    g_Log.AddError('procedure TdmExplorer.InsertMemRecord (aRec: TdmMemItemRec);','aRec.Guid_str='' ''; Object name:'+aRec.ObjectName);

  s:= Format('%2d %s', [Integer(not aRec.IsFolder),aRec.Name]);

  db_AddRecord_ (mem_Items,
      [
       FLD_ID,           aRec.ID,
       FLD_NAME,         aRec.Name,
       FLD_OBJECT_NAME,  aRec.ObjectName,
       FLD_HAS_ChILDREN, aRec.HasChildren,
       FLD_IS_FOLDER,    aRec.IsFolder,
       FLD_GUID,         aRec.Guid_str,

       FLD_CHECKED,      aRec.CHECKED,

//       FLD_GEOREGION_ID,  aRec.GEOREGION_ID,
//       FLD_GEOREGION1_ID, aRec.GEOREGION1_ID,
//       FLD_GEOREGION2_ID, aRec.GEOREGION2_ID,

       FLD_OWNER,        aRec.OWNER,

       FLD_REF_ID,       aRec.ref_ID,
       FLD_REF_OBJNAME,  aRec.ref_ObjName,

       FLD_SORTED_STR,   s
      ]);
end;


//-------------------------------------------------------------------------------------
function TdmExplorer.MakeFolderListSQL (aTableName  : string;

                                        aFilterField: string;
                                        aFilterValue: integer
                                        ): string;
//-------------------------------------------------------------------------------------
var sWhere: string;
begin
  Result:='SELECT id,name,guid FROM :table :where ORDER BY name';

  if aFilterField<>''
    then sWhere:=Format('WHERE %s=%d', [aFilterField, aFilterValue])
    else sWhere:='';

  Result:=ReplaceStr(Result, ':table', aTableName);
  Result:=ReplaceStr(Result, ':where', sWhere);
end;

//----------------------------------------------------------------
function TdmExplorer.MakeSelectFoldersSQL (aObjectName : string;
                                            aParentID   : integer
                                           ): string;
//----------------------------------------------------------------
var
  sWhereProject: string;
  oItem: TObjectTypeItem;
  sTableName: string;
begin
  Assert(aObjectName<>'');

  oItem:=g_Obj.ItemByName[aObjectName];

  if not Assigned(oItem) then
  begin
    g_log.Error('function TdmExplorer.MakeSelectFoldersSQL : ObjectName='+aObjectName);
    Exit;
  end;


  sTableName:=oItem.TableName;

  if oItem.IsUseProjectID
      then sWhereProject:=Format(' AND (project_id=%d)',[dmMain.ProjectID])
      else sWhereProject:='';

//flIncludeComment
  Result:= Format(
       'SELECT id,name,guid, ' +
       ' (folder_count + (SELECT Count(*) FROM '+sTableName+' WHERE folder_id=f.id)) AS child_count'+
       ' FROM '+ VIEW_FOLDER + ' f'+
       ' WHERE (type=''%s'') and '+
       '       ( ((%d = 0) and (parent_id IS NULL)) or (parent_id = %d) ) '+
       '        %s  '+
       ' ORDER BY name',  // SQL 2005 !!!!!
        //where
       [aObjectName, aParentID,aParentID, sWhereProject]);
end;

//----------------------------------------------------------------
procedure TdmExplorer.Build (aPIDL: TrpPIDL);
//----------------------------------------------------------------


    //------------------------------------------------------------
    procedure DoLinkNet_Link_Ref; // begin
    //------------------------------------------------------------
    begin
      if aPIDL.IsFolder then
      begin
        InsertFolderItems (OBJ_LINKNET, aPIDL.id,  [flGetChildCount]);
      end else
        InsertItems2 (OBJ_LINK, aPIDL.ref_ID);
    end;

    //------------------------------------------------------------
    procedure DoLinkNet_LinkLine_Ref; // begin
    //------------------------------------------------------------
    begin
      if aPIDL.IsFolder then
      begin
        InsertFolderItems (OBJ_LINKNET, aPIDL.id,  [flGetChildCount]);
      end else
        InsertItems2 (OBJ_LINKLINE, aPIDL.ref_ID);
    end;


    //------------------------------------------------------------
    procedure DoLinkFreqPlan_Link;
    //------------------------------------------------------------
 //   var
   //   iID: Integer;
    begin

        if aPIDL.IsFolder then
        begin
          InsertFolderItems (OBJ_LinkFreqPlan_LINK, aPIDL.id, [flGetChildCount]);

        end else begin
          Assert(aPIDL.ref_ID>0, 'Value <=0');

          InsertItems2 (OBJ_LINK, aPIDL.ref_ID);

        end;



    end;


    //------------------------------------------------------------
    procedure Do_PMPCalcRegion_Site_Link;
    //------------------------------------------------------------
    begin
      InsertItems2 (OBJ_PMP_SITE, aPIDL.ref_ID);
    end;


    //------------------------------------------------------------
    procedure DoCalcRegionPmp;
    //------------------------------------------------------------
    begin
      if aPIDL.IsFolder then
      begin                              //_PMP
        InsertFolderItems (OBJ_PMP_CALC_REGION, aPIDL.id, [flGetChildCount]);
      end else
        InsertItems2 (OBJ_PMP_CALC_REGION, aPIDL.ID);

    end;


    //------------------------------------------------------------
    procedure DoSite;
    //------------------------------------------------------------
    begin
      if aPIDL.IsFolder then
      begin
        InsertFolderItems (OBJ_SITE, aPIDL.id,  [flGetChildCount])
      end else
        InsertItems2 (OBJ_SITE, aPIDL.ID);
    end;


    //------------------------------------------------------------
    procedure DoProperty;
    //------------------------------------------------------------
    begin
      if aPIDL.IsFolder then
        InsertFolderItems (OBJ_PROPERTY, aPIDL.id, [flGetChildCount])
      else
        InsertItems2 (OBJ_PROPERTY, aPIDL.ID);
    end;


    // ---------------------------------------------------------------
    procedure DoPMPTerminal;
    // ---------------------------------------------------------------
    begin
      if aPIDL.IsFolder then
        InsertFolderItems (OBJ_PMP_TERMINAL, aPIDL.id,  [flGetChildCount])
      else
        InsertItems2 (OBJ_PMP_TERMINAL, aPIDL.ID);
    end;

    //------------------------------------------------------------
    procedure DoPmpSite;
    //------------------------------------------------------------
    begin
      if aPIDL.IsFolder then
        InsertFolderItems (OBJ_PMP_SITE, aPIDL.id,  [flGetChildCount])
      else
        InsertItems2 (OBJ_Pmp_Site, aPIDL.ID);
    end;

    // ---------------------------------------------------------------
    procedure DoPMPSector;
    // ---------------------------------------------------------------
    begin
      InsertItems2 (OBJ_PMP_Sector, aPIDL.ID);
    end;

    //------------------------------------------------------------
    procedure DoLinkNet;
    //------------------------------------------------------------
    begin
      if aPIDL.IsFolder then
        InsertFolderItems (OBJ_LINKNET, aPIDL.ID,  [flGetChildCount])
      else
        InsertItems2 (OBJ_LinkNet, aPIDL.ID);
    end;

    //------------------------------------------------------------
    procedure DoLinkEnd;
    //------------------------------------------------------------
    begin
 { TODO : bug }
      if aPIDL.IsFolder then
      begin
        InsertFolderItems (OBJ_LINKEND, aPIDL.id, [flGetChildCount]);
      end else
        InsertItems2 (OBJ_LINKEND, aPIDL.ID);
    end;

    // ---------------------------------------------------------------
    procedure DoTemplate_Pmp_Site;
    // ---------------------------------------------------------------
    begin
      if aPIDL.IsFolder then
        InsertFolderItems (OBJ_TEMPLATE_PMP_SITE,  aPIDL.id, [flGetChildCount])
      else
        InsertItems2 (OBJ_TEMPLATE_PMP_SITE, aPIDL.ID);

    end;


    // ---------------------------------------------------------------
    procedure DoTemplate_Link;
    // ---------------------------------------------------------------
    begin
      if aPIDL.IsFolder then
        InsertFolderItems (OBJ_TEMPLATE_Link,  aPIDL.id, [flGetChildCount])
      else
        InsertItems2 (OBJ_TEMPLATE_Link, aPIDL.ID);

    end;

{
    // ---------------------------------------------------------------
    procedure DoTemplate_Site;
    // ---------------------------------------------------------------
    begin
      if aPIDL.IsFolder then
        InsertFolderItems (OBJ_TEMPLATE_SITE,  aPIDL.id, [flGetChildCount])
      else
        InsertItems2 (OBJ_TEMPLATE_SITE, aPIDL.ID);

    end;
 }

    // ---------------------------------------------------------------
    procedure DoTemplate_Site_linkend;
    // ---------------------------------------------------------------
    begin
        if aPIDL.IsFolder then
        begin
          InsertFolderItems (OBJ_Template_site_linkend_ANTENNA, aPIDL.id, []);
        end else
          InsertItems2 (OBJ_Template_site_linkend, aPIDL.ID);

    end;

    // ---------------------------------------------------------------
    procedure DoLink_SDB;
    // ---------------------------------------------------------------
    begin
      if aPIDL.IsFolder then
        InsertFolderItems (OBJ_LINK_SDB, aPIDL.id, [flGetChildCount])
      else
        InsertItems2 (OBJ_LINK_SDB, aPIDL.ID);
    end;

    // ---------------------------------------------------------------
    procedure DoLink;
    // ---------------------------------------------------------------
    begin
      if aPIDL.IsFolder then
        InsertFolderItems (OBJ_LINK, aPIDL.id, [flGetChildCount])
      else
        InsertItems2 (OBJ_LINK, aPIDL.ID);
    end;

    // ---------------------------------------------------------------
    procedure DoLinkLine;
    // ---------------------------------------------------------------
    begin
       if aPIDL.IsFolder then
      begin
        InsertFolderItems (OBJ_LINKLINE, aPIDL.ID,  [flGetChildCount]);
      end else
        InsertItems2 (OBJ_LINKLINE, aPIDL.ID);
    end;

    // ---------------------------------------------------------------
    procedure DoLinkFreqPlan;
    // ---------------------------------------------------------------
    begin
      if aPIDL.IsFolder then
        InsertFolderItems (OBJ_LinkFreqPlan,  aPIDL.ID,  [flGetChildCount])
      else
        InsertItems2 (OBJ_LinkFreqPlan, aPIDL.ID);
    end;

    // ---------------------------------------------------------------
    procedure DoLinkLine_Link;
    // ---------------------------------------------------------------
    begin
      InsertItems2 (OBJ_LINK, aPIDL.ref_ID);
    end;

   //------------------------------------------------------------
    procedure DoLinkRepeater;
    //------------------------------------------------------------
    begin

     if aPIDL.IsFolder then
      begin
        InsertFolderItems (OBJ_LINK_Repeater, aPIDL.ID,  [flGetChildCount]);
      end else
        InsertItems2 (OBJ_LINK_Repeater, aPIDL.ID);

    end;


    // ---------------------------------------------------------------
    procedure DoTemplate_FreqPlan;
    // ---------------------------------------------------------------
    begin
        if aPIDL.IsFolder then
        begin
          InsertFolderItems (OBJ_Template_FreqPlan, aPIDL.id, []);
        end else
          InsertItems2 (OBJ_Template_FreqPlan, aPIDL.ID);

    end;


var
  sSQL,sFolderSQL,s: string;
  aID,
  iID,iPropID,iFolderID,i: integer;
  obj_type: TrpObjectType;

  rec: TdmMemItemRec;

  oItem: TObjectTypeItem;
  sTableName: string;
//begin
  //Assert(aObjectName<>'');

begin
  mem_Items.Close;
  mem_Items.Open;
 // mem_Items.SortedField:= '';

  if aPIDL=nil then
    obj_type:=otNone
  else
  begin
    if aPIDL.ObjectName='' then
      Exit;


    Assert(aPIDL.ObjectName<>'', 'Value not assigned');

    oItem:=g_Obj.ItemByName[aPIDL.ObjectName];

    Assert(Assigned(oItem), 'Value not assigned: ' + aPIDL.ObjectName);

    obj_type:=oItem.Type_; //g_Obj.ItemByName [aPIDL.ObjectName]
  end;

  if Assigned(aPIDL) then aID:=aPIDL.ID
                     else aID:=0;

  case obj_type of
    //------------------------------------------------------------
    otNone: begin // root level
    //------------------------------------------------------------
      case Style of
        esProject:begin
                    if dmMain.ProjectID=0 then
                      Exit;

                    sSQL:=MakeFolderListSQL (TBL_PROJECT, FLD_ID, dmMain.ProjectID);
                    InsertItems (sSQL, OBJ_PROJECT, [flRoot]);
                  end;

        esDict:   begin
                    FillChar(rec,SizeOf(rec),0);

                    rec.ID         :=0;
                    rec.Name       :=DN_FOLDER_DICT;
                    rec.ObjectName :=OBJ_DICT;
                    rec.IsFolder   :=True;
                    rec.HasChildren:=True;
                    rec.Guid_str   :=GUID_DICTS;

                    InsertMemRecord (rec);
                  end;

        esObject: for i:=0 to High(FViewObjects) do
                    InsertSysObject (FViewObjects[i]);
      end;
    end;

    //------------------------------------------------------------
    otProject: begin
    //------------------------------------------------------------
      iID:=aPIDL.ID;

      // common for projects
      case Style of
        esObject : InsertFolderItems (OBJ_PROJECT,  aID, []);

        esProject: begin

                     for i:=0 to High(FViewObjects) do
                       InsertSysObject (FViewObjects[i]);

                     //  PROJECT_LINK_TYPES

                      InsertSysObject_new(otLINK_GROUP);

                      if not g_Config.IsInterfaceSimple then
                      begin
                        InsertSysObject_new(otPMP_GROUP);
                        InsertSysObject_new(otCOMMON_GROUP);

                      end;


                      InsertSysObject (otMapFile);
                      InsertSysObject (otRelMatrixFile);

                   end;
      end;
    end;

    //------------------------------------------------------------
    otRootDict: begin
    //------------------------------------------------------------
     // iID:=aPIDL.ID;

      for i:=0 to High(FViewObjects) do
        InsertSysObject (FViewObjects[i]);

      if Style=esDict then
      begin

      //  InsertSysObject_new(otGSM_DICT_GROUP);
       // InsertSysObject_new(otLINK_DICT_GROUP);
      ////  InsertSysObject_new(ot3G_DICT_GROUP);
      end;

    end;


    //------------------------------------------------------------
    otLINK_GROUP: begin
    //------------------------------------------------------------
      for i:=0 to High(PROJECT_LINK_TYPES) do
        if PROJECT_LINK_TYPES[i]<>otLinkFreqPlan then
          InsertSysObject (PROJECT_LINK_TYPES[i]);
    end;

    //------------------------------------------------------------
    otPMP_GROUP: begin
    //------------------------------------------------------------
      for i:=0 to High(PROJECT_PMP_TYPES) do  InsertSysObject (PROJECT_PMP_TYPES[i]);
    end;

    //------------------------------------------------------------
    otCOMMON_GROUP: begin
    //------------------------------------------------------------
      for i:=0 to High(PROJECT_COMMON_TYPES) do  InsertSysObject (PROJECT_COMMON_TYPES[i]);
    end;

    //------------------------------------------------------------
    // �����������
    otAntennaType:         InsertFolderItems (OBJ_ANTENNA_TYPE,  aID, []);
    otCalcModel:           InsertFolderItems (OBJ_CALC_MODEL,    aID, []);
    otCellLayer:           InsertFolderItems (OBJ_CELL_LAYER,    aID, []);
    otClutterModel:        InsertFolderItems (OBJ_Clutter_Model, aID, []);
    otColorSchema:         InsertFolderItems (OBJ_COLOR_SCHEMA,  aID, []);
    otCombiner:            InsertFolderItems (OBJ_COMBINER,      aID, []);
    otLinkEndType:         InsertFolderItems (OBJ_LINKEND_TYPE,  aID, []);
    otLinkType:            InsertFolderItems (OBJ_LINK_TYPE,     aID, []);
    otReport:              InsertFolderItems (OBJ_REPORT,        aID, []);
    otTerminalType:        InsertFolderItems (OBJ_TERMINAL_TYPE, aID, []);
    otTrxType:             InsertFolderItems (OBJ_TRX_TYPE,      aID, []);

    otCompany:             InsertFolderItems (OBJ_COMPANY,       aID, []);
  //  otTrafficModel:        InsertFolderItems (OBJ_TRAFFIC,      aID, []);

    otLink:                 DoLink;

    otLink_SDB:             DoLink_SDB;

    otLinkEnd:              DoLinkEnd;

    otLinkRepeater:         DoLinkRepeater;

    otLinkFreqPlan:         DoLinkFreqPlan;
    otLinkFreqPlan_Link:    DoLinkFreqPlan_Link;
    otLinkLine:             DoLinkLine;
    otLinkLine_Link:        DoLinkLine_Link;
    otLinkNet:              DoLinkNet;
    otLinkNet_Link_Ref:     DoLinkNet_Link_Ref;
    otLinkNet_LinkLine_Ref: DoLinkNet_LinkLine_Ref;

    otPMPCalcRegion_Site:   Do_PMPCalcRegion_Site_Link;

    otPMPSector:            DoPMPSector;
    otPmpCalcRegion:        DoCalcRegionPmp();
    otPmpSite:              DoPmpSite();
    otPMPTerminal:          DoPMPTerminal;
    otProperty:             DoProperty;
    otSite:                 DoSite;

    //!!!!!!!
    otRelMatrixFile:        InsertFolderItems (OBJ_REL_MATRIX,   aID, []);  //flIncludeComment

    otMapFile:              InsertFolderItems (OBJ_MAP, aID, []);

    otTemplatePmpSite:      DoTemplate_Pmp_Site;

    otTemplate_Link:         DoTemplate_Link;
//    otTemplate_Site:         DoTemplate_Site;
    otTemplate_site_linkend: DoTemplate_Site_linkend;

    otTemplate_FreqPlan:     DoTemplate_FreqPlan;

//    otTemplate_site_mast:   InsertItems2 (OBJ_Template_site_mast, aPIDL.ID);

//    otMast:   InsertItems2 (OBJ_site, aPIDL.ID);


    //------------------------------------------------------------
    otMSC: begin
    //------------------------------------------------------------
      if aPIDL.IsFolder then
      begin
        InsertFolderItems (OBJ_MSC, aPIDL.id, [flGetChildCount]);
      end else
        InsertItems2 (OBJ_MSC, aPIDL.ID);
     end;

    //------------------------------------------------------------
    otBSC: begin
    //------------------------------------------------------------
      if aPIDL.IsFolder then
      begin
        InsertFolderItems (OBJ_BSC, aPIDL.id, [flGetChildCount]);
      end else
        InsertItems2 (OBJ_BSC, aPIDL.ID);
    end;

    //------------------------------------------------------------
    otTemplateLinkend: begin
    //------------------------------------------------------------
      InsertItems2 (OBJ_TEMPLATE_LINKEND, aPIDL.ID);
    end;

    //------------------------------------------------------------
    otGeoRegion: begin
    //------------------------------------------------------------
      if aPIDL.IsFolder then
      begin
        InsertFolderItems (OBJ_GEO_REGION, aPIDL.id,  [flGetChildCount]);
      end else begin
     //   sSQL:=MakeFolderListSQL (TBL_GEOCITY, FLD_GEOREGION_ID, aPIDL.ID);
      //  InsertItems (sSQL, OBJ_GEO_CITY, []);
      end;
    end;


    else
      g_Log.Add('procedure TdmExplorer.Build (aPIDL: TrpPIDL);');

  end;

end;


procedure TdmExplorer.SetViewObjects(aValue: TrpObjectTypeArr);
begin
  FViewObjects:=aValue;
end;


//----------------------------------------------------------------
procedure TdmExplorer.InsertItems3(aObjectName: string; aFolder_ID: Integer);
//----------------------------------------------------------------
var
  k: Integer;
begin
  k:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc1,
         'sp_Explorer_Select_Folder_Items',
         [FLD_ObjName,    aObjectName,
          FLD_FOLDER_ID,  aFolder_ID,
          FLD_PROJECT_ID, dmMain.ProjectID
         ]);


//  if Eq(aObjectName, OBJ_REL_MATRIX) then
//    db_View(ADOStoredProc1);


  if k>0 then
    InsertDataset (ADOStoredProc1);
end;



begin

end.
