unit u_Inspector_common;

interface

uses Classes, SysUtils,
     u_func;

type

  TOnFieldChangedEvent = procedure(Sender: TObject; aFieldName: string; aNewValue: Variant) of object;
  TOnRenameEvent = procedure(Sender: TObject; aGUID,aNAME: string) of object;

  TViewEngFieldType = (vftNone, vftText, vftInt,
                       vftDegree,vftDegreeLat,vftDegreeLon,
                       vftFloat, vftBoolean, vftDate,
                       vftFileDir, vftFileName,
                       vftDatetime,

                       vftList, //integer field
                       vftStrList, //string field

                       vftStatus,
                     //  vftPickStrList,
                       vftXRef,
                       vftButton,
                       vftDelimiter
                       );


   TViewEngFieldInfoList = class;

  //-------------------------------------------------------------------
  TViewEngFieldInfo = class //(TCollectionItem)
  //-------------------------------------------------------------------
  public
    ID              : Integer;
    ParentID        : integer;
//    Name_           : string;
    Alias          : string;
    FieldName      : string;

    Caption         : string;

    Min_Value        : Integer;
    Max_Value        : integer;

    //type_
    FieldType       : TViewEngFieldType;

    XREF_ObjectName : string;
//    XREF_FieldName  : string;
    IsFolder        : boolean;
    IsReadOnly      : boolean;

    StatusName      : string;

    ListNames       : TStringList;

    SubFields       : TViewEngFieldInfoList;

    DisplayIndex    : Integer;

  //  View_FieldName : string;


    constructor Create;
    destructor Destroy; override;
  end;


  TViewEngFieldInfoList = class(TList)
  private
    function GetItems(Index: Integer): TViewEngFieldInfo;
  public
    procedure Clear; override;
    function GetItemByID(aID: Integer): TViewEngFieldInfo;
    procedure ReOrder;

    property Items[Index: Integer]: TViewEngFieldInfo read GetItems; default;
  end;


  function TdmViewEngine_StrToFieldType (aValue: string): TViewEngFieldType;



implementation


//-------------------------------------------------------------------
function TdmViewEngine_StrToFieldType (aValue: string): TViewEngFieldType;
//-------------------------------------------------------------------
begin
  if Eq(aValue, '') or
     Eq(aValue, 'text') or
     Eq(aValue, 'lib') or
     Eq(aValue, 'comments')   then Result := vftText else

  if Eq(aValue, 'int')        then Result := vftInt  else
  if Eq(aValue, 'float')      then Result := vftFloat    else
  if Eq(aValue, 'bool')       then Result := vftBoolean        else
  if Eq(aValue, 'boolean')    then Result := vftBoolean  else
  if Eq(aValue, 'date')       then Result := vftDate     else
  if Eq(aValue, 'datetime')   then Result := vftDatetime else

  if Eq(aValue, 'degree')     then Result := vftDegree   else
  if Eq(aValue, 'degree_N')   then Result := vftDegreeLat   else
  if Eq(aValue, 'degree_E')   then Result := vftDegreeLon   else

  if Eq(aValue, 'strlist')    then Result := vftStrList     else
  if Eq(aValue, 'list')       then Result := vftList        else

  if //Eq(aValue, 'UserList') or
     Eq(aValue, 'Status')     then Result := vftStatus   else

//  if Eq(aValue, 'strlist')    then Result := vftStrList     else
//  if Eq(aValue, 'PickStrlist')then Result := vftPickStrList else
  if Eq(aValue, 'xref')       then Result := vftXRef        else
 // if Eq(aValue, 'FileDir')    then Result := vftFileDir     else
  if Eq(aValue, 'FileName')   then Result := vftFileName    else
  if Eq(aValue, 'Delimiter')  then Result := vftDelimiter   else
  if Eq(aValue, 'button')     then Result := vftbutton
  else
    raise Exception.Create('');
   // Result := vftNone;
end;





constructor TViewEngFieldInfo.Create;
begin
  inherited;
  ListNames := TStringList.Create;
  SubFields := TViewEngFieldInfoList.Create();
end;

destructor TViewEngFieldInfo.Destroy;
begin
  FreeAndNil(SubFields);
  FreeAndNil(ListNames);
  inherited;
end;


procedure TViewEngFieldInfoList.Clear;
var  I: integer;
begin
  for I := 0 to Count - 1 do  Items[i].Free;
  inherited;
end;


function TViewEngFieldInfoList.GetItemByID(aID: Integer): TViewEngFieldInfo;
var
  I: integer;
begin
  Result:=nil;

  for I := 0 to Count - 1 do
  begin
    if Items[i].ID=aID then
    begin
      Result := Items[i];
      Break;
    end;

    Result:=Items[i].SubFields.GetItemByID(aID);
    if Assigned(Result) then
      Break;
  end;

end;


function TViewEngFieldInfoList.GetItems(Index: Integer): TViewEngFieldInfo;
begin
  Result := TViewEngFieldInfo(inherited Items[Index]);
end;



procedure TViewEngFieldInfoList.ReOrder;
var
  I: integer;
  oItem: TViewEngFieldInfo;
begin
  for I := Count - 1 downto 0 do
    if Items[i].ParentID>0 then
    begin
      oItem:=GetItemByID(Items[i].ParentID);

 {     if not Assigned(oItem) then
        raise Exception.Create('TViewEngFieldInfoList.ReOrder; : FieldName:'+oItem.FieldName+ ', Items[i].ParentID:'+ IntToStr(Items[i].ParentID));
}
      if Assigned(oItem) then
      begin
        oItem.SubFields.Add(Items[i]);
        Delete(i);
      end
    end;

end;



end.


