unit fr_Explorer;

interface

uses

  u_cx_TreeList,

  dm_User_Security,
  dm_Onega_db_data,

  I_DBExplorerForm,
  I_Shell,

  dm_Main,

  dm_Main_res,
  dm_Explorer,

  u_Explorer,

  u_func_msg,
  u_const_msg,

  u_types,
  u_const_db,
  u_const,
  u_const_str,

  u_classes,
  u_db,
  u_db_tree,
  u_dlg,
//

  u_cx,

  u_files,
  u_func_arr,

  u_func,
  u_Log,

  i_explorer,

  u_Shell_new,

  Windows, cxControls, dxmdaset,  Buttons, Messages, SysUtils, Classes,
  Controls, Forms,  AxCtrls, AppEvnts, cxTLData, cxDBTL, cxGraphics,
  Dialogs,  ExtCtrls, Menus, ActnList, ImgList, ToolWin,
  ComCtrls, StdCtrls, Db, RxMemDS, Grids, DBGrids,  ADODB,
  ComObj, Registry, Variants, cxStyles, cxInplaceContainer, cxTL,

  cxClasses, dxBar, cxBarEditItem, cxLookAndFeels,
  cxLookAndFeelPainters, cxCustomData, cxTextEdit, cxTLdxBarBuiltInMenu,
  dxSkinsCore, dxSkinsDefaultPainters;

type


  Tframe_Explorer = class(TForm, IShellNotifyX, IDBExplorerFormX, IexplorerX, IMessageHandler)
    PopupMenu1: TPopupMenu;
    ImageList1: TImageList;
    ToolBar1: TToolBar;
    ActionList1: TActionList;
    Timer1: TTimer;
    StatusBar1: TStatusBar;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_ReadOnly: TcxStyle;
    CheckStateImages: TImageList;
    DataSource1: TDataSource;
    cxTreeList1: TcxTreeList;
    col_ID_: TcxTreeListColumn;
    col_GUID_: TcxTreeListColumn;
    col_Name_: TcxTreeListColumn;
    col_Owner_: TcxTreeListColumn;
    col_Sort_By_Object_Name: TcxTreeListColumn;
    col_Tree_ID_: TcxTreeListColumn;
    col_OBJECT_NAME: TcxTreeListColumn;
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    DataSource3: TDataSource;
    ADOStoredProc1: TADOStoredProc;
    Button6: TButton;
    ImageList_Search: TImageList;
    ActionList_Search: TActionList;
    act_Search_next: TAction;
    act_Search_Prior: TAction;
    Panel2: TPanel;
    cbDEBUG: TCheckBox;
    ed_GUID: TEdit;
    Button4: TButton;
    cxStyleRepository2: TcxStyleRepository;
    cxStyle_StrikeOut: TcxStyle;
    act_Search: TAction;
    pn_ToolBar_Search: TPanel;
    ed_Search: TEdit;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    cb_Whole_word: TCheckBox;
    SpeedButton3: TSpeedButton;
    Button1: TButton;
    Button2: TButton;
    procedure ActionList_SearchUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure act_Search_nextExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
//    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
//    procedure Button1Click(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
//    procedure Button4Click(Sender: TObject);
//    procedure Button5Click(Sender: TObject);

    procedure cbDEBUGClick(Sender: TObject);
    procedure cxTreeList1Collapsed(Sender: TcxCustomTreeList; ANode:
        TcxTreeListNode);
    procedure cxTreeList1MouseWheel(Sender: TObject; Shift: TShiftState;
        WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
//    procedure cxTreeList1_CanSelectNode(Sender: TcxCustomTreeList; ANode: TcxTreeListNode; var
  //      Allow: Boolean);
//    procedure cxTreeList1Change(Sender: TObject);
 //   procedure cxTreeList1_Click(Sender: TObject);
//    procedure cxTreeList1_CustomDrawCell(Sender: TObject; ACanvas: TcxCanvas;
 //       AViewInfo: TcxTreeListEditCellViewInfo; var ADone: Boolean);
    procedure cxTreeList1_DblClick(Sender: TObject);
    procedure cxTreeList1_Deletion(Sender: TcxCustomTreeList; ANode:
        TcxTreeListNode);

    procedure cxTreeList1_Expanding(Sender: TcxCustomTreeList; ANode:      TcxTreeListNode; var Allow: Boolean);

    procedure cxTreeList1_FocusedNodeChanged(Sender: TcxCustomTreeList;
        APrevFocusedNode, AFocusedNode: TcxTreeListNode);
    procedure cxTreeList1_MouseDown(Sender: TObject; Button: TMouseButton; Shift:
        TShiftState; X, Y: Integer);
    procedure cxTreeList1_MouseUp(Sender: TObject; Button: TMouseButton; Shift:
        TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
(*//    procedure dxDBTreeList1_CustomDrawCell(Sender: TObject;
      ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
      AColumn: TdxTreeListColumn; ASelected, AFocused,
      ANewItemRow: Boolean; var AText: String; var AColor: TColor;
      AFont: TFont; var AAlignment: TAlignment; var ADone: Boolean);
*)
//    procedure dxTreeList1_CanNodeSelected(Sender: TObject; ANode: TdxTreeListNode;
 //       var Allow: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure dxTreeList1_Click(Sender: TObject);
//    procedure Panel1Click(Sender: TObject);
//    procedure dxTreeList1_Expanding(Sender: TObject; Node: TdxTreeListNode;
  //    var Allow: Boolean);

(*
    procedure dxTreeList1_CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
*)

//    procedure dxTreeList1_ChangeNode(Sender: TObject; OldNode,
 //     Node: TdxTreeListNode);
    procedure Timer1Timer(Sender: TObject);
//    procedure dxTreeList1_DblClick(Sender: TObject);
//    procedure dxTreeList1_Deletion(Sender: TObject; Node: TdxTreeListNode);
//    procedure dxTreeList1_MouseDown(Sender: TObject; Button: TMouseButton; Shift:
 //       TShiftState; X, Y: Integer);
//    procedure dxTreeList1_MouseUp(Sender: TObject; Button: TMouseButton; Shift:
//        TShiftState; X, Y: Integer);
   // procedure dxTreeList1_SelectedCountChange(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
//    procedure Button5Click(Sender: TObject);
//    procedure Button6Click(Sender: TObject);
//    procedure Button7Click(Sender: TObject);
//    procedure Button8Click(Sender: TObject);
 //   procedure cb_Search1KeyPress(Sender: TObject; var Key: Char);
  //  procedure cxBarEditItem1KeyPress(Sender: TObject; var Key: Char);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure ed_SearchKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
//    procedure ed_SearchKeyPress(Sender: TObject; var Key: Char);
    procedure cxTreeList1StylesGetContentStyle(Sender: TcxCustomTreeList;
      AColumn: TcxTreeListColumn; ANode: TcxTreeListNode;
      var AStyle: TcxStyle);
    procedure ed_SearchPropertiesEditValueChanged(Sender: TObject);
    procedure ed_SearchPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
 //   procedure Button6Click(Sender: TObject);
  private
    FChangeChecked : Boolean;
  private
    FDataset: TDataSet;

    FIsUpdated : Boolean;


//    FActive : Boolean;

//    FRegPath: string;
//    FIsUpdated: boolean;
    FSelectedCount: Integer;

    FTreeID: Integer;

//    FOnExplorerCustomDrawCellByGUID: TOnExplorerCustomDrawCellByGUID;
    FOnNodeFocused: TOnExplorerItemEvent;
  //  FFocusedNode: TdxTreeListNode;
//    FFocusedNode: TcxTreeListNode;
    
    FOnAskMenu: TOnAskMenuEvent;

    FTempPIDL: TrpPIDL;

//    function FindNodeByTreeID(aTreeID: integer): TdxTreeListNode;
    function FindNodeByGUID(aGUID: string): TcxTreeListNode;

//    procedure SetRegPath (aValue: string);

  private
    FViewObjects: TrpObjectTypeArr;
//  protected
 // private
    { TODO : ! }
    
    FIsNodeChanging: boolean;
//    FDisableControls: boolean;

    FFocusedPIDL: TrpPIDL;
//    FRegStrList: TStringList;
    FSelectedPIDLs:  TrpPIDL_List;

    FOnItemDblClick: TOnExplorerItemEvent;

//    procedure cxTreeList1Expanding(Sender: TcxTreeList; ANode: TcxTreeListNode; var
//        Allow: Boolean);
//    FIShellNotifyX: IShellNotifyX;

    procedure GetShellMessage(aMsg: integer; aRec: TShellEventRec);
    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList; var aHandled:
        boolean);

//    function NodeToRec(aNode: TdxTreeListNode): TExplorerNodeRec;

 // private
    procedure DoChangeFocusedNode;

//    procedure UpdateIfExpanded (aGUID: string);
    procedure UpdateFocusedNodeChildren ();
    procedure UpdateNodeChildren(aParentKeyID: integer; aNode_cx: TcxTreeListNode);

    procedure DeleteDBNodeChildren (aParentKeyID: integer);
    procedure DeleteDBNode (aKeyID: integer);
//    procedure DeleteDBNodeChildren_cx(aParentKeyID: integer);
    procedure DELETE_NODE_BY_ObjectNameAndID(aObjectName: string; aID: integer);
    procedure DoAddNode(aNode_cx: TcxTreeListNode; aDataset: TDataset;
        aParentKeyID, aTreeID: Integer);
//    function ExpandByGUID_ex_1111(aGUID: string): TcxTreeListNode;

//    procedure DoOnShellEvent1111111111(aRec: TShellParamRec);

    function FindNodeByText(aColumn: TcxTreeListColumn; aText: string):
        TcxTreeListNode;

    function FindNodeByTreeID(aTreeID: integer): TcxTreeListNode;

    function GetNodePIDL(ANode: TcxTreeListNode): TrpPIDL;

    procedure NodeToPIDL(aNode: TcxTreeListNode; aPIDL: TrpPIDL);
    procedure Search(aSearch_text: string);

    procedure Search_FocuseNodeByObjName;
    procedure Search_Next;
    procedure Search_Prior;



  public
    procedure UPDATE_NODE_CHILDREN_by_ObjName(aObjectName: string; aID: integer);

//      procedure UPDATE_NODE_CHILDREN_ByGUID (aGUID: string );


  public

    Style:    TdmExplorerStyle;

    Options:  TdmExplorerOptionSet;


{    procedure FindByObjectNameAndFocus (aObjectName: string;
                               aID: integer;
                               aIsFolder: Boolean);
}

    function FindByObjectName(aObjectName: Widestring; aID: integer; aIsFolder:
        boolean): TcxTreeListNode;


    procedure SaveSelectedToPIDL_List (aPIDL_List: TrpPIDL_List);

    procedure SetViewObjects (aValues: array of TrpObjectType);
    procedure SetViewObject (aObjectName: string);


    procedure Debug (aValue: boolean);

//    procedure SaveStateToReg   (aRegPath: string);
//    procedure LoadStateFromReg (aRegPath: string);

   // function FindByGUID(aGUID: string): TdxTreeListNode;



    function FocuseNodeByGUID(aGUID: string): Boolean;
    function FocuseNodeByObjName(aObjectName: Widestring; aID: integer): Boolean;

    procedure Load;

    procedure ReLoad_Project;

 //   property RegPath1: string read FRegPath write SetRegPath;

    property OnItemDblClick: TOnExplorerItemEvent read FOnItemDblClick write FOnItemDblClick;
    property OnNodeFocused: TOnExplorerItemEvent read FOnNodeFocused write FOnNodeFocused;
//    property OnExplorerCustomDrawCellByGUID: TOnExplorerCustomDrawCellByGUID read FOnExplorerCustomDrawCellByGUID write FOnExplorerCustomDrawCellByGUID;

    property OnAskMenu: TOnAskMenuEvent read FOnAskMenu write FOnAskMenu;

  public
    StateFileName : string;

    //IDBExplorerFormX
    procedure RENAME_ITEM(aGUID, aNewName: string);
    function ExpandByGUID(aGUID: string): Boolean;
    procedure DELETE_NODE_BY_GUID (aGUID: string );

    procedure UPDATE_NODE_CHILDREN_ByGUID(aGUID: string);

  end;


//==============================================================
//==============================================================
implementation

//uses dm_Rel_Engine;

{$R *.dfm}



const
  REG_SECTION_EXPANDED = 'Expanded nodes';
  REG_FOCUSED_GUID     = 'Focused GUID';

  DEF_STATE_CHECKED   = 0;
  DEF_STATE_UNCHECKED = 1;




//----------------------------------------------------------------
procedure Tframe_Explorer.FormCreate(Sender: TObject);
//----------------------------------------------------------------
begin
//  mem_Data.

//  cxTreeList1.OnCanSelectNode      :=cxTreeList1_CanSelectNode;
  cxTreeList1.OnDblClick           :=cxTreeList1_DblClick;

//  cxTreeList1.OnCustomDrawCell     :=cxTreeList1_CustomDrawCell;

  cxTreeList1.OnDeletion           :=cxTreeList1_Deletion;
  cxTreeList1.OnExpanding          :=cxTreeList1_Expanding;
  cxTreeList1.OnFocusedNodeChanged :=cxTreeList1_FocusedNodeChanged;

  cxTreeList1.OnMouseDown          :=cxTreeList1_MouseDown;
  cxTreeList1.OnMouseUp            :=cxTreeList1_MouseUp;


  cxTreeList1.OptionsSelection.MultiSelect:=True;


 // with dxTreeList1 do
//    Options := Options + [aoAutoWidth, aoMultiSelect, aoExtMultiSelect];

//  aoAutoWidth

//  col_Name.Caption:='���';
  col_Name_.Caption.Text:='���';
  col_Name_.Visible:=true;

//  col_Comment.Caption:='�����������';

 // dxTreeList1_.Align:=alClient;
  cxTreeList1.Align:=alClient;

 // cb_WHOLE_WORD.EditValue:=false;


  TdmMain_res.Init;

//  dxTreeList1_.Images.Assign (dmMain_res.imgs_Common);
//
//  dxTreeList1_.StateImages:=CheckStateImages;

//  Assert(Assigned(cxTreeList1.Images));

  cxTreeList1.Images:=ImageList1;

  cxTreeList1.Images.Assign (dmMain_res.imgs_Common);
  cxTreeList1.StateImages:=CheckStateImages;

  cxTreeList1.OptionsView.Headers:=False;


//  dxTreeList1.StateImages.Assign (dmMain_res.imgs_Common);


  FFocusedPIDL:=TrpPIDL.Create;
  FTempPIDL:=TrpPIDL.Create;
 // FRegStrList:=TStringList.Create;

  FSelectedPIDLs:=TrpPIDL_List.Create;
//  FSelectedPIDLs.Sender := dxTreeList1;
  FSelectedPIDLs.Sender := cxTreeList1;

  //default
  Style := esObject;


  FDataset:=dmExplorer.GetDataset();
  DataSource1.DataSet := FDataset;

//  Debug (True);
  Debug (false);

 //  col_GUID_.Visible:=True;

{

  col_ID.Visible:=False;
  col_ID.Width:=20;


}


{
  col_ID.Visible:=g_Debug;
  tb_Debug.Visible:= g_Debug;;
}

//  tb_Debug.Visible:=dmMain.Debug;

//  Assert(GetInterface(IShellNotifyX, FIShellNotifyX));
  g_ShellEvents.RegisterObject(Self);

//  g_ShellEvents.RegisterProc(GetShellProcEvent);


 //  Debug (True);


//  col_OBJECT_NAME.Visible:=True;

//

///////col_Guid.Visible:=True;

//  col_ID.Visible:=True;


 //  col_ID_.Visible:=True;
  // col_OBJECT_NAME.Visible:=True;

////  FProcIndex:=RegisterProc(GetMessage, Name);


//  cxTreeList1.Find

 /////// col_Tree_ID.Visible:=True;

//  FActive:=True;


//  FStateFileName := g_Application_FormStateDir + 'explorer.txt';


end;

//----------------------------------------------------------------
procedure Tframe_Explorer.FormDestroy(Sender: TObject);
//----------------------------------------------------------------
begin
//  FActive:=False;

  Timer1.Enabled := False;

  g_ShellEvents.UnRegisterObject(Self);


 // FreeAndNil(FRegStrList);
  FreeAndNil(FTempPIDL);
  FreeAndNil(FFocusedPIDL);
  FreeAndNil(FSelectedPIDLs);

//  SaveStateToReg (RegPath);

  inherited;
end;


//----------------------------------------------------------------
procedure Tframe_Explorer.Debug (aValue: boolean);
//----------------------------------------------------------------
var i: integer;
begin
  StatusBar1.Visible := aValue;

  cxTreeList1.OptionsView.Headers:=aValue;

//  cxTreeList1.ShowHeader:=aValue;

//  dxTreeList1.ShowHeader:=True;

  for i:=1 to cxTreeList1.ColumnCount-1 do
    cxTreeList1.Columns[i].Visible:=aValue;


end;



//--------------------------------------------------------------------
procedure Tframe_Explorer.UpdateFocusedNodeChildren ();
//--------------------------------------------------------------------
var
 // oNode: TdxTreeListNode;
  oNode_cx: TcxTreeListNode;
begin
//  if not FActive then
//    Exit;


 // oNode:=dxTreeList1.FocusedNode;
  oNode_cx:=cxTreeList1.FocusedNode;

  if Assigned(oNode_cx) then
  begin
//    UpdateNodeChildren (TrpPIDL(oNode.Data).Tree_ID1, oNode, oNode_cx);
    UpdateNodeChildren (TrpPIDL(oNode_cx.Data).Tree_ID1, oNode_cx);
 //   UpdateNodeChildren (oNode.Values[col_Tree_ID.Index], oNode);
  end;

end;

//--------------------------------------------------------------------
procedure Tframe_Explorer.DoChangeFocusedNode;
//--------------------------------------------------------------------
//var
 // oFocusedNode: TcxTreeListNode;

begin
//  if not FActive then
//    Exit;


  if FIsNodeChanging then
   Exit;

  FIsNodeChanging:=true;


  if  cxTreeList1.FocusedNode=nil then
    Exit;

{

 if dxTreeList1.FocusedNode=FFocusedNode then
    Exit
  else
 }

 // FFocusedNode:=dxTreeList1.FocusedNode;
 // oFocusedNode:=cxTreeList1.FocusedNode;

  NodeToPIDL (cxTreeList1.FocusedNode, FTempPIDL);
  if FFocusedPIDL=FTempPIDL then
    Exit;

 // NodeToPIDL (dxTreeList1.FocusedNode, FFocusedPIDL);
  NodeToPIDL (cxTreeList1.FocusedNode, FFocusedPIDL);


  if Assigned(FOnNodeFocused) then
    FOnNodeFocused(FFocusedPIDL);

(* if Assigned(cxTreeList1.FocusedNode) then
 //   with dxTreeList1.FocusedNode do
    with cxTreeList1.FocusedNode do
      g_Log.Add (TrpPIDL(Data).Name +', id:'+ IntToStr(TrpPIDL(Data).ID));
*)

     // g_Log.Add (Values[col_Name.Index] +', id:'+ Values[col_ID.Index]);
end;



procedure Tframe_Explorer.SetViewObjects (aValues: array of TrpObjectType);
begin
  FViewObjects:=g_Obj.MakeObjectTypeArr (aValues);
end;



procedure Tframe_Explorer.NodeToPIDL(aNode: TcxTreeListNode; aPIDL: TrpPIDL);
begin
  aPIDL.Assign(GetNodePIDL(aNode));
end;

//--------------------------------------------------------------------
function Tframe_Explorer.FindByObjectName(aObjectName: Widestring; aID:
    integer; aIsFolder: boolean): TcxTreeListNode;
//--------------------------------------------------------------------
var
  s: string;
begin
//  if not FActive then
//    Exit;

  s:= Format('%s:%d:%s', [aObjectName, aID, BoolToStr(aIsFolder)]);

//  Result:= cxTreeList1.FindNodeByText(s, col_Sort_By_Object_Name);
  Result:= FindNodeByText(col_Sort_By_Object_Name, s);
end;


//--------------------------------------------------------------------
function Tframe_Explorer.FindNodeByText(aColumn: TcxTreeListColumn; aText:
    string): TcxTreeListNode;
//--------------------------------------------------------------------
begin
  Result:=cx_FindNodeByText(cxTreeList1, aColumn, aText);
end;


//--------------------------------------------------------------------
function Tframe_Explorer.FindNodeByTreeID(aTreeID: integer): TcxTreeListNode;
//--------------------------------------------------------------------
begin
//  cxTreeList1.Find

//  Result:= cxTreeList1.FindNodeByText(IntToStr(aTreeID), col_Tree_ID_);
  Result:= FindNodeByText(col_Tree_ID_, IntToStr(aTreeID));

//  Assert( Result.Values[col_Tree_ID_.ItemIndex] = aTreeID);
  if Assigned(Result) then
    if Result.Values[col_Tree_ID_.ItemIndex] <> aTreeID then
      raise Exception.Create('if Result.Values[col_Tree_ID_.ItemIndex] <> aTreeID then');

//    ShowMessage('');
    

 // Assert(Result.Values[col_Tree_ID_.ItemIndex] = aTreeID);

//  ShowMessage(Result.Values[col_Name_.ItemIndex]);
 // ShowMessage(IntToStr(Result.Level));


end;


//--------------------------------------------------------------------
function Tframe_Explorer.FindNodeByGUID(aGUID: string): TcxTreeListNode;
//--------------------------------------------------------------------
begin
//  Result:= cxTreeList1.FindNodeByText(aGUID, col_GUID_);
  Result:= FindNodeByText(col_GUID_, aGUID );
end;



function Tframe_Explorer.GetNodePIDL(ANode: TcxTreeListNode): TrpPIDL;
begin
  Assert(Assigned(aNode), 'Assigned(aNode)');
  Assert(aNode.Data<>nil, 'aNode.Data<>nil');
  Assert(Assigned(aNode.Data), 'Assigned(aNode.Data)');

  Result := TrpPIDL(aNode.Data);
end;



//-------------------------------------------------------------------
procedure Tframe_Explorer.DeleteDBNodeChildren (aParentKeyID: integer);
//-------------------------------------------------------------------
var
  oNode: TcxTreeListNode;
begin
//  if not FActive then
//    Exit;

  oNode:=FindNodeByTreeID (aParentKeyID);

  if Assigned(oNode) then
    oNode.DeleteChildren;  
end;



//-------------------------------------------------------------------
procedure Tframe_Explorer.DeleteDBNode (aKeyID: integer);
//-------------------------------------------------------------------
var oNode: TcxTreeListNode;
begin
  DeleteDBNodeChildren (aKeyID);

  oNode:=FindNodeByTreeID (aKeyID);
  if Assigned(oNode) then
    FreeAndNil(oNode);

end;


//-------------------------------------------------------------------
procedure Tframe_Explorer.DoAddNode(aNode_cx: TcxTreeListNode; aDataset:
    TDataset; aParentKeyID, aTreeID: Integer);
//-------------------------------------------------------------------
var
   sObjName: string;
   bIsFolder: boolean;
   iID,iTreeID,ind,i: integer;

  // oNode1: TdxTreeListNode;
   oNode1_cx: TcxTreeListNode;

   bHasChildren: boolean;
   bFolderIcon: boolean;
   oPIDL: TrpPIDL;
  s: string;
begin
//  db_View(aDataset);

//  if not FActive then
//    Exit;

  oPIDL:=TrpPIDL.Create;

  oPIDL.ID         :=aDataset.FieldByName(FLD_ID).AsInteger;
  oPIDL.Name       :=aDataset.FieldByName(FLD_NAME).AsString;
  oPIDL.ObjectName :=aDataset.FieldByName(FLD_OBJECT_NAME).AsString;
  oPIDL.IsFolder   :=aDataset.FieldByName(FLD_IS_FOLDER).AsBoolean;
  oPIDL.Guid       :=aDataset.FieldbyName(FLD_GUID).AsString;

  oPIDL.Checked    :=aDataset.FieldbyName(FLD_Checked).AsBoolean;

  oPIDL.ref_ID     :=aDataset.FieldByName(FLD_ref_ID).AsInteger;
  oPIDL.ref_ObjName:=aDataset.FieldByName(FLD_ref_ObjName).AsString;

  if Assigned(aDataset.FindField(FLD_OWNER)) then
    oPIDL.Owner:=aDataset.FieldByName(FLD_OWNER).AsString;

  oPIDL.Tree_ID1    :=aTreeID;


 with aDataset do
 begin
    sObjName    :=FieldByName(FLD_OBJECT_NAME).AsString;
    bHasChildren:=FieldByName(FLD_HAS_Children).AsBoolean;
    bFolderIcon :=FieldByName(FLD_IS_FOLDER).AsBoolean;


      //////////////////////


    s:= Format('%s:%d:%s', [oPIDL.ObjectName, oPIDL.ID, BoolToStr (oPIDL.IsFolder)]);

//  Result:= cxTreeList1.FindNodeByText(s, col_Sort_By_Object_Name);

    aNode_cx.Values[col_Sort_By_Object_Name.ItemIndex] :=s;



//    aNode_cx.Values[col_Sort_By_Object_Name.ItemIndex] :=
  //    Format('%s:%s:', [oPIDL.ObjectName, oPIDL.id]);

    aNode_cx.Values[col_OBJECT_NAME.ItemIndex] :=sObjName;



    aNode_cx.Values[col_GUID_.ItemIndex] :=oPIDL.GUID;
    aNode_cx.Values[col_Name_.ItemIndex] :=oPIDL.Name;
    aNode_cx.Values[col_id_.ItemIndex] :=oPIDL.id;

//    aNode_cx.Values[col_Tree_ID_.ItemIndex] :=aTreeID; //oPIDL.ID;
//    aNode_cx.Values[col_Tree_ID_.ItemIndex] :=IntToStr(aTreeID); //oPIDL.ID;
    aNode_cx.Values[col_Tree_ID_.ItemIndex] :=aTreeID; //oPIDL.ID;

      
  //  if oPIDL.Owner<>'' then
      aNode_cx.Values[col_Owner_.ItemIndex] :=oPIDL.Owner;


  end;


 // Assert(aNode.Data=nil);

 // aNode.Data:=oPIDL;
  aNode_cx.Data:=oPIDL;

  if bFolderIcon or eq(sObjName,OBJ_Status_LIST)
    then ind:=dmMain_res.GetImageIndexByName(ICON_FOLDER)
    else ind:=dmMain_res.GetImageIndexByObject (sObjName);

  aNode_cx.ImageIndex:=ind;
  aNode_cx.SelectedIndex:=ind;

  /////////////////////////////

  if Eq(sObjName, OBJ_REL_MATRIX) then
    if not oPIDL.IsFolder then
    begin
      aNode_cx.StateIndex:= IIF(not oPIDL.Checked, 0, 1);
    end;


  /////////////////////////////

  {
  if bHasChildren then
  begin// temp '..' node
    oNode1:=aNode.AddChild;
    oNode1.Values[col_Name.Index]:='...';
  end;
   }


  if bHasChildren then
  begin// temp '..' node
    aNode_cx.HasChildren:=bHasChildren;

(*
    oNode1_cx:=aNode_cx.AddChild;
    oNode1_cx.Values[col_Name_.ItemIndex]:='...';
*)



  end;
end;

//----------------------------------------------------------------
procedure Tframe_Explorer.UpdateNodeChildren(aParentKeyID: integer; aNode_cx:
    TcxTreeListNode);
//----------------------------------------------------------------

var
  oParentNode_cx: TcxTreeListNode;
  oNode_test: TcxTreeListNode;
  oNode_cx: TcxTreeListNode;


  oPIDL: TrpPIDL;
  oPIDL_node: TrpPIDL;

 // oNode,oParentNode: TdxTreeListNode;

//  oNode1,oParentNode1: TTreeNode;

  bExpanded: boolean;
  sName: string;

//  oDataset: TDataset;
begin
  oParentNode_cx:=nil;
  oNode_test:=nil;
  oNode_cx:=nil;


//  if not FActive then
//    Exit;

 // oNode_cx:=nil;


  dmExplorer.Style:=Style;
  dmExplorer.SetViewObjects(FViewObjects);
  dmExplorer.Options:=Options;

 // oNode:=nil;

 // cxTreeList1.BeginUpdate;

 // dxTreeList1.BeginUpdate;

  if aParentKeyID<=0 then begin
    dmExplorer.Build (nil);

  end else begin
    oPIDL:=TrpPIDL.Create;

    if Assigned(aNode_cx) then
    begin
      Assert(Assigned(aNode_cx.Data));

      oPIDL_node:=GetNodePIDL(aNode_cx);
      oPIDL.Assign(oPIDL_node);

    end;;

    dmExplorer.Build(oPIDL);

    FreeAndNil(oPIDL);

  end;


  if aParentKeyID>0 then
    DeleteDBNodeChildren (aParentKeyID)
  else begin
    cxTreeList1.Clear;
  end;


    ///////////////////////////////
//  if aParentKeyID=0 then oParentNode:=nil
//                    else oParentNode:=FindNodeByTreeID (aParentKeyID);


  if aParentKeyID=0 then oParentNode_cx:=nil
                    else oParentNode_cx:=FindNodeByTreeID(aParentKeyID);

   if Assigned(oParentNode_cx) then
   begin

   //  ShowMessage(oParentNode_cx.Values[col_Name_.ItemIndex]);
   end;
     


    //FindNodeByTreeID (aParentKeyID);


(*  if aParentKeyID=0 then oParentNode1:=nil
                    else oParentNode1:=GetNodeByTreeID1 (aParentKeyID);

*)



 // bExpanded:=(oParentNode<>nil) and oParentNode.Expanded;
  bExpanded:=(oParentNode_cx<>nil) and oParentNode_cx.Expanded;

//  oDataset:=dmExplorer.GetDataset();
//  oDataset.First;
  FDataset.First;


 //   db_View(oDataset);


//  mem_Data1.DisableControls;

//  with oDataset do
  with FDataset do
    while not EOF do
  begin

    if Assigned(oParentNode_cx) then
      oNode_cx:=oParentNode_cx.AddChild
    else
      oNode_cx:=cxTreeList1.Add;


    //FTreeID:=
    Inc(FTreeID);

    //test
//    oNode_test:=cxTreeList1.FindNodeByText(IntToStr(FTreeID), col_Tree_ID_);
  //  oNode_test:=FindNodeByText(col_Tree_ID_, IntToStr(FTreeID));
  //  Assert(not Assigned(oNode_test));


    DoAddNode( oNode_cx, FDataset, aParentKeyID, FTreeID);
    Next;
  end;


 // cxTreeList1.EndUpdate;


  if (Assigned(oNode_cx)) and (aParentKeyID=0) and
      (dmExplorer.mem_Items.RecordCount=1)
  then
    oNode_cx.Expand(False);


  if Assigned(oParentNode_cx) and bExpanded then
    oParentNode_cx.Expand (False);

end;


procedure Tframe_Explorer.Timer1Timer(Sender: TObject);
begin
  FIsNodeChanging:=false;

 // StatusBar1.SimpleText :=  Format('SelectedCount: %d', [dxTreeList1.SelectedCount]);

end;

procedure Tframe_Explorer.dxTreeList1_Click(Sender: TObject);
begin
  DoChangeFocusedNode;
end;


// ---------------------------------------------------------------
procedure Tframe_Explorer.cxTreeList1_Expanding(Sender: TcxCustomTreeList;
    ANode: TcxTreeListNode; var Allow: Boolean);
// ---------------------------------------------------------------

var
  iTopIndex: Integer;
  oNode,oParentNode: TCxTreeListNode;

   {
begin
  cxTreeList1Expanding(Sender as TcxTreeList, ANode, Allow);
end;



// ---------------------------------------------------------------
procedure Tframe_Explorer.cxTreeList1Expanding(Sender: TcxTreeList; ANode:
    TcxTreeListNode; var Allow: Boolean);
// ---------------------------------------------------------------

var
  iTopIndex: Integer;
  oNode,oParentNode: TCxTreeListNode;
  }
begin
  if FIsNodeChanging then
    Exit;

//....  ShowMessage('cxTreeList1Expanding');


  Assert(Assigned(ANode.Data));


//  if not FActive then
//    Exit;

  cxTreeList1.OnFocusedNodeChanged:=nil;
  UpdateNodeChildren (TrpPIDL(ANode.Data).Tree_ID1, ANode);
  cxTreeList1.OnFocusedNodeChanged:=cxTreeList1_FocusedNodeChanged;


 // try
//    if not FIsUpdated then
  //  begin
//      FDisableControls:= true;

 //     iTopIndex:= dxTreeList1.TopIndex;    // ��������� ��������� �����
  //    oNode    := dxTreeList1.FocusedNode; // ��������� ������� ������

     // Assert(Assigned(ANode.Data));



  //    UpdateNodeChildren (Node.Values[col_Tree_ID.Index], Node);

 //     if Assigned(oNode) then
  //      dx_FocuseNode(oNode);       // ������������ ������� ������

   //   dxTreeList1.TopIndex:= iTopIndex;    // ������������ ��������� �����

//      FDisableControls:= false;
   // end;

 // except end;


end;



procedure Tframe_Explorer.SetViewObject (aObjectName: string);
var ot: TrpObjectType;
begin
  ot:=g_Obj.ItemByName[aObjectName].Type_;
  FViewObjects:=(g_Obj.MakeObjectTypeArr([ot]) );
end;



procedure Tframe_Explorer.cbDEBUGClick(Sender: TObject);
begin
  Debug(cbDEBUG.Checked);
end;



//--------------------------------------------------------------------
procedure Tframe_Explorer.SaveSelectedToPIDL_List (aPIDL_List: TrpPIDL_List);
//--------------------------------------------------------------------
var i: integer;
  oNode: TcxTreeListNode;
  oPIDL: TrpPIDL;

begin
//  if not FActive then
//    Exit;

  aPIDL_List.Clear;

//  cxTreeList1.Sele

 // for i:=0 to dxTreeList1.SelectedCount-1  do
  for i:=0 to cxTreeList1.SelectionCount-1  do
  begin
   // oNode:=dxTreeList1.SelectedNodes[i];
    oNode:=cxTreeList1.Selections[i];

    Assert(Assigned(oNode.Data), 'Value not assigned');

    oPIDL:=aPIDL_List.AddItem;

    oPIDL.Assign(TrpPIDL(oNode.Data));


  end;
end;


//--------------------------------------------------------------------
procedure Tframe_Explorer.RENAME_ITEM(aGUID, aNewName: string);
//--------------------------------------------------------------------
var
  oNode: TcxTreeListNode;
begin
  Assert(aGUID<>'', 'GUID=''''');
                  
  oNode:= FindNodeByGUID (aGUID);
  if Assigned(oNode) then
    oNode.Values[col_Name_.ItemIndex]:=aNewName;

end;

//--------------------------------------------------------------------
function Tframe_Explorer.ExpandByGUID(aGUID: string): Boolean;
//--------------------------------------------------------------------
var
  b: Boolean;
  oNode: TcxTreeListNode;
 // s: string;
//  iKey: integer;
begin
//  if not FActive then
//    Exit;

  Assert(aGUID<>'', 'GUID=''''');


//  FIsNodeChanging:=True;

  oNode:=FindNodeByGUID (aGUID);
  if Assigned(oNode) then
  begin
//    s:=oNode.Values[col_Name_.ItemIndex];

//    oNode.Expand(False);
    oNode.Expand(False);
    b:=oNode.Expanded;
  end;
//    oNode.Expand(False);

//  FIsNodeChanging:=False;


  Result := Assigned(oNode);// and oNode.Expanded;
end;

{
//--------------------------------------------------------------------
function Tframe_Explorer.ExpandByGUID_ex_1111(aGUID: string): TcxTreeListNode;
//--------------------------------------------------------------------
var
  oNode: TcxTreeListNode;
begin

  Assert(aGUID<>'', 'GUID=''''');

  FIsNodeChanging:=True;

  oNode:=FindNodeByGUID (aGUID);
  if Assigned(oNode) then
    oNode.Expand(False);

  FIsNodeChanging:=False;


  Result := oNode;
end;
}

//-------------------------------------------------------------------
function Tframe_Explorer.FocuseNodeByObjName(aObjectName: Widestring; aID:  integer): Boolean;
//-------------------------------------------------------------------
var
  oNode: TcxTreeListNode;
begin
  oNode:=FindByObjectName(aObjectName, aID, False);
  Result := Assigned(oNode);

  FIsNodeChanging:=True;


  if Assigned(oNode) then
  begin
    oNode.Focused:=True;

    oNode.Visible := True;

    cxTreeList1.TopVisibleNode:=oNode;


  //  cxTreeList1.SetFocusedNode(oNode, [ssShift]);

  end;

  FIsNodeChanging:=False;
end;


//-------------------------------------------------------------------
function Tframe_Explorer.FocuseNodeByGUID(aGUID: string): Boolean;
//-------------------------------------------------------------------
var
  oNode: TcxTreeListNode;
begin
//  if not FActive then
//    Exit;

  Assert(aGUID<>'', 'GUID=''''');

  FIsNodeChanging:=True;

  oNode:=FindNodeByGUID (aGUID);

  Result := Assigned(oNode);


  if Assigned(oNode) then
    oNode.Focused:=True

//    dx_FocuseNode (oNode)
  else
  ;
//    g_Log.Error('node not found. guid='+aGUID);


  FIsNodeChanging:=False;
end;


//--------------------------------------------------------------
procedure Tframe_Explorer.UPDATE_NODE_CHILDREN_by_ObjName(aObjectName: string; aID: integer);
//--------------------------------------------------------------
var
  oNode: TcxTreeListNode;

begin
  oNode:=FindByObjectName(aObjectName, aID, False);

  if Assigned(oNode) then
    if oNode.Expanded then
    begin
      oNode.Collapse(false);
      oNode.Expand(false);
    end;

end;


//--------------------------------------------------------------------
procedure Tframe_Explorer.UPDATE_NODE_CHILDREN_ByGUID(aGUID: string);
//--------------------------------------------------------------------
var
  oNode: TcxTreeListNode;
begin

//  if not FActive then
//    Exit;

//  db_ViewDataSet(mem_Data);   WM_EXPLORER_FOCUS_NODE

  Assert(aGUID<>'', 'GUID=''''');


  oNode:=FindNodeByGUID(aGUID); //: TdxTreeListNode;
  if assigned(oNode) then //and (oNode.Expanded) then
    if (not oNode.HasChildren) or (oNode.Expanded) then
    begin
      UpdateNodeChildren (TrpPIDL(oNode.Data).Tree_ID1, oNode);

  //    UpdateNodeChildren (iTreeID, nil);
    end;


//   FDisableControls:=True;

 //  FDisableControls:=False;

end;


//--------------------------------------------------------------------
procedure Tframe_Explorer.DELETE_NODE_BY_GUID (aGUID: string );
//--------------------------------------------------------------------
var
  oNode: TcxTreeListNode;
  iTreeID: integer;
//  s: string;
begin
//  if not FActive then
//    Exit;

  Assert(aGUID<>'', 'GUID=''''');

  repeat
    oNode:=FindNodeByGUID (aGUID);

    if Assigned(oNode) then
    begin
     //  s:=oNode.Values[col_Name_.ItemIndex];

       try
         iTreeID:=TrpPIDL(oNode.Data).Tree_ID1;

//         iTreeID:=oNode.Values[col_Tree_ID.Index];
         DeleteDBNode (iTreeID);
       except

       end;  // try/except
    end;
  until
     not Assigned(oNode);
end;


//--------------------------------------------------------------------
procedure Tframe_Explorer.DELETE_NODE_BY_ObjectNameAndID(aObjectName: string;
    aID: integer);
//--------------------------------------------------------------------
var
  oNode: TcxTreeListNode;
  iTreeID: integer;
begin
//  if not FActive then
//    Exit;

  repeat
    oNode:=FindByObjectName(aObjectName, aID, False);

    if Assigned(oNode) then
    begin
      iTreeID:=TrpPIDL(oNode.Data).Tree_ID1;
    //  iTreeID:=oNode.Values[col_Tree_ID.Index];

      if iTreeID>0 then
         DeleteDBNode (iTreeID);
    end;

  until
    not Assigned(oNode);

end;



procedure Tframe_Explorer.cxTreeList1_Deletion(Sender: TcxCustomTreeList;
    ANode: TcxTreeListNode);
begin
   if Assigned(aNode.Data) then
     TrpPIDL(aNode.Data).Free;
end;



//----------------------------------------------------------------
procedure Tframe_Explorer.Load;
//----------------------------------------------------------------
begin
  FIsUpdated:=True;
  
  FIsNodeChanging := True;

  UpdateNodeChildren (0, nil);

  /////LoadStateFromReg (RegPath);

//  mem_Data1.First;

  FIsNodeChanging := False;


//  FIsUpdated:=True;
//  if if FileExists(FStateFileName) then
  cx_LoadFromReg_Expanded(cxTreeList1, col_Name_,  StateFileName);
  FIsUpdated:=False;

end;


//--------------------------------------------------------------------
procedure Tframe_Explorer.PopupMenu1Popup(Sender: TObject);
//--------------------------------------------------------------------
begin                
//  if not FActive then
//    Exit;

  if (otDisablePopup in Options) or
     (not Assigned(cxTreeList1.FocusedNode))
  then Exit;

  SaveSelectedToPIDL_List (FSelectedPIDLs);

  if assigned ( FOnAskMenu ) then
    FOnAskMenu (FSelectedPIDLs, PopupMenu1);

end;

// ---------------------------------------------------------------
procedure Tframe_Explorer.ReLoad_Project;
// ---------------------------------------------------------------
begin
  if Style=esProject then
  begin
    StateFileName:=dmMain.Dirs.Dir+ 'ExplorerState.txt';

    Load;
  end;
end;



//--------------------------------------------------------------
procedure Tframe_Explorer.GetShellMessage(aMsg: integer; aRec: TShellEventRec);
//--------------------------------------------------------------
var
  I: Integer;
  oIDList: TIDList;
begin
//  if not FActive then
//    Exit;


  //Exit;


  case aMsg of
 {   //-------------------------------------------------------------------
    WE_PROJECT_CHANGED:
    //-------------------------------------------------------------------
      if Style<>esDict then
        Load;

        
    procedure Shell_PostDelNode(aGUID: string);
    procedure Shell_PostDelNodeByList(aItemList: TIDList);
    procedure Shell_PostDelNodesByIDList(aIDList: TIDList);
    procedure Shell_PostFocusNode(aGUID: string);



}

    WE__EXPLORER_ReLoad_Project:  ReLoad_Project;

    //-------------------------------------------------------------------
    WE__EXPLORER_RENAME_ITEM: begin
    //-------------------------------------------------------------------
      RENAME_ITEM (aRec.GUID, aRec.Name);
    end;

  {  //-------------------------------------------------------------------
    WE__EXPLORER_UPDATE_FOLDER_AND_FOCUS_CHILD: begin
    //-------------------------------------------------------------------
      UPDATE_FOLDER_AND_FOCUS_CHILD (aRec.GUID,aRec.ID);
    end;
}
    //-------------------------------------------------------------------
    WE__EXPLORER_EXPAND_BY_GUID: begin
    //-------------------------------------------------------------------
      Assert(aRec.GUID<>'');

      ExpandByGUID(aRec.GUID);
    end;

    //-------------------------------------------------------------------
    WE__EXPLORER_UPDATE_NODE_CHILDREN_BY_OBJNAME: begin
    //-------------------------------------------------------------------
      UPDATE_NODE_CHILDREN_BY_ObjName(aRec.ObjName, aRec.ID);
    end;

    //-------------------------------------------------------------------
    WE__EXPLORER_UPDATE_NODE_CHILDREN_BY_GUID: begin
    //-------------------------------------------------------------------
      UPDATE_NODE_CHILDREN_ByGUID(aRec.GUID);
    end;

    //-------------------------------------------------------------------
    WE__EXPLORER_DELETE_NODES_BY_IDLIST: begin
    //-------------------------------------------------------------------
      oIDList:=TIDList(aRec.IDList);

      for I := 0 to oIDList.Count - 1 do
        DELETE_NODE_BY_ObjectNameAndID(oIDList[i].ObjName, oIDList[i].ID);

      //DELETE_NODE_BY_GUID(aRec.GUID);
    end;

    //-------------------------------------------------------------------
    WE__EXPLORER_DELETE_NODE_BY_GUID: begin
    //-------------------------------------------------------------------
      DELETE_NODE_BY_GUID(aRec.GUID);
    end;
  end;
end;


procedure Tframe_Explorer.Button3Click(Sender: TObject);
begin
  cx_LoadFromReg_Expanded(cxTreeList1, col_Name_, StateFileName);

end;

procedure Tframe_Explorer.Button4Click(Sender: TObject);
var
  sFolderGUID: string;
begin
  sFolderGUID:=g_Obj.ItemByName[OBJ_PROPERTY].RootFolderGUID;

  ExpandByGUID(ed_GUID.Text);



//  db_View ( dmExplorer.mem_Items);


end;

procedure Tframe_Explorer.cxTreeList1Collapsed(Sender: TcxCustomTreeList;
    ANode: TcxTreeListNode);
begin
  if StateFileName<>'' then
    if not FIsUpdated then
      cx_SaveToReg_Expanded(cxTreeList1, col_Name_, StateFileName);

end;

procedure Tframe_Explorer.cxTreeList1MouseWheel(Sender: TObject; Shift:
    TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
 //!!!!!!!!!!
  Handled := FindVCLWindow( MousePos ) <> Sender;
end;



procedure Tframe_Explorer.cxTreeList1_DblClick(Sender: TObject);
var
  b: Boolean;
begin
//  ShowMessage('cxTreeList1 Dbl Click');


//  if not cxTreeList1.HitTest.HitAtNode then
//    Exit;
//   //
//
//  DoChangeFocusedNode;
//
//
// /////////ShowMessage('Tframe_Explorer.dxTreeList1_DblClick(Sender: TObject);');
//
//
// Exit;

 // b:= cxTreeList1.HitTest.HitAtNode;


//
//  if not FActive then
//    Exit;

//  if not dx_CheckCursorInGrid1( TdxTreeList( Sender)) then
  if not cxTreeList1.HitTest.HitAtNode then
    exit;

  if Assigned(FOnItemDblClick) then
    if Assigned(cxTreeList1.FocusedNode) then
    begin
       NodeToPIDL (cxTreeList1.FocusedNode, FTempPIDL);
  //  if FFocusedPIDL=FTempPIDL then
   //   Exit;

      FOnItemDblClick (FTempPIDL)
    end;


end;

// ---------------------------------------------------------------
procedure Tframe_Explorer.cxTreeList1_FocusedNodeChanged(Sender:
    TcxCustomTreeList; APrevFocusedNode, AFocusedNode: TcxTreeListNode);
// ---------------------------------------------------------------
var
  s1: string;
  s2: string;
begin
//    ShowMessage('cxTreeList1FocusedNodeChanged');


  if APrevFocusedNode=AFocusedNode then
    Exit;


  if APrevFocusedNode<>nil then
    s1:=APrevFocusedNode.Values[col_Name_.ItemIndex]
  else
    s1:='null';

  if AFocusedNode<>nil then
    s2:=AFocusedNode.Values[col_Name_.ItemIndex];

  assert(APrevFocusedNode<>AFocusedNode);

  DoChangeFocusedNode;


//  ShowMessage('procedure Tframe_Explorer.cxTreeList1FocusedNodeChanged(Sender: TObject;   APrevFocusedNode, AFocusedNode: TcxTreeListNode); '+ s1 + '    ' + s2  );
end;

procedure Tframe_Explorer.cxTreeList1_MouseDown(Sender: TObject; Button:
    TMouseButton; Shift: TShiftState; X, Y: Integer);

begin
  FChangeChecked:= cxTreeList1.HitTest.HitAtStateImage;
end;

procedure Tframe_Explorer.cxTreeList1_MouseUp(Sender: TObject; Button:
    TMouseButton; Shift: TShiftState; X, Y: Integer);

var
  b: Boolean;
  k: Integer;
  oNode: TcxTreeListNode;
  oPIDL: TrpPIDL;
  s: string;

begin

  if not FChangeChecked then
    exit;

  FChangeChecked:=False;

  if Button = mbLeft then
  begin
    oNode:= cxTreeList1.GetNodeAt(X, Y);

    if Assigned(oNode) then
    begin
      oNode.StateIndex:=
        IIF(oNode.StateIndex=DEF_STATE_CHECKED, DEF_STATE_UNCHECKED, DEF_STATE_CHECKED);

      oPIDL := TrpPIDL(oNode.Data);


      if Assigned(oPIDL) and Eq(oPIDL.ObjectName,OBJ_REL_MATRIX) then
      begin
        b:=oNode.StateIndex<>0;

        k:=oPIDL.ref_ID;

      //  oPIDL.ID

    //  k:=oPIDL.ref_ID;

        g_EventManager.PostEvent_(et_REL_MATRIX_ENABLED, ['id', oPIDL.ID, 'Checked', b ] );
//        PostUserMessage(WM__REL_MATRIX_ENABLED, oPIDL.ID, Integer(b));


     //   k:=dmOnega_DB_data.Relief_XREF_Update1 (oPIDL.ID, b);
 //       k:=dmOnega_DB_data.Relief_XREF_Update (oPIDL.ref_ID, b);
     {
        s:=oNode.Values[col_Name_.ItemIndex];
        if not b then
          dmRel_Engine.CloseFile(s);
      }
       // s:='';


(*  b:=FDataSet.FieldByName(FLD_CHECKED).AsBoolean;
  s:=FDataSet.FieldByName(FLD_NAME).AsString;

  if not b then
    dmRel_Engine.CloseFile(s);
*)

     end;

    end;
  end;


end;



procedure Tframe_Explorer.ed_SearchKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
begin

end;

// ---------------------------------------------------------------
procedure Tframe_Explorer.Search_FocuseNodeByObjName;
// ---------------------------------------------------------------
var
  I: Integer;
  b: Boolean;
  iID: Integer;
  sObj: string;
  sPath: string;

  arr: TStrArray;

  oNode: TcxTreeListNode;

  //  function StringToStrArray(aValue: string; aDelimiter: Char = ';'): TStrArray;

begin
  if ADOStoredProc1.Active then
  begin
    sPath:=ADOStoredProc1.FieldByName(FLD_FOLDER_PATH).AsString;
    arr:=StringToStrArray (sPath, ' ');



    sObj:=ADOStoredProc1.FieldByName(FLD_OBJNAME).AsString;
    iID :=ADOStoredProc1.FieldByName(FLD_ID).AsInteger;


    if Eq(sObj, OBJ_PROPERTY) then
    begin
//      b:=ExpandByGUID(GUID_PROJECT);
//      b:=ExpandByGUID(GUID_COMMON_GROUP);
      b:=ExpandByGUID(GUID_PROPERTY);


       for I := 0 to High(arr) do    // Iterate
       begin
         b:=ExpandByGUID('{'+ arr[i] +'}');
       end;    // for

    end else

    if Eq(sObj, OBJ_LINK) then
    begin
//      b:=ExpandByGUID(GUID_PROJECT);
      b:=ExpandByGUID(GUID_LINK_GROUP);
      b:=ExpandByGUID(GUID_LINK);

      for I := 0 to High(arr) do    // Iterate
      begin
         b:=ExpandByGUID('{'+ arr[i] +'}');
      end;    // for

    end;

    FocuseNodeByObjName(sObj,iID);
  end;
end;

// ---------------------------------------------------------------
procedure Tframe_Explorer.Search_Next;
// ---------------------------------------------------------------
begin
  if ADOStoredProc1.Active then
  begin
    ADOStoredProc1.Next;
    Search_FocuseNodeByObjName;
  end;
end;

// ---------------------------------------------------------------
procedure Tframe_Explorer.Search_Prior;
// ---------------------------------------------------------------
begin
  if ADOStoredProc1.Active then
  begin
    ADOStoredProc1.Prior;
    Search_FocuseNodeByObjName;
  end;
end;

procedure Tframe_Explorer.act_Search_nextExecute(Sender: TObject);
begin
   if Sender=act_Search then
    Search(ed_Search.Text) else

  if Sender=act_Search_next then
    Search_next else

  if Sender=act_Search_prior then
    Search_Prior;
end;



procedure Tframe_Explorer.ActionList_SearchUpdate(Action: TBasicAction; var
    Handled: Boolean);
begin
  if ADOStoredProc1.Active then
  begin
    act_Search_next.Enabled := (ADOStoredProc1.RecordCount>1) and (not ADOStoredProc1.EOF);
    act_Search_prior.Enabled:= (ADOStoredProc1.RecordCount>1) and (not ADOStoredProc1.BOF);
  end;

end;

procedure Tframe_Explorer.Button1Click(Sender: TObject);
var
  b: Boolean;
begin
  b:=ExpandByGUID(GUID_PROJECT);

end;


procedure Tframe_Explorer.Button2Click(Sender: TObject);
var
  b: Boolean;
begin
//  b:=ExpandByGUID(GUID_PROJECT);
  b:=ExpandByGUID(GUID_LINK_GROUP);
  b:=ExpandByGUID(GUID_LINK);

end;

procedure Tframe_Explorer.Button5Click(Sender: TObject);
var
  b: Boolean;
begin

//   b:=ExpandByGUID(GUID_PROPERTY);

end;

procedure Tframe_Explorer.Button6Click(Sender: TObject);
var
  b: Boolean;
begin
  b:=ExpandByGUID(GUID_PROPERTY);
end;

procedure Tframe_Explorer.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    Search (ed_Search.Text);

end;



procedure Tframe_Explorer.GetMessage(aMsg: TEventType; aParams:
    TEventParamList; var aHandled: boolean);
var
  I: Integer;
  bChecked: Boolean;
  oIDList: TIDList;

   oNode: TcxTreeListNode;
begin
  case aMsg of

    et_Explorer_REFRESH_REL_MATRIX: UPDATE_NODE_CHILDREN_ByGUID(GUID_REL_MATRIX);
    et_Explorer_REFRESH_MAP_FILES:  UPDATE_NODE_CHILDREN_ByGUID(GUID_MAP);


    et_REL_MATRIX_LIST_CHECKED:
    begin
      oIDList:= aParams.IDList;// (msg.wParam);
      bChecked:=aParams.Checked; //  Boolean (msg.lParam);

      Assert(Assigned(oIDList));

      for I := 0 to oIDList.Count - 1 do    // Iterate
      begin
        oNode:=FindByObjectName(OBJ_REL_MATRIX, oIDList[i].id, false );
    //    oNode:=FindByObjectName(OBJ_REL_MATRIX, oIDList[i].ref_id, false );

        if assigned(oNode) then
         oNode.StateIndex:= IIF(bChecked, DEF_STATE_UNCHECKED, DEF_STATE_CHECKED);


      end;    // for


      //  PostUserMessage (WM__REL_MATRIX_LIST_CHECKED, Integer(FSelectedIDList), Integer( bChecked));
    end;

  end;

end;



procedure Tframe_Explorer.cxTreeList1StylesGetContentStyle(
  Sender: TcxCustomTreeList; AColumn: TcxTreeListColumn;
  ANode: TcxTreeListNode; var AStyle: TcxStyle);
var
  sFileName: string;
begin
   if  LowerCase( VarToStr (ANode.Values[col_OBJECT_NAME.ItemIndex])) = 'rel_matrix' then
   begin
     sFileName:= ANode.Values[col_Name_.ItemIndex];
     if (sFileName<>DN_FOLDER_REL_MATRIX)
        and (Pos(':', sFileName) > 0)
        and (not FileExists(sFileName))
     then
        AStyle:=cxStyle_StrikeOut;
   end;


         //DN_FOLDER_REL_MATRIX
//  AStyle:=cxStyle_StrikeOut;
end;

procedure Tframe_Explorer.ed_SearchPropertiesEditValueChanged( Sender: TObject);
begin

end;            


// ---------------------------------------------------------------
procedure Tframe_Explorer.Search(aSearch_text: string);
// ---------------------------------------------------------------
var
  b: boolean;
  k: Integer;
  s: string;
begin


//  b:=cb_WHOLE_WORD.EditValue;

  k:=dmOnega_db_data.OpenStoredProc(ADOStoredProc1, 'sp_Explorer_Search',
    [
      FLD_PROJECT_ID, dmMain.ProjectID,
      FLD_SEARCH, Trim(aSearch_text),
      FLD_WHOLE_WORD, cb_WHOLE_WORD.Checked

 //     FLD_WHOLE_WORD, cb_WHOLE_WORD.EditValue
    ]);


//db_View (ADOStoredProc1);


{  cb_Search1.Items.Clear;

  with ADOStoredProc1 do
    while not EOF do
    begin
      cb_Search1.Items.Add(ADOStoredProc1.FieldByName(FLD_NAME).AsString);

      Next;
    end;
}

//  ADOStoredProc1.First;

  Search_FocuseNodeByObjName;
end;


procedure Tframe_Explorer.ed_SearchPropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
//  Search (DisplayValue);
//   ShowMessage('procedure Tframe_Explorer.ed_SearchPropertiesValidate');
end;

end.


