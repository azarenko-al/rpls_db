unit i_Explorer;

interface

type

//  IInspector111 = interface(IDispatch)
//    ['{23D74FFF-4E0D-48D4-AA93-B9F773786C2F}']
//    procedure SetFieldValue(const aFieldName: WideString; aValue: OleVariant); safecall;
//  end;


  IExplorerX = interface(IDispatch)
  ['{D06978C5-D3CE-4C09-9A9A-1ECF9A748C34}']
  ///  procedure UpdateNodeChildren_ByGUID(const aGUID: WideString); safecall;
   /// procedure UpdateNodeChildren_ByObjName(const aObjName: WideString; aID: integer); safecall;

    procedure UPDATE_NODE_CHILDREN_by_ObjName( aObjectName: string; aID: integer);
    procedure UPDATE_NODE_CHILDREN_ByGUID( aGUID: string);

    function FocuseNodeByGUID(aGUID: string): Boolean;

    function FocuseNodeByObjName(aObjectName: Widestring; aID: integer): Boolean;


  end;




    //  procedure Shell_UpdateNodeChildren_ByGUID(aGUID: string); overload;
//    procedure Shell_UpdateNodeChildren_ByObjName(aObjName: string; aID: integer); overload;




var
  g_IExplorer_lib: IExplorerX;
  g_IExplorer_prj: IExplorerX;



implementation

end.
