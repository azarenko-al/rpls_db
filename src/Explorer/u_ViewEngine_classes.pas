unit u_ViewEngine_classes;

interface

uses Classes,SysUtils,DB,

   u_DB,

   u_func,
   u_const_db;

type
  TViewEngineFieldList    = class;
  TViewEngineFieldRefList = class;
  TViewField              = class;

  // ---------------------------------------------------------------
  TViewField = class(TCollectionItem)
  private
//    XREF_FieldName2 : string;

  public
 //   Enabled : Boolean;

 is_custom: boolean;


    ID             : Integer;

    PARENT_ID      : Integer;

//    Name_           : string;
    Alias           : string;
    Field_Name1     : string;

    CAPTION        : string;
    FieldTypeStr      : string;

    IsFOLDER      : Boolean;
    IsReadOnly    : Boolean;

    IsShowButton : Boolean;

    IsHidden : Boolean;

//    IsCalculated_ : Boolean; // ����������� ����

  //  ItemsText      : string;
    ListNames      : TStringList;



    IS_IN_LIST     : Boolean;
    IS_IN_VIEW     : Boolean;

    Status_Name    : string;

    XREF_ObjectName: string;
    XREF_TableName : string;

    DisplayIndex : Integer;

//    Min_Value : Integer;
//    Max_Value : Integer;

//    View_FieldName1: string;

    Items1          : TStringList;

    SubItems: TViewEngineFieldRefList;



    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;

    procedure LoadFromDataset(aDataSet: TDataSet);

    procedure LoadFromDataset_user(aDataSet: TDataSet);

  end;

  // ---------------------------------------------------------------
  TViewEngineObject = class(TCollectionItem)
  // ---------------------------------------------------------------
  private
  ////////////////
//    FilterFields: TViewEngineFieldRefList;

  public
    ID : Integer;
    ObjectName : string;
   // List_ViewName : string;  //for List

    Fields: TViewEngineFieldList;

    ViewFields: TViewEngineFieldRefList;
    ListFields: TViewEngineFieldRefList;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;

//    function GetTableSQLForFields(aListFields: TViewEngineFieldRefList): string;

    procedure Prepare;
  end;

  // ---------------------------------------------------------------
  TViewEngineObjectList = class(TCollection)
  // ---------------------------------------------------------------
  
  private
    function FindByID(aID: integer): TViewEngineObject;
     function GetItems(Index: Integer): TViewEngineObject;
   public
    constructor Create;
    function AddItem: TViewEngineObject;

    function FindByObjectName(aObjectName: string): TViewEngineObject;

    procedure LoadFromDB_Objects(aDataset: TDataSet);
    procedure LoadFromDB_ObjectFields(aDataset: TDataSet);

    procedure LoadFromDB_ObjectFields_user(aDataset: TDataSet);

    procedure Prepare;


    property Items[Index: Integer]: TViewEngineObject read GetItems; default;
  end;


  // ---------------------------------------------------------------
  TViewEngineFieldList = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TViewField;
  public
    constructor Create;
    function AddItem: TViewField;
    function FindByID(aID: integer): TViewField;

    property Items[Index: Integer]: TViewField read GetItems; default;
  end;


  // ---------------------------------------------------------------
  TViewEngineFieldRefList = class(TList)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TViewField;
  public
    function FindByFieldName(aName: string): TViewField;
    function FindByID(aID: integer): TViewField;
// TODO: Sort
//  procedure Sort;
    property Items[Index: Integer]: TViewField read GetItems; default;
  end;



implementation

const
  FLD_Hidden = 'Hidden';
 // FLD_Calculated = 'Calculated';

  FLD_ROW = 'ROW';

  FLD_Min_Value = 'Min_Value';
  FLD_Max_Value = 'Max_Value';



//  FLD_VIEW_FieldName = 'VIEW_FieldName';



constructor TViewEngineFieldList.Create;
begin
  inherited Create(TViewField);
end;

function TViewEngineFieldList.AddItem: TViewField;
begin
  Result := TViewField(inherited Add);
end;

// ---------------------------------------------------------------
function TViewEngineFieldList.FindByID(aID: integer): TViewField;
// ---------------------------------------------------------------
var I: Integer;
begin
  Result := nil;
  for I := 0 to Count - 1 do
    if Items[i].ID = aID then
    begin
      result := Items[i];
      exit;
    end;
end;


function TViewEngineFieldList.GetItems(Index: Integer): TViewField;
begin
  Result := TViewField(inherited Items[Index]);
end;

constructor TViewEngineObjectList.Create;
begin
  inherited Create(TViewEngineObject);
end;

function TViewEngineObjectList.AddItem: TViewEngineObject;
begin
  Result := TViewEngineObject(inherited Add);
end;

function TViewEngineObjectList.GetItems(Index: Integer): TViewEngineObject;
begin
  Result := TViewEngineObject(inherited Items[Index]);
end;


// ---------------------------------------------------------------
function TViewEngineObjectList.FindByObjectName(aObjectName: string): TViewEngineObject;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to Count - 1 do
    if Eq(Items[i].ObjectName, aObjectName) then
    begin
      result := Items[i];
      exit;
    end;
end;

// ---------------------------------------------------------------
function TViewEngineObjectList.FindByID(aID: integer): TViewEngineObject;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Result := nil;

  for I := 0 to Count - 1 do
    Assert(Items[i].ID>0);

  Assert(Count>0);

  for I := 0 to Count - 1 do
    if Items[i].ID= aID then
    begin
      result := Items[i];
      exit;
    end;
end;


constructor TViewEngineObject.Create(Collection: TCollection);
begin
  inherited Create(Collection);

  Fields := TViewEngineFieldList.Create();

  ViewFields := TViewEngineFieldRefList.Create();
  ListFields := TViewEngineFieldRefList.Create();
//  FilterFields := TViewEngineFieldRefList.Create();
end;

// ---------------------------------------------------------------
destructor TViewEngineObject.Destroy;
// ---------------------------------------------------------------
begin
//  FreeAndNil(FilterFields);
  FreeAndNil(ListFields);
  FreeAndNil(ViewFields);

  FreeAndNil(Fields);
  inherited Destroy;
end;


// ---------------------------------------------------------------
procedure TViewEngineObject.Prepare;
// ---------------------------------------------------------------

   //procedure TViewEngineObject.Prepare;



var
  I: Integer;
  oViewField: TViewField;

begin
  // sort items  //Fields
  {

  for I := Fields.Count-1 downto 0 do
    if Fields[i].PARENT_ID>0 then
    begin
      oViewField := Fields.FindByID(Fields[i].PARENT_ID);

      Assert(Assigned(oViewField), 'Value not assigned');

      oViewField.SubItems.Add(Fields[i]);
      Fields.Delete(i);
    end;



  for I := Fields.Count-1 downto 0 do
    if Fields[i].Enabled then
    begin

      oViewField := Fields.FindByID(Fields[i].PARENT_ID);

      Assert(Assigned(oViewField), 'Value not assigned');

      oViewField.SubItems.Add(Fields[i]);
      Fields.Delete(i);

    end;

  }






  ListFields.Clear;
  ViewFields.Clear;

  for I := 0 to Fields.Count - 1 do
  begin


    if Fields[i].IS_IN_LIST or Fields[i].IsHidden then
      ListFields.Add(Fields[i]);

    if Fields[i].IS_IN_VIEW or Fields[i].IsHidden then
      ViewFields.Add(Fields[i]);

  end;
end;

// ---------------------------------------------------------------
constructor TViewField.Create(Collection: TCollection);
// ---------------------------------------------------------------
begin
  inherited Create(Collection);
  ListNames := TStringList.Create();
  Items1    := TStringList.Create();


  SubItems:=TViewEngineFieldRefList.Create();


end;

destructor TViewField.Destroy;
begin
  FreeAndNil(ListNames);
  FreeAndNil(Items1);

  FreeAndNil(SubItems);
  

  inherited Destroy;
end;


function TViewEngineFieldRefList.GetItems(Index: Integer): TViewField;
begin
  Result := TViewField(inherited Items[Index]);
end;


// ---------------------------------------------------------------
function TViewEngineFieldRefList.FindByFieldName(aName: string): TViewField;
// ---------------------------------------------------------------
var I: Integer;
begin
  Result := nil;
  for I := 0 to Count - 1 do
    if Eq(Items[i].Field_Name1, aName) then
    begin
      result := Items[i];
      exit;
    end;
end;



// ---------------------------------------------------------------
function TViewEngineFieldRefList.FindByID(aID: integer): TViewField;
// ---------------------------------------------------------------
var I: Integer;
begin
  Result := nil;
  for I := 0 to Count - 1 do
    if Items[i].ID=aID then
    begin
      result := Items[i];
      exit;
    end;
end;


// ---------------------------------------------------------------
procedure TViewEngineObjectList.LoadFromDB_ObjectFields(aDataset: TDataSet);
// ---------------------------------------------------------------
var
  oViewField: TViewField;
  oObject: TViewEngineObject;
  iID: integer;
  i: integer;
  sOBJBNAME: string;
begin
  Assert(aDataSet.Active);


  with aDataset do
    while not EOF do
  begin
    iID :=aDataset.FieldBYName(FLD_object_ID).AsInteger;
 //   oObject := FindByID(iID);

    sOBJBNAME :=aDataset.FieldBYName(FLD_OBJNAME).AsString;
    Assert(sOBJBNAME <> '');

    oObject := FindByObjectName(sOBJBNAME);


    Assert(Assigned(oObject), 'Value not assigned: object_ID='+ IntToStr(iID) +'  '+ sOBJBNAME);

    oViewField:=oObject.Fields.AddItem;

    oViewField.LoadFromDataset (aDataset);


  //  oObject.Prepare;
   // AddItem.ObjectName :=aDataset.FieldByName(FLD_NAME).AsString;


    Next;
  end;


//  for I := 0 to Count - 1 do
//    Items[i].

  Prepare;


//  db_ForEach(aDataset, ForEach_ObjectFields);
end;


// ---------------------------------------------------------------
procedure TViewEngineObjectList.LoadFromDB_ObjectFields_user(aDataset:
    TDataSet);
// ---------------------------------------------------------------
var
  oViewField: TViewField;
  oObject: TViewEngineObject;
  iID: integer;
  i: integer;
  sOBJBNAME: string;
begin
  Assert(aDataSet.Active);


  with aDataset do
    while not EOF do
  begin
    iID :=aDataset.FieldBYName(FLD_object_ID).AsInteger;
 //   oObject := FindByID(iID);

    sOBJBNAME :=aDataset.FieldBYName(FLD_OBJNAME).AsString;
    oObject := FindByObjectName(sOBJBNAME);


    Assert(Assigned(oObject), 'Value not assigned: object_ID='+ IntToStr(iID));

    oViewField:=oObject.Fields.AddItem;

    oViewField.LoadFromDataset_user (aDataset);


  //  oObject.Prepare;
   // AddItem.ObjectName :=aDataset.FieldByName(FLD_NAME).AsString;

    Next;
  end;

  Prepare;


//  db_ForEach(aDataset, ForEach_ObjectFields);
end;


// ---------------------------------------------------------------
procedure TViewEngineObjectList.LoadFromDB_Objects(aDataset: TDataSet);
// ---------------------------------------------------------------
var
 oObject: TViewEngineObject;

begin
  Assert(aDataSet.Active);


  Clear;
 // db_ForEach(aDataset, ForEach_Object);

  with aDataset do
    while not EOF do
  begin
    oObject:=AddItem;

    oObject.ID           :=FieldByName(FLD_ID).AsInteger;
    oObject.ObjectName   :=FieldByName(FLD_NAME).AsString;
  //  oObject.List_ViewName:=FieldByName(FLD_VIEWNAME).AsString;

    Next;
  end;
end;

// ---------------------------------------------------------------
procedure TViewEngineObjectList.Prepare;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    Items[i].Prepare;

end;

// ---------------------------------------------------------------
procedure TViewField.LoadFromDataset(aDataSet: TDataSet);
// ---------------------------------------------------------------
begin
  Assert(aDataSet.Active);

  with aDataSet do
  begin
    try
//      Enabled  :=FieldBYName(FLD_Enabled).AsBoolean;

      ID              :=FieldBYName(FLD_ID).AsInteger;    // with
      PARENT_ID       :=FieldBYName(FLD_PARENT_ID).AsInteger;

  //    Name_            :=Trim(FieldByName(FLD_NAME).AsString);
      Alias            :=Trim(FieldByName(FLD_Alias).AsString);
      Field_Name1       :=Trim(FieldByName(FLD_NAME).AsString);

      CAPTION         :=FieldBYName(FLD_CAPTION).AsString;
      FieldTypeStr    :=FieldBYName(FLD_TYPE).AsString;

      ISFOLDER        :=FieldBYName(FLD_IS_FOLDER).AsBoolean;
      ISReadOnly      :=FieldBYName(FLD_IS_ReadOnly).AsBoolean;
      ListNames.Text  :=FieldBYName(FLD_ITEMS).AsString;

      IsHidden        :=FieldBYName(FLD_Hidden).AsBoolean;
    //  IsCalculated    :=FieldBYName(FLD_Calculated).AsBoolean;

      DisplayIndex    :=FieldBYName(FLD_Index_).AsInteger;

      IS_IN_LIST      :=FieldBYName(FLD_IS_IN_LIST).AsBoolean;
      IS_IN_VIEW      :=FieldBYName(FLD_IS_IN_VIEW).AsBoolean;

      Status_Name     :=FieldBYName(FLD_Status_Name).AsString;

      XREF_ObjectName :=FieldBYName(FLD_XREF_ObjectName).AsString;
      XREF_TableName  :=FieldBYName(FLD_XREF_TableName).AsString;
    //  XREF_FieldName1  :=Trim(FieldBYName(FLD_XREF_FieldName).AsString);

     // View_FieldName1  :=Trim(FieldBYName(FLD_VIEW_FieldName).AsString);

//      Min_Value :=FieldBYName(FLD_Min_Value).AsInteger;
//      Max_Value :=FieldBYName(FLD_Max_Value).AsInteger;

    except

    end;



  end;
end;


// ---------------------------------------------------------------
procedure TViewField.LoadFromDataset_user(aDataSet: TDataSet);
// ---------------------------------------------------------------
begin
  Assert(aDataSet.Active);

  with aDataSet do
  begin
    try
//      Enabled  :=FieldBYName(FLD_Enabled).AsBoolean;

      ID              :=FieldBYName(FLD_ID).AsInteger;    // with
//      PARENT_ID       :=FieldBYName(FLD_PARENT_ID).AsInteger;

  //    Name_            :=Trim(FieldByName(FLD_NAME).AsString);
//      Alias            :=Trim(FieldByName(FLD_Alias).AsString);
      Field_Name1       :=Trim(FieldByName(FLD_NAME).AsString);

      CAPTION         :=FieldBYName(FLD_CAPTION).AsString;
      FieldTypeStr    :=FieldBYName(FLD_TYPE).AsString;

      IS_IN_LIST      :=FieldBYName(FLD_IS_IN_LIST).AsBoolean;
      IS_IN_VIEW      :=FieldBYName(FLD_IS_IN_VIEW).AsBoolean;


      Status_Name     :=FieldBYName(FLD_Status_Name).AsString;
      

//      ISFOLDER        :=FieldBYName(FLD_IS_FOLDER).AsBoolean;
//      ISReadOnly      :=FieldBYName(FLD_IS_ReadOnly).AsBoolean;

{
      ListNames.Text  :=FieldBYName(FLD_ITEMS).AsString;

      IsHidden        :=FieldBYName(FLD_Hidden).AsBoolean;
    //  IsCalculated    :=FieldBYName(FLD_Calculated).AsBoolean;

      DisplayIndex    :=FieldBYName(FLD_Index_).AsInteger;

      IS_IN_LIST      :=FieldBYName(FLD_IS_IN_LIST).AsBoolean;
      IS_IN_VIEW      :=FieldBYName(FLD_IS_IN_VIEW).AsBoolean;


      XREF_ObjectName :=FieldBYName(FLD_XREF_ObjectName).AsString;
      XREF_TableName  :=FieldBYName(FLD_XREF_TableName).AsString;
    //  XREF_FieldName1  :=Trim(FieldBYName(FLD_XREF_FieldName).AsString);

     // View_FieldName1  :=Trim(FieldBYName(FLD_VIEW_FieldName).AsString);

      Min_Value :=FieldBYName(FLD_Min_Value).AsInteger;
      Max_Value :=FieldBYName(FLD_Max_Value).AsInteger;
 }

    except

    end;



  end;
end;



end.


(*
      if (((aFieldShowType=fstView) and (FieldByName(FLD_IS_IN_VIEW).AsBoolean)) or
          ((aFieldShowType=fstList) and (FieldByName(FLD_IS_IN_LIST).AsBoolean)))
*)


