unit fr_Browser;

interface
{$I ver.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, StdCtrls,

  Dialogs,  ExtCtrls,

  u_Storage,

  I_Shell,
  u_shell_var,
  dm_Main,

 // dm_Object_Manager,
  u_Object_Manager,

  u_log,
  u_func,
  u_db,
  u_dlg,
//  u_func_msg,

  u_types,
  u_const,
 // u_const_msg,

  u_Explorer,

  fr_View_Base,

  fr_Browser_simple_view,
  fr_Browser_List,

  fr_Browser_Container,

  dm_Folder
//  dm_User

  , rxPlacemnt, ComCtrls, ToolWin
  //, cxPC, cxControls, ComCtrls
  ;

type


  Tframe_Browser = class(TForm, IShellNotifyX)
    FormPlacement1: TFormPlacement;
    PageControl1: TPageControl;
    p_Main: TTabSheet;
    p_Blank: TTabSheet;
    Panel1: TPanel;
    ToolBar1: TToolBar;
    Button1: TButton;

    procedure Button1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);

  private
    FObjectName: string;
    FIsFolder  : boolean;
  //  FProcIndex: integer;
   // FMode: (mtNone,mtList,mtItem);

    Ffr_View_Base: Tframe_View_Base;

    Fframe_Browser: Tframe_Browser_Container;

    FOnObjectFocused: TOnObjectFocused;

    procedure GetShellMessage(aMsg: integer; aRec: TShellEventRec);

    procedure DoOnObjectDblClick (aObjectName: string; aID: integer; aGUID,aName: string);
    procedure DoOnObjectClick (aObjectName: string; aID: integer; aGUID,aName: string);

  private
    SelectedID: integer;
    SelectedObjectName: string;
    SelectedName: string;
    SelectedGUID: string;

    FRegPath: string;

    procedure DoLog (aMsg: string);
  //  procedure GetMessage(aMsg: integer; aParams: TEventParamList; var aHandled:  Boolean);
  public

    procedure ViewObjectByPIDL (aPIDL: TrpPIDL);
    procedure ViewObjectByID (aObjectName: string; aID: integer);

    property OnObjectFocused: TOnObjectFocused read FOnObjectFocused write FOnObjectFocused;

  end;


//==================================================================
implementation {$R *.dfm}
//==================================================================

const
  REGISTRY_COMMON_FORMS  = 'Software\Onega\Common\Forms\';
                           


procedure PageControl_SetActiveTabSheet(aPageControl: TPageControl; aTabSheet: TTabSheet);
var
  I: Integer;
begin
//  aPageControl.

  for I := 0 to aPageControl.PageCount-1 do
    aPageControl.Pages[i].TabVisible:= False;// aTabVisible;

  aPageControl.ActivePage:=aTabSheet;

end;


procedure Tframe_Browser.Button1Click(Sender: TObject);
begin
 // Ffr_View_Base.Repaint;
end;

//-------------------------------------------------------------------
procedure Tframe_Browser.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  Caption:='������';


 // GSPages1.Align:=alClient;
//  cxPageControl1.Align:=alClient;
//
//  cxPageControl1.HideTabs:=True;

 // PageControl111.ActivePage:=nil;

  PageControl1.Align:=alClient;


  PageControl_SetActiveTabSheet (PageControl1, nil);

 // FProcIndex:=g_EventManager.RegisterProc(GetMessage, Name);




//  FRegPath:= REGISTRY_COMMON_FORMS + ClassName +'\';
  FRegPath:=g_Storage.GetPathByClass(ClassName);

  //REGISTRY_COMMON_FORMS + ClassName +'\';

  FormPlacement1.IniFileName:=FRegPath;
  FormPlacement1.Active:=True;

  g_ShellEvents.RegisterObject (Self);
end;                       

/// g_EventManager.UnRegisterProc(FProcIndex, GetMessage);



//--------------------------------------------------------------------
procedure Tframe_Browser.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  g_ShellEvents.UnRegisterObject (Self);

 // g_EventManager.UnRegisterProc(FProcIndex, GetMessage);

  if Assigned(Ffr_View_Base) then  FreeAndNil (Ffr_View_Base);
  if Assigned(Fframe_Browser) then FreeAndNil (Fframe_Browser);

  inherited;
end;

//--------------------------------------------------------------------
procedure Tframe_Browser.ViewObjectByID (aObjectName: string; aID: integer);
//--------------------------------------------------------------------
var oPIDL: TrpPIDL;
begin
  oPIDL:=TrpPIDL.Create;
  oPIDL.ID:=aID;
  oPIDL.ObjectName:=aObjectName;
  oPIDL.IsFolder:=False;

  ViewObjectByPIDL (oPIDL );

  FreeAndNil(oPIDL);

 // .Free;
end;


procedure Tframe_Browser.DoLog (aMsg: string);
begin
  //raise Exception.Create('PostEvent (WE_LOG, '''', aMsg);');
  g_Log.Add(aMsg);
end;



//--------------------------------------------------------------------
procedure Tframe_Browser.ViewObjectByPIDL (aPIDL: TrpPIDL);
//--------------------------------------------------------------------

//
//  procedure DoDisable();
//  var
//    i: integer;
//  begin
//
//{    for i:=0 to Ffr_View_Base.pn_Main.ControlCount-1 do
//      DisableControls_(TWinControl(Ffr_View_Base.pn_Main.Controls[i]));
//
//    for i:=0 to Ffr_View_Base.TBDock1.ControlCount-1 do
//      Ffr_View_Base.TBDock1.Controls[i].Enabled:= False;
//}
//  end;
//
//
//  procedure DoEnable();
//  var
//    i: integer;
//  begin
//
//{    for i:=0 to Ffr_View_Base.pn_Main.ControlCount-1 do
//      EnableControls_(TWinControl(Ffr_View_Base.pn_Main.Controls[i]));
//
//    for i:=0 to Ffr_View_Base.TBDock1.ControlCount-1 do
//      Ffr_View_Base.TBDock1.Controls[i].Enabled:= true;
//}
//  end;
//


var
  b: Boolean;
  tObjType: TrpObjectType;
  oForm: TForm;
  sObjectName: string;
  iObjectID: integer;
 // oForm: Integer;
begin
//  try

  { TODO : optimize }
  if (aPIDL=nil) then
  begin

    try
       if Assigned(Ffr_View_Base)  then begin FreeAndNil (Ffr_View_Base); {DOLog('FreeAndNil Ffr_View_Base');} end;
       if Assigned(Fframe_Browser) then begin FreeAndNil (Fframe_Browser);{ DOLog('FreeAndNil Fframe_Browser');} end;
    except
    end;  // try/except

    Exit;
  end;

  sObjectName:=aPIDL.ObjectName;
  iObjectID:=aPIDL.ID;


  SelectedID:=aPIDL.ID;
  SelectedObjectName:=aPIDL.ObjectName;
  SelectedGUID:=aPIDL.GUID;


  if (sObjectName <> FObjectName) OR (aPIDL.IsFolder <> FIsFolder) then
  // free old browser
  begin
    try
      if Assigned(Ffr_View_Base)  then begin FreeAndNil (Ffr_View_Base); {DoLog('FreeAndNil Ffr_View_Base'); } end;
      if Assigned(Fframe_Browser) then begin FreeAndNil (Fframe_Browser); {DoLog('FreeAndNil Fframe_Browser');} end;
    except
    end;  // try/except

  ////////  cxPageControl1.ActivePage:=p_Blank;

    PageControl_SetActiveTabSheet(PageControl1, p_Blank);

///////    GSPages1.ActivePage:=ts_Blank;

    FObjectName:='';
    FIsFolder := aPIDL.IsFolder;
  end;

  if Assigned(Ffr_View_Base) then
  begin
//////    gLog.

//////    Ffr_View_Base.CanBeModified:= dmUser.ObjectCanBeModified (aPIDL.ObjectName, aPIDL.ID);
    Ffr_View_Base.View(aPIDL.ID, aPIDL.GUID);
    Exit;
  end;

//  FObjectName := aPIDL.ObjectName;
  b:=Assigned(g_Obj.ItemByName[sObjectName]);

  if not Assigned(g_Obj.ItemByName[sObjectName]) then
    raise Exception.Create(sObjectName);

  Assert(Assigned(g_Obj.ItemByName[sObjectName]), 'Value not assigned:' + sObjectName);


  tObjType:=g_Obj.ItemByName[sObjectName].Type_;

  if (not FoundInProjectTypes(tObjType)) and
     (not FoundInDictTypes(tObjType)) and
     (not FoundInMiscTypes(tObjType))
  then
    Exit;
  //  raise Exception.Create(aPIDL.ObjectName);
//    Exit;


  if (aPIDL.IsFolder)
     then
  begin
    if not Assigned(Fframe_Browser) then
    begin
//      CreateChildForm(Tframe_Browser_Container, Fframe_Browser, p_Main);
      CreateChildForm(Tframe_Browser_Container, Fframe_Browser, p_Main);

//      Fframe_Browser:=Tframe_Browser_Container.CreateChildForm (ts_Main);
      Fframe_Browser.OnObjectDblClick:=DoOnObjectDblClick;
      Fframe_Browser.OnObjectClick   :=DoOnObjectClick;

    end;

    Fframe_Browser.SetObjectName (sObjectName);
    Fframe_Browser.View(aPIDL.ID,aPIDL.GUID);


  end else

  begin
    if Assigned(Ffr_View_Base) then
      raise Exception.Create('Assigned(Ffr_View_Base)');

//    oForm := dmObject_Manager.GetViewForm (sObjectName, Self);
    oForm := g_Object_Manager.GetViewForm (sObjectName, Self);
    Ffr_View_Base:=Tframe_View_Base(oForm);

    FObjectName:=sObjectName;


    if not Assigned(Ffr_View_Base) then
    begin
      Ffr_View_Base:=Tframe_Browser_simple_View.Create(Self);
      Ffr_View_Base.SetObjectName(sObjectName);
    end;

    if Assigned(Ffr_View_Base) then
    begin
      CursorHourGlass();

//      CopyDockingEx(Ffr_View_Base, p_Main);
      CopyDockingEx(Ffr_View_Base, p_Main);

//      CopyControls(Ffr_View_Base, ts_Main);
      CursorDefault();
    end;
{
    if Assigned(Ffr_View_Base) then
      Ffr_View_Base.View(aPIDL.ID);
}

  end;

//  cxPageControl1.ActivePage:=p_Main;

  PageControl_SetActiveTabSheet(PageControl1, p_Main);


//  GSPages1.ActivePage:=ts_Main;


  if Assigned(Ffr_View_Base) then
  begin
 //zzzzzzzz   Ffr_View_Base.CanBeModified:= dmUser.ObjectCanBeModified (aPIDL.ObjectName, aPIDL.ID);
    Ffr_View_Base.View(aPIDL.ID, aPIDL.GUID);

{
    if not Ffr_View_Base.CanBeModified and (aPIDL.ID<>0) then
      DoDisable()
    else
      DoEnable();
}
  end;


end;


//-------------------------------------------------------------------
procedure Tframe_Browser.DoOnObjectDblClick (aObjectName: string; aID: integer; aGUID,aName: string);
//-------------------------------------------------------------------
var oPIDL: TrpPIDL;
  I: Integer;
  sGUID: string;
  arStrArray: TStrArray;
begin
  if not Assigned(IMainProjectExplorer) then
    Exit;
    


  SelectedID:=aID;
  SelectedObjectName:=aObjectName;
  SelectedName:=aName;


  oPIDL:=TrpPIDL.Create;
  oPIDL.ID:=aID;
  oPIDL.ObjectName:=aObjectName;
  ViewObjectByPIDL (oPIDL);
  oPIDL.Free;

  if Assigned(FOnObjectFocused) then
    FOnObjectFocused (SelectedObjectName,SelectedID,aGUID,aName);

  arStrArray:= dmFolder.GetFolderGUIDList(g_Obj.ItemByName[aObjectName].Type_, SelectedID);

  for i:= High(arStrArray) downto 0 do
    IMainProjectExplorer.ExpandByGUID(arStrArray[i]);
//    PostEvent (WE_EXPLORER_EXPAND_BY_GUID,
  //             [app_Par (PAR_GUID, arStrArray[i]) ]);

  sGUID:= gl_DB.GetGUIDByID(g_Obj.ItemByName[aObjectName].TableName, SelectedID);




  IMainProjectExplorer.FocuseNodeByGUID(sGUID);



//  PostEvent (WM_EXPLORER_FOCUS_NODE,
  //           [app_Par (PAR_GUID, sGUID) ]);

(*  PostEvent (WM_EXPLORER_SELECT_NODE_BY_ID_IN_FOLDER,
             [app_Par (PAR_ID, aID),
              //app_Par (PAR_FOLDER_ID, aFolderID),
              app_Par (PAR_OBJECT_NAME, aObjectName)
              ] );*)

end;

//-------------------------------------------------------------------
procedure Tframe_Browser.DoOnObjectClick (aObjectName: string; aID: integer; aGUID,aName: string);
//-------------------------------------------------------------------
var oPIDL: TrpPIDL;
begin
  SelectedID:=aID;
  SelectedObjectName:=aObjectName;
  SelectedName:=aName;

  if Assigned(FOnObjectFocused) then
    FOnObjectFocused (SelectedObjectName,SelectedID,aGUID,aName);

end;

procedure Tframe_Browser.GetShellMessage(aMsg: integer; aRec: TShellEventRec);
begin
  case aMsg of
    //-------------------------------------------------------------------
    WE__EXPLORER_DELETE_NODE_BY_GUID: begin
    //-------------------------------------------------------------------
    //  sGUID:=aParams.ValueByName(PAR_GUID);

      if SelectedGUID = aRec.GUID then
        ViewObjectByPIDL(nil);
    end;
  end;

end;

end.







{

//-------------------------------------------------------------------
procedure Tframe_Browser.DoOnObjectClick (aObjectName: string; aID: integer; aName: string);
//-------------------------------------------------------------------
var oPIDL: TrpPIDL;
begin
  SelectedID:=aID;
  SelectedObjectName:=aObjectName;
  SelectedName:=aName;

  if Assigned(FOnObjectFocused) then
    FOnObjectFocused (SelectedObjectName,SelectedID,aName);

{
  oPIDL:=TrpPIDL.Create;
  oPIDL.ID:=aID;
  oPIDL.ObjectName:=aObjectName;
  ViewObjectByPIDL (oPIDL);
  oPIDL.Free;

  PostEvent (WM_EXPLORER_SELECT_NODE_BY_ID_IN_FOLDER,
             [app_Par (PAR_ID, aID),
              app_Par (PAR_FOLDER_ID, aFolderID),
              app_Par (PAR_OBJECT_NAME, aObjectName)
              ] );
}
end;




{
//--------------------------------------------------------------------
procedure Tframe_Browser.GetMessage(aMsg: integer; aParams: TEventParamList; var aHandled: Boolean);
//--------------------------------------------------------------------
var
  sGUID: string;
begin
  case aMsg of
    //-------------------------------------------------------------------
    WE_EXPLORER_DELETE_NODE_BY_GUID: begin
    //-------------------------------------------------------------------
      sGUID:=aParams.ValueByName(PAR_GUID);

      if SelectedGUID = sGUID then
        ViewObjectByPIDL(nil);
    end;
  end;

end;
}

