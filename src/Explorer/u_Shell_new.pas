unit u_Shell_new;

interface

uses
  SysUtils, Classes, 

   I_DBExplorerForm,

/// / u_func,
  u_classes,
//  u_types

  
  u_func,

  I_Object_list

  ;

type
  TShellParamRec = record
                    Command: (ctPostFocusNode);

                    GUID: string;
                    NewName: string;

                    ObjName: string;
                    ID: Integer;

                    ItemList: TIDList;
                  end;

  TOnShellEvent = procedure (aRec: TShellParamRec) of object;

type
  // ---------------------------------------------------------------
  TShellInterfaceList = class(TInterfaceList)
  // ---------------------------------------------------------------
  private
  {
    FProcArr: array[0..100] of TOnShellEvent;

    FList: TList;

    pProc1: TOnShellEvent;

    FRec: TShellParamRec;

//    procedure ForEach______(aRec: TShellParamRec);

//    function Add____(const aItem: IInterface): Integer;
//    procedure PostDelNodeByList(aItemList: TIDList);
    function RegisterEvent(aProc: TOnShellEvent): Integer;
    procedure UnRegisterEvent(aProc: TOnShellEvent);
  }

  public
//    constructor Create;
//    destructor Destroy; override;

    procedure RENAME_ITEM_(aGUID,aNewName: string);
    procedure PostFocusNode(aGUID: string);

//    procedure Shell_PostDelNodesByIDList(aIDList: TIDList);
//    procedure Shell_PostFocusNode(aGUID: string);
//    procedure Shell_ReLoad_Project;
//    procedure Shell_RENAME_ITEM(aGUID,aNewName: string);
//    procedure Shell_UpdateNodeChildren_ByGUID(aGUID: string);

//    procedure Shell_UpdateNodeChildren_ByObjName(aObjName: string; aID: integer);

    procedure OBJECT_FOCUS_IN_PROJECT_TREE(aObjName: string; aID: integer);

    procedure FocuseNodeByObjName(aObjName: string; aID: integer);

    procedure DelNodeByID(aID: integer; aObjName: string);
    procedure PostDelNode(aGUID: string);

    procedure EXPAND_BY_GUID(aGUID: string);
    procedure PostDelNodeByList(aItemList: TIDList);

    procedure UpdateNodeChildren_ByGUID(aGUID: string);
    function FocuseNodeByGUID(aGUID: string): Boolean;



  end;

var
  g_Shell: TShellInterfaceList;

implementation

uses
    u_shell_var;


 {
constructor TShellInterfaceList.Create;
begin
  inherited;

  FList:=TList.Create;

end;


destructor TShellInterfaceList.Destroy;
begin

  Assert (Count=0);

  Assert (FList.Count=0);

  FreeAndNil(FList);

  inherited;
end;
 }

{
function TShellInterfaceList.Add____(const aItem: IInterface): Integer;
var
  h: HRESULT;
  v: IDBExplorerFormX;
begin
//  Count;
//
//  Result := Add(aItem);
//
//
//  h:=aItem.QueryInterface(IDBExplorerFormX, V);
//  Count;

 // IDBExplorerFormX

end;
 }

procedure TShellInterfaceList.EXPAND_BY_GUID(aGUID: string);
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    (Items[i] as IDBExplorerFormX).ExpandByGUID (aGUID);

end;

procedure TShellInterfaceList.FocuseNodeByObjName(aObjName: string; aID:
    integer);

var
  i: Integer;
begin
  for I := 0 to Count - 1 do
      (Items[i] as IDBExplorerFormX).FocuseNodeByObjName (aObjName, aID);

end;




{
// ---------------------------------------------------------------
procedure TShellInterfaceList.ForEach______(aRec: TShellParamRec);
// ---------------------------------------------------------------
var
  I: Integer;
  pProc: TOnShellEvent;
begin
//  for I := 0 to FList.Count - 1 do
//  begin
//    @pProc:=FList[i];
//    pProc(aRec);
//  end;
end;
}



// ---------------------------------------------------------------
procedure TShellInterfaceList.OBJECT_FOCUS_IN_PROJECT_TREE(aObjName: string;
    aID: integer);
// ---------------------------------------------------------------
var
  I: Integer;
  v: IDBExplorerFormX;
  arr: TStrArray;
  b: Boolean;
begin
  arr:=g_Objects1.ViewInProjectTree(aObjName, aID);


  for I := 0 to High(arr) do
  begin
    b:=IMainProjectExplorer.ExpandByGUID(arr[i]);
  end;

 //    IMainProjectExplorer.FocuseNodeByGUID (aGUID);

  b:=IMainProjectExplorer.FocuseNodeByObjName(aObjName, aID);
  b:=b;


(*
  for I := 0 to Count - 1 do
  begin
    v:=Items[i] as IDBExplorerFormX;


  end;
  //  (Items[i] as IDBExplorerFormX).FocuseNodeByObjName (aObjName,aID);
*)

(*
IMainProjectExplorer.ExpandByGUID(GUID_PROJECT);
    IMainProjectExplorer.ExpandByGUID(GUID_MSC);

    iFolderID:=dmMSC.GetFolderID(aID);

    if iFolderID=0 then
      sFolderGUID:=g_Obj.ItemByName[ObjectName].RootFolderGUID
    else
      sFolderGUID:=dmFolder.GetGUIDByID(iFolderID);


     Result := StrArray([GUID_PROJECT,
                         GUID_MSC,
                         sFolderGUID]);

  //  sGUID:=dmFolder.GetGUIDByID (dmMSC.GetFolderID(aID));

    IMainProjectExplorer.ExpandByGUID(sFolderGUID);
//    IMainProjectExplorer.FocuseNodeByGUID (aGUID);

    IMainProjectExplorer.FocuseNodeByObjName(ObjectName, aID);

*)
end;



// ---------------------------------------------------------------
procedure TShellInterfaceList.RENAME_ITEM_(aGUID,aNewName: string);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    (Items[i] as IDBExplorerFormX).RENAME_ITEM (aGUID,aNewName);

end;

procedure TShellInterfaceList.PostDelNode(aGUID: string);
var
  I: Integer;
begin
//DELETE_NODE_BY_GUID

  for I := 0 to Count - 1 do
   // if Items[i] is IDBExplorerFormX then

    (Items[i] as IDBExplorerFormX).DELETE_NODE_BY_GUID (aGUID);


 // FRec.GUID:=aGUID;
//  PostEvent (WE__EXPLORER_DELETE_NODE_BY_GUID);
end;


procedure TShellInterfaceList.DelNodeByID(aID: integer; aObjName: string);
var
  I: Integer;
begin
//DELETE_NODE_BY_GUID
{
  for I := 0 to Count - 1 do
   // if Items[i] is IDBExplorerFormX then

    (Items[i] as IDBExplorerFormX).DELETE_NODE_BY_ GUID (aGUID);

}



 // FRec.GUID:=aGUID;
//  PostEvent (WE__EXPLORER_DELETE_NODE_BY_GUID);
end;

function TShellInterfaceList.FocuseNodeByGUID(aGUID: string): Boolean;
var
  i: Integer;
begin
  for I := 0 to Count - 1 do
      (Items[i] as IDBExplorerFormX).FocuseNodeByGUID (aGUID);

end;




{
procedure TShellInterfaceList.Shell_UpdateNodeChildren_ByGUID(aGUID: string);
begin
  FRec.GUID:=aGUID;
  PostEvent (WE__EXPLORER_UPDATE_NODE_CHILDREN_BY_GUID);//, [app_Par(PAR_GUID, aGUID) ]);

//  if Assigned(IMainProjectExplorer) then
 //   IMainProjectExplorer.UPDATE_NODE_CHILDREN(aGUID);

//  PostEvent (WE_EXPLORER_UPDATE_NODE_CHILDREN, [app_Par(PAR_GUID, aGUID) ]);
end;
}


procedure TShellInterfaceList.PostDelNodeByList(aItemList: TIDList);
var
  i: Integer;
  k: Integer;
 //  v: IDBExplorerFormX;

begin
  for I := 0 to Count - 1 do
  begin
//    v:=(Items[i] as IDBExplorerFormX);

    for k := 0 to aItemList.Count - 1 do
      (Items[i] as IDBExplorerFormX).DELETE_NODE_BY_GUID (aItemList[k].GUID);

  end;

//    (Items[i] as IDBExplorerFormX).DELETE_NODE_BY_GUID (aGUID);



(*  for I:=0 to aItemList.Count-1 do
    PostDelNode (aItemList[i].GUID);
*)
end;

procedure TShellInterfaceList.PostFocusNode(aGUID: string);
var
  i: Integer;
begin
  Assert(aGUID<>'', 'GUID=''''');

  for I := 0 to Count - 1 do
      (Items[i] as IDBExplorerFormX).FocuseNodeByGUID (aGUID);

end;

// ---------------------------------------------------------------
procedure TShellInterfaceList.UpdateNodeChildren_ByGUID(aGUID: string);
// ---------------------------------------------------------------
var
  i: Integer;
begin
  for I := 0 to Count - 1 do
      (Items[i] as IDBExplorerFormX).UPDATE_NODE_CHILDREN_ByGUID (aGUID);

end;



initialization
  g_Shell:=TShellInterfaceList.Create;

finalization
  FreeAndNil(g_Shell);

end.

  {

  //---------------------------------------------------------
    WE_OBJECT_FOCUS_IN_PROJECT_TREE11:
    //---------------------------------------------------------
    begin
      iID     :=aParams.IntByName(PAR_ID);
      sObjName:=aParams.ValueByName(PAR_OBJECT_NAME);

      if IsObjectSupported(sObjName) then
      begin
        ViewInProjectTree (iID);

        aHandled:=True;
      end;
    end;


    

procedure TShellEventManager_.Shell_ReLoad_Project;
begin
  PostEvent (WE__EXPLORER_ReLoad_Project);
end;


procedure TShellEventManager_.Shell_PostDelNode(aGUID: string);
begin
  FRec.GUID:=aGUID;
  PostEvent (WE__EXPLORER_DELETE_NODE_BY_GUID);
end;


procedure TShellEventManager_.Shell_PostDelNodeByList(aItemList: TIDList);
var
  i: Integer;
begin
  for I:=0 to aItemList.Count-1 do
    Shell_PostDelNode (aItemList[i].GUID);
end;


procedure TShellEventManager_.Shell_PostDelNodesByIDList(aIDList: TIDList);
begin
  FRec.IDList:=aIDList;
  PostEvent(WE__EXPLORER_DELETE_NODES_BY_IDLIST, FRec);
end;


procedure TShellEventManager_.Shell_PostFocusNode(aGUID: string);
begin
  FRec.GUID:=aGUID;
  PostEvent (WE__EXPLORER_FOCUS_NODE_BY_GUID);
end;



procedure TShellEventManager_.Shell_UpdateNodeChildren_ByObjName(aObjName: string; aID: integer);
begin
  FRec.ID:=aID;
  FRec.ObjName:=aObjName;
  PostEvent (WE__EXPLORER_UPDATE_NODE_CHILDREN_BY_OBJNAME);//, [app_Par(PAR_GUID, aGUID) ]);
end;

procedure TShellEventManager_.Shell_UpdateNodeChildren_ByGUID(aGUID: string);
begin
  FRec.GUID:=aGUID;
  PostEvent (WE__EXPLORER_UPDATE_NODE_CHILDREN_BY_GUID);//, [app_Par(PAR_GUID, aGUID) ]);

//  if Assigned(IMainProjectExplorer) then
 //   IMainProjectExplorer.UPDATE_NODE_CHILDREN(aGUID);

//  PostEvent (WE_EXPLORER_UPDATE_NODE_CHILDREN, [app_Par(PAR_GUID, aGUID) ]);
end;


procedure TShellEventManager_.Shell_RENAME_ITEM(aGUID,aNewName: string);
begin
  FRec.GUID:=aGUID;
  FRec.Name:=aNewName;

  PostEvent (WE__EXPLORER_RENAME_ITEM);//, [app_Par(PAR_GUID, aGUID) ]);

 // if Assigned(IMainProjectExplorer) then
  //  IMainProjectExplorer.RENAME_ITEM(aGUID, aNewName);

//   PostEvent (WE_EXPLORER_RENAME_ITEM,
 //         [app_Par(PAR_GUID, aGUID),
  //         app_Par(PAR_NAME, aNewName) ]);
end;







procedure TShellInterfaceList.Shell_PostDelNodesByIDList(aIDList: TIDList);
begin
(*  FRec.IDList:=aIDList;
  PostEvent(WE__EXPLORER_DELETE_NODES_BY_IDLIST, FRec);
*)
end;

procedure TShellInterfaceList.Shell_PostFocusNode(aGUID: string);
begin
(*  FRec.GUID:=aGUID;
  PostEvent (WE__EXPLORER_FOCUS_NODE_BY_GUID);
*)
end;

procedure TShellInterfaceList.Shell_ReLoad_Project;
begin

end;

procedure TShellInterfaceList.Shell_RENAME_ITEM(aGUID,aNewName: string);
begin

end;

procedure TShellInterfaceList.Shell_UpdateNodeChildren_ByGUID(aGUID: string);
begin

end;


procedure TShellInterfaceList.Shell_UpdateNodeChildren_ByObjName(aObjName:
    string; aID: integer);
begin

end;



// ---------------------------------------------------------------
function TShellInterfaceList.RegisterEvent(aProc: TOnShellEvent): Integer;
// ---------------------------------------------------------------
var
  I: Integer;
  iIndex: Integer;
  p: pointer;

begin
//
//  p:=Addr(aProc);
//
//  iIndex:=FList.IndexOf(p);
//  Assert(iIndex<0, 'Value <=0');
//
//  Result := FList.Add(p);
//
end;

// ---------------------------------------------------------------
procedure TShellInterfaceList.UnRegisterEvent(aProc: TOnShellEvent);
// ---------------------------------------------------------------
var
  iIndex: Integer;
  p: pointer;
begin
//  p:=Addr(aProc);
//
//
//  iIndex:=FList.Remove(p)  ;
//
//
//  iIndex:=FList.IndexOf(p);
//  if iIndex>=0 then
//    FList.Delete(iIndex);
//

//  Result := ;
end;

