unit fr_Browser_Container;

interface

uses
  Windows, Messages, Controls, Classes,Forms,menus, QActnList, SysUtils,

  u_log,
  u_func,

  i_object_list,


  //dm_User,

  fr_Browser_List,
  u_Explorer,


  //dm_Explorer,

  u_const_msg,
  u_const_db,

  u_Types,

  u_db,
//    
  u_func_msg

  ;

type
 // TOnNodeFocusedEvent  = procedure (aPIDL: TrpPIDL) of object;
  //TOnExplorerItemEvent = procedure (const aPIDL: TrpPIDL) of object;


  Tframe_Browser_Container = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    Fframe_Browser_list: Tframe_Browser_List;

    FOnObjectDblClick: TOnObjectDblClick;
    FOnObjectClick: TOnObjectDblClick;
    FOnAskMenu: TOnAskMenuEvent;
    FOnLog: TOnLogEvent;

//    FProcIndex: integer;

  //  procedure GetMessage(aMsg: integer; aParams: TEventParamList; var aHandled: Boolean);

    procedure DoOnAskMenu(aSelectedPIDLs: TrpPIDL_List; aPopupMenu: TPopupMenu);
//    procedure DoOnLog (aMsg: string);

    procedure DoOnObjectDblClick (aObjectName: string; aID: integer; aGUID,aName: string);
    procedure DoOnObjectClick (aObjectName: string; aID: integer; aGUID,aName: string);

  public
    procedure View (aFolderID: integer; aGUID: string);

    procedure SetObjectName (aObjectName: string);

    property OnObjectClick: TOnObjectDblClick read FOnObjectClick write FOnObjectClick;
    property OnObjectDblClick: TOnObjectDblClick read FOnObjectDblClick write FOnObjectDblClick;

    property OnAskMenu: TOnAskMenuEvent read FOnAskMenu write FOnAskMenu;
    property OnLog: TOnLogEvent read FOnLog write FOnLog;

  //  property RegPath: string write SetRegPath;


  end;


implementation  {$R *.DFM}

//--------------------------------------------------------------------
procedure Tframe_Browser_Container.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  CreateChildForm(Tframe_Browser_List, Fframe_Browser_list, Self);

 // Fframe_Browser_list.OnLog:=DoOnLog;
  Fframe_Browser_list.OnAskMenu:=DoOnAskMenu;

  Fframe_Browser_list.OnObjectClick:=DoOnObjectClick;
  Fframe_Browser_list.OnObjectDblClick:=DoOnObjectDblClick;

 // FProcIndex:=g_EventManager.RegisterProc(GetMessage, Name);

end;


//--------------------------------------------------------------------
procedure Tframe_Browser_Container.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
 // g_EventManager.UnRegisterProc(FProcIndex,GetMessage);

//  Fframe_Browser_list.Free;
  FreeAndNil(Fframe_Browser_list);

  inherited;
end;



procedure Tframe_Browser_Container.View(aFolderID: integer; aGUID: string);
begin
  Fframe_Browser_list.View (aFolderID, aGUID);
end;


// -------------------------------------------------------------------
procedure Tframe_Browser_Container.DoOnAskMenu (aSelectedPIDLs: TrpPIDL_List;
                                                aPopupMenu: TPopupMenu);
// -------------------------------------------------------------------
//var
//  j: Integer;
//  i: Integer;
//  oAlwaysEnabledActionList: TStringList;
//  bObjectCanBeModified, bMenuEnabled: boolean;
//  oIntArr: TIntArray;

begin
//  g_Object
  g_Objects1.GetPopupMenu_list(aSelectedPIDLs, aPopupMenu);

  g_EventManager.postEvent_ (WE_OBJECT_GET_POPUP_MENU,
            ['PIDL_LIST',  Integer(aSelectedPIDLs),
             'Popup_Menu', Integer(aPopupMenu)
             ]);


Exit;

//
//  //------------------------------------------------
//  // !!USERS!!
//
//  oAlwaysEnabledActionList:= TStringList.Create;
//
//  if aSelectedPIDLs.Count=0 then
//    exit;
//
//  PostEvent (WE_OBJECT_GET_ALWAYS_ENABLED_ACTIONS,
//            [app_Par(PAR_OBJECT_NAME,  aSelectedPIDLs[0].ObjectName),
//             app_Par(PAR_NAME,         oAlwaysEnabledActionList)  ]);
//
//
//  if aPopupMenu.Items.Count>0 then
//  begin
//    bObjectCanBeModified := True;
//
//   (* if aSelectedPIDLs.Count=1 then
//      bObjectCanBeModified:=
//        dmUser.ObjectCanBeModified (aSelectedPIDLs[0].ObjectName, aSelectedPIDLs[0].ID)
//
//    else begin
//
//      SetLength(oIntArr, 0);
//     // IntArrayClear(oIntArr);
//      for i:= 0 to aSelectedPIDLs.Count-1 do
//        IntArrayAddValue(oIntArr, aSelectedPIDLs[i].ID);
//
//      bObjectCanBeModified:=
//        dmUser.ObjectsCanBeModified (aSelectedPIDLs[0].ObjectName, oIntArr);
//
//    end;
//*)
//
//  end;
//
//
//  for i:= 0 to aPopupMenu.Items.Count-1 do
//    aPopupMenu.Items[i].Enabled:=  bObjectCanBeModified;
//
//  for j := 0 to oAlwaysEnabledActionList.Count-1 do
//  begin
//    for i:= 0 to aPopupMenu.Items.Count-1 do
//      if aPopupMenu.Items[i].Action is TAction then
//        if (aPopupMenu.Items[i].Action as TAction).Name = oAlwaysEnabledActionList[j] then
//          aPopupMenu.Items[i].Enabled:=  true;
//  end;
//
//  FreeAndNil(oAlwaysEnabledActionList);
//  //------------------------------------------------
//
end;



procedure Tframe_Browser_Container.SetObjectName(aObjectName: string);
begin
  Fframe_Browser_list.SetObjectName(aObjectName);
end;



procedure Tframe_Browser_Container.DoOnObjectClick(aObjectName: string; aID: integer; aGUID,aName:
    string);
begin
  if assigned ( FOnObjectClick ) then
    FOnObjectClick (aObjectName, aID, aGUID, aName);


end;

procedure Tframe_Browser_Container.DoOnObjectDblClick(aObjectName: string; aID: integer; aGUID,
    aName: string);
begin
  if assigned ( FOnObjectDblClick ) then
    FOnObjectDblClick (aObjectName, aID, aGUID, aName);

end;



end.

  (*
 {
//--------------------------------------------------------------------
procedure Tframe_Browser_Container.GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean);
//--------------------------------------------------------------------
var
  sGUID: string;
begin

 { case aMsg of
    //-------------------------------------------------------------------
    WE_EXPLORER_DELETE_NODE_BY_GUID: begin
    //-------------------------------------------------------------------
      sGUID:=aParams.ValueByName(PAR_GUID);

      Fframe_Browser_list.DELETE_NODE_BY_GUID(sGUID);
    end;
  end;}

end;




{procedure Tframe_Browser_Container.DoOnLog (aMsg: string);
begin
  //raise Exception.Create('PostEvent (WE_LOG, '''', aMsg);');
  g_Log.Add(aMsg);
end;
}
