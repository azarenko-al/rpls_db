object frm_Test1: Tfrm_Test1
  Left = 505
  Top = 226
  Width = 611
  Height = 399
  Caption = 'frm_Test1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 469
    Top = 0
    Width = 134
    Height = 293
    Align = alRight
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 0
    object pn_Browser: TPanel
      Left = 5
      Top = 5
      Width = 124
      Height = 283
      Align = alClient
      Caption = 'pn_Browser'
      TabOrder = 0
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 430
    Height = 293
    ActivePage = ts_Project
    Align = alLeft
    TabOrder = 1
    object ts_Project: TTabSheet
      Caption = 'ts_Project'
    end
    object ts_Dict: TTabSheet
      Caption = 'ts_Dict'
      ImageIndex = 1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 293
    Width = 603
    Height = 72
    Align = alBottom
    Caption = 'Panel2'
    TabOrder = 2
    object Button1: TButton
      Left = 38
      Top = 21
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 185
      Top = 22
      Width = 75
      Height = 25
      Caption = 'Button2'
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 272
    Top = 232
  end
end
