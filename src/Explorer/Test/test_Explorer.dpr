program test_Explorer;



uses
  a_Unit7 in 'a_Unit7.pas' {frm_Test1},
  d_expl_Object_Edit in '..\d_expl_Object_Edit.pas' {dlg_Object_Edit},
  d_expl_Object_Move in '..\d_expl_Object_Move.pas' {dlg_expl_Object_Move},
  d_expl_Object_Move_enhanced in '..\d_expl_Object_Move_enhanced.pas' {dlg_expl_Object_Move_Enhanced},
  d_expl_Object_Move_on_Filter in '..\d_expl_Object_Move_on_Filter.pas' {dlg_expl_Object_Move_on_Filter1},
  d_expl_Object_Select in '..\d_expl_Object_Select.pas' {dlg_expl_Object_Select},
  d_expl_Object_Select_ByIDList in '..\d_expl_Object_Select_BYIDList.pas' {Dlg_expl_Object_Select_ByIDList1},
  d_Explorer_Item_Rename in '..\d_Explorer_Item_Rename.pas' {dlg_Explorer_Item_Rename},
  d_Shell_Delete in '..\d_Shell_Delete.pas' {dlg_Shell_Delete},
  dm_act_Explorer in '..\dm_act_Explorer.pas' {dmAct_Explorer: TDataModule},
  dm_Explorer in '..\dm_Explorer.pas' {dmExplorer: TDataModule},
  dm_ViewEngine in '..\dm_ViewEngine.pas' {dmViewEngine: TDataModule},
  f_Browser in '..\f_Browser.pas' {frm_Browser},
  f_Explorer in '..\f_Explorer.pas' {frm_Explorer},
  Forms,
  fr_Browser in '..\fr_Browser.pas' {frame_Browser},
  fr_Browser_Container in '..\fr_Browser_Container.pas' {frame_Browser_Container},
  fr_Browser_List in '..\fr_Browser_List.pas' {frame_Browser_List},
  fr_Browser_simple_view in '..\fr_Browser_simple_view.pas' {frame_Browser_Simple_View},
  fr_DBInspector in '..\fr_DBInspector.pas' {frame_DBInspector_},
  fr_DBInspector_Container in '..\fr_DBInspector_Container.pas' {frame_DBInspector_Container},
  fr_expl_Object_CheckTree in '..\fr_expl_Object_CheckTree.pas' {frame_expl_Object_CheckTree},
  fr_expl_Object_List in '..\fr_expl_Object_List.pas' {frame_expl_Object_list},
  fr_Explorer in '..\fr_Explorer.pas' {frame_Explorer},
  fr_Explorer_Container in '..\fr_Explorer_Container.pas' {frame_Explorer_Container},
  I_DBExplorerForm in '..\I_DBExplorerForm.pas',
  I_Shell in '..\I_Shell.pas',
  u_explorer in '..\u_Explorer.pas',
  u_Inspector_common in '..\u_Inspector_common.pas',
  u_types in '..\..\u_types.pas',
  x_Explorer in '..\x_Explorer.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(Tfrm_Test1, frm_Test1);
  Application.Run;
end.
