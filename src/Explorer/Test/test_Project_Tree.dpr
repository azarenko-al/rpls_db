program test_Project_Tree;

uses
  GSDock,
  Forms,
  dm_Main_res in '..\..\DataModules\dm_Main_res.pas' {dmMain_res: TDataModule},
  dm_Main_ in '..\..\COM_main\src\dm_Main_.pas' {dmMain_: TDataModule},
  f_Main_MDI in '..\..\Forms\f_Main_MDI.pas' {frm_Main_MDI},
  f_Log in '..\..\Forms\f_Log.pas' {frm_Log},
  f_Explorer in '..\f_Explorer.pas' {frm_Explorer},
  f_Browser_MDI_child in '..\f_Browser_MDI_child.pas' {frm_Browser_MDI_child},
  dm_Main_Init in '..\..\DataModules\dm_Main_Init.pas' {dmMain_Init: TDataModule},
  dm_Act_CalcModel in '..\..\CalcModel\dm_act_CalcModel.pas' {dmAct_CalcModel: TDataModule},
  fr_ListObjects in '..\fr_ListObjects.pas' {frame_ListObjects},
  fr_Browser_List in '..\fr_Browser_List.pas' {frame_Browser_List},
  dm_act_Explorer in '..\dm_act_Explorer.pas' {dmAct_Explorer: TDataModule},
  d_Wizard in '..\..\.common\d_Wizard.pas' {dlg_Wizard_base},
  d_Custom_Add in '..\..\..\..\Common\d_custom_add.pas' {dlg_Custom_Add},
  fr_Simple_view in '..\fr_Simple_view.pas' {frame_Simple_View},
  dm_act_Search in '..\..\Search\dm_act_Search.pas' {dmAct_Search: TDataModule},
  dm_Act_View_manager in '..\dm_Act_View_manager.pas' {dmAct_View_manager: TDataModule},
  dm_Explorer in '..\dm_Explorer.pas' {dmExplorer: TDataModule},
  fr_Explorer in '..\fr_Explorer.pas' {frame_Explorer},
  fr_Browser in '..\fr_Browser.pas' {frame_Browser},
  d_Search in '..\..\Search\d_Search.pas' {dlg_Search},
  fr_DBInspector in '..\fr_DBInspector.pas' {frame_DBInspector},
  dm_View_manager in '..\dm_View_manager.pas' {dmViewManager: TDataModule};

{$R *.RES}

const
  REG_KEY = '\Software\Onega\RPLS';

begin
  Application.Initialize;
  InitGSDockManager;

  Application.CreateForm(TdmMain_, dmMain_);
  Application.CreateForm(TdmMain_res, dmMain_res);
  Application.CreateForm(TdmMain_Init, dmMain_Init);
  Application.CreateForm(Tfrm_Main_MDI, frm_Main_MDI);
  Application.CreateForm(Tfrm_Log, frm_Log);
  Application.CreateForm(Tfrm_Explorer, frm_Explorer);
  Application.CreateForm(Tfrm_Browser_MDI_child, frm_Browser_MDI_child);
  LoadForms(REG_KEY);
  Application.Run;
  SaveForms(REG_KEY);

end.
