program test_Tree;

uses
  Forms,
  frm_test_Explorer_ in 'frm_test_Explorer_.pas' {frm_Explorer_test},
  dm_Main_res in '..\..\DataModules\dm_Main_res.pas' {dmMain_res: TDataModule},
  dm_Main in '..\..\DataModules\dm_Main.pas' {dmMain: TDataModule},
  dm_act_Explorer in '..\dm_act_Explorer.pas' {dmAct_Explorer: TDataModule},
  fr_Explorer in '..\fr_Explorer.pas' {frame_Explorer},
  dm_Explorer in '..\dm_Explorer.pas' {dmExplorer: TDataModule},
  d_expl_Folder in '..\d_expl_Folder.pas' {dlg_expl_Folder},
  dm_act_TrxType in '..\..\Dicts\dm_act_TrxType.pas' {dmAct_TrxType: TDataModule},
  dm_act_Terminal in '..\..\Dicts\dm_act_Terminal.pas' {dmAct_TerminalType: TDataModule},
  dm_Dicts in '..\..\Dicts\dm_Dicts.pas' {dmDicts: TDataModule};

{$R *.RES}

{$R ..\..\images.res}

begin
  Application.Initialize;
  Application.CreateForm(TdmMain_res, dmMain_res);
  Application.CreateForm(TdmMain, dmMain);
  Application.CreateForm(Tfrm_Explorer_test, frm_Explorer_test);
  Application.Run;
end.
