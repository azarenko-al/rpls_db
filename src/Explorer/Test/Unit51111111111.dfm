object Form5: TForm5
  Left = 1306
  Top = 253
  Width = 1142
  Height = 656
  Caption = 'Form5'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object cxDBVerticalGrid1: TcxDBVerticalGrid
    Left = 120
    Top = 160
    Width = 697
    Height = 345
    OptionsView.RowHeaderWidth = 198
    TabOrder = 0
    DataController.DataSource = DataSource1
    object cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.Alignment.Vert = taVCenter
      Properties.DataBinding.FieldName = 'gradient'
    end
    object cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'refraction'
    end
    object cxDBVerticalGrid1DBEditorRow3: TcxDBEditorRow
    end
  end
  object DataSource1: TDataSource
    DataSet = ADOTable1
    Left = 208
    Top = 48
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=SQL_ONEGA'
    LoginPrompt = False
    Left = 312
    Top = 40
  end
  object ADOTable1: TADOTable
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 'Link'
    Left = 208
    Top = 104
  end
end
