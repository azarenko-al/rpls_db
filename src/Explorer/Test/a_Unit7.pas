unit a_Unit7;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,


  I_Options,
  
  I_Shell,

  dm_Act_Map_Engine,

  I_DBExplorerForm,

  dm_Act_Explorer,

  fr_Browser_List,

 // dm_Main,
  dm_Main,

  u_explorer,
  //

  fr_DBInspector_Container,
  fr_Explorer_Container,

  u_types,

  u_log,
  u_func,
  u_reg,
  u_db,

  u_const,

  //dm_Explorer,

  dm_Act_Project,

  StdCtrls, rxPlacemnt, ExtCtrls, ComCtrls, ToolWin
  ;

type
  Tfrm_Test1 = class(TForm)
    FormStorage1: TFormStorage;
    Panel1: TPanel;
    pn_Browser: TPanel;
    PageControl1: TPageControl;
    ts_Project: TTabSheet;
    ts_Dict: TTabSheet;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    Fframe_Explorer : Tframe_Explorer_Container;
    Fframe_Explorer2 : Tframe_Explorer_Container;

    Fframe_Browser: Tframe_Browser_List;


    procedure DoOnDblClick (aPIDL: TrpPIDL);

  //////////////  IdmMainTemp: IMainX;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_Test1: Tfrm_Test1;

  frame_DBInspector_Container: Tframe_DBInspector_Container;

 // id: integer;


implementation



 {$R *.DFM}


procedure Tfrm_Test1.Button1Click(Sender: TObject);
begin
  g_ShellEvents.ShowCount;
end;

procedure Tfrm_Test1.Button2Click(Sender: TObject);
begin
Fframe_Explorer.Free;
end;

procedure Tfrm_Test1.DoOnDblClick(aPIDL: TrpPIDL);
begin
   ShowMessage('');
end;



procedure Tfrm_Test1.FormCreate(Sender: TObject);
var
  i: Integer;

begin
// g_Log.Start;
  g_Log.Add('start ---------------');



 // gl_Reg:=TRegStore_Create(REGISTRY_ROOT);

  TdmMain.Init;
  dmMain.OpenDlg;

  TdmAct_Explorer.Init;

  IOptions_Init;


  TdmAct_Project.init;
//  IShellFactory_Init (dmMain);

  dmAct_Map_Engine.DebugMode:=True;
                          


  CreateChildForm (Tframe_DBInspector_Container, frame_DBInspector_Container, pn_Browser);
  frame_DBInspector_Container.PrepareView('link');

//  frame_DBInspector_Container:=Tframe_DBInspector_Container.CreateChildForm (
  //                                   pn_Browser, 'link');
 // frame_DBInspector_Container.View(40787);



  CreateChildForm (Tframe_Explorer_Container, Fframe_Explorer, ts_Project);
//  Fframe_Explorer := Tframe_Explorer_Container.CreateChildForm ( ts_Project);
//  Fframe_Explorer.SetViewObjects([otAntennaType]);
  Fframe_Explorer.Style :=esProject;
  Fframe_Explorer.SetViewObjects (PROJECT_TYPES);
  Fframe_Explorer.RegPath:='test';

  Fframe_Explorer.Load;


  CreateChildForm(Tframe_Explorer_Container, Fframe_Explorer2, ts_Dict);

//  Fframe_Explorer2 := Tframe_Explorer_Container.CreateChildForm ( ts_Dict);
//  Fframe_Explorer.SetViewObjects([otAntennaType]);
  Fframe_Explorer2.Style :=esDict;
  Fframe_Explorer2.SetViewObjects (DICT_TYPES);
  Fframe_Explorer2.RegPath:='test2';


  Fframe_Explorer2.Load;


  dmAct_Project.LoadLastProject;


 //zzzzzzzzzz frame_DBInspector_Container.Load;

{
  CreateChildForm(Tframe_Browser_List, Fframe_Browser, pn_Browser);

//  Fframe_Browser.SetObjectName(OBJ_PROPERTY);
  Fframe_Browser.SetObjectName(OBJ_LINKEND);
}
 // Fframe_Explorer.OnNodeFocused:=Fframe_Browser.ViewObjectByPIDL;



//  dmMain.ProjectID:=gl_Reg.RegIni.ReadInteger ('TdmAct_Project', FLD_PROJECT_ID, 0);

//  dmAct_Project.LoadLastProject;

////  Fframe_Browser.View(0,'');


///  Fframe_Explorer.Load;
 /////

//  frame_DBInspector_Container.Free;


{
  frame_DBInspector_Container:=Tframe_DBInspector_Container.CreateChildForm (
                                    Self, pn_Browser, 'linkend');
  frame_DBInspector_Container.Free;
}

// dmAct_Map_Engine.Enabled:=False;

{
  Fframe_Explorer := Tframe_Explorer_Container.CreateChildForm ( TabSheet1);
  Fframe_Explorer.SetViewObjects([otProperty, otLink, otLinkEnd]);
  Fframe_Explorer.RegPath:='Property';
  Fframe_Explorer.Load;
}
 // Fframe_Browser := Tframe_Browser.CreateChildForm( pn_Browser);
 { Fframe_Explorer.OnNodeFocused:=Fframe_Browser.ViewObjectByPIDL;

  dmAct_Project.LoadLastProject;
}


end;


procedure Tfrm_Test1.FormDestroy(Sender: TObject);
begin
  // frame_DBInspector_Container.Free;

  Fframe_Explorer.Free;
  Fframe_Explorer2.Free;

   g_Log.Add('procedure Tfrm_Test1.FormDestroy(Sender: TObject);');


//    dmMain_Free;



 // Fframe_Explorer.Free;
  Fframe_Browser.Free;

end;


//
//initialization
//finalization
//   //g_Log.Show;


end.

