object frm_Explorer_test: Tfrm_Explorer_test
  Left = 238
  Top = 64
  Width = 307
  Height = 416
  Caption = 'frm_Explorer_test'
  Color = clBtnFace
  DragKind = dkDock
  DragMode = dmAutomatic
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 101
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 299
    Height = 57
    Caption = 'ToolBar1'
    Color = clGrayText
    EdgeBorders = []
    ParentColor = False
    TabOrder = 0
    Visible = False
  end
  object GSPages1: TGSPages
    Left = 0
    Top = 86
    Width = 177
    Height = 302
    Align = alLeft
    ActivePage = ts_Project
    Orientation = toBottom
    object ts_Project: TGSPage
      Left = 0
      Top = 0
      Width = 177
      Height = 279
      HorzScrollBar.Smooth = True
      HorzScrollBar.Tracking = True
      VertScrollBar.Smooth = True
      VertScrollBar.Tracking = True
      Align = alClient
      TabOrder = 0
      Caption = 'Проект'
    end
    object tsDicts: TGSPage
      Left = 0
      Top = 0
      Width = 177
      Height = 279
      HorzScrollBar.Smooth = True
      HorzScrollBar.Tracking = True
      VertScrollBar.Smooth = True
      VertScrollBar.Tracking = True
      Align = alClient
      TabOrder = 1
      Visible = False
      Caption = 'Справочники'
    end
  end
  object ToolBar2: TToolBar
    Left = 0
    Top = 57
    Width = 299
    Height = 29
    Caption = 'ToolBar2'
    TabOrder = 2
    object Button1: TButton
      Left = 0
      Top = 2
      Width = 75
      Height = 22
      Caption = 'Button1'
      TabOrder = 0
      OnClick = Button1Click
    end
  end
  object FormStorage1: TFormStorage
    StoredValues = <>
    Left = 40
    Top = 4
  end
end
