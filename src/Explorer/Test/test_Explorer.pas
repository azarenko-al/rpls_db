unit test_Explorer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics,
  Dialogs, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  rxPlacemnt, Menus, ActnList, Controls, Forms,  

  fr_Explorer,
  dm_Explorer,
  u_Explorer,

  dm_Main,

  u_reg,
  u_dlg,
  u_func_app,

  u_const,
  u_const_msg,
  u_types,

  d_expl_Folder
  ;

type
  Tfrm_Explorer_test = class(TForm)
    FormStorage1: TFormStorage;
    ToolBar1: TToolBar;
    GSPages1: TGSPages;
    ts_Project: TGSPage;
    tsDicts: TGSPage;
    ToolBar2: TToolBar;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
  private
    FProcIndex: integer;

    Fframe_Explorer: Tframe_Explorer;
    Fframe_Dicts: Tframe_Explorer;

    procedure DoOnNodeFocused (Sender: TObject; aPIDL: TrpPIDL);

    procedure ApplicationEvents1ActionExecute
                 (aMsg: integer; aParams: TEventParamList; var Handled: Boolean);

  public
    { Public declarations }
  end;

var
  frm_Explorer_test: Tfrm_Explorer_test;

//==================================================================
implementation {$R *.dfm}
//==================================================================

//--------------------------------------------------------------------
procedure Tfrm_Explorer_test.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  dmMain.Test;
         
  GSPages1.Align:=alCLient;

  Caption:='Структура';

//  ts_Project.Caption:='Проект';
//  tsDicts.Caption:='Справочники';

  Fframe_Explorer:=Tframe_Explorer.Create(Self);
  CopyControls(Fframe_Explorer, ts_Project);

  Fframe_Explorer.Debug (True);

  Fframe_Explorer.OnNodeFocused:=DoOnNodeFocused;
  Fframe_Explorer.Style:=vstProject;
  Fframe_Explorer.SetViewObjects (PROJECT_TYPES);
  Fframe_Explorer.Load();


  Fframe_Dicts:=Tframe_Explorer.Create(Self);
  CopyControls(Fframe_Dicts, tsDicts);
  Fframe_Dicts.OnNodeFocused:=DoOnNodeFocused;
  Fframe_Dicts.Style:=vstDict;
  Fframe_Dicts.SetViewObjects (DICT_TYPES);
  Fframe_Dicts.Load();


  with gl_Reg do begin
    BeginGroup(Self, Name);
    AddControl (GSPages1, [PROP_ACTIVE_PAGE_INDEX]);
  end;

  FProcIndex:=RegisterProc (ApplicationEvents1ActionExecute);

end;


procedure Tfrm_Explorer_test.FormDestroy(Sender: TObject);
begin
  gl_Reg.SaveAndClearGroup(Self, Name);

  Fframe_Explorer.Free;
//  Fframe_Dicts.Free;

  UnRegisterProc (FProcIndex);

//  frm_Explorer:=nil;
end;


procedure Tfrm_Explorer_test.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;

//--------------------------------------------------------------------
procedure Tfrm_Explorer_test.DoOnNodeFocused(Sender: TObject; aPIDL: TrpPIDL);
//--------------------------------------------------------------------
begin
//  ViewObjectByPIDL (aPIDL);
  PostMsg (WM_EXPLORER_OPEN_PIDL, Integer(aPIDL), 0);
  //  DoOnLog ('----' + aPIDL.ObjectName);

end;


//-------------------------------------------------------------------
procedure Tfrm_Explorer_test.ApplicationEvents1ActionExecute
  (aMsg: integer; aParams: TEventParamList; var Handled: Boolean);
//-------------------------------------------------------------------
//var //oListID: TIDList;
   // objType: TrpObjectType;
    //iID: integer;
    //sObjName,sName: string;
begin
  case aMsg of

    WE_PROJECT_CHANGED:
    begin
      Fframe_Explorer.Load();
      {
      sObjName:=aParams.ValueByName (PAR_OBJECT_NAME);
      objType :=g_Obj.GetObjectTypeByName(sObjName);
      oListID :=TIDList(aParams.PointerByName('ListID'));

      if Tdlg_expl_MoveObjects.SelectItems (objType, oListID)
        then PostEvent (WE_MODAL_RESULT_OK);
      }
    end;

  end;

end;




procedure Tfrm_Explorer_test.Button1Click(Sender: TObject);
begin
  Tdlg_expl_Folder.ExecDlg (OBJ_PROJECT);

end;


end.