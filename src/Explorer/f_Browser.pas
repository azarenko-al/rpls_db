unit f_Browser;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,  rxPlacemnt, 

  u_func,
  u_Explorer,

 // u_const_msg,

//  u_func_msg,

  fr_Browser,
  I_Shell;


type
  Tfrm_Browser = class(TForm, IShellNotifyX)
    FormPlacement1: TFormPlacement;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    Fframe_Browser: Tframe_Browser;
  //  FProcIndex: integer;

    procedure DoViewObjectByID (aObjectName: string; aID: integer);
  //  procedure GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean);
    procedure GetShellMessage(aMsg: integer; aRec: TShellEventRec);

  public

    class function CreateForm: Tfrm_Browser;
    class function CreateSingleForm: Tfrm_Browser;

//    class procedure ViewObjectByID_Dlg (aObjectName: string; aID: integer);
    class procedure ViewObjectByID (aObjectName: string; aID: integer);
    class procedure ViewObjectByPIDL (aPIDL: TrpPIDL);


   // constructor Create(AOwner: TComponent);
  end;


//==================================================================
implementation {$R *.DFM}
//==================================================================

//var
  // ONLY 1 FORM !!!
//  frm_Browser: Tfrm_Browser;


//-------------------------------------------------------
class function Tfrm_Browser.CreateForm: Tfrm_Browser;
//-------------------------------------------------------
begin
  Result:=Tfrm_Browser.Create(Application);

  Result.Show;
end;


class procedure Tfrm_Browser.ViewObjectByID (aObjectName: string; aID: integer);
var
  obj: Tfrm_Browser;
begin
  obj:=Tfrm_Browser.CreateSingleForm();
  obj.Fframe_Browser.ViewObjectByID (aObjectName, aID);

end;


class procedure Tfrm_Browser.ViewObjectByPIDL (aPIDL: TrpPIDL);
var
  obj: Tfrm_Browser;
begin
  obj:=Tfrm_Browser.CreateSingleForm();
  obj.Fframe_Browser.ViewObjectByPIDL (aPIDL);
end;


//-------------------------------------------------------
class function Tfrm_Browser.CreateSingleForm: Tfrm_Browser;
//-------------------------------------------------------
var
  oForm: TForm;

begin
  oForm:=IsFormExists (Application, Tfrm_Browser.ClassName);

  if not Assigned(oForm) then
    oForm:=Tfrm_Browser.CreateForm();

  oForm.Show;

  if oForm.WindowState = wsMinimized then
    oForm.WindowState := wsMaximized;



//  if not Assigned(frm_Browser) then
  //  frm_Browser:=Tfrm_Browser.CreateForm();

  //frm_Browser.Show;

  Result:=Tfrm_Browser(oForm);
end;




// -------------------------------------------------------------------
procedure Tfrm_Browser.FormCreate(Sender: TObject);
// -------------------------------------------------------------------
begin
  inherited;

  Caption:='������';

  CreateChildForm(Tframe_Browser, Fframe_Browser, Self);

  FormPlacement1.Active:=true;
  FormPlacement1.RestoreFormPlacement;

  g_ShellEvents.RegisterObject (Self);

 // FProcIndex:=g_EventManager.RegisterProc(GetMessage, Name);
end;


procedure Tfrm_Browser.FormDestroy(Sender: TObject);
begin
/// g_EventManager.UnRegisterProc(FProcIndex, GetMessage);
  g_ShellEvents.UnRegisterObject (Self);

  inherited;
end;


procedure Tfrm_Browser.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;
          
//--------------------------------------------------------------
procedure Tfrm_Browser.GetShellMessage(aMsg: integer; aRec: TShellEventRec);
//--------------------------------------------------------------
var oPIDL: TrpPIDL;
   i: Integer;
begin
 case aMsg of
    WE__EXPLORER_RELOAD_PROJECT:
      begin
        Assert(Assigned(Fframe_Browser), 'Value not assigned');

        Fframe_Browser.ViewObjectByPIDL (nil);
      end;

    WE___EXPLORER_OPEN_PIDL:

       if Active then
       begin
       //   i:=aParams.Count;
//                oPIDL:=TrpPIDL(aParams.PointerByName(PAR_PIDL));
            oPIDL:=TrpPIDL(aRec.PIDL);

        try
          Assert(Assigned(Fframe_Browser), 'Value not assigned');

           Fframe_Browser.ViewObjectByPIDL (oPIDL);

         except
         end;
       end;
  end;
end;



//--------------------------------------------------------------------
procedure Tfrm_Browser.DoViewObjectByID (aObjectName: string; aID: integer);
//--------------------------------------------------------------------
begin
  Fframe_Browser.ViewObjectByID (aObjectName, aID);
end;


end.




//
////-------------------------------------------------------
//constructor Tfrm_Browser.Create(AOwner: TComponent);
////-------------------------------------------------------
//begin
//  inherited;
//
//(*
//  if Assigned(Application.MainForm) then
//    if Application.MainForm.FormStyle=fsMDIForm then
//      FormStyle:=fsMDIChild;
//*)
//
//end;
