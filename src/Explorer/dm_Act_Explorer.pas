unit dm_act_Explorer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs,    Db, AdoDb,  cxVGrid,
  cxButtonEdit,

 // I_Act_Explorer,

  I_Shell,

  dm_Main_res,

  u_const,
  u_const_db,
  u_types,

  u_classes,

  u_cx_vgrid,
  u_db,

  f_Browser,

  fr_Browser,

  d_expl_Object_Select

//  x_Explorer

  ;


type
//  TOnShellNotifyEvent = procedure (aMsg: Integer) of object;


  TdmAct_Explorer = class(TDataModule) //, IAct_ExplorerX)
  //  procedure DataModuleDestroy(Sender: TObject);

  public
    procedure Browser_ViewObjectByID(aObjectName: WideString; aID: integer);

    // VertcalGrid
    function Dlg_SelectObject_cxButtonEdit (Sender: TcxButtonEdit; aType: TrpObjectType;
                    var aID: Integer): Boolean;

    function Dlg_EditObject(aObjectName: WideString; aID: integer): boolean;
    // dx button edit


    function Dlg_Select_Object(aType: TrpObjectType; var aID: integer; var aName:
        WideString): Boolean;


    class procedure Init;
  end;


var
  dmAct_Explorer: TdmAct_Explorer;


//====================================================================
// implementation
//====================================================================
implementation {$R *.DFM}



//--------------------------------------------------------------------
class procedure TdmAct_Explorer.Init;
//--------------------------------------------------------------------
begin
  Assert(not Assigned(dmAct_Explorer));

  Application.CreateForm(TdmAct_Explorer, dmAct_Explorer);

 // dmAct_Explorer.GetInterface(IAct_ExplorerX, IAct_Explorer);
 // Assert(Assigned(IAct_Explorer));

end;

//
//procedure TdmAct_Explorer.DataModuleDestroy(Sender: TObject);
//begin
// // IAct_Explorer := nil;
// // dmAct_Explorer :=nil;
//
//  inherited;
//end;


// VertcalGrid
//--------------------------------------------------------------
function TdmAct_Explorer.Dlg_SelectObject_cxButtonEdit (Sender: TcxButtonEdit; aType: TrpObjectType;
                                                        var aID: Integer): Boolean;
//--------------------------------------------------------------
var
  sName: Widestring;
  iID: integer;
begin
  Result :=Dlg_Select_Object(aType, aID, sName);
  if Result then
    cx_SetButtonEditText(Sender, sName);

end;


function TdmAct_Explorer.Dlg_EditObject(aObjectName: WideString; aID: integer):
    boolean;
var
  obj: Tframe_Browser;
begin
  obj:=Tframe_Browser.Create(Application);

  obj.ViewObjectByID (aObjectName, aID);
  obj.ShowMOdal;

  FreeAndNil(obj);


  { TODO : ! }
  Result:=True;
end;



function TdmAct_Explorer.Dlg_Select_Object(aType: TrpObjectType; var aID:
    integer; var aName: WideString): Boolean;
begin
  Result:=Tdlg_expl_Object_Select.ExecDlg1 (aType, aID, aName);
end;



procedure TdmAct_Explorer.Browser_ViewObjectByID(aObjectName: WideString; aID:
    integer);
begin
  Tfrm_Browser.ViewObjectByID (aObjectName,aID);
end;


end.


