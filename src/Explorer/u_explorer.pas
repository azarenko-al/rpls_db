unit u_explorer;

interface
uses Classes, Graphics, Menus,

     u_types;

type
  TrpPIDL = class;

  TOnObjectDblClick    = procedure (aObjectName: string; aID: integer; aGUID,aName: string) of object;
  TOnObjectFocused     = procedure (aObjectName: string; aID: integer; aGUID,aName: string) of object;
  TOnNodeFocusedEvent  = procedure (aPIDL: TrpPIDL) of object;



  //-------------------------------------------------------
  TrpPIDL = class
  //-------------------------------------------------------
    ID          : integer;
    Name        : string;
    ObjectName  : string;
    IsFolder    : boolean;
    GUID        : string;

    Checked : Boolean;

//    GeoRegion_ID : Integer;
//    GeoRegion1_ID : Integer;
//    GeoRegion2_ID : Integer;

    Owner        : string;


    Tree_ID1    : Integer;

    ref_ID      : integer;
    ref_ObjName : string;


    procedure Assign (aSource: TrpPIDL);
    function GetObjectType: TrpObjectType;

  end;


  //-------------------------------------------------------
  TrpPIDL_List = class(TList)
  //-------------------------------------------------------
  private
    function GetItem (aIndex: integer): TrpPIDL;
  public
    Sender : TObject;


    function AddItem: TrpPIDL;
    function AddID (aID: integer; aGUID: string): TrpPIDL;

    procedure Assign (aSource: TrpPIDL_List);

    procedure Clear; override;
    property Items [index: integer]: TrpPIDL read GetItem; default;
  end;

  TPIDL_List = TrpPIDL_List;


  TOnAskMenuEvent = procedure (aSelectedPIDLs: TrpPIDL_List; aPopupMenu: TPopupMenu) of object;



//====================================================================
implementation
//====================================================================

//--------------------------------------------------------------------
procedure TrpPIDL.Assign(aSource: TrpPIDL);
//--------------------------------------------------------------------
begin
  ID            :=aSource.ID;
  Name          :=aSource.Name;
  GUID          :=aSource.GUID;
  IsFolder      :=aSource.IsFolder;
  ObjectName    :=aSource.ObjectName;

//  GeoRegion_ID  :=aSource.GeoRegion_ID;
//  GeoRegion1_ID :=aSource.GeoRegion1_ID;
//  GeoRegion2_ID :=aSource.GeoRegion2_ID;

  Owner         :=aSource.Owner;

  ref_ID        :=aSource.ref_ID;
  ref_ObjName   :=aSource.ref_ObjName;



 // oPIDL.GUID      :=TrpPIDL(oNode.Data).GUID;
//    oPIDL.Name      :=TrpPIDL(oNode.Data).Name;
//    oPIDL.ID        :=TrpPIDL(oNode.Data).ID;
//    oPIDL.IsFolder  :=TrpPIDL(oNode.Data).IsFolder;
//    oPIDL.ObjectName:=TrpPIDL(oNode.Data).ObjectName;
//
//    oPIDL.GeoRegion_ID:=TrpPIDL(oNode.Data).GeoRegion_ID;
//    oPIDL.GeoRegion1_ID:=TrpPIDL(oNode.Data).GeoRegion1_ID;
//    oPIDL.GeoRegion2_ID:=TrpPIDL(oNode.Data).GeoRegion2_ID;
//
//    oPIDL.Owner       :=TrpPIDL(oNode.Data).Owner;





end;


function TrpPIDL.GetObjectType: TrpObjectType;
begin
  Result:=g_Obj.ItemByName[ObjectName].Type_;
end;


{ TrpPIDL_List }

function TrpPIDL_List.AddID(aID: integer; aGUID: string): TrpPIDL;
begin
  Result:=AddItem;
  Result.ID:=aID;
  Result.GUID:=aGUID;

end;


function TrpPIDL_List.AddItem: TrpPIDL;
begin
  Result:=TrpPIDL.Create;
  Add(Result);
end;


procedure TrpPIDL_List.Assign(aSource: TrpPIDL_List);
var i: integer;
  oPIDL: TrpPIDL;

begin
  Clear;

  for i:=0 to aSource.Count-1 do
  begin
    oPIDL:=AddItem();
    oPIDL.Assign (aSource.Items[i]);
  end;
end;



procedure TrpPIDL_List.Clear;
var i: integer;
begin
  for i:=0 to Count-1 do
     Items[i].Free;
  inherited;
end;


function TrpPIDL_List.GetItem(aIndex: integer): TrpPIDL;
begin
  Result:=TrpPIDL(inherited Items[aIndex]);
end;


end.
