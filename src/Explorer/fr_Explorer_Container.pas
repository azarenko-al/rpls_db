unit fr_Explorer_Container;

interface

uses
  Windows,Messages,Controls,Classes,Forms,menus,Graphics, SysUtils, Dialogs,

  u_cx_TreeList,

  
 // i_explorer,


  u_Shell_new,

  i_object_list,

//  I_Shell,
  I_DBExplorerForm,

  fr_Explorer,

  u_Explorer,

  u_const_msg,
  u_const_db,

  u_Types,

  u_db,
  u_log,
  u_func,

 //
  u_func_msg

  ;

type
  TdmExplorerOptionSet =  set of TdmExplorerOption;


  Tframe_Explorer_Container = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
//    FOnExplorerCustomDrawCellByGUID: TOnExplorerCustomDrawCellByGUID;

    FOnNodeFocused: TOnNodeFocusedEvent;
    FOnItemDblClick: TOnExplorerItemEvent;


    procedure DoOnNodeFocused(aPIDL: TrpPIDL);
    procedure DoOnItemDblClick (aPIDL: TrpPIDL);

    procedure DoOnAskMenu (aSelectedPIDLs: TrpPIDL_List; aPopupMenu: TPopupMenu);
// TODO: DoOnExplorerCustomDrawCellByGUID
//  procedure DoOnExplorerCustomDrawCellByGUID (Sender: TObject;
//                       aPIDL: TrpPIDL; var AColor: TColor; AFont: TFont);


    procedure SetOptions(aValueSet: TdmExplorerOptionSet);

//    procedure SetRegPath(aValue: string);
    procedure SetStyle(aValue: TdmExplorerStyle);
  public

    Fframe_Explorer: Tframe_Explorer;

    function ExpandByGUID(aGUID: string): Boolean;
    function SetStateFileName(aFileName: string): Integer;

//    function GetInterfaceX: IDBExplorerFormX;

    procedure Load ();

    procedure FocuseNodeByGUID(aGUID: string);
    function FocuseNodeByObjName(aObjectName: Widestring; aID: integer): boolean;


    procedure SetViewObjects (aValues: array of TrpObjectType);
    procedure SetViewObject (aValue: string);
    procedure UPDATE_NODE_CHILDREN(aGUID: string);


//    procedure DoOnShellEvent(aRec: TShellParamRec);



    property OnNodeFocused: TOnNodeFocusedEvent read FOnNodeFocused write FOnNodeFocused;
    property OnItemDblClick: TOnExplorerItemEvent read FOnItemDblClick write FOnItemDblClick;
//    property OnExplorerCustomDrawCellByGUID: TOnExplorerCustomDrawCellByGUID read FOnExplorerCustomDrawCellByGUID write FOnExplorerCustomDrawCellByGUID;

    property Options: TdmExplorerOptionSet write SetOptions;

//    property RegPath: string write SetRegPath;
    property Style: TdmExplorerStyle write SetStyle;

  end;

//  IDBExplorerFormX;


implementation
{$R *.DFM}


//--------------------------------------------------------------------
procedure Tframe_Explorer_Container.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  CreateChildForm(Tframe_Explorer, Fframe_Explorer, Self);

  Fframe_Explorer.OnNodeFocused:=DoOnNodeFocused;
  Fframe_Explorer.OnItemDblClick:=DoOnItemDblClick;
  Fframe_Explorer.OnAskMenu:=DoOnAskMenu;

  g_Shell.Add(Fframe_Explorer);

//  g_Shell.RegisterEvent(DoOnShellEvent);

//  g_Shell.test;
end;



//--------------------------------------------------------------------
procedure Tframe_Explorer_Container.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
//  g_EventManager.UnRegisterProc(FProcIndex,GetMessage);

 // g_ShellEvents.UnRegisterProc(GetShellProcEvent);

  g_Shell.Remove(Fframe_Explorer);

//  g_Shell.UnRegisterEvent(DoonshellEvent);



  Fframe_Explorer.OnAskMenu:=nil;
  Fframe_Explorer.OnNodeFocused:=nil;
  Fframe_Explorer.OnItemDblClick:=nil;

  FreeAndNil(Fframe_Explorer);

//  Fframe_Explorer:=nil;

  inherited;
end;

procedure Tframe_Explorer_Container.DoOnItemDblClick(aPIDL: TrpPIDL);
begin
  if assigned (FOnItemDblClick) then
    FOnItemDblClick(aPIDL);

//  Tfrm_Browser.ViewObjectByPIDL (aPIDL);

//  PostWMsg (WM_EXPLORER_OPEN_PIDL, Integer(aPIDL), 0);

end;



procedure Tframe_Explorer_Container.SetStyle(aValue: TdmExplorerStyle);
begin
  Fframe_Explorer.Style:=aValue;
 // Fframe_Explorer.SetStyle(aValue);
end;


procedure Tframe_Explorer_Container.SetViewObject(aValue: string);
begin
  Fframe_Explorer.SetViewObject(aValue);
end;


procedure Tframe_Explorer_Container.SetViewObjects (aValues: array of TrpObjectType);
begin
  Fframe_Explorer.SetViewObjects (g_Obj.MakeObjectTypeArr (aValues));

end;


// ---------------------------------------------------------------
procedure Tframe_Explorer_Container.Load;
// ---------------------------------------------------------------
begin
//  if Fframe_Explorer.Style<>esDict then
  Fframe_Explorer.Load;

   {
//  Fframe_Explorer.ExpandByGUID (GUID_3G_GROUP);
  Fframe_Explorer.ExpandByGUID (GUID_GSM_GROUP);
  Fframe_Explorer.ExpandByGUID (GUID_LINK_GROUP);
  Fframe_Explorer.ExpandByGUID (GUID_PMP_GROUP);

  }

//  Fframe_Explorer.ExpandByGUID (GUID_3G_DICT_GROUP);

end;


procedure Tframe_Explorer_Container.SetOptions(aValueSet: TdmExplorerOptionSet);
begin
  Fframe_Explorer.Options:=aValueSet;
end;



//--------------------------------------------------------------------
procedure Tframe_Explorer_Container.DoOnNodeFocused(aPIDL: TrpPIDL);
//--------------------------------------------------------------------
begin
  if assigned (FOnNodeFocused) then
    FOnNodeFocused(aPIDL);

///////  PostWMsg (WM_EXPLORER_OPEN_PIDL, Integer(aPIDL), 0);
end;



procedure Tframe_Explorer_Container.DoOnAskMenu (aSelectedPIDLs: TrpPIDL_List; aPopupMenu: TPopupMenu);
begin
  g_Objects1.GetPopupMenu_list(aSelectedPIDLs, aPopupMenu);

  g_EventManager.postEvent_ (WE_OBJECT_GET_POPUP_MENU,
       [
         'PIDL_LIST',  Integer(aSelectedPIDLs),
         'Popup_Menu', Integer(aPopupMenu)
       ]);

  {
  PostEvent (WE_OBJECT_GET_POPUP_MENU1,
            [app_Par(PAR_PIDL_LIST, aSelectedPIDLs),
             app_Par(PAR_Popup_Menu, aPopupMenu)
             ]);
  }
end;



procedure Tframe_Explorer_Container.FocuseNodeByGUID(aGUID: string);
begin
  Fframe_Explorer.FocuseNodeByGUID(aGUID);
end;


function Tframe_Explorer_Container.ExpandByGUID(aGUID: string): Boolean;
begin
  Result := Fframe_Explorer.ExpandByGUID(aGUID);
end;


function Tframe_Explorer_Container.FocuseNodeByObjName(aObjectName: Widestring;
    aID: integer): boolean;
begin
   Result := Fframe_Explorer.FocuseNodeByObjName(aObjectName,aID);
end;

function Tframe_Explorer_Container.SetStateFileName(aFileName: string): Integer;
begin
  Fframe_Explorer.StateFileName:=aFileName;
end;



procedure Tframe_Explorer_Container.UPDATE_NODE_CHILDREN(aGUID: string);
begin
  Fframe_Explorer.UPDATE_NODE_CHILDREN_ByGUID(aGUID);
end;



end.

