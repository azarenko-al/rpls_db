unit fr_Browser_List;

interface

uses

  u_dxBar,
  dm_Onega_DB_data,

  fr_View_base,

  cxEdit,
  SysUtils,

  Variants,

  i_Filter,

  u_Storage,

  u_ViewEngine_classes,

  I_Shell,

  dm_Main,

  u_Inspector_common,

  dm_ViewEngine,
  dm_Folder,

  u_func_msg,
  u_const_msg,

  u_reg,
  u_dlg,


  u_db,
  u_cx,
  u_func,

  u_classes,
  u_Explorer,

  u_types,
  u_const_DB,
  u_const,

  cxMaskEdit, cxButtonEdit, cxStyles, dxBar, cxBarEditItem, dxBarExtItems,
  cxLookAndFeels, cxClasses, DB, ADODB, ExtCtrls, Dialogs, StdCtrls,
  Controls, ComCtrls, ToolWin, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxControls, cxGridCustomView, cxGrid,
   cxPropertiesStore, Menus, ImgList, Classes, ActnList,
   rxPlacemnt, cxCheckBox, cxGraphics, cxLookAndFeelPainters,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData, dxSkinsCore,
  dxSkinsDefaultPainters;


type
  TOnAskMenuEvent = procedure (aSelectedPIDLs: TrpPIDL_List; aPopupMenu: TPopupMenu) of object;


  Tframe_Browser_List = class(Tframe_View_Base, IShellNotifyX)
    act_Filter_Open: TAction;
    act_SaveAsExcel: TAction;
    act_Filter_Clear: TAction;
    StatusBar1: TStatusBar;
    act_Column_customize: TAction;
    act_All_Folders: TAction;
    SaveDialog1: TSaveDialog;
    TimerOnDblClick: TTimer;
    ds_Data: TDataSource;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    col__ID: TcxGridDBColumn;
    col__guid: TcxGridDBColumn;
    col__recNo: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    ToolBar1: TToolBar;
    ed_Search: TEdit;
    Button1: TButton;
    act_Search: TAction;
    ToolButton1: TToolButton;
    FormStorage1: TFormStorage;
    dxBarManager1Bar3: TdxBar;
    cb_show_list: TcxBarEditItem;
    sp_Data: TADOStoredProc;

    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxDBGrid11DblClick(Sender: TObject);
    procedure cxDBGrid11KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure _Actions(Sender: TObject);
//    procedure ed_Filter111ButtonClick(Sender: TObject;
 //     AbsoluteIndex: Integer);
//    procedure mnu_ActionsPopup(Sender: TTBCustomItem; FromLink: Boolean);
    procedure FormDestroy(Sender: TObject);
     procedure cb_Filter_All_FoldersClick(Sender: TObject);
    procedure ClearSelection;
    procedure col__recNoGetDataText(Sender: TcxCustomGridTableItem; ARecordIndex: Integer; var AText:
        string);
    procedure cb_Filter_All_Folders_1Change(Sender: TObject);
    procedure cb_show_listChange(Sender: TObject);
    procedure cb_show_listClick(Sender: TObject);
 //   procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure TimerOnDblClickTimer(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure dxBarSubItem_MenuPopup(Sender: TObject);
    procedure ed_FilterPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
//    procedure cb_show_listChange(Sender: TObject);
//    procedure cxGrid1DBTableView1StylesGetContentStyle(
//      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
//      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
  private
    FFilter_SQL: String;

  private
    FDataset: TDataset;

    FObjectRegPath: string;

    FViewObject: TViewEngineObject;


   // FOnLog: TOnLogEvent;
    FOnAskMenu: TOnAskMenuEvent;


    FIsUpdated: boolean;
    FObjectType: TrpObjectType;

//    FFilterID:  Integer;
    FFolderID:  integer;

  //  FGUID: string;

    FOnObjectDblClick: TOnObjectDblClick;
    FOnObjectClick: TOnObjectDblClick;

//    FFilterWhere,
    FWhereSQL:   string;

    FSelectedPIDLs: TrpPIDL_List;
    FFieldNames: TStringList;

    FDblClick : record
                  ID: integer;
                  Name: string;
                  GUID: string;
                end;

    procedure AskActionsMenu ();
    procedure Exec_SaveAsExcel;
    procedure GetShellMessage(aMsg: integer; aRec: TShellEventRec);
//    procedure ClearColumns;
    procedure ReView();

    procedure LoadDBGridColumns ();
 //   procedure Show_folders_all111111111111111;
//    function GetRegistryPath (): string;

  protected

//    FSQL: string;
//    FRegPath: string;

  public

    procedure View(aFolderID: integer; aGUID: string); override;
    procedure SetObjectName (aObjectName: string); override;
//    procedure DELETE_NODE_BY_GUID (aGUID: string );

    procedure GetSelectedPIDLs (out aPIDL_List: TrpPIDL_List);

    property OnObjectClick: TOnObjectDblClick read FOnObjectClick write FOnObjectClick;
    property OnObjectDblClick: TOnObjectDblClick read FOnObjectDblClick write FOnObjectDblClick;

    property OnAskMenu: TOnAskMenuEvent read FOnAskMenu write FOnAskMenu;
//    property OnLog: TOnLogEvent read FOnLog write FOnLog;

   // property RegPath: string  read FRegPath write FOnAskMenu;

  end;


//==================================================================
//==================================================================
implementation

{$R *.dfm}


const
  FLD_TYPE_LIST = 'list';
  FILTER_ALL_RECORDS = '-��� ������-';
  SYS_FIELDS_COUNT = 2;


//-------------------------------------------------------------------
procedure Tframe_Browser_List.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;


(*  cxPageControl1.HideTabs:=True;//not g_Debug;
  cxPageControl1.ActivePage:=p_Main;
  cxPageControl1.Align:=alClient;
*)

  cxGrid1.Align:=alClient;

  bar_Init(dxBarManager1);

//////  db_SetComponentADOConnection(Self, dmMain.ADOConnection);


{  AddComponentProp(cb_Filter_All_Folders, 'checked');
  cxPropertiesStore.RestoreFrom;
}




 { mnu_Grid.Caption:='�������';
  mnu_SaveAs.Caption:='��������� ���...';
}


//  col_RecNo.Caption:='�';
  col__RecNo.Caption:='�';

  act_Column_customize.Caption:='��������� �������';
//  cb_Filter_All_Folders.Caption:='��� �����';

  // test---------
//  col_ID.Visible:=False;
 // col_GUID.Visible:=False;
  //-------------

  //ed_Filter1.Text:=FILTER_ALL_RECORDS;


  col__RecNo.Tag:=1;
  col__ID.Tag:=1;
  col__guid.Tag:=1;


{  col_GeoRegion.Tag:=1;
  col_GeoRegion1.Tag:=1;
  col_GeoRegion2.Tag:=1;

}

  FSelectedPIDLs:=TrpPIDL_List.Create;
  FFieldNames:=TStringList.Create;


{  //!!!!!!!!!!!!!!!!!!!!!
  mem_Fields:=dmViewEngine.CreateDB_ObjectFields();
  ds_Fields.Dataset:=mem_Fields;

}

 //////////////// dmViewEngine.InitDB_ObjectFields (mem_Fields);


//  FProcIndex:=RegisterProc (ApplicationEvents, Name);

//  FRegPath:= REGISTRY_FORMS + ClassName +'\';

  FIsUpdated:=True;
//  AddComponentProp (cb_Filter_All_Folders,   PROP_CHECKED);
  AddComponentProp (cb_Filter_All_Folders_1, 'EditValue');
  AddComponentProp (cb_show_list, 'EditValue');

  cxPropertiesStore.RestoreFrom;
  FIsUpdated:=False;

  col__recNo.Visible:=False;


  col__ID.Visible:=False;
  col__GUID.Visible:=False;


{  col_GeoRegion.Visible:=False;
  col_GeoRegion1.Visible:=False;
  col_GeoRegion2.Visible:=False;


}
(*
  col__ID.Visible:=True;
  col__GUID.Visible:=True;
*)
  g_ShellEvents.RegisterObject(Self);


  FDataset:=sp_Data;
 // FDataset:=sp_Data;

end;

//------------------------------------------------------------------
procedure Tframe_Browser_List.FormDestroy(Sender: TObject);
//------------------------------------------------------------------
begin
  g_ShellEvents.UnRegisterObject(Self);

  FreeAndNil(FFieldNames);
  FreeAndNil(FSelectedPIDLs);

  //save layout
   SetObjectName('');

//  UnRegisterProc (FProcIndex);
  inherited;
end;


//------------------------------------------------------------------
procedure Tframe_Browser_List.SetObjectName (aObjectName: string);
//------------------------------------------------------------------
//var sXML: string;

var
  s: string;
begin


  if (ObjectName<>'') and (ObjectName<>aObjectName) then
  begin
    s:=FObjectRegPath+ cxGrid1DBTableView1.Name;

    cxGrid1DBTableView1.StoreToRegistry(s, True,[gsoUseFilter]);

  end;

  if aObjectName='' then
    Exit;

  ObjectName:=aObjectName;



  FObjectRegPath:=FRegPath + aObjectName+'\1\';


  FObjectType:=g_Obj.ItemByName[ObjectName].Type_;

  FViewObject := dmViewEngine.PrepareObject1 (aObjectName);

  Assert(Assigned(FViewObject));

//  sObjectName:=g_Obj.ItemByType[aRec.ObjectType].Name;

//  FSQL:=dmViewEngine.GetTableSQLForFields(aObjectName,  FViewObject, FViewObject.ListFields, qry_Data);  //aMem_Fields,



  //�������� ������� �����
  LoadDBGridColumns ();



  cxGrid1DBTableView1.RestoreFromRegistry(FObjectRegPath+ cxGrid1DBTableView1.Name);


end;

//------------------------------------------------------------------
//  �������� ������� ��� �����
//------------------------------------------------------------------
procedure Tframe_Browser_List.View(aFolderID: integer; aGUID: string);
//------------------------------------------------------------------
var
  rec: TdmViewEngineOpenDatasetRec;
  sSQL: string;
begin
  if cb_show_list.EditValue <> true then
     Exit;

  FFolderID := aFolderID;
  { TODO : ! }

 // FGUID:=dmFolder.GetGuidByID (aFolderID);

  FillChar(rec, SizeOf(rec), 0);

  rec.ObjectType  :=FObjectType;
  rec.FolderID    :=FFolderID;
//  rec.FilterWhere :=FFilterWhere;

//  rec.IsAllFolders:=cb_Filter_All_Folders.Checked;
  rec.IsAllFolders:=cb_Filter_All_Folders_1.EditValue;


//  rec.FieldNames  :=FFieldNames;

 // db_ViewDataSet(mem_Data);

//  sSQL:=IIF(FFilter_SQL<>'',FFilter_SQL, FSQL);

  dmOnega_DB_data.OpenStoredProc (sp_Data, 'dbo.sp_Explorer_List_SEL', [
    'ObjName',    ObjectName,
    'FOLDER_ID',  FFolderID,
    'PROJECT_ID', dmMain.ProjectID,

    'IS_ALL_FOLDERS', cb_Filter_All_Folders_1.EditValue
  ]);


//..  if Assigned(FViewObject) then
//    dmViewEngine.OpenDB_list (sSQL, qry_Data, rec,  FViewObject);//  mem_Fields,
//    dmViewEngine.OpenDB_list (FSQL, qry_Data, rec,  FViewObject);//  mem_Fields,
//  dmViewEngine.OpenDB (mem_Data, qry_Data, rec, mem_Fields);

  StatusBar1.Panels[0].Text:=Format('�����: %d', [FDataset.RecordCount]);
//  StatusBar1.Panels[0].Text:=Format('�����: %d', [mem_Data.RecordCount]);
end;

//------------------------------------------------------------------
procedure Tframe_Browser_List.ReView ();
//------------------------------------------------------------------
//var
//  iFocusedNumber: integer;

begin

  View (FFolderID, '');
//  View (FFolderID, FGUID);

end;



//===========================================================================//
//  ��������� Control-A ��� ������ ���� ���������
//===========================================================================//
procedure Tframe_Browser_List.cxDBGrid11KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (Key = 65) AND (ssCtrl IN Shift) then
    cxGrid1DBTableView1.Controller.SelectAll;

end;


procedure Tframe_Browser_List.cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word; Shift:
    TShiftState);
begin
  if (Key = 65) AND (ssCtrl IN Shift) then
    cxGrid1DBTableView1.Controller.SelectAll;

end;

//===========================================================================//
procedure Tframe_Browser_List._Actions(Sender: TObject);
//===========================================================================//
var sName: string;
begin
  if Sender = act_SaveAsExcel then
    Exec_SaveAsExcel;


  if Sender = act_Search then
    cx_Search(cxGrid1DBTableView1, ed_Search.Text);


end;


//-------------------------------------------------------------------
procedure Tframe_Browser_List.GetSelectedPIDLs (out aPIDL_List: TrpPIDL_List);
//-------------------------------------------------------------------
var i: integer;
   oRow: TcxCustomGridRow;
   oPIDL: TrpPIDL;
begin
  FSelectedPIDLs.Clear;


  for i := 0 to cxGrid1DBTableView1.Controller.SelectedRowCount - 1 do
  begin
    oRow:=cxGrid1DBTableView1.Controller.SelectedRows[i];
    if not Assigned(oRow) then
      Continue;

//    oNode:=cxGrid1DBTableView1.SelectedNodes[i];
    oPIDL           :=FSelectedPIDLs.AddItem;
    oPIDL.ID        :=oRow.Values[col__ID.Index];
    oPIDL.GUID      :=oRow.Values[col__GUID.Index];
    oPIDL.ObjectName:=ObjectName;
  end;


  aPIDL_List.Assign (FSelectedPIDLs);
end;

{
//-------------------------------------------------------------------
procedure Tframe_Browser_List.mnu_ActionsPopup(Sender: TTBCustomItem; FromLink: Boolean);
//-------------------------------------------------------------------
begin
  AskActionsMenu();
end;
}

//-------------------------------------------------------------------
procedure Tframe_Browser_List.PopupMenu1Popup(Sender: TObject);
//-------------------------------------------------------------------
begin
  AskActionsMenu();
end;

//-------------------------------------------------------------------
procedure Tframe_Browser_List.AskActionsMenu ();
//-------------------------------------------------------------------
var
  i,j: integer;
  oPIDL: TrpPIDL;
  oRow: TcxCustomGridRow;

begin
//  inherited;
  // �������� ������ ���������� �������, ��� ������������ ���������� ��������� ��������
// ��� "��������" POPUP-���� �������� ������ ������ ��� �������� �������,
// � �������� �������� �� ���������� �����
  FSelectedPIDLs.Clear;



  for i := 0 to cxGrid1DBTableView1.Controller.SelectedRowCount - 1 do
  begin
    oRow:=cxGrid1DBTableView1.Controller.SelectedRows[i];
    if not Assigned(oRow) then
      Continue;


    oPIDL           :=FSelectedPIDLs.AddItem;
    oPIDL.ID        :=oRow.Values[col__ID.Index];
    oPIDL.GUID      :=oRow.Values[col__GUID.Index];
    oPIDL.ObjectName:=ObjectName;
  end;



{
  PostEvent (WE_OBJECT_GET_POPUP_MENU,
            [app_Par (PAR_PIDL_LIST, FSelectedPIDLs),
             app_Par (PAR_POPUP_MENU, PopupMenu1)
            ]);
}

  if assigned ( FOnAskMenu ) then
    FOnAskMenu (FSelectedPIDLs, PopupMenu1);


  if FSelectedPIDLs.Count = 0 then
  begin
    //dxBarSubItem_Menu
//    dlg_AssignPopupMenu (PopupMenu1, mnu_Actions);

    dlg_AssignPopupMenu_dx (PopupMenu1,  dxBarSubItem_Menu, dxBarManager1);

    exit;
  end;


{  //------------------------------------------------
  // !!USERS!!

  oAlwaysEnabledActionList:= TStringList.Create;

  if FSelectedPIDLs.Count=0 then
    exit;

  PostEvent (WE_OBJECT_GET_ALWAYS_ENABLED_ACTIONS,
            [app_Par(PAR_OBJECT_NAME,  ObjectName), //FSelectedPIDLs[0].ObjectName),
             app_Par(PAR_NAME,         oAlwaysEnabledActionList)  ]);


  if PopupMenu1.Items.Count>0 then
  begin
    if FSelectedPIDLs.Count=1 then
      bObjectCanBeModified:=
        dmUser.ObjectCanBeModified (FSelectedPIDLs[0].ObjectName, FSelectedPIDLs[0].ID)

    else begin
      SetLength(oIntArr, 0);
     // IntArrayClear(oIntArr);
      for i:= 0 to FSelectedPIDLs.Count-1 do
        IntArrayAddValue(oIntArr, FSelectedPIDLs[i].ID);

      bObjectCanBeModified:=
        dmUser.ObjectsCanBeModified (FSelectedPIDLs[0].ObjectName, oIntArr);
    end;
  end;


  for i:= 0 to PopupMenu1.Items.Count-1 do
    PopupMenu1.Items[i].Enabled:=  bObjectCanBeModified;

  for j := 0 to oAlwaysEnabledActionList.Count-1 do
  begin
    for i:= 0 to PopupMenu1.Items.Count-1 do
      if PopupMenu1.Items[i].Action is TAction then
        if (PopupMenu1.Items[i].Action as TAction).Name = oAlwaysEnabledActionList[j] then
          PopupMenu1.Items[i].Enabled:=  true;
  end;

  oAlwaysEnabledActionList.Free;
  //------------------------------------------------
}

//  dlg_AssignPopupMenu (PopupMenu1, mnu_Actions);
  dlg_AssignPopupMenu_dx (PopupMenu1, dxBarSubItem_Menu, dxBarManager1);



end;


procedure Tframe_Browser_List.Button1Click(Sender: TObject);
begin
  cx_Search(cxGrid1DBTableView1, ed_Search.Text);



end;



//===========================================================================//
// ��� ������� ������ �� ������ � ����� �������� � ����� ������� � Explorer-�
//===========================================================================//
procedure Tframe_Browser_List.cxDBGrid11DblClick(Sender: TObject);
//===========================================================================//
{var iID: integer;
  sGUID,sName: string;
}
begin
  if not cx_IsCursorInGrid(Sender) then
   Exit;

  if not FDataset.IsEmpty then
  begin
    FDblClick.ID  :=FDataset[FLD_ID];
    FDblClick.Name:=FDataset.FieldByName(FLD_NAME).AsString;
    FDblClick.GUID:=FDataset.FieldByName(FLD_GUID).AsString;
  end;

  TimerOnDblClick.Enabled:=True;

end;


procedure Tframe_Browser_List.TimerOnDblClickTimer(Sender: TObject);
begin
  TimerOnDblClick.Enabled:=False;
  
  if Assigned(FOnObjectDblClick) then
    FOnObjectDblClick (ObjectName, FDblClick.ID, FDblClick.GUID, FDblClick.Name);

end;


procedure Tframe_Browser_List.cb_Filter_All_FoldersClick(Sender: TObject);
begin
  if FIsUpdated then
    Exit;

  ReView ();
end;


procedure Tframe_Browser_List.ClearSelection;
begin
  cxGrid1DBTableView1.Controller.ClearSelection;
end;
       

procedure Tframe_Browser_List.col__recNoGetDataText(Sender: TcxCustomGridTableItem; ARecordIndex:
    Integer; var AText: string);
begin
  AText:= IntToStr(aRecordIndex);
end;

procedure Tframe_Browser_List.cb_Filter_All_Folders_1Change(Sender: TObject);
begin
  if FIsUpdated then
    Exit;

  ReView ();
end;

procedure Tframe_Browser_List.cb_show_listChange(Sender: TObject);
begin
  if FIsUpdated then
    Exit;

  if cb_show_list.EditValue = True then
    ReView ();

end;

procedure Tframe_Browser_List.cb_show_listClick(Sender: TObject);
begin
end;

{
procedure Tframe_Browser_List.Show_folders_all111111111111111;
begin
  if FIsUpdated then
    Exit;

  ReView ();
end;
 }


//-------------------------------------------------------------------
procedure Tframe_Browser_List.LoadDBGridColumns ();
//-------------------------------------------------------------------

     //--------------------------------------------------------------
     procedure DoAddColumn(aViewField: TViewField);
//     procedure DoAddColumn(aDataset: TDataset);
     //--------------------------------------------------------------
     var
       sColumnFieldName: string;
       sCaption,sFieldName,sDisplayName, //sXrefFieldName,
       sType: string;
//       oStrList: TStringList;
       ot: TViewEngFieldType;
       iWidth: integer;

       oCXGridDBColumn:TcxGridDBColumn;
     begin
   //    Result:=nil;

       sFieldName:=aViewField.Field_Name1;
     //  sFieldName:=aDataset.FieldByName(FLD_NAME).AsString;
       if sFieldName='' then
         Exit;


       oCXGridDBColumn:=cxGrid1DBTableView1.CreateColumn;


    //   sCaption:=aDataset.FieldByName(FLD_CAPTION).AsString;
       sCaption:=aViewField.Caption;

       sDisplayName   := aViewField.DisplayName;


      sColumnFieldName:=sFieldName;


      if TdmViewEngine_StrToFieldType(aViewField.FieldTypeStr) in

//      if TdmViewEngine_StrToFieldType(aDataset.FieldByName(FLD_TYPE).AsString) in
        [vftList,vftXRef,vftStatus]
      then
        sColumnFieldName:=sColumnFieldName+ '_str';


       oCXGridDBColumn.DataBinding.FieldName := sColumnFieldName; //Result.FieldName;

       oCXGridDBColumn.Caption:=sCaption;//aDataset.FieldByName(FLD_CAPTION).AsString;

       // ���� ���� - ������, �� � ������� �������� StringList � ���������� ����������

       sType:=aViewField.FieldTypeStr;

  //     sType:=aDataset.FieldByName(FLD_TYPE).AsString;
       ot:=TdmViewEngine_StrToFieldType(sType);

       case ot of
          vftText    : iWidth:=150; //db_CreateField (aMem_Data, sFieldName, ftString, 200);

          vftDegree,
          vftDegreeLat,
          vftDegreeLon,
          vftDate,
          vftDatetime,
          vftList,
          vftStatus,
          vftXRef,
          vftInt,
          vftFloat,
          vftBoolean : iWidth:=100;
       else
         iWidth:=100;
       end;

    //   Result.Width:=iWidth;

       oCXGridDBColumn.Width:=iWidth;



     end;

//var  j: integer;  //i,
var
  I: integer;
begin
  Assert(Assigned(FViewObject), 'FViewObject not assigned');


  cxGrid1DBTableView1.BeginUpdate;

 // cxGrid1DBTableView1.ColumnCount


  for I := cxGrid1DBTableView1.ColumnCount - 1 downto 0 do
    if cxGrid1DBTableView1.Columns[i].Tag<>1 then
      cxGrid1DBTableView1.Columns[i].Free;



  for I := 0 to FViewObject.ListFields.Count - 1 do
    if not FViewObject.ListFields[i].IsHidden then
      DoAddColumn (FViewObject.ListFields[i]);


  cxGrid1DBTableView1.EndUpdate;
 
end;


//--------------------------------------------------------------
procedure Tframe_Browser_List.GetShellMessage(aMsg: integer; aRec: TShellEventRec);
//--------------------------------------------------------------
var
  oIDList: TIDList;
  I: integer;

begin
  case aMsg of
    //-------------------------------------------------------------------
    WE__EXPLORER_DELETE_NODE_BY_GUID: begin
    //-------------------------------------------------------------------
//      sGUID:=aParams.ValueByName(PAR_GUID);

//      DELETE_NODE_BY_GUID(aRec.GUID);

      ReView ();
    end;


    //-------------------------------------------------------------------
    WE__EXPLORER_DELETE_NODES_BY_IDLIST: begin
    //-------------------------------------------------------------------
      ReView ();

{      oIDList:=TIDList(aRec.IDList);

      for I := 0 to oIDList.Count - 1 do
        DELETE_NODE_BY_GUID(oIDList[i].ObjName, oIDList[i].ID);

      //DELETE_NODE_BY_GUID(aRec.GUID);
}

    end;

  end;

end;



procedure Tframe_Browser_List.cxButtonEdit1PropertiesButtonClick( Sender: TObject; AButtonIndex: Integer);
begin
  case AButtonIndex of
    0: act_Filter_Open.Execute;
    1: act_Filter_Clear.Execute;
  end;
end;

procedure Tframe_Browser_List.dxBarSubItem_MenuPopup(Sender: TObject);
begin
  AskActionsMenu();
end;


procedure Tframe_Browser_List.Exec_SaveAsExcel;
begin
  if SaveDialog1.Execute then
  begin
    cx_ExportGridToExcel(SaveDialog1.FileName, cxGrid1);

//    ExportGrid4ToExcel(SaveDialog1.FileName, cxGrid1);

    ShellExec(SaveDialog1.FileName);
  end;
end;


// ---------------------------------------------------------------
procedure Tframe_Browser_List.ed_FilterPropertiesButtonClick( Sender: TObject; AButtonIndex: Integer);
// ---------------------------------------------------------------
var
  iID: Integer;
  sName, sSQL: WideString;   
begin
  inherited;

  case AButtonIndex of
    0: begin
         Init_IFilterX.Dlg (dmMain.GetConnectionString,  ObjectName , iID, sName, sSQL);

         FFilter_SQL:=sSQL;

         ed_Filter.EditValue:=sName;

         ReView ();
       end;

    1: begin
         TcxCustomEdit(Sender).Clear;
     //    ed_Filter.EditValue:='';
      //   TcxCustomEdit(Sender).PostEditValue;

         FFilter_SQL:='';
         ReView ();

       end;
  end;

end;

end.








      {

      ed_Filter.EditValue:=null;



var
  iID: Integer;
  sName, sSQL: WideString;

begin
 // ShowMessage(Sender.ClassName);

//  TcxCustomEdit(Sender)

  case AButtonIndex of
    0: begin
         CoFilterX.Create.Dlg(ADOConnection1.ConnectionString,  'link', iID, sName, sSQL);

         ed_Filter.EditValue:=sName;    
       end;
       
    1: begin
         ed_Filter.EditValue:=null;

         TcxCustomEdit(Sender).PostEditValue;


       end;

  end;
end;
      
procedure TForm22.ed_FilterPropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
 
var
  iID: Integer; 
  sName, sSQL: WideString;

begin
 // ShowMessage(Sender.ClassName);

//  TcxCustomEdit(Sender)

  case AButtonIndex of
    0: begin
         CoFilterX.Create.Dlg(ADOConnection1.ConnectionString,  'link', iID, sName, sSQL);

         ed_Filter.EditValue:=sName;    
       end;
       
    1: begin
         ed_Filter.EditValue:=null;

         TcxCustomEdit(Sender).PostEditValue;

         
       end;
    
  end;
end;





// ---------------------------------------------------------------
procedure cx_Search(aGrid: TcxGridTableView; aText: string);
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
  sFilter: string;

  oFilter: TcxFilterCriteria;

begin
 
  
  aGrid.FilterBox.Visible:=fvNever;// :=false;


  /////////////////////////////////////////////

  s:=Trim(aText);
  if Length(s)<2 then
  begin
   //  dmMain.Filter.Enabled :=False;
  
    aGrid.DataController.Filter.Active := False; 
    
    Exit;
  end;

  
  with aGrid.DataController.Filter do
  try
    BeginUpdate;
    Root.Clear;

    sFilter:= '%'+ s + '%';

    Root.BoolOperatorKind:= fboOr;
               

    for I := 1 to aGrid.ColumnCount-1 do
      Root.AddItem(aGrid.Columns[i], foLike, sFilter, sFilter);

  finally
    EndUpdate;
    Active := true;
  end;

end;



procedure Tframe_Browser_List.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
//var
//  bReadOnly: Boolean;
//  iGeoRegion1_ID: Integer;
//  iGeoRegion2_ID: Integer;
//  iGeoRegion_ID: Integer;
begin
  inherited;

 // ARecord.

 {
  assert (Assigned(col_GeoRegion));
  assert (Assigned(col_GeoRegion1));
  assert (Assigned(col_GeoRegion2));

  iGeoRegion_ID  := AsInteger(ARecord.Values[col_GeoRegion.Index]);
  iGeoRegion1_ID := AsInteger(ARecord.Values[col_GeoRegion1.Index]);
  iGeoRegion2_ID := AsInteger(ARecord.Values[col_GeoRegion2.Index]);

  bReadOnly:=
      dmUser_Security.GeoRegion_is_ReadOnly(iGeoRegion_ID) or
      dmUser_Security.GeoRegion_is_ReadOnly(iGeoRegion1_ID) or
      dmUser_Security.GeoRegion_is_ReadOnly(iGeoRegion2_ID);


  if bReadOnly then
    AStyle := cxStyle1;
 }

//
//end;
//
//  ARecord.



end;

