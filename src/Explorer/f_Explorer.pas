unit f_Explorer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Dialogs, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  rxPlacemnt, Menus, ActnList, Controls, Forms,


  u_const_msg,

  u_config,

  i_explorer,

u_shell_var,

u_vars,

  I_DBExplorerForm,

  dm_Main,
  I_Shell,

  fr_Explorer_Container,

  //dm_Explorer,
  u_Explorer,


  u_func,
  u_reg,
  u_dlg,
  u_func_msg,

  f_Browser,

  f_Custom,

  u_const,
//  u_const_msg,
  u_types,

  cxPropertiesStore, cxPC, cxControls, AppEvnts
  ;



type
  Tfrm_Explorer = class(Tfrm_Custom, IMessageHandler)
    FormPlacement1: TFormPlacement;
    Panel21111111: TPanel;
    cxPageControl1: TcxPageControl;
    cxTabSheet_project: TcxTabSheet;
    cxTabSheet_lib: TcxTabSheet;
//    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    Fframe_Explorer_Container: Tframe_Explorer_Container;
    Fframe_Dicts_Container: Tframe_Explorer_Container;

    procedure DoOnNodeFocused (aPIDL: TrpPIDL);
    procedure DoOnItemDblClick (aPIDL: TrpPIDL);
    procedure UpdateView;

    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList ; var aHandled:        boolean);

  public
    class function CreateForm: Tfrm_Explorer;

  end;


//==================================================================
implementation{$R *.dfm}     
//==================================================================


//-------------------------------------------------------
class function Tfrm_Explorer.CreateForm: Tfrm_Explorer;
//-------------------------------------------------------
var oForm: TForm;
begin
  oForm:=IsFormExists (Application,  Tfrm_Explorer.ClassName);

  if not Assigned(oForm) then
    oForm:=Tfrm_Explorer.Create(Application.MainForm);

//  oForm.Show;

  Result := Tfrm_Explorer(oForm);

//  if oForm.WindowState = wsMinimized then
 //   oForm.WindowState := wsMaximized;

end;

//--------------------------------------------------------------------
procedure Tfrm_Explorer.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
var
  b: Boolean;
begin
  inherited;

  cxPageControl1.Align:=alClient;
  cxPageControl1.ActivePageIndex :=0;



(*  Panel1.Align:=alCLient;
  PageControl1.Align:=alCLient;
*)


(*  TabSheet_Project.Caption:='Проект';
  TabSheet_Dicts.Caption:='Справочники';
*)
 (* ts_Project.Caption:='Проект';
  ts_Dicts.Caption:='Справочники';
*)

  Caption:='Структура';


//  ts_Project.Caption:='Проект';
//  tsDicts.Caption:='Справочники';



  CreateChildForm(Tframe_Explorer_Container, Fframe_Explorer_Container, cxTabSheet_project);
//  CreateChildForm(Tframe_Explorer_Container, Fframe_Explorer_Container, TabSheet_Project);
//  CreateChildForm(Tframe_Explorer_Container, Fframe_Explorer_Container, ts_Project);

 // Fframe_Explorer_Container:=Tframe_Explorer_Container.CreateChildForm ( ts_Project);


  Fframe_Explorer_Container.OnNodeFocused:=DoOnNodeFocused;
  Fframe_Explorer_Container.OnItemDblClick:=DoOnItemDblClick;
  Fframe_Explorer_Container.Style :=esProject;
//  Fframe_Explorer_Container.RegPath :='Projects\' + IntToStr(dmMain.ProjectID);

  Fframe_Explorer_Container.SetViewObjects (PROJECT_TYPES);

  Fframe_Explorer_Container.Load();

 ///////////
//  IMainProjectExplorer:=Fframe_Explorer_Container.GetInterfaceX;
  IMainProjectExplorer:=Fframe_Explorer_Container;


//  CreateChildForm(Tframe_Explorer_Container, Fframe_Dicts_Container, ts_Dicts);
  CreateChildForm(Tframe_Explorer_Container, Fframe_Dicts_Container, cxTabSheet_lib);
//  CreateChildForm(Tframe_Explorer_Container, Fframe_Dicts_Container, TabSheet_Dicts);



//  Fframe_Dicts_Container:=Tframe_Explorer_Container.CreateChildForm( ts_Dicts);
  Fframe_Dicts_Container.OnNodeFocused:=DoOnNodeFocused;
  Fframe_Dicts_Container.Fframe_Explorer.pn_ToolBar_Search.Visible:=False;

  Fframe_Dicts_Container.OnItemDblClick:=DoOnItemDblClick;
  Fframe_Dicts_Container.Style :=esDict;
 // Fframe_Dicts_Container.RegPath :='Dictionary';
  Fframe_Dicts_Container.SetStateFileName( g_Application_FormStateDir + 'Dictionary.txt');

  if g_Config.IsInterfaceSimple then
    Fframe_Dicts_Container.SetViewObjects (DICT_TYPES_SIMPLE)
  else
    Fframe_Dicts_Container.SetViewObjects (DICT_TYPES);


  Fframe_Dicts_Container.Load();


  AddComponentProp(cxPageControl1, PROP_ACTIVE_PAGE);
  cxPropertiesStore.RestoreFrom;

 // AddControl (GSPages1, [PROP_ACTIVE_PAGE_INDEX]);

  FormPlacement1.Active:=true;
  FormPlacement1.RestoreFormPlacement;


//  Fframe_Explorer_Container.


  b:=Fframe_Explorer_Container.Fframe_Explorer.GetInterface(IExplorerX, g_IExplorer_prj);
  assert(b);

  b:=Fframe_Dicts_Container.Fframe_Explorer.GetInterface(IExplorerX, g_IExplorer_lib);
  assert(b);




 // Assert(dmObject_Manager<>nil);


end;

// ---------------------------------------------------------------
procedure Tfrm_Explorer.UpdateView;
// ---------------------------------------------------------------
begin
  if g_Config.IsInterfaceSimple then
    Fframe_Dicts_Container.SetViewObjects (DICT_TYPES_SIMPLE)
  else
    Fframe_Dicts_Container.SetViewObjects (DICT_TYPES);


  Fframe_Dicts_Container.Load();

  Fframe_Explorer_Container.Load();


end;


//--------------------------------------------------------------------
procedure Tfrm_Explorer.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  g_IExplorer_prj:=nil;
  g_IExplorer_lib:=nil;


  IMainProjectExplorer:=nil;

  FreeAndNil(Fframe_Explorer_Container);
  FreeAndNil(Fframe_Dicts_Container);

//  ShowMessage('procedure Tfrm_Explorer.FormDestroy(Sender: TObject);');

//  Fframe_Explorer_Container.Free;
 // Fframe_Dicts_Container.Free;

  inherited;
end;

//--------------------------------------------------------------------
procedure Tfrm_Explorer.DoOnNodeFocused(aPIDL: TrpPIDL);
//--------------------------------------------------------------------
//var
 // rec: TShellEventRec;
begin
  g_EventManager.PostEvent_(et_MAP_UPDATE_STOP,  []);

  g_ShellEvents.Shell_OPEN_PIDL(aPIDL);

  Application.ProcessMessages;
  g_EventManager.PostEvent_(et_MAP_UPDATE_START,  []);


{  rec.PIDL:=aPIDL;
  g_ShellEvents.PostEvent(WE__EXPLORER_OPEN_PIDL, rec);
}
//  PostEvent (WE_EXPLORER_OPEN_PIDL, [app_Par(PAR_PIDL, aPIDL) ]);// , 0);
//  PostWMsg (WM_EXPLORER_OPEN_PIDL, Integer(aPIDL), 0);

end;

// ---------------------------------------------------------------
procedure Tfrm_Explorer.DoOnItemDblClick(aPIDL: TrpPIDL);
// ---------------------------------------------------------------
begin
  g_EventManager.PostEvent_(et_MAP_UPDATE_STOP,  []);

  Tfrm_Browser.ViewObjectByPIDL (aPIDL);

  Application.ProcessMessages;
  g_EventManager.PostEvent_(et_MAP_UPDATE_START,  []);

//  PostWMsg (WM_EXPLORER_OPEN_PIDL, Integer(aPIDL), 0);

end;


procedure Tfrm_Explorer.GetMessage(aMsg: TEventType; aParams: TEventParamList ;
    var aHandled: boolean);
begin
   case aMsg of
    et_INTERFACE_NORMAL,
    et_INTERFACE_SIMPLE: UpdateView();

  end;

end;


end.


(*
{

procedure Tfrm_Explorer.ApplicationEvents1Message(var Msg: tagMSG; var Handled:
    Boolean);
begin
  inherited;

{

  case Msg.message of
    Integer (WM_INTERFACE_NORMAL): ShowMessage('WM_INTERFACE_NORMAL');
    Integer (WM_INTERFACE_SIMPLE): ShowMessage('WM_INTERFACE_SIMPLE');
  else
    Exit;
  end;
  }


   case Msg.message of
    Integer (WM_INTERFACE_NORMAL),
    Integer (WM_INTERFACE_SIMPLE): UpdateView();

  end;





{    TMyMesssage_ =
    (
      WM_TEST = WM_USER + 6000 + 100,
      WM_INTERFACE_NORMAL,
      WM_INTERFACE_SIMPLE
    );
}

end;

