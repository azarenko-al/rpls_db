object Form1: TForm1
  Left = 551
  Top = 308
  Width = 813
  Height = 515
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 96
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 96
    Top = 144
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Chart1: TChart
    Left = 264
    Top = 40
    Width = 433
    Height = 393
    Title.Text.Strings = (
      'TChart')
    BottomAxis.Increment = 10.000000000000000000
    View3D = False
    TabOrder = 2
    ColorPaletteIndex = 13
    object Series1: TPolarSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      Circled = True
      AngleIncrement = 10.000000000000000000
      AngleValues.Name = 'Angle'
      AngleValues.Order = loAscending
      Brush.Color = clWhite
      Brush.Style = bsClear
      Pen.Color = 10708548
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = True
    end
  end
end
