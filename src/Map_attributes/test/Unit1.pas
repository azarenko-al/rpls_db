unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,

  dm_Main,

  d_Map_Attributes,

  Dialogs, StdCtrls, TeEngine, Series, TeePolar, ExtCtrls, TeeProcs, Chart;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Chart1: TChart;
    Series1: TPolarSeries;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  TdmMain.Init;
  if not dmMain.OpenDB_reg then
    Exit;    
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  s: string;
begin
  s := '';

  Tdlg_Map_Attributes.ExecDlg('property', s);
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  s: string;
begin
  s := '';


  Tdlg_Map_Attributes.ExecDlg('link', s);

end;

end.
 