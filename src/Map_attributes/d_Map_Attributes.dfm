object dlg_Map_Attributes: Tdlg_Map_Attributes
  Left = 698
  Top = 313
  Width = 891
  Height = 599
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'dlg_Map_Attributes'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pn_Header: TPanel
    Left = 0
    Top = 0
    Width = 883
    Height = 113
    Align = alTop
    Color = clAppWorkSpace
    TabOrder = 0
    Visible = False
  end
  object pn_Buttons: TPanel
    Left = 0
    Top = 530
    Width = 883
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    PopupMenu = PopupMenu1
    TabOrder = 1
    DesignSize = (
      883
      41)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 883
      Height = 2
      Align = alTop
    end
    object btn_Cancel: TButton
      Left = 802
      Top = 10
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 0
    end
    object btn_Ok: TButton
      Left = 719
      Top = 10
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 1
    end
    object Button1: TButton
      Left = 16
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 2
      OnClick = Button1Click
    end
    object Edit1: TEdit
      Left = 120
      Top = 8
      Width = 473
      Height = 21
      TabOrder = 3
      Text = 'Edit1'
    end
    object Button2: TButton
      Left = 616
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Button2'
      TabOrder = 4
      OnClick = Button2Click
    end
  end
  object cxDBTreeList2: TcxDBTreeList
    Left = 0
    Top = 113
    Width = 883
    Height = 232
    Align = alTop
    Bands = <
      item
      end>
    DataController.DataSource = DataSource2
    DataController.ParentField = 'parent_id'
    DataController.KeyField = 'id'
    DefaultRowHeight = 17
    LookAndFeel.Kind = lfFlat
    OptionsCustomizing.BandMoving = False
    OptionsCustomizing.BandVertSizing = False
    OptionsCustomizing.ColumnVertSizing = False
    OptionsData.Deleting = False
    OptionsView.ShowEditButtons = ecsbFocused
    OptionsView.Indicator = True
    OptionsView.SimpleCustomizeBox = True
    Preview.LeftIndent = 1
    Preview.MaxLineCount = 1
    RootValue = -1
    TabOrder = 2
    object cxDBTreeList2cxDBTreeListColumn1: TcxDBTreeListColumn
      PropertiesClassName = 'TcxCheckBoxProperties'
      DataBinding.FieldName = 'checked'
      Width = 60
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList2is_folder: TcxDBTreeListColumn
      DataBinding.FieldName = 'is_folder'
      Width = 100
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList2index_: TcxDBTreeListColumn
      DataBinding.FieldName = 'index_'
      Width = 100
      Position.ColIndex = 4
      Position.RowIndex = 0
      Position.BandIndex = 0
      SortOrder = soAscending
      SortIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList2name: TcxDBTreeListColumn
      DataBinding.FieldName = 'name'
      Width = 100
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList2alias: TcxDBTreeListColumn
      DataBinding.FieldName = 'alias'
      Width = 100
      Position.ColIndex = 5
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList2type: TcxDBTreeListColumn
      DataBinding.FieldName = 'type'
      Width = 100
      Position.ColIndex = 6
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList2caption: TcxDBTreeListColumn
      DataBinding.FieldName = 'caption'
      Width = 145
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList2is_readonly: TcxDBTreeListColumn
      DataBinding.FieldName = 'is_readonly'
      Width = 100
      Position.ColIndex = 7
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList2hidden: TcxDBTreeListColumn
      DataBinding.FieldName = 'hidden'
      Width = 100
      Position.ColIndex = 8
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList2enabled: TcxDBTreeListColumn
      DataBinding.FieldName = 'enabled'
      Width = 100
      Position.ColIndex = 9
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList2xref_ObjectName: TcxDBTreeListColumn
      DataBinding.FieldName = 'xref_ObjectName'
      Width = 100
      Position.ColIndex = 10
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList2xref_TableName: TcxDBTreeListColumn
      DataBinding.FieldName = 'xref_TableName'
      Width = 100
      Position.ColIndex = 11
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList2xref_FieldName: TcxDBTreeListColumn
      DataBinding.FieldName = 'xref_FieldName'
      Width = 100
      Position.ColIndex = 12
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList2is_in_list: TcxDBTreeListColumn
      DataBinding.FieldName = 'is_in_list'
      Width = 100
      Position.ColIndex = 13
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList2is_in_view: TcxDBTreeListColumn
      DataBinding.FieldName = 'is_in_view'
      Width = 100
      Position.ColIndex = 14
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList2column_caption: TcxDBTreeListColumn
      DataBinding.FieldName = 'column_caption'
      Width = 100
      Position.ColIndex = 15
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList2status_name: TcxDBTreeListColumn
      DataBinding.FieldName = 'status_name'
      Width = 100
      Position.ColIndex = 16
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxDBTreeList2calculated: TcxDBTreeListColumn
      DataBinding.FieldName = 'calculated'
      Width = 100
      Position.ColIndex = 17
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object FormStorage: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 440
    Top = 8
  end
  object ActionList: TActionList
    Left = 384
    Top = 8
    object act_CheckedEdited: TAction
      Caption = #1091' '#1087#1072#1088#1072#1084#1077#1090#1088#1086#1074' '#1089' '#1079#1072#1087#1086#1083#1085#1077#1085#1085#1099#1084#1080' '#1079#1085#1072#1095#1077#1085#1080#1103#1084#1080
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 200
    Top = 8
    object Jnkflrf1: TMenuItem
      Caption = #1054#1090#1083#1072#1076#1082#1072
    end
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega_link;'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 248
    Top = 432
  end
  object DataSource2: TDataSource
    DataSet = ADOStoredProc1
    Left = 48
    Top = 464
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 384
    Top = 432
    PixelsPerInch = 96
    object cxStyle_folder: TcxStyle
    end
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    Filter = 'is_folder=true'
    LockType = ltBatchOptimistic
    ProcedureName = 'sp_Map_Attributes'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 22
      end
      item
        Name = '@OBJNAME'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = 'property'
      end>
    Left = 48
    Top = 408
  end
end
