unit d_Map_Attributes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,

  dm_Onega_DB_data,

  u_const_db,

  u_db,
  u_img,
  u_func,

  cxTLData, Grids, DBGrids, Menus, DB, ADODB, ActnList, rxPlacemnt,
  cxInplaceContainer, cxTL, cxDBTL, cxControls, StdCtrls, ExtCtrls,
  dxmdaset, cxStyles;

//const
//  sp_User_Map_Attributes_update_record = 'sp_User_Map_Attributes_update_record';
//  sp_User_Map_Attributes_select        = 'sp_User_Map_Attributes_select';


type
  Tdlg_Map_Attributes = class(TForm)
    pn_Header: TPanel;
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    btn_Cancel: TButton;
    btn_Ok: TButton;
    FormStorage: TFormStorage;
    ActionList: TActionList;
    act_CheckedEdited: TAction;
    PopupMenu1: TPopupMenu;
    Jnkflrf1: TMenuItem;
    cxDBTreeList2: TcxDBTreeList;
    ADOConnection1: TADOConnection;
    DataSource2: TDataSource;
    cxDBTreeList2is_folder: TcxDBTreeListColumn;
    cxDBTreeList2index_: TcxDBTreeListColumn;
    cxDBTreeList2name: TcxDBTreeListColumn;
    cxDBTreeList2alias: TcxDBTreeListColumn;
    cxDBTreeList2type: TcxDBTreeListColumn;
    cxDBTreeList2caption: TcxDBTreeListColumn;
    cxDBTreeList2is_readonly: TcxDBTreeListColumn;
    cxDBTreeList2hidden: TcxDBTreeListColumn;
    cxDBTreeList2enabled: TcxDBTreeListColumn;
    cxDBTreeList2xref_ObjectName: TcxDBTreeListColumn;
    cxDBTreeList2xref_TableName: TcxDBTreeListColumn;
    cxDBTreeList2xref_FieldName: TcxDBTreeListColumn;
    cxDBTreeList2is_in_list: TcxDBTreeListColumn;
    cxDBTreeList2is_in_view: TcxDBTreeListColumn;
    cxDBTreeList2column_caption: TcxDBTreeListColumn;
    cxDBTreeList2status_name: TcxDBTreeListColumn;
    cxDBTreeList2calculated: TcxDBTreeListColumn;
    cxDBTreeList2cxDBTreeListColumn1: TcxDBTreeListColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_folder: TcxStyle;
    ADOStoredProc1: TADOStoredProc;
    Button1: TButton;
    Edit1: TEdit;
    Button2: TButton;
//    procedure ADOStoredProc1_SelectAfterPost(DataSet: TDataSet);
    procedure Button1Click(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
//    procedure Button3Click(Sender: TObject);
//    procedure btn_UpdateClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
//    procedure cxDBTreeList1Editing(Sender: TObject; AColumn: TcxTreeListColumn; var
   //     Allow: Boolean);
 //   procedure Jnkflrf1Click(Sender: TObject);
  private
    FDataset : TDataSet;
    function GetSelected: string;

//    procedure Debug(Value: Boolean);
 //   procedure UpdateRec;
  public
    class function ExecDlg(aObjName: string; var aColumns: string): boolean;
  end;

//==============================================================================
implementation  {$R *.DFM}
//==============================================================================

//------------------------------------------------------------------------------
class function Tdlg_Map_Attributes.ExecDlg(aObjName: string; var aColumns:
    string): boolean;
//------------------------------------------------------------------------------
var
  iRes: Integer;
  iResult: Integer;

begin
  // -------------------------
  with Tdlg_Map_Attributes.Create(Application) do
  // -------------------------
  try
    assert(Assigned(dmOnega_DB_data));

    ADOStoredProc1.Close;
    ADOStoredProc1.Parameters.ParamByName('@objname').Value:=aObjName;
    ADOStoredProc1.Open;


{
    iRes:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc1_Select, sp_User_Map_Attributes_select,
        [
         db_Par(FLD_OBJNAME,  aObjName)
        ]);
}

 //   cxDBTreeList1.FullExpand;


    Result:=(ShowModal=mrOk);

    // -------------------------
    if Result then
    // -------------------------
    begin
    end;


  finally
    Free;
  end;
end;


//------------------------------------------------------------------------------
procedure Tdlg_Map_Attributes.FormCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  Caption:= '������������ �������� �����';

  FDataset:=ADOStoredProc1;

//  cxDBTreeList1.Align:=alClient;
//  cxDBTreeList1.OptionsView.ShowEditButtons :=ecsbFocused;


  cxDBTreeList2.Align:=alClient;
  

//  dxMemData.LoadFromDataSet(ADOQuery1);

//  DataSource2.DataSet:=dxMemData;

//db_View (ADOStoredProc1);



//  Debug(False);

//  if ADOStoredProc1_Select.Active then
//    ShowMessage('ADOStoredProc1_Select.Active');

//  db_ClearReadOnly(ADOStoredProc1);

end;



procedure Tdlg_Map_Attributes.Button1Click(Sender: TObject);
var
  b: Boolean;
  oList: TStringList;
begin
  db_PostDataset(FDataset);


  oList:=TStringList.create;


  FDataset.First;

  with FDataset do
    while not EOF do
    begin
      b:= FieldByName(FLD_CHECKED).AsBoolean;

      if b then
        oList.Add(FieldByName(FLD_NAME).AsString);

      Next;
    end;


  Edit1.Text:=oList.DelimitedText;


  FreeAndNil(oList);

end;


// ---------------------------------------------------------------
function Tdlg_Map_Attributes.GetSelected: string;
// ---------------------------------------------------------------
var
  b: Boolean;
  oList: TStringList;
begin
  db_PostDataset(FDataset);


  oList:=TStringList.create;


  FDataset.First;

  with FDataset do
    while not EOF do
    begin
      b:= FieldByName(FLD_CHECKED).AsBoolean;

      if b then
        oList.Add(FieldByName(FLD_NAME).AsString);

      Next;
    end;


  Result:=oList.DelimitedText;


  FreeAndNil(oList);

end;




procedure Tdlg_Map_Attributes.Button2Click(Sender: TObject);
var
  I: Integer;
  oList: TStringList;
begin
  oList:=TStringList.create;


  oList.DelimitedText:=Edit1.Text;


  for I := 0 to  oList.Count - 1 do
  begin
    if FDataset.Locate(FLD_NAME, oList[i], []) then
      db_UpdateRecord(FDataset, FLD_CHECKED, True);

  end;


  FreeAndNil(oList);

end;

end.


