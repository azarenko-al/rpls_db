 unit fr_Template_Link_inspector;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, rxPlacemnt, StdCtrls, ExtCtrls, Math,

  fr_DBInspector_Container,

  d_LinkEndType_Mode,

 // fr_Object_inspector,

//  dm_Terminal,
  dm_LinkEndType,
//  dm_LinkEnd,

  u_func,
  u_db,
  u_dlg,

  u_const_db,
  u_types

  ;

type
  Tframe_Template_Link_inspector = class(Tframe_DBInspector_Container)

//  Tframe_PMP_inspector = class(Tframe_Object_inspector)
    procedure FormCreate(Sender: TObject);
  private
    function Dlg_UpdateMode: Boolean;

    procedure DoOnButtonFieldClick (Sender: TObject; aFieldName: string;     aNewValue: Variant); override;

  public

  end;


//==================================================================
implementation {$R *.dfm}
//==================================================================


//-------------------------------------------------------------------
procedure Tframe_Template_Link_inspector.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;
  PrepareViewForObject (OBJ_TEMPLATE_LINK);

end;


//-------------------------------------------------------------------
procedure Tframe_Template_Link_inspector.DoOnButtonFieldClick (Sender: TObject; aFieldName: string;     aNewValue: Variant);
//-------------------------------------------------------------------
begin
  if Eq(aFieldName, FLD_MODE) then
    Dlg_UpdateMode


end;




//-------------------------------------------------------------------
function Tframe_Template_Link_inspector.Dlg_UpdateMode: Boolean;
//-------------------------------------------------------------------
var
  iID: Integer;
  rec: TdmLinkEndTypeModeInfoRec;

  iLinkEndType_Mode_ID: integer;
  iLinkEndTypeID: Integer;

begin
  iLinkEndType_Mode_ID:=GetIntFieldValue (FLD_LinkEndType_Mode_ID);
  iLinkEndTypeID      :=GetIntFieldValue (FLD_LINKENDTYPE_ID);

 // Assert(iLinkEndTypeID>0);

  iID:= Tdlg_LinkEndType_Mode.Dlg_GetMode1 (iLinkEndTypeID, iLinkEndType_Mode_ID, rec);

  if iID>0 then
  begin
     SetFieldValue (FLD_MODE,                rec.Mode);
     SetFieldValue (FLD_LinkEndType_Mode_ID, rec.ID);


     HideEdit;


//     Result := True;

  end //else
 //    Result := False;
end;


end.

