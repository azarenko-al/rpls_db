unit fr_Template_Link_View;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,cxControls, cxSplitter,
  Dialogs, ActnList, Menus, DBCtrls, StdCtrls, rxPlacemnt, ToolWin,
  ComCtrls, ExtCtrls, Db, ADODB,  AppEvnts, ImgList,
  cxPropertiesStore, cxGraphics, cxLookAndFeels,

  dm_User_Security,

  fr_Template_Link_inspector,

  fr_View_base,

//  fr_Template_Site_LinkEnd_antennas,

  fr_DBInspector_Container,
                                                              

 // fr_Template_Site_view_ant,


//  fr_Template_Site_Cells,

  u_reg,
  u_dlg,
  u_func,

  u_const_db,

  u_types,

  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters, dxBar,
  cxBarEditItem, cxClasses
  ;


type
  Tframe_Template_Link_View = class(Tframe_View_Base)
    FormStorage1: TFormStorage;
    pn_Inspector: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  //  procedure FormResize(Sender: TObject);
  private
    Ffrm_Inspector: Tframe_Template_Link_inspector; //Tframe_DBInspector_Container;

  //  FAnt: Tframe_Template_Site_view_ant;

//    FAnt1: Tframe_Template_Site_view_ant;
//    FAnt2: Tframe_Template_Site_view_ant;

//    Ffrm_SiteTemplate_Cells: Tframe_SiteTemplate_Cells;

  public

    procedure View(aID: integer; aGUID: string); override;
  end;



//====================================================================
implementation {$R *.dfm}
//====================================================================

//--------------------------------------------------------------------
procedure Tframe_Template_Link_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_TEMPLATE_LINK;
  TableName:=TBL_TEMPLATE_LINK;

  pn_Inspector.Align:=alClient;


 // Ffrm_SiteTemplate_Cells :=Tframe_SiteTemplate_Cells.Create (pn_Cells);

  //////////////////
//  CreateChildForm(Tframe_SiteTemplate_Cells, Ffrm_SiteTemplate_Cells, pn_Cells);
  CreateChildForm(Tframe_Template_Link_inspector,  Ffrm_Inspector, pn_Inspector);

 // CreateChildForm(Tframe_Template_Site_view_ant,  FAnt,  pn_Ant);

//  CreateChildForm(Tframe_Template_Site_view_ant,  FAnt1,  Panel_1);
//  CreateChildForm(Tframe_Template_Site_view_ant,  FAnt2,  Panel_2);

//  Ffrm_Inspector.OnButtonClick:=

  Ffrm_Inspector.PrepareViewForObject(OBJ_TEMPLATE_LINK);

//  PageControl1.Height:=reg_ReadInteger(FRegPath, PageControl1.Name,  PageControl1.Height);


 // PageControl1.Height:=250;


//  pn_Ant.Align:=alClient;
//  Panel_2.Align:=alClient;


end;

//--------------------------------------------------------------------
procedure Tframe_Template_Link_View.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
 // reg_WriteInteger(FRegPath, PageControl1.name,  PageControl1.Height);

  inherited;
end;



//--------------------------------------------------------------------
procedure Tframe_Template_Link_View.View(aID: integer; aGUID: string);
//--------------------------------------------------------------------
var
  bReadOnly: Boolean;
begin
  inherited;

  Ffrm_Inspector.View (aID);

 // FAnt.View(aID);


  bReadOnly:=dmUser_Security.Lib_Edit_ReadOnly();

  Ffrm_Inspector.SetReadOnly(bReadOnly);
  
//  Fframe_AntType_masks.SetReadOnly(bReadOnly);




//  FAnt1.View(aID,1);
//  FAnt2.View(aID,2);

 /// Ffrm_SiteTemplate_Cells.View(aID);
end;



end.
