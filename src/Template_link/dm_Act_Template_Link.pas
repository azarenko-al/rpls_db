unit dm_Act_Template_Link;

interface

uses
  Classes, Forms, ActnList, Dialogs,

dm_User_Security,

  dm_act_Base,

  i_Audit,

  I_Object,

  dm_Template_Link,

  u_DataExport_run,


//  dm_Template_Site_Linkend,

//  d_Template_Site_Linkend_add,

 // fr_Template_Linkend_Antenna_inspector,
  fr_Template_Link_view,

  u_const_db,
  u_const_str,


//    u_const_str,
  u_const,
//  u_const_msg,
//  u_const_db,



  u_Types,

  u_func
  , Menus;

type
  TdmAct_Template_Link = class(TdmAct_Base)
    ActionList2: TActionList;
    act_Add_item: TAction;
    act_Export_MDB: TAction;
    act_Audit: TAction;
    procedure act_Object_ShowOnMapExecute(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    function CheckActionsEnable: Boolean;
    procedure DoAction(Sender: TObject);
    function ItemDel(aID: integer; aName: string = ''): boolean;  override;
  protected
    procedure GetPopupMenu(aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function GetViewForm (aOwnerForm: TForm): TForm; override;

  public
  //  function Dlg_Edit(aID: integer): boolean; //override;

    class procedure Init;
  end;

var
  dmAct_Template_Link: TdmAct_Template_Link;

//====================================================================
implementation

uses dm_Main;
{$R *.dfm}

procedure TdmAct_Template_Link.act_Object_ShowOnMapExecute(Sender:
    TObject);
begin

end;


//--------------------------------------------------------------------
class procedure TdmAct_Template_Link.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_Template_Link) then
    dmAct_Template_Link:=TdmAct_Template_Link.Create(Application);
end;

//--------------------------------------------------------------------
procedure TdmAct_Template_Link.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_TEMPLATE_LINK;


//  act_Add_Linkend.Caption:='������� �� �������';
 // act_Add_Linkend_simple.Caption:='������� �� �������';

  act_Add_item.Caption:='������� ������';

  act_Export_MDB.Caption:='������� � MDB';

  act_Audit.Caption:=STR_Audit;


  SetActionsExecuteProc ([
          // act_Add_mast,
           act_Add_item,
        //   act_Add_Linkend,
//           act_Add_Linkend_simple,
           act_Export_MDB,

//
//           act_Object_ShowOnMap,
//
//        //   act_MoveToFolder,
//           act_Change_LinkEndType,
//           act_ShowLinkInfo,
//
//           act_Object_dependence,

         //  act_Copy

//           act_GroupAssign  ,
//           act_Change_AntType

           act_Audit

         //  act_Copy

         ], DoAction);

end;


// ---------------------------------------------------------------
procedure TdmAct_Template_Link.DoAction(Sender: TObject);
// ---------------------------------------------------------------
var
  iID: Integer;
  sName: string;
begin
  //---------------------------------
//  if Sender = act_Add_Linkend_simple then begin
  //---------------------------------
  //   dmTemplate_Site_Linkend.Dlg_Add (FFocusedID);

 //    ConfirmDlg()

//    if Tdlg_Template_Antenna_add.ExecDlg(iID)>0 then
 //     View();

//  end else

 //---------------------------------
  if Sender = act_Add_item then begin
  //---------------------------------
     dmTemplate_Link.Dlg_Add;


 //    ConfirmDlg()

//    if Tdlg_Template_Antenna_add.ExecDlg(iID)>0 then
 //     View();

  end else



  if Sender=act_Audit then
//    Dlg_Audit(TBL_ANTENNATYPE, FFocusedID) else
     Init_IAuditX.ExecDlg( dmMain.GetConnectionString, TBL_Template_Link, FFocusedID) else


//  if Sender=act_Audit then
//     Dlg_Audit(TBL_LinkEnd, FFocusedID) else


  //   Tdlg_Audit.ExecDlg (TBL_LinkEnd, FFocusedID) else


(*  if Sender=act_MoveToFolder then
    raise Exception.Create('')

   // dmAct_Folder.MoveToFolderDlg (ObjectName, FSelectedPIDLs)
  else
*)

//  if Sender=act_Copy then
//    Copy() else


{
  //---------------------------------
  if Sender = act_Add_Linkend then
  //---------------------------------
  begin
    iID:= Tdlg_Template_Site_Linkend_add.ExecDlg (FFocusedID );

  //  if iID > 0 then
     // View();
  end else
 }

  if Sender=act_Export_MDB then
//    TDataExport_run.ExportToRplsDbGuides1 (OBJ_LINKEND_TYPE);
    TDataExport_run.ExportToRplsDb_selected (TBL_Template_LINK, FSelectedIDList)
  else


{
  //---------------------------------
  if Sender = act_Add then begin
  //---------------------------------
//    if Tdlg_Template_Antenna_add.ExecDlg(iID)>0 then
 //     View();

  end else
  }


end;


//--------------------------------------------------------------------
function TdmAct_Template_Link.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_Template_Link_View.Create(aOwnerForm);
end;


//--------------------------------------------------------------------
function TdmAct_Template_Link.ItemDel(aID: integer; aName: string = ''):
    boolean;
//--------------------------------------------------------------------
begin

  dmTemplate_Link.Del (aID);
  Result:=True;

end;


// ---------------------------------------------------------------
function TdmAct_Template_Link.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
  Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];


  SetActionsEnabled1( [

// act_Export_MDB,
//                           act_Import_MDB,

  act_Add_item,

  act_Del_,
  act_Del_list

//  act_Add_Linkend_simple

      ],

   Result );

   
   //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );


end;



//--------------------------------------------------------------------
procedure TdmAct_Template_Link.GetPopupMenu(aPopupMenu: TPopupMenu;
    aMenuType: TMenuType);
//--------------------------------------------------------------------
var
  bEnable: Boolean;
begin
  bEnable:=CheckActionsEnable();



  case aMenuType of
    mtFolder: begin
                AddMenuItem (aPopupMenu, act_Add_item);

                AddMenuItem (aPopupMenu, act_Export_MDB);


              //  AddFolderMenu_Create (aPopupMenu);
               // AddFolderPopupMenu (aPopupMenu);
              end;
    mtItem:   begin
          //      AddMenuItem (aPopupMenu, act_Add_Linkend);
//                AddMenuItem (aPopupMenu, act_Add_Linkend_simple);

             //   AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Del_);
                AddMenuItem (aPopupMenu, act_Export_MDB);


                AddMenuItem (aPopupMenu, act_Audit);


              //  AddFolderMenu_Tools (aPopupMenu);
               /// AddMenuItem (aPopupMenu, act_Export_MDB);

              end;
    mtList:   begin
                AddMenuItem (aPopupMenu, act_Del_list);
              //  AddFolderMenu_Tools (aPopupMenu);
              //  AddMenuItem (aPopupMenu, act_Export_MDB);

              end;
  end;


end;


end.



