unit d_Company_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   rxPlacemnt, ActnList,    cxPropertiesStore,
  StdCtrls, ExtCtrls,

  d_Wizard_Add_with_params,

  u_const_str,
  u_const_db,
//  u_const_msg,

  u_func,
  u_reg,
//  u_func_msg,

  dm_Main,
  dm_Company, ComCtrls

  ;

type
  Tdlg_Company_add = class(Tdlg_Wizard_add_with_params)
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
  private
    FID,FFolderID: integer;
    procedure Append;
  public
    class function ExecDlg (aFolderID: integer =0): integer;
  end;


//====================================================================
implementation {$R *.DFM}
//====================================================================

//--------------------------------------------------------------------
class function Tdlg_Company_add.ExecDlg (aFolderID: integer =0): integer;
//--------------------------------------------------------------------
begin
  with Tdlg_Company_add.Create(Application) do
  begin
    FFolderID:= aFolderID;
    ed_Name_.Text:= dmCompany.GetNewName;

    ShowModal;
    Result:=FID;

    Free;
  end;
end;

//--------------------------------------------------------------------
procedure Tdlg_Company_add.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  SetActionName (STR_DLG_ADD_COMPANY);
  lb_Name.Caption:= STR_NAME;

  SetDefaultSize();

end;

//-------------------------------------------------------------------
procedure Tdlg_Company_add.Append;
//-------------------------------------------------------------------
var rec: TdmCompanyAddRec;
begin
  inherited;

  rec.Name:=ed_Name_.Text;
  rec.FolderID:=FFolderID;

  FID:= dmCompany.Add (rec);
end;

//-------------------------------------------------------------------
procedure Tdlg_Company_add.act_OkExecute(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;
  
  Append();
end;


end.