unit dm_act_Company;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs,  Menus, ActnList,

  dm_User_Security,

  dm_act_Base,
  I_Object,

  dm_Company,
  dm_Folder,

  d_Company_add,

  u_func,
//  u_func_msg,
  u_dlg,

  u_const,
//  u_const_msg,
  u_const_db,

  u_types

  ;

  //, dm_act_Folder;

type
  TdmAct_Company = class(TdmAct_Base)
    procedure DataModuleCreate(Sender: TObject);
  private
    function CheckActionsEnable: Boolean;

  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    procedure DoAction (Sender: TObject);

    function ItemDel(aID: integer; aName: string = ''): boolean; override;
    function  ItemAdd (aFolderID: integer): integer; override;

//    function  LoadFromXML (aFileName: string; aFocusedID: Integer): boolean; override;
//    procedure SaveToXML (aIntArray: TIntArray; aFileName: string); override;

//    procedure Clone (aID: integer); override;
  public
    class procedure Init;

  end;

var
  dmAct_Company: TdmAct_Company;

//====================================================================
implementation {$R *.dfm}
//====================================================================

//--------------------------------------------------------------------
class procedure TdmAct_Company.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_Company) then
    dmAct_Company:=TdmAct_Company.Create(Application)
  else
    raise Exception.Create('class procedure TdmAct_Company.Init;');
end;


//--------------------------------------------------------------------
procedure TdmAct_Company.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_COMPANY;
end;

//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
{ TODO : fix }

//--------------------------------------------------------------------
procedure TdmAct_Company.DoAction (Sender: TObject);
//--------------------------------------------------------------------
begin
//  if Sender=act_Add    then Add (FFocusedID) else
//  if Sender=act_Del    then Del (FFocusedID) else

end;

//--------------------------------------------------------------------
function TdmAct_Company.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_Company_add.ExecDlg (aFolderID);
end;


//--------------------------------------------------------------------
function TdmAct_Company.ItemDel(aID: integer; aName: string = ''): boolean;
//--------------------------------------------------------------------
begin
  Result:=dmCompany.Del (aID);
end;



// ---------------------------------------------------------------
function TdmAct_Company.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
  Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];

  SetActionsEnabled1( [
          act_Del_,
          act_Del_list

      ],

   Result );

  //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );

end;


//--------------------------------------------------------------------
procedure TdmAct_Company.GetPopupMenu;
//--------------------------------------------------------------------
var
  bEnable: Boolean;
begin
  bEnable:=CheckActionsEnable();


  case aMenuType of
    mtFolder: begin
                AddFolderMenu_Create (aPopupMenu, bEnable);
//                AddMenuItem (aPopupMenu, act_XmlImport);
                AddFolderPopupMenu(aPopupMenu, bEnable);
              end;

    mtItem:   begin
//                AddMenuItem (aPopupMenu, act_Clone);
//                AddMenuItem (aPopupMenu, act_XmlExport);
                AddMenuItem (aPopupMenu, act_Del_);
                AddMenuItem (aPopupMenu, nil);

                AddFolderMenu_Tools (aPopupMenu);
              end;
    mtList:   begin
//                AddMenuItem (aPopupMenu, act_XmlExport);
                AddMenuItem (aPopupMenu, act_Del_list);
                AddFolderMenu_Tools (aPopupMenu);
              end;
  end;

end;


begin
 
end.



{
//--------------------------------------------------------------------
function  TdmAct_Company.LoadFromXML (aFileName: string; aFocusedID: Integer): boolean;
//--------------------------------------------------------------------
begin
//  Result:= dmCompany.LoadFromXML (aFileName, aFocusedID);
end;

//--------------------------------------------------------------------
procedure TdmAct_Company.SaveToXML (aIntArray: TIntArray; aFileName: string);
//--------------------------------------------------------------------
begin
//  dmTrxType.SaveToXML (aIntArray, aFileName);
end;
}


{//--------------------------------------------------------------------
procedure TdmAct_Company.Clone (aID: integer);
//--------------------------------------------------------------------
var
  iId: Integer;
begin}
(*  with dmCompany do
    if ConfirmDlg('������� �����?') then begin
      iId:= GetFolderID(FFocusedID);
      if Clone (FFocusedID)>0 then
        SH_PostUpdateNodeChildren (IIF(iId=0, GUID_TRX_TYPE, dmFolder.GetGuidByID(iId)));
    end;*)

// end;
