unit dm_Company;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms, Windows, Dialogs, Controls,

  dm_Main,
           
 // u_xml,
  u_func,
  u_db,

  u_types,

  u_const_str,
  u_const_db,

  dm_Object_base
  ;


type
  TdmCompanyAddRec = record
    name             : string;
    FolderID         : integer;
    address          ,
  //  INN              ,
  //  KPP              ,
 //   phone            ,
//    fax              ,
 //   E_Mail           ,
 //   bank_account     ,
  //  bank_name        ,
  //  bank_kor_account ,
  //  bank_BIK         ,
  //  contact_man      ,
  //  director_general ,
  //  main_book_keeper ,
    comment          : string;
  end;


  TdmCompany = class(TdmObject_base)
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    function  Add (aRec: TdmCompanyAddRec): integer;
    procedure Update(aID: Integer; aRec: TdmCompanyAddRec);
 //   function Find (aName: string): integer;

    function Get (aID: integer; var aRec: TdmCompanyAddRec): boolean;

    function  LoadFromXML (aFileName: string; aFolderID: Integer): boolean;
    procedure SaveToXML (aIDArray: TIntArray; aFileName: string);

    function  Clone (aID: Integer): Integer;
  end;

var
  FdmCompany: TdmCompany;

function  dmCompany: TdmCompany;

//========================================================
implementation    {$R *.DFM}
//========================================================

{const
  FLD_fax              ='fax';
  FLD_E_Mail           ='E_Mail';
  FLD_bank_account     ='bank_account';
  FLD_bank_name        ='bank_name';
  FLD_bank_kor_account ='bank_kor_account';
  FLD_bank_BIK         ='bank_BIK';
  FLD_contact_man      ='contact_man';
  FLD_director_general ='director_general';
  FLD_main_book_keeper ='main_book_keeper';

}
function  dmCompany: TdmCompany;
begin
  if not Assigned(FdmCompany) then
    FdmCompany:= TdmCompany.Create(Application);

  Result:= FdmCompany;
end;


procedure TdmCompany.DataModuleCreate(Sender: TObject);
begin
  inherited;
  TableName  := TBL_TCOMPANY;
  DisplayName:= STR_COMPANY;
  ObjectName := OBJ_COMPANY;
end;


//--------------------------------------------------------------------
function TdmCompany.Add (aRec: TdmCompanyAddRec): integer;
//--------------------------------------------------------------------
begin
  with aRec do
    Result:= gl_DB.AddRecordID (TableName,
                  [db_Par(FLD_name             , name            ),
                   db_Par(FLD_folder_id        , IIF_NULL(FolderID)),
                //   db_Par(FLD_address          , address         ),
                  // db_Par(FLD_INN              , INN             ),
                 //  db_Par(FLD_KPP              , KPP             ),
//                   db_Par(FLD_phone            , phone           ),
                //   db_Par(FLD_fax              , fax             ),
             //      db_Par(FLD_E_Mail           , E_Mail          ),
             //      db_Par(FLD_bank_account     , bank_account    ),
              //     db_Par(FLD_bank_name        , bank_name       ),
               //    db_Par(FLD_bank_kor_account , bank_kor_account),
                //   db_Par(FLD_bank_BIK         , bank_BIK        ),
                 //  db_Par(FLD_contact_man      , contact_man     ),
                  // db_Par(FLD_director_general , director_general),
                   //db_Par(FLD_main_book_keeper , main_book_keeper),
                   db_Par(FLD_comment          , comment         )
                   ] );
end;

//--------------------------------------------------------------------
procedure TdmCompany.Update(aID: Integer; aRec: TdmCompanyAddRec);
//--------------------------------------------------------------------
begin
  with aRec do
    gl_DB.UpdateRecord (TableName, aID,
                  [db_Par(FLD_name             , name            ),
                   db_Par(FLD_folder_id        , IIF_NULL(FolderID)),
                   db_Par(FLD_address          , address         ),
                //   db_Par(FLD_INN              , INN             ),
                 //  db_Par(FLD_KPP              , KPP             ),
             //      db_Par(FLD_phone            , phone           ),
//                   db_Par(FLD_fax              , fax             ),
//                   db_Par(FLD_E_Mail           , E_Mail          ),
//                   db_Par(FLD_bank_account     , bank_account    ),
//                   db_Par(FLD_bank_name        , bank_name       ),
//                   db_Par(FLD_bank_kor_account , bank_kor_account),
//                   db_Par(FLD_bank_BIK         , bank_BIK        ),
//                   db_Par(FLD_contact_man      , contact_man     ),
//                   db_Par(FLD_director_general , director_general),
//                   db_Par(FLD_main_book_keeper , main_book_keeper),

                 db_Par(FLD_comment          , comment         )
                   ] );
end;

//--------------------------------------------------------------------
function TdmCompany.Get (aID: integer; var aRec: TdmCompanyAddRec): boolean;
//--------------------------------------------------------------------
begin
  FillChar(aRec, SizeOf(aRec), 0);

  db_OpenTableByID (gl_DB.Query, TableName, aID);
  Result:=not gl_DB.Query.IsEmpty;

  if Result then  with gl_DB.Query do
  begin
    aRec.name            :=FieldBYName(FLD_name            ).AsString;
    aRec.FolderID        :=FieldBYName(FLD_folder_id       ).AsInteger;

//    aRec.address         :=FieldBYName(FLD_address         ).AsString;
//  //  aRec.INN             :=FieldBYName(FLD_INN             ).AsString;
//  //  aRec.KPP             :=FieldBYName(FLD_KPP             ).AsString;
//    aRec.phone           :=FieldBYName(FLD_phone           ).AsString;
//    aRec.fax             :=FieldBYName(FLD_fax             ).AsString;
//    aRec.E_Mail          :=FieldBYName(FLD_E_Mail          ).AsString;
//    aRec.bank_account    :=FieldBYName(FLD_bank_account    ).AsString;
//    aRec.bank_name       :=FieldBYName(FLD_bank_name       ).AsString;
//    aRec.bank_kor_account:=FieldBYName(FLD_bank_kor_account).AsString;
//    aRec.bank_BIK        :=FieldBYName(FLD_bank_BIK        ).AsString;
//    aRec.contact_man     :=FieldBYName(FLD_contact_man     ).AsString;
//    aRec.director_general:=FieldBYName(FLD_director_general).AsString;
//    aRec.main_book_keeper:=FieldBYName(FLD_main_book_keeper).AsString;
    aRec.comment         :=FieldBYName(FLD_comment         ).AsString;
  end;
end;



//--------------------------------------------------------------------
function TdmCompany.LoadFromXML (aFileName: string; aFolderID: Integer): boolean;
//--------------------------------------------------------------------
var i, iID: integer;
   vRoot, vNode: Variant;
   rec: TdmCompanyAddRec;
   wMsgRes: word;
begin
 { Result:= false;

  gl_XMLDoc.LoadFromFile (aFileName);
  vRoot:= gl_XMLDoc.DocumentElement;
  vNode:= xml_GetNodeByPath(vRoot, [TAG_ITEM]);

  if not VarExists(vNode) then
    Exit;

  for i:=0 to vRoot.ChildNodes.Length-1 do
  begin
    vNode:= vRoot.ChildNodes.Item(i);

    FillChar(rec, SizeOf(rec), 0);

    rec.FolderID    := aFolderID;
    rec.Name        := xml_GetAttr (vNode, FLD_NAME);


    iID:= FindByName(rec.Name);

    if iID>0 then begin
      wMsgRes:= MessageDlg ('������ '+rec.Name+' ��� ����������, ��������?',
                          mtConfirmation, [mbYes,mbNo], 0);

      if wMsgRes=mrYes then
        Update(iID, rec)
      else
      if wMsgRes=mrNo then begin
        rec.Name:= GetNewName(rec.Name);
        Add(rec);
      end;
    end else
      Add(rec);
  end;

  Result:= true;}
end;

//--------------------------------------------------------------------
procedure TdmCompany.SaveToXML (aIDArray: TIntArray; aFileName: string);
//--------------------------------------------------------------------
var
  I: Integer;
begin
 { gl_XMLDoc.Clear;

  db_OpenQuery (qry_Temp,
      Format('SELECT * FROM %s WHERE id in (%s)',
            [TableName, IntArrayToStringSimple(aIDArray, ',')]));

  with qry_Temp do
    while not EOF do
  begin
    xml_AddNode (gl_XMLDoc.DocumentElement, TAG_ITEM,
        [xml_Par (FLD_NAME            ,  FieldByName(FLD_NAME).AsString),
         xml_Par (FLD_FOLDER_ID       ,  FieldByName(FLD_FOLDER_ID).AsFloat),
//                 xml_Par (FLD_FOLDER_NAME     ,  FieldValues[FLD_FOLDER_NAME],
         xml_Par (FLD_POWER           ,  FieldByName(FLD_POWER).AsFloat),
         xml_Par (FLD_FREQ            ,  FieldByName(FLD_FREQ).AsFloat),
         xml_Par (FLD_SENSE           ,  FieldByName(FLD_SENSE).AsFloat)
        ]);

    Next;
  end;

  gl_XMLDoc.SaveToFile (aFileName);}
end;


//--------------------------------------------------------------------
function TdmCompany.Clone (aID: Integer): Integer;
//--------------------------------------------------------------------
var
  sMainFlds: string;
  sNewName: string;
  iResult: Integer;

  //------------------------------------
  function  DoCopyMain (aFields, aNewName: string): boolean;
  var
    sSql: string;
  begin
    sSql:= Format(
      ' INSERT INTO %s ([name],%s) SELECT ''%s'',%s FROM %s WHERE id=%d   '
      , [TableName, aFields, aNewName, aFields, TableName, aID]);

    Result:= gl_Db.ExecCommand(sSql);

    iResult:= gl_DB.GetIdentCurrent(TableName);
  end;

begin
  iResult:= -1;

{  sNewName:= GetNewName();//)(GetNameByID(aID)+ ' (�����) ');

  gl_DB.BeginTrans;

  sMainFlds:= gl_DB.GetFieldsStr_Tbl(TableName,
       [FLD_ID, FLD_GUID, FLD_NAME, FLD_DATE_CREATED, FLD_DATE_MODIFY,
        FLD_USER_CREATED_ID, FLD_USER_MODIFY_ID,
        FLD_USER_CREATE, FLD_USER_MODIFY]);

  CursorSql;

  if DoCopyMain(sMainFlds, sNewName) then begin
    gl_DB.CommitTrans;
    Result:= iResult;
  end else
    gl_DB.RollbackTrans;
}

  CursorRestore;
end;



end.

