unit u_LinkFreqPlan_classes;

interface

uses DB, Classes, SysUtils, Dialogs,

   u_antenna_mask,
   //u_LinkFreqPlan_classes,

   u_files,

   u_Func_arrays,

   u_geo,
   u_func,

   u_Link_const,
   u_LinkFreqPlan_const,
   u_const_db;


(*const
  FLD_MaxAntennaHeight = 'MaxAntennaHeight';
*)


type
//  TnbAntennaRefList = class;
  TnbAntennaList= class;
  TnbLinkFreqPlan_Linkend= class;
  TnbNeighborRefList= class;
  TnbLinkFreqPlan_Linkend_List= class;
  TnbNeighborList= class;


  TnbPolarization = (ptNone,ptv,ptH,ptX);


  //-------------------------------------------------------------------
  TnbResource = class(TCollectionItem)
  //-------------------------------------------------------------------
  public
    Band : string;
    FREQ_BAND_ALLOW_Mhz : String; //������ ������
    FREQ_BAND_FORBIDDEN_Mhz: string; //������ ������

  end;


  // ---------------------------------------------------------------
  TnbResourceList = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TnbResource;
  public
    constructor Create;

    function AddItem: TnbResource;
    function FindByBand(aBand: string): TnbResource;

    procedure LoadFromDataset(aDataset: TDataset);

    property Items[Index: Integer]: TnbResource read GetItems; default;
  end;



  //-------------------------------------------------------------------
  TnbAntenna = class(TCollectionItem)
  //-------------------------------------------------------------------
  private
    FPolarization: TnbPolarization;

    FVert_Width: double;
    FHorz_Width: double;
    FFreq_MHz: double;


  public
    Polarization_str: string;

    Linkend_id : Integer;
    // -------------------------

    Gain           :  double;
    Diameter          :  double;


    Polarization_ratio: double;

    Loss : Double;

    Azimuth : double;

 //   VertMaskStr: string;
  //  HorzMaskStr: string;

    Vert_width  : Double;


    Height  : Double;

    BLPoint : TBLPoint;

  public
    HorzMask: TAntennaMask;
    VertMask: TAntennaMask;

//    HorzMaskArr,
//    VertMaskArr: TdmAntTypeMaskArray;

  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;

    procedure LoadFromDataset(aDataset: TDataset);

    property Polarization: TnbPolarization read FPolarization;

  end;

  // ---------------------------------------------------------------
  TnbNeighbor = class(TCollectionItem)
  // ---------------------------------------------------------------
  private
//    function GetDistanse_m: Double;
  public
    ID : Integer;

    Distanse_m: Double;

    LINKEND1: TnbLinkFreqPlan_Linkend;
    LINKEND2: TnbLinkFreqPlan_Linkend;

    RX_for_LINKEND2: TnbLinkFreqPlan_Linkend;


//    TX_LINKEND_ID : Integer;
//    RX_LINKEND_ID : Integer;

    LINKEND1_ID : Integer;
    LINKEND2_ID : Integer;

    // -------------------------

    PROPERTY1_ID  : Integer;
    PROPERTY2_ID  : Integer;

//    TX_PROPERTY_ID  : Integer;
//    RX_PROPERTY_ID  : Integer;

//    TX_PROPERTY_name  : string;
//    RX_PROPERTY_name  : string;

    PROPERTY1_name  : string;
    PROPERTY2_name  : string;


    LINKEND1_name  : string;
    LINKEND2_name  : string;


    // -------------------------

////    RX_LINK_ID : Integer;

  //zzzzzz    bandwidth1: Double;
    Rx_Mode__bandwidth: Double;
    Rx_LinkEndType__coeff_noise  : Double;


    IsLink        : Boolean;
    IsPropertyOne : Boolean;


  //  POWER_dBm        : Double;
    DIAGRAM_LEVEL: Double;
    EXTRA_LOSS   : Double;

    IsNeigh1 : Boolean;
    CI : Double;

    POWER_NOISE : Double;
    FREQ_SPACING_MHz : Double;

    Threshold_degradation_dB: Double;

    CIA: variant;
    CA: variant;
    CI_: variant;

    // -------------------------
    
    RX_LEVEL_DBM: Double; //,  eRx_Level_dBm), //POWER_dBm
    SESR: Double;
    Kng: Double;


//    Diagram_level: Double;


    procedure LoadFromDataset(aDataset: TDataset);

//    property Distanse_m: Double read GetDistanse_m;
  end;

  // ---------------------------------------------------------------
  TLinkFreqPlan = class
  // ---------------------------------------------------------------
  public
    Linkends: TnbLinkFreqPlan_Linkend_List;
    Neighbors: TnbNeighborList;

  public

    Id                  : Integer;
  //  Name                : String;

    LINKNET_ID          : Integer;

    MaxCalcDistance_km  : Double;
    Polarization_Level_dB: Double;
    SectorFreqSpacing   : Integer;
    SiteFreqSpacing     : Integer;
    MIN_CI_dB           : Double;
    StartSectorNum      : Integer;
    PereborQueueType    : Integer;
    FreqDistribType     : Integer;
    MaxIterCount        : Integer;
    PereborOrderType    : Integer;
    MaxFreqRepeat       : Integer;
    Interference_ChannelsKind: string;
    Threshold_degradation_db: Double;

    constructor Create;
    destructor Destroy; override;

    procedure LoadFromDataset(aDataset: TDataset);
  end;


  TnbLinkFreqPlan_Linkend_Get_Info = record

    Distr_TX_arr,
    Distr_RX_arr,

    Fixed_TX_arr,
    Fixed_RX_arr  : TStrArray;


  end;


  TLinkendChannelType = (ctLow,ctHigh);

  // ---------------------------------------------------------------
  TnbLinkFreqPlan_Linkend = class(TCollectionItem)
  // ---------------------------------------------------------------

  public


    id : Integer;

    Name : String;


    KRATNOST_BY_SPACE  : Integer;

    KRATNOST_BY_FREQ   : Integer;
    FREQ_CHANNEL_COUNT : Integer;


    Linkend_id : Integer;
    THRESHOLD_DEGRADATION : Double;

    Next_Linkend_id : Integer;

    Link_id : Integer;
    Link_Name : string;
    ClutterModel_ID : Integer;


//    MaxAntennaHeight : double;

    LINKENDTYPE_ID: Integer;

    THRESHOLD_BER_6    : double;
    THRESHOLD_BER_3    : double;
    Power_dBm          : double;

//    Rx_level_dBm        : double;



    TX_FREQ_MHz        : Double;

    BitRate_Mbps       : double;  //BitRate_Mbps

    BAND               : string;

    LOSS          : Double;
    KNG                : Double;


    Freq_Spacing_MHz   : Double;



    SIGNATURE_WIDTH_MHz : double;// '������ ���������, ���');
    SIGNATURE_HEIGHT_dB: double;// '������ ���������, ��');
    MODULATION_COUNT: double;// '����� ������� ���������');
//    EQUALISER_PROFIT: double;// '�������, �������� ������������, ��');

    // -------------------------
    // band
    // -------------------------

    // -------------------------
    // LinkEndType
    // -------------------------

    LinkEndType_name : string;

    LinkEndType: record
      level_tx_a  : Integer;
      level_rx_a  : Integer;

//      FREQ_MAX_HIGH : double;
 //     FREQ_MIN_LOW  : double;

    end;

  //  Mode__bandwidth: Double;
   // LinkEndType__
    coeff_noise  : Double;


    LinkEndType_band: record
 //     level_tx_a  : Integer;
  //    level_rx_a  : Integer;

      FREQ_MAX_HIGH : double;
      FREQ_MIN_LOW  : double;

    end;

   FREQ_ALLOW    : string;
   FREQ_FORBIDDEN: string;

   FREQ_FIXED: string;
   freq_distributed: string;

   FreqReqCount:  Integer;
//   = FieldByName(FLD_FREQ_REQ_COUNT).AsInteger;

   ChShift1 : integer;


//    Channel_number: integer;


    TxRx_Shift: double;
    ChannelWidth: double;
    FreqMinLow_MHz: double;
    ChannelCount: integer;

//    CHANNEL_TYPE_FIXED,
  //  ChannelFixedType,
    ChannelType       : TLinkendChannelType; //(ctLow,ctHigh);

    ChannelTypeStr  : string;

//    CHANNEL_TYPE_FIXED_Str  : string;

    // -------------------------
    // Property
    // -------------------------
    PROPERTY_ID : Integer;
    PROPERTY_GROUND_HEIGHT : double;

    BLPoint    : TBLPoint;
//    PROPERTY_BLPoint   : TBLPoint;

    PROPERTY_name : string;


    // -------------------------
    // MODE
    // -------------------------
    ch_spacing: Double;
    BANDWIDTH : Double;

    BANDWIDTH_TX_30 : Double;
    BANDWIDTH_TX_3  : Double;
    BANDWIDTH_TX_A  : Double;

    BANDWIDTH_RX_30 : Double;
    BANDWIDTH_RX_3  : Double;
    BANDWIDTH_RX_A  : Double;


    Antenna: record
        Polarization_str   : string;
        Polarization_ratio : double;

        Gain               :  double;
        Diameter           :  double;


        Loss               : Double;

        Azimuth            : double;

        Vert_width         : Double;

        Height             : Double;
    end;
     //   VertMaskStr: string;
      //  HorzMaskStr: string;




  public
    Antennas: TnbAntennaList;

  public
//    AntennaRefList: TnbAntennaRefList;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;


    function ChannelsAsArray(aName: string): TIntArray;


  //  procedure Validate_____;


    procedure ChannelNumberToFreqMHz(aChannelNumber: integer; var aTxFreq, aRxFreq:
        double);

    function ChannelsToFreqMHz(aChannels: string; var aTxArr, aRxArr:
      TDoubleArray): Integer;

    function Get: TnbLinkFreqPlan_Linkend_Get_Info;

    procedure LoadFromDataset(aDataset: TDataset);
    procedure SaveToDebugTxt(aFileName: string);
  end;

  // ---------------------------------------------------------------
  TnbLinkFreqPlan_Linkend_List = class(TCollection)
  // ---------------------------------------------------------------
  private
    function AddItem: TnbLinkFreqPlan_Linkend;
    function GetItems(Index: Integer): TnbLinkFreqPlan_Linkend;
    function Validate22: Boolean;
  public
    constructor Create;

    function IsValid(aID: Integer; aDataType: string = 'distr'): Boolean;


    function FindByID1(aID: Integer): TnbLinkFreqPlan_Linkend;
    function FindByLinkendID(aID: Integer): TnbLinkFreqPlan_Linkend;

    procedure LoadFromDataset_Antennas(aDataset: TDataSet);
    procedure LoadFromDataset(aDataset: TDataset);

    procedure Show;


    function Validate: Boolean;

    property Items[Index: Integer]: TnbLinkFreqPlan_Linkend read GetItems; default;
  end;


  // ---------------------------------------------------------------
  TnbNeighborList = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TnbNeighbor;
    procedure SelectBy_RX_LINK_ID(aRX_LINK_ID : Integer);
  public
    Selection: TnbNeighborRefList;

    constructor Create;
    destructor Destroy; override;

    procedure LoadFromDataset(aDataset: TDataset);
    function AddItem: TnbNeighbor;
    function FindIByID(aID: Integer): TnbNeighbor;
    function FindItemByLinkendID_IsLink(aLinkend_ID: Integer): TnbNeighbor;

//    function FindItemByLinkendID(aLinkend_ID: Integer): TnbNeighbor;
    procedure SelectByRxLinkendID_and_not_IsLink(aLinkend_ID: Integer);
    procedure test;
//    procedure SelectBy_Rx_LinkendID(aLinkend_ID: Integer);


    property Items[Index: Integer]: TnbNeighbor read GetItems; default;
  end;


  // ---------------------------------------------------------------
  TnbNeighborRefList = class(TList)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TnbNeighbor;
  public
    function FindIByID(aID: Integer): TnbNeighbor;

    procedure Show;

    property Items[Index: Integer]: TnbNeighbor read GetItems; default;
  end;


  // ---------------------------------------------------------------
  TnbAntennaList = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TnbAntenna;
  public
    constructor Create;
    function AddItem: TnbAntenna;
    procedure LoadFromDataset(aDataset: TDataset);

    property Items[Index: Integer]: TnbAntenna read GetItems; default;
  end;


  // ---------------------------------------------------------------
  TnbData = class
  // ---------------------------------------------------------------
  public
    LinkFreqPlan: TLinkFreqPlan;
    Linkends: TnbLinkFreqPlan_Linkend_List;
    Neighbors: TnbNeighborList;

    Resources: TnbResourceList;

    constructor Create;
    destructor Destroy; override;

  end;




implementation

{

function TnbNeighbor.GetDistanse_m: Double;
begin
   Assert (Assigned(RX_for_LINKEND2));
   Assert (Assigned(LINKEND2));

  Result := geo_Distance_m(RX_for_LINKEND2.BLPoint, LINKEND2.BLPoint);


//--  if (TX_LINKEND_ID=3378) and (RX_LINKEND_ID=5119) then
//      assert (result>0);

  // TODO -cMM: TnbNeighbor.GetDistanse_m default body inserted
//  Result := ;
end;
}

// ---------------------------------------------------------------
procedure TnbNeighbor.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
begin
  with aDataset do
  begin
    ID:=FieldByName(FLD_ID).AsInteger;

    LINKEND1_ID:=FieldByName(FLD_LINKEND1_ID).AsInteger;
    LINKEND2_ID:=FieldByName(FLD_LINKEND2_ID).AsInteger;


    LINKEND1_name :=FieldByName(FLD_TX_LINKEND_NAME).AsString;
    LINKEND2_name :=FieldByName(FLD_RX_LINKEND_NAME).AsString;


    //PROPERTY1_ID:=FieldByName(FLD_PROPERTY1_ID).AsInteger;
//    PROPERTY2_ID:=FieldByName(FLD_PROPERTY2_ID).AsInteger;

 //   TX_PROPERTY_name:=FieldByName(FLD_TX_PROPERTY_name).AsString;
  //  RX_PROPERTY_name:=FieldByName(FLD_RX_PROPERTY_name).AsString;


  /////  RX_LINK_ID:=FieldByName(FLD_RX_LINK_ID).AsInteger;

    IsLink       :=FieldByName(FLD_IsLink).AsBoolean;
    IsPropertyOne:=FieldByName(FLD_IsPropertyOne).AsBoolean;

    IsNeigh1  :=FieldByName(FLD_IsNeigh).AsBoolean;

//////////    bandwidth1  :=FieldByName(FLD_bandwidth).AsFloat;


    Rx_LinkEndType__coeff_noise:=FieldByName(FLD_coeff_noise).AsFloat;
    Rx_Mode__bandwidth  :=FieldByName(FLD_bandwidth).AsFloat;

    RX_LEVEL_DBM :=FieldByName(FLD_Rx_Level_dBm).AsFloat;
    DIAGRAM_LEVEL:=FieldByName(FLD_DIAGRAM_LEVEL).AsFloat;
    EXTRA_LOSS   :=FieldByName(FLD_EXTRA_LOSS).AsFloat;


    FREQ_SPACING_MHz   :=FieldByName(FLD_FREQ_SPACING_MHz).AsFloat;

  end;
end;


constructor TnbLinkFreqPlan_Linkend.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  Antennas := TnbAntennaList.Create();
end;

destructor TnbLinkFreqPlan_Linkend.Destroy;
begin
  FreeAndNil(Antennas);
  inherited Destroy;
end;



//-------------------------------------------------------------------
function TnbLinkFreqPlan_Linkend.Get: TnbLinkFreqPlan_Linkend_Get_Info;
//-------------------------------------------------------------------
var


   // iChTypeDistr, iChTypeFixed,
 // iChCount1: integer;
//  iChCount: integer;
 // iLinkEndID: integer;

//  sCHANNEL_TYPE : string;

//  dChannelWidth1, dFreqMin_MHZ1, dTxRx_Shift1: double;

//  dChannelWidth, dFreqMinLow_MHZ, dTxRx_Shift: double;
  dDistrTxFreq, dDistrRxFreq, dFixedTxFreq, dFixedRxFreq: double;
 // i: Integer;



    // ---------------------------------------------------------------
    procedure DoCalcFreqs(aChannelNumber: integer; aLowOrHigh: string; var aTxFreq, aRxFreq:
        double);
    // ---------------------------------------------------------------
//    procedure DoCalcFreqs(aChannelNumber, aChType: integer; var aTxFreq, aRxFreq: double);
    begin
      Assert(aLowOrHigh<>'' );

    //  if aLowOrHigh='' then
     //   exit;


  //    Assert(ChannelWidth>0, 'ChannelWidth > 0');
//      Assert(TxRx_Shift>0,   'TxRx_Shift   > 0');

      if (ChannelWidth=0) or (TxRx_Shift=0) then
      begin
        aTxFreq:=0;
        aRxFreq:=0;
        
        exit;
      end;





      if Eq(aLowOrHigh,'low') then
      begin  //low
        aTxFreq:= FreqMinLow_MHZ + ChannelWidth * (aChannelNumber - 1);
        aRxFreq:= aTxFreq + TxRx_Shift;
      end else
      begin  //high
        aRxFreq:= FreqMinLow_MHZ + ChannelWidth * (aChannelNumber - 1);
        aTxFreq:= aRxFreq + TxRx_Shift;   //!!!
      end;


  (*    case aChType of
          0: begin  //low
               aTxFreq:= dFreqMinLow_MHZ + dChannelWidth*(aChannelNumber - 1);
               aRxFreq:= aTxFreq + dTxRx_Shift;
             end;
          1: begin  //high
               aRxFreq:= dFreqMinLow_MHZ + dChannelWidth*(aChannelNumber - 1);
               aTxFreq:= aRxFreq + dTxRx_Shift;
             end;
      end;
*)
    end;    //

var
  iCount: Integer;
  iID: Integer;
  oLinkend: TnbLinkFreqPlan_Linkend;


  arrDistr, arrFixed: TIntArray;

  arrDistr_TX_str,
  arrDistr_RX_str,
  arrFixed_TX_str,
  arrFixed_RX_str  : TStrArray;
  i: Integer;


begin

  if ChannelTypeStr='' then
    exit;



 // with aDataset do
  begin
  //  sCHANNEL_TYPE:=ChannelTypeStr;
//    FieldByName(FLD_CHANNEL_TYPE).AsString;

 //   if sCHANNEL_TYPE='' then
   //   Exit;


    arrDistr := StringToIntArray(FREQ_DISTRIBUTED);
    arrFixed := StringToIntArray(FREQ_FIXED);


//    sChTypeDistr:= FieldByName(FLD_CHANNEL_TYPE_DISTRIBUTED).AsString;
//    sChTypeFixed:= FieldByName(FLD_CHANNEL_TYPE_FIXED).AsString;





(*
  iChTypeDistr:= IIF(FieldByName(FLD_CH_TYPE_DISTRIBUTED).AsString='Low', 0, 1);
    iChTypeFixed:= IIF(FieldByName(FLD_CH_TYPE_FIXED).AsString='Low', 0, 1);
*)

{
    iLinkEndID:= FieldByName(FLD_LINKEND_ID).AsInteger;



    dChannelWidth  := FieldByName(FLD_CHANNEL_WIDTH).AsFloat;
    dTxRx_Shift    := FieldByName(FLD_TXRX_SHIFT).AsFloat;
    dFreqMinLow_MHZ:= FieldByName(FLD_FREQ_MIN_LOW).AsFloat;
    iChCount       := FieldByName(FLD_CHANNEL_COUNT).AsInteger;
 }

 {
    dChannelWidth  := ChannelWidth;
    dTxRx_Shift    := TxRx_Shift;
    dFreqMinLow_MHZ:= FreqMinLow_MHZ;
    iChCount       := ChannelCount;
  }


    {
    dmLinkEnd_ex.GetBand_Params(iLinkEndID,
      dTxRx_Shift, dChannelWidth, dFreqMinLow_MHZ, iChCount, OBJ_LINKEND);

     Assert(dTxRx_Shift=dTxRx_Shift1);
     Assert(dChannelWidth=dChannelWidth1);
     Assert(dFreqMinLow_MHZ=dFreqMin_MHZ1);
     Assert(iChCount=iChCount1);

    }

    if (Length(arrDistr)=0) and (Length(arrFixed)=0) then
      exit;

    iCount:=Length(arrDistr);

    if iCount>0 then
    begin
      SetLength(arrDistr_TX_str, iCount);
      SetLength(arrDistr_RX_str, iCount);



      for i:=0 to iCount-1 do
      begin
        DoCalcFreqs(arrDistr[i], ChannelTypeStr, dDistrTxFreq, dDistrRxFreq);

        arrDistr_TX_str[i]:= AsString(dDistrTxFreq);
        arrDistr_RX_str[i]:= AsString(dDistrRxFreq);
      end;

//      FieldByName(FLD_FREQ_DISTRIBUTED_TX_STR).Value := StrArrayToString(arrDistr_TX_str);
//      FieldByName(FLD_FREQ_DISTRIBUTED_RX_STR).Value := StrArrayToString(arrDistr_RX_str);

    end;


    iCount:=Length(arrFixed);

    if iCount>0 then
    begin
      SetLength(arrFixed_TX_str, iCount);
      SetLength(arrFixed_RX_str, iCount);


      for i:=0 to iCount-1 do
      begin
        DoCalcFreqs(arrFixed[i], ChannelTypeStr, dFixedTxFreq, dFixedRxFreq);

        arrFixed_TX_str[i]:= AsString(dFixedTxFreq);
        arrFixed_RX_str[i]:= AsString(dFixedRxFreq);

      end;



    //  FieldByName(FLD_FREQ_FIXED_TX_STR).Value := StrArrayToString(arrFixed_TX_str);
    //  FieldByName(FLD_FREQ_FIXED_RX_STR).Value := StrArrayToString(arrFixed_RX_str);

    end;

  end;


  Result.Distr_TX_arr:=arrDistr_TX_str;
  Result.Distr_RX_arr:=arrDistr_RX_str;

  Result.Fixed_TX_arr:=arrFixed_TX_str;
  Result.Fixed_RX_arr:=arrFixed_RX_str;


end;






// ---------------------------------------------------------------
procedure TnbLinkFreqPlan_Linkend.ChannelNumberToFreqMHz(aChannelNumber:
    integer; var aTxFreq, aRxFreq: double);
// ---------------------------------------------------------------
//    procedure ChannelNumberToFreqMHz(aChannelNumber, aChType: integer; var aTxFreq, aRxFreq: double);
begin
  Assert(TxRx_Shift <> 0);

  if ChannelType = ctLow then
  begin  //low
    aTxFreq:= FreqMinLow_MHZ + ChannelWidth * (aChannelNumber - 1);
    aRxFreq:= aTxFreq + TxRx_Shift;
  end else
  begin  //high
    aRxFreq:= FreqMinLow_MHZ + ChannelWidth * (aChannelNumber - 1);
    aTxFreq:= aRxFreq + TxRx_Shift;
  end;

end;

// ---------------------------------------------------------------
function TnbLinkFreqPlan_Linkend.ChannelsAsArray(aName: string): TIntArray;
// ---------------------------------------------------------------
begin
  if Eq(aName, 'distr') then
    Result := StringToIntArray(freq_distributed) else

  if Eq(aName, 'allow') then
    Result := StringToIntArray(FREQ_ALLOW) else

  if Eq(aName, 'FIXED') then
    Result := StringToIntArray(FREQ_FIXED)

  else
    raise EClassNotFound.Create('');

end;


// ---------------------------------------------------------------
function TnbLinkFreqPlan_Linkend.ChannelsToFreqMHz(aChannels: string; var
    aTxArr, aRxArr: TDoubleArray): Integer;
// ---------------------------------------------------------------
var
  arr: TIntArray;
  eRxFreq: Double;
  eTxFreq: Double;
  i: Integer;
begin
  arr:=StringToIntArray(aChannels);

  Result:=Length(arr);

  SetLength(aTxArr, Length(arr));
  SetLength(aRxArr, Length(arr));

  for i:=0 to High(arr) do
  begin
    ChannelNumberToFreqMHz(arr[i], eTxFreq, eRxFreq);

    aTxArr[i]:= eTxFreq;
    aRxArr[i]:= eRxFreq;
  end;

end;


// ---------------------------------------------------------------
procedure TnbLinkFreqPlan_Linkend.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
var
  s: string;
begin
//  db_View(aDataset);

  with aDataset do
  begin
 //   channel_number  :=FieldByName(FLD_channel_number).AsInteger;

 {
   Antenna.Polarization_str   := FieldByName('Antenna_Polarization_str').AsString;
   Antenna.Polarization_ratio := FieldByName('Antenna_Polarization_ratio').AsFloat;
   Antenna.Gain               := FieldByName('Antenna_Gain').AsFloat;
   Antenna.Diameter           := FieldByName('Antenna_Diameter').AsFloat;
   Antenna.Loss               := FieldByName('Antenna_Loss').AsFloat;
   Antenna.Azimuth            := FieldByName('Antenna_Azimuth').AsFloat;
   Antenna.Vert_width         := FieldByName('Antenna_Vert_width').AsFloat;
   Antenna.Height             := FieldByName('Antenna_Height').AsFloat;
  }


    id :=FieldByName(FLD_id).AsInteger;

    Self.name :=FieldByName(FLD_LINKEND_NAME).AsString;

    Linkend_id :=FieldByName(FLD_Linkend_id).AsInteger;

    // -------------------------
    // Link
    // -------------------------
    Next_Linkend_id:=FieldByName(FLD_NEXT_Linkend_id).AsInteger;

    Link_id        :=FieldByName(FLD_Link_id).AsInteger;

    Assert (Link_id>0, 'TnbLinkFreqPlan_Linkend.LoadFromDataset(aDataset: TDataset);   Link_id>0');


    Link_Name      :=FieldByName(FLD_Link_Name).AsString;
    ClutterModel_ID:= FieldByName(FLD_CLUTTER_MODEL_ID).AsInteger;

/////////    MaxAntennaHeight:=FieldByName(FLD_MaxAntennaHeight).AsFloat;


    // -------------------------
    // LinkEnd
    // -------------------------
    LINKENDTYPE_ID :=FieldByName(FLD_LINKENDTYPE_ID).AsInteger;
//    LINKENDTYPE_ID :=FieldByName(FLD_LINKENDTYPE_ID).AsInteger;

    BAND              :=FieldByName(FLD_BAND).AsString;
   // TX_FREQ_MHz       :=FieldByName(FLD_TX_FREQ_MHz).AsFloat;


//    Rx_level_dBm      :=FieldByName(FLD_Rx_level_dBm).AsFloat;
    Power_dBm         :=FieldByName(FLD_POWER_dBm).AsFloat;

    Kng               :=FieldByName(FLD_Kng).AsFloat;
    LOSS          :=FieldByName(FLD_LOSS).AsFloat;


    ChannelTypeStr:= FieldByName(FLD_CHANNEL_TYPE).AsString;
    ChannelType  := IIF(Eq(ChannelTypeStr,'low'), ctLow, ctHigh);

/////    s:= FieldByName(FLD_CHANNEL_TYPE_FIXED).AsString;
  //  ChannelFixedType  := IIF(Eq(s,'low'), ctLow, ctHigh);


     FreqReqCount := FieldByName(FLD_FREQ_REQ_COUNT).AsInteger;


//    FieldByName(FLD_CHANNEL_TYPE).AsString;


    Freq_Spacing_MHz  :=FieldByName(FLD_Freq_Spacing).AsFloat;

    KRATNOST_BY_SPACE:=FieldByName(FLD_KRATNOST_BY_SPACE).AsInteger;
    KRATNOST_BY_FREQ :=FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger;

    SIGNATURE_WIDTH_MHz:= FieldByName(FLD_SIGNATURE_WIDTH).AsFloat;//,  'double; //  '������ ���������, ���');
    SIGNATURE_HEIGHT_dB:= FieldByName(FLD_SIGNATURE_HEIGHT).AsFloat;//, 'double; // '������ ���������, ��');
    MODULATION_COUNT   := FieldByName(FLD_MODULATION_COUNT).AsFloat;//,double;// '����� ������� ���������');

  //  EQUALISER_PROFIT:= FieldByName(FLD_EQUALISER_PROFIT).AsFloat;//, 'double; // '�������, �������� ������������, ��');

    // -------------------------
    // LinkFreqPlan_Linkend
    // -------------------------
    FREQ_CHANNEL_COUNT:=FieldByName(FLD_FREQ_CHANNEL_COUNT).AsInteger;

    // -------------------------
    // PROPERTY
    // -------------------------
    PROPERTY_ID       :=FieldByName(FLD_PROPERTY_ID).AsInteger;
    PROPERTY_name     :=FieldByName(FLD_PROPERTY_name).AsString;

    Property_GROUND_HEIGHT := FieldByName(FLD_GROUND_HEIGHT).AsFloat;
//    Property_BLPoint       := db_ExtractBLPoint(aDataset);

//    Property_BLPoint_.B      := FieldByName(FLD_Property_LAT).AsFloat;
//    Property_BLPoint_.L      := FieldByName(FLD_Property_LON).AsFloat;

    BLPoint.B      := FieldByName(FLD_LAT).AsFloat;
    BLPoint.L      := FieldByName(FLD_LON).AsFloat;
//

    // -------------------------

    // band
    // -------------------------
//    FREQ_MAX_HIGH_ := FieldByName(FLD_FREQ_MAX_HIGH).AsFloat;
//    FREQ_MIN_LOW  := FieldByName(FLD_FREQ_MIN_LOW).AsFloat;



    // -------------------------
    // LinkEndType
    // -------------------------
    LinkEndType.level_tx_a:=FieldByName(FLD_level_tx_a).AsInteger;
    LinkEndType.level_rx_a:=FieldByName(FLD_level_rx_a).AsInteger;


    LinkEndType_name:=FieldByName(FLD_LinkEndType_name).AsString;


    coeff_noise:=FieldByName(FLD_coeff_noise).AsFloat;

    //Mode__bandwidth  :=FieldByName(FLD_bandwidth).AsFloat;



    // -------------------------
    // LinkEndType_band
    // -------------------------

    LinkEndType_band.FREQ_MAX_HIGH :=FieldByName(FLD_FREQ_MAX_HIGH).AsFloat;
    LinkEndType_band.FREQ_MIN_LOW  :=FieldByName(FLD_FREQ_MIN_LOW).AsFloat;


    ChannelWidth  := FieldByName(FLD_CHANNEL_WIDTH).AsFloat;
    TxRx_Shift    := FieldByName(FLD_TXRX_SHIFT).AsFloat;
    FreqMinLow_MHz:= FieldByName(FLD_FREQ_MIN_LOW).AsFloat;
    ChannelCount  := FieldByName(FLD_CHANNEL_COUNT).AsInteger;


  //  Assert(ChannelWidth>0, 'ChannelWidth > 0');
//    Assert(TxRx_Shift>0,   'TxRx_Shift   > 0');


    if ChannelWidth>0 then
      ChShift1:=Round( TxRx_Shift /ChannelWidth)  ;



    FREQ_ALLOW     := FieldByName(FLD_FREQ_ALLOW).AsString;
    FREQ_FORBIDDEN := FieldByName(FLD_FREQ_FORBIDDEN).AsString;
    FREQ_FIXED     := FieldByName(FLD_FREQ_FIXED).AsString;  
    freq_distributed:= FieldByName(FLD_freq_distributed).AsString;

//    FREQ_ALLOW_arr     := StringToStrArray(FieldByName(FLD_FREQ_ALLOW).AsString);
//    FREQ_FORBIDDEN_arr := StringToStrArray(FieldByName(FLD_FREQ_FORBIDDEN).AsString);



    // -------------------------
    // MODE
    // -------------------------
    ch_spacing:=FieldByName(FLD_ch_spacing).AsFloat;

    BANDWIDTH :=FieldByName(FLD_BANDWIDTH).AsFloat;

    BANDWIDTH_TX_30 :=FieldByName(FLD_BANDWIDTH_TX_30).AsFloat;
    BANDWIDTH_TX_3  :=FieldByName(FLD_BANDWIDTH_TX_3).AsFloat;
    BANDWIDTH_TX_A  :=FieldByName(FLD_BANDWIDTH_TX_A).AsFloat;


    BANDWIDTH_RX_30 :=FieldByName(FLD_BANDWIDTH_RX_30).AsFloat;
    BANDWIDTH_RX_3  :=FieldByName(FLD_BANDWIDTH_RX_3).AsFloat;
    BANDWIDTH_RX_A  :=FieldByName(FLD_BANDWIDTH_RX_A).AsFloat;

    THRESHOLD_BER_6   :=FieldByName(FLD_THRESHOLD_BER_6).asFloat;
    THRESHOLD_BER_3   :=FieldByName(FLD_THRESHOLD_BER_3).asFloat;

  end;
end;

procedure TnbLinkFreqPlan_Linkend.SaveToDebugTxt(aFileName: string);
begin

end;

{
procedure TnbLinkFreqPlan_Linkend.Validate_____;
var
  I: Integer;
begin
  for I := 0 to Count - 1 do    // Iterate
  begin
//    iLinkID
  end;    // for
end;
}

constructor TnbLinkFreqPlan_Linkend_List.Create;
begin
  inherited Create(TnbLinkFreqPlan_Linkend);
end;

function TnbLinkFreqPlan_Linkend_List.AddItem: TnbLinkFreqPlan_Linkend;
var
  i: Integer;
begin
  Result := TnbLinkFreqPlan_Linkend (inherited Add);

//  i := Result.ID;
end;


// ---------------------------------------------------------------
function TnbLinkFreqPlan_Linkend_List.IsValid(aID: Integer; aDataType: string =
    'distr'): Boolean;
// ---------------------------------------------------------------
var
  oItem: TnbLinkFreqPlan_Linkend;

  arrAllow1: TIntArray;
  arrAllow2: TIntArray;
  I: Integer;

begin
  Result := False;

  if Count=0 then
    exit;

  oItem:=FindByID1(aID);

  Assert(Assigned(oItem));

  arrAllow1:= oItem.ChannelsAsArray ('distr');



//  oItem.

  for I := 0 to Count - 1 do
    if Items[i].ID<> aID then
      if Items[i].PROPERTY_ID = oItem.PROPERTY_ID then
        if Items[i].ChannelType <> oItem.ChannelType then
        begin
          arrAllow2:= Items[i].ChannelsAsArray ('distr');

          if  IntArrayCrossed (arrAllow1, arrAllow2) then
          begin
            Result := False;
            exit;

          end;

       //   Result := Items[i];
        //  Exit;
        end;

  Result := True;

end;

// ---------------------------------------------------------------
function TnbLinkFreqPlan_Linkend_List.FindByID1(aID: Integer):
    TnbLinkFreqPlan_Linkend;
// ---------------------------------------------------------------
var
  I: integer;
begin
  Assert(Count>0, 'function TnbLinkFreqPlan_Linkend_List.FindByID1   - Count>0,');;

  for I := 0 to Count - 1 do
    if Items[i].ID= aID then
    begin
      Result := Items[i];
      Exit;
    end;

  Result := nil;
end;

// ---------------------------------------------------------------
function TnbLinkFreqPlan_Linkend_List.FindByLinkendID(aID: Integer):
    TnbLinkFreqPlan_Linkend;
// ---------------------------------------------------------------
var
  I: integer;
begin
  for I := 0 to Count - 1 do
    if Items[i].Linkend_ID= aID then
    begin
      Result := Items[i];
      Exit;
    end;

  Result := nil;
end;



function TnbLinkFreqPlan_Linkend_List.GetItems(Index: Integer):
    TnbLinkFreqPlan_Linkend;
begin
  Result := TnbLinkFreqPlan_Linkend(inherited Items[Index]);
end;

// ---------------------------------------------------------------
procedure TnbLinkFreqPlan_Linkend_List.LoadFromDataset_Antennas(aDataset:
    TDataSet);
// ---------------------------------------------------------------
var
  I: Integer;
 // iID: Integer;
 // iLinkend_id: Integer;
  k: Integer;
//  oLinkend: TnbLinkFreqPlan_Linkend;
  s: string;

  oAntenna: TnbAntenna;
begin

  for i:=0 to Count-1 do
  begin
    aDataset.Filter:=Format('Linkend_id=%d',[Items[i].Linkend_id])   ;
    aDataset.Filtered:=True;

    k:=aDataset.RecordCount;
    Assert (aDataset.RecordCount>0, 'procedure TnbLinkFreqPlan_Linkend_List.LoadFromDataset_Antennas aDataset.RecordCount>0');

     Items[i].Antennas.AddItem.LoadFromDataset(aDataset);

     Assert (Items[i].Antennas.Count>0, 'Items[i].Antennas.Count>0');

 //    oAntenna:=Items[i].Antennas[0];
  end;

 {
  Exit;


  aDataset.First;

  while not aDataset.EOF do
  begin


   // Assert(aDataset.FieldByName(FLD_id).AsInteger>0, 'Value <=0');

    iLinkend_id :=aDataset.FieldByName(FLD_Linkend_id).AsInteger;

    oLinkend := FindByLinkendID(iLinkend_id);

    Assert(Assigned(oLinkend));


    oLinkend.Antennas.AddItem.LoadFromDataset(aDataset);
   // Assert (Items[i].Antennas.Count>0, 'Items[i].Antennas.Count>0');


  //  AddItem.LoadFromDataset(aDataset);
    aDataset.Next;
  end;

  for i:=0 to Count-1 do
  begin
    s:=Items[i].Name;

    Assert (Items[i].Antennas.Count>0, 'Items[i].Antennas.Count>0');
  end;
 }

//  Result := True;

(*
  for I := 0 to aAntennas.Count - 1 do
  begin
    oLinkend := FindByID1(aAntennas[i].Linkend_id);

    if Assigned(oLinkend) then
      oLinkend.AntennaRefList.AddItem(aAntennas[i])
    else
      raise Exception.Create('');
  end;
*)
end;

// ---------------------------------------------------------------
procedure TnbLinkFreqPlan_Linkend_List.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
begin
  Clear;
  aDataset.First;

  while not aDataset.EOF do
  begin
    AddItem.LoadFromDataset(aDataset);
    aDataset.Next;
  end;
end;


// ---------------------------------------------------------------
procedure TnbLinkFreqPlan_Linkend_List.Show;
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;

  oStrList: TStringList;

begin
  oStrList:=TStringList.Create;




  for I := 0 to Count - 1 do
  begin
    s:= Format('id: %d, linkend_id: %d!', [Items[i].id, Items[i].Linkend_id]);
    oStrList.Add(s);
  end;

//  s:=oStrList.Text;

  ShowMessage(oStrList.Text);

  FreeAndNil(oStrList);
end;

// ---------------------------------------------------------------
function TnbLinkFreqPlan_Linkend_List.Validate22: Boolean;
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;

  oStrList: TStringList;

begin
  oStrList:=TStringList.Create;


  for I := 0 to Count - 1 do
    if Items[i].LINKENDTYPE_ID = 0 then
    begin
      s:= Format('�� ��� ''%s'' ��������� ������������!', [Items[i].Name]);
      oStrList.Add(s);
    end;

  s:=oStrList.Text;

  FreeAndNil(oStrList);
  

  Result:=s='';

  if not Result then
  begin
    if ConfirmDlg('���������� ������ ������. �������� ������?') then
      ShellExec_Notepad_temp1(s);

//    if not ConfirmDlg('���������� ������?') then
  //    Exit;

  end;



(*
  if aLinkEnd1.LINKENDTYPE_ID = 0 then
  begin
    aTerminated:=(MessageDlg ('�� ��� '''+
              aLinkEnd1.Name +
              //qry_LinkEnd1.FieldByName(FLD_NAME).AsString+
             ''' ��������� ������������!'+#13#10+'���������� ������?',
             mtError, [mbYes,mbNo],0)=mrYes);
    Exit;
  end;*)

  // TODO -cMM: TnbLinkFreqPlan_Linkend_List.Validate22 default body inserted
end;

// ---------------------------------------------------------------
function TnbLinkFreqPlan_Linkend_List.Validate: Boolean;
// ---------------------------------------------------------------
var
  s: string;
  I: Integer;
  oSList: TStringList;
  oErrorList: TStringList;

  oLinkend: TnbLinkFreqPlan_Linkend;

begin

  oSList:=TStringList.Create;
  oErrorList:=TStringList.Create;


  for I := 0 to Count - 1 do
  begin
    oLinkend := Items[i];

    if oLinkend.BAND = '' then
      oSList.Add('BAND ������');

    if oLinkend.LINKENDTYPE_ID = 0 then
      oSList.Add('����������� ��������� ������������.');

    if oLinkend.BANDWIDTH_TX_30 <= oLinkend.BANDWIDTH_TX_3 then
      oSList.Add(oLinkend.LinkEndType_name + ': - BANDWIDTH_TX_30 > BANDWIDTH_TX_3');


    if oLinkend.BANDWIDTH_RX_30 <= oLinkend.BANDWIDTH_RX_3 then
      oSList.Add(oLinkend.LinkEndType_name + ' - BANDWIDTH_RX_30 > BANDWIDTH_RX_3');


(*
    if (oLinkend.TxRx_Shift = 0) then   oSList.Add('TxRx_Shift = 0');
    if (oLinkend.ChannelWidth = 0) then oSList.Add('ChannelWidth = 0');
    if (oLinkend.Freq_Min_low_MHz = 0) then oSList.Add('Freq_Min_low_MHz = 0');
*)

//    oLinkend.IsValid:=oSList.Count=0;

//    if not oLinkend.IsValid then
    if oSList.Count>0 then
    begin
      oErrorList.Add('-----------------------------------------');

      s:= Format('��C - ������ � ����������: %s', [oLinkend.Name]);
      oErrorList.Add(s);
      oErrorList.Add('Link_name: '+ oLinkend.Link_name);

(*      aErrorList.Add('LinkEndType_name: '+ oLinkend.LinkEndType_name);

      aErrorList.Add('LinkEndType_band_id: '+ IntToStr( oLinkend.LinkEndType_band_id));
      aErrorList.Add('LinkEndType_band_name: '+ oLinkend.LinkEndType_band_name);

      aErrorList.Add('LinkEndType_mode_id: '+ IntToStr( oLinkend.LinkEndType_mode_id));
*)

      oErrorList.AddStrings(oSList);
    end;

    oSList.Clear;
  end;

  s:=oErrorList.Text;

  Result:=s='';

  FreeAndNil(oSList);
  FreeAndNil(oErrorList);






  if not Result then
  begin
    if ConfirmDlg('���������� ������ ������. �������� ������?') then
      ShellExec_Notepad_temp1(s);

//    if not ConfirmDlg('���������� ������?') then
  //    Exit;

  end;




(*
  b:= GetFreq_Resource(aLinkEndID, dTxRx_Shift, dChannelWidth, dFreqMin_MHz, sChAllowed);

  if (dTxRx_Shift=0) or (dChannelWidth=0) or (dFreqMin_MHz=0) then
  begin
    s:=gl_DB.GetNameByID(TBL_LINKEND, aLinkEndID);

    ErrorDlg( Format('��� ��� "%s" �� ������ �����������!', [s]));
    Exit;
  end;

*)
(*

  for I := 0 to Count - 1 do
    if (Items[i].TxRx_Shift   = 0) or
       (Items[i].ChannelWidth = 0) or
       (Items[i].Freq_Min_low_MHz  = 0)
    then
    begin
      s:= Format('��� ��� "%s" �� ������ �����������!', [Items[i].Name_]);

      aErrorList.Add(s);
    end;
*)
(*
  if (dTxRx_Shift=0) or (dChannelWidth=0) or (dFreqMin_MHz=0) then
  begin
    ErrorDlg('��� ��� "'+gl_DB.GetNameByID(TBL_LINKEND, aLinkEndID)+'" '+
             #13#10 + '�� ������ �����������!');
    Exit;
  end;
*)


end;

constructor TnbNeighborList.Create;
begin
  inherited Create(TnbNeighbor);
  Selection := TnbNeighborRefList.Create();
end;

function TnbNeighborList.AddItem: TnbNeighbor;
begin

  Result := TnbNeighbor (inherited Add);

 // Result.TX_LINKEND_ID := aTX_LINKEND_ID;
//  Result.RX_LINKEND_ID := aRX_LINKEND_ID;

//  Result.

end;

function TnbNeighborList.GetItems(Index: Integer): TnbNeighbor;
begin
  Result := TnbNeighbor(inherited Items[Index]);
end;


constructor TLinkFreqPlan.Create;
begin
  inherited Create;
  Linkends := TnbLinkFreqPlan_Linkend_List.Create();
  Neighbors := TnbNeighborList.Create();
end;

destructor TLinkFreqPlan.Destroy;
begin
  FreeAndNil(Neighbors);
  FreeAndNil(Linkends);
  inherited Destroy;
end;


// ---------------------------------------------------------------
procedure TLinkFreqPlan.LoadFromDataset(aDataset: TDataset);
// --------------------------------------------------------------- 
begin
  with aDataset do
  begin
    Id                  :=FieldByName(FLD_Id).AsInteger;
//    Self.Name           :=FieldByName(FLD_Name).AsString;
    Linknet_id          :=FieldByName(FLD_Linknet_id).AsInteger;

    MaxCalcDistance_km  :=FieldByName(FLD_MaxCalcDistance_KM).AsFloat;
    Polarization_Level_dB:=FieldByName(FLD_Polarization_Level_dB).AsFloat;
    SectorFreqSpacing   :=FieldByName(FLD_SectorFreqSpacing).AsInteger;
    SiteFreqSpacing     :=FieldByName(FLD_SiteFreqSpacing).AsInteger;
    MIN_CI_dB            :=FieldByName(FLD_MIN_CI_dB).AsFloat;
    StartSectorNum      :=FieldByName(FLD_StartSectorNum).AsInteger;
    PereborQueueType    :=FieldByName(FLD_PereborQueueType).AsInteger;
    FreqDistribType     :=FieldByName(FLD_FreqDistribType).AsInteger;
    MaxIterCount        :=FieldByName(FLD_MaxIterCount).AsInteger;
    PereborOrderType    :=FieldByName(FLD_PereborOrderType).AsInteger;
    MaxFreqRepeat       :=FieldByName(FLD_MaxFreqRepeat).AsInteger;
    Threshold_degradation_db:=FieldByName(FLD_Threshold_degradation).AsFloat;


    Interference_ChannelsKind:=FieldByName(FLD_Interference_ChannelsKind1).AsString;

  end;
end;




destructor TnbNeighborList.Destroy;
begin
  FreeAndNil(Selection);
  inherited Destroy;
end;

// ---------------------------------------------------------------
function TnbNeighborList.FindItemByLinkendID_IsLink(aLinkend_ID: Integer):
    TnbNeighbor;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    if Items[i].IsLink and
       ((Items[i].LINKEND1_ID=aLinkend_ID) or
        (Items[i].LINKEND2_ID=aLinkend_ID))
    then begin
      Result:=Items[i];
      Exit;
    end;

  Result := nil;
end;


// ---------------------------------------------------------------
procedure TnbNeighborList.SelectByRxLinkendID_and_not_IsLink(aLinkend_ID:
    Integer);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Selection.Clear;

  for I := 0 to Count - 1 do
    if //Items[i].IsLink and
       (Items[i].LINKEND2_ID=aLinkend_ID) // or
        and
       (not Items[i].IsLink)

//       (IsLink<>1

        //(Items[i].TX_LINKEND_ID=aLinkend_ID))
    then begin
      Selection.Add(Items[i]);
     // Exit;
    end;
end;




// ---------------------------------------------------------------
function TnbNeighborList.FindIByID(aID: Integer): TnbNeighbor;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Assert (aID>0); 

  for I := 0 to Count - 1 do
    if Items[i].ID=aID then
    begin
      Result:=Items[i];
      Exit;
    end;

  Result := nil;
end;


// ---------------------------------------------------------------
procedure TnbNeighborList.test;
// ---------------------------------------------------------------

var
  I: Integer;
  oSList: TStringList;

  obNeighbor: TnbNeighbor;

begin
  oSList:=TStringList.Create;


  oSList.Add( 'tx, rx, is_link');

  for I := 0 to Count - 1 do
  begin
    obNeighbor := Items[i];




//    obNeighbor.IsLink1


    oSList.Add( Format('%d, %d, %s', [obNeighbor.LINKEND1_ID, obNeighbor.LINKEND2_ID,  BoolToStr(obNeighbor.IsLink)  ]));


{    iRX_LINKEND_ID:=obNeighbor.RX_LINKEND_ID;

    obNeighbor1 := FData.Neighbors.FindItemByLinkendID_IsLink(iRX_LINKEND_ID);

    if not Assigned(obNeighbor1) then
      Assert(Assigned(obNeighbor1), 'RX_LINKEND_ID: '+ IntTostr(iRX_LINKEND_ID));

}

  end;    // for

////////////  ShellExec_Notepad_temp(  oSList.Text  );


  FreeAndNil (oSList);

end;


{
  for I := 0 to FData.Neighbors.Count - 1 do    // Iterate
  begin
    obNeighbor := FData.Neighbors[i];

    iRX_LINKEND_ID:=obNeighbor.RX_LINKEND_ID;

    obNeighbor1 := FData.Neighbors.FindItemByLinkendID_IsLink(iRX_LINKEND_ID);

    if not Assigned(obNeighbor1) then
      Assert(Assigned(obNeighbor1), 'RX_LINKEND_ID: '+ IntTostr(iRX_LINKEND_ID));


  end;    // for

}


procedure TnbNeighborList.LoadFromDataset(aDataset: TDataset);
begin
  Clear;
  aDataset.First;

  while not aDataset.EOF do
  begin
    AddItem.LoadFromDataset(aDataset);
    aDataset.Next;
  end;
end;


procedure TnbNeighborList.SelectBy_RX_LINK_ID(aRX_LINK_ID : Integer);
var
  I: Integer;
begin
  Selection.Clear;



(*

  for I := 0 to Count - 1 do
    if Items[i].RX_LINK_ID=aRX_LINK_ID then
      Selection.Add(Items[i]);
*)
end;


constructor TnbAntenna.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  HorzMask := TAntennaMask.Create();
  VertMask := TAntennaMask.Create();
end;


destructor TnbAntenna.Destroy;
begin
  FreeAndNil(VertMask);
  FreeAndNil(HorzMask);
  inherited Destroy;
end;


//-------------------------------------------------------------------
procedure TnbAntenna.LoadFromDataset(aDataset: TDataset);
//-------------------------------------------------------------------
var
  sHorzMask: string;
  sVertMask: string;
begin

  with aDataset do
    if not IsEmpty then
  begin
    Linkend_id :=FieldByName(FLD_Linkend_id).AsInteger;
    // -------------------------

    Azimuth           :=FieldByName(FLD_Azimuth).AsFloat;

    Gain           :=FieldByName(FLD_GAIN).AsFloat;
    Diameter          :=FieldByName(FLD_Diameter).AsFloat;
    Polarization_str  :=FieldByName(FLD_POLARIZATION_str).Asstring;

    Loss     :=FieldByName(FLD_LOSS).AsFloat;
    Vert_width  :=FieldByName(FLD_Vert_width).AsFloat;


    if Eq(Polarization_str,'v') then FPolarization:=ptV else
    if Eq(Polarization_str,'h') then FPolarization:=ptH else
    if Eq(Polarization_str,'x') then FPolarization:=ptx ;


(*      if Eq(aTypeMask,'v') then
        Result:=aAntenna.VertMask.GetLossByAngle(dAngle)
     //   Result:=dmAntType.Mask_GetLossByAngle (aAntType.VertMaskArr, dAngle)
      else
        Result:=0;



*)

    Height :=FieldByName(FLD_Height).AsFloat;


    // -------------------------
    // -------------------------

(*
    BLPoint.B :=FieldByName(FLD_PROPERTY_LAT).AsFloat;
    BLPoint.L :=FieldByName(FLD_PROPERTY_LON).AsFloat;
*)


    BLPoint.B :=FieldByName('MAP_LAT').AsFloat;
    BLPoint.L :=FieldByName('MAP_LON').AsFloat;


    // -------------------------
    // AntType
    // -------------------------
    POLARIZATION_RATIO:=FieldByName(FLD_POLARIZATION_RATIO).AsFloat;

    sVertMask :=FieldByName(FLD_vert_mask).AsString;
    sHorzMask :=FieldByName(FLD_horz_mask).AsString;

    VertMask.LoadFromStr(sVertMask);
    HorzMask.LoadFromStr(sHorzMask);

//    LoadFromStr

//    Mask_StrToArr (aRec.HorzMask, aRec.HorzMaskArr);
//    Mask_StrToArr (aRec.VertMask, aRec.VertMaskArr);



(*    FFreq_MHz          :=FieldByName(FLD_FREQ_MHZ).AsFloat;
  //  aRec.Polarization      :=FieldByName(FLD_POLARIZATION).AsInteger;


    FVert_Width        :=FieldByName(FLD_Vert_Width).AsFloat;
    FHorz_Width        :=FieldByName(FLD_Horz_Width).AsFloat;

    VertMaskStr          :=FieldByName(FLD_vert_mask).AsString;
    HorzMaskStr          :=FieldByName(FLD_horz_mask).AsString;
*)

//    Mask_StrToArr (aRec.HorzMaskStr, aRec.HorzMaskArr);
//    Mask_StrToArr (aRec.VertMaskStr, aRec.VertMaskArr);

  end;
end;

constructor TnbAntennaList.Create;
begin
  inherited Create(TnbAntenna);
end;

function TnbAntennaList.AddItem: TnbAntenna;
begin
  Result := TnbAntenna (inherited Add);
end;

function TnbAntennaList.GetItems(Index: Integer): TnbAntenna;
begin
  Result := TnbAntenna(inherited Items[Index]);
end;

procedure TnbAntennaList.LoadFromDataset(aDataset: TDataset);
begin
  Clear;

  aDataset.First;
  while not aDataset.EOF do
  begin
    AddItem.LoadFromDataset(aDataset);
    aDataset.Next;
  end;
end;

// ---------------------------------------------------------------
function TnbNeighborRefList.FindIByID(aID: Integer): TnbNeighbor;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    if Items[i].ID=aID then
    begin
      Result:=Items[i];
      Exit;
    end;

  Result := nil;
end;


procedure TnbNeighborRefList.Show;
var
  I: Integer;
  s: string;
begin
  s:='';

  for I := 0 to Count - 1 do
    begin
      s:=s+  IntToStr(Items[i].ID) + CRLF ;
    end;

   showMessage(s);

end;


function TnbNeighborRefList.GetItems(Index: Integer): TnbNeighbor;
begin
  Result := TnbNeighbor(inherited Items[Index]);
end;




constructor TnbData.Create;
begin
  inherited;

  LinkFreqPlan := TLinkFreqPlan.Create();
  Linkends := TnbLinkFreqPlan_Linkend_List.Create();
  Neighbors := TnbNeighborList.Create();
  Resources := TnbResourceList.Create();
end;


destructor TnbData.Destroy;
begin
  FreeAndNil(Resources);
  FreeAndNil(Neighbors);
  FreeAndNil(Linkends);
  FreeAndNil(LinkFreqPlan);

  inherited Destroy;
end;

constructor TnbResourceList.Create;
begin
  inherited Create(TnbNeighbor);
end;

function TnbResourceList.AddItem: TnbResource;
begin
  Result := TnbResource(inherited Add);
end;

function TnbResourceList.GetItems(Index: Integer): TnbResource;
begin
 Result := TnbResource(inherited Items[Index]);
end;

// ---------------------------------------------------------------
function TnbResourceList.FindByBand(aBand: string): TnbResource;
// ---------------------------------------------------------------
var
  I: integer;
begin
  for I := 0 to Count - 1 do
    if Eq(Items[i].Band, aBand) then
    begin
      Result := Items[i];
      Exit;
    end;

  Result := nil;
end;



// ---------------------------------------------------------------
procedure TnbResourceList.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
var
  oItem: TnbResource;
begin
  Clear;

  aDataset.First;

  with aDataset do
    while not EOF do
    begin
      oItem:=AddItem();

      oItem.Band                   :=FieldByName(FLD_Band).AsString;
      oItem.FREQ_BAND_ALLOW_Mhz    :=FieldByName(FLD_FREQ_ALLOW).AsString;
      oItem.FREQ_BAND_FORBIDDEN_Mhz:=FieldByName(FLD_FREQ_FORBIDDEN).AsString;


    //  AddItem.LoadFromDataset(aDataset);

      Next;
    end;
end;


end.




// TODO: LoadFromDataset
//procedure TnbAntennaList.LoadFromDataset(aDataset: TDataset);
//begin
//
// (* Clear;
//aDataset.First;
//while not aDataset.EOF do
//begin
//  Add.LoadFromDataset(aDataset);
//  aDataset.Next;
//end;*)
//
//end;


(*function TnbAntennaRefList.GetItems(Index: Integer): TnbAntenna;
begin
  Result := TnbAntenna(inherited Items[Index]);
end;
*)


    Band_Resources: array of record
                          Band : string;
                          FREQ_ALLOW : String;
                          FREQ_FORBIDDEN: string;

                       //   StrArrAllow     : TStrArray;
                       //   StrArrForbidden : TStrArray;

                        end;


                        //
//
//// ---------------------------------------------------------------
//procedure TnbNeighborList.SelectBy_Rx_LinkendID(aLinkend_ID: Integer);
//// ---------------------------------------------------------------
//var
//  I: Integer;
//begin
//  Selection.Clear;
//
//  for I := 0 to Count - 1 do
//    if //Items[i].IsLink and
//       (Items[i].RX_LINKEND_ID=aLinkend_ID) // or
//    //    and
//     //  (not Items[i].IsLink))
//
////       (IsLink<>1
//
//        //(Items[i].TX_LINKEND_ID=aLinkend_ID))
//    then begin
//      Selection.Add(Items[i]);
//      Exit;
//    end;
//end;
