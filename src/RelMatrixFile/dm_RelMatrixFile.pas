unit dm_RelMatrixFile;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Db, ADODB,
  Registry, Variants, Math, dxmdaset, FileCtrl,

  u_const_filters,

  u_rel_Matrix_base,

  dm_Onega_DB_data,

  dm_Object_base,                                                             

  I_rel_Matrix1,
  u_rel_engine,


  dm_Main,

  u_common_const,

  u_Mapx,
//  u_mitab,
//  u_zlib,
  u_func,
  u_files,
  u_db,
  u_dlg,
  u_reg,

  u_log,
  u_GEO,
  u_geo_convert_new,

  u_classes,

  u_types,

  u_const_db,
  u_const

  ;


type
  TdmMatrixFileAddRec = record
    FolderID : integer;
    FileName: string;
  end;


//  TdmRelMatrixFile = class(TDataModule)
  TdmRelMatrixFile = class(TdmObject_base)
    qry_RelMatrix11: TADOQuery;
    qry_Temp: TADOQuery;
    OpenDialog1: TOpenDialog;
    procedure DataModuleCreate(Sender: TObject);
  private
    FFolderPath: string;
// TODO: GetAllList1111111111
//  function GetAllList1111111111(aStrings: TStrings): integer;
//    function GetMinStepForVector1111(aBLVector: TBLVector): integer;

  protected

    procedure ForEach_SaveFileExistToDB(aDataset: TDataSet; var aTerminated:
        boolean);

  public
  //  procedure LoadChecked;

//    procedure SetChecked(aID: Integer; aValue: Boolean);
 //   function GetChecked(aID: Integer): Boolean;


    function SaveFileExistToDB(aDataSet: TDataSet): boolean;

    function Add(aRec: TdmMatrixFileAddRec): Integer;

    function Add_file(aFileName: string; aFolderID: integer): Integer;

// TODO: Add_
//  procedure Add_(aRec: TdmMatrixFileAddRec); overload;
//    function  Add_folder (aFileName: string; aFolderID: integer): integer; overload;

 //   function  Del    (aID: integer): boolean; override;

    procedure AddList (aList: TStrings; aFolderID: integer);
    procedure AddFromDir (aFileDir: string; aFolderID: integer=0);

    function  Dlg_Add(aFolderID: integer): Boolean;
    procedure Dlg_LoadReliefFromDir(aFolderID: integer);


    function GetListInRect1 (aBLRect: TBLRect;
                            aStrings: TStringList;
                            var aBest6Zone: integer;
                            var aMinStep: integer): integer;

  end;


function dmRelMatrixFile: TdmRelMatrixFile;


//==================================================================
implementation {$R *.DFM}
//==================================================================

var
  FdmRelMatrixFile: TdmRelMatrixFile;

const
  FLD_GEOM_Pulkovo = 'GEOM_Pulkovo';



// -----------------------------------------------------------------
function dmRelMatrixFile: TdmRelMatrixFile;
// -----------------------------------------------------------------

begin
  if not Assigned(FdmRelMatrixFile) then
      FdmRelMatrixFile := TdmRelMatrixFile.Create(Application);

  Result := FdmRelMatrixFile;           

end;


procedure TdmRelMatrixFile.DataModuleCreate(Sender: TObject);
begin
  inherited;
  TableName:= TBL_RELIEF;

 // ObjectName:=OBJ_REL_MATRIX;

 /////// IReliefEngine_Init;
end;



//-------------------------------------------------------------------
procedure TdmRelMatrixFile.ForEach_SaveFileExistToDB(aDataset: TDataSet; var
    aTerminated: boolean);
//-------------------------------------------------------------------
var
  b: boolean;
  sFileName: string;
begin
  sFileName:=aDataset.FieldByName(FLD_NAME).AsString;
  b:=FileExists(sFileName);

//  if aDataset.FieldByName(FLD_EXIST).AsBoolean <> b then
  if aDataset.FieldValues[FLD_EXIST] <> b then
    db_UpdateRecord(aDataSet, [db_Par(FLD_EXIST, b)]);

end;


//-------------------------------------------------------------------
function TdmRelMatrixFile.SaveFileExistToDB(aDataSet: TDataSet): boolean;
//-------------------------------------------------------------------
begin
  db_ForEach(aDataSet, ForEach_SaveFileExistToDB);
end;

//--------------------------------------------------------------------
procedure TdmRelMatrixFile.AddList (aList: TStrings; aFolderID: integer );
//--------------------------------------------------------------------
var
  i: Integer;
begin
  for i := 0 to aList.Count-1 do
    Add_file(aList[i], aFolderID);
//    Add_folder(aList[i], aFolderID);
//    Add(aList[i], aFolderID);
end;

{
//--------------------------------------------------------------------
function  TdmRelMatrixFile.Del (aID: integer): boolean;
//--------------------------------------------------------------------
begin
  Result := inherited Del(aID);
end;
}


//--------------------------------------------------------------------
//   function  TdmRelMatrixFile.Add_folder (aFileName: string; aFolderID: integer): integer;
function TdmRelMatrixFile.Add_file(aFileName: string; aFolderID: integer):
    Integer;
//--------------------------------------------------------------------
var
  rec: TdmMatrixFileAddRec;
begin
  FillChar(rec, SizeOf(rec), 0);

  rec.FileName:=aFileName;
  rec.FolderID:=aFolderID;

  Result := Add(rec);
end;



(*function  TdmRelMatrixFile.Add_folder (aFileName: string; aFolderID: integer): integer;
function  TdmRelMatrixFile.Add_folder (aFileName: string; aFolderID: integer): integer;
//--------------------------------------------------------------------
var
  rec: TdmMatrixFileAddRec;
begin
  FillChar(rec, SizeOf(rec), 0);

  rec.FileName:=aFileName;
 // rec.FolderID:=aFolderID;
  Add(rec);
end;



*)//--------------------------------------------------------------------
function TdmRelMatrixFile.GetListInRect1 (aBLRect: TBLRect;
                                        aStrings: TStringList;
                                        var aBest6Zone: integer;
                                        var aMinStep: integer
                                        ): integer;

//--------------------------------------------------------------------
// iStep: min ���������
//--------------------------------------------------------------------
var
  frame_rect_BL: TBLRect;
  frame_rectXY: TXYRect;
  iStep: integer;
  bChecked: Boolean;
  k: Integer;
begin
  assert(dmMain.ProjectID<>0);
  Assert(Assigned(aStrings));

  aBest6Zone:=0;
  aMinStep:=0;

  aStrings.Clear;

//  'SELECT * FROM '+TBL_RELIEF+' WHERE project_id=:project_id'

  k:=dmOnega_DB_data.Relief_Select(ADOStoredProc1, dmMain.ProjectID, True);


///// // db_OpenQuery (qry_RelMatrix, VIEW_RELIEF, FLD_PROJECT_ID, dmMain.ProjectID);
//  FLD_PROJECT_ID, dmMain.ProjectID);


{  db_OpenQuery (qry_RelMatrix, //TBL_RELIEF,
                STEP_Y'SELECT Y_MIN,Y_MAX,X_MAX,X_MIN, FROM '+TBL_RELIEF+' WHERE project_id=:project_id',
                [db_Par(FLD_PROJECT_ID, dmMain.ProjectID)]);
//  FLD_PROJECT_ID, dmMain.ProjectID);

}
  with ADOStoredProc1 do
    while not EOF do
  begin
    bChecked:=FieldByName(FLD_Checked).AsBoolean;

    //��������, ������������ �� �������
    if not bChecked then
//    if not SetChecked(FieldByName(FLD_ID).AsInteger) then
    begin
      Next;
      continue;
    end;

    //---------------------
    // ������� � XY
    if (FieldByName(FLD_Y_MIN).AsFloat>0) and
       (FieldByName(FLD_Y_MAX).AsFloat>0) and
       (FieldByName(FLD_X_MAX).AsFloat>0) and
       (FieldByName(FLD_X_MIN).AsFloat>0)
    then begin
      if aBest6Zone=0 then
        aBest6Zone:=geo_Get6ZoneY (FieldByName(FLD_Y_MIN).AsFloat);

      frame_rectXY.TopLeft.X    := FieldByName(FLD_X_MAX).AsFloat;
      frame_rectXY.BottomRight.X:= FieldByName(FLD_X_MIN).AsFloat;
      frame_rectXY.TopLeft.Y    := FieldByName(FLD_Y_MIN).AsFloat;
      frame_rectXY.BottomRight.Y:= FieldByName(FLD_Y_MAX).AsFloat;

//      frame_rect_BL:= geo_XYRect_to_BLRect(frame_rectXY);
      frame_rect_BL:= geo_RoundXYRect(frame_rectXY);

      iStep:=Min(FieldByName(FLD_STEP_X).AsInteger, FieldByName(FLD_STEP_Y).AsInteger);

      if (aMinStep=0) or (aMinStep>iStep) then
        aMinStep:=iStep;


      // ������� ������������ ?
      // aBLRect.TopLeft.B=0 - open all matrix
      if (aBLRect.TopLeft.B=0) or
         (geo_IsBLRects_Crossed (aBLRect, frame_rect_BL))
//         and
//         if FileExists(oStrList[i]) then
      then
        aStrings.Add(FieldValues[FLD_NAME]);
    end;

    //---------------------
    // ������� � ������-������� (BL)
    if (FieldByName(FLD_LON_MIN).AsFloat>0) and
       (FieldByName(FLD_LON_MAX).AsFloat>0) and
       (FieldByName(FLD_LAT_MAX).AsFloat>0) and
       (FieldByName(FLD_LAT_MIN).AsFloat>0)
    then begin
      frame_rect_BL.TopLeft.L    := FieldBYName(FLD_LON_MIN).AsFloat;
      frame_rect_BL.BottomRight.L:= FieldBYName(FLD_LON_MAX).AsFloat;
      frame_rect_BL.TopLeft.B    := FieldBYName(FLD_LAT_MAX).AsFloat;
      frame_rect_BL.BottomRight.B:= FieldBYName(FLD_LAT_MIN).AsFloat;

      // ������� ������������ ?
      // aBLRect.TopLeft.B=0 - open all matrix
      if (aBLRect.TopLeft.B=0) or
         (geo_IsBLRects_Crossed (aBLRect, frame_rect_BL))
      then
        aStrings.Add(FieldValues[FLD_NAME]);
    end;

    Next;
  end;

  Result:=aStrings.Count;
end;


//--------------------------------------------------------------------
procedure TdmRelMatrixFile.AddFromDir (aFileDir: string; aFolderID: integer=0);
//--------------------------------------------------------------------
var
  oStrList: TStringList;
begin
  if aFileDir='' then
    exit;

  oStrList:=TStringList.Create;

{ TODO : reduce }
  // -------------------------------------------------------------------
//  FindFilesByMask (aFileDir, '*.rlf',  oStrList, False);
//  FindFilesByMask (aFileDir, '*.ter',  oStrList, False);
//  FindFilesByMask (aFileDir, '*.hgt',  oStrList, False);

 // FindFilesByMaskEx (aFileDir, '.rlf;.ter;.hgt',  oStrList);//, False);

//  ScanDir1 (aFileDir, '*.rlf;*.hgt',  oStrList);//, False);
  ScanDir1 (aFileDir, '*.rlf;*.ter;*.hgt',  oStrList);//, False);


  //FileSearchEx('C:\Temp', '.txt;.tmp;.exe;.doc', FileList);

//  FindFilesByMask (aFileDir, '*.zip',  oStrList, False);
  // -------------------------------------------------------------------

//  ShellExec_Notepad_temp(oStrList.Text);


  AddList (oStrList, aFolderID);


//  oStrList.Free;

  FreeAndNil(oStrList);

end;


//--------------------------------------------------------------------
function TdmRelMatrixFile.Dlg_Add(aFolderID: integer): Boolean;
//--------------------------------------------------------------------
var i,iParentID: integer;
  rec: TdmMatrixFileAddRec;
begin
  OpenDialog1.DefaultExt:='*.rlf';
  OpenDialog1.Filter:=FILTER_RLF_MATRIX;

  if OpenDialog1.Execute then
  begin
    for i:=0 to OpenDialog1.Files.Count-1 do
    begin
      FillChar(rec, SizeOf(rec), 0);

      rec.FileName:=OpenDialog1.Files[i];
      rec.FolderID:=aFolderID;

      Add (rec);
    end;

  //  PostEvent(WE_PROJECT_UPDATE_MATRIX_LIST);
    Result:=true;
  end else
    Result:=False;


 {
//  if aFolderID=0 then
  if Frec.Folder_ID>0 then
    sFolderGUID:=dmFolder.GetGUIDByID(Frec.Folder_ID)
  else
    sFolderGUID:=g_Obj.ItemByName[OBJ_PROPERTY].RootFolderGUID;
 // else
  //  sFolderGUID:=dmFolder.GetGUIDByID(aFolderID);

  g_Shell.UpdateNodeChildren_ByGUID (sFolderGUID);

//  g_ShellEvents.Shell_EXPAND_BY_GUID(sFolderGUID);
  g_Shell.EXPAND_BY_GUID(sFolderGUID);

  g_Shell.FocuseNodeByObjName (OBJ_PROPERTY, FID);
   }
    

end;

//-------------------------------------------------------------------
procedure TdmRelMatrixFile.Dlg_LoadReliefFromDir(aFolderID: integer);
//-------------------------------------------------------------------
var
  // oDialog: TPBFolderDialog;
  S: string;
begin
 // oDialog:=TPBFolderDialog.Create(Application.MainForm);
 // oDialog.Flags:= oDialog.Flags;//-[ShowShared];
  if FileCtrl.SelectDirectory('����� �����...','',FFolderPath) then

//  if PBFolderDialog1.Execute then
  begin
    s := FFolderPath;

    AddFromDir (s);
  end;

  
 {
//  if aFolderID=0 then
  if Frec.Folder_ID>0 then
    sFolderGUID:=dmFolder.GetGUIDByID(Frec.Folder_ID)
  else
    sFolderGUID:=g_Obj.ItemByName[OBJ_PROPERTY].RootFolderGUID;
 // else
  //  sFolderGUID:=dmFolder.GetGUIDByID(aFolderID);

  g_Shell.UpdateNodeChildren_ByGUID (sFolderGUID);

//  g_ShellEvents.Shell_EXPAND_BY_GUID(sFolderGUID);
  g_Shell.EXPAND_BY_GUID(sFolderGUID);

  g_Shell.FocuseNodeByObjName (OBJ_PROPERTY, FID);
   }


 // oDialog.Free;

{  if s<>'' then
  begin
  //  Screen.Cursor:=crHourGlass;
    AddFromDir (s);

  //  PostEvent(WE_PROJECT_UPDATE_MATRIX_LIST);

  //  Screen.Cursor:=crDefault;
  end;

}
end;



//--------------------------------------------------------------------
function TdmRelMatrixFile.Add(aRec: TdmMatrixFileAddRec): Integer;
//--------------------------------------------------------------------
var iID: integer;
 // sFile: string;
  iFileSize: int64;
  dtFileDate: TDateTime;

  rInfo: TrelMatrixInfoRec;
  sComment: string;
  sExt: string;

  bl: TBLPoint;

  arrPoints: TXYPointArray;
  i: Integer;
  s: string;
  sGeom: string;        

  oRelMatrix: TrelMatrixBase;
  
begin
  Result := -1;

  sExt:=LowerCase(ExtractFileExt (aRec.FileName));

//  if sExt='.rel'  then obj:=TrelNevaMatrix.GetInfo (aFileName, aRec) else
//  if not ((sExt='.rlf') or (sExt='.hgt'))
  if not ((sExt='.rlf') or (sExt='.ter') or (sExt='.hgt'))
  then Exit;

//  if sExt='.vm'  then Result:=Trel_VerticalMapper.GetInfo    (aFileName, aRec)



  iFileSize  := GetFileSize(aRec.FileName);
  dtFileDate := GetFileDate(aRec.FileName);

  if iFileSize=0 then
  begin
    g_Log.Error('FileSize=0; FileName:'+aRec.FileName);
    Exit;
  end;



 /// I_rel_Matrix1.IReliefEngine_Load;

  if not GetReliefEngine.GetMatrixInfo (aRec.FileName, rInfo) then
    Exit;


  oRelMatrix:=rel_CreateRelMatrixByFileName (aRec.FileName);
  oRelMatrix.OpenHeader(aRec.FileName);

  if Assigned(oRelMatrix) then
    sGeom:=oRelMatrix.ToGeometry()
  else
    sGeom:='';

  FreeAndNil(oRelMatrix);


  g_Log.Add(sGeom);



//  begin
    // -------------------------------------------------------------------
    if rInfo.StepX>0 then
    // -------------------------------------------------------------------
    begin
      sComment:=Format('step X: %1.1f; step Y:%1.1f; '+
                       'X min-max:%1.3f-%1.3f; Y min-max:%1.3f-%1.3f;'+
                       'zone: %d',
                       [rInfo.StepX,
                        rInfo.StepY,
                        rInfo.XYBounds.BottomRight.X, rInfo.XYBounds.TopLeft.X,
                        rInfo.XYBounds.TopLeft.Y,     rInfo.XYBounds.BottomRight.Y,
                        rInfo.ZoneNum
                       ]);

   {
   arrPoints:= geo_XYRectToXYPoints_(rInfo.XYBounds);

   s:='';

   for i:=0 to high(arrPoints) do
   begin
     bl:=geo_Pulkovo42_XY_to_BL (arrPoints[i] , rInfo.ZoneNum ); //, EK_CK_42, EK_CK_42 );
     //XY_to_BL

     s:=s + Format (', %1.6f %1.6f', [bl.L, bl.B] );
   end;
   }

//  s:=Format( 'POLYGON (( %s ))', [Copy(s, 2, 1000) ] );

//      function geo_XY_to_BL(aXYPoint: TXYPoint; aZone: integer=0; aFromElp: byte = EK_CK_42; aToElp: Integer = EK_CK_42): TBLPoint;

//                    RowCount,ColCount : integer;


//DECLARE @g geometry;
//SET @g = geometry::STPolyFromText('POLYGON ((5 5, 10 5, 10 10, 5 5))', 0);
//SELECT @g.ToString();


  //////// sGeom:=ReplaceStr(sGeom, '.',',');



   iID:=dmOnega_DB_data.ExecStoredProc_ ('GIS.sp_Relief_Add',

        [
         FLD_PROJECT_ID, dmMain.ProjectID,
         FLD_FOLDER_ID, IIF_NULL(aRec.FOLDERID),

         FLD_NAME,       aRec.FileName,

         FLD_FileSize,   iFileSize,
         FLD_FileDate,   dtFileDate,

         FLD_STEP_X,    rInfo.StepX ,
         FLD_STEP_Y,    rInfo.StepY ,

         FLD_X_MIN,     rInfo.XYBounds.BottomRight.X ,
         FLD_Y_MAX,     rInfo.XYBounds.BottomRight.Y ,
         FLD_X_MAX,     rInfo.XYBounds.TopLeft.X ,
         FLD_Y_MIN,     rInfo.XYBounds.TopLeft.Y ,

//         FLD_ROW_COUNT, rInfo.RowCount,
//         FLD_COL_COUNT, rInfo.ColCount,

         'GEOM_STR', sGeom,

//         FLD_Y_MIN,     rInfo.XYBounds.TopLeft.Y ,


         FLD_COMMENT,   sComment
       //  db_Par (FLD_FILESIZE,  rInfo.FileSize)
        ]);
    end else

    // -------------------------------------------------------------------
    if rInfo.StepB>0 then
    // -------------------------------------------------------------------
    begin
      with rInfo do
      begin
        StepB:=TruncFloat(StepB,8);
        StepL:=TruncFloat(StepL,8);

        BLBounds.BottomRight.B:=TruncFloat(BLBounds.BottomRight.B, 8);
        BLBounds.BottomRight.L:=TruncFloat(BLBounds.BottomRight.L, 8);
        BLBounds.TopLeft.B    :=TruncFloat(BLBounds.TopLeft.B, 8);
        BLBounds.TopLeft.L    :=TruncFloat(BLBounds.TopLeft.L, 8);
      end;

      sComment:=Format('step lat: %1.6f; step lon:%1.6f; '+
                       'lat min-max:%1.6f-%1.6f; lon min-max:%1.6f-%1.6f;',
                       [rInfo.StepB, rInfo.StepL,
                        rInfo.BLBounds.BottomRight.B, rInfo.BLBounds.TopLeft.B,
                        rInfo.BLBounds.TopLeft.L, rInfo.BLBounds.BottomRight.L
                       ]);

   iID:=dmOnega_DB_data.ExecStoredProc_ ('dbo.sp_Relief_Add',
        [
         FLD_PROJECT_ID, dmMain.ProjectID,
         FLD_FOLDER_ID, IIF_NULL(aRec.FOLDERID),
         FLD_NAME,       aRec.FileName,
         FLD_FileSize,   iFileSize,
         FLD_FileDate,   dtFileDate,

         FLD_STEP_LAT, rInfo.StepB ,
         FLD_STEP_LON, rInfo.StepL ,

         FLD_LAT_MIN,  rInfo.BLBounds.BottomRight.B ,
         FLD_LON_MAX,  rInfo.BLBounds.BottomRight.L ,
         FLD_LAT_MAX,  rInfo.BLBounds.TopLeft.B ,
         FLD_LON_MIN,  rInfo.BLBounds.TopLeft.L ,

       //  'GEOM_STR', s,


         FLD_COMMENT,  sComment

        ])
    end else
      ShowMessage('error - if IRel.GetMatrixInfo (aFileName, rInfo) then');



  Result := iID;

{
   CursorSQL;
//   sFile:=GetTempFileName_('.z');
   sFile:='u:\1.z';
   sFile:=aRec.FileName;
  // zlib_CompressFile(aRec.FileName, sFile);

//   gl_DB.LoadBlobFromFile(TableName, iID, FLD_CONTENT, sFile, False);

//   DeleteFile(sFile);
   CursorDefault;}

//   Com

  //       (Table_.FieldByName(field_name_) as TBlobField).loadfromfile(ExtractFilePath(Application.ExeName) + 'temp\' + FormatFilePath(path));

end;



begin

end.


{

function TdmRelMatrixFile.GetChecked(aID: Integer): Boolean;
begin
//  Result := ;
end;

procedure TdmRelMatrixFile.LoadChecked;
begin

end;

procedure TdmRelMatrixFile.SetChecked(aID: Integer; aValue: Boolean);
begin
 // Result := ;
end;





// TODO: UpdateFileRecord
////--------------------------------------------------------------------
//procedure TdmRelMatrixFile.UpdateFileRecord (aID: integer; aFileName: string);
////--------------------------------------------------------------------
//var
//rInfo: TrelMatrixInfoRec;
//sComment: string;
//
//begin
//I_rel_Matrix1.IReliefEngine_Load;
//
//if IReliefEngine.GetMatrixInfo (aFileName, rInfo) then
////  if IRel.GetMatrixInfo (aFileName, rInfo) then
//begin
//  // -------------------------------------------------------------------
//  if rInfo.StepX>0 then
//  // -------------------------------------------------------------------
//  begin
//    sComment:=Format('step X: %1.1f; step Y:%1.1f; '+
//                     'X min-max:%1.3f-%1.3f; Y min-max:%1.3f-%1.3f;'+
//                     'zone: %d',
//                     [rInfo.StepX, rInfo.StepY,
//                      rInfo.XYBounds.BottomRight.X/1000, rInfo.XYBounds.TopLeft.X/1000,
//                      rInfo.XYBounds.TopLeft.Y/1000,     rInfo.XYBounds.BottomRight.Y/1000,
//                      rInfo.ZoneNum
//                     ]);
//
//    Update_ (aID,
//      [db_Par (FLD_STEP_X,     rInfo.StepX ),
//       db_Par (FLD_STEP_Y,     rInfo.StepY ),
//
//       db_Par (FLD_X_MIN,      rInfo.XYBounds.BottomRight.X ),
//       db_Par (FLD_Y_MAX,      rInfo.XYBounds.BottomRight.Y ),
//       db_Par (FLD_X_MAX,      rInfo.XYBounds.TopLeft.X ),
//       db_Par (FLD_Y_MIN,      rInfo.XYBounds.TopLeft.Y ),
//
//       db_Par (FLD_COMMENT,    sComment )
//
//      ]);
//  end else
//
//  // -------------------------------------------------------------------
//  if rInfo.StepB>0 then
//  // -------------------------------------------------------------------
//  begin
//    with rInfo do
//    begin
//      StepB:=TruncFloat(StepB,8);
//      StepL:=TruncFloat(StepL,8);
//
//      BLBounds.BottomRight.B:=TruncFloat(BLBounds.BottomRight.B, 8);
//      BLBounds.BottomRight.L:=TruncFloat(BLBounds.BottomRight.L, 8);
//      BLBounds.TopLeft.B    :=TruncFloat(BLBounds.TopLeft.B, 8);
//      BLBounds.TopLeft.L    :=TruncFloat(BLBounds.TopLeft.L, 8);
//    end;
//
//    sComment:=Format('step lat: %1.6f; step lon:%1.6f; '+
//                     'lat min-max:%1.6f-%1.6f; lon min-max:%1.6f-%1.6f;',
//                     [rInfo.StepB, rInfo.StepL,
//                      rInfo.BLBounds.BottomRight.B, rInfo.BLBounds.TopLeft.B,
//                      rInfo.BLBounds.TopLeft.L, rInfo.BLBounds.BottomRight.L
//                     ]);
//
//    Update_ (aID,
//      [db_Par (FLD_STEP_LAT, rInfo.StepB ),
//       db_Par (FLD_STEP_LON, rInfo.StepL ),
//
//       db_Par (FLD_LAT_MIN,  rInfo.BLBounds.BottomRight.B ),
//       db_Par (FLD_LON_MAX,  rInfo.BLBounds.BottomRight.L ),
//       db_Par (FLD_LAT_MAX,  rInfo.BLBounds.TopLeft.B ),
//       db_Par (FLD_LON_MIN,  rInfo.BLBounds.TopLeft.L ),
//
//       db_Par (FLD_COMMENT,  sComment )
//      ])
//  end else
//    ShowMessage('error - if IRel.GetMatrixInfo (aFileName, rInfo) then');
//
////      sComment:='';
//
// end;
//end;


// TODO: UpdateFileRecord
//// TODO: SaveBoundFramesToMIF
////  procedure SaveBoundFramesToMIF(aFileName: string);
//  procedure UpdateFileRecord(aID: integer; aFileName: string);




(*//-------------------------------------------------------------------
function TdmRelMatrixFile.GetMinStepForVector1111(aBLVector: TBLVector):
    integer;
//-------------------------------------------------------------------
var
  blRect: TBLRect;
  oStrList: TStringList;
  iBest6Zone: integer;
  iMinStep: integer;

begin
  blRect.TopLeft:=aBLVector.Point1;
  blRect.BottomRight:=aBLVector.Point2;

  oStrList:=TStringList.Create;

  GetListInRect1 (blRect, oStrList, iBest6Zone, iMinStep);

  oStrList.Free;

  Result := iMinStep;
end;
*)
