unit dm_Act_RelFile;

interface
{.$DEFINE test}

uses
  Classes, Controls, Forms, Dialogs,   Menus, ActnList,  SysUtils,  FileCtrl,

  u_ini_rel_to_bmp_3D,

  dm_Onega_db_data,

  u_files,

 // I_Shell,

  I_Object,
//  I_Act_RelFile,  //dm_Act_RelFile_X

  dm_act_Base,

  dm_RelMatrixFile,

  dm_RelMatrixFile_export_import,

  dm_Rel_Engine,

  dm_CalcMap,


  u_func_msg,
  

  u_vars,

  
  u_func,
  u_classes,

  u_const_str,

  u_Types

  ;


type
  TdmAct_RelFile = class(TdmAct_Base) //, IAct_RelFile_X)
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    SaveDialog_XML: TSaveDialog;
    act_ExportToXML: TAction;
    act_ImportFromXML: TAction;
    act_SaveRelToMIF: TAction;
    act_SaveCluToMIF: TAction;
    act_Add_from_Dir: TAction;
    act_SaveRelFramesToMIF: TAction;
    act_SaveTo3D: TAction;
    act_Save_Clutter_map: TAction;
    ActionList2: TActionList;
    act_Check: TAction;
    act_UnCheck: TAction;
    act_Relief_restore_geo_frames: TAction;
 //   procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FFolderPath : string;

    procedure SaveTo3D(aID, aCode: integer);
  //  procedure CheckActionsEnable;
  //
  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    procedure DoAction (Sender: TObject);

    function ItemDel(aID: integer; aName: string = ''): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;

  //  procedure SaveRelToMIF (aID: integer);
   // procedure SaveCluToMIF (aID: integer);


  public
    procedure Dlg_Add;

    function Dlg_LoadReliefFromDir: Boolean;

    procedure Del_List (aIDList: TIDList);
    procedure Dlg_SaveRelFramesToMIF;

    class procedure Init;
  end;

var
  dmAct_RelFile: TdmAct_RelFile;



//============================================================
// implementation
//============================================================
implementation

 {$R *.DFM}

const
  DEF_CODE_3D = 0;
  DEF_CODE_CLUTTER = 1;


//------------------------------------------------------------
class procedure TdmAct_RelFile.Init;
//------------------------------------------------------------
begin
  Assert(not Assigned(dmAct_RelFile));

  Application.CreateForm(TdmAct_RelFile, dmAct_RelFile);

 // dmAct_RelFile.GetInterface(IAct_RelFile_X, IAct_RelFile);
 // Assert(Assigned(IAct_RelFile));

end;

//
//
//procedure TdmAct_RelFile.DataModuleDestroy(Sender: TObject);
//begin
// // IAct_RelFile := nil;
// // dmAct_RelFile:= nil;
//
//  inherited;  
//end;

//------------------------------------------------------------
procedure TdmAct_RelFile.DataModuleCreate(Sender: TObject);
//------------------------------------------------------------
var b: Boolean;
begin
  inherited;
  ObjectName:=OBJ_REL_MATRIX;

  act_Add.Caption     :='�������� ������� �����';

//  act_SaveRelToMIF.Caption:='��������� ������ � Mapinfo';
//  act_SaveCluToMIF.Caption:='��������� ����� � Mapinfo';

  act_ExportToxml.Caption:='������� � XML';
  act_ImportFromxml.Caption:='������ �� XML';
  act_Add_from_Dir.Caption:='�������� �� �����';

  act_SaveTo3D.Caption:='������� ���������������� ��������';
  act_Save_Clutter_map.Caption:='������� ����� ����� ���������';

  act_SaveRelFramesToMIF.Caption:='��������� ������� � Mapinfo';

  act_Relief_restore_geo_frames.Caption:='������������ ������� ������';

  act_Check.Caption:='��������';
  act_UnCheck.Caption:='���������';



  SetActionsExecuteProc
     ([//act_ExportToXML,
      // act_ImportFromXML,
       act_Add_from_Dir,


        act_Check,
        act_UnCheck,


//       act_SaveRelToMIF,
//       act_SaveCluToMIF,

       act_SaveRelFramesToMIF,
       act_Relief_restore_geo_frames,

       act_SaveTo3D,
       act_Save_Clutter_map
       ],
      DoAction);

  SetCheckedActionsArr([


        act_Check,
        act_UnCheck,

        act_Add,
        act_Add_from_Dir

       // act_Add_list

//        act_Add_Item_passive,
//        act_Edit,
//        act_Calc
     ]);
                 
end;


//------------------------------------------------------------
// actions
//------------------------------------------------------------
procedure TdmAct_RelFile.DoAction(Sender: TObject);
var
  bChecked: Boolean;
  I: Integer;
  k: Integer;
begin
//  if Sender=act_Check then ;
//  if Sender=act_UnCheck then ;

  if Sender = act_Relief_restore_geo_frames then
  begin
    dmRel_Engine.Init_Geom;
  end else

  // ---------------------------------------------------------------
  if (Sender=act_Check) or (Sender=act_UnCheck) then
  // ---------------------------------------------------------------
  begin
    bChecked:=Sender=act_Check;

    for I := 0 to FSelectedIDList.Count - 1 do    // Iterate
    begin
      k:=dmOnega_DB_data.Relief_XREF_Update1 (
         FSelectedIDList[i].ID,
     //    DataSet.FieldByName(FLD_ID).AsInteger,
         bChecked);

    end;

    g_EventManager.PostEvent_(et_REL_MATRIX_LIST_CHECKED, ['IDList', Integer(FSelectedIDList),
                                                           'Checked', bChecked]);

//     PostUserMessage (WM__REL_MATRIX_LIST_CHECKED, Integer(FSelectedIDList), Integer( bChecked));

  end else

{
  FSelectedIDList

   k:=dmOnega_DB_data.Relief_XREF_Update1 (
       DataSet.FieldByName(FLD_RELIEF_ID).AsInteger,
   //    DataSet.FieldByName(FLD_ID).AsInteger,
       DataSet.FieldByName(FLD_CHECKED).AsBoolean);
}



//  if Sender=act_SaveRelToMIF then SaveRelToMIF (FFocusedID) else
//  if Sender=act_SaveCluToMIF then SaveCluToMIF (FFocusedID) else

  if Sender=act_SaveTo3D then SaveTo3D (FFocusedID, 0) else
  if Sender=act_Save_Clutter_map then SaveTo3D (FFocusedID, 1) else

{  if Sender=act_ExportToXML   then dmRelMatrixFile_export_import.ExportToXML () else
  if Sender=act_ImportFromXML then dmRelMatrixFile_export_import.ImportFromXML (FFocusedID) else
}
  if Sender=act_SaveRelFramesToMIF then Dlg_SaveRelFramesToMIF else

  if Sender=act_Add_from_Dir  then Dlg_LoadReliefFromDir ()

  else
   raise Exception.Create('');

end;

// ---------------------------------------------------------------
procedure TdmAct_RelFile.Dlg_Add;
// ---------------------------------------------------------------
begin
  if dmRelMatrixFile.Dlg_Add(0) then
  begin
    g_EventManager.PostEvent_(et_PROJECT_UPDATE_MATRIX_LIST,[]);
    g_EventManager.PostEvent_(et_Explorer_REFRESH_REL_MATRIX,[]);

//    PostMessage(Application.Handle, WE_PROJECT_UPDATE_MATRIX_LIST1, 0,0);

//    PostMessage (Application.Handle, WM_Explorer_REFRESH_REL_MATRIX,0,0);



   ////// PostEvent(WE_PROJECT_UPDATE_MATRIX_LIST);
    //WE_PROJECT_UPDATE_MATRIX_LIST
  end;
end;



//--------------------------------------------------------------------
function TdmAct_RelFile.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
var i,iParentID: integer;
  rec: TdmMatrixFileAddRec;
begin
  if dmRelMatrixFile.Dlg_Add(aFolderID) then
  begin
   // PostEvent(WE_PROJECT_UPDATE_MATRIX_LIST1);
    g_EventManager.PostEvent_(et_PROJECT_UPDATE_MATRIX_LIST, []);
    g_EventManager.PostEvent_(et_Explorer_REFRESH_REL_MATRIX, []);

//    PostMessage (Application.Handle, WE_PROJECT_UPDATE_MATRIX_LIST1,0,0);
//    PostMessage (Application.Handle, WM_Explorer_REFRESH_REL_MATRIX,0,0);

  //  IMainProjectExplorer.UPDATE_NODE_CHILDREN(GUID_REL_MATRIX);

    //  PostMessage (Application.Handle, WM_Explorer_REFRESH_REL_MATRIX,0,0); 

  end;

  Result := 0;

{
  OpenDialog1.DefaultExt:='*.rlf';
  OpenDialog1.Filter:=FILTER_RLF_MATRIX;

  if OpenDialog1.Execute then
  begin
    for i:=0 to OpenDialog1.Files.Count-1 do
    begin
      FillChar(rec, SizeOf(rec), 0);

      rec.FileName:=OpenDialog1.Files[i];
      rec.FolderID:=aFolderID;

      dmRelMatrixFile.Dlg_Add (rec);
    end;

    PostEvent(WE_PROJECT_UPDATE_MATRIX_LIST);
    Result:=1;
  end else
    Result:=-1;
}
end;

//--------------------------------------------------------------------
procedure TdmAct_RelFile.SaveTo3D(aID, aCode: integer);
//--------------------------------------------------------------------
const
  EXE_REL_to_BMP_3D = 'REL_to_BMP_3D.exe';
  TEMP_INI_FILE     = 'rel_to_bmp.ini';

var
  iCode: Integer;
  oParams: Tini_rel_to_bmp_3D_Params;
  r: Integer;
  sFile: string;
  sFileName: string;

  rec: TdmCalcMapAddRec1;
  sPrefix: string;
  sTabFileName: string;

begin

  sFile := g_ApplicationDataDir + TEMP_INI_FILE;
  SysUtils.DeleteFile(sFile);


  sFileName:=dmRelMatrixFile.GetNameByID (aID);

  oParams:=Tini_rel_to_bmp_3D_Params.Create;


 // sFile := g_ApplicationDataDir + TEMP_INI_FILE;

  // --------------------------------------------

 // oParams.RelFileName := sFileName;
//  oParams.TabFileName := sTabFileName;

  case aCode of
    0: begin
         oParams.TaskType := tt3D;
         sPrefix:='_3D';
       end;
    1: begin
         oParams.TaskType := ttClutters;
         sPrefix:='_2D';
       end
  end;


//  sTabFileName  := ChangeFileExt(sFileName, sPrefix + '_3D.tab') ;
  sTabFileName  := ChangeFileExt(sFileName, sPrefix + '.tab') ;


  sTabFileName  := IncludeTrailingBackslash(ExtractFileDir(sTabFileName))
                   + ReplaceStr(ExtractFileName(sTabFileName), ' ','');


  oParams.RelFileName := sFileName;
  oParams.TabFileName := sTabFileName;

  oParams.SaveToFile(sFile);

  FreeAndNil (oParams);


////  oParams.Files.Values [sFileName] := sTabFileName;


 {
  FreeAndNil(oParams);
  // --------------------------------------------

  Init_relief_dll();

    vUtils:=CoUtils.Create;
    vUtils.SaveCluttersToTAB(g_Params.RelFileName, g_Params.TabFileName);


  end else
  begin
    Init_relief_dll();

    vUtils:=CoUtils.Create;
    vUtils.SaveRelief3DToTab(g_Params.RelFileName, g_Params.TabFileName);

}


//  oParams.RelFileName :=


  RunApp (GetApplicationDir() + EXE_REL_to_BMP_3D,  DoubleQuotedStr(sFile) , iCode );

  if FileExists(sTabFileName) then
  begin
    FillChar(rec,SizeOf(rec),0);

    rec.Section:='������� �������';
  //  rec.Name:='3D �������';

    rec.Name:=  ExtractFileName(sFileName) + sPrefix;// + ' 3D';
    rec.TabFileName:=sTabFileName;

    Assert(fileExists(sTabFileName));

    r:=dmCalcMap.RegisterCalcMap_ (rec);
 //   Assert(r>0);

   // r:=0;

//    PostMessage (Application.Handle, WM_REFRESH_CALC_MAP_FILES,0,0);
//    PostMessage (Application.Handle, WM_USER + Integer(WM__CalcMap_REFRESH) ,0,0);
   g_EventManager.PostEvent_(et_CalcMap_REFRESH, []);

//    PostMessage (Application.Handle, WM_USER + WM_REFRESH_CALC_MAP_FILES,0,0);

  end else
    ShowMessage('���� �� ������: '+ sTabFileName);

//  CursorDefault;
//  {$ENDIF}
end;




//--------------------------------------------------------------------
function TdmAct_RelFile.ItemDel(aID: integer; aName: string = ''): boolean;
//--------------------------------------------------------------------
var sFileName: string;
begin
  sFileName:=dmRelMatrixFile.GetNameByID (aID);
  Result:=dmRelMatrixFile.Del (aID);
  Assert(Result);


  dmRel_Engine.CloseFile (sFileName);
end;

//--------------------------------------------------------------------
procedure TdmAct_RelFile.Del_List (aIDList: TIDList);
//--------------------------------------------------------------------
var i: integer;
begin
  if ConfirmDlg (STR_ASK_DEL) then
  begin
    for i:=0 to aIDList.Count-1 do
      ItemDel(aIDList[i].ID);

    g_EventManager.PostEvent_(et_Explorer_REFRESH_REL_MATRIX, []);
//    PostMessage (Application.Handle, WM_Explorer_REFRESH_REL_MATRIX,0,0);

  end;
end;

//-------------------------------------------------------------------
function TdmAct_RelFile.Dlg_LoadReliefFromDir: Boolean;
//-------------------------------------------------------------------
var
  // oDialog: TPBFolderDialog;
  S: string;
begin
 // oDialog:=TPBFolderDialog.Create(Application.MainForm);
 // oDialog.Flags:= oDialog.Flags;//-[ShowShared];

  if FileCtrl.SelectDirectory('����� �����...','',FFolderPath) then

//  if PBFolderDialog1.Execute then
    s := FFolderPath // PBFolderDialog1.Folder
  else
    Exit;



 // oDialog.Free;

  if s<>'' then
  begin
    Screen.Cursor:=crHourGlass;
    dmRelMatrixFile.AddFromDir (s);

    g_EventManager.PostEvent_(et_PROJECT_UPDATE_MATRIX_LIST, []);
    g_EventManager.PostEvent_(et_Explorer_REFRESH_REL_MATRIX, []);


 {
//  if aFolderID=0 then
  if Frec.Folder_ID>0 then
    sFolderGUID:=dmFolder.GetGUIDByID(Frec.Folder_ID)
  else
    sFolderGUID:=g_Obj.ItemByName[OBJ_PROPERTY].RootFolderGUID;
 // else
  //  sFolderGUID:=dmFolder.GetGUIDByID(aFolderID);

  g_Shell.UpdateNodeChildren_ByGUID (sFolderGUID);

//  g_ShellEvents.Shell_EXPAND_BY_GUID(sFolderGUID);
  g_Shell.EXPAND_BY_GUID(sFolderGUID);

  g_Shell.FocuseNodeByObjName (OBJ_PROPERTY, FID);
   }


//    PostMessage (Application.Handle, WE_PROJECT_UPDATE_MATRIX_LIST1,0,0);
//    PostMessage (Application.Handle, WM_Explorer_REFRESH_REL_MATRIX,0,0);

(*
    PostEvent(WE_PROJECT_UPDATE_MATRIX_LIST1);
    IMainProjectExplorer.UPDATE_NODE_CHILDREN(GUID_REL_MATRIX);
*)


    Screen.Cursor:=crDefault;
  end;


  Result := s<>'';
end;


//--------------------------------------------------------------------
procedure TdmAct_RelFile.Dlg_SaveRelFramesToMIF;
//--------------------------------------------------------------------
//var i,iParentID: integer;
  // sRelFileName: string;
//   obj: TTask_RelMatrixToBMP;
var
  iID: integer;
  k: Integer;

  rec: TdmCalcMapAddRec1;
begin
//  SaveDialog1.InitialDir:=ExtractFileDir(sRelFileName);

  SaveDialog1.FileName:='frames.tab';

  if SaveDialog1.Execute then
  begin
    dmRelMatrixFile_export_import.SaveBoundFramesToMIF (SaveDialog1.FileName);

//    iID:=dmCalcMap.RegisterSection (iParentID, sName, 'project.relief_frames');

    FillChar(rec,SizeOf(rec),0);

    rec.Section:='������� �������';
    rec.Name:='������� ������ �������';
    rec.TabFileName:=SaveDialog1.FileName;

   // rec.KeyName:='project.relief_frames';

   //  Checked: boolean;

    // ParentID: integer;
    // IsSelectable: Boolean;

    // PmpCalcRegionID: integer;

    k:=dmCalcMap.RegisterCalcMap_ (rec);

//    PostMessage (Application.Handle, WM_CalcMap_REFRESH,0,0);

    g_EventManager.PostEvent_(et_CalcMap_REFRESH, []);

//    PostMessage (Application.Handle, WM_USER + Integer(WM__CalcMap_REFRESH) ,0,0);





(*                           ,
                               0, 0, '������� ������ �������',
                               SaveDialog1.FileName,
                               'project.relief_frames',
                               True);
*)

  end;
end;




//--------------------------------------------------------------------
procedure TdmAct_RelFile.GetPopupMenu;
//--------------------------------------------------------------------
begin
  //CheckActionsEnable();

  case aMenuType of
    mtFolder: begin
              AddFolderMenu_Create (aPopupMenu);
              AddFolderPopupMenu (aPopupMenu);
              AddMenuItem (aPopupMenu, nil);
           //   AddMenuItem (aPopupMenu, act_ExportToXML);
           //   AddMenuItem (aPopupMenu, act_ImportFromXML);
              AddMenuItem (aPopupMenu, act_SaveRelFramesToMIF);

              act_ExportToxml.Enabled:=(FFolderID = 0);
              act_SaveRelFramesToMIF.Enabled:=(FFolderID = 0);

              AddMenuItem (aPopupMenu, act_Add_from_Dir);
              AddMenuItem (aPopupMenu, nil);
              AddMenuItem (aPopupMenu, act_Relief_restore_geo_frames);  

            end;


    mtItem: begin
        //      AddMenuItem (aPopupMenu, act_Map_Move);
        //      AddMenuItem (aPopupMenu, act_SaveRelToMIF);
        //      AddMenuItem (aPopupMenu, act_SaveCluToMIF);

       //       AddMenuItem (aPopupMenu, nil);
              AddMenuItem (aPopupMenu, act_Del_);

          //    AddMenuItem (aPopupMenu, nil);

           // {$IFDEF test}
                AddMenuItem (aPopupMenu, act_SaveTo3D);
                AddMenuItem (aPopupMenu, act_Save_Clutter_map);

          //  {$ENDIF}

     //         AddMenuItem  (aPopupMenu, nil);
     //         AddFolderMenu_Tools (aPopupMenu);


              AddMenuItem (aPopupMenu, act_Check);
              AddMenuItem (aPopupMenu, act_UnCheck);


            end;

    mtList: begin
              AddMenuItem (aPopupMenu, act_Del_List);
              AddMenuItem (aPopupMenu, nil);

          //    AddFolderMenu_Tools (aPopupMenu);


              AddMenuItem (aPopupMenu, act_Check);
              AddMenuItem (aPopupMenu, act_UnCheck);


            end;
  end;
end;


 
end.


{



//--------------------------------------------------------------------
procedure TdmAct_RelFile.SaveRelToMIF (aID: integer);
//--------------------------------------------------------------------
var i,iParentID: integer;
   sRelFileName: string;
//   obj: TTask_RelMatrixToBMP;
begin
  MsgDlg ('������� �������������.');
{
  sRelFileName:=dmRelMatrixFile.GetNameByID (aID);

  if not FileExists(sRelFileName) then begin
    ErrorDlg ('���� �� ������: '+ sRelFileName);
    Exit;
  end;

  SaveDialog1.InitialDir:=ExtractFileDir(sRelFileName);

  SaveDialog1.FileName:='rel_' + ChangeFileExt(ExtractFileName (sRelFileName), '.tab');
  if SaveDialog1.Execute then
  begin
    obj:=TTask_RelMatrixToBMP.Create;
    obj.SaveReliefToMIF (sRelFileName, SaveDialog1.FileName);
    obj.Free;
  end;
}
end;

//--------------------------------------------------------------------
procedure TdmAct_RelFile.SaveCluToMIF (aID: integer);
//--------------------------------------------------------------------
var i,iParentID: integer;
   sRelFileName: string;
//   obj: TTask_RelMatrixToBMP;
begin
  MsgDlg ('������� �������������.');

{
  sRelFileName:=dmRelMatrixFile.GetNameByID (aID);

  if not FileExists(sRelFileName) then begin
    ErrorDlg ('���� �� ������: '+ sRelFileName);
    Exit;
  end;

  SaveDialog1.InitialDir:=ExtractFileDir(sRelFileName);

  SaveDialog1.FileName:='clu_' + ChangeFileExt(ExtractFileName (sRelFileName), '.tab');
  if SaveDialog1.Execute then
  begin
    obj:=TTask_RelMatrixToBMP.Create;
    obj.SaveCluToMIF (sRelFileName, SaveDialog1.FileName);
    obj.Free;
  end;

  
 {
//  if aFolderID=0 then
  if Frec.Folder_ID>0 then
    sFolderGUID:=dmFolder.GetGUIDByID(Frec.Folder_ID)
  else
    sFolderGUID:=g_Obj.ItemByName[OBJ_PROPERTY].RootFolderGUID;
 // else
  //  sFolderGUID:=dmFolder.GetGUIDByID(aFolderID);

  g_Shell.UpdateNodeChildren_ByGUID (sFolderGUID);

//  g_ShellEvents.Shell_EXPAND_BY_GUID(sFolderGUID);
  g_Shell.EXPAND_BY_GUID(sFolderGUID);

  g_Shell.FocuseNodeByObjName (OBJ_PROPERTY, FID);
   }


}
end;
