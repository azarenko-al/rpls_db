inherited dmAct_RelFile: TdmAct_RelFile
  OldCreateOrder = True
  Left = 1298
  Top = 446
  Height = 369
  Width = 629
  inherited ActionList1: TActionList
    object act_ExportToXML: TAction
      Caption = 'act_ExportToXML'
    end
    object act_ImportFromXML: TAction
      Caption = 'act_ImportFromXML'
    end
    object act_SaveRelToMIF: TAction
      Caption = 'act_SaveRelToMIF'
    end
    object act_SaveCluToMIF: TAction
      Caption = 'act_SaveCluToMIF'
    end
    object act_Add_from_Dir: TAction
      Caption = 'act_Add_from_Dir'
    end
    object act_SaveRelFramesToMIF: TAction
      Caption = 'act_SaveRelFramesToMIF'
    end
    object act_SaveTo3D: TAction
      Caption = 'act_SaveTo3D'
    end
    object act_Save_Clutter_map: TAction
      Caption = 'act_Save_Clutter_map'
    end
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.rlf'
    Filter = #1052#1072#1090#1088#1080#1094#1099' '#1074#1099#1089#1086#1090'|*.rlf;*.ter;*.rel;*.zip'
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 44
    Top = 106
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '*.tab'
    Filter = 'Mapinfo (*.tab)|*.tab'
    Left = 156
    Top = 100
  end
  object SaveDialog_XML: TSaveDialog
    DefaultExt = 'xml'
    Filter = 'XML (*.xml)|*.xml'
    Left = 44
    Top = 180
  end
  object ActionList2: TActionList
    Left = 264
    Top = 216
    object act_Check: TAction
      Caption = 'act_Check'
    end
    object act_UnCheck: TAction
      Caption = 'act_UnCheck'
    end
    object act_Relief_restore_geo_frames: TAction
      Caption = 'act_Relief_restore_geo_frames'
    end
  end
end
