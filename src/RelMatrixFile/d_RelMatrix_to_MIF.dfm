inherited dlg_RelMatrix_to_MIF: Tdlg_RelMatrix_to_MIF
  Left = 282
  Top = 201
  Caption = 'dlg_RelMatrix_to_MIF'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox [2]
    Left = 36
    Top = 96
    Width = 201
    Height = 113
    Caption = #1062#1074#1077#1090' '#1088#1077#1083#1100#1077#1092#1072
    TabOrder = 2
    object Label1: TLabel
      Left = 8
      Top = 44
      Width = 77
      Height = 13
      Caption = 'Min '#1074#1099#1089#1086#1090#1072' (m):'
    end
    object Label8: TLabel
      Left = 8
      Top = 20
      Width = 80
      Height = 13
      Caption = 'Max '#1074#1099#1089#1086#1090#1072' (m):'
    end
    object ed_Max_H: TEdit
      Left = 96
      Top = 16
      Width = 50
      Height = 21
      TabOrder = 0
      Text = '100'
    end
    object ed_Min_H: TEdit
      Left = 96
      Top = 40
      Width = 50
      Height = 21
      TabOrder = 1
      Text = '0'
    end
    object cb_UseAquaColor: TCheckBox
      Left = 93
      Top = 70
      Width = 49
      Height = 17
      Caption = #1042#1086#1076#1072
      TabOrder = 2
    end
    object color_Max_H_color: TGSColorEdit
      Left = 148
      Top = 16
      Width = 41
      Height = 21
      SelColor = clOlive
      TabOrder = 3
    end
    object color_Min_H_color: TGSColorEdit
      Left = 148
      Top = 40
      Width = 41
      Height = 21
      SelColor = clGray
      TabOrder = 4
    end
    object color_Aqua_color: TGSColorEdit
      Left = 148
      Top = 68
      Width = 41
      Height = 21
      SelColor = clNavy
      TabOrder = 5
    end
  end
end
