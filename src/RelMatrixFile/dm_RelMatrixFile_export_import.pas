unit dm_RelMatrixFile_export_import;

interface

uses
  Windows, Messages, Db, ADODB, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,

  dm_Onega_DB_data,

  dm_Custom,

  dm_Main,
  dm_RelMatrixFile,

  u_const_db,
  u_GEO,
  u_geo_convert_new,


  u_Mapx,
  u_Mapx_lib,
//  u_mitab,
  XMLDoc, XMLIntf,

  u_xml_document,

  u_common_const,
  u_func,
  u_db,
  u_const


  ;

type
  TdmRelMatrixFile_export_import = class(TDataModule)
    OpenDialog1111: TOpenDialog;
    SaveDialog_XML: TSaveDialog;
    ADOQuery1: TADOQuery;
    ADOStoredProc1: TADOStoredProc;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  protected


  public
    function ExtractReliefBoundsFromDataset(aDataSet: TDataSet): TBLPointArray;

    procedure SaveBoundFramesToMIF(aFileName: string);

  end;


function dmRelMatrixFile_export_import: TdmRelMatrixFile_export_import;

//==============================================================
implementation {$R *.DFM}
//==============================================================

var
  FdmRelMatrixFile_export_import: TdmRelMatrixFile_export_import;


// ---------------------------------------------------------------
function dmRelMatrixFile_export_import: TdmRelMatrixFile_export_import;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmRelMatrixFile_export_import) then
    FdmRelMatrixFile_export_import := TdmRelMatrixFile_export_import.Create(Application);

  Result := FdmRelMatrixFile_export_import;
end;


procedure TdmRelMatrixFile_export_import.DataModuleCreate(Sender: TObject);
begin
  inherited;

  db_SetComponentADOConnection(Self, dmMain.ADOConnection);

end;


//--------------------------------------------------------------------
procedure TdmRelMatrixFile_export_import.SaveBoundFramesToMIF(aFileName: string);
//--------------------------------------------------------------------
var
  bChecked: Boolean;
  bl: TBLPoint;
  iZone: integer;
  iID,I: Integer;
  xyRect: TXYRect;
  blRect: TBLRect;

//  oMapFile: TmitabMap;
  oMapFile: TmiMap;

  blPointsF: TBLPointArrayF;
  k: Integer;
  xyPoints: TXYPointArrayF;

  rStyle: TmiStyleRec;
  s: string;

const
  DEF_CHECKED_COLOR = clNavy;
  DEF_NOTCHECKED_COLOR = clRed;

begin
  k:=dmOnega_DB_data.Relief_Select(ADOStoredProc1, dmMain.ProjectID);


 ///// db_OpenQuery (qry_RelMatrix, VIEW_RELIEF, FLD_PROJECT_ID, dmMain.ProjectID);

 // ShowMessage(IntToStr(qry_RelMatrix.RecordCount));


//  oMapFile:=TmitabMap.Create;
  oMapFile:=TmiMap.Create;
  oMapFile.CreateFile1 (aFileName,
                    [
                     mapx_Field(FLD_filename, miTypeString, 50),
                     mapx_Field(FLD_ZONE, miTypeInt),

                     mapx_Field(FLD_X_MIN, miTypeFloat),
                     mapx_Field(FLD_X_MAX, miTypeFloat),
                     mapx_Field(FLD_Y_MIN, miTypeFloat),
                     mapx_Field(FLD_Y_MAX, miTypeFloat)
                    ]);

  FillChar(rStyle, SizeOf(rStyle), 0);

  rStyle.RegionBorderStyle:=MI_PEN_PATTERN_SOLId;
//  rStyle.RegionBorderColor:=clRed;

  rStyle.RegionPattern          :=MI_BRUSH_PATTERN_BLANK ;
//  rStyle.RegionForegroundColor  :=clYellow;
  //rStyle.RegionBackgroundColor  :=clBlue;
  rStyle.RegionIsTransparent    :=False;


  with ADOStoredProc1 do
    while not EOF do
  begin
    iID:=FieldByName(FLD_ID).AsInteger;

    bChecked:=FieldByName(FLD_Checked).AsBoolean;

//    bChecked:=IsChecked(iID);

    rStyle.RegionBorderColor:=IIF(bChecked, DEF_CHECKED_COLOR, DEF_NOTCHECKED_COLOR);
    rStyle.RegionBorderWidth:=IIF(bChecked, 2, 1);


//    rStyle.RegionBorderColor:=clRed;

    if (FieldByName(FLD_X_MIN).AsFloat>0) and
       (FieldByName(FLD_X_MAX).AsFloat>0) and
       (FieldByName(FLD_Y_MAX).AsFloat>0) and
       (FieldByName(FLD_Y_MIN).AsFloat>0)
    then begin
      xyRect.TopLeft.X    := FieldBYName(FLD_X_MAX).AsFloat;
      xyRect.TopLeft.Y    := FieldBYName(FLD_Y_MIN).AsFloat;

      xyRect.BottomRight.X:= FieldBYName(FLD_X_MIN).AsFloat;
      xyRect.BottomRight.Y:= FieldBYName(FLD_Y_MAX).AsFloat;

      geo_XYRectToXYPointsF (xyRect, xyPoints);

      iZone:=geo_Get6ZoneXY (xyRect.TopLeft);

      blPointsF.Count:=4;

      for I:=0 to 3 do
      begin
//        blPointsF.Items[i]:=geo_XY_to_BL (xyPoints.Items[i], iZone);
        bl:=geo_Pulkovo42_XY_to_BL (xyPoints.Items[i], iZone);

        s:=geo_FormatBLPoint(bl);

        blPointsF.Items[i]:=geo_Pulkovo42_XY_to_BL (xyPoints.Items[i], iZone);
      end;


      oMapFile.PrepareStyle(miFeatureTypeRegion, rStyle );

      oMapFile.WriteRegionF (blPointsF, //rStyle,
            [
            mapx_Par(FLD_filename, FieldByName(FLD_NAME).AsString),
            mapx_Par(FLD_ZONE,     iZone),

            mapx_Par(FLD_Y_MIN,    FieldBYName(FLD_Y_MIN).AsFloat),
            mapx_Par(FLD_X_MAX,    FieldBYName(FLD_X_MAX).AsFloat),
            mapx_Par(FLD_X_MIN,    FieldBYName(FLD_X_MIN).AsFloat),
            mapx_Par(FLD_Y_MAX,    FieldBYName(FLD_Y_MAX).AsFloat)

            ]);

    end else

    if (FieldByName(FLD_LON_MIN).AsFloat>0) and
       (FieldByName(FLD_LON_MAX).AsFloat>0) and
       (FieldByName(FLD_LAT_MAX).AsFloat>0) and
       (FieldByName(FLD_LAT_MIN).AsFloat>0)
    then begin
      blRect.TopLeft.L    := FieldBYName(FLD_LON_MIN).AsFloat;
      blRect.BottomRight.L:= FieldBYName(FLD_LON_MAX).AsFloat;
      blRect.TopLeft.B    := FieldBYName(FLD_LAT_MAX).AsFloat;
      blRect.BottomRight.B:= FieldBYName(FLD_LAT_MIN).AsFloat;
   
//      oMapFile.WriteBLRect RegionBLRect (blRect, rStyle, [mapx_Par(FLD_filename, FieldByName(FLD_NAME).AsString)]);
      oMapFile.WriteBLRect  (blRect, [mapx_Par(FLD_filename, FieldByName(FLD_NAME).AsString)]);

    end;

    Next;
  end;

 // oMapFile.Free;

  FreeAndNil(oMapFile);
end;


//--------------------------------------------------------------------
function TdmRelMatrixFile_export_import.ExtractReliefBoundsFromDataset(aDataSet: TDataSet): TBLPointArray;
//--------------------------------------------------------------------
var
//  bChecked: Boolean;
  bl: TBLPoint;
  iZone: integer;
  iID,I: Integer;
  xyRect: TXYRect;
  blRect: TBLRect;

 // oMapFile: TmitabMap;

 // aBLPointsF: TBLPointArrayF;
  xyPoints: TXYPointArrayF;
  xyPoints_: TXYPointArray;

  s: string;

begin


//  result := ;

 ///// db_OpenQuery (qry_RelMatrix, VIEW_RELIEF, FLD_PROJECT_ID, dmMain.ProjectID);

 // ShowMessage(IntToStr(qry_RelMatrix.RecordCount));


  with aDataSet do
//    while not EOF do
    begin

//    rStyle.RegionBorderColor:=clRed;

    if (FieldByName(FLD_X_MIN).AsFloat  +
        FieldByName(FLD_X_MAX).AsFloat  +
        FieldByName(FLD_Y_MAX).AsFloat  +
        FieldByName(FLD_Y_MIN).AsFloat) > 0
    then begin
      xyRect.TopLeft.X    := FieldBYName(FLD_X_MAX).AsFloat;
      xyRect.TopLeft.Y    := FieldBYName(FLD_Y_MIN).AsFloat;

      xyRect.BottomRight.X:= FieldBYName(FLD_X_MIN).AsFloat;
      xyRect.BottomRight.Y:= FieldBYName(FLD_Y_MAX).AsFloat;


      xyPoints_:=geo_XYRectToXYPoints_ (xyRect);


      geo_XYRectToXYPointsF (xyRect, xyPoints);

      iZone:=geo_Get6ZoneXY (xyRect.TopLeft);

    //  aBLPointsF.Count:=4;
      SetLength (Result, 4);

      for I:=0 to 3 do
      begin
//        aBLPointsF.Items[i]:=geo_XY_to_BL (xyPoints.Items[i], iZone);
        bl:=geo_Pulkovo42_XY_to_BL (xyPoints.Items[i], iZone);

        s:=geo_FormatBLPoint(bl);


      //  aBLPointsF.Items[i]:=geo_GK_XY_to_Pulkovo42_BL (xyPoints.Items[i], iZone);

        Result[i]:=geo_Pulkovo42_XY_to_BL (xyPoints.Items[i], iZone);
      end;

    end else

    if (FieldByName(FLD_LON_MIN).AsFloat +
        FieldByName(FLD_LON_MAX).AsFloat +
        FieldByName(FLD_LAT_MAX).AsFloat +
        FieldByName(FLD_LAT_MIN).AsFloat) >0
    then begin
      blRect.TopLeft.L    := FieldBYName(FLD_LON_MIN).AsFloat;
      blRect.BottomRight.L:= FieldBYName(FLD_LON_MAX).AsFloat;
      blRect.TopLeft.B    := FieldBYName(FLD_LAT_MAX).AsFloat;
      blRect.BottomRight.B:= FieldBYName(FLD_LAT_MIN).AsFloat;


    //  geo_BLRectToBLPointsF (blRect, aBLPointsF);

    Result := geo_BLRectToBLPoints_ (blRect );


     // oMapFile.WriteRegionRect (blRect, rStyle, [mapx_Par(FLD_filename, FieldByName(FLD_NAME).AsString)]);

    end;

 //z  Next;
  end;

 // oMapFile.Free;

//  FreeAndNil(oMapFile);
end;


begin

end.