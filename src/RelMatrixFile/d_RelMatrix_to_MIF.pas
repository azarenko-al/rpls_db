unit d_RelMatrix_to_MIF;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rxPlacemnt, ActnList, StdCtrls, ExtCtrls, GSCtrls,

  d_Wizard;

  
type
  Tdlg_RelMatrix_to_MIF = class(Tdlg_Wizard)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label8: TLabel;
    ed_Max_H: TEdit;
    ed_Min_H: TEdit;
    cb_UseAquaColor: TCheckBox;
    color_Max_H_color: TGSColorEdit;
    color_Min_H_color: TGSColorEdit;
    color_Aqua_color: TGSColorEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

{var
  dlg_RelMatrix_to_MIF: Tdlg_RelMatrix_to_MIF;
}

implementation

{$R *.DFM}

procedure Tdlg_RelMatrix_to_MIF.FormCreate(Sender: TObject);
begin
  inherited;

  with gl_Reg do begin
    BeginGroup (Self, Name);
    AddControl (ed_RPLS_Matrix_File,  [PROP_FILENAME]);

    {
    AddControl (pn_Min_H_color,    ['Color']);
    AddControl (pn_Max_H_color,    ['Color']);
    AddControl (pn_Aqua_color,     ['Color']);
    }

    AddControl (ed_Min_H,          [PROP_Text]);
    AddControl (ed_Max_H,          [PROP_Text]);
    AddControl (cb_UseAquaColor,   [PROP_Checked]);
  end;

end;

end.
