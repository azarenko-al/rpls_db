unit dm_Object_base;

interface
{$I ver.inc}

uses
  SysUtils, Classes, ADODB, 

  dm_Onega_DB_data,

  dm_Main,

  dm_Custom,

  u_types,

  u_const_db,

  u_Geo,
  u_db, DB
  ;

type
  TdmObject_base = class(TdmCustom)
    ADOStoredProc1: TADOStoredProc;
  private
  protected
  public
    TableName  : string;
    DisplayName: string;
    ObjectName : string;

// TODO: GetNewName_for_Cell1111111
//  function GetNewName_for_Cell1111111(aSiteID: integer): string;

    function GetNameByID (aID: integer): string;
    function GetGUIDByID (aID: integer): string;

    function GetNewName: string;

	  function Del  (aID: integer): boolean; virtual;
    procedure Del_Mark(aID: integer);

    function FindByName(aName: string): integer; virtual;
    function FindByNameAndFolder(aName: string; aFolderID: integer): integer;

    procedure Rename(aID: integer; aName: string);

    function GetPropertyID  (aID: integer): integer; virtual;
    function GetPropertyPos (aID: integer): TBLPoint; virtual;

	  function GetIntFieldValue    (aID: integer; aFieldName: string): integer;
    function GetDoubleFieldValue (aID: integer; aFieldName: string): Double;
    function GetStringFieldValue (aID: integer; aFieldName: string): string;
    function GetBoolFieldValue   (aID: integer; aFieldName: string): boolean;

    function GetFolderID(aID: integer): integer;
// TODO: GetIDByName1

    function GetIDByName(aName: string): integer;
    function GetPos(aID: integer; aObjName: string): TBLPoint;

    procedure Update_ (aID: integer; aParams: array of TDBParamRec); overload;
    procedure Update (aID: integer; aFieldName: string; aFieldValue: Variant); overload;
  end;



//==================================================================
implementation {$R *.DFM}
//==================================================================



function TdmObject_base.GetIDByName(aName: string): integer;
begin
  Result:=gl_DB.GetIDByName (TableName, aName);
end;

//--------------------------------------------------------------------
function TdmObject_base.GetFolderID(aID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=GetIntFieldValue(aID, FLD_FOLDER_ID);
end;


//---------------------------------------------------------
function TdmObject_base.GetPropertyID (aID: integer): integer;
//---------------------------------------------------------
begin
  Result:=GetIntFieldValue (aID, FLD_PROPERTY_ID);
end;


//-------------------------------------------------------------------
function TdmObject_base.GetPropertyPos (aID: integer): TBLPoint;
//-------------------------------------------------------------------
var iPropertyID: integer;
begin
  iPropertyID:=GetPropertyID(aID);

  if iPropertyID>0 then
     gl_DB.GetPosByID (TBL_PROPERTY, iPropertyID, Result.B, Result.L )

  else
    Result:=NULL_BLPOINT;
end;


//-------------------------------------------------------------------
function TdmObject_base.GetPos(aID: integer; aObjName: string): TBLPoint;
//-------------------------------------------------------------------
var
  k: integer;

begin
  k:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc1, 'sp_Object_Get_Pos',
    [FLD_OBJNAME, aObjName,
     FLD_ID, aID
    ]);

  k:=ADOStoredProc1.Parameters.Count;

  Result:=db_ExtractBLPoint(ADOStoredProc1);

//  ADOStoredProc1.


  // ParamValues


{      @OBJNAME    VARCHAR(50),
  @ID   INT,


  @LAT  float OUTPUT,
  @Lon  float OUTPUT
}

{
  iPropertyID:=GetPropertyID(aID);

  if iPropertyID>0 then
     gl_DB.GetPosByID (TBL_PROPERTY, iPropertyID, Result.B, Result.L )

  else
    Result:=NULL_BLPOINT;

}
end;



//-------------------------------------------------------------------
function TdmObject_base.GetNameByID (aID: integer): string;
//-------------------------------------------------------------------
begin
  Result:=gl_DB.GetNameByID (TableName, aID);
end;

function TdmObject_base.GetGUIDByID (aID: integer): string;
begin
  Result:=gl_DB.GetStringFieldValue (TableName, FLD_GUID, [db_Par(FLD_ID, aID)]);
end;


//--------------------------------------------------------------------
procedure TdmObject_base.Rename(aID: integer; aName: string);
//--------------------------------------------------------------------
begin
//dmFolder.Rename (FID, ed_Name_.Text);
  gl_DB.UpdateRecord(TableName, aID, [db_Par (FLD_NAME, aName) ]);
end;


//--------------------------------------------------------------------
function TdmObject_base.GetNewName: string;
//--------------------------------------------------------------------
//var
//  rec: TObject_GetNewName_Params;
begin
{
  FillChar(rec,SizeOf(rec),0);
  rec.ObjName    := ObjectName;
  rec.Project_ID := dmMain.ProjectID;
//  rec.Property_ID := aPropertyID;

 }
//begin
  Assert (ObjectName<>'');

  Result := dmOnega_DB_data.Object_GetNewName_select (ObjectName, dmMain.ProjectID);

end;


//-------------------------------------------------------------------
function TdmObject_base.FindByNameAndFolder(aName: string; aFolderID: integer):
    integer;
//-------------------------------------------------------------------
begin
  if (ObjectName='') then begin
    raise Exception.Create('function TdmObject_base.FindByNameAndFolder (aName: string; aFolderID: integer): integer;' + '(ObjectName='''''')');
  end;


  if (g_Obj.ItemByName[ObjectName].IsUseProjectID)
  then
    Result:=gl_DB.GetIntFieldValue (TableName, FLD_ID,
           [db_Par(FLD_NAME,       aName),
            db_Par(FLD_FOLDER_ID,  aFolderID),
            db_Par(FLD_PROJECT_ID, dmMain.ProjectID) ])
  else
    Result:=gl_DB.GetIntFieldValue (TableName, FLD_ID,
           [db_Par(FLD_NAME,      aName),
            db_Par(FLD_FOLDER_ID, aFolderID)  ]);
end;


procedure TdmObject_base.Update_ (aID: integer; aParams: array of TDBParamRec);
begin
   gl_DB.UpdateRecord (TableName, aID, aParams);
end;


procedure TdmObject_base.Update (aID: integer; aFieldName: string; aFieldValue: Variant);
begin
   gl_DB.UpdateRecord (TableName, aID, [db_Par(aFieldName, aFieldValue)]);
end;

//-------------------------------------------------------------------
function TdmObject_base.Del (aID: integer): boolean;
//-------------------------------------------------------------------
begin
//  Result:=ExecCommand ('DELETE FROM ' + aTableName + ' WHERE id=:id',   [db_Par('id', aID)]);

//  Result := dmOnega_DB_data.Object_GetNewName_select (ObjectName, dmMain.ProjectID);

 	Result:= gl_db.DeleteRecordByID (TableName, aID);
end;

//-------------------------------------------------------------------
procedure TdmObject_base.Del_Mark(aID: integer);
//-------------------------------------------------------------------
var
  s: string;
begin
  s:='update ' + TableName + ' set is_deleted=1 WHERE id=' + IntToStr(aID);

  dmOnega_DB_data.ADOConnection.Execute(s);

//  Result := dmOnega_DB_data.Object_GetNewName_select (ObjectName, dmMain.ProjectID);

// 	Result:= gl_db.DeleteRecordByID (TableName, aID);
end;


//-------------------------------------------------------------------
function TdmObject_base.FindByName(aName: string): integer;
//-------------------------------------------------------------------
begin
  Assert(aName<>'', 'aName=''''');
  Assert(ObjectName<>'', 'ObjectName=''''');

//  if (ObjectName='') then   raise Exception.Create('');

  if  (g_Obj.ItemByName[ObjectName].IsUseProjectID)
  then
    Result:=gl_DB.GetIntFieldValue (TableName, FLD_ID,
                                   [db_Par(FLD_NAME, aName),
                                    db_Par(FLD_PROJECT_ID, dmMain.ProjectID) ])
  else
     Result:=GetIDByName (aName);

end;


//-------------------------------------------------------------------
function TdmObject_base.GetIntFieldValue (aID: integer; aFieldName: string): integer;
//-------------------------------------------------------------------
begin
  Result:=gl_DB.GetIntFieldValue (TableName, aFieldName, [db_Par(FLD_ID, aID) ]);
end;

//-------------------------------------------------------------------
function TdmObject_base.GetDoubleFieldValue (aID: integer; aFieldName: string): Double;
//-------------------------------------------------------------------
begin
  Result:=gl_DB.GetDoubleFieldValue (TableName, aFieldName, [db_Par(FLD_ID, aID) ]);
end;

//-------------------------------------------------------------------
function TdmObject_base.GetBoolFieldValue(aID: integer; aFieldName: string): boolean;
//-------------------------------------------------------------------
begin
  Result:=gl_DB.GetBoolFieldValue (TableName, aFieldName, [db_Par(FLD_ID, aID) ]);
end;

//-------------------------------------------------------------------
function TdmObject_base.GetStringFieldValue (aID: integer; aFieldName: string): string;
//-------------------------------------------------------------------
begin
  Result:=gl_DB.GetStringFieldValue (TableName, aFieldName, [db_Par(FLD_ID, aID) ]);
end;


begin

end.



