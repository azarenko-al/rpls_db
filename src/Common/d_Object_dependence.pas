unit d_Object_dependence;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  Grids, DBGrids, DB, ADODB,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,


  dm_Onega_DB_data,

//  dm_Main,

//  u_const_db,
//  u_types,

  u_db,
  u_func, rxPlacemnt
  ;

type
  Tdlg_Object_dependence = class(TForm)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    DataSOurce: TDataSource;
    col_ID: TcxGridDBColumn;
    cxGrid1DBTableView1guid: TcxGridDBColumn;
    col_name: TcxGridDBColumn;
    col_objname: TcxGridDBColumn;
    FormPlacement1: TFormPlacement;
    col_objcaption: TcxGridDBColumn;
    ADOStoredProc1: TADOStoredProc;
    procedure FormCreate(Sender: TObject);
    procedure qry_ItemsCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    class procedure ExecDlg(aObjectName, aName: string; aID: integer);
    class procedure ExecDlg_new(aProcName, aName: string; aID: integer);

  end;


implementation
{$R *.dfm}


//--------------------------------------------------------------
class procedure Tdlg_Object_dependence.ExecDlg(aObjectName, aName: string; aID: integer);
//--------------------------------------------------------------
begin
  with Tdlg_Object_dependence.Create(Application) do
  try
    Caption:='����������� �������: '+ aName;

    dmOnega_DB_data.Object_Dependency (ADOStoredProc1, aObjectName, aID);

    cxGrid1DBTableView1.ViewData.Expand(True);

  //  CursorDefault;

    ShowModal;

  finally
    Free;
  end;
end;

//--------------------------------------------------------------
class procedure Tdlg_Object_dependence.ExecDlg_new(aProcName, aName: string;
    aID: integer);
//--------------------------------------------------------------
begin
  with Tdlg_Object_dependence.Create(Application) do
  try
    Caption:='����������� �������: '+ aName;
(*
    dmOnega_DB_data.OpenStoredProc(ADOStoredProc1,
       aProcName,
       [db_Par(FLD_ID,  aID)]);

//    sp_Object_Dependency (ADOStoredProc1, aProcName, aID);
*)
    cxGrid1DBTableView1.ViewData.Expand(True);

  //  CursorDefault;

    ShowModal;

  finally
    Free;
  end;
end;


procedure Tdlg_Object_dependence.FormCreate(Sender: TObject);
begin
  cxGrid1.Align:= alClient;
//  db_SetComponentADOConnection(Self, dmMain.ADOConnection);
end;



procedure Tdlg_Object_dependence.qry_ItemsCalcFields(DataSet: TDataSet);
var
  s: string;
begin

(*  s:= DataSet[FLD_OBJNAME];
  if g_Obj.IsObjectNameExist(s) then
    DataSet[FLD_OBJCAPTION]:=g_Obj.ItemByName[s].RootFolderName;
*)
end;


end.