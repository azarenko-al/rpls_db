unit dm_act_Base;

interface
{$I ver.inc}

uses
  SysUtils, Classes, menus, ActnList, Forms,  Dialogs,

  dm_Settings,

  u_Shell_new,

 // d_Audit_new,

  d_Folder_Select,

  I_MapAct,  //  dm_Act_Map,

 // dm_MapEngine,
//  dm_Object_Manager,

  I_Object_list,

  I_Object,
  I_Shell,
 // u_Shell_new,

  dm_act_Folder,
  dm_Folder,

  dm_Object_Base,

  u_Explorer,
  u_types,

  u_func_msg,
      
  u_classes,

  u_dlg,
  u_func,
  u_Geo,

  u_const_str
  ;


type
  TOnExplorerEvent = procedure(aPIDL: TrpPIDL; aOwnerForm: TForm) of object;
//  TOnExplorerEvent = procedure(aPIDL: TrpPIDL; aOwnerForm: TForm) of object;

//  TMenuType = (mtFolder,mtItem,mtList);
  TNodeType = (ntFolder,ntItem);


  TdmAct_Base = class(TDataModule, IMessageHandler, IObjectHandlerX)
    ActionList1: TActionList;
    act_Add: TAction;
    act_Del_: TAction;
    act_Object_ShowOnMap: TAction;
    act_Del_list: TAction;
    act_GroupAssign: TAction;
    act_GoToProperty: TAction;
    ActionList_Folders: TActionList;
    act_Folder_Add: TAction;
    act_Folder_Del: TAction;
    act_Folder_Rename: TAction;
    act_Folder_MoveTo: TAction;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FObjectName: string;

    FID: integer;

    procedure AddActionNamesToList(aActArray: array of TAction; var aList:
        TStringList);
    procedure DoExecAction (Sender: TObject);
    procedure DoExecFolderAction(Sender: TObject);

    procedure SelectedPIDLs_to_SelectedIDList ();


    //IObjectHandler
    procedure OBJECT_GET_POPUP_MENU(aPopupMenu: TPopupMenu; aPIDLs: TrpPIDL_List; var aHandled:
        boolean);
    procedure OBJECT_GET_POPUP_MENU_new(aPopupMenu: TPopupMenu; aPIDLs:
        TrpPIDL_List);

  protected
    FOnDeleteList: TNotifyEvent;


    function GetObjectName: WideString;
    procedure SetObjectName(const Value: WideString);

  { TODO : ???? }

  protected
    //check project is ReadOnly
    FCheckActionsArr: array of TAction;


    FPIDL: TrpPIDL;
    FFolderID: integer;
    FIsFolder: boolean;

    //1 ������ ��������� �������� - ��� MapEngine
    FDeletedIDList: TIDList;

    //for actions
    FFocusedID: integer;
    FFocusedGUID: string;
    FFocusedName: string;

    FSelectedPIDLs:  TrpPIDL_List;
    FSelectedIDList: TIDList;


    procedure ShowRectOnMap(aBLRect: TBLRect; aID: integer);
    procedure ShowPointOnMap(aBLPoint: TBLPoint; aID: integer);
    procedure ShowVectorOnMap(aBLVector: TBLVector; aID: integer);


    function AddMenuItem (aOwner: TObject; aAction: TBasicAction=nil): TMenuItem;

    procedure AddFolderPopupMenu(aPopupMenu: TPopupMenu; aEnabled: Boolean = true);
    procedure AddFolderMenu_Create(aPopupMenu: TPopupMenu; aEnabled: Boolean =
        true);

    procedure AddFolderMenu_Tools(aPopupMenu: TPopupMenu);
//    procedure AddFolderMenu_Tools(aPopupMenu: TPopupMenu; aSelectedPIDLs:  TrpPIDL_List);

//    procedure Dlg_Audit(aTableName: string; aID: Integer);


    function ItemDel(aID: integer; aName: string = ''): boolean; virtual; abstract;
    function ItemDelList(aIDList: TIDList): boolean;virtual; abstract;

    function ItemAdd (aParentID: integer): integer; virtual; abstract;

//    procedure GetMessage(aMsg: integer; aParams: TEventParamList); virtual;
    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList; var aHandled:
        boolean); virtual;
//    procedure GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean); virtual;



    procedure Del_list ();
    procedure Dlg_MoveTo(aPIDLs: TrpPIDL_List);
    procedure SetCheckedActionsArr(aActionArr: array of TAction);


  public
    function  GetViewForm  (aOwnerForm: TForm): TForm; virtual;
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); virtual; //abstract;

    function ViewInProjectTree(aID: integer): TStrArray; virtual;

  public
    procedure Add (aFolderID: integer); virtual; // aFolderGUID: string='');

    procedure Del(aID: integer; aGUID: string = ''; aName: string = '');

// TODO: PostShowInExplorer
//  procedure PostShowInExplorer (aObjType: TrpObjectType; aID: integer);

    function Dlg_DelByList(aIDListWithGuids: TIDList): boolean;

    function IsObjectSupported(aObjectName: WideString): Boolean; virtual;

    property ObjectName: WideString read GetObjectName write SetObjectName;


    property OnDeleteList: TNotifyEvent read FOnDeleteList write FOnDeleteList;

  //   FOnDeleteList: TNotifyEvent;


  end;


var
  dmAct_Base: TdmAct_Base;


//====================================================================
// implementation
//====================================================================

implementation  {$R *.dfm}


//--------------------------------------------------------------------
procedure TdmAct_Base.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  FSelectedPIDLs:=TrpPIDL_List.Create;
  FSelectedIDList:=TIDList.Create;
  FDeletedIDList := TIDList.Create();

  act_Object_ShowOnMap.Caption:='�������� �� �����';

  act_Add.Caption:=STR_ITEM_ADD; // STR_ADD;
  act_Del_.Caption:=STR_ACT_DEl;
  act_Del_list.Caption:='������� ������';

  SetComponentActionsExecuteProc (Self, DoExecAction);

  g_Objects1.Add_(Self);



  act_Folder_Add.Caption:=STR_FOLDER_ADD_;
  act_Folder_Del.Caption:=STR_FOLDER_DEL_;
  act_Folder_Rename.Caption:=STR_FOLDER_RENAME;
  act_Folder_MoveTo.Caption:=STR_FOLDER_MOVE_TO;


  SetActionsExecuteProc
               ([act_Folder_Add,
                 act_Folder_Del,
                 act_Folder_Rename,
                 act_Folder_MoveTo
                 ],
                DoExecFolderAction);


 // dmObject_Manager.RegisterObject(Self);   //u_object_manager

 // g_ClassManager.RegisterDataModule (Self);

end;

// ---------------------------------------------------------------
procedure TdmAct_Base.DataModuleDestroy(Sender: TObject);
// ---------------------------------------------------------------
var
  s: string;
begin
  g_Objects1.Remove(Self);

 // g_ClassManager.UnRegisterDataModule (Self);  //u_object_manager

  s:=FObjectName;


  FreeAndNil(FDeletedIDList);
  FreeAndNil(FSelectedPIDLs);
  FreeAndNil(FSelectedIDList);

 // dmObject_Manager.UnRegisterObject(Self);

  inherited;
end;
             

// ---------------------------------------------------------------
procedure TdmAct_Base.ShowRectOnMap (aBLRect: TBLRect; aID: integer);
// ---------------------------------------------------------------
begin

  Assert(Assigned(IAct_Map));
  IAct_Map.ShowForm;

  g_Act_Map.ShowForm;


  if Assigned(IActiveMapView) then
  begin
    IActiveMapView.MAP_SHOW_BLRECT (aBLRect);
    IActiveMapView.SelectObjectByID (ObjectName, NULL_BLPOINT, aID);
  end;

end;

// ---------------------------------------------------------------
procedure TdmAct_Base.ShowPointOnMap(aBLPoint: TBLPoint; aID: integer);
// ---------------------------------------------------------------
begin
  Assert(Assigned(IAct_Map));
  IAct_Map.ShowForm;

  g_Act_Map.ShowForm;


  if Assigned(IActiveMapView) then
  begin
    g_Act_Map.MAP_SHOW_POINT (aBLPoint);
    g_Act_Map.SelectObjectByID (ObjectName, aBLPoint, aID);


    IActiveMapView.MAP_SHOW_POINT (aBLPoint);
    IActiveMapView.SelectObjectByID (ObjectName, aBLPoint, aID);
  end;
end;

// ---------------------------------------------------------------
procedure TdmAct_Base.ShowVectorOnMap(aBLVector: TBLVector; aID: integer);
// ---------------------------------------------------------------
begin
  Assert(Assigned(IAct_Map));
  IAct_Map.ShowForm;

  g_Act_Map.ShowForm;

  if Assigned(IActiveMapView) then
  begin
    g_Act_Map.MAP_SHOW_VECTOR (aBLVector);
    g_Act_Map.SelectObjectByID (ObjectName, NULL_BLPOINT, aID);

    IActiveMapView.MAP_SHOW_VECTOR (aBLVector);
    IActiveMapView.SelectObjectByID (ObjectName, NULL_BLPOINT, aID);
  end;

end;

//--------------------------------------------------------------------
procedure TdmAct_Base.DoExecAction (Sender: TObject);
//--------------------------------------------------------------------
var
  s: string;
begin
  s:=FObjectName;

  try

  if Sender=act_Add     then Add (FFocusedID) else
  if Sender=act_Del_    then Del (FFocusedID, FFocusedGUID, FFocusedName ) else

  if Sender=act_Del_list then
    Del_List ()
   // else

   except
   end;

end;


function TdmAct_Base.AddMenuItem (aOwner: TObject; aAction: TBasicAction=nil): TMenuItem;
begin
  if Assigned(aAction) then
    aAction.Tag:=Integer(FSelectedPIDLs);

  Result:=dlg_AddMenuItem (aOwner, aAction);
end;


//--------------------------------------------------------------------
procedure TdmAct_Base.AddFolderPopupMenu(aPopupMenu: TPopupMenu; aEnabled:
    Boolean = true);
//--------------------------------------------------------------------
begin
  if aEnabled then
    dmAct_Folder.AddFolderPopupMenu(aPopupMenu, FSelectedPIDLs);

end;


//--------------------------------------------------------------------
procedure TdmAct_Base.AddFolderMenu_Create(aPopupMenu: TPopupMenu; aEnabled:
    Boolean = true);
//--------------------------------------------------------------------
var
  oMenuItem: TMenuItem;
begin
  if not aEnabled then
    Exit; 

//  oMenuItem:=dlg_AddMenuItem (aPopupMenu, nil, '�������');
  dlg_AddMenuItem (aPopupMenu, act_Add);
//  dlg_AddMenuItem (oMenuItem, dmAct_Folder.act_Folder_Add);

  act_Add.Tag:=integer(FSelectedPIDLs);
//  dmAct_Folder.act_Folder_Add.Tag:=integer(FSelectedPIDLs);

end;

//--------------------------------------------------------------------
procedure TdmAct_Base.AddFolderMenu_Tools(aPopupMenu: TPopupMenu);
//--------------------------------------------------------------------
begin
//  if aEnabled then
///  begin
    AddMenuDelimiter (aPopupMenu);
    dlg_AddMenuItem (aPopupMenu, act_Folder_MoveTo);

    act_Folder_MoveTo.Enabled:= (FSelectedPIDLs.Count>0);
  //  act_Folder_MoveTo.Enabled:= act_Folder_MoveTo.Enabled and (aSelectedPIDLs.Count>0);
  //  act_Folder_MoveTo.Enabled:=(FFocusedID>0);
    act_Folder_MoveTo.Tag:=integer(FSelectedPIDLs);

 // end;


//    AddFolderMenu_Tools(aPopupMenu, FSelectedPIDLs);
//    dmAct_Folder.AddFolderMenu_Tools(aPopupMenu, FSelectedPIDLs);
end;


  {
//--------------------------------------------------------------------
procedure TdmAct_Base.AddFolderMenu_Tools(aPopupMenu: TPopupMenu;
    aSelectedPIDLs: TrpPIDL_List);
//--------------------------------------------------------------------
//var oMenuItem: TMenuItem;
begin
  AddMenuDelimiter (aPopupMenu);
  dlg_AddMenuItem (aPopupMenu, act_Folder_MoveTo);

  act_Folder_MoveTo.Enabled:= (aSelectedPIDLs.Count>0);
//  act_Folder_MoveTo.Enabled:= act_Folder_MoveTo.Enabled and (aSelectedPIDLs.Count>0);
//  act_Folder_MoveTo.Enabled:=(FFocusedID>0);
  act_Folder_MoveTo.Tag:=integer(aSelectedPIDLs);


 // SetFormActionsEnabled(Self);


end;
   }

//--------------------------------------------------------------------
procedure TdmAct_Base.Del(aID: integer; aGUID: string = ''; aName: string = '');
//--------------------------------------------------------------------
var
  ot : TrpObjectType;

  oItem: TIDListItem;
begin

  if not ConfirmDlg (STR_ASK_DEL) then
    Exit;

//  CursorHourGlass;

  FDeletedIDList.Clear;
  oItem:=FDeletedIDList.AddObjNameAndGUID1(aID, ObjectName, aGUID);
  oItem.Name:= aName;


  if ItemDel (aID, aName) then
  begin
  //  if g_IsDebug then
   //  FDeletedIDList.DlgShow;

 //   g_ShellEvents.Shell_PostDelNode(aGUID);

    // delete this object
    g_ShellEvents.Shell_PostDelNode(aGUID);
    g_Shell.PostDelNode(aGUID);

    g_ShellEvents.Shell_PostDelNodesByIDList(FDeletedIDList);
  //  g_Shell.Shell_PostDelNodesByIDList(FDeletedIDList);

    ot:=g_Obj.ItemByName[ObjectName].Type_;

(*
  	if FoundInMapObjects(ot) then
    begin
     // if Assigned(IMapEngine) then
        dmMapEngine.DelList(FDeletedIDList);

    //  if Assigned(IMapAct) then
      dmAct_Map.MapRefresh;
    end;
*)

  end else
   ;
//    raise Exception.Create('procedure TdmAct_Base.Del.  ������ ��� ��������.');

 // CursorDefault;

   if Assigned(FOnDeleteList) then
     FOnDeleteList(nil);


end;


//--------------------------------------------------------------------
procedure TdmAct_Base.Del_List ();
//--------------------------------------------------------------------
var
  i: integer;
  iMax: Integer;
  s: string;

  oItem: TIDListItem;
begin
  iMax:=dmSettings.ParamByName(DEF_LIST_delete_max_count);

  //     ghjfgjhfgjhfgh
   if iMax < FSelectedPIDLs.Count then
   begin
     ShowMessage ('���������� ����� �� �������� �������: ' + IntToStr(iMax));
     Exit;
   end;

  if not ConfirmDlg (STR_ASK_DEL_LIST) then
    Exit;


  FDeletedIDList.Clear;


  with FSelectedPIDLs do
    for i:=0 to Count-1 do
    begin
     // oItem:=Items[i];

      s:=Items[i].ObjectName;

      if (not Items[i].IsFolder) and Eq(Items[i].ObjectName,ObjectName) then
        if (ItemDel (Items[i].ID, Items[i].Name)) then
        begin
          oItem:=FDeletedIDList.AddObjNameAndGUID1(Items[i].ID, Items[i].ObjectName, Items[i].GUID);
          oItem.Name:=Items[i].Name;
        end;

    end;

    {
  if g_IsDebug then
   FDeletedIDList.DlgShow;
   }

   if Assigned(FOnDeleteList) then
     FOnDeleteList(nil);




//  if Assigned(IMapEngine) then
 //// dmMapEngine.DelList(FDeletedIDList);

//    dmMapEngine1.DelObjectsByList(FDeletedIDList);

 // if Assigned(IMapAct) then
 ////// dmAct_Map.MapRefresh;

  g_ShellEvents.Shell_PostDelNodesByIDList(FDeletedIDList);
 // g_Shell.Shell_PostDelNodesByIDList(FDeletedIDList);

end;

//--------------------------------------------------------------------
function TdmAct_Base.Dlg_DelByList(aIDListWithGuids: TIDList): boolean;
//--------------------------------------------------------------------
var i: integer;
  s: string;
//   rec: TShellEventRec;
begin
  Result:= false;

  if ConfirmDlg (STR_ASK_DEL) then
  begin
    with aIDListWithGuids do
      for i:=0 to Count-1 do
        if ItemDel (Items[i].ID, Items[i].Name ) then
        begin
          Result:= true;

          s:=Items[i].GUID;

          Assert(Items[i].GUID<>'');

//          rec.GUID:=Items[i].GUID;
          g_ShellEvents.Shell_PostDelNode(Items[i].GUID);
          g_Shell.PostDelNode(Items[i].GUID);


       //   PostEvent (WE_EXPLORER_DELETE_NODE_BY_GUID,  [app_Par(PAR_GUID, Items[i].GUID) ]);
//

         // SH_PostDelNode (Items[i].GUID);
      ////    PostSyncDB_DelObject (Items[i].ID);
        end;

    g_ShellEvents.Shell_PostDelNodesByIDList(aIDListWithGuids);

  //  g_Shell.Shell_PostDelNodesByIDList(aIDListWithGuids);


  end;
end;

//--------------------------------------------------------------------
procedure TdmAct_Base.SelectedPIDLs_to_SelectedIDList ();
//--------------------------------------------------------------------
var i: integer;
  obj: TIDListItem;
begin
//g_ShellEvents
  FSelectedIDList.Clear;

  with FSelectedPIDLs do
    for i:=0 to Count-1 do
      if  //(not Items[i].IsFolder) and
         Eq(Items[i].ObjectName,ObjectName)
      then begin
        obj:=FSelectedIDList.AddName (Items[i].ID, Items[i].Name);
        obj.GUID:=Items[i].GUID;
        obj.IsFolder:=Items[i].IsFolder;
      end;
end;


//--------------------------------------------------------------------
procedure TdmAct_Base.Add (aFolderID: integer);
//--------------------------------------------------------------------
var
  sFolderGUID: string;
begin
  FID:=ItemAdd (aFolderID);


  if FID>0 then
  begin
    if aFolderID=0 then
      sFolderGUID:=g_Obj.ItemByName[ObjectName].RootFolderGUID
    else
      sFolderGUID:=dmFolder.GetGUIDByID(aFolderID);


    g_Shell.UpdateNodeChildren_ByGUID (sFolderGUID);
    g_Shell.EXPAND_BY_GUID(sFolderGUID);

    g_Shell.FocuseNodeByObjName (ObjectName, FID);

  end;

  {

  exit;  // !!!!!!!!!!


  if FID>0 then
  begin
    if aFolderID=0 then
      sFolderGUID:=g_Obj.ItemByName[ObjectName].RootFolderGUID
    else
      sFolderGUID:=dmFolder.GetGUIDByID(aFolderID);


//       .Shell_UpdateNodeChildren_ByGUID (sFolderGUID);
    g_Shell.UpdateNodeChildren_ByGUID (sFolderGUID);
   // g_Shell.

  //  SH_PostUpdateNodeChildren (sFolderGUID);
  end;
  }
end;


function TdmAct_Base.ViewInProjectTree(aID: integer): TStrArray;
begin
 // raise Exception.Create('procedure TdmAct_Base.ViewInProjectTree (aID: integer);');
end;


procedure TdmAct_Base.AddActionNamesToList(aActArray: array of TAction; var
    aList: TStringList);
var
  i: Integer;
begin
  for I := 0 to High(aActArray) do
    if aList.IndexOf(aActArray[i].Name)=-1 then
      aList.Add(aActArray[i].Name);
end;

{
procedure TdmAct_Base.Dlg_Audit(aTableName: string; aID: Integer);
begin
   Tdlg_Audit_new.ExecDlg (aTableName, aID)
end;

}

function TdmAct_Base.GetViewForm (aOwnerForm: TForm): TForm;
begin
  Result:=nil;
end;


procedure TdmAct_Base.GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); //virtual; abstract;
begin
// !!
end;


//--------------------------------------------------------------------
procedure TdmAct_Base.GetMessage(aMsg: TEventType; aParams: TEventParamList;
    var aHandled: boolean);
//--------------------------------------------------------------------
                                                                                 
var
  sObjName: string;
  oPIDL: TrpPIDL;
  oPopupMenu: TPopupMenu;
  iID: integer;
  oPIDLs: TrpPIDL_List;

begin
  //---------------------------------------------------------
  case aMsg of
  //---------------------------------------------------------
(*
    //---------------------------------------------------------
    WE_OBJECT_FOCUS_IN_PROJECT_TREE11:
    //---------------------------------------------------------
    begin
      iID     :=aParams.IntByName(PAR_ID);
      sObjName:=aParams.ValueByName(PAR_OBJECT_NAME);

      if IsObjectSupported(sObjName) then
      begin
        ViewInProjectTree (iID);

        aHandled:=True;
      end;
    end;
*)

    //---------------------------------------------------------
    WE_OBJECT_GET_POPUP_MENU: begin
    //---------------------------------------------------------

      oPopupMenu:=TPopupMenu(Integer(aParams.ValueByName('POPUP_MENU')));
      oPIDLs    :=TrpPIDL_List (Integer(aParams.ValueByName('PIDL_LIST')));

       Assert(oPopupMenu<>nil);
       Assert(oPIDLs<>nil);

      OBJECT_GET_POPUP_MENU (oPopupMenu, oPIDLs, aHandled);
    end;
  end;
end;


//-------------------------------------------------------------------
function TdmAct_Base.IsObjectSupported(aObjectName: WideString): Boolean;
//-------------------------------------------------------------------
begin
  result := Eq(aObjectName, ObjectName);
end;

// ---------------------------------------------------------------
procedure TdmAct_Base.SetCheckedActionsArr(aActionArr: array of TAction);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  SetLength (FCheckActionsArr, Length(aActionArr));
  for I := 0 to High(aActionArr) do
    FCheckActionsArr[i] := aActionArr[i];
end;

//-------------------------------------------------------------------
procedure TdmAct_Base.OBJECT_GET_POPUP_MENU (aPopupMenu: TPopupMenu;
                                            aPIDLs: TrpPIDL_List;
                                            var aHandled: boolean);
//-------------------------------------------------------------------
var
//  bReadOnly: Boolean;
  sObjName: string;
  oPIDL: TrpPIDL;
  iID: integer;
  s: string;

begin
  Assert(aPopupMenu<>nil);

  aPopupMenu.Items.Clear;

  if (not Assigned(aPIDLs)) or (aPIDLs.Count=0) then
    Exit;

  //get first item
  oPIDL:=aPIDLs[0];
  sObjName:=oPIDL.ObjectName;

  FFocusedID  :=oPIDL.ID;
  FFocusedGUID:=oPIDL.GUID;
  FFocusedName:=oPIDL.Name;

  s:=ObjectName;

  if IsObjectSupported(sObjName) then
//  if sObjName=ObjectName then
  begin
    aHandled:=True;

    aPopupMenu.Items.Clear;
    FSelectedPIDLs.Clear;
    FSelectedIDList.Clear;

{       FSelectedPIDLs.Assign (oPIDLs);
    SelectedPIDLs_to_SelectedIDList();
}
    //������ ������� ������� �������
    if (aPIDLs.Count>1) and (aPIDLs[0].ID=0) then
      Exit;

    //������ 1 folder ����� �������
    if (aPIDLs.Count>1) and (aPIDLs[0].IsFolder=True) then
      Exit;

    FSelectedPIDLs.Assign (aPIDLs);
    SelectedPIDLs_to_SelectedIDList();

{
    // -------------------------
    bReadOnly := False;

    if g_Obj.ItemByName[sObjName].IsUseProjectID then
      bReadOnly := dmUser_Security.ProjectIsReadOnly;

   // if bReadOnly then
    //  SetActionsEnabled1 (FCheckActionsArr, not bReadOnly);


    if (aPIDLs.Count=1) and
        g_Obj.ItemByName[sObjName].IsHasGeoRegionID then
    begin
      bReadOnly :=
        dmUser_Security.GeoRegion_is_ReadOnly(oPIDL.GeoRegion_ID) or
        dmUser_Security.GeoRegion_is_ReadOnly(oPIDL.GeoRegion1_ID) or
        dmUser_Security.GeoRegion_is_ReadOnly(oPIDL.GeoRegion2_ID) ;

    //  if (oPIDL.IsFolder) then GetPopupMenu (aPopupMenu, mtFolder)
     //                     else GetPopupMenu (aPopupMenu, mtItem);

      //IsHasGeoRegionID
    end;
    // -------------------------

//  if bReadOnly then
    SetActionsEnabled1 (FCheckActionsArr, not bReadOnly);


    dmAct_Folder.SetAllActionsEnabled(not bReadOnly);
 }
//    ([
//        act_Add,
//        act_Del_,
//
//        act_Antenna_Add,
//        act_Copy,
//        act_GroupAssign,
//
//        act_Change_LinkEndType
//     ],
//     not dmUser_Security.ProjectIsReadOnly);



    if (aPIDLs.Count=1) then
    begin
      if (oPIDL.IsFolder) then GetPopupMenu (aPopupMenu, mtFolder)
                          else GetPopupMenu (aPopupMenu, mtItem);
    end else
      GetPopupMenu (aPopupMenu, mtList);
  end;
// end;


end;



//-------------------------------------------------------------------
procedure TdmAct_Base.OBJECT_GET_POPUP_MENU_new(aPopupMenu: TPopupMenu; aPIDLs:
    TrpPIDL_List);
//-------------------------------------------------------------------
var
//  bReadOnly: Boolean;
  sObjName: string;
  oPIDL: TrpPIDL;
  iID: integer;

begin
  Assert(aPopupMenu<>nil);

  aPopupMenu.Items.Clear;

  if (not Assigned(aPIDLs)) or (aPIDLs.Count=0) then
    Exit;

  //get first item
  oPIDL:=aPIDLs[0];
  sObjName:=oPIDL.ObjectName;

  FFocusedID  :=oPIDL.ID;
  FFocusedGUID:=oPIDL.GUID;
  FFocusedName:=oPIDL.Name;



 ///////// if IsObjectSupported(sObjName) then

//  if sObjName=ObjectName then
  begin
   ///// aHandled:=True;

    aPopupMenu.Items.Clear;
    FSelectedPIDLs.Clear;
    FSelectedIDList.Clear;

{       FSelectedPIDLs.Assign (oPIDLs);
    SelectedPIDLs_to_SelectedIDList();
}
    //������ ������� ������� �������
    if (aPIDLs.Count>1) and (aPIDLs[0].ID=0) then
      Exit;

    //������ 1 folder ����� �������
    if (aPIDLs.Count>1) and (aPIDLs[0].IsFolder=True) then
      Exit;

    FSelectedPIDLs.Assign (aPIDLs);
    SelectedPIDLs_to_SelectedIDList();

    // -------------------------
 {
   bReadOnly := False;

   
    if g_Obj.ItemByName[sObjName].IsUseProjectID then
      bReadOnly := dmUser_Security.ProjectIsReadOnly;

   // if bReadOnly then
    //  SetActionsEnabled1 (FCheckActionsArr, not bReadOnly);


    if (aPIDLs.Count=1) and
        g_Obj.ItemByName[sObjName].IsHasGeoRegionID then
    begin
      bReadOnly :=
        dmUser_Security.GeoRegion_is_ReadOnly(oPIDL.GeoRegion_ID) or
        dmUser_Security.GeoRegion_is_ReadOnly(oPIDL.GeoRegion1_ID) or
        dmUser_Security.GeoRegion_is_ReadOnly(oPIDL.GeoRegion2_ID) ;

    //  if (oPIDL.IsFolder) then GetPopupMenu (aPopupMenu, mtFolder)
     //                     else GetPopupMenu (aPopupMenu, mtItem);

      //IsHasGeoRegionID
    end;
    // -------------------------

//  if bReadOnly then
    SetActionsEnabled1 (FCheckActionsArr, not bReadOnly);

    dmAct_Folder.SetAllActionsEnabled(not bReadOnly);
 }

//    ([
//        act_Add,
//        act_Del_,
//
//        act_Antenna_Add,
//        act_Copy,
//        act_GroupAssign,
//
//        act_Change_LinkEndType
//     ],
//     not dmUser_Security.ProjectIsReadOnly);



    if (aPIDLs.Count=1) then
    begin
      if (oPIDL.IsFolder) then GetPopupMenu (aPopupMenu, mtFolder)
                          else GetPopupMenu (aPopupMenu, mtItem);
    end else
      GetPopupMenu (aPopupMenu, mtList);
  end;
// end;


end;


// -------------------------------------------------------------------
//IObjectHandler
// -------------------------------------------------------------------

function TdmAct_Base.GetObjectName: WideString;
begin
  result:=FObjectName;
end;

procedure TdmAct_Base.SetObjectName(const Value: WideString);
begin
  FObjectName:=Value;
end;


//--------------------------------------------------------------------
procedure TdmAct_Base.Dlg_MoveTo(aPIDLs: TrpPIDL_List);
//--------------------------------------------------------------------
var
  i,iFolderID: integer;
  k: Integer;
  sName,sFolderGUID,sObjName: string;

begin
  if aPIDLs.Count=0 then
    Exit;

  sObjName:= aPIDLs[0].ObjectName;

  iFolderID:= Tdlg_Folder_Select.ExecDlg (sObjName,sName);
  if (iFolderID < 0) then
    Exit;

 // if aPIDLs[0].IsFolder then
 // begin
{
    k:=dmFolder.MoveToFolder (aPIDLs.Items[0].ID, iFolderID);

    if k<=0 then
      ErrorDlg('������ ��� ����������� ����� '+ aPIDLs.Items[0].name);
}

 //   Exit;

    {
    if dmFolder.FolderIsChildOf(iFolderID, aPIDLs[0].ID) then
    begin
      MsgDlg('���������� ����������� �����');
      Exit;
    end;
     }
 /// end else
{
  if (iFolderID = aPIDLs[0].ID) then
  begin
    MsgDlg('������ ����������� ����� ���� � ����');
    Exit;
  end;
 }

    with aPIDLs do
      for i:=0 to Count-1 do
        if Eq(Items[i].ObjectName, sObjName) then
        begin
          if Items[i].IsFolder then
          begin
            k:=dmFolder.MoveToFolder (aPIDLs.Items[i].ID, iFolderID);

            if k<=0 then
            begin
            //sName
               ErrorDlg(Format('������ ��� ����������� �����: %s -> %s   (%d)->(%d)', [aPIDLs.Items[i].name, sName, aPIDLs.Items[i].ID, iFolderID ]));
//               ErrorDlg(Format('������ ��� ����������� �����: %s -> %s', [aPIDLs.Items[i].name, sName]));
               exit;
            end;


          ///////  dmFolder.MoveToFolder (Items[i].ID, iFolderID);

    //        g_ShellEvents.Shell_PostDelNode(Items[i].GUID);
        ////    PostEvent (WE_EXPLORER_DELETE_NODE_BY_GUID,  [app_Par(PAR_GUID, Items[i].GUID) ]);
  //          SH_PostDelNode (Items[i].GUID);
          end else begin
            dmFolder.MoveObjectToFolder (Items[i].ObjectName, Items[i].ID, iFolderID);
  //          SH_PostDelNode (Items[i].GUID);

  //          g_ShellEvents.Shell_PostDelNode(Items[i].GUID);
           // PostEvent (WE_EXPLORER_DELETE_NODE_BY_GUID,  [app_Par(PAR_GUID, Items[i].GUID) ]);
  //
          end;

          g_ShellEvents.Shell_PostDelNode(Items[i].GUID);
          g_Shell.PostDelNode(Items[i].GUID);

        end;

  sFolderGUID:= dmFolder.GetGUIDByID (iFolderID);
  if sFolderGUID = '' then
    if g_Obj.IsObjectNameExist(sObjName) then
      sFolderGUID:= g_Obj.ItemByName[sObjName].RootFolderGUID;

  g_Shell.UpdateNodeChildren_ByGUID(sFolderGUID);
 // g_Shell.Shell_UpdateNodeChildren_ByGUID(sFolderGUID);


//  dmAct_Explorer.Shell_UpdateNodeChildren (sFolderGUID);
end;



//--------------------------------------------------------------------
procedure TdmAct_Base.DoExecFolderAction(Sender: TObject);
//--------------------------------------------------------------------
var
  sObjName: string;
  oSelectedPIDLs:  TrpPIDL_List;
  oPIDL: TrpPIDL;
  iFocusedID: integer;
  sFocusedGUID: string;
begin
 // if Sender=act_Add     then Add (iFocusedID) else
 // if Sender=act_Del_    then Del (iFocusedID, sFocusedGUID) else
 // if Sender=act_Del_list then Del_List () else

   if not Assigned(Sender) then
     Exit;

   oSelectedPIDLs:=TrpPIDL_List(TBasicAction(Sender).Tag);

   oPIDL:=oSelectedPIDLs[0];
   sObjName:=oPIDL.ObjectName;

   iFocusedID:=oPIDL.ID;
   sFocusedGUID:=oPIDL.GUID;


//  if Sender=act_Folder_Add    then Dlg_FolderAdd (sObjName,iFocusedID, sFocusedGUID) else
//  if Sender=act_Folder_Del    then Dlg_FolderDel (iFocusedID, sFocusedGUID) else
//  if Sender=act_Folder_Rename then Dlg_FolderRename (iFocusedID, sFocusedGUID) else

  if Sender=act_Folder_MoveTo then Dlg_MoveTo (oSelectedPIDLs) else

//  if Sender=act_GroupAssign   then Dlg_GroupAssignValues() else

end;



end.



