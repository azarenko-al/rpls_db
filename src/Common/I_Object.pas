unit I_Object;

interface
uses
  Forms,Menus,

  u_func,

  u_geo;


type
  TMenuType = (mtFolder,mtItem,mtList);

  IObjectHandlerX = interface(IInterface)
  ['{46376BB3-5A35-4F13-9D38-C0042DFFFA77}']
    function GetObjectName: WideString;
    procedure SetObjectName(const Value: WideString);

    function GetViewForm (aOwnerForm: TForm): TForm;
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType);

    //; aGUID: WideString
    function ViewInProjectTree(aID: integer): TStrArray;

    function IsObjectSupported(aObjectName: WideString): Boolean;

    property ObjectName: WideString read GetObjectName write SetObjectName;
  end;


implementation

end.
