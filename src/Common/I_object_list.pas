unit i_object_list;

interface
uses
  SysUtils, Classes, Forms,  Menus,

  u_func,

  

  i_object, u_explorer;

type
  TObjectInterfaceList = class(TInterfaceList)
  public
    function Add_(const aItem: IInterface): Integer;

    function GetViewForm(aObjectName: String; aOwnerForm: TForm): TForm;

    procedure GetPopupMenu_object(aObjectName: String; aPopupMenu: TPopupMenu;
        aMenuType: TMenuType);

    procedure GetPopupMenu_list(aPIDLs: TrpPIDL_List; aPopupMenu: TPopupMenu);

    function ViewInProjectTree(aObjectName: String; aID: integer): TStrArray;
  end;

var
  g_Objects1:  TObjectInterfaceList;


implementation

// ---------------------------------------------------------------
function TObjectInterfaceList.Add_(const aItem: IInterface): Integer;
// ---------------------------------------------------------------
var
  h: HRESULT;
  k: Integer;
   v: IObjectHandlerX;

  // v1: IDBExplorerFormX;
begin
  k:=Count;

  h:=aItem.QueryInterface(IObjectHandlerX, v);
  Assert(h=0);

//  h:=aItem.QueryInterface(IDBExplorerFormX, v1);
 // Assert(h=0);


  Result := Add(aItem);


  k:=Count;


end;

// ---------------------------------------------------------------
procedure TObjectInterfaceList.GetPopupMenu_list(aPIDLs: TrpPIDL_List;
    aPopupMenu: TPopupMenu);
// ---------------------------------------------------------------
var
  oPIDL: TrpPIDL;
  sObjName: string;
begin
  if (not Assigned(aPIDLs)) or (aPIDLs.Count=0) then
    Exit;

  //get first item
  oPIDL:=aPIDLs[0];
  sObjName:=oPIDL.ObjectName;




end;

// ---------------------------------------------------------------
procedure TObjectInterfaceList.GetPopupMenu_object(aObjectName: String;
    aPopupMenu: TPopupMenu; aMenuType: TMenuType);
// ---------------------------------------------------------------
var
  I: Integer;
  v: IObjectHandlerX;
begin
  for I := 0 to Count - 1 do
  begin
    v:= Items[i] as IObjectHandlerX  ;// is IShellNotifyX;

    if v.IsObjectSupported(aObjectName) then
    begin
      v.GetPopupMenu(aPopupMenu, aMenuType);
      Exit;
    end;
  end;

  raise Exception.Create('');

end;



// ---------------------------------------------------------------
function TObjectInterfaceList.GetViewForm(aObjectName: String; aOwnerForm:
    TForm): TForm;
// ---------------------------------------------------------------
var
  I: Integer;
  v: IObjectHandlerX;
begin
  for I := 0 to Count - 1 do
  begin
    v:= Items[i] as IObjectHandlerX  ;// is IShellNotifyX;
    if v.IsObjectSupported(aObjectName) then
    begin
      Result := v.GetViewForm(aOwnerForm);
      Exit;
    end;
  end;

  raise Exception.Create(aObjectName + ' -  function TObjectInterfaceList.GetViewForm(aObjectName: String; aOwnerForm:');


end;

// ---------------------------------------------------------------
function TObjectInterfaceList.ViewInProjectTree(aObjectName: String; aID:
    integer): TStrArray;
// ---------------------------------------------------------------
var
  I: Integer;
  v: IObjectHandlerX;
begin
  for I := 0 to Count - 1 do
  begin
    v:= Items[i] as IObjectHandlerX  ;// is IShellNotifyX;

    if v.IsObjectSupported(aObjectName) then
    begin
      Result := v.ViewInProjectTree(aID);
      Exit;
    end;
  end;

  raise Exception.Create('');


end;



initialization
  g_Objects1:=TObjectInterfaceList.Create;
Finalization
  Assert(g_Objects1.Count = 0);

//  ShowMessage(IntToStr(g_Objects1.Count));

  FreeAndNil(g_Objects1);


end.
