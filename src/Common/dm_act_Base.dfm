object dmAct_Base: TdmAct_Base
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 874
  Top = 555
  Height = 219
  Width = 439
  object ActionList1: TActionList
    Left = 144
    Top = 12
    object act_Add: TAction
      Category = 'common'
      Caption = 'act_Add'
    end
    object act_Del_: TAction
      Category = 'common'
      Caption = 'act_Del_'
    end
    object act_Object_ShowOnMap: TAction
      Category = 'common Map'
      Caption = 'act_Object_ShowOnMap'
    end
    object act_Del_list: TAction
      Caption = 'act_Del_list'
    end
    object act_GroupAssign: TAction
      Caption = #1043#1088#1091#1087#1087#1086#1074#1086#1077' '#1087#1088#1080#1089#1074#1072#1080#1074#1072#1085#1080#1077' '#1079#1085#1072#1095#1077#1085#1080#1081
    end
    object act_GoToProperty: TAction
      Caption = #1055#1077#1088#1077#1081#1090#1080' '#1082' '#1087#1083#1086#1097#1072#1076#1082#1077
    end
  end
  object ActionList_Folders: TActionList
    Left = 256
    Top = 12
    object act_Folder_Add: TAction
      Category = 'common Folder'
      Caption = 'act_FolderDel'
    end
    object act_Folder_Del: TAction
      Category = 'common Folder'
      Caption = 'act_FolderAdd'
    end
    object act_Folder_Rename: TAction
      Category = 'common Folder'
      Caption = 'act_Folder_Rename'
    end
    object act_Folder_MoveTo: TAction
      Category = 'common Folder'
      Caption = 'act_Folder_MoveTo'
    end
  end
end
