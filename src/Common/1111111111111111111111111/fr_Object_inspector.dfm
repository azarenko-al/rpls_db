object frame_Object_inspector: Tframe_Object_inspector
  Left = 581
  Top = 280
  Width = 460
  Height = 364
  Caption = 'frame_Object_inspector'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object pn_Buttons: TPanel
    Left = 0
    Top = 302
    Width = 452
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 0
    DesignSize = (
      452
      35)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 452
      Height = 2
      Align = alTop
      Shape = bsTopLine
    end
    object btn_Ok: TButton
      Left = 289
      Top = 8
      Width = 75
      Height = 23
      Action = act_Ok
      Anchors = [akTop, akRight]
      Default = True
      ModalResult = 1
      TabOrder = 0
      Visible = False
    end
    object btn_Cancel: TButton
      Left = 373
      Top = 8
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
  object pn_Header: TPanel
    Left = 0
    Top = 0
    Width = 452
    Height = 60
    Align = alTop
    BevelOuter = bvLowered
    BorderWidth = 6
    Color = clInfoBk
    Constraints.MaxHeight = 60
    Constraints.MinHeight = 60
    TabOrder = 1
    Visible = False
  end
  object ActionList1: TActionList
    Left = 18
    Top = 3
    object act_Ok: TAction
      Category = 'Main'
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    end
    object act_Cancel: TAction
      Category = 'Main'
      Caption = #1054#1090#1084#1077#1085#1072
    end
  end
  object FormPlacement1: TFormPlacement
    IniFileName = 'Software\Onega\'
    Left = 48
    Top = 2
  end
end
