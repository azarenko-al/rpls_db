unit fr_Object_inspector;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList,  StdCtrls, ExtCtrls, rxPlacemnt,

  u_Func,

  u_Inspector_common,
  u_Explorer,

  fr_DBInspector_Container
  ;

type
  Tframe_Object_inspector = class(TForm) //(Tframe_DBInspector_Container)
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    pn_Header: TPanel;
    ActionList1: TActionList;
    act_Ok: TAction;
    act_Cancel: TAction;
    FormPlacement1: TFormPlacement;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

    Fframe_DBInspector_Container: Tframe_DBInspector_Container;

  protected
    FRegPath: string;

    FID: integer;
    FGUID: string;

    procedure DoOnFieldChanged (Sender: TObject; aFieldName: string; aNewValue: Variant); virtual;
    procedure DoOnButtonFieldClick (Sender: TObject; aFieldName: string; aNewValue: Variant); virtual;
    procedure DoOnPost (Sender: TObject); virtual;

    procedure ExpandRowByFieldName (aFieldName: string);
    procedure CollapseRowByFieldName (aFieldName: string);

    function  GetFieldInfoByFieldName (aFieldName: string): TViewEngFieldInfo;
    procedure SetRowCaptionByFieldName(aFieldName,aCaption: string);


    procedure PrepareView (aObjectName: string);

//    procedure SetFieldValue(aFieldName: string;  aValue: Variant);

    procedure SetFieldValue (aFieldName: string; aValue: Variant);
    function  GetFieldValue (aFieldName: string): Variant;
    function GetBooleanFieldValue(aFieldName: string): boolean;
    function  GetIntFieldValue (aFieldName: string): Integer;
    function  GetStrFieldValue (aFieldName: string): string;
    function  GetFloatFieldValue(aFieldName: string): double;

    procedure HideEdit;
//    procedure SetFieldValue(aFieldName: string;  aValue: Variant);

    procedure SetFieldReadOnly(aFieldName: string; aValue: boolean);

    procedure SetMinMaxValues(aFieldName: string; aMin, aMax: Integer);



  public
    procedure View (aID: integer); virtual;
    procedure Post();
  end;



//==================================================================
//implementation
//==================================================================
implementation
{$R *.DFM}

{
const
  REGISTRY_COMMON_FORMS  = 'Software\Onega\Common\Forms\';
}

//-------------------------------------------------------------------
procedure Tframe_Object_inspector.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  pn_Buttons.Visible:=False;

  Caption:='��������� �������';


  CreateChildForm (Tframe_DBInspector_Container, Fframe_DBInspector_Container, Self);
//  Fframe_DBInspector_Container:=Tframe_DBInspector_Container.CreateChildForm(Self);
  Fframe_DBInspector_Container.OnFieldChanged:=DoOnFieldChanged;
  Fframe_DBInspector_Container.OnButtonClick:= DoOnButtonFieldClick;
  Fframe_DBInspector_Container.OnPost:=DoOnPost;

{
  FRegPath:= REGISTRY_COMMON_FORMS + ClassName +'\';
  FormPlacement1.IniFileName:=FRegPath;
}

//  FRegPath:= REGISTRY_COMMON_FORMS + ClassName +'\'+aObjectName+'\';
 // FormPlacement1.IniFileName:=FRegPath;
 // FormPlacement1.Active:=True;

end;


procedure Tframe_Object_inspector.FormDestroy(Sender: TObject);
begin
  Fframe_DBInspector_Container.Free;

  inherited;
end;


procedure Tframe_Object_inspector.DoOnFieldChanged(Sender: TObject;  aFieldName: string; aNewValue: Variant);
begin
end;


procedure Tframe_Object_inspector.DoOnPost(Sender: TObject);
begin
//  ShowMessage('procedure Tframe_Object_inspector.DoOnPost(Sender: TObject);');
end;


function Tframe_Object_inspector.GetFieldValue(aFieldName: string): Variant;
begin
  Result := Fframe_DBInspector_Container.GetFieldValue(aFieldName);
end;

function Tframe_Object_inspector.GetFloatFieldValue(aFieldName: string): double;
begin
  Result := Fframe_DBInspector_Container.GetFloatFieldValue(aFieldName);
end;

function Tframe_Object_inspector.GetIntFieldValue(aFieldName: string): Integer;
begin
  Result := Fframe_DBInspector_Container.GetIntFieldValue(aFieldName);
end;

function Tframe_Object_inspector.GetStrFieldValue(aFieldName: string): string;
begin
  Result := Fframe_DBInspector_Container.GetStrFieldValue(aFieldName);
end;

function Tframe_Object_inspector.GetBooleanFieldValue(aFieldName: string):
    boolean;
begin
  Result := Fframe_DBInspector_Container.GetBooleanFieldValue(aFieldName);
end;


procedure Tframe_Object_inspector.Post;
begin
  Fframe_DBInspector_Container.Post;
end;

procedure Tframe_Object_inspector.PrepareView(aObjectName: string);
begin
   {
  FRegPath:= REGISTRY_COMMON_FORMS + ClassName +'\'+aObjectName+'\';
  FormPlacement1.IniFileName:=FRegPath;
  FormPlacement1.Active:=True;
  }

  Fframe_DBInspector_Container.PrepareView (aObjectName);

end;


procedure Tframe_Object_inspector.SetFieldValue(aFieldName: string; aValue: Variant);
begin
  Fframe_DBInspector_Container.SetFieldValue(aFieldName,aValue);
end;


procedure Tframe_Object_inspector.View(aID: integer);
begin
  FID:=aID;

  Fframe_DBInspector_Container.View (aID);
end;


procedure Tframe_Object_inspector.DoOnButtonFieldClick(Sender: TObject; aFieldName: string; aNewValue: Variant);
begin
end;



procedure Tframe_Object_inspector.CollapseRowByFieldName(aFieldName: string);
begin
  Fframe_DBInspector_Container.CollapseRowByFieldName(aFieldName);
end;

procedure Tframe_Object_inspector.ExpandRowByFieldName(aFieldName: string);
begin
  Fframe_DBInspector_Container.ExpandRowByFieldName(aFieldName);

end;


function Tframe_Object_inspector.GetFieldInfoByFieldName(aFieldName: string): TViewEngFieldInfo;
begin
  Result := Fframe_DBInspector_Container.GetFieldInfoByFieldName(aFieldName);

end;

procedure Tframe_Object_inspector.HideEdit;
begin
  Fframe_DBInspector_Container.HideEdit;

end;

procedure Tframe_Object_inspector.SetFieldReadOnly(aFieldName: string; aValue: boolean);
begin
  Fframe_DBInspector_Container.SetFieldReadOnly(aFieldName,aValue);
end;

procedure Tframe_Object_inspector.SetMinMaxValues(aFieldName: string; aMin, aMax: Integer);
begin
  Fframe_DBInspector_Container.SetMinMaxValues(aFieldName, aMin, aMax);

end;

procedure Tframe_Object_inspector.SetRowCaptionByFieldName(aFieldName, aCaption: string);
begin
  Fframe_DBInspector_Container.SetRowCaptionByFieldName(aFieldName, aCaption);
end;

end.



