inherited frame_Link_SDB_View: Tframe_Link_SDB_View
  Left = 971
  Top = 834
  Width = 1312
  Height = 605
  Caption = 'frame_Link_SDB_View'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 1296
    Height = 56
  end
  inherited pn_Caption: TPanel
    Top = 84
    Width = 1296
  end
  inherited pn_Main: TPanel
    Top = 101
    Width = 1296
    Height = 324
    BorderWidth = 1
    object PageControl1: TPageControl
      Left = 2
      Top = 2
      Width = 1292
      Height = 279
      ActivePage = TabSheet3
      Align = alTop
      Style = tsFlatButtons
      TabOrder = 0
      OnChange = PageControl1Change
      object TabSheet_Main: TTabSheet
        Caption = #1057#1074#1086#1081#1089#1090#1074#1072
      end
      object TabSheet3: TTabSheet
        Caption = #1056#1056' '#1080#1085#1090#1077#1088#1074#1072#1083#1099
        ImageIndex = 1
        object cxGrid2: TcxGrid
          Left = 0
          Top = 0
          Width = 1284
          Height = 161
          Align = alTop
          TabOrder = 0
          LookAndFeel.Kind = lfFlat
          object cxGrid1DBTableView1: TcxGridDBTableView
            NavigatorButtons.ConfirmDelete = False
            DataController.DataSource = DataSource2
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.ImmediateEditor = False
            OptionsCustomize.ColumnFiltering = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.GroupByBox = False
            OptionsView.HeaderAutoHeight = True
            object cxGrid1DBTableView1name_sdb: TcxGridDBColumn
              Caption = #1058#1080#1087' '#1056#1056#1048
              DataBinding.FieldName = 'name_sdb'
              Width = 119
            end
            object cxGrid1DBTableView1name: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1056#1056#1048
              DataBinding.FieldName = 'name'
              Width = 125
            end
            object cxGrid1DBTableView1Column1: TcxGridDBColumn
              Caption = 'T'#1080#1087' '#1087#1086#1090#1086#1082#1072
              DataBinding.FieldName = 'stream_type'
              Width = 121
            end
            object cxGrid1DBTableView1SDB_bitrate_priority: TcxGridDBColumn
              Caption = #1057#1082#1086#1088#1086#1089#1090#1100' '#1087#1086#1090#1086#1082#1072', [Mbit/s]'
              DataBinding.FieldName = 'stream_bitrate'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.DisplayFormat = ',0.0'
              Width = 110
            end
            object cxGrid1DBTableView1SESR: TcxGridDBColumn
              Caption = 'SESR [%]'
              DataBinding.FieldName = 'SESR'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.DisplayFormat = ',0.00000'
            end
            object cxGrid1DBTableView1kng: TcxGridDBColumn
              Caption = #1050#1085#1075' [%]'
              DataBinding.FieldName = 'kng'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.DisplayFormat = ',0.00000'
            end
            object cxGrid1DBTableView1Column2_kng_year: TcxGridDBColumn
              Caption = 'KNG '#1084#1080#1085' '#1074' '#1075#1086#1076
              DataBinding.FieldName = 'kng_year'
            end
            object cxGrid1DBTableView1SESR_required: TcxGridDBColumn
              Caption = 'SESR '#1090#1088#1077#1073' [%]'
              DataBinding.FieldName = 'SESR_required'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.DisplayFormat = ',0.00000'
            end
            object cxGrid1DBTableView1kng_required: TcxGridDBColumn
              Caption = #1050#1085#1075' '#1090#1088#1077#1073' [%]'
              DataBinding.FieldName = 'kng_required'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.DisplayFormat = ',0.00000'
              Width = 72
            end
            object cxGrid1DBTableView1bandwidth: TcxGridDBColumn
              Caption = #1055#1086#1083#1086#1089#1072' [MHz]'
              DataBinding.FieldName = 'bandwidth'
              Width = 50
            end
            object cxGrid1DBTableView1length_m: TcxGridDBColumn
              Caption = #1044#1083#1080#1085#1072' '#1056#1056#1048' ['#1084']'
              DataBinding.FieldName = 'length_m'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.DisplayFormat = ',0'
            end
            object cxGrid1DBTableView1band: TcxGridDBColumn
              Caption = #1044#1072#1087#1072#1079#1086#1085' [GHz]'
              DataBinding.FieldName = 'band'
              Width = 62
            end
            object cxGrid1DBTableView1status_str: TcxGridDBColumn
              Caption = #1055#1088#1080#1075#1086#1076#1085#1086#1089#1090#1100
              DataBinding.FieldName = 'status_str'
              Width = 115
            end
          end
          object cxGridLevel1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
    end
  end
  inherited MainActionList: TActionList
    Left = 28
    Top = 4
    object act_report: TAction
      Caption = #1054#1090#1095#1077#1090
      OnExecute = act_reportExecute
    end
  end
  inherited ImageList1: TImageList
    Left = 84
    Top = 4
  end
  inherited PopupMenu1: TPopupMenu
    Left = 180
    Top = 4
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 272
    Top = 0
  end
  inherited cxLookAndFeelController1: TcxLookAndFeelController
    Top = 448
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    Top = 512
    DockControlHeights = (
      0
      0
      28
      0)
    inherited dxBarManager1Bar1: TdxBar
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem_Menu'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton3'
        end>
    end
    inherited dxBarSubItem_Menu: TdxBarSubItem [3]
    end
    inherited ed_Filter: TcxBarEditItem [4]
    end
    inherited dxBarButton3: TdxBarButton [5]
      Action = act_report
    end
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=SERVER1'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 328
    Top = 448
  end
  object ADOStoredProc_new: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'link.sp_Link_SDB_items_SEL'
    Parameters = <>
    Left = 216
    Top = 448
  end
  object DataSource2: TDataSource
    DataSet = ADOStoredProc_new
    Left = 218
    Top = 508
  end
end
