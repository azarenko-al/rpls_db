unit d_Link_SDB_Add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, ExtCtrls,
  rxPlacemnt, ActnList,     ADODB,
  Buttons, cxPropertiesStore, cxGridCustomTableView,cxGridLevel,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid,
  cxControls, cxGridCustomView, cxClasses, cxSplitter,

  cxButtonEdit,

  dm_Act_Explorer,

  dm_User_Security,

  dm_Onega_DB_data,

  d_Wizard_add_with_Inspector,

  dm_Main,

  dm_Link_SDB,

  u_db,
  u_dlg,
  u_classes,

  I_Shell,
  u_shell_new,

  u_types,
  u_const_str, ComCtrls, cxVGrid, cxInplaceContainer, cxLookAndFeels

  ;

type
  Tdlg_Link_SDB_add = class(Tdlg_Wizard_add_with_Inspector)
    cxVerticalGrid1: TcxVerticalGrid;
    row_Link_1: TcxEditorRow;
    row_Link_2: TcxEditorRow;

    procedure Button1Click(Sender: TObject);
    procedure _ActionMenu(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure row_Link_1EditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure row_Link_2EditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
 //   procedure FormDestroy(Sender: TObject);
//    procedure FormResize(Sender: TObject);


  private
    FID: integer;            

    FRec: TdmLink_SDB_AddRec;


//    FIDList: TIDList;

 //   FIDList_link: TIDList;
//    FIDList_linkline: TIDList;


   // FIsEdited: boolean;

    procedure Append;

//    procedure GetCheckedSiteIDList(aDataSet: TDataSet; out aIDList: TIDList);
//    procedure OpenData(aLinkNet_ID: Integer);
  public
//    class function Dlg_Update(aID: integer): boolean;
    class function Dlg_Add: integer;
  end;


//====================================================================
//====================================================================
implementation

uses d_Wizard;
 {$R *.DFM}



//--------------------------------------------------------------------
class function Tdlg_Link_SDB_add.Dlg_Add: integer;
//--------------------------------------------------------------------
begin
{
  if dmUser_Security.ProjectIsReadOnly then
  begin
    ShowMessage('���������� �������� ������. ������ ������ ��� ������.');
    Result:=0;
    Exit;
  end;
}

  with Tdlg_Link_SDB_add.Create(Application) do
  try
    ed_Name_.Text:= dmLink_SDB.GetNewName ();


 //   FFolderID:=aFolderID;
//    ed_Name_.Text:=dmLinkNet.GetNewName;

 //   FIsEdited:=false;

  //  OpenData(0);

//    dmLinkNet_add.Add_all_Links_and_LinkLines();

    if ShowModal=mrOk then
      Result:=FID
    else
      Result:=0;

  finally
    Free;
  end;
end;


//--------------------------------------------------------------------
procedure Tdlg_Link_SDB_add.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
  SetActionName (STR_DLG_ADD_Link_SDB);

  lb_Action.Caption:= STR_DLG_ADD_Link_SDB;

  SetDefaultSize;

//  TdmLink_SDB.Init;

//  FIDList:=TIDList.Create;
 // FIDList_link:=TIDList.Create;
//  FIDList_linkline:=TIDList.Create;

  //GSPages1.ActivePageIndex:=0;
//  cxGrid2.Align := alclient;

end;






//--------------------------------------------------------------------
procedure Tdlg_Link_SDB_add.Append;
//--------------------------------------------------------------------
begin
//  FillChar(rec, SizeOf(rec), 0);

  FRec.Name:=ed_Name_.Text;

  FID:=dmLink_SDB.Add (FRec);



(*
  if (FISEdited = false) and (dmLinkNet.FindByName(rec.Name)>0)  then
  begin
    ErrorDlg('��� ���� ��� ����������, ������� ������');
    Exit;
  end;
*)

 // rec.FolderID:=FFolderID;
 {
  GetCheckedSiteIDList(ADOStoredProc_Links, FIDList_link);
//  GetCheckedSiteIDList(ADOStoredProc_LinkLines, FIDList_linkline);

  if FISEdited then
    dmLinkNet.Add_new_items (FID, FIDList_link, FIDList_linkline)
  else
    FID:=dmLinkNet.Add_new (rec, FIDList_link, FIDList_linkline);
 }

 
//  if aFolderID=0 then
 // sFolderGUID:=g_Obj.ItemByName[OBJ_PROPERTY].RootFolderGUID;
 // else
  //  sFolderGUID:=dmFolder.GetGUIDByID(aFolderID);

  g_Shell.UpdateNodeChildren_ByGUID (GUID_Link_SDB);



  // .. g_Shell.UpdateNodeChildren_ByGUID (sFolderGUID);
    g_Shell.EXPAND_BY_GUID(GUID_Link_SDB);

    g_Shell.FocuseNodeByObjName (OBJ_LINK_SDB, FID);
//  g_ShellEvents.


//  SH_PostUpdateNodeChildren(dmLinkNet.GetGUIDByID(FNetID));

end;

procedure Tdlg_Link_SDB_add.Button1Click(Sender: TObject);
begin
  inherited;
end;


//-------------------------------------------------------------------
procedure Tdlg_Link_SDB_add._ActionMenu(Sender: TObject);
//-------------------------------------------------------------------
var LinkEnd1_ID, LinkEnd2_ID: integer;
begin
  if Sender=act_Ok then
  begin
    Append;
   //////// SH_PostUpdateNodeChildren(dmLinkNet.GetGUIDByID(FNetID));
  end
  else

end;



procedure Tdlg_Link_SDB_add.row_Link_1EditPropertiesButtonClick( Sender: TObject; AButtonIndex: Integer);
begin
 // inherited;
  //

  dmAct_Explorer.Dlg_SelectObject_cxButtonEdit(Sender as TcxButtonEdit, otLink, FRec.Link1_ID);

 // Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otLink, FRec.Link1_ID);


//                 FLD_Link1_ID,   aRec.Link1_ID,
//                 FLD_Link2_ID,   aRec.Link2_ID

end;


procedure Tdlg_Link_SDB_add.row_Link_2EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
//  inherited;
  dmAct_Explorer.Dlg_SelectObject_cxButtonEdit(Sender as TcxButtonEdit, otLink, FRec.Link2_ID);

//  Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otLink, FRec.Link2_ID);

end;

end.



 (*



procedure Tdlg_Link_SDB_add.FormResize(Sender: TObject);
begin
 // cxGrid1.Width:=TabSheet1.Width div 2;
end;

//-------------------------------------------------------------------
procedure Tdlg_Link_SDB_add.GetCheckedSiteIDList(aDataSet: TDataSet; out
    aIDList: TIDList);
//-------------------------------------------------------------------
begin
  aDataSet.DisableControls;

  aDataSet.First;
  aIDList.Clear;

  with aDataSet do
    while not EOF do
  begin
    if FieldByName(FLD_CHECKED).AsBoolean then
      aIDList.AddID(FieldByName(FLD_ID).AsInteger);

    Next;
  end;

  aDataSet.EnableControls;
end;




//----------------------------------------------------------------------
procedure Tdlg_Link_SDB_add.FormDestroy(Sender: TObject);
//----------------------------------------------------------------------
begin
 // FreeAndNil(FIDList);
 // FreeAndNil(FIDList_link);
//  FreeAndNil(FIDList_linkline);

  inherited;
end;


//--------------------------------------------------------------------
procedure Tdlg_Link_SDB_add.OpenData(aLinkNet_ID: Integer);
//--------------------------------------------------------------------
begin
 {
  if aLinkNet_ID>0 then
  begin
    dmOnega_DB_data.OpenStoredProc(ADOStoredProc_Links,
           'sp_LinkNet_select_items_for_add',
           [FLD_ID, aLinkNet_ID,
            FLD_OBJName, 'link'
           ]);

    dmOnega_DB_data.OpenStoredProc(ADOStoredProc_LinkLines,
           'sp_LinkNet_select_items_for_add',
           [FLD_ID, aLinkNet_ID,
            FLD_OBJName, 'linkLine'
           ]);
  end else
  begin
    dmOnega_DB_data.OpenStoredProc(ADOStoredProc_Links,
           'sp_LinkNet_select_items_for_add',
           [FLD_PROJECT_ID, dmMain.ProjectID,
            FLD_OBJName, 'link'
           ]);

    dmOnega_DB_data.OpenStoredProc(ADOStoredProc_LinkLines,
           'sp_LinkNet_select_items_for_add',
           [FLD_PROJECT_ID, dmMain.ProjectID,
            FLD_OBJName, 'linkLine'
           ]);
  end;

  }

end;
