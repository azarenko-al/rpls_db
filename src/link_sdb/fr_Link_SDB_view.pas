unit fr_Link_SDB_view;

interface

uses

  fr_View_base,
  u_Link_Report_Lib,

  fr_DBInspector_Container,
//  fr_LinkNet_Links,

  dm_User_Security,  
  
  dm_Onega_DB_data,


  u_func,
  u_reg,

  u_const_db,

  u_const_str,
  u_types,

  cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid, DB,
  ADODB, ComCtrls,  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, Menus, cxPropertiesStore, DBCtrls, ExtCtrls,  ImgList,

  dxBar, cxBarEditItem, cxLookAndFeels, dxSkinsCore,
  dxSkinsDefaultPainters, dxSkinsdxBarPainter, cxCheckBox, cxButtonEdit,
  cxGraphics, cxLookAndFeelPainters, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxCurrencyEdit
  ;

type
  Tframe_Link_SDB_View = class(Tframe_View_Base)
    PageControl1: TPageControl;
    TabSheet_Main: TTabSheet;
    ADOConnection1: TADOConnection;
    TabSheet3: TTabSheet;
    cxGrid2: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1name_sdb: TcxGridDBColumn;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    cxGrid1DBTableView1SESR: TcxGridDBColumn;
    cxGrid1DBTableView1kng: TcxGridDBColumn;
    cxGrid1DBTableView1SESR_required: TcxGridDBColumn;
    cxGrid1DBTableView1kng_required: TcxGridDBColumn;
    cxGrid1DBTableView1bandwidth: TcxGridDBColumn;
    cxGrid1DBTableView1length_m: TcxGridDBColumn;
    cxGrid1DBTableView1band: TcxGridDBColumn;
    cxGrid1DBTableView1SDB_bitrate_priority: TcxGridDBColumn;
    cxGrid1DBTableView1status_str: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    ADOStoredProc_new: TADOStoredProc;
    DataSource2: TDataSource;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    act_report: TAction;
    cxGrid1DBTableView1Column2_kng_year: TcxGridDBColumn;
    procedure act_reportExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
//    procedure GSPages1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
  private
 //   FID: integer;
 //   FActivePage: integer;
//    FIsUpdated: boolean;

    Ffrm_Inspector: Tframe_DBInspector_Container;
  //  procedure ReportExecute(Sender: TObject);

//    Ffrm_Links: Tframe_LinkNet_Links;

  public
    procedure View (aID: integer; aGUID: string); override;
  end;

//==================================================================
//==================================================================
implementation
 {$R *.dfm}

procedure Tframe_Link_SDB_View.act_reportExecute(Sender: TObject);
begin
  LinkReport_Execute_Link_SDB(ID);

end;

//--------------------------------------------------------------------
procedure Tframe_Link_SDB_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  if ADOConnection1.Connected then
    ShowMessage ('ADOConnection1.Connected');


  ObjectName:=OBJ_LINK_SDB;

  PageControl1.Align:=alClient;

//  Caption:=STR_LINK_NET;

  CreateChildForm(Tframe_DBInspector_Container, Ffrm_Inspector, TabSheet_Main);
  Ffrm_Inspector.PrepareViewForObject (OBJ_LINK_SDB);

//  CreateChildForm(Tframe_LinkNet_Links, Ffrm_Links, TabSheet_Links);

  AddComponentProp(PageControl1, PROP_ACTIVE_PAGE);
  cxPropertiesStore.RestoreFrom;

end;


//--------------------------------------------------------------------
procedure Tframe_Link_SDB_View.View(aID: integer; aGUID: string);
//--------------------------------------------------------------------
//var
//  bReadOnly: Boolean;
var
  bReadOnly: Boolean;
begin
  inherited;

//  dmOnega_DB_data.OpenStoredProc(ADOStoredProc1, 'link.sp_Link_SDB_items_SEL', [FLD_ID, aID]);

  dmOnega_DB_data.OpenStoredProc(ADOStoredProc_new, 'link.sp_Link_SDB_items_SEL', [FLD_ID, aID]);

                 

  //------------------------------------

   bReadOnly:=dmUser_Security.Object_Edit_ReadOnly();


//  bReadOnly := dmUser_Security.ProjectIsReadOnly;

  Ffrm_Inspector.SetReadOnly (bReadOnly);
//  Ffrm_Links.SetReadOnly (bReadOnly);

  Ffrm_Inspector.View (ID);


 // FId := aID;
//  FActivePage:= -1;
 {
  case PageControl1.ActivePageIndex of
    0: Ffrm_Inspector.View (ID);
  //  1: Ffrm_Links.View (ID);
  end;
 }




//  FISUpdated:=true;

  PageControl1Change (nil);

end;




procedure Tframe_Link_SDB_View.PageControl1Change(Sender: TObject);
begin
//  if not FIsUpdated = true then
//    Exit;

//  if FActivePage=PageControl1.ActivePageIndex then
//    Exit;

//  FActivePage:= PageControl1.ActivePageIndex;

  case PageControl1.ActivePageIndex of
    0: Ffrm_Inspector.View (ID);
  //  1: Ffrm_Links.View (ID);
  end;
end;

end.



{

FLD_KNG_year,   'KNG ��� � ���',
