inherited dlg_Link_SDB_add: Tdlg_Link_SDB_add
  Left = 973
  Top = 350
  Width = 740
  Height = 539
  Caption = 'dlg_Link_SDB_add'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 476
    Width = 732
    inherited Bevel1: TBevel
      Width = 732
    end
    inherited Panel3: TPanel
      Left = 556
      Width = 176
      inherited btn_Ok: TButton
        Left = 9
      end
      inherited btn_Cancel: TButton
        Left = 93
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 732
    inherited Bevel2: TBevel
      Width = 732
    end
    inherited pn_Header: TPanel
      Width = 732
      Height = 55
    end
  end
  inherited Panel1: TPanel
    Width = 732
    inherited ed_Name_: TEdit
      Width = 720
    end
  end
  inherited Panel2: TPanel
    Width = 732
    Height = 347
    inherited PageControl1: TPageControl
      Width = 722
      Height = 116
      Align = alTop
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 714
          Height = 85
          Align = alClient
          LayoutStyle = ulsBandsView
          LookAndFeel.Kind = lfFlat
          OptionsView.PaintStyle = psDelphi
          OptionsView.RowHeaderWidth = 167
          TabOrder = 0
          Version = 1
          object row_Link_1: TcxEditorRow
            Properties.Caption = 'Common Band : '#1056#1056#1048
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = row_Link_1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object row_Link_2: TcxEditorRow
            Properties.Caption = 'E-Band : '#1056#1056#1048
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = row_Link_2EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
        end
      end
    end
  end
  inherited ActionList1: TActionList
    inherited act_Ok: TAction
      OnExecute = _ActionMenu
    end
    inherited act_Cancel: TAction
      OnExecute = _ActionMenu
    end
  end
  inherited FormStorage1: TFormStorage
    Left = 394
    Top = 2
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 497
  end
end
