unit dm_Link_SDB;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Db, ADODB,  Variants,

  dm_Onega_DB_data,

  dm_Main,
  dm_Object_base,

  u_Geo,
  u_db,
  u_func,
  u_classes,

  u_types,

  u_const_str,
  u_const_db,

  dxmdaset
  ;



type
  //-------------------------------------------------------------------
  TdmLink_SDB_AddRec = record
  //-------------------------------------------------------------------
//    LinkNetID: integer;
    Name:     string;
    Link1_ID: integer;
    Link2_ID: integer;
//    FolderID: integer;
  end;


  //-------------------------------------------------------------------
  TdmLink_SDB = class(TdmObject_base)
    mem_Links: TdxMemData;
    qry_Temp: TADOQuery;
    qry_Temp2: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
//    procedure GetAllLinks_for_Net_to_Dataset1(aNetID: integer; var aMem: TdxMemData); overload;
//    procedure RemoveItems(aID: integer; aIDList: TIDList);
  public
    function Add(aRec: TdmLink_SDB_AddRec): integer;
    function Del(aID: integer): integer;

{
    function Add_by_LinkList(aRec: TdmLinkNetAddRec; aLinkIDList: TIDList): integer;

    function Add(aRec: TdmLinkNetAddRec; aIDList_link,aIDList_linkline:
        TIDList): integer;

    function Add_new_items(aNetNet_ID : Integer; aIDList_link, aIDList_linkline:
        TIDList): integer;

    procedure GetLinks_for_Net    (aNetID: integer; var aMem: TdxMemData); overload;
    procedure GetAllLinks_for_Net (aNetID: integer; var aIDList: TIDList); overload;
    procedure GetLinks_for_Net    (aNetID: integer; var aIDList: TIDList); overload;

    procedure GetLinkLines_for_Net_to_Dataset(aNetID: integer; var aMem: TdxMemData); overload;
    procedure GetLinkLines_for_Net(aNetID: integer; var aIDList: TIDList); overload;


    function GetBLRect (aID: integer): TBLRect;
 }
  end;


function dmLink_SDB: TdmLink_SDB;

//====================================================================
implementation {$R *.dfm}
//====================================================================

var
  FdmLink_SDB: TdmLink_SDB;


// ---------------------------------------------------------------
function dmLink_SDB: TdmLink_SDB;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLink_SDB) then
    FdmLink_SDB := TdmLink_SDB.Create(Application);

   Result := FdmLink_SDB;
end;

//--------------------------------------------------------------------
procedure TdmLink_SDB.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

//  TableName :=TBL_Link_SDB;
//  DisplayName:=STR_Link_SDB;
  ObjectName:=OBJ_Link_SDB;

 // TdmLink_SDB.Init;


  {

  db_CreateField(mem_Links, [db_Field(FLD_ID,        ftInteger),
                            db_Field(FLD_NAME,       ftString),
                            db_Field(FLD_TYPE,       ftString),
                            db_Field(FLD_LENGTH,     ftFloat),
                            db_Field(FLD_KNG,        ftFloat),
                            db_Field(FLD_SESR,       ftFloat),
                            db_Field(FLD_rx_Level_dBm, ftFloat)
                           ]);

  TableName :=TBL_LINKNET;
  DisplayName:=STR_LINK_NET;
  ObjectName:=OBJ_LINKNet;

  }
end;




//-------------------------------------------------------
function TdmLink_SDB.Add(aRec: TdmLink_SDB_AddRec): integer;
//-------------------------------------------------------
//var i: Integer;
begin
//  Result:=dmLink_SDB.Add (aRec);


 Result:=dmOnega_DB_data.ExecStoredProc_ ('link.sp_Link_SDB_add',
                [
                 FLD_PROJECT_ID, dmMain.ProjectID,
                 FLD_NAME,       aRec.Name,

                 FLD_Link1_ID,   aRec.Link1_ID,
                 FLD_Link2_ID,   aRec.Link2_ID

                 ]  );


//  Assert(Result>0, 'function TdmLinkNet.Add(');


 // Add_new_items (Abs(Result), aIDList_link, aIDList_linkline);
end;




//-------------------------------------------------------
function TdmLink_SDB.Del(aID: integer): integer;
//-------------------------------------------------------
begin
   Result:=dmOnega_DB_data.ExecStoredProc_ ('link.sp_Link_SDB_Del',
                  [FLD_ID, aID
                 ]  );

end;



begin

end.

