unit dm_Act_Link_SDB;

interface

uses
  SysUtils, Classes, 
   Menus, Forms, ActnList,

  dm_act_Base,

  u_Link_Report_lib,

 // dm_LinkFreqPlan,


 // I_Act_Report, // dm_Act_Report;

  I_Object,

  u_types,

  u_func,


 // u_LinkNet_const,
//  Tframe_Link_SDB_View

  fr_Link_SDB_view,

  u_shell_new,


  dm_Link_SDB,

  d_Link_SDB_add

//  d_LinkFreqPlan_add
   // dm_Link_Net_Add

  ;

type
  TdmAct_Link_SDB = class(TdmAct_Base) //, IAct_LinkNet_X)
    act_Edit: TAction;
    act_AddFreqPlan: TAction;
    ActionList2: TActionList;
    act_Report: TAction;
    procedure act_ReportExecute(Sender: TObject);
  //  procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

  private
    function CheckActionsEnable: Boolean;
 //   procedure MakeReport(aID: integer);

  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm  (aOwnerForm: TForm): TForm; override;

    procedure DoAction (Sender: TObject);

    function ItemDel(aID: integer; aName: string = ''): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;

  public
    function Dlg_Add: integer;
  //  function Add_by_LinkList (aIDList: TIDList): integer;

    class procedure Init;
  end;


var
  dmAct_Link_SDB: TdmAct_Link_SDB;

//==================================================================
//==================================================================
implementation
{$R *.dfm}

procedure TdmAct_Link_SDB.act_ReportExecute(Sender: TObject);
//var
//..  oReportSelect: TIni_Link_Report_Param;
begin
  LinkReport_Execute_Link_SDB(FFocusedID);

 // Assert(Assigned(IReport));

//  oReportSelect:=TIni_Link_Report_Param.Create;


//  if dmAct_Report.Dlg_Select ('link', oReportSelect) then //, sRepName); //aType: string);
//  begin

 //  u_Link_Report_lib.LinkReport_Execute_LinkList ( FSelectedIDList);

//    u_Link_Report_lib.LinkReport_Execute_Link(aID, rReportSelect.XMLFileName);


//  end;


 // oReportSelect.Free;

//
end;

///uses
//  dm_Act_LinkFreqPlan, dm_Act_Report;



//--------------------------------------------------------------------
class procedure TdmAct_Link_SDB.Init;
//--------------------------------------------------------------------
begin
  Assert( not Assigned(dmAct_Link_SDB));

  dmAct_Link_SDB:=TdmAct_Link_SDB.Create(Application);

//  dmAct_LinkNet.GetInterface(IAct_LinkNet_X, IAct_LinkNet);
//  Assert(Assigned(IAct_LinkNet));
end;

//
//procedure TdmAct_LinkNet.DataModuleDestroy(Sender: TObject);
//begin
////  IAct_LinkNet := nil;
////  dmAct_LinkNet:= nil;
//
//  inherited;
//end;
//


//--------------------------------------------------------------------
procedure TdmAct_Link_SDB.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------

begin
  inherited;

  ObjectName:=OBJ_Link_SDB;

  act_Add.Caption:='������� �� SDB';


  SetActionsExecuteProc ([//act_Edit,
                          act_Add,
                          act_Del_ ,
                          act_Report
                      //    act_AddFreqPlan
                      //    act_Report
                          ], DoAction);

  SetCheckedActionsArr([
        act_Add,
        act_Del_,
        act_Report

     //   act_Edit
     //   act_AddFreqPlan
     ]);

end;


//--------------------------------------------------------------------
function TdmAct_Link_SDB.Dlg_Add: integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_Link_SDB_add.Dlg_Add ();
end;



//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_Link_SDB.DoAction(Sender: TObject);
//var
//  oListID: TIDList;
begin
//  oListID:=TIDList.Create;

 // if Sender=act_Object_ShowOnMap then ShowOnMap (FFocusedID);

  //-------------------------------------------------------------------
  if Sender=act_Add then
  //-------------------------------------------------------------------
    Tdlg_Link_SDB_add.Dlg_Add ()
  else

  //-------------------------------------------------------------------
  if Sender=act_Del_ then begin
  //-------------------------------------------------------------------
    dmLink_SDB.Del (FFocusedID);

    g_Shell.UpdateNodeChildren_ByGUID (GUID_Link_SDB );

  end
  else

  if Sender=act_Report then
    LinkReport_Execute_Link_SDB(FFocusedID)

  else

//    MakeReport(FFocusedID) else


 {
 procedure Tframe_Link_SDB_View.act_reportExecute(Sender: TObject);
begin
  LinkReport_Execute_Link_SDB(ID);

end;



  //-------------------------------------------------------------------
  if Sender=act_AddFreqPlan then
  //-------------------------------------------------------------------
  begin
    if dmAct_LinkFreqPlan.Dlg_AddForLinkNet(FFocusedID, FFolderID)> 0 then
//    if Tdlg_LinkFreqPlan_add.ExecDlg(FFocusedID, FFolderID)  then
      g_ShellEvents.Shell_UpdateNodeChildren_ByGUID (dmLinkNet.GetGUIDByID(FFocusedID));
  end else
 }
  raise EClassNotFound.Create('');



  //////////// PostMsg (aLinkLineID)
end;

//--------------------------------------------------------------------
function TdmAct_Link_SDB.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_Link_SDB_add.Dlg_Add ();
end;

//--------------------------------------------------------------------
function TdmAct_Link_SDB.ItemDel(aID: integer; aName: string = ''): boolean;
//--------------------------------------------------------------------
begin
  Result:=dmLink_SDB.Del (aID)>0;

  Result:=True;
end;




//--------------------------------------------------------------------
function TdmAct_Link_SDB.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_Link_SDB_View.Create(aOwnerForm);
end;


// ---------------------------------------------------------------
function TdmAct_Link_SDB.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
 {
  Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];

  SetActionsEnabled1( [

 act_Export_MDB,
//                           act_Import_MDB,

  act_Copy,
  act_Change_Vendor,
  act_Change_Vendor_Equipment,

  act_Update_Field,

  act_Mode_copy_tx_to_RX,
  act_Del_,
  act_Del_list

      ],

   Result );
  }
   //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );


end;


//--------------------------------------------------------------------
procedure TdmAct_Link_SDB.GetPopupMenu;
//--------------------------------------------------------------------
var
  bEnable: Boolean;
begin
  bEnable:=CheckActionsEnable();


 // CheckActionsEnable();

  case aMenuType of
    mtFolder: begin
            //    AddFolderMenu_Create (aPopupMenu);
              //  AddFolderPopupMenu   (aPopupMenu);
                AddMenuItem (aPopupMenu, act_Add);
              end;
    mtItem:   begin
              //  AddMenuItem (aPopupMenu, act_Edit);
                AddMenuItem (aPopupMenu, act_Del_);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Report);



         //       AddMenuItem (aPopupMenu, nil);
    //            AddMenuItem (aPopupMenu, act_AddFreqPlan);
//                AddMenuItem (aPopupMenu, nil);
//                AddMenuItem (aPopupMenu, act_Report);
              end;
//    mtList:
 //               AddMenuItem (aPopupMenu, act_Del_list);
  end;
end;





end.

        (*

//--------------------------------------------------------------------
procedure TdmAct_LinkNet.MakeReport(aID: integer);
//--------------------------------------------------------------------
var
  sXMLFileName: string;
  oLinkIDList, oLinkLineIDList: TIDList;
  sNetName: string;
//  rReportSelect: TdmReportSelectRec;

  oReportSelect: TIni_Link_Report_Param;

begin
   Exit;

(*
  oLinkIDList     := TIDList.Create;
  oLinkLineIDList := TIDList.Create;

  oReportSelect:=TIni_Link_Report_Param.Create;


 // if dmAct_Report.ExecDlg ('linknet', rReportSelect) then //, sRepName); //aType: string);
  if dmAct_Report.Dlg_Select('linknet', oReportSelect) then //, sRepName); //aType: string);
  begin
  //  rReportSelect.XMLFileName:=GetTempXmlFileName();
   // rReportSelect.XMLFileName:=sXMLFileName;


    dmLinkNet.GetLinks_for_Net     (aID, oLinkIDList);
    dmLinkNet.GetLinkLines_for_Net (aID, oLinkLineIDList);

  //  sNetName:= dmLinkNet.GetNameByID (aID);

//    u_Link_Report_lib.LinkReport_Execute_LinkList (rReportSelect.ReportID, SelectedIDList, rReportSelect.XMLFileName);
    u_Link_Report_lib.LinkReport_Execute_LinkList (oReportSelect, FSelectedIDList);

//    u_Link_Report_lib.LinkReport_Execute_Link(aID, rReportSelect.XMLFileName);

   // dmReport.MakeReport (rReportSelect);

  end;

  oReportSelect.Free;
*)

{
  sXMLFileName:=GetTempXmlFileName();
  //..sXMLFileName:='t:\wwwwww.xml';

//  dmLInkNet.GetAllLinks_for_Net(aID, oIDList);



  LinkReport_Execute_LinkNet (oLinkLineIDList, oLinkIDList,
                                sXMLFileName,    sNetName);

  dmAct_Report.ExecDlg (sXMLFileName, 'linknet');
}
 // oLinkIDList.Free;
 // oLinkLineIDList.Free;
end;


{

//--------------------------------------------------------------------
function TdmAct_Link_SDB.Add_by_LinkList (aIDList: TIDList): integer;
//--------------------------------------------------------------------
{
var
  rec: TdmLinkNetAddRec;
  bNameOK : boolean;
  sFolderGUID: string;
 }
begin
  {

  rec.Name:= dmLinkNet.GetNewName();

  bNameOK:= False;
  while not bNameOK do
  begin
    if not InputQuery('�������� �� ����', '������� ��� ����', rec.Name) then
      Exit;

    if (dmLinkNet.FindByName(rec.Name)>0)  then
    begin
      ErrorDlg('��� ���� ��� ����������, ������� ������.');
      bNameOK:= False;
    end
    else
      bNameOK:= True;
  end;    // while

  rec.FolderID:=FFolderID;

  Result:= dmLinkNet.Add_by_LinkList (rec, aIDList);

  if Result>0 then begin
    if FFolderID=0 then
      sFolderGUID:=g_Obj.ItemByName[OBJ_LINKNET].RootFolderGUID
    else
      sFolderGUID:=dmFolder.GetGUIDByID(FFolderID);


    g_ShellEvents.Shell_UpdateNodeChildren_ByGUID(sFolderGUID);

//    SH_PostUpdateNodeChildren (sFolderGUID);
  end;

  }
end;



  //-------------------------------------------------------------------
  if Sender=act_Edit then
  //-------------------------------------------------------------------
  begin
    {
    if Tdlg_LinkNet_add.Dlg_Update (FFocusedID) then
    begin
      g_ShellEvents.Shell_UpdateNodeChildren_ByGUID(dmLinkNet.GetGUIDByID(FFocusedID));

    //  sh_PostUpdateNodeChildren(dmLinkNet.GetGUIDByID(FFocusedID));

      PostEvent(WE_LINKNET_LINKS_REFRESH_, [app_Par(PAR_ID, FFocusedID)]);

      PostMessage(Application.Handle, WE_LINKNET_LINKS_REFRESH_, FFocusedID, 0);

   //   dmLinkFreqPlan.Update_LinkList(FFocusedID);

    end;
    }
  end
  else

}


//--------------------------------------------------------------------
procedure TdmAct_Link.ReportSelectedItems;
//--------------------------------------------------------------------
var
  oReportSelect: TIni_Link_Report_Param;
begin
 // Assert(Assigned(IReport));

  oReportSelect:=TIni_Link_Report_Param.Create;


//  if dmAct_Report.Dlg_Select ('link', oReportSelect) then //, sRepName); //aType: string);
//  begin

    u_Link_Report_lib.LinkReport_Execute_ LinkList ( FSelectedIDList);

//    u_Link_Report_lib.LinkReport_Execute_Link(aID, rReportSelect.XMLFileName);


//  end;


  oReportSelect.Free;


  //  sXMLFileName:=GetTempXmlFileName();
//    sXMLFileName:='t:\aaaaaaaaaaaaaaaa.xml';

  //  u_Link_Report_lib.LinkReport_Execute_LinkList (SelectedIDList, sXMLFileName);

{
    if SelectedIDList.Count>1 then
      sRepName:='����� �� ����������'

    else

    if SelectedIDList.Count=1 then
      sRepName:=dmLink.GetNameByID ( SelectedIDList[0].ID)
    else
      Exit;}


//    dmAct_Report.ExecDlg (sXMLFileName, 'link'); //, sRepName); //aType: string);
  {  if dmAct_Report.ExecDlg ('link', rReportSelect) then //, sRepName); //aType: string);
      dmReport.MakeReport (rReportSelect);}

    //  TReportSelectRec


end;

