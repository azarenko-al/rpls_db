unit d_Repeater_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rxPlacemnt,    cxPropertiesStore, ActnList,  StdCtrls, ExtCtrls,   cxButtonEdit,  Registry,

  u_Property_Add,


  dm_Onega_DB_data,   

//  I_Act_Explorer,

  u_func,

  u_cx_vgrid,

  dm_MapEngine_store,

 // d_Wizard,
  d_Wizard_Add_with_params,

  I_Shell,
  dm_Main,

 // I_MapEngine,
 // dm_MapEngine,


 // I_Object,
//  I_MSC,

 // I_Shell,
//  I_MapAct,

  u_const_DB,
  u_const_str,


  u_cx_VGrid_export,
  //u_cx_VGrid,

  u_dlg,
  u_db,
  u_Geo,

  u_types,

  dm_Repeater,
  dm_Property,

  cxVGrid, cxControls, cxInplaceContainer, ComCtrls, cxLookAndFeels

  ;

type
  Tdlg_Repeater_add = class(Tdlg_Wizard_add_with_params)
    cxVerticalGrid1: TcxVerticalGrid;
    row_Property_name: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    row_Antenna1: TcxCategoryRow;
    row_Antenna2: TcxCategoryRow;
    row_Antenna2_model: TcxEditorRow;
    row_Antenna1_H: TcxEditorRow;
    row_Antenna1_model: TcxEditorRow;
    row_Antenna2_H: TcxEditorRow;
    row_Antenna1_Gain: TcxEditorRow;
    row_Antenna2_Gain: TcxEditorRow;
    row_Property_type: TcxEditorRow;
    row_Property_address: TcxEditorRow;
    row_Property_pos: TcxEditorRow;
    row_is_active: TcxEditorRow;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
//    procedure row_Property1ButtonClick(Sender: TObject;
   //   AbsoluteIndex: Integer);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure cxVerticalGrid1EditorRow1EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure row_Property_typeEditPropertiesChange(Sender: TObject);
  private
//    FRegPath

    FPropertyID: Integer;


    FRec: TdmRepeaterAddRec;

    FID: integer;
    function AddProperty: integer;
    procedure Append;

 //   FFolderID: integer;
  public
     class function ExecDlg (aPropertyID: integer;
                             aBLPoint: TBLPoint): integer;
  end;

//==================================================================
implementation {$R *.dfm}

uses
  dm_Act_Explorer;


//-------------------------------------------------------------------
class function Tdlg_Repeater_add.ExecDlg(aPropertyID: integer;
                            aBLPoint: TBLPoint): integer;
//-------------------------------------------------------------------
begin
  with Tdlg_Repeater_add.Create(Application) do
  begin
    if (aPropertyID = 0)  then
      aPropertyID:=dmProperty.GetNearestID(aBLPoint);

    FRec.PropertyID:=aPropertyID;
  //  FRec.FolderID:=0;

    ed_Name_.Text:=dmRepeater.GetNewName; //(aPropertyID);
    row_Property_name.Properties.Value:=gl_DB.GetNameByID (TBL_PROPERTY, aPropertyID);

    if (ShowModal=mrOk) then
    begin
      Result:=FID;
    end else
      Result:=0;

    Free;
  end;
end;

//-------------------------------------------------------------------
procedure Tdlg_Repeater_add.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;
 // SetActionName (STR_DLG_ADD_Repeater);

  row_Property_type.Visible := False;


  row_Property_name.Properties.Caption:=STR_PROPERTY;

  SetDefaultSize();

  cx_InitVerticalGrid(cxverticalGrid1);

  row_Antenna1.Properties.Caption :='������� 1';
  row_Antenna2.Properties.Caption :='������� 2';


  row_Antenna1_model.Properties.Caption :='������';
  row_Antenna2_model.Properties.Caption :='������';

  row_Antenna1_H.Properties.Caption :='������ [m]';
  row_Antenna2_H.Properties.Caption :='������ [m]';

  row_Antenna1_Gain.Properties.Caption :='�������� [dB]';
  row_Antenna2_Gain.Properties.Caption :='�������� [dB]';


  row_Property_pos.Properties.Caption    :='����������';
  row_Property_address.Properties.Caption:='�����';

  cx_VerticalGrid_LoadFromReg(cxVerticalGrid1, FRegPath);


  with TRegIniFile.Create(FRegPath) do
  begin
    //////////

(*    WriteString ('Antenna1', row_Antenna1_model.Name,  row_Antenna1_model.Properties.Value);
    WriteInteger('Antenna1', FLD_ANTENNATYPE_ID, FRec.Antenna1.AntennaTypeID);
    WriteFloat  ('Antenna1', FLD_Height, FRec.Antenna1.Height);

*)
    FRec.Antenna1.AntennaType_ID:= ReadInteger('Antenna1', FLD_ANTENNATYPE_ID, 0);
    FRec.Antenna2.AntennaType_ID:= ReadInteger('Antenna2', FLD_ANTENNATYPE_ID, 0);

//    row_Mode.Text              := ReadString ('', row_Mode.Name,   '');
///////    row_Antenna1_model.Properties.Value := ReadString ('Antenna1', row_Antenna1_model.Name, '');
///////    row_Antenna2_model.Properties.Value := ReadString ('Antenna2', row_Antenna2_model.Name, '');

{
    row_Antenna1_H.Properties.Value := ReadString ('Antenna1', row_Antenna1_H.Name, '10');
    row_Antenna2_H.Properties.Value := ReadString ('Antenna2', row_Antenna2_H.Name, '10');

    row_Antenna1_Gain.Properties.Value := ReadString ('Antenna1', row_Antenna1_Gain.Name, '40');
    row_Antenna2_Gain.Properties.Value := ReadString ('Antenna2', row_Antenna2_Gain.Name, '40');
}
   // if Assigned(row_AssociateWithPmpSite) then
///////    row_AssociateWithPmpSite.Properties.Value:= ReadBool ('', row_AssociateWithPmpSite.Name, true);

    Free;
  end;


  row_Antenna1_model.Properties.Value := dmOnega_DB_data.GetNameByID (TBL_AntennaType, FRec.Antenna1.AntennaType_ID);
  row_Antenna2_model.Properties.Value := dmOnega_DB_data.GetNameByID (TBL_AntennaType, FRec.Antenna2.AntennaType_ID);


end;


// ---------------------------------------------------------------
procedure Tdlg_Repeater_add.FormDestroy(Sender: TObject);
// ---------------------------------------------------------------
var
  oRegIniFile: TRegIniFile;
begin
//  oRegIniFile.Wri

  cx_VerticalGrid_SaveToReg(cxVerticalGrid1, FRegPath);



  with TRegIniFile.Create(FRegPath) do
  begin
    //////////
//////    WriteString ('Antenna1', row_Antenna1_model.Name,  row_Antenna1_model.Properties.Value);
//////    WriteString ('Antenna2', row_Antenna2_model.Name,  row_Antenna2_model.Properties.Value);

    WriteInteger('Antenna1', FLD_ANTENNATYPE_ID, FRec.Antenna1.AntennaType_ID);
    WriteInteger('Antenna2', FLD_ANTENNATYPE_ID, FRec.Antenna2.AntennaType_ID);

{    WriteString ('Antenna1', FLD_Height, row_Antenna1_H.Properties.Value);
    WriteString ('Antenna2', FLD_Height, row_Antenna2_H.Properties.Value);

    WriteString ('Antenna1', FLD_Gain, row_Antenna1_Gain.Properties.Value);
    WriteString ('Antenna2', FLD_Gain, row_Antenna2_Gain.Properties.Value);}

//    WriteFloat  ('Antenna1', FLD_ANTENNATYPE_ID, FRec.Antenna1.Height);

   // if Assigned(row_AssociateWithPmpSite) then
///////    row_AssociateWithPmpSite.Properties.Value:= ReadBool ('', row_AssociateWithPmpSite.Name, true);

    Free;
  end;


  inherited;
end;


procedure Tdlg_Repeater_add.act_OkExecute(Sender: TObject);
begin
  Append();
end;

// ---------------------------------------------------------------
procedure Tdlg_Repeater_add.Append;
// ---------------------------------------------------------------
begin
  FRec.NewName:=ed_Name_.Text;

  FRec.IS_ACTIVE:=row_is_active.Properties.Value=1;

  FRec.Antenna1.Height :=AsFloat(row_Antenna1_H.Properties.Value);
  FRec.Antenna2.Height :=AsFloat(row_Antenna2_H.Properties.Value);

  FRec.Antenna1.Height :=AsFloat(row_Antenna1_H.Properties.Value);
  FRec.Antenna2.Height :=AsFloat(row_Antenna2_H.Properties.Value);

  
 // FRec.PropertyID:=FPropertyID;

  FID:=dmRepeater.Add (FRec);

  if FID>0 then
  begin
    g_ShellEvents.Shell_UpdateNodeChildren_ByObjName (OBJ_PROPERTY, FRec.PropertyID);

   // g_ShellEvents.Shell_UpdateNodeChildren_ByObjName (OBJ_MSC, FID);

//    dmMapEngine.CreateObject (otMSC, FID);
//    dmAct_Map.MapRefresh;


 //   {$IFDEF test}
  //  dmMapEngine_store.Feature_Add(OBJ_Repeater, FID);

  //  {$ELSE}
  //  {$ENDIF}



//    dmAct_Map.MapRefresh;
  end;

end;

(*

procedure Tdlg_Repeater_add.row_Property1ButtonClick(Sender: TObject; AbsoluteIndex: Integer);
begin
//  Dlg_SelectObject_dx (row_Property, otProperty, FRec.PropertyID);
end;
*)

procedure Tdlg_Repeater_add.ActionList1Update(Action: TBasicAction;  var Handled: Boolean);
begin
  act_Ok.Enabled:= (FRec.PropertyID > 0);
end;


// ---------------------------------------------------------------
procedure Tdlg_Repeater_add.cxVerticalGrid1EditorRow1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
// ---------------------------------------------------------------
begin
  if cxVerticalGrid1.FocusedRow =row_Property_name then
    dmAct_Explorer.Dlg_SelectObject_cxButtonEdit(Sender as TcxButtonEdit, otProperty, FRec.PropertyID);

  //--------------------------------------------

  if cxVerticalGrid1.FocusedRow =row_Antenna1_model then
    dmAct_Explorer.Dlg_SelectObject_cxButtonEdit(Sender as TcxButtonEdit, otAntennaType, FRec.Antenna1.AntennaType_ID);

  if cxVerticalGrid1.FocusedRow =row_Antenna2_model then
    dmAct_Explorer.Dlg_SelectObject_cxButtonEdit(Sender as TcxButtonEdit, otAntennaType, FRec.Antenna2.AntennaType_ID);

//    Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otProperty, FRec.PropertyID);

end;



//--------------------------------------------------------------------
function Tdlg_Repeater_add.AddProperty: integer;
//--------------------------------------------------------------------
var
  oPropertyAdd: TPropertyAddToDB;

 // rec: TdmPropertyAddRec;
begin
  //FillChar(rec, SizeOf(rec), 0);


  if FPropertyID=0 then
  begin
    oPropertyAdd := TPropertyAddToDB.Create();

    oPropertyAdd.Project_ID:=dmMain.ProjectID;


    oPropertyAdd.NewName  :=row_Property_name.Properties.Value;

    oPropertyAdd.Address:=AsString(row_Property_address.Properties.Value);

//    oPropertyAdd.BLPoint_Pulkovo :=Params.Property_BLPos;

  //  oPropertyAdd.Height    := AsFloat(row_Height.Properties.Value);
   // oPropertyAdd.Azimuth   := row_Azimuth.Properties.Value;
  //  FAntennaAdd.PropertyID:= FPropertyID;

  //  oPropertyAdd.AntennaType_ID :=FRec.AntennaTypeID;

    Result:=oPropertyAdd.Add_DB; //(dmMain.ADOConnection);

    FreeAndNil(oPropertyAdd);

//
//
//
//    rec.Name   :=row_Property.Properties.Value;
//    rec.BLPoint_Pulkovo:=Params.Property_BLPos;
//    rec.Address:=row_Property_address.Properties.Value;
//
//    Result:=dmProperty.Add(rec);

    //---------------------------------------------------

    if Result>0 then
      dmMapEngine_store.Feature_Add (OBJ_Property, Result);

  end else
    Result:=FPropertyID;




{
  FillChar(rec, SizeOf(rec), 0);

  rec.Name    :=row_Prop.Properties.Value;
  rec.BLPoint :=Params.Property_BLPos;
//  rec.FolderID:=0;

  if (not IsPropExist) then
  begin
 //   ShowMessage('property:'+ rec.Name+'   property:'+ IntTostr(rec.FolderID) );

    if (dmProperty.FindByNameAndFolder(rec.Name, rec.FolderID) > 0) then
    begin
      ErrDlg('�������� '+rec.Name+' ��� ���������� �� ��. ���������� ����������.');
      exit;
    end;

    Result:=dmProperty.Add(rec);

    dmMapEngine1.CreateObject (otProperty, Result);
  end
  else
    Result:=dmProperty.FindByName(rec.Name);
}

 // Result:=FPropertyID;

end;





procedure Tdlg_Repeater_add.row_Property_typeEditPropertiesChange(
  Sender: TObject);
var
  v: Variant;
begin
  //v:=row_Property_type.Properties.Value;

end;

end.



 {

//-------------------------------------------------------------------
procedure Tdlg_PMP_site_add.DoOnProperty_TypeChange;
//-------------------------------------------------------------------
var
  iInd: integer;
begin
  iInd:=cx_GetRowItemIndex(row_Property_type);

  row_Property_new_name.Visible :=iInd=0;
  row_Property.Visible          :=iInd=1;
end;




//--------------------------------------------------------------------
function Tframe_Link_add_from_Profile.AddProperty1: integer;
//--------------------------------------------------------------------
var
  oPropertyAdd: TPropertyAddToDB;
 // rec: TdmPropertyAddRec;
begin
  //FillChar(rec, SizeOf(rec), 0);


  if FPropertyID=0 then
  begin
    oPropertyAdd := TPropertyAddToDB.Create();

    oPropertyAdd.Project_ID:=dmMain.ProjectID;

    oPropertyAdd.NewName  :=row_Property.Properties.Value;

    oPropertyAdd.Address:=AsString(row_Property_address.Properties.Value);

    oPropertyAdd.BLPoint_Pulkovo :=Params.Property_BLPos;

  //  oPropertyAdd.Height    := AsFloat(row_Height.Properties.Value);
   // oPropertyAdd.Azimuth   := row_Azimuth.Properties.Value;
  //  FAntennaAdd.PropertyID:= FPropertyID;

  //  oPropertyAdd.AntennaType_ID :=FRec.AntennaTypeID;

    Result:=oPropertyAdd.Add; //(dmMain.ADOConnection);

    FreeAndNil(oPropertyAdd);

//
//
//
//    rec.Name   :=row_Property.Properties.Value;
//    rec.BLPoint_Pulkovo:=Params.Property_BLPos;
//    rec.Address:=row_Property_address.Properties.Value;
//
//    Result:=dmProperty.Add(rec);

    //---------------------------------------------------

    if Result>0 then
      dmMapEngine_store.Feature_Add (OBJ_Property, Result);

  end else
    Result:=FPropertyID;










{
  FillChar(rec, SizeOf(rec), 0);

  rec.Name    :=row_Prop.Properties.Value;
  rec.BLPoint :=Params.Property_BLPos;
//  rec.FolderID:=0;

  if (not IsPropExist) then
  begin
 //   ShowMessage('property:'+ rec.Name+'   property:'+ IntTostr(rec.FolderID) );

    if (dmProperty.FindByNameAndFolder(rec.Name, rec.FolderID) > 0) then
    begin
      ErrDlg('�������� '+rec.Name+' ��� ���������� �� ��. ���������� ����������.');
      exit;
    end;

    Result:=dmProperty.Add(rec);

    dmMapEngine1.CreateObject (otProperty, Result);
  end
  else
    Result:=dmProperty.FindByName(rec.Name);
}

 // Result:=FPropertyID;

end;

