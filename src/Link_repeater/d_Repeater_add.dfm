inherited dlg_Repeater_add: Tdlg_Repeater_add
  Left = 839
  Top = 338
  Width = 601
  Height = 518
  Caption = 'dlg_Repeater_add'
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 455
    Width = 593
    inherited Bevel1: TBevel
      Width = 593
    end
    inherited Panel3: TPanel
      Left = 418
      Width = 175
      inherited btn_Ok: TButton
        Left = 4
      end
      inherited btn_Cancel: TButton
        Left = 88
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 593
    inherited Bevel2: TBevel
      Width = 593
    end
    inherited pn_Header: TPanel
      Width = 593
      Height = 55
    end
  end
  inherited Panel1: TPanel
    Width = 593
    inherited ed_Name_: TEdit
      Width = 582
    end
  end
  inherited Panel2: TPanel
    Width = 593
    Height = 316
    inherited PageControl1: TPageControl
      Width = 583
      Height = 284
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 575
          Height = 253
          Align = alClient
          LookAndFeel.Kind = lfOffice11
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 139
          OptionsView.ValueWidth = 50
          TabOrder = 0
          Version = 1
          object row_Property_type: TcxEditorRow
            Properties.Caption = 'Property_type'
            Properties.ImageIndex = 0
            Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
            Properties.EditProperties.Columns = 2
            Properties.EditProperties.DefaultValue = 0
            Properties.EditProperties.Items = <
              item
                Caption = #1053#1086#1074#1072#1103
                Value = 0
              end
              item
                Caption = #1057#1091#1097#1077#1089#1090#1074#1091#1102#1097#1072#1103
                Value = 1
              end>
            Properties.EditProperties.OnChange = row_Property_typeEditPropertiesChange
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            Visible = False
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object row_Property_name: TcxEditorRow
            Properties.Caption = #1055#1083#1086#1097#1072#1076#1082#1072
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = cxVerticalGrid1EditorRow1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
          object row_Property_pos: TcxEditorRow
            Properties.Caption = 'Property_pos'
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            Visible = False
            ID = 2
            ParentID = 1
            Index = 0
            Version = 1
          end
          object row_Property_address: TcxEditorRow
            Properties.Caption = 'Property_address'
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            Visible = False
            ID = 3
            ParentID = 1
            Index = 1
            Version = 1
          end
          object row_is_active: TcxEditorRow
            Properties.Caption = #1058#1080#1087
            Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
            Properties.EditProperties.Items = <
              item
                Caption = #1055#1072#1089#1089#1080#1074#1085#1099#1081
                Value = '0'
              end
              item
                Caption = #1040#1082#1090#1080#1074#1085#1099#1081
                Value = '1'
              end>
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = '0'
            ID = 4
            ParentID = -1
            Index = 2
            Version = 1
          end
          object cxVerticalGrid1CategoryRow1: TcxCategoryRow
            ID = 5
            ParentID = -1
            Index = 3
            Version = 1
          end
          object row_Antenna1: TcxCategoryRow
            Properties.Caption = 'Antenna1'
            ID = 6
            ParentID = -1
            Index = 4
            Version = 1
          end
          object row_Antenna1_model: TcxEditorRow
            Properties.Caption = 'Model'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = cxVerticalGrid1EditorRow1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 7
            ParentID = 6
            Index = 0
            Version = 1
          end
          object row_Antenna1_H: TcxEditorRow
            Properties.Caption = 'H'
            Properties.DataBinding.ValueType = 'Float'
            Properties.Value = 10.000000000000000000
            ID = 8
            ParentID = 6
            Index = 1
            Version = 1
          end
          object row_Antenna1_Gain: TcxEditorRow
            Properties.Caption = 'row_Antenna1_Gain'
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = '40'
            ID = 9
            ParentID = 6
            Index = 2
            Version = 1
          end
          object row_Antenna2: TcxCategoryRow
            Properties.Caption = 'Antenna2'
            ID = 10
            ParentID = -1
            Index = 5
            Version = 1
          end
          object row_Antenna2_model: TcxEditorRow
            Properties.Caption = 'Model'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = cxVerticalGrid1EditorRow1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 11
            ParentID = 10
            Index = 0
            Version = 1
          end
          object row_Antenna2_H: TcxEditorRow
            Properties.Caption = 'H'
            Properties.DataBinding.ValueType = 'Float'
            Properties.Value = 10.000000000000000000
            ID = 12
            ParentID = 10
            Index = 1
            Version = 1
          end
          object row_Antenna2_Gain: TcxEditorRow
            Properties.Caption = 'row_Antenna2_Gain'
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = '40'
            ID = 13
            ParentID = 10
            Index = 2
            Version = 1
          end
        end
      end
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 454
    Top = 6
  end
end
