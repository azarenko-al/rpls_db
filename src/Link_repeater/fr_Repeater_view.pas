unit fr_Repeater_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, Menus, ToolWin, Grids, DBGrids,
  cxPropertiesStore,
  ComCtrls, ExtCtrls, DBCtrls,  ImgList,  cxSplitter,
 
  dm_Onega_DB_data,

  //fr_LinkEnd_antennas,
  fr_repeater_antennas,

  fr_DBInspector_Container,

  u_dlg,
  u_func,
  u_db,
  u_reg,

  u_types,
  u_const_db,

  dm_Repeater,
 // dm_MSC_View,

  u_const_str,

  fr_View_base, cxControls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters, dxBar,
  cxBarEditItem, cxClasses, cxCheckBox, cxButtonEdit
    ;

type
  Tframe_Repeater_View = class(Tframe_View_Base)
    pn_Inspector: TPanel;
    cxSplitter1: TcxSplitter;
    pn_Antennas: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    Ffrm_DBInspector: Tframe_DBInspector_Container;

    Fframe_Link_repeater_antennas: Tframe_Link_repeater_antennas;

  public
    procedure View(aID: integer; aGUID: string); override;
  end;


implementation

{$R *.dfm}


//--------------------------------------------------------------------
procedure Tframe_Repeater_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_LINK_Repeater;

  TableName:=TBL_LINK_Repeater;



  pn_Inspector.Align:=alClient;

 // dbGrid1.Columns[0].;

//  col_Name.FieldName:=FLD_NAME;
 // col_Name.Caption:=STR_COL_NAME;

  CreateChildForm(Tframe_DBInspector_Container, Ffrm_DBInspector, pn_Inspector);
  Ffrm_DBInspector.PrepareViewForObject(OBJ_LINK_Repeater);


  CreateChildForm(Tframe_Link_repeater_antennas,  Fframe_Link_repeater_antennas,  pn_Antennas);


//  AddComponentProp(pn_Antennas, PROP_HEIGHT);


//  :=.CreateChildForm(pn_Inspector, OBJ_MSC);

 // dmMSC_View.InitDB (mem_Items);

 // AddControl(dxDBGrid1, [PROP_HEIGHT]);

 // AddComponentProp(dbGrid1, PROP_HEIGHT);
 // cxPropertiesStore.RestoreFrom;
end;


procedure Tframe_Repeater_View.FormDestroy(Sender: TObject);
begin
  FreeAndNil(Ffrm_DBInspector);
  FreeAndNil(Fframe_Link_repeater_antennas);

  inherited;
end;


procedure Tframe_Repeater_View.View(aID: integer; aGUID: string);
//const
 // SQL_SELECT_ITEMS = 'SELECT id,name FROM bsc WHERE msc_id=:msc_id';

begin

  inherited;

  if not RecordExists (aID) then
    Exit;



  Ffrm_DBInspector.View (aID);

  Fframe_Link_repeater_antennas.View (aID);


 // dmOnega_DB_data.OpenQuery (qry_Items, SQL_SELECT_ITEMS,  [db_Par(FLD_msc_id, aID)]);

//  dmMSC_View.OpenDB (aID, mem_Items);
end;



end.


(*

  Fframe_Inspector.View (aID, OBJ_LINKEND);

  // -------------------------
  FGeoRegion_ID := Fframe_Inspector.GetDataset().FieldByName(FLD_GeoRegion_ID).AsInteger;// GetIntFieldValue(FLD_GeoRegion_ID);
  bReadOnly:=dmUser_Security.GeoRegion_is_ReadOnly(FGeoRegion_ID);

  Fframe_Inspector.SetReadOnly(bReadOnly, IntToStr(FGeoRegion_ID));
  Fframe_LinkEnd_antennas.SetReadOnly(bReadOnly);
  // -------------------------

  Fframe_LinkEnd_antennas.View (aID, OBJ_LINKEND);

*)


(*

  SQL_SELECT_TEMPLATE   = 'SELECT id,name FROM %s WHERE msc_id=:msc_id';

  //-------------------------------------------------------------------
  procedure DoAddRecords (aTableName,aObjectName: string);
  //-------------------------------------------------------------------
  var sSQL: string;
  begin
    sSQL:=Format(SQL_SELECT_TEMPLATE, [aTableName]);

    db_OpenQuery (gl_DB.Query, sSQL, [db_Par(FLD_MSC_ID, aID)] );
*)