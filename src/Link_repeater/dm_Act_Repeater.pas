unit dm_Act_Repeater;

interface
{$I ver.inc}


uses
  Classes, Forms, ActnList, Menus, SysUtils,

  dm_act_Base,

  d_Object_dependence,

  I_Object,

  

 // I_Act_Antenna, //dm_Act_Antenna
//  I_Act_Repeater, //dm_Act_Repeater,

  

  dm_Repeater,

  u_Func,
  u_Geo,

  u_const_str,

  u_types,

  fr_Repeater_view,
  d_Repeater_add
  ;


type
  TdmAct_Link_Repeater = class(TdmAct_Base) //, IAct_Repeater_X)
    ActionList2: TActionList;
    act_Antenna_Add: TAction;
    act_Object_dependence: TAction;
 //   procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FBLPoint: TBLPoint;
    procedure ShowOnMap(aID: integer);
  protected
    procedure DoAction (Sender: TObject);

    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm (aOwnerForm: TForm): TForm; override;


  public
    procedure AddByPos(aBLPoint: TBLPoint); overload;
    procedure AddByProperty(aProperty: integer);

 //   function ItemDel(aID: integer): boolean; override;

    function ItemDel(aID: integer; aName: string): boolean; override;

    class procedure Init;
  end;


var
  dmAct_Link_Repeater: TdmAct_Link_Repeater;

//====================================================================
// implementation
//====================================================================
implementation

uses dm_act_Antenna; {$R *.dfm}



//--------------------------------------------------------------------
class procedure TdmAct_Link_Repeater.Init;
//--------------------------------------------------------------------
begin
  Assert(not Assigned(dmAct_Link_Repeater), 'Value not assigned');

  Application.CreateForm(TdmAct_Link_Repeater, dmAct_Link_Repeater);

 // dmAct_Repeater.GetInterface(IAct_Repeater_X, IAct_Repeater);
//  Assert(Assigned(IAct_Repeater));

end;


//-------------------------------------------------------------------
procedure TdmAct_Link_Repeater.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
var b: Boolean;
begin
  inherited;
  ObjectName:=OBJ_LINK_Repeater;


  act_Add.Caption := '������� ��������� ������������';

  act_Object_dependence.Caption:=S_OBJECT_DEPENDENCE;//  '�������� �����������';

  SetActionsExecuteProc ([
           act_Object_ShowOnMap,

           act_Object_dependence
           
       //    act_Antenna_Add
         ], DoAction);


(*
 SetCheckedActionsArr( ([
           act_Add,
           act_Del_
         ]);
*)

end;


//--------------------------------------------------------------------
function TdmAct_Link_Repeater.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_Repeater_View.Create(aOwnerForm);
end;


//--------------------------------------------------------------------
procedure TdmAct_Link_Repeater.ShowOnMap(aID: integer);
//--------------------------------------------------------------------
begin
  ShowPointOnMap (dmRepeater.GetPos (aID), aID);
 // HighLightObjectOnMap (aID);
end;


//--------------------------------------------------------------------
procedure TdmAct_Link_Repeater.AddByPos(aBLPoint: TBLPoint);
//--------------------------------------------------------------------
begin
  Tdlg_Repeater_add.ExecDlg (0, aBLPoint);
end;

procedure TdmAct_Link_Repeater.AddByProperty(aProperty: integer);
begin
  Tdlg_Repeater_add.ExecDlg (aProperty, NULL_POINT);

end;

//--------------------------------------------------------------------
function TdmAct_Link_Repeater.ItemDel(aID: integer; aName: string): boolean;
//--------------------------------------------------------------------
//var
 // sGUID: string;
begin
 // sGUID:= dmMSC.GetGUIDByID(aID);

//  FDeletedIDList.AddObjNameAndGUID(aID, sGUID, OBJ_MSC);

  Result:=dmRepeater.Del (aID);

  // then
   // g_ShellEvents.Shell_PostDelNode (sGUID);
end;

//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_Link_Repeater.DoAction (Sender: TObject);
begin
  if Sender = act_Object_dependence then
    Tdlg_Object_dependence.ExecDlg(OBJ_LINK_Repeater, FFocusedName, FFocusedID)
  else  

  //------------------------------------------------------
  if Sender=act_Antenna_Add then
  //------------------------------------------------------
  begin
   // Assert(Assigned(IAct_Antenna), 'IAct_Antenna not assigned');

    dmAct_Antenna.Dlg_Add (OBJ_LINK_Repeater, FFocusedID);
  end else

  //------------------------------------------------------
  if Sender=act_Object_ShowOnMap then
    ShowOnMap (FFocusedID) else


  raise EClassNotFound.Create('');

(*  if Sender=act_Object_ShowOnMap then
    ShowOnMap (FFocusedID) else ;
*)

end;

//--------------------------------------------------------------------
procedure TdmAct_Link_Repeater.GetPopupMenu;
//--------------------------------------------------------------------
begin
  case aMenuType of
    mtFolder: begin
                AddFolderMenu_Create (aPopupMenu);
              //  AddMenuItem (aPopupMenu, act_Add);
                AddFolderPopupMenu (aPopupMenu);
              end;

    mtItem: begin
                AddMenuItem (aPopupMenu, act_Object_ShowOnMap);
                AddMenuItem (aPopupMenu, nil);

                AddMenuItem (aPopupMenu, act_Del_);
                AddMenuItem (aPopupMenu, act_Object_dependence);


            //   AddFolderMenu_Tools (aPopupMenu);

             //   AddMenuItem (aPopupMenu, act_Antenna_Add);


            end;
    mtList: begin
                AddMenuItem (aPopupMenu, act_Del_list);
                AddFolderMenu_Tools (aPopupMenu);

            end;
  end;
end;
 

end.




(*
//--------------------------------------------------------------------
procedure TdmAct_Repeater.ShowOnMap (aID: integer);
//--------------------------------------------------------------------
begin
 // ShowPointOnMap (dmMSC.GetPropertyPos(aID), aID);
end;

*)



(*
procedure TdmAct_Repeater.DataModuleDestroy(Sender: TObject);
begin
//  IAct_Repeater :=nil;
//  dmAct_Repeater :=nil;

  inherited;
end;
*)
