unit dm_Repeater;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms, Variants,

 // dm_Object_base,

  dm_Onega_DB_data,

  u_geo,

  u_const_db,

  u_classes,
  u_func,
  u_db,

  u_types,

  //dm_Folder,
  dm_Main ;

 // dm_Object_base;

type
  //-------------------------------------------------------------------
  TdmRepeaterAddRec = record
  //-------------------------------------------------------------------
    NewName: string;
    PropertyID: integer;

    IS_ACTIVE: boolean;

    Antenna1,Antenna2: record
                         AntennaType_ID : Integer;
                         Height : double;
                         Gain   : double;
                       end;

   // FolderID: integer;
  end;


  TdmRepeater = class(TDatamodule)
    ADOStoredProc1: TADOStoredProc;
    ADOQuery1: TADOQuery;
  //  procedure DataModuleCreate(Sender: TObject);
  private
  public
    function Add(aRec: TdmRepeaterAddRec): integer;
//    function Check_for_Link(aID, aLink_ID: integer): Integer;
    function Del(aID: integer): boolean;

    function GetNewName: string;
    function GetPos(aID: integer): TBLPoint;


  end;


function dmRepeater: TdmRepeater;


//================================================================
implementation
   {$R *.DFM}

var
  FdmRepeater: TdmRepeater;


// ---------------------------------------------------------------
function dmRepeater: TdmRepeater;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmRepeater) then
    FdmRepeater := TdmRepeater.Create(Application);

  Result := FdmRepeater;
end;



//--------------------------------------------------------------------
function TdmRepeater.GetPos(aID: integer): TBLPoint;
//--------------------------------------------------------!-----------
begin
   gl_DB.GetPosByID (VIEW_LINK_Repeater, aID, Result.B, Result.L );
end;


//--------------------------------------------------------------------
function TdmRepeater.GetNewName: string;
//--------------------------------------------------------------------
var
  rec: TObject_GetNewName_Params;
begin
  FillChar(rec,SizeOf(rec),0);

  rec.ObjName    := OBJ_LINK_Repeater;
  rec.Project_ID := dmMain.ProjectID;
 // rec.Property_ID := aPropertyID;
             
 // Assert (ObjectName<>'');

  Result := dmOnega_DB_data.Object_GetNewName_new
      (rec, OBJ_LINK_Repeater, dmMain.ProjectID, 0);// aPropertyID);

end;

// ---------------------------------------------------------------
function TdmRepeater.Del(aID: integer): boolean;
// ---------------------------------------------------------------
begin
//  dmOnega_DB_data.MSC_del(aID);

  Result := dmOnega_DB_data.ExecStoredProc_ ('sp_Link_Repeater_Del', [FLD_ID, aID]) > 0;

// -- Result:=True;
  //gl_DB.ExecSP (sp_MSC_del,  [db_Par(FLD_ID, aID)]);

end;



//---------------------------------------------------------
function TdmRepeater.Add(aRec: TdmRepeaterAddRec): integer;
//---------------------------------------------------------
begin
  Result := dmOnega_DB_data.OpenStoredProc (ADOStoredProc1,
     'sp_Link_Repeater_Add',
     [
      FLD_NAME,        aRec.NewName,
      FLD_PROPERTY_ID, aRec.PropertyID,

      FLD_IS_ACTIVE,    aRec.IS_ACTIVE,

      FLD_Antenna1_AntennaType_ID, aRec.Antenna1.AntennaType_ID,
      FLD_Antenna2_AntennaType_ID, aRec.Antenna2.AntennaType_ID,

      FLD_Antenna1_Height,         aRec.Antenna1.Height,
      FLD_Antenna2_Height,         aRec.Antenna2.Height,

      FLD_Antenna1_Gain,         aRec.Antenna1.Gain,
      FLD_Antenna2_Gain,         aRec.Antenna2.Gain

     ]);


//  db_View(ADOStoredProc1);

end;


begin
end.


