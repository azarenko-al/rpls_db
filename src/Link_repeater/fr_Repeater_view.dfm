inherited frame_Repeater_View: Tframe_Repeater_View
  Left = 700
  Top = 247
  Width = 601
  Height = 503
  Caption = 'frame_Repeater_View'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 585
    Height = 121
    TabOrder = 1
  end
  inherited pn_Caption: TPanel
    Top = 148
    Width = 585
    TabOrder = 0
  end
  inherited pn_Main: TPanel
    Top = 165
    Width = 585
    Height = 190
    object pn_Inspector: TPanel
      Left = 1
      Top = 1
      Width = 583
      Height = 64
      Align = alTop
      BevelOuter = bvLowered
      Caption = 'pn_Inspector'
      TabOrder = 0
    end
    object cxSplitter1: TcxSplitter
      Left = 1
      Top = 84
      Width = 583
      Height = 8
      HotZoneClassName = 'TcxSimpleStyle'
      AlignSplitter = salBottom
      Control = pn_Antennas
    end
    object pn_Antennas: TPanel
      Left = 1
      Top = 92
      Width = 583
      Height = 97
      Align = alBottom
      BevelOuter = bvLowered
      Caption = 'pn_Antennas'
      TabOrder = 2
    end
  end
  inherited PopupMenu1: TPopupMenu
    Left = 140
  end
  inherited cxLookAndFeelController1: TcxLookAndFeelController
    Left = 80
    Top = 416
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    Left = 80
    Top = 368
    DockControlHeights = (
      0
      0
      27
      0)
  end
end
