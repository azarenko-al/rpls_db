object frame_Link_repeater_antennas: Tframe_Link_repeater_antennas
  Left = 512
  Top = 734
  Width = 867
  Height = 437
  Caption = 'frame_Link_repeater_antennas'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 859
    Height = 136
    Align = alTop
    TabOrder = 0
    OnExit = cxGrid1Exit
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      PopupMenu = PopupMenu1
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = ds_qry_Antennas
      DataController.KeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      OptionsView.HeaderAutoHeight = True
      OptionsView.Indicator = True
      object col_ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Visible = False
      end
      object col___AntType_Name: TcxGridDBColumn
        DataBinding.FieldName = 'ANTENNATYPE_NAME'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.AutoSelect = False
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.ReadOnly = True
        Properties.OnButtonClick = col___AntType_NamePropertiesButtonClick
        Width = 131
      end
      object col___azimuth: TcxGridDBColumn
        DataBinding.FieldName = 'azimuth'
        Width = 49
      end
      object col___height: TcxGridDBColumn
        DataBinding.FieldName = 'height'
        Width = 67
      end
      object col___Diameter: TcxGridDBColumn
        DataBinding.FieldName = 'Diameter'
        Options.Editing = False
        Width = 55
      end
      object col___antenna_loss: TcxGridDBColumn
        DataBinding.FieldName = 'loss'
        Width = 81
      end
      object col___Gain: TcxGridDBColumn
        DataBinding.FieldName = 'Gain'
        Options.Editing = False
        Width = 41
      end
      object col___Polarization_STR: TcxGridDBColumn
        DataBinding.FieldName = 'Polarization_STR'
        Options.Editing = False
        Width = 82
      end
      object col___vert_width: TcxGridDBColumn
        DataBinding.FieldName = 'vert_width'
        Options.Editing = False
      end
      object col___horz_width: TcxGridDBColumn
        DataBinding.FieldName = 'horz_width'
        Options.Editing = False
      end
      object col___Pos: TcxGridDBColumn
        DataBinding.FieldName = 'pos'
        Visible = False
        Options.Editing = False
        Width = 72
      end
      object col_slot: TcxGridDBColumn
        DataBinding.FieldName = 'slot'
      end
      object col_repeater_gain: TcxGridDBColumn
        Caption = #1059#1089#1080#1083#1077#1085#1080#1077' '#1088#1077#1090#1088#1072#1085#1089#1083#1103#1090#1086#1088#1072', dB'
        DataBinding.FieldName = 'repeater_gain'
        Width = 111
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 92
    Top = 204
    object act_Antenna_Edit: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
    end
    object act_AntennaType_Edit: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1084#1086#1076#1077#1083#1100' '#1072#1085#1090#1077#1085#1085#1099
      OnExecute = act_AntennaType_EditExecute
    end
    object act_Audit: TAction
      Caption = 'act_Audit'
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 24
    Top = 204
    object actAntennaTypeEdit1: TMenuItem
      Action = act_AntennaType_Edit
    end
    object actAudit1: TMenuItem
      Action = act_Audit
    end
  end
  object ds_qry_Antennas: TDataSource
    DataSet = ADOQuery1
    Left = 173
    Top = 252
  end
  object ADOConnection1: TADOConnection
    CommandTimeout = 60
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link_mts;Data Source=SERVER1'
    ConnectionTimeout = 30
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 304
    Top = 208
  end
  object ADOQuery1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select top 2 * from '
      '  view_Link_repeater_Antenna')
    Left = 176
    Top = 200
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 312
    Top = 272
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clMoneyGreen
    end
  end
end
