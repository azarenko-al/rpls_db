object frm_Test_MSC: Tfrm_Test_MSC
  Left = 224
  Top = 128
  Width = 560
  Height = 331
  Caption = 'frm_Test_MSC'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pc_Main: TPageControl
    Left = 263
    Top = 0
    Width = 289
    Height = 302
    ActivePage = TabSheet1
    Align = alRight
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
    end
  end
  object bt_Add: TButton
    Left = 48
    Top = 32
    Width = 75
    Height = 25
    Caption = 'bt_Add'
    TabOrder = 1
    OnClick = bt_AddClick
  end
  object bt_View: TButton
    Left = 48
    Top = 88
    Width = 75
    Height = 25
    Caption = 'bt_View'
    TabOrder = 2
    OnClick = bt_ViewClick
  end
  object FormStorage1: TFormStorage
    StoredValues = <>
    Left = 64
    Top = 128
  end
end
