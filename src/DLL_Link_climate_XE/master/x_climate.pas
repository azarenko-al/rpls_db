unit x_climate;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, ActiveX, Classes, ComObj, Climate_TLB, StdVcl;

type
  TClimateX = class(TTypedComObject, IClimateX)
  protected
    function Find(aLat, aLon: Double; aData: Integer): HResult; stdcall;
  end;

implementation

uses ComServ;

function TClimateX.Find(aLat, aLon: Double; aData: Integer): HResult;
begin

end;

initialization
  TTypedComObjectFactory.Create(ComServer, TClimateX, Class_ClimateX,
    ciMultiInstance, tmApartment);
end.
