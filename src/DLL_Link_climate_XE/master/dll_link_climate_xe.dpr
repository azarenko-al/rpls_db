library dll_link_climate_xe;

uses
  ComServ,
  Climate_TLB in 'Climate_TLB.pas',
  x_climate in 'x_climate.pas' {ClimateX: CoClass},
  u_Link_climate_file in 'u_Link_climate_file.pas',
  I_link_climate in '..\I_link_climate.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer,
  DllInstall;

{$R *.TLB}

{$R *.RES}

begin
end.
