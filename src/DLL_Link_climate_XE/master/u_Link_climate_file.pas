unit u_Link_climate_file;

interface
//{$I mapx.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Variants, FileCtrl,

  MapXLib_TLB,

  u_assert,


  u_MapX_lib,

  u_func,
  u_files,
  u_Geo,

  I_link_climate
  ;

type

{
  TMapItem = class(TObject)
  public
    BLPointArr: TBLPointArray;

    Data: TLinkClimateDataRec;
  end;

  TMapItemList = class(TList)
  public
    function AddItem: TMapItem;
//    FileType : ();

    function LoadFromTab(aFileName: string): Boolean;

    function FindData(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec): Boolean;
  end;
}
 {
  TRainIntense = class(TObject)
  private
    FData : array[0..120-1] of
            array[0..240-1] of Double;

  public

    function LoadFromFile(aFileName: string): Boolean;

    function FindData(aBLPoint: TBLPoint; var aValue: double): Boolean;
  end;
  }
//function DoGetRainIntenseByPoint(aPoint: TBLPoint): Double;



  TLink_climate_file = class(TInterfacedObject, ILink_climate_fileX)
  private
//..    FRainIntense: TRainIntense;

    FActive: boolean;
  //  FActive_GOST: boolean;

//    FFileExists: Boolean;
//    FIsOpenedFiles: boolean;

    FFileDir: string;

//    FTxtFile_Rain        : TextFile;

    FMap_Q_factor        : TmiMap;
    FMap_RadioKlimRegion : TmiMap;
    FMap_Climate         : TmiMap;
    FMap_ClimateFactor   : TmiMap;

    //GOST
//    FMap_GOST_humidity: TmiMap;
//    FMap_GOST_radioklimat: TmiMap;

    FMap_NIIR_1998_rain_8_11: TmiMap;
    FMap_NIIR_1998_rain_8_15: TmiMap;
    FMap_NIIR_1998_humidity: TmiMap;

    FMap_GOST_rain_intensity: TmiMap;



    function ValidateFiles: Boolean;

    function DoGetRainIntenseByPoint(aPoint: TBLPoint): Double;

    function GetFileDir: string;

    function Get_Climate(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec): Boolean;
    function Get_ClimateFactor(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec): Boolean;
    function Get_GOST_rain(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec):
        Boolean;
    function Get_Q_factor(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec): Boolean;
    function Get_RadioKlimRegion(aBLPoint: TBLPoint; var aRec:  TLinkClimateDataRec): Boolean;

// TODO: Get_RadioKlimRegion
//  function Get_RadioKlimRegion(aBLPoint: TBLPoint; var aRec:
//      TLinkClimateDataRec): Boolean;

  //  function Get_GOST_humidity(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec):
     //   Boolean;

// TODO: Get_GOST_radioklimat
//  function Get_GOST_radioklimat(aBLPoint: TBLPoint; var aRec:
//      TLinkClimateDataRec): Boolean;

    function Get_NIIR_1998_rain_8_11(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec): Boolean;
    function Get_NIIR_1998_rain_8_15(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec): Boolean;
    function Get_NIIR_1998_humidity(aBLPoint: TBLPoint; var aRec:
        TLinkClimateDataRec): Boolean;



    //humidity.TAB
    //radioklimat.TAB
    //rain_8_11.TAB
    //rain_8_15.TAB


// TODO: test_CreateMap
//  procedure test_CreateMap;

  public
    constructor Create;
    destructor Destroy; override;

    function OpenFiles: Boolean; stdcall;
    procedure CloseFiles; stdcall;

// TODO: CloseFiles_GOST
//// TODO: OpenFiles_GOST
////  function OpenFiles_GOST: Boolean; stdcall;
//  procedure CloseFiles_GOST; stdcall;


    function GetByBLPoint(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec):
        Boolean; stdcall;

// TODO: GetByBLPoint_GOST
//  function GetByBLPoint_GOST(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec):
//      Boolean; stdcall;

 //   function GetByBLPoint_GOST(aBLPoint: TBLPoint; var aRec:
  //      TdmLinkClimateConditions_GOST_Rec): Boolean; stdcall;

    property Active: boolean read FActive;
  //  property Active_GOST: boolean read FActive_GOST;
  end;



  function GetInterface_ILink_climate_fileX(var Obj): HResult; stdcall;

exports
  GetInterface_ILink_climate_fileX;


implementation


function GetInterface_ILink_climate_fileX(var Obj): HResult;
begin
  Result:=S_FALSE; Integer(Obj):=0;

  with TLink_climate_file.Create do
    if GetInterface(ILink_climate_fileX, Obj) then
      Result:=S_OK

end;



const
  DEF_CLIMATE_FOLDER_NAME = '�����������������\';

  DEF_FileName_Climate          = '������.tab';
  DEF_FileName_Q_factor         = 'Q������.tab';
  DEF_FileName_RadioKlimRegion  = 'RadioKlimRegion.tab';
  DEF_FileName_ClimateFactor    = '������������.tab';
  DEF_FileName_Rain             = 'ESARAIN_R001.PRN';

//  DEF_FileName_humidity        ='NIIR_humidity.tab';
 // DEF_FileName_radioklimat     ='NIIR_gradient.TAB';
  DEF_FileName_rain_8_11       ='NIIR_rain_8_11.tab';
  DEF_FileName_rain_8_15       ='NIIR_rain_8_15.tab';
  DEF_FileName_humidity        ='NIIR_humidity.tab';

  DEF_FileName_GOST_rain_intensity = 'GOST_rain_intensity.tab';



 // FILE_ARR: array[0..4] of string;


// ---------------------------------------------------------------
constructor TLink_climate_file.Create;
// ---------------------------------------------------------------
var
  I: Integer;
  oSList: TStringList;
  s: string;
begin
  inherited;

  FFileDir := GetFileDir();

//  FRainIntense := TRainIntense.Create();
end;


// ---------------------------------------------------------------
function TLink_climate_file.ValidateFiles: Boolean;
// ---------------------------------------------------------------
var
  I: Integer;
  oSList: TStringList;
  s: string;
begin
  inherited;

//  FFileDir := GetFileDir();

  oSList := TStringList.Create;

  s:=FFileDir + DEF_FileName_Q_factor;
  if not FileExists(s) then oSList.Add(s);

  s:=FFileDir + DEF_FileName_RadioKlimRegion;
  if not FileExists(s) then oSList.Add(s);

  s:=FFileDir + DEF_FileName_Climate;
  if not FileExists(s) then oSList.Add(s);

  s:=FFileDir + DEF_FileName_ClimateFactor;
  if not FileExists(s) then oSList.Add(s);

  s:=FFileDir + DEF_FileName_Rain;
  if not FileExists(s) then oSList.Add(s);

  s:=FFileDir + DEF_FileName_humidity;
  if not FileExists(s) then oSList.Add(s);
                                   
  s:=FFileDir + DEF_FileName_GOST_rain_intensity;
  if not FileExists(s) then oSList.Add(s);


//  FFileExists:= oSList.Count=0;

  Result := oSList.Count=0;

  if oSList.Count>0 then
  begin
    oSList.Insert(0, '��� ������ �� ������������� ����� � ��������: '+ FFileDir);

    ShowMessage(oSList.Text);
  end;


(*        FileExists (FFileDir + DEF_FileName_Q_factor) and
        FileExists (FFileDir + DEF_FileName_RadioKlimRegion) and
        FileExists (FFileDir + DEF_FileName_Climate) and
        FileExists (FFileDir + DEF_FileName_ClimateFactor) and
        FileExists (FFileDir + DEF_FileName_Rain);

  if not FActive then
    ShowMessage(Format('� �������� - %s - ��� ������ �� ������������� �����.', [FFileDir]));

  *)

  FreeAndNil(oSList);
end;



destructor TLink_climate_file.Destroy;
begin
 // FreeAndNil(FRainIntense);
 // if FIsOpenedFiles then
    CloseFiles();

  inherited;
end;


// -------------------------------------------------------------------
function TLink_climate_file.GetFileDir: string;
// -------------------------------------------------------------------
begin
  Result:= GetApplicationDir() + 'DATA\' + DEF_CLIMATE_FOLDER_NAME;

  if not DirectoryExists(Result) then begin
    Result:= GetParentFileDir(GetApplicationDir()) + 'DATA\' + DEF_CLIMATE_FOLDER_NAME;
  end;
end;

// ---------------------------------------------------------------
procedure TLink_climate_file.CloseFiles;
// ---------------------------------------------------------------
begin
(*  FMap_Q_factor.CloseFile;
  FMap_RadioKlimRegion.CloseFile;
  FMap_Climate.CloseFile;
  FMap_ClimateFactor.CloseFile;
*)

  FreeAndNil(FMap_Q_factor);
  FreeAndNil(FMap_RadioKlimRegion);
  FreeAndNil(FMap_Climate);
  FreeAndNil(FMap_ClimateFactor);

  FreeAndNil(FMap_NIIR_1998_rain_8_11  );
  FreeAndNil(FMap_NIIR_1998_rain_8_15  );
  FreeAndNil(FMap_NIIR_1998_humidity  );

  FreeAndNil(FMap_GOST_rain_intensity  );


 // FActive_GOST := False;

  FActive := False;

 // FIsOpenedFiles:=False;
end;


// -------------------------------------------------------------------
function TLink_climate_file.DoGetRainIntenseByPoint(aPoint: TBLPoint): Double;
// -------------------------------------------------------------------
var
    i,j: Integer;
    i_lat,j_lon: integer;
    buf, d1, d2: double;
     FTxtFile_Rain: TextFile;
begin
  AssignFile(FTxtFile_Rain, GetFileDir() + DEF_FileName_Rain);
  Reset(FTxtFile_Rain);

  i_lat:=round((90-aPoint.B)/1.5);
  j_lon:=round(aPoint.L/1.5);

  {
  AssignFile(FTxtFile_Rain, GetFileDir() + DEF_FileName_Rain);
  Reset(FTxtFile_Rain);
  }

  if (i_lat in [0..120]) and (j_lon in [0..240]) then
  begin
    if i_lat>0 then for i:=0 to i_lat-1 do Readln(FTxtFile_Rain, d1);
    if j_lon>0 then for j:=0 to j_lon-1 do Read(FTxtFile_Rain, d2);

    buf:= 0;
    Read(FTxtFile_Rain, buf);
    Result:=buf;
  end
    else Result:=0;

  CloseFile(FTxtFile_Rain);

  //      CloseFile(FTxtFile_Rain);
end;


// -------------------------------------------------------------------
function TLink_climate_file.GetByBLPoint(aBLPoint: TBLPoint; var aRec:
    TLinkClimateDataRec): Boolean;
// -------------------------------------------------------------------
begin
  Result:=False;

  FillChar(aRec, SizeOf(aRec), 0);

  if not FActive then
    OpenFiles();



//  {$IFDEF Mapx5}
//  {$ELSE}
  // vPt := CoPoint.Create;
 // {$ENDIF}

  Result:=Get_Q_factor (aBLPoint, aRec);
  Result:=Get_Climate (aBLPoint, aRec);
  Result:=Get_ClimateFactor (aBLPoint, aRec);
  Result:=Get_RadioKlimRegion (aBLPoint, aRec);

  aRec.ITU.rain_intensity1:=DoGetRainIntenseByPoint (aBLPoint);

  aRec.ITU.steam_wet:=7.5;

  Result:=Get_NIIR_1998_rain_8_11 (aBLPoint, aRec);
  Result:=Get_NIIR_1998_rain_8_15 (aBLPoint, aRec);
  Result:=Get_NIIR_1998_humidity (aBLPoint, aRec);

  Result:=Get_GOST_rain (aBLPoint, aRec);


  aRec.ITU.underlying_terrain_type:=1;
  aRec.air_temperature :=15;
  aRec.atmosphere_pressure :=1013;


end;

// ---------------------------------------------------------------
function TLink_climate_file.OpenFiles: Boolean;
// ---------------------------------------------------------------
var
  sFileName: string;
begin
//  if not FFileExists then
//    Exit;

  Result := ValidateFiles;

{
  sFileName:=GetFileDir() + DEF_FileName_Q_factor;

  if not FileExists (sFileName) then
  begin
   ////////// ErrDlg_FileNotExists (GetFileDir() + DEF_FileName_Q_factor);
    ShowMessage ( 'File no found: ' + sFileName);
    Exit;
  end;}



  FMap_Q_factor:=TmiMap.Create;
  FMap_RadioKlimRegion:=TmiMap.Create;
  FMap_Climate:=TmiMap.Create;
  FMap_ClimateFactor:=TmiMap.Create;

  FMap_NIIR_1998_rain_8_11 := TmiMap.Create;
  FMap_NIIR_1998_rain_8_15 := TmiMap.Create;
  FMap_NIIR_1998_humidity  := TmiMap.Create;

  FMap_GOST_rain_intensity := TmiMap.Create;

//
//  FMap_GOST_radioklimat.OpenFile (FFileDir + DEF_FileName_radioklimat);
  FMap_NIIR_1998_rain_8_11.OpenFile   (FFileDir + DEF_FileName_rain_8_11);
  FMap_NIIR_1998_rain_8_15.OpenFile   (FFileDir + DEF_FileName_rain_8_15);
  FMap_NIIR_1998_humidity.OpenFile    (FFileDir + DEF_FileName_humidity);

//  FIsOpenedFiles:=true;

 // FActive:=
  FMap_Q_factor.OpenFile (FFileDir + DEF_FileName_Q_factor);
  FMap_RadioKlimRegion.OpenFile (FFileDir + DEF_FileName_RadioKlimRegion);
  FMap_Climate.OpenFile (FFileDir + DEF_FileName_Climate);
  FMap_ClimateFactor.OpenFile (FFileDir + DEF_FileName_ClimateFactor);

  FMap_GOST_rain_intensity.OpenFile (FFileDir + DEF_FileName_GOST_rain_intensity);



  FActive :=FMap_Q_factor.Active and
           (FMap_RadioKlimRegion.Active )and
           (FMap_Climate.Active )and
           (FMap_ClimateFactor.Active ) and

//  FActive := FActive and
  //         FMap_GOST_humidity.Active and
   //        FMap_GOST_radioklimat.Active and
           FMap_NIIR_1998_rain_8_11.Active and
           FMap_NIIR_1998_rain_8_15.Active and
           FMap_NIIR_1998_humidity.Active and
           FMap_GOST_rain_intensity.Active ;

end;



//// ---------------------------------------------------------------
function TLink_climate_file.Get_RadioKlimRegion(aBLPoint: TBLPoint; var aRec:
    TLinkClimateDataRec): Boolean;
//// ---------------------------------------------------------------
var
 //
  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vSelection: CMapXSelection;
  vRowValues: CMapXRowValues;
  vPt: CMapXPoint;
  iCount: integer;
  k: Integer;

  oMap : TmiMap;

//  s: string;
begin
  vPt := CoPoint.Create;

  oMap := FMap_RadioKlimRegion;

//// -------------------------------------------------------------------
//// RadioKlimRegion: string;
//// -------------------------------------------------------------------


  if oMap.Active then
  begin
    k:=oMap.VLayer.AllFeatures.Count;
  

      oMap.VLayer.Selection.SelectByPoint (aBLPoint.L, aBLPoint.B, miSelectionNew, EmptyParam);

      vSelection:=oMap.VLayer.Selection;
      iCount:=vSelection.Count;

      if iCount>0 then
        vFeature:=vSelection.Item[1];


    if iCount>0 then
    begin
      vRowValues:=oMap.VDataset.RowValues[vFeature];

       aRec.ITU.RadioClimate_Region_name:=AsString(mapx_RowValues_GetItemValue(vRowValues,'name'));
       aRec.ITU.gradient_diel     :=AsFloat(mapx_RowValues_GetItemValue(vRowValues,'gradient'));
       aRec.ITU.gradient_deviation:=AsFloat(mapx_RowValues_GetItemValue(vRowValues,'deviation'));
    end;


    Result:=iCount>0;
  end else
    Result:=False;
end;


// ---------------------------------------------------------------
function TLink_climate_file.Get_Climate(aBLPoint: TBLPoint; var aRec:
    TLinkClimateDataRec): Boolean;
// ---------------------------------------------------------------
var
 //
  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vSelection: CMapXSelection;
  vRowValues: CMapXRowValues;
  vPt: CMapXPoint;
  iCount: integer;
  k: Integer;

  oMap : TmiMap;

//  s: string;
begin
  vPt := CoPoint.Create;

  oMap := FMap_Climate;


  // -------------------------------------------------------------------
  // ������ : string;
  // -------------------------------------------------------------------
 // s :=GetFileDir() + DEF_FileName_Climate;


  if oMap.Active then
  begin
    k:=oMap.VLayer.AllFeatures.Count;



      oMap.VLayer.Selection.SelectByPoint (aBLPoint.L, aBLPoint.B, miSelectionNew, EmptyParam);

      vSelection:=oMap.VLayer.Selection;
      iCount:=vSelection.Count;

      if iCount>0 then
        vFeature:=vSelection.Item[1];



    if iCount>0 then
    begin
      vRowValues:=oMap.VDataset.RowValues[vFeature];

      aRec.ITU.factor_C:=AsFloat(mapx_RowValues_GetItemValue(vRowValues,'factor_C'));

    end;


    Result:=iCount>0;
  end else
    Result:=False;
end;

// ---------------------------------------------------------------
function TLink_climate_file.Get_ClimateFactor(aBLPoint: TBLPoint; var aRec:
    TLinkClimateDataRec): Boolean;
// ---------------------------------------------------------------
var
//  vPt := CoPoint.Create;

  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vSelection: CMapXSelection;
  vRowValues: CMapXRowValues;
  vPt: CMapXPoint;
  iCount: integer;

  oMap : TmiMap;

begin
  vPt := CoPoint.Create;

  oMap := FMap_ClimateFactor;


  // -------------------------------------------------------------------
  // ClimateFactor: string;
  // -------------------------------------------------------------------
  if oMap.Active then
  begin

      oMap.VLayer.Selection.SelectByPoint (aBLPoint.L, aBLPoint.B, miSelectionNew, EmptyParam);

      vSelection:=oMap.VLayer.Selection;
      iCount:=vSelection.Count;

      if iCount>0 then
        vFeature:=vSelection.Item[1];




    if iCount>0 then
    begin
      vRowValues:=oMap.VDataset.RowValues[vFeature];

      aRec.ITU.factor_B:=AsFloat(mapx_RowValues_GetItemValue(vRowValues,'factor_B'));
      aRec.ITU.factor_D:=AsFloat(mapx_RowValues_GetItemValue(vRowValues,'factor_D'));
      aRec.ITU.Climate_factor:=Round(AsFloat(mapx_RowValues_GetItemValue(vRowValues,'climate_factor')));
    end;

    Result:=iCount>0;

  end else
    Result:=False;

end;

// ---------------------------------------------------------------
function TLink_climate_file.Get_NIIR_1998_humidity(aBLPoint: TBLPoint; var
    aRec: TLinkClimateDataRec): Boolean;
// ---------------------------------------------------------------
var
  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vSelection: CMapXSelection;
  vRowValues: CMapXRowValues;
  vPt: CMapXPoint;
  iCount: integer;
  k: Integer;

  oMap : TmiMap;

begin
  vPt := CoPoint.Create;

  oMap := FMap_NIIR_1998_humidity;


   // -------------------------------------------------------------------
  // Q_factor
  // -------------------------------------------------------------------
  if oMap.Active then
  begin
    k:=oMap.VLayer.AllFeatures.Count;



      oMap.VLayer.Selection.SelectByPoint (aBLPoint.L, aBLPoint.B, miSelectionNew, EmptyParam);

      vSelection:=oMap.VLayer.Selection;
      iCount:=vSelection.Count;

      if iCount>0 then
        vFeature:=vSelection.Item[1];


    if iCount>0 then
    begin
      vRowValues:=oMap.VDataset.RowValues[vFeature];

      aRec.NIIR_1998.absolute_humidity:=
        AsFloat(mapx_RowValues_GetItemValue(vRowValues,'absolute_humidity'));
    end;
          
    Result:=iCount>0;
  end else
    Result:=False;

end;



// ---------------------------------------------------------------
function TLink_climate_file.Get_Q_factor(aBLPoint: TBLPoint; var aRec:
    TLinkClimateDataRec): Boolean;
// ---------------------------------------------------------------
var
  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vSelection: CMapXSelection;
  vRowValues: CMapXRowValues;
  vPt: CMapXPoint;
  iCount: integer;
  k: Integer;

  oMap : TmiMap;
  
begin
  vPt := CoPoint.Create;

  oMap := FMap_Q_factor;


   // -------------------------------------------------------------------
  // Q_factor
  // -------------------------------------------------------------------
  if oMap.Active then
  begin
    k:=oMap.VLayer.AllFeatures.Count;



      oMap.VLayer.Selection.SelectByPoint (aBLPoint.L, aBLPoint.B, miSelectionNew, EmptyParam);

      vSelection:=oMap.VLayer.Selection;
      iCount:=vSelection.Count;

      if iCount>0 then
        vFeature:=vSelection.Item[1];



    if iCount>0 then
    begin
      vRowValues:=oMap.VDataset.RowValues[vFeature];


//        with aRec do
//      begin
      //1,2,3
      // ��������� �� 1
      aRec.ITU.terrain_type:=AsInteger(mapx_RowValues_GetItemValue(vRowValues,'terrain_type'));
      if aRec.ITU.terrain_type>0 then
        Dec(aRec.ITU.terrain_type);


      aRec.ITU.Q_factor    :=AsFloat  (mapx_RowValues_GetItemValue(vRowValues,'Q_factor'));
   //     end;
    end;
          
    Result:=iCount>0;
  end else
    Result:=False;

end;

// ---------------------------------------------------------------
function TLink_climate_file.Get_NIIR_1998_rain_8_11(aBLPoint: TBLPoint; var
    aRec: TLinkClimateDataRec): Boolean;
// ---------------------------------------------------------------
var
 //
  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vSelection: CMapXSelection;
  vRowValues: CMapXRowValues;
  vPt: CMapXPoint;
  iCount: integer;
  k: Integer;

  oMap : TmiMap;

//  s: string;
begin
  vPt := CoPoint.Create;

  oMap := FMap_NIIR_1998_rain_8_11;

  // -------------------------------------------------------------------
  // ������ : string;
  // -------------------------------------------------------------------
 // s :=GetFileDir() + DEF_FileName_Climate;


  if oMap.Active then
  begin
    k:=oMap.VLayer.AllFeatures.Count;


      oMap.VLayer.Selection.SelectByPoint (aBLPoint.L, aBLPoint.B, miSelectionNew, EmptyParam);

      vSelection:=oMap.VLayer.Selection;
      iCount:=vSelection.Count;

      if iCount>0 then
        vFeature:=vSelection.Item[1];


    if iCount>0 then
    begin
      vRowValues:=oMap.VDataset.RowValues[vFeature];

      aRec.NIIR_1998.rain_8_11_intensity_region_number:=
         AsInteger(mapx_RowValues_GetItemValue(vRowValues,'region_number'));

    end;


    Result:=iCount>0;
  end else
    Result:=False;
end;


// ---------------------------------------------------------------
function TLink_climate_file.Get_NIIR_1998_rain_8_15(aBLPoint: TBLPoint; var
    aRec: TLinkClimateDataRec): Boolean;
// ---------------------------------------------------------------
var
 //
  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vSelection: CMapXSelection;
  vRowValues: CMapXRowValues;
  vPt: CMapXPoint;
  iCount: integer;
  k: Integer;

  oMap : TmiMap;

//  s: string;
begin
  vPt := CoPoint.Create;

  oMap := FMap_NIIR_1998_rain_8_15;

  // -------------------------------------------------------------------
  // ������ : string;
  // -------------------------------------------------------------------
 // s :=GetFileDir() + DEF_FileName_Climate;


  if oMap.Active then
  begin
    k:=oMap.VLayer.AllFeatures.Count;

    oMap.VLayer.Selection.SelectByPoint (aBLPoint.L, aBLPoint.B, miSelectionNew, EmptyParam);

    vSelection:=oMap.VLayer.Selection;
    iCount:=vSelection.Count;

    if iCount>0 then
      vFeature:=vSelection.Item[1];



   if iCount>0 then
    begin
      vRowValues:=oMap.VDataset.RowValues[vFeature];
                                      
      aRec.NIIR_1998.rain_8_15_Qd_region_number:=
         AsInteger(mapx_RowValues_GetItemValue(vRowValues,'region_number'));

    end;

    Result:=iCount>0;
  end else
    Result:=False;
end;


// ---------------------------------------------------------------
function TLink_climate_file.Get_GOST_rain(aBLPoint: TBLPoint; var aRec:
    TLinkClimateDataRec): Boolean;
// ---------------------------------------------------------------
var
 //
  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vSelection: CMapXSelection;
  vRowValues: CMapXRowValues;
  vPt: CMapXPoint;
  iCount: integer;

  oMap : TmiMap;

//  s: string;
begin
  vPt := CoPoint.Create;

  oMap := FMap_GOST_rain_intensity;

  // -------------------------------------------------------------------
  // ������ : string;
  // -------------------------------------------------------------------
 // s :=GetFileDir() + DEF_FileName_Climate;


  if oMap.Active then
  begin   
    oMap.VLayer.Selection.SelectByPoint (aBLPoint.L, aBLPoint.B, miSelectionNew, EmptyParam);

    vSelection:=oMap.VLayer.Selection;
    iCount:=vSelection.Count;

    if iCount>0 then
      vFeature:=vSelection.Item[1];


   if iCount>0 then
    begin
      vRowValues:=oMap.VDataset.RowValues[vFeature];


      aRec.GOST.rain_intensity:=
         AsFloat(mapx_RowValues_GetItemValue(vRowValues,'rain_intensity_coefficient'));

    end;

    Result:=iCount>0;
  end else
    Result:=False;
end;

{
function TMapItemList.AddItem: TMapItem;
begin
  Result := TMapItem.Create;
  Add(Result);
end;


// ---------------------------------------------------------------
function TMapItemList.FindData(aBLPoint: TBLPoint; var aRec:
    TLinkClimateDataRec): Boolean;
// ---------------------------------------------------------------
begin

end;


// ---------------------------------------------------------------
function TMapItemList.LoadFromTab(aFileName: string): Boolean;
// ---------------------------------------------------------------
var
  I: Integer;
 //
  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vSelection: CMapXSelection;
  vRowValues: CMapXRowValues;
  vPt: CMapXPoint;
  iCount: integer;

  oMap : TmiMap;

  rec: TLinkClimateDataRec;

  oMapItem: TMapItem;
  sShortFileName: string;

//  s: string;
begin
  vPt := CoPoint.Create;

  oMap := TmiMap.Create;

  sShortFileName:= ExtractFileName(aFileName);

  // -------------------------------------------------------------------
  // ������ : string;
  // -------------------------------------------------------------------
 // s :=GetFileDir() + DEF_FileName_Climate;


  if oMap.OpenFile (aFileName) then
//  if oMap.Active then
  begin
    vFeatures := oMap.VLayer.AllFeatures;

    for I := 1 to vFeatures.Count  do
    begin
      vFeature := vFeatures[i];

      oMapItem:=AddItem;

      vRowValues:=oMap.VDataset.RowValues[vFeature];

      
      if Eq(sShortFileName, DEF_FileName_Climate) then
      begin

      end;
}
(*

  DEF_FileName_Climate          = '������.tab';
  DEF_FileName_Q_factor         = 'Q������.tab';
  DEF_FileName_RadioKlimRegion  = 'RadioKlimRegion.tab';
  DEF_FileName_ClimateFactor    = '������������.tab';
  DEF_FileName_Rain             = 'ESARAIN_R001.PRN';

//  DEF_FileName_humidity        ='NIIR_humidity.tab';
 // DEF_FileName_radioklimat     ='NIIR_gradient.TAB';
  DEF_FileName_rain_8_11       ='NIIR_rain_8_11.tab';
  DEF_FileName_rain_8_15       ='NIIR_rain_8_15.tab';
  DEF_FileName_humidity        ='NIIR_humidity.tab';
*)
{

      rec.GOST.rain_intensity:=
         AsFloat(mapx_RowValues_GetItemValue(vRowValues,'rain_intensity_coefficient'));


      oMapItem.Data := rec;


    end;
  end;

  FreeAndNil(oMap);

end;
}
{
function TRainIntense.FindData(aBLPoint: TBLPoint; var aValue: double): Boolean;
begin
 // Result := ;
end;
}





end.




{


// ---------------------------------------------------------------
function TRainIntense.LoadFromFile(aFileName: string): Boolean;
// ---------------------------------------------------------------
//var
//  FData : array[0..120-1] of
//          array[0..240-1] of Double;

var
  i,j: Integer;
  i_lat,j_lon: integer;
  buf, d1, d2: double;
   FTxtFile_Rain: TextFile;
begin
  AssignFile(FTxtFile_Rain, aFileName);
  Reset(FTxtFile_Rain);

//  i_lat:=round((90-aPoint.B)/1.5);
//  j_lon:=round(aPoint.L/1.5);

  {
  AssignFile(FTxtFile_Rain, GetFileDir() + DEF_FileName_Rain);
  Reset(FTxtFile_Rain);
  }

  for i_lat:=0 to 120-1 Do
  begin
    for j_lon:=0 to 240-1 Do
      read (FTxtFile_Rain, FData[i_lat,j_lon]);

    Readln(FTxtFile_Rain);
  end;

//   and (j_lon in [0..240]) then

//
//  if (i_lat in [0..120]) and (j_lon in [0..240]) then
//  begin
//    if i_lat>0 then for i:=0 to i_lat-1 do Readln(FTxtFile_Rain, d1);
//    if j_lon>0 then for j:=0 to j_lon-1 do Read  (FTxtFile_Rain, d2);
//
//    buf:= 0;
//    Read(FTxtFile_Rain, buf);
//    Result:=buf;
//  end
//    else Result:=0;

  CloseFile(FTxtFile_Rain);

  //      CloseFile(FTxtFile_Rain);
end;






}
