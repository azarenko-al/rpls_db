unit Climate_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 18.08.2016 12:04:26 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\RPLS_DB\src\DLL_Link_climate_XE\master\Project16 (1)
// LIBID: {1AFA4C76-4184-4E43-A27E-40C95D93CAD4}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\Windows\system32\stdvcl40.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Winapi.Windows, System.Classes, System.Variants, System.Win.StdVCL, Vcl.Graphics, Vcl.OleServer, Winapi.ActiveX;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ClimateMajorVersion = 1;
  ClimateMinorVersion = 0;

  LIBID_Climate: TGUID = '{1AFA4C76-4184-4E43-A27E-40C95D93CAD4}';

  IID_IClimateX: TGUID = '{E35A84F4-A4A2-4347-8017-FB2937ED3F78}';
  CLASS_ClimateX: TGUID = '{F0B5E5A1-3BBA-48CA-89DD-31E78902961E}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IClimateX = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  ClimateX = IClimateX;


// *********************************************************************//
// Interface: IClimateX
// Flags:     (256) OleAutomation
// GUID:      {E35A84F4-A4A2-4347-8017-FB2937ED3F78}
// *********************************************************************//
  IClimateX = interface(IUnknown)
    ['{E35A84F4-A4A2-4347-8017-FB2937ED3F78}']
    function Find(aLat: Double; aLon: Double; aData: Integer): HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoClimateX provides a Create and CreateRemote method to          
// create instances of the default interface IClimateX exposed by              
// the CoClass ClimateX. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoClimateX = class
    class function Create: IClimateX;
    class function CreateRemote(const MachineName: string): IClimateX;
  end;

implementation

uses System.Win.ComObj;

class function CoClimateX.Create: IClimateX;
begin
  Result := CreateComObject(CLASS_ClimateX) as IClimateX;
end;

class function CoClimateX.CreateRemote(const MachineName: string): IClimateX;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_ClimateX) as IClimateX;
end;

end.

