unit dm_Act_Cell_Layer;

interface

uses
  Classes, Forms,
   Menus, 

   dm_User_Security,
   u_const_db,
   u_const_str,
   u_func,

  I_Object,

  dm_act_Base,

  dm_Cell_Layer,
  d_Cell_Layer_add,

  i_Audit,
//  u_func_msg,
  

  
  
  
  u_types, ActnList
  ;

type
  TdmAct_Cell_Layer = class(TdmAct_Base)
    ActionList2: TActionList;
    act_Audit: TAction;
    procedure DataModuleCreate(Sender: TObject);
  private
    function CheckActionsEnable: Boolean;

  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    procedure DoAction (Sender: TObject);

    function ItemDel(aID: integer; aName: string): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;

  public

    class procedure Init;

  end;

var
  dmAct_Cell_Layer: TdmAct_Cell_Layer;

//==================================================================
implementation

uses dm_Main; {$R *.dfm}
//==================================================================

//--------------------------------------------------------------------
class procedure TdmAct_Cell_Layer.Init;
//--------------------------------------------------------------------
begin
  Assert(not Assigned(dmAct_Cell_Layer));

  dmAct_Cell_Layer:=TdmAct_Cell_Layer.Create(Application);
end;


//--------------------------------------------------------------------
procedure TdmAct_Cell_Layer.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
                
  ObjectName:=OBJ_CELL_LAYER;

  act_Audit.Caption:=STR_Audit;



  SetActionsExecuteProc ([

           act_Audit

         //  act_Copy

         ], DoAction);
//  dlg_SetAllActionsExecuteProc ([act_Add,act_Del], DoAction);

end;

//--------------------------------------------------------------------
procedure TdmAct_Cell_Layer.DoAction (Sender: TObject);
//--------------------------------------------------------------------
begin
  if Sender=act_Audit then
    Init_IAuditX.ExecDlg( dmMain.GetConnectionString, TBL_Cell_Layer, FFocusedID) else

end;

//--------------------------------------------------------------------
function TdmAct_Cell_Layer.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_Cell_Layer_add.ExecDlg (aFolderID);
end;

//--------------------------------------------------------------------
function TdmAct_Cell_Layer.ItemDel(aID: integer; aName: string): boolean;
//--------------------------------------------------------------------
begin
  Result:=dmCellLayer.Del (aID);
end;


// ---------------------------------------------------------------
function TdmAct_Cell_Layer.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
  Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];

  SetActionsEnabled1( [
          act_Del_,
          act_Del_list
      ],

   Result );

   //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );


end;


//--------------------------------------------------------------------
procedure TdmAct_Cell_Layer.GetPopupMenu;
//--------------------------------------------------------------------
var
  bEnable: Boolean;
begin
  bEnable:=CheckActionsEnable();

  case aMenuType of
    mtFolder:  begin
                 AddFolderMenu_Create (aPopupMenu, bEnable);
                 AddFolderPopupMenu(aPopupMenu, bEnable);
               end;
    mtItem:    begin
                 AddMenuItem (aPopupMenu, act_Del_);
                  AddMenuItem (aPopupMenu, nil);
                 AddFolderMenu_Tools (aPopupMenu);

                 AddMenuItem (aPopupMenu, act_Audit);
               end;
    mtList: begin
              AddMenuItem (aPopupMenu, act_Del_list);
                AddMenuItem (aPopupMenu, nil);
              AddFolderMenu_Tools (aPopupMenu);
            end;
  end;

end;


begin

end.
