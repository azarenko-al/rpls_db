unit a_Unit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,

  fr_Browser,

  fr_Explorer,
  dm_Explorer,

  dm_Main,
  dm_Main_res,

  dm_Act_Cell_Layer,
  dm_Act_View_Eng,

  u_func_app,
  u_db,

  u_const,
  u_const_msg,
  u_const_db,
  u_dlg,
  u_Types,

  f_Explorer,

  ComCtrls, rxPlacemnt, ToolWin, ExtCtrls;

type
  Tfrm_test_TrxType = class(TForm)
    Panel1: TPanel;
    Splitter1: TSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    pn_Browser: TPanel;
    ToolBar1: TToolBar;
    Add: TButton;
    View: TButton;
    Import_: TButton;
    Button1: TButton;
    FormStorage1: TFormStorage;
    procedure ViewClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Fframe_Explorer : Tframe_Explorer;

    Fframe_Browser : Tframe_Browser;
  public
    { Public declarations }
  end;

var
  frm_test_TrxType: Tfrm_test_TrxType;

//===================================================
implementation {$R *.DFM}
//===================================================

var
  id: integer;


//------------------------------------------------------------------
procedure Tfrm_test_TrxType.ViewClick(Sender: TObject);
//------------------------------------------------------------------
begin
  id:=gl_DB.GetMaxID (TBL_TRX_TYPE); //, [EMPTY_PARAM] );

end;

//------------------------------------------------------------------
procedure Tfrm_test_TrxType.FormCreate(Sender: TObject);
//------------------------------------------------------------------
begin

  Fframe_Explorer := Tframe_Explorer.CreateChildForm ( TabSheet1);
  Fframe_Explorer.SetViewObjects([otCellLayer]);
  Fframe_Explorer.RegPath:='cell layer';
  Fframe_Explorer.Load;

  Fframe_Browser := Tframe_Browser.CreateChildForm( pn_Browser);
  Fframe_Explorer.OnNodeFocused:=Fframe_Browser.ViewObjectByPIDL;

end;


end.
