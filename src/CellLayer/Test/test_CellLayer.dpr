program test_CellLayer;

uses
  Forms,
  a_Unit in 'a_Unit.pas' {frm_test_TrxType},
  dm_Act_Cell_Layer in '..\dm_Act_Cell_Layer.pas' {dmAct_Cell_Layer: TDataModule},
  dm_Cell_Layer in '..\dm_Cell_Layer.pas' {dmCellLayer: TDataModule},
  dm_Main in '..\..\DataModules\dm_Main.pas' {dmMain: TDataModule},
  d_Cell_Layer_add in '..\d_Cell_Layer_add.pas' {dlg_Cell_Layer_add};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(Tfrm_test_TrxType, frm_test_TrxType);
  Application.Run;
end.
