unit d_Cell_Layer_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   cxPropertiesStore, rxPlacemnt, ActnList,    StdCtrls, ExtCtrls,

  d_Wizard_add_with_Inspector,

  dm_Main,

  u_const_str,
  u_const_db,
 // u_const_msg,

  u_func,
  u_reg,
 // u_func_msg,

  dm_Cell_Layer, ComCtrls

  ;

type
  Tdlg_Cell_Layer_add = class(Tdlg_Wizard_add_with_Inspector)
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
  private
    FID,FFolderID: integer;
    procedure Append;
  public
    class function ExecDlg (aFolderID: integer =0): integer;
  end;


//====================================================================
implementation {$R *.DFM}
//====================================================================

//--------------------------------------------------------------------
class function Tdlg_Cell_Layer_add.ExecDlg (aFolderID: integer =0): integer;
//--------------------------------------------------------------------
begin
  with Tdlg_Cell_Layer_add.Create(Application) do
  begin
    FFolderID:=aFolderID;                         
    ed_Name_.Text:=dmCellLayer.GetNewName;

    ShowModal;
    Result:=FID;

    Free;
  end;
end;

//--------------------------------------------------------------------
procedure Tdlg_Cell_Layer_add.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  SetActionName(STR_DLG_ADD_CELL_LAYER_TYPE);

  lb_Name.Caption:=STR_NAME;

{
  row_Freq_Min.Caption:='';
  row_Freq_Max.Caption:='';
 }
//  row_Sense.Caption:='���������������� [dBm]';
//  row_Power.Caption:='�������� [��]';

//  FAntTypeID:=RegIni.ReadInteger(Name, FLD_ANTENNA_TYPE_ID, 0);
//  edr_AntType.Text:=dmMain.GetNameByID(TBL_ANTENNA_TYPE, FAntTypeID);
end;


//-------------------------------------------------------------------
procedure Tdlg_Cell_Layer_add.Append;
//-------------------------------------------------------------------
var rec: TdmCellLayerAddRec;
begin
  inherited;

  FillChar(rec, SizeOf(rec), 0);

  rec.Name:=ed_Name_.Text;
  rec.FolderID:=FFolderID;
 // rec.Channels:=row_Channels.Text;

  FID:=dmCellLayer.Add (rec);
end;


procedure Tdlg_Cell_Layer_add.act_OkExecute(Sender: TObject);
begin
  inherited;
  Append();
end;


end.