unit dm_Cell_Layer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,Variants, Db, ADODB,

//  I_CalcModel,
  dm_Main,

  dm_Object_base,

  u_types,

  u_func,
  u_db,

  u_const_str,
  u_const_db
  ;

type
  //-------------------------------------------------------------------
  TdmCellLayerAddRec = record
  //-------------------------------------------------------------------
    Name: string;
    FolderID: integer;
  end;


  TdmCellLayer = class(TdmObject_base)
    procedure DataModuleCreate(Sender: TObject);

  public
    function Add (aRec: TdmCellLayerAddRec): integer;

  end;

  
function dmCellLayer: TdmCellLayer;


//===================================================================
implementation {$R *.dfm}
//===================================================================


var
  FdmCellLayer: TdmCellLayer;


// ---------------------------------------------------------------
function dmCellLayer: TdmCellLayer;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmCellLayer) then
      FdmCellLayer := TdmCellLayer.Create(Application);

  Result := FdmCellLayer;
end;


// ---------------------------------------------------------------
procedure TdmCellLayer.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;
  TableName:=TBL_CELLLAYER;
  ObjectName:=OBJ_CEll_LAYER;
end;

//--------------------------------------------------------------------
function TdmCellLayer.Add (aRec: TdmCellLayerAddRec): integer;
//--------------------------------------------------------------------
begin
  with aRec do
    Result:=gl_DB.AddRecordID (TableName,

                  [db_Par(FLD_NAME, Name),
                   db_Par(FLD_FOLDER_ID, IIF(FolderID>0,FolderID,NULL))

                  ] );

end;


begin
   
end.

