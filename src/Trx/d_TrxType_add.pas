unit d_TrxType_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   rxPlacemnt, ActnList,     Db, ADODB,
  StdCtrls, ExtCtrls,Registry, cxDropDownEdit,  
  cxPropertiesStore, cxVGrid, cxControls, cxInplaceContainer,

  d_Wizard_add_with_Inspector,

  u_const_str,
  u_const_db,

  u_func,
  u_db,

  u_cx_vgrid,

  u_func_msg,

  dm_Main,

  dm_TrxType, ComCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxEdit, cxTextEdit, dxSkinsCore, dxSkinsDefaultPainters;


type
  Tdlg_TrxType_add = class(Tdlg_Wizard_add_with_Inspector)
    qry_Temp: TADOQuery;
    cxVerticalGrid1: TcxVerticalGrid;
    row_NetStandard: TcxEditorRow;
    row_Power_w: TcxEditorRow;
    row_Sensitivity_dBm: TcxEditorRow;
    row_Freq_MHz: TcxEditorRow;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);

  private
    FID,FFolderID: integer;
    FSensitivity_dBm: string;

    procedure Append;
   
  public
    class function ExecDlg (aFolderID: integer =0): integer;
  end;


//====================================================================
implementation

uses dm_Onega_db_data;
{$R *.DFM}
//====================================================================

//--------------------------------------------------------------------
class function Tdlg_TrxType_add.ExecDlg (aFolderID: integer =0): integer;
//--------------------------------------------------------------------
begin
  with Tdlg_TrxType_add.Create(Application) do
  begin                                   
    FFolderID:= aFolderID;
    ed_Name_.Text:= dmTrxType.GetNewName;

    Result:= IIF(ShowModal=mrOk, FID, 0);

    Free;
  end;
end;

//--------------------------------------------------------------------
procedure Tdlg_TrxType_add.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
  SetActionName (STR_DLG_ADD_LINKEND_TYPE);
  lb_Name.Caption:=STR_NAME;

  row_Power_W.Properties.Caption:= STR_POWER_W;
  row_Freq_MHz.Properties.Caption := STR_FREQ_MHz;
  row_Sensitivity_dBm.Properties.Caption := STR_Sensitivity_dBm;



  AddComponentProp(row_Power_w, 'Text');
  AddComponentProp(row_Freq_MHz, 'Text');
  RestoreFrom();

  with TRegistryIniFile.Create(FRegPath) do

  begin
 //   CurrentRegPath := FLocalRegPath;

    row_Power_w.Properties.Value := ReadString('', FLD_POWER,   '20');
    row_Freq_MHz.Properties.Value  := ReadString('', FLD_FREQ,      '');

    FSensitivity_dBm         := ReadString('', FLD_Sensitivity_dBm,     '');
//    FNoise         := ReadString('', FLD_NOISE,     '');

    Free;
  end;

//  row_Sense_Noise.Text:= FSense; //!!!
//  row_Sense_Noise.Caption:= STR_SENSITIVITY_dBm;

  // ������ ��������� ���������� ����
  qry_Temp.Connection:= dmMain.ADOConnection;

  dmOnega_DB_data.OpenQuery (qry_Temp, 'SELECT id,Name FROM ' + TBL_CELLLAYER);



  //  row_Band.Properties.EditProperties).Items.Assign (dmLIbrary.Bands);


  db_CopyRecordsToStringList(qry_Temp,
    TcxComboBoxProperties(row_NetStandard.Properties.EditProperties).Items);


  SetDefaultSize();

  cx_InitVerticalGrid (cxVerticalGrid1);

end;


//-------------------------------------------------------------------
procedure Tdlg_TrxType_add.FormDestroy(Sender: TObject);
//-------------------------------------------------------------------
begin
  with TRegistryIniFile.Create(FRegPath) do
//  with gl_Reg do
  begin
  //  CurrentRegPath := FLocalRegPath;
    WriteString('', FLD_POWER,  row_Power_w.Properties.Value);
    WriteString('', FLD_FREQ,   row_Freq_MHz.Properties.Value);

    WriteString('', FLD_Sensitivity_dBm,  FSensitivity_dBm);
  //  WriteString('', FLD_NOISE,  FNoise);
  end;
  inherited;


end;


//-------------------------------------------------------------------
procedure Tdlg_TrxType_add.Append;
//-------------------------------------------------------------------
var rec: TdmTrxTypeAddRec;
begin
  inherited;

  with rec do
  begin
    NewName       :=ed_Name_.Text;
    FolderID      :=FFolderID;

//      TcxComboBoxProperties(row_NetStandard.Properties.EditProperties).Items);


//    NetStandard_ID:= gl_DB.GetIDByName(TBL_CELLLAYER, row_NetStandard.Text);

//row_Freq_MHz.Properties.

    Power_w       :=AsFloat(row_Power_w.Properties.Value);
    Freq_MHz      :=AsFloat(row_Freq_MHz.Properties.Value);

    Sensitivity_dBm :=AsFloat(row_Sensitivity_dBm.Properties.Value);
   // IIF(row_Sense_Noise.Caption=STR_SENSE_dBm, AsFloat(FSense), 0);


 //   TrxNoise      := IIF(row_Sense_Noise.Caption=STR_TRXNOISE,  AsFloat(FNoise), 0);
  //  Sense_dBm     := IIF(row_Sense_Noise.Caption=STR_SENSE_dBm, AsFloat(FSense), 0);
  end;

  FID:=dmTrxType.Add (rec);
end;

//-------------------------------------------------------------------
procedure Tdlg_TrxType_add.act_OkExecute(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;
  Append();
end;





end.