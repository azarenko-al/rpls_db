program test_TrxType;

uses
  Forms,
  a_Unit in 'a_Unit.pas' {frm_test_TrxType},
  dm_act_TrxType in '..\dm_act_TrxType.pas' {dmAct_TrxType: TDataModule},
  dm_TrxType in '..\dm_TrxType.pas' {dmTrxType: TDataModule},
  dm_Main in '..\..\DataModules\dm_Main.pas' {dmMain: TDataModule},
  d_Wizard_Add_with_params in '..\..\..\..\Common\d_Wizard_Add_with_params.pas' {dlg_Wizard_add_with_params},
  d_TrxType_add in '..\d_TrxType_add.pas' {dlg_TrxType_add};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(Tfrm_test_TrxType, frm_test_TrxType);
  Application.Run;
end.
