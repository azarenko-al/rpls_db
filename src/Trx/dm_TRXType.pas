unit dm_TrxType;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms, Windows, Variants,

  dm_Onega_DB_data,

  u_func,
  u_db,

  u_types,
  u_const_str,
  u_const_db,
  //u_const_db_3g,

  dm_Main,
  dm_Object_base
  ;


type
  TdmTrxTypeAddRec = record

    NewName        : string;
    FolderID      : integer;

    NetStandard_ID : integer;

    Sensitivity_dBm: double;      // ���������������� (����, 9.6 ����/���, 0 ��/�) [���]
    Freq_MHz       : double;      // ������� ������� (��� ����� ������� ���)
    Power_W        : double;


  end;


  TdmTrxType = class(TdmObject_base)
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    function Add (aRec: TdmTrxTypeAddRec): integer;
    function Get(aID: integer; var aRec: TdmTrxTypeAddRec): boolean;
  end;

function dmTrxType: TdmTrxType;

//====================================================================
implementation    {$R *.DFM}
//====================================================================

var
  FdmTrxType: TdmTrxType;


// ------------------------------------------------------------------
function dmTrxType: TdmTrxType;
// ------------------------------------------------------------------
begin
  if not Assigned(FdmTrxType) then
      FdmTrxType := TdmTrxType.Create(Application);

   Result := FdmTrxType;
end;


procedure TdmTrxType.DataModuleCreate(Sender: TObject);
begin
  inherited;
  TableName  := TBL_TRXTYPE;
  ObjectName := OBJ_TRX_TYPE;
end;


//--------------------------------------------------------------------
function TdmTrxType.Add (aRec: TdmTrxTypeAddRec): integer;
//--------------------------------------------------------------------
begin
  with aRec do
    Result:=dmOnega_DB_data.ExecStoredProc(SP_TrxType_ADD,

                  [
                   db_Par(FLD_NAME,            NewName),
                //   db_Par(FLD_FOLDER_ID,       IIF_NULL(FolderID)),

                   db_Par(FLD_Sensitivity_dBm, Sensitivity_dBm),
                   db_Par(FLD_POWER_W,         Power_w),
                   db_Par(FLD_FREQ_MHZ,        Freq_MHz),

           
                   db_Par(FLD_NETSTANDARD_ID,  IIF_NULL(NetStandard_ID))
                   ] );
end;


//--------------------------------------------------------------------
function TdmTrxType.Get(aID: integer; var aRec: TdmTrxTypeAddRec): boolean;
//--------------------------------------------------------------------
const
  FLD_POWER       = 'POWER';
  FLD_Sensitivity = 'Sensitivity';
  FLD_FREQ        = 'FREQ';
begin
  FillChar (aRec, SizeOf(aRec), 0);

  db_OpenTableByID (qry_Temp, TBL_TRXtype, aID);
  Result:=not qry_Temp.IsEmpty;

  if Result then
    with qry_Temp do
  begin
 //   aRec.FolderID      :=FieldBYName(FLD_FOLDER_ID).AsInteger;
    aRec.NetStandard_ID:=FieldByName(FLD_NETSTANDARD_ID).AsInteger;
    aRec.NewName        :=FieldValues[FLD_NAME];

    // OLD !!!!!!
    aRec.Power_w         :=FieldBYName(FLD_POWER).AsFloat;
    aRec.Sensitivity_dBm :=FieldBYName(FLD_Sensitivity).AsInteger;
    aRec.Freq_MHz        :=FieldBYName(FLD_FREQ).AsFloat;

  end;
end;


begin
end.