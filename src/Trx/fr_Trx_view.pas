unit fr_Trx_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  cxControls, cxSplitter, Menus, AppEvnts, ImgList, ActnList,
   ExtCtrls,

  fr_DBInspector,
  fr_View_base,
  u_reg,
  u_const_db,
  u_db,
  u_types,

  cxPropertiesStore, dxBar, cxBarEditItem, cxClasses,
  cxLookAndFeels;

type
  Tframe_Trx_View = class(Tframe_View_Base)
    pn_Inspector: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    Fframe_Inspector: Tframe_DBInspector;
  public
    procedure View (aID: integer); override;
  end;

//==============================================================================
implementation  {$R *.DFM}
//==============================================================================


//------------------------------------------------------------------------------
procedure Tframe_Trx_View.FormCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;
  ObjectName:= OBJ_TRX_TYPE;
  pn_Inspector.Align:= alClient;

  Fframe_Inspector:= Tframe_DBInspector.CreateChildForm( pn_Inspector, '');
end;


//------------------------------------------------------------------------------
procedure Tframe_Trx_View.FormDestroy(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;
  Fframe_Inspector.Free;
end;


//------------------------------------------------------------------------------
procedure Tframe_Trx_View.View(aID: integer);
//------------------------------------------------------------------------------
var
  bReadOnly: Boolean;
  sNetType, sObjectType: string;

begin
  inherited;
  sNetType:= gl_DB.GetStringFieldValueByID(VIEW_TRX_TYPE, FLD_NET_TYPE, aID);
  sObjectType:= IIF(sNetType=TYPE_3G, OBJ_TRX_TYPE_3G, OBJ_TRX_TYPE);


  Fframe_Inspector.PrepareView(sObjectType);
  Fframe_Inspector.View (aID);

  bReadOnly:=dmUser_Security.Lib_Edit_ReadOnly(TableName, aID);


  Fframe_Inspector.SetReadOnly(bReadOnly);


end;


end.
