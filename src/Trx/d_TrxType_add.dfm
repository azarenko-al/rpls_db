inherited dlg_TrxType_add: Tdlg_TrxType_add
  Left = 1173
  Top = 348
  Height = 477
  Caption = 'dlg_TrxType_add'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 403
    inherited Panel3: TPanel
      Left = 368
      Width = 184
    end
  end
  inherited pn_Top_: TPanel
    inherited pn_Header: TPanel
      Height = 55
    end
  end
  inherited Panel2: TPanel
    Height = 262
    inherited PageControl1: TPageControl
      Height = 252
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 534
          Height = 221
          Align = alClient
          LookAndFeel.Kind = lfStandard
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 251
          OptionsView.ValueWidth = 40
          TabOrder = 0
          Version = 1
          object row_NetStandard: TcxEditorRow
            Expanded = False
            Properties.Caption = #1057#1090#1072#1085#1076#1072#1088#1090' '#1089#1077#1090#1080
            Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.DropDownListStyle = lsEditFixedList
            Properties.EditProperties.DropDownRows = 20
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.EditProperties.Revertable = True
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object row_Power_w: TcxEditorRow
            Expanded = False
            Properties.Caption = 'Power W'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
          object row_Sensitivity_dBm: TcxEditorRow
            Expanded = False
            Properties.Caption = 'Sensitivity_dBm'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 2
            ParentID = -1
            Index = 2
            Version = 1
          end
          object row_Freq_MHz: TcxEditorRow
            Expanded = False
            Properties.Caption = 'Freq MHz'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 3
            ParentID = -1
            Index = 3
            Version = 1
          end
        end
      end
    end
  end
  object qry_Temp: TADOQuery
    Parameters = <>
    Left = 85
    Top = 5
  end
end
