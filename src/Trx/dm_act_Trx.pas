unit dm_act_Trx;

interface

uses
  Classes, Forms,  Menus,

  dm_User_Security,

  i_Audit,


  u_func,

  I_Object,

  dm_act_Base,

  dm_TrxType,
  d_TrxType_add,

   u_const_db,
  
  u_const_str,
  

  u_types, ActnList;


type
  TdmAct_Trx = class(TdmAct_Base)
    ActionList2: TActionList;
    act_Audit: TAction;
    procedure DataModuleCreate(Sender: TObject);
  private
    function CheckActionsEnable: Boolean;
    procedure DoAction(Sender: TObject);

  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
 //   procedure DoAction (Sender: TObject);

    function ItemDel(aID: integer; aName: string = ''): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;

  public
    class procedure Init;

  end;

var
  dmAct_Trx: TdmAct_Trx;

//====================================================================
implementation

uses dm_Main; {$R *.dfm}
//====================================================================

//--------------------------------------------------------------------
class procedure TdmAct_Trx.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_Trx) then
    dmAct_Trx:=TdmAct_Trx.Create(Application);
end;


//--------------------------------------------------------------------
procedure TdmAct_Trx.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_TRX_TYPE;

  act_Audit.Caption:=STR_Audit;


  // dlg_SetAllActionsExecuteProc ([act_Add,act_Del], DoAction);
end;

//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------



//--------------------------------------------------------------------
function TdmAct_Trx.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_TrxType_add.ExecDlg (aFolderID);
end;


//--------------------------------------------------------------------
function TdmAct_Trx.ItemDel(aID: integer; aName: string = ''): boolean;
//--------------------------------------------------------------------
begin
  Result:=dmTrxType.Del (aID);
end;


procedure TdmAct_Trx.DoAction(Sender: TObject);
begin

  if Sender=act_Audit then
     Init_IAuditX.ExecDlg( dmMain.GetConnectionString, TBL_TRXTYPE, FFocusedID) else


end;


// ---------------------------------------------------------------
function TdmAct_Trx.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
  Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];

  SetActionsEnabled1( [
          act_Del_,
          act_Del_list

      ],

   Result );


  //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );


end;



//--------------------------------------------------------------------
procedure TdmAct_Trx.GetPopupMenu;
//--------------------------------------------------------------------
var
  bEnable: Boolean;
begin
  bEnable:=CheckActionsEnable();


  case aMenuType of
    mtFolder: begin
                AddFolderMenu_Create (aPopupMenu, bEnable);
                AddFolderPopupMenu(aPopupMenu, bEnable);
              end;

    mtItem:   begin
                AddMenuItem (aPopupMenu, act_Del_);
                AddMenuItem (aPopupMenu, nil);

                AddFolderMenu_Tools (aPopupMenu);

                AddMenuItem (aPopupMenu, act_Audit);

              end;
    mtList:   begin
                AddMenuItem (aPopupMenu, act_Del_list);
                AddFolderMenu_Tools (aPopupMenu);
              end;
  end;

end;


begin
 
end.


{


//--------------------------------------------------------------------
procedure TdmAct_Trx.DoAction (Sender: TObject);
//--------------------------------------------------------------------
begin
//  if Sender=act_Add    then Add (FFocusedID) else
//  if Sender=act_Del    then Del (FFocusedID) else

end;
