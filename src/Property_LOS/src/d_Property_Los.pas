unit d_Property_Los;

interface

uses
  d_Wizard,
  u_db,
  u_db_,
  u_cx,

  u_rel_Profile,

  dm_Onega_DB_data,

  fr_Profile_Rel_Graph,

 // I_Shell,
  dm_Main,
  u_ini_Property_LOS_params,

 // d_DB_select,
  dm_Property,
  dm_Property_LOS,
 // dm_Act_Explorer,

  d_DB_select,

  u_Geo,
  u_func,
  u_files,

  u_cx_VGrid_export,
  u_cx_VGrid,

  u_const_db,

  dm_ClutterModel,

  Dialogs, Forms, SysUtils,
  Classes, Controls, ActnList, Registry,  cxVGrid, cxButtonEdit,
  ComCtrls, cxControls, cxInplaceContainer, cxPropertiesStore, rxPlacemnt,
  StdCtrls, ExtCtrls, cxLookAndFeels, dxSkinsCore, dxSkinsDefaultPainters,
  cxGraphics, cxLookAndFeelPainters, cxStyles, cxEdit, cxTextEdit,
  cxCheckBox, cxSpinEdit, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, DB, cxDBData, ADODB, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, Grids, DBGrids, dxmdaset, StdActns, Menus;

type


  Tdlg_Property_Los = class(Tdlg_Wizard)
    act_Calc: TAction;
    PageControl1: TPageControl;
    TabSheet_Params: TTabSheet;
    cxVerticalGrid1: TcxVerticalGrid;
    row_name: TcxEditorRow;
    row_ClutterModel: TcxEditorRow;
    row_Max_distance_km: TcxEditorRow;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    col_name_: TcxGridDBColumn;
    cxGrid1DBTableView1lat: TcxGridDBColumn;
    cxGrid1DBTableView1lon: TcxGridDBColumn;
    col_dist_km: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    ADOConnection1: TADOConnection;
    sp_Calc: TADOStoredProc;
    DataSource1: TDataSource;
    row_Height: TcxEditorRow;
    col_ground_height: TcxGridDBColumn;
    col_height_max: TcxGridDBColumn;
    cxGrid1DBTableView1src_lat: TcxGridDBColumn;
    cxGrid1DBTableView1src_lon: TcxGridDBColumn;
    col_src_ground_height: TcxGridDBColumn;
    col_src_height_max: TcxGridDBColumn;
    col_prosvet_m: TcxGridDBColumn;
    Button1: TButton;
    Button2: TButton;
    q_Prop: TADOQuery;
    row_Prop_antenna_H: TcxEditorRow;
    row_Prop_ground_H: TcxEditorRow;
    col_min_H: TcxGridDBColumn;
    row_Prop_Antenna_height_for_calc: TcxEditorRow;
    act_Excel_export: TAction;
    Button3: TButton;
    SaveDialog_Excel: TSaveDialog;
    col_property_name_src: TcxGridDBColumn;
    col_SRC_CALC_HEIGHT: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    Excel1: TMenuItem;
    N1: TMenuItem;
    act_view_XML: TAction;
    act_view_profile: TAction;
    actviewprofile1: TMenuItem;


    procedure Action1Execute(Sender: TObject);

   procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
//    procedure DoOnButtonClick(Sender: TObject; AbsoluteIndex: Integer);
//    procedure act_OkExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_CalcExecute(Sender: TObject);
    procedure act_Excel_exportExecute(Sender: TObject);
    procedure act_view_profileExecute(Sender: TObject);
    procedure act_view_XMLExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure row_ClutterModelEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);

  private
//    Fframe_lOS_cells: Tframe_LosRegion_cells;
//    Fframe_LosM_view: Tframe_LosM_view;

    FClutterModelID,
    FRadius_KM: Integer;
    FID: integer;

    procedure Calc;
//    procedure OpenData;
    procedure OpenData_calc;
    procedure OpenData_Prop(aID: Integer);
    procedure Profile_Image;
    procedure TestCalc;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  public

    class function ExecDlg: Boolean;

  end;


//==================================================================
implementation {$R *.DFM}
//==================================================================


//--------------------------------------------------------------------
class function Tdlg_Property_Los.ExecDlg: Boolean;
//--------------------------------------------------------------------
begin
  with Tdlg_Property_Los.Create(Application) do
  try
    OpenData_Prop ( g_Ini_Property_LOS_Params.Property_ID );

  //  Fframe_lOS_cells.ViewBYPoints (aBLPointArr);

    Result:= (ShowModal=mrOk);

  finally
    Free;
  end;
end;

//--------------------------------------------------------------------
procedure Tdlg_Property_Los.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  if ADOConnection1.Connected then
    showMessage ('ADOConnection1.Connected');

  inherited;

  TdmProperty_LOS.Init;

 // Caption:='������';

  Caption :='������ LOS �� �������� | '+ GetAppVersionStr();

// SetActionName ('������ LOS �� ��������');

 // PageControl1.Align:=alClient;
//  PageControl1.Visible := True;

  cxGrid1.Align:=alClient;

  btn_Ok.Action:=act_Calc;

  col_property_name_src.Caption :='�������� (���)';
  col_SRC_CALC_HEIGHT.Caption   :='������ ��������� (���), �';

  col_Name_.Caption             :='��������';
  col_dist_km.Caption           :='����������, km';
  col_ground_height.Caption     :='������ �����, �';
  col_prosvet_m.Caption         :='�������, �';

  {
  col_height_max: TcxGridDBColumn;
  cxGrid1DBTableView1src_lat: TcxGridDBColumn;
  cxGrid1DBTableView1src_lon: TcxGridDBColumn;
  col_src_ground_height: TcxGridDBColumn;
  col_src_height_max: TcxGridDBColumn;
  col_prosvet_m: TcxGridDBColumn;
  }

//  TabSheet_Params.Caption :='���������';
//  TabSheet_Cells.Caption  :='��������';

//  CreateChildForm(Tframe_LosRegion_cells, Fframe_lOS_cells, TabSheet_cells);
//  CreateChildForm(Tframe_LosM_view,       Fframe_LosM_view, TabSheet_Results);


//  cx_VerticalGrid_SaveToReg(cx aRegPath: string);

//  cx_VerticalGrid_LoadFromReg(cxVerticalGrid1, FRegPath);


//  AddComponentProp(PageControl1, 'ActivePage');
//  RestoreFrom();

//  AddComponentProp(row_Step, 'text');
//  AddComponentProp(row_Step, 'text');


  with TRegIniFile.Create(FRegPath) do
  begin

//    sClutterModel_name:=ReadInteger('', FLD_CLUTTER_MODEL_ID, 0);
    FClutterModelID:=ReadInteger('', FLD_CLUTTER_MODEL_ID, 0);
    Free;
  end;


  if FClutterModelID=0 then
    FClutterModelID:=dmClutterModel.GetDefaultID;



  row_ClutterModel.Properties.Value:= dmClutterModel.GetNameByID(FClutterModelID);


//  row_ClutterModel.Text:= gl_DB.GetNameByID(TBL_CLUTTER_MODEL, FClutterModelID);
  if row_ClutterModel.Properties.Value='' then
    FClutterModelID:= 0;

 // row_CalcModel.Text:= 'LOS';

  cx_InitVerticalGrid (cxVerticalGrid1);

//  FRegPath:= 'Software\Onega\Property_Los\'+ ClassName+'\';

  cx_VerticalGrid_LoadFromReg(cxVerticalGrid1, FRegPath);



///////  db_SetComponentADOConnection(Self, dmMain.ADOConnection1);

//  FormStyle := fsStayOnTop;
end;

//--------------------------------------------------------------------
procedure Tdlg_Property_Los.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  cx_VerticalGrid_SaveToReg(cxVerticalGrid1, FRegPath);


  with TRegIniFile.Create(FRegPath) do
  begin
    WriteInteger('', FLD_CLUTTER_MODEL_ID, FClutterModelID);
    Free;
  end;


  inherited;
end;

procedure Tdlg_Property_Los.CreateParams(var Params: TCreateParams);
begin
  inherited;
//  Params.WndParent := 0;
end;

//-------------------------------------------------------------------
procedure Tdlg_Property_Los.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//-------------------------------------------------------------------
begin
  act_Calc.Enabled:= (FClutterModelID > 0);
end;


procedure Tdlg_Property_Los.act_CalcExecute(Sender: TObject);
begin
  Calc();
end;



procedure Tdlg_Property_Los.Action1Execute(Sender: TObject);
begin
  Calc();
end;



procedure Tdlg_Property_Los.Button1Click(Sender: TObject);
begin
  Calc();
end;

procedure Tdlg_Property_Los.Button2Click(Sender: TObject);
begin
  OpenData_calc;
end;


procedure Tdlg_Property_Los.OpenData_calc;
var
  k: Integer;
begin
 // db_SetComponentADOConnection(Self, dmMain.ADOConnection1);

  k:=dmOnega_DB_data.OpenStoredProc(sp_Calc, 'property.sp_Get_Nearest_by_radius_km_SEL',

//  k:=db_StoredProc_Open(ADOStoredProc1, 'property.sp_Get_Nearest_by_radius_km_SEL',
  [
    'property_ID', g_Ini_Property_LOS_Params.Property_ID,
    'RADIUS_KM',   row_Max_distance_km.Properties.Value,

    'SRC_CALC_HEIGHT', row_Prop_Antenna_height_for_calc.Properties.Value


  ]);

  {
  try
    dxMemData1.Close;
    dxMemData1.LoadFromDataSet(ADOStoredProc1);
    DataSource1.DataSet:=dxMemData1;

  except
    on e:exception do // Solve the problem


  end;
  }

//  u_ini_Property_LOS_params


 //   @property_ID int = null,
//  @RADIUS_KM float = 10

end;



//-------------------------------------------------------------------
procedure Tdlg_Property_Los.Calc;
//-------------------------------------------------------------------
//var
//  iLen: integer;
//  I: integer;
begin
  OpenData_calc;


  dmProperty_LOS.Params.ClutterModelID :=FClutterModelID;
  dmProperty_LOS.Params.Max_distance_km:=row_Max_distance_km.Properties.Value;
  dmProperty_LOS.Params.height_max     :=row_Height.Properties.Value;
  dmProperty_LOS.Params.Master_Antenna_height_for_calc:=row_Prop_Antenna_height_for_calc.Properties.Value;

  dmProperty_LOS.Params.Dataset:=sp_Calc;
//  dmProperty_LOS.Params.Dataset:=dxMemData1;

  dmProperty_LOS.ExecuteProc;

end;

//-------------------------------------------------------------------
procedure Tdlg_Property_Los.TestCalc;
//-------------------------------------------------------------------
begin
 // Fframe_lOS_cells.SelectAll;

  Calc;
end;

//-------------------------------------------------------------------
procedure Tdlg_Property_Los.OpenData_Prop(aID: Integer);
//-------------------------------------------------------------------
var
  k: Integer;
begin
  dmOnega_DB_data.OpenQuery(q_Prop,
//  db_Ope  nQuery(q_Prop,
      ' SELECT id, name,	IsNull(ground_height,0) as ground_height , '+
      ' 			(select max(height) from Linkend_Antenna where Property_id=p.id) as height_max'+
      ' FROM  [PROPERTY] P '+
      ' where id='+IntToStr(aID) );

  if q_Prop.RecordCount>0 then
  begin
    row_name.Properties.Value           := q_Prop.FieldValues['name'];

    row_Prop_antenna_H.Properties.Value := q_Prop.FieldValues['height_max'];
    row_Prop_ground_H.Properties.Value  := q_Prop.FieldValues['ground_height'];

    row_Prop_Antenna_height_for_calc.Properties.Value := q_Prop.FieldValues['height_max'];

  end;

 // Fframe_lOS_cells.SelectAll;

end;



procedure Tdlg_Property_Los.row_ClutterModelEditPropertiesButtonClick( Sender: TObject; AButtonIndex: Integer);
var
  iID: integer;
  sName: string;
begin
  //------------------
  if cxVerticalGrid1.FocusedRow=row_ClutterModel then
  begin

(*
    if dmAct_Explorer.Dlg_Select_Object (otClutterModel, FClutterModelID, sName) then
    begin
      row_ClutterModel.Properties.Value:=sName;
      (Sender as TcxButtonEdit).EditValue:=sName;
    end;
    *)


    if Tdlg_DB_select.ExecDlg('', dmMain.ADOConnection, TBL_ClutterModel, FClutterModelID, sName) then
    begin
      cx_SetButtonEditText(Sender as TcxButtonEdit, sName);
    end;

  //    row__Clu.Properties.Value:=FtmpName;
    //  FClutterModelID:=FtmpID;


{      cx_

      row_ClutterModel.Properties.Value:=FtmpName;
      (Sender as TcxButtonEdit).EditValue:=FtmpName;
}

  end else

end;


procedure Tdlg_Property_Los.act_Excel_exportExecute(Sender: TObject);
begin
  if SaveDialog_Excel.Execute then
    cx_ExportGridToExcel(SaveDialog_Excel.FileName, cxGrid1);

//  inherited;
end;

procedure Tdlg_Property_Los.act_view_profileExecute(Sender: TObject);
begin
  Profile_Image;
end;


procedure Tdlg_Property_Los.act_view_XMLExecute(Sender: TObject);
var
  s: string;
begin
  s:=sp_Calc['profile_xml'];

  ShellExec_Notepad_temp1(s, 'xml');

end;

//----------------------------------------------------------------
procedure Tdlg_Property_Los.Profile_Image;
//----------------------------------------------------------------
var
//  s: string;
  oProfile: TrelProfile;
  oForm: Tframe_Profile_Rel_Graph;
  s: string;

begin
  oForm:=Tframe_Profile_Rel_Graph.Create(nil);
  oForm.Hide_TxRx;

  oProfile:=TrelProfile.Create;

  s:=sp_Calc['profile_xml'];
  oProfile.LoadFromXml(s);

  oForm.Width := 900;
  oForm.Height:= 700;

//  oForm.Height:=g_Config.IMAGE_PROFILE_HEIGHT;//400;

  oForm.RelProfile_Ref:=oProfile;

  oForm.Params.SetSiteGroupCount(1);
  oForm.Params.SiteGroups[0].Site1.AntennaHeights[0]:=sp_Calc.FieldByName('SRC_CALC_HEIGHT').AsFloat;
  oForm.Params.SiteGroups[0].Site2.AntennaHeights[0]:=sp_Calc.FieldByName('min_h').AsFloat;
  oForm.Params.SiteGroups[0].Distance_KM:=oProfile.Data.Distance_KM;

  //SRC_CALC_HEIGHT
  //min_h

  oForm.Params.SiteGroups[0].Site1.Rel_H:=oProfile.FirstItem.Rel_H;
  oForm.Params.SiteGroups[0].Site2.Rel_H:=oProfile.LastItem.Rel_H;

  oForm.ShowGraph;

  oForm.ShowModal;

//  oForm.pr
  //  ShellExec_Notepad_temp1(s, 'xml');

  FreeAndNil (oProfile);
  FreeAndNil (oForm);
end;



end.





  {

procedure Tdlg_Property_Los.OpenData;
begin
//  db_SetComponentADOConnection(Self, dmMain.ADOConnection1);

 // db_Open Query(ADOQuery1, 'select * from property where id=7996' )



//  u_ini_Property_LOS_params


 //   @property_ID int = null,
//  @RADIUS_KM float = 10

end;



  @SRC_CALC_HEIGHT int =null




  Tframe_Profile_Rel_Graph

  // ---------------------------------------------------------------
procedure TdmTask_calc.MakeProfileImagePNG(aTaskPlace: TTaskPlace; aFileName:
    string);
// ---------------------------------------------------------------
var
  oForm: Tframe_Profile_Rel_Graph;
  i: Integer;
  dt_start: TDateTime;
begin
  dt_start:=Now;
  CodeSite.Send( 'procedure TdmTask_profile.MakeProfileImagePNG...' );


  oForm:=Tframe_Profile_Rel_Graph.Create(nil);

 // oForm.SetChartSize (550,400);

//  oForm.Show;
  oForm.BringToFront;

//  oForm.SetChartSize(g_Config.IMAGE_PROFILE_WIDTH, g_Config.IMAGE_PROFILE_HEIGHT);// 550;

  oForm.Width :=g_Config.IMAGE_PROFILE_WIDTH;// 550;
  oForm.Height:=g_Config.IMAGE_PROFILE_HEIGHT;//400;

//  oForm.Height:=g_Config.IMAGE_PROFILE_HEIGHT;//400;

  oForm.RelProfile_Ref:=FProfile;


//  oForm.Params.SetSiteGroupCount(1);
  if aTaskPlace.Freq_MHz > 0 then
    oForm.Params.Freq_MHz:=aTaskPlace.Freq_MHz
  else
    oForm.Params.Freq_MHz:=g_Config.Freq_GHz * 1000;

//  oForm.Params.Freq_MHz:=aRec.freq_GHz  * 1000;

  oForm.Params.Distance_KM:=FProfile.Data.Distance_KM;

//  oForm.Params.Place_Name:=aTaskPlace.place_name + Format(' %1.1f', [oTaskPlace.Azimuth_Place_to_Client]);
//  oForm.Params.Place_Name:=aTaskPlace.place_name_display + Format(' %1.1f', [oTaskPlace.Azimuth_Place_to_Client])
//
//  oForm.Params.Place_Name:=aTaskPlace.place_name;
  oForm.Params.Place_Name :=aTaskPlace.place_name_display + Format(' (%1.1f�)', [aTaskPlace.Azimuth_Place_to_Client]);
  oForm.Params.Client_Name:='������'                      + Format(' (%1.1f�)', [aTaskPlace.Azimuth_Client_to_Place]);

//      oTaskPlace.Azimuth_Place_to_Client:=
//        geo_Azimuth (MakeBLPoint (oTaskPlace.Pos_wgs.Lat, oTaskPlace.Pos_wgs.Lon),
//                     MakeBLPoint (aTask.wgs.client_lat, aTask.wgs.client_lon));
//
//    oTaskPlace.Azimuth_Client_to_Place:=
//        geo_Azimuth (MakeBLPoint (aTask.wgs.client_lat, aTask.wgs.client_lon),



  //�� ����. ������� ������ � �������� �� �� ��������� ����� (�.3.3 ����������� ����������).
  //���������� ��������� �������� �� �� �������� � �������� ���������

  //RelProfile.GeoDistance_KM();

//  iDirection:=ADOStoredProc_Link.FieldByName(FLD_CALC_DIRECTION).AsInteger;
//
//  Assert (aTaskPlace.CalcResult.client_antenna_height > 0, 'aTaskPlace.CalcResult.client_antenna_height>0');

  if aTaskPlace.CalcResult.client_antenna_height=0 then
    aTaskPlace.CalcResult.client_antenna_height:=1;

  oForm.Params.Site1.AntennaHeight:= aTaskPlace.CalcResult.client_antenna_height;
  oForm.Params.Site1.HouseHeight  := FTask.Client.map_building_height; // aTaskPlace.map_building_height;

//  oForm.Params.SiteGroups[0].Site1.AntennaHeights:= MakeDoubleArray([FTask.client_antenna_height]);
  oForm.Params.Site2.AntennaHeight:= aTaskPlace.height;
  oForm.Params.Site2.HouseHeight  := aTaskPlace.map_building_height;


  oForm.Params.Site1.Rel_H:=FProfile.FirstItem.Rel_H;
  oForm.Params.Site2.Rel_H:=FProfile.LastItem.Rel_H;


 // oForm.Params.SiteGroups[0].ReflectionPointArr

  oForm.ShowGraph;

//  oForm.ShowModal;

//  oForm.DrawHeader;

//  Delay(500);

//.........
  { TODO : speed }
  ForceDirectories( ExtractFilePath (aFileName) );

  oForm.SaveToPNG(aFileName);

  Assert (FileExists (aFileName));

  //ConvertFileToBase64(aFileName, ChangeFileExt(aFileName, '.Base64'));

//  ShellExec(aFileName); //aFileName: string; aFileParams: string = '');

  FreeAndNil(oForm);

  aTaskPlace.CalcResult.FileName_Profile:=aFileName;

  CodeSite.Send( 'procedure TdmTask_profile.MakeProfileImagePNG done - ' +
                 intToStr( MilliSecondsBetween(dt_start, Now)) + ' msec, ' +
                 intToStr( SecondsBetween(dt_start, Now)) + ' sec');


end;


min_h
