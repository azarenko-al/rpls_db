unit dm_Property_LOS;

interface

uses

  d_db_ViewDataset,
  u_db,
  u_func,
  u_vars,

   I_rel_Matrix1,
   u_rel_engine,

//  dm_CalcMap,

//  dm_Progress,

  dm_Rel_Engine,
  dm_ClutterModel,

 // dm_ColorSchema_export,

  u_MapX,
  u_MapX_lib,
 // u_mitab,

//  u_XML,
  u_rel_Profile,
  u_rel_Profile_tools,
  u_profile_types,

  u_geo,

  SysUtils, Classes, Graphics, Forms, Dialogs, DB

  ;

type
  TdmProperty_LOS = class(TDatamodule)
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FRowCount: integer;
    FColCount: integer;
    FCellCount: integer;

    FrelProfile: TrelProfile;

    FLosFileName: string;
//    FTabFileName: string;
//    procedure RegisterMap;

  protected
 //   procedure SaveToXMLFile(aFileName: string);
  public
    Params: record
      CalcStep_m      : integer;
      Refraction      : double;
      ClutterModelID  : integer;

      TabFileName     : string;

      Max_distance_km : Integer;
      Dataset: TDataset;

      height_max : double;

      Master_Antenna_height_for_calc : double;


{      BLPoint: TBLPoint;
      Radius: double;}
    //  SiteHeight, TerminalHeight : double;
//      Freq:   double;

  //    Azimuth: double;
  //    Width  : double;

//      ColorSchemaID_LOS: integer;

    //  Use_Site_Pos: boolean;

    end;

   {
    Sites: array of record
                 ID      : integer;
                 Name    : string;
                 Height  : integer;
                 BLPoint : TBLPoint;
               end;

    OutMatrix: array of array of
               record
              //   ID1,ID2: integer;
                 LosValue: integer;

//               Height : integer;
 //              BLPoint: TBLPoint;
               end;
    }

    procedure CreateMap;
    procedure ExecuteProc;

    class procedure Init;

//    procedure InitOutMatrix;
//    procedure SetOutMatrixValue(aSite1, aSite2, aValue: Integer);
  end;

var
  dmProperty_LOS: TdmProperty_LOS;


const
   DEF_NULL_VALUED=999;

//====================================================================
// implementation
//====================================================================

implementation  {$R *.DFM}


// -------------------------------------------------------------
class procedure TdmProperty_LOS.Init;
// -------------------------------------------------------------
begin
  if not Assigned(dmProperty_LOS) then
    dmProperty_LOS := TdmProperty_LOS.Create(Application);
end;

// ---------------------------------------------------------------
procedure TdmProperty_LOS.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;

//    db_SetComponentADOConnection(Self, dmMain.AdoConnection);


{
  Params.BLPoint.B:=59.9966352038686;
  Params.BLPoint.L:=30.1366276631735;
}

{  Params.Radius:=15;
  Params.SiteHeight:=30;
  Params.TerminalHeight:=5;}

 // Params.Freq:=13;
//  Params.Refraction:=1.33;
//  Params.CalcStep_m:=200;


  Params.CalcStep_m:=20;
  Params.Refraction:=1.33;


  FrelProfile:=TrelProfile.Create;

end;


procedure TdmProperty_LOS.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FrelProfile);

  inherited;
end;


// ---------------------------------------------------------------
procedure TdmProperty_LOS.ExecuteProc;
// ---------------------------------------------------------------

var
  iSite2: integer;
  iSite1: integer;
  i: integer;
  dLosValue: Double;
   sDir: string;

  // blRect: TBLRect;
   blVector: TblVector;

  profType: TrelProfileType;

  blPoints: TBLPointArrayF;
  dLosValue_min_h: Double;
  eClient_H: Double;
  eMaster_H: Double;
//  eClient_ground_H: Double;
  iH: integer;

  k: Integer;
 // eDistance_km: Double;


 rec: TBuildProfileParamRec;
  sXML: string;

begin
  Assert(Assigned(Params.Dataset));

  Assert(Params.ClutterModelID>0);
  Assert(Params.Max_distance_km>0, 'Value <=0');


  dmClutterModel.AssignClutterModel (Params.ClutterModelID, FrelProfile.Clutters);


//  blPoints.Count:=Length(Sites);

//  for I := 0 to Length(Sites) - 1 do
//    blPoints.Items[i] := Sites[i].BLPoint;


 // blRect:=geo_RoundBLPointsToBLRect_F (blPoints);

  // ---------------------------------------------------------------

//  DoLog ('���������� ������ �����');

  dmRel_Engine.Open;// OpenByRect (blRect);


  k:=GetReliefEngine.Count;

  if GetReliefEngine.Count=0 then
  begin
    ShowMessage('��� ������ ����� !');
    exit;
  end;



//db_View(Params.Dataset);

   Params.Dataset.DisableControls;
   Params.Dataset.First;

   with Params.Dataset do
     while not EOF do
     begin
       blVector.Point1.B:=FieldByName('src_lat').AsFloat;
       blVector.Point1.L:=FieldByName('src_lon').AsFloat;

       blVector.Point2.B:=FieldByName('lat').AsFloat;
       blVector.Point2.L:=FieldByName('lon').AsFloat;



       eMaster_H:=//FieldByName('src_ground_height').AsFloat  //+ FieldByName('src_height_max').AsFloat;
               // FieldByName('src_height_max').AsFloat;
                Params.Master_Antenna_height_for_calc;


//            + IIF(FieldByName('src_height_max').AsFloat > 0, FieldByName('src_height_max').AsFloat, Params.height_max);

       eClient_H:=//FieldByName('ground_height').AsFloat
                FieldByName('height_max').AsFloat;

//            + IIF(FieldByName('height_max').AsFloat > 0, FieldByName('height_max').AsFloat, Params.height_max);

//       eClient_ground_H:=FieldByName('ground_height').AsFloat;
//            + IIF(FieldByName('height_max').AsFloat > 0, FieldByName('height_max').AsFloat, Params.height_max);


       //src_lat

       FillChar(rec,SizeOf(rec),0);

       rec.blVector := blVector;
       rec.Step_m   := Params.CalcStep_m;
      // rec.Zone     := iBest6Zone;
       rec.refraction    := Params.Refraction;
       rec.MaxDistance_KM:= Params.Max_Distance_KM;



      // if BuildProfileLine (antennaXYPoint.x, antennaXYPoint.y, x,y)
       if GetReliefEngine.BuildProfileBL (FrelProfile.Data, rec,
            blVector, Params.CalcStep_m, Params.Refraction, Params.Max_Distance_KM
            )
       then
       begin
          FrelProfile.ApplyDefaultClutterHeights;
      //    FrelProfile.SaveToXmlFile('f:\profile.xml');

          profType:=TRelProfile_tools.GetType_with_Los (FrelProfile,
                              eMaster_H, eClient_H,
                              //Sites[iSite1].Height, Sites[iSite2].Height,
                              999999, //freq
                              dLosValue);

          sXML:=FrelProfile.GetXML();

          db_UpdateRecord__(Params.Dataset,
           [
            'prosvet_m',     dLosValue,
            'profile_XML',   sXML,
            'ground_height', FrelProfile.LastItem.Rel_H
            ]);

//              oForm.Params.SiteGroups[0].Site1.Rel_H:=oProfile.FirstItem.Rel_H;
//  oForm.Params.SiteGroups[0].Site2.Rel_H:=oProfile.LastItem.Rel_H;


          //--------------------------------------------
          //
          //--------------------------------------------
          dLosValue_min_h:=0;

          for iH:=1 to 100 do
          begin
            profType:=TRelProfile_tools.GetType_with_Los (FrelProfile,
                                eMaster_H, iH,
                                //Sites[iSite1].Height, Sites[iSite2].Height,
                                999999, //freq
                                dLosValue_min_h);

            if profType=prtOpen then
            begin
               db_UpdateRecord__(Params.Dataset, ['min_h', iH]);

              Break;
            end;
          end;

      //min_h


  //        SetOutMatrixValue(iSite1,iSite2, -Round(dLosValue));
       end;

       Next;
     end;

   Params.Dataset.EnableControls;

end;


//-------------------------------------------------------------------
procedure TdmProperty_LOS.CreateMap;
//-------------------------------------------------------------------
var
  cl: integer;
  j: integer;
  I: integer;

  oMap: TmiMap;
  blVector: TblVector;
  miStyle: TmiStyleRec;
const
  FLD_VALUE = 'value'; 
begin
{

//  oMap:=TmitabMap.Create;
  oMap:=TmiMap.Create;

  FillChar(miStyle, SizeOf(miStyle), 0);

  miStyle.LineWidth:=1;
  miStyle.LineStyle:=MI_PEN_PATTERN_SOLId;

  oMap.CreateFile1 (Params.TabFileName,
                   [mapX_Field (FLD_VALUE, miTypeInt)]);

  oMap.NumericCoordSys_Set_DATUM_KRASOVKSY42();


  for i:=0 to High(Sites) do
    for j:=i+1 to High(Sites) do
    begin
      blVector.Point1:=Sites[i].BLPoint;
      blVector.Point2:=Sites[j].BLPoint;


      if OutMatrix[I,j].LosValue>=0 then
      begin
        miStyle.LineColor:=DEF_COLOR_LOSS;
        miStyle.LineStyle:=MI_PEN_PATTERN_DASH;
      end
        else
      begin
        miStyle.LineColor:=DEF_COLOR_NO_LOSS;
        miStyle.LineStyle:=MI_PEN_PATTERN_SOLId;
      end;

//      miStyle.LineColor:=cl;

      oMap.PrepareStyle(miFeatureTypeLine, miStyle);

//      oMap.WriteVector (blVector, // miStyle,
      oMap.WriteLine (blVector, // miStyle,
                     [mapx_Par(FLD_VALUE, OutMatrix[I,j].LosValue)
                     ]);

    end;


  oMap.CloseFile;
  oMap.Free;
  }
end;




end.



{
procedure TdmProperty_LOS.InitOutMatrix;
var
  i: integer;
begin
  SetLength (OutMatrix,  Length(Sites) );
  for i:=0 to High(Sites) do
    SetLength (OutMatrix[i],  Length(Sites));

end;



procedure TdmProperty_LOS.SetOutMatrixValue(aSite1, aSite2, aValue: Integer);
begin
  OutMatrix[aSite1,aSite2].LosValue:= aValue;
  OutMatrix[aSite2,aSite1].LosValue:= aValue;

end;


// ---------------------------------------------------------------
procedure TdmProperty_LOS.RegisterMap;
// ---------------------------------------------------------------
var
  iID: Integer;
//  map_rec: TdmCalcMapAddRec1;

begin
  FillChar(map_rec,SizeOf(map_rec),0);

//    map_rec.pro
  map_rec.Section := 'LOS';
  map_rec.Name    := '����� LOS ��� ������ ��';
  map_rec.TabFileName:= Params.TabFileName;
//  map_rec.KeyName := 'los_m';
  map_rec.Checked := True;

//    map_rec.PmpCalcRegionID


//   0,  '����� LOS', FTabFileName, 'los', False, True

  iID:=dmCalcMap.RegisterCalcMap_ (map_rec);

end;


