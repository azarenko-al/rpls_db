inherited dlg_Property_Los: Tdlg_Property_Los
  Left = 680
  Top = 261
  Width = 634
  Height = 664
  Caption = 'dlg_Property_Los'
  OldCreateOrder = True
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 590
    Width = 618
    inherited Bevel1: TBevel
      Width = 618
    end
    inherited Panel3: TPanel
      Left = 431
      Width = 187
      inherited btn_Ok: TButton
        Left = 16
        Action = act_Calc
        ModalResult = 0
      end
      inherited btn_Cancel: TButton
        Left = 100
      end
    end
    object Button3: TButton
      Left = 10
      Top = 7
      Width = 75
      Height = 23
      Action = act_Excel_export
      TabOrder = 1
    end
  end
  inherited pn_Top_: TPanel
    Width = 618
    inherited Bevel2: TBevel
      Width = 618
    end
    inherited pn_Header: TPanel
      Width = 618
      Visible = False
    end
  end
  object PageControl1: TPageControl [2]
    Left = 0
    Top = 60
    Width = 618
    Height = 181
    ActivePage = TabSheet_Params
    Align = alTop
    TabOrder = 2
    object TabSheet_Params: TTabSheet
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099
      object cxVerticalGrid1: TcxVerticalGrid
        Left = 0
        Top = 0
        Width = 610
        Height = 145
        BorderStyle = cxcbsNone
        Align = alTop
        LookAndFeel.Kind = lfOffice11
        OptionsView.CellTextMaxLineCount = 3
        OptionsView.AutoScaleBands = False
        OptionsView.PaintStyle = psDelphi
        OptionsView.GridLineColor = clBtnShadow
        OptionsView.RowHeaderMinWidth = 30
        OptionsView.RowHeaderWidth = 226
        OptionsView.ValueWidth = 40
        TabOrder = 0
        Version = 1
        object row_name: TcxEditorRow
          Properties.Caption = #1055#1083#1086#1097#1072#1076#1082#1072
          Properties.EditPropertiesClassName = 'TcxTextEditProperties'
          Properties.EditProperties.Alignment.Horz = taLeftJustify
          Properties.EditProperties.Alignment.Vert = taVCenter
          Properties.EditProperties.MaxLength = 0
          Properties.EditProperties.ReadOnly = False
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object row_Prop_ground_H: TcxEditorRow
          Properties.Caption = #1042#1099#1089#1086#1090#1072' '#1047#1077#1084#1083#1080', m'
          Properties.DataBinding.ValueType = 'String'
          Properties.Options.Editing = False
          Properties.Value = Null
          ID = 1
          ParentID = 0
          Index = 0
          Version = 1
        end
        object row_Prop_antenna_H: TcxEditorRow
          Properties.Caption = 'Max '#1074#1099#1089#1086#1090#1072' '#1072#1085#1090#1077#1085#1085', m'
          Properties.DataBinding.ValueType = 'String'
          Properties.Options.Editing = False
          Properties.Value = Null
          ID = 2
          ParentID = 0
          Index = 1
          Version = 1
        end
        object row_Prop_Antenna_height_for_calc: TcxEditorRow
          Properties.Caption = #1042#1099#1089#1086#1090#1072' '#1072#1085#1090#1077#1085#1085#1099' '#1076#1083#1103' '#1088#1072#1089#1095#1077#1090#1086#1074', m'
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
          ID = 3
          ParentID = 0
          Index = 2
          Version = 1
        end
        object row_Max_distance_km: TcxEditorRow
          Properties.Caption = #1054#1075#1088#1072#1085#1080#1095#1077#1085#1080#1077' '#1076#1072#1083#1100#1085#1086#1089#1090#1080' '#1088#1072#1089#1095#1077#1090#1072' [km]'
          Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
          Properties.DataBinding.ValueType = 'Integer'
          Properties.Value = 20
          ID = 4
          ParentID = -1
          Index = 1
          Version = 1
        end
        object row_ClutterModel: TcxEditorRow
          Expanded = False
          Properties.Caption = #1052#1086#1076#1077#1083#1100' '#1090#1080#1087#1086#1074' '#1084#1077#1089#1090#1085#1086#1089#1090#1080
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Alignment.Horz = taLeftJustify
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.ReadOnly = True
          Properties.EditProperties.OnButtonClick = row_ClutterModelEditPropertiesButtonClick
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
          ID = 5
          ParentID = -1
          Index = 2
          Version = 1
        end
        object row_Height: TcxEditorRow
          Properties.Caption = #1042#1099#1089#1086#1090#1072' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102', m'
          Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
          Properties.DataBinding.ValueType = 'Integer'
          Properties.Value = 20
          ID = 6
          ParentID = -1
          Index = 3
          Version = 1
        end
      end
      object Button1: TButton
        Left = 0
        Top = 152
        Width = 75
        Height = 25
        Action = act_Calc
        TabOrder = 1
        Visible = False
      end
      object Button2: TButton
        Left = 88
        Top = 152
        Width = 73
        Height = 25
        Caption = 'test'
        TabOrder = 2
        Visible = False
        OnClick = Button2Click
      end
    end
  end
  object cxGrid1: TcxGrid [3]
    Left = 0
    Top = 436
    Width = 618
    Height = 154
    Align = alBottom
    PopupMenu = PopupMenu1
    TabOrder = 3
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = ''
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      OptionsView.HeaderAutoHeight = True
      object cxGrid1DBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Width = 37
      end
      object col_property_name_src: TcxGridDBColumn
        DataBinding.FieldName = 'property_name_src'
        Width = 100
      end
      object col_SRC_CALC_HEIGHT: TcxGridDBColumn
        DataBinding.FieldName = 'SRC_CALC_HEIGHT'
      end
      object col_name_: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Options.Editing = False
        Width = 100
      end
      object cxGrid1DBTableView1lat: TcxGridDBColumn
        DataBinding.FieldName = 'lat'
        Visible = False
        Options.Editing = False
      end
      object cxGrid1DBTableView1lon: TcxGridDBColumn
        DataBinding.FieldName = 'lon'
        Visible = False
        Options.Editing = False
      end
      object col_dist_km: TcxGridDBColumn
        DataBinding.FieldName = 'dist_km'
        Options.Editing = False
      end
      object col_ground_height: TcxGridDBColumn
        DataBinding.FieldName = 'ground_height'
        Options.Editing = False
        Width = 73
      end
      object col_height_max: TcxGridDBColumn
        Caption = 'Max '#1074#1099#1089#1086#1090#1072' '#1072#1085#1090#1077#1085#1085', m'
        DataBinding.FieldName = 'height_max'
        Options.Editing = False
        Width = 65
      end
      object cxGrid1DBTableView1src_lat: TcxGridDBColumn
        DataBinding.FieldName = 'src_lat'
        Visible = False
        Options.Editing = False
      end
      object cxGrid1DBTableView1src_lon: TcxGridDBColumn
        DataBinding.FieldName = 'src_lon'
        Visible = False
        Options.Editing = False
      end
      object col_src_ground_height: TcxGridDBColumn
        DataBinding.FieldName = 'src_ground_height'
        Visible = False
        Width = 70
      end
      object col_src_height_max: TcxGridDBColumn
        DataBinding.FieldName = 'src_height_max'
        Visible = False
        Width = 69
      end
      object col_prosvet_m: TcxGridDBColumn
        Caption = #1055#1088#1086#1089#1074#1077#1090', m'
        DataBinding.FieldName = 'prosvet_m'
        Visible = False
        Width = 66
      end
      object col_min_H: TcxGridDBColumn
        Caption = 'Min '#1074#1099#1089#1086#1090#1072', m'
        DataBinding.FieldName = 'min_h'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 216
    inherited act_Cancel: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
    end
    object act_Calc: TAction
      Caption = #1056#1072#1089#1095#1077#1090
      OnExecute = Action1Execute
    end
    object act_Excel_export: TAction
      Caption = 'Excel'
      OnExecute = act_Excel_exportExecute
    end
    object act_view_XML: TAction
      Caption = 'view XML'
      OnExecute = act_view_XMLExecute
    end
    object act_view_profile: TAction
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1087#1088#1086#1092#1080#1083#1100
      OnExecute = act_view_profileExecute
    end
  end
  inherited FormStorage1: TFormStorage
    Left = 348
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 383
  end
  inherited cxLookAndFeelController1: TcxLookAndFeelController
    Left = 312
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=server1'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 24
    Top = 256
  end
  object sp_Calc: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'property.sp_Get_Nearest_by_radius_km_SEL'
    Parameters = <>
    Left = 136
    Top = 256
  end
  object DataSource1: TDataSource
    DataSet = sp_Calc
    Left = 136
    Top = 304
  end
  object q_Prop: TADOQuery
    Parameters = <>
    Left = 360
    Top = 256
  end
  object SaveDialog_Excel: TSaveDialog
    DefaultExt = 'xls'
    Filter = 'Excel|*.xls'
    Left = 464
    Top = 264
  end
  object PopupMenu1: TPopupMenu
    Left = 24
    Top = 328
    object Excel1: TMenuItem
      Action = act_Excel_export
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object actviewprofile1: TMenuItem
      Action = act_view_profile
    end
  end
end
