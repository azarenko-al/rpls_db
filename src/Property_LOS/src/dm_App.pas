unit dm_App;

interface

uses
  SysUtils, Classes, Dialogs, 

 // i_LOS_,

  u_Ini_Property_LOS_Params,

  d_Property_Los,

  u_vars,

  dm_Main
  ;

type
  TdmMain_app = class(TDataModule)
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  end;                                                


var
  dmMain_app: TdmMain_app;

//===================================================================
// implementation
//===================================================================
implementation
 {$R *.DFM}

//--------------------------------------------------------------
procedure TdmMain_app.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const
  DEF_TEMP_FILENAME = 'Property_los.ini';

var
//  sCaption: string;
//  sKey: string;
  iID: integer;
//  iCount: integer;
  I: integer;
//  iMode: integer;
//  iMapDesktopID: integer;
  b: boolean;
 // sTabFileName: string;
//  iProjectID: integer;
  sIniFileName: string;

 // oInifile: TIniFile;
//  blPoint: TBLPoint;

//  BLPoints: TBLPointArrayF;

 // rec: TdmCalcMapAddRec;

 // iMode: (mtLos, mtLosM);

// d: double;
// BLPoint1,BLPoint2,newBLPoint: TBLPoint;

begin

//  sIniFileName:=GetCommonApplicationDataDir(DEF_SOFTWARE_NAME)+
 //                 DEF_TEMP_FILENAME;
                                    

  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
    sIniFileName:=g_ApplicationDataDir+ DEF_TEMP_FILENAME;


  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;

  //---------------------------------------------------------------------


  g_Ini_Property_LOS_Params:=TIni_Property_LOS_Params.Create;
  g_Ini_Property_LOS_Params.LoadFromFile(sIniFileName);
//  if not g_Ini_Property_LOS_Params.LoadFromFile(sIniFileName) then
 //   Exit;



  TdmMain.Init;
  if not dmMain.OpenDB_reg then
//  if not dmMain.OpenFromReg then
    Exit;



  dmMain.ProjectID:=g_Ini_Property_LOS_Params.Project_ID;


     //    dmLOS_calc.Params.TabFileName:=sTabFileName;

 // b:=
  Tdlg_Property_Los.ExecDlg();

      
{
if bIsConverted then
    ExitProcess(1)
  else
    ExitProcess(0);}

end;



procedure TdmMain_app.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(g_Ini_Property_LOS_Params);
end;



end.

