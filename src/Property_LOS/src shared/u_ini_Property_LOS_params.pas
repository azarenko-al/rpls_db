unit u_ini_Property_LOS_params;


interface
uses
   IniFiles, SysUtils, Dialogs,

   u_func,
   u_GEO
   ;

type
  TIni_Property_LOS_Params = class(TObject)
  private

  public
  //  MapDesktopID: integer;
    Project_ID: integer;
    Property_ID: integer;

    TabFileName: string;

  public
    procedure LoadFromFile(aFileName: string);
    procedure SaveToFile(aFileName: string);

  end;


var
  g_Ini_Property_LOS_Params: TIni_Property_LOS_Params;


implementation


// ---------------------------------------------------------------
procedure TIni_Property_LOS_Params.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
var
  I: Integer;
  iMode: Integer;
begin
  Assert(FileExists(aFileName), aFileName);


  with TIniFile.Create(aFileName) do
  begin
  //  IsTest   :=ReadBool   ('main', 'IsTest', False);


    Project_ID   :=ReadInteger ('main', 'Project_ID',   0);
    Property_ID  :=ReadInteger ('main', 'Property_ID',   0);
//    MapDesktopID:=ReadInteger ('main', 'MapDesktopID', 0);

    TabFileName :=ReadString  ('main', 'TabFileName', '');


    Free;
  end;

//  Result := Validate();
end;

// ---------------------------------------------------------------
procedure TIni_Property_LOS_Params.SaveToFile(aFileName: string);
// ---------------------------------------------------------------
var
  I: Integer;
begin      
  with TIniFile.Create (aFileName) do
  begin
//    WriteBool ('main', 'IsTest', IsTest)

    WriteInteger ('main', 'Project_ID',  Project_ID);
    WriteInteger ('main', 'Property_ID', Property_ID);

    WriteString  ('main', 'TabFileName',  TabFileName);
//    WriteInteger ('main', 'MapDesktopID', MapDesktopID);

    Free;
  end;

end;



end.

