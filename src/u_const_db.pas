unit u_const_db;

//==========================================================
interface
//==========================================================


const

//TBL_TEMPLATE_PMP_SITE = 'TEMPLATE_PMP_SITE';

  TBL_lib_freqplan_LinkEndType = 'lib_freqplan.LinkEndType';

  FLD_Template_link_ID = 'Template_link_ID';

 VIEW_Pmp_Terminal    = 'VIEW_Pmp_Terminal';

 view_Link_base = 'view_Link_base';

  FLD_STYLE = 'STYLE';


 TBL_PMP_CalcRegion_Site = 'PMP_CalcRegion_Site';
 TBL_PMP_CalcRegion_Polygons = 'PMP_CalcRegion_Polygons';


  FLD_CONNECTED_PMP_SITE_ID   = 'CONNECTED_PMP_SITE_ID';
  FLD_CONNECTED_PMP_SECTOR_ID = 'CONNECTED_PMP_SECTOR_ID';



 FLD_repeater_gain = 'repeater_gain';

  FLD_IS_ACTIVE = 'IS_ACTIVE';


////  FLD_CONNECTED_PMP_SECTOR_ID = 'CONNECTED_PMP_SECTOR_ID';

  FLD_lat_lon_full_str  = 'lat_lon_full_str';

  FLD_MAP_LAT = 'MAP_LAT';
  FLD_MAP_LON = 'MAP_LON';


  FLD_Template_Site_LinkEnd_name = 'Template_Site_LinkEnd_name';

  FLD_Property1_ID_ = 'Property1_ID_';

  FLD_Property2_ID_ = 'Property2_ID_';

  FLD_Height_MAX = 'Height_MAX';


  FLD_lat_lon_kras_wgs = 'lat_lon_kras_wgs';

  FLD_mast_index = 'mast_index';

//  FLD_Template_site_id = 'Template_site_id';


  DEF_SP_TEMPLATE_SITE_LINKEND_ANTENNA_ADD  = 'SP_TEMPLATE_SITE_LINKEND_ANTENNA_ADD';

  sp_Template_Site_Linkend_Antenna_Update = 'sp_Template_Site_Linkend_Antenna_Update';

  sp_Property_Apply_template = 'sp_Property_Apply_template';


  view_Template_Site_LinkEnd_antenna = 'view_Template_Site_LinkEnd_antenna';

  view_LINKFREQPLAN_Calc_NEIGHBORS = 'view_LINKFREQPLAN_Calc_NEIGHBORS';


  FLD_Template_Site_Linkend_ID = 'Template_Site_Linkend_ID';
  FLD_Template_Site_ID         = 'Template_Site_ID';

//  FLD_Template_Site_mast_ID = 'Template_Site_mast_ID';

//  TBL_mast = 'mast';

  SP_TEMPLATE_SITE_LINKEND_ANTENNA_ADD = 'SP_TEMPLATE_SITE_LINKEND_ANTENNA_ADD';


  TBL_TEMPLATE_Site                   = 'TEMPLATE_Site';
  TBL_TEMPLATE_Site_Linkend           = 'TEMPLATE_Site_Linkend';
  TBL_TEMPLATE_Site_Linkend_antenna   = 'TEMPLATE_Site_Linkend_antenna';


  FLD_SEARCH = 'SEARCH';

  FLD_WHOLE_WORD = 'WHOLE_WORD';


  FLD_FOLDER_PATH = 'FOLDER_PATH';


  VIEW_link_location = 'VIEW_link_location';


  FLD_Reliability = 'Reliability';

  FLD_Is_Working = 'Is_Working';


  view_LINKEND_with_PROJECT_ID = 'view_LINKEND_with_PROJECT_ID';
  VIEW_LINKEND_ANTENNA_with_PROJECT_ID = 'VIEW_LINKEND_ANTENNA_with_PROJECT_ID';



  FLD_CHANNEL_NUMBER = 'CHANNEL_NUMBER';

  FLD_MAX_LAT = 'MAX_LAT';
  FLD_MIN_LAT = 'MIN_LAT';
  FLD_MAX_LON = 'MAX_LON';
  FLD_MIN_LON = 'MIN_LON';


  FLD_CALC_RESULTS_XML_1 = 'CALC_RESULTS_XML_1';
  FLD_CALC_RESULTS_XML_2 = 'CALC_RESULTS_XML_2';
  FLD_CALC_RESULTS_XML_3 = 'CALC_RESULTS_XML_3';


  FLD_Antenna1_Loss = 'Antenna1_Loss';
  FLD_Antenna2_Loss = 'Antenna2_Loss';


  FLD_CALC_METHOD='CALC_METHOD';


  FLD_LAT_WGS_STR='LAT_WGS_STR';
  FLD_LON_WGS_STR='LON_WGS_STR';

  FLD_ELEVATION_angle_1 = 'ELEVATION_angle_1';
  FLD_ELEVATION_angle_2 = 'ELEVATION_angle_2';

  FLD_POLARIZATION = 'POLARIZATION';

  FLD_LINKENDTYPE_Band = 'LINKENDTYPE_Band';
  FLD_AntennaType_Band = 'AntennaType_Band';


  FLD_FOLDER_IMAGE_INDEX = 'FOLDER_IMAGE_INDEX';

  FLD_Antenna_offset_horizontal_m = 'Antenna_offset_horizontal_m';


//  FLD_Loss_dBm = 'Loss_dBm';
  FLD_Reflection_coef = 'Reflection_coef';


  FLD_Profile_XML_1 = 'Profile_XML_1';
  FLD_Profile_XML_2 = 'Profile_XML_2';

  FLD_PARENT_GUID = 'PARENT_GUID';

  FLD_LINK_INDEX = 'LINK_INDEX';


  FLD_file_date_created ='file_date_created';
  FLD_file_date_modify  ='file_date_modify';


  FLD_Alias = 'Alias';

  sp_Link_report = 'sp_Link_report';
  view_Linkend_report = 'view_Linkend_report';

  view__object_fields_enabled = 'view__object_fields_enabled';

  VIEW_LINK_REPEATER_ANTENNA = 'view_Link_repeater_Antenna';
  VIEW_LINK_REPEATER         = 'view_Link_repeater';

 // TBL_LINK_REPEATER         = 'Link_repeater';

  TBL_Link_repeater_Antenna='Link_repeater_Antenna';

  FLD_Ground_Height = 'Ground_Height';

  FLD_Property1_Ground_Height = 'Property1_Ground_Height';
  FLD_Property2_Ground_Height = 'Property2_Ground_Height';


  FLD_Antenna1_Gain = 'Antenna1_Gain';
  FLD_Antenna2_Gain = 'Antenna2_Gain';


  FLD_is_calc_map_at_bottom = 'is_calc_map_at_bottom';


  FLD_MATRIX_FILENAME = 'MATRIX_FILENAME';


//  TBL__db_version_ = '_db_version_';
  //TBL__db_upgrade = '_db_upgrade';
  TBL__db_version_ = '_db_version_';

  FLD_GUID = 'GUID';



  FLD_Antenna1_AntennaType_ID = 'Antenna1_AntennaType_ID';
  FLD_Antenna1_Height         = 'Antenna1_Height';

  FLD_Antenna2_AntennaType_ID = 'Antenna2_AntennaType_ID';
  FLD_Antenna2_Height         = 'Antenna2_Height';

//  FLD_HAS_REPEATER = 'HAS_REPEATER';


  VIEW_LinkEnd_Antenna = 'VIEW_LinkEnd_Antenna';

  FLD_LINK_REPEATER_ID = 'LINK_REPEATER_ID';


  FLD_GeoRegion1_ID = 'GeoRegion1_ID';
  FLD_GeoRegion2_ID = 'GeoRegion2_ID';

  FLD_REPEATER_LAT = 'REPEATER_LAT';
  FLD_REPEATER_LON = 'REPEATER_LON';

  FLD_NIIR1998_rain_region_number = 'NIIR1998_rain_region_number';
  FLD_NIIR1998_Qd_region_number = 'NIIR1998_Qd_region_number';

//  FLD_NIIR1998_gradient = 'NIIR1998_gradient';
//  FLD_NIIR1998_numidity = 'NIIR1998_numidity';

(*

             :=FieldByName(FLD_NIIR1998_Qd_region_number).AsInteger;

          //12 ������� �������� ��������� ��������������� ��������-����� �� ����� ���� (���.2.3 � ����.2.1)
          NIIR1998_gradient :=FieldByName(FLD_NIIR1998_gradient).AsFloat;

          //���������� ��������� �� ����� ���� (���.2.12 � ����.2.1)
          NIIR1998_numidity :=FieldByName(FLD_NIIR1998_numidity).AsFloat;


*)

  FLD_GEONAME = 'GEONAME';


//  FLD_Repeater_ID = 'Repeater_ID';


  FLD_SECTION = 'SECTION';

//  view_PMP_SECTORS = 'view_PMP_SECTORS';
  view_PMP_SECTOR = 'view_PMP_SECTOR';

  TBL_LINK_Repeater = 'Link_Repeater';


  FLD_Is_UsePassiveElements     = 'Is_UsePassiveElements';
  FLD_Is_CalcWithAdditionalRain = 'Is_CalcWithAdditionalRain';


  FLD_GST_equivalent_length_km = 'GST_equivalent_length_km';

  FLD_LAT_GCK_2011 = 'LAT_GCK_2011';
  FLD_LON_GCK_2011 = 'LON_GCK_2011';

  FLD_LAT_CK95 ='LAT_CK95';
  FLD_LON_CK95 ='LON_CK95';


//  FLD_lat_CK_95  = 'lat_CK_95';
//  FLD_lon_CK_95  = 'lon_CK_95';

  FLD_lat_WGS_84 = 'lat_WGS_84';
  FLD_lon_WGS_84 = 'lon_WGS_84';



  FLD_lat_CK_95_str  = 'lat_CK_95_str';
  FLD_lon_CK_95_str  = 'lon_CK_95_str';
  FLD_lat_WGS_84_str = 'lat_WGS_84_str';
  FLD_lon_WGS_84_str = 'lon_WGS_84_str';

  FLD_lat_lon_CK_42     = 'lat_lon_CK_42';
  FLD_lat_lon_CK_42_str = 'lat_lon_CK_42_str';

  FLD_lat_lon_CK_95     = 'lat_lon_CK_95';
  FLD_lat_lon_CK_95_str = 'lat_lon_CK_95_str';

  FLD_lat_lon_WGS_84     = 'lat_lon_WGS_84';
  FLD_lat_lon_WGS_84_str = 'lat_lon_WGS_84_str';













//  TBL_PmpTerminal    = 'PmpTerminal';

  FLD_Relief_ID = 'Relief_ID';

  FLD_OBJNAME = 'OBJNAME';

  FLD_FREQ = 'FREQ';

  VIEW_Link_with_Repeater = 'VIEW_Link_with_Repeater';
//  VIEW_PROJECT_SUMMARY_ = 'view_project_summary_';

  FLD_LinkEnd_name = 'LinkEnd_name';

  FLD_XREF_ID = 'XREF_ID';

  FLD_object_field_id = 'object_field_id';

  FLD_level_tx_a = 'level_tx_a';
  FLD_level_rx_a = 'level_rx_a';

//  FLD_is_profile_reversed = 'is_profile_reversed';

  FLD_antenna_count = 'antenna_count';

  FLD_RX_LINK_ID = 'RX_LINK_ID';

  FLD_THRESHOLD_DEGRADATION_CIA = 'THRESHOLD_DEGRADATION_CIA';

  FLD_DISTANCE_km = 'DISTANCE_km';


  FLD_NEXT_Linkend_id = 'NEXT_Linkend_id';

  FLD_NAME_TN ='NAME_TN';

  FLD_DISTANCE_m = 'DISTANCE_m';


//  VIEW_Status                     = 'VIEW_status';
  FLD_PROPERTY_LAT = 'PROPERTY_LAT';
  FLD_PROPERTY_LON = 'PROPERTY_LON';

  FLD_ANTENNA_LOSS = 'ANTENNA_LOSS';


  FLD_RX_LINKEND_ID = 'RX_LINKEND_ID';

  FLD_PMP_SITE_NAME ='PMP_SITE_NAME';

  FLD_CALCMAP_ID='CALCMAP_ID';
  FLD_channel_width_MHz = 'channel_width_MHz';
  FLD_GOST_53363_modulation = 'GOST_53363_modulation';
  FLD_GOST_53363_RX_count_on_combined_diversity = 'GOST_53363_RX_count_on_combined_diversity';

  FLD_Modulation_LEVEL_COUNT = 'Modulation_LEVEL_COUNT';
  FLD_radiation_class ='radiation_class';
  FLD_REF_ID      = 'REF_ID';
  FLD_REF_OBJNAME = 'REF_OBJNAME';
  FLD_show_calc_maps   ='show_calc_maps';
  FLD_show_geo_maps    ='show_geo_maps';
  FLD_show_object_maps ='show_object_maps';
  FLD_traffic_type ='traffic_type';
  FLD_TxRx_Shift ='TxRx_Shift';
  FLD_CHANNEL_TYPE ='CHANNEL_TYPE';
  FLD_Channel ='Channel';

  FLD_Modulation_type ='Modulation_type';

  FLD_Link1_ID ='Link1_ID';

  FLD_Link2_ID ='Link2_ID';


  FLD_label_style ='label_style';

 // FLD_CHANNEL_TYPE = 'CHANNEL_TYPE';


  FLD_NAME_ERP ='NAME_ERP';
  FLD_NAME_SDB ='NAME_SDB';

//  FLD_Loss_dB ='Loss_dB';
  FLD_Freq_Spacing_MHz   ='Freq_Spacing_MHz';

  FLD_Signature_width_MHz='Signature_width_MHz';
  FLD_Signature_height_dB='Signature_height_dB';

  FLD_SIGNATURE_HEIGHT = 'SIGNATURE_HEIGHT';
  FLD_SIGNATURE_WIDTH  = 'SIGNATURE_WIDTH';
  FLD_THRESHOLD_BER_3  = 'THRESHOLD_BER_3';
  FLD_THRESHOLD_BER_6  = 'THRESHOLD_BER_6';


  TBL_LINKEND_ANTENNA = 'LINKEND_ANTENNA';

  TBL_status         = 'status';
//  TBL_Status_Values  = 'Status_Values';

  TBL_LIB_PROPERTY_PLACEMENT = 'LIB_PROPERTY_PLACEMENT';
  TBL_LIB_PROPERTY_STATE = 'LIB_PROPERTY_STATE';
  TBL_LIB_LINK_STATE = 'LIB_LINK_STATE';

  VIEW_LINKENDTYPE = 'VIEW_LINKENDTYPE';
  view_LINKLINE = 'view_LINKLINE';

  VIEW_LINKENDTYPE_MODE ='VIEW_LINKENDTYPE_MODE';
  
 // VIEW_LINKFREQPLAN_REPORT_NEW ='VIEW_LINKFREQPLAN_REPORT_NEW';

 VIEW_LINKFREQPLAN_REPORT ='VIEW_LINKFREQPLAN_REPORT';

  VIEW_Status_Values = 'VIEW_Status_Values';
  VIEW_Status_Items  = 'VIEW_Status_Items';
//  FLD_LEVEL_COUNT ='LEVEL_COUNT';


  VIEW_PMP_CALCREGION_POLYGONS ='VIEW_PMP_CALCREGION_POLYGONS';
 // VIEW_Link_Properties         ='VIEW_Link_Properties';


  VIEW_PMP_CALCREGION_USED_CELL_Layers ='VIEW_PMP_CALCREGION_USED_CELL_Layers';

  VIEW_PMP_CALCREGION_PMP_Antenna = 'VIEW_PMP_CALCREGION_PMP_Antenna';



  //-------------------------------------------------------
  // LinkEndType
  //-------------------------------------------------------
//  sp_LinkEndType_Band_Channel_Select = 'sp_LinkEndType_Band_Channel_Select';


  FLD_Vendor_Equipment_ID = 'Vendor_Equipment_ID';
  FLD_Vendor_Equipment_NAME ='Vendor_Equipment_NAME';


//  TBL_PMP_SECTOR                  = 'PMP_SECTOR';

  FLD_CALC_RADIUS_KM ='CALC_RADIUS_KM';

  //-------------------------------------------------------
  // LinkLine
  //-------------------------------------------------------

  //-------------------------------------------------------
  // AntType
  //-------------------------------------------------------

 // sp_AntType_Copy = 'sp_AntType_Copy';


  //-------------------------------------------------------
  // ANTENNA
  //-------------------------------------------------------

(*
  sp_Antenna_Del = 'sp_Antenna_Del';
*)


  FLD_AZIMUTH1 = 'AZIMUTH1';
  FLD_AZIMUTH2 = 'AZIMUTH2';

  FLD_Placement_id = 'Placement_id';
  FLD_State_id     = 'State_id';

  FLD_FREQ_MIN_MHZ = 'FREQ_MIN_MHZ';
  FLD_FREQ_MAX_MHZ = 'FREQ_MAX_MHZ';
  FLD_FREQ_AVE_MHZ = 'FREQ_AVE_MHZ';


  FLD_Checksum = 'Checksum';

  FLD_FREQ_MHZ = 'FREQ_MHZ';
  FLD_FREQ_GHz = 'FREQ_GHZ';

  FLD_LENGTH_M = 'LENGTH_M';

  FLD_CITY  = 'CITY';
  FLD_ERP   = 'ERP';

  FLD_ATPC_profit_dB           ='ATPC_profit_dB';
  FLD_Threshold_degradation_dB ='Threshold_degradation_dB';
  FLD_Use_ATPC = 'Use_ATPC';

  FLD_sesr_norm = 'sesr_norm';
  FLD_kng_norm = 'kng_norm';

  FLD_GAIN_dB = 'GAIN_dB';

//  FLD_RxLevel_dBm ='RxLevel_dBm';



  FLD_Id    = 'Id';

  FLD_Name  = 'Name';

  FLD_BitRate_Mbps = 'BitRate_Mbps';

  FLD_MAXPOWER = 'MAXPOWER';
  FLD_TRXNOISE = 'TRXNOISE';
  FLD_NETSTANDARD_ID = 'NETSTANDARD_ID';

  FLD_CHANNEL_LOGIC_COUNT = 'CHANNEL_LOGIC_COUNT';

  FLD_recommendation = 'recommendation';

  FLD_VENDOR_NAME  = 'VENDOR_NAME';
  FLD_VENDOR_ID    = 'VENDOR_ID';

///////  FLD_Equipment_ID ='Equipment_ID';

  FLD_LinkEndType_MODE_ID ='LinkEndType_MODE_ID';

  FLD_Rx_Level_dBm ='Rx_Level_dBm';

  FLD_HOSTNAME  = 'HOSTNAME';

  FLD_IS_MASTER = 'IS_MASTER';


//  FLD_MAX_LINKEND_HEIGHT = 'MAX_LINKEND_HEIGHT';

  FLD_PMP_CALC_REGION_ID = 'PMP_CALC_REGION_ID';


  FLD_POLARIZATION_STR ='POLARIZATION_STR';


 // VIEW_Link_property_id = 'VIEW_Link_property_id';



//  sp_User_Add                             = 'sp_User_Add';
 // sp_User_Del_by_login                    = 'sp_User_Del_by_login';

 // sp_User_GetRoles                        = 'sp_User_GetRoles';
 // sp_UserDel                              = 'sp_UserDel';

 // SP_PROPERTY_DEL                       = 'SP_PROPERTY_DEL';
//  sp_Link_UpdateEndPoints               = 'sp_Link_UpdateEndPoints';
//  sp_User_GetAllowedGeoRegions          = 'sp_User_GetAllowedGeoRegions';


//  TBL_object_fields_user_list_items = 'object_fields_user_list_items';


  VIEW_TRXTYPE = 'VIEW_TRXTYPE';

  FLD_INDEX_ ='INDEX_';
  FLD_PATH = 'path';
  FLD_TAG ='TAG';


  TBL_LIB_VENDOR    = 'LIB_VENDOR';
  TBL_lib_VENDOR_Equipment = 'lib_VENDOR_Equipment';

  TBL_LIB_BANDS = 'LIB_BANDS';
  TBL_DOCUMENTS = 'DOCUMENTS';

  TBL_lib_Radiation_class = 'lib_Radiation_class';


//  TBL_FP_REQUEST_CELLS            = 'FP_REQUEST_CELLS';
//  TBL_FP_FREQS                    = 'FP_FREQS';
//  TBL_FP_REQUEST_DOC              = 'FP_REQUEST_DOC';
 // TBL_USER_GEOREGION_XREF         = 'USER_GEOREGION_XREF';
 // TBL_BSIC                        = 'BSIC';
  TBL_CALC_MAP                    = 'CALCMAP';
 // TBL_CARRIERS                    = 'CARRIERS';
//  TBL_CELL_CARRIERS               = 'CELL_CARRIERS';
  TBL_CELL_NEIGHBOUR              = 'CELL_NEIGHBOUR';
 // TBL_DRIVETEST_CELLS             = 'DriveTestCells';
//  TBL_DRIVETEST_POINTS            = 'DriveTestPoints';
  TBL_FILTER                      = 'FILTER';
  TBL_FOLDER                      = 'Folder';
//  TBL_GEOCITY                     = 'GEOCITY';
  TBL_GEOREGION                   = 'GEOREGION';

 // TBL_LAC                         = 'LAC';
//  TBL_LinkFreqPlan                = 'LinkFreqPlan';
//  TBL_LinkFreqPlan_LINKEND        = 'LinkFreqPlan_LINKEND';
 // TBL_LinkFreqPlan_NEIGHBORS      = 'LinkFreqPlan_NEIGHBORS';
 // TBL_LinkFreqPlan_RESOURCE       = 'LinkFreqPlan_RESOURCE';
//  TBL_LINK_FREQPLAN               = 'LinkFreqPlan';

  TBL_LinkFreqPlan_LINK           = 'LinkFreqPlan_Link';

  TBL_LinkFreqPlan                = 'LinkFreqPlan';
  TBL_LinkFreqPlan_LINKEND        = 'LinkFreqPlan_LINKEND';
  TBL_LinkFreqPlan_NEIGHBORS      = 'LinkFreqPlan_NEIGHBORS';
  TBL_LinkFreqPlan_RESOURCE       = 'LinkFreqPlan_RESOURCE';
  TBL_LINK_FREQS                  = 'LINKFREQS';
  TBL_LINKENDTYPE_BAND            = 'LINKENDTYPE_BAND';
////////  TBL_LINKENDTYPE_BAND_SPEED      = 'LINKENDTYPE_BAND_SPEED';
  TBL_LINKENDTYPE_MODE            = 'LINKENDTYPE_MODE';

//  TBL_MAP            = 'MAP';

  //  TBL_LINKLINETYPE              = 'LINKLINETYPE';
  TBL_LINK_SDB                    = 'LINK_SDB';

  TBL_TEMPLATE_Link = 'lib.TEMPLATE_Link';

  TBL_LINKNET                     = 'LINKNET';
  TBL_LINKNET_ITEMS_XREF          = 'LINKNET_ITEMS_XREF';
  
  TBL_PASSIVE_COMPONENT           = 'PASSIVE_COMPONENT';
  TBL_TEMPLATE_LINKEND_ANTENNA    = 'Template_LINKEND_Antenna';
  TBL_TEMPLATE_LINKEND            = 'Template_LINKEND';
  TBL_TEMPLATE_PMP_SITE           = 'Template_PMP_Site';
  TBL_TerminalType               = 'TerminalType';
//  TBL_TRAFFIC_MODEL_ITEMS         = 'TrafficModelItems';
//  TBL_TRANSFER_TECH               = 'TransferTechnologies';
//  TBL_TRANSFER_TECHNOLOGIES_VALUES= 'TransferTechnologiesValues';
//  TBL_TRX                       = TBL_TRXTYPE;
  TBL_TRXTYPE                     = 'TrxType';
  TBL_USER_USERGROUP_XREF         = 'USER_USERGROUP_XREF';
  TBL_USERGROUP                   = 'USERGROUP';
//  TBL_TEMPLATE_LINK               = 'TEMPLATE_LINK';

//  VIEW_ANTENNA                    = 'VIEW_ANTENNA';
  VIEW_BSC                        = 'VIEW_BSC';
  VIEW_PMP_CalcRegion             = 'view_PMP_CalcRegion';

 // VIEW_CALCREGION_CELLS           = 'VIEW_CALCREGION_CELLS';

  VIEW_PMP_CALCREGION_PMP_SECTORS     = 'VIEW_PMP_CALCREGION_PMP_SECTORS';



(*
  VIEW_PMP_CALCREGION_PMP_SITES       = 'VIEW_CALCREGION_PMP_SITES';
  VIEW_PMP_CALCREGION_SITES           = 'VIEW_CALCREGION_SITES';
*)

  VIEW_CELL                       = 'VIEW_CELL';

  VIEW_CELL_antennas              = 'view_Cell_antennas';


(*  VIEW_CELL_NB2                   = 'VIEW_CELL_NB2';
  VIEW_CELL_NEIGHBOUR             = 'VIEW_CELL_NEIGHBOUR';
  VIEW_CELLS_LOCATION             = 'VIEW_CELLS_LOCATION';
*)

  VIEW_CLUTTERS                   = 'VIEW_CLUTTERS';
  VIEW_DOCUMENTS                  = 'VIEW_DOCUMENTS';
//  VIEW_DRIVE_TEST_CELLS           = 'VIEW_DRIVETEST_CELLS';
//  VIEW_DRIVETEST_CELLS            = 'VIEW_DRIVETEST_CELLS';
  VIEW_FOLDER                     = 'view_Folder';

(*  VIEW_FreqPlan_Cells             = 'view_FreqPlan_Cells';
  VIEW_FREQPLAN_CELLS_ANTENNAS    = 'VIEW_FREQPLAN_CELLS_ANTENNAS';
  VIEW_FreqPlan_Neighbors         = 'view_FreqPlan_Neighbors';
  VIEW_FreqPLan_Report            = 'view_FreqPLan_Report';
*)

  VIEW_LINK                       = 'VIEW_LINK';
//  view_LINK_summary               = 'view_LINK_summary';
  VIEW_LINKEND                    = 'view_LinkEnd';
  VIEW_LINKEND_ANTENNAS           = 'view_LinkEnd_antennas';
  VIEW_LINKENDTYPE_BAND           = 'VIEW_LINKENDTYPE_BAND';
  VIEW_LINKFREQPLAN_CIA           = 'VIEW_LINKFREQPLAN_CIA';

  view_LINKFREQPLAN_linkend = 'view_LINKFREQPLAN_linkend';

//  VIEW_LINKFREQPLAN_LINKENDEx       = 'VIEW_LINKFREQPLAN_LINKENDEx';
  view_LinkFreqPlan_Neighbors     = 'view_LinkFreqPlan_Neighbors';
 // VIEW_LINKFREQPLAN_REPORT        = 'VIEW_LINKFREQPLAN_REPORT';
  VIEW_LINKLINE_LINKS             = 'view_LinkLine_links';
//  VIEW_linknet_items              = 'VIEW_linknet_items';
  VIEW_MSC                        = 'VIEW_MSC';
//  VIEW_PMP_CALC_ANTENNA_TYPE      = 'VIEW_PMP_CALC_ANTENNA_TYPE';
//  VIEW_PMP_CALCREGION             = 'VIEW_PMP_CALCREGION';
  VIEW_PMP_LINK                   = 'VIEW_PMP_LINK';
  VIEW_PMP_SECTOR_ANTENNAS        = 'VIEW_PMP_SECTOR_ANTENNAS';
 // VIEW_PMP_SECTORS                = 'VIEW_PMP_SECTORS';
//  VIEW_PMP_SECTORS_LOCATION       = 'VIEW_PMP_SECTORS_LOCATION';
  VIEW_PMP_SITE                   = 'VIEW_PMP_SITE';
//  view_PMP_TERMINAL               = 'view_PMP_TERMINAL';

  //view_PMP_TERMINAL_full          = 'view_PMP_TERMINAL_full';

  view_PMP_TERMINAL_ANTENNAS      = 'view_PMP_TERMINAL_ANTENNAS';
  VIEW_PROPERTY_                  = 'VIEW_PROPERTY_';
  view_Relief                     = 'view_Relief';

//  view_PMP_TERMINAL = 'view_PMP_TERMINAL';

 // VIEW_SH_LINKLINE_LINK           = 'VIEW_SH_LINKLINE_LINK';


(*
  VIEW_SH_CALCREGION_SITE         = 'VIEW_SH_CALCREGION_SITE';
  VIEW_SH_PMPCALCREGION_SITE      = 'VIEW_SH_PMPCALCREGION_SITE';
*)

  VIEW_SITE                       = 'view_Site';
  VIEW_SITE_CELLS                 = 'view_Site_cells';
//  view_Site_Full_Info             = 'view_Site_Full_Info';
//  view_Template_CellAntenna       = 'view_Template_CellAntenna';
 // VIEW_TEMPLATE_PMP_SITE          = 'view_Template_PMP_Site';


(*
  VIEW_USER_UserGroup             = 'VIEW_USER_UserGroup';
  VIEW_USERGROUP_GeoREGIONS       = 'VIEW_USERGROUP_GeoREGIONS';
  view_UserGroup_Users            = 'view_UserGroup_Users';
*)

  // ������� � ��������� �������� - ��������
  TBL__OBJECTS           = '_objects';
  TBL_OBJECT_FIELDS      = '_object_fields';

 // TBL_TrafficModel       = 'TrafficModel';

  //TBL_XCONN              = 'XCONN';
//  TBL_USER               = 'USERS';

//  TBL_Antenna             = 'Antenna';
  TBL_AntennaType        = 'AntennaType';

  TBL_CalcModel           = 'CalcModel';
  TBL_CalcModelParams     = 'CalcModelParams';

  TBL_CalcMap             = 'CalcMap';
//  TBL_CALC_MAPS           = 'CalcMap';

 // TBL_TRAU                = 'TRAU';

  TBL_ColorSchema         = 'ColorSchema';
  TBL_ColorSchemaRanges   = 'ColorSchemaRanges';

  TBL_ClutterModel        = 'ClutterModel';
  TBL_ClutterModelType    = 'ClutterModelType';
//  TBL_CLUTTER_TYPE        = 'ClutterModelType';
  TBL_ClutterModelItems = 'ClutterModelItems';

//  TBL_TRAFFIC_MODEL   = 'TrafficModel';
  TBL_CellLayer      = 'CellLayer';

  TBL_USERS           = 'USERS';
//  TBL_FEEDER          = 'FEEDER';

  TBL_SITE            = 'Site';
  TBL_CELL            = 'Cell';

  TBL_LINK            = 'Link';
  TBL_LINKEND         = 'LinkEnd';

 // TBL_LINKEND_CARRIERS= 'LinkEnd_Carriers';

//  TBL_ANTENNA_XREF = 'AntennaXREF';
//  TBL_Pmp_Terminal = 'PmpTerminal';


  TBL_LinkType         = 'LinkType';
  TBL_LinkEndType       = 'LinkEndType';
//  TBL_LINKEND_FEEDER    = 'LinkEndFeeder';
  TBL_LINKLINE          = 'LinkLine';      // RR �����
  TBL_LinkLineLinkXREF  = 'LinkLineLinkXREF'; // RR �����

  TBL_LinkCalcResults   = 'LinkCalcResults';

  TBL_RELIEF              = 'Relief';
//  VIEW_MAPFILE_ByHostName = 'view_MapFile_ByHostName';
  VIEW_MAPFILE = 'view_MapFile';
  TBL_MAP1                 = 'Map';

///  VIEW_MAP_DESKTOP      = 'view_MAP_DESKTOP';

 // TBL_MAP_DESKTOPS1      = 'MAP_DESKTOPS';
//  view_Map_Desktop     = 'view_Map_Desktop';

//  TBL_MAP_DESKTOP_MAPS  = 'MAP_DESKTOP_MAPS';
  TBL_CalcMaps          = 'CalcMap';

  TBL_REPORT            = 'REPORT';

//  TBL_NEIGHBOUR         = 'NEIGHBOUR';
  TBL_BSC               = 'BSC';
  TBL_MSC               = 'MSC';
  TBL_PROJECT           = 'Project';
  TBL_PROPERTY          = 'Property';      // ��������

//  TBL_FREQPLAN          = 'FreqPlan';

(*  TBL_FREQPLAN_CELL     = 'FreqPlan_Cell';
  TBL_FREQPLAN_CARRIERS = 'FreqPlan_CARRIERS';
  TBL_FreqPlan_Neighbor = 'FreqPlan_Neighbor';
*)

//  TBL_CALCREGION        = 'CalcRegion';
  TBL_PMP_CALCREGION = 'PMP_CALCREGION';

  TBL_PMP_CALCREGIONCELLS  = 'PMP_CalcRegionCells';
//  TBL_POLYGONS          = 'Polygons';
//  TBL_PMP_CALCREGION_POLYGONS = 'PMP_CALCREGION_Polygons';

  TBL_PMP_SITE          = 'PMP_SITE';
 // TBL_REGION            = 'Region';

  //explorer views
//  VIEW_LINK_EXPLORER ='VIEW_LINK_EXPLORER';


const
//  FLD_NUMBER_    = 'NUMBER_';
//  FLD_NUMBER     = 'NUMBER';
//  FLD_GUID_      = 'GUID_';

  FLD_OBJECT_TYPE= 'OBJECT_TYPE';
//  FLD_ACT        = 'ACT';

  FLD_LAC_ID        = 'lac_id';
 // FLD_LACK_ID       = 'lack_id';
  FLD_NCELL_ID      = 'NCELL_ID';
  FLD_TX_LinkEnd_ID = 'tx_LinkEnd_id';

  FLD_TX_PROPERTY_ID = 'TX_PROPERTY_ID';
  FLD_RX_PROPERTY_ID = 'RX_PROPERTY_ID';

  FLD_TX_PROPERTY_name = 'TX_PROPERTY_name';
  FLD_RX_PROPERTY_name = 'RX_PROPERTY_name';



  FLD_TEMPLATE_LINKEND_ID ='TEMPLATE_LINKEND_ID';

  FLD_TX_LINK_ID    = 'TX_LINK_ID';

  FLD_LINKNET_ID    = 'LINKNET_ID';

 // FLD_USER_ID     = 'USER_ID';

  FLD_SECTOR_ID     = 'SECTOR_ID';
 /////////////// FLD_FADE_RESERVE  = 'FADE_RESERVE';

  FLD_FADE_MARGIN_DB = 'FADE_MARGIN_DB';

  FLD_SUBBAND     = 'SUBBAND';

  FLD_BAND        = 'BAND';
  FLD_LINK_NAME   = 'LINK_NAME';
//  FLD_STATE       = 'STATE';
  FLD_INTERFERENCE = 'INTERFERENCE';
//  FLD_SYS_CELLID   = 'SYS_CELLID';
  FLD_LinkEnd_Type_Name = 'LinkEnd_Type_Name';

//  FLD_LinkFreqPlan_ID = 'LinkFreqPlan_ID';

 // FLD_FILEDIR     = 'FILEDIR';

  FLD_PMP_SITE_ID = 'PMP_SITE_ID';


  FLD_LINKEND_LOSS = 'LINKEND_LOSS';
//  FLD_ANTENNA_LOSS = 'ANTENNA_LOSS';

  FLD_ABS_PROSVET = 'ABS_PROSVET';


 // FLD_AveAbonentTraffic = 'AveAbonentTraffic';
 // FLD_TotalTraffic      = 'TotalTraffic';
 // FLD_MaxAbonentCount   = 'MaxAbonentCount';

  FLD_CHANEL_SPACING  = 'CHANNEL_SPACING';

  FLD_EQUIPMENT_TYPE  = 'EQUIPMENT_TYPE';

  FLD_MODULATION_COUNT= 'MODULATION_COUNT';


  FLD_COMBINER_NAME   = 'combiner_name';
  FLD_COMBINER_LOSS   = 'combiner_loss';
  FLD_COMBINER_ID     = 'COMBINER_ID';


  FLD_WHERE_SQL       ='where_SQL';

  FLD_SESR            = 'SESR';
  FLD_KNG             = 'KNG';

  FLD_XML             = 'XML';

  FLD_DATA_SOURCE     = 'data_source';

  FLD_Channel_Width   = 'Channel_Width';

//  FLD_CALC_DIRECTION  = 'CALC_DIRECTION';

 // FLD_CELL1_ID       = 'CELL1_ID';
 // FLD_CELL2_ID       = 'CELL2_ID';
  FLD_CELL_COUNT     = 'CELL_COUNT';

  FLD_POWER = 'POWER';
  FLD_Sense = 'Sense';
  

//  FLD_SPEED          = 'SPEED';
//  FLD_BAND_ID        = 'BAND_ID';
  FLD_POWER_LOSS     = 'POWER_LOSS';


  FLD_LOS_STATUS     = 'LOS_STATUS';

  FLD_CALC_RESULTS_XML = 'CALC_RESULTS_XML';


  FLD_IS_LINK_USED   = 'IS_LINK_USED';

 // FLD_PMPTERMINAL_ID = 'PMPTERMINAL_ID';
  FLD_PMP_TERMINAL_ID= 'PMP_TERMINAL_ID';
  FLD_PMP_SECTOR_ID  = 'PMP_SECTOR_ID';


  FLD_LINKEND1_ID    = 'LINKEND1_ID';
  FLD_LINKEND2_ID    = 'LINKEND2_ID';

  FLD_TX_LINKEND1_NAME = 'TX_LINKEND1_NAME';
  FLD_TX_LINKEND2_NAME = 'TX_LINKEND2_NAME';


//  FLD_BEE_BEENET       = 'BEE_BEENET';


  FLD_ALLOW_ZOOM = 'ALLOW_ZOOM';
  FLD_ZOOM_MIN   = 'ZOOM_MIN';
  FLD_ZOOM_MAX   = 'ZOOM_MAX';

  FLD_ALLOWED_CHANNELS = 'ALLOWED_CHANNELS';

  //----------------------------------------------

  //----------------------------------------------
  // project
  //----------------------------------------------
  FLD_FILEDIR_NETWORK = 'FileDir';
  FLD_FILEDIR_LOCAL   = 'FileDir_local';
//  FLD_DIR_REMOTE     = 'dir_remote';
//  FLD_DIR_LOCAL      = 'dir_local';
  FLD_IMAGE_INDEX    = 'ImageIndex';

  FLD_OBJECT_ID = 'object_id';
  FLD_INDEX_id = 'INDEX_id';

//  FLD_LINKLINE_ID = 'LINKLINE_ID';

  FLD_IS_DEFAULT = 'is_default';

  FLD_IS_GRADIENT = 'is_gradient';
  FLD_MIN_VALUE ='min_value';
  FLD_MAX_VALUE ='max_value';
  FLD_MIN_COLOR ='min_color';
  FLD_MAX_COLOR ='max_color';
  FLD_IS_TRANSPARENT = 'IS_TRANSPARENT';
  FLD_MIN = 'MIN';
  FLD_MAX = 'MAX';

  FLD_STATUS_STR = 'STATUS_STR';
 // FLD_CONTRACTOR = 'CONTRACTOR';

  FLD_STEP_LAT = 'STEP_LAT';
  FLD_STEP_LON = 'STEP_LON';

  FLD_LOCATION_TYPE = 'LOCATION_TYPE';

  FLD_PARAM   = 'param';
  FLD_ACTION  = 'action';
  FLD_POS     = 'pos';
  FLD_POS_WGS = 'pos_WGS';

  FLD_COLOR_SCHEMA_ID = 'color_schema_id';

  FLD_COLOR = 'COLOR';

  FLD_SITE_NAME = 'site_name';

  //----------------------------------------------
  // calc region
  //----------------------------------------------
  FLD_AREA          = 'area';
  FLD_DIRECTION     = 'direction';
  FLD_AntennaOnRoof = 'AntennaOnRoof';

  FLD_DISPLAY_WIDTH= 'DISPLAY_WIDTH';
  FLD_DISPLAY_NAME = 'DISPLAY_NAME';
  FLD_XREF_ObjectName = 'xref_ObjectName';
  FLD_XREF_FieldName  = 'xref_FieldName';
  FLD_XREF_TableName  = 'xref_TableName';
  FLD_IS_IN_LIST = 'IS_IN_LIST';
  FLD_IS_IN_VIEW = 'IS_IN_VIEW';

  FLD_VALUE_     = 'value_';

  //----------------------------------------------
  // carriers
  //----------------------------------------------
  FLD_CHANNELS = 'CHANNELS';

//  FLD_ANTENNA_LOSS = 'ANTENNA_LOSS';
//  FLD_CELL_LOSS = 'CELL_LOSS';


  FLD_Has_Children = 'Has_Children';

  FLD_PROFILE_STEP = 'PROFILE_STEP';

//  FLD_ADDRESS1         = 'address1';
  FLD_COMMENT         = 'comment';

  // reference by ID ----------------------------------
  FLD_ANTENNA_ID       = 'antenna_id';
  FLD_ANTENNATYPE_ID   = 'antennaType_id';
  FLD_MAP_ID           = 'map_id';
  FLD_MAP_DESKTOP_ID   = 'map_desktop_id';
  FLD_CELL_LAYER_ID    = 'cell_layer_id';
 // FLD_DRIVETEST_ID     = 'DRIVETEST_ID';

  FLD_CALC_MODEL_ID    = 'calc_model_id';
//  FLD_PMP_CALC_REGION_ID= 'PMP_calc_region_id';
  FLD_CLUTTER_MODEL_ID = 'clutter_model_id';
  FLD_CLUTTER_TYPE_ID  = 'clutter_type_id';
  FLD_FOLDER_ID        = 'folder_id';
//  FLD_FILTER_ID        = 'filter_id';

/////  FLD_FREQPLAN_ID      = 'freqplan_id';

  FLD_PROJECT_ID       = 'project_id';
  FLD_PROPERTY_ID      = 'property_id';
  FLD_TERMINAL_ID      = 'terminal_id';
//  FLD_POLYGON_ID       = 'polygon_id';
  FLD_MSC_ID           = 'msc_id';
  FLD_BSC_ID           = 'bsc_id';
//  FLD_BTS_ID           = 'bts_id';
 // FLD_CABIN_ID         = 'cabin_id';
 // FLD_TOWER_ID         = 'tower_id';

  FLD_LINK_ID          = 'link_id';
  FLD_LINKEND_ID       = 'linkend_id';

  FLD_LinkEndType_ID   = 'LinkEndType_id';

//  FLD_LINKEND1_ID_      = 'linkend1_id';
 // FLD_LINKEND2_ID_      = 'linkend2_id';

  FLD_LinkLine_ID      = 'linkline_id';
  FLD_LINKTYPE_ID      = 'linkType_id';

  FLD_SITE_ID          = 'site_id';
  FLD_CELL_ID          = 'cell_id';
  FLD_TRX_ID           = 'trx_id';


//  FLD_KEY_NAME = 'KEY_NAME';

//  FLD_MODEL_ID        = 'model_id';

  FLD_CELLSITE_ID    = 'cellsite_id';
//  FLD_DRIVE_TEST_ID  = 'drive_test_id';
//  FLD_LINKTERMEQUIP_ID  = 'linktermequip_id';

  FLD_REDUNDANCY = 'REDUNDANCY';

  FLD_MIN_DISTANCE = 'MIN_DISTANCE';
  FLD_MAX_DISTANCE = 'MAX_DISTANCE';

//  FLD_COEFF_CORR = 'COEFF_CORR';
//  FLD_DISPERSION = 'DISPERSION';
//  FLD_CKO        = 'CKO';
//  FLD_AVE_ERROR = 'AVE_ERROR';

  // ����������
  FLD_MEAN_CI  = 'meanci';

  FLD_CELLID   = 'CELLID';
 // FLD_E = 'E';

  //---- site adress -------
 // FLD_GROUND_HEIGHT = 'ground_height';

  FLD_STEP_X    = 'STEP_X';
  FLD_STEP_Y    = 'STEP_Y';

  FLD_X_MIN     = 'X_MIN';
  FLD_Y_MAX     = 'Y_MAX';
  FLD_X_MAX     = 'X_MAX';
  FLD_Y_MIN     = 'Y_MIN';

  FLD_LAT_MIN   = 'lat_min';  //������ (B)
  FLD_LON_MIN   = 'lon_min';  //������� (L)
  FLD_LAT_MAX   = 'lat_max';  //������ (B)
  FLD_LON_MAX   = 'lon_max';  //������� (L)
  FLD_LAT       = 'lat';      //������ (B)
  FLD_LON       = 'lon';      //������� (L)

//  FLD_LAT_WGS84 = 'lat_WGS';  //������ (B)
//  FLD_LON_WGS84 = 'lon_WGS';  //������� (L)

  FLD_LAT_WGS = 'lat_WGS';  //������ (B)
  FLD_LON_WGS = 'lon_WGS';  //������� (L)

  FLD_LAT1      = 'lat1';     //������ (B)
  FLD_LON1      = 'lon1';     //������� (L)
  FLD_LAT2      = 'lat2';     //������ (B)
  FLD_LON2      = 'lon2';     //������� (L)

  FLD_Equipment = 'Equipment';










  // cell layers ---------

  //---- cell site ---------
//  FLD_CALC_RADIUS  = 'calc_radius';

  FLD_CELL_CELLID = 'cellid';


  //----------------------------------------------
  // antenna types
  //----------------------------------------------
  FLD_GAIN              = 'gain';
  FLD_TILT              = 'tilt';

//  FLD_FREQ              = 'freq';

  FLD_DIAMETER          = 'diameter';
  FLD_VERT_WIDTH        = 'vert_width';
  FLD_HORZ_WIDTH        = 'horz_width';
//  FLD_Polarization      = 'polarization';
  FLD_FrontToBackRatio  = 'fronttobackratio'; // ����.���.�������� (������/�����) ���-�������, ��
  FLD_vert_mask         = 'vert_mask';
  FLD_horz_mask         = 'horz_mask';

  FLD_POLARIZATION_RATIO = 'POLARIZATION_RATIO';


  FLD_AZIMUTH     = 'azimuth';
  FLD_ANGLE       = 'angle';
  FLD_LOSS        = 'loss';
  FLD_TYPE_STR     = 'type_STR';
//  FLD_LOSS1       = 'loss1';
 // FLD_LOSS2       = 'loss2';
 // FLD_TYPE        = 'type';

//  FLD_POWER       = 'power';
  FLD_POWER_dBm  = 'power_dBm';
  FLD_THRESHOLD = 'THRESHOLD';

//  FLD_POWER    = 'power';
  FLD_POWER_W    = 'power_w';

  FLD_POWER_WT    = 'power_wt';
//  FLD_POWER_DBM   = 'power_dbm';
  FLD_HEIGHT      = 'height';
  FLD_WEIGHT      = 'weight';
  FLD_WIDTH       = 'width';
  FLD_LENGTH      = 'length';
  FLD_LENGTH_KM   = 'length_km';

  FLD_SENSE1       ='SENSE';

  //----------------------------------------------
  // map, matrix
  //----------------------------------------------
  FLD_ZONE      = 'zone';
  FLD_ZONENUM   = 'zonenum';
  FLD_STEP      = 'step';
  FLD_FILESIZE  = 'filesize';


  FLD_HEIGHT_OLD  = 'height_old';


  //----------------------------------------------
  // linkend
  //----------------------------------------------
  FLD_TX_FREQ_MHz  = 'tx_freq_MHz';
  FLD_RX_FREQ_MHz  = 'rx_freq_MHz';

  FLD_Priority = 'Priority';

  FLD_FREQ_REQ_COUNT  ='Freq_Req_Count';  // ��������� ���������� ������

  FLD_FREQ_DISTRIBUTED    ='Freq_Distributed'; //: TIntArray; // ������ �������������� ������
//  FLD_Freq_Fixed          ='Freq_Fixed'; //: TIntArray; // ������ ������������ ������
//  FLD_Freq_Forbidden      ='Freq_Forbidden'; //: TIntArray; // ������ ������������ ������

//  FLD_FREQ_REQ         = 'freq_req_count';
  FLD_FREQ_FORBIDDEN   = 'freq_forbidden';
  FLD_FREQ_FIXED       = 'freq_fixed';


  FLD_POWER_MAX = 'POWER_MAX';
//  FLD_EQUALISER_PROFIT = 'EQUALISER_PROFIT';

//=========================================================
// calc region: ����� �������
//=========================================================
  FLD_REFRACTION = 'REFRACTION';
  FLD_NFRENEL = 'NFRENEL';
//  FLD_AntennaIsOnRoof = 'AntennaIsOnRoof';
  FLD_CalcDirection = 'CalcDirection';
  FLD_CALC_MODEL_NAME = 'CALC_MODEL_NAME';

  FLD_SENSITIVITY_dBm = 'SENSITIVITY_dBm';

  FLD_Terminal_Height   = 'Terminal_Height';
  FLD_Terminal_Power_W  = 'Terminal_Power_W';
  FLD_Terminal_Sensitivity  = 'Terminal_Sensitivity';
  FLD_Terminal_Loss         = 'Terminal_Loss';


//=========================================================
// ��������� ����
//=========================================================

    // new
    FLD_CellLayer_NAME = 'CellLayer_NAME';
  //  FLD_CELL_LAYER_NAME = 'CELL_LAYER_NAME';
    FLD_TRX_NAME = 'TRX_NAME';
    FLD_ANTENNATYPE_NAME = 'ANTENNATYPE_NAME';
    FLD_PROPERTY_NAME = 'PROPERTY_NAME';

    FLD_LINKENDTYPE_NAME = 'LINKENDTYPE_NAME';
    FLD_EQUIPMENTTYPE_NAME = 'EQUIPMENTTYPE_NAME';
    FLD_LINKENDType_Band_name = 'LINKENDType_Band_name';

    FLD_START_CHANNEL_NUM = 'START_CHANNEL_NUM';
////////////////// OLD ////////////////

    FLD_cell_Color =  'cell_Color';
    FLD_Site_Number = 'Site_Number';
//    FLD_Site_Name = 'Site_Name';
    FLD_cell_name =   'cell_name';
    FLD_Distance =    'Distance';

//    FLD_Freq_req_count = 'Freq_req_count';

    FLD_Distributed_Min_raznos =    'Distrubuted_Min_raznos';
    FLD_Distributed_Count =         'Distributed_Count';
    FLD_Distributed =               'Distributed';

    FLD_Calc_order = 'Calc_order';
//    FLD_Assigned_count = 'Assigned_count';
//    FLD_Assigned       = 'Assigned';
    FLD_Fixed                 = 'Fixed';
    FLD_Fixed_Min_raznos      = 'Fixed_Min_raznos';
    FLD_Fixed_count           = 'Fixed_count';
    FLD_RowColor              = 'RowColor';
    FLD_Freqs_Forbidden_Count = 'Freqs_Forbidden_Count';
    FLD_Freqs_Forbidden       =  'Freqs_Forbidden';
    FLD_CI                    =  'CI';
    FLD_CA                    =  'CA';
    FLD_CIA                   =  'CIA';
    FLD_PercentCI             = 'PercentCI';
    FLD_Percent_TrafficLoss   = 'PercentTrafficLoss';
    FLD_Sorted_Freqs          = 'Sorted_Freqs';
    FLD_By_CIA                = 'By_CIA';
    FLD_By_CI                 = 'By_CI';
    FLD_By_CA                 = 'By_CA';
    FLD_Total_Cells           = 'Total_Cells';

    FLD_FreqsSortedByCI       = 'FreqsSortedByCI';
    FLD_FreqsSortedByCA       = 'FreqsSortedByCA';
    FLD_FreqsSortedByCIA      = 'FreqsSortedByCIA';
    FLD_FreqsSortedByCI_cells = 'FreqsSortedByCI_cells';
    FLD_FreqsSortedByCA_cells = 'FreqsSortedByCA_cells';
    FLD_FreqsSortedByCIA_cells= 'FreqsSortedByCIA_cells';
    FLD_KGR                   = 'kgr';
    FLD_Traffic               ='Traffic'; //: double;    // �������� ��������
    FLD_CalcOrder             ='CalcOrder'; //: integer;

    FLD_EFFECT= 'FLD_EFFECT';

    FLD_HandOff_Hysteresis      = 'HandOff_Hysteresis';
    FLD_HandOff_CalcRadiusLimit = 'HandOff_CalcRadiusLimit';
    FLD_HandOff_NeighborsLimit  = 'HandOff_NeighborsLimit';


    //FLD_GSM_TRAFFIC_MODEL_ID = 'TRAFFIC_MODEL_ID';

  FLD_LINKENDTYPE_BAND_ID= 'LINKENDTYPE_BAND_ID';
//  FLD_LINKEND_TYPE_BAND_ID= 'LINKEND_TYPE_BAND_ID';

  FLD_TX_FREQ_GHZ = 'TX_FREQ_GHZ';


//  FLD_LINE_NAME         = 'LINE_NAME';
////////  FLD_SPEED_E1            = 'SPEED_E1';


  FLD_RESTORE_PERIOD      = 'RESTORE_PERIOD';
  FLD_FAILURE_PERIOD      = 'FAILURE_PERIOD';


  FLD_OBJECTNAME          = 'OBJECTNAME';
  FLD_RADIUS              = 'RADIUS';
  FLD_RAIN_LENGTH_km     = 'RAIN_LENGTH_km';

  FLD_Signal_Depression_dB = 'Signal_Depression_dB';
//  FLD_Signal_Depression = 'Signal_Depression';


  FLD_TERMINAL_GAIN      = 'TERMINAL_GAIN';

  FLD_KUP_ID          = 'KUP_ID';
  FLD_KMO_ID          = 'KMO_ID';

  FLD_HANDOVER_ID     = 'HANDOVER_ID';

  FLD_COMMENTS        = 'COMMENTS';
  FLD_RECID           = 'RECID';


  FLD_TEMPLATE_PMP_SITE_ID= 'template_pmp_site_id';
  //FLD_TEMPLATE_CELL_ID= 'template_cell_id';


  FLD_CH_SPACING      = 'CH_SPACING';

//  FLD_BS_TYPE         = 'BS_TYPE';

  FLD_SORTING_VALUE   = 'SORTING_VALUE';

  FLD_CHANNEL_SPACING = 'CHANNEL_SPACING';


  FLD_EQUIPMENT_STANDARD    = 'EQUIPMENT_STANDARD';
  FLD_EQUIPMENT_NON_STANDARD= 'EQUIPMENT_NON_STANDARD';
  FLD_freq_channel_count    = 'freq_channel_count';


  FLD_CLUTTER_HEIGHT        = 'CLUTTER_HEIGHT';
  FLD_CLUTTER_NAME          = 'CLUTTER_NAME';

  FLD_SIZE                  = 'SIZE';


//  FLD_EXECUTOR_OF_WORKS     = 'EXECUTOR_OF_WORKS';

//  FLD_SPACE_SPREAD          = 'SPACE_SPREAD';


//  FLD_GOS                   = 'GOS';
//  FLD_WEIGHT_NORM           = 'WEIGHT_NORM';
//  FLD_TrafficPerCell        = 'TrafficPerCell';

 // FLD_BAND                  = 'BAND';

  FLD_OMNI                  = 'OMNI';

  FLD_TILT_TYPE             = 'TILT_TYPE';

  FLD_AutoLabel      = 'AutoLabel';
  FLD_IsSelectable   = 'IsSelectable';

  FLD_Label_BackColor  = 'Label_BackColor';
  FLD_Label_FONT_NAME  = 'Label_FONT_NAME';
  FLD_Label_FONT_COLOR = 'Label_FONT_COLOR';
  FLD_Label_FONT_SIZE  = 'Label_FONT_SIZE';



(*  FLD_LAC  ='LAC';
  FLD_BSIC ='BSIC';
  FLD_BCCH ='BCCH';
*)

  FLD_MARGIN = 'MARGIN';

  FLD_NLAT = 'NLAT';
  FLD_NLON = 'NLON';

  FLD_REQUEST_DOC_ID   = 'REQUEST_DOC_ID';
  FLD_REQUEST_CELLS_ID = 'REQUEST_CELLS_ID';


  FLD_FILENAME         = 'filename';
  FLD_OBJCAPTION       = 'objcaption';

  FLD_Alignment        = 'Alignment';

  FLD_PASSIVE_ELEMENT_ID  = 'PASSIVE_ELEMENT_ID';
  FLD_PASSIVE_ELEMENT_LOSS= 'PASSIVE_ELEMENT_LOSS';
  FLD_PASSIVE_ELEMENT_NAME= 'PASSIVE_ELEMENT_NAME';

  FLD_MODE                = 'MODE';

  FLD_BANDWIDTH           = 'BANDWIDTH';

  FLD_BANDWIDTH_TX_3      = 'BANDWIDTH_TX_3';
  FLD_BANDWIDTH_TX_30     = 'BANDWIDTH_TX_30';
  FLD_BANDWIDTH_TX_A      = 'BANDWIDTH_TX_A';

  FLD_BANDWIDTH_RX_3      = 'BANDWIDTH_RX_3';
  FLD_BANDWIDTH_RX_30     = 'BANDWIDTH_RX_30';
  FLD_BANDWIDTH_RX_A      = 'BANDWIDTH_RX_A';
{
  FLD_HIGH                = 'HIGH';
  FLD_LOW                 = 'LOW';
}

  FLD_FREQ_MIN            = 'FREQ_MIN';
  FLD_FREQ_MAX            = 'FREQ_MAX';

  FLD_E1                  = 'E1';

  FLD_MIN_CI_dB = 'MIN_CI_dB';

  FLD_CI_                 = 'ci_';

  FLD_FREQ_MIN_LOW = 'freq_min_low';
  FLD_FREQ_MAX_LOW = 'freq_max_low';
  FLD_FREQ_MIN_HIGH = 'freq_min_high';
  FLD_FREQ_MAX_HIGH = 'freq_max_high';
//  FLD_E1       = 'E1';

  FLD_HYSTERESIS           = 'HYSTERESIS';
  FLD_Level_Stored_Noise   = 'Level_Stored_Noise';

//  FLD_Manufacturer         = 'Manufacturer';

  FLD_POWER_NOISE          = 'power_noise';
  FLD_COEFF_NOISE          = 'coeff_noise';
  FLD_THRESHOLD_DEGRADATION= 'threshold_degradation';
  FLD_FREQ_ALLOW           = 'freq_allow';
  FLD_MODEL                = 'model';

  FLD_CONTENT_BIN          = 'CONTENT_BIN';
  FLD_CONTENT              = 'CONTENT';

//  FLD_CONTENT_             = 'CONTENT_';

 // FLD_THRESHOLD_BER_3      = 'THRESHOLD_BER_3';
//  FLD_THRESHOLD_BER_6      = 'THRESHOLD_BER_6';

  FLD_HEEL                 = 'HEEL';
//  FLD_SERVICE_DISTANCE     = 'SERVICE_DISTANCE';

  FLD_FREQ_SPACING         = 'freq_spacing';

 // FLD_USERGROUP_ID         = 'USERGROUP_ID';


  FLD_GEOREGION_ID         = 'GEOREGION_ID';

  FLD_NEIGHBOUR_COUNT      = 'NEIGHBOUR_COUNT';

  FLD_DESCRIPTION = 'DESCRIPTION';

//  TBL_GROUPS = 'GROUPS';


 // FLD_GROUP_ID = 'GROUP_ID';
  FLD_LOGIN    = 'LOGIN';
  FLD_PASSWORD = 'PASSWORD';
//  FLD_PHONE    = 'PHONE';
//  FLD_MAIL     = 'MAIL';

 // FLD_CAPITAL   = 'CAPITAL';
  FLD_COUNTRY  = 'COUNTRY';

 // FLD_BEELINE = 'BEELINE'; //used in fr_LinkEnd_inspector;


//  FLD_NCC    = 'NCC';
//  FLD_BCC    = 'BCC';

//  FLD_BSIC_FIXED               = 'BSIC_FIXED';
//  FLD_BSIC_DISTRIBUTED        = 'BSIC_DISTRIBUTED';

 // FLD_STATISTIC               = 'STATISTIC';

 // FLD_BSIC_ID                 = 'BSIC_ID';
  FLD_CALC_RADIUS_NOISE_km       = 'CALC_RADIUS_NOISE_km';

 // TBL_FREQPLAN_GROUPS         = 'FREQPLAN_GROUPS';

  FLD_FREQ_COUNT              = 'FLD_FREQ_COUNT';

 // FLD_DUAL_HYSTERESIS         = 'DUAL_HYSTERESIS';

//  FLD_DUAL_CHOOSE_SECTOR_PARAM= 'DUAL_CHOOSE_SECTOR_PARAM';
//  FLD_DUAL_A_CRITERIA         = 'DUAL_A_CRITERIA';

//  FLD_PercentError            = 'PercentError';

  FLD_CLUTTER_CODE            = 'CLUTTER_CODE';

//  FLD_TRANSFERTECH_ID         = 'TRANSFERTECH_ID';
//  FLD_TRANSFERTECH_NAME       = 'TRANSFERTECH_NAME';


//  FLD_SHOW_LABELS             = 'SHOW_LABELS';

  FLD_ADDRESS                 = 'ADDRESS';

  FLD_Electrical_Tilt         = 'Electrical_Tilt';


  FLD_IS_FILTERED = 'IS_FILTERED';


  FLD_AVE='ave';
//  FLD_CKO='cko';

  FLD_K0            = 'K0';
  FLD_K0_OPEN       = 'K0_OPEN';
  FLD_K0_CLOSED     = 'K0_CLOSED';
  FLD_RXLEV         = 'rxlev';
//  FLD_RXLEV_PRED = 'rxlev_pred';

(*  FLD_RXLEV_CALC    = 'rxlev_calc';
  FLD_RXLEV_ERROR   = 'rxlev_error';
  FLD_RXLEV_DRIVE   = 'rxlev';
*)

  FLD_TRAFFIC_MODEL_ID = 'TRAFFIC_MODEL_ID';
  FLD_TRANSFER_TECH_ID   = 'TRANSFER_TECH_ID';

  FLD_VALUE_BY_X         = 'VALUE_BY_X';
  FLD_VALUE_BY_Y         = 'VALUE_BY_Y';

  FLD_TRAFFIC_PERCENT='TRAFFIC_PERCENT';

  FLD_N = 'N';

  FLD_MAPSTEP = 'MAPSTEP';

  FLD_POINT_COUNT = 'POINT_COUNT';


  FLD_EXIST           = 'EXIST';
  FLD_ABONENT_COUNT   = 'ABONENT_COUNT';
//  FLD_ant_type        = 'ant_type';
 // FLD_antenna_freq    = 'antenna_freq';
//  FLD_band_1800_status= 'band_1800_status';
//  FLD_band_900_status = 'band_900_status';
  FLD_bsc_name        = 'bsc_name';
  FLD_FEATURE_ID      = 'FEATURE_ID';
  FLD_FILTERED        = 'FILTERED';
  //FLD_FREQ_CENTER     = 'FREQ_CENTER';
  FLD_ISEDITABLE      = 'ISEDITABLE';
  FLD_ISTRANSPARENT   = 'ISTRANSPARENT';
  FLD_MAP_INDEX       = 'MAP_INDEX';
  FLD_NOISE           = 'NOISE';
  FLD_property_status_id = 'property_status_id';
  FLD_REGION          = 'REGION';
//  FLD_SIGNED          = 'SIGNED';
//  FLD_AUTOLABEL       = 'AUTOLABEL';
  FLD_site_status     = 'site_status';
  FLD_SORT            = 'SORT';
  FLD_SITE            = 'SITE';
  FLD_SECTOR          = 'SECTOR';

 // FLD_IS_WARNING = 'IS_WARNING';

//  FLD_CODE_ALLOC = 'CODE_ALLOC';
//  FLD_CODE_FIX   = 'CODE_FIX';
//  FLD_CODE_FIX_COPY = 'CODE_FIX_COPY';
  FLD_REGION_NAME = 'REGION_NAME';

  FLD_TOWN = 'TOWN';
 // FLD_BSIC   = 'BSIC';
//  FLD_FREQ_SPACING = 'FLD_FREQ_SPACING';

  FLD_TOTAL_AREA = 'TOTAL_AREA';
  FLD_TOTAL_COVERAGE = 'TOTAL_COVERAGE';

 // FLD_calced_vz_vliyan = 'calced_vz_vliyan';

(*  TYPE_GSM = 'gsm';
  TYPE_PMP = 'pmp';
  TYPE_3G  = '3G';
  TYPE_2G  = '2G';
*)

  FLD_LAT_STR = 'LAT_STR';
  FLD_LON_STR = 'LON_STR';


  FLD_DisplayName = 'DisplayName';


//  FLD_ANTENNA = 'ANTENNA';


const


  FLD_Status_Name ='Status_Name';
  FLD_MAX_HEIGHT = 'MAX_HEIGHT';
  FLD_LINK_STATUS = 'LINK_STATUS';

  TBL_CELL_LAYER =TBL_CellLayer;
  TBL_TCOMPANY ='TCOMPANY';
  TBL_LIB_MODULATIONS = 'LIB_MODULATIONS';
  TBL_Link_reflection_points = 'Link_reflection_points';

  FLD_PROPERTY1_ID = 'PROPERTY1_ID';
  FLD_PROPERTY2_ID = 'PROPERTY2_ID';
  FLD_PROPERTY1_NAME = 'PROPERTY1_NAME';
  FLD_PROPERTY2_NAME = 'PROPERTY2_NAME';


  FLD_profile_XML ='profile_XML';

  FLD_STATUS_ID ='STATUS_ID';

  FLD_LINKFREQPLAN_ID ='LINKFREQPLAN_ID';

  FLD_CHANNEL_COUNT ='CHANNEL_COUNT';


{  FLD_HasRepeater          =   'HasRepeater';
  FLD_Repeater_Link1_ID    =   'Repeater_Link1_ID';
  FLD_Repeater_Link2_ID    =   'Repeater_Link2_ID';
  FLD_Repeater_Property_ID =   'Repeater_Property_ID';

}

//  FLD_HAS_REPEATER = 'HAS_REPEATER';   //TdmLinkLine
//  FLD_IS_Repeater = 'is_repeater';   //TdmLinkEnd

 // FLD_beenet = 'beenet';


  FLD_Manufacturer_name = 'Manufacturer_name';
  FLD_Equipment_name    = 'Equipment_name';

  

//==============================================================
implementation
//==============================================================


end.





 // sp_ColorSchema_Copy = 'sp_ColorSchema_Copy';

//  FLD_BEE_BEENET = 'BEE_BEENET';


//  FLD_Rx_Freq_MHz = 'Rx_Freq_MHz';
//  FLD_Tx_Freq_MHz = 'Tx_Freq_MHz';



//  SP_ANTENNA_ADD = 'SP_ANTENNA_ADD';

//  FLD_LOSS_dB = 'LOSS_dB';


{
  FLD__Link_ID        ='__Link_ID';
  FLD__NEXT_LINKEND_ID='__NEXT_LINKEND_ID';
}


