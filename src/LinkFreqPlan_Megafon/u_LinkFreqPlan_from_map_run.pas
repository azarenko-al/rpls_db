unit u_LinkFreqPlan_from_map_run;

interface


uses
  Classes, Forms, Menus, ActnList, IniFiles, SysUtils, Dialogs, u_assert,
  u_files,
  u_geo,
  u_func,
  u_vars,
  dm_Main,

  u_types,

  I_Shell,
  u_Shell_new,

  u_ini_LinkFreqPlan;  //shared


type
  TLinkFreqPlan_from_Map_run = class(TObject)
  private
    class procedure AddByGeom(aGeom_str: string);
  public
    class procedure AddByPoly(aBLPoints: TBLPointArrayF);
    class procedure AddByRect(aBLRect: TBLRect);
    class procedure AddByRing(aCenter: TBLPoint; aRadius_m: double);

  end;

implementation
const
  PROG_LinkFreqPlan = 'LinkFreqPlan.exe' ;


// ---------------------------------------------------------------
class procedure TLinkFreqPlan_from_Map_run.AddByGeom(aGeom_str: string);
// ---------------------------------------------------------------
const
  TEMP_INI_FILE = 'LinkFreqPlan.ini';
var
  i: Integer;
  iCode: Integer;
  oParams: TIni_LinkFreqPlan_Params;
  sFile: string;
  sFolderGUID: string;

begin

  sFile := g_ApplicationDataDir + TEMP_INI_FILE;

  oParams := TIni_LinkFreqPlan_Params.Create;
  oParams.Project_ID := dmMain.ProjectID;
  oParams.ConnectionString:=dmMain.GetConnectionString;

  oParams.Geometry_Str:=aGeom_str;

  oParams.SaveToFile(sFile);

  FreeAndNil(oParams);

  // -----------------------------
  RunApp(GetApplicationDir() + PROG_LinkFreqPlan, DoubleQuotedStr(sFile), iCode);

  // -----------------------------
  sFolderGUID:=g_Obj.ItemByName[OBJ_LINKNET].RootFolderGUID;
  g_Shell.UpdateNodeChildren_ByGUID (sFolderGUID);
  g_Shell.EXPAND_BY_GUID(sFolderGUID);

end;

// ---------------------------------------------------------------
class procedure TLinkFreqPlan_from_Map_run.AddByRing(aCenter: TBLPoint; aRadius_m: double);
// ---------------------------------------------------------------
const
  TEMP_INI_FILE = 'LinkFreqPlan.ini';
var
  i: Integer;
  iCode: Integer;
  oParams: TIni_LinkFreqPlan_Params;
  sFile: string;
  sFolderGUID: string;

begin
   DecimalSeparator:='.';

  sFile := g_ApplicationDataDir + TEMP_INI_FILE;

  oParams := TIni_LinkFreqPlan_Params.Create;
  oParams.Project_ID := dmMain.ProjectID;
  oParams.ConnectionString:=dmMain.GetConnectionString;

  oParams.Lat:=aCenter.B;
  oParams.Lon:=aCenter.L;
  oParams.Radius_m:=round(aRadius_m);

  oParams.SaveToFile(sFile);

  FreeAndNil(oParams);

  // -----------------------------
  RunApp(GetApplicationDir() + PROG_LinkFreqPlan, DoubleQuotedStr(sFile), iCode);

  // -----------------------------
  sFolderGUID:=g_Obj.ItemByName[OBJ_LINKNET].RootFolderGUID;
  g_Shell.UpdateNodeChildren_ByGUID (sFolderGUID);
  g_Shell.EXPAND_BY_GUID(sFolderGUID);

 // g_Shell.FocuseNodeByObjName (OBJ_ANTENNA_TYPE, iID);

end;


class procedure TLinkFreqPlan_from_Map_run.AddByPoly(aBLPoints: TBLPointArrayF);
var
  s: string;
begin
  s:=geo_BLPointArray_ToGeometry (aBLPoints);
  TLinkFreqPlan_from_Map_run.AddByGeom (s);
end;


class procedure TLinkFreqPlan_from_Map_run.AddByRect(aBLRect: TBLRect);
var
  s: string;
begin
  s:=geo_BLRect_ToGeometry (aBLRect);
  TLinkFreqPlan_from_Map_run.AddByGeom (s);
end;




end.

{

  if FFolderID>0 then
    sFolderGUID:=dmFolder.GetGUIDByID(FFolderID)
  else
//  if aFolderID=0 then
    sFolderGUID:=g_Obj.ItemByName[OBJ_LINK].RootFolderGUID;


  g_Shell.UpdateNodeChildren_ByGUID (sFolderGUID);
  g_Shell.EXPAND_BY_GUID(sFolderGUID);

  g_Shell.FocuseNodeByObjName (OBJ_LINK, FID);
