unit u_config_classes;

interface

uses Classes, SysUtils,

     xml_Config;

type
  TItemList = class;

  // -------------------------------------------------------------------
  TItem = class(TCollectionItem)
  // -------------------------------------------------------------------
  public
    Caption: string;
    Name_: string;

    Items: TItemList;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;

  end;


  TItemList = class(TCollection)
  private
    function GetItems(Index: Integer): TItem;
  public
    constructor Create;

    function AddItem(aCaption, aName: string): TItem;
    function Add: TItem;
    property Items[Index: Integer]: TItem read GetItems; default;

  end;



  TXMLConfig = class
  public
    Project: TItemList;
    Library_: TItemList;

    constructor Create;
    destructor Destroy; override;

    procedure LoadFromFile(aFileName: string);
  end;



implementation

constructor TItemList.Create;
begin
  inherited Create (TItem);
end;

function TItemList.AddItem(aCaption, aName: string): TItem;
begin
  Result := TItem(inherited Add);

  Result.Caption:=aCaption;
  Result.Name_   :=aName;
end;

function TItemList.Add: TItem;
begin
  Result := TItem(inherited Add);
end;

function TItemList.GetItems(Index: Integer): TItem;
begin
  Result := TItem(inherited Items[Index]);
end;

constructor TItem.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  Items := TItemList.Create();
end;

destructor TItem.Destroy;
begin
  FreeAndNil(Items);
  inherited Destroy;
end;


constructor TXMLConfig.Create;
begin
  inherited;
  Project := TItemList.Create();
  Library_ := TItemList.Create();
end;

destructor TXMLConfig.Destroy;
begin
  FreeAndNil(Library_);
  FreeAndNil(Project);
  inherited Destroy;
end;


procedure TXMLConfig.LoadFromFile(aFileName: string);


  procedure DoInsertItem (AItem: TItem; aVItem: IXMLItemType);
  var
    i: Integer;
  begin
    AItem.Name_  :=aVItem.Name;
    AItem.Caption:=aVItem.Caption;

    for I := 0 to aVItem.Count - 1 do
      DoInsertItem (AItem.Items.Add, aVItem.Item[i]);
  end;     

var
  I: Integer;
  vDoc: IXMLDocumentType;

begin
  Project.Clear;
  Library_.Clear;

  vDoc:=xml_Config.LoadDocument(aFileName);

  for I := 0 to vDoc.library_.Count - 1 do
    DoInsertItem (Library_.Add, vDoc.Library_[i]);

  for I := 0 to vDoc.Project.Count - 1 do
    DoInsertItem (Project.Add, vDoc.Project[i]);


end;


var
  oParams: TXMLConfig;

begin
 { Application.Initialize;


  oParams:=TXMLConfig.Create;
  oParams.LoadFromFile('w:\lib.xml');

}

end.
