
{*****************************************}
{                                         }
{            XML Data Binding             }
{                                         }
{         Generated on: 05.09.2010 10:28:06 }
{       Generated from: W:\lib.xml        }
{   Settings stored in: W:\lib.xdb        }
{                                         }
{*****************************************}

unit xml_Config;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLDocumentType = interface;
  IXMLLibraryType = interface;
  IXMLItemType = interface;
  IXMLProjectType = interface;

{ IXMLDocumentType }

  IXMLDocumentType = interface(IXMLNode)
    ['{A2BDCA54-FFED-4C5D-9E9E-27EDD19E957D}']
    { Property Accessors }
    function Get_Library_: IXMLLibraryType;
    function Get_Project: IXMLProjectType;
    { Methods & Properties }
    property Library_: IXMLLibraryType read Get_Library_;
    property Project: IXMLProjectType read Get_Project;
  end;

{ IXMLLibraryType }

  IXMLLibraryType = interface(IXMLNodeCollection)
    ['{F3DC8233-11F1-478F-8083-4A63720B525E}']
    { Property Accessors }
    function Get_Caption: WideString;
    function Get_Item(Index: Integer): IXMLItemType;
    procedure Set_Caption(Value: WideString);
    { Methods & Properties }
    function Add: IXMLItemType;
    function Insert(const Index: Integer): IXMLItemType;
    property Caption: WideString read Get_Caption write Set_Caption;
    property Item[Index: Integer]: IXMLItemType read Get_Item; default;
  end;

{ IXMLItemType }

  IXMLItemType = interface(IXMLNodeCollection)
    ['{EEF709AB-291D-43C9-9FB1-232A8778AE96}']
    { Property Accessors }
    function Get_Name: WideString;
    function Get_Caption: WideString;
    function Get_Item(Index: Integer): IXMLItemType;
    procedure Set_Name(Value: WideString);
    procedure Set_Caption(Value: WideString);
    { Methods & Properties }
    function Add: IXMLItemType;
    function Insert(const Index: Integer): IXMLItemType;
    property Name: WideString read Get_Name write Set_Name;
    property Caption: WideString read Get_Caption write Set_Caption;
    property Item[Index: Integer]: IXMLItemType read Get_Item; default;
  end;

{ IXMLProjectType }

  IXMLProjectType = interface(IXMLNodeCollection)
    ['{E8FDB181-DCC0-466D-8457-CFCEDD1DA85B}']
    { Property Accessors }
    function Get_Name: WideString;
    function Get_Caption: WideString;
    function Get_Item(Index: Integer): IXMLItemType;
    procedure Set_Name(Value: WideString);
    procedure Set_Caption(Value: WideString);
    { Methods & Properties }
    function Add: IXMLItemType;
    function Insert(const Index: Integer): IXMLItemType;
    property Name: WideString read Get_Name write Set_Name;
    property Caption: WideString read Get_Caption write Set_Caption;
    property Item[Index: Integer]: IXMLItemType read Get_Item; default;
  end;

{ Forward Decls }

  TXMLDocumentType = class;
  TXMLLibraryType = class;
  TXMLItemType = class;
  TXMLProjectType = class;

{ TXMLDocumentType }

  TXMLDocumentType = class(TXMLNode, IXMLDocumentType)
  protected
    { IXMLDocumentType }
    function Get_Library_: IXMLLibraryType;
    function Get_Project: IXMLProjectType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLLibraryType }

  TXMLLibraryType = class(TXMLNodeCollection, IXMLLibraryType)
  protected
    { IXMLLibraryType }
    function Get_Caption: WideString;
    function Get_Item(Index: Integer): IXMLItemType;
    procedure Set_Caption(Value: WideString);
    function Add: IXMLItemType;
    function Insert(const Index: Integer): IXMLItemType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLItemType }

  TXMLItemType = class(TXMLNodeCollection, IXMLItemType)
  protected
    { IXMLItemType }
    function Get_Name: WideString;
    function Get_Caption: WideString;
    function Get_Item(Index: Integer): IXMLItemType;
    procedure Set_Name(Value: WideString);
    procedure Set_Caption(Value: WideString);
    function Add: IXMLItemType;
    function Insert(const Index: Integer): IXMLItemType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLProjectType }

  TXMLProjectType = class(TXMLNodeCollection, IXMLProjectType)
  protected
    { IXMLProjectType }
    function Get_Name: WideString;
    function Get_Caption: WideString;
    function Get_Item(Index: Integer): IXMLItemType;
    procedure Set_Name(Value: WideString);
    procedure Set_Caption(Value: WideString);
    function Add: IXMLItemType;
    function Insert(const Index: Integer): IXMLItemType;
  public
    procedure AfterConstruction; override;
  end;

{ Global Functions }

function GetDocument(Doc: IXMLDocument): IXMLDocumentType;
function LoadDocument(const FileName: WideString): IXMLDocumentType;
function NewDocument: IXMLDocumentType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetDocument(Doc: IXMLDocument): IXMLDocumentType;
begin
  Result := Doc.GetDocBinding('Document', TXMLDocumentType, TargetNamespace) as IXMLDocumentType;
end;

function LoadDocument(const FileName: WideString): IXMLDocumentType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Document', TXMLDocumentType, TargetNamespace) as IXMLDocumentType;
end;

function NewDocument: IXMLDocumentType;
begin
  Result := NewXMLDocument.GetDocBinding('Document', TXMLDocumentType, TargetNamespace) as IXMLDocumentType;
end;

{ TXMLDocumentType }

procedure TXMLDocumentType.AfterConstruction;
begin
  RegisterChildNode('Library', TXMLLibraryType);
  RegisterChildNode('Project', TXMLProjectType);
  inherited;
end;

function TXMLDocumentType.Get_Library_: IXMLLibraryType;
begin
  Result := ChildNodes['Library'] as IXMLLibraryType;
end;

function TXMLDocumentType.Get_Project: IXMLProjectType;
begin
  Result := ChildNodes['Project'] as IXMLProjectType;
end;

{ TXMLLibraryType }

procedure TXMLLibraryType.AfterConstruction;
begin
  RegisterChildNode('Item', TXMLItemType);
  ItemTag := 'Item';
  ItemInterface := IXMLItemType;
  inherited;
end;

function TXMLLibraryType.Get_Caption: WideString;
begin
  Result := AttributeNodes['caption'].Text;
end;

procedure TXMLLibraryType.Set_Caption(Value: WideString);
begin
  SetAttribute('caption', Value);
end;

function TXMLLibraryType.Get_Item(Index: Integer): IXMLItemType;
begin
  Result := List[Index] as IXMLItemType;
end;

function TXMLLibraryType.Add: IXMLItemType;
begin
  Result := AddItem(-1) as IXMLItemType;
end;

function TXMLLibraryType.Insert(const Index: Integer): IXMLItemType;
begin
  Result := AddItem(Index) as IXMLItemType;
end;

{ TXMLItemType }

procedure TXMLItemType.AfterConstruction;
begin
  RegisterChildNode('Item', TXMLItemType);
  ItemTag := 'Item';
  ItemInterface := IXMLItemType;
  inherited;
end;

function TXMLItemType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLItemType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

function TXMLItemType.Get_Caption: WideString;
begin
  Result := AttributeNodes['caption'].Text;
end;

procedure TXMLItemType.Set_Caption(Value: WideString);
begin
  SetAttribute('caption', Value);
end;

function TXMLItemType.Get_Item(Index: Integer): IXMLItemType;
begin
  Result := List[Index] as IXMLItemType;
end;

function TXMLItemType.Add: IXMLItemType;
begin
  Result := AddItem(-1) as IXMLItemType;
end;

function TXMLItemType.Insert(const Index: Integer): IXMLItemType;
begin
  Result := AddItem(Index) as IXMLItemType;
end;

{ TXMLProjectType }

procedure TXMLProjectType.AfterConstruction;
begin
  RegisterChildNode('Item', TXMLItemType);
  ItemTag := 'Item';
  ItemInterface := IXMLItemType;
  inherited;
end;

function TXMLProjectType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLProjectType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

function TXMLProjectType.Get_Caption: WideString;
begin
  Result := AttributeNodes['caption'].Text;
end;

procedure TXMLProjectType.Set_Caption(Value: WideString);
begin
  SetAttribute('caption', Value);
end;

function TXMLProjectType.Get_Item(Index: Integer): IXMLItemType;
begin
  Result := List[Index] as IXMLItemType;
end;

function TXMLProjectType.Add: IXMLItemType;
begin
  Result := AddItem(-1) as IXMLItemType;
end;

function TXMLProjectType.Insert(const Index: Integer): IXMLItemType;
begin
  Result := AddItem(Index) as IXMLItemType;
end;

end.