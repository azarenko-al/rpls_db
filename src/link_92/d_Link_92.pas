unit d_Link_92;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, rxPlacemnt, ActnList, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, DB, ADODB;

(*

type
  TLinkLineNormRec = record
    Name_    : string;      // ������� ��� ������
    Length_KM: integer;     // ����� ������, ��
    SESR     : double;      // ���TSESR.��, %
    KNG      : double;      // ������.��, %
    Comment  : string;      // ������������� ����������� �������� ��� �������� �����
    NamePr92 : integer;
    Param_C  : double;
  end;

  TLinkCSTTypeRec = record
    Bitrate_Mbps : double;   //speed
    Param_A_ESR  : double;
    Param_A_BBER : double;
  end;*)


type
  Tdlg_Link_92 = class(TForm)
    FormPlacement1: TFormPlacement;
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    ActionList1: TActionList;
    act_Ok: TAction;
    act_Cancel: TAction;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    ADOQuery1: TADOQuery;
    ADOStoredProc1: TADOStoredProc;
    DataSource1: TDataSource;
    procedure FormCreate(Sender: TObject);
  private

  public
    class function ExecDlg: boolean;

  end;


implementation

{$R *.dfm}

// ---------------------------------------------------------------
class function Tdlg_Link_92.ExecDlg: boolean;
// ---------------------------------------------------------------
begin
  with Tdlg_Link_92.Create(Application) do
  begin

    ShowModal;

    Free;
  end;     
end;



procedure Tdlg_Link_92.FormCreate(Sender: TObject);
begin
  cxGrid1.Align := alClient;
end;


end.




type
  TLinkLineNormRec = record
    Name_    : string;      // ������� ��� ������
    Length_KM: integer;     // ����� ������, ��
    SESR     : double;      // ���TSESR.��, %
    KNG      : double;      // ������.��, %
    Comment  : string;      // ������������� ����������� �������� ��� �������� �����
    NamePr92 : integer;
    Param_C  : double;
  end;

  TLinkCSTTypeRec = record
    Bitrate_Mbps : double;   //speed
    Param_A_ESR  : double;
    Param_A_BBER : double;
  end;


const

  LINK_CST_TYPE_ARRAY: array [0..4] of TLinkCSTTypeRec =
    (
     (Bitrate_Mbps: 0.064;
      Param_A_ESR:  0.08;
      Param_A_BBER: 0
     ),
     (Bitrate_Mbps: 2.048;
      Param_A_ESR:  0.04;
      Param_A_BBER: 0.0003
     ),
     (Bitrate_Mbps: 8.448;
      Param_A_ESR:  0.05;
      Param_A_BBER: 0.0002
     ),
     (Bitrate_Mbps: 34.368;
      Param_A_ESR:  0.075;
      Param_A_BBER: 0.0002
     ),
     (Bitrate_Mbps: 139.264;
      Param_A_ESR:  0.16;
      Param_A_BBER: 0.0002
     )
    );

  LINKLINE_NORM_ARR1: array [0..7] of TLinkLineNormRec =
    (
     //0
     (Name_:    '-���������-';
      Length_KM:  600;
      SESR:     0.025;
      KNG:     0.05;
      Comment: '��������������� R ���  R >= 600 �� '+
               '���������� �� ����� ��� 200 < R < 600 ��';
      NamePr92: 2;
      Param_C: 0.075
     ),

     //1
     (Name_:    '������������� ������� (�� 12.500 km)';
      Length_KM:  12500;
      SESR:     0.07;
      KNG:     1.5;
      Comment: 'R >= 2500 �����.�� ��������������� RTSESR.��=0.05+0.02* R / 12500';
      NamePr92: 1;
      Param_C: 0.2
     ),

     //2
     (Name_:    '������������� ���� (�� 2500 km)';
      Length_KM:  2500;
      SESR:     0.054;
      KNG:     0.3;
      Comment: '��������������� R ���  R >= 50 ��';
      NamePr92: 1;
      Param_C: 0.04
     ),

     //3
     (Name_:    '������������� ���� (�� 600 km)';
      Length_KM:  600;
      SESR:     0.025;
      KNG:     0.05;
      Comment: '��������������� R ���  R >= 600 �� '+
               '���������� �� ����� ��� 200 < R < 600 ��';
      NamePr92: 2;
      Param_C: 0.075
     ),

     //4
     (Name_:    '������������� ���� (�� 200 km)';
      Length_KM:  200;
      SESR:     0.025;
      KNG:     0.05;
      Comment: '��������������� R ���  50 < R < 200 ��';
      NamePr92: 2;
      Param_C: 0.025
     ),

     //5
     (Name_:    '������������� ���� (�� 50 km)';
      Length_KM:  50;
      SESR:     0.006;
      KNG:     0.0125;
      Comment: '���������� �� ����� ��� R < 50 ��';
      NamePr92: 2;
      Param_C: 0.00625
     ),

     //6
     (Name_:    '������� ���� (�� 100 km)';
      Length_KM:  100;
      SESR:     0.015;
      KNG:     0.05;
      Comment: '���������� �� ����� ��� R < 100 ��';
      NamePr92: 3;
      Param_C: 0.075
     ),

     //7
     (Name_:    '���� �������';
      Length_KM:  50;
      SESR:     0.015;
      KNG:     0.05;
      Comment: '���������� �� �����';
      NamePr92: 4;
      Param_C: 0.15
     )

    );

implementation




