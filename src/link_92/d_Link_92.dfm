object dlg_Link_92: Tdlg_Link_92
  Left = 551
  Top = 428
  Width = 606
  Height = 345
  BorderWidth = 5
  Caption = 'dlg_Link_92'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pn_Buttons: TPanel
    Left = 0
    Top = 266
    Width = 588
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 0
    DesignSize = (
      588
      35)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 588
      Height = 2
      Align = alTop
      Shape = bsTopLine
    end
    object btn_Ok: TButton
      Left = 425
      Top = 8
      Width = 75
      Height = 23
      Action = act_Ok
      Anchors = [akTop, akRight]
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object btn_Cancel: TButton
      Left = 509
      Top = 8
      Width = 75
      Height = 23
      Action = act_Cancel
      Anchors = [akTop, akRight]
      Cancel = True
      TabOrder = 1
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 588
    Height = 129
    Align = alTop
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      OptionsView.GroupByBox = False
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    IniFileName = 'Software\Onega\'
    Left = 36
    Top = 204
  end
  object ActionList1: TActionList
    Left = 32
    Top = 156
    object act_Ok: TAction
      Category = 'Main'
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    end
    object act_Cancel: TAction
      Category = 'Main'
      Caption = #1054#1090#1084#1077#1085#1072
    end
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    Left = 224
    Top = 160
  end
  object ADOStoredProc1: TADOStoredProc
    Parameters = <>
    Left = 320
    Top = 160
  end
  object DataSource1: TDataSource
    Left = 408
    Top = 160
  end
end
