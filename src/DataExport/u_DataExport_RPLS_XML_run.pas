unit u_DataExport_RPLS_XML_run;

interface

uses
   SysUtils,

   I_Shell,

   
   
   u_func,

   dm_Main,

   u_classes,

   u_files,


   u_Vars,

   u_ini_Data_Export_RPLS_XML_params;  //shared


const
  // for action caption
  DEF_STR_Export_RPLS_XML = 'Export -> RPLS_XML';


type
  TDataExport_RPLS_XML_run = class(TObject)
  private
  public
    class procedure Export_Property_selected(aIDList: TIDList; aFolderID: Integer =
        0);
    class procedure Export_Link_selected(aIDList: TIDList; aFolderID: Integer = 0);

    class procedure Import_Property;

  end;


implementation

const
  TEMP_FILENAME = 'project_export_RPLS_XML.ini';

  PROG_data_export_RPLS_XML = 'data_export_RPLS_XML.exe';



//-------------------------------------------------------------------
class procedure TDataExport_RPLS_XML_run.Export_Property_selected(aIDList:
    TIDList; aFolderID: Integer = 0);
//-------------------------------------------------------------------
var
  sFile : string;

var
  iCode: Integer;
  oParams: TIni_Data_Export_RPLS_XML_Param;
begin
  sFile := g_ApplicationDataDir + TEMP_FILENAME;


  oParams:=TIni_Data_Export_RPLS_XML_Param.Create;

  oParams.ProjectID := dmMain.ProjectID;
  oParams.FolderID := aFolderID;
  oParams.ObjName := 'Property';
//  oParams.Mode     := mtExport_Selected;
 // oParams.TableName  :=aTableName;

  if Assigned(aIDList) then
    oParams.IDList.Assign(aIDList);

  oParams.SaveToIni(sFile);

  FreeAndNil(oParams);


  RunApp(GetApplicationDir()+ PROG_data_export_RPLS_XML, DoubleQuotedStr(sFile), iCode );

end;

//-------------------------------------------------------------------
class procedure TDataExport_RPLS_XML_run.Import_Property;
//-------------------------------------------------------------------
var
  sFile : string;

var
  iCode: Integer;
  oParams: TIni_Data_Export_RPLS_XML_Param;
begin
  sFile := g_ApplicationDataDir + TEMP_FILENAME;


  oParams:=TIni_Data_Export_RPLS_XML_Param.Create;

  oParams.ProjectID := dmMain.ProjectID;
//  oParams.FolderID := aFolderID;
  oParams.ObjName := 'Property';

  oParams.Action := 'import';
//  oParams.Mode     := mtExport_Selected;
 // oParams.TableName  :=aTableName;

(*  if Assigned(aIDList) then
    oParams.IDList.Assign(aIDList);
*)
  oParams.SaveToIni(sFile);

  FreeAndNil(oParams);


  RunApp(GetApplicationDir()+ PROG_data_export_RPLS_XML, DoubleQuotedStr(sFile), iCode );


//  PostEvent (WE_PROJECT_CHANGED_);
//  g_ShellEvents.PostEvent(WE__EXPLORER_ReLoad_Project);
  g_ShellEvents.EXPLORER_ReLoad_Project;


  ////////

end;



//-------------------------------------------------------------------
class procedure TDataExport_RPLS_XML_run.Export_Link_selected(aIDList: TIDList;
    aFolderID: Integer = 0);
//-------------------------------------------------------------------
var
  sFile : string;

var
  iCode: Integer;
  oParams: TIni_Data_Export_RPLS_XML_Param;
  s: string;
begin
  sFile := g_ApplicationDataDir + TEMP_FILENAME;


  oParams:=TIni_Data_Export_RPLS_XML_Param.Create;

  oParams.ProjectID := dmMain.ProjectID;
  oParams.FolderID := aFolderID;

  oParams.ObjName := 'Link';

//  oParams.Mode     := mtExport_Selected;
 // oParams.TableName  :=aTableName;

  if Assigned(aIDList) then
    oParams.IDList.Assign(aIDList);

  oParams.SaveToIni(sFile);

  FreeAndNil(oParams);

  s:=GetApplicationDir()+ PROG_data_export_RPLS_XML;

  if not FileExists(s) then
    raise Exception.Create('');

  RunApp(s, DoubleQuotedStr(sFile), iCode);

end;




end.
