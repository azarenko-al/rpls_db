unit u_DataExport_RPLS_DB_run;

interface

uses
  Classes, Forms, Menus, ActnList, IniFiles,  SysUtils, Dialogs,

  u_files,
  u_const,
  u_func,

  dm_Main,

  u_classes,

  u_reg,

  u_Vars,

  u_ini_Data_Export_RPLS_XML_params;  //shared


const
  // for action caption
  DEF_STR_Export_MDB = 'Export -> MDB';


type
  TDataExport_RPLS_XML_run = class(TObject)
  private

  public
    class procedure ExportToRplsDb_selected(aTableName: string; aIDList: TIDList);

    class procedure ExportToRplsDbGuides;
    class procedure ExportToRplsDbProject1;

    class procedure ImportFromRplsDbGuides;
    class procedure ImportFromRplsDbProject;

  end;


implementation

const
  TEMP_FILENAME = 'project_export.ini';




//-------------------------------------------------------------------
class procedure TDataExport_RPLS_XML_run.ExportToRplsDbGuides;
//-------------------------------------------------------------------
var
  sFile : string;

var
  oParams: TIni_Data_export_import_params;
begin
  sFile := g_ApplicationDataDir + TEMP_FILENAME;


  oParams:=TIni_Data_export_import_params.Create;

  oParams.ProjectID := dmMain.ProjectID;
  oParams.Mode     := mtExport_GUIDES;

  oParams.SaveToIni(sFile);

  FreeAndNil(oParams);


  ShellExec(GetApplicationDir()+ PROG_data_export_import, DoubleQuotedStr(sFile));

end;

//-------------------------------------------------------------------
class procedure TDataExport_RPLS_XML_run.ExportToRplsDb_selected(aTableName: string;
    aIDList: TIDList);
//-------------------------------------------------------------------
var
  sFile : string;

var
  oParams: TIni_Data_export_import_params;
begin
  sFile := g_ApplicationDataDir + TEMP_FILENAME;


  oParams:=TIni_Data_export_import_params.Create;

  oParams.ProjectID := dmMain.ProjectID;
  oParams.Mode     := mtExport_Selected;
  oParams.TableName  :=aTableName;

  oParams.SelectedIDList.Assign(aIDList);

  oParams.SaveToIni(sFile);

  FreeAndNil(oParams);


  ShellExec(GetApplicationDir()+ PROG_data_export_import, DoubleQuotedStr(sFile));

end;

// -------------------------------------------------------------------
// Export
// -------------------------------------------------------------------

//-------------------------------------------------------------------
class procedure TDataExport_RPLS_XML_run.ExportToRplsDbProject1;
//-------------------------------------------------------------------
var
  oParams: TIni_Data_export_import_params;
  
  oIni: TIniFile;
  sIniFile : string;
begin
  sIniFile := g_ApplicationDataDir + TEMP_FILENAME;

  oParams:=TIni_Data_export_import_params.Create;
  oParams.ProjectID := dmMain.ProjectID;
  oParams.Mode     := mtExport_PROJECT;

  oParams.SaveToIni(sIniFile);

  FreeAndNil(oParams);


  ShellExec(GetApplicationDir()+PROG_data_export_import, DoubleQuotedStr(sIniFile));

end;

//-------------------------------------------------------------------
class procedure TDataExport_RPLS_XML_run.ImportFromRplsDbGuides;
//-------------------------------------------------------------------
var
  i: Integer;
  sFile : string;    
  oParams: TIni_Data_export_import_params;

begin
  sFile := g_ApplicationDataDir + TEMP_FILENAME;

  oParams:=TIni_Data_export_import_params.Create;
  oParams.ProjectID := dmMain.ProjectID;
  oParams.Mode     := mtImport_Guides;

  oParams.SaveToIni(sFile);

  FreeAndNil(oParams);


  RunApp (GetApplicationDir()+PROG_data_export_import, DoubleQuotedStr(sFile)); // then       //    IntToStr(dmMain.ProjectID)


end;

//-------------------------------------------------------------------
class procedure TDataExport_RPLS_XML_run.ImportFromRplsDbProject;
//-------------------------------------------------------------------
var
  sFile : string;

  oParams: TIni_Data_export_import_params;
  
begin
  sFile := g_ApplicationDataDir + TEMP_FILENAME;


//  sFile := g_ApplicationDataDir + TEMP_FILENAME;

  oParams:=TIni_Data_export_import_params.Create;
  oParams.ProjectID := dmMain.ProjectID;
  oParams.Mode     := mtImport_project;

  oParams.SaveToIni(sFile);

  FreeAndNil(oParams);

  // -------------------------

  ShellExec (GetApplicationDir()+ PROG_data_export_import, DoubleQuotedStr(sFile));

end;




end.
