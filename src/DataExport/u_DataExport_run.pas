unit u_DataExport_run;

interface

{$DEFINE use_dll___}

uses
  Classes, Forms, Menus, ActnList, IniFiles,  SysUtils, Dialogs,

  u_ini_Excel_to_DB_params,

  u_files,

  u_const,
  u_func,

//  I_data_export_import,

  dm_Main,

  u_classes,

  u_reg,

  u_Vars,

  u_ini_data_export_import_params;  //shared


const
  // for action caption
  DEF_STR_Export_MDB = '������� � MDB';
  DEF_STR_Import_MDB = '������ �� MDB';


type
  TDataExport_run = class(TObject)
  private
  public
    class procedure ExportToRplsDb_selected(aTableName: string; aIDList: TIDList);

    class procedure ExportToRplsDbGuides;
    class procedure ExportToRplsDbProject1;

    class procedure ImportFromRplsDbGuides;
    class procedure ImportFromRplsDbProject;
    class procedure ImportFromExcel;

    class procedure ImportKML;


  end;


implementation

const
  TEMP_FILENAME = 'project_export.ini';


// ---------------------------------------------------------------
procedure RunProg_data_export_import(aFileName: string);
// ---------------------------------------------------------------
var
  iCode: Integer;
begin
  {$IFDEF use_dll}

  {
    if Load_IData_export_import() then
    begin
      IData_export_import.InitADO(dmMain.ADOConnection1.ConnectionObject);
   //   ILink_graph.InitAppHandle(Application.Handle);
      IData_export_import.Execute(aFileName);

      UnLoad_IData_export_import();
    end;
   }

   
  {$ELSE}
    RunApp(GetApplicationDir()+ PROG_data_export_import, DoubleQuotedStr(aFileName), iCode );

//    ShellExec(GetApplicationDir()+ PROG_data_export_import, DoubleQuotedStr(aFileName));
  {$ENDIF}
end;


//-------------------------------------------------------------------
class procedure TDataExport_run.ExportToRplsDbGuides;
//-------------------------------------------------------------------
var
  sFile : string;

var
  oParams: TIni_Data_export_import_params;
begin
  sFile := g_ApplicationDataDir + TEMP_FILENAME;


  oParams:=TIni_Data_export_import_params.Create;

  oParams.ProjectID := dmMain.ProjectID;
  oParams.Mode     := mtExport_GUIDES;

  oParams.ConnectionString:=dmMain.GetConnectionString;

  oParams.SaveToIni(sFile);

  FreeAndNil(oParams);


  RunProg_data_export_import (sFile);

//
//  {$IFDEF use_dll}
//
//    if Load_IData_export_import() then
//    begin
//      IData_export_import.InitADO(dmMain.ADOConnection1.ConnectionObject);
//   //   ILink_graph.InitAppHandle(Application.Handle);
//      IData_export_import.Execute(sFile);
//
//      UnLoad_IData_export_import();
//    end;
//
//  {$ELSE}
//    ShellExec(GetApplicationDir()+ PROG_data_export_import, DoubleQuotedStr(sFile));
//  {$ENDIF}
//
//
//  // -------------------------

end;

//-------------------------------------------------------------------
class procedure TDataExport_run.ExportToRplsDb_selected(aTableName: string; aIDList: TIDList);
//-------------------------------------------------------------------
var
  sFile : string;

var
  oParams: TIni_Data_export_import_params;
begin
  Assert (aIDList.Count>0);

  sFile := g_ApplicationDataDir + TEMP_FILENAME;


  oParams:=TIni_Data_export_import_params.Create;


  oParams.ConnectionString := dmMain.GetConnectionString();

  if aIDList.Items[0].IsFolder then
  begin
    oParams.Folder_ID:=aIDList.Items[0].ID;
    oParams.IsFolder1 :=true;
  end;

  oParams.ProjectID := dmMain.ProjectID;
  oParams.Mode     := mtExport_Selected;
  oParams.TableName  :=aTableName;

  oParams.SelectedIDList.Assign(aIDList);

  oParams.SaveToIni(sFile);

  FreeAndNil(oParams);

  RunProg_data_export_import (sFile);

//  ShellExec(GetApplicationDir()+ PROG_data_export_import, DoubleQuotedStr(sFile));


//
//
//  {$IFDEF use_dll}
//
//    if Load_IData_export_import() then
//    begin
//      IData_export_import.InitADO(dmMain.ADOConnection1.ConnectionObject);
//   //   ILink_graph.InitAppHandle(Application.Handle);
//      IData_export_import.Execute(sFile);
//
//      UnLoad_IData_export_import();
//    end;
//
//  {$ELSE}
//    ShellExec(GetApplicationDir()+ PROG_data_export_import, DoubleQuotedStr(sFile));
//  {$ENDIF}

end;

// -------------------------------------------------------------------
// Export
// -------------------------------------------------------------------

//-------------------------------------------------------------------
class procedure TDataExport_run.ExportToRplsDbProject1;
//-------------------------------------------------------------------
var
  oParams: TIni_Data_export_import_params;
  
  oIni: TIniFile;
  sIniFile : string;
begin
  sIniFile := g_ApplicationDataDir + TEMP_FILENAME;

  oParams:=TIni_Data_export_import_params.Create;
  oParams.ProjectID := dmMain.ProjectID;
  oParams.Mode     := mtExport_PROJECT;

  oParams.ConnectionString := dmMain.GetConnectionString();


  oParams.SaveToIni(sIniFile);

  FreeAndNil(oParams);


  RunProg_data_export_import (sIniFile);

//
//
//  {$IFDEF use_dll}
//
//    if Load_IData_export_import() then
//    begin
//      IData_export_import.InitADO(dmMain.ADOConnection1.ConnectionObject);
//   //   ILink_graph.InitAppHandle(Application.Handle);
//      IData_export_import.Execute(sFile);
//
//      UnLoad_IData_export_import();
//    end;
//
//  {$ELSE}
//    ShellExec(GetApplicationDir()+ PROG_data_export_import, DoubleQuotedStr(sFile));
//  {$ENDIF}
//
//
//
//  // -------------------------
////  ShellExec(GetApplicationDir()+PROG_data_export_import, DoubleQuotedStr(sIniFile));

end;

//-------------------------------------------------------------------
class procedure TDataExport_run.ImportFromExcel;
//-------------------------------------------------------------------
var
  iCode: Integer;
  sFile : string;
  oParams: TIni_Excel_to_DB_params;

begin
  sFile := g_ApplicationDataDir + TEMP_FILENAME;

//  sFile := g_ApplicationDataDir + TEMP_FILENAME;

  oParams:=TIni_Excel_to_DB_params.Create;
  oParams.Project_ID := dmMain.ProjectID;
  oParams.ConnectionString := dmMain.GetConnectionString();


  //oParams.Mode     := mtImport_project;

  oParams.SaveToIni(sFile);

  FreeAndNil(oParams);

  RunApp(GetApplicationDir()+ PROG_Excel_to_DB_exe, DoubleQuotedStr(sFile), iCode );


end;

//-------------------------------------------------------------------
class procedure TDataExport_run.ImportFromRplsDbGuides;
//-------------------------------------------------------------------
var
  i: Integer;
  sFile : string;    
  oParams: TIni_Data_export_import_params;

begin
  sFile := g_ApplicationDataDir + TEMP_FILENAME;

  oParams:=TIni_Data_export_import_params.Create;
  oParams.ProjectID := dmMain.ProjectID;
  oParams.Mode     := mtImport_Guides;

  oParams.ConnectionString:=dmMain.GetConnectionString;


  oParams.SaveToIni(sFile);

  FreeAndNil(oParams);

  RunProg_data_export_import (sFile);

//
//  // -------------------------
//
// // RunApp (GetApplicationDir()+PROG_data_export_import, DoubleQuotedStr(sFile)); // then       //    IntToStr(dmMain.ProjectID)
//
//  {$IFDEF use_dll}
//
//    if Load_IData_export_import() then
//    begin
//      IData_export_import.InitADO(dmMain.ADOConnection1.ConnectionObject);
//   //   ILink_graph.InitAppHandle(Application.Handle);
//      IData_export_import.Execute(sFile);
//
//      UnLoad_IData_export_import();
//    end;
//
//  {$ELSE}
//    ShellExec(GetApplicationDir()+ PROG_data_export_import, DoubleQuotedStr(sFile));
//  {$ENDIF}



end;

//-------------------------------------------------------------------
class procedure TDataExport_run.ImportFromRplsDbProject;
//-------------------------------------------------------------------
var
  sFile : string;

  oParams: TIni_Data_export_import_params;

begin
  sFile := g_ApplicationDataDir + TEMP_FILENAME;


//  sFile := g_ApplicationDataDir + TEMP_FILENAME;

  oParams:=TIni_Data_export_import_params.Create;
  oParams.ProjectID := dmMain.ProjectID;
  oParams.Mode     := mtImport_project;

  oParams.ConnectionString:=dmMain.GetConnectionString;


  oParams.SaveToIni(sFile);

  FreeAndNil(oParams);

  RunProg_data_export_import (sFile);

//
//
//  // -------------------------
//
// // ShellExec (GetApplicationDir()+ PROG_data_export_import, DoubleQuotedStr(sFile));
//
//  {$IFDEF use_dll}
//
//    if Load_IData_export_import() then
//    begin
//      IData_export_import.InitADO(dmMain.ADOConnection1.ConnectionObject);
//   //   ILink_graph.InitAppHandle(Application.Handle);
//      IData_export_import.Execute(sFile);
//
//      UnLoad_IData_export_import();
//    end;
//
//  {$ELSE}
//    ShellExec(GetApplicationDir()+ PROG_data_export_import, DoubleQuotedStr(sFile));
//  {$ENDIF}
//


end;

// ---------------------------------------------------------------
class procedure TDataExport_run.ImportKML;
// ---------------------------------------------------------------
var
  iCode: Integer;
  sFile : string;
  oParams: TIni_Data_export_import_params;

const
  PROG_KML_to_DB_exe = 'KML_to_DB.exe';


begin
  sFile := g_ApplicationDataDir + TEMP_FILENAME;

//  sFile := g_ApplicationDataDir + TEMP_FILENAME;

  oParams:=TIni_Data_export_import_params.Create;
  oParams.ProjectID := dmMain.ProjectID;
  //oParams.Mode     := mtImport_project;

  oParams.SaveToIni(sFile);

  FreeAndNil(oParams);

  RunApp(GetApplicationDir()+ PROG_KML_to_DB_exe, DoubleQuotedStr(sFile), iCode );


end;




end.
