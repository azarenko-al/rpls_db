unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, DB, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, dxmdaset, cxLookAndFeels,

  u_db,

  u_Mapinfo_WOR_classes, ExtCtrls, ADODB, OleCtrls, MapXLib_TLB, rxPlacemnt

  ;

type
  TForm1 = class(TForm)
    dxMemData1: TdxMemData;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    DataSource1: TDataSource;
    dxMemData1filename: TStringField;
    dxMemData1zoom_min: TIntegerField;
    dxMemData1zoom_max: TIntegerField;
    cxGrid1DBTableView1RecId: TcxGridDBColumn;
    cxGrid1DBTableView1filename: TcxGridDBColumn;
    cxGrid1DBTableView1zoom_min: TcxGridDBColumn;
    cxGrid1DBTableView1zoom_max: TcxGridDBColumn;
    Button1: TButton;
    cxLookAndFeelController1: TcxLookAndFeelController;
    dxMemData1is_zoomed: TBooleanField;
    cxGrid1DBTableView1is_zoomed: TcxGridDBColumn;
    Panel1: TPanel;
    Panel2: TPanel;
    Button2: TButton;
    Button3: TButton;
    Map1: TMap;
    FormPlacement1: TFormPlacement;
    Memo1: TMemo;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class function ExecDlg(aFileName: string): Integer;
                              
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}



// ---------------------------------------------------------------
class function TForm1.ExecDlg(aFileName: string): Integer;
// ---------------------------------------------------------------
begin
  with TForm1.Create(Application) do
  begin

    ShowModal;

    Free;
  end;


end;


procedure TForm1.FormCreate(Sender: TObject);
var
  i: Integer;
  oWorkspace: TWorkspace;
  oTable: TworTable;
  s: string;
  vLayer: CMapxLayer;

const

  FLD_ZOOM_MIN ='ZOOM_MIN';
  FLD_ZOOM_MAX ='ZOOM_MAX';
  FLD_IS_ZOOM  ='IS_ZOOM';

//  FLD_FILENAME = 'FILENAME';

begin
  Caption:='';

//  cxGrid1.Align:=alClient;


  Map1.Align:=alClient;


  Map1.MapUnit:=miUnitKilometer;

{
  const
  miUnitMile = $00000000;
  miUnitKilometer = $00000001;
  miUnitInch = $00000002;
  miUnitFoot = $00000003;
  miUnitYard = $00000004;
  miUnitMillimeter = $00000005;
  miUnitCentimeter = $00000006;
  miUnitMeter = $00000007;
  miUnitSurveyFoot = $00000008;
  miUnitNauticalMile = $00000009;
  miUnitTwip = $0000000A;
  miUnitPoint = $0000000B;
  miUnitPica = $0000000C;
  miUnitDegree = $0000000D;
  miUnitLink = $0000001E;
  miUnitChain = $0000001F;
  miUnitRod = $00000020;
}

//    Width 9.95833 Units "in" Height 4.875 Units "in"

//  Map1.


  oWorkspace:=TWorkspace.Create;
  oWorkspace.LoadFromFile('O:\_alex\_WOR\arhangelskaya_obl\arhangelskaya_obl.WOR');


  Memo1.Lines.Assign(oWorkspace.OrderedFileList);


  for i:=0 to oWorkspace.OrderedFileList.Count-1 do
  begin
    s:=oWorkspace.OrderedFileList[i];

    oTable:=oWorkspace.GetItemByName( s );

//    oTable.FileName

    Assert (Assigned(oTable));



      db_AddRecord_(dxMemData1,
      [
        FLD_FILENAME, oTable.FileName,

        FLD_ZOOM_MIN, oTable.ZoomMin,
        FLD_ZOOM_MAX, oTable.ZoomMax,

        FLD_IS_ZOOM,  oTable.IsZoom

      ]);


    vLayer:=Map1.Layers.Add(oTable.FileName,  EmptyParam);

    vLayer.ZoomLayer:=oTable.IsZoom;

    vLayer.ZoomMin:=oTable.ZoomMin;
    vLayer.ZoomMax:=oTable.ZoomMax;

  end;

end;

end.
