object Form1: TForm1
  Left = 478
  Top = 237
  Width = 1306
  Height = 535
  BorderWidth = 5
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 457
    Height = 344
    Align = alLeft
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1RecId: TcxGridDBColumn
        DataBinding.FieldName = 'RecId'
        Visible = False
      end
      object cxGrid1DBTableView1filename: TcxGridDBColumn
        DataBinding.FieldName = 'filename'
        Width = 197
      end
      object cxGrid1DBTableView1is_zoomed: TcxGridDBColumn
        DataBinding.FieldName = 'is_zoom'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssInactive
        Width = 65
      end
      object cxGrid1DBTableView1zoom_min: TcxGridDBColumn
        DataBinding.FieldName = 'zoom_min'
        Width = 95
      end
      object cxGrid1DBTableView1zoom_max: TcxGridDBColumn
        DataBinding.FieldName = 'zoom_max'
        Width = 80
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object Button1: TButton
    Left = 1160
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 1
  end
  object Panel1: TPanel
    Left = 0
    Top = 461
    Width = 1288
    Height = 36
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Panel2: TPanel
      Left = 1112
      Top = 0
      Width = 176
      Height = 36
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object Button2: TButton
        Left = 8
        Top = 8
        Width = 75
        Height = 25
        Caption = 'Ok'
        TabOrder = 0
      end
      object Button3: TButton
        Left = 90
        Top = 8
        Width = 75
        Height = 25
        Caption = #1054#1090#1084#1077#1085#1072
        TabOrder = 1
      end
    end
  end
  object Map1: TMap
    Left = 464
    Top = 0
    Width = 433
    Height = 337
    ParentColor = False
    TabOrder = 3
    ControlData = {
      2CA10700C02C0000D4220000010000000F0000FFFFFEFF0D470065006F004400
      69006300740069006F006E0061007200790000FFFEFF3745006D007000740079
      002000470065006F007300650074004E0061006D00650020007B003900410039
      00410043003200460034002D0038003300370035002D0034003400640031002D
      0042004300450042002D00340037003600410045003900380036004600310039
      0030007D00FFFEFF001E00FFFEFF000600010A000000000000FFFEFF00500001
      0300000001F4010000050000800C0000000014000000000002000E00EB030005
      00000100000000D4D0C8000000000000000001370000000000D4D0C800000000
      0000000352E30B918FCE119DE300AA004BB85101CC00009001DC7C0100054172
      69616C000352E30B918FCE119DE300AA004BB851010200009001A42C02000B4D
      61702053796D626F6C730000000000000001000100FFFFFF000200D4D0C80000
      0000000001000000010000FFFEFF3245006D0070007400790020005400690074
      006C00650020007B00300031004100390035003000340042002D004300450031
      0033002D0034003400310035002D0041003500410030002D0035003100440038
      00430032004600310035003200300034007D0000000000FFFFFF000100000000
      000000000000000000000000000000000000000352E30B918FCE119DE300AA00
      4BB85101CC00009001348C030005417269616C000352E30B918FCE119DE300AA
      004BB85101CC00009001348C030005417269616C000000000000000000000000
      00000000000000000000000000000000000000000000000000EAA04000000000
      000075400100000100001801000001000000FFFFFFFF1C000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000020000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008076C000000000008056C0000000000080764000000000008056400000
      00009CCB11101801000001000000FFFFFFFF1C00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000200
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000080
      76C000000000008056C000000000008076400000000000805640000000009CCB
      1110000000000000000000000000000000000000000000000000000000000000
      00000000000000000000}
  end
  object Memo1: TMemo
    Left = 0
    Top = 344
    Width = 1288
    Height = 117
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    TabOrder = 4
  end
  object dxMemData1: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 1072
    Top = 88
    object dxMemData1filename: TStringField
      FieldName = 'filename'
      Size = 200
    end
    object dxMemData1zoom_min: TIntegerField
      FieldName = 'zoom_min'
    end
    object dxMemData1zoom_max: TIntegerField
      FieldName = 'zoom_max'
    end
    object dxMemData1is_zoomed: TBooleanField
      FieldName = 'is_zoom'
    end
  end
  object DataSource1: TDataSource
    DataSet = dxMemData1
    Left = 1072
    Top = 144
  end
  object cxLookAndFeelController1: TcxLookAndFeelController
    Kind = lfFlat
    Left = 1072
    Top = 200
  end
  object FormPlacement1: TFormPlacement
    IniFileName = 'Software\Onega\'
    Left = 1072
    Top = 280
  end
end
