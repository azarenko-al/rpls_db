unit dm_Act_MapFile;

interface

uses
  Classes, Forms, Dialogs,  Menus, ActnList,

  u_shell_var,
  I_Object,
  //I_MapFile,

  dm_act_Base,


  dm_MapFile,

//  u_Explorer,

  u_func,

  
  u_func_msg,

  u_Types

//  PBFolderDialog
  ;

type
  TdmAct_MapFile = class(TdmAct_Base)  //, IMapFileX
    OpenDialog1: TOpenDialog;
    SaveDialog_XML: TSaveDialog;
    act_ExportToXML: TAction;
    act_ImportFromXML: TAction;
    act_Add_from_Dir: TAction;
    ActionList2: TActionList;
    act_Add_WOR: TAction;
//    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure act_FileOpen_WORAccept(Sender: TObject);
  private
//    FStrList: TStringList;

    procedure CheckActionsEnable;
//    procedure Del_List;


    procedure DoAction (Sender: TObject);

  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;

    function ItemDel(aID: integer; aName: string = ''): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;


    procedure DoOnDeleteList(Sender : TObject);


  public

    procedure Dlg_LoadMapsFromDir (aFolderID: integer);
    function Dlg_LoadMapsFromFiles: Boolean;

	  function  Del (aID: integer): boolean;


    class procedure Init;
  end;

var
  dmAct_MapFile: TdmAct_MapFile;

//============================================================
// implementation
//============================================================

implementation

//uses dm_User_Security;

{$R *.DFM}
 

//--------------------------------------------------------------------
class procedure TdmAct_MapFile.Init;
//--------------------------------------------------------------------
begin
  Assert(not Assigned(dmAct_MapFile));

  Application.CreateForm(TdmAct_MapFile, dmAct_MapFile);

end;

//---------------------------------------------------------
procedure TdmAct_MapFile.DataModuleCreate(Sender: TObject);
//---------------------------------------------------------
var b: Boolean;
begin
  inherited;
  ObjectName:=OBJ_MAP;

//  FStrList:=TStringList.create;

  OnDeleteList:=DoOnDeleteList;


  act_Add_from_Dir.Caption:='�������� �� �����';

  act_Add.Caption :='�������� �����';
  act_Add_WOR.Caption :='�������� WOR';

//  act_Add

//  TdmMapFile.Init;

//  act_Add.Caption:=STR_ADD;
//  act_Del.Caption:=STR_DEl;
//  act_ExportToxml.Caption:='������� � XML';
 // act_ImportFromxml.Caption:='������ �� XML';

  SetActionsExecuteProc
     ([//act_Add,act_Del,
       act_Add_from_Dir,
       act_Add_WOR
     //  act_Add_item

      // act_ExportToXML,act_ImportFromXML
       ],
      DoAction);

  SetCheckedActionsArr([

        act_Add,
        act_Del_,
        act_Del_List,

        act_Add_from_Dir,

        act_Add_WOR

     ]);

end;



//--------------------------------------------------------------------
procedure TdmAct_MapFile.CheckActionsEnable;
//--------------------------------------------------------------------
begin


(*
  SetActionsEnabled
    ([
        act_Add,
        act_Del_,
        act_Del_List,

        act_Add_from_Dir

     ],
     not dmUser_Security.ProjectIsReadOnly);
*)
end;


//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_MapFile.DoAction(Sender: TObject);
begin
//  if Sender=act_Add then Add (FFocusedID) else
//  if Sender=act_Del then Del (FFocusedID) else

//    dmProperty_export.ExportToRPLS_XML ('');//, FSelectedIDList);
//    TProperty_run.Export_Google (FSelectedIDList) else




  if Sender = act_Add_WOR then
    dmMapFile.Dlg_Add_WOR (FFocusedID) else

//    Dlg_LoadMapsFromDir(FFocusedID) else

  if Sender = act_Add_from_Dir then
    Dlg_LoadMapsFromDir(FFocusedID) else

//  if Sender=act_ExportToXML then ExportToXML () else
//  if Sender=act_ImportFromXML then ImportFromXML (FFocusedID) else

end;

//--------------------------------------------------------------------
function TdmAct_MapFile.ItemDel(aID: integer; aName: string = ''): boolean;
//--------------------------------------------------------------------
begin
  Result:=dmMapFile.Del (aID);

  //FStrList

end;


function TdmAct_MapFile.Del (aID: integer): boolean;
begin
  Result := dmMapFile.Del(aID);

end;


procedure TdmAct_MapFile.DoOnDeleteList(Sender : TObject);
//var
//  I: Integer;
begin
  g_EventManager.PostEvent_(et_DEL_MAP, ['idlist', Integer(FDeletedIDList)] );
 // PostUserMessage (WM__DEL_MAP, Integer(FDeletedIDList));

//   ShowMessage('DoOnDeleteList');
end;



//--------------------------------------------------------------------
function TdmAct_MapFile.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  if dmMapFile.Dlg_Add(aFolderID) then
  begin
  //   PostMessage (Application.Handle, WE_PROJECT_UPDATE_MAP_LIST,0,0);

     g_EventManager.PostEvent_(et_Explorer_REFRESH_MAP_FILES, []);

//     PostMessage (Application.Handle, WM_Explorer_REFRESH_MAP_FILES,0,0);

    //   PostUserMessage(WM__ADD_MAP,  Integer( aList));

  //  IMainProjectExplorer.UPDATE_NODE_CHILDREN(GUID_REL_MATRIX);


  //   PostEvent(WE_PROJECT_UPDATE_MAP_LIST);

//     IMainProjectExplorer.

   //  IMainProjectExplorer.UPDATE_NODE_CHILDREN(GUID_MAP);

  end;

end;



//-------------------------------------------------------------------
procedure TdmAct_MapFile.Dlg_LoadMapsFromDir (aFolderID: integer);
//-------------------------------------------------------------------
{var
   oDialog: TPBFolderDialog;
   s: string;}
begin
  if dmMapFile.Dlg_LoadMapsFromDir(aFolderID) then
  begin
  //  PostEvent(WE_PROJECT_UPDATE_MAP_LIST);

    g_EventManager.PostEvent_(et_Explorer_REFRESH_MAP_FILES, []);

//    PostMessage (Application.Handle, WM_Explorer_REFRESH_MAP_FILES,0,0);


    IMainProjectExplorer.UPDATE_NODE_CHILDREN(GUID_MAP);

  end;


{
  oDialog:=TPBFolderDialog.Create(Application);
//  oDialog:=TPBFolderDialog.Create(Application.MainForm);
 // oDialog.RootFolder:= foMyComputer;

  if oDialog.Execute then
    s := oDialog.Folder
  else
    s:='';

  oDialog.Free;

  if s<>'' then begin
    Screen.Cursor:= crHourGlass;
    dmMapFile.AddFromDir (s);
    PostEvent(WE_PROJECT_UPDATE_MAP_LIST);
    Screen.Cursor:= crDefault;
  end;}
end;




//-------------------------------------------------------------------
function TdmAct_MapFile.Dlg_LoadMapsFromFiles: Boolean;
//-------------------------------------------------------------------
//var
 // OpenDialog: TOpenDialog;
begin
  Result := dmMapFile.Dlg_LoadMapsFromFiles;

  if Result then
  begin
 //   PostEvent(WE_PROJECT_UPDATE_MAP_LIST);

    g_EventManager.PostEvent_(et_Explorer_REFRESH_MAP_FILES, []);
//    PostMessage (Application.Handle, WM_Explorer_REFRESH_MAP_FILES,0,0);

//    IMainProjectExplorer.ExpandByGUID(GUID_PROJECT);
    IMainProjectExplorer.ExpandByGUID(GUID_MAP);

  end;


end;

procedure TdmAct_MapFile.act_FileOpen_WORAccept(Sender: TObject);
begin

  //acr_Add_from_WOR

end;


//--------------------------------------------------------------------
procedure TdmAct_MapFile.GetPopupMenu;
//--------------------------------------------------------------------
begin
  CheckActionsEnable();

  case aMenuType of
    mtFolder: begin
                //  AddMenuItem (aPopupMenu, act_Add_Item);

                  AddFolderMenu_Create (aPopupMenu);

                  AddFolderPopupMenu (aPopupMenu);

                //  AddMenuItem (aPopupMenu, nil);
               //   AddMenuItem (aPopupMenu, act_ExportToXML);
               //   AddMenuItem (aPopupMenu, act_ImportFromXML);
                  act_ExportToxml.Enabled:=FFolderID = 0;

                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Add_from_Dir);

                AddMenuItem (aPopupMenu, act_Add_WOR);
              end;

    mtItem:   begin
                AddMenuItem (aPopupMenu, act_Del_);
                AddMenuItem (aPopupMenu, nil);
                AddFolderMenu_Tools (aPopupMenu);


              end;

    mtList:   begin
                AddMenuItem (aPopupMenu, act_Del_list);
                AddMenuItem (aPopupMenu, nil);
                AddFolderMenu_Tools (aPopupMenu);



              end;
  end;
end;


begin

end.




 (*
//--------------------------------------------------------------------
procedure TdmAct_MapFile.Del_List;
//--------------------------------------------------------------------
var i: integer;
begin
{
  if ConfirmDlg (STR_ASK_DEL) then
  begin
    for i:=0 to FSelectedIDList.Count-1 do
      if dmMapFile.Del (FSelectedIDList[i].ID) then
        PostDelNode (FSelectedIDList[i].ID, ntItem);
  end;
}
end;

*)



//
//procedure TdmAct_MapFile.DataModuleDestroy(Sender: TObject);
//begin
//
//end;
