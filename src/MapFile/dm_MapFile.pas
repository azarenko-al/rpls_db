unit dm_MapFile;

interface

uses
  SysUtils, Classes, Forms,  Variants, Controls, Dialogs,
  Messages, Windows, FileCtrl,

  dm_Onega_DB_data,

  dm_Object_base,

  u_const_msg,

u_Mapinfo_WOR_classes,

  dm_Main,

  u_assert,

  u_mapx,

  //u_const_msg,

  u_types,

//  u_common_const,
  u_const_filters,

  u_db,
  u_files,

  u_const_db,
  u_func_msg,

   DB, ADODB;



type
  TdmMapFileAddRec = record
    FileName: string;
    FolderID: integer;

    ZOOM_MIN: integer;
    ZOOM_MAX: integer;
    ALLOW_ZOOM : boolean;

    Priority: integer;

  end;




  TdmMapFile = class(TdmObject_base)
    OpenDialog1: TOpenDialog;
    OpenDialog_WOR: TOpenDialog;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

  private
    FFolderPath: string;
    FStrList: TStringList;

    procedure AddList(aList: TStrings);

  protected
  public
    procedure AddFromDir (aFileDir: string; aFolderID: integer=0);

    function Del(aID: integer): boolean; overload;


    function Add(aRec: TdmMapFileAddRec): integer; overload;

    function AddFileName (aFileName: string; aFolderID: integer=0): integer; overload;
    function Dlg_Add(aFolderID: integer): Boolean;
    function Dlg_Add_WOR(aFolderID: integer): Boolean;

    function Dlg_LoadMapsFromDir(aFolderID: integer): Boolean;
    function Dlg_LoadMapsFromFiles: Boolean;

  end;

function dmMapFile: TdmMapFile;

//====================================================================
implementation    {$R *.DFM}
//====================================================================

var
  FdmMapFile: TdmMapFile;


// ---------------------------------------------------------------
function dmMapFile: TdmMapFile;
// ---------------------------------------------------------------

begin
  if not Assigned(FdmMapFile) then
      FdmMapFile := TdmMapFile.Create(Application);

  Result := FdmMapFile;
end;


procedure TdmMapFile.DataModuleCreate(Sender: TObject);
begin
  inherited;
  TableName:=TBL_MAP1;
  ObjectName:=OBJ_MAP;


  FStrList:=TStringList.Create();

end;




procedure TdmMapFile.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil (FStrList);

  inherited;
end;



//--------------------------------------------------------------------
procedure TdmMapFile.AddList(aList: TStrings);
//--------------------------------------------------------------------
var
  i: Integer;
  iID: Integer;
begin
  for i := 0 to aList.Count-1 do
  begin
   // iID:=
    AddFileName(aList[i], 0);

//    aList.Objects[i]:=Pointer(iID);
  end;


  FStrList.Assign(aList);

  g_EventManager.PostEvent_(et_ADD_MAP, ['Files', Integer(FStrList)]);

//  PostUserMessage(WM__ADD_MAP,  Integer( FStrList));

end;



//-------------------------------------------------------------------
function TdmMapFile.Dlg_LoadMapsFromFiles: Boolean;
//-------------------------------------------------------------------
//var
 // OpenDialog: TOpenDialog;
begin
//  OpenDialog:=TOpenDialog.Create (nil);
  Result := False;

  with OpenDialog1 do
  begin
    Title     :='�����';
    DefaultExt:='*.tab';
    Filter    :='MapInfo files (*.tab)|*.tab';
    Options   := [ofFileMustExist, ofHideReadOnly, ofAllowMultiSelect, ofEnableSizing];

    if Execute then
  //  begin
      if Files.Count>0 then
      begin
        Screen.Cursor:= crHourGlass;
        AddList (Files);
      //  PostEvent(WE_PROJECT_UPDATE_MAP_LIST);
        Screen.Cursor:= crDefault;

        Result := True;
      end;
  //  end;

  //  Free;
  end;
end;


//--------------------------------------------------------------------
function TdmMapFile.AddFileName (aFileName: string; aFolderID: integer=0): integer;
//--------------------------------------------------------------------
var
  rec: TdmMapFileAddRec;
begin
  FillChar(rec, SizeOf(rec), 0);

  rec.FileName:=aFileName;
  rec.FolderID:=aFolderID;

  Result:=Add(rec);
end;

//--------------------------------------------------------------------
function TdmMapFile.Add(aRec: TdmMapFileAddRec): integer;
//--------------------------------------------------------------------
var
  dDateCreated, dDateModify: TDateTime;
  iSize: int64;

begin
  dDateCreated:= GetFileDate(aRec.FileName);
//  dDateCreated:= mapx_GetFileDate(aRec.FileName);

  dDateModify := GetFileModifyDate(aRec.FileName);
//  dDateModify := mapx_GetFileModifyDate(aRec.FileName);



  iSize := mapx_FileSize(aRec.FileName);


  Result:=dmOnega_DB_data.ExecStoredProc_ ('sp_Map_Add',
            [
              FLD_PROJECT_ID, dmMain.ProjectID,
              FLD_FOLDER_ID,  aRec.FolderID,

              FLD_Name,       aRec.FileName,


              FLD_ZOOM_MIN,   aRec.Zoom_Min,
              FLD_ZOOM_MAX,   aRec.Zoom_Max,
              FLD_ALLOW_ZOOM, aRec.Allow_Zoom,


              FLD_priority,  aRec.priority,

           //  db_Par(FLD_Name,        ExtractFileName(aRec.FileName)),


//              db_Par(FLD_Name,       aRec.FileName),
           //  db_Par(FLD_Name,       ExtractFileName(aRec.FileName)),
          //    db_Par(FLD_FileName,   aRec.FileName),

               FLD_file_date_created, dDateCreated,
               FLD_file_date_modify , dDateModify,
               FLD_filesize         , iSize

            ]);
end;

//---------------------------------------------------------------------------
function TdmMapFile.Del(aID: integer): boolean;
//---------------------------------------------------------------------------
begin
  Result := dmOnega_DB_data.Map_Del (aID)  > 0;
//  Result := k > 0;
end;


//--------------------------------------------------------------------
procedure TdmMapFile.AddFromDir (aFileDir: string; aFolderID: integer=0);
//--------------------------------------------------------------------
var
  oStrList: TStringList;
begin
  if aFileDir='' then
    exit;

  oStrList:=TStringList.Create;

{ TODO : reduce }
  // -------------------------------------------------------------------
  //FindFilesByMaskEx (aFileDir, '*.tab', oStrList);//, False);
  ScanDir1 (aFileDir, '*.tab', oStrList);//, False);
  // -------------------------------------------------------------------
  //FileSearchEx('C:\Temp', '.txt;.tmp;.exe;.doc', FileList);

(*  ShowMessage('procedure TdmMapFile.AddFromDir (aFileDir: string; aFolderID: integer=0);');

  ShellExec_Notepad_temp (oStrList.text);
*)


  AddList (oStrList);

  FreeAndNil(oStrList);


end;


//--------------------------------------------------------------------
function TdmMapFile.Dlg_Add(aFolderID: integer): Boolean;
//--------------------------------------------------------------------
var i: integer;
  iID: Integer;
   rec: TdmMapFileAddRec;
begin
  FillChar(rec, SizeOf(rec), 0);



  OpenDialog1.DefaultExt:='*.tab';
  OpenDialog1.Filter:=FILTER_MAPINFO_TAB;

  if OpenDialog1.Execute then
  begin

    for i:=0 to OpenDialog1.Files.Count-1 do
    begin
      rec.FileName:=OpenDialog1.Files[i];
      rec.FolderID:=aFolderID;

      AssertFileExists (rec.FileName);

      iID:=Add (rec);

    ////  OpenDialog1.Files.Objects[i]:=Pointer(iID);

    end;

    Result:=true;

    g_EventManager.PostEvent_(et_ADD_MAP, ['Files',Integer(  OpenDialog1.Files )]);

////    PostUserMessage(WM__ADD_MAP,  Integer( OpenDialog1.Files));


 //   AddList (OpenDialog1.Files);

//    OpenDialog1.Files.

  //  PostEvent(WE_PROJECT_UPDATE_MAP_LIST);
///    PostUpdateFolder (aFolderID);
  end else
    Result:=False;
end;


//--------------------------------------------------------------------
function TdmMapFile.Dlg_Add_WOR(aFolderID: integer): Boolean;
//--------------------------------------------------------------------
var i: integer;
  iID: Integer;
  oTable: TworTable;
  oWorkspace: TWorkspace;
   rec: TdmMapFileAddRec;
  s: string;
  
begin
  OpenDialog_WOR.DefaultExt:='*.wor';
  OpenDialog_WOR.Filter:=FILTER_MAPINFO_WOR;

  if OpenDialog_WOR.Execute then
  begin

      oWorkspace:=TWorkspace.Create;
      oWorkspace.LoadFromFile(OpenDialog_WOR.FileName);


      for i:=0 to oWorkspace.OrderedFileList.Count-1 do
      begin
        s:=oWorkspace.OrderedFileList[i];

        oTable:=oWorkspace.GetItemByName( s );

    //    oTable.FileName

        if Assigned(oTable) then
        begin
          FillChar(rec, SizeOf(rec), 0);


          rec.FileName:=oTable.FileName;
          rec.FolderID:=aFolderID;

          rec.ZOOM_MIN:=round(oTable.ZoomMin);
          rec.ZOOM_Max:=round(oTable.ZoomMax);
          rec.Allow_ZOOM:=oTable.IsZoom;

          rec.priority:=i+1;


      //    AssertFileExists (rec.FileName);

          iID:=Add (rec);

          {
            db_AddRecord_(dxMemData1,
            [
              FLD_FILENAME, oTable.FileName,
              FLD_ZOOM_MIN, oTable.ZoomMin,
              FLD_ZOOM_MAX, oTable.ZoomMax,
              FLD_IS_ZOOM, oTable.IsZoom

            ]);
           }
        end;


      end;


    FreeAndNil (oWorkspace);


  //  for i:=0 to OpenDialog_WOR.Files.Count-1 do
  //  begin


    ////  OpenDialog1.Files.Objects[i]:=Pointer(iID);

 //   end;



    Result:=true;


    g_EventManager.PostEvent_(et_Explorer_REFRESH_MAP_FILES, []);

   // PostMessage (Application.Handle, WM_Explorer_REFRESH_MAP_FILES,0,0);

//PostEvent(WE_PROJECT_UPDATE_MAP_LIST);

//    PostUserMessage(WM__ADD_MAP,  Integer( OpenDialog1.Files));


 //   AddList (OpenDialog1.Files);

//    OpenDialog1.Files.

  //  PostEvent(WE_PROJECT_UPDATE_MAP_LIST);
///    PostUpdateFolder (aFolderID);
  end else
    Result:=False;
end;


//-------------------------------------------------------------------
function TdmMapFile.Dlg_LoadMapsFromDir(aFolderID: integer): Boolean;
//-------------------------------------------------------------------
var
  s: string;
begin
  if FileCtrl.SelectDirectory('����� �����...','',FFolderPath) then
//    cx_SetButtonEditText(Sender, sDir);


//  if PBFolderDialog1.Execute then
    s := FFolderPath
  else
    s:='';

//  oDialog.Free;

  if s<>'' then begin
    Screen.Cursor:= crHourGlass;
  //  dmMapFile.
    AddFromDir (s);
  //  PostEvent(WE_PROJECT_UPDATE_MAP_LIST);
    Screen.Cursor:= crDefault;
  end;

  Result := s<>'';
end;

begin
end.



