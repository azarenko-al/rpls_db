inherited dmAct_MapFile: TdmAct_MapFile
  OldCreateOrder = True
  Left = 1304
  Top = 578
  Height = 294
  Width = 481
  inherited ActionList1: TActionList
    Left = 112
    object act_ExportToXML: TAction
      Caption = 'act_ExportToXML'
    end
    object act_ImportFromXML: TAction
      Caption = 'ImportFromXML'
    end
    object act_Add_from_Dir: TAction
      Caption = 'act_Add_from_Dir'
    end
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.rlf'
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 216
    Top = 10
  end
  object SaveDialog_XML: TSaveDialog
    DefaultExt = 'xml'
    Filter = 'XML (*.xml)|*.xml'
    Left = 215
    Top = 85
  end
  object ActionList2: TActionList
    Left = 96
    Top = 176
    object act_Add_WOR: TAction
      Caption = 'act_Add_WOR'
    end
  end
end
