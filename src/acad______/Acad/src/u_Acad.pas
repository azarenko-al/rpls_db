unit u_Acad;


interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,

  u_ACAD_classes,

  u_geo,

  u_func,
  u_acad_params,
  u_acad_config
  ;

type

  TAcad = class   
  private
    Config: tacad_config;

  public
    Params: TIni_acad_params;

    constructor Create;
    destructor Destroy; override;

    procedure SaveLineToFile(aFileName: string);

  end;

implementation

const
  Groundtext= 'H���.';
  Antennatext='H �.�.';

  LF=#13+#10;

  DEF_END=
    'ENDSEC'     + LF+
    '  0'        + LF+
    'EOF'        ;
  DEF_POLYLINE=
    'POLYLINE'   + LF+
    '  8'        + LF+
    '0'          + LF+
    '  6'        + LF+
    'CONTINUOUS' + LF+
    ' 62'        + LF+
    '     7'     + LF+
    ' 66'        + LF+
    '     1'     + LF+
    '  0'        ;
  DEF_COLORPOLYLINE=
    'POLYLINE'   + LF+
    '  8'        + LF+
    '0'          + LF+
    '  6'        + LF+
    'CONTINUOUS' + LF+
    ' 62'        + LF+
    '     %d'    + LF+
    ' 66'        + LF+
    '     1'     + LF+
    '  0'        ;
  DEF_POLYLINE_END=
    'SEQEND'     + LF+
    '  8'        + LF+
    '0'          + LF+
    ' 62'        + LF+
    '     7'     + LF+
    '  0'        ;
  DEF_VERTEX=
    'VERTEX'     + LF+
    '  8'        + LF+
    '0'          + LF+
    ' 10'        + LF+
    '%8.6f'      + LF+
    ' 20'        + LF+
    '%8.6f'      + LF+
    ' 30'        + LF+
    '0.0'        + LF+
    '  0'        ;
  DEF_TEXT=
    'TEXT'       + LF+
    '  8'        + LF+
    '0'          + LF+
    ' 62'        + LF+
    '     7'     + LF+
    ' 10'        + LF+
    '%8.6f'      + LF+
    ' 20'        + LF+
    '%8.6f'      + LF+
    ' 30'        + LF+
    '0.0'        + LF+
    '  1'        + LF+
    '%s'         + LF+
    ' 40'        + LF+
    '%8.6f'      + LF+
    ' 50'        + LF+
    '%8.6f'      + LF+
    ' 41'        + LF+
    '0.545455'   + LF+
    ' 51'        + LF+
    '15.0'        + LF+
    '  0'        ;
  DEF_LINE=
    'LINE'       + LF+
    '  5'        + LF+
    '26A90'      + LF+
    '  8'        + LF+
    '0'          + LF+
    ' 62'        + LF+
    '     0'     + LF+
    ' 10'        + LF+
    '%8.6f'      + LF+
    ' 20'        + LF+
    '%8.6f'      + LF+
    ' 30'        + LF+
    '0.0'        + LF+
    ' 11'        + LF+
    '%8.6f'      + LF+
    ' 21'        + LF+
    '%8.6f'      + LF+
    ' 31'        + LF+
    '0.0'        + LF+
    '  0'        ;
  DEF_COLORLINE=
    'LINE'       + LF+
    '  5'        + LF+
    '26A90'      + LF+
    '  8'        + LF+
    '0'          + LF+
    ' 62'        + LF+
    '     %d'    + LF+
    ' 10'        + LF+
    '%8.6f'      + LF+
    ' 20'        + LF+
    '%8.6f'      + LF+
    ' 30'        + LF+
    '0.0'        + LF+
    ' 11'        + LF+
    '%8.6f'      + LF+
    ' 21'        + LF+
    '%8.6f'      + LF+
    ' 31'        + LF+
    '0.0'        + LF+
    '  0'        ;

const
  HEIGHT_FOREST_DEF = 20;
  HEIGHT_WATER_DEF  = -2;
  HEIGHT_CITY_DEF   = 25;
  HEIGHT_COUNTRY_DEF= 7;

  COLOR_FOREST = 3;
  COLOR_WATER  = 5;
  COLOR_CITY   = 1;
  COLOR_COUNTRY= 9;
  COLOR_OPEN   = 7;



//------------------------------------------------------
function geo_GetEarthHeight___(aOffset_KM, aDistance_KM: double; aRefraction:
    double=1.33): double;
//------------------------------------------------------
var fRadiusZem: double; //const Radius_Zem=8500; //yeaea.?aaeon Caiee (ei)
begin
  Result := geo_GetEarthHeight(aOffset_KM, aDistance_KM, aRefraction);

  Exit;



  Assert(aOffset_KM >=0, 'aOffset_KM >=0');
  Assert(aDistance_KM >=0,'aDistance_KM >=0');

  // iia?aoiinou
  if Abs(aOffset_KM - aDistance_KM) < 0.00000001 then
    aOffset_KM := aDistance_KM;

  Assert(aOffset_KM <= aDistance_KM,
    Format('aOffset_KM <= aDistance_KM : %1.10f-%1.10f',[aOffset_KM, aDistance_KM]));


  if aRefraction=0 then
    aRefraction:=1.33;

  fRadiusZem:=12.740*aRefraction;  // eiyoo.?ao?aeoee
  Result:=aOffset_KM*(aDistance_KM-aOffset_KM) / (fRadiusZem); {aiaaaea ia e?eaecio Caiee (i)}
end;



//------------------------------------------------------
constructor TAcad.Create;
//------------------------------------------------------
begin
  inherited;

  Params := TIni_acad_params.Create();
  Config := tacad_config.Create();
end;

//------------------------------------------------------
destructor TAcad.Destroy;
//------------------------------------------------------
begin
  FreeAndNil(Config);
  FreeAndNil(Params);

  inherited Destroy;
end;


//------------------------------------------------------
procedure TAcad.SaveLineToFile(aFileName: string);
//------------------------------------------------------
const
  DEF_POINT_COUNT=  30;

var
  Font_size,OtnX,OtnY,RightY,LeftY:Double;

  x_dugi,y_dugi,x_dugi0,y_dugi0:Double;
  r_dugi:integer;
  x_p:double;
  y_p:double;
  y_p1:double;
  y_m:double;
  x_km:double;
  Heig_m,DistXY,HeigXY,Dist,Heig:double;
  i,j,c:integer;
  s:string;
  ClutterTmp,d:string;
  oList:TStringList;
  h1:double;
  iColor: Integer;
  x,f,bAntenna2:boolean;

  oProject: TAcad_Project;
  oPolyline: TAcad_Polyline;


begin
  Assert(FileExists(Config.Template_filename), 'FileExists: '+ Config.Template_filename);

  aFileName:= ChangeFileExt(aFileName, '.dxf');


  oProject:=TAcad_Project.Create;


  DecimalSeparator:='.';
  oList:=TStringList.Create;

  with Params,Config do
  begin
    oList.LoadFromFile(Template_filename);

    if (Site1.Antenna2Height<>0) and (site2.Antenna2Height<>0)
      then bAntenna2:= True
      else bAntenna2:= False;

    if (Site1.GroundHeight+Site1.AntennaHeight)> (Site2.GroundHeight+Site2.AntennaHeight)
      then Heig_m:=(Site1.GroundHeight-ref_plane)+Site1.AntennaHeight
      else Heig_m:=(Site2.GroundHeight-ref_plane)+Site2.AntennaHeight;

    Site1.GroundHeight:=Site1.GroundHeight-ref_plane;
    Site2.GroundHeight:=Site2.GroundHeight-ref_plane;



    Assert(Length_km>0);
    
    //������ ������
    DistXY:=(Graph.BoundsXY.MaxX-Graph.BoundsXY.MinX)/Length_km;
    HeigXY:=(Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/(Heig_m);

    j:= 0;
    font_size:=2.5;
    //����� � ���������
    for i:= 0 to High(lines) do
    Begin
      Heig:= (HeigXY* (Lines[i].Height_m-ref_plane));
      Dist:= (DistXY* Lines[i].Distance_km);

      oProject.Lines.AddItem(Graph.BoundsXY.MinX+ Dist,
                                 Graph.BoundsXY.MinY,
                                 Graph.BoundsXY.MinX+ Dist,
                                 Graph.BoundsXY.MinY+ heig);


      oList.Add(format(DEF_LINE,[Graph.BoundsXY.MinX+ Dist,
                                 Graph.BoundsXY.MinY,
                                 Graph.BoundsXY.MinX+ Dist,
                                 Graph.BoundsXY.MinY+ heig]));

      // ����������� ������ � ��������� �������
      if (i = 0) or (i = High(lines)) then
      begin

(*        oProject.Texts.AddItem(Graph.BoundsXY.MinX+ Dist,
                                 Graph.BoundsXY.MinY,
                                 Graph.BoundsXY.MinX+ Dist,
                                 Graph.BoundsXY.MinY+ heig);
*)
        oProject.Texts.AddItem(Graph.BoundsXY.MinX+ Dist,-958.0, FloattoStr(Lines[i].Distance_km),font_size,90.0);
        oProject.Texts.AddItem(Graph.BoundsXY.MinX+ Dist,-971.0, FloattoStr(Lines[i].Height_m),font_size,90.0);



        oList.Add(format(DEF_TEXT,[Graph.BoundsXY.MinX+ Dist,-958.0, FloattoStr(Lines[i].Distance_km),font_size,90.0]));
        oList.Add(format(DEF_TEXT,[Graph.BoundsXY.MinX+ Dist,-971.0, FloattoStr(Lines[i].Height_m),font_size,90.0]));
      end else

      // ����������� ������� ����������� � ������ ���������� ������� ������,
      // ����� �� ���� ��������� �������� ���� �� �����. /13 - ��������� ����������������
      if (params.Lines[i].Distance_km - params.Lines[j].Distance_km)>(font_size/13) then
        // ���� ������� ����� �� ���������� �� ���������� ������ - ����� �� ����������
        if Lines[i].Height_m <> Lines[j].Height_m then
        begin
          oProject.Texts.AddItem(Graph.BoundsXY.MinX+ Dist,-971.0, FloattoStr(Lines[i].Height_m),font_size,90.0);
          oProject.Texts.AddItem(Graph.BoundsXY.MinX+ Dist,-958.0, FloattoStr(Lines[i].Distance_km),font_size,90.0);

          oList.Add(format(DEF_TEXT,[Graph.BoundsXY.MinX+ Dist,-971.0, FloattoStr(Lines[i].Height_m),font_size,90.0]));
          oList.Add(format(DEF_TEXT,[Graph.BoundsXY.MinX+ Dist,-958.0, FloattoStr(Lines[i].Distance_km),font_size,90.0]));

           if lines[i].Clu_Height_m>0 then
             begin
               oList.Add(format(DEF_TEXT,[Graph.BoundsXY.MinX+ Dist,-984.0, FloattoStr(Lines[i].Clu_Height_m),font_size,90.0]));
             end;
          j:= i;
        end;

    end;

    //���������
    // -----------------------------------------
    oPolyline := oProject.PolyLines.AddItem;

    oList.Add(DEF_POLYLINE);
    for i:= 0 to high(lines) do
    Begin
      Heig:= (HeigXY* (Lines[i].Height_m-ref_plane));
      Dist:= (DistXY* Lines[i].Distance_km);

      oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX+ Dist,
                                 Graph.BoundsXY.MinY+ heig);

      oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX+ Dist,
                                   Graph.BoundsXY.MinY+ heig]));
    End;

    oList.Add(DEF_POLYLINE_END);

    //������� ���������
    x:=false;
    f:=true;
    ClutterTmp:='0';
    for i:= 0 to high(lines) do
    Begin
      If ClutterTmp=lines[i].Clutter then x:=false
                                     else x:=true;

      while (f) do
      begin
        if lines[i].Clutter='forest' then
        begin
          oList.Add(format(DEF_COLORPOLYLINE,[COLOR_FOREST]));
          iColor:=COLOR_FOREST;

          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_FOREST_DEF);
        end else

        if lines[i].Clutter='building' then
        begin
          oList.Add(format(DEF_COLORPOLYLINE,[COLOR_CITY]));
          iColor:=COLOR_CITY;

          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_CITY_DEF);
        end else

        if lines[i].Clutter='country' then
        begin
          oList.Add(format(DEF_COLORPOLYLINE,[COLOR_COUNTRY]));
          iColor:=COLOR_COUNTRY;

          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_COUNTRY_DEF);
        end else

        if lines[i].Clutter='open' then
        begin
          oList.Add(format(DEF_COLORPOLYLINE,[COLOR_OPEN]));
          iColor:=COLOR_OPEN;

         h1:= 0;
        end else

        if lines[i].Clutter='water' then
        begin
          oList.Add(format(DEF_COLORPOLYLINE,[COLOR_WATER]));
          iColor:=COLOR_WATER;

          h1:= HEIGHT_WATER_DEF;
        end;


        oPolyline := oProject.PolyLines.AddColorItem(iColor);


        f:=false;
        x:=false;
      end;

      while (x) do
      begin
        oList.Add(DEF_POLYLINE_END);

        if lines[i].Clutter='forest' then
        begin
          oList.Add(format(DEF_COLORPOLYLINE,[COLOR_FOREST]));
          iColor:=COLOR_FOREST;

          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_FOREST_DEF);
        end else

        if lines[i].Clutter='building' then
        begin
          oList.Add(format(DEF_COLORPOLYLINE,[COLOR_CITY]));
           iColor:=COLOR_CITY;

          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_CITY_DEF);
        end else

        if lines[i].Clutter='country' then
        begin
          oList.Add(format(DEF_COLORPOLYLINE,[COLOR_COUNTRY]));
           iColor:=COLOR_COUNTRY;

          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_COUNTRY_DEF);
        end else

        if lines[i].Clutter='open' then
        begin
          oList.Add(format(DEF_COLORPOLYLINE,[COLOR_OPEN]));
           iColor:=COLOR_OPEN;

          h1:= 0;
        end else

        if lines[i].Clutter='water' then
        begin
          oList.Add(format(DEF_COLORPOLYLINE,[COLOR_WATER]));
           iColor:=COLOR_WATER;

         h1:= HEIGHT_WATER_DEF;
        end;

        x:=false;
      end;

      Heig:= (HeigXY* (Lines[i].Height_m-ref_plane+h1));
      Dist:= (DistXY* Lines[i].Distance_km);
      //h1:= h1*HeigXY;

//       oPolyline := oProject.PolyLines.AddItem;

      oPolyline := oProject.PolyLines.AddColorItem(iColor);

      oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX+ Dist,
                                (Graph.BoundsXY.MinY+ heig));


      oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX+ Dist,
                                   (Graph.BoundsXY.MinY+ heig)]));

      ClutterTmp:=lines[i].Clutter;
    end;

    oList.Add(DEF_POLYLINE_END);

     //������ ���������
    x:=false;
    f:=true;
    ClutterTmp:='0';

    for i:= 0 to high(lines) do
    begin
      if ClutterTmp=lines[i].Clutter then x:=false
                                     else x:=true;

      while (f) do
      begin
        if lines[i].Clutter='forest' then
        begin
          c := COLOR_FOREST;
          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_FOREST_DEF);
        end else

        if lines[i].Clutter='building' then
        begin
          c := COLOR_CITY;
          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_CITY_DEF);
        end else

        if lines[i].Clutter='country' then
        begin
          c := COLOR_COUNTRY;
          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_COUNTRY_DEF);
        end else

        if lines[i].Clutter='open' then
        begin
         c := COLOR_OPEN;
         h1:= 0;
        end else

        if lines[i].Clutter='water' then
        begin
          c := COLOR_WATER;
          h1:= HEIGHT_WATER_DEF;
        end;

        f:=false;
        x:=true;
      end;

      while (x) do
      begin
        if lines[i].Clutter='forest' then
        begin
          c := COLOR_FOREST;
          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_FOREST_DEF);
        end else

        if lines[i].Clutter='building' then
        begin
          c := COLOR_CITY;
          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_CITY_DEF);
        end else

        if lines[i].Clutter='country' then
        begin
          c := COLOR_COUNTRY;
          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_COUNTRY_DEF);
        end else

        if lines[i].Clutter='open' then
        begin
          c := COLOR_OPEN;
          h1:= 0;
        end else

        if lines[i].Clutter='water' then
        begin
          c := COLOR_WATER;
          h1:= HEIGHT_WATER_DEF;
        end;

        x:=false;
      end;

      Heig:= (HeigXY* (Lines[i].Height_m-ref_plane));
      Dist:= (DistXY* Lines[i].Distance_km);
      h1:= h1*HeigXY;

      oProject.Lines.AddColorItem(c, Graph.BoundsXY.MinX+ Dist, Graph.BoundsXY.MinY+ heig, Graph.BoundsXY.MinX+ Dist, (Graph.BoundsXY.MinY+ heig)+h1);

      oList.Add(format(DEF_COLORLINE,[c,Graph.BoundsXY.MinX+ Dist, Graph.BoundsXY.MinY+ heig, Graph.BoundsXY.MinX+ Dist, (Graph.BoundsXY.MinY+ heig)+h1]));
    end;

   //������ �����
    OtnX:=22;
    // ����� ����� �������
    OtnY:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/(Heig_m))*(Site1.GroundHeight+Site1.AntennaHeight);


    // ---------------------------------------------------------------
    oPolyline := oProject.PolyLines.AddItem;

    oList.Add(DEF_POLYLINE);

     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(OtnX),    Graph.BoundsXY.MinY+(OtnY+8));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(OtnX-8), Graph.BoundsXY.MinY+(OtnY+8));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(OtnX-8), Graph.BoundsXY.MinY+(OtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(OtnX-4) , Graph.BoundsXY.MinY+(OtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(OtnX-12), Graph.BoundsXY.MinY+(OtnY));


     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(OtnX),    Graph.BoundsXY.MinY+(OtnY+8)]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(OtnX-8), Graph.BoundsXY.MinY+(OtnY+8)]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(OtnX-8), Graph.BoundsXY.MinY+(OtnY)]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(OtnX-4) , Graph.BoundsXY.MinY+(OtnY)]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(OtnX-12), Graph.BoundsXY.MinY+(OtnY)]));
    oList.Add(DEF_POLYLINE_END);


     oProject.Texts.AddItem(Graph.BoundsXY.MinX-(OtnX),    Graph.BoundsXY.MinY+(OtnY+8.5),IntToStr(Site1.AntennaHeight),3.0,0.0); // ������
     oProject.Texts.AddItem(Graph.BoundsXY.MinX-(OtnX),    Graph.BoundsXY.MinY+(OtnY+4.5),AntennaText,3.0,0.0); // �������

     oList.Add(format(DEF_TEXT,[Graph.BoundsXY.MinX-(OtnX),    Graph.BoundsXY.MinY+(OtnY+8.5),IntToStr(Site1.AntennaHeight),3.0,0.0])); // ������
     oList.Add(format(DEF_TEXT,[Graph.BoundsXY.MinX-(OtnX),    Graph.BoundsXY.MinY+(OtnY+4.5),AntennaText,3.0,0.0])); // �������


     // ---------------------------------------------------------------
    oPolyline := oProject.PolyLines.AddItem;

    oList.Add(DEF_POLYLINE);
      oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-((OtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3)));
      oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(OtnX-8),    Graph.BoundsXY.MinY+(OtnY));
      oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-((OtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3)));


     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-((OtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3))]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(OtnX-8),    Graph.BoundsXY.MinY+(OtnY)]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-((OtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3))]));
    oList.Add(DEF_POLYLINE_END);

    //������ ������������
    OtnY:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/(Heig_m))*(Site2.GroundHeight+Site2.AntennaHeight);

    // ---------------------------------------------------------------
    oPolyline := oProject.PolyLines.AddItem;

    oList.Add(DEF_POLYLINE);
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX),    Graph.BoundsXY.MinY+(OtnY+8));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX-8), Graph.BoundsXY.MinY+(OtnY+8));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX-8), Graph.BoundsXY.MinY+(OtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX-4) , Graph.BoundsXY.MinY+(OtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX-12), Graph.BoundsXY.MinY+(OtnY));

     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX),    Graph.BoundsXY.MinY+(OtnY+8)]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX-8), Graph.BoundsXY.MinY+(OtnY+8)]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX-8), Graph.BoundsXY.MinY+(OtnY)]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX-4) , Graph.BoundsXY.MinY+(OtnY)]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX-12), Graph.BoundsXY.MinY+(OtnY)]));
    oList.Add(DEF_POLYLINE_END);

     oProject.Texts.AddItem(Graph.BoundsXY.MaxX+(OtnX-7),    Graph.BoundsXY.MinY+(OtnY+8.5),IntToStr(Site2.AntennaHeight),3.0,0.0);//������
     oProject.Texts.AddItem(Graph.BoundsXY.MaxX+(OtnX-7), Graph.BoundsXY.MinY+(OtnY+4.5),AntennaText,3.0,0.0);   //�������


     oList.Add(format(DEF_TEXT,[Graph.BoundsXY.MaxX+(OtnX-7),    Graph.BoundsXY.MinY+(OtnY+8.5),IntToStr(Site2.AntennaHeight),3.0,0.0]));//������
     oList.Add(format(DEF_TEXT,[Graph.BoundsXY.MaxX+(OtnX-7), Graph.BoundsXY.MinY+(OtnY+4.5),AntennaText,3.0,0.0]));   //�������

    // ---------------------------------------------------------------
    oPolyline := oProject.PolyLines.AddItem;

    oList.Add(DEF_POLYLINE);
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+((OtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3)));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX-8),    Graph.BoundsXY.MinY+(OtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+((OtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3)));


     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+((OtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3))]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX-8),    Graph.BoundsXY.MinY+(OtnY)]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+((OtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3))]));
    oList.Add(DEF_POLYLINE_END);

    if bAntenna2=true then
    begin
    OtnX:=36;
    // ����� ����� �������
      OtnY:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/(Heig_m))*(Site1.GroundHeight+Site1.Antenna2Height);

      // ---------------------------------------------------------------
      oPolyline := oProject.PolyLines.AddItem;


      oList.Add(DEF_POLYLINE);
       oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(OtnX),    Graph.BoundsXY.MinY+(OtnY+8));
       oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(OtnX-8), Graph.BoundsXY.MinY+(OtnY+8));
       oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(OtnX-8), Graph.BoundsXY.MinY+(OtnY));
       oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(OtnX-4) , Graph.BoundsXY.MinY+(OtnY));
       oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(OtnX-12), Graph.BoundsXY.MinY+(OtnY));


       oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(OtnX),    Graph.BoundsXY.MinY+(OtnY+8)]));
       oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(OtnX-8), Graph.BoundsXY.MinY+(OtnY+8)]));
       oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(OtnX-8), Graph.BoundsXY.MinY+(OtnY)]));
       oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(OtnX-4) , Graph.BoundsXY.MinY+(OtnY)]));
       oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(OtnX-12), Graph.BoundsXY.MinY+(OtnY)]));
      oList.Add(DEF_POLYLINE_END);

       oProject.Texts.AddItem(Graph.BoundsXY.MinX-(OtnX),    Graph.BoundsXY.MinY+(OtnY+8.5),IntToStr(Site1.Antenna2Height),3.0,0.0); // ������
       oProject.Texts.AddItem(Graph.BoundsXY.MinX-(OtnX),    Graph.BoundsXY.MinY+(OtnY+4.5),AntennaText,3.0,0.0); // �������


       oList.Add(format(DEF_TEXT,[Graph.BoundsXY.MinX-(OtnX),    Graph.BoundsXY.MinY+(OtnY+8.5),IntToStr(Site1.Antenna2Height),3.0,0.0])); // ������
       oList.Add(format(DEF_TEXT,[Graph.BoundsXY.MinX-(OtnX),    Graph.BoundsXY.MinY+(OtnY+4.5),AntennaText,3.0,0.0])); // �������

      // ---------------------------------------------------------------
      oPolyline := oProject.PolyLines.AddItem;


      oList.Add(DEF_POLYLINE);
       oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-((OtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3)));
       oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(OtnX-8),    Graph.BoundsXY.MinY+(OtnY));
       oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-((OtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3)));


       oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-((OtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3))]));
       oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(OtnX-8),    Graph.BoundsXY.MinY+(OtnY)]));
       oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-((OtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3))]));
      oList.Add(DEF_POLYLINE_END);

        //������ ������������
      OtnY:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/(Heig_m))*(Site2.GroundHeight+Site2.Antenna2Height);

      // ---------------------------------------------------------------
      oPolyline := oProject.PolyLines.AddItem;


      oList.Add(DEF_POLYLINE);
       oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX),    Graph.BoundsXY.MinY+(OtnY+8));
       oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX-8), Graph.BoundsXY.MinY+(OtnY+8));
       oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX-8), Graph.BoundsXY.MinY+(OtnY));
       oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX-4) , Graph.BoundsXY.MinY+(OtnY));
       oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX-12), Graph.BoundsXY.MinY+(OtnY));


       oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX),    Graph.BoundsXY.MinY+(OtnY+8)]));
       oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX-8), Graph.BoundsXY.MinY+(OtnY+8)]));
       oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX-8), Graph.BoundsXY.MinY+(OtnY)]));
       oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX-4) , Graph.BoundsXY.MinY+(OtnY)]));
       oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX-12), Graph.BoundsXY.MinY+(OtnY)]));

      oList.Add(DEF_POLYLINE_END);
       oProject.Texts.AddItem(Graph.BoundsXY.MaxX+(OtnX-7),    Graph.BoundsXY.MinY+(OtnY+8.5),IntToStr(Site2.Antenna2Height),3.0,0.0);//������
       oProject.Texts.AddItem(Graph.BoundsXY.MaxX+(OtnX-7), Graph.BoundsXY.MinY+(OtnY+4.5),AntennaText,3.0,0.0);   //�������


       oList.Add(format(DEF_TEXT,[Graph.BoundsXY.MaxX+(OtnX-7),    Graph.BoundsXY.MinY+(OtnY+8.5),IntToStr(Site2.Antenna2Height),3.0,0.0]));//������
       oList.Add(format(DEF_TEXT,[Graph.BoundsXY.MaxX+(OtnX-7), Graph.BoundsXY.MinY+(OtnY+4.5),AntennaText,3.0,0.0]));   //�������


      // ---------------------------------------------------------------
      oPolyline := oProject.PolyLines.AddItem;

      oList.Add(DEF_POLYLINE);
       oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+((OtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3)));
       oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX-8),    Graph.BoundsXY.MinY+(OtnY));
       oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+((OtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3)));


       oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+((OtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3))]));
       oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX-8),    Graph.BoundsXY.MinY+(OtnY)]));
       oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+((OtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3))]));
      oList.Add(DEF_POLYLINE_END);
    end;

   //����� �����
    OtnX:=22;
   //������� �����
    OtnY:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/(Heig_m))*(Site1.GroundHeight);

      // ---------------------------------------------------------------
      oPolyline := oProject.PolyLines.AddItem;


    oList.Add(DEF_POLYLINE);
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(OtnX),    Graph.BoundsXY.MinY+(OtnY+8));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(OtnX-8), Graph.BoundsXY.MinY+(OtnY+8));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(OtnX-8), Graph.BoundsXY.MinY+(OtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(OtnX-4) , Graph.BoundsXY.MinY+(OtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(OtnX-12), Graph.BoundsXY.MinY+(OtnY));


     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(OtnX),    Graph.BoundsXY.MinY+(OtnY+8)]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(OtnX-8), Graph.BoundsXY.MinY+(OtnY+8)]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(OtnX-8), Graph.BoundsXY.MinY+(OtnY)]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(OtnX-4) , Graph.BoundsXY.MinY+(OtnY)]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(OtnX-12), Graph.BoundsXY.MinY+(OtnY)]));
    oList.Add(DEF_POLYLINE_END);

     oProject.Texts.AddItem(Graph.BoundsXY.MinX-(OtnX),    Graph.BoundsXY.MinY+(OtnY+8.5),FloatToStr(Site1.GroundHeight+ref_plane),3.0,0.0); // ������
     oProject.Texts.AddItem(Graph.BoundsXY.MinX-(OtnX),    Graph.BoundsXY.MinY+(OtnY+4.5),GroundText,3.0,0.0); // �������


     oList.Add(format(DEF_TEXT,[Graph.BoundsXY.MinX-(OtnX),    Graph.BoundsXY.MinY+(OtnY+8.5),FloatToStr(Site1.GroundHeight+ref_plane),3.0,0.0])); // ������
     oList.Add(format(DEF_TEXT,[Graph.BoundsXY.MinX-(OtnX),    Graph.BoundsXY.MinY+(OtnY+4.5),GroundText,3.0,0.0])); // �������

      // ---------------------------------------------------------------
      oPolyline := oProject.PolyLines.AddItem;


    oList.Add(DEF_POLYLINE);
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-((OtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3)));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(OtnX-8),    Graph.BoundsXY.MinY+(OtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-((OtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3)));


     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-((OtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3))]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(OtnX-8),    Graph.BoundsXY.MinY+(OtnY)]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-((OtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3))]));
    oList.Add(DEF_POLYLINE_END);

    //---------
    //������ �����
    //������� �����
    OtnY:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/(Heig_m))*(Site2.GroundHeight);

      // ---------------------------------------------------------------
      oPolyline := oProject.PolyLines.AddItem;


    oList.Add(DEF_POLYLINE);
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX),    Graph.BoundsXY.MinY+(OtnY+8));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX-8), Graph.BoundsXY.MinY+(OtnY+8));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX-8), Graph.BoundsXY.MinY+(OtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX-4) , Graph.BoundsXY.MinY+(OtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX-12), Graph.BoundsXY.MinY+(OtnY));




     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX),    Graph.BoundsXY.MinY+(OtnY+8)]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX-8), Graph.BoundsXY.MinY+(OtnY+8)]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX-8), Graph.BoundsXY.MinY+(OtnY)]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX-4) , Graph.BoundsXY.MinY+(OtnY)]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX-12), Graph.BoundsXY.MinY+(OtnY)]));
    oList.Add(DEF_POLYLINE_END);

     oProject.Texts.AddItem(Graph.BoundsXY.MaxX+(OtnX-7),    Graph.BoundsXY.MinY+(OtnY+8.5),FloatToStr(Site2.GroundHeight+ref_plane),3.0,0.0);//������
     oProject.Texts.AddItem(Graph.BoundsXY.MaxX+(OtnX-7), Graph.BoundsXY.MinY+(OtnY+4.5),GroundText,3.0,0.0);   //�������


     oList.Add(format(DEF_TEXT,[Graph.BoundsXY.MaxX+(OtnX-7),    Graph.BoundsXY.MinY+(OtnY+8.5),FloatToStr(Site2.GroundHeight+ref_plane),3.0,0.0]));//������
     oList.Add(format(DEF_TEXT,[Graph.BoundsXY.MaxX+(OtnX-7), Graph.BoundsXY.MinY+(OtnY+4.5),GroundText,3.0,0.0]));   //�������

      // ---------------------------------------------------------------
      oPolyline := oProject.PolyLines.AddItem;


    oList.Add(DEF_POLYLINE);
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+((OtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3)));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX-8),    Graph.BoundsXY.MinY+(OtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+((OtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3)));


     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+((OtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3))]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX-8),    Graph.BoundsXY.MinY+(OtnY)]));
     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+((OtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((OtnY)-(sin(30)*3))]));
    oList.Add(DEF_POLYLINE_END);
    //---------

    //������ �����
    //������ �����
    OtnY:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/(Heig_m))*(Site2.GroundHeight+Site2.AntennaHeight);
    RightY:=OtnY;
    oList.Add(format(DEF_LINE,[Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY+ OtnY]));
    //�������
    r_dugi:=5;
    OtnX:=-r_dugi;
    y_dugi0:=(Graph.BoundsXY.MinY+OtnY);
    x_dugi0:=(Graph.BoundsXY.Maxx+OtnX);


    // ---------------------------------------------------------------
    oPolyline := oProject.PolyLines.AddItem;


    oList.Add(DEF_POLYLINE);
    for i:=0 to r_dugi+1 -1 do
    begin
      y_dugi:=(y_dugi0-i);
      x_dugi:=(x_dugi0)+ Sqrt(Abs(Sqr(r_dugi)-sqr(y_dugi-(y_dugi0))));
      oList.Add(format(DEF_VERTEX,[x_dugi,y_dugi]));

      oPolyline.Vertexes.AddItem(x_dugi,y_dugi);
    end;
    oList.Add(DEF_POLYLINE_END);

//    TAcad_POLYLINE


    // ---------------------------------------------------------------
    oPolyline := oProject.PolyLines.AddItem;

    oList.Add(DEF_POLYLINE);
    for i:=0 to r_dugi+1 -1 do
    begin
      y_dugi:=(y_dugi0+i);
      x_dugi:=(x_dugi0)+ Sqrt(Abs(Sqr(r_dugi)-sqr(y_dugi-(y_dugi0))));
      oList.Add(format(DEF_VERTEX,[x_dugi,y_dugi]));

      oPolyline.Vertexes.AddItem(x_dugi,y_dugi);
    end;
    oList.Add(DEF_POLYLINE_END);

    //----
    //����� �����
    OtnX:=r_dugi;
    OtnY:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/Heig_m)*(Site1.GroundHeight+Site1.AntennaHeight);
    LeftY:=OtnY;


    oProject.Lines.AddItem(Graph.BoundsXY.MinX, Graph.BoundsXY.MinY, Graph.BoundsXY.MinX, Graph.BoundsXY.MinY+ OtnY);

    oList.Add(format(DEF_LINE,[Graph.BoundsXY.MinX, Graph.BoundsXY.MinY, Graph.BoundsXY.MinX, Graph.BoundsXY.MinY+ OtnY]));
    //�������
    y_dugi0:=(Graph.BoundsXY.MinY+OtnY);
    x_dugi0:=(Graph.BoundsXY.Minx+OtnX);

    // ---------------------------------------------------------------
    oPolyline := oProject.PolyLines.AddItem;


    oList.Add(DEF_POLYLINE);
    for i:=0 to r_dugi+1 -1 do
    begin
      y_dugi:=(y_dugi0+i);
      x_dugi:=(x_dugi0)- Sqrt(Abs(Sqr(r_dugi)-sqr(y_dugi-(y_dugi0))));
      oList.Add(format(DEF_VERTEX,[x_dugi,y_dugi]));

      oPolyline.Vertexes.AddItem(x_dugi,y_dugi);
    end;
    oList.Add(DEF_POLYLINE_END);

    // ---------------------------------------------------------------
    oPolyline := oProject.PolyLines.AddItem;


    oList.Add(DEF_POLYLINE);
    for i:=0 to r_dugi+1 -1 do
    begin
      y_dugi:=(y_dugi0-i);
      x_dugi:=(x_dugi0)- Sqrt(Abs(Sqr(r_dugi)-sqr(y_dugi-(y_dugi0))));
      oList.Add(format(DEF_VERTEX,[x_dugi,y_dugi]));


      oPolyline.Vertexes.AddItem(x_dugi,y_dugi);
    end;
    oList.Add(DEF_POLYLINE_END);

    //----
    //��������� �����
    oProject.Lines.AddItem(Graph.BoundsXY.MinX, Graph.BoundsXY.MinY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY);
    oProject.Lines.AddItem(Graph.BoundsXY.MinX, Graph.BoundsXY.MinY+ LeftY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY+ RightY);

    oList.Add(format(DEF_LINE,[Graph.BoundsXY.MinX, Graph.BoundsXY.MinY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY]));
    oList.Add(format(DEF_LINE,[Graph.BoundsXY.MinX, Graph.BoundsXY.MinY+ LeftY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY+ RightY]));

    //------
    // 2-�� �������
    if bAntenna2=true then
    begin
       //������ �����
       //������ �����
      OtnY:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/Heig_m)*(Site2.GroundHeight+Site2.Antenna2Height);
      RightY:=OtnY;

      oProject.Lines.AddItem(Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY+ OtnY);

      oList.Add(format(DEF_LINE,[Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY+ OtnY]));
      //�������
      r_dugi:=5;
      OtnX:=-r_dugi;
      y_dugi0:=(Graph.BoundsXY.MinY+OtnY);
      x_dugi0:=(Graph.BoundsXY.Maxx+OtnX);

      // ---------------------------------------------------------------
      oPolyline := oProject.PolyLines.AddItem;


      oList.Add(DEF_POLYLINE);
      for i:=0 to r_dugi+1 -1 do
      Begin
        y_dugi:=(y_dugi0-i);
        x_dugi:=(x_dugi0)+ Sqrt(Abs(Sqr(r_dugi)-sqr(y_dugi-(y_dugi0))));
        oList.Add(format(DEF_VERTEX,[x_dugi,y_dugi]));

        oPolyline.Vertexes.AddItem(x_dugi,y_dugi);
      End;
      oList.Add(DEF_POLYLINE_END);

      // ---------------------------------------------------------------
      oPolyline := oProject.PolyLines.AddItem;


      oList.Add(DEF_POLYLINE);
      for i:=0 to r_dugi+1 -1 do
      Begin
        y_dugi:=(y_dugi0+i);
        x_dugi:=(x_dugi0)+ Sqrt(Abs(Sqr(r_dugi)-sqr(y_dugi-(y_dugi0))));
        oList.Add(format(DEF_VERTEX,[x_dugi,y_dugi]));

        oPolyline.Vertexes.AddItem(x_dugi,y_dugi);
      End;
      oList.Add(DEF_POLYLINE_END);

      //----
      //����� �����
      OtnX:=r_dugi;
      OtnY:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/Heig_m)*(Site1.GroundHeight+Site1.Antenna2Height);
      LeftY:=OtnY;


      oProject.Lines.AddItem(Graph.BoundsXY.MinX, Graph.BoundsXY.MinY, Graph.BoundsXY.MinX, Graph.BoundsXY.MinY+ OtnY);

      oList.Add(format(DEF_LINE,[Graph.BoundsXY.MinX, Graph.BoundsXY.MinY, Graph.BoundsXY.MinX, Graph.BoundsXY.MinY+ OtnY]));
      //�������
      y_dugi0:=(Graph.BoundsXY.MinY+OtnY);
      x_dugi0:=(Graph.BoundsXY.Minx+OtnX);

      // ---------------------------------------------------------------
      oPolyline := oProject.PolyLines.AddItem;


      oList.Add(DEF_POLYLINE);
      for i:=0 to r_dugi+1 -1 do
      Begin
       y_dugi:=(y_dugi0+i);
       x_dugi:=(x_dugi0)- Sqrt(Abs(Sqr(r_dugi)-sqr(y_dugi-(y_dugi0))));
       oList.Add(format(DEF_VERTEX,[x_dugi,y_dugi]));

       oPolyline.Vertexes.AddItem(x_dugi,y_dugi);
      End;
      oList.Add(DEF_POLYLINE_END);

      // ---------------------------------------------------------------
      oPolyline := oProject.PolyLines.AddItem;


      oList.Add(DEF_POLYLINE);
      for i:=0 to r_dugi+1 -1 do
      Begin
       y_dugi:=(y_dugi0-i);
       x_dugi:=(x_dugi0)- Sqrt(Abs(Sqr(r_dugi)-sqr(y_dugi-(y_dugi0))));
       oList.Add(format(DEF_VERTEX,[x_dugi,y_dugi]));

       oPolyline.Vertexes.AddItem(x_dugi,y_dugi);
      End;
      oList.Add(DEF_POLYLINE_END);

      //----
      //��������� �����

      oProject.Lines.AddItem(Graph.BoundsXY.MinX, Graph.BoundsXY.MinY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY);
      oProject.Lines.AddItem(Graph.BoundsXY.MinX, Graph.BoundsXY.MinY+ LeftY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY+ RightY);


      oList.Add(format(DEF_LINE,[Graph.BoundsXY.MinX, Graph.BoundsXY.MinY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY]));
      oList.Add(format(DEF_LINE,[Graph.BoundsXY.MinX, Graph.BoundsXY.MinY+ LeftY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY+ RightY]));
      //------
    end;

    // ---------------------------------------------------------------
    oPolyline := oProject.PolyLines.AddItem;


    //������ �����
    oList.Add(DEF_POLYLINE);
    for i:=0 to (DEF_POINT_COUNT+1) -1 do
    begin
      x_p:=Graph.BoundsXY.MinX+(((Graph.BoundsXY.MaxX-Graph.BoundsXY.MinX)/(DEF_POINT_COUNT))*i);
      x_km:=(((Length_Km)/(DEF_POINT_COUNT))*i);

      y_m:=geo_GetEarthHeight___(x_km, Length_Km );
      y_p:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/(Heig_m))*y_m;

      oList.Add(format(DEF_VERTEX,[x_p,Graph.BoundsXY.MinY+y_p]));

      oPolyline.Vertexes.AddItem(x_p,Graph.BoundsXY.MinY+y_p);
    end;
    oList.Add(DEF_POLYLINE_END);
    //---------

    //������� �������
    oProject.Texts.AddItem(Graph.MainBounds.x+Graph.AddressXY.x,Graph.MainBounds.y+Graph.AddressXY.y,Site1.Address,3.0,0.0);
    oProject.Texts.AddItem(Graph.MainBounds.x+Graph.Address2XY.x,Graph.MainBounds.y+Graph.Address2XY.y,Site2.Address,3.0,0.0);


    oList.Add(format(DEF_TEXT,[Graph.MainBounds.x+Graph.AddressXY.x,Graph.MainBounds.y+Graph.AddressXY.y,Site1.Address,3.0,0.0]));
    oList.Add(format(DEF_TEXT,[Graph.MainBounds.x+Graph.Address2XY.x,Graph.MainBounds.y+Graph.Address2XY.y,Site2.Address,3.0,0.0]));
    //-------

    //������
    oProject.Texts.AddItem(935.0 ,-994.0 ,'������ ������        ����. 0 ���.',3.0,0.0);
    oProject.Texts.AddItem(961.0 ,-994.0 ,FloattoStr(Site1.Azimuth),3.0,0.0);
    oProject.Texts.AddItem(935.0 ,-1000.0 ,'�������� ������        ����. 0 ���.',3.0,0.0);
    oProject.Texts.AddItem(962.0 ,-1000.0 ,FloattoStr(Site2.Azimuth),3.0,0.0);
    oProject.Texts.AddItem(947.0 ,-1031.0 , '5. �������� ������� �������    �;',3.0,0.0);
    oProject.Texts.AddItem(991.0 ,-1031.0 , FloattoStr(params.ref_plane),3.0,0.0);


    oList.Add(format(DEF_TEXT,[935.0 ,-994.0 ,'������ ������        ����. 0 ���.',3.0,0.0]));
    oList.Add(format(DEF_TEXT,[961.0 ,-994.0 ,FloattoStr(Site1.Azimuth),3.0,0.0]));
    oList.Add(format(DEF_TEXT,[935.0 ,-1000.0 ,'�������� ������        ����. 0 ���.',3.0,0.0]));
    oList.Add(format(DEF_TEXT,[962.0 ,-1000.0 ,FloattoStr(Site2.Azimuth),3.0,0.0]));
    oList.Add(format(DEF_TEXT,[947.0 ,-1031.0 , '5. �������� ������� �������    �;',3.0,0.0]));
    oList.Add(format(DEF_TEXT,[991.0 ,-1031.0 , FloattoStr(params.ref_plane),3.0,0.0]));
    //------

    //���������� ����� �������
    //����������
    oProject.Texts.AddItem(Graph.MainBounds.x+332 ,Graph.MainBounds.y+53 ,Ramka.masthead,5.0,0.0);
    oProject.Texts.AddItem(Graph.MainBounds.x+312 ,Graph.MainBounds.y+41 ,Ramka.name,5.0,0.0);
    oProject.Texts.AddItem(Graph.MainBounds.x+312 ,Graph.MainBounds.y+26 ,Ramka.appointment,3.0,0.0);
    oProject.Texts.AddItem(Graph.MainBounds.x+302 ,Graph.MainBounds.y+9  ,Ramka.location,3.0,0.0);


    oList.Add(format(DEF_TEXT,[Graph.MainBounds.x+332 ,Graph.MainBounds.y+53 ,Ramka.masthead,5.0,0.0]));
    oList.Add(format(DEF_TEXT,[Graph.MainBounds.x+312 ,Graph.MainBounds.y+41 ,Ramka.name,5.0,0.0]));
    oList.Add(format(DEF_TEXT,[Graph.MainBounds.x+312 ,Graph.MainBounds.y+26 ,Ramka.appointment,3.0,0.0]));
    oList.Add(format(DEF_TEXT,[Graph.MainBounds.x+302 ,Graph.MainBounds.y+9  ,Ramka.location,3.0,0.0]));
    //---------
  end;
  oList.Add(DEF_END);

  oList.SaveToFile(aFileName);
  oList.Free;
end;



end.

