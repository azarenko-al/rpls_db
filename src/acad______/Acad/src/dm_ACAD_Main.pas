unit dm_ACAD_Main;

interface

uses
  SysUtils, Classes, Dialogs,

   u_Vars,
  u_acad
  ;

type
  TdmACAD_Main = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
  private

  public

  end;

var
  dmACAD_Main: TdmACAD_Main;

//=============================================================================
implementation {$R *.dfm}
//=============================================================================


//------------------------------------------------------------------------------
procedure TdmACAD_Main.DataModuleCreate(Sender: TObject);
//------------------------------------------------------------------------------
const
  TEMP_INI_FILE = 'link_ACAD.ini';




var
  sIniFileName: String;
 // sDxfFileName: String;
  oAcad:TAcad ;
begin
 // sDxfFileName:='S:\-- tasks --\1.dxf';
 // sDxfFileName:='S:\-- tasks --\1.ini';


  sIniFileName:= ParamStr(1);
  If sIniFileName=''  then
    sIniFileName := g_ApplicationDataDir + TEMP_INI_FILE;


(*
  sDxfFileName:= ParamStr(1);
  If sDxfFileName=''  then
 //   sDxfFileName:='c:\temp\ACAD_test.dxf';
    sDxfFileName:='d:\1.dxf';


  sIniFileName:= ChangeFileExt(sDxfFileName, '.ini');

*)

//  sIniFileName:= ExtractFileDir(sDXFFileName) + '\' +
//      StringReplace(ExtractFileName(sDXFFileName), ExtractFileExt(sDXFFileName), '.ini', [rfReplaceAll]);

  if not FileExists(sIniFileName) then
  begin
    MessageDlg('File params ('+sIniFileName+') not found!', mtError, [mbOK],0);
    Exit;
  end;

  oAcad:=TAcad.Create;
  oAcad.Params.LoadFromIniFile (sIniFileName);
//  oAcad.Config.LoadFromIniFile (sIniFileName);
  oAcad.SaveLineToFile(sDxfFileName);
  oAcad.Free;

end;





end.
