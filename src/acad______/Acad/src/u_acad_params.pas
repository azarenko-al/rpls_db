unit u_acad_params;

interface

uses
  IniFiles,SysUtils,

  u_files,

  u_func
  ;

type
  TIni_Acad_params = class(TObject)
  public
    Length_km : double;
    Ref_plane : double; //��������, � �������  offset
  //..  Ref_plane : double; //��������, � �������


    Site1,Site2: record
      Address        : string;
      GroundHeight   : double;
      GroundText     : string;

      Azimuth        : double;

      AntennaHeight  : integer;
      Antenna2Height : integer;
      AntennaText    : string;

    end;

    Lines: array of record
      Height_m     : double;
      Distance_km  : double;
      Clutter      : string;
      Clu_Height_m : double;
    end;


    procedure LoadFromIniFile(aFileName: string);
    procedure SavetoIniFile(aFileName: string);
    
    function Validate: Boolean;
  end;


implementation

//-----------------------------------------------------------------------------
procedure TIni_Acad_params.LoadFromIniFile(aFileName: string);
//-----------------------------------------------------------------------------
var
  iCount,i: integer;
begin
  with TmemIniFile.Create(aFileName) do
  Begin
    Site1.Address       :=ReadString ('Site1','Address1','');
    Site1.GroundHeight  :=AsFloat(ReadString  ('Site1','GroundHeight1','0'));
    Site1.AntennaHeight :=ReadInteger('Site1','AntennaHeight1',0);
    Site1.Antenna2Height:=ReadInteger('Site1','Antenna2Height1',0);
    Site1.Azimuth        :=AsFloat(ReadString  ('Site1','Azimut1','0'));

    Site2.Address       :=ReadString ('Site2','Address2','');
    Site2.GroundHeight  :=AsFloat(ReadString  ('Site2','GroundHeight2','0'));
    Site2.AntennaHeight :=ReadInteger('Site2','AntennaHeight2',0);
    Site2.Antenna2Height:=ReadInteger('Site2','Antenna2Height2',0);
    Site2.Azimuth        :=AsFloat(ReadString  ('Site2','Azimut2','0'));

    Length_km:=AsFloat(ReadString ('lines','Length_km','0'));
    ref_plane:=AsFloat(ReadString ('lines','Ref_plane','0'));

    iCount   :=        ReadInteger('Lines','Count',0);

    SetLength(Lines,iCount);

    for i:=0 to iCount -1 do
    begin
      Lines[i].Height_m    := AsFloat(ReadString('Lines','Height_m'+IntToStr(i),'0'));
      Lines[i].Distance_km := AsFloat(ReadString('Lines','Distance_km'+IntToStr(i),'0'));
      Lines[i].Clutter     :=         ReadString('Lines','Clutter'+IntToStr(i),'0');
      Lines[i].Clu_Height_m:= AsFloat(ReadString('Lines','Clu_Height_m'+IntToStr(i),'0'));
    end;

    Free;
  end;
end;

//-----------------------------------------------------------------------------
procedure TIni_Acad_params.SavetoIniFile(aFileName: string);
//-----------------------------------------------------------------------------
var
  iCount,i: integer;
begin
  DeleteFile(aFileName);


  ForceDirByFileName(aFileName);



  with TmemIniFile.Create(aFileName) do
  begin
    WriteString ('Site1','Address1',       Site1.Address);
    WriteFloat  ('Site1','GroundHeight1',  Site1.GroundHeight);
    WriteInteger('Site1','AntennaHeight1', Site1.AntennaHeight);
    WriteInteger('Site1','Antenna2Height1',Site1.Antenna2Height);
    WriteFloat  ('Site1','Azimut1',        Site1.Azimuth);

    WriteString ('Site2','Address2',       Site2.Address);
    WriteFloat  ('Site2','GroundHeight2',  Site2.GroundHeight);
    WriteInteger('Site2','AntennaHeight2', Site2.AntennaHeight);
    WriteInteger('Site2','Antenna2Height2',Site2.Antenna2Height);
    WriteFloat  ('Site2','Azimut2',        Site2.Azimuth);

    WriteFloat  ('lines','Length_km', Length_km);
    WriteInteger('lines','Count',     Length(Lines));
    WriteFloat  ('lines','Ref_plane', ref_plane);

    for i:=0 to high(Lines) do
    begin
       WriteFloat( 'Lines','Height_m'    +IntToStr(i),Lines[i].Height_m);
       WriteFloat ('Lines','Distance_km' +IntToStr(i),Lines[i].Distance_km);
       WriteString('Lines','Clutter'     +IntToStr(i),Lines[i].Clutter);
       WriteFloat ('Lines','Clu_Height_m'+IntToStr(i),Lines[i].Clu_Height_m);
    end;

    UpdateFile;
    Free;
  end;
end;


function TIni_Acad_params.Validate: Boolean;
begin
  Result := (site1.AntennaHeight > 0) and
            (site2.AntennaHeight > 0);

end;


end.
