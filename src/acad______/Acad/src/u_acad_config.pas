unit u_acad_config;

interface

uses
  IniFiles,SysUtils,

  u_files
  ;


type
  TAcad_config = class(TObject)
  public
    Template_filename: string;

    Ramka:record
      masthead:string;
      name:string;
      appointment:string;
      location:string;
    end;

    Graph:record
       LeftBottom:record
         x: double;
         y: double;
       end;

       AddressXY:record
         x:double;
         y:double;
       end;

        Address2XY:record
         x:double;
         y:double;
       end;

       BoundsXY:record
         MinX: double;
         MaxX: double;
         MinY: double;
         MaxY: double;
       End;

       MainBounds:record
         x: double;
         y: double;
       end;
      end;

    constructor Create;
    procedure LoadFromIniFile(aFileName: string);
  end;

var
  IniFile: TIniFile;

implementation

constructor TAcad_config.Create;
begin
  inherited;

  Template_filename:= GetApplicationDir+'acad\TemplateACAD.DXF';

  Ramka.masthead:='test';
  Ramka.name:='test';
  Ramka.appointment:='test';
  Ramka.location:='test';


  Graph.MainBounds.x:=844;
  Graph.MainBounds.y:=-1055;

  Graph.BoundsXY.MinX:=916;
  Graph.BoundsXY.MinY:=-946;
  Graph.BoundsXY.MaxX:=1218;
  Graph.BoundsXY.MaxY:=-775;

  Graph.AddressXY.x:=58;
  Graph.AddressXY.y:=65;

  Graph.Address2XY.x:=361;
  Graph.Address2XY.y:=65;


end;

//------------------------------------------------------
procedure TAcad_config.LoadFromIniFile(aFileName: string);
//------------------------------------------------------
var iCount,i: integer;
begin
  with TIniFile.Create(aFileName) do
  Begin
    Template_filename:=ReadString('filename','Template',Template_filename);

    Ramka.masthead    :=ReadString('Ramka','masthead',   Ramka.masthead);
    Ramka.name        :=ReadString('Ramka','name',       Ramka.name);
    Ramka.appointment :=ReadString('Ramka','appointment',Ramka.appointment);
    Ramka.location    :=ReadString('Ramka','location',   Ramka.location);


    Graph.MainBounds.x:=ReadFloat('graph','MainBound.x', Graph.MainBounds.x);
    Graph.MainBounds.y:=ReadFloat('graph','MainBound.y', Graph.MainBounds.y);

    Graph.BoundsXY.MinX:=ReadFloat('graph','BoundsXY.MinX', Graph.BoundsXY.MinX);
    Graph.BoundsXY.MinY:=ReadFloat('graph','BoundsXY.MinY', Graph.BoundsXY.MinY);
    Graph.BoundsXY.MaxX:=ReadFloat('graph','BoundsXY.MaxX', Graph.BoundsXY.MaxX);
    Graph.BoundsXY.MaxY:=ReadFloat('graph','BoundsXY.MaxY', Graph.BoundsXY.MaxY);

    Graph.AddressXY.x:=ReadFloat('graph','AddressXY.x',Graph.AddressXY.x);
    Graph.AddressXY.y:=ReadFloat('graph','AddressXY.y',Graph.AddressXY.y);

    Graph.Address2XY.x:=ReadFloat('graph','Address2XY.x',Graph.Address2XY.x);
    Graph.Address2XY.y:=ReadFloat('graph','Address2XY.y',Graph.Address2XY.y);

    free;
  end;

end;

end.
