program ACAD;

uses
  Forms,
  dm_ACAD_Main in 'src\dm_ACAD_Main.pas' {dmACAD_Main: TDataModule},
  u_Acad in 'src\u_Acad.pas',
  u_acad_config in 'src\u_acad_config.pas',
  u_acad_params in 'src\u_acad_params.pas',
  u_ACAD_classes in '..\..\Link_ACAD\src\u_ACAD_classes.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TdmACAD_Main, dmACAD_Main);
  Application.Run;
end.
