object dlg_LinkFreqPlan_CIA_maps_data_test: Tdlg_LinkFreqPlan_CIA_maps_data_test
  Left = 572
  Top = 347
  Width = 626
  Height = 417
  Caption = 'dlg_LinkFreqPlan_CIA_maps_data_test'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 618
    Height = 339
    ActivePage = TabSheet1
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'LinkFreqPlan'
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 610
        Height = 311
        Align = alClient
        DataSource = ds_LinkFreqPlan
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'LinkFreqPlan_LinkEnd'
      ImageIndex = 1
      object DBGrid2: TDBGrid
        Left = 0
        Top = 0
        Width = 610
        Height = 311
        Align = alClient
        DataSource = ds_LinkFreqPlan_LinkEnd
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Neighbors'
      ImageIndex = 2
      object DBGrid3: TDBGrid
        Left = 0
        Top = 0
        Width = 610
        Height = 311
        Align = alClient
        DataSource = ds_Neighbors
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
  end
  object ds_LinkFreqPlan: TDataSource
    DataSet = dmLinkFreqPlan_calc.qry_LinkFreqPlan
    Left = 100
    Top = 72
  end
  object ds_LinkFreqPlan_LinkEnd: TDataSource
    DataSet = dmLinkFreqPlan_calc.qry_LinkFreqPlan_LinkEnd
    Left = 100
    Top = 128
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 376
    Top = 96
  end
  object ds_Neighbors: TDataSource
    DataSet = dmLinkFreqPlan_calc.qry_RRL_Neighbors
    Left = 100
    Top = 192
  end
end
