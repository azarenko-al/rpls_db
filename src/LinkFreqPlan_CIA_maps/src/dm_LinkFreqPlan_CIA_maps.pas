unit dm_LinkFreqPlan_CIA_maps;

interface

{$DEFINE db}

uses
  SysUtils, Classes, Graphics, Forms, Db, ADODB, Dialogs, Variants, // Windows,

  dm_Main,

  u_LinkFreqPlan_CIA_map_classes,

  u_LinkFreqPlan_classes,

  dm_LinkFreqPlan,
 // dm_LinkNet,

  dm_CalcMap,

  dm_Progress,

//  u_mitab,

  u_func,
  u_db,
  
  u_geo,

  u_MapX,
  u_MapX_lib,


  u_const_db;


type
  TdmLinkFreqPlan_CIA_maps = class(TdmProgress)
    qry_LinkFreqPlan_LinkEnd: TADOQuery;
    qry_LinkFreqPlan: TADOQuery;
    qry_LinkEnd_NeighBors: TADOQuery;
    qry_Link: TADOQuery;
    qry_Temp: TADOQuery;

    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    
  private
    FParams: TParams_for_maps;

    Fdata: TnbData;


//    FLinkFreqPlan: TLinkFreqPlan;

///////////    FDBNeighbors: TDBNeighbor_List;
//    FDBLinkFreqPlan_Linkends: TnbLinkFreqPlan_Linkend_List;


    procedure RegisterMaps (aLinkFreqPlanID: integer);

    procedure BuildMap;

  protected
    procedure ExecuteProc; override;

  public
    Params: record
              LinkFreqPlanID: Integer;

            end;
              
    class procedure Init;

    procedure OpenData;
  end;


var
  dmLinkFreqPlan_CIA_maps: TdmLinkFreqPlan_CIA_maps;

//==========================================================================
implementation

uses u_db_manager; {$R *.DFM}
//==========================================================================

const
  arrFileTypes: array[0..2] of TdmLinkFreqPlanFileType = (ftCI,ftCA,ftCIA);

 // FLD_THRESHOLD_DEGRADATION_CIA = 'threshold_degradation_CIA';


//--------------------------------------------------------------------------
class procedure TdmLinkFreqPlan_CIA_maps.Init;
//--------------------------------------------------------------------------
begin
  if not Assigned(dmLinkFreqPlan_CIA_maps) then
    dmLinkFreqPlan_CIA_maps:=TdmLinkFreqPlan_CIA_maps.Create(Application);
end;

//--------------------------------------------------------------------------
procedure TdmLinkFreqPlan_CIA_maps.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------------
begin
  inherited;

  db_SetComponentADOConnection (Self, dmMain.ADOConnection);


//  FLinkFreqPlan := TnbLinkFreqPlan.Create();
  
//////////////  FDBNeighbors := TDBNeighbor_List.Create();
//  FDBLinkFreqPlan_Linkends := TnbLinkFreqPlan_Linkend_List.Create();
  FParams := TParams_for_maps.Create();

  FData:=TnbData.Create;

end;


procedure TdmLinkFreqPlan_CIA_maps.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FParams);

  FreeAndNil(FData);


//  FreeAndNil(FDBLinkFreqPlan_Linkends);
/////////////  FreeAndNil(FDBNeighbors);
//  FreeAndNil(FLinkFreqPlan);
end;


//--------------------------------------------------------------------------
procedure TdmLinkFreqPlan_CIA_maps.BuildMap;
//--------------------------------------------------------------------------
var
  arrBLPoint: TBLPointArrayF;
  blVector: TBLVector;


  //----------------------------------------------------------------
  function GetLinkDistance_m(aLinkEndID: integer): double;
  // ---------------------------------------------------------------
  var blVector: TBLVector;
  begin
    db_OpenQuery (qry_Link,
                 'SELECT id, lat1, lon1, lat2, lon2 '+
                 ' FROM '+ view_link + ' WHERE (LinkEnd1_id=:id) or (LinkEnd2_id=:id)',
                 [db_Par(FLD_ID, aLinkEndID)]);

    if qry_Link.IsEmpty then
      Result:=0
    else
    begin
      blVector:= db_ExtractBLVector(qry_Link);
      Result:=geo_Distance_m (blVector);
    end;
  end;

  //----------------------------------------------------------------
  function DoGetAzimuth_LinkEndID (aLinkEndID: integer): double;
  // ---------------------------------------------------------------
  begin
    dmOnega_db_data.OpenQuery(qry_Temp, 'SELECT azimuth FROM ' + TBL_LINKEND_ANTENNA +
                           ' WHERE linkend_id=:linkend_id',
                             [FLD_LINKEND_ID, aLinkEndID]);

    Result :=  qry_Temp.FieldByName(FLD_AZIMUTH).AsFloat;
  end;

  //----------------------------------------------------------------
//  procedure DoWrite_line (aMap: TmiMap; aLabel: string;
  //----------------------------------------------------------------
//  procedure DoWrite_line(aMap: TmitabMap; aLabel: string;
  procedure DoWrite_line(aMap: TmiMap; aLabel: string;
                         aColor, aWidth, aLinkEndID: Integer;
                         aIsBezier: boolean);
  // ---------------------------------------------------------------
  var
    rStyle: TmiStyleRec;
  begin

    rStyle.LineColor:=aColor;
    rStyle.LineWidth:=aWidth;
    rStyle.LineStyle:=IIF(aIsBezier, MI_PEN_SOLID, MI_PEN_PATTERN_ARROW);

    aMap.PrepareStyle (miFeatureTypeLine, rStyle);

    if aIsBezier then
      aMap.WriteBezierF (arrBLPoint, // rStyle,
                    [mapx_Par(FLD_ID,      aLinkEndID),
                     mapx_Par(FLD_NAME,    aLabel)//,
                   //  mapx_Par(FLD_OBJNAME, '')
                     ])
    else
      aMap.WriteVector (blVector, // rStyle,
                    [mapx_Par(FLD_ID,      aLinkEndID),
                     mapx_Par(FLD_NAME,    'Arrow')//,
                    // mapx_Par(FLD_OBJNAME, '')
                     ]);

  end;




var
  bL: TBLPoint;
  I: Integer;
  blPoint1: TBLPoint;
  dLength, dAdmitted_CI, dAzimuth, dAdmitted_Degradation: Double;
  iID1: Integer;
  iID2: Integer;


//  oCIMiMap,oCAMiMap,oCIAMiMap: TmitabMap; //TmiMap;
  oCIMiMap,oCAMiMap,oCIAMiMap: TmiMap; //TmiMap;


  iLinkEndID1,iLinkEndID2: integer;
  iLINKEND_ID: Integer;
  sFileName: string;

   // oDBLinkFreqPlan_Linkend: TnbLinkFreqPlan_Linkend;

begin
  arrBLPoint.Count:= 4;

 // ShowMessage('1');
(*
  oCIMiMap :=TmitabMap.Create;
  oCAMiMap :=TmitabMap.Create;
  oCIAMiMap:=TmitabMap.Create;
*)

  oCIMiMap :=TmiMap.Create;
  oCAMiMap :=TmiMap.Create;
  oCIAMiMap:=TmiMap.Create;


 // ShowMessage('2');

  //�������� ����� CI
  sFileName:=dmLinkFreqPlan.GetMapFileName(Params.LinkFreqPlanID, ftCI);

  ShowMessage ('procedure TdmLinkFreqPlan_CIA_maps.BuildMap;   '+sFileName);

  oCIMiMap.CreateFile1(sFileName, //dmLinkFreqPlan.GetMapFileName(Params.LinkFreqPlanID, ftCI),
                  [mapX_Field (FLD_ID,      miTypeInt),
                   mapX_Field (FLD_NAME,    miTypeString)
                //   mapX_Field (FLD_OBJNAME, miTypeString)
                   ]);
  //�������� ����� CA
  sFileName:=dmLinkFreqPlan.GetMapFileName(Params.LinkFreqPlanID, ftCA);

  oCAMiMap.CreateFile1(sFileName, //dmLinkFreqPlan.GetMapFileName(Params.LinkFreqPlanID, ftCA),
                  [mapX_Field (FLD_ID,      miTypeInt),
                   mapX_Field (FLD_NAME,    miTypeString)
                 //  mapX_Field (FLD_OBJNAME, miTypeString)
                   ]);
  //�������� ����� CIA
  sFileName:=dmLinkFreqPlan.GetMapFileName(Params.LinkFreqPlanID, ftCIA);

  oCIAMiMap.CreateFile1(sFileName, //dmLinkFreqPlan.GetMapFileName(Params.LinkFreqPlanID, ftCIA),
                  [mapX_Field (FLD_ID,      miTypeInt),
                   mapX_Field (FLD_NAME,    miTypeString)
                  // mapX_Field (FLD_OBJNAME, miTypeString)
                   ]);

  dmOnega_db_data.OpenQuery(qry_LinkFreqPlan,
     'SELECT * FROM '+ TBL_LINKFREQPLAN +' WHERE id=:id',
       [FLD_ID, Params.LinkFreqPlanID]);

  FData.LinkFreqPlan.LoadFromDataset(qry_LinkFreqPlan);
  FParams.LinkFreqPlan.LoadFromDataset(qry_LinkFreqPlan);

//  FLinkFreqPlan.LoadFromDataset(qry_LinkFreqPlan);

  dAdmitted_CI          := FParams.LinkFreqPlan.MIN_CI_dB;
  dAdmitted_Degradation := FParams.LinkFreqPlan.Threshold_degradation_dB;


  dAdmitted_CI          := qry_LinkFreqPlan.FieldByName(FLD_MIN_CI_dB).AsFloat;
  dAdmitted_Degradation := qry_LinkFreqPlan.FieldByName(FLD_THRESHOLD_DEGRADATION).AsFloat;


 // dAdmitted_CI          := FLinkFreqPlan.MIN_CI_dB;//  CI;
 // dAdmitted_Degradation := FLinkFreqPlan.THRESHOLD_DEGRADATION;


  dmOnega_db_data.OpenQuery(qry_LinkFreqPlan_LinkEnd,
//        'SELECT id, linkend_id, LinkEnd_name, CIA, CI, CA,  '+ //Link_FreqPlan_id,
        'SELECT linkend_id, LinkEnd_name, CIA, CI, CA,  '+ //Link_FreqPlan_id,
//        '       lat, lon, threshold_degradation_CIA '+
        '       property_lat, property_lon, threshold_degradation_CIA '+
        ' FROM '+ VIEW_LINKFREQPLAN_LINKEND +
        ' WHERE LinkFreqPlan_id=:LinkFreqPlan_id',
              [FLD_LinkFreqPlan_ID, Params.LinkFreqPlanID]);

 // FData.LInkends.LoadFromDataset(qry_LinkFreqPlan_LinkEnd);
  FParams.LInkends.LoadFromDataset(qry_LinkFreqPlan_LinkEnd);

  qry_LinkFreqPlan_LinkEnd.First;

  //for I := 0 to FDBLinkFreqPlan_Linkends.Count - 1 do    // Iterate
  while not qry_LinkFreqPlan_LinkEnd.Eof do
  begin
    //������� 1 � 2 linkend
    dmOnega_db_data.OpenQuery (qry_Link,
        'SELECT id,linkend1_id,linkend2_id,lat1,lon1,lat2,lon2 FROM '
         + view_LINK +
        ' WHERE (LinkEnd1_id=:id) or (LinkEnd2_id=:id)',
      [FLD_ID, qry_LinkFreqPlan_LinkEnd[FLD_LINKEND_ID]]);

    iLinkEndID1:= qry_Link[FLD_LINKEND1_ID];
    iLinkEndID2:= qry_Link[FLD_LINKEND2_ID];
    blVector:=db_ExtractBLVector (qry_Link);

    //�������� ������� �����, ���� ����� ����������
    if qry_LinkFreqPlan_LinkEnd[FLD_LINKEND_ID]=iLinkEndID1 then
    begin
      blPoint1:=blVector.Point2;
      blVector.Point2:=blVector.Point1;
      blVector.Point1:=blPoint1;

      iLinkEndID2:= qry_Link[FLD_LINKEND1_ID];
      iLinkEndID1:= qry_Link[FLD_LINKEND2_ID];
    end;

    dLength :=geo_Distance_m(blVector);
    dAzimuth:=geo_Azimuth(blVector.Point1,blVector.Point2);
    //������� ����� �� �������� ���������
    blVector.Point1:=geo_RotateByAzimuth(blVector.Point1, dLength/2+dLength/20, dAzimuth);
    blVector.Point2:=geo_RotateByAzimuth(blVector.Point2, dLength/20, IIF(dAzimuth<180, dAzimuth+180, dAzimuth-180));

    if qry_LinkFreqPlan_LinkEnd['CIA'] <> NULL then
      if (AsFloat(qry_LinkFreqPlan_LinkEnd['CIA']) < dAdmitted_CI) or
         (AsFloat(qry_LinkFreqPlan_LinkEnd[FLD_THRESHOLD_DEGRADATION_CIA]) > dAdmitted_Degradation)
      then
        DoWrite_line(oCIAmiMap,'',clNavy,5, iLinkEndID2, False);

    if qry_LinkFreqPlan_LinkEnd['CI'] <> NULL then
      if AsFloat(qry_LinkFreqPlan_LinkEnd['CI']) < dAdmitted_CI
      then
        DoWrite_line(oCImiMap,'',clMaroon,4, iLinkEndID2, False);

    if qry_LinkFreqPlan_LinkEnd['CA'] <> NULL then
      if AsFloat(qry_LinkFreqPlan_LinkEnd['CA']) < dAdmitted_CI
      then
        DoWrite_line(oCAmiMap,'',clPurple,3, iLinkEndID2, False);

    //������������ ������ �����
    if (qry_LinkFreqPlan_LinkEnd[FLD_CIA]<>null) and
       ((qry_LinkFreqPlan_LinkEnd.FieldByName(FLD_CIA).AsFloat < dAdmitted_CI) or
        (AsFloat(qry_LinkFreqPlan_LinkEnd[FLD_THRESHOLD_DEGRADATION_CIA]) > dAdmitted_Degradation))  then
    begin
      iLINKEND_ID :=qry_LinkFreqPlan_LinkEnd[FLD_LINKEND_ID];

//      FData.Neighbors.SelectByRxLinkendID(iLINKEND_ID);


      dmOnega_db_data.OpenQuery(qry_LinkEnd_NeighBors,

              'SELECT * FROM '+ view_LinkFreqPlan_Neighbors +
              ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) AND '+
              '       (rx_linkend_id=:rx_linkend_id)',

            [FLD_LINKFREQPLAN_ID, Params.LinkFreqPlanID,
             FLD_RX_LINKEND_ID, qry_LinkFreqPlan_LinkEnd[FLD_LINKEND_ID]
             ]);

   //   db_ViewDataSet(qry_LinkEnd_NeighBors);

/////////////      FDBNeighbors.LoadFromDataset(qry_LinkEnd_NeighBors);


      qry_LinkEnd_NeighBors.First;


   //   db_ViewDataSet(qry_LinkEnd_NeighBors);

      if not qry_LinkEnd_NeighBors.IsEmpty then
      begin
        bL.B:=qry_LinkFreqPlan_LinkEnd.FieldByName(FLD_PROPERTY_LAT).AsFloat;
        bL.L:=qry_LinkFreqPlan_LinkEnd.FieldByName(FLD_PROPERTY_LON).AsFloat;

        arrBLPoint.Items[0]:=bL; // db_ExtractBLPoint(qry_LinkFreqPlan_LinkEnd);
       // arrBLPoint.Items[0]:= db_ExtractBLPoint(qry_LinkFreqPlan_LinkEnd);


        //-------------------------------------------------------------------
/////////////        for I := 0 to FDBNeighbors.Count - 1 do
        begin


{          arrBLPoint.Items[3].B:= FieldByName(FLD_LAT2).AsFloat;
          arrBLPoint.Items[3].L:= FieldByName(FLD_LON2).AsFloat;
}


{          if (FDBNeighbors[i].CIA < dAdmitted_CI) or
             (AsFloat(FDBNeighbors[i].THRESHOLD_DEGRADATION_CIA) > dAdmitted_Degradation) then
          begin
            arrBLPoint.Items[1]:=geo_RotateByAzimuth(arrBLPoint.Items[0],
                GetLinkDistance_m(qry_LinkFreqPlan_LinkEnd[FLD_LINKEND_ID])/8,
                                DoGetAzimuth_LinkEndID(qry_LinkFreqPlan_LinkEnd[FLD_LINKEND_ID]));

            arrBLPoint.Items[2]:=geo_RotateByAzimuth(arrBLPoint.Items[3],
                                    GetLinkDistance_m(qry_LinkEnd_NeighBors[FLD_TX_ID])/2,
                                            DoGetAzimuth_LinkEndID(FDBNeighbors[i].TX_ID));

//            DoWrite_line(oCIAmiMap, qry_LinkEnd_NeighBors.FieldByName(FLD_CIA).AsString,
  //                                  clRed, 3, FDBNeighbors[i].RX_LINK_ID, True);
          end;
          //else
           // begin Next; Continue; end;

}
        end;


     //   end;

        //-------------------------------------------------------------------


        with qry_LinkEnd_NeighBors do
            while not Eof do
        begin
          arrBLPoint.Items[3].B:= FieldByName(FLD_LAT2).AsFloat;
          arrBLPoint.Items[3].L:= FieldByName(FLD_LON2).AsFloat;

          if (qry_LinkEnd_NeighBors[FLD_CIA] < dAdmitted_CI) or
             (AsFloat(qry_LinkEnd_NeighBors[FLD_THRESHOLD_DEGRADATION_CIA]) > dAdmitted_Degradation) then
          begin
            iID1 := qry_LinkFreqPlan_LinkEnd[FLD_LINKEND_ID];

            iID2 := qry_LinkEnd_NeighBors[FLD_TX_LINKEND_ID];


            arrBLPoint.Items[1]:=geo_RotateByAzimuth(arrBLPoint.Items[0],
                GetLinkDistance_m(qry_LinkFreqPlan_LinkEnd[FLD_LINKEND_ID])/8,
                                DoGetAzimuth_LinkEndID(qry_LinkFreqPlan_LinkEnd[FLD_LINKEND_ID]));

            arrBLPoint.Items[2]:=geo_RotateByAzimuth(arrBLPoint.Items[3],
                          GetLinkDistance_m(qry_LinkEnd_NeighBors[FLD_TX_LINKEND_ID])/2,
                                  DoGetAzimuth_LinkEndID(qry_LinkEnd_NeighBors[FLD_TX_LINKEND_ID]));

            DoWrite_line(oCIAmiMap, qry_LinkEnd_NeighBors.FieldByName(FLD_CIA).AsString,
                         clRed, 3, qry_LinkEnd_NeighBors[FLD_RX_LINKEND_ID], True);
          end;
          //else
           // begin Next; Continue; end;

          Next;
        end;

      end;
    end;

    qry_LinkFreqPlan_LinkEnd.Next;
  end;



  FreeAndNil(oCIMiMap);
  FreeAndNil(oCAMiMap);
  FreeAndNil(oCIAMiMap);

end;

//--------------------------------------------------------------------------
procedure TdmLinkFreqPlan_CIA_maps.RegisterMaps(aLinkFreqPlanID: integer);
//--------------------------------------------------------------------------
var
  i, iID,  iLinkNetID: integer;
//  sKey,
  sFreqPlanName, sLinkNetName, sName, sMap_fname: string;

  rec_map: TdmCalcMapAddRec1;
 //.. rec_sec: TdmCalcMapSectionAddRec;

begin
//   with dmCalcMap do
//   begin
 // Zeromemory(@rec_sec, SizeOf(rec_sec));

 // FillChar(rec_sec,SizeOf(rec_sec),0);
  FillChar(rec_map,SizeOf(rec_map),0);

 // rec_sec.Name   :='�� ����';
 // rec_sec.KeyName:='LinkNet';

//  iID:=dmCalcMap.RegisterSection(rec_sec, 0,'�� ����','LinkNet');

  sFreqPlanName:= FParams.LinkFreqPlan.Name;
  iLinkNetID   := FParams.LinkFreqPlan.LINKNET_ID;

{  sFreqPlanName:= dmLinkFreqPlan.GetNameByID(aLinkFreqPlanID);
  iLinkNetID   := dmLinkFreqPlan.GetIntFieldValue(aLinkFreqPlanID, FLD_LINKNET_ID);

}

  sLinkNetName := gl_DB.GetNameByID(TBL_LINKNET, iLinkNetID);
//  sLinkNetName := dmLinkNet.GetNameByID(iLinkNetID);

 //���������������� ���� � ������ ���� �� linknet_id � �������� ��� id
 // rec_sec.ParentID:=iID;
//  rec_sec.Name   :=sLinkNetName;
 // rec_sec.KeyName:='LinkNet.'+IntToStr(iLinkNetID);

//  iID:=dmCalcMap.RegisterSection(rec_sec, iID, sLinkNetName, 'LinkNet.'+IntToStr(iLinkNetID));

  //���������������� ���� � ������ ���������� ����� � �������� ��� id
 // sKey:= Format('LinkNet.%d-LinkFreqPlan.%d', [iLinkNetID, aLinkFreqPlanID]);

//  FillChar(rec_sec,SizeOf(rec_sec),0);
//  rec_sec.ParentID:=iID;
 // rec_sec.Name    :=sFreqPlanName;
 // rec_sec.KeyName :=sKey;

//  iParentID:=dmCalcMap.RegisterSection(rec_sec, iID, sFreqPlanName, sKey);

//  FillChar(rec_sec,SizeOf(rec_sec),0);

  for i:=0 to High(arrFileTypes) do
  begin
    case arrFileTypes[i] of
      ftCI : sName:='CI';
      ftCA : sName:='CA';
      ftCIA: sName:='CIA';
    end;

    sMap_fname:=dmLinkFreqPlan.GetMapFileName(aLinkFreqPlanID, arrFileTypes[i]);


  //  rec_map.ParentID:=iParentID;
    rec_map.name    :='����� '+ sName;
//    rec_map.FileName:=sMap_fname;
    rec_map.TabFileName:=sMap_fname;

    rec_map.LinkFreqPlanID := aLinkFreqPlanID ;

  //  rec_map.KeyName :=sKey+'-'+sName;

    dmCalcMap.RegisterCalcMap_(rec_map); //, iParentID, 0, '����� '+ sName, sMap_fname, sKey+'-'+sName, True);
  end;
//  end;
end;


procedure TdmLinkFreqPlan_CIA_maps.ExecuteProc;
begin
  BuildMap;
  RegisterMaps(Params.LinkFreqPlanID);
end;


// ---------------------------------------------------------------
procedure TdmLinkFreqPlan_CIA_maps.OpenData;
// ---------------------------------------------------------------
begin

  dmOnega_db_data.OpenQuery(qry_LinkFreqPlan,
     'SELECT * FROM '+ TBL_LINKFREQPLAN +' WHERE id=:id',
       [FLD_ID, Params.LinkFreqPlanID]);

  FData.LinkFreqPlan.LoadFromDataset(qry_LinkFreqPlan);

//  FLinkFreqPlan.LoadFromDataset(qry_LinkFreqPlan);


  dmOnega_db_data.OpenQuery(qry_LinkFreqPlan_LinkEnd,
//        'SELECT id, linkend_id, LinkEnd_name, CIA, CI, CA,  '+ //Link_FreqPlan_id,
        'SELECT *  '+ //Link_FreqPlan_id,
//        '       lat, lon, threshold_degradation_CIA '+
        ' FROM '+ VIEW_LINKFREQPLAN_LINKEND +
        ' WHERE LinkFreqPlan_id=:LinkFreqPlan_id',
              [FLD_LinkFreqPlan_ID, Params.LinkFreqPlanID ]);

  FData.LInkends.LoadFromDataset(qry_LinkFreqPlan_LinkEnd);



  dmOnega_db_data.OpenQuery(qry_LinkEnd_NeighBors,

          'SELECT * FROM '+ view_LinkFreqPlan_Neighbors +   ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) ' ,
     //     '       (rx_linkend_id=:rx_linkend_id)',

        [
          FLD_LINKFREQPLAN_ID, Params.LinkFreqPlanID
     //    db_par(FLD_RX_LINKEND_ID, qry_LinkFreqPlan_LinkEnd[FLD_LINKEND_ID])
         ]);

  FData.Neighbors.LoadFromDataset(qry_LinkEnd_NeighBors);


  // TODO -cMM: TdmLinkFreqPlan_CIA_maps.OpenData default body inserted
end;



begin


end.
