unit dm_Main_app__CIA_maps;

interface

uses
  SysUtils, Classes,  Dialogs, IniFiles,

  u_Log,
  u_vars,

  dm_LinkFreqPlan_CIA_maps,

  dm_Main
  ;

type
  TdmMain_app__CIA_maps = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);

  end;


var
  dmMain_app__CIA_maps: TdmMain_app__CIA_maps;

//===================================================================
implementation {$R *.DFM}
//===================================================================


//--------------------------------------------------------------
procedure TdmMain_app__CIA_maps.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const
 // DEF_SOFTWARE_NAME = 'RPLS_DB_LINK';
  DEF_TEMP_FILENAME = 'LinkFreqPlan_CIA_maps.ini';


var
  iProjectID: Integer;
  sIniFileName: string;
  oIni: TIniFile;

  iLinkFreqPlanID: integer;

begin
  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
    sIniFileName:=g_ApplicationDataDir+  DEF_TEMP_FILENAME;

//    sIniFileName:=TEMP_INI_FILE;

//    sIniFileName:=GetCommonApplicationDataDir(DEF_SOFTWARE_NAME)+
  //                DEF_TEMP_FILENAME;


  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;




  oIni:=TIniFile.Create (sIniFileName);

  iLinkFreqPlanID  :=oIni.ReadInteger('main', 'LinkFreqPlanID',  0);
  iProjectID       :=oIni.ReadInteger('main', 'ProjectID',      0);

  oIni.Free;

  if (iProjectID=0) or (iLinkFreqPlanID=0) then
  begin
    ShowMessage('(iProjectID=0) or (iCalcRegionID=0)');
    Exit;
  end;
{
  Assert (iProjectID<>0);
  Assert (iLinkFreqPlanID>0);
}

  TdmMain.Init;
  if not dmMain.OpenDB_reg then
    Exit;

  dmMain.ProjectID:=iProjectID;


  TdmLinkFreqPlan_CIA_maps.Init;


  with dmLinkFreqPlan_CIA_maps do
  begin
    Params.LinkFreqPlanID:= iLinkFreqPlanID;

    ExecuteDlg ('������ ���� �������������');
  end;

end;


end.
