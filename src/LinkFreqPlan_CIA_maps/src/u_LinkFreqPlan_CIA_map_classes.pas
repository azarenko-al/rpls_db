unit u_LinkFreqPlan_CIA_map_classes;

interface

uses
  Classes, DB, SysUtils,

  u_const_db
  ;

type

  // ---------------------------------------------------------------
  TLinkFreqPlan_Linkend = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
    linkend_id: Integer;
    LinkEnd_name: string;

(*    ca: Double;
    CI: Double;
    CIA: Double;
*)
    ca: Variant;
    CI: Variant;
    CIA: Variant;


    Property_lon: Double;
    Property_lat: Double;

    threshold_degradation_CIA: Double;

    procedure LoadFromDataset(aDataset: TDataSet);
  public
  end;


  // ---------------------------------------------------------------
  TLinkFreqPlan_NeighBor = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
    TX_linkend_id: Integer;
    RX_linkend_id: Integer;

    procedure LoadFromDataset(aDataset: TDataSet);
  public
  end;


 TLinkFreqPlan_NeighBor_List = class(TCollection)
  private
    function GetItems(Index: Integer): TLinkFreqPlan_NeighBor;
  public
    constructor Create;

    function AddItem: TLinkFreqPlan_NeighBor;

    procedure LoadFromDataset(aDataset: TDataSet);

    property Items[Index: Integer]: TLinkFreqPlan_NeighBor read GetItems;
        default;
  end;



  TLinkFreqPlan_Linkend_List = class(TCollection)
  private
    function GetItems(Index: Integer): TLinkFreqPlan_Linkend;
  public
    constructor Create;

    function AddItem: TLinkFreqPlan_Linkend;

    procedure LoadFromDataset(aDataset: TDataSet);

    property Items[Index: Integer]: TLinkFreqPlan_Linkend read GetItems;
        default;
  end;


  // ---------------------------------------------------------------
  TLinkFreqPlan = class
  // ---------------------------------------------------------------
  public
    Name                     : string;
    LINKNET_ID               : Integer;
    MIN_CI_dB                : Double;
    Threshold_degradation_dB : Double;

    procedure LoadFromDataset(aDataset: TDataset);
  end;


  // ---------------------------------------------------------------
  TParams_for_maps = class(TObject)
  // ---------------------------------------------------------------
  public
    LinkFreqPlan: TLinkFreqPlan;

    Linkends: TLinkFreqPlan_Linkend_List;
    NeighBors: TLinkFreqPlan_NeighBor_List;


    constructor Create;
    destructor Destroy; override;
  end;


implementation

constructor TLinkFreqPlan_Linkend_List.Create;
begin
  inherited Create(TLinkFreqPlan_Linkend);
end;

function TLinkFreqPlan_Linkend_List.AddItem: TLinkFreqPlan_Linkend;
begin
  Result := TLinkFreqPlan_Linkend (inherited Add);
end;

function TLinkFreqPlan_Linkend_List.GetItems(Index: Integer):
    TLinkFreqPlan_Linkend;
begin
  Result := TLinkFreqPlan_Linkend(inherited Items[Index]);
end;


procedure TLinkFreqPlan_Linkend_List.LoadFromDataset(aDataset: TDataset);
begin
  Clear;
  aDataset.First;

  while not aDataset.EOF do
  begin
    AddItem.LoadFromDataset(aDataset);
    aDataset.Next;
  end;
end;

procedure TLinkFreqPlan_Linkend.LoadFromDataset(aDataset: TDataset);
begin
  with aDataset do
  begin
    linkend_id   := FieldByName(FLD_linkend_id).AsInteger;
    LinkEnd_name := FieldByName(FLD_LinkEnd_name).AsString;

    CIA:= FieldByName(FLD_CIA).AsVariant;
    CI := FieldByName(FLD_CA).AsVariant;
    ca := FieldByName(FLD_CI).AsVariant;

    Property_lat := FieldByName(FLD_Property_lat).AsFloat;
    Property_lon := FieldByName(FLD_Property_lon).AsFloat;


    threshold_degradation_CIA:= FieldByName(FLD_threshold_degradation_CIA).AsFloat;

  end;

end;

procedure TLinkFreqPlan.LoadFromDataset(aDataset: TDataset);
begin
  with aDataset do
  begin
    Self.Name                := FieldByName(FLD_Name).AsString;
    LINKNET_ID               := FieldByName(FLD_LINKNET_ID).AsInteger;
    MIN_CI_dB                := FieldByName(FLD_MIN_CI_dB).AsFloat;
    THRESHOLD_DEGRADATION_dB := FieldByName(FLD_THRESHOLD_DEGRADATION).AsFloat;
  end;
end;


constructor TParams_for_maps.Create;
begin
  inherited Create;
  LinkFreqPlan := TLinkFreqPlan.Create();
  Linkends := TLinkFreqPlan_Linkend_List.Create();
  NeighBors := TLinkFreqPlan_NeighBor_List.Create();
end;


destructor TParams_for_maps.Destroy;
begin
  FreeAndNil(NeighBors);
  FreeAndNil(Linkends);
  FreeAndNil(LinkFreqPlan);
  inherited Destroy;
end;


procedure TLinkFreqPlan_NeighBor.LoadFromDataset(aDataset: TDataSet);
begin
  with aDataset do
  begin
  //  MIN_CI_dB             := FieldByName(FLD_MIN_CI_dB).AsFloat;
  //  THRESHOLD_DEGRADATION := FieldByName(FLD_THRESHOLD_DEGRADATION).AsFloat;
  end;

end;



constructor TLinkFreqPlan_NeighBor_List.Create;
begin
  inherited Create(TLinkFreqPlan_NeighBor);
end;

function TLinkFreqPlan_NeighBor_List.AddItem: TLinkFreqPlan_NeighBor;
begin
  Result := TLinkFreqPlan_NeighBor (inherited Add);
end;

function TLinkFreqPlan_NeighBor_List.GetItems(Index: Integer):
    TLinkFreqPlan_NeighBor;
begin
  Result := TLinkFreqPlan_NeighBor(inherited Items[Index]);
end;

procedure TLinkFreqPlan_NeighBor_List.LoadFromDataset(aDataset: TDataSet);
begin

end;



end.
