program dll_LinkFreqPlan_CIA_maps;

uses
  Forms,
  dm_Progress in '..\..\..\..\common\Package_Common\dm_Progress.pas' {dmProgress: TDataModule},
  dm_LinkFreqPlan_CIA_maps in '..\src\dm_LinkFreqPlan_CIA_maps.pas' {dmLinkFreqPlan_CIA_maps: TDataModule},
  dm_Main_app__CIA_maps in '..\src\dm_Main_app__CIA_maps.pas' {dmMain_app__CIA_maps: TDataModule},
  u_LinkFreqPlan_CIA_map_classes in '..\src\u_LinkFreqPlan_CIA_map_classes.pas',
  d_LinkFreqPlan_calc_data_test in '..\src\d_LinkFreqPlan_calc_data_test.pas' {dlg_LinkFreqPlan_CIA_maps_data_test};

{$R *.RES}




begin
  Application.Initialize;
  Application.CreateForm(TdmMain_app__CIA_maps, dmMain_app__CIA_maps);
  Application.CreateForm(Tdlg_LinkFreqPlan_CIA_maps_data_test, dlg_LinkFreqPlan_CIA_maps_data_test);
  Application.Run;
end.
