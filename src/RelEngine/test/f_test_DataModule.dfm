object frm_test_DataModule: Tfrm_test_DataModule
  Left = 248
  Top = 139
  Width = 635
  Height = 505
  Caption = 'frm_test_DataModule'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 101
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 16
    Top = 36
    Width = 281
    Height = 325
    DataSource = dmMapEngine.DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBGrid2: TDBGrid
    Left = 328
    Top = 36
    Width = 277
    Height = 321
    DataSource = dmMapEngine.DataSource2
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
end
