object Form23: TForm23
  Left = 233
  Top = 154
  Width = 616
  Height = 448
  Caption = 'Form23'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 101
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 96
    Width = 608
    Height = 324
    ActivePage = TabSheet4
    Align = alBottom
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'tbl_LinkEnd_ant'
      object DBGrid3: TDBGrid
        Left = 0
        Top = 0
        Width = 600
        Height = 236
        Align = alTop
        DataSource = DataSource1
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      object DBGrid4: TDBGrid
        Left = 0
        Top = 0
        Width = 600
        Height = 185
        Align = alTop
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'TabSheet3'
      ImageIndex = 2
      object DBGrid5: TDBGrid
        Left = 0
        Top = 0
        Width = 600
        Height = 169
        Align = alTop
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'TabSheet4'
      ImageIndex = 3
    end
  end
  object ADOConnection2: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=SQL_ONEGA;Extended Properties="DSN=SQL_ONEGA;UID=sa;APP=En' +
      'terprise;WSID=ALEX;DATABASE=ONEGA"'
    LoginPrompt = False
    Left = 72
    Top = 24
  end
  object tbl_LinkEnd_ant: TADOTable
    Active = True
    Connection = ADOConnection2
    CursorType = ctStatic
    TableName = 'view_LinkEnd_antennas'
    Left = 284
    Top = 4
  end
  object DataSource1: TDataSource
    DataSet = tbl_LinkEnd_ant
    Left = 280
    Top = 48
  end
end
