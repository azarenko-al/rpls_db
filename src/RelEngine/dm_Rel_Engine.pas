unit dm_Rel_Engine;

interface

uses
  SysUtils, Classes, Forms, Db, ADODB,

   u_rel_Matrix_base,

  u_func_msg,

  i_SNMP,
  snmp_TLB,

  dm_Main,

  dm_Onega_DB_data,

  I_rel_Matrix1,
  u_rel_engine,

  

  dm_ClutterModel,

  u_clutters,
  u_rel_Profile,

  u_const_db,

  u_log,

  u_Geo,
  u_func,
  u_db,

  AppEvnts
  ;



type
  TdmRel_Engine = class(TDataModule, IMessageHandler)
    qry_Matrixes: TADOQuery;
    qry_ClutterModelType: TADOQuery;
    ADOStoredProc1: TADOStoredProc;
    ApplicationEvents1: TApplicationEvents;
    ADOStoredProc_Rel: TADOStoredProc;
//    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
//    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
//  procedure DataModuleCreate(Sender: TObject);
 //   procedure DataModuleDestroy(Sender: TObject);
  private
//    function GetMinStepForVector(aBLVector: TBLVector): integer;
// TODO: GetAllList1111111111
//  function GetAllList1111111111(aStrings: TStrings): integer;
    procedure Log (aMsg: string);
///////////    function OpenByFileName(aFileName: string): boolean;

     procedure GetMessage(aMsg: TEventType; aParams: TEventParamList ; var aHandled:
         boolean);

  public
 //   procedure RegisterSender(aBLVector: TBLVector);
    Best6Zone: Integer;

    procedure Init_Geom;


//    function GetList(aStrings: TStringList): Integer;

    procedure AssignClutterModel(aClutterModelID: integer; aRelClutterModel: TrelClutterModel);
//    procedure OpenAll ();

//    procedure OpenByPoint (aBLPoint: TBLPoint);

//    procedure OpenByVector (aBLVector: TBLVector);
    procedure Open;
    procedure Close1;

    procedure CloseFile(aFileName: string);

    function GetGroundHeight (aBLPoint: TBLPoint): integer;

    procedure Dlg_ShowMatrixList;


    function GetRelInfo_(aBLPoint: TBLPoint; var aRec: Trel_Record): Boolean;

    function FindPointBL    (aBLPoint: TBLPoint; var aRec: Trel_Record): boolean;

//    function FindPointBL_ex1(aBLPoint: TBLPoint; var aRec: Trel_Record; var
//        aFileName: String): boolean;

    function BuildProfile1(aProfile: TrelProfile; aBLVector: TBLVector; aStep: double): boolean; overload;

    function FindPointBLInFile(aFileName: string; aBLPoint: TBLPoint; var aRec: Trel_Record; var aZone: integer): boolean;

    function GetClutterNameByCode(aCode: Integer): string;
    function GetList(aStrings: TStringList): Integer;
    procedure Open_All;
//    class procedure Init1;

   end;


 function dmRel_Engine: TdmRel_Engine;


(*var
  dmRel_Engine: TdmRel_Engine;
*)
(*
function dmRel_Engine: TdmRel_Engine;
*)

implementation {$R *.dfm}

var
  FdmRel_Engine: TdmRel_Engine=nil;


// ---------------------------------------------------------------
function dmRel_Engine: TdmRel_Engine;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmRel_Engine) then
    Application.CreateForm(TdmRel_Engine, FdmRel_Engine);
    //  FdmRel_Engine := TdmRel_Engine.Create(Application);

  Result := FdmRel_Engine;
end;



procedure TdmRel_Engine.DataModuleCreate(Sender: TObject);
begin
  inherited;
 /// I_rel_Matrix1.IReliefEngine_Load;

  db_SetComponentADOConnection(Self, dmMain.ADOConnection);
end;



//--------------------------------------------------------------------
function TdmRel_Engine.FindPointBLInFile(aFileName: string; aBLPoint: TBLPoint;
    var aRec: Trel_Record; var aZone: integer): boolean;
//--------------------------------------------------------------------
begin
//  Result:= rel_FindPointBLInFile(aFileName, aBLPoint, aRec);
  Result:= GetReliefEngine.FindPointBLInFile(aFileName, aBLPoint, aRec, aZone);

end;


//--------------------------------------------------------
procedure TdmRel_Engine.AssignClutterModel(aClutterModelID: integer; aRelClutterModel: TrelClutterModel);
//--------------------------------------------------------
begin
  Assert (aClutterModelID > 0);

  dmClutterModel.AssignClutterModel (aClutterModelID, aRelClutterModel);
end;


procedure TdmRel_Engine.Log (aMsg: string);
begin
  g_Log.Add(aMsg);
end;



//--------------------------------------------------------------------
procedure TdmRel_Engine.Close1;
//--------------------------------------------------------------------
begin
 // Assert(Assigned(IRel));

  Log (ClassName  + ': close');
  GetReliefEngine.Clear;
//  IRel.Clear;
end;

//--------------------------------------------------------------------
procedure TdmRel_Engine.CloseFile(aFileName: string);
//--------------------------------------------------------------------
begin
  GetReliefEngine.RemoveFile (aFileName);
end;


// ---------------------------------------------------------------
procedure TdmRel_Engine.Init_Geom;
// ---------------------------------------------------------------
var
  k: Integer;
  oRelMatrix: TrelMatrixBase;
  s: string;
  sFileName: string;
begin
  k:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc_Rel, 'gis.sp_Relief_with_null_geom_SEL',['PROJECT_ID', dmMain.ProjectID]);


  with ADOStoredProc_Rel do
    while not eof do
    begin
      sFileName:=FieldValues['name'];
      g_Log.Add('add geom - '+ sFileName);

      if FileExists(sFileName) then
      begin

         oRelMatrix:=rel_CreateRelMatrixByFileName (sFileName);
         oRelMatrix.OpenHeader(sFileName);

         if Assigned(oRelMatrix) then
         begin
           s:=oRelMatrix.ToGeometry();

           k:=dmOnega_DB_data.ExecStoredProc( ADOStoredProc1, 'gis.sp_Relief_UPD',
             ['ID', FieldValues['id'],
              'geom_str', oRelMatrix.ToGeometry()
             ]);
         end;
          //         'geom_str',  oRelMatrix.ToGeometry(),

        FreeAndNil(oRelMatrix);
      end;

      Next;
    end;


end;


//--------------------------------------------------------------------
procedure TdmRel_Engine.Open;
//--------------------------------------------------------------------
var sFileName,sMsg: string;
  i,iMinStep: Integer;
  oStrList: TStringList;
begin
  g_Log.Add('Init_Geom...');
  Init_Geom();

  Open_All;

 // Exit;

/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
   {*

  oStrList := TStringList.Create();
  Log(ClassName  + ': Open');

//  db_Clear(mem_Matrixes);

//  rel_Close;

//   oStrList

//dmRelMatrixFile.GetAllList1111111111
//  i:=dmRelMatrixFile.GetList (aBLRect,oStrList, Best6Zone, iMinStep);
//  i:=GetList (aBLRect,oStrList, Best6Zone, iMinStep);
  i:=GetList (oStrList);

  Log(ClassName  + ': GetList = '+ IntToStr(i));

  for i := 0 to oStrList.Count-1 do
  begin
     sFileName:=oStrList[i];

     if GetReliefEngine.FileNameExists (sFileName) then
     begin
       Log(ClassName  + ': IReliefEngine.FileNameExists - '+ sFileName);

       Continue;
     end;


     if GetReliefEngine.AddFile (sFileName) then begin
       Log(ClassName  + ': open matrix - '+ sFileName);
       sMsg:='success';
     end else begin
       g_Log.AddError(ClassName,   ' ERROR adding matrix - '+ sFileName);
       sMsg:='error';
     end;


   end;

  FreeAndNil(oStrList);

  *}

//  oStrList.Free;
 // rel_Open;

//  Active:=True;
end;



//--------------------------------------------------------------------
procedure TdmRel_Engine.Open_All;
//--------------------------------------------------------------------
var sFileName,sMsg: string;
  i,iMinStep: Integer;
  oStrList: TStringList;
begin
  GetReliefEngine.Clear();

  oStrList := TStringList.Create();
  Log(ClassName  + ': Open');

//  db_Clear(mem_Matrixes);

//  rel_Close;

//   oStrList

//  dmRelMatrixFile.GetAllL
//  i:=dmRelMatrixFile.GetList (aBLRect,oStrList, Best6Zone, iMinStep);
//  i:=GetList (aBLRect,oStrList, Best6Zone, iMinStep);
  i:=GetList (oStrList);

  Log(ClassName  + ': GetList = '+ IntToStr(i));

  for i := 0 to oStrList.Count-1 do
  begin
     sFileName:=oStrList[i];

     {
     if GetReliefEngine.FileNameExists (sFileName) then
     begin
       Log(ClassName  + ': IReliefEngine.FileNameExists - '+ sFileName);

       Continue;
     end;

     }

     GetReliefEngine.AddFile (sFileName);

{      then begin
       Log(ClassName  + ': open matrix - '+ sFileName);
       sMsg:='success';
     end else begin
       g_Log.AddError(ClassName,   ' ERROR adding matrix - '+ sFileName);
       sMsg:='error';
     end;

}
   end;

  FreeAndNil(oStrList);



//  oStrList.Free;
 // rel_Open;

//  Active:=True;
end;




//--------------------------------------------------------------------
function TdmRel_Engine.FindPointBL (aBLPoint: TBLPoint; var aRec: Trel_Record): boolean;
//--------------------------------------------------------------------
var
  iCount: Integer;
begin
  iCount:=GetReliefEngine.Count;

  if iCount=0 then
  begin
    exit;

    g_Log.AddError('TRelEngine', '���-�� ������ = 0');

    Open;
//    exit;
  end;

  Result:=GetReliefEngine.FindPointBL (aBLPoint, aRec);

end;



//--------------------------------------------------------------------
function TdmRel_Engine.BuildProfile1(aProfile: TrelProfile; aBLVector: TBLVector; aStep: double): boolean;
//--------------------------------------------------------------------
var
  k: Integer;
  rec: TBuildProfileParamRec;
  s: string;
  sFileName: string;
begin
  GetReliefEngine.Clear;

  k:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc1, 'gis.sp_Relief_Intersects_line_SEL',
    [
      'PROJECT_ID', dmMain.ProjectID,
      'LAT1', aBLVector.Point1.B,
      'LON1', aBLVector.Point1.L,
      'LAT2', aBLVector.Point2.B,
      'LON2', aBLVector.Point2.L
    ]);




  with ADOStoredProc1 do
    while not eof do
    begin
      sFileName:=FieldValues['name'];

//      oRelMatrix:=rel_CreateRelMatrixByFileName (sFileName);

      GetReliefEngine.AddFile(sFileName);

      Next;
    end;


  //CREATE PROCEDURE gis.sp_Relief_Intersects_line_SEL


  if GetReliefEngine.Count=0 then
    g_Log.AddError('TRelEngine', '���-�� ������ = 0');


  FillChar(rec,SizeOf(rec),0);

  rec.BLVector   := aBLVector;
  rec.Step_m     := aStep;
  rec.Refraction := aProfile.Refraction;


  Result:=GetReliefEngine.BuildProfileBL (aProfile.Data,  rec, aBLVector, aStep, aProfile.Refraction, 0 );
  aProfile.ApplyDefaultClutterHeights;

  if not Result then
  begin
    s:=Format(': positions not found: %s; %s',
                 [geo_FormatBLPoint(ablVector.Point1),
                  geo_FormatBLPoint(ablVector.Point2) ]) + ' ��������� ������� ������ �����.';

    g_Log.AddError(TdmRel_Engine.ClassName, s);

    Init_ISnmp.Send(SNMP_ERROR_PROFILE, 'Profile: '+ s);
  end;

//   Result:=aProfile.BuildProfile1 (gl_RelMatrixList, aBLVector, aStep);
end;


//-------------------------------------------------------------------
function TdmRel_Engine.GetGroundHeight (aBLPoint: TBLPoint): integer;
//-------------------------------------------------------------------
// 0 - no result
//-------------------------------------------------------------------
var
  rec: Trel_Record;

begin
  if aBLPoint.B=0 then
  begin
    Result:=0;
    Exit;
  end;

  Open() ;
//  OpenByPoint (aBLPoint) ;

  if FindPointBL (aBLPoint, rec) then
    Result:=rec.Rel_H
  else
    Result:=0;

  //dmRel_Engine.
 // Close1;
end;


//-------------------------------------------------------------------
function TdmRel_Engine.GetRelInfo_(aBLPoint: TBLPoint; var aRec: Trel_Record):
    Boolean;
//-------------------------------------------------------------------
begin
  Result := False;

  if aBLPoint.B=0 then
    Exit;

 // OpenByPoint (aBLPoint);

  Result:=FindPointBL (aBLPoint, aRec);

 // Close1;
end;


procedure TdmRel_Engine.Dlg_ShowMatrixList;
begin
  GetReliefEngine.Dlg_ShowMatrixList;
end;


function TdmRel_Engine.GetClutterNameByCode(aCode: Integer): string;
begin
  if not qry_ClutterModelType.Active then
    dmOnega_DB_data.OpenQuery (qry_ClutterModelType, 'SELECT * FROM '+ TBL_ClutterModelType +' ORDER BY code');

  Result := AsString(qry_ClutterModelType.Lookup(FLD_CODE, aCode, FLD_NAME));
end;



procedure TdmRel_Engine.GetMessage(aMsg: TEventType; aParams: TEventParamList ;
    var aHandled: boolean);
begin
  case aMsg of
    et_PROJECT_CHANGED
    //WE_PROJECT_CHANGED_
    : dmRel_Engine.Close1; //ChangeProject();

///////////   WM_REL_ENGINE_CHECK   : ShowMessage('WM_REL_ENGINE_CHECK');
//////////    WM_REL_ENGINE_UNCHECK : ShowMessage('WM_REL_ENGINE_UNCHECK');


  end;

end;


//--------------------------------------------------------------------
function TdmRel_Engine.GetList(aStrings: TStringList): Integer;
//--------------------------------------------------------------------
// iStep: min ���������
//--------------------------------------------------------------------

var
  bChecked: Boolean;
  k: Integer;
begin
  assert(dmMain.ProjectID>0, 'dmMain.ProjectID<>0');
  Assert(Assigned(aStrings));

//  aBest6Zone:=0;
//  aMinStep:=0;

  aStrings.Clear;

//  'SELECT * FROM '+TBL_RELIEF+' WHERE project_id=:project_id'
{
  k := dmOnega_DB_data.OpenStoredProc(ADOStoredProc1, 'sp_Relief_Select',
      [FLD_Project_ID, aProject_ID,
       FLD_Checked, IIF(aChecked, aChecked, null)
      ]);
 }
 
  k:=dmOnega_DB_data.Relief_Select(ADOStoredProc1, dmMain.ProjectID, True);


  with ADOStoredProc1 do
    while not EOF do
  begin
  //  bChecked:=FieldByName(FLD_Checked).AsBoolean;

    //��������, ������������ �� �������
//    if bChecked then

    aStrings.Add(FieldByName(FLD_NAME).asstring);

    Next;
  end;


  result:=aStrings.Count;

end;


 
end.

    (*


//
//
//
//procedure TdmRel_Engine.DataModuleDestroy(Sender: TObject);
//begin
////  I_rel_Matrix1.IReliefEngine_UnLoad;
//
////  FdmRel_Engine :=nil;
//
//  inherited;
//end;
//

          {

//--------------------------------------------------------------------
procedure TdmRel_Engine.OpenByVector (aBLVector: TBLVector);
//--------------------------------------------------------------------
var
  blRect: TBLRect;
begin
  blRect.TopLeft.B:=Max(aBLVector.Point1.B, aBLVector.Point2.B);
  blRect.TopLeft.L:=Min(aBLVector.Point1.L, aBLVector.Point2.L);
  blRect.BottomRight.B:=Min(aBLVector.Point1.B, aBLVector.Point2.B);
  blRect.BottomRight.L:=Max(aBLVector.Point1.L, aBLVector.Point2.L);

  Open (blRect);
end;


//--------------------------------------------------------------------
procedure TdmRel_Engine.OpenByPoint (aBLPoint: TBLPoint);
//--------------------------------------------------------------------
var blRect: TBLRect;
begin
  blRect.TopLeft.B:=aBLPoint.B;
  blRect.TopLeft.L:=aBLPoint.L;
  blRect.BottomRight.B:=aBLPoint.B;
  blRect.BottomRight.L:=aBLPoint.L;

  Open(blRect);
end;




//--------------------------------------------------------------------
procedure TdmRel_Engine.OpenAll;
//--------------------------------------------------------------------
var blRect: TBLRect;
begin
  blRect.TopLeft.B:=0;
  blRect.TopLeft.L:=0;
  blRect.BottomRight.B:=0;
  blRect.BottomRight.L:=0;

  Open (blRect);
end;




{
//-------------------------------------------------------------------
function TdmRel_Engine.GetMinStepForVector(aBLVector: TBLVector): integer;
//-------------------------------------------------------------------
var
  blRect: TBLRect;
  oStrList: TStringList;
  iBest6Zone: integer;
  iMinStep: integer;

begin
  blRect.TopLeft:=aBLVector.Point1;
  blRect.BottomRight:=aBLVector.Point2;

  oStrList:=TStringList.Create;

  GetList (blRect, oStrList, iBest6Zone, iMinStep);

  FreeAndNil(oStrList);

  Result := iMinStep;
end;

}


    {
//--------------------------------------------------------------------
function TdmRel_Engine.FindPointBL_ex1(aBLPoint: TBLPoint; var aRec:
    Trel_Record; var aFileName: String): boolean;
//--------------------------------------------------------------------
begin
  Result:=GetReliefEngine.FindPointBL_with_FileName (aBLPoint, aRec, aFileName);
end;
         }




procedure TdmRel_Engine.ApplicationEvents1Message(var Msg: tagMSG; var Handled:
    Boolean);
begin
  case Msg.Message of
    WE_PROJECT_CHANGED_: dmRel_Engine.Close1; //ChangeProject();

///////////   WM_REL_ENGINE_CHECK   : ShowMessage('WM_REL_ENGINE_CHECK');
//////////    WM_REL_ENGINE_UNCHECK : ShowMessage('WM_REL_ENGINE_UNCHECK');


  end;
end;




//--------------------------------------------------------------------
function TdmRel_Engine.GetList(aStrings: TStringList): Integer;
//--------------------------------------------------------------------
// iStep: min ���������
//--------------------------------------------------------------------

var
  bChecked: Boolean;
  k: Integer;
begin
  assert(dmMain.ProjectID>0, 'dmMain.ProjectID<>0');
  Assert(Assigned(aStrings));

//  aBest6Zone:=0;
//  aMinStep:=0;

  aStrings.Clear;

//  'SELECT * FROM '+TBL_RELIEF+' WHERE project_id=:project_id'

  k:=dmOnega_DB_data.Relief_Select(ADOStoredProc1, dmMain.ProjectID, True);


  with ADOStoredProc1 do
    while not EOF do
  begin
    bChecked:=FieldByName(FLD_Checked).AsBoolean;

    //��������, ������������ �� �������
    if bChecked then
      aStrings.Add(FieldByName(FLD_NAME).asstring);

    Next;
  end;


  result:=aStrings.Count;

end;



//--------------------------------------------------------------------
procedure TdmRel_Engine.Open_All;
//--------------------------------------------------------------------
var sFileName,sMsg: string;
  i,iMinStep: Integer;
  oStrList: TStringList;
begin
  g_Log.Add('Init_Geom...');
  Init_Geom();

 // Exit;

/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
   {*

  oStrList := TStringList.Create();
  Log(ClassName  + ': Open');

//  db_Clear(mem_Matrixes);

//  rel_Close;

//   oStrList

//dmRelMatrixFile.GetAllList1111111111
//  i:=dmRelMatrixFile.GetList (aBLRect,oStrList, Best6Zone, iMinStep);
//  i:=GetList (aBLRect,oStrList, Best6Zone, iMinStep);
  i:=GetList (oStrList);

  Log(ClassName  + ': GetList = '+ IntToStr(i));

  for i := 0 to oStrList.Count-1 do
  begin
     sFileName:=oStrList[i];

     if GetReliefEngine.FileNameExists (sFileName) then
     begin
       Log(ClassName  + ': IReliefEngine.FileNameExists - '+ sFileName);

       Continue;
     end;


     if GetReliefEngine.AddFile (sFileName) then begin
       Log(ClassName  + ': open matrix - '+ sFileName);
       sMsg:='success';
     end else begin
       g_Log.AddError(ClassName,   ' ERROR adding matrix - '+ sFileName);
       sMsg:='error';
     end;


   end;

  FreeAndNil(oStrList);

  *}

//  oStrList.Free;
 // rel_Open;

//  Active:=True;
end;

