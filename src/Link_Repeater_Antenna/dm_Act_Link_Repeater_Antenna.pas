unit dm_Act_Link_Repeater_Antenna;

interface
{$I ver.inc}

uses
  Classes, Forms, Menus, ActnList, 


 // d_Audit,

  

  

  dm_act_Base,

  
  //I_Antenna,

  //dm_MapEngine,
  I_Object,

 // I_Act_Antenna,

  dm_Antenna,
 // dm_Cell,

//  dm_Pmp_Sector,
 // dm_PmpTerminal,
 // dm_LinkEnd,

  fr_Antenna_view,
 // d_Antenna_inspector,

//  d_Antenna_add,
 // d_Antenna_Move,

  

  
  
  

  u_Types

  ;

type
  TdmAntennaParentType = (aptCell,aptLinkend);


  TdmAct_Link_Repeater_Antenna = class(TdmAct_Base) //, IAct_Antenna_X)
    act_Move: TAction;

 //   procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
 
  private
    procedure DoAction (Sender: TObject);
  protected
    function ItemDel(aID: integer; aName: string= ''): boolean; override;

    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm (aOwnerForm: TForm): TForm; override;
 
  public

    class procedure Init;

  end;

var
  dmAct_Link_Repeater_Antenna: TdmAct_Link_Repeater_Antenna;

//==================================================================
// implementation
//==================================================================
implementation

 {$R *.dfm}

// ---------------------------------------------------------------
class procedure TdmAct_Link_Repeater_Antenna.Init;
// ---------------------------------------------------------------
begin
  Assert(not Assigned(dmAct_Link_Repeater_Antenna));

  Application.CreateForm(TdmAct_Link_Repeater_Antenna, dmAct_Link_Repeater_Antenna);

 // dmAct_Antenna.GetInterface(IAct_Antenna_X, IAct_Antenna);
//  Assert(Assigned(IAct_Antenna));

end;

//
//procedure TdmAct_Antenna.DataModuleDestroy(Sender: TObject);
//begin
////  IAct_Antenna :=nil;
////  dmAct_Antenna:=nil;
//
//  inherited;
//end;



//--------------------------------------------------------------------
procedure TdmAct_Link_Repeater_Antenna.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
var b: Boolean;
begin
  inherited;
  ObjectName:=OBJ_Link_Repeater_Antenna;
//  ObjectName:=OBJ_LINKEND_ANTENNA;

 // act_Move.Caption:='�����������';
//  act_Copy.Caption:='����������';

//  act_Audit.Caption:=STR_Audit;
//  act_Audit.Enabled:=False;

{
  SetActionsExecuteProc ([act_Move
//                          act_Audit
                          ],
                          DoAction);

  SetCheckedActionsArr
    ([
        act_Add,
        act_Del_,
        act_Del_list,

        act_Move,
        act_Copy
     ]);

}


 // dmLocalization.Load_DataModule (Self);
end;





//--------------------------------------------------------------------
procedure TdmAct_Link_Repeater_Antenna.DoAction (Sender: TObject);
//--------------------------------------------------------------------
begin

{  if Sender=act_Audit then
     dlg_Audit(TBL_LINKEND_ANTENNA, FFocusedID) else
}

 { if Sender=act_Copy then
     Copy()
  else

  if Sender=act_Move then
    Dlg_Move (FFocusedID);}
end;


//--------------------------------------------------------------------
function TdmAct_Link_Repeater_Antenna.ItemDel(aID: integer; aName: string= ''):
    boolean;
//--------------------------------------------------------------------
//var
 // sOwnerObjName: string;
//  sObjName: string;
 // s: string;
 // iAntCount, iLinkEndID: integer;
//  ot: TrpObjectType;

//  Rec: TdmAntennaOwnerInfoRec;
begin

//  if not dmAntenna.GetOwnerInfo1(aID, Rec) then
//    Exit;
//
//  FDeletedIDList.AddObjName(aID, Rec.ObjectName);


{
    Result := Rec.ID
  else
    Result := 0;
}

  // sOwnerObjName:=rec.OwnerObjectName;

 //  sOwnerObjName:=dmAntenna.GetOwnerObjectName (aID);

//  ot:=dmAntenna.GetOwnerType (aID);


//  {$IFDEF link}



//  {$ENDIF}

//  AddTableToBackup (TBL_ANTENNA_XREF,db_Par(FLD_ANTENNA_ID, aID));
//  sObjName:=dmAntenna.GetObjectName (aID);

  Result:=dmAntenna.Del (aID);

//
//  if Result then
//  begin
//    if Assigned(IMapEngine) then
//      dmMapEngine1.DelObject (ObjNameToType(sObjName), aID);
//
//{    case ObjNameToType(s) of
//      otCell_3G:        dmMapEngine1.DelObject (otCellAnt_3G, aID);
//      otCell:           dmMapEngine1.DelObject (otCellAnt, aID);
//      otLinkEnd:        dmMapEngine1.DelObject (otLinkEndAnt, aID);
//      otPmpSector:      dmMapEngine1.DelObject (otPmpSectorAnt, aID);
//      otPmpTerminal:    dmMapEngine1.DelObject (otPmpTerminalAnt, aID);
//    else
//      raise Exception.Create('');
//    end;}
//
//  //  PostEvent (WE_MAP_ENGINE_DEL_OBJECT, Integer(otCellAnt), aID);
////    PostEvent (WE_MAP_ENGINE_DEL_OBJECT, Integer(otLinkEndAnt), aID);
//    if Assigned(IMapAct) then
//      dmAct_Map.MapRefresh;
//  end else
//  begin
//   // RemoveFromBackup(TBL_ANTENNA_XREF,db_Par(FLD_ANTENNA_ID, aID));
//  end;

end;


//--------------------------------------------------------------------
function TdmAct_Link_Repeater_Antenna.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_Antenna_View.Create(aOwnerForm);
end;




//--------------------------------------------------------------------
procedure TdmAct_Link_Repeater_Antenna.GetPopupMenu;
//--------------------------------------------------------------------
begin
 // CheckActionsEnable();
{
  case aMenuType of
    mtItem: begin// ,mtList:
              AddMenuItem (aPopupMenu, act_Move);
              AddMenuItem (aPopupMenu, nil);
              AddMenuItem (aPopupMenu, act_Del_);
              AddMenuItem (aPopupMenu, act_Copy);
//              AddMenuItem (aPopupMenu, act_Audit);

     ///////         AddMenuItem (aPopupMenu, act_Undelete);
            end;
  end;
}

end;



end.


