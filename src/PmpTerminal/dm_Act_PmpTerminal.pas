unit dm_Act_PmpTerminal;

interface

uses
  Classes, Menus, Forms, ActnList, 


  u_shell_var,  

  dm_MapEngine_store,

  dm_act_Base,
//  I_Act_PmpTerminal,

//  I_act_Antenna,  // dm_act_Antenna;

  I_Object,

  d_PmpTerminal_Plug,

 // dm_MapEngine,
            

  dm_Folder,   
  
  dm_PmpTerminal,

  d_LinkEnd_inspector,
  fr_PmpTerminal_View,

  u_classes,
  u_func,

  u_Types,
  u_Geo,

  d_LinkEnd_add
 // d_PmpLink_add

  ;

type
  TdmAct_PmpTerminal = class(TdmAct_Base) //, IAct_PmpTerminal_X)
  //, IPmpTerminalX)
    act_Antenna_Add: TAction;
    act_MoveToFolder: TAction;
    act_Change_LinkEndType: TAction;
    act_ShowLinkInfo: TAction;
    act_Change_Sector: TAction;
  //  procedure DataModuleDestroy(Sender: TObject);
 //   procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
//    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
  private
    FBLPoint: TBLPoint;
    FProperty_ID: integer;
  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm (aOwnerForm: TForm): TForm; override;
    procedure DoAction (Sender: TObject);

    function GetExplorerPath(aID: integer; aGUID: string): string;

    function ItemDel(aID: integer; aName: string = ''): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;

    procedure ShowOnMap (aID: integer);

 //   procedure Change_LinkEndType(Sender: TObject; aIDList: TIDList);

//    procedure GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean); override;

    function ViewInProjectTree(aID: integer): TStrArray; override;
 public
    procedure Add (aFolderID,aProperty: integer); overload;
    procedure AddByProperty (aProperty: integer);

    procedure AddByPos(aBLPoint: TBLPoint);

    procedure SwitchPmpSector (aIDList: TIDList);

    function Dlg_Edit(aID: integer): boolean; //override;

    class procedure Init;
  end;

var
  dmAct_PmpTerminal: TdmAct_PmpTerminal;

//====================================================================
// implementation
//====================================================================
implementation

uses dm_act_Antenna;

 {$R *.dfm}



//--------------------------------------------------------------------
class procedure TdmAct_PmpTerminal.Init;
//--------------------------------------------------------------------
begin
  Assert(not Assigned(dmAct_PmpTerminal));

  Application.CreateForm(TdmAct_PmpTerminal, dmAct_PmpTerminal);

 // dmAct_PmpTerminal.GetInterface(IAct_PmpTerminal_X, IAct_PmpTerminal);
//  Assert(Assigned(IAct_PmpTerminal));

end;
//
//
//procedure TdmAct_PmpTerminal.DataModuleDestroy(Sender: TObject);
//begin
////  IAct_PmpTerminal := nil;
////  dmAct_PmpTerminal := nil;
//
//  inherited;
//end;


//--------------------------------------------------------------------
procedure TdmAct_PmpTerminal.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
var b: Boolean;

begin
  inherited;

  ObjectName:=OBJ_PMP_TERMINAL;

  act_Antenna_Add.Caption:='������� �������';
//  act_AddPmpLink.Caption:='������� PMP ��������';
  act_Change_Sector.Caption:='����������� �� ������ ������';
//  act_Change_LinkEndType.Caption:='������� ������������';

////////////  OnAfterItemDel:=DoAfterItemDel;

  act_Add.Caption:='������� ��� ��������';
              

  SetActionsExecuteProc ([
     act_Object_ShowOnMap,

     act_Antenna_Add,
     act_MoveToFolder,
//     act_AddPmpLink,
     act_Change_Sector,
//     act_Change_LinkEndType,
     act_ShowLinkInfo
   ], DoAction);


  SetCheckedActionsArr([
        act_Add,
        act_Del_,
        act_Del_List,

        act_Change_Sector,

        act_Antenna_Add
     ]);

end;



//--------------------------------------------------------------------
procedure TdmAct_PmpTerminal.ShowOnMap (aID: integer);
//--------------------------------------------------------------------
begin
  ShowPointOnMap (dmPmpTerminal.GetPropertyPos (aID), aID);
end;

//--------------------------------------------------------------------
function TdmAct_PmpTerminal.Dlg_Edit(aID: integer): boolean;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_LinkEnd_inspector.ExecDlg1 (aID, OBJ_PMP_TERMINAL);
//  Result:=Tframe_LinkEnd_inspector.ExecDlg1 (aID, OBJ_PMP_TERMINAL);

end;


//--------------------------------------------------------------------
function TdmAct_PmpTerminal.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_PmpTerminal_View.Create(aOwnerForm);
end;

//--------------------------------------------------------------------
procedure TdmAct_PmpTerminal.AddByProperty (aProperty: integer);
//--------------------------------------------------------------------
begin
  FBLPoint:=NULL_POINT;
  FProperty_ID:=aProperty;
  inherited Add (0);
end;

//--------------------------------------------------------------------
procedure TdmAct_PmpTerminal.Add (aFolderID,aProperty: integer);
//--------------------------------------------------------------------
begin
  FBLPoint:=NULL_POINT;
  FProperty_ID:=aProperty;
  inherited Add (aFolderID);
end;

//--------------------------------------------------------------------
procedure TdmAct_PmpTerminal.AddByPos(aBLPoint: TBLPoint);
//--------------------------------------------------------------------
begin
  FBLPoint:=aBLPoint;
  FProperty_ID:=0;
  inherited Add (0);

end;


//-------------------------------------------------------------------
procedure TdmAct_PmpTerminal.SwitchPmpSector (aIDList: TIDList);
//-------------------------------------------------------------------
var
  iPmpLinkID,iPmpTerminalID: Integer;
  rec: Tdlg_PmpTerminal_Plug_rec;
begin
  if aIDList.Count<>1 then
    Exit;

  iPmpTerminalID:=aIDList[0].ID;

//  oSectorIDList:=TIDList.Create;

  FillChar(rec,SizeOf(rec),0);


  if Tdlg_PmpTerminal_Plug.ExecDlg(iPmpTerminalID, 0, rec) then
  begin
    iPmpLinkID:=dmPmpTerminal.SwitchSector(iPmpTerminalID, rec.PmpSector_ID);

//      iPmpLinkID:=dmPmpTerminal.GetLinkID(iPmpTerminalID);

  //  dmMapEngine.ReCreateObject(otLink, iPmpLinkID);

  //  dmAct_Map.MapRefresh;
  end;

(*

   if dmAct_Explorer.Dlg_Object_Move_Enhanced(otPmpSite, otPmpSector, oSectorIDList, true) then
    begin

      iPmpLinkID:=dmPmpTerminal.SwitchSector(iPmpTerminalID, oSectorIDList[0].ID);

//      iPmpLinkID:=dmPmpTerminal.GetLinkID(iPmpTerminalID);

      dmMapEngine1.ReCreateObject(otLink, iPmpLinkID);

      dmAct_Map.MapRefresh;
    end;
*)

  { TODO : !!!!!!!!!!! }
{

  sh_PostUpdateNodeChildren(dmPmp_Sector.GetGUIDByID(iOldPmpSectorID));
  sh_PostUpdateNodeChildren(dmPmp_Sector.GetGUIDByID(aSectorID));
  sh_PostUpdateNodeChildren(GetGUIDByID(aID));


  dmMapEngine.CreateObject(otLink, iPmpLinkID);
  dmAct_Map.MapRefresh;
}



//    for i:= 0 to aIDList.Count-1 do
//      dmPmpTerminal.SwitchSector(aIDList[i].ID, oSectorIDList[0].ID);

 // oSectorIDList.Free;

end;



//--------------------------------------------------------------------
function TdmAct_PmpTerminal.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
var
 // iPmpTerminalID: integer;
  rec: Tdlg_LinkEnd_add_rec;

begin

  FillChar(rec, SizeOf(rec), 0);

//   rec.FolderID:=aFolderID;
  rec.BLPoint:=FBLPoint;
  rec.PMPSectorID:=0;
  rec.PropertyID :=FProperty_ID ;


  Result:= Tdlg_LinkEnd_add.ExecDlg_PmpTerminal (rec);
//  Result:= Tdlg_LinkEnd_add.ExecDlg_PmpTerminal (aFolderID, FBLPoint, FProperty_ID, 0);

{
  if Result>0 then
  begin
    dmMapEngine1.CreateObject (otPmpTerminal, Result);

    g_ShellEvents.Shell_UpdateNodeChildren_ByGUID (dmPmpTerminal.GetGUIDByID(Result));

//    SH_PostUpdateNodeChildren (dmPmpTerminal.GetGUIDByID(Result));

    dmAct_Map.MapRefresh;
  end;
}
end;


//--------------------------------------------------------------------
function TdmAct_PmpTerminal.ItemDel(aID: integer; aName: string = ''): boolean;
//--------------------------------------------------------------------
//var
 // iPmpLinkID: integer;

begin
 // iPmpLinkID:= dmPmpTerminal.GetLinkID (aID);
 // dmAntenna.GetAntennaIDList (OBJ_Pmp_Terminal, aID, FDeletedIDList);

  Result:= dmPmpTerminal.Del (aID);

  if Result then
  begin
    dmMapEngine_store.Feature_Del (OBJ_Pmp_Terminal, aID);

///    PostEvent (WE_MAP_ENGINE_DEL_OBJECT, Integer(otCalcRegion), aID);

  //  dmAct_Map.MapRefresh;
  end;


{
  if Result then
  begin
    dmMapEngine1.DelObject (otPmpTerminal, aID);

    if (iPmpLinkID > 0) then
      dmMapEngine1.DelObject (otLink, iPmpLinkID);

    dmAct_Map.MapRefresh;
  end;}
end;

//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_PmpTerminal.DoAction (Sender: TObject);
var
  iID,
  iLink: integer;
  sGUID: string;
begin
//  if Sender=act_MoveToFolder then
 //   dmAct_Folder.MoveToFolderDlg (ObjectName, FSelectedPIDLs) else

  if Sender=act_Object_ShowOnMap then
    ShowOnMap(FFocusedID) else

  if Sender=act_Change_Sector then
    SwitchPmpSector(FSelectedIDList) else

  if Sender=act_Antenna_Add then
   // dmAct_Antenna.AddPmpTerminalAntenna (FFocusedID) else
    dmAct_Antenna.Dlg_Add_LinkEnd_Antenna(FFocusedID)
  else // > 0 then


(*  if Sender=act_AddPmpLink then
  begin
    iLink:=Tdlg_PmpLink_add.ExecDlg(FFocusedID);

    if (iLink > 0) then
    begin

//      dmPmpTerminal.UpdateAntennaAzimuth(iLink);

      dmMapEngine1.CreateObject(otLink, iLink);
      dmAct_Map.MapRefresh;

      sGUID := dmPmpTerminal.GetGUIDByID(FFocusedID);
      g_ShellEvents.Shell_UpdateNodeChildren_ByGUID(sGUID);

    end;
*)
 //  else
  //  ;
    {
      PostEvent(WE_INSPECTOR_REFRESH_ANTENNAS,
               [app_Par(PAR_ID,   FFocusedID),
                app_Par(PAR_TYPE, OBJ_PMPTERMINAL)
               ]);
     }
//  if Sender=act_ShowLinkInfo then
  //  dmPmpTerminal.ShowLinkInfo(FFocusedID) else

  {
  if Sender=act_Change_LinkEndType then
    Change_LinkEndType(Sender, SelectedIDList) else
   }
end;



//--------------------------------------------------------------------
procedure TdmAct_PmpTerminal.GetPopupMenu;
//--------------------------------------------------------------------
begin
//  CheckActionsEnable();

  case aMenuType of
    mtFolder: begin
                AddFolderMenu_Create (aPopupMenu);
                AddFolderPopupMenu (aPopupMenu);
              end;
    mtItem:   begin
                AddMenuItem (aPopupMenu, act_Antenna_Add);
                AddMenuItem (aPopupMenu, act_Change_Sector);
         //       AddMenuItem (aPopupMenu, act_AddPmpLink);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Object_ShowOnMap);
      //          AddMenuItem (aPopupMenu, act_ShowLinkInfo);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Del_);
              //  AddMenuItem (aPopupMenu, act_MoveToFolder);

             //   AddFolderMenu_Tools (aPopupMenu);
      //          AddMenuItem (aPopupMenu, act_Change_LinkEndType);
              end;
    mtList:   begin
                AddMenuItem (aPopupMenu, act_Change_Sector);
                AddMenuItem (aPopupMenu, act_Del_list);
            //    AddFolderMenu_Tools (aPopupMenu);
      //          AddMenuItem (aPopupMenu, act_Change_LinkEndType);
              end;

  end;
end;



//--------------------------------------------------------------------
function TdmAct_PmpTerminal.GetExplorerPath(aID: integer; aGUID: string): string;
//--------------------------------------------------------------------
var sFolderGUID: string;
begin
  sFolderGUID:=dmFolder.GetGUIDByID (dmPmpTerminal.GetFolderID(aID));

  Result := GUID_PROJECT + CRLF +
            GUID_Pmp_Terminal    + CRLF +
            sFolderGUID  + CRLF +
            aGUID;
end;

//--------------------------------------------------------------------
function TdmAct_PmpTerminal.ViewInProjectTree(aID: integer): TStrArray;
//--------------------------------------------------------------------
var
  iFolderID: Integer;
  sFolderGUID: string;

begin

  if Assigned(IMainProjectExplorer) then
  begin
    IMainProjectExplorer.ExpandByGUID(GUID_PROJECT);
    IMainProjectExplorer.ExpandByGUID(GUID_Pmp_Terminal);


    iFolderID := dmPmpTerminal.GetFolderID(aID);
//    sGUID:=dmFolder.GetGUIDByID (iFolderID);

    if iFolderID=0 then
      sFolderGUID:=g_Obj.ItemByName[ObjectName].RootFolderGUID
    else
      sFolderGUID:=dmFolder.GetGUIDByID(iFolderID);

  //  sGUID:=dmFolder.GetGUIDByID (dmPmpTerminal.GetFolderID(aID));

    IMainProjectExplorer.ExpandByGUID(sFolderGUID);
//    IMainProjectExplorer.FocuseNodeByGUID (aGUID);

    IMainProjectExplorer.FocuseNodeByObjName(ObjectName, aID);

  end;
end;


end.

