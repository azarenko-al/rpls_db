object Form16: TForm16
  Left = 301
  Top = 233
  Width = 466
  Height = 290
  Caption = 'Form16'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 458
    Height = 215
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 185
      Top = 5
      Width = 3
      Height = 205
      Cursor = crHSplit
    end
    object pc_Main: TPageControl
      Left = 5
      Top = 5
      Width = 180
      Height = 205
      ActivePage = TabSheet1
      Align = alLeft
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'TabSheet1'
      end
    end
    object pn_Browser: TPanel
      Left = 188
      Top = 5
      Width = 265
      Height = 205
      Align = alClient
      Caption = 'pn_Browser'
      TabOrder = 1
    end
  end
  object FormStorage1: TFormStorage
    StoredProps.Strings = (
      'TabSheet1.Width')
    StoredValues = <>
    Left = 60
    Top = 88
  end
end
