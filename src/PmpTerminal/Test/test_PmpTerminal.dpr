program test_PmpTerminal;

uses
  Forms,
  a_Unit in 'a_Unit.pas' {Form16},
  dm_Explorer in '..\..\Explorer\dm_Explorer.pas' {dmExplorer: TDataModule},
  fr_Object_inspector111 in '..\..\Common\fr_Object_inspector111.pas' {frame_Object_inspector111111},
  fr_DBInspector in '..\..\Explorer\fr_DBInspector.pas' {frame_DBInspector},
  fr_Explorer in '..\..\Explorer\fr_Explorer.pas' {frame_Explorer1},
  fr_Explorer_base in '..\..\Explorer\fr_Explorer_base.pas' {frame_Explorer_base},
  fr_View_base in '..\..\Explorer\fr_View_base.pas' {frame_View_Base},
  fr_Browser_List in '..\..\Explorer\fr_Browser_List.pas' {frame_Browser_List},
  dm_Custom in '..\..\.common\dm_Custom.pas' {dmCustom: TDataModule},
  d_Folder_Select in '..\..\Folder\d_Folder_Select.pas' {dlg_Folder_Select},
  d_expl_Object_Select in '..\..\Explorer\d_expl_Object_Select.pas' {dlg_expl_Object_Select},
  f_Explorer in '..\..\Explorer\f_Explorer.pas' {frm_Explorer},
  fr_PmpTerminal_view in '..\fr_PmpTerminal_view.pas' {frame_PmpTerminal_View},
  dm_Act_PmpTerminal in '..\dm_Act_PmpTerminal.pas' {dmAct_PmpTerminal: TDataModule},
  dm_PmpTerminal in '..\dm_PmpTerminal.pas' {dmPmpTerminal: TDataModule},
  dm_PmpTerminal_View in '..\dm_PmpTerminal_View.pas' {dmPmpTerminal_View: TDataModule},
  dm_PMP_site in '..\..\PMP\dm_PMP_site.pas' {dmPmpSite: TDataModule},
  dm_act_PMP_site in '..\..\PMP\dm_act_PMP_site.pas' {dmAct_PMP_site: TDataModule},
  dm_Act_PMP_Sector in '..\..\PMP\dm_Act_PMP_Sector.pas' {dmAct_PMP_Sector: TDataModule},
  fr_LinkEnd_antennas in '..\..\LinkEnd\fr_LinkEnd_antennas.pas' {frame_LinkEnd_antennas},
  fr_LinkEnd_inspector in '..\..\LinkEnd\fr_LinkEnd_inspector.pas' {frame_LinkEnd_inspector},
  dm_ClutterModel in '..\..\Clutters\dm_ClutterModel.pas' {dmClutterModel: TDataModule};

{$R *.RES}
{$R ..\..\images.RES}

begin
  Application.Initialize;
  Application.CreateForm(TForm16, Form16);
  Application.CreateForm(TdmClutterModel, dmClutterModel);
  Application.Run;
end.
