unit a_Unit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, rxPlacemnt, ComCtrls, ExtCtrls, ToolWin,

  f_Log,

  u_types,

  fr_Browser,

  fr_Explorer,
  dm_Explorer,

  dm_Act_PmpTerminal,

  dm_Act_Map_Engine,

  dm_Act_Project,

  u_reg,
  u_func_msg,

  u_const,
  u_const_db,
  u_const_msg,
  u_Geo,

  u_db,

   dm_Act_Link,

  dm_act_Property,
  //dm_Act_PmpTerminal,
  dm_Act_LinkEndType,

  fr_PmpTerminal_view

  ;


type
  TForm16 = class(TForm)
    FormStorage1: TFormStorage;
    Panel1: TPanel;
    pc_Main: TPageControl;
    TabSheet1: TTabSheet;
    pn_Browser: TPanel;
    Splitter1: TSplitter;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    id: integer;

    Fframe_Browser : Tframe_Browser;
    Fframe_Explorer : Tframe_Explorer;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form16: TForm16;

implementation

uses dm_Main;

{$R *.DFM}


procedure TForm16.FormCreate(Sender: TObject);
begin
  InitRegistry1(REGISTRY_ROOT);

//  Tfrm_Log.CreateForm;

  dmAct_Map_Engine.Enabled:=False;

  dmMain.Open;

  Fframe_Explorer := Tframe_Explorer.CreateChildForm ( TabSheet1);
  Fframe_Explorer.SetViewObjects([otPmp,otPmpTerminal,otLinkEnd,otProperty]);
  Fframe_Explorer.RegPath:='aaaaaaaaaa';

  Fframe_Browser := Tframe_Browser.CreateChildForm( pn_Browser);
  Fframe_Explorer.OnNodeFocused:=Fframe_Browser.ViewObjectByPIDL;

 // dmAct_Map_Engine.Enabled:=False;

  dmAct_Project.LoadLastProject;   
end;


procedure TForm16.FormDestroy(Sender: TObject);
begin
  Fframe_Explorer.Free;
  Fframe_Browser.Free;
end;


end.
