unit d_PmpTerminal_Plug;

interface

uses
  Classes, Controls, Forms, ADODB, cxGridLevel, rxPlacemnt,
  cxGridDBTableView, cxGrid, DB, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, StdCtrls, ExtCtrls, ToolWin,
  ComCtrls,

  dm_Main,

  dm_Onega_DB_data,

  u_Func_arrays,
  u_func,
  u_db,

  u_const_db
;

type
  Tdlg_PmpTerminal_Plug_rec = record
   // PmpTerminal_ID : Integer;

    PmpSector_ID: Integer;

//    PmpSite_ID: Integer;

    PmpSector_name : string;
    PmpSite_name : string;

  end;


  Tdlg_PmpTerminal_Plug = class(TForm)
    ToolBar1: TToolBar;
    Panel1: TPanel;
    btn_Ok: TButton;
    Button2: TButton;
    DataSource1: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    col_ID: TcxGridDBColumn;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    col_pmp_site_id: TcxGridDBColumn;
    cxGrid1DBTableView1pmp_site_name: TcxGridDBColumn;
    col_band: TcxGridDBColumn;
    col_Property_name: TcxGridDBColumn;
    FormPlacement1: TFormPlacement;
    ADOStoredProc1: TADOStoredProc;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
  private
    procedure Open(aPmpTerminalID, aPROJECT_ID: Integer);

  public
    class function ExecDlg(aPmpTerminalID, aPROJECT_ID: Integer; var aRec:
        Tdlg_PmpTerminal_Plug_rec): boolean;

  end;


implementation

{$R *.dfm}


//--------------------------------------------------------------------
class function Tdlg_PmpTerminal_Plug.ExecDlg(aPmpTerminalID, aPROJECT_ID:
    Integer; var aRec: Tdlg_PmpTerminal_Plug_rec): boolean;
//--------------------------------------------------------------------
begin
  with Tdlg_PmpTerminal_Plug.Create(Application) do
  begin
 //   assert (aPROJECT_ID>0);

    Open(aPmpTerminalID, aPROJECT_ID);

 //   db_view ( ADOStoredProc1 );

    if aRec.PmpSector_ID >0 then
      ADOStoredProc1.Locate (FLD_ID, aRec.PmpSector_ID, []);
//      ADOStoredProc1.Locate (FLD_Pmp_Sector_ID, aRec.PmpSector_ID, []);


    Result:=ShowModal=mrOK;

    if Result then
    begin
      aRec.PmpSector_ID  := ADOStoredProc1.FieldByName(FLD_ID).AsInteger;
      aRec.PmpSector_name:= ADOStoredProc1.FieldByName(FLD_name).AsString;

//      aRec.PmpSite_ID  := ADOStoredProc1.FieldByName(FLD_Pmp_Site_ID).AsInteger;
      aRec.PmpSite_name:= ADOStoredProc1.FieldByName(FLD_Pmp_Site_name).AsString;
    end;

    Free;
  end;
end;


procedure Tdlg_PmpTerminal_Plug.FormCreate(Sender: TObject);
begin
  Caption:='����� PMP �������';

  cxGrid1.Align:=alClient;
end;

// ---------------------------------------------------------------
procedure Tdlg_PmpTerminal_Plug.Open(aPmpTerminalID, aPROJECT_ID: Integer);
// ---------------------------------------------------------------
var
  k: Integer;
begin
  k:=dmOnega_DB_data.PMP_Terminal_Select_PMP_Sectors_for_Plug_ (ADOStoredProc1, aPmpTerminalID, aPROJECT_ID );

 {
  Result := OpenStoredProc(aADOStoredProc,
     sp_PMP_Terminal_Select_PMP_Sectors_for_Plug1,
       [
         FLD_ID, aPmpTerminalID,
         FLD_PROJECT_ID, aPROJECT_ID
        ]);
  }

//    sp_PMP_Terminal_Select_PMP_Sectors_for_Plug1 = 'sp_PMP_Terminal_Select_PMP_Sectors_for_Plug';

//  db_View(ADOStoredProc1);

    
//
//
//  dmOnega_DB_data.OpenStoredProc(ADOStoredProc1,
//        sp_PMP_Terminal_Select_PMP_Sectors_for_Plug,
//        [
//         db_Par(FLD_ID,         aPmpTerminalID),
//         db_Par(FLD_PROJECT_ID, aPROJECT_ID)
//        ]);



  db_SetFieldCaptions(ADOStoredProc1,
     [
       FLD_name,           '��������',
       FLD_Pmp_Site_name,  'PMP �������',
    //   SArr(FLD_antenna_count,  '���-�� ������'),
       FLD_Property_name,  '��������'

       //���������, m

     ] );

end;

end.