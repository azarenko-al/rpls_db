unit fr_PmpTerminal_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, Menus, DBCtrls, ToolWin,ComCtrls, ExtCtrls,
  ImgList,  cxPropertiesStore,  cxControls, cxSplitter,

  dm_User_Security,

  fr_View_base,

  u_func,
  u_reg,

  u_db,

  u_types,

  fr_LinkEnd_inspector,
  fr_LinkEnd_antennas,


  dxBar, cxBarEditItem, cxClasses, cxLookAndFeels
  ;

type
  Tframe_PmpTerminal_View = class(Tframe_View_Base)
    pn_Inspector: TPanel;
    cxSplitter1: TcxSplitter;
    pn_Antennas: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private

    Fframe_LinkEnd_inspector: Tframe_LinkEnd_inspector;
    Fframe_antennas: Tframe_LinkEnd_antennas;

  public
    procedure View(aID: integer; aGUID: string); override;

  end;


//===============================================================
//===============================================================
implementation

{$R *.dfm}


//--------------------------------------------------------------------
procedure Tframe_PmpTerminal_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_PMP_TERMINAL;

  pn_Inspector.Align:=alClient;

  CreateChildForm(Tframe_LinkEnd_inspector,  Fframe_LinkEnd_inspector,    pn_Inspector);
  CreateChildForm(Tframe_LinkEnd_antennas,   Fframe_antennas,     pn_Antennas);


 // Fframe_LinkEnd_inspector:=Tframe_LinkEnd_inspector.CreateChildForm( pn_Inspector);
//  Fframe_antennas:=Tframe_LinkEnd_antennas.CreateChildForm( pn_Antennas);


  AddComponentProp(pn_Antennas, PROP_HEIGHT);
  cxPropertiesStore.RestoreFrom;

end;


procedure Tframe_PmpTerminal_View.FormDestroy(Sender: TObject);
begin
 // SaveAndClearGroup (Self, Name);

 // Fframe_LinkEnd_inspector.Free;
 // Fframe_antennas.Free;

  inherited;
end;

// ---------------------------------------------------------------
procedure Tframe_PmpTerminal_View.View(aID: integer; aGUID: string);
// ---------------------------------------------------------------
//var
//  bReadOnly: Boolean;
begin
  inherited;

  Fframe_LinkEnd_inspector.View (aID, OBJ_PMP_TERMINAL);

  // -------------------------
//  FGeoRegion_ID := Fframe_LinkEnd_inspector.GetDataset().FieldByName(FLD_GeoRegion_ID).AsInteger;// GetIntFieldValue(FLD_GeoRegion_ID);
//  bReadOnly:=dmUser_Security.GeoRegion_is_ReadOnly(FGeoRegion_ID);

//  Fframe_LinkEnd_inspector.SetReadOnly(bReadOnly, IntToStr(FGeoRegion_ID));
//  Fframe_antennas.SetReadOnly(bReadOnly);
  // -------------------------

  Fframe_antennas.View  (aID, OBJ_PMP_TERMINAL);


end;


end.
