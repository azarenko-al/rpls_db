inherited frame_PmpTerminal_View: Tframe_PmpTerminal_View
  Left = 462
  Top = 230
  Width = 409
  Height = 499
  Caption = 'frame_PmpTerminal_View'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 401
    TabOrder = 1
  end
  inherited pn_Caption: TPanel
    Width = 401
    TabOrder = 0
  end
  inherited pn_Main: TPanel
    Width = 401
    Height = 246
    object pn_Inspector: TPanel
      Left = 1
      Top = 1
      Width = 383
      Height = 109
      Align = alTop
      BevelOuter = bvLowered
      Caption = 'pn_Inspector'
      TabOrder = 0
    end
    object cxSplitter1: TcxSplitter
      Left = 1
      Top = 154
      Width = 383
      Height = 8
      HotZoneClassName = 'TcxSimpleStyle'
      AlignSplitter = salBottom
      Control = pn_Antennas
    end
    object pn_Antennas: TPanel
      Left = 1
      Top = 162
      Width = 383
      Height = 83
      Align = alBottom
      BevelOuter = bvLowered
      Caption = 'pn_Antennas'
      TabOrder = 2
    end
  end
  inherited MainActionList: TActionList
    Left = 12
    Top = 3
  end
  inherited ImageList1: TImageList
    Left = 40
    Top = 3
  end
  inherited PopupMenu1: TPopupMenu
    Left = 104
    Top = 3
  end
  inherited cxLookAndFeelController1: TcxLookAndFeelController
    Top = 416
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    Top = 368
    DockControlHeights = (
      0
      0
      25
      0)
  end
end
