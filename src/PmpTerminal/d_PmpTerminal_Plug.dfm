object dlg_PmpTerminal_Plug: Tdlg_PmpTerminal_Plug
  Left = 1060
  Top = 265
  Width = 599
  Height = 444
  Caption = 'dlg_PmpTerminal_Plug'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 591
    Height = 105
    Caption = 'ToolBar1'
    Color = clAppWorkSpace
    EdgeBorders = []
    ParentColor = False
    TabOrder = 0
    Visible = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 381
    Width = 591
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 1
    DesignSize = (
      591
      35)
    object btn_Ok: TButton
      Left = 428
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object Button2: TButton
      Left = 510
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ModalResult = 2
      TabOrder = 1
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 105
    Width = 591
    Height = 192
    Align = alTop
    TabOrder = 2
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      object col_Property_name: TcxGridDBColumn
        DataBinding.FieldName = 'Property_name'
        SortIndex = 0
        SortOrder = soAscending
        Width = 106
      end
      object cxGrid1DBTableView1pmp_site_name: TcxGridDBColumn
        DataBinding.FieldName = 'pmp_site_name'
        SortIndex = 1
        SortOrder = soAscending
        Width = 109
      end
      object cxGrid1DBTableView1name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        SortIndex = 2
        SortOrder = soAscending
        Width = 179
      end
      object col_band: TcxGridDBColumn
        Caption = #1044#1080#1072#1087#1072#1079#1086#1085
        DataBinding.FieldName = 'band'
        Width = 41
      end
      object cxGrid1DBTableView1Column1: TcxGridDBColumn
        Caption = #1044#1080#1089#1090#1072#1085#1094#1080#1103', m'
        DataBinding.FieldName = 'Length_m'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0'
      end
      object col_pmp_site_id: TcxGridDBColumn
        DataBinding.FieldName = 'pmp_site_id'
        Visible = False
      end
      object col_ID: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
      end
      object cxGrid1DBTableView1Column2: TcxGridDBColumn
        DataBinding.FieldName = 'pmp_Terminal_id'
        Visible = False
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc1
    Left = 36
    Top = 52
  end
  object FormPlacement1: TFormPlacement
    IniFileName = 'Software\Onega\'
    Left = 104
    Top = 8
  end
  object ADOStoredProc1: TADOStoredProc
    LockType = ltReadOnly
    Parameters = <>
    Left = 32
    Top = 8
  end
end
