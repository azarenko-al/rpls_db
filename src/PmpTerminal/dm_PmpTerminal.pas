unit dm_PmpTerminal;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms,

  dm_Onega_DB_data,

  dm_Main,

  u_types,

  u_func,
  u_db,
  u_GEO,
  u_dlg,
  u_classes,

  u_const_db,
  u_const_str,

  dm_LinkEnd,

  dm_Object_base
  ;

type

  TdmPmpTerminal = class(TdmObject_base)
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);

  private
    public
    function Del (aID: integer): boolean; override;
    function GetNewName(aPropertyID: integer): string;

    function SwitchSector(aID, aNewSectorID: integer): Integer;

  end;



function dmPmpTerminal: TdmPmpTerminal;

//==================================================================
implementation  {$R *.dfm}
//==================================================================

var
 FdmPmpTerminal: TdmPmpTerminal;

// ---------------------------------------------------------------
function dmPmpTerminal: TdmPmpTerminal;
// ---------------------------------------------------------------
begin
 if not Assigned(FdmPmpTerminal) then            
     FdmPmpTerminal := TdmPmpTerminal.Create(Application);

 Result := FdmPmpTerminal;
end;

//-------------------------------------------------------------------
procedure TdmPmpTerminal.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

//  TableName:=TBL_PmpTerminal;
  TableName:=VIEW_Pmp_Terminal;
  ObjectName:=OBJ_PMP_TERMINAL;
end;

// ---------------------------------------------------------------
function TdmPmpTerminal.GetNewName(aPropertyID: integer): string;
// ---------------------------------------------------------------
var
  rec: TObject_GetNewName_Params;
begin
  FillChar(rec,SizeOf(rec),0);
  rec.ObjName := OBJ_PMP_TERMINAL;
  rec.Project_ID := dmMain.ProjectID;
  rec.Property_ID := aPropertyID;


  Result := dmOnega_DB_data.Object_GetNewName_new(rec, OBJ_PMP_TERMINAL, dmMain.ProjectID, aPropertyID);

end;



//-------------------------------------------------------------------
function TdmPmpTerminal.Del (aID: integer): boolean;
//-------------------------------------------------------------------
begin
  Result:=dmOnega_DB_data.PMPTERMINAL_DEL(aID)>0;
end;

//--------------------------------------------------------------------
function TdmPmpTerminal.SwitchSector(aID, aNewSectorID: integer): Integer;
//--------------------------------------------------------------------
 begin
  Result:= dmOnega_DB_data.PMP_TERMINAL_PLUG(ADOStoredProc1, aID, aNewSectorID);

(*
  Result := dmOnega_DB_data.OpenStoredProc(ADOStoredProc1,
            SP_PMP_TERMINAL_PLUG,
                [db_Par(FLD_ID, aID),
                 db_Par(FLD_PMP_SECTOR_ID, aNewSectorID)
                 ]);
*)


end;


end.

