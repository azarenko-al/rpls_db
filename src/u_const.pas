unit u_const;

interface
uses Messages;


{const
  // debug info
  DEBUG_ON = True; //False; //True;
}

const
//  APPLICATION_CAPTION = 'RPLS-DB Link - Version 2008.06.24';
//  APPLICATION_CAPTION1 = 'RPLS-DB Link - '; //Version 2008.08.01
  APPLICATION_CAPTION1 = 'ONEPLAN RPLS-DB Link - '; //Version 2008.08.01


//-----------------------------------------
// REGISTRY
//-----------------------------------------

  REGISTRY_COMMON_FORMS = 'Software\Onega\Common\Forms\';

  REGISTRY_ROOT      = 'Software\Onega\RPLS_DB_LINK\';
//  REGISTRY_OPTIONS   = REGISTRY_ROOT + 'Options\';
  REGISTRY_FORMS     = REGISTRY_ROOT + 'Forms\';
  REGISTRY_PROJECTS1 = REGISTRY_ROOT + 'Projects_Link\';
  REGISTRY_LOGIN     = REGISTRY_ROOT + 'Login\';


  REGISTRY_MapAttributes    = REGISTRY_ROOT + 'MapAttributes\';
  REGISTRY_MapObjectsLabels1 = REGISTRY_ROOT + 'MapObjectsLabels\';



{  //3g
  REGISTRY_TEMP    = REGISTRY_ROOT + 'Temp\';
  REG_IDENT_NETCONFIG_ID = 'NetConfig_ID';
}

const
 // EXE_LINK_PASSIVE_CALC = 'LINK_PASSIVE_CALC.exe';


 // CALC_PROG_NAME = 'rpls_rrl.exe';
 // EXE_RPLS_RRL ='rpls_rrl.exe';

  EXE_ANT_TYPE_IMPORT       = 'dll_AntType_Import.exe';
  EXE_CALC_FILE_NAME        = 'dll_CalcRegion_calc.exe';
//  EXE_FREQPLAN_CALC         = 'FreqPlan_Calc.exe';
  EXE_LINK_REPORT           = 'dll_link_report.exe';
  EXE_LINK_CALC             = 'dll_link_calc.exe';
  EXE_LINK_FREQPLAN_NB_CALC = 'dll_LinkFreqPlan_NB_Calc.exe';
  EXE_LINK_FREQPLAN_CIA     = 'dll_LinkFreqPlan_CIA.exe';
  EXE_LINKFREQPLAN_CIA_maps = 'dll_LinkFreqPlan_CIA_maps.exe';
  EXE_LINKFREQPLAN_CALC     = 'dll_LinkFreqPlan_calc.exe';

  EXE_LINK_ACAD             = 'link_ACAD.exe';


//  EXE_BIN_TO_MIF            = 'rpls_BIN_TO_MIF.exe';
//  EXE_RPLS_BIN_TO_DM        = 'rpls_bin_to_dm.exe';
  EXE_RPLS_BIN_TO_MIF       = 'rpls_bin_to_mif.exe';
  EXE_RPLS_RRL              = 'rpls_rrl.exe';
  EXE_RPLS_CALC             = 'rpls_calc.exe';
  EXE_RPLS_FREQ             = 'rpls_freq.exe';
  EXE_RPLS_FREQ_CIA         = 'RPLS_FREQ_CIA.exe';
  EXE_RPLS_JOIN             = 'rpls_calc_manager.exe';
 // EXE_RPLS_RRL              = 'rpls_rrl.exe';

  EXE_LOS                   = 'los.exe';


  PROG_data_export_import = 'data_export_import.exe';


(*  PROG_PROJECT_EXPORT      = 'ProjectExport.exe';
  PROG_PROJECT_IMPORT      = 'ProjectImport.exe';
  PROG_GUIDES_EXPORT       = 'GuidesExport.exe';
  PROG_GUIDES_IMPORT       = 'GuidesImport.exe';
*)
  PROG_Excel_to_DB         = 'Excel_to_DB.exe';

  PROG_Google              = 'Google.exe';
 // PROG_Megafon_import      = 'Megafon_import.exe';

  EXE_LINK_GRAPH = 'LINKGRAPH.exe';
  EXE_DLL_LINK_FREQ_report = 'DLL_LINK_FREQ_report.exe';



 //////// PROG_NRI_IMPORT          = 'bee_NRI_import.exe';
  PROG_BEE_SDB_IMPORT           = 'bee_SDB_import.exe';

//  PROG_BEE_TN_EXPORT           = 'bee_TN_Export.exe';

//  PROG_IMPORT_PROJECT_FROM_XML      = 'RPLS_xml_to_DB.exe';



implementation

end.




