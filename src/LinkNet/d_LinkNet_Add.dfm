inherited dlg_LinkNet_add: Tdlg_LinkNet_add
  Left = 854
  Top = 285
  Width = 740
  Height = 539
  Caption = 'dlg_LinkNet_add'
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 471
    Width = 732
    inherited Bevel1: TBevel
      Width = 732
    end
    inherited Panel3: TPanel
      Left = 568
      Width = 164
      inherited btn_Ok: TButton
        Left = 1
      end
      inherited btn_Cancel: TButton
        Left = 85
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 732
    inherited Bevel2: TBevel
      Width = 732
    end
    inherited pn_Header: TPanel
      Width = 732
      Height = 55
    end
  end
  inherited Panel1: TPanel
    Width = 732
    inherited ed_Name_: TEdit
      Width = 720
    end
  end
  inherited Panel2: TPanel
    Width = 732
    Height = 347
    inherited PageControl1: TPageControl
      Width = 722
      Height = 337
      inherited TabSheet1: TTabSheet
        object Splitter1: TSplitter
          Left = 257
          Top = 0
          Width = 8
          Height = 306
        end
        object cxGrid1: TcxGrid
          Left = 0
          Top = 0
          Width = 257
          Height = 306
          Align = alLeft
          TabOrder = 0
          LookAndFeel.Kind = lfFlat
          object cxGridDBBandedTableView1: TcxGridDBBandedTableView
            NavigatorButtons.ConfirmDelete = False
            DataController.DataSource = DataSource_LinkLines
            DataController.Filter.MaxValueListCount = 1000
            DataController.KeyFieldNames = 'id'
            DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086': 0'
                Kind = skCount
                Column = cxGridDBBandedColumn1
              end>
            DataController.Summary.SummaryGroups = <>
            Filtering.ColumnPopup.MaxDropDownItemCount = 12
            OptionsSelection.HideFocusRectOnExit = False
            OptionsSelection.HideSelection = True
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsView.ColumnAutoWidth = True
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.GroupFooters = gfVisibleWhenExpanded
            OptionsView.BandHeaders = False
            Preview.AutoHeight = False
            Preview.MaxLineCount = 2
            Bands = <
              item
                Caption = #1056#1056' '#1051#1080#1085#1080#1080
              end>
            object cxGridDBBandedColumn1: TcxGridDBBandedColumn
              Caption = #1056#1056' '#1051#1080#1085#1080#1080
              DataBinding.FieldName = 'name'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.MaxLength = 0
              Properties.ReadOnly = True
              Options.Editing = False
              Options.Filtering = False
              SortIndex = 0
              SortOrder = soAscending
              Width = 257
              Position.BandIndex = 0
              Position.ColIndex = 2
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn2: TcxGridDBBandedColumn
              Caption = #1048#1044
              DataBinding.FieldName = 'ID'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.MaxLength = 0
              Properties.ReadOnly = True
              Visible = False
              Options.Filtering = False
              Width = 23
              Position.BandIndex = 0
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn3: TcxGridDBBandedColumn
              Caption = ' '
              DataBinding.FieldName = 'checked'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.ImmediatePost = True
              Properties.NullStyle = nssUnchecked
              Options.Filtering = False
              Width = 22
              Position.BandIndex = 0
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
          end
          object cxGridLevel2: TcxGridLevel
            GridView = cxGridDBBandedTableView1
          end
        end
        object cxGrid2: TcxGrid
          Left = 265
          Top = 0
          Width = 288
          Height = 306
          Align = alLeft
          TabOrder = 1
          LookAndFeel.Kind = lfFlat
          object cxGridDBBandedTableView2: TcxGridDBBandedTableView
            NavigatorButtons.ConfirmDelete = False
            DataController.DataSource = DataSource_Links
            DataController.Filter.MaxValueListCount = 1000
            DataController.KeyFieldNames = 'id'
            DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086': 0'
                Kind = skCount
                Column = cxGridDBBandedColumn5
              end>
            DataController.Summary.SummaryGroups = <>
            Filtering.ColumnPopup.MaxDropDownItemCount = 12
            OptionsSelection.HideFocusRectOnExit = False
            OptionsSelection.HideSelection = True
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsView.ColumnAutoWidth = True
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.GroupFooters = gfVisibleWhenExpanded
            OptionsView.BandHeaders = False
            Preview.AutoHeight = False
            Preview.MaxLineCount = 2
            Bands = <
              item
                Caption = #1056#1056' '#1048#1085#1090#1077#1088#1074#1072#1083#1099
              end>
            object cxGridDBBandedColumn4: TcxGridDBBandedColumn
              Caption = #1048#1044
              DataBinding.FieldName = 'ID'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.MaxLength = 0
              Properties.ReadOnly = True
              Visible = False
              Options.Filtering = False
              Width = 88
              Position.BandIndex = 0
              Position.ColIndex = 2
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn5: TcxGridDBBandedColumn
              Caption = #1056#1056' '#1048#1085#1090#1077#1088#1074#1072#1083#1099
              DataBinding.FieldName = 'NAME'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.MaxLength = 0
              Properties.ReadOnly = True
              Options.Editing = False
              Options.Filtering = False
              SortIndex = 0
              SortOrder = soAscending
              Width = 250
              Position.BandIndex = 0
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn6: TcxGridDBBandedColumn
              Caption = ' '
              DataBinding.FieldName = 'checked'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.ImmediatePost = True
              Properties.NullStyle = nssUnchecked
              Options.Filtering = False
              Width = 21
              Position.BandIndex = 0
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
          end
          object cxGridLevel3: TcxGridLevel
            GridView = cxGridDBBandedTableView2
          end
        end
      end
    end
  end
  inherited ActionList1: TActionList
    inherited act_Ok: TAction
      OnExecute = _ActionMenu
    end
    inherited act_Cancel: TAction
      OnExecute = _ActionMenu
    end
  end
  inherited FormStorage1: TFormStorage
    Left = 394
    Top = 2
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 497
  end
  object ADOStoredProc_LinkLines: TADOStoredProc
    Parameters = <>
    Left = 616
    Top = 272
  end
  object ADOStoredProc_Links: TADOStoredProc
    Parameters = <>
    Left = 616
    Top = 152
  end
  object DataSource_Links: TDataSource
    DataSet = ADOStoredProc_Links
    Left = 613
    Top = 207
  end
  object DataSource_LinkLines: TDataSource
    DataSet = ADOStoredProc_LinkLines
    Left = 613
    Top = 319
  end
end
