object frame_LinkNet_Links: Tframe_LinkNet_Links
  Left = 590
  Top = 313
  Width = 703
  Height = 418
  Caption = 'frame_LinkNet_Links'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 209
    Width = 695
    Height = 181
    Align = alBottom
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = DataSource1
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = #1042#1089#1077#1075#1086': 0'
          Kind = skCount
          Column = cxGrid1DBTableView1name
        end>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Editing = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object col_ID_: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        VisibleForCustomization = False
        Width = 28
      end
      object cxGrid1DBTableView1name: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Options.Filtering = False
        Width = 155
      end
      object col_ObjName: TcxGridDBColumn
        DataBinding.FieldName = 'ObjName'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        VisibleForCustomization = False
        Width = 34
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 128
    Top = 9
    object actLineAdd1: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    end
    object actLineDel1: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100
    end
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc1
    Left = 40
    Top = 64
  end
  object ADOStoredProc1: TADOStoredProc
    Parameters = <>
    Left = 40
    Top = 16
  end
  object ApplicationEvents1: TApplicationEvents
    Left = 240
    Top = 8
  end
  object ActionList1: TActionList
    Left = 336
    Top = 8
    object actLineAdd11: TAction
      Caption = 'actLineAdd11'
      OnExecute = actLineAdd11Execute
    end
    object actLineDel12: TAction
      Caption = 'actLineDel12'
      OnExecute = actLineAdd11Execute
    end
  end
end
