unit fr_LinkNet_Links;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, dxmdaset,  ExtCtrls,
  cxGridDBTableView, cxControls, cxGridCustomView, cxClasses, cxGridLevel,

cxGrid,

  u_func_msg,

  u_func,

  u_Storage,

  dm_Onega_DB_data,

  I_Shell,

  f_Custom,


  dm_LinkNet,
  dm_LinkFreqPlan,

  d_LinkNet_Add,

  u_classes,
  u_db,
  u_dlg,

  u_types,

  u_const_DB,

  u_const_msg,
  u_const_str,

  DB, ADODB, Menus, cxGridCustomTableView, cxGridTableView,
  cxPropertiesStore, ToolWin, ComCtrls, AppEvnts
  ;




type
  Tframe_LinkNet_Links = class(TForm, IMessageHandler)
    PopupMenu1: TPopupMenu;
    actLineAdd1: TMenuItem;
    actLineDel1: TMenuItem;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    col_ID_: TcxGridDBColumn;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    col_ObjName: TcxGridDBColumn;
    DataSource1: TDataSource;
    ADOStoredProc1: TADOStoredProc;
    ApplicationEvents1: TApplicationEvents;
    ActionList1: TActionList;
    actLineAdd11: TAction;
    actLineDel12: TAction;
    procedure actLineAdd11Execute(Sender: TObject);
//    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure MainActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormDestroy(Sender: TObject);

     procedure GetMessage(aMsg: TEventType; aParams: TEventParamList ; var aHandled:
         boolean);
  private
    FID: integer;
  protected
//    procedure GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean); override;

  public
    procedure SetReadOnly(Value: Boolean);
    procedure View (aID: integer);
   // class function CreateChildForm ( aDest: TWinControl): Tframe_LinkNet_Links;

  end;

//==================================================================

//==================================================================
implementation {$R *.DFM}



procedure Tframe_LinkNet_Links.actLineAdd11Execute(Sender: TObject);
begin
//
end;

//--------------------------------------------------------------------
procedure Tframe_LinkNet_Links.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
  cxGrid1.Align:=alClient;
  cxGrid1DBTableView1.DataController.DataSource:=DataSource1;

 // cxGrid1DBTableView1.RestoreFromRegistry(FRegPath+ cxGrid1DBTableView1.Name);

  g_Storage.RestoreFromRegistry(cxGrid1DBTableView1, className);

end;


procedure Tframe_LinkNet_Links.FormDestroy(Sender: TObject);
begin
 // cxGrid1DBTableView1.StoreToRegistry(FRegPath+ cxGrid1DBTableView1.Name);

  g_Storage.StoreToRegistry(cxGrid1DBTableView1, className);

  inherited;
end;

//--------------------------------------------------------------------
procedure Tframe_LinkNet_Links.View (aID: integer);
//--------------------------------------------------------------------
begin
  FID:=aID;

  dmOnega_DB_data.LinkNet_SELECT_items (ADOStoredProc1, aID);


//  StatusBar1.SimpleText:=Format('�����: %d', [mem_Links.RecordCount]);
end;


//--------------------------------------------------------------------
procedure Tframe_LinkNet_Links.MainActionListUpdate(Action: TBasicAction; var Handled: Boolean);
begin
 // act_Del.Enabled:=(cxGrid1DBTableView1.Controller.SelectedRowCount>0);

 // StatusBar1.SimpleText:=Format('�����: %d', [mem_Links.RecordCount]);
end;




procedure Tframe_LinkNet_Links.GetMessage(aMsg: TEventType; aParams:
    TEventParamList ; var aHandled: boolean);
begin
  case aMsg of
    WE_LINKNET_LINKS_REFRESH_: View(FID); // (aParams[0].Value);
  end;

end;


procedure Tframe_LinkNet_Links.SetReadOnly(Value: Boolean);
begin
//  cxGrid1DBTableView1.OptionsData.Editing := not Value;
//  FReadOnly := Value;

  ADOStoredProc1.LockType := IIF(Value, ltReadOnly, ltBatchOptimistic);
  SetFormActionsEnabled(Self, not Value);

//  ADOStoredProc1.LockType := IIF(dmUser_Security.ProjectIsReadOnly, ltReadOnly, ltBatchOptimistic);
//  SetFormActionsEnabled(Self, not dmUser_Security.ProjectIsReadOnly);

end;


end.


(*
//-------------------------------------------------------------------
procedure Tframe_LinkNet_Links.GetMessage (aMsg: integer; aParams: TEventParamList;
                                 var aHandled: Boolean);
//-------------------------------------------------------------------
begin
  case aMsg of
    WE_LINKNET_LINKS_REFRESH_: View (aParams[0].Value);
  end;
end;



procedure Tframe_LinkNet_Links.ApplicationEvents1Message(var Msg: tagMSG; var
    Handled: Boolean);
begin
  case Msg.message of
    WE_LINKNET_LINKS_REFRESH_: View(FID); // (aParams[0].Value);
  end;

end;