unit fr_LinkNet_add;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView,
  cxClasses, cxControls, cxGridCustomView, cxGrid,

  dm_Onega_DB_data,

  u_db,
  dm_Main;



type
  Tframe_LinkNet_add = class(TForm)
    cxGrid1: TcxGrid;
    cxGridDBBandedTableView1: TcxGridDBBandedTableView;
    cxGridDBBandedColumn1: TcxGridDBBandedColumn;
    cxGridDBBandedColumn2: TcxGridDBBandedColumn;
    cxGridDBBandedColumn3: TcxGridDBBandedColumn;
    cxGridLevel2: TcxGridLevel;
    cxGrid2: TcxGrid;
    cxGridDBBandedTableView2: TcxGridDBBandedTableView;
    cxGridDBBandedColumn4: TcxGridDBBandedColumn;
    cxGridDBBandedColumn5: TcxGridDBBandedColumn;
    cxGridDBBandedColumn6: TcxGridDBBandedColumn;
    cxGridLevel3: TcxGridLevel;
    Splitter1: TSplitter;
    DataSource_LinkLines: TDataSource;
    ADOStoredProc_LinkLines: TADOStoredProc;
    DataSource_Links: TDataSource;
    ADOStoredProc_Links: TADOStoredProc;
    procedure FormCreate(Sender: TObject);
  private
    procedure OpenData(aLinkNet_ID: Integer);

  public

  end;

(*var
  frame_LinkNet_add: Tframe_LinkNet_add;
*)


implementation


{$R *.dfm}

procedure Tframe_LinkNet_add.FormCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
procedure Tframe_LinkNet_add.OpenData(aLinkNet_ID: Integer);
//--------------------------------------------------------------------
begin
  if aLinkNet_ID>0 then
  begin
    dmOnega_DB_data.OpenStoredProc(ADOStoredProc_Links,
           'sp_LinkNet_select_items_for_add',
           [db_Par(FLD_ID, aLinkNet_ID),
            db_Par(FLD_OBJName, 'link')
           ]);

    dmOnega_DB_data.OpenStoredProc(ADOStoredProc_LinkLines,
           'sp_LinkNet_select_items_for_add',
           [db_Par(FLD_ID, aLinkNet_ID),
            db_Par(FLD_OBJName, 'linkLine')
           ]);
  end else
  begin
    dmOnega_DB_data.OpenStoredProc(ADOStoredProc_Links,
           'sp_LinkNet_select_items_for_add',
           [db_Par(FLD_PROJECT_ID, dmMain.ProjectID),
            db_Par(FLD_OBJName, 'link')
           ]);

    dmOnega_DB_data.OpenStoredProc(ADOStoredProc_LinkLines,
           'sp_LinkNet_select_items_for_add',
           [db_Par(FLD_PROJECT_ID, dmMain.ProjectID),
            db_Par(FLD_OBJName, 'linkLine')
           ]);
  end;



end;

end.