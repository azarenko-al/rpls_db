unit dm_act_LinkNet_Link_Ref;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Dialogs,  Menus,
  Controls, Forms, ActnList,

  dm_Main,
  I_Object,
  
  u_Geo,
  
  u_func,
  u_db,

  u_types,
  
  u_const_db,

  fr_Link_view,

  dm_act_Base,

  dm_LinkNet_Item_Ref,
  dm_LinkFreqPlan,

  dm_Link

  ;

type
  TdmAct_LinkNet_Link_Ref = class(TdmAct_Base)
    procedure DataModuleCreate(Sender: TObject);

  private
// TODO: ItemDel
//  function ItemDel(aID: integer): boolean; override;
  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm  (aOwnerForm: TForm): TForm; override;

    procedure DoAction (Sender: TObject);

    procedure ShowOnMap (aID: integer);
  public
    class procedure Init;
  end;


var
  dmAct_LinkNet_Link_Ref: TdmAct_LinkNet_Link_Ref;

//==================================================================
implementation {$R *.dfm}
//==================================================================



//--------------------------------------------------------------------
class procedure TdmAct_LinkNet_Link_Ref.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_LinkNet_Link_Ref) then
    dmAct_LinkNet_Link_Ref:=TdmAct_LinkNet_Link_Ref.Create(Application);
end;

//--------------------------------------------------------------------
procedure TdmAct_LinkNet_Link_Ref.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_LINKNET_LINK_REF;

  SetActionsExecuteProc ([act_Object_ShowOnMap], DoAction);
end;

//--------------------------------------------------------------------
procedure TdmAct_LinkNet_Link_Ref.ShowOnMap (aID: integer);
//--------------------------------------------------------------------
var iLinkID: integer;
  BLVector: TBLVector;
begin
  iLinkID:=dmLinkNet_Item_Ref.GetLinkID (aID);

  if dmLink.GetBLVector(iLinkID, BLVector) then
    ShowVectorOnMap (BLVector, aID);
end;

//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_LinkNet_Link_Ref.DoAction(Sender: TObject);
begin
  if Sender=act_Object_ShowOnMap then ShowOnMap (FFocusedID);
end;


//--------------------------------------------------------------------
function TdmAct_LinkNet_Link_Ref.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_Link_View.Create(aOwnerForm);
  (Result as Tframe_Link_View).ViewMode:=vmRR_Net_Link_Ref;
end;

//--------------------------------------------------------------------
procedure TdmAct_LinkNet_Link_Ref.GetPopupMenu;
//--------------------------------------------------------------------
begin
  case aMenuType of
    mtItem:   begin
                AddMenuItem (aPopupMenu, act_Object_ShowOnMap);
             //   AddMenuItem (aPopupMenu, nil);
           //     AddMenuItem (aPopupMenu, act_Del_);
              end;
  //  mtList:
       //         AddMenuItem (aPopupMenu, act_Del_list);
  end;
end;



begin

end.



// TODO: ItemDel
////--------------------------------------------------------------------
//function TdmAct_LinkNet_Link_Ref.ItemDel(aID: integer): boolean;
////--------------------------------------------------------------------
//var
//  i: Integer;
//begin
////  i:=gl_DB.GetIntFieldValue(TBL_LINKNET_ITEMS_XREF, FLD_LINKNET_ID,
// //                        [db_Par(FLD_ID, aID)  ]);
//
////  Result:=dmLinkNet_Item_Ref.Del (aID);
//(*
//Screen.Cursor:=crHourGlass;
//dmLinkFreqPlan.Update_LinkList(i);
//Screen.Cursor:=crDefault;*)
//
//end;

