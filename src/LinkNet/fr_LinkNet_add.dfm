object frame_LinkNet_add: Tframe_LinkNet_add
  Left = 905
  Top = 246
  Width = 800
  Height = 367
  Caption = 'frame_LinkNet_add'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 120
  TextHeight = 16
  object Splitter1: TSplitter
    Left = 346
    Top = 0
    Width = 10
    Height = 334
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 346
    Height = 334
    Align = alLeft
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    object cxGridDBBandedTableView1: TcxGridDBBandedTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'id'
      DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086': 0'
          Kind = skCount
          Column = cxGridDBBandedColumn1
        end>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.HideSelection = True
      OptionsSelection.InvertSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.BandHeaders = False
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      Bands = <
        item
          Caption = #1056#1056' '#1051#1080#1085#1080#1080
        end>
      object cxGridDBBandedColumn1: TcxGridDBBandedColumn
        Caption = #1056#1056' '#1051#1080#1085#1080#1080
        DataBinding.FieldName = 'name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Options.Editing = False
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soAscending
        Width = 257
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object cxGridDBBandedColumn2: TcxGridDBBandedColumn
        Caption = #1048#1044
        DataBinding.FieldName = 'ID'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 23
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object cxGridDBBandedColumn3: TcxGridDBBandedColumn
        Caption = ' '
        DataBinding.FieldName = 'checked'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.ImmediatePost = True
        Properties.NullStyle = nssUnchecked
        Options.Filtering = False
        Width = 22
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object cxGridLevel2: TcxGridLevel
      GridView = cxGridDBBandedTableView1
    end
  end
  object cxGrid2: TcxGrid
    Left = 356
    Top = 0
    Width = 325
    Height = 334
    Align = alLeft
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    object cxGridDBBandedTableView2: TcxGridDBBandedTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'id'
      DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086': 0'
          Kind = skCount
          Column = cxGridDBBandedColumn5
        end>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.HideSelection = True
      OptionsSelection.InvertSelect = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.BandHeaders = False
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      Bands = <
        item
          Caption = #1056#1056' '#1048#1085#1090#1077#1088#1074#1072#1083#1099
        end>
      object cxGridDBBandedColumn4: TcxGridDBBandedColumn
        Caption = #1048#1044
        DataBinding.FieldName = 'ID'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 88
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object cxGridDBBandedColumn5: TcxGridDBBandedColumn
        Caption = #1056#1056' '#1048#1085#1090#1077#1088#1074#1072#1083#1099
        DataBinding.FieldName = 'NAME'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Options.Editing = False
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soAscending
        Width = 250
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object cxGridDBBandedColumn6: TcxGridDBBandedColumn
        Caption = ' '
        DataBinding.FieldName = 'checked'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.ImmediatePost = True
        Properties.NullStyle = nssUnchecked
        Options.Filtering = False
        Width = 21
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object cxGridLevel3: TcxGridLevel
      GridView = cxGridDBBandedTableView2
    end
  end
  object DataSource_LinkLines: TDataSource
    DataSet = ADOStoredProc_LinkLines
    Left = 629
    Top = 199
  end
  object ADOStoredProc_LinkLines: TADOStoredProc
    Parameters = <>
    Left = 632
    Top = 152
  end
  object DataSource_Links: TDataSource
    DataSet = ADOStoredProc_Links
    Left = 629
    Top = 87
  end
  object ADOStoredProc_Links: TADOStoredProc
    Parameters = <>
    Left = 632
    Top = 32
  end
end
