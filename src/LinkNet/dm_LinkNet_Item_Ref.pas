unit dm_LinkNet_Item_Ref;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  dm_Custom,

  u_db,

  u_const_db,
  
  dm_Main,

  Db,  ADODB

  ;


type
  TdmLinkNet_Item_Ref = class(TDataModule)

    qry_LinkEnd: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    function  Del (aID: integer): boolean;

  public
    function  GetLinkLineID (aID: integer): integer;
    function  GetLinkID (aID: integer): integer;

  end;

function dmLinkNet_Item_Ref: TdmLinkNet_Item_Ref;


implementation {$R *.DFM}

var
  FdmLinkNet_Item_Ref: TdmLinkNet_Item_Ref;


// ---------------------------------------------------------------
function dmLinkNet_Item_Ref: TdmLinkNet_Item_Ref;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLinkNet_Item_Ref) then
    FdmLinkNet_Item_Ref := TdmLinkNet_Item_Ref.Create(Application);

   Result := FdmLinkNet_Item_Ref;
end;

procedure TdmLinkNet_Item_Ref.DataModuleCreate(Sender: TObject);
begin
   db_SetComponentADOConnection (Self, dmMain.ADOConnection);

end;

// -------------------------------------------------------------------
function TdmLinkNet_Item_Ref.GetLinkID (aID: integer): integer;
// -------------------------------------------------------------------
begin
  Result:=gl_DB.GetIntFieldValue (TBL_LINKNET_ITEMS_XREF, FLD_LINK_ID,
                                  [db_Par(FLD_ID, aID)]);
end;

// -------------------------------------------------------------------
function TdmLinkNet_Item_Ref.GetLinkLineID (aID: integer): integer;
// -------------------------------------------------------------------
begin
  Result:=gl_DB.GetIntFieldValue (TBL_LINKNET_ITEMS_XREF, FLD_LINKLINE_ID,
                                  [db_Par(FLD_ID, aID)]);
end; 

//--------------------------------------------------------------------
function TdmLinkNet_Item_Ref.Del (aID: integer): boolean;
//--------------------------------------------------------------------
begin
  Result:= gl_db.DeleteRecordByID(TBL_LINKNET_ITEMS_XREF, aID);
end;

begin

end.
