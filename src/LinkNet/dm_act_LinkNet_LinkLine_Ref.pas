unit dm_act_LinkNet_LinkLine_Ref;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Dialogs,  Menus,
  Controls, Forms, ActnList,

  dm_Main,
  I_Object,

  u_types,

  u_func,
  u_db,

  u_const_db,

  fr_LinkLine_view,

  dm_act_Base,
  dm_LinkNet_Item_Ref,

  dm_LinkLine,
  dm_LinkFreqPlan

  ;

type
  TdmAct_LinkNet_LinkLine_Ref = class(TdmAct_Base)
    procedure DataModuleCreate(Sender: TObject);

  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm  (aOwnerForm: TForm): TForm; override;

    procedure DoAction (Sender: TObject);

    function ItemDel(aID: integer; aName: string = ''): boolean; override;

    procedure ShowOnMap (aID: integer);
  public
    class procedure Init;
  end;


var
  dmAct_LinkNet_LinkLine_Ref: TdmAct_LinkNet_LinkLine_Ref;

//==================================================================
implementation {$R *.dfm}
//==================================================================


//--------------------------------------------------------------------
class procedure TdmAct_LinkNet_LinkLine_Ref.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_LinkNet_LinkLine_Ref) then
    dmAct_LinkNet_LinkLine_Ref:=TdmAct_LinkNet_LinkLine_Ref.Create(Application);
end;

//--------------------------------------------------------------------
procedure TdmAct_LinkNet_LinkLine_Ref.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_LINKNET_LINKLINE_REF;

  SetActionsExecuteProc ([act_Object_ShowOnMap], DoAction);
end;

//--------------------------------------------------------------------
procedure TdmAct_LinkNet_LinkLine_Ref.ShowOnMap (aID: integer);
//--------------------------------------------------------------------
var iLinkLineID: integer;
begin
  iLinkLineID:=dmLinkNet_Item_Ref.GetLinkLineID (aID);
  ShowRectOnMap (dmLinkLine.GetBLRect(iLinkLineID), aID);
end;

//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_LinkNet_LinkLine_Ref.DoAction(Sender: TObject);
begin
  if Sender=act_Object_ShowOnMap then ShowOnMap (FFocusedID);
end;


//--------------------------------------------------------------------
function TdmAct_LinkNet_LinkLine_Ref.ItemDel(aID: integer; aName: string = ''):
    boolean;
//--------------------------------------------------------------------
var
  iID: Integer;
begin
 // iID:=gl_DB.GetIntFieldValue(TBL_LINKNET_ITEMS_XREF, FLD_LINKNET_ID,
                      //     [db_Par(FLD_ID, aID)  ]);

 // Result:=dmLinkNet_Item_Ref.Del (aID);
(*
  Screen.Cursor:=crHourGlass;
  dmLinkFreqPlan.Update_LinkList(iID);
  Screen.Cursor:=crDefault;*)

end;

//--------------------------------------------------------------------
function TdmAct_LinkNet_LinkLine_Ref.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_LinkLine_View.Create(aOwnerForm);
  (Result as Tframe_LinkLine_View).ViewMode:=vmRR_Net_LinkLine_Ref;
end;

//--------------------------------------------------------------------
procedure TdmAct_LinkNet_LinkLine_Ref.GetPopupMenu;
//--------------------------------------------------------------------
begin
  case aMenuType of
    mtItem:   begin
                AddMenuItem (aPopupMenu, act_Object_ShowOnMap);
                AddMenuItem (aPopupMenu, nil);
              //  AddMenuItem (aPopupMenu, act_Del_);
              end;
   // mtList:
      //          AddMenuItem (aPopupMenu, act_Del_list);
  end;
end;


begin

end.

