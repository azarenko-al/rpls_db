unit d_LinkNet_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, ExtCtrls,
  rxPlacemnt, ActnList,     ADODB,
  Buttons, cxPropertiesStore, cxGridCustomTableView,cxGridLevel,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid,
  cxControls, cxGridCustomView, cxClasses, cxSplitter,
                 
  dm_User_Security,

  dm_Onega_DB_data,

  d_Wizard_add_with_Inspector,

  dm_Main,

  dm_LinkNet,

  u_db,
  u_dlg,                                   
  u_classes,

  u_types,
  u_const_str, ComCtrls

  ;

type
  Tdlg_LinkNet_add = class(Tdlg_Wizard_add_with_Inspector)
    ADOStoredProc_LinkLines: TADOStoredProc;
    ADOStoredProc_Links: TADOStoredProc;
    DataSource_Links: TDataSource;
    DataSource_LinkLines: TDataSource;
    cxGrid1: TcxGrid;
    cxGridDBBandedTableView1: TcxGridDBBandedTableView;
    cxGridDBBandedColumn1: TcxGridDBBandedColumn;
    cxGridDBBandedColumn2: TcxGridDBBandedColumn;
    cxGridDBBandedColumn3: TcxGridDBBandedColumn;
    cxGridLevel2: TcxGridLevel;
    Splitter1: TSplitter;
    cxGrid2: TcxGrid;
    cxGridDBBandedTableView2: TcxGridDBBandedTableView;
    cxGridDBBandedColumn4: TcxGridDBBandedColumn;
    cxGridDBBandedColumn5: TcxGridDBBandedColumn;
    cxGridDBBandedColumn6: TcxGridDBBandedColumn;
    cxGridLevel3: TcxGridLevel;

    procedure Button1Click(Sender: TObject);
    procedure _ActionMenu(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);


  private
    FID,FFolderID, FNetID: integer;

    FIDList: TIDList;

    FIDList_link: TIDList;
    FIDList_linkline: TIDList;


    FIsEdited: boolean;

    procedure Append;
    procedure GetCheckedSiteIDList(aDataSet: TDataSet; out aIDList: TIDList);
    procedure OpenData(aLinkNet_ID: Integer);
  public
    class function Dlg_Update(aID: integer): boolean;
    class function Dlg_Add(aFolderID: integer =0; aIDList: TIDList=nil): integer;
  end;

  
//====================================================================
//====================================================================
implementation
 {$R *.DFM}
              

//--------------------------------------------------------------------
class function Tdlg_LinkNet_add.Dlg_Update(aID: integer): boolean;
//--------------------------------------------------------------------
var
  i: Integer;
begin
{
  // ---------------------------------------------------------------
  if dmUser_Security.ProjectIsReadOnly then
  begin
    ShowMessage('���������� �������� ������. ������ ������ ��� ������.');
    Result:=False;
    Exit;
  end;
  // ---------------------------------------------------------------
 }

  with Tdlg_LinkNet_add.Create(Application) do
  try
    FID := aID;

    ed_Name_.Text:=dmLinkNet.GetNameByID(aID);
    FIsEdited:=true;

    OpenData(aID);

   // dmLinkNet_add.EditItem (aID);

  //  UpdateStatusBar;                            

    Result:=(ShowModal=mrOk);

  finally
    Free;
  end;
end;


//--------------------------------------------------------------------
class function Tdlg_LinkNet_add.Dlg_Add(aFolderID: integer =0; aIDList: TIDList=nil): integer;
//--------------------------------------------------------------------
begin
{
  if dmUser_Security.ProjectIsReadOnly then
  begin
    ShowMessage('���������� �������� ������. ������ ������ ��� ������.');
    Result:=0;
    Exit;
  end;
}

  with Tdlg_LinkNet_add.Create(Application) do
  try
    FFolderID:=aFolderID;
    ed_Name_.Text:=dmLinkNet.GetNewName;

    FIsEdited:=false;

    OpenData(0);

//    dmLinkNet_add.Add_all_Links_and_LinkLines();

    if ShowModal=mrOk then
      Result:=FID
    else
      Result:=0;

  finally
    Free;
  end;
end;


//--------------------------------------------------------------------
procedure Tdlg_LinkNet_add.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
  SetActionName (STR_DLG_ADD_LINKNET);

  lb_Action.Caption:= STR_DLG_ADD_LINKNET;

  FIDList:=TIDList.Create;
  FIDList_link:=TIDList.Create;
  FIDList_linkline:=TIDList.Create;

  //GSPages1.ActivePageIndex:=0;
  cxGrid2.Align := alclient;

end;


//--------------------------------------------------------------------
procedure Tdlg_LinkNet_add.OpenData(aLinkNet_ID: Integer);
//--------------------------------------------------------------------
begin
  if aLinkNet_ID>0 then
  begin
    dmOnega_DB_data.OpenStoredProc(ADOStoredProc_Links,
           'sp_LinkNet_select_items_for_add',
           [FLD_ID, aLinkNet_ID,
            FLD_OBJName, 'link'
           ]);

    dmOnega_DB_data.OpenStoredProc(ADOStoredProc_LinkLines,
           'sp_LinkNet_select_items_for_add',
           [FLD_ID, aLinkNet_ID,
            FLD_OBJName, 'linkLine'
           ]);
  end else
  begin
    dmOnega_DB_data.OpenStoredProc(ADOStoredProc_Links,
           'sp_LinkNet_select_items_for_add',
           [FLD_PROJECT_ID, dmMain.ProjectID,
            FLD_OBJName, 'link'
           ]);

    dmOnega_DB_data.OpenStoredProc(ADOStoredProc_LinkLines,
           'sp_LinkNet_select_items_for_add',
           [FLD_PROJECT_ID, dmMain.ProjectID,
            FLD_OBJName, 'linkLine'
           ]);
  end;



end;



//----------------------------------------------------------------------
procedure Tdlg_LinkNet_add.FormDestroy(Sender: TObject);
//----------------------------------------------------------------------
begin
  FreeAndNil(FIDList);
  FreeAndNil(FIDList_link);
  FreeAndNil(FIDList_linkline);

  inherited;
end;


//--------------------------------------------------------------------
procedure Tdlg_LinkNet_add.Append;
//--------------------------------------------------------------------
var
  rec: TdmLinkNetAddRec;
begin

  FillChar(rec, SizeOf(rec), 0);

  rec.Name:=ed_Name_.Text;

  if rec.Name='' then
  begin
    ErrorDlg('������� ��� ����');
    Exit;
  end;

(*
  if (FISEdited = false) and (dmLinkNet.FindByName(rec.Name)>0)  then
  begin
    ErrorDlg('��� ���� ��� ����������, ������� ������');
    Exit;
  end;
*)

 // rec.FolderID:=FFolderID;

  GetCheckedSiteIDList(ADOStoredProc_Links, FIDList_link);
  GetCheckedSiteIDList(ADOStoredProc_LinkLines, FIDList_linkline);

  if FISEdited then
    dmLinkNet.Add_new_items (FID, FIDList_link, FIDList_linkline)
  else
    FID:=dmLinkNet.Add_new (rec, FIDList_link, FIDList_linkline);

end;

procedure Tdlg_LinkNet_add.Button1Click(Sender: TObject);
begin
  inherited;
end;


//-------------------------------------------------------------------
procedure Tdlg_LinkNet_add._ActionMenu(Sender: TObject);
//-------------------------------------------------------------------
var LinkEnd1_ID, LinkEnd2_ID: integer;
begin
  if Sender=act_Ok then
  begin
    Append;
   //////// SH_PostUpdateNodeChildren(dmLinkNet.GetGUIDByID(FNetID));
  end
  else

end;



//-------------------------------------------------------------------
procedure Tdlg_LinkNet_add.GetCheckedSiteIDList(aDataSet: TDataSet; out
    aIDList: TIDList);
//-------------------------------------------------------------------
begin
  aDataSet.DisableControls;

  aDataSet.First;
  aIDList.Clear;

  with aDataSet do
    while not EOF do
  begin
    if FieldByName(FLD_CHECKED).AsBoolean then
      aIDList.AddID(FieldByName(FLD_ID).AsInteger);

    Next;
  end;

  aDataSet.EnableControls;
end;



procedure Tdlg_LinkNet_add.FormResize(Sender: TObject);
begin
  cxGrid1.Width:=TabSheet1.Width div 2;
end;

end.