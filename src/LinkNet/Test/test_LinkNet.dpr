program test_LinkNet;

uses
  a_Uni1 in 'a_Uni1.pas' {Form9},
  d_expl_Object_Select_Items in '..\..\Explorer\d_expl_Object_Select_Items.pas' {dlg_expl_Object_Select_Items},
  d_LinkNet_add in '..\d_LinkNet_Add.pas' {dlg_LinkNet_add},
  dm_act_LinkFreqPlan in '..\..\LinkFreqPlan\dm_act_LinkFreqPlan.pas' {dmAct_LinkFreqPlan: TDataModule},
  dm_act_LinkNet in '..\dm_act_LinkNet.pas' {dmAct_LinkNet: TDataModule},
  dm_act_LinkNet_Link_Ref in '..\dm_act_LinkNet_Link_Ref.pas' {dmAct_LinkNet_Link_Ref: TDataModule},
  dm_act_LinkNet_LinkLine_Ref in '..\dm_act_LinkNet_LinkLine_Ref.pas' {dmAct_LinkNet_LinkLine_Ref: TDataModule},
  dm_Explorer in '..\..\Explorer\dm_Explorer.pas' {dmExplorer: TDataModule},
  dm_LinkNet in '..\dm_LinkNet.pas' {dmLinkNet: TDataModule},
  dm_LinkNet_Add in '..\dm_LinkNet_Add.pas' {dmLinkNet_Add: TDataModule},
  dm_LinkNet_calc in '..\dm_LinkNet_calc.pas' {dmLinkNet_calc: TDataModule},
  dm_LinkNet_Item_Ref in '..\dm_LinkNet_Item_Ref.pas' {dmLinkNet_Item_Ref: TDataModule},
  dm_LinkNet_view in '..\dm_LinkNet_view.pas' {dmLinkNet_view: TDataModule},
  dm_Object_base in '..\..\Common\dm_Object_base.pas' {dmObject_base: TDataModule},
  Forms,
  fr_LinkNet_Links in '..\fr_LinkNet_Links.pas' {frame_LinkNet_Links},
  fr_LinkNet_view in '..\fr_LinkNet_view.pas' {frame_LinkNet_View},
  u_const_db in '..\..\u_const_db.pas';

{$R *.RES}




begin
  Application.Initialize;
  Application.CreateForm(TForm9, Form9);
  Application.Run;
end.
