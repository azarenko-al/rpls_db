program test_RR_Net;

uses
  Forms,
  dm_Main in '..\..\DataModules\dm_Main.pas' {dmMain: TDataModule},
  dm_Object_base in '..\..\Common\dm_Object_base.pas' {dmObject_base: TDataModule},
  dm_Custom in '..\..\Common\dm_Custom.pas' {dmCustom: TDataModule},
  d_RR_Net_add in '..\d_RR_Net_add.pas' {dlg_RR_Net_add},
  fr_RR_Net_view in '..\fr_RR_Net_view.pas' {frame_RR_Net_View},
  dm_act_RR_Net in '..\dm_act_RR_Net.pas' {dmAct_RR_Net: TDataModule},
  dm_RR_Net in '..\dm_RR_Net.pas' {dmRR_Net: TDataModule},
  a_Uni1 in 'a_Uni1.pas' {Form9},
  dm_RR_Net_Add in '..\dm_RR_Net_Add.pas' {dmRR_Net_Add: TDataModule},
  u_RR_Net_const in '..\u_RR_Net_const.pas',
  u_const_db in '..\..\u_const_db.pas',
  dm_Explorer in '..\..\Explorer\dm_Explorer.pas' {dmExplorer: TDataModule},
  d_expl_Object_Select_Items in '..\..\Explorer\d_expl_Object_Select_Items.pas' {dlg_expl_Object_Select_Items},
  dm_act_LinkFreqPlan in '..\..\LinkFreqPlan\dm_act_LinkFreqPlan.pas' {dmAct_LinkFreqPlan: TDataModule},
  fr_RR_Net_Links in '..\fr_RR_Net_Links.pas' {frame_RR_Net_Links},
  dm_RR_Net_view in '..\dm_RR_Net_view.pas' {dmRR_Net_view: TDataModule},
  dm_LinkFreqPlan in '..\..\LinkFreqPlan\dm_LinkFreqPlan.pas' {dmLinkFreqPlan: TDataModule},
  dm_RR_Net_calc in '..\dm_RR_Net_calc.pas' {dmRR_Net_calc: TDataModule},
  dm_act_RR_Net_LinkLine_Ref in '..\dm_act_RR_Net_LinkLine_Ref.pas' {dmAct_RR_Net_LinkLine_Ref: TDataModule},
  dm_act_RR_Net_Link_Ref in '..\dm_act_RR_Net_Link_Ref.pas' {dmAct_RR_Net_Link_Ref: TDataModule},
  dm_RR_Net_Item_Ref in '..\dm_RR_Net_Item_Ref.pas' {dmRR_Net_Item_Ref: TDataModule},
  dm_Main_res in '..\..\DataModules\dm_Main_res.pas' {dmMain_res: TDataModule};

{$R *.RES}




begin
  Application.Initialize;
  Application.CreateForm(TForm9, Form9);
  Application.Run;
end.
