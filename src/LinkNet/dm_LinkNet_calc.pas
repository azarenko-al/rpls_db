unit dm_LinkNet_calc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB,

  dm_Main,
//  I_Link,

  dm_Custom,

  u_func,
  u_db,
  u_log,

  u_const_db,

  
  dm_LinkNet,
  dm_LinkLine,


  u_linkLine_const,
  u_link_const,


  dxmdaset
  ;

type
  TdmLinkNet_calc = class(TDataModule)
    qry_Links: TADOQuery;
    qry_Summary: TADOQuery;
    qry_LinkLine: TADOQuery;
    mem_Links: TdxMemData;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);

  private
    function Execute(aID: integer): boolean;
  public
    procedure UpdateSummary1 (aID: integer);


  end;

function dmLinkNet_calc: TdmLinkNet_calc;

//==================================================================
implementation  {$R *.DFM}
//==================================================================

//uses dm_Act_Link;


var
  FdmLinkNet_calc: TdmLinkNet_calc;


// ---------------------------------------------------------------
function dmLinkNet_calc: TdmLinkNet_calc;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLinkNet_calc) then
    FdmLinkNet_calc := TdmLinkNet_calc.Create(Application);

  Result := FdmLinkNet_calc;
end;




//--------------------------------------------------------------------
function TdmLinkNet_calc.Execute(aID: integer): boolean;
//--------------------------------------------------------------------
const
  SQL_SELECT_LINKLINE_LINKS =
     'SELECT * FROM '+ view_LinkLine_links +
     ' WHERE (linkline_id=:id)';
begin


  CursorHourGlass();

// db_OpenQuery (qry_Links, SQL_SELECT_LINKLINE_LINKS, [db_Par(FLD_ID, aID)]);

  dmLinkNet.GetLinks_for_Net(aID, mem_Links);

  with mem_Links do
    while not EOF do
  begin
    g_Log.Add ('������ ���������: '+ FieldValues[FLD_NAME]);

    dmAct_Link.Calc(FieldValues[FLD_ID]);
    Next;
  end;


 ///////////////////////// UpdateSummary(aID);

  CursorDefault();

end;

//--------------------------------------------------------------------
procedure TdmLinkNet_calc.UpdateSummary1 (aID: integer);
//--------------------------------------------------------------------
const
 SQL_SUMMARY =
     'SELECT SUM(SESR) AS SESR_, SUM(Kng) AS Kng_, SUM(length) AS Length_ '+
     ' FROM '+ view_LinkLine_links +
     ' WHERE (linkline_id=:id)';

  SQL_UPDATE_LINKLINE =
     'UPDATE '+ TBL_LINKLINE +
     ' SET SESR=:SESR, Kng=:Kng, Length=:Length '+
     ' WHERE id=:id';

var
  iType,iStatus: integer;
  kng_norm,length_norm,sesr_norm,
  kng_req,sesr_req,
  sesr,kng,
  length: double;
begin
 // CursorHourGlass();

  dmOnega_DB_data.OpenQuery (qry_Summary, SQL_SUMMARY, [FLD_ID, aID]);

  with qry_Summary do
    gl_DB.ExecCommand (SQL_UPDATE_LINKLINE,
                      [db_Par(FLD_ID, aID),

                       db_Par(FLD_SESR,   FieldValues[FLD_SESR+'_']),
                       db_Par(FLD_KNG,    FieldValues[FLD_KNG+'_']),
                       db_Par(FLD_LENGTH, FieldValues[FLD_LENGTH+'_'])
                      ]);



  db_OpenTableByID (qry_LinkLine, TBL_LINKLINE, aID);
  if qry_LinkLine.IsEmpty then
    Exit;

 { with qry_LinkLine do
  begin
    //��� �����
    iType   :=FieldByName(FLD_TYPE).AsInteger;

//    iStatus :=FieldByName(FLD_STATUS).AsInteger;
    length  :=FieldByName(FLD_LENGTH).AsFloat / 1000 ;

    sesr  :=FieldByName(FLD_SESR).AsFloat;
    kng   :=FieldByName(FLD_KNG).AsFloat;

    sesr_norm  :=FieldByName(FLD_SESR_NORM).AsFloat;
    kng_norm   :=FieldByName(FLD_KNG_NORM).AsFloat;
    length_norm:=FieldByName(FLD_LENGTH_NORM).AsFloat;

  end;
  }

  {
    '������������� ������� (�� 12.500)',
    '������������� ���� (�� 2500)',
    '������������� ���� (�� 600)',
    '������������� ���� (�� 200)',
    '������������� ���� (�� 50)',
    '������� ���� (�� 100)',
    '���� �������'
   }

  if (length > LINKLINE_NORM_ARR[iType].Length) or
     (length_norm=0) then
  begin
  //  dmLink.Update (aID, [db_Par(FLD)]);
    Exit;
  end;

  //��� �����
  case iType of
    0:     begin
             if length < 50 then
               length:=50;

             kng_req :=kng_norm * length / length_norm ;
             sesr_req:=0.05 + 0.02 * length / length_norm;
           end;

    1,2,3: begin
             if length < 50 then
               length:=50;

             kng_req :=kng_norm * length / length_norm ;
             sesr_req:=sesr_norm * length / length_norm;

           end;

    else begin
             kng_req:=kng_norm;
             sesr_req:=sesr_norm;
         end;
  end;


  iStatus:=IIF((kng_req < kng) and (sesr_req < sesr), 1, 2);



  dmLinkLine.Update (aID,
                     [db_Par(FLD_SESR_REQUIRED, TruncFloat(SESR_req,6)),
                      db_Par(FLD_KNG_REQUIRED,  TruncFloat(KNG_req,6)),
                      db_Par(FLD_STATUS,        iStatus)
                     ]);

 // CursorDefault();
end;


//--------------------------------------------------------------------
procedure TdmLinkNet_calc.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

   db_SetComponentADOConnection (Self, dmMain.ADOConnection);
  

  db_CreateField (mem_Links,
                [db_Field(FLD_ID,      ftInteger),
                 db_Field(FLD_NAME,    ftString),
                 db_Field(FLD_POWER,   ftFloat),
                 db_Field(FLD_SESR,    ftFloat),
                 db_Field(FLD_KNG,     ftFloat),
                 db_Field(FLD_LENGTH,  ftFloat),
                 db_Field(FLD_STATUS,  ftInteger)
                ]);
  db_CreateCalculatedField (mem_Links, FLD_STATUS_str, ftString);

  db_Clear(mem_Links);
end;


//--------------------------------------------------------------------
procedure TdmLinkNet_calc.DataModuleDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  db_ClearFields (mem_Links);
  inherited;
  
end;



begin 
end.