unit dm_act_LinkNet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics,
  Dialogs,  Menus, Controls, Forms, ActnList,

   i_Audit,


  dm_act_Base,

  u_ini_Link_report_params,

  dm_Main, 

  dm_LinkFreqPlan,


 // I_Act_Report, // dm_Act_Report;

  I_Object,

 // I_Act_LinkNet,
  //I_LinkFreqPlan,

 // I_Act_LinkFreqPlan, //dm_Act_LinkFreqPlan,

  u_Link_Report_Lib,

  u_classes,
  u_types,

  u_func,
  u_dlg,

  u_func_msg,
  u_const_msg,

  u_const_str,

 // u_LinkNet_const,

  fr_LinkNet_view,

  //I_Report,


  I_Shell,
  u_Shell_new,
//  dm_Act_Report,
  dm_Folder,
  dm_LinkNet,
  //dm_Link_Report,

  d_LinkNet_add

//  d_LinkFreqPlan_add
   // dm_Link_Net_Add

  ;

type
  TdmAct_LinkNet = class(TdmAct_Base) //, IAct_LinkNet_X)
    act_Edit: TAction;
    act_AddFreqPlan: TAction;
    act_Audit: TAction;
  //  procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

  private
//    function CheckActionsEnable: Boolean;
 //   procedure MakeReport(aID: integer);

  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm  (aOwnerForm: TForm): TForm; override;

    procedure DoAction (Sender: TObject);

    function ItemDel(aID: integer; aName: string = ''): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;

  public
    function Dlg_Add (aIDList: TIDList): integer;
    function Add_by_LinkList (aIDList: TIDList): integer;

    class procedure Init;
  end;


var
  dmAct_LinkNet: TdmAct_LinkNet;

//==================================================================
//==================================================================
implementation
{$R *.dfm}
uses
  dm_Act_LinkFreqPlan, dm_Act_Report;



//--------------------------------------------------------------------
class procedure TdmAct_LinkNet.Init;
//--------------------------------------------------------------------
begin
  Assert( not Assigned(dmAct_LinkNet));

  dmAct_LinkNet:=TdmAct_LinkNet.Create(Application);

//  dmAct_LinkNet.GetInterface(IAct_LinkNet_X, IAct_LinkNet);
//  Assert(Assigned(IAct_LinkNet));
end;

//
//procedure TdmAct_LinkNet.DataModuleDestroy(Sender: TObject);
//begin
////  IAct_LinkNet := nil;
////  dmAct_LinkNet:= nil;
//
//  inherited;
//end;
//


//--------------------------------------------------------------------
procedure TdmAct_LinkNet.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------

begin
  inherited;

  ObjectName:=OBJ_LINKNet;

  act_Add.Caption:='������� �� ����';

    act_Audit.Caption:=STR_Audit;


  SetActionsExecuteProc ([act_Edit,
                          act_AddFreqPlan
                      //    act_Report
                          ], DoAction);

  SetCheckedActionsArr([
        act_Add,
        act_Del_,

        act_Edit,
        act_AddFreqPlan,

        act_Audit
     ]);

end;


//--------------------------------------------------------------------
function TdmAct_LinkNet.Dlg_Add (aIDList: TIDList): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_LinkNet_add.Dlg_Add (0, aIDList);
end;


//--------------------------------------------------------------------
function TdmAct_LinkNet.Add_by_LinkList (aIDList: TIDList): integer;
//--------------------------------------------------------------------
var
  rec: TdmLinkNetAddRec;
  bNameOK : boolean;
  sFolderGUID: string;

begin
  rec.Name:= dmLinkNet.GetNewName();

  bNameOK:= False;
  while not bNameOK do
  begin
    if not InputQuery('�������� �� ����', '������� ��� ����', rec.Name) then
      Exit;

    if (dmLinkNet.FindByName(rec.Name)>0)  then
    begin
      ErrorDlg('��� ���� ��� ����������, ������� ������.');
      bNameOK:= False;
    end
    else
      bNameOK:= True;
  end;    // while

  rec.FolderID:=FFolderID;

  Result:= dmLinkNet.Add_by_LinkList (rec, aIDList);

  if Result>0 then begin
    if FFolderID=0 then
      sFolderGUID:=g_Obj.ItemByName[OBJ_LINKNET].RootFolderGUID
    else
      sFolderGUID:=dmFolder.GetGUIDByID(FFolderID);


    g_Shell.UpdateNodeChildren_ByGUID(sFolderGUID);

//    SH_PostUpdateNodeChildren (sFolderGUID);
  end;
end;


//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_LinkNet.DoAction(Sender: TObject);
//var
//  oListID: TIDList;
begin
//  oListID:=TIDList.Create;

 // if Sender=act_Object_ShowOnMap then ShowOnMap (FFocusedID);

  //-------------------------------------------------------------------
  if Sender=act_Edit then
  //-------------------------------------------------------------------
  begin
    if Tdlg_LinkNet_add.Dlg_Update (FFocusedID) then
    begin
      g_Shell.UpdateNodeChildren_ByGUID(dmLinkNet.GetGUIDByID(FFocusedID));

    //  sh_PostUpdateNodeChildren(dmLinkNet.GetGUIDByID(FFocusedID));

      g_EventManager.postEvent_ (WE_LINKNET_LINKS_REFRESH_, ['ID', FFocusedID]);

   //   PostEvent(WE_LINKNET_LINKS_REFRESH_, [app_Par(PAR_ID, FFocusedID)]);

   //   PostMessage(Application.Handle, WE_LINKNET_LINKS_REFRESH_, FFocusedID, 0);

   //   dmLinkFreqPlan.Update_LinkList(FFocusedID);

    end;
  end
  else


  //-------------------------------------------------------------------
  if Sender=act_AddFreqPlan then
  //-------------------------------------------------------------------
  begin
    if dmAct_LinkFreqPlan.Dlg_AddForLinkNet(FFocusedID, FFolderID)> 0 then
//    if Tdlg_LinkFreqPlan_add.ExecDlg(FFocusedID, FFolderID)  then
      g_Shell.UpdateNodeChildren_ByGUID (dmLinkNet.GetGUIDByID(FFocusedID));
  end else

  raise EClassNotFound.Create('');


//  if Sender=act_Report then
 //   MakeReport(FFocusedID) else

  //////////// PostMsg (aLinkLineID)
end;

//--------------------------------------------------------------------
function TdmAct_LinkNet.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_LinkNet_add.Dlg_Add (aFolderID);
end;

//--------------------------------------------------------------------
function TdmAct_LinkNet.ItemDel(aID: integer; aName: string = ''): boolean;
//--------------------------------------------------------------------
begin
  Result:=dmLinkNet.Del (aID)>0;

  Result:=True;
end;




//--------------------------------------------------------------------
function TdmAct_LinkNet.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_LinkNet_View.Create(aOwnerForm);
end;



//--------------------------------------------------------------------
procedure TdmAct_LinkNet.GetPopupMenu;
//--------------------------------------------------------------------
//var
//  bEnable: Boolean;
begin
//  bEnable:=CheckActionsEnable();


 // CheckActionsEnable();

  case aMenuType of
    mtFolder: begin
                AddFolderMenu_Create (aPopupMenu);
                AddFolderPopupMenu   (aPopupMenu);
              end;
    mtItem:   begin
                AddMenuItem (aPopupMenu, act_Edit);
                AddMenuItem (aPopupMenu, act_Del_);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_AddFreqPlan);

                AddMenuItem (aPopupMenu, act_Audit);

//                AddMenuItem (aPopupMenu, nil);
//                AddMenuItem (aPopupMenu, act_Report);
              end;
    mtList:
                AddMenuItem (aPopupMenu, act_Del_list);
  end;
end;





end.

        (*

//--------------------------------------------------------------------
procedure TdmAct_LinkNet.MakeReport(aID: integer);
//--------------------------------------------------------------------
var
  sXMLFileName: string;
  oLinkIDList, oLinkLineIDList: TIDList;
  sNetName: string;
//  rReportSelect: TdmReportSelectRec;

  oReportSelect: TIni_Link_Report_Param;

begin
   Exit;

(*
  oLinkIDList     := TIDList.Create;
  oLinkLineIDList := TIDList.Create;

  oReportSelect:=TIni_Link_Report_Param.Create;


 // if dmAct_Report.ExecDlg ('linknet', rReportSelect) then //, sRepName); //aType: string);
  if dmAct_Report.Dlg_Select('linknet', oReportSelect) then //, sRepName); //aType: string);
  begin
  //  rReportSelect.XMLFileName:=GetTempXmlFileName();
   // rReportSelect.XMLFileName:=sXMLFileName;


    dmLinkNet.GetLinks_for_Net     (aID, oLinkIDList);
    dmLinkNet.GetLinkLines_for_Net (aID, oLinkLineIDList);

  //  sNetName:= dmLinkNet.GetNameByID (aID);

//    u_Link_Report_lib.LinkReport_Execute_LinkList (rReportSelect.ReportID, SelectedIDList, rReportSelect.XMLFileName);
    u_Link_Report_lib.LinkReport_Execute_LinkList (oReportSelect, FSelectedIDList);

//    u_Link_Report_lib.LinkReport_Execute_Link(aID, rReportSelect.XMLFileName);

   // dmReport.MakeReport (rReportSelect);

  end;

  oReportSelect.Free;
*)

{
  sXMLFileName:=GetTempXmlFileName();
  //..sXMLFileName:='t:\wwwwww.xml';

//  dmLInkNet.GetAllLinks_for_Net(aID, oIDList);



  LinkReport_Execute_LinkNet (oLinkLineIDList, oLinkIDList,
                                sXMLFileName,    sNetName);

  dmAct_Report.ExecDlg (sXMLFileName, 'linknet');
}
 // oLinkIDList.Free;
 // oLinkLineIDList.Free;
end;



// ---------------------------------------------------------------
function TdmAct_LinkNet.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
 {
  Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];

  SetActionsEnabled1( [

 act_Export_MDB,
//                           act_Import_MDB,

  act_Copy,
  act_Change_Vendor,
  act_Change_Vendor_Equipment,

  act_Update_Field,

  act_Mode_copy_tx_to_RX,
  act_Del_,
  act_Del_list

      ],

   Result );
  }
   //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );


end;

