unit dm_LinkNet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Db, ADODB,  Variants,

  dm_Onega_DB_data,

  dm_Main,
  dm_Object_base,

  u_Geo,
  u_db,
  u_func,
  u_classes,

  u_types,

  u_const_str,
  u_const_db,

  dxmdaset
  ;



type
  //-------------------------------------------------------------------
  TdmLinkNetAddRec = record
  //-------------------------------------------------------------------
//    LinkNetID: integer;
    Name:     string;
    FolderID: integer;
  end;


  //-------------------------------------------------------------------
  TdmLinkNet = class(TdmObject_base)
    mem_Links: TdxMemData;
    qry_Temp: TADOQuery;
    qry_Temp2: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private

    procedure GetAllLinks_for_Net_to_Dataset1(aNetID: integer; var aMem: TdxMemData); overload;
    procedure RemoveItems(aID: integer; aIDList: TIDList);
  public
    function Add_by_LinkList(aRec: TdmLinkNetAddRec; aLinkIDList: TIDList): integer;

    function Add_new(aRec: TdmLinkNetAddRec; aIDList_link,aIDList_linkline:
        TIDList): integer;

    function Add_new_items(aNetNet_ID : Integer; aIDList_link, aIDList_linkline:
        TIDList): integer;
    function Del(aID: integer): integer;

    procedure GetLinks_for_Net    (aNetID: integer; var aMem: TdxMemData); overload;
    procedure GetAllLinks_for_Net (aNetID: integer; var aIDList: TIDList); overload;
    procedure GetLinks_for_Net    (aNetID: integer; var aIDList: TIDList); overload;

    procedure GetLinkLines_for_Net_to_Dataset(aNetID: integer; var aMem: TdxMemData); overload;
    procedure GetLinkLines_for_Net(aNetID: integer; var aIDList: TIDList); overload;


    function GetBLRect (aID: integer): TBLRect;

  end;


function dmLinkNet: TdmLinkNet;

//====================================================================
implementation {$R *.dfm}
//====================================================================

var
  FdmLinkNet: TdmLinkNet;


// ---------------------------------------------------------------
function dmLinkNet: TdmLinkNet;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLinkNet) then
    FdmLinkNet := TdmLinkNet.Create(Application);

   Result := FdmLinkNet;
end;

//--------------------------------------------------------------------
procedure TdmLinkNet.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  db_CreateField(mem_Links, [db_Field(FLD_ID,        ftInteger),
                            db_Field(FLD_NAME,       ftString),
                            db_Field(FLD_TYPE,       ftString),
                            db_Field(FLD_LENGTH,     ftFloat),
                            db_Field(FLD_KNG,        ftFloat),
                            db_Field(FLD_SESR,       ftFloat),
                            db_Field(FLD_rx_Level_dBm, ftFloat)
                           ]);

  TableName :=TBL_LINKNET;
  DisplayName:=STR_LINK_NET;
  ObjectName:=OBJ_LINKNet;

end;


//--------------------------------------------------------------------
procedure TdmLinkNet.GetLinks_for_Net(aNetID: integer;
                                       var aMem: TdxMemData);
//--------------------------------------------------------------------
const
  SQL_SELECT_LINKS_FOR_NET = 'SELECT * FROM ' + TBL_LINKNET_ITEMS_XREF +
                            ' WHERE (linknet_id=:linknet_id) and' +
                            ' (link_id is not null)';
var
  sName: string;
begin
  dmOnega_DB_data.OpenQuery(qry_Temp, SQL_SELECT_LINKS_FOR_NET,
              [FLD_LINKNET_ID, aNetID]);

  qry_Temp.First;
  with qry_Temp do
    while not Eof do
  begin
    sName:=  gl_DB.GetStringFieldValue(TBL_LINK, FLD_NAME,
                      [db_Par(FLD_ID, FieldValues[FLD_LINK_ID]) ]);

    db_AddRecord_(aMem, [FLD_ID,      FieldValues[FLD_LINK_ID],
                         FLD_NAME,    sName,
                         FLD_TYPE,    OBJ_LINK
                       ]);
    Next;
  end;

  //aMem.First;

end;


//--------------------------------------------------------------------
procedure TdmLinkNet.GetLinks_for_Net (aNetID: integer; var aIDList: TIDList);
//--------------------------------------------------------------------
//procedure TdmAct_LinkNet.MakeReport(aID: integer);


begin
  mem_Links.Close;
  mem_Links.Open;

  GetLinks_for_Net(aNetID, mem_Links);

  aIDList.LoadIDsFromDataSet(mem_Links, FLD_ID);

{
  mem_Temp.First;
  with mem_Temp do    while not EOF do
  begin
    Next;
  end;    // with
}
end;


//--------------------------------------------------------------------
procedure TdmLinkNet.GetLinkLines_for_Net_to_Dataset(aNetID: integer; var aMem:
    TdxMemData);
//--------------------------------------------------------------------
const
  SQL_SELECT_LINKLINE_FOR_NET = 'SELECT * FROM ' + TBL_LINKNet_Items_XREF +
                                ' WHERE (linknet_id=:linknet_id) and' +
                                ' (LinkLine_id is not null)';
var
  sName: string;
begin
  db_OpenQuery(qry_Temp, SQL_SELECT_LINKLINE_FOR_NET,
              [db_par(FLD_LINKNET_ID, aNetID)]);

  qry_Temp.First;
  with qry_Temp do
    while not Eof do
  begin
    sName:=  gl_DB.GetStringFieldValue(TBL_LINKLINE, FLD_NAME,
                    [db_Par(FLD_ID, FieldValues[FLD_LINKLINE_ID]) ]);

    db_AddRecord_(aMem,   [FLD_ID,      FieldValues[FLD_LINKLINE_ID],
                           FLD_NAME,    sName,
                           FLD_TYPE,    OBJ_LINKLINE
                          ]);
    Next;
  end;

  //aMem.First;

end;


//--------------------------------------------------------------------
procedure TdmLinkNet.GetLinkLines_for_Net (aNetID: integer; var aIDList: TIDList);
//--------------------------------------------------------------------
begin
  mem_Links.Close;
  mem_Links.Open;

  GetLinkLines_for_Net_to_Dataset(aNetID, mem_Links);

  aIDList.LoadIDsFromDataSet(mem_Links);

{  mem_Temp.First;
  with mem_Temp do    while not EOF do
  begin
    aIDList.AddID(mem_Temp[FLD_ID]);
    Next;
  end;    // with
}
end;



//--------------------------------------------------------------------
procedure TdmLinkNet.GetAllLinks_for_Net_to_Dataset1(aNetID: integer; var aMem: TdxMemData);
//--------------------------------------------------------------------

begin
  dmOnega_DB_data.LinkNet_links(ADOStoredProc1, aNetID);


  with ADOStoredProc1 do
    while not Eof do
  begin

    db_AddRecord_(aMem,  [FLD_ID,      FieldValues[FLD_ID],
                          FLD_NAME,    FieldValues[FLD_NAME],
                          FLD_LENGTH,  FieldValues[FLD_Length],
                          FLD_KNG,     FieldValues[FLD_KNG],
                          FLD_SESR,    FieldValues[FLD_Sesr],
                                                                 
    //                      db_Par(FLD_POWER,   FieldValues[FLD_Power])
                          FLD_rx_Level_dBm,   FieldValues[FLD_rx_Level_dBm]
                         ]);


    Next;
  end;


  aMem.First;

end;

//----------------------------------------------------------------------
procedure TdmLinkNet.GetAllLinks_for_Net(aNetID: integer; var aIDList: TIDList);
//----------------------------------------------------------------------
begin
  GetAllLinks_for_Net_to_Dataset1(aNetID, mem_Links);

  aIDList.LoadIDsFromDataSet(mem_Links, FLD_ID);

end;

//-------------------------------------------------------------------
function TdmLinkNet.GetBLRect (aID: integer): TBLRect;
//-------------------------------------------------------------------
var blVector: TblVector;
begin
  Result:=NULL_BLRECT();

  dmOnega_DB_data.OpenQuery (qry_Temp,
                'SELECT lat1,lon1,lat2,lon2 FROM '+ view_LinkLine_links + ' WHERE (linkline_id=:id)',
               [FLD_ID, aID]);

  with qry_Temp do
    while not EOF do
    begin
      blVector:=db_ExtractBLVector(qry_Temp);
      geo_RoundedBLRect (blVector, Result);

      Next;
    end;
end;


//-------------------------------------------------------
function TdmLinkNet.Add_new(aRec: TdmLinkNetAddRec; aIDList_link,
    aIDList_linkline: TIDList): integer;
//-------------------------------------------------------
//var i: Integer;
begin
  Result:=dmOnega_DB_data.LinkNet_add (dmMain.ProjectID,  aRec.Name);

{
 Result:=dmOnega_DB_data.ExecStoredProc_ ('sp_LinkNet_add',
                [FLD_PROJECT_ID, dmMain.ProjectID,
                 FLD_NAME,       aRec.Name
                 ]  );
}

//  Assert(Result>0, 'function TdmLinkNet.Add_new(');


  Add_new_items (Abs(Result), aIDList_link, aIDList_linkline);
end;




//-------------------------------------------------------
function TdmLinkNet.Del(aID: integer): integer;
//-------------------------------------------------------
begin
  Result:=dmOnega_DB_data.LinkNet_Del ( aID) ;
end;

//-------------------------------------------------------
function TdmLinkNet.Add_new_items(aNetNet_ID : Integer; aIDList_link,
    aIDList_linkline: TIDList): integer;
//-------------------------------------------------------
var i: Integer;
  k: Integer;
begin
  Assert(aNetNet_ID>0, 'Value <=0');

   dmOnega_DB_data.ExecStoredProc_ ('sp_LinkNet_items_clear',
                [FLD_ID, aNetNet_ID
               //  db_Par(FLD_Link_ID,    aIDList_link[i].ID)
                 ]  );

  for i:= 0 to aIDList_link.Count-1 do
     k:=dmOnega_DB_data.ExecStoredProc_ ('sp_LinkNet_add_item',
                  [FLD_ID, aNetNet_ID,
                   FLD_Link_ID,    aIDList_link[i].ID
                   ]  );

  for i:= 0 to aIDList_linkline.Count-1 do
     k:=dmOnega_DB_data.ExecStoredProc_ ('sp_LinkNet_add_item',
                  [FLD_ID,  aNetNet_ID,
                   FLD_LinkLine_ID, aIDList_linkline[i].ID
                   ]  );

end;


//-------------------------------------------------------
function TdmLinkNet.Add_by_LinkList(aRec: TdmLinkNetAddRec; aLinkIDList:
    TIDList): integer;
//-------------------------------------------------------
var i: integer;
begin
  dmOnega_DB_data.OpenQuery(qry_Temp,
      'SELECT * FROM '+TBL_LINKNET+
      ' WHERE project_id=:project_id',
      [FLD_PROJECT_ID, dmMain.ProjectID]);

  if qry_Temp.Locate(FLD_NAME, aRec.Name, [])
  then begin
    // ���� ����������� ����, �� ���������� � ������ ���������

    Result:= qry_Temp[FLD_ID];
//    gl_DB.GetIDByName(TBL_LINKNET, aRec.Name);

    gl_DB.ExecCommand('DELETE FROM '+TBL_LINKNET_ITEMS_XREF+
                      ' WHERE linknet_id=:linknet_id',
                     [db_Par(FLD_LINKNET_ID, Result)]);
  end
  else
    begin
       Result:=gl_DB.AddRecordID (TableName,

                        [db_Par(FLD_PROJECT_ID, dmMain.ProjectID),
                         db_Par(FLD_NAME,       aRec.Name)
                     //    db_Par(FLD_FOLDER_ID,  IIF_NULL(aRec.FolderID))
                         ]  );
      //   ;
    
      end;


   // Result:=inherited Add (aRec.Name, aRec.FolderID);

  if Assigned(aLinkIDList) then
    for i:= 0 to aLinkIDList.Count-1 do
      dmOnega_DB_data.LinkNet_Add_Item(Result, 0, aLinkIDList.Items[i].ID)

    (*  gl_DB.AddRecord(TBL_LINKNET_ITEMS_XREF,
                  [db_Par(FLD_LINKNET_ID, Result),
                   db_Par(FLD_LINK_ID,   aLinkIDList.Items[i].ID) ])
                   else*)

end;

//-------------------------------------------------------
procedure TdmLinkNet.RemoveItems(aID: integer; aIDList: TIDList);
//-------------------------------------------------------
var
  I: Integer;
begin
  for i:=0 to aIDList.Count-1 do
  begin
    if eq(aIDList[i].Name, OBJ_LINK) then
    begin
      gl_DB.DeleteRecords(TBL_LINKNET_ITEMS_XREF,
                          [db_Par(FLD_LINKNET_ID, aID),
                           db_Par(FLD_LINK_ID, aIDList[i].ID)
                          ]);
    end         else
    if eq(aIDList[i].Name, OBJ_LINKLINE) then
    begin
      gl_DB.DeleteRecords(TBL_LINKNET_ITEMS_XREF,
                          [db_Par(FLD_LINKNET_ID, aID),
                           db_Par(FLD_LINKLINE_ID, aIDList[i].ID)
                          ]);
    end
  end;

end;


begin

end.

