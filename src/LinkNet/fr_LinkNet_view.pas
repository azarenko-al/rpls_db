unit fr_LinkNet_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, Menus, cxPropertiesStore,
   DBCtrls,
  ExtCtrls,  ImgList,

  fr_View_base,

  fr_DBInspector_Container,
  fr_LinkNet_Links,

  dm_User_Security,


  u_func,
  u_reg,

  u_const_str,
  u_types, ComCtrls, dxBar, cxBarEditItem, cxClasses, cxLookAndFeels
  ;

type
  Tframe_LinkNet_View = class(Tframe_View_Base)
    PageControl1: TPageControl;
    TabSheet_Main: TTabSheet;
    TabSheet_Links: TTabSheet;
    procedure FormCreate(Sender: TObject); 
    procedure GSPages1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
  private
   // FID: integer;
    FActivePage: integer;
    FIsUpdated: boolean;

    Ffrm_Inspector: Tframe_DBInspector_Container;
    Ffrm_Links: Tframe_LinkNet_Links;

  public
    procedure View (aID: integer; aGUID: string); override;
  end;

//==================================================================
//==================================================================
implementation
 {$R *.dfm}

//--------------------------------------------------------------------
procedure Tframe_LinkNet_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  FISUpdated:=false;

  FActivePage:= -1;

  ObjectName:=OBJ_LINKNet;

  PageControl1.Align:=alClient;

  Caption:=STR_LINK_NET;

  CreateChildForm(Tframe_DBInspector_Container, Ffrm_Inspector, TabSheet_Main);
  Ffrm_Inspector.PrepareViewForObject (OBJ_LINKNET);

  CreateChildForm(Tframe_LinkNet_Links, Ffrm_Links, TabSheet_Links);


  AddComponentProp(PageControl1, PROP_ACTIVE_PAGE);
  cxPropertiesStore.RestoreFrom;


end;


//--------------------------------------------------------------------
procedure Tframe_LinkNet_View.View(aID: integer; aGUID: string);
//--------------------------------------------------------------------
//var
//  bReadOnly: Boolean;
var
  bReadOnly: Boolean;
begin
  inherited;

   bReadOnly:=dmUser_Security.Object_Edit_ReadOnly();


//  bReadOnly := dmUser_Security.ProjectIsReadOnly;

  Ffrm_Inspector.SetReadOnly (bReadOnly);
  Ffrm_Links.SetReadOnly (bReadOnly);


 // FId := aID;
  FActivePage:= -1;

  case PageControl1.ActivePageIndex of
    0: Ffrm_Inspector.View (ID);
    1: Ffrm_Links.View (ID);
  end;





  FISUpdated:=true;

  PageControl1Change (nil);

end;



//-------------------------------------------------------------------
procedure Tframe_LinkNet_View.GSPages1Click(Sender: TObject);
//-------------------------------------------------------------------
begin
(*  if not FIsUpdated = true then
    Exit;

  if FActivePage=GSPages1.ActivePageIndex then
    Exit;

  FActivePage:= GSPages1.ActivePageIndex;

  case GSPages1.ActivePageIndex of
    0:Ffrm_Inspector.View (ID);
    1:Ffrm_Links.View (ID);
  end;*)

end;


procedure Tframe_LinkNet_View.PageControl1Change(Sender: TObject);
begin
  if not FIsUpdated = true then
    Exit;

  if FActivePage=PageControl1.ActivePageIndex then
    Exit;

  FActivePage:= PageControl1.ActivePageIndex;

  case PageControl1.ActivePageIndex of
    0: Ffrm_Inspector.View (ID);
    1: Ffrm_Links.View (ID);
  end;
end;





end.