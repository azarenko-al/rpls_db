unit Filter_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 08.04.2019 12:02:29 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\RPLS_DB\.Parts\Filter\Project20 (1)
// LIBID: {17A75E47-684E-40A6-9D82-5B059FBFFD89}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (stdvcl40.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface


uses ComObj, ActiveX;



// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  FilterMajorVersion = 1;
  FilterMinorVersion = 0;

  LIBID_Filter: TGUID = '{17A75E47-684E-40A6-9D82-5B059FBFFD89}';

  IID_IFilterX: TGUID = '{D667E756-D22E-425A-877E-5EFF94FE059F}';
  CLASS_FilterX: TGUID = '{83F6D00A-A003-44AE-BDC1-FF8DE50FC85C}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IFilterX = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  FilterX = IFilterX;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  PInteger1 = ^Integer; {*}
  PWideString1 = ^WideString; {*}


// *********************************************************************//
// Interface: IFilterX
// Flags:     (256) OleAutomation
// GUID:      {D667E756-D22E-425A-877E-5EFF94FE059F}
// *********************************************************************//
  IFilterX = interface(IUnknown)
    ['{D667E756-D22E-425A-877E-5EFF94FE059F}']
    function Dlg(const aConnectionStr: WideString; const aObjName: WideString; var aID: Integer; 
                 var aName: WideString; var aSQL: WideString): HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoFilterX provides a Create and CreateRemote method to          
// create instances of the default interface IFilterX exposed by              
// the CoClass FilterX. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoFilterX = class
    class function Create: IFilterX;
    class function CreateRemote(const MachineName: string): IFilterX;
  end;

implementation

class function CoFilterX.Create: IFilterX;
begin
  Result := CreateComObject(CLASS_FilterX) as IFilterX;
end;

class function CoFilterX.CreateRemote(const MachineName: string): IFilterX;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_FilterX) as IFilterX;
end;

end.

