unit RPLS_search_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 22.08.2016 10:30:34 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\RPLS_DB_XE_PARTS\Search\Master\Project16 (1)
// LIBID: {5E77DBAA-F390-47A2-92C4-7790C259C745}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (stdvcl40.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface


  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  RPLS_searchMajorVersion = 1;
  RPLS_searchMinorVersion = 0;

  LIBID_RPLS_search: TGUID = '{5E77DBAA-F390-47A2-92C4-7790C259C745}';

  IID_ISearchX: TGUID = '{A9723BEF-B487-43C6-9408-C93DC82FBDBA}';
  CLASS_SearchX: TGUID = '{8E7540F4-37A6-4B23-96D9-CD9CAFAA0A81}';
  IID_ISearchEventsX: TGUID = '{646A7602-70BB-4906-9927-09FB625EDDB2}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ISearchX = interface;
  ISearchEventsX = interface;
  ISearchEventsXDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  SearchX = ISearchX;


// *********************************************************************//
// Interface: ISearchX
// Flags:     (256) OleAutomation
// GUID:      {A9723BEF-B487-43C6-9408-C93DC82FBDBA}
// *********************************************************************//
  ISearchX = interface(IUnknown)
    ['{A9723BEF-B487-43C6-9408-C93DC82FBDBA}']
    function Dlg(aProject_id: Integer; const aADOConnectionString: WideString; 
                 const aEvents: ISearchEventsX): HResult; stdcall;
    function Yandex(const aEvents: ISearchEventsX): HResult; stdcall;
    function KML(aProject_ID: Integer; const aADOConnectionString: WideString): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: ISearchEventsX
// Flags:     (320) Dual OleAutomation
// GUID:      {646A7602-70BB-4906-9927-09FB625EDDB2}
// *********************************************************************//
  ISearchEventsX = interface(IUnknown)
    ['{646A7602-70BB-4906-9927-09FB625EDDB2}']
    procedure Find(aLat: Double; aLon: Double); safecall;
  end;

// *********************************************************************//
// DispIntf:  ISearchEventsXDisp
// Flags:     (320) Dual OleAutomation
// GUID:      {646A7602-70BB-4906-9927-09FB625EDDB2}
// *********************************************************************//
  ISearchEventsXDisp = dispinterface
    ['{646A7602-70BB-4906-9927-09FB625EDDB2}']
    procedure Find(aLat: Double; aLon: Double); dispid 201;
  end;

// *********************************************************************//
// The Class CoSearchX provides a Create and CreateRemote method to          
// create instances of the default interface ISearchX exposed by              
// the CoClass SearchX. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSearchX = class
    class function Create: ISearchX;
    class function CreateRemote(const MachineName: string): ISearchX;
  end;

implementation

uses ComObj;

class function CoSearchX.Create: ISearchX;
begin
  Result := CreateComObject(CLASS_SearchX) as ISearchX;
end;

class function CoSearchX.CreateRemote(const MachineName: string): ISearchX;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SearchX) as ISearchX;
end;

end.

