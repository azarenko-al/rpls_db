unit i_Search_shared;

interface
uses
  Forms, Dialogs,

  RPLS_search_TLB,

//  WMS_tlb,

  u_COM;

type

  
{  ISearchEventsX = interface(IUnknown)
    procedure Find(aLat: Double; aLon: Double); safecall;
  end;


}


  TSearchFindEvents = procedure (aLat: Double; aLon: Double) of object;
             


  TSearchEventsX = class(TInterfacedObject, ISearchEventsX )    //    ITileEventsX
  private
    FOnFind: TSearchFindEvents;
  public
    destructor Destroy; override;
    procedure Find(aLat: Double; aLon: Double); safecall;

    property OnFind: TSearchFindEvents read FOnFind write FOnFind;
  end;



procedure WMS_Register;


implementation

uses SysUtils;


const
  DEF_FILE_NAME = 'W:\RPLS_DB_XE_PARTS\Search\Master\Win32\Debug\search.dll';


procedure WMS_Register;
var
  sFile: string;
begin
//  sFile:=IncludeTrailingBackslash(ExtractFileDir(Application.ExeName)) + 'WMS\Search.dll';
  sFile:=DEF_FILE_NAME;  

  Assert(FileExists(sFile));

  RegisterOCX (sFile);
  //RegisterOCX (DEF_FILE_NAME);


end;


destructor TSearchEventsX.Destroy;
begin
//  ShowMessage('destructor TSearchEventsX.Destroy;');

  inherited;
  // TODO -cMM: TSearchEventsX.Destroy default body inserted
end;

//--------------------------------
//TSearchEventsX
//--------------------------------

procedure TSearchEventsX.Find(aLat: Double; aLon: Double);
begin
  if Assigned(FOnFind) then
    FOnFind(aLat,aLon);
    
end;



end.
