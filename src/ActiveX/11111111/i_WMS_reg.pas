unit i_WMS_reg;

interface
uses
  Forms, SysUtils,

  WMS_TLB,
    
  u_COM;

  
type
  
  TOnTileLoadEvent         = procedure (aFileName: String; aZ: Integer) of object;
 // TOnTileLoad1Event         = procedure (aFileName: String;  aZ: Integer) of object;

  TOnTileLayerChangedEvent = procedure (aName: String) of object;

  
  // ---------------------------------------------------------------
  TTileManagerEvents = class(TInterfacedObject, ITileEventsX)
  // ---------------------------------------------------------------
  private
    FOnTileLoad_: TOnTileLoadEvent;
    FOnTileLayerChanged_: TOnTileLayerChangedEvent;
  public
    procedure OnTileLoad(const aFileName: WideString; aZ: Integer); safecall;
    procedure OnTileLayerChanged(const aName: WideString); safecall;

    property OnTileLoad_: TOnTileLoadEvent read FOnTileLoad_ write FOnTileLoad_;
    property OnTileLayerChanged_: TOnTileLayerChangedEvent read
        FOnTileLayerChanged_ write FOnTileLayerChanged_;
            
  end;      

  

procedure WMS_Register(aDir: string = '');


implementation

const
//  DEF_FILE_NAME = 'TileManager.dll' ;
//  DEF_FILE_NAME = 'W:\WMS\ActiveX\Win32\Debug\WMS.dll' ;  
  DEF_FILE_NAME = 'WMS_10_21.dll' ;
  

  //  function RegisterOCX(FileName: string): Boolean;

// ---------------------------------------------------------------
procedure WMS_Register(aDir: string = '');
// ---------------------------------------------------------------
var
  sFile: string;
begin
  if aDir<>'' then
    sFile := IncludeTrailingBackslash(Trim(aDir)) +  DEF_FILE_NAME
  else
  begin
    //same folder
    sFile := ExtractFilePath(Application.ExeName) +  DEF_FILE_NAME;
    if not FileExists(sFile) then
      //WMS folder
      sFile := ExtractFilePath(Application.ExeName) +'WMS\'+ DEF_FILE_NAME;
              
  end;

  Assert(FileExists(sFile), 'not FileExists : '+ sFile);
  
  
  RegisterOCX (sFile);
end;



procedure TTileManagerEvents.OnTileLayerChanged(const aName: WideString);
begin
  if Assigned(FOnTileLayerChanged_) then
    FOnTileLayerChanged_( aName);

end;

procedure TTileManagerEvents.OnTileLoad(const aFileName: WideString; aZ:
    Integer);
begin
  if Assigned(FOnTileLoad_) then
    FOnTileLoad_( aFileName, aZ);

end;

end.

