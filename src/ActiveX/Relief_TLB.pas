unit Relief_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 26.04.2019 21:55:47 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\RPLS_DB\.Parts\Relief\Project20 (1)
// LIBID: {16C5BE95-4BD4-4BDF-87C2-A4D679D1C0D7}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (stdvcl40.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

   uses ComObj, ActiveX;


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ReliefMajorVersion = 1;
  ReliefMinorVersion = 0;

  LIBID_Relief: TGUID = '{16C5BE95-4BD4-4BDF-87C2-A4D679D1C0D7}';

  IID_IReliefX: TGUID = '{24B2950F-163A-47F3-A03E-EF6BB68D80F3}';
  CLASS_ReliefX: TGUID = '{D69E53DD-45AF-43FD-9E16-061ABE2DFBB2}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IReliefX = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  ReliefX = IReliefX;


// *********************************************************************//
// Interface: IReliefX
// Flags:     (256) OleAutomation
// GUID:      {24B2950F-163A-47F3-A03E-EF6BB68D80F3}
// *********************************************************************//
  IReliefX = interface(IUnknown)
    ['{24B2950F-163A-47F3-A03E-EF6BB68D80F3}']
    function Init(const aConnectionStr: WideString; aProject_ID: Integer): HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoReliefX provides a Create and CreateRemote method to          
// create instances of the default interface IReliefX exposed by              
// the CoClass ReliefX. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoReliefX = class
    class function Create: IReliefX;
    class function CreateRemote(const MachineName: string): IReliefX;
  end;

implementation


class function CoReliefX.Create: IReliefX;
begin
  Result := CreateComObject(CLASS_ReliefX) as IReliefX;
end;

class function CoReliefX.CreateRemote(const MachineName: string): IReliefX;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_ReliefX) as IReliefX;
end;

end.

