unit snmp_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 25.03.2019 11:47:40 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\RPLS_DB\.Parts\SNMP\Project17 (1)
// LIBID: {8C7E482B-B49C-4C48-9B7A-751BD47ED717}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (stdvcl40.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses ComObj, ActiveX;


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  snmpMajorVersion = 1;
  snmpMinorVersion = 0;

  LIBID_snmp: TGUID = '{9C790F9F-435C-4570-BDFD-94C19C456CF4}';

  IID_ISnmpX_: TGUID = '{DA041EE4-9C0E-4FC4-9426-5D228F20878D}';
  CLASS_SnmpX_: TGUID = '{620F752F-F813-4042-8D20-AA04B0FFC9A6}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum Enum1
type
  Enum1 = TOleEnum;
const
  SNMP_ERROR_PROFILE = $00000000;
  SNMP_ERROR_DB_CONNECT = $00000001;
  SNMP_ERROR_LICENCE = $00000002;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ISnmpX_ = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  SnmpX_ = ISnmpX_;


// *********************************************************************//
// Interface: ISnmpX_
// Flags:     (256) OleAutomation
// GUID:      {DA041EE4-9C0E-4FC4-9426-5D228F20878D}
// *********************************************************************//
  ISnmpX_ = interface(IUnknown)
    ['{DA041EE4-9C0E-4FC4-9426-5D228F20878D}']
    function Send(aCode: Integer; const aMsg: WideString): HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoSnmpX_ provides a Create and CreateRemote method to          
// create instances of the default interface ISnmpX_ exposed by              
// the CoClass SnmpX_. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSnmpX_ = class
    class function Create: ISnmpX_;
    class function CreateRemote(const MachineName: string): ISnmpX_;
  end;

implementation


class function CoSnmpX_.Create: ISnmpX_;
begin
  Result := CreateComObject(CLASS_SnmpX_) as ISnmpX_;
end;

class function CoSnmpX_.CreateRemote(const MachineName: string): ISnmpX_;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SnmpX_) as ISnmpX_;
end;

end.

