program link_ACAD;

uses
  dm_Custom in '..\..\.common\dm_Custom.pas' {dmCustom: TDataModule},
  dm_Rel_Engine in '..\..\RelEngine\dm_Rel_Engine.pas' {dmRel_Engine: TDataModule},
  Forms,
  u_antenna_classes in '..\..\Common classes\u_antenna_classes.pas',
  u_link_classes in '..\..\Common classes\u_link_classes.pas',
  u_LinkEnd_classes in '..\..\Common classes\u_LinkEnd_classes.pas',
  dm_Link_calc in '..\src\dm_Link_calc.pas' {dmLink_calc: TDataModule},
  dm_Main_app in '..\src\dm_Main_app.pas' {dmMain_link_calc: TDataModule},
  u_link_calc_classes in '..\src\u_link_calc_classes.pas',
  u_LinkCalcParams in '..\src\u_LinkCalcParams.pas',
  u_XML_bind_rrl_result in '..\src\u_XML_bind_rrl_result.pas',
  xml_rrl in '..\src\xml_rrl.pas',
  u_rrl_param_rec in '..\src_shared\u_rrl_param_rec.pas';

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(TdmMain_link_calc, dmMain_link_calc);
  Application.Run;
end.
