program link_ACAD;

{%File '..\src\ver.inc'}

uses
  Forms,
  dm_Main_app_acad in '..\src\dm_Main_app_acad.pas' {dmMain_link_ACAD: TDataModule},
  dm_Link_ACAD in '..\src\dm_Link_ACAD.pas' {dmLink_ACAD: TDataModule},
  u_ACAD_classes in '..\src\u_ACAD_classes.pas',
  u_acad_params in '..\src\u_acad_params.pas',
  u_Acad in '..\src\u_Acad.pas',
  u_acad_config in '..\src\u_acad_config.pas',
  u_ini_Link_ACAD in '..\src_shared\u_ini_Link_ACAD.pas';

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(TdmMain_link_ACAD, dmMain_link_ACAD);
  Application.Run;
end.
