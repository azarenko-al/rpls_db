unit dm_Main_app;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, IniFiles,

  u_files,
  u_log,

  u_vars,

  dm_Link_ACAD,

  dm_Main

  ;

type
  TdmMain_link_ACAD = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);

  end;

var
  dmMain_link_ACAD: TdmMain_link_ACAD;

//==============================================================
implementation {$R *.DFM}



//--------------------------------------------------------------
procedure TdmMain_link_ACAD.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const
  DEF_SOFTWARE_NAME= 'RPLS_DB_LINK';
  DEF_TEMP_FILENAME = 'link_acad.ini';

var
  oIniFile: TIniFile;

var
  iLinkID: Integer;
  iProjectID: Integer;

  sIniFileName: string;


begin
  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
//    sIniFileName:=TEMP_INI_FILE;
    sIniFileName:=g_ApplicationDataDir+ DEF_TEMP_FILENAME;

   // sIniFileName:=GetCommonApplicationDataDir(DEF_SOFTWARE_NAME)+
      //            DEF_TEMP_FILENAME;


  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;

 // g_Log:=TLog.Create();
//    IncludeTrailingBackslash(GetTempFileDir())+'_rpls_log.txt');


  oIniFile := TIniFile.Create(sIniFileName);

  with oIniFile do
  begin
    iProjectID :=ReadInteger ('main', 'ProjectID', 0);
    iLinkID    :=ReadInteger ('main', 'LinkID',    0);
  end;


  FreeAndNil(oIniFile);



 // if not oLinkCalcParams.Validate then
   // Exit;

(*
  if (iLinkID=0) or
     (iProjectID=0)
  then begin
    ShowMessage('(iLinkID=0) or (iProjectID=0) ');
    Exit;
  end;*)


  TdmMain.Init;
  if not dmMain.OpenDB_reg then
    Exit;


////////////
 // TdmRel_Engine.Init;

  dmMain.ProjectID:=iProjectID;

  TdmLink_ACAD.Init;

 // dmLink_ACAD.Params.LinkID:=iLinkID;
//  dmLink_Calc.Params.IsUsePassiveElements:=oLinkCalcParams.UsePassiveElements;
//  dmLink_Calc.Params.IsCalcWithAdditionalRain:=oLinkCalcParams.IsCalcWithAdditionalRain;
//  dmLink_Calc.Params.IsSaveReportToDB:=oLinkCalcParams.SaveReportToDB;

 // dmLink_ACAD.Execute (iLinkID);

  FreeAndNil(dmLink_ACAD);


 // ShowMessage('export to ACAD');
end;

end.
