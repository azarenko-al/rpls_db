unit dm_Link_ACAD;

interface
  {$I ver.inc}


uses
  SysUtils, Forms, Classes,

  u_ACAD,

  u_const_db,

  u_files,

  u_func,

  u_db,

  I_rel_Matrix1,

  u_Link_const,

  u_acad_params,

  dm_Onega_DB_data,

  ADODB, DB , Dialogs, rxPlacemnt
  ;

type
  TdmLink_ACAD = class(TDataModule)
    qry_LinkEnd1: TADOQuery;
    qry_Antennas1: TADOQuery;
    qry_LinkEnd2: TADOQuery;
    qry_Antennas2: TADOQuery;
    ADOStoredProc_Link: TADOStoredProc;
    qry_Property1: TADOQuery;
    qry_Property2: TADOQuery;
    SaveDialog1: TSaveDialog;
    procedure DataModuleCreate(Sender: TObject);
  //  procedure DataModuleCreate(Sender: TObject);
  private
    function OpenData(aID: integer): boolean;

  public
    Params: record
      Debug_FileName : string;
    end;


//    Params: record
//              LinkID : Integer;
//            //  IniFileName: string;
//            end;


//    function OpenData(aID: integer): Boolean;
    procedure Execute(aID: Integer);

    class procedure Init;


  end;

var
  dmLink_ACAD: TdmLink_ACAD;

implementation 
uses
  u_rel_Profile;

{$R *.dfm}
       

class procedure TdmLink_ACAD.Init;
begin
  if not Assigned(dmLink_ACAD) then
    dmLink_ACAD := TdmLink_ACAD.Create(Application);

end;


procedure TdmLink_ACAD.DataModuleCreate(Sender: TObject);
begin

  SaveDialog1.DefaultExt:='*.dxf';
  SaveDialog1.Filter    :='Autocad (*.dxf)|*.dxf';

end;

//--------------------------------------------------------------------
function TdmLink_ACAD.OpenData(aID: integer): boolean;
//--------------------------------------------------------------------
const
  SQL_SELECT_LINKEND_ANT = //band,
     'SELECT * '+
//     'SELECT id,lat,lon,polarization,diameter,gain,vert_width,loss,height,fronttobackratio,is_master '+
     '  FROM '+ view_LINKEND_ANTENNAS +
     ' WHERE linkend_id=:linkend_id '+
     ' ORDER BY is_master desc, height desc';
//     ' WHERE linkend_id=:linkend_id ORDER BY height desc';


  SQL_SELECT_PROPERTY=
     'SELECT * FROM ' + TBL_PROPERTY + ' WHERE id=:id ';


  SQL_SELECT_LINKEND=
     'SELECT * FROM ' + TBL_LinkEnd + ' WHERE id=:id ';

  SQL_SELECT_PMP_SECTOR=
      'SELECT * FROM ' + TBL_PMP_Sector + ' WHERE id=:id ';

  SQL_SELECT_PMP_TERMINAL=
     'SELECT *  FROM ' + TBL_PmpTerminal + ' WHERE id=:id ';

(*
  // for AM
  SQL_SELECT_LINKENDType_MODES =
     'SELECT * FROM ' + TBL_LinkEndType_Mode +
     ' WHERE LINKENDType_id=:id '+
     ' ORDER by bitrate_Mbps';
*)
                  
var
  oACAD_Params: TIni_Acad_params;


  iPmpSectorID,iPmpTerminalID,
//  iLinkEndID1,iLinkEndID2: integer;
  dFreq,f1,f2: double;
  iLinkEnd1_ID: Integer;
  iLinkEnd2_ID: Integer;
  iPropertyID1: Integer;
  iPropertyID2: Integer;
begin
  Assert(aID>0);


//  FLinkID:=aID;


{  dFreq:=dmLink.GetFreq (aID);
  if dFreq=0 then
}

  dmOnega_DB_data.Link_Select(ADOStoredProc_Link, aID);
  Assert(not ADOStoredProc_Link.IsEmpty);

  //db_View(ADOStoredProc_Link);


  Result := ADOStoredProc_Link.RecordCount>0;


  with ADOStoredProc_Link do
  begin
    iLinkEnd1_ID :=FieldByName(FLD_LinkEnd1_ID).AsInteger;
    iLinkEnd2_ID :=FieldByName(FLD_LinkEnd2_ID).AsInteger;

    iPropertyID1 :=FieldByName(FLD_Property1_ID).AsInteger;
    iPropertyID2 :=FieldByName(FLD_Property2_ID).AsInteger;
  end;

  dmOnega_DB_data.OpenQuery(qry_LinkEnd1, SQL_SELECT_LINKEND, [db_Par(FLD_ID, iLinkEnd1_ID)]);
  dmOnega_DB_data.OpenQuery(qry_LinkEnd2, SQL_SELECT_LINKEND, [db_Par(FLD_ID, iLinkEnd2_ID)]);


 // db_View(qry_LinkEnd1);
 // db_View(qry_LinkEnd2);


  dmOnega_DB_data.OpenQuery(qry_Property1, SQL_SELECT_Property, [db_Par(FLD_ID, iPropertyID1)]);
  dmOnega_DB_data.OpenQuery(qry_Property2, SQL_SELECT_Property, [db_Par(FLD_ID, iPropertyID2)]);


  dmOnega_DB_data.OpenQuery(qry_Antennas1, SQL_SELECT_LINKEND_ANT,
                [db_Par(FLD_LINKEND_ID, iLinkEnd1_ID)]);

  //db_View(qry_Antennas1);

  dmOnega_DB_data.OpenQuery(qry_Antennas2, SQL_SELECT_LINKEND_ANT,
                [db_Par(FLD_LINKEND_ID, iLinkEnd2_ID)]);

 // db_View(qry_Antennas2);


  assert(ADOStoredProc_Link.RecordCount>0);
  assert(qry_LinkEnd1.RecordCount>0);
  assert(qry_LinkEnd2.RecordCount>0);

  assert(qry_Property1.RecordCount>0);
  assert(qry_Property2.RecordCount>0);

  assert(qry_Antennas1.RecordCount>0);
  assert(qry_Antennas2.RecordCount>0);



  Result := (ADOStoredProc_Link.RecordCount>0) and
            (qry_LinkEnd1.RecordCount>0) and
            (qry_LinkEnd2.RecordCount>0) and

            (qry_Property1.RecordCount>0) and
            (qry_Property2.RecordCount>0) and

            (qry_Antennas1.RecordCount>0) and
            (qry_Antennas2.RecordCount>0);




//  Result:= Link.Validate();


//  if not Result then
  //  Exit;

end;


// ---------------------------------------------------------------
procedure TdmLink_ACAD.Execute(aID: Integer);
// ---------------------------------------------------------------
var
  I: Integer;
  oAcad: TAcad;
  oProfile: TrelProfile;
//  oACAD_Params: TIni_Acad_params;
  s: string;
  sClutter: string;
  sDXFFileName: string;


//  assert(Result);


 // sIniFileName: string;
  sProfile_XML: string;

  rClutterType: TClutterType;

begin
  {$IFDEF test}
    sDXFFileName:='s:\1.dxf';

  {$ELSE}
  if not SaveDialog1.Execute then
    Exit;

  sDXFFileName:= ChangeFileExt(SaveDialog1.FileName, '.dxf');

  {$ENDIF}



  if not OpenData(aID) then
    Exit;



//  sIniFileName:= ChangeFileExt(SaveDialog1.FileName, '.ini');
//  sDXFFileName:=SaveDialog1.FileName;


  // -------------------------

  oAcad:=TAcad.Create;


//  oACAD_Params:=TIni_Acad_params.Create;

  oProfile:=TrelProfile.Create;


  oACAD.Params.Freq_MHz := qry_LinkEnd1.FieldByName(FLD_TX_FREQ_MHz).AsFloat;



  // -------------------------
  with ADOStoredProc_Link do
  begin
    sProfile_XML := FieldByName(FLD_Profile_XML).AsString;

   // Assert(sProfile_XML<>'');

    oProfile.LoadFromXml(sProfile_XML,0);

    oACAD.Params.Length_km1 :=FieldByName(FLD_Length_km).AsFloat;

    oACAD.Params.Site1.Azimuth := FieldByName(FLD_Azimuth1).AsFloat;
    oACAD.Params.Site2.Azimuth := FieldByName(FLD_Azimuth2).AsFloat;

    oACAD.Params.Site1.Address     := qry_Property1.FieldByName(FLD_Address).AsString;
    oACAD.Params.Site1.GroundHeight:= qry_Property1.FieldByName(FLD_Ground_Height).AsFloat;

    oACAD.Params.Site2.GroundHeight:= qry_Property2.FieldByName(FLD_Ground_Height).AsFloat;
    oACAD.Params.Site2.Address     := qry_Property2.FieldByName(FLD_Address).AsString;

  end;

  // -------------------------
  with qry_Antennas1 do
  begin
    oACAD.Params.Site1.AntennaHeight := FieldByName(FLD_Height).AsInteger;

    Next;
    if not EOF then
      oACAD.Params.Site1.Antenna2Height := FieldByName(FLD_Height).AsInteger;
  end;

  // -------------------------
  with qry_Antennas2 do
  begin
    oACAD.Params.Site2.AntennaHeight := FieldByName(FLD_Height).AsInteger;

    Next;
    if not EOF then
      oACAD.Params.Site2.Antenna2Height := FieldByName(FLD_Height).AsInteger;
  end;


//  Assert(oProfile.Count>0, 'Value <=0');

//  FRe
  SetLength(oACAD.Params.Lines, oProfile.Count);

  for i:=0 to oProfile.Count-1 do
  begin

    oACAD.Params.Lines[i].Height_m    := TruncFloat(oProfile.Items[i].Rel_H);
    oACAD.Params.Lines[i].Distance_km := TruncFloat(oProfile.Items[i].Distance_KM);
    oACAD.Params.Lines[i].Clu_Height_m:= TruncFloat(oProfile.Items[i].Clutter_H);


   // rClutterType
//    VirtualBox:=TClutterType



    case oProfile.Items[i].Clutter_Code of    //
      DEF_CLU_OPEN_AREA : rClutterType:= ctOpen;     //0
      DEF_CLU_FOREST    : rClutterType:= ctforest;   //1

      DEF_CLU_BOLOTO,
      DEF_CLU_BOLOTO_NEPROHODIMOE,
      DEF_CLU_WATER     : rClutterType:= ctwater;    //2

      DEF_CLU_ROAD      : rClutterType:= ctOpen;
      DEF_CLU_RailROAD  : rClutterType:= ctOpen;

      DEF_CLU_CITY,
      DEF_CLU_ONE_BUILD,
      DEF_CLU_ONE_HOUSE: rClutterType:= ctBuilding; //7,73



      DEF_CLU_COUNTRY  : rClutterType:= ctCountry;  //3,31

(*      LO_TOWN,
      LO_SUBURB_HOUSE: oACAD_Params.Lines[i].Clutter:= 'country';  //3,31

      LO_CITY,
      LO_HOUSE       : oACAD_Params.Lines[i].Clutter:= 'building'; //7,73
*)

    else
      Assert(oProfile.Items[i].Clutter_Code=0,  IntToStr(oProfile.Items[i].Clutter_Code));

      rClutterType:= ctOpen;  // case
    end;

//
//    case oProfile.Items[i].Clutter_Code of    //
//      DEF_CLU_OPEN_AREA : sClutter:= 'open';     //0
//      DEF_CLU_FOREST    : sClutter:= 'forest';   //1
//      DEF_CLU_WATER     : sClutter:= 'water';    //2
//
//
//      DEF_CLU_ONE_BUILD,
//      DEF_CLU_ONE_HOUSE: sClutter:= 'building'; //7,73
//
//
//      DEF_CLU_COUNTRY  : sClutter:= 'country';  //3,31
//
//(*      LO_TOWN,
//      LO_SUBURB_HOUSE: oACAD_Params.Lines[i].Clutter:= 'country';  //3,31
//
//      LO_CITY,
//      LO_HOUSE       : oACAD_Params.Lines[i].Clutter:= 'building'; //7,73
//*)
//
//    else
//      sClutter:= 'open';  // case
//    end;


    oACAD.Params.Lines[i].Clutter:=rClutterType;
   // oACAD.Params.Lines[i].ClutterType:=rClutterType;

  end;



//  oACAD_Params.SavetoIniFile(sIniFileName);

//  FreeAndNil(oACAD_Params);

  FreeAndNil(oProfile);


  // --------------------------------------------------------

(*  sINIFileName:= ExtractFileDir(sDXFFileName) + '\'+
                  ExtractFileNameNoExt(sDXFFileName) + '.ini';
  ACAD_Params.SavetoIniFile(sINIFileName);

  FreeAndNil(ACAD_Params);

*)

//  oAcad.Params.LoadFromIniFile(sIniFileName);
  oAcad.SaveLineToFile(sDXFFileName);

  FreeAndNil(oAcad);
   {

 // s:=GetApplicationDir + 'ACAD\ACAD.exe';

  assert( FileExists(GetApplicationDir + 'ACAD\ACAD.exe'));

//  RunApp (GetApplicationDir + 'ACAD\ACAD.exe', sDXFFileName);
  RunApp (GetApplicationDir + 'ACAD\ACAD.exe', sIniFileName);

//  DeleteFile(sINIFileName);
  ////////////////////////////////////////////////////////////////////////////
   }
//  CursorDefault;


end;


end.

