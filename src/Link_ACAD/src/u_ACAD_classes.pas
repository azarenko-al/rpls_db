unit u_ACAD_classes;

interface

uses
  SysUtils,

  classes;



type
  // ---------------------------------------------------------------
  TAcad_Vertex = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
    X,Y: Double;
  end;


  // ---------------------------------------------------------------
  TAcad_Vertex_List = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TAcad_Vertex;
  public
    constructor Create;
    function AddItem(aX, aY: double): TAcad_Vertex;

    property Items[Index: Integer]: TAcad_Vertex read GetItems; default;
  end;



  // ---------------------------------------------------------------
  TAcad_Polyline = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
    Vertexes: TAcad_Vertex_List;
    Color : Integer;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
  end;


  // ---------------------------------------------------------------
  TAcad_POLYLINE_List = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TAcad_POLYLINE;
  public
    constructor Create;

    function AddItem: TAcad_POLYLINE;
    function AddColorItem(aColor: Integer): TAcad_POLYLINE;

    property Items[Index: Integer]: TAcad_POLYLINE read GetItems; default;
  end;

  // ---------------------------------------------------------------
  TAcad_Text = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
     X, Y: double;
     Text: string;
     Angle: double;
     Font_size: double;
  end;


  // ---------------------------------------------------------------
  TAcad_Text_List = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TAcad_Text;
  public
    constructor Create;

    function AddItem(aX, aY: double; aText: string; aFont_size, aAngle: double):
        TAcad_Text;

    property Items[Index: Integer]: TAcad_Text read GetItems; default;
  end;



  // ---------------------------------------------------------------
  TAcad_Line = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
    X1,Y1, X2,Y2: double;

    Color : Integer;
  end;


  // ---------------------------------------------------------------
  TAcad_Line_List = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TAcad_Line;
  public
    constructor Create;
    function AddColorItem(aColor: Integer; aX1,aY1, aX2,aY2: double): TAcad_Line;
    function AddItem(aX1,aY1, aX2,aY2: double): TAcad_Line;

    property Items[Index: Integer]: TAcad_Line read GetItems; default;
  end;


  // ---------------------------------------------------------------
  TAcad_Project = class
  // ---------------------------------------------------------------
  private
  public
    Lines: TAcad_Line_List;
    Texts: TAcad_Text_List;
    PolyLines: TAcad_POLYLINE_List;


    constructor Create;
    destructor Destroy; override;
    procedure SaveToAcad(aTemplate_filename, aFileName: string);


  end;


implementation

const
//  Groundtext= 'H���.';
//  Antennatext='H �.�.';

  LF=#13+#10;

  DEF_END=
    'ENDSEC'     + LF+
    '  0'        + LF+
    'EOF'        ;
  DEF_POLYLINE=
    'POLYLINE'   + LF+
    '  8'        + LF+
    '0'          + LF+
    '  6'        + LF+
    'CONTINUOUS' + LF+
    ' 62'        + LF+
    '     7'     + LF+
    ' 66'        + LF+
    '     1'     + LF+
    '  0'        ;
  DEF_COLORPOLYLINE=
    'POLYLINE'   + LF+
    '  8'        + LF+
    '0'          + LF+
    '  6'        + LF+
    'CONTINUOUS' + LF+
    ' 62'        + LF+
    '     %d'    + LF+
    ' 66'        + LF+
    '     1'     + LF+
    '  0'        ;
  DEF_POLYLINE_END=
    'SEQEND'     + LF+
    '  8'        + LF+
    '0'          + LF+
    ' 62'        + LF+
    '     7'     + LF+
    '  0'        ;
  DEF_VERTEX=
    'VERTEX'     + LF+
    '  8'        + LF+
    '0'          + LF+
    ' 10'        + LF+
    '%8.6f'      + LF+
    ' 20'        + LF+
    '%8.6f'      + LF+
    ' 30'        + LF+
    '0.0'        + LF+
    '  0'        ;
  DEF_TEXT=
    'TEXT'       + LF+
    '  8'        + LF+
    '0'          + LF+
    ' 62'        + LF+
    '     7'     + LF+
    ' 10'        + LF+
    '%8.6f'      + LF+
    ' 20'        + LF+
    '%8.6f'      + LF+
    ' 30'        + LF+
    '0.0'        + LF+
    '  1'        + LF+
    '%s'         + LF+
    ' 40'        + LF+
    '%8.6f'      + LF+
    ' 50'        + LF+
    '%8.6f'      + LF+
    ' 41'        + LF+
    '0.545455'   + LF+
    ' 51'        + LF+
    '15.0'        + LF+
    '  0'        ;
  DEF_LINE=
    'LINE'       + LF+
    '  5'        + LF+
    '26A90'      + LF+
    '  8'        + LF+
    '0'          + LF+
    ' 62'        + LF+
    '     0'     + LF+
    ' 10'        + LF+
    '%8.6f'      + LF+
    ' 20'        + LF+
    '%8.6f'      + LF+
    ' 30'        + LF+
    '0.0'        + LF+
    ' 11'        + LF+
    '%8.6f'      + LF+
    ' 21'        + LF+
    '%8.6f'      + LF+
    ' 31'        + LF+
    '0.0'        + LF+
    '  0'        ;
  DEF_COLORLINE=
    'LINE'       + LF+
    '  5'        + LF+
    '26A90'      + LF+
    '  8'        + LF+
    '0'          + LF+
    ' 62'        + LF+
    '     %d'    + LF+
    ' 10'        + LF+
    '%8.6f'      + LF+
    ' 20'        + LF+
    '%8.6f'      + LF+
    ' 30'        + LF+
    '0.0'        + LF+
    ' 11'        + LF+
    '%8.6f'      + LF+
    ' 21'        + LF+
    '%8.6f'      + LF+
    ' 31'        + LF+
    '0.0'        + LF+
    '  0'        ;



constructor TAcad_POLYLINE_List.Create;
begin
  inherited Create(TAcad_POLYLINE);
end;

function TAcad_POLYLINE_List.AddColorItem(aColor: Integer): TAcad_POLYLINE;
begin
  Result := AddItem();
  Result.Color :=aColor;
end;

function TAcad_POLYLINE_List.AddItem: TAcad_POLYLINE;
begin
  Result := TAcad_POLYLINE (inherited Add);
end;

function TAcad_POLYLINE_List.GetItems(Index: Integer):
    TAcad_POLYLINE;
begin
  Result := TAcad_POLYLINE(inherited Items[Index]);
end;

constructor TAcad_Text_List.Create;
begin
  inherited Create(TAcad_Text);
end;

function TAcad_Text_List.AddItem(aX, aY: double; aText: string; aFont_size,
    aAngle: double): TAcad_Text;
begin
  Result := TAcad_Text (inherited Add);

  Result.X:=aX;
  Result.Y:=aY;
  Result.Text:=aText;
  Result.Font_size:=aFont_size;
  Result.Angle:=aAngle;

end;

function TAcad_Text_List.GetItems(Index: Integer): TAcad_Text;
begin
  Result := TAcad_Text(inherited Items[Index]);
end;

constructor TAcad_Line_List.Create;
begin
  inherited Create(TAcad_Line);
end;

function TAcad_Line_List.AddItem(aX1,aY1, aX2,aY2: double): TAcad_Line;
begin
  Result := TAcad_Line (inherited Add);

  Result.X1:=aX1;
  Result.Y1:=aY1;
  Result.X2:=aX2;
  Result.Y2:=aY2;

end;



function TAcad_Line_List.AddColorItem(aColor: Integer; aX1,aY1, aX2,aY2:
    double): TAcad_Line;
begin
  Result := AddItem (aX1,aY1, aX2,aY2);
  Result.Color:=aColor;
end;



function TAcad_Line_List.GetItems(Index: Integer): TAcad_Line;
begin
  Result := TAcad_Line(inherited Items[Index]);
end;

constructor TAcad_Project.Create;
begin
  inherited Create;
  Lines := TAcad_Line_List.Create();
  Texts := TAcad_Text_List.Create();
  PolyLines := TAcad_POLYLINE_List.Create();
end;

destructor TAcad_Project.Destroy;
begin
  FreeAndNil(PolyLines);
  FreeAndNil(Texts);
  FreeAndNil(Lines);
  inherited Destroy;
end;


constructor TAcad_Vertex_List.Create;
begin
  inherited Create(TAcad_Vertex);
end;

function TAcad_Vertex_List.AddItem(aX, aY: double): TAcad_Vertex;
begin
  Result := TAcad_Vertex (inherited Add);
  Result.X:=aX;
  Result.Y:=aY;
end;

function TAcad_Vertex_List.GetItems(Index: Integer): TAcad_Vertex;
begin
  Result := TAcad_Vertex(inherited Items[Index]);
end;

constructor TAcad_POLYLINE.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  Vertexes := TAcad_Vertex_List.Create();
end;

destructor TAcad_POLYLINE.Destroy;
begin
  FreeAndNil(Vertexes);
  inherited Destroy;
end;


procedure TAcad_Project.SaveToAcad(aTemplate_filename, aFileName: string);
var
  I: Integer;
  j: Integer;
  oList: TStringList;

  oVertex: TAcad_Vertex;

  oText: TAcad_Text;
  oLine: TAcad_Line;
  oPolyline: TAcad_Polyline;
begin
  oList:=TStringList.Create;

  Assert(FileExists(aTemplate_filename));

  oList.LoadFromFile(aTemplate_filename);


  for I := 0 to Lines.Count - 1 do
  begin
    oLine:=Lines[i];

    if oLine.Color=0 then
      oList.Add(format(DEF_LINE,[oLine.X1, oLine.X1, oLine.X2, oLine.Y2]))
    else
      oList.Add(format(DEF_COLORLINE,[oLine.Color, oLine.X1, oLine.X1, oLine.X2, oLine.Y2]))

  end;


  for I := 0 to PolyLines.Count - 1 do
  begin
    oPolyline:=PolyLines[i];

    oList.Add(DEF_POLYLINE);

    for j := 0 to oPolyline.Vertexes.Count - 1 do
    begin
      oVertex:=oPolyline.Vertexes[j];

      oList.Add(format(DEF_VERTEX,[ oVertex.X, oVertex.Y]));
    end;

//     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX),    Graph.BoundsXY.MinY+(OtnY+8));
//     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX-8), Graph.BoundsXY.MinY+(OtnY+8));
//     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX-8), Graph.BoundsXY.MinY+(OtnY));
//     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX-4) , Graph.BoundsXY.MinY+(OtnY));
//     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(OtnX-12), Graph.BoundsXY.MinY+(OtnY));
//
//     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX),    Graph.BoundsXY.MinY+(OtnY+8)]));
//     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX-8), Graph.BoundsXY.MinY+(OtnY+8)]));
//     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX-8), Graph.BoundsXY.MinY+(OtnY)]));
//     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX-4) , Graph.BoundsXY.MinY+(OtnY)]));
//     oList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(OtnX-12), Graph.BoundsXY.MinY+(OtnY)]));
    oList.Add(DEF_POLYLINE_END);

  end;


  for I := 0 to Texts.Count - 1 do
  begin
    oText:=Texts[i];

    oList.Add(format(DEF_TEXT,[
          oText.X,
          oText.Y,
          oText.Text,
          oText.Font_size,
          oText.Angle]));

//          Graph.BoundsXY.MinX+ Dist,-984.0, FloattoStr(Lines[i].Clu_Height_m),font_size,90.0]));
  end;

  oList.SaveToFile(aFileName);


  FreeAndNil(oList);


end;



end.
