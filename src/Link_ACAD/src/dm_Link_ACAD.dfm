object dmLink_ACAD: TdmLink_ACAD
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 581
  Top = 300
  Height = 311
  Width = 578
  object qry_LinkEnd1: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT    *'
      'FROM      Property'
      'ORDER BY name')
    Left = 44
    Top = 92
  end
  object qry_Antennas1: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT     dbo.LinkEndAntenna.*, dbo.AntennaType.*'
      'FROM         dbo.AntennaType RIGHT OUTER JOIN'
      
        '                      dbo.LinkEndAntenna ON dbo.AntennaType.id =' +
        ' dbo.LinkEndAntenna.antennaType_id')
    Left = 44
    Top = 160
  end
  object qry_LinkEnd2: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT    *'
      'FROM      Property'
      'ORDER BY name')
    Left = 132
    Top = 92
  end
  object qry_Antennas2: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT     dbo.LinkEndAntenna.*, dbo.AntennaType.*'
      'FROM         dbo.AntennaType RIGHT OUTER JOIN'
      
        '                      dbo.LinkEndAntenna ON dbo.AntennaType.id =' +
        ' dbo.LinkEndAntenna.antennaType_id')
    Left = 136
    Top = 160
  end
  object ADOStoredProc_Link: TADOStoredProc
    Parameters = <>
    Left = 72
    Top = 16
  end
  object qry_Property1: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT    *'
      'FROM      Property'
      'ORDER BY name')
    Left = 236
    Top = 92
  end
  object qry_Property2: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT    *'
      'FROM      Property'
      'ORDER BY name')
    Left = 324
    Top = 92
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '*.dxf'
    Filter = 'Autocad (*.dxf)|*.dxf'
    Left = 452
    Top = 20
  end
end
