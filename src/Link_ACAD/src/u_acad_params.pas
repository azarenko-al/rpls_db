unit u_acad_params;

interface

uses
  IniFiles,SysUtils,Math,

  u_geo,
  u_func
  ;

type
  TClutterType = (ctNone, ctOpen, ctForest, ctWater, ctBuilding, ctCountry);


//
//    case oProfile.Items[i].Clutter_Code of    //
//      DEF_CLU_OPEN_AREA : sClutter:= 'open';     //0
//      DEF_CLU_FOREST    : sClutter:= 'forest';   //1
//      DEF_CLU_WATER     : sClutter:= 'water';    //2
//
//
//      DEF_CLU_ONE_BUILD,
//      DEF_CLU_ONE_HOUSE: sClutter:= 'building'; //7,73
//
//
//      DEF_CLU_COUNTRY  : sClutter:= 'country';  //3,31
//
//(*      LO_TOWN,
//      LO_SUBURB_HOUSE: oACAD_Params.Lines[i].Clutter:= 'country';  //3,31
//
//      LO_CITY,
//      LO_HOUSE       : oACAD_Params.Lines[i].Clutter:= 'building'; //7,73
//*)
//
//    else
//      sClutter:= 'open';  // case
//    end;



  TIni_Acad_params = class(TObject)
  private
  public
    Length_km1 : Double;
    Ref_plane1_offset: double;
  //..  Ref_plane : double; //��������, � �������

    Freq_MHz : double;


    Site1,Site2: record
      Address        : string;
      GroundHeight   : double;
      GroundText     : string;

      Azimuth        : double;

      AntennaHeight  : integer;
      Antenna2Height : integer;
      AntennaText    : string;

      FullHeight   : Double;
      FullHeight2   : Double;
    end;

    Lines: array of record
      Height_m     : double;
      Distance_km  : double;
     // Clutter1      : string;

      Clutter: TClutterType;

      Clu_Height_m : double;
    end;

    function GetMaxHeight: Double;
    
    procedure Prepare;


(*
    procedure LoadFromIniFile(aFileName: string);
    procedure SavetoIniFile(aFileName: string);

    function Validate: Boolean;
*)
  end;


implementation

// ---------------------------------------------------------------
procedure TIni_Acad_params.Prepare;
// ---------------------------------------------------------------
var
  eMinH: Double;
  I: Integer;
begin
  Assert(Freq_MHz>0, 'Value <=0');



  if Length(Lines)>0 then
  begin
    if Site1.GroundHeight=0 then
      Site1.GroundHeight:=Lines[0].Height_m;

    if Site2.GroundHeight=0 then
      Site2.GroundHeight:=Lines[High(Lines)].Height_m;
  end;

  eMinH:=0;

  for I := 0 to High(Lines) do
  begin
    if (eMinH=0) or (Lines[i].Height_m  < eMinH) then
      eMinH := Lines[i].Height_m;
  end;


  // -------------------------
  if Site1.AntennaHeight>0 then
    Site1.FullHeight := Site1.GroundHeight+Site1.AntennaHeight;

  if Site1.Antenna2Height>0 then
    Site1.FullHeight2 := Site1.GroundHeight+Site1.Antenna2Height;

  // -------------------------
  if Site2.AntennaHeight>0 then
    Site2.FullHeight := Site2.GroundHeight+Site2.AntennaHeight;

  if Site2.Antenna2Height>0 then
    Site2.FullHeight2 := Site2.GroundHeight+Site2.Antenna2Height;


 ////////
 // ref_plane1       := eMinH;
  Ref_plane1_offset:= eMinH;
end;


// ---------------------------------------------------------------
function TIni_Acad_params.GetMaxHeight: Double;
// ---------------------------------------------------------------
var
  e1: Double;
  e2: Double;
  eH: Double;
  I: Integer;
begin
  Assert(Site1.AntennaHeight>0, 'Value <=0');
  Assert(Site2.AntennaHeight>0, 'Value <=0');


  e1:=Site1.GroundHeight+Site1.AntennaHeight;
  e2:=Site2.GroundHeight+Site2.AntennaHeight;

  Result:=Max(e1,e2);

(*
  if (Site1.GroundHeight+Params.Site1.AntennaHeight)> (Site2.GroundHeight+Params.Site2.AntennaHeight)
    then eHeig_m:=(Params.Site1.GroundHeight) + Site1.AntennaHeight
    else eHeig_m:=(Params.Site2.GroundHeight) + Site2.AntennaHeight;
*)


  for I := 0 to High(Lines) do
  begin
    eH:=Lines[i].Height_m + Lines[i].Clu_Height_m +
        geo_GetEarthHeight(Lines[i].Distance_km, Length_km1);

    if eH>Result then
      Result :=eH;

  end;


//  Result := ;

end;

end.


{

//-----------------------------------------------------------------------------
procedure TIni_Acad_params.LoadFromIniFile(aFileName: string);
//-----------------------------------------------------------------------------
var
  iCount,i: integer;
begin
  with TmemIniFile.Create(aFileName) do
  Begin
    Site1.Address       :=ReadString ('Site1','Address1','');
    Site1.GroundHeight  :=AsFloat(ReadString  ('Site1','GroundHeight1','0'));
    Site1.AntennaHeight :=ReadInteger('Site1','AntennaHeight1',0);
    Site1.Antenna2Height:=ReadInteger('Site1','Antenna2Height1',0);
    Site1.Azimuth        :=AsFloat(ReadString  ('Site1','Azimut1','0'));

    Site2.Address       :=ReadString ('Site2','Address2','');
    Site2.GroundHeight  :=AsFloat(ReadString  ('Site2','GroundHeight2','0'));
    Site2.AntennaHeight :=ReadInteger('Site2','AntennaHeight2',0);
    Site2.Antenna2Height:=ReadInteger('Site2','Antenna2Height2',0);
    Site2.Azimuth        :=AsFloat(ReadString  ('Site2','Azimut2','0'));

    Length_km1:=AsFloat(ReadString ('lines','Length_km','0'));
    ref_plane1:=AsFloat(ReadString ('lines','Ref_plane','0'));

    iCount   :=        ReadInteger('Lines','Count',0);

    SetLength(Lines,iCount);

    for i:=0 to iCount -1 do
    begin
      Lines[i].Height_m    := AsFloat(ReadString('Lines','Height_m'+IntToStr(i),'0'));
      Lines[i].Distance_km := AsFloat(ReadString('Lines','Distance_km'+IntToStr(i),'0'));
      Lines[i].Clutter     :=         ReadString('Lines','Clutter'+IntToStr(i),'0');
      Lines[i].Clu_Height_m:= AsFloat(ReadString('Lines','Clu_Height_m'+IntToStr(i),'0'));
    end;

    Free;
  end;
end;


//-----------------------------------------------------------------------------
procedure TIni_Acad_params.SavetoIniFile(aFileName: string);
//-----------------------------------------------------------------------------
var
  iCount,i: integer;
begin
  with TmemIniFile.Create(aFileName) do
  begin
    WriteString ('Site1','Address1',       Site1.Address);
    WriteFloat  ('Site1','GroundHeight1',  Site1.GroundHeight);
    WriteInteger('Site1','AntennaHeight1', Site1.AntennaHeight);
    WriteInteger('Site1','Antenna2Height1',Site1.Antenna2Height);
    WriteFloat  ('Site1','Azimut1',        Site1.Azimuth);

    WriteString ('Site2','Address2',       Site2.Address);
    WriteFloat  ('Site2','GroundHeight2',  Site2.GroundHeight);
    WriteInteger('Site2','AntennaHeight2', Site2.AntennaHeight);
    WriteInteger('Site2','Antenna2Height2',Site2.Antenna2Height);
    WriteFloat  ('Site2','Azimut2',        Site2.Azimuth);

    WriteFloat  ('lines','Length_km', Length_km1);
    WriteInteger('lines','Count',     Length(Lines));
    WriteFloat  ('lines','Ref_plane', ref_plane1);

    for i:=0 to high(Lines) do
    begin
       WriteFloat( 'Lines','Height_m'    +IntToStr(i),Lines[i].Height_m);
       WriteFloat ('Lines','Distance_km' +IntToStr(i),Lines[i].Distance_km);
       WriteString('Lines','Clutter'     +IntToStr(i),Lines[i].Clutter);
       WriteFloat ('Lines','Clu_Height_m'+IntToStr(i),Lines[i].Clu_Height_m);
    end;

    UpdateFile;
    Free;
  end;
end;


function TIni_Acad_params.Validate: Boolean;
begin
  Result := (site1.AntennaHeight > 0) and
            (site2.AntennaHeight > 0);

end;


end.

