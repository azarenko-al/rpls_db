unit dm_Main_app_acad;

interface


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, IniFiles,

  u_files,
  u_log,

  I_Link_ACAD,

  u_vars,

  dm_Link_ACAD,

  dm_Main

  ;

type
  TdmMain_link_ACAD = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);

  end;

var
  dmMain_link_ACAD: TdmMain_link_ACAD;

//==============================================================
implementation {$R *.DFM}



//--------------------------------------------------------------
procedure TdmMain_link_ACAD.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const
//  DEF_SOFTWARE_NAME= 'RPLS_DB_LINK';
  DEF_TEMP_FILENAME = 'link_acad.ini';

var
  oIniFile: TIniFile;

var
  iLinkID: Integer;
  iProjectID: Integer;

  sIniFileName: string;


begin
  sIniFileName:=ParamStr(1);
  
  if sIniFileName='' then
//    sIniFileName:=TEMP_INI_FILE;
    sIniFileName:=g_ApplicationDataDir+ DEF_TEMP_FILENAME;

   // sIniFileName:=GetCommonApplicationDataDir(DEF_SOFTWARE_NAME)+
      //            DEF_TEMP_FILENAME;


  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;

 // g_Log:=TLog.Create();
//    IncludeTrailingBackslash(GetTempFileDir())+'_rpls_log.txt');


  oIniFile := TIniFile.Create(sIniFileName);

  with oIniFile do
  begin
  //  iProjectID :=ReadInteger ('main', 'ProjectID', 0);
    iLinkID    :=ReadInteger ('main', 'link_ID',    0);
  end;


  FreeAndNil(oIniFile);



 // if not oLinkCalcParams.Validate then
   // Exit;

(*
  if (iLinkID=0) or
     (iProjectID=0)
  then begin
    ShowMessage('(iLinkID=0) or (iProjectID=0) ');
    Exit;
  end;*)


  TdmMain.Init;
  if not dmMain.OpenDB_reg then
    Exit;


////////////
 // TdmRel_Engine.Init;

 // dmMain.ProjectID:=iProjectID;

 {$DEFINE use_dll1111111}

 {$IFDEF use_dll}
  if Load_ILink_ACAD() then
  begin
    ILink_ACAD.InitADO(dmMain.ADOConnection1.ConnectionObject);
 //   ILink_graph.InitAppHandle(Application.Handle);
    ILink_ACAD.Execute(sIniFileName);

    UnLoad_ILink_ACAD();
  end;

  Exit;

  {$ENDIF}


  TdmLink_ACAD.Init;
//  dmLink_ACAD.Params.LinkID:=iLinkID;

   

 // dmLink_ACAD.OpenData (iLinkID);
  dmLink_ACAD.Execute (iLinkID);//  ('d:\111.ini');

  FreeAndNil(dmLink_ACAD);


 // ShowMessage('export to ACAD');
end;

end.
