unit u_Acad;


interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,

  u_ACAD_classes,

  u_geo,

  u_func,
  u_acad_params,
  u_acad_config
  ;

type

  TAcad = class
  private
    Config: tacad_config;

    FProject: TAcad_Project;

    rBoundsXY:  TBoundsXY;
    FHeig_max_m: Double;

    FList: TStringList;


    procedure Ant_Right;
    procedure DrawAzimuth;
    procedure DrawFrenel;
    procedure DrawRelief;
    procedure Left_ant;
    procedure Left_ant2;
    procedure Left_Label;
    procedure DrawLines;
    procedure DrawClutters;
    procedure Right_ant2;
    procedure Right_label;


  public
    Params: TIni_acad_params;

    constructor Create;
    destructor Destroy; override;

    procedure SaveLineToFile(aFileName: string);

  end;

implementation

const
  DEF_POINT_COUNT=  60;


const
  Groundtext= 'H���.';
  Antennatext='H �.�.';

  LF=#13+#10;

  DEF_END=
    'ENDSEC'     + LF+
    '  0'        + LF+
    'EOF'        ;
  DEF_POLYLINE=
    'POLYLINE'   + LF+
    '  8'        + LF+
    '0'          + LF+
    '  6'        + LF+
    'CONTINUOUS' + LF+
    ' 62'        + LF+
    '     7'     + LF+
    ' 66'        + LF+
    '     1'     + LF+
    '  0'        ;
  DEF_COLORPOLYLINE=
    'POLYLINE'   + LF+
    '  8'        + LF+
    '0'          + LF+
    '  6'        + LF+
    'CONTINUOUS' + LF+
    ' 62'        + LF+
    '     %d'    + LF+
    ' 66'        + LF+
    '     1'     + LF+
    '  0'        ;
  DEF_POLYLINE_END=
    'SEQEND'     + LF+
    '  8'        + LF+
    '0'          + LF+
    ' 62'        + LF+
    '     7'     + LF+
    '  0'        ;
  DEF_VERTEX=
    'VERTEX'     + LF+
    '  8'        + LF+
    '0'          + LF+
    ' 10'        + LF+
    '%8.6f'      + LF+
    ' 20'        + LF+
    '%8.6f'      + LF+
    ' 30'        + LF+
    '0.0'        + LF+
    '  0'        ;
  DEF_TEXT=
    'TEXT'       + LF+
    '  8'        + LF+
    '0'          + LF+
    ' 62'        + LF+
    '     7'     + LF+
    ' 10'        + LF+
    '%8.6f'      + LF+
    ' 20'        + LF+
    '%8.6f'      + LF+
    ' 30'        + LF+
    '0.0'        + LF+
    '  1'        + LF+
    '%s'         + LF+
    ' 40'        + LF+
    '%8.6f'      + LF+
    ' 50'        + LF+
    '%8.6f'      + LF+
    ' 41'        + LF+
    '0.545455'   + LF+
    ' 51'        + LF+
    '15.0'        + LF+
    '  0'        ;
  DEF_LINE=
    'LINE'       + LF+
    '  5'        + LF+
    '26A90'      + LF+
    '  8'        + LF+
    '0'          + LF+
    ' 62'        + LF+
    '     0'     + LF+
    ' 10'        + LF+
    '%8.6f'      + LF+
    ' 20'        + LF+
    '%8.6f'      + LF+
    ' 30'        + LF+
    '0.0'        + LF+
    ' 11'        + LF+
    '%8.6f'      + LF+
    ' 21'        + LF+
    '%8.6f'      + LF+
    ' 31'        + LF+
    '0.0'        + LF+
    '  0'        ;
  DEF_COLORLINE=
    'LINE'       + LF+
    '  5'        + LF+
    '26A90'      + LF+
    '  8'        + LF+
    '0'          + LF+
    ' 62'        + LF+
    '     %d'    + LF+
    ' 10'        + LF+
    '%8.6f'      + LF+
    ' 20'        + LF+
    '%8.6f'      + LF+
    ' 30'        + LF+
    '0.0'        + LF+
    ' 11'        + LF+
    '%8.6f'      + LF+
    ' 21'        + LF+
    '%8.6f'      + LF+
    ' 31'        + LF+
    '0.0'        + LF+
    '  0'        ;

const
  HEIGHT_FOREST_DEF = 20;
  HEIGHT_WATER_DEF  = -2;
  HEIGHT_CITY_DEF   = 25;
  HEIGHT_COUNTRY_DEF= 7;

  COLOR_FOREST = 3;
  COLOR_WATER  = 5;
  COLOR_CITY   = 1;
  COLOR_COUNTRY= 9;
  COLOR_OPEN   = 7;




//------------------------------------------------------
constructor TAcad.Create;
//------------------------------------------------------
begin
  inherited;

  Params := TIni_acad_params.Create();
  Config := tacad_config.Create();

  FProject:=TAcad_Project.Create;

  FList:=TStringList.Create;

end;

//------------------------------------------------------
destructor TAcad.Destroy;
//------------------------------------------------------
begin
  FreeAndNil(Config);
  FreeAndNil(Params);

  FreeAndNil(FProject);

  FList.Free;


  inherited Destroy;
end;

// ---------------------------------------------------------------
procedure TAcad.Ant_Right;
// ---------------------------------------------------------------
var
  eOtnX: Double;
  eOtnY: Double;
  oPolyline: TAcad_Polyline;

begin
  with Params,Config do
  begin
    eOtnX:=22;

    //������ ������������
    eOtnY:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/(FHeig_max_m))*(Site2.GroundHeight+Site2.AntennaHeight);

    // ---------------------------------------------------------------
    oPolyline := FProject.PolyLines.AddItem;

    FList.Add(DEF_POLYLINE);
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+8));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY+8));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(eOtnX-4) , Graph.BoundsXY.MinY+(eOtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(eOtnX-12), Graph.BoundsXY.MinY+(eOtnY));

     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+8)]));
     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY+8)]));
     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY)]));
     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(eOtnX-4) , Graph.BoundsXY.MinY+(eOtnY)]));
     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(eOtnX-12), Graph.BoundsXY.MinY+(eOtnY)]));
    FList.Add(DEF_POLYLINE_END);

     FProject.Texts.AddItem(Graph.BoundsXY.MaxX+(eOtnX-7),    Graph.BoundsXY.MinY+(eOtnY+8.5),IntToStr(Site2.AntennaHeight),3.0,0.0);//������
     FProject.Texts.AddItem(Graph.BoundsXY.MaxX+(eOtnX-7), Graph.BoundsXY.MinY+(eOtnY+4.5),AntennaText,3.0,0.0);   //�������


     FList.Add(format(DEF_TEXT,[Graph.BoundsXY.MaxX+(eOtnX-7),    Graph.BoundsXY.MinY+(eOtnY+8.5),IntToStr(Site2.AntennaHeight),3.0,0.0]));//������
     FList.Add(format(DEF_TEXT,[Graph.BoundsXY.MaxX+(eOtnX-7), Graph.BoundsXY.MinY+(eOtnY+4.5),AntennaText,3.0,0.0]));   //�������

    // ---------------------------------------------------------------
    oPolyline := FProject.PolyLines.AddItem;

    FList.Add(DEF_POLYLINE);
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+((eOtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3)));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(eOtnX-8),    Graph.BoundsXY.MinY+(eOtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+((eOtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3)));


     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+((eOtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3))]));
     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(eOtnX-8),    Graph.BoundsXY.MinY+(eOtnY)]));
     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+((eOtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3))]));
    FList.Add(DEF_POLYLINE_END);

  end;

  // TODO -cMM: TAcad.Ant_Right default body inserted
end;

// ---------------------------------------------------------------
procedure TAcad.DrawAzimuth;
// ---------------------------------------------------------------
begin
  //������
  FProject.Texts.AddItem(935.0 ,-994.0 ,'������ ������        ����. 0 ���.',3.0,0.0);
  FProject.Texts.AddItem(961.0 ,-994.0 ,FloattoStr(Params.Site1.Azimuth),3.0,0.0);
  FProject.Texts.AddItem(935.0 ,-1000.0 ,'�������� ������        ����. 0 ���.',3.0,0.0);
  FProject.Texts.AddItem(962.0 ,-1000.0 ,FloattoStr(Params.Site2.Azimuth),3.0,0.0);
  FProject.Texts.AddItem(947.0 ,-1031.0 , '5. �������� ������� �������    �;',3.0,0.0);
  FProject.Texts.AddItem(991.0 ,-1031.0 , FloattoStr(params.Ref_plane1_offset),3.0,0.0);


  FList.Add(format(DEF_TEXT,[935.0 ,-994.0 ,'������ ������        ����. 0 ���.',3.0,0.0]));
  FList.Add(format(DEF_TEXT,[961.0 ,-994.0 ,FloattoStr(Params.Site1.Azimuth),3.0,0.0]));
  FList.Add(format(DEF_TEXT,[935.0 ,-1000.0 ,'�������� ������        ����. 0 ���.',3.0,0.0]));
  FList.Add(format(DEF_TEXT,[962.0 ,-1000.0 ,FloattoStr(Params.Site2.Azimuth),3.0,0.0]));
  FList.Add(format(DEF_TEXT,[947.0 ,-1031.0 , '5. �������� ������� �������    �;',3.0,0.0]));
  FList.Add(format(DEF_TEXT,[991.0 ,-1031.0 , FloattoStr(params.Ref_plane1_offset),3.0,0.0]));
  //------

  // TODO -cMM: TAcad.DrawAzimuth default body inserted
end;


// ---------------------------------------------------------------
procedure TAcad.DrawFrenel;
// ---------------------------------------------------------------

(*

 //���
      //dy:=dx* (1 - dx/(Distance)));
      if (i=0) or (i=FRENEL_STEP_COUNT) then
        dy:=0
      else
        dy:=Sqrt(( 300/aFreq_Mhz )*
            aNFrenel * (1000*dx)* (1 - dx/aLength_km));


*)

var
  dx: Double;
  dx_km: Double;
  dy: double;
  dy_m: Double;
  eFreq_Mhz: Double;
  eTang: Double;
  eTopY: Double;
  h: Double;
  i: Integer;
  iNFrenel: Double;

  oPolyline: TAcad_Polyline;
  x: Double;
  x_km: Double;
  x_p: Double;
  y: Double;
  y_m: double;
  y_p: Double;

begin
 // Exit;


  eFreq_Mhz:= Params.Freq_MHz;
  iNFrenel:=0.3;


  eTang :=( Params.Site1.FullHeight - Params.Site2.FullHeight) / Params.Length_km1; //������ ����� ��.�����.
//  delta:=Params.Length_km1 / DEF_POINT_COUNT;  //  ������ �������

   //������ ������������
//  eTopY:=((rBoundsXY.MaxY-rBoundsXY.MinY)/(FHeig_max_m)) * Site1.FullHeight;



(*
  tang :=(aH2 - aH1) / aLength_km; //������ ����� ��.�����.
  delta:=aLength_km / FRENEL_STEP_COUNT;  //  ������ �������


    delta:=aLength_km / FRENEL_STEP_COUNT;  //  ������ �������
        //

//
  //  if k=1 then
    //  aSeries.AddNull;


    for i:=0 to FRENEL_STEP_COUNT do
    begin
      dx:= i*delta;
      h:= tang*dx + aH1; //������ ����� ��.�����.
*)

  rBoundsXY:=config.Graph.BoundsXY;



//  with Params,config do
  begin

//    eDistXY:=(Graph.BoundsXY.MaxX-Graph.BoundsXY.MinX) / Params.Length_km1;
 //   eHeigXY:=(Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/(FHeig_max_m);




  oPolyline := FProject.PolyLines.AddItem;

  // ---------------------------------------------------------------
  //������ �����
  // ---------------------------------------------------------------
  FList.Add(DEF_POLYLINE);

  for i:=0 to (DEF_POINT_COUNT+1) -1 do
  begin
    dx:=(rBoundsXY.MaxX-rBoundsXY.MinX)/DEF_POINT_COUNT*i;

//    x_p:=rBoundsXY.MinX+(((rBoundsXY.MaxX-rBoundsXY.MinX)/(DEF_POINT_COUNT))*i);
    x:=rBoundsXY.MinX + dx;

    dx_km:=(Params.Length_km1/DEF_POINT_COUNT)*i;



    dy_m:=Sqrt(( 300/eFreq_Mhz )*
          iNFrenel * (1000*dx_km)* (1 - dx_km/  Params.Length_km1));

    h:= Params.Site1.FullHeight - eTang*dx_km - dY_m  - Params.Ref_plane1_offset;


  //  y_m:=geo_GetEarthHeight___(x_km, Params.Length_km1 );

    y:=((rBoundsXY.MaxY-rBoundsXY.MinY)/(FHeig_max_m)) * h;

    FList.Add(format(DEF_VERTEX,[x, rBoundsXY.MinY + y]));

    oPolyline.Vertexes.AddItem(x_p, rBoundsXY.MinY+y);
  end;

  FList.Add(DEF_POLYLINE_END);
  //---------

  end;

end;



// ---------------------------------------------------------------
procedure TAcad.DrawRelief;
// ---------------------------------------------------------------
var
  i: Integer;

  oPolyline: TAcad_Polyline;
  x_km: Double;
  x_p: Double;
  y_m: double;
  y_p: Double;

begin



  oPolyline := FProject.PolyLines.AddItem;

  // ---------------------------------------------------------------
  //������ �����
  // ---------------------------------------------------------------
  FList.Add(DEF_POLYLINE);
  
  for i:=0 to (DEF_POINT_COUNT+1) -1 do
  begin
    x_p:=rBoundsXY.MinX+(((rBoundsXY.MaxX-rBoundsXY.MinX)/(DEF_POINT_COUNT))*i);
    x_km:=(((Params.Length_km1)/(DEF_POINT_COUNT))*i);


    y_m:= geo_GetEarthHeight(x_km, Params.Length_km1 );

 //   y_m:=geo_GetEarthHeight___(x_km, Params.Length_km1 );


    y_p:=((rBoundsXY.MaxY-rBoundsXY.MinY)/(FHeig_max_m))*y_m;

    FList.Add(format(DEF_VERTEX,[x_p, rBoundsXY.MinY+y_p]));

    oPolyline.Vertexes.AddItem(x_p, rBoundsXY.MinY+y_p);
  end;
  FList.Add(DEF_POLYLINE_END);
  //---------


end;

// ---------------------------------------------------------------
procedure TAcad.Left_ant;
// ---------------------------------------------------------------
var
  eOtnX: Double;
  eOtnY: Double;
  oPolyline: TAcad_Polyline;
begin
  with Params,config do
  begin


   // ---------------------------------------------------------------
  //������ �����
  // ---------------------------------------------------------------
   eOtnX:=22;
   // ����� ����� �������
   eOtnY:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/(FHeig_max_m)) * (Site1.GroundHeight+Site1.AntennaHeight);


   // ---------------------------------------------------------------
   oPolyline := FProject.PolyLines.AddItem;

   FList.Add(DEF_POLYLINE);

    oPolyline.Vertexes.AddItem(rBoundsXY.MinX-(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+8));
    oPolyline.Vertexes.AddItem(rBoundsXY.MinX-(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY+8));
    oPolyline.Vertexes.AddItem(rBoundsXY.MinX-(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY));
    oPolyline.Vertexes.AddItem(rBoundsXY.MinX-(eOtnX-4) , Graph.BoundsXY.MinY+(eOtnY));
    oPolyline.Vertexes.AddItem(rBoundsXY.MinX-(eOtnX-12), Graph.BoundsXY.MinY+(eOtnY));


    FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+8)]));
    FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY+8)]));
    FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY)]));
    FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(eOtnX-4) , Graph.BoundsXY.MinY+(eOtnY)]));
    FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(eOtnX-12), Graph.BoundsXY.MinY+(eOtnY)]));
   FList.Add(DEF_POLYLINE_END);


    FProject.Texts.AddItem(Graph.BoundsXY.MinX-(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+8.5),IntToStr(Site1.AntennaHeight),3.0,0.0); // ������
    FProject.Texts.AddItem(Graph.BoundsXY.MinX-(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+4.5),AntennaText,3.0,0.0); // �������

    FList.Add(format(DEF_TEXT,[Graph.BoundsXY.MinX-(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+8.5),IntToStr(Site1.AntennaHeight),3.0,0.0])); // ������
    FList.Add(format(DEF_TEXT,[Graph.BoundsXY.MinX-(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+4.5),AntennaText,3.0,0.0])); // �������


    // ---------------------------------------------------------------
   oPolyline := FProject.PolyLines.AddItem;

   FList.Add(DEF_POLYLINE);
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-((eOtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3)));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(eOtnX-8),    Graph.BoundsXY.MinY+(eOtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-((eOtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3)));


    FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-((eOtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3))]));
    FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(eOtnX-8),    Graph.BoundsXY.MinY+(eOtnY)]));
    FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-((eOtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3))]));
   FList.Add(DEF_POLYLINE_END);

  end;
end;

// ---------------------------------------------------------------
procedure TAcad.Left_ant2;
// ---------------------------------------------------------------
var
  eOtnX: Double;
  eOtnY: Double;

   oPolyline: TAcad_Polyline;
begin
  eOtnX:=36;

  with Params,config do
  begin


      // ����� ����� �������
      eOtnY:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/(FHeig_max_m))*(Site1.GroundHeight+Site1.Antenna2Height);

      // ---------------------------------------------------------------
      oPolyline := FProject.PolyLines.AddItem;


      FList.Add(DEF_POLYLINE);
       oPolyline.Vertexes.AddItem(rBoundsXY.MinX-(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+8));
       oPolyline.Vertexes.AddItem(rBoundsXY.MinX-(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY+8));
       oPolyline.Vertexes.AddItem(rBoundsXY.MinX-(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY));
       oPolyline.Vertexes.AddItem(rBoundsXY.MinX-(eOtnX-4) , Graph.BoundsXY.MinY+(eOtnY));
       oPolyline.Vertexes.AddItem(rBoundsXY.MinX-(eOtnX-12), Graph.BoundsXY.MinY+(eOtnY));


       FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+8)]));
       FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY+8)]));
       FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY)]));
       FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(eOtnX-4) , Graph.BoundsXY.MinY+(eOtnY)]));
       FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(eOtnX-12), Graph.BoundsXY.MinY+(eOtnY)]));
      FList.Add(DEF_POLYLINE_END);

       FProject.Texts.AddItem(Graph.BoundsXY.MinX-(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+8.5),IntToStr(Site1.Antenna2Height),3.0,0.0); // ������
       FProject.Texts.AddItem(Graph.BoundsXY.MinX-(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+4.5),AntennaText,3.0,0.0); // �������


       FList.Add(format(DEF_TEXT,[Graph.BoundsXY.MinX-(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+8.5),IntToStr(Site1.Antenna2Height),3.0,0.0])); // ������
       FList.Add(format(DEF_TEXT,[Graph.BoundsXY.MinX-(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+4.5),AntennaText,3.0,0.0])); // �������

      // ---------------------------------------------------------------
      oPolyline := FProject.PolyLines.AddItem;


      FList.Add(DEF_POLYLINE);
       oPolyline.Vertexes.AddItem(rBoundsXY.MinX-((eOtnX-8)-(sin(60)*3)),     Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3)));
       oPolyline.Vertexes.AddItem(rBoundsXY.MinX-(eOtnX-8),                   Graph.BoundsXY.MinY+(eOtnY));
       oPolyline.Vertexes.AddItem(rBoundsXY.MinX-((eOtnX-8)+(sin(60)*3)),     Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3)));


       FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-((eOtnX-8)-(sin(60)*3)),  Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3))]));
       FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(eOtnX-8),                Graph.BoundsXY.MinY+(eOtnY)]));
       FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-((eOtnX-8)+(sin(60)*3)),  Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3))]));
      FList.Add(DEF_POLYLINE_END);

  end;
  
end;

// ---------------------------------------------------------------
procedure TAcad.Left_Label;
// ---------------------------------------------------------------
var
  eOtnX: Double;
  eOtnY: Double;
   oPolyline: TAcad_Polyline;
begin
  with Params,config do
  begin


    eOtnX:=22;
   //������� �����
    eOtnY:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/(FHeig_max_m))*(Site1.GroundHeight);

      // ---------------------------------------------------------------
      oPolyline := FProject.PolyLines.AddItem;


    FList.Add(DEF_POLYLINE);
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+8));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY+8));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(eOtnX-4) , Graph.BoundsXY.MinY+(eOtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(eOtnX-12), Graph.BoundsXY.MinY+(eOtnY));


     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+8)]));
     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY+8)]));
     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY)]));
     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(eOtnX-4) , Graph.BoundsXY.MinY+(eOtnY)]));
     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(eOtnX-12), Graph.BoundsXY.MinY+(eOtnY)]));
    FList.Add(DEF_POLYLINE_END);

     FProject.Texts.AddItem(Graph.BoundsXY.MinX-(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+8.5),FloatToStr(Site1.GroundHeight+Ref_plane1_offset),3.0,0.0); // ������
     FProject.Texts.AddItem(Graph.BoundsXY.MinX-(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+4.5),GroundText,3.0,0.0); // �������


     FList.Add(format(DEF_TEXT,[Graph.BoundsXY.MinX-(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+8.5),FloatToStr(Site1.GroundHeight+Ref_plane1_offset),3.0,0.0])); // ������
     FList.Add(format(DEF_TEXT,[Graph.BoundsXY.MinX-(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+4.5),GroundText,3.0,0.0])); // �������

      // ---------------------------------------------------------------
      oPolyline := FProject.PolyLines.AddItem;


    FList.Add(DEF_POLYLINE);
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-((eOtnX-8)-(sin(60)*3)),  Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3)));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-(eOtnX-8),                Graph.BoundsXY.MinY+(eOtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MinX-((eOtnX-8)+(sin(60)*3)),  Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3)));


     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-((eOtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3))]));
     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-(eOtnX-8),    Graph.BoundsXY.MinY+(eOtnY)]));
     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MinX-((eOtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3))]));
    FList.Add(DEF_POLYLINE_END);
  end;

end;

// ---------------------------------------------------------------
procedure TAcad.DrawLines;
// ---------------------------------------------------------------
var
  eDist: Double;
  eDistXY: Double;
  eEarthHeight_m: Double;
  eHeig: Double;
  eHeigXY: Double;
  font_size: Double;
  i: Integer;
  j: Integer;
begin
  with Params,config do
  begin

    //������ ������
    eDistXY:=(Graph.BoundsXY.MaxX-Graph.BoundsXY.MinX) / Params.Length_km1;
    eHeigXY:=(Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/(FHeig_max_m);

    j:= 0;
    font_size:=2.5;



    //����� � ���������
    for i:= 0 to High(lines) do
    Begin
      eEarthHeight_m:= geo_GetEarthHeight(Lines[i].Distance_km, Params.Length_km1 );


      eHeig:= (eHeigXY* (Lines[i].Height_m-Ref_plane1_offset  + eEarthHeight_m ));
      eDist:= (eDistXY* Lines[i].Distance_km);

      FProject.Lines.AddItem(    rBoundsXY.MinX+ eDist,
                                 rBoundsXY.MinY,
                                 rBoundsXY.MinX+ eDist,
                                 rBoundsXY.MinY+ eheig);


      FList.Add(format(DEF_LINE,[Config.Graph.BoundsXY.MinX+ eDist,
                                 Config.Graph.BoundsXY.MinY,
                                 Config.Graph.BoundsXY.MinX+ eDist,
                                 Config.Graph.BoundsXY.MinY+ eHeig]));

      // ����������� ������ � ��������� �������
      if (i = 0) or (i = High(lines)) then
      begin

(*        FProject.Texts.AddItem(Graph.BoundsXY.MinX+ Dist,
                                 Graph.BoundsXY.MinY,
                                 Graph.BoundsXY.MinX+ Dist,
                                 Graph.BoundsXY.MinY+ heig);
*)
        FProject.Texts.AddItem(rBoundsXY.MinX+ eDist,-958.0, FloattoStr(Lines[i].Distance_km),font_size,90.0);
        FProject.Texts.AddItem(rBoundsXY.MinX+ eDist,-971.0, FloattoStr(Lines[i].Height_m),font_size,90.0);



        FList.Add(format(DEF_TEXT,[Config.Graph.BoundsXY.MinX+ eDist,-958.0, FloattoStr(Lines[i].Distance_km),font_size,90.0]));
        FList.Add(format(DEF_TEXT,[Config.Graph.BoundsXY.MinX+ eDist,-971.0, FloattoStr(Lines[i].Height_m),font_size,90.0]));
      end else

      // ����������� ������� ����������� � ������ ���������� ������� ������,
      // ����� �� ���� ��������� �������� ���� �� �����. /13 - ��������� ����������������
      if (params.Lines[i].Distance_km - params.Lines[j].Distance_km)>(font_size/13) then
        // ���� ������� ����� �� ���������� �� ���������� ������ - ����� �� ����������
        if Lines[i].Height_m <> Lines[j].Height_m then
        begin
          FProject.Texts.AddItem(Config.Graph.BoundsXY.MinX+ eDist,-971.0, FloattoStr(Lines[i].Height_m),font_size,90.0);
          FProject.Texts.AddItem(Config.Graph.BoundsXY.MinX+ eDist,-958.0, FloattoStr(Lines[i].Distance_km),font_size,90.0);

          FList.Add(format(DEF_TEXT,[Config.Graph.BoundsXY.MinX+ eDist,-971.0, FloattoStr(Lines[i].Height_m),font_size,90.0]));
          FList.Add(format(DEF_TEXT,[Config.Graph.BoundsXY.MinX+ eDist,-958.0, FloattoStr(Lines[i].Distance_km),font_size,90.0]));

           if lines[i].Clu_Height_m>0 then
             begin
               FList.Add(format(DEF_TEXT,[Config.Graph.BoundsXY.MinX+ eDist,-984.0, FloattoStr(Lines[i].Clu_Height_m),font_size,90.0]));
             end;
          j:= i;
        end;

    end;

  end;
end;


// ---------------------------------------------------------------
procedure TAcad.DrawClutters;
// ---------------------------------------------------------------
var
  c: Integer;
  rClutterTmp: TClutterType;

  eDist: Double;
  eDistXY: Double;
  eEarthHeight_m: Double;
  eHeig: Double;
  eHeigXY: Double;
  f: Boolean;
  h1: Double;
  i: Integer;
  iColor: Integer;
  oPolyline: TAcad_Polyline;
  x: Boolean;
begin
 //Exit;

  with Params,config do
  begin

    eDistXY:=(Graph.BoundsXY.MaxX-Graph.BoundsXY.MinX) / Params.Length_km1;
    eHeigXY:=(Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/(FHeig_max_m);



    //���������
    // -----------------------------------------
    oPolyline := FProject.PolyLines.AddItem;

    FList.Add(DEF_POLYLINE);
    for i:= 0 to high(lines) do
    Begin
      eEarthHeight_m:= geo_GetEarthHeight(Lines[i].Distance_km, Params.Length_km1 );


      eHeig:= (eHeigXY* (Lines[i].Height_m - Ref_plane1_offset + eEarthHeight_m));
      eDist:= (eDistXY* Lines[i].Distance_km);

      oPolyline.Vertexes.AddItem(rBoundsXY.MinX+ eDist,
                                 rBoundsXY.MinY+ eheig);

      FList.Add(format(DEF_VERTEX,[rBoundsXY.MinX+ eDist,
                                   rBoundsXY.MinY+ eheig]));
    End;

    FList.Add(DEF_POLYLINE_END);

    //������� ���������
    x:=false;
    f:=true;
    rClutterTmp:=ctNone;

    for i:= 0 to high(lines) do
    Begin
      If rClutterTmp=lines[i].Clutter then x:=false
                                      else x:=true;

      while (f) do
      begin
        if lines[i].Clutter=ctForest then
        begin
          FList.Add(format(DEF_COLORPOLYLINE,[COLOR_FOREST]));
          iColor:=COLOR_FOREST;

          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_FOREST_DEF);
        end else

        if lines[i].Clutter=ctbuilding then
        begin
          FList.Add(format(DEF_COLORPOLYLINE,[COLOR_CITY]));
          iColor:=COLOR_CITY;

          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_CITY_DEF);
        end else

        if lines[i].Clutter=ctcountry then
        begin
          FList.Add(format(DEF_COLORPOLYLINE,[COLOR_COUNTRY]));
          iColor:=COLOR_COUNTRY;

          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_COUNTRY_DEF);
        end else

        if lines[i].Clutter=ctopen then
        begin
          FList.Add(format(DEF_COLORPOLYLINE,[COLOR_OPEN]));
          iColor:=COLOR_OPEN;

         h1:= 0;
        end else

        if lines[i].Clutter=ctwater then
        begin
          FList.Add(format(DEF_COLORPOLYLINE,[COLOR_WATER]));
          iColor:=COLOR_WATER;

          h1:= HEIGHT_WATER_DEF;
        end;


        oPolyline := FProject.PolyLines.AddColorItem(iColor);


        f:=false;
        x:=false;
      end;

      while (x) do
      begin
        FList.Add(DEF_POLYLINE_END);

        if lines[i].Clutter=ctforest then
        begin
          FList.Add(format(DEF_COLORPOLYLINE,[COLOR_FOREST]));
          iColor:=COLOR_FOREST;

          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_FOREST_DEF);
        end else

        if lines[i].Clutter=ctbuilding then
        begin
          FList.Add(format(DEF_COLORPOLYLINE,[COLOR_CITY]));
           iColor:=COLOR_CITY;

          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_CITY_DEF);
        end else

        if lines[i].Clutter=ctcountry then
        begin
          FList.Add(format(DEF_COLORPOLYLINE,[COLOR_COUNTRY]));
           iColor:=COLOR_COUNTRY;

          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_COUNTRY_DEF);
        end else

        if lines[i].Clutter=ctopen then
        begin
          FList.Add(format(DEF_COLORPOLYLINE,[COLOR_OPEN]));
           iColor:=COLOR_OPEN;

          h1:= 0;
        end else

        if lines[i].Clutter=ctwater then
        begin
          FList.Add(format(DEF_COLORPOLYLINE,[COLOR_WATER]));
           iColor:=COLOR_WATER;

         h1:= HEIGHT_WATER_DEF;
        end;

        x:=false;
      end;

      eEarthHeight_m:= geo_GetEarthHeight(Lines[i].Distance_km, Params.Length_km1 );
//      eEarthHeight_m:=0;

      eHeig:= (eHeigXY* (Lines[i].Height_m - Ref_plane1_offset + h1 + eEarthHeight_m ));
      eDist:= (eDistXY * Lines[i].Distance_km);

      //h1:= h1*eHeigXY;

//       oPolyline := FProject.PolyLines.AddItem;

      oPolyline := FProject.PolyLines.AddColorItem(iColor);

      oPolyline.Vertexes.AddItem(rBoundsXY.MinX+ eDist,
                                (rBoundsXY.MinY+ eheig));


      FList.Add(format(DEF_VERTEX,[rBoundsXY.MinX+ eDist,
                                   (rBoundsXY.MinY+ eheig)]));

      rClutterTmp:=lines[i].Clutter;
    end;

    FList.Add(DEF_POLYLINE_END);

    // ---------------------------------------------------------------
     //������ ���������
    x:=false;
    f:=true;
    rClutterTmp:=ctNone;

    for i:= 0 to high(lines) do
    begin
      if rClutterTmp=lines[i].Clutter then x:=false
                                     else x:=true;

      while (f) do
      begin
        if lines[i].Clutter=ctforest then
        begin
          c := COLOR_FOREST;
          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_FOREST_DEF);
        end else

        if lines[i].Clutter=ctbuilding then
        begin
          c := COLOR_CITY;
          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_CITY_DEF);
        end else

        if lines[i].Clutter=ctcountry then
        begin
          c := COLOR_COUNTRY;
          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_COUNTRY_DEF);
        end else

        if lines[i].Clutter=ctopen then
        begin
         c := COLOR_OPEN;
         h1:= 0;
        end else

        if lines[i].Clutter=ctwater then
        begin
          c := COLOR_WATER;
          h1:= HEIGHT_WATER_DEF;
        end;

        f:=false;
        x:=true;
      end;

      while (x) do
      begin
        if lines[i].Clutter=ctforest then
        begin
          c := COLOR_FOREST;
          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_FOREST_DEF);
        end else

        if lines[i].Clutter=ctbuilding then
        begin
          c := COLOR_CITY;
          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_CITY_DEF);
        end else

        if lines[i].Clutter=ctcountry then
        begin
          c := COLOR_COUNTRY;
          h1:= IIF(lines[i].Clu_Height_m>0, lines[i].Clu_Height_m, HEIGHT_COUNTRY_DEF);
        end else

        if lines[i].Clutter=ctopen then
        begin
          c := COLOR_OPEN;
          h1:= 0;
        end else

        if lines[i].Clutter=ctwater then
        begin
          c := COLOR_WATER;
          h1:= HEIGHT_WATER_DEF;
        end;

        x:=false;
      end;

      eEarthHeight_m:= geo_GetEarthHeight(Lines[i].Distance_km, Params.Length_km1 );
//      eEarthHeight_m:=0;


      eHeig:= (eHeigXY* (Lines[i].Height_m - Ref_plane1_offset  + eEarthHeight_m));
      eDist:= (eDistXY* Lines[i].Distance_km);
      h1:= h1*eHeigXY;

      FProject.Lines.AddColorItem(c, Config.Graph.BoundsXY.MinX+ eDist, Config.Graph.BoundsXY.MinY+ eHeig, Graph.BoundsXY.MinX+ eDist, (Graph.BoundsXY.MinY+ eHeig)+h1);

      FList.Add(format(DEF_COLORLINE,[c,Config.Graph.BoundsXY.MinX+ eDist, Graph.BoundsXY.MinY+ eHeig, Graph.BoundsXY.MinX+ eDist, (Graph.BoundsXY.MinY+ eHeig)+h1]));
    end;
  end;
end;


// ---------------------------------------------------------------
procedure TAcad.Right_ant2;
// ---------------------------------------------------------------
var
  eOtnX: Double;
  eOtnY: Double;
   oPolyline: TAcad_Polyline;
begin
  with Params,config do
  begin
      eOtnX:=36;

        //������ ������������
      eOtnY:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/(FHeig_max_m))*(Site2.GroundHeight+Site2.Antenna2Height);

      // ---------------------------------------------------------------
      oPolyline := FProject.PolyLines.AddItem;


      FList.Add(DEF_POLYLINE);
       oPolyline.Vertexes.AddItem(rBoundsXY.MaxX+(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+8));
       oPolyline.Vertexes.AddItem(rBoundsXY.MaxX+(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY+8));
       oPolyline.Vertexes.AddItem(rBoundsXY.MaxX+(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY));
       oPolyline.Vertexes.AddItem(rBoundsXY.MaxX+(eOtnX-4) , Graph.BoundsXY.MinY+(eOtnY));
       oPolyline.Vertexes.AddItem(rBoundsXY.MaxX+(eOtnX-12), Graph.BoundsXY.MinY+(eOtnY));


       FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+8)]));
       FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY+8)]));
       FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY)]));
       FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(eOtnX-4) , Graph.BoundsXY.MinY+(eOtnY)]));
       FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(eOtnX-12), Graph.BoundsXY.MinY+(eOtnY)]));

      FList.Add(DEF_POLYLINE_END);
       FProject.Texts.AddItem(Graph.BoundsXY.MaxX+(eOtnX-7),    Graph.BoundsXY.MinY+(eOtnY+8.5),IntToStr(Site2.Antenna2Height),3.0,0.0);//������
       FProject.Texts.AddItem(Graph.BoundsXY.MaxX+(eOtnX-7), Graph.BoundsXY.MinY+(eOtnY+4.5),AntennaText,3.0,0.0);   //�������


       FList.Add(format(DEF_TEXT,[Graph.BoundsXY.MaxX+(eOtnX-7),    Graph.BoundsXY.MinY+(eOtnY+8.5),IntToStr(Site2.Antenna2Height),3.0,0.0]));//������
       FList.Add(format(DEF_TEXT,[Graph.BoundsXY.MaxX+(eOtnX-7), Graph.BoundsXY.MinY+(eOtnY+4.5),AntennaText,3.0,0.0]));   //�������


      // ---------------------------------------------------------------
      oPolyline := FProject.PolyLines.AddItem;

      FList.Add(DEF_POLYLINE);
       oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+((eOtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3)));
       oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(eOtnX-8),    Graph.BoundsXY.MinY+(eOtnY));
       oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+((eOtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3)));


       FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+((eOtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3))]));
       FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(eOtnX-8),    Graph.BoundsXY.MinY+(eOtnY)]));
       FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+((eOtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3))]));
      FList.Add(DEF_POLYLINE_END);

  end;

end;

// ---------------------------------------------------------------
procedure TAcad.Right_label;
// ---------------------------------------------------------------
var
  eOtnX: Double;
  eOtnY: Double;
  oPolyline: TAcad_Polyline;

begin
  eOtnX:=22;

  with Params,Config do
  begin
    //---------
    // ---------------------------------------------------------------
    //������ �����
    // ---------------------------------------------------------------
    //������� �����
    eOtnY:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/(FHeig_max_m))*(Site2.GroundHeight);

      // ---------------------------------------------------------------
      oPolyline := FProject.PolyLines.AddItem;


    FList.Add(DEF_POLYLINE);
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+8));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(eOtnX-8),  Graph.BoundsXY.MinY+(eOtnY+8));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(eOtnX-8),  Graph.BoundsXY.MinY+(eOtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(eOtnX-4) , Graph.BoundsXY.MinY+(eOtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(eOtnX-12), Graph.BoundsXY.MinY+(eOtnY));




     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(eOtnX),    Graph.BoundsXY.MinY+(eOtnY+8)]));
     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY+8)]));
     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(eOtnX-8), Graph.BoundsXY.MinY+(eOtnY)]));
     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(eOtnX-4) , Graph.BoundsXY.MinY+(eOtnY)]));
     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(eOtnX-12), Graph.BoundsXY.MinY+(eOtnY)]));
    FList.Add(DEF_POLYLINE_END);

     FProject.Texts.AddItem(Graph.BoundsXY.MaxX+(eOtnX-7),    Graph.BoundsXY.MinY+(eOtnY+8.5),FloatToStr(Site2.GroundHeight+Ref_plane1_offset),3.0,0.0);//������
     FProject.Texts.AddItem(Graph.BoundsXY.MaxX+(eOtnX-7), Graph.BoundsXY.MinY+(eOtnY+4.5),GroundText,3.0,0.0);   //�������


     FList.Add(format(DEF_TEXT,[Graph.BoundsXY.MaxX+(eOtnX-7),    Graph.BoundsXY.MinY+(eOtnY+8.5),FloatToStr(Site2.GroundHeight+Ref_plane1_offset),3.0,0.0]));//������
     FList.Add(format(DEF_TEXT,[Graph.BoundsXY.MaxX+(eOtnX-7), Graph.BoundsXY.MinY+(eOtnY+4.5),GroundText,3.0,0.0]));   //�������

      // ---------------------------------------------------------------
      oPolyline := FProject.PolyLines.AddItem;


    FList.Add(DEF_POLYLINE);
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+((eOtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3)));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+(eOtnX-8),    Graph.BoundsXY.MinY+(eOtnY));
     oPolyline.Vertexes.AddItem(Graph.BoundsXY.MaxX+((eOtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3)));


     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+((eOtnX-8)-(sin(60)*3)),    Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3))]));
     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+(eOtnX-8),    Graph.BoundsXY.MinY+(eOtnY)]));
     FList.Add(format(DEF_VERTEX,[Graph.BoundsXY.MaxX+((eOtnX-8)+(sin(60)*3)),    Graph.BoundsXY.MinY+((eOtnY)-(sin(30)*3))]));
    FList.Add(DEF_POLYLINE_END);
    //---------
  end;
end;


//------------------------------------------------------
procedure TAcad.SaveLineToFile(aFileName: string);
//------------------------------------------------------

var
  Font_size,eOtnX,eOtnY,eRightY,eLeftY:Double;



  x_dugi,y_dugi,x_dugi0,y_dugi0:Double;
  r_dugi:integer;
  x_p:double;
  y_p:double;
  y_p1:double;
  y_m:double;
  x_km:double;

  eDistXY,eHeigXY,eDist,eHeig:double;

  i,c:Integer;
  s:string;
  ClutterTmp,d:string;
  eEarthHeight_m: Double;
  h1:double;
  iColor: Integer;
  x,f,bAntenna2:boolean;

  oPolyline: TAcad_Polyline;


begin
  Assert(FileExists(Config.Template_filename), 'FileExists: '+ Config.Template_filename);

  aFileName:= ChangeFileExt(aFileName, '.dxf');


  Params.Prepare;


//  FProject:=TAcad_Project.Create;



 // Assert(Length(Params.Lines)>0);


//  lines[i]


  rBoundsXY := Config.Graph.BoundsXY;

  // ---------------------------------------------------------------



  FHeig_max_m := Params.GetMaxHeight()-Params.Ref_plane1_offset;

  Params.Site1.GroundHeight:=Params.Site1.GroundHeight-Params.Ref_plane1_offset;
  Params.Site2.GroundHeight:=Params.Site2.GroundHeight-Params.Ref_plane1_offset;


 // Length_km

//  Params.Site1

  with Params,Config do
//  with Params do
  begin
    FList.LoadFromFile(Config.Template_filename);

    if (Site1.Antenna2Height>0) and (site2.Antenna2Height>0)
      then bAntenna2:= True
      else bAntenna2:= False;

   {
    if (Params.Site1.GroundHeight+Params.Site1.AntennaHeight)> (Params.Site2.GroundHeight+Params.Site2.AntennaHeight)
      then FHeig_max_m:=(Params.Site1.GroundHeight-ref_plane1) + Params.Site1.AntennaHeight
      else FHeig_max_m:=(Params.Site2.GroundHeight-ref_plane1) + Params.Site2.AntennaHeight;

    Site1.GroundHeight:=Params.Site1.GroundHeight-ref_plane1;
    Site2.GroundHeight:=Params.Site2.GroundHeight-ref_plane1;


    FHeig_max_m := Params.GetMaxHeight()-ref_plane1;
    }

    Assert(Params.Length_km1>0);

    //������ ������
    eDistXY:=(Graph.BoundsXY.MaxX-Graph.BoundsXY.MinX) / Params.Length_km1;
    eHeigXY:=(Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/(FHeig_max_m);

    DrawLines();

    //���������
    DrawClutters();


   //������ �����
   Left_ant();

    //������ ������������
    Ant_Right();

    if bAntenna2=true then
    begin
       // ����� ����� �������
       Left_ant2();

        //������ ������������
       Right_ant2();
    end;

    // ---------------------------------------------------------------
   //����� �����
   // ---------------------------------------------------------------
    Left_Label();

    //������ �����
    //---------
    Right_label();

    // ---------------------------------------------------------------
    // ---------------------------------------------------------------
    //������� �����

    // ---------------------------------------------------------------
    //������ �����
    // ---------------------------------------------------------------
    //������ �����
    eOtnY:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/(FHeig_max_m))*(Site2.GroundHeight+Site2.AntennaHeight);
    eRightY:=eOtnY;
    FList.Add(format(DEF_LINE,[Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY+ eOtnY]));

    //�������
    r_dugi:=5;
    eOtnX:=-r_dugi;
    y_dugi0:=(Graph.BoundsXY.MinY+eOtnY);
    x_dugi0:=(Graph.BoundsXY.Maxx+eOtnX);


    // ---------------------------------------------------------------
    oPolyline := FProject.PolyLines.AddItem;


    FList.Add(DEF_POLYLINE);
    for i:=0 to r_dugi+1 -1 do
    begin
      y_dugi:=(y_dugi0-i);
      x_dugi:=(x_dugi0)+ Sqrt(Abs(Sqr(r_dugi)-sqr(y_dugi-(y_dugi0))));
      FList.Add(format(DEF_VERTEX,[x_dugi,y_dugi]));

      oPolyline.Vertexes.AddItem(x_dugi,y_dugi);
    end;
    FList.Add(DEF_POLYLINE_END);

//    TAcad_POLYLINE


    // ---------------------------------------------------------------
    oPolyline := FProject.PolyLines.AddItem;

    FList.Add(DEF_POLYLINE);
    for i:=0 to r_dugi+1 -1 do
    begin
      y_dugi:=(y_dugi0+i);
      x_dugi:=(x_dugi0)+ Sqrt(Abs(Sqr(r_dugi)-sqr(y_dugi-(y_dugi0))));
      FList.Add(format(DEF_VERTEX,[x_dugi,y_dugi]));

      oPolyline.Vertexes.AddItem(x_dugi,y_dugi);
    end;
    FList.Add(DEF_POLYLINE_END);


    //----
    //����� �����
    eOtnX:=r_dugi;
    eOtnY:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/FHeig_max_m)*(Site1.GroundHeight+Site1.AntennaHeight);
    eLeftY:=eOtnY;


    FProject.Lines.AddItem(Graph.BoundsXY.MinX, Graph.BoundsXY.MinY, Graph.BoundsXY.MinX, Graph.BoundsXY.MinY+ eOtnY);

    FList.Add(format(DEF_LINE,[Graph.BoundsXY.MinX, Graph.BoundsXY.MinY, Graph.BoundsXY.MinX, Graph.BoundsXY.MinY+ eOtnY]));
    //�������
    y_dugi0:=(Graph.BoundsXY.MinY+eOtnY);
    x_dugi0:=(Graph.BoundsXY.Minx+eOtnX);

    // ---------------------------------------------------------------
    oPolyline := FProject.PolyLines.AddItem;


    FList.Add(DEF_POLYLINE);
    for i:=0 to r_dugi+1 -1 do
    begin
      y_dugi:=(y_dugi0+i);
      x_dugi:=(x_dugi0)- Sqrt(Abs(Sqr(r_dugi)-sqr(y_dugi-(y_dugi0))));
      FList.Add(format(DEF_VERTEX,[x_dugi,y_dugi]));

      oPolyline.Vertexes.AddItem(x_dugi,y_dugi);
    end;
    FList.Add(DEF_POLYLINE_END);

    // ---------------------------------------------------------------
    oPolyline := FProject.PolyLines.AddItem;


    FList.Add(DEF_POLYLINE);
    for i:=0 to r_dugi+1 -1 do
    begin
      y_dugi:=(y_dugi0-i);
      x_dugi:=(x_dugi0)- Sqrt(Abs(Sqr(r_dugi)-sqr(y_dugi-(y_dugi0))));
      FList.Add(format(DEF_VERTEX,[x_dugi,y_dugi]));


      oPolyline.Vertexes.AddItem(x_dugi,y_dugi);
    end;
    FList.Add(DEF_POLYLINE_END);



    //----
    //��������� �����
    FProject.Lines.AddItem(Graph.BoundsXY.MinX, Graph.BoundsXY.MinY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY);
    FProject.Lines.AddItem(Graph.BoundsXY.MinX, Graph.BoundsXY.MinY+ eLeftY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY+ eRightY);

    FList.Add(format(DEF_LINE,[Graph.BoundsXY.MinX, Graph.BoundsXY.MinY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY]));
    FList.Add(format(DEF_LINE,[Graph.BoundsXY.MinX, Graph.BoundsXY.MinY+ eLeftY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY+ eRightY]));

    //------
    // 2-�� �������
    if bAntenna2=true then
    begin
       //������ �����
       //������ �����
      eOtnY:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/FHeig_max_m)*(Site2.GroundHeight+Site2.Antenna2Height);
      eRightY:=eOtnY;

      FProject.Lines.AddItem(Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY+ eOtnY);

      FList.Add(format(DEF_LINE,[Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY+ eOtnY]));
      //�������
      r_dugi:=5;
      eOtnX:=-r_dugi;
      y_dugi0:=(Graph.BoundsXY.MinY+eOtnY);
      x_dugi0:=(Graph.BoundsXY.Maxx+eOtnX);

      // ---------------------------------------------------------------
      oPolyline := FProject.PolyLines.AddItem;


      FList.Add(DEF_POLYLINE);
      for i:=0 to r_dugi+1 -1 do
      Begin
        y_dugi:=(y_dugi0-i);
        x_dugi:=(x_dugi0)+ Sqrt(Abs(Sqr(r_dugi)-sqr(y_dugi-(y_dugi0))));
        FList.Add(format(DEF_VERTEX,[x_dugi,y_dugi]));

        oPolyline.Vertexes.AddItem(x_dugi,y_dugi);
      End;
      FList.Add(DEF_POLYLINE_END);

      // ---------------------------------------------------------------
      oPolyline := FProject.PolyLines.AddItem;


      FList.Add(DEF_POLYLINE);
      for i:=0 to r_dugi+1 -1 do
      Begin
        y_dugi:=(y_dugi0+i);
        x_dugi:=(x_dugi0)+ Sqrt(Abs(Sqr(r_dugi)-sqr(y_dugi-(y_dugi0))));
        FList.Add(format(DEF_VERTEX,[x_dugi,y_dugi]));

        oPolyline.Vertexes.AddItem(x_dugi,y_dugi);
      End;
      FList.Add(DEF_POLYLINE_END);

      //----
      //����� �����
      eOtnX:=r_dugi;
      eOtnY:=((Graph.BoundsXY.MaxY-Graph.BoundsXY.MinY)/FHeig_max_m)*(Site1.GroundHeight+Site1.Antenna2Height);
      eLeftY:=eOtnY;


      FProject.Lines.AddItem(Graph.BoundsXY.MinX, Graph.BoundsXY.MinY, Graph.BoundsXY.MinX, Graph.BoundsXY.MinY+ eOtnY);

      FList.Add(format(DEF_LINE,[Graph.BoundsXY.MinX, Graph.BoundsXY.MinY, Graph.BoundsXY.MinX, Graph.BoundsXY.MinY+ eOtnY]));
      //�������
      y_dugi0:=(Graph.BoundsXY.MinY+eOtnY);
      x_dugi0:=(Graph.BoundsXY.Minx+eOtnX);

      // ---------------------------------------------------------------
      oPolyline := FProject.PolyLines.AddItem;


      FList.Add(DEF_POLYLINE);
      for i:=0 to r_dugi+1 -1 do
      Begin
       y_dugi:=(y_dugi0+i);
       x_dugi:=(x_dugi0)- Sqrt(Abs(Sqr(r_dugi)-sqr(y_dugi-(y_dugi0))));
       FList.Add(format(DEF_VERTEX,[x_dugi,y_dugi]));

       oPolyline.Vertexes.AddItem(x_dugi,y_dugi);
      End;
      FList.Add(DEF_POLYLINE_END);

      // ---------------------------------------------------------------
      oPolyline := FProject.PolyLines.AddItem;


      FList.Add(DEF_POLYLINE);
      for i:=0 to r_dugi+1 -1 do
      Begin
       y_dugi:=(y_dugi0-i);
       x_dugi:=(x_dugi0)- Sqrt(Abs(Sqr(r_dugi)-sqr(y_dugi-(y_dugi0))));
       FList.Add(format(DEF_VERTEX,[x_dugi,y_dugi]));

       oPolyline.Vertexes.AddItem(x_dugi,y_dugi);
      End;
      FList.Add(DEF_POLYLINE_END);

      //----
      //��������� �����

      FProject.Lines.AddItem(Graph.BoundsXY.MinX, Graph.BoundsXY.MinY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY);
      FProject.Lines.AddItem(Graph.BoundsXY.MinX, Graph.BoundsXY.MinY+ eLeftY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY+ eRightY);


      FList.Add(format(DEF_LINE,[Graph.BoundsXY.MinX, Graph.BoundsXY.MinY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY]));
      FList.Add(format(DEF_LINE,[Graph.BoundsXY.MinX, Graph.BoundsXY.MinY+ eLeftY, Graph.BoundsXY.MaxX, Graph.BoundsXY.MinY+ eRightY]));
      //------
    end;

    // ---------------------------------------------------------------
    DrawRelief();
    DrawFrenel();



    //������� �������
    FProject.Texts.AddItem(Graph.MainBounds.x+Graph.AddressXY.x,Graph.MainBounds.y+Graph.AddressXY.y,Site1.Address,3.0,0.0);
    FProject.Texts.AddItem(Graph.MainBounds.x+Graph.Address2XY.x,Graph.MainBounds.y+Graph.Address2XY.y,Site2.Address,3.0,0.0);


    FList.Add(format(DEF_TEXT,[Graph.MainBounds.x+Graph.AddressXY.x,Graph.MainBounds.y+Graph.AddressXY.y,Site1.Address,3.0,0.0]));
    FList.Add(format(DEF_TEXT,[Graph.MainBounds.x+Graph.Address2XY.x,Graph.MainBounds.y+Graph.Address2XY.y,Site2.Address,3.0,0.0]));
    //-------

    DrawAzimuth();


    //���������� ����� �������
    //����������
    FProject.Texts.AddItem(Graph.MainBounds.x+332 ,Graph.MainBounds.y+53 ,Ramka.masthead,5.0,0.0);
    FProject.Texts.AddItem(Graph.MainBounds.x+312 ,Graph.MainBounds.y+41 ,Ramka.name,5.0,0.0);
    FProject.Texts.AddItem(Graph.MainBounds.x+312 ,Graph.MainBounds.y+26 ,Ramka.appointment,3.0,0.0);
    FProject.Texts.AddItem(Graph.MainBounds.x+302 ,Graph.MainBounds.y+9  ,Ramka.location,3.0,0.0);


    FList.Add(format(DEF_TEXT,[Graph.MainBounds.x+332 ,Graph.MainBounds.y+53 ,Ramka.masthead,5.0,0.0]));
    FList.Add(format(DEF_TEXT,[Graph.MainBounds.x+312 ,Graph.MainBounds.y+41 ,Ramka.name,5.0,0.0]));
    FList.Add(format(DEF_TEXT,[Graph.MainBounds.x+312 ,Graph.MainBounds.y+26 ,Ramka.appointment,3.0,0.0]));
    FList.Add(format(DEF_TEXT,[Graph.MainBounds.x+302 ,Graph.MainBounds.y+9  ,Ramka.location,3.0,0.0]));


    //---------
  end;
  FList.Add(DEF_END);

  FList.SaveToFile(aFileName);


  // ShellExecute(0, 'open', 'notepad.exe', PChar(aFileName), nil, SW_SHOWNORMAL);


//                              FList.LoadFromFile(Template_filename);


///////
 /////// FProject.SaveToAcad( Config.Template_filename, 's:\acad.dxf');


end;


begin
  DecimalSeparator:='.';


end.

{

//------------------------------------------------------
function geo_GetEarthHeight___(aOffset_KM, aDistance_KM: double; aRefraction:
    double=1.33): double;
//------------------------------------------------------
var fRadiusZem: double; //const Radius_Zem=8500; //yeaea.?aaeon Caiee (ei)
begin
  Result := geo_GetEarthHeight(aOffset_KM, aDistance_KM, aRefraction);

(*  Exit;



  Assert(aOffset_KM >=0, 'aOffset_KM >=0');
  Assert(aDistance_KM >=0,'aDistance_KM >=0');

  // iia?aoiinou
  if Abs(aOffset_KM - aDistance_KM) < 0.00000001 then
    aOffset_KM := aDistance_KM;

  Assert(aOffset_KM <= aDistance_KM,
    Format('aOffset_KM <= aDistance_KM : %1.10f-%1.10f',[aOffset_KM, aDistance_KM]));


  if aRefraction=0 then
    aRefraction:=1.33;

  fRadiusZem:=12.740*aRefraction;  // eiyoo.?ao?aeoee
  Result:=aOffset_KM*(aDistance_KM-aOffset_KM) / (fRadiusZem); {aiaaaea ia e?eaecio Caiee (i)}
*)
end;


