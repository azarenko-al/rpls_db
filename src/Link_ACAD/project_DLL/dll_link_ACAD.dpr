library dll_link_ACAD;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  Classes,
  dm_Link_ACAD in '..\src\dm_Link_ACAD.pas' {dmLink_ACAD: TDataModule},
  dm_Main_app_acad in '..\src\dm_Main_app_acad.pas' {dmMain_link_ACAD: TDataModule},
  i_link_ACAD in '..\src_shared\i_link_ACAD.pas',
  SysUtils,
  u_acad_params in '..\src\u_acad_params.pas',
  x_Link_ACAD in '..\src_dll\x_link_ACAD.pas';

{$R *.res}

begin
end.
