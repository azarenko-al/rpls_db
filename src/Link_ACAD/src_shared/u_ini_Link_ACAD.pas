unit u_ini_Link_ACAD;

interface
uses
  SysUtils, IniFiles, 

  u_classes;


const
   PROG_Link_ACAD_exe = 'link_ACAD.exe';

type


  // ---------------------------------------------------------------
  TIni_Link_ACAD_params = class(TObject)
  // ---------------------------------------------------------------
  private

  public
    link_ID: Integer;
    IsAuto: Boolean;


    IsTest : boolean;


    procedure SaveToIni(aFileName: String);
    procedure LoadFromIni(aFileName: String);
  end;

var
  g_Ini_Link_ACAD_params: TIni_Link_ACAD_params;


implementation


// ---------------------------------------------------------------
procedure TIni_Link_ACAD_params.LoadFromIni(aFileName: String);
// ---------------------------------------------------------------
var
  oIni: TMemIniFile;
  s: string;
begin
  aFileName:= ChangeFileExt(aFileName, '.ini');


  oIni:=TMemIniFile.Create (aFileName);

  // -------------------------
  link_ID:=oIni.ReadInteger('main', 'link_ID',  0);
  IsAuto   :=oIni.ReadBool   ('main', 'IsAuto', False);

  IsTest := oIni.ReadBool   ('main', 'IsTest', False);
              

  oIni.Free;
  // -------------------------

end;


// ---------------------------------------------------------------
procedure TIni_Link_ACAD_params.SaveToIni(aFileName: String);
// ---------------------------------------------------------------
var
  oIni: TIniFile;
  s: string;
begin
  aFileName:= ChangeFileExt(aFileName, '.ini');

  DeleteFile(aFileName);

  ForceDirectories( ExtractFileDir(aFileName) );


  oIni:=TIniFile.Create (aFileName);

//  oIni.EraseSection('main');

  oIni.WriteInteger('main', 'link_ID', link_ID);

  oIni.WriteBool   ('main', 'IsTest', IsTest);

 // oIni.UpdateFile;
  oIni.Free;

end;




initialization
  g_Ini_Link_ACAD_params:=TIni_Link_ACAD_params.Create;

finalization
  FreeAndNil(g_Ini_Link_ACAD_params);


end.



