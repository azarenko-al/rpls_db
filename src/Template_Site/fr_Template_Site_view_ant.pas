unit fr_Template_Site_view_ant;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  cxTLData, cxPropertiesStore, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, ADODB,
  cxVGrid, cxDBVGrid, DB, Menus, ActnList, cxControls,

  ComCtrls, cxInplaceContainer, ExtCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,

  Dialogs,

   ToolWin,

  dm_Main,

 // dm_User_Security,


 // I_Act_Explorer, //dm_Act_Explorer,
 // I_Act_LinkEnd,  //dm_Act_LinkEnd,
 // I_Act_Antenna, //dm_Act_Antenna,

  u_storage,

  dm_Onega_DB_data,

 // f_Custom,
  //I_core,

  u_vars,


  u_Func_arrays,
  u_func,
  u_Geo,
  u_dlg,
  u_db,
  u_cx,
  u_Log,

  u_func_msg,
  u_const_msg,

  u_const,
  u_const_str,
  u_const_db,

  u_types,



  cxEdit, cxDBData, cxButtonEdit, cxCurrencyEdit, StdCtrls,
  cxGridBandedTableView, cxGridDBBandedTableView

  ;

type
  Tframe_Template_Site_view_ant = class(TForm)
    ActionList1: TActionList;
    act_Antenna_Edit: TAction;
    PopupMenu1: TPopupMenu;
    act_Antenna_New: TAction;
    N2: TMenuItem;
    actAntennaNew1: TMenuItem;
    ds_qry_Antennas: TDataSource;
    act_Antenna_Del: TAction;
    N3: TMenuItem;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    ADOConnection1: TADOConnection;
    qry_Ant: TADOQuery;
    StatusBar1: TStatusBar;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1antennaType_name: TcxGridDBColumn;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    cxGrid1DBTableView1band: TcxGridDBColumn;
    cxGrid1DBTableView1freq_MHz: TcxGridDBColumn;
    cxGrid1DBTableView1gain: TcxGridDBColumn;
    cxGrid1DBTableView1diameter: TcxGridDBColumn;
    cxGrid1DBTableView1polarization_str: TcxGridDBColumn;
    cxGrid1DBTableView1vert_width: TcxGridDBColumn;
    cxGrid1DBTableView1horz_width: TcxGridDBColumn;
    cxGrid1DBTableView1azimuth: TcxGridDBColumn;
    cxGrid1DBTableView1mast_index: TcxGridDBColumn;
    cxGrid1DBTableView1Template_Site_LinkEnd_name: TcxGridDBColumn;


    procedure FormCreate(Sender: TObject);
//    procedure act_Antenna_EditExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
//    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//    procedure ADOStoredProc1111AfterPost(DataSet: TDataSet);
//    procedure Button1Click(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
//    procedure Button3Click(Sender: TObject);
 //   procedure col___AntType_NamePropertiesButtonClick(Sender: TObject;
  //    AButtonIndex: Integer);
//    procedure col___AntType_NamePropertiesButtonClick(Sender: TObject;
  //    AButtonIndex: Integer);
//    procedure cxGrid1Exit(Sender: TObject);
//    procedure cxDBVerticalGrid1antennaType_nameEditPropertiesButtonClick(
  //    Sender: TObject; AButtonIndex: Integer);
  private
 //   FID: integer;

  //  FDataSet: TDataSet;


///    procedure Dlg_Select_AntType;
 //   FObjName: string;


 //   FReadOnly : Boolean;

 //   procedure Dlg_Select_AntType;
  public
 //   Template_Site_ID : integer;

//    procedure SetReadOnly(Value: Boolean);
// TODO: OpenAntennas
//  procedure OpenAntennas;
    procedure View(aTemplate_Site_id: integer; aMast_index: integer = 0);

 //   procedure View_mast(aTemplate_Site_mast_ID: integer);

  //  procedure Set_Template_Site_ID(aTemplate_Site_mast_ID: integer);

  end;

//====================================================================
//====================================================================
implementation
 {$R *.dfm}



//
//  FReadOnly := dmUser_Security.Project_is_ReadOnly;
//
//
//  ADOStoredProc1.LockType := IIF(FReadOnly, ltReadOnly, ltBatchOptimistic);
//  SetFormActionsEnabled(Self, not FReadOnly);
//
//
//  cxGrid1DBTableView1.OptionsData.Editing := not FReadOnly;
//



//--------------------------------------------------------------------
procedure Tframe_Template_Site_view_ant.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;



//  if ADOConnection1.Connected then
  //  ShowMessage(' Tframe_Template_Site_LinkEnd_antennas.FormCreate  -  Connected ');



//  FRegPath :=FRegPath + '1';


  //Assert(Assigned(IAntenna));
 // Assert(Assigned(ILinkEnd));
//  Assert(Assigned(IShell));

/////////////  FDataSet:=mem_Data1;
 // FDataSet:=ADOStoredProc1;//  qry_Antennas;
//  FDataSet:=qry_Ant;//  qry_Antennas;

//  Assert(Assigned(IMapEngine));

 ///////////
 ////////// cxDBTreeList1.Align:=alClient;
//  FUpdated:=false;

 // act_Setup_Dlg.Caption:= STR_SETUP_TABLE;
//..  act_Antenna_Edit.Caption:='��������';

  //GSPages.Align:=alClient;
  cxGrid1.Align:=alClient;

(*
  col_ant_Azimuth.Caption   :=STR_AZIMUTH;
  col_ant_Diameter.Caption  :=STR_DIAMETER;
  col_ant_Gain.Caption      :=STR_GAIN; // '�� [dB]';
  col_ant_Height.Caption    :=STR_HEIGHT;
  col_AntType_Name.Caption  :=STR_TYPE;
  col_vert_width.Caption    :='������ ��� (V)';
  col_horz_width.Caption    :='������ ��� (H)';
  col_Loss.Caption          :=STR_LOSS_dB;
  col_Name.Caption          :=STR_NAME;
  col_Polar.Caption         :=STR_Polarization;
  col_Pos.Caption           :=STR_COORDS;
  col_LOCATION_TYPE.Caption :=STR_ANTENNA_Location_type;
*)


  // -------------------------------------------------------------------
//  cxGrid1DBTableView1.RestoreFromRegistry(FRegPath + cxGrid1DBTableView1.Name);

  g_Storage.RestoreFromRegistry(cxGrid1DBTableView1, className);

  cxGrid1DBTableView1.OptionsView.GroupbyBox:=False;


{
  col__Azimuth.Caption.Text      :=STR_AZIMUTH;
  col__Diameter.Caption.Text     :=STR_DIAMETER;
  col__Gain.Caption.Text         :=STR_GAIN; // '�� [dB]';
  col__Height.Caption.Text       :=STR_HEIGHT;
  col__AntType_Name.Caption.Text :=STR_TYPE;
  col__horz_width.Caption.Text   :='������ ��� (H)';
  col__antenna_loss.Caption.Text :=STR_LOSS_dB;
  col__Name.Caption.Text         :=STR_NAME;
  col__Polarization.Caption.Text :=STR_Polarization;
  col__Pos.Caption.Text          :=STR_COORDS;
  col__vert_width.Caption.Text   :='������ ��� (V)';
  col__LOCATION_TYPE.Caption.Text:=STR_ANTENNA_Location_type;
}
(*
  //------------------------------------------------------
  col___Name.Caption         :=STR_NAME;
  col___Azimuth.Caption      :=STR_AZIMUTH;
  col___Diameter.Caption     :=STR_DIAMETER;
  col___Gain.Caption         :=STR_GAIN; // '�� [dB]';
  col___Height.Caption       :=STR_HEIGHT;
  col___AntType_Name.Caption :=STR_TYPE;
  col___vert_width.Caption   :='������ ��� (V)';
  col___horz_width.Caption   :='������ ��� (H)';
  col___antenna_loss.Caption :=STR_LOSS_dB;
  col___Polarization.Caption :=STR_Polarization;
 // col___Pos.Caption.Text          :=STR_COORDS;
 // col___LOCATION_TYPE.Caption.Text:=STR_ANTENNA_Location_type;
*)

  // -------------------------------------------------------------------


 // col_AntType_Name.FieldName:=FLD_ANTENNATYPE_NAME;
//  col___AntType_Name.DataBinding.FieldName:=FLD_ANTENNATYPE_NAME;

//  col_LOCATION_TYPE.FieldName:=FLD_LOCATION_TYPE;

///  col__LOCATION_TYPE.DataBinding.FieldName:=FLD_LOCATION_TYPE;


 //zzzzzzzzzzz dmLinkEnd_View.InitDB_Antennas (mem_Data1);

//  dxDBTree.LoadFromRegistry (FRegPath+ dxDBTree.name);


//  col___Pos.Visible := False;

//  dx_CheckColumnSizes_DBTreeList (dxDBTree);


  cx_SetColumnCaptions(cxGrid1DBTableView1,
    [
      FLD_Template_Site_LinkEnd_name, STR_Template_Site_LinkEnd_name,

      FLD_Name,          STR_NAME,
      FLD_Azimuth,       STR_AZIMUTH,
      FLD_Height,        STR_HEIGHT,


      FLD_Diameter,      STR_DIAMETER,
      FLD_TILT,          STR_TILT, // '�� [dB]',
      FLD_Gain,          STR_GAIN, // '�� [dB]',

//      FLD_Height_MAX,    STR_HEIGHT_MAX,

      FLD_ANTENNATYPE_NAME, STR_TYPE,

      FLD_vert_width,    '������ ��� (V)',
      FLD_horz_width,    '������ ��� (H)',

      FLD_loss,        STR_LOSS,

      FLD_freq_MHz,    STR_freq_MHz,


      FLD_MAST_INDEX,    STR_MAST_INDEX,


    //  SArr(FLD_antenna_loss,  STR_LOSS),
      FLD_Polarization_STR,  STR_Polarization,
      FLD_BAND,              STR_BAND

//      FLD_ANTENNATYPE_NAME, STR_TYPE,


    ]);


end;


procedure Tframe_Template_Site_view_ant.FormDestroy(Sender: TObject);
begin
//  dxDBTree.SaveToRegistry (FRegPath+ dxDBTree.name);
//  cxGrid1DBTableView1.StoreToRegistry(FRegPath + cxGrid1DBTableView1.Name);

  g_Storage.StoreToRegistry(cxGrid1DBTableView1, className);


  inherited;
end;



// ---------------------------------------------------------------
procedure Tframe_Template_Site_view_ant.View(aTemplate_Site_id: integer;
    aMast_index: integer = 0);
// ---------------------------------------------------------------
begin
  StatusBar1.Visible:= aMast_index>0;


  if aMast_index>0 then
  begin
    StatusBar1.SimpleText:= Format('�����: %d',[aMast_index]);

    dmOnega_DB_data.OpenQuery_(qry_Ant,
       'SELECT  *  FROM  '+ view_Template_Site_LinkEnd_antenna +' WHERE Template_Site_id=:id and mast_index=:mast_index',
        [
         FLD_ID,  aTemplate_Site_id,
         FLD_mast_index, aMast_index
        ])

  end
  else
    dmOnega_DB_data.OpenQuery_(qry_Ant,
       'SELECT  *  FROM  '+ view_Template_Site_LinkEnd_antenna +' WHERE Template_Site_id=:id',

        [FLD_ID,  aTemplate_Site_id ]);


  cxGrid1DBTableView1.ViewData.Expand(True);

end;



end.


