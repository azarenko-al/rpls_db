object frame_Template_Site_LinkEnd_antennas: Tframe_Template_Site_LinkEnd_antennas
  Left = 1072
  Top = 725
  Width = 1280
  Height = 372
  Caption = 'frame_Template_Site_LinkEnd_antennas'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 1264
    Height = 103
    Align = alTop
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      PopupMenu = PopupMenu1
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = ds_qry_Antennas
      DataController.KeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      OptionsView.HeaderAutoHeight = True
      OptionsView.Indicator = True
      object col_ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Visible = False
        Options.Editing = False
        Width = 71
      end
      object col___name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Width = 167
      end
      object col___AntType_Name: TcxGridDBColumn
        DataBinding.FieldName = 'ANTENNATYPE_NAME'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.AutoSelect = False
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.ReadOnly = True
        Properties.OnButtonClick = col___AntType_NamePropertiesButtonClick
        Width = 156
      end
      object col___height: TcxGridDBColumn
        DataBinding.FieldName = 'height'
        Width = 67
      end
      object col___Diameter: TcxGridDBColumn
        DataBinding.FieldName = 'Diameter'
        Options.Editing = False
        Width = 55
      end
      object col___antenna_loss: TcxGridDBColumn
        DataBinding.FieldName = 'loss'
        Options.Editing = False
        Width = 59
      end
      object col___Gain: TcxGridDBColumn
        DataBinding.FieldName = 'Gain'
        Options.Editing = False
        Width = 54
      end
      object col___Polarization_STR: TcxGridDBColumn
        DataBinding.FieldName = 'Polarization_STR'
        Options.Editing = False
        Width = 84
      end
      object col___vert_width: TcxGridDBColumn
        DataBinding.FieldName = 'vert_width'
        Options.Editing = False
      end
      object col___horz_width: TcxGridDBColumn
        DataBinding.FieldName = 'horz_width'
        Options.Editing = False
        Width = 75
      end
      object col_Band: TcxGridDBColumn
        Caption = #1044#1080#1072#1087#1072#1079#1086#1085
        DataBinding.FieldName = 'Band'
        Options.Editing = False
        Width = 72
      end
      object col_azimuth: TcxGridDBColumn
        DataBinding.FieldName = 'azimuth'
        Width = 59
      end
      object col_Mast_index: TcxGridDBColumn
        DataBinding.FieldName = 'mast_index'
        Width = 124
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 116
    Top = 204
    object act_Antenna_Edit: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      OnExecute = act_Antenna_EditExecute
    end
    object act_Antenna_New: TAction
      Caption = 'act_Antenna_New'
      OnExecute = act_Antenna_EditExecute
    end
    object act_Antenna_Del: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = act_Antenna_EditExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 48
    Top = 204
    object actAntennaNew1: TMenuItem
      Action = act_Antenna_New
    end
    object N2: TMenuItem
      Caption = '-'
      Visible = False
    end
    object N3: TMenuItem
      Action = act_Antenna_Del
    end
  end
  object ds_qry_Antennas: TDataSource
    DataSet = qry_Ant
    Left = 277
    Top = 268
  end
  object ADOConnection1: TADOConnection
    CommandTimeout = 60
    ConnectionString = 
      'Provider=MSDASQL.1;Password=sa;Persist Security Info=True;User I' +
      'D=sa;Data Source=onega_link'
    ConnectionTimeout = 30
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 64
    Top = 272
  end
  object qry_Ant: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    AfterPost = ADOStoredProc1111AfterPost
    Parameters = <>
    SQL.Strings = (
      'SELECT  *  FROM lib.view_Template_Site_LinkEnd_antenna')
    Left = 280
    Top = 200
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    NotDocking = [dsNone]
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 408
    Top = 200
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 733
      FloatTop = 876
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton2'
        end>
      NotDocking = [dsNone, dsTop, dsRight, dsBottom]
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarButton1: TdxBarButton
      Action = act_Antenna_Edit
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = act_Antenna_Del
      Category = 0
    end
  end
end
