unit fr_Template_Site_Linkend_inspector;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, AppEvnts, ImgList, ActnList, ExtCtrls,


  u_const_db,
  u_link_const,




   dm_LinkEnd,
//  dm_Link,
  dm_LinkEndType,
//  dm_LinkEnd_View,
  dm_Combiner,



  d_LinkEndType_Mode,

  fr_View_base,

  u_func,
  u_const_str,

  u_types,


  cxPropertiesStore ,

  fr_DBInspector_Container;

type

//  Tframe_LinkEnd_inspector = class(Tframe_DBInspector_Container)


  Tframe_Template_Site_linkend_inspector = class(Tframe_DBInspector_Container)
    procedure FormCreate(Sender: TObject);
 //   procedure FormDestroy(Sender: TObject);
  private
  //  Finspector: Tframe_DBInspector_Container;

    function Dlg_UpdateMode: Boolean;

    procedure DoOnButtonFieldClick(Sender: TObject; aFieldName: string; aNewValue:
        Variant); override;

    procedure DoOnFieldChanged(Sender: TObject; aFieldName: string; aNewValue:
        Variant); override;

    procedure UpdatePassiveElement(aNewValue: Variant);

  protected

  public
//    procedure View(aID: integer);
  end;


//===============================================================
implementation{$R *.dfm}
//===============================================================

//--------------------------------------------------------------------
procedure Tframe_Template_Site_linkend_inspector.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;


 //  Assert(Assigned (g_Obj.ItemByName[OBJ_TEMPLATE_SITE_LINKEND]));



  ObjectName:=OBJ_TEMPLATE_SITE_LINKEND;

  //CreateChildForm(Tframe_DBInspector_Container, Finspector, pn_Main);
  PrepareViewForObject (OBJ_TEMPLATE_SITE_LINKEND);

  //  PrepareViewForObject (aObjName);


//  Finspector.


end;

//-------------------------------------------------------------------
procedure Tframe_Template_Site_linkend_inspector.DoOnFieldChanged(Sender:
    TObject; aFieldName: string; aNewValue: Variant);
//-------------------------------------------------------------------
var
 // dPowerLoss,dPowerMax,dPower,dAllowedPowerLoss: double;

//  iValue: integer;
//  dPassiveElementLoss, dLoss: double;

//  sProtectionType: string;
 // iLinkEndTypeID: integer;
  iID: integer;

begin
  if aFieldName='' then
    Exit;


  //------------------------------
  if Eq(aFieldName, FLD_PASSIVE_ELEMENT_ID) then
  begin
    iID :=AsInteger(aNewValue);
    UpdatePassiveElement(iID);
  end else

//------------------------------
  if Eq(aFieldName, FLD_LINKENDTYPE_ID) then
  //------------------------------
  begin
    Dlg_UpdateMode();
  end;

{    if aNewValue = 0 then
    begin
      SetFieldValue(FLD_EQUIPMENT_TYPE, DEF_EQUIPMENT_NON_STANDARD);

//      SetFieldValue (FLD_LINKENDTYPE_BAND_ID, null);
    end
    else
      SetFieldValue(FLD_EQUIPMENT_TYPE, DEF_EQUIPMENT_STANDARD);

    if aNewValue>0 then
    begin
      UpdateLinkEndTypeInfo(aNewValue);
      Dlg_UpdateMode();
    end;
}

end;



//-------------------------------------------------------------------
procedure Tframe_Template_Site_linkend_inspector.UpdatePassiveElement(
    aNewValue: Variant);
//-------------------------------------------------------------------
var
  dPassiveElementLoss: double;
//  dPassiveElementLoss, dPassiveElementLoss1, dLoss: double;
begin
//  if AsInteger(aNewValue) > 0 then
//  begin
  dPassiveElementLoss:= dmCombiner.GetLoss(aNewValue);

  SetFieldValue (FLD_PASSIVE_ELEMENT_LOSS, dPassiveElementLoss);

end;


//-------------------------------------------------------------------
procedure Tframe_Template_Site_linkend_inspector.DoOnButtonFieldClick(Sender:
    TObject; aFieldName: string; aNewValue: Variant);
//-------------------------------------------------------------------
begin

  if Eq(aFieldName, FLD_MODE) then
    Dlg_UpdateMode
  else





{
  //------------------------------
  if Eq(aFieldName, FLD_CHANNEL_NUMBER) or
     Eq(aFieldName, FLD_TX_FREQ_MHz) or
     Eq(aFieldName, FLD_RX_FREQ_MHz) or
     Eq(aFieldName, FLD_CHANNEL_TYPE) or
     Eq(aFieldName, FLD_SUBBAND)
  then begin
    Dlg_GetBand;
  end;
  }

end;

{
//--------------------------------------------------------------------
procedure Tframe_Template_Site_linkend_inspector.View(aID: integer);
//--------------------------------------------------------------------
begin
  inherited;

//  Fframe_LinkEnd_Inspector.View (aID, OBJ_LINKEND);


 // Finspector.
  View (aID);
end;
}


//-------------------------------------------------------------------
function Tframe_Template_Site_linkend_inspector.Dlg_UpdateMode: Boolean;
//-------------------------------------------------------------------
var
  iID: Integer;
  rec: TdmLinkEndTypeModeInfoRec;

  iLinkEndType_Mode_ID: integer;
  iLinkEndTypeID: Integer;

begin
  iLinkEndType_Mode_ID:=GetIntFieldValue (FLD_LinkEndType_Mode_ID);
  iLinkEndTypeID      :=GetIntFieldValue (FLD_LINKENDTYPE_ID);

  Assert(iLinkEndTypeID>0);

  iID:= Tdlg_LinkEndType_Mode.Dlg_GetMode1 (iLinkEndTypeID, iLinkEndType_Mode_ID, rec);

  if iID>0 then
  begin
     SetFieldValue (FLD_MODE,                rec.Mode);
     SetFieldValue (FLD_LinkEndType_Mode_ID, rec.ID);


{
     SetFieldValue (FLD_BitRate_Mbps,        rec.BitRate_Mbps_);
     SetFieldValue (FLD_MODULATION_TYPE,     rec.MODULATION_TYPE);
     SetFieldValue (FLD_SIGNATURE_HEIGHT,    rec.SIGNATURE_HEIGHT);
     SetFieldValue (FLD_SIGNATURE_WIDTH,     rec.SIGNATURE_WIDTH);
     SetFieldValue (FLD_THRESHOLD_BER_3,     rec.THRESHOLD_BER_3);
     SetFieldValue (FLD_THRESHOLD_BER_6,     rec.THRESHOLD_BER_6);

//     SetFieldValue (FLD_SUBBAND, null);
//     SetFieldValue (FLD_CHAnnel_number,     null);


     SetFieldValue (FLD_channel_width_MHz,   rec.bandwidth_MHz);


     SetFieldValue (FLD_POWER_dBm,           rec.Power_max_dBm);
     SetFieldValue (FLD_POWER_MAX,           rec.Power_max_dBm);
     SetFieldValue (FLD_POWER_LOSS,          0);

     SetFieldValue (FLD_MODULATION_COUNT,      rec.MODULATION_level_COUNT);
     SetFieldValue (FLD_GOST_53363_modulation, rec.GOST_Modulation);

     HideEdit;
}
   //  FIsPostEnabled := True;

  //   UpdateFreqSpacing;

     Result := True;

  end else
     Result := False;
end;





end.

