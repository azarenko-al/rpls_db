inherited frame_Template_Site_linkend_view: Tframe_Template_Site_linkend_view
  Left = 1402
  Top = 240
  Width = 579
  Height = 578
  Caption = 'frame_Template_Site_linkend_view'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 571
  end
  inherited pn_Caption: TPanel
    Width = 571
  end
  inherited pn_Main: TPanel
    Width = 571
    Height = 343
    object cxSplitter1: TcxSplitter
      Left = 1
      Top = 189
      Width = 569
      Height = 8
      HotZoneClassName = 'TcxSimpleStyle'
      AlignSplitter = salBottom
      Control = pn_Antennas
    end
    object pn_Antennas: TPanel
      Left = 1
      Top = 197
      Width = 569
      Height = 145
      Align = alBottom
      BevelOuter = bvLowered
      Caption = 'pn_Antennas'
      TabOrder = 1
    end
    object pn_Inspector: TPanel
      Left = 1
      Top = 1
      Width = 569
      Height = 144
      Align = alTop
      BevelOuter = bvLowered
      Caption = 'pn_Inspector'
      TabOrder = 2
    end
  end
  inherited cxLookAndFeelController1: TcxLookAndFeelController
    Left = 176
    Top = 472
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    Left = 72
    Top = 472
    DockControlHeights = (
      0
      0
      25
      0)
  end
end
