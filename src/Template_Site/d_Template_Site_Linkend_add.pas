unit d_Template_site_linkend_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,

  Variants,

//    Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
//  ActnList,
  cxButtonEdit, //cxGraphics, cxVGrid, cxDropDownEdit,
//  Registry,


  rxPlacemnt, ActnList,     Registry,
  StdCtrls, cxVGrid, cxControls, cxInplaceContainer, ExtCtrls,
  cxPropertiesStore,

  d_Wizard_Add_with_params,

  u_cx_vgrid,

  u_cx_VGrid_export,
                     
  dm_Main,

  dm_Act_LinkEndType,

  dm_Template_Site_Linkend,

 // dm_Folder,
//  dm_Template_Site,

 // u_SiteTemplate_types,

  u_const_str,
  u_const_db,

  u_Geo,
  u_dlg,
  u_reg,
  u_db,
  u_cx,
  u_func,

  //u_cx_vgrid,

  u_types,

  dxSkinsCore, dxSkinsDefaultPainters, cxStyles, cxEdit, cxSpinEdit
  ;


type
  Tdlg_Template_site_linkend_add = class(Tdlg_Wizard_add_with_params)
    cxVerticalGrid1: TcxVerticalGrid;
    row_LinkEndType: TcxEditorRow;
    row_Mode: TcxEditorRow;
    row_Combiner_id: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
//    procedure row_FolderButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure act_OkExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure row_ModeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
   private
    FID: Integer;
    FSiteID: integer;

    FRec: TdmTemplate_Site_Linkend_Rec;


    procedure Append;
  public
    class function ExecDlg(aSiteID: integer): integer;
  end;


//==============================================================================
implementation {$R *.dfm}
//==============================================================================


//------------------------------------------------------------------------------
class function Tdlg_Template_site_linkend_add.ExecDlg(aSiteID: integer):
    integer;
//------------------------------------------------------------------------------
begin
  with Tdlg_Template_site_linkend_add.Create(Application) do
  begin
//    ed_Name_.Text:= dmTemplate_PMP_Site.GetNewName;

    Result:= IIF(ShowModal=mrOk, FID, 0);

    Free;
  end;
end;


//------------------------------------------------------------------------------
procedure Tdlg_Template_site_linkend_add.FormCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;
  SetActionName (STR_DLG_ADD_SITE_linkend_TEMPLATE);
//  row_Folder.Caption:=STR_FOLDER;


  SetDefaultSize();


  with TRegIniFile.Create(FRegPath) do
  begin
    FRec.LinkEndType_ID        := ReadInteger('', FLD_LINKENDTYPE_ID, 0);

    FRec.LinkEndType_Mode_ID   := ReadInteger('', FLD_LinkEndType_Mode_ID, 0);

//
    FRec.Combiner_ID      := ReadInteger('', FLD_Combiner_ID, 0);



//    row_type.Text      := ReadString('', 'BS_TYPE', BS_GSM_TYPE);
  //  row_CalcRadius_km.Properties.Value:= AsFloat(ReadString('', 'CalcRadius_km', '10'));

    Free;
  end;


  row_LinkEndType.Properties.Value  := gl_DB.GetNameByID(TBL_LINKENDTYPE, FRec.LinkEndType_ID);

//  row_Cell_Layer.Properties.Value   := gl_DB.GetNameByID(TBL_CELLLAYER,   FRec.PmpSector.CellLayer_ID);
//  row_Calc_Model.Properties.Value   := gl_DB.GetNameByID(TBL_CALCMODEL,   FRec.PmpSector.CalcModel_ID);




  cx_InitVerticalGrid (cxVerticalGrid1);


  cx_VerticalGrid_LoadFromReg (cxVerticalGrid1, FRegPath);

end;

//------------------------------------------------------------------------------
procedure Tdlg_Template_site_linkend_add.FormDestroy(Sender: TObject);
//------------------------------------------------------------------------------
begin
  cx_VerticalGrid_SaveToReg (cxVerticalGrid1, FRegPath);


  with TRegIniFile.Create(FRegPath) do
  begin
 //   WriteString(Name, 'BS_TYPE', row_type.Text);
 //   WriteString(Name, 'CalcRadius_km', FloatToStr(row_CalcRadius_km.Properties.Value));

    WriteInteger('', FLD_LINKENDTYPE_ID,        FRec.LinkEndType_ID);
//    WriteInteger('', FLD_CELL_LAYER_ID,         FRec.PmpSector.CellLayer_ID);
//    WriteInteger('', FLD_CALC_MODEL_ID,         FRec.PmpSector.CalcModel_ID);

    WriteInteger('', FLD_LinkEndType_Mode_ID,   FRec.LinkEndType_Mode_ID);



    Free;
  end;


{   with TRegIniFile.Create(FRegPath) do
  begin
    //////////
    FRec.Antenna1_.AntennaTypeID:= ReadInteger('', FLD_ANTENNATYPE_ID, 0);


    //////////
    FRec.LinkEndType_ID        := ReadInteger('', FLD_LINKENDTYPE_ID, 0);
    FRec.PmpSector.CellLayer_ID      := ReadInteger('', FLD_CELL_LAYER_ID, 0);
    FRec.PmpSector.CalcModel_ID      := ReadInteger('', FLD_CALC_MODEL_ID, 0);

    FRec.LinkEndType_Mode_ID   := ReadInteger('', FLD_LinkEndType_Mode_ID, 0);
}




  inherited;
end;


//------------------------------------------------------------------------------
procedure Tdlg_Template_site_linkend_add.act_OkExecute(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;
  Append;
end;

//------------------------------------------------------------------------------
procedure Tdlg_Template_site_linkend_add.Append;
//------------------------------------------------------------------------------
var
  rec: TdmTemplate_Site_Linkend_Rec;

begin

  FillChar(rec, SizeOf(rec), 0);

  rec.NewName      := ed_Name_.Text;
 // rec.CalcRadius_km:= AsFloat(row_CalcRadius_km.Properties.Value);

  FID:=dmTemplate_Site_linkend.Add (rec);


end;



// ---------------------------------------------------------------
procedure Tdlg_Template_site_linkend_add.row_ModeEditPropertiesButtonClick( Sender: TObject; AButtonIndex: Integer);
// ---------------------------------------------------------------
begin
  //-------------------------------
  if cxVerticalGrid1.FocusedRow =row_Combiner_id then
  //-------------------------------
    case AButtonIndex of
      0: begin
           if Dlg_SelectObject_cxButtonEdit(Sender as TcxButtonEdit, otCombiner, FRec.Passive_Element_ID) then
           //  if dmAct_LinkEndType.Dlg_GetMode (FRec.Feeder_id, 0, rec) then  //iMode, iLinkEndType_Mode_ID,
             begin
             //  FRec.LinkEndType_mode_ID := rec.ID;

             //  row_Mode.Properties.Value:=IntToStr(rec.Mode);
             end;
         end;
      1: begin
           FRec.Combiner_id:=0;
         //  FRec.LinkEndType_mode_ID:=0;
           row_Combiner_id.Properties.Value:=null;
         end;
    end
  else



  //-------------------------------
  if cxVerticalGrid1.FocusedRow =row_LinkendType then
  //-------------------------------
    case AButtonIndex of
      0: begin
           if Dlg_SelectObject_cxButtonEdit(Sender as TcxButtonEdit, otLinkEndType, FRec.LinkEndType_ID) then
           //  if dmAct_LinkEndType.Dlg_GetMode (FRec.LinkEndType_ID, 0, rec) then  //iMode, iLinkEndType_Mode_ID,
             begin
             //  FRec.LinkEndType_mode_ID := rec.ID;

             //  row_Mode.Properties.Value:=IntToStr(rec.Mode);
             end;
         end;

      1: begin
       //    FRec.LinkEndType_ID:=0;
        //   FRec.LinkEndType_mode_ID:=0;
         //  row_LinkendType.Properties.Value:='';
         end;
    end
  else


  //-------------------------------
  if cxVerticalGrid1.FocusedRow =row_Mode then begin
  //-------------------------------
{
    if dmAct_LinkEndType.Dlg_GetMode (FRec.LinkEndType_ID, FRec.LinkEndType_Mode_ID, rec) then
    begin
      FRec.LinkEndType_mode_ID := rec.ID;

      cx_SetButtonEditText(Sender, IntToStr(rec.Mode));
    end;
}
  end else




end;

end.


{
// ---------------------------------------------------------------
procedure Tdlg_Property_add.row_TemplateEditPropertiesButtonClick( Sender: TObject; AButtonIndex: Integer);
// ---------------------------------------------------------------
begin
  if AButtonIndex=0 then
    if cxVerticalGrid1.FocusedRow=row_Template_site then
      Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otTemplate_Site, FTemplate_Site_ID);

//    Dlg_Apply_template;

  if AButtonIndex=1 then
  begin
    FTemplate_Site_ID:=0;
    row_Template_site.Properties.Value:='';
  end;

end;
