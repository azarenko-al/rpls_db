unit dm_Template_Site_Linkend;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Db, ADODB, Forms, Dialogs,Variants,

  dm_Main,

  i_Shell,

  i_explorer,

//  I_CalcModel,

  dm_Onega_DB_data,

 // u_SiteTemplate_types,

  u_const_str,
  u_const_db,

  u_types,

  u_func,
  u_db,
  u_img,

  dm_Object_base
  ;

type

  TdmTemplate_Site_Linkend_Rec = record

    NewName         : string;

//    Template_pmp_Site_ID : integer;

 //   CellLayer_ID    : integer;
  //  CalcModel_ID    : integer;
    Combiner_ID     : integer;
//    TrxID          : integer;
    LinkEndType_ID : integer;

    LinkEndType_MODE_ID: integer;

    Passive_Element_ID : integer;


  //  CELLID         : integer;

//    Color          : integer;
//    CalcRadius_km  : double;
//    CombinerLoss   : double;

  //  POWER_dBm       : double;
  //  Tx_FREQ_MHz        : double;

  //  SENSE_dBm      : double;    // ������ GSM/PMP
  end;



  TdmTemplate_Site_Linkend = class(TDataModule)
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
  public
    function Del(aID: integer): boolean;

    function Add(aRec: TdmTemplate_Site_Linkend_Rec): integer;

    function Dlg_Add(aTemplate_Site_ID: Integer): Boolean;

    function GetNewName(aSite_ID: integer): string;
  end;


function dmTemplate_Site_Linkend: TdmTemplate_Site_Linkend;


//==============================================================================
implementation {$R *.dfm}
//==============================================================================

var
  FdmTemplate_Site_Linkend : TdmTemplate_Site_Linkend ;


// ---------------------------------------------------------------
function dmTemplate_Site_Linkend: TdmTemplate_Site_Linkend;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmTemplate_Site_Linkend) then
    FdmTemplate_Site_Linkend := TdmTemplate_Site_Linkend.Create(Application);

  Result := FdmTemplate_Site_Linkend;
end;



// ---------------------------------------------------------------
function TdmTemplate_Site_Linkend.Dlg_Add(aTemplate_Site_ID: Integer): Boolean;
// ---------------------------------------------------------------
var
  sGUID: string;
  sName: string;
begin

  sName:='���';

  if InputQuery ('���','��������',sName) then
  begin
    dmOnega_DB_data.ExecCommand_( 'INSERT INTO '+ TBL_TEMPLATE_Site_Linkend +
        ' (name,Template_Site_ID) values (:name,:Template_Site_ID) ',

      [ FLD_name,  sName ,
        FLD_Template_Site_id, aTemplate_Site_ID
      ]);


   // g_ShellEvents.Shell_UpdateNodeChildren_ByGUID (GUID_TEMPLATE_SITE);


//     g_ShellEvents.Shell_UpdateNodeChildren_ByGUID (GUID_TEMPLATE_SITE);

     if Assigned(g_IExplorer_lib) then
     begin
       dmOnega_DB_data.OpenQuery(ADOQuery1,
           'SELECT * FROM '+ TBL_TEMPLATE_Site_Linkend + ' WHERE id=@@IDENTITY ',[]);

       sGUID:=ADOQuery1.FieldValues[FLD_GUID];


       g_IExplorer_lib.UPDATE_NODE_CHILDREN_ByGUID(GUID_TEMPLATE_LINK);
       g_IExplorer_lib.FocuseNodeByGUID( sGUID);

     end;


  end;

  // TODO -cMM: TdmTemplate_Site.Dlg_Add default body inserted
end;


//------------------------------------------------------------------------------
procedure TdmTemplate_Site_Linkend.DataModuleCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;
//  TableName:=TBL_TEMPLATE_Site_LINKEND;
//  DisplayName:=STR_CELL;
end;


// ---------------------------------------------------------------
function TdmTemplate_Site_Linkend.GetNewName(aSite_ID: integer): string;
// ---------------------------------------------------------------
var
  rec: TObject_GetNewName_Params;
begin
  FillChar(rec,SizeOf(rec),0);

  rec.ObjName    := OBJ_TEMPLATE_SITE_LINKEND;
//  rec.Project_ID := dmMain.ProjectID;
  rec.Parent_ID  := aSite_ID;


  Result := dmOnega_DB_data.Object_GetNewName_new
     (rec, OBJ_TEMPLATE_Site_LINKEND, 0, 0, aSite_ID);
end;




//------------------------------------------------------------------------------
function TdmTemplate_Site_Linkend.Add(aRec: TdmTemplate_Site_Linkend_Rec):
    integer;
//------------------------------------------------------------------------------
begin
 {


           FLD_PASSIVE_ELEMENT_ID,  IIF_NULL(aRec.PassiveElement_ID),
//               db_Par(FLD_PASSIVE_ELEMENT_LOSS, dPassiveComponent_Loss,


           FLD_LINKENDTYPE_ID,       aRec.LinkEndType_ID,
           FLD_LinkEndType_MODE_ID,  aRec.LinkEndType_MODE_ID,




  Assert(aRec.Template_pmp_Site_ID>0, 'Value <=0');

   with aRec do

     Result:=dmOnega_DB_data.ExecStoredProc_(SP_TEMPLATE_LINKEND_ADD,
            [
             FLD_OBJNAME,           'pmp_sector',

             FLD_NAME,              NewName,
             FLD_TEMPLATE_pmp_SITE_ID,  Template_pmp_Site_ID,


             FLD_COMBINER_ID,       IIF_NULL(Combiner_ID),
             FLD_COMBINER_LOSS,     CombinerLoss,

             FLD_CALC_MODEL_ID,     IIF_NULL(CalcModel_ID),


             FLD_CELL_LAYER_ID,     IIF_NULL(CellLayer_ID),

             FLD_LINKENDTYPE_ID,    IIF_NULL(LinkEndType_ID),

             FLD_CALC_RADIUS_km,     CalcRadius_km,

             FLD_POWER_dBm,         POWER_dBm,


             FLD_TX_FREQ_MHZ,       tx_FREQ_MHz

             ] );

  Assert(Result>0, 'Value <=0');
  }

end;

// ---------------------------------------------------------------
function TdmTemplate_Site_Linkend.Del(aID: integer): boolean;
// ---------------------------------------------------------------
begin
  dmOnega_DB_data.DeleteRecordByID (TBL_TEMPLATE_Site_Linkend, aID);

  Result := True;
end;




begin
end.