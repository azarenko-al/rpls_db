unit d_Template_Site_Linkend_Antenna_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  cxButtonEdit ,  cxVGrid, cxControls, cxInplaceContainer, ComCtrls, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  cxPropertiesStore,  rxPlacemnt, ActnList,    StdCtrls, ExtCtrls,


  dm_Main,

  d_Wizard_Add_with_params,


  dm_Template_Site_Linkend_Ant,
//  dm_Template_Ant,

  u_Types,
  u_const_db,
  u_const_str,

  u_cx_vgrid,
  u_cx_vgrid_export,


  u_Geo,
  u_db,
  u_reg,
  u_dlg,
//  u_func_msg,
  u_func,


  dxSkinsDefaultPainters, cxStyles, cxEdit, cxTextEdit;


type
  Tdlg_Template_Site_Linkend_Antenna_add = class(Tdlg_Wizard_add_with_params)
    cxVerticalGrid1: TcxVerticalGrid;
    row_Antenna_Model: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    row_Height: TcxEditorRow;
    row_Azimuth: TcxEditorRow;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
//    procedure DoOnButtonClick(Sender: TObject;
 //     AbsoluteIndex: Integer);
    procedure row_Antenna_Model_EditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    rec: TdmTemplate_Site_linkend_Antenna_Rec;


    FID: Integer;
 //   FAntTypeID: Integer;
  //  FTemplateLinkendID: integer;

    procedure Append;
  public
    class function ExecDlg(aTemplate_Linkend_ID: integer): integer;
  end;

//===================================================================
implementation  {$R *.DFM}
//===================================================================

//-------------------------------------------------------------------
class function Tdlg_Template_Site_Linkend_Antenna_add.ExecDlg(aTemplate_Linkend_ID:
    integer): integer;
//-------------------------------------------------------------------
//var iPropID: integer;      dlg_Template_Antenna_add
begin
  with Tdlg_Template_Site_Linkend_Antenna_add.Create(Application) do
  try
    rec.Template_Site_Linkend_ID:=aTemplate_Linkend_ID;

    ed_Name_.Text:='�������';
//    dmTemplate_ant.GetNewName(FTemplateLinkendID);

    if ShowModal=mrOk then
      Result:=FID
    else
      Result:=0;

  finally
    Free;
  end;
end;

//-------------------------------------------------------------------
procedure Tdlg_Template_Site_Linkend_Antenna_add.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  SetActionName(STR_DLG_ADD_ANTENNA);


  rec.AntennaType_ID:=reg_ReadInteger (FRegPath, FLD_ANTENNATYPE_ID, 0);

  row_Antenna_Model.Properties.Value := gl_DB.GetNameByID(TBL_ANTENNATYPE, rec.AntennaType_ID);

//  row_Gain.Caption:=STR_GAIN;
//  row_Diameter.Caption:=STR_Diameter;

 // row_Antenna_Model.OnButtonClick:=DoOnButtonClick;

  SetDefaultSize();

  cx_InitVerticalGrid(cxVerticalGrid1);

  cx_VerticalGrid_LoadFromReg ( cxVerticalGrid1,  FRegPath);

end;


procedure Tdlg_Template_Site_Linkend_Antenna_add.FormDestroy(Sender: TObject);
begin
  reg_WriteInteger(FRegPath, FLD_ANTENNATYPE_ID, rec.AntennaType_ID);

  cx_VerticalGrid_SaveToReg  (cxVerticalGrid1, FRegPath);


  inherited;
end;


procedure Tdlg_Template_Site_Linkend_Antenna_add.act_OkExecute(Sender: TObject);
begin
  inherited;
  Append;
end;

//-------------------------------------------------------------------
procedure Tdlg_Template_Site_Linkend_Antenna_add.Append;
//-------------------------------------------------------------------
//var

begin
//  FillChar(rec, SizeOf(rec), 0);

  rec.NewName        := ed_Name_.Text;
  rec.Height         := AsFloat(row_Height.Properties.Value);
  rec.Azimuth        := AsFloat(row_Azimuth.Properties.Value);


//  rec.Template_Linkend_ID:=FTemplateLinkendID;

  FID:=dmTemplate_Site_Linkend_Antenna.Add (rec);

//' // if Sender=row_Antenna_Model then
  //  Dlg_SelectObject_dx (Sender, otAntennaType, FAntTypeID);

end;


procedure Tdlg_Template_Site_Linkend_Antenna_add.row_Antenna_Model_EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  if cxVerticalGrid1.FocusedRow=row_Antenna_Model then
    Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otAntennaType, rec.AntennaType_ID);
//    Dlg_SelectObject_cx (row_ClutterModel, otClutterModel, FClutterModelID);

end;

end.