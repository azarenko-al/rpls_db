object frame_Template_Site_view_ant: Tframe_Template_Site_view_ant
  Left = 1200
  Top = 426
  Width = 1237
  Height = 553
  Caption = 'frame_Template_Site_view_ant'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 32
    Width = 1229
    Height = 224
    Align = alTop
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = ds_qry_Antennas
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object cxGrid1DBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
      end
      object cxGrid1DBTableView1Template_Site_LinkEnd_name: TcxGridDBColumn
        Caption = #1056#1056#1057
        DataBinding.FieldName = 'Template_Site_LinkEnd_name'
        Visible = False
        GroupIndex = 0
        Width = 200
      end
      object cxGrid1DBTableView1name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Width = 257
      end
      object cxGrid1DBTableView1antennaType_name: TcxGridDBColumn
        DataBinding.FieldName = 'antennaType_name'
        Width = 145
      end
      object cxGrid1DBTableView1band: TcxGridDBColumn
        DataBinding.FieldName = 'band'
        Width = 39
      end
      object cxGrid1DBTableView1freq_MHz: TcxGridDBColumn
        DataBinding.FieldName = 'freq_MHz'
      end
      object cxGrid1DBTableView1gain: TcxGridDBColumn
        DataBinding.FieldName = 'gain'
      end
      object cxGrid1DBTableView1diameter: TcxGridDBColumn
        DataBinding.FieldName = 'diameter'
      end
      object cxGrid1DBTableView1polarization_str: TcxGridDBColumn
        DataBinding.FieldName = 'polarization_str'
        Width = 71
      end
      object cxGrid1DBTableView1vert_width: TcxGridDBColumn
        DataBinding.FieldName = 'vert_width'
      end
      object cxGrid1DBTableView1horz_width: TcxGridDBColumn
        DataBinding.FieldName = 'horz_width'
      end
      object cxGrid1DBTableView1azimuth: TcxGridDBColumn
        DataBinding.FieldName = 'azimuth'
      end
      object cxGrid1DBTableView1mast_index: TcxGridDBColumn
        DataBinding.FieldName = 'mast_index'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 0
    Width = 1229
    Height = 32
    Align = alTop
    Panels = <>
    SimplePanel = True
    SimpleText = #1052#1072#1095#1090#1072' 1'
  end
  object ActionList1: TActionList
    Left = 116
    Top = 356
    object act_Antenna_Edit: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
    end
    object act_Antenna_New: TAction
      Caption = 'act_Antenna_New'
    end
    object act_Antenna_Del: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 48
    Top = 356
    object actAntennaNew1: TMenuItem
      Action = act_Antenna_New
    end
    object N2: TMenuItem
      Caption = '-'
      Visible = False
    end
    object N3: TMenuItem
      Action = act_Antenna_Del
    end
  end
  object ds_qry_Antennas: TDataSource
    DataSet = qry_Ant
    Left = 349
    Top = 428
  end
  object ADOConnection1: TADOConnection
    CommandTimeout = 60
    ConnectionString = 
      'Provider=MSDASQL.1;Password=sa;Persist Security Info=True;User I' +
      'D=sa;Data Source=onega_link'
    ConnectionTimeout = 30
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 208
    Top = 360
  end
  object qry_Ant: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'SELECT  *  FROM lib.view_Template_Site_LinkEnd_antenna')
    Left = 352
    Top = 368
  end
end
