unit dm_Act_Template_Linkend_Antenna;

interface

uses
  Classes, Forms, ActnList, 

  dm_act_Base,

  I_Object,


 // fr_Template_Linkend_Antenna_inspector,
  fr_Template_Site_Linkend_Antenna_view,

  u_Types,

  u_func
  , Menus;

type
  TdmAct_Template_Site_Linkend_Antenna = class(TdmAct_Base)
    procedure act_Object_ShowOnMapExecute(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure DoAction(Sender: TObject);

 //   function ItemDel(aID: integer; aName: string = ''): boolean;

    function ItemDel(aID: integer; aName: string = ''): boolean; override;

  protected
    procedure GetPopupMenu(aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function GetViewForm (aOwnerForm: TForm): TForm; override;

  public
  //  function Dlg_Edit(aID: integer): boolean; //override;

    class procedure Init;
  end;

var
  dmAct_Template_Site_Linkend_Antenna: TdmAct_Template_Site_Linkend_Antenna;

//====================================================================
implementation {$R *.dfm}

procedure TdmAct_Template_Site_Linkend_Antenna.act_Object_ShowOnMapExecute(Sender:
    TObject);
begin

end;


//--------------------------------------------------------------------
class procedure TdmAct_Template_Site_Linkend_Antenna.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_Template_Site_Linkend_Antenna) then
    dmAct_Template_Site_Linkend_Antenna:=TdmAct_Template_Site_Linkend_Antenna.Create(Application);
end;

//--------------------------------------------------------------------
procedure TdmAct_Template_Site_Linkend_Antenna.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_TEMPLATE_Site_LINKEND_Antenna;


  act_Add.Caption:='������� �� �������';
  

 SetActionsExecuteProc ([
          // act_Antenna_Add,
//
//           act_Object_ShowOnMap,
//
//        //   act_MoveToFolder,
//           act_Change_LinkEndType,
//           act_ShowLinkInfo,
//
//           act_Object_dependence,

           act_Copy

//           act_GroupAssign  ,
//           act_Change_AntType

//           act_Audit

         //  act_Copy

         ], DoAction);

end;


// ---------------------------------------------------------------
procedure TdmAct_Template_Site_Linkend_Antenna.DoAction(Sender: TObject);
// ---------------------------------------------------------------
begin

{
  if Sender=act_Audit then
     Dlg_Audit(TBL_LinkEnd, FFocusedID) else
}
  //   Tdlg_Audit.ExecDlg (TBL_LinkEnd, FFocusedID) else


(*  if Sender=act_MoveToFolder then
    raise Exception.Create('')

   // dmAct_Folder.MoveToFolderDlg (ObjectName, FSelectedPIDLs)
  else
*)

//  if Sender=act_Copy then
//    Copy() else


//
//  //---------------------------------
//  if Sender = act_Add_Cell then
//  //---------------------------------
//  begin
//    iID:= Tdlg_Template_Linkend_add.ExecDlg (FSiteTemplateID);
//
//    if iID > 0 then
//      View();
//  end else


  //---------------------------------
  if Sender = act_Add then begin
  //---------------------------------
//    if Tdlg_Template_Antenna_add.ExecDlg(iID)>0 then
 //     View();

  end else


end;



//-----------------------------------------------------------------
function TdmAct_Template_Site_Linkend_Antenna.ItemDel(aID: integer; aName:
    string = ''): boolean;
//--------------------------------------------------------------------
begin
  Result:= false;

//  bHas_children := dmOnega_DB_data.Object_Has_children (OBJ_property, aID);

end;

//--------------------------------------------------------------------
function TdmAct_Template_Site_Linkend_Antenna.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_Template_Site_Linkend_Antenna_View.Create(aOwnerForm);
end;



//--------------------------------------------------------------------
procedure TdmAct_Template_Site_Linkend_Antenna.GetPopupMenu(aPopupMenu: TPopupMenu;
    aMenuType: TMenuType);
//--------------------------------------------------------------------
begin

  case aMenuType of
    mtFolder: begin
              //  AddFolderMenu_Create (aPopupMenu);
               // AddFolderPopupMenu (aPopupMenu);
              end;
    mtItem:   begin
             //   AddMenuItem (aPopupMenu, act_Cell_Add);
             //   AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Del_);

              //  AddFolderMenu_Tools (aPopupMenu);
               /// AddMenuItem (aPopupMenu, act_Export_MDB);

              end;
    mtList:   begin
                AddMenuItem (aPopupMenu, act_Del_list);
              //  AddFolderMenu_Tools (aPopupMenu);
              //  AddMenuItem (aPopupMenu, act_Export_MDB);

              end;
  end;


end;


end.




         (*
//--------------------------------------------------------------------
function TdmAct_Template_Linkend_Antenna.Dlg_Edit(aID: integer): boolean;
//--------------------------------------------------------------------
begin
  Result:=Tframe_Template_Linkend_inspector.ExecDlg (aID);
end;
*)



  //---------------------------------
  if Sender = act_Add_Cell then
  //---------------------------------
  begin
    iID:= Tdlg_Template_Linkend_add.ExecDlg (FSiteTemplateID);

    if iID > 0 then
      View();
  end else

  //---------------------------------
  if Sender = act_Add_Antenna then begin
  //---------------------------------
    if Tdlg_Template_Antenna_add.ExecDlg(iID)>0 then
      View();

  end else






//--------------------------------------------------------------------
procedure TdmAct_Template_Linkend.GetPopupMenu(aPopupMenu: TPopupMenu;
    aMenuType: TMenuType);
//--------------------------------------------------------------------
begin

  case aMenuType of
    mtFolder: begin
               // AddFolderMenu_Create (aPopupMenu);
                //AddFolderPopupMenu (aPopupMenu);


                AddMenuItem (aPopupMenu, act_Add);
              end;
    mtItem:   begin
//                AddMenuItem (aPopupMenu, act_Cell_Add);
 //               AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Del_);

              //  AddFolderMenu_Tools (aPopupMenu);
                AddMenuItem (aPopupMenu, act_Export_MDB);

              end;
    mtList:   begin
                AddMenuItem (aPopupMenu, act_Del_list);
              //  AddFolderMenu_Tools (aPopupMenu);
               // AddMenuItem (aPopupMenu, act_Export_MDB);

              end;
  end;

end;



-----------------------------------------------------------------
  function TdmAct_Property.ItemDel(aID: integer; aName: string = ''): boolean;
//--------------------------------------------------------------------
var
  bHas_children: boolean;
begin
  Result:= false;

  bHas_children := dmOnega_DB_data.Object_Has_children (OBJ_property, aID);


 // bHas_children := dmProperty.IsEmpty(aID);

  if not bHas_children then
    Result:= dmProperty.Del(aID)

  else

//  if ConfirmDlg('������ � ��������� ����� ������� ��� ����������� �� ��� ��������. ������������� ��������? '+ FFocusedName)
//  then

  Result:= dmProperty_tools.Del(aID, FDeletedIDList);


  if Result then
  begin
    dmMapEngine_store.Feature_Del (OBJ_Property, aID);

///    PostEvent (WE_MAP_ENGINE_DEL_OBJECT, Integer(otCalcRegion), aID);

  //  dmAct_Map.MapRefresh;
  end;

end;
