inherited dlg_Template_site_linkend_add: Tdlg_Template_site_linkend_add
  Left = 1468
  Top = 400
  Width = 559
  Height = 429
  Caption = 'dlg_Template_site_linkend_add'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 367
    Width = 551
    inherited Bevel1: TBevel
      Width = 551
    end
    inherited Panel3: TPanel
      Left = 367
    end
  end
  inherited pn_Top_: TPanel
    Width = 551
    inherited Bevel2: TBevel
      Width = 551
    end
    inherited pn_Header: TPanel
      Width = 551
      Height = 55
    end
  end
  inherited Panel1: TPanel
    Width = 551
    inherited ed_Name_: TEdit
      Width = 541
    end
  end
  inherited Panel2: TPanel
    Width = 551
    Height = 184
    inherited PageControl1: TPageControl
      Width = 541
      Height = 140
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 533
          Height = 109
          Align = alClient
          LookAndFeel.Kind = lfOffice11
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.ShowEditButtons = ecsbAlways
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 177
          OptionsView.ValueWidth = 40
          TabOrder = 0
          Version = 1
          object row_LinkEndType: TcxEditorRow
            Expanded = False
            Properties.Caption = #1054#1073#1086#1088#1091#1076#1086#1074#1072#1085#1080#1077
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = row_ModeEditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object row_Mode: TcxEditorRow
            Properties.Caption = #1056#1077#1078#1080#1084
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = row_ModeEditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
          object cxVerticalGrid1CategoryRow1: TcxCategoryRow
            ID = 2
            ParentID = -1
            Index = 2
            Version = 1
          end
          object row_Combiner_id: TcxEditorRow
            Properties.Caption = #1060#1080#1076#1077#1088
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 3
            ParentID = -1
            Index = 3
            Version = 1
          end
        end
      end
    end
  end
end
