unit fr_Template_Site_Linkend_antenna_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, AppEvnts, ImgList, ActnList,  ExtCtrls,

  fr_View_base,

  u_func,
  u_const_str,

  u_types,

  
  fr_DBInspector_Container,


//  fr_Template_Site_LinkEnd_inspector,


  cxPropertiesStore, cxControls, cxSplitter, dxBar, cxBarEditItem,
  cxClasses, cxLookAndFeels ;

//  fr_DBInspector_Container;




type
  Tframe_Template_Site_linkend_antenna_view = class(Tframe_View_Base)
    pn_Inspector: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure pn_InspectorClick(Sender: TObject);
 //   procedure FormDestroy(Sender: TObject);
  private
    Fframe_Inspector: Tframe_DBInspector_Container;

 //   Fframe_Inspector: Tframe_Template_Site_linkend_inspector;
//    Fframe_antennas: Tframe_Template_Site_linkend_antennas;


  public
    procedure View(aID: integer; aGUID: string); override;
  end;


//===============================================================
implementation{$R *.dfm}
//===============================================================

//--------------------------------------------------------------------
procedure Tframe_Template_Site_linkend_antenna_view.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_TEMPLATE_SITE_LINKEND_antenna;
//  ObjectName:=OBJ_TEMPLATE_PMP_SITE;

  pn_Inspector.Align:=alClient;


  CreateChildForm_(Fframe_Inspector, Tframe_DBInspector_Container, pn_Inspector);

//  CreateChildForm_(Fframe_antennas,  Tframe_Template_Site_linkend_antennas, pn_Antennas);

  Fframe_Inspector.PrepareViewForObject ( OBJ_TEMPLATE_SITE_LINKEND_antenna);

//  Finspector.






end;

procedure Tframe_Template_Site_linkend_antenna_view.pn_InspectorClick(Sender:
    TObject);
begin
  inherited;
end;


//--------------------------------------------------------------------
procedure Tframe_Template_Site_linkend_antenna_view.View(aID: integer; aGUID: string);
//--------------------------------------------------------------------
begin
  inherited;

//  Fframe_LinkEnd_Inspector.View (aID, OBJ_LINKEND);


  Fframe_Inspector.View (aID);
//  Fframe_antennas.View (aID);

end;


end.

