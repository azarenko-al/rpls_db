unit dm_Act_Template_Site_linkend;

interface

uses
  Classes, Menus, ActnList, Forms,

  dm_User_Security,

  u_DataExport_run,


  I_Object,
  

  dm_act_Base,

  u_const_msg,
  u_func_msg,

  
  u_func,
  

  u_Types,

  u_const_str,
  u_const_db,


  dm_Template_Site_linkend,

  fr_Template_Site_linkend_view,

//  d_Template_Site_add,
  d_Template_Site_Linkend_antenna_add,
  d_Template_Site_Linkend_add

  ;

type
  TdmAct_Template_Site_Linkend = class(TdmAct_Base)
    act_Cell_Add: TAction;
    ActionList2: TActionList;
    act_Add_antenna: TAction;
    procedure DataModuleCreate(Sender: TObject);
  private
//    FBLPoint: TBLPoint;

function CheckActionsEnable: Boolean;

    procedure DoAction (Sender: TObject);

  protected
    function  GetViewForm (aOwnerForm: TForm): TForm; override;
    procedure GetPopupMenu(aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;

    function ItemDel(aID: integer; aName: string = ''): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;


  public
    procedure Add (aFolderID: integer); override;

    class procedure Init;
  end;


var
  dmAct_Template_Site_Linkend: TdmAct_Template_Site_Linkend;

//==================================================================
implementation {$R *.dfm}
//==================================================================


//--------------------------------------------------------------------
class procedure TdmAct_Template_Site_Linkend.Init;
//--------------------------------------------------------------------
begin
 // Assert(not Assigned(dmAct_Template_Site));

  Application.CreateForm(TdmAct_Template_Site_linkend, dmAct_Template_Site_linkend);

//  dmAct_TemplateSite:=TdmAct_TemplateSite.Create(Application);

end;

//--------------------------------------------------------------------
procedure TdmAct_Template_Site_Linkend.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_TEMPLATE_SITE_Linkend;

  act_Add_antenna.Caption:=STR_ADD_ANTENNA;

//  act_Export_MDB.Caption := DEF_STR_Export_MDB;

  SetActionsExecuteProc
    ([
      act_Add_antenna
  //    act_Export_MDB
     ],

    DoAction);

end;

//--------------------------------------------------------------------
procedure TdmAct_Template_Site_Linkend.Add (aFolderID: integer);
//--------------------------------------------------------------------
begin
  inherited Add (aFolderID);
end;

//--------------------------------------------------------------------
function TdmAct_Template_Site_Linkend.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
//  Result:=Tdlg_Template_Site_linkend_add.ExecDlg (aFolderID);

  Result:=0;
   dmTemplate_Site_linkend.Dlg_Add (aFolderID);



end;

//--------------------------------------------------------------------
function TdmAct_Template_Site_Linkend.ItemDel(aID: integer; aName: string = ''):
    boolean;
//--------------------------------------------------------------------
begin

  Result:=dmTemplate_Site_linkend.Del (aID);

end;

//--------------------------------------------------------------------
function TdmAct_Template_Site_Linkend.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_Template_Site_linkend_View.Create(aOwnerForm);

end;



// ---------------------------------------------------------------
function TdmAct_Template_Site_Linkend.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
  Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];


  SetActionsEnabled1( [

// act_Export_MDB,
//                           act_Import_MDB,

  act_Del_,
  act_Del_list

      ],

   Result );


   //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );


end;


//--------------------------------------------------------------------
procedure TdmAct_Template_Site_Linkend.GetPopupMenu(aPopupMenu: TPopupMenu; aMenuType: TMenuType);
//--------------------------------------------------------------------
var
  bEnable: Boolean;
begin
  bEnable:=CheckActionsEnable();



  case aMenuType of
    mtFolder: begin
                AddFolderMenu_Create (aPopupMenu);
                AddFolderPopupMenu (aPopupMenu);
              end;
    mtItem:   begin
                AddMenuItem (aPopupMenu, act_Add_antenna);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Del_);

            //    AddFolderMenu_Tools (aPopupMenu);
            //    AddMenuItem (aPopupMenu, act_Export_MDB);

              end;
    mtList:   begin
                AddMenuItem (aPopupMenu, act_Del_list);
           //     AddFolderMenu_Tools (aPopupMenu);
            //    AddMenuItem (aPopupMenu, act_Export_MDB);

              end;
  end;
end;


//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_Template_Site_Linkend.DoAction (Sender: TObject);
begin


  if Sender=act_Add_antenna then
  begin
    Tdlg_Template_Site_Linkend_antenna_add.ExecDlg (FFocusedID); //, oBS_Type);
  end;



{
  if Sender=act_Export_MDB then
//    TDataExport_run.ExportToRplsDbGuides1 (OBJ_ANTENNA_TYPE);
    TDataExport_run.ExportToRplsDb_selected (TBL_Template_PMP_Site, FSelectedIDList);



  }


 // PostWMsg (WM_REFRESH_SITE);

end;



end.

{
    g_ShellEvents.Shell_PostDelNode(aGUID);
    g_Shell.Shell_PostDelNode(aGUID);

    g_ShellEvents.Shell_PostDelNodesByIDList(FDeletedIDList);
