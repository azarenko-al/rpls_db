unit dm_Template_Site_linkend_Ant;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms,

  dm_Onega_DB_data,

//  dm_AntType,

  u_const,
  u_const_db,
  u_const_str,

  u_types,
  u_func,
  u_db,
  u_Geo

 // dm_Main

//  dm_Object_base
  ;


type

  //-------------------------------------------------------------------
  TdmTemplate_Site_linkend_Antenna_Rec = record
  //-------------------------------------------------------------------
    NewName          : string;

    Template_Site_Linkend_ID: integer;

    Height        : double;
    Azimuth       : double;
    Loss          : double;
//    Tilt          : double;
    Freq_MHZ      : double;

    Gain          : double;
    Diameter      : double;
    Polarization  : integer;

    Vert_Width    : double;
    Horz_Width    : double;

    AntennaType_ID : Integer;
  end;


  TdmTemplate_Site_Linkend_Antenna = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);

  private
// TODO: GetAntTypeID
//  function GetAntTypeID(aID: integer): integer;

  public
    function Add(aRec: TdmTemplate_Site_linkend_Antenna_Rec): integer;
    function Del(aID: integer): boolean;

//    procedure Del_Antenna (aID: integer);

 //   function  GetNewName (aCellTemplateID: integer): string;

  end;

  //dmTemplate_Site_Linkend_Ant


function dmTemplate_Site_Linkend_Antenna: TdmTemplate_Site_Linkend_Antenna;


//=============================================================
implementation    {$R *.DFM}
//=============================================================

var
  FdmTemplate_Site_Linkend_Ant: TdmTemplate_Site_linkend_Antenna;


// ---------------------------------------------------------------
function dmTemplate_Site_Linkend_Antenna: TdmTemplate_Site_Linkend_Antenna;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmTemplate_Site_Linkend_Ant) then
      FdmTemplate_Site_Linkend_Ant := TdmTemplate_Site_Linkend_Antenna.Create(Application);

    Result := FdmTemplate_Site_Linkend_Ant;
end;

//------------------------------------------------------------------------------
procedure TdmTemplate_Site_Linkend_Antenna.DataModuleCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;

 // TableName   := TBL_TEMPLATE_LINKEND_ANTENNA;
 // ObjectName  := OBJ_TEMPLATE_ANT;
end;

{

//------------------------------------------------------------------------------
function TdmTemplate_Site_Linkend_Ant.GetNewName(aCellTemplateID: integer): string;
//------------------------------------------------------------------------------
begin
  Result := dmOnega_DB_data.Linkend_Antenna_GetNewName (OBJ_TEMPLATE_LINKEND, aCellTemplateID);
end;


//-------------------------------------------------------------------
procedure TdmTemplate_Site_Linkend_Ant.Del_Antenna (aID: integer);
//-------------------------------------------------------------------
begin
  gl_DB.DeleteRecordByID (TBL_TEMPLATE_LINKEND_ANTENNA, aID);
end;
}


function TdmTemplate_Site_Linkend_Antenna.Del(aID: integer): boolean;
begin
  dmOnega_DB_data.DeleteRecordByID (TBL_TEMPLATE_Site_Linkend_antenna, aID);

//  Result := 0;
end;


//-------------------------------------------------------------------
function TdmTemplate_Site_Linkend_Antenna.Add(aRec:
    TdmTemplate_Site_linkend_Antenna_Rec): integer;
//-------------------------------------------------------------------
begin
  Assert(aRec.Template_Site_Linkend_ID>0, 'Value <=0');

//  with aRec do
//    if (Template_Linkend_ID=0)  then
  //    raise Exception.Create('');

  if aRec.NewName='' then
    aRec.NewName:='�������';

  with aRec do
  begin
    Result:=dmOnega_DB_data.ExecStoredProc_(SP_TEMPLATE_SITE_LINKEND_ANTENNA_ADD,

            [
             FLD_NAME, NewName,

             FLD_TEMPLATE_SITE_LINKEND_ID, IIF_NULL(Template_Site_Linkend_ID),

             FLD_ANTENNATYPE_ID,   IIF_NULL(AntennaType_ID),

             FLD_HEIGHT,  Height,
             FLD_GAIN,    GAIN,
             FLD_AZIMUTH, Azimuth,
             FLD_LOSS,    Loss

            ]);


  end;

  Assert(Result>0, 'Value <=0');
end;


end.

