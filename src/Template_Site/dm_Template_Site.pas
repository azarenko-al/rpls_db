unit dm_Template_Site;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Db, ADODB, Forms, Dialogs,Variants,

  dm_Main,


  i_Shell,

  i_Explorer,


//  I_CalcModel,

  dm_Onega_DB_data,

 // u_SiteTemplate_types,

  u_const_str,
  u_const_db,

  u_types,

  u_func,
  u_db,
  u_img,

  dm_Object_base
  ;

type

  TdmTemplate_Site_Rec = record
    NewName         : string;

//    Template_pmp_Site_ID : integer;

 //   CellLayer_ID    : integer;
  //  CalcModel_ID    : integer;
  
  {  Combiner_ID     : integer;
//    TrxID          : integer;
    LinkEndType_ID : integer;
  //  CELLID         : integer;

    Color          : integer;
    CalcRadius_km  : double;
    CombinerLoss   : double;
    }

  //  POWER_dBm       : double;
  //  Tx_FREQ_MHz        : double;

  //  SENSE_dBm      : double;    // ������ GSM/PMP
  end;



  TdmTemplate_Site = class(TDataModule)
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
  public
    procedure Del(aID: integer);

    function Add(Dlg_Add: TdmTemplate_Site_Rec): integer;

    function Dlg_Add: Boolean;
    function GetNewName: string;
  end;


function dmTemplate_Site: TdmTemplate_Site;


//==============================================================================
implementation {$R *.dfm}
//==============================================================================

var
  FdmTemplate_Site : TdmTemplate_Site ;


// ---------------------------------------------------------------
function dmTemplate_Site: TdmTemplate_Site;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmTemplate_Site) then
    FdmTemplate_Site := TdmTemplate_Site.Create(Application);

  Result := FdmTemplate_Site;
end;


//------------------------------------------------------------------------------
procedure TdmTemplate_Site.DataModuleCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;
//  TableName:=TBL_TEMPLATE_Site;

//  DisplayName:=STR_CELL;
end;

// ---------------------------------------------------------------
function TdmTemplate_Site.GetNewName: string;
// ---------------------------------------------------------------
var
  rec: TObject_GetNewName_Params;
begin
  FillChar(rec,SizeOf(rec),0);

  rec.ObjName    := OBJ_TEMPLATE_Link;
//  rec.Project_ID := dmMain.ProjectID;
//  rec.Parent_ID  := aParentID;


  Result := dmOnega_DB_data.Object_GetNewName_new
     (rec, OBJ_TEMPLATE_LINK, 0, 0, 0);
end;



//------------------------------------------------------------------------------
function TdmTemplate_Site.Add(Dlg_Add: TdmTemplate_Site_Rec): integer;
//------------------------------------------------------------------------------
begin
//  dmOnega_DB_data.ExecCommand()


 {
  Assert(Dlg_Add.Template_pmp_Site_ID>0, 'Value <=0');

   with Dlg_Add do

     Result:=dmOnega_DB_data.ExecStoredProc_(SP_TEMPLATE_LINKEND_ADD,
            [
             FLD_OBJNAME,           'pmp_sector',

             FLD_NAME,              NewName,
             FLD_TEMPLATE_pmp_SITE_ID,  Template_pmp_Site_ID,


             FLD_COMBINER_ID,       IIF_NULL(Combiner_ID),
             FLD_COMBINER_LOSS,     CombinerLoss,

             FLD_CALC_MODEL_ID,     IIF_NULL(CalcModel_ID),


             FLD_CELL_LAYER_ID,     IIF_NULL(CellLayer_ID),

             FLD_LINKENDTYPE_ID,    IIF_NULL(LinkEndType_ID),

             FLD_CALC_RADIUS_km,     CalcRadius_km,

             FLD_POWER_dBm,         POWER_dBm,


             FLD_TX_FREQ_MHZ,       tx_FREQ_MHz

             ] );

  Assert(Result>0, 'Value <=0');
  }

end;

// ---------------------------------------------------------------
function TdmTemplate_Site.Dlg_Add: Boolean;
// ---------------------------------------------------------------
var
  iID: Integer;
//  sGUID: string;
  sName: string;
begin

  sName:='������';

  if InputQuery ('������','��������',sName) then
  begin
    iID:=dmOnega_DB_data.ExecStoredProc_('lib.sp_Template_Site_add',  [FLD_name, sName]);

//    dmOnega_DB_data.ExecCommand_('INSERT INTO '+ TBL_Template_Site + ' (name) values (:name) ',
//      [ FLD_name,  sName
//      ]);



//     g_ShellEvents.Shell_UpdateNodeChildren_ByGUID (GUID_TEMPLATE_SITE);

     if Assigned(g_IExplorer_lib) then
     begin
//       dmOnega_DB_data.OpenQuery_(ADOQuery1,
//           'SELECT * FROM '+ TBL_Template_Site + ' WHERE id=@@IDENTITY ',[]);

  //     sGUID:=ADOQuery1.FieldValues[FLD_GUID];



       g_IExplorer_lib.UPDATE_NODE_CHILDREN(GUID_TEMPLATE_LINK);
       g_IExplorer_lib.FocuseNodeByObjName( TBL_Template_Site, iID );
  //     g_IExplorer_lib.FocuseNodeByGUID( sGUID);

     end;

{
    procedure UPDATE_NODE_CHILDREN_by_ObjName( aObjectName: string; aID: integer);
    procedure UPDATE_NODE_CHILDREN( aGUID: string);

    function FocuseNodeByGUID(aGUID: string): Boolean;

    function FocuseNodeByObjName(aObjectName: Widestring; aID: integer): Boolean;
}


  end;

 
end;


procedure TdmTemplate_Site.Del(aID: integer);
begin
  dmOnega_DB_data.DeleteRecordByID (TBL_TEMPLATE_Site, aID);
end;


begin
end.


(*
//------------------------------------------------------------------------------
function TdmTemplate_Linkend.Del (aID: integer): boolean;
//------------------------------------------------------------------------------
begin
 // dmAntTemplate.Cell_Del_Antennas (aID);
  Result:= inherited Del (aID);
end;
*)

