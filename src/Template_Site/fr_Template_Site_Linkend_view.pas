unit fr_Template_Site_Linkend_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, AppEvnts, ImgList, ActnList,
   ExtCtrls,

  fr_View_base,

  u_func,
  u_const_str,

  u_types,


  fr_Template_Site_LinkEnd_inspector,
  fr_Template_Site_LinkEnd_antennas,


  cxPropertiesStore, cxControls, cxSplitter, dxBar, cxBarEditItem,
  cxClasses, cxLookAndFeels ;

//  fr_DBInspector_Container;




type                                                                                           
  Tframe_Template_Site_linkend_view = class(Tframe_View_Base)
    cxSplitter1: TcxSplitter;
    pn_Antennas: TPanel;
    pn_Inspector: TPanel;
    procedure FormCreate(Sender: TObject);
 //   procedure FormDestroy(Sender: TObject);
  private
 //   Finspector: Tframe_DBInspector_Container;

    Fframe_Inspector: Tframe_Template_Site_linkend_inspector;
    Fframe_antennas: Tframe_Template_Site_linkend_antennas;


  public
    procedure View(aID: integer; aGUID: string); override;
  end;


//===============================================================
implementation{$R *.dfm}
//===============================================================

//--------------------------------------------------------------------
procedure Tframe_Template_Site_linkend_view.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_TEMPLATE_SITE_LINKEND;
//  ObjectName:=OBJ_TEMPLATE_PMP_SITE;

  pn_Inspector.Align:=alClient;


  CreateChildForm_(Fframe_Inspector, Tframe_Template_Site_linkend_inspector, pn_Inspector);
  CreateChildForm_(Fframe_antennas,  Tframe_Template_Site_linkend_antennas, pn_Antennas);

//  Fframe_Inspector.PrepareViewForObject ( OBJ_TEMPLATE_SITE_LINKEND);

//  Finspector.


end;


//--------------------------------------------------------------------
procedure Tframe_Template_Site_linkend_view.View(aID: integer; aGUID: string);
//--------------------------------------------------------------------
begin
  inherited;

//  Fframe_LinkEnd_Inspector.View (aID, OBJ_LINKEND);


  Fframe_Inspector.View (aID);

//  Fframe_antennas.Template_Site_ID:=aID;
  Fframe_antennas.View (aID);

end;


end.

