library dll_LOS;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  SysUtils,
  Classes,
  d_Calc_LOS in '..\src\d_Calc_LOS.pas' {dlg_Calc_LOS},
  d_LosRegion in '..\src\d_LosRegion.pas' {dlg_LosRegion},
  dm_LOS_calc in '..\src\dm_LOS_calc.pas' {dmLOS_calc: TDataModule},
  dm_LOS_calc_m in '..\src\dm_LOS_calc_m.pas' {dmLOS_calc_m: TDataModule},
  dm_LOS_main in '..\src\dm_LOS_main.pas' {dmLOS_main1: TDataModule},
  dm_LosRegion_view in '..\src\dm_LosRegion_view.pas' {dmLosRegion_view: TDataModule},
  fr_LosM_view in '..\src\fr_LosM_view.pas' {frame_LosM_view},
  fr_LosRegion_cells in '..\src\fr_LosRegion_cells.pas' {frame_LosRegion_cells},
  i_LOS_ in '..\src shared\i_LOS_.pas',
  u_ini_LOS_params in '..\src shared\u_ini_LOS_params.pas',
  u_dll_with_dmMain in '..\..\DLL_common\u_dll_with_dmMain.pas';

{$R *.res}

begin
end.
