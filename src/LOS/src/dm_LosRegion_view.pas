unit dm_LosRegion_view;

interface

uses
  Classes, Forms, SysUtils, Db, ADODB,  Variants,

  u_Excel,
  dm_Excel_file,

  dm_Onega_DB_data,

  dm_Main,
//  dm_Custom,

  

  u_classes,
  u_Geo,
  u_func,
  u_db
  ;

type
  TdmLosRegion_view = class(TDataModule)
    ADOStoredProc1: TADOStoredProc;
    ADOConnection1: TADOConnection;
    procedure DataModuleCreate(Sender: TObject);

  private
  public
//    procedure AddSiteCellsToMemDB  (aDataset: TDataset;  aSiteID: Integer);

    procedure Open (aDataset: TDataset; var aPoints: TBLPointArrayF);
// TODO: Open12
//  procedure Open12(aDataset: TDataset; aSiteIDList: TIDList);

    procedure OpenAll(aDataset, aExistedDataset: TDataset);

    procedure InitDB (aDataset: TDataset);

    procedure GetPropertyIDList (aDataset: TDataset; out aIDList: TIDList);
    procedure LoadExcel(aFileName: string; aDataset: TDataset);
  end;


function
  dmLosRegion_view: TdmLosRegion_view;

//==================================================================
//==================================================================
implementation
{$R *.dfm}

var
  FdmLosRegion_view: TdmLosRegion_view;


const
//  view_Property_Max_Antenna_heights = 'view_Property_Max_Antenna_heights';
  FLD_MAX_HEIGHT = 'MAX_HEIGHT';



function  dmLosRegion_view: TdmLosRegion_view;
begin
  if not Assigned(FdmLosRegion_view) then
    FdmLosRegion_view:= TdmLosRegion_view.Create (Application);

  Result:= FdmLosRegion_view;
end;




procedure TdmLosRegion_view.DataModuleCreate(Sender: TObject);
begin
  inherited;

  db_SetComponentADOConnection(Self, dmMain.AdoConnection);

end;

//--------------------------------------------------------------------
procedure TdmLosRegion_view.InitDB (aDataset: TDataset);
//--------------------------------------------------------------------
begin
  db_CreateField (aDataset,
           [db_Field(FLD_ID,      ftInteger),
            db_Field(FLD_NAME,    ftString),

            db_Field(FLD_LAT,     ftFloat),
            db_Field(FLD_LON,     ftFloat),

            db_Field(FLD_HEIGHT,     ftFloat),
            db_Field(FLD_MAX_HEIGHT, ftFloat)

//            db_Field(FLD_MAX_LINKEND_HEIGHT,  ftInteger)
           ]);
end;

//-------------------------------------------------------------------
procedure TdmLosRegion_view.GetPropertyIDList (aDataset: TDataset; out aIDList: TIDList);
//-------------------------------------------------------------------
begin
  aDataset.First;

  with aDataset do
    while not EOF do
  begin
    aIDList.AddGUID(aDataset[FLD_ID], aDataset[FLD_GUID]);

    Next;
  end;
end;


//-------------------------------------------------------------------
procedure TdmLosRegion_view.Open (aDataset: TDataset;
                                  var aPoints: TBLPointArrayF);
//-------------------------------------------------------------------

(*const

  SQL_PROPERTY_IN_RECT =
      'SELECT * FROM ' + view_Property_Max_Antenna_heights +
      ' WHERE (lat BETWEEN %s AND %s) AND (lon BETWEEN %s AND %s) '+
      '       and (project_id=:project_id)';
*)
var
  blRect: TBLRect;
  blPoint: TBLPoint;
  sSQL, tempstr: string;
  iID: integer;
  iCount: integer;
begin
  aDataset.Close;
  aDataset.Open;

  if aPoints.Count=0 then
    Exit;

  aDataset.DisableControls;
 // CursorHourGlass;

  // ����� ������
//  blRect:=

  blRect:=geo_DefineRoundBLRectForArrayF(aPoints);

  dmOnega_DB_data.LOS_property_select (ADOStoredProc1,
         dmMain.ProjectID,

         blRect.BottomRight.B,
         blRect.TopLeft.B,

         blRect.TopLeft.L,     //lon_min
         blRect.BottomRight.L  //lon_max
        );

//
//  sSQL:= Format(SQL_PROPERTY_IN_RECT,
//               [AsString(blRect.BottomRight.B),
//                AsString(blRect.TopLeft.B),
//                AsString(blRect.TopLeft.L),
//                AsString(blRect.BottomRight.L)
//               ]);
//
//  sSQL:= ReplaceStr(sSQL, ',', '.');
//
//  //������� �� ��� ��������
//  db_OpenQuery (qry_Property, sSQL,
//                [db_Par(FLD_PROJECT_ID, dmMain.Projectid)  ]);


  with ADOStoredProc1 do
    while not EOF do
  begin
    blPoint:=db_ExtractBLPoint(ADOStoredProc1);

    if geo_IsBLPointInBLPolygon(BLPoint, aPoints) then
      db_AddRecord (aDataset,
        [
         db_par(FLD_ID,          FieldValues[FLD_ID]),
         db_par(FLD_NAME,        FieldValues[FLD_NAME]),
         db_par(FLD_LAT,         FieldValues[FLD_LAT]),
         db_par(FLD_LON,         FieldValues[FLD_LON]),

         db_par(FLD_HEIGHT,      FieldValues[FLD_MAX_HEIGHT]),
         db_par(FLD_MAX_HEIGHT, FieldValues[FLD_MAX_HEIGHT])

        ]);

    Next;
  end;



  aDataset.First;
  aDataset.EnableControls;
 // CursorDefault;
end;

//-------------------------------------------------------------------
procedure TdmLosRegion_view.OpenAll(aDataset, aExistedDataset: TDataset);
//-------------------------------------------------------------------
var
  oIDList: TIDList;
begin
  db_Clear(aDataset);

  aDataset.DisableControls;
  CursorHourGlass;

  dmOnega_DB_data.LOS_property_select (ADOStoredProc1, dmMain.ProjectID);

//
//  db_OpenQuery(qry_Temp,
//      'SELECT * FROM ' + view_Property_Max_Antenna_heights +
//      ' WHERE (project_id=:project_id)',
//      [db_Par(FLD_PROJECT_ID, dmMain.ProjectID)]);


  oIDList:= TIDList.Create;
  oIDList.LoadIDsFromDataSet(aExistedDataset, FLD_ID);


  with ADOStoredProc1 do
    while not EOF do
  begin
    if not oIDList.IDFound(FieldByName(FLD_ID).AsInteger) then
      db_AddRecord (aDataset,
          [
           db_par(FLD_ID,            FieldValues[FLD_ID]),
           db_par(FLD_NAME,          FieldValues[FLD_NAME]),

           db_par(FLD_LAT,           FieldValues[FLD_LAT]),
           db_par(FLD_LON,           FieldValues[FLD_LON]),

           db_par(FLD_HEIGHT,        FieldValues[FLD_MAX_HEIGHT]),
           db_par(FLD_MAX_HEIGHT,    FieldValues[FLD_MAX_HEIGHT])

  //         db_par(FLD_HEIGHT,        FieldValues[FLD_HEIGHT]),
//           db_par(FLD_MAX_LINKEND_HEIGHT,  FieldValues[FLD_MAX_HEIGHT])

          ]);

    Next;
  end;

  CursorDefault;
  aDataset.First;
  aDataset.EnableControls;

  FreeAndNil(oIDList);


end;


//----------------------------------------------------------------
procedure TdmLosRegion_view.LoadExcel(aFileName: string; aDataset: TDataset);
//----------------------------------------------------------------
var
  c: Integer;
  r: Integer;
  s: string;
  sHeader: string;

  arr: array of record
                  Name : string;
                  Height : Integer;
                end;
  i: Integer;

  oExcel: TExcel;

begin
 // aFileName := 'C:\_bee\�����1.xls';

  db_Clear(aDataset);

//  oExcel:=TExcel.Create;
//  oExcel.Open(aFileName);
//
//  oExcel.

//  dmExcel_file.Open(afileName);

 // dmExcel_file.Open('W:\Tasks new\Tasks new\Link import excel\Data\1.xls');
  dmExcel_file.Open (aFileName);
  dmExcel_file.LoadWorksheetByIndex_0(0);


  //oList:=TLinkEndType_mode_List.Create;

  Assert (dmExcel_file.RowCount > 0, 'dmExcel_file.RowCount > 0');

  setLength(arr, dmExcel_file.RowCount -1);


  for r := 2 to dmExcel_file.RowCount  do
  begin
   // oMode := AddItem;

    for c := 1 to dmExcel_file.ColCount  do
    begin
      sHeader:=VarToStr(dmExcel_file.Cells_1[1,c]);

      s:=VarToStr(dmExcel_file.Cells_1[r,c]);

      i:=r-2;

      case c of
        1: arr[i].Name   := s;
        2: arr[i].Height := AsInteger(s);
      end;

    end;
  end;

  // -------------------------
  dmExcel_file.Close;
                 
  dmOnega_DB_data.LOS_property_select (ADOStoredProc1, dmMain.ProjectID);


//  db_View(ADOStoredProc1);

  aDataset.DisableControls;


  for I := 0 to High(arr) do
  begin
//    if mem_Selected.Locate(FLD_name, arr[i].Name, []) then
//      db_UpdateRecord(mem_Selected, FLD_Height, arr[i].Height);

     with ADOStoredProc1 do
        if Locate(FLD_name, arr[i].Name, []) then
          db_AddRecord_ (aDataset,
              [
               FLD_ID,            FieldValues[FLD_ID],
               FLD_NAME,          FieldValues[FLD_NAME],

               FLD_LAT,           FieldValues[FLD_LAT],
               FLD_LON,           FieldValues[FLD_LON],

//               FLD_HEIGHT,        FieldValues[FLD_MAX_HEIGHT],
               FLD_HEIGHT,        arr[i].Height,
               FLD_MAX_HEIGHT,    FieldValues[FLD_MAX_HEIGHT]

      //         db_par(FLD_HEIGHT,        FieldValues[FLD_HEIGHT]),
    //           db_par(FLD_MAX_LINKEND_HEIGHT,  FieldValues[FLD_MAX_HEIGHT])

              ]);


  end;


  // -------------------------


  aDataset.First;
  aDataset.EnableControls;


end;



end.

