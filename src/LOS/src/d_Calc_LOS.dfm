inherited dlg_Calc_LOS: Tdlg_Calc_LOS
  Left = 1099
  Top = 232
  Width = 543
  Height = 661
  Caption = 'dlg_Calc_LOS'
  OldCreateOrder = True
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 598
    Width = 535
    inherited Bevel1: TBevel
      Width = 535
    end
    inherited Panel3: TPanel
      Left = 361
      Width = 174
      inherited btn_Ok: TButton
        Left = 3
      end
      inherited btn_Cancel: TButton
        Left = 87
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 535
    inherited Bevel2: TBevel
      Width = 535
    end
    inherited pn_Header: TPanel
      Width = 535
      Visible = False
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 60
    Width = 535
    Height = 469
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 2
    object cxVerticalGrid1: TcxVerticalGrid
      Left = 5
      Top = 5
      Width = 525
      Height = 459
      BorderStyle = cxcbsNone
      Align = alClient
      LookAndFeel.Kind = lfOffice11
      OptionsView.CellTextMaxLineCount = 3
      OptionsView.AutoScaleBands = False
      OptionsView.PaintStyle = psDelphi
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.RowHeaderMinWidth = 30
      OptionsView.RowHeaderWidth = 205
      OptionsView.ValueWidth = 40
      TabOrder = 0
      Version = 1
      object row_Pos1: TcxEditorRow
        Properties.Caption = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1099
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = True
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object row_Use_Site_Pos1: TcxEditorRow
        Expanded = False
        Properties.Caption = #1042#1079#1103#1090#1100' '#1073#1083#1080#1078#1072#1081#1096#1091#1102' '#1087#1083#1086#1097#1072#1076#1082#1091
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.Alignment = taLeftJustify
        Properties.EditProperties.NullStyle = nssUnchecked
        Properties.EditProperties.ReadOnly = False
        Properties.EditProperties.UseAlignmentWhenInplace = True
        Properties.EditProperties.ValueChecked = 'True'
        Properties.EditProperties.ValueGrayed = ''
        Properties.EditProperties.ValueUnchecked = 'False'
        Properties.DataBinding.ValueType = 'Variant'
        Properties.Value = 'True'
        ID = 1
        ParentID = 0
        Index = 0
        Version = 1
      end
      object cxVerticalGrid1CategoryRow1: TcxCategoryRow
        Expanded = False
        ID = 2
        ParentID = -1
        Index = 1
        Version = 1
      end
      object cxVerticalGrid1CategoryRow2: TcxCategoryRow
        Properties.Caption = #1057#1090#1072#1085#1094#1080#1103
        ID = 3
        ParentID = -1
        Index = 2
        Version = 1
      end
      object row_Site_H1: TcxEditorRow
        Expanded = False
        Properties.Caption = #1042#1099#1089#1086#1090#1072' '#1072#1085#1090#1077#1085#1085#1099' ('#1084')'
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = False
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = '30'
        ID = 4
        ParentID = 3
        Index = 0
        Version = 1
      end
      object cxVerticalGrid1CategoryRow3: TcxCategoryRow
        Properties.Caption = #1058#1077#1088#1084#1080#1085#1072#1083
        ID = 5
        ParentID = -1
        Index = 3
        Version = 1
      end
      object row_Terminal_H1: TcxEditorRow
        Expanded = False
        Properties.Caption = #1042#1099#1089#1086#1090#1072' '#1072#1085#1090#1077#1085#1085#1099' ('#1084')'
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = False
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = '2'
        ID = 6
        ParentID = 5
        Index = 0
        Version = 1
      end
      object cxVerticalGrid1EditorRow5: TcxEditorRow
        Expanded = False
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = False
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 7
        ParentID = -1
        Index = 4
        Version = 1
      end
      object row_CalcStep1: TcxEditorRow
        Expanded = False
        Properties.Caption = #1064#1072#1075' '#1088#1072#1089#1095#1077#1090#1072' ('#1084')'
        Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
        Properties.EditProperties.MaxValue = 5000.000000000000000000
        Properties.EditProperties.MinValue = 1.000000000000000000
        Properties.EditProperties.UseCtrlIncrement = True
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = '100'
        ID = 8
        ParentID = -1
        Index = 5
        Version = 1
      end
      object row_Freq111: TcxEditorRow
        Expanded = False
        Properties.Caption = #1063#1072#1089#1090#1086#1090#1072' [GHz)'
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = False
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = '10'
        Visible = False
        ID = 9
        ParentID = -1
        Index = 6
        Version = 1
      end
      object row_Refraction1: TcxEditorRow
        Expanded = False
        Properties.Caption = #1056#1077#1092#1088#1072#1082#1094#1080#1103
        Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
        Properties.EditProperties.MaxValue = 100.000000000000000000
        Properties.EditProperties.UseCtrlIncrement = True
        Properties.EditProperties.ValueType = vtFloat
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = '1.33'
        ID = 10
        ParentID = -1
        Index = 7
        Version = 1
      end
      object cxVerticalGrid1EditorRow9: TcxEditorRow
        Expanded = False
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = False
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 11
        ParentID = -1
        Index = 8
        Version = 1
      end
      object row_ClutterModel1: TcxEditorRow
        Expanded = False
        Properties.Caption = #1052#1086#1076#1077#1083#1100' '#1090#1080#1087#1086#1074' '#1084#1077#1089#1090#1085#1086#1089#1090#1080
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.ReadOnly = True
        Properties.EditProperties.OnButtonClick = row_ClutterModel1EditPropertiesButtonClick
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 12
        ParentID = -1
        Index = 9
        Version = 1
      end
      object row_ColorSchema_LOS1: TcxEditorRow
        Expanded = False
        Properties.Caption = #1062#1074#1077#1090#1086#1074#1099#1077' '#1089#1093#1077#1084#1099
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.ReadOnly = True
        Properties.EditProperties.OnButtonClick = row_ClutterModel1EditPropertiesButtonClick
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 13
        ParentID = -1
        Index = 10
        Version = 1
      end
      object cxVerticalGrid1EditorRow12: TcxEditorRow
        Expanded = False
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = False
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 14
        ParentID = -1
        Index = 11
        Version = 1
      end
      object row_radius1: TcxEditorRow
        Expanded = False
        Properties.Caption = #1056#1072#1076#1080#1091#1089' '#1088#1072#1089#1095#1077#1090#1072' ['#1082#1084']'
        Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
        Properties.EditProperties.UseCtrlIncrement = True
        Properties.EditProperties.ValueType = vtFloat
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = '20'
        ID = 15
        ParentID = -1
        Index = 12
        Version = 1
      end
      object cxVerticalGrid1CategoryRow4: TcxCategoryRow
        Expanded = False
        ID = 16
        ParentID = -1
        Index = 13
        Version = 1
      end
      object row_Azimuth: TcxEditorRow
        Expanded = False
        Properties.Caption = #1040#1079#1080#1084#1091#1090' ('#1075#1088')'
        Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
        Properties.EditProperties.MaxValue = 360.000000000000000000
        Properties.EditProperties.UseCtrlIncrement = True
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = '0'
        ID = 17
        ParentID = -1
        Index = 14
        Version = 1
      end
      object row_Width: TcxEditorRow
        Expanded = False
        Properties.Caption = #1064#1080#1088#1080#1085#1072' '#1079#1086#1085#1099' ('#1075#1088')'
        Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.AssignedValues.MinValue = True
        Properties.EditProperties.MaxValue = 360.000000000000000000
        Properties.EditProperties.ReadOnly = False
        Properties.EditProperties.UseCtrlIncrement = True
        Properties.DataBinding.ValueType = 'Float'
        Properties.Value = 360.000000000000000000
        ID = 18
        ParentID = -1
        Index = 15
        Version = 1
      end
      object cxVerticalGrid1CategoryRow5: TcxCategoryRow
        ID = 19
        ParentID = -1
        Index = 16
        Version = 1
      end
      object cxVerticalGrid1CategoryRow6: TcxCategoryRow
        Properties.Caption = #1050#1072#1088#1090#1072
        ID = 20
        ParentID = -1
        Index = 17
        Version = 1
      end
      object row_Map_type11: TcxEditorRow
        Properties.Caption = #1058#1080#1087' '#1082#1072#1088#1090#1099
        Properties.ImageIndex = 1
        Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
        Properties.EditProperties.Columns = 2
        Properties.EditProperties.DefaultValue = 0
        Properties.EditProperties.ImmediatePost = True
        Properties.EditProperties.Items = <
          item
            Caption = #1056#1072#1089#1090#1088
            Value = '0'
          end
          item
            Caption = #1042#1077#1082#1090#1086#1088
            Value = '1'
          end>
        Properties.DataBinding.ValueType = 'Integer'
        Properties.Value = 0
        Visible = False
        ID = 21
        ParentID = 20
        Index = 0
        Version = 1
      end
      object row_MapRasterTransparence: TcxEditorRow
        Properties.Caption = #1055#1088#1086#1079#1088#1072#1095#1085#1086#1089#1090#1100' [%]'
        Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
        Properties.DataBinding.ValueType = 'Integer'
        Properties.Value = 50
        ID = 22
        ParentID = 20
        Index = 1
        Version = 1
      end
    end
  end
  object StatusBar1: TStatusBar [3]
    Left = 0
    Top = 579
    Width = 535
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 384
    inherited act_Cancel: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
    end
    object act_Calc_EMP: TAction
      Caption = #1056#1072#1089#1095#1077#1090' '#1069#1052#1055
    end
    object act_Join: TAction
      Caption = #1054#1073#1098#1077#1076#1080#1085#1077#1085#1080#1077' '#1088#1072#1089#1095#1077#1090#1086#1074
    end
    object act_MakeMaps: TAction
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1088#1090#1099
    end
  end
  inherited FormStorage1: TFormStorage
    Left = 424
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 344
  end
  object ActionList2: TActionList
    Left = 192
    Top = 4
    object FileSaveAs1: TFileSaveAs
      Category = 'File'
      Caption = 'Save &As...'
      Hint = 'Save As|Saves the active file with a new name'
      ImageIndex = 30
    end
  end
end
