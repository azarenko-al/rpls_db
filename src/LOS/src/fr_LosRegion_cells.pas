unit fr_LosRegion_cells;

 interface

 uses
  Classes, Controls, Forms, Db, StdCtrls, ExtCtrls, ComCtrls,
  ActnList,  cxGridCustomTableView, cxGridTableView, Variants,
//  cxExportGrid4Link,

  dxmdaset,   cxGridDBBandedTableView,
  cxGridBandedTableView, cxGridLevel, cxGridDBTableView, cxGrid,

//  u_Excel,
 // dm_Excel_file,


  u_cx,
  u_db,
  u_func,
  u_Geo,
  u_classes,

  u_const_str,
  u_const_db,

  dm_LosRegion_view,


   cxClasses, cxControls, cxGridCustomView, ToolWin,
  StdActns;


type
  Tframe_LosRegion_cells = class(TForm)
    ToolBar2: TToolBar;
    ds_Selected: TDataSource;
    ActionList1: TActionList;
    act_Collapse: TAction;
    act_Expand: TAction;
    act_Add111: TAction;
    act_Del1111: TAction;
    mem_Selected: TdxMemData;
    act_Add: TAction;
    act_Del: TAction;
    act_Add_All: TAction;
    act_Del_All: TAction;
    act_Add_By_Criteria: TAction;
    mem_All: TdxMemData;
    ds_All: TDataSource;
    act_Collapse_right: TAction;
    act_Expand_right: TAction;
    Panel1: TPanel;
    cxGrid1: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridBandedTableView1: TcxGridBandedTableView;
    cxGridBandedColumn1: TcxGridBandedColumn;
    cxGridBandedColumn2: TcxGridBandedColumn;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGrid1DBBandedTableView1_selected: TcxGridDBBandedTableView;
    col_name1: TcxGridDBBandedColumn;
    col_Max_H1: TcxGridDBBandedColumn;
    col_ID_selected: TcxGridDBBandedColumn;
    col_H1: TcxGridDBBandedColumn;
    cxGridLevel1: TcxGridLevel;
    pn_Button: TPanel;
    Button6: TButton;
    Button5: TButton;
    Button4: TButton;
    Button3: TButton;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2BandedTableView1_selected: TcxGridBandedTableView;
    col__id_selected: TcxGridBandedColumn;
    cxGrid2BandedTableView1_selectedBandedColumn1: TcxGridBandedColumn;
    cxGrid2DBTableView1_selected: TcxGridDBTableView;
    cxGrid2DBTableView1_selectedDBColumn1: TcxGridDBColumn;
    cxGrid2DBBandedTableView1_all: TcxGridDBBandedTableView;
    col_Name2: TcxGridDBBandedColumn;
    col_Max_H2: TcxGridDBBandedColumn;
    col_id_all: TcxGridDBBandedColumn;
    cxGrid2DBBandedTableView1_allDBBandedColumn1: TcxGridDBBandedColumn;
    cxGrid2Level1: TcxGridLevel;
    act_FileOpen_Excel: TFileOpen;
    Panel2: TPanel;
    Panel3: TPanel;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure DoAction(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_FileOpen_ExcelAccept(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure tree_Selected_BS111DblClick(Sender: TObject);
    procedure tree_All_BSDblClick(Sender: TObject);

  private
    FRegPath: string;

    FIDList: TIDList;
    FPointsArr: TBLPointArrayF;

//    procedure CollapseAll();
    procedure EnableControls();
    procedure DisableControls();

    procedure GetPropertyIDList(aIDList: TIDList);
    procedure LoadExcel(aFileName: string);

    procedure MoveRecords (aSrcDataSet, aDestDataSet: TDataSet; aID: integer);
  public
    SelectedItems: array of record
                      ID: integer;
                      Name: string;
                      Height : integer;
                      BLPoint: TBLPoint;
                   end;


    function  SelectedCount(): Integer;

    procedure ViewBYPoints(var aPointsArr: TBLPointArrayF);
    procedure FillSelectedSites;
    procedure SelectAll;

  end;


//==================================================================
implementation {$R *.DFM}
//==================================================================


//--------------------------------------------------------------------
procedure Tframe_LosRegion_cells.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  panel1.Align:=alClient;

  cxGrid2.Align := alClient;

  //GSPages1.Align := alClient;
  //GSPages1.ActivePageIndex := 0;

  col_Name1.Caption:=STR_NAME;
  col_Name2.Caption:=STR_NAME;

  act_Collapse.Caption:=        STR_ACT_COLLAPSE;
  act_Expand.Caption:=          STR_ACT_EXPAND;
  act_Collapse_right.Caption:=  STR_ACT_COLLAPSE;
  act_Expand_right.Caption:=    STR_ACT_EXPAND;

  FIDList:=TIDList.Create;

  dmLosRegion_view.InitDB (mem_Selected);
  dmLosRegion_view.InitDB (mem_All);

  col_Max_H1.DataBinding.FieldName := FLD_MAX_HEIGHT;
  col_Max_H2.DataBinding.FieldName := FLD_MAX_HEIGHT;


  cx_CheckDataFields (cxGrid1DBBandedTableView1_selected);
  cx_CheckDataFields (cxGrid2DBBandedTableView1_all);


  act_FileOpen_Excel.Dialog.Filter := DEF_EXCEL_Filter;

end;

//--------------------------------------------------------------------
procedure Tframe_LosRegion_cells.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  FIDList.Free;

  inherited;
end;

//--------------------------------------------------------------------
function  Tframe_LosRegion_cells.SelectedCount(): Integer;
//--------------------------------------------------------------------
begin
  Result:= mem_Selected.RecordCount;
end;


//--------------------------------------------------------------------
procedure Tframe_LosRegion_cells.ViewBYPoints(var aPointsArr: TBLPointArrayF);
//--------------------------------------------------------------------
begin
  dmLosRegion_view.Open    (mem_Selected, aPointsArr);
  dmLosRegion_view.OpenAll (mem_all, mem_Selected);

  FPointsArr:= aPointsArr;

  act_Collapse.Execute;
end;

//--------------------------------------------------------------------
procedure Tframe_LosRegion_cells.DoAction(Sender: TObject);
//--------------------------------------------------------------------
var i: integer;  //, iSiteID
//    sSiteName: string;
begin
  if (Sender=act_Add) or
     (Sender=act_Del) or
     (Sender=act_Add_All) or
     (Sender=act_Del_All) or
     (Sender=act_Add_By_Criteria) then
  begin
    CursorHourGlass();
    DisableControls();
    FIDList.Clear;
  end;

  //-------------------------------------------------------------------
  if Sender=act_Add then begin
  //-------------------------------------------------------------------
    with cxGrid2DBBandedTableView1_all.Controller do
      for i:=0 to SelectedRowCount-1 do
        FIDList.AddID (SelectedRows[i].Values[col_id_all.Index]);

    for i:= 0 to FIDList.Count-1 do
      MoveRecords (mem_All, mem_Selected, FIDList[i].ID);
  end else

  //-------------------------------------------------------------------
  if Sender=act_Add_All then begin
  //-------------------------------------------------------------------
    mem_All.First;

    with mem_All do
      while not EOF do
      begin
//        if (mem_All[FLD_TYPE] = OBJ_SITE)  then
          FIDList.AddID(mem_All[FLD_ID]);

        Next;
      end;

    for I := 0 to FIDList.Count-1 do
      MoveRecords(mem_All, mem_Selected, FIDList[i].ID);
  end else

  //-------------------------------------------------------------------
  if Sender=act_Del then begin
  //-------------------------------------------------------------------
    with cxGrid1DBBandedTableView1_selected.Controller do
      for i:=0 to SelectedRowCount-1 do
//        if (SelectedNodes[i].Values[col_type.Index] = OBJ_SITE) then
          FIDList.AddID(SelectedRows[i].Values[col_id_selected.Index]);

    for i:= 0 to FIDList.Count-1 do
      MoveRecords (mem_Selected, mem_All, FIDList[i].ID);
  end else

  //-------------------------------------------------------------------
  if Sender=act_Del_All then begin
  //-------------------------------------------------------------------
    mem_Selected.First;

    with mem_Selected do
      while not EOF do
      begin
//        if (mem_Selected[FLD_TYPE] = OBJ_SITE)  then
          FIDList.AddID(mem_Selected[FLD_ID]);

        Next;
      end;

    for I := 0 to FIDList.Count-1 do
      MoveRecords(mem_Selected, mem_All, FIDList[i].ID);
  end;

  if (Sender=act_Add) or
     (Sender=act_Del) or
     (Sender=act_Add_All) or
     (Sender=act_Del_All) or
     (Sender=act_Add_By_Criteria) then
  begin
    EnableControls();
  //  tree_Selected_BS.FullRefresh;
  //  tree_All_BS.FullRefresh;
    CursorDefault();
  end;
end;


//--------------------------------------------------------------------
procedure Tframe_LosRegion_cells.DisableControls();
//--------------------------------------------------------------------
begin
  mem_All.DisableControls;
  mem_Selected.DisableControls;
end;

//--------------------------------------------------------------------
procedure Tframe_LosRegion_cells.EnableControls();
//--------------------------------------------------------------------
begin
  mem_All.EnableControls;
  mem_Selected.EnableControls;
end;

//--------------------------------------------------------------------
procedure Tframe_LosRegion_cells.MoveRecords (aSrcDataSet, aDestDataSet: TDataSet; aID: integer);
//--------------------------------------------------------------------
var
  iRecNoSrc, iRecNoDest: integer;
begin
  iRecNoSrc:=  aSrcDataSet.RecNo;
  iRecNoDest:= aDestDataSet.RecNo;

  // Copy Records
  if aSrcDataSet.Locate(FLD_ID, aID, []) then
  begin
    if not aDestDataSet.Locate(FLD_ID, aID, []) then
    begin
      aDestDataset.Append;
      db_CopyRecord(aSrcDataSet, aDestDataSet);
      aDestDataset.Post;
    end;

    aSrcDataSet.Delete;
  end;

  aSrcDataSet.RecNo:=  iRecNoSrc;
  aDestDataSet.RecNo:= iRecNoDest;
end;


//--------------------------------------------------------------------
procedure Tframe_LosRegion_cells.GetPropertyIDList(aIDList: TIDList);
//--------------------------------------------------------------------
begin
  dmLosRegion_view.GetPropertyIDList (mem_Selected, aIDList);
end;

//--------------------------------------------------------------------
procedure Tframe_LosRegion_cells.FillSelectedSites;
//--------------------------------------------------------------------
var
  i: Integer;
begin
  SetLength(SelectedItems, mem_Selected.RecordCount);
  i:=0;

  with mem_Selected do
  begin
    DisableControls;

    First;
    while not EOF do begin
      SelectedItems[i].BLPoint.B:= FieldByName(FLD_LAT).AsFloat;
      SelectedItems[i].BLPoint.L:= FieldByName(FLD_LON).AsFloat;
      SelectedItems[i].ID       := FieldByName(FLD_ID).AsInteger;
      SelectedItems[i].name     := FieldByName(FLD_name).AsString;

      SelectedItems[i].Height   := FieldByName(FLD_Height).AsInteger;

      Inc(i);
      Next;
    end;

    EnableControls;
  end;

end;

//----------------------------------------------------------------
procedure Tframe_LosRegion_cells.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//----------------------------------------------------------------
var
  bAddEnabled, bDelEnabled: boolean;
begin
  bAddEnabled:= false;
  bDelEnabled:= false;

  bAddEnabled:= cxGrid2DBBandedTableView1_all.Controller.SelectedRowCount>0;
  bDelEnabled:= cxGrid1DBBandedTableView1_selected.Controller.SelectedRowCount>0;

  act_Add.Enabled:= bAddEnabled;
  act_Del.Enabled:= bDelEnabled;

  act_Add_All.Enabled:= not mem_All.IsEmpty;
  act_Del_All.Enabled:= not mem_Selected.IsEmpty;

end;

procedure Tframe_LosRegion_cells.FormResize(Sender: TObject);
begin
  cxGrid1.Width:=(Width - pn_Button.Width) div 2;
end;

procedure Tframe_LosRegion_cells.SelectAll;
begin
  act_Add_All.Execute;
end;


//----------------------------------------------------------------
procedure Tframe_LosRegion_cells.tree_Selected_BS111DblClick(Sender: TObject);
//----------------------------------------------------------------
begin
  try
    if Assigned(cxGrid1DBBandedTableView1_selected.Controller.FocusedRow) then
      with cxGrid1DBBandedTableView1_selected.Controller.FocusedRow do
        MoveRecords (mem_Selected, mem_All, values[col_id_selected.Index]);
  except  end;
end;

//----------------------------------------------------------------
procedure Tframe_LosRegion_cells.tree_All_BSDblClick(Sender: TObject);
//----------------------------------------------------------------
begin
  try
    if Assigned(cxGrid2DBBandedTableView1_all.Controller.FocusedRow) then
      with cxGrid2DBBandedTableView1_all.Controller.FocusedRow do
        MoveRecords (mem_All, mem_Selected, values[col_id_all.Index]);
  except  end;
end;



procedure Tframe_LosRegion_cells.act_FileOpen_ExcelAccept(Sender: TObject);
begin
  LoadExcel(act_FileOpen_Excel.Dialog.FileName);
//
end;    

procedure Tframe_LosRegion_cells.Button1Click(Sender: TObject);
begin

end;


//----------------------------------------------------------------
procedure Tframe_LosRegion_cells.LoadExcel(aFileName: string);
//----------------------------------------------------------------
begin
//  db_Clear(mem_All);
  db_Clear(mem_Selected);

  dmLosRegion_view.LoadExcel(aFileName, mem_All);
      
end;



end.


