unit fr_LosM_view;

interface

uses
  SysUtils, Variants, Classes, Graphics, Controls, Forms,
  ActnList, Menus, cxInplaceContainer, cxTL, cxGraphics, Dialogs,
  cxControls, StdActns, StdCtrls, ToolWin, ComCtrls, cxStyles, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,

//  u_cxTreeExport,

 // cxExportGrid4Link,

  dm_LOS_calc_m,

  u_func,

  u_cx,

  cxGridDBTableView, cxGrid
  ;

type
  Tframe_LosM_view = class(TForm)
    cxTreeList1: TcxTreeList;
    col_Name1: TcxTreeListColumn;
    col_N1: TcxTreeListColumn;
    col_LOS: TcxTreeListColumn;
    PopupMenu1: TPopupMenu;
    ActionList1: TActionList;
    N1: TMenuItem;
    col_H: TcxTreeListColumn;
    ToolBar1: TToolBar;
    Button1: TButton;
    cb_Create_columns: TCheckBox;
    ToolButton1: TToolButton;
    cb_SubNodes: TCheckBox;
    act_Refresh: TAction;
    Button2: TButton;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_Red: TcxStyle;
    act_FileSaveAs1_: TAction;
    cxStyle_Lime: TcxStyle;
    FileSaveAs_ver2: TFileSaveAs;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    cxGrid_ver2: TcxGrid;
    cxGrid1TableView_ver2: TcxGridTableView;
    cxGrid1TableView_ver2Column_Site1_name: TcxGridColumn;
    cxGrid1TableView_ver2Column_Site1_H: TcxGridColumn;
    cxGrid1TableView_ver2Column_Offset: TcxGridColumn;
    cxGrid_ver2Level1: TcxGridLevel;
    cxGrid_ver1: TcxGrid;
    cxGrid1TableView_ver1: TcxGridTableView;
    cxGridColumn1: TcxGridColumn;
    cxGridColumn2: TcxGridColumn;
    cxGridColumn3: TcxGridColumn;
    cxGridColumn4: TcxGridColumn;
    cxGridLevel1: TcxGridLevel;
    cxGrid1TableView_ver2Column_Site2_name: TcxGridColumn;
    cxGrid1TableView_ver2Column_Site2_H: TcxGridColumn;
//    procedure cxTreeList1CustomDrawCell(Sender: TObject; ACanvas: TcxCanvas; AViewInfo:
   //     TcxTreeListEditCellViewInfo; var ADone: Boolean);
    procedure act_FileSaveAs1Accept(Sender: TObject);
    procedure act_RefreshExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxGrid1TableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure cxTreeList1CustomDrawDataCell(Sender: TcxCustomTreeList; ACanvas:
        TcxCanvas; AViewInfo: TcxTreeListEditCellViewInfo; var ADone: Boolean);
    procedure FileSaveAs_ver2Accept(Sender: TObject);
    procedure cxTreeList1StylesGetContentStyle(Sender, AItem: TObject;
      ANode: TcxTreeListNode; var AStyle: TcxStyle);
  private
    procedure MakeReport;
    procedure MakeReport_ver2;
    { Private declarations }
  public
    procedure ShowResults;
    { Public declarations }
  end;


{var
  frame_LosM_view: Tframe_LosM_view;
}

implementation
{$R *.dfm}

const
  DEF_FIXED_COLS    = 4;  
  DEF_COLOR_LOSS    = clLime;
  DEF_COLOR_NO_LOSS = clRed;



procedure Tframe_LosM_view.act_FileSaveAs1Accept(Sender: TObject);
var
  sFileName: string;
begin
//  sFileName:=act_FileSaveAs1.Dialog.FileName;
//
//  ExportTreeToExcel(cxTreeList1, Self, sFileName);

//  ShellExec(sFileName);

end;


procedure Tframe_LosM_view.act_RefreshExecute(Sender: TObject);
begin
  if Sender= act_Refresh then
     ShowResults;


end;

procedure Tframe_LosM_view.FormCreate(Sender: TObject);
begin
  cxTreeList1.Align:=alClient;

  FileSaveAs_ver2.Dialog.Filter := DEF_EXCEL_Filter;

end;                           

//-------------------------------------------------------------------
procedure Tframe_LosM_view.ShowResults;
//-------------------------------------------------------------------
var
  iValue: integer;
  j: integer;
  iCount: integer;
  I: integer;
  oColumn: TcxTreeListColumn;
  oNode,oNode1: TcxTreeListNode;
  sValue: string;

  bDrawColumns : Boolean;
  bSubNodes: Boolean;
begin

  bDrawColumns  := cb_Create_columns.Checked;
  bSubNodes     := cb_SubNodes.Checked;
                   
//  bDrawColumns := False;


  iCount:=Length(dmLOS_calc_m.OutMatrix);

  cxTreeList1.BeginUpdate;
  cxTreeList1.Clear;


  while cxTreeList1.ColumnCount>DEF_FIXED_COLS do
    cxTreeList1.Columns[DEF_FIXED_COLS].Free;

  if bDrawColumns then
    for I := 0 to iCount - 1 do
    begin
      oColumn:=cxTreeList1.CreateColumn();
      oColumn.Caption.Text:= IntToStr(i+1);
      oColumn.Width:=25;
    end;


  for I := 0 to iCount - 1 do
  begin
    oNode:=cxTreeList1.Add;
    oNode.Values[col_Name1.ItemIndex]:=dmLOS_calc_m.Sites[i].Name;
    oNode.Values[col_H.ItemIndex]    :=dmLOS_calc_m.Sites[i].Height;

 //   oNode.Values[col_ID1.ItemIndex]  :=dmLOS_calc_m.Sites[i].ID;
    oNode.Values[col_N1.ItemIndex]   :=IntToStr(i+1);


    if bDrawColumns then
    begin
      for j := 0 to iCount - 1 do
        oNode.Values[DEF_FIXED_COLS+j]:='';

      for j := 0 to iCount - 1 do
          if i<>j then
      begin
        iValue:=dmLOS_calc_m.OutMatrix[i,j].LosValue;
            
        if iValue<>DEF_NULL_VALUED then
          oNode.Values[DEF_FIXED_COLS+j]:=IntToStr(iValue);
      end;
    end;

    if bSubNodes then
      for j := 0 to iCount - 1 do
      begin
        iValue:=dmLOS_calc_m.OutMatrix[i,j].LosValue;

        if iValue<>DEF_NULL_VALUED then
        begin
          oNode1:=cxTreeList1.AddChild(oNode);
          oNode1.Values[col_Name1.ItemIndex]:=dmLOS_calc_m.Sites[j].Name;
      //    oNode1.Values[col_ID1.ItemIndex]  :=dmLOS_calc_m.Sites[j].ID;

          oNode1.Values[col_H.ItemIndex]  :=dmLOS_calc_m.Sites[j].Height;
          oNode1.Values[col_LOS.ItemIndex]  :=iValue;
        end;
      end;

  end;


  cxTreeList1.EndUpdate;

end;



//-------------------------------------------------------------------
procedure Tframe_LosM_view.MakeReport;
//-------------------------------------------------------------------
var
  iValue: integer;
  j: integer;
  iCount: integer;
  I: integer;
  k: Integer;
  sValue: string;

  oColumn: TcxGridColumn;
  oItem: TcxCustomGridTableItem;

  cxGridTableView : TcxGridTableView;

begin
  cxGridTableView := cxGrid1TableView_ver1;

  cxGridTableView.BeginUpdate;

  while cxGridTableView.DataController.RecordCount>0 do
    cxGridTableView.DataController.DeleteRecord(0);

 // cxGrid1TableView1.ClearItems;

//  bDrawColumns := False;


  iCount:=Length(dmLOS_calc_m.OutMatrix);



  while cxGridTableView.ColumnCount>DEF_FIXED_COLS do
    cxGridTableView.Columns[DEF_FIXED_COLS].Free;

// if bDrawColumns then
  for I := 0 to iCount - 1 do
  begin
    oColumn:= cxGridTableView.CreateColumn;
    oColumn.Caption :=IntToStr(i+1);


  //  oColumn:=cxTreeList1.CreateColumn();
  //  oColumn.Caption.Text:= IntToStr(i+1);
    oColumn.Width:=30;
  end;

  with cxGridTableView.DataController do
    for I := 0 to iCount - 1 do
  begin
    k:=AppendRecord;

(*
  //  oNode:=cxGrid1TableView1.Add;
    oNode.Values[0]:=IntToStr(i+1);
    oNode.Values[1]:=dmLOS_calc_m.Sites[i].Name;
    oNode.Values[2]:=dmLOS_calc_m.Sites[i].Height;
*)

    Values[k, 0]:=IntToStr(i+1);
    Values[k, 1]:=dmLOS_calc_m.Sites[i].Name;
    Values[k, 2]:=dmLOS_calc_m.Sites[i].Height;


 //   oNode.Values[col_ID1.ItemIndex]  :=dmLOS_calc_m.Sites[i].ID;



     // for j := 0 to iCount - 1 do
      //  oNode.Values[DEF_FIXED_COLS+j]:='';

      for j := 0 to iCount - 1 do
          if i<>j then
      begin
        iValue:=dmLOS_calc_m.OutMatrix[i,j].LosValue;

        if iValue<>DEF_NULL_VALUED then
          Values[k, DEF_FIXED_COLS+j]:=IntToStr(iValue);

 //         oNode.Values[DEF_FIXED_COLS+j]:=IntToStr(iValue);
      end;
   // end;

    Post;
  end;



  cxGridTableView.EndUpdate;

end;



//-------------------------------------------------------------------
procedure Tframe_LosM_view.MakeReport_ver2;
//-------------------------------------------------------------------
//�������� 1	������ ��.1	�������� 2	������ ��.2	�������
//-------------------------------------------------------------------
var
  iValue: integer;
  j: integer;
  iCount: integer;
  I: integer;
  k: Integer;
  sValue: string;

  oColumn: TcxGridColumn;
  oItem: TcxCustomGridTableItem;

  cxGridTableView : TcxGridTableView;


begin
  cxGridTableView := cxGrid1TableView_ver2;

  cxGridTableView.BeginUpdate;

  while cxGridTableView.DataController.RecordCount>0 do
    cxGridTableView.DataController.DeleteRecord(0);

 // cxGrid1TableView1.ClearItems;

//  bDrawColumns := False;


  iCount:=Length(dmLOS_calc_m.Sites);


(*
    Values[k, 0]:=IntToStr(i+1);
    Values[k, 1]:=dmLOS_calc_m.Sites[i].Name;
    Values[k, 2]:=dmLOS_calc_m.Sites[i].Height;

*)



  with cxGridTableView.DataController do
    for i := 0 to iCount - 1 do
      for j := 0 to iCount - 1 do
        if i<>j then
          if dmLOS_calc_m.OutMatrix[i,j].LosValue <> DEF_NULL_VALUED then

  begin
    k:=AppendRecord;

    Values[k, 0]:=dmLOS_calc_m.Sites[i].Name;
    Values[k, 1]:=dmLOS_calc_m.Sites[i].Height;

    Values[k, 2]:=dmLOS_calc_m.Sites[j].Name;
    Values[k, 3]:=dmLOS_calc_m.Sites[j].Height;
               
    iValue:=dmLOS_calc_m.OutMatrix[i,j].LosValue;

    if iValue<>DEF_NULL_VALUED then
      Values[k, 4]:=IntToStr(iValue);
                            

 //   oNode.Values[col_ID1.ItemIndex]  :=dmLOS_calc_m.Sites[i].ID;

//
//
//     // for j := 0 to iCount - 1 do
//      //  oNode.Values[DEF_FIXED_COLS+j]:='';
//
//      for j := 0 to iCount - 1 do
//          if i<>j then
//      begin
//        iValue:=dmLOS_calc_m.OutMatrix[i,j].LosValue;
//
//        if iValue<>DEF_NULL_VALUED then
//          Values[k, DEF_FIXED_COLS+j]:=IntToStr(iValue);
//
// //         oNode.Values[DEF_FIXED_COLS+j]:=IntToStr(iValue);
//      end;
//   // end;

    Post;
  end;



  cxGridTableView.EndUpdate;

end;


procedure Tframe_LosM_view.cxGrid1TableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
var
  i: Integer;
  v: Variant;
begin

  if AItem.Index>=DEF_FIXED_COLS then
  begin
    v:=ARecord.Values[AItem.Index];

    if VarIsNull(v) then
      Exit;

    i:=AsInteger(v);
    if i>=0 then
      AStyle:=cxStyle_Lime else
    if i<0 then
      AStyle:=cxStyle_Red;
  end;

end;

// ---------------------------------------------------------------
procedure Tframe_LosM_view.FileSaveAs_ver2Accept(Sender: TObject);
// ---------------------------------------------------------------
begin
  if Sender= FileSaveAs_ver2 then
  begin
    MakeReport_ver2;

    cx_ExportGridToExcel  (FileSaveAs_ver2.dialog.FileName, cxGrid_ver2);

  //  ExportGrid4ToExcel (FileSaveAs_ver2.dialog.FileName, cxGrid_ver2);

    ShellExec(FileSaveAs_ver2.dialog.FileName);

//    ExportTreeToExcel(cxTreeList1, Self);
  end;
end;

// ---------------------------------------------------------------
procedure Tframe_LosM_view.cxTreeList1StylesGetContentStyle(Sender,
  AItem: TObject; ANode: TcxTreeListNode; var AStyle: TcxStyle);
// ---------------------------------------------------------------
var
  i: integer;

  oColumn: TcxTreeListColumn;
begin
 // Exit;

  oColumn:=TcxTreeListColumn(AItem);

  if not Assigned(oColumn) then
    Exit;


//  ShowMessage(AItem.ClassName);



  if oColumn.ItemIndex>=DEF_FIXED_COLS then
    if ANode.Level=0 then
    begin
      if aNode.Values[oColumn.ItemIndex]='' then
        Exit;

      i:=AsInteger(aNode.Values[oColumn.ItemIndex]);
      if i>=0 then
        AStyle:=cxStyle_Lime;//  DEF_COLOR_LOSS;
      if i<0 then
       // ACanvas.Brush.Color:=DEF_COLOR_NO_LOSS;
        AStyle:=cxStyle_Red;
    end;



  if aNode.Level>0 then
  begin
 //   if AViewInfo.Node.Values[AViewInfo.Column.ItemIndex]='' then
  //    Exit;

    i:=AsInteger(aNode.Values[col_LOS.ItemIndex]);

  //  if i=DEF_NULL_VALUED  then

    if i>=0 then
      AStyle:=cxStyle_Lime;
     // ACanvas.Brush.Color:=DEF_COLOR_LOSS;
    if i<0 then
      AStyle:=cxStyle_Red;
  //    ACanvas.Brush.Color:=DEF_COLOR_NO_LOSS;
  end;




//
//const
//  DEF_FIXED_COLS    = 4;
//  DEF_COLOR_LOSS    = clLime;
//  DEF_COLOR_NO_LOSS = clRed;
//


end;



procedure Tframe_LosM_view.cxTreeList1CustomDrawDataCell(Sender:
    TcxCustomTreeList; ACanvas: TcxCanvas; AViewInfo:
    TcxTreeListEditCellViewInfo; var ADone: Boolean);
var
  i: integer;
begin


  if AViewInfo.Column.ItemIndex>=DEF_FIXED_COLS then
    if AViewInfo.Node.Level=0 then
    begin
      if AViewInfo.Node.Values[AViewInfo.Column.ItemIndex]='' then
        Exit;

      i:=AsInteger(AViewInfo.Node.Values[AViewInfo.Column.ItemIndex]);
      if i>=0 then
        ACanvas.Brush.Color:=DEF_COLOR_LOSS;
      if i<0 then
        ACanvas.Brush.Color:=DEF_COLOR_NO_LOSS;
    end;



  if AViewInfo.Node.Level>0 then
  begin
 //   if AViewInfo.Node.Values[AViewInfo.Column.ItemIndex]='' then
  //    Exit;

    i:=AsInteger(AViewInfo.Node.Values[col_LOS.ItemIndex]);

  //  if i=DEF_NULL_VALUED  then

    if i>=0 then
      ACanvas.Brush.Color:=DEF_COLOR_LOSS;
    if i<0 then
      ACanvas.Brush.Color:=DEF_COLOR_NO_LOSS;
  end;


end;



end.

     {

//-------------------------------------------------------------------
procedure Tframe_LosM_view.cxTreeList1CustomDrawCell(Sender: TObject; ACanvas: TcxCanvas;
    AViewInfo: TcxTreeListEditCellViewInfo; var ADone: Boolean);
//-------------------------------------------------------------------
var
  i: integer;
begin
Exit;
//
//  if AViewInfo.Column.ItemIndex>=DEF_FIXED_COLS then
//    if AViewInfo.Node.Level=0 then
//    begin
//      if AViewInfo.Node.Values[AViewInfo.Column.ItemIndex]='' then
//        Exit;
//
//      i:=AsInteger(AViewInfo.Node.Values[AViewInfo.Column.ItemIndex]);
//      if i>=0 then
//        ACanvas.Brush.Color:=DEF_COLOR_LOSS;
//      if i<0 then
//        ACanvas.Brush.Color:=DEF_COLOR_NO_LOSS;
//    end;
//
//
//
//  if AViewInfo.Node.Level>0 then
//  begin
// //   if AViewInfo.Node.Values[AViewInfo.Column.ItemIndex]='' then
//  //    Exit;
//
//    i:=AsInteger(AViewInfo.Node.Values[col_LOS.ItemIndex]);
//
//  //  if i=DEF_NULL_VALUED  then
//
//    if i>=0 then
//      ACanvas.Brush.Color:=DEF_COLOR_LOSS;
//    if i<0 then
//      ACanvas.Brush.Color:=DEF_COLOR_NO_LOSS;
//  end;


end;

