object frame_LosRegion_cells: Tframe_LosRegion_cells
  Left = 835
  Top = 419
  Width = 690
  Height = 426
  Cursor = crHandPoint
  BorderWidth = 5
  Caption = 'frame_LosRegion_cells'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 13
  object ToolBar2: TToolBar
    Left = 0
    Top = 0
    Width = 672
    Height = 97
    Caption = 'ToolBar2'
    Color = clAppWorkSpace
    EdgeBorders = []
    ParentColor = False
    TabOrder = 0
    Visible = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 129
    Width = 672
    Height = 179
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object cxGrid1: TcxGrid
      Left = 0
      Top = 0
      Width = 281
      Height = 179
      Align = alLeft
      TabOrder = 0
      LookAndFeel.Kind = lfFlat
      object cxGridDBTableView1: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
      end
      object cxGridBandedTableView1: TcxGridBandedTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnFiltering = False
        OptionsSelection.MultiSelect = True
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        Bands = <
          item
            Caption = #1042#1099#1073#1088#1072#1085#1085#1099#1077' '#1089#1090#1072#1085#1094#1080#1080
            Width = 137
          end>
        object cxGridBandedColumn1: TcxGridBandedColumn
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object cxGridBandedColumn2: TcxGridBandedColumn
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
      end
      object cxGridDBTableView2: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = ds_Selected
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnFiltering = False
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsSelection.CellSelect = False
        OptionsSelection.MultiSelect = True
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        object cxGridDBColumn1: TcxGridDBColumn
          Caption = #1048#1084#1103
          DataBinding.FieldName = 'name'
          Width = 142
        end
      end
      object cxGrid1DBBandedTableView1_selected: TcxGridDBBandedTableView
        OnDblClick = tree_Selected_BS111DblClick
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = ds_Selected
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnFiltering = False
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsSelection.MultiSelect = True
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        OptionsView.HeaderAutoHeight = True
        Bands = <
          item
            Caption = #1042#1099#1073#1088#1072#1085#1085#1099#1077' '#1087#1083#1086#1097#1072#1076#1082#1080
            Width = 231
          end>
        object col_name1: TcxGridDBBandedColumn
          Caption = #1048#1084#1103
          DataBinding.FieldName = 'Name'
          Options.Editing = False
          Options.Filtering = False
          Width = 132
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object col_Max_H1: TcxGridDBBandedColumn
          Caption = 'Max '#1074#1099#1089#1086#1090#1072' '#1040#1052#1057
          DataBinding.FieldName = 'max_linkend_Height'
          Options.Editing = False
          Options.Filtering = False
          Width = 80
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object col_ID_selected: TcxGridDBBandedColumn
          DataBinding.FieldName = 'id'
          Visible = False
          Options.Filtering = False
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object col_H1: TcxGridDBBandedColumn
          Caption = #1042#1099#1089#1086#1090#1072' '#1072#1085#1090#1077#1085#1085#1099
          DataBinding.FieldName = 'Height'
          Width = 55
          Position.BandIndex = 0
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGrid1DBBandedTableView1_selected
      end
    end
    object pn_Button: TPanel
      Left = 281
      Top = 0
      Width = 40
      Height = 179
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object Button6: TButton
        Left = 7
        Top = 123
        Width = 25
        Height = 25
        Action = act_Del_All
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object Button5: TButton
        Left = 7
        Top = 91
        Width = 25
        Height = 25
        Action = act_Add_All
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
      object Button4: TButton
        Left = 7
        Top = 59
        Width = 25
        Height = 25
        Action = act_Del
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
      object Button3: TButton
        Left = 7
        Top = 27
        Width = 25
        Height = 25
        Action = act_Add
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
      end
    end
    object cxGrid2: TcxGrid
      Left = 321
      Top = 0
      Width = 288
      Height = 179
      Align = alLeft
      TabOrder = 2
      LookAndFeel.Kind = lfFlat
      object cxGrid2DBTableView1: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
      end
      object cxGrid2BandedTableView1_selected: TcxGridBandedTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnFiltering = False
        OptionsSelection.MultiSelect = True
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        Bands = <
          item
            Caption = #1042#1099#1073#1088#1072#1085#1085#1099#1077' '#1089#1090#1072#1085#1094#1080#1080
            Width = 137
          end>
        object col__id_selected: TcxGridBandedColumn
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object cxGrid2BandedTableView1_selectedBandedColumn1: TcxGridBandedColumn
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
      end
      object cxGrid2DBTableView1_selected: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = ds_Selected
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnFiltering = False
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsSelection.CellSelect = False
        OptionsSelection.MultiSelect = True
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        object cxGrid2DBTableView1_selectedDBColumn1: TcxGridDBColumn
          Caption = #1048#1084#1103
          DataBinding.FieldName = 'name'
          Width = 142
        end
      end
      object cxGrid2DBBandedTableView1_all: TcxGridDBBandedTableView
        OnDblClick = tree_All_BSDblClick
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = ds_All
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnFiltering = False
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsSelection.MultiSelect = True
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        OptionsView.HeaderAutoHeight = True
        Bands = <
          item
            Caption = #1042#1089#1077' '#1087#1083#1086#1097#1072#1076#1082#1080
            Width = 231
          end>
        object col_Name2: TcxGridDBBandedColumn
          Caption = #1048#1084#1103
          DataBinding.FieldName = 'Name'
          Options.Editing = False
          Options.Filtering = False
          Width = 116
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object col_Max_H2: TcxGridDBBandedColumn
          Caption = 'Max '#1074#1099#1089#1086#1090#1072' '#1040#1052#1057
          DataBinding.FieldName = 'max_linkend_Height'
          Options.Editing = False
          Options.Filtering = False
          Width = 71
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object col_id_all: TcxGridDBBandedColumn
          DataBinding.FieldName = 'id'
          Visible = False
          Options.Filtering = False
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1_allDBBandedColumn1: TcxGridDBBandedColumn
          Caption = #1042#1099#1089#1086#1090#1072' '#1072#1085#1090#1077#1085#1085#1099
          DataBinding.FieldName = 'Height'
          Width = 55
          Position.BandIndex = 0
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
      end
      object cxGrid2Level1: TcxGridLevel
        GridView = cxGrid2DBBandedTableView1_all
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 97
    Width = 672
    Height = 32
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 2
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 240
      Height = 30
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Button1: TButton
        Left = 6
        Top = 5
        Width = 193
        Height = 22
        Action = act_FileOpen_Excel
        TabOrder = 0
      end
    end
  end
  object ds_Selected: TDataSource
    DataSet = mem_Selected
    Left = 234
    Top = 46
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 24
    Top = 4
    object act_Collapse: TAction
      Caption = 'act_Collapse'
      OnExecute = DoAction
    end
    object act_Expand: TAction
      Caption = 'act_Expand'
      OnExecute = DoAction
    end
    object act_Add111: TAction
      Caption = 'Add'
      OnExecute = DoAction
    end
    object act_Del1111: TAction
      Caption = 'Del'
      OnExecute = DoAction
    end
    object act_Add: TAction
      Caption = '<-'
      OnExecute = DoAction
    end
    object act_Del: TAction
      Caption = '->'
      OnExecute = DoAction
    end
    object act_Add_All: TAction
      Caption = '<<'
      OnExecute = DoAction
    end
    object act_Del_All: TAction
      Caption = '>>'
      OnExecute = DoAction
    end
    object act_Add_By_Criteria: TAction
      Caption = '<< '#1042#1099#1073#1088#1072#1090#1100' '#1087#1086' '#1082#1088#1080#1090#1077#1088#1080#1102
      OnExecute = DoAction
    end
    object act_Collapse_right: TAction
      Caption = 'act_Collapse_right'
      OnExecute = DoAction
    end
    object act_Expand_right: TAction
      Caption = 'act_Expand_right'
      OnExecute = DoAction
    end
    object act_FileOpen_Excel: TFileOpen
      Category = 'File'
      Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1074#1099#1089#1086#1090' '#1072#1085#1090#1077#1085#1085' '#1080#1079' Excel...'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = act_FileOpen_ExcelAccept
    end
  end
  object mem_Selected: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 232
  end
  object mem_All: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 328
  end
  object ds_All: TDataSource
    DataSet = mem_All
    Left = 328
    Top = 47
  end
end
