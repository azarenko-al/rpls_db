unit d_Calc_LOS;

interface

uses
  Classes, Controls, Forms, Registry,  cxPropertiesStore, cxVGrid,
  ComCtrls, ActnList, ExtCtrls, cxButtonEdit, SysUtils,

  u_DB_Manager,

  d_Wizard,

  d_DB_select,

//  I_Shell,
  dm_Main,

  dm_LOS_calc,

  dm_Act_Explorer,

  dm_Act_ColorSchema,
  dm_Act_Clutter,

  u_types,

  u_files,

  u_func,
  u_geo,

  u_cx_Vgrid_export,
  u_cx_Vgrid,


  u_const,
  u_const_db,

  StdActns, cxControls, cxInplaceContainer, rxPlacemnt, StdCtrls,
  cxLookAndFeels;



type
  Tdlg_Calc_LOS = class(Tdlg_Wizard)
    act_Calc_EMP: TAction;
    act_Join: TAction;
    act_MakeMaps: TAction;
    Panel1: TPanel;
    cxVerticalGrid1: TcxVerticalGrid;
    row_Pos1: TcxEditorRow;
    row_Use_Site_Pos1: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;
    row_Site_H1: TcxEditorRow;
    cxVerticalGrid1CategoryRow3: TcxCategoryRow;
    row_Terminal_H1: TcxEditorRow;
    cxVerticalGrid1EditorRow5: TcxEditorRow;
    row_CalcStep1: TcxEditorRow;
    row_Freq111: TcxEditorRow;
    row_Refraction1: TcxEditorRow;
    cxVerticalGrid1EditorRow9: TcxEditorRow;
    row_ClutterModel1: TcxEditorRow;
    row_ColorSchema_LOS1: TcxEditorRow;
    cxVerticalGrid1EditorRow12: TcxEditorRow;
    row_radius1: TcxEditorRow;
    cxVerticalGrid1CategoryRow4: TcxCategoryRow;
    row_Azimuth: TcxEditorRow;
    row_Width: TcxEditorRow;
    StatusBar1: TStatusBar;
    row_Map_type11: TcxEditorRow;
    cxVerticalGrid1CategoryRow5: TcxCategoryRow;
    cxVerticalGrid1CategoryRow6: TcxCategoryRow;
    row_MapRasterTransparence: TcxEditorRow;
    ActionList2: TActionList;
    FileSaveAs1: TFileSaveAs;
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_OkExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  
    procedure row_ClutterModel1EditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
//    procedure row_Map_typeEditPropertiesChange(Sender: TObject);
  private
//    FClutterModel_ID: integer;
//    FColorSchema_LOS_ID: integer;

    FRegPath : string;

  public
    class function ExecDlg (aBLPoint: TblPoint): boolean;
  end;


//====================================================================
implementation {$R *.DFM}
//====================================================================


//--------------------------------------------------------------------
class function Tdlg_Calc_LOS.ExecDlg(aBLPoint: TblPoint): boolean;
//--------------------------------------------------------------------
begin
  with Tdlg_Calc_LOS.Create(Application.MainForm) do
  try
    row_Pos1.Properties.Value:=geo_FormatBLPoint (aBLPoint);

    Result:=(ShowModal=mrOk);
  finally
    Free;
  end;
end;


//-------------------------------------------------------------------
procedure Tdlg_Calc_LOS.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
var
  iID: Integer;
  oDBManager: TDBManager;
begin
  inherited;

//  Caption :='���������� LOS | '+ GetAppVersionStr();

  Caption:= '���������� LOS | '+  '������: '  + FormatDateTime ('yyyy-mm-dd hh:mm', GetFileDate (Application.Exename));



//  Caption:= '���������� LOS |'+  GetAppVersionStr() + '  ������: '
//    + FormatDateTime ('yyyy-mm-dd hh:mm', GetFileDate (Application.Exename));


  //(Application.ExeName);

  SetActionName ('������ ����� LOS ��� �����');

  Panel1.Align:= alClient;

  {
  with TRegIniFile.Create(FRegPath) do
  begin
//    CurrentRegPath:='';

    FColorSchema_LOS_ID   :=ReadInteger('', row_ColorSchema_LOS1.Name, 0);
    FClutterModel_ID      :=ReadInteger('', row_ClutterModel1.Name, 0);

    Free;
  end;
 }

  FRegPath:=REGISTRY_FORMS + ClassName;

  cx_VerticalGrid_LoadFromReg(cxVerticalGrid1, FRegPath);

  {


  AddComponentProp(row_Use_Site_Pos1, 'Properties.Value');

  AddComponentProp(row_Radius1,     'Properties.Value');
  AddComponentProp(row_CalcStep1,   'Properties.Value');
  AddComponentProp(row_Site_H1,     'Properties.Value');
  AddComponentProp(row_Terminal_H1, 'Properties.Value');
  ////// AddComponentProp(row_Freq1,       'Properties.Value');
  AddComponentProp(row_Refraction1, 'Properties.Value');

  AddComponentProp(row_Azimuth,    'Properties.Value');
  AddComponentProp(row_Width,      'Properties.Value');

  // AddComponentProp(row_Map_type,         'Properties.Value');
  // AddComponentProp(row_Google_FileName,  'Properties.Value');
  AddComponentProp(row_MapRasterTransparence,  'Properties.Value');

  cxPropertiesStore.RestoreFrom;

  }


 // Assert(Assigned(gl_DB), 'Value not assigned');

  oDBManager:=TDBManager.Create(dmMain.ADOConnection);




  row_ColorSchema_LOS1.Properties.Value:=oDBManager.GetNameByID (TBL_ColorSchema, row_ColorSchema_LOS1.Tag);
  if row_ColorSchema_LOS1.Properties.Value='' then
  begin
    iID:=oDBManager.GetIDByName (TBL_ColorSchema, 'LOS');
    row_ColorSchema_LOS1.Properties.Value:=oDBManager.GetNameByID (TBL_ColorSchema, iID);
    row_ColorSchema_LOS1.Tag:=iID;
  end;


  row_ClutterModel1.Properties.Value:=oDBManager.GetNameByID (TBL_ClutterModel, row_ClutterModel1.Tag);
  if row_ClutterModel1.Properties.Value='' then
  begin
    iID:=oDBManager.GetIDByName (TBL_ClutterModel, 'default');
    row_ClutterModel1.Properties.Value:=oDBManager.GetNameByID (TBL_ClutterModel, iID);
    row_ClutterModel1.Tag:=iID;

  end;


  FreeAndNil(oDBManager);


  cx_InitVerticalGrid (cxVerticalGrid1);

  SetDefaultWidth;

  StatusBar1.SimpleText:=dmMain.LoginRec.ConnectionStatusStr;


  TdmAct_Explorer.Init;
  TdmAct_ColorSchema.Init;
  TdmAct_ClutterModel.Init;

 // row_Google_FileName.Visible := False;

 // Height:=500;
end;

//-------------------------------------------------------------------
procedure Tdlg_Calc_LOS.FormDestroy(Sender: TObject);
//-------------------------------------------------------------------
begin
  cx_VerticalGrid_SaveToReg(cxVerticalGrid1, FRegPath);

  {
  with TRegIniFile.Create(FRegPath) do
  begin
    WriteInteger('', row_ColorSchema_LOS1.Name, FColorSchema_LOS_ID);
    WriteInteger('', row_ClutterModel1.Name,    FClutterModel_ID);


//    WriteString ('', row_Google_FileName.Name,       row_Google_FileName.Properties.Value);
//    WriteInteger('', row_MapRasterTransparence.Name, row_MapRasterTransparence.Properties.Value);

//    Params.Google_FileName := row_Google_FileName.Properties.Value;
//    Params.MapRasterTransparence_0_255 := AsInteger(row_MapRasterTransparence.Properties.Value);


    Free;
  end;
   }

  inherited;
end;


procedure Tdlg_Calc_LOS.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
begin
  inherited;

  act_Ok.Enabled:=(row_ColorSchema_LOS1.Tag>0) and (row_ClutterModel1.Tag>0);
end;


// ---------------------------------------------------------------
procedure Tdlg_Calc_LOS.act_OkExecute(Sender: TObject);
// ---------------------------------------------------------------
var
  iVal: Integer;
begin
  inherited;

  with dmLOS_calc do
  begin
    Params.Use_Site_Pos := (row_Use_Site_Pos1.Properties.Value = True);

    Params.SiteHeight   :=AsFloat(row_Site_H1.Properties.Value);
    Params.TerminalHeight:=AsFloat(row_Terminal_H1.Properties.Value);
 ///////   Params.Freq         :=AsFloat(row_Freq1.Properties.Value);
    Params.Refraction   :=AsFloat(row_Refraction1.Properties.Value);
    Params.CalcStep_m   :=AsInteger(row_CalcStep1.Properties.Value);

    Params.Radius_km    :=AsFloat(row_radius1.Properties.Value);

    Params.Azimuth      :=AsFloat(row_Azimuth.Properties.Value);
    Params.Width        :=AsFloat(row_Width.Properties.Value);

    Params.ColorSchema_LOS_ID:=row_ColorSchema_LOS1.Tag;
    Params.ClutterModel_ID   :=row_ClutterModel1.Tag;

(*
    case row_Map_type.Properties.Value of
      0:  Params.MapType := mtRaster_;
      1:  Params.MapType := mtVector_;
  //    2:  Params.MapType := mtKML_;
   //   3:  Params.MapType := mtKMZ_;
    end;
*)


(*    case Params.MapType of
      mtKML_,
      mtKMZ_: dmLOS_calc.Params.TabFileName1 := row_Google_FileName.Properties.Value;
    end;
*)

//    Params.Google_FileName := row_Google_FileName.Properties.Value;

    iVal:=AsInteger(row_MapRasterTransparence.Properties.Value);

    Params.MapRasterTransparence_0_255 := Round( (255/100) * iVal);

 //   WriteString('', row_Azimuth.Name,      row_Azimuth.Text);
  //  WriteString('', row_Width.Name,        row_Width.Text);

  end;


  dmLOS_calc.ExecuteDlg() ;
end;


//-------------------------------------------------------------------
procedure Tdlg_Calc_LOS.row_ClutterModel1EditPropertiesButtonClick( Sender: TObject; AButtonIndex: Integer);
//-------------------------------------------------------------------
var
  FtmpID: integer;
  FtmpName: Widestring;
  iID: Integer;
  sName: string;
begin
 // Assert(Assigned(IShell));

  //--------------------------------------------
  if cxVerticalGrid1.FocusedRow=row_ColorSchema_LOS1 then
  //--------------------------------------------
  begin
    iID:=row_ColorSchema_LOS1.Tag;

    if Tdlg_DB_select.ExecDlg('', dmMain.ADOConnection, TBL_ColorSchema, iID, sName) then
    begin
      cx_SetButtonEditText(Sender as TcxButtonEdit, sName);
      row_ColorSchema_LOS1.Tag:=iID;
    end;

    {
   Assert(Assigned(dmAct_Explorer), 'Value not assigned');


    if dmAct_Explorer.Dlg_Select_Object (otColorSchema, FColorSchema_LOS_ID, FtmpName) then
    begin
      row_ColorSchema_LOS1.Properties.Value:=FtmpName;
      (Sender as TcxButtonEdit).EditValue:=FtmpName;
    end;
   }

  end else


  //--------------------------------------------
  if cxVerticalGrid1.FocusedRow=row_ClutterModel1 then
  //--------------------------------------------
  begin
    iID:=row_ClutterModel1.Tag;

    if Tdlg_DB_select.ExecDlg('', dmMain.ADOConnection, TBL_ClutterModel, iID, sName) then
    begin
      cx_SetButtonEditText(Sender as TcxButtonEdit, sName);
      row_ClutterModel1.Tag:=iID;
    end;

{
   Assert(Assigned(dmAct_Explorer), 'Value not assigned');

    if dmAct_Explorer.Dlg_Select_Object (otClutterModel, FClutterModel_ID, FtmpName) then
    begin
      row_ClutterModel1.Properties.Value:=FtmpName;
      (Sender as TcxButtonEdit).EditValue:=FtmpName;
    end;
   }

  end;
end;


end.





// ---------------------------------------------------------------
procedure Tdlg_Calc_LOS.row_Map_typeEditPropertiesChange(Sender: TObject);
// ---------------------------------------------------------------
(*var
  iVal: Integer;
  sFileName: string;
*)
begin
(*  sFileName:= AsString( row_Google_FileName.Properties.Value);

  iVal:=row_Map_type.Properties.Value;

  case row_Map_type.Properties.Value of
    2: row_Google_FileName.Properties.Value:= ChangeFileExt(sFileName, '.kml');
    3: row_Google_FileName.Properties.Value:= ChangeFileExt(sFileName, '.kmz');
  end;
  *)

end;
