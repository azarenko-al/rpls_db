unit dm_LOS_calc;

interface

uses
  SysUtils, Classes, Forms, Dialogs,

  I_rel_Matrix1,
  u_rel_engine,


  u_Params_Bin_to_Map,
  u_Params_Bin_To_Map_classes,
  u_Matrix_base,

  u_vars,

  u_func,

  dm_CalcMap,

  dm_Progress,

  dm_Rel_Engine,
  dm_ClutterModel,

  dm_ColorSchema_export,
  dm_Property,

  u_const,

  u_Matrix,
  u_rel_Profile,
  u_rel_Profile_tools,
  u_profile_types,

  u_run,

  
//  u_XML,

  u_geo,
  u_geo_convert_new

  ;

type
  TdmLOS_calc = class(TdmProgress)
//    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FRowCount: integer;
    FColCount: integer;
//    FCellCount: integer;

    FrelProfile: TrelProfile;

    FLosMatrixFileName: string;
//    FTabFileName: string;
    function RegisterCalcMap(aLosMatrix : TLosMatrix): Integer;

  protected
    procedure ExecuteProc; override;

    procedure SaveColorSchemaToXMLFile(aFileName: string);

  public
    Params1: record
       TabFileName: string;
    end;

    Params: record
      Site_BLPoint: TBLPoint;

      Radius_km: double;
      CalcStep_m: integer;
      Refraction: double;

      SiteHeight, TerminalHeight : double;
   //   Freq:   double;

      Azimuth: double;
      Width  : double;

      ColorSchema_LOS_ID: integer;
      ClutterModel_ID: integer;

      Use_Site_Pos: boolean;



//      MapType: (mtRaster_,mtVector_,mtKML_,mtKMZ_);
   //   MapType: (mtRaster_,mtVector_);
      MapRasterTransparence_0_255 : Integer; //0..255

//      Google_FileName : string;

    end;

//    procedure ExecuteDlg;


    class procedure Init;
  end;

var
  dmLOS_calc: TdmLOS_calc;


//====================================================================
// implementation
//====================================================================

implementation

uses Math; {$R *.DFM}


const
  TEMP_FILENAME='bin_to_map.xml';


// -------------------------------------------------------------
class procedure TdmLOS_calc.Init;
// -------------------------------------------------------------
begin
  if not Assigned(dmLOS_calc) then
    dmLOS_calc := TdmLOS_calc.Create(Application);
end;


// ---------------------------------------------------------------
procedure TdmLOS_calc.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;


////  ShowMessage('TdmLOS_calc.DataModuleCreate(Sender: TObject);');

  //  db_SetComponentADOConnection(Self, dmMain.AdoConnection);


{
  Params.BLPoint.B:=59.9966352038686;
  Params.BLPoint.L:=30.1366276631735;
}

  Params.Radius_km:=15;
  Params.SiteHeight:=30;
  Params.TerminalHeight:=5;
//  Params.Freq:=13;
  Params.Refraction:=1.33;
  Params.CalcStep_m:=200;


  FrelProfile:=TrelProfile.Create;

end;



// ---------------------------------------------------------------
procedure TdmLOS_calc.ExecuteProc;
// ---------------------------------------------------------------


    function geo_Azimuth_Delta (aValue1, aValue2: double): double;
    begin
      if aValue1 > aValue2 then
        Result := aValue1 - aValue2
      else
        Result := aValue2 - aValue1;

      if Result>180 then
        Result:=360-Result
    end;


var
  iBest6Zone,iProperty,iRow,iCol: integer;
  dLosValue: double;
  y: double;
  x: double;
   sDir: string;
    
   blRect: TBLRect;
   xyRect: TxyRect;
   antenna_XYPoint: TXYPoint;

   xyVector: TxyVector;
//   xy2: TxyPoint;

  oLosMatrix : TLosMatrix;
  profType: TrelProfileType;

  d,dAzim: double;
  blPoint2,newBLPoint: TBLPoint;

  blPoints: TBLPointArrayF;
  eDist_km: Double;
  iID: Integer;
  k: Integer;
  sName: string;
  sXmlFile: string;

  rec: TBuildProfileParamRec;


 // map_rec: TdmCalcMapAddRec;

 // rProfile: TrelBuildProfileRec;
begin
  dmClutterModel.AssignClutterModel (Params.ClutterModel_ID, FrelProfile.Clutters);

  Randomize;
  k:=RandomRange(0, High(Integer));

  Params1.TabFileName:= ChangeFileExt(Params1.TabFileName, Format('_%d.tab', [k]));


//  FTabFileName:=Params.TabFileName;

  FLosMatrixFileName:=ChangeFileExt(Params1.TabFileName, '.int');


  if Params.Use_Site_Pos then
  begin
    if dmProperty.GetNearestIDandPos (Params.Site_BLPoint, newBLPoint, sName)>0 then
      Params.Site_BLPoint:=newBLPoint;
  end;


//  blRect:=u_geo_lib.geo_MakeBLRectByCircle (Params.BLPoint, 1000*Params.Radius);


  geo_MakeBLPointsByCircle (Params.Site_BLPoint, 1000*Params.Radius_km, blPoints);

  blRect:=geo_RoundBLPointsToBLRect_F (blPoints);

//  blRect:=u_geo_lib.geo_MakeBLRectByCircle (Params.BLPoint, 1000*Params.Radius);


  // ---------------------------------------------------------------

  DoLog ('���������� ������ �����');
  dmRel_Engine.Open_All;
//  dmRel_Engine.OpenByRect (blRect);

  if GetReliefEngine.Count=0 then
  begin
    ShowMessage('��� ������ ����� !');
    exit;
  end;

  iBest6Zone:=dmRel_Engine.Best6Zone;

  iBest6Zone:=geo_Get6ZoneBL(Params.Site_BLPoint);

  // ---------------------------------------------

  xyRect:=geo_RoundBLPointsToXYRect (blPoints, iBest6Zone);



//  xyRect:=geo_GetRoundXYRect (blRect, iBest6Zone);

  antenna_XYPoint:=geo_Pulkovo42_BL_to_XY(Params.Site_BLPoint, iBest6Zone);

  FRowCount:=Round((xyRect.TopLeft.X - xyRect.BottomRight.X)/Params.CalcStep_m);
  FColCount:=Round((xyRect.BottomRight.Y - xyRect.TopLeft.Y)/Params.CalcStep_m);
  // �� ������ ���� ����� ��������
  if FRowCount<0 then FRowCount:=0;
  if FColCount<0 then FColCount:=0;

//  FCellCount:=FRowCount * FColCount;

  // ---------------------------------------------------------------

 // ShowMessage('0');




  oLosMatrix := TLosMatrix.Create;

  oLosMatrix.SetMatrixSize(FRowCount, FColCount);
  oLosMatrix.ZoneNum   := iBest6Zone;
  oLosMatrix.TopLeft.X := xyRect.TopLeft.X;
  oLosMatrix.TopLeft.Y := xyRect.TopLeft.Y;
  oLosMatrix.Step      := Params.CalcStep_m;

  oLosMatrix.FillBlank;

  // ---------------------------------------------------------------

  for iRow:=0 to FRowCount-1 do
//  for iCol:=Round (FColCount/2)-1  to Round (FColCount/2)+10 do
  for iCol:=0 to FColCount-1 do
//  for iCol:=FColCount-1 downto 0 do
    if not Terminated then
  begin
//    if FColCount then


    // dAzim:=geo_Azimuth ( blPoint2, Params.BLPoint);


     if iCol=0 then
       DoProgress (iRow*FColCount, FRowCount*FColCount);

//     if iCol=0 then
 //      DoProgress (iRow*FColCount, FRowCount*FColCount);


     x := xyRect.TopLeft.X - (iRow + 1/2) * Params.CalcStep_m;
     y := xyRect.TopLeft.Y + (iCol + 1/2) * Params.CalcStep_m;

     xyVector.Point1:=antenna_XYPoint;
     xyVector.Point2.X:=x;
     xyVector.Point2.Y:=y;

     eDist_km:=geo_DistanceXY_m(xyVector. Point1, xyVector.Point2) / 1000;

//     eDist_km:=geo_Distance_km(xyVector.Point1, xyVector.Point2);

     if eDist_km>Params.Radius_km then
       Continue;
       


//     xy2.X := x;
//     xy2.Y := y;

     // -------------------------
     if Params.Width < 360 then
     // -------------------------
     begin
       blPoint2:=geo_Pulkovo42_XY_to_BL (xyVector.Point2, iBest6Zone);

 //      blPoint2:=geo_XY_to_BL (xy2);

//       dAzim:=geo_Azimuth (Params.BLPoint, blPoint2);
//       dAzim:=geo_Azimuth ( blPoint2, Params.Site_BLPoint);
       dAzim:=geo_Azimuth ( Params.Site_BLPoint, blPoint2);

{
       if iCol=FColCount-2 then
         d:=2;
}
       d:=geo_Azimuth_Delta (Params.Azimuth, dAzim);

       if (d > Params.Width/2)
       then
         Continue;
     end;

{
     FProfileRec.ProfileRec:=FrelProfile.Data;
     FProfileRec.XYVector  :=xyVector;
     FProfileRec.Step      :=Params.CalcStep;
     FProfileRec.Refraction:=Params.Refraction;
     FProfileRec.Zone      :=0;
     FProfileRec.MaxDistance:=1000*Params.Radius;
}

   FillChar(rec,SizeOf(rec),0);

   rec.XYVector := xyVector;
   rec.Step_m   := Params.CalcStep_m;
   rec.Zone     := iBest6Zone;
   rec.refraction  := Params.Refraction;
   rec.MaxDistance_KM  := Params.Radius_km;

   if GetReliefEngine.BuildProfileXY (FrelProfile.Data,
       rec,
       xyVector, Params.CalcStep_m,
       iBest6Zone, Params.Refraction, Params.Radius_km)

//     if rel_BuildProfileXY (FProfileRec)
   // aProfile.ApplyDefaultHeights;

    // if BuildProfileLine (antenna_XYPoint.x, antenna_XYPoint.y, x,y)
     then begin
         FrelProfile.ApplyDefaultClutterHeights;

     //  if IsCalcLos then

         profType:=TRelProfile_tools.GetType_with_Los (FrelProfile, Params.SiteHeight, Params.TerminalHeight,
                                 0, dLosValue);

(*
         profType:=FrelProfile.GetType_with_Los (Params.SiteHeight, Params.TerminalHeight,
                                 0, dLosValue);
                               //  Params.Freq, dLosValue);
*)

//        if ABS(dLosValue)>high(Shortint) then
 //         dLosValue:=high(Shortint);


         oLosMatrix[iRow,iCol]:= -Round(dLosValue);
     end;

  end;


 // ShowMessage('1');


  // ---------------------------------------------------------------
  if not Terminated then
  // ---------------------------------------------------------------
  begin
  //  oLosMatrix.SaveToTxtFile (FLosFileName+'_');
    oLosMatrix.SaveToFile (FLosMatrixFileName);

    sXmlFile := g_ApplicationDataDir + TEMP_FILENAME;

//  ShowMessage('SaveColorSchemaToXMLFile (sXmlFile);');
    SaveColorSchemaToXMLFile (sXmlFile);

    RunApp (EXE_RPLS_BIN_TO_MIF, DoubleQuotedStr(sXmlFile));


    RegisterCalcMap(oLosMatrix);

  end;


  FreeAndNil(oLosMatrix);
end;

// ---------------------------------------------------------------
function TdmLOS_calc.RegisterCalcMap(aLosMatrix : TLosMatrix): Integer;
// ---------------------------------------------------------------
//

var
 // k: Integer;
//  iID: Integer;
  map_rec: TdmCalcMapAddRec1;
  s: string;

  xyBounds: TXYRect;

begin

  FillChar(map_rec,SizeOf(map_rec),0);
//    map_rec.pro

s:=FormatDateTime('hh:mm', Now());


  s:=geo_FormatBLPoint(params.Site_BLPoint) + ' '+ FormatDateTime('(hh:mm)', Now());




 // k:=FNV1aHash(s);
//  k:=FNV1aHash(s);



  map_rec.Section := 'LOS';
  map_rec.Name    := '����� LOS : '+ s;

//  Randomize;
//  k:=RandomRange(0, High(Integer));

  //map_rec.TabFileName:= ChangeFileExt(Params1.TabFileName12, Format('_%d.tab', [k]));
  map_rec.TabFileName:= Params1.TabFileName;

  map_rec.Matrix_FileName:= FLosMatrixFileName;


 // map_rec.KeyName := 'los';
  map_rec.Checked := True;

  xyBounds := aLosMatrix.GetXYBounds;

  map_rec.Zone  := aLosMatrix.ZoneNum;
  map_rec.x_min := xyBounds.BottomRight.X;
  map_rec.x_max := xyBounds.TopLeft.X;
  map_rec.y_min := xyBounds.TopLeft.Y;
  map_rec.y_max := xyBounds.BottomRight.Y;

//    map_rec.PmpCalcRegionID


//   0,  '����� LOS', FTabFileName, 'los', False, True
  Result := dmCalcMap.RegisterCalcMap_ (map_rec);

  // TODO -cMM: TdmLOS_calc.RegisterCalcMap default body inserted
end;


//--------------------------------------------------------------
procedure TdmLOS_calc.SaveColorSchemaToXMLFile(aFileName: string);
//--------------------------------------------------------------
//const
 // TAG_COLOR_SCHEMAS = 'COLOR_SCHEMAS';

var
  b: Boolean;
  oMatrixFile: u_Params_Bin_To_Map_classes.TBinMatrixFile;
  oParams: u_Params_Bin_To_Map.TParams_Bin_to_Map1;

begin

  oParams:=TParams_Bin_to_Map1.Create;

  oParams.Transparence := params.MapRasterTransparence_0_255;

  {
    case Params.MapType of
      0:  oParams.MapType := mtRaster;
      1:  oParams.MapType := mtVector;
      2:  oParams.MapType := mtKML;
      3:  oParams.MapType := mtKMZ;
    end;
   }

  oParams.MapExportType :=mtRaster;

//
//  case Params.MapType of
//    mtRaster_ : oParams.MapType :=mtRaster;
//    mtVector_ : oParams.MapType :=mtVector;
////    mtKML_    : oParams.MapType :=mtKML;
////    mtKMZ_    : oParams.MapType :=mtKMZ;
//  end;


  b:=dmColorSchema_export.ExportItemToClass(Params.ColorSchema_LOS_ID, oParams.ColorSchemaList);


  oMatrixFile:=oParams.BinMatrixArrayList.AddItem;
  oMatrixFile.MatrixFileName :=FLosMatrixFileName;
  oMatrixFile.MapFileName    :=Params1.TabFileName;
  oMatrixFile.ColorSchema_ID :=Params.ColorSchema_LOS_ID;

//  -----------------------

  oParams.SaveToXMLFile(aFileName);

  FreeAndNil(oParams);



end;



end.



