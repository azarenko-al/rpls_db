inherited dlg_LosRegion: Tdlg_LosRegion
  Left = 1072
  Top = 281
  Width = 539
  Height = 475
  Caption = 'dlg_LosRegion'
  OldCreateOrder = True
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 412
    Width = 531
    inherited Bevel1: TBevel
      Width = 531
    end
    inherited Panel3: TPanel
      Left = 344
      Width = 187
      inherited btn_Ok: TButton
        Left = 16
        Action = act_Calc
        ModalResult = 0
      end
      inherited btn_Cancel: TButton
        Left = 100
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 531
    inherited Bevel2: TBevel
      Width = 531
    end
    inherited pn_Header: TPanel
      Width = 531
    end
  end
  object PageControl1: TPageControl [2]
    Left = 0
    Top = 60
    Width = 531
    Height = 277
    ActivePage = TabSheet_Params
    Align = alTop
    TabOrder = 2
    Visible = False
    object TabSheet_Params: TTabSheet
      Caption = 'Params'
      object cxVerticalGrid1: TcxVerticalGrid
        Left = 0
        Top = 0
        Width = 523
        Height = 249
        BorderStyle = cxcbsNone
        Align = alClient
        LookAndFeel.Kind = lfOffice11
        OptionsView.CellTextMaxLineCount = 3
        OptionsView.AutoScaleBands = False
        OptionsView.PaintStyle = psDelphi
        OptionsView.GridLineColor = clBtnShadow
        OptionsView.RowHeaderMinWidth = 30
        OptionsView.RowHeaderWidth = 226
        OptionsView.ValueWidth = 40
        TabOrder = 0
        Version = 1
        object cxEditorRow1: TcxEditorRow
          Expanded = False
          Properties.Caption = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1099
          Properties.EditPropertiesClassName = 'TcxTextEditProperties'
          Properties.EditProperties.Alignment.Horz = taLeftJustify
          Properties.EditProperties.Alignment.Vert = taVCenter
          Properties.EditProperties.MaxLength = 0
          Properties.EditProperties.ReadOnly = True
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
          Visible = False
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object cxEditorRow2: TcxEditorRow
          Expanded = False
          Properties.Caption = #1042#1079#1103#1090#1100' '#1073#1083#1080#1078#1072#1081#1096#1091#1102' '#1087#1083#1086#1097#1072#1076#1082#1091
          Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
          Properties.EditProperties.Alignment = taLeftJustify
          Properties.EditProperties.NullStyle = nssUnchecked
          Properties.EditProperties.ReadOnly = False
          Properties.EditProperties.ValueChecked = 'True'
          Properties.EditProperties.ValueGrayed = ''
          Properties.EditProperties.ValueUnchecked = 'False'
          Properties.DataBinding.ValueType = 'Variant'
          Properties.Value = 'True'
          Visible = False
          ID = 1
          ParentID = -1
          Index = 1
          Version = 1
        end
        object cxCategoryRow1: TcxCategoryRow
          Expanded = False
          Visible = False
          ID = 2
          ParentID = -1
          Index = 2
          Version = 1
        end
        object cxCategoryRow2: TcxCategoryRow
          Properties.Caption = #1057#1090#1072#1085#1094#1080#1103
          Visible = False
          ID = 3
          ParentID = -1
          Index = 3
          Version = 1
        end
        object cxEditorRow3: TcxEditorRow
          Expanded = False
          Properties.Caption = #1042#1099#1089#1086#1090#1072' '#1072#1085#1090#1077#1085#1085#1099' ('#1084')'
          Properties.EditPropertiesClassName = 'TcxTextEditProperties'
          Properties.EditProperties.Alignment.Horz = taLeftJustify
          Properties.EditProperties.Alignment.Vert = taVCenter
          Properties.EditProperties.MaxLength = 0
          Properties.EditProperties.ReadOnly = False
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = '30'
          ID = 4
          ParentID = 3
          Index = 0
          Version = 1
        end
        object cxCategoryRow3: TcxCategoryRow
          Properties.Caption = #1058#1077#1088#1084#1080#1085#1072#1083
          Visible = False
          ID = 5
          ParentID = -1
          Index = 4
          Version = 1
        end
        object cxEditorRow4: TcxEditorRow
          Expanded = False
          Properties.Caption = #1042#1099#1089#1086#1090#1072' '#1072#1085#1090#1077#1085#1085#1099' ('#1084')'
          Properties.EditPropertiesClassName = 'TcxTextEditProperties'
          Properties.EditProperties.Alignment.Horz = taLeftJustify
          Properties.EditProperties.Alignment.Vert = taVCenter
          Properties.EditProperties.MaxLength = 0
          Properties.EditProperties.ReadOnly = False
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = '2'
          ID = 6
          ParentID = 5
          Index = 0
          Version = 1
        end
        object cxEditorRow5: TcxEditorRow
          Expanded = False
          Properties.EditPropertiesClassName = 'TcxTextEditProperties'
          Properties.EditProperties.Alignment.Horz = taLeftJustify
          Properties.EditProperties.Alignment.Vert = taVCenter
          Properties.EditProperties.MaxLength = 0
          Properties.EditProperties.ReadOnly = False
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
          Visible = False
          ID = 7
          ParentID = -1
          Index = 5
          Version = 1
        end
        object row__Step: TcxEditorRow
          Expanded = False
          Properties.Caption = #1064#1072#1075' '#1088#1072#1089#1095#1077#1090#1072' [m]'
          Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
          Properties.EditProperties.MaxValue = 1000.000000000000000000
          Properties.EditProperties.MinValue = 1.000000000000000000
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = '100'
          ID = 8
          ParentID = -1
          Index = 6
          Version = 1
        end
        object cxEditorRow7: TcxEditorRow
          Expanded = False
          Properties.Caption = #1063#1072#1089#1090#1086#1090#1072' [GHz)'
          Properties.EditPropertiesClassName = 'TcxTextEditProperties'
          Properties.EditProperties.Alignment.Horz = taLeftJustify
          Properties.EditProperties.Alignment.Vert = taVCenter
          Properties.EditProperties.MaxLength = 0
          Properties.EditProperties.ReadOnly = False
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = '10'
          Visible = False
          ID = 9
          ParentID = -1
          Index = 7
          Version = 1
        end
        object row_Max_distance_km: TcxEditorRow
          Properties.Caption = #1054#1075#1088#1072#1085#1080#1095#1077#1085#1080#1077' '#1076#1072#1083#1100#1085#1086#1089#1090#1080' '#1088#1072#1089#1095#1077#1090#1072' [km]'
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = '200'
          ID = 10
          ParentID = -1
          Index = 8
          Version = 1
        end
        object row__Refraction: TcxEditorRow
          Expanded = False
          Properties.Caption = #1056#1077#1092#1088#1072#1082#1094#1080#1103
          Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
          Properties.EditProperties.ValueType = vtFloat
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = '1.33'
          ID = 11
          ParentID = -1
          Index = 9
          Version = 1
        end
        object cxEditorRow9: TcxEditorRow
          Expanded = False
          Properties.EditPropertiesClassName = 'TcxTextEditProperties'
          Properties.EditProperties.Alignment.Horz = taLeftJustify
          Properties.EditProperties.Alignment.Vert = taVCenter
          Properties.EditProperties.MaxLength = 0
          Properties.EditProperties.ReadOnly = False
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
          ID = 12
          ParentID = -1
          Index = 10
          Version = 1
        end
        object row_ClutterModel: TcxEditorRow
          Expanded = False
          Properties.Caption = #1052#1086#1076#1077#1083#1100' '#1090#1080#1087#1086#1074' '#1084#1077#1089#1090#1085#1086#1089#1090#1080
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Alignment.Horz = taLeftJustify
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.ReadOnly = True
          Properties.EditProperties.OnButtonClick = row_ClutterModelEditPropertiesButtonClick
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
          ID = 13
          ParentID = -1
          Index = 11
          Version = 1
        end
        object row_ColorSchemaa: TcxEditorRow
          Expanded = False
          Properties.Caption = #1062#1074#1077#1090#1086#1074#1099#1077' '#1089#1093#1077#1084#1099
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Alignment.Horz = taLeftJustify
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.ReadOnly = True
          Properties.EditProperties.OnButtonClick = row_ClutterModelEditPropertiesButtonClick
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
          Visible = False
          ID = 14
          ParentID = -1
          Index = 12
          Version = 1
        end
        object cxEditorRow12: TcxEditorRow
          Expanded = False
          Properties.EditPropertiesClassName = 'TcxTextEditProperties'
          Properties.EditProperties.Alignment.Horz = taLeftJustify
          Properties.EditProperties.Alignment.Vert = taVCenter
          Properties.EditProperties.MaxLength = 0
          Properties.EditProperties.ReadOnly = False
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
          Visible = False
          ID = 15
          ParentID = -1
          Index = 13
          Version = 1
        end
        object cxEditorRow13: TcxEditorRow
          Expanded = False
          Properties.Caption = #1056#1072#1076#1080#1091#1089' '#1088#1072#1089#1095#1077#1090#1072' [km]'
          Properties.EditPropertiesClassName = 'TcxTextEditProperties'
          Properties.EditProperties.Alignment.Horz = taLeftJustify
          Properties.EditProperties.Alignment.Vert = taVCenter
          Properties.EditProperties.MaxLength = 0
          Properties.EditProperties.ReadOnly = False
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = '20'
          Visible = False
          ID = 16
          ParentID = -1
          Index = 14
          Version = 1
        end
        object cxCategoryRow4: TcxCategoryRow
          Expanded = False
          Visible = False
          ID = 17
          ParentID = -1
          Index = 15
          Version = 1
        end
        object cxEditorRow14: TcxEditorRow
          Expanded = False
          Properties.Caption = #1040#1079#1080#1084#1091#1090' ('#1075#1088')'
          Properties.EditPropertiesClassName = 'TcxTextEditProperties'
          Properties.EditProperties.Alignment.Horz = taLeftJustify
          Properties.EditProperties.Alignment.Vert = taVCenter
          Properties.EditProperties.MaxLength = 0
          Properties.EditProperties.ReadOnly = False
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = '0'
          Visible = False
          ID = 18
          ParentID = -1
          Index = 16
          Version = 1
        end
        object cxEditorRow15: TcxEditorRow
          Expanded = False
          Properties.Caption = #1064#1080#1088#1080#1085#1072' '#1079#1086#1085#1099' ('#1075#1088')'
          Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
          Properties.EditProperties.Alignment.Horz = taLeftJustify
          Properties.EditProperties.Alignment.Vert = taVCenter
          Properties.EditProperties.AssignedValues.MinValue = True
          Properties.EditProperties.MaxValue = 360.000000000000000000
          Properties.EditProperties.ReadOnly = False
          Properties.DataBinding.ValueType = 'Float'
          Properties.Value = 360.000000000000000000
          Visible = False
          ID = 19
          ParentID = -1
          Index = 17
          Version = 1
        end
      end
    end
    object TabSheet_Cells: TTabSheet
      Caption = 'Cells'
      ImageIndex = 1
    end
    object TabSheet_Results: TTabSheet
      Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090#1099' '#1088#1072#1089#1095#1077#1090#1072
      ImageIndex = 2
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 296
    inherited act_Cancel: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
    end
    object act_Calc: TAction
      Caption = #1056#1072#1089#1095#1077#1090
      OnExecute = Action1Execute
    end
    object Action1: TAction
      Caption = #1056#1072#1089#1095#1077#1090
      OnExecute = Action1Execute
    end
  end
  inherited FormStorage1: TFormStorage
    Left = 348
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 399
  end
end
