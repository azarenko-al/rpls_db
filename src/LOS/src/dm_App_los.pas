unit dm_App_los;

interface

uses
  SysUtils, Classes, Dialogs,

  dm_User_Security,



  u_Ini_LOS_Params,

  d_LosRegion,
  d_Calc_LOS,

  dm_LOS_calc,
  dm_LOS_calc_m,

  u_vars,

  dm_Main
  ;

type
  TdmMain_app = class(TDataModule)
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  end;                                                


var
  dmMain_app: TdmMain_app;

//===================================================================
// implementation
//===================================================================
implementation
 {$R *.DFM}

//--------------------------------------------------------------
procedure TdmMain_app.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const
  DEF_TEMP_FILENAME = 'los.ini';

var
  sCaption: string;
  sKey: string;
  iID: integer;
//  iCount: integer;
  I: integer;
//  iMode: integer;
//  iMapDesktopID: integer;
  b: boolean;
  sTabFileName: string;
//  iProjectID: integer;
  sIniFileName: string;

begin

//  sIniFileName:=GetCommonApplicationDataDir(DEF_SOFTWARE_NAME)+
 //                 DEF_TEMP_FILENAME;
                                    

  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
    sIniFileName:=g_ApplicationDataDir+ DEF_TEMP_FILENAME;


  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;

  //---------------------------------------------------------------------


  g_Ini_LOS_Params:=TIni_LOS_Params.Create;
  if not g_Ini_LOS_Params.LoadFromFile(sIniFileName) then
    Exit;
         


  TdmMain.Init;
  if not dmMain.OpenDB_reg then
//  if not dmMain.OpenFromReg then
    Exit;



 {$DEFINE use_dll11111}

  {$IFDEF use_dll}
  // -----------------------------
  if Load_ILos() then
  begin
    ILos.InitADO(dmMain.ADOConnection1.ConnectionObject);
    ILos.Execute(sIniFileName);

    UnLoad_ILos();
  end;

  Exit;
  {$ENDIF}


  TdmLOS_calc.Init;
  TdmLOS_calc_m.Init;

  TdmUser_Security.Init;


  dmMain.ProjectID:=g_Ini_LOS_Params.ProjectID;



  case g_Ini_LOS_Params.Mode of
    mtLos_: begin
         dmLOS_calc.Params.Site_BLPoint  :=g_Ini_LOS_Params.blPoint;
         dmLOS_calc.Params1.TabFileName:=g_Ini_LOS_Params.TabFileName;

     //    dmLOS_calc.Params.TabFileName:=sTabFileName;

         b:=Tdlg_Calc_LOS.ExecDlg(g_Ini_LOS_Params.blPoint);

         sKey:='los';
         sCaption:='����� LOS';

       end;

    mtLosM_: begin
         dmLOS_calc_m.Params.TabFileName:=g_Ini_LOS_Params.TabFileName;

         Tdlg_LosRegion.ExecDlg(g_Ini_LOS_Params.BLPoints);

         sKey:='los_m';
         sCaption:='����� LOS ��� ������ ��';

       end;
  else
     Exit;
  end;



//  FreeAndNil(g_Ini_LOS_Params);

 // TdmLOS_calc.Init;

//  if iMode=1 then
  //  dmLOS_calc_m.Params.FileName:=sTabFileName;

///zzzzzzzzzzzz/

(*  if g_Ini_LOS_Params.Mode of
    mtLos_: begin
*)

 // sKey:=IIF(iMode=0,'los','los_m');
//  sCaption:=IIF(iMode=0,'����� LOS','����� LOS ��� ������ ��');

//  iID:=dmCalcMap.RegisterCalcMap (rec, 0,0, sCaption, sTabFileName, sKey, True); //False,
 // dmCalcMap_View111.SetChecked(iMapDesktopID, iID, True);


//zzzzzzzzz  ini_WriteBool (sIniFileName, 'main', 'result', b);


//  ExitProcess(�0)�


{
if bIsConverted then
    ExitProcess(1)
  else
    ExitProcess(0);}

end;



procedure TdmMain_app.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(g_Ini_LOS_Params);
end;



end.

