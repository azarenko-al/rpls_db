unit dm_LOS_calc_m;

interface

uses
  SysUtils, Classes, Graphics, Forms, Dialogs,

  I_rel_Matrix1,
   u_rel_engine,


  dm_CalcMap,


  dm_Progress,

  dm_Rel_Engine,
  dm_ClutterModel,

 // dm_ColorSchema_export,

  u_MapX,
  u_MapX_lib,
 // u_mitab,

//  u_XML,
  u_rel_Profile,
  u_rel_Profile_tools,
  u_profile_types,

  u_geo

  ;

type
  TdmLOS_calc_m = class(TdmProgress)
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FRowCount: integer;
    FColCount: integer;
    FCellCount: integer;

    FrelProfile: TrelProfile;

    FLosFileName: string;
//    FTabFileName: string;
    procedure RegisterMap;

  protected
    procedure ExecuteProc; override;

 //   procedure SaveToXMLFile(aFileName: string);
  public
    Params: record
      CalcStep_m      : integer;
      Refraction      : double;
      ClutterModelID  : integer;
      TabFileName     : string;

      Max_distance_km : Integer;

{      BLPoint: TBLPoint;
      Radius: double;}
    //  SiteHeight, TerminalHeight : double;
//      Freq:   double;

  //    Azimuth: double;
  //    Width  : double;

//      ColorSchemaID_LOS: integer;

    //  Use_Site_Pos: boolean;

    end;

    Sites: array of record
                 ID      : integer;
                 Name    : string;
                 Height  : integer;
                 BLPoint : TBLPoint;
               end;

    OutMatrix: array of array of
               record
              //   ID1,ID2: integer;
                 LosValue: integer;

//               Height : integer;
 //              BLPoint: TBLPoint;
               end;


    procedure CreateMap;

    class procedure Init;

    procedure InitOutMatrix;
    procedure SetOutMatrixValue(aSite1, aSite2, aValue: Integer);
  end;

var
  dmLOS_calc_m: TdmLOS_calc_m;


const
   DEF_NULL_VALUED=999;

//====================================================================
// implementation
//====================================================================

implementation  {$R *.DFM}

const

  DEF_COLOR_LOSS    = clLime;
  DEF_COLOR_NO_LOSS = clRed;


// -------------------------------------------------------------
class procedure TdmLOS_calc_m.Init;
// -------------------------------------------------------------
begin
  if not Assigned(dmLOS_calc_m) then
    dmLOS_calc_m := TdmLOS_calc_m.Create(Application);
end;

// ---------------------------------------------------------------
procedure TdmLOS_calc_m.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;

//    db_SetComponentADOConnection(Self, dmMain.AdoConnection);


{
  Params.BLPoint.B:=59.9966352038686;
  Params.BLPoint.L:=30.1366276631735;
}

{  Params.Radius:=15;
  Params.SiteHeight:=30;
  Params.TerminalHeight:=5;}

 // Params.Freq:=13;
  Params.Refraction:=1.33;
  Params.CalcStep_m:=200;


  FrelProfile:=TrelProfile.Create;

end;


procedure TdmLOS_calc_m.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FrelProfile);

  inherited;
end;


// ---------------------------------------------------------------
procedure TdmLOS_calc_m.ExecuteProc;
// ---------------------------------------------------------------

var
  iSite2: integer;
  iSite1: integer;
  i: integer;
  dLosValue: Double;
   sDir: string;

   blRect: TBLRect;
   blVector: TblVector;

  profType: TrelProfileType;

  blPoints: TBLPointArrayF;
  eDistance_km: Double;


 rec: TBuildProfileParamRec;

begin
  Assert(Params.ClutterModelID>0);
  Assert(Params.Max_distance_km>0, 'Value <=0');


  dmClutterModel.AssignClutterModel (Params.ClutterModelID, FrelProfile.Clutters);


  blPoints.Count:=Length(Sites);

  for I := 0 to Length(Sites) - 1 do
    blPoints.Items[i] := Sites[i].BLPoint;


  blRect:=geo_RoundBLPointsToBLRect_F (blPoints);

  // ---------------------------------------------------------------

  DoLog ('���������� ������ �����');
  dmRel_Engine.Open;// OpenByRect (blRect);

  if GetReliefEngine.Count=0 then
  begin
    ShowMessage('��� ������ ����� !');
    exit;
  end;

  // ---------------------------------------------------------------

  InitOutMatrix();

  //---------------------------------------------------------

  for iSite1:=0 to High(Sites) do
   for iSite2:=iSite1+1 to High(Sites) do
//  for iCol:=0 to FColCount-1 do
    if not Terminated then
  begin
    DoProgress (iSite1, High(Sites));

    if (Sites[iSite1].Height=0) or
       (Sites[iSite2].Height=0) then
    begin
      SetOutMatrixValue(iSite1,iSite2, DEF_NULL_VALUED);
      Continue;
    end;


     blVector.Point1:=Sites[iSite1].BLPoint;
     blVector.Point2:=Sites[iSite2].BLPoint;

     eDistance_km:=geo_Distance_km(blVector);


    if eDistance_km > Params.Max_distance_km  then
    begin
      SetOutMatrixValue(iSite1,iSite2, DEF_NULL_VALUED);
      Continue;
    end;



     FillChar(rec,SizeOf(rec),0);

     rec.blVector := blVector;
     rec.Step_m   := Params.CalcStep_m;
    // rec.Zone     := iBest6Zone;
     rec.refraction  := Params.Refraction;
     rec.MaxDistance_KM  := 0;//Params.Radius_km;



    // if BuildProfileLine (antennaXYPoint.x, antennaXYPoint.y, x,y)
     if GetReliefEngine.BuildProfileBL (FrelProfile.Data, rec,
          blVector, Params.CalcStep_m, Params.Refraction, 0
          )
     then begin
        FrelProfile.ApplyDefaultClutterHeights;

     //  if IsCalcLos then
//        profType:=FrelProfile.GetType_with_Los (Sites[iSite1].Height, Sites[iSite2].Height,
        profType:=TRelProfile_tools.GetType_with_Los (FrelProfile,
                            Sites[iSite1].Height, Sites[iSite2].Height,
                                 100, //freq
                                  dLosValue);

        SetOutMatrixValue(iSite1,iSite2, -Round(dLosValue));
     end;

  end;

  CreateMap();

  RegisterMap();


end;


//-------------------------------------------------------------------
procedure TdmLOS_calc_m.CreateMap;
//-------------------------------------------------------------------
var
  cl: integer;
  j: integer;
  I: integer;

  oMap: TmiMap;
  blVector: TblVector;
  miStyle: TmiStyleRec;
const
  FLD_VALUE = 'value'; 
begin

//  oMap:=TmitabMap.Create;
  oMap:=TmiMap.Create;

  FillChar(miStyle, SizeOf(miStyle), 0);

  miStyle.LineWidth:=1;
  miStyle.LineStyle:=MI_PEN_PATTERN_SOLId;

  oMap.CreateFile1 (Params.TabFileName,      
                   [mapX_Field (FLD_VALUE, miTypeInt)]);

  oMap.NumericCoordSys_Set_DATUM_KRASOVKSY42();


  for i:=0 to High(Sites) do
    for j:=i+1 to High(Sites) do
    begin
      blVector.Point1:=Sites[i].BLPoint;
      blVector.Point2:=Sites[j].BLPoint;


      if OutMatrix[I,j].LosValue>=0 then
      begin
        miStyle.LineColor:=DEF_COLOR_LOSS;
        miStyle.LineStyle:=MI_PEN_PATTERN_DASH;
      end
        else
      begin
        miStyle.LineColor:=DEF_COLOR_NO_LOSS;
        miStyle.LineStyle:=MI_PEN_PATTERN_SOLId;
      end;

//      miStyle.LineColor:=cl;

      oMap.PrepareStyle(miFeatureTypeLine, miStyle);

//      oMap.WriteVector (blVector, // miStyle,
      oMap.WriteLine (blVector, // miStyle,
                     [mapx_Par(FLD_VALUE, OutMatrix[I,j].LosValue)
                     ]);

    end;


  oMap.CloseFile;
  oMap.Free;

end;


procedure TdmLOS_calc_m.InitOutMatrix;
var
  i: integer;
begin
  SetLength (OutMatrix,  Length(Sites) );
  for i:=0 to High(Sites) do
    SetLength (OutMatrix[i],  Length(Sites));

end;

// ---------------------------------------------------------------
procedure TdmLOS_calc_m.RegisterMap;
// ---------------------------------------------------------------
var
  iID: Integer;
  map_rec: TdmCalcMapAddRec1;

begin
  FillChar(map_rec,SizeOf(map_rec),0);

//    map_rec.pro
  map_rec.Section := 'LOS';
  map_rec.Name    := '����� LOS ��� ������ ��';
  map_rec.TabFileName:= Params.TabFileName;
//  map_rec.KeyName := 'los_m';
  map_rec.Checked := True;

//    map_rec.PmpCalcRegionID


//   0,  '����� LOS', FTabFileName, 'los', False, True

  iID:=dmCalcMap.RegisterCalcMap_ (map_rec);

end;


procedure TdmLOS_calc_m.SetOutMatrixValue(aSite1, aSite2, aValue: Integer);
begin
  OutMatrix[aSite1,aSite2].LosValue:= aValue;
  OutMatrix[aSite2,aSite1].LosValue:= aValue;

end;



end.



