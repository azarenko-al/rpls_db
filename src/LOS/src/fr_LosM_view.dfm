object frame_LosM_view: Tframe_LosM_view
  Left = 1248
  Top = 409
  Width = 525
  Height = 433
  Caption = 'frame_LosM_view'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 120
  TextHeight = 13
  object cxTreeList1: TcxTreeList
    Left = 0
    Top = 29
    Width = 517
    Height = 148
    Align = alTop
    Bands = <
      item
      end>
    LookAndFeel.Kind = lfFlat
    OptionsData.Editing = False
    TabOrder = 0
    OnCustomDrawDataCell = cxTreeList1CustomDrawDataCell
    object col_Name1: TcxTreeListColumn
      Caption.Text = #1055#1083#1086#1097#1072#1076#1082#1072
      DataBinding.ValueType = 'String'
      Width = 117
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_N1: TcxTreeListColumn
      Caption.Text = #8470
      DataBinding.ValueType = 'String'
      Width = 39
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_LOS: TcxTreeListColumn
      Caption.Text = #1055#1088#1086#1089#1074#1077#1090
      DataBinding.ValueType = 'String'
      Width = 66
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_H: TcxTreeListColumn
      Caption.Text = #1042#1099#1089#1086#1090#1072
      DataBinding.ValueType = 'String'
      Width = 61
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 517
    Height = 29
    Caption = 'ToolBar1'
    TabOrder = 1
    object Button2: TButton
      Left = 0
      Top = 2
      Width = 81
      Height = 22
      Action = act_Refresh
      TabOrder = 3
    end
    object ToolButton1: TToolButton
      Left = 81
      Top = 2
      Width = 8
      Caption = 'ToolButton1'
      Style = tbsSeparator
    end
    object cb_Create_columns: TCheckBox
      Left = 89
      Top = 2
      Width = 80
      Height = 22
      Caption = #1050#1086#1083#1086#1085#1082#1080
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object cb_SubNodes: TCheckBox
      Left = 169
      Top = 2
      Width = 112
      Height = 22
      Caption = #1059#1088#1086#1074#1085#1080' '#1076#1077#1088#1077#1074#1072
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object Button1: TButton
      Left = 281
      Top = 2
      Width = 120
      Height = 22
      Action = FileSaveAs_ver2
      TabOrder = 0
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 220
    Width = 517
    Height = 180
    ActivePage = TabSheet1
    Align = alBottom
    TabOrder = 2
    Visible = False
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      object cxGrid_ver2: TcxGrid
        Left = 0
        Top = 16
        Width = 509
        Height = 136
        Align = alBottom
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGrid1TableView_ver2: TcxGridTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnFiltering = False
          OptionsCustomize.ColumnGrouping = False
          OptionsView.GroupByBox = False
          Styles.OnGetContentStyle = cxGrid1TableView1StylesGetContentStyle
          object cxGrid1TableView_ver2Column_Site1_name: TcxGridColumn
            Caption = #1055#1083#1086#1097#1072#1076#1082#1072'1'
            PropertiesClassName = 'TcxTextEditProperties'
          end
          object cxGrid1TableView_ver2Column_Site1_H: TcxGridColumn
            Caption = #1042#1099#1089#1086#1090#1072' '#1087#1083'.1'
            Width = 71
          end
          object cxGrid1TableView_ver2Column_Site2_name: TcxGridColumn
            Caption = #1055#1083#1086#1097#1072#1076#1082#1072'2'
            PropertiesClassName = 'TcxTextEditProperties'
            Width = 82
          end
          object cxGrid1TableView_ver2Column_Site2_H: TcxGridColumn
            Caption = #1042#1099#1089#1086#1090#1072' '#1087#1083'.2'
            Width = 86
          end
          object cxGrid1TableView_ver2Column_Offset: TcxGridColumn
            Caption = #1055#1088#1086#1089#1074#1077#1090
          end
        end
        object cxGrid_ver2Level1: TcxGridLevel
          GridView = cxGrid1TableView_ver2
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      object cxGrid_ver1: TcxGrid
        Left = 0
        Top = 34
        Width = 509
        Height = 118
        Align = alBottom
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGrid1TableView_ver1: TcxGridTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnFiltering = False
          OptionsCustomize.ColumnGrouping = False
          OptionsView.GroupByBox = False
          Styles.OnGetContentStyle = cxGrid1TableView1StylesGetContentStyle
          object cxGridColumn1: TcxGridColumn
            Caption = #8470
            Width = 45
          end
          object cxGridColumn2: TcxGridColumn
            Caption = #1055#1083#1086#1097#1072#1076#1082#1072
          end
          object cxGridColumn3: TcxGridColumn
            Caption = #1042#1099#1089#1086#1090#1072
          end
          object cxGridColumn4: TcxGridColumn
            Caption = #1055#1088#1086#1089#1074#1077#1090
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGrid1TableView_ver1
        end
      end
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 232
    Top = 96
    object N1: TMenuItem
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1082#1072#1082'...'
    end
  end
  object ActionList1: TActionList
    Left = 264
    Top = 96
    object act_Refresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      OnExecute = act_RefreshExecute
    end
    object act_FileSaveAs1_: TAction
      Category = 'File'
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' Excel...'
    end
    object FileSaveAs_ver2: TFileSaveAs
      Category = 'File'
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' Excel...'
      Hint = 'Save As|Saves the active file with a new name'
      ImageIndex = 30
      OnAccept = FileSaveAs_ver2Accept
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 368
    Top = 128
    PixelsPerInch = 120
    object cxStyle_Red: TcxStyle
      AssignedValues = [svColor]
      Color = clRed
    end
    object cxStyle_Lime: TcxStyle
      AssignedValues = [svColor]
      Color = clLime
    end
  end
end
