unit d_LosRegion;

interface

uses
  Classes, Controls, Forms, ActnList, Registry,  cxVGrid, cxButtonEdit,
  ComCtrls, cxControls, cxInplaceContainer, cxPropertiesStore, rxPlacemnt,

  d_Wizard,

 // I_Shell,
  dm_Main,

 // d_DB_select,
  dm_LOS_calc_m,
 // dm_Act_Explorer,

  d_DB_select,

  u_Geo,
  u_func,
  
  u_cx_VGrid_export,
  u_cx_VGrid,

  u_const_db,
  

  fr_LosRegion_cells,
  fr_LosM_view,

  dm_ClutterModel,

  StdCtrls, ExtCtrls, cxLookAndFeels;

type



  Tdlg_LosRegion = class(Tdlg_Wizard)
    act_Calc: TAction;
    Action1: TAction;
    PageControl1: TPageControl;
    TabSheet_Params: TTabSheet;
    TabSheet_Cells: TTabSheet;
    TabSheet_Results: TTabSheet;
    cxVerticalGrid1: TcxVerticalGrid;
    cxEditorRow1: TcxEditorRow;
    cxEditorRow2: TcxEditorRow;
    cxCategoryRow1: TcxCategoryRow;
    cxCategoryRow2: TcxCategoryRow;
    cxEditorRow3: TcxEditorRow;
    cxCategoryRow3: TcxCategoryRow;
    cxEditorRow4: TcxEditorRow;
    cxEditorRow5: TcxEditorRow;
    row__Step: TcxEditorRow;
    cxEditorRow7: TcxEditorRow;
    row__Refraction: TcxEditorRow;
    cxEditorRow9: TcxEditorRow;
    row_ClutterModel: TcxEditorRow;
    row_ColorSchemaa: TcxEditorRow;
    cxEditorRow12: TcxEditorRow;
    cxEditorRow13: TcxEditorRow;
    cxCategoryRow4: TcxCategoryRow;
    cxEditorRow14: TcxEditorRow;
    cxEditorRow15: TcxEditorRow;
    row_Max_distance_km: TcxEditorRow;


    procedure Action1Execute(Sender: TObject);

   procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
//    procedure DoOnButtonClick(Sender: TObject; AbsoluteIndex: Integer);
//    procedure act_OkExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_CalcExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure row_ClutterModelEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);

  private
    Fframe_lOS_cells: Tframe_LosRegion_cells;
    Fframe_LosM_view: Tframe_LosM_view;

    FClutterModelID,
    FStep: Integer;
    FID: integer;

    procedure Calc;
    procedure TestCalc;
  public

    class function ExecDlg(var aBLPointArr: TBLPointArrayF): Boolean;

  end;


//==================================================================
implementation {$R *.DFM}
//==================================================================

const
  DEF_FIXED_COLS = 4;


//--------------------------------------------------------------------
class function Tdlg_LosRegion.ExecDlg(var aBLPointArr: TBLPointArrayF): Boolean;
//--------------------------------------------------------------------
begin
  with Tdlg_LosRegion.Create(Application) do
  try
    Fframe_lOS_cells.ViewBYPoints (aBLPointArr);
        
    Result:= (ShowModal=mrOk);

  finally
    Free;
  end;
end;

//--------------------------------------------------------------------
procedure Tdlg_LosRegion.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

 // Caption:='������';

  Caption :='������ ��������� | '+ GetAppVersionStr();

  SetActionName ('������ LOS �� ���������');

  PageControl1.Align:=alClient;
  PageControl1.Visible := True;

  btn_Ok.Action:=act_Calc;

  TabSheet_Params.Caption :='���������';
  TabSheet_Cells.Caption  :='��������';

  CreateChildForm(Tframe_LosRegion_cells, Fframe_lOS_cells, TabSheet_cells);
  CreateChildForm(Tframe_LosM_view,       Fframe_LosM_view, TabSheet_Results);


//  cx_VerticalGrid_SaveToReg(cx aRegPath: string);

  cx_VerticalGrid_LoadFromReg(cxVerticalGrid1, FRegPath);


  AddComponentProp(PageControl1, 'ActivePage');
  RestoreFrom();

//  AddComponentProp(row_Step, 'text');
//  AddComponentProp(row_Step, 'text');


  with TRegIniFile.Create(FRegPath) do
  begin
    FClutterModelID:=ReadInteger('', FLD_CLUTTER_MODEL_ID, 0);
    Free;
  end;


  row_ClutterModel.Properties.Value:= dmClutterModel.GetNameByID(FClutterModelID);


//  row_ClutterModel.Text:= gl_DB.GetNameByID(TBL_CLUTTER_MODEL, FClutterModelID);
  if row_ClutterModel.Properties.Value='' then
    FClutterModelID:= 0;

 // row_CalcModel.Text:= 'LOS';

  cx_InitVerticalGrid (cxVerticalGrid1);

end;

//--------------------------------------------------------------------
procedure Tdlg_LosRegion.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  cx_VerticalGrid_SaveToReg(cxVerticalGrid1, FRegPath);


  with TRegIniFile.Create(FRegPath) do
  begin
    WriteInteger('', FLD_CLUTTER_MODEL_ID, FClutterModelID);
    Free;
  end;


  inherited;
end;

//-------------------------------------------------------------------
procedure Tdlg_LosRegion.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//-------------------------------------------------------------------
begin
  act_Calc.Enabled:= (FClutterModelID > 0);
end;


procedure Tdlg_LosRegion.act_CalcExecute(Sender: TObject);
begin
  Calc();
end;



procedure Tdlg_LosRegion.Action1Execute(Sender: TObject);
begin
  Calc();
end;

procedure Tdlg_LosRegion.Button1Click(Sender: TObject);
begin
  Calc();
end;


//-------------------------------------------------------------------
procedure Tdlg_LosRegion.Calc;
//-------------------------------------------------------------------
var
  iLen: integer;
  I: integer;
begin
  Assert(Assigned(dmLOS_calc_m), 'Assigned(dmLOS_calc_m)');


//  Fframe_lOS_cells.SelectAll;
  Fframe_lOS_cells.FillSelectedSites;

  iLen:=Length(Fframe_lOS_cells.SelectedItems);

  SetLength (dmLOS_calc_m.Sites, iLen);


  dmLOS_calc_m.Params.Max_distance_km:=AsInteger(row_Max_distance_km.Properties.Value);
  dmLOS_calc_m.Params.CalcStep_m:=AsInteger(row__Step.Properties.Value);
  dmLOS_calc_m.Params.Refraction:=AsFloat(row__Refraction.Properties.Value);
  dmLOS_calc_m.Params.ClutterModelID:=FClutterModelID;


  for I := 0 to iLen - 1 do
  begin
    dmLOS_calc_m.Sites[i].ID     := Fframe_lOS_cells.SelectedItems[i].id;
    dmLOS_calc_m.Sites[i].Name   := Fframe_lOS_cells.SelectedItems[i].Name;
    dmLOS_calc_m.Sites[i].BLPoint:= Fframe_lOS_cells.SelectedItems[i].BLPoint;
    dmLOS_calc_m.Sites[i].Height := Fframe_lOS_cells.SelectedItems[i].Height;
  end;


  dmLOS_calc_m.ExecuteDlg();

  Fframe_LosM_view.ShowResults();

  PageControl1.ActivePage:=TabSheet_Results;
end;

//-------------------------------------------------------------------
procedure Tdlg_LosRegion.TestCalc;
//-------------------------------------------------------------------
begin
  Fframe_lOS_cells.SelectAll;

  Calc;
end;



procedure Tdlg_LosRegion.row_ClutterModelEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  iID: integer;
  sName: string;
begin
  //------------------
  if cxVerticalGrid1.FocusedRow=row_ClutterModel then
  begin

(*
    if dmAct_Explorer.Dlg_Select_Object (otClutterModel, FClutterModelID, sName) then
    begin
      row_ClutterModel.Properties.Value:=sName;
      (Sender as TcxButtonEdit).EditValue:=sName;
    end;
    *)


    if Tdlg_DB_select.ExecDlg('', dmMain.ADOConnection, TBL_ClutterModel, FClutterModelID, sName) then
    begin
      cx_SetButtonEditText(Sender as TcxButtonEdit, sName);
    end;

  //    row__Clu.Properties.Value:=FtmpName;
    //  FClutterModelID:=FtmpID;


{      cx_

      row_ClutterModel.Properties.Value:=FtmpName;
      (Sender as TcxButtonEdit).EditValue:=FtmpName;
}

  end else

end;



end.




{
  //------------------
  if cxVerticalGrid1.FocusedRow=row__Clu then
  begin
    FtmpID:=Tdlg_DB_select.ExecDlg(TBL_CLUTTER_MODEL, FClutterModelID, FtmpName);
    if FtmpID>0 then begin
  //    row__Clu.Properties.Value:=FtmpName;
      FClutterModelID:=FtmpID;

      row__Clu.Properties.Value:=FtmpName;
      (Sender as TcxButtonEdit).EditValue:=FtmpName;
    end;
  end else



  

//-------------------------------------------------------------------
procedure Tdlg_LosRegion.ShowResults;
//-------------------------------------------------------------------
var
  iValue: integer;
  j: integer;
  iLen: integer;
  I: integer;
  oColumn: TcxTreeListColumn;
  oNode,oNode1: TcxTreeListNode;
  sValue: string;
begin

  iLen:=Length(dmLOS_calc_m.Sites);

  cxTreeList1.BeginUpdate;
  cxTreeList1.Clear;


  while cxTreeList1.ColumnCount>DEF_FIXED_COLS do
    cxTreeList1.Columns[DEF_FIXED_COLS].Free;


  for I := 0 to iLen - 1 do
  begin
    oColumn:=cxTreeList1.CreateColumn();
    oColumn.Caption.Text:= IntToStr(i+1);
    oColumn.Width:=25;
  end;


  for I := 0 to iLen - 1 do
  begin
    oNode:=cxTreeList1.Add;
    oNode.Values[col_Name1.ItemIndex]:=dmLOS_calc_m.Sites[i].Name;
    oNode.Values[col_ID1.ItemIndex]  :=dmLOS_calc_m.Sites[i].ID;
    oNode.Values[col_N1.ItemIndex]   :=IntToStr(i+1);

    for j := 0 to iLen - 1 do
      oNode.Values[DEF_FIXED_COLS+j]:='';

    for j := 0 to iLen - 1 do
        if i<>j then
    begin
      iValue:=dmLOS_calc_m.OutMatrix[i,j].LosValue;
      if iValue<>DEF_NULL_VALUED then
        oNode.Values[DEF_FIXED_COLS+j]:=IntToStr(iValue);

{
        sValue:= IntToStr(iValue)
      else
        sValue:= '';


      oNode.Values[DEF_FIXED_COLS+j]:=sValue;}

    end;


    for j := 0 to iLen - 1 do
    begin
      iValue:=dmLOS_calc_m.OutMatrix[i,j].LosValue;

      if iValue<>DEF_NULL_VALUED then
      begin
        oNode1:=cxTreeList1.AddChild(oNode);
        oNode1.Values[col_Name1.ItemIndex]:=dmLOS_calc_m.Sites[j].Name;
        oNode1.Values[col_ID1.ItemIndex]  :=dmLOS_calc_m.Sites[j].ID;

        oNode1.Values[col_LOS.ItemIndex]  :=iValue;
      end;
    end;
  end;


  cxTreeList1.EndUpdate;


end;




//-------------------------------------------------------------------
procedure Tdlg_LosRegion.cxTreeList1CustomDrawCell(Sender: TObject; ACanvas: TcxCanvas; AViewInfo:
    TcxTreeListEditCellViewInfo; var ADone: Boolean);
//-------------------------------------------------------------------
var
  i: integer;
begin
 { if AViewInfo.Column.ItemIndex>DEF_FIXED_COLS then
    if AViewInfo.Node.Level=0 then
    begin
      if AViewInfo.Node.Values[AViewInfo.Column.ItemIndex]='' then
        Exit;

      i:=AsInteger(AViewInfo.Node.Values[AViewInfo.Column.ItemIndex]);
      if i>=0 then
        ACanvas.Brush.Color:=clLime;
      if i<0 then
        ACanvas.Brush.Color:=clRed;
    end;



  if AViewInfo.Node.Level>0 then
  begin
 //   if AViewInfo.Node.Values[AViewInfo.Column.ItemIndex]='' then
  //    Exit;

    i:=AsInteger(AViewInfo.Node.Values[col_LOS.ItemIndex]);

  //  if i=DEF_NULL_VALUED  then

    if i>=0 then
      ACanvas.Brush.Color:=clLime;
    if i<0 then
      ACanvas.Brush.Color:=clRed;
  end;

}
end;



//--------------------------------------------------------------------
procedure Tdlg_LosRegion.DoOnButtonClick(Sender: TObject; AbsoluteIndex: Integer);
//--------------------------------------------------------------------
var
  sName: string ;
begin

(*
  if Sender=row_ClutterModel then
  begin
  //  Assert(Assigned(IShell));

    if dmAct_Explorer.Dlg_Select_Object (otClutterModel, FClutterModelID, sName) then
      row_ClutterModel.Properties.Value:=sName;
  end;
*)
  //  Dlg_SelectObject (Sender, otClutterModel, FClutterModelID) else

end;
(*
//-------------------------------------------------------------------
procedure Tdlg_LosRegion.act_OkExecute(Sender: TObject);
//-------------------------------------------------------------------
begin

end;
*)