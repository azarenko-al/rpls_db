unit dm_LOS_main;

interface

uses
  Classes, Forms, ADODB, DB;

type
  TdmLOS_main1 = class(TDataModule)
    ADOConnection1: TADOConnection;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class procedure Init;
    procedure SetConnection(aConnectionObject: _Connection);
    { Public declarations }
  end;

var
  dmLOS_main1: TdmLOS_main1;

implementation

uses dm_LOS_calc;

{$R *.dfm}

procedure TdmLOS_main1.DataModuleCreate(Sender: TObject);
begin

end;

// -------------------------------------------------------------
class procedure TdmLOS_main1.Init;
// -------------------------------------------------------------
begin
  if not Assigned(dmLOS_calc) then
    dmLOS_calc := TdmLOS_calc.Create(Application);
end;


procedure TdmLOS_main1.SetConnection(aConnectionObject: _Connection);
begin
  ADOConnection1.ConnectionObject := aConnectionObject;
end;


end.
