program LOS;

uses
  d_Calc_LOS in '..\src\d_Calc_LOS.pas' {dlg_Calc_LOS},
  d_LosRegion in '..\src\d_LosRegion.pas' {dlg_LosRegion},
  dm_App_los in '..\src\dm_App_los.pas' {dmMain_app: TDataModule},
  dm_CalcMap in '..\..\CalcMap\dm_CalcMap.pas' {dmCalcMap: TDataModule},
  dm_Excel_file in '..\..\..\..\common7\office_new\dm_Excel_file.pas' {dmExcel_file: TDataModule},
  dm_LOS_calc in '..\src\dm_LOS_calc.pas' {dmLOS_calc: TDataModule},
  dm_LOS_calc_m in '..\src\dm_LOS_calc_m.pas' {dmLOS_calc_m: TDataModule},
  dm_LosRegion_view in '..\src\dm_LosRegion_view.pas' {dmLosRegion_view: TDataModule},
  dm_Progress in '..\..\..\..\common7\Package_Common\dm_Progress.pas' {dmProgress: TDataModule},
  Forms,
  fr_LosM_view in '..\src\fr_LosM_view.pas' {frame_LosM_view},
  fr_LosRegion_cells in '..\src\fr_LosRegion_cells.pas' {frame_LosRegion_cells},
  i_LOS_ in '..\src shared\i_LOS_.pas',
  u_Excel in '..\..\..\..\common7\Office\u_Excel.pas',
  u_ini_LOS_params in '..\src shared\u_ini_LOS_params.pas',
  u_Params_Bin_to_Map in '..\..\..\..\onega dll 7\Bin_to_Map\src shared\u_Params_Bin_to_Map.pas',
  Windows;

{$R *.RES}


begin
  SetThreadLocale(1049);


  Application.Initialize;
  Application.CreateForm(TdmMain_app, dmMain_app);
  Application.Run;
end.
