program LOS;

uses
  d_Calc_LOS in '..\src\d_Calc_LOS.pas' {dlg_Calc_LOS},
  d_LosRegion in '..\src\d_LosRegion.pas' {dlg_LosRegion},
  dm_LOS_calc in '..\src\dm_LOS_calc.pas' {dmLOS_calc: TDataModule},
  dm_LOS_calc_m in '..\src\dm_LOS_calc_m.pas' {dmLOS_calc_m: TDataModule},
  dm_LOS_main in '..\src\dm_LOS_main.pas' {dmLOS_main: TDataModule},
  dm_LosRegion_view in '..\src\dm_LosRegion_view.pas',
  dm_Main_app_loss in '..\src\dm_Main_app_loss.pas' {dmMain_app: TDataModule},
  Forms,
  fr_LosM_view in '..\src\fr_LosM_view.pas' {frame_LosM_view},
  fr_LosRegion_cells in '..\src\fr_LosRegion_cells.pas' {frame_LosRegion_cells},
  I_DB_login in '..\..\..\..\common DLL\DB_LOGIN\I_DB_login.pas',
  i_LOS in '..\src shared\i_LOS.pas',
  u_LOS in '..\src\u_LOS.pas';

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(TdmMain_app, dmMain_app);
  Application.Run;
end.
