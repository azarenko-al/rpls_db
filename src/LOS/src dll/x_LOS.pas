unit x_LOS;

interface

uses Sysutils, 

  dm_Main,

  dm_LOS_calc,
  dm_LOS_calc_m,


  d_LosRegion,
  d_Calc_LOS,

  u_ini_LOS_params,

  u_dll_with_dmMain,

  i_LOS_
  ;

type
  TLosX = class(TDllCustomX, ILosX)
  private
  public
    procedure Execute(aFileName: WideString); stdcall;
  end;

  
  function DllGetInterface(const IID: TGUID; var Obj): HResult; stdcall;

exports
  DllGetInterface;


implementation


// ---------------------------------------------------------------
procedure TLosX.Execute(aFileName: WideString);
// ---------------------------------------------------------------

var
  iID: integer;
//  iCount: integer;
  I: integer;
//  iMode: integer;
//  iMapDesktopID: integer;
  b: boolean;


begin
  Assert(Assigned(dmMain), 'Assigned(dmMain) not assigned');

  g_Ini_LOS_Params:=TIni_LOS_Params.Create;
  g_Ini_LOS_Params.LoadFromFile(aFileName);


  dmMain.ProjectID:=g_Ini_LOS_Params.ProjectID;


  case g_Ini_LOS_Params.Mode of
    // -------------------------
    mtLos_:
    // -------------------------
    begin
         TdmLOS_calc.Init;

         dmLOS_calc.Params.Site_BLPoint    :=g_Ini_LOS_Params.blPoint;
         dmLOS_calc.Params.TabFileName:=g_Ini_LOS_Params.TabFileName;

     //    dmLOS_calc.Params.TabFileName:=sTabFileName;

         b:=Tdlg_Calc_LOS.ExecDlg(g_Ini_LOS_Params.blPoint);

     //    sKey:='los';
      //   sCaption:='����� LOS';

       end;
    // -------------------------
    mtLosM_:
    // -------------------------
    begin
         TdmLOS_calc_m.Init;
         dmLOS_calc_m.Params.TabFileName:=g_Ini_LOS_Params.TabFileName;

         Tdlg_LosRegion.ExecDlg(g_Ini_LOS_Params.BLPoints);

       //  sKey:='los_m';
       //  sCaption:='����� LOS ��� ������ ��';

       end;
//  else
 //    Exit;
  end;

  FreeAndNil(g_Ini_LOS_Params);

  FreeAndNil(dmLOS_calc);
  FreeAndNil(dmLOS_calc_m);

end;


// ---------------------------------------------------------------
function DllGetInterface(const IID: TGUID; var Obj): HResult;
// ---------------------------------------------------------------
begin
  Result:=S_FALSE; Integer(Obj):=0;

  if IsEqualGUID(IID, ILosX) then
  begin
    with TLosX.Create() do
      if GetInterface(IID,OBJ) then
        Result:=S_OK
  end else
    raise Exception.Create('if IsEqualGUID(IID, ILosX)');


end;



begin
 // RegisterClass(TdmMain_bpl);

end.


