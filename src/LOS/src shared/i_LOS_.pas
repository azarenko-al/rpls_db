unit i_LOS_;

interface

uses ADOInt, //Forms,

//x_LOS

  u_dll;
 // u_Geo;

type

(*  TLosParamsRec = packed record
                 Project_ID    : Integer;
                 TabFileName   : WideString;
                 MapDesktop_ID : Integer;

                 Mode : Integer; //0,1

               end;
*)

  ILosX = interface(IInterface)
  ['{93D001B3-F162-473E-9593-1869BAE7576F}']

    procedure InitADO(aConnection: _Connection); stdcall;
    procedure InitAppHandle(aHandle: integer); stdcall;

    procedure Execute (aFileName: WideString);  stdcall;
  end;

var
  ILos: ILosX;

  function Load_ILos: boolean;
  procedure UnLoad_ILos;


implementation

const
//  DEF_BPL_FILENAME = 'bpl_LOS.bpl';
  DEF_FILENAME = 'dll_LOS.dll';

var
  LHandle : Integer;


function Load_ILos: boolean;
begin
  Result := GetInterface_DLL(DEF_FILENAME, LHandle, ILosX, ILos)=0;
end;


procedure UnLoad_ILos;
begin
  ILos := nil;
  UnloadPackage_dll(LHandle);
end;


end.



