unit i_LOS;

interface

uses ADOInt,

  u_dll,
  u_Geo;

type
  TLosParams = packed record
                 Project_ID    : Integer;
                 TabFileName   : WideString;
                 MapDesktop_ID : Integer;

                 Mode : Integer; //0,1

               end;


  ILosX = interface(IInterface)
  ['{EB668E61-7E28-42D3-B589-6F94D6ABC7F6}']

    procedure Init(aAppHandle: Integer; aConnection: _Connection); stdcall;
    procedure SetParams(aRec: TLosParams); stdcall;

  end;

var
  ILos: ILosX;

  function Load_ILos: boolean;
  procedure UnLoad_ILos;


implementation

const
  DEF_DLL_FILENAME = 'dll_LOS.dll';

var
  LHandle : Integer;


function Load_ILos: boolean;
begin
  Result := GetInterface_dll(DEF_DLL_FILENAME, LHandle, ILosX, ILos)=0;
end;


procedure UnLoad_ILos;
begin
  ILos := nil;
  UnloadPackage_dll(LHandle);
end;


end.



