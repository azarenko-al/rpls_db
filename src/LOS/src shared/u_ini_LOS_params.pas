unit u_ini_LOS_params;


interface
uses
   IniFiles, SysUtils, Dialogs,

   u_func,
   u_GEO
   ;

type
  TIni_LOS_Params = class(TObject)
  private
    function Validate: boolean;
  public
    Mode: (mtLos_, mtLosM_);

  //  MapDesktopID: integer;
    ProjectID: integer;

    TabFileName: string;


    blPoint: TBLPoint;
    BLPoints: TBLPointArrayF;

    IsTest:  Boolean;


  public
    function LoadFromFile(aFileName: string): Boolean;
    procedure SaveToFile(aFileName: string);

  end;


var
  g_Ini_LOS_Params: TIni_LOS_Params;


implementation


// ---------------------------------------------------------------
function TIni_LOS_Params.LoadFromFile(aFileName: string): Boolean;
// ---------------------------------------------------------------
var
  I: Integer;
  iMode: Integer;
begin
  Assert(FileExists(aFileName), aFileName);


  with TIniFile.Create(aFileName) do
  begin
    IsTest   :=ReadBool   ('main', 'IsTest', False);


    ProjectID   :=ReadInteger ('main', 'Project_ID',   0);
//    MapDesktopID:=ReadInteger ('main', 'MapDesktopID', 0);

    TabFileName :=ReadString  ('main', 'TabFileName', '');


    iMode:=ReadInteger ('main', 'Mode', 0);

    case iMode of
      0:  begin;
            Mode :=mtLos_;

            blPoint.B :=AsFloat(ReadString ('main', 'lat',  '0'));
            blPoint.L :=AsFloat(ReadString ('main', 'lon',  '0'));

          end;

      1:  begin
            Mode :=mtLosM_;

              blPoints.Count:=ReadInteger ('Points', 'Count', 0);
        //      SetLength (dmLOS_calc_m.Sites, iCount);

              for I := 0 to blPoints.Count - 1 do
              begin
                blPoints.Items[I].B :=AsFloat(ReadString ('Points', Format('lat%d',[i]), '0'));
                blPoints.Items[I].L :=AsFloat(ReadString ('Points', Format('lon%d',[i]), '0'));
              end;
          end;
    end;
    Free;
  end;

  Result := Validate();
end;

// ---------------------------------------------------------------
procedure TIni_LOS_Params.SaveToFile(aFileName: string);
// ---------------------------------------------------------------
var
  I: Integer;
begin

  with TIniFile.Create (aFileName) do
  begin
    WriteBool ('main', 'IsTest', IsTest);


    WriteInteger ('main', 'Project_ID',   ProjectID);
    WriteString  ('main', 'TabFileName',  TabFileName);
//    WriteInteger ('main', 'MapDesktopID', MapDesktopID);

    WriteInteger ('main', 'Mode', IIF(Mode=mtLos_,0,1));

    case Mode of
      mtLos_:
          begin
            WriteFloat ('main', 'lat', BLPoint.B);
            WriteFloat ('main', 'lon', BLPoint.L);
          end;

      mtLosM_:
          begin
            WriteInteger ('points', 'count', BLPoints.Count);

            for I := 0 to BLPoints.Count - 1 do
            begin
              WriteFloat   ('points', Format('lat%d',[i]),  BLPoints.Items[I].B);
              WriteFloat   ('points', Format('lon%d',[i]),  BLPoints.Items[I].L);
            end;
          end;
    end;

    Free;
  end;

end;




function TIni_LOS_Params.Validate: boolean;
begin
  Result := (ProjectID>0);

  if not Result then
    ShowMessage('ProjectID=0');

end;



end.

