unit dm_MSC;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms, Variants,

  dm_Onega_DB_data,

  u_const_db,

  u_classes,
  u_func,
  u_db,

  u_types,

  //dm_Folder,
  dm_Main,

  dm_Object_base;

type
  //-------------------------------------------------------------------
  TdmMSCAddRec = record
  //-------------------------------------------------------------------
    NewName: string;
    PropertyID: integer;
   // FolderID: integer;
  end;


  TdmMSC = class(TdmObject_base)
    qry_MSC: TADOQuery;
    ds_MSC: TDataSource;
    ADOConnection1_temp: TADOConnection;
    procedure DataModuleCreate(Sender: TObject);
  private
  public
    function Add (aRec: TdmMSCAddRec): integer;
    function Del (aID: integer): boolean; override;


  end;


function dmMSC: TdmMSC;


//================================================================
implementation    {$R *.DFM}
//================================================================

var
  FdmMSC: TdmMSC;


// ---------------------------------------------------------------
function dmMSC: TdmMSC;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmMSC) then
    FdmMSC := TdmMSC.Create(Application);   

  Result := FdmMSC;
end;


procedure TdmMSC.DataModuleCreate(Sender: TObject);
begin
  inherited;
  TableName := TBL_MSC;
  ObjectName := OBJ_MSC;
 // DisplayName := 'MSC';
end;


function TdmMSC.Del (aID: integer): boolean;
begin
  dmOnega_DB_data.MSC_del(aID);

  Result:=True;
  //gl_DB.ExecSP (sp_MSC_del,  [db_Par(FLD_ID, aID)]);

end;


//---------------------------------------------------------
function TdmMSC.Add (aRec: TdmMSCAddRec): integer;
//---------------------------------------------------------
begin
  Result := dmOnega_DB_data.ExecStoredProc_ ('sp_MSC_Add',

 // with aRec do
   // Result:=gl_DB.AddRecordID (TBL_MSC,

   // gl_DB.AddRecord (TBL_MSC,
     [
      //db_Par (FLD_PROJECT_ID,  dmMain.ProjectID),
      FLD_NAME,        aRec.NewName,
    //  db_Par (FLD_FOLDER_ID,   IIF(FolderID>0,FolderID,NULL)),
      FLD_PROPERTY_ID, aRec.PropertyID
     ]);

 //  ;
end;


begin
end.