unit fr_MSC_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, Menus, ToolWin, Grids, DBGrids,
  cxPropertiesStore, ADODB, DB, cxControls, cxSplitter, 
  ComCtrls, ExtCtrls, DBCtrls,  ImgList, 
             
  dm_User_Security,

  dm_Onega_DB_data,

  fr_DBInspector_Container,

  u_dlg,
  u_func,
  u_db,
  u_reg,

  u_types,
  u_const_db,

  dm_MSC,
 // dm_MSC_View,
                                                                                          
  u_const_str,

  fr_View_base, dxBar, cxBarEditItem, cxClasses, cxLookAndFeels,
  cxCheckBox, cxButtonEdit
  ;

type
  Tframe_MSC_View = class(Tframe_View_Base)
    pn_Inspector: TPanel;
    DataSource1: TDataSource;
    qry_Items: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    Ffrm_Inspector: Tframe_DBInspector_Container;

  public
    procedure View (aID: integer; aGUID: string); override;
  end;


implementation


{$R *.dfm}


//--------------------------------------------------------------------
procedure Tframe_MSC_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_MSC;

  pn_Inspector.Align:=alClient;

 // dbGrid1.Columns[0].;

//  col_Name.FieldName:=FLD_NAME;
 // col_Name.Caption:=STR_COL_NAME;

  CreateChildForm(Tframe_DBInspector_Container, Ffrm_Inspector, pn_Inspector);
  Ffrm_Inspector.PrepareViewForObject(OBJ_MSC);
//  :=.CreateChildForm(pn_Inspector, OBJ_MSC);

 // dmMSC_View.InitDB (mem_Items);

 // AddControl(dxDBGrid1, [PROP_HEIGHT]);

  //AddComponentProp(dbGrid1, PROP_HEIGHT);
 // cxPropertiesStore.RestoreFrom;
end;


procedure Tframe_MSC_View.FormDestroy(Sender: TObject);
begin
  FreeAndNil(Ffrm_Inspector);

//  Ffrm_Inspector.Free;
  inherited;
end;

procedure Tframe_MSC_View.View (aID: integer; aGUID: string);
//var
//  bReadOnly: Boolean;
 // iGeoRegion_ID: Integer;
begin
  inherited;

  Ffrm_Inspector.View (aID);

  // -------------------------
//  FGeoRegion_ID := Ffrm_inspector.GetDataset().FieldByName(FLD_GeoRegion_ID).AsInteger;// GetIntFieldValue(FLD_GeoRegion_ID);
//  bReadOnly:=dmUser_Security.GeoRegion_is_ReadOnly(FGeoRegion_ID);

//  Ffrm_inspector.SetReadOnly(bReadOnly, IntToStr(FGeoRegion_ID));
  // -------------------------


end;



end.



(*

  SQL_SELECT_TEMPLATE   = 'SELECT id,name FROM %s WHERE msc_id=:msc_id';

  //-------------------------------------------------------------------
  procedure DoAddRecords (aTableName,aObjectName: string);
  //-------------------------------------------------------------------
  var sSQL: string;
  begin
    sSQL:=Format(SQL_SELECT_TEMPLATE, [aTableName]);

    db_OpenQuery (gl_DB.Query, sSQL, [db_Par(FLD_MSC_ID, aID)] );
*)