unit dm_act_MSC;

interface
{$I ver.inc}

uses
  Classes, Forms, ActnList, Menus,  

  I_Object,
 // I_MSC,
  

  u_shell_var,

//  I_Act_MSC,

  dm_Folder,

  dm_act_Base,
  dm_MSC,

  u_Func,
  u_Geo,
  
  u_types,

  fr_MSC_view,
  d_MSC_add
  ;


type
  TdmAct_MSC = class(TdmAct_Base) //, IAct_Msc_X)
 //   procedure DataModuleDestroy(Sender: TObject);   
    procedure DataModuleCreate(Sender: TObject);
  private
   FBLPoint: TBLPoint;
//    procedure CheckActionsEnable;
  protected
    procedure DoAction (Sender: TObject);
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm (aOwnerForm: TForm): TForm; override;

    function ItemDel(aID: integer ; aName: string = ''): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;

//    procedure DoAfterItemDel (aID: integer);
//    procedure DoOnMapRefresh (Sender: TObject);
    function GetExplorerPath(aID: integer; aGUID: string): string;

    procedure ShowOnMap (aID: integer);

    function ViewInProjectTree(aID: integer): TStrArray; override;
  public
    procedure Add (aFolderID: integer); overload;
    procedure AddByPos(aBLPoint: TBLPoint); overload;
    procedure AddByProperty(aProperty: integer);
//    procedure Del (aID: integer);

    class procedure Init;
  end;


var
  dmAct_MSC: TdmAct_MSC;

//====================================================================
// implementation
//====================================================================
implementation
{$R *.dfm}


//--------------------------------------------------------------------
class procedure TdmAct_MSC.Init;
//--------------------------------------------------------------------
begin
  Assert(not Assigned(dmAct_MSC));

//  if  then
//  begin
  Application.CreateForm(TdmAct_MSC, dmAct_MSC);

 // dmAct_MSC.GetInterface(IAct_MSC_X, IAct_MSC);
  //Assert(Assigned(IAct_MSC));

 // end;

//    dmAct_MSC:=TdmAct_MSC.Create(nil);

end;

//
//procedure TdmAct_MSC.DataModuleDestroy(Sender: TObject);
//begin
// // IAct_MSC := nil;
// // dmAct_MSC:= nil;
//
//  inherited;
//end;

//-------------------------------------------------------------------
procedure TdmAct_MSC.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;
  ObjectName:=OBJ_MSC;


  act_Add.Caption := '������� MSC';

  SetActionsExecuteProc ([
           act_Object_ShowOnMap
         ], DoAction);

  SetCheckedActionsArr([
       act_Add,
       act_Del_,
       act_Del_list
     ]);
end;



//--------------------------------------------------------------------
function TdmAct_MSC.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_MSC_View.Create(aOwnerForm);
end;


//--------------------------------------------------------------------
procedure TdmAct_MSC.AddByPos(aBLPoint: TBLPoint);
//--------------------------------------------------------------------
begin
  FBLPoint:=aBLPoint;
  inherited Add(0);
end;

//--------------------------------------------------------------------
function TdmAct_MSC.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_MSC_add.ExecDlg (aFolderID, FBLPoint);

end;

//--------------------------------------------------------------------
procedure TdmAct_MSC.AddByProperty(aProperty: integer);
//--------------------------------------------------------------------
begin
 // Result:=
  Tdlg_MSC_add.ExecDlg (aProperty, NULL_POINT);


(*  FBLPoint:=NULL_BLPOINT;
  FProperty:=aProperty;
  inherited Add (0);*)
end;



//--------------------------------------------------------------------
procedure TdmAct_MSC.Add (aFolderID: integer);
//--------------------------------------------------------------------
begin
  FBLPoint:=NULL_POINT;
  inherited Add(aFolderID);
end;

//--------------------------------------------------------------------
function TdmAct_MSC.ItemDel(aID: integer ; aName: string = ''): boolean;
//--------------------------------------------------------------------
//var
 // sGUID: string;
begin
 // sGUID:= dmMSC.GetGUIDByID(aID);

//  FDeletedIDList.AddObjNameAndGUID(aID, sGUID, OBJ_MSC);

  Result:=dmMSC.Del (aID);

  // then
   // g_ShellEvents.Shell_PostDelNode (sGUID);
end;

//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_MSC.DoAction (Sender: TObject);
begin
  if Sender=act_Object_ShowOnMap then
    ShowOnMap (FFocusedID) else
end;

//--------------------------------------------------------------------
procedure TdmAct_MSC.ShowOnMap (aID: integer);
//--------------------------------------------------------------------
begin
  ShowPointOnMap (dmMSC.GetPropertyPos(aID), aID);
end;


//--------------------------------------------------------------------
procedure TdmAct_MSC.GetPopupMenu;
//--------------------------------------------------------------------
begin
  //CheckActionsEnable();

  case aMenuType of
    mtFolder: begin
                AddFolderMenu_Create (aPopupMenu);
              //  AddMenuItem (aPopupMenu, act_Add);
                AddFolderPopupMenu (aPopupMenu);
              end;

    mtItem: begin
               AddMenuItem (aPopupMenu, act_Object_ShowOnMap);
               AddMenuItem (aPopupMenu, nil);

               AddMenuItem (aPopupMenu, act_Del_);
               AddFolderMenu_Tools (aPopupMenu);

            end;
    mtList: begin
               AddMenuItem (aPopupMenu, act_Del_list);
                AddFolderMenu_Tools (aPopupMenu);

            end;
  end;
end;

//--------------------------------------------------------------------
function TdmAct_MSC.ViewInProjectTree(aID: integer): TStrArray;
//--------------------------------------------------------------------
var //sGUID: string;
  iFolderID: integer;
  sFolderGUID: string;

begin
//  Assert(Assigned(IMainProjectExplorer));

  if Assigned(IMainProjectExplorer) then
  begin
    IMainProjectExplorer.ExpandByGUID(GUID_PROJECT);
    IMainProjectExplorer.ExpandByGUID(GUID_MSC);

    iFolderID:=dmMSC.GetFolderID(aID);

    if iFolderID=0 then
      sFolderGUID:=g_Obj.ItemByName[ObjectName].RootFolderGUID
    else
      sFolderGUID:=dmFolder.GetGUIDByID(iFolderID);


     Result := StrArray([GUID_PROJECT,
                         GUID_MSC,
                         sFolderGUID]);

  //  sGUID:=dmFolder.GetGUIDByID (dmMSC.GetFolderID(aID));

    IMainProjectExplorer.ExpandByGUID(sFolderGUID);
//    IMainProjectExplorer.FocuseNodeByGUID (aGUID);

    IMainProjectExplorer.FocuseNodeByObjName(ObjectName, aID);

  end;
end;

//--------------------------------------------------------------------
function TdmAct_MSC.GetExplorerPath(aID: integer; aGUID: string): string;
//--------------------------------------------------------------------
var sFolderGUID: string;
begin
  sFolderGUID:=dmFolder.GetGUIDByID (dmMSC.GetFolderID(aID));

  Result := GUID_PROJECT + CRLF +
            GUID_MSC    + CRLF +
            sFolderGUID  + CRLF +
            aGUID;
end;

 

end.

  {

//--------------------------------------------------------------------
procedure TdmAct_MSC.CheckActionsEnable;
//--------------------------------------------------------------------
begin

 (* SetActionsEnabled
    ([
       act_Add,
       act_Del_
     ],
     not dmUser_Security.ProjectIsReadOnly);

*)
end;

