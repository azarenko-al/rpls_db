program test_MSC;

uses
  Forms,
  a_Unit in 'a_Unit.pas' {frm_Test_MSC},
  dm_MSC in '..\dm_MSC.pas' {dmMSC: TDataModule},
  dm_Main in '..\..\DataModules\dm_Main.pas' {dmMain: TDataModule},
  dm_Main_res in '..\..\DataModules\dm_Main_res.pas' {dmMain_res: TDataModule},
  dm_MSC_View in '..\dm_MSC_View.pas' {dmMSC_View: TDataModule},
  dm_act_MSC in '..\dm_act_MSC.pas' {dmAct_MSC: TDataModule},
  dm_Explorer in '..\..\Explorer\dm_Explorer.pas' {dmExplorer: TDataModule},
  u_const_db in '..\..\u_const_db.pas',
  fr_MSC_view in '..\fr_MSC_view.pas' {frame_MSC_View},
  d_MSC_add in '..\d_MSC_add.pas' {dlg_MSC_add};

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(Tfrm_Test_MSC, frm_Test_MSC);
  Application.Run;
end.
