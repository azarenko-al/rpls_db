unit a_Unit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, rxPlacemnt,

  f_Log,
  u_db,
  u_dlg,
  u_const_msg,
  u_const_db,
  u_types,
  dm_explorer,
  fr_explorer,
  fr_MSC_view,

  I_Shell,
  dm_Act_MSC,
  dm_Main, ToolWin
  ;

type
  Tfrm_Test_MSC = class(TForm)
    pc_Main: TPageControl;
    TabSheet1: TTabSheet;
    bt_Add: TButton;
    bt_View: TButton;
    FormStorage1: TFormStorage;
    procedure FormCreate(Sender: TObject);
    procedure bt_AddClick(Sender: TObject);
    procedure bt_ViewClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Fframe_MSC : Tframe_Explorer;
  end;

var
  frm_Test_MSC: Tfrm_Test_MSC;
  id: integer;

implementation

{$R *.DFM}

procedure Tfrm_Test_MSC.FormCreate(Sender: TObject);
begin
  Tfrm_Log.CreateForm;

//  dmMain.Test;
  dmMain.OpenDefault;
  //map_eng; explorer; property

///////  PostEvent (WE_PROJECT_CHANGED);

//  viewClick (nil);

  Fframe_MSC := Tframe_Explorer.CreateChildForm( TabSheet1);
  Fframe_MSC.SetViewObjects([otMSC]);
  Fframe_MSC.Load;

end;

procedure Tfrm_Test_MSC.bt_AddClick(Sender: TObject);
begin
  dmAct_MSC.Add (0);
end;

procedure Tfrm_Test_MSC.bt_ViewClick(Sender: TObject);
begin
  id:=gl_DB.GetMaxID (TBL_MSC);
////////  Tframe_MSC_View.CreateForm (id);
end;

end.
