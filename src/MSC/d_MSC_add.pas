unit d_MSC_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rxPlacemnt,    cxPropertiesStore, ActnList,  
  StdCtrls, ExtCtrls,   cxButtonEdit,  cxVGrid, cxControls, cxInplaceContainer,

  dm_User_Security,

  dm_Act_Map,

  u_cx_vgrid,

  dm_MapEngine_store,
  
 // d_Wizard,
  d_Wizard_Add_with_params,

  I_Shell,
  dm_Main,

 // I_MapEngine,
 // dm_MapEngine,


 // I_Object,
//  I_MSC,

 // I_Shell,
//  I_MapAct,

  u_const_DB,
  u_const_str,

  u_dlg,
  u_db,
  u_Geo,

  u_types,

  dm_MSC,
  dm_Property,


  ComCtrls

  ;

type
  Tdlg_MSC_add = class(Tdlg_Wizard_add_with_params)
    cxVerticalGrid1: TcxVerticalGrid;
    row_Property: TcxEditorRow;
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure row_Property1ButtonClick(Sender: TObject;
      AbsoluteIndex: Integer);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure cxVerticalGrid1EditorRow1EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
  private
    FRec: TdmMscAddRec;

    FID: integer;

 //   FFolderID: integer;
  public
     class function ExecDlg (aPropertyID: integer;
                             aBLPoint: TBLPoint): integer;
  end;

//==================================================================
implementation {$R *.dfm}
//==================================================================



//-------------------------------------------------------------------
class function Tdlg_MSC_add.ExecDlg(aPropertyID: integer;
                            aBLPoint: TBLPoint): integer;
//-------------------------------------------------------------------
begin
{
  // ---------------------------------------------------------------
  if dmUser_Security.ProjectIsReadOnly then
  begin
    ShowMessage('���������� �������� ������. ������ ������ ��� ������.');
    Result:=0;
    Exit;
  end;
  // ---------------------------------------------------------------
}


  with Tdlg_MSC_add.Create(Application) do
  begin
    if (aPropertyID = 0)  then
      aPropertyID:=dmProperty.GetNearestID(aBLPoint);

    FRec.PropertyID:=aPropertyID;
  //  FRec.FolderID:=0;

    ed_Name_.Text:=dmMSC.GetNewName();
    row_Property.Properties.Value:=gl_DB.GetNameByID (TBL_PROPERTY, aPropertyID);

    if (ShowModal=mrOk) then
    begin
      Result:=FID;
    end else
      Result:=0;

    Free;
  end;
end;

//-------------------------------------------------------------------
procedure Tdlg_MSC_add.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;
  SetActionName (STR_DLG_ADD_MSC);

  row_Property.Properties.Caption:=STR_PROPERTY;

  SetDefaultSize();

  cx_InitVerticalGrid(cxverticalGrid1);

end;


procedure Tdlg_MSC_add.act_OkExecute(Sender: TObject);
begin
  FRec.NewName:=ed_Name_.Text;
 // FRec.PropertyID:=FPropertyID;

  FID:=dmMSC.Add (FRec);

  if FID>0 then
  begin
    g_ShellEvents.Shell_UpdateNodeChildren_ByObjName (OBJ_PROPERTY, FRec.PropertyID);
   // g_ShellEvents.Shell_UpdateNodeChildren_ByObjName (OBJ_MSC, FID);

//    dmMapEngine.CreateObject (otMSC, FID);
//    dmAct_Map.MapRefresh;



    dmMapEngine_store.Feature_Add(OBJ_MSC, FID);


    if not dmMapEngine_store.Is_Object_Map_Exists(OBJ_MSC) then
       dmMapEngine_store.Open_Object_Map(OBJ_MSC);



//    dmAct_Map.MapRefresh;
  end;

end;


procedure Tdlg_MSC_add.row_Property1ButtonClick(Sender: TObject; AbsoluteIndex: Integer);
begin
//  Dlg_SelectObject_dx (row_Property, otProperty, FRec.PropertyID);
end;


procedure Tdlg_MSC_add.ActionList1Update(Action: TBasicAction;  var Handled: Boolean);
begin
  act_Ok.Enabled:= (FRec.PropertyID > 0);
end;


procedure Tdlg_MSC_add.cxVerticalGrid1EditorRow1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  if cxVerticalGrid1.FocusedRow =row_Property then
    Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otProperty, FRec.PropertyID);

end;

end.