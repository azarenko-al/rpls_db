inherited dlg_MSC_add: Tdlg_MSC_add
  Left = 732
  Top = 348
  Width = 519
  Height = 448
  Caption = 'dlg_MSC_add'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 385
    Width = 511
    inherited Bevel1: TBevel
      Width = 511
    end
    inherited Panel3: TPanel
      Left = 327
      inherited btn_Ok: TButton
        Left = 348
      end
      inherited btn_Cancel: TButton
        Left = 432
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 511
    inherited Bevel2: TBevel
      Width = 511
    end
    inherited pn_Header: TPanel
      Width = 511
      Height = 55
    end
  end
  inherited Panel1: TPanel
    Width = 511
    inherited ed_Name_: TEdit
      Width = 500
    end
  end
  inherited Panel2: TPanel
    Width = 511
    Height = 206
    inherited PageControl1: TPageControl
      Width = 501
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 493
          Height = 98
          Align = alClient
          LookAndFeel.Kind = lfStandard
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 150
          OptionsView.ValueWidth = 50
          TabOrder = 0
          Version = 1
          object row_Property: TcxEditorRow
            Expanded = False
            Properties.Caption = 'Property'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = cxVerticalGrid1EditorRow1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
        end
      end
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 454
    Top = 6
  end
end
