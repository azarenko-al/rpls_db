inherited dlg_Antenna_add: Tdlg_Antenna_add
  Left = 967
  Top = 301
  Width = 668
  Height = 518
  Caption = 'dlg_Antenna_add'
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 455
    Width = 660
    inherited Bevel1: TBevel
      Width = 660
    end
    inherited Panel3: TPanel
      Left = 468
    end
  end
  inherited pn_Top_: TPanel
    Width = 660
    inherited Bevel2: TBevel
      Width = 660
    end
    inherited pn_Header: TPanel
      Width = 660
      Height = 55
    end
  end
  inherited Panel1: TPanel
    Width = 660
    inherited ed_Name_: TEdit
      Width = 644
    end
  end
  inherited Panel2: TPanel
    Width = 660
    Height = 310
    inherited PageControl1: TPageControl
      Width = 650
      Height = 300
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 642
          Height = 170
          BorderStyle = cxcbsNone
          Align = alClient
          LookAndFeel.Kind = lfFlat
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.ShowEditButtons = ecsbAlways
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 176
          OptionsView.ValueWidth = 56
          TabOrder = 0
          Version = 1
          object row_Antenna_Model: TcxEditorRow
            Expanded = False
            Properties.Caption = #1052#1086#1076#1077#1083#1100' '#1072#1085#1090#1077#1085#1085#1099
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end
              item
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = DoOnButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object cxVerticalGrid1CategoryRow1: TcxCategoryRow
            Expanded = False
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
          object row_Height: TcxEditorRow
            Expanded = False
            Properties.Caption = #1042#1099#1089#1086#1090#1072' [m]'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = '30'
            ID = 2
            ParentID = -1
            Index = 2
            Version = 1
          end
          object row_Azimuth: TcxEditorRow
            Expanded = False
            Properties.Caption = #1040#1079#1080#1084#1091#1090' ['#1075#1088']'
            Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
            Properties.EditProperties.MinValue = 359.000000000000000000
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = '0'
            ID = 3
            ParentID = -1
            Index = 3
            Version = 1
          end
          object cxVerticalGrid1CategoryRow2: TcxCategoryRow
            Expanded = False
            Visible = False
            ID = 4
            ParentID = -1
            Index = 4
            Version = 1
          end
          object row_PosType: TcxEditorRow
            Expanded = False
            Properties.Caption = 'PosType'
            Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.DropDownListStyle = lsFixedList
            Properties.EditProperties.DropDownRows = 7
            Properties.EditProperties.Items.Strings = (
              '0 - '#1087#1088#1080#1074#1103#1079#1082#1072' '#1082' '#1087#1083#1086#1097#1072#1076#1082#1077
              '1 - '#1089#1074#1086#1080' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1099)
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.EditProperties.Revertable = True
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            Visible = False
            ID = 5
            ParentID = -1
            Index = 5
            Version = 1
          end
          object row_Pos: TcxEditorRow
            Expanded = False
            Properties.Caption = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1099
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ViewStyle = vsHideCursor
            Properties.EditProperties.OnButtonClick = DoOnButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            Visible = False
            ID = 6
            ParentID = -1
            Index = 6
            Version = 1
          end
        end
        object GroupBox1: TGroupBox
          Left = 0
          Top = 170
          Width = 642
          Height = 80
          Align = alBottom
          Caption = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1099
          Constraints.MaxHeight = 80
          Constraints.MinHeight = 80
          TabOrder = 1
          Visible = False
        end
        object sb_Raznos: TStatusBar
          Left = 0
          Top = 250
          Width = 642
          Height = 19
          Panels = <
            item
              Width = 50
            end>
        end
      end
    end
  end
  inherited ActionList1: TActionList
    Left = 452
    Top = 0
  end
  inherited FormStorage1: TFormStorage
    Left = 424
    Top = 0
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 393
    Top = 2
  end
end
