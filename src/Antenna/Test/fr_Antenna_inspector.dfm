object frame_Antenna_inspector: Tframe_Antenna_inspector
  Left = 644
  Top = 270
  Width = 403
  Height = 366
  Caption = 'frame_Antenna_inspector'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object pn_Buttons: TPanel
    Left = 0
    Top = 304
    Width = 395
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 0
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 395
      Height = 2
      Align = alTop
      Shape = bsTopLine
    end
    object btn_Ok: TButton
      Left = 232
      Top = 8
      Width = 75
      Height = 23
      Action = act_Ok
      Anchors = [akTop, akRight]
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object btn_Cancel: TButton
      Left = 316
      Top = 8
      Width = 75
      Height = 23
      Action = act_Cancel
      Anchors = [akTop, akRight]
      Cancel = True
      TabOrder = 1
    end
  end
  object pn_Header: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 60
    Align = alTop
    BevelOuter = bvLowered
    BorderWidth = 6
    Color = clInfoBk
    Constraints.MaxHeight = 60
    Constraints.MinHeight = 60
    TabOrder = 1
    Visible = False
  end
  object FormPlacement1: TFormPlacement
    Left = 40
    Top = 12
  end
  object ActionList1: TActionList
    Left = 76
    Top = 12
    object act_Ok: TAction
      Category = 'Main'
      Caption = '���������'
      OnExecute = act_OkExecute
    end
    object act_Cancel: TAction
      Category = 'Main'
      Caption = '������'
      OnExecute = act_OkExecute
    end
  end
end
