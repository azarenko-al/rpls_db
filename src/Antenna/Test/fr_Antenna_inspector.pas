unit fr_Antenna_inspector;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,

  dm_AntType,

  fr_DBInspector,

  u_func,
  u_dlg,

  u_const_db,
  u_types,

  rxPlacemnt, ActnList, StdCtrls, ExtCtrls
  ;


type
  Tframe_Antenna_inspector = class(TForm)
    FormPlacement1: TFormPlacement;
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    ActionList1: TActionList;
    act_Ok: TAction;
    act_Cancel: TAction;
    pn_Header: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);

  private
    Ffrm_DBInspector: Tframe_DBInspector;

    procedure DoOnFieldChanged (Sender: TObject; aFieldName: string; aNewValue: Variant);
  public
    procedure View (aID: integer);

    class function ExecDlg (aID: integer): boolean;

    class function CreateChildForm ( aDest: TWinControl): Tframe_Antenna_inspector;

  end;


//====================================================================
implementation {$R *.DFM}
//====================================================================


class function Tframe_Antenna_inspector.CreateChildForm ( aDest: TWinControl): Tframe_Antenna_inspector;
begin
  Result:=Tframe_Antenna_inspector.Create(nil);
  CopyControls (Result, aDest);
end;

//--------------------------------------------------------------------
class function Tframe_Antenna_inspector.ExecDlg (aID: integer): boolean;
//--------------------------------------------------------------------
begin
  with Tframe_Antenna_inspector.Create(Application) do
  try
    pn_Buttons.Visible:=True;

    View (aID);
    //Ffrm_DBInspector.Options.AutoCommit:=False;

    Result:=(ShowModal=mrOK);
    if Result then
      Ffrm_DBInspector.Post;

  finally
    Free;
  end;

end;


//--------------------------------------------------------------------
procedure Tframe_Antenna_inspector.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin

  Ffrm_DBInspector:=Tframe_DBInspector.CreateChildForm( Self, OBJ_ANTENNA);
  Ffrm_DBInspector.OnFieldChanged:=DoOnFieldChanged;

  pn_Buttons.Visible:=False;
end;


procedure Tframe_Antenna_inspector.FormDestroy(Sender: TObject);
begin
  Ffrm_DBInspector.Free;
end;


//--------------------------------------------------------------------
procedure Tframe_Antenna_inspector.View (aID: integer);
//--------------------------------------------------------------------
begin
  Ffrm_DBInspector.View (aID);

end;


//-------------------------------------------------------------------
procedure Tframe_Antenna_inspector.DoOnFieldChanged (Sender: TObject; aFieldName: string;
                                                aNewValue: Variant);
//-------------------------------------------------------------------
var rec: TdmAntTypeInfoRec;
begin
  if Eq(aFieldName, FLD_ANTENNATYPE_ID) then
  begin
    if dmAntType.GetInfoRec (aNewValue, rec) then
    begin
      Ffrm_DBInspector.SetFieldValue (FLD_GAIN, rec.Gain);
      Ffrm_DBInspector.SetFieldValue (FLD_DIAMETER, rec.Diameter);
      Ffrm_DBInspector.SetFieldValue (FLD_Polarization , rec.Polarization);

      Ffrm_DBInspector.SetFieldValue (FLD_Vert_Width, rec.Vert_Width);
      Ffrm_DBInspector.SetFieldValue (FLD_Horz_Width, rec.Horz_Width);

    end;
  end;


end;


procedure Tframe_Antenna_inspector.act_OkExecute(Sender: TObject);
begin
//.
end;

end.
 
