unit a_Unit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,

  fr_Explorer,
  dm_Explorer,

  dm_Main,
  dm_Act_LinkEnd,
  dm_Act_Antenna, 
  //dm_Act_View_Eng,

  //   u_func_app,
  u_db,


  u_const,
  u_const_msg,
  u_const_db,
  u_dlg,
  u_Types,

  fr_AntType_view,

  f_Explorer,

  ComCtrls;

type
  Tfrm_test_Ant = class(TForm)
    Add: TButton;
    View: TButton;
    Import_: TButton;
    pc_Main: TPageControl;
    TabSheet1: TTabSheet;
    procedure AddClick(Sender: TObject);
    procedure ViewClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Fframe_AntType : Tframe_Explorer;
  public
    { Public declarations }
  end;

var
  frm_test_Ant: Tfrm_test_Ant;

//===================================================
implementation {$R *.DFM}
//===================================================

var
  id: integer;

//------------------------------------------------------------------
procedure Tfrm_test_Ant.AddClick(Sender: TObject);
//------------------------------------------------------------------
begin
///////  id:=dmMain.GetFolderMaxID (OBJ_ANTENNA_TYPE);

//  dmAct_Antenna.AddForCell (0);

//  frm_Explorer:=Tfrm_Explorer.Create(Application);

end;

//------------------------------------------------------------------
procedure Tfrm_test_Ant.ViewClick(Sender: TObject);
//------------------------------------------------------------------
begin
  id:=gl_DB.GetMaxID (TBL_ANTENNA_TYPE); //, [EMPTY_PARAM] );

////////  Tframe_AntType_View.CreateView (id);
end;



//------------------------------------------------------------------
procedure Tfrm_test_Ant.FormCreate(Sender: TObject);
//------------------------------------------------------------------
begin

  dmMain.OpenDlg1;

  Fframe_AntType := Tframe_Explorer.CreateChildForm ( TabSheet1);
  Fframe_AntType.Style := esObject;
  Fframe_AntType.SetViewObjects([otLinkEnd]);
  Fframe_AntType.Load;

/////  dmMain.Test;
/////  PostEvent (WE_PROJECT_CHANGED);

////////  PostMsg (WM_SHOW_FORM_MAP);
end;

end.
