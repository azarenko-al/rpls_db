program test_ant;

uses
  Forms,
  dm_act_Explorer in '..\..\Explorer\dm_act_Explorer.pas' {dmAct_Explorer: TDataModule},
  a_Unit in 'a_Unit.pas' {frm_test_Ant},
  fr_View_base in '..\..\Explorer\fr_View_base.pas' {frame_View_Base},
  dm_act_Antenna in '..\dm_act_Antenna.pas' {dmAct_Antenna: TDataModule},
  dm_Antenna_tools in '..\dm_Antenna_tools.pas' {dmAntenna_tools: TDataModule},
  d_Antenna_add in '..\d_Antenna_add.pas' {dlg_Antenna_add},
  dm_Property in '..\..\Property\dm_Property.pas' {dmProperty: TDataModule},
  fr_Antenna_inspector in '..\fr_Antenna_inspector.pas' {frame_Antenna_inspector},
  dm_Antenna_view in '..\dm_Antenna_View.pas' {dmAntenna_view: TDataModule},
  fr_AntType_view in '..\..\AntennaType\fr_AntType_view.pas' {frame_AntType_View},
  fr_Antenna_view in '..\fr_Antenna_view.pas' {frame_Antenna_View},
  d_Antenna_Move in '..\d_Antenna_Move.pas' {dlg_Antenna_Move},
  dm_Antenna in '..\dm_Antenna.pas' {dmAntenna: TDataModule};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(Tfrm_test_Ant, frm_test_Ant);
  Application.Run;
end.
