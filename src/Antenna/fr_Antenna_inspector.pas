unit fr_Antenna_inspector;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, rxPlacemnt, StdCtrls, ExtCtrls, Variants,

  fr_DBInspector_Container,

 // dm_User_Security,

//  I_MapEngine,
//  dm_MapEngine,

  dm_AntType,
  dm_Antenna,

  u_func,
  u_dlg,
  u_db,

  u_const_db,
  u_const_str,
  u_types

  ;

type
  TOnFieldChanged = procedure (Sender: TObject; aFieldName: string; aNewValue: Variant) of object;


  Tframe_Antenna_inspector = class(Tframe_DBInspector_Container)
    procedure FormCreate(Sender: TObject);
  private
//    FOldAzimuth: integer;
    FAntennaID: integer;
//    FIsMapRebuild: boolean;

    FOnFieldChanged: TOnFieldChanged;

  protected
    procedure DoOnFieldChanged (Sender: TObject; aFieldName: string; aNewValue: Variant); override;
//    procedure DoOnPost (Sender: TObject); override;

  public
    procedure View (aID: integer); override;

    property OnFieldChanged: TOnFieldChanged read FOnFieldChanged write FOnFieldChanged;

  end;


//====================================================================
implementation

 {$R *.DFM}

procedure Tframe_Antenna_inspector.FormCreate(Sender: TObject);
begin
  inherited;
  PrepareViewForObject (OBJ_LINKEND_ANTENNA);
end;


procedure Tframe_Antenna_inspector.View(aID: integer);
//var
//  b: Boolean;
//  iGeoRegion_ID: Integer;
begin
  inherited;

  FAntennaID:= aID;
//  FIsMapRebuild:= false;

//  FOldAzimuth:=GetINtFieldValue (FLD_AZIMUTH);

//  FIsMapRebuild := False;
(*
  iGeoRegion_ID := GetIntFieldValue(FLD_GeoRegion_ID);

  b:=dmUser_Security.GeoRegion_is_ReadOnly(iGeoRegion_ID);

  SetReadOnly(b, IntToStr(iGeoRegion_ID));

*)
//  ShowMessage(IntToStr(iGeoRegion_ID));


//  inherited;
end;



//-------------------------------------------------------------------
procedure Tframe_Antenna_inspector.DoOnFieldChanged (Sender: TObject; aFieldName: string;
                                                aNewValue: Variant);
//-------------------------------------------------------------------
var rec: TdmAntTypeInfoRec;
//  dOldAzimuth,
 // dOldHorzWidth: double;
begin
  // -------------------------------------------------------------------
  if Eq(aFieldName, FLD_ANTENNATYPE_ID) then
  // -------------------------------------------------------------------
  begin
    if dmAntType.GetInfoRec11 (aNewValue, rec) then
    begin
      SetFieldValue (FLD_GAIN,         rec.Gain_dB);
      SetFieldValue (FLD_DIAMETER,     rec.Diameter);
      SetFieldValue (FLD_POLARIZATION_STR, rec.Polarization_str);
      SetFieldValue (FLD_FREQ_MHZ,     rec.Freq_MHz);

      SetFieldValue (FLD_VERT_WIDTH,   rec.Vert_Width);
      SetFieldValue (FLD_HORZ_WIDTH,   rec.Horz_Width);

//      FIsMapRebuild := True;
    end;
  end  else

//   -------------------------------------------------------------------
//  if Eq(aFieldName, FLD_AZIMUTH) then
//  begin
//    dOldAzimuth:= gl_DB.GetIntFieldValueByID(TBL_ANTENNA, FLD_AZIMUTH, FAntennaID);
//    FIsMapRebuild:= True; //(FOldAzimuth <> AsFloat(aNewValue));
//  end  else

  // -------------------------------------------------------------------
  if Eq(aFieldName, FLD_GAIN) or
     Eq(aFieldName, FLD_DIAMETER) or
     Eq(aFieldName, FLD_POLARIZATION_STR) or
     Eq(aFieldName, FLD_FREQ_MHZ) or
     Eq(aFieldName, FLD_VERT_WIDTH) or
     Eq(aFieldName, FLD_HORZ_WIDTH) then
  // -------------------------------------------------------------------
  begin
    SetFieldValue (FLD_ANTENNATYPE_ID, NULL);
//    FIsMapRebuild:= True;
  //  View(FID);
  end;


  if assigned (FOnFieldChanged) then
    FOnFieldChanged (Sender, aFieldName, aNewValue);

end;

end.
      

{


//-------------------------------------------------------------------
procedure Tframe_Antenna_inspector.DoOnPost(Sender: TObject);
//-------------------------------------------------------------------
//var
//  sObj: string;

begin
  inherited;

//  if FIsMapRebuild then
//  begin
//  //  Assert(Assigned(IMapEngine));
//
//   /////// sObj:=dmAntenna.GetObjectName(FAntennaID);
//
// //   dmMapEngine.ReCreateObject (ObjNameToType(sObj),  FAntennaID);
//
//  end;


end;

