unit fr_Antenna_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, StdCtrls, cxPropertiesStore,
  Dialogs, rxPlacemnt, ActnList, Menus, ToolWin,  ComCtrls, ExtCtrls, DBCtrls,
  ImgList,  cxControls, cxSplitter,

  dm_Main,

  dm_User_Security,

  fr_View_base,

  fr_Antenna_inspector,
  fr_AntType_masks,

  u_func,
  u_db,
  u_reg,

  u_const_db,

  u_types, dxBar, cxBarEditItem, cxClasses, cxLookAndFeels

  ;

type
  Tframe_Antenna_View = class(Tframe_View_Base)
    cxSplitter1: TcxSplitter;
    pn_Inspector: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    procedure FormCreate(Sender: TObject);
  private
    Fframe_Inspector: Tframe_Antenna_inspector;
    Fframe_AntType_masks: Tframe_AntType_masks;

    procedure DoOnFieldChanged (Sender: TObject; aFieldName: string; aNewValue: Variant);

  public
    procedure View(aID: integer; aGUID: string); override;
  end;


implementation

{$R *.DFM}



//--------------------------------------------------------------------
procedure Tframe_Antenna_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
  ObjectName:=OBJ_LINKEND_ANTENNA;
  pn_Inspector.Align:= alClient;

  CreateChildForm(Tframe_Antenna_inspector, Fframe_Inspector, pn_Inspector);
  Fframe_Inspector.OnFieldChanged:=DoOnFieldChanged;

  CreateChildForm(Tframe_AntType_masks, Fframe_AntType_masks, TabSheet1);

  AddComponentProp(PageControl1, 'HEIGHT');
  cxPropertiesStore.RestoreFrom;

end;


//--------------------------------------------------------------------
procedure Tframe_Antenna_View.View(aID: integer; aGUID: string);
//--------------------------------------------------------------------
var
  b: Boolean;
  bReadOnly: Boolean;
//  bReadOnly: Boolean;
  iAntTypeID: integer;
 // iGeoRegion_ID: Integer;
begin
   inherited;

  Fframe_Inspector.View (aID);

  iAntTypeID:= Fframe_Inspector.GetIntFieldValue(FLD_ANTENNATYPE_ID);


  // -------------------------
//  FGeoRegion_ID := Fframe_inspector.GetDataset().FieldByName(FLD_GeoRegion_ID).AsInteger;// GetIntFieldValue(FLD_GeoRegion_ID);
//  bReadOnly:=dmUser_Security.GeoRegion_is_ReadOnly(FGeoRegion_ID);

//  Fframe_Inspector.SetReadOnly(bReadOnly, IntToStr(FGeoRegion_ID));
//  Fframe_AntType_masks.SetReadOnly(bReadOnly);
  // -------------------------

//
//
//  iGeoRegion_ID := Fframe_Inspector.GetIntFieldValue(FLD_GeoRegion_ID);
//
//  b:=dmUser_Security.GeoRegion_is_ReadOnly(iGeoRegion_ID);

 // SetReadOnly(b, 'GeoRegion:'+IntToStr(iGeoRegion_ID));


   bReadOnly:=dmUser_Security.Object_Edit_ReadOnly();

   Fframe_Inspector.SetReadOnly(bReadOnly);


//  iAntTypeID:= gl_DB.GetIntFieldValueByID(TBL_ANTENNA, FLD_ANTENNATYPE_ID, aID);

  Fframe_AntType_masks.View (iAntTypeID);
  Fframe_AntType_masks.SetReadOnly(bReadOnly);

end;

//-------------------------------------------------------------------
procedure Tframe_Antenna_View.DoOnFieldChanged (Sender: TObject; aFieldName: string;  aNewValue: Variant);
//-------------------------------------------------------------------
begin
  if Eq(aFieldName, FLD_ANTENNATYPE_ID) then
    Fframe_AntType_masks.View (aNewValue);
end;


end.

