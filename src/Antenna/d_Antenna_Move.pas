unit d_Antenna_Move;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rxPlacemnt, ActnList, StdCtrls, ExtCtrls, Mask,  Db, ADODB,
  cxPropertiesStore,

  d_Wizard,

 // I_MapEngine,
//  I_MapAct,

  I_Options_,
//  X_Options,

  dm_Antenna,
  dm_Property,

//  d_Coordinates,
  //dm_Antenna_tools,

  fr_geo_Coordinates,

  u_types,

  u_func,
  u_db,
  u_Geo,

  u_const,
  u_const_db, cxLookAndFeels
  ;

type
  Tdlg_Antenna_Move = class(Tdlg_Wizard)
    rb_Location_Property: TRadioButton;
    ed_Prop_pos: TEdit;
    rb_Location_Fixed: TRadioButton;
    GroupBox_Pos: TGroupBox;
    procedure FormCreate(Sender: TObject);
//    procedure dxButtonEdit1ButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_OkExecute(Sender: TObject);
//    procedure RadioButton_Update(Sender: TObject);
  private

    FInfoRec: TdmAntennaInfoRec;

  //  FPropertyPos,
    FBLPointOld,
    FBLPoint: TBLPoint;
    FAntennaID: integer;
//    FObjName: string;

    Fframe_geo_Coordinates: Tframe_geo_Coordinates;

    procedure DoOnChange(Sender: TObject);

  public
    class function ExecDlg (aAntennaID: integer): boolean;
  end;


//====================================================================
implementation

uses Options_TLB; {$R *.DFM}
//====================================================================

//--------------------------------------------------------------------
class function Tdlg_Antenna_Move.ExecDlg (aAntennaID: integer): boolean;
//--------------------------------------------------------------------
//var
////iPosType: integer;
//  s: string;
// // iPropertyID: integer;

begin
  with Tdlg_Antenna_Move.Create(Application) do
  try
    FAntennaID := aAntennaID;

    if dmAntenna.GetInfoRec (aAntennaID, FInfoRec) then
    begin
      rb_Location_Property.Checked:=(FInfoRec.Location_Type=0);
      rb_Location_Fixed.Checked   :=(FInfoRec.Location_Type<>0);

      FBLPoint:=FInfoRec.BLPoint;
    end;

  //  iPosType:= dmAntenna.GetPosType (aAntennaID);


//    s:=FObjName;
   // group_Location.ItemIndex:=iPosType;

   // rb_Property_Pos.Checked := (iPosType=0);

  //  FBLPoint   := dmAntenna.GetPos (FAntennaID);
    FBLPointOld:= FBLPoint;

//    iPropertyID :=dmAntenna.GetPropertyID(FAntennaID);
  //  FPropertyPos:=dmProperty.GetPos(iPropertyID);


    ed_Prop_pos.Text:= geo_FormatBLPoint(FBLPoint);

//    dxButtonEdit1.Text:= geo_FormatBLPoint(FBLPoint);
 //aa   ed_Pos.Text:= geo_FormatBLPoint(FBLPoint);

//    Fframe_geo_Coordinates.BLPoint_Pulkovo:=FBLPoint;
//    Fframe_geo_Coordinates.DisplayCoordSys:=IOptions.Get_CoordSys;

    Fframe_geo_Coordinates.Set_BLPoint_and_CoordSys(FBLPoint, EK_CK_42); // IOptions.Get_CoordSys);


    Result:= (ShowModal=mrOk);

  finally
    Free;
  end;
end;

//--------------------------------------------------------------------
procedure Tdlg_Antenna_Move.act_OkExecute(Sender: TObject);
//--------------------------------------------------------------------
var
  rec: TdmAntennaInfoRec;

begin
  inherited;

  FillChar(rec,SizeOf(rec),0);

  rec.BLPoint:=Fframe_geo_Coordinates.GetBLPoint_Pulkovo;

  if rb_Location_Fixed.Checked then
    rec.Location_Type:=1;

  dmAntenna.Move (FAntennaID, rec);


{
  function TdmAntenna.Move(aID: integer; aRec: TdmAntennaInfoRec): Integer;
//-------------------------------------------------------------------
begin
  Update_ (aID,
    [db_Par(FLD_LOCATION_TYPE, aRec.LOCATION_TYPE),
     db_Par(FLD_LAT, aRec.BLPoint.B),
     db_Par(FLD_LON, aRec.BLPoint.L)
    ]);
}


(*  rec.Location_Type :=

  dmAntenna.Move
*)


 ////////// Move;
end;


//--------------------------------------------------------------------
procedure Tdlg_Antenna_Move.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  Caption:='���������� �������';
  SetActionName ('����� ��������� ��� �������.');

  rb_Location_Property.Caption:='����� ���������� ��������';
  rb_Location_Fixed.Caption   :='������� ����� ����������� ����������';


  CreateChildForm(Tframe_geo_Coordinates, Fframe_geo_Coordinates, GroupBox_Pos);
  Fframe_geo_Coordinates.OnPosChange:=DoOnChange;

  GroupBox_Pos.Height := DEF_frame_geo_Coordinates_HEIGHT;


  SetDefaultSize;
end;


//--------------------------------------------------------------------
procedure Tdlg_Antenna_Move.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//--------------------------------------------------------------------
begin

(*  if rb_Location_Property.Checked then
    act_Ok.Enabled:= not geo_ComparePoints (FPropertyPos, FBLPointOld)
  else
*)

  act_Ok.Enabled:= not geo_ComparePoints (FBLPoint, FBLPointOld);

 // ed_Pos.Enabled:= rb_Location_Fixed.Checked;
end;



procedure Tdlg_Antenna_Move.DoOnChange(Sender: TObject);
begin
  FBLPoint:=Fframe_geo_Coordinates.GetBLPoint_Pulkovo;
end;

end.
