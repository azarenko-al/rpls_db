unit u_Antenna_add;

interface

uses 

  dm_Onega_DB_data,

  u_const_db,

  u_geo,
  u_func,
  u_db
  ;


type
  //-------------------------------------------------------------------
  TAntennaAdd = class
  //-------------------------------------------------------------------
    ObjName  :  string;


    NewName:  string;

    Height:   double;
    Azimuth:  double;
    Tilt:     double;
    Loss:  double;

    AntennaType_ID: integer; //0 - ���������
                                             
    Custom: record
              Band:   string;

              Freq_MHz:      double;
              Gain_dB:       double;
              Diameter:      double;
              Polarization_str : string;  //0-horz, 1-vert, 2-kross
              Vert_Width:    double;
              Horz_Width:    double;
            end;


    BLPoint:       TBLPoint;
    LocationType:  byte;

    //(ltProperty,ltFixed); //��� ���������� (��������� � ��������, �����������)


  //  Repeater_ID: Integer;
    LinkEnd_ID: Integer;
//    PMPSector_ID: Integer;
//    PMPTerminal_ID: integer;

  public
    function Add: integer;
  end;




implementation

// ---------------------------------------------------------------
function TAntennaAdd.Add: integer;
// ---------------------------------------------------------------
begin
  Assert(NewName<>'');
//  Assert(Project_ID>0, ' ProjectID <=0');


//  Assert(LINKEND_ID +
  //       PMPSector_ID +
    //     PMPTerminal_ID>0,
      //   'LINKEND_ID + PMPSector_ID + PMPTerminal_ID=0');

  Assert(LINKEND_ID>0, 'LINKEND_ID = 0');

  Assert(Assigned(dmOnega_DB_data));

//  Result:=dmOnega_DB_data.OpenStoredProc( dmOnega_DB_data.ADOStoredProc1,
  Result:=dmOnega_DB_data.ExecStoredProc_( //dmOnega_DB_data.ADOStoredProc1,

  // Result:=db_ExecStoredProc(aADOConnection,
   SP_LINKEND_Antenna_ADD1,

            [
    //        FLD_ObjName, ObjName,

            FLD_NAME,            NewName,

            FLD_ANTENNATYPE_ID,  IIF_NULL(AntennaType_ID),

            FLD_HEIGHT,          Height,
            FLD_AZIMUTH,         Azimuth,

            FLD_TILT,            Tilt,
            FLD_LOSS,            Loss,

            FLD_Band,            IIF_NULL(Custom.Band),
            FLD_FREQ_MHz,        IIF_NULL(Custom.Freq_MHz),
            FLD_GAIN,            IIF_NULL(Custom.Gain_dB),

            FLD_POLARIZATION_STR, IIF_NULL(Custom.Polarization_str),

            FLD_DIAMETER,        IIF_NULL(Custom.Diameter),
            FLD_VERT_WIDTH,      IIF_NULL(Custom.Vert_Width),
            FLD_HORZ_WIDTH,      IIF_NULL(Custom.Horz_Width),


(*             db_Par(FLD_LAT,            IIF_NULL(BLPoint.B)),
             db_Par(FLD_LON,            IIF_NULL(BLPoint.L)),
             db_Par(FLD_LOCATION_TYPE,  LocationType),
*)

          //   db_Par(FLD_Repeater_ID,     IIF_NULL(Repeater_ID)),

             FLD_LINKEND_ID,      LINKEND_ID
//             FLD_PMP_SECTOR_ID,   IIF_NULL(PMPSector_ID),
//             FLD_PMP_TERMINAL_ID, IIF_NULL(PMPTerminal_ID)

            ]);

 //if Result<0 then
 //   g_Log.AddError(Result);


//  Assert(Result>0, 'Result=0');

end;

end.