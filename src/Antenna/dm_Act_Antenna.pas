unit dm_act_Antenna;

interface
{$I ver.inc}

uses
  Classes, Forms, Menus, ActnList, Dialogs, 

  i_Audit,


 // d_Audit,

  dm_Onega_DB_data,

  dm_MapEngine_store,

  dm_act_Base,

  I_Shell,
  u_shell_new,
  //I_Antenna,

  //dm_MapEngine,
  I_Object,

 // I_Act_Antenna,

  dm_Antenna,
 // dm_Cell,

  
  
  dm_LinkEnd,

  fr_Antenna_view,
  d_Antenna_inspector,

  d_Antenna_add,
  d_Antenna_Move,

  u_func_msg,

  
  
  u_func,

  u_Types,


  
  u_const_str,
  u_const_db

  ;

type
  TdmAntennaParentType = (aptCell,aptLinkend);


  TdmAct_Antenna = class(TdmAct_Base) //, IAct_Antenna_X)
    act_Move: TAction;
    ActionList2: TActionList;
    act_Copy: TAction;
    act_Audit1: TAction;

 //   procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
 
  private
    procedure Copy;
    procedure DoAction (Sender: TObject);
  protected
    function ItemDel(aID: integer; aName: string): boolean; override;
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm (aOwnerForm: TForm): TForm; override;
 
  public

    function Dlg_Delete(aAntennaID: integer): Boolean;

    function Dlg_Move(aAntennaID: integer): Boolean;
    function Dlg_Edit(aID: integer): boolean;//  override;


    function Dlg_Add(aObjName: WideString; aParentID: Integer): integer;

//    function Add (aParentID: integer; aType: TdmAntennaParentType): boolean;
//    function Dlg_AddPMPTerminalAntenna(aPmpTerminalID: integer): integer;
    function Dlg_Add_LinkEnd_Antenna(aLinkEndID: integer): integer;
//    function Dlg_AddCellAntenna(aCellID: integer): integer;
//    function Dlg_AddPmpAntenna(aPmpID: integer): integer;
//    function Dlg_AddCellAntenna3G(a3GCellID: integer): integer;

    class procedure Init;

  end;

var
  dmAct_Antenna: TdmAct_Antenna;

//==================================================================
// implementation
//==================================================================
implementation

uses dm_Main;

//uses dm_Localization;

  {$R *.dfm}

// ---------------------------------------------------------------
class procedure TdmAct_Antenna.Init;
// ---------------------------------------------------------------
begin
  Assert(not Assigned(dmAct_Antenna));

  Application.CreateForm(TdmAct_Antenna, dmAct_Antenna);

 // dmAct_Antenna.GetInterface(IAct_Antenna_X, IAct_Antenna);
//  Assert(Assigned(IAct_Antenna));

end;

//
//procedure TdmAct_Antenna.DataModuleDestroy(Sender: TObject);
//begin
////  IAct_Antenna :=nil;
////  dmAct_Antenna:=nil;
//
//  inherited;
//end;



//--------------------------------------------------------------------
procedure TdmAct_Antenna.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
var b: Boolean;
begin
  inherited;
  ObjectName:=OBJ_LINKEND_ANTENNA;

  act_Move.Caption:='�����������';
  act_Copy.Caption:='����������';

  act_Audit1.Caption:=STR_Audit;
//  act_Audit.Enabled:=False;

  SetActionsExecuteProc ([act_Move,
                          act_Copy,
                          act_Audit1
                          ],
                          DoAction);

  SetCheckedActionsArr
    ([
        act_Add,
        act_Del_,
        act_Del_list,
        act_Audit1,

        act_Move,
        act_Copy
     ]);


 // dmLocalization.Load_DataModule (Self);
end;



//--------------------------------------------------------------------
function TdmAct_Antenna.Dlg_Edit(aID: integer): boolean;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_Antenna_inspector.ExecDlg (aID);
end;


//--------------------------------------------------------------------
function TdmAct_Antenna.Dlg_Delete(aAntennaID: integer): Boolean;
//--------------------------------------------------------------------
//var
 // iID: Integer;
var
  sGUID: string;
begin
  Result:=False;

  if ConfirmDlg('������� ?') then
  begin
   {
    iID:=gl_DB.GetIntFieldValue(TBL_PMPSECTOR_ANT_XREF,
                                FLD_ANTENNA_ID,
                                [db_Par(FLD_PMPSECTOR_ID, FID)  ]);
   }

    //if Eq(FType,'ANTENNA') then
//    PostWMsg (WM_CELL_ANTENNA_DEL, iID); // else


//    iID:=gl_DB.GetIntFieldValue(TBL_PMPSECTOR_ANT_XREF,
//                                FLD_ANTENNA_ID,
 //                               [db_Par(FLD_PMPSECTOR_ID, FID)  ]);
   ////   ItemDel(aAntennaID);

    sGUID := dmAntenna.GetGUIDByID(aAntennaID);

    if ItemDel(aAntennaID, '') then
    begin

      Assert(sGUID<>'');

      g_ShellEvents.Shell_PostDelNode(sGUID);

   //   PostEvent (WE_EXPLORER_DELETE_NODE_BY_GUID,  [app_Par(PAR_GUID, sGUID) ]);
//      SH_PostDelNode (sGUID);
    end;

    Result:=True;
  end;

end;


//--------------------------------------------------------------------
function TdmAct_Antenna.Dlg_Move(aAntennaID: integer): boolean;
//--------------------------------------------------------------------
//var oBLPoint: TBLPoint;
begin
//  oBLPoint:= dmAntenna.GetPos(aAntennaID);

  Result:=Tdlg_Antenna_Move.ExecDlg (aAntennaID);

{  if Result then begin
    dmMapEngine1.Sync (otPmpSectorAnt);
  //  dmMapEngine.Sync (otLinkEndAnt);

   // PostEvent (WE_MAP_ENGINE_SYNC_OBJECT, Integer(otCellAnt), aAntennaID);
   // PostEvent (WE_MAP_ENGINE_SYNC_OBJECT, Integer(otLinkEndAnt), aAntennaID);
    dmAct_Map.MapRefresh;
  end;}

end;

// ---------------------------------------------------------------
procedure TdmAct_Antenna.Copy;
// ---------------------------------------------------------------
var
  iID: Integer;
  sNewName: string;
begin
  sNewName:=FFocusedName +' (�����)';

  if InputQuery ('����� ������','������� ����� ��������', sNewName) then
  begin
    iID:=dmOnega_DB_data.Linkend_Antenna_Copy(FFocusedID, sNewName);


//      dmAntType.Copy (FFocusedID, sNewName);

//      sGUIDg_Obj.ItemByName[OBJ_ANTENNA_TYPE].RootFolderGUID;

//    g_ShellEvents.Shell_UpdateNodeChildren_ByGUID(GUID_ANTENNA_TYPE);
 end;

end;

function TdmAct_Antenna.Dlg_Add(aObjName: WideString; aParentID: Integer):
    integer;
begin
  Result := Tdlg_Antenna_add.ExecDlg (aParentID, aObjName);

end;

//--------------------------------------------------------------------
procedure TdmAct_Antenna.DoAction (Sender: TObject);
//--------------------------------------------------------------------
begin

//  if Sender=act_Audit then1ANTENNA, FFocusedID) else
 // if Sender=act_Audit th1

  if Sender=act_Audit1 then
    Init_IAuditX.ExecDlg( dmMain.GetConnectionString, TBL_LINKEND_ANTENNA, FFocusedID) else



  if Sender=act_Copy then
     Copy()
  else

  if Sender=act_Move then
    Dlg_Move (FFocusedID);
end;


//--------------------------------------------------------------------
function TdmAct_Antenna.ItemDel(aID: integer; aName: string): boolean;
//--------------------------------------------------------------------
//var
 // sOwnerObjName: string;
//  sObjName: string;
 // s: string;
 // iAntCount, iLinkEndID: integer;
//  ot: TrpObjectType;

//  Rec: TdmAntennaOwnerInfoRec;
begin

//  if not dmAntenna.GetOwnerInfo1(aID, Rec) then
//    Exit;
//
//  FDeletedIDList.AddObjName(aID, Rec.ObjectName);


{
    Result := Rec.ID
  else
    Result := 0;
}

  // sOwnerObjName:=rec.OwnerObjectName;

 //  sOwnerObjName:=dmAntenna.GetOwnerObjectName (aID);

//  ot:=dmAntenna.GetOwnerType (aID);


//  {$IFDEF link}



//  {$ENDIF}

//  AddTableToBackup (TBL_ANTENNA_XREF,db_Par(FLD_ANTENNA_ID, aID));
//  sObjName:=dmAntenna.GetObjectName (aID);

  Result:=dmAntenna.Del (aID);

//
//  if Result then
//  begin
//    if Assigned(IMapEngine) then
//      dmMapEngine1.DelObject (ObjNameToType(sObjName), aID);
//
//{    case ObjNameToType(s) of
//      otCell_3G:        dmMapEngine1.DelObject (otCellAnt_3G, aID);
//      otCell:           dmMapEngine1.DelObject (otCellAnt, aID);
//      otLinkEnd:        dmMapEngine1.DelObject (otLinkEndAnt, aID);
//      otPmpSector:      dmMapEngine1.DelObject (otPmpSectorAnt, aID);
//      otPmpTerminal:    dmMapEngine1.DelObject (otPmpTerminalAnt, aID);
//    else
//      raise Exception.Create('');
//    end;}
//
//  //  PostEvent (WE_MAP_ENGINE_DEL_OBJECT, Integer(otCellAnt), aID);
////    PostEvent (WE_MAP_ENGINE_DEL_OBJECT, Integer(otLinkEndAnt), aID);
//    if Assigned(IMapAct) then
//      dmAct_Map.MapRefresh;
//  end else
//  begin
//   // RemoveFromBackup(TBL_ANTENNA_XREF,db_Par(FLD_ANTENNA_ID, aID));
//  end;

end;


//--------------------------------------------------------------------
function TdmAct_Antenna.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_Antenna_View.Create(aOwnerForm);
end;


//-------------------------------------------------------------------
function TdmAct_Antenna.Dlg_Add_LinkEnd_Antenna(aLinkEndID: integer): integer;
//-------------------------------------------------------------------
var iAntennaCount,iID: integer;
  iLinkID: Integer;
  sGUID: string;
begin

(*  iAntennaCount:=dmAntenna.GetOwnerAntennaCount (aLinkEndID, OBJ_LINKEND);

  if iAntennaCount >= 2 then
  begin
    ErrorDlg('���������� �������� ������ ���� ������');
    Result:=0;
    Exit;
  end;
*)
  // -------------------------

  iID:=Tdlg_Antenna_add.ExecDlg (aLinkEndID, OBJ_Linkend);

  if iID>0 then
  begin
     sGUID := dmLinkEnd.GetGUIDByID(aLinkEndID);
     g_Shell.UpdateNodeChildren_ByGUID (sGUID);

  //   SH_PostUpdateNodeChildren (dmLinkEnd.GetGUIDByID(aLinkEndID));
     g_EventManager.postEvent_ (WE_LINKEND_ANTENNAS_REFRESH, ['LinkEnd_ID', aLinkEndID] );

//     PostEvent (WE_LINKEND_ANTENNAS_REFRESH, [app_Par(PAR_ID, aLinkEndID)]);

(*
     iLinkID := dmLinkEnd.GetLinkID (aLinkEndID);
     if iLinkID>0 then
       dmLink.UpdateAntennasAzimuthAndLen (iLinkID);
*)

(*     if iAntennaCount=1 then
       dmLinkEnd.Update (aLinkEndID, [db_Par(FLD_KRATNOST_BY_SPACE, 1)]);
*)

//..    PostUpdateFolderAndFocusNode (aFolderID, FID, ntItem);

   //  if Assigned(IMapEngine) then
       dmMapEngine_store.Feature_Add(OBJ_LinkEnd_Antenna, iID);

      // dmMapEngine.CreateObject (otLinkEnd_Antenna, iID);

//     if Assigned(IMapAct) then
    //   dmAct_Map.MapRefresh;

//       PostSyncDB_AddObject (iID, OBJ_LINKEND_ANT);
  end;

  Result:=iID;

end;


//--------------------------------------------------------------------
procedure TdmAct_Antenna.GetPopupMenu;
//--------------------------------------------------------------------
begin
 // CheckActionsEnable();

  case aMenuType of
    mtItem: begin// ,mtList:
              AddMenuItem (aPopupMenu, act_Move);
              AddMenuItem (aPopupMenu, nil);
              AddMenuItem (aPopupMenu, act_Del_);
              AddMenuItem (aPopupMenu, act_Copy);
              AddMenuItem (aPopupMenu, act_Audit1);

     ///////         AddMenuItem (aPopupMenu, act_Undelete);
            end;
  end;

end;



end.




//-------------------------------------------------------------------
function TdmAct_Antenna.Dlg_AddPMPTerminalAntenna(aPmpTerminalID: integer): integer;
//-------------------------------------------------------------------
var iAntennaCount,iID: integer;
  sGUID: string;
begin

//  iAntennaCount:=dmAntenna.GetOwnerAntennaCount (aPmpTerminalID, OBJ_PMP_Terminal);

 // if iAntennaCount < 2 then
 // begin

    iID:=Tdlg_Antenna_add.ExecDlg (aPmpTerminalID, OBJ_PMP_Terminal);

    if iID>0 then
    begin
       sGUID := dmPmpTerminal.GetGUIDByID(aPmpTerminalID);
       g_Shell.UpdateNodeChildren_ByGUID (sGUID);

//       SH_PostUpdateNodeChildren (dmPmpTerminal.GetGUIDByID(aPmpTerminalID));
       PostEvent (WE_PMP_TERMINAL_ANTENNAS_REFRESH, [app_Par(PAR_ID, aPmpTerminalID)]);

       {$IFDEF link}
     /// alex
     ///  dmLink.UpdateAntennasAzimuth (dmPmpTerminal.GetLinkID (aPmpTerminalID));
       {$ENDIF}

  //     if iAntennaCount=1 then
   //      dmLinkEnd.Update (aPmpTerminalID, [db_Par(FLD_KRATNOST_BY_SPACE, 1)]);


        dmMapEngine_store.Feature_Add(OBJ_LinkEnd_Antenna, iID);

 //      dmMapEngine_store.Feature_Add(OBJ_LINKEND, FID);


  //..    PostUpdateFolderAndFocusNode (aFolderID, FID, ntItem);
  //     dmMapEngine.CreateObject (otPmpTerminalAnt, iID);

      // if Assigned(IMapAct) then
    //     dmAct_Map.MapRefresh;

//       PostSyncDB_AddObject (iID, OBJ_CELL_ANT);
    end;

    Result:=iID;

 // end else begin
  //   ErrorDlg('���������� �������� ������ ���� ������');
   //  Result:=0;
 // end;


end;


//-------------------------------------------------------------------
function TdmAct_Antenna.Dlg_AddPmpAntenna(aPmpID: integer): integer;
//-------------------------------------------------------------------
var iID: integer;
  sGUID: string;
begin

  iID:=Tdlg_Antenna_add.ExecDlg (aPmpID, OBJ_PMP_SECTOR );

  if iID>0 then
  begin
    sGUID := dmPmp_Sector.GetGUIDByID(aPmpID);
    g_Shell.UpdateNodeChildren_ByGUID (sGUID);

//    SH_PostUpdateNodeChildren (dmPmp_Sector.GetGUIDByID(aPmpID));

   // SH_PostUpdateNodeChildren (dmPmp S.GetGUIDByID(aParentID));

//..    PostUpdateFolderAndFocusNode (aFolderID, FID, ntItem);
 //   dmMapEngine.CreateObject (otPmpSectorAnt, iID);

//    dmMapEngine_store.Feature_Add(OBJ_LinkEnd_Antenna, iID);


    PostEvent (WE_PMPSECTOR_ANTENNAS_REFRESH,  [app_Par(PAR_NAME, aPmpID)]);

  //  if Assigned(IMapAct) then
  //    dmAct_Map.MapRefresh;

//    PostSyncDB_AddObject (iID, OBJ_PMP_SECTOR_ANT);
  end;

  Result:=iID;//(iID>0);


end;

