unit dm_Antenna;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms, Dialogs, Variants,

  dm_Onega_DB_data,

  u_Antenna_Add,

  dm_Object_base,

  dm_Main,

  //I_AntType,
 /////////// dm_AntType,

  u_types,

  u_const,
  //u_const_db_3g,
  u_const_db,
  u_const_str,


  u_Log,
  u_classes,
  u_func,

  u_db,
  u_Geo
  ;


(*
const
  DEF_ANTENNA_LOCATION_PROPERTY = 0; // fixed property
  DEF_ANTENNA_LOCATION_FIXED    = 1;

*)

type
  TdmAntennaOwnerInfoRec = record
    OwnerObjectName: string;
    ObjectName: string;
    ID: Integer;
  end;


  //-------------------------------------------------------------------
  TdmAntennaAddRec = record
  //-------------------------------------------------------------------
    NewName:     string;

    Height:   double;
    Azimuth:  double;
    Loss:  double;
    Tilt:     double;

    AntennaTypeID: integer; //0 - ���������

    Custom: record
              Freq_MHz: double;
              Gain_dB: double;
              Diameter:      double;
            //  Polarization:  integer;  //0-horz, 1-vert, 2-kross
              Vert_Width:     double;
              Horz_Width:     Double;
            end;


    Parent: record
        //      Repeater_ID: Integer;
              LinkEnd_ID: Integer;
//              PMPSector_ID: Integer;
//              PMPTerminal_ID: integer;
            end;

  end;


  //-------------------------------------------------------------------
  TdmAntennaInfoRec = record
  //-------------------------------------------------------------------
  //  ID : Integer;

    BLPoint:       TBLPoint;
    Location_Type:  byte; //��� ���������� (��������� � ��������, �����������)
    AntennaType_ID: integer; //0 - ���������

    Property_ID : Integer;
  end;



  TdmAntenna = class(TdmObject_base)
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);

  private
// TODO: GetAntennaIDList11111111
//  procedure GetAntennaIDList11111111(aObjName: string; aOwnerID: integer; out
//      aAntIDList: TIDList);
//    function GetObjectNameByOwner1(aObjectName: string): string;
//    function GetOwnerObjectName(aID: integer): string;

  public
    function Move(aID: integer; aRec: TdmAntennaInfoRec): Integer;

    function Del(aID: integer): boolean;

    function Add(aRec: TdmAntennaAddRec; aIDList: TIDList = nil): integer;
    function GetNewName(aOwnerID: integer; aObjName: string): string;

// TODO: GetOwnerAntennaCount
//  function GetOwnerAntennaCount(aOwnerID: integer; aObjName: string): inInteger;
    procedure UpdateHeight1(aID: integer; aHeight: double);
    function GetAntTypeID(aID: integer): integer;

    function GetInfoRec(aID: integer; var aRec: TdmAntennaInfoRec): boolean;
//    function GetObjectName(aID: integer): string;

// TODO: GetOwnerInfo1
//  function GetOwnerInfo1(aID: Integer; var aRec: TdmAntennaOwnerInfoRec): boolean;

  end;
        

function dmAntenna: TdmAntenna;

//===================================================================

implementation
{$R *.DFM}

var
  FdmAntenna: TdmAntenna;

//-------------------------------------------------------------------
function dmAntenna: TdmAntenna;
//-------------------------------------------------------------------
begin
 if not Assigned(FdmAntenna) then
     FdmAntenna := TdmAntenna.Create(Application);

  Result := FdmAntenna;
end;



//-------------------------------------------------------------------
procedure TdmAntenna.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;
  TableName   := TBL_LINKEND_ANTENNA;
  DisplayName := STR_ANTENNA;
  ObjectName  := OBJ_LINKEND_ANTENNA;
end;


//-------------------------------------------------------------------
function TdmAntenna.Add(aRec: TdmAntennaAddRec; aIDList: TIDList = nil): integer;
//-------------------------------------------------------------------
// AntennaTypeID=0 - ������� �������
//-------------------------------------------------------------------
var sTable: string;
/////////  sObjName: string;
  //  iPolarization: integer;
   // dGain,dDiameter: double;

  //  rec: TdmAntTypeInfoRec;
  sObjectName: string;
  oAntennaAdd: TAntennaAdd;
begin
///////  Assert (aRec.AntennaTypeID<>0);
 ///// Assert (aRec.Height>0);
 //san Assert ((aRec.BLPoint.B > 0) and (aRec.BLPoint.L > 0), 'aRec.BLPoint=0');

(*  if (aRec.BLPoint.B=0) or (aRec.BLPoint.L=0) then
    g_Log.AddRecord('dmAntenna', 'TdmAntenna.Add',
                  'Parent.LinkEnd_ID='+AsString(aRec.Parent.LinkEnd_ID)+
                  '; aRec.BLPoint='+AsString(aRec.BLPoint.B)+'/'+AsString(aRec.BLPoint.L));
*)
//  Assert ((aRec.BLPoint.B > 0) and (aRec.BLPoint.L > 0));

//  with aRec do
 //   if (ParentID=0)  then
  //    raise Exception.Create('');

//////////////  geo_ReduceBLPoint(aRec.BLPoint);

  if aRec.NewName='' then
    aRec.NewName:='�������';

  with aRec do
  begin
    Height :=TruncFloat(Height, 1);
    Azimuth:=TruncFloat(Azimuth, 2);


  oAntennaAdd:=TAntennaAdd.Create;

 // oAntennaAdd.Project_ID := dmMain.ProjectID;
  oAntennaAdd.NewName    := aRec.NewName;

  oAntennaAdd.AntennaType_ID := aRec.AntennaTypeID;

  oAntennaAdd.Height  := Height;
  oAntennaAdd.Azimuth := Azimuth;
  oAntennaAdd.Tilt    := Tilt;
  oAntennaAdd.Loss := Loss;

(*  oAntennaAdd.BLPoint:= BLPoint;
  oAntennaAdd.LocationType:= LocationType;
*)

assert ( Parent.LINKEND_ID > 0);

//  oAntennaAdd.Repeater_ID    :=Parent.Repeater_ID;
  oAntennaAdd.LinkEnd_ID     :=Parent.LINKEND_ID;
//  oAntennaAdd.PMPSector_ID   :=Parent.PMPSector_ID;
//  oAntennaAdd.PMPTerminal_ID :=Parent.PMPTerminal_ID;

  Result:=oAntennaAdd.Add;//(dmMain.ADOConnection);

  FreeAndNil(oAntennaAdd);



 ///////// if Parent.Cell_ID>0         then sObjectName:=OBJ_CELL_ANT else
 // if Parent.Cell_3g_ID>0      then sObjectName:=OBJ_CELL_ANT_3G else
//  if Parent.Repeater_ID>0     then sObjectName:=OBJ_LINKEND_Antenna else
  if Parent.LinkEnd_ID>0      then sObjectName:=OBJ_LINKEND_Antenna else
//  if Parent.PMPSector_ID>0    then sObjectName:=OBJ_PMP_SECTOR_ANT else
//  if Parent.PMPTerminal_ID>0  then sObjectName:=OBJ_PMP_TERMINAL_ANT;

  if Assigned(aIDList) then
    aIDList.AddObjName(Result, sObjectName);

  end;
end;


//--------------------------------------------------------------------
function TdmAntenna.GetNewName(aOwnerID: integer; aObjName: string): string;
//--------------------------------------------------------------------
begin
  Result :=dmOnega_DB_data.Linkend_Antenna_GetNewName(aObjName, aOwnerID);
end;



//-------------------------------------------------------------------
procedure TdmAntenna.UpdateHeight1(aID: integer; aHeight: double);
//-------------------------------------------------------------------
begin
  Update_ (aID, [db_Par(FLD_HEIGHT, aHeight)]);
end;


//-------------------------------------------------------------------
function TdmAntenna.GetAntTypeID(aID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=GetIntFieldValue (aID, FLD_ANTENNATYPE_ID);
end;



//-------------------------------------------------------------------
function TdmAntenna.Del(aID: integer): boolean;
//-------------------------------------------------------------------
var iRes: Integer;
begin
  iRes := dmOnega_DB_data.Linkend_Antenna_Del(aID);

//  gl_DB.ExecSP_Result (sp_Antenna_Del, [db_Par(FLD_ID, aID)]);
  Result := true;
end;


//-------------------------------------------------------------------
function TdmAntenna.GetInfoRec(aID: integer; var aRec: TdmAntennaInfoRec): boolean;
//-------------------------------------------------------------------
begin
  FillChar (aRec, SizeOf(aRec), 0);

  dmOnega_DB_data.OpenQuery (qry_Temp,
          'SELECT lat, lon, LOCATION_TYPE, ANTENNATYPE_ID, Property_ID '+
          ' FROM '+ TBL_linkend_antenna +
          ' WHERE id = ' +IntToStr(aID));

  Result:=not qry_Temp.IsEmpty;

  if Result then
    with qry_Temp, aRec do
  begin
     BLPoint.B      := FieldByName(FLD_LAT).AsFloat;
     BLPoint.L      := FieldByName(FLD_LON).AsFloat;
     Location_Type   := FieldByName(FLD_LOCATION_TYPE).AsInteger;
     AntennaType_ID  := FieldByName(FLD_ANTENNATYPE_ID).AsInteger;

     Property_ID  := FieldByName(FLD_Property_ID).AsInteger;
  end;
end;


//-------------------------------------------------------------------
function TdmAntenna.Move(aID: integer; aRec: TdmAntennaInfoRec): Integer;
//-------------------------------------------------------------------
var
  k: Integer;
begin
  k := dmOnega_DB_data.ExecStoredProc_('sp_Linkend_Antenna_Move',
    [
     FLD_ID, aID,

     FLD_LOCATION_TYPE, aRec.LOCATION_TYPE,
     FLD_LAT, aRec.BLPoint.B,
     FLD_LON, aRec.BLPoint.L
    ]);
{
  aID);




  Update_ (aID,
    [db_Par(FLD_LOCATION_TYPE, aRec.LOCATION_TYPE),
     db_Par(FLD_LAT, aRec.BLPoint.B),
     db_Par(FLD_LON, aRec.BLPoint.L)
    ]);}


 (* db_OpenQuery (qry_Temp,
          'SELECT lat, lon, LOCATION_TYPE, ANTENNATYPE_ID, Property_ID '+
          ' FROM '+ TBL_linkend_antenna + ' WHERE id = ' +IntToStr(aID));
*)

end;


end.






