object frm_Antenna_list: Tfrm_Antenna_list
  Left = 443
  Top = 256
  Width = 899
  Height = 432
  Caption = 'frm_Antenna_list'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 891
    Height = 200
    Align = alTop
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      OptionsCustomize.ColumnsQuickCustomization = True
      object cxGrid1DBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Width = 55
      end
      object cxGrid1DBTableView1PropertyName: TcxGridDBColumn
        DataBinding.FieldName = 'Property.Name'
        SortIndex = 0
        SortOrder = soAscending
        Width = 157
      end
      object cxGrid1DBTableView1name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Width = 123
      end
      object cxGrid1DBTableView1antennaType_id: TcxGridDBColumn
        DataBinding.FieldName = 'antennaType_id'
        Width = 83
      end
      object cxGrid1DBTableView1lat: TcxGridDBColumn
        DataBinding.FieldName = 'lat'
        Width = 139
      end
      object cxGrid1DBTableView1lon: TcxGridDBColumn
        DataBinding.FieldName = 'lon'
        Width = 127
      end
      object cxGrid1DBTableView1height: TcxGridDBColumn
        DataBinding.FieldName = 'height'
      end
      object cxGrid1DBTableView1azimuth: TcxGridDBColumn
        DataBinding.FieldName = 'azimuth'
      end
      object cxGrid1DBTableView1tilt: TcxGridDBColumn
        DataBinding.FieldName = 'tilt'
      end
      object cxGrid1DBTableView1heel: TcxGridDBColumn
        DataBinding.FieldName = 'heel'
      end
      object cxGrid1DBTableView1location_type: TcxGridDBColumn
        DataBinding.FieldName = 'location_type'
      end
      object cxGrid1DBTableView1loss_dB: TcxGridDBColumn
        DataBinding.FieldName = 'loss_dB'
      end
      object cxGrid1DBTableView1band: TcxGridDBColumn
        DataBinding.FieldName = 'band'
        Width = 54
      end
      object cxGrid1DBTableView1freq_MHz: TcxGridDBColumn
        DataBinding.FieldName = 'freq_MHz'
      end
      object cxGrid1DBTableView1gain: TcxGridDBColumn
        DataBinding.FieldName = 'gain'
      end
      object cxGrid1DBTableView1diameter: TcxGridDBColumn
        DataBinding.FieldName = 'diameter'
      end
      object cxGrid1DBTableView1polarization_str: TcxGridDBColumn
        DataBinding.FieldName = 'polarization_str'
      end
      object cxGrid1DBTableView1vert_width: TcxGridDBColumn
        DataBinding.FieldName = 'vert_width'
      end
      object cxGrid1DBTableView1horz_width: TcxGridDBColumn
        DataBinding.FieldName = 'horz_width'
      end
      object cxGrid1DBTableView1is_master: TcxGridDBColumn
        DataBinding.FieldName = 'is_master'
      end
      object cxGrid1DBTableView1property_id: TcxGridDBColumn
        DataBinding.FieldName = 'property_id'
      end
      object cxGrid1DBTableView1linkend_id: TcxGridDBColumn
        DataBinding.FieldName = 'linkend_id'
      end
      object cxGrid1DBTableView1pmp_sector_id: TcxGridDBColumn
        DataBinding.FieldName = 'pmp_sector_id'
      end
      object cxGrid1DBTableView1pmp_terminal_id: TcxGridDBColumn
        DataBinding.FieldName = 'pmp_terminal_id'
      end
      object cxGrid1DBTableView1date_created: TcxGridDBColumn
        DataBinding.FieldName = 'date_created'
      end
      object cxGrid1DBTableView1date_modify: TcxGridDBColumn
        DataBinding.FieldName = 'date_modify'
      end
      object cxGrid1DBTableView1user_created: TcxGridDBColumn
        DataBinding.FieldName = 'user_created'
      end
      object cxGrid1DBTableView1user_modify: TcxGridDBColumn
        DataBinding.FieldName = 'user_modify'
      end
      object cxGrid1DBTableView1antennaTypename: TcxGridDBColumn
        DataBinding.FieldName = 'antennaType.name'
      end
      object cxGrid1DBTableView1Propertylat: TcxGridDBColumn
        DataBinding.FieldName = 'Property.lat'
      end
      object cxGrid1DBTableView1Propertylon: TcxGridDBColumn
        DataBinding.FieldName = 'Property.lon'
      end
      object cxGrid1DBTableView1LinkEndname: TcxGridDBColumn
        DataBinding.FieldName = 'LinkEnd.name'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object ADOQuery1: TADOQuery
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega_1'
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from view_Linkend_Antenna_summary')
    Left = 104
    Top = 232
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 104
    Top = 280
  end
end
