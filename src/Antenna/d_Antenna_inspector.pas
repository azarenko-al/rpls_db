unit d_Antenna_inspector;

interface

uses
  Classes, Controls, Forms,
  rxPlacemnt,

  u_func,

  fr_Antenna_inspector, StdCtrls, ExtCtrls
  ;


type
  Tdlg_Antenna_inspector = class(TForm)
    FormPlacement1: TFormPlacement;
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    Panel1: TPanel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    procedure FormCreate(Sender: TObject);
  private
    Fframe_Antenna_inspector: Tframe_Antenna_inspector;

  public
    class function ExecDlg(aID: integer): Boolean;
  end;


implementation
{$R *.dfm}




//--------------------------------------------------------------------
class function Tdlg_Antenna_inspector.ExecDlg(aID: integer): boolean;
//--------------------------------------------------------------------
begin
  with Tdlg_Antenna_inspector.Create(Application) do
  try
    Fframe_Antenna_inspector.View(aID);
   //  View (aID);

    Result:=(ShowModal=mrOK);

    if Result then
      Fframe_Antenna_inspector.Post;


(*
    FormPlacement1.Active:=True;
    FormPlacement1.RestoreFormPlacement;

    pn_Buttons.Visible:=True;

    View (aID);

    Result:=(ShowModal=mrOK);
    if Result then
      Post;
*)

  finally
    Free;
  end;

end;


procedure Tdlg_Antenna_inspector.FormCreate(Sender: TObject);
begin
  Caption :='�������';
  CreateChildForm(Tframe_Antenna_inspector, Fframe_Antenna_inspector, Self);
end;


end.
