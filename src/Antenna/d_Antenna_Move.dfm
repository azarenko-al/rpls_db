inherited dlg_Antenna_Move: Tdlg_Antenna_Move
  Left = 755
  Top = 243
  Width = 483
  Height = 407
  Caption = 'dlg_Antenna_Move'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 344
    Width = 475
    inherited Bevel1: TBevel
      Width = 475
    end
    inherited Panel3: TPanel
      Left = 302
      Width = 173
      inherited btn_Ok: TButton
        Left = 3
      end
      inherited btn_Cancel: TButton
        Left = 87
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 475
    inherited Bevel2: TBevel
      Width = 475
    end
    inherited pn_Header: TPanel
      Width = 475
    end
  end
  object rb_Location_Property: TRadioButton [2]
    Left = 5
    Top = 72
    Width = 301
    Height = 17
    Caption = #1042#1079#1103#1090#1100' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1099' '#1087#1083#1086#1097#1072#1076#1082#1080
    Checked = True
    TabOrder = 2
    TabStop = True
  end
  object ed_Prop_pos: TEdit [3]
    Left = 5
    Top = 95
    Width = 462
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    ParentColor = True
    ReadOnly = True
    TabOrder = 3
  end
  object rb_Location_Fixed: TRadioButton [4]
    Left = 5
    Top = 127
    Width = 301
    Height = 17
    Caption = #1040#1085#1090#1077#1085#1085#1072' '#1080#1084#1077#1077#1090' '#1089#1086#1073#1089#1090#1074#1077#1085#1085#1099#1077' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1099
    TabOrder = 4
    TabStop = True
  end
  object GroupBox_Pos: TGroupBox [5]
    Left = 5
    Top = 146
    Width = 462
    Height = 106
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 5
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 204
  end
  inherited FormStorage1: TFormStorage
    Left = 176
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 231
  end
end
