unit d_Antenna_add;

interface
{$I ver.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,ComCtrls,
    rxPlacemnt, ActnList,    StdCtrls, ExtCtrls,
  cxPropertiesStore, cxVGrid, cxControls, cxInplaceContainer,  cxDropDownEdit, cxButtonEdit,
   dm_User_Security,

  d_Wizard_add_with_Inspector,

 // I_Act_Explorer, //
  dm_Act_Explorer,


  d_geo_Coordinates,
  fr_geo_Coordinates,

  dm_Main,
  I_Shell,

  dm_Act_LinkEnd,

//  I_Options,

  dm_Property,
  dm_AntType,
  dm_Antenna,

  u_Antenna_add,

  dm_LinkEnd,
  dm_Pmp_Sector,
  dm_PmpTerminal,

  u_Types,

  u_const_db,
  u_const_str,

  u_cx_vgrid_export,

  u_db,
  u_dlg,
  u_cx_vgrid,
 //   
  u_Geo,
  u_reg,
  u_func, cxLookAndFeels

  ;


type
  Tdlg_Antenna_add = class(Tdlg_Wizard_add_with_Inspector)
    cxVerticalGrid1: TcxVerticalGrid;
    row_Antenna_Model: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    row_Height: TcxEditorRow;
    row_Azimuth: TcxEditorRow;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;
    row_PosType: TcxEditorRow;
    row_Pos: TcxEditorRow;
    GroupBox1: TGroupBox;
    sb_Raznos: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure DoOnButtonClick(Sender: TObject; AbsoluteIndex: Integer);
//    procedure row_PosType1EditPropertiesChange(Sender: TObject);
  private
    FPropertyID: integer;
    FID: integer;

    FRec: TdmAntennaAddRec;

  //  FParentObjName: string;

    procedure ShowRaznos;
    procedure Append;
  //  procedure DoOnChange(Sender: TObject);
  public
    class function ExecDlg (aParentID: integer;
                            aOwnerObjName: string): integer;
  end;



//===================================================================
// implementation
//===================================================================

implementation {$R *.DFM}


//-------------------------------------------------------------------
class function Tdlg_Antenna_add.ExecDlg(aParentID: integer;
                           aOwnerObjName: string): integer;
//-------------------------------------------------------------------
//var iPropID: integer;
begin
{
  if dmUser_Security.ProjectIsReadOnly then
  begin
    ShowMessage('���������� �������� ������. ������ ������ ��� ������.');
    Result:=0;
    Exit;
  end;
}


  with Tdlg_Antenna_add.Create(Application) do
  try

    ShowRaznos;
    FRec.Parent.LinkEnd_ID:=aParentID;

    {
    case ObjNameToType(aOwnerObjName) of
      otLinkEnd:     begin
                       ShowRaznos;
                       FRec.Parent.LinkEnd_ID:=aParentID;
                     end;

      otPMPSector :  FRec.Parent.PMPSector_ID:=aParentID;

      otPMPTerminal: FRec.Parent.PMPTerminal_ID:=aParentID;

    //  otRepeater:    FRec.Parent.Repeater_ID:=aParentID;

    else
      raise Exception.Create(' case ObjNameToType(aOwnerObjName) of: '+ aOwnerObjName);
    end;
     }


    ed_Name_.Text:=dmAntenna.GetNewName(aParentID, aOwnerObjName);

//      Result :=dmOnega_DB_data.Linkend_Antenna_GetNewName(aObjName, aOwnerID);


   // row_Pos.Properties.Value:=geo_FormatBLPoint (FRec.BLPoint);

  //  Fframe_geo_Coordinates.BLPoint_Pulkovo :=FRec.BLPoint;
  //  Fframe_geo_Coordinates.DisplayCoordSys:=IOptions.GeoCoord;

    if ShowModal=mrOk then
      Result:=FID
    else
      Result:=0;

  finally
    Free;
  end;
end;

//-------------------------------------------------------------------
procedure Tdlg_Antenna_add.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
var
  s: string;
begin
  inherited;

  SetActionName(STR_DLG_ADD_ANTENNA);

  row_PosType.Properties.Caption:=STR_LOCATION_TYPE;



  FRec.AntennaTypeID := reg_ReadInteger (FRegPath, FLD_ANTENNATYPE_ID, 0);
  row_Antenna_Model.Properties.Value := gl_DB.GetNameByID(TBL_ANTENNATYPE, FRec.AntennaTypeID);


  cx_InitVerticalGrid (cxVerticalGrid1);


  sb_Raznos.Visible:=false;

  row_Antenna_Model.Properties.Caption:= STR_ANTENNA_TYPE;

  SetDefaultWidth();


  s:=FRegPath;

//  cx_VerticalGrid_LoadFromReg(cxVerticalGrid1, FRegPath);

//  AddComponentProp(row_Antenna_Model, DEF_PropertiesValue);
  AddComponentProp(row_Height,        DEF_PropertiesValue);
  AddComponentProp(row_Azimuth,       DEF_PropertiesValue);

  RestoreFrom;

end;


procedure Tdlg_Antenna_add.FormDestroy(Sender: TObject);
begin
 // cx_VerticalGrid_SaveToReg(cxVerticalGrid1, FRegPath);

  reg_WriteInteger(FRegPath, FLD_ANTENNATYPE_ID, FRec.AntennaTypeID);

  inherited;
end;


procedure Tdlg_Antenna_add.act_OkExecute(Sender: TObject);
begin
//  inherited;
  Append;
end;


//-------------------------------------------------------------------
procedure Tdlg_Antenna_add.ShowRaznos;
//-------------------------------------------------------------------
var
  d: double;
begin
  if (FRec.Parent.LinkEnd_ID > 0) and
    // (dmAntenna.GetOwnerAntennaCount (Frec.Parent.LinkEnd_ID, OBJ_LINKEND) = 1) and
     (FRec.AntennaTypeID > 0)
  then
  begin
    d:=dmAntType.GetRaznos_MHZ (FRec.AntennaTypeID);
    if d = 0 then
      exit;

    sb_Raznos.Panels[0].Text:= Format('������������� ������ = %1.2f m', [d]);// '+ AsString(TruncFloat(d, 2));
    sb_Raznos.Visible:=true;
  end
  else
    sb_Raznos.Visible:=false;
end;




//-------------------------------------------------------------------
procedure Tdlg_Antenna_add.Append;
//-------------------------------------------------------------------
var
  FAntennaAdd: TAntennaAdd;

begin
 // FillChar(rec, SizeOf(rec), 0);

  FRec.NewName := ed_Name_.Text;
  FRec.Height  := AsFloat(row_Height.Properties.Value);
  FRec.Azimuth := row_Azimuth.Properties.Value;


//  FRec.PropertyID:=FPropertyID;
//  FRec.AntennaTypeID := FAntTypeID;
//  FRec.ParentID:=FParentID;
//..  rec.BLPoint :=FBLPoint;

  { TODO : ? }
 // recAntenna.BLPoint:=


/////////  FID:=dmAntenna.Add (FRec);
  FAntennaAdd := TAntennaAdd.Create();

//  FAntennaAdd.Project_ID:=dmMain.ProjectID;
  FAntennaAdd.NewName   :=ed_Name_.Text;

  FAntennaAdd.Height    := AsFloat(row_Height.Properties.Value);
  FAntennaAdd.Azimuth   := AsFloat(row_Azimuth.Properties.Value);
//  FAntennaAdd.PropertyID:= FPropertyID;

  FAntennaAdd.AntennaType_ID :=FRec.AntennaTypeID;

  FAntennaAdd.LinkEnd_ID    :=FRec.Parent.LinkEnd_ID;
//  FAntennaAdd.PMPSector_ID  :=FRec.Parent.PMPSector_ID;
//  FAntennaAdd.PMPTerminal_ID:=FRec.Parent.PMPTerminal_ID;

 // FAntennaAdd.Repeater_ID   :=FRec.Parent.Repeater_ID;

// Assert(FAntennaAdd.LinkEnd_ID +
//        FAntennaAdd.PMPSector_ID  +
  //      FAntennaAdd.PMPTerminal_ID > 0
//        );


  FID:=FAntennaAdd.Add;//(dmMain.ADOConnection);

  FreeAndNil(FAntennaAdd);
end;



//-------------------------------------------------------------------
procedure Tdlg_Antenna_add.DoOnButtonClick(Sender: TObject; AbsoluteIndex: Integer);
//-------------------------------------------------------------------
var

  sName: Widestring;
begin
 // Assert(Assigned(IShell));
 // Assert(Assigned(ILinkEnd));


  if cxVerticalGrid1.FocusedRow=row_Antenna_Model then
  begin

//   (Sender as TcxButtonEdit).EditValue:=FtmpName;
    case AbsoluteIndex of    //
      0:  if dmAct_Explorer.Dlg_Select_Object (otAntennaType, FRec.AntennaTypeID, sName) then
          begin

            cx_SetButtonEditText(Sender as TcxButtonEdit, sName);

         //   row_Antenna_Model.Properties.Value:=sName;
          //  (Sender as TcxButtonEdit).EditValue:=sName;
           // aID:=iID;

            ShowRaznos;

           // if FRec.Parent.LinkEnd_ID > 0 then
            //  dmAct_LinkEnd.CheckRanges(FRec.Parent.LinkEnd_ID, FRec.AntennaTypeID);

          end;

      1: begin
           FRec.AntennaTypeID:=0;
           cx_SetButtonEditText(Sender as TcxButtonEdit, '');
         end;
    end;

 

  end;


end;


end.

        {


procedure Tdlg_Antenna_add.DoOnChange(Sender: TObject);
begin
//  FRec.BLPoint:=Fframe_geo_Coordinates.BLPoint_Pulkovo;
end;


procedure Tdlg_Antenna_add.row_PosType1EditPropertiesChange(
  Sender: TObject);
begin
/////////////  Fframe_geo_Coordinates.ReadOnly:= (GetPosType()=0);

end;

