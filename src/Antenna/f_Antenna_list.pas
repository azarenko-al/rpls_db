unit f_Antenna_list;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, DB,
  ADODB, cxGridLevel, cxClasses, cxControls, cxGridCustomView, cxGrid,

  dm_Main,

  u_db,
  u_Storage
  ;

type
  Tfrm_Antenna_list = class(TForm)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    ADOQuery1: TADOQuery;
    DataSource1: TDataSource;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    cxGrid1DBTableView1antennaType_id: TcxGridDBColumn;
    cxGrid1DBTableView1lat: TcxGridDBColumn;
    cxGrid1DBTableView1lon: TcxGridDBColumn;
    cxGrid1DBTableView1height: TcxGridDBColumn;
    cxGrid1DBTableView1azimuth: TcxGridDBColumn;
    cxGrid1DBTableView1tilt: TcxGridDBColumn;
    cxGrid1DBTableView1heel: TcxGridDBColumn;
    cxGrid1DBTableView1location_type: TcxGridDBColumn;
    cxGrid1DBTableView1loss_dB: TcxGridDBColumn;
    cxGrid1DBTableView1band: TcxGridDBColumn;
    cxGrid1DBTableView1freq_MHz: TcxGridDBColumn;
    cxGrid1DBTableView1gain: TcxGridDBColumn;
    cxGrid1DBTableView1diameter: TcxGridDBColumn;
    cxGrid1DBTableView1polarization_str: TcxGridDBColumn;
    cxGrid1DBTableView1vert_width: TcxGridDBColumn;
    cxGrid1DBTableView1horz_width: TcxGridDBColumn;
    cxGrid1DBTableView1is_master: TcxGridDBColumn;
    cxGrid1DBTableView1property_id: TcxGridDBColumn;
    cxGrid1DBTableView1linkend_id: TcxGridDBColumn;
    cxGrid1DBTableView1pmp_sector_id: TcxGridDBColumn;
    cxGrid1DBTableView1pmp_terminal_id: TcxGridDBColumn;
    cxGrid1DBTableView1date_created: TcxGridDBColumn;
    cxGrid1DBTableView1date_modify: TcxGridDBColumn;
    cxGrid1DBTableView1user_created: TcxGridDBColumn;
    cxGrid1DBTableView1user_modify: TcxGridDBColumn;
    cxGrid1DBTableView1antennaTypename: TcxGridDBColumn;
    cxGrid1DBTableView1Propertylat: TcxGridDBColumn;
    cxGrid1DBTableView1Propertylon: TcxGridDBColumn;
    cxGrid1DBTableView1PropertyName: TcxGridDBColumn;
    cxGrid1DBTableView1LinkEndname: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Open;

  end;

var
  frm_Antenna_list: Tfrm_Antenna_list;

implementation

{$R *.dfm}

procedure Tfrm_Antenna_list.FormCreate(Sender: TObject);
begin
  //select * from view_Linkend_Antenna_summary
end;

procedure Tfrm_Antenna_list.Open;
begin

end;

end.
