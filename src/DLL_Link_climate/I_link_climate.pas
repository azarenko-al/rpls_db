unit I_link_climate;

interface

{$DEFINE test111}

uses
  Windows,

  //u_link_climate;

 // u_dll,
  u_Geo;

type

  // ---------------------------------------------------------------
  TLinkClimateDataRec_ITU = packed record
  // ---------------------------------------------------------------
    underlying_terrain_type : Integer;

    Q_factor: double;       // = 'Q_factor';     //5= 1.000 ;Q-������ ������ �����������
    terrain_type: integer;  // = 'terrain_type'; //3= 1.000 ;��� ������������ �������.(1...3,0->����.�����=����.���.)


    Factor_C: double;       // = 'factor_C';     //8= 0.500 ;�������� ������������ c ��� ������� ������


    Climate_factor: double; // = 'climate_factor'; //6=41.000     ;������������� ������ K��, 10^-6
    Factor_B: double;       // = 'factor_B';     //7= 1.500 ;�������� ������������ b ��� ������� ������
    Factor_D: double;       // = 'factor_D';     //9= 2.000 ;�������� ������������ d ��� ������� ������


    RadioClimate_Region_name: ShortString;    //  ��� ������

    gradient_diel: double;  // = 'gradient_diel';//1=-9.000 ;������� �������� ��������� ����.�������������,10^-8 1/�
    gradient_deviation: double;  // = ���������� ���������

    rain_intensity1: double;   //  = 'rain_rate' ������������� �����
    steam_wet: double;      // 10. ���������� ��������� �������� ���� [g/cub.m]

  end;
 // ---------------------------------------------------------------



  // ---------------------------------------------------------------
  TLinkClimateDataRec = packed record
  // ---------------------------------------------------------------
    Comment    : ShortString;    //  ��� ������
    LinkType_ID: Integer;

    air_temperature     : double; //6
    atmosphere_pressure : double; //7


     GOST_gradient_diel: double;
     GOST_gradient_deviation: double;
     GOST_terrain_type: double;
     GOST_underlying_terrain_type: double;
     GOST_steam_wet: double;
     GOST_air_temperature: double;
     GOST_atmosphere_pressure: double;
     GOST_climate_factor: double;
     GOST_rain_intensity: double;

     NIIR1998_gradient_diel: double;
     NIIR1998_gradient_deviation: double;
     NIIR1998_terrain_type: double;
     NIIR1998_steam_wet: double;
     NIIR1998_air_temperature: double;
     NIIR1998_atmosphere_pressure: double;
     NIIR1998_rain_region_number: integer;
     NIIR1998_Qd_region_number  : integer;
     NIIR1998_water_area_percent: double;



    ITU  : TLinkClimateDataRec_ITU;
    GOST : record
             rain_intensity: double;   //  = 'rain_rate' ������������� �����

           end;

    // -------------------------
    NIIR_1998: record
    // -------------------------
      //9. ���� ������ ����������� �� �������-��, %
      Water_area_percent : double;

      //humidity.TAB   ���������
      absolute_humidity : Double; //���������

      //rain_8_11.TAB
      //����� ������ �� ����� ���� (���.8.11) ��� ������������� ������
      rain_8_11_intensity_region_number : Integer;

      //rain_8_15.TAB
      //����� ������ �� ����� ���� (���.8.15) ��� ��������� Q�
      rain_8_15_Qd_region_number : Integer;
    end;

  

  //  TLinkClimateDataRec_NIIR_1998;
  end;


  ILink_climate_fileX = interface(IInterface)
  ['{6C57D22C-1B19-4736-A147-C92EFE2A819E}']
    function OpenFiles: Boolean; stdcall;
    procedure CloseFiles; stdcall;

 //   function OpenFiles_GOST: Boolean; stdcall;
  //  procedure CloseFiles_GOST; stdcall;

    function GetByBLPoint(aBLPoint: TBLPoint; var aRec:
        TLinkClimateDataRec): Boolean; stdcall;

 //   function GetByBLPoint_GOST(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec):
   //     Boolean; stdcall;
  end;


  function Get_ILink_climate_fileX: ILink_climate_fileX;



var
  ILink_climate_file: ILink_climate_fileX;


function Load_ILink_climate_file: boolean;
procedure UnLoad_ILink_climate_file;



implementation

{$IFDEF test1111}
uses
  u_Link_climate_file;

{$ELSE}
const
  DEF_DLL = 'dll_link_climate.dll';

  function GetInterface_ILink_climate_fileX(var Obj): HResult; stdcall; external
     DEF_DLL;
{$ENDIF}


(*
uses
  u_Link_climate_file;
*)


var
  LHandle : Integer;


function Load_ILink_climate_file: boolean;
begin
//  Result := GetInterface_dll(DEF_DLL, LHandle, ILink_climate_fileX, ILink_climate_file)=0;
//  Result := DllGetInterface(ILink_climate_fileX, ILink_climate_file)=0;

  Result :=GetInterface_ILink_climate_fileX(ILink_climate_file) = 0;

  Assert(Assigned(ILink_climate_file), 'ILink_climate_file not assigned');

end;


procedure UnLoad_ILink_climate_file;
begin
  ILink_climate_file := nil;
//  UnloadPackage_dll(LHandle);
end;


function Get_ILink_climate_fileX: ILink_climate_fileX;
var
  b: Boolean;
begin
//  GetInterface_dll(DEF_DLL, LHandle, ILink_climate_fileX, Result);

  b:=GetInterface_ILink_climate_fileX(Result)=0;

 // DllGetInterface(ILink_climate_fileX, Result);

end;


end.



(*


    //radioklimat.TAB
    radioklimat: packed record
                  region_number      : Integer;

                //  name               : WideString;
                  gradient_summer    : Double;
                  deviation_summer   : Double;
                  gradient_winter    : Double;
                  deviation_winter   : Double;

                  gradient           : Double;
                  gradient_deviation : Double;

            //      months           : WideString;

                end;
*)
