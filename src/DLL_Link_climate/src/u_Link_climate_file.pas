unit u_Link_climate_file;

interface
//{$I mapx.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Variants, FileCtrl,

  MapXLib_TLB,

  u_debug,

  u_assert,

  u_MapX_lib,

  u_func,
  u_files,
  u_Geo,

  I_link_climate
  ;

type

  TLink_climate_file = class(TInterfacedObject, ILink_climate_fileX)
  private
    FActive: boolean;

    FFileDir: string;

    FMap_Q_factor        : TmiMap;
    FMap_RadioKlimRegion : TmiMap;
    FMap_Climate         : TmiMap;
    FMap_ClimateFactor   : TmiMap;


//      DEF_FileName_NIIR_gradient     ='NIIR_gradient.TAB';

    FMap_NIIR_gradient: TmiMap;

    FMap_NIIR_1998_rain_8_11: TmiMap;
    FMap_NIIR_1998_rain_8_15: TmiMap;
    FMap_NIIR_1998_humidity: TmiMap;

    FMap_GOST_rain_intensity: TmiMap;

    FMap_ESARAIN_R001: TmiMap;





    function ValidateFiles: Boolean;

    function DoGetRainIntenseByPoint(aPoint: TBLPoint): Double;

    function GetFileDir: string;

    function Get_Climate(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec): Boolean;
    function Get_ClimateFactor(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec): Boolean;
    function Get_GOST_rain(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec):
        Boolean;
    function Get_Q_factor(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec): Boolean;
    function Get_RadioKlimRegion(aBLPoint: TBLPoint; var aRec:  TLinkClimateDataRec): Boolean;



    function Get_NIIR_1998_rain_8_11(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec): Boolean;
    function Get_NIIR_1998_rain_8_15(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec): Boolean;
    function Get_NIIR_1998_humidity(aBLPoint: TBLPoint; var aRec:  TLinkClimateDataRec): Boolean;

    function Get_NIIR_gradient(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec): Boolean;

    function Get_ESARAIN_R001(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec):  Boolean;

//    FMap_NIIR_gradient



    //humidity.TAB
    //radioklimat.TAB
    //rain_8_11.TAB
    //rain_8_15.TAB


// TODO: test_CreateMap
//  procedure test_CreateMap;

  public
    constructor Create;
    destructor Destroy; override;

    function OpenFiles: Boolean; stdcall;
    procedure CloseFiles; stdcall;



    function GetByBLPoint(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec):
        Boolean; stdcall;

    property Active: boolean read FActive;
  //  property Active_GOST: boolean read FActive_GOST;
  end;



  function GetInterface_ILink_climate_fileX(var Obj): HResult; stdcall;

exports
  GetInterface_ILink_climate_fileX;


implementation


function GetInterface_ILink_climate_fileX(var Obj): HResult;
begin
  Result:=S_FALSE; Integer(Obj):=0;

  with TLink_climate_file.Create do
    if GetInterface(ILink_climate_fileX, Obj) then
      Result:=S_OK

end;



const
  DEF_CLIMATE_FOLDER_NAME = '�����������������\';

  DEF_FileName_Climate          = '������.tab';
  DEF_FileName_Q_factor         = 'Q������.tab';
  DEF_FileName_RadioKlimRegion  = 'RadioKlimRegion.tab';
  DEF_FileName_ClimateFactor    = '������������.tab';
  DEF_FileName_Rain             = 'ESARAIN_R001.PRN';

//  DEF_FileName_humidity        ='NIIR_humidity.tab';
  DEF_FileName_NIIR_gradient     ='NIIR_gradient.TAB';

  DEF_FileName_rain_8_11       ='NIIR_rain_8_11.tab';
  DEF_FileName_rain_8_15       ='NIIR_rain_8_15.tab';
  DEF_FileName_humidity        ='NIIR_humidity.tab';

  DEF_FileName_GOST_rain_intensity = 'GOST_rain_intensity.tab';

  DEF_FileName_ESARAIN_R001 = 'ESARAIN_R001.tab';



 // FILE_ARR: array[0..4] of string;


// ---------------------------------------------------------------
constructor TLink_climate_file.Create;
// ---------------------------------------------------------------
var
  I: Integer;
  oSList: TStringList;
  s: string;
begin
  inherited;

  FFileDir := GetFileDir();

//  FRainIntense := TRainIntense.Create();
end;


// ---------------------------------------------------------------
function TLink_climate_file.ValidateFiles: Boolean;
// ---------------------------------------------------------------
var
  I: Integer;
  oSList: TStringList;
  s: string;
begin
  inherited;

//  FFileDir := GetFileDir();

  oSList := TStringList.Create;

  s:=FFileDir + DEF_FileName_Q_factor;
  if not FileExists(s) then oSList.Add(s);

  s:=FFileDir + DEF_FileName_RadioKlimRegion;
  if not FileExists(s) then oSList.Add(s);

  s:=FFileDir + DEF_FileName_Climate;
  if not FileExists(s) then oSList.Add(s);

  s:=FFileDir + DEF_FileName_ClimateFactor;
  if not FileExists(s) then oSList.Add(s);

  s:=FFileDir + DEF_FileName_Rain;
  if not FileExists(s) then oSList.Add(s);

  s:=FFileDir + DEF_FileName_humidity;
  if not FileExists(s) then oSList.Add(s);

  s:=FFileDir + DEF_FileName_GOST_rain_intensity;
  if not FileExists(s) then oSList.Add(s);


  s:=FFileDir + DEF_FileName_NIIR_gradient;
  if not FileExists(s) then oSList.Add(s);

  s:=FFileDir + DEF_FileName_ESARAIN_R001;
  if not FileExists(s) then oSList.Add(s);



//  FFileExists:= oSList.Count=0;

  Result := oSList.Count=0;

  if oSList.Count>0 then
  begin
    oSList.Insert(0, '��� ������ �� ������������� ����� � ��������: '+ FFileDir);

    ShowMessage(oSList.Text);
  end;


(*        FileExists (FFileDir + DEF_FileName_Q_factor) and
        FileExists (FFileDir + DEF_FileName_RadioKlimRegion) and
        FileExists (FFileDir + DEF_FileName_Climate) and
        FileExists (FFileDir + DEF_FileName_ClimateFactor) and
        FileExists (FFileDir + DEF_FileName_Rain);

  if not FActive then
    ShowMessage(Format('� �������� - %s - ��� ������ �� ������������� �����.', [FFileDir]));

  *)

  FreeAndNil(oSList);
end;



destructor TLink_climate_file.Destroy;
begin
 // FreeAndNil(FRainIntense);
 // if FIsOpenedFiles then
    CloseFiles();

  inherited;
end;


// -------------------------------------------------------------------
function TLink_climate_file.GetFileDir: string;
// -------------------------------------------------------------------
begin
  Result:= GetApplicationDir() + 'DATA\' + DEF_CLIMATE_FOLDER_NAME;

  if not DirectoryExists(Result) then begin
    Result:= GetParentFileDir(GetApplicationDir()) + 'DATA\' + DEF_CLIMATE_FOLDER_NAME;
  end;
end;

// ---------------------------------------------------------------
procedure TLink_climate_file.CloseFiles;
// ---------------------------------------------------------------
begin
(*  FMap_Q_factor.CloseFile;
  FMap_RadioKlimRegion.CloseFile;
  FMap_Climate.CloseFile;
  FMap_ClimateFactor.CloseFile;
*)

  FreeAndNil(FMap_Q_factor);
  FreeAndNil(FMap_RadioKlimRegion);
  FreeAndNil(FMap_Climate);
  FreeAndNil(FMap_ClimateFactor);

  FreeAndNil(FMap_NIIR_1998_rain_8_11  );
  FreeAndNil(FMap_NIIR_1998_rain_8_15  );
  FreeAndNil(FMap_NIIR_1998_humidity  );

  FreeAndNil(FMap_NIIR_gradient  );



  FreeAndNil(FMap_GOST_rain_intensity  );

  FreeAndNil(FMap_ESARAIN_R001 );

 // FActive_GOST := False;

  FActive := False;

 // FIsOpenedFiles:=False;
end;


// -------------------------------------------------------------------
function TLink_climate_file.DoGetRainIntenseByPoint(aPoint: TBLPoint): Double;
// -------------------------------------------------------------------
var
    i,j: Integer;
    i_lat,j_lon: integer;
    buf, d1, d2: double;
  e_lat: Double;
  e_lon: Double;
     FTxtFile_Rain: TextFile;
begin
 // DEF_FileName_Rain             = 'ESARAIN_R001.PRN';

  AssignFile(FTxtFile_Rain, GetFileDir() + DEF_FileName_Rain);
  Reset(FTxtFile_Rain);

//  e_lat:=(90-aPoint.B)/1.5;
//  e_lon:=aPoint.L/1.5;



//  i_lat:=round((90-aPoint.B)/1.5);
//  j_lon:=round(aPoint.L/1.5);

  i_lat:=trunc((90-aPoint.B)/1.5);
  j_lon:=trunc(aPoint.L/1.5);


  {
  AssignFile(FTxtFile_Rain, GetFileDir() + DEF_FileName_Rain);
  Reset(FTxtFile_Rain);
  }

  if (i_lat in [0..120]) and (j_lon in [0..240]) then
  begin
    if i_lat>0 then for i:=0 to i_lat-1 do Readln(FTxtFile_Rain, d1);
    if j_lon>0 then for j:=0 to j_lon-1 do Read(FTxtFile_Rain, d2);

    buf:= 0;
    Read(FTxtFile_Rain, buf);
    Result:=buf;
  end
    else Result:=0;

  CloseFile(FTxtFile_Rain);

  //      CloseFile(FTxtFile_Rain);
end;


// -------------------------------------------------------------------
function TLink_climate_file.GetByBLPoint(aBLPoint: TBLPoint; var aRec:
    TLinkClimateDataRec): Boolean;
// -------------------------------------------------------------------
begin
  Result:=False;

  FillChar(aRec, SizeOf(aRec), 0);

  if not FActive then
    OpenFiles();



  aRec.ITU.rain_intensity1:=DoGetRainIntenseByPoint (aBLPoint);

  Result:=Get_ESARAIN_R001 (aBLPoint, aRec);



  aRec.ITU.steam_wet:=7.5;

  aRec.ITU.underlying_terrain_type:=1;
//  aRec.air_temperature :=15;
  aRec.atmosphere_pressure :=1013;
                


//  {$IFDEF Mapx5}
//  {$ELSE}
  // vPt := CoPoint.Create;
 // {$ENDIF}

  Result:=Get_Q_factor (aBLPoint, aRec);
  Result:=Get_Climate (aBLPoint, aRec);
  Result:=Get_ClimateFactor (aBLPoint, aRec);
  Result:=Get_RadioKlimRegion (aBLPoint, aRec);


  Result:=Get_NIIR_1998_rain_8_11 (aBLPoint, aRec);
  Result:=Get_NIIR_1998_rain_8_15 (aBLPoint, aRec);
  Result:=Get_NIIR_1998_humidity (aBLPoint, aRec);

  Result:=Get_GOST_rain (aBLPoint, aRec);

  Result:=Get_NIIR_gradient (aBLPoint, aRec);




  aRec.GOST_gradient_diel           :=arec.ITU.gradient_diel;
  aRec.GOST_gradient_deviation      :=arec.ITU.gradient_deviation;
  aRec.GOST_terrain_type            :=arec.ITU.terrain_type;
  aRec.GOST_underlying_terrain_type :=arec.ITU.underlying_terrain_type;
  aRec.GOST_steam_wet               :=arec.ITU.steam_wet;
  aRec.GOST_climate_factor          :=arec.ITU.Climate_factor;
  aRec.GOST_rain_intensity          :=arec.ITU.rain_intensity1;
  aRec.GOST_air_temperature         :=arec.air_temperature;
  aRec.GOST_atmosphere_pressure     :=arec.atmosphere_pressure;



end;

// ---------------------------------------------------------------
function TLink_climate_file.OpenFiles: Boolean;
// ---------------------------------------------------------------
var
  sFileName: string;
begin
//  if not FFileExists then
//    Exit;

  Result := ValidateFiles;

{
  sFileName:=GetFileDir() + DEF_FileName_Q_factor;

  if not FileExists (sFileName) then
  begin
   ////////// ErrDlg_FileNotExists (GetFileDir() + DEF_FileName_Q_factor);
    ShowMessage ( 'File no found: ' + sFileName);
    Exit;
  end;}



  FMap_Q_factor:=TmiMap.Create;
  FMap_RadioKlimRegion:=TmiMap.Create;
  FMap_Climate:=TmiMap.Create;
  FMap_ClimateFactor:=TmiMap.Create;

  FMap_NIIR_1998_rain_8_11 := TmiMap.Create;
  FMap_NIIR_1998_rain_8_15 := TmiMap.Create;
  FMap_NIIR_1998_humidity  := TmiMap.Create;

  FMap_NIIR_gradient := TmiMap.Create;


  FMap_GOST_rain_intensity := TmiMap.Create;

  FMap_ESARAIN_R001 := TmiMap.Create;


//
//  FMap_GOST_radioklimat.OpenFile (FFileDir + DEF_FileName_radioklimat);
  FMap_NIIR_1998_rain_8_11.OpenFile   (FFileDir + DEF_FileName_rain_8_11);
  FMap_NIIR_1998_rain_8_15.OpenFile   (FFileDir + DEF_FileName_rain_8_15);
  FMap_NIIR_1998_humidity.OpenFile    (FFileDir + DEF_FileName_humidity);

  FMap_NIIR_gradient.OpenFile    (FFileDir + DEF_FileName_NIIR_gradient);

  FMap_ESARAIN_R001.OpenFile    (FFileDir + DEF_FileName_ESARAIN_R001);

//  FIsOpenedFiles:=true;

 // FActive:=
  FMap_Q_factor.OpenFile (FFileDir + DEF_FileName_Q_factor);
  FMap_RadioKlimRegion.OpenFile (FFileDir + DEF_FileName_RadioKlimRegion);
  FMap_Climate.OpenFile (FFileDir + DEF_FileName_Climate);
  FMap_ClimateFactor.OpenFile (FFileDir + DEF_FileName_ClimateFactor);

  FMap_GOST_rain_intensity.OpenFile (FFileDir + DEF_FileName_GOST_rain_intensity);



  FActive :=FMap_Q_factor.Active and
           (FMap_RadioKlimRegion.Active )and
           (FMap_Climate.Active )and
           (FMap_ClimateFactor.Active ) and

//  FActive := FActive and
  //         FMap_GOST_humidity.Active and
   //        FMap_GOST_radioklimat.Active and
           FMap_NIIR_1998_rain_8_11.Active and
           FMap_NIIR_1998_rain_8_15.Active and
           FMap_NIIR_1998_humidity.Active and
           FMap_GOST_rain_intensity.Active and
           FMap_NIIR_gradient.Active and
           FMap_ESARAIN_R001.Active

           ;

end;



//// ---------------------------------------------------------------
function TLink_climate_file.Get_RadioKlimRegion(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec): Boolean;
//// ---------------------------------------------------------------
var
 //
  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vSelection: CMapXSelection;
  vRowValues: CMapXRowValues;
  vPt: CMapXPoint;
  iCount: integer;
  k: Integer;

  oMap : TmiMap;

//  s: string;
begin
  vPt := CoPoint.Create;

  oMap := FMap_RadioKlimRegion;

//// -------------------------------------------------------------------
//// RadioKlimRegion: string;
//// -------------------------------------------------------------------


  if oMap.Active then
  begin
    k:=oMap.VLayer.AllFeatures.Count;
  

      oMap.VLayer.Selection.SelectByPoint (aBLPoint.L, aBLPoint.B, miSelectionNew, EmptyParam);

      vSelection:=oMap.VLayer.Selection;
      iCount:=vSelection.Count;

      if iCount>0 then
        vFeature:=vSelection.Item[1];


    if iCount>0 then
    begin
      vRowValues:=oMap.VDataset.RowValues[vFeature];

       aRec.ITU.RadioClimate_Region_name:=AsString(mapx_RowValues_GetItemValue(vRowValues,'name'));
       aRec.ITU.gradient_diel     :=AsFloat(mapx_RowValues_GetItemValue(vRowValues,'gradient'));
       aRec.ITU.gradient_deviation:=AsFloat(mapx_RowValues_GetItemValue(vRowValues,'deviation'));
    end;


    Result:=iCount>0;
  end else
    Result:=False;
end;


// ---------------------------------------------------------------
function TLink_climate_file.Get_Climate(aBLPoint: TBLPoint; var aRec:  TLinkClimateDataRec): Boolean;
// ---------------------------------------------------------------
var
 //
  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vSelection: CMapXSelection;
  vRowValues: CMapXRowValues;
  vPt: CMapXPoint;
  iCount: integer;
  k: Integer;

  oMap : TmiMap;

//  s: string;
begin
  vPt := CoPoint.Create;

  oMap := FMap_Climate;


  // -------------------------------------------------------------------
  // ������ : string;
  // -------------------------------------------------------------------
 // s :=GetFileDir() + DEF_FileName_Climate;


  if oMap.Active then
  begin
    k:=oMap.VLayer.AllFeatures.Count;



      oMap.VLayer.Selection.SelectByPoint (aBLPoint.L, aBLPoint.B, miSelectionNew, EmptyParam);

      vSelection:=oMap.VLayer.Selection;
      iCount:=vSelection.Count;

      if iCount>0 then
        vFeature:=vSelection.Item[1];



    if iCount>0 then
    begin
      vRowValues:=oMap.VDataset.RowValues[vFeature];

      aRec.ITU.factor_C:=AsFloat(mapx_RowValues_GetItemValue(vRowValues,'factor_C'));

    end;


    Result:=iCount>0;
  end else
    Result:=False;
end;

// ---------------------------------------------------------------
function TLink_climate_file.Get_ClimateFactor(aBLPoint: TBLPoint; var aRec:    TLinkClimateDataRec): Boolean;
// ---------------------------------------------------------------
var
//  vPt := CoPoint.Create;

  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vSelection: CMapXSelection;
  vRowValues: CMapXRowValues;
  vPt: CMapXPoint;
  iCount: integer;

  oMap : TmiMap;

begin
  vPt := CoPoint.Create;

  oMap := FMap_ClimateFactor;


  // -------------------------------------------------------------------
  // ClimateFactor: string;
  // -------------------------------------------------------------------
  if oMap.Active then
  begin

      oMap.VLayer.Selection.SelectByPoint (aBLPoint.L, aBLPoint.B, miSelectionNew, EmptyParam);

      vSelection:=oMap.VLayer.Selection;
      iCount:=vSelection.Count;

      if iCount>0 then
        vFeature:=vSelection.Item[1];




    if iCount>0 then
    begin
      vRowValues:=oMap.VDataset.RowValues[vFeature];

      aRec.ITU.factor_B:=AsFloat(mapx_RowValues_GetItemValue(vRowValues,'factor_B'));
      aRec.ITU.factor_D:=AsFloat(mapx_RowValues_GetItemValue(vRowValues,'factor_D'));
      aRec.ITU.Climate_factor:=Round(AsFloat(mapx_RowValues_GetItemValue(vRowValues,'climate_factor')));
    end;

    Result:=iCount>0;

  end else
    Result:=False;

end;

// ---------------------------------------------------------------
function TLink_climate_file.Get_ESARAIN_R001(aBLPoint: TBLPoint; var aRec:
    TLinkClimateDataRec): Boolean;
// ---------------------------------------------------------------

var
  iCol: Integer;
  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vSelection: CMapXSelection;
  vRowValues: CMapXRowValues;
  vPt: CMapXPoint;
  iCount: integer;
  iRow: Integer;
  k: Integer;

  oMap : TmiMap;

begin
  vPt := CoPoint.Create;

  oMap := FMap_ESARAIN_R001;


   // -------------------------------------------------------------------
  // Q_factor
  // -------------------------------------------------------------------
  if oMap.Active then
  begin
    k:=oMap.VLayer.AllFeatures.Count;



      oMap.VLayer.Selection.SelectByPoint (aBLPoint.L, aBLPoint.B, miSelectionNew, EmptyParam);

      vSelection:=oMap.VLayer.Selection;
      iCount:=vSelection.Count;

      if iCount>0 then
        vFeature:=vSelection.Item[1];


    if iCount>0 then
    begin
      vRowValues:=oMap.VDataset.RowValues[vFeature];

      aRec.ITU.rain_intensity1:=   AsFloat(mapx_RowValues_GetItemValue(vRowValues,'rain'));

      iRow:=mapx_RowValues_GetItemValue(vRowValues,'Row');
      iCol:=mapx_RowValues_GetItemValue(vRowValues,'Col');

      Debug_int(iRow, 'row');
      Debug_int(iCol, 'col');

    end;

    Result:=iCount>0;
  end else
    Result:=False;

end;


// ---------------------------------------------------------------
function TLink_climate_file.Get_NIIR_1998_humidity(aBLPoint: TBLPoint; var     aRec: TLinkClimateDataRec): Boolean;
// ---------------------------------------------------------------
var
  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vSelection: CMapXSelection;
  vRowValues: CMapXRowValues;
  vPt: CMapXPoint;
  iCount: integer;
  k: Integer;

  oMap : TmiMap;

begin
  vPt := CoPoint.Create;

  oMap := FMap_NIIR_1998_humidity;


   // -------------------------------------------------------------------
  // Q_factor
  // -------------------------------------------------------------------
  if oMap.Active then
  begin
    k:=oMap.VLayer.AllFeatures.Count;



      oMap.VLayer.Selection.SelectByPoint (aBLPoint.L, aBLPoint.B, miSelectionNew, EmptyParam);

      vSelection:=oMap.VLayer.Selection;
      iCount:=vSelection.Count;

      if iCount>0 then
        vFeature:=vSelection.Item[1];


    if iCount>0 then
    begin
      vRowValues:=oMap.VDataset.RowValues[vFeature];

      aRec.NIIR_1998.absolute_humidity:=   AsFloat(mapx_RowValues_GetItemValue(vRowValues,'absolute_humidity'));
    end;

    Result:=iCount>0;
  end else
    Result:=False;

end;



// ---------------------------------------------------------------
function TLink_climate_file.Get_Q_factor(aBLPoint: TBLPoint; var aRec:   TLinkClimateDataRec): Boolean;
// ---------------------------------------------------------------
var
  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vSelection: CMapXSelection;
  vRowValues: CMapXRowValues;
  vPt: CMapXPoint;
  iCount: integer;
  k: Integer;

  oMap : TmiMap;
  
begin
  vPt := CoPoint.Create;

  oMap := FMap_Q_factor;


   // -------------------------------------------------------------------
  // Q_factor
  // -------------------------------------------------------------------
  if oMap.Active then
  begin
    k:=oMap.VLayer.AllFeatures.Count;



      oMap.VLayer.Selection.SelectByPoint (aBLPoint.L, aBLPoint.B, miSelectionNew, EmptyParam);

      vSelection:=oMap.VLayer.Selection;
      iCount:=vSelection.Count;

      if iCount>0 then
        vFeature:=vSelection.Item[1];



    if iCount>0 then
    begin
      vRowValues:=oMap.VDataset.RowValues[vFeature];


//        with aRec do
//      begin
      //1,2,3
      // ��������� �� 1
      aRec.ITU.terrain_type:=AsInteger(mapx_RowValues_GetItemValue(vRowValues,'terrain_type'));
      if aRec.ITU.terrain_type>0 then
        Dec(aRec.ITU.terrain_type);


      aRec.ITU.Q_factor    :=AsFloat  (mapx_RowValues_GetItemValue(vRowValues,'Q_factor'));
   //     end;
    end;
          
    Result:=iCount>0;
  end else
    Result:=False;

end;

// ---------------------------------------------------------------
function TLink_climate_file.Get_NIIR_1998_rain_8_11(aBLPoint: TBLPoint; var  aRec: TLinkClimateDataRec): Boolean;
// ---------------------------------------------------------------
var
 //
  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vSelection: CMapXSelection;
  vRowValues: CMapXRowValues;
  vPt: CMapXPoint;
  iCount: integer;
  k: Integer;

  oMap : TmiMap;

//  s: string;
begin
  vPt := CoPoint.Create;

  oMap := FMap_NIIR_1998_rain_8_11;

  // -------------------------------------------------------------------
  // ������ : string;
  // -------------------------------------------------------------------
 // s :=GetFileDir() + DEF_FileName_Climate;


  if oMap.Active then
  begin
    k:=oMap.VLayer.AllFeatures.Count;


      oMap.VLayer.Selection.SelectByPoint (aBLPoint.L, aBLPoint.B, miSelectionNew, EmptyParam);

      vSelection:=oMap.VLayer.Selection;
      iCount:=vSelection.Count;

      if iCount>0 then
        vFeature:=vSelection.Item[1];


    if iCount>0 then
    begin
      vRowValues:=oMap.VDataset.RowValues[vFeature];

      aRec.NIIR_1998.rain_8_11_intensity_region_number:=    AsInteger(mapx_RowValues_GetItemValue(vRowValues,'region_number'));

    end;


    Result:=iCount>0;
  end else
    Result:=False;
end;


// ---------------------------------------------------------------
function TLink_climate_file.Get_NIIR_1998_rain_8_15(aBLPoint: TBLPoint; var   aRec: TLinkClimateDataRec): Boolean;
// ---------------------------------------------------------------
var
 //
  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vSelection: CMapXSelection;
  vRowValues: CMapXRowValues;
  vPt: CMapXPoint;
  iCount: integer;
  k: Integer;

  oMap : TmiMap;

//  s: string;
begin
  vPt := CoPoint.Create;

  oMap := FMap_NIIR_1998_rain_8_15;

  // -------------------------------------------------------------------
  // ������ : string;
  // -------------------------------------------------------------------
 // s :=GetFileDir() + DEF_FileName_Climate;


  if oMap.Active then
  begin
    k:=oMap.VLayer.AllFeatures.Count;

    oMap.VLayer.Selection.SelectByPoint (aBLPoint.L, aBLPoint.B, miSelectionNew, EmptyParam);

    vSelection:=oMap.VLayer.Selection;
    iCount:=vSelection.Count;

    if iCount>0 then
      vFeature:=vSelection.Item[1];



   if iCount>0 then
    begin
      vRowValues:=oMap.VDataset.RowValues[vFeature];
                                      
      aRec.NIIR_1998.rain_8_15_Qd_region_number:=  AsInteger(mapx_RowValues_GetItemValue(vRowValues,'region_number'));

    end;

    Result:=iCount>0;
  end else
    Result:=False;
end;


// ---------------------------------------------------------------
function TLink_climate_file.Get_GOST_rain(aBLPoint: TBLPoint; var aRec:  TLinkClimateDataRec): Boolean;
// ---------------------------------------------------------------
var
 //
  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vSelection: CMapXSelection;
  vRowValues: CMapXRowValues;
  vPt: CMapXPoint;
  iCount: integer;

  oMap : TmiMap;

//  s: string;
begin
  vPt := CoPoint.Create;

  oMap := FMap_GOST_rain_intensity;

  // -------------------------------------------------------------------
  // ������ : string;
  // -------------------------------------------------------------------
 // s :=GetFileDir() + DEF_FileName_Climate;


  if oMap.Active then
  begin   
    oMap.VLayer.Selection.SelectByPoint (aBLPoint.L, aBLPoint.B, miSelectionNew, EmptyParam);

    vSelection:=oMap.VLayer.Selection;
    iCount:=vSelection.Count;

    if iCount>0 then
      vFeature:=vSelection.Item[1];


   if iCount>0 then
    begin
      vRowValues:=oMap.VDataset.RowValues[vFeature];


      aRec.GOST.rain_intensity:=  AsFloat(mapx_RowValues_GetItemValue(vRowValues,'rain_intensity_coefficient'));

    end;

    Result:=iCount>0;
  end else
    Result:=False;
end;


// ---------------------------------------------------------------
function TLink_climate_file.Get_NIIR_gradient(aBLPoint: TBLPoint; var aRec: TLinkClimateDataRec): Boolean;
// ---------------------------------------------------------------
var
  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vSelection: CMapXSelection;
  vRowValues: CMapXRowValues;
  vPt: CMapXPoint;
  iCount: integer;
  k: Integer;

  oMap : TmiMap;

begin
  vPt := CoPoint.Create;

  oMap := FMap_NIIR_gradient;


   // -------------------------------------------------------------------
  // Q_factor
  // -------------------------------------------------------------------
  if oMap.Active then
  begin
    k:=oMap.VLayer.AllFeatures.Count;


    oMap.VLayer.Selection.SelectByPoint (aBLPoint.L, aBLPoint.B, miSelectionNew, EmptyParam);

    vSelection:=oMap.VLayer.Selection;
    iCount:=vSelection.Count;


    if iCount>0 then
    begin
      vFeature:=vSelection.Item[1];

      vRowValues:=oMap.VDataset.RowValues[vFeature];

      aRec.air_temperature:=  AsFloat(mapx_RowValues_GetItemValue(vRowValues,'temperature'));


   //   ShowMessage (DEF_FileName_NIIR_gradient +' - '+ FloatToStr (aRec.air_temperature) );

    end;
          
    Result:=iCount>0;
  end else
    Result:=False;

end;




end.




