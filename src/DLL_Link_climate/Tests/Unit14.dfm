object Form14: TForm14
  Left = 894
  Top = 322
  Width = 759
  Height = 468
  Caption = 'Form14'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 24
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 24
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 1
    OnClick = Button2Click
  end
  object PageControl1: TPageControl
    Left = 284
    Top = 0
    Width = 467
    Height = 440
    ActivePage = TabSheet1
    Align = alRight
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      object ValueListEditor_NIIR: TValueListEditor
        Left = 80
        Top = 0
        Width = 379
        Height = 412
        Align = alRight
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goEditing, goRowSelect, goThumbTracking]
        Strings.Strings = (
          'sdf=sdf'
          'sdfg=sdfg'
          'sdf=sdf'
          'sdf=sdf')
        TabOrder = 0
        ColWidths = (
          166
          207)
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      object ValueListEditor2: TValueListEditor
        Left = 80
        Top = 0
        Width = 379
        Height = 406
        Align = alRight
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goEditing, goRowSelect, goThumbTracking]
        Strings.Strings = (
          'sdf=sdf'
          'sdfg=sdfg'
          'sdf=sdf'
          'sdf=sdf')
        TabOrder = 0
        ColWidths = (
          166
          207)
      end
    end
  end
  object Button3: TButton
    Left = 24
    Top = 144
    Width = 75
    Height = 25
    Caption = 'Button3'
    TabOrder = 3
    OnClick = Button3Click
  end
end
