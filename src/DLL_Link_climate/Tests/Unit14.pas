unit Unit14;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,

  u_geo,

  u_Link_climate_file,

  I_link_climate,


  StdCtrls, Grids, ValEdit, ComCtrls
  ;

type
  TForm14 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    ValueListEditor_NIIR: TValueListEditor;
    ValueListEditor2: TValueListEditor;
    Button3: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    procedure Test;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form14: TForm14;

implementation

{$R *.dfm}

procedure TForm14.FormCreate(Sender: TObject);
begin
(*

(51,664879372, 35,434980953)


  ValueListEditor1.InsertRow('a','345345',True);
//  ValueListEditor1.ItemProps['a'].HasPickList := True ;
  ValueListEditor1.ItemProps['a'].EditStyle  :=esPickList;
*)

end;


procedure TForm14.Button1Click(Sender: TObject);
const

  DEF_ARR : array[1..1] of string =
   ('11. ����� ������ �� ����� ���� (���.8.11) ��� ������������� ������'  //gradient_diel
   );

var
  I: Integer;
  v: ILink_climate_fileX;

  r: TLinkClimateDataRec ;

  bl: TBLPoint;
  s: string;
begin
  bl.B:=56.3753;
  bl.L:=62.0377;

  v:=Get_ILink_climate_fileX;

  if Assigned(v) then
  begin
  //  v.OpenFiles;
    v.OpenFiles;

   // for I := 0 to 101 do
    begin
     // if v.GetByBLPoint(bl, r) then
      //  s := r.RadioClimate_Region_name;


       ValueListEditor_NIIR.Strings.Clear;

       if v.GetByBLPoint(bl, r) then
       begin
       //  s := r.rain_8_15.Name;

     //    ValueListEditor_NIIR.InsertRow('', '',True);
//         ValueListEditor1.InsertRow('rain_8_15.TAB', '',True);
      //   ValueListEditor_NIIR.InsertRow('����� ������ �� ����� ���� (���.8.15) ��� ��������� Q�',
        //                            IntToStr(r.niiR_1998.rain_8_15_Qd.region_number), True);
//         ValueListEditor_NIIR.InsertRow('�������� ������', r.GOST.rain_8_15.Name, True);


(*         ValueListEditor_NIIR.InsertRow('humidity.TAB   ���������','',True);
         ValueListEditor_NIIR.InsertRow('-', FloatToStr(r.GOST.humidity.absolute_humidity),True);
*)
  //
 //        ValueListEditor_NIIR.InsertRow('radioklimat.TAB', '',True);

   //      ValueListEditor_NIIR.InsertRow('', IntToStr(r.GOST.radioklimat.region_number), True);
      //   ValueListEditor_NIIR.InsertRow('', r.GOST.radioklimat.name, True);

(*         ValueListEditor_NIIR.InsertRow('', FloatToStr(r.GOST.radioklimat.gradient_summer), True);
         ValueListEditor_NIIR.InsertRow('', FloatToStr(r.GOST.radioklimat.deviation_summer), True);
         ValueListEditor_NIIR.InsertRow('', FloatToStr(r.GOST.radioklimat.gradient_winter), True);
         ValueListEditor_NIIR.InsertRow('', FloatToStr(r.GOST.radioklimat.deviation_winter), True);
*)
 //        ValueListEditor_NIIR.InsertRow('', FloatToStr(r.GOST.radioklimat.gradient), True);
//         ValueListEditor_NIIR.InsertRow('', FloatToStr(r.GOST.radioklimat.deviation), True);

  //       ValueListEditor_NIIR.InsertRow('������', r.GOST.radioklimat.months, True);


  //       ValueListEditor_NIIR.InsertRow('', '',True);
 //        ValueListEditor1.InsertRow('rain_8_11.TAB', '',True);
    //     ValueListEditor_NIIR.InsertRow('����� ������ �� ����� ���� (���.8.11) ��� ������������� ������',
        //                            IntToStr(r.NiiR_1998.rain_8_11_intensity.region_number), True);



(*         ValueListEditor_NIIR.InsertRow('Aq', FloatToStr(r.GOST.rain_8_15.A), True);
         ValueListEditor_NIIR.InsertRow('Bq', FloatToStr(r.GOST.rain_8_15.B), True);
         ValueListEditor_NIIR.InsertRow('Cq', FloatToStr(r.GOST.rain_8_15.C), True);
*)

       end;


    end;

    v.CloseFiles;

  end;


  v:=nil;

end;


// ---------------------------------------------------------------
procedure TForm14.Test;
// ---------------------------------------------------------------

var
  I: Integer;
  v: TLink_climate_file ;// ILink_climate_fileX;
 // v: ILink_climate_fileX;

  r: TLinkClimateDataRec ;

  bl: TBLPoint;
  //obj: TLink_climate_file;
  s: string;
begin
//   51,664879372, 35,434980953)

  bl.B:=51.664879372;
  bl.L:=35.434980953;

//  bl.B:=90;
//  bl.L:=0;


  v:=TLink_climate_file.Create;

// v:= Get_ILink_climate_fileX;

  if Assigned(v) then
  begin
  //  v.OpenFiles;
    v.OpenFiles;


     if v.GetByBLPoint(bl, r) then
     begin

     end;


    v.CloseFiles;

  end;


  v:=nil;

end;



procedure TForm14.Button2Click(Sender: TObject);
const
  DEF_ARR : array[1..13] of string =
   ('1. ������� �������� ��������� ����.������������� [10^-8 x 1/m]',  //gradient_diel
    '2. ����������� ���������� ��������� [10^-8 x 1/m]', //gradient
    '3. ������������ �����������',  //underlying_terrain_type
    '4. ��� ���������',
    '5. ������������� ������ K�� [x10^-6]', //
    '6. Q-������ ������ �����������', //Q_factor
    '7. �������� ������������ b ��� ������� ������',
    '8. �������� ������������ c ��� ������� ������',
    '9. �������� ������������ d ��� ������� ������',
    '10. ������������� ����� � ������� 0.01% ������� [mm/h]',
    '11. ���������� ��������� �������� ���� [g/cub.m]',
    '12 ����������� ������� [��.�)',
    '13  ����������� �������� [����]'
   );



var
  I: Integer;
  v: ILink_climate_fileX;

  r: TLinkClimateDataRec ;
//  r_GOST: TdmLinkClimateConditions_GOST_Rec ;

  bl: TBLPoint;
  s: string;
begin
  bl.B:=63;
  bl.L:=120;


  v:=Get_ILink_climate_fileX;

  if Assigned(v) then
  begin
    v.OpenFiles;
  //  v.OpenFiles_GOST;

   // for I := 0 to 101 do
    begin
      if v.GetByBLPoint(bl, r) then ;
  //      s := r.RadioClimate_Region_name;


       ValueListEditor2.Strings.Clear;

    //   if v.GetByBLPoint(bl, r_GOST) then
       begin
       //  s := r_GOST.rain_8_15.Name;


(*

                      aRec.underlying_terrain_type:=FieldByName(FLD_underlying_terrain_type).AsInteger;


                      aRec.terrain_type:=FieldByName(FLD_TERRAIN_TYPE).AsInteger;
                      aRec.Q_factor    :=FieldByName(FLD_Q_FACTOR).AsFloat;

                      aRec.Factor_C:=FieldByName(FLD_Factor_C).AsFloat;

                      aRec.Climate_factor:=FieldByName(FLD_Climate_factor).AsFloat;
                      aRec.Factor_B:=FieldByName(FLD_Factor_B).AsFloat;
                      aRec.Factor_D:=FieldByName(FLD_Factor_D).AsFloat;

                      //RadioKlimRegion_rec.
                      aRec.gradient_diel:=FieldByName(FLD_gradient_diel).AsFloat;
                      aRec.gradient:=FieldByName(FLD_gradient).AsFloat;

                      aRec.rain_rate:=FieldByName(FLD_rain_rate).AsFloat;
                      aRec.steam_wet:=FieldByName(FLD_steam_wet).AsFloat;

*)

(*         ValueListEditor1.InsertRow('humidity.TAB   ���������','',True);
         ValueListEditor1.InsertRow('-', FloatToStr(r_GOST.humidity.absolute_humidity),True);


         ValueListEditor1.InsertRow('radioklimat.TAB', '',True);

         ValueListEditor1.InsertRow('', IntToStr(r_GOST.radioklimat.region_number), True);
//         ValueListEditor1.InsertRow('', r_GOST.radioklimat.name, True);

         ValueListEditor1.InsertRow('', FloatToStr(r_GOST.radioklimat.gradient_summer), True);
         ValueListEditor1.InsertRow('', FloatToStr(r_GOST.radioklimat.deviation_summer), True);
         ValueListEditor1.InsertRow('', FloatToStr(r_GOST.radioklimat.gradient_winter), True);
         ValueListEditor1.InsertRow('', FloatToStr(r_GOST.radioklimat.deviation_winter), True);
         ValueListEditor1.InsertRow('', FloatToStr(r_GOST.radioklimat.gradient), True);
         ValueListEditor1.InsertRow('', FloatToStr(r_GOST.radioklimat.deviation), True);

         ValueListEditor1.InsertRow('������', r_GOST.radioklimat.months, True);


         ValueListEditor1.InsertRow('', '',True);
 //        ValueListEditor1.InsertRow('rain_8_11.TAB', '',True);
         ValueListEditor1.InsertRow('����� ������ �� ����� ���� (���.8.11) ��� ������������� ������',
                                    IntToStr(r_GOST.rain_8_11.region_number), True);


         ValueListEditor1.InsertRow('', '',True);
//         ValueListEditor1.InsertRow('rain_8_15.TAB', '',True);
         ValueListEditor1.InsertRow('����� ������ �� ����� ���� (���.8.15) ��� ��������� Q�',
                                    IntToStr(r_GOST.rain_8_15.region_number), True);
         ValueListEditor1.InsertRow('�������� ������', r_GOST.rain_8_15.Name, True);

         ValueListEditor1.InsertRow('�������� ������������ a', FloatToStr(r_GOST.rain_8_15.param_A), True);
         ValueListEditor1.InsertRow('�������� ������������ b', FloatToStr(r_GOST.rain_8_15.param_B), True);
         ValueListEditor1.InsertRow('�������� ������������ c', FloatToStr(r_GOST.rain_8_15.param_C), True);

*)
       end;


    end;

    v.CloseFiles;

  end;


  v:=nil;

end;

procedure TForm14.Button3Click(Sender: TObject);
begin
  test;
end;


end.
