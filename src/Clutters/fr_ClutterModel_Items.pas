unit fr_ClutterModel_Items;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,

  ToolWin, ComCtrls, Db, ADODB, ActnList, Menus, cxColorComboBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, cxPropertiesStore,

  u_cx,

  dm_Main,

  dm_Onega_DB_data,

  u_Storage,

  u_const_str,
  u_const_db,

  dm_ClutterModel,

  u_func,
  u_db,
  u_img, f_Custom, cxStyles, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxTextEdit, cxSpinEdit, cxCurrencyEdit
  ;

type
  Tframe_ClutterModel_Items = class(Tfrm_Custom)
    qry_Items: TADOQuery;
    ds_Items: TDataSource;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    col__code: TcxGridDBColumn;
    col__name: TcxGridDBColumn;
    col__height: TcxGridDBColumn;
    col__loss: TcxGridDBColumn;
    col__color: TcxGridDBColumn;
    col__clutter_type_id: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_Edit: TcxStyle;
    cxStyle_ReadOnly: TcxStyle;

    procedure FormDestroy(Sender: TObject);
     procedure FormCreate(Sender: TObject);
    procedure qry_ItemsAfterPost(DataSet: TDataSet);
 

  private
    FReadOnly: Boolean;
    FID: integer;

  public
    procedure View (aID: integer);

    procedure SetReadOnly(aValue: boolean);
  end;


implementation
{$R *.dfm}


//--------------------------------------------------------------------
procedure Tframe_ClutterModel_Items.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  // -------------------------------------------------------------------

  col__Name.Caption  := STR_NAME;
  col__Height.Caption:= STR_HEIGHT;
  col__Loss.Caption  := STR_LOSS;
  col__Code.Caption  := '���';
  col__Color.Caption := STR_COLOR;

  // -------------------------------------------------------------------


 // cxGrid1DBTableView1.RestoreFromRegistry(FRegPath + cxGrid1DBTableView1.Name);

  g_Storage.RestoreFromRegistry(cxGrid1DBTableView1, ClassName);


  cxGrid1.Align:=alClient;

  SetReadOnly(True);
end;



procedure Tframe_ClutterModel_Items.FormDestroy(Sender: TObject);
begin
//  cxGrid1DBTableView1.StoreToRegistry(FRegPath + cxGrid1DBTableView1.Name);
  g_Storage.StoreToRegistry(cxGrid1DBTableView1, ClassName);

end;



//--------------------------------------------------------------------
procedure Tframe_ClutterModel_Items.View(aID: integer);
//--------------------------------------------------------------------
const
  SQL_SELECT_CLUTTERS =
    'SELECT  * FROM '+ view_Clutters + ' WHERE (clutter_model_id=:clutter_model_id)';
begin
//  Assert(aID>0);

  FID:=aID;

  dmOnega_DB_data.OpenQuery(
//  db_OpenQuery (
                qry_Items, SQL_SELECT_CLUTTERS,
               [FLD_CLUTTER_MODEL_ID, aID] );

///  db_ViewDataSet(qry_Items);

//  dmClutterModel.View_Open (aID, qry_Items);
end;


// ---------------------------------------------------------------
procedure Tframe_ClutterModel_Items.SetReadOnly(aValue: boolean);
// ---------------------------------------------------------------
begin
  FReadOnly:=aValue;

//  cxGrid1DBTableView1.OptionsData.Editing :=not aValue;


  { check : !!! }
  cx_SetGridReadOnly (cxGrid1DBTableView1, aValue);


  if aValue then
    cxGrid1DBTableView1.Styles.Background:=cxStyle_ReadOnly
  else
    cxGrid1DBTableView1.Styles.Background:=cxStyle_Edit

end;


procedure Tframe_ClutterModel_Items.qry_ItemsAfterPost(DataSet: TDataSet);
begin
  dmClutterModel.OpenData();
end;



end.



