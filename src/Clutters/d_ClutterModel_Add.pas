unit d_ClutterModel_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rxPlacemnt, cxPropertiesStore, ActnList, StdCtrls, ExtCtrls,

  d_wizard_add,

  u_const_db,
  u_const_str,

  dm_ClutterModel
  ;

type
  Tdlg_ClutterModel_add = class(Tdlg_Wizard_add)
    procedure act_OkExecute(Sender: TObject);

    procedure FormCreate(Sender: TObject);
  private
    FID,FFolderID: integer;

    procedure Append();
  public
    class function ExecDlg (aFolderID: integer): integer;

  end;

//==================================================================
implementation {$R *.DFM}
//==================================================================


//-------------------------------------------------------------------
class function Tdlg_ClutterModel_add.ExecDlg;
//-------------------------------------------------------------------
begin
  with Tdlg_ClutterModel_add.Create(Application) do
  begin
    FFolderID:=aFolderID;
    ed_Name_.Text:=dmClutterModel.GetNewName();

    ShowModal;
    Result:=FID;
    
    Free;
  end;
end;


procedure Tdlg_ClutterModel_add.FormCreate(Sender: TObject);
begin
  inherited;

  SetActionName(STR_DLG_ADD_CLU_MODEL);

  SetDefaultSize();

//  ed_Name_.Text:=STR_CLU_MODEL;

end;


procedure Tdlg_ClutterModel_add.act_OkExecute(Sender: TObject);
begin
  inherited;
  Append();
end;

//-------------------------------------------------------------------
procedure Tdlg_ClutterModel_add.Append;
//-------------------------------------------------------------------
var rec: TdmClutterModelAddRec;
begin
  rec.Name:=ed_Name_.Text;
 // rec.FolderID:=FFolderID;

  FID:=dmClutterModel.Add (rec);
end;
 


end.



