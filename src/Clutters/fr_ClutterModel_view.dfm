inherited frame_ClutterModel_view: Tframe_ClutterModel_view
  Left = 726
  Top = 291
  Width = 498
  Height = 378
  Caption = 'frame_ClutterModel_view'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 490
  end
  inherited pn_Caption: TPanel
    Width = 490
  end
  inherited pn_Main: TPanel
    Width = 490
    Height = 225
    object pn_Items: TPanel
      Left = 1
      Top = 108
      Width = 472
      Height = 116
      Align = alBottom
      BevelOuter = bvLowered
      Caption = 'pn_Items'
      TabOrder = 0
    end
    object pn_Inspector: TPanel
      Left = 1
      Top = 1
      Width = 472
      Height = 69
      Align = alTop
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelOuter = bvLowered
      Caption = 'pn_Inspector'
      TabOrder = 1
    end
    object cxSplitter1: TcxSplitter
      Left = 1
      Top = 100
      Width = 472
      Height = 8
      HotZoneClassName = 'TcxSimpleStyle'
      AlignSplitter = salBottom
      Control = pn_Items
    end
  end
  inherited MainActionList: TActionList
    object Action1: TAction
      Caption = 'Action1'
    end
    object act_Select_Color: TAction
    end
    object copy_model: TAction
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
    end
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      25
      0)
  end
end
