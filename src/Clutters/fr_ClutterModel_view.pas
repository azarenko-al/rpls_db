unit fr_ClutterModel_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, ToolWin, Menus, DBCtrls, ComCtrls, ExtCtrls,
 
  ImgList, cxControls, cxSplitter,
   
  dm_User_Security,

  fr_View_base,

  u_const_db,
  u_types,

  u_func,
  u_dlg,
  u_db,
  u_reg,

  fr_DBInspector_Container,
  fr_ClutterModel_Items,

  dm_ClutterModel, cxPropertiesStore, dxBar, cxBarEditItem, cxClasses,
  cxLookAndFeels
  ;

type
  Tframe_ClutterModel_view = class(Tframe_View_Base)
    Action1: TAction;
    act_Select_Color: TAction;
    copy_model: TAction;
    pn_Items: TPanel;
    pn_Inspector: TPanel;
    cxSplitter1: TcxSplitter;
    procedure FormCreate(Sender: TObject);

  private
    Ffrm_DBInspector: Tframe_DBInspector_Container;
    Fframe_Clutter_Items: Tframe_ClutterModel_Items;

  public
    procedure View (aID: integer; aGUID: string); override;

  end;


//====================================================================
// implementation
//====================================================================

implementation {$R *.dfm}


//--------------------------------------------------------------------
procedure Tframe_ClutterModel_view.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  pn_Inspector.Align:=alClient;

  ObjectName:=OBJ_CLUTTER_MODEL;

  CreateChildForm(Tframe_DBInspector_Container, Ffrm_DBInspector, pn_Inspector);
  Ffrm_DBInspector.PrepareViewForObject(OBJ_CLUTTER_MODEL);

  CreateChildForm(Tframe_ClutterModel_Items, Fframe_Clutter_Items, pn_Items);

 // AddControl(pn_Items, [PROP_HEIGHT]);

  AddComponentProp(pn_Items, PROP_HEIGHT);
  cxPropertiesStore.RestoreFrom;


  Fframe_Clutter_Items.SetReadOnly(False);


  pn_Inspector.Constraints.MinHeight:=20;

end;

// ---------------------------------------------------------------
procedure Tframe_ClutterModel_view.View (aID: integer; aGUID: string);
// ---------------------------------------------------------------
var
  bReadOnly: Boolean;
begin
  inherited;


  bReadOnly:=dmUser_Security.Lib_Edit_ReadOnly();


  Ffrm_DBInspector.View (aID);
  Fframe_Clutter_Items.View (aID);

  Ffrm_DBInspector.SetReadOnly(bReadOnly);
  Fframe_Clutter_Items.SetReadOnly(bReadOnly);


end;




end.

{

  bReadOnly:=dmUser_Security.Lib_Edit_ReadOnly(TableName, aID);


  Ffrm_DBInspector.View (aID);
  Ffrm_DBInspector.SetReadOnly(bReadOnly);

  SetReadOnly(bReadOnly);
