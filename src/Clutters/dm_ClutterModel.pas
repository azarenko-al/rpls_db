unit dm_ClutterModel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Db, ADODB, Variants,

  dm_Onega_DB_data,

  dm_Object_base,

  dm_Main,
  u_types,

  u_clutters,
  u_rel_Profile,
              
  u_const_DB,

  u_func,
  u_db;

type
  TdmClutterModelAddRec = record
    Name: string;
  //  FolderID: integer;
  end;


  TdmClutterModel = class(TdmObject_base)
    qry_Clutter_Items: TADOQuery;
    qry_Types: TADOQuery;
    qry_Clutter_codes11111: TADOQuery;
    qry_Temp: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

  private
//    procedure View_Open (aID: integer; aQuery: TADOQuery);
    { Private declarations }
  public
    function  Add (aRec: TdmClutterModelAddRec): integer;

//    function  Copy(aID: integer): integer;

    procedure SetItemColor (aID,aColor: integer);

    procedure AssignClutterModel (aClutterModelID: integer;
                                  aClutterModel: TrelClutterModel);
//    function GetClutterItemsDataset(aClutterModelID: integer): TDataset;

    function GetDefaultID(): integer;
    procedure OpenData;
//    procedure Restore(aiD: Integer);


  end;


function dmClutterModel: TdmClutterModel;


//====================================================================
implementation {$R *.dfm}
//====================================================================

var
  FdmClutterModel: TdmClutterModel;
                                                   

// ---------------------------------------------------------------
function dmClutterModel: TdmClutterModel;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmClutterModel) then
      FdmClutterModel := TdmClutterModel.Create(Application);

  Result := FdmClutterModel;
end;


const
//  DEF_CLUTTER_MODEL = 'default';
  SQL_SELECT_CLUTTERS =
    'SELECT  * FROM '+ view_Clutters +
    ' WHERE (clutter_model_id=:clutter_model_id)';


procedure TdmClutterModel.DataModuleDestroy(Sender: TObject);
begin
  FdmClutterModel := nil;
  inherited;
end;

//--------------------------------------------------------------------
procedure TdmClutterModel.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_CLUTTER_MODEL;
  TableName:=TBL_CLUTTERMODEL;
end;


//--------------------------------------------------------------------
function TdmClutterModel.Add (aRec: TdmClutterModelAddRec): integer;
//--------------------------------------------------------------------
begin
  with aRec do
   Result:=gl_DB.AddRecordID (TableName,
                  [
                   db_Par(FLD_NAME, Name)
               //    db_Par(FLD_FOLDER_ID, IIF_NULL(FolderID) )
                   ]);
 //  ();
  dmOnega_DB_data.ClutterModel_restore(Result);
//  Restore (Result);
end;


//--------------------------------------------------------------------
function TdmClutterModel.GetDefaultID(): integer;        //var aName: strin
//--------------------------------------------------------------------
begin
  Result:=0;

  dmOnega_DB_data.OpenQuery(qry_Temp, 'SELECT TOP 1 id  FROM '+ TBL_CLUTTERMODEL);
  if qry_Temp.RecordCount > 0 then
  begin
    Result:= qry_Temp[FLD_ID];
  end;
end;


//--------------------------------------------------------------------
procedure TdmClutterModel.SetItemColor(aID, aColor: integer);
//--------------------------------------------------------------------
begin
  gl_DB.UpdateRecord (TBL_ClutterModelType, aID,
                      [db_Par(FLD_COLOR, aColor)]);
end;

//--------------------------------------------------------------------
procedure TdmClutterModel.AssignClutterModel (aClutterModelID: integer;
                                              aClutterModel: TrelClutterModel);
//--------------------------------------------------------------------
var iID,i,iIndex,iCode,iHeight,iColor: integer;
  sName: string;
begin
  Assert (aClutterModelID <> 0);


//  iID:=dmMain.Find (TBL_CLUTTER_MODEL, aName);

//  Assert (aClutterModelID<>0);

  if not qry_Clutter_Items.Active then
    OpenData;
//  db_OpenQuery (qry_Clutter_Items, SQL_SELECT_CLUTTERS,
  //             [db_Par (FLD_CLUTTER_MODEL_ID, aClutterModelID)]);

  qry_Clutter_Items.Filtered :=True;
  qry_Clutter_Items.Filter:='clutter_model_id = '+ IntToStr(aClutterModelID);

  aClutterModel.Count:=qry_Clutter_Items.RecordCount;
  FillChar (aClutterModel.Items, SizeOf(aClutterModel.Items), 0);
 // FillChar (aClutterModel.Items, aClutterModel.Count, 0);

//  i:=0;

  qry_Clutter_Items.First;


  i:=qry_Clutter_Items.RecordCount;

  SetLength(aClutterModel.Items_new, qry_Clutter_Items.RecordCount);

//  Items_new

  with qry_Clutter_Items do
    while not EOF do
    begin
     iCode  :=FieldValues[FLD_CODE];

  //   Assert(iCode>0, 'Value <=0');
     Assert(iCode>=0, 'Value <=0');

     iHeight:=FieldByName(FLD_HEIGHT).AsInteger;
     iColor :=FieldByName(FLD_COLOR).AsInteger;
     sName  :=FieldByName(FLD_NAME).AsString;

     i:=RecNo-1;
       aClutterModel.Items_new[i].Code  :=iCode;
       aClutterModel.Items_new[i].Name  :=sName;
       aClutterModel.Items_new[i].Color :=iColor;
       aClutterModel.Items_new[i].Height:=iHeight;


       aClutterModel.Items[iCode].Code  :=iCode;
       aClutterModel.Items[iCode].Color :=iColor;
       aClutterModel.Items[iCode].Height:=iHeight;
       aClutterModel.Items[iCode].IsUsed1:=True;
       aClutterModel.Items[iCode].Name  :=sName;



     if iCode<255 then
     begin
       aClutterModel.Items[iCode].Code  :=iCode;
       aClutterModel.Items[iCode].Color :=iColor;
       aClutterModel.Items[iCode].Height:=iHeight;
       aClutterModel.Items[iCode].IsUsed1:=True;
       aClutterModel.Items[iCode].Name  :=sName;
     end;

  //   Inc(i);
     Next;
    end;


  iIndex:=0;
  for i:=0 to High(Byte) do
    if aClutterModel.Items[i].IsUsed1 then
    begin
      aClutterModel.Items[i].Index:=iIndex;
      Inc(iIndex);
    end;

 // aClutterModel.Prepare;

end;    

procedure TdmClutterModel.OpenData;
begin
  dmOnega_DB_data.OpenQuery (qry_Clutter_Items,
        'SELECT  * FROM '+ view_Clutters);

//  db_OpenQuery (qry_Clutter_Items, 'SELECT  * FROM '+ view_Clutters);
//  db_OpenQuery (qry_Clutter_codes, 'SELECT * FROM '+ TBL_CLUTTER_MODEL_TYPE);

end;
 
end.


