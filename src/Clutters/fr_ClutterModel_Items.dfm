inherited frame_ClutterModel_Items: Tframe_ClutterModel_Items
  Left = 867
  Top = 199
  Width = 558
  Height = 417
  Caption = 'frame_ClutterModel_Items'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    Width = 542
  end
  object cxGrid1: TcxGrid [1]
    Left = 0
    Top = 57
    Width = 542
    Height = 152
    Align = alTop
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = ds_Items
      DataController.Filter.MaxValueListCount = 1000
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.Indicator = True
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object col__code: TcxGridDBColumn
        DataBinding.FieldName = 'code'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Editing = False
        Options.Filtering = False
        Width = 47
      end
      object col__name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Editing = False
        Options.Filtering = False
        Width = 149
      end
      object col__height: TcxGridDBColumn
        Caption = ' [m]'
        DataBinding.FieldName = 'height'
        PropertiesClassName = 'TcxSpinEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 68
      end
      object col__loss: TcxGridDBColumn
        DataBinding.FieldName = 'loss'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DisplayFormat = '0.00;;'
        Properties.Nullable = False
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 60
      end
      object col__color: TcxGridDBColumn
        DataBinding.FieldName = 'color'
        PropertiesClassName = 'TcxColorComboBoxProperties'
        Properties.CustomColors = <>
        Properties.ShowDescriptions = False
        Options.Filtering = False
        Width = 48
      end
      object col__clutter_type_id: TcxGridDBColumn
        DataBinding.FieldName = 'clutter_type_id'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Width = 75
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qry_Items: TADOQuery
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=ONEGA'
    CursorType = ctStatic
    AfterPost = qry_ItemsAfterPost
    Parameters = <>
    SQL.Strings = (
      'SELECT  * FROM   view_Clutters')
    Left = 244
    Top = 233
  end
  object ds_Items: TDataSource
    DataSet = qry_Items
    Left = 240
    Top = 290
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 64
    Top = 240
    PixelsPerInch = 96
    object cxStyle_Edit: TcxStyle
    end
    object cxStyle_ReadOnly: TcxStyle
      AssignedValues = [svColor]
      Color = clInactiveBorder
    end
  end
end
