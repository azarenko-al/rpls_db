unit dm_Act_Clutter;

interface


uses
  Classes, Forms, Menus, ActnList,

  dm_User_Security,

  I_Audit,


  u_DataExport_run,

  dm_Onega_DB_data,

   u_const_db,

  u_const_str,

  I_Object,
  dm_act_Base,

  dm_ClutterModel,

  u_func,

  u_types,

  fr_ClutterModel_view,
  d_ClutterModel_Add;


type
  TdmAct_ClutterModel = class(TdmAct_Base)
    act_restore: TAction;
    ActionList2: TActionList;
    act_Export_MDB: TAction;
    act_Audit: TAction;
    procedure act_AuditExecute(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

    procedure DoAction(Sender: TObject);

  private
    function CheckActionsEnable: Boolean;
  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm (aOwnerForm: TForm): TForm; override;

    function ItemDel(aID: integer; aName: string): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;
  public
    class procedure Init;
  end;

var
  dmAct_ClutterModel: TdmAct_ClutterModel;

//==================================================================
// implementation
//==================================================================
implementation

uses dm_Main; {$R *.dfm}


procedure TdmAct_ClutterModel.act_AuditExecute(Sender: TObject);
begin
  inherited;
end;

//--------------------------------------------------------------------
class procedure TdmAct_ClutterModel.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_ClutterModel) then
    dmAct_ClutterModel:=TdmAct_ClutterModel.Create(Application);
end;

//--------------------------------------------------------------------
procedure TdmAct_ClutterModel.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_CLUTTER_MODEL;

  act_Restore.Caption:='Восстановить';

  act_Export_MDB.Caption:=DEF_STR_Export_MDB;

   act_Audit.Caption:=STR_Audit;



  SetActionsExecuteProc ([act_Restore,
                          act_Audit,
                          act_Export_MDB

                          ],
                         DoAction);

end;

//--------------------------------------------------------------------
procedure TdmAct_ClutterModel.DoAction (Sender: TObject);
//--------------------------------------------------------------------
begin
  if Sender=act_Audit then
//    Dlg_Audit(TBL_ANTENNATYPE, FFocusedID) else
     Init_IAuditX.ExecDlg( dmMain.GetConnectionString, TBL_ClutterModel, FFocusedID) else


  if Sender=act_Export_MDB then
    TDataExport_run.ExportToRplsDb_selected (TBL_ClutterModel, FSelectedIDList);


  if Sender=act_Restore then
    dmOnega_DB_data.ClutterModel_restore(FFocusedID);

//    dmClutterModel.Restore(FFocusedID);

end;

//--------------------------------------------------------------------
function TdmAct_ClutterModel.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_ClutterModel_add.ExecDlg (aFolderID);
end;

//--------------------------------------------------------------------
function TdmAct_ClutterModel.ItemDel(aID: integer; aName: string): boolean;
//--------------------------------------------------------------------
begin
  Result:=dmClutterModel.Del (aID);
end;


// ---------------------------------------------------------------
function TdmAct_ClutterModel.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
  Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];

  SetActionsEnabled1( [

 act_Export_MDB,
//                           act_Import_MDB,
     act_Restore,

  act_Del_,
  act_Del_list

      ],

   Result );

   //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );


end;



//--------------------------------------------------------------------
procedure TdmAct_ClutterModel.GetPopupMenu;
//--------------------------------------------------------------------
var
  bEnable: Boolean;
begin
  bEnable:=CheckActionsEnable();


  case aMenuType of
    mtFolder: begin

                AddFolderMenu_Create (aPopupMenu, bEnable);

             //   AddMenuItem (aPopupMenu, act_Add);
                AddFolderPopupMenu (aPopupMenu, bEnable);
              end;
    mtItem: begin
              AddMenuItem (aPopupMenu, act_Del_);
              AddMenuItem (aPopupMenu, act_Restore);
              AddMenuItem (aPopupMenu, nil);

              AddFolderMenu_Tools (aPopupMenu);

              AddMenuItem (aPopupMenu, act_Export_MDB);

              AddMenuItem (aPopupMenu, nil);
              AddMenuItem (aPopupMenu, act_Audit);
            end;

    mtList: begin
              AddMenuItem (aPopupMenu, act_Del_list);
              AddMenuItem (aPopupMenu, act_Export_MDB);
            end;
  end;

end;




//--------------------------------------------------------------------
function TdmAct_ClutterModel.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_ClutterModel_view.Create(aOwnerForm);
end;

begin

end.