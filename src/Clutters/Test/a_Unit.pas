unit a_Unit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,

//  f_Log,

  dm_Explorer,
  fr_Explorer,
  fr_Browser,

  fr_ClutterModel_view,

  dm_Act_View_Eng,
  I_Shell,
  dm_Act_Clutter,
  dm_Main,

  u_const_msg,
  u_const_DB,
  u_func_app,

  u_db,

  u_types, ComCtrls, StdCtrls, ToolWin, ExtCtrls, Placemnt, rxPlacemnt
  ;

type
  TForm13 = class(TForm)
    Panel1: TPanel;
    Splitter1: TSplitter;
    pc_Main: TPageControl;
    TabSheet1: TTabSheet;
    pn_Browser: TPanel;
    ToolBar1: TToolBar;
    Add: TButton;
    View: TButton;
    FormPlacement1: TFormPlacement;
    procedure AddClick(Sender: TObject);
    procedure ViewClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    Fframe_Explorer : Tframe_Explorer;
    Fframe_Browser : Tframe_Browser;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form13: TForm13;


implementation
{$R *.DFM}

var
  id: integer;


procedure TForm13.AddClick(Sender: TObject);
begin
  dmAct_Clutter.Add (0);
//  PostMsg (WM_CLUTTER_MODEL_ADD,0,0);
end;


procedure TForm13.ViewClick(Sender: TObject);
begin
  id:=gl_DB.GetMaxID (TBL_Clutter_Model);

//  dmAct_Clutter.
//  Tframe_ClutterModel_view.CreateForm (id);
end;


procedure TForm13.FormCreate(Sender: TObject);
begin

 

  Fframe_Explorer := Tframe_Explorer.CreateChildForm(TabSheet1);
  Fframe_Explorer.SetViewObjects([otClutterModel]);
  Fframe_Explorer.Load;

  Fframe_Browser := Tframe_Browser.CreateChildForm(pn_Browser);
  Fframe_Explorer.OnNodeFocused:=Fframe_Browser.ViewObjectByPIDL;

//////////  dmMain.Test;

  PostEvent (WE_PROJECT_CHANGED);

//  PostMsg (WM_SHOW_FORM_MAP);

end;


end.





