program test_Clutters;

uses
  Forms,
  dm_Act_Clutter in '..\dm_Act_Clutter.pas' {dmAct_Clutter: TDataModule},
  fr_ClutterModel_view in '..\fr_ClutterModel_view.pas' {frame_ClutterModel_view},
  fr_ClutterModel_Items in '..\fr_ClutterModel_Items.pas' {frame_ClutterModel_Items},
  d_ClutterModel_add in '..\d_ClutterModel_add.pas' {dlg_ClutterModel_add},
  a_Unit in 'a_Unit.pas' {Form13},
  dm_Main_res in '..\..\DataModules\dm_Main_res.pas' {dmMain_res: TDataModule},
  dm_Object_base in '..\..\Common\dm_Object_base.pas' {dmObject_base: TDataModule},
  dm_ClutterModel in '..\dm_ClutterModel.pas' {dmClutterModel: TDataModule};

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(TdmMain_res, dmMain_res);
  Application.Run;
end.
