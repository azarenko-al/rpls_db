unit d_Wizard_Add_new;

interface

uses
  Classes, Controls, Forms, ExtCtrls, SysUtils,
  u_cx_vgrid,

  u_cx_VGrid_manager,

//  d_Wizard_add,


  cxPropertiesStore, rxPlacemnt, ActnList, StdCtrls,
  cxVGrid, cxControls, cxInplaceContainer  ;

type

  Tdlg_Wizard_Add_new = class(TForm)
    Panel2: TPanel;
    cxVerticalGrid1: TcxVerticalGrid;
    row_Band: TcxEditorRow;
    row_Gain_dBi: TcxEditorRow;
    row_Freq_MHz: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    row_Comment1: TcxEditorRow;
    cxVerticalGrid1EditorRow4: TcxEditorRow;
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    FormStorage1: TFormStorage;
    ActionList1: TActionList;
    act_Ok: TAction;
    act_Cancel: TAction;
    cxPropertiesStore: TcxPropertiesStore;
    pn_Header: TPanel;
    lb_Action: TLabel;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  protected
    FRegPath: string;
    FVGridManager: TcxVGridManager;
    procedure SetDefaultSize;
    procedure SetDefaultWidth;
  public
  end;

(*
const
  DEF_PropertiesValue ='Properties.Value';

*)

//==================================================================
//implementation
//==================================================================
implementation {$R *.DFM}

const
  REGISTRY_COMMON_FORMS  = 'Software\Onega\Common\Forms\';


procedure Tdlg_Wizard_Add_new.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FVGridManager);
end;

//-------------------------------------------------------------------
procedure Tdlg_Wizard_Add_new.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;


  Assert(not FormStorage1.Active);

 // lb_Action.Top:=8;
 // lb_Action.Left:=5;
 // pn_Header.BorderWidth:=0;

//  pn_Header.Visible := True;

//  btn_Ok.Action:=act_Ok;

  FRegPath:= REGISTRY_COMMON_FORMS + ClassName +'\';

  Assert(not FormStorage1.Active);

  FormStorage1.IniFileName:=FRegPath + FormStorage1.name;
  FormStorage1.IniSection:='Window';
  FormStorage1.Active:=True;

(*
  Assert(not cxPropertiesStore.Active);

  cxPropertiesStore.StorageName:=FRegPath + cxPropertiesStore.Name;
  cxPropertiesStore.Active := True;

*)
(*  ts_Params.Caption:='���������';

  Panel2.Align:=alClient;

  with GSPages1 do
    if PageCount = 1 then
      BackColor:=Color;*)

  cx_InitVerticalGrid (cxVerticalGrid1);

  FVGridManager := TcxVGridManager.Create();
end;


procedure Tdlg_Wizard_Add_new.SetDefaultSize;
begin
  with Constraints do
  begin
    MaxHeight:=385;
    MinHeight:=MaxHeight;
  end;

  SetDefaultWidth();
end;


procedure Tdlg_Wizard_Add_new.SetDefaultWidth;
begin
  with Constraints do
  begin
    MaxWidth :=505;
    MinWidth :=MaxWidth;
  end;
end;


end.

