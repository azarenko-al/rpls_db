object dlg_Wizard_Add_new: Tdlg_Wizard_Add_new
  Left = 725
  Top = 370
  Width = 463
  Height = 358
  BorderWidth = 5
  Caption = 'dlg_Wizard_Add_new'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 55
    Width = 445
    Height = 184
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    TabOrder = 0
    object cxVerticalGrid1: TcxVerticalGrid
      Left = 0
      Top = 0
      Width = 445
      Height = 124
      BorderStyle = cxcbsNone
      Align = alTop
      LookAndFeel.Kind = lfStandard
      OptionsView.CellTextMaxLineCount = 3
      OptionsView.AutoScaleBands = False
      OptionsView.PaintStyle = psDelphi
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.RowHeaderMinWidth = 30
      OptionsView.RowHeaderWidth = 151
      OptionsView.ValueWidth = 49
      TabOrder = 0
      object row_Band: TcxEditorRow
        Properties.Caption = #1044#1080#1072#1087#1072#1079#1086#1085
        Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
      end
      object row_Gain_dBi: TcxEditorRow
        Expanded = False
        Properties.Caption = #1059#1089#1080#1083#1077#1085#1080#1077' [dBi]'
        Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.EditProperties.AssignedValues.DisplayFormat = True
        Properties.EditProperties.ValidateOnEnter = True
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = '45'
      end
      object row_Freq_MHz: TcxEditorRow
        Expanded = False
        Properties.Caption = #1063#1072#1089#1090#1086#1090#1072' ['#1052#1075#1094']'
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = False
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
      end
      object cxVerticalGrid1CategoryRow1: TcxCategoryRow
        Expanded = False
      end
      object row_Comment1: TcxEditorRow
        Expanded = False
        Properties.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = False
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
      end
      object cxVerticalGrid1EditorRow4: TcxEditorRow
        Properties.Caption = 'DNA'
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        Visible = False
      end
    end
  end
  object pn_Buttons: TPanel
    Left = 0
    Top = 279
    Width = 445
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 1
    DesignSize = (
      445
      35)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 445
      Height = 2
      Align = alTop
      Shape = bsTopLine
    end
    object btn_Ok: TButton
      Left = 282
      Top = 8
      Width = 75
      Height = 23
      Action = act_Ok
      Anchors = [akTop, akRight]
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object btn_Cancel: TButton
      Left = 366
      Top = 8
      Width = 75
      Height = 23
      Action = act_Cancel
      Anchors = [akTop, akRight]
      Cancel = True
      ModalResult = 2
      TabOrder = 1
    end
  end
  object pn_Header: TPanel
    Left = 0
    Top = 0
    Width = 445
    Height = 55
    Align = alTop
    BevelOuter = bvLowered
    BorderWidth = 6
    Color = clInfoBk
    Constraints.MaxHeight = 55
    Constraints.MinHeight = 55
    TabOrder = 2
    Visible = False
    object lb_Action: TLabel
      Left = 8
      Top = 8
      Width = 44
      Height = 13
      Caption = 'lb_Action'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
  end
  object FormStorage1: TFormStorage
    Active = False
    IniFileName = 'Software\Onega\'
    Options = [fpPosition]
    StoredValues = <>
    Left = 92
    Top = 4
  end
  object ActionList1: TActionList
    Left = 120
    Top = 4
    object act_Ok: TAction
      Category = 'Main'
      Caption = 'Ok'
    end
    object act_Cancel: TAction
      Category = 'Main'
      Caption = #1054#1090#1084#1077#1085#1072
    end
  end
  object cxPropertiesStore: TcxPropertiesStore
    Active = False
    Components = <>
    StorageName = 'cxPropertiesStore'
    StorageType = stRegistry
    Left = 151
    Top = 5
  end
end
