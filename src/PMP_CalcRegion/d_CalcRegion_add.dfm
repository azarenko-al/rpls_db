inherited dlg_PMP_CalcRegion_add: Tdlg_PMP_CalcRegion_add
  Left = 723
  Top = 142
  Width = 629
  Height = 539
  Caption = 'dlg_PMP_CalcRegion_add'
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 476
    Width = 621
    inherited Bevel1: TBevel
      Width = 621
    end
    inherited Panel3: TPanel
      Left = 437
      inherited btn_Ok: TButton
        Left = 458
      end
      inherited btn_Cancel: TButton
        Left = 540
      end
    end
    object cb_RegionSizeByBSRadius: TCheckBox
      Left = 9
      Top = 11
      Width = 272
      Height = 17
      Caption = #1056#1072#1089#1095#1105#1090' '#1088#1072#1079#1084#1077#1088#1072' '#1088#1072#1081#1086#1085#1072' '#1087#1086' '#1088#1072#1076#1080#1091#1089#1072#1084' '#1088#1072#1089#1095#1105#1090#1072' '#1041#1057
      TabOrder = 1
    end
  end
  inherited pn_Top_: TPanel
    Width = 621
    inherited Bevel2: TBevel
      Width = 621
    end
    inherited pn_Header: TPanel
      Width = 621
      Height = 55
    end
  end
  inherited Panel1: TPanel
    Width = 621
    inherited ed_Name_: TEdit
      Width = 611
    end
  end
  inherited Panel2: TPanel
    Width = 621
    Height = 323
    inherited PageControl1: TPageControl
      Width = 611
      Height = 230
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 603
          Height = 199
          Align = alClient
          LookAndFeel.Kind = lfUltraFlat
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 258
          OptionsView.ValueWidth = 40
          TabOrder = 0
          Version = 1
          object row_CalcModel: TcxEditorRow
            Expanded = False
            Properties.Caption = #1052#1086#1076#1077#1083#1100' '#1088#1072#1089#1095#1077#1090#1072
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = row_CalcModel1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object row_Step: TcxEditorRow
            Expanded = False
            Properties.Caption = #1064#1072#1075' '#1088#1072#1089#1095#1077#1090#1072' [m]'
            Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.AssignedValues.MaxValue = True
            Properties.EditProperties.AssignedValues.MinValue = True
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'Float'
            Properties.Value = 100.000000000000000000
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
          object row_ClutterModel: TcxEditorRow
            Expanded = False
            Properties.Caption = #1058#1080#1087' '#1084#1077#1089#1090#1085#1086#1089#1090#1080
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = row_CalcModel1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 2
            ParentID = -1
            Index = 2
            Version = 1
          end
          object cxVerticalGrid1CategoryRow1: TcxCategoryRow
            Expanded = False
            ID = 3
            ParentID = -1
            Index = 3
            Version = 1
          end
          object cxVerticalGrid1EditorRow4: TcxEditorRow
            Properties.Caption = #1062#1074#1077#1090#1086#1074#1099#1077' '#1089#1093#1077#1084#1099' '#1076#1083#1103' '#1082#1072#1088#1090
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = True
            Properties.DataBinding.ValueType = 'String'
            Properties.Options.Editing = False
            Properties.Value = Null
            ID = 4
            ParentID = -1
            Index = 4
            Version = 1
          end
          object row_KUP: TcxEditorRow
            Expanded = False
            Properties.Caption = #1059#1088#1086#1074#1085#1103' '#1087#1086#1083#1103
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = row_CalcModel1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 5
            ParentID = 4
            Index = 0
            Version = 1
          end
          object row_KMO: TcxEditorRow
            Expanded = False
            Properties.Caption = #1055#1086#1082#1088#1099#1090#1080#1103
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = row_CalcModel1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 6
            ParentID = 4
            Index = 1
            Version = 1
          end
          object cxVerticalGrid1CategoryRow2: TcxCategoryRow
            Expanded = False
            ID = 7
            ParentID = -1
            Index = 5
            Version = 1
          end
          object row_Terminal: TcxEditorRow
            Properties.Caption = #1040#1073#1086#1085#1077#1085#1090
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = row_CalcModel1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 8
            ParentID = -1
            Index = 6
            Version = 1
          end
          object row_Terminal_H: TcxEditorRow
            Expanded = False
            Properties.Caption = #1074#1099#1089#1086#1090#1072' [m]'
            Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
            Properties.EditProperties.ValueType = vtFloat
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 9
            ParentID = 8
            Index = 0
            Version = 1
          end
          object row_Terminal_power_W: TcxEditorRow
            Expanded = False
            Properties.Caption = #1084#1086#1097#1085#1086#1089#1090#1100
            Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
            Properties.EditProperties.ValueType = vtFloat
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 10
            ParentID = 8
            Index = 1
            Version = 1
          end
          object row_Terminal_sense_dBM: TcxEditorRow
            Expanded = False
            Properties.Caption = #1095#1091#1074#1089#1090#1074#1080#1090#1077#1083#1100#1085#1086#1089#1090#1100' [dB]'
            Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
            Properties.EditProperties.ValueType = vtFloat
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 11
            ParentID = 8
            Index = 2
            Version = 1
          end
          object row_Terminal_loss: TcxEditorRow
            Expanded = False
            Properties.Caption = #1087#1086#1090#1077#1088#1080' '#1074' '#1040#1060#1059
            Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
            Properties.EditProperties.ValueType = vtFloat
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 12
            ParentID = 8
            Index = 3
            Version = 1
          end
          object cxVerticalGrid1EditorRow12: TcxEditorRow
            Properties.Caption = 'Handover'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            Visible = False
            ID = 13
            ParentID = -1
            Index = 7
            Version = 1
          end
        end
      end
      object TabSheet_Cells: TTabSheet
        Caption = 'Cells'
        ImageIndex = 1
      end
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 392
    object act_GetMetrics: TAction
      Caption = #1052#1077#1090#1088#1080#1082#1072
    end
  end
  inherited FormStorage1: TFormStorage
    Left = 364
  end
end
