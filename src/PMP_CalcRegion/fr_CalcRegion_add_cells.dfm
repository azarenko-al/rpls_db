object frame_CalcRegion_add_cells: Tframe_CalcRegion_add_cells
  Left = 1247
  Top = 327
  Width = 582
  Height = 356
  Cursor = crHandPoint
  Caption = 'frame_CalcRegion_add_cells'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 117
    Width = 574
    Height = 173
    Align = alBottom
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    object cxGridDBBandedTableView1: TcxGridDBBandedTableView
      PopupMenu = PopupMenu1
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      OptionsView.BandHeaders = False
      Bands = <
        item
          Caption = #1042#1099#1073#1088#1072#1085#1085#1099#1077' '#1089#1090#1072#1085#1094#1080#1080
          Width = 191
        end>
      object cxGridDBBandedColumn4: TcxGridDBBandedColumn
        Caption = #1048#1084#1103
        DataBinding.FieldName = 'name'
        Options.Editing = False
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soDescending
        Width = 381
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object cxGridDBBandedColumn5: TcxGridDBBandedColumn
        Caption = #1053#1086#1084#1077#1088
        DataBinding.FieldName = 'number'
        Visible = False
        Options.Filtering = False
        Width = 61
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object cxGridDBBandedColumn6: TcxGridDBBandedColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'Status_str'
        Options.Editing = False
        Options.Filtering = False
        Width = 223
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object cxGridDBBandedColumn7: TcxGridDBBandedColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object cxGridDBBandedTableView1DBBandedColumn1: TcxGridDBBandedColumn
        Caption = ' '
        DataBinding.FieldName = 'checked'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Options.Filtering = False
        Width = 39
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object cxGridDBBandedTableView1DBBandedColumn2_lat: TcxGridDBBandedColumn
        DataBinding.FieldName = 'lat'
        Visible = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object cxGridDBBandedTableView1DBBandedColumn2_lon: TcxGridDBBandedColumn
        DataBinding.FieldName = 'lon'
        Visible = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object cxGridDBBandedTableView1DBBandedColumn2Status_id: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Status_id'
        Visible = False
        Options.Filtering = False
        Width = 80
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
    end
    object cxGridLevel2: TcxGridLevel
      GridView = cxGridDBBandedTableView1
    end
  end
  object pn_Criteria: TPanel
    Left = 0
    Top = 290
    Width = 574
    Height = 38
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Button9: TButton
      Left = 8
      Top = 8
      Width = 137
      Height = 25
      Action = act_Add_By_Criteria
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object combo_Status: TComboBox
      Left = 400
      Top = 10
      Width = 161
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
    end
    object combo_Criteria: TComboBox
      Left = 152
      Top = 10
      Width = 201
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      OnChange = combo_CriteriaPropertiesCloseUp
    end
    object ed_Radius_km: TEdit
      Left = 360
      Top = 10
      Width = 33
      Height = 21
      TabOrder = 3
      Text = '10'
    end
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 24
    Top = 4
    object act_Add_All: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074#1089#1077
      OnExecute = DoAction
    end
    object act_Del_All: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1089#1077
      OnExecute = DoAction
    end
    object act_Add_By_Criteria: TAction
      Caption = #1042#1099#1073#1088#1072#1090#1100' '#1087#1086' '#1082#1088#1080#1090#1077#1088#1080#1102
      OnExecute = DoAction
    end
  end
  object ADOStoredProc1: TADOStoredProc
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega'
    CursorType = ctStatic
    ProcedureName = 'sp_PMP_CalcRegion_PMP_site_select_new'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PROJECT_ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1178
      end>
    Left = 104
    Top = 8
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc1
    Left = 104
    Top = 54
  end
  object PopupMenu1: TPopupMenu
    Left = 200
    Top = 8
    object N1: TMenuItem
      Action = act_Add_All
    end
    object N2: TMenuItem
      Action = act_Del_All
    end
  end
  object qry_Poly11: TADOQuery
    Parameters = <>
    Left = 320
    Top = 8
  end
end
