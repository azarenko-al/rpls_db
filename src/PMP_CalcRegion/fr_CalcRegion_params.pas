unit fr_CalcRegion_params;

 interface

 uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   ComCtrls, ToolWin,  DBTables,
   ActnList,  RXSplit, ExtCtrls, cxControls, cxSplitter,

  //fr_Polygon,
  fr_CalcRegion_inspector,

//  fr_ClutterModel_Items,
 // fr_CalcRegion_cells,

//  u_const_str,
  u_const_DB,
//  u_types,

  u_func,
//  u_dlg,
  u_reg,

  f_Custom,

  dm_CalcRegion, cxPropertiesStore
  ;


type
  Tframe_CalcRegion_params = class(Tfrm_Custom)
    ToolBar2: TToolBar;
    pn_Inspector: TPanel;
    PageControl1: TPageControl;
    ts_Clu: TTabSheet;
    cxSplitter1: TcxSplitter;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

  private
    Fframe_inspector: Tframe_CalcRegion_inspector;


    FID: Integer;

  public
    procedure View (aRegionID: Integer);
    procedure PostEditor ();
    procedure SetReadOnly(Value: Boolean);
   end;


//==================================================================
implementation {$R *.DFM}
//==================================================================

procedure Tframe_CalcRegion_params.View (aRegionID: Integer);
begin
  Assert(aRegionID>0);


  FID:=aRegionID;
  Fframe_inspector.View (aRegionID);

 // PageControl1Change(nil);
end;



//--------------------------------------------------------------------
procedure Tframe_CalcRegion_params.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  pn_Inspector.Align:=alClient;

  CreateChildForm(Tframe_CalcRegion_inspector, Fframe_inspector, pn_Inspector);
//  Fframe_inspector:=Tframe_CalcRegion_inspector.CreateChildForm( pn_Inspector);

  AddComponentProp(pn_Inspector, PROP_HEIGHT);
  cxPropertiesStore.RestoreFrom;

 // AddControl(pn_Inspector, [PROP_HEIGHT]);
 // AddControl(PageControl1, [PROP_ACTIVE_PAGE_INDEX]);

end;

//--------------------------------------------------------------------
procedure Tframe_CalcRegion_params.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  FreeAndNil(Fframe_inspector);


  inherited;
end;



procedure Tframe_CalcRegion_params.PostEditor;
begin
  Fframe_inspector.PostEditor;
end;

procedure Tframe_CalcRegion_params.SetReadOnly(Value: Boolean);
begin
  Fframe_inspector.SetReadOnly(Value);

//  cxGrid1DBTableView1.OptionsData.Editing := not Value;
//  FReadOnly := Value;

 // ADOStoredProc1.LockType := IIF(Value, ltReadOnly, ltBatchOptimistic);
 // SetFormActionsEnabled(Self, not Value);

//  ADOStoredProc1.LockType := IIF(dmUser_Security.ProjectIsReadOnly, ltReadOnly, ltBatchOptimistic);
//  SetFormActionsEnabled(Self, not dmUser_Security.ProjectIsReadOnly);

end;

end.