unit f_CalcRegion_tool_EMP;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  rxPlacemnt, ImgList, ActnList,     ComCtrls,  Db, ADODB,
  ToolWin, ExtCtrls, cxControls, cxSplitter,
  Series, TeEngine, TeeProcs, Chart, StdCtrls,  Dialogs,

  u_CalcRegion_classes,

  dm_Onega_DB_data,

  f_map_Tool_base,

  dm_Main,
  I_MapAct,
  I_MapView,


  dm_CalcRegion,
 // dm_Site,
  dm_Pmp_Site,
  //dm_Cell,
  dm_Cell_Layer,

  u_reg,
  u_func,
  u_db,
  u_dlg,
  u_img,
  u_classes,


  u_const_DB,

  u_cx,

  u_matrix_nbest,
  u_Log,
  u_Geo,

  u_geo_convert_new,


  Grids, DBGrids,

  cxGridCustomTableView, cxGridTableView,
  cxGridCustomView, cxClasses, cxGridLevel, cxGrid, dxmdaset,
  cxGridDBTableView, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData

  ;

type

  TRegion6MaxViewRec =
  record
    RecordCount: Byte;

    Items: array[0..High(Byte)] of
      record
        Sector_ID       : integer;  // ��� ������� (��������)
        Sector_Name     : string;
        Sector_Color    : integer;

 //       Sector_Freqs_Assigned,
  //      Sector_Freqs_Fixed    : string;

        Site_Name   : string;
        SitePos     : TBLPoint;
        EMP_value   : smallint;     // ������� ���
        Distance_km : double;
        IsCoverage  : boolean; // �������� - ����,���
      end;
  end;



  Tfrm_CalcRegion_tool_EMP = class(Tfrm_Map_Tool_base)
    ActionList1: TActionList;
    act_View_Chart: TAction;
    act_Test: TAction;
    ImageList1: TImageList;
    qry_Cells: TADOQuery;
    Panel1: TPanel;
    cb_Standard: TComboBox;
  //  ser_bars: TBarSeries;
    cxSplitter1: TcxSplitter;
    DataSource1: TDataSource;


    Panel2: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Chart1: TChart;

    Series1: TFastLineSeries;
    ser_Bars: TBarSeries;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    dxMemData1: TdxMemData;
    dxMemData1station: TStringField;
    dxMemData1sector: TStringField;
    dxMemData1distance: TFloatField;
    dxMemData1EMP: TFloatField;
    dxMemData1sector_color: TIntegerField;
    DataSource2: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1RecId: TcxGridDBColumn;
    cxGrid1DBTableView1Site_Name: TcxGridDBColumn;
    cxGrid1DBTableView1Sector_Name: TcxGridDBColumn;
    cxGrid1DBTableView1distance: TcxGridDBColumn;
    cxGrid1DBTableView1EMP: TcxGridDBColumn;
    cxGrid1DBTableView1sector_color: TcxGridDBColumn;

   
    procedure cb_StandardChange(Sender: TObject);

(*
  procedure dxTreeList1CustomDraw(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      const AText: String; AFont: TFont; var AColor: TColor; ASelected,
      AFocused: Boolean; var ADone: Boolean);
*)

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cb_StandardClick(Sender: TObject);

  private
    series_Sense: TFastLineSeries;
    series_InCar: TFastLineSeries;
    series_InHouse: TFastLineSeries;

    FSectorInfoList: TSectorInfoList;

(*    FSectors : array of record
                     ID        : integer;
                     Name      : string;
                     Color     : integer;
//                     FreqsAssigned,FreqsFixed: string; // ������������ ������ �� ��������� ���������� �����
                     Site_Name : string;
                     SitePos   : TBLPoint; // station position
                   end;
*)

  //  FCalcRegionType: TdmCalcRegionType;

    // ��������� ������ �� ��������
    procedure LoadSectorInfo;
    
// TODO: IndexOfSector
//  function  IndexOfSector  (aSectorID: integer): integer;

    procedure TransformFileRecToMem (var aRec: TNBestRecArrayF; var aMemRec: TRegion6MaxViewRec);

  private
    FNBestMatrix : TMatrixNBest;

    FSensitivity_dBm: integer;
    FStandardID  : integer;
    FCalcRegionID: integer;


    procedure SetActiveRegion ();

    procedure Clear;

    procedure ShowListItem (aItemIndex: integer; // N ������
                            aSiteName,aSectorName: string;
                            aDistance_km: double;
//                            aFreqsAssigned,aFreqsFixed: string;
                            aEMP_level: integer;
                            aColor    : integer );

  protected
    procedure SetMapCursor (aBLCursor: TBLPoint); override;
  public
    procedure SetCalcRegionID (aCalcRegionID: integer);

    class procedure ShowWindow (aCalcRegionID: integer);
  end;


//====================================================================
// implementation
//====================================================================

implementation  {$R *.DFM}

resourcestring
   CAPTION_STATUS = '�����: %s';
const
//  REG_SECTION = 'emp_window\';

    SENSE_IN_CAR   = -80;
    SENSE_IN_HOUSE = -70;



// -------------------------------------------------------------------
class procedure Tfrm_CalcRegion_tool_EMP.ShowWindow (aCalcRegionID: integer);
// -------------------------------------------------------------------
var oForm: TForm;
  vIMapView: IMapViewX;
begin
  if not Assigned(IActiveMapView) then
    Exit;

  oForm:=IsFormExists (Application, Tfrm_CalcRegion_Tool_EMP.ClassName);

  if not Assigned(oForm) then
  begin
    vIMapView:=IActiveMapView; 
    oForm:=Tfrm_CalcRegion_Tool_EMP.Create(Application);
    Tfrm_CalcRegion_Tool_EMP(oForm).SetIMapView(vIMapView);
  end;

  Tfrm_CalcRegion_Tool_EMP(oForm).SetCalcRegionID (aCalcRegionID);

  oForm.Show;

{


  oForm:=IsFormExists(Tfrm_CalcRegion_Tool_Best_Server.ClassName);

  if not Assigned(oForm) then
  begin

    oForm:=Tfrm_CalcRegion_Tool_Best_Server.Create(application );
    Tfrm_CalcRegion_Tool_Best_Server(oForm).SetIMapView(vIMapView);
  end;
}

 // if oForm.WindowState = wsMinimized then
  //  oForm.WindowState := wsMaximized;

end;


//-------------------------------------------------------------------
procedure Tfrm_CalcRegion_tool_EMP.SetCalcRegionID (aCalcRegionID: integer);
//-------------------------------------------------------------------
var i: integer;
    s: string;
    oIDList: TIDList;
begin
  FCalcRegionID:= aCalcRegionID;
//  FCalcRegionType:= dmCalcRegion.GetRegionType(FCalcRegionID);

  oIDList:= TIDList.Create;

  dmCalcRegion_pmp.GetCellLayerList_used(FCalcRegionID, oIDList);

  for i:=0 to oIDList.Count-1 do
    cb_Standard.Items.AddObject (oIDList[i].Name , Pointer(oIDList[i].ID));

  oIDList.Free;


  s:=reg_ReadString(FRegPath, cb_Standard.Name, '');
  i:=cb_Standard.Items.IndexOf(s);
  if i>=0 then
    cb_Standard.ItemIndex:=i;
    
  cb_StandardChange(nil);

end;

// -------------------------------------------------------------------
procedure Tfrm_CalcRegion_tool_EMP.FormCreate(Sender: TObject);
// -------------------------------------------------------------------
begin
  inherited;

//..  cxGrid1TableView1.ClearItems;

//  dxTreeList1.ClearNodes;

  series_Sense:=TFastLineSeries.Create(self);
  series_InCar:=TFastLineSeries.Create(self);
  series_InHouse:=TFastLineSeries.Create(self);


  with series_Sense   do begin ParentChart:=Chart1; SeriesColor:=clRed; LinePen.Style:=psSolid; end;
  with series_InCar   do begin ParentChart:=Chart1; SeriesColor:=clGreen; LinePen.Style:=psSolid; end;
  with series_InHouse do begin ParentChart:=Chart1; SeriesColor:=clYellow; LinePen.Style:=psSolid; end;

  //dxTreeList1.ClearNodes;
  Chart1.Align:=alClient;


 /////// Series1.UseYOrigin:= true;
 ///////// Series1.YOrigin:= -1500;


  ser_Bars.UseYOrigin:= true;
  ser_Bars.YOrigin:= -1500;

  db_SetComponentADOConnection (Self, dmMain.ADOConnection);

 // dxTreeList1.LoadFromRegistry (FRegPath+ dxTreeList1.Name);
//  cxGrid1TableView1.RestoreFromRegistry (FRegPath+ cxGrid1TableView1.Name);

  FNBestMatrix:=TMatrixNBest.Create;


  cxGrid1.Height:=reg_ReadInteger(FRegPath, cxGrid1.Name, 100);

  FSectorInfoList := TSectorInfoList.Create();
end;


// -------------------------------------------------------------------
procedure Tfrm_CalcRegion_tool_EMP.FormDestroy(Sender: TObject);
// -------------------------------------------------------------------
begin
  FreeAndNil(FSectorInfoList);

//  cxGrid1TableView1.StoreToRegistry (FRegPath+ cxGrid1TableView1.Name);

  reg_WriteString(FRegPath, cb_Standard.Name, cb_Standard.Text);
  reg_WriteInteger(FRegPath, cxGrid1.Name, cxGrid1.Height);

  FreeAndNil(FNBestMatrix);

  inherited;;
end;




//----------------------------------------
procedure Tfrm_CalcRegion_tool_EMP.Clear;
//----------------------------------------
var i,j:integer;
begin
{  with Series1 do
    for i:=0 to Count-1 do
      XLabel [i]:='';

}
  with ser_Bars do

//  with Series1 do
    for i:=0 to Count-1 do
  begin
    YValues[i]:=-200;
    XLabel [i]:='';
  //  OffsetValues[i]:=-200;
  end;


//  with dxTreeList1 do

//  cxGrid1TableView1.ItemCount


//cxGrid1TableView1.DataControllerntroller.  [8]. //ViewData.[1].;

{  with cxGrid1TableView1 do
   for i:=0 to ItemCount-1 do
   for j:=0 to ColumnCount-1 do
    begin
      DataController.Values[i,j]:='';
     // Items[i].Data:=nil;
    end;
 }
 
end;


//----------------------------------------
procedure Tfrm_CalcRegion_tool_EMP.SetMapCursor (aBLCursor: TBLPoint);
//----------------------------------------
var i,sector_id,r,c: integer;
    distance : double;
    mem_rec : TRegion6MaxViewRec;
    rec: TNBestRecArrayF;
    xyPoint: TXYPoint;
    iZone: integer;
begin
  inherited;

  dxMemData1.Close;
  dxMemData1.Open;


  Clear();

  iZone:=FNBestMatrix.ZoneNum;
  xyPoint:=geo_Pulkovo42_BL_to_XY (aBLCursor, iZone);

  if FNBestMatrix.Active then
  begin
    xyPoint:=geo_Pulkovo42_BL_to_XY (aBLCursor,iZone);

  //  if FNBestMatrix.FindCellXY (xyPoint, r,c) then
  //    StatusBar1.SimpleText:= Format('r=%d; c=%d', [r,c]);

  end;




  if FNBestMatrix.Active then
    if FNBestMatrix.FindBL (aBLCursor, iZone, rec) then
  begin // NOT UNIQUE ID !!!!!!!!!!!!!!!
    TransformFileRecToMem (rec, mem_rec);

    for i:=0 to mem_rec.RecordCount-1 do // REGION_INFO_SECTOR_MAX_COUNT-1 do
    begin
      sector_id:=mem_rec.Items[i].Sector_ID;
      if sector_id<=0 then Break;

      // ���������� �� �� ������� �� ����� (�������)
      distance:= geo_Distance_m (mem_rec.Items[i].SitePos, aBLCursor);
      distance:=distance / 1000;

      if mem_rec.Items[i].IsCoverage then
         ShowListItem (i,
                       mem_rec.Items[i].Site_Name,
                       mem_rec.Items[i].Sector_Name,
                       distance,

//                       mem_rec.Items[i].Sector_Freqs_Assigned,
 //                      mem_rec.Items[i].Sector_Freqs_Fixed,

                       mem_rec.Items[i].EMP_value,
                       mem_rec.Items[i].Sector_Color);

    end;

  end;
  // else
   // Clear();


end;


//----------------------------------------
procedure Tfrm_CalcRegion_tool_EMP.SetActiveRegion ();
//----------------------------------------
      //-------------------------------------------------------
      procedure DoInitChart();
      //-------------------------------------------------------
      var i: integer;
      begin
        // add REGION_INFO_SECTOR_MAX_COUNT items to chart

        ser_Bars.Clear;
//        Series1.Clear;
        series_Sense.Clear;
        series_InCar.Clear;
        series_InHouse.Clear;

        for i:=0 to FNBestMatrix.MaxRecordCount-1 do
        begin

          ser_Bars.Add (-150,'');
//          Series1.Add (-150,'');

          series_Sense.Add (FSensitivity_dBm);
          series_InCar.Add (sense_in_car);
          series_InHouse.Add (sense_in_house);
        end;
      end;
      //-------------------------------------------------------

var
  sFileName: string;
  i,iStep: integer;
  
begin
//  FNoiseLevel:= gl_DB.GetFieldValueByID(TBL_CALCREGION, FLD_LEVEL_STORED_NOISE, FCalcRegionID);
  FSensitivity_dBm:= gl_DB.GetIntFieldValueByID(TBL_PMP_CALCREGION, FLD_Terminal_Sensitivity, FCalcRegionID); //  iSens:= ; ///////FRegion.Abonent.Sensitivity;

///////////  FFreqPlan:=FRegion.FreqPlans.ActiveFreqPlan;
  iStep:= dmCalcRegion_pmp.GetIntFieldValue(FCalcRegionID, FLD_STEP);

  StatusBar1.SimpleText:=
    Format(CAPTION_STATUS, [dmCalcRegion_pmp.GetNameByID(FCalcRegionID), iStep]);

  // advanced information about each point
  sFileName:=dmCalcRegion_pmp.GetFileName(FCalcRegionID, FStandardID, ftNBEST);
//   FRegion.GetFileName (ftNBest, FStandardID); // load KUP files


  LoadSectorInfo; //(FScenario.Sites);

  if not FNBestMatrix.OpenFile (sFileName) then
  begin
    g_Log.Add ( 'Tfrm_CalcRegion_tool_EMP: �� ������� ������� ������� nbest');
    ShowMessage('�� ������� ������� ������� N ��������');
  end;

 // FNBestMatrix.Sensitivity:=FSensitivity_dBm;//FRegion.Abonent.Sensitivity;

//  cxGrid1TableView1.ClearItems;

//  for i:=0 to FNBestMatrix.MaxRecordCount-1 do
//    cxGrid1TableView1.DataController.Append;//  aAdd;

  // ������� ���-�� ����� � ������� - ������� ��������
  DoInitChart();


  Chart1.LeftAxis.Automatic:=false;
  Chart1.LeftAxis.Minimum:=-120; //FNBestMatrix.Summary.MinEMP;
  Chart1.LeftAxis.Maximum:=-30; //FNBestMatrix.Summary.MaxEMP;
end;


//-------------------------------------------------------
procedure Tfrm_CalcRegion_tool_EMP.ShowListItem(aItemIndex: integer; // N ������
                                                aSiteName,aSectorName: string;
                                                aDistance_km: double;
                                               // aFreqsAssigned,aFreqsFixed: string;
                                                aEMP_level: integer;
                                                aColor    : integer
                                                );
//-------------------------------------------------------
begin

      db_AddRecord_(dxMemData1,
      [
        'Site_Name',    aSiteName,
        'Sector_Name',  aSectorName,
        'distance',     aDistance_km,
        'EMP',          aEMP_level,
        'Sector_Color', aColor

      ]);





{
  if cxGrid1TableView1.DataController.RecordCount = 0 then
    exit;

//  with cxGrid1TableView1.DataController Items[aItemIndex] do
  with cxGrid1TableView1.DataController do
  begin
    Values [aItemIndex, col_Station.Index]     := aSiteName;
    Values [aItemIndex, col_Sector.Index]      := aSectorName;
    Values [aItemIndex, col_Distance.Index]    := Format('%1.2f ', [aDistance_km]);

//    Values [col_Freqs_Assigned.Index] := aFreqsAssigned;
 //   Values [col_Freqs_Fixed.Index]    := aFreqsFixed;

    Values [aItemIndex, col_EMP.Index]         := Format('%d ', [aEMP_level]);;
    Values [aItemIndex, col_Sector_Color.Index]:= aColor;
   // Data:=Pointer(1);
  end;
  }

{
  // Chart -----------------
  Series1.XLabel[aItemIndex]      :=Format('%1.2f km', [aDistance]);
//  ser_Bars.OffsetValues[aItemIndex]:=aEMP_level;
  Series1.YValues[aItemIndex]     :=aEMP_level;
  Series1.ValueColor[aItemIndex]  :=aColor;
}

  ser_Bars.XLabel[aItemIndex]      :=Format('%1.2f km', [aDistance_km]);
//  ser_Bars.OffsetValues[aItemIndex]:=aEMP_level;
  ser_Bars.YValues[aItemIndex]     :=aEMP_level;
  ser_Bars.ValueColor[aItemIndex]  :=aColor;


end;



(*

//-------------------------------------------------------
procedure Tfrm_CalcRegion_tool_EMP.dxTreeList1CustomDraw(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn; const AText: String; AFont: TFont;
  var AColor: TColor; ASelected, AFocused: Boolean; var ADone: Boolean);
//-------------------------------------------------------
begin
  if Assigned(aNode.Data) then
  begin
    if (not AFocused) and (aNode.Index mod 2=0) then
      AColor:=clInfoBk;

    if Assigned(aNode.Data) and (AColumn=col_Sector_Color) then
    begin
       if aNode.Values[col_Sector_Color.Index]='' then Exit;

       img_DrawColorRect_OnCanvas (ACanvas, aRect, aColor, aNode.Values[col_Sector_Color.Index], 4);
       ADone:=true;
    end;
  end;
end;
*)


//-------------------------------------------------------
procedure Tfrm_CalcRegion_tool_EMP.cb_StandardClick(Sender: TObject);
//-------------------------------------------------------
begin
  with cb_Standard do
    FStandardID:=Integer(Items.Objects[ItemIndex]);

  SetActiveRegion ();
end;

// TODO: IndexOfSector
////------------------------------------------------------
//function Tfrm_CalcRegion_tool_EMP.IndexOfSector (aSectorID: integer): integer;
////------------------------------------------------------
//var i: integer;
//begin
//for i:=0 to High(FSectors) do
//  if FSectors[i].ID=aSectorID then begin Result:=i; Exit; end;
//Result:=-1;
//end;

//----------------------------------------
procedure Tfrm_CalcRegion_tool_EMP.LoadSectorInfo; //(aSiteList: TSiteList);
//----------------------------------------
const

(*  SQL_TOOL_EMP_SELECT_GSM_CELL_INFO =
    'SELECT DISTINCT'+
    '  Site.name AS site_name, Property.Address, Property.lat, Property.lon, Cell.id AS cell_id, Cell.CellID,'+
    '  Cell.name AS cell_name, Cell.color AS cell_color, CalcRegionCells.calc_region_id'+
    ' FROM  CalcRegionCells LEFT OUTER JOIN'+
    '       Site ON CalcRegionCells.site_id = Site.id LEFT OUTER JOIN'+
    '       Property ON Site.property_id = Property.id RIGHT OUTER JOIN'+
    '       Cell ON CalcRegionCells.cell_id = Cell.id'+
    ' WHERE     (CalcRegionCells.calc_region_id = :calc_region_id)';
*)


  SQL_TOOL_EMP_SELECT_PMP_CELL_INFO =
    'SELECT * ' +
//    'SELECT DISTINCT  lat, lon, cell_id,  cell_name, color, site_name' +
    ' FROM  '+VIEW_PMP_CALCREGION_PMP_SECTORS+
    ' WHERE     (calc_region_id = :calc_region_id)';


var k: integer;
begin

  CursorHourGlass;

//  case FCalcRegionType of
// //   crtGSM: db_OpenQuery(qry_Cells, SQL_TOOL_EMP_SELECT_GSM_CELL_INFO, [db_Par(FLD_CALC_REGION_ID, FCalcRegionID)]);
//    crtPMP:
//    db_OpenQuery(qry_Cells, SQL_TOOL_EMP_SELECT_PMP_CELL_INFO, [db_Par(FLD_CALC_REGION_ID, FCalcRegionID)]);
//  end;

  dmOnega_DB_data.OpenQuery(qry_Cells,
    'SELECT * ' +
    ' FROM  '+ VIEW_PMP_CALCREGION_PMP_SECTORS+
    ' WHERE     (pmp_calc_region_id = :id)',
     [FLD_ID, FCalcRegionID]);

  FSectorInfoList.Clear;

  FSectorInfoList.LoadFromDatabase(qry_Cells);

  k:=FSectorInfoList.Count;

{
  SetLength(FSectors, qry_Cells.RecordCount);
  k:=0;

  with qry_Cells do
    while not EOF do
  begin
    oSectorInfo:=FSectorInfoList.AddItem;


    FSectors[k].ID        := FieldByName(FLD_CELL_ID).AsInteger;
    FSectors[k].Name      := FieldByName(FLD_CELL_NAME).AsString;
    FSectors[k].Color     := FieldByName(FLD_COLOR).AsInteger;
    FSectors[k].Site_Name := FieldByName(FLD_SITE_NAME).AsString;
    FSectors[k].SitePos.B := FieldByName(FLD_LAT).AsFloat;
    FSectors[k].SitePos.L := FieldByName(FLD_LON).AsFloat;

    Inc(k);
    Next;
  end;
 }

  CursorDefault;

end;

//------------------------------------------------------
procedure Tfrm_CalcRegion_tool_EMP.TransformFileRecToMem(var aRec: TNBestRecArrayF;
                                  var aMemRec:  TRegion6MaxViewRec);
//------------------------------------------------------
var
  i,iSectorID,ind: integer;

  oSectorInfo: TSectorInfo;
begin
//  FillChar (aMemRec, SizeOf(aMemRec), 0);

//  iRecSize:=FNBestMatrix.MaxRecordRowCount;

  aMemRec.RecordCount:=aRec.RecordCount;

  for i:=0 to aRec.RecordCount-1 do
  begin
    iSectorID:=aRec.Items[i].ID;

    oSectorInfo := FSectorInfoList.FindByID(iSectorID);

  //  ind:=IndexOfSector (iSectorID);
  //  if ind>=0 then

    if Assigned(oSectorInfo) then
    begin
      aMemRec.Items[i].Sector_ID      := oSectorInfo.SectorID;
      aMemRec.Items[i].Sector_Name    := oSectorInfo.SectorName;
      aMemRec.Items[i].Sector_Color   := oSectorInfo.Color;

      aMemRec.Items[i].Site_Name      := oSectorInfo.SiteName;
      aMemRec.Items[i].SitePos        := oSectorInfo.SitePos;
      aMemRec.Items[i].EMP_value      := -aRec.Items[i].Value;
      aMemRec.Items[i].IsCoverage     := true;
                       //(-aFileRec.Items[i].EMP_value >= FSensitivity_dBm);

//      aMemRec.Items[i].Sector_Freqs_Assigned := FSectors[ind].FreqsAssigned;
 //     aMemRec.Items[i].Sector_Freqs_Fixed    := FSectors[ind].FreqsFixed;


    end else
      aMemRec.Items[i].Sector_ID      := 0;

  end;
end;



procedure Tfrm_CalcRegion_tool_EMP.cb_StandardChange(Sender: TObject);
begin
  with cb_Standard do
    if ItemIndex>=0 then
    begin
      FStandardID:=Integer(Items.Objects[ItemIndex]);
      SetActiveRegion ();
    end;

end;

end.



               {
  // �������� ������� �� ��������� ���
  with FNBestMatrix do
  for i:=0 to High(FSectors) do
  begin
    ind:=0;//////////////FreqPlan.Sectors.IndexByID (FSectors[i].ID);
    if ind<0 then Continue;

   //////////////// frSector:=FFreqPlan.Sectors.Items[ind];
//    ByID [FSectors[i].ID];

(*    if Assigned(frSector) then begin
      FSectors[i].FreqsAssigned:=IntArrayToString(frSector.FreqDistributedArr);
      FSectors[i].FreqsFixed   :=IntArrayToString(frSector.FreqFixedArr);
    end;
*)  end;      }




     {


//-------------------------------------------------------
procedure Tfrm_CalcRegion_tool_EMP.dxTreeList1CustomDraw(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn; const AText: String; AFont: TFont;
  var AColor: TColor; ASelected, AFocused: Boolean; var ADone: Boolean);
//-------------------------------------------------------
begin
  if Assigned(aNode.Data) then
  begin
    if (not AFocused) and (aNode.Index mod 2=0) then
      AColor:=clInfoBk;

    if Assigned(aNode.Data) and (AColumn=col_Sector_Color) then
    begin
       if aNode.Values[col_Sector_Color.Index]='' then Exit;

       img_DrawColorRect_OnCanvas (ACanvas, aRect, aColor, aNode.Values[col_Sector_Color.Index], 4);
       ADone:=true;
    end;
  end;
end;
