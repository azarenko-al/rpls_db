unit f_CalcRegion_tool_Best_Server;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, rxPlacemnt,  ToolWin, AdoDb, Db,    

  dm_Onega_DB_data,

  dm_Main,

  I_MapAct,
  I_MapView,

  f_map_Tool_base,

  u_Log,

  u_Geo,
  u_const_db,

  dm_CalcRegion,

  u_CalcRegion_classes,

  u_Matrix,
  u_func,
  u_db,
  u_classes
  ;

type


  Tfrm_CalcRegion_tool_Best_Server = class(Tfrm_Map_Tool_base)
    ListView1: TListView;
    bar_Region: TStatusBar;
    Panel1: TPanel;
    qry_Cells: TADOQuery;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FStandardNameList : TStringList;
    FSectorInfoList  : TSectorInfoList;

    FkgrMatrixList : TkgrMatrixList;
    FkupMatrixList : TkupMatrixList;

    FCalcRegionID: integer;
   // FCalcRegionType: TdmCalcRegionType;

    procedure SetActiveRegion ();
    procedure SetCalcRegion(aCalcRegionID: Integer);

  protected
    procedure SetMapCursor (aBLCursor: TBLPoint); override;

  public
    class procedure ShowWindow (aCalcRegionID: integer);

 //   constructor CreateForm (aCalcRegionID: integer);

  end;



//====================================================================
// implementation
//====================================================================


implementation {$R *.DFM}

const
  COLOR_BEST_SERVER_LINE = clLime;



//---------------------------------------------------
class procedure Tfrm_CalcRegion_tool_Best_Server.ShowWindow (aCalcRegionID: integer);
//---------------------------------------------------
var oForm: TForm;
  vIMapView: IMapViewX;
begin
  if not Assigned(IActiveMapView) then
    Exit;


  oForm:=IsFormExists(Application, Tfrm_CalcRegion_Tool_Best_Server.ClassName);

  if not Assigned(oForm) then
  begin
    vIMapView:=IActiveMapView;

    oForm:=Tfrm_CalcRegion_Tool_Best_Server.Create(application );
    Tfrm_CalcRegion_Tool_Best_Server(oForm).SetIMapView(vIMapView);
  end;


  Tfrm_CalcRegion_Tool_Best_Server(oForm).SetCalcRegion(aCalcRegionID);

  oForm.Show;


 // if oForm.WindowState = wsMinimized then
  //  oForm.WindowState := wsMaximized;

end;



//-------------------------------------------------------------------
procedure Tfrm_CalcRegion_tool_Best_Server.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

 // Name:='BestServer';
  Height := 140;

  ListView1.Align:=alClient;

  FkgrMatrixList:=TkgrMatrixList.Create;
  FkupMatrixList:=TkupMatrixList.Create;

  FStandardNameList:=TStringList.Create;
  FSectorInfoList:= TSectorInfoList.Create;

  qry_Cells.Connection:= dmMain.AdoConnection;

end;


//----------------------------------------------------
procedure Tfrm_CalcRegion_tool_Best_Server.FormDestroy(Sender: TObject);
//----------------------------------------------------
begin
  FreeAndNil(FkgrMatrixList);
  FreeAndNil(FkupMatrixList);

  FreeAndNil(FSectorInfoList);
  FreeAndNil(FStandardNameList);

  inherited;
end;



//----------------------------------------------------
procedure Tfrm_CalcRegion_tool_Best_Server.SetMapCursor (aBLCursor: TBLPoint);
//----------------------------------------------------
var iValue,ind,i,iSectorID: integer;
    fDistance: double;
    oSectorInfo: TSectorInfo;             
    oListItem: TListItem;
    bIsFound: boolean;
begin
 //  Assert(Assigned(IActiveMapView));

  //�������� �����

  ListView1.Items.Clear;

  // �����, ����� ��8���� ������� � �����
  bIsFound:= false;



  for i:=0 to FkgrMatrixList.Count-1 do
    if (FkgrMatrixList[i].FindPointBL (aBLCursor, iSectorID)) then
  begin
    bIsFound:= true;

    oSectorInfo:=FSectorInfoList.FindByID (iSectorID);



    if Assigned(oSectorInfo) then
      with oSectorInfo do
    begin
     FIMapView.MAP_ADD_USER_LINE(clGreen, 1, SitePos, aBLCursor);


      fDistance:=geo_Distance_m (SitePos, aBLCursor)/1000;

      oListItem:=ListView1.Items.Add;

//exit;


      oListItem.Caption:=FStandardNameList[i];
      oListItem.SubItems.Add (Format('%s : %s', [SiteName, SiteAddress]));
      oListItem.SubItems.Add (Format('%s : %d', [SectorName, CellID]));
      oListItem.SubItems.Add (Format('%1.1f',   [fDistance]));



      if FkupMatrixList[i].FindPointBL (aBLCursor, iValue) then
      begin
        oListItem.SubItems.Add (Format('%d',[iValue] ));

        FIMapView.MAP_ADD_USER_TEXT(aBLCursor, Format('%d dBm', [iValue]));

     end;
    end;
  end;

  if not bIsFound then
      FIMapView.MAP_CLEAR_USER_DRAWINGS;

//////////
  FIMapView.UserLayerDraw;


end;

//-----------------------------------------------
procedure Tfrm_CalcRegion_tool_Best_Server.SetActiveRegion();
//-----------------------------------------------
//const
  (*SQL_TOOL_EMP_SELECT_GSM_CELL_INFO =
    'SELECT DISTINCT'+
    '  Site.name AS site_name, Property.Address, Property.lat, Property.lon, Cell.id AS cell_id, Cell.CellID,'+
    '  Cell.name AS cell_name, Cell.color AS cell_color, CalcRegionCells.calc_region_id'+
    ' FROM  CalcRegionCells LEFT OUTER JOIN'+
    '       Site ON CalcRegionCells.site_id = Site.id LEFT OUTER JOIN'+
    '       Property ON Site.property_id = Property.id RIGHT OUTER JOIN'+
    '       Cell ON CalcRegionCells.cell_id = Cell.id'+
    ' WHERE     (CalcRegionCells.calc_region_id = :calc_region_id)';
*)

(*
  SQL_TOOL_EMP_SELECT_PMP_CELL_INFO =
    'SELECT DISTINCT  lat, lon, cell_id, cell_name, color, site_name' +

//    'SELECT DISTINCT  lat, lon, cell_id, cell_name, color, site_name' +
    ' FROM  '+ VIEW_PMP_CALCREGION_PMP_SECTORS +
    ' WHERE     (calc_region_id = :calc_region_id)';
*)


var
  kgr_fname,kup_fname: string;
  i,j,k,iCount: integer;
  gX,gY: double;

  oSectorInfo: TSectorInfo;
  oCellLayersIDList: TIDList;
  s: string;
  sCalcRegion_Name: string;
  sName: string;
 //   oQry: TAdoQuery;
begin
  oCellLayersIDList:= TIDList.Create;

  FSectorInfoList.Clear;
  FStandardNameList.Clear;


  CursorHourGlass;

//  case FCalcRegionType of
//  //  crtGSM: db_OpenQuery(qry_Cells, SQL_TOOL_EMP_SELECT_GSM_CELL_INFO, [db_Par(FLD_CALC_REGION_ID, FCalcRegionID)]);
//    crtPMP:
//            db_OpenQuery(qry_Cells,
//               SQL_TOOL_EMP_SELECT_PMP_CELL_INFO, [db_Par(FLD_CALC_REGION_ID, FCalcRegionID)]);
//  end;


  db_OpenQuery(qry_Cells,
        'SELECT *  FROM  '+ VIEW_PMP_CALCREGION_PMP_SECTORS +
        ' WHERE  (pmp_calc_region_id = :id)',
         [db_Par(FLD_ID, FCalcRegionID)]);


  FSectorInfoList.LoadFromDatabase(qry_Cells);


  dmCalcRegion_pmp.GetCellLayerList_used (FCalcRegionID, oCellLayersIDList);

  sCalcRegion_Name:=dmCalcRegion_pmp.GetNameByID(FCalcRegionID);

  if (oCellLayersIDList.Count = 0) then
    g_Log.Add ( '����� �������'+ sCalcRegion_Name+ '. ��� ��������� ����');


  bar_Region.Panels[1].Text:=Format('%s', [sCalcRegion_Name]);

  for k:=0 to oCellLayersIDList.Count-1 do
  begin
    kgr_fname:=dmCalcRegion_pmp.GetFileName (FCalcRegionID, oCellLayersIDList[k].ID, ftKGR);
    kup_fname:=dmCalcRegion_pmp.GetFileName (FCalcRegionID, oCellLayersIDList[k].ID, ftKUP);

    if not FkgrMatrixList.AddMatrix (kgr_fname) then
    begin
      g_Log.Add ( '����� �������' + sCalcRegion_Name+ '. �� ������� KGR �������');
      Continue;
    end;

    if not FkupMatrixList.AddMatrix (kup_fname, 0, 0) then
    begin
      g_Log.Add ( '����� �������' + sCalcRegion_Name+'. �� ������� KUP �������');
      Continue;
    end;

//        cb_Standard.Items.AddObject (oIDList[i].Name , Pointer(oIDList[i].ID));

    s:=oCellLayersIDList[k].Name;
//    s:=dmCellLayer.GetNameByID(oCellLayersIDList[k].ID);

    FStandardNameList.Add (s);
//    FStandardNameList.Add (dmCellLayer.GetNameByID(oCellLayersIDList[k].ID));
  end;

  CursorDefault;

  oCellLayersIDList.Free;
end;


procedure Tfrm_CalcRegion_tool_Best_Server.SetCalcRegion(aCalcRegionID: Integer);
begin
  FCalcRegionID:=   aCalcRegionID;
//  FCalcRegionType:= dmCalcRegion.GetRegionType(FCalcRegionID);

  SetActiveRegion();

end;


end.



(*
//----------------------------------------------------
function TSectorInfoList.GetItem (Index: integer): TSectorInfo;
//----------------------------------------------------
begin
  Result:=TSectorInfo (inherited Items[Index]);
end;


//----------------------------------------------------
function TSectorInfoList.FindByID (aID: integer): TSectorInfo;
//----------------------------------------------------
var i: integer;
begin
  for i:=0 to Count-1 do
    if Items[i].SectorID = aID then begin Result:=Items[i]; Exit; end;
  Result:=nil;
end;

*)


{
//-----------------------------------------------
constructor Tfrm_CalcRegion_tool_Best_Server.CreateForm (aCalcRegionID: integer);
//-----------------------------------------------
begin
  inherited Create(Application);


  FCalcRegionID:=   aCalcRegionID;
  FCalcRegionType:= dmCalcRegion.GetRegionType(FCalcRegionID);

  SetActiveRegion();

end;}


  //dm_Site,
//  dm_Pmp_Site,
  //dm_Cell,
 // dm_Cell_Layer,

 

(*  TSectorInfo = class
  public
    SectorID,
    CellID      : integer;  // ����� �������
    SectorName,
    SiteName,
    SiteAddress : string;
    SitePos     : TBLPoint;
  end;

  TSectorInfoList = class(TList)
  public
    function GetItem(Index: integer): TSectorInfo;
    function FindByID (aID: integer): TSectorInfo;
    property Items  [Index: integer]: TSectorInfo read GetItem;
  end;

*)


(*
  with qry_Cells do
    while not EOF do
  begin
    oSectorInfo:=FSectorInfoList.AddItem;

//    oSectorInfo:= TSectorInfo.Create;

    oSectorInfo.SectorName  := FieldByName(FLD_CELL_NAME).AsString;
    oSectorInfo.SectorID    := FieldByName(FLD_PMP_SECTOR_ID).AsInteger;
//    oSectorInfo.CellID      := FieldByName(FLD_CELLID).AsInteger;
    oSectorInfo.SiteName    := FieldByName(FLD_PMP_SITE_NAME).AsString;
//    oSectorInfo.SiteAddress := FieldByName(FLD_ADDRESS).AsString;
    oSectorInfo.SitePos.B   := FieldByName(FLD_LAT).AsFloat;
    oSectorInfo.SitePos.L   := FieldByName(FLD_LON).AsFloat;

  //  FSectorInfoList.Add (oSectorInfo);

  //////  Inc(k);
    Next;
  end;
*)