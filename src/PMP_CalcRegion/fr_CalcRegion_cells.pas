unit fr_CalcRegion_cells;

 interface

 uses
  ActnList, Forms, cxTLData, cxPropertiesStore,   Variants, cxColorComboBox,
  ADODB, DB, Classes, Menus, cxInplaceContainer, cxTL, cxDBTL, SysUtils,
  Controls, cxControls, ComCtrls, ToolWin, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, cxCustomData, cxStyles, cxTextEdit, cxCheckBox,


  
//  dm_Act_Explorer, //

  d_CalcRegion_add_sites,

  dm_Onega_DB_data,

//  f_Custom,

  I_Shell,
  u_Shell_new,
  
  dm_Main,

  u_db_ini,

  u_Geo,
  u_db,
  u_cx,
    
  u_Func_arrays,  
  u_func,
  u_dlg,
  u_img,

  u_const,
  u_const_str,
  u_const_db,

  u_classes,

  dm_CalcRegion,

  u_types,

  cxButtonEdit, cxMaskEdit, cxTLdxBarBuiltInMenu, dxSkinsCore,
  dxSkinsDefaultPainters
  ;



type
  Tframe_CalcRegion_cells = class(TForm)
    PopupMenu1: TPopupMenu;
    ActionList1: TActionList;
    act_Add: TAction;
    act_Del: TAction;
    Add1: TMenuItem;
    Del1: TMenuItem;
    act_UnCheck: TAction;
    act_Check: TAction;
    N3: TMenuItem;
    Check1: TMenuItem;
    UnCheck1: TMenuItem;
    StatusBar1: TStatusBar;
    act_Grid_Setup: TAction;
    act_Clear_CalcModel: TAction;
    N4: TMenuItem;
    actClearCalcModel1: TMenuItem;
    act_CheckByStatus: TAction;
    col__name1: TcxDBTreeListColumn;
    col__Number: TcxDBTreeListColumn;
    col__type: TcxDBTreeListColumn;
    col__checked: TcxDBTreeListColumn;
    col_id: TcxDBTreeListColumn;
    col__calc_radius: TcxDBTreeListColumn;
    col__power_dBM: TcxDBTreeListColumn;
    col_THRESHOLD_BER_3: TcxDBTreeListColumn;
    col__freq: TcxDBTreeListColumn;
    col_Cell_Layer_name: TcxDBTreeListColumn;
    col__CalcModel_name: TcxDBTreeListColumn;
    col__Color: TcxDBTreeListColumn;
    col_k0_OPEN: TcxDBTreeListColumn;
    col__k0_closed: TcxDBTreeListColumn;
    col__k0: TcxDBTreeListColumn;
    col__AntennaType_Name: TcxDBTreeListColumn;
    col__Height: TcxDBTreeListColumn;
    col__Azimuth: TcxDBTreeListColumn;
    col__Tilt: TcxDBTreeListColumn;
    col__Loss: TcxDBTreeListColumn;
    col__Band: TcxDBTreeListColumn;
    cxDBTreeList1: TcxDBTreeList;
    DataSource1: TDataSource;
    col_THRESHOLD_BER_6: TcxDBTreeListColumn;
    ADOStoredProc1: TADOStoredProc;
    col_xref_id: TcxDBTreeListColumn;
    ADOConnection1: TADOConnection;
    col_Property_lat: TcxDBTreeListColumn;
    col_Property_lon: TcxDBTreeListColumn;
    col_Property_lat_str: TcxDBTreeListColumn;
    col_Property_lon_str: TcxDBTreeListColumn;
    procedure FormCreate(Sender: TObject);
    procedure _ActionMenu(Sender: TObject);

    procedure FormDestroy(Sender: TObject);
    procedure dxDBTreeListExit(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure ADOQuery1AfterPost(DataSet: TDataSet);
    procedure cxDBTreeList1Editing(Sender: TObject; AColumn: TcxTreeListColumn; var Allow:
        Boolean);

  private
    FPmp_Region_ID: Integer;
    FDataSet: TDataSet;

    FReadOnly : Boolean;


//    FSelectionList : TList;

//    FUpdated: boolean;
//    FRegionGUID: string;

//    FRegionType: TdmCalcRegionType;
//    FCanBeModified: boolean;

    FIDList: TIDList;

    procedure CheckItems(aList: TList; aChecked: Boolean);

    procedure DbExplorerRefreshItem;
    procedure DelItem(aID: integer);
    procedure Dlg_Add;
    procedure UpdateItem(aID: integer; aChecked: Boolean);

  public
  
    procedure SetReadOnly(Value: Boolean);
//    procedure CheckByStatus;
    procedure View(aPmpRegionID: Integer);

  end;

//====================================================================
// implementation
//====================================================================

implementation

uses dm_User_Security; {$R *.DFM}



const
  STR_CLEAR_CALC_MODELS = '�������� ������ �������?';



procedure Tframe_CalcRegion_cells.cxDBTreeList1Editing(Sender: TObject; AColumn:
    TcxTreeListColumn; var Allow: Boolean);

(*
var
  sObjName: string;
*)
begin

// sObjName := FDataSet.FieldByName(FLD_OBJNAME).AsString;
//  sObjName := FDataSet.FieldByName(FLD_TYPE).AsString;

//  s:=AColumn.DataBinding.FieldName;

//  bAnt:=Eq(sObjName,OBJ_LINKEND_ANTENNA) or
 //    Eq(sObjName,'ANTENNA');

  Allow:=(AColumn=col__checked);

//
//  (Eq(sObjName, OBJ_PMP_SECTOR) and
//          (
//            (AColumn=col__linkendtype_name) or
//            (AColumn=col__cell_layer_name) or
//            (AColumn=col__Trx_Sense) or
//            (AColumn=col__Trx_Power_dbm) or
//          //  col_Trx_Power.Options.Editing       := bAnt;
//            (AColumn=col__Trx_Freq) or
//
//            (AColumn=col__Calc_Radius) or
//            (AColumn=col__calc_model_name) or
//            (AColumn=col__K0) or
//            (AColumn=col__K0_Open) or
//            (AColumn=col__K0_Closed)
//           )
//         )

  //
end;


//--------------------------------------------------------------------
procedure Tframe_CalcRegion_cells.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  act_Add.Caption     := STR_ACT_ADD;
  act_Del.Caption     := STR_ACT_DEL;
//  act_Collapse.Caption:= STR_ACT_COLLAPSE + ' ���';
//  act_Expand.Caption  := STR_ACT_EXPAND + ' ���';
  act_Check.Caption   := STR_ACT_CHECK;
  act_UnCheck.Caption := STR_ACT_UNCHECK;

//  dxDBTreeList.Align := alClient;

  // ----------------------------

  cxDBTreeList1.Align := alClient;


  act_Grid_Setup.Caption:= STR_SETUP_TABLE;

  FIDList := TIDList.Create;


//  oList :=TList.Create;

 

(*  col_Name.Caption:=    STR_BS_NAME;
  col_Number.Caption:=  STR_BS_NUMBER;
  col_Checked.Caption:= '������';
  col_Cell_Layer_name.Caption:= STR_CELL_LAYER;
  col_calc_radius.Caption    := STR_CALC_RADIUS_km;
  col_CalcModel_name.Caption := STR_NAME;
  col_power_dBm.Caption      := STR_POWER_dBm;
  col_sense.Caption          := STR_SENSITIVITY_dBm;
  col_freq.Caption           := STR_FREQ_MHz;
  col_Color.Caption          := STR_COLOR;

  col_Antenna_Type.Caption   := STR_MODEL;
  col_Antenna_Height.Caption := STR_HEIGHT;
  col_Antenna_Azimuth.Caption:= STR_AZIMUTH;
  col_Antenna_tilt.Caption   := STR_TILT;
  col_Antenna_pos.Caption    := STR_COORDS;
  col_Ant_Loss.Caption       := STR_LOSS_dB;
*)
  // -------------------------------------------------------------------

  col__Name1.Caption.Text:=    STR_BS_NAME;
  col__Number.Caption.Text:=  STR_BS_NUMBER;
  col__Checked.Caption.Text:= '������';
  col_Cell_Layer_name.Caption.Text:= STR_CELL_LAYER;

  col_Cell_Layer_name.DataBinding.FieldName := FLD_CellLayer_NAME;


  col__calc_radius.Caption.Text    := STR_CALC_RADIUS_km;
  col__CalcModel_name.Caption.Text:= STR_NAME;
  col__power_dBM.Caption.Text      := STR_POWER_dBm;

  col_ThRESHOLD_BER_3.Caption.Text := STR_ThRESHOLD_BER_3;
  col_ThRESHOLD_BER_6.Caption.Text := STR_ThRESHOLD_BER_6;

//  col__sense.Caption.Text          := STR_SENSITIVITY_dBm;
  col__freq.Caption.Text           := STR_FREQ_MHz;
  col__Color.Caption.Text          := STR_COLOR;

  col__AntennaType_Name.Caption.Text   := STR_MODEL;
  col__Height.Caption.Text := STR_HEIGHT;
  col__Azimuth.Caption.Text:= STR_AZIMUTH;
  col__tilt.Caption.Text   := STR_TILT;
 // col__Pos.Caption.Text    := STR_COORDS;
  col__Loss.Caption.Text       := STR_LOSS;

//  col_Lat.Caption:=STR_LAT;
//  col_Lon.Caption:=STR_LON;

 // col_Checked.Width:=25;

 // dmCalcRegion_cells_view.InitDB (mem_Items);
(*
  col_CalcModel_name.FieldName  :=FLD_CALC_MODEL_NAME;
  col_Cell_Layer_name.FieldName :=FLD_CELL_LAYER_NAME;
  col_Color.FieldName           :=FLD_COLOR;

  dx_CheckRegistry (dxDBTreeList, FRegPath+ dxDBTreeList.Name);
  dxDBTreeList.LoadFromRegistry (FRegPath+ dxDBTreeList.Name);
  dx_CheckColumnSizes_DBTreeList (dxDBTreeList);

  col_Type.Visible:=False;
  col_Tree_ID.Visible:=False;


  col_HAS_CHILDREN.Visible:=False;
  col__HAS_CHILDREN.Visible:=False;
  *)

  FDataSet:=ADOStoredProc1;



 cx_SetColumnCaptions(cxDBTreeList1,
     [
     // SArr(FLD_NAME, '��������'),
 //     SArr(FLD_CHECKED,  '���'),
//      SArr(FLD_NAME,     '��������'),

      FLD_THRESHOLD_BER_3,  STR_THRESHOLD_BER_3,
      FLD_THRESHOLD_BER_6,  STR_THRESHOLD_BER_6
      ]);

(*      SArr(FLD_CITY,     '�����'),
      SArr(FLD_LAT,      '������ WGS'),
      SArr(FLD_LON,      '������� WGS'),

      SArr(FLD_LAT_STR,  '������ WGS, ��'),
      SArr(FLD_LON_STR,  '������� WGS, ��'),
*)

end;


procedure Tframe_CalcRegion_cells.FormDestroy(Sender: TObject);
begin
 // dxDBTreeList.SaveToRegistry (FRegPath+ dxDBTreeList.Name);
  inherited;
end;


//--------------------------------------------------------------------
procedure Tframe_CalcRegion_cells.View(aPmpRegionID: Integer);
//--------------------------------------------------------------------
var
  sRegion: string;
  i: Integer;
begin
  Assert(aPmpRegionID>0);


  dmOnega_DB_data.OpenStoredProc(ADOStoredProc1,
         SP_PMP_CALCREGION_SELECT_ITEM,
         [FLD_ID,  aPmpRegionID ]);



 db_SetFieldCaptions(ADOStoredProc1,
     [
     // SArr(FLD_NAME, '��������'),
 //     SArr(FLD_CHECKED,  '���'),
//      SArr(FLD_NAME,     '��������'),

      FLD_THRESHOLD_BER_3,  STR_THRESHOLD_BER_3,
      FLD_THRESHOLD_BER_6,  STR_THRESHOLD_BER_6
      ]);

(*      SArr(FLD_CITY,     '�����'),
      SArr(FLD_LAT,      '������ WGS'),
      SArr(FLD_LON,      '������� WGS'),

      SArr(FLD_LAT_STR,  '������ WGS, ��'),
      SArr(FLD_LON_STR,  '������� WGS, ��'),
*)

  FPmp_Region_ID:=aPmpRegionID;



 // StatusBar1.SimpleText:='�����: '+ IntToStr (dxDBTreeList.Count);

end;


//-------------------------------------------------------------------
procedure Tframe_CalcRegion_cells._ActionMenu(Sender: TObject);
//-------------------------------------------------------------------

var iCount, iCellCount, i, j, iID, iTreeID, iSiteID: integer;
  sTable, sObjName: string;
  bChecked: boolean;
  oCellIDList: TIDList;

  oList: TList;

begin
//  if FUpdated then Exit;

(*  if Sender=act_Collapse then
    dxDBTreeList.FullCollapse else
*)

  //-------------------------------------------------------------------
  if Sender=act_Add then begin
  //-------------------------------------------------------------------
    Dlg_Add();
  end else


  //-------------------------------------------------------------------
  if (Sender=act_Del) or
     (Sender=act_Check) or
     (Sender=act_UnCheck) then begin
  //-------------------------------------------------------------------
     FIDList.Clear;

     with cxDBTreeList1 do
          for i := 0 to SelectionCount - 1 do
          begin
//            iID:=Selections[i].Values[col_xref_id.ItemIndex];
            iID:=Selections[i].Values[col_id.ItemIndex];
         //   iID:=cxGrid1DBTableView1.DataController.Values[SelectedRows[i].RecordIndex, col__id.Index];
            FIDList.AddID (iID);
          end;


    oList :=TList.Create;

    with cxDBTreeList1 do
      for i := 0 to SelectionCount - 1 do
      begin
        iID:=Selections[i].Values[col_xref_id.ItemIndex];
     //   iID:=cxGrid1DBTableView1.DataController.Values[SelectedRows[i].RecordIndex, col__id.Index];
        oList.Add(Pointer(iID));
      end;


  //   if (Sender=act_Del) then;
   //    CheckItems(oList, True);

     if (Sender=act_Check) then
       CheckItems(oList, True)
     else

     if (Sender=act_UnCheck) then
       CheckItems(oList, False);

      FreeAndNil(oList);


    if (Sender=act_Del) then
    begin
      if ConfirmDlg ('������� ������ ?') then
        for i := 0 to FIDList.Count - 1 do
          DelItem (FIDList[i].ID );

    end;

   //   if (Sender=act_Del) then

    View (FPmp_Region_ID);


  end else

{
  //---------------------------------
  if Sender = act_Grid_Setup then begin
  //---------------------------------
    cx_DBTreeList_Customizing(cxDBTreeList1);

  ////////  cxDBTreeList1.Customizing.MakeColumnSheetVisible;
//    dx_Dlg_Customize_DBTreeList (dxDBTreeList);
  end else
}

  //---------------------------------
  if Sender = act_CheckByStatus then begin
  //---------------------------------
 //   CheckByStatus();

  end else

  ;
end;


//--------------------------------------------------------------------
procedure Tframe_CalcRegion_cells.dxDBTreeListExit(Sender: TObject);
//--------------------------------------------------------------------
begin
  db_PostDataset (FDataSet);
end;



//--------------------------------------------------------------------
procedure Tframe_CalcRegion_cells.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//--------------------------------------------------------------------
var
  bNoData: Boolean;
  bSector: Boolean;
  bSite: Boolean;
  sObjName: string;
begin
  if FReadOnly then
    Exit;
     



  bNoData := (not FDataSet.Active) or (FDataSet.RecordCount=0);

  if bNoData then
  begin
    act_Check.Enabled:=False;
    act_UnCheck.Enabled:=False;
    act_Del.Enabled:=False;
    act_Clear_CalcModel.Enabled:=False;

    Exit;
  end;

  sObjName := FDataSet.FieldByName(FLD_OBJNAME).AsString;


(*  if not FCanBeModified then
    exit;
*)
 // iID:=FDataSet.FieldByName(FLD_ID).AsInteger;

  bSite:=  Eq(sObjName, OBJ_PMP_SITE);
  bSector:=Eq(sObjName, OBJ_PMP_SECTOR);

  act_Check.Enabled:=bSite;

  act_UnCheck.Enabled:= bSite;
  act_Del.Enabled:=     bSite;

  act_Clear_CalcModel.Enabled:= bSector;

end;

// ---------------------------------------------------------------
procedure Tframe_CalcRegion_cells.CheckItems(aList: TList; aChecked: Boolean);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to aList.Count - 1 do
   // UpdateItem(Integer(aList[i]), aChecked);

    dmOnega_DB_data.ExecStoredProc (SP_PMP_CALCREGION_UPDATE_ITEM,
        [
          db_Par(FLD_ID,      Integer(aList[i])),
          db_Par(FLD_checked, aChecked)
         ]);
end;


procedure Tframe_CalcRegion_cells.ADOQuery1AfterPost(DataSet: TDataSet);
var
  iRes: Integer;
begin
 // UpdateItem(DataSet[FLD_XREF_ID], DataSet[FLD_checked]);

  iRes:=dmOnega_DB_data.ExecStoredProc (SP_PMP_CALCREGION_UPDATE_ITEM,
        [
          db_Par(FLD_ID,      DataSet[FLD_XREF_ID]),
          db_Par(FLD_checked, DataSet[FLD_checked])
         ]);
end;

// ---------------------------------------------------------------
procedure Tframe_CalcRegion_cells.UpdateItem(aID: integer; aChecked: Boolean);
// ---------------------------------------------------------------
var
  i: Integer;
begin
  i:=dmOnega_DB_data.ExecStoredProc (SP_PMP_CALCREGION_UPDATE_ITEM,
        [
          db_Par(FLD_ID,      aID),
          db_Par(FLD_checked, aChecked)
         ]);

  Assert(i>0);
end;

// ---------------------------------------------------------------
procedure Tframe_CalcRegion_cells.DelItem(aID: integer);
// ---------------------------------------------------------------
var
  i: Integer;
begin

//CREATE PROCEDURE dbo.sp_PMP_CalcRegion_pmp_site_Del
//(
//  @PMP_CALC_REGION_ID int,
//  @PMP_SITE_ID int
//)

  i:=dmOnega_DB_data.ExecStoredProc (sp_PMP_CalcRegion_pmp_site_Del,
        [
          db_Par(FLD_PMP_SITE_ID,      aID),
          db_Par(FLD_PMP_CALC_REGION_ID, FPmp_Region_ID)
        ]);

   Assert(i>0);
end;



// ---------------------------------------------------------------
procedure Tframe_CalcRegion_cells.DbExplorerRefreshItem;
// ---------------------------------------------------------------
var
  sRegionGUID : string;
begin

  if sRegionGUID='' then
    sRegionGUID:=dmCalcRegion_pmp.GetGuidByID (FPmp_Region_ID);

  g_Shell.UpdateNodeChildren_ByGUID(sRegionGUID);

//  IMainProjectExplorer.UPDATE_NODE_CHILDREN(sRegionGUID);

end;

//-------------------------------------------------------------------
procedure Tframe_CalcRegion_cells.Dlg_Add;
//-------------------------------------------------------------------
begin
  if Tdlg_CalcRegion_add_sites.ExecDlg(FPmp_Region_ID) then
    view(FPmp_Region_ID);


 // FIDList.Clear;

//    case FRegionType of
//(*      crtGSM: begin
//                dmCalcRegion_cells_view.GetSiteIDListFromDataset (mem_Items, FIDList); //, OBJ_SITE);
//
//                if dmAct_Explorer.Dlg_Select_Items_on_Filter (otSite, FIDList) then
//                begin
//                  dmCalcRegion_cells_view.SetSiteIDList (mem_Items, FPmp_Region_ID, FIDList, OBJ_SITE);
//                  View (FPmp_Region_ID);
//                end;
//              end;
//*)
//      crtPMP: begin
//
//                dmCalcRegion_cells_view.GetSiteIDListFromDataset (FDataSet, FIDList, OBJ_PMP_SITE);
//
//                if dmAct_Explorer.Dlg_Select_Items_on_Filter (otPmpSite, FIDList) then
//                begin
//                  dmCalcRegion_cells_view.SetSiteIDList (FDataSet, FPmp_Region_ID, FIDList, OBJ_PMP_SITE);
//                  View (FPmp_Region_ID);
//                end;
//
//              end;
//
//    end;

(*
  dmCalcRegion_cells_view.GetSiteIDListFromDataset (FDataSet, FIDList);

  if dmAct_Explorer.Dlg_Select_Items_on_Filter1 (otPmpSite, FIDList) then
  begin
    dmCalcRegion_cells_view.SetSiteIDList (FDataSet, FPmp_Region_ID, FIDList);
    View (FPmp_Region_ID);
  end;

//  end;


  DbExplorerRefreshItem();
*)

end;

procedure Tframe_CalcRegion_cells.SetReadOnly(Value: Boolean);
begin
//  cxGrid1DBTableView1.OptionsData.Editing := not Value;
  FReadOnly := Value;

//  ADOStoredProc1.LockType := IIF(Value, ltReadOnly, ltBatchOptimistic);

  ADOStoredProc1.LockType := IIF(Value, ltReadOnly, ltBatchOptimistic);
  SetFormActionsEnabled(Self, not Value);

//  SetFormActionsEnabled(Self, not dmUser_Security.ProjectIsReadOnly);

end;


end.

{

sp_PMP_CalcRegion_pmp_site_Del
