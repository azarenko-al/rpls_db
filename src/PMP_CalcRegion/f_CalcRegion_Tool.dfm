inherited frm_CalcRegion_Tool: Tfrm_CalcRegion_Tool
  Left = 660
  Top = 308
  Width = 356
  Height = 466
  Caption = 'frm_CalcRegion_Tool'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel [0]
    Left = 0
    Top = 87
    Width = 340
    Height = 2
    Align = alTop
  end
  inherited ToolBar1: TToolBar
    Width = 340
  end
  inherited StatusBar1: TStatusBar
    Top = 408
    Width = 340
    Panels = <
      item
        Width = 50
      end>
  end
  object bar_Region: TStatusBar [3]
    Left = 0
    Top = 65
    Width = 340
    Height = 22
    HelpContext = 20
    Align = alTop
    Panels = <
      item
        Bevel = pbNone
        Text = #1056#1072#1081#1086#1085' '#1088#1072#1089#1095#1077#1090#1072
        Width = 80
      end
      item
        Width = 50
      end>
    SizeGrip = False
  end
  object ListView1: TListView [4]
    Left = 0
    Top = 89
    Width = 340
    Height = 286
    Align = alTop
    Columns = <
      item
        Caption = #1057#1090#1072#1085#1076#1072#1088#1090
        Width = 86
      end
      item
        Caption = #1057#1077#1082#1090#1086#1088
        Width = 86
      end
      item
        Caption = 'R [km]'
        Width = 86
      end
      item
        Caption = 'E [dBm]'
        Width = 86
      end>
    ColumnClick = False
    TabOrder = 3
    ViewStyle = vsReport
  end
  object qry_Temp: TADOQuery
    Parameters = <>
    Left = 268
    Top = 6
  end
end
