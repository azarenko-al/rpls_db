unit u_CalcRegion_classes;

interface

uses
  Classes,

  u_const_db,

  u_geo
  , DB;

type
  TSectorInfo = class
  public
    SectorID    : Integer;
    CellID      : integer;  // ����� �������

    Color : Integer;

    SectorName  : string;
    SiteName    : string;

    SiteAddress : string;
    SitePos     : TBLPoint;
  end;


  TSectorInfoList = class(TList)
  private
    function GetItem(Index: integer): TSectorInfo;
  public
    function AddItem: TSectorInfo;
    function FindByID (aID: integer): TSectorInfo;
    procedure LoadFromDatabase(aDataset: TDataset);

    property Items[Index: integer]: TSectorInfo read GetItem;
  end;



implementation

function TSectorInfoList.AddItem: TSectorInfo;
begin
  Result := TSectorInfo.Create;
  Add(Result);
end;

//----------------------------------------------------
function TSectorInfoList.GetItem (Index: integer): TSectorInfo;
//----------------------------------------------------
begin
  Result:=TSectorInfo (inherited Items[Index]);
end;

//----------------------------------------------------
function TSectorInfoList.FindByID (aID: integer): TSectorInfo;
//----------------------------------------------------
var i: integer;
begin
  for i:=0 to Count-1 do
    if Items[i].SectorID = aID then begin Result:=Items[i]; Exit; end;
  Result:=nil;
end;


procedure TSectorInfoList.LoadFromDatabase(aDataset: TDataset);
var
  oSectorInfo: TSectorInfo;
begin
  Clear;
  aDataset.First;

  with aDataset do
    while not EOF do
  begin
    oSectorInfo:=AddItem;

//    oSectorInfo:= TSectorInfo.Create;

    oSectorInfo.SectorName  := FieldByName(FLD_NAME).AsString;
    oSectorInfo.SectorID    := FieldByName(FLD_PMP_SECTOR_ID).AsInteger;

    oSectorInfo.Color       := FieldByName(FLD_COLOR).AsInteger;

//    oSectorInfo.CellID      := FieldByName(FLD_CELLID).AsInteger;
    oSectorInfo.SiteName    := FieldByName(FLD_PMP_SITE_NAME).AsString;
//    oSectorInfo.SiteAddress := FieldByName(FLD_ADDRESS).AsString;
    oSectorInfo.SitePos.B   := FieldByName(FLD_LAT).AsFloat;
    oSectorInfo.SitePos.L   := FieldByName(FLD_LON).AsFloat;

    Next;
  end;
end;

end.
