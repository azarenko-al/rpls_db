unit dm_act_PMP_CalcRegion_Site;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Dialogs,  Menus,
  Controls, Forms, ActnList,

  I_Object,

  dm_act_Base,

//  u_GEO,
  u_classes,
  u_types,

  u_func,
  u_dlg,
 // u_func_msg,
 // u_const_msg,

  u_const_str,
  u_const_db,

  fr_PMP_site_View

//  fr_Link_view,

//  dm_LinkLine,
//  dm_Link,
 // dm_LinkLine_link

  ;

type
  TdmAct_PMP_CalcRegion_Site = class(TdmAct_Base)
    procedure DataModuleCreate(Sender: TObject);

  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm  (aOwnerForm: TForm): TForm; override;

    procedure DoAction (Sender: TObject);

// TODO: ShowOnMap
//// TODO: ItemDel
////  function ItemDel (aID: integer): boolean; override;
//
//  procedure ShowOnMap (aID: integer);
  public

    class procedure Init;
  end;


var
  dmAct_PMP_CalcRegion_Site: TdmAct_PMP_CalcRegion_Site;

//==================================================================

//==================================================================
implementation {$R *.dfm}



//--------------------------------------------------------------------
class procedure TdmAct_PMP_CalcRegion_Site.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_PMP_CalcRegion_Site) then
    dmAct_PMP_CalcRegion_Site:=TdmAct_PMP_CalcRegion_Site.Create(Application);
end;

//--------------------------------------------------------------------
procedure TdmAct_PMP_CalcRegion_Site.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_PMP_CALC_REGION_SITE;

//  act_Move.Caption:=STR_MOVE;

 // SetActionsExecuteProc ([act_Object_ShowOnMap], DoAction);
end;

// TODO: ShowOnMap
////--------------------------------------------------------------------
//procedure TdmAct_PMP_CalcRegion_Site.ShowOnMap (aID: integer);
////--------------------------------------------------------------------
//(*var iLinkID: integer;
//BLVector: TBLVector;*)
//begin
//
//(*  iLinkID:= dmLinkLine_link.GetLinkID (aID);
//
//if dmLink.GetBLVector (iLinkID, BLVector) then
//  ShowVectorOnMap (BLVector, aID);
//*)
////  ShowRectOnMap (dmLink.GetBLRect (iLinkID), aID);
//end;

//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_PMP_CalcRegion_Site.DoAction(Sender: TObject);
begin
 // if Sender=act_Object_ShowOnMap then
 //   ShowOnMap (FFocusedID);
end;

//--------------------------------------------------------------------
function TdmAct_PMP_CalcRegion_Site.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_PMP_site_View.Create(aOwnerForm);
  (Result as Tframe_PMP_site_View).ViewMode:=vmPmpCalcRegionSite;//  inkLine_Link;
end;

//--------------------------------------------------------------------
procedure TdmAct_PMP_CalcRegion_Site.GetPopupMenu;
//--------------------------------------------------------------------
begin
  case aMenuType of
    mtItem:   begin
              //  AddMenuItem (aPopupMenu, act_Object_ShowOnMap);
               // AddMenuItem (aPopupMenu, nil);
               // AddMenuItem (aPopupMenu, act_Del_);
              end;
//    mtList:
 //               AddMenuItem (aPopupMenu, act_Del_list);
  end;
end;



begin
 
end.





// TODO: ItemDel
////--------------------------------------------------------------------
//function TdmAct_LinkLine_Link.ItemDel (aID: integer): boolean;
////--------------------------------------------------------------------
//begin
//Result:=dmLinkLine_link.Del (aID);
//end;

