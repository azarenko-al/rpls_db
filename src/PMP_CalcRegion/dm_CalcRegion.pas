unit dm_CalcRegion;

interface

uses
  SysUtils, Classes, Forms, Variants, Db, ADODB,

  dm_Onega_DB_data,

  dm_Main,

  u_Geo,
  u_func,
  u_db,
  u_files,

  u_classes,

  u_types,

  u_const_db,

  dm_Object_base,

  dm_CalcRegion_Polygons
  ;

type
  TdmCalcRegionFileType = (ftKUP, ftKGR, ftNBEST); //,ftKMO);


  //-------------------------------------------------------------------
  TdmCalcRegion_pmp_AddRec = record
  //-------------------------------------------------------------------

    ProjectID :  integer;

    NewName       :  string;

    KupID         :  integer;
    KmoId         :  integer;
//    HandoverID    :  integer;

    CalcStep      :  integer;

    CalcModelID   :  integer;
    TerminalID    :  integer;
    ClutterModelID:  integer;

//    PointArr      :  TBLPointArrayF;
    PointArr1      :  TBLPointArray;

    Terminal : record
                 Height: double;
                 Power_W: double;
                 Sensitivity: double;
                 Loss : Double;
               end;
  end;

  //-------------------------------------------------------------------
  TdmCalcRegion_pmp_InfoRec = record
  //-------------------------------------------------------------------
    CalcModelID:    integer;
    ClutterModelID: integer;
    TerminalID:     integer;
  //  PolygonID:      integer;
  end;


  TdmCalcRegion_pmp = class(TdmObject_base)
    qry_Region1: TADOQuery;
    qry_Cell_Layers1: TADOQuery;
    qry_RelMatrix1: TADOQuery;
    qry_Temp: TADOQuery;

    procedure DataModuleCreate(Sender: TObject);
  private
// TODO: MakeCopy
//  function MakeCopy(aID: integer): Integer;

  public
    function  Del  (aID: integer): boolean;
    function  Add  (aRec: TdmCalcRegion_pmp_AddRec): integer;

    function GetSectorFileNameByID (aFileType: TdmCalcRegionFileType; aCellID, aID: integer): string;

    function  GetFileDir (aID: integer; aInitDir: string = ''): string;
    function GetFileName (aID, aNetStandard_ID: integer; aFileType: TdmCalcRegionFileType): string;

    function GetInfo1(aID: Integer; var aRec: TdmCalcRegion_pmp_InfoRec): boolean;

    function GetBLRect (aID: integer): TBLRect;
    procedure GetCellLayerList_used(aID: integer; aIDList: TIDList);

  //  procedure RegisterDriveMaps(aRegionID, aDriveID: integer; aCellName, aDriveMap, aErrorMap: string);


  end;


function dmCalcRegion_pmp: TdmCalcRegion_pmp;

//==================================================================
implementation  {$R *.dfm}
//==================================================================


var
  FdmCalcRegion_pmp: TdmCalcRegion_pmp;


// ---------------------------------------------------------------
function dmCalcRegion_pmp: TdmCalcRegion_pmp;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmCalcRegion_pmp) then
      FdmCalcRegion_pmp := TdmCalcRegion_pmp.Create(Application);

  Result := FdmCalcRegion_pmp;
end;


//-------------------------------------------------------------------
procedure TdmCalcRegion_pmp.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  TableName:=TBL_PMP_CALCREGION;
 // DisplayName:=STR_CALC_REGION;
  ObjectName:=OBJ_pmp_Calc_Region;
end;



//-------------------------------------------------------
procedure TdmCalcRegion_pmp.GetCellLayerList_used(aID: integer; aIDList:
    TIDList);
//-------------------------------------------------------
var
  sql: string;
  s: string;
  iID: integer;
begin
  Assert(Assigned(aIDList));


  sql := 'SELECT * FROM '+ VIEW_PMP_CALCREGION_USED_CELL_Layers +
         ' WHERE (PMP_Calc_Region_ID = :ID)';

  dmOnega_DB_data.OpenQuery(qry_Temp, SQL, [FLD_ID, aID]);


// db_CopyRecordsToStringList();


  with qry_Temp do
    while (not EOF) do
    begin
      iID := FieldValues[FLD_ID];
      s :=FieldValues[FLD_NAME];

      aIDList.AddName (iID, s);

      Next;
    end;
end;


//-----------------------------------------------------
function TdmCalcRegion_pmp.Del (aID: integer): boolean;
//-----------------------------------------------------
var //iPolyID: integer;    //, iStep
    I: Integer;
    sRegionDir: string;
   // arrFreqPlans: TIntArray;

(*const
  sp_CalcRegion_del = 'sp_CalcRegion_del';
*)

begin
  try
////    dmCalcMap.DelByCalcRegionID (aID);

    sRegionDir:= GetFileDir(aID);
    DirClear (sRegionDir); //, True)DirExists(sRegionDir));

    dmOnega_DB_data.Pmp_CalcRegion_del (aID);

//    gl_DB.ExecSP (sp_CalcRegion_del, [db_Par(FLD_ID, aID)] );



//    dmMain.Dirs.ProjectCalcDir + Format('region.%d\', [aID]);

  //  arrFreqPlans:= GetFreqPlanIDs (aID);

  //zz  for I := 0 to High(arrFreqPlans) do
  //zz    dmFreqPlan.Del(arrFreqPlans[i]);

{ TODO : rename }


 //zzzzzzz   PostEvent (WE_MAP_REMOVE_RASTERS, [app_Par(FLD_FILEDIR, sRegionDir)   ]);
//    sRegionDir:= GetFileDir (aID);


  //z  dmCalcMap.DelByCalcRegionID (aID);

//    dmCalcMap.DelByCalcRegionName (GetNameByID(aID));

//  14 11 07  ������ ������� �� �������� CalcRegion,
//  ��������� ��������������� ������ � �������� Polygons
//    iPolyID:= GetPolyID (aID);
//    dmPolygons.Del (iPolyID);


//    inherited Del(aID);

    Result:= True;
  except
    Result:= False;
  end;
end;


//-----------------------------------------------------
function TdmCalcRegion_pmp.Add (aRec: TdmCalcRegion_pmp_AddRec): integer;
//-----------------------------------------------------

(*      function DoGetTerminalParams(aRegionID: integer; var aOutRec: TdmTerminalAddRec): boolean;
      begin
        Result:= aRec.TerminalID>0;

        if Result then
          dmTerminal.GetInfo1 (aRec.TerminalID, aOutRec)
        else begin
          aOutRec.Height:= 0;
          aOutRec.Power_W:= 0;
          aOutRec.Sensitivity_dBm:= 0;
        end;
      end;
*)



var //str: string;
  //  iPolyID: integer;
//    i: integer;
//    K0, k0_Open, k0_Closed: double;
  //  rTerminalRec: TdmTerminalAddRec;
  i: integer;
begin


//  Assert(aRec.PointArr.Count>0);
  Assert( Length(aRec.PointArr1)>0);


  Assert(aRec.ProjectID>0, 'Value <=0');


  Assert(aRec.ClutterModelID>0, 'Value <=0');
  Assert(aRec.CalcModelID>0, 'Value <=0');

 // Assert(aRec.CalcModelID>0, 'Value <=0');


  try
//    gl_Db.BeginTrans;
//    iPolyID:=dmPolygons.Add (aRec.PointArr);

  //  Assert (iPolyID>0);

    with aRec do
    begin
(*      if CalcModelID > 0 then
        dmCalcModel.GetK0_open_close(CalcModelID, K0, k0_Open, k0_Closed);
*)
    //  DoGetTerminalParams(Result, rTerminalRec);

      Result:=dmOnega_DB_data.ExecStoredProc_ (sp_PMP_CalcRegion_add,

 //     Result:=gl_DB.AddRecordID (TBL_CALCREGION,
          [
           FLD_PROJECT_ID,       aRec.ProjectID,
//           db_Par(FLD_PROJECT_ID,       dmMain.ProjectID),
           FLD_NAME,             NewName,

//           db_Par(FLD_OBJNAME,          'pmp'),

      //     db_Par(FLD_TYPE,             'pmp'),

//           db_Par(FLD_TYPE,             IIF(Type_=crtGSM,'gsm','pmp')),


      //     db_Par(FLD_FOLDER_ID,        IIF_NULL(FolderID)),

           FLD_KUP_ID,           IIF_NULL(KupID),
           FLD_KMO_ID,           IIF_NULL(KmoID),
      //     db_Par(FLD_HANDOVER_ID,      IIF_NULL(HandoverID)),

           FLD_TERMINAL_ID,      IIF_NULL(TerminalID),
           FLD_Terminal_Height,  IIF_NULL(Terminal.Height),
           FLD_Terminal_Power_W, IIF_NULL(Terminal.Power_W),
           FLD_Terminal_Sensitivity, IIF_NULL(Terminal.Sensitivity),

//           terminal_threshold_dBm

           FLD_STEP,    			    CalcStep,

           FLD_CALC_MODEL_ID,    CalcModelID,
(*
           db_par(FLD_K0,               K0),
           db_par(FLD_K0_OPEN,          k0_Open),
           db_par(FLD_K0_CLOSED,        k0_Closed),
*)
           FLD_CLUTTER_MODEL_ID, ClutterModelID //,
       //    db_Par(FLD_POLYGON_ID,       iPolyID)
          ]);


       Assert(Result>0, 'Value <=0');

      if Result>0 then
        for i:=0 to High(aRec.PointArr1) do
//        for i:=0 to aRec.PointArr.Count-1 do
         dmOnega_DB_data.Pmp_CalcRegion_add_point(Result,
                aRec.PointArr1[i].B, aRec.PointArr1[i].L);

    ///////    dmPolygons.Add_new (Result, aRec.PointArr);

////      Result:=gl_DB.GetIdentCurrent(TBL_CALCREGION);//gl_DB.GetIdent;

      {
      if Assigned(SiteIDList) then
        for i:=0 to SiteIDList.Count-1 do
          AddSite(Result, SiteIDList[i].ID);
       }

{      for i:=0 to High(CellIDArr) do
        gl_DB.AddRecord(TBL_CALC_REGION_CELLS,
            [db_Par(FLD_CALC_REGION_ID, Result),
             db_par(FLD_CELL_ID,        CellIDArr[i]) ] );        }
    end;

 //   gl_Db.CommitTrans;
  except
 //   gl_Db.RollbackTrans;
    Result:=-1;
  end;

end;

//-------------------------------------------------------------------
function TdmCalcRegion_pmp.GetInfo1(aID: Integer; var aRec:
    TdmCalcRegion_pmp_InfoRec): boolean;
//-------------------------------------------------------------------
begin
  db_OpenTableByID (qry_Temp, TBL_PMP_CALCREGION, aID);

  with qry_Temp do
   if not IsEmpty then
   begin
     aRec.TerminalID    :=FieldByName(FLD_TERMINAL_ID).AsInteger;
     aRec.CalcModelId   :=FieldByName(FLD_CALC_MODEL_ID).AsInteger;
     aRec.ClutterModelId:=FieldByName(FLD_CLUTTER_MODEL_ID).AsInteger;
//     aRec.PolygonID     :=FieldByName(FLD_POlYGON_ID).AsInteger;
     Result:=True;
   end else
     Result:=False;

end;


//-------------------------------------------------------------------
function TdmCalcRegion_pmp.GetFileDir (aID: integer; aInitDir: string = ''): string;
//-------------------------------------------------------------------
var
  iStep: integer;
  sDir: string;
begin
  iStep:=  gl_DB.GetIntFieldValueByID(TableName, FLD_STEP, aID);


  Assert ( dmMain.Dirs.ProjectCalcDir <> '');

  sDir:= IIF(aInitDir='', dmMain.Dirs.ProjectCalcDir, IncludeTrailingBackslash(aInitDir));


   Assert ( sDir <> '');


 // if aInitDir='' then
  //  aInitDir:=dmMain.Dirs.ProjectCalcDir;

//  aInitDir := IncludeTrailingBackslash(aInitDir);


  Result:= sDir + Format('region.%d\%d\', [aID, iStep]);
end;


//-------------------------------------------------------------------
function TdmCalcRegion_pmp.GetSectorFileNameByID (aFileType: TdmCalcRegionFileType;
			aCellID, aID: integer): string;
//-------------------------------------------------------------------
begin
  Result:= GetFileDir(aID) + AsString(aCellID)+'.';

  case aFileType of
    ftKUP :   Result:=Result + 'kup';
    ftKGR :   Result:=Result + 'kgr';
    ftNBEST : Result:=Result + 'nbest';
  //  ftKMO : Result:=Result + Format('%s\', ['kmo']);
  else
    Result:='';
  end;
end;


//-------------------------------------------------------------------
function TdmCalcRegion_pmp.GetFileName (aID, aNetStandard_ID: integer;
                                    aFileType: TdmCalcRegionFileType): string;
//-------------------------------------------------------------------
begin
  Result:=GetFileDir (aID) + Format('cell_layer.%d\region.', [aNetStandard_ID]);


//  Result:=Format('%s%s.%d\%s.%d\%s', [dmMain.ProjectNetDir, 'region', aID,
//                                      'standard', aNetStandard_ID, 'region.']);
  case aFileType of
    ftKUP : Result:=Result + 'kup';
    ftKGR : Result:=Result + 'kgr';
    ftNBEST : Result:=Result + 'nbest';
  //  ftKMO : Result:=Result + Format('%s\', ['kmo']);
  else
    Result:='';
  end;
end;

//-------------------------------------------------------------------
function TdmCalcRegion_pmp.GetBLRect (aID: integer): TBLRect;
//-------------------------------------------------------------------
begin
  Result:=dmCalcRegion_Polygons.Get_BoundBLRect (aID);
//  Result:=dmPolygons.GetBoundBLRect (GetPolyID(aID));
end;


begin

end.






