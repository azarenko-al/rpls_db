unit a_Unit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics,
  Controls, Forms, Dialogs, StdCtrls, ComCtrls, rxPlacemnt,


  f_CalcRegion_tool_Best_Server,
  f_CalcRegion_tool_EMP,

  dm_Act_ColorSchema,

  dm_act_CalcRegion,
  dm_Act_Project,

//  dm_CalcRegion_PMP_site,

  //dm_CalcRegion_export_join,

  u_func_msg,
  u_dlg,

  dm_Main,


  dm_Act_Map,
  dm_Act_AntType,

  dm_Act_Report,

  dm_Act_CalcMOdel,


  dm_Act_Property,

  dm_Act_Site,
///
 /// dm_Act_PMP_site,
 /// dm_Act_PMP_sector,

  u_classes,

  u_const,
  u_const_msg,
  u_const_db,
  u_db,


  u_reg,
  u_types,

  f_Log,

  fr_Browser,

  dm_Main,
 // dm_Main_res,
 // dm_CalcRegion_export_calc,


  fr_CalcRegion_view,
 

  dm_Explorer,
  fr_Explorer,

  ExtCtrls, ToolWin
  ;

type
  Tfrm_Test = class(TForm)
    FormStorage1: TFormStorage;
    ToolBar1: TToolBar;
    bt_Add: TButton;
    bt_View: TButton;
    Button2: TButton;
    Button3: TButton;
    b_Map: TButton;
    Panel1: TPanel;
    Splitter1: TSplitter;
    pc_Main: TPageControl;
    ts_Regions: TTabSheet;
    pn_Browser: TPanel;
    ts_Sites: TTabSheet;
    bCalc: TButton;
    btn_MoveObjects: TButton;
    procedure bt_AddClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure b_MapClick(Sender: TObject);
    procedure bCalcClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
//    FSelectedID: TIDList;

    Fframe_CalcRegion : Tframe_Explorer;

    Fframe_Browser : Tframe_Browser;

  public
    { Public declarations }
  end;

var
  frm_Test: Tfrm_Test;

implementation

uses dm_Act_Map_Engine;

///uses dm_CalcRegion_export_maps, dm_CalcRegion_calc, dm_Act_Map_Engine;



{$R *.DFM}


procedure Tfrm_Test.bt_AddClick(Sender: TObject);
begin
  dmAct_CalcRegion.Add (0);

//  PostMsg (WM_CALC_REGION_ADD,0,0);

end;   


procedure Tfrm_Test.FormCreate(Sender: TObject);
var
  Fframe_CalcRegion_view: Tframe_CalcRegion_view;

begin

  gl_Reg :=TRegStore_Create (REGISTRY_ROOT);

 // Init_EventManager1;

  TdmMain.Init;
  if not dmMain.OpenDlg then  Exit;


  Tfrm_CalcRegion_tool_Best_Server.ShowWindow (22);





  dmAct_Map_Engine.Enabled:=False;
{

  Fframe_Browser := Tframe_Browser.CreateChildForm( pn_Browser);

  Fframe_CalcRegion := Tframe_Explorer.CreateChildForm ( ts_Regions);
  Fframe_CalcRegion.SetViewObjects([otCalcRegion,otSite,otReport]);
  Fframe_CalcRegion.RegPath:='CalcRegion';

  Fframe_CalcRegion.OnNodeFocused:=Fframe_Browser.ViewObjectByPIDL;
}


  dmAct_Project.LoadLastProject;

 /////////// PostMsg(WM_SHOW_FORM_MAP);

  Panel1.Align:=alClient;
//  FSelectedID:=TIDList.Create;


//  Fframe_CalcRegion_view:=Tframe_CalcRegion_view.Create(Application);

 // Fframe_CalcRegion_view.Show;
//  Fframe_CalcRegion_view.View(855);

  {
   }
//  btn_MoveObjectsClick (nil);
end;



procedure Tfrm_Test.FormDestroy(Sender: TObject);
begin
 //  Fframe_Browser.Free;
 //  Fframe_CalcRegion.Free;
end;


procedure Tfrm_Test.b_MapClick(Sender: TObject);
begin
//  dmAct_Map.ShowForm;
end;



procedure Tfrm_Test.bCalcClick(Sender: TObject);
begin
//  dmCalcRegion_calc.Execute (958);

end;



end.
