object frm_Test: Tfrm_Test
  Left = 262
  Top = 219
  Width = 595
  Height = 322
  Caption = 'frm_Test'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 587
    Height = 29
    ButtonHeight = 25
    Caption = 'ToolBar1'
    TabOrder = 0
    object btn_MoveObjects: TButton
      Left = 0
      Top = 2
      Width = 75
      Height = 25
      Caption = 'MoveObjects'
      TabOrder = 6
    end
    object bt_Add: TButton
      Left = 75
      Top = 2
      Width = 85
      Height = 25
      Caption = 'bt_Add'
      TabOrder = 0
      OnClick = bt_AddClick
    end
    object b_Map: TButton
      Left = 160
      Top = 2
      Width = 75
      Height = 25
      Caption = 'b_Map'
      TabOrder = 4
      OnClick = b_MapClick
    end
    object Button3: TButton
      Left = 235
      Top = 2
      Width = 85
      Height = 25
      Caption = 'test_Dialog'
      TabOrder = 3
    end
    object Button2: TButton
      Left = 320
      Top = 2
      Width = 85
      Height = 25
      Caption = 'to XML'
      TabOrder = 2
    end
    object bt_View: TButton
      Left = 405
      Top = 2
      Width = 89
      Height = 25
      Caption = 'view'
      TabOrder = 1
    end
    object bCalc: TButton
      Left = 494
      Top = 2
      Width = 75
      Height = 25
      Caption = 'calc'
      TabOrder = 5
      OnClick = bCalcClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 29
    Width = 587
    Height = 208
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 1
    object Splitter1: TSplitter
      Left = 185
      Top = 5
      Height = 198
    end
    object pc_Main: TPageControl
      Left = 5
      Top = 5
      Width = 180
      Height = 198
      ActivePage = ts_Regions
      Align = alLeft
      TabOrder = 0
      object ts_Regions: TTabSheet
        Caption = 'ts_Regions'
      end
      object ts_Sites: TTabSheet
        Caption = 'ts_Sites'
        ImageIndex = 1
      end
    end
    object pn_Browser: TPanel
      Left = 188
      Top = 5
      Width = 394
      Height = 198
      Align = alClient
      Caption = 'pn_Browser'
      TabOrder = 1
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 64
    Top = 92
  end
end
