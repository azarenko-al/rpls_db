program test_CalcRegion;

{%File '..\..\ver.inc'}

uses
  ShareMem,
  Forms,
  a_f_test_CalcRegion in 'a_f_test_CalcRegion.pas' {frm_test_CalcRegion},
  a_Unit in 'a_Unit.pas' {frm_Test},
  d_CalcRegion_add in '..\d_CalcRegion_add.pas' {dlg_CalcRegion_add},
  dm_Act_PMP_CalcRegion_Site in '..\dm_Act_PMP_CalcRegion_Site.pas' {dmAct_Pmp_CalcRegion_Site: TDataModule},
  dm_Act_ColorSchema in '..\..\ColorSchema\dm_act_ColorSchema.pas' {dmAct_ColorSchema: TDataModule},
  dm_act_Map in '..\..\Map\dm_act_Map.pas' {dmAct_Map: TDataModule},
  dm_Act_Map_Engine in '..\..\MapEngine\dm_act_Map_Engine.pas' {dmAct_Map_Engine: TDataModule},
  dm_Act_Project in '..\..\Project\dm_Act_Project.pas' {dmAct_Project: TDataModule},
  dm_act_Site in '..\..\Site\dm_Act_Site.pas' {dmAct_Site: TDataModule},
  dm_CalcRegion in '..\dm_CalcRegion.pas' {dmCalcRegion: TDataModule},
  dm_CalcRegion_add_view in '..\dm_CalcRegion_add_view.pas' {dmCalcRegion_add_view: TDataModule},
  dm_CalcRegion_calc_View in '..\dm_CalcRegion_calc_View.pas' {dmCalcRegion_calc_View: TDataModule},
  dm_CalcRegion_cells_view in '..\dm_CalcRegion_cells_view.pas' {dmCalcRegion_cells_view: TDataModule},
  dm_CalcRegion_site in '..\dm_CalcRegion_site.pas' {dmCalcRegion_site: TDataModule},
  dm_Custom in '..\..\.common\dm_Custom.pas' {dmCustom: TDataModule},
  dm_Explorer in '..\..\Explorer\dm_Explorer.pas' {dmExplorer: TDataModule},
  dm_Main in '..\..\DataModules\dm_Main.pas' {dmMain: TDataModule},
  dm_Object_base in '..\..\Common\dm_Object_base.pas' {dmObject_base: TDataModule},
  dm_Terminal in '..\..\Terminal\dm_Terminal.pas' {dmTerminal: TDataModule},
  f_CalcRegion_tool_Best_Server in '..\f_CalcRegion_tool_Best_Server.pas' {frm_CalcRegion_tool_Best_Server},
  f_CalcRegion_tool_EMP in '..\f_CalcRegion_tool_EMP.pas' {frm_CalcRegion_tool_EMP},
  fr_CalcRegion_add_cells in '..\fr_CalcRegion_add_cells.pas' {frame_CalcRegion_add_cells},
  fr_CalcRegion_cells in '..\fr_CalcRegion_cells.pas' {frame_CalcRegion_cells},
  fr_CalcRegion_inspector in '..\fr_CalcRegion_inspector.pas' {frame_CalcRegion_inspector},
  fr_CalcRegion_map in '..\fr_CalcRegion_map.pas' {frame_CalcRegion_Map},
  fr_CalcRegion_params in '..\fr_CalcRegion_params.pas' {frame_CalcRegion_params},
  fr_CalcRegion_view in '..\fr_CalcRegion_view.pas' {frame_CalcRegion_View},
  fr_Cell_inspector in '..\..\Cell\fr_Cell_inspector.pas' {frame_Cell_inspector},
  fr_expl_Object_List in '..\..\Explorer\fr_expl_Object_List.pas' {frame_expl_Object_list},
  fr_Map in '..\..\Map\fr_Map.pas' {frame_Map},
  fr_MapView_base in '..\..\Map\fr_MapView_base.pas' {frame_MapView_base},
  fr_Site_Cells in '..\..\Site\fr_Site_Cells.pas' {frame_Site_Cells},
  fr_Site_view in '..\..\Site\fr_Site_view.pas' {frame_Site_View},
  u_CalcRegion_const in '..\u_CalcRegion_const.pas',
  u_explorer in '..\..\Explorer\u_explorer.pas',
  u_types in '..\..\u_types.pas';

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(Tfrm_Test, frm_Test);
  Application.Run;
end.