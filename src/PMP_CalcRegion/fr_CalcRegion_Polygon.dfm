object frame_CalcRegion_Polygon: Tframe_CalcRegion_Polygon
  Left = 659
  Top = 481
  Width = 588
  Height = 343
  Caption = 'frame_CalcRegion_Polygon'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 96
    Width = 580
    Height = 213
    Align = alBottom
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = ds_Points
      DataController.Filter.MaxValueListCount = 1000
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      Filtering.MaxDropDownCount = 12
      OptionsData.Editing = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object col_Lat: TcxGridDBColumn
        DataBinding.FieldName = 'lat_str'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 119
      end
      object col_Lon: TcxGridDBColumn
        DataBinding.FieldName = 'lon_str'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 119
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object ds_Points: TDataSource
    DataSet = ADOQuery1
    Left = 116
    Top = 4
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    SQL.Strings = (
      'SELECT top 2000  id,name,lat,lon  FROM property ')
    Left = 40
    Top = 8
  end
end
