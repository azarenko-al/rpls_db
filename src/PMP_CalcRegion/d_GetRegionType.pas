unit d_GetRegionType;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, rxPlacemnt, ActnList, DB, ADODB, DBCtrls, cxPropertiesStore,

  d_Wizard,

  dm_Main,

  u_reg,

  u_db,
  u_const_db, cxLookAndFeels
  ;

const
   DEF_RECT   = 0;
   DEF_POLY   = 1;
   DEF_CIRCLE = 2;
   DEF_GEOREGION = 3;


type
  Tdlg_GetRegionType = class(Tdlg_Wizard)
    GroupBox1: TGroupBox;
    rb_Rect: TRadioButton;
    rb_Circle: TRadioButton;
    rb_Poly: TRadioButton;
    cb_GeoRegion: TRadioButton;
    DataSource1: TDataSource;
    qry_Temp: TADOQuery;
    DBLookupComboBox1: TDBLookupComboBox;
    Bevel3: TBevel;
    procedure FormDestroy(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_OkExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public
    class function ExecDlg(var aGeoRegionName: string): integer;
  end;


implementation

uses dm_Onega_db_data;
{$R *.DFM}


//--------------------------------------------------------------
class function Tdlg_GetRegionType.ExecDlg(var aGeoRegionName: string): integer;
//--------------------------------------------------------------
begin
  with Tdlg_GetRegionType.Create(Application) do
  try
    if (ShowModal = mrOK) then
    begin
      if rb_Rect.Checked then  Result := 0 else
      if rb_Poly.Checked then  Result := 1 else
      if rb_Circle.Checked then  Result := 2 else

      if cb_GeoRegion.Checked then
      begin
        Result := 3;
        aGeoRegionName:=qry_Temp[FLD_NAME];
      end;

    end else
      Result:=-1;
  finally
    Free;
  end;
end;


procedure Tdlg_GetRegionType.act_OkExecute(Sender: TObject);
begin
  inherited;
  //
end;


//-------------------------------------------------------------------
procedure Tdlg_GetRegionType.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
var
  i: integer;
  s: string;
begin
  inherited;
  Caption:='����� ��������';

  qry_Temp.Connection:= dmMain.AdoConnection;

  dmOnega_DB_data.OpenQuery(qry_Temp,
      'SELECT * FROM '+TBL_GEOREGION +' ORDER BY name');



  GroupBox1.Top:=5;

  Height:= 250;

  AddComponentProp(rb_Rect, 'checked');
  AddComponentProp(rb_Poly, 'checked');
  AddComponentProp(rb_Circle, 'checked');
  AddComponentProp(cb_GeoRegion, 'checked');
  AddComponentProp(DBLookupComboBox1, 'value');
  RestoreFrom();


  i:=reg_ReadInteger (FRegPath, DBLookupComboBox1.Name, 0);
  DBLookupComboBox1.KeyValue:=i;

 // DBLookupComboBox1.KeyValue:=reg_ReadString (FRegPath, DBLookupComboBox1.Name, '');

end;


procedure Tdlg_GetRegionType.FormDestroy(Sender: TObject);
begin
  reg_WriteInteger(FRegPath, DBLookupComboBox1.Name, DBLookupComboBox1.KeyValue);

end;


procedure Tdlg_GetRegionType.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
var
  b: Boolean;
begin
  DBLookupComboBox1.Enabled:=cb_GeoRegion.Checked;

  b:=DBLookupComboBox1.KeyValue>0;

 // if cb_GeoRegion.Checked and (DBLookupComboBox1.KeyValue>0) then
  //  btn_Ok.Enabled:=

//  btn_Ok.Enabled:=cb_GeoRegion.Checked;
end;


end.
