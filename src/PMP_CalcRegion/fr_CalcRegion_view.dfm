inherited frame_CalcRegion_View: Tframe_CalcRegion_View
  Left = 524
  Top = 347
  Width = 548
  Height = 429
  Caption = 'frame_CalcRegion_View'
  KeyPreview = True
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 540
    TabOrder = 1
  end
  inherited pn_Caption: TPanel
    Width = 540
    TabOrder = 0
  end
  inherited pn_Main: TPanel
    Width = 540
    Height = 159
    object Bevel1: TBevel
      Left = 1
      Top = 1
      Width = 538
      Height = 8
      Align = alTop
      Shape = bsBottomLine
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 9
      Width = 538
      Height = 112
      ActivePage = tsClutters
      Align = alTop
      Style = tsFlatButtons
      TabOrder = 0
      OnChange = cxPageControl1Change
      object ts_Params: TTabSheet
        Caption = #1057#1074#1086#1081#1089#1090#1074#1072
      end
      object ts_Cells: TTabSheet
        Caption = #1057#1077#1082#1090#1086#1088#1072
        ImageIndex = 1
      end
      object tsClutters: TTabSheet
        Caption = #1052#1086#1076#1077#1083#1100' '#1090#1080#1087#1072' '#1084#1077#1089#1090#1085#1086#1089#1090#1080
        ImageIndex = 2
      end
      object tsMetrics: TTabSheet
        Caption = #1052#1077#1090#1088#1080#1082#1072
        ImageIndex = 3
      end
    end
  end
  inherited MainActionList: TActionList
    Left = 8
  end
  inherited ImageList1: TImageList
    Left = 36
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      25
      0)
  end
  object ActionList1: TActionList
    Images = ImageList1
    Left = 328
    Top = 12
    object act_QuickCalc: TAction
      Caption = #1056#1072#1089#1095#1105#1090
      OnExecute = act_QuickCalcExecute
    end
  end
end
