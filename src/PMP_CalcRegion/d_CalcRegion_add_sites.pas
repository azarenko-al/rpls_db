unit d_CalcRegion_add_sites;

interface
{$I ver.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rxPlacemnt, ActnList,     StdCtrls, ExtCtrls,
     Registry, cxPropertiesStore,
  cxButtonEdit,

  u_cx_VGrid,

  d_Wizard_Add_with_params,

  dm_Main,

(*  I_Shell,
  I_MapView,
  I_MapEngine,
  dm_MapEngine,*)

 // I_MapAct,
//  dm_Act_Map,

  dm_CalcRegion,
//  dm_UserRegion,


  dm_PMP_site,
  dm_CalcRegion_PMP_site,

  u_func,
  u_db,

  u_dlg,

  u_func_msg,

  u_const_db,
  u_const_msg,
  u_const_str,

  u_classes,
  u_types,
  u_Geo,

  fr_CalcRegion_add_cells,

  cxVGrid, cxControls, cxInplaceContainer, ComCtrls, cxLookAndFeels
  ;

type


  Tdlg_CalcRegion_add_sites = class(Tdlg_Wizard_add_with_params)
    act_GetMetrics: TAction;
    cb_RegionSizeByBSRadius: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
//    procedure DoOnButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure act_OkExecute(Sender: TObject);
  private
    Fframe_CalcRegion_add_cells: Tframe_CalcRegion_add_cells;
//    Fframe_Polygon: Tframe_Polygon;

  //  FRec: TdmCalcRegionAddRec;

//    FBLPointArr: TBLPointArrayF;
    FID: integer;

    FIDList: TIDList;

//    FIMapView: IMapViewX;

    procedure UpdateMetrics();
  protected
    procedure Append;

  public

    class function ExecDlg(aID: Integer): Boolean;
  end;


//====================================================================
// implementation
//====================================================================
implementation {$R *.DFM}
(*
const
  STR_ON_MAP    = '����� �� �����';
  STR_ON_RADIUS = '�� ������� �������';
*)

//--------------------------------------------------------------------
class function Tdlg_CalcRegion_add_sites.ExecDlg(aID: Integer): Boolean;
//--------------------------------------------------------------------
begin
  with Tdlg_CalcRegion_add_sites.Create(Application) do
  try
    FID :=aID;

//    FObjName:=aObjName;

  //  FIMapView:=IActiveMapView;

//    IMapView:=aIMapView;
  //  FBLPointArr   := aBLPointArr;

//    FRec.FolderID := aFolderID;
//    FSiteIDs   := aSiteIDList;

  //  FRec.Type_ := aType;

   // ed_Name_.Text:=dmCalcRegion_pmp.GetNewName();// + IIF(FRec.Type_=crtGSM, '-GSM','-PMP');

//    row_Folder.Text:=dmFolder.GetPath (FRec.FolderID);

  //  Fframe_CalcRegion_add_cells.SetObjectName (aObjName);

   // Fframe_CalcRegion_add_cells.CalcRegionType:=aType;

    Fframe_CalcRegion_add_cells.View(FID);
//    Fframe_CalcRegion_add_cells.ViewBYPoints (FBLPointArr); //, aSiteIDList, aType);

//    Fframe_Polygon.ViewPoints (FBLPointArr);


    Result := ShowModal=mrOk;

  finally

    Free;
  end;
end;

//--------------------------------------------------------------------
procedure Tdlg_CalcRegion_add_sites.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

//  Assert(Assigned(IShell));

  SetActionName (STR_DLG_ADD_CALC_REGION);

  lb_Name.Caption:=STR_NAME;
//  row_Folder.Caption:=STR_FOLDER;



  CreateChildForm(Tframe_CalcRegion_add_cells, Fframe_CalcRegion_add_cells, tabSheet1);


  FIDList:=TIDList.Create;

  //cx_InitVerticalGrid (cxVerticalGrid1);


 // FSiteIDList:=TIDList.Create;

end;

//--------------------------------------------------------------------
procedure Tdlg_CalcRegion_add_sites.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  FreeAndNil(FIDList);


  inherited;
end;

//--------------------------------------------------------------------
procedure Tdlg_CalcRegion_add_sites.Append;
//--------------------------------------------------------------------
var
  d: double;
  I: Integer;
//  oBLPointArr: TBLPointArrayF;
begin
  if cb_RegionSizeByBSRadius.Checked then
    UpdateMetrics();


  {  if Length(FBLPointArr)>0 then
    FRec.PointArr := FBLPointArr
  else
    case FRec.Type_ of
      crtGSM: FRec.PointArr := dmSite.GetBLRect_for_Sites(FSiteIDList);
      crtPMP: FRec.PointArr := dmPMP_site.GetBLRect_for_PMP(FSiteIDList);
    end;    }

  Fframe_CalcRegion_add_cells.GetSiteIDList (FIDList); //,  FRec.Type_ );

//  if FIDList.Count>0 then
 // begin
//    Fframe_CalcRegion_add_cells.GetSiteBLPointArr(oBLPointArr);


 {   if not dmUserRegion.BLPosInUserRegion(oBLPointArr) then
    begin
      MsgDlg('������������ ���� ��� �������� �������');
      ModalResult:= mrNone;
      exit;
    end;
}

 // end;

 // Assert(FRec.PointArr.Count<>0);


//  FID:=dmCalcRegion_pmp.Add (FRec);

  dmCalcRegion_pmp_site.AddSiteList (FID, FIDList); //, OBJ_PMP_SITE);

//
//  case  FRec.Type_  of
//  //  crtGSM: dmCalcRegion_site.AddSiteList (FID, FIDList, OBJ_SITE);
//
////    crtPMP: dmCalcRegion_site.AddSiteList (FID, FIDList); //, OBJ_PMP_SITE);
//    crtPMP:
//  else
//    raise Exception.Create('');
//  end;
//

 // Assert(Assigned(IMapEngine));
//  Assert(Assigned(IMapAct));


  if FID>0 then
   // if FRec.Type_=crtPMP then
    begin
     // dmMapEngine1.CreateObject (otPmpCalcRegion, FID);

(*      d:= dmMapEngine1.GetPolyArea  (otPmpCalcRegion, FID);
      dmCalcRegion.Update(FID, [db_Par(FLD_AREA, TruncFloat(d))]);
*)
   //   g_ShellEvents.Shell_UpdateNodeChildren_ByGUID (dmCalcRegion_pmp.GetGUIDByID(FID));

  //    SH_PostUpdateNodeChildren (dmCalcRegion.GetGUIDByID(FID));

    //  PostEvent (WE_MAP_ENGINE_CREATE_OBJECT, Integer(otCalcRegion), Result);
   //   dmAct_Map.MapRefresh;
    end;

end;


//-------------------------------------------------------------------
procedure Tdlg_CalcRegion_add_sites.act_OkExecute(Sender: TObject);
//-------------------------------------------------------------------
begin
  if Sender=act_Ok then
   Append;
end;

//-------------------------------------------------------------------
procedure Tdlg_CalcRegion_add_sites.UpdateMetrics ();
//-------------------------------------------------------------------
//var oBLPointArr: TBLPointArrayF;
begin
//  if FMetricsType = mtBySiteRadius then
//  begin

  Fframe_CalcRegion_add_cells.GetSiteIDList (FIDList);

  if FIDList.Count=0 then
    Exit;


 // dmPmpSite.GetBLRect_for_PMP(FIDList, FBLPointArr);

//    Fframe_Polygon.ViewPoints (FBLPointArr);
//  end;

  {if Length(FBLPointArr)>0 then
      oBLPointArr := FBLPointArr

    else begin
      Fframe_CalcRegion_add_cells.GetSiteIDList(FSiteIDList,  FRec.Type_ );
    end;  }

 ///////   Fframe_Polygon.ViewPoints (oBLPointArr);
end;

//-------------------------------------------------------------------
end.