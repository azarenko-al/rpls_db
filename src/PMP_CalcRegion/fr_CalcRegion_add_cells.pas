unit fr_CalcRegion_add_cells;

 interface

 uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, Db, Grids, DBGrids, ADODB, StdCtrls, ExtCtrls, ComCtrls,
  ToolWin,  DBTables,  ActnList,
   cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGridCustomView,

  dm_Main,

  Registry,

  dm_Onega_DB_data,

  dm_Status,

  u_db,
  u_func,
  u_dlg,
    
  u_img,

  u_types,
  u_Geo,
  u_classes,

  u_const,
  u_const_str,
  u_const_db,

  dm_CalcRegion,

  cxGridLevel, cxClasses, cxGrid
  ;


type
  Tframe_CalcRegion_add_cells = class(TForm)
    ActionList1: TActionList;
    act_Add_All: TAction;
    act_Del_All: TAction;
    act_Add_By_Criteria: TAction;
    ADOStoredProc1: TADOStoredProc;
    DataSource1: TDataSource;
    cxGrid1: TcxGrid;
    cxGridDBBandedTableView1: TcxGridDBBandedTableView;
    cxGridDBBandedColumn4: TcxGridDBBandedColumn;
    cxGridDBBandedColumn5: TcxGridDBBandedColumn;
    cxGridDBBandedColumn6: TcxGridDBBandedColumn;
    cxGridDBBandedColumn7: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1DBBandedColumn1: TcxGridDBBandedColumn;
    cxGridLevel2: TcxGridLevel;
    cxGridDBBandedTableView1DBBandedColumn2_lat: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1DBBandedColumn2_lon: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1DBBandedColumn2Status_id: TcxGridDBBandedColumn;
    pn_Criteria: TPanel;
    Button9: TButton;
    combo_Status: TComboBox;
    combo_Criteria: TComboBox;
    ed_Radius_km: TEdit;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    qry_Poly11: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure DoAction(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure combo_CriteriaPropertiesCloseUp(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
  //  procedure FormResize(Sender: TObject);
  //  procedure tree_Selected_BS1DblClick(Sender: TObject);

  private
    FBLPoints: TBLPointArrayF;

    FRegPath: string;

    procedure CheckSites(aMode: Integer);
    procedure GetCheckedSiteIDList(aDataSet: TDataSet; out aIDList: TIDList);
    procedure UpdateAll(aChecked: Boolean);
  public
    procedure View(aCalcRegion_ID: Integer);
    procedure ViewBYPoints(var aBLPoints: TBLPointArrayF);

    procedure GetSiteIDList (aIDList: TIDList);
  end;


//==================================================================
implementation


{$R *.DFM}
//uses dm_Main;

//--------------------------------------------------------------------
procedure Tframe_CalcRegion_add_cells.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
var
  i: Integer;
 // oStringList: TStringList;

//  oStatusItem: TStatusItem;
begin
  inherited;

 
  cxGrid1.Align:=alClient;


 // col_Name1.Caption:=STR_NAME;
 // col_Name_right.Caption:=STR_NAME;


{  tree_Selected_BS.KeyField   :=FLD_DXtree_id;
  tree_Selected_BS.ParentField:=FLD_DXtree_parent_id;

  tree_All_BS.KeyField   :=FLD_DXtree_id;
  tree_All_BS.ParentField:=FLD_DXtree_parent_id;

  tree_All_BS.Align:=alClient;
}

 // act_Add.Caption:= STR_ACT_ADD;
//  act_Del.Caption:= STR_ACT_DEL;

//  col_Checked.Caption:=' ';
//  col_Lat.Caption:=STR_LAT;
//  col_Lon.Caption:=STR_LON;


//  col_Checked.Width:=25;

{  act_Collapse.Caption      :=STR_ACT_COLLAPSE;
  act_Expand.Caption        :=STR_ACT_EXPAND;
  act_Collapse_right.Caption:=STR_ACT_COLLAPSE;
  act_Expand_right.Caption  :=STR_ACT_EXPAND;
}
//  FIDList:=TIDList.Create;
//  FFoundedSites:=TIDList.Create;

 // dmCalcRegion_add_PMP_View.InitDB (mem_Selected);
 // dmCalcRegion_add_PMP_View.InitDB (mem_All);

///////////  dmCalcRegion_add_View.InitDB (mem_ItemsPMP);

//  tree_All_BS.Align:= alClient;

 // FRegPath:=REGISTRY_FORMS + Name +'\'+ tree_Selected_BS.Name;
//  dx_CheckRegistry (tree_Selected_BS,  FRegPath);
//  tree_Selected_BS.LoadFromRegistry (FRegPath);


  with combo_Criteria.Items do
  begin
    Clear;
    AddObject('��� ������� ������ ������',    Pointer(0));
    AddObject('�� ������� (��)',              Pointer(1));
    AddObject('�� ������� - ��� ��',          Pointer(2));
    AddObject('�� ������� - �� ������ ������',Pointer(3));
  end;

  combo_Criteria.ItemIndex:=0;


  FRegPath:=REGISTRY_COMMON_FORMS+ClassName+'\';


  with TRegIniFile.Create(FRegPath) do
  begin
    ed_Radius_km.Text          := ReadString  ('', ed_Radius_km.Name,     '10');
  //  combo_Criteria.ItemIndex := ReadInteger ('', combo_Criteria.Name, 0);
  //  combo_Status.ItemIndex   := ReadInteger ('', combo_Status.Name,   0);

    combo_Criteria.ItemIndex := ReadInteger ('', combo_Criteria.Name, 0);
    combo_Status.ItemIndex   := ReadInteger ('', combo_Status.Name,   0);


    Free;
  end;

  if combo_Criteria.ItemIndex<0 then
    combo_Criteria.ItemIndex :=0;


  combo_CriteriaPropertiesCloseUp (nil);

 // oStringList :=TStringList(combo_Status.Items);

  dmStatus.AssignList('pmp_site_status', combo_Status.Items);

  dmOnega_DB_data.OpenStoredProc (ADOStoredProc1,
     sp_PMP_CalcRegion_PMP_site_select_new,

      [FLD_Project_ID, dmMain.ProjectID
      ]);

end;

//--------------------------------------------------------------------
procedure Tframe_CalcRegion_add_cells.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
var
  i: Integer;
begin
//  FIDList.Free;
//  FFoundedSites.Free;

 // tree_Selected_BS.SaveToRegistry (FRegPath);

  with TRegIniFile.Create(FRegPath) do
  begin
    WriteString  ('', ed_Radius_km.Name,   ed_Radius_km.Text);
    WriteInteger ('', combo_Criteria.Name, combo_Criteria.ItemIndex);

    WriteInteger ('', combo_Status.Name,   combo_Status.ItemIndex);

    Free;
  end;

  inherited;

 (* dmObjects.ItemsByFieldName (otPmpSite, FLD_STATUS, combo_Status.Properties.Items);

  with combo_Status.Properties  do
    for i := 0 to Items.Count - 1 do
      Items[i]:=Items[i]+ Format(' [%d]', [i]);
*)

end;



//--------------------------------------------------------------------
procedure Tframe_CalcRegion_add_cells.combo_CriteriaPropertiesCloseUp(Sender: TObject);
//--------------------------------------------------------------------
begin
{
    Add('��� ������� ������ ������');
    Add('�� ������� (��)');
    Add('�� ������� - ��� ��');
    Add('�� ������� - �� ������ ������');
}

//  ed_Radius.Visible:= combo_Criteria.ItemIndex=-1;

 // combo_Status.Visible:= combo_Criteria.ItemIndex=-1;;

  case combo_Criteria.ItemIndex of
    0,
   -1: begin   ed_Radius_km.Visible:= false;  combo_Status.Visible:= false;  end;
    1: begin   ed_Radius_km.Visible:= true;   combo_Status.Visible:= false;  end;

    2,
    3: begin   ed_Radius_km.Visible:= false;  combo_Status.Visible:= true;   end;
  end;

end;

//--------------------------------------------------------------------
procedure Tframe_CalcRegion_add_cells.ViewBYPoints(var aBLPoints:
    TBLPointArrayF);
//--------------------------------------------------------------------
begin
  FBLPoints := aBLPoints;

  CheckSites(0);
end;


procedure Tframe_CalcRegion_add_cells.UpdateAll(aChecked: Boolean);
begin
  db_UpdateAllRecords_ (adoStoredProc1, FLD_CHECKED, aChecked);
end;


//--------------------------------------------------------------------
procedure Tframe_CalcRegion_add_cells.DoAction(Sender: TObject);
//--------------------------------------------------------------------
begin
  //-------------------------------------------------------------------
  if Sender=act_Add_All then begin
  //-------------------------------------------------------------------
    UpdateAll(True);
  end else

  //-------------------------------------------------------------------
  if Sender=act_Del_All then begin
  //-------------------------------------------------------------------
    UpdateAll(False);
  end;

  if (Sender=act_Add_By_Criteria) then
    CheckSites(combo_Criteria.ItemIndex);
end;


//--------------------------------------------------------------------
procedure Tframe_CalcRegion_add_cells.GetSiteIDList (aIDList: TIDList);
//--------------------------------------------------------------------
begin
  GetCheckedSiteIDList(ADOStoredProc1, aIDList);
end;


//----------------------------------------------------------------
procedure Tframe_CalcRegion_add_cells.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//----------------------------------------------------------------
begin
 // act_Add_By_Criteria.Enabled:=
 //   (combo_Criteria.ItemIndex = 1) and (ed_Radius.Text <> '');

(*
  if (combo_Criteria.ItemIndex = 1) and (ed_Radius.Text = '') then
    act_Add_By_Criteria.Enabled:= false
  else
    act_Add_By_Criteria.Enabled:= true;
*)

end;

//--------------------------------------------------------------------
procedure Tframe_CalcRegion_add_cells.CheckSites(aMode: Integer);
//--------------------------------------------------------------------
var
  b: Boolean;
  blPoint: TBLPoint;
  dDistance_km: Double;
  i: Integer;
  oBLPointFixed: TBLPoint;
  oCurrentBLPoint: TBLPoint;
begin
  case aMode of
    1:  //AddObject('�� ������� (��)', Pointer(1));
        oBLPointFixed:= db_ExtractBLPoint(ADOStoredProc1);
  end;

  ADOStoredProc1.DisableControls;
  ADOStoredProc1.First;

  with ADOStoredProc1 do
    while not EOF do
    begin
      blPoint:=db_ExtractBLPoint(ADOStoredProc1);


      case aMode of
        0: //AddObject('��� ������� ������ ������', Pointer(0));
           begin
          //   b:= (FieldByName(FLD_STATUS_NAME).AsString = combo_Status.Text);

         ///   if  dmPMP_site.GetSectorCount (qry_PMP_Sites[FLD_ID]) > 0 then
             b:= geo_IsBLPointInBLPolygon(blPoint, FBLPoints);
            //   db_UpdateRecord(ADOStoredProc1, FLD_CHECKED, True);
          end;

        1: //AddObject('�� ������� (��)', Pointer(1));
          begin
          //  oCurrentBLPoint:= db_ExtractBLPoint(ADOStoredProc1);
            dDistance_km:= geo_Distance_km(oBLPointFixed, blPoint);

            b:= (dDistance_km < AsFloat(ed_Radius_km.Text));
          end;

        2: //AddObject('�� ������� - ��� ��', Pointer(2));
           b:= (FieldByName(FLD_STATUS_STR).AsString = combo_Status.Text);

        3: //AddObject('�� ������� - �� ������ ������', Pointer(3));
           begin
             b:= geo_IsBLPointInBLPolygon(blPoint, FBLPoints);
             b:= b and (FieldByName(FLD_STATUS_STR).AsString = combo_Status.Text);
           end;
      end;

   //   b:= (FieldByName(FLD_STATUS_NAME).AsString = combo_Status.Text);
      db_UpdateRecord(ADOStoredProc1, FLD_CHECKED, b);

      Next;
    end;

  ADOStoredProc1.First;
  ADOStoredProc1.EnableControls;

end;


//-------------------------------------------------------------------
procedure Tframe_CalcRegion_add_cells.GetCheckedSiteIDList(aDataSet: TDataSet;
    out aIDList: TIDList);
//-------------------------------------------------------------------
begin
  aDataSet.First;
  aIDList.Clear;

  with aDataSet do
    while not EOF do
  begin
    if FieldByName(FLD_CHECKED).AsBoolean then
      aIDList.AddID(FieldByName(FLD_ID).AsInteger);

    Next;
  end;
end;

//----------------------------------------------------------------
procedure Tframe_CalcRegion_add_cells.View(aCalcRegion_ID: Integer);
//----------------------------------------------------------------
begin
  dmOnega_DB_data.OpenStoredProc (ADOStoredProc1,
     'sp_PMP_CalcRegion_PMP_site_select_new',

      [FLD_ID, aCalcRegion_ID]);

end;


end.


