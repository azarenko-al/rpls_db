unit fr_CalcRegion_inspector;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, rxPlacemnt, StdCtrls, ExtCtrls, Math,

  fr_DBInspector_Container,

//  fr_Object_inspector,

  dm_CalcModel,
  dm_CalcRegion,
  dm_Terminal,

  u_func,
  u_db,
  u_dlg,

  u_const_db,
  u_const_msg,
  u_func_msg,

  u_types
  ;

type

  Tframe_CalcRegion_inspector = class(Tframe_DBInspector_Container)
  private
    procedure DoOnFieldChanged (Sender: TObject; aFieldName: string; aNewValue: Variant); override;

    procedure DoOnPost(Sender: TObject); override;
  public
    procedure View (aID: integer); override;

    procedure PostEditor();
  end;


//====================================================================
implementation {$R *.dfm}
//====================================================================


//-------------------------------------------------------------------
procedure Tframe_CalcRegion_inspector.DoOnFieldChanged (Sender: TObject; aFieldName: string;
                                                aNewValue: Variant);
//-------------------------------------------------------------------
var
  rec: TdmTerminalTypeAddRec;
  aK0,aK0_open,aK0_closed: double;

begin
  //------------------------------
  if Eq(aFieldName, FLD_CALC_MODEL_ID) then
  //------------------------------
    if dmCalcModel.GetK0_open_close (aNewValue, aK0,aK0_open,aK0_closed) then
    begin
        SetFieldValue (FLD_K0,        aK0);
        SetFieldValue (FLD_K0_OPEN,   aK0_OPEN);
        SetFieldValue (FLD_K0_CLOSED, aK0_CLOSED);
    end;


  //------------------------------
  if Eq(aFieldName, FLD_TERMINAL_ID) then
  //------------------------------
    if dmTerminalType.GetInfo (aNewValue, rec) then
    begin
        SetFieldValue (FLD_TERMINAL_HEIGHT, rec.Height);
        SetFieldValue (FLD_TERMINAL_POWER_W, rec.Power_W);
        SetFieldValue (FLD_TERMINAL_SENSITIVITY,  rec.Sensitivity);

        SetFieldValue (FLD_TERMINAL_GAIN,  rec.Gain);
        SetFieldValue (FLD_TERMINAL_LOSS,  rec.Loss);


    end;
end;


procedure Tframe_CalcRegion_inspector.DoOnPost(Sender: TObject);
begin
 // inherited;
  g_EventManager.postEvent_ (WM_DATA_CHANGED, []);

//  PostMessage (Application.Handle, WM_DATA_CHANGED, 0,0);


end;


procedure Tframe_CalcRegion_inspector.PostEditor;
begin
  Post;
end;


procedure Tframe_CalcRegion_inspector.View(aID: integer);
begin
  PrepareViewForObject (OBJ_PMP_CALC_REGION);

  inherited View(aID);
end;

end.
