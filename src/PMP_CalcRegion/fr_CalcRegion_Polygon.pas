unit fr_CalcRegion_Polygon;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  cxControls, cxGridCustomView, cxClasses, cxGridLevel, cxGrid,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView,

  dm_Onega_DB_data,

  dm_Main,

  u_const_db,

  u_const_str,

  u_func,
  u_db,
  u_Geo, DB, ADODB

  ;

type
  Tframe_CalcRegion_Polygon = class(TForm)
    ds_Points: TDataSource;
    ADOQuery1: TADOQuery;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    col_Lat: TcxGridDBColumn;
    col_Lon: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
  private

  public
    procedure View (aID: integer);

  end;



//==================================================================
// implementation
//==================================================================
implementation  {$R *.DFM}


procedure Tframe_CalcRegion_Polygon.FormCreate(Sender: TObject);
begin
  inherited;

 // col_N.Caption  :=STR_RECNO;
  col_Lat.Caption:=STR_LAT_PULKOVO;
  col_Lon.Caption:=STR_LON_PULKOVO;

  cxGrid1.Align:=alClient;

 // ADOQuery1.Connection := dmMain.ADOConnection;

end;

//-------------------------------------------------------------------
procedure Tframe_CalcRegion_Polygon.View(aID: integer);
//-------------------------------------------------------------------
begin
  dmOnega_DB_data.OpenQuery (ADOQuery1,
    Format('SELECT * FROM %s WHERE pmp_calc_region_id=%d',
      [VIEW_PMP_CALCREGION_POLYGONS, aID]));

end;


end.