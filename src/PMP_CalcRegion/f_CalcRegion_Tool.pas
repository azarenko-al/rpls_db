unit f_CalcRegion_Tool;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, ExtCtrls, rxPlacemnt, ComCtrls, ToolWin, StdCtrls,

  dm_CalcRegion,
  dm_Cell,
  dm_Site,
  dm_Main,

  u_classes,

  f_map_Tool_base,

  u_func_msg,
  u_const_str,
  u_const_msg,
  u_const_db,
  u_db,
  u_func,
  u_geo,
  u_dlg,
  u_matrix

  ;

type

  Tfrm_CalcRegion_Tool = class(Tfrm_Map_Tool_base)
    qry_Temp: TADOQuery;
    bar_Region: TStatusBar;
    ListView1: TListView;
    Bevel1: TBevel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

  private
    FID: integer;
                   
    FCellLayersIDList: TIDList;
    FLayerCreated: boolean;

    FkgrMatrix : TkgrMatrix;
    FkupMatrix : TkupMatrix;

    procedure OpenKupMatrix;

  protected
     procedure SetMapCursor; override;
  public

     class procedure CreateForm(aID: integer);

     class function CreateSingleForm: Tfrm_CalcRegion_Tool;

  end;


//=======================================================
implementation {$R *.DFM}
//=======================================================


//-------------------------------------------------------
class function Tfrm_CalcRegion_Tool.CreateSingleForm: Tfrm_CalcRegion_Tool;
//-------------------------------------------------------
var oForm: TForm;
begin
{
  oForm:=IsFormExists (Tfrm_CalcRegion_Tool.ClassName);

  if not Assigned(oForm) then
    oForm:=Tfrm_CalcRegion_Tool.Create(Application.MainForm);
 }
 // oForm.Show;

end;


//-------------------------------------------------------
class procedure Tfrm_CalcRegion_Tool.CreateForm(aID: integer);
//-------------------------------------------------------
var oForm: Tfrm_CalcRegion_Tool;
begin
  oForm:=Tfrm_CalcRegion_Tool (IsFormExists (Tfrm_CalcRegion_Tool.ClassName));

  if not Assigned(oForm) then
    oForm:=Tfrm_CalcRegion_Tool.Create(Application);

  with oForm do
  begin
    FID:=aID;

    Screen.Cursor:=crHourGlass;
    dmOnega_DB_data.OpenQuery(qry_Temp, 'SELECT * FROM ' + VIEW_CELLS_LOCATION +
                           ' WHERE calc_region_id=:calc_region_id' ,
                           [FLD_CALC_REGION_ID, FID   ]);
    Screen.Cursor:=crDefault;

    bar_Region.Panels[1].Text:=Format('%s', [dmCalcRegion.GetNameByID(FID)  ]);

    OpenKupMatrix;

    Show;
  end;
end;

//-------------------------------------------------------
procedure Tfrm_CalcRegion_Tool.FormCreate(Sender: TObject);
//-------------------------------------------------------
begin
  inherited;
  ListView1.Align:= alClient;

  FkgrMatrix := TkgrMatrix.Create;
  FkupMatrix := TkupMatrix.Create;

  FormStyle:=fsStayOnTop;

  qry_Temp.Connection:=dmMain.ADOConnection;

  FCellLayersIDList:=TIDList.Create;

  Caption:='������� �������';
end;


//-------------------------------------------------------
procedure Tfrm_CalcRegion_Tool.FormDestroy(Sender: TObject);
//-------------------------------------------------------
begin
  PostEvent(WE_MAP_DESTROY_USER_LAYER, [app_Par(PAR_ID, 1)]);

  FreeAndNil(FkgrMatrix);
  FreeAndNil(FkupMatrix);

  FreeAndNil(FCellLayersIDList);

  inherited;
end;

//-------------------------------------------------------
procedure Tfrm_CalcRegion_Tool.OpenKupMatrix;
//-------------------------------------------------------
var
  S: string;
begin
  FLayerCreated:=false;

  dmCalcRegion.GetCellLayerIDs(FID, FCellLayersIDList);

  if (FCellLayersIDList.Count <> 0) then
  begin
    s:=dmCalcRegion.GetFileName (FID, FCellLayersIDList[0].ID, ftKUP);
    if not FkupMatrix.LoadFromFile (s) then
      g_Log.Add ( '����� �������'+dmCalcRegion.GetNameByID(FID)+
                        '. �� ������� KUP �������');

    s:=dmCalcRegion.GetFileName (FID, FCellLayersIDList[0].ID, ftKGR);
    if not FkgrMatrix.LoadFromFile (s) then
      g_Log.Add ( '����� �������'+dmCalcRegion.GetNameByID(FID)+
                        '. �� ������� KGR �������');
  end
  else
    g_Log.Add ( '����� �������'+dmCalcRegion.GetNameByID(FID)+
                      '. ��� ��������� ����');
end;


//-------------------------------------------------------
procedure Tfrm_CalcRegion_Tool.SetMapCursor;
//-------------------------------------------------------
var
  fDistance: double;
  iValue: Integer;
  iCell, iSite, iRow, iCol: integer;
  bl: TBLPoint;
  oListItem: TListItem;
begin
  StatusBar1.Panels[0].Text:=geo_FormatBLPoint(blCursor);
  FkgrMatrix.FindPointBL (blCursor, iCell);


  ListView1.Items.Clear;

  qry_Temp.Locate(FLD_ID, iCell, []);

  bl.B:= qry_Temp[FLD_LAT];
  bl.L:= qry_Temp[FLD_LON];
  fDistance:=geo_Distance (bl, blCursor)/1000;
  oListItem:=ListView1.Items.Add;
  oListItem.Caption:=FCellLayersIDList[0].Name;
  oListItem.SubItems.Add (Format('%s : %s', [qry_Temp[FLD_Site_Name], qry_Temp[FLD_Cell_Name]]));
  oListItem.SubItems.Add (Format('%1.1f', [fDistance]));

  if FkupMatrix.FindPointBL (blCursor, iValue) then
  begin
    oListItem.SubItems.Add (Format('%d',[iValue] ));

    PostEvent(WE_MAP_ADD_USER_LINE,
        [app_Par(PAR_LAT1, qry_Temp[FLD_LAT]),
         app_Par(PAR_LON1, qry_Temp[FLD_LON]),
         app_Par(PAR_LAT2, blCursor.B),
         app_Par(PAR_LON2, blCursor.L)
        ]);

    PostEvent(WE_MAP_ADD_USER_TEXT,
        [app_Par(PAR_name, Format('%d dBm', [iValue])),
         app_Par(PAR_LAT,  blCursor.B),
         app_Par(PAR_LON,  blCursor.L)
        ]);

    PostEvent(WE_MAP_DRAW_USER_LAYER);
  end
  else
    PostEvent(WE_MAP_DESTROY_USER_LAYER, [app_Par(PAR_ID, 1)]);
end;


end.