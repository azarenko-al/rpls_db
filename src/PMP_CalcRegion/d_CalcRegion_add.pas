unit d_CalcRegion_add;

interface
{$I ver.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rxPlacemnt, ActnList,     StdCtrls, ExtCtrls,    Registry, cxPropertiesStore,
  cxButtonEdit, cxVGrid, cxControls, cxInplaceContainer,

  dm_User_Security,

  dm_MapEngine_store,

  u_cx_VGrid,

  d_Wizard_Add_with_params,

  dm_Main,

  I_Shell,
  u_Shell_new,

  I_MapView,
  
 // I_MapEngine,
 // dm_MapEngine,

  I_MapAct,
//  dm_Act_Map,

  dm_CalcRegion,

  dm_Terminal,

  dm_PMP_site,
  dm_CalcRegion_PMP_site,

  u_func,
  u_db,
  u_dlg,     
  u_func_msg,

  u_const_db,
  u_const_msg,
  u_const_str,

  u_classes,
  u_types,
  u_Geo,

  fr_CalcRegion_add_cells,

   ComCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinsDefaultPainters, cxStyles, cxEdit, cxSpinEdit,
  cxTextEdit
  ;

type
  TmyPOINTS = record
    x: shortint;
    y: shortint;
  end;

  Tdlg_PMP_CalcRegion_add = class(Tdlg_Wizard_add_with_params)
    act_GetMetrics: TAction;
    cb_RegionSizeByBSRadius: TCheckBox;
    cxVerticalGrid1: TcxVerticalGrid;
    row_CalcModel: TcxEditorRow;
    row_Step: TcxEditorRow;
    row_ClutterModel: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    cxVerticalGrid1EditorRow4: TcxEditorRow;
    row_KUP: TcxEditorRow;
    row_KMO: TcxEditorRow;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;
    row_Terminal: TcxEditorRow;
    row_Terminal_H: TcxEditorRow;
    row_Terminal_power_W: TcxEditorRow;
    row_Terminal_sense_dBM: TcxEditorRow;
    row_Terminal_loss: TcxEditorRow;
    cxVerticalGrid1EditorRow12: TcxEditorRow;
    TabSheet_Cells: TTabSheet;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
//    procedure DoOnButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure act_OkExecute(Sender: TObject);
 //zzzz ..  procedure GSPages1Click(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
     procedure row_CalcModel1EditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    Fframe_CalcRegion_add_cells: Tframe_CalcRegion_add_cells;
//    Fframe_Polygon: Tframe_Polygon;

    FRec: TdmCalcRegion_pmp_AddRec;
    FBLPointArrF: TBLPointArrayF;
    FID: integer;
    FDrawed: boolean;

    FIDList: TIDList;

    FIMapView: IMapViewX;

//    FMetricsType: (mtByMap, mtBySiteRadius);

    procedure LoadTerminalParams;
    procedure UpdateMetrics();
    procedure DrawRegionOnMapUserLayer();

  protected
    procedure Append;
    procedure SaveToRec;

    procedure WindowMove (var Message: TmyPOINTS); message WM_MOVE;

  public

    class function ExecDlg (
                            var aBLPointArrF: TBLPointArrayF;
                            aSiteIDList: TIDList
                            ): integer;
  end;


//====================================================================
// implementation
//====================================================================
implementation {$R *.DFM}

const
  STR_ON_MAP    = '����� �� �����';
  STR_ON_RADIUS = '�� ������� �������';


//--------------------------------------------------------------------
class function Tdlg_PMP_CalcRegion_add.ExecDlg (
                                            var aBLPointArrF: TBLPointArrayF;    
                                            aSiteIDList: TIDList

                                            ): integer;
//--------------------------------------------------------------------
begin
{
  if dmUser_Security.ProjectIsReadOnly then
  begin
    ShowMessage('���������� �������� ������. ������ ������ ��� ������.');
    Result:=0;
    Exit;
  end;
 }


  with Tdlg_PMP_CalcRegion_add.Create(Application) do
  try
   // FObjName:=aObjName;

    FIMapView:=IActiveMapView;

//    IMapView:=aIMapView;
    FBLPointArrF := aBLPointArrF;

//    FRec.FolderID := aFolderID;
//    FSiteIDs   := aSiteIDList;

  //  FRec.Type_ := aType;

    ed_Name_.Text:=dmCalcRegion_pmp.GetNewName();// + IIF(FRec.Type_=crtGSM, '-GSM','-PMP');

//    row_Folder.Text:=dmFolder.GetPath (FRec.FolderID);

  //  Fframe_CalcRegion_add_cells.SetObjectName (aObjName);

   // Fframe_CalcRegion_add_cells.CalcRegionType:=aType;

    Fframe_CalcRegion_add_cells.ViewBYPoints (FBLPointArrF); //, aSiteIDList, aType);
//    Fframe_Polygon.ViewPoints (FBLPointArr);

////////////////    DrawRegionOnMapUserLayer ();

    FDrawed:= true;
{
    if aType of
      0: ;
      1: ;
      2: ;
      3: ;
    end;}

{    if FBLPointArr.Count>0
      then begin ed_Size_type.Text:= STR_ON_MAP;    FMetricsType:=mtByMap; end
      else begin ed_Size_type.Text:= STR_ON_RADIUS; FMetricsType:=mtBySiteRadius; end;
}


    if (ShowModal=mrOk) then
      Result:=FID
    else
      Result:=0;


  finally
    if Assigned(FIMapView) then
      FIMapView.UserLayerDestroy;

//    PostEvent(WE_MAP_DESTROY_USER_LAYER);

    Free;
  end;
end;

//--------------------------------------------------------------------
procedure Tdlg_PMP_CalcRegion_add.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

//  Assert(Assigned(IShell));

  SetActionName (STR_DLG_ADD_CALC_REGION);

  lb_Name.Caption:=STR_NAME;
//  row_Folder.Caption:=STR_FOLDER;

//  ts_Params.Caption :='���������';
  TabSheet_Cells.Caption  :='������� �������';

//  ts_Cells.Caption  :='������� �������';
  //ts_Metrics.Caption:='�������';

  row_Terminal_power_W.Properties.Caption:= STR_POWER_W;
  row_Terminal_sense_dBM.Properties.Caption:= STR_Sensitivity_dBm;
  row_Terminal_loss.Properties.Caption := STR_LOSS;

  CreateChildForm(Tframe_CalcRegion_add_cells, Fframe_CalcRegion_add_cells, TabSheet_Cells);


//  AddComponentProp(Page, 'ActivePage');
  RestoreFrom();


  with TRegIniFile.Create(FRegPath) do
  begin
//    GSPages1.ActivePageIndex:=ReadInteger('', 'ActivePageIndex', 0);

    FRec.TerminalID    :=ReadInteger('', FLD_Terminal_ID, 0);
    FRec.ClutterModelID:=ReadInteger('', FLD_CLUTTER_MODEL_ID, 0);
    FRec.CalcModelID   :=ReadInteger('', FLD_CALC_MODEL_ID, 0);
    FRec.KupID         :=ReadInteger('', FLD_KUP_ID, 0);
    FRec.KmoId         :=ReadInteger('', FLD_KMO_ID, 0);
 //   FRec.HandoverID    :=ReadInteger('', FLD_HANDOVER_ID, 0);

    Free;
  end;

(*  row_Terminal.Text    :=gl_DB.GetNameByID(TBL_TerminalType, FRec.TerminalID);
  row_ClutterModel.Text:=gl_DB.GetNameByID(TBL_CLUTTERMODEL, FRec.ClutterModelID);
  row_CalcModel.Text   :=gl_DB.GetNameByID(TBL_CALCMODEL,    FRec.CalcModelID);

  row_KUP.Text         :=gl_DB.GetNameByID(TBL_ColorSchema,  FRec.KupID);
  row_KMO.Text         :=gl_DB.GetNameByID(TBL_ColorSchema,  FRec.KmoId);
 // row_Handover.Text    :=gl_DB.GetNameByID(TBL_COLOR_SCHEMA,  FRec.HandoverID);
*)

  row_Terminal.Properties.Value    :=gl_DB.GetNameByID(TBL_TerminalType, FRec.TerminalID);
  row_ClutterModel.Properties.Value:=gl_DB.GetNameByID(TBL_CLUTTERMODEL, FRec.ClutterModelID);
  row_CalcModel.Properties.Value   :=gl_DB.GetNameByID(TBL_CALCMODEL,    FRec.CalcModelID);
  row_KUP.Properties.Value         :=gl_DB.GetNameByID(TBL_ColorSchema,  FRec.KupID);
  row_KMO.Properties.Value         :=gl_DB.GetNameByID(TBL_ColorSchema,  FRec.KmoId);

 // row_Handover.Text    :=gl_DB.GetNameByID(TBL_COLOR_SCHEMA,  FRec.HandoverID);


  LoadTerminalParams();

  FDrawed:= false;


  FIDList:=TIDList.Create;

  cx_InitVerticalGrid (cxVerticalGrid1);


 // FSiteIDList:=TIDList.Create;

end;

//--------------------------------------------------------------------
procedure Tdlg_PMP_CalcRegion_add.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin

  with TRegIniFile.Create(FRegPath) do
  begin
  //  WriteInteger('', 'ActivePageIndex', GSPages1.ActivePageIndex);
     
    WriteInteger('', FLD_Terminal_ID,      FRec.TerminalID);
    WriteInteger('', FLD_CALC_MODEL_ID,    FRec.CalcModelID);
    WriteInteger('', FLD_CLUTTER_MODEL_ID, FRec.ClutterModelID);
    WriteInteger('', FLD_KUP_ID,           FRec.KupID);
    WriteInteger('', FLD_KMO_ID,           FRec.KmoId);


 //   WriteInteger('', FLD_HANDOVER_ID,      FRec.HandoverID);

    Free;
  end;

  FreeAndNil(FIDList);

 // Fframe_CalcRegion_add_cells.Free;
//  Fframe_Polygon.Free;

 // FSiteIDList.Free;

  inherited;
end;

//--------------------------------------------------------------------
procedure Tdlg_PMP_CalcRegion_add.DrawRegionOnMapUserLayer();
//--------------------------------------------------------------------
var
  i: Integer;
begin

  if Assigned(FIMapView) then
    if FBLPointArrF.Count > 0 then
    begin
      FIMapView.Map_Add_user_Poly(FBLPointArrF, clRed);
      FIMapView.UserLayerDraw;
    end;

end;


//--------------------------------------------------------------------
procedure Tdlg_PMP_CalcRegion_add.SaveToRec;
//--------------------------------------------------------------------
begin



  FRec.NewName :=ed_Name_.Text;
  FRec.PointArr1:=geo_BLPointArrayF_to_BLPointArray(FBLPointArrF);

  FRec.CalcStep:=AsInteger(row_step.Properties.Value);

  FRec.Terminal.Height :=AsFloat(row_terminal_H.Properties.Value);
  FRec.Terminal.Power_W:=AsFloat(row_Terminal_power_W.Properties.Value);
  FRec.Terminal.Sensitivity:=AsFloat(row_Terminal_sense_dBM.Properties.Value);
  FRec.Terminal.Loss :=AsFloat(row_Terminal_loss.Properties.Value);

end;

//--------------------------------------------------------------------
procedure Tdlg_PMP_CalcRegion_add.Append;
//--------------------------------------------------------------------
var
  d: double;
  I: Integer;

begin
  if cb_RegionSizeByBSRadius.Checked then
    UpdateMetrics();

    
  if FBLPointArrF.Count=0 then
  begin
    ShowMessage('������� �� ������!');

    ModalResult:=mrNone;
    Exit;
  end;


  FRec.ProjectID:=dmMain.ProjectID;

  FRec.NewName :=ed_Name_.Text;
  FRec.PointArr1:= geo_BLPointArrayF_to_BLPointArray( FBLPointArrF);

  FRec.CalcStep:=AsInteger(row_step.Properties.Value);

  FRec.Terminal.Height      :=AsFloat(row_terminal_H.Properties.Value);
  FRec.Terminal.Power_W     :=AsFloat(row_Terminal_power_W.Properties.Value);
  FRec.Terminal.Sensitivity :=AsFloat(row_Terminal_sense_dBM.Properties.Value);
  FRec.Terminal.Loss        :=AsFloat(row_Terminal_loss.Properties.Value);


  {  if Length(FBLPointArr)>0 then
    FRec.PointArr := FBLPointArr
  else
    case FRec.Type_ of
      crtGSM: FRec.PointArr := dmSite.GetBLRect_for_Sites(FSiteIDList);
      crtPMP: FRec.PointArr := dmPMP_site.GetBLRect_for_PMP(FSiteIDList);
    end;    }

  Fframe_CalcRegion_add_cells.GetSiteIDList (FIDList); //,  FRec.Type_ );

//  if FIDList.Count>0 then
 // begin
//    Fframe_CalcRegion_add_cells.GetSiteBLPointArr(oBLPointArr);


 {   if not dmUserRegion.BLPosInUserRegion(oBLPointArr) then
    begin
      MsgDlg('������������ ���� ��� �������� �������');
      ModalResult:= mrNone;
      exit;
    end;
}

 // end;

  Assert( Length(FRec.PointArr1)>0);


  FID:=dmCalcRegion_pmp.Add (FRec);

  dmCalcRegion_pmp_site.AddSiteList (FID, FIDList); //, OBJ_PMP_SITE);

//
//  case  FRec.Type_  of
//  //  crtGSM: dmCalcRegion_site.AddSiteList (FID, FIDList, OBJ_SITE);
//
////    crtPMP: dmCalcRegion_site.AddSiteList (FID, FIDList); //, OBJ_PMP_SITE);
//    crtPMP:
//  else
//    raise Exception.Create('');
//  end;
//

 // Assert(Assigned(IMapEngine));
//  Assert(Assigned(IMapAct));


  if FID>0 then
   // if FRec.Type_=crtPMP then
    begin
      dmMapEngine_store.Feature_Add (OBJ_PMP_CALC_REGION, FID);

      if not dmMapEngine_store.Is_Object_Map_Exists(OBJ_PMP_CALC_REGION) then
         dmMapEngine_store.Open_Object_Map(OBJ_PMP_CALC_REGION);




 //     dmMapEngine.CreateObject (otPmpCalcRegion, FID);

(*      d:= dmMapEngine1.GetPolyArea  (otPmpCalcRegion, FID);
      dmCalcRegion.Update(FID, [db_Par(FLD_AREA, TruncFloat(d))]);
*)
      g_Shell.UpdateNodeChildren_ByGUID (dmCalcRegion_pmp.GetGUIDByID(FID));

  //    SH_PostUpdateNodeChildren (dmCalcRegion.GetGUIDByID(FID));

    //  PostEvent (WE_MAP_ENGINE_CREATE_OBJECT, Integer(otCalcRegion), Result);
   //   dmAct_Map.MapRefresh;
    end;

end;


//-------------------------------------------------------------------
procedure Tdlg_PMP_CalcRegion_add.LoadTerminalParams;
//-------------------------------------------------------------------
var rec: TdmTerminalTypeAddRec;
begin
  if dmTerminalType.GetInfo (FRec.TerminalID, rec) then
  begin
    row_Terminal_H.Properties.Value:= AsFloat(rec.Height);
    row_Terminal_power_W.Properties.Value := AsFloat(rec.Power_W);
    row_Terminal_sense_dBM.Properties.Value := AsFloat(rec.Sensitivity);
    row_Terminal_loss.Properties.Value  := AsFloat(rec.Loss);
  end
end;

//-------------------------------------------------------------------
procedure Tdlg_PMP_CalcRegion_add.act_OkExecute(Sender: TObject);
//-------------------------------------------------------------------
begin
  if Sender=act_Ok then
   Append;
end;


//-------------------------------------------------------------------
procedure Tdlg_PMP_CalcRegion_add.UpdateMetrics ();
//-------------------------------------------------------------------
//var oBLPointArr: TBLPointArray;
begin
//  if FMetricsType = mtBySiteRadius then
//  begin

  Fframe_CalcRegion_add_cells.GetSiteIDList (FIDList);

  if FIDList.Count=0 then
    Exit;


//  case FRec.Type_ of    //
//
//    {$IFDEF gsm}
////    crtGSM: FBLPointArr := dmSite.GetBLRect_for_Sites(FIDList);
//    {$ENDIF}
//
//    crtPMP: dmPmpSite.GetBLRect_for_PMP1(FIDList, FBLPointArr);
//  end;    // case


  dmPmpSite.GetBLRect_for_PMP(FIDList, FBLPointArrF);

//    Fframe_Polygon.ViewPoints (FBLPointArr);
//  end;

  {if Length(FBLPointArr)>0 then
      oBLPointArr := FBLPointArr

    else begin
      Fframe_CalcRegion_add_cells.GetSiteIDList(FSiteIDList,  FRec.Type_ );
    end;  }

 ///////   Fframe_Polygon.ViewPoints (oBLPointArr);
end;

//-------------------------------------------------------------------
procedure Tdlg_PMP_CalcRegion_add.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//-------------------------------------------------------------------
begin
  act_Ok.Enabled:=
    //  (FTerminalID > 0) and
      (FRec.ClutterModelID > 0) and
      (FRec.CalcModelID > 0) and

      (FRec.KupID > 0) and
      (FRec.KmoID > 0) and
    ////  (FRec.HandoverID > 0) and

      (AsFloat(row_Terminal_power_W.Properties.Value) > 0) and
      (AsFloat(row_Terminal_sense_dBM.Properties.Value) <> 0) and
      (AsFloat(row_Terminal_H.Properties.Value) > 0) ;

      // and
      //((FBLPointArr.Count)>0)

end;



//-------------------------------------------------------------------
procedure Tdlg_PMP_CalcRegion_add.WindowMove (var Message: TmyPOINTS);
//-----------------------------------------------------------  --------
begin

//  if FDrawed then
  //  DrawRegionOnMapUserLayer;


end;


procedure Tdlg_PMP_CalcRegion_add.row_CalcModel1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  if cxVerticalGrid1.FocusedRow =row_Terminal then
  begin
    if Dlg_SelectObject_cxButtonEdit(Sender as TcxButtonEdit, otTerminalType, FRec.TerminalID) then
      LoadTerminalParams();
  end else

  if cxVerticalGrid1.FocusedRow =row_CalcModel then
    Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otCalcModel, FRec.CalcModelID) else

  if cxVerticalGrid1.FocusedRow =row_ClutterModeL then
    Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otClutterModel, FRec.ClutterModelID) else

  if cxVerticalGrid1.FocusedRow =row_ClutterModel then
    Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otClutterModel, FRec.ClutterModelID) else

  if cxVerticalGrid1.FocusedRow =row_KUP then
    Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otColorSchema, FRec.KUPID) else

  if cxVerticalGrid1.FocusedRow =row_KMO then
    Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otColorSchema, FRec.KMOID)
  else ;
end;

end.