object frame_CalcRegion_cells: Tframe_CalcRegion_cells
  Left = 774
  Top = 378
  Width = 1106
  Height = 389
  Cursor = crHandPoint
  Caption = 'frame_CalcRegion_cells'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 342
    Width = 1098
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object cxDBTreeList1: TcxDBTreeList
    Tag = 1
    Left = 0
    Top = 154
    Width = 1098
    Height = 188
    Align = alBottom
    Bands = <
      item
        Caption.AlignHorz = taCenter
        Width = 191
      end
      item
        Caption.AlignHorz = taCenter
        Width = 47
      end
      item
        Caption.AlignHorz = taCenter
        Caption.Text = #1057#1077#1082#1090#1086#1088#1072
      end
      item
        Caption.AlignHorz = taCenter
        Caption.Text = #1052#1086#1076#1077#1083#1100' '#1088#1072#1089#1095#1077#1090#1072
      end
      item
        Caption.AlignHorz = taCenter
        Caption.Text = #1040#1085#1090#1077#1085#1085#1072
      end
      item
        Caption.AlignHorz = taCenter
        Caption.Text = #1055#1083#1086#1097#1072#1076#1082#1072
      end
      item
        Visible = False
      end>
    DataController.DataSource = DataSource1
    DataController.ParentField = 'parent_guid'
    DataController.KeyField = 'guid'
    LookAndFeel.Kind = lfFlat
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsBehavior.AutoDragCopy = True
    OptionsBehavior.DragCollapse = False
    OptionsBehavior.ExpandOnDblClick = False
    OptionsBehavior.ExpandOnIncSearch = True
    OptionsBehavior.ShowHourGlass = False
    OptionsCustomizing.BandCustomizing = False
    OptionsCustomizing.BandVertSizing = False
    OptionsCustomizing.ColumnVertSizing = False
    OptionsSelection.HideFocusRect = False
    OptionsSelection.InvertSelect = False
    OptionsSelection.MultiSelect = True
    OptionsView.CellTextMaxLineCount = -1
    OptionsView.Bands = True
    OptionsView.Indicator = True
    OptionsView.SimpleCustomizeBox = True
    ParentColor = False
    PopupMenu = PopupMenu1
    Preview.AutoHeight = False
    Preview.MaxLineCount = 2
    RootValue = -1
    TabOrder = 1
    object col_id: TcxDBTreeListColumn
      Tag = 1
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      DataBinding.FieldName = 'id'
      Options.Editing = False
      Options.Sorting = False
      Width = 29
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 6
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_k0_OPEN: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Caption.Text = 'k0 open'
      DataBinding.FieldName = 'k0_OPEN'
      Options.Editing = False
      Options.Sorting = False
      Width = 45
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__checked: TcxDBTreeListColumn
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.Alignment = taLeftJustify
      Properties.ImmediatePost = True
      Properties.NullStyle = nssUnchecked
      Properties.ReadOnly = False
      Properties.ValueChecked = 'True'
      Properties.ValueGrayed = ''
      Properties.ValueUnchecked = 'False'
      DataBinding.FieldName = 'checked'
      Options.Sorting = False
      Width = 48
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__name1: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'name'
      Options.Editing = False
      Width = 78
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      SortOrder = soAscending
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__Number: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      Caption.Text = #8470
      DataBinding.FieldName = 'Number'
      Options.Editing = False
      Options.Sorting = False
      Width = 20
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__type: TcxDBTreeListColumn
      Tag = 1
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      DataBinding.FieldName = 'type'
      Options.Editing = False
      Options.Sorting = False
      Width = 24
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Cell_Layer_name: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Caption.Text = 'Cell_Layer'
      DataBinding.FieldName = 'CellLayer_name'
      Options.Editing = False
      Options.Sorting = False
      Width = 62
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__calc_radius: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      DataBinding.FieldName = 'calc_radius_km'
      Options.Editing = False
      Options.Sorting = False
      Width = 82
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__power_dBM: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'power_dBM'
      Options.Editing = False
      Options.Sorting = False
      Width = 65
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_THRESHOLD_BER_3: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      DataBinding.FieldName = 'THRESHOLD_BER_3'
      Options.Editing = False
      Options.Sorting = False
      Width = 43
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__freq: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      DataBinding.FieldName = 'tx_freq_MHz'
      Options.Editing = False
      Options.Sorting = False
      Width = 68
      Position.ColIndex = 5
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__AntennaType_Name: TcxDBTreeListColumn
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Caption.ShowEndEllipsis = False
      DataBinding.FieldName = 'AntennaType_Name'
      Options.Editing = False
      Options.Sorting = False
      Width = 104
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 4
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__Height: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'Height'
      Options.Editing = False
      Options.Sorting = False
      Width = 37
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 4
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__Azimuth: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'Azimuth'
      Options.Editing = False
      Options.Sorting = False
      Width = 43
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 4
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__Tilt: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'Tilt'
      Options.Editing = False
      Options.Sorting = False
      Width = 20
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 4
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__Loss: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'Loss'
      Options.Editing = False
      Options.Sorting = False
      Width = 28
      Position.ColIndex = 4
      Position.RowIndex = 0
      Position.BandIndex = 4
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__Band: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Caption.Text = #1044#1080#1072#1087#1072#1079#1086#1085
      DataBinding.FieldName = 'Band'
      Options.Editing = False
      Options.Sorting = False
      Width = 62
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__k0: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Caption.Text = 'ko'
      DataBinding.FieldName = 'k0'
      Options.Editing = False
      Options.Sorting = False
      Width = 30
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__CalcModel_name: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'calc_model_Name'
      Options.Editing = False
      Options.Sorting = False
      Width = 96
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__Color: TcxDBTreeListColumn
      PropertiesClassName = 'TcxColorComboBoxProperties'
      Properties.CustomColors = <>
      Properties.ImmediateDropDown = False
      Properties.ReadOnly = True
      Properties.ShowDescriptions = False
      DataBinding.FieldName = 'color'
      Options.Editing = False
      Options.Sorting = False
      Width = 51
      Position.ColIndex = 6
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__k0_closed: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Caption.Text = 'k0 closed'
      DataBinding.FieldName = 'k0_closed'
      Options.Editing = False
      Options.Sorting = False
      Width = 53
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_THRESHOLD_BER_6: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      DataBinding.FieldName = 'THRESHOLD_BER_6'
      Options.Sorting = False
      Width = 53
      Position.ColIndex = 4
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_xref_id: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'xref_id'
      Options.Editing = False
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 6
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Property_lat: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'Property_lat'
      Width = 85
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 5
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Property_lon: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'Property_lon'
      Width = 75
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 5
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Property_lat_str: TcxDBTreeListColumn
      Caption.Text = #1064#1080#1088#1086#1090#1072
      DataBinding.FieldName = 'Property_lat_str'
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 5
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Property_lon_str: TcxDBTreeListColumn
      Caption.Text = #1044#1086#1083#1075#1086#1090#1072
      DataBinding.FieldName = 'Property_lon_str'
      Width = 89
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 5
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 24
    Top = 84
    object Add1: TMenuItem
      Action = act_Add
    end
    object Del1: TMenuItem
      Action = act_Del
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Check1: TMenuItem
      Action = act_Check
    end
    object UnCheck1: TMenuItem
      Action = act_UnCheck
    end
    object N4: TMenuItem
      Caption = '-'
      Visible = False
    end
    object actClearCalcModel1: TMenuItem
      Action = act_Clear_CalcModel
      Visible = False
    end
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 28
    Top = 20
    object act_Add: TAction
      Caption = 'Add'
      OnExecute = _ActionMenu
    end
    object act_Del: TAction
      Caption = 'Del'
      OnExecute = _ActionMenu
    end
    object act_UnCheck: TAction
      Caption = 'UnCheck'
      Enabled = False
      OnExecute = _ActionMenu
    end
    object act_Check: TAction
      Caption = 'Check'
      Enabled = False
      OnExecute = _ActionMenu
    end
    object act_Grid_Setup: TAction
      Caption = 'act_Grid_Setup'
      Enabled = False
      OnExecute = _ActionMenu
    end
    object act_Clear_CalcModel: TAction
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1084#1086#1076#1077#1083#1080' '#1088#1072#1089#1095#1105#1090#1072
      Enabled = False
      OnExecute = _ActionMenu
    end
    object act_CheckByStatus: TAction
      Caption = #1054#1090#1084#1077#1090#1080#1090#1100' '#1087#1086' '#1089#1090#1072#1090#1091#1089#1091
      Enabled = False
      OnExecute = _ActionMenu
    end
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc1
    Left = 173
    Top = 73
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    AfterPost = ADOQuery1AfterPost
    ProcedureName = 'sp_PMP_CalcRegion_Select_Item'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 6730
      end>
    Left = 176
    Top = 24
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 296
    Top = 24
  end
end
