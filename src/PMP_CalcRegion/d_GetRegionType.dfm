inherited dlg_GetRegionType: Tdlg_GetRegionType
  Left = 1272
  Top = 327
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = 'dlg_GetRegionType'
  ClientHeight = 349
  ClientWidth = 382
  OldCreateOrder = True
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 314
    Width = 382
    TabOrder = 1
    inherited Bevel1: TBevel
      Width = 382
    end
    inherited Panel3: TPanel
      Left = 202
      Width = 180
      inherited btn_Ok: TButton
        Left = 10
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 382
    TabOrder = 0
    inherited Bevel2: TBevel
      Width = 382
    end
    inherited pn_Header: TPanel
      Width = 382
      Visible = False
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 5
    Top = 64
    Width = 372
    Height = 153
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 2
    DesignSize = (
      372
      153)
    object Bevel3: TBevel
      Left = 8
      Top = 88
      Width = 356
      Height = 3
      Anchors = [akLeft, akTop, akRight]
      Shape = bsTopLine
    end
    object rb_Rect: TRadioButton
      Left = 5
      Top = 16
      Width = 113
      Height = 17
      Caption = #1055#1088#1103#1084#1086#1091#1075#1086#1083#1100#1085#1080#1082
      Checked = True
      TabOrder = 0
      TabStop = True
    end
    object rb_Circle: TRadioButton
      Tag = 2
      Left = 5
      Top = 64
      Width = 113
      Height = 17
      Caption = #1050#1088#1091#1075
      TabOrder = 1
    end
    object rb_Poly: TRadioButton
      Tag = 1
      Left = 5
      Top = 40
      Width = 113
      Height = 17
      Caption = #1055#1086#1083#1080#1075#1086#1085
      TabOrder = 2
    end
    object cb_GeoRegion: TRadioButton
      Tag = 3
      Left = 5
      Top = 96
      Width = 236
      Height = 17
      Caption = #1043#1077#1086#1075#1088#1072#1092#1080#1095#1077#1089#1082#1080#1081' '#1088#1077#1075#1080#1086#1085
      TabOrder = 3
    end
    object DBLookupComboBox1: TDBLookupComboBox
      Left = 24
      Top = 116
      Width = 249
      Height = 21
      DropDownRows = 20
      KeyField = 'id'
      ListField = 'name'
      ListSource = DataSource1
      TabOrder = 4
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 265
  end
  inherited FormStorage1: TFormStorage
    Left = 236
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 207
  end
  object DataSource1: TDataSource
    DataSet = qry_Temp
    Left = 88
    Top = 240
  end
  object qry_Temp: TADOQuery
    Parameters = <>
    Left = 56
    Top = 240
  end
end
