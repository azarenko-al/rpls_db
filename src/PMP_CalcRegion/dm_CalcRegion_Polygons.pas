unit dm_CalcRegion_Polygons;

interface

uses
  Windows, ActiveX, Messages, SysUtils, Classes, Graphics, Controls, Forms, Variants,
  Dialogs, Db, ADODB,

  dm_Onega_DB_data,
  u_log,

  u_const_db,

  u_Geo,
  u_func,
  u_db

  ;

type
  TdmCalcRegion_Polygons = class(TDataModule)
    qry_Poly_points: TADOQuery;
    qry_Temp: TADOQuery;
  private
  public
    procedure Add(aPMP_CalcRegion_ID: Integer; var aPointArr: TBLPointArrayF);

    procedure Get_Points(aCalcRegion_ID: integer; var aBLPoints: TBLPointArrayF);

    function Get_BoundBLRect(aCalcRegion_ID: integer): TBLRect;

  end;


function dmCalcRegion_Polygons: TdmCalcRegion_Polygons;


//==================================================================
implementation {$R *.DFM}

var
  FdmCalcRegion_Polygons: TdmCalcRegion_Polygons;


// ---------------------------------------------------------------
function dmCalcRegion_Polygons: TdmCalcRegion_Polygons;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmCalcRegion_Polygons) then
    FdmCalcRegion_Polygons := TdmCalcRegion_Polygons.Create(Application);

  Result := FdmCalcRegion_Polygons;
end;


//--------------------------------------------------------------------
function TdmCalcRegion_Polygons.Get_BoundBLRect(aCalcRegion_ID: integer): TBLRect;
//--------------------------------------------------------------------
var blPoints: TBLPointArrayF;
begin

  Get_Points(aCalcRegion_ID, blPoints);
//  Result:=geo_DefineRoundBLRect(blPoints);
  Result:=geo_RoundBLPointsToBLRect_F(blPoints);

end;

//--------------------------------------------------------------------
procedure TdmCalcRegion_Polygons.Get_Points(aCalcRegion_ID: integer; var aBLPoints:
    TBLPointArrayF);
//--------------------------------------------------------------------
const
  SQL_SELECT_POLY_POINTS =
    'SELECT * FROM '+ VIEW_PMP_CALCREGION_POLYGONS +
    ' WHERE (pmp_calc_region_id=:id)';
var
  blPoint: TBLPoint;
  iID,iIndex: integer;
  sName: string;
begin
  dmOnega_DB_data.OpenQuery(qry_Poly_points, SQL_SELECT_POLY_POINTS, [FLD_ID, aCalcRegion_ID]);


//  db_OpenQuery (qry_Poly_points, SQL_SELECT_POLY_POINTS, [db_Par (FLD_ID, aCalcRegion_ID)]);

  aBLPoints.Count:=qry_Poly_points.RecordCount;
  iIndex:=0;

  with qry_Poly_points do
    while not EOF do
  begin
    blPoint:=db_ExtractBLPoint (qry_Poly_points);
    aBLPoints.Items[iIndex]:=blPoint;

    Inc(iIndex);
    Next;
  end;

 // Result:=blPoints;
end;


//-----------------------------------------------------
procedure TdmCalcRegion_Polygons.Add(aPMP_CalcRegion_ID: Integer; var aPointArr:
    TBLPointArrayF);
//-----------------------------------------------------
var i: integer;
begin
  Assert(aPointArr.Count<3, 'function TdmPolygons.Add (aPointArr: TBLPointArrayF): integer;  aPointArr.Count<3');

  g_log.Add(Format('���������� ����� ��������: %d', [aPointArr.Count]));

  CursorSQL;

  dmOnega_DB_data.ADOConnection.BeginTrans;

  for i:=0 to aPointArr.Count-1 do
   dmOnega_DB_data.Pmp_CalcRegion_add_point(aPMP_CalcRegion_ID,
          aPointArr.Items[i].B, aPointArr.Items[i].L);

  dmOnega_DB_data.ADOConnection.CommitTrans;

  CursorDefault;

end;


begin

end.