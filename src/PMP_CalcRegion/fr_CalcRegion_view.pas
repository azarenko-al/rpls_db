unit fr_CalcRegion_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, Menus, ComCtrls, ExtCtrls, ToolWin, Grids, DBGrids,
  DBCtrls, StdCtrls,  ImgList, 
  cxPC, cxControls, cxPropertiesStore,
             
  dm_User_Security,

  dm_Onega_DB_data,

  fr_View_base,

//  I_CalcRegion_Pmp,
//  I_CalcRegion,

  fr_CalcRegion_params,
  fr_CalcRegion_cells,

//  fr_CalcRegion_Map,
  fr_CalcRegion_Polygon,
  fr_ClutterModel_Items,

  dm_CalcRegion,

  u_const_str,
  u_const_DB,
  u_types,

  u_func,
  u_dlg,
  u_reg, dxBar, cxBarEditItem, cxClasses, cxLookAndFeels;

type
  Tframe_CalcRegion_View = class(Tframe_View_Base)
    ActionList1: TActionList;
    act_QuickCalc: TAction;
    PageControl1: TPageControl;
    ts_Params: TTabSheet;
    ts_Cells: TTabSheet;
    tsClutters: TTabSheet;
    tsMetrics: TTabSheet;
    Bevel1: TBevel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
 
    procedure act_QuickCalcExecute(Sender: TObject);
    procedure cxPageControl1Change(Sender: TObject);
 //   procedure cxPageControl1PageChanging(Sender: TObject; NewPage: TcxTabSheet; var AllowChange:
    //    Boolean);

  private
 //   FID: integer;
//    FRegionType: TdmCalcRegionType;

    Fframe_CalcRegion_params: Tframe_CalcRegion_params;
    Fframe_CalcRegion_cells:  Tframe_CalcRegion_cells;
 //   Fframe_CalcRegion_Map:    Tframe_CalcRegion_Map;

    Fframe_CalcRegion_Polygon:   Tframe_CalcRegion_Polygon;
    Ffrm_Clutters:    Tframe_ClutterModel_Items;

    procedure SelectPage (aPageIndex: integer);

  public
    procedure View (aID: integer; aGUID: string); override;
  end;


//====================================================================
// implementation
//====================================================================

implementation {$R *.DFM}

uses

  dm_Act_PMP_CalcRegion; // dm_Act_PMP_CalcRegion


//--------------------------------------------------------------------
procedure Tframe_CalcRegion_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  Caption:=STR_CALC_REGION;
  ObjectName:=OBJ_PMP_CALC_REGION;


  TableName:=TBL_PMP_CALCREGION;


  PageControl1.Align:=alClient;
  PageControl1.ActivePageIndex:=0;

 // ts_Map.Caption:=STR_MAP;
  CreateChildForm(Tframe_CalcRegion_params,  Fframe_CalcRegion_Params, ts_Params);
  CreateChildForm(Tframe_CalcRegion_cells,   Fframe_CalcRegion_Cells,  ts_Cells);
  CreateChildForm(Tframe_CalcRegion_Polygon, Fframe_CalcRegion_Polygon, tsMetrics);
  CreateChildForm(Tframe_ClutterModel_Items, Ffrm_Clutters,            tsClutters);


  AddComponentProp(PageControl1, 'ActivePage');
  cxPropertiesStore.RestoreFrom;

end;

//--------------------------------------------------------------------
procedure Tframe_CalcRegion_View.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  PageControl1.OnChange := nil; // dont remove !!!!

  inherited;
end;

//--------------------------------------------------------------------
procedure Tframe_CalcRegion_View.View (aID: integer; aGUID: string);
//--------------------------------------------------------------------
//var
//  bReadOnly: Boolean;
begin
  inherited;

  if not RecordExists (aID) then
    Exit;


 // FID:=aID;

  dmOnega_DB_data.Pmp_CalcRegion_Init(aID);

//  bReadOnly := dmUser_Security.ProjectIsReadOnly;

//  Fframe_CalcRegion_Params.SetReadOnly (bReadOnly);
//  Fframe_CalcRegion_Cells.SetReadOnly (bReadOnly);

//  Fframe_CalcRegion_Polygon
//  Ffrm_Clutters,


        //  FRegionType:=dmCalcRegion.GetRegionType(aID);

(*  case FRegionType of
    crtPMP: Assert(Assigned(ICalcRegionPmp));
 //   crtGSM: Assert(Assigned(ICalcRegion));
  end;

*)
  SelectPage (PageControl1.ActivePageIndex);
end;


//--------------------------------------------------------------------
procedure Tframe_CalcRegion_View.SelectPage (aPageIndex: integer);
//--------------------------------------------------------------------
var
  recInfo: TdmCalcRegion_pmp_InfoRec;
begin
  if aPageIndex in [2,3] then
    dmCalcRegion_pmp.GetInfo1(ID, recInfo);

  case aPageIndex of
    0: Fframe_CalcRegion_params.View(ID);
    1: Fframe_CalcRegion_cells.View(ID);
    2: Ffrm_Clutters.View(recInfo.ClutterModelId);
    3: Fframe_CalcRegion_Polygon.View(ID);

  end;



end;

//--------------------------------------------------------------------
procedure Tframe_CalcRegion_View.act_QuickCalcExecute(Sender: TObject);
//--------------------------------------------------------------------
begin
  if PageControl1.ActivePageIndex = 0 then
    Fframe_CalcRegion_params.PostEditor();
   
//  case FRegionType of
  //  crtPMP:
    dmAct_PMP_CalcRegion.Calc(ID);

//    ICalcRegionPmp.Calc(ID);


//    crtGSM: ICalcRegion.Calc(ID);
//  end;


end;


procedure Tframe_CalcRegion_View.cxPageControl1Change(Sender: TObject);
begin
  if ID>0 then
    SelectPage (PageControl1.ActivePageIndex);
end;



end.

//
//
//procedure Tframe_CalcRegion_View.cxPageControl1PageChanging(Sender: TObject; NewPage: TcxTabSheet;
//    var AllowChange: Boolean);
//begin
////  if NewPage.PageIndex<>cxPageControl1.ActivePageIndex then
// //   SelectPage (NewPage.PageIndex);
//end;