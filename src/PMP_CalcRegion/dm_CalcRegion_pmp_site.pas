unit dm_CalcRegion_pmp_site;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB,

  dm_Onega_DB_data,      

  dm_Main,

  u_const_db,
  u_types,

  u_classes,
  u_Geo,
  u_func,
  u_db
  ;

type
  TdmCalcRegion_pmp_site = class(TDataModule)

  public
    function GetSiteID (aID: integer): integer;

    procedure Add(aCalcRegionID, aPmpSiteID: integer);
    procedure AddSiteList(aCalcRegionID: integer; aSiteIDList: TIDList);
             
  end;

function dmCalcRegion_pmp_site: TdmCalcRegion_pmp_site;


//==================================================================
implementation {$R *.DFM}
//==================================================================

var
  FdmCalcRegion_pmp_site: TdmCalcRegion_pmp_site;


// ---------------------------------------------------------------
function dmCalcRegion_pmp_site: TdmCalcRegion_pmp_site;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmCalcRegion_pmp_site) then
      FdmCalcRegion_pmp_site := TdmCalcRegion_pmp_site.Create(Application);

   Result := FdmCalcRegion_pmp_site;
end;


// -------------------------------------------------------------------
function TdmCalcRegion_pmp_site.GetSiteID (aID: integer): integer;
// -------------------------------------------------------------------
begin
  Result:=gl_DB.GetIntFieldValue (TBL_PMP_CALCREGIONCELLS,
      FLD_PMP_SITE_ID, [db_Par(FLD_ID, aID)]);
end;



//-------------------------------------------------------------------
procedure TdmCalcRegion_pmp_site.AddSiteList(aCalcRegionID: integer; aSiteIDList:
    TIDList);
//-------------------------------------------------------------------
var i: integer;
begin
  for i:=0 to aSiteIDList.Count-1 do
    Add (aCalcRegionID, aSiteIDList[i].ID);
end;

//-------------------------------------------------------------------
procedure TdmCalcRegion_pmp_site.Add(aCalcRegionID, aPmpSiteID: integer);
//-------------------------------------------------------------------
begin
  dmOnega_DB_data.Pmp_CalcRegion_pmp_site_Add(aCalcRegionID,aPmpSiteID);
end;
     

begin

end.
