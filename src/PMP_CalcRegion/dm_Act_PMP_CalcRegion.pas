unit dm_act_PMP_CalcRegion;

interface
{$I ver.inc}

uses
  SysUtils, Classes, Forms, Menus, ActnList, IniFiles, Dialogs,  Windows,

  u_shell_var,
  u_func_msg,

 i_Audit,


 // d_Audit,

//  I_Act_PMP_CalcRegion,

  dm_Act_Map,

  u_vars,
  dm_Onega_DB_data,

  dm_act_Base,

  dm_GeoRegion,
  dm_Folder,

  dm_Main,

//  dm_MapEngine,

  I_Object,
  I_Shell,
  u_Shell_new,

  u_const,
  u_const_str,

 // u_vars,

  dm_MapEngine_store,


  u_const_db,
  u_const_msg,
  u_types,

  u_Geo,
  u_func,

  //u_func_msg,
  u_dlg,
  u_files,

  d_CalcRegion_add,
//  d_FreqPlan_add,

  fr_CalcRegion_view,


//  f_CalcRegion_Tool,
  f_CalcRegion_tool_Best_Server,
  f_CalcRegion_tool_EMP,

  dm_CalcRegion

//  d_CalcRegion_Select_Type,


  ;

  //, ICalcRegionPmpX

type
  TdmAct_PMP_CalcRegion = class(TdmAct_Base) //, IAct_PMP_CalcRegion_X)
    act_Calc: TAction;
    act_BestServer_Tool: TAction;
    act_EmpTool: TAction;
    act_ShowInExplorer: TAction;
    act_Copy: TAction;
    ActionList2: TActionList;
    act_Audit: TAction;
  //  procedure DataModuleDestroy(Sender: TObject);
//    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
//    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure DoAction (Sender: TObject);

  private
    FTempRect: TBLRect;
    FTempPoints: TBLPointArrayF;
    FPointIndex: integer;

    FBLPoints: TBLPointArrayF;
    procedure CheckActionsEnable;
//    FSiteIDs: TIDList;

  //  FTempCalcRegionType: TdmCalcRegionType;

    function ItemDel(aID: integer ; aName: string = ''): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;

    procedure ShowOnMap (aID: integer);

  //  function Get_FreqPlan_from_CalcRegion (aCalcRegionID: integer): integer;

//    procedure DoOnMapRefresh (Sender: TObject);
  protected
    function  GetViewForm  (aOwnerForm: TForm): TForm; override;
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;

// TODO: GetMessage
//  procedure GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean); override;


    function ViewInProjectTree(aID: integer): TStrArray; override;


// TODO: Dlg_Edit
//  function Dlg_Edit(aID: integer): boolean; //override;
    function GetExplorerPath(aID: integer; aGUID: string): string;
  public

    procedure Add (aFolderID: integer); override;

    procedure AddByPoly(var aBLPoints: TBLPointArrayF);
    procedure AddByRect (aBLRect: TBLRect);
    procedure AddByRing(aCenter: TBLPoint; aRadius: double);

    procedure AddByGeoRegionID(aGeoRegionName: WideString);


    procedure Calc (aID: integer);

    class procedure Init;
  end;

var
  dmAct_PMP_CalcRegion: TdmAct_PMP_CalcRegion;

//====================================================================
implementation  {$R *.dfm}



//--------------------------------------------------------------------
class procedure TdmAct_PMP_CalcRegion.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_PMP_CalcRegion) then
  begin
    dmAct_PMP_CalcRegion:=TdmAct_PMP_CalcRegion.Create(Application);

  //  dmAct_PMP_CalcRegion.GetInterface(IAct_PMP_CalcRegion_X, IAct_PMP_CalcRegion);
//    Assert(Assigned(IAct_PMP_CalcRegion));

  end;
end;

//
//
//procedure TdmAct_PMP_CalcRegion.DataModuleDestroy(Sender: TObject);
//begin
////  IAct_PMP_CalcRegion := nil;
// // dmAct_PMP_CalcRegion:= nil;
//
//  inherited;
//end;


//--------------------------------------------------------------------
procedure TdmAct_PMP_CalcRegion.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_PMP_CALC_REGION;

  act_Calc.Caption:='������';
  act_EmpTool.Caption:='���';

//  act_Get_FreqPlan_from_CalcRegion.Caption:='������� ��� �� ������ �������';
  act_ShowInExplorer.Caption:='������� ����� � ��������� � ����������';


  act_Add.Caption:='������� ����� ������� PMP'; // STR_ADD;

  act_Copy.Caption  := STR_ACT_COPY;

  act_Audit.Caption  := STR_Audit;


{
  act_Audit.Enabled:=False;
}

end;



// ---------------------------------------------------------------
procedure TdmAct_PMP_CalcRegion.CheckActionsEnable;
// ---------------------------------------------------------------
begin
  SetActionsExecuteProc
     ([
        act_Calc,
     // act_Get_FreqPlan_from_CalcRegion,
        act_Object_ShowOnMap,
        act_EmpTool,
        act_BestServer_Tool,
        act_ShowInExplorer,
        act_Copy,

        act_Audit

      ],
      DoAction);

end;



//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_PMP_CalcRegion.DoAction (Sender: TObject);
var
  S: string;
  sNewName: string;
  iID: integer;
begin

  if Sender=act_Audit then
    Init_IAuditX.ExecDlg( dmMain.GetConnectionString, TBL_PMP_CalcRegion, FFocusedID) else


{  if Sender=act_Audit then
     dlg_Audit (TBL_PMP_CalcRegion, FFocusedID) else
}

  // -------------------------------------------
  if Sender=act_Copy then
  // -------------------------------------------
  begin
    sNewName:=FFocusedName +' (�����)';

    if InputQuery ('����� ������','������� ����� ��������', sNewName) then
    begin
      iID:=dmOnega_DB_data.PMP_CalcRegion_Copy(FFocusedID, sNewName);

//      dmAntType.Copy (FFocusedID, sNewName);

//      sGUIDg_Obj.ItemByName[OBJ_ANTENNA_TYPE].RootFolderGUID;

      dmMapEngine_store.Feature_Add (OBJ_PMP_CALC_REGION, iID);


      g_Shell.UpdateNodeChildren_ByGUID(GUID_CALC_REGION_PMP);


    end;
  end else

//  if Sender=act_Add_  then Add (FFocusedID) else
//  if Sender=act_Del_  then Del (FFocusedID) else

  //-------------------------------------------
  if Sender=act_Calc then Calc(FFocusedID) else

  //-------------------------------------------
  if Sender=act_Object_ShowOnMap then
    ShowOnMap(FFocusedID) else

  if Sender=act_BestServer_Tool then
    Tfrm_CalcRegion_Tool_Best_Server.ShowWindow (FFocusedID) else
//    Tfrm_CalcRegion_Tool.CreateForm (FFocusedID) else

  if Sender=act_EmpTool then
    Tfrm_CalcRegion_tool_EMP.ShowWindow (FFocusedID) else


  if Sender=act_ShowInExplorer then
  begin
    s:= dmCalcRegion_pmp.GetFileDir(FFocusedID);

    if DirectoryExists(s) then
      ShellExec('explorer.exe', s)
    else
      MsgDlg('���������� �� ������� (��� ��������)');
  end else
  ;

    //Tfrm_CalcRegion_tool_EMP.ShowWindow (FFocusedID) else
{
  //-------------------------------------------
  if Sender=act_Get_FreqPlan_from_CalcRegion then
    Get_FreqPlan_from_CalcRegion(FFocusedID)
    ;
}
end;

//--------------------------------------------------------------------
procedure TdmAct_PMP_CalcRegion.ShowOnMap (aID: integer);
//--------------------------------------------------------------------
var
  r: TBLRect;
begin
  r:=dmCalcRegion_pmp.GetBLRect(aID);

  ShowRectOnMap (r, aID);
end;


//--------------------------------------------------------------------
function TdmAct_PMP_CalcRegion.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
{var
  d: double;}
begin
   //FTempCalcRegionType
 // Assert(Assigned(IMapEngine));


//  Result:=Tdlg_CalcRegion_add.ExecDlg ( FBLPoints, nil, OBJ_PMP_CALC_REGION, aFolderID);
  Result:=Tdlg_PMP_CalcRegion_add.ExecDlg ( FBLPoints, nil);

//  Result:=Tdlg_CalcRegion_add.ExecDlg ( FBLPoints, nil, OBJ_PMP_CALC_REGION, crtPMP, aFolderID);

 { if Result>0 then
  begin
    dmMapEngine1.CreateObject (otPmpCalcRegion, Result);

    d:= dmMapEngine1.GetPolyArea  (otPmpCalcRegion, Result);
    dmCalcRegion.Update(Result, [db_Par(FLD_AREA, TruncFloat(d))]);

    SH_PostUpdateNodeChildren (dmCalcRegion.GetGUIDByID(Result));

  //  PostEvent (WE_MAP_ENGINE_CREATE_OBJECT, Integer(otCalcRegion), Result);
    dmAct_Map.MapRefresh;
  end;
}

end;

//--------------------------------------------------------------------
procedure TdmAct_PMP_CalcRegion.AddByRing(aCenter: TBLPoint; aRadius: double);
//--------------------------------------------------------------------
const POINT_COUNT=20;
begin
  geo_MakeCirclePointsF(aCenter, aRadius, POINT_COUNT, FBLPoints);
  inherited Add (0);
end;


//--------------------------------------------------------------------
procedure TdmAct_PMP_CalcRegion.AddByPoly(var aBLPoints: TBLPointArrayF);
//--------------------------------------------------------------------
begin
  FBLPoints:=aBLPoints;
  inherited Add (0);
end;

//--------------------------------------------------------------------
procedure TdmAct_PMP_CalcRegion.AddByRect (aBLRect: TBLRect);
//--------------------------------------------------------------------
begin
  geo_BLRectToBLPointsF(aBLRect, FBLPoints);
  inherited Add (0);
end;

//--------------------------------------------------------------------
procedure TdmAct_PMP_CalcRegion.Add (aFolderID: integer);
//--------------------------------------------------------------------
begin
//  FBLPoints:=nil;
  FBLPoints.Count:=0;
  inherited Add (aFolderID);
end;

//--------------------------------------------------------------------
function TdmAct_PMP_CalcRegion.ItemDel(aID: integer ; aName: string = ''):
    boolean;
//--------------------------------------------------------------------
begin
 // Assert(Assigned(IMapEngine));
 // Assert(Assigned(IMapAct));

//  if Assigned(IMapAct) then
 // dmAct_Map.RemoveCalcRegionMaps(aID);

//      procedure RemoveCalcRegionMaps(aCalcRegionID: Integer);


  Result:=dmCalcRegion_pmp.Del (aID);
  if Result then
  begin
    dmMapEngine_store.Feature_Del (OBJ_PMP_CALC_REGION, aID);

///    PostEvent (WE_MAP_ENGINE_DEL_OBJECT, Integer(otCalcRegion), aID);

  //  dmAct_Map.MapRefresh;
  end;
end;

//--------------------------------------------------------------------
function TdmAct_PMP_CalcRegion.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_CalcRegion_View.Create(aOwnerForm);

end;

//--------------------------------------------------------------------
procedure TdmAct_PMP_CalcRegion.GetPopupMenu;
//--------------------------------------------------------------------
begin
  CheckActionsEnable();

  case aMenuType of
    mtFolder: begin


(*                AddFolderMenu_Create (aPopupMenu);
                AddMenuItem (aPopupMenu, nil);
                AddFolderPopupMenu (aPopupMenu);
*)
                AddFolderMenu_Create (aPopupMenu);
               // AddFolderPopupMenu (aPopupMenu);
              end;

    mtItem:   begin
                AddMenuItem (aPopupMenu, act_Object_ShowOnMap);
                AddMenuItem (aPopupMenu, act_BestServer_Tool);
                AddMenuItem (aPopupMenu, act_EmpTool);
                  AddMenuDelimiter(aPopupMenu);
                AddMenuItem (aPopupMenu, act_Calc);
                AddMenuItem (aPopupMenu, act_Copy);
                AddMenuItem (aPopupMenu, act_ShowInExplorer);
                  AddMenuDelimiter(aPopupMenu);
              //  AddMenuItem (aPopupMenu, act_Get_FreqPlan_from_CalcRegion);
                AddMenuItem (aPopupMenu, act_Del_);

                AddMenuItem (aPopupMenu, act_Audit);



                //  AddMenuDelimiter(aPopupMenu);
               // AddFolderMenu_Tools (aPopupMenu);
              end;

    mtList:   begin
              //  AddMenuItem (aPopupMenu, act_Calc);
               // AddMenuDelimiter(aPopupMenu);
                AddMenuItem (aPopupMenu, act_Del_List);
                AddMenuItem (aPopupMenu, act_GroupAssign);
             //   AddFolderMenu_Tools (aPopupMenu);
              end;
  end;
end;

//------------------------------------------------------------
procedure TdmAct_PMP_CalcRegion.Calc (aID: integer);
//------------------------------------------------------------
const
  TEMP_INI_FILE = 'CalcRegion_calc.ini';

var
  iCode: Integer;
  sIniFileName: string;
  oInifile: TInifile;
begin
  sIniFileName:=g_ApplicationDataDir + TEMP_INI_FILE;

  oInifile:=TIniFile.Create (sIniFileName);

  oInifile.WriteString  ('main', 'ConnectionString',    dmMain.GetConnectionString);
  oInifile.WriteInteger ('main', 'ProjectID',    dmMain.ProjectID);
  oInifile.WriteInteger ('main', 'CalcRegionID', aID);

  FreeAndNil(oInifile);

//  oInifile.Free;

//const

//  Assert(Assigned(IMapAct));

//  PostMessage (Application.Handle, MSG_MAP_SAVE_AND_CLOSE_WORKSPACE, 0,0);


 // dmAct_Map.MAP_SAVE_AND_CLOSE_WORKSPACE;

  RunApp (GetApplicationDir() + EXE_CALC_FILE_NAME,  DoubleQuotedStr(sIniFileName), iCode);

  g_EventManager.PostEvent_ (et_CalcMap_REFRESH, []);
  
//  PostUserMessage(WM__CalcMap_REFRESH);

{
    case Msg.message of
     WM_USER + Integer(WM__CalcMap_REFRESH):
        View(FMap, FMapDesktopID)
 }

 // dmAct_Map.MAP_RESTORE_WORKSPACE;

//  PostMessage (Application.Handle, MSG_MAP_RESTORE_WORKSPACE, 0,0);

end;

//--------------------------------------------------------------------
function TdmAct_PMP_CalcRegion.GetExplorerPath(aID: integer; aGUID: string): string;
//--------------------------------------------------------------------
var sFolderGUID: string;
begin
  sFolderGUID:=dmFolder.GetGUIDByID (dmCalcRegion_pmp.GetFolderID(aID));

  Result := GUID_PROJECT            + CRLF +
            GUID_CALC_REGION_PMP    + CRLF +
            sFolderGUID             + CRLF +
            aGUID;
end;

//--------------------------------------------------------------------
function TdmAct_PMP_CalcRegion.ViewInProjectTree(aID: integer): TStrArray;
//--------------------------------------------------------------------
var
  iFolderID: Integer;
  sFolderGUID: string;

begin
  if (Assigned(IMainProjectExplorer)) then
  begin
    IMainProjectExplorer.ExpandByGUID(GUID_PROJECT);
    IMainProjectExplorer.ExpandByGUID(GUID_PMP_GROUP);
    IMainProjectExplorer.ExpandByGUID(GUID_CALC_REGION_PMP);

  //  sGUID:=dmFolder.GetGUIDByID (dmCalcRegion.GetFolderID(aID));


    iFolderID := dmCalcRegion_pmp.GetFolderID(aID);
//    sGUID:=dmFolder.GetGUIDByID (iFolderID);

    if iFolderID=0 then
      sFolderGUID:=g_Obj.ItemByName[ObjectName].RootFolderGUID
    else
      sFolderGUID:=dmFolder.GetGUIDByID(iFolderID);


     Result := StrArray([GUID_PROJECT,
                         GUID_PMP_GROUP,
                         GUID_CALC_REGION_PMP,
                         sFolderGUID]);


    IMainProjectExplorer.ExpandByGUID(sFolderGUID);
//    IMainProjectExplorer.FocuseNodeByGUID (aGUID);

    IMainProjectExplorer.FocuseNodeByObjName(ObjectName, aID);

  end;

end;


//--------------------------------------------------------------------
procedure TdmAct_PMP_CalcRegion.AddByGeoRegionID(aGeoRegionName: WideString);
//--------------------------------------------------------------------
begin
  if dmGeoRegion.GetPolyPointsFromMap(aGeoRegionName, FBLPoints) then //geo_BLRectToBLPoints(aBLRect);
    inherited Add (0)
  else
    ErrorDlg('�������� ������ �� ����� ���������� ����������');

end;


end.