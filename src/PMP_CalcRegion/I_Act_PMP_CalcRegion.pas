unit I_Act_PMP_CalcRegion;

interface

//dm_Act_PMP_CalcRegion

uses
  u_geo;


type
  IAct_PMP_CalcRegion_X = interface(IInterface)
  ['{BC49C34A-9D2C-460C-ABBD-A1AAFED77D3B}']

    procedure AddByPoly (var aBLPoints: TBLPointArrayF);
    procedure AddByRect (aBLRect: TBLRect);
    procedure AddByRing (aCenter: TBLPoint; aRadius: double);

    procedure AddByGeoRegionID(aGeoRegionName: Widestring);

    procedure Calc (aID: integer);


  end;

  
//var
//  IAct_PMP_CalcRegion: IAct_PMP_CalcRegion_X;


implementation

end.
