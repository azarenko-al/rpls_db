object Form1: TForm1
  Left = 536
  Top = 189
  Width = 914
  Height = 608
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 16
    Top = 160
    Width = 529
    Height = 120
    DataSource = DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBGrid2: TDBGrid
    Left = 16
    Top = 296
    Width = 537
    Height = 120
    DataSource = DataSource2
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBGrid3: TDBGrid
    Left = 16
    Top = 424
    Width = 529
    Height = 120
    DataSource = DataSource3
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object Button1: TButton
    Left = 520
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 3
    OnClick = Button1Click
  end
  object cxVerticalGrid1: TcxVerticalGrid
    Left = 624
    Top = 16
    Width = 241
    Height = 177
    OptionsView.RowHeaderWidth = 85
    TabOrder = 4
    Version = 1
    object cxVerticalGrid1EditorRow1: TcxEditorRow
      Properties.Caption = 'asdfasdf'
      Properties.EditPropertiesClassName = 'TcxPopupEditProperties'
      Properties.EditProperties.PopupControl = DBGrid1
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxVerticalGrid1EditorRow2: TcxEditorRow
      Properties.Caption = 'asdfsdf'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxVerticalGrid1EditorRow3: TcxEditorRow
      Properties.Caption = 'asdfasdf'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxVerticalGrid1EditorRow4: TcxEditorRow
      Properties.Caption = 'asdfasd'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
  end
  object Button2: TButton
    Left = 648
    Top = 288
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 5
    OnClick = Button2Click
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\ONEGA\RPLS_DB\bi' +
      'n\link_calc_model_setup\6.mdb;Persist Security Info=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 24
    Top = 16
  end
  object t_Calc_METHOD: TADOTable
    Connection = ADOConnection1
    CursorLocation = clUseServer
    TableName = 'Calc_METHOD'
    Left = 112
    Top = 16
  end
  object t_group: TADOTable
    Connection = ADOConnection1
    CursorLocation = clUseServer
    TableName = '[group]'
    Left = 200
    Top = 16
  end
  object t_group_param: TADOTable
    Connection = ADOConnection1
    CursorLocation = clUseServer
    TableName = 'group_param'
    Left = 280
    Top = 16
  end
  object DataSource1: TDataSource
    DataSet = t_Calc_METHOD
    Left = 112
    Top = 72
  end
  object DataSource2: TDataSource
    DataSet = t_group
    Left = 200
    Top = 72
  end
  object DataSource3: TDataSource
    DataSet = t_group_param
    Left = 280
    Top = 72
  end
  object t_Calc_METHOD_items_XREF1: TADOTable
    Connection = ADOConnection1
    CursorLocation = clUseServer
    TableName = 'Calc_METHOD_items_XREF'
    Left = 408
    Top = 16
  end
  object DataSource4: TDataSource
    DataSet = t_Calc_METHOD_items_XREF1
    Left = 408
    Top = 72
  end
end
