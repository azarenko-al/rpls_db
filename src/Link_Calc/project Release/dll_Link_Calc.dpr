program dll_Link_Calc;

uses
  d_AdaptiveModulation_new in '..\src\d_AdaptiveModulation_new.pas' {frame_Link_AdaptiveModulation_},
  d_Message in '..\..\..\..\common7\Package_Common\d_Message.pas' {dlg_Message1},
  dm_App in '..\src\dm_App.pas' {dmApp_link_calc: TDataModule},
  dm_Link_calc in '..\src\dm_Link_calc.pas' {dmLink_calc: TDataModule},
  dm_Link_calc_data in '..\src\dm_Link_calc_data.pas' {dmLink_calc_data: TDataModule},
  dm_Link_calc_SESR in '..\..\Link\dm_Link_Calc_SESR.pas' {dmLink_calc_SESR: TDataModule},
  dm_MDB in '..\src\dm_MDB.pas' {dmMDB_link_calc: TDataModule},
  dm_Rel_Engine in '..\..\RelEngine\dm_Rel_Engine.pas' {dmRel_Engine: TDataModule},
  Forms,
  u_collection in '..\src\u_collection.pas',
  u_ini_LinkCalcParams in '..\src_shared\u_ini_LinkCalcParams.pas',
  u_link_calc_classes_export in '..\src\u_link_calc_classes_export.pas',
  u_link_calc_classes_main in '..\src\u_link_calc_classes_main.pas',
  u_link_model_layout in '..\src\u_link_model_layout.pas',
  u_LinkCalcResult_new in '..\src\u_LinkCalcResult_new.pas',
  u_rrl_param_rec_new in '..\src_shared\u_rrl_param_rec_new.pas',
  u_classes_Adaptive in '..\src\u_classes_Adaptive.pas',
  u_vars in '..\..\u_vars.pas',
  dm_Main in '..\..\DataModules\dm_Main.pas' {dmMain: TDataModule},
  u_Link_CalcRains in '..\..\link\u_Link_CalcRains.pas',
  u_SDB_classes in '..\src\u_SDB_classes.pas',
  u_opti_class_new in '..\src\opti\u_opti_class_new.pas',
  u_Opti_classes in '..\src\opti\u_Opti_classes.pas',
  u_opti_params in '..\src\opti\u_opti_params.pas',
  f_Main_opti in '..\src\opti\f_Main_opti.pas' {frm_Main_opti};

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(TdmApp_link_calc, dmApp_link_calc);
  Application.Run;
end.
