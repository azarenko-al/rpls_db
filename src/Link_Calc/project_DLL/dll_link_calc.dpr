library dll_link_calc;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  Classes,
  dm_Link_calc in '..\src\dm_Link_calc.pas' {dmLink_calc: TDataModule},
  i_link_calc in '..\src_shared\i_link_calc.pas',
  SysUtils,
  u_link_calc_classes_export in '..\src\u_link_calc_classes_export.pas',
  u_link_calc_classes_main in '..\src\u_link_calc_classes_main.pas',
  u_LinkCalcResult in '..\src\u_LinkCalcResult.pas',
  x_link_calc in '..\src\x_link_calc.pas',
  dm_Link_calc_SESR in '..\..\Link\dm_Link_calc_SESR.pas' {dmLink_calc_SESR: TDataModule},
  u_dll_with_dmMain in '..\..\DLL_common\u_dll_with_dmMain.pas';

{$R *.res}

begin
end.
