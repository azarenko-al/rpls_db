unit dm_MDB;

interface

uses
  Classes, DB, ADODB,  Forms,  Dialogs, SysUtils,


  u_db_mdb,
  u_db
  ;

type
  TdmMDB_link_calc = class(TDataModule)
    ADOConnection: TADOConnection;
    ds_AntennaType: TDataSource;
    ADOTable1: TADOTable;
    procedure DataModuleCreate(Sender: TObject);
  private
    class procedure Init;
    procedure Open;
  public
  end;

var
  dmMDB_link_calc: TdmMDB_link_calc;

implementation
{$R *.dfm}

const
   DEF_Data = 'Data';


// ---------------------------------------------------------------
class procedure TdmMDB_link_calc.Init;
// ---------------------------------------------------------------
begin
  if not Assigned(dmMDB_link_calc) then
    dmMDB_link_calc := TdmMDB_link_calc.Create(Application);
end;

// ---------------------------------------------------------------
procedure TdmMDB_link_calc.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
var
  sFileName: string;

begin
  {

  sAppPath:= IncludeTrailingBackslash (System.IOUtils.TPath.GetHomePath) + 'RPLS_DB_LINK\';
  
//  s:=Application.
//  CodeSite.Send('System.IOUtils.TPath.GetDocumentsPath - ' + s);

                             
//  s:=System.IOUtils.TPath.Get DocumentsPath;
//  CodeSite.Send('System.IOUtils.TPath.GetDocumentsPath - ' + s);

  
//  System.IOUtils.TPath.GetTempPath
  

  //w:\GIS panorama\SXF_to_RLF\Data\

  s:= ExtractFilePath(GetModuleFileName()) + 'options.mdb';
  Assert(FileExists(s), s);

  sFile_MDB:=sAppPath + 'options.mdb';

  if not FileExists(sFile_MDB) then
  begin
    ForceDirectories(sAppPath);


    TFile.Copy  (s, sFile_MDB);
    
  end;
    
   }


 if ADOConnection.Connected then
    ShowMessage('TdmMDB.ADOConnection.Connected');


  sFileName := ChangeFileExt(Application.ExeName, '.mdb');

  if not mdb_OpenConnection (ADOConnection, sFileName) then
    Halt;

  Assert (ADOConnection.Mode = cmRead);

end;


// ---------------------------------------------------------------
procedure TdmMDB_link_calc.Open;
// ---------------------------------------------------------------
begin

  ADOConnection.Execute('DELETE FROM ' + DEF_Data);

//  db_TableDeleteRecords(ADOTable1);
  db_TableOpen1(adoTable1);

end;



end.