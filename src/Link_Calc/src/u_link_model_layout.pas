unit u_link_model_layout;

interface

uses
   SysUtils,  Variants, 

   XMLIntf,


   u_files,

  
  u_xml_document;


  procedure CalcMethod_LoadFromXMLFile(aFileName: string; aCalcMethod: Integer;
      aOutFileName: string);


implementation

//
// CalcMethod_LoadFromXMLFile(
//      GetApplicationDir + 'link_calc_model_setup\RRL_ParamSys.xml',
//      oCalcParams.Calc_method,
//      sFile_layout
//      );



//-----------------------------------------------------------------
procedure CalcMethod_LoadFromXMLFile(aFileName: string; aCalcMethod:
    Integer; aOutFileName: string);
//-----------------------------------------------------------------

var
  b: boolean;
  b2: Boolean;
  b3: Boolean;
  bActive: Boolean;
  bIsFull: Boolean;
  iID: Integer;

  i: integer;
  iCode: Integer;
  vRoot,vGroup,vParam,vNode,vNode1,vNode2,vNode3 : IXMLNode;

  vRoot_out,vParam_out: IXMLNode;

  vGroup_out: IXMLNode;
  vMETHOD: IXMLNode;
  vMETHOD_out: IXMLNode;

  iLen: Integer;
  iNumber: Integer;
  j: Integer;
  k: Integer;
  oXMLDoc: TXMLDoc;

//  s: string;
//  s1: string;
//  s2: string;
//  s3: string;


//  oSList1: TStringList;
  oXMLDoc_out: TXMLDoc;

  sName: string;

begin
  if aFileName='' then
    aFileName:=GetApplicationDir + 'link_calc_model_setup\RRL_ParamSys.xml';


//  aFileName:='C:\_test_rrl\RRL_ParamUser.xml';

  Assert(FileExists(aFileName), 'FileExists(aFileName):' + aFileName);


  if not FileExists(aFileName) then
    Exit;

//  oSList1:=TStringList.Create;

  // FreeAndNil(oSList);

//  if not assigned(FXMLDoc) then
  oXMLDoc:=TXMLDoc.Create;
  oXMLDoc_out:=TXMLDoc.Create;


//  oXMLDoc1:=TXMLDoc.Create;

  oXMLDoc.LoadFromFile(aFileName);

  vRoot := oXMLDoc.DocumentElement;

 // b:=FileExists(aFileName);

 // if vRoot=NULL then
 //   Exit;


  if VarIsNull(vRoot) then
    Exit;


  k:=vRoot.ChildNodes.Count;

  //for MIF use rasters
  vNode := xml_FindNode (vRoot, 'METHODS');
  if VarIsNull(vRoot) then
    Exit;


  for i := 0 to vNode.ChildNodes.Count - 1 do
  begin
    vMETHOD:=vNode.ChildNodes[i];

    iNumber := xml_GetIntAttr(vMETHOD,'numb');
    bIsFull := xml_GetIntAttr(vMETHOD,'full')=1;
    sName   := xml_GetStrAttr(vMETHOD,'name');

    if aCalcMethod<>iNumber then
      Continue;


//    oSList1.Clear;

//    s:= Format('%d %s', [iNumber,sName]);
//    oSList1.Add(s);
//    oSList.AddObject(s, Pointer(iNumber));

    vMETHOD_out:=oXMLDoc_out.DocumentElement.AddChild('METHOD');
    vMETHOD_out.Attributes['name']:=sName;

{ TODO :     vMETHOD_out.Attributes['name']:=vMETHOD.Attributes['name']; }
//vMETHOD_out.Attributes['name']:=vMETHOD.Attributes['name'];


    for j := 0 to vMETHOD.ChildNodes.Count - 1 do
    begin
      vGroup:=vMETHOD.ChildNodes[j];

      bActive := xml_GetIntAttr(vGroup,'Active')=1;
      iCode:=xml_GetIntAttr(vGroup,'code');
      sName:=xml_GetStrAttr(vGroup,'name');

      if bActive then
      begin
        vGroup_out:=vMETHOD_out.AddChild('GROUP');
        vGroup_out.Attributes['code']:=vGroup.Attributes['code'];
        vGroup_out.Attributes['name']:=vGroup.Attributes['name'];


  //      s:= Format('-%d %d.%s', [iCode,iCode,sName]);
//        oSList.AddObject(s, Pointer(iNumber));
//          oSList1.Add(s);


        for k := 0 to vGroup.ChildNodes.Count - 1 do
        begin
          vParam:=vGroup.ChildNodes[k];

          vParam_out:=vGroup_out.AddChild('ITEM');
          vParam_out.Attributes['code']:=vParam.Attributes['code'];
          vParam_out.Attributes['name']:=vParam.Attributes['name'];

//          s := xml_GetAttr(vParam,'rep');

  //        ParseStringToString3(s, s1,s2,s3, ';');

    //      b2:=s2='1';
     //     b3:=s3='1';

          iCode:=xml_GetIntAttr(vParam,'code');
          sName:=xml_GetAttr(vParam,'name');

//          if (bIsFull and b2) or (not bIsFull and (b3)) then
//          begin
   //         s:= Format('%d %s', [iCode,sName]);

//            oSList1.Add(s);
          //  oSList.AddObject(s, Pointer(iNumber));

  //        end;
        end;
      end;
    end;


  //  s:= Format('C:\_test_rrl\%d.txt', [iNumber]);

    ForceDirByFileName(aOutFileName);
//    oSList1.SaveToFile(aOutFileName);

  end;

  oXMLDoc_out.SaveToFile( ChangeFileExt(aOutFileName, '.xml') );

  FreeAndNil(oXMLDoc);
  FreeAndNil(oXMLDoc_out);


//////////  ShellExec_Notepad_temp(oSList.Text);


end;

end.
