unit u_opti_class_new;

//g_OptiTaskItemList


interface
uses
   classes;


type


// ---------------------------------------------------------------
  TTaskItem = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
    LinkEndType  : record
      ID   : Integer;
      name : string;
    end;  


    ID              : Integer;
    Mode            : Integer;

    THRESHOLD_BER_3 : double;
    THRESHOLD_BER_6 : double;

    Power_dBm       : Double;
    BitRate_Mbps    : double;

    Modulation_type : string;

    Antenna : record
      ID   : Integer;
      Name : string;

      Diameter            : Double;
      Gain                : Double;
      Vert_width          : Double;

      Polarization : string;

      Freq_Mhz            : Double;
    end;


    Result: record
      Rx_Level_draft  : Double;
      Zapas_draft     : Double;


      KNG             : Double;
      SESR            : Double;
      Rx_Level_dBm    : Double;


      fade_margin_dB  : Double;  // ����� �� ��������� [dB]
      KNG_req         : Double;  // ����
      SESR_req        : Double;  // ����

      IsWorking : Boolean;  //�����, �� �����

    end;


  end;



  // ---------------------------------------------------------------
  TTaskItemList = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TTaskItem;
  public
    Params: record
       SESR: double;
       KNG: double
    end;


    constructor Create;

    function AddItem: TTaskItem;

    property Items[Index: Integer]: TTaskItem read GetItems; default;
  end;


var
  g_OptiTaskItemList: TTaskItemList;



implementation

constructor TTaskItemList.Create;
begin
   inherited Create(TTaskItem);
end;

function TTaskItemList.AddItem: TTaskItem;
begin
  Result := TTaskItem (inherited Add());
end;

function TTaskItemList.GetItems(Index: Integer): TTaskItem;
begin
    Result := TTaskItem(inherited Items[Index]);

end;


begin
  g_OptiTaskItemList:=TTaskItemList.Create;


end.


