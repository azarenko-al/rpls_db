unit u_opti_params;

interface
uses
  SysUtils,IniFiles, 

  u_classes

 // u_xml_document

  ;


type
  Topti_params = class(TObject)
  public
    AntTypeID: TIDList;
    LinkEnsTypeID: TIDList;


    ConnectionString : string;

    constructor Create;
    destructor Destroy; override;

    function LoadFromIniFile(aFileName: string): boolean;
    procedure SaveToIniFile(aFileName: string);
  end;

implementation

constructor Topti_params.Create;
begin
  inherited Create;
  AntTypeID := TIDList.Create();
end;

destructor Topti_params.Destroy;
begin
  FreeAndNil(AntTypeID);
  inherited Destroy;
end;

// ---------------------------------------------------------------
function Topti_params.LoadFromIniFile(aFileName: string): boolean;
// ---------------------------------------------------------------
var
  I: Integer;
  iCount: Integer;
  oIniFile: TIniFile;
begin

  oIniFile := TIniFile.Create(aFileName);


  oIniFile.WriteString ('main', 'ConnectionString',  ConnectionString);

  {
  FirstRowIndex_1 := oIniFile.ReadInteger (DEF_main, 'FirstRowIndex', FirstRowIndex_1);
//  SheetIndex := oIniFile.ReadInteger (DEF_main, 'SheetIndex', SheetIndex);
  SheetName  := oIniFile.ReadString (DEF_main, 'SheetName', '');


  CoordSys := TCoordSys(oIniFile.ReadInteger (DEF_main, 'CoordSys', 0));
  DataSourceType := TDataSourceType(oIniFile.ReadInteger (DEF_main, 'DataSourceType', 0));


  IsGet_antenna_H_from_site   := oIniFile.ReadBool (DEF_main, 'IsGet_antenna_H_from_site', IsGet_antenna_H_from_site);



  iCount:=oIniFile.ReadInteger(DEF_columns, 'count',0);

  SetLength(Columns, iCount);

  for I := 0 to High(Columns) do
  begin
  //  Columns[i].Header:= oIniFile.ReadString (DEF_columns, Format('%d_Header', [i]), '');
    Columns[i].FieldName := oIniFile.ReadString (DEF_columns, Format('%d_Field_Name', [i]), '');
    Columns[i].IsKey     := oIniFile.ReadBool   (DEF_columns, Format('%d_Is_Key', [i]), False);
    Columns[i].DegreeFormat  := oIniFile.ReadString (DEF_columns, Format('%d_Degree_Format', [i]), '');

  end;
    }

  FreeAndNil(oIniFile);
end;

// ---------------------------------------------------------------
procedure Topti_params.SaveToIniFile(aFileName: string);
// ---------------------------------------------------------------

var
  I: Integer;
  oIniFile: TIniFile;
begin
  DeleteFile(aFileName);


  oIniFile := TIniFile.Create(aFileName);

  oIniFile.WriteString ('main', 'ConnectionString',  ConnectionString);


  {
  oIniFile.WriteString (DEF_main, 'SheetName', SheetName);


  oIniFile.WriteInteger (DEF_main, 'FirstRowIndex', FirstRowIndex_1);
 // oIniFile.WriteInteger (DEF_main, 'SheetIndex', SheetIndex);    

  oIniFile.WriteInteger (DEF_main, 'CoordSys', Integer(CoordSys));
  oIniFile.WriteInteger (DEF_main, 'DataSourceType', Integer(DataSourceType));

  oIniFile.WriteBool (DEF_main, 'IsGet_antenna_H_from_site', IsGet_antenna_H_from_site);


  oIniFile.WriteInteger (DEF_columns, 'count', Length(Columns));

  for I := 0 to High(Columns) do
  begin
   // oIniFile.WriteString (DEF_columns, Format('%d_Header_name', [i]), Columns[i].Header );
    oIniFile.WriteString (DEF_columns, Format('%d_Field_name',  [i]), Columns[i].FieldName );
    oIniFile.WriteBool   (DEF_columns, Format('%d_Is_key', [i]), Columns[i].Iskey );
    oIniFile.WriteString (DEF_columns, Format('%d_Degree_Format', [i]), Columns[i].DegreeFormat );


  end;

    }
  FreeAndNil(oIniFile);
end;

end.
