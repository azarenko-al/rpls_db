unit u_Opti_classes;

interface

uses
  classes, DB,  SysUtils,

  u_func,
  u_db,     
  
  u_const_db
   ;

type
  TOptiAntennaType = class;
  TOptiLinkEndType_mode_List= class;

  // ---------------------------------------------------------------
  TOptiAntennaList = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TOptiAntennaType;
  public
    constructor Create;
    function AddItem: TOptiAntennaType;

    procedure LoadFromDataset(aDataset: TDataset);

    property Items[Index: Integer]: TOptiAntennaType read GetItems; default;
  end;


  TOptiAntennaPolarizationType = (ptV1__,ptH1__,ptX1__);


  // ---------------------------------------------------------------
  TOptiAntennaType = class(TCollectionItem)
  // ---------------------------------------------------------------
  private

//    function GetPolarization1: Integer;
    function GetPolarizationType: TOptiAntennaPolarizationType;

  public

    ID : Integer;
    Name_ : string;

    Diameter    : Double;
    Gain        : Double;
    Vert_width  : Double;

  //  Band        : string;

    Polarization_str: string;


    Freq_Mhz            : Double;
 //   Band                : string;


    procedure LoadFromDataset(aDataset: TDataset);

    property PolarizationType: TOptiAntennaPolarizationType read GetPolarizationType;
  end;


  // ---------------------------------------------------------------
  TOptiLinkEndType = class(TCollectionItem)
  // ---------------------------------------------------------------
//  private
  //  function GetTx_Freq_GHz: double;
 // public
  //  property Tx_Freq_GHz: double read GetTx_Freq_GHz;
  public
    ID : Integer;
    Name_ : string;

 //   Antenna_offset_horizontal_m : double;


    Band : string;


  public
    Modes: TOptiLinkEndType_mode_List;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;

    procedure LoadFromDataset(aDataset: TDataset);
  end;


 // ---------------------------------------------------------------
  TOptiLinkEndType_mode = class(TCollectionItem)
  // ---------------------------------------------------------------

  public

    ID : Integer;
    Mode : Integer;


    THRESHOLD_BER_3    : double;
    THRESHOLD_BER_6    : double;
    Power_dBm          : double;

    BitRate_Mbps       : double;

    Modulation_type : string;

  public


    procedure LoadFromDataset(aDataset: TDataset);
  end;


  // ---------------------------------------------------------------
  TOptiLinkEndType_mode_List = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TOptiLinkEndType_mode;
  public
    constructor Create;
    function AddItem: TOptiLinkEndType_mode;

    procedure LoadFromDataset(aDataset: TDataset);

    property Items[Index: Integer]: TOptiLinkEndType_mode read GetItems; default;
  end;



  // ---------------------------------------------------------------
  TLinkEndType_List = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TOptiLinkEndType;
  public
    constructor Create;

    function AddItem: TOptiLinkEndType;
    function FindByID(aID: Integer): TOptiLinkEndType;

    procedure LoadFromDataset(aDataset: TDataset);

    procedure Mode_LoadFromDataset(aDataset: TDataset; aMin_BitRate_Mbps: Integer);
                           
    property Items[Index: Integer]: TOptiLinkEndType read GetItems; default;
  end;



  // ---------------------------------------------------------------
  TOptiData = class
  // ---------------------------------------------------------------
  public
    Antennas: TOptiAntennaList;
    LinkEndTypes: TLinkEndType_List;

    constructor Create;
    destructor Destroy; override;

  end;


var
  g_OptiData: TOptiData;


implementation

constructor TOptiAntennaList.Create;
begin
  inherited Create(TOptiAntennaType);
end;

function TOptiAntennaList.AddItem: TOptiAntennaType;
begin
  Result := TOptiAntennaType (inherited Add);
end;

function TOptiAntennaList.GetItems(Index: Integer): TOptiAntennaType;
begin
  Result := TOptiAntennaType(inherited Items[Index]);
end;

// ---------------------------------------------------------------
procedure TOptiAntennaList.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
var
  bChecked: Boolean;
begin
  Clear;

 // aDataset.DisableControls;
  aDataset.First;

  while not aDataset.EOF do
  begin
    bChecked:= aDataset.FieldByName(FLD_Checked).AsBoolean;

    if bChecked then
      AddItem.LoadFromDataset(aDataset);

    aDataset.Next;
  end;

  aDataset.First;
  //aDataset.EnableControls;


end;



function TOptiAntennaType.GetPolarizationType: TOptiAntennaPolarizationType;
begin
  if Eq(Polarization_str,'h') then
    Result:=ptH1__ else
  if Eq(Polarization_str,'v') then
    Result:=ptV1__
  else
    Result:=ptX1__;


(*  case FPolarization_str of
    0: Result:=ptH;
    1: Result:=ptV;
    2: Result:=ptX;
  else
     Result:=ptH;
  end;
*)
end;
{
// ---------------------------------------------------------------
function TOptiAntennaType.GetPolarization1: Integer;
// ---------------------------------------------------------------
var
  s: string;
begin
 //TTX.POLARIZATION, '��� �����������, 0-��������������,1-������������,2-�����');
  s := LowerCase(FPolarization_str);

  if s='h' then  Result := 0 else
  if s='v' then  Result := 1 else
                 Result := 2;

end;}

// ---------------------------------------------------------------
procedure TOptiAntennaType.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
begin

  with aDataset do
  begin
    Name_    :=FieldByName(FLD_Name).AsString;


    Id                  :=FieldByName(FLD_Id).AsInteger;

//    BLPoint.B           :=FieldByName(FLD_Lat).AsFloat;
//    BLPoint.L           :=FieldByName(FLD_Lon).AsFloat;

    Polarization_str    :=LowerCase(FieldByName(FLD_Polarization_str).AsString);

    Diameter            :=FieldByName(FLD_Diameter).AsFloat;

    Gain                :=FieldByName(FLD_Gain).AsFloat;
    Vert_width          :=FieldByName(FLD_Vert_width).AsFloat;


   Freq_Mhz:=FieldByName(FLD_Freq).AsFloat;

//    Band    :=FieldByName(FLD_Band).AsString;


  {
    AntennaType_ID:=FieldByName(FLD_AntennaType_ID).AsInteger;

    if AntennaType_ID>0 then
      Band :=  FieldByName(FLD_AntennaType_Band).AsString
    else
      Band :=  FieldByName(FLD_Band).AsString;
   }

//    FronttoBackratio    :=FieldByName(FLD_Fronttobackratio).AsFloat;

/////////    Band := FieldByName(FLD_BAND).AsString;
  end;
end;

constructor TOptiLinkEndType.Create(Collection: TCollection);
begin
  inherited Create(Collection);

//  Antennas := TOptiAntennaList.Create();
  Modes := TOptiLinkEndType_mode_List.Create();
end;

destructor TOptiLinkEndType.Destroy;
begin
  FreeAndNil(Modes);
 // FreeAndNil(Antennas);
  inherited Destroy;
end;

//function TOptiLinkEndType.GetTx_Freq_GHz: double;
//begin
//  Result := TX_FREQ_MHz / 1000;
//end;

// ---------------------------------------------------------------
procedure TOptiLinkEndType.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
var
  sBand: string;
  v: Variant;
begin
  with aDataset do
  begin
    Name_ :=  FieldByName(FLD_Name).AsString;



    ID:=FieldByName(FLD_ID).AsInteger;
    // -----------------------------

    Band :=  FieldByName(FLD_Band).AsString;


(*
    THRESHOLD_BER_3   :=FieldByName(FLD_THRESHOLD_BER_3).asFloat;
    THRESHOLD_BER_6   :=FieldByName(FLD_THRESHOLD_BER_6).asFloat;




  //  BAND              :=FieldByName(FLD_BAND).AsString;
    TX_FREQ_MHz       :=FieldByName(FLD_TX_FREQ_MHz).AsFloat;

  //  if TX_FREQ_MHz=0 then
   //   TX_FREQ_MHz:=FieldByName(FLD_RX_FREQ_MHz).AsFloat;


    Power_dBm         :=FieldByName(FLD_POWER_dBm).AsFloat;
*)

    {
    Kng               :=FieldByName(FLD_Kng).AsFloat;
    LOSS_dBm          :=FieldByName(FLD_LOSS).AsFloat;

    Passive_element_LOSS_dBm:=FieldByName(FLD_Passive_element_LOSS).AsFloat;

    FREQ_CHANNEL_COUNT:=FieldByName(FLD_FREQ_CHANNEL_COUNT).AsInteger;

    Freq_Spacing_MHz  :=FieldByName(FLD_Freq_Spacing).AsFloat;

    KRATNOST_BY_SPACE:=FieldByName(FLD_KRATNOST_BY_SPACE).AsInteger;
    KRATNOST_BY_FREQ :=FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger;

    SIGNATURE_WIDTH := FieldByName(FLD_SIGNATURE_WIDTH).AsFloat;//,  'double; //  '������ ���������, ���');
    SIGNATURE_HEIGHT:= FieldByName(FLD_SIGNATURE_HEIGHT).AsFloat;//, 'double; // '������ ���������, ��');
    MODULATION_COUNT:= FieldByName(FLD_MODULATION_COUNT).AsFloat;//,double;// '����� ������� ���������');
  //  EQUALISER_PROFIT1:= FieldByName('EQUALISER_PROFIT').AsFloat;//, 'double; // '�������, �������� ������������, ��');

//    BitRate_Mbps  := FieldByName(FLD_BitRate_Mbps).AsInteger;
    BitRate_Mbps  := FieldByName(FLD_BitRate_Mbps).AsFloat;

//    if Assigned(FindField(FLD_ATPC_profit_dB)) then
 //   begin
    ATPC_profit_dB:= FieldByName(FLD_ATPC_profit_dB).AsFloat;//, 'double; // '�������, �������� ������������, ��');
    Use_ATPC      := FieldByName(FLD_Use_ATPC).AsBoolean;//, 'double; // '�������, �������� ������������, ��');
    Threshold_degradation_dB:= FieldByName(FLD_Threshold_degradation_dB).AsFloat;//, 'double; // '�������, �������� ������������, ��');

    GOST_53363_modulation                      := FieldByName(FLD_GOST_53363_modulation).AsString;
    GOST_53363_RX_count_on_combined_diversity  := FieldByName(FLD_GOST_53363_RX_count_on_combined_diversity).AsInteger;

    v:=FieldByName(FLD_Antenna_offset_horizontal_m).asFloat;

    Antenna_offset_horizontal_m :=FieldByName(FLD_Antenna_offset_horizontal_m).asFloat;
   }

//  end;

   //   : double;// <param id="45" value="10" comment="������� �� ATPC, ��"/>
  //  : Double; //Threshold_degradation_dB,  '���������� ����������������, ��');


  end;
end;


constructor TOptiLinkEndType_mode_List.Create;
begin
   inherited Create(TOptiLinkEndType_mode);
end;

function TOptiLinkEndType_mode_List.AddItem: TOptiLinkEndType_mode;
begin
  Result := TOptiLinkEndType_mode (inherited Add);
end;

function TOptiLinkEndType_mode_List.GetItems(Index: Integer): TOptiLinkEndType_mode;
begin
  Result := TOptiLinkEndType_mode(inherited Items[Index]);
end;

procedure TOptiLinkEndType_mode_List.LoadFromDataset(aDataset: TDataset);
begin
  Clear;

  aDataset.DisableControls;
  aDataset.First;
                 
  while not aDataset.EOF do
  begin
    AddItem.LoadFromDataset(aDataset);
    aDataset.Next;
  end;


  aDataset.First;
  aDataset.EnableControls;

end;


procedure TOptiLinkEndType_mode.LoadFromDataset(aDataset: TDataset);
begin

  with aDataset do
  begin
  //  Name_ :=  FieldByName(FLD_Name).AsString;


    ID:=FieldByName(FLD_ID).AsInteger;
    // -----------------------------

    Mode:=FieldByName(FLD_Mode).AsInteger;

  //  Band :=  FieldByName(FLD_Band).AsString;

    THRESHOLD_BER_3   :=FieldByName(FLD_THRESHOLD_BER_3).asFloat;
    THRESHOLD_BER_6   :=FieldByName(FLD_THRESHOLD_BER_6).asFloat;

//    Power_dBm         :=FieldByName(FLD_POWER_dBm).AsFloat;
    Power_dBm         :=FieldByName(FLD_POWER_max).AsFloat;

    BitRate_Mbps  := FieldByName(FLD_BitRate_Mbps).AsFloat;

    Modulation_type := FieldByName(FLD_Modulation_type).AsString;

(*




  //  BAND              :=FieldByName(FLD_BAND).AsString;
    TX_FREQ_MHz       :=FieldByName(FLD_TX_FREQ_MHz).AsFloat;

  //  if TX_FREQ_MHz=0 then
   //   TX_FREQ_MHz:=FieldByName(FLD_RX_FREQ_MHz).AsFloat;


    Power_dBm         :=FieldByName(FLD_POWER_dBm).AsFloat;
*)

    {
    Kng               :=FieldByName(FLD_Kng).AsFloat;
    LOSS_dBm          :=FieldByName(FLD_LOSS).AsFloat;

    Passive_element_LOSS_dBm:=FieldByName(FLD_Passive_element_LOSS).AsFloat;

    FREQ_CHANNEL_COUNT:=FieldByName(FLD_FREQ_CHANNEL_COUNT).AsInteger;

    Freq_Spacing_MHz  :=FieldByName(FLD_Freq_Spacing).AsFloat;

    KRATNOST_BY_SPACE:=FieldByName(FLD_KRATNOST_BY_SPACE).AsInteger;
    KRATNOST_BY_FREQ :=FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger;

    SIGNATURE_WIDTH := FieldByName(FLD_SIGNATURE_WIDTH).AsFloat;//,  'double; //  '������ ���������, ���');
    SIGNATURE_HEIGHT:= FieldByName(FLD_SIGNATURE_HEIGHT).AsFloat;//, 'double; // '������ ���������, ��');
    MODULATION_COUNT:= FieldByName(FLD_MODULATION_COUNT).AsFloat;//,double;// '����� ������� ���������');
  //  EQUALISER_PROFIT1:= FieldByName('EQUALISER_PROFIT').AsFloat;//, 'double; // '�������, �������� ������������, ��');

//    BitRate_Mbps  := FieldByName(FLD_BitRate_Mbps).AsInteger;
    BitRate_Mbps  := FieldByName(FLD_BitRate_Mbps).AsFloat;

//    if Assigned(FindField(FLD_ATPC_profit_dB)) then
 //   begin
    ATPC_profit_dB:= FieldByName(FLD_ATPC_profit_dB).AsFloat;//, 'double; // '�������, �������� ������������, ��');
    Use_ATPC      := FieldByName(FLD_Use_ATPC).AsBoolean;//, 'double; // '�������, �������� ������������, ��');
    Threshold_degradation_dB:= FieldByName(FLD_Threshold_degradation_dB).AsFloat;//, 'double; // '�������, �������� ������������, ��');

    GOST_53363_modulation                      := FieldByName(FLD_GOST_53363_modulation).AsString;
    GOST_53363_RX_count_on_combined_diversity  := FieldByName(FLD_GOST_53363_RX_count_on_combined_diversity).AsInteger;

    v:=FieldByName(FLD_Antenna_offset_horizontal_m).asFloat;

    Antenna_offset_horizontal_m :=FieldByName(FLD_Antenna_offset_horizontal_m).asFloat;
   }

//  end;

   //   : double;// <param id="45" value="10" comment="������� �� ATPC, ��"/>
  //  : Double; //Threshold_degradation_dB,  '���������� ����������������, ��');


  end;
end;


//end;

constructor TLinkEndType_List.Create;
begin
  inherited Create(TOptiLinkEndType);;
end;

function TLinkEndType_List.AddItem: TOptiLinkEndType;
begin
  Result := TOptiLinkEndType (inherited Add());

  Assert(Assigned(Result.Modes));

end;

// ---------------------------------------------------------------
procedure TLinkEndType_List.Mode_LoadFromDataset(aDataset: TDataset;
    aMin_BitRate_Mbps: Integer);
// ---------------------------------------------------------------
var
//  bChecked: Boolean;
  eBitRate_Mbps: double;
  iLinkEndType_ID: Integer;
  oItem: TOptiLinkEndType;
begin
 // Clear;

  aDataset.First;
  while not aDataset.EOF do
  begin
   // bChecked:= aDataset.FieldByName(FLD_Checked).AsBoolean;

   // if bChecked then
  //  begin
      iLinkEndType_ID:= aDataset.FieldByName(FLD_LinkEndType_ID).AsInteger;
      eBitRate_Mbps  := aDataset.FieldByName(FLD_BitRate_Mbps).AsFloat;


      oItem:=FindByID(iLinkEndType_ID);

      if Assigned(oItem) and  (eBitRate_Mbps >= aMin_BitRate_Mbps)
      then begin
        Assert(Assigned(oItem.Modes));

        oItem.Modes.AddItem.LoadFromDataset(aDataset);

      end;


    aDataset.Next;
  end;

end;

function TLinkEndType_List.GetItems(Index: Integer): TOptiLinkEndType;
begin
  Result := TOptiLinkEndType(inherited Items[Index]);
end;

// ---------------------------------------------------------------
procedure TLinkEndType_List.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
var
  bChecked: Boolean;
begin
  Clear;

//  aDataset.DisableControls;
  aDataset.First;

  while not aDataset.EOF do
  begin
    bChecked:= aDataset.FieldByName(FLD_Checked).AsBoolean;

    if bChecked then
      AddItem.LoadFromDataset(aDataset);

    aDataset.Next;
  end;

  aDataset.First;
//  aDataset.DisableControls;

end;




//-------------------------------------------------------------------
function TLinkEndType_List.FindByID(aID: Integer): TOptiLinkEndType;
//-------------------------------------------------------------------
var i: integer;
begin
  Result := nil;

  for i:=0 to Count-1 do
    if Items[i].ID = aID then
    begin
      Result:=Items[i];
      Break;
    end;
end;



constructor TOptiData.Create;
begin
  inherited;
  Antennas := TOptiAntennaList.Create();
  LinkEndTypes := TLinkEndType_List.Create();
end;

destructor TOptiData.Destroy;
begin
  FreeAndNil(Antennas);
  FreeAndNil(LinkEndTypes);
  inherited Destroy;
end;



begin
  g_OptiData:=TOptiData.Create;


end.
