unit d_AdaptiveModulation;

interface

uses
  Variants, Classes, Controls, Forms, SysUtils,
    Menus, cxGridTableView, cxGridBandedTableView,
  cxClasses, cxControls, cxGridCustomView, cxGridLevel,
  Graphics,


  Dialogs, ActnList, StdActns, StdCtrls, ExtCtrls, DB,
  //  cxExportGrid4Link,

  dm_Onega_DB_data,

  u_SDB_classes,

  u_Storage,

  u_LinkEndType_const,
  u_Link_const,

  u_cx,
  u_func,

  u_const_db,

  u_db,


  cxGridCustomTableView, cxGridDBBandedTableView, cxGrid, RxMemDS,
  rxPlacemnt, cxInplaceContainer, cxVGrid, frxClass, frxDBSet, frxExportXLS,
  dxmdaset, cxGridDBTableView, ADODB, cxStyles
  , cxGraphics, Grids, DBGrids;

type
  TAdaptiveModeRec = record
      LinkEndType_ID : Integer;

      ID              : Integer;
      Mode            : Integer;

      THRESHOLD_BER_3 : double;
      THRESHOLD_BER_6 : double;

      Power_dBm       : Double;
      BitRate_Mbps    : double;

      Modulation_type : string;

      bandwidth  : double;

      Result: record
        KNG             : Double;
        SESR            : Double;
        Rx_Level_dBm    : Double;

        fade_margin_dB  : Double;  // ����� �� ��������� [dB]
        KNG_req         : Double;  // ����
        SESR_req        : Double;  // ����

        IsWorking : Boolean;  //�����, �� �����

      end;

    end;

type
  Tdlg_AdaptiveModulation = class(TForm)
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    Panel3: TPanel;
    btn_Cancel: TButton;
    cxGrid1: TcxGrid;
    cxGrid1DBBandedTableView1: TcxGridDBBandedTableView;
    col_ID: TcxGridDBBandedColumn;
    col__Mode: TcxGridDBBandedColumn;
    col_Bitrate_Mbps: TcxGridDBBandedColumn;
    col__Power_max: TcxGridDBBandedColumn;
    col__threshold_BER_3: TcxGridDBBandedColumn;
    col__threshold_BER_6: TcxGridDBBandedColumn;
    cxGrid1Level1: TcxGridLevel;
    DataSource: TDataSource;
    col_sesr: TcxGridDBBandedColumn;
    col_kng: TcxGridDBBandedColumn;
    col_rx_level: TcxGridDBBandedColumn;
    FormStorage1: TFormStorage;
    col_IsWorking: TcxGridDBBandedColumn;
    col_Modulation_type: TcxGridDBBandedColumn;
    ActionList1: TActionList;
    FileSaveAs1: TFileSaveAs;
    PopupMenu1: TPopupMenu;
    SaveAs1: TMenuItem;
    col_fade_margin_dB: TcxGridDBBandedColumn;
    col_kng_req: TcxGridDBBandedColumn;
    col_sesr_req: TcxGridDBBandedColumn;
    col_bandwidth: TcxGridDBBandedColumn;
    col_kng_year: TcxGridDBBandedColumn;
    act_Save: TAction;
    Button2: TButton;
    Panel1: TPanel;
    cxVerticalGrid1: TcxVerticalGrid;
    row_BitRate: TcxEditorRow;
    row_Sum: TcxEditorRow;
    frxReport1: TfrxReport;
    frxXLSExport1: TfrxXLSExport;
    frxDBDataset1: TfrxDBDataset;
    dxMemData1: TdxMemData;
    dxMemData1THRESHOLD_BER_3: TFloatField;
    dxMemData1THRESHOLD_BER_6: TFloatField;
    dxMemData1Power_dBm: TFloatField;
    dxMemData1BitRate_Mbps: TFloatField;
    dxMemData1Modulation_type: TStringField;
    dxMemData1KNG: TFloatField;
    dxMemData1KNG_year: TFloatField;
    dxMemData1SESR: TFloatField;
    dxMemData1Rx_Level_dBm: TFloatField;
    dxMemData1FADE_MARGIN_DB: TFloatField;
    dxMemData1KNG_required: TFloatField;
    dxMemData1SESR_required: TFloatField;
    dxMemData1MODE: TIntegerField;
    dxMemData1Is_Working: TBooleanField;
    dxMemData1bandwidth: TFloatField;
    dxMemData1id: TIntegerField;
    act_Set_Priority_Bitrate_mas_0: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    act_Set_Priority_Bitrate_eq_0: TAction;
    N01: TMenuItem;
    row_B: TcxEditorRow;
    dxMemData1K: TFloatField;
    cxGrid1DBBandedTableView1K: TcxGridDBBandedColumn;
    b_Calc: TButton;
    b_Save_SDB: TButton;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_selected: TcxStyle;
    cxStyle1: TcxStyle;
    cxGrid1DBBandedTableView1Column1: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Column2: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Column3: TcxGridDBBandedColumn;
    dxMemData1T_neg_hours: TFloatField;
    dxMemData1T_work_hours: TFloatField;
    dxMemData1T_work_percent: TFloatField;
    dxMemData1sum: TFloatField;
    cxGrid1DBBandedTableView1Column4: TcxGridDBBandedColumn;
    dxMemData1Working_str: TStringField;
    dxMemData1LinkEndType_ID: TIntegerField;
    Button1: TButton;
    col_K_gotov: TcxGridDBBandedColumn;
    qry_Modes: TADOQuery;
    ds_Modes: TDataSource;
    DBGrid1: TDBGrid;
    Button3: TButton;
    procedure act_SaveExecute(Sender: TObject);
    procedure act_Set_Priority_Bitrate_eq_0Execute(Sender: TObject);
    procedure act_Set_Priority_Bitrate_mas_0Execute(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
//    procedure Button5Click(Sender: TObject);
    procedure b_CalcClick(Sender: TObject);
    procedure b_Save_SDBClick(Sender: TObject);
    procedure cxGrid1DBBandedTableView1CustomDrawCell(Sender:
        TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo:
        TcxGridTableDataCellViewInfo; var ADone: Boolean);
        procedure FormDestroy(Sender: TObject);
//    procedure b_Save_SDBClick(Sender: TObject);
//    procedure b_Save_SDB__Click(Sender: TObject);
    procedure dxMemData1CalcFields(DataSet: TDataSet);
    procedure FileSaveAs1Accept(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FSelected_ID: Integer;

    FDataset: TDataset;
    FLink_ID: Integer;

    procedure Calc;
    procedure Save_bitrates;
    procedure Save_Results;

  public
    class procedure ExecDlg(aArr: array of TAdaptiveModeRec; aID: Integer);

  end;



implementation

uses dm_Link_calc;
{$R *.dfm}


const
  FLD_Is_Working = 'Is_Working';




// ---------------------------------------------------------------
class procedure Tdlg_AdaptiveModulation.ExecDlg(aArr: array of
    TAdaptiveModeRec; aID: Integer);
// ---------------------------------------------------------------
var
  I: Integer;
begin

  with Tdlg_AdaptiveModulation.Create(Application) do
  begin
    FLink_ID:=aID;
    

    for I := 0 to High(aArr) do
    begin
      db_AddRecord_ (FDataset,
            [

            FLD_LinkEndType_ID,  aArr[i].LinkEndType_ID,

            FLD_ID,              aArr[i].ID,
            FLD_MODE,            aArr[i].Mode,
            FLD_THRESHOLD_BER_3, aArr[i].THRESHOLD_BER_3,
            FLD_THRESHOLD_BER_6, aArr[i].THRESHOLD_BER_6,
            FLD_Power_dBm,       aArr[i].Power_dBm,
            FLD_BitRate_Mbps,    aArr[i].BitRate_Mbps,

            FLD_Modulation_type, aArr[i].Modulation_type,

            FLD_bandwidth       , aArr[i].bandwidth,

            // Result-------------------------------------------

            FLD_KNG,             aArr[i].Result.KNG,
            FLD_SESR,            aArr[i].Result.SESR,
            FLD_Rx_Level_dBm,    aArr[i].Result.Rx_Level_dBm,
            FLD_FADE_MARGIN_DB,  aArr[i].Result.fade_margin_dB,

            FLD_KNG_required,    aArr[i].Result.KNG_req ,
            FLD_SESR_required,   aArr[i].Result.SESR_req,


            FLD_KNG_year,    aArr[i].Result.KNG  * 0.01 *  365 * 24 * 60,

            //0.01 * [Kng] * 0.01 *  365 * 24 * 60

            FLD_Is_Working,      aArr[i].Result.IsWorking
            ]);

    end;


//    FDataset.First;
    Calc;


    ShowModal;

    Free;
  end;
end;

procedure Tdlg_AdaptiveModulation.act_SaveExecute(Sender: TObject);
begin
 //row_BitRate

   Save_Results();
end;

procedure Tdlg_AdaptiveModulation.act_Set_Priority_Bitrate_eq_0Execute(Sender:
    TObject);
begin
//  if FDataset.RecordCount>0 then
    row_BitRate.Properties.Value:=0;

//
end;


procedure Tdlg_AdaptiveModulation.Button3Click(Sender: TObject);
begin
//  frxXLSExport1.FileName:= FileSaveAs1.Dialog.fIleName;
  frxXLSExport1.ShowDialog:= False;

  frxReport1.PrepareReport();

  frxReport1.ShowReport;

//  frxReport1.Export(frxXLSExport1);



//  cxGrid1.Repaint;
end;

procedure Tdlg_AdaptiveModulation.Button4Click(Sender: TObject);
begin
  Calc;
end;

// ---------------------------------------------------------------
procedure Tdlg_AdaptiveModulation.Calc;
// ---------------------------------------------------------------
var
  oList: TBitrateList;
  oItem: TBitrateItem;
begin
  oList:=TBitrateList.Create;

  dxMemData1.DisableControls;
  dxMemData1.First;

  while not dxMemData1.EOF do
  begin
    oItem:= oList.AddItem_();

    oItem.ID        := dxMemData1.FieldValues[FLD_ID];

    oItem.Bitrate  := dxMemData1.FieldValues[FLD_Bitrate_MBps];
    oItem.Bandwidth:= dxMemData1.FieldValues[FLD_Bandwidth];

    oItem.K := dxMemData1.FieldByName ('K').AsFloat;

    dxMemData1.Next;
  end;

  oList.Calc;

//  ---------------------

  dxMemData1.First;

  while not dxMemData1.EOF do
  begin
    oItem:= oList.FindByID(dxMemData1.FieldValues[FLD_ID]);

    Assert (oItem.T_work_percent >=0);

    db_UpdateRecord__(dxMemData1,

    [
      'T_work_percent', oItem.T_work_percent,
      'sum',            oItem.Sum

    ]);


    dxMemData1.Next;
  end;


  dxMemData1.First;
  dxMemData1.EnableControls;


end;

// ---------------------------------------------------------------
procedure Tdlg_AdaptiveModulation.Save_bitrates;
// ---------------------------------------------------------------
const
  FLD_SDB_bitrate_priority    = 'SDB_bitrate_priority';
  FLD_SDB_bitrate_no_priority = 'SDB_bitrate_no_priority';

var
  k: Integer;
begin
  k:=dmOnega_DB_data.ExecStoredProc_('dbo.sp_Link_Update',
    [
      FLD_ID,  FLink_ID,

      FLD_SDB_bitrate_priority,    row_BitRate.Properties.Value,
      FLD_SDB_bitrate_no_priority, row_Sum.Properties.Value
    ]);


//  @SDB_bitrate_priority float = null,
//  @SDB_bitrate_no_priority float = null

end;


// ---------------------------------------------------------------
procedure Tdlg_AdaptiveModulation.b_CalcClick(Sender: TObject);
// ---------------------------------------------------------------
var
  eBandwidth: Double;
  eBitRate: Double;
  eK: Double;
  oList: TBitrateList;
begin
  oList:=TBitrateList.Create;

 // oList.AddItem(0, 0);
  oList.AddItem(99999, 100);


  dxMemData1.First;

  with dxMemData1 do
    while not EOF do
    begin
      eBitRate  :=FieldByName(FLD_BitRate_Mbps).AsFloat;
      eBandwidth:=FieldByName(FLD_bandwidth).AsFloat;
      eK        :=FieldByName('k').AsFloat;

      if row_B.Properties.Value = eBandwidth then
        oList.AddItem(eBitRate, eK);

   //   row_B.Properties.Value;

      Next;
    end;


  oList.Sort_;
         
  row_Sum.Properties.Value:=oList.Calc_Sum(row_BitRate.Properties.Value);


{
  oList.AddItem(100, 234234);
  oList.AddItem(10, 234234);
  oList.AddItem(120, 234234);

  oList.Sort_;
 }

end;

procedure Tdlg_AdaptiveModulation.b_Save_SDBClick(Sender: TObject);
begin
  Save_bitrates;
end;

// ---------------------------------------------------------------
procedure Tdlg_AdaptiveModulation.Save_Results;
// ---------------------------------------------------------------
begin
{
  dmLink_Calc.Params.Results.Enabled := True;


  dmLink_Calc.Params.Results.LinkEndType_mode_id   := FDataset.FieldByName(FLD_ID).AsInteger;

  dmLink_Calc.Params.Results.MODE           := FDataset.FieldByName(FLD_MODE).AsInteger;
  dmLink_Calc.Params.Results.THRESHOLD_BER_3:= FDataset.FieldByName(FLD_THRESHOLD_BER_3).AsFloat;
  dmLink_Calc.Params.Results.THRESHOLD_BER_6:= FDataset.FieldByName(FLD_THRESHOLD_BER_6).AsFloat;
  dmLink_Calc.Params.Results.Power_dBm      := FDataset.FieldByName(FLD_Power_dBm).AsFloat;
  dmLink_Calc.Params.Results.BitRate_Mbps   := FDataset.FieldByName(FLD_BitRate_Mbps).AsFloat;


  dmLink_Calc.Params.Results.Modulation_type:= FDataset.FieldByName(FLD_Modulation_type).AsString;


  dmLink_Calc.Params.Results.KNG            := FDataset.FieldByName(FLD_KNG).AsFloat;
  dmLink_Calc.Params.Results.SESR           := FDataset.FieldByName(FLD_SESR).AsFloat;
  dmLink_Calc.Params.Results.Rx_Level_dBm   := FDataset.FieldByName(FLD_Rx_Level_dBm).AsFloat;

  dmLink_Calc.Params.Results.FADE_MARGIN_DB := FDataset.FieldByName(FLD_FADE_MARGIN_DB).AsFloat;
  dmLink_Calc.Params.Results.Is_Working     := FDataset.FieldByName(FLD_Is_Working).AsBoolean;


  dmLink_Calc.Params.SaveToFile('');
 }
  dmOnega_DB_data.ExecStoredProc_ ('sp_Link_Update_LinkEndType',
     [
      'LINK_ID' , FLink_ID,

      'LinkEndType_ID',       FDataset.FieldByName(FLD_LinkEndType_ID).AsInteger,
      'LinkEndType_Mode_ID' , FDataset.FieldByName(FLD_ID).AsInteger

     ]);

end;


//  CAPTION_BITRATE_Mbps = '�������� [Mbps]';


// ---------------------------------------------------------------
procedure Tdlg_AdaptiveModulation.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  Caption:='������ � ���������� ����������.';

  cxGrid1.Align:=alClient;

  dxMemData1.Open;


//  while dxMemData1.RecordCount>0 do
//    dxMemData1.delete;


  FDataset:=dxMemData1 ; //RxMemoryData1;

//  dlg_SetDefaultSize(Self);

  db_CreateField (FDataset,
           [
            db_Field(FLD_ID,    ftInteger),
            db_Field(FLD_MODE, ftInteger),
            db_Field(FLD_THRESHOLD_BER_3, ftFloat),
            db_Field(FLD_THRESHOLD_BER_6, ftFloat),
            db_Field(FLD_Power_dBm, ftFloat),
            db_Field(FLD_BitRate_Mbps, ftFloat),

            db_Field(FLD_Modulation_type, ftString, 10),

            db_Field(FLD_KNG,  ftFloat),
            db_Field(FLD_KNG_year,  ftFloat),

            db_Field(FLD_SESR, ftFloat),
            db_Field(FLD_Rx_Level_dBm, ftFloat),
            db_Field(FLD_FADE_MARGIN_DB, ftFloat),
            db_Field(FLD_KNG_required,  ftFloat),
            db_Field(FLD_SESR_required, ftFloat),

            db_Field(FLD_Is_Working, ftBoolean),

            db_Field(FLD_bandwidth, ftFloat)
           ]);

  db_CreateCalculatedField(FDataset, 'Working_str', ftString);

  FDataset.Open;



  col_IsWorking.Caption:='�����������';

 // FileSaveAs1.Dialog.Filter:=


  {
  cx_SetColumnCaptions(cxGrid1DBBandedTableView1,
     [
     FLD_Mode,             '�����',

     FLD_BITRATE_MBPS,     CAPTION_BITRATE_Mbps,
     FLD_threshold_BER_3,  CAPTION_THRESHOLD_BER_3,
     FLD_threshold_BER_6,  CAPTION_THRESHOLD_BER_6,

     FLD_Modulation_type,  CAPTION_MODULATION_TYPE,

     FLD_MODE,             CAPTION_MODE,
     FLD_Power_dBm,        CAPTION_Power_max_dBm,
     FLD_Rx_Level_dBm,     CAPTION_Rx_Level_dBm,

     FLD_Modulation_type,  CAPTION_Modulation_type,
     FLD_fade_margin_dB,  CAPTION_fade_margin_dB,

     FLD_KNG_required,   'KNG ����[%]',
     FLD_SESR_required,  'SESR ����[%]',

     FLD_Is_Working,  '�����������'

    ]);
  }


  cx_SetColumnCaptions(cxGrid1DBBandedTableView1,
     [
      FLD_Mode,             '�����',

      FLD_BITRATE_MBPS,     CAPTION_BITRATE_Mbps,
      FLD_threshold_BER_3,  CAPTION_THRESHOLD_BER_3,
      FLD_threshold_BER_6,  CAPTION_THRESHOLD_BER_6,

      FLD_Modulation_type,  CAPTION_MODULATION_TYPE,

      FLD_MODE,             CAPTION_MODE,
      FLD_Power_dBm,        CAPTION_Power_max_dBm,
      FLD_Rx_Level_dBm,     CAPTION_Rx_Level_dBm,

      FLD_Modulation_type,  CAPTION_Modulation_type,
      FLD_fade_margin_dB,  CAPTION_fade_margin_dB,

      FLD_KNG,   'KNG [%]',
      FLD_KNG_year,   'KNG ��� � ���',
      FLD_SESR,  'SESR [%]',

      FLD_KNG_required,   'KNG ���� [%]',
      FLD_SESR_required,  'SESR ���� [%]',

      FLD_Is_Working,  '�����������',

      FLD_bandwidth, '������ ������ [MHz]'


    ]);


 // cxGrid1DBBandedTableView1.ApplyBestFit();
//  cx_

  g_Storage.RestoreFromRegistry(cxGrid1DBBandedTableView1, ClassName+'4');

end;

procedure Tdlg_AdaptiveModulation.FormDestroy(Sender: TObject);
begin
  g_Storage.StoreToRegistry(cxGrid1DBBandedTableView1, ClassName+'4');

end;

//--------------------------------------------------------------
procedure Tdlg_AdaptiveModulation.FileSaveAs1Accept(Sender: TObject);
//--------------------------------------------------------------
begin
//  db_View(FDataset);


  frxXLSExport1.FileName:= FileSaveAs1.Dialog.fIleName;
  frxXLSExport1.ShowDialog:= False;

  frxReport1.PrepareReport();

//  frxReport1.Preview;

  frxReport1.Export(frxXLSExport1);


//  cx_ExportGridToExcel(FileSaveAs1.Dialog.fIleName, cxGrid1);

//  ExportGrid4ToExcel(FileSaveAs1.Dialog.fIleName, cxGrid1);

  ShellExec(FileSaveAs1.Dialog.fIleName);

end;



// ---------------------------------------------------------------
procedure Tdlg_AdaptiveModulation.dxMemData1CalcFields(DataSet: TDataSet);
// ---------------------------------------------------------------
var
  e: Double;
  eK: Double;
  eKNG: Double;
  eSESR: Double;
  eT_neg_hours: Double;
  eWork_hours: Double;
begin
  if not Assigned(FDataset) then
    exit;

  eSESR:=FDataset.FieldByName(FLD_SESR).AsFloat;
  eKNG :=FDataset.FieldByName(FLD_KNG).AsFloat;



  eK := (eKNG + eSESR - 0.01 * eKNG * eSESR);

  eT_neg_hours:=8760 * eK * 0.01;

//  eWork_hours:=8760 * eK * 0.01;

//  8760

  FDataset['K']:=eK;
  FDataset['T_neg_hours']:=eT_neg_hours;


  e :=FDataset.FieldByName('T_work_percent').AsFloat;

  if e<>0 then
    FDataset['T_work_hours']:=e * 8760 * 0.01;


  if FDataset.FieldByName(FLD_Is_Working).AsBoolean = true then
    FDataset['Working_str'] := '��������';


  //    db_Field(FLD_Is_Working, ftBoolean),


//    db_CreateCalculatedField(FDataset, 'Working_str', ftString);



   {
  FDataset['K']:=0.001 * (FDataset.FieldByName(FLD_KNG).AsFloat +
         FDataset.FieldByName(FLD_SESR).AsFloat
          - 0.01 * FDataset.FieldByName(FLD_KNG).AsFloat * FDataset.FieldByName(FLD_SESR).AsFloat);
  }


//    row_BitRate.Properties.Value:=FDataset.FieldByName(FLD_BitRate_Mbps).AsFloat;
//    row_B.Properties.Value:=FDataset.FieldByName(FLD_BANDWIDTH).AsFloat;


end;

procedure Tdlg_AdaptiveModulation.act_Set_Priority_Bitrate_mas_0Execute(Sender: TObject);
begin
  if FDataset.RecordCount>0 then
  begin
    FSelected_ID:=FDataset.FieldByName(FLD_ID).AsInteger;

//    Edit1.Text:= IntToStr (FSelected_ID);

    cxGrid1.Invalidate();


    row_BitRate.Properties.Value:=FDataset.FieldByName(FLD_BitRate_Mbps).AsFloat;
    row_B.Properties.Value:=FDataset.FieldByName(FLD_BANDWIDTH).AsFloat;

  //  cxGrid1.Repaint;

  end;
end;

procedure Tdlg_AdaptiveModulation.cxGrid1DBBandedTableView1CustomDrawCell(
    Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo:
    TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  v: Variant;

begin
  v:= AViewInfo.GridRecord.Values[col_ID.Index];

  if v = FSelected_ID then
    ACanvas.Brush.Color:=cxStyle_selected.Color;

end;

end.



{

procedure Tdlg_AdaptiveModulation.b_Save_SDBClick(Sender: TObject);
const
  FLD_SDB_bitrate_priority    = 'SDB_bitrate_priority';
  FLD_SDB_bitrate_no_priority = 'SDB_bitrate_no_priority';

var
  k: Integer;
begin
  k:=dmOnega_DB_data.ExecStoredProc_('dbo.sp_Link_Update',
    [
      FLD_ID,  FLink_ID,

      FLD_SDB_bitrate_priority,    row_BitRate.Properties.Value,
      FLD_SDB_bitrate_no_priority, row_Sum.Properties.Value
    ]);


//  @SDB_bitrate_priority float = null,
//  @SDB_bitrate_no_priority float = null

end;


  g_Storage.RestoreFromRegistry(cxDBTreeList11, ClassName);


//  g_Storage.RestoreFromRegistry();

 // dx_CheckColumnSizes_DBTreeList (dxDBTree);


end;


//--------------------------------------------------------------------
procedure Tframe_Link_items.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
 // dxDBTree.SaveToRegistry (FRegPath + dxDBTree.Name);
//  cxDBTreeList1.StoreToRegistry(FRegPath + cxDBTreeList1.Name);

  g_Storage.StoreToRegistry(cxDBTreeList11, ClassName);





lg_AdaptiveModulation.Button1Click(Sender: TObject);
// ---------------------------------------------------------------
begin
  frxXLSExport1.FileName:= 'd:\aaaaaa.xls';
  frxXLSExport1.ShowDialog:= False;

  frxReport1.PrepareReport();

//  frxReport1.Preview;

  frxReport1.Export(frxXLSExport1);

end;


procedure Tdlg_AdaptiveModulation.cxGrid1DBBandedTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);

var
 // id: Integer;
  v: Variant;

begin


//  v:=ARecord.Values[col_ID.Index];


 // if v = FSelected_ID then
  //  AStyle := cxStyle_selected

end;




procedure Tdlg_AdaptiveModulation.Button5Click(Sender: TObject);
begin

    {
cxGrid1DBBandedTableView1.in

  cxGrid1DBBandedTableView1.  Styles.ResetStyles;// OnGetContentStyle:=nil;

  //cxGrid1DBBandedTableView1StylesGetContentStyle

    cxGrid1.Repaint;
    cxGrid1.Refresh;

    Refresh;
    }
end;

