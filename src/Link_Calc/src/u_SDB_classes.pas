unit u_SDB_classes;

interface
uses Classes, SysUtils,

  u_lists
;

type

//TDoubleListItem


  TBitrateItem = class //(TCollectionItem)
  public
    ID: integer;

    Bitrate: double;
    Bandwidth: double;

    K: double;

    Value: double;
    Sum: double;

    T_work_percent: double;

    SESR: Double;
    KNG: Double;

  end;



  TBitrateList = class(TList)
  private
//    TDoubleListItem

 //   procedure Calc_Sum_v2;
    function GetItem(Index: Integer): TBitrateItem;
  public
    function AddItem_: TBitrateItem;
//    constructor Create;
    function AddItem(aBitrate, aK: double): TBitrateItem;
    function Calc_Sum(aBitrate_Priority: Double): Double;

    procedure Sort_;

    procedure Calc;
    function FindByID(aID: integer): TBitrateItem;


    property Items[Index: Integer]: TBitrateItem read GetItem; default;
  end;


implementation




function Compare_Bitrates(Item1, Item2: Pointer): Integer;
begin
  // ���������� �� Bandwidth, Bitrate

  Result:=0;

  if TBitrateItem(Item1).Bandwidth > TBitrateItem(Item2).Bandwidth then
    Result := 1
  else

  if TBitrateItem(Item1).Bandwidth < TBitrateItem(Item2).Bandwidth then
    Result := -1

  else
//  if Result<>0 then
//    exit;


  //--------------------------------------------

  if TBitrateItem(Item1).Bitrate > TBitrateItem(Item2).Bitrate then
    Result := 1   else

  if TBitrateItem(Item1).Bitrate < TBitrateItem(Item2).Bitrate then
    Result := -1;

end;


{
procedure TForm1.Button1Click(Sender: TObject);
begin
  List1.Sort(@CompareText);
end;
}


function TBitrateList.AddItem(aBitrate, aK: double): TBitrateItem;
begin
  Result:=TBitrateItem.Create;
  Result.Bitrate:=aBitrate;
  Result.K:=aK;

  Add (Result);
end;


function TBitrateList.AddItem_: TBitrateItem;
begin
  Result:=TBitrateItem.Create;
//  Result.Bitrate:=aBitrate;
//  Result.K:=aK;

  Add (Result);
end;

procedure TBitrateList.Calc;
var
  e: Double;
  eBandwidth: Double;
  eSum: Double;
  i: Integer;
  k: Integer;
  oList_Bandwidth: TDoubleList;
  oValueList: TDoubleList;

  oValue: TDoubleListItem;

  oBitrateItem:  TBitrateItem;

begin
  oList_Bandwidth:=TDoubleList.Create;
  oValueList:=TDoubleList.Create;

  Sort_;


  for i:=0 to Count-1 do
    if not oList_Bandwidth.ValueExists(Items[i].Bandwidth) then
       oList_Bandwidth.AddValue(Items[i].Bandwidth);



//  for k:=0 to 1 do
  for k:=0 to oList_Bandwidth.Count-1 do
  begin
    eBandwidth:=oList_Bandwidth[k].Value;

    oValueList.Clear;

    for i:=0 to Count-1 do
      if Items[i].Bandwidth = eBandwidth then
         oValueList.AddValue(Items[i].K, Items[i].ID);

    for i:=0 to oValueList.Count-1-1 do
    begin
//      Assert (oValueList[i+1].Value >= oValueList[i].Value);
    end;


    for i:=0 to oValueList.Count-1 do
    begin
      if i< oValueList.Count-1 then
      begin
    //    Assert (oValueList[i+1].Value >= oValueList[i].Value);

        oValueList[i].Value2:=oValueList[i+1].Value - oValueList[i].Value;
      end
      else
        oValueList[i].Value2:=100- oValueList[i].Value;

      if oValueList[i].Value2 < 0 then
        oValueList[i].Value2:=0;


//      Assert (oValueList[i].Value2 >=0);


  //    eSum:=eSum + oValueList[i].Bitrate * oValueList[i].Value2;

    end;

    eSum:=0;

//    for i:=0 to oValueList.Count-1 do
    for i:=0 to oValueList.Count-1 do
    begin
      oBitrateItem:=FindByID (oValueList[i].ID);
      Assert (Assigned(oBitrateItem));

      eSum:=eSum + oBitrateItem.Bitrate * oValueList[i].Value2 * 0.01;

 //     oBitrateItem.T_work_percent:=oValueList[i].Value2;
  //    oBitrateItem.Sum  :=eSum;
    end;


    for i:=0 to oValueList.Count-1 do
    begin
      oBitrateItem:=FindByID (oValueList[i].ID);

      Assert (Assigned(oBitrateItem));

      Assert (oValueList[i].Value2 >=0);


      oBitrateItem.T_work_percent:=oValueList[i].Value2;


      oBitrateItem.Sum  :=eSum;
    end;


//    for i:=oValueList.Count-1-1 do
 //     oValueList[i].Value2:=oValueList[i+1].Value - oValueList[i].Value;

  end;


  FreeAndNil(oList_Bandwidth);
  FreeAndNil(oValueList);

end;



procedure TBitrateList.Sort_;
begin
  Sort(@Compare_Bitrates);
end;


function TBitrateList.GetItem(Index: Integer): TBitrateItem;
begin
  Result := TBitrateItem (inherited Items[Index]);
end;

// ---------------------------------------------------------------
function TBitrateList.Calc_Sum(aBitrate_Priority: Double): Double;
// ---------------------------------------------------------------
var
  e: Double;
  i: Integer;
begin
  //----------------------------
  // ��������� �� ���� ��������
  //----------------------------

  if Count<2 then
    exit;

  Assert(Count>0);

  if Items[Count-2].Bitrate <= aBitrate_Priority then
  begin
    Result:=0;
    Exit;
  end;

  Result:=0;

  for i:=1 to Count-2 do
   if Items[i].Bitrate > aBitrate_Priority then
   begin
     e:= Items[i+1].K - Items[i].K;       
     if e<0 then e:=0;

     Result:=Result + (Items[i].Bitrate - aBitrate_Priority) * e;
   end;

    Result:=Result / 100;

//   Result:=Result - aBitrate_Priority;

end;



function TBitrateList.FindByID(aID: integer): TBitrateItem;
var
  I: Integer;
begin
  Result := nil;

  for I := 0 to Count - 1 do
    if Items[i].ID = aID then
    begin
      Result:=Items[i];
      exit;
    end;

end;



//------------------------------------------
  {
var
  oList: TBitrateList;
begin
  oList:=TBitrateList.Create;

  oList.AddItem(0, 0);
  oList.AddItem(99999, 1);

  oList.AddItem(40, 0.1);
  oList.AddItem(10, 0.01);
  oList.AddItem(60, 0.5);


  oList.Sort_;
  oList.Calc_Sum(10);
   }

end.






procedure TBitrateList.Calc_Sum_v2;
var
  e: Double;
  i: Integer;
begin
  //----------------------------
  // ��������� �� ���� ��������
  //----------------------------
{
  if Items[Count-2].Bitrate <= aBitrate_Priority then
  begin
    Result:=0;
    Exit;
  end;
 }

 // Result:=0;
 {

  for i:=1 to Count-2 do
   if Items[i].Bitrate > aBitrate_Priority then
   begin
     e:= Items[i+1].K - Items[i].K;
     if e<0 then e:=0;

   //  Result:=Result + Items[i].Bitrate * e;
   end;

 }

//   Result:=Result - aBitrate_Priority;

end;

