
unit u_classes_Adaptive;

interface

uses
  XMLIntf, DB,  Classes, SysUtils,

  u_log,

  u_db,
  u_const_db,
  u_Link_const,

  u_xml_document
  ;

type

  // ---------------------------------------------------------------
  TAdaptiveMode = class(TCollectionItem)
  // ---------------------------------------------------------------
  private
    THRESHOLD_BER_3 : double;
    THRESHOLD_BER_6 : double;
    BER_REQUIRED_Index : Integer;

//    procedure LoadFromDB111111111111111(aDataset: TDataset);
  public
    ID              : Integer;
    Mode            : integer;

    Threshold_dBm   : double;

    SIGNATURE_WIDTH : double;
    SIGNATURE_HEIGHT: double;

    Power_dBm       : Double;
    BitRate_Mbps    : double;

    Power_noise_ : double;

    Modulation_type : string;
    MODULATION_Level_COUNT: word;


//    bandwidth       : double;


    Result: record
      Rx_Level  : Double;
//      Zapas_draft     : Double;

      KNG             : Double;
      SESR            : Double;
  //    Rx_Level_dBm    : Double;

      fade_margin_dB  : Double;  // ����� �� ��������� [dB]
      KNG_req         : Double;  // ����
      SESR_req        : Double;  // ����

      IsWorking : Boolean;  //�����, �� �����

      //7	������� ����������� �� ���� ���������
      Param_7 : word;
      //16	���������� ���������� �������� �� ������� SESR
      Param_16 : Double;
      //17	������������ ���������� �������� �� ������� SESR
      Param_17 : Double;
      //18	���������� ���������� ������������ ���
      Param_18 : Double;
      //19	������������ ���������� ������������ ���
      Param_19 : Double;
      //101	������� �������� ������� �� ����� �������� (������� �������)
      Param_101 : Double;
      //127	���.���������� ���������� � ������ (����� �� ��������� � �������� ������)
      Param_127 : Double;

    end;


    {
    KNG             : Double;
    SESR            : Double;
    Rx_Level_dBm    : Double;

    fade_margin_dB  : Double;  // ����� �� ��������� [dB]
    KNG_req         : Double;  // ����
    SESR_req        : Double;  // ����

    IsWorking : Boolean;  //�����, �� �����
    }
  end;


  TAdaptiveModeList = class(TCollection)
  private
    function GetItems(Index: integer): TAdaptiveMode;
  public
    function AddRecord: TAdaptiveMode;

    function FindByID(aID: Integer): TAdaptiveMode;

    procedure LoadFromDB(aDataset: TDataSet; aIsXPIC: Boolean);

    procedure SaveResultsToDB(aDataset: TDataSet);
    procedure SaveToXMLFile(aFileName: string);

    procedure LoadFromXML(aFileName: string);

    property Items[Index: integer]: TAdaptiveMode read GetItems; default;
  end;


implementation

const
  FLD_Is_Working = 'Is_Working';


function TAdaptiveModeList.AddRecord: TAdaptiveMode;
begin
  Result := Add as TAdaptiveMode;
end;


function TAdaptiveModeList.GetItems(Index: integer): TAdaptiveMode;
begin
  Result := TAdaptiveMode(inherited Items[Index]);
end;



//--------------------------------------------------------------------
procedure TAdaptiveModeList.SaveToXMLFile(aFileName: string);
//--------------------------------------------------------------------
const
  DEF_PARAMS : array[0..6] of record
     id: integer;
     name: string;
   end = (
      (id:7;	  name:'������� ����������� �� ���� ���������'),
      (id:16;	  name:'���������� ���������� �������� �� ������� SESR'),
      (id:17;	  name:'������������ ���������� �������� �� ������� SESR'),
      (id:18;	  name:'���������� ���������� ������������ ���'),
      (id:19;	  name:'������������ ���������� ������������ ���'),
      (id:101;	name:'������� �������� ������� �� ����� �������� (������� �������)'),
      (id:127;	name:'���.���������� ���������� � ������ (����� �� ��������� � �������� ������)')
    );

 DEF_TTX : array[0..7] of record
     id: integer;
     name: string;
   end = (
      (id:1;	  name:'��������'),
      (id:7;	  name:'����������������'),
      (id:15;	  name:'�������� ��������'),
      (id:16;	  name:'������ ���������'),
      (id:17;	  name:'������ ���������'),
      (id:18;	  name:'��� ���������'),
      (id:19;	  name:'����� ������� ���������'),
      (id:51;	  name:'�������� ����')
    );

{
    oCalcParams.TTX.Site1_GAIN      :=oTaskItem.Antenna.Gain;
    oCalcParams.TTX.Site1_DIAMETER  :=oTaskItem.Antenna.Diameter;
    oCalcParams.TTX.Site1_VERT_WIDTH:=oTaskItem.Antenna.Vert_width;

    oCalcParams.TTX.Site2_GAIN      :=oTaskItem.Antenna.Gain;
    oCalcParams.TTX.Site2_DIAMETER  :=oTaskItem.Antenna.Diameter;
    oCalcParams.TTX.Site2_VERT_WIDTH:=oTaskItem.Antenna.Vert_width;



    oCalcParams.TTX.BitRate_Mbps    := oTaskItem.bitRate_Mbps;
    oCalcParams.TTX.Site1_Power_dBm := oTaskItem.Power_dBm;
 }

    {
������ ������ � ID ��� ��������� �������:

1	��������
7	����������������
15	�������� ��������
16	������ ���������
17	������ ���������
18	��� ���������
19	����� ������� ���������
}

var
  i: integer;
  vGroup: IXMLNode;
  oXMLDoc: TXMLDoc;


begin
  oXMLDoc:=TXMLDoc.Create;

{
7   16     17  18  19      101 127   0 - ID ����������� ������� (0-������� ����� ������)
1    7      15  16  17       18    19   0 - ID ����������� ������������� (0-������� ����� ������)
15 -92.5 13  7   72.76     0     4
13 -86.0 26  7   72.76     1   16
12 -83.0 33  7   70.2       1   32

7	������� ����������� �� ���� ���������
16	���������� ���������� �������� �� ������� SESR
17	������������ ���������� �������� �� ������� SESR
18	���������� ���������� ������������ ���
19	������������ ���������� ������������ ���
101	������� �������� ������� �� ����� �������� (������� �������)
127	���.���������� ���������� � ������ (����� �� ��������� � �������� ������)

 }

  vGroup:=xml_AddNode (oXMLDoc.DocumentElement, 'calc_params',[]);

  for i:=0 to High(DEF_PARAMS) do
    xml_AddNode_ (vGroup, 'item',
         [
           'id',    DEF_PARAMS[i].id,
           'name',  DEF_PARAMS[i].Name
         ]);


  vGroup:=xml_AddNode (oXMLDoc.DocumentElement, 'TTX',[]);

  for i:=0 to High(DEF_TTX) do
    xml_AddNode_ (vGroup, 'item',
         [
           'id',    DEF_TTX[i].id,
           'name',  DEF_TTX[i].Name
         ]);


{
������ ������ � ID ��� ��������� �������:

1	��������
7	����������������
15	�������� ��������
16	������ ���������
17	������ ���������
18	��� ���������
19	����� ������� ���������
 }

  vGroup:=xml_AddNode_ (oXMLDoc.DocumentElement, 'data',
            ['comment', '��� ���������, 0-PSK (QPSK), 1-QAM (TCM)']);

  for i:=0 to Count-1 do
    xml_AddNode_ (vGroup, 'item',
         [
          'id',           Items[i].id,
          'Mode',         Items[i].Mode,

          'Power',        Items[i].Power_dBm,

          'THRESHOLD',    Items[i].Threshold_dBm, // BER_6,
//          'THRESHOLD',    Items[i].THRESHOLD_BER_3,
          'BitRate_Mbps', Items[i].BitRate_Mbps,

          'SIGNATURE_WIDTH',  Items[i].SIGNATURE_WIDTH,
          'SIGNATURE_HEIGHT', Items[i].SIGNATURE_HEIGHT,

          'Modulation',             IIF(Pos('QAM', Items[i].Modulation_type)>0, 1,0),
          'Modulation_type',        Items[i].Modulation_type,
          'MODULATION_Level_COUNT', Items[i].MODULATION_Level_COUNT,

          'Power_noise',            Items[i].Power_noise_

//           Power_noise:=FieldByName(FLD_Power_noise).AsFloat;

         ]);


  oXMLDoc.XmlDocument.SaveToFile(aFileName);

  FreeAndNil(oXMLDoc);

  /////////////////////////////////////

//  LoadFromXML(aFileName);
end;
    
// ---------------------------------------------------------------
function TAdaptiveModeList.FindByID(aID: Integer): TAdaptiveMode;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Result := nil;

  for I := 0 to Count - 1 do
      if Items[i].ID= aID then
    begin
      Result := Items[i];
      Break;
    end;
end;

//--------------------------------------------------------------------
procedure TAdaptiveModeList.LoadFromXML(aFileName: string);
//--------------------------------------------------------------------

var
  i: integer;
  iID: Integer;
  vItem: IXMLNode;
  vGroup: IXMLNode;
  oXMLDoc: TXMLDoc;

//  FindByID(aID: Integer):
  oMode: TAdaptiveMode;

begin
  Assert (FileExists (aFileName));

  oXMLDoc:=TXMLDoc.Create;
  oXMLDoc.LoadFromFile(aFileName);


  // ---------------------------------------------------------------
//  vGroup:=xml_FindNode (oXMLDoc.DocumentElement, 'data');
  vGroup:=oXMLDoc.DocumentElement;


    for i:=0 to vGroup.ChildNodes.Count-1 do
    begin
      vItem:=vGroup.ChildNodes[i];

      iID:=xml_GetIntAttr(vItem, 'id');

      oMode:=FindByID(iID);

//      Assert( oMode

      oMode.Result.Param_7   :=xml_GetIntAttr(vItem,   'param_7');
      oMode.Result.param_16  :=xml_GetFloatAttr(vItem, 'param_16');
      oMode.Result.param_17  :=xml_GetFloatAttr(vItem, 'param_17');
      oMode.Result.param_18  :=xml_GetFloatAttr(vItem, 'param_18');
      oMode.Result.param_19  :=xml_GetFloatAttr(vItem, 'param_19');
      oMode.Result.param_101 :=xml_GetFloatAttr(vItem, 'param_101');
      oMode.Result.param_127 :=xml_GetFloatAttr(vItem, 'param_127');

{
    <item id="7" value="0" name="������� ����������� �� ���� ���������"/>
    <item id="16" value="0.0059998087283624" name="���������� ���������� �������� �� ������� SESR"/>
    <item id="17" value="0.0716435218217313" name="������������ ���������� �������� �� ������� SESR"/>
    <item id="18" value="0.0124996015174217" name="���������� ���������� ������������ ���"/>
    <item id="19" value="1E-5" name="������������ ���������� ������������ ���"/>
    <item id="101" value="-52.8542371373839" name="������� �������� ������� �� ����� �������� (������� �������)"/>
    <item id="127" value="-18.1457628626161" name="���.���������� ���������� � ������ (����� �� ��������� � �������� ������)"/>

 }

      oMode.Result.IsWorking:=oMode.Result.Param_7=1;

      oMode.Result.Rx_Level:=oMode.Result.param_101;
//      Zapas_draft     : Double;

      oMode.Result.SESR    :=oMode.Result.param_17;
      oMode.Result.SESR_req:=oMode.Result.param_16;;  // ����

      oMode.Result.KNG     :=oMode.Result.param_19;
      oMode.Result.KNG_req :=oMode.Result.param_18;;  // ����

      oMode.Result.fade_margin_dB  := - oMode.Result.param_127;;  // ����� �� ��������� [dB]
    end;


  FreeAndNil(oXMLDoc);
end;    


// ---------------------------------------------------------------
procedure TAdaptiveModeList.LoadFromDB(aDataset: TDataSet; aIsXPIC: Boolean);
// ---------------------------------------------------------------
var
  oItem: TAdaptiveMode;

begin
  Clear;

//  db_ForEach(dxMemData1, DoOnLoadChecked);

//  aDataset.First;
//  aDataset.DisableControls;

  with aDataset do
    while not EOF do
    begin
 //     if FieldByName(FLD_CHECKED).AsBoolean then

      oItem:=AddRecord;

      oItem.ID              :=FieldByName(FLD_ID).AsInteger;
      oItem.Mode            :=FieldByName(FLD_Mode).AsInteger;
      // ---------------------------------------------------------------

      oItem.THRESHOLD_BER_3 :=FieldByName(FLD_THRESHOLD_BER_3).AsFloat;
      oItem.THRESHOLD_BER_6 :=FieldByName(FLD_THRESHOLD_BER_6).AsFloat;

      oItem.Power_dBm       :=FieldByName(FLD_Power_MAX).AsFloat;
//      oItem.Power_dBm       :=FieldByName(FLD_Power_dBm).AsFloat;
      oItem.BitRate_Mbps    :=FieldByName(FLD_BitRate_Mbps).AsFloat;

      if aIsXPIC then
        oItem.BitRate_Mbps:=2*oItem.BitRate_Mbps;


      oItem.SIGNATURE_WIDTH :=FieldByName(FLD_SIGNATURE_WIDTH).AsFloat;
      oItem.SIGNATURE_HEIGHT:=FieldByName(FLD_SIGNATURE_HEIGHT).AsFloat;

      oItem.Modulation_type        :=FieldByName(FLD_Modulation_type).AsString;
      oItem.MODULATION_Level_COUNT :=FieldByName(FLD_MODULATION_Level_COUNT).AsInteger;

      oItem.Power_noise_:=FieldByName(FLD_Power_noise).AsFloat;

      oItem.BER_REQUIRED_Index := FieldByName(FLD_BER_REQUIRED).AsInteger;


      //���������������� dBm
      case oItem.BER_REQUIRED_Index of
        0: oItem.Threshold_dBm:=oItem.THRESHOLD_BER_3;
        1: oItem.Threshold_dBm:=oItem.THRESHOLD_BER_6;
      else
        raise Exception.Create('');
      end;


      Next;
    end;

//  aDataset.First;
//  aDataset.EnableControls;


end;

// ---------------------------------------------------------------
procedure TAdaptiveModeList.SaveResultsToDB(aDataset: TDataSet);
// ---------------------------------------------------------------
var
  i: Integer;
 // oItem: TAdaptiveMode;

begin
  Assert (Count>0);

 // Clear;

//  db_ForEach(dxMemData1, DoOnLoadChecked);

  aDataset.First;
  aDataset.DisableControls;

  for i:=0 to Count-1 do

  with aDataset do
    if Locate(FLD_ID, Items[i].ID, [] ) then
    begin
      Assert (Items[i].Result.Rx_Level <> 0);

      db_UpdateRecord__(aDataset,
        [
          FLD_Rx_Level_dBm   , Items[i].Result.Rx_Level ,
          FLD_KNG            , Items[i].Result.KNG,

          FLD_Kng_year,        Round(0.01 * Items[i].Result.KNG * 365 * 24 * 60),

          FLD_SESR           , Items[i].Result.SESR,

          FLD_KNG_required   , Items[i].Result.KNG_req,
          FLD_SESR_required  , Items[i].Result.SESR_req,

          FLD_fade_margin_dB , Items[i].Result.fade_margin_dB,

          FLD_Is_Working      , Items[i].Result.IsWorking
        ] );

{
    <item id="7" value="0" name="������� ����������� �� ���� ���������"/>
    <item id="16" value="0.0059998087283624" name="���������� ���������� �������� �� ������� SESR"/>
    <item id="17" value="0.0359940356215168" name="������������ ���������� �������� �� ������� SESR"/>
    <item id="18" value="0.0124996015174217" name="���������� ���������� ������������ ���"/>
    <item id="19" value="1E-5" name="������������ ���������� ������������ ���"/>
    <item id="101" value="-52.8542371373839" name="������� �������� ������� �� ����� �������� (������� �������)"/>
    <item id="127" value="-21.1457628626161" name="���.���������� ���������� � ������ (����� �� ��������� � �������� ������)"/>
  </mode>
 }
    //  FieldByName(FLD_THRESHOLD_BER_3):= Items[i].Result.IsWorking

    end
     else

      g_Log.Add('TAdaptiveModeList.SaveResultsToDB id:'+ IntToStr( Items[i].ID ));
    ;;
//      raise Exception.Create('');


  aDataset.First;
  aDataset.EnableControls;


end;


end.



