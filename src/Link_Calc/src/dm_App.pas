unit dm_App;

interface

uses
  SysUtils, Classes, Dialogs, Windows,

  u_vars,
  u_ini_LinkCalcParams,
  dm_Link_Calc,

  d_AdaptiveModulation_new,

  dm_Main
  ;

type
  TdmApp_link_calc = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);

  end;

var
  dmApp_link_calc: TdmApp_link_calc;

//==============================================================
implementation {$R *.DFM}



//--------------------------------------------------------------
procedure TdmApp_link_calc.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const
  DEF_SOFTWARE_NAME= 'RPLS_DB_LINK';
  DEF_TEMP_FILENAME = 'link_calc.ini';

var
  bHasResult: Boolean;
  sIniFileName: string;
//  oParams: TIni_Link_Calc_Params;

begin
    //g_ApplicationDataDir


  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
//    sIniFileName:=TEMP_INI_FILE;

//    sIniFileName:=GetCommonApplicationDataDir_Local_AppData(DEF_SOFTWARE_NAME)+
    sIniFileName:=g_ApplicationDataDir + DEF_TEMP_FILENAME;


  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;



  TdmMain.Init;
 // if not dmMain.OpenDB_dlg then
//  if not dmMain.OpenDB_reg then
//    Exit;


  TdmLink_Calc.Init;
  dmLink_Calc.Params.LoadFromFile(sIniFileName);

//  TdmMain.Init;
  dmMain.Open_ADOConnection(dmLink_Calc.Params.ConnectionString);


  dmMain.ProjectID:=dmLink_Calc.Params.ProjectID;

  Assert (dmLink_Calc.Params.ProjectID > 0);


  if dmLink_Calc.Params.LinkID>0 then
    dmLink_Calc.Params.IDList.AddID(dmLink_Calc.Params.LinkID);

   assert(dmLink_Calc.Params.IDList.Count>0, 'dmLink_Calc.Params.IDList.Count>0');


  // ---------------------------------------------------------------
  if dmLink_Calc.Params.Mode = mtAdaptiveModulation then
  begin
    Assert(dmLink_Calc.Params.LinkID>0, 'dmLink_Calc.Params.LinkID>0');

    Tframe_Link_AdaptiveModulation_.ExecDlg(dmLink_Calc.Params.LinkID);

    Exit;
  end;



  if dmLink_Calc.Params.Validate then
 //   dmLink_Calc.Execute (dmLink_Calc.Params.LinkID, nil);
//    dmLink_Calc.ExecuteList1 (dmLink_Calc.Params.LinkID, nil);
    dmLink_Calc.ExecuteList1 (dmLink_Calc.Params.IDList);


  bHasResult:=dmLink_Calc.Params.Results.Enabled;


  FreeAndNil(dmLink_Calc);


  if bHasResult then
    ExitProcess(1)
  else
    ExitProcess(0);


end;
      


end.

