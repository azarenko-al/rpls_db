unit i_link_calc;

interface

uses ADOInt,

  u_dll;

type


  ILink_calcX = interface(IInterface)
  ['{88EB6067-E8EA-440A-AC7E-8ABB47DDD543}']

    procedure InitADO(aConnection: _Connection); stdcall;
//    procedure InitAppHandle(aHandle: integer); stdcall;

//    procedure LoadFromFile(aFileName: WideString);  stdcall;
    procedure Execute(aFileName: WideString) ;  stdcall;
  end;

var
  ILink_calc: ILink_calcX;

  function Load_ILink_calc: boolean;
  procedure UnLoad_ILink_calc;


implementation

const
//  DEF_BPL_FILENAME = 'bpl_LOS.bpl';
  DEF_FILENAME = 'dll_Link_calc.dll';

var
  LHandle : Integer;


function Load_ILink_calc: boolean;
begin
  if LHandle>0 then
  begin
    Assert(Result, 'LHandle <=0');

    Result := False;
    Exit;
  end;


  Result := GetInterface_DLL(DEF_FILENAME, LHandle,
               ILink_calcX, ILink_calc)=0;
             //  ILink_calcX, ILink_calc, 'DllGetInterface_ILink_calc' )=0;

  Assert(Result, 'Value <=0');
end;


procedure UnLoad_ILink_calc;
begin
  ILink_calc := nil;
  UnloadPackage_dll(LHandle);
end;


end.



