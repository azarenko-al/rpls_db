unit dm_Idle;

interface

uses
  Windows, Messages, Variants, Classes, Graphics, Controls, Forms,
  DateUtils, Dialogs, IniFiles,

//  DbugIntf,

  u_func,
  u_files,
  u_run,

  u_debug,

  ShellAPI,

  SysUtils, ExtCtrls;



type
  TdmIdle = class(TDataModule)
    Timer1: TTimer;
    procedure DataModuleCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private

  public
    Fidle_time_minutes: Integer;
    Fidle_msg: string;

    FLastHookTime: TDateTime;

    procedure MouseMoveDone;

    class procedure Init;


  end;

var
  dmIdle: TdmIdle;

implementation

{$R *.dfm}


class procedure TdmIdle.Init;
begin
  if not Assigned(dmIdle) then
    Application.CreateForm(TdmIdle, dmIdle);

end;



// ---------------------------------------------------------------
procedure TdmIdle.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
var
  oIni: TIniFile;
  s: string;
  sFile: string;

begin
  FLastHookTime:=Now();

 // s:=ChangeFileExt( Application.ExeName, '.ini');

  sFile:=ChangeFileExt( Application.ExeName, '.ini');
  Assert (FileExists(sFile)); 

  //-----------------------------------------------

  oIni := TIniFile.Create (sFile);

  Fidle_time_minutes:= oIni.ReadInteger ('IDLE', 'time_minutes', 1);
  Fidle_msg         := oIni.ReadString  ('IDLE', 'msg', '������ ��������� ��������� ��-�� ������������ %s');

  FreeAndNil(oIni);


//   ShowMessage( IntToStr(Fidle_time_minutes) + ' minute');


end;


procedure TdmIdle.MouseMoveDone;
begin
  FLastHookTime:=Now();

  debug ('MouseMoveDone - '+ TimeToStr (FLastHookTime));

end;


// ---------------------------------------------------------------
procedure TdmIdle.Timer1Timer(Sender: TObject);
// ---------------------------------------------------------------
//var
 // iMaxMinutes: Integer;
 // myDate : TDateTime;

var
  s: string;
  sFile: string;
begin
  exit;



//  debug ('MouseMoveDone - '+ TimeToStr (FLastHookTime));



  s:='������ - '+ FormatDateTime('hh:nn:ss', Now() - FLastHookTime); // DiffTimeStr:=

  debug (s);

  //function EncodeTime(Hour, Min, Sec, MSec: Word): TDateTime;


 // Fidle_time_minutes:=1;


//  iMaxMinutes:= 1; //;  StrToIntDef (ed_Minutes.Text,0);

  if CompareTime(Now() - FLastHookTime, EncodeTime(0, Fidle_time_minutes, 0, 0)) > 0 then
  begin
    Timer1.Enabled:=False;

//    debug ('exit');

    s:= Format(Fidle_msg , [FormatDateTime('hh:nn:ss', Now() - FLastHookTime) ]);

    sFile:=GetTempFileNameWithExt('.txt');

    StrToFile(s, sFile);

    ShellExecute(0, 'open', pChar(sFile), nil, nil, SW_SHOWNORMAL);


//    ShowMessage( IntToStr(Fidle_time_minutes) + ' minute');

  PostMessage(Application.MainForm.Handle, WM_CLOSE, 0,0);



//    Halt;
  end;


  // Set my date variable using the EncodeDateTime function
 // myDate := EncodeDateTime(0, 0, 0, 0, 3, 0, 0);

//  ed_Minutes.Text:= FormatDateTime('hh:nn:ss', myDate);

end;





end.


