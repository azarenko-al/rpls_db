program dll_CalcRegion_calc;

uses
  d_Calc in '..\src\d_Calc.pas' {dlg_Calc},
  dm_CalcRegion_export_calc in '..\src\dm_CalcRegion_export_calc.pas' {dmCalcRegion_export_calc: TDataModule},
  dm_CalcRegion_export_join in '..\src\dm_CalcRegion_export_join.pas' {dmCalcRegion_export_join: TDataModule},
  dm_CalcRegion_export_maps in '..\src\dm_CalcRegion_export_maps.pas' {dmCalcRegion_export_maps: TDataModule},
  dm_Main_app__calc_region in '..\src\dm_Main_app__calc_region.pas' {dmMain_app: TDataModule},
  Forms,
  u_Calc_Types in '..\src\u_Calc_Types.pas',
  u_CalcParams_ in '..\src\u_CalcParams_.pas',
  u_CalcParams_Loader in '..\src\u_CalcParams_Loader.pas',
  u_CalcParams_Saver in '..\src\u_CalcParams_Saver.pas',
  u_CalcRegion_export_types in '..\src\u_CalcRegion_export_types.pas',
  u_custom_DB_error_log in '..\src\u_custom_DB_error_log.pas',
  u_Matrix_base in '..\..\..\..\common\Matrix\u_Matrix_base.pas',
  u_bin_to_mif_params in '..\src\u_bin_to_mif_params.pas',
  I_CalcRegion_calc in '..\src shared\I_CalcRegion_calc.pas',
  u_CalcRegion_calc in '..\src\u_CalcRegion_calc.pas',
  u_Join_params in '..\src\u_Join_params.pas';

{$R *.RES}
 


 begin
  Application.Initialize;
  Application.CreateForm(TdmMain_app, dmMain_app);
  Application.Run;
end.
