unit I_pmp_CalcRegion_calc;

interface

uses ADOInt,

  u_dll;

type

  IPmp_CalcRegion_calc_X = interface(IInterface)
  ['{5B08D75F-D51B-402A-A32B-F3DF9E3177A0}']

    procedure InitADO(aConnection: _Connection); stdcall;
    procedure InitAppHandle(aHandle: integer); stdcall;

    procedure Execute (aFileName: WideString);  stdcall;


//    procedure Init(aAppHandle: Integer; aConnection: _Connection); stdcall;
//    procedure SetParams(aRec: TParams); stdcall;

  end;


  function Load_IPmp_CalcRegion_calc: Boolean;
  procedure UnLoad_IPmp_CalcRegion_calc;

var
  IPmp_CalcRegion_calc: IPmp_CalcRegion_calc_X;


implementation

const
  DEF_DLL_FILENAME = 'dll_Pmp_CalcRegion_calc.dll';

var
  LHandle : Integer;


function Load_IPmp_CalcRegion_calc: Boolean;
begin
  Result := GetInterface_dll(DEF_DLL_FILENAME, LHandle, IPmp_CalcRegion_calc_X, IPmp_CalcRegion_calc)=0;
end;


procedure UnLoad_IPmp_CalcRegion_calc;
begin
  IPmp_CalcRegion_calc := nil;
  UnloadPackage_dll(LHandle);
end;


end.



