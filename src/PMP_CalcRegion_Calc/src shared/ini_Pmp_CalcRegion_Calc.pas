unit ini_Pmp_CalcRegion_Calc;

interface
uses
   IniFiles, SysUtils, Dialogs,

   u_Assert
   ;

type
  // ---------------------------------------------------------------
  TIni_Pmp_CalcRegion_Calc_Params1 = class
  // ---------------------------------------------------------------
  private
 //  LinkID: integer;
  public
    ProjectID: Integer;

    ConnectionString: string;



 //   UsePassiveElements       : Boolean;
 //   UseAM  : Boolean;

   // IsCalcWithAdditionalRain : Boolean;


    procedure LoadFromFile(aFileName: string);
    procedure SaveToFile(aFileName: string);

    function Validate: boolean;
  end;


implementation


// ---------------------------------------------------------------
procedure TIni_Pmp_CalcRegion_Calc_Params1.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
begin
  AssertFileExists(aFileName);


  with TIniFile.Create(aFileName) do
  begin
    ProjectID  :=ReadInteger ('main', 'ProjectID',  0);
  //  LinkID     :=ReadInteger ('main', 'ID',  0);


    ConnectionString := ReadString ('main', 'ConnectionString',  '');


  //  UsePassiveElements   := ReadBool ('main', 'UsePassiveElements',  False);

    Free;
  end;
end;

// ---------------------------------------------------------------
procedure TIni_Pmp_CalcRegion_Calc_Params1.SaveToFile(aFileName: string);
// ---------------------------------------------------------------
begin
  with TIniFile.Create(aFileName) do
  begin
    WriteInteger ('main', 'ProjectID', ProjectID);

    WriteString ('main', 'ConnectionString', ConnectionString);

  //  WriteInteger ('main', 'ID',    LinkID);

  //  WriteBool ('main', 'UsePassiveElements',    UsePassiveElements);
  //  WriteInteger ('main', 'LinkID',    LinkID);


  //  UsePassiveElements       : Boolean;
   // IsCalcWithAdditionalRain : Boolean;

    Free;
  end;
end;




function TIni_Pmp_CalcRegion_Calc_Params1.Validate: boolean;
begin
  Result :=  (ProjectID>0);
//  Result := (LinkID>0) and (ProjectID>0);

//  if not Result then
 //   ShowMessage('(iLinkID=0) or (iProjectID=0)');

end;


end.
