program dll_CalcRegion_calc;

uses
  d_Calc in '..\src\d_Calc.pas' {dlg_Calc},
  dm_CalcMap in '..\..\CalcMap\dm_CalcMap.pas' {dmCalcMap: TDataModule},
  dm_CalcRegion_export_calc in '..\src\dm_CalcRegion_export_calc.pas' {dmCalcRegion_export_calc: TDataModule},
  dm_CalcRegion_export_join in '..\src\dm_CalcRegion_export_join.pas' {dmCalcRegion_export_join: TDataModule},
  dm_CalcRegion_export_maps in '..\src\dm_CalcRegion_export_maps.pas' {dmCalcRegion_export_maps: TDataModule},
  dm_Data_Calc in '..\src\dm_Data_Calc.pas' {dmData_calcregion: TDataModule},
  dm_Main_app__calc_region in '..\src\dm_Main_app__calc_region.pas' {dmMain_app: TDataModule},
  Forms,
  ini_Pmp_CalcRegion_Calc in '..\src shared\ini_Pmp_CalcRegion_Calc.pas',
  u_bin_to_mif_params in '..\src\u_bin_to_mif_params.pas',
  u_Calc_Types in '..\src\u_Calc_Types.pas',
  u_CalcParams_ in '..\src\u_CalcParams_.pas',
  u_CalcParams_Loader in '..\src\u_CalcParams_Loader.pas',
  u_CalcParams_Saver in '..\src\u_CalcParams_Saver.pas',
  u_CalcRegion_export_types in '..\src\u_CalcRegion_export_types.pas',
  u_Join_params in '..\src\u_Join_params.pas',
  u_Matrix_base in '..\..\..\..\common7\Matrix\u_Matrix_base.pas',
  u_shared_calc_manager_rec in '..\..\..\..\onega dll 7\Calc_Manager\src_shared\u_shared_calc_manager_rec.pas',
  Windows;

{$R *.RES}



 begin
  SetThreadLocale(1049);


  Application.Initialize;
  Application.CreateForm(TdmMain_app, dmMain_app);
  Application.Run;
end.
