object dmCalcRegion_export_calc: TdmCalcRegion_export_calc
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 777
  Top = 357
  Height = 417
  Width = 509
  object qry_CalcRegion: TADOQuery
    Parameters = <>
    Left = 31
    Top = 8
  end
  object qry_RelMatrix: TADOQuery
    Parameters = <>
    Left = 36
    Top = 224
  end
  object qry_Cells: TADOQuery
    Parameters = <>
    Left = 36
    Top = 96
  end
  object qry_Antennas: TADOQuery
    Filtered = True
    Parameters = <>
    Left = 36
    Top = 160
  end
  object qry_CalcModel_Params: TADOQuery
    Parameters = <>
    Left = 240
    Top = 64
  end
  object qry_Clutters: TADOQuery
    Parameters = <>
    Left = 240
    Top = 128
  end
  object sp_CalcModels: TADOStoredProc
    Parameters = <>
    Left = 240
    Top = 192
  end
  object sp_AntType: TADOStoredProc
    Parameters = <>
    Left = 240
    Top = 256
  end
  object qry_Poly: TADOQuery
    LockType = ltReadOnly
    Parameters = <>
    Left = 108
    Top = 304
  end
end
