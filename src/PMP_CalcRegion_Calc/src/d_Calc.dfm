inherited dlg_Calc: Tdlg_Calc
  Left = 1409
  Top = 301
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = 'dlg_Calc'
  ClientHeight = 453
  ClientWidth = 502
  OldCreateOrder = True
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 418
    Width = 502
    inherited Bevel1: TBevel
      Width = 502
    end
    inherited Panel3: TPanel
      Left = 323
      inherited btn_Ok: TButton
        Left = 339
      end
      inherited btn_Cancel: TButton
        Left = 423
      end
    end
    object cb_CloseAfterDone: TCheckBox
      Left = 4
      Top = 8
      Width = 173
      Height = 21
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086' '#1087#1086#1089#1083#1077' '#1088#1072#1089#1095#1077#1090#1072
      TabOrder = 1
    end
  end
  inherited pn_Top_: TPanel
    Width = 502
    inherited Bevel2: TBevel
      Width = 502
    end
    inherited pn_Header: TPanel
      Width = 502
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 60
    Width = 502
    Height = 301
    Align = alTop
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 2
    DesignSize = (
      502
      301)
    object Button1: TButton
      Left = 355
      Top = 9
      Width = 137
      Height = 25
      Action = act_Calc_EMP
      Anchors = [akTop, akRight]
      TabOrder = 0
    end
    object Button2: TButton
      Left = 355
      Top = 49
      Width = 137
      Height = 25
      Action = act_Join
      Anchors = [akTop, akRight]
      TabOrder = 1
    end
    object Button3: TButton
      Left = 355
      Top = 88
      Width = 137
      Height = 25
      Action = act_MakeMaps
      Anchors = [akTop, akRight]
      TabOrder = 2
    end
    object Button4: TButton
      Left = 355
      Top = 128
      Width = 137
      Height = 25
      Action = act_Update_KGR
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Visible = False
    end
    object GroupBox1: TGroupBox
      Left = 355
      Top = 164
      Width = 137
      Height = 133
      Anchors = [akTop, akRight]
      Caption = #1057#1090#1072#1085#1076#1072#1088#1090
      TabOrder = 4
      object lbox_Standards: TCheckListBox
        Left = 2
        Top = 15
        Width = 133
        Height = 116
        Align = alClient
        ItemHeight = 13
        Items.Strings = (
          'DCS1800'
          'GSM900'
          'EGSM900')
        TabOrder = 0
      end
    end
    object cxVerticalGrid1: TcxVerticalGrid
      Left = 5
      Top = 5
      Width = 329
      Height = 291
      Align = alLeft
      LookAndFeel.Kind = lfUltraFlat
      OptionsView.CellTextMaxLineCount = 3
      OptionsView.AutoScaleBands = False
      OptionsView.PaintStyle = psDelphi
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.RowHeaderMinWidth = 30
      OptionsView.RowHeaderWidth = 181
      OptionsView.ValueWidth = 92
      TabOrder = 5
      Version = 1
      object cxVerticalGrid1CategoryRow1: TcxCategoryRow
        Properties.Caption = #1056#1072#1089#1095#1077#1090' '#1069#1052#1055
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object row_Update_WS: TcxEditorRow
        Expanded = False
        Properties.Caption = #1056#1072#1089#1095#1077#1090' '#1084#1072#1090#1088#1080#1094' WS '#1079#1072#1085#1086#1074#1086
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.Alignment = taLeftJustify
        Properties.EditProperties.NullStyle = nssUnchecked
        Properties.EditProperties.ReadOnly = False
        Properties.EditProperties.ValueChecked = 'True'
        Properties.EditProperties.ValueGrayed = ''
        Properties.EditProperties.ValueUnchecked = 'False'
        Properties.DataBinding.ValueType = 'Boolean'
        Properties.Value = Null
        ID = 1
        ParentID = 0
        Index = 0
        Version = 1
      end
      object cxVerticalGrid1EditorRow2: TcxEditorRow
        Expanded = False
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = False
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 2
        ParentID = 0
        Index = 1
        Version = 1
      end
      object row_Join_calc: TcxEditorRow
        Expanded = False
        Properties.Caption = #1054#1073#1098#1077#1076#1080#1085#1080#1090#1100' '#1088#1072#1089#1095#1077#1090#1099
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.Alignment = taLeftJustify
        Properties.EditProperties.NullStyle = nssUnchecked
        Properties.EditProperties.ReadOnly = False
        Properties.EditProperties.ValueChecked = 'True'
        Properties.EditProperties.ValueGrayed = ''
        Properties.EditProperties.ValueUnchecked = 'False'
        Properties.DataBinding.ValueType = 'Boolean'
        Properties.Value = Null
        ID = 3
        ParentID = 0
        Index = 2
        Version = 1
      end
      object row_Calc_maps: TcxEditorRow
        Expanded = False
        Properties.Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1088#1090#1099
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.Alignment = taLeftJustify
        Properties.EditProperties.NullStyle = nssUnchecked
        Properties.EditProperties.ReadOnly = False
        Properties.EditProperties.ValueChecked = 'True'
        Properties.EditProperties.ValueGrayed = ''
        Properties.EditProperties.ValueUnchecked = 'False'
        Properties.DataBinding.ValueType = 'Boolean'
        Properties.Value = Null
        ID = 4
        ParentID = 0
        Index = 3
        Version = 1
      end
      object cxVerticalGrid1CategoryRow2: TcxCategoryRow
        Expanded = False
        ID = 5
        ParentID = 0
        Index = 4
        Version = 1
      end
      object cxVerticalGrid1CategoryRow3: TcxCategoryRow
        Properties.Caption = #1054#1073#1098#1077#1076#1080#1085#1077#1085#1080#1077' '#1088#1072#1089#1095#1077#1090#1086#1074
        ID = 6
        ParentID = -1
        Index = 1
        Version = 1
      end
      object row_NSignal_Calc: TcxEditorRow
        Properties.Caption = #1060#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1084#1072#1090#1088' N '#1089#1080#1075#1085#1072#1083#1086#1074
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.Alignment = taLeftJustify
        Properties.EditProperties.NullStyle = nssUnchecked
        Properties.EditProperties.ReadOnly = False
        Properties.EditProperties.ValueChecked = 'True'
        Properties.EditProperties.ValueGrayed = ''
        Properties.EditProperties.ValueUnchecked = 'False'
        Properties.DataBinding.ValueType = 'Boolean'
        Properties.Value = Null
        ID = 7
        ParentID = 6
        Index = 0
        Version = 1
      end
      object row_NSignal_Count: TcxEditorRow
        Expanded = False
        Properties.Caption = #1082#1086#1083'-'#1074#1086' '#1089#1080#1075#1085#1072#1083#1086#1074
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = False
        Properties.DataBinding.ValueType = 'Integer'
        Properties.Value = 6
        ID = 8
        ParentID = 7
        Index = 0
        Version = 1
      end
      object cxVerticalGrid1EditorRow7: TcxEditorRow
        Expanded = False
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = False
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 9
        ParentID = -1
        Index = 2
        Version = 1
      end
      object row_MapType: TcxEditorRow
        Expanded = False
        Properties.Caption = #1058#1080#1087' '#1082#1072#1088#1090
        Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.DropDownListStyle = lsEditFixedList
        Properties.EditProperties.DropDownRows = 7
        Properties.EditProperties.Items.Strings = (
          #1056#1072#1089#1090#1088#1086#1074#1099#1077
          #1042#1077#1082#1090#1086#1088#1085#1099#1077)
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = False
        Properties.EditProperties.Revertable = True
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = #1056#1072#1089#1090#1088#1086#1074#1099#1077
        ID = 10
        ParentID = -1
        Index = 3
        Version = 1
      end
    end
  end
  object StatusBar1: TStatusBar [3]
    Left = 0
    Top = 399
    Width = 502
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  inherited ActionList1: TActionList
    Left = 104
    inherited act_Ok: TAction
      Visible = False
    end
    inherited act_Cancel: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
    end
    object act_Calc_EMP: TAction
      Caption = #1056#1072#1089#1095#1077#1090' '#1069#1052#1055
      OnExecute = DoCalcAction
    end
    object act_Join: TAction
      Caption = #1054#1073#1098#1077#1076#1080#1085#1077#1085#1080#1077' '#1088#1072#1089#1095#1077#1090#1086#1074
      OnExecute = DoCalcAction
    end
    object act_MakeMaps: TAction
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1088#1090#1099
      OnExecute = DoCalcAction
    end
    object act_Update_KGR: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1082#1072#1088#1090#1099' '#1075#1088#1072#1085#1080#1094
      Hint = 
        #1054#1073#1085#1086#1074#1083#1103#1077#1090' '#1087#1086#1089#1095#1080#1090#1072#1085#1085#1099#1077' '#1042#1045#1050#1058#1054#1056#1053#1067#1045' '#1082#1072#1088#1090#1099' '#1075#1088#1072#1085#1080#1094', '#13#10#1076#1086#1073#1072#1074#1083#1103#1103' '#1090#1091#1076#1072' '#1080#1085 +
        #1092#1086#1088#1084#1072#1094#1080#1102' '#1087#1086' CellID '#1089#1077#1082#1090#1086#1088#1086#1074' '#1080' '#1072#1076#1088#1077#1089#1091' '#1041#1057'.'
      OnExecute = DoCalcAction
    end
  end
  inherited FormStorage1: TFormStorage
    StoredProps.Strings = (
      'cb_CloseAfterDone.Checked')
  end
end
