unit d_Calc;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, ActnList, StdCtrls, ExtCtrls,
  cxVGrid, cxControls, cxInplaceContainer, rxPlacemnt, cxPropertiesStore, CheckLst, Registry,

  dm_Main,

  u_vars,
  d_Wizard,

  u_cx_vgrid,

//  I_Shell,

 // u_custom_DB_error_log,
  //fr_Calc_cells,

  dm_ColorSchema,
  dm_CalcRegion,

 // dm_Act_ColorSchema,

 // u_Log,
  u_func,

  u_files,
  u_dlg,
  u_classes,

  u_const_str,

  u_const_db,
  u_const,

  dm_CalcRegion_export_calc,
  dm_CalcRegion_export_maps,
  dm_CalcRegion_export_join,


   ComCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters, cxStyles,
  cxEdit, cxCheckBox, cxTextEdit, cxDropDownEdit

  ;



type
  Tdlg_Calc = class(Tdlg_Wizard)
    act_Calc_EMP: TAction;
    act_Join: TAction;
    act_MakeMaps: TAction;
    cb_CloseAfterDone: TCheckBox;
    act_Update_KGR: TAction;
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    GroupBox1: TGroupBox;
    lbox_Standards: TCheckListBox;
    cxVerticalGrid1: TcxVerticalGrid;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    row_Update_WS: TcxEditorRow;
    cxVerticalGrid1EditorRow2: TcxEditorRow;
    row_Join_calc: TcxEditorRow;
    row_Calc_maps: TcxEditorRow;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;
    cxVerticalGrid1CategoryRow3: TcxCategoryRow;
    row_NSignal_Calc: TcxEditorRow;
    row_NSignal_Count: TcxEditorRow;
    cxVerticalGrid1EditorRow7: TcxEditorRow;
    row_MapType: TcxEditorRow;
    StatusBar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
//    procedure RowButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure DoCalcAction(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
  private
    FCalcRegionID: integer;
    FStandardIDs: TIDList;

//    FMapTypeIndex: integer;

  //  FRegPath: string;

  //   FParams: TCalcParamsRec;

  //-------------------------------------------------------------------
    FParams: record
    //-------------------------------------------------------------------
    //  CalcRegionsArr : TIntArray;

      //Actions
     // ActionType: (atCalcEMP, atJoin, atMakeMaps);
    //  IsDoCalc: boolean;
      IsRecalcWS: boolean;

      IsJoin_Calc: boolean;
      IsCalc_Maps: boolean;
      UpdateWS   : boolean;

      MapType: (mtNone, mtVector_, mtRaster_); //TdmCalcRegion_export_mapsMapType;

   //   IsUpdateKgr: boolean;

      IsMakeNBestMatrix,
      IsMakeHandOffMatrix: boolean;

      NBestMatrixCount: integer;

      ColorSchemaID_KUP: integer;
      ColorSchemaID_KMO: integer;
      ColorSchemaID_Handover: integer;

    end;

//    procedure PrepareStandards();

    procedure SaveParams();

  public
//    class function ExecDlg (var aRec: TCalcParamsRec): boolean;
    class function ExecDlg (aID: integer): boolean;
  end;


//=========================================================
implementation {$R *.DFM}
//=========================================================

const

  TEST_CALC_FILE_NAME     = 'calc_params.xml';
  TEST_JOIN_FILE_NAME     = 'calc_manager.xml';
  TEST_MAKE_MAPS_FILE_NAME= 'bin_to_mif.xml';

 // COLOR_SCHEMA_HANDOVER = 'HANDOVER';
  COLOR_SCHEMA_KMO      = 'KMO';
  COLOR_SCHEMA_KUP      = 'KUP';


//--------------------------------------------------------------------
class function Tdlg_Calc.ExecDlg;
//--------------------------------------------------------------------
var
  i: Integer;
 // sCalcRegType: string;
begin
  with Tdlg_Calc.Create(Application) do
  try
    FCalcRegionID:=aID;

(*    sCalcRegType:=gl_db.GetStringFieldValueByID(TBL_PMP_CALCREGION, FLD_TYPE, aID);

    if sCalcRegType = 'gsm' then
    begin
      dmCalcRegion_export_calc.CalcRegionTypeIsGSM:=true;
      dmCalcRegion_export_Maps.CalcRegionTypeIsGSM:=true;
      dmCalcRegion_export_join.CalcRegionTypeIsGSM:=true;
    end   else
*)
//    if sCalcRegType = 'pmp' then
  (*  begin
      dmCalcRegion_export_calc.CalcRegionTypeIsGSM:=false;
      dmCalcRegion_export_Maps.CalcRegionTypeIsGSM:=false;
      dmCalcRegion_export_join.CalcRegionTypeIsGSM:=false;
    end;
*)
    //   else
     //   Raise Exception.Create('CalcRegion Exception: no CalcRegion Type');

  //  oIDList:=TIDList.Create;

    dmCalcRegion_pmp.GetCellLayerList_used(FCalcRegionID, FStandardIDs);

    for i := 0 to FStandardIDs.Count-1 do
    begin
      lbox_Standards.Items.AddObject (FStandardIDs[i].NAME, Pointer(FStandardIDs[i].ID));
      lbox_Standards.Checked[i]:=True;
    end;

  //  oIDList.Free;
{
  oQry_Temp:= TAdoQuery.Create(nil);
  oQry_Temp.Connection:= dmMain.AdoConnection;

  try
    db_Ope nQuery(oQry_Temp, 'SELECT * FROM '+TBL_CELLLAYER+
                            ' WHERE name in (''DCS1800'', ''GSM900'', ''EGSM900'')');
  except
  end;

  lbox_Standards.Items.Clear;
  with oQry_Temp do
    while not EOF do
  begin
    lbox_Standards.Items.AddObject (oQry_Temp[FLD_NAME], Pointer(oQry_Temp.FieldByName(FLD_ID).AsInteger));

    Next;
  end;

  oQry_Temp.Free;

}

    Result:=(ShowModal=mrOk);

{    if Result then
    begin
      SaveParamsToRec();
      aRec:=FParams;
    end;}

  finally
    Free;
  end;
end;

//-------------------------------------------------------------------
procedure Tdlg_Calc.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
//var
 // oQry_Temp: TAdoQuery;
begin
  inherited;

 // Assert(Assigned(IShell));           
               

 ///////// FormStyle:=fsStayOnTop;
  btn_Cancel.Caption:= '�������';

  lbox_Standards.Items.Clear;

  FStandardIDs:= TIDList.Create;

  Caption:= STR_CALC;
  SetActionName(STR_CALC);

 // FRegPath:=REGISTRY_FORMS + ClassName;

//  FRegPath:=REGISTRY_COMMON_FORMS+ClassName+'\';

 // AddComponentProp();



{  with gl_Reg do
  begin
    CurrentRegPath:=FRegPath;

    BeginGroup (Self, Name);
    AddControl (lbox_Standards, ['checked']);
  end;
}
 // AddComponentProp(lbox_Standards, 'checked');

  AddComponentProp(row_Join_calc, DEF_PropertiesValue);
  AddComponentProp(row_Calc_maps, DEF_PropertiesValue);
  AddComponentProp(row_Update_WS, DEF_PropertiesValue);
  AddComponentProp(row_NSignal_Calc, DEF_PropertiesValue);
  AddComponentProp(row_NSignal_Count, DEF_PropertiesValue);

 // AddComponentProp(row_Calc_Handover, 'Text');

//  AddComponentProp(row_IsUpdateKgr,   'Text');

  cxPropertiesStore.RestoreFrom;


  with TRegIniFile.Create(FRegPath) do
  begin
    row_Update_WS.properties.Value            :=ReadString('', row_Update_WS.Name, 'False');
                                    

    row_Join_calc.properties.Value            :=ReadString('', row_Join_calc.Name, 'False');
    row_Calc_maps.properties.Value            :=ReadString('', row_Calc_maps.Name, 'False');

    row_NSignal_Calc.properties.Value         :=ReadString('', row_NSignal_Calc.Name, 'False');
    row_NSignal_Count.properties.Value        :=ReadString('', row_NSignal_Count.Name, '6');

 //   row_Calc_Handover.properties.Value        :=ReadString('', row_Calc_Handover.Name, 'False');
   // row_IsUpdateKgr.properties.Value          :=ReadString('', row_IsUpdateKgr.Name, 'False');


//    FMapTypeIndex                 :=ReadInteger('', row_MapType1.Name, 0);

 //   FParams.ColorSchemaID_KMO     :=ReadInteger('', row_KMO_Schema.Name, 0);
   // FParams.ColorSchemaID_Handover:=ReadInteger('', row_Handover_Schema.Name, 0);
    Free;
  end;


  cx_InitVerticalGrid(cxverticalGrid1);

  StatusBar1.SimpleText:='Onega: '+ dmMain.LoginRec.ConnectionStatusStr;// GetConnectionStatusStr;


 // row_MapType1.Properties.Value :=row_MapType1.Items[FMapTypeIndex];


//  row_KMO_Schema.Text     := gl_DB.GetNameByID (TBL_COLOR_SCHEMA, FParams.ColorSchemaID_KMO);
//  row_Handover_Schema.Text:= gl_DB.GetNameByID (TBL_COLORSCHEMA, FParams.ColorSchemaID_Handover);

end;


//-------------------------------------------------------------------
procedure Tdlg_Calc.FormDestroy(Sender: TObject);
//-------------------------------------------------------------------
begin
  FStandardIDs.Free;

  with TRegIniFile.Create(FRegPath) do
//  with gl_Reg do
  begin
  //  CurrentRegPath:=FRegPath;

    WriteBool('', row_Update_WS.Name, row_Update_WS.Properties.Value);

    WriteBool('', row_Join_calc.Name, row_Join_calc.Properties.Value);
    WriteBool('', row_Calc_maps.Name, row_Calc_maps.Properties.Value);

    WriteBool('', row_NSignal_Calc.Name,  row_NSignal_Calc.Properties.Value);

    WriteInteger('', row_NSignal_Count.Name, row_NSignal_Count.Properties.Value);

  //  WriteString('', row_Calc_Handover.Name, row_Calc_Handover.Text);

   // WriteString('', row_IsUpdateKgr.Name, row_IsUpdateKgr.Text);


  //  WriteInteger('', row_MapType1.Name,  row_MapType1.Items.IndexOf(row_MapType.Text));

  //  WriteInteger('', row_KMO_Schema.Name,      FParams.ColorSchemaID_KMO);
  //  WriteInteger('', row_Handover_Schema.Name, FParams.ColorSchemaID_Handover);
    Free;
 //
  end;

 // gl_Reg.SaveAndClearGroup (Self);

  inherited;
end;



//-------------------------------------------------------------------
procedure Tdlg_Calc.SaveParams();
//-------------------------------------------------------------------
var
  iValue: integer;
begin
  with FParams do
  begin
  //  IsRecalcWS   := row_Update_WS.Properties.Value; //  IIF(row_Update_WS.Text=row_Update_WS.ValueChecked, True, False);

    IsRecalcWS   := AsBoolean(row_Update_WS.Properties.Value); //  IIF(row_Update_WS.Text=row_Update_WS.ValueChecked, True, False);

    IsJoin_Calc  := AsBoolean(row_Join_calc.Properties.Value); // IIF(row_Join_calc.Text=row_Join_calc.ValueChecked, True, False);
    IsCalc_Maps  := AsBoolean(row_Calc_maps.Properties.Value); //       IIF(row_Calc_maps.Text=row_Calc_maps.ValueChecked, True, False);
  //  IsUpdateKgr  := IIF(row_IsUpdateKgr.Text=row_IsUpdateKgr.ValueChecked, True, False);

    iValue := AsInteger(row_MapType.Properties.Value);

    case iValue of
      0: MapType := mtRaster_;
      1: MapType := mtVector_;
    else
      raise Exception.Create(''); 
    end;

  //  MapType  := row_MapType1.Properties.Value; //      IIF(Eq(row_MapType.Text, row_MapType.Items[0]), mtRaster, mtVector);


    IsMakeNBestMatrix  := AsBoolean(row_NSignal_Calc.Properties.Value); //   IIF(row_NSignal_Calc.Text=row_NSignal_Calc.ValueChecked, True, False);
    NBestMatrixCount   := AsInteger(row_NSignal_Count.Properties.Value);


    ColorSchemaID_KUP:= dmCalcRegion_pmp.GetIntFieldValue (FCalcRegionID, FLD_KUP_ID);
    ColorSchemaID_KMO:= dmCalcRegion_pmp.GetIntFieldValue (FCalcRegionID, FLD_KMO_ID);

    if ColorSchemaID_KUP = 0 then
      ColorSchemaID_KUP:= dmColorSchema.GetIDByName(COLOR_SCHEMA_KUP);

    if ColorSchemaID_KUP = 0 then
    begin
      MsgDlg('�� ������ �������� ����� ��� ����� ������ ����');
      ExitProcess(0);
    end;

    if ColorSchemaID_KMO = 0 then
      ColorSchemaID_KMO:= dmColorSchema.GetIDByName(COLOR_SCHEMA_KMO);

  //  if ColorSchemaID_Handover = 0 then
 //     ColorSchemaID_Handover:= dmColorSchema.GetIDByName(COLOR_SCHEMA_HANDOVER);


  //  IsMakeHandOffMatrix:= IIF(row_Calc_Handover.Text=row_Calc_Handover.ValueChecked, True, False);

   // CalcRegionsArr:=Fframe_Calc_cells.GetCheckedRegionsID();
  end;



  with dmCalcRegion_export_calc.Params do
  begin
(*    with mem_Matrixes do
    begin
      First;
      while not EOF do
      begin
        if not FieldByName(FLD_CHECKED).AsBoolean then
          ExcludeMatrixesIDList.AddName (FieldValues[FLD_ID], FieldValues[FLD_NAME]);

        Next;
      end;
    end;*)

    CellLayerIDList:=FStandardIDs;
    IsRecalcWS     :=FParams.IsRecalcWS;
  end;


  with dmCalcRegion_export_join.Params do
  begin
    IsMakeNBestMatrix  := FParams.IsMakeNBestMatrix;
    NBestMatrixCount   := FParams.NBestMatrixCount;

    IsMakeHandOffMatrix:= FParams.IsMakeHandOffMatrix;
    CellLayerIDList:=FStandardIDs;
  end;


  with dmCalcRegion_export_maps.Params do
  begin
    ColorSchemaID_KUP      := FParams.ColorSchemaID_KUP;
    ColorSchemaID_KMO      := FParams.ColorSchemaID_KMO;
//    ColorSchemaID_Handover := FParams.ColorSchemaID_Handover;

    case FParams.MapType of
      mtVector_:  MapType:=mtVector;
      mtRaster_:  MapType:=mtRaster;
    end;

//    MapType                := FParams.MapType;


//    if MapType = mtVector then
//    IsUpdateKgr            := FParams.IsUpdateKgr;
   //  CellLayerIDList:=FStandardIDs;
  //  else
    //  IsUpdateKgr          := false;
  end;

{ //with FParams do
  begin
   // CalcRegionsArr:=Fframe_Calc_cells.GetCheckedRegionsID();
  end;}

end;




//-------------------------------------------------------------------
procedure Tdlg_Calc.DoCalcAction(Sender: TObject);
//-------------------------------------------------------------------
   //-----------------------------------------------
   function DoCalcEMP: boolean;
   var
     iCode: Integer;
     sFile: string;
   begin
     sFile := g_ApplicationDataDir + TEST_CALC_FILE_NAME;

     dmCalcRegion_export_calc.Execute_SaveToXMLFile (FCalcRegionID,  sFile);


   (*  if custom_DbErrorLog.Count>0 then
     begin
       custom_DbErrorLog.Save;
       if not custom_DbErrorLog.Show then
         exit;
//       custom_DbErrorLog.Count:= 0;
     end;*)

      Result :=RunApp (EXE_RPLS_CALC, DoubleQuotedStr(sFile), iCode);
   end;

   //-----------------------------------------------
   function DoJoin: boolean;
   //-----------------------------------------------
   var
     iCode: Integer;
     sFile: string;
   begin
     sFile := g_ApplicationDataDir + TEST_JOIN_FILE_NAME;

     dmCalcRegion_export_join.Execute_SaveToXMLFile (FCalcRegionID,  sFile);

(*     if custom_DbErrorLog.Count>0 then begin
       custom_DbErrorLog.Save;
       if not custom_DbErrorLog.Show then
         exit;
//       custom_DbErrorLog.Count:=0;
     end;
*)

     Result :=RunApp (EXE_RPLS_JOIN, DoubleQuotedStr(sFile), iCode);
   end;

   //-----------------------------------------------
   function DoMap: boolean;
   //-----------------------------------------------
   var
     iCode: Integer;
     sFile: string;
   begin
     sFile := g_ApplicationDataDir + TEST_MAKE_MAPS_FILE_NAME;

     Result:= false;
     if dmCalcRegion_export_Maps.Execute_SaveToXMLFile (FCalcRegionID,  sFile) then
     begin

(*       if custom_DbErrorLog.Count>0 then begin
         custom_DbErrorLog.Save;
         if not custom_DbErrorLog.Show then
         exit;
//         custom_DbErrorLog.Count:=0;
       end;
*)
       Result :=RunApp (EXE_RPLS_BIN_TO_MIF, DoubleQuotedStr(sFile), iCode);
     end;

     if Result then
       dmCalcRegion_export_Maps.RegisterMaps (FCalcRegionID);
   end;

   //-----------------------------------------------
   function DoValidateColorSchemaParams: boolean;
   //-----------------------------------------------
   var
     sMsg: string;
   begin
     Result:= true;

     sMsg:= '�� ������ �������� ����� ��� ����: ';

     if FParams.ColorSchemaID_KUP = 0 then
     begin
       AppendStr(sMsg, '   ������ ����   ');
       Result:= false;
     end;

     if FParams.ColorSchemaID_KMO = 0 then
     begin
       AppendStr(sMsg, '   ��������   ');
       Result:= false;
     end;

     if FParams.IsMakeHandOffMatrix then
       if FParams.ColorSchemaID_Handover = 0 then
       begin
         AppendStr(sMsg, '   handover');
         Result:= false;
       end;

     if not Result then
     begin
       AppendStr(sMsg, CRLF+'��������������� ����� �� ����� ������������ ���������');
       MsgDlg(sMsg);
     end;
   end;

{   //-----------------------------------------------
   function DoUpdateKgr: boolean;
   begin
     Result:= false;

     if dmCalcRegion.GetRegionType(FCalcRegionID)<>crtGSM then
     begin
       MsgDlg('���������� ���� ������ �������� ������ ��� ������� ������� GSM');
       exit;
     end;

     if FParams.MapType <> mtVector then
     begin
       MsgDlg('���������� ���� ������ �������� ������ ��� ���� ���������� ����');
       exit;
     end;

     dmCalcRegion_export_Maps.UpdateKgr (FCalcRegionID);
     Result:= true;
   end;
}


var
  bool: boolean;
  CalcTime: double;
label
  exit_proc;

begin
 // custom_DbErrorLog.Clear();

  SaveParams();

  //--------------------------
  if Sender = act_Calc_EMP then
  //--------------------------
  begin
    if FParams.IsCalc_Maps then
      if not DoValidateColorSchemaParams() then
        exit;

    bool:= DoCalcEMP();
    if not bool then GoTO exit_proc;

    if FParams.IsJoin_Calc then begin
      bool:= DoJoin();
      if not bool then GoTO exit_proc;
    end;

    if FParams.IsCalc_Maps then begin
      bool:= DoMap();
      if not bool then GoTO exit_proc;

{      if FParams.IsUpdateKgr then begin
        bool:= DoUpdateKgr();
        if not bool then GoTO exit_proc;
      end;
}
    end;
  end;

  //--------------------------
  if Sender = act_Join then
  //--------------------------
  begin
    if FParams.IsCalc_Maps then
      if not DoValidateColorSchemaParams() then
        exit;

    bool:=DoJoin();
 //   if not bool then GoTO exit_proc;

    if FParams.IsCalc_Maps then begin
      bool:=DoMap();
      if not bool then GoTO exit_proc;

 {     if FParams.IsUpdateKgr then begin
        bool:= DoUpdateKgr();
        if not bool then GoTO exit_proc;
      end;
}
    end;
  end;

  //--------------------------
  if Sender = act_MakeMaps then
   //--------------------------
  begin
    if not DoValidateColorSchemaParams() then
      exit;

    bool:=DoMap();
    if not bool then GoTO exit_proc;

{
    if FParams.IsUpdateKgr then begin
      bool:= DoUpdateKgr();
      if not bool then GoTO exit_proc;
    end;

}
  end;


{  //--------------------------
  if Sender = act_Update_KGR then
   //--------------------------
  begin
    bool:= DoUpdateKgr();
    if not bool then
      exit;      
  end;
}


  exit_proc:

  if cb_CloseAfterDone.Checked then
    Close;
end;


procedure Tdlg_Calc.act_OkExecute(Sender: TObject);
begin
  inherited;
  //
end;


end.


