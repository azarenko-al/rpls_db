unit u_CalcRegion_export_types;

interface
uses
  u_geo;

type
  TRegionRec_ = record
     ID : Integer;


     Name: string;
     CalcDirection: (cdBS_to_PO, cdPO_to_BS);
     ClutterModel_ID: integer;

     CalcModel_ID: integer;
     K0,K0_open,K0_closed: double;


     AntennaIsOnRoof: boolean;
     Refraction: double;
     CalcStep: integer;
   //  Level_Stored_Noise : integer;

     BLRect: TBLRect;
     XYRect: TXYRect;
     Zone6: integer;

     Abonent: record
       Height          : double;
       Power_W         : double;
       Sensitivity  : double;
       FeederLoss   : double;
       Gain         : double;
     end;
  end;


type
  TCellRec_ = record
      ID             : integer;
      NAME           : string;
      Combiner_Loss: double;

      CalcModel_ID   : integer;
      CalcModel_name : string;

      CELL_LAYER_NAME: string;

      SITE_ID        : integer;
      SITE_NAME      : string;

      CELLID          : string;

      K0,
      K0_open,
      K0_closed      : double;

      CALC_RADIUS_km  : double; //������ ������� �������
      Radius_signal_km: Double; //������ ������� �������

      KupFileName    : string; // ��� ����� � ���������

      TRX_POWER_W     : double;
      TRX_Sensitivity : double;
      TRX_FREQ_MHz    : double;
  end;


  TCellAntennaRec_ = record
     ID               : integer;
     AntennaType_ID  : integer;
    // feederloss       : double;
     height           : double;
     azimuth          : double;
     Tilt             : double;
//     Heel             : double;
     FeederLoss    : double;
     gain          : double;
                                
     CalcRadius_KM    : Double;

     blPos            : TBLPoint;
     xyPos            : TXYPoint;

   //  ZONENUM          : integer;
     ZoneNum          : integer;

     plFileName       : string;
  end;

implementation

end.
