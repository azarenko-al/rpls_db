unit dm_Main_app__calc_region;

interface

uses
  SysUtils, Classes, Dialogs,IniFiles,

  ini_Pmp_CalcRegion_Calc,

  d_Calc,
  u_vars,

 // dm_Act_Explorer,
 // dm_Act_ColorSchema,

  //u_func_reg_lib,
  
  
 
  dm_Main
  ;

type
  TdmMain_app = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
  end;


var
  dmMain_app: TdmMain_app;

//===================================================================
implementation {$R *.DFM}
//===================================================================



//--------------------------------------------------------------
procedure TdmMain_app.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const
  TEMP_FILENAME = 'CalcRegion_calc.ini';

label
  exit_lbl;

var
 // iProjectID,
  iCalcRegionID: integer;
  iProjectID: Integer;
  sIniFileName: string;

  oIni: TIni_Pmp_CalcRegion_Calc_Params1;

  oInifile: TIniFile;
begin
  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
    sIniFileName:=g_ApplicationDataDir + TEMP_FILENAME;


  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;


  oIni:=TIni_Pmp_CalcRegion_Calc_Params1.Create;
  oIni.LoadFromFile(sIniFileName);

//  iProjectID:=oIni.ProjectID;


  oInifile:=TIniFile.Create (sIniFileName);

  iProjectID   :=oInifile.ReadInteger ('main', 'ProjectID',  0);
  iCalcRegionID:=oInifile.ReadInteger ('main', 'CalcRegionID',  0);

  oInifile.Free;

  if (iCalcRegionID=0) then
  begin
//    ShowMessage('(iProjectID=0) or (iCalcRegionID=0)');
    ShowMessage(' (CalcRegionID=0)');
    Exit;
  end;

//  Assert (iProjectID<>0);
//  Assert (iCalcRegionID<>0);


 // gl_Reg:=TRegStore_Create (REGISTRY_ROOT);


  TdmMain.Init;

  if not dmMain.Open_ADOConnection (oIni.ConnectionString) then
    Exit;



//  if not dmMain.OpenDB_reg then
//    Exit;


  {$DEFINE use_dll111}

  {$IFDEF use_dll}
(*
//  /
   IPmp_CalcRegion_calc_X
  if Load_Pmp_CalcRegion_calc() then
  begin
    IPmp_CalcRegion_calc.InitADO(dmMain.ADOConnection1.ConnectionObject);
 //   ILink_graph.InitAppHandle(Application.Handle);
    Ibee_sdb_import.Execute(sIniFileName);

    UnLoad_Ibee_sdb_import();
  end;

  Exit;*)

  {$ENDIF}


  Assert (iProjectID>0);

  dmMain.ProjectID:=iProjectID;

//
 // TdmAct_Explorer.Init;
//  TdmAct_ColorSchema.Init;

 
  Tdlg_Calc.ExecDlg (iCalcRegionID);


 // dmCalcRegion_Calc.Execute (iID);

exit_lbl:

  FreeAndNil (oIni);


{
  if bIsConverted then
    ExitProcess(1)
  else
    ExitProcess(0);
 }
end;





end.
