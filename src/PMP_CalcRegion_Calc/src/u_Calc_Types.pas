unit u_Calc_Types;

interface
uses u_func;

type
  //-------------------------------------------------------------------
  TCalcParamsRec = record
  //-------------------------------------------------------------------
    CalcRegionsArr : TIntArray;

    //Actions
    ActionType: (atCalcEMP, atJoin, atMakeMaps);
    IsDoCalc: boolean;

    IsJoin_Calc: boolean;
    Calc_Maps: boolean;
    UpdateWS : boolean;

    Calc_NSignal,
    Calc_Handover: boolean;

    Count_Signal: integer;

    ColorSchemaID_KUP: integer;
    ColorSchemaID_Handover: integer;
  end;

  TCalcMapType  = (mtKmo, mtKup, mtKgr, mtHandover);
  TCalcMapTypes = set of TCalcMapType;



implementation

end.
 