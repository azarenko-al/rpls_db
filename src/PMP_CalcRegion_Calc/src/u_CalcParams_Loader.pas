unit u_CalcParams_Loader;

interface

uses Variants,

XMLDoc, XMLIntf,u_xml_document,

    u_Func_arrays,

   u_func,
   u_Geo,
   u_Geo_convert_new,


   u_CalcParams_;

type
    TCalcParamsLoader = class
    private
      procedure Antenna_LoadFromXML(aCell: TcmpCell; aParentNode: IXMLNode; aZone:
          integer; aMaxXYBounds: TXYRect);
//      procedure Clear; override;

      procedure CalcModelType_LoadFromXML(aParentNode: IXMLNode);
   (*   Clutters:  array [TcalcClutterType] of record
                    Height : integer; // ������ �� ���������
                    Loss   : double;  // �������� ���������
                    Reflection_coef : double; //����. ���������
                  end;*)

      procedure CalcRegion_LoadFromXML(aParentNode: IXMLNode);
      procedure Cell_LoadFromXML(aParentNode: IXMLNode; aZone: integer; aMaxBounds:
          TXYRect; aStart, aFin : integer);
    public
      CalcParamsRef: TCalcParams;

      procedure AntennaTypeList_LoadFromXML(aParentNode: IXMLNode);

      function LoadFromXML(aFileName: string): boolean;
    end;



implementation

//-------------------------------------------------
//function TCalcParams.Validate(): boolean;
//-------------------------------------------------
//var
//  i,j,id: integer;
//  oAntenna: TcmpAntenna;
//  oCalcModel: TcmpCalcModelType;
//  sMsg: string;
//begin
//  Result:=False;
//
//  if not (CalcType in [ctCalibrate,ctCalibrateRegion]) then
//  begin
//    oCalcModel:=CalcModels.FindByID(CalcRegion.CalcModel_ID);
//    if (not Assigned(oCalcModel)) and (not IsLosOnly) then begin
//      if g_IsShowMessage then
//        ErrorDlg('� ������ �� ������ ������ �������')
//      else
//        custom_error_log.AddError('TCalcParams.Validate', '� ������ �� ������ ������ �������');
//      ErrorDlg ('� ������ �� ������ ������ �������');
//      Terminated:=True;
//      Exit;
//    end;
//
//    if (not Assigned(oCalcModel)) and (IsLosOnly) then
//    begin
//      oCalcModel := TcmpCalcModelType.Create;
//      oCalcModel.ID :=0;
//      oCalcModel.ModelType := cmtDetermin;
//    end;
//
//  end;
//
  //-------------------------------------------------------------------
  // check antennas
  //-------------------------------------------------------------------
//  for i:=0 to Cells.Count-1 do
//  for j:=0 to Cells[i].Antennas.Count-1 do
//  begin
//    oAntenna:=Cells[i].Antennas[j];
//
//    oAntenna.AntennaType:=AntennaTypes.FindByID (oAntenna.AntennaType_ID);
//
//    if (not Assigned(oAntenna.AntennaType)) and (not (IsLosOnly)) then begin
//      sMsg:= Format('����������� ������ ������� c������ %s ������� %s ',
//                                [Cells[i].Name_, Cells[i].SiteName]);
//      if g_IsShowMessage then
//        ErrorDlg(sMsg)
//      else
//        custom_error_log.AddError('TCalcParams.Validate', sMsg);
//
//      Terminated:=True;
      //ErrorDlg (sMsg);
//      Exit;
//    end;
//  end;
//
  //-------------------------------------------------------------------
  // check cell calc model
  //-------------------------------------------------------------------
//  if not (CalcType in [ctCalibrate,ctCalibrateRegion]) then
//    for i:=0 to Cells.Count-1 do
//    begin
//      with Cells[i] do
//      begin
//        if CalcModel_ID=0 then
//        begin
//          CalcModel_ID:=CalcRegion.CalcModel_ID;
//          K0_open  :=CalcRegion.K0_open;
//          K0_closed:=CalcRegion.K0_closed;
//          K0       :=CalcRegion.K0;
//        end;
//
//        if not IsLosONLY then
//        begin
//
//          oCalcModel:=CalcModels.FindByID(CalcModel_ID);
//
//          if not Assigned(oCalcModel) then begin
//            if g_IsShowMessage then
//              ErrorDlg('�� ���������� ������ �������: ������ ' + Cells[i].Name_)
//            else
//              custom_error_log.AddError('TCalcParams.Validate', '�� ���������� ������ �������: ������ ' + Cells[i].Name_);
            //ErrorDlg ('�� ���������� ������ �������: ������ ' + Cells[i].Name_);
//            Terminated:=True;
//            Exit;
//          end;
//
//          CalcModel_name:=oCalcModel.Name;
//          CalcModelLink:=oCalcModel;
//        end;
//      end;
//    end;
//
//
//  Result:=True;
//end;
//
//
//
//
//
//
//-------------------------------------------------------------------
//procedure TCalcParams.PrepareCellCalcModelData;
//-------------------------------------------------------------------
//var i,j: integer;
//begin
//  for i:=0 to Cells.Count-1  do
//  for j:=0 to CalcModels.Count-1  do
//    db_AddRecord (Cells[i].Calibrate.CalcModelMemDB,
//       [db_Par(FLD_ID,   CalcModels[j].ID),
//        db_Par(FLD_NAME, CalcModels[j].name),
//
//        db_Par(FLD_DXTREE_ID,  CalcModels[j].ID)
//       ]);
//end;
//
//-------------------------------------------------------------------
//procedure TCalcParams.LoadCellPointsFromFiles;
//-------------------------------------------------------------------
//var i: integer;
//begin
//  for i:=0 to Cells.Count-1  do
//    Cells[i].Calibrate.Points_.LoadFromFile (Cells[i].Calibrate.FileName);
//end;
//
//
//-------------------------------------------------------------------
//procedure TCalcParams.SaveToXML(aFileName: string);
//-------------------------------------------------------------------
//var i: integer;
//    vCell,vCells: Variant;
//begin
//  gl_XMLDoc.Clear;
//
//  vCells:=xml_AddNode (gl_XMLDoc.DocumentElement, 'cells');
//
//  for i:=0 to Cells.Count-1 do begin
//    vCell:=xml_AddNode (vCells, 'cell',
//           [xml_Par(ATT_ID, Cells[i].ID),
//            xml_Par('best_calc_model_id',   Cells[i].Calibrate.Summary.ModelID),
//            xml_Par('CKO',       TruncFloat(Cells[i].Calibrate.Summary.CKO,2)),
//            xml_Par('AveError',  TruncFloat(Cells[i].Calibrate.Summary.AveError,2)),
//
//            xml_Par('K0_open',   TruncFloat(Cells[i].Calibrate.Summary.K0_open, 2)),
//            xml_Par('K0_closed', TruncFloat(Cells[i].Calibrate.Summary.K0_closed, 2)),
//            xml_Par('K0',        TruncFloat(Cells[i].Calibrate.Summary.K0, 2))
//           ]);
//
//    Cells[i].Calibrate.CalcModelMemDB.SaveToXMLNode (vCell);
//  end;
//
//
//  if CalcType=ctCalibrateRegion then
//    with RegionCalibrate do
//  begin
//    xml_AddNode (gl_XMLDoc.DocumentElement, 'CalcRegion',
//           [
//            xml_Par('best_calc_model_id', BestCalcModelID),
//            xml_Par('K0_open',   TruncFloat(K0_open, 2)),
//            xml_Par('K0_closed', TruncFloat(K0_closed, 2)),
//            xml_Par('K0',        TruncFloat(K0, 2)),
//            xml_Par('AVE',       TruncFloat(AveErr, 2)),
//            xml_Par('CKO',       TruncFloat(CKO, 2))
//           ]);
//
//  end;
//
//
//  gl_XMLDoc.SaveToFile (aFileName);
//

//end;



//-----------------------------------------------------------------
procedure TCalcParamsLoader.AntennaTypeList_LoadFromXML(aParentNode: IXMLNode);
//-----------------------------------------------------------------
var iCount,i,j: integer;
  s: string;
    vNode,vItem: IXMLNode;
    strArr: TStrArray;
    oItem: TcmpAntennaType;
begin
  for i:=0 to aParentNode.ChildNodes.Count-1 do
  begin
    vNode:=aParentNode.ChildNodes[i];

    oItem:=CalcParamsRef.AntennaTypes.AddItem;
//    Add(oItem);

    oItem.ID  :=xml_GetIntAttr (vNode, 'id');
    oItem.Gain:=AsFloat   (xml_GetAttr (vNode, 'gain'));

    // -------------------------------------------------------------------
    vItem:=xml_FindNode (vNode, 'horz_losses');

    s:=xml_GetAttr (vItem, 'value');
    if s<>'' then
    begin
      strArr:=StringToStrArray (xml_GetAttr (vItem, 'value'));
     // j:=High(strArr)+1;//test

    //  if High(strArr)+1 = 360 then
      SetLength(oItem.Losses_Hor, 360);
      for j:=0 to 360-1 do
      begin
        oItem.Losses_Hor[j].Angle:=j;
        oItem.Losses_Hor[j].Loss:=AsFloat(strArr[j]);
      end;
    end;


    s:=xml_GetAttr (vItem, 'value_ex');
    if s<>'' then
    begin
   //   ParseAntennaMaskArray (s, oItem.Losses_Hor);
    end;

    // -------------------------------------------------------------------
    vItem:=xml_FindNode (vNode, 'vert_losses');

    s:=xml_GetAttr (vItem, 'value');
    if s<>'' then
    begin
      strArr:=StringToStrArray (s);

      SetLength(oItem.Losses_ver, 360);
      for j:=0 to 360-1 do
      begin
        oItem.Losses_ver[j].Angle:=j;
        oItem.Losses_ver[j].Loss:=AsFloat(strArr[j]);
      end;
    end;

    s:=xml_GetAttr (vItem, 'value_ex');
    if s<>'' then
    begin
  //    ParseAntennaMaskArray (s, oItem.Losses_ver);
    end;


  end;
end;

//--------------------------------------------------------------
// TcmpAntennaList
//--------------------------------------------------------------
//--------------------------------------------------------------
procedure TCalcParamsLoader.Antenna_LoadFromXML(aCell: TcmpCell; aParentNode:
    IXMLNode; aZone: integer; aMaxXYBounds: TXYRect);
//--------------------------------------------------------------
var i: integer;
    vNode: IXMLNode;
    oAntenna: TcmpAntenna;
    sTag: string;

var
  xyPoint: TXYPoint;
  bl: TBLPoint;
  xyRect: TXYRect;
  iRadius: integer;

  vNode1: IXMLNode;

begin
  for i:=0 to aParentNode.ChildNodes.Count-1 do
  begin
    vNode:=aParentNode.ChildNodes[i];
    sTag:=vNode.LocalName;

    if not Eq(sTag,'antenna') then
      Continue;

    oAntenna:=aCell.Antennas.AddItem;
//    Add(oAntenna);

///    oAntenna.ParentCellLink:=aCell;

    with oAntenna do
    begin
      ID        :=  (xml_GetIntAttr (vNode, 'ID'));
      FeederLoss:= AsFloat   (xml_GetAttr (vNode, 'FeederLoss'));
      Height    := AsFloat   (xml_GetAttr (vNode, 'Height'));
      Azimuth   := AsFloat   (xml_GetAttr (vNode, 'Azimuth'));
      Tilt:= AsFloat   (xml_GetAttr (vNode, 'Slope'));
    //  Rotate    := AsFloat   (xml_GetAttr (vNode, 'Rotate'));

      ZoneNum :=  (xml_GetIntAttr (vNode, 'ZoneNum'));

      AntennaType_ID :=xml_GetIntAttr (vNode, 'Antenna_Type_ID');

      vNode1 :=xml_FindNode(vNode, 'plFileName');
      if Assigned(vNode1) then
        plFileName:=xml_GetAttr (vNode1, 'value');


     // kupFileName:=ChangeFileExt(plFileName, '.kup');

      iRadius   :=AsInteger (xml_GetAttr (vNode, 'Radius'));
      if iRadius=0 then iRadius:=35;

      bl.B:=AsFloat (xml_GetAttr (vNode, 'B'));
      bl.L:=AsFloat (xml_GetAttr (vNode, 'L'));

//      ..

//  function geo_Pulkovo42_XY_to_BL(aXYPoint: TXYPoint; aGK_Zone: Integer): TBLPoint;
//  function geo_Pulkovo42_BL_to_XY(aBLPoint: TBLPoint; aGK_Zone: word = 0):  TXYPoint;

      xyPoint:=geo_Pulkovo42_BL_to_XY (bl, aZone);

//      xyPoint:=geo_BL_to_XY (bl, 0, aZone);

(*      X:=Round(xyPoint.X);
      Y:=Round(xyPoint.Y);

      xyRect:=geo_MakeXYRectByCircle (xyPoint, 1000*iRadius, aMaxXYBounds);

      XYBounds.TopLeft.X:=Trunc(xyRect.TopLeft.X);
      XYBounds.TopLeft.Y:=Trunc(xyRect.TopLeft.Y);
      XYBounds.BottomRight.X:=Trunc(xyRect.BottomRight.X);
      XYBounds.BottomRight.Y:=Trunc(xyRect.BottomRight.Y);
*)
    end;
  end;

end;

//=================================================
//(*
//const
//  MODEL_DET     = 0;
//  MODEL_HATA567 = 1;
//  MODEL_COST231 = 2;
//  MODEL_WALLFISH = 3;
//  MODEL_DEGO = 3;
//  MODEL_KSJ  = 4;
//  MODEL_METHOD_B = 5;
//  MODEL_INDOOR = 6;
//  MODEL_SOFDMA = 7;
//  MODEL_LOS = 8;
//*)
//
//            {
//  ((Name: '1. ?????????????????';      ID: MODEL_DET),
//   (Name: '2. Rec.567 (Okumura-Hata)'; ID: MODEL_HATA567),
//   (Name: '3. Cost. 231';              ID: MODEL_COST231),
//   (Name: '4. Walfish';                ID: MODEL_WALLFISH),
//   (Name: '4. Dego';                   ID: MODEL_DEGO),
//   (Name: '5. KSJ';                    ID: MODEL_KSJ),
//   (Name: '6. Metod B';                ID: MODEL_METHOD_B)
//       }
//
//
//--------------------------------------------------------------
// TcmpCalcModelTypeList
//--------------------------------------------------------------
//
//--------------------------------------------------------------
//function TcmpCalcModelType.GetParamByName (aName: string; aDefValue: double=0): double;
//--------------------------------------------------------------
//var i: integer;
//begin
//  for i:=0 to High(Params) do
//    if Params[i].Name=aName then begin Result:=Params[i].Value; Exit; end;
//  Result:=aDefValue;
//end;
(*
//--------------------------------------------------------------
function TcmpCalcModelType.Compare (aModel: TcmpCalcModelType): boolean;
//--------------------------------------------------------------
var i: integer;
begin
  Result:=(ModelType = aModel.ModelType) and
          (Length(Params) = Length(aModel.Params));
  if not Result then Exit;

  for i:=0 to High(Params) do
    if (Params[i].Value<>aModel.Params[i].Value) then
      begin Result:=False; Exit; end;
  Result:=True;
end;
*)(*
procedure TcmpCalcModelTypeList.Clear;
var i: integer;
begin
  for i:=0 to Count-1 do  Items[i].Free;
  inherited;
end;*)

//--------------------------------------------------------------
procedure TCalcParamsLoader.CalcModelType_LoadFromXML(aParentNode: IXMLNode);
//--------------------------------------------------------------
var iType,iCount,i,j: integer;
    vNode,vItem: IXMLNode;
  //  strArr: TStrArray;
    oItem: TcmpCalcModelType;
begin
  for i:=0 to aParentNode.ChildNodes.Count-1 do
  begin
    vNode:=aParentNode.ChildNodes[i];

    oItem:=CalcParamsRef.CalcModelTypes.AddItem;
 
    oItem.ID  :=xml_GetIntAttr (vNode, 'id');
    oItem.Name:=xml_GetAttr (vNode, 'name');

(*
    iType:=xml_GetIntAttr (vNode, 'type');
    case iType of
      MODEL_DET:      oItem.ModelType:=cmtDetermin;
      MODEL_COST231:  oItem.ModelType:=cmtCost231;
      MODEL_HATA567:  oItem.ModelType:=cmt567;
//      MODEL_WALLFISH: oItem.ModelType:=cmtWallfish;
      MODEL_DEGO:     oItem.ModelType:=cmtDego;
      MODEL_KSJ:      oItem.ModelType:=cmtKSJ;
      MODEL_METHOD_B: oItem.ModelType:=cmtMETHOD_B;
      MODEL_INDOOR:   oItem.ModelType:=cmtIndoor;
      MODEL_SOFDMA:   oItem.ModelType:=cmtSOFDMA;

      MODEL_LOS:      oItem.ModelType:=cmtLOS;

    else
      begin
//      raise Exception.Create('model not defined!');
        if g_IsShowMessage then
          ErrorDlg('model not defined!')
        else
          custom_error_log.AddError('TcmpCalcModelTypeList.CalcModelType_LoadFromXML', 'model not defined!');

        ExitProcess(1);
      end;
*)

    end;

   (* iCount:=vNode.ChildNodes.Length;
    SetLength (oItem.Params, iCount);

    for j:=0 to vNode.ChildNodes.Length-1 do
    begin
      vItem:=vNode.ChildNodes.Item(j);
      oItem.Params[j].Name:=xml_GetAttr (vItem, 'name');
      oItem.Params[j].Value:=AsFloat(xml_GetAttr (vItem, 'value'));
    end;
      
  end;
     *)
end;

//-------------------------------------------------
// TCalcRegion
//-------------------------------------------------
procedure TCalcParamsLoader.CalcRegion_LoadFromXML(aParentNode: IXMLNode);
//-------------------------------------------------
var i: integer;
   vNode,vRegion: IXMLNode;
 //zzzzzzzzzzz  clutterType: TcalcClutterType;
   vClutters: IXMLNode;
   sValue,str: string;

  oItem: TCalcRegion;
begin
  vRegion:=aParentNode;

  oItem :=CalcParamsRef.CalcRegion;

  oItem.Name        :=xml_GetAttr (vRegion, 'name');

  sValue:=xml_GetAttr (vRegion, 'CalcDirection');
  oItem.CalcDirection:=IIF (Eq (sValue,'BS_to_PO'), cdBS_to_PO, cdPO_to_BS);
//  oItem.Level_Stored_Noise:= AsInteger (xml_GetAttr (vRegion, 'Level_Stored_Noise'));
//  oItem.PercentError      := AsInteger (xml_GetAttr (vRegion, 'Percent_Error'));


  oItem.CalcModel_ID :=  (xml_GetIntAttr (vRegion, 'CalcModel_ID'));
  oItem.k0_open  := AsFloat (xml_GetAttr (vRegion, 'k0_open'));
  oItem.k0_closed:= AsFloat (xml_GetAttr (vRegion, 'k0_closed'));
  oItem.k0       := AsFloat (xml_GetAttr (vRegion, 'k0'));


  oItem.AntennaIsOnRoof := AsBoolean (xml_GetAttr (vRegion, 'AntennaIsOnRoof'));
  oItem.Refraction      := AsFloat   (xml_GetAttr (vRegion, 'Refraction'));
  oItem.CalcStep        := AsInteger (xml_GetAttr (vRegion, 'CalcStep'));

  oItem.Zone6 := AsInteger (xml_GetAttr (vRegion, 'Zone'));
  oItem.xyRect.TopLeft.X    :=AsInteger (xml_GetAttr (vRegion, 'TopLeft_x'));
  oItem.xyRect.TopLeft.Y    :=AsInteger (xml_GetAttr (vRegion, 'TopLeft_y'));
  oItem.xyRect.BottomRight.X:=AsInteger (xml_GetAttr (vRegion, 'BottomRight_x'));
  oItem.xyRect.BottomRight.Y:=AsInteger (xml_GetAttr (vRegion, 'BottomRight_y'));

  vNode:=xml_FindNode (vRegion, 'abonent');
    oItem.Abonent.Height     :=AsFloat   (xml_GetAttr (vNode, 'Height'));
    oItem.Abonent.Power_W      :=AsFloat   (xml_GetAttr (vNode, 'Power'));
    oItem.Abonent.Sensitivity:=AsInteger (xml_GetAttr (vNode, 'Sensitivity'));
    oItem.Abonent.FeederLoss :=AsFloat   (xml_GetAttr (vNode, 'FeederLoss'));
(*

  vClutters:=xml_GetNodeByPath (vRegion, ['CLUTTERS']);
  for i:=0 to vClutters.ChildNodes.Length-1 do
  begin
    vNode:=vClutters.ChildNodes.Item(i);

    str:=xml_GetAttr (vNode, 'type');
    if Eq(str,'Forest')   then clutterType:=ctForest else
    if Eq(str,'Country')  then clutterType:=ctCountry else
    if Eq(str,'City')     then clutterType:=ctCity else
    if Eq(str,'OneBuild') then clutterType:=ctOneBuild else
    if Eq(str,'OneHouse') then clutterType:=ctOneHouse;

    with Clutters[clutterType] do begin
      Height         :=AsInteger (xml_GetAttr (vNode, 'Height'));
      Loss           :=AsFloat   (xml_GetAttr (vNode, 'Loss'));
      Reflection_coef:=AsFloat   (xml_GetAttr (vNode, 'Reflection_coef'));
    end;
  end;
*)

end;

//--------------------------------------------------------------
procedure TCalcParamsLoader.Cell_LoadFromXML(aParentNode: IXMLNode; aZone:
    integer; aMaxBounds: TXYRect; aStart, aFin : integer);
//--------------------------------------------------------------
var iCount,i,j, iStart, iFin: integer;
    vRoot,vNode,vNode1,vTrx,vItem: IXMLNode;
    oCell: TcmpCell;
   // oBLPointItem: Tdrive_BLPointItem;
begin

 // Self.Clear;

//  for i:=0 to aParentNode.ChildNodes.Length-1 do
  for i:=aStart to aFin do
  begin
    vNode:=aParentNode.ChildNodes[i];

    oCell:=CalcParamsRef.Cells.AddItem;
//    Add(oCell);

    oCell.ID  :=xml_GetIntAttr (vNode, 'id');

    oCell.Name:=xml_GetAttr (vNode, 'name');

    oCell.Standard_Name :=xml_GetAttr (vNode, 'StandardName');

    vNode1 :=xml_FindNode (vNode, 'kupFileName');

    oCell.kupFileName  :=xml_GetAttr (vNode1, 'value');

    oCell.Combiner_Loss:= AsFloat(xml_GetAttr (vNode, 'Combiner_Loss'));
    oCell.Site_Name       :=  xml_GetAttr (vNode, 'SiteName');

//    oCell.Radius_Cov   := AsFloat(xml_GetAttr (vNode, 'Radius_cov'));
//    oCell.Radius_CIA   := AsFloat(xml_GetAttr (vNode, 'Radius_cia'));
     oCell.CALC_RADIUS_km:= AsFloat(xml_GetAttr (vNode, 'Radius'));

    oCell.CalcModel_ID := xml_GetIntAttr (vNode, 'CalcMOdel_ID');

    oCell.K0_open  :=AsFloat(xml_GetAttr (vNode, 'K0_open'));
    oCell.K0_closed:=AsFloat(xml_GetAttr (vNode, 'K0_closed'));
    oCell.K0       :=AsFloat(xml_GetAttr (vNode, 'K0'));


    vTrx:=xml_FindNode (vNode, 'TRX');
    if not VarIsNull(vTrx) then
    begin
      oCell.TRX.Power_W        :=AsFloat  (xml_GetAttr (vTrx, 'Power'));
      oCell.TRX.Sensitivity:=xml_GetIntAttr (vTrx, 'Sensitivity');
      oCell.TRX.Freq_MHz       :=AsFloat  (xml_GetAttr (vTrx, 'Freq'));
    end;

    { TODO : test }
//    oCell.Calibrate.FileName:=Format('t:\cell.%d.points', [oCell.ID]);
(*    oCell.Calibrate.FileName:=oCell.kupFileName + '.points';
    oCell.Calibrate.Points_.LoadFromFile (oCell.Calibrate.FileName);
*)
    Antenna_LoadFromXML (oCell, vNode, aZone, aMaxBounds);

  end;
end;

//-------------------------------------------------
function TCalcParamsLoader.LoadFromXML(aFileName: string): boolean;
//-------------------------------------------------
var vRoot,vNode,vGroup: IXMLNode;

    //-----------------------------------------
    procedure DoLoadGeneral();
    //-----------------------------------------
    var iCount,i: integer;   sValue,fname: string;
    begin
      vGroup:=xml_FindNode (vRoot, 'MATRIXES');
      if VarIsNull(vGroup) then Exit;

      for i:=0 to vGroup.ChildNodes.Count-1 do
      begin
        vNode:=vGroup.ChildNodes[i];

        fname:=xml_GetAttr (vNode, 'filename');
        CalcParamsRef.ReliefMatrixFiles.Add (fname);
      end;

      vGroup:=xml_FindNode (vRoot, 'PARAMS');
      if VarExists (vGroup) then
      begin
      (*  CalcParamsRef.IsWsOnly := AsBoolean(xml_GetAttr (vGroup, 'WS_ONLY'));
       // g_IsShowMessage := AsBoolean(xml_GetAttr (vGroup, 'IsShowMessage', '1'));

        IsUseSavedPathLoss:=not AsBoolean(xml_GetAttr (vGroup, 'IsRecalcPathLoss'));

        IsCalcLos         := AsBoolean(xml_GetAttr (vGroup, 'IsCalcLos'));

        IsLosOnly         := AsBoolean(xml_GetAttr (vGroup, 'IsLosOnly'));

        sValue:=AsString(xml_GetAttr (vGroup, 'CalcType'));
*)
//        if Eq (sValue, 'points') then CalcType:=ctPoints else
{ TODO : . }
      (*  if Eq (sValue, 'CalcRegion') then CalcType:=ctRegion else
        if Eq (sValue, 'points') then CalcType:=ctPoints else
        if Eq (sValue, 'Caliber') then CalcType:=ctCalibrate else
        if Eq (sValue, 'Caliber_Region') then CalcType:=ctCalibrateRegion
        else
          CalcType:=ctRegion;*)
         // raise Exception.Create('Error: CalcType not defined');

      end;
   //   IsUseSavedPathLoss:=True;
    end;


var
  oXMLDoc: TXMLDoc;
begin
  Assert(Assigned(CalcParamsRef), 'Value not assigned');

  oXMLDoc:=TXMLDoc.Create();

 // Result:=
  oXMLDoc.LoadFromFile (aFileName);
 // if not Result then Exit;

  vRoot:=oXMLDoc.DocumentElement;

  DoLoadGeneral();

  vNode:=xml_FindNode (vRoot, 'CalcRegion');
 // CalcParamsRef.
  CalcRegion_LoadFromXML (vNode);

  vNode:=xml_FindNode (vRoot, 'models');
 // CalcParamsRef.
  CalcModelType_LoadFromXML (vNode);

  vNode:=xml_FindNode (vRoot, 'cells');

 // FCellCount := vNode.ChildNodes.Length;
//  FFileName := aFileName;

(*  if CalcType<>ctRegion then
    Cells.LoadFromXML (vNode, CalcRegion.Zone, CalcRegion.XYBounds, 0, FCellCount-1);
*)

  vNode:=xml_FindNode (vRoot, 'antenna_types');
  AntennaTypeList_LoadFromXML (vNode);

 // Result:=Prepare();
end;

end.
