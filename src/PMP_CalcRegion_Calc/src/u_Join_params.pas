unit u_Join_params;

interface
uses
  SysUtils, Classes,

  XMLDoc, XMLIntf, u_xml_document,


  u_func
  //u_XML

  ;

type
  TCellList = class;
  TCellLayerList= class;


  TCellItem = class(TCollectionItem)
  public
    ID:           integer;
    Name:         string;
    SiteName:     string;
    KupFileName:  string;
    sector_name:  string;
    Hysteresis:   integer;
    CalcRadius_km:double;
    B:            double;
    L:            double;
    Sector_Sense_dBm: integer;
  end;


  TCellLayer = class(TCollectionItem)
  public
    ID:      integer;
    Name:    string;

    kupFileName:     string;
    kgrFileName:     string;
    kmoFileName:     string;
    //  bsicFileName: string;
    //  lacFileName: string;
    NBestFileName:   string;
    hoffFileName:    string;
    SummaryFileName: string;

    Cells: TCellList;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
  end;

  // ---------------------------------------------------------------
  TParams_Join = class
  // ---------------------------------------------------------------
  private


    procedure AddOptions(aParentNode: IXMLNode);
    procedure AddCellLayers(aParentNode: IXMLNode);
    procedure AddCells(aParentNode: IXMLNode; aCells: TCellList);
    procedure AddRegion(aParentNode: IXMLNode);

  public
    Options: record
      IsMakeNBestMatrix: boolean;
      IsMakeHandOffMatrix: boolean;
      NBestMatrixRecSize: integer;
    end;

    CalcRegion: record
       ID: integer;
       Name:        string;
       CalcStep:    integer;
       Hysteresis:  integer;

       CalcResultsDir: string;

       RowCount:    integer;
       ColCount:    integer;
       Abonent_Sensitivity_dBm: integer;
       CalcDirection: (cdBS_to_PO, cdPO_to_BS);

       Zone6:        integer;
       TopLeft,BottomRight: record
                              x: double;
                              y: Double;
                            end;
    end;

    //'Polygone'
    PolygonePoints: array of record
                      B : double;
                      L : double;
                    end;

    CellLayers: TCellLayerList;


    constructor Create;
    destructor Destroy; override;
//    CalcRegionTypeIsGSM: boolean;

    procedure Validate(aErrorList: TStrings);

    procedure Clear;


    procedure SaveToXMLFile(aFileName: string);

 end;


  //-----------------------------------------------------
  TCellList = class(TCollection)
  //-----------------------------------------------------
  private
    function GetItem(Index: integer): TCellItem;
  protected
  public
    constructor Create;
    function AddItem: TCellItem;

    property Items[Index: integer]: TCellItem read GetItem; default;
  end;


  //-----------------------------------------------------
  TCellLayerList = class(TCollection)
  //-----------------------------------------------------
  private
    function GetItem(Index: integer): TCellLayer;
  protected
  public
    constructor Create;
    function AddItem: TCellLayer;

    property Items[Index: integer]: TCellLayer read GetItem; default;
  end;



implementation

const
  ATT_REGION  = 'region';
  ATT_ZONENUM = 'zonenum';
  ATT_SITE_NAME = 'site_name';

  ATT_KUP_FILENAME    ='KupFileName';
  ATT_kmo_FileName    ='kmoFileName';
  ATT_kgr_FileName    ='kgrFileName';
  ATT_NBest_FileName  ='NBestFileName';
//  ATT_hoff_FileName   ='hoffFileName';
//  ATT_SummaryFileName='SummaryFileName';



function TCellList.GetItem(Index: integer): TCellItem;
begin
  Result:=TCellItem(inherited Items[Index]);
end;

constructor TCellList.Create;
begin
  inherited Create(TCellItem);
end;

function TCellList.AddItem: TCellItem;
begin
  Result := TCellItem(Add);
end;

constructor TParams_Join.Create;
begin
  inherited Create;
  CellLayers := TCellLayerList.Create();
end;

destructor TParams_Join.Destroy;
begin
  FreeAndNil(CellLayers);
  inherited;
end;

//-----------------------------------------------------------------------------------------
procedure TParams_Join.AddCellLayers(aParentNode: IXMLNode);
//-----------------------------------------------------------------------------------------
var
  vGroup: IXMLNode;
  vNode: IXMLNode;
  i : integer;
  oCellLayer: TCellLayer;

begin      
  vGroup:=aParentNode.AddChild('Standards');


  for I := 0 to CellLayers.Count - 1 do
  begin
      oCellLayer :=CellLayers[i];

      vNode:=xml_AddNode (vGroup, 'Standard',
                [xml_Par (ATT_ID, oCellLayer.ID),
                 xml_Par (ATT_name, oCellLayer.name)
                ]);

//      if mtKup in Params.MapTypes then
        xml_AddNode (vNode, 'kupFileName',    [xml_Par(ATT_VALUE, oCellLayer.kupFileName)]);
//      if mtKgr in Params.MapTypes then
        xml_AddNode (vNode, 'kgrFileName',    [xml_Par(ATT_VALUE, oCellLayer.kgrFileName)]);
//      if mtKmo in Params.MapTypes then
        xml_AddNode (vNode, 'kmoFileName',    [xml_Par(ATT_VALUE, oCellLayer.kmoFileName)]);

      if Options.IsMakeNBestMatrix then
        xml_AddNode (vNode, 'NBestFileName',  [xml_Par(ATT_VALUE, oCellLayer.NBestFileName)]);

     //   if mtHandover in MapTypes then
     //     xml_AddNode (vNode, 'hoffFileName',   [xml_Par(ATT_VALUE, CellLayer.hoffFileName)]);

    //    if IsCalcStatistic then
    //      xml_AddNode (vNode, 'SummaryFileName',[xml_Par(ATT_VALUE, CellLayer.SummaryFileName)]);
  //      xml_AddNode (vNode, 'lacFileName',    [xml_Par(ATT_VALUE, sFileDir + 'region.lac')]);
  //      xml_AddNode (vNode, 'bsicFileName',   [xml_Par(ATT_VALUE, sFileDir + 'region.bsic')]);
  //    end;

    AddCells (vNode, oCellLayer.Cells);

  end;


 // end;

end;

//-----------------------------------------------------------------------------------------
procedure TParams_Join.AddCells(aParentNode: IXMLNode; aCells: TCellList);
//-----------------------------------------------------------------------------------------
var
  vCells: IXMLNode;
  s, sKupFileName: string;
  i: integer;
  dRadius_km, dHysteresis, dLat, dLon: double;

  oCell: TCellItem;

begin
  vCells:=xml_AddNodeTag (aParentNode, 'CELLS');

 /// db_ViewDataSet(qry_Cells);


//  ind:=0;

  for I := 0 to aCells.Count - 1 do
  begin
    oCell:=aCells[i];

    xml_AddNode (vCells, 'CELL',
          [xml_Par (ATT_ID,           oCell.ID),
           xml_Par (ATT_NAME,         oCell.Name),
           xml_Par (ATT_SITE_NAME,    oCell.SiteName),
           xml_Par ('sector_name',    oCell.sector_name),
     //      xml_Par ('Hysteresis',     dHysteresis),
           xml_Par ('CalcRadius',     oCell.CalcRadius_km),
     /////////      xml_Par ('BS_sense',       qry_Cells.FieldByName(FLD_SENSE).AsInteger),
           xml_Par ('B',              oCell.B),
           xml_Par ('L',              oCell.L),
           xml_Par (ATT_KUP_FILENAME, oCell.KupFileName)
          ]);

  //  Next;
  end;

end;

//----------------------------------------------------
procedure TParams_Join.AddOptions(aParentNode: IXMLNode);
//----------------------------------------------------
var
  vNode: IXMLNode;
begin
  vNode:=xml_AddNodeTag (aParentNode, 'Options');

  xml_AddNode (vNode, 'IsMakeNBestMatrix',   [xml_Par(ATT_VALUE, Options.IsMakeNBestMatrix)]);
  xml_AddNode (vNode, 'NBestMatrixRecSize',  [xml_Par(ATT_VALUE, Options.NBestMatrixRecSize)]);
  xml_AddNode (vNode, 'IsMakeHandOffMatrix', [xml_Par(ATT_VALUE, Options.IsMakeHandOffMatrix)]);
 // xml_AddNode (vNode, 'IsCalcStatistic',     [xml_Par(ATT_VALUE, false)]);
end;

//----------------------------------------------------
procedure TParams_Join.AddRegion(aParentNode: IXMLNode);
//----------------------------------------------------
var
  vRegion, vGroup: IXMLNode;
  I: integer;
  iZone, iRelBestZone, iCalcStep: integer; //, iPolygonID

begin

// -------------------------------------------------------------------

  vRegion:=xml_AddNode (aParentNode, ATT_REGION,
    [xml_Par (ATT_NAME,           CalcRegion.Name),
     xml_Par ('CalcResultsDir',   CalcRegion.CalcResultsDir),
     xml_Par ('Hysteresis',       CalcRegion.Hysteresis),
     xml_Par (ATT_ID,             CalcRegion.ID),
     xml_Par ('Zone',             CalcRegion.Zone6),
     xml_Par ('CalcDirection',    IIF(CalcRegion.CalcDirection=cdBS_to_PO, 'BS_to_PO', 'PO_to_BS')),
     xml_Par ('RowCount',         CalcRegion.RowCount),
     xml_Par ('ColCount',         CalcRegion.ColCount),
     xml_Par ('CalcStep',         CalcRegion.CalcStep),
     xml_Par ('Abonent_Sensitivity', CalcRegion.Abonent_Sensitivity_dBm),

//           xml_SetAttr (vRegion, 'CalcDirection', IIF(ARegion.Params.CalcDirection=cdt_BS_to_PO, 'BS_to_PO', 'PO_to_BS'));

     xml_Par ('TopLeft_x',     CalcRegion.TopLeft.X),
     xml_Par ('TopLeft_y',     CalcRegion.TopLeft.Y),
     xml_Par ('BottomRight_x', CalcRegion.BottomRight.X),
     xml_Par ('BottomRight_y', CalcRegion.BottomRight.Y)
     ]);



    vGroup:= xml_AddNodeTag (vRegion, 'Polygone');


    for I := 0 to High(PolygonePoints) do
    //  begin
      xml_AddNode (vGroup, 'Point',
          [xml_Par ('B',    PolygonePoints[i].B),
           xml_Par ('L',    PolygonePoints[i].L)     ]);

    AddCellLayers (vRegion);

end;

procedure TParams_Join.Clear;
begin
  SetLength(PolygonePoints, 0);

  CellLayers.Clear;

  FillChar(Options,SizeOf(Options),0);
  FillChar(CalcRegion,SizeOf(CalcRegion),0);


end;

//----------------------------------------------------
procedure TParams_Join.SaveToXMLFile(aFileName: string);
//----------------------------------------------------
var
  vRoot,vGroup: IXMLNode;
//var
  oXMLDoc: TXMLDoc;

begin
  oXMLDoc:=TXMLDoc.Create;


  vRoot:=oXMLDoc.DocumentElement;

  AddOptions (vRoot);

//  qry_Cells.Filtered:= true;

  vGroup:=xml_AddNodeTag (vRoot, 'REGIONS');
  AddRegion (vGroup);

  oXMLDoc.SaveToFile (aFileName);
  oXMLDoc.Free;

end;

// ---------------------------------------------------------------
procedure TParams_Join.Validate(aErrorList: TStrings);
// ---------------------------------------------------------------
begin
  if not DirectoryExists(CalcRegion.CalcResultsDir) then
    aErrorList.Add('����������� ��������� ���������� (��� �������� ������ ���� ���� ����� ��������(�������) ��� �������)');

(*  if (CalcRegion.TopLeft.B<=0)     then aErrorList.Add('������� ������: ������� �����: ������<=0');
  if (CalcRegion.TopLeft.L<=0)     then aErrorList.Add('������� ������: ������� �����: �������<=0');
  if (CalcRegion.BottomRight.B<=0) then aErrorList.Add('������� ������: ������ ������: ������<=0');
  if (CalcRegion.BottomRight.L<=0) then aErrorList.Add('������� ������: ������ ������: �������<=0');
*)

  if (CalcRegion.Abonent_Sensitivity_dBm >=0) then
    aErrorList.Add('���������������� ������������ ���������>=0');


(*
  if Params.IsVerifyParams then
    if not DirectoryExists(FCalcRegionFileDir) then
      custom_DbErrorLog.AddError('����������� ��������� ���������� (��� �������� ������ ���� ���� ����� ��������(�������) ��� �������)');

  if (FBLRect.TopLeft.B<=0)     then custom_DbErrorLog.AddError('������� ������: ������� �����: ������<=0');
  if (FBLRect.TopLeft.L<=0)     then custom_DbErrorLog.AddError('������� ������: ������� �����: �������<=0');
  if (FBLRect.BottomRight.B<=0) then custom_DbErrorLog.AddError('������� ������: ������ ������: ������<=0');
  if (FBLRect.BottomRight.L<=0) then custom_DbErrorLog.AddError('������� ������: ������ ������: �������<=0');

      if (CalcRegion.Abonent_Sensitivity_dBm >=0) then
        custom_DbErrorLog.AddWarning('���������������� ������������ ���������>=0');


*)

(*  if not FileExists(FileName) then
    aErrorList.Add('��� ����� ������� ������ ��� ������ �������: '+FileName);
*)

end;

function TCellLayerList.GetItem(Index: integer): TCellLayer;
begin
  Result:=TCellLayer(inherited Items[Index]);
end;

constructor TCellLayerList.Create;
begin
  inherited Create(TCellLayer);
end;

function TCellLayerList.AddItem: TCellLayer;
begin
  Result := TCellLayer(Add);
end;

constructor TCellLayer.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  Cells := TCellList.Create();
end;

destructor TCellLayer.Destroy;
begin
  FreeAndNil(Cells);
  inherited Destroy;
end;

end.
 