object dmCalcRegion_export_maps: TdmCalcRegion_export_maps
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  Left = 1146
  Top = 611
  Height = 204
  Width = 548
  object qry_CalcRegion: TADOQuery
    Parameters = <>
    Left = 31
    Top = 12
  end
  object qry_Cells: TADOQuery
    Filtered = True
    Parameters = <>
    Left = 208
    Top = 12
  end
  object qry_Cell_Layers: TADOQuery
    Parameters = <>
    Left = 120
    Top = 12
  end
  object ds_Cell_Layers: TDataSource
    DataSet = qry_Cell_Layers
    Left = 116
    Top = 84
  end
  object ds_Cells: TDataSource
    DataSet = qry_Cells
    Left = 204
    Top = 84
  end
  object ds_Region: TDataSource
    DataSet = qry_CalcRegion
    Left = 32
    Top = 88
  end
  object qry_ColorSchema: TADOQuery
    Parameters = <>
    Left = 408
    Top = 12
  end
  object qry_ColorSchemaRanges: TADOQuery
    Parameters = <>
    Left = 408
    Top = 76
  end
end
