unit u_bin_to_mif_params;

interface

uses Classes, SysUtils,


XMLDoc, XMLIntf,
u_xml_document,

   u_func
//   u_Xml
;

type
  TBinToMifParams = class;
  TBinMatrixFile = class;
  TBinMatrixList = class;
  TColorSchemaRangeList = class;
  TColorSchemaList  = class;
  TCell = class;
  TCellList = class;

  TBinMatrixFile = class(TCollectionItem)
  public
    FileName       : string;
    MapFileName    : string;
    ColorSchema_ID : Integer;

    procedure Validate(aErrorList: TStrings);
  end;

  TCell = class(TCollectionItem)
  public
    ID   : Integer;
    Color : Integer; //string;

    procedure SaveToXMLNode(aParentNode: IXMLNode);
  end;


  TColorSchema = class(TCollectionItem)
  private
  public
    ID : Integer;
    Name : string;
    Ranges: TColorSchemaRangeList;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;

    procedure SaveToXMLNode(aParentNode: IXMLNode);
  end;


  TColorSchemaRange = class(TCollectionItem)
  public
    MinValue : double;  // �������� � �������
    MaxValue : double;  // �������� � �������
    Color    : integer; // ���� �� �����
    MaxColor : integer;
    MinColor : integer;

    Comment : string;

//    Type_    : (rtFixed,rtGradient);
    Type_    : Integer;
    procedure SaveToXMLNode(aParentNode: IXMLNode);
  end;


  //-----------------------------------------------------
  TBinMatrixList = class(TCollection)
  //-----------------------------------------------------
  private
    function GetItem(Index: integer): TBinMatrixFile;
  public
    constructor Create;
    function AddItem: TBinMatrixFile;

    property Items[Index: integer]: TBinMatrixFile read GetItem; default;
  end;



  //-----------------------------------------------------
  TColorSchemaRangeList = class(TCollection)
  //-----------------------------------------------------
  private
    function GetItem(Index: integer): TColorSchemaRange;
  public
    constructor Create;
    function AddItem: TColorSchemaRange;

    property Items[Index: integer]: TColorSchemaRange read GetItem; default;
  end;


  //-----------------------------------------------------
  TColorSchemaList = class(TCollection)
  //-----------------------------------------------------
  private
    function GetItem(Index: integer): TColorSchema;
  public
    constructor Create;
    function AddItem: TColorSchema;

    property Items[Index: integer]: TColorSchema read GetItem; default;
  end;


  //-----------------------------------------------------
  TCellList = class(TCollection)
  //-----------------------------------------------------
  private
    function GetItem(Index: integer): TCell;
  public
    constructor Create;
    function AddItem: TCell;

    property Items[Index: integer]: TCell read GetItem; default;
  end;

  //-----------------------------------------------------
  TBinToMifParams = class
  //-----------------------------------------------------
  private
    procedure AddBinMatrixes(aParentNode: IXMLNode);
    function AddColorSchemasToXMLNode(aParentNode: IXMLNode): boolean;
  public
    BinMatrixList: TBinMatrixList;
    ColorSchemaList: TColorSchemaList;
    CellList: TCellList;

    MapType : (mtNone, mtVector_, mtRaster_);

    constructor Create;
    destructor Destroy; override;

    procedure Clear;

    function SaveToXMLFile(aFileName: string): boolean;
    procedure Validate(aErrorList: TStrings);
  end;





implementation

const
  ATT_kgr_FileName = 'kgrFileName';

  TAG_COLOR_SCHEMAS = 'COLOR_SCHEMAS';



function TBinMatrixList.GetItem(Index: integer): TBinMatrixFile;
begin
  Result:=TBinMatrixFile(inherited Items[Index]);
end;

constructor TBinMatrixList.Create;
begin
  inherited Create(TBinMatrixFile);
end;

function TBinMatrixList.AddItem: TBinMatrixFile;
begin
  Result := TBinMatrixFile(Add);
end;

constructor TBinToMifParams.Create;
begin
  inherited Create;
  BinMatrixList := TBinMatrixList.Create();
  ColorSchemaList := TColorSchemaList.Create();
  CellList := TCellList.Create();
end;

destructor TBinToMifParams.Destroy;
begin
  FreeAndNil(CellList);
  FreeAndNil(ColorSchemaList);
  FreeAndNil(BinMatrixList);
  inherited Destroy;
end;

procedure TBinToMifParams.Clear;
begin
  BinMatrixList.Clear;
end;


procedure TBinMatrixFile.Validate(aErrorList: TStrings);
begin
  if not FileExists(FileName) then
    aErrorList.Add('��� ����� ������� ������ ��� ������ �������: '+FileName);

end;


//-------------------------------------------------------------------
function TBinToMifParams.AddColorSchemasToXMLNode(aParentNode: IXMLNode):
    boolean;
//-------------------------------------------------------------------
var vGroup,vNode: IXMLNode;
  I: Integer;
begin
  vGroup := aParentNode.AddChild(TAG_COLOR_SCHEMAS);

  for I := 0 to ColorSchemaList.Count - 1 do
    ColorSchemaList[i].SaveToXMLNode(vGroup);



 (* db_OpenQuery (qry_Item, TBL_COLORSCHEMA, FLD_ID, aID);

  with qry_Item do
    while not Eof do
    begin
       vGroup := xml_AddNode (aParentNode, 'item',
                            [xml_Par(FLD_NAME, FieldValues[FLD_NAME]),
                             xml_Par(FLD_ID, FieldValues[FLD_ID])]);

       db_OpenQuery (qry_Ranges,
                'SELECT * FROM '+ TBL_COLORSCHEMARANGES +
                ' WHERE (color_schema_id=:color_schema_id) and (enabled<>0) '+
                ' ORDER BY min_value ',
               [db_Par(FLD_color_schema_id, FieldValues[FLD_ID])] );

       Result:= not qry_Ranges.IsEmpty;

       with qry_Ranges do
         while not Eof do
         begin
             xml_AddNode (vGroup, 'range',
               [xml_Par(FLD_COLOR, FieldValues[FLD_COLOR]),
                xml_Par('min_value', FieldValues['min_value']),
                xml_Par('max_value', FieldValues['max_value']),
                xml_Par('min_color', FieldValues['min_color']),
                xml_Par('max_color', FieldValues['max_color']),
                xml_Par(FLD_TYPE, FieldValues[FLD_TYPE])]);
            Next;
         end;

       Next;
    end;
    *)

end;




// ---------------------------------------------------------------
procedure TBinToMifParams.AddBinMatrixes(aParentNode: IXMLNode);
// ---------------------------------------------------------------
var
  I: Integer;
  vGroup, vNode, vNode1: IXMLNode;
  oBinMatrix: TBinMatrixFile;

begin
  vGroup := aParentNode.AddChild('MATRIXES');

  for I := 0 to BinMatrixList.Count - 1 do
  begin
    oBinMatrix := BinMatrixList[i];

    xml_AddNode (vGroup, 'item',
                [xml_Par('filename', oBinMatrix.FileName),
                 xml_Par('map',      oBinMatrix.MapFileName),
                 xml_par('schema',   IIF_NULL(oBinMatrix.ColorSchema_ID) )
                ]);
  end;
end;


constructor TColorSchemaRangeList.Create;
begin
  inherited Create(TColorSchemaRange);
end;

function TColorSchemaRangeList.AddItem: TColorSchemaRange;
begin
  Result := TColorSchemaRange(Add);
end;

function TColorSchemaRangeList.GetItem(Index: integer): TColorSchemaRange;
begin
  Result:=TColorSchemaRange(inherited Items[Index]);
end;

constructor TColorSchema.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  Ranges := TColorSchemaRangeList.Create();
end;

destructor TColorSchema.Destroy;
begin
  FreeAndNil(Ranges);
  inherited Destroy;
end;


//-------------------------------------------------------------------
procedure TColorSchemaRange.SaveToXMLNode(aParentNode: IXMLNode);
//-------------------------------------------------------------------
begin
//    Type_    : (rtFixed,rtGradient);
  if Type_=0 then
    xml_AddNode (aParentNode, 'range',
             [xml_Par(ATT_COLOR ,  Color),
              xml_Par('min_value', MinValue),
              xml_Par('max_value', MaxValue),
          //    xml_Par('min_color', MinColor),
           //   xml_Par('max_color', MaxColor),
              xml_Par('comment', Comment),

              xml_Par(ATT_TYPE,    Type_)
              ])
  else
    xml_AddNode (aParentNode, 'range',
             [xml_Par(ATT_COLOR ,  Color),
              xml_Par('min_value', MinValue),
              xml_Par('max_value', MaxValue),
              xml_Par('min_color', MinColor),
              xml_Par('max_color', MaxColor),
              xml_Par('comment', Comment),
              xml_Par(ATT_TYPE,    Type_)
              ]);
end;

//-------------------------------------------------------------------
procedure TColorSchema.SaveToXMLNode(aParentNode: IXMLNode);
//-------------------------------------------------------------------
var
  vGroup,vNode: IXMLNode;
  I: Integer;
  oItem: TColorSchemaRange;
begin
  vGroup := xml_AddNode (aParentNode, 'item',
                      [xml_Par(ATT_NAME, Name),
                       xml_Par(ATT_ID,   ID)
                       ]);

  for I := 0 to Ranges.Count - 1 do
    Ranges[i].SaveToXMLNode (vGroup);

(*    xml_AddNode (vGroup, 'range',
               [xml_Par(ATT_COLOR ,   oItem.Color),
                xml_Par('min_value', oItem.MinValue),
                xml_Par('max_value', oItem.MaxValue),
                xml_Par('min_color', oItem.MinColor),
                xml_Par('max_color', oItem.MaxColor),
                xml_Par(ATT_TYPE,    oItem.Type_)
                ]);

  end;
*)

(*
       with qry_Ranges do
         while not Eof do
         begin
             xml_AddNode (vGroup, 'range',
               [xml_Par(FLD_COLOR, FieldValues[FLD_COLOR]),
                xml_Par('min_value', FieldValues['min_value']),
                xml_Par('max_value', FieldValues['max_value']),
                xml_Par('min_color', FieldValues['min_color']),
                xml_Par('max_color', FieldValues['max_color']),
                xml_Par(FLD_TYPE, FieldValues[FLD_TYPE])]);
            Next;
         end;

       Next;
    end;
*)

end;



constructor TColorSchemaList.Create;
begin
  inherited Create(TColorSchema);
end;

function TColorSchemaList.AddItem: TColorSchema;
begin
  Result := TColorSchema(Add);
end;

function TColorSchemaList.GetItem(Index: integer): TColorSchema;
begin
  Result:=TColorSchema(inherited Items[Index]);
end;

constructor TCellList.Create;
begin
  inherited Create(TCell);
end;

function TCellList.AddItem: TCell;
begin
  Result := TCell(Add);
end;

function TCellList.GetItem(Index: integer): TCell;
begin
  Result:=TCell(inherited Items[Index]);
end;

procedure TCell.SaveToXMLNode(aParentNode: IXMLNode);
begin
  xml_AddNode (aParentNode, 'item',
      [xml_Par (ATT_ID,     ID),
       xml_Par (ATT_COLOR,  Color)
      ]);
end;




//----------------------------------------------------
function TBinToMifParams.SaveToXMLFile(aFileName: string): boolean;
//----------------------------------------------------

  procedure TCell_SaveToXMLNode(aCell: TCell; aParentNode: IXMLNode);
  begin
    xml_AddNode (aParentNode, 'item',
        [xml_Par (ATT_ID,     aCell.ID),
         xml_Par (ATT_COLOR,  aCell.Color)
        ]);
  end;


var vRoot,vGroup: IXMLNode;
  I: Integer;
////   iColorSchemaID_KUP: integer;
   oXMLDoc: TXMLDoc;
   sSql, S: string;
begin

  oXMLDoc:=TXMLDoc.Create;

//  CursorHourGlass;
//begin


//  gl_XMLDoc.Clear;
  vRoot:= oXMLDoc.DocumentElement;

  AddColorSchemasToXMLNode (vRoot);

  // -------------------------
  vGroup:=vRoot.AddChild('PARAMS');

  case MapType of
    mtVector_: xml_AddNode(vGroup, 'MapType', [xml_Par(ATT_VALUE, 'Vector') ]);
    mtRaster_: xml_AddNode(vGroup, 'MapType', [xml_Par(ATT_VALUE, 'raster') ]);
  else
    raise Exception.create('');
  end;


//  vGroup:= xml_AddNodeTag (vRoot, TAG_COLOR_SCHEMAS);

////  iColorSchemaID_KUP:= dmCalcRegion.GetIntFieldValue (aCalcRegionID, FLD_KUP_ID);
 // if iColorSchemaID_KUP = 0 then
   // iColorSchemaID_KUP:= Params.ColorSchemaID_KUP;

 // OpenCellLayers(FCalcRegionID);
(*
  AddParams (vRoot);
  AddCellLayers (vRoot);
*)
  // -------------------------
  vGroup:=vRoot.AddChild('CELLS');
  for I := 0 to CellList.Count - 1 do
    CellList[i].SaveToXMLNode(vGroup);


  AddBinMatrixes (vRoot);//, Params.ColorSchemaID_KUP);


  oXMLDoc.SaveToFile (aFileName);

  oXMLDoc.Free;
end;



procedure TBinToMifParams.Validate(aErrorList: TStrings);
begin

 (* if Params.ColorSchemaID_KUP = 0 then
    AddWarning('�� ������ �������� ����� ��� ����� ������ ����');

  if Params.ColorSchemaID_KMO = 0 then
    AddWarning('�� ������ �������� ����� ��� ����� ��������');

  if Params.ColorSchemaID_Handover = 0 then
    AddWarning('�� ������ �������� ����� ��� ����� handover');

  if Params.ColorSchemaID_KUP <> 0 then
//    if not dmColorSchema_export.AddColorSchemaToXMLNode (Params.ColorSchemaID_KUP, vGroup) then
    if not AddColorSchemaToXMLNode (Params.ColorSchemaID_KUP, vGroup) then
      AddError('�������� ����� ��� ����� ������ ����. �� ������ ���������');
  if Params.ColorSchemaID_KMO <> 0 then
//    if not dmColorSchema_export.AddColorSchemaToXMLNode (Params.ColorSchemaID_KMO, vGroup) then
    if not AddColorSchemaToXMLNode (Params.ColorSchemaID_KMO, vGroup) then
      AddError('�������� ����� ��� ����� ��������. �� ������ ���������');
  if Params.ColorSchemaID_Handover <> 0 then
//    if not dmColorSchema_export.AddColorSchemaToXMLNode (Params.ColorSchemaID_Handover, vGroup) then
    if not AddColorSchemaToXMLNode (Params.ColorSchemaID_Handover, vGroup) then
      AddError('�������� ����� ��� ����� handover. �� ������ ���������');
*)

end;




end.



(*

    vNode:=xml_AddNode (aParentNode, 'item',
      [xml_Par (ATT_ID,     qry_Cells[FLD_CELL_ID]),
       xml_Par (FLD_COLOR,  qry_Cells[FLD_COLOR])      ]);

    Next;
  end;

end;*)

(*
//------------------------------------------------------------------------------
procedure TdmCalcRegion_export_maps.AddCellLayers (aParentNode: Variant);
//------------------------------------------------------------------------------
var
  vCells: Variant;
begin
//  db_OpenQuery (qry_Cell_Layers,  Fsql,
//                [db_Par(FLD_CALC_REGION_ID, FCalcRegionID) ]);

  vCells:=xml_AddNodeTag (aParentNode, 'CELLS');

  qry_Cell_Layers.First;
  with qry_Cell_Layers do  while not EOF do
  begin
    AddCells (vCells, qry_Cell_Layers[FLD_CELL_LAYER_ID]);
    Next;
  end;
end;
*)

