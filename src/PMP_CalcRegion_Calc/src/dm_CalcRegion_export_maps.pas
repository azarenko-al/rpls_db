unit dm_CalcRegion_export_maps;

interface

uses SysUtils, Classes, Forms, Db, ADODB, FileCtrl, Dialogs,

   XMLDoc, XMLIntf, u_xml_document,


  dm_Main,

  dm_CalcRegion,
  dm_CalcMap,

  u_const_db,


  u_calc_types,

  u_Geo,
  u_func,
  u_db,

  u_bin_to_mif_params;


type
  TdmCalcRegion_export_mapsMapType = (mtVector, mtRaster);

  TdmCalcRegion_export_maps = class(TDataModule)
    qry_CalcRegion: TADOQuery;
    qry_Cells: TADOQuery;
    qry_Cell_Layers: TADOQuery;
    ds_Cell_Layers: TDataSource;
    ds_Cells: TDataSource;
    ds_Region: TDataSource;
    qry_ColorSchema: TADOQuery;
    qry_ColorSchemaRanges: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);

  private
    FCalcRegionFileDir: string;

    FBLRect: TBLRect;
    FxyRectMax: TXYRect;
    FCalcRegionID: integer;

//    Fsql: string;
//    procedure OpenCellLayers(aCalcRegionID: Integer);

    procedure AddCellLayers;
    procedure AddCells(aCellLayerID: integer);
    procedure AddBinMatrixes;
    procedure AddError(aMsg: string);

    procedure AddParams;
    procedure AddWarning(aMsg: string);
    function AddColorSchema(aID: integer): boolean;
    procedure OpenData;
//    procedure RegisterMaps__(aCalcRegionID: integer);

  private
    FBinToMifParams: TBinToMifParams;

  public
    Params: record
      ColorSchemaID_KUP      : integer;
      ColorSchemaID_KMO      : integer;
//      ColorSchemaID_Handover : integer;

      MapType: TdmCalcRegion_export_mapsMapType;

 //     IsUpdateKgr: boolean;

//      CellLayerIDList: TIDList;

      IsVerifyParams: boolean;

      CalcFileDir: string;

      MapTypes: TCalcMapTypes;
    end;

//    CalcRegionTypeIsGSM: boolean;

    destructor Destroy; override;
//    procedure UpdateKgr    (aCalcRegionID: integer);
    procedure RegisterMaps(aCalcRegionID: integer);
//    procedure RegisterMapsOld (aCalcRegionID: integer);

    function Execute_SaveToXMLFile(aCalcRegionID: integer; aFileName: string): boolean;
  end;



function  dmCalcRegion_export_maps: TdmCalcRegion_export_maps;

//==================================================================
implementation {$R *.DFM}
//==================================================================

var
  FdmCalcRegion_export_maps: TdmCalcRegion_export_maps;


const
  ATT_kgr_FileName = 'kgrFileName';

  TAG_COLOR_SCHEMAS = 'COLOR_SCHEMAS';



//--------------------------------------------------------------------
function  dmCalcRegion_export_maps: TdmCalcRegion_export_maps;
//--------------------------------------------------------------------
begin
  if not Assigned(FdmCalcRegion_export_maps) then
    FdmCalcRegion_export_maps:= TdmCalcRegion_export_maps.Create(Application);

  Result:= FdmCalcRegion_export_maps;
end;

destructor TdmCalcRegion_export_maps.Destroy;
begin
  FreeAndNil(FBinToMifParams);
  inherited Destroy;
end;

//------------------------------------------------------------------------------
procedure TdmCalcRegion_export_maps.DataModuleCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;
  Params.MapTypes:= [mtKmo, mtKup, mtKgr, mtHandover];

  FBinToMifParams := TBinToMifParams.Create();

  db_SetComponentADOConnection(Self, dmMain.AdoConnection);


end;


//------------------------------------------------------------------------------
procedure TdmCalcRegion_export_maps.AddCells(aCellLayerID: integer);
//------------------------------------------------------------------------------

var
  vNode: Variant;
  sKupFileName: string;
  iID: integer;
  sql: string;
  oCell: TCell;

begin
  qry_Cells.Filter:= Format('cell_layer_id=%d', [aCellLayerID]);

  with qry_Cells do
     while not EOF do
  begin
    oCell:= FBinToMifParams.CellList.AddItem;
    oCell.ID    :=  qry_Cells.FieldByName(FLD_ID).AsInteger;
    oCell.Color :=  qry_Cells.FieldByName(FLD_COLOR).AsInteger;

    Next;
  end;

end;

//------------------------------------------------------------------------------
procedure TdmCalcRegion_export_maps.AddCellLayers;
//------------------------------------------------------------------------------
//var
//  vCells: Variant;
begin

 // vCells:=xml_AddNodeTag (aParentNode, 'CELLS');

 // qry_Cell_Layers.First;

  with qry_Cell_Layers do
     while not EOF do
  begin
    AddCells (qry_Cell_Layers[FLD_ID]);
    Next;
  end;
end;

//------------------------------------------------------------------------------
procedure TdmCalcRegion_export_maps.AddBinMatrixes;
//------------------------------------------------------------------------------
var
 // vGroup, vNode, vNode1: Variant;
  iID, iCalcStep: integer;
  sFileDir: string;
  oBinMatrix: TBinMatrixFile;
begin
  iCalcStep := qry_CalcRegion.FieldByName(FLD_STEP).AsInteger;

  FCalcRegionFileDir:=dmCalcRegion_pmp.GetFileDir (FCalcRegionID, Params.CalcFileDir);

 // vGroup := xml_AddNodeTag(aParentNode,'MATRIXES');


  qry_Cell_Layers.First;

  with qry_Cell_Layers do
    while not EOF do
      begin
        iID:=qry_Cell_Layers[FLD_ID];
//        sFileDir:=FCalcRegionFileDir + Format('cell_layer.%d\', [iID]);
        sFileDir:=FCalcRegionFileDir + Format('cell_layer.%d\', [iID]);
    //    sFileDir:=FCalcRegionFileDir + Format('cell_layer.%d\map\', [iID]);



        oBinMatrix := FBinToMifParams.BinMatrixList.AddItem;
        oBinMatrix.FileName     :=sFileDir + 'region.kgr';
        oBinMatrix.MapFileName  :=sFileDir + 'region.kgr.tab';

        if Params.ColorSchemaID_KUP>0 then
        begin
          oBinMatrix := FBinToMifParams.BinMatrixList.AddItem;
          oBinMatrix.FileName       :=sFileDir + 'region.kup';
          oBinMatrix.MapFileName    :=sFileDir + 'region.kup.tab';
          oBinMatrix.ColorSchema_ID :=Params.ColorSchemaID_KUP;
  //        oBinMatrix.ColorSchema_ID :=aColorSchema_ID;
        end;


 
        if Params.ColorSchemaID_KMO>0 then
        begin
          oBinMatrix := FBinToMifParams.BinMatrixList.AddItem;
          oBinMatrix.FileName       :=sFileDir + 'region.kmo';
          oBinMatrix.MapFileName    :=sFileDir + 'region.kmo.tab';
          oBinMatrix.ColorSchema_ID :=Params.ColorSchemaID_KMO;
        end;
       


      Next;
  end;

end;

//------------------------------------------------------------------------------
procedure TdmCalcRegion_export_maps.AddParams;
//------------------------------------------------------------------------------
begin

  case Params.MapType of
    mtVector: FBinToMifParams.MapType := mtVector_;
    mtRaster: FBinToMifParams.MapType := mtRaster_;
  end;
end;






//------------------------------------------------------------------------------
procedure TdmCalcRegion_export_maps.RegisterMaps(aCalcRegionID: integer);
//------------------------------------------------------------------------------
var
  iStep, iID : integer;
  sKey, sName, sFileDir, sCalcRegionFileDir: string;

  rec: TdmCalcMapAddRec1;
  s: string;
begin
  iStep := qry_CalcRegion.FieldByName(FLD_STEP).AsInteger;

  sCalcRegionFileDir:= dmCalcRegion_pmp.GetFileDir (aCalcRegionID, Params.CalcFileDir);


  FillChar(rec, SizeOf(rec), 0);

  rec.PmpCalcRegionID:=aCalcRegionID;

  qry_Cell_Layers.First;
  with qry_Cell_Layers do
    while not EOF do
  begin
    iID     := qry_Cell_Layers[FLD_ID];
    sKey    := Format ('region.%d-step.%d-cell_layer.%d', [aCalcRegionID, iStep, iID]);
    sFileDir:= sCalcRegionFileDir + Format('cell_layer.%d\', [iID]);


     rec.CellLayer_ID  := qry_Cell_Layers.FieldByName(FLD_ID).AsInteger;


  //  rec.KeyName:=sKey;
//    rec.Name:=qry_Cell_Layers[FLD_NAME];
//    rec.CellLayer_ID  := qry_Cell_Layers[FLD_ID];
//    rec.Name:=qry_Cell_Layers[FLD_CellLayer_NAME];

  ///  iParentID:= dmCalcMap.RegisterCellLayer(aCalcRegionID, iID, qry_Cell_Layers[FLD_NAME]);

//    iParentID:= dmCalcMap.RegisterCellLayer(aCalcRegionID, iID, qry_Cell_Layers[FLD_CELL_LAYER_NAME]);

    if DirectoryExists(sFileDir) then
    begin

    ///  rec.ParentID:=iParentID;
      rec.PmpCalcRegionID := aCalcRegionID;

      // -------------------------
      s:= sFileDir + 'region.kmo';



      sName:= sFileDir + 'region.kmo.tab';
//      rec.FileName  := sName;
      rec.TabFileName  := sName;
      rec.Name      := '����� ��������';
      rec.Matrix_FileName := sFileDir + 'region.kmo';

    //  rec.KeyName   := sKey+'-kmo';

//      Assert (FileExists(sName), 'File not found: '+ sName);

      if FileExists(sName) then
        dmCalcMap.RegisterCalcMap_ (rec)//, iParentID, aCalcRegionID, '����� ��������', sName, sKey+'-kmo');
      else
        ShowMessage ('File not found: '+ sName);

      // -------------------------
      s:= sFileDir + 'region.kup';


      sName:= sFileDir + 'region.kup.tab';
  //    rec.FileName:= sName;
      rec.TabFileName:= sName;
      rec.Name    := '����� ������ ����';
      rec.Matrix_FileName := sFileDir + 'region.kup';

   //   rec.KeyName := sKey+'-kup';

    //  Assert (FileExists(sName), 'File not found: '+sName);

      if FileExists(sName) then
        dmCalcMap.RegisterCalcMap_ (rec)//, iParentID, aCalcRegionID, '����� ������ ����', sName, sKey+'-kup');
      else
        ShowMessage ('File not found: '+ sName);


      // -------------------------
      s:= sFileDir + 'region.kgr';


      sName:= sFileDir + 'region.kgr.tab';
//      rec.FileName := sName;
      rec.TabFileName := sName;
      rec.Name := '����� ������';
      rec.Matrix_FileName := sFileDir + 'region.kgr';

   //   rec.KeyName :=sKey+'-kgr';

    //  Assert (FileExists(sName), 'File not found: '+ sName);


      if FileExists(sName) then
        dmCalcMap.RegisterCalcMap_ (rec)//, iParentID, aCalcRegionID, '����� ������', sName, sKey+'-kgr');
      else
        ShowMessage ('File not found: '+ sName);


        {
      // -------------------------
      sName:= sFileDir + 'region.hoff.tab';
//      rec.FileName := sName;
      rec.TabFileName := sName;
      rec.Name := '����� handover';
   //   rec.KeyName :=sKey+'-hoff';

      if FileExists(sName) then
        dmCalcMap.RegisterCalcMap (rec);//, iParentID, aCalcRegionID, '����� handover', sName, sKey+'-hoff');
      }

    end;

    Next;
  end;


end;



//-------------------------------------------------------------------
function TdmCalcRegion_export_maps.AddColorSchema(aID: integer): boolean;
//-------------------------------------------------------------------
var
  oColorSchema: TColorSchema;
  oColorSchemaRange: TColorSchemaRange;
begin
  db_OpenQueryByFieldValue (qry_ColorSchema, TBL_COLORSCHEMA, FLD_ID, aID);

  Result := qry_ColorSchema.RecordCount=1;

  with qry_ColorSchema do
    while not Eof do
    begin
      oColorSchema := FBinToMifParams.ColorSchemaList.AddItem;
      
      oColorSchema.ID   := FieldValues[FLD_ID];
      oColorSchema.Name := FieldValues[FLD_NAME];

       db_OpenQuery (qry_ColorSchemaRanges,
                'SELECT * FROM '+ TBL_COLORSCHEMARANGES +
                ' WHERE (color_schema_id=:color_schema_id) and (enabled<>0) '+
                ' ORDER BY min_value ',
               [db_Par(FLD_color_schema_id, FieldValues[FLD_ID])] );

       Result:= not qry_ColorSchemaRanges.IsEmpty;

       with qry_ColorSchemaRanges do
         while not Eof do
         begin
           oColorSchemaRange := oColorSchema.Ranges.AddItem;

           oColorSchemaRange.Color := FieldByName('Color').AsInteger;

           oColorSchemaRange.MinValue := FieldByName('min_value').AsInteger;
           oColorSchemaRange.MaxValue := FieldByName('max_value').AsInteger;
           oColorSchemaRange.MinColor := FieldByName('min_Color').AsInteger;
           oColorSchemaRange.MaxColor := FieldByName('max_Color').AsInteger;
           oColorSchemaRange.Type_    := FieldByName(FLD_TYPE).AsInteger;
           oColorSchemaRange.Comment  := FieldByName(FLD_COMMENT).AsString;

           Next;
         end;

       Next;
    end;
end;

// ---------------------------------------------------------------
procedure TdmCalcRegion_export_maps.OpenData;
// ---------------------------------------------------------------
begin
  db_OpenQueryByFieldValue (qry_CalcRegion, TBL_PMP_CALCREGION, FLD_ID, FCalcRegionID);

  dmOnega_DB_data.OpenQuery(qry_Cell_Layers, //SQL,
         'SELECT * FROM '+ VIEW_PMP_CALCREGION_USED_CELL_Layers +
         ' WHERE (PMP_Calc_Region_ID = :PMP_Calc_Region_ID)',
        [FLD_PMP_Calc_Region_ID, FCalcRegionID ]);

  dmOnega_DB_data.OpenQuery (qry_Cells,
      'SELECT * FROM '+ VIEW_PMP_CALCREGION_PMP_SECTORS +
      ' WHERE (pmp_calc_region_id=:pmp_calc_region_id) and (checked=1)',
      [FLD_pmp_CALC_REGION_ID, FCalcRegionID ]);

  qry_Cells.Filtered:= True;
end;


//----------------------------------------------------
function TdmCalcRegion_export_maps.Execute_SaveToXMLFile(aCalcRegionID: integer;
    aFileName: string): boolean;
//----------------------------------------------------
var
  oXMLDoc: TXMLDoc;
  sSql, S: string;
begin
  Result:= false;

  FCalcRegionID:=aCalcRegionID;

  OpenData();

  oXMLDoc:=TXMLDoc.Create;

  CursorHourGlass;

//  custom_DbErrorLog.AddText('������������ ����');

  if Params.ColorSchemaID_KUP = 0 then
    AddWarning('�� ������ �������� ����� ��� ����� ������ ����');

  if Params.ColorSchemaID_KMO = 0 then
    AddWarning('�� ������ �������� ����� ��� ����� ��������');

//  if Params.ColorSchemaID_Handover = 0 then
//    AddWarning('�� ������ �������� ����� ��� ����� handover');

  if Params.ColorSchemaID_KUP > 0 then
//    if not dmColorSchema_export.AddColorSchema (Params.ColorSchemaID_KUP, vGroup) then
    if not AddColorSchema (Params.ColorSchemaID_KUP) then
      AddError('�������� ����� ��� ����� ������ ����. �� ������ ���������');

  if Params.ColorSchemaID_KMO > 0 then
//    if not dmColorSchema_export.AddColorSchema (Params.ColorSchemaID_KMO, vGroup) then
    if not AddColorSchema (Params.ColorSchemaID_KMO) then
    begin
      Params.ColorSchemaID_KMO:=0;
      AddError('�������� ����� ��� ����� ��������. �� ������ ���������');
    end;

{
  if Params.ColorSchemaID_Handover > 0 then
//    if not dmColorSchema_export.AddColorSchema (Params.ColorSchemaID_Handover, vGroup) then
    if not AddColorSchema (Params.ColorSchemaID_Handover) then
      begin
        Params.ColorSchemaID_Handover  := 0;
        AddError('�������� ����� ��� ����� handover. �� ������ ���������');
      end;
}

 // OpenCellLayers(FCalcRegionID);

  AddParams ();
  AddCellLayers ();
  AddBinMatrixes ();
//  AddBinMatrixes (Params.ColorSchemaID_KUP);

  oXMLDoc.SaveToFile (aFileName);

  oXMLDoc.Free;

  CursorDefault;

  Result:= true;

  FBinToMifParams.SaveToXMLFile(aFileName);

end;


procedure TdmCalcRegion_export_maps.AddWarning(aMsg: string);
begin
 // custom_DbErrorLog.AddWarning(aMsg);
end;

procedure TdmCalcRegion_export_maps.AddError(aMsg: string);
begin
 // custom_DbErrorLog.AddError(aMsg);
end;



end.






{




//------------------------------------------------------------------------------
procedure TdmCalcRegion_export_maps.RegisterMaps__(aCalcRegionID: integer);
//------------------------------------------------------------------------------
var
  iStep, iID : integer;
  sKey, sName, sFileDir, sCalcRegionFileDir: string;

  rec: TdmCalcMapAddRec1;
  s: string;
begin
  iStep := qry_CalcRegion.FieldByName(FLD_STEP).AsInteger;

  sCalcRegionFileDir:= dmCalcRegion_pmp.GetFileDir (aCalcRegionID, Params.CalcFileDir);


  FillChar(rec, SizeOf(rec), 0);

  rec.PmpCalcRegionID:=aCalcRegionID;

  qry_Cell_Layers.First;
  with qry_Cell_Layers do
    while not EOF do
  begin
    iID     := qry_Cell_Layers[FLD_ID];
    sKey    := Format ('region.%d-step.%d-cell_layer.%d', [aCalcRegionID, iStep, iID]);
    sFileDir:= sCalcRegionFileDir + Format('cell_layer.%d\', [iID]);


     rec.CellLayer_ID  := qry_Cell_Layers.FieldByName(FLD_ID).AsInteger;


   // rec.KeyName:=sKey;
   // rec.Name:=qry_Cell_Layers[FLD_NAME];
//    rec.Name:=qry_Cell_Layers[FLD_CellLayer_NAME];

//    iParentID:= dmCalcMap.RegisterCellLayer(aCalcRegionID, iID, qry_Cell_Layers[FLD_NAME]);

//    iParentID:= dmCalcMap.RegisterCellLayer(aCalcRegionID, iID, qry_Cell_Layers[FLD_CELL_LAYER_NAME]);

    if DirectoryExists(sFileDir)  then //and (iParentID>0)
    begin
  //    rec.ParentID:=iParentID;
      rec.PmpCalcRegionID := aCalcRegionID;


     // s := sFileDir + Format('%d.kmo', [iID]);
      s:= sFileDir + 'region.kmo';

      // -------------------------

      sName:= sFileDir + 'region.kmo.tab';
      rec.TabFileName  := sName;
      rec.Name      := '����� ��������';

  //    rec.KeyName   := sKey+'-kmo';

      if FileExists(sName) then
        dmCalcMap.RegisterCalcMap (rec);//, iParentID, aCalcRegionID, '����� ��������', sName, sKey+'-kmo');

      // -------------------------
      sName:= sFileDir + 'region.kup.tab';
      rec.TabFileName:= sName;
      rec.Name    := '����� ������ ����';
    //  rec.KeyName := sKey+'-kup';

      if FileExists(sName) then
        dmCalcMap.RegisterCalcMap (rec);//, iParentID, aCalcRegionID, '����� ������ ����', sName, sKey+'-kup');

      // -------------------------
      sName:= sFileDir + 'region.kgr.tab';
//      rec.FileName := sName;
      rec.TabFileName := sName;
      rec.Name := '����� ������';
   //   rec.KeyName :=sKey+'-kgr';

      if FileExists(sName) then
        dmCalcMap.RegisterCalcMap (rec);//, iParentID, aCalcRegionID, '����� ������', sName, sKey+'-kgr');
    {
      // -------------------------
      sName:= sFileDir + 'region.hoff.tab';
//      rec.FileName := sName;
      rec.TabFileName := sName;
      rec.Name := '����� handover';
    //  rec.KeyName :=sKey+'-hoff';

      if FileExists(sName) then
        dmCalcMap.RegisterCalcMap (rec);//, iParentID, aCalcRegionID, '����� handover', sName, sKey+'-hoff');

       } 
    end;

    Next;
  end;


end;



}


