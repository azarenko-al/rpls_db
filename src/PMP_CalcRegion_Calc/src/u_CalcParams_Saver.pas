unit u_CalcParams_Saver;

interface

uses Classes,SysUtils,Variants, XMLDoc, XMLIntf,

   u_xml_document,

   u_func,
   u_Geo,

   u_CalcParams_;

type
  TCalcParamsSaver = class
  private
    FCalcParams: TCalcParams;

    procedure AddAntennaTypesNode(aParentNode: IXMLNode);
    procedure AddCalcModelsNode(aParentNode: IXMLNode);
    procedure AddCalcRegionNode(aParentNode: IXMLNode);
    procedure AddCellAntennasNode(aCell_ID: integer; aParentNode: IXMLNode;
        aRadius: double; aCellAlias: string; aCell: TcmpCell);
    procedure AddCellsNode(aParentNode: IXMLNode);
    procedure AddCluttersNode(aParentNode: IXMLNode);
    procedure AddReliefNode(aParentNode: IXMLNode);

  public
    procedure SaveToXML(aCalcParams: TCalcParams; aFileName: string);
  end;



implementation


const

  ANTENNA_WS_FILENAME  = 'WS\ant%d.ws';

  ATT_ID            = 'id';

  ATT_LOSS          = 'loss';


  ATT_CALCMODEL_ID = 'CalcModel_ID';
  ATT_SITE_ID      = 'SiteID';
  ATT_SITE_NAME    = 'SiteName';
  TAG_ITEM         = 'ITEM';

  ATT_FILENAME = 'FILENAME';

  ATT_RADIUS       = 'Radius';
//  ATT_RADIUS_CIA   = 'Radius_cia';
//  ATT_RADIUS_COV   = 'Radius_cov';
  ATT_STANDART_NAME= 'StandardName';
  ATT_ANTENNA_TYPES= 'ANTENNA_TYPES';
  ATT_MODEL        = 'MODEL';
  ATT_MODELS       = 'MODELS';
  ATT_ANTENNA      = 'ANTENNA';
  ATT_CELLID       = 'CELLID';
  ATT_TRX          = 'TRX';
  ATT_SLOPE        = 'Slope';
  ATT_ABONENT      = 'abonent';
  ATT_HEIGHT       = 'height';
  ATT_reflection_coef = 'reflection_coef';
  ATT_MATRIXES     = 'MATRIXES';
  ATT_CLUTTERS     = 'CLUTTERS';
  ATT_COMBINER_LOSS= 'Combiner_Loss';
  ATT_FEEDERLOSS  = 'FeederLoss';
  ATT_SENSE       = 'Sensitivity';
  ATT_POWER       = 'power';
  ATT_FREQ        = 'freq';
  ATT_AZIMUTH       = 'azimuth';
  ATT_GAIN          = 'gain';

  ATT_INDEX = 'INDEX';

  ATT_REGION        = 'region';
  ATT_ZONENUM       = 'zonenum';
  ATT_ROTATE        = 'ROTATE';


              
//----------------------------------------------------
procedure TCalcParamsSaver.AddAntennaTypesNode(aParentNode: IXMLNode);
//----------------------------------------------------
var
  vNode1,vNode,vNode_vert,vNode_hor: IXMLNode;
  sAntTypeAlias, s, sIds, sSql: string;
  fAngle, fLoss: double;
  i: Integer;
 // HorzMaskArr, VertMaskArr: TdmAntTypeMaskArray;

  sVert_mask,sHorz_mask: string;

  oItem: TcmpAntennaType;
begin
  vNode:=aParentNode.AddChild (ATT_ANTENNA_TYPES);


  for I := 0 to FCalcParams.AntennaTypes.Count - 1 do
  begin
    oItem:=FCalcParams.AntennaTypes[i];

(*    sAntTypeAlias:= Format('��� �������-%s, id-%d. ',
        [oItem.Name, oItem.ID]);
*)

    vNode1:=xml_AddNode (vNode, TAG_ITEM,
          [xml_Par (ATT_ID,   oItem.ID),
           xml_Par (ATT_NAME, oItem.Name),
           xml_Par (ATT_GAIN, oItem.Gain)
           ]);

    xml_AddNode (vNode1, 'horz_losses', [xml_Par('value_ex', oItem.Horz_mask)]);
    xml_AddNode (vNode1, 'vert_losses', [xml_Par('value_ex', oItem.Vert_mask)]);

  end;



end;

//----------------------------------------------------
procedure TCalcParamsSaver.AddCellAntennasNode(aCell_ID: integer; aParentNode:
    IXMLNode; aRadius: double; aCellAlias: string; aCell: TcmpCell);
//----------------------------------------------------
var
  I: Integer;
  vNode: IXMLNode;
  sField, sFileName: string;
  iID: integer;
  blPos: TBLPoint;
  xyPos: TXYPoint;

  oAntenna: TcmpAntenna;

begin

  //  DoVerifyParams();

  for I := 0 to aCell.Antennas.Count - 1 do
  begin
    oAntenna := aCell.Antennas[i];


    vNode:=xml_AddNode (aParentNode, ATT_ANTENNA,
            [xml_Par ('Antenna_Type_ID',   oAntenna.AntennaType_ID),
             xml_Par (ATT_ID,              oAntenna.ID),
             xml_Par (ATT_FEEDERLOSS,      oAntenna.FeederLoss),
             xml_Par (ATT_HEIGHT,          oAntenna.height),
             xml_Par (ATT_AZIMUTH,         oAntenna.AZIMUTH),
             xml_Par (ATT_SLOPE,           oAntenna.TILT),
        //     xml_Par (ATT_ROTATE,          oAntenna.Heel),
          //   xml_Par (ATT_GAIN,            oAntenna.gain_dB),

             xml_Par ('B',                 oAntenna.blPos.B),
             xml_Par ('L',                 oAntenna.blPos.L),
  //           xml_Par ('x',                 rec.xyPos.X),
  //           xml_Par ('y',                 rec.xyPos.Y),

             xml_Par (ATT_ZONENUM,         oAntenna.ZoneNum),
             xml_Par (ATT_RADIUS,          oAntenna.CalcRadius_KM)
         ]);

        xml_AddNode (vNode, 'plFileName',
             [xml_Par (ATT_VALUE, oAntenna.plFileName)]);

  end;

end;


// ---------------------------------------------------------------
procedure TCalcParamsSaver.AddCellsNode(aParentNode: IXMLNode);
// ---------------------------------------------------------------
var
  I: Integer;

  oCell: TcmpCell;
  vNode, vNode1, vNode2, vNode3: IXMLNode;
  sCellAlias: string;
  dRadius_km: Double;
begin
  vNode:= aParentNode.AddChild ('CELLS');

  for I := 0 to FCalcParams.Cells.Count - 1 do
  begin
    oCell:=FCalcParams.Cells[i];

    if not oCell.EnabledToExport then
      Continue;


   // iCell_ID:= qry_Cells[FLD_ID];
(*    sCellAlias:= Format('������-%s,id-%d; ��-%s,id-%d. ',
                [FieldByName(FLD_NAME).AsString, iCell_ID,
                 FieldByName(FLD_SITE_NAME).AsString,
                 FieldByName(FLD_SITE_ID).AsInteger]);
*)

    sCellAlias:= Format('������-%s,id-%d; ��-%s,id-%d. ',
          [oCell.Name, oCell.ID, oCell.Site_Name, oCell.SITE_ID]);


    vNode1:=xml_AddNode (vNode, 'CELL',
          [
           xml_Par (ATT_INDEX,         i),
           xml_Par (ATT_ID,            oCell.id),
           xml_Par (ATT_NAME,          oCell.Name),
           xml_Par (ATT_COMBINER_LOSS, oCell.COMBINER_LOSS),
           xml_Par (ATT_CALCMODEL_ID,  oCell.CALCMODEL_ID),
           xml_Par (ATT_SITE_ID,       oCell.SITE_ID),
           xml_Par (ATT_SITE_NAME,     oCell.SITE_NAME),
           xml_Par (ATT_STANDART_NAME, oCell.Standard_Name),
         //  xml_Par (ATT_RADIUS_CIA,    0),
          // xml_Par (ATT_RADIUS_COV,    FieldValues[FLD_CALC_RADIUS]),
           xml_Par (ATT_RADIUS,        oCell.CALC_RADIUS_km),
           xml_Par ('Radius_signal',   oCell.Radius_signal_km),
           xml_Par ('K0_open',         oCell.K0_OPEN),
           xml_Par ('K0_closed',       oCell.K0_CLOSED),
           xml_Par ('K0',              oCell.k0 ),
           xml_Par (ATT_CELLID,        oCell.CELLID)
           ]);

    vNode3:=xml_AddNode (vNode1, 'kupFileName',
                           [xml_Par (ATT_VALUE, oCell.kupFileName)]);

    vNode2:=xml_AddNode (vNode1, ATT_TRX,
          [xml_Par (ATT_POWER,  oCell.TRX.POWER_W),
           xml_Par (ATT_SENSE,  oCell.TRX.Sensitivity),
           xml_Par (ATT_FREQ,   oCell.TRX.FREQ_MHz)
           ]);

    dRadius_km:= oCell.CALC_RADIUS_km;// qry_Cells.FieldByName(FLD_CALC_RADIUS).AsFloat;

    AddCellAntennasNode (oCell.ID, vNode1, dRadius_km, sCellAlias, oCell);

  end;


end;

//----------------------------------------------------
procedure TCalcParamsSaver.AddReliefNode(aParentNode: IXMLNode);
//----------------------------------------------------
var vNode: IXMLNode;
  i: integer;
//  iIndex, i,
  iPolyID: integer;
  oStrList: TStringList;
  blRect: TBLRect;
  iStep: integer;
  sFileName: string;
begin
(*  oStrList:= TStringList.Create;

  iPolyID:= dmCalcRegion.GetIntFieldValue (FCalcRegionID, FLD_POLYGON_ID);
  blRect:=dmPolygons.GetBoundBLRect (iPolyID);

  //dmRelMatrixFile.GetListInRect (blRect, FStrList);
  dmRelMatrixFile.GetListInRect (blRect, oStrList, FRelBestZone, iStep);
*)

(*  if Params.ExcludeMatrixesIDList.Count>0 then
    for I := 0 to Params.ExcludeMatrixesIDList.Count-1 do
    begin
      iIndex := oStrList.IndexOf(Params.ExcludeMatrixesIDList[i].Name);
      if iIndex >= 0 then
        oStrList.Delete(iIndex);
    end;*)
(*
  if (oStrList.Count=0) then
    custom_DbErrorLog.AddError('�� ������ ������� �����, ���� �������� ������� �� ������������ �� ����������� � ������� �������');
*)

  vNode:=aParentNode.AddChild( ATT_MATRIXES);
  for i:=0 to FCalcParams.ReliefMatrixFiles.Count-1 do
  begin
    sFileName :=FCalcParams.ReliefMatrixFiles[i];
  ////////////  if FileExists(sFileName) then
      xml_AddNode (vNode, TAG_ITEM,  [xml_Par (ATT_FILENAME, sFileName) ]);
  end;

//  oStrList.Free;
end;


// ---------------------------------------------------------------
procedure TCalcParamsSaver.AddCalcRegionNode(aParentNode: IXMLNode);
// ---------------------------------------------------------------
var
  vNode: IXMLNode;
  oCalcRegion: TCalcRegion;
begin
  oCalcRegion:=FCalcParams.CalcRegion;

  vNode:=xml_AddNode (aParentNode, ATT_REGION,
      [xml_Par ('name',              oCalcRegion.Name ),
       xml_Par ('CalcDirection',     IIF(oCalcRegion.CalcDirection=cdBS_to_PO, 'BS_to_PO', 'PO_to_BS')),

     //  xml_Par ('Level_Stored_Noise',oCalcRegion.Level_Stored_Noise),
       xml_Par ('AntennaIsOnRoof',   oCalcRegion.AntennaIsOnRoof),
       xml_Par ('Refraction',        oCalcRegion.refraction),
       xml_Par ('CalcStep',          oCalcRegion.CalcStep),
       xml_Par ('Zone',              oCalcRegion.Zone6),

       xml_Par (ATT_CALCMODEL_ID,    oCalcRegion.CalcModel_ID),
       xml_Par ('K0_open',           oCalcRegion.K0_open ),
       xml_Par ('K0_closed',         oCalcRegion.K0_closed ),
       xml_Par ('K0',                oCalcRegion.k0),

       xml_Par ('TopLeft_x',         oCalcRegion.XYRect.TopLeft.X),
       xml_Par ('TopLeft_y',         oCalcRegion.XYRect.TopLeft.Y),
       xml_Par ('BottomRight_x',     oCalcRegion.XYRect.BottomRight.X),
       xml_Par ('BottomRight_y',     oCalcRegion.XYRect.BottomRight.Y)
      ]);


  xml_AddNode (vNode, ATT_ABONENT,
      [xml_Par (ATT_HEIGHT,     oCalcRegion.Abonent.HEIGHT),
       xml_Par (ATT_POWER,      oCalcRegion.Abonent.Power_W),
       xml_Par (ATT_SENSE,      oCalcRegion.Abonent.Sensitivity),
////////////           xml_Par (ATT_LOSS,       rec.Abonent.LOSS - rec.Abonent.Gain),

       //old ver
       xml_Par (ATT_FEEDERLOSS, oCalcRegion.Abonent.FeederLoss - oCalcRegion.Abonent.Gain)
      ]);

  AddCluttersNode (vNode);

end;


//----------------------------------------------------
procedure TCalcParamsSaver.AddCluttersNode(aParentNode: IXMLNode);
//----------------------------------------------------
var
  I: Integer;
  vNode: IXMLNode;
  sCluModelName: string;
  oItem: TcmpClutterItem;

begin
  vNode:=aParentNode.AddChild( ATT_CLUTTERS);

  FCalcParams.CalcRegion.Clutters.LoadDefaults;

  for I := 0 to FCalcParams.CalcRegion.Clutters.Count - 1 do
  begin
    oItem :=FCalcParams.CalcRegion.Clutters[i];

    xml_AddNode (vNode, TAG_ITEM,
         [
          xml_Par (ATT_TYPE,   oItem.Name),
//          xml_Par (FLD_TYPE,   oItem.Type_),
          xml_Par (ATT_HEIGHT, oItem.Height),
          xml_Par (ATT_LOSS,   oItem.Loss_dBm ),
          xml_Par (ATT_reflection_coef,  oItem.Reflection_coef )
          ]);
  end;

end;


//----------------------------------------------------
procedure TCalcParamsSaver.AddCalcModelsNode(aParentNode: IXMLNode);
//----------------------------------------------------
var
  vNode,vNode1: IXMLNode;
  I: Integer;
  oCalcModel: TcmpCalcModelType;
  j: integer;
begin
  vNode:=aParentNode.AddChild(ATT_MODELS);


  for I := 0 to FCalcParams.CalcModelTypes.Count - 1 do
  begin
    oCalcModel := FCalcParams.CalcModelTypes[i];

    vNode1:=xml_AddNode (vNode, TAG_ITEM,
          [xml_Par (ATT_NAME, oCalcModel.Name),
           xml_Par (ATT_TYPE, oCalcModel.ModelType),
           xml_Par (ATT_ID,   oCalcModel.ID)
           ]);

    for j := 0 to High(oCalcModel.Params) do
      xml_AddNode (vNode1, 'PARAM',
                    [xml_Par (ATT_NAME,  oCalcModel.Params[j].Name),
                     xml_Par (ATT_value, oCalcModel.Params[j].Value)
                    ]);
  end;
end;

// ---------------------------------------------------------------
procedure TCalcParamsSaver.SaveToXML(aCalcParams: TCalcParams; aFileName: string);
// ---------------------------------------------------------------
var
  oXMLDoc: TXMLDoc;
  vRoot: IXMLNode;
 // sValue: string;

begin
  FCalcParams :=aCalcParams;

  Assert(Assigned(aCalcParams), 'Value not assigned');

  oXMLDoc := TXMLDoc.Create;

  vRoot:=oXMLDoc.DocumentElement;

 // sValue:= 'region';

  xml_AddNode (vRoot, 'PARAMS',
       [xml_Par('IsRecalcPathLoss',FCalcParams.IsRecalcWS),
        xml_Par('CalcType',        'region'),
        xml_Par('IsShowMessage',   '0')
       ]);


  AddReliefNode       (vRoot);
  AddCalcRegionNode   (vRoot);
  AddCellsNode        (vRoot);
  AddCalcModelsNode   (vRoot);
  AddAntennaTypesNode (vRoot);

  oXMLDoc.SaveToFile (aFileName);

  oXMLDoc.Free;
end;


end.
