unit dm_CalcRegion_export_calc;

interface

uses
  SysUtils, Classes, Forms, Dialogs, Db, ADODB, Math,

  XMLDoc, XMLIntf,
  u_xml_document,

  dm_Rel_Engine,

  dm_Onega_DB_data,

  u_CalcParams_,
  u_CalcParams_Saver,

  dm_Main,

  u_CalcRegion_export_types,

  dm_CalcRegion,
  dm_CalcRegion_Polygons,

  dm_AntType,

  dm_RelMatrixFile,

  u_classes,

  u_db,

  u_func,
  u_Geo,


  u_const_db;

type

//  TdriveCalcType = (ctRegion); //, , ctCalc, ctCalc, ct, ctCalc, ctCalc, ctCalcCalc, ctCalcvctCalibrate, ctCalibrateRegion


  TdmCalcRegion_export_calc = class(TDataModule)
    qry_CalcRegion: TADOQuery;
    qry_RelMatrix: TADOQuery;
    qry_Cells: TADOQuery;
    qry_Antennas: TADOQuery;
    qry_CalcModel_Params: TADOQuery;
    qry_Clutters: TADOQuery;
    sp_CalcModels: TADOStoredProc;
    sp_AntType: TADOStoredProc;
    qry_Poly: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

  private
    FFileDir,
   // FCellLayerIds,
    FSqlWhereStr : string;
    FCalcStep : integer; //, FPolygonID

    FBLRect: TBLRect;
    FxyRectMax: TXYRect;

    FRelBestZone: integer;
    FZone6: integer;

    FCalcRegionID: integer;

    FIntArray: TIntArray;

    FCalcParams: TCalcParams;

    procedure AddWarning(aMsg: string);
    procedure CalcModel_SaveToXMLNode(aParentNode: IXMLNode);

  protected
    procedure AddCalcRegionNode(aParentNode: IXMLNode);
    procedure AddCluttersNode(aID: integer; aParentNode: IXMLNode);
    procedure AddReliefNode(aParentNode: IXMLNode);

    procedure AddCellsNode(aParentNode: IXMLNode);
    procedure AddCellAntennasNode(aCell_ID: integer; aParentNode: IXMLNode;
        aRadius_KM: double; aCellAlias: string; aCell: TcmpCell);

//    procedure AddCalcModelsNode   (aParentNode: Variant);

    procedure AddAntennaTypesNode(aParentNode: IXMLNode);

    { TODO : to dll }
//    function XYRect_to_CalcRect (aXYRect: TXYRect; aCalcStep:integer): TcalcRect;

  public

    Params: record
      IsRecalcWS:  boolean;
      CellLayerIDList: TIDList;

    end;

    procedure AddError(aMsg: string);
    procedure OpenData;

    procedure Execute_SaveToXMLFile(aCalcRegionID: integer; aFileName: string);


  end;



function dmCalcRegion_export_calc: TdmCalcRegion_export_calc;


//==================================================================
implementation {$R *.DFM}
//==================================================================
var
  FdmCalcRegion_export_calc: TdmCalcRegion_export_calc;

const

  ANTENNA_WS_FILENAME  = 'WS\ant%d.ws';

  ATT_CALCMODEL_ID = 'CalcModel_ID';
  ATT_SITE_ID      = 'SiteID';
  ATT_SITE_NAME    = 'SiteName';
  ATT_RADIUS       = 'Radius';
//  ATT_RADIUS_CIA   = 'Radius_cia';
//  ATT_RADIUS_COV   = 'Radius_cov';
  ATT_STANDART_NAME= 'StandardName';
  ATT_ANTENNA_TYPES= 'ANTENNA_TYPES';
  ATT_MODEL        = 'MODEL';
  ATT_MODELS       = 'MODELS';
  ATT_ANTENNA      = 'ANTENNA';
  ATT_TRX          = 'TRX';
  ATT_SLOPE        = 'Slope';
  ATT_ABONENT      = 'abonent';
  ATT_HEIGHT       = 'height';
  ATT_LOSS          = 'loss';

  ATT_ITEM         = 'ITEM';
  ATT_MATRIXES     = 'MATRIXES';
  ATT_CLUTTERS     = 'CLUTTERS';
  ATT_COMBINER_LOSS= 'Combiner_Loss';
  ATT_FEEDERLOSS  = 'FeederLoss';
  ATT_SENSE       = 'Sensitivity';
  ATT_POWER       = 'power';
  ATT_FREQ        = 'freq';
  ATT_AZIMUTH       = 'azimuth';
  ATT_GAIN          = 'gain';

  ATT_REGION        = 'region';
  ATT_ZONENUM       = 'zonenum';
  ATT_ROTATE        = 'ROTATE';



//--------------------------------------------------------------------
function dmCalcRegion_export_calc: TdmCalcRegion_export_calc;
//--------------------------------------------------------------------
begin
  if not Assigned(FdmCalcRegion_export_calc) then
    FdmCalcRegion_export_calc:=TdmCalcRegion_export_calc.Create(Application);

  Result:= FdmCalcRegion_export_calc;
end;


procedure TdmCalcRegion_export_calc.DataModuleCreate(Sender: TObject);
begin
  inherited;

  db_SetComponentADOConnection(Self, dmMain.AdoConnection);

//  Params.ExcludeMatrixesIDList:=  TIDList.Create;
  Params.CellLayerIDList:= nil;

  FCalcParams := TCalcParams.Create();
end;


procedure TdmCalcRegion_export_calc.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FCalcParams);
//  Params.ExcludeMatrixesIDList.Free;

  inherited;
end;



//----------------------------------------------------
procedure TdmCalcRegion_export_calc.AddCalcRegionNode(aParentNode: IXMLNode);
//----------------------------------------------------
var vNode: IXMLNode;
   rec: TRegionRec_;

  //-------------------------------------
  function DoCalcRegionVerifyParams: boolean;
  //-------------------------------------
  begin
   // custom_DbErrorLog.AddText('������ ������ ����. ����� �������  '+rec.Name);

    if (FCalcStep<=0) then
      AddError('��� ������� <=0');

    if (FBLRect.TopLeft.B<=0)     then AddError('������� ������: ������� �����: ������<=0');
    if (FBLRect.TopLeft.L<=0)     then AddError('������� ������: ������� �����: �������<=0');
    if (FBLRect.BottomRight.B<=0) then AddError('������� ������: ������ ������: ������<=0');
    if (FBLRect.BottomRight.L<=0) then AddError('������� ������: ������ ������: �������<=0');

    if (FZone6<=0)                 then AddError('6-�� ��������� ����<=0');


(*    if (rec.CalcModel_ID<=0)     then AddError('�� ������ ������ �������');
    if (rec.ClutterModel_ID<=0)  then AddError('�� ������ ������ �������� ��������');
    if (rec.Refraction<=0)       then AddError('�� ����� ����������� ���������');
//    if (rec.Level_Stored_Noise<=0)  ????

    if (rec.Abonent.Height<=0)  then AddError('������ ������������ ���������<=0');
    if (rec.Abonent.Power_W<=0) then AddWarning('�������� ������������ ���������<=0');

    if (rec.Abonent.Sensitivity_dBm >=0)
                                then AddWarning('���������������� ������������ ���������>=0');
*)

 end;
  //-------------------------------------

var
  iValue: Integer;
  oItem: TCalcRegion;

begin
  try
 // db_OpenQuery (qry_CalcRegion, TBL_PMP_CALCREGION, FLD_ID, FCalcRegionID);

  FCalcStep := qry_CalcRegion.FieldByName(FLD_STEP).AsInteger;

 // FPolygonID:= qry_CalcRegion.FieldByName(FLD_POLYGON_ID).AsInteger;
  FBLRect   :=dmCalcRegion_Polygons.Get_BoundBLRect(FCalcRegionID);

(*    if ExtractFileExt(FStrList4[0]) = '.rlf' then
      FZone   := geo_Get6ZoneBL(FBLRect.TopLeft)
    else*)
  //  FZone   := geo_Get6ZoneL(FBLRect.TopLeft.L);

  if FRelBestZone>0 then
    FZone6:=FRelBestZone
  else
    FZone6:=geo_Get6ZoneL(FBLRect.TopLeft.L);

  FxyRectMax:=geo_GetRoundXYRect (FBLRect, FZone6);


  with qry_CalcRegion do
  begin
    rec.ID                      :=qry_CalcRegion.FieldByName(FLD_ID).AsInteger;
    rec.Name                    :=qry_CalcRegion.FieldByName(FLD_NAME).AsString;
    rec.CalcDirection           :=IIF(FieldByName(FLD_DIRECTION).AsInteger=0, cdBS_to_PO, cdPO_to_BS);

    rec.CalcModel_ID            :=FieldByName(FLD_CALC_MODEL_ID).AsInteger;
    rec.CalcStep                :=FCalcStep;

    rec.K0                      :=FieldByName(FLD_K0).AsFloat;
    rec.K0_open                 :=FieldByName(FLD_K0_OPEN).AsFloat;
    rec.K0_closed               :=FieldByName(FLD_K0_CLOSED).AsFloat;

    rec.ClutterModel_ID         :=FieldByName(FLD_CLUTTER_MODEL_ID).AsInteger;
    rec.AntennaIsOnRoof         :=FieldByName(FLD_AntennaOnRoof).AsBoolean;
    rec.Refraction              :=FieldByName(FLD_refraction).AsFloat;
  //  rec.Level_Stored_Noise      := FieldByName(FLD_Level_Stored_Noise).AsInteger;

    rec.XYRect.TopLeft.X        :=Floor(FxyRectMax.TopLeft.X);
    rec.XYRect.TopLeft.Y        :=Floor(FxyRectMax.TopLeft.Y);
    rec.XYRect.BottomRight.X    :=Floor(FxyRectMax.BottomRight.X);
    rec.XYRect.BottomRight.Y    :=Floor(FxyRectMax.BottomRight.Y);

    rec.BLRect := FBLRect;

    rec.Abonent.Height          :=FieldByName(FLD_TERMINAL_HEIGHT).AsFloat;
    rec.Abonent.Power_W         :=FieldByName(FLD_Terminal_Power_W).AsFloat;
    rec.Abonent.Sensitivity     :=FieldByName(FLD_Terminal_Sensitivity).AsFloat;
//    rec.Abonent.Loss          :=FieldByName(FLD_TERMINAL_LOSS).AsFloat;
    rec.Abonent.FeederLoss   :=FieldByName(FLD_Terminal_Loss).AsFloat;
    rec.Abonent.Gain         :=FieldByName(FLD_TERMINAL_GAIN).AsFloat;


 //   Assert (rec.Abonent.Gain>0, 'rec.Abonent.Gain=0');

    rec.Zone6:=FZone6;



    // ---------------------------------------------------------------
     oItem:=FCalcParams.CalcRegion;

     oItem.ID                     := FieldByName(FLD_ID).AsInteger;;
     oItem.Name                   := FieldByName(FLD_NAME).AsString;

     iValue                       :=FieldByName(FLD_DIRECTION).AsInteger;

     oItem.CalcDirection          := IIF(FieldByName(FLD_DIRECTION).AsInteger=0, cdBS_to_PO, cdPO_to_BS);

//               := rec.CalcDirection;

     oItem.CalcModel_ID            := FieldByName(FLD_CALC_MODEL_ID).AsInteger;
     oItem.CalcStep                := FCalcStep;

     oItem.K0                      := FieldByName(FLD_K0).AsFloat;
     oItem.K0_open                 := FieldByName(FLD_K0_OPEN).AsFloat;
     oItem.K0_closed               := FieldByName(FLD_K0_CLOSED).AsFloat;

     oItem.ClutterModel_ID         := FieldByName(FLD_CLUTTER_MODEL_ID).AsInteger;
     oItem.AntennaIsOnRoof         := FieldByName(FLD_AntennaOnRoof).AsBoolean;
     oItem.Refraction              := FieldByName(FLD_refraction).AsFloat;
   //  oItem.Level_Stored_Noise      := rec.Level_Stored_Noise;

     oItem.Zone6:=FZone6;

     oItem.XYRect.TopLeft.X        := Floor(FxyRectMax.TopLeft.X);
     oItem.XYRect.TopLeft.Y        := Floor(FxyRectMax.TopLeft.Y);
     oItem.XYRect.BottomRight.X    := Floor(FxyRectMax.BottomRight.X);
     oItem.XYRect.BottomRight.Y    := Floor(FxyRectMax.BottomRight.Y);

     oItem.BLRect                  := FBLRect;

     oItem.Abonent.Height          := FieldByName(FLD_TERMINAL_HEIGHT).AsFloat;
     oItem.Abonent.Power_W         := FieldByName(FLD_Terminal_Power_W).AsFloat;
     oItem.Abonent.Sensitivity     := rec.Abonent.Sensitivity;
    //   oItem.Abonent.Loss            := rec.Abonent.Loss
     oItem.Abonent.FeederLoss   := FieldByName(FLD_Terminal_Loss).AsFloat;
     oItem.Abonent.Gain         := FieldByName(FLD_TERMINAL_GAIN).AsFloat;

//    Assert (oItem.Abonent.Gain>0);


  end;

    DoCalcRegionVerifyParams();
(*

    SetLength (FParams.PolygonePoints, qry_Poly.RecordCount);

    with qry_Poly do
      for I := 0 to RecordCount - 1 do
      begin
        FParams.PolygonePoints[i].B:=FieldByName(FLD_LAT).AsFloat;
        FParams.PolygonePoints[i].L:=FieldByName(FLD_LON).AsFloat;
        Next;
      end;
*)



(*    if not Result then
      exit;*)

      vNode:=xml_AddNode (aParentNode, ATT_REGION,
          [
           xml_Par ('id',                rec.ID),
           xml_Par ('name',              rec.Name),
           xml_Par ('CalcDirection',     IIF(rec.CalcDirection=cdBS_to_PO, 'BS_to_PO', 'PO_to_BS')),

        //   xml_Par ('Level_Stored_Noise',rec.Level_Stored_Noise),
           xml_Par ('AntennaIsOnRoof',   rec.AntennaIsOnRoof),
           xml_Par ('Refraction',        rec.refraction),
           xml_Par ('CalcStep',          rec.CalcStep),
           xml_Par ('Zone',              rec.Zone6),

           xml_Par (ATT_CALCMODEL_ID,    rec.CalcModel_ID),
           xml_Par ('K0_open',           rec.K0_open ),
           xml_Par ('K0_closed',         rec.K0_closed ),
           xml_Par ('K0',                rec.k0),

           xml_Par ('TopLeft_x',         rec.xyRect.TopLeft.X),
           xml_Par ('TopLeft_y',         rec.xyRect.TopLeft.Y),
           xml_Par ('BottomRight_x',     rec.xyRect.BottomRight.X),
           xml_Par ('BottomRight_y',     rec.xyRect.BottomRight.Y),

           xml_Par ('TopLeft_b',         rec.blRect.TopLeft.B),
           xml_Par ('TopLeft_l',         rec.blRect.TopLeft.L),
           xml_Par ('BottomRight_b',     rec.blRect.BottomRight.B),
           xml_Par ('BottomRight_l',     rec.blRect.BottomRight.L)

          ]);


      xml_AddNode (vNode, ATT_ABONENT,
          [xml_Par (ATT_Height,   rec.Abonent.HEIGHT),
           xml_Par (ATT_POWER,    rec.Abonent.Power_W),
           xml_Par (ATT_SENSE,    rec.Abonent.Sensitivity),

////////////           xml_Par (ATT_LOSS,       rec.Abonent.LOSS - rec.Abonent.Gain),

           //old ver
           xml_Par (ATT_FEEDERLOSS, rec.Abonent.FeederLoss - rec.Abonent.Gain)
          ]);

      AddCluttersNode (rec.ClutterModel_ID, vNode);

  except
    on E: Exception do
      ShowMessage(E.Message);
  end;

end;

//----------------------------------------------------
procedure TdmCalcRegion_export_calc.AddCluttersNode(aID: integer; aParentNode:
    IXMLNode);
//----------------------------------------------------
var
  vNode: IXMLNode;
  sCluModelName: string;
begin
  vNode:=aParentNode.AddChild(ATT_CLUTTERS);

///  db_OpenQuery (qry_ClutterModel, TBL_CLUTTER_MODEL, FLD_ID, aID);

  dmOnega_DB_data.OpenQuery (qry_Clutters,
      'SELECT * FROM '+ VIEW_CLUTTERS + ' WHERE CLUTTER_MODEL_ID ='+ AsString(aID) );
//  qry_Clutters1.Filtered:= true;


 // qry_Clutters1.Filter:= (FLD_CLUTTER_MODEL_ID +'='+ AsString(aID));

  if qry_Clutters.IsEmpty then
  begin
    sCluModelName:= gl_DB.GetNameByID(TBL_CLUTTERMODEL, aID);
    AddError(Format('��� ������ ��������� "%s" �� ����� �� ���� ��������', [sCluModelName]));
  end;

  with qry_Clutters do
    while not EOF do
    begin
      if VarExists(FieldValues[FLD_TYPE]) then
        xml_AddNode (vNode, ATT_ITEM,
             [xml_Par (ATT_TYPE,   FieldValues[FLD_TYPE]),
              xml_Par (ATT_HEIGHT, FieldValues[FLD_HEIGHT]),
              xml_Par (ATT_LOSS,   FieldValues[FLD_LOSS])    ]);

      Next;
    end;
end;

//----------------------------------------------------
procedure TdmCalcRegion_export_calc.AddReliefNode(aParentNode: IXMLNode);
//----------------------------------------------------
var
  vNode: IXMLNode;
  i: integer;

  oStrList: TStringList;
  blRect: TBLRect;
 // iStep: integer;

begin
  oStrList:= TStringList.Create;

//  iPolyID:= dmCalcRegion.GetIntFieldValue (FCalcRegionID, FLD_POLYGON_ID);

//  blRect:=dmPolygons.GetBoundBLRect (iPolyID);

  blRect:=dmCalcRegion_Polygons.Get_BoundBLRect (FCalcRegionID);

  FRelBestZone:=geo_Get6ZoneBL(blRect.TopLeft);


  //dmRelMatrixFile.GetListInRect (blRect, FStrList);
  dmRel_Engine.Open_All;// GetListInRect (blRect, oStrList, FRelBestZone, iStep);

  dmRel_Engine.GetList(oStrList);


//  dmRelMatrixFile.GetListInRect (blRect, oStrList, FRelBestZone, iStep);

(*  if Params.ExcludeMatrixesIDList.Count>0 then
    for I := 0 to Params.ExcludeMatrixesIDList.Count-1 do
    begin
      iIndex := oStrList.IndexOf(Params.ExcludeMatrixesIDList[i].Name);
      if iIndex >= 0 then
        oStrList.Delete(iIndex);
    end;*)

  if (oStrList.Count=0) then
    AddError('�� ������ ������� �����, ���� �������� ������� �� ������������ �� ����������� � ������� �������');

  vNode:=aParentNode.AddChild(ATT_MATRIXES);
  for i:=0 to oStrList.Count-1 do
    if FileExists(oStrList[i]) then
    begin
      FCalcParams.ReliefMatrixFiles.Add(oStrList[i]);
      xml_AddNode (vNode, ATT_ITEM,  [xml_Par (FLD_FILENAME, oStrList[i]) ]);
    end;

  oStrList.Free;
end;


//-----------------------------------------------------------------------------------------
procedure TdmCalcRegion_export_calc.AddCellsNode(aParentNode: IXMLNode);
//-----------------------------------------------------------------------------------------
var
  { TODO : rename }
  FCellLayerIds: string;

  rec: TCellRec_;
  iCell_ID, I: Integer;
  vNode, vNode1, vNode2, vNode3: IXMLNode;
  s1, sCellAlias, s, sName, sSql, sCellIDs: string;      // , sCellLayerIds
  t, dRadius_KM, dRadiusCIA: double;

  oItem: TcmpCell;

begin
  vNode:= aParentNode.AddChild( 'CELLS');


  qry_Cells.First;
  with qry_Cells do
    while not EOF do
  begin
    iCell_ID:= qry_Cells[FLD_ID];

    sCellAlias:= Format('������-%s,id-%d; ��-%s,id-%d. ',
                [FieldByName(FLD_NAME).AsString,
                 iCell_ID,
                 FieldByName(FLD_PMP_SITE_NAME).AsString,
                 FieldByName(FLD_PMP_SITE_ID).AsInteger]);


    // -------------------------------------------------------------------

(*    rec.ID             := qry_Cells[FLD_ID];
    rec.NAME           := FieldByName(FLD_NAME).AsString;
    rec.COMBINER_LOSS  := 0;
    rec.CALC_MODEL_ID  := FieldByName(FLD_CALC_MODEL_ID).AsInteger;
 //   if CalcRegionTypeIsGSM then
      rec.CELL_LAYER_NAME:= FieldByName(FLD_CELL_LAYER_NAME).AsString;
    rec.SITE_ID        := FieldByName(FLD_SITE_ID).AsInteger;
    rec.SITE_NAME      := FieldByName(FLD_SITE_NAME).AsString;
     //  xml_Par (ATT_RADIUS_CIA,    0),
      // xml_Par (ATT_RADIUS_COV,    FieldValues[FLD_CALC_RADIUS]),
    rec.K0             :=FieldByName(FLD_K0).AsFloat;
    rec.K0_open        :=FieldByName(FLD_K0_OPEN).AsFloat;
    rec.K0_closed      :=FieldByName(FLD_K0_CLOSED).AsFloat;

    rec.CALC_RADIUS    := FieldByName(FLD_CALC_RADIUS).AsFloat;*)

    dRadius_KM:= qry_Cells.FieldByName(FLD_CALC_RADIUS_KM).AsFloat;

    /////////////////// !!!!!

(*
    //----------------------------------------------------
    if CalcRegionTypeIsGSM then
      if dRadius_KM = 0 then
      begin

        if qry_Site_radius.Locate(FLD_ID, qry_Cells[FLD_CELL_ID], []) then
        begin
          dRadius_KM:= qry_Site_radius.FieldByName(FLD_CALC_RADIUS).AsFloat;
          if dRadius_KM = 0 then
            dRadius_KM := 35;

          dRadiusCIA:= qry_Site_radius.FieldByName(FLD_CALC_RADIUS_NOISE).AsFloat;
          if dRadiusCIA = 0 then
            dRadiusCIA := 35;

          dRadius_KM:= Max(dRadius_KM, dRadiusCIA);
        end;

      end;
    //----------------------------------------------------

*)

(*    if Assigned(FindField(FLD_COMBINER_LOSS)) then
      rec.COMBINER_LOSS_dB:= FieldByName(FLD_COMBINER_LOSS).AsFloat
    else
*)
      rec.COMBINER_LOSS:= 0;

    rec.CALCMODEL_ID  := FieldByName(FLD_CALC_MODEL_ID).AsInteger;

    rec.kupFileName := FFileDir + Format('%d.kup', [FieldByName(FLD_ID).AsInteger]);

(*
    if CalcRegionTypeIsGSM then
    begin
      rec.TRX_Sensitivity_dBm :=FieldByName(FLD_SENSE).AsFloat;
      rec.TRX_FREQ_MHz :=FieldByName(FLD_FREQ).AsFloat;
      rec.TRX_POWER_W  := TruncFloat(FieldByName(FLD_POWER).AsFloat, 2);
    end  else
    begin
*)

      rec.TRX_Sensitivity:=FieldByName(FLD_THRESHOLD_BER_6).AsFloat;
      rec.TRX_FREQ_MHz :=FieldByName(FLD_TX_FREQ_MHz).AsFloat;
      rec.TRX_POWER_W  := TruncFloat(Power(10, 0.1*(FieldByName(FLD_POWER_dBm).AsFloat-30)));
  //  end;


    rec.CELL_LAYER_NAME:= qry_Cells.FieldByName(FLD_CellLayer_NAME).AsString;


    if (rec.TRX_FREQ_MHz<=0)     then AddError  (sCellAlias+'������� ����������������<=0');
    if (rec.TRX_POWER_W<=0)      then AddWarning(sCellAlias+'�������� ����������������<=0');
    if (rec.TRX_Sensitivity>=0) then AddWarning(sCellAlias+'���������������� ����������������>=0');

    if (rec.CELL_LAYER_NAME='') then AddError(sCellAlias+'�������� ���� �� ������');
    // -------------------------------------------------------------------


    rec.id       :=qry_Cells.FieldValues[FLD_ID];
    rec.Name     :=qry_Cells.FieldByName(FLD_NAME).AsString;
    rec.SITE_ID  :=qry_Cells.FieldByName(FLD_PMP_SITE_ID).AsInteger;
    rec.SITE_Name:=qry_Cells.FieldByName(FLD_PMP_SITE_NAME).AsString;

    rec.CALC_RADIUS_km  :=dRadius_KM;
    rec.Radius_signal_km:=35;

    rec.K0_OPEN  := qry_Cells.FieldByName(FLD_K0_OPEN).AsFloat;
    rec.K0_CLOSED:= qry_Cells.FieldByName(FLD_K0_CLOSED).AsFloat;
    rec.k0       := qry_Cells.FieldByName(FLD_K0).AsFloat;

    vNode1:=xml_AddNode (vNode, 'CELL',
          [xml_Par (ATT_ID,            rec.id),
           xml_Par (ATT_NAME,          rec.Name),
           xml_Par (ATT_COMBINER_LOSS, rec.COMBINER_LOSS),
           xml_Par (ATT_SITE_ID,       rec.SITE_ID),
           xml_Par (ATT_SITE_NAME,     rec.SITE_NAME),
           xml_Par (ATT_STANDART_NAME, rec.CELL_LAYER_NAME),
         //  xml_Par (ATT_RADIUS_CIA,    0),
          // xml_Par (ATT_RADIUS_COV,    FieldValues[FLD_CALC_RADIUS]),
           xml_Par (ATT_RADIUS,        rec.CALC_RADIUS_km),
           xml_Par ('Radius_signal',   rec.Radius_signal_km),

           xml_Par (ATT_CALCMODEL_ID,  rec.CALCMODEL_ID),
           xml_Par ('K0_open',         rec.K0_OPEN),
           xml_Par ('K0_closed',       rec.K0_CLOSED),
           xml_Par ('K0',              rec.k0 )
           ]);

    vNode3:=xml_AddNode (vNode1, 'kupFileName', [xml_Par (ATT_VALUE, rec.kupFileName)]);

    vNode2:=xml_AddNode (vNode1, ATT_TRX,
          [xml_Par (ATT_POWER,  rec.TRX_POWER_W),
           xml_Par (ATT_SENSE,  rec.TRX_Sensitivity),
           xml_Par (ATT_FREQ,   rec.TRX_FREQ_MHz)
           ]);


    oItem:=FCalcParams.Cells.AddItem;

    oItem.ID                  := rec.ID;
    oItem.Name                := rec.Name;
    oItem.SITE_ID             := rec.SITE_ID;
    oItem.Site_Name           := rec.SITE_NAME;

    oItem.TRX.Sensitivity := rec.TRX_Sensitivity;
    oItem.TRX.FREQ_MHz        := rec.TRX_FREQ_MHz;
    oItem.TRX.POWER_W         := rec.TRX_POWER_W;

    oItem.Combiner_Loss    := rec.COMBINER_LOSS;
    oItem.kupFileName         := rec.kupFileName;

    oItem.CALC_RADIUS_km      := rec.CALC_RADIUS_km;
    oItem.Radius_signal_km    := rec.Radius_signal_km;

    oItem.CALCMODEL_ID        := rec.CALCMODEL_ID;
    oItem.K0_OPEN             := rec.K0_OPEN;
    oItem.K0_CLOSED           := rec.K0_CLOSED;
    oItem.k0                  := rec.k0;

    oItem.Standard_Name       := rec.CELL_LAYER_NAME ;

    AddCellAntennasNode (qry_Cells[FLD_ID], vNode1, dRadius_KM, sCellAlias, oItem);

    Next;
  end;

end;


//----------------------------------------------------
procedure TdmCalcRegion_export_calc.AddCellAntennasNode(aCell_ID: integer;
    aParentNode: IXMLNode; aRadius_KM: double; aCellAlias: string; aCell:
    TcmpCell);
//----------------------------------------------------
var
  vNode: IXMLNode;
 // sField,
  sFileName: string;
  iID: integer;
  blPos: TBLPoint;
  xyPos: TXYPoint;

  rec: TCellAntennaRec_;

  oItem: TcmpAntenna;

    //------------
    function DoCellAntennasVerifyParams(): boolean;
    //------------
    begin

      if (rec.blPos.B<=0)     then AddError(aCellAlias+'������<=0');
      if (rec.blPos.L<=0)     then AddError(aCellAlias+'�������<=0');

      if (rec.AntennaType_ID<=0) then AddError(aCellAlias+'�� ����� ��� �������');

      if (rec.height<=0) then AddError(aCellAlias+'������<=0');

      if (rec.AZIMUTH<0) or
         (rec.AZIMUTH>360) then
         AddWarning(aCellAlias+'������<0 ��� ������>360');

      if (rec.gain<=0) then
        AddWarning(aCellAlias+'��������<=0');

    end;


    //-----------------------------
    procedure DoAdd();
    //-----------------------------
    begin
  //   db_View(qry_Antennas);


      with qry_Antennas do
    //        while not EOF do
      begin
        rec.ID     :=FieldByName(FLD_ID).AsInteger;

        rec.AntennaType_ID:=FieldByName(FLD_ANTENNATYPE_ID).AsInteger;

        rec.blPos.B:=FieldByName(FLD_PROPERTY_LAT).AsFloat;
        rec.blPos.L:=FieldByName(FLD_PROPERTY_LON).AsFloat;

        Assert(rec.blPos.B<>0, 'Value <=0');
        Assert(rec.blPos.L<>0, 'Value <=0');


      //  rec.blPos  :=db_ExtractBLPoint (qry_Antennas);
      //  rec.xyPos  :=geo_BL_to_XY (rec.blPos,  FZone);

        (*    rec.xyPos.X:=Round(rec.xyPos.X);
            rec.xyPos.Y:=Round(rec.xyPos.Y);*)

        rec.height :=FieldByName(FLD_HEIGHT).AsFloat;
        rec.AZIMUTH:=FieldByName(FLD_AZIMUTH).AsFloat;
        rec.TILT   :=FieldByName(FLD_TILT).AsFloat;

(*        if Assigned(FindField(FLD_HEEL)) then
          rec.Heel:= FieldByName(FLD_HEEL).AsFloat
        else
          rec.Heel:= 0;
*)

        rec.gain        :=FieldByName(FLD_GAIN).AsFloat;

        rec.plFileName     :=FFileDir + Format(ANTENNA_WS_FILENAME, [rec.ID]);
        rec.FeederLoss  :=FieldByName(FLD_LOSS).AsFloat;
        rec.ZoneNum        :=FZone6;

        rec.CalcRadius_KM  :=aRadius_KM;

        // ---------------------------------------------------------------
        oItem:=aCell.Antennas.AddItem;

        oItem.ID                 :=FieldByName(FLD_ID).AsInteger;
        oItem.AntennaType_ID     :=FieldByName(FLD_ANTENNATYPE_ID).AsInteger;

        oItem.blPos.B:=FieldByName(FLD_PROPERTY_LAT).AsFloat;
        oItem.blPos.L:=FieldByName(FLD_PROPERTY_LON).AsFloat;

      //  oItem.blPos              :=rec.blPos;
       // oItem.xyPos              :=rec.xyPos;
        oItem.Height             :=FieldByName(FLD_HEIGHT).AsFloat;
        oItem.AZIMUTH            :=FieldByName(FLD_AZIMUTH).AsFloat;
        oItem.TILT               :=FieldByName(FLD_TILT).AsFloat;
     //   oItem.Heel               :=rec.Heel;
     //   oItem.gain_dB            :=rec.gain_dB;
//        oItem.plFileName         :=rec.plFileName;
        oItem.plFileName         :=FFileDir + Format(ANTENNA_WS_FILENAME, [oItem.ID]);

        oItem.FeederLoss      :=FieldByName(FLD_LOSS).AsFloat;
        oItem.ZoneNum            :=FZone6;
        oItem.CalcRadius_KM      :=aRadius_KM;


        DoCellAntennasVerifyParams;


        vNode:=xml_AddNode (aParentNode, ATT_ANTENNA,
                [xml_Par ('Antenna_Type_ID',   rec.AntennaType_ID),
                 xml_Par (ATT_ID,              rec.ID),
                 xml_Par (ATT_FEEDERLOSS,      rec.FeederLoss),
                 xml_Par (ATT_HEIGHT,          rec.height),
                 xml_Par (ATT_AZIMUTH,         rec.AZIMUTH),
                 xml_Par (ATT_SLOPE,           rec.TILT),
             //    xml_Par (ATT_ROTATE,          rec.Heel),
                 xml_Par (ATT_GAIN,            rec.gain),

                 xml_Par ('B',                 rec.blPos.B),
                 xml_Par ('L',                 rec.blPos.L),
      //           xml_Par ('x',                 rec.xyPos.X),
      //           xml_Par ('y',                 rec.xyPos.Y),

                 xml_Par (ATT_ZONENUM,         rec.ZoneNum),
                 xml_Par (ATT_RADIUS,          rec.CalcRadius_KM)
             ]);

            xml_AddNode (vNode, 'plFileName', [xml_Par (ATT_VALUE, rec.plFileName)]);

    //        Next;
          end;
      end;

begin
(*  if CalcRegionTypeIsGSM then
    db_OpenQuery (qry_Cell_Antennas, VIEW_CELL_ANTENNAS, FLD_CELL_ID, aCellID)
  else
    db_OpenQuery (qry_Cell_Antennas, VIEW_PMP_SECTOR_ANTENNAS, FLD_PMP_SECTOR_ID, aCellID); *)

(*  if CalcRegionTypeIsGSM then
    qry_Cell_Antennas.Filter:= FLD_CELL_ID +'='+ AsString(aCellID)
  else
    qry_Cell_Antennas.Filter:= FLD_PMP_SECTOR_ID +'='+ AsString(aCellID);*)

  aCellAlias:= aCellAlias+'�������(�) �������. ';


//  qry_Antennas.

  qry_Antennas.Filter  := FLD_LINKEND_ID +'='+ IntToStr(aCell_ID);

//  sField:= IIF (CalcRegionTypeIsGSM, FLD_CELL_ID, FLD_PMP_SECTOR_ID);
//  sField:= FLD_PMP_SECTOR_ID;

//  if qry_Antennas.FieldByName(FLD_PMP_SECTOR_ID).AsInteger<>aCell_ID then
//    if not qry_Antennas.Locate(FLD_PMP_SECTOR_ID, aCell_ID, []) then
   //   exit;
(*      ErrorDlg(Format('� ������� %s, �� %s ��� �� ����� �������',
               [dmCell.GetNameByID(aCell_ID),
                dmSite.GetNameByID( dmCell.GetSiteID(aCell_ID) ) ]));*)

//  DoAdd();

  qry_Antennas.First;

  with qry_Antennas do
    while not EOF do
  begin
    DoAdd();

    Next;

  end;
end;


//----------------------------------------------------
procedure TdmCalcRegion_export_calc.CalcModel_SaveToXMLNode(aParentNode:
    IXMLNode);
//----------------------------------------------------
var
  vFolderNode,vNode: IXMLNode;
  oItem: TcmpCalcModelType;
  i: integer;

begin
  vFolderNode:=aParentNode.AddChild(ATT_MODELS);


  with sp_CalcModels do
    while not EOF do
  begin
    oItem:=FCalcParams.CalcModelTypes.AddItem;

    oItem.ID         :=FieldValues[FLD_ID];
    oItem.Name       :=FieldValues[FLD_NAME];
    oItem.ModelType  :=FieldValues[FLD_TYPE];


    vNode:=xml_AddNode (vFolderNode, TAG_ITEM,
          [xml_Par (ATT_NAME, FieldValues[FLD_NAME]),
           xml_Par (ATT_TYPE, FieldValues[FLD_TYPE]),
           xml_Par (ATT_ID,   FieldValues[FLD_ID])
           ]);

    db_OpenQuery (qry_CalcModel_Params,
                 'SELECT * FROM ' + TBL_CalcModelParams + ' WHERE calc_model_id=:id ORDER BY name',
                 [db_Par(FLD_ID, FieldValues[FLD_ID])] );

    SetLength(oItem.Params, qry_CalcModel_Params.RecordCount);

    with qry_CalcModel_Params do
      while not EOF do
      begin
        i := RecNo-1;

        oItem.Params[i].Name  := FieldValues[FLD_NAME];
        oItem.Params[i].Value := FieldValues[FLD_VALUE_];

        xml_AddNode (vNode, 'PARAM',
                    [xml_Par (ATT_NAME,  FieldValues[FLD_NAME]),
                     xml_Par (ATT_value, FieldValues[FLD_VALUE_])  ]);

        Next;
      end;

    Next;
  end;

end;


//----------------------------------------------------
procedure TdmCalcRegion_export_calc.AddAntennaTypesNode(aParentNode: IXMLNode);
//----------------------------------------------------
var
  vNode1,vNode,vNode_vert,vNode_hor: IXMLNode;
  sAntTypeAlias, s, sIds, sSql: string;
  fAngle, fLoss: double;
  i: Integer;
  HorzMaskArr, VertMaskArr: TdmAntTypeMaskArray;

  sVert_mask,sHorz_mask: string;

  oItem: TcmpAntennaType;
begin
  vNode:=aParentNode.AddChild(ATT_ANTENNA_TYPES);

{ TODO : dmAntType }

//  qry_AntType.First;

  with sp_AntType do
    while not EOF do
  begin

    sAntTypeAlias:= Format('��� �������-%s, id-%d. ',
        [FieldByName(FLD_NAME).AsString,
         FieldByName(FLD_ID).AsInteger]);


//    sHorz:= str_DeleteLastCharConst(FieldByName(FLD_HORZ_MASK).AsString);
 //   sVert:= str_DeleteLastCharConst(FieldByName(FLD_HORZ_MASK).AsString);

    dmAntType.Mask_StrToArr(FieldByName(FLD_HORZ_MASK).AsString, HorzMaskArr);
    dmAntType.Mask_StrToArr(FieldByName(FLD_VERT_MASK).AsString, VertMaskArr);


    i:=Length(HorzMaskArr);
    i:=Length(VertMaskArr);

    if (Length(HorzMaskArr)<5) then AddError(sAntTypeAlias+'���='+AsString(Length(HorzMaskArr)));
    if (Length(VertMaskArr)<5) then AddError(sAntTypeAlias+'���='+AsString(Length(HorzMaskArr)));


    sVert_mask := '';
    sHorz_mask := '';

    for I := 0 to High(HorzMaskArr) do
      sHorz_mask := sHorz_mask + Format('%s=%s;', [FloatToStr(HorzMaskArr[i].Angle), FloatToStr(HorzMaskArr[i].Loss)]);

    for I := 0 to High(VertMaskArr) do
      sVert_mask := sVert_mask + Format('%s=%s;', [FloatToStr(VertMaskArr[i].Angle), FloatToStr(VertMaskArr[i].Loss)]);


  //  str_DeleteLastChar(sHorz_mask);
 //   str_DeleteLastChar(sVert_mask);


    oItem:=FCalcParams.AntennaTypes.AddItem;

    oItem.ID     :=FieldByName(FLD_ID).AsInteger;
    oItem.Name   :=FieldByName(FLD_NAME).AsString;
    oItem.Gain:=FieldByName(FLD_GAIN).AsFloat;

    oItem.Horz_mask:=FieldByName(FLD_HORZ_MASK).AsString;
    oItem.Vert_mask:=FieldByName(FLD_VERT_MASK).AsString;

//   oItem.

    vNode1:=xml_AddNode (vNode, ATT_ITEM,
          [xml_Par (ATT_ID,   FieldValues[FLD_ID]),
           xml_Par (ATT_NAME, FieldValues[FLD_NAME]),
           xml_Par (ATT_GAIN, FieldValues[FLD_GAIN])
           ]);

    vNode_hor := xml_AddNode (vNode1, 'horz_losses', [xml_Par('value_ex', sHorz_mask)]);
    vNode_vert:= xml_AddNode (vNode1, 'vert_losses', [xml_Par('value_ex', sVert_mask)]);

    Next;
  end;

end;



procedure TdmCalcRegion_export_calc.AddWarning(aMsg: string);
begin
 // custom_DbErrorLog.AddWarning(aMsg);
end;

procedure TdmCalcRegion_export_calc.AddError(aMsg: string);
begin
//  custom_DbErrorLog.AddError(aMsg);
end;

// ---------------------------------------------------------------
procedure TdmCalcRegion_export_calc.OpenData;
// ---------------------------------------------------------------
begin
  db_OpenQueryByFieldValue (qry_CalcRegion, TBL_PMP_CALCREGION, FLD_ID, FCalcRegionID);

  dmMain.ProjectID:=qry_CalcRegion[FLD_Project_ID];


  dmOnega_DB_data.pmp_CalcRegion_Select_CalcModels(sp_CalcModels, FCalcRegionID);
  dmOnega_DB_data.pmp_CalcRegion_Select_AntennaTypes(sp_AntType, FCalcRegionID);


  dmOnega_DB_data.OpenQuery (qry_Poly,
        ' SELECT * FROM ' + VIEW_PMP_CalcRegion_Polygons +
        ' WHERE (pmp_calc_region_id = :pmp_calc_region_id)',
         [FLD_pmp_calc_region_id,  FCalcRegionID  ]);

  Assert (not qry_Poly.IsEmpty);


  dmOnega_DB_data.OpenQuery (qry_Cells,
      'SELECT * FROM '+ VIEW_PMP_CALCREGION_PMP_SECTORS +
      ' WHERE (pmp_calc_region_id=:id) and (checked=1)',
      [FLD_ID, FCalcRegionID ]);


  Assert (not qry_Cells.IsEmpty, 'not qry_Cells.IsEmpty');


  dmOnega_DB_data.OpenQuery (qry_Antennas,
      'SELECT * FROM '+ VIEW_PMP_CALCREGION_PMP_Antenna +
      ' WHERE (pmp_calc_region_id=:id) and (checked=1)',
      [FLD_ID, FCalcRegionID ]);

  Assert (not qry_Antennas.IsEmpty);

end;

//----------------------------------------------------
procedure TdmCalcRegion_export_calc.Execute_SaveToXMLFile(aCalcRegionID: integer;
    aFileName: string);
//----------------------------------------------------
var
  vRoot: IXMLNode;

  oXMLDoc: TXMLDoc;

  oCalcParamsSaver: TCalcParamsSaver;
begin

//  CursorHourGlass;

  FCalcRegionID:=aCalcRegionID;

  OpenData();


  FFileDir:=dmCalcRegion_pmp.GetFileDir (FCalcRegionID);

{//  qry_Clutters1.Filtered:= false;
  db_OpenQuery (qry_Clutters1, 'SELECT * FROM '+VIEW_CLUTTERS);
  qry_Clutters1.Filtered:= true;
}
  oXMLDoc := TXMLDoc.Create();

  vRoot:=oXMLDoc.DocumentElement;

//  case Params.CalcType of
////    ctCalc:             sValue:= 'points';
//    ctRegion:           sValue:= 'region';
//   // ctCalibrate,
//   // ctCalibrateRegion:  sValue:= 'caliber';
//  end;

  FCalcParams.IsRecalcWS := Params.IsRecalcWS;
//  FCalcParams.CalcType
  //FCalcParams.Ca IsRecalcWS := Params.IsRecalcWS;


  xml_AddNode (vRoot, 'PARAMS', [xml_Par('IsRecalcPathLoss',Params.IsRecalcWS),
                                 xml_Par('CalcType',        'region')      ]);

  AddReliefNode       (vRoot);
  AddCalcRegionNode   (vRoot);
  AddCellsNode        (vRoot);
//  AddCalcModelsNode   (vRoot);

  CalcModel_SaveToXMLNode (vRoot) ;
  
  AddAntennaTypesNode (vRoot);

(*  if custom_DbErrorLog.Count>0 then begin
    custom_DbErrorLog.Save;
    custom_DbErrorLog.Show;
  end;*)

  oXMLDoc.SaveToFile (aFileName);

  oXMLDoc.Free;

//  CursorDefault;


  oCalcParamsSaver := TCalcParamsSaver.Create;
//  oCalcParamsSaver.CalcParams_Ref :=FCalcParams;
//  oCalcParamsSaver.SaveToXML(aFileName);

  FreeAndNil( oCalcParamsSaver);


end;



end.


