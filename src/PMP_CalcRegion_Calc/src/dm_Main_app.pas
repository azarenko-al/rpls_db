unit dm_Main_app;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,IniFiles,

  d_Calc,

 // u_types,
  u_func,
  u_db,
  u_files,

  u_const,
  u_const_db,
//  u_reg,

  dm_CalcRegion,
///  dm_CustomErrorLog,
  u_custom_db_error_log,

  dm_Main
  ;

type
  TdmMain_app = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
  end;


var
  dmMain_app: TdmMain_app;

//===================================================================
implementation  {$R *.DFM}
//===================================================================



//--------------------------------------------------------------
procedure TdmMain_app.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
{const
  REGISTRY_ROOT  = 'Software\Onega\RPLS_DB\';
}
var
 // iProjectID,
  iID: integer;
 // bIsConverted: boolean;
  sIniFileName: string;
  oInifile: TInifile;
begin
//////////  CoInitialize(nil);
 {
  if ParamStr(1) <> '' then
    iProjectID:=AsInteger(ParamStr(1))
  else
    iProjectID:=0;
  }


  sIniFileName:=ParamStr(1);
  if not FileExists(sIniFileName) then
    sIniFileName:= CALCREGION_CALC_INI_FILE;

(*  if not FileExists(sIniFileName) then begin
    ShowMessage('���� ���������� �����������');
    Close;
  end;*)


  dmMain.Open;

  if FileExists(sIniFileName) then
  begin
    oInifile:=TIniFile.Create (sIniFileName);


    with oInifile do
    begin
      dmMain.ProjectID:= ReadInteger ('main', FLD_PROJECT_ID, 0);
      iID:= ReadInteger ('main', FLD_CALC_REGION_ID, 0);
//      Free;
    end;

    FreeAndNil(oInifile);

  end else


  begin
    iID:= AsInteger(ParamStr(1));

    if iID=0 then
      iID:=
  //   1492 // kuznechnoe on local
  //   1543 // ����� ������� 1, test_lina, server
  //   1455 // ����� ������� 001, bee_spb, server/onega
  //   1697 // ����� ������� 003, bee_spb, server/onega
  //   1963 // ����� ������� 1, ����_������, server/onega
  //   1976 // ����� ������� 010, !������������� �������, server/onega

  //   1987 // ����� ������� 1, ����_Lina, server/onega
  //   1996
  //   2011
  //   2034
  //   1453
  ///////////////   1976

  //   1451 // �����-��������� �� server/onega_bee
     1449 // RollOut2006 �� server/onega
  //   1452 // bee_spb on local
  //   1391 // �����-��������� �� server/onega_bee
      ;

    Assert (iID<>0);

    dmMain.ProjectID:= dmCalcRegion.GetIntFieldValue (iID, FLD_PROJECT_ID);
  end;

  custom_DbErrorLog:= TDBErrorLog.Create;
  custom_DbErrorLog.Init(TBL_CALCREGION, FLD_ERRORS_OCCURED, iID);
  custom_DbErrorLog.Clear;

 // gl_Reg:=TRegStore_Create1 (REGISTRY_ROOT);

 
//  bIsConverted:=Tdlg_TN_Property_And_Link_Import.ExecDlg (iProj_id);

 // ShowMessage(IIF(bIsConverted, '���-�� ���������������', '������ �� ���������������'));

  Tdlg_Calc.ExecDlg (iID);


 // dmCalcRegion_Calc.Execute (iID);


{
  if bIsConverted then
    ExitProcess(1)
  else
    ExitProcess(0);
 }
end;





end.
