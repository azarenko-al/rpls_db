unit dm_Data_Calc;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms,


 u_LinkFreqPlan_classes,
 u_const_db,

//  u_const_msg,
  u_LinkFreqPlan_const,

  dm_Main,

  u_Link_const,

  u_db,

  u_func,


  dm_Onega_DB_data


  ;

type
  TdmData_calcregion = class(TDataModule)
    ADOConnection1: TADOConnection;
  private
    { Private declarations }
  public

    class procedure Init;

    procedure OpenData;

  end;

var
  dmData_calcregion: TdmData_calcregion;

implementation

{$R *.dfm}


// ---------------------------------------------------------------
class procedure TdmData_calcregion.Init;
// ---------------------------------------------------------------
begin
  if not Assigned(dmData_calcregion) then
    dmData_calcregion := TdmData_calcregion.Create(Application);

end;


procedure TdmData_calcregion.OpenData;
begin

end;

end.
