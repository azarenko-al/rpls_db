unit u_custom_DB_error_log;

interface

uses Classes, SysUtils, Db, 

//  d_ShowText,

  

  
  u_db

  ;


type
  TDBErrorLog = class(TObject)
  private
    FErrorStrings: TStringList;
    FFileName: String;
    FCount, FWarningCount: Integer;

    FLogTable, FLogField: string;
    FLogID: Integer;
  public
    constructor Create;
    destructor  Destroy;

    property  Count: Integer read FCount write FCount;
    property  WarningCount: Integer read FWarningCount write FWarningCount;

(*    function  Add (aErrorMsg: string; aWithTime: boolean=true): boolean;
    function  SaveToFile (aFileName: string): boolean;
*)

    procedure Init (aTableName, aFieldName: string; aID: Integer);
    procedure Clear ();

    procedure Save ();
    procedure Load ();

    procedure AddText (const AText: String);
    procedure AddError (const AErrorText: String; aIncCount: boolean=true; aWithTime: boolean=true);
    procedure AddWarning (const AErrorText: String; aIncCount: boolean=true; aWithTime: boolean=true);

    function  Show (): boolean;
  end;

var
  custom_DbErrorLog: TDBErrorLog;


//=========================================================
implementation
//=========================================================

//---------------------------------------------------------
constructor TDBErrorLog.Create;
//---------------------------------------------------------
begin
  inherited;

  FErrorStrings:= TStringList.Create;
end;

//---------------------------------------------------------
destructor TDBErrorLog.Destroy;
//---------------------------------------------------------
begin
  FreeAndNil(FErrorStrings);

  inherited;
end;

//---------------------------------------------------------
procedure TDBErrorLog.Init(aTableName, aFieldName: string; aID: Integer);
//---------------------------------------------------------
begin
  FErrorStrings.Clear;

  FLogTable:= aTableName;
  FLogField:= aFieldName;
  FLogID:= aID;
end;

//---------------------------------------------------------
procedure TDBErrorLog.Clear();
//---------------------------------------------------------
begin
 // !!!!!!!!!!!!!!!!!!!1
  FErrorStrings.Clear;
  {
  gl_DB.ExecCommand(Format('UPDATE %s SET %s=NULL WHERE id=%d',
                                [FLogTable, FLogField, FLogID]));
                                }
end;

//---------------------------------------------------------
procedure TDBErrorLog.Save ();
//---------------------------------------------------------
var
  oStream: TMemoryStream;
  sSQL: string;
begin

{  oStream:= TMemoryStream.Create;

  try
    with gl_DB.ADOCommand do
    begin
      FErrorStrings.SaveToStream (oStream);
      sSQL:=Format('UPDATE %s SET %s=:%s WHERE id=:id',
          [FLogTable,FLogField,FLogField]);

      CommandText := sSQL;
      Parameters.ParamByName(FLD_ID).Value :=FLogID;
      Parameters.ParamByName(FLogField).LoadFromStream(oStream, ftMemo); // Blob);
      Execute;

      oStream.Free;
    end;
  except
    oStream.Free;
  end;
}

end;

//---------------------------------------------------------
procedure TDBErrorLog.Load ();
//---------------------------------------------------------
var
  oMemo: TMemoField;
  oStream: TStream;
begin
{
  with gl_DB do
  begin
    oStream:= TStream.Create;

    db_OpenQuery(Query, Format('SELECT %s FROM %s WHERE id=%d',
                                  [FLogTable, FLogField, FLogID]));
    if Query.IsEmpty then
      exit;

    oMemo:= TMemoField.Create(nil);
    oMemo:= TMemoField(Query.FieldByName(FLogField));

    oMemo.SaveToStream(oStream);
    FErrorStrings.LoadFromStream(oStream);

    oStream.Free;
  end;}

end;

//---------------------------------------------------------
procedure TDBErrorLog.AddError(const AErrorText: String; aIncCount: boolean; aWithTime: boolean);
//---------------------------------------------------------
var
  S: String;
begin
  if aIncCount then
    Inc(FCount);

  S:= IIF(aWithTime, DateTimeToStr(now)+' ', '');
  S:= S + '������: '+AErrorText;

  FErrorStrings.Add(S)
end;

//---------------------------------------------------------
procedure TDBErrorLog.AddText(const AText: String);
//---------------------------------------------------------
begin
  FErrorStrings.Add(AText);
end;

//---------------------------------------------------------
procedure TDBErrorLog.AddWarning(const AErrorText: String; aIncCount: boolean; aWithTime: boolean);
//---------------------------------------------------------
var
  S: String;
begin
  if aIncCount then
    Inc(FWarningCount);

  S:= IIF(aWithTime, DateTimeToStr(now)+' ', '');
  S:= S + '��������������: '+AErrorText;

  FErrorStrings.Add(S)
end;

//---------------------------------------------------------
function  TDBErrorLog.Show (): boolean;
//---------------------------------------------------------
begin
{
  Result:= Tdlg_ShowText.CreateSingleForm('��� ������', FErrorStrings,
                '�������� ���� ������', true,
                '���������� ������', '�������� ������');
}
end;


begin
  custom_DbErrorLog:=TDBErrorLog.Create;

end.





(*//---------------------------------------------------------
function TDBErrorLog.Add (aErrorMsg: string; aWithTime: boolean): boolean;
//---------------------------------------------------------
var
  S: string;
begin
  s:= IIF(aWithTime, DateTimeToStr(now)+' ', '');
  FErrorStrings.Add(s + aErrorMsg + CRLF);
  Result:= true;
end;

//---------------------------------------------------------
function TDBErrorLog.SaveToFile (aFileName: string): boolean;
//---------------------------------------------------------
begin
  FErrorStrings.SaveToFile(aFileName);
end;*)
