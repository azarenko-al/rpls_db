unit dm_CalcRegion_export_join;

interface
uses
  SysUtils, Classes, Db, ADODB, Forms, FileCtrl,

  u_Join_params,

  dm_Rel_Engine,

  u_Calc_Types,

  u_const_db,

  dm_Main,

  dm_CalcRegion_Polygons,
  dm_CalcRegion,
  dm_RelMatrixFile,

  u_classes,
  
  u_Geo,
  
  u_db

  ;


type
  TdmCalcRegion_export_join = class(TDataModule)
    qry_Region: TADOQuery;
    qry_Cells: TADOQuery;
    qry_Cell_Layers: TADOQuery;
    ds_Cell_Layers: TDataSource;
    ds_Cells: TDataSource;
    ds_Region: TDataSource;
    qry_Poly: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    FCalcRegionFileDir: string;
    FBLRect: TBLRect;
    FxyRectMax: TXYRect;

    FViewSQL: string;

    procedure OpenData;

  private
    FParams: TParams_Join;

    FCalcRegionID: integer;

    procedure AddOptions;

    procedure AddRegion;
    procedure AddCellLayers;
    procedure AddCells(aCellLayer: TCellLayer);

  public
    Params: record
      IsMakeNBestMatrix: boolean;
      NBestMatrixCount: integer;

      IsMakeHandOffMatrix: boolean;
      IsCalcStatistic: boolean;

      CellLayerIDList: TIDList;

      IsVerifyParams: boolean;

      CalcFileDir: string;

      MapTypes: TCalcMapTypes;
    end;

    destructor Destroy; override;

    procedure Execute_SaveToXMLFile(aCalcRegionID: integer; aFileName: string);
  end;

var
  FdmCalcRegion_export_join: TdmCalcRegion_export_join;

function dmCalcRegion_export_join: TdmCalcRegion_export_join;

//======================================================
implementation {$R *.DFM}
//======================================================

//--------------------------------------------------------------------
function dmCalcRegion_export_join: TdmCalcRegion_export_join;
//--------------------------------------------------------------------
begin
  if not Assigned(FdmCalcRegion_export_join) then
    FdmCalcRegion_export_join:=TdmCalcRegion_export_join.Create(Application);

  Result:= FdmCalcRegion_export_join;
end;

destructor TdmCalcRegion_export_join.Destroy;
begin
  FreeAndNil(FParams);
  inherited Destroy;
end;

//----------------------------------------------------
procedure TdmCalcRegion_export_join.DataModuleCreate(Sender: TObject);
//----------------------------------------------------
begin
  inherited;

  db_SetComponentADOConnection(Self, dmMain.AdoConnection);

///  Params.MapTypes:= [mtKmo, mtKup, mtKgr, mtHandover];
  FParams := TParams_Join.Create();
end;

// ---------------------------------------------------------------
procedure TdmCalcRegion_export_join.OpenData;
// ---------------------------------------------------------------
begin
  db_OpenQueryByFieldValue (qry_Region, TBL_PMP_CALCREGION, FLD_ID, FCalcRegionID);

  dmOnega_DB_data.OpenQuery (qry_Poly,
        ' SELECT * FROM ' + VIEW_PMP_CalcRegion_Polygons +
        ' WHERE (pmp_calc_region_id = :pmp_calc_region_id)',
         [FLD_pmp_calc_region_id,  FCalcRegionID  ]);

  dmOnega_DB_data.OpenQuery (qry_Cell_Layers,
        'SELECT * FROM '+ VIEW_PMP_CALCREGION_USED_CELL_Layers +
        ' WHERE (pmp_calc_region_id=:pmp_calc_region_id) ',
        [FLD_pmp_CALC_REGION_ID, FCalcRegionID ]);


  dmOnega_DB_data.OpenQuery (qry_Cells,
      'SELECT * FROM '+ VIEW_PMP_CALCREGION_PMP_SECTORS +
      ' WHERE (pmp_calc_region_id=:pmp_calc_region_id) and (checked=1)',
      [FLD_pmp_CALC_REGION_ID, FCalcRegionID ]);

 // db_ViewDataSet(qry_Cells);

  qry_Cells.Filtered:= True;

end;


//----------------------------------------------------
procedure TdmCalcRegion_export_join.AddRegion;
//----------------------------------------------------
var
   I: integer;
   iZone, iRelBestZone, iCalcStep: integer; //, iPolygonID

   {
  // -------------------------------------------------------------------
  function DoGetZone: integer;
  // -------------------------------------------------------------------
  var
 //   oStrList: TStringList;
    blRect: TBLRect;
    iMinStep: integer;
  begin
//    oStrList:=TStringList.Create;
//    dmRelMatrixFile.GetListInRect (FblRect, oStrList, Result, iMinStep);
    dmRel_Engine.Open;// GetListInRect (FblRect, oStrList, Result, iMinStep);

 //   oStrList.Free;
  end;

  }

begin
  iCalcStep := qry_Region.FieldByName(FLD_STEP).AsInteger;

  FBLRect   :=dmCalcRegion_Polygons.Get_BoundBLRect(FCalcRegionID);

//  Assert (Params.CalcFileDir <> '');

  FCalcRegionFileDir:= dmCalcRegion_pmp.GetFileDir (FCalcRegionID, Params.CalcFileDir);

  if Params.IsVerifyParams then
    if not DirectoryExists(FCalcRegionFileDir) then
      ; //custom_DbErrorLog.AddError('����������� ��������� ���������� (��� �������� ������ ���� ���� ����� ��������(�������) ��� �������)');


  iRelBestZone:= geo_Get6ZoneBL(FBLRect.TopLeft);

//  iRelBestZone:=DoGetZone;

  assert (iRelBestZone < 1000);


  if iRelBestZone>0 then
    iZone:=iRelBestZone
  else
    iZone:=geo_Get6ZoneL(FBLRect.TopLeft.L);

  FxyRectMax:=geo_GetRoundXYRect (FBLRect, iZone);

  with qry_Region do
  begin
    with FParams do
    begin
      CalcRegion.ID  :=qry_Region[FLD_ID];
      CalcRegion.Name:=qry_Region[FLD_NAME];

      CalcRegion.Zone6:=iZone;
      CalcRegion.CalcStep:=iCalcStep;

      CalcRegion.Abonent_Sensitivity_dBm:=Round(FieldByName(FLD_Terminal_Sensitivity).AsFloat); // -100;

 //     if (CalcRegion.Abonent_Sensitivity_dBm >=0) then
   //     custom_DbErrorLog.AddWarning('���������������� ������������ ���������>=0');

///     rec.CalcDirection  :=IIF(FieldValues[FLD_DIRECTION]=0, cdBS_to_PO, cdPO_to_BS);

      CalcRegion.TopLeft.x:=     Round(FxyRectMax.TopLeft.X);
      CalcRegion.TopLeft.y:=     Round(FxyRectMax.TopLeft.y);
      CalcRegion.BottomRight.x:= Round(FxyRectMax.BottomRight.X);
      CalcRegion.BottomRight.y:= Round(FxyRectMax.BottomRight.Y);

      if Assigned(FindField(FLD_HYSTERESIS)) then
        CalcRegion.Hysteresis:=   FieldByName(FLD_HYSTERESIS).AsInteger;

      CalcRegion.RowCount:=       Round((CalcRegion.TopLeft.x-CalcRegion.BottomRight.x)/iCalcStep);
      CalcRegion.ColCount:=       Round((CalcRegion.BottomRight.y-CalcRegion.TopLeft.y)/iCalcStep);

      CalcRegion.CalcResultsDir:= FCalcRegionFileDir;

    end;



    SetLength (FParams.PolygonePoints, qry_Poly.RecordCount);

    with qry_Poly do
      for I := 0 to RecordCount - 1 do
      begin
        FParams.PolygonePoints[i].B:=FieldByName(FLD_LAT).AsFloat;
        FParams.PolygonePoints[i].L:=FieldByName(FLD_LON).AsFloat;
        Next;
      end;

    AddCellLayers ();

    Next;
  end;
end;


//-----------------------------------------------------------------------------------------
procedure TdmCalcRegion_export_join.AddCellLayers;
//-----------------------------------------------------------------------------------------
var
  sFileDir: string;

  oCellLayer: TCellLayer;

  i : integer;
  iID: integer;
begin

  with qry_Cell_Layers do
    while not EOF do
  begin
    iID  :=FieldValues[FLD_ID];
    sFileDir:=FCalcRegionFileDir + Format('cell_layer.%d\', [iID]);

    oCellLayer:=FParams.CellLayers.AddItem;

  //---------------------------------------------------------
  //  FillChar(FParams.CellLayer, SizeOf(FParams.CellLayer), 0);

    oCellLayer.ID:=iID;

    oCellLayer.kupFileName:=sFileDir + 'region.kup';
//      if mtKgr in Params.MapTypes then
    oCellLayer.kgrFileName:=sFileDir + 'region.kgr';
//      if mtKmo in Params.MapTypes then
    oCellLayer.kmoFileName:=sFileDir + 'region.kmo';

    if Params.IsMakeNBestMatrix then
      oCellLayer.NBestFileName:=sFileDir + 'region.nbest';


(*    if mtHandover in Params.MapTypes then
      oCellLayer.hoffFileName:=sFileDir + 'region.hoff';

    if Params.IsCalcStatistic then
      oCellLayer.SummaryFileName:=sFileDir + 'region.summary';
*)


    //---------------------------------------------------------

     AddCells (oCellLayer);

  //  DoAddCellLayers(vGroup, qry_Cell_Layers.FieldByName(FLD_ID).AsInteger);
//    DoAddCellLayers(vGroup, qry_Cell_Layers.FieldByName(FLD_CELL_LAYER_ID).AsInteger);

    Next;
  end;

 // end;

end;


//-----------------------------------------------------------------------------------------
procedure TdmCalcRegion_export_join.AddCells(aCellLayer: TCellLayer);
//-----------------------------------------------------------------------------------------
var
 // vCells: IXMLNode;
  s, sKupFileName: string;
  ind,iID: integer;
  dRadius_km, dHysteresis, dLat, dLon: double;
  oCell: TCellItem;

begin
  qry_Cells.Filter:= Format('cell_layer_id=%d', [aCellLayer.ID]);
  qry_Cells.First;

  with qry_Cells do
    while not EOF do
  begin
   // dHysteresis:= qry_Cells.FieldByName(FLD_HYSTERESIS).AsInteger;

    iID:= FieldValues[FLD_ID];

    dRadius_km:= FieldByName(FLD_CALC_RADIUS_KM).AsFloat;
    if dRadius_km = 0 then
      dRadius_km:= qry_Cells.FieldByName('site_calc_radius_km').AsFloat;

    oCell:=aCellLayer.Cells.AddItem;

    oCell.ID:=iID;

    oCell.Name       :=FieldByName(FLD_NAME).AsString;
    oCell.SiteName   :=FieldByName(FLD_PMP_SITE_NAME).AsString;
    oCell.sector_name:=FieldByName(FLD_NAME).AsString;

    oCell.B:=FieldByName(FLD_LAT).AsFloat;
    oCell.L:=FieldByName(FLD_LON).AsFloat;

    oCell.CalcRadius_km:=dRadius_km;

    oCell.KupFileName:=FCalcRegionFileDir + Format('%d.kup', [iID]);

    Next;
  end;
end;


//----------------------------------------------------
procedure TdmCalcRegion_export_join.AddOptions;
//----------------------------------------------------
begin
  FParams.Options.IsMakeNBestMatrix  :=Params.IsMakeNBestMatrix;
  FParams.Options.NBestMatrixRecSize :=Params.NBestMatrixCount;
  FParams.Options.IsMakeHandOffMatrix:=Params.IsMakeHandOffMatrix;
end;


//----------------------------------------------------
procedure TdmCalcRegion_export_join.Execute_SaveToXMLFile(aCalcRegionID: integer;
    aFileName: string);
//----------------------------------------------------

begin
  FCalcRegionID:=aCalcRegionID;

  OpenData();

  AddOptions ();
  AddRegion ();

  FParams.SaveToXMLFile(aFileName);

end;


end.

{

<?xml version="1.0" encoding="windows-1251"?>
<?xml:stylesheet type="text/xsl" href=""?>
<Document>
  <Options>
    <IsMakeNBestMatrix value="0"/>
    <NBestMatrixRecSize value="6"/>
    <IsMakeHandOffMatrix value="0"/>
  </Options>
  <REGIONS>
    <REGION name="����� �������1" id="1" Zone="7"
         CalcDirection="BS_to_PO"
         RowCount="1928" ColCount="2241"
         CalcStep="300" Abonent_Sensitivity="-98"
         x1="7280808" y1="7317239" x2="6702386" y2="7989418">

      <DUAL>
        <dual_C1 value="0"/>
        <dual_A value="10"/>
        <kupFileName value="U:\ONEGA\PROJECTS\ARHANGELSKAYA_OBL_BEELINE\ARHANGELSKAYA_OBL_BEELINE\SCENARIO.1\REGION.1\300\STANDARD.0\region.kup"/>
        <kmoFileName value="U:\ONEGA\PROJECTS\ARHANGELSKAYA_OBL_BEELINE\ARHANGELSKAYA_OBL_BEELINE\SCENARIO.1\REGION.1\300\STANDARD.0\region.kmo"/>
        <kgrFileName value="U:\ONEGA\PROJECTS\ARHANGELSKAYA_OBL_BEELINE\ARHANGELSKAYA_OBL_BEELINE\SCENARIO.1\REGION.1\300\STANDARD.0\region.kgr"/>
      </DUAL>

      <Standards>
        <Standard id="1">
          <kupFileName value="U:\ONEGA\PROJECTS\ARHANGELSKAYA_OBL_BEELINE\ARHANGELSKAYA_OBL_BEELINE\SCENARIO.1\REGION.1\300\STANDARD.1\region.kup"/>
          <kmoFileName value="U:\ONEGA\PROJECTS\ARHANGELSKAYA_OBL_BEELINE\ARHANGELSKAYA_OBL_BEELINE\SCENARIO.1\REGION.1\300\STANDARD.1\region.kmo"/>
          <kgrFileName value="U:\ONEGA\PROJECTS\ARHANGELSKAYA_OBL_BEELINE\ARHANGELSKAYA_OBL_BEELINE\SCENARIO.1\REGION.1\300\STANDARD.1\region.kgr"/>
          <NBestFileName value="U:\ONEGA\PROJECTS\ARHANGELSKAYA_OBL_BEELINE\ARHANGELSKAYA_OBL_BEELINE\SCENARIO.1\REGION.1\300\STANDARD.1\region.nbest"/>
          <hoffFileName value="U:\ONEGA\PROJECTS\ARHANGELSKAYA_OBL_BEELINE\ARHANGELSKAYA_OBL_BEELINE\SCENARIO.1\REGION.1\300\STANDARD.1\region.hoff"/>
          <SummaryFileName value="U:\ONEGA\PROJECTS\ARHANGELSKAYA_OBL_BEELINE\ARHANGELSKAYA_OBL_BEELINE\SCENARIO.1\REGION.1\300\STANDARD.1\region.summary"/>
          <CELLS>
            <CELL id="5115" name="������: 12501, ��-����������:3" site_name="12501, ��-����������" B="64,549441" L="40,528055" KupFileName="U:\ONEGA\PROJECTS\ARHANGELSKAYA_OBL_BEELINE\ARHANGELSKAYA_OBL_BEELINE\SCENARIO.1\REGION.1\300\5115.kup"/>
            <CELL id="5114" name="������: 12501, ��-����������:2" site_name="12501, ��-����������" B="64,549441" L="40,528055" KupFileName="U:\ONEGA\PROJECTS\ARHANGELSKAYA_OBL_BEELINE\ARHANGELSKAYA_OBL_BEELINE\SCENARIO.1\REGION.1\300\5114.kup"/>
            <CELL id="5113" name="������: 12501, ��-����������:1" site_name="12501, ��-����������" B="64,549441" L="40,528055" KupFileName="U:\ONEGA\PROJECTS\ARHANGELSKAYA_OBL_BEELINE\ARHANGELSKAYA_OBL_BEELINE\SCENARIO.1\REGION.1\300\5113.kup"/>
          </CELLS>
        </Standard>
        <Standard id="157">
          <kupFileName value="U:\ONEGA\PROJECTS\ARHANGELSKAYA_OBL_BEELINE\ARHANGELSKAYA_OBL_BEELINE\SCENARIO.1\REGION.1\300\STANDARD.157\region.kup"/>
          <kmoFileName value="U:\ONEGA\PROJECTS\ARHANGELSKAYA_OBL_BEELINE\ARHANGELSKAYA_OBL_BEELINE\SCENARIO.1\REGION.1\300\STANDARD.157\region.kmo"/>
          <kgrFileName value="U:\ONEGA\PROJECTS\ARHANGELSKAYA_OBL_BEELINE\ARHANGELSKAYA_OBL_BEELINE\SCENARIO.1\REGION.1\300\STANDARD.157\region.kgr"/>
          <NBestFileName value="U:\ONEGA\PROJECTS\ARHANGELSKAYA_OBL_BEELINE\ARHANGELSKAYA_OBL_BEELINE\SCENARIO.1\REGION.1\300\STANDARD.157\region.nbest"/>
          <hoffFileName value="U:\ONEGA\PROJECTS\ARHANGELSKAYA_OBL_BEELINE\ARHANGELSKAYA_OBL_BEELINE\SCENARIO.1\REGION.1\300\STANDARD.157\region.hoff"/>
          <SummaryFileName value="U:\ONEGA\PROJECTS\ARHANGELSKAYA_OBL_BEELINE\ARHANGELSKAYA_OBL_BEELINE\SCENARIO.1\REGION.1\300\STANDARD.157\region.summary"/>
        </Standard>
      </Standards>
    </REGION>
  </REGIONS>
</Document>


*)


