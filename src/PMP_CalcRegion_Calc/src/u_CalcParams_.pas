unit u_CalcParams_;

interface
uses Classes,SysUtils,IniFiles,

   u_crc32,

   u_Geo
   ;

type
  TCRC_rec =  record
                 CRC_CalcRegion  : Longword;
                 CRC_AntennaType : Longword;
                 CRC_Antenna     : Longword;
                 CRC_Cell        : Longword;
                 CRC_CalcModel   : Longword;
//                 CRC_Relief   : Longword;
               end;


type
    TcmpAntenna         = class;
    TcmpAntennaList     = class;
    TcmpAntennaType     = class;
    TcmpAntennaTypeList = class;
    TcmpCalcModelType   = class;
    TcmpCalcModelTypeList   = class;
    TcmpCell            = class;
    TcmpCellList        = class;
//    TcmpAntennaLinkList = class;

    TCalcRegion          = class;
    TCalcParams          = class;


    TAntennaMask = array of record  // ��������� �������������� �������
                       Angle: double;
                       Loss: double; // ��������� �������������� �������
                   end;


    //----------------------------------------------------
    // ��������� ��� ���������� ������
    //----------------------------------------------------
    TCalcParams = class
    //----------------------------------------------------
    private
      FErrorList: TStringList;
    public

//      IsUseSavedPathLoss : boolean; //������ WS ������

      IsRecalcWS:   boolean;

      // -------------------------
      CalcRegion    : TCalcRegion;
      Cells         : TcmpCellList;

      // -------------------------
      CalcModelTypes: TcmpCalcModelTypeList;
      AntennaTypes  : TcmpAntennaTypeList;

      ReliefMatrixFiles: TStringList;

      //runtime

      // ������ ������ ��� �����
     // CalcType: TCalcType;

      constructor Create;
      destructor Destroy; override;

      procedure Clear;
      procedure Validate;

      function AnalyzeCRC32: Integer;
      procedure SaveCRC32;

    end;

    //-----------------------------------------------------
    TcmpReliefMatrix = class(TCollectionItem)
    //-----------------------------------------------------
    public
      FileName : string;
      Zone6 : Integer;
    end;

    //-----------------------------------------------------
    TcmpAntenna = class(TCollectionItem)
    //-----------------------------------------------------
    public
      ID          : integer;

      AntennaType_ID : integer;

      Height        : double; // ���o�� ����� �������
      FeederLoss : double;
      Azimuth       : double; // ������ � ����
      Tilt          : double; // ������ ���������� � ���� //Slope_Tilt

      blPos         : TBLPoint;


 //     Rotate        : double; // ���� ���������� � ����
//      Heel          : Double;

     // AntennaType    : TcmpAntennaType;

      plFileName     : string;
      ZoneNum        : integer;

    //  X,Y            : integer;

     // xyPos            : TXYPoint;

   //   gain_dB : Double;
      CalcRadius_KM : Double;

      function GetCRC32: Longword;
      procedure Validate(aErrorList: Tstrings);
    end;

    //-----------------------------------------------------
    // Calc Model Types
    //-----------------------------------------------------

    //-----------------------------------------------------
    TcmpClutterItem = class(TCollectionItem)
    //-----------------------------------------------------
    public
//    Clutters:  array [TcalcClutterType] of record
      Name : string;
      Type_ : integer;
      Height : integer; // ������ �� ���������
      Loss_dBm   : double;  // �������� ���������
      Reflection_coef : double; //����. ���������
    end;

    //-----------------------------------------------------
    TcmpClutterItemList = class (TCollection)
    //-----------------------------------------------------
    private
      function GetItem(Index: integer): TcmpClutterItem;
    public
      constructor Create;
//      procedure Clear; override;
      function AddItem: TcmpClutterItem;
      procedure LoadDefaults;

//      function FindByID (aID: integer): TcmpAntennaType;
      property Items   [Index: integer]: TcmpClutterItem read GetItem; default;
    end;



    //-----------------------------------------------------
    TcmpAntennaType = class(TCollectionItem)
    //-----------------------------------------------------
    public
      ID        : integer;
      Name      : string;
      Gain    : double;  // ��������

      Horz_mask : string;
      Vert_mask : string;

      Losses_Hor,
      Losses_Ver  : TAntennaMask; // ��������� �������������� �������

      function GetCRC32: Longword;
    end;

    //-----------------------------------------------------
    TcmpCalcModelType = class(TCollectionItem)
    //-----------------------------------------------------
    public
      ID        : integer;
      Name      : string;  // ��������
//      ModelType : TcalcModelType;
      ModelType : Integer;

      Params  : array of record
                  Name: string;
                  Value: double;
                end;

       function GetCRC32: Longword;
     end;


    //-----------------------------------------------------
    TcmpCell = class(TCollectionItem)
    //-----------------------------------------------------
    public
      ID            : integer;
      Name          : string;

      SITE_ID       : Integer;
      Site_Name     : string;

      CELLID        : string;

      Standard_Name : string;

      CALC_RADIUS_km   : double; //������ ������� �������
      Radius_signal_km: Double;

      Combiner_Loss  : double; // ������ � ����������
      kupFileName    : string; // ��� ����� � ��������� (KUP ��� KMO)

      // ���������������� �������
      TRX : record
             Power_W         : double;  // �������� �����������, ��
             Sensitivity : double;  // ����������������//���������������� ���������, �����
             Freq_MHz        : double;  // ������� �������, ���
            end;

      CalcModel_ID   : integer;

      K0_open: Double;
      K0_closed: Double;
      k0: double;

      Antennas : TcmpAntennaList; // ������� ����������

      //for HASH ini file
      EnabledToExport: Boolean;

      //AnalyzeCRC32
      CRC_rec: TCRC_rec;

(*      CRC_Arr: array[0..1] of record
                     CRC_CalcRegion  : Longword;
                     CRC_AntennaType : Longword;
                     CRC_Antenna     : Longword;
                     CRC_Cell        : Longword;
                   end;
*)

      constructor Create(Collection: TCollection); override;
      destructor Destroy; override;

      function GetCRC32: Longword;

      procedure Validate(aErrorList: TStrings);
    end;

    //-----------------------------------------------------
    TCalcRegion = class
    //-----------------------------------------------------
    public
      ID : Integer;

      Name  : string;

   //   PercentError    : integer;
      CalcDirection   : (cdBS_to_PO, cdPO_to_BS); //������ ����� (��-��)
      AntennaIsOnRoof : boolean; // true - �� �� �����  false - �� �� �����
      Refraction      : double;
      CalcStep        : integer;

 //     Level_Stored_Noise: Integer;

      BLRect: TBLRect;
      XYRect          : TXYRect;
      Zone6           : integer;

      ClutterModel_ID: integer;

      CalcModel_ID: integer;
      K0_open,K0_closed,K0: double;

      Abonent: record
                 Height      : double;
                 Power_W     : Double; //1;
                 Sensitivity : double; //-95;
                 FeederLoss  : double;
                 Gain        : double;
               end;

      Clutters: TcmpClutterItemList;

      constructor Create;
      destructor Destroy; override;

      function GetCRC32: Longword;

      function Validate(aErrorList: Tstrings): boolean;
    end;

    //-----------------------------------------------------
    TcmpAntennaList = class (TCollection)
    //-----------------------------------------------------
    private
      function GetItem(Index: integer): TcmpAntenna;
    public
      constructor Create;
      function AddItem: TcmpAntenna;
      property Items [Index: integer]: TcmpAntenna read GetItem; default;
    end;



    //-----------------------------------------------------
    TcmpReliefMatrixList = class (TCollection)
    //-----------------------------------------------------
    private
      function GetItem(Index: integer): TcmpReliefMatrix;
    public
      constructor Create;
      function AddItem: TcmpReliefMatrix;

      function GetCRC32: Longword;

      property Items   [Index: integer]: TcmpReliefMatrix read GetItem; default;
    end;

    //-----------------------------------------------------
    TcmpAntennaTypeList = class (TCollection)
    //-----------------------------------------------------
    private
      function GetItem(Index: integer): TcmpAntennaType;
    public
      constructor Create;
      function AddItem: TcmpAntennaType;
      function FindByID(aID: integer): TcmpAntennaType;
      property Items   [Index: integer]: TcmpAntennaType read GetItem; default;
    end;

    //-----------------------------------------------------
    TcmpCalcModelTypeList = class (TCollection)
    //-----------------------------------------------------
    private
      function  GetItem (Index: integer): TcmpCalcModelType;
    public
      constructor Create;
      function AddItem: TcmpCalcModelType;
      function FindByID(aID: integer): TcmpCalcModelType;
      property  Items   [Index: integer]: TcmpCalcModelType read GetItem; default;
    end;

    //-----------------------------------------------------
    TcmpCellList = class (TCollection)
    //-----------------------------------------------------
    private
      function GetItem(Index: integer): TcmpCell;
    public
      constructor Create;
      function AddItem: TcmpCell;
      function FindByID(aID: integer): TcmpCell;

      property Items   [Index: integer]: TcmpCell read GetItem; default;
    end;


//=================================================
implementation




function TcmpCalcModelTypeList.GetItem (Index: integer): TcmpCalcModelType;
begin
  Result:=TcmpCalcModelType(inherited Items[Index]);
end;



function TcmpAntennaList.GetItem(Index: integer): TcmpAntenna;
begin
  Result:=TcmpAntenna(inherited Items[Index]);
end;



constructor TcmpAntennaTypeList.Create;
begin
  inherited Create(TcmpAntennaType);
end;

function TcmpAntennaTypeList.AddItem: TcmpAntennaType;
begin
  Result := TcmpAntennaType(Add);
end;

function TcmpAntennaTypeList.FindByID(aID: integer): TcmpAntennaType;
var
  I: Integer;
begin
  Result := nil;

  for I := 0 to Count - 1 do
    if Items[i].ID= aID then
    begin
      result := Items[i];
      exit;
    end;
end;

function TcmpAntennaTypeList.GetItem(Index: integer): TcmpAntennaType;
begin
  Result:=TcmpAntennaType(inherited Items[Index]);
end;


constructor TcmpCell.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  Antennas:=TcmpAntennaList.Create;

  EnabledToExport := True;
end;


destructor TcmpCell.Destroy;
begin
  Antennas.Free;
  inherited;
end;

// ---------------------------------------------------------------
function TcmpCell.GetCRC32: LongWord;
// ---------------------------------------------------------------
var
  s: string;
begin

  s := Format('%1.6f', [TRX.Power_W]) +
       Format('%1.6f', [TRX.Sensitivity]) +
       Format('%1.6f', [TRX.Freq_MHz])
       ;

  Result := GetHashCRC32_AsLongWord(s);

end;

// ---------------------------------------------------------------
procedure TcmpCell.Validate(aErrorList: TStrings);
// ---------------------------------------------------------------
var
  sCellAlias: string;
begin
  if (TRX.Freq_MHz<=0) then
    aErrorList.Add (sCellAlias+'������� ����������������<=0');

  if (TRX.POWER_W<=0)  then
    aErrorList.Add (sCellAlias+'�������� ����������������<=0');

  if (TRX.Sensitivity>=0) then
    aErrorList.Add (sCellAlias+'���������������� ����������������>=0');

//   rec.CELL_LAYER_NAME:= qry_Cells.FieldByName(FLD_CELL_LAYER_NAME).AsString;

//  if (CEL L _LAYER_NAME='') then aErrorList.Add (sCellAlias+'�������� ���� �� ������');
  // -------------------------------------------------------------------

end;


constructor TcmpAntennaList.Create;
begin
  inherited Create(TcmpAntenna);
end;

function TcmpAntennaList.AddItem: TcmpAntenna;
begin
  Result := TcmpAntenna(Add);
end;


constructor TcmpCellList.Create;
begin
  inherited Create(TcmpCell);
end;

function TcmpCellList.AddItem: TcmpCell;
begin
  Result := TcmpCell(Add);
end;

function TcmpCellList.FindByID(aID: integer): TcmpCell;
var
  I: Integer;
begin
  Result := nil;

  for I := 0 to Count - 1 do
    if Items[i].ID= aID then
    begin
      result := Items[i];
      exit;
    end;
end;


function TcmpCellList.GetItem(Index: integer): TcmpCell;
begin
  Result:=TcmpCell(inherited Items[Index]);
end;


constructor TcmpCalcModelTypeList.Create;
begin
  inherited Create (TcmpCalcModelType);
end;

function TcmpCalcModelTypeList.AddItem: TcmpCalcModelType;
begin
  Result := TcmpCalcModelType(Add);
end;


function TcmpCalcModelTypeList.FindByID(aID: integer): TcmpCalcModelType;
var
  I: Integer;
begin
  Result := nil;

  for I := 0 to Count - 1 do
    if Items[i].ID= aID then
    begin
      result := Items[i];
      exit;
    end;
end;


//-------------------------------------------------
// TcalcModelParams
//-------------------------------------------------
constructor TCalcParams.Create;
begin
  inherited;
  CalcRegion:=TCalcRegion.Create;

  Cells:=TcmpCellList.Create;
  ReliefMatrixFiles:=TStringList.Create;
  CalcModelTypes:=TcmpCalcModelTypeList.Create;
  AntennaTypes:=TcmpAntennaTypeList.Create;

  //AntennaLinks:=TcmpAntennaLinkList.Create;

  FErrorList := TStringList.Create();
end;

// ---------------------------------------------------------------
destructor TCalcParams.Destroy;
// ---------------------------------------------------------------
begin
  FreeAndNil(FErrorList);
 // AntennaLinks.Free;

  FreeAndNil(Cells);
  FreeAndNil(ReliefMatrixFiles);
  FreeAndNil(CalcModelTypes);
  FreeAndNil(AntennaTypes);

  FreeAndNil(CalcRegion);

  inherited;
end;


procedure TCalcParams.Clear;
begin
  Cells.Clear;
  ReliefMatrixFiles.Clear;
  CalcModelTypes.Clear;
  AntennaTypes.Clear;
end;


procedure TCalcParams.Validate;
begin
  FErrorList.Clear;

end;


procedure TcmpClutterItemList.LoadDefaults;
var
  oItem: TcmpClutterItem;
begin
  Clear;

  oItem :=AddItem;
  oItem.Name :='Forest';   oItem.Height :=20; oItem.Loss_dBm :=0.06;  oItem.Reflection_coef := 0.5;

  oItem :=AddItem;
  oItem.Name :='Country';  oItem.Height :=7;  oItem.Loss_dBm :=0.04;  oItem.Reflection_coef := 0.6;

  oItem :=AddItem;
  oItem.Name :='City';     oItem.Height :=0;  oItem.Loss_dBm :=0.2;   oItem.Reflection_coef := 0.8;

  oItem :=AddItem;
  oItem.Name :='OneBuild'; oItem.Height :=30; oItem.Loss_dBm :=0.2;   oItem.Reflection_coef := 0.8;

  oItem :=AddItem;
  oItem.Name :='OneHouse'; oItem.Height :=7;  oItem.Loss_dBm :=0.15;  oItem.Reflection_coef := 0.6;



(*    <item type="Forest" Height="20" Loss="0.06" reflection_coef="0.5"/>
      <item type="Country" Height="7" Loss="0.04" reflection_coef="0.6"/>
      <item type="City" Height="0"    Loss="0.2" reflection_coef="0.8"/>
      <item type="OneBuild" Height="30" Loss="0.2" reflection_coef="0.8"/>
      <item type="OneHouse" Height="7" Loss="0.15" reflection_coef="0.6"/>
*)

end;


constructor TCalcRegion.Create;
begin
  inherited Create;
  Clutters := TcmpClutterItemList.Create();
end;

destructor TCalcRegion.Destroy;
begin
  FreeAndNil(Clutters);
  inherited Destroy;
end;

function TCalcRegion.GetCRC32: LongWord;
var
  s: string;
begin
  s := Format('%d', [CalcStep]) +

       Format('%1.6f', [Abonent.Height]) +
       Format('%1.6f', [Abonent.Power_W]) +
       Format('%1.6f', [Abonent.Sensitivity]) +
       Format('%1.6f', [Abonent.FeederLoss]) +
       Format('%1.6f', [Abonent.Gain]) 
     ;

  Result := GetHashCRC32_AsLongWord(s);
end;

// ---------------------------------------------------------------
function TCalcRegion.Validate(aErrorList: Tstrings): boolean;
// ---------------------------------------------------------------
begin
  if (CalcStep<=0) then
    aErrorList.Add('��� ������� <=0');

(*  if (FBLRect.TopLeft.B<=0)     then aErrorList.Add('������� ������: ������� �����: ������<=0');
  if (FBLRect.TopLeft.L<=0)     then aErrorList.Add('������� ������: ������� �����: �������<=0');
  if (FBLRect.BottomRight.B<=0) then aErrorList.Add('������� ������: ������ ������: ������<=0');
  if (FBLRect.BottomRight.L<=0) then aErrorList.Add('������� ������: ������ ������: �������<=0');
*)
  if (Zone6<=0) then
    aErrorList.Add('6-�� ��������� ����<=0');

  if (CalcModel_ID<=0)     then
    aErrorList.Add('�� ������ ������ �������');

  if (ClutterModel_ID<=0)  then
    aErrorList.Add('�� ������ ������ �������� ��������');

  if (Refraction<=0) then
    aErrorList.Add('�� ����� ����������� ���������');
//    if (rec.Level_Stored_Noise<=0)  ????

  if (Abonent.Height<=0)  then
    aErrorList.Add('������ ������������ ���������<=0');

  if (Abonent.Power_W<=0) then
    aErrorList.Add('�������� ������������ ���������<=0');

  if (Abonent.Sensitivity >=0) then
    aErrorList.Add('���������������� ������������ ���������>=0');


end;


function TcmpAntenna.GetCRC32: LongWord;
var s: string;
begin
  s := Format('%1.6f', [Height]) +
       Format('%1.6f', [Azimuth]) +
       Format('%1.6f', [Tilt]) +

       Format('%1.6f', [blPos.B]) +
       Format('%1.6f', [blPos.L])
     ;

  Result := GetHashCRC32_AsLongWord(s);


(*      Height        : double; // ���o�� ����� �������
      FeederLoss_dB : double;
      Azimuth       : double; // ������ � ����
      Tilt          : double; // ������ ���������� � ���� //Slope_Tilt
*)

(*      Gain_dB   : double;  // ��������

      Horz_mask : string;
      Vert_mask : string;
*)

(*
  s := Format('%1.6f', [GAIN_dB]) +
       vert_mask +
       horz_mask;

  Result := GetHashCRC32(s);
*)
end;

procedure TcmpAntenna.Validate(aErrorList: Tstrings);
var
  s : string;
begin
  s := '';

  if (blPos.B<=0) then aErrorList.Add(s+'������<=0');
  if (blPos.L<=0) then aErrorList.Add(s+'�������<=0');

  if (AntennaType_ID<=0) then aErrorList.Add(s+'�� ����� ��� �������');

  if (height<=0) then aErrorList.Add(s+'������<=0');
  if (AZIMUTH<0) or (AZIMUTH>360) then
    aErrorList.Add(s+'������<0 ��� ������>360');

 // if (gain<=0) then aErrorList.Add(s+'��������<=0');
//  if (gain_dB<=0) then aErrorList.Add(s+'��������<=0');

end;


constructor TcmpClutterItemList.Create;
begin
  inherited Create(TcmpClutterItem);
end;

function TcmpClutterItemList.AddItem: TcmpClutterItem;
begin
  Result := TcmpClutterItem(Add);
end;

function TcmpClutterItemList.GetItem(Index: integer): TcmpClutterItem;
begin
  Result:=TcmpClutterItem(inherited Items[Index]);
end;


constructor TcmpReliefMatrixList.Create;
begin
  inherited Create(TcmpReliefMatrix);
end;

function TcmpReliefMatrixList.AddItem: TcmpReliefMatrix;
begin
  Result := TcmpReliefMatrix(Add);
end;

// ---------------------------------------------------------------
function TcmpReliefMatrixList.GetCRC32: LongWord;
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
begin
  s :='';

  for I := 0 to Count - 1 do
    s :=s+ ExtractFileName(Items[i].FileName);

(*  s := Format('%1.6f', [TRX.Power_W]) +
       Format('%1.6f', [TRX.Sensitivity_dBm]) +
       Format('%1.6f', [TRX.Freq_MHz])
       ;
*)

  Result := GetHashCRC32_AsLongWord(s);

end;

function TcmpReliefMatrixList.GetItem(Index: integer): TcmpReliefMatrix;
begin
  Result := TcmpReliefMatrix(inherited Items[Index]);
end;


function TcmpAntennaType.GetCRC32: LongWord;
var s: string;
begin
(*      Gain_dB   : double;  // ��������

      Horz_mask : string;
      Vert_mask : string;
*)

  s := Format('%1.6f', [GAIN]) +
       vert_mask +
       horz_mask;

  Result := GetHashCRC32_AsLongWord(s);
end;

// ---------------------------------------------------------------
function TCalcParams.AnalyzeCRC32: Integer;
// ---------------------------------------------------------------
var
  I: Integer;
  sIniFile: string;

  old_crc: TCRC_rec;

  oCell: TcmpCell;
  oAntenna: TcmpAntenna;
  oAntennaType: TcmpAntennaType;
  oCalcModelType: TcmpCalcModelType;

  oIni: TIniFile;
  iID: integer;

  bDiff : Boolean;
begin
  Result := 0;

  for I := 0 to Cells.Count - 1 do
  begin
    oCell :=Cells[i];

    sIniFile := ChangeFileExt(oCell.kupFileName, '.crc32') ;

    FillChar(oCell.CRC_rec, SizeOf(oCell.CRC_rec), 0);


    oCell.CRC_rec.CRC_Cell := oCell.GetCRC32;
    oCell.CRC_rec.CRC_CalcRegion := CalcRegion.GetCRC32;
 //   oCell.CRC_rec.CRC_Relief := ReliefMatrixFiles.GetCRC32;


    oCalcModelType:=CalcModelTypes.FindByID(CalcRegion.CalcModel_ID);

    if Assigned(oCalcModelType) then
      oCell.CRC_rec.CRC_CalcModel := oCalcModelType.GetCRC32;


    if oCell.Antennas.Count>0 then
    begin
      oAntenna:=oCell.Antennas[0];

      oCell.CRC_rec.CRC_Antenna := oAntenna.GetCRC32;

      iID := oAntenna.AntennaType_ID;

      oAntennaType := AntennaTypes.FindByID(iID);
      if Assigned(oAntennaType) then
        oCell.CRC_rec.CRC_AntennaType := oAntennaType.GetCRC32;

    end;

    bDiff := True;

    if FileExists(sIniFile) then
    begin
      oIni:=TIniFile.Create (sIniFile);

      old_crc.CRC_Antenna    := StrToInt64(oIni.ReadString('main','Antenna', '0'));
      old_crc.CRC_AntennaType:= StrToInt64(oIni.ReadString('main','AntennaType', '0'));
      old_crc.CRC_CalcRegion := StrToInt64(oIni.ReadString('main','CalcRegion', '0'));
      old_crc.CRC_Cell       := StrToInt64(oIni.ReadString('main','cell', '0'));
      old_crc.CRC_CalcModel  := StrToInt64(oIni.ReadString('main','CalcModel', '0'));
    //  old_crc.CRC_Relief     := StrToInt64(oIni.ReadString('main','Relief', '0'));

      oIni.Free;

    //  bDiff :=oCell.CRC_rec = old_crc;

      bDiff :=
        (oCell.CRC_rec.CRC_Antenna    <> old_crc.CRC_Antenna) or
        (oCell.CRC_rec.CRC_AntennaType<> old_crc.CRC_AntennaType) or
        (oCell.CRC_rec.CRC_CalcRegion <> old_crc.CRC_CalcRegion) or
        (oCell.CRC_rec.CRC_Cell       <> old_crc.CRC_Cell) or
        (oCell.CRC_rec.CRC_CalcModel  <> old_crc.CRC_CalcModel)
      //and
     //   (oCell.CRC_rec.CRC_Relief     = old_crc.CRC_Relief)

         ;
    end;

    if bDiff then
      Inc(Result);

    oCell.EnabledToExport := bDiff;
  end;
end;


// ---------------------------------------------------------------
procedure TCalcParams.SaveCRC32;
// ---------------------------------------------------------------
var
  I: Integer;
  sIniFile: string;

  oIni: TIniFile;
  oCell: TcmpCell;

begin
  for I := 0 to Cells.Count - 1 do
  begin
    oCell :=Cells[i];

    if oCell.EnabledToExport and
       FileExists (oCell.kupFileName) then
    begin
      sIniFile := ChangeFileExt(oCell.kupFileName, '.crc32') ;

      oIni:=TIniFile.Create (sIniFile);
      oIni.WriteString('main','Antenna',       IntToStr(oCell.CRC_rec.CRC_Antenna));
      oIni.WriteString('main','AntennaType',   IntToStr(oCell.CRC_rec.CRC_AntennaType));
      oIni.WriteString('main','CalcRegion',    IntToStr(oCell.CRC_rec.CRC_CalcRegion));
      oIni.WriteString('main','cell',          IntToStr(oCell.CRC_rec.CRC_Cell));
      oIni.WriteString('main','CalcModel',     IntToStr(oCell.CRC_rec.CRC_CalcModel));
//      oIni.WriteString('main','Relief',        IntToStr(oCell.CRC_rec.CRC_Relief));


      oIni.Free;

    end;
    // INI file values

  //  crcArr[0,0].Cell := oCell.Cell;

  end;


end;


function TcmpCalcModelType.GetCRC32: LongWord;
var s: string;
   I: Integer;
begin
  s :='';

  for I := 0 to High(Params) do
    s := s+ Params[i].Name +
            Format('%1.6f', [Params[i].Value]);

  s := IntToStr(ModelType) + s;

  Result := GetHashCRC32_AsLongWord(s);
end;



begin
 // IsWsOnly := False;
end.
