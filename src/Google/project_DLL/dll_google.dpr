library dll_google;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  Classes,
  d_Google_export in '..\src\d_Google_export.pas' {dlg_Google_export},
  dm_Google_export in '..\src\dm_Google_export.pas' {dmGoogle_export: TDataModule},
  dm_Main_app_g in '..\src\dm_Main_app_g.pas' {dmMain_app: TDataModule},
  fr_Google_filter in '..\src\fr_Google_filter.pas' {frame_Google_filter: TFrame},
  i_Google in '..\src shared\I_google.pas',
  SysUtils,
  u_Google_classes in '..\src\u_Google_classes.pas',
  u_ini_Google_params in '..\src shared\u_ini_Google_params.pas',
  x_Google in '..\src_dll\x_google.pas';

{$R *.res}

begin
end.
