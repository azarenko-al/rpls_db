unit fr_Google_filter;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid, DB,
  DBClient, Provider, ADODB,

  dm_Main,

  u_const_db,

  u_classes,
  u_db, RxMemDS, dxmdaset
  ;

type
  Tframe_Google_filter = class(TFrame)
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    cxGrid1DBTableView1checked: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    qry_Links: TADOQuery;
    DataSource1: TDataSource;
    dxMemData1: TdxMemData;
  private
    { Private declarations }
  protected
//    procedure ExecuteProc; override;
  public
    procedure Init;
    procedure OpenData(aProjectID: Integer);

    procedure GetCheckedList(aIDList: TIDList);

  end;

implementation

{$R *.dfm}



procedure Tframe_Google_filter.Init;
begin
  cxGrid1.Align:=alClient;

  db_SetComponentADOConnection(Self, dmMain.ADOConnection);
end;


procedure Tframe_Google_filter.GetCheckedList(aIDList: TIDList);
begin
  db_GetCheckedList (dxMemData1, aIDList);
end;


procedure Tframe_Google_filter.OpenData(aProjectID: Integer);
begin
  Assert (aProjectID>0);

//  if aProjectID=0 then
  //  Exit;

  db_OpenQuery(qry_Links,
               'SELECT id,name, CAST(0 AS bit) AS checked  FROM '+ view_LINK +
               ' WHERE project_id='+ IntToStr(aProjectID) + ' ORDER BY name');

  dxMemData1.Close;
  dxMemData1.Open;
  dxMemData1.LoadFromDataSet(qry_Links);
  dxMemData1.First;

//  db_OpenQuery(qry_Links,
 //             'SELECT id,name FROM '+ TBL_LINK +' WHERE project_id='+ IntToStr(dmMain.ProjectID));

end;

end.
