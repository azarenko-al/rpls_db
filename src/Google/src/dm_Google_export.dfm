object dmGoogle_export: TdmGoogle_export
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 903
  Top = 417
  Height = 635
  Width = 811
  object qry_Links: TADOQuery
    Parameters = <>
    Left = 32
    Top = 16
  end
  object qry_Props1: TADOQuery
    DataSource = ds_Linkend1
    Parameters = <>
    Left = 152
    Top = 192
  end
  object qry_Antenna1: TADOQuery
    DataSource = ds_Linkend1
    Parameters = <>
    Left = 336
    Top = 32
  end
  object qry_Props2: TADOQuery
    DataSource = ds_Linkend2
    Parameters = <>
    Left = 232
    Top = 192
  end
  object qry_Antenna2: TADOQuery
    DataSource = ds_Linkend2
    Parameters = <>
    Left = 416
    Top = 32
  end
  object qry_Projects: TADOQuery
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'SELECT id,name FROM PROJECT ORDER BY name')
    Left = 706
    Top = 11
  end
  object ds_Projects: TDataSource
    DataSet = qry_Projects
    Left = 708
    Top = 68
  end
  object ds_Links: TDataSource
    DataSet = qry_Links
    Left = 32
    Top = 80
  end
  object ds_Props1: TDataSource
    DataSet = qry_Props1
    Left = 152
    Top = 256
  end
  object ds_Props2: TDataSource
    DataSet = qry_Props2
    Left = 232
    Top = 256
  end
  object qry_Linkend1: TADOQuery
    DataSource = ds_Links
    Parameters = <>
    Left = 152
    Top = 24
  end
  object ds_Linkend1: TDataSource
    DataSet = qry_Linkend1
    Left = 152
    Top = 88
  end
  object qry_Linkend2: TADOQuery
    DataSource = ds_Links
    Parameters = <>
    Left = 240
    Top = 24
  end
  object ds_Linkend2: TDataSource
    DataSet = qry_Linkend2
    Left = 240
    Top = 88
  end
  object qry_Link_Repeater: TADOQuery
    DataSource = ds_Links
    Parameters = <>
    Left = 192
    Top = 392
  end
  object ds_Link_Repeater: TDataSource
    DataSet = qry_Link_Repeater
    Left = 192
    Top = 456
  end
  object qry_Link_Repeater_Ant: TADOQuery
    DataSource = ds_Link_Repeater
    Parameters = <>
    Left = 336
    Top = 400
  end
  object ds_Antenna1: TDataSource
    DataSet = qry_Antenna1
    Left = 336
    Top = 96
  end
  object ds_Antenna2: TDataSource
    DataSet = qry_Antenna2
    Left = 424
    Top = 96
  end
  object qry_Link_Repeater_prop: TADOQuery
    DataSource = ds_Link_Repeater
    Parameters = <>
    Left = 328
    Top = 456
  end
  object qry_Prop: TADOQuery
    DataSource = ds_Links
    Parameters = <>
    Left = 32
    Top = 408
  end
end
