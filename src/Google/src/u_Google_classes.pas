unit u_Google_classes;

interface

uses
  Classes, DB, SysUtils,

  u_classes,

  u_const_db,

  u_geo,
  u_db
  ;

type
  TDBLinkRepeater = class;
  TAntennaList = class;
  TLinkEnd= class;

  //---------------------------------------------------------------------------
  TProperty = class(TCollectionItem)
  //---------------------------------------------------------------------------
  private
  public
    id : Integer;
    Name_ : string;

    lat : double;
    lon : double;

    lat_str : string;
    lon_str : string;

    address : string;

    procedure LoadFromDataset(aDataset: TDataset);

  //  LinkEnd_ID : Integer;
  //  Height: Integer;
  end;

  TAntenna = class(TCollectionItem)
  public
  //  LinkEnd_ID : Integer;
    Height: double;
  end;

  //---------------------------------------------------------------------------
  TLink = class(TCollectionItem)
  //---------------------------------------------------------------------------
  public
    Name_ : string;

    LinkEnd1: TLinkEnd;
    LinkEnd2: TLinkEnd;
    Property1: TProperty;
    Property2: TProperty;


    LinkEnd1_ID : Integer;
    LinkEnd2_ID : Integer;

    PROPERTY1_id : Integer;
    PROPERTY2_id : Integer;

    ObjName : string;

    LinkRepeater_ID : Integer;
  public

    LinkRepeater: TDBLinkRepeater;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
  end;

  //---------------------------------------------------------------------------
  TLinkEnd = class(TCollectionItem)
  //---------------------------------------------------------------------------
  public
    ID : Integer;
    Name_ : string;
//    PROPERTY_id : Integer;

    TX_FREQ_MHZ : double;


    AntennaList: TAntennaList;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;

    procedure LoadFromDataset(aDataset: TDataset);

  end;

  //---------------------------------------------------------------------------
  TAntennaList = class(TCollection)
  //---------------------------------------------------------------------------
  private
    function GetItems(Index: Integer): TAntenna;
  public
    constructor Create;
    
    function AddItem: TAntenna;
    procedure LoadFromDataset(aDataset: TDataset);
    property Items[Index: Integer]: TAntenna read GetItems; default;
  end;

  //---------------------------------------------------------------------------
  TLinkList = class(TCollection)
  //---------------------------------------------------------------------------
  private
    function GetItems(Index: Integer): TLink;
  public
    PROPERTY_IDList : TIDList;
    LinkEnd_IDList : TIDList;



    constructor Create;
    function AddItem: TLink;

//    procedure Prepare;
    procedure LoadFromDataset(aDataset: TDataset);

    property Items[Index: Integer]: TLink read GetItems; default;
  end;


  TLinkEndList = class(TCollection)
  private
    function GetItems(Index: Integer): TLinkEnd;
  public
    constructor Create;
    function AddItem: TLinkEnd;
    property Items[Index: Integer]: TLinkEnd read GetItems; default;
  end;


  TPropertyList = class(TCollection)
  private
    function GetItems(Index: Integer): TProperty;
  public
    constructor Create;
    function AddItem: TProperty;
    procedure LoadFromDataset(aDataset: TDataset);

    property Items[Index: Integer]: TProperty read GetItems; default;
  end;


  // ---------------------------------------------------------------
  TDBLinkRepeater = class
  // ---------------------------------------------------------------
  private
  protected
  public
    ID : Integer;

     Antenna1_Height : double;
     Antenna2_Height : Double;

//     Property_Pos: TBLPoint;
     Property_ID : Integer;

     Property_: TProperty;

  public

    constructor Create;
    destructor Destroy; override;

    procedure LoadFromDataset(aDataset: TDataset);
  end;



  TDBProject = class
  private
  public
    LinkList: TLinkList;
    PropertyList: TPropertyList;

    constructor Create;
    destructor Destroy; override;

  end;




implementation

constructor TLinkEndList.Create;
begin
  inherited Create(TLinkEnd);
end;

// ---------------------------------------------------------------
function TLinkEndList.AddItem: TLinkEnd;
// ---------------------------------------------------------------
begin
  Result := TLinkEnd(inherited Add);
end;

function TLinkEndList.GetItems(Index: Integer): TLinkEnd;
begin
  Result := TLinkEnd(inherited Items[Index]);
end;

constructor TAntennaList.Create;
begin
  inherited Create(TAntenna);
end;

// ---------------------------------------------------------------
function TAntennaList.AddItem: TAntenna;
// ---------------------------------------------------------------
begin
  Result := TAntenna(inherited Add);
end;

function TAntennaList.GetItems(Index: Integer): TAntenna;
begin
  Result := TAntenna(inherited Items[Index]);
end;


// ---------------------------------------------------------------
procedure TAntennaList.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
var
  oAntenna: TAntenna;
begin
 // aDataset.First;

  with aDataset do
    while not EOF do
    begin
      oAntenna:=AddItem();

      oAntenna.Height:=FieldByName(FLD_HEIGHT).AsFloat;

      Next;
    end;
end;



constructor TLinkList.Create;
begin
  inherited Create(TLink);

  PROPERTY_IDList := TIDList.Create;
  LinkEnd_IDList  := TIDList.Create;


end;

// ---------------------------------------------------------------
function TLinkList.AddItem: TLink;
// ---------------------------------------------------------------
begin
  Result := TLink(inherited Add);
end;  


// ---------------------------------------------------------------
procedure TLinkList.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
var
  oLink: TLink;
begin
  aDataset.First;

  with aDataset do
    while not EOF do
    begin
      oLink:=AddItem();

      oLink.PROPERTY1_id:=FieldByName(FLD_PROPERTY1_ID).AsInteger;
      oLink.PROPERTY2_id:=FieldByName(FLD_PROPERTY2_ID).AsInteger;

      oLink.LinkEnd1_ID:=FieldByName(FLD_LINKEND1_ID).AsInteger;
      oLink.LinkEnd2_ID:=FieldByName(FLD_LINKEND2_ID).AsInteger;


      oLink.ObjName:=FieldByName(FLD_ObjName).AsString;

      oLink.LinkRepeater_ID :=  FieldByName(FLD_LINK_REPEATER_ID).AsInteger;


      Next;
    end;
end;


function TLinkList.GetItems(Index: Integer): TLink;
begin
  Result := TLink(inherited Items[Index]);
end;

{
//---------------------------------------------------------------------------
procedure TLinkList.Prepare;
//---------------------------------------------------------------------------
var
  I: Integer;
begin
  PROPERTY_IDList.Clear;
  LinkEnd_IDList.Clear;

  for I := 0 to Count - 1 do    // Iterate
  begin
    PROPERTY_IDList.AddID(Items[i].PROPERTY1_id );
    PROPERTY_IDList.AddID(Items[i].PROPERTY2_id );

    LinkEnd_IDList.AddID(Items[i].LinkEnd1_ID );
    LinkEnd_IDList.AddID(Items[i].LinkEnd2_ID );

  end;

end;
}

constructor TPropertyList.Create;
begin
  inherited Create(TProperty);
end;

// ---------------------------------------------------------------
function TPropertyList.AddItem: TProperty;
// ---------------------------------------------------------------
begin
  Result := TProperty(inherited Add);
end;

function TPropertyList.GetItems(Index: Integer): TProperty;
begin
  Result := TProperty(inherited Items[Index]);
end;


// ---------------------------------------------------------------
procedure TPropertyList.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
var
  oProperty: TProperty;
begin
 // aDataset.First;

  with aDataset do
    while not EOF do
    begin
      oProperty:=AddItem();

      oProperty.id:=FieldByName(FLD_ID).AsInteger;

      oProperty.address:=FieldByName(FLD_address).AsString;
      oProperty.Name_:=FieldByName(FLD_Name).AsString;

      oProperty.lat:=  FieldByName(FLD_LAT).AsFloat;
      oProperty.lon:=  FieldByName(FLD_LON).AsFloat;


      Next;
    end;
end;



constructor TDBProject.Create;
begin
  inherited Create;
  LinkList := TLinkList.Create();
  PropertyList := TPropertyList.Create();
end;


destructor TDBProject.Destroy;
begin
  FreeAndNil(PropertyList);
  FreeAndNil(LinkList);
  inherited Destroy;
end;

constructor TDBLinkRepeater.Create;
begin
  inherited Create;
  Property_ := TProperty.Create(nil);
end;

destructor TDBLinkRepeater.Destroy;
begin
  FreeAndNil(Property_);
  inherited Destroy;
end;

//---------------------------------------------------------------------------
procedure TDBLinkRepeater.LoadFromDataset(aDataset: TDataset);
//---------------------------------------------------------------------------
begin
  with aDataset do
    if not EOF then
  begin
    Antenna1_Height:=FieldByName(FLD_Antenna1_Height).AsFloat;
    Antenna2_Height:=FieldByName(FLD_Antenna2_Height).AsFloat;

//    Property_Ground_Height:=FieldByName(FLD_Ground_Height).AsVariant;
//    Property_Pos  :=  db_ExtractBLPoint (aDataset);

    Property_ID:=FieldByName(FLD_Property_ID).AsInteger;

    ID:=FieldByName(FLD_ID).AsInteger;


   // Assert(Property_Pos.B<>0, 'Value <=0');

  end;

end;

constructor TLink.Create(Collection: TCollection);
begin
  inherited Create(Collection);


  LinkEnd1 := TLinkEnd.Create(nil);
  LinkEnd2 := TLinkEnd.Create(nil);

  Property1 := TProperty.Create(nil);
  Property2 := TProperty.Create(nil);

  LinkRepeater := TDBLinkRepeater.Create();
end;

destructor TLink.Destroy;
begin
  FreeAndNil(LinkRepeater);
  FreeAndNil(Property1);
  FreeAndNil(LinkEnd1);

  FreeAndNil(Property2);
  FreeAndNil(LinkEnd2);



  FreeAndNil(LinkRepeater);
  inherited Destroy;
end;

constructor TLinkEnd.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  AntennaList := TAntennaList.Create();
end;

destructor TLinkEnd.Destroy;
begin
  FreeAndNil(AntennaList);
  inherited Destroy;
end;

procedure TLinkEnd.LoadFromDataset(aDataset: TDataset);
begin
  Assert(not aDataset.IsEmpty);

  with aDataset do
    if not EOF then
    begin
      id:=FieldByName(FLD_ID).AsInteger;
      Name_:=FieldByName(FLD_NAME).AsString;


      TX_FREQ_MHZ:=FieldByName(FLD_TX_FREQ_MHZ).AsFloat;

    end;

end;


// ---------------------------------------------------------------
procedure TProperty.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
begin
  Assert(not aDataset.IsEmpty);

  with aDataset do
    if not EOF then
    begin
      id:=FieldByName(FLD_ID).AsInteger;
      Name_:=FieldByName(FLD_NAME).AsString;

      address:= FieldByName(FLD_address).AsString;

      LAT:=FieldByName(FLD_LAT).AsFloat;
      LON:=FieldByName(FLD_LON).AsFloat;

      LAT_STR:=FieldByName(FLD_LAT_STR).AsString;
      LON_STR:=FieldByName(FLD_LON_STR).AsString;

    end;
end;



end.



        {







// ---------------------------------------------------------------
function TDBObjectList.FindByName(aName: string): TDBObject;
// ---------------------------------------------------------------
var I: Integer;
begin
 Result := nil;

  for I := 0 to Count - 1 do
    if Eq(Items[i].Name, aName) then
    begin
      Result := Items[i];
      Break;
    end;
end;

