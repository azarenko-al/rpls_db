unit d_Google_export;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  cxPropertiesStore, ComCtrls, StdCtrls, ExtCtrls, rxToolEdit,
  DB, ADODB, ActnList, DBCtrls,  Dialogs,

  d_Wizard,

  dm_Main,

  u_DB,

  u_ini_Google_params,

  u_func,

  u_files,

  dm_Google_export,

  Mask, rxPlacemnt, dxSkinsCore, dxSkinsDefaultPainters, cxLookAndFeels
  ;

type
  Tdlg_Google_export = class(Tdlg_Wizard)
    pn_File: TPanel;
    Label1: TLabel;
    ed_FileName: TFilenameEdit;
    pn_DB_Project: TPanel;
    Label2: TLabel;
    StatusBar1: TStatusBar;
    qry_Projects: TADOQuery;
    ds_Projects: TDataSource;
    DBLookupComboBox_Projects: TDBLookupComboBox;
    Bevel3: TBevel;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    cb_Frenel: TCheckBox;
    cb_LinkLabels: TCheckBox;
    cb_Prop1: TCheckBox;
    cb_Run_Google_Earth: TCheckBox;
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
//    procedure ed_ProjectButtonClick(Sender: TObject);
    procedure row_UseFilterEditPropertiesEditValueChanged(Sender: TObject);
    procedure StatusBar1DblClick(Sender: TObject);
  private
    procedure ConnectONEGA;
//    FProjectID: integer;

  public
    class procedure ExecDlg;
  end;


implementation     
{$R *.dfm}


class procedure Tdlg_Google_export.ExecDlg;
begin
  with Tdlg_Google_export.Create(Application ) do
  begin
    ShowModal;
    Free;
  end;
end;

procedure Tdlg_Google_export.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
begin
//  act_Ok.Enabled:=FProjectID>0

  act_Ok.Enabled:=DBLookupComboBox_Projects.KeyValue>0;

end;



procedure Tdlg_Google_export.ConnectONEGA;
begin
  if dmMain.OpenDB_Dlg then
  begin
    StatusBar1.SimpleText:=dmMain.LoginRec.ConnectionStatusStr;
    qry_Projects.Open;
  end;

end;

//-------------------------------------------------------------------
procedure Tdlg_Google_export.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

//  Caption:='������� ������ � Google Map';
  Caption :='������� ������ � Google Map | '+ GetAppVersionStr();//(Application.ExeName);


  SetActionName('������� ������ � Google Map');


 // GSPages1.Align:=alClient;


{  AddComponentProp(ed_FileName, 'FileName');
  AddComponentProp(row_Frenel, 'Text');
  cxPropertiesStore.RestoreFrom;
}

  SetDefaultWidth();

 // cxVerticalGrid1.Align:=alClient;


//  AddComponentProp (cb_Frenel,     DEF_PropertiesValue);
 // AddComponentProp (cb_LinkLabels, DEF_PropertiesValue);
//  AddComponentProp (row_Run_Google, DEF_PropertiesValue);
               
 // AddComponentProp (ed_FileName, 'FileName');

  cxPropertiesStore.RestoreFrom;    

  cb_Run_Google_Earth.Caption := '��������� Google Earth';

//  FProjectID:=dmMain.ProjectID;

(*  if (FProjectID <> 0) then
    pn_DB_Project.Visible:= False;
*)

(*  frame_Google_filter1.Align:=alClient;
  frame_Google_filter1.Init;

*)

  StatusBar1.SimpleText:=dmMain.LoginRec.ConnectionStatusStr;

 // cx_InitVerticalGrid (cxVerticalGrid1);


  db_SetComponentsADOConn([qry_Projects], dmMain.ADOConnection);

  qry_Projects.Open;

  DBLookupComboBox_Projects.KeyValue:=dmMain.ProjectID;

  //!!!!!!!!!!!!!!!!!!
//  DBLookupComboBox_Projects.KeyValue:=qry_Projects[FLD_ID];


end;


//-------------------------------------------------------------------
procedure Tdlg_Google_export.act_OkExecute(Sender: TObject);
//-------------------------------------------------------------------

var
  iCode: Integer;
  sKmlFile,sXmlFile: string;
begin
 // dmMain.ProjectID:=FProjectID;

  dmMain.ProjectID:=DBLookupComboBox_Projects.KeyValue;


  sXmlFile := ChangeFileExt(ed_FileName.FileName,'.xml');
  sKmlFile := ChangeFileExt(ed_FileName.FileName,'.kml');


  TdmGoogle_export.Init;
  dmGoogle_export.Params.ProjectID := g_Ini_Google_Params.ProjectID;

  dmGoogle_export.Params.IsLinkLabels:=cb_LinkLabels.Checked;
  dmGoogle_export.Params.IsShowFrenel:=cb_Frenel.Checked;
  dmGoogle_export.Params.IsExportProperties:=cb_Prop1.Checked;
   

 // dmGoogle_export.Params.IsUseFilter:=row_UseFilter.Properties.Value;
 // dmGoogle_export.Params.IsRunGoogle:=row_r.Properties.Value;


(*
  if row_UseFilter.Properties.Value then
    frame_Google_filter1.GetCheckedList (dmGoogle_export.CheckedIDList);
*)


  dmGoogle_export.Params.XmlFileName:=sXmlFile;
  dmGoogle_export.Params.KmlFileName:=sKmlFile;

  dmGoogle_export.ExecuteProc;

//  if g_Ini_Google_Params.Mode = mtProperty then
//    dmGoogle_export.ExecuteProc_Prop
//  else
//    dmGoogle_export.ExecuteProc;


  FreeAndNil(dmGoogle_export);

  // ---------------------------------------------------------------

//  sXmlFile


  RunApp ('ExportToKML.exe', DoubleQuotedStr(sXmlFile), iCode );
//  RunApp ('ExportToKML.exe', sXmlFile) ;

  if cb_Run_Google_Earth.Checked then
    if FileExists(sKmlFile) then
   // if row_Run_Google.Properties.Value then
       ShellExec(sKmlFile,'');

  ShowMessage('���������.');
  Close;
end;



procedure Tdlg_Google_export.Button1Click(Sender: TObject);
var
  iCode: Integer;
  sXmlFile: string;
begin
//  sXmlFile:='d:\g.xml';

 // RunApp ('ExportToKML.exe', DoubleQuotedStr(sXmlFile), iCode );
//  RunApp ('ExportToKML.exe', sXmlFile) ;


end;


procedure Tdlg_Google_export.row_UseFilterEditPropertiesEditValueChanged(
  Sender: TObject);
begin
  //if (Sender as TcxCheckBox).Checked then
   //  frame_Google_filter1.OpenData(FProjectID);
end;


procedure Tdlg_Google_export.StatusBar1DblClick(Sender: TObject);
begin
  ConnectONEGA;
end;

end.



//
//
//
//procedure Tdlg_Google_export.ed_ProjectButtonClick(Sender: TObject);
//(*var
//  FtmpID: integer;
//  FtmpName: string;*)
//begin
// (* //------------------
//  if Sender=ed_Project then
//  begin
//   // FtmpID:=
//    if Tdlg_DB_select.ExecDlg('', dmMain.ADOConnection,  TBL_PROJECT, FProjectID, FtmpName) then
//   // if FtmpID>0 then
//    begin
//      ed_Project.Text:=FtmpName + ' : '+ IntToStr(FtmpID);
////      FProjectID:=FtmpID;
//    end;
//
//  end else*)
//
//end;
//
//

