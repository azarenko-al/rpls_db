unit dm_Main_app_g;

interface

uses
  SysUtils, Classes, Dialogs,

  u_vars,

 // i_Google,

  dm_Main,
//  dm_Google_export,

  u_ini_Google_params,

  d_Google_export

  ;

type
  TdmMain_app = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
  end;


var
  dmMain_app: TdmMain_app;

//===================================================================
// implementation
//===================================================================
implementation
 {$R *.DFM}


//--------------------------------------------------------------
procedure TdmMain_app.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const
  DEF_TEMP_FILENAME = 'project_google.ini';
var
  iID: integer;
  iCount: integer;
  I: integer;
  iMode: integer;

  iMapDesktopID: integer;
  b: boolean;
  sTabFileName: string;
  iProjectID: integer;
  k: Integer;
  sIniFileName: string;


begin
  sIniFileName:=ParamStr(1);
//  if sIniFileName='' then
 //   sIniFileName:=TEMP_EXPORT_FILENAME;

  if sIniFileName='' then
    sIniFileName:=g_ApplicationDataDir+ DEF_TEMP_FILENAME;

//  ShowMessage(sIniFileName);


  g_Ini_Google_Params:=TIni_Google_Params.Create;
  g_Ini_Google_Params.LoadFromFile(sIniFileName);

  k:=g_Ini_Google_Params.Link_IDList.Count;
  k:=g_Ini_Google_Params.Property_IDList.Count;


  iProjectID:=g_Ini_Google_Params.ProjectID;

 //---------------------------------------------------------------------

  if iProjectID=0 then
  begin
    ShowMessage('Project_ID=0');
  /////  Exit;
  end;

//  Assert (iProjectID<>0);


  TdmMain.Init;
  if not dmMain.OpenDB_reg then
//  if not dmMain.OpenFromReg then
    Exit;


  {$DEFINE use_dll1111111}

  {$IFDEF use_dll}



  {$ENDIF}



  if iProjectID>0 then
    dmMain.ProjectID:=iProjectID;

(*  TdmGoogle_export.Init;
  dmGoogle_export.Params.ProjectID := g_Ini_Google_Params.ProjectID;
*)
  Tdlg_Google_export.ExecDlg;


  FreeAndNil(g_Ini_Google_Params);

{
if bIsConverted then
    ExitProcess(1)
  else
    ExitProcess(0);}

end;





end.

