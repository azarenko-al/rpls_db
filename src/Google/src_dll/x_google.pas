unit x_Google;

interface

uses  Sysutils, Dialogs, IniFiles, Windows, Messages,

  u_dll_with_dmMain,

  u_vars,

  dm_Main,

  dm_Google_export,

  u_ini_Google_params,

  d_Google_export ,


  i_Google;

type
  TGoogle_X = class(TDllCustomX, IGoogle_X)
  public
    procedure Execute(aFileName: WideString); stdcall;

  end;

  function DllGetInterface(const IID: TGUID; var Obj): HResult; stdcall;

exports
  DllGetInterface;


implementation



// ---------------------------------------------------------------
procedure TGoogle_X.Execute(aFileName: WideString);
// ---------------------------------------------------------------
const
  TEMP_INI_FILE = 'project_google.ini';

var
  b: Boolean;
  g_Ini_Google_Params: TIni_Google_Params;
  iProjectID: Integer;
  oIniFile: TIniFile;

 // oParams: TIni_Link_Optimize_Params;


begin
  Assert(Assigned(dmMain), 'Assigned(dmMain) not assigned');


  if not FileExists(aFileName ) then
  begin
    ShowMessage(aFileName);
    Exit;
  end;

 //---------------------------------------------------------------------

  g_Ini_Google_Params:=TIni_Google_Params.Create;
  g_Ini_Google_Params.LoadFromFile(aFileName);

  iProjectID:=g_Ini_Google_Params.ProjectID;

 //---------------------------------------------------------------------

  if iProjectID=0 then
  begin
    ShowMessage('Project_ID=0');
    Exit;
  end;

//  Assert (iProjectID<>0);

(*
  TdmMain.Init;
  if not dmMain.OpenDB_reg then
//  if not dmMain.OpenFromReg then
    Exit;*)

 // if iProjectID>0 then
  dmMain.ProjectID:=iProjectID;


  TdmGoogle_export.Init;
  dmGoogle_export.Params.ProjectID := g_Ini_Google_Params.ProjectID;

  Tdlg_Google_export.ExecDlg;


  FreeAndNil(g_Ini_Google_Params);


  FreeAndNil(dmGoogle_export);



//
//
//  oParams:= TIni_Link_Optimize_Params.Create;
//  oParams.LoadFromFile(aFileName);
//
//
//  dmMain.ProjectID:=oParams.ProjectID;
//
//
//
//  if oParams.Mode=LowerCase('Optimize_freq_diversity') then
//    b:=Tdlg_Link_Optimize_freq_diversity.ExecDlg(oParams.LinkID, oParams)
//  else
//
//  if oParams.Mode=LowerCase('Optimize_antennas') then
//    b:=Tdlg_Link_antennas_optimize.ExecDlg (oParams.LinkID, oParams)
//  else
//    ShowMessage('Mode=?');
//
//
//  if b then
//  begin
//  //  ShowMessage('WM_REFRESH_DATA: ' + IntToStr(iHandle));
//
//    if oParams.Handle>0 then
//      SendMessage (oParams.Handle, WM_REFRESH_DATA, 0,0);
//
//  end;
//
//
//  FreeAndNil(oParams);
//
//
//

(*

  TdmMain.Init;
  if not dmMain.OpenDB_reg then
    Exit;
*)
(*

  Tfrm_Main_link_graph.ExecDlg (oParams.LinkID, oParams);


  FreeAndNil(oParams);


  *)

end;


// ---------------------------------------------------------------
function DllGetInterface(const IID: TGUID; var Obj): HResult;
// ---------------------------------------------------------------
begin
  Result:=S_FALSE; Integer(Obj):=0;

  if IsEqualGUID(IID, IGoogle_X) then
  begin
    with TGoogle_X.Create() do
      if GetInterface(IID,OBJ) then
        Result:=S_OK
  end else
    raise Exception.Create('if IsEqualGUID(IID, ILink_graphX)');


end;



begin
 // RegisterClass(TdmMain_bpl);

end.


