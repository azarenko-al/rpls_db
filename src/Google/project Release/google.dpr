 program google;

uses
  Forms,
  Windows,
  d_Google_export in '..\src\d_Google_export.pas' {dlg_Google_export},
  dm_Google_export in '..\src\dm_Google_export.pas' {dmGoogle_export},
  dm_Main_app_g in '..\src\dm_Main_app_g.pas' {dmMain_app: TDataModule},
  u_ini_Google_params in '..\src shared\u_ini_Google_params.pas',
  u_Google_classes in '..\src\u_Google_classes.pas';

{$R *.RES}


begin
  SetThreadLocale(1049);

  Application.Initialize;
  Application.CreateForm(TdmMain_app, dmMain_app);
  Application.Run;
end.
