 program google;

uses
  d_Google_export in '..\src\d_Google_export.pas' {dlg_Google_export},
  d_Wizard in '..\..\..\..\common\Package_Common\d_Wizard.pas' {dlg_Wizard},
  dm_Google_export in '..\src\dm_Google_export.pas' {dmGoogle_export},
  dm_Main_app_g in '..\src\dm_Main_app_g.pas' {dmMain_app: TDataModule},
  Forms,
  i_Google in '..\src shared\I_google.pas',
  u_ini_Google_params in '..\src shared\u_ini_Google_params.pas',
  u_xml_document in '..\..\..\..\common\Package_Common\u_xml_document.pas';

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(TdmMain_app, dmMain_app);
  //  Application.CreateForm(Tdlg_Dataset_Select_records, dlg_Dataset_Select_records);
  Application.Run;
end.
