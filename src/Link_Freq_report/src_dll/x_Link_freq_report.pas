unit x_Link_freq_report ;

interface

uses  Sysutils, Dialogs, IniFiles, Windows, Messages,

  dm_Main,
                                u_dll_with_dmMain,
  u_ini_Link_report_params,

  I_Link_freq_report;


type
  TLink_freq_report_X = class(TDllCustomX, ILink_freq_report_X)
  public
    procedure Execute(aFileName: WideString); stdcall;

  end;

  function DllGetInterface(const IID: TGUID; var Obj): HResult; stdcall;

exports
  DllGetInterface;


implementation

uses
  u_ini_Link_freq_report_params;



// ---------------------------------------------------------------
procedure TLink_freq_report_X.Execute(aFileName: WideString);
// ---------------------------------------------------------------
const
  TEMP_INI_FILE = 'link_report.ini';

var
  b: Boolean;
  oParam: TIni_Link_freq_Report_Param;

  sReportFileName : string;

begin
  Assert(Assigned(dmMain), 'Assigned(dmMain) not assigned');


  if not FileExists(aFileName ) then
  begin
    ShowMessage(aFileName);
    Exit;
  end;



  oParam:=TIni_Link_freq_Report_Param.Create;
  oParam.LoadFromINI(aFileName);


  FreeAndNil(oParam);


 // u_ini_Link_report_params


end;


// ---------------------------------------------------------------
function DllGetInterface(const IID: TGUID; var Obj): HResult;
// ---------------------------------------------------------------
begin
  Result:=S_FALSE; Integer(Obj):=0;

  if IsEqualGUID(IID, ILink_freq_report_X) then
  begin
    with TLink_freq_report_X.Create() do
      if GetInterface(IID,OBJ) then
        Result:=S_OK
  end else
    raise Exception.Create('if IsEqualGUID(IID, ILink_graphX)');


end;



begin
 // RegisterClass(TdmMain_bpl);

end.


