program dll_Link_freq_report;



uses
  d_Setup in '..\src\d_Setup.pas' {dlg_Setup},
  dm_App_link_freq_report in '..\src\dm_App_link_freq_report.pas' {dmMain_app__freq_report: TDataModule},
  dm_FastReport in '..\..\..\..\common7\dm_FastReport.pas' {dmFastReport: TDataModule},
  dm_Link_freq_plan_report in '..\src\dm_Link_freq_plan_report.pas' {dmLink_freq_plan_report: TDataModule},
  Forms,
  u_dll_geo_convert in '..\..\..\..\DLL\Geo_Convert\shared\u_dll_geo_convert.pas',
  u_GEO in '..\..\..\..\common7\Package GEO\src\u_geo.pas',
  u_ini_Link_freq_report_params in '..\src_shared\u_ini_Link_freq_report_params.pas',
  u_Link_freq_report_classes in '..\src\u_Link_freq_report_classes.pas';

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(TdmMain_app__freq_report, dmMain_app__freq_report);
  Application.Run;
end.
