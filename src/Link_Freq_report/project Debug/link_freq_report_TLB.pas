unit link_freq_report_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 2014-10-23 12:59:38 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\RPLS_DB\src\Link_Freq_report\project Debug\link_freq_report.tlb (1)
// LIBID: {9CC6A8E8-E6DE-4294-8F2B-050D83E0BF67}
// LCID: 0
// Helpfile: 
// HelpString: link_freq_report Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS1\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  link_freq_reportMajorVersion = 1;
  link_freq_reportMinorVersion = 0;

  LIBID_link_freq_report: TGUID = '{9CC6A8E8-E6DE-4294-8F2B-050D83E0BF67}';

  IID_IController: TGUID = '{A94CD933-11C0-458A-95E9-D0B51CD0D23F}';
  CLASS_Controller: TGUID = '{474E75E3-69A4-461A-979C-B64D59B42BB4}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IController = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  Controller = IController;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//

  Alias1 = Int64; 

// *********************************************************************//
// Interface: IController
// Flags:     (256) OleAutomation
// GUID:      {A94CD933-11C0-458A-95E9-D0B51CD0D23F}
// *********************************************************************//
  IController = interface(IUnknown)
    ['{A94CD933-11C0-458A-95E9-D0B51CD0D23F}']
    function Run(aConnectionObject: Integer): HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoController provides a Create and CreateRemote method to          
// create instances of the default interface IController exposed by              
// the CoClass Controller. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoController = class
    class function Create: IController;
    class function CreateRemote(const MachineName: string): IController;
  end;

implementation

uses ComObj;

class function CoController.Create: IController;
begin
  Result := CreateComObject(CLASS_Controller) as IController;
end;

class function CoController.CreateRemote(const MachineName: string): IController;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Controller) as IController;
end;

end.
